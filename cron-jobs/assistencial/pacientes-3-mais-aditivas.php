<?php
set_time_limit(600);

include('../../db/config.php');
include('../../vendor/autoload.php');

use App\Core\Config;
use App\Services\MailAdapter;
use App\Services\Gateways\SendGridGateway;
use App\Controllers\CronJobs\Assistencial\Assistencial;

$_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__).'/../');

$apiData = (object) Config::get('sendgrid');

$gateway = new MailAdapter(
    new SendGridGateway(
        new \SendGrid($apiData->user, $apiData->pass),
        new \SendGrid\Email()
    )
);

list($dir, $filenameAtual, $filenameAnterior) = Assistencial::cronPacientesComTresOuMaisAditivas();
$filePath = $dir . '/' . $filenameAtual;
$msg = "Segue em anexo.";

$titulo = "Resumo de Pacientes com 3 ou mais Aditivas";

$emails = require $_SERVER['DOCUMENT_ROOT'] . '/app/configs/email.php';
$para[]= ['email' => 'diradm@mederi.com.br', 'nome' => 'Diretor Administrativo - Mederi'];
$para[]= ['email' => 'dirassist@mederi.com.br', 'nome' => 'Diretoria de Assistência - Mederi'];
$para[]= ['email' => 'ti@mederi.com.br', 'nome' => 'TI - Mederi'];

if($filenameAnterior != '') {
    $gateway->adapter->addAttachment($dir . '/' . $filenameAnterior);
}
$gateway->adapter->sendSimpleMail($titulo, $msg, $para, $filePath, $filename);
unlink($filePath);