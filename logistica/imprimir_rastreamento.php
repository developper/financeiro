<?php
$id = session_id();
if(empty($id))
  session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');





class Relatorio_rastrear_itens_imprimir{
public function rastrear($tipo,$cod,$ientrada,$fentrada){
  	//$tipo=$_POST['tipo-rastreamento'];
  	$html = "<form>";
  	$html .= "<center><img src='../utils/logo.jpg' width='30%' /></center>";
    $html.= "<center><b>RELAT&Oacute;RIO DE ENTRADA  ENTRE {$ientrada} ATE {$fentrada}</b></center>";
  	
  	$html.= "<table class='mytable' width=100% >"; 
    $html.= "<tr bgcolor='#ccc'>";
    $html.= "<th><b>Nome</b> </th>";
    $html.= "<th><b>Fonecedor</b></th>";
    $html.= "<th><b>Data</b></th>";
    $html.= "<th><b>Quantidade</b></th>";
    $html.= "<th><b>Valor unit&aacute;rio</b></th>";
    $html.= "</tr>";
    $cor = false;
    $sql = "";
    $i = "1900-01-01";
    $f = "2200-01-01";
    if($ientrada != "") $i = implode("-",array_reverse(explode("/",$ientrada)));
    if($fentrada != "") $f = implode("-",array_reverse(explode("/",$fentrada)));
  
	if($tipo == "med"){
    	$sql = "SELECT 
            e.qtd as quantidade,
            e.valor,
            e.data, 
            f.razaoSocial as fornecedor,
            b.principio as nome, 
            b.apresentacao as segundoNome 
    	     FROM entrada as e,
             fornecedores as f,
             catalogo as b
    	    WHERE f.idFornecedores = e.fornecedor
            AND b.ID = e.CATALOGO_ID 
            AND e.data BETWEEN '{$i}' AND '{$f}' 
            AND b.tipo = 0 
            AND b.ID = '{$cod}'  
           ORDER BY e.data DESC, nome, segundoNome, fornecedor, quantidade DESC, valor DESC";
 } else if($tipo == "mat"){
    	$sql = "SELECT 
            e.qtd as quantidade,
            e.valor, 
            e.data, 
            f.razaoSocial as fornecedor,
            b.principio as nome,
            b.apresentacao as segundoNome 
    	    FROM entrada as e, 
            fornecedores as f, 
            catalogo as b
    	    WHERE f.idFornecedores = e.fornecedor 
            AND b.ID = e.CATALOGO_ID 
            AND e.data BETWEEN '{$i}' AND '{$f}' 
            AND b.tipo = 1 
            AND b.ID = '{$cod}'   
    	    ORDER BY e.data DESC, nome, segundoNome, fornecedor, quantidade DESC, valor DESC";
 } else if($tipo == "die"){
    	$sql = "SELECT 
            e.qtd as quantidade, 
            e.valor, 
            e.data,
            f.razaoSocial as fornecedor,
            b.principio as nome,
            b.apresentacao as segundoNome 
    	    FROM entrada as e, 
            fornecedores as f,
            catalogo as b
            WHERE f.idFornecedores = e.fornecedor 
             AND b.ID = e.CATALOGO_ID 
             AND e.data BETWEEN '{$i}' AND '{$f}'
             AND b.tipo = 3 
             AND b.ID = '{$cod}'   
    	     ORDER BY e.data DESC, nome, segundoNome, fornecedor, quantidade DESC, valor DESC";
 } else if($tipo == "fornecedor"){
    	$sql = "SELECT 
            e.qtd as quantidade,
            e.valor, 
            e.data,
            f.razaoSocial as fornecedor, 
            b.principio as nome, 
            b.apresentacao as segundoNome 
    	    FROM entrada as e,
            fornecedores as f, 
            catalogo as b
    	    WHERE f.idFornecedores = e.fornecedor
            AND b.ID = e.CATALOGO_ID 
            AND e.data BETWEEN '{$i}' AND '{$f}' 
            AND f.idFornecedores = '{$cod}'  
    	   ORDER BY e.data DESC, nome, segundoNome, fornecedor, quantidade DESC, valor DESC";
 } 
  	$result = mysql_query($sql) or trigger_error(mysql_error());
    $f = true;
    $cont=1;
    while($row = mysql_fetch_array($result)){ 
      $f = false;
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
       if($cont++%2==0)
			$cor = '#E9F4F8';
			else
			$cor = '#FFFFFF';
      
      $html.= "<tr  bgcolor={$cor}>"; 
      $data = implode("/",array_reverse(explode("-",$row['data'])));  
      $html.= "<td valign='top'>{$row['nome']} - {$row['segundoNome']}</td>";  
      $html.= "<td valign='top'>{$row['fornecedor']}</td>";  
      $html.= "<td valign='top'>{$data}</td>";
      $html.= "<td valign='top'>{$row['quantidade']}</td>";
      $html.= "<td valign='top'> R$ {$row['valor']}</td>";
      $html.= "</tr>";      
    }
    $html.= "</table>";
    
  


	$paginas []= $header.$html.= "</form>";
	
	
	//print_r($paginas);
//	print_r($paginas);
	
	$mpdf=new mPDF('pt','A4',9);
	$mpdf->SetHeader('página {PAGENO} de {nbpg}');
	$ano = date("Y");
	$mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
	$mpdf->WriteHTML("<html><body>");
	$flag = false;
	foreach($paginas as $pag){
		if($flag) $mpdf->WriteHTML("<formfeed>");
		$mpdf->WriteHTML($pag);
		$flag = true;
	}
	$mpdf->WriteHTML("</body></html>");
	$mpdf->Output('relatorio_entrada.pdf','I');
	exit;
	
	
	/*
	
	$mpdf=new mPDF('pt','A4',13);
    $mpdf->WriteHTML($html);
	$mpdf->Output('pacientes.pdf','I');
	*/
	
	
}

}

$p = new Relatorio_rastrear_itens_imprimir();
$p->rastrear($_GET['tipo'],$_GET['cod'],$_GET['inicio'],$_GET['fim']);


?>