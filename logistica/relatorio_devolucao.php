<?php

$id = session_id();
if(empty($id))
  session_start();
require_once('../db/config.php');
require_once('../utils/codigos.php');
require_once('query.php');
require_once ('busca.php');

class RelatorioDevolucao
{

	public function relatorio_devolucao()
	{
		$devolucao = new Buscas;
		echo "<div>";
		echo "<h1><center>BUSCAR " . htmlentities("DEVOLUÇÕES") . "</center></h1>";
		echo $devolucao->buscarSaidas(1);
		echo "</div>";
	}
}