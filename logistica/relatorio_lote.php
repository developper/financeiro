<?php

$id = session_id();
if(empty($id))
  session_start();
require_once('../db/config.php');
require_once('../utils/codigos.php');
require_once('query.php');

///////Relatório das entradas e saidas por lote.
class Relatorio_lote{
    public function rel_lote(){
       echo "<div id='div-relatorio-lote'>";
       echo "<h1><center>Relat&oacute;rio por Lote </center></h1>";
       echo "<b>Item: </b><input type='text' name='busa-item' class='OBG' id='busca-item' size='80' />";
       echo "<input type='hidden' id='id-novo-item' value='-1' />";
       echo "<input type='hidden' id='id-tipo' value='-1' />";    
       echo "<input type='hidden' id='catalogo_id' value='-1' />";
       echo "<b>Lote:</b><input type='text' id='lote' class='OBG'  />";
       echo "<fieldset style='border: 1px solid; font-size: 14px;width:30%;'>";
       echo "<legend><b>Per&iacute;odo</b></legend>";
       echo "<table style='margin:5 5 25px;width:30%;'>";
       echo "<tbody>";
       echo "<tr>";
       echo "<td><b>In&iacute;cio</b> </td><td>".input('text','inicio','fim',"class='data OBG' size='10' maxlength='10'")."</td>". 
    	"<td><b>Fim</b> </td><td>".input('text','fim','inicio',"class='data OBG' size='10' maxlength='10' ")."</td>";
       echo "</tbody></table>";
       echo "</fieldset><br>";
       echo "<p><button type='submit' id='pesquisa-relatorio-lote'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Pesquisar</span></button>";
       echo " </div>"; 
       echo "<div id='tabela-relatorio-lote'>";
       echo"</div>";
    }
}
?>
