<?php
require_once('../../../db/config.php');

$like = $_GET['term'];

$itens = array();

$sql = <<<SQL
SELECT 
  concat(principio,' ',apresentacao) as name,
  apresentacao,numero_tiss as cod, 
  ID as catalogo_id, 
  tipo as tipo 
  FROM 
    catalogo 
  WHERE 
    concat(principio,' ',apresentacao) like '%{$like}%' 
    AND catalogo.tipo = '{$_GET['tipo']}' 
    AND ATIVO = 'A' 
  ORDER BY 
    principio, apresentacao
SQL;

$result = mysql_query($sql);
while ($row = mysql_fetch_array($result)) {
    $item['value'] = $row['name'];
    $item['id'] = $row['cod'];
    $item['tipo'] = $row['tipo'];
    $item['catalogo_id'] = $row['catalogo_id'];

    array_push($itens, $item);
}
echo json_encode($itens);