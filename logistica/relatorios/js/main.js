$("#mostrar, #urs, #tipo").chosen();

$("#tipo").change(function(){
    var tipo = $(this).val();
    $("#buscar-por-item").hide();
    if(tipo != ''){
        $("#buscar-por-item").show();
    } else {
        $("#busca-item").val('');
        $("#item").val('');
    }
});

$("#busca-item").autocomplete({
    source: function(request, response) {
        $.getJSON('/logistica/relatorios/ajax/busca_catalogo.php', {
            term: request.term,
            tipo: $("#tipo").val()
        }, response);
    },
    minLength: 3,
    select: function( event, ui ) {
        $('#item').val(ui.item.catalogo_id);
        $(this).attr('item_name', ui.item.value);
    }
});

$(function () {
    $(".chosen-select").chosen({search_contains: true});

    $('.bstrap-datepicker').datetimepicker(
        {
            format: 'DD/MM/YYYY',
            locale: 'pt-br'
        }
    );
});