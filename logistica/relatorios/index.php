<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';
//ini_set('display_errors', 1);

use \App\Controllers\Logistica;

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);


$routes = [
	"GET" => [
		'/logistica/relatorios/?action=form-relatorio-itens-vencidos'           => '\App\Controllers\Logistica::formRelatorioItensVencidos',
		'/logistica/relatorios/?action=excel-relatorio-itens-vencidos'          => '\App\Controllers\Logistica::excelRelatorioItensVencidos',
        '/logistica/relatorios/?action=relatorio-notas-entrada'                 => '\App\Controllers\Logistica::formRelatorioNotasEntrada',
        '/logistica/relatorios/?action=excel-relatorio-notas-entrada'           => '\App\Controllers\Logistica::excelRelatorioNotasEntrada',
        '/logistica/relatorios/?action=relatorio-saida-interna'                 => '\App\Controllers\Logistica::formRelatorioSaidaInterna',
        '/logistica/relatorios/?action=excel-relatorio-saida-interna'           => '\App\Controllers\Logistica::excelRelatorioSaidaInterna',
		'/logistica/relatorios/?action=relatorio-saida-condensada'              => '\App\Controllers\Logistica::formRelatorioSaidaCondensada',
		'/logistica/relatorios/?action=excel-relatorio-saida-condensada'        => '\App\Controllers\Logistica::excelRelatorioSaidaCondensada',
        '/logistica/relatorios/?action=relatorio-aluguel-equipamentos'          => '\App\Controllers\Logistica::formRelatorioAluguelEquipamentos',
        '/logistica/relatorios/?action=excel-relatorio-aluguel-equipamentos'    => '\App\Controllers\Logistica::excelRelatorioAluguelEquipamentos',
		'/logistica/relatorios/?action=relatorioAjusteEstoque'    => '\App\Controllers\RelatorioAjusteEstoque::form',
        '/logistica/relatorios/?action=relatorio-saida-remessa'              => '\App\Controllers\Logistica::formRelatorioSaidaRemessa',
        '/logistica/relatorios/?action=excel-relatorio-saida-remessa'        => '\App\Controllers\Logistica::excelRelatorioSaidaRemessa',
        '/logistica/relatorios/?action=excel-pesquisar-movimentacoes'        => '\App\Controllers\Logistica::excelPesquisarMovimentacoes',

    ],
	"POST" => [
        '/logistica/relatorios/?action=form-relatorio-itens-vencidos'    => '\App\Controllers\Logistica::relatorioItensVencidos',
        '/logistica/relatorios/?action=relatorio-notas-entrada'          => '\App\Controllers\Logistica::relatorioNotasEntrada',
        '/logistica/relatorios/?action=relatorio-saida-interna'          => '\App\Controllers\Logistica::relatorioSaidaInterna',
		'/logistica/relatorios/?action=relatorio-saida-condensada'       => '\App\Controllers\Logistica::relatorioSaidaCondensada',
        '/logistica/relatorios/?action=relatorio-aluguel-equipamentos'   => '\App\Controllers\Logistica::relatorioAluguelEquipamentos',
		'/logistica/relatorios/?action=relatorioAjusteEstoque'    => '\App\Controllers\RelatorioAjusteEstoque::form',
        '/logistica/relatorios/?action=relatorio-saida-remessa'       => '\App\Controllers\Logistica::relatorioSaidaRemessa',

    ]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}
