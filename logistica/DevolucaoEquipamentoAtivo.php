<?php
$id = session_id();
if(empty($id))
  session_start();
require_once('../db/config.php');
require_once('../utils/codigos.php');
require_once('query.php');
require_once ('relatorio_equipamento_ativo.php');

class DevolucaoEquipamentoAtivo {
  public function devolverEequipamentoAtivo(){
    echo "<div id='div-paciente-equipamento-ativo'>";
    echo "<h1><center>Devolução de  Equipamentos Ativos  </center></h1>
          </br>
          <div style='border:2px dashed;padding: 6px'>
            <b>".htmlentities('Ao fazer a devolução de Equipamentos Ativos por essa
                                funcionalidade, o sistema irá automaticamente gerar uma devolução e removê-lo
                                da atual lista de Equipamentos Ativos.')."
            </b>
          </div>
          </br>";

    echo" <p><label><b>Paciente:</b></label></p>";
    $euipamento = New Relatorio_equipamento_ativo();
    $euipamento->paciente_equipamento();
    echo "<input type='hidden' id='convenio-equipamento' value='-1'>";

    echo"<br/><br/><button id='pesquisa-equipamento-ativo' data-tipo='devolucao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button' style='float:left;' type='button'>
              <span class='ui-button-text'>Pesquisar</span>
              </button>";
    echo " </div><br/><br/>";
    echo "<div id='tabela-equipamento-ativo'>";
    echo"</div>";
  }
}