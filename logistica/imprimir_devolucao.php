<?php
$id = session_id();
if(empty($id))
  session_start();
include_once('../validar.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../db/config.php');
include_once('busca.php');

class Devolucoes{
	
	public function imprimir_devolucao($tipo, $data_inicio, $data_fim, $codigo,$todos){  
  $condicao = '';
   $inicio = implode("-",array_reverse(explode("/",$data_inicio)));
   $fim = implode("-",array_reverse(explode("/",$data_fim)));

   $html = "<form>";
   
   $html .= "<center><img src='../utils/logo.jpg' width='30%' /></center>";
   $html.= "<center><h1><b>" . htmlentities("RELATÓRIO DE DEVOLUÇÕES") . "</b></h1></center><br>";
   $html .= Buscas::cabecalho($tipo, $data_inicio, $data_fim, $codigo);
   $html.= "<table class='mytable' width=100% >"; 
   $html.= "<thead><tr bgcolor='#ccc'>";
   $html.= "<th><b>Paciente</b></th>";   
   $html .= "<th><b>Data</b></th>";
   $aSelect = ", devolucao.DATA";
   $aGroupBy = "GROUP BY devolucao.data,  nome, paciente";
   $html .="<th><b>Item</b></th>";
   $html .="<th><b>QTD</b></th>";
   $html .="<th><b>Lote</b></th>";
   $html .="<th><b>Profissional</b></th>";
        $html .= "<th><b>Custo Unitário</b></th>";
        $html .= "<th><b>Subtotal</b></th>";

   $html .="</tr></thead>";
   $cor = false;
   $total = 0;
   if ($_SESSION['empresa_principal'] != '1') {
      $cond_empresa = " AND clientes.empresa = {$_SESSION['empresa_principal']} ";
    } else {
      $cond_empresa = "";
    }
        $condicao = '';

    if($tipo == 'eqp'){
        $condCod = $codigo != 'T' ? "AND devolucao.CATALOGO_ID = '{$codigo}' " : "";

      $sql= "SELECT 
              clientes.nome as paciente, 
              cobrancaplanos.id as cod, 
              cobrancaplanos.item as nome,
              devolucao.LOTE as LOTE,
              sum(devolucao.QUANTIDADE) as qtd ,
              devolucao.DATA_DEVOLUCAO,
              usuarios.nome as profissional,
              DATE_FORMAT(devolucao.DATA_DEVOLUCAO,'%Y-%m-%d') as dt,
              custo_servicos_plano_ur.CUSTO AS custo
            FROM
              cobrancaplanos INNER JOIN
              devolucao on (cobrancaplanos.id = devolucao.CATALOGO_ID) INNER JOIN
              clientes on (clientes.idClientes = devolucao.PACIENTE_ID) INNER JOIN
              usuarios on (devolucao.USUARIO_ID = usuarios.idUsuarios)
              LEFT JOIN custo_servicos_plano_ur ON cobrancaplanos.id = custo_servicos_plano_ur.COBRANCAPLANO_ID AND custo_servicos_plano_ur.UR={$_SESSION['empresa_principal']} and custo_servicos_plano_ur.STATUS = 'A'
            where devolucao.TIPO = 5 and {$condCod} and devolucao.DATA_DEVOLUCAO BETWEEN '{$inicio}' AND '{$fim}' and devolucao
            .PACIENTE_ID != 0
              {$cond_empresa}
            {$aGroupBy}      
            ORDER BY DATA_DEVOLUCAO ASC ";
    }
    else if($tipo == 'paciente') {

        $condPaciente = $codigo != 'T' ? "AND devolucao.PACIENTE_ID = '{$codigo}' " : "";

      $sql =" SELECT 
                clientes.nome as paciente, 
                devolucao.NUMERO_TISS,
                devolucao.CATALOGO_ID,
                concat(catalogo.principio,' - ',catalogo.apresentacao) as nome, 
                devolucao.LOTE, 
                devolucao.QUANTIDADE as qtd,
                devolucao.DATA_DEVOLUCAO,
                usuarios.nome as profissional,
                catalogo.valorMedio AS custo
              FROM devolucao INNER JOIN clientes on ( clientes.idClientes = devolucao.PACIENTE_ID) 
                INNER JOIN catalogo on(catalogo.ID= devolucao.CATALOGO_ID) INNER JOIN
                usuarios on (devolucao.USUARIO_ID = usuarios.idUsuarios)
              WHERE
                devolucao.TIPO in (0,1,3) 
                {$condPaciente}
                {$cond_empresa}
                AND devolucao.DATA_DEVOLUCAO BETWEEN '$inicio' AND '$fim'

      union
      SELECT
        clientes.nome as paciente, 
        devolucao.NUMERO_TISS,
        devolucao.CATALOGO_ID,
        cobrancaplanos.item as nome, 
        devolucao.LOTE, 
        devolucao.quantidade as qtd,
        devolucao.DATA_DEVOLUCAO,
        usuarios.nome  as profissional,
        custo_servicos_plano_ur.CUSTO AS custo
      FROM 
        devolucao INNER JOIN
        clientes on ( clientes.idClientes = devolucao.PACIENTE_ID) INNER JOIN
        cobrancaplanos on (cobrancaplanos.id = devolucao.CATALOGO_ID)  INNER JOIN
        usuarios on (devolucao.USUARIO_ID = usuarios.idUsuarios)
        LEFT JOIN custo_servicos_plano_ur ON cobrancaplanos.id = custo_servicos_plano_ur.COBRANCAPLANO_ID AND custo_servicos_plano_ur.UR={$_SESSION['empresa_principal']} and custo_servicos_plano_ur.STATUS = 'A'
      WHERE
        devolucao.TIPO = 5 
        {$condPaciente}
        AND devolucao.DATA_DEVOLUCAO   BETWEEN '$inicio' AND '$fim'
         {$cond_empresa}
      ORDER BY
        `nome` ASC";  

    }else{

      if($tipo == 'med'){
        $tip = 0;
      }else if($tipo == 'mat'){
        $tip = 1;
      }else{
        $tip = 3;
      }

        $condCod = $codigo != 'T' ? "AND devolucao.CATALOGO_ID = '{$codigo}' " : "";

      if( $_SESSION['empresa_principal']== 1){

        $sql="SELECT 
        clientes.nome as paciente, 
        devolucao.NUMERO_TISS,
        devolucao.CATALOGO_ID,
        concat(catalogo.principio,' - ',catalogo.apresentacao) as nome, 
        devolucao.LOTE, 
        devolucao.QUANTIDADE as qtd,
        devolucao.DATA_DEVOLUCAO,
        usuarios.nome as profissional,
        catalogo.valorMedio AS custo
        FROM 
        devolucao INNER JOIN 
        clientes on ( clientes.idClientes = devolucao.PACIENTE_ID) INNER JOIN 
        catalogo on(catalogo.ID = devolucao.CATALOGO_ID) INNER JOIN
        usuarios on (devolucao.USUARIO_ID = usuarios.idUsuarios)
        WHERE  
        devolucao.TIPO ={$tip} AND 
        devolucao.DATA_DEVOLUCAO BETWEEN '$inicio' AND '$fim' {$condCod}
        ORDER BY DATA_DEVOLUCAO";

      }else{
        $sql="SELECT 
        clientes.nome as paciente, 
        devolucao.NUMERO_TISS,
        devolucao.CATALOGO_ID,
        concat(catalogo.principio,' - ',catalogo.apresentacao) as nome, 
        devolucao.LOTE, 
        sum(devolucao.QUANTIDADE )as qtd,
        devolucao.DATA_DEVOLUCAO,
        usuarios.nome as profissional,
        catalogo.valorMedio AS custo
        FROM 
        devolucao INNER JOIN 
        clientes on ( clientes.idClientes = devolucao.PACIENTE_ID) INNER JOIN 
        catalogo on(catalogo.ID = devolucao.CATALOGO_ID) INNER JOIN
        usuarios on (devolucao.USUARIO_ID = usuarios.idUsuarios)
        WHERE  
        devolucao.TIPO ={$tip} AND 
        {$cond_empresa}
        devolucao.DATA_DEVOLUCAO BETWEEN '$inicio' AND '$fim' {$condCod}
      group by data,devolucao.CATALOGO_ID  
      order by 
      DATA_DEVOLUCAO";
      }
}
//die($sql);

$cor = 1;
$result = mysql_query($sql) or trigger_error(mysql_error());
$f = true;
        $totalCusto = 0;

while($row = mysql_fetch_array($result)){ 
  $f = false;
  foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
  
  if($cont++%2==0) {
        $cor = '#E9F4F8';
  }else{
        $cor = '#FFFFFF';
  }

    $subtotal = round($row['custo'], 2) * $row['qtd'];
  $html .="<tr bgcolor={$cor}>";
  $html .= "<td valign='top'>{$row['paciente']}</td>";  
  $data = implode("/",array_reverse(explode("-",$row['DATA_DEVOLUCAO'])));
  $html .= "<td valign='top'>{$data}</td>";
  $html .= "<td valign='top'>{$row['nome']}</td>";  

  $html .= "<td valign='top'>{$row['qtd']}</td>";
  $html .= "<td valign='top'>{$row['LOTE']}</td>";
  $html .= "<td valign='top'>{$row['profissional']}</td>";
    $html .= "<td valign='top'>R$ " . number_format($row['custo'], 2, ',', '.') . "</td>";
    $html .= "<td valign='top'>R$ " . number_format($subtotal, 2, ',', '.') . "</td>";
    $html .= "</tr>";
    $totalCusto += $subtotal;
}
        $html .= "<tr style='background-color:lightgrey'>
                <td colspan='7' style='text-align: right;b'> <b>Custo Total:</b></td>
                <td>R$ " . number_format($totalCusto, 2, ',', '.') . "</td>
              </tr>";
$html .= "</table>";

$html .= "</form>";

$paginas []= $header.$html.= "</form>";  
  
  $mpdf=new mPDF('pt','A4',10);
  $mpdf->SetHeader('página {PAGENO} de {nbpg}');
  $ano = date("Y");
  $mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
  $mpdf->WriteHTML("<html><body>");
  $flag = false;
  foreach($paginas as $pag){
    if($flag) $mpdf->WriteHTML("<formfeed>");
    $mpdf->WriteHTML($pag);
    $flag = true;
  }
  $mpdf->WriteHTML("</body></html>");
  $mpdf->Output('controlados.pdf','I');
  exit;
    
  }//imprimir_saida
  
}//Saidas

$s = new Devolucoes;
$s->imprimir_devolucao( $_GET["tipo"],$_GET["inicio"], $_GET["fim"], $_GET["codigo"], $_GET["todos"]);