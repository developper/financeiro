<?php
require_once('../db/config.php');
include_once('../utils/codigos.php');

class MAT{
  public function menu(){
    echo "<p><a href='?op=mat&act=cadastrar'>Cadastrar</a>";
    //echo "<button id='cadastrar'>Cadastrar</button>";
    echo "<p><a href='?op=mat&act=buscar'>Buscar</a>";    
  }
  
  public function cadastrar($nome,$descricao,$cb){
    if($nome == NULL){
      echo "<div id='div-cadastrar-material'>".alerta();
      echo "<form name=\"form\" action='' method='POST'>";
      echo "<p><b>Nome</b><input class='OBG' type='text' name='nome'/>";
      echo "<p><b>Descri&ccedil;&atilde;o</b><input class='OBG' type='text' name='descricao'/>";
      echo "<p><b>C&oacute;digo de Barras</b><input type='text' maxlength='13' name='codigobarras'/>";
      echo "<input type='submit' value='Inserir' id='cadastrar-material' />";
      echo "</form></div>";
    } else {
        $sql = "INSERT INTO `material` ( `nome` ,  `descricao`, codigobarras) VALUES(  '{$nome}' ,  '{$descricao}', '{$cb}') ";
      mysql_query($sql) or die(mysql_error());
      $this->buscar($nome);
    }
  }
  
  public function editar($id,$nome,$descricao,$cb){
    if($nome == NULL){
      $row = mysql_fetch_array ( mysql_query("SELECT * FROM `material` WHERE `idMaterial` = '$id' "));
      echo "<div id='div-cadastrar-material'>".alerta();
      echo "<form name=\"form\" action='' method='POST'>";
      echo "<p><b>Nome</b><input class='OBG' type='text' name='nome' value='".$row['nome']."' />";
      echo "<p><b>Descri&ccedil;&atilde;o</b><input class='OBG' type='text' size=50 name='descricao' value='".$row['descricao']."' />";
      echo "<p><b>C&oacute;digo de Barras</b><input type='text' maxlength='13' name='codigobarras' value='".$row['codigobarras']."'/>";
      echo "<input type='hidden' name='id' value='$id' />";
      echo "<input type='submit' value='Salvar' id='cadastrar-material' />";
      echo "</form></div>";
    } else {
      $sql = "UPDATE `material` SET `nome` = '{$nome}', `descricao` =  '{$descricao}', codigobarras = '{$cb}' WHERE idMaterial = '{$id}' "; 
      mysql_query($sql) or die(mysql_error());
      $this->buscar($nome);
    }
  }
  
  public function show_result($result,$nome){
    echo "<table class='mytable' width=100% >"; 
    echo "<thead><tr>"; 
    echo "<th><b>Nome</b></th>";
    echo "<th><b>Descri&ccedil;&atilde;o</b></th>"; 
    echo "<th><b>C&oacute;digo de Barras</b></th>";
    echo "<th colspan='2' ><b>Op&ccedil;&otilde;es</b></th>";
    echo "</tr></thead>";
    $cor = false;
    $total = 0;
    while($row = mysql_fetch_array($result)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      if($cor) echo "<tr>";
      else echo "<tr class='odd'>"; 
      $cor = !$cor;
      $total++;
      echo "<td valign='top'>" . $row['nome'] . "</td>";  
      echo "<td valign='top'>" . $row['descricao'] . "</td>";
      echo "<td valign='top'>" . $row['codigobarras'] . "</td>";
      $del = "index.php?op=mat&act=excluir&id={$row['idMaterial']}&confirm=ok";
      echo "<td valign='top'><a href=?op=mat&act=editar&id={$row['idMaterial']}><img src='../utils/edit_16x16.png' title='Editar' border='0'></a></td><td><a href=\"javascript:ConfirmChoiceMat('excluir','{$row['idMaterial']}','{$row['descricao']}','{$del}')\"><img src='../utils/delete_16x16.png' title='Excluir' border='0'></a></td> ";
      echo "</tr>";      
    }
    echo "</table>";
    if($total == 0) echo "<center>Nenhum resultado para <b><u>$nome</u></b> foi encontrado!</center>";
  }
  
  public function buscar($nome){
    if($nome == NULL){
      echo "<form name=\"form\" action='' method='POST'>";
      echo "<p><b>Nome / Descri&ccedil;&atilde;o / C&oacute;digo de Barras </b><input type='text' name='nome'/>";
      echo "<input type='submit' value='Buscar' />";
      echo "</form>";
    } else {
      $result = mysql_query("SELECT m.idMaterial, m.nome, m.descricao, m.codigobarras FROM `material` as m 
			    WHERE m.nome LIKE '%{$nome}%' OR m.descricao LIKE '%{$nome}%' OR m.codigobarras = '{$nome}' ") or trigger_error(mysql_error());
      $this->show_result($result,$nome);
    }
  }
  
  public function excluir($id){
    mysql_query("DELETE FROM `material` WHERE `idMaterial` = '$id' ") ;
    echo "Material exclu&iacute;do.<a href='?op=mat&act=buscar'>(voltar)</a><br />";
  }
}
?>