<?php
$id = session_id();
if(empty($id))
    session_start();
include_once('../validar.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../db/config.php');

class Saidas{

    //http://localhost/logistica/imprimir_saidas.php?resultado=a&tipo=paciente&inicio=2012-12-04&fim=2012-12-05

    public function imprimir_saida($resultado ,$tipo,$inicio ,$fim ,$codigo, $ur = 0 ){
        //var_dump($resultado ,$tipo,$inicio ,$fim ,$codigo, $ur);
        $iniciotext = \Datetime::createFromFormat('Y-m-d', $inicio);
        $fimtext = \Datetime::createFromFormat('Y-m-d', $fim);

        $html = "<form>";

        $html .= "<center><img src='../utils/logo.jpg' width='30%' /></center>";
        $html.= "<center><b>RELAT&Oacute;RIO DE SA&Iacute;DA  ENTRE ". $iniciotext->format('d/m/Y') ." ATE ". $fimtext->format('d/m/Y') ." </b></center>";
        $html.= "<table class='mytable' width=100% >";
        $html.= "<thead><tr bgcolor='#ccc'>";
        $html.= "<th><b>Paciente</b></th>";
        if($resultado == 'a'){
            $html .= "<th><b>Data</b></th>";
            $aSelect = ", s.data";
            $aGroupBy = "GROUP BY s.data,  nome, paciente";
        } else {
            $aGroupBy = "GROUP BY s.CATALOGO_ID";
        }
        $html .="<th><b>Item</b></th>";
        //$html .="<th><b>Segundo Nome</b></th>";
        $html .="<th><b>Quantidade</b></th>";
        $html .="<th><b>Lote</b></th>";
        $html .= "<th><b>Custo Unitário</b></th>";
        $html .= "<th><b>Subtotal</b></th>";
        $html .="</tr></thead>";
        $cor = false;
        $total = 0;

        if($tipo == 'eqp'){
            $condCod = $codigo == 'T' ? '' : "s.CATALOGO_ID ={$codigo} and";

            $sql= <<<SQL
SELECT
	c.nome as paciente,
	cb.id as cod,
	cb.item as nome,
	s.LOTE as LOTE,
	sum(s.quantidade) as qtd ,
	s.data,
	DATE_FORMAT(s.data,'%Y-%m-%d') as dt,
	cs.CUSTO AS valor
FROM
	cobrancaplanos as cb inner join
	saida as s on (cb.id = s.CATALOGO_ID) inner join
	clientes as c on (c.idClientes = s.idCliente)
	LEFT JOIN custo_servicos_plano_ur AS cs ON cb.id = cs.COBRANCAPLANO_ID AND cs.UR={$_SESSION['empresa_principal']} and cs.STATUS = 'A'
WHERE
	s.tipo = 5 and
	{$condCod}
	s.data BETWEEN '{$inicio}' AND '{$fim}' and
	s.idCliente != 0 AND
	c.empresa = {$_SESSION['empresa_principal']}
{$aGroupBy}
UNION
	SELECT
		c.nome as paciente,
		cb.id as cod,
		cb.item as nome,
		s.LOTE as LOTE,
		sum(s.quantidade) as qtd ,
		s.data,
		DATE_FORMAT(s.data,'%Y-%m-%d') as dt,
        cs.CUSTO AS valor
	FROM
		cobrancaplanos as cb inner join
		saida  as s on (cb.id = s.CATALOGO_ID) inner join
		empresas as c on (c.id = s.UR)
		LEFT JOIN custo_servicos_plano_ur AS cs ON cb.id = cs.COBRANCAPLANO_ID AND cs.UR={$_SESSION['empresa_principal']} and cs.STATUS = 'A'
	WHERE
		s.tipo = 5 and
		{$condCod}
		s.data BETWEEN '{$inicio}' AND '{$fim}' and
		s.UR != 0
		{$aGroupBy}
	ORDER BY
		data ASC
SQL;
        }else if($tipo == 'ur'){

            if($resultado == 'a'){

                $aGroupBy = "GROUP BY data, catalogo_id";
            }else{
                $aGroupBy = "GROUP BY  nome";
            }

            if( $codigo== 1)
            {
                $sql =
                    "SELECT 
                p.empresa,   
    		        IF(s.UR = 0 ,p.nome,e.nome) as paciente, 
                s.NUMERO_TISS,
                s.CATALOGO_ID,
                concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                s.LOTE, 
                s.quantidade as qtd,
                cb.valorMedio as valor,
                s.data
          FROM 
                saida as s Left join 
                clientes as p on ( p.idClientes = s.idCliente) inner join 
                catalogo as cb on(cb.ID = s.CATALOGO_ID) Left join
    			      empresas as e on (s.UR = e.id)
    			WHERE  
                s.tipo in (0,1,3) and
                s.data BETWEEN '$inicio' AND '$fim' AND 
    			      (                       
                  idCliente in(
                               select 
                                  idClientes 
                               from 
                                  clientes 
                               where 
                                  empresa = '1'
                              )
    		           OR s.UR <> 0
    			      )
    		
    		UNION ALL
    		SELECT 
                 p.empresa,   
    					   IF(s.UR = 0 ,p.nome,e.nome) as paciente, 
                 s.NUMERO_TISS,
                 s.CATALOGO_ID,
                 concat(cb.item,' -  ',if(s.segundoNome is null,'',s.segundonome)) as nome,
                 s.LOTE, 
                 s.quantidade as qtd,
                 cs.CUSTO as valor,
                 s.data
        FROM 
                 saida as s Left join 
                 clientes as p on ( p.idClientes = s.idCliente) inner join 
                 cobrancaplanos as cb on(cb.ID = s.CATALOGO_ID) Left join
    		         empresas as e on (s.UR = e.id)
    		         LEFT JOIN custo_servicos_plano_ur AS cs ON cb.id = cs.COBRANCAPLANO_ID AND cs.UR={$_SESSION['empresa_principal']} and cs.STATUS = 'A'
    		 WHERE  
                 s.tipo =5 and
                 s.data BETWEEN '$inicio' AND '$fim' AND 
    						(                       
                 idCliente in(
                             select 
                                idClientes 
                             from 
                                clientes 
                             where 
                                empresa = '1'
                            )
    					    OR s.UR <> 0
    						)
    		 ORDER BY 
              data ";
            }else{
                $condCod = $codigo == 'T' ? '' : "AND s.UR ={$codigo} ";

                $sql ="SELECT 
                  e.nome as paciente,
                  cb.id as cod,
                  cb.id as catalogo_id,
                  cb.item as nome,
                  s.LOTE as LOTE,
                  sum(s.quantidade) as qtd ,
                  cs.CUSTO as valor,
                  s.data
              FROM 
                  cobrancaplanos as cb inner join 
                  saida as s on (cb.id = s.CATALOGO_ID) inner join 
                  empresas as e on (e.id = s.UR)
                  LEFT JOIN custo_servicos_plano_ur AS cs ON cb.id = cs.COBRANCAPLANO_ID AND cs.UR={$_SESSION['empresa_principal']} and cs.STATUS = 'A' 
              WHERE 
                  s.tipo = 5 and
                  s.data BETWEEN '{$inicio}' AND '{$fim}' and
                  s.idCliente = 0 
                  {$condCod}
                  {$aGroupBy}
              UNION ALL
              SELECT 
                  e.nome as paciente, 
                  cb.NUMERO_TISS as cod, 
                  cb.ID as catalogo_id,
                  concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                  s.LOTE as LOTE,
                  sum(s.quantidade )as qtd ,
                  cb.valorMedio as valor,
                  s.data 
              FROM 
                  catalogo as cb  inner join 
                  saida as s on (cb.ID = s.CATALOGO_ID) inner join 
                  empresas as e on (e.id = s.UR) 
              WHERE 
                  s.tipo in(0,1,3)  and 
                  s.data BETWEEN '{$inicio}'  AND '{$fim}'
                  {$condCod}
                  {$aGroupBy} 
              ORDER BY 
                  data ASC ";
            }
        }else if($tipo == 'paciente'){
            if($resultado == 'a'){

                $aGroupBy = "group by data,s.CATALOGO_ID";
            }else{
                $aGroupBy = "group by  s.CATALOGO_ID";
            }
            $condPaciente = $codigo == 'T' ? '' : "s.idCliente ={$codigo} and";

            $sql =" SELECT 
		p.nome as paciente, 
		s.NUMERO_TISS,
         s.CATALOGO_ID,
		concat(cb.principio,' - ',cb.apresentacao) as nome, 
		s.LOTE, 
		s.quantidade as qtd,
		s.data,
		cb.valorMedio AS valor
		FROM saida as s inner join clientes as p on ( p.idClientes = s.idCliente) 
		inner join catalogo as cb on(cb.ID= s.CATALOGO_ID)
		WHERE $condPaciente s.tipo in (0,1,3) AND s.data BETWEEN '$inicio' AND '$fim'
		
		union
		SELECT
                p.nome as paciente, 
                s.numero_tiss,
                s.CATALOGO_ID,
		cb.item as nome, 
		s.LOTE, 
		s.quantidade as qtd,
		s.data ,
		cs.CUSTO AS valor
		FROM 
		saida as s inner join clientes as p on ( p.idClientes = s.idCliente) 
		inner join cobrancaplanos as cb on (cb.id = s.CATALOGO_ID)
		LEFT JOIN custo_servicos_plano_ur AS cs ON cb.id = cs.COBRANCAPLANO_ID AND cs.UR={$_SESSION['empresa_principal']} and cs.STATUS = 'A' 
		WHERE $condPaciente s.tipo = 5
		AND s.data BETWEEN '$inicio' AND '$fim'  
		ORDER BY `nome` ASC";
        }
        else{

            $condCod = $codigo == 'T' ? '' : "AND s.CATALOGO_ID= {$codigo} ";

            if($tipo == 'med'){
                $tip = 0;
            }else if($tipo == 'mat'){
                $tip = 1;
            }else{
                $tip = 3;
            }

            if( $_SESSION['empresa_principal']== 1){

                $sql="SELECT 
                        p.nome as paciente, 
                        s.NUMERO_TISS,
                        s.CATALOGO_ID,
                        concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                        s.LOTE, 
                        s.quantidade as qtd,
                        s.data,
                        cb.valorMedio AS valor
                    FROM 
                        saida as s inner join 
                        clientes as p on ( p.idClientes = s.idCliente) inner join 
                        catalogo as cb on(cb.ID = s.CATALOGO_ID)
                    WHERE  
                        s.tipo ={$tip} AND 
                        s.data BETWEEN '$inicio' AND '$fim' 
                        {$condCod}
                        AND idCliente in(
                                     select 
                                        idClientes 
                                     from 
                                        clientes 
                                     where 
                                        empresa = '{$_SESSION['empresa_principal']}'
                                    )
		
                    union all
                    SELECT 
                        p.nome as paciente, 
                        s.NUMERO_TISS,
                        s.CATALOGO_ID,
                        concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                        s.LOTE, 
                        s.quantidade as qtd,
                        s.data,
                        cb.valorMedio AS valor
                    FROM 
                        saida as s inner join 
                        empresas as p on ( p.id = s.UR) inner join 
                        catalogo as cb on (cb.ID = s.CATALOGO_ID)
                    WHERE 
                        s.tipo ={$tip} AND 
                        s.data BETWEEN '$inicio' AND '$fim' AND 
                        s.UR <> 0 AND 
                        s.idCliente = 0 
                        {$condCod}
                     order by data";

            }else{
                $sql="SELECT 
                        p.nome as paciente, 
                        s.NUMERO_TISS,
                        s.CATALOGO_ID,
                        concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                        s.LOTE, 
                        sum(s.quantidade )as qtd,
                        s.data,
                        cb.valorMedio AS valor
                    FROM 
                        saida as s inner join 
                        clientes as p on ( p.idClientes = s.idCliente) inner join 
                        catalogo as cb on(cb.ID = s.CATALOGO_ID)
                    WHERE  
                        s.tipo ={$tip} AND 
                        s.data BETWEEN '$inicio' AND '$fim'
                        {$condCod}
                        AND idCliente in(
                                            select 
                                                idClientes 
                                             from 
                                                clientes 
                                             where 
                                                empresa ='{$_SESSION['empresa_principal']}'
                                         )
                     $aGroupBy  
                     order by 
                        data";

            }

        }
        $cor= 1;
        $result = mysql_query($sql) or trigger_error(mysql_error());
        $f = true;
        $totalItens = 0;
        $totalCusto = 0;
        while($row = mysql_fetch_array($result)){
            $f = false;
            foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }

            if($cont++%2==0)
                $cor = '#E9F4F8';
            else
                $cor = '#FFFFFF';
            $html .="<tr bgcolor={$cor}>";

            $subtotal = round($row['valor'], 2) * $row['qtd'];
            $html .="<td valign='top'>{$row['paciente']}</td>";
            if($resultado == 'a'){
                $data = implode("/",array_reverse(explode("-",$row['data'])));
                $html .="<td valign='top'>{$data}</td>";
            }
            $html .="<td valign='top'>{$row['nome']}</td>";
            $html .="<td valign='top'>{$row['qtd']}</td>";
            $html .="<td valign='top'>{$row['LOTE']}</td>";
            $totalItens += $row['qtd'];
            $totalCusto += $subtotal;
            $html .= "<td valign='top'>R$ " . number_format($row['valor'], 2, ',', '.') . "</td>";
            $html .= "<td valign='top'>R$ " . number_format($subtotal, 2, ',', '.') . "</td>";
            $html .="</tr>";
        }

        $colspan = $resultado == 'a' ? 3 : 2;
        $html .= "<tr style='background-color:lightgrey'>
                <td colspan='{$colspan}' style='text-align: right;b'> <b>Total de Itens:</b></td>
                <td>$totalItens</td>
                <td>&nbsp;</td>";
        $html .= "<td style='text-align: right;b'> <b>Custo Total:</b></td>
                <td>R$ " . number_format($totalCusto, 2, ',', '.') . "</td>
              </tr>";
        $html .="</table>";

        /* echo"<button id='imprimir_saida' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
          <span class='ui-button-text'>Imprimir</span></button>";
      */
        $html .="</form>";

        $paginas []= $header.$html.= "</form>";

        /*print_r($paginas);

        die();*/
        $mpdf=new mPDF('pt','A4',10);
        $mpdf->SetHeader('página {PAGENO} de {nbpg}');
        $ano = date("Y");
        $mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
        $mpdf->WriteHTML("<html><body>");
        $flag = false;
        foreach($paginas as $pag){
            if($flag) $mpdf->WriteHTML("<formfeed>");
            $mpdf->WriteHTML($pag);
            $flag = true;
        }
        $mpdf->WriteHTML("</body></html>");
        $mpdf->Output('controlados.pdf','I');
        exit;

    }//imprimir_saida

}//Saidas

$s = new Saidas();
$s->imprimir_saida( $_GET["resultado"], $_GET["tipo"],$_GET["inicio"], $_GET["fim"], $_GET["codigo"], $_GET["ur"]);


?>