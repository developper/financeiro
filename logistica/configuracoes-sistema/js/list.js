$(function($) {
    $(".excluir-categoria").click(function () {
        var id = $(this).attr('categoria-id');

        if(confirm('Deseja realmente excluir essa categoria?')) {
            $.get(
                '/logistica/configuracoes-sistema/?action=excluir-categorias-materiais',
                {
                    id: id
                },
                function (response) {
                    if (response == '2') {
                        alert('Existem materiais associados a essa categoria! Para poder excluir, desvincule os itens.');
                    } else {
                        alert('Categoria excluída com sucesso!');
                        $("tr#" + id).remove();
                    }
                }
            );
        }
    });
});
