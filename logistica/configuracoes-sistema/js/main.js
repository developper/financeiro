(function($) {
    $(".chosen-select").chosen({search_contains: true});
    $('#ocorrencia-paciente').bootstrapToggle();
    $("#paciente-combo").hide();
    $("#nome-encaminhamento-combo").hide();
    $("#setor-encaminhamento-combo").hide();

    $("#ocorrencia-paciente").change(function () {
        $("#paciente-combo").hide();
        $("#paciente").prop('required', false);
        $("#paciente").removeClass('OBG_CHOSEN');
        if($("#ocorrencia-paciente").is(':checked')) {
            $("#paciente-combo").show();
            $("#paciente").prop('required', true);
            $("#paciente").addClass('OBG_CHOSEN');
        } else {
            $("#paciente-combo").hide();
            $("#paciente").val('');
        }
        $(".chosen-select").trigger('chosen:updated');
    });

    $("#encaminhamento").change(function () {
        $("#nome-encaminhamento-combo").hide();
        $("#setor-encaminhamento-combo").hide();
        $("#setor").removeClass('OBG_CHOSEN_MULTIPLE');
        if($(this).val() == '2') {
            $("#nome-encaminhamento-combo").show();
            $("#nome-encaminhamento").prop('required', true);
            $("#setor-encaminhamento-combo").hide();
            $("#setor").prop('required', false).val('');
        } else {
            $("#setor-encaminhamento-combo").show();

            $("#setor").prop('required', true);
            $("#setor").addClass('OBG_CHOSEN_MULTIPLE');
            $("#nome-encaminhamento-combo").hide();
            $("#nome-encaminhamento").prop('required', false).val('');
        }
    });

    $('.telefone').mask('(99) 99999-9999');

    $("#classificacao").change(function () {
        var classificacao = $(this).val();
        $("#subclassificacao").html('');
        $(".chosen-select").trigger('chosen:updated');

            $.get(
                '/ocorrencia/?action=json-subclassificacoes',
                {
                    classificacao_id: classificacao
                },
                function (response) {
                    var json = JSON.parse(response);
                    if( json.length == 0){
                        $("#subclassificacao").attr('disabled',true);
                        $(".chosen-select").trigger('chosen:updated');

                    }else {


                        $.each(json, function (index, value) {
                            $("#subclassificacao").append(
                                '<option value="' + value.id + '">' + value.descricao + '</option>'
                            );
                            $("#subclassificacao").attr('disabled', false);
                            $(".chosen-select").trigger('chosen:updated')
                        });
                    }
                }
            );



    });

    $('#salvar-ocorrencia').click(function(){
        if(!validar_campos('div-salvar-ocorrencia')){
            return false;
        }
        return true;

    });
});
