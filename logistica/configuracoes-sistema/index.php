<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
    "GET" => [
        '/logistica/configuracoes-sistema/?action=categorias-materiais' =>'\App\Controllers\ConfiguracoesSistema\CategoriaMaterial::formCategoriasMateriais',
        '/logistica/configuracoes-sistema/?action=listar-categorias-materiais' =>'\App\Controllers\ConfiguracoesSistema\CategoriaMaterial::listarCategoriasMateriais',
        '/logistica/configuracoes-sistema/?action=excluir-categorias-materiais' =>'\App\Controllers\ConfiguracoesSistema\CategoriaMaterial::excluirCategoriasMateriais',
        '/logistica/configuracoes-sistema/?action=editar-categorias-materiais' =>'\App\Controllers\ConfiguracoesSistema\CategoriaMaterial::formEditarCategoriasMateriais',
    ],
    "POST" => [
        '/logistica/configuracoes-sistema/?action=categorias-materiais' => '\App\Controllers\ConfiguracoesSistema\CategoriaMaterial::salvarCategoriasMateriais',
        '/logistica/configuracoes-sistema/?action=listar-categorias-materiais' => '\App\Controllers\ConfiguracoesSistema\CategoriaMaterial::filtrarCategoriasMateriais',
        '/logistica/configuracoes-sistema/?action=editar-categorias-materiais' => '\App\Controllers\ConfiguracoesSistema\CategoriaMaterial::atualizarCategoriasMateriais',
    ]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
    $app = new App\Application;
    $app->setRoutes($routes);
    $app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
    echo $e->getMessage();
} catch(App\NotFoundException $e) {
    http_response_code(404);
    echo $e->getMessage();
}