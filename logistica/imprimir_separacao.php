<?php
$id = session_id();
if(empty($id))
  session_start();
include_once('../validar.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../db/config.php');

class Itens{
	private $tipo = "";
	private $nome = "";
	private $apresentacao = "";
	private $via = "";
	private $aprazamento = "";
	private $obs = "";
	private $dose = "";
	private $freq = "";
	
	function Itens($t,$n,$a,$v,$apraz,$o,$d,$f){
		$this->tipo = $t;
		$this->nome = $n;
		$this->apresentacao = $a;
		$this->via = $v;
		$this->aprazamento = $apraz;
		$this->obs = $o;
		$this->dose = $d;
		$this->freq = $f;
	}
	
	public function getAprazamento($dia){
		if(array_key_exists($dia,$this->aprazamento))
			return $this->aprazamento[$dia];
		return "&nbsp;";
	}
	
	public function getLabel(){
		return "{$this->tipo}{$this->nome} {$this->apresentaca} {$this->dose} {$this->via} {$this->freq} {$this->obs}";
	}
}

function aprazamento($i,$f,$inc,$hora,$ip,$fp){

	$apraz = array();
	$inicio = new DateTime($i);
	$fim = new DateTime($f);
	$atual = new DateTime($ip);
	$fim_periodo = new DateTime($fp);
	$a = $hora;
	while($atual <= $fim_periodo){ 
		if($atual >= $inicio && $atual <= $fim)
			$apraz[$atual->format('Y-m-d')] = $a;
		else $apraz[$atual->format('Y-m-d')] = "&nbsp;";
		$atual->modify("+1 day");
	}
	return $apraz;
}

$p = $_REQUEST['p'];
$tipo = $_REQUEST['tipo'];
$presc = $_REQUEST['presc'];
$condPresc = '';
if($tipo=='2'){
	$opcao = "c.empresa = {$p}";
	$nome = "UPPER(e.nome) as nome";
	$cond = "c.empresa = e.id";
}else{
	$opcao = "s.paciente = {$p}";
	$nome = "UPPER(c.nome) as nome";
	$cond = "c.empresa = e.id";
    if($presc != '-1') {
        $condPresc = " AND (s.idPrescricao = '{$presc}' OR s.id_prescricao_curativo = '{$presc}') ";
    }
}

ob_start();
$sql = "SELECT
s.enfermeiro,
u.nome as profissional,
s.tipo,
s.qtd,
p.inicio,
p.fim,
{$nome},
c.cuidador,
c.relcuid,
s.autorizado,
s.enviado,
DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA
FROM
solicitacoes AS s LEFT JOIN 
prescricoes AS p ON (s.`idPrescricao` = p.`id`) INNER JOIN
clientes AS c ON (c.idclientes = s.paciente) INNER JOIN
empresas as e ON ($cond) INNER JOIN
usuarios AS u ON (s.enfermeiro = u.idUsuarios)
WHERE
{$opcao}
{$condPresc}";






$result = mysql_query($sql);

$row = mysql_fetch_array($result);
$ip = $row['inicio'];
$fp = $row['fim'];
$img = "<img src='../utils/logo.jpg' width='30%' style='align:center' />";
$hoje = date("d/m/Y");
$header = "<table border='1' width='100%' style='border:1px solid #000;border-collapse:collapse;'><thead><tr>
<th width='40%' rowspan='3' colspan='3' >{$img}<br/>".utf8_decode($_SESSION['nome_empresa'])."</th>
<th width=20% colspan='5'>Prescri&ccedil;&atilde;o ". join("/",array_reverse(explode("-",$row['inicio'])))." a ". join("/",array_reverse(explode("-",$row['fim'])))." - ".ucwords(strtolower(utf8_decode($row['nome'])))."</th>";
$header .= "<th rowspan='3' colspan='2' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th></tr></tr>";
//	$header .= "<tr><td colspan='7'><b>Nome: </b>".ucwords(strtolower($row['paciente']))."</td></tr>";
if($tipo!='2')
	$header .= "<tr><td colspan='5'><b>Cuidador: </b>".ucwords(strtolower($row['cuidador']))." (".strtolower($row['relcuid']).")</td></tr>";
$tipo = "Enfermeira";
if($tipo!='2')
	$header .= "<tr><td colspan='5' ><b>Profissional: </b>".ucwords(strtolower(utf8_decode($row['profissional'])))." ({$tipo})</td></tr></table><br/>";

echo $header;
echo "<p><b>Data Impress&atilde;o: ".$hoje."</b></p>";
echo "<div id='cancelar-item' title='Cancelar item' sol='' >";
echo "<table  width=100% id='lista-enviados' style='border:1px solid #000;bor' ><thead><tr><th>Itens Solicitados</th><th align='center' width='1%'>enviado/pedido</th><th width='1%'>Enviar</th></tr></thead>";

/*$sqlmed = "SELECT s.enfermeiro,(CASE s.status when 2 THEN s.qtd ELSE s.autorizado END) as autorizado, s.enviado, s.cod, s.idSolicitacoes as sol, s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data, b.principio, b.apresentacao
FROM solicitacoes as s, brasindice as b,clientes as c
WHERE {$opcao} AND s.paciente = c.idClientes AND s.tipo = '0' AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND (s.status = '1' OR s.status = '2')
AND b.numero_tiss = s.cod ORDER BY principio, apresentacao,enviado";*/
$sqlmed ="SELECT
	s.TIPO_SOLICITACAO,
	s.enfermeiro,
	(CASE WHEN COUNT(*)>1 THEN SUM(s.enviado) ELSE s.enviado END) AS enviados,
    sum((CASE s.status when 2 THEN  s.qtd ELSE  s.autorizado  END)) as autorizado,
	
	(CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia ,
   
	s.NUMERO_TISS, 
	s.CATALOGO_ID,
	s.idSolicitacoes as sol,
	s.status, 
	DATE_FORMAT(s.data,'%d/%m/%Y') as data, 
	b.principio, 
	b.apresentacao, 
	b.lab_desc,
  	(CASE  when s.status= 1 and s.idPrescricao=-1 THEN '1' ELSE '0' END) as avulsa,
	DATE_FORMAT(s.DATA_MAX_ENTREGA,'%d/%m/%Y %H:%m:%s') as data_entrega,
	e.principio AS principioAtivo
  	FROM solicitacoes as s
  	INNER JOIN catalogo as b ON b.ID = s.CATALOGO_ID
	INNER JOIN clientes as c ON s.paciente = c.idClientes
	LEFT JOIN catalogo_principio_ativo AS d ON b.ID = d.catalogo_id
	LEFT JOIN principio_ativo AS e ON d.principio_ativo_id = e.id
  	WHERE {$opcao} 
	AND s.tipo = '0' 
	AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) 
	AND (s.status = '1' OR s.status = '2')
	{$condPresc}
	GROUP BY s.CATALOGO_ID 
	ORDER BY 
	principio, 
	apresentacao,
	enviado";

$rmed = mysql_query($sqlmed);
while($row = mysql_fetch_array($rmed)){
	foreach($row AS $key => $value) {
		$row[$key] = stripslashes($value);
	}
	$dados = "sol='{$row['sol']}' cod='{$row['cod']}' n='{$row['principio']}' sn='{$row['apresentacao']}' soldata='{$row['data']}' env='{$row['enviados']}' total='{$row['qtd']}' tipo='0'";
    $laboratorio = htmlentities($row['lab_desc']);
    $principio = htmlentities($row['principio']);
    $apresentacao = htmlentities($row['apresentacao']);
    $principioAtivo = htmlentities($row['principioAtivo']);
	echo "<tr style='border:1px solid #000;'>
	 			<td style='border:1px solid #000;'>
                    <b>MEDICAMENTO: </b><b>(LAB: {$laboratorio})</b> - {$principio} {$apresentacao} 
                    <br> <b>PRINC&Iacute;PIO ATIVO: {$principioAtivo} </b>
	 			</td>
				<td align='center' style='border:1px solid #000;'>{$row['enviados']}/{$row['autorizado']}</td><td style='border:1px solid #000;width:20px;' class='dados' $dados >$envio</td></tr>";
	echo "</tr>";
	echo "<tr><td style='border:1px solid #000;height:20px;' align='center'></td><td style='border:1px solid #000;' align='center'></td><td style='border:1px solid #000;' align='center'></td></tr>";
}
/*$sqlmat = "SELECT s.enfermeiro, (CASE s.status when 2 THEN s.qtd ELSE s.autorizado END) as autorizado, s.enviado, s.cod, s.idSolicitacoes as sol, s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data, b.principio, b.apresentacao
FROM solicitacoes as s, brasindice as b,clientes as c
WHERE {$opcao} AND s.paciente = c.idClientes AND s.tipo = '1' AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND (s.status = '1' OR s.status = '2')
AND b.numero_tiss = s.cod ORDER BY principio, apresentacao, enviado";
*/
$sqlmat = "SELECT 
	s.enfermeiro,
	s.TIPO_SOLICITACAO,  
	(CASE WHEN COUNT(*)>1 THEN SUM(s.enviado) ELSE s.enviado END) AS enviados,
    sum((CASE s.status when 2 THEN  s.qtd ELSE  s.autorizado  END)) as autorizado,
	(CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia ,
	(CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia, 
	
	s.NUMERO_TISS, 
	s.CATALOGO_ID,
	s.idSolicitacoes as sol, 
	s.status, 
	DATE_FORMAT(s.data,'%d/%m/%Y') as data,
	b.principio, 
	b.apresentacao ,
	 b.lab_desc,
	(CASE  when s.status= 1 and s.idPrescricao =-1 THEN '1' ELSE '0' END) as avulsa,
	DATE_FORMAT(s.DATA_MAX_ENTREGA,'%d/%m/%Y %H:%m:%s') as data_entrega		   
  	FROM solicitacoes as s,
	catalogo as b,
	clientes as c 
  	WHERE {$opcao}  AND
	s.paciente = c.idClientes 
	AND s.tipo = '1' 
	AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) 
	AND (s.status = '1' OR s.status = '2') 
  	AND b.ID = s.CATALOGO_ID 
  	{$condPresc}
	GROUP BY s.CATALOGO_ID 
	ORDER BY principio, apresentacao, enviado";
$rmat = mysql_query($sqlmat);
while($row = mysql_fetch_array($rmat)){
	foreach($row AS $key => $value) {
	$row[$key] = stripslashes($value);
}
$dados = "sol='{$row['sol']}' cod='{$row['cod']}' n='{$row['principio']}' sn='{$row['apresentacao']}' soldata='{$row['data']}' env='{$row['enviados']}' total='{$row['qtd']}' tipo='1'";

    $laboratorio = htmlentities($row['lab_desc']);
    $principio = htmlentities($row['principio']);
    $apresentacao = htmlentities($row['apresentacao']);
echo "<tr style='border:1px solid #000;'>
			<td style='border:1px solid #000;'><b>MATERIAL: </b><b>(LAB: {$laboratorio})</b> - {$principio} {$apresentacao}</td>
      		<td align='center' style='border:1px solid #000;'>{$row['enviados']}/{$row['autorizado']}</td><td style='border:1px solid #000;width:20px;' class='dados' $dados >$envio</td>";
echo "</tr>";
echo "<tr><td style='border:1px solid #000;height:20px;' align='center'></td><td style='border:1px solid #000;' align='center'></td><td style='border:1px solid #000;' align='center'></td></tr>";
}
$sqlformula = "SELECT s.enfermeiro, (CASE s.status when 2 THEN s.qtd ELSE s.autorizado END) as autorizado, s.enviado, s.cod, s.idSolicitacoes as sol, s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data, f.formula
FROM solicitacoes as s, formulas as f,clientes as c
WHERE {$opcao} AND s.paciente = c.idClientes AND s.tipo = '12' AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND (s.status = '1' OR s.status = '2')
AND f.id = s.cod ORDER BY formula,enviado";
$rformula = mysql_query($sqlformula);
while($row = mysql_fetch_array($rformula)){
	foreach($row AS $key => $value) {
		$row[$key] = stripslashes($value);
	}
	$dados = "sol='{$row['sol']}' cod='{$row['cod']}' n='{$row['formula']}' sn='' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['qtd']}' tipo='12'";

	echo "<tr>
				<td style='border:1px solid #000;'>{$row['formula']} </td>
				<td style='border:1px solid #000;' align='center'>{$row['enviado']}/{$row['autorizado']}</td><td style='border:1px solid #000;width:20px;' class='dados' $dados ></td>";
	echo "</tr>";
	echo "<tr><td style='border:1px solid #000;height:20px;' align='center'></td><td style='border:1px solid #000;' align='center'></td><td style='border:1px solid #000;' align='center'></td></tr>";
}

//Eriton 29-05-2013

/*$sqleqp = "SELECT s.enfermeiro,(CASE WHEN COUNT(*)>1 THEN SUM(s.enviado) ELSE s.enviado END) AS enviados,sum((CASE s.status when 2 THEN  s.qtd ELSE  s.autorizado  END)) as autorizado, s.enviado, s.cod, s.idSolicitacoes as sol,
s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data, cb.item,
(SELECT

(CASE WHEN COUNT( * ) >1 THEN SUM( i.QTD ) ELSE i.QTD END) - (CASE WHEN COUNT( * ) >1 THEN sum( i.ENVIADO ) ELSE i.ENVIADO END) AS
dif_pedido

FROM
itenspedidosinterno AS i INNER JOIN
pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
WHERE (
i.STATUS = 'A'	OR
(i.QTD - i.ENVIADO) >0)	AND
i.NUMERO_TISS = s.cod AND

i.TIPO = s.tipo AND
i.NUMERO_TISS = s.cod AND
p.EMPRESA = {$p}
GROUP BY
i.NUMERO_TISS) as dif_pedido, s.TIPO_SOL_EQUIPAMENTO
FROM solicitacoes as s, cobrancaplanos as cb, clientes as c
WHERE {$opcao} AND s.paciente = c.idClientes AND s.tipo = '5'
AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND (s.status = '1' OR s.status = '2')
		AND cb.id = s.cod GROUP BY cod ORDER BY TIPO_SOL_EQUIPAMENTO,item,enviado";
*/

	/*$sqldie = "
			SELECT
				s.enfermeiro,(CASE s.status when 2 THEN s.qtd ELSE s.autorizado END) as autorizado,
				s.enviado,
				s.cod,
				s.idSolicitacoes as sol,
				s.status,
				DATE_FORMAT(s.data,'%d/%m/%Y') as data,
				b.principio,
				b.apresentacao
			FROM
	 			solicitacoes as s,
	 			brasindice as b,
	 			clientes as c
			WHERE {$opcao} AND s.paciente = c.idClientes AND s.tipo = '3'
				AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND (s.status = '1' OR s.status = '2')
				AND b.numero_tiss = s.cod ORDER BY principio, apresentacao,enviado";
				*/
	$sqldie = "SELECT 
	s.enfermeiro,
	s.TIPO_SOLICITACAO, 
	(CASE WHEN COUNT(*)>1 THEN SUM(s.enviado) ELSE s.enviado END) AS enviados,
    sum((CASE s.status when 2 THEN  s.qtd ELSE  s.autorizado  END)) as autorizado,
	(CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia , 
	 
	s.NUMERO_TISS, 
	s.CATALOGO_ID,
	s.idSolicitacoes as sol, 
  	s.status,
	DATE_FORMAT(s.data,'%d/%m/%Y') as data,
	b.principio, b.apresentacao	, b.lab_desc,
	(CASE  when s.status= 1 and s.idPrescricao =-1 THEN '1' ELSE '0' END) as avulsa,
	DATE_FORMAT(s.DATA_MAX_ENTREGA,'%d/%m/%Y %H:%m:%s') as data_entrega
  	FROM 
	solicitacoes as s,
	catalogo as b,
	clientes as c
  	WHERE {$opcao} 
	AND s.paciente = c.idClientes 
	AND s.tipo = '3' 
  	AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END)
	AND (s.status = '1' OR s.status = '2')
  	AND b.ID = s.CATALOGO_ID 
  	{$condPresc}
	GROUP BY s.CATALOGO_ID
	ORDER BY principio, apresentacao,enviado";

	$rdie = mysql_query($sqldie);
	while($row = mysql_fetch_array($rdie)){
		foreach($row AS $key => $value) {
			$row[$key] = stripslashes($value);
		}
		$dados = "sol='{$row['sol']}' cod='{$row['cod']}' n='{$row['principio']}' sn='{$row['apresentacao']}' soldata='{$row['data']}' env='{$row['enviados']}' total='{$row['qtd']}' tipo='3'";
	    $laboratorio = htmlentities($row['lab_desc']);
	    $principio = htmlentities($row['principio']);
	    $apresentacao = htmlentities($row['apresentacao']);
		echo "<tr style='border:1px solid #000;'>
		<td style='border:1px solid #000;'><b>DIETA: </b><b>(LAB: {$laboratorio})</b> - {$principio} {$apresentacao}</td>
		<td align='center' style='border:1px solid #000;'>{$row['enviados']}/{$row['autorizado']}</td><td style='border:1px solid #000;width:20px;' class='dados' $dados >$envio</td></tr>";
		echo "</tr>";
	echo "<tr><td style='border:1px solid #000;height:20px;' align='center'></td><td style='border:1px solid #000;' align='center'></td><td style='border:1px solid #000;' align='center'></td></tr>";
}
	
	//FIm Eriton 29-05-2013

	$sqleqp = "SELECT 
	s.enfermeiro,
	s.TIPO_SOLICITACAO,
	(CASE WHEN COUNT(*)>1 THEN SUM(s.enviado) ELSE s.enviado END) AS enviados,
    sum((CASE s.status when 2 THEN  s.qtd ELSE  s.autorizado  END)) as autorizado,
	(CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia ,
	
	s.NUMERO_TISS, 
	s.CATALOGO_ID,
	s.idSolicitacoes as sol, 
  	s.status, 
	DATE_FORMAT(s.data,'%d/%m/%Y') as data,
	cb.item,
	s.TIPO_SOL_EQUIPAMENTO ,
	(CASE  when s.status= 1 and s.idPrescricao =-1 THEN '1' ELSE '0' END) as avulsa,
	DATE_FORMAT(s.DATA_MAX_ENTREGA,'%d/%m/%Y %H:%m:%s') as data_entrega
  	FROM 
	solicitacoes as s,
	cobrancaplanos as cb, 
	clientes as c
  	WHERE {$opcao}  
	AND s.paciente = c.idClientes AND s.tipo = '5' 
  	AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) 
	AND (s.status = '1' OR s.status = '2')
	
  	AND cb.id = s.CATALOGO_ID 
  	{$condPresc}
	GROUP BY s.CATALOGO_ID
	ORDER BY TIPO_SOL_EQUIPAMENTO,item,enviado";
  	


			
	$reqp = mysql_query($sqleqp);
	while($row = mysql_fetch_array($reqp)){
	foreach($row AS $key => $value) {
	$row[$key] = stripslashes($value);
	}

if($row['TIPO_SOL_EQUIPAMENTO'] == 1){
	$tipo_sol_eqp ="Enviar ";
	//$trocar = "<img align='right' sol='{$row['sol']}' tipo='eqp' class='troca-item' src='../utils/exchange_16x16.png' title='Trocar' border='0'>";
	if ($row['dif_pedido'] == null || $row['dif_pedido'] == NULL){
	$dif = 0;
	 
	}else{
	$dif= $row['dif_pedido'];
	}
		$pedidos = ($row['autorizado'] - $row['enviados']) -$dif;
		$dados = "autorizado='{$row['autorizado']}' sol='{$row['sol']}' cod='{$row['cod']}' n='{$row['item']}' sn='{$tipo_sol_eqp}' soldata='{$row['data']}' env='{$row['enviados']}' total='{$row['qtd']}' tipo='5' tipo_sol_equipamento='{$row['TIPO_SOL_EQUIPAMENTO']}'";
		//$cancela = "<img align='right' sol='{$row['sol']}' class='cancela-item' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";

	echo "<tr style='border:1px solid #000;'>
			<td style='border:1px solid #000;'><b>EQUIPAMENTO: </b>{$tipo_sol_eqp}{$row['item']}  {$cancela} {$trocar}</td>
      		<td align='center' style='border:1px solid #000;'>{$row['enviados']}/{$row['autorizado']}</td><td style='border:1px solid #000;width:20px;' class='dados' $dados >$envio</td>";
	echo "</tr>";
	echo "<tr><td style='border:1px solid #000;height:20px;' align='center'></td><td style='border:1px solid #000;' align='center'></td><td style='border:1px solid #000;' align='center'></td></tr>";

	  		}
	}
	
echo "</table></div>";



$paginas[] = ob_get_clean();
$mpdf=new mPDF('pt','A4',9);
$mpdf->SetHeader('página {PAGENO} de {nbpg}');
$mpdf->allow_charset_conversion=true;
$mpdf->charset_in='iso-8859-1';

$ano = date("Y");
$mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
$mpdf->WriteHTML("<html><body>");
$flag = false;
foreach($paginas as $pag){
	if($flag) $mpdf->WriteHTML("<formfeed>");
	$mpdf->WriteHTML($pag);
	$flag = true;
}
$mpdf->WriteHTML("</body></html>");
$mpdf->Output('prescricao.pdf','I');
exit;
?>