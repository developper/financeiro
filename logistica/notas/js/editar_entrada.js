$(function($) {
    $(".fornecedor").chosen({search_contains: true});

    $(".busca-item-editar-entrada").autocomplete({
        source: function(request, response) {
            $.getJSON('/logistica/busca_brasindice_fatura.php', {
                term: request.term
            }, response);
        },
        minLength: 3,
        select: function( event, ui ) {
            $(this).parent().find(".item").val(ui.item.catalogo_id);
            $(this).val(ui.item.value);
        }
    });

    $("#reset").click( function () {
        $("input[type='date'], .fornecedor, .busca-item-editar-entrada, .item").val('');
        $(".fornecedor").trigger("chosen:updated");
    });

    $(".valor").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
});
