var total,
    total_nota,
    outras_despesas;

$(function($) {
    total = parseFloat($("#total-produtos").val());
    outras_despesas = parseFloat($("#outras-despesas").val());
    total_nota = total + outras_despesas;
    $("#div-xml-nota").dialog({
        autoOpen: false, modal: true, width: 600, position: 'top',
        buttons: {
            "Fechar" : function(){
                $(this).dialog("close");
            }
        }
    });

    $("#xml_nota").click(function(){
        $("#div-xml-nota").dialog("open");
    });


    $(".fornecedor").chosen({search_contains: true});

    $(".valor, #desconto, #valor-unitario, #outras-despesas").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $("#busca-entrada-equipamento").click(function(){
        $("#tipo-busca").attr('tipo','');
        if($(this).is(':checked')){
            $("#tipo-busca").attr('tipo','eqp');
        }
    });

    $("#outras-despesas").blur(function(){
        var aux = typeof $(this).val() != 'number' ? parseFloat($(this).val().replace(",", ".")) : $(this).val();
        if(outras_despesas != aux){
            outras_despesas = aux;
            total_nota = parseFloat(outras_despesas) + parseFloat(total);
            $("#total-nota-label").html('R$ '+ total_nota.toFixed(2).replace('.', ','));
        }
    });

    $("#busca-item").autocomplete({
        source: function(request, response) {
            $.getJSON('/logistica/busca_brasindice_fatura.php', {
                term: request.term,
                tipo: $("#tipo-busca").attr("tipo")
            }, response);},
        minLength: 3,
        select: function( event, ui ) {
            $(this).attr('item_id', ui.item.catalogo_id);
            $(this).attr('tipo', ui.item.tipo);
            $(this).attr('item_name', ui.item.value);
            $("#qtd").focus();
        }
    });

    $(".busca-item-xml").autocomplete({
        source: function(request, response) {
            $.getJSON('/logistica/busca_brasindice_fatura.php', {
                term: request.term,
            }, response);},
        minLength: 3,
        select: function( event, ui ) {
            var tr = $(this).parent().parent();
            var controle = tr.find('.controle').val();
            tr.find('.tipo_produto').attr('name', 'tipo[' + ui.item.catalogo_id + '-' + controle + ']').val(ui.item.tipo);
            tr.find('.ean_produto').attr('name', 'ean[' + ui.item.catalogo_id + '-' + controle + ']');
            tr.find('.cprod_produto').attr('name', 'cprod[' + ui.item.catalogo_id + '-' + controle + ']');
            tr.find('.lote_produto').attr('name', 'lote[' + ui.item.catalogo_id + '-' + controle + ']');
            tr.find('.qtd_produto').attr('name', 'qtd[' + ui.item.catalogo_id + '-' + controle + ']');
            tr.find('.vl_unitario_produto').attr('name', 'vl_unitario[' + ui.item.catalogo_id + '-' + controle + ']');
            tr.find('.validade_produto').attr('name', 'validade[' + ui.item.catalogo_id + '-' + controle + ']');
            tr.find('.tipo_aquisicao_produto').attr('name', 'tipo_aquisicao[' + ui.item.catalogo_id + '-' + controle + ']');
            tr.find('.cfop_produto').attr('name', 'cfop[' + ui.item.catalogo_id + '-' + controle + ']');
            tr.find('.desconto_produto').attr('name', 'desconto[' + ui.item.catalogo_id + '-' + controle + ']');
            tr.find('.referencia_produto').attr('name', 'referencia[' + ui.item.catalogo_id + '-' + controle + ']');
        }
    });

    $("#incluir-produto").live('click', function(){
        var busca_item          = $("#busca-item"),
            item_id             = busca_item.attr('item_id'),
            item_name           = busca_item.attr('item_name'),
            tipo                = busca_item.attr('tipo'),
            qtd                 = parseInt($("#qtd").val()),
            lote                = $("#lote").val(),
            vl_unitario_text    = $("#valor-unitario").val(),
            vl_unitario         = parseFloat($("#valor-unitario").val().replace('.','').replace(',','.')),
            validade            = $("#validade").val(),
            tipo_aquisicao      = $("#tipo-aquisicao option:selected").val(),
            tipo_aquisicao_text = $("#tipo-aquisicao option:selected").text(),
            cfop                = $("#cfop").val(),
            desconto_text       = $("#desconto").val(),
            desconto            = parseFloat($("#desconto").val().replace('.','').replace(',','.')),
            referencia          = $("#referencia").val(),
            subtotal            = 0,
            vl_aux              = 0;

        var valor_desconto = vl_unitario - (vl_unitario * (desconto / 100));
        vl_aux         = (desconto == 0 ? vl_unitario : valor_desconto) / qtd;
        subtotal       = parseFloat(valor_desconto);
        subtotal       = parseFloat(subtotal);
        subtotal       = subtotal.toFixed(2);
        subtotal       = parseFloat(subtotal);
        total          = total + subtotal;
        total_nota     = total_nota + subtotal;

        if(tipo == '5' && referencia == '') {
            alert('Para equipamentos, é necessário preencher o Número de Patrimônio!');
            return false;
        }

        if(item_name != '' || qtd < 1 || vl_unitario <= 0) {
            $('.produtos').find('tbody').append(
                '<tr>' +
                '<td>' +
                '<img subtotal="' + subtotal + '" class="del_produto" src="../../../utils/delete_16x16.png" title="Remover" border="0"> ' +
                item_name +
                '<input type="hidden" name="ean[' + item_id + '-' + lote + ']" class="ean_produto" value="">' +
                '<input type="hidden" name="cprod[' + item_id + '-' + lote + ']" class="cprod_produto" value="">' +
                '<input type="hidden" name="tipo[' + item_id + '-' + lote + ']" class="tipo_produto" value="' + tipo + '">' +
                '</td>' +
                '<td>' +
                lote +
                '<input type="hidden" name="lote[' + item_id + '-' + lote + ']" class="lote_produto" value="' + lote + '">' +
                '</td>' +
                '<td>' +
                qtd +
                '<input type="hidden" name="qtd[' + item_id + '-' + lote + ']" class="qtd_produto" size="4" maxlenght="11" value="' + qtd + '">' +
                '</td>' +
                '<td>' +
                vl_aux.toFixed(2).replace('.', ',') +
                '<input type="hidden" name="vl_unitario[' + item_id + '-' + lote + ']" class="vl_unitario_produto" value="' + vl_aux.toFixed(2) + '">' +
                '</td>' +
                '<td>' +
                validade.split('-').reverse().join('/') +
                '<input type="hidden" name="validade[' + item_id + '-' + lote + ']" class="validade_produto" value="' + validade + '">' +
                '</td>' +
                '<td>' +
                tipo_aquisicao_text +
                '<input type="hidden" name="tipo_aquisicao[' + item_id + '-' + lote + ']" class="tipo_aquisicao_produto" value="' + tipo_aquisicao + '">' +
                '</td>' +
                '<td>' +
                cfop +
                '<input type="hidden" name="cfop[' + item_id + '-' + lote + ']" class="cfop_produto" value="' + cfop + '">' +
                '</td>' +
                '<td>' +
                desconto_text + ' %' +
                '<input type="hidden" name="desconto[' + item_id + '-' + lote + ']" class="desconto_produto" value="' + desconto + '">' +
                '</td>' +
                '<td>' +
                referencia +
                '<input type="hidden" name="referencia[' + item_id + '-' + lote + ']" class="referencia_produto" value="' + referencia + '">' +
                '</td>' +
                '<td>' +
                subtotal.toFixed(2).toString().replace('.', ',') +
                '</td>' +
                '</tr>'
            );
        } else {
            alert('Por favor, preencha todos os campos corretamente!');
            return false;
        }

        total       = parseFloat(total);
        total_nota  = parseFloat(total_nota);

        $("#total-nota-label").html('R$ '+ total_nota.toFixed(2).replace('.', ','));
        $("#total-produtos-label").html('R$ '+ total.toFixed(2).replace('.', ','));
        $("#total-produtos").val(total.toFixed(2));
        busca_item.attr('item_id', '');
        busca_item.attr('item_name', '');
        busca_item.val('');
        busca_item.attr('tipo', '');
        $("#qtd").val('0');
        $("#lote").val('');
        $("#valor-unitario").val('0');
        $("#validade").val('');
        $("#tipo-aquisicao").val('');
        $("#cfop").val('');
        $("#desconto").val('0');
        $("#referencia").val('');
    });

    $("#add_parcela").live('click', function(){
        var valor = $(".parcelas").find(".tr_parcela:last").val(),
            par = parseInt(valor) + 1;

        $(".parcelas").append(
            '<tr class="parcela" align="center">' +
            '<td>' +
            '<img align="left" class="del_parcela" src="/utils/delete_16x16.png" title="Remover" border="0" />' +
            '<input type="number" name="tr_parcela[]" class="tr_parcela" value="' + par + '" size="5" />' +
            '</td>' +
            '<td><input name="vencimento[]" value="" type="date" maxlength="10" size="10" /></td>' +
            '<td><input type="text" maxlength="128" size="20" name="codigo_barras[]" /></td>' +
            '<td><input name="valor_parcela[]" class="valor OBG valor_parcela" value="0,00" type="text" size="10" style="text-align:right" /></td>' +
            '</tr>'
        );

        $(".valor").priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
    });

    $(".del_produto").live('click', function () {
        total -= parseFloat($(this).attr('subtotal'));
        total_nota -= parseFloat($(this).attr('subtotal'));
        $("#total-nota-label").html('R$ '+ total_nota.toFixed(2).replace('.', ','));
        $("#total-produtos-label").html('R$ '+ total.toFixed(2).replace('.', ','));
        $("#total-produtos").val(total.toFixed(2));
        $(this).parent().parent().remove();
    });

    $(".del_parcela").live('click', function () {
        if($(".parcelas tr").length > 1) {
            $(this).parent().parent().remove();
        }
    });

    $(".qtd_produto").live('blur', function () {
        var $this = $(this);
        var subtotalProd = $this.parent().parent().find('.subtotal').text();
        var qtdProd = parseFloat($this.val());
        subtotalProd = parseFloat(subtotalProd.replace('.', '').replace(',', '.'));
        var vlunitario = subtotalProd / qtdProd;
        console.log(subtotalProd, vlunitario);
        $(this).parent().parent().find('.vl_unitario_produto').val(vlunitario.toFixed(2));
        vlunitario = vlunitario.toFixed(2).replace('.', ',');
        $(this).parent().parent().find('.vlunitario').html(vlunitario);
    });

    $(".vencimento, .emissao").live('change', function () {
        var emissao = new Date($(".emissao").val()), isValid = true;

        $(".vencimento").each(function () {
            var $this = $(this);
            var vencimento = new Date($this.val());

            if(vencimento < emissao) {
                isValid = !isValid;
                $this.val("");
                return false;
            }
        });

        if(!isValid){
            alert('Um ou mais vencimentos de parcelas são menores que a data de emissão da nota fiscal!');
        }
    });
});

function validar_form(){
    var valor = 0;

    if($(".fornecedor").val() == ''){
        alert("Escolha um fornecedor!");
        return false;
    }

    if($(".lote_produto").length == 0){
        alert('É necessário ao mínimo 1 produto para salvar essa entrada!');
        return false;
    }
    $(".valor_parcela").each(function(){
        valor += parseFloat($(this).val().replace('.','').replace(',','.'));
    });

    total_nota = total_nota.toFixed(2);
    valor = parseFloat(valor);
    valor = valor.toFixed(2);
    if(valor < total_nota || valor > total_nota){
        alert("A soma da(s) parcela(s) não condiz ao valor total.");
        return false;
    }
    return true;
}