<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';


use \App\Controllers\Logistica;

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
	"GET" => [
		'/logistica/notas/?action=menu' => '\App\Controllers\Logistica::menu',
		'/logistica/notas/?action=entrada-produtos' => '\App\Controllers\Logistica::formEntradaProdutos',
		'/logistica/notas/?action=editar-entrada-produtos' => '\App\Controllers\Logistica::formEditarEntradaProdutos',
	],
	"POST" => [
		'/logistica/notas/?action=salvar-entrada-produtos' => '\App\Controllers\Logistica::salvarEntradaProdutos',
        '/logistica/notas/?action=entrada-produtos' => '\App\Controllers\Logistica::importarXMLEntradaProdutos',
        '/logistica/notas/?action=editar-entrada-produtos' => '\App\Controllers\Logistica::buscarEntradaProdutos',
        '/logistica/notas/?action=atualizar-entrada-produtos' => '\App\Controllers\Logistica::atualizarEntradaProdutos',
    ]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}