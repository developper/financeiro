$(function($){

    $('.fracionar').live("click", function () {
        var $this = $(this), $tr = $this.parent().parent(),
            indice = $tr.index()+1, bgcolor = $tr.attr('bgcolor') || '';
        var background = 'bgcolor="' + bgcolor + '"';

        var catalogo = $this.attr('catalogo');
        var aux = parseInt($('#aux').val())+1;

        var string = "<tr "+background+" class='item' aux="+aux+">"+
                                "<td>"+
                                     "<img src='/utils/delete_24x24.png' width='16' style='cursor: pointer;' title='Remover' border='0' class='remover'>" +
                                    "<input type='hidden' name = 'item["+aux+"][catalogoId]' value = '"+catalogo+"'>"+
                                "</td>"+
                                "<td></td>"+
                                "<td>"+
                                "<input type='number'  name = 'item["+aux+"][quantidade]' value = ''>"+
                                    "</td>"+
                                   " <td>"+
                                   " <input type='text'   name = 'item["+aux+"][lote]' value = ''>"+
                                   " </td>"+
                                   " <td>"+
                                   " <input type='date'  name = 'item["+aux+"][validade]' value = ''>"+
                                   " </td>"+

                   " </tr>";
        $(".mytable tr").eq(indice).after(
            string
        );

        $('#aux').val(aux);

    });

    $('.remover').live('click',function(){
        $(this).parent().parent().remove();
    });

    $('#salvar').click(function(){
        var i = 0;
        var aux2 = 0;
        var dados = $('#div-item').find('form').serialize();

        $('.item').each(function(){
            var $this = $(this);
            var aux = $this.attr('aux');
            var quantidade = $this.find("input[name = 'item["+aux+"][quantidade]']").val();
            var catalogo = $this.find("input[name = 'item["+aux+"][catalogoId]']").val();
            var validade = $this.find("input[name = 'item["+aux+"][validade]']").val();
            var lote = $this.find("input[name = 'item["+aux+"][lote]']").val();
            $this.find("input[name = 'item["+aux+"][quantidade]']").css({"border":""});
            $this.find("input[name = 'item["+aux+"][validade]']").css({"border":""});
            $this.find("input[name = 'item["+aux+"][lote]']").css({"border":""});

            if(quantidade != '' || validade != '' || lote != '' ){
                if(quantidade == ''){
                    $this.find("input[name = 'item["+aux+"][quantidade]']").css({"border":"3px solid red"});
                    i++;
                }
                if(validade == ''){
                    $this.find("input[name = 'item["+aux+"][validade]']").css({"border":"3px solid red"});
                    i++;
                }
                if(lote == ''){
                    $this.find("input[name = 'item["+aux+"][lote]']").css({"border":"3px solid red"});
                    i++;
                }
                aux2++;
            }
        });
        if(aux2 == 0){
            alert('Nenhum item foi inserido');
            return false;
        }
        if(i>0){
            alert('Campos em vermelho são obrigatórios.');
            return false;
        }
        $.post( '/logistica/FerramentaAjusteEstoque/?action=salvar',
            dados,

            function(r){
                $('#div-item').find('form input').val('');
                alert(r)
                return  false;


            });

    });












});
