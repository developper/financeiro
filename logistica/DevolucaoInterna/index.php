<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

//ini_set('display_errors',1);
define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);


$routes = [
    "GET" => [
        '/logistica/DevolucaoInterna/?action=formSolicitarDevolucaoInterna' => '\App\Controllers\DevolucaoInterna::formSolicitarDevolucaoInterna',
        '/logistica/DevolucaoInterna/?action=pesquisarSolicitacaoDevolucaoInterna' => '\App\Controllers\DevolucaoInterna::pesquisarSolicitacaoDevolucaoInterna',
        '/logistica/DevolucaoInterna/?action=visualizarSolicitacaoDevolucaoInterna' => '\App\Controllers\DevolucaoInterna::visualizarSolicitacaoDevolucaoInterna',
        '/logistica/DevolucaoInterna/?action=autorizarSolicitacaoDevolucaoInterna' => '\App\Controllers\DevolucaoInterna::autorizarSolicitacaoDevolucaoInterna',
        '/logistica/DevolucaoInterna/?action=enviarItensSolicitacaoDevolucaoInterna' => '\App\Controllers\DevolucaoInterna::enviarItensSolicitacaoDevolucaoInterna',
        "/logistica/DevolucaoInterna/?action=confirmarEntregaItensSolicitacaoDevolucaoInterna" => '\App\Controllers\DevolucaoInterna::confirmarEntregaItensSolicitacaoDevolucaoInterna',

    ],
    "POST" => [
        '/logistica/DevolucaoInterna/?action=salvarDevolucaoInterna' =>'\App\Controllers\DevolucaoInterna::salvarDevolucaoInterna',
        '/logistica/DevolucaoInterna/?action=pesquisarSolicitacaoDevolucaoInterna' => '\App\Controllers\DevolucaoInterna::pesquisarSolicitacaoDevolucaoInterna',
        '/logistica/DevolucaoInterna/?action=salvarAutorizacaoDevolucaoInterna' => '\App\Controllers\DevolucaoInterna::salvarAutorizacaoDevolucaoInterna',
        '/logistica/DevolucaoInterna/?action=salvarEnvioItensDevolucaoInterna'  => '\App\Controllers\DevolucaoInterna::salvarEnvioItensDevolucaoInterna',
        "/logistica/DevolucaoInterna/?action=salvarConfirmarcaoEntregaItensDevolucaoInterna" => '\App\Controllers\DevolucaoInterna::salvarConfirmarcaoEntregaItensDevolucaoInterna',
        "/logistica/DevolucaoInterna/?action=cancelarPedidoDevolucaoInterna" => '\App\Controllers\DevolucaoInterna::cancelarPedidoDevolucaoInterna',


    ]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
    $app = new App\Application;
    $app->setRoutes($routes);
    $app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
    echo $e->getMessage();
} catch(App\NotFoundException $e) {
    http_response_code(404);
    echo $e->getMessage();
}