$(function($){

    $("#salvar-solicitacao-devolucao").click(function(){
         if($("#form-itens-para-devolucao").find("input[name='qtd[]']").length == 0){
             alert('Você deve inserir um item antes de salvar.');
             return false;
        }

        ok = true;
        $("input[name='qtd[]']").each(function (i){
            this.value = parseInt(this.value);
            if(this.value < 1){
                ok = false;

                $(this).css({"border":"3px solid red"});
            } else {
                $(this).css({"border":""});
            }
        });

        if(!ok){
            return false;
        }

        if(validar_campos('tabela-itens-devolucao')){

            var data = $("#form-itens-para-devolucao").serialize();

            $.ajax({
             url: "/logistica/DevolucaoInterna/?action=salvarDevolucaoInterna",
             type: 'POST',
             data: data,
             success: function (response) {
             if (response == '1') {
             alert('Pedido salvo com sucesso!');
             window.location.href = "?action=pesquisarSolicitacaoDevolucaoInterna";
             } else {
             alert(response);
             }
             }
             });

        }
        return false;
    });

    $('#autorizar-todos').click(function(){

        if($(this).is(':checked')){
            $('.quantidade-autorizada').each(function(){
                    $(this).val($(this).attr('max'));
            });
        }

    });

    $('#salvar-autorizar-solicitacao').click(function(){

        $('.quantidade-autorizada').each(function(){
            if($(this).val() > $(this).attr('max')){
                $(this).val('');
                $(this).focus();
                alert('A quantidade autorizada deve ser menor ou igual a solicitada. ');
                return false;
            }else if($(this).val() < 0){
                $(this).val('');
                $(this).focus();
                alert('A quantidade autorizada não pode ser menor que zero. ');
                return false;

            }
        });
        if(validar_campos('div-autorizar-solicitacao')){

            var data = $("#form-autorizar-solicitacao").serialize();

            $.ajax({
                url: "/logistica/DevolucaoInterna/?action=salvarAutorizacaoDevolucaoInterna",
                type: 'POST',
                data: data,
                success: function (response) {
                    if (response == '1') {
                        alert('Pedido salvo com sucesso!');
                        window.location.href = "?action=pesquisarSolicitacaoDevolucaoInterna";
                    } else {
                        alert(response);
                    }
                }
            });

        }
        return false;

    });

    $('#enviar-todos').click(function(){

        if($(this).is(':checked')){
            $('.quantidade-enviar').each(function(){
                $(this).val($(this).attr('max'));
            });
        }

    });

    $('#salvar-envio-solicitacao').click(function(){

        $('.quantidade-enviar').each(function(){
            if($(this).val() > $(this).attr('max')){
                $(this).val('');
                $(this).focus();
                alert('A quantidade de envio não deve ser maior que a autorizada. ');
                return false;
            }else if($(this).val() < 0){
                $(this).val('');
                $(this).focus();
                alert('A quantidade de envio não pode ser menor que zero. ');
                return false;

            }
        });
        if(validar_campos('div-enviar-solicitacao')){

            var data = $("#form-enviar-solicitacao").serialize();

            $.ajax({
                url: "/logistica/DevolucaoInterna/?action=salvarEnvioItensDevolucaoInterna",
                type: 'POST',
                data: data,
                success: function (response) {
                    if (response == '1') {
                        alert('Pedido salvo com sucesso!');
                        window.location.href = "?action=pesquisarSolicitacaoDevolucaoInterna";
                    } else {
                        alert(response);
                    }
                }
            });

        }
        return false;

    });

    $('#confirmar-todos').click(function(){

        if($(this).is(':checked')){
            $('.quantidade-confirmar').each(function(){
                $(this).val($(this).attr('max'));
            });
        }

    });

    $('#salvar-confirmar-solicitacao').click(function(){
        var cont =0;
        $('.quantidade-confirmar').each(function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('max'))){
                $(this).val('');
                $(this).focus();
                alert('A quantidade confirmada não deve ser maior que a enviada. ');
                return false;
            }else if(parseInt($(this).val()) < 0){
                $(this).val('');
                $(this).focus();
                alert('A quantidade confirmada não pode ser menor que zero. ');
                return false;

            }
           if( $(this).val() >0){
               cont ++;
           }

        });
        if(cont == 0){
            alert('Você deve confirmar pelo menos um item para poder salvar.');
            return false;
        }
        if(validar_campos('div-confirmar-solicitacao')){

            var data = $("#form-confirmar-solicitacao").serialize();

            $.ajax({
                url: "/logistica/DevolucaoInterna/?action=salvarConfirmarcaoEntregaItensDevolucaoInterna",
                type: 'POST',
                data: data,
                success: function (response) {
                    if (response == '1') {
                        alert('Pedido salvo com sucesso!');
                        window.location.href = "?action=pesquisarSolicitacaoDevolucaoInterna";
                    } else {
                        alert(response);
                    }
                }
            });

        }
        return false;

    });

    $('.cancelar-pedido').on('click',function(){
        $("#input-pedido-id").val('');
        $("#justificativa-cancelar").val('');
        var id = $(this).attr('pedido-id');
        $("#input-pedido-id").val(id);

    });

    $('#salvar-cancelar-pedido').on('click', function(){

        if(validar_campos('div-cancelar-pedido')){

            var data = $("#form-cancelar-pedido").serialize();

            $.ajax({
                url: "/logistica/DevolucaoInterna/?action=cancelarPedidoDevolucaoInterna",
                type: 'POST',
                data: data,
                success: function (response) {
                    if (response == '1') {
                        alert('Pedido de devolução cancelado com sucesso!');
                        window.location.reload();
                        $("#input-pedido-id").val('');
                        $("#justificativa-cancelar").val('');
                    } else {
                        alert(response);
                    }
                }
            });

        }
        return false;

    });



});
