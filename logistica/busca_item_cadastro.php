<?php
require_once('../db/config.php');

$like = $_GET['term'];

$itens = array();

if(isset($_GET['tipo']) && ($_GET['tipo'] == 1)) {

    $sqlNotIn = "SELECT catalogo.simpro_id FROM catalogo WHERE tipo = 1 AND (catalogo.simpro_id IS NOT NULL and catalogo.simpro_id <> 0)";
    $resultNotIn = mysql_query($sqlNotIn);
    $notIn = '';
    $arrayNotIn = [];
    while ($row = mysql_fetch_array($resultNotIn)) {
        $arrayNotIn[] = $row['simpro_id'];
    }

    $notIn = implode(',',$arrayNotIn);

    if(count($arrayNotIn) == 0){
        $condNotIn = "";
    }else{
        $condNotIn = "AND simpro.id not in ({$notIn})";
    }




    $sql=" SELECT
CONCAT(simpro.descricao,' LAB:',simpro.fabricante,' TISS: ',simpro.codigo_simpro_tiss,' TUSS: ',simpro.codigo_tuss) AS item,
simpro.id,
simpro.descricao AS principio,
simpro.fabricante AS lab,
simpro.codigo_tuss AS tuss,
simpro.codigo_simpro_tiss AS tiss,
'' AS apresentacao,
1 AS tipo,
'S' AS referencia
FROM
simpro

WHERE
simpro.codigo_mercado IN (90,20,70,30) 
AND simpro.descricao LIKE '%$like%'
{$condNotIn}
";



} elseif(isset($_GET['tipo']) && ($_GET['tipo'] == 0)) {

    $sqlNotIn = "SELECT catalogo.brasindice_id FROM catalogo WHERE tipo = 0 AND (catalogo.brasindice_id IS NOT NULL and catalogo.brasindice_id <> 0)";
    $resultNotIn = mysql_query($sqlNotIn);
    $notIn = '';
    $arrayNotIn = [];
    while ($row = mysql_fetch_array($resultNotIn)) {
        $arrayNotIn[] = $row['brasindice_id'];
    }

    $notIn = implode(',',$arrayNotIn);

    if(count($arrayNotIn) == 0){
        $condNotIn = "";
    }else{
        $condNotIn = "AND brasindice.id not in ({$notIn})";
    }

    $sql ="SELECT
CONCAT(brasindice.ITEM,' ',brasindice.APRESENTACAO,' LAB:',brasindice.LABORATORIO,' TISS:',brasindice.TISS,' TUSS:',COALESCE(brasindice.TUSS,'')) AS item,
brasindice.LABORATORIO AS lab,
brasindice.ITEM AS principio,
brasindice.APRESENTACAO AS apresentacao,
brasindice.TISS AS tiss,
brasindice.TUSS AS tuss,
0 AS tipo,
brasindice.id,
'T' AS referencia,
brasindice.GENERICO
FROM
brasindice

WHERE
brasindice.TIPO ='MED' 
AND CONCAT(brasindice.ITEM,' ',brasindice.APRESENTACAO) LIKE '%$like%'
{$condNotIn}";

} elseif(isset($_GET['tipo']) && ($_GET['tipo'] == 3)) {

    $sqlNotIn = "SELECT catalogo.brasindice_id FROM catalogo WHERE tipo = 3 AND (catalogo.brasindice_id IS NOT NULL and catalogo.brasindice_id <> 0)";
    $resultNotIn = mysql_query($sqlNotIn);
    $notIn = '';
    $arrayNotIn = [];
    while ($row = mysql_fetch_array($resultNotIn)) {
        $arrayNotIn[] = $row['brasindice_id'];
    }

    $notIn = implode(',',$arrayNotIn);

    if(count($arrayNotIn) == 0){
        $condNotIn = "";
    }else{
        $condNotIn = "AND brasindice.id not in ({$notIn})";
    }

    $sql ="SELECT
CONCAT(brasindice.ITEM,' ',brasindice.APRESENTACAO,' LAB:',brasindice.LABORATORIO,' TISS:',brasindice.TISS,' TUSS:',COALESCE(brasindice.TUSS,'')) AS item,
brasindice.LABORATORIO AS lab,
brasindice.ITEM AS principio,
brasindice.APRESENTACAO AS apresentacao,
brasindice.TISS AS tiss,
brasindice.TUSS AS tuss,
3 AS tipo,
brasindice.id,
'T' AS referencia
FROM
brasindice
LEFT JOIN catalogo ON brasindice.id = catalogo.brasindice_id
WHERE
brasindice.TIPO ='DIE' 
AND CONCAT(brasindice.ITEM,' ',brasindice.APRESENTACAO) LIKE '%$like%'
{$condNotIn}";

}


$result = mysql_query($sql);

while ($row = mysql_fetch_array($result)) {
    $item['generico'] = '';
    if($_GET['tipo'] == 0)
        $item['generico'] = $row['GENERICO'];

    $item['value'] = $row['item'];
    $item['nome'] = $row['principio']." ".$row['apresentacao'];
    $item['id'] = $row['id'];
    $item['principio'] = $row['principio'];
    $item['apresentacao'] = $row['apresentacao'];
    $item['lab'] = $row['lab'];
    $item['tiss'] = $row['tiss'];
    $item['tuss'] = $row['tuss'];
    $item['tipo'] = $row['tipo'];
    $item['referencia'] = $row['referencia'];

    array_push($itens, $item);
}

echo json_encode($itens);