<?php
$id = session_id();
if(empty($id))
	session_start();

include_once('../db/config.php');
include_once('../utils/codigos.php');
require __DIR__ . '/../vendor/autoload.php';

use App\Models\Logistica\PedidosInternos;

//class Estoque{
	
	function estoque(){
		echo "<div id='estoque'>";
		echo "<p><b>Buscar por: </b> <select style='background-color:transparent;' id='sel_estoque'>
				 <option value='t'>Tudo</option>
				 <option value='med'>Medicamento</option>
				 <option value='medcontrolado'>Medicamento Controlado</option>
				 <option value='mat'>Material</option>
				 <option value='die'>Dieta</option>
				 <option value='eqp'>Equipamento</option></select></p>";
		echo "<p><b>Maior que zero? </b> <select style='background-color:transparent;' id='maior_que_zero'>
				 <option value='N' selected >Não</option>
				 <option value='S'>SIM</option></select></p>";
		echo "<p><span id='busca_est_span'>
				<b>Item:</b>
					<input type='text' name='item' id='nome_item' ></input>
				<b>Todos:</b>
					<input type='checkbox' id='todos_est'></input></span></p>";
		echo"<p><input type='hidden' name='UR' id='ur' value='{$_SESSION["empresa_principal"]}'/></p>";
		echo"<p><input type='hidden' name='tiss' id='cod' value=''/></p>";
                 echo "<p><input type='hidden' id='catalogo_id' value='-1' /></p>";
		echo "<button id='pesquisa-estoque' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Pesquisar</span></button></div>";
		echo "<div id='dialog-menssagem' title='Nota salva'>
	 			<p><span >Editada com sucesso!</span>".
				"<div id='text-dialog'></div></p></div>";
        echo "<center><h4 id='msg-sucesso' style='display: none; background: #2ca02c'></h4></center>";
		echo "<div id='est'></div>";
		echo "</div>";
	
	}
	
	function pedido_estoque(){
		echo "<center><h1>Compra para o Estoque</h1></center>";
		//echo "<center><h3>Solicitação Avulsa</h3></center>";
	
		echo "<div id='div-pedido-estoque'>";
	
			echo "<p><b>Data:</b><input type='text' class='data_envio OBG' id='data_envio' name='data_envio' /></p>";
			 $data = date("Ymd");
  		  	 echo "<input type ='hidden' id='data_atual' value = '{$data}'/>";
  		  	
			echo "<p ><b>Observação:</b><br><textarea name='obs' id='obs' rows='2' cols='120'/></textarea></p>";
			formulario();
			echo "<button aria-disabled='false' role='button' 
					class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' 
					style='float:left;' id='add-item-pedido-estoque'><span class='ui-button-text'>+</span></button>";
			
			//Tabela de itens
			echo "<br><table class='mytable' width=100% id='tabela-itens-pedido' >
				<thead>
					<tr>
						<th>Itens</th>.
						<th>Tipo</th>
	  					<th width='1%'>Quantidade</th>
					</tr>
				</thead>
			  </table>";
		echo "</div>";
		
		echo "<br>
			<button aria-disabled='false role='button' 
				class='mostrar_botao_pedido ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' 
				style='float:left;' id='salvar-pedido-estoque'>
				<span class='ui-button-text'>Salvar</span>
			</button>";
	
	}
	
	function formulario(){
		
		echo "<div id='dialog-pedido-estoque' title='Formul&aacute;rio' tipo='' classe='' cod=''>";
		//echo alerta();
		echo "<center><div id='tipos'>
			  	<input type='radio' id='bmed' name='tipos' checked value='med' /><label for='bmed'>Medicamentos</label>
			  	<input type='radio' id='bmat' name='tipos' value='mat' /><label for='bmat'>Materiais</label>
			  	<input type='radio' id='bequip' name='tipos' value='eqp' /><label for='bequip'>Equipamentos</label>
				<input type='radio' id='bdie' name='tipos' value='die' /><label for='bdie'>Dieta</label>
  			</div></center>";
		echo "<p><b>Busca:</b><br/>
				<input type='text' name='busca_pedido' id='busca-item-pedido' /></p>";
		//echo "<div id='opcoes-kits'></div>";
		echo "<input type='hidden' name='numero_tiss' id='numero_tiss' value='-1' />";
                 echo "<p><input type='hidden' id='catalogo_id' value='-1' /></p>";
		echo "<input type='hidden' name='apresentacao' id='apresentacao' value='-1' />";
		echo "<input type='hidden' name='tipo' id='tipo' value='-1' />";
		
		echo "<p><b>Nome:</b><input type='text' name='nome' id='nome' class='OBG' />";
	
		echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
		echo "<button id='add-item-pedido' tipo='' >+</button>
			</div>";
	}
	
	function entrada_estoque(){
		echo "<div><center><h1>Entrada em Estoque (CMD)</h1></center></div>";
		
		$p=$_SESSION["empresa_principal"];
		$sql = "SELECT 
						p.ID, 
						DATE_FORMAT(p.DATA, '%d/%m/%Y') as DATA, 
						U.nome,DATE_FORMAT(p.DATA, '%H:%i:%S') as hora,
						(
							(
								CASE WHEN COUNT( * ) >1 THEN 
									SUM(i.ENVIADO ) 
								ELSE 
									i.ENVIADO END
							) -
							(
								CASE WHEN COUNT( * ) >1 THEN 
									sum(i.CONFIRMADO) 
								ELSE 
									i.CONFIRMADO END
							) 
						) AS itens
		
		FROM
			itenspedidosinterno AS i INNER JOIN
			pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID ) inner join  
			usuarios AS U on (U.idUsuarios = p.USUARIO) inner join 
			empresas AS E on (p.EMPRESA = E.id)
		WHERE 
			(	
				i.STATUS = 'P'	OR 
                                (
					i.STATUS = 'F' AND 
					i.CONFIRMADO < i.ENVIADO AND  
					i.CANCELADO='N' AND 
					i.CANCELADO_ENTRADA='N'
				) 
                OR 
				(
					i.STATUS = 'F' AND 
					i.CANCELADO='S' AND 
					i.CONFIRMADO < i.ENVIADO AND
					i.CANCELADO_ENTRADA='N'
				)
			) AND 
			p.EMPRESA ={$p}  AND
			i.CATALOGO_ID <> 0
                GROUP BY 
			p.ID 
		ORDER BY
			p.ID";
		
		$r = mysql_query($sql);
		
		while($row = mysql_fetch_array($r)){
			foreach($row AS $key => $value) {
				$row[$key] = stripslashes($value);
			}
			$vazio = false;
			$presc=0;
			if($row['idPrescricao']!=-1){
				$presc = $row['idPrescricao'];
			}
		
			echo "<div style='border:1px solid #fff;width:240px;display:table;float:left;margin:0 auto;padding:15px; font-size:12px;'>";
			echo "<table width='100%'>";
			echo "<tr>";
			echo "<td>";
			echo "<center><button class='entrada_pedido' p='{$row['ID']}' idempresa='{$p}' presc='{$presc}' tipo='1' url='{$url}'></button></center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center><b> PEDIDO N&deg;".strtoUpper($row['ID'])."</b></center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center style='font-size:9px;'>(".strtoUpper($row['nome']).")</center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center>".$row['DATA']."</center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center style='font-size:9px;'>".$row['hora']."</center>";
			echo "</td>";
			echo "</tr>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo utf8_encode("<center style='color:red;'>&iacute;tens (".$row['itens'].")</center>");
			echo "</td>";
			echo "</tr>";
			echo "</table>";
		echo "</div>";
		}
	}


	

function pedido_entrada_estoque($p,$emp){
	
	echo "<div id='cancelar-item-entrada-estoque' title='Cancelar item' id_item='' >";
  		echo alerta();
  	  		echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar o cancelamento, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
  		//echo "<p><b>Quantidade:</b><input type='text' name='quantidade' class='OBG' size='5' /></p>";
  	  		echo "<p><b>Justificativa:</b><input type='text' name='justificativa' class='OBG' size='100' /></p>
  		</div>";
		
  		$opcao = 'c.empresa';
		$row = mysql_fetch_array(mysql_query("SELECT e.nome FROM empresas as e inner join pedidosinterno as p on (p.EMPRESA = e.id) WHERE p.ID = '$p'"));
	
	echo "<div id='div-envio-paciente' p='$p' np='$paciente' >";
		echo "<center><h1>Enviados para o pedido N&deg; {$p}</h1></center>";
		echo "<center><h3> {$row['nome']}</h3></center>";
		
		echo alerta();
	
		//echo "<p><b>Data:</b><input type='text' class='data OBG' id='data-envio' name='data_envio' >";
		//	echo "<p><b>Observação:</b><br><textarea name='obs' id='obs' rows='2' cols='140'/></textarea>";
	
		echo "<table class='mytable' width=100% id='lista-enviados' ><thead><tr><th>Itens Solicitados</th><th align='center' width='10%'></th><th align='center' width='10%'></th>
		<th align='center' width='8%'></th><th align='center' width='8%'></th></tr></thead>";
	
		
		$sqlmed = PedidosInternos::getMatMedDieEnviadoNaoConfimandoPorPedidoETipo($p, 0);
		$rmed = mysql_query($sqlmed);
		while($row = mysql_fetch_array($rmed)) {
			foreach ($row AS $key => $value) {
				$row[$key] = stripslashes($value);
			}


				$dados = "autorizado='{$row['autorizado']}' id_pedido='{$row['PEDIDOS_INTERNO_ID']}' id_item='{$row['ID']}' n='{$row['principio']}'
				 tiss='{$row['NUMERO_TISS']}' sn='{$row['apresentacao']}' soldata='{$row['data']}' env='{$row['enviados']}'
				  total='{$row['QTD']}' tipo='0'";

				$pedidos = ($row['QTD'] - $row['ENVIADO']);

				echo "<tr style='background-color:#E6E6FA '>
							<td>
								<b>MEDICAMENTO: </b>{$row['principio']} {$row['apresentacao']} <b>LAB:</b> {$row['lab_desc']}
								<br>
								(<b> Principio ativo: </b>{$row['principioAtivo']})

							</td>";
					echo "<td align='center' colspan='2'>
								<b>Pedidos: </b>{$row['QTD']}
								/
								<b>Enviados: </b>{$row['ENVIADO']}
							</td>
							<td align='center' colspan='2' >
								<b>Confirmados: </b>{$row['CONFIRMADO']}
							</td>

				</tr>
				<tr style='background-color:lightgrey '>
							<td>
								<b>Itens enviados pela Matriz </b>
							</td>
							<td>
								<b>Enviados</b>
							</td>
							<td align='center'  >
								<b>Confirmados: </b>
							</td>
							<td>
								<b>Entrada</b>
							</td>
							<td align='center'  >
								<b>Lote: </b>
							</td>

				</tr>";
			$sqlSaidaMed = '';
			$saidaMed = '';
			$sqlSaidaMed = PedidosInternos::getItensMatMedDiePedidosInternosSaida($row['ID']);
			$saidaMed = mysql_query($sqlSaidaMed);
			while($rowSaidaMed = mysql_fetch_array($saidaMed)) {
				$dadosSaida = " saida_id = '{$rowSaidaMed['idSaida']}' saida_quantidade='{$rowSaidaMed['quantidade']}'
									saida_confirmados = '{$rowSaidaMed['confirmacao_pedidointerno']}'  catalogo_id='{$rowSaidaMed['CATALOGO_ID']}' ";
				echo "<tr>
							<td>
								<b>MEDICAMENTO: </b>{$rowSaidaMed['item']}	<b>LAB:</b> {$rowSaidaMed['lab_desc']}
								<br>
								(<b> Principio ativo: </b>{$rowSaidaMed['principioAtivo']})
								<br>
								<b>Enviado em: </b>{$rowSaidaMed['data_br']} |
								<b>Quantidade: </b>{$rowSaidaMed['quantidade']} |
								<b>Lote: </b>{$rowSaidaMed['LOTE']}

							</td>
							<td align='center'>
								{$rowSaidaMed['quantidade']}
								<br>
								<b> Lote:</b> {$rowSaidaMed['LOTE']}
							</td>
							<td align='center' >
								{$rowSaidaMed['confirmacao_pedidointerno']}
							</td>
							<td class='dados' $dados $dadosSaida>
								<input type='text' name='qtd-envio' class='numerico qtd_entrada' style='text-align:right' size='4' value=''/>
							<td>
								<input type='text' name='lote' value='{$rowSaidaMed['LOTE']}' style='text-align:right' size='4'/></td>
							</td>
					</tr>";

			}





			}
	
			$sqldie = PedidosInternos::getMatMedDieEnviadoNaoConfimandoPorPedidoETipo($p, 3);
	
			$rdie = mysql_query($sqldie);
			while($row = mysql_fetch_array($rdie)){
					foreach($row AS $key => $value) {
					$row[$key] = stripslashes($value);
			}
			$dados = "autorizado='{$row['autorizado']}' id_pedido='{$row['PEDIDOS_INTERNO_ID']}'
			 			  id_item='{$row['ID']}'
					 	n='{$row['principio']}' tiss='{$row['NUMERO_TISS']}' sn='{$row['apresentacao']}'
					 	 soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['QTD']}'
					 	 autorizado='{$row['autorizado']}' tipo='3'
					 	 ";

				echo "<tr style='background-color:#E6E6FA '>
							<td>
								<b>DIETA: </b>{$row['principio']} {$row['apresentacao']} <b>LAB:</b> {$row['lab_desc']}


							</td>";
				echo "<td align='center' colspan='2'>
								<b>Pedidos: </b>{$row['QTD']}
								/
								<b>Enviados: </b>{$row['ENVIADO']}
							</td>
							<td align='center' colspan='2' >
								<b>Confirmados: </b>{$row['CONFIRMADO']}
							</td>

				</tr>
				<tr style='background-color:lightgrey '>
							<td>
								<b>Itens enviados pela Matriz </b>
							</td>
							<td>
								<b>Enviados</b>
							</td>
							<td align='center'  >
								<b>Confirmados: </b>
							</td>
							<td>
								<b>Entrada</b>
							</td>
							<td align='center'  >
								<b>Lote: </b>
							</td>

				</tr>";
				$sqlSaidaDieta = '';
				$saidaDieta = '';
				$sqlSaidaDieta = PedidosInternos::getItensMatMedDiePedidosInternosSaida($row['ID']);
				$saidaDieta = mysql_query($sqlSaidaDieta);
				while($rowSaidaDieta = mysql_fetch_array($saidaDieta)) {
					$dadosSaida = " saida_id = '{$rowSaidaDieta['idSaida']}' saida_quantidade='{$rowSaidaDieta['quantidade']}'
									saida_confirmados = '{$rowSaidaDieta['confirmacao_pedidointerno']}' catalogo_id='{$rowSaidaDieta['CATALOGO_ID']}'";
					echo "<tr>
							<td>
								<b>DIETA: </b>{$rowSaidaDieta['item']}	<b>LAB:</b> {$rowSaidaDieta['lab_desc']}
								<br>
								<b>Enviado em: </b>{$rowSaidaDieta['data_br']} |
								<b>Quantidade: </b>{$rowSaidaDieta['quantidade']} |
								<b>Lote: </b>{$rowSaidaDieta['LOTE']}

							</td>
							<td align='center'>
								{$rowSaidaDieta['quantidade']}
								<br>
								<b> Lote:</b> {$rowSaidaDieta['LOTE']}
							</td>
							<td align='center' >
								{$rowSaidaDieta['confirmacao_pedidointerno']}
							</td>
							<td class='dados' $dados $dadosSaida>
								<input type='text' name='qtd-envio' class='numerico qtd_entrada' style='text-align:right' size='4' value=''/>
							<td>
								<input type='text' name='lote' value='{$rowSaidaDieta['LOTE']}' style='text-align:right' size='4'/></td>
							</td>
					</tr>";

				}



			}
	
			/*$sqlmat = "SELECT s.enfermeiro, (CASE WHEN COUNT(*)>1 THEN SUM(s.enviado) ELSE s.enviado END) AS enviados,sum((CASE s.status when 2 THEN  s.qtd ELSE  s.autorizado  END)) as autorizado, s.enviado, s.cod, s.idSolicitacoes as sol, s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data, b.principio, b.apresentacao
			FROM solicitacoes as s, brasindice as b, clientes as c
			WHERE {$opcao} = '$p' AND s.paciente = c.idClientes AND s.tipo = '1' AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND (s.status = '1' OR s.status = '2')
			AND b.numero_tiss = s.cod GROUP BY cod ORDER BY principio, apresentacao, enviado";*/
	
			$sqlmat = PedidosInternos::getMatMedDieEnviadoNaoConfimandoPorPedidoETipo($p, 1);
	
			$rmat = mysql_query($sqlmat);
					while($row = mysql_fetch_array($rmat)){
					foreach($row AS $key => $value) {
					$row[$key] = stripslashes($value);
			}
			$dados = "autorizado='{$row['autorizado']}' id_pedido='{$row['PEDIDOS_INTERNO_ID']}'
			  id_item='{$row['ID']}' n='{$row['principio']}' tiss='{$row['NUMERO_TISS']}' sn='{$row['apresentacao']}'
			   soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['QTD']}' tipo='1'";


						echo "<tr style='background-color:#E6E6FA '>
							<td>
								<b>MATERIAL: </b>{$row['principio']} {$row['apresentacao']} <b>LAB:</b> {$row['lab_desc']}


							</td>";
						echo "<td align='center' colspan='2'>
								<b>Pedidos: </b>{$row['QTD']}
								/
								<b>Enviados: </b>{$row['ENVIADO']}
							</td>
							<td align='center' colspan='2' >
								<b>Confirmados: </b>{$row['CONFIRMADO']}
							</td>

				</tr>
				<tr style='background-color:lightgrey '>
							<td>
								<b>Itens enviados pela Matriz </b>
							</td>
							<td>
								<b>Enviados</b>
							</td>
							<td align='center'  >
								<b>Confirmados: </b>
							</td>
							<td>
								<b>Entrada</b>
							</td>
							<td align='center'  >
								<b>Lote: </b>
							</td>

				</tr>";
						$sqlSaidaDieta = '';
						$saidaMat = '';
						$sqlSaidaMat = PedidosInternos::getItensMatMedDiePedidosInternosSaida($row['ID']);
						$saidaMat = mysql_query($sqlSaidaMat);
						while($rowSaidaMat = mysql_fetch_array($saidaMat)) {
							$dadosMat = " saida_id = '{$rowSaidaMat['idSaida']}' saida_quantidade='{$rowSaidaMat['quantidade']}'
									saida_confirmados = '{$rowSaidaMat['confirmacao_pedidointerno']}' catalogo_id='{$rowSaidaMat['CATALOGO_ID']}'";
							echo "<tr>
							<td>
								<b>MATERIAL: </b>{$rowSaidaMat['item']}	<b>LAB:</b> {$rowSaidaMat['lab_desc']}
								<br>
								<b>Enviado em: </b>{$rowSaidaMat['data_br']} |
								<b>Quantidade: </b>{$rowSaidaMat['quantidade']} |
								<b>Lote: </b>{$rowSaidaMat['LOTE']}

							</td>
							<td align='center'>
								{$rowSaidaMat['quantidade']}
								<br>
								<b> Lote:</b> {$rowSaidaMat['LOTE']}
							</td>
							<td align='center' >
								{$rowSaidaMat['confirmacao_pedidointerno']}
							</td>
							<td class='dados' $dados $dadosMat>
								<input type='text' name='qtd-envio' class='numerico qtd_entrada' style='text-align:right' size='4' value=''/>
							<td>
								<input type='text' name='lote' value='{$rowSaidaMat['LOTE']}' style='text-align:right' size='4'/></td>
							</td>
					</tr>";

						}
	}

	///jeferson 2012-10-24
	$sqleqp = PedidosInternos::getEquipamentoEnviadoNaoConfimandoPorPedido($p);
	
							$reqp = mysql_query($sqleqp);
							while($row = mysql_fetch_array($reqp)){
							foreach($row AS $key => $value) {
							$row[$key] = stripslashes($value);
						}
	
						
						$dados = "autorizado='{$row['autorizado']}' sol='{$row['sol']}' catalogo_id ='{$row['CATALOGO_ID']}' id_pedido='{$row['PEDIDOS_INTERNO_ID']}' id_item='{$row['ID']}' sn='{$tipo_sol_eqp}' tiss='{$row['NUMERO_TISS']}' soldata='{$row['data']}' env='{$row['enviados']}' total='{$row['QTD']}' tipo='5' tipo_sol_equipamento='{$row['TIPO_SOL_EQUIPAMENTO']}'";





								echo "<tr style='background-color:#E6E6FA '>
							<td>
								<b>EQUIPAMENTO: </b>{$row['item']} <b>


							</td>";
								echo "<td align='center' colspan='2'>
								<b>Pedidos: </b>{$row['QTD']}
								/
								<b>Enviados: </b>{$row['ENVIADO']}
							</td>
							<td align='center' colspan='2' >
								<b>Confirmados: </b>{$row['CONFIRMADO']}
							</td>

				</tr>
				<tr style='background-color:lightgrey '>
							<td>
								<b>Itens enviados pela Matriz </b>
							</td>
							<td>
								<b>Enviados</b>
							</td>
							<td align='center'  >
								<b>Confirmados: </b>
							</td>
							<td>
								<b>Entrada</b>
							</td>
							<td align='center'  >
								<b>Lote: </b>
							</td>

				</tr>";
								$sqlSaidaEquipamento = '';
								$saidaEquipamento = '';
								$sqlSaidaEquipamento = PedidosInternos::getItensEquipamentosPedidosInternosSaida($row['ID']);
								$saidaEquipamento = mysql_query($sqlSaidaEquipamento);
								while($rowSaidaEquipamento = mysql_fetch_array($saidaEquipamento)) {
									$dadosSaida = " saida_id = '{$rowSaidaEquipamento['idSaida']}' saida_quantidade='{$rowSaidaEquipamento['quantidade']}'
									saida_confirmados = '{$rowSaidaEquipamento['confirmacao_pedidointerno']}' catalogo_id='{$rowSaidaEquipamento['CATALOGO_ID']}'";
									echo "<tr>
							<td>
								<b>EQUIPAMENTO: </b>{$rowSaidaEquipamento['principio']}	<b>LAB:</b> {$rowSaidaEquipamento['lab_desc']}
								<br>
								<b>Enviado em: </b>{$rowSaidaEquipamento['data_br']} |
								<b>Quantidade: </b>{$rowSaidaEquipamento['quantidade']} |
								<b>Lote: </b>{$rowSaidaEquipamento['LOTE']}

							</td>
							<td align='center'>
								{$rowSaidaEquipamento['quantidade']}
								<br>
								<b> Lote:</b> {$rowSaidaEquipamento['LOTE']}
							</td>
							<td align='center' >
								{$rowSaidaEquipamento['confirmacao_pedidointerno']}
							</td>
							<td class='dados' $dados $dadosSaida>
								<input type='text' name='qtd-envio' class='numerico qtd_entrada' style='text-align:right' size='4' value=''/>
							<td>
								<input type='text' name='lote' value='{$rowSaidaEquipamento['LOTE']}' style='text-align:right' size='4'/></td>
							</td>
					</tr>";

								}
	
	
						}
						///jeferson 2012-10-24 fim
	                       
						echo "</table><br/>";
	
						echo "<button id='salvar-entrada-estoque-pedido' id_pedido='{$p}' emp='{$emp}' style='display:inline;float:left;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
						<span class='ui-button-text'>Salvar</span></button>";
	
	
						/*echo "<form action='imprimir_separacao.php?p={$p}&tipo={$_GET['tipo']}' method='POST' target='_blank'>";
	echo "<button style='display:inline;float:left;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
	<span class='ui-button-text'>Imprimir</span></button>";
	echo "</form>";
	echo "</p>";*/
	
	echo "</div>";
	
	}

function getSQLSaida($idItens){
	$sql = "SELECT
saida.idSaida,
saida.CATALOGO_ID,
saida.NUMERO_TISS,
CONCAT(saida.nome,' ',saida.segundoNome) as item,
saida.quantidade,
saida.LOTE
 FROM
`saida`
WHERE
saida.ITENSPEDIDOSINTERNOS_ID  = {$idItens}";
	$lista = array();
	$result = mysql_query($sql) or die (mysql_error());
	if($result){
		while ($row = mysql_fetch_array($result))
			$lista[] = $row;
		return $lista;
	}
}
	
//}
?>