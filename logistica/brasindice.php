<?php

$id = session_id();
if(empty($id))
	session_start();

include_once('../db/config.php');
include_once('../utils/codigos.php');

class Brasindice{

	function buscar(){
	    $buttonNovo = "";
		//95 Manoella, 44 Gisa
        if (
            $_SESSION["adm_user"] == 1 || $_SESSION["id_user"] == 95 || $_SESSION["id_user"] == 44
        ) {
            echo "<div><b>*Antes de clicar em <i>Novo</i>, verifique se o item desejado já existe no catálogo como <i>Ativo</i>!</b></div>";
            $buttonNovo = "<button id='novo-brasindice' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Novo</span></button>";
        }
		echo "<div id='brasindice_busca'>";                   
                    echo "<p><b>Buscar: </b>
                            <select style='background-color:transparent;' id='tipo_brasindice'>
                                     <option value='-1'>Todos</option>
                                     <option value='0'>Medicamento</option>
                                     <option value='1'>Material</option>
                                     <option value='3'>Dieta</option>
                            </select></p>
                            <p> <b>Status: </b><select style='background-color:transparent;' id='status_item'>
                                     <option value='-1'>Todos</option>
                                     <option value='A'>Ativo</option>
                                     <option value='D'>Inativo</option>

                            </select></p>";
		echo "<p>
			<span id='busca_brasindice_span'>
				<b>Item:</b>
					<input type='text' name='item_busca_brasindice' id='item-busca-brasindice' p='pes-item-brasindice'></input>
			</span></p>";
		echo"<p><input type='hidden' name='UR' id='ur' value='{$_SESSION["empresa_principal"]}'/></p>";
		echo"<p><input type='hidden' name='cod' id='cod' value=''/></p>";
                echo"<p><input type='hidden' name='catalogo_id' id='catalogo_id' value=''/></p>";
		echo "<p><button type='submit' id='pesquisa-brasindice' name='pesquisa-brasindice' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Pesquisar</span></button>";

		if (
			$_SESSION["adm_user"] == 1 || $_SESSION["id_user"] == 95 || $_SESSION["id_user"] == 44
		){ //95 Manoella, 44 Gisa
			echo $buttonNovo;
		}
		
		echo "<p><div id='brasindice_resultado'></div></p>";
		echo "</div>";
		
		
		
	}

    function novo()
    {
        $data = date("Y-m-j H:i:s");

        echo '<link rel="stylesheet" href="/utils/bootstrap-3.3.6-dist/css/bootstrap.min.css" />';
        echo "<form class='form'>";
        echo "<div id='div-novo-brasindice' class='container-fluid'>";
        echo "<div class='alert alert-info'>
                <strong class='glyphicon-info-sign glyphicon'></strong>
                <b> Caso o item procurado não seja encontrado na busca, favor verificar se o mesmo já foi ativado no catálogo!</b>
              </div>";
        $select_material = "";
        if( $_SESSION["adm_user"] == 1 || $_SESSION["id_user"] == 44 ||  $_SESSION["id_user"] == 95 ){
            $select_material = "<option value='1'>Material</option>";
        }

        $disabled ="disabled = 'disabled'";
        echo "<div class='row'>
                <div class='col-md-12'>
                    <label>Selecione o tipo do item: </label>
                    <select class='form-control COMBO_OBG' style='background-color:transparent;' id='tipo_brasindice_cadastro' >
                         <option value='-1'></option>
                         <option value='0'>Medicamento</option>
                         $select_material
                         <option value='3'>Dieta</option>
                    </select>
				</div>
              </div>";
        if($_SESSION["adm_user"] == 1 || $_SESSION["id_user"] == 168 || $_SESSION["id_user"] == 95){
            echo "  <br>
                    <div class='row'>
                        <div class='col-md-12'>
                            <label class='checkbox-inline' style='font-size: 16px; vertical-align: middle;' for='cadastro-produto-interno'>
                                <input type='checkbox' id='cadastro-produto-interno' />
                                <b>Cadastrar como Produto Interno</b>
                            </label>
                        </div>
                    </div>
                    <br>";
        }
        echo "<div class='row'>
                <div class='col-md-12'>
                    <label>Buscar item:</label>
                    <input type='text' class='form-control' $disabled name='buscar-item-cadastro' id='buscar-item-cadastro'/>
                </div>
              </div>";
        echo "<div class='row'>
                <div class='col-md-6'>
                    <label>Nome do Item:</label>
                    <input class='form-control OBG' type='text' name='principio' id='principio' $disabled />
                </div>
                <div class='col-md-6'>
                    <label>Apresentação:</label>
                    <input class='form-control OBG' type='text' name='apresentacao' $disabled id='apresentacao' />
                </div>
              </div>";
        echo "<div class='row'>
                <div class='col-md-4'>
                    <label>Número TISS:</label>
                    <input class='form-control OBG' type='text' $disabled name='numero_tiss' id='numero_tiss' />
                </div>
                <div class='col-md-4'>
                    <label>Número TUSS:</label>
                    <input class='form-control' type='text' maxlength='8' class='OBG' $disabled name='numero_tuss' id='numero_tuss' />
                </div>
                <div class='col-md-4'>
                    <label>Referencia: </label>
                    <select class='form-control COMBO_OBG' $disabled id='referencia'>
                        <option value='-1'></option>
                        <option value='S' " . ($row['REFERENCIA'] == 'S' ? "selected" : "") . ">Simpro</option>
                        <option value='T' " . ($row['REFERENCIA'] == 'T' ? "selected" : "") . ">Brasindice</option>
                        <option value='I' " . ($row['REFERENCIA'] == 'I' ? "selected" : "") . ">Produto Interno</option>
				    </select>
                </div>
              </div>";
        echo "<div class='row'>
                <div class='col-md-6'>
                    <label>Laboratorio:</label>
                    <input class='form-control OBG' type='text' name='laboratorio' $disabled id='laboratorio' />
                </div>
                <div class='col-md-6'>
                    <label for='buscar-principio'>
                        <b>Princípio Ativo:</b>
                    </label>
                    <input type='text'
                           name='buscar_principio'
                           ord='1'
                           data-principio-id=''
                           data-item-name=''
                           class='ui-autocomplete-input buscar-principio-cadastro form-control'
                           autocomplete='off'
                           role='textbox'
                           aria-autocomplete='list'
                           aria-haspopup='true'
                           id='buscar-principio' $disabled />
                    <input type='hidden' name='id-principio' id='id-principio'/>
                </div>
             </div>";
        echo "<div class='row'>
                <div class='col-md-6'>
                    <label>Nome Simpro/Brasindice:</label>
                    <input type='text' class='form-control OBG' name='nome-simpro-brasindice' $disabled id='nome-simpro-brasindice'  />
                </div>
                <div class='col-md-3' id='tag_brasindice'>
                    <label>Tags:</label>
                    <input type='text' class='form-control' name='tags' id='tags' />
                </div>
                <div class='col-md-3 controlado' >
                    <label>Controlado: </label>
                    <select class='form-control' readonly id='controlado'>
                        <option value='-1'></option>
                        <option value='N' selected " . ( $row['ativo'] == 'N' ? "selected" : "") . ">N&atilde;o</option>
                        <option value='S' " . ( $row['ativo'] == 'S' ? "selected" : "") . ">Sim</option>
                    </select>
                </div>
              </div>";

        echo "<div class='row'>
                <div class='col-md-3 antibiotico' >
                    <label>Antibiotico: </label>
                    <select class='form-control {$combo}' id='antibiotico'>
                        <option value='-1'></option>
                        <option value='N' " . ($row['ANTIBIOTICO'] == 'N' ? "selected" : "") . ">N&atilde;o</option>
                        <option value='S' " . ($row['ANTIBIOTICO'] == 'S' ? "selected" : "") . ">Sim</option>
                    </select>
                </div>
                <div class='col-md-3'>
                    <label>Status: </label>
				    <select class='form-control COMBO_OBG' id='status'>
					    <option value='-1'></option>
					    <option value='A'>Ativo</option>
					    <option value='D'>Inativo</option>
				    </select>
		        </div>
		        <div class='col-md-3'>
                    <label>Genérico: </label>
                    <select class='form-control' $disabled id='generico'>
                        <option value=''></option>
                        <option value='N'>Não</option>
                        <option value='S'>Sim</option>
                    </select>
                </div>
                <div class='col-md-3 possui-generico'>
                    <label>Possui Genérico? </label>
                    <select class='form-control' $disabled id='possui-generico'>
                        <option value='-1'></option>
                        <option value='S' " . ($row['possui_generico'] == 'S' ? "selected" : "") . ">Sim</option>
                        <option value='N' " . ($row['possui_generico'] == 'N' ? "selected" : "") . ">Não</option>
				    </select>
                </div>
              </div>";

        $categorias_materiais = \App\Models\Logistica\Catalogo::getCategoriasMateriais();
        echo "<br>
                <div class='panel panel-default' id='categoria-material-combo' style='display: none;'>
                    <div class='panel-heading clearfix'>
                        <h3 class='panel-title pull-left'>
                            <i class='fa fa-list-alt' aria-hidden='true'></i> Categorias utilizadas para referência de materiais nos contratos dos pacientes.</b>
                        </h3>
                    </div>
                    <div class='panel-body'>
                        <div class='row'>
                            <div class='col-md-12'>
                                <label for='categoria-material'>Categoria do Material:</label>
                                <select name='categoria_material' class='form-control' id='categoria-material'>
                                    <option value=''>Selecione uma categoria...</option>";
        foreach ($categorias_materiais as $categorias_material) {
            echo "                  <option value='{$categorias_material['ID']}'>{$categorias_material['ITEM']}</option>";
        }
        echo "                  </select>
                            </div>
                        </div>
                    </div>
                </div>";

        echo "<input type='hidden' type='text' class='numerico' value='{$_SESSION["id_user"]}' $disabled id='usuario' />";
        echo "<input type='hidden' type='text' class='numerico' value='{$data}' id='data'/>";
        echo "<input type='hidden' type='text'  value='' id='id-simpro-brasindice'/>";

        echo "<br><br>
              <div class='row'>
                <div class='col-md-12 text-center'>
                    <button id='brasindice_salvar' class='btn btn-success'>
                        <i class='fa fa-save'></i> Salvar
                    </button>
                </div>
              </div>";
        echo "</div>";
        echo "</form>";
    }
	
	function editar($id) {
	
		$id = $_GET['id'];
		
		$sql = "SELECT principio, apresentacao, lab_desc, DESC_MATERIAIS_FATURAR, NUMERO_TISS, TUSS, tipo, ATIVO, TAG, CONTROLADO,ID,ANTIBIOTICO,REFERENCIA,GENERICO,descontinuado,CATEGORIA_MATERIAL_ID, possui_generico
                    FROM catalogo WHERE ID = '$id'";
		$resultado = mysql_query($sql);
		$sqlPrincipio = "
		SELECT
principio_ativo.principio,
principio_ativo.id
FROM
catalogo_principio_ativo
INNER JOIN principio_ativo ON catalogo_principio_ativo.principio_ativo_id = principio_ativo.id
WHERE
catalogo_principio_ativo.catalogo_id = {$id}";

        $result = mysql_query($sqlPrincipio);
        $rowPrincipio = mysql_fetch_array($result);
        $data = date("Y-m-j H:i:s");

        echo '<link rel="stylesheet" href="/utils/bootstrap-3.3.6-dist/css/bootstrap.min.css" />';
        echo "<form class='form'>";
        echo "<div class='container-fluid' id='div-editar-brasindice'>";
        while($row = mysql_fetch_array($resultado)){
            echo "<div class='row'>
                    <div class='col-md-6'>
                        <label>Principio:</label>
                        <input class='OBG form-control' type='text' size='50' value='{$row['principio']}' name='principio' id='principio' />
                    </div>
                    <div class='col-md-6'>
                        <label>Apresentacao:</label>
                        <input class='OBG form-control' type='text' size='50' value='{$row['apresentacao']}' name='apresentacao' id='apresentacao' />
                    </div>
                  </div>";
            echo "<div class='row'>
                    <div class='col-md-4'>
                        <label>Laboratorio:</label>
                        <input readonly class='OBG form-control' type='text' value='{$row['lab_desc']}' name='laboratorio' id='laboratorio' />
                    </div>
                    <div class='col-md-4'>
                        <label>Numero TISS:</label>
                        <input readonly class='OBG form-control' type='text' class='numerico' value='{$row['NUMERO_TISS']}' name='numero_tiss' id='numero_tiss' />
                    </div>
                    <div class='col-md-4'>
                        <label>Numero TUSS:</label>
                        <input readonly type='text' class='numerico form-control' value='{$row['TUSS']}' name='numero_tuss' id='numero_tuss' />
                    </div>
                  </div>";
            echo"<p><input type='hidden' name='catalogo_id' id='catalogo_id' value='{$row['ID']}' /></p>";


            $disabled = 'disabled';
            $obg ='';
            if($row['tipo']==0){
                $disabled = '';
                $obg ='OBG';
            }
            $principioAntigo = empty($rowPrincipio['id']) ? 0 : $rowPrincipio['id'];
            echo "<div class='row'>
                     <div class='col-md-6'>
                        <label>Tipo: </label>
                        <select disabled class='COMBO_OBG form-control' id='tipo_brasindice_cadastro'>
                             <option value='-1'></option>
                             <option value='0' ".(($row['tipo']=='0')?"selected":"").">Medicamento</option>
                             <option value='1' ".(($row['tipo']=='1')?"selected":"").">Material</option>
                             <option value='3' ".(($row['tipo']=='3')?"selected":"").">Dieta</option>
                        </select>
                    </div>
                    <div class='col-md-6'>
					    <label for='buscar-principio'><b>Princípio Ativo:</b></label>
					    <input value='{$rowPrincipio['principio']}'
                            type='text'
						    size='30'
						    name='buscar_principio'
						    ord='1'
						    data-principio-id=''
						    data-item-name=''
						    class='ui-autocomplete-input buscar-principio-cadastro {$obg} form-control'
						    autocomplete='off'
						    role='textbox'
						    aria-autocomplete='list'
						    aria-haspopup='true'
						    id='buscar-principio' $disabled />
					<input type='hidden' name='id-principio' principio-antigo='{$principioAntigo}' value='{$rowPrincipio['id']}' id='id-principio'/>
           		 </div>
             </div>";
            echo "<div class='row'>
                    <div class='col-md-6'>
                        <label>Nome Simpro:</label>
                        <input type='text' class='form-control nome_simpro' value='{$row['DESC_MATERIAIS_FATURAR']}' name='nome_simpro' id='nome_simpro' readonly='readonly' />
                    </div>
                    <div id='tag_brasindice' class='col-md-6 mostrar_tag'>
                        <label>Tags:</label>
                        <input type='text' name='tags' class='form-control' id='tags' value='{$row['TAG']}' />
                    </div>
                  </div>";
            if($row['tipo']==0){
                $combo='COMBO_OBG';
            }else{
                $combo='';
            }
            echo "<div class='row'>
                    <div class='col-md-4 controlado'>
                        <label>Controlado: </label>
                        <select class='{$combo} form-control' id='controlado'>
                            <option value='-1'></option>
                            <option value='N' ".(($row['CONTROLADO']=='N')?"selected":"").">N&atilde;o</option>
                            <option value='S' ".(($row['CONTROLADO']=='S')?"selected":"").">Sim</option>
				        </select>
                    </div>
                    <div class='col-md-4 antibiotico' >
                        <label>Antibiotico: </label>
                        <select class='{$combo} form-control' id='antibiotico'>
                            <option value='-1'></option>
                            <option value='N' ".(($row['ANTIBIOTICO']=='N')?"selected":"").">N&atilde;o</option>
                            <option value='S' ".(($row['ANTIBIOTICO']=='S')?"selected":"").">Sim</option>
                        </select>
                    </div>
                    <div class='col-md-4'>
                        <label>Status: </label>
                        <select class='COMBO_OBG form-control' id='status'>
                             <option value='-1'></option>
                             <option value='A' ".(($row['ATIVO']=='A')?"selected":"").">Ativo</option>
                             <option value='D' ".(($row['ATIVO']=='D')?"selected":"").">Inativo</option>
                        </select>
                    </div>
                 </div>";

            echo "<br>
                  <div class='alert alert-info'>
                    <strong class='glyphicon-info-sign glyphicon'></strong>
                    Para que o item possa ser adicionado na fatura mesmo depois de <b>Inativo</b>, marque o campo <b>Descontinuado</b> igual a <b>Sim</b>.
                  </div>
                  <div class='row'>
                    <div class='col-md-4'>
                        <label>Descontinuado: </label>
                        <select class='COMBO_OBG form-control'  style='background-color:transparent;' id='descontinuado'>
                             <option value='-1'></option>
                             <option value='N' ".(($row['descontinuado']=='N')?"selected":"").">Não</option>
                             <option value='S' ".(($row['descontinuado']=='S')?"selected":"").">Sim</option>
                        </select>
                    </div>
                    <div class='col-md-4'>
                        <label>Genérico: </label>
                        <select class='{$obg} form-control' $disabled id='generico'>
                             <option value=''></option>
                             <option value='N' ".(($row['GENERICO']=='N')?"selected":"").">Não</option>
                             <option value='S' ".(($row['GENERICO']=='S')?"selected":"").">Sim</option>
                        </select>
                    </div>
                    <div class='col-md-4 possui-generico'>
                        <label>Possui Genérico? </label>
                        <select class='{$obg} form-control' $disabled id='possui-generico'>
                             <option value=''></option>
                             <option value='S' ".(($row['possui_generico']=='S')?"selected":"").">Sim</option>
                             <option value='N' ".(($row['possui_generico']=='N')?"selected":"").">Não</option>
                        </select>
                    </div>
                  </div>";

            if ($row['REFERENCIA'] == null || $row['REFERENCIA'] == ''){
                $disabledRef='';
            }else{
                $disabledRef='disabled';
            }

            echo "<div class='row'>
                    <div class='col-md-12'>
                        <label>Referencia: </label>
                        <select $disabledRef class='COMBO_OBG form-control' id='referencia'>
                             <option value='-1'></option>
                             <option value='S' ".(($row['REFERENCIA']=='S')?"selected":"").">Simpro</option>
                             <option value='T' ".(($row['REFERENCIA']=='T')?"selected":"").">Brasindice</option>
                             <option value='I' ".(($row['REFERENCIA']=='I')?"selected":"").">Produto Interno</option>
                        </select>
                    </div>
                  </div>";

            if($row['tipo'] == 1) {
                $categorias_materiais = \App\Models\Logistica\Catalogo::getCategoriasMateriais();
                echo "<br>
                <div class='panel panel-default'>
                    <div class='panel-heading clearfix'>
                        <h3 class='panel-title pull-left'>
                            <i class='fa fa-list-alt' aria-hidden='true'></i> Categorias utilizadas para referência de materiais nos contratos dos pacientes.</b>
                        </h3>
                    </div>
                    <div class='panel-body'>
                        <div class='row'>
                            <div class='col-md-12'>
                                <label for='categoria-material'>Categoria do Material:</label>
                                <select name='categoria_material' class='form-control' id='categoria-material'>
                                    <option value=''>Selecione uma categoria...</option>";
                foreach ($categorias_materiais as $categorias_material) {
                    echo "          <option " . ($row['CATEGORIA_MATERIAL_ID'] == $categorias_material['ID'] ? 'selected' : '') . " value='{$categorias_material['ID']}'>{$categorias_material['ITEM']}</option>";
                }
                echo "          </select>
                            </div>
                        </div>
                    </div>
                </div>";
            }

            echo "<input type='hidden' type='text' class='numerico' value='{$_SESSION["id_user"]}' id='usuario' />";
            echo "<input type='hidden' type='text' class='numerico' value='{$data}' id='data'/>";

            echo "<br>
                  <div class='row text-center'>
                    <button id='brasindice_editar' class='btn btn-success'>
                        <i class='fa fa-save'></i> Salvar
                    </button>
                  </div>";
        }
        echo "</div>";
        echo "</form>";
	}
	
}

?>