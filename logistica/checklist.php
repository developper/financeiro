<?php
session_start();
include_once('../utils/mpdf/mpdf.php');
include_once('../db/config.php');

class Itens{
    private $tipo = "";
    private $nome = "";
    private $apresentacao = "";
    private $via = "";
    private $aprazamento = "";
    private $obs = "";
    private $dose = "";
    private $freq = "";

    function Itens($t,$n,$a,$v,$apraz,$o,$d,$f){
        $this->tipo = $t;
        $this->nome = $n;
        $this->apresentacao = $a;
        $this->via = $v;
        $this->aprazamento = $apraz;
        $this->obs = $o;
        $this->dose = $d;
        $this->freq = $f;
    }

    public function getAprazamento($dia){
        if(array_key_exists($dia,$this->aprazamento))
            return $this->aprazamento[$dia];
        return "&nbsp;";
    }

    public function getLabel(){
        return "{$this->tipo}{$this->nome} {$this->apresentaca} {$this->dose} {$this->via} {$this->freq} {$this->obs}";
    }
}

$p = $_REQUEST['p'];

$sq = "SELECT 
		      min(p.inicio) as min,
		       max(p.fim) as max 
		FROM 
		       prescricoes AS p
		WHERE 
		       p.paciente = {$p} AND p.fim>=NOW() 
		ORDER BY 
		       p.id DESC LIMIT 1";

$res = mysql_query($sq);
$r = mysql_fetch_array($res);
$inicio = $r['min'];
$fim = $r['max'];

$sql = "SELECT
			s.enfermeiro,						
			u.nome AS profissional,
			s.tipo,
			s.qtd,
			p.inicio,
			p.fim,
			UPPER(c.nome) AS nome,
			CONCAT(c.END_INTERNADO,', ',c.BAI_INTERNADO,', ',cidades.NOME) as endereco,
			c.cuidador,
			c.TEL_CUIDADOR,
			c.relcuid,
			s.autorizado,
			s.enviado,
			s.NUMERO_TISS,
			s.CATALOGO_ID,
			s.idSolicitacoes AS sol,
			s.status,
			DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
			CONCAT(b.principio,' - ',b.apresentacao) AS produto
		FROM
			solicitacoes AS s LEFT JOIN
			prescricoes AS p ON (s.idPrescricao = p.id) INNER JOIN
			catalogo AS b ON (b.ID = s.CATALOGO_ID ) LEFT JOIN
			clientes AS c ON (s.paciente = c.idclientes) INNER JOIN
			usuarios AS u ON (s.enfermeiro = u.idUsuarios) LEFT JOIN
			cidades ON (c.CIDADE_ID_DOM = cidades.id)
		WHERE
			s.paciente = '{$p}' AND						 
			s.tipo = '1' AND			
			(CASE s.idPrescricao WHEN '-1' THEN s.data BETWEEN '{$inicio}' AND '{$fim}' ELSE p.fim >= now()  END)
                GROUP BY
			s.CATALOGO_ID		
                UNION
		SELECT
			s.enfermeiro,
			u.nome AS profissional,
			s.tipo,
			s.qtd,
			p.inicio,
			p.fim,
			UPPER(c.nome) AS nome,
			CONCAT(c.END_INTERNADO,', ',c.BAI_INTERNADO,', ',cidades.NOME) as endereco,
			c.cuidador,
			c.TEL_CUIDADOR,
			c.relcuid,
			s.autorizado,
			s.enviado,
			s.NUMERO_TISS,
			s.CATALOGO_ID,
			s.idSolicitacoes AS sol,
			s.status,
			DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
			CONCAT(b.principio,' - ',b.apresentacao) AS produto
		FROM
			solicitacoes AS s LEFT JOIN
			prescricoes AS p ON (s.idPrescricao = p.id) INNER JOIN
			catalogo AS b ON (s.CATALOGO_ID = b.ID) LEFT JOIN
			clientes AS c ON (s.paciente = c.idclientes) INNER JOIN
			usuarios AS u ON (s.enfermeiro = u.idUsuarios) LEFT JOIN
			cidades ON (c.CIDADE_ID_DOM = cidades.id)
		WHERE
			s.paciente = '{$p}' AND			 			
			s.tipo in(0,3) AND 
			(CASE s.idPrescricao WHEN '-1' THEN s.data BETWEEN '{$inicio}' AND '{$fim}' ELSE p.fim >= now()   END)
		GROUP BY
			s.CATALOGO_ID
		ORDER BY
			tipo,
			produto,
			autorizado,
			enviado";

$sq = "SELECT			
			C.item AS produto,
                        C.id
		FROM
			equipamentosativos AS E INNER JOIN
			cobrancaplanos AS C ON (E.COBRANCA_PLANOS_ID = C.id)
		WHERE 
		    E.PACIENTE_ID = '{$p}' AND 
		    E.DATA_FIM = '0000-00-00 00:00:00'	
                    Group by
                    C.id
		ORDER BY
			item ASC";

$result = mysql_query($sql);
$result1 = mysql_query($sql);
$result2 = mysql_query($sq);

$row = mysql_fetch_array($result);

$paciente = $row['nome'];

$img = "<img src='../utils/logos/logo_assiste_vida.jpg' width='20%' style='align:center' />";
$hoje = date("d/m/Y");
$header = "<table width='100%' style='border:1px solid #000;border-collapse:collapse;'>
	              <thead>
	                  <tr>
	                  	<th rowspan='2' style='float:left;padding:10px;width:20%;'>
	                  		{$img}
	                  	</th>
	                  	<th style='float:left;padding:10px;'>
	                  		<span style='font-size:16px;'>CONTROLE DE ESTOQUE DOMICILIAR DE MAT/MED  </span><br>DE ".join('/',array_reverse(explode("-",$inicio)))." AT&Eacute; ".join('/',array_reverse(explode("-",$fim)))."<br>
	                  		<br><span style='font-size:12px;'>PROFISSIONAL: <i>".$row['profissional']."</i></span>
	                  		<br>
	                  		<br><span style='font-size:14px;'>PACIENTE: <i>".$row['nome']."</i></span>
	                  		<br><span style='font-size:12px;'>" . htmlentities("ENDEREÇO") . ": <i>".$row['endereco']."</i></span>
	                  		<br><span style='font-size:12px;'>CUIDADOR: <i>".$row['cuidador']." TEL: " .$row['TEL_CUIDADOR'] ."</i></span>
	                  	</th>
					  </tr>
					</thead>  
	          </table>";

$flag = true;

$tabela = "<br><table style='border:1px solid #000;border-collapse:collapse;' width='100%' >
           <thead>
             <tr>
               <th ></th>
               <th ></th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
            </tr>
			<tr>
               <th style='border:1px solid #000;'>Ord.</th>
               <th style='border:1px solid #000;'>Itens</th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               
            </tr>
           ";
$tabela .= "<tr>
				<th style='border:1px solid #000;'></th>
				<th style='border:1px solid #000;'></th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
			</tr>
</thead>";
$i = 0;
while($row1 = mysql_fetch_array($result1)){

    $tabela .= "<tr>
                  <td align='center' style='width:5px;border:1px solid #000;'>{$i}</td>
                  <td style='border:1px solid #000;height:20px;'>{$row1['produto']}</td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
              </tr>";
    $i++;

}

$j=0;
while($row2 = mysql_fetch_array($result2)){
    $j++;
    if($j==1){
        $tabela .= "<tr><td colspan='30'><center><b>EQUIPAMENTOS</b></center></td></tr>";
    }
    $tabela .= "<tr>
	<td align='center' style='width:5px;border:1px solid #000;'>{$i}</td>
	<td style='border:1px solid #000;height:20px;'>{$row2['produto']}</td>
	 <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>  
     <td style='border:1px solid #000;' align='center'></td>
     <td style='border:1px solid #000;' align='center'></td>  
     <td style='border:1px solid #000;' align='center'></td>                     
</tr>";
    $i++;
}

/*ANOTA��ES*/
$tabela .= "<tr>
<td align='center' style='width:5px;border:1px solid #000;'>{$i}</td>
<td style='border:1px solid #000; height:20px;'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
</tr>";
$i++;
$tabela .= "<tr>
<td align='center' style='width:5px;border:1px solid #000;'>{$i}</td>
<td style='border:1px solid #000; height:20px;'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
</tr>";
$i++;
$tabela .= "<tr>
<td align='center' style='width:5px;border:1px solid #000;'>{$i}</td>
<td style='border:1px solid #000; height:20px;'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
</tr>";
$i++;
$tabela .= "<tr>
<td align='center' style='width:5px;border:1px solid #000;'>$i</td>
<td style='border:1px solid #000; height:20px;'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
</tr>";
$i++;

$tabela .="</table><br>
<table style='width:100%;border:1px solid #000000;border-collapse:collapse;'>
<tr>
	<td style='border:1px solid #000;height:20px;text-align:center'><b>ANOTA&Ccedil;&Otilde;ES</b></td>
</tr>
<tr>
	<td style='border:1px solid #000;height:25px;'></td>
</tr>
<tr>
	<td style='border:1px solid #000;height:25px;'></td>
</tr>
<tr>
	<td style='border:1px solid #000;height:25px;'></td>
</tr>
<tr>
	<td style='border:1px solid #000;height:25px;'></td>
</tr>
";

$paginas []= $header.$tabela."</table>";

$mpdf=new mPDF('pt','A4-L',9);
$mpdf->SetHeader('página {PAGENO} de {nbpg}');

$ano = date("Y");
// $mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
$mpdf->WriteHTML("<html><body>");
$flag = false;
foreach($paginas as $pag){
    if($flag) $mpdf->WriteHTML("<formfeed>");
    $mpdf->WriteHTML($pag);
    $flag = true;
}
$mpdf->WriteHTML("</body></html>");
$mpdf->Output('prescricao.pdf','I');
exit;
