<?php
include_once('../validar.php');
require_once('medicamento.php');
require_once('material.php');
require_once('dieta.php');
require_once('entrada.php');
require_once('saida.php');
require_once('busca.php');
require_once('inventario.php');
require_once('especiais.php');
require_once('devolucao.php');
require_once('relatorios.php');
require_once('estoque.php');
require_once('brasindice.php');
require_once('saida_interna.php');
require_once('relatorio_diferenca_entrada_saida_ur.php');
require_once('relatorio_lote.php');
require_once('relatorio_equipamento_ativo.php');
require_once ('gerenciar_pedidos.php');
require_once ('relatorio_devolucao.php');
require_once ('DevolucaoEquipamentoAtivo.php');

if(!function_exists('redireciona')){
	function redireciona($link){
		if ($link==-1){
			echo" <script>history.go(-1);</script>";
		}else{
      echo" <script>document.location.href='$link'</script>";
		}
	}
}

include($_SERVER['DOCUMENT_ROOT'].'/cabecalho.php');

/*destroi a sess�o da busca de medico*/
unset($_SESSION['paciente']);
?>

<script type="text/javascript" src="logistica.js?v=33" ></script>

<style>
		body { font-size: 62.5%; }
		label, input { display:block; }
		input.text { margin-bottom:12px; width:95%; padding: .4em; }
		fieldset { padding:0; border:0; margin-top:25px; }
		h1 { font-size: 1.2em; margin: .6em 0; }
		div#users-contain { width: 350px; margin: 20px 0; }
		div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
		div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
		.ui-dialog .ui-state-error { padding: .3em; }
		.validateTips { border: 1px solid transparent; padding: 0.3em; }
		.ui-autocomplete-loading { background: white url('utils/load.gif') right center no-repeat; }
</style>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/cabecalho_fim.php');
if(!validar_tipo("modlog") && !(validar_tipo('modfin') && $_GET['op'] == 'estoque')){
	redireciona('/inicio.php');
}
?>
</div>
<div id="content">
<div id="right">
<?php
$a = 1;
   if(isset($_GET['op'])){
   switch($_GET['op']){
      case "gerenciar-pedidos":
          $gerenciar = new Gerenciar_pedidos;
          $gerenciar-> pesquisar_pedidos();
          break;
       case "visualizar-pedido":
          $g = new Gerenciar_pedidos;
          $g-> visualizar_pedidos($_GET['p']);
          break;
      case "saida-interna":
          $saida_interna = new Saida_interna();
         echo $saida_interna ->form_sinterna();
         break;
      case "med":
	if(isset($_GET['act'])){
	  switch($_GET['act']){
	    case "cadastrar":
	      echo "<center><h1>Medicamentos - Cadastro</h1></center>";
	      $med = new MED;
	      if(isset($_POST['nome']) && $_POST['nome'] <> "" )
		  	$med->cadastrar($_POST['nome'],$_POST['classe']);
	      else $med->cadastrar(NULL,NULL);
	      break;
	    case "editar":
	      echo "<center><h1>Medicamentos - Editar</h1></center>";
	      $med = new MED;
	      if(isset($_POST['nome']) && isset($_POST['classe']) && $_POST['nome'] <> "" && $_POST['classe'] <> "")
		$med->editar($_POST['id'],$_POST['nome'],$_POST['classe']);
	      else $med->editar($_GET['id'],NULL,NULL);
	      break;
	    case "buscar":
	      echo "<center><h1>Medicamentos - Buscar</h1></center>";
	      $med = new MED;
	      if(isset($_POST['nome']))
		$med->buscar($_POST['nome']);
	      else $med->buscar(NULL);
	      break;
	      case "excluir":
	      if(isset($_GET['confirm']) && ($_GET['confirm'] == "ok")){
		$med = new MED;
		$med->excluir($_GET['id']);
	      }
	      break;
	      case "detalhes":
		echo "<center><h1>Medicamentos - Detalhes</h1></center>";
		if(isset($_GET['id'])){
		  $med = new MED;
		  $med->detalhes($_GET['id']);
		}
	      break;
	  }
	} else {
	  echo "<center><h1>Medicamentos</h1></center>";
	  $med = new MED;
	  $med->menu();
	}
      break;
      case "mat":
	if(isset($_GET['act'])){
	  switch($_GET['act']){
	    case "cadastrar":
	      echo "<center><h1>Material - Cadastro</h1></center>";
	      $mat = new MAT;
	      if(isset($_POST['nome']) && isset($_POST['descricao']) && $_POST['nome'] <> "" && $_POST['descricao'] <> "")
		$mat->cadastrar($_POST['nome'],$_POST['descricao'],$_POST['codigobarras']);
	      else $mat->cadastrar(NULL,NULL,NULL);
	      break;
	    case "editar":
	      echo "<center><h1>Material - Editar</h1></center>";
	      $mat = new MAT;
	      if(isset($_POST['nome']) && isset($_POST['descricao']) && $_POST['nome'] <> "" && $_POST['descricao'] <> "")
		$mat->editar($_POST['id'],$_POST['nome'],$_POST['descricao'],$_POST['codigobarras']);
	      else $mat->editar($_GET['id'],NULL,NULL,NULL);
	      break;
	    case "buscar":
	      echo "<center><h1>Material - Buscar</h1></center>";
	      $mat = new MAT;
	      if(isset($_POST['nome']))
		$mat->buscar($_POST['nome']);
	      else $mat->buscar(NULL);
	      break;
	      case "excluir":
	      if(isset($_GET['confirm']) && ($_GET['confirm'] == "ok")){
		$mat = new MAT;
		$mat->excluir($_GET['id']);
	      }
	      break;
	  }
	} else {
	  echo "<center><h1>Material</h1></center>";
	  $mat = new MAT;
	  $mat->menu();
	}
     break;
    case "entrada":
      if(isset($_POST['nota']) && $_POST['nota'] <> ""){
				$entrada = new Entrada;
				$entrada->show_itens($_POST['nota'],$_POST['valor_nota'],$_POST['codFornecedor'],$_POST['descricao'],$_POST['val_produtos'],$_POST['icms'],
						$_POST['frete'],$_POST['seguro'],$_POST['ipi'],$_POST['issqn'],$_POST['des_acessorias'],$_POST['des_bon']);
       } else {
				echo "<center><h1>Entrada em Estoque (Fornecedores)</h1></center>";
				$entrada = new Entrada;
				$entrada->select_nota();
       }
    break;
    case "importar":
    	$importar = new Entrada();	
    	
    	$importar->importar($_FILES);
    break;
    
    case "saida2":
    	/*if(isset($_GET['p'])){    		    		
    		$saida = new Saida;
    		$saida->saida_paciente($_GET['p'],$_GET['tipo']);
    	} else */
    	//if(isset($_GET['act']) && $_GET['act'] == 'pendentes'){
    		$saida = new Saida;
    		$saida->pendentes();
    	//}
    	
    break;
    case"pedido_entrada_estoque":
    	pedido_entrada_estoque($_GET['ur'],$_GET['emp']);
    	break;
    case "saida1":
    	if(isset($_GET['p'])){
    		$saida = new Saida;
    		$saida->saida_paciente($_GET['p'], $_GET['tipo'], $_GET['carater'], $_GET['presc'], $_GET['primeira']);
    	} else if(isset($_GET['act']) && $_GET['act'] == 'pendentes'){
    		$saida = new Saida;
    		$saida->pendentes_separados($_GET['ur']);
    	}
        	break;
    case "pedido":
    	if(isset($_GET['p'])){
    		$saida = new Saida;
    		$saida->pedidos($_GET['p']);
    	}else if(isset($_GET['ur'])){
    		$saida = new Saida;
    		$saida->pedido_saida($_GET['ur'],$_GET['emp']);
    	}else if(isset($_GET['condensadoUr'])){
			$saida = new Saida;
			$saida->pedidoCondensado($_GET['condensadoUr'],$_GET['emp']);
		}
    	break;
    case "buscar":
      if(isset($_GET["act"])){
		if($_GET["act"] == "item" && isset($_POST['nome']) /*&& $_POST['nome'] <> ""*/){
	  		$b = new Buscas;
	  		$b->buscar($_POST['nome']);
		} else if($_GET["act"] == "saida") {
	  	  	$b = new Buscas;
		 	$b->buscar_saida($_POST, $_SESSION['empresa_principal']);
		 	//['paciente'],implode("-",array_reverse(explode("/",$_POST['inicio']))),implode("-",array_reverse(explode("/",$_POST['fim']))));
		} else if($_GET["act"] == "nota" && isset($_POST['numero']) ) {
	  	  	$b = new Buscas;
		 	$b->buscar_nota($_POST['numero'],$_POST['codigo']);
		} else if($_GET["act"] == "rastreamento") {
	  		$b = new Buscas;
	  		$b->rastrear($_POST[''],$_POST['codigo'],$_POST['ientrada'],$_POST['fentrada']);	
		}else if($_GET["act"] == "controlado") {
	  		$b = new Buscas;
	  		$b->controlado_busca($_POST['select-controlado'],$_POST['tipo-controlado'],$_POST['ci-entrada'],$_POST['cf-entrada']);	
		}else if($_GET["act"] == "antibiotico") {
			$b = new Buscas;
			$b->antibiotico_busca($_POST['select-antibiotico'],$_POST['tipo-antibiotico'],$_POST['antibiotico-inicio-entrada'],$_POST['antibiotico-fim-entrada']);
		}else if($_GET["act"] == "dieta") {
	  		$b = new Buscas;
	  		$b->buscar_dieta($_POST['di-entrada'],$_POST['df-entrada']);	
		} else if ($_GET["act"] == "devolucao") {
                        $b = new Buscas;
                        $b->buscar_devolucao($_POST);
                    } else {
	  		$b = new Buscas;
	  		$b->form();
		}
      } else {
		$b = new Buscas;
		$b->form();
      }
      break;
    case "inventario":
      echo "<center><h1>Invent&aacute;rio</h1></center>";
      $inv = new Inventario();
      $inv->form();
      break;
      case "estoque":
      	echo "<center><h1>Estoque</h1></center>";
         estoque();
      	break;
      case "brasindice":
      	
      	if(isset($_GET['act'])){
      		
      		switch($_GET['act']){
				case "buscar":
					echo "<center><h1>Cat&aacute;logo</h1></center>";
					$b = new Brasindice();
					$b->buscar();
					
					break; 
      			case "cadastrar":
      				echo "<center><h1>Atualiza&ccedil;&atilde;o Cat&aacute;logo</h1></center>";
      				$b = new Brasindice();
      				$b->novo();
      				break;
      			case "editar":
      				echo "<center><h1>Atualiza&ccedil;&atilde;o Cat&aacute;logo</h1></center>";
      				$b = new Brasindice();
      				$b->editar();
      				break;      		
      		}
      	}
      	break;
      	
    case "pedido_estoque":
    	pedido_estoque();
    	break;  	
    	case "entrada_estoque":
    		entrada_estoque();
    		break;
    case "especial":
      if(isset($_POST['principio'])){
	$esp = new Especial();
	$esp->salvar($_POST['principio'],implode("-",array_reverse(explode("/",$_POST['vencimento']))),$_POST['especial_lote'],$_POST['especial_qtd']);
      } else if(isset($_POST['show_especial']) && $_POST['show_especial'] <> ""){
	$esp = new Especial();
	$esp->show(implode("-",array_reverse(explode("/",$_POST['show_especial']))));
      } else {
	echo "<center><h1>Estoque especial</h1></center>";
	$esp = new Especial();
	$esp->form();
      }
      break;
    case "devolucao":
      $d = new Devolucao();
      $d->form();
      break;
    case "relatorios":
      $r = new Relatorio();
      $r->menu_relatorios();
      break;
    case "relatorio-entrada-saida":
      $r = new Relatorio_entrada_saida();
      $r->entrada_saida();
      break;
    case "relatorio-devolucao":
      $r = new RelatorioDevolucao;
      $r->relatorio_devolucao();
      break;
   
   case "relatorio-lote":
      $r = new Relatorio_lote();
      $r->rel_lote();
      break;
   case "relatorio-equipamento-ativo":
      $r = new Relatorio_equipamento_ativo();
      $r->rel_equipamento_ativo();
      break;
     case "devolucao-equipamento-ativo":

       $d = new DevolucaoEquipamentoAtivo();
       $d->devolverEequipamentoAtivo();
       break;
   }
} else include('content.php');
?>
</div>
<div id="left">
<?php include($_SERVER['DOCUMENT_ROOT'].'/painel_login.php'); ?>
<!-- <div class="box">
  <ul>
   
   <li><a href='?op=saida2&act=pendentes'>Solicita&ccedil;&otilde;es Pendentes</a></li>
   <li><a href='?op=entrada'>Entrada em Estoque (Fornecedores)</a></li>
   <li><a href='?op=saida-interna'> Sa&iacute;da Interna</a>
   <li><a href='?op=buscar'>Pesquisar Movimenta&ccedil;&otilde;es</a></li>
   <li><a href='../medico/?view=buscar'>Buscar Prescri&ccedil;&otilde;es</a></li>
 
   <li><a href='?op=devolucao'>Devolu&ccedil;&atilde;o</a></li>
    <li><a href=?op=devolucao-equipamento-ativo>Devolu&ccedil;&atilde;o Equipamento Ativo</a></li>
   
   <li><a href='?op=estoque'>Estoque</a></li>
   <?php if($_SESSION['empresa_principal'] == 1 ||  $_SESSION["id_user"] == 168){?>
   <li><a href='?op=brasindice&act=buscar'>Cat&aacute;logo</a></li>
   <?php } ?> 
   <li><a href='?op=relatorios'>Relat&oacute;rio</a></li>
   <?php if($_SESSION['empresa_principal'] !=1){?>
   <li><a href='?op=pedido_estoque'>Estoque Compras</a>
   <li><a href='?op=entrada_estoque'>Entrada em Estoque (CDM)</a>
   <?php }?>
  </ul>
</div>
<div class="box">
<div id='calculadora'></div>
</div> -->
</div>
</div>
</body>
</html>
