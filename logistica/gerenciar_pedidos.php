<?php
$id = session_id();
if(empty($id))
	session_start();

include_once('../db/config.php');
include_once('../utils/codigos.php');
include('saida_interna');
class Gerenciar_pedidos{
    
    public function pesquisar_pedidos(){
        $empresa = new Saida_interna();
      $unidade_regional = $empresa->empresa();
        
      echo "<div id='div-pesquisar-pedidos'>
               <center><h1>Gerenciar Pedidos</h1></center>
              {$unidade_regional} 
         <fieldset style='width:550px;'>
  	 <legend><b>Per&iacute;odo</b></legend>
         DE
         <input type='text' name='inicio' value='' maxlength='10' size='15' class='OBG data periodo' style='display:inline;'/>
         AT&Eacute; <input type='text' name='fim' value='' maxlength='10' size='15' class='OBG data' style='display:inline;'/>
  	 </fieldset>
         </br>
         <button id='pesquisar-gerenciar-pedidos' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'  aria-disabled='false' >
         <span class='ui-button-text'>Pesquisar</span></button>
         </div>";
     echo "</br>
         <div id='resultado-pesquisa-gerenciar-pedidos'>
         </div>";
              
        
    }
    
    public function visualizar_pedidos($p,$emp){
       echo "<center><h1>Visualizar o pedido N&deg; {$p}</h1></center>";
       echo "<div id='cancelar-item-pedido' title='Cancelar item' id_item='' >";
      echo alerta();
      echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar o cancelamento, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
      echo "<p><b>Justificativa:</b><input type='text' name='justificativa' class='OBG' size='100' /></div>";
       echo "<div id='visualizar-pedido-gerenciar'>";
       $sql = "SELECT
                E.nome as nome_empresa,
                DATE_FORMAT(p.DATA, '%d/%m/%Y %H:%i:%s') as dat,
                U.nome as usuario,
                DATE_FORMAT(p.DATA_MAX_ENTREGA, '%d/%m/%Y' ) as dat_entrega,
                (Case 
                  WHEN p.STATUS = 'A' THEN 'Aberto'
                  WHEN p.STATUS = 'P' THEN 'Pendente'
                  WHEN p.STATUS = 'F' THEN 'Finalizado'
                  END) as st,
                (Case 
                  WHEN p.CONFIRMADO= 'S' THEN 'Sim'
                  WHEN p.CONFIRMADO = 'N' THEN 'Não'
                 END) as confirm,
                 p.CONFIRMADO,
                 p.EMPRESA,
                 DATE_FORMAT(p.DATA_CONFIRMADO, '%d/%m/%Y %H:%i:%s') as dat_confirmado,
                (Select nome from usuarios where idUsuarios = p.CONFIRMADO_POR) as usuario_confirmado,
                p.STATUS,
                p.JUSTIFICATIVA,
                p.OBSERVACAO,
                DATE_FORMAT(p.DATA_CANCELADO, '%d/%m/%Y %H:%i:%s') as dat_cancelado,
                (Select nome from usuarios where idUsuarios = p.CANCELADO_POR) as usuario_cancelou
  	     FROM
  		
  		pedidosinterno AS p 
  		inner join  usuarios AS U on (U.idUsuarios = p.USUARIO)
  		inner join empresas AS E on (p.EMPRESA = E.id)
  		WHERE 
                p.ID ={$p} 
                ";
                $r= mysql_query($sql);
                while($row = mysql_fetch_array($r)){
                     $status = $row['STATUS'];
                    echo "                       
                          <table  width=100% style='border:2px dashed;' >
                                <tr><td> <b>Criador:</b> {$row['usuario']}</td>
                                    <td> <b>Empresa:</b> {$row['nome_empresa']}</td>
                                    <td> <b>Data:</b> {$row['dat']}  <b>Data maxima para entrega:</b> {$row['dat_entrega']}</td>
                                </tr>
                                ";
                      if($status == 'C'){              
                           echo "<tr bgcolor='#EEEEEE'><td><b>Pedido Cancelado</td>
                                    <td><b>Cancelado por :</b> {$row['usuario_cancelou']}</td>
                                    <td><b>Data cancelado:</b> {$row['dat_cancelado']}</td>
                                    <td></td>
                                </tr>
                                <tr> 
                                    <td colspan='3'><b>Justificativa:</b> {$row['JUSTIFICATIVA']}</td>  
                                </tr>";
                      }else{
                          echo "<tr bgcolor='#EEEEEE'><td><b>Confirmado:</b> {$row['confirm']}</td>
                                    <td><b>Confirmado por :</b> {$row['usuario_confirmado']}</td>
                                    <td><b>Data confirmado:</b> {$row['dat_confirmado']}</td>
                                    <td></td>
                                </tr>
                                <tr> 
                                    <td colspan='3'>OBS: {$row['OBSERVACAO']}</td>  
                                </tr>";
                      }
                         echo" </table>";
                    $confirmado=$row['CONFIRMADO'];
                    $empresa = $row['EMPRESA'];
                   
                }
                if($confirmado == 'N' && $status != 'C' && ($empresa == $_SESSION['empresa_principal'] || $_SESSION['adm_user'] == 1 )){
               echo "<p><label for='excluir-total' style='display:inline;'>
                       <input type='checkbox' style='display:inline;' id='excluir-total' /><b>Cancelar todo o Pedido.</b>
                       </label>
                   </p>
                    <p><span id='span-justificatica-excluir-total' style='display:none'><b>Justificativa:</b>
                       </br>
                       <div id ='div-justificativa-excluir-total'>
                       <textarea rowls='30' cols='30' id='text-justificatica-excluir-total' style='display:none'></textarea>
                      </div>
                    </p>
                    <button id='cancelar-pedido-total' style='display:none' id_pedido='$p' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'  aria-disabled='false' >
                  <span class='ui-button-text'>Cancelar</span>
                  </button>
                       </span>";
               
                }
                echo "</br>
                      </br>
                      <table class='mytable' width=100% id='gerenciar-itens-pedido' >
                            <thead>
                              <tr>
                                <th>Itens Solicitados</th>
                                <th>Pedido</th>
                                <th>Enviado</th>
                                <th>Confirmado</th>
  			     </tr>
                            </thead>";
                
                
              $sqlmed = "SELECT
                        i.QTD , 
                        i.ENVIADO ,
                        i.CONFIRMADO,
                        i.ID,
                        i.PEDIDOS_INTERNO_ID, 
			i.NUMERO_TISS,
			i.CATALOGO_ID,
  	  		b.principio,
  	  		b.apresentacao,
  	  		i.EMERGENCIA,
             i.CANCELADO,
             i.STATUS,
             i.JUSTIFICATIVA
  	
  	  		FROM
  	  		itenspedidosinterno AS i INNER JOIN
  	  		pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
  	  		INNER JOIN catalogo AS b ON ( b.ID = i.CATALOGO_ID )
  	  		WHERE  
                        i.TIPO = 0 and 
  	  		p.ID = {$p} 
  	  		
  	  		 ORDER BY principio, apresentacao";
  	  		$rmed = mysql_query($sqlmed);
  	  		while($row = mysql_fetch_array($rmed)){
                        if ($confirmado =='N' && $row['CANCELADO'] != 'S' && $status != 'C' && $row['status'] != 'T' ){
  	  		    $cancela = "<img align='right' id_item='{$row['ID']}' class='cancela-item-pedido' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
                        }
                $info = "";
                if($row['STATUS'] == 'T'){
                    $info =  "<img src='../../utils/details.png' width='16' height='16' title='{$row['JUSTIFICATIVA']}' />";

                }


                        
  	  		
  	              if($row["EMERGENCIA"] == 1){
  	              	$cor = "style='color:red;'";
  	              }else{
  	              	$cor ='';
  	              }
  	  		echo "<tr $cor > 
                             <td>$info <b>MEDICAMENTO: </b>{$row['principio']} {$row['apresentacao']}  $cancela </td>
                             <td align='center'>{$row['QTD']}</td>
                              <td align='center'>{$row['ENVIADO']}</td>
                              <td class='center'>{$row['CONFIRMADO']}</td>
  	  		</tr>";
  	  		}
  	  			
  	  		$sqldie= "SELECT
                        i.QTD , 
                        i.ENVIADO ,
                        i.ID,
                        i.CONFIRMADO,                        
                        i.PEDIDOS_INTERNO_ID,
			 i.PEDIDOS_INTERNO_ID, 
			i.NUMERO_TISS,
			i.CATALOGO_ID,
  	  		b.principio,
  	  		b.apresentacao,
  	  		i.EMERGENCIA,
                        i.CANCELADO
  	
  	  		FROM
  	  		itenspedidosinterno AS i INNER JOIN
  	  		pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
  	  		INNER JOIN catalogo AS b ON ( b.ID = i.CATALOGO_ID )
  	  		WHERE 
                        i.TIPO = 3 and 
  	  		p.ID = {$p} 
  	  	    ORDER BY principio, apresentacao";
  	
  	
  	  	$rdie = mysql_query($sqldie);
  	  	while($row = mysql_fetch_array($rdie)){
  	  		if ($confirmado =='N' && $row['CANCELADO'] != 'S' && $status != 'C' && $status != 'T'){
  	  		 $cancela = "<img align='right' id_item='{$row['ID']}' class='cancela-item-pedido' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
                        }
  	  		
  	              if($row["EMERGENCIA"] == 1){
  	              	$cor = "style='color:red;'";
  	              }else{
  	              	$cor ='';
  	              }
  	  		echo "<tr $cor > 
                              <td><b>DIETA: </b>{$row['principio']} {$row['apresentacao']}  $cancela</td>
                              <td align='center'>{$row['QTD']}</td>
                              <td align='center'>{$row['ENVIADO']}</td>
                              <td class='center'>{$row['CONFIRMADO']}</td>
  	  		     </tr>";
  	  		}
  	  			
  	  		
  	
  	  		$sqlmat= "SELECT
                        i.QTD , 
                        i.ENVIADO ,
                        i.CONFIRMADO, 
                        i.ID,
                        i.PEDIDOS_INTERNO_ID,
			i.PEDIDOS_INTERNO_ID, 
			i.NUMERO_TISS,
			i.CATALOGO_ID,
  	  		b.principio,
  	  		b.apresentacao,
  	  		i.EMERGENCIA,
                        i.CANCELADO
  	
  	  		FROM
  	  		itenspedidosinterno AS i INNER JOIN
  	  		pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
  	  		INNER JOIN catalogo AS b ON ( b.ID = i.CATALOGO_ID )
  	  		WHERE   	  		
  	                i.TIPO = 1  and
  	  		p.ID = {$p} 
  	  	    ORDER BY principio, apresentacao";
  	
  	  	$rmat = mysql_query($sqlmat);
  	  	while($row = mysql_fetch_array($rmat)){
  	  	  if ($confirmado =='N' && $row['CANCELADO'] != 'S' && $status != 'C' && $status != 'T'){
  	  		    $cancela = "<img align='right' id_item='{$row['ID']}' class='cancela-item-pedido' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
                        }
  	  		
                            if($row["EMERGENCIA"] == 1){
                              $cor = "style='color:red;'";
                            }else{
                              $cor ='';
                            }
  	  		echo "<tr $cor > 
                              <td><b>MATERIAL: </b>{$row['principio']} {$row['apresentacao']}  $cancela</td>
                              <td align='center'>{$row['QTD']}</td>
                              <td align='center'>{$row['ENVIADO']}</td>
                              <td class='center'>{$row['CONFIRMADO']}</td>
  	  		     </tr>";
  	  				}
  	  				
  	  	/*			
  $sqlformula = "SELECT s.enfermeiro, (CASE WHEN COUNT(*)>1 THEN SUM(s.enviado) ELSE s.enviado END) AS enviados,
   				sum((CASE s.status when 2 THEN  s.qtd ELSE  s.autorizado  END)) as autorizado,
   				s.enviado, s.cod, s.idSolicitacoes as sol,
   				s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data,
   				(SELECT
   				(CASE WHEN COUNT( * ) >1 THEN SUM( i.QTD ) ELSE i.QTD END) - (CASE WHEN COUNT( * ) >1 THEN sum( i.ENVIADO ) ELSE i.ENVIADO END) AS
  	  		  dif_pedido
  	
  	  		  FROM
  	  		  itenspedidosinterno AS i INNER JOIN
  	  		  pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
  	  		  WHERE (
  	  		  i.STATUS = 'A'	OR
  	  		  (i.QTD - i.ENVIADO) >0)	AND
  	  		  i.NUMERO_TISS = s.cod AND
  	
  	  		  i.TIPO = s.tipo AND
  	  		  i.NUMERO_TISS = s.cod AND
  	  		  p.EMPRESA = {$p}
  	  		  GROUP BY
  	  		  i.NUMERO_TISS) as dif_pedido,i.NUMERO_TISS,
  	  		  f.formula,
  	  		  i.EMERGENCIA
  	  		  FROM solicitacoes as s,
  	  		  formulas as f,
  	  		  clientes as c
  	  		  WHERE {$opcao} = '$p' AND s.paciente = c.idClientes AND s.tipo = '12' AND
  	  		  (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END)
  	  		  AND (s.status = '1' OR s.status = '2') and i.CANCELADO = 'N' and i.CANCELADO_ENTRADA = 'N'
  	  		  AND f.id = s.cod GROUP BY cod ORDER BY formula,enviado";
  	  		  $rformula = mysql_query($sqlformula);
  	  		  while($row = mysql_fetch_array($rformula)){
  	  		  foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
  	  		  $dados = "autorizado='{$row['autorizado']}' qtd='{$qtd}' sol='{$row['sol']}' cod='{$row['cod']}' n='{$row['formula']}' sn='' soldata='{$row['data']}' tiss='{$row['NUMERO_TISS']}' env='{$row['enviados']}' total='{$row['QTD']}' tipo='12'";
  	  		  $cancela = "<img align='right' id_item='{$row['ID']}' class='cancela-item-pedido' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
  	  		  //$pedidos = $row['autorizado'] - $row['enviados'];
  	  		  if ($row['dif_pedido'] == null || $row['dif_pedido'] == NULL){
  	  		  $dif = 0;
  	  		  	
  	  		  }else{
  	  		  $dif= $row['dif_pedido'];
  	  		  }
  	  		  $pedidos = ($row['autorizado'] - $row['enviados']) -$dif;
  	  		  if($row["EMERGENCIA"] == 1){
  	              	$cor = "style='color:red;'";
  	              }else{
  	              	$cor ='';
  	              }
  	  		  	echo "<tr $cor ><td>{$row['formula']} {$cancela}</td>";
  	  		  	echo "<td align='center'>{$row['ENVIADO']}/{$row['QTD']}</td><td class='dados' $dados qtd={$qtd} >
  	  		  			<input type='text' name='qtd-envio' class='numerico quantidade' style='text-align:right' size='4' value=''/>
  	  		  			<td><input type='text' name='lote'  style='text-align:right' size='4'/></td>
  	  		  	</td></tr>";
  	}*/
  	///jeferson 2012-10-24
  	$sqleqp = 
  	"SELECT
  	i.QTD ,
  	i.ENVIADO ,
        i.CONFIRMADO,
  	i.ID,
  	i.PEDIDOS_INTERNO_ID,
  	cb.item ,
	 i.PEDIDOS_INTERNO_ID, 
	i.NUMERO_TISS,
	i.CATALOGO_ID,
  	i.EMERGENCIA,
        i.CANCELADO
  	
  	FROM
  	itenspedidosinterno AS i INNER JOIN
  	pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
  	INNER JOIN cobrancaplanos as cb ON ( cb.id = i.CATALOGO_ID )
  	WHERE 
  	i.TIPO = 5 and 
  	p.ID = {$p} 
  	ORDER BY cb.item";
  	
  	$reqp = mysql_query($sqleqp);
  	while($row = mysql_fetch_array($reqp)){
  	             if ($confirmado =='N' && $row['CANCELADO'] != 'S' && $status != 'C' && $status != 'T'){
  	  		    $cancela = "<img align='right' id_item='{$row['ID']}' class='cancela-item-pedido' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
                        }
  	  		
                            if($row["EMERGENCIA"] == 1){
                              $cor = "style='color:red;'";
                            }else{
                              $cor ='';
                            }
  	  		echo "<tr $cor > 
                              <td><b>EQUIPAMENTO: </b>{$row['item']}  $cancela</td>
                              <td align='center'>{$row['QTD']}</td>
                              <td align='center'>{$row['ENVIADO']}</td>
                              <td class='center'>{$row['CONFIRMADO']}</td>
  	  		     </tr>";
            
        }
  	  		  	///jeferson 2012-10-24 fim
  	  		 		
  	  		  	echo "</table><br/>";
        echo "</div>";
        
        if($confirmado == 'N' && $status != 'C' &&($empresa == $_SESSION['empresa_principal'] || $_SESSION['adm_user'] == 1 )){
            
            echo "<button id='confirmar-pedido' id_pedido='$p' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'  aria-disabled='false' >
                  <span class='ui-button-text'>Confirmar Pedido</span>
                  </button>
                  ";
            
        }
    }
}
?>
