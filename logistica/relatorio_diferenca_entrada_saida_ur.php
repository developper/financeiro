<?php
$id = session_id();
if(empty($id))
  session_start();
require_once('../db/config.php');
require_once('../utils/codigos.php');
require_once('query.php');

///////Relatório que vai verificar a diferença dos itens comprados pela UR na central com os itens que foi dado saída para os pacientes dessa UR.
class Relatorio_entrada_saida{
    public function entrada_saida(){
       echo "<div>";
       echo "<h1><center>Relat&oacute;rio Entrada-Saida UR</center></h1>";
       echo "<p><b>Empresa:</b></p>";
       combo_busca_saida('ur');
       echo "<fieldset style='border: 1px solid; font-size: 14px;width:30%;' >";
       echo "<legend><b>Per&iacute;odo</b></legend>";
       echo "<table style='margin:5 5 25px;width:30%;'>";
       echo "<tbody>";
       echo "<tr>";
       echo "<td><b>In&iacute;cio</b> </td><td>".input('text','inicio','fim',"class='data' size='10' maxlength='10' ")."</td>". 
    	"<td><b>Fim</b> </td><td>".input('text','fim','inicio',"class='data' size='10' maxlength='10' ")."</td>";
       echo "</tbody></table>";
       echo "</fieldset><br>";
       echo "<p><button type='submit' id='pesquisa-entrada-saida'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Pesquisar</span></button>";
       echo " </div>"; 
       echo "<div id='tabela-entrada-saida'>";
       echo"</div>";
    }
}
?>
