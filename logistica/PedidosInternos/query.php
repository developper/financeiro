<?php
$id = session_id();
if(empty($id))
    session_start();

include_once ("db/config.php");

function avisos() {
    if(isset($_SESSION["avisos"]) && $_SESSION["avisos"] == true){
        $sql = "SELECT dia, info FROM atualizacoes WHERE NOW() < ADDDATE(dia,7)";
        $rs = mysql_query($sql);
        if(mysql_num_rows($rs) > 0){
            echo "<ul>";
            while($row = mysql_fetch_array($rs)){
                foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
                echo "<li>".implode("/",array_reverse(explode("-",$row['dia'])))." - {$row['info']}</li>";
            }
            echo "</ul>";
        } else {
            echo "n";
        }
        $_SESSION["avisos"] = false;
    } else {
        echo "n";
    }
}