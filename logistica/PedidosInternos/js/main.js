$(function($){
    $(".chosen-select").chosen({search_contains: true});
    //function validar_busca(){
    //    if(validar_campos('busca')){
    //        return true;
    //    }else{
    //        return false;
    //    }
    //
    //
    //}

    $("#buscar-pedidos-enviados-nao-confirmados").click(function(){
       console.log(validar_campos('busca'));
        if(!validar_campos('busca'))
            return false;


    });

    $("#cancelar-item-enviado-nao-confirmado").click(function(){
        var aux = 0;
        $(".max-value").each(function(){
            vm = $(this);
            var valor = parseFloat(vm.val());
            if(valor > 0){
               aux++;
            };
        });

        if(aux == 0){
            alert("Todos os itens estão zerados, favor informar ao menos um valor maior que zero.");
            return false;
        }
        return true;



    });

    $(".max-value").blur(function(){
        vm = $(this);
        var valorMax = parseFloat(vm.attr('max'));
        var valor = parseFloat(vm.val());
        console.log(valor,valorMax);

        if(valor > valorMax){
            $(this).val(0);
            $(this).focus();
            alert("Você só pode cancelar "+ valorMax+" desse item .");

        };
        return false;


    });


    function validar_campos(div){
        var ok = true;
        $("#"+div+" .OBG").each(function (i){

            if(this.value == ""){
                ok = false;
                this.style.border = "3px solid red";
            } else {
                this.style.border = "";
            }

        });

        $("#"+div+" .COMBO_OBG option:selected").each(function (i){
            if(this.value == "-1"){
                ok = false;
                $(this).parent().css({"border":"3px solid red"});
            } else {
                $(this).parent().css({"border":""});
            }
        });

        $("#"+div+" .OBG_CHOSEN option:selected").each(function (i){

            if(this.value == "" || this.value == -1){
                ok = false;
                $(this).parent().parent().children('div').css({"border":"3px solid red"});
            } else {

                var estilo = $(this).parent().parent().children('div').attr('style');
                $(this).parent().parent().children('div').removeAttr('style');
                $(this).parent().parent().children('div').attr('style',estilo.replace("border: 3px solid red",''));
            }
        });

        if(!ok){
            $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
            $("#"+div+" .div-aviso").show();
        } else {
            $("#"+div+" .aviso").text("");
            $("#"+div+" .div-aviso").hide();
        }
        return ok;
    }




});













