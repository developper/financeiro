<?php
$id = session_id();
if(empty($id))
    session_start();
require_once('../db/config.php');
require_once('../utils/codigos.php');
require_once('query.php');

///////Relatório equipamentos ativos.
class Relatorio_equipamento_ativo{
    public function rel_equipamento_ativo(){
        echo "<div id='div-paciente-equipamento-ativo'>";
        echo "<h1><center>Relat&oacute;rio Equipamentos Ativos  </center></h1>
              </br>
              <div style='border:2px dashed;padding: 6px'>
                <b>".htmlentities('Com esta funcionalidade,
                                    você pode desativar equipamentos com status de "ativo",
                                     porém, ele precisa já ter sido devolvido no Sistema.')."
                </b>
              </div>
             </br>";
        echo "<div id='bloco1' style='position:relative; float:left; width:100%; display: table;'>
              <label style='display: inline;'>
                <input style='display: inline;' type='radio' name='filtro_relatorio' id='filtro-relatorio-0' class='filtro-rel-equip-ativo' checked value='p'> Por Paciente
              </label>
              <label style='display: inline;'>
                <input style='display: inline;' type='radio' name='filtro_relatorio' id='filtro-relatorio-1' class='filtro-rel-equip-ativo' value='c'> Por Convênio
              </label>
              </div>
              <br>";
        echo "<div id='rel-equip-ativo-por-paciente'>
                <p>
                    <label><b>Paciente:</b></label>
                </p>";
        $this->paciente_equipamento();
        echo"</div> 
             <div id='rel-equip-ativo-por-convenio'>
                <p>
                    <label><b>Convênio:</b></label>
                </p>";
        $this->convenio_equipamento();
        echo '</div>';

        echo"<br/><br/><button id='pesquisa-equipamento-ativo' data-tipo='relatorio' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button' style='float:left;' type='button'>
              <span class='ui-button-text'>Pesquisar</span>
              </button>";
        echo " </div><br/><br/>";
        echo "<div id='tabela-equipamento-ativo'>";
        echo"</div>";
    }

    public function paciente_equipamento(){
        if($_SESSION['empresa_principal'] == 1){
            $cond_paciente = "";
        }else{
            $cond_paciente ="and c.empresa ={$_SESSION['empresa_principal']} ";
        }
        $result = mysql_query("
        SELECT
                 c.nome as nomeCliente,
                 c.idClientes,
                 c.codigo,
                 e.nome as ur
        FROM
                clientes AS c
        INNER JOIN `equipamentosativos` AS ea ON (c.idClientes = ea.PACIENTE_ID)
        LEFT JOIN empresas AS e ON (c.empresa = e.id)
        WHERE
                `DATA_FIM` = '0000-00-00'
                AND `PRESCRICAO_AVALIACAO` <> 's'
                AND `PACIENTE_ID` = c.idClientes
                AND `DATA_INICIO` <> '0000-00-00'
                $cond_paciente 
        GROUP BY
                c.idClientes
        ORDER BY
                c.nome;");
        echo "<p><select id='paciente-equipamento' class='pacientes' style='background-color:transparent;' >";
        echo "<option value='-1' selected >SELECIONE UM PACIENTE... </option>";
        $pacientes = [];
        while($row = mysql_fetch_array($result))
        {
            $pacientes[$row['ur']][$row['idClientes']] = '(' . $row['codigo'] . ') ' . mb_strtoupper($row['nomeCliente'], 'UTF8');
        }
        foreach ($pacientes as $ur => $paciente) {
            echo "<optgroup label='{$ur}'>";
            foreach ($paciente as $id => $nome) {
                echo "<option value='{$id}'>{$nome}</option>";
            }
            echo "</optgroup>";
        }
        echo "</select>";
    }

    public function convenio_equipamento(){
        if($_SESSION['empresa_principal'] == 1){
            $cond_paciente = "";
        }else{
            $cond_paciente ="and c.empresa = '{$_SESSION['empresa_principal']}' ";
        }

        $result = mysql_query("
        SELECT
             ps.id,
             ps.nome
        FROM
          planosdesaude AS ps    
        INNER JOIN clientes AS c ON c.convenio = ps.id
        INNER JOIN `equipamentosativos` AS ea ON (c.idClientes = ea.PACIENTE_ID)
        LEFT JOIN empresas AS e ON (c.empresa = e.id)
        WHERE
            `DATA_FIM` = '0000-00-00'
            AND `PRESCRICAO_AVALIACAO` <> 's'
            AND `PACIENTE_ID` = c.idClientes
            AND `DATA_INICIO` <> '0000-00-00'
            {$cond_paciente} 
        GROUP BY
                c.idClientes
        ORDER BY
                c.nome;");
        echo "<p><select id='convenio-equipamento' class='convenios' style='background-color:transparent;' >";
        echo "<option value='-1' selected >SELECIONE UM CONVENIO... </option>";
        $convenios = [];
        while($row = mysql_fetch_array($result))
        {
            $convenios[$row['id']] = mb_strtoupper($row['nome'], 'UTF8');
        }
        foreach ($convenios as $id => $convenio) {
            echo "<option value='{$id}'>{$convenio}</option>";
        }
        echo "</select>";
    }
}
