<?php
$id = session_id();
if(empty($id))
  session_start();
include_once('../validar.php');
require_once('../db/config.php');
require_once('../utils/codigos.php');
include_once('../utils/mpdf/mpdf.php');

use App\Models\Logistica\Saidas;
use App\Models\Administracao\Filial;
use App\Controllers\SendGrid;

function pes_estoque($ur,$cod,$todos,$tipo,$maiorQueZero)
{

    if (isset($ur) && !empty($ur)) {
        $condur = 'e.empresas_id_' . $ur;
        $qtd = 'empresas_id_' . $ur;
        $qtd_min = 'QTD_MIN_' . $ur;

    } else {
        echo 'Erro falta a Unidade Regional. Entre em contato com o TI.';
        return;
    }

    $condMaiorQueZero = $maiorQueZero == 'N' ? '' : "and {$condur} > 0";

// 479 Fabio Roberto Compras.
    // 478 Marcio  supervisor logistica
    if ($_SESSION['id_user'] == '478' || $_SESSION['id_user'] == '479' || $_SESSION['adm_user'] == '1') {
        $enabled = "";
    } else {
        $enabled = "disabled=disabled";
    }
    $enableQuantidades ='';

    if($_SESSION["modlog"] != 1 && $_SESSION['adm_user'] != '1'){
        $enableQuantidades = "disabled=disabled";
    }
	
	if($todos == 0){
		$cond = " and concat(b.ID, ' - ', b.principio,' ',b.apresentacao)  like'%{$cod}%'";
	}else{
		$cond='';
	}
	if($tipo == 't'){
		$sql= "select 
                       e.*,
                      {$condur},
                      concat(b.ID, ' - ', b.principio,' ',b.apresentacao) as name,
                      b.valor as custo
                      ,b.lab_desc as lab,
                      b.principio 
                      from
                       estoque  as e inner join
                      catalogo as b on(e.CATALOGO_ID = b.ID)



        where e.TIPO in(0,1,3) and b.ATIVO = 'A' {$condMaiorQueZero}
		     union
		     select
                     e.*,
                     {$condur},
                     C.item as name,
                     C.CUSTO as custo,
                     '' as lab,
                     C.item as principio
                     from 
                     estoque as e inner join 
                     cobrancaplanos as C on(e.CATALOGO_ID = C.id) 
                     where 
                     e.TIPO = 5 {$condMaiorQueZero}
                     order by principio ASC";

	}else{
	if($tipo == 'mat'){
	$t=1;
	$sql= "select 
            e.*,{$condur},
                concat(b.ID, ' - ', b.principio,' ',b.apresentacao)as name ,
                      b.valor as custo
                      ,b.lab_desc as lab
                from 
                estoque as e inner join
                catalogo as b on(e.CATALOGO_ID = b.ID) 
                where  e.TIPO = {$t} {$cond} and b.ATIVO = 'A' {$condMaiorQueZero}
                order by b.principio ASC";
		
		
	}
	if($tipo == 'med'){
        if($todos == 0){
            $cond = " and concat(b.ID, ' - ', b.principio,' ',b.apresentacao,' ',principio_ativo.principio )  like'%{$cod}%'";
        }
	$t=0;
		$sql= "select 
                    e.*,{$condur},
                        concat(b.ID, ' - ', b.principio,' ',b.apresentacao) as name,
                        principio_ativo.principio as principioAtivo,
                      b.valor as custo
                      ,b.lab_desc as lab
                        from
                        estoque as e inner join
                        catalogo as b on(e.CATALOGO_ID = b.ID) 
                        LEFT JOIN catalogo_principio_ativo ON b.ID = catalogo_principio_ativo.catalogo_id
                        LEFT JOIN principio_ativo ON principio_ativo.id = catalogo_principio_ativo.principio_ativo_id
                        where  e.TIPO = {$t} {$cond} and b.ATIVO = 'A' {$condMaiorQueZero} order by principioAtivo ASC";
			
		}
		if($tipo == 'medcontrolado'){
            if($todos == 0){
                $cond = " and concat(b.ID, ' - ', b.principio,' ',b.apresentacao,' ',principio_ativo.principio )  like'%{$cod}%'";
            }
            $t=0;
		$sql= "select 
                    e.*,{$condur},
                        concat(b.ID, ' - ', b.principio,' ',b.apresentacao) as name ,
                        principio_ativo.principio as principioAtivo,
                      b.valor as custo, b.lab_desc as lab
                        from 
                        estoque as e inner join 
                        catalogo as b on(e.CATALOGO_ID = b.ID)
                        LEFT JOIN catalogo_principio_ativo ON b.ID = catalogo_principio_ativo.catalogo_id
                        Left JOIN principio_ativo ON principio_ativo.id = catalogo_principio_ativo.principio_ativo_id
                        where  
                        b.CONTROLADO = 'S' and 
                        e.TIPO = {$t} {$cond} and b.ATIVO = 'A' {$condMaiorQueZero} order by principioAtivo ASC";
			
		}
		
		if($tipo == 'die'){
		$t=3;
		$sql= "select e.*,{$condur},
                   concat(b.ID, ' - ', b.principio,' ',b.apresentacao) as name ,
                      b.valor as custo ,b.lab_desc as lab
                    from
                    estoque as e inner join
                    catalogo as b on(e.CATALOGO_ID = b.ID)
                    where  
                    e.TIPO = {$t} {$cond} and b.ATIVO = 'A' {$condMaiorQueZero} order by b.principio ASC";
			
		}
		if($tipo =='eqp'){
		$t=5;
		if($cond == ''){
			$cond= 'and C.tipo = 1';
		}
		
		$sql= "select 
                    e.*,{$condur},
                    C.item as name,
                     C.CUSTO as custo 
                    from 
                    estoque as e inner join 
                    cobrancaplanos as C on(e.CATALOGO_ID = C.id)
                    where 
                    e.TIPO = {$t} {$cond} {$condMaiorQueZero}
                    order by name ASC";
				
		}

		}
		
		$result = mysql_query($sql);

		$resposta = mysql_num_rows($result);



    $t1 = '';
		if($resposta > 0){

		$t1 .= "
<br>
            <button id='exportar-excel-estoque' class='ui-button
                    ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'
                      role='button' aria-disabled='false'
                       data-ur='$ur' data-tipo='$tipo' data-cod='$cod'
                       data-todos='$todos' data-maiorQueZero ='$maiorQueZero'
                        >
                      <span class='ui-button-text'>
                         Exportar Excel
                      </span>
            </button>
            <table id='res-b'  class='mytable' width=100% ><thead>
		<tr>
		<th>Item</th>";
        if($tipo == 'medcontrolado' || $tipo == 'med'){
            $labelPrincipio = "Princípio Ativo";
        }
        $t1 .= "<th>Quantidade</th>
		        <th>Quantidade Minima</th>
		        <th>Justificativa</th>";
		if($ur == 1){
		$t1 .="<th>Custo</th>";
            }


		$t1 .= "</tr>
		</thead>";
		while($row = mysql_fetch_array($result)){

		///$data =join("/",array_reverse(explode("-",$row['DATA'])));


			$t1.="<tr>
                              <td class='linha_est' id={$row['ID']} catalogo_id='{$row['CATALOGO_ID']}' tipo='{$row['TIPO']}' custo='".number_format($row['custo'], 2, ',', '.')."'>{$row['name']}
                               <br> <b> (LAB: {$row['lab']})</b>"; if($tipo == 'medcontrolado' || $tipo == 'med'){
                $t1.= "<br><b>Princípio Ativo:</b> ".$row['principioAtivo'];
            }
            $t1.=" </td>";

            $t1 .="<td><input $enableQuantidades type='text' value='{$row[$qtd]}' size='4px' qtd_anterior='{$row[$qtd]}' class='OBG' /></td>";

            if($ur == 1){
                $t1.="<td>
                        <input $enableQuantidades type='text' name='qtd_min' size='4px'  value='{$row[$qtd_min]}' qtd_min_anterior='{$row[$qtd_min]}' class='OBG' ocultar='N' tipo='{$row['TIPO']}'/>
                      </td>
                      <td>
                            <textarea $enableQuantidades class='justificativa'></textarea>

                      </td>";
                $t1.= "<td><b>R$ </b><input type='text' name='custo' id='custo' size='4px' class='valor OBG' {$enabled} value='".number_format($row['custo'], 2, ',', '.')."' /></td>";
            }else{
                $t1.="<td><input type='text' $enableQuantidades name= qtd_min value='{$row[$qtd_min]}' qtd_min_anterior='{$row[$qtd_min]}' class='OBG' ocultar='S'  tipo='{$row['TIPO']}' valor='".number_format($row['custo'],2,',','.')."'/></td>
                <td>
                            <textarea class='justificativa'></textarea>

                      </td>";

            }
			$t1.= "</tr>";	
	}

	$t1 .= "</table>";

            if($_SESSION["modlog"] == 1 || $_SESSION['adm_user'] == '1'){
                $t1 .= "<button id='salvar_est' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button>";

            }


	}else{
		$t1 .="<p id='res-b'>";
		$t1 .=  "<center><b>Nenhum resultado foi encontrado!</b></center></p>";

	}
	print_r($t1);
			}

function edt_estoque($post){
	$itens = explode("$",$post);
	mysql_query('begin');
	
	$ur=$_SESSION["empresa_principal"];
    if(isset($ur) && !empty($ur)){
        $condur = 'e.empresas_id_'.$ur;
        $qtd = 'empresas_id_'.$ur;
        $qtd_min ='QTD_MIN_'.$ur;

    }else{
        echo 'Erro falta a Unidade Regional. Entre em contato com o TI.';
        return;
    }
$c= 0;
	foreach($itens as $item) {
        if (!empty($item)) {
            $c++;
            $i = explode("#", str_replace("undefined", "", $item));
            $id = anti_injection($i[0], 'literal');
            $sql = "UPDATE estoque SET $qtd= '{$i[1]}', $qtd_min = '{$i[3]}' WHERE ID = '{$id}' ";
            $r = mysql_query($sql);

            $sql = "UPDATE catalogo SET valor = {$i[2]}, valorMedio = {$i[2]},
	custo_medio_atualizado_em = now() WHERE ID = '{$i[4]}' ";
            $r = mysql_query($sql);
            //jeferson

            if (!$r) {
                echo "ERRO: Tente mais tarde!--1";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
            $sql = <<<SQL
insert into `historico_modificacao_estoque` (

`quantidade_anterior`,
`quantidade_modificada`,
`quantidade_min_anterior`,
`quantidade_min_modificada`,
`usuario_id`,
`catalogo_id`,
`data`,
`justificativa`,
ur,
estoque_id,
tipo)
VALUES
 (
 '{$i[5]}',
 '{$i[1]}',
 '{$i[6]}',
 '{$i[3]}',
 '{$_SESSION['id_user']}',
 '{$i[4]}',
 now(),
 '{$i[7]}',
 '{$ur}',
 '{$i[0]}',
  '{$i[8]}'
 )

SQL;
            $r = mysql_query($sql);
            //jeferson

            if (!$r) {
                echo "ERRO: Erro ao gravar historioco de modificação.";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));


        }
    }

	mysql_query('commit');
				
	echo '1';
}

function inicio_inventario($tipo){
  if($tipo == 0){
    $sql = $sql = "SELECT ID, NUMERO_TISS as cod, concat(`principio`,' - ',`apresentacao`) as nome FROM catalogo WHERE tipo = 0 and ATIVO='A' ORDER BY nome ASC";
  } else if($tipo == 1){
    $sql = $sql = "SELECT ID, NUMERO_TISS  as cod, concat(`principio`,' - ',`apresentacao`) as nome FROM catalogo WHERE tipo = 1 and ATIVO='A' ORDER BY nome ASC";
  }else if($tipo == 3){
    $sql = $sql = "SELECT ID, NUMERO_TISS  as cod, concat(`principio`,' - ',`apresentacao`) as nome FROM catalogo WHERE tipo = 3 and ATIVO='A' ORDER BY nome ASC";
  }
  $result = mysql_query($sql);
  echo "<p><b>Nome:</b><br/>";
  echo "<select name='nome' id='inicio_inventario' style='background-color:transparent;' width='100%'>";
  while($row = mysql_fetch_array($result))
  {
    foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
    echo "<option value='". $row['nome'] ."'>". $row['nome'] ."</option>";
  }
  echo "</select>";
}

function inventario($tipo,$zerados,$offset,$inicio){
  $sql = ""; $print = "<html><body>";
  if($zerados == "true") $condicao = "1"; else $condicao = " estoque > '0' ";
  if($tipo == 0){
      $sql = "SELECT principio, apresentacao, estoque as qtd FROM catalogo  
	      WHERE concat(`principio`,' - ',`apresentacao`) >= '$inicio' AND tipo = 0 AND $condicao  
	      ORDER BY LOWER(principio), LOWER(apresentacao) ASC LIMIT 0,$offset";
  } else if($tipo == 1){
    $sql = "SELECT principio, apresentacao, estoque as qtd FROM catalogo   
	      WHERE concat(`principio`,' - ',`apresentacao`) >= '$inicio' AND tipo = 1 AND $condicao  
	      ORDER BY LOWER(principio), LOWER(apresentacao) ASC LIMIT 0,$offset";
  }else if($tipo == 3){
    $sql = "SELECT principio, apresentacao, estoque as qtd FROM catalogo   
	      WHERE concat(`principio`,' - ',`apresentacao`) >= '$inicio' AND tipo = 3 AND $condicao  
	      ORDER BY LOWER(principio), LOWER(apresentacao) ASC LIMIT 0,$offset";
  }
  $print .= "<table border=1 cellspacing=0 cellpadding=0 >".
  			"<thead><tr><th>N</th><th>Nome</th><th>Apresenta&ccedil;&atilde;o</th><th>Estoque</th><th>OBS</th></tr></thead>";
  $result = mysql_query($sql);
  $c = 0;
//  echo $sql;
  while($row = mysql_fetch_array($result))
  {
    $c++;
    $print .= "<tr>";
    foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
    $print .= "<td>$c</td>";
    $print .= "<td>". $row['principio']. "</td><td>". $row['apresentacao']. "</td><td>" . $row['qtd'] . "</td>";
    $print .= "<td width=25% >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
  }
  $print .= "</table></body></html>";
  $html = $c."<br>".$print;
  $mpdf=new mPDF('pt','A4',9);
  $mpdf->WriteHTML($print);
  $mpdf->Output('inventario.pdf','I');
  exit;
}

function pdf_especiais($venc){
  $sql = "SELECT * FROM especiais WHERE vencimento <= '$venc' ORDER BY vencimento ASC";
  $print = "<html><body>";
  $result = mysql_query($sql);
  $print .= "<table border=0 width=100% >";
  $print .= "<thead><tr><th><b>Princ&iacute;pio Ativo</b></th><th><b>Vencimento</b></th><th><b>Lote</b></th><th><b>Quantidade</b></th></tr></thead>";
  while($row = mysql_fetch_array($result))
  {
    foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
    $vencimento = implode("/",array_reverse(explode("-",$row['vencimento'])));
    $print .= "<tr><th>{$row['principio']}</td><td>{$vencimento}</td><td>{$row['lote']}</td><td>{$row['qtd']}</td></tr>";
  }
  $print .= "</table></body></html>";
  $mpdf=new mPDF('pt','A4',9);
  $mpdf->WriteHTML($print);
  $mpdf->Output('especiais.pdf','I');
  exit;
}

function remover_especial($id){
  mysql_query("DELETE FROM especiais WHERE id='$id' ");
}

function opcoes_itens($post) {
  extract($post,EXTR_OVERWRITE);
  echo "<table id='tabela_itens' class='mytable' width=100% >"; 
  echo "<thead><tr>"; 
  echo "<th><b>Princ&iacute;pio Ativo</b></th>";
  echo "<th><b>Apresenta&ccedil;&atilde;o</b></th>";
  echo "</tr></thead>";
  $cor = false;
  $total = 0;
  $sql = "SELECT DISTINCT med.principioAtivo as nome, com.nome as apr, com.idNomeComercialMed as item FROM medicacoes as med, nomecomercialmed as com 
	 WHERE (med.principioAtivo LIKE '%$like%' OR com.nome LIKE '%$like%' OR com.codigoBarras = '$like') AND med.idMedicacoes = com.Medicacoes_idMedicacoes 
	 ORDER BY med.principioAtivo";
  if($tipo == "1")
    $sql = "SELECT DISTINCT mat.idMaterial as item, mat.nome as nome, mat.descricao as apr FROM material as mat 
			  WHERE (mat.nome LIKE '%$like%' OR mat.descricao LIKE '%$like%' OR mat.codigoBarras = '$like') ORDER BY mat.nome";
  $result = mysql_query($sql) or trigger_error(mysql_error());
  while($row = mysql_fetch_array($result)){ 
    foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
    if($cor) echo "<tr pnome='{$row['nome']}' snome='{$row['apr']}' cod='{$row['item']}' >";
    else echo "<tr pnome='{$row['nome']}' snome='{$row['apr']}' cod='{$row['item']}' class='odd'>";
    $cor = !$cor;
    echo "<td valign='top'>" . $row['nome'] . "</td>";
    echo "<td valign='top'>" . $row['apr'] . "</td>";
    echo "</tr>";
  }
  echo "</table>";
  echo "<script type='text/javascript' >";
  echo " $('#tabela_itens tr').dblclick(function(){
	      $('#nome-item').val($(this).attr('pnome'));
	      $('#apr-item').val($(this).attr('snome'));
	      $('#item').val($(this).attr('cod'));
	      $('#op').html('');
	      $('#qtd-dev').focus();
	      $('#busca-item').val('');
	  });";
    echo "</script>";
}



function relatorio($get){
    extract($get,EXTR_OVERWRITE);
    $dta = implode("-",array_reverse(explode("/",$data)));
    $tabela = "<table border=1 width=100% style='font-size:x-small' >";
    $tabela .= "<tr><th><b>Nome</b></th>";
    $tabela .= "<th><b>Apresenta&ccedil;&atilde;o</b></th>";
    $tabela .= "<th><b>Quantidade</b></th>";
    $tabela .= "<th><b>Observa&ccedil;&atilde;o</b></th></tr></thead>";
    
    $sql = "SELECT s.nome as n, s.segundoNome as sn, s.obs, s.quantidade as qtd, p.nome as paciente  FROM saida as s, clientes as p 
	    WHERE s.idCliente = '{$paciente}' AND s.idCliente = p.idClientes AND s.data = '{$dta}' AND s.idUsuario = '{$_SESSION["id_user"]}' AND quantidade > 0 
	    ORDER BY s.tipo, s.nome, s.segundoNome ASC";
    
    $result = mysql_query($sql) or trigger_error(mysql_error());
    while($row = mysql_fetch_array($result)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      $tabela .= "<tr><td>{$row['n']}</td><td>{$row['sn']}</td><td>{$row['qtd']}</td><td>{$row['obs']}</td></tr>";
    }
    $tabela .= "</table>";
    
    $html = "<html><meta http-equiv='Content-Type' content='text/html; charset='utf-8'><body>";
    $html .= "<img src='../utils/logo.jpg' width=30% style='align:center' /><div style='font-size:x-small'>";
    $html .= "<table align='center' width=100% border=0 >";
    $html .= "<tr><td>Paciente:</td><td>Data:</td><td>Respons&aacute;vel pela sa&iacute;da:</td></tr>";
    $usuario = ucwords(strtolower($_SESSION["nome_user"]));
    $html .= "<tr><td>{$pacientenome}</td><td>{$data}</td><td>{$usuario}</td></tr>";
    $html .= "</table>";
    $html .= "<p><b>Relat&oacute;rio</b>";
    $html .= $tabela;
    $html .= "<br />
               <table>
                  <tr>
                      <td>_______________________________________</td> 
                      <td></td>
                      <td>___/___/____</td>
                      <td></td>
                      <td>__:__:__</td>
                   </tr>
                   <tr>
                       <td>Assinatura do respons&aacute;vel</td>
                       <td></td>
                       <td>Data</td>
                       <td></td>
                       <td>Hora</td>
                   </tr>
                   <tr>
                      <td>_______________________________________</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>/td>
                   </tr>

               </table>";
   
    $html .= "</div></body></html>";
	$mpdf=new mPDF('pt','A4',11);
  	$mpdf->WriteHTML($html);
  	$mpdf->Output('relatorio.pdf','I');
  	exit;
}

function combo_rastreamento($tipo){
	
	if($tipo == "med")
		$sql = "SELECT ID as cod, concat(principio,' - ',apresentacao,' LAB:', lab_desc) as nome FROM catalogo WHERE tipo = 0 and ATIVO = 'A' ORDER BY nome";
	else if($tipo == "mat")
		$sql = "SELECT ID as cod, concat(principio,' - ',apresentacao) as nome FROM catalogo WHERE tipo = 1 and ATIVO = 'A' ORDER BY nome";
	else if($tipo == "die"){
	$sql = "SELECT ID  as cod, concat(principio,' - ',apresentacao' LAB:', lab_desc) as nome FROM catalogo WHERE tipo = 3 and ATIVO = 'A' ORDER BY nome";
        }else{
           $sql = "SELECT idFornecedores as cod, razaoSocial as nome FROM fornecedores ORDER BY razaoSocial"; 
        }
        $result = mysql_query($sql);
    echo "<p>";
    echo "<select name='codigo' style='background-color:transparent;width:400px' >";
    while($row = mysql_fetch_array($result))
    	echo "<option style='width:400px' value='{$row['cod']}'>{$row['nome']}</option>";     
    echo "</select>";

}

function combo_busca_saida($tipo, $origem = null) {
  if ($tipo == 'eqp') {
    $sql = "select id as cod, item as nome from cobrancaplanos where tipo = 1";
  } else if ($tipo == 'ur') {
    $sql = "SELECT id AS cod, nome AS nome FROM empresas WHERE ATIVO='S'";
  } else if ($tipo == 'paciente') {
    if ($_SESSION['empresa_principal'] == 1) {
      $cond_paciente = '1 = 1';
    } else {
      $cond_paciente = "empresa ={$_SESSION['empresa_principal']} ";
    }

    $sql = "SELECT c.idClientes, c.nome as nomeCliente, e.nome as ur FROM clientes c INNER JOIN empresas e ON (c.empresa = e.id) where {$cond_paciente} ORDER BY c.nome";
    $result = mysql_query($sql);
    
    $pacientes = [];
    $var = "<p>";
    $var .= "<select name='codigo' class='combo-pesquisar-saidas' style='background-color:transparent;width:400px;display:inline;' >";
    $var .= "<option value='T'>Todos</option>";
    while($row = mysql_fetch_array($result))
    {
      $pacientes[$row['ur']][$row['idClientes']] = $row['nomeCliente'];
    }
    foreach ($pacientes as $ur => $paciente) {
      $var .= "<optgroup label='{$ur}'>";    
      foreach ($paciente as $id => $nome) {
        $var .= "<option value='{$id}'>{$nome}</option>";
      }
      $var .= "</optgroup>";
    }
    $var .= "</select></p>";
    
  } else {
    if ($tipo == 'med') {
      $tip = 0;
      $todos = "<input type='checkbox' name='todos' id='todos' value='1' style='display:inline;' /> <label for='todos' style='display:inline;'>TODOS</label>";
    } else if ($tipo == 'mat') {
      $tip = 1;
        $todos = "<input type='checkbox' name='todos' id='todos' value='1' style='display:inline;' /> <label for='todos' style='display:inline;'>TODOS</label>";
    } else {
      $tip = 3;
      $todos = "<input type='checkbox' name='todos' id='todos' value='1' style='display:inline;' /> <label for='todos' style='display:inline;'>TODOS</label>";
    }
    $sql = "SELECT ID  as cod, NUMERO_TISS , concat(principio,' - ',apresentacao, ' LAB:', lab_desc) as nome FROM catalogo WHERE tipo = {$tip} and ATIVO='A' ORDER BY nome";
  }
  if ($origem == null) {
    $todos = '';
  }
  
  if($tipo != 'paciente'){
    $result = mysql_query($sql);
    $html = "<p>";
    $html .= "<select name='codigo' class='combo-pesquisar-saidas' style='background-color:transparent;width:400px;display:inline;' >";
      $html .= "<option value='T'>Todos</option>";
    while ($row = mysql_fetch_array($result)) {
        $html .= "<option style='width:400px' value='{$row['cod']}'  >{$row['nome']}</option>";
    }
    $html .= "</select></p>";
  
    echo $html;
  }else{
    echo $var;
  }
}

function envio_paciente($post)
{
    extract($post, EXTR_OVERWRITE);
    mysql_query("begin");
    $c = 0;
    $dados = substr($dados, 0, -1);
    $itens = explode("$", $dados);
    $data = implode("-", array_reverse(explode("/", $dia)));
    $tabela = "<table border=1 width=100% style='font-size:x-small' >";
    $tabela .= "<tr><th><b>Nome{$i[1]}</b></th>";
    $tabela .= "<th><b>" . htmlentities("Apresentação") . "</b></th>";
    $tabela .= "<th><b>Quantidade</b></th>";
    $tabela .= "<th><b>Lote</b></th>";
    $tabela .= "<th><b>" . htmlentities("Solicitação") . "</b></th>";
    $tabela .= "<th><b>" . htmlentities("Separação") . "</b></th>";
    $tabela .= "<th><b>" . htmlentities("Observação") . "</b></th></tr></thead>";
    $ur = $_SESSION['empresa_principal'];
    $idEmpresaPaciente = anti_injection($idEmpresaPaciente, 'numerico');
    if (isset($ur) && !empty($ur)) {
        $condur = 'e.empresas_id_' . $ur;
        $qtd = 'empresas_id_' . $ur;
        $qtd_min = 'QTD_MIN_' . $ur;

    }
    $arrayTextoEmailEquipamento = [];

    $resultRemessaPaciente = Saidas::getMaxRemessaByUR($idEmpresaPaciente);
    $resultEmpresaPaciente = Filial::getUnidadesUsuario('('.$idEmpresaPaciente.')');
    $siglaSaidaUrPaciente = $resultEmpresaPaciente[0]['sigla_saida'];
    $nomeEmpresaPaciente = $resultEmpresaPaciente[0]['nome'];

    if(empty($resultRemessaPaciente)){
        $maxRemessaUrPaciente = 1;
    }else{
        $maxRemessaUrPaciente = $resultRemessaPaciente[0]['numerica'] + 1;
    }
    $remessaPaciente = $siglaSaidaUrPaciente.'-'.str_pad($maxRemessaUrPaciente, 6, "0", STR_PAD_LEFT);
    // Criar remessa para colocar na saída.
    $insertRemessa = "insert into 
                                          saida_remessa 
                                          (alfa, numerica, empresa_id, remessa, tipo_saida ) 
                                          value 
                                          ('{$siglaSaidaUrPaciente}', '{$maxRemessaUrPaciente}', '{$idEmpresaPaciente}', '{$remessaPaciente}', 'PACIENTE')";
    $resultRemessa = mysql_query($insertRemessa);
    if(!$resultRemessa){
        echo "-1#&ERRO: 01 Problema ao gerar remessa de saída!";
        mysql_query("rollback");
        return;
    }
    $idRemessaPaciente = mysql_insert_id();
    savelog(mysql_escape_string(addslashes($insertRemessa)));

    if ($ur != $idEmpresaPaciente) {

        $resultRemessaUr = Saidas::getMaxRemessaByUR($ur);
        $resultEmpresaUr = Filial::getUnidadesUsuario('('.$ur.')');
        $siglaSaidaUr = $resultEmpresaUr[0]['sigla_saida'];

        if(empty($resultRemessaUr)){
            $maxRemessaUr = 1;
        }else{
            $maxRemessaUr = $resultRemessaUr[0]['numerica'] + 1;
        }
        $remessaUr = $siglaSaidaUr.'-'.str_pad($maxRemessaUr, 6, "0", STR_PAD_LEFT);
        // Criar remessa para colocar na saída.
        $insertRemessa = "insert into 
                                          saida_remessa 
                                          (alfa, numerica, empresa_id, remessa, tipo_saida ) 
                                          value 
                                          ('{$siglaSaidaUr}', '{$maxRemessaUr}', '{$ur}', '{$remessaUr}', 'UR')";
        $resultRemessa = mysql_query($insertRemessa);
        if(!$resultRemessa){
            echo "-1#&ERRO: 02 Problema ao gerar remessa de saída!";
            mysql_query("rollback");
            return;
        }
        $idRemessaUr = mysql_insert_id();
        savelog(mysql_escape_string(addslashes($insertRemessa)));

        $sqlInsertPedido = "INSERT INTO
                      pedidosinterno
                      (DATA,
                       USUARIO,
                       EMPRESA,
                       STATUS,
                       OBSERVACAO,
                       DATA_MAX_ENTREGA,
                       CONFIRMADO,
                       DATA_CONFIRMADO,
                       CONFIRMADO_POR)
             VALUES   (now(),
                     '{$_SESSION['id_user']}',
                     '{$idEmpresaPaciente}',
                     'F',
                     'PEDIDO DE COMPRA GERADO PELO SISTEMA',
                     now(),
                     'S',
                     now(),
                     '{$_SESSION['id_user']}') ";
        

        $resultInsertPedido = mysql_query($sqlInsertPedido);
        if (!$resultInsertPedido) {

            echo "-1#&Erro ao gerar pedido de compra. Entre em contato com o TI";
            mysql_query("rollback");
            return;
        }
        $id_pedidosinterno = mysql_insert_id();
        savelog(mysql_escape_string(addslashes($sqlInsertPedido)));
}
	foreach($itens as $item){        

		
		$c++;
		$i = explode("#",str_replace("undefined","",$item));

                
                $i[0]= anti_injection($i[0], 'numerico');
                $i[1]= anti_injection($i[1], 'literal');
                $i[2]= anti_injection($i[2], 'literal');
                $i[3]= anti_injection($i[3], 'literal');
                $i[4]= anti_injection($i[4], 'literal');
                $i[5]= anti_injection($i[5], 'numerico');
                $i[6]= anti_injection($i[6], 'numerico');
                $i[7]= anti_injection($i[7], 'literal');
                $i[8]= anti_injection($i[8], 'numerico');
                $i[9]= anti_injection($i[9], 'literal');
                $i[10]= anti_injection($i[10], 'literal');
                $i[11]= anti_injection($i[11], 'literal');
                $i[12]= anti_injection($i[12], 'numerico');
                $i[13]= anti_injection($i[13], 'literal');
                $i[14]= anti_injection($i[14], 'numerico');




       // Montar email para envio de email para fornecedor de equipamentos
         // verifica se é equipamento.
        if($i[6] == 5){
            $arrayTextoEmailEquipamento[$i[14]]['nomeFornecedor'] = $i[13];
            if($i[11] == 1 || $i[11] == 3 ){
                $arrayTextoEmailEquipamento[$i[14]]['envio'] = empty($arrayTextoEmailEquipamento[$i[14]]['envio'])
                                                                ? "<tr><td>$i[3]</td><td>$i[10]</td></tr>"
                                                                :$arrayTextoEmailEquipamento[$i[14]]['envio']."<tr><td>$i[3]</td><td>$i[10]</td></tr>";
            }
        }
                
        $sql = "UPDATE solicitacoes SET enviado = (enviado + '{$i[1]}') WHERE idSolicitacoes = '{$i[0]}'";
		$r = mysql_query($sql);
        if(!$r){
			echo "-1#&ERRO: Tente mais tarde!--1.1";
			mysql_query("rollback");
			return;
		}
		savelog(mysql_escape_string(addslashes($sql)));



		
		if($i[11] == 2){
            $sql = "Select e.ID, s.NUMERO_TISS,e.CAPMEDICA_ID,s.CATALOGO_ID from solicitacoes as s inner join equipamentosativos as e on (e.COBRANCA_PLANOS_ID = s.CATALOGO_ID) WHERE idSolicitacoes = '{$i[0]}' and e.PACIENTE_ID= '{$i[5]}' and  e.DATA_INICIO != '0000-00-00 00:00:00' and  e.DATA_FIM = '0000-00-00 00:00:00'";
			$r = mysql_query($sql);
			$ideqpatvo=mysql_result($r,0,0);
			$cod=mysql_result($r,0,1);
			$idcap=mysql_result($r,0,2);
			if(!$r){
				echo "-1#&ERRO: Tente mais tarde!--3";
				mysql_query("rollback");
				return;
			}
            savelog(mysql_escape_string(addslashes($sql)));

			$sql = "UPDATE solicitacoes SET enviado = autorizado WHERE idSolicitacoes = '{$i[0]}'";
		$r = mysql_query($sql);
		if(!$r){
				echo "-1#&ERRO: Tente mais tarde!--4.1";
				mysql_query("rollback");
				return;
			}
                        savelog(mysql_escape_string(addslashes($sql)));

			$sql = "UPDATE equipamentosativos SET DATA_FIM = now() WHERE id = '{$ideqpatvo}'";
			$r = mysql_query($sql);
			if(!$r){
				echo "-1#&ERRO: Tente mais tarde!--4";
				mysql_query("rollback");
				return;
			}
                        savelog(mysql_escape_string(addslashes($sql)));
		}
		
		
		
		$sql = "SELECT (CASE WHEN autorizado=enviado or autorizado<enviado THEN '1' ELSE '2' END) as teste FROM `solicitacoes` WHERE `solicitacoes`.`idSolicitacoes`={$i[0]}";
		$r = mysql_query($sql);
		if(!$r){
			echo "-1#&ERRO: Tente mais tarde!--5--'{$i[0]}'";
			mysql_query("rollback");
			return;
		}
		while($row = mysql_fetch_array($r))
			$teste = $row['teste'];
		
		if($teste=='1')
            $sql = "UPDATE solicitacoes SET `envioCompleto` = now() WHERE idSolicitacoes = '{$i[0]}'";
			$r = @mysql_query($sql);
		if(!$r){
			echo "-1#&ERRO: Tente mais tarde!--6";
			mysql_query("rollback");
			return;
		}
                savelog(mysql_escape_string(addslashes($sql)));
                $lote= anti_injection($i[10], 'literal');

    $sql = "INSERT INTO saida (NUMERO_TISS,tipo, nome, segundoNome, quantidade, idCliente, idUsuario, data,idSolicitacao,LOTE,CATALOGO_ID,DATA_SISTEMA,fornecedor,UR_ORIGEM,remessa,saida_remessa_id)
	  			VALUES ('{$i[2]}','{$i[6]}','{$i[3]}','{$i[4]}','{$i[1]}','{$i[5]}','{$_SESSION['id_user']}','{$data}','{$i[0]}','{$lote}','{$i[12]}',now(),$i[14],'{$idEmpresaPaciente}','{$remessaPaciente}','{$idRemessaPaciente}') ";
  		$a = mysql_query($sql);
                
  		if(!$a){
  			echo "-1#&ERRO: Tente mais tarde!--7";
  			mysql_query("rollback");
  			return;
  		}
        $idSaida = mysql_insert_id();
      savelog(mysql_escape_string(addslashes($sql)));
    //enviar equipamento
    if($i[11] == 1 || $i[11] == 3 ) {

      $sql = "INSERT INTO
             equipamentosativos
             (COBRANCA_PLANOS_ID,
             CAPMEDICA_ID,
             QTD,
             DATA_INICIO,
             DATA_FIM,
             USUARIO,
             PACIENTE_ID,
             DATA,
             saida_id,
             lote,
             fornecedor)
             VALUES
             ('{$i[12]}',
             '',
             '{$i[1]}',
             '{$data}',
             '0000-00-00 00:00:00',
             '{$_SESSION['id_user']}',
             '{$i[5]}',
             now(),
             $idSaida,
             '$lote',
             $i[14]
             )";
      $r = mysql_query($sql);
      if(!$r){
        echo "-1#&ERRO: Tente mais tarde!--2";
        mysql_query("rollback");
        return;
      }
      savelog(mysql_escape_string(addslashes($sql)));
    }
		
  		if($i[6]==12 ){
			
  		}else{
                    if($i[11]!= 2 && $i[11]!= 3){
                        //verifica se o usuário é da mesma empresa do paciente, Caso seja da saída normal.

                            $sql = "UPDATE estoque SET $qtd = ($qtd - {$i[1]}) WHERE CATALOGO_ID = '{$i[12]}' and TIPO={$i[6]} ";
                            $b = mysql_query($sql);
                            if (!$b) {
                                echo "-1#&ERRO: Tente mais -tarde!--8" ;
                                mysql_query("rollback");
                                return;
                            }
                            savelog(mysql_escape_string(addslashes($sql)));
                        //Verifica se o usuário é da mesma empresa do paciente, Caso seja diferente o sistema faz a compra automatica
                        //para Central..
                            if($ur != $idEmpresaPaciente){
                              $sqlInsertItemPedido = " INSERT INTO
                                                     itenspedidosinterno
                                                      (PEDIDOS_INTERNO_ID,
                                                       NUMERO_TISS,
                                                        QTD,
                                                        TIPO,
                                                        ENVIADO,
                                                        STATUS,
                                                        EMERGENCIA,
                                                        CATALOGO_ID,
                                                        CONFIRMADO)
			                                    VALUES ('{$id_pedidosinterno}',
			                                             '{$i[2]}',
			                                             '{$i[1]}',
			                                             '{$i[6]}',
			                                             '{$i[1]}',
                                                         'F',
			                                             '{0}',
                                                         '{$i[12]}',
                                                         '{$i[1]}'
                                                         )";

                            $resultInsertItemPedido = mysql_query($sqlInsertItemPedido);

                            if(!$resultInsertItemPedido){
                                echo "-1#&Erro ao inserir um item do pedido automatico. Entre em contato com o TI";
                                mysql_query("rollback");
                                return;
                            }
                            $idInsertItemPedido =mysql_insert_id();
                            savelog(mysql_escape_string(addslashes($sqlInsertItemPedido)));



                            $sqlInsertSaidaPedido = " INSERT INTO
                                                      saida
                                                      (NUMERO_TISS,
                                                      tipo,
                                                      quantidade,
                                                      idUsuario,
                                                       data,
                                                       LOTE,
                                                       ITENSPEDIDOSINTERNOS_ID,
                                                       UR,
                                                       UR_ORIGEM,
                                                       DATA_SAIDA,
                                                       nome,
                                                       segundoNome,
                                                       CATALOGO_ID,
                                                       DATA_SISTEMA,
                                                       remessa,
                                                       saida_remessa_id)
		                                        VALUES ('{$i[2]}',
		                                                '{$i[6]}',
		                                                '{$i[1]}',
		                                                '{$_SESSION['id_user']}',
		                                                now(),
		                                                '{$lote}',
		                                                '{$idInsertItemPedido}',
		                                                '{$idEmpresaPaciente}',
		                                                '{$_SESSION['empresa_principal']}',
		                                                '{$data}',
		                                                '{$i[3]}',
		                                                '{$i[4]}',
		                                                '{$i[12]}',
		                                                now(),
		                                                '{$remessaUr}',
		                                                '{$idRemessaUr}') ";
                            $resultInsertSaidaPedido = mysql_query($sqlInsertSaidaPedido);
                            if(!$resultInsertSaidaPedido){
                                echo "-1#&Erro ao dar saída automatica do pedido de compra. Entre em contato com o TI";
                                mysql_query("rollback");
                                return;
                            }
                            savelog(mysql_escape_string(addslashes($sqlInsertSaidaPedido)));
                            $sqlInsertEntradaPedido = "Insert into
                                                     entrada_ur
                                                     (DATA,
                                                     USUARIO_ID,
                                                     QTD,
                                                     LOTE,
                                                     ITENSPEDIDOSINTERNOS_ID,
                                                     UR)
                                             VALUES (now(),
                                                     '{$_SESSION['id_user']}',
                                                     '{$i[1]}',
                                                     '{$lote}',
                                                     '{$idInsertItemPedido}',
                                                     11)";
                            $resultInsertEntradaPedido = mysql_query($sqlInsertEntradaPedido);
                            if(!$resultInsertEntradaPedido){
                                echo "-1#&Erro ao execultar sqlInsertEntradaPedido. Entre em contato com o TI.";
                                mysql_query("rollback");
                                return;
                            }
                            savelog(mysql_escape_string(addslashes($sqlInsertEntradaPedido)));

                        }
                    }
		}

		$enviado = $i[1]+$i[8];
		$tabela .= "<tr><td>{$i[3]}</td><td>{$i[4]}</td><td>{$i[1]} TOTAL: $enviado/{$i[9]}</td><td>$lote<td>{$i[7]}</td><td>{$dia}</td><td></td></tr>";
	}
	$dadosEnvio['paciente'] = $np;
    $dadosEnvio['data'] = $dia;
    $dadosEnvio['remessa'] = $remessaPaciente;
    $dadosEnvio['nomeEmpresa'] = $nomeEmpresaPaciente;

	$tabela .= "</table>";
//	$html = "<html><meta http-equiv='Content-Type' content='text/html; charset='utf-8'><body>";
//    $html .= "<img src='../utils/logo.jpg' width=30% style='align:center' /><div style='font-size:x-small'>";
    $html = "<table align='center' width=100% border=0 >";
    $html .= "<tr><td>Paciente:</td><td>Data:</td><td>" . htmlentities("Responsável pela saída") . ":</td></tr>";
    $usuario = ucwords(strtolower($_SESSION["nome_user"]));
    $html .= "<tr><td>{$np}</td><td>{$dia}</td><td>{$usuario}</td></tr>";
    $html .= "<tr><td>" . htmlentities("Endereço") . ":</td><td>Cuidador:</td><td>Tel:</td></tr>";
    $html .= "<tr><td>{$endereco}</td><td>{$cuidador}</td><td>{$tel}</td></tr>";
    $html .= "<tr><td colspan='3'> Remessa: <b>{$remessaPaciente}</b></td></tr>";
    $html .= "</table>";
    $html .= "<p><b>" . htmlentities("Tipo de envio: ") .$tipo_prescricao. " </b>";
    $html .= "<p><b>" . htmlentities("Relatório") . "</b>";
    $html .= $tabela;
    $html .= "<br /><br /><table>
                  <tr>
                      <td>_______________________________________</td> 
                      <td></td>
                      <td>___/___/____</td>
                      <td></td>
                      <td>__:__:__</td>
                   </tr>
                   <tr>
                       <td>Assinatura do " . htmlentities("Responsável") . "</td>
                       <td></td>
                       <td>Data</td>
                       <td></td>
                       <td>Hora</td>
                   </tr>
                   <tr>
                      <td>_______________________________________</td>
                      <td></td>
                      <td colspan ='3'>______________________________</td>

                   </tr>
                   <tr>
                       <td>Assinatura do condutor.</td>
                       <td></td>
                       <td colspan ='3'>Veículo</td>

                   </tr>
               </table>";
    
    $html .= "</div></body></html>";

   mysql_query("commit");
    if(!empty($arrayTextoEmailEquipamento)){
        emailControleEquipamento($arrayTextoEmailEquipamento, $dadosEnvio);
    }

	echo $html;
}

function cancelar_item_solicitacao($post){
	extract($post,EXTR_OVERWRITE);
	$sol = anti_injection($sol,'numerico');
	$just = anti_injection($just,'literal');
	$just = "CANCELADO PELA LOG&Iacute;STICA: ".$just; 
        $sql="UPDATE solicitacoes SET status = '-2', obs = '{$just}', CANCELADO_POR='{$_SESSION['id_user']}',DATA_CANCELADO=now() WHERE idSolicitacoes = '{$sol}' ";
	$r = mysql_query($sql);
	if(!$r){
		echo "ERRO: Tente mais tarde!";
		return; 
	}
        savelog(mysql_escape_string(addslashes($sql)));
	echo "1";
	return;
}



function busca_itens($like,$tipo){
	$sql = "";
	$label_tipo = "";
	$like = anti_injection($like,'literal');
	switch($tipo){
		case 'kits':
			$label_tipo = "KITS";
			$sql = "SELECT k.nome as nome, k.idKit as cod FROM kitsmaterial as k WHERE k.nome LIKE '%$like%' ORDER BY k.nome";
		break;
		case 'mat':
			$label_tipo = "MATERIAIS";
			$sql = "SELECT concat(mat.nome,' ',mat.descricao) as nome, mat.idMaterial as cod FROM material as mat WHERE mat.nome LIKE '%$like%' OR mat.descricao LIKE '%like%' ORDER BY nome";
		break;
		case 'med':
			$label_tipo = "MEDICAMENTOS";
			$sql = "SELECT concat(med.principioAtivo,' ',com.nome) as nome, com.idNomeComercialMed as cod FROM medicacoes as med, nomecomercialmed as com WHERE (med.principioAtivo LIKE '%$like%' OR com.nome LIKE '%$like%') AND med.idMedicacoes = com.Medicacoes_idMedicacoes ORDER BY nome";
		break;
	}
	$result = mysql_query($sql);
	echo "<table class='mytable' width=100% id='lista-itens' ><thead><tr><th>{$label_tipo}</th></tr></thead>";
	while($row = mysql_fetch_array($result)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      echo "<tr cod='{$row['cod']}' ><td>{$row['nome']}</td></tr>";
	}
	echo "</table>";
	echo "<script type='text/javascript' >";
	echo " $('#lista-itens tr').dblclick(function(){
	      $('#nome').val($(this).text());
	      $('#cod').val($(this).attr('cod'));
	      $('#lista-itens').html('');
	      $('#busca-item-troca').val('');
	      $(\"#formulario-troca input[name='qtd']\").focus();
	  });";
	echo "</script>";
}


function podeEditar($userId, $isAdm, $tipo)
{
  return  $userId == 44
          || $userId == 95
          || $isAdm == 1
          || (in_array($tipo, [3, -1]) );
}

function brasindice($ur, $item, $tipo, $status){
    $condStatus = '';
    if($status <> -1){
        $condStatus =   " and ATIVO = '{$status}'";
    }
	if ($tipo == -1) {//Todos
		$sql = "SELECT lab_desc, principio AS item, apresentacao, NUMERO_TISS, ID, ativo, tipo, brasindice_id, simpro_id FROM catalogo WHERE  1 $condStatus";
	}
	
	if ($tipo == 0) {//Medicamento
		$sql = "SELECT lab_desc, catalogo.principio AS item, apresentacao, principio_ativo.principio AS principioAtivo, NUMERO_TISS, catalogo.ID,  ativo, tipo, brasindice_id, descontinuado
FROM catalogo LEFT JOIN catalogo_principio_ativo ON catalogo.ID = catalogo_principio_ativo.catalogo_id LEFT JOIN principio_ativo ON principio_ativo.id = catalogo_principio_ativo.principio_ativo_id WHERE tipo = 0 $condStatus and catalogo.principio like '%$item%'";
	}
	
	if ($tipo == 1) {//Material
		$sql = "SELECT lab_desc, principio AS item, apresentacao, NUMERO_TISS, ID,  ativo, tipo, tag, simpro_id, descontinuado FROM catalogo WHERE tipo = 1 $condStatus and principio like '%$item%'";
	}
	
	if ($tipo == 3) {//Dieta
		$sql = "SELECT lab_desc, principio AS item, apresentacao, NUMERO_TISS, ID,  ativo, tipo,  brasindice_id, descontinuado FROM catalogo WHERE tipo = 3 $condStatus and principio like '%$item%'";
	}
	
	$resultado = mysql_query($sql) or die (mysql_error());
	
	$resposta = mysql_num_rows($resultado);

    $resultado_consulta = "";
	
	if($resposta > 0){
            
            if (podeEditar($_SESSION['id_user'], $_SESSION['adm_user'], $tipo)) {
              $flag = 1;
              $editar = "<th>Editar</th>";
            }

            $resultado_consulta .= "<table id='res-b'  class='mytable' width=100% >
			<thead>
				<tr>     
				    <th>Cod. Interno</th>
					<th>Laboratorio</th>
					<th>Item</th>
					<th>Apresentacao</th>";
            if ($tipo == 0) {
                $resultado_consulta .= "<th>Principio Ativo</th>";
            }
            $resultado_consulta .= "                    
					<th>TISS</th>
					<th>Status</th>
					<th>Tags</th>
					$editar
				</tr>"
                    . "</thead>";
			while($row = mysql_fetch_array($resultado)){
                $descontinuado = '';
                if($row['descontinuado'] == 'S'){
                    $descontinuado = "(Descontinuado)";
                }
				$resultado_consulta.="
				<tr>
                    <td>{$row['ID']}</td>
					<td>{$row['lab_desc']}</td>
					<td>{$row['item']}</td>
					<td>{$row['apresentacao']}</td>";
                if ($tipo == 0) {
                    $resultado_consulta .= "<td>{$row['principioAtivo']}</td>";
                }
                $resultado_consulta .= "					
					<td>{$row['NUMERO_TISS']}</td>
					<td>".(($row['ativo']=='A')?"Ativo":"Inativo")." ".$descontinuado."</td>
					<td>{$row['tag']}</td>";
                $podeEditar = 0;
                if($row['ativo'] == 'D'){
                    if($row['tipo'] == 1 && $row['simpro_id'] > 0){
                        $podeEditar= 1;
                    }elseif(($row['tipo'] == 0 || $row['tipo'] == 3)  && $row['brasindice_id'] > 0){
                        $podeEditar= 1;
                    }
                }

          if ($flag === 1) {
            if ($_SESSION['id_user'] == 139 || $_SESSION['id_user'] == 311 && $row['tipo'] != 3) {
              $resultado_consulta .= "<td></td>";
            } else {
              if($row['ativo'] == 'A' || $podeEditar == 1 )
              {
                $resultado_consulta .= "<td><a href='?op=brasindice&act=editar&id={$row['ID']}'><img border='0' title='Editar' src='../utils/edit_16x16.png'></a></td></tr>";
              }else
              {
                $resultado_consulta .= "<td></td>";
              }
            }
          }
			}
			
		

		$resultado_consulta .= "</table>";
	}else{
		$resultado_consulta .="<p id='res-b'>";
		$resultado_consulta .=  "<center><b>Nenhum resultado foi encontrado!</b></center></p>";
	
	}
	print_r($resultado_consulta);
	
}

function novo_brasindice($principio, $apresentacao, $laboratorio, $numero_tiss, $nome_simpro_brasindice, $tipo_brasindice, $tag_brasindice, $status, $usuario, $data,$controlado,$antibiotico,$referencia,$id_simpro_brasindice,$numero_tuss,$id_principio,$generico,$categoria_material,$possui_generico) {
	$simpro_id='';
        $brasindice_id='';
     if($referencia == 'S'){
        $simpro_id= $id_simpro_brasindice;
    }else if($referencia == 'T'){
        $brasindice_id=$id_simpro_brasindice;
    }


if($numero_tuss == ''){
    $numero_tuss = '';
    $campoTuss = '';
}else{
    $numero_tuss = $numero_tuss.',';
    $campoTuss = 'TUSS,';
}

$laboratorio = anti_injection($laboratorio,'literal');
$principio= anti_injection($principio,'literal');
$apresentacao = anti_injection($apresentacao ,'literal');
$nome_simpro_brasindice = anti_injection($nome_simpro_brasindice,'literal');


 // die($numero_tuss);
    mysql_query('begin');
	$sql = "insert into catalogo
	(lab_desc ,principio,apresentacao,NUMERO_TISS,DESC_MATERIAIS_FATURAR,REFERENCIA,estoque,valorMedio,tipo,AddMederi,status,usuario,data,valor , VALOR_VENDA , VALOR_FABRICA, ATIVO , TAG , CONTROLADO ,ANTIBIOTICO,simpro_id,brasindice_id,".$campoTuss."GENERICO, 
	custo_medio_atualizado_em, CATEGORIA_MATERIAL_ID, possui_generico ) values
	('{$laboratorio}', '{$principio}', '{$apresentacao}', '{$numero_tiss}', '{$nome_simpro_brasindice}','{$referencia}', 0, 0, '{$tipo_brasindice}', 0, 0, '{$usuario}', '{$data}', 0.00, 0.00, 0.00, '{$status}', '{$tag_brasindice}','{$controlado}','{$antibiotico}','{$simpro_id}','{$brasindice_id}',".$numero_tuss."'{$generico}', '', '{$categoria_material}', '{$possui_generico}')";
   
    //die($sql);
    $resultado = mysql_query($sql) or die(mysql_error());
	if(!$resultado){
		echo "Ocorreu um erro ao tentar salvar o item.";
        mysql_query('rollback');
		return;
		
	}
    $idCatalogo = mysql_insert_id();
    savelog(mysql_escape_string(addslashes($sql)));

    # Buscando todas as UR's ativas
    $empresas = [];
    $complemento = '';
    $sql = "SELECT id FROM empresas WHERE ATIVO = 'S'";
    if($rs = mysql_query($sql)) {
        while ($row = mysql_fetch_array($rs)) {
            $empresas[] = $row['id'];
        }
    }

    $values = '';
    foreach($empresas as $empresa) {
        $complemento .= "empresas_id_{$empresa},";
        $values .= "'0',";
    }
    $complemento = substr_replace($complemento, '', -1, 1);
    $values = substr_replace($values, '', -1, 1);

    $sql = "INSERT INTO estoque (TIPO, NUMERO_TISS, CATALOGO_ID, {$complemento})
            VALUES ('{$tipo_brasindice}', '{$numero_tiss}', '{$idCatalogo}', {$values})";
    $resultado = mysql_query($sql);
    if (!$resultado) {
        echo "Ocorreu um erro ao tentar ativar item no estoque.";
        mysql_query('rollback');
        return;

    }
    savelog(mysql_escape_string(addslashes($sql)));

    if($tipo_brasindice == 0) {
        $sql = "insert into catalogo_principio_ativo (catalogo_id,principio_ativo_id) values ($idCatalogo,$id_principio)";

        $resultado = mysql_query($sql);
        if (!$resultado) {
            echo "Ocorreu um erro ao tentar associar o Princípio Ativo  ao novo item .";
            mysql_query('rollback');
            return;

        }
        savelog(mysql_escape_string(addslashes($sql)));
    }

    mysql_query('commit');
	echo "0";

	
}

function editar_brasindice($principio, $apresentacao, $laboratorio, $numero_tiss, $tipo_brasindice, $tag_brasindice,
                           $status, $usuario, $data,$controlado,$antibiotico,$catalogo_id,$referencia, $nome_simpro,
                           $numero_tuss,$id_principio,$generico, $principio_antigo, $descontinuado, $categoria_material,
                           $possui_generico) {
        
        $nome_simpro = empty($nome_simpro)?'NULL':"'$nome_simpro'";
    if($numero_tuss == ''){
        $tuss ='';
    }else{

        $tuss = 'TUSS ='.$numero_tuss.',';
    }
        
    mysql_query('begin');
    $sql = "update
            catalogo set 
            lab_desc = '{$laboratorio}',
            principio = '{$principio}', 
            apresentacao = '{$apresentacao}',
            NUMERO_TISS = '{$numero_tiss}',
	        estoque = 0, 
            tipo = '{$tipo_brasindice}',
            AddMederi = 0,
            status = 0, 
            usuario = '{$usuario}',
            data = '{$data}', 
            ATIVO = '{$status}', 
            TAG  = '{$tag_brasindice}',
            CONTROLADO = '{$controlado}',
            ANTIBIOTICO='{$antibiotico}',
            REFERENCIA='{$referencia}',
            DESC_MATERIAIS_FATURAR={$nome_simpro},
            $tuss
            GENERICO = '{$generico}',
            descontinuado = '{$descontinuado}',
            CATEGORIA_MATERIAL_ID = '{$categoria_material}',
            possui_generico = '{$possui_generico}'
	    where ID = '{$catalogo_id}'";
	
	$resultado = mysql_query($sql);
	
	if(!$resultado){
		echo "ERRO: AO fazer update em catalogo!";
        mysql_query('rollback');
		return;
	}
            savelog(mysql_escape_string(addslashes($sql)));
		if($tipo_brasindice == 0){
            if(!empty($principio_antigo)) {
                $result = mysql_query("update catalogo_principio_ativo set principio_ativo_id = {$id_principio} where catalogo_id = {$catalogo_id}");

                if (!$result) {
                    echo "ERRO: Ao atualizar o principio!";
                    mysql_query('rollback');
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }else {

                $sql = "insert into catalogo_principio_ativo (catalogo_id,principio_ativo_id) values ($catalogo_id,$id_principio)";

                $resultado = mysql_query($sql);
                if (!$resultado) {
                    echo "Ocorreu um erro ao tentar associar o Princípio Ativo  ao novo item .";
                    mysql_query('rollback');
                    return;

                }
                savelog(mysql_escape_string(addslashes($sql)));
            }
        }
    if($status == 'A') {
            $sqlEstoque = "
        select
         *
         from
          estoque
          where
          CATALOGO_ID = {$catalogo_id}
          AND
          TIPO <> 5";
            $resultadoEstoque = mysql_query($sqlEstoque);
            if(mysql_num_rows($resultadoEstoque) == 0){
                $sql = "
                INSERT INTO
                estoque
                (CATALOGO_ID, NUMERO_TISS, TIPO)
                VALUES
                ($catalogo_id,'{$numero_tiss}','{$tipo_brasindice}')";
            }else{
                $sql = "Update
                estoque
                set
                TIPO = '{$tipo_brasindice}'
                where
                CATALOGO_ID = $catalogo_id
                AND
                TIPO <> 5";

            }
        $resultado = mysql_query($sql);
        if (!$resultado) {
            echo "Erro ao atualizar estoque.";
            mysql_query('rollback');
            return;

        }
        savelog(mysql_escape_string(addslashes($sql)));
    }
    mysql_query('commit');
    echo 1;
}

function trocar_item_solicitacao($post){
	extract($post,EXTR_OVERWRITE);
	$sol = anti_injection($sol,'numerico');
	$justificativa = anti_injection($just,'literal');
	$p = anti_injection($p,'numerico');
	$item = anti_injection($item,'numerico');
	$qtd = anti_injection($qtd,'numerico');
	$n = anti_injection($n,'literal');
    $tiss = anti_injection($tiss,'literal');
    $tipo = anti_injection($tipo,'literal');
    $dataTroca = (new \DateTime())->format('d/m/Y \a\s H:i:s');
	$just = "TROCADO PELA LOG&Iacute;STICA POR: ".$n." MOTIVO: ".$justificativa . ". EM {$dataTroca}.";
	//a pres foi alterade de A para B por causa de C. mandar por email para msgsistema.
	mysql_query("begin");

	///jeferson 2012-10-24
    if($qtd > 0) {
        $r = mysql_query("Select
CATALOGO_ID,
idPrescricao,
TIPO_SOLICITACAO,
concat(catalogo.principio, '- ',catalogo.apresentacao) as item,
DATA_MAX_ENTREGA,
entrega_imediata,
solicitacoes.status,
solicitacoes.qtd,
solicitacoes.enviado,
(Case  entrega_imediata when   'S' THEN solicitacoes.qtd else solicitacoes.autorizado  END ),
solicitacoes.DATA_SOLICITACAO,
solicitacoes.TIPO_SOL_EQUIPAMENTO,
solicitacoes.CHAVE_AVULSA,
solicitacoes.JUSTIFICATIVA_AVULSO,
solicitacoes.JUSTIFICATIVA_AVULSO_ID,
solicitacoes.id_prescricao_curativo,
solicitacoes.primeira_prescricao,
solicitacoes.relatorio_aditivo_enf_id
from
solicitacoes inner JOIN
catalogo on (catalogo.ID = solicitacoes.CATALOGO_ID)
WHERE
idSolicitacoes = '{$sol}'
and paciente= '{$p}'");
        $catalogoIdAntigo = mysql_result($r, 0, 0);
        $idPrescricao = mysql_result($r, 0, 1);
        $tipoSolicitacao = mysql_result($r, 0, 2);
        $nomeItem = mysql_result($r, 0, 3);
        $dataMaxEntrega = mysql_result($r, 0, 4);
        $entregaImediata = mysql_result($r, 0, 5);
        $status = mysql_result($r, 0, 6);
        $enviado = mysql_result($r, 0, 8);
        $autorizado = mysql_result($r, 0, 9);
        $dataSolicitacao = mysql_result($r, 0, 10);
        $qtdAtual = $autorizado - $enviado;
        $tipoSolEquipamento = mysql_result($r, 0, 11);
        $chaveAvulsa = mysql_result($r, 0, 12);
        $justificativaAvulsa = mysql_result($r, 0, 13);
        $justificativaAvulsaId = mysql_result($r, 0, 14);
        $idPrescricaoCurativo =  mysql_result($r, 0, 15);
        $primeiraPrescricao = mysql_result($r, 0, 16);
        $relatorioAditivoEnfId = mysql_result($r, 0, 17);

        if (!$r) {
            echo "ERRO: Tente mais tarde!--2";
            mysql_query("rollback");
            return;
        }
        if ($enviado == 0) {
            $r = mysql_query("UPDATE
solicitacoes
SET
status = '-2',
obs = '{$just}',
trocado = 'S'
WHERE
idSolicitacoes = '{$sol}' ");
        } elseif ($enviado > 0) {
            $r = mysql_query("UPDATE
solicitacoes
SET
status = '-2',
obs = '{$just}', 
enviado = '{$enviado}',
qtd = '{$enviado}',
autorizado = '{$enviado}',
trocado = 'S'
WHERE
idSolicitacoes = '{$sol}' ");
        }
        if (!$r) {
            echo "ERRO: Tente mais tarde!--1";
            mysql_query("rollback");
            return;
        }

        $justificativaNovoItem = "Item inserido pela Logistica  no lugar de:{$nomeItem} da solicitaçao {$sol}";
        if ($tipo == 'med') {
            $tipo = 0;
        } elseif ($tipo == 'mat') {
            $tipo = 1;
        } else {
            $tipo = 3;
        }

        $solicitante = $_SESSION["id_user"];

        if ($qtdAtual == $qtd || $qtd > $qtdAtual) {
            $sql = "
INSERT INTO solicitacoes
(paciente,
enfermeiro,
NUMERO_TISS,
CATALOGO_ID,
qtd,
tipo,
autorizado,
data,
status,
TIPO_SOL_EQUIPAMENTO,
idPrescricao,
TIPO_SOLICITACAO,
DATA_MAX_ENTREGA,
obs,
entrega_imediata,
DATA_SOLICITACAO,
inserido_troca,
CHAVE_AVULSA,
JUSTIFICATIVA_AVULSO,
JUSTIFICATIVA_AVULSO_ID,
id_prescricao_curativo,
primeira_prescricao,
relatorio_aditivo_enf_id,
trocado
)
VALUES
('$p',
'$solicitante',
'{$tiss}',
'{$item}',
'{$qtd}',
'{$tipo}',
'{$qtd}',
now(),
'{$status}',
'0',
{$idPrescricao},
{$tipoSolicitacao},
'{$dataMaxEntrega}',
'{$justificativaNovoItem}',
'$entregaImediata',
'{$dataSolicitacao}',
'S',
'{$chaveAvulsa}',
'{$justificativaAvulsa}',
'{$justificativaAvulsaId}',
'{$idPrescricaoCurativo}',
'{$primeiraPrescricao}',
'{$relatorioAditivoEnfId}',
'N'
)";
            $r = mysql_query($sql);

            if (!$r) {
                echo "ERRO: Tente mais tarde!--3";
                mysql_query("rollback");
                return;
            }
            $idNovaSolicitacao = mysql_insert_id();

            savelog(mysql_escape_string(addslashes($sql)));


            $sql = "INSERT INTO historico_troca_item
(catalogo_id_item,
catalogo_id_item_substituto,
user_id,
motivo,
data,
solicitacao_id,
tipo,
solicitacao_id_substituta)
VALUES
($catalogoIdAntigo,
$item,
$solicitante,
'$justificativa',
now(),
$sol,
$tipo,
$idNovaSolicitacao
)";
            $r = mysql_query($sql);
            if (!$r) {
                echo "ERRO: Tente mais tarde!--4";
                mysql_query("rollback");
                return;
            }

        } elseif ($qtdAtual > $qtd) {
            // INSERE A QUANTIDADE SOLICITADA
            $sql = "INSERT INTO
solicitacoes
(paciente,
enfermeiro,
NUMERO_TISS,
CATALOGO_ID,
qtd,
tipo,
autorizado,
data,
status,
TIPO_SOL_EQUIPAMENTO,
idPrescricao,
TIPO_SOLICITACAO,
DATA_MAX_ENTREGA,
obs,
entrega_imediata,
DATA_SOLICITACAO,
inserido_troca,
CHAVE_AVULSA,
JUSTIFICATIVA_AVULSO,
JUSTIFICATIVA_AVULSO_ID,
id_prescricao_curativo,
primeira_prescricao,
relatorio_aditivo_enf_id,
trocado
)
VALUES
('$p',
'$solicitante',
'{$tiss}',
'{$item}',
'{$qtd}',
'{$tipo}',
'{$qtd}',
now(),
'{$status}',
'0',
{$idPrescricao},
{$tipoSolicitacao},
'{$dataMaxEntrega}',
'{$justificativaNovoItem}',
'$entregaImediata',
'{$dataSolicitacao}',
'S',
'{$chaveAvulsa}',
'{$justificativaAvulsa}',
'{$justificativaAvulsaId}',
'{$idPrescricaoCurativo}',
'{$primeiraPrescricao}',
'{$relatorioAditivoEnfId}',
'N'
)";
            $r = mysql_query($sql);

            if (!$r) {
                echo "ERRO: Tente mais tarde!--3";
                mysql_query("rollback");
                return;
            }

            $idSolicitacao = mysql_insert_id();

            $sql = "INSERT INTO historico_troca_item
(catalogo_id_item,
catalogo_id_item_substituto,
user_id,
motivo,
data,
solicitacao_id,
tipo,
solicitacao_id_substituta)
VALUES
($catalogoIdAntigo,
$item,
$solicitante,
'$justificativa',
now(),
$sol,
$tipo,
'$idSolicitacao')";
            $r = mysql_query($sql);
            if (!$r) {
                echo "ERRO: Tente mais tarde!--6";
                mysql_query("rollback");
                return;
            }

            // INSERE A QUANTIDADE SOBRESALENTE
            $qtdNova = $qtdAtual - $qtd;
            $sql = "INSERT INTO
solicitacoes
(paciente,
enfermeiro,
NUMERO_TISS,
CATALOGO_ID,
qtd,
tipo,
autorizado,
data,
status,
TIPO_SOL_EQUIPAMENTO,
idPrescricao,
TIPO_SOLICITACAO,
DATA_MAX_ENTREGA,
obs,
entrega_imediata,
DATA_SOLICITACAO,
inserido_troca,
CHAVE_AVULSA,
JUSTIFICATIVA_AVULSO,
JUSTIFICATIVA_AVULSO_ID,
id_prescricao_curativo,
primeira_prescricao,
relatorio_aditivo_enf_id
)
VALUES
('$p',
'$solicitante',
'{$tiss}',
'{$catalogoIdAntigo}',
'{$qtdNova}',
'{$tipo}',
'{$qtdNova}',
now(),
'{$status}',
'0',
{$idPrescricao},
{$tipoSolicitacao},
'{$dataMaxEntrega}',
'{$justificativaNovoItem}',
'$entregaImediata',
'{$dataSolicitacao}',
'S',
'{$chaveAvulsa}',
'{$justificativaAvulsa}',
'{$justificativaAvulsaId}',
'{$idPrescricaoCurativo}',
'{$primeiraPrescricao}',
'{$relatorioAditivoEnfId}'
)";
            $r = mysql_query($sql);
            $idSolicitacao = mysql_insert_id();

            if (!$r) {
                echo "ERRO: Tente mais tarde!--3";
                mysql_query("rollback");
                return;
            }

            $sql = "INSERT INTO historico_troca_item
(catalogo_id_item,
catalogo_id_item_substituto,
user_id,
motivo,
data,
solicitacao_id,
tipo,
solicitacao_id_substituta)
VALUES
($catalogoIdAntigo,
$item,
$solicitante,
'$justificativa',
now(),
$sol,
$tipo,
'$idSolicitacao')";
            $r = mysql_query($sql);
            if (!$r) {
                echo "ERRO: Tente mais tarde!--4";
                mysql_query("rollback");
                return;
            }
        }

        mysql_query("commit");
        echo "1";
        return;
    } else {
        echo "É necessário colocar uma quantidade maior que zero.";
    }
}

function trocar_item_pedido_interno($post){
    extract($post,EXTR_OVERWRITE);
    $ped = anti_injection($ped,'numerico');
    $justificativa = anti_injection($just,'literal');
    $ur = anti_injection($ur,'numerico');
    $item = anti_injection($item,'numerico');
    $qtd = anti_injection($qtd,'numerico');
    $n = anti_injection($n,'literal');
    $tiss = anti_injection($tiss,'literal');
    $tipo = anti_injection($tipo,'literal');
    $dataTroca = (new \DateTime())->format('d/m/Y \a\s H:i:s');
    $just = "TROCADO PELA LOG&Iacute;STICA POR: ".$n." MOTIVO: ".$justificativa . ". EM {$dataTroca}.";
    //a pres foi alterade de A para B por causa de C. mandar por email para msgsistema.
    mysql_query("begin");
    $solicitante = $_SESSION["id_user"];

    if($qtd > 0) {
        $r = mysql_query("
        SELECT
            i.QTD , 
            i.ENVIADO ,
            i.CATALOGO_ID,
            concat(b.principio, ' ', b.apresentacao) AS item,
            i.CONFIRMADO,
            i.EMERGENCIA,
            i.NUMERO_TISS,
            i.JUSTIFICATIVA,
            i.JUSTIFICATIVA_ENTRADA,
            i.STATUS,
            i.TIPO,
            i.PEDIDOS_INTERNO_ID
  	  	FROM
  	  		itenspedidosinterno AS i 
  	  		INNER JOIN pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
  	  		INNER JOIN catalogo AS b ON ( b.ID = i.CATALOGO_ID )
  	  	WHERE i.ID = '{$ped}'
        ");
        $qtdPedido = mysql_result($r, 0, 0);
        $enviado = mysql_result($r, 0, 1);
        $catalogoIdAntigo = mysql_result($r, 0, 2);
        $nomeItem = mysql_result($r, 0, 3);
        $confirmado = mysql_result($r, 0, 4);
        $emergencia = mysql_result($r, 0, 5);
        $tissAntigo = mysql_result($r, 0, 6);
        $justAntiga = mysql_result($r, 0, 7);
        $justEntrada = mysql_result($r, 0, 8);
        $status = mysql_result($r, 0, 9);
        $tipoAntigo = mysql_result($r, 0, 10);
        $pedIntId = mysql_result($r, 0, 11);
        $qtdAtual = $qtdPedido - $enviado;

        if (!$r) {
            echo "ERRO: Tente mais tarde!--2" . mysql_error();
            mysql_query("rollback");
            return;
        }
        if ($enviado == 0) {
            $r = mysql_query("UPDATE
itenspedidosinterno
SET
STATUS = 'T',
JUSTIFICATIVA = '{$just}'
WHERE
ID = '{$ped}' ");
        } elseif ($enviado > 0) {
            $r = mysql_query("UPDATE
itenspedidosinterno
SET
STATUS = 'F',
JUSTIFICATIVA = '{$just}', 
ENVIADO = '{$enviado}',
QTD = '{$enviado}'
WHERE
ID = '{$ped}' ");
        }
        if (!$r) {
            echo "ERRO: Tente mais tarde!--1";
            mysql_query("rollback");
            return;
        }
        $justificativaNovoItem = "Item inserido pela Logistica  no lugar de:{$nomeItem} da solicitaçao {$ped}";
        if ($tipo == 'med') {
            $tipo = 0;
        } elseif ($tipo == 'mat') {
            $tipo = 1;
        } else {
            $tipo = 3;
        }

        if ($qtdAtual == $qtd || $qtd > $qtdAtual) {
            $sql = "
INSERT INTO itenspedidosinterno
(
  PEDIDOS_INTERNO_ID,
  NUMERO_TISS,
  CATALOGO_ID,
  QTD,
  TIPO,
  ENVIADO,
  STATUS,
  CONFIRMADO,
  CANCELADO,
  DATA_CANCELADO,
  CANCELADO_POR,
  JUSTIFICATIVA,
  EMERGENCIA,
  CANCELADO_ENTRADA,
  DATA_CANCELADO_ENTRADA,
  CANCELADO_ENTRADA_POR,
  JUSTIFICATIVA_ENTRADA
)
VALUES
(
  '{$pedIntId}',
  '{$tiss}',
  '{$item}',
  '{$qtd}',
  '{$tipo}',
  '0',
  'A',
  'S',
  'N',
  '0000-00-00 00:00:00',
  '0',
  '{$just}',
  '{$emergencia}',
  'N',
  '0000-00-00 00:00:00',
  '0',
  ''
)";
            $r = mysql_query($sql);

            if (!$r) {
                echo "ERRO: Tente mais tarde!--3";
                mysql_query("rollback");
                return;
            }

            $sql = "
INSERT INTO historico_troca_item
(
  catalogo_id_item,
  catalogo_id_item_substituto,
  user_id,
  motivo,
  data,
  item_pedido_interno_id,
  solicitacao_id,
  tipo
)
VALUES
(
  '{$catalogoIdAntigo}',
  '{$item}',
  '{$solicitante}',
  '{$justificativa}',
  NOW(),
  '{$ped}',
  '0',
  '{$tipo}'
)";
            $r = mysql_query($sql);
            if (!$r) {
                echo "ERRO: Tente mais tarde!--4";
                mysql_query("rollback");
                return;
            }

        } elseif ($qtdAtual > $qtd) {
            // INSERE A QUANTIDADE SOLICITADA
            $sql = "
INSERT INTO itenspedidosinterno
(
  PEDIDOS_INTERNO_ID,
  NUMERO_TISS,
  CATALOGO_ID,
  QTD,
  TIPO,
  ENVIADO,
  STATUS,
  CONFIRMADO,
  CANCELADO,
  DATA_CANCELADO,
  CANCELADO_POR,
  JUSTIFICATIVA,
  EMERGENCIA,
  CANCELADO_ENTRADA,
  DATA_CANCELADO_ENTRADA,
  CANCELADO_ENTRADA_POR,
  JUSTIFICATIVA_ENTRADA
)
VALUES
(
  '{$pedIntId}',
  '{$tiss}',
  '{$item}',
  '{$qtd}',
  '{$tipo}',
  '0',
  'A',
  'S',
  'N',
  '0000-00-00 00:00:00',
  '0',
  '{$just}',
  '{$emergencia}',
  'N',
  '0000-00-00 00:00:00',
  '0',
  ''
)";
            $r = mysql_query($sql);

            if (!$r) {
                echo "ERRO: Tente mais tarde!--3";
                mysql_query("rollback");
                return;
            }

            $idItemPedido = mysql_insert_id();

            $sql = "
INSERT INTO historico_troca_item
(
  catalogo_id_item,
  catalogo_id_item_substituto,
  user_id,
  motivo,
  data,
  item_pedido_interno_id,
  solicitacao_id,
  tipo
)
VALUES
(
  '{$catalogoIdAntigo}',
  '{$item}',
  '{$solicitante}',
  '{$justificativa}',
  NOW(),
  '{$ped}',
  '0',
  '{$tipo}'
)";
            $r = mysql_query($sql);
            if (!$r) {
                echo "ERRO: Tente mais tarde!--4";
                mysql_query("rollback");
                return;
            }

            // INSERE A QUANTIDADE SOBRESALENTE
            $qtdNova = $qtdAtual - $qtd;

            $sql = "
INSERT INTO itenspedidosinterno
(
  PEDIDOS_INTERNO_ID,
  NUMERO_TISS,
  CATALOGO_ID,
  QTD,
  TIPO,
  ENVIADO,
  STATUS,
  CONFIRMADO,
  CANCELADO,
  DATA_CANCELADO,
  CANCELADO_POR,
  JUSTIFICATIVA,
  EMERGENCIA,
  CANCELADO_ENTRADA,
  DATA_CANCELADO_ENTRADA,
  CANCELADO_ENTRADA_POR,
  JUSTIFICATIVA_ENTRADA
)
VALUES
(
  '{$pedIntId}',
  '{$tissAntigo}',
  '{$catalogoIdAntigo}',
  '{$qtdNova}',
  '{$tipo}',
  '0',
  'A',
  'S',
  'N',
  '0000-00-00 00:00:00',
  '0',
  '{$justAntiga}',
  '{$emergencia}',
  'N',
  '0000-00-00 00:00:00',
  '0',
  ''
)";
            $r = mysql_query($sql);

            if (!$r) {
                echo "ERRO: Tente mais tarde!--3";
                mysql_query("rollback");
                return;
            }

            $sql = "
INSERT INTO historico_troca_item
(
  catalogo_id_item,
  catalogo_id_item_substituto,
  user_id,
  motivo,
  data,
  item_pedido_interno_id,
  solicitacao_id,
  tipo
)
VALUES
(
  '{$catalogoIdAntigo}',
  '{$item}',
  '{$solicitante}',
  '{$justificativa}',
  NOW(),
  '{$idItemPedido}',
  '0',
  '{$tipo}'
)";
            $r = mysql_query($sql);
            if (!$r) {
                echo "ERRO: Tente mais tarde!--4";
                mysql_query("rollback");
                return;
            }
        }

        mysql_query("commit");
        echo "1";
        return;
    } else {
        echo "É necessário colocar uma quantidade maior que zero.";
    }
}

function salvar_pedido($dados, $observacao, $data_max) {
	//atual += quantidade+"#"+tipo+"#"+codigo;
	$usuario = $_SESSION['id_user'];
	$empresa = $_SESSION['empresa_principal'];
	$data_max = join("-",array_reverse(explode("/",$data_max)));
	
	mysql_query("begin");
	$sql = "INSERT INTO pedidosinterno (DATA, USUARIO, EMPRESA, STATUS, OBSERVACAO, DATA_MAX_ENTREGA) VALUES (now(),'{$usuario}','{$empresa}', 'A', '{$observacao}','{$data_max}') ";
		
	$r = mysql_query($sql);
	if(!$r){
		
		echo "1".$sql;
		mysql_query("rollback");
		return;
	}
	
	$id_pedidosinterno = mysql_insert_id();
	
	savelog(mysql_escape_string(addslashes($sql)));
	
	$pedidos = explode("$",$dados);
	
	foreach($pedidos as $p){
		if($p != "" && $p != null){
			$i = explode("#",$p);
			$quantidade = anti_injection($i[0],"literal");
			$tipo = anti_injection($i[1],"literal");
			$codigo = anti_injection($i[2],"literal");
			$emergencia = anti_injection($i[3],"literal");
			$catalogo_id = anti_injection($i[4],"numerico");
			if($quantidade > 0){
			$sql_itens = "INSERT INTO itenspedidosinterno (PEDIDOS_INTERNO_ID, NUMERO_TISS, QTD, TIPO, ENVIADO, STATUS,EMERGENCIA,CATALOGO_ID) 
			VALUES ('{$id_pedidosinterno}', '{$codigo}', '{$quantidade}', '{$tipo}', '0', 'A','{$emergencia}', '{$catalogo_id}')";
			
			$resultado_itens = mysql_query($sql_itens);
			
			if(!$resultado_itens){
				echo "1".$sql;
				mysql_query("rollback");
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
			}
		}
	}
	
	mysql_query("commit");
	echo "2";
}

function salvar_pedido_estoque($dados, $data_envio, $observacao) {
	$usuario = $_SESSION['id_user'];
	$empresa = $_SESSION['empresa_principal'];
	$data_envio = join("-",array_reverse(explode("/", $data_envio)));
	
	mysql_query("BEGIN");
	
	//Grava o pedido
	$sql_pedido = "INSERT INTO pedidosinterno (DATA, USUARIO, EMPRESA, STATUS, OBSERVACAO, DATA_MAX_ENTREGA) VALUES (NOW(), '{$usuario}', 
	'{$empresa}', 'A', '{$observacao}', '{$data_envio}')";
	
	$resultado_pedido = mysql_query($sql_pedido);
	
	if (!$resultado_pedido) {
		echo "Erro 1";
		mysql_query("ROLLBACK");
		return;
	}
	
	$id_pedido = mysql_insert_id();
	
	savelog(mysql_escape_string(addslashes($sql_pedido)));
	
	//Separa os itens
	$itens = explode("$", $dados);
	
	foreach($itens as $p){
		if($p != "" && $p != null){
			$i = explode("#", $p);
			$numero_tiss = anti_injection($i[0],"literal");
			$tipo = anti_injection($i[1],"literal");
			$quantidade = anti_injection($i[2],"literal");
            $catalogo_id = anti_injection($i[3],"numerico");
				
			$sql_itens = "INSERT INTO itenspedidosinterno (PEDIDOS_INTERNO_ID, NUMERO_TISS, QTD, TIPO, ENVIADO, STATUS,CATALOGO_ID)
			VALUES ('{$id_pedido}', '{$numero_tiss}', '{$quantidade}', '{$tipo}', '0', 'A','{$catalogo_id}')";
				
			$resultado_itens = mysql_query($sql_itens);
				
			if(!$resultado_itens){
				echo "Erro 2";
				mysql_query("ROLLBACK");
				return;
			}
			savelog(mysql_escape_string(addslashes($sql_itens)));
		}
	}
	
	mysql_query("COMMIT");
	echo 1;
	
}












function cancelar_entrada($post)
{


	$ur = $_SESSION['empresa_principal'];
  if(isset($ur) && !empty($ur)){
    $qtd = 'empresas_id_'.$ur;

  }else{
    echo 'Erro falta a Unidade Regional. Entre em contato com o TI.';
    return;
  }
	mysql_query("BEGIN");
	$sql = "SELECT
				CATALOGO_ID,qtd
			FROM
				entrada
			WHERE
				entrada.id = {$post}";
	$result = mysql_query($sql);
	while ($row = mysql_fetch_array($result)) {
		$catalogo_id = $row['CATALOGO_ID'];
		$quantidade = $row['qtd'];
	}
	
	$sql = "UPDATE entrada SET CANCELADO = 1, DATA_CANCELADO = now(), CANCELADO_POR = {$_SESSION['id_user']} WHERE id = {$post}";
	$sql2 = "UPDATE estoque set $qtd = ($qtd - $quantidade) WHERE CATALOGO_ID = $catalogo_id";

	if(!mysql_query($sql)){
		echo "ERRO: Tente mais tarde!";
		
		mysql_query("ROLLBACK");
		
	}
	savelog(mysql_real_escape_string(addslashes($sql)));

	if(!mysql_query($sql2)){
		echo "ERRO: Tente mais tarde!";
		
		mysql_query("ROLLBACK");
		
	}
	savelog(mysql_real_escape_string(addslashes($sql2)));
	mysql_query("COMMIT");	
	header('Location: index.php?op=buscar');
}


function cancelar_item_pedido($post){
        mysql_query("BEGIN");
	extract($post,EXTR_OVERWRITE);
	$user=$_SESSION['id_user'];
	$id_item = anti_injection($id_item,'numerico');
	$just = anti_injection($just,'literal');
	$just = "CANCELADO PELA LOG&Iacute;STICA: ".$just; 
	 $sql="UPDATE itenspedidosinterno  set STATUS= 'F',CANCELADO='S',CANCELADO_POR='{$user}', JUSTIFICATIVA = '{$just}',DATA_CANCELADO =now()  WHERE ID = '{$id_item}' ";
	if(!mysql_query($sql)){
		echo "ERRO: Tente mais tarde!".$user;
		
		mysql_query("ROLLBACK");
		
	}
	savelog(mysql_escape_string(addslashes($sql)));
	mysql_query("COMMIT");
	echo 1;
	
}

function cancelar_item_entrada_estoque($post){
 mysql_query("BEGIN");
	extract($post,EXTR_OVERWRITE);
	$user=$_SESSION['id_user'];
	$id_item = anti_injection($id_item,'numerico');
	$just = anti_injection($just,'literal');

	$just = "CANCELADO PELA LOG&Iacute;STICA: ".$just; 
	
	$r = mysql_query("UPDATE itenspedidosinterno  set STATUS= 'F',CANCELADO_ENTRADA ='S',CANCELADO_ENTRADA_POR='{$user}', JUSTIFICATIVA_ENTRADA = '{$just}',DATA_CANCELADO_ENTRADA =now()  WHERE ID = '{$id_item}' ");
	if(!$r){
		echo "ERRO: Tente mais tarde!".$user;
		mysql_query("ROLLBACK");
	}
	savelog(mysql_escape_string(addslashes($sql)));
	mysql_query("COMMIT");
	echo "1";
	
}
function pesquisa_entrada_saida($post){
    extract($post,EXTR_OVERWRITE);
    $ur = anti_injection($ur,'numerico');
    $inicio = anti_injection(join("-",array_reverse(explode("/",$inicio))),'literal');
    $fim = anti_injection(join("-",array_reverse(explode("/",$fim))),'literal');
    $sql ="SELECT
        i.CATALOGO_ID,
        (CASE i.TIPO when 0 then 'Medicamento' 
              when 1 then 'Material'
              when 3 then 'Dieta'
              end ) as tipo1,
        COALESCE(sum( i.CONFIRMADO ),0) AS enviado1,
        concat( c.principio, '', c.apresentacao ) AS nome1, 
            ( SELECT 
            COALESCE(sum( s.`quantidade` ),0) 
            FROM `saida` AS s
            INNER JOIN clientes AS cli ON ( s.`idCliente` = cli.`idClientes` )
            WHERE DATE_FORMAT( s.data, '%Y-%m-%d' )
            BETWEEN '{$inicio}'
            AND '{$fim}'
            AND `TIPO`
            IN ( 0, 1, 3 )
            AND cli.empresa =$ur
            AND s.CATALOGO_ID = i.CATALOGO_ID
            ) AS saidas1
        FROM `itenspedidosinterno` AS i
        INNER JOIN pedidosinterno AS p ON ( i.`PEDIDOS_INTERNO_ID` = p.ID )
        INNER JOIN catalogo AS c ON ( i.`CATALOGO_ID` = c.ID )
        WHERE DATE_FORMAT( p.DATA, '%Y-%m-%d' )
        BETWEEN '{$inicio }'
        AND '{$fim}'
        AND i.`TIPO`
        IN ( 0, 1, 3 )
        AND p.EMPRESA ={$ur}
        GROUP BY i.`CATALOGO_ID`
        ORDER BY i.TIPO, nome1
        ";
        $result= mysql_query($sql);
        $resposta = 1;
        if($resposta > 0){
            echo "<table class='mytable' width='95%'>";
            echo "<tr><thead><th><b>TIPO</b></th>";
            echo "<th><b>ITEM</b></th>";
            echo "<th><b>ENTRADA</b></th>";
            echo "<th><b>SAIDA</b></th>";
            echo "<th><b>ENTRADA - SAIDA</b></th></thead></tr>"; 
            while($row = mysql_fetch_array($result)){
               $subtracao=$row['enviado1'] - $row['saidas1'];
                echo"<tr><td>{$row['tipo1']}</td><td>{$row['nome1']}</td><td>{$row['enviado1']}</td><td>{$row['saidas1']}</td><td>$subtracao</td></tr>";
                
            }
            echo "</table>";
            
            
        }else{
                echo "<p><b>Nenhum resultado encontrado.</b></p>";
         }
    
    
}
function pesquisa_relatorio_lote($post){
    extract($post,EXTR_OVERWRITE);
    $catalogo_id= anti_injection($catalogo_id,'numerico');
    $lote= anti_injection($lote,'literal');
    $inicio = anti_injection(join("-",array_reverse(explode("/",$inicio))),'literal');
    $fim = anti_injection(join("-",array_reverse(explode("/",$fim))),'literal');
    
    
    
    
}

function pesquisa_equipamento_ativo($post){
    extract($post,EXTR_OVERWRITE);
    $paciente= anti_injection($paciente,'numerico');
    $convenio= anti_injection($convenio,'numerico');
    $tipo = anti_injection($tipo,'literal');

    $comp = " AND e.`PACIENTE_ID` = {$paciente} ";
    $trPaciente = "";
    if($paciente == '-1') {
        $comp = " AND cl.`convenio` = {$convenio} ";
        $trPaciente = "<th><b>PACIENTE</b></th>";
    }

  if ($tipo != 'devolucao' && $tipo != 'relatorio' ){
    echo "Ocorreu um erro entre em contato com o TI.";
    return ;
  }
    $sql ="SELECT 
        c.item,
        cl.nome AS paciente,
        DATE_FORMAT(e.DATA_INICIO,'%d/%m/%Y') as dt,
        e.lote,
        e.saida_id,
        e.ID,
        e.COBRANCA_PLANOS_ID,
        f.razaoSocial,
        e.fornecedor
        FROM 
        `equipamentosativos` as e inner join 
        cobrancaplanos as c on (e.`COBRANCA_PLANOS_ID` = c.id) left join
        fornecedores as f on e.fornecedor = f.idFornecedores
        INNER JOIN clientes AS cl ON e.PACIENTE_ID = cl.idClientes
        WHERE 
        e.`DATA_FIM` = '0000-00-00'
        AND e.`DATA_INICIO` <> '0000-00-00'
        AND e.`PRESCRICAO_AVALIACAO` <> 's'
        {$comp}
        order by c.item";
        $result= mysql_query($sql);
            echo "<table class='mytable' width='95%'>";
            echo "<tr>
                    <thead>
                      <th></th>
                      {$trPaciente}
                      <th><b>ITEM</b></th>
                      <th><b>DATA ENVIADO</b></th>
                      <th><b>FORNECEDOR</b></th>
                      <th><b>LOTE</b></th>
                      <th><b>DATA RECOLHIDO</b></th>
                    </thead>
                 </tr>";
            $contFinalizar =0;
            while($row = mysql_fetch_array($result)){
                $cancela ='';
                $check ='';
                $dataRecolher='';
                $lote ='';
              if($tipo == 'devolucao' ){
                $check = "<input type='checkbox' class='check-equipamento-ativo-devolver' id-equipamento-ativo='{$row['ID']}'
                            id-equipamento='{$row['COBRANCA_PLANOS_ID']}'
                            id-fornecedor='{$row['fornecedor']}'
                            nome-fornecedor='{$row['razaoSocial']}'
                            nome-equipamento= '{$row['item']}' 
                            nome-paciente = '{$row['paciente']}'
                            id-paciente = '{$paciente}' >";
                $dataRecolher="<input type='text' class='data-devolucao' readonly name='data'>";
                if (empty($row['lote'])) {
                  $lote = "<input type='text' class='lote' value='' name='lote'>";
                }else{
                  $lote= "<input type='text' class='lote' name='lote' disabled='disabled' value='{$row['lote']}'>";
                }

              }else {
                if (empty($row['saida_id'])) {
                  $check = "<input type='checkbox' class='check-equipamento-ativo-finalizar' id-equipamento-ativo='{$row['ID']}'
                                   id-equipamento='{$row['COBRANCA_PLANOS_ID']}'>";
                  $dataRecolher="<input type='text' class='data-devolucao' readonly name='data' >";
                  $contFinalizar++;
                }
                $lote= $row['lote'];
              }

                $rowPaciente = "";
              if($paciente == '-1') {
                  $rowPaciente = "<td>{$row['paciente']}</td>";
              }
                echo"<tr id='tr-equipamento-".$row['ID']."'>
                             <td>{$check}</td>
                             {$rowPaciente}
                            <td>{$row['item']}</td>
                            <td>{$row['dt']}</td>
                            <td>{$row['razaoSocial']}</td>
                            <td>$lote</td>
                            <td>$dataRecolher</td>
                            </tr>";
                
            }
            echo "</table>";
  if($tipo == 'devolucao' ){
    echo"<br/>
          <br/>
          <button id='devolver-equipamento-ativo' data-tipo='devolucao' data-idpaciente='{$paciente}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button' style='float:left;' type='button'>
              <span class='ui-button-text'>Salvar</span>
          </button>";
  }else{
    if($contFinalizar >0){
      echo"<br/>
          <br/>
          <button id='finalizar-equipamento-ativo' data-tipo='devolucao' data-idpaciente='{$paciente}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button' style='float:left;' type='button'>
              <span class='ui-button-text'>Salvar</span>
          </button>";
    }

  }
        
}
                
 function excluir_solicitacao_total($post) {
     extract($post,EXTR_OVERWRITE);
       $id_solicitacao = explode("#",$dados);
        mysql_query('begin');
        $usuario_id =$_SESSION["id_user"];
	foreach($id_solicitacao as $sol){
            if($sol != '' && $sol != null){
                $sol = anti_injection($sol,'numerico');
                $justificativa = anti_injection($justificativa,'literal');
                $just = "CANCELADO PELA LOG&Iacute;STICA: ".$justificativa; 
                $sql ="UPDATE solicitacoes SET status = '-2', obs = '{$just}',".
                      "CANCELADO_POR= '{$usuario_id}',DATA_CANCELADO= now()  WHERE idSolicitacoes = '{$sol}' ";
                 
                $r = mysql_query($sql);
                if(!$r){
                        echo "ERRO: Ao cancelar um item do pedido!";
                        mysql_query($rollback);
                        return; 
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }
        }
        mysql_query('commit');
	echo 1;
	return;
        
    }
    
    function excluir_pedido_total($post) {
     extract($post,EXTR_OVERWRITE);
       $id_itenspedidosinterno = explode("#",$dados);
        mysql_query('begin');
        $usuario_id =$_SESSION["id_user"];
	foreach($id_itenspedidosinterno as $id_item){
            if($id_item != '' && $id_item != null){
                $id_item= anti_injection($id_item,'numerico');
                $justificativa = anti_injection($justificativa,'literal');
                $just = "CANCELADO PELA LOG&Iacute;STICA: ".$justificativa; 
                $sql="UPDATE itenspedidosinterno  set STATUS= 'F',CANCELADO='S',CANCELADO_POR='{$usuario_id}',
                    JUSTIFICATIVA = '{$just}',DATA_CANCELADO =now()  WHERE ID = '{$id_item}' ";
                                
                $r = mysql_query($sql);
                if(!$r){
                        echo "ERRO: Ao cancelar um item do pedido!";
                        mysql_query($rollback);
                        return; 
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }
            
        }
        $sql = "Update pedidosinterno set STATUS='F' where ID='{$id_pedido}'";
        $r = mysql_query($sql);
                if(!$r){
                        echo "ERRO: Ao cancelar um item do pedido!";
                        mysql_query($rollback);
                        return; 
                }
                savelog(mysql_escape_string(addslashes($sql)));
        mysql_query('commit');
	echo 1;
	return;
        
    }
    
    function pesquisar_gerenciar_pedidos($post){
        extract($post,EXTR_OVERWRITE);
         $inicio = anti_injection(join("-",array_reverse(explode("/",$inicio))),'literal');
         $fim = anti_injection(join("-",array_reverse(explode("/",$fim))),'literal');
         
         $sql = "SELECT 
             p.ID,
             DATE_FORMAT(p.DATA, '%d/%m/%Y') as DATA,
             U.nome,
             DATE_FORMAT(p.DATA, '%H:%i:%S') as hora,
  		(CASE WHEN COUNT( * ) >1 THEN SUM( i.QTD ) ELSE i.QTD  END) AS	itens, 
                sum((case i.EMERGENCIA when 1 then 1 else 0 END)) as emergencia,
                 p.STATUS as statu,
                 p.CONFIRMADO,
                 p.CANCELADO_POR,
                 (select 
                   count(*) 
                   from  
                   itenspedidosinterno
                   where 
                   PEDIDOS_INTERNO_ID =p.ID and STATUS = 'P') AS pendente,
                  (select 
                   count(*) 
                   from  
                   itenspedidosinterno
                   where 
                   PEDIDOS_INTERNO_ID =p.ID and 
                   STATUS = 'F' AND 
                   ENVIADO >= CONFIRMADO AND
                   ENVIADO  >0)  as confir,
                   E.nome as empresa
                FROM
  		itenspedidosinterno AS i INNER JOIN
  		pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
  		inner join  usuarios AS U on (U.idUsuarios = p.USUARIO)
  		inner join empresas AS E on (p.EMPRESA = E.id)
  		WHERE 
                p.EMPRESA ={$empresa} and 
                p.DATA BETWEEN '{$inicio}' and '{$fim}'
                GROUP BY 
                p.ID 
                order by id ";
              
  		 
  		$r = mysql_query($sql);
  		 $resultado = mysql_num_rows($r);

	
	if($resultado != 0){
  		while($row = mysql_fetch_array($r)){
  		foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
  		$vazio = false;
  		
  		if($row['emergencia'] > 0){
	  		$hora = "<b>EMERG&Ecirc;NCIA</b> <img src='../utils/sirene_ativa.gif' style='width:10px;' />";
	  		
	  	}else{
	  		$hora = $row['hora'];
	  		
	  	}
                $st= '';
                if($row['CONFIRMADO'] == 'N' && $row['CANCELADO_POR'] == 0){
                    $st .='Pedido ainda nÃo foi Confirmado. </br>';
                }else if($row['CONFIRMADO'] == 'S' && $row['pendente']<=0){
                    $st .='Pedido enviado para Mederi Central. </br>';
                }else if($row['pendente'] > 0) {
                    $st .='Mederi Central falta enviar alguns itens. </br>';
                } else if($row['confir'] >0 ){
                    $st .="{$row['empresa']} falta confirmar alguns itens. </br>";
                } else if($row['CANCELADO_POR'] != 0){
                    $st = 'Pedido Cancelado.';
                }

                    $html .= "<div style='border:1px solid #fff;width:240px;display:table;float:left;margin:0 auto;padding:15px; font-size:12px;'>
                    <table width='100%'>
                    <tr>
                    <td>
                    <center><button class='bpedido-gerenciar' p='{$row['ID']}' idempresa='{$p}' presc='{$presc}' tipo='1' url='{$url}'></button></center>
                    </td>
                    </tr>
                     <tr>
                      <td>
                      <center><b> PEDIDO N&deg;".strtoUpper($row['ID'])."</b></center>
                      </td>
                    </tr>
                    <tr>
                      <td>
                      <center><b> STATUS: ".strtoUpper($st)."</b></center>
                      </td>
                    </tr>
                     <tr>
                    <td>
                    <center style='font-size:9px;'>(".strtoUpper($row['nome']).")</center>
                    </td>
                    </tr>
                    <tr>
                    <td>
                    <center>".$row['DATA']."</center>
                      </td>
                      </tr>
                      <tr>
                      <td>
                      <center style='font-size:9px;'>".$hora."</center>
                    </td>
                    </tr>
                    </tr>
                            <tr>
                     <td>".
                      utf8_encode("<center style='color:red;'>itens (".$row['itens'].")</center>")
                    ."</td>
                    </tr>
                     </table>
                    </div>";
        
              }
        echo $html;
        }else{
            echo "Nenhum Pedido foi encontrado.";
        }
   
     return;
    }
function pesquisarEquipamentosAtivosRecolher($post){


  extract($post,EXTR_OVERWRITE);
  $idPaciente = anti_injection($idPaciente,'numerico');
  $idItem = anti_injection($idItem,'numerico');
  $sql= "SELECT
        equipamentosativos.*,
        cobrancaplanos.item,
        DATE_FORMAT(equipamentosativos.DATA_INICIO,'%d/%m/%Y') as datapt,
        fornecedores.razaoSocial as nomeFornecedor
    FROM
        equipamentosativos
    INNER JOIN cobrancaplanos ON (
        equipamentosativos.COBRANCA_PLANOS_ID = cobrancaplanos.id
    )
    LEFT JOIN fornecedores on equipamentosativos.fornecedor = fornecedores.idFornecedores
    WHERE
        equipamentosativos.PACIENTE_ID = {$idPaciente}
    AND equipamentosativos.COBRANCA_PLANOS_ID = {$idItem}
    AND equipamentosativos.DATA_FIM = '0000-00-00 00:00:00'
    AND equipamentosativos.DATA_INICIO > '0000-00-00 00:00:00'
    ORDER BY
	equipamentosativos.DATA_INICIO DESC";
  $result =mysql_query($sql);

  if(!$result){
    echo "Erro ao fazer pesquisa";
    return ;
  }
     $quantidadeLinha= mysql_num_rows($result);

  if($quantidadeLinha == 0){
    echo "Esse equipamento não está mais ativo, cancele a solicitação. ";
    return ;
  }
    echo "
<div id='div-tabela-recolher-equipamento'>
<table class='mytable'>
        <thead>
             <tr>
                <th>Item</th>
                <th>Lote</th>
                <th>Fornecedor</th>
                <th>Devolvido em</th>
            </tr>        
        </thead>";

    while($row =mysql_fetch_array($result)){
        echo "<tr>
                 <td> <input type='checkbox' name='{$row['ID']}' class='check-recolhe-equipamento' style='display:inline'
                       id-equipamento-ativo='{$row['ID']}' />
                    {$row['item']} enviado em: {$row['datapt']}
                </td>
                <td>{$row['lote']}</td>
                <td>{$row['nomeFornecedor']}</td>
                <td>
                    <input type='text' class='data-devolucao'  disabled />                
                </td>
                     

              </tr>";
    }
    echo "</table>
</div>";
return  ;


}

/**
 * @param $post
 */
function desativarEquipamentosAtivosRecolher($post)
{
    extract($post, EXTR_OVERWRITE);
    $idSolicitacao = anti_injection($idSolicitacao, 'numerico');
    $dados = anti_injection($dados, 'literal');
    $arrayTextoEmailEquipamento = [];
    $arrayDataRecolher = json_decode($jsonData,true);
    $arrayData = [];


    mysql_query('begin');
    $sql = '';
    foreach ($arrayDataRecolher as $recolher){

        $dataRecolher = \DateTime::createFromFormat('d/m/Y', $recolher['data'])->format('Y-m-d');

        $sql = "UPDATE equipamentosativos SET DATA_FIM = '{$dataRecolher}', finalizado_em = now(),
                finalizado_por='{$_SESSION['id_user']}'  WHERE id = {$recolher['id']} ";
        $arrayData[$recolher['id']] = $recolher['data'];
        $r = mysql_query($sql);
        if (!$r) {
            echo "ERRO: Ao tentar colocar data final nos equipamentos ativos.";
            mysql_query("rollback");
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));

    }

    if (!empty($idSolicitacao)) {

    $sql = "UPDATE solicitacoes SET enviado = qtd  WHERE idSolicitacoes = {$idSolicitacao}";


    $r = mysql_query($sql);
    if (!$r) {
        echo "ERRO: Ao finalizar solicitação de recolhimneto.";
        mysql_query("rollback");
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));
    }
    $sql= "select 
e.* ,
c.item as nomeEquipamento,
f.razaoSocial as nomeFornecedor,
cli.nome as nomePaciente
from 
equipamentosativos as e 
INNER JOIN  cobrancaplanos as c on (e.COBRANCA_PLANOS_ID = c.id)
INNER JOIN  clientes as cli on (e.PACIENTE_ID = cli.idClientes)
LEFT JOIN fornecedores as f on (e.fornecedor = f.idFornecedores)
where 
e.ID in ($dados)";

    $r = mysql_query($sql);
     if(!$r){
         echo "ERRO: Na pesquisa para trazer equipamentos.";
         mysql_query("rollback");
         return;
     }
    while($row= mysql_fetch_array($r)) {
        $dadosEnvio['paciente'] = $row['nomePaciente'];
        $dadosEnvio['data'] = date('d/m/Y');
        $dadosEnvio['nomeEmpresa'] = $_SESSION["nome_empresa"];

        $arrayTextoEmailEquipamento[$row['fornecedor']]['devolucao'] = empty($arrayTextoEmailEquipamento[$row['fornecedor']]['devolucao'])
            ? "<tr><td>{$row['nomeEquipamento']}</td><td>{$row['lote']}</td><td>{$arrayData[$row['ID']]}</td></tr>"
            :$arrayTextoEmailEquipamento[$row['fornecedor']]['devolucao']."<tr><td>{$row['nomeEquipamento']}</td>
                                                                                <td>{$row['lote']}</td>
                                                                                <td>{$arrayData[$row['ID']]}</td>
                                                                                </tr>";
        $arrayTextoEmailEquipamento[$row['fornecedor']]['nomeFornecedor'] = $row['nomeFornecedor'];

        $s = "INSERT INTO
              devolucao (
               `TIPO`,
               `QUANTIDADE`,
               `DATA`,
                `PACIENTE_ID`,
               `USUARIO_ID`,
               `DATA_DEVOLUCAO`,
                NUMERO_TISS,
                LOTE,
                CATALOGO_ID)
                    VALUES
                    ( 5,
                    '{$row['QTD']}',
                    now(),
                    '{$row['PACIENTE_ID']}',
                     '{$_SESSION['id_user']}',
                     now(),
                     '{$row['COBRANCA_PLANOS_ID']}',
                     '{$row['lote']}',
                     '{$row['COBRANCA_PLANOS_ID']}'
                     )";

        $r =mysql_query($s);
        if(!$r) {
            echo "erro: Ao gerar devolução.";
            mysql_query("rollback");

            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));

    }
    if(!empty($arrayTextoEmailEquipamento)){
        emailControleEquipamento($arrayTextoEmailEquipamento, $dadosEnvio);
    }


mysql_query('commit');

echo 1;
}


function devolverEquipamentoAtivo($post){
  extract($post, EXTR_OVERWRITE);
  $paciente = anti_injection($paciente, 'numerico');
  $equipamentosDevolver = explode('$',$dados);
  mysql_query('begin');
    $arrayTextoEmailEquipamento =[];

  foreach($equipamentosDevolver as $equipamento){
    if($equipamento != '' && $equipamento!= null ){
      $eqp = explode('#',$equipamento);
      $idEquipamentoAtivo = anti_injection($eqp[0], 'numerico');
      $idEquipamento = anti_injection($eqp[1], 'numerico');
      $lote =   anti_injection($eqp[1], 'literal');
      $dataBr= anti_injection($eqp[3], 'literal');
      $data = anti_injection(join("-",array_reverse(explode("/",$eqp[3]))),'literal');
      $nomePaciente = anti_injection($eqp[7], 'literal');
      $nomeFornecedor = anti_injection($eqp[6], 'literal');
    $idFornecedor = anti_injection($eqp[5], 'literal');
    $nomeEquipamento= anti_injection($eqp[4], 'literal');

      $sql = "UPDATE equipamentosativos SET DATA_FIM = '$data', finalizado_em = now(),
                finalizado_por='{$_SESSION['id_user']}' WHERE id = {$idEquipamentoAtivo} ";
        $dadosEnvio['paciente'] = $nomePaciente;
        $dadosEnvio['data'] = date('d/m/Y');
        $dadosEnvio['nomeEmpresa'] = $_SESSION["nome_empresa"];

        $arrayTextoEmailEquipamento[$idFornecedor]['devolucao'] = empty($arrayTextoEmailEquipamento[$idFornecedor]['devolucao'])
            ? "<tr><td>{$nomeEquipamento}</td><td>{$lote}</td><td>{$dataBr}</td></tr>"
            :$arrayTextoEmailEquipamento[$idFornecedor]['devolucao']."<tr><td>{$nomeEquipamento}</td>
                                                                                <td>{$lote}</td>
                                                                                <td>{$dataBr}</td>
                                                                                </tr>";
        $arrayTextoEmailEquipamento[$idFornecedor]['nomeFornecedor'] = $nomeFornecedor;


        $r = mysql_query($sql);
      if (!$r) {
        echo "ERRO: Ao tentar finalizar os equipamentos ativos.";
        mysql_query("rollback");
        return;
      }
      savelog(mysql_escape_string(addslashes($sql)));
      $s = "INSERT INTO
              devolucao (
               `TIPO`,
               `QUANTIDADE`,
               `DATA`,
                `PACIENTE_ID`,
               `USUARIO_ID`,
               `DATA_DEVOLUCAO`,
                NUMERO_TISS,
                LOTE,
                CATALOGO_ID)
                    VALUES
                    ( 5,
                    1,
                    now(),
                    '{$paciente}',
                     '{$_SESSION['id_user']}',
                     '{$data}',
                     '{$idEquipamento}',
                     '{$lote}',
                     '{$idEquipamento}'
                     )";

      if(!mysql_query($s)) {
        echo "erro: Ao gerar devolução.";
        mysql_query("rollback");

        return;
      }
      savelog(mysql_escape_string(addslashes($sql)));

    }

  }

  mysql_query('commit');
    if(!empty($arrayTextoEmailEquipamento)){
        emailControleEquipamento($arrayTextoEmailEquipamento, $dadosEnvio);
    }

  echo 1;

}


function finalizarEquipamentoAtivo($post){
  extract($post, EXTR_OVERWRITE);
  $paciente = anti_injection($paciente, 'numerico');
  $equipamentosFinalizar = explode('$',$dados);
  mysql_query('begin');

  foreach($equipamentosFinalizar as $equipamento){
    if($equipamento != '' && $equipamento!= null ){
      $eqp = explode('#',$equipamento);
      $idEquipamentoAtivo = anti_injection($eqp[0], 'numerico');
      $data = anti_injection(join("-",array_reverse(explode("/",$eqp[1]))),'literal');
      $sql = "UPDATE equipamentosativos SET DATA_FIM = '$data', finalizado_em = now(),
                finalizado_por='{$_SESSION['id_user']}' WHERE id = {$idEquipamentoAtivo} ";
      $r = mysql_query($sql);
      if (!$r) {
        echo "ERRO: Ao tentar finalizar os equipamentos ativos.";
        mysql_query("rollback");
        return;
      }
      savelog(mysql_escape_string(addslashes($sql)));

    }

  }
  mysql_query('commit');

  echo 1;

}

function emailControleEquipamento($arrayEmailEquipamento, $dadosEnvio){

    $titulo = "Aviso Sismederi Controle de Movimentação de Equipamentos";

    foreach ($arrayEmailEquipamento as $fornecedorId => $array){
        $observacao = '';
        $texto = '';
        $resultEmailFornecedorJson = \App\Models\Financeiro\Fornecedor::getEmailFornecedorEquipmento($fornecedorId);
        $emails = '';
        $para = [];
        $arrayEmailFornecedor = json_decode($resultEmailFornecedorJson,true);
        $para[] = ['email' => 'fabiosantana@assistevida.com', 'nome'=> 'Fábio Santana'];

        if(!empty($arrayEmailFornecedor)){
            foreach ($arrayEmailFornecedor as $email){
                $para[] = ['email' => "{$email['email']}", 'nome'=> "{$array['nomeFornecedor']}"];
                $emails .= $email['email']."<br>";
            }
        }else{
            $observacao = "<p>
                            <b>Observação: </b> 
                            O fornecedor não tem e-mail cadastrado no sistema, esse e-mail não foi enviado para ele.
                            </p>";
        }
        if(isset($array['envio']) && !empty($array['envio']) && !isset($array['devolucao'])){
            $texto .= "<p>
                        Comunicado da Mederi para o fornecedor <b>{$array['nomeFornecedor']}</b>
                    </p>
                    <p>
                        Comunicamos para fins de registro e controle que o(s) equipamento(s) abaixo foi(ram) 
                        implantado(s) no homecare do paciente <b>{$dadosEnvio['paciente']}</b>
                        na data <b>{$dadosEnvio['data']}</b>, por <b>{$_SESSION['nome_user']}</b>
                        na remessa <b>{$dadosEnvio['remessa']}</b>, Unidade Regional <b>{$dadosEnvio['nomeEmpresa']}</b>
                     </p>
                    <p>
                     <b>Envio:</b>
                     <table border='1' cellspacing=0  bordercolor='black'>
                     <tr bgcolor='#d3d3d3'>
                     <td><b>Equipamento</b></td>
                     <td><b>Tombo</b></td>
                    </tr>
                    {$array['envio']}
                    </table>
                    </p>";
            $texto .= "<p>
                            A cobrança da locação iniciará a partir da data de implantação informada neste e-mail.
                            <br>
                             Caso seja constatada alguma divergência ou deseje maiores informações, favor contactar o setor de logística da UR.                        
                        </p>";
        }else{
            $texto .= "<p>
                        Comunicado da Mederi para o fornecedor <b>{$array['nomeFornecedor']}</b>
                    </p>
                    <p>
                        Comunicamos para fins de registro e controle que o(s) equipamento(s) abaixo foi(ram) 
                        recolhido(s) no homecare do paciente <b>{$dadosEnvio['paciente']}</b>
                        , por <b>{$_SESSION['nome_user']}</b>, lançado no sistema em <b>{$dadosEnvio['data']}</b>
                         Unidade Regional <b>{$dadosEnvio['nomeEmpresa']}</b>
                     </p>
                    <p>
                     <b>Devolução:</b>
                     <table border='1' cellspacing=0  bordercolor='black'>
                     <tr bgcolor='#d3d3d3'>
                     <td><b>Equipamento</b></td>
                     <td><b>Tombo</b></td>
                     <td><b>Devolvido Em</b></td>
                    </tr>
                    {$array['devolucao']}
                    </table>
                    </p>";
            $texto .= "<p>
                            A cobrança da locação será interrompida a partir da data de implantação informada neste e-mail.
                             Caso seja constatada alguma divergência ou deseje maiores informações, favor contactar o setor de logística da UR.                        
                        </p>";

        }
        $texto.= $observacao;


        SendGrid::enviarEmailSimples($titulo, $texto, $para);
    }

    return;


}

function ajustarQuantidade($post){
    extract($post, EXTR_OVERWRITE);
    $quantidade     = anti_injection($quantidade, 'numerico');
    $solicitacao    = anti_injection($sol, 'numerico');
    $justificativa  = anti_injection($just, 'literal');
    mysql_query('begin');
    $sql = "
    Insert INTO
     `historico_ajuste_autorizado_solicitacao`
     (
        `solicitacoes_id`,
        `justificativa`,
        `quantidade`,
        `created_at`,
        `created_by`
        )
        values
        ({$solicitacao},
        '{$justificativa}',
        {$quantidade},
        now(),
        {$_SESSION['id_user']}
        )";
    $result = mysql_query($sql);
    if(!$result){
        mysql_query('rolback');
        echo "Erro ao salvar ajuste.".$sql;
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));

    $sql = "
        Update
        solicitacoes
        set
        solicitacoes.autorizado = (solicitacoes.autorizado - $quantidade)
        WHERE
           solicitacoes.idSolicitacoes = {$solicitacao}
    ";
    $result = mysql_query($sql);
    if(!$result){
        mysql_query('rolback');
        echo "Erro ao atualizar a quantidade autorizada.";
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));
    mysql_query('commit');
    echo 1;

}



if(isset($_POST['query'])){
  switch($_POST['query']){
    case "inicio_inventario":
      inicio_inventario($_POST['tipo']);
      break;
    case "remover_especial":
      remover_especial($_POST['id']);
      break;
    case  "op-itens":
      opcoes_itens($_POST);
      break;
    case "salvar-devolucao":
      salvar_devolucao($_POST);
      break;
    case "combo-rastreamento":
      combo_rastreamento($_POST['tipo']);
      break;
    case "combo-busca-saida":
      combo_busca_saida($_POST['tipo'],$_POST['origem']);
      break;
    case "envio-paciente":
      envio_paciente($_POST);
      break;
    case "cancelar-item-solicitacao":
      cancelar_item_solicitacao($_POST);
      break;
    case "busca-itens-troca":
    	busca_itens($_POST['like'],$_POST['tipo']);
  	break;
  	case "trocar-item-solicitacao":
      trocar_item_solicitacao($_POST);
    break;
    case "trocar-item-pedido-interno":
      trocar_item_pedido_interno($_POST);
      break;
    case "estoque":
    	pes_estoque($_POST['ur'],$_POST['cod'],$_POST['todos'],$_POST['tipo'],$_POST['maiorQueZero']);
    	break;
    case "brasindice":
    	brasindice($_POST['ur'],$_POST['item'], $_POST['tipo'], $_POST['status']);
    	break;
    case "novo_brasindice":
    	novo_brasindice($_POST['principio'],$_POST['apresentacao'], $_POST['laboratorio'], $_POST['numero_tiss'], $_POST['nome_simpro_brasindice'],$_POST['tipo_brasindice'], $_POST['tag_brasindice'],$_POST['status'], $_POST['usuario'], $_POST['data'],
            $_POST['controlado'],$_POST['antibiotico'],$_POST['referencia'], $_POST['id_simpro_brasindice'], $_POST['numero_tuss'],$_POST['id_principio'],$_POST['generico'],$_POST['categoria_material'],$_POST['possui_generico']);
    	break;
    case "editar_brasindice":
   		editar_brasindice($_POST['principio'],$_POST['apresentacao'], $_POST['laboratorio'], $_POST['numero_tiss'],
            $_POST['tipo_brasindice'], $_POST['tag_brasindice'],$_POST['status'], $_POST['usuario'], $_POST['data'],
            $_POST['controlado'],$_POST['antibiotico'],$_POST['catalogo_id'],$_POST['referencia'], $_POST['nome_simpro'],
            $_POST['numero_tuss'],$_POST['id_principio'],$_POST['generico'], $_POST['principio_antigo'],
            $_POST['descontinuado'], $_POST['categoria_material'], $_POST['possui_generico']);
    	break;
    case "edt_estoque":
    	edt_estoque($_POST['dados']);
    	break;
    case "salvar-pedido":
    	salvar_pedido($_POST['dados'],$_POST['observacao'], $_POST['data_max']);
    	break;
    case "salvar-envio-pedido":
    	salvar_envio_pedido($_POST['dados'],$_POST['id'], $_POST['data'],$_POST['empresa']);
    	break;
    case "salvar-pedido-estoque":
    	salvar_pedido_estoque($_POST['dados'], $_POST['data_envio'], $_POST['observacao']);
    	break;
   	case "salvar-entrada-estoque":
    	salvar_entrada_pedido($_POST['dados'],$_POST['id'], $_POST['data'],$_POST['empresa']);
    	break;
    	case "cancelar-item-pedido":
      cancelar_item_pedido($_POST);
      break;
      case "cancelar-item-entrada-estoque":
      cancelar_item_entrada_estoque($_POST);
      break;
      case "pesquisa-entrada-saida":
      pesquisa_entrada_saida($_POST);
      break;
      case "pesquisa-relatorio-lote":
      pesquisa_relatorio_lote($_POST);
      break;
     case "pesquisa-equipamento-ativo":
      pesquisa_equipamento_ativo($_POST);
      break;
      case "excluir-solicitacao-total":
      excluir_solicitacao_total($_POST);
      break;
      case "excluir-pedido-total":
       excluir_pedido_total($_POST);
      break;
    case "pesquisar-gerenciar-pedidos":
        pesquisar_gerenciar_pedidos($_POST);
        break;
    case "pesquisar-equipamentos-ativos-recolher":
      pesquisarEquipamentosAtivosRecolher($_POST);
      break;
      case "desativar-equipamentos-ativos-recolher":
          desativarEquipamentosAtivosRecolher($_POST);
          break;
    case "devolver-equipamento-ativo":
      devolverEquipamentoAtivo($_POST);
      break;
    case "finalizar-equipamento-ativo":
      finalizarEquipamentoAtivo($_POST);
      break;
  case "ajustar-quantidade":
      ajustarQuantidade($_POST);
      break;
  }
}
if(isset($_GET['query'])){
  switch($_GET['query']){
    case "pdf_especiais":
      pdf_especiais(implode("-",array_reverse(explode("/",$_GET['venc']))));
      break;
    case "inicio_inventario":
      inicio_inventario($_GET['tipo']);
      break;
    case "inventario":
      inventario($_GET['tipo'],$_GET['zerados'],$_GET['offset'],$_GET['nome']);
      break;
    case "relatorio":
      relatorio($_GET);
      break;


  }
}

if(isset($_GET['act'])) {
	switch ($_GET['act']) {
		case 'cancelar_entrada':
			cancelar_entrada($_POST['idnota']);
			break;
	}

}

?>