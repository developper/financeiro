<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';


define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);


$routes = [
    "GET" => [
        '/logistica/PadronizarMedicamento/?action=menu' =>'\App\Controllers\PadronizarMedicamento::menu',
        '/logistica/PadronizarMedicamento/?action=principio-ativo-inserir' =>'\App\Controllers\PadronizarMedicamento::principioAtivo',
        '/logistica/PadronizarMedicamento/?action=principio-ativo-editar' =>'\App\Controllers\PadronizarMedicamento::principioAtivoEditar',

    ],
    "POST" => [
        '/logistica/PadronizarMedicamento/?action=inserir-principio-ativo' =>'\App\Controllers\PadronizarMedicamento::inserirPrincipioAtivo',
        '/logistica/PadronizarMedicamento/?action=editar-principio-ativo' =>'\App\Controllers\PadronizarMedicamento::editarPrincipioAtivo'

    ]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
    $app = new App\Application;
    $app->setRoutes($routes);
    $app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
    echo $e->getMessage();
} catch(App\NotFoundException $e) {
    http_response_code(404);
    echo $e->getMessage();
}