<?php
$id = session_id();
if(empty($id))
    session_start();
include_once('../validar.php');
require_once('../db/config.php');
require_once('../utils/codigos.php');
include_once('../utils/mpdf/mpdf.php');

use App\Models\Logistica\Saidas;
use App\Models\Administracao\Filial;

function salvarEnvioPedido($dados, $id, $data, $ur) {
    //atual += quantidade+"#"+tipo+"#"+codigo;
    $usuario = $_SESSION['id_user'];
    $empresa = $_SESSION['empresa_principal'];
    $data_pedido = join("-",array_reverse(explode("/",$data)));
    mysql_query('BEGIN');
    if(isset($ur) && !empty($ur)){
        $condur = 'empresas_id_'.$ur;

    }else{
        echo 'Erro falta a Unidade Regional. Entre em contato com o TI.';
        return;
    }
    $sqlNomeEmpresa ="select nome from empresas where id = {$ur}";
    $resultNomeEmpresa = mysql_query($sqlNomeEmpresa);

    $nomeEmpresa = mysql_result($resultNomeEmpresa,0);

    $qtd="empresas_id_".$empresa;
    $pedidos = explode("$",$dados);

    $tabela = "<table border=1 width=100% style='font-size:x-small' >";
    $tabela .= "<thead>
                <tr>
                <th><b>Nome</b></th>";
    $tabela .= "<th><b>Apresenta&ccedil;&atilde;o</b></th>";
    $tabela .= "<th><b>Quantidade</b></th>";
    $tabela .= "<th><b>Lote</b></th>";
    $tabela .= "<th><b>Data</b></th>
                </tr>
                </thead>";

    $resultRemessaUr = Saidas::getMaxRemessaByUR($empresa);
    $resultEmpresaUr = Filial::getUnidadesUsuario('('.$empresa.')');
    $siglaSaidaUr = $resultEmpresaUr[0]['sigla_saida'];

    if(empty($resultRemessaUr)){
        $maxRemessaUr = 1;
    }else{
        $maxRemessaUr = $resultRemessaUr[0]['numerica'] + 1;
    }
    $remessaUr = $siglaSaidaUr.'-'.str_pad($maxRemessaUr, 6, "0", STR_PAD_LEFT);
    // Criar remessa para colocar na saída.
    $insertRemessa = "insert into 
                                          saida_remessa 
                                          (alfa, numerica, empresa_id, remessa, tipo_saida ) 
                                          value 
                                          ('{$siglaSaidaUr}', '{$maxRemessaUr}', '{$empresa}', '{$remessaUr}', 'UR')";
    $resultRemessa = mysql_query($insertRemessa);
    if(!$resultRemessa){
        echo "-1#&ERRO: 01 Problema ao gerar remessa de saída!";
        mysql_query("rollback");
        return;
    }
    $idRemessaUr = mysql_insert_id();
    savelog(mysql_escape_string(addslashes($insertRemessa)));


    foreach($pedidos as $p){
        if($p != "" && $p != null){
            $i = explode("#",$p);
            $quantidade = anti_injection($i[0],"literal");
            $id_item = anti_injection($i[1],"literal");
            $tot = anti_injection($i[2],"literal");
            $tiss = anti_injection($i[3],"literal");
            $lote = anti_injection($i[4],"literal");
            $tipo = anti_injection($i[5],"literal");
            $n = anti_injection($i[6],"literal");
            $sn = anti_injection($i[7],"literal");
            $catalogo_id = anti_injection($i[8],"numerico");

            $tabela .= "<tr><td>{$i[6]}</td><td>{$i[7]}</td><td>{$i[0]}</td><td>$lote</td><td>{$data}</td></tr>";


            if(($tot - quantidade )<= 0){

                $st ='F';
            }else{
                $st ='P';
            }
            if($quantidade){
                $sql_itens1 = "update  itenspedidosinterno set   ENVIADO =('{$quantidade}' + ENVIADO) , STATUS = (case when (ENVIADO) >= QTD then 'F' else 'P' END) where ID ={$id_item}";

                $resultado_itens1 = mysql_query($sql_itens1);

                if(!$resultado_itens1){
                    echo "erro 13";

                    mysql_query('ROLLBACK');
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql_itens1)));
            }
            $sql_itens = "update estoque set  {$qtd} = ( {$qtd} - {$quantidade} )  where CATALOGO_ID ={$catalogo_id} and TIPO={$tipo}";

            $resultado_itens = mysql_query($sql_itens);
            if(!$resultado_itens){
                echo 'Erro ao atualizar estoque';
                mysql_query('ROLLBACK');
                return;
            }
            savelog(mysql_escape_string(addslashes($sql_itens)));
            if($quantidade != 0){
                $sql = "INSERT INTO saida (NUMERO_TISS,tipo,quantidade,idUsuario, data,LOTE,ITENSPEDIDOSINTERNOS_ID,UR,DATA_SAIDA,nome,segundoNome,CATALOGO_ID,DATA_SISTEMA,modelo,remessa,saida_remessa_id)
		VALUES ('{$tiss}','{$tipo}','{$quantidade}','{$_SESSION['id_user']}',now(),'{$lote}','{$id_item}','{$ur}','{$data_pedido}','{$n}','{$sn}','{$catalogo_id}',now(),'v2','{$remessaUr}','{$idRemessaUr}') ";
                $a = mysql_query($sql);
                if(!$a){
                    echo "ERRO: Tente mais tarde!--7";
                    mysql_query('ROLLBACK');
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }

        }


    }


    $sql = "select count(*) as c from  itenspedidosinterno where  (STATUS = 'A' OR STATUS = 'P') and PEDIDOS_INTERNO_ID = {$id} ";

    $r = mysql_query($sql);

    if(!$r){

        echo "erro 12";
        mysql_query("'ROLLBACK'");
        return;
    }

    while($row = mysql_fetch_array($r)){

        if($row['c'] == 0){
            $sql = "UPDATE pedidosinterno SET status = 'F' WHERE ID = '{$id}'";
            $r =mysql_query($sql);
            if(!$r){

                echo "erro 13";
                mysql_query("'ROLLBACK'");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }else{
            $sql = "UPDATE pedidosinterno SET status = 'P' WHERE ID = '{$id}'";
            $r= mysql_query($sql);
            if(!r){

                echo "erro 14";
                mysql_query("'ROLLBACK'");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }


    //Tabela Controlados
    $tabela .= "</table>";
    $dia= date('d/m/Y');
    $html = "<div><table align='center' width=100% border=0 >";
    $html .= "<tr><td>Empresa:</td><td>Data:</td><td>Respons&aacute;vel pela sa&iacute;da:</td></tr>";
    $usuario = ucwords(strtolower($_SESSION["nome_user"]));
    $html .= "<tr><td>{$nomeEmpresa}</td><td>{$data}</td><td>{$usuario}</td></tr>";
    $html .= "<tr><td colspan='3'> Remessa: <b>{$remessaUr}</b></td></tr>";
    $html .= "</table>";
    $html .= "<p><b>Relat&oacute;rio</b>";
    $html .= $tabela;
    $html .= "<br /><br /><table>
                  <tr>
                      <td>_______________________________________</td>
                      <td></td>
                      <td>___/___/____</td>
                      <td></td>
                      <td>__:__:__</td>
                   </tr>
                   <tr>
                       <td>Assinatura do " . htmlentities("Responsável") . "</td>
                       <td></td>
                       <td>Data</td>
                       <td></td>
                       <td>Hora</td>
                   </tr>

               </table>";
    $html .= "</div>";


    mysql_query('COMMIT');
    echo $html;
}

function salvarEntradaPedido($dados, $id, $data, $ur, $lote){
    //atual += quantidade+"#"+tipo+"#"+codigo;
    $usuario = $_SESSION['id_user'];
    $empresa = $_SESSION['empresa_principal'];
    $data_pedido = join("-",array_reverse(explode("/",$data)));
    mysql_query('BEGIN');
    if(isset($ur) && !empty($ur)){
        $qtd = 'empresas_id_'.$ur;

    }else{
        echo 'Erro falta a Unidade Regional. Entre em contato com o TI.';
        return;
    }



    $pedidos = explode("$",$dados);

    foreach($pedidos as $p){
        if($p != "" && $p != null){
            $i = explode("#",$p);
            $quantidade = anti_injection($i[0],"literal");
            $id_item = anti_injection($i[1],"literal");
            $tot = anti_injection($i[2],"literal");
            $tiss = anti_injection($i[3],"literal");
            $lote = anti_injection($i[4],"literal");
            $catalogo_id = anti_injection($i[5],"numerico");
            $tipo = anti_injection($i[6],"numerico");
            $saidaId = anti_injection($i[7],"numerico");
            $saidaQuantidade = anti_injection($i[8],"numerico");
            $saidaConfirmados = anti_injection($i[9],"numerico");

            if($quantidade >0){
                $sql_itens = "update itenspedidosinterno set   CONFIRMADO =('{$quantidade}' + CONFIRMADO)  where ID ={$id_item}";

                $resultado_itens = mysql_query($sql_itens);

                if(!$resultado_itens){
                    echo "13";
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));



                $sql = "insert into entrada_ur (DATA,USUARIO_ID,QTD,LOTE,ITENSPEDIDOSINTERNOS_ID,catalogo_id, saida_id)
                          VALUES
                          (now(),'{$usuario}','{$quantidade}','{$lote}','{$id_item}','{$catalogo_id}','{$saidaId}')";
                $result = mysql_query($sql);
                if(!$result){
                    echo "Erro 14".$sql;
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));


                $sql_itens = "update estoque set  {$qtd} =({$quantidade} + {$qtd})  where CATALOGO_ID= '{$catalogo_id}' and TIPO={$tipo}";
                $resultado_itens = mysql_query($sql_itens);

                if(!$resultado_itens){
                    echo 19;
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
                $confirmados = $quantidade + $saidaConfirmados;
                $sqlSaida = "UPDATE
                            saida
                            SET
                            saida.confirmacao_pedidointerno = {$confirmados}
                            WHERE
                            saida.idSaida = {$saidaId}";
                        $resultadoSaida = mysql_query($sqlSaida);

                if(!$resultadoSaida){
                    echo "Erro ao fazer Update em saida";
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sqlSaida)));

            }
        }
    }
    savelog(mysql_escape_string(addslashes($sql)));


    mysql_query("commit");
    echo "1";
}
if(isset($_POST['query'])){
    switch($_POST['query']){
        case "salvar-envio-pedido":
            salvarEnvioPedido($_POST['dados'],$_POST['id'], $_POST['data'],$_POST['empresa']);
            break;
        case "salvar-entrada-estoque":
            salvarEntradaPedido($_POST['dados'],$_POST['id'], $_POST['data'],$_POST['empresa']);
            break;


//        case "cancelar-item-solicitacao":
//            cancelar_item_solicitacao($_POST);
//            break;
//        case "busca-itens-troca":
//            busca_itens($_POST['like'],$_POST['tipo']);
//            break;
//        case "trocar-item-solicitacao":
//            trocar_item_solicitacao($_POST);
//            break;
//        case "trocar-item-pedido-interno":
//            trocar_item_pedido_interno($_POST);
//            break;
//        case "estoque":
//            pes_estoque($_POST['ur'],$_POST['cod'],$_POST['todos'],$_POST['tipo'],$_POST['maiorQueZero']);
//            break;
//        case "brasindice":
//            brasindice($_POST['ur'],$_POST['item'], $_POST['tipo'], $_POST['status']);
//            break;
//        case "novo_brasindice":
//            novo_brasindice($_POST['principio'],$_POST['apresentacao'], $_POST['laboratorio'], $_POST['numero_tiss'], $_POST['nome_simpro_brasindice'],$_POST['tipo_brasindice'], $_POST['tag_brasindice'],$_POST['status'], $_POST['usuario'], $_POST['data'],
//                $_POST['controlado'],$_POST['antibiotico'],$_POST['referencia'], $_POST['id_simpro_brasindice'], $_POST['numero_tuss'],$_POST['id_principio'],$_POST['generico']);
//            break;
//        case "editar_brasindice":
//            editar_brasindice($_POST['principio'],$_POST['apresentacao'], $_POST['laboratorio'], $_POST['numero_tiss'],
//                $_POST['tipo_brasindice'], $_POST['tag_brasindice'],$_POST['status'], $_POST['usuario'], $_POST['data'],
//                $_POST['controlado'],$_POST['antibiotico'],$_POST['catalogo_id'],$_POST['referencia'], $_POST['nome_simpro'],
//                $_POST['numero_tuss'],$_POST['id_principio'],$_POST['generico'], $_POST['principio_antigo'],
//                $_POST['descontinuado']);
//            break;
//        case "edt_estoque":
//            edt_estoque($_POST['dados']);
//            break;
//        case "salvar-pedido":
//            salvar_pedido($_POST['dados'],$_POST['observacao'], $_POST['data_max']);
//            break;
//        case "salvar-envio-pedido":
//            salvar_envio_pedido($_POST['dados'],$_POST['id'], $_POST['data'],$_POST['empresa']);
//            break;
//        case "salvar-pedido-estoque":
//            salvar_pedido_estoque($_POST['dados'], $_POST['data_envio'], $_POST['observacao']);
//            break;
//        case "salvar-entrada-estoque":
//            salvar_entrada_pedido($_POST['dados'],$_POST['id'], $_POST['data'],$_POST['empresa']);
//            break;
//        case "cancelar-item-pedido":
//            cancelar_item_pedido($_POST);
//            break;
//        case "cancelar-item-entrada-estoque":
//            cancelar_item_entrada_estoque($_POST);
//            break;
//        case "pesquisa-entrada-saida":
//            pesquisa_entrada_saida($_POST);
//            break;
//        case "pesquisa-relatorio-lote":
//            pesquisa_relatorio_lote($_POST);
//            break;
//        case "pesquisa-equipamento-ativo":
//            pesquisa_equipamento_ativo($_POST);
//            break;
//        case "excluir-solicitacao-total":
//            excluir_solicitacao_total($_POST);
//            break;
//        case "excluir-pedido-total":
//            excluir_pedido_total($_POST);
//            break;
//        case "pesquisar-gerenciar-pedidos":
//            pesquisar_gerenciar_pedidos($_POST);
//            break;
//        case "pesquisar-equipamentos-ativos-recolher":
//            pesquisarEquipamentosAtivosRecolher($_POST);
//            break;
//        case "desativar-equipamentos-ativos-recolher":
//            desativarEquipamentosAtivosRecolher($_POST);
//            break;
//        case "devolver-equipamento-ativo":
//            devolverEquipamentoAtivo($_POST);
//            break;
//        case "finalizar-equipamento-ativo":
//            finalizarEquipamentoAtivo($_POST);
//            break;
    }
}