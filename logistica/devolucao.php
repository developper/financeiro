<?php
$id = session_id();
if (empty($id)) {
    session_start();
}

include_once '../db/config.php';
include_once '../utils/codigos.php';

class Devolucao
{

    private function pacientes($id)
    {

        $codEmpresa = ' IN ' . $_SESSION['empresa_user'];

        $result = mysql_query("SELECT * FROM clientes where empresa {$codEmpresa} ORDER BY nome");

        echo "<p><b>Paciente:</b><br/>";
        echo "<select name='paciente' class='pacientes' data-placeholder='Selecione um paciente...' style='background-color:transparent;' >";
        while ($row = mysql_fetch_array($result)) {
            if (($id != null) && ($id == $row['idClientes'])) {
                echo "<option value='" . $row['idClientes'] . "' selected>({$row['codigo']}) " . strtoupper($row['nome']) . "</option>";
            } else {
                echo "<option value='" . $row['idClientes'] . "'>({$row['codigo']}) " . strtoupper($row['nome']) . "</option>";
            }

        }
        echo "</select>";
    }

    public function form()
    {
        echo "<h1><center>Devolu&ccedil;&atilde;o</center></h1>";
        echo "<div id='div-devolucao' tipo=''>" . alerta();
        echo " </br><div style='border:2px dashed;padding: 6px'></div> </br>";

        echo $this->pacientes(null);

        echo "<p><b>Data:</b><input type='text' class='data-devolucao OBG' readonly name='data' size='10' />";
        echo "<b>Item: </b><input type='text' name='busca-item' class='OBG' id='busca-item' size='80' /><button id='limpar-busca'>limpar</button>";
        echo "<input type='hidden' id='id-novo-item' value='-1' />";
        echo "<input type='hidden' id='id-tipo' value='-1' />";
        echo "<input type='hidden' id='catalogo_id' value='-1' />";
        echo "<br/><b>Quantidade:</b><input id='qtd-dev' type='text' class='OBG numerico' name='qtd' size='10' style='text-align:right' />";
        echo "<br/><b>Lote/N&deg; de S&eacute;rie:</b><input id='lote-dev' type='text' class='OBG' name='lote-dev' size='10' style='text-align:right' />";
        echo "<button id='add-devolucao' >+</button>";
        echo "</div><br/>";
//       echo "<h2><center>Devolu&ccedil;&otilde;es conclu&iacute;das</center><h2>";
        echo "<table id='tab-devolucao' class='mytable' width=100% >";
        echo "<thead><tr>";
        echo "<th><input type='checkbox' name='select' id='checktodos' /></th>";
        echo "<th><b>Paciente</b></th>";
        echo "<th><b>Item</b></th>";
        echo "<th width=10% ><b>Quantidade</b></th>";
        echo "<th width=15% ><b>Lote/N&deg; de S&eacute;rie</b></th>";
        echo "</tr></thead>";
        echo "</table>";
        echo "<button id='del-devolucao' >-</button>";
        echo "<br/><br/><button id='finalizar-devolucao' >Finalizar</button>";
    }

}
