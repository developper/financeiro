<?php
$id = session_id();
if(empty($id))
    session_start();
include_once('../validar.php');
require_once('../db/config.php');
require_once('../utils/codigos.php');
$extension = 'xls';
if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
    $extension = 'xlsx';
}

header("Content-type: application/x-msexce");
header("Content-type: application/force-download;");
header("Content-Disposition: attachment; filename=estoque.{$extension}");
header("Pragma: no-cache");

function pes_estoque($ur,$cod,$todos,$tipo,$maiorQueZero)
{

    if (isset($ur) && !empty($ur)) {
        $condur = 'e.empresas_id_' . $ur;
        $qtd = 'empresas_id_' . $ur;
        $qtd_min = 'QTD_MIN_' . $ur;

    } else {
        echo 'Erro falta a Unidade Regional. Entre em contato com o TI.';
        return;
    }

    $condMaiorQueZero = $maiorQueZero == 'N' ? '' : "and {$condur} > 0";




    if($todos == 0){
        $cond = " and concat(b.ID, ' - ', b.principio,' ',b.apresentacao)  like'%{$cod}%'";
    }else{
        $cond='';
    }
    if($tipo == 't'){
        $sql= "select
                       e.*,
                      {$condur},
                      concat(b.ID, ' - ', b.principio,' ',b.apresentacao) as name,
                      b.valor as custo
                      ,b.lab_desc as lab,
                       b.principio
                      from
                       estoque  as e inner join
                      catalogo as b on(e.CATALOGO_ID = b.ID)



        where e.TIPO in(0,1,3) and b.ATIVO = 'A' {$condMaiorQueZero}
		     union
		     select
                     e.*,
                     {$condur},
                     C.item as name,
                     C.CUSTO as custo,
                     '' as lab,
                     C.item as principio
                     from
                     estoque as e inner join
                     cobrancaplanos as C on(e.CATALOGO_ID = C.id)
                     where
                     e.TIPO = 5 {$condMaiorQueZero}
                     order by name ASC";

    }else{
        if($tipo == 'mat'){
            $t=1;
            $sql= "select
            e.*,{$condur},
                concat(b.ID, ' - ', b.principio,' ',b.apresentacao)as name ,
                      b.valor as custo
                      ,b.lab_desc as lab
                from
                estoque as e inner join
                catalogo as b on(e.CATALOGO_ID = b.ID)
                where  e.TIPO = {$t} {$cond} and b.ATIVO = 'A' {$condMaiorQueZero}
                order by b.principio ASC";


        }
        if($tipo == 'med'){
            if($todos == 0){
                $cond = " and concat(b.ID, ' - ', b.principio,' ',b.apresentacao,' ',principio_ativo.principio )  like'%{$cod}%'";
            }
            $t=0;
            $sql= "select
                    e.*,{$condur},
                        concat(b.ID, ' - ', b.principio,' ',b.apresentacao) as name,
                        principio_ativo.principio as principioAtivo,
                      b.valor as custo
                      ,b.lab_desc as lab
                        from
                        estoque as e inner join
                        catalogo as b on(e.CATALOGO_ID = b.ID)
                        LEFT JOIN catalogo_principio_ativo ON b.ID = catalogo_principio_ativo.catalogo_id
                        LEFT JOIN principio_ativo ON principio_ativo.id = catalogo_principio_ativo.principio_ativo_id
                        where  e.TIPO = {$t} {$cond} and b.ATIVO = 'A' {$condMaiorQueZero} order by principioAtivo ASC";

        }
        if($tipo == 'medcontrolado'){
            if($todos == 0){
                $cond = " and concat(b.ID, ' - ', b.principio,' ',b.apresentacao,' ',principio_ativo.principio )  like'%{$cod}%'";
            }
            $t=0;
            $sql= "select
                    e.*,{$condur},
                        concat(b.ID, ' - ', b.principio,' ',b.apresentacao) as name ,
                        principio_ativo.principio as principioAtivo,
                      b.valor as custo, b.lab_desc as lab
                        from
                        estoque as e inner join
                        catalogo as b on(e.CATALOGO_ID = b.ID)
                        LEFT JOIN catalogo_principio_ativo ON b.ID = catalogo_principio_ativo.catalogo_id
                        Left JOIN principio_ativo ON principio_ativo.id = catalogo_principio_ativo.principio_ativo_id
                        where
                        b.CONTROLADO = 'S' and
                        e.TIPO = {$t} {$cond} and b.ATIVO = 'A' {$condMaiorQueZero} order by principioAtivo ASC";

        }

        if($tipo == 'die'){
            $t=3;
            $sql= "select e.*,{$condur},
                   concat(b.ID, ' - ', b.principio,' ',b.apresentacao) as name ,
                      b.valor as custo ,b.lab_desc as lab
                    from
                    estoque as e inner join
                    catalogo as b on(e.CATALOGO_ID = b.ID)
                    where
                    e.TIPO = {$t} {$cond} and b.ATIVO = 'A' {$condMaiorQueZero} order by b.principio ASC";

        }
        if($tipo =='eqp'){
            $t=5;
            if($cond == ''){
                $cond= 'and C.tipo = 1';
            }

            $sql= "select
                    e.*,{$condur},
                    C.item as name,
                     C.CUSTO as custo
                    from
                    estoque as e inner join
                    cobrancaplanos as C on(e.CATALOGO_ID = C.id)
                    where
                    e.TIPO = {$t} {$cond} {$condMaiorQueZero}
                    order by name ASC";

        }

    }

    $result = mysql_query($sql);

    $resposta = mysql_num_rows($result);



    $t1 = '';
    if($resposta > 0){

        $t1 .= "

            <table  width=100% border='1px' bordercolor='#0000'>
            <thead>
		<tr>
		<th>Item</th>
		<th>Tipo</th>
		";
        if($tipo == 'medcontrolado' || $tipo == 'med'){
            $labelPrincipio = "Princípio Ativo";
        }
        $t1 .= "<th>Quantidade</th>
		        <th>Quantidade Minima</th>
		        ";
        if($ur == 1){
            $t1 .="<th width='5%'>Custo</th>";
        }


        $t1 .= "</tr>
		</thead>";
        while($row = mysql_fetch_array($result)){

            $tipoItem = '';
            switch($row['TIPO']){
                case 0:
                    $tipoItem = 'Medicamento' ;
                break;
                case 1:
                    $tipoItem = 'Material' ;
                break;
                case 3:
                    $tipoItem = 'Dieta' ;
                break;
                case 5:
                    $tipoItem = 'Equipamento' ;
                break;
            }


            $t1.="<tr>
                              <td >{$row['name']}   <b> (LAB: {$row['lab']})</b>";
            if($tipo == 'medcontrolado' || $tipo == 'med'){
                $t1.= " <b>Princípio Ativo:</b> ".$row['principioAtivo'];
            }
            $t1.=" </td>";

            $t1 .="<td>{$tipoItem}</td>";
            $t1 .="<td>{$row[$qtd]}</td>";


            if($ur == 1){
                $t1.="<td>
                        {$row[$qtd_min]}
                      </td>
                      ";
                $t1.= "<td><b>R$ </b>".number_format($row['custo'], 2, ',', '.')."</td>";
            }else{
                $t1.="<td>$row[$qtd_min]</td>";

            }
            $t1.= "</tr>";
        }

        $t1 .= "</table>";

    }else{
        $t1 .="<p id='res-b'>";
        $t1 .=  "<center><b>Nenhum resultado foi encontrado!</b></center></p>";

    }
    echo $t1;
}

if(isset($_GET)) {

    	pes_estoque($_GET['ur'],$_GET['cod'],$_GET['todos'],$_GET['tipo'],$_GET['maiorQueZero']);

}