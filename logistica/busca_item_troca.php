<?php
require_once('../db/config.php');

$like = $_GET['term'];
$somenteGenerico = $_GET['somenteGenerico'];
$compGenerico = $somenteGenerico == 'G' ? " AND ( catalogo.GENERICO = 'S' OR catalogo.possui_generico = 'N' ) " : "";

$itens = array();
if(isset($_GET['tipo']) && ($_GET['tipo'] == 'med' || $_GET['tipo'] == 0) && isset($_GET['catalogo_id'])) {
	$catalogoId = $_GET['catalogo_id'];
	$sqlPrincipio = "SELECT
principio_ativo.principio
FROM
catalogo_principio_ativo
inner join principio_ativo on catalogo_principio_ativo.principio_ativo_id = principio_ativo.id

WHERE
catalogo_principio_ativo.catalogo_id = $catalogoId";
	$resultPrincipio = mysql_query($sqlPrincipio);
	if(mysql_num_rows($resultPrincipio) == 0){
		$msg = 'O item ainda não têm principio ativo associado a ele, para fazer a troca é necessário associar um principio.';
		$itens['msg'] = $msg;
		echo json_encode($itens);
		return;
	}
	$principioAtivo = mysql_result($resultPrincipio,0);

	$sql = "SELECT
			concat('(',principio_ativo.principio,') ',catalogo.principio,' ',catalogo.apresentacao,' LAB: ',catalogo.lab_desc) as name,
			apresentacao,numero_tiss as cod,
			catalogo.ID as catalogo_id,
			tipo as tipo,
			catalogo.GENERICO

FROM
catalogo
INNER JOIN catalogo_principio_ativo ON catalogo.ID = catalogo_principio_ativo.catalogo_id
LEFT JOIN principio_ativo ON principio_ativo.id = catalogo_principio_ativo.principio_ativo_id
WHERE
catalogo.ATIVO = 'A' AND
catalogo.tipo = 0 AND
concat('(',principio_ativo.principio,') ',catalogo.principio,' ',catalogo.apresentacao) like '%{$principioAtivo}%'
{$compGenerico}
ORDER BY catalogo.principio";


}else{
	$sql = "SELECT
			concat('(',principio_ativo.principio,') ',catalogo.principio,' ',catalogo.apresentacao,' LAB: ',catalogo.lab_desc) as name,
			apresentacao,numero_tiss as cod,
			catalogo.ID as catalogo_id,
			tipo as tipo,
			catalogo.GENERICO,
			catalogo.lab_desc
FROM
catalogo

WHERE
catalogo.ATIVO = 'A' AND
concat('(catalogo.principio,' ',catalogo.apresentacao) like '%{$principioAtivo}%'
{$compGenerico}
ORDER BY catalogo.principio";

}

$result = mysql_query($sql);
while ($row = mysql_fetch_array($result)) {
	$textRed = 'N';
	$item['value'] = $row['name']; 
	$item['id'] = $row['cod']; 
    $item['tipo'] = $row['tipo'];
	$item['catalogo_id'] = $row['catalogo_id'];
	if ($somenteGenerico == 'G' && $row['GENERICO'] != 'S')
		$textRed = 'S';
	$item['textRed'] = $textRed;
	
	array_push($itens, $item); 
}
//$itens['msg'] = null;

echo json_encode($itens);
?>