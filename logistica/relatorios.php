<?php

include_once('../utils/codigos.php');

use App\Helpers\StringHelper;

class Relatorio
{

    private function pacientes($id){
        $result = mysql_query("SELECT * FROM clientes ORDER BY nome;");
        echo "<p><b>Paciente:</b><br/>";
        echo "<select name='paciente' style='background-color:transparent;' >";
        while($row = mysql_fetch_array($result))
        {
            if(($id <> NULL) && ($id == $row['idClientes']))
                echo "<option value='". $row['idClientes'] ."' selected>({$row['codigo']}) ". strtoupper(StringHelper::removeAcentosViaEReg($row['nome'])) ."</option>";
            else
                echo "<option value='". $row['idClientes'] ."'>({$row['codigo']}) ". strtoupper(StringHelper::removeAcentosViaEReg($row['nome'])) ."</option>";
        }
        echo "</select>";
    }

    public function form(){
        echo "<h1><center>Relat&oacute;rios</center></h1>";
        echo "<div id='div-relatorios'>".alerta();
        echo $this->pacientes(NULL);
        echo "<p><b>Data:</b><input type='text' class='OBG data' name='data' size='10' />";
        echo "<button id='relatorios' >Gerar</button>";
        echo "</div>";
    }
    public function menu_relatorios(){
        if(strpos($_SESSION['empresa_user'],'1') !== false || $_SESSION['empresa_principal'] === '1'){
            echo "<p><a href=?op=relatorio-entrada-saida>Relat&oacute;rio Entrada-Saida UR</a>";
            echo "<p><a href=/logistica/relatorios/?action=relatorio-saida-condensada>" . htmlentities("Relatório de Saídas Condensadas") . "</a>";
            echo "<p><a href=/logistica/relatorios/?action=relatorio-aluguel-equipamentos>" . htmlentities("Relatório de Aluguel de Equipamentos") . "</a>";
        }
        echo "<p><a href=?op=relatorio-equipamento-ativo>Relat&oacute;rio Equipamento/Ativo</a>";
        echo "<p><a href=?op=relatorio-devolucao>" . htmlentities("Relatório de Devolução") . "</a>";
        echo "<p><a href=/logistica/relatorios/?action=form-relatorio-itens-vencidos>" . htmlentities("Relatório de Itens Vencidos") . "</a>";
        echo "<p><a href=/logistica/relatorios/?action=relatorio-notas-entrada>" . htmlentities("Relatório de Entrada de Notas") . "</a>";
        echo "<p><a href=/logistica/relatorios/?action=relatorio-saida-interna>" . htmlentities("Relatório de Saídas Internas") . "</a>";
        echo "<p><a href=../relatorios/?saida-antibiotico-paciente=form>" . htmlentities("Relatório de Saídas de Antibióticos para Pacientes.") . "</a>";
        echo "<p><a href=../logistica/relatorios/?action=relatorioAjusteEstoque>" . htmlentities("Relatório Ajuste de Estoque.") . "</a>";
        echo "<p><a href=/logistica/relatorios/?action=relatorio-saida-remessa>" . htmlentities("Relatório de Saídas por Remessas") . "</a>";

    }

}