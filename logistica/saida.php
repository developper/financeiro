<?php

require_once('../db/config.php');
require_once('../utils/codigos.php');

// use \App\Services\CacheAdapter;
// use \App\Services\Cache\SimpleCache;
use App\Helpers\StringHelper as String;

class Saida{

	private function formularioAdicionarMedicamentoSaida($tipoMedicamentoPlano){

		echo "<div id='formulario-adicionar-medicamento-saida' title='Adicionar Medicamento'
				somente-generico='{$tipoMedicamentoPlano['tipoMedicamento']}'  tipo='' catalogo_id = ''  soldata=''
				env='' total='' autorizado='' qtd=''  id_pedido=''
					id_item_pedido=''>";
		echo alerta();
		if($tipoMedicamentoPlano['tipoMedicamento'] == 'G'){
			echo info("Paciente do plano {$tipoMedicamentoPlano['nomePlano']} é recomendado prescrever itens Genéricos.");
		}
		echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar a troca de item, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
		echo "<p><b>Item:</b><br/>
				<select name='select-adicionar-medicamento' id='select-adicionar-medicamento'
				 data-placeholder='Selecione um item'  class='OBG_CHOSEN chosen-select'
				 size='80%'>
				</select>";
		echo "<div id='opcoes-troca'></div>";
		echo "<input type='hidden' name='cod' id='cod' value='-1' />";
		echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
		echo "<b>Lote:</b><input type='text' name='lote' class='lote OBG' size='10' /></div>";
	}

	private function formularioAdicionarDietaMaterialSaida(){

		echo "<div id='formulario-adicionar-mat-die-saida' title='Adicionar Item'  sol='' tipo=''  catalogo_id = ''  soldata=''
				env='' total='' autorizado='' qtd=''  >";
		echo alerta();

		//echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar a troca de item, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
		echo "<div><b>Item:</b><br/>
				<input name = 'busca-adicionar-mat-die-saida' id='busca-adicionar-mat-die-saida' size='80%'>
				</div>";
		echo "<div id='opcoes-troca'></div>";
		echo "<input type='hidden' name='cod-mat-die' id='cod-mat-die' value='-1' />";
		echo "<input type='hidden' name='tiss-mat-die' id='tiss-mat-die' value='-1' />";
		echo "<b>Quantidade:</b> <input type='text' id='qtd-mat-die' name='qtd-mat-die' class='numerico OBG' size='3'/>";
		echo "<b>Lote:</b><input type='text' name='lote' class='OBG' size='10' /></div>";
	}
	private function formularioTrocaMedicamento($tipoMedicamentoPlano){

		echo "<div id='formulario-troca-med' title='Formul&aacute;rio de troca' somente-generico='{$tipoMedicamentoPlano['tipoMedicamento']}' sol='' tipo='' catalogo_id = '' >";
		echo alerta();
		if($tipoMedicamentoPlano['tipoMedicamento'] == 'G'){
			echo info("Paciente do plano {$tipoMedicamentoPlano['nomePlano']} é recomendado prescrever itens Genéricos.");
		}
		echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar a troca de item, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
		echo "<p><b>Item:</b><br/><select name='select-troca-item' id='select-troca-item' data-placeholder='Selecione um Paciente'  class='chosen-select'>
				</select>";
		echo "<div id='opcoes-troca'></div>";
		echo "<input type='hidden' name='cod' id='cod' value='-1' />";
		echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
		echo "<b>Justificativa:</b><input type='text' name='justificativa' class='OBG' size='100' /></div>";
	}

    private function formularioTrocaMedicamentoPedidoInterno(){

        echo "<div id='formulario-troca-med-pedido' title='Formul&aacute;rio de troca' ped='' tipo='' catalogo_id = '' >";
        echo alerta();
        echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar a troca de item, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
        echo "<p><b>Item:</b><br/><select name='select-troca-item' id='select-troca-item' data-placeholder='Selecione um Paciente'  class='chosen-select'>
				</select>";
        echo "<div id='opcoes-troca'></div>";
        echo "<input type='hidden' name='cod' id='cod' value='-1' />";
        echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
        echo "<b>Justificativa:</b><input type='text' name='justificativa' class='OBG' size='100' /></div>";
    }
	private function formularioSaidaMedicamentoPedidoInterno(){

		echo "<div id='formulario-adicionar-medicamento-saida-pedido' title='Adicionar Medicamento' tipo='' catalogo_id = ''  soldata=''
				env='' total='' autorizado='' qtd=''  id_pedido=''
					id_item_pedido=''>";
		echo alerta();
		// echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar a troca de item, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
		echo "<p><b>Item:</b><br/><select name='select-saida-item' id='select-saida-item'
 				data-placeholder='Selecione um Paciente'  class='chosen-select'>
				</select>
				</p>";
		echo "<div id='opcoes-saida'></div>";
		echo "<input type='hidden' name='cod' id='cod' value='-1' />";
		echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
		echo "<b>Lote:</b><input type='text' name='lote' class='OBG' size='10' /></div>";
	}

//	private function formularioTrocaDietaMaterial(){
//
//		echo "<div id='formulario-troca-mat-die' title='Formul&aacute;rio de troca'  sol='' tipo='' catalogo_id = '' >";
//		echo alerta();
//
//		echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar a troca de item, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
//		echo "<p><b>Item:</b><br/>
//				<input name = 'busca-troca-mat-die' id='busca-troca-mat-die'>";
//		echo "<div id='opcoes-troca'></div>";
//		echo "<input type='hidden' name='cod-mat-die' id='cod-mat-die' value='-1' />";
//		echo "<input type='hidden' name='tiss-mat-die' id='tiss-mat-die' value='-1' />";
//		echo "<b>Quantidade:</b> <input type='text' id='qtd-mat-die' name='qtd-mat-die' class='numerico OBG' size='3'/>";
//		echo "<b>Justificativa:</b><input type='text' name='justificativa-mat-die' class='OBG' size='100' /></div>";
//	}
	private function formularioSaidaDietaMaterialPedidoInterno(){

		echo "<div id='formulario-saida-mat-die-pedido' title='Adicionar Item'   tipo='' catalogo_id = ''  soldata=''
				env='' total='' autorizado='' qtd=''  id_pedido='' nome='' tiss=''
					id_item_pedido='' >";
		echo alerta();

		echo "<p><b>Item:</b><br/>
				<input name = 'busca-saida-mat-die-pedido' size = '40' id='busca-saida-mat-die-pedido'>";
		echo "<div id='opcoes-troca'></div>";
		echo "<input type='hidden' name='cod-mat-die' id='cod-mat-die' value='-1' />";
		echo "<input type='hidden' name='tiss-mat-die' id='tiss-mat-die' value='-1' />";
		echo "<b>Quantidade:</b> <input type='text' id='qtd-mat-die' name='qtd-mat-die' class='numerico OBG' size='3'/>";
		echo "<b>Lote:</b><input type='text' name='lote-mat-die' class='OBG' size='5' /></div>";
	}

    private function formularioTrocaDietaMaterialPedidoInterno(){

        echo "<div id='formulario-troca-mat-die-pedido' title='Formul&aacute;rio de troca'  ped='' tipo='' catalogo_id = '' >";
        echo alerta();

        echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar a troca de item, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
        echo "<p><b>Item:</b><br/>
				<input name = 'busca-troca-mat-die-pedido' id='busca-troca-mat-die-pedido'>";
        echo "<div id='opcoes-troca'></div>";
        echo "<input type='hidden' name='cod-mat-die' id='cod-mat-die' value='-1' />";
        echo "<input type='hidden' name='tiss-mat-die' id='tiss-mat-die' value='-1' />";
        echo "<b>Quantidade:</b> <input type='text' id='qtd-mat-die' name='qtd-mat-die' class='numerico OBG' size='3'/>";
        echo "<b>Justificativa:</b><input type='text' name='justificativa-mat-die' class='OBG' size='100' /></div>";
    }

	public function pendentes_separados($ur,$url=''){
		$vazio = true;
		$this->imprimir_check();
       /* $cache = new CacheAdapter( new SimpleCache(__DIR__.'/cache'));
		$cached = $cache->adapter->read('solicitacoes-pendentes-logistica');
		echo "<div><center><h1>Solicita&ccedil;&otilde;es Pendentes</h1></center></div>";

		if(!$cached){

		}*/
		$consulta = mysql_fetch_array(mysql_query("SELECT
  			COUNT(*) as sol
  			FROM
  			solicitacoes as s  	
  			WHERE
  			s.status = '1' OR s.status = '2' AND qtd <> 0 "));



		//Ricardo alterando 07-08-2013
		if(!isset($_SESSION['id_max_sol']))
		{
			$_SESSION['id_max_sol']=$consulta['sol'];

		}
		else if($_SESSION['id_max_sol']!=$consulta['sol'])
		{
			if($_SESSION['status']==1)
			{
				echo "<audio src='alerta.mp3' autoplay=true></audio>";
				$_SESSION['id_max_sol'] = $consulta['sol'];
			}
			else
			{
				echo "<audio src='sirene.mid' autoplay=true></audio>";
				$_SESSION['id_max_sol'] = $consulta['sol'];
			}

		}

		/* DIV COM OS �LTIMOS PEDIDOS */
		echo "<div style='display:table;width:100%;border:1px dotted #ccc;background-color:#4B5054;color:#fff;'>";
		$z=0;
		$arquivo= 'UR_'.$ur;
		$nomess = explode(" | ",file_get_contents("../logistica/chamada_logistica/{$arquivo}.txt"));

		foreach($nomess as $key=>$nome_paciente){
			$z++;
			if($z<=6){
				echo "<span style='padding:10px;float:left;'><b>| ".strtoupper(String::removeAcentosViaEReg($nome_paciente))."</b></span>";
			}else{
				array_shift($_SESSION["NON"]);
				array_push($_SESSION["NON"], $nome_paciente);

				echo "<span style='padding:10px;float:left;'><b>| ".strtoupper(String::removeAcentosViaEReg($nome_paciente))."</b></span>";
			}
		}
		echo "</div>";

//        $compEmpresas = "";
//		if($_SESSION['empresa_principal'] == '1' || $_SESSION['empresa_principal'] == '11') {
//		    $compEmpresas = " OR c.empresa IN " . $_SESSION['empresa_user'];
//        }

		$sqlTodosPendentes = <<<SQL
SELECT
c.nome,
c.empresa,
e.nome as nome_empresa,
u.nome AS usuario,
s.enviado,
s.autorizado,
DATE_FORMAT(s.data_auditado, '%d/%m/%Y %H:%i') as data_hora,
s.STATUS as status,
s.data_auditado,
s.qtd,
s.paciente,
s.TIPO_SOLICITACAO,
s.visualizado_logistica,
s.primeira_prescricao,
DATE_FORMAT(s.DATA_SOLICITACAO, '%d/%m/%Y %H:%i') as data_hora_solicitacao,
s.DATA_SOLICITACAO,
s.idPrescricao,
s.id_prescricao_curativo,
concat(ci.NOME, '/', ci.UF) as cidade
FROM
solicitacoes AS s INNER JOIN
clientes AS c ON (s.paciente = c.idClientes) INNER JOIN
usuarios AS u ON (u.idUsuarios = s.enfermeiro) INNER JOIN
empresas AS e ON (e.id = c.empresa) LEFT JOIN
cidades AS ci ON (c.CIDADE_ID_INT = ci.ID)
WHERE
( s.tipo = 0 OR s.tipo = 1 OR s.tipo = 5 OR s.tipo = 3 ) AND IF
		( s.`status` = 2, s.qtd > s.enviado, s.autorizado > s.enviado ) AND
( s.`status` = '1' OR s.`status` = '2' ) AND
s.qtd <> 0 AND
(c.empresa = {$_GET['ur']}
{$compEmpresas})
ORDER BY
	s.primeira_prescricao DESC, c.empresa ASC
SQL;
		//echo '<pre>'; die($sqlTodosPendentes);

		$rTodosPendentes = mysql_query($sqlTodosPendentes) or die($sqlTodosPendentes . ' - ' . mysql_error());
		$pedidosPeriodicos = [];
		$outrosPedidos= [];
		$todosPedidos = [];
		$paciente=0;
		$caraterPeriodico = [2, 5, 9];
		$condensadoPeriodico = 0;
		$condensadoOutros = 0;
		$condensadoTodos = 0;
		while($row2 = mysql_fetch_array($rTodosPendentes)) {
			if($paciente != $row2['paciente']){
				$dataTodos = $row2['status'] == 2 ? $row2['DATA_SOLICITACAO'] : $row2['data_hora'];
				$dataPeriodica = 0;
				$dataOutros = 0;

				if(in_array($row2['TIPO_SOLICITACAO'], $caraterPeriodico)){
					$dataPeriodica = $row2['data_auditado'];
					$pedidosPeriodicos[$row2['paciente']]['data_auditado'] = $row2['data_hora'];
					$pedidosPeriodicos[$row2['paciente']]['solicitante'] = $row2['usuario'];
					$pedidosPeriodicos[$row2['paciente']]['prescricao'] = $row2['id_prescricao_curativo'] != '0' ? $row2['id_prescricao_curativo'] : $row2['idPrescricao'];
				}else{
					$dataOutros = $row2['status'] == 2 ? $row2['DATA_SOLICITACAO'] : $row2['data_auditado'];
					$outrosPedidos[$row2['paciente']]['data_auditado'] = $row2['status'] == 2 ?
						$row2['data_hora_solicitacao'] :
						$row2['data_hora']
					;
					$outrosPedidos[$row2['paciente']]['solicitante'] = $row2['usuario'];
					$outrosPedidos[$row2['paciente']]['prescricao'] = $row2['id_prescricao_curativo'] != '0' ? $row2['id_prescricao_curativo'] : $row2['idPrescricao'];
				}
				$todosPedidos[$row2['paciente']]['data_auditado'] = $row2['data_hora'];
				$todosPedidos[$row2['paciente']]['solicitante'] = $row2['usuario'];
				$todosPedidos[$row2['paciente']]['prescricao'] = $row2['id_prescricao_curativo'] != '0' ? $row2['id_prescricao_curativo'] : $row2['idPrescricao'];

			}else{
				if(in_array($row2['TIPO_SOLICITACAO'], $caraterPeriodico)){
					if($row2['data_auditado'] >= $dataPeriodica) {
						$dataPeriodica = $row2['data_auditado'];
						$pedidosPeriodicos[$row2['paciente']]['data_auditado'] = $row2['data_hora'];
						$pedidosPeriodicos[$row2['paciente']]['solicitante'] = $row2['usuario'];
					}
				}else{
					if($row2['data_auditado'] > $dataOutros || $row2['DATA_SOLICITACAO'] > $dataOutros) {
						$dataOutros = $row2['status'] == 2 ? $row2['DATA_SOLICITACAO'] : $row2['data_auditado'];
						$outrosPedidos[$row2['paciente']]['data_auditado'] = $row2['status'] == 2 ?
							$row2['data_hora_solicitacao'] :
							$row2['data_hora']
						;
						$outrosPedidos[$row2['paciente']]['solicitante'] = $row2['usuario'];
					}
				}
				if($row2['data_auditado'] > $dataTodos || $row2['DATA_SOLICITACAO'] > $dataTodos) {
					$dataTodos = $row2['status'] == 2 ? $row2['DATA_SOLICITACAO'] : $row2['data_auditado'];
					$todosPedidos[$row2['paciente']]['data_auditado'] = $row2['status'] == 2 ?
						$row2['data_hora_solicitacao'] :
						$row2['data_hora']
					;
					$todosPedidos[$row2['paciente']]['solicitante'] = $row2['usuario'];
				}
			}

			if(in_array($row2['TIPO_SOLICITACAO'], $caraterPeriodico)){
				$pedidosPeriodicos[$row2['paciente']]['nome'] = $row2['nome'];
				$pedidosPeriodicos[$row2['paciente']]['cidade'] = $row2['cidade'];
				$pedidosPeriodicos[$row2['paciente']]['nome_empresa'] = $row2['nome_empresa'];
				$pedidosPeriodicos[$row2['paciente']]['primeira_prescricao'] = $row2['primeira_prescricao'];
				$pedidosPeriodicos[$row2['paciente']]['totalPendentes'] += $row2['autorizado'] - $row2['enviado'];
				if($row2['visualizado_logistica'] == 0) {
					$pedidosPeriodicos[$row2['paciente']]['visualizado_logistica'] += $row2['autorizado'];
				}
			}else{
				$outrosPedidos[$row2['paciente']]['nome'] = $row2['nome'];
				$outrosPedidos[$row2['paciente']]['cidade'] = $row2['cidade'];
				$outrosPedidos[$row2['paciente']]['nome_empresa'] = $row2['nome_empresa'];
				if($row2['status'] == 2){
					$outrosPedidos[$row2['paciente']]['totalPendentes'] += $row2['qtd'] - $row2['enviado'] ;
					if($row2['visualizado_logistica'] == 0) {
						$outrosPedidos[$row2['paciente']]['visualizado_logistica'] += $row2['qtd'];
					}
				}else{
					$outrosPedidos[$row2['paciente']]['totalPendentes'] += $row2['autorizado'] - $row2['enviado'] ;
					if($row2['visualizado_logistica'] == 0) {
						$outrosPedidos[$row2['paciente']]['visualizado_logistica'] += $row2['autorizado'];
					}
				}
				if($outrosPedidos[$row2['paciente']]['status'] != 2)
					$outrosPedidos[$row2['paciente']]['status'] = $row2['status'];
			}

			$todosPedidos[$row2['paciente']]['nome'] = $row2['nome'];
			$todosPedidos[$row2['paciente']]['cidade'] = $row2['cidade'];
			$todosPedidos[$row2['paciente']]['nome_empresa'] = $row2['nome_empresa'];
			$todosPedidos[$row2['paciente']]['primeira_prescricao'] = $row2['primeira_prescricao'];
			if($todosPedidos[$row2['paciente']]['status'] != 2)
				$todosPedidos[$row2['paciente']]['status'] = $row2['status'];

			if($row2['status'] == 2){
				$todosPedidos[$row2['paciente']]['totalPendentes'] += $row2['qtd'] - $row2['enviado'];
				if($row2['visualizado_logistica'] == 0) {
					$todosPedidos[$row2['paciente']]['visualizado_logistica'] += $row2['qtd'] - $row2['visualizado_logistica'];
				}
			}else{
				$todosPedidos[$row2['paciente']]['totalPendentes'] += $row2['autorizado'] - $row2['enviado'];
				if($row2['visualizado_logistica'] == 0) {
					$todosPedidos[$row2['paciente']]['visualizado_logistica'] += $row2['autorizado'];
				}
			}

			$paciente = $row2['paciente'];
		}

		echo <<<HTML
<div id="solicitacoes-pendentes-tabs">
	<ul>
		<li><a href='#tabs-emergenciais'>Emergenciais</a></li>
		<li><a href='#tabs-periodicas'>Prescrições Periódicas</a></li>
		<li><a href='#tabs-todos'>Todos</a></li>
	</ul>
	<div style="display: table;" id="tabs-emergenciais">
	<div class="ui-state-highlight" style="margin-top: 5px;">
		<center>
			<strong> Esta seção contempla os seguintes tipos de solicitações: Aditivas, Avulsas e Entrega Imediata.</strong>
		</center>
	</div>
	<br>
HTML;

		foreach($outrosPedidos as $paciente => $row){
			$condensadoOutros += $row['totalPendentes'];
			$nome_ur = $row['nome_empresa'];
			$vazio = false;
			$presc = $row['prescricao'];
			$class_button_paciente = $row['primeira_prescricao'] == 0 ? "bpaciente" : "bpaciente_prioridade";

			$hora = "";
			if($row['status'] == 2){
				$hora = "<b>EMERG&Ecirc;NCIA</b> <img src='../utils/sirene_ativa.gif' style='width:10px;' />";
			}

			echo "<div style='position: relative;border:1px solid #fff;width:240px;display:table;float:left;margin:0 auto;padding:15px; font-size:12px;'>";
			if ($row['visualizado_logistica'] > 0) {
				$qtd_nao_visualizado = str_pad ($row['visualizado_logistica'], 2, '0', STR_PAD_LEFT);
				echo "<div class='ribbon-wrapper-green'><div class='ribbon-green'> {$qtd_nao_visualizado} Novo(s)</div></div>";
			}
			echo "<table width='100%'>";
			echo "<tr>";
			echo "<td>";
			echo "<center><button class='{$class_button_paciente}' p='$paciente' carater='o' presc='{$presc}' tipo='1' url='{$url}'></button></center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center><b>".strtoUpper($row['nome'])."</b></center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<div class='ui-state-highlight' style='margin-top: 5px;'>";
			echo "<center><b>".strtoUpper($row['cidade'])."</b></center>";
			echo "</div>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center style='font-size:9px;'>{$row['solicitante']}</center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center>{$row['data_auditado']}</center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center style='font-size:9px;'>{$hora}</center>";
			echo "</td>";
			echo "</tr>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo utf8_encode("<center style='color:red;'>itens ({$row['totalPendentes']})</center>");
			echo "</td>";
			echo "</tr>";
			echo "</table>";
			echo "</div>";
		}
		//CONDENSADO OUTROS
		echo "<div style='border:1px solid #fff;width:240px;display:table;float:left;margin:0 auto;padding:15px; font-size:12px;'>";
		echo "<table width='100%'>";
		echo "<tr>";
		echo "<td>";
		echo "<center><button class='bpacotePaciente' p='{$ur}' carater='o' tipo='2' url='{$url}'></button></center>";
		echo "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td>";
		echo "<center><b>".strtoUpper($nome_ur)."</b></center>";
		echo "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td>";
		echo "<center style='font-size:9px;color:#006400'><b>(CONDENSADO)</b></center>";
		echo "</td>";
		echo "</tr>";
		echo "</tr>";
		echo "<tr>";
		echo "<td>";
		echo utf8_encode("<center style='color:red;'>itens ({$condensadoOutros})</center>");
		echo "</td>";
		echo "</tr>";
		echo "</table>";
		echo "</div>";

		// Fecha aba Emergenciais
		echo "</div>";

		echo '<div style="display: table;" id="tabs-periodicas">';
		echo '<div class="ui-state-highlight" style="margin-top: 5px;">
						<center>
							<strong>
								Esta seção contempla as solicitações de Prescrições Períodicas e Períodicas de Avaliação!
								<br>
								Quando o paciente estiver de camisa verde, significa que é uma Periódica de Avaliação.
							</strong>
						</center>
					</div>
					<br>';
		foreach($pedidosPeriodicos as $paciente => $row){
			$condensadoPeriodico += $row['totalPendentes'];
			$nome_ur = $row['nome_empresa'];
			$vazio = false;
            $presc = $row['prescricao'];
			$class_button_paciente = $row['primeira_prescricao'] == 0 ? "bpaciente" : "bpaciente_prioridade";

			echo "<div style='position: relative;border:1px solid #fff;width:240px;display:table;float:left;margin:0 auto;padding:15px; font-size:12px;'>";
			if ($row['visualizado_logistica'] > 0) {
				$qtd_nao_visualizado = str_pad ($row['visualizado_logistica'], 2, '0', STR_PAD_LEFT);
				echo "<div class='ribbon-wrapper-green'><div class='ribbon-green'> {$qtd_nao_visualizado} Novo(s)</div></div>";
			}
			echo "<table width='100%'>";
			echo "<tr>";
			echo "<td>";
			echo "<center><button class='{$class_button_paciente}' p='$paciente'  primeira-prescricao = '{$row['primeira_prescricao']}'  carater='p' presc='{$presc}' tipo='1' url='{$url}'></button></center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center><b>".strtoUpper($row['nome'])."</b></center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<div class='ui-state-highlight' style='margin-top: 5px;'>";
			echo "<center><b>".strtoUpper($row['cidade'])."</b></center>";
			echo "</div>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center style='font-size:9px;'>{$row['solicitante']}</center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center>{$row['data_auditado']}</center>";
			echo "</td>";
			echo "</tr>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo utf8_encode("<center style='color:red;'>itens ({$row['totalPendentes']})</center>");
			echo "</td>";
			echo "</tr>";
			echo "</table>";
			echo "</div>";
		}
		//CONDENSADO PERIODICAS
		echo "<div style='border:1px solid #fff;width:240px;display:table;float:left;margin:0 auto;padding:15px; font-size:12px;'>";
		echo "<table width='100%'>";
		echo "<tr>";
		echo "<td>";
		echo "<center><button class='bpacotePaciente' p='{$ur}'  primeira-prescricao = '{$row['primeira_prescricao']}' carater='p' tipo='2' url='{$url}'></button></center>";
		echo "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td>";
		echo "<center><b>".strtoUpper($nome_ur)."</b></center>";
		echo "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td>";
		echo "<center style='font-size:9px;color:#006400'><b>(CONDENSADO)</b></center>";
		echo "</td>";
		echo "</tr>";
		echo "</tr>";
		echo "<tr>";
		echo "<td>";
		echo utf8_encode("<center style='color:red;'>itens ({$condensadoPeriodico})</center>");
		echo "</td>";
		echo "</tr>";
		echo "</table>";
		echo "</div>";

		// Fecha aba Periodicas
		echo "</div>";

		echo '<div style="display: table;" id="tabs-todos">';
		echo '<div class="ui-state-highlight" style="margin-top: 5px;">
						<center>
							<strong>
								Esta seção contempla as solicitações da aba "Emergenciais" e da Aba "Prescrições Periódicas"!
							</strong>
						</center>
					</div>
					<br>';
		foreach($todosPedidos as $paciente => $row){
			$condensadoTodos += $row['totalPendentes'];
			$nome_ur = $row['nome_empresa'];
			$vazio = false;
            $presc = $row['prescricao'];
			$class_button_paciente = $row['primeira_prescricao'] == 0 ? "bpaciente" : "bpaciente_prioridade";

			$hora = "";
//			if($row['status'] == 2){
//				$hora = "<b>EMERG&Ecirc;NCIA</b> <img src='../utils/sirene_ativa.gif' style='width:10px;' />";
//			}
//
//			echo "<div style='position: relative;border:1px solid #fff;width:240px;display:table;float:left;margin:0 auto;padding:15px; font-size:12px;'>";
//			if ($row['visualizado_logistica'] > 0) {
//				$qtd_nao_visualizado = str_pad ($row['visualizado_logistica'], 2, '0', STR_PAD_LEFT);
//				echo "<div class='ribbon-wrapper-green'><div class='ribbon-green'> {$qtd_nao_visualizado} Novo(s)</div></div>";
//			}
////			echo "<table width='100%'>";
//			echo "<tr>";
//			echo "<td>";
//			echo "<center><button class='{$class_button_paciente}' p='$paciente' carater='t' presc='{$presc}' tipo='1' url='{$url}'></button></center>";
//			echo "</td>";
//			echo "</tr>";
//			echo "<tr>";
//			echo "<td>";
//			echo "<center><b>".strtoUpper($row['nome'])."</b></center>";
//			echo "</td>";
//			echo "</tr>";
//			echo "<tr>";
//			echo "<td>";
//			echo "<center style='font-size:9px;'>{$row['solicitante']}</center>";
//			echo "</td>";
//			echo "</tr>";
//			echo "<tr>";
//			echo "<td>";
//			echo "<center>{$row['data_auditado']}</center>";
//			echo "</td>";
//			echo "</tr>";
//			echo "<tr>";
//			echo "<td>";
//			echo "<center style='font-size:9px;'>{$hora}</center>";
//			echo "</td>";
//			echo "</tr>";
//			echo "</tr>";
//			echo "<tr>";
//			echo "<td>";
//			echo utf8_encode("<center style='color:red;'>itens ({$row['totalPendentes']})</center>");
//			echo "</td>";
//			echo "</tr>";
//			echo "</table>";
//			echo "</div>";
		}
		//CONDENSADO TODOS
		echo "<div style='border:1px solid #fff;width:240px;display:table;float:left;margin:0 auto;padding:15px; font-size:12px;'>";
		echo "<table width='100%'>";
		echo "<tr>";
		echo "<td>";
		echo "<center><button class='bpacotePaciente' p='{$ur}' carater='t' primeira-prescricao = '{$row['primeira_prescricao']}'   tipo='2' url='{$url}'></button></center>";
		echo "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td>";
		echo "<center><b>".strtoUpper($nome_ur)."</b></center>";
		echo "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td>";
		echo "<center style='font-size:9px;color:#006400'><b>(CONDENSADO)</b></center>";
		echo "</td>";
		echo "</tr>";
		echo "</tr>";
		echo "<tr>";
		echo "<td>";
		echo utf8_encode("<center style='color:red;'>itens ({$condensadoTodos})</center>");
		echo "</td>";
		echo "</tr>";
		echo "</table>";
		echo "</div>";

		// Fecha aba Outros
		echo "</div>";

		// Fecha Container tabs
		echo "</div>";

		if($vazio) echo "<p style='text-align:center;color:red;'><b>Nenhuma solicita&ccedil;&atilde;o pendente!</b></p>";

	}

	public function pedidos($p){

		echo "<div><center><h1>Pedidos Pendentes</h1></center></div>";



		/* DIV COM OS �LTIMOS PEDIDOS */

		/*$sql = "SELECT DISTINCT c.nome, s.paciente,s.idPrescricao,(case idPrescricao WHEN -1 THEN MIN(DATE_FORMAT(s.data, '%d/%m/%Y'))
    ELSE MIN(DATE_FORMAT(s.data_auditado, '%d/%m/%Y')) END) as data,
    (case s.status WHEN 2 THEN '<b>EMERG&Ecirc;NCIA</b> <img src=\'../utils/sirene_ativa.gif\' style=\'width:10px;\'>'
    ELSE MIN(DATE_FORMAT(s.data_auditado, '%H:%i:%S')) END)
    as hora,U.nome as usuario,
    (SELECT count(*)
    FROM solicitacoes as X, brasindice as b
    WHERE X.paciente = s.paciente AND (X.tipo = 0 OR X.tipo = 1 OR X.tipo = 12 OR X.tipo = 5 OR X.tipo = 3) AND (case X.status WHEN 2 THEN qtd > enviado
    ELSE autorizado > enviado END) AND (X.status = '1' OR X.status = '2')
    AND b.numero_tiss = X.cod ) as itens
    FROM solicitacoes as s, clientes as c, usuarios as U  WHERE s.paciente = c.idClientes AND U.idUsuarios = s.enfermeiro
    AND (case s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND (s.status = '1' OR s.status = '2')
    and c.empresa ={$ur} Group BY s.paciente
    ORDER BY MIN(s.data) ASC";*/

		$sql = "SELECT p.ID, DATE_FORMAT(p.DATA, '%d/%m/%Y') as DATA, U.nome,DATE_FORMAT(p.DATA, '%H:%i:%S') as hora,
  		(CASE WHEN COUNT( * ) >1 THEN SUM( i.QTD ) ELSE i.QTD END) -
  		(CASE WHEN COUNT( * ) >1 THEN sum( i.ENVIADO ) ELSE i.ENVIADO END) AS
  		itens, sum((case i.EMERGENCIA when 1 then 1 else 0 END)) as emergencia
  		
  		FROM
  		itenspedidosinterno AS i INNER JOIN
  		pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
  		inner join  usuarios AS U on (U.idUsuarios = p.USUARIO)
  		inner join empresas AS E on (p.EMPRESA = E.id)
  		WHERE (
  		i.STATUS = 'A'	OR

  		((i.QTD - i.ENVIADO) >0 AND i.STATUS <> 'T')) and p.EMPRESA ={$p}
                    and i.CANCELADO = 'N' and i.CANCELADO_ENTRADA = 'N' and 
                    p.CONFIRMADO ='S'
                    
                    GROUP BY p.ID order by id ";

		$r = mysql_query($sql);

		while($row = mysql_fetch_array($r)){
			foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
			$vazio = false;
			$presc=0;
			if($row['idPrescricao']!=-1){
				$presc = $row['idPrescricao'];
			}
			if($row['emergencia'] > 0){
				$hora = "<b>EMERG&Ecirc;NCIA</b> <img src='../utils/sirene_ativa.gif' style='width:10px;' />";

			}else{
				$hora = $row['hora'];

			}

			echo "<div style='border:1px solid #fff;width:240px;display:table;float:left;margin:0 auto;padding:15px; font-size:12px;'>";
			echo "<table width='100%'>";
			echo "<tr>";
			echo "<td>";
			echo "<center><button class='bpedido' p='{$row['ID']}' idempresa='{$p}' presc='{$presc}' tipo='1' url='{$url}'></button></center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center><b> PEDIDO N&deg;".strtoUpper($row['ID'])."</b></center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center style='font-size:9px;'>(".strtoUpper($row['nome']).")</center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center>".$row['DATA']."</center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center style='font-size:9px;'>".$hora."</center>";
			echo "</td>";
			echo "</tr>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo utf8_encode("<center style='color:red;'>itens (".$row['itens'].")</center>");
			echo "</td>";
			echo "</tr>";
			echo "</table>";
			echo "</div>";
		}

		//// caixa condensado
		$sql2 = "
  		SELECT DISTINCT E.nome, DATE_FORMAT(p.DATA,'%d/%m/%Y %H:%i:%s') AS DATA,p.EMPRESA,
	    (CASE WHEN COUNT( * ) >1 THEN SUM( i.QTD ) ELSE i.QTD END) -
 (CASE WHEN COUNT( * ) >1 THEN sum( i.ENVIADO ) ELSE i.ENVIADO END) AS
itens,sum((CASE i.EMERGENCIA WHEN 1 THEN 1 ELSE 0 END)) as emergencia

	FROM
		itenspedidosinterno AS i INNER JOIN
		pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
		inner join  usuarios AS U on (U.idUsuarios = p.USUARIO)
		inner join empresas AS E on (p.EMPRESA = E.id)
	WHERE (
		i.STATUS = 'A'	OR
		((i.QTD - i.ENVIADO) >0) and i.STATUS != 'F' and i.STATUS != 'T')
			and i.CANCELADO ='N' and i.CANCELADO_ENTRADA ='N' and p.CONFIRMADO ='S' and p.EMPRESA ={$p}
	      GROUP BY 	p.EMPRESA";

		/*fim da alteração 2013-08-12*/
		$r2 = mysql_query($sql2);
		while($row2 = mysql_fetch_array($r2)){

			if($row2['emergencia'] > 0){

				$hora = "<b>EMERG&Ecirc;NCIA</b> <img src='../utils/sirene_ativa.gif' style='width:10px;' />";

			}else{
				$hora = $row2['hora'];

			}
			echo $row2['empresa'];
			echo "<div style='border:1px solid #fff;display:table;float:left;margin:0 auto;padding:15px; font-size:12px;'>";
			echo "<table width='100%'>";
			echo "<tr>";
			echo "<td>";
			echo "<center><button class='pedido_condensado' empresa='{$row2['EMPRESA']}' nome-empresa = '{$row2['nome']}' ></button></center>";
			echo "<center><b>PEDIDOS CONDENSADOS</b></center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center><b>".strtoUpper($row2['nome'])."</b></center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center style='font-size:9px;'>(".$row2['DATA'].")</center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center> ".$row2['data']."</center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center style='font-size:9px;'>".$hora."</center>";
			echo "</td>";
			echo "</tr>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo utf8_encode("<center style='color:red;'>itens (".$row2['itens'].")</center>");
			echo "</td>";
			echo "</tr>";
			echo "</table>";

			echo "</div>";
		}


	}


	public function pedidoCondensado($ur,$nome)
	{
		echo "<div><center><h1>Pedidos Condensado {$nome} </h1></center></div>";
		$sql ="
		SELECT
  	        SUM(i.QTD - i.ENVIADO) as quantidade ,
			concat(b.principio,' ', b.apresentacao) as item,
			b.tipo,
			case
			when b.tipo =0 THEN 'MEDICAMENTO'
			WHEN b.tipo =1 THEN 'MATERIAL'
			WHEN b.tipo =3 THEN 'DIETA' end as nometipo
  	  		FROM
  	  		itenspedidosinterno AS i
  	  		INNER JOIN pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
  	  		INNER JOIN catalogo AS b ON ( b.ID = i.CATALOGO_ID )
  	  		LEFT JOIN catalogo_principio_ativo AS d ON d.catalogo_id = b.ID
			LEFt JOIN principio_ativo AS e ON e.id = d.principio_ativo_id
  	  		WHERE
			(i.STATUS = 'A'	OR ((i.QTD - i.ENVIADO) >0) and i.STATUS != 'F' and i.STATUS != 'T')	and
			i.CANCELADO ='N' and
			i.CANCELADO_ENTRADA ='N' and
			p.CONFIRMADO ='S' and
			p.EMPRESA={$ur}
GROUP BY i.CATALOGO_ID
UNION
SELECT
  	SUM(i.QTD -  	i.ENVIADO) as quantidade  ,
    cb.item,
		5 as tipo,
		'EQUIPAMNTO' as nometipo
  	FROM
  	itenspedidosinterno AS i INNER JOIN
  	pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
  	INNER JOIN cobrancaplanos as cb ON ( cb.id = i.CATALOGO_ID )
  	WHERE (i.STATUS = 'A'	OR ((i.QTD - i.ENVIADO) >0) and i.STATUS != 'F' and i.STATUS != 'T')	and
	i.CANCELADO ='N' and
	i.CANCELADO_ENTRADA ='N' and
	p.CONFIRMADO ='S' and
	p.EMPRESA= {$ur} AND i.TIPO = 5
group by cb.id
		ORDER BY tipo,item
		";



		$result = mysql_query($sql);
		echo "<table class='mytable' width='100%'>
<thead>
<th>Item</th>
<th>Quantidade</th>
</thead>";
		$i =0;
		while($row = mysql_fetch_array($result)){
			$style = '';
			if($i % 2 <> 0){
				$style='background-color:#d3d3d3';
			}
			$i++;
		echo "<tr style='{$style}'><td width='90%'><b>{$row['nometipo']}:</b> {$row['item']}</td><td width='10%'>{$row['quantidade']}</td></tr>";

		}
		echo "</table>";

		echo "<form action='imprimir_pedido_ur_condensado.php?emp={$ur}&np={$nome}' method='POST' target='_blank'>";
		echo "<button id='imprimir-saida' style='display:inline;float:left;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
  	<span class='ui-button-text'>Imprimir</span></button>";
		echo "</form>";




	}


	public function pendentes($url=''){

		$vazio = true;
		$this->imprimir_check();

		//header("Cache-Control: no-cache, must-revalidate");
		echo "<div><center><h1>Solicita&ccedil;&otilde;es Pendentes</h1></center>";

		$consulta = mysql_fetch_array(mysql_query("
  			SELECT
  			COUNT(*) as sol
  			FROM
  			solicitacoes as s
  			WHERE
  			s.status = '1' OR s.status = '2'"));




		if(!isset($_SESSION['id_max_sol'])){
			$_SESSION['id_max_sol']=$consulta['sol'];

		}else if($_SESSION['id_max_sol']!=$consulta['sol']){

			$ultimos = array();
			if(isset($_COOKIE['pendentes'])){
				$ultimos = unserialize($_COOKIE['pendentes']);
			}

			$ultimos[] = $_SESSION['NON'];
			setcookie('pendentes',serialize($ultimos));

			if($_SESSION['status']==1){
				echo "<audio src='alerta.mp3' autoplay=true></audio>";
				$_SESSION['id_max_sol'] = $consulta['sol'];
			}else{
				echo "<audio src='sirene.mid' autoplay=true></audio>";
				$_SESSION['id_max_sol'] = $consulta['sol'];
			}

		}

        if($_SESSION['empresa_principal'] == 1 || $_SESSION['empresa_principal'] == 11){
            $sqlComprarAutomatica = "SELECT
                                        empresas.id
                                        FROM
                                        empresas
                                        WHERE
                                        empresas.compra_automatica_central = 'S' ";
            $resultComprarAutomatica = mysql_query($sqlComprarAutomatica);
            $idEmpresasComprarAutomatica [] = 1;
            while($row = mysql_fetch_array($resultComprarAutomatica)){
                $idEmpresasComprarAutomatica[] = $row['id'];

            }
            $idEmpresas = implode(',',$idEmpresasComprarAutomatica);

        }else{
            $idEmpresas = $_SESSION['empresa_principal'];
        }

		$nomess = unserialize($_COOKIE['pendentes']);

		/* DIV COM OS �LTIMOS PEDIDOS */
		echo "<div style='display:table;width:100%;border:1px dotted #ccc;background-color:#4B5054;color:#fff;'>";
		$z=0;
		foreach($nomess as $key=>$no){
			$z++;
			if($z<=6){
				echo "<span style='padding:10px;float:left;'><b>| ".strtoupper($no)."</b></span>";
			}else{
				setcookie('pendentes');
				$ultimos[] = $_SESSION['NON'];
				setcookie('pendentes',serialize($ultimos));
				$nomess = unserialize($_COOKIE['pendentes']);
				echo "<span style='padding:10px;float:left;'><b>| ".strtoupper($no)."</b></span>";
			}
		}

		echo "</div>";
		/* DIV COM OS �LTIMOS PEDIDOS */

	/*	$sql2 = <<<SQL
SELECT
	e.nome,
	c.empresa,
	u.nome AS usuario,
	(
		IF (
			idPrescricao = - 1,
			MIN(
				DATE_FORMAT(s. DATA, '%d/%m/%Y')
			),
			MIN(
				DATE_FORMAT(s.data_auditado, '%d/%m/%Y')
			)
		)
	) AS `data`,
	(
		IF (
			s. STATUS = 2,
			'<b>EMERG&Ecirc;NCIA</b> <img src=\'../utils/sirene_ativa.gif\' style=\'width:10px;\'>',
			MAX(
				DATE_FORMAT(
					s.data_auditado,
					'%d/%m/%Y %H:%i:%S'
				)
			)
		)
	) AS hora,
	(
		SELECT
			(
				IF (
					COUNT(*) > 1,
					IF (
						s2.`status` = 2,
						SUM(s2.qtd),
						SUM(s2.autorizado)
					),
					IF (
						s2. STATUS = 2,
						s2.qtd,
						s2.autorizado
					)
				)
				- -- SUBTRAÇÃO
				IF (
					COUNT(*) > 1,
					(SUM(s2.enviado)),
					s2.enviado
				)
			) AS itens
		FROM
			solicitacoes AS s2
		INNER JOIN catalogo AS ct ON (s2.CATALOGO_ID = ct.ID)
		INNER JOIN clientes AS cl ON (s2.paciente = cl.idClientes)
		WHERE
			s2.paciente = s.paciente
		AND (
			s2.tipo = 0
			OR s2.tipo = 1
			OR s2.tipo = 5
			OR s2.tipo = 3
		)
		AND
		IF (
			s2.`status` = 2,
			s2.qtd > s2.enviado,
			s2.autorizado > s2.enviado
		)
		AND (
			s2.`status` = '1'
			OR s2.`status` = '2'
		)
	) AS itens
FROM
	solicitacoes AS s
INNER JOIN clientes AS c ON (s.paciente = c.idClientes)
INNER JOIN usuarios AS u ON (u.idUsuarios = s.enfermeiro)
INNER JOIN empresas AS e ON (e.id = c.empresa)
WHERE
	(
		s.tipo = 0
		OR s.tipo = 1
		OR s.tipo = 5
		OR s.tipo = 3
	)
AND
IF (
	s.`status` = 2,
	s.qtd > s.enviado,
	s.autorizado > s.enviado
)
AND (
	s.`status` = '1'
	OR s.`status` = '2'
)
AND c.empresa in ($idEmpresas)
GROUP BY
	s.paciente
ORDER BY
	c.empresa,
	MIN(s. DATA) ASC
SQL;
	*/
	$sql2 = "SELECT
e.nome,
c.empresa,
u.nome AS usuario,
s.enviado,
s.autorizado,
DATE_FORMAT(s.data_auditado, '%d/%m/%Y %H:%i') as data_hora,
s. STATUS as status,
s.data_auditado,
s.qtd

FROM
solicitacoes AS s INNER JOIN
clientes AS c ON (s.paciente = c.idClientes) INNER JOIN
usuarios AS u ON (u.idUsuarios = s.enfermeiro) INNER JOIN
empresas AS e ON (e.id = c.empresa)
WHERE
( s.tipo = 0 OR s.tipo = 1 OR s.tipo = 5 OR s.tipo = 3 ) AND IF
		( s.`status` = 2, s.qtd > s.enviado, s.autorizado > s.enviado ) AND
( s.`status` = '1' OR s.`status` = '2' ) AND
s.qtd <> 0 AND
c.empresa
in ($idEmpresas)
ORDER BY c.empresa ASC
";


		$r2 = mysql_query($sql2);
		$pedidos = [];
		$empresa=0;
		while($row2 = mysql_fetch_array($r2)) {

			if($empresa != $row2['empresa']){
				$data = $row2['data_auditado'];
				$pedidos[$row2['empresa']]['data_hora'] = $row2['data_hora'];
				$pedidos[$row2['empresa']]['data_auditado'] = $row2['data_auditado'];
				$pedidos[$row2['empresa']]['solicitante'] = $row2['usuario'];
			}else{
				if($row2['data_auditada'] > $data ){
					$pedidos[$row2['empresa']]['data_auditado'] = $row2['data_auditado'];
					$pedidos[$row2['empresa']]['data_hora'] = $row2['data_hora'];
					$pedidos[$row2['empresa']]['solicitante'] = $row2['usuario'];
				}
			}

			$pedidos[$row2['empresa']]['nome'] = $row2['nome'];
			if($pedidos[$row2['empresa']]['status'] != 2)
			$pedidos[$row2['empresa']]['status'] = $row2['status'];

			if($row2['status'] == 2){
				$pedidos[$row2['empresa']]['totalPendentes'] += $row2['qtd'] - $row2['enviado'] ;
				$emergencia = "<b>EMERG&Ecirc;NCIA</b> <img src='../utils/sirene_ativa.gif' style='width:10px;'>";
			}else{
				$pedidos[$row2['empresa']]['totalPendentes'] += $row2['autorizado'] - $row2['enviado'] ;
			}

			$empresa = $row2['empresa'];
		}
		//echo '<pre>'; die(var_dump($pedidos));
        echo "
<div  style='display:table;width:100%;'>
    <fieldset style='border:2px solid black;'>
        <legend><b>Pedidos para Pacientes</b></legend>";

		foreach($pedidos as $ur => $dados){

			$vazio = false;

			echo "<div style='border:1px solid #fff;display:table;float:left;margin:0 auto;padding:15px; font-size:12px;'>";
			echo "<table width='100%'>";
			echo "<tr>";
			echo "<td>";
			echo "<center><button class='bpacote' p='{$ur}' tipo='2'></button></center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center><b>".strtoUpper($dados['nome'])."</b></center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center style='font-size:9px;'>(".implode('/',array_reverse(explode("-",$dados['solicitante']))).")</center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center>".$dados['data_hora']."</center>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo "<center style='font-size:9px;'>{$emergencia}</center>";
			echo "</td>";
			echo "</tr>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>";
			echo utf8_encode("<center style='color:red;'>itens (".$dados['totalPendentes'].")</center>");
			echo "</td>";
			echo "</tr>";
			echo "</table>";
			echo "</div>";
		}
        echo "</fieldset>
                    </div>";

		if($_SESSION['empresa_principal'] == 1){

			$sql2 = "
  		SELECT DISTINCT E.nome, DATE_FORMAT(p.DATA,'%d/%m/%Y %H:%i:%s') AS DATA,p.EMPRESA,
	    (CASE WHEN COUNT( * ) >1 THEN SUM( i.QTD ) ELSE i.QTD END) -
 (CASE WHEN COUNT( * ) >1 THEN sum( i.ENVIADO ) ELSE i.ENVIADO END) AS  
itens,sum((CASE i.EMERGENCIA WHEN 1 THEN 1 ELSE 0 END)) as emergencia

	FROM 
		itenspedidosinterno AS i INNER JOIN 
		pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
		inner join  usuarios AS U on (U.idUsuarios = p.USUARIO)	
		inner join empresas AS E on (p.EMPRESA = E.id)
	WHERE (
		i.STATUS = 'A'	OR 
		((i.QTD - i.ENVIADO) >0) and i.STATUS != 'F' and i.STATUS != 'T')
			and i.CANCELADO ='N' and i.CANCELADO_ENTRADA ='N' and p.CONFIRMADO ='S'
	      GROUP BY 	p.EMPRESA";

			/*fim da alteração 2013-08-12*/
			$r2 = mysql_query($sql2);
			echo "<br>";
			echo "<div style='display:table;width:100%;' > 
                    <fieldset style='border:2px solid black;'>
                    <legend><b>Pedidos de Compras das Unidades Regionais:</b></legend>";
			while($row2 = mysql_fetch_array($r2)){

				if($row2['emergencia'] > 0){

					$hora = "<b>EMERG&Ecirc;NCIA</b> <img src='../utils/sirene_ativa.gif' style='width:10px;' />";

				}else{
					$hora = $row2['hora'];

				}
				echo $row2['empresa'];
				echo "<div style='border:1px solid #fff;display:table;float:left;margin:0 auto;padding:15px; font-size:12px;'>";
				echo "<table width='100%'>";
				echo "<tr>";
				echo "<td>";
				echo "<center><button class='bpacote_pedido' p='{$row2['EMPRESA']}' presc='{$presc}' tipo='2' url='{$url}'></button></center>";
				echo "<center><b>Pedidos de Compras</b></center>";
				echo "</td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td>";
				echo "<center><b>".strtoUpper($row2['nome'])."</b></center>";
				echo "</td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td>";
				echo "<center style='font-size:9px;'>(".$row2['DATA'].")</center>";
				echo "</td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td>";
				echo "<center> ".$row2['data']."</center>";
				echo "</td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td>";
				echo "<center style='font-size:9px;'>".$hora."</center>";
				echo "</td>";
				echo "</tr>";
				echo "</tr>";
				echo "<tr>";
				echo "<td>";
				echo utf8_encode("<center style='color:red;'>itens (".$row2['itens'].")</center>");
				echo "</td>";
				echo "</tr>";
				echo "</table>";

				echo "</div>";
			}
			echo "</fildset>
                </div>";

		}
		/*****************************************************************************************************/
		echo "<div style='display:table;width:100%;'><button id='monitor' tipo='' style='float:right;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Modo Monitor</span></button></form></div>";
		if($vazio) echo "<p style='text-align:center;color:red;'><b>Nenhuma solicita&ccedil;&atilde;o pendente!</b></p></div>";
		else echo "</div>";

		echo "<br>";

	}

	private function imprimir_check(){
		echo "<div id='formulario-check' title='Check-List' p='' presc='0' tipo='1' primeira=''>";
		echo alerta();
		echo info("<b>Aten&ccedil;&atilde;o:</b> A log&iacute;stica deve imprimir o Check-List antes de fazer qualquer lan&ccedil;amento no estoque.");
		echo "<p><center><b>Deseja imprimir o Check-List Agora?</b></center></p><br/></div>";

	}

	public function pedido_saida($p,$emp){
        $i = 0;
        $fracionar = "<img src='/utils/fraction_64x64.png' width='16' class='fracionar-pedido' style='cursor: pointer;' title='Fracionar'>";

		$opcao = 'c.empresa';
		$row = mysql_fetch_array(mysql_query("SELECT e.nome FROM empresas as e inner join pedidosinterno as p on (p.EMPRESA = e.id) WHERE p.ID = '$p'"));


		echo "<div id='cancelar-item-pedido' title='Cancelar item' id_item='' >";
		echo alerta();
		echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar o cancelamento, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
		echo "<p><b>Justificativa:</b><input type='text' name='justificativa' class='OBG' size='100' /></div>";
		$this->formularioSaidaDietaMaterialPedidoInterno();
		// $this->formularioTrocaMedicamentoPedidoInterno();
		$this->formularioSaidaMedicamentoPedidoInterno();

		$envio = "<input type='text' name='qtd-envio' class='numerico quantidade' style='text-align:right' size='4' />";

		echo "<center><h1>Sa&iacute;da para o pedido N&deg; {$p}</h1></center>";
		echo "<center><h3> " . String::removeAcentosViaEReg($row['nome']) . "</h3></center>";


		echo "<div id='dialog-relatorio-pedido' title='Relat&oacute;rio' ><img src='../utils/logo.jpg' width=30% style='align:center' /><div id='pre-relatorio-pedido'></div>
  		  <form action='print.php' method='post' name='form_relatorio' id='print-relatorio'>
          <input type='hidden' name='relatorio' value='' id='relatorio-pedido' />
          <input type='submit' value='Imprimir' />
          </form></div>";
		echo "<div id='div-envio-ur' ur='$emp' >";
		echo alerta();

		echo "<p><b>Data de Entrega:</b><input type='text' class='data OBG' id='data-envio' name='data_envio' >";
		//	echo "<p><b>Observação:</b><br><textarea name='obs' id='obs' rows='2' cols='140'/></textarea>";

		$sql="select OBSERVACAO,DATE_FORMAT(DATA_MAX_ENTREGA,'%d/%m/%Y' ) as data from pedidosinterno where ID = {$p}";
		$result = mysql_query($sql);
		$obs = mysql_result($result, 0,0);
		$data = mysql_result($result, 0,1);

		echo "<p width='90%'><b>OBSERVA&Ccedil;&Atilde;O:</b> {$obs}</p>";
		echo "<p width='90%'><b>DATA MAXIMA PARA ENTREGA:</b> {$data}</p>";
		echo "<p><label for='excluir-total' style='display:inline;'><input type='checkbox' style='display:inline;' id='excluir-total' /><b>Cancelar todo o Pedido.</b></label></p>";
		echo "<p><span id='span-justificatica-excluir-total' style='display:none'><b>Justificativa:</b>
                    </br>
                    <div id ='div-justificativa-excluir-total'>
                    <textarea rowls='30' cols='30' id='text-justificatica-excluir-total' style='display:none'></textarea>
                     </div></span></p>";
		echo "<table class='mytable' width=100% id='lista-enviados' ><thead><tr><th>Itens Solicitados</th><th align='center' width='1%'>Enviado/Pedido</th>
  			<th align='center' width='1%'>Enviar</th><th align='center' width='1%'>Lote</th></tr></thead>";

		/*$sqlmed = "SELECT s.enfermeiro,(CASE WHEN COUNT(*)>1 THEN SUM(s.enviado) ELSE s.enviado END) AS enviados,sum((CASE s.status when 2 THEN  s.qtd ELSE  s.autorizado  END)) as autorizado, s.enviado, s.cod, s.idSolicitacoes as sol, s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data, b.principio, b.apresentacao
FROM solicitacoes as s, brasindice as b, clientes as c
WHERE {$opcao} = '$p' AND s.paciente = c.idClientes AND s.tipo = '0' AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND (s.status = '1' OR s.status = '2')
AND b.numero_tiss = s.cod GROUP BY cod ORDER BY principio, apresentacao,enviado";*/

		$sqlmed = "SELECT
  	        i.QTD , 
  	        i.ENVIADO ,
  	        i.ID,
  	        i.PEDIDOS_INTERNO_ID, 
			i.NUMERO_TISS,
			i.CATALOGO_ID,
  	  		b.principio,
  	  		b.apresentacao,
  	  		i.EMERGENCIA,
  	  		e.principio AS principioAtivo,
  	  		b.lab_desc
  	  		FROM
  	  		itenspedidosinterno AS i 
  	  		INNER JOIN pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
  	  		INNER JOIN catalogo AS b ON ( b.ID = i.CATALOGO_ID )
  	  		LEFT JOIN catalogo_principio_ativo AS d ON d.catalogo_id = b.ID
			LEFT JOIN principio_ativo AS e ON e.id = d.principio_ativo_id
  	  		WHERE 
  	  		i.STATUS IN ('A', 'P')
  	  		AND (i.QTD - i.ENVIADO) > 0
  	  		AND i.TIPO = 0 
  	  		AND p.ID = '{$p}'
  	  		AND i.CANCELADO = 'N' 
  	  		AND i.CANCELADO_ENTRADA = 'N'
  	  		
  	  		 ORDER BY principio, apresentacao";
		$rmed = mysql_query($sqlmed);
		while($row = mysql_fetch_array($rmed)){
			foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
            $background = $i % 2 != 0 ? 'bgcolor="#a9a9a9"' : '';
			$qtd=$row['QTD']-$row['ENVIADO'];
			$dados = "autorizado='{$row['autorizado']}' qtd='{$qtd}' id_pedido='{$row['PEDIDOS_INTERNO_ID']}'
					id_item='{$row['ID']}' n='{$row['principio']}' tiss='{$row['NUMERO_TISS']}' sn='{$row['apresentacao']}'
					catalogo_id='{$row[CATALOGO_ID]}' soldata='{$row['data']}' env='{$row['enviados']}'
					total='{$row['QTD']}' tipo='0'";
			// $cancela = "<img align='right' id_item='{$row['ID']}' class='cancela-item-pedido' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
           // $trocar = "<img align='right' ped='{$row['ID']}' catalogo_id='{$row['CATALOGO_ID']}' tipo='med' class='troca-item-med-pedido' src='../utils/exchange_16x16.png' title='Trocar' border='0'>";

			$pedidos = ($row['QTD'] - $row['ENVIADO']) ;
			if($row["EMERGENCIA"] == 1){
				$cor = "style='color:red;'";
			}else{
				$cor ='';
			}
			echo "<tr  >
                    <td >

                        <b>MEDICAMENTO: </b>
                        {$row['principio']} {$row['apresentacao']} (<b>LAB: </b>{$row['lab_desc']}) {$cancela}
                        <br>
                        <u style='margin-left: 19px;'><b>PRINCÍPIO ATIVO: </b> {$row['principioAtivo']} </u>
                    </td>";
			echo "<td align='center'>{$row['ENVIADO']}/{$row['QTD']}
	<input type='hidden' class='excluir-dados' $dados /></td>";
//			<td class='dados' $dados >
//  	  		<input type='text' name='qtd-envio' class='numerico quantidade' style='text-align:right' size='4' value='' autocomplete='off'/></td>
//  	  		<td><input type='text' name='lote'  style='text-align:right' size='8' autocomplete='off'/></td>

  	  		echo "<td>
							<button id='' style='display:inline;float:left;background-color: #FF0000'
								class='ui-button ui-widget  ui-corner-all ui-button-text-only cancela-item-pedido'
								 role='button' aria-disabled='false'
								id_item='{$row['ID']}'  >
  								<span class='ui-button-text'><b>Cancelar</b></span>
  							</button>
						</td>
						<td>
							<button  style='display:inline;float:left;background-color: #8ebf45'
									$dados
									class=' adicionar-medicamento-saida-pedido ui-button  ui-widget ui-corner-all ui-button-text-only '
									 role='button' aria-disabled='false'>
									<span class='ui-button-text'><b>Adicionar</b></span>
								</button>

							</td>
  	  		</tr>
  	  		<tbody id='linha_{$row['ID']}'></tbody>";
            $i++;
		}

		/*$sqldie = "SELECT s.enfermeiro,(CASE WHEN COUNT(*)>1 THEN SUM(s.enviado) ELSE s.enviado END) AS enviados,sum((CASE s.status when 2 THEN  s.qtd ELSE  s.autorizado  END)) as autorizado, s.enviado, s.cod, s.idSolicitacoes as sol,
    s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data, b.principio, b.apresentacao
    FROM solicitacoes as s, brasindice as b, clientes as c
    WHERE {$opcao} = '$p' AND s.paciente = c.idClientes AND s.tipo = '3'
    AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND (s.status = '1' OR s.status = '2')
    AND b.numero_tiss = s.cod GROUP BY cod ORDER BY principio, apresentacao,enviado";*/
		$sqldie= "SELECT
  	        i.QTD , 
  	        i.ENVIADO ,
  	        i.ID,
  	        i.PEDIDOS_INTERNO_ID,
			 i.PEDIDOS_INTERNO_ID, 
			i.NUMERO_TISS,
			i.CATALOGO_ID,
  	  		b.principio,
  	  		b.apresentacao,
  	  		i.EMERGENCIA,
  	        b.lab_desc
  	  		FROM
  	  		itenspedidosinterno AS i INNER JOIN
  	  		pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
  	  		INNER JOIN catalogo AS b ON ( b.ID = i.CATALOGO_ID )
  	  		WHERE
  	  		i.STATUS IN ('A', 'P')
  	  		AND (i.QTD - i.ENVIADO) > 0
  	  		AND i.TIPO = 3 
  	  		AND p.ID = '{$p}'
  	  		AND i.CANCELADO = 'N' 
  	  		AND i.CANCELADO_ENTRADA = 'N'
  	  	    ORDER BY principio, apresentacao";


		$rdie = mysql_query($sqldie);
		while($row = mysql_fetch_array($rdie)){
			foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
            $background = $i % 2 != 0 ? 'bgcolor="#a9a9a9"' : '';
			$qtd=$row['QTD']-$row['ENVIADO'];
			$dados = "autorizado='{$row['autorizado']}' qtd='{$qtd}' id_pedido='{$row['PEDIDOS_INTERNO_ID']}' id_item='{$row['ID']}' n='{$row['principio']}' tiss='{$row['NUMERO_TISS']}' catalogo_id='{$row[CATALOGO_ID]}' sn='{$row['apresentacao']}' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['QTD']}' autorizado='{$row['autorizado']}' tipo='3'";
		//	$cancela = "<img align='right' id_item='{$row['ID']}' class='cancela-item-pedido' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
          //  $trocar = "<img align='right' ped='{$row['ID']}' catalogo_id='{$row['CATALOGO_ID']}' tipo='die' class='troca-item-mat-die-pedido' src='../utils/exchange_16x16.png' title='Trocar' border='0'>";
			$pedidos = ($row['QTD'] - $row['ENVIADO']) ;
			if($row["EMERGENCIA"] == 1){
				$cor = "style='color:red;'";
			}else{
				$cor ='';
			}

			echo "<tr  $cor >
                    <td>

                        <b>DIETA: </b>
                        {$row['principio']} {$row['apresentacao']} (<b>LAB: </b>{$row['lab_desc']}) {$cancela}
                    </td>";
			echo "<td align='center'>{$row['ENVIADO']}/{$row['QTD']}
<input type='hidden' class='excluir-dados' $dados />
</td>";

			echo "<td>
							<button id='' style='display:inline;float:left;background-color: #FF0000'
								class='ui-button ui-widget  ui-corner-all ui-button-text-only cancela-item-pedido'
								 role='button' aria-disabled='false'
								id_item='{$row['ID']}'  >
  								<span class='ui-button-text'><b>Cancelar</b></span>
  							</button>
						</td>
						<td>
							<button  style='display:inline;float:left;background-color: #8ebf45'
									$dados
									nome='".$row['principio'].$row['apresentacao']." (LAB:".$row['lab_desc'].")'
									class=' adicionar-mat-die-saida-pedido ui-button  ui-widget ui-corner-all ui-button-text-only '
									 role='button' aria-disabled='false'>
									<span class='ui-button-text'><b>Adicionar</b></span>
								</button>

							</td>
  	  				</td></tr>
			<tbody id='linha_{$row['ID']}'></tbody>";
            $i++;
		}

		$sqlmat= "SELECT
  	        i.QTD , 
  	        i.ENVIADO ,
  	        i.ID,
  	        i.PEDIDOS_INTERNO_ID,
			 i.PEDIDOS_INTERNO_ID, 
			i.NUMERO_TISS,
			i.CATALOGO_ID,
  	  		b.principio,
  	  		b.apresentacao,
  	  		i.EMERGENCIA,
  	        b.lab_desc
  	  		FROM
  	  		itenspedidosinterno AS i INNER JOIN
  	  		pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
  	  		INNER JOIN catalogo AS b ON ( b.ID = i.CATALOGO_ID )
  	  		WHERE 
  	  		i.STATUS IN ('A', 'P')
  	  		AND (i.QTD - i.ENVIADO) > 0
  	  		AND i.TIPO = 1 
  	  		AND p.ID = '{$p}'
  	  		AND i.CANCELADO = 'N' 
  	  		AND i.CANCELADO_ENTRADA = 'N'
  	  	    ORDER BY principio, apresentacao";

		$rmat = mysql_query($sqlmat);
		while($row = mysql_fetch_array($rmat)){
			foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
            $background = $i % 2 != 0 ? 'bgcolor="#a9a9a9"' : '';
			$qtd=$row['QTD']-$row['ENVIADO'];
			$dados = "autorizado='{$row['autorizado']}' qtd='{$qtd}' id_pedido='{$row['PEDIDOS_INTERNO_ID']}' id_item='{$row['ID']}' n='{$row['principio']}' tiss='{$row['NUMERO_TISS']}' catalogo_id='{$row[CATALOGO_ID]}' sn='{$row['apresentacao']}' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['QTD']}' tipo='1'";
			//$cancela = "<img align='right' id_item='{$row['ID']}' class='cancela-item-pedido' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
           // $trocar = "<img align='right'  ped='{$row['ID']}' catalogo_id='{$row['CATALOGO_ID']}' tipo='mat' class='adicionar-item-mat-die-pedido' src='../utils/exchange_16x16.png' title='Trocar' border='0'>";
			$pedidos = ($row['QTD'] - $row['ENVIADO']) ;
			if($row["EMERGENCIA"] == 1){
				$cor = "style='color:red;'";
			}else{
				$cor ='';
			}
			echo "<tr  $cor >
                    <td>

                        <b>MATERIAL: </b>
                        {$row['principio']} {$row['apresentacao']} (<b>LAB: </b>{$row['lab_desc']}) {$cancela}
                    </td>";
			echo "<td align='center'>{$row['ENVIADO']}/{$row['QTD']}
															<input type='hidden' class='excluir-dados' $dados />
					</td>";
				echo "<td>
							<button id='' style='display:inline;float:left;background-color: #FF0000'
								class='ui-button ui-widget  ui-corner-all ui-button-text-only cancela-item-pedido'
								 role='button' aria-disabled='false'
								id_item='{$row['ID']}'  >
  								<span class='ui-button-text'><b>Cancelar</b></span>
  							</button>
						</td>
						<td>
							<button  style='display:inline;float:left;background-color: #8ebf45'
									$dados
									nome='".$row['principio'].$row['apresentacao']." (LAB:".$row['lab_desc'].")'
									class=' adicionar-mat-die-saida-pedido ui-button  ui-widget ui-corner-all ui-button-text-only '
									 role='button' aria-disabled='false'>
									<span class='ui-button-text'><b>Adicionar</b></span>
								</button>

							</td>
  	  				</td></tr>
			<tbody id='linha_{$row['ID']}'></tbody>";
            $i++;
		}


		$sqlformula = "SELECT s.enfermeiro, (CASE WHEN COUNT(*)>1 THEN SUM(s.enviado) ELSE s.enviado END) AS enviados,
   				sum((CASE s.status when 2 THEN  s.qtd ELSE  s.autorizado  END)) as autorizado,
   				s.enviado, s.cod, s.idSolicitacoes as sol,
   				s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data,
   				(SELECT
   				(CASE WHEN COUNT( * ) >1 THEN SUM( i.QTD ) ELSE i.QTD END) - (CASE WHEN COUNT( * ) >1 THEN sum( i.ENVIADO ) ELSE i.ENVIADO END) AS
  	  		  dif_pedido
  	
  	  		  FROM
  	  		  itenspedidosinterno AS i INNER JOIN
  	  		  pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
  	  		  WHERE (
  	  		  i.STATUS = 'A'	OR
  	  		  (i.QTD - i.ENVIADO) >0)	AND
  	  		  i.NUMERO_TISS = s.cod AND
  	
  	  		  i.TIPO = s.tipo AND
  	  		  i.NUMERO_TISS = s.cod AND
  	  		  p.EMPRESA = {$p}
  	  		  GROUP BY
  	  		  i.NUMERO_TISS) as dif_pedido,i.NUMERO_TISS,
  	  		  f.formula,
  	  		  i.EMERGENCIA
  	  		  FROM solicitacoes as s,
  	  		  formulas as f,
  	  		  clientes as c
  	  		  WHERE {$opcao} = '$p' AND s.paciente = c.idClientes AND s.tipo = '12' AND
  	  		  (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END)
  	  		  AND (s.status = '1' OR s.status = '2') and i.CANCELADO = 'N' and i.CANCELADO_ENTRADA = 'N'
  	  		  AND f.id = s.cod GROUP BY cod ORDER BY formula,enviado";
		$rformula = mysql_query($sqlformula);
		while($row = mysql_fetch_array($rformula)){
			foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
            $background = $i % 2 != 0 ? 'bgcolor="#a9a9a9"' : '';
			$dados = "autorizado='{$row['autorizado']}' qtd='{$qtd}' sol='{$row['sol']}' cod='{$row['cod']}' n='{$row['formula']}' sn='' soldata='{$row['data']}' tiss='{$row['NUMERO_TISS']}' env='{$row['enviados']}' total='{$row['QTD']}' tipo='12'";
			$cancela = "<img align='right' id_item='{$row['ID']}' class='cancela-item-pedido' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
			//$pedidos = $row['autorizado'] - $row['enviados'];
			if ($row['dif_pedido'] == null || $row['dif_pedido'] == NULL){
				$dif = 0;

			}else{
				$dif= $row['dif_pedido'];
			}
			$pedidos = ($row['autorizado'] - $row['enviados']) -$dif;
			if($row["EMERGENCIA"] == 1){
				$cor = "style='color:red;'";
			}else{
				$cor ='';
			}
			echo "<tr $cor ><td>{$row['formula']} {$cancela}</td>";
			echo "<td align='center'>{$row['ENVIADO']}/{$row['QTD']}</td><td class='dados' $dados qtd={$qtd} >
  	  		  			<input type='text' name='qtd-envio' class='numerico quantidade' style='text-align:right' size='4' value='' autocomplete='off'/>
  	  		  			<td><input type='text' name='lote'  style='text-align:right' size='8' autocomplete='off'/></td>
  	  		  	</td></tr>";
            $i++;
		}
		///jeferson 2012-10-24
		$sqleqp =
			"SELECT
  	i.QTD ,
  	i.ENVIADO ,
  	i.ID,
  	i.PEDIDOS_INTERNO_ID,
  	cb.item,
	 i.PEDIDOS_INTERNO_ID, 
	i.NUMERO_TISS,
	i.CATALOGO_ID,
  	i.EMERGENCIA
  	
  	FROM
  	itenspedidosinterno AS i INNER JOIN
  	pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
  	INNER JOIN cobrancaplanos as cb ON ( cb.id = i.CATALOGO_ID )
  	WHERE (
  	i.STATUS = 'A'	OR
  	(i.QTD - i.ENVIADO) >0)	and
  	i.TIPO = 5  AND
  	i.PEDIDOS_INTERNO_ID = {$p} and i.CANCELADO = 'N' and i.CANCELADO_ENTRADA = 'N'
  	ORDER BY cb.item";

		$reqp = mysql_query($sqleqp);
		while($row = mysql_fetch_array($reqp)){
			foreach($row AS $key => $value) {
				$row[$key] = stripslashes($value);
			}

            $background = $i % 2 != 0 ? 'bgcolor="#a9a9a9"' : '';
			//$trocar = "<img align='right' sol='{$row['sol']}' tipo='eqp' class='troca-item' src='../utils/exchange_16x16.png' title='Trocar' border='0'>";
			$pedidos = ($row['QTD'] - $row['ENVIADO']) ;
			$qtd=$row['QTD']-$row['ENVIADO'];
			$dados = "autorizado='{$row['autorizado']}' n='{$row['item']}' qtd='{$qtd}' sol='{$row['sol']}' id_pedido='{$row['PEDIDOS_INTERNO_ID']}' id_item='{$row['ID']}' sn='{$tipo_sol_eqp}' tiss='{$row['NUMERO_TISS']}' catalogo_id='{$row['CATALOGO_ID']}' soldata='{$row['data']}' env='{$row['enviados']}' total='{$row['QTD']}' tipo='5' tipo_sol_equipamento='{$row['TIPO_SOL_EQUIPAMENTO']}'";
			$cancela = "<img align='right' id_item='{$row['ID']}' class='cancela-item-pedido' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
			if($row["EMERGENCIA"] == 1){
				$cor = "style='color:red;'";
			}else{
				$cor ='';
			}
			echo "<tr $cor ><td><b>Equipamento: </b>{$tipo_sol_eqp}{$row['item']}  {$cancela} {$trocar}</td>";
			echo "<td align='center'>{$row['ENVIADO']}/{$row['QTD']}
<input type='hidden' class='excluir-dados' $dados /></td><td class='dados' $dados qtd={$qtd} >
  	  		  		<input type='text' name='qtd-envio' class='numerico quantidade ' style='text-align:right' size='4' value='' autocomplete='off'/>
  	  		  		</td><td><input type='text' name='lote'  style='text-align:right' size='8' autocomplete='off'/></td></tr>";
            $i++;
		}
		///jeferson 2012-10-24 fim

		echo "</table><br/>";

		echo "<button id='salvar-envio-pedido' id_pedido='{$p}' emp='{$emp}' style='display:inline;float:left;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
  	  		  	<span class='ui-button-text'>Salvar</span></button>";


		echo "<form action='imprimir_pedido_ur.php?p={$p}&tipo={$_GET['tipo']}' method='POST' target='_blank'>";
		echo "<button id='imprimir-saida' style='display:inline;float:left;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
  	<span class='ui-button-text'>Imprimir</span></button>";
		echo "</form>";
		echo "</p>";

		echo "</div>";

	}
	public function tipoMedicamentoPlano($idPaciente)
	{
		$sqlTpoMedicamento = "SELECT
planosdesaude.nome,
planosdesaude.tipo_medicamento
FROM
clientes
INNER JOIN planosdesaude ON clientes.convenio = planosdesaude.id
WHERE
clientes.idClientes = $idPaciente ";
		$result=  mysql_query($sqlTpoMedicamento);
		$tipoMedicamento = mysql_result($result,0,1);
		$nomePlano = mysql_result($result,0,0);
		$tipoMedicamentoPlano = ["tipoMedicamento" => $tipoMedicamento, "nomePlano" => $nomePlano];
		return $tipoMedicamentoPlano;
	}


	public function saida_paciente($p, $tipo, $carater, $presc, $primeira=''){
        $i = 0;
		if ($tipo == 1) {
			$sqlVisualizado = "UPDATE solicitacoes SET visualizado_logistica = 1 WHERE paciente = {$p}";
			$rsVisualizado = mysql_query($sqlVisualizado);
		}

		$p = anti_injection($p,'numerico');
		switch($carater){
			case "t":
				$compCarater = "";
				break;
			case "p":
				$compCarater = " AND s.TIPO_SOLICITACAO IN (2, 5, 9) ";
				break;
			case "o":
				$compCarater = " AND s.TIPO_SOLICITACAO NOT IN (2, 5, 9) ";
				break;
		}
		if($tipo==1){
			$opcao = 's.paciente';
			$row = mysql_fetch_array(mysql_query("SELECT c.nome,
 														CONCAT(c.END_INTERNADO,', ',c.BAI_INTERNADO, IF(c.REF_ENDERECO_INTERNADO != '' OR c.REF_ENDERECO_INTERNADO != '-', CONCAT(', ', c.REF_ENDERECO_INTERNADO), ''),', ',cidades.NOME, '-', cidades.UF) as endereco,
 														c.cuidador,
 														 c.TEL_CUIDADOR,
 														 c.empresa
 														 FROM
 														 clientes as c LEFT JOIN cidades
 														 on
 														 (c.CIDADE_ID_INT = cidades.id)
 														 WHERE
 														 c.idClientes = '$p'"));


			echo "<div id='cancelar-item' title='Cancelar item' sol='' >";
			echo alerta();
			echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar o cancelamento, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
			echo "<p><b>Justificativa:</b><input type='text' name='justificativa' class='OBG' size='100' /></div>";

			echo "<div id='ajustar-quantidade' title='Ajustar quantidade do item.' sol='' >";

			echo info("Informe a quantidade que você deseja abater do item, justifique a mudança.");
			echo "	<p>
						<b>Quantidade que deve ser subtraída entre 1 e <span id='span-quantidade-maxima'></span>:</b>
						<input type='number' max='' min='1' id ='quantidade-subtrair' name='quantidade' class='OBG' size='100' />
					</p>
					<p>
						<b>Justificativa:</b>
						<br>
						<textarea type='text' row='3' width='100%' id='justificativa-ajuste' name='justificativa' class='OBG'  ></textarea>
					</p>
					</div>";
			$tipoMedicamentoPlano = $this->tipoMedicamentoPlano($p);
			//$this->formularioTrocaMedicamento($tipoMedicamentoPlano);
			$this->formularioAdicionarMedicamentoSaida($tipoMedicamentoPlano);



			//$this->formularioTrocaDietaMaterial();
			$this->formularioAdicionarDietaMaterialSaida();

			$envio = "<input type='text' name='qtd-envio' class='numerico quantidade' style='text-align:right' size='4' autocomplete='off'/>";
			$lote = "<input type='text' name='lote' style='text-align:right' size='8' class='lote ' autocomplete='off'/>";
			echo "<center><h1>Sa&iacute;da para o paciente:<h1>";
			$paciente = ucwords(strtolower(String::removeAcentosViaEReg($row['nome'])));
			echo "<h3>$paciente</h3> {$tipoMedicamentoPlano['nomePlano']}
<div class='ui-state-highlight' style='margin-top: 5px;'>
						<strong>{$row['endereco']}</strong>
					</div></center>";
			echo "<div class='ui-state-highlight' style='margin-top: 5px;'>
						<center><strong>Obs: Só é permitido o ajuste de itens que não sejam de entrega imediata e falte enviar mais de um.</strong></center>
					</div>";


			echo "<div id='dialog-relatorio' title='Relat&oacute;rio' ><img src='../utils/logo.jpg' width=30% style='align:center' /><div id='pre-relatorio'></div>
  		  <form action='print.php' method='post' name='form_relatorio' id='print-relatorio'>
          <input type='hidden' name='relatorio' value='' id='relatorio' />
          <input type='submit' value='Imprimir' />
          </form></div>";
			echo "<div id='div-envio-paciente' empresa-paciente='{$row['empresa']}' p='$p' np='$paciente' endereco='{$row['endereco']}' cuidador='{$row['cuidador']}' tel='{$row['TEL_CUIDADOR']}'>";
			echo alerta();

			echo "<p><b>Data:</b><input type='text' class='data OBG' id='data-envio' name='data_envio' >";
			echo "<p><label for='excluir-total' style='display:inline;'><input type='checkbox' style='display:inline;' id='excluir-total' /><b>Cancelar todo o Pedido.</b></label></p>";
			echo "<p><span id='span-justificatica-excluir-total' style='display:none'><b>Justificativa:</b>
            </br>
            <div id ='div-justificativa-excluir-total'>
            <textarea rowls='30' cols='30' id='text-justificatica-excluir-total' style='display:none'></textarea></span></p>
             </div>
             <div id='recolher-equipamento-solicitado' title='Selecione o(s) equipamento(s) para dar devolução' id-paciente='' id-item='' id-solicitacao=''></div>";
			echo "<table class='mytable' width=100% id='lista-enviados' >
              <thead><tr><th>Itens Solicitados</th>
                          <th align='center' width='1%'>enviado/pedido</th>
  			  <th width='10%'>Enviar</th>
  			  <th width='1%'>Fornecedor</th>
  			  <th width='1%'>Lote</th>
                         </tr></thead>";

			$sqlmed = "SELECT
									s.TIPO_SOLICITACAO,
									s.enfermeiro,
									(CASE s.status when 2 THEN s.qtd ELSE s.autorizado END) as autorizado,
									(CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia ,
										s.enviado,
									s.NUMERO_TISS,
									s.CATALOGO_ID,
									s.idSolicitacoes as sol,
									s.status,
									DATE_FORMAT(s.DATA_SOLICITACAO,'%d/%m/%Y') as data,
									b.principio, b.apresentacao
										,(CASE  when s.status= 1 and s.idPrescricao=-1 THEN '1' ELSE '0' END) as avulsa,
									DATE_FORMAT(s.DATA_MAX_ENTREGA,'%d/%m/%Y %H:%m:%s') as data_entrega,
												(CASE s.idPrescricao WHEN -1 THEN
													s.data
													ELSE
												 DATE_FORMAT(p.inicio,'%d/%m/%Y ') END) AS inicio,
													(CASE s.idPrescricao WHEN -1 THEN
													s.data
													ELSE
													DATE_FORMAT(p.fim,'%d/%m/%Y ')
													END) AS fim,
													b.lab_desc,
													e.principio AS principioAtivo,
												 IF(
													s.idPrescricao = - 1	AND s.id_periodica_complemento IS NULL,
														DATE_FORMAT(s.DATA_SOLICITACAO,'%Y/%m '),
															IF(
																	s.idPrescricao <> - 1,
																	DATE_FORMAT(p.inicio, '%Y/%m '),
																	IF(
																		s.idPrescricao = - 1
																		AND s.id_periodica_complemento IS NOT NULL,
																		DATE_FORMAT(p2.inicio, '%Y/%m '),
																		DATE_FORMAT(s.DATA_SOLICITACAO,'%Y/%m ')
																		)
																)
													)AS dataBd,
												 	DATE_FORMAT(p2.inicio,'%d/%m/%Y ') as complementoInicio,
												 	DATE_FORMAT(p2.fim,'%d/%m/%Y ') as complementoFim,
												 	s.id_periodica_complemento,
												 	COALESCE(p.flag, '') as flag

										FROM
												 solicitacoes as s
												 INNER JOIN catalogo as b on (b.ID = s.CATALOGO_ID)
												 LEFT JOIN catalogo_principio_ativo AS d ON d.catalogo_id = b.ID
												 LEFT JOIN principio_ativo AS e ON e.id = d.principio_ativo_id
												 INNER JOIN clientes as c on (s.paciente = c.idClientes)
												 LEFT JOIN prescricoes as p on (s.idPrescricao = p.id)
												 LEFT JOIN kitsmaterial as k on (s.KIT_ID = idKit)
												 LEFT JOIN prescricoes as p2 on (s.id_periodica_complemento = p2.id)
										WHERE {$opcao} = '$p'

									AND s.tipo = '0'

									AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END)
									AND
									s.qtd <> 0 AND (s.status = '1' OR s.status = '2')
									{$compCarater}
									AND s.id_prescricao_curativo = 0

									UNION

									SELECT
									s.TIPO_SOLICITACAO,
									s.enfermeiro,
									(CASE s.status when 2 THEN s.qtd ELSE s.autorizado END) as autorizado,
									(CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia ,
										s.enviado,
									s.NUMERO_TISS,
									s.CATALOGO_ID,
									s.idSolicitacoes as sol,
									s.status,
									DATE_FORMAT(s.DATA_SOLICITACAO,'%d/%m/%Y') as data,
									b.principio, b.apresentacao
										,(CASE  when s.status= 1 and s.idPrescricao=-1 THEN '1' ELSE '0' END) as avulsa,
									DATE_FORMAT(p.data_max_entrega,'%d/%m/%Y') as data_entrega,
									DATE_FORMAT(p.inicio,'%d/%m/%Y') AS inicio,
									DATE_FORMAT(p.fim,'%d/%m/%Y') AS fim,
													b.lab_desc,
													e.principio AS principioAtivo,
													(CASE s.idPrescricao WHEN -1 THEN
													DATE_FORMAT(s.DATA_SOLICITACAO ,'%Y/%m ')
													ELSE
												 	DATE_FORMAT(p.inicio,'%Y/%m ') END) as dataBd,
												 	0 as complementoInicio,
												 	0 as complementoFim,
												 	s.id_periodica_complemento,
												 	'' as flag
										FROM
												 solicitacoes as s 
												 INNER JOIN catalogo as b on (b.ID = s.CATALOGO_ID)
												 LEFT JOIN catalogo_principio_ativo AS d ON d.catalogo_id = b.ID
												 LEFT JOIN principio_ativo AS e ON e.id = d.principio_ativo_id
												 INNER JOIN clientes as c on (s.paciente = c.idClientes) 
												 LEFT JOIN prescricao_curativo as p on (s.id_prescricao_curativo = p.id) 
												 LEFT JOIN kitsmaterial as k on (s.KIT_ID = idKit)
										WHERE {$opcao} = '$p'

									AND s.tipo = '0'
									AND s.qtd <> 0

									AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END)
									AND (s.status = '1' OR s.status = '2')
									AND s.idPrescricao = 0
									AND s.TIPO_SOLICITACAO != 10
									{$compCarater}
									ORDER BY
									dataBd,
									principio,
									apresentacao,
									enviado";


			$rmed = mysql_query($sqlmed) or die ($sqlmed . ' - ' . mysql_error());
			$dataAuxiliar = 0;
			while($row = mysql_fetch_array($rmed)){
				foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
				$qtd=$row['autorizado']-$row['enviado'];
				$qtdMaxima = $qtd -1;
				$dados = "autorizado='{$row['autorizado']}' sol='{$row['sol']}' qtd='{$qtd}' cod='{$row['NUMERO_TISS']}' catalogo_id='{$row['CATALOGO_ID']}' n='{$row['principio']}' sn='{$row['apresentacao']}' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['qtd']}' tipo='0'";
				// $cancela = "<img align='right' sol='{$row['sol']}' class='cancela-item' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";

				$emergencia=0;
				$emergencia_label='';
				$data_entrega ='';
				$cor='';
                $background =  '';
				$complementoPeriodico = '';

				if(!empty($row['id_periodica_complemento']) && $row['id_periodica_complemento'] != -1 ){
					$complementoPeriodico = "Complemento prescrição períodica de {$row['complementoInicio']} - {$row['complementoFim']}";
				}


				if($row['status'] == '2') {
					$emergencia = '2';
					$emergencia_label = "<b><i>Entrega Imediata {$row['data_entrega']}</i></b>";
				}
				if ($row["emergencia"] == 1) {
					$cor = "style='color:red'";
					$data_entrega = "";
				} else if ($row["avulsa"] == 1) {
					$cor = "style='color:blue'";
					if ($row['data_entrega'] != '00/00/0000 00:00:00') {
						$data_entrega = "&nbsp;<span style='color:#000;'><b> Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";
					} else {
						$data_entrega = "";
					}
				} else if ($row["TIPO_SOLICITACAO"] == 2 || $row["TIPO_SOLICITACAO"] == 5 || $row["TIPO_SOLICITACAO"] == 9) {
					$cor = "style='color:#006400'";
					$data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b>";
				} else if ($row["TIPO_SOLICITACAO"] == 3 || $row["TIPO_SOLICITACAO"] == 6 || $row["TIPO_SOLICITACAO"] == 8) {
					$cor = "style='color:purple'";
					$data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b><span style='color:#000;'><b> Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";
				}

				if(empty($row['kit'])){
					$kit = " ";

				}else{
					$kit = "<b>(kit-{$row['kit']})</b> ";
				}
					//$trocar = "<img align='right' sol='{$row['sol']}' catalogo_id='{$row['CATALOGO_ID']}' tipo='med' class='troca-item-med' src='../utils/exchange_16x16.png' title='Trocar' border='0'>";

				if($dataAuxiliar != $row['dataBd'] ){
					$dataAuxiliar = $row['dataBd'];
					echo "<tr class='backgrounSienna' ><td colspan='5'><b><center>Medicamentos para ".implode('/',array_reverse(explode('/',$dataAuxiliar)))."</center></b></td></tr>";
				}
				//  <img src='/utils/fraction_64x64.png' width='16' sol='{$row['sol']}' catalogo_id='{$row['CATALOGO_ID']}' tipo='med'class='fracionar'  style='cursor: pointer;' title='Fracionar'>
				// <img src='/utils/add_24x24.png' width='16' class='adicionar-medicamento-saida' sol='{$row['sol']}' catalogo_id='{$row['CATALOGO_ID']}' tipo='med' style='cursor: pointer;' title='Adicionar'>

				//  -------------------------------------------
				$labelFlag = '';
				if(!empty($row['flag'])) {
					$labelFlag = "<div class='ui-state-highlight' style='margin-top: 5px;'>
                    <center><b>{$row['flag']}</center>
            </div>";
				}
				echo "<tr {$background} {$cor}>
                        <td>

                        <b>MEDICAMENTO: </b><span style='color:#000;'>{$kit}</span>{$row['principio']} {$row['apresentacao']}
                        (<b>LAB: </b>{$row['lab_desc']}) {$data_entrega} {$emergencia_label}
                        <br>
                        <u style='margin-left: 19px;'><b>PRINCÍPIO ATIVO: </b> {$row['principioAtivo']} </u>
                        <br>
                        $labelFlag

                        <p style='color:#000;'><b>{$complementoPeriodico}</b></p>
                        </td>";
				echo "<td align='center'>{$row['enviado']}/<span id='quantidade-autorizada-sol-".$row['sol']."'>{$row['autorizado']}</span></td>
						<td  $dados >";
							if($qtd > 1 && $row['emergencia'] != 1){
							echo "<button
										style='display:inline;float:left;'
										class='ui-button ui-widget backgrounSienna ui-corner-all ui-button-text-only ajustar-quantidade-item'
										 role='button'
										 aria-disabled='false'
										 id='btn-ajustar-sol-".$row['sol']."'
										 sol='{$row['sol']}'
										 quantidade  = '{$qtdMaxima}'>
										<span class='ui-button-text'><b>Ajustar </b></span>
  									</button>";
							}


						echo "</td>
						<td>
							<button id='' style='display:inline;float:left;background-color: #FF0000'
								class='ui-button ui-widget  ui-corner-all ui-button-text-only cancela-item'
								 role='button' aria-disabled='false'
								 sol='{$row['sol']}'  >
  								<span class='ui-button-text'><b>Cancelar</b></span>
  							</button>
						</td>
						<td>
							<button  style='display:inline;float:left;background-color: #8ebf45'
									 sol='{$row['sol']}' catalogo_id='{$row['CATALOGO_ID']}' tipo='med'
									 qtd='{$qtd}' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['qtd']}' autorizado='{$row['autorizado']}'
									class=' adicionar-medicamento-saida ui-button  ui-widget ui-corner-all ui-button-text-only '
									 id='adicionar-solicitacao-".$row['sol']."'
									 role='button' aria-disabled='false'>
									<span class='ui-button-text'><b>Adicionar</b></span>
								</button>

							</td>
						</tr>
						<tbody id='solicitacao_{$row['sol']}'> </tbody>";
			    $i++;
			}


			$trocar = '';
			$sqldie = <<<SQL
SELECT
	s.enfermeiro,
	s.TIPO_SOLICITACAO,
	(
		CASE s. STATUS
		WHEN 2 THEN
			s.qtd
		ELSE
			s.autorizado
		END
	)AS autorizado,
	(
		CASE s. STATUS
		WHEN 2 THEN
			'1'
		ELSE
			'0'
		END
	)AS emergencia,
	s.enviado,
	s.NUMERO_TISS,
	s.CATALOGO_ID,
	s.idSolicitacoes AS sol,
	s. STATUS,
	DATE_FORMAT(s.DATA_SOLICITACAO, '%d/%m/%Y')AS data,
	b.principio,
	b.apresentacao,
	(
		CASE
		WHEN s. STATUS = 1
		AND s.idPrescricao =- 1 THEN
			'1'
		ELSE
			'0'
		END
	)AS avulsa,
	DATE_FORMAT(
		s.DATA_MAX_ENTREGA,
		'%d/%m/%Y %H:%i:%s'
	)AS data_entrega,
	(
		CASE s.idPrescricao
		WHEN - 1 THEN
			s. DATA
		ELSE
			DATE_FORMAT(p.inicio, '%d/%m/%Y ')
		END
	)AS inicio,
	(
		CASE s.idPrescricao
		WHEN - 1 THEN
			s.DATA_SOLICITACAO
		ELSE
			DATE_FORMAT(p.fim, '%d/%m/%Y ')
		END
	)AS fim,
	b.lab_desc,

 IF(
s.idPrescricao = - 1	AND s.id_periodica_complemento IS NULL,
	DATE_FORMAT(s.DATA_SOLICITACAO,'%Y/%m '),
		IF(
				s.idPrescricao <> - 1,
				DATE_FORMAT(p.inicio, '%Y/%m '),
				IF(
					s.idPrescricao = - 1
					AND s.id_periodica_complemento IS NOT NULL,
					DATE_FORMAT(p2.inicio, '%Y/%m '),
					DATE_FORMAT(s.DATA_SOLICITACAO,'%Y/%m ')
					)
			)
)AS dataBd,
DATE_FORMAT(p2.inicio,'%d/%m/%Y ') as complementoInicio,
DATE_FORMAT(p2.fim,'%d/%m/%Y ') as complementoFim,
s.id_periodica_complemento,
COALESCE(p.flag, '') as flag
FROM
	solicitacoes AS s
INNER JOIN catalogo AS b ON(b.ID = s.CATALOGO_ID)
INNER JOIN clientes AS c ON(s.paciente = c.idClientes)
LEFT JOIN prescricoes AS p ON(s.idPrescricao = p.id)
LEFT JOIN kitsmaterial AS k ON(s.KIT_ID = idKit)
LEFT JOIN prescricoes as p2 on (s.id_periodica_complemento = p2.id)
WHERE
	{$opcao} = '$p'
AND s.tipo = '3'
AND(
	CASE s. STATUS
	WHEN 2 THEN
		s.qtd > s.enviado
	ELSE
		s.autorizado > s.enviado
	END
)
AND
s.qtd <> 0 AND
(s. STATUS = '1' OR s. STATUS = '2')
{$compCarater}
ORDER BY
dataBd,
	principio,
	apresentacao,
	enviado
SQL;


			$rdie = mysql_query($sqldie) or die ($sqldie . ' - ' . mysql_error());
			$dataAuxiliar = 0;
			while($row = mysql_fetch_array($rdie)){
				foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
				$qtd=$row['autorizado']-$row['enviado'];
				$qtdMaxima = $qtd -1;
				$dados = "sol='{$row['sol']}' qtd='{$qtd}' cod='{$row['NUMERO_TISS']}' catalogo_id='{$row['CATALOGO_ID']}' n='{$row['principio']}' sn='{$row['apresentacao']}' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['qtd']}' autorizado='{$row['autorizado']}' tipo='3'";
				$cancela = "<img align='right' sol='{$row['sol']}' class='cancela-item' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
				$complementoPeriodico = '';
				if(!empty($row['id_periodica_complemento']) && $row['id_periodica_complemento'] != -1){
					$complementoPeriodico = "Complemento prescrição períodica de {$row['complementoInicio']} - {$row['complementoFim']}";
				}
				$emergencia=0;
				$emergencia_label='';
				$data_entrega ='';
				$cor='';
				$background =  '';

				if($row['status'] == '2') {
					$emergencia = '2';
					$emergencia_label = "<b><i>Entrega Imediata {$row['data_entrega']}</i></b>";
				}
				if ($row["emergencia"] == 1) {
					$cor = "style='color:red'";
					$data_entrega = "";
				} else if ($row["avulsa"] == 1) {
					$cor = "style='color:blue'";
					if ($row['data_entrega'] != '00/00/0000 00:00:00') {
						$data_entrega = "&nbsp;<span style='color:#000;'><b>Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";
					} else {
						$data_entrega = "";
					}
				} else if ($row["TIPO_SOLICITACAO"] == 2 || $row["TIPO_SOLICITACAO"] == 5) {
					$cor = "style='color:#006400'";
					$data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b>";
				} else if ($row["TIPO_SOLICITACAO"] == 3 || $row["TIPO_SOLICITACAO"] == 6) {
					$cor = "style='color:purple'";
					$data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b> <span style='color:#000;'><b> Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";
				}

				if(empty($row['kit'])){
					$kit = " ";

				}else{
					$kit = "<b>(kit-{$row['kit']})</b> ";
				}
					//$trocar = "<img align='right' sol='{$row['sol']}' catalogo_id='{$row['CATALOGO_ID']}' tipo='die' class='troca-item-mat-die' src='../utils/exchange_16x16.png' title='Trocar' border='0'>";



				if($dataAuxiliar != $row['dataBd'] ){
					$dataAuxiliar = $row['dataBd'];
					//var_dump(array_reverse(explode('/',$dataAuxiliar)));

					echo "<tr class='backgrounSienna' ><td colspan='5'><b><center>Dietas para ".implode('/',array_reverse(explode('/',$dataAuxiliar)))."</center></b></td></tr>";
				}
				// <img src='/utils/fraction_64x64.png' width='16' class='fracionar'  style='cursor: pointer;' title='Fracionar'>
				$labelFlag = '';
				if(!empty($row['flag'])) {
					$labelFlag = "<div class='ui-state-highlight' style='margin-top: 5px;'>
                    <center><b>{$row['flag']}</center>
            </div>";
				}
				echo "<tr {$background} {$cor} >
						<td>
						 <b>DIETA: </b>
						 <span style='color:#000;'>{$kit}</span>
						 {$row['principio']} {$row['apresentacao']}
						 <b>LAB: </b>{$row['lab_desc']} {$data_entrega} {$emergencia_label}
						 <br>
						 {$labelFlag}
						 <p style='color:#000;'><b>$complementoPeriodico</b></p></td>";
				echo "<td align='center'>{$row['enviado']}/<span id='quantidade-autorizada-sol-".$row['sol']."'>{$row['autorizado']}</span></td>
						<td>";
						if($qtd > 1 && $row['emergencia'] != 1){
							echo "	<button
							 			style='display:inline;float:left;'
										class='ui-button ui-widget backgrounSienna ui-corner-all ui-button-text-only ajustar-quantidade-item'
										role='button'
										aria-disabled='false'
								 		id='btn-ajustar-sol-".$row['sol']."'
								 		sol='{$row['sol']}'
								 		quantidade  = '{$qtdMaxima}'>
  								<span class='ui-button-text'><b>Ajustar</b></span>
  							</button>";
							}
						echo "</td>
						<td>
							<button id='' style='display:inline;float:left;background-color: #FF0000'
								class='ui-button ui-widget  ui-corner-all ui-button-text-only cancela-item'
								 role='button' aria-disabled='false'
								 sol='{$row['sol']}' >
  								<span class='ui-button-text'><b>Cancelar</b></span>
  							</button>
						</td>
						<td>
							<button  style='display:inline;float:left;background-color: #8ebf45'
									 sol='{$row['sol']}' catalogo_id='{$row['CATALOGO_ID']}'
									  tipo='die' soldata='{$row['data']}' env='{$row['enviado']}' qtd='{$qtd}'
									   total='{$row['qtd']}' autorizado='{$row['autorizado']}'
									   nome-item = '".$row['principio']." ".$row['apresentacao']." LAB: ".$row['lab_desc']."'
									   tiss = '{$row['NUMERO_TISS']}'
									    id='adicionar-solicitacao-".$row['sol']."'
									class=' adicionar-material-dieta-saida ui-button  ui-widget ui-corner-all ui-button-text-only '
									 role='button' aria-disabled='false'>
									<span class='ui-button-text'><b>Adicionar</b></span>
								</button>

							</td>
						<tbody id='solicitacao_{$row['sol']}'> </tbody>";
			    $i++;
			}

			$sqlmat = "SELECT
	s.enfermeiro,
	s.TIPO_SOLICITACAO,  
	(CASE s.status when 2 THEN s.qtd ELSE s.autorizado END) as autorizado,
	(CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia, 
	s.enviado, 
	s.NUMERO_TISS, 
	s.CATALOGO_ID,
	s.idSolicitacoes as sol, 
	s.status, 
	DATE_FORMAT(s.DATA_SOLICITACAO,'%d/%m/%Y') as data,
	b.principio, 
	b.apresentacao ,
	(CASE  when s.status= 1 and s.idPrescricao =-1 THEN '1' ELSE '0' END) as avulsa,
	DATE_FORMAT(s.DATA_MAX_ENTREGA,'%d/%m/%Y %H:%i:%s') as data_entrega,
	(CASE s.idPrescricao WHEN -1 THEN
				  s.data
				  ELSE
				 DATE_FORMAT(p.inicio,'%d/%m/%Y ') END) AS inicio,
				  (CASE s.idPrescricao WHEN -1 THEN
				  s.data
				  ELSE
				  DATE_FORMAT(p.fim,'%d/%m/%Y ')
				  END) AS fim,
         k.nome as kit,
	b.lab_desc,
 IF(
s.idPrescricao = - 1	AND s.id_periodica_complemento IS NULL,
	DATE_FORMAT(s.DATA_SOLICITACAO,'%Y/%m '),
		IF(
				s.idPrescricao <> - 1,
				DATE_FORMAT(p.inicio, '%Y/%m '),
				IF(
					s.idPrescricao = - 1
					AND s.id_periodica_complemento IS NOT NULL,
					DATE_FORMAT(p2.inicio, '%Y/%m '),
					DATE_FORMAT(s.DATA_SOLICITACAO,'%Y/%m ')
					)
			)
)AS dataBd,
DATE_FORMAT(p2.inicio,'%d/%m/%Y ') as complementoInicio,
DATE_FORMAT(p2.fim,'%d/%m/%Y ') as complementoFim,
s.id_periodica_complemento,
COALESCE(p.flag, '') as flag
  	FROM 
        solicitacoes as s inner join
         catalogo as b on (b.ID = s.CATALOGO_ID) inner join  
         clientes as c on (s.paciente = c.idClientes) left join
          prescricoes as p on (s.idPrescricao = p.id) LEFT JOIN
          kitsmaterial as k on (s.KIT_ID = idKit)
          LEFT JOIN prescricoes as p2 on (s.id_periodica_complemento = p2.id)
  	WHERE {$opcao} = '$p' 
	AND s.tipo = '1'          
	AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) 
	AND s.qtd <> 0 AND
(s.status = '1' OR s.status = '2')
	  AND s.id_prescricao_curativo = 0
	  {$compCarater}

		UNION

		SELECT
	s.enfermeiro,
	s.TIPO_SOLICITACAO,
	(CASE s.status when 2 THEN s.qtd ELSE s.autorizado END) as autorizado,
	(CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia,
	s.enviado,
	s.NUMERO_TISS,
	s.CATALOGO_ID,
	s.idSolicitacoes as sol,
	s.status,
	DATE_FORMAT(s.DATA_SOLICITACAO,'%d/%m/%Y') as data,
	b.principio,
	b.apresentacao ,
	(CASE  when s.status= 1 and s.idPrescricao =-1 THEN '1' ELSE '0' END) as avulsa,
	DATE_FORMAT(p.data_max_entrega,'%d/%m/%Y') as data_entrega,
	DATE_FORMAT(p.inicio,'%d/%m/%Y') AS inicio,
	DATE_FORMAT(p.fim,'%d/%m/%Y') AS fim,
         k.nome as kit,
	b.lab_desc,
	(CASE s.idPrescricao WHEN -1 THEN
	DATE_FORMAT(s.DATA_SOLICITACAO,'%Y/%m ')
	ELSE
	DATE_FORMAT(p.inicio,'%Y/%m ') END) as dataBd,
	0 as complementoInicio,
	0 as complementoFim,
	s.id_periodica_complemento,
	'' as flag
  	FROM
        solicitacoes as s inner join
         catalogo as b on (b.ID = s.CATALOGO_ID) inner join
         clientes as c on (s.paciente = c.idClientes) left join
          prescricao_curativo as p on (s.id_prescricao_curativo = p.id) LEFT JOIN
          kitsmaterial as k on (s.KIT_ID = idKit)
  	WHERE {$opcao} = '$p'
	AND s.tipo = '1'
	AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END)
	AND s.qtd <> 0 AND
(s.status = '1' OR s.status = '2')
		AND s.idPrescricao = 0
		AND s.TIPO_SOLICITACAO != 10
		{$compCarater}

	ORDER BY dataBd, principio, apresentacao, enviado";

			$dataAuxiliar = 0;

			$rmat = mysql_query($sqlmat) or die ($sqlmat . ' - ' . mysql_error());
			while($row = mysql_fetch_array($rmat)){
				$trocar = '';
				foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
				$qtd=$row['autorizado']-$row['enviado'];
				$qtdMaxima = $qtd -1;
				$dados = "sol='{$row['sol']}'  qtd='{$qtd}' cod='{$row['NUMERO_TISS']}' catalogo_id='{$row['CATALOGO_ID']}' n='{$row['principio']}' sn='{$row['apresentacao']}' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['qtd']}' tipo='1'";
				// $cancela = "<img align='right' sol='{$row['sol']}' class='cancela-item' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";

				$emergencia=0;
				$emergencia_label='';
				$data_entrega ='';
				$cor='';
                $background ='' ;
				$complementoPeriodico = '';
				if(!empty($row['id_periodica_complemento']) && $row['id_periodica_complemento'] != -1){
					$complementoPeriodico = "Complemento prescrição períodica de {$row['complementoInicio']} - {$row['complementoFim']}";
				}
				if($row['status'] == '2') {
					$emergencia = '2';
					$emergencia_label = "<b><i>Entrega Imediata {$row['data_entrega']}</i></b>";
				}
				if ($row["emergencia"] == 1) {
					$cor = "style='color:red'";
					$data_entrega = "";
				} else if ($row["avulsa"] == 1) {
					$cor = "style='color:blue'";
					if ($row['data_entrega'] != '00/00/0000 00:00:00') {
						$data_entrega = "&nbsp;<span style='color:#000;'><b> Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";
					} else {
						$data_entrega = "";
					}
				} else if ($row["TIPO_SOLICITACAO"] == 2 || $row["TIPO_SOLICITACAO"] == 5 || $row["TIPO_SOLICITACAO"] == 9) {
					$cor = "style='color:#006400'";
					$data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b>";
				} else if ($row["TIPO_SOLICITACAO"] == 3 || $row["TIPO_SOLICITACAO"] == 6 || $row["TIPO_SOLICITACAO"] == 8) {
					$cor = "style='color:purple'";
					$data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b><span style='color:#000;'><b> Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";
				}

				if(empty($row['kit'])){
					$kit = " ";

				}else{
					$kit = "<b>(kit-{$row['kit']})</b> ";
				}
					// $trocar = "<img align='right' sol='{$row['sol']}' catalogo_id='{$row['CATALOGO_ID']}' tipo='mat' class='troca-item-mat-die' src='../utils/exchange_16x16.png' title='Trocar' border='0'>";

				if($dataAuxiliar != $row['dataBd'] ){
					$dataAuxiliar = $row['dataBd'];
					echo "<tr class='backgrounSienna'><th colspan='5'><b><center>Materiais para ".implode('/',array_reverse(explode('/',$dataAuxiliar)))."</center></b></th></tr>";
				}
				$labelFlag = '';
				if(!empty($row['flag'])) {
					$labelFlag = "<div class='ui-state-highlight' style='margin-top: 5px;'>
                    <center><b>{$row['flag']}</center>
            </div>";
				}

				echo "<tr {$background} {$cor} ><td><b>MATERIAL: </b><span style='color:#000;'>{$kit}</span>{$row['principio']} {$row['apresentacao']}
							<b>LAB: </b>{$row['lab_desc']} {$data_entrega} {$emergencia_label}
							<br>
							{$labelFlag}
							<p style='color:#000;'><b>{$complementoPeriodico}</b></p></td>";
				echo "<td align='center'>{$row['enviado']}/<span id='quantidade-autorizada-sol-".$row['sol']."'>{$row['autorizado']}</span></td>
						<td>";
						if($qtd > 1 && $row['emergencia'] != 1){
							echo "	<button
										style='display:inline;float:left;'
										class='ui-button ui-widget backgrounSienna ui-corner-all ui-button-text-only ajustar-quantidade-item'
										role='button'
										aria-disabled='false'
										id='btn-ajustar-sol-".$row['sol']."'
										sol='{$row['sol']}'
										quantidade  = '{$qtdMaxima}'>
  											<span class='ui-button-text'><b>Ajustar</b></span>
  									</button>";
							}
						echo "</td>
						<td>
							<button id='' style='display:inline;float:left;background-color: #FF0000'
								class='ui-button ui-widget  ui-corner-all ui-button-text-only cancela-item'
								 role='button' aria-disabled='false'
								 sol='{$row['sol']}'  >
  								<span class='ui-button-text'><b>Cancelar</b></span>
  							</button>
						</td>
						<td>
							<button  style='display:inline;float:left;background-color: #8ebf45'
									 sol='{$row['sol']}' catalogo_id='{$row['CATALOGO_ID']}'
									 tipo='mat' qtd='{$qtd}' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['qtd']}'
									 autorizado='{$row['autorizado']}'
									  nome-item = '".$row['principio']." ".$row['apresentacao']." LAB: ".$row['lab_desc']."'
									   tiss = '{$row['NUMERO_TISS']}'
									   id='adicionar-solicitacao-".$row['sol']."'
									class=' adicionar-material-dieta-saida ui-button  ui-widget ui-corner-all ui-button-text-only '
									 role='button' aria-disabled='false'>
									<span class='ui-button-text'><b>Adicionar</b></span>
								</button>

							</td>
						<tbody id='solicitacao_{$row['sol']}'></tbody>";
			    $i++;
			}
			$trocar='';
			$sqlformula = "SELECT s.enfermeiro,s.TIPO_SOLICITACAO, (CASE s.status when 2 THEN s.qtd ELSE s.autorizado END) as autorizado, s.enviado, s.cod, (CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia ,s.idSolicitacoes as sol, s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data, f.formula
  	,(CASE  when s.status= 1 and s.idPrescricao=-1 THEN '1' ELSE '0' END) as avulsa,
        DATE_FORMAT(s.DATA_MAX_ENTREGA,'%d/%m/%Y %H:%i:%s') as data_entrega,
        (CASE s.idPrescricao WHEN -1 THEN
				  s.data				  
				  ELSE
				 DATE_FORMAT(p.inicio,'%d/%m/%Y ') END) AS inicio,
				  (CASE s.idPrescricao WHEN -1 THEN
				  s.data
				  ELSE
				  DATE_FORMAT(p.fim,'%d/%m/%Y ')
				  END) AS fim  	
FROM solicitacoes as s, formulas as f, clientes as c,prescricoes as p 
  			   WHERE {$opcao} = '$p' AND s.paciente = c.idClientes AND s.tipo = '12'  AND s.idPrescricao = p.id  AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND (s.status = '1' OR s.status = '2') 
  			   AND f.id = s.cod
  			   {$compCarater}
  			   ORDER BY formula,enviado";
			$rformula = mysql_query($sqlformula);
			while($row = mysql_fetch_array($rformula)){
				foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
				$dados = "sol='{$row['sol']}' cod='{$row['NUMERO_TISS']}' catalogo_id='{$row['CATALOGO_ID']}' n='{$row['formula']}' sn='' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['qtd']}' tipo='12'";
				$cancela = "<img align='right' sol='{$row['sol']}' class='cancela-item' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";

				$emergencia=0;
				$emergencia_label='';
				$data_entrega ='';
				$cor='';
                $background = $i % 2 != 0 ? 'bgcolor="#a9a9a9"' : '';

				if($row['status'] == '2') {
					$emergencia = '2';
					$emergencia_label = "<b><i>emerg&ecirc;ncia</i></b>";
				}
				if ($row["emergencia"] == 1) {
					$cor = "style='color:red'";
					$data_entrega = "";
				} else if ($row["avulsa"] == 1) {
					$cor = "style='color:blue'";
					if ($row['data_entrega'] != '00/00/0000 00:00:00') {
						$data_entrega = "&nbsp;<span style='color:#000;'><b>Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";
					} else {
						$data_entrega = "";
					}
				} else if ($row["TIPO_SOLICITACAO"] == 2 || $row["TIPO_SOLICITACAO"] == 5) {
					$cor = "style='color:#006400'";
					$data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b>";
				} else if ($row["TIPO_SOLICITACAO"] == 3 || $row["TIPO_SOLICITACAO"] == 6) {
					$cor = "style='color:purple'";
					$data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b> <span style='color:#000;'><b>Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";
				}
				echo "<tr {$background} {$cor} ><td>{$row['formula']} {$data_entrega} {$emergencia_label} {$cancela}</td>";
				echo "<td align='center'>{$row['enviado']}/{$row['autorizado']}</td><td class='dados' $dados >$envio</td><td>$lote</td></tr>";
			    $i++;
			}
			///jeferson 2012-10-24
			$sqleqp = "SELECT
	s.enfermeiro,
	s.TIPO_SOLICITACAO,
	(CASE s.status when 2 THEN s.qtd ELSE s.autorizado END) as autorizado,
	(CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia ,
	s.enviado,
	s.NUMERO_TISS, 
	s.CATALOGO_ID,
	s.idSolicitacoes as sol, 
  	s.status, 
	DATE_FORMAT(s.DATA_SOLICITACAO,'%d/%m/%Y') as data,
	cb.item,
	s.TIPO_SOL_EQUIPAMENTO ,
	(CASE  when s.status= 1 and s.idPrescricao =-1 THEN '1' ELSE '0' END) as avulsa,
	DATE_FORMAT(s.DATA_MAX_ENTREGA,'%d/%m/%Y %H:%i:%s') as data_entrega,
        (CASE s.idPrescricao WHEN -1 THEN
				  s.data				  
				  ELSE
				 DATE_FORMAT(p.inicio,'%d/%m/%Y ') END) AS inicio,
				  (CASE s.idPrescricao WHEN -1 THEN
				  s.DATA_SOLICITACAO
				  ELSE
				  DATE_FORMAT(p.fim,'%d/%m/%Y ')
				  END) AS fim,
        s.JUSTIFICATIVA_AVULSO,
	(CASE s.idPrescricao WHEN -1 THEN
	DATE_FORMAT(s.DATA_SOLICITACAO ,'%Y/%m ')
	ELSE
	DATE_FORMAT(p.inicio,'%Y/%m ') END) as dataBd,
	COALESCE(p.flag, '') as flag
  	FROM 
         solicitacoes as s inner join
         cobrancaplanos as cb on (cb.id = s.CATALOGO_ID) inner join  
         clientes as c on (s.paciente = c.idClientes) left join
          prescricoes as p on (s.idPrescricao = p.id) LEFT JOIN
          kitsmaterial as k on (s.KIT_ID = idKit)
	
  	WHERE {$opcao} = '$p' 
	
        AND s.qtd <> 0 AND
s.tipo = '5'
  	AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) 
	AND (s.status = '1' OR s.status = '2')
	{$compCarater}
  	  ORDER BY dataBd,TIPO_SOL_EQUIPAMENTO,item,enviado";
			$reqp = mysql_query($sqleqp);
			$dataAuxiliar=0;
			while($row = mysql_fetch_array($reqp)){
				$envioEquipamento = "<input type='text' name='qtd-envio' class='quantidade envio-equipamento-unico' style='text-align:right' size='4' autocomplete='off'/>";
				$lote = "<input type='text' name='lote' style='text-align:right' size='8' class='lote ' autocomplete='off'/>";
				foreach($row AS $key => $value) {
					$row[$key] = stripslashes($value);
				}

				if($row['TIPO_SOL_EQUIPAMENTO'] == 1){
					$tipo_sol_eqp ="Enviar ";
					$select = $this->fornecedoresEquipamento();
					//$trocar = "<img align='right' sol='{$row['sol']}' tipo='eqp' class='troca-item' src='../utils/exchange_16x16.png' title='Trocar' border='0'>";


				}
				if($row['TIPO_SOL_EQUIPAMENTO'] == 2){
					$tipo_sol_eqp ="Recolher ";
          $envioEquipamento= "<button  style='display:inline;float:left;' catalogo_id='{$row['CATALOGO_ID']}' sol='{$row['sol']}'
                                   paciente='{$p}'
                                     class='ui-button ui-widget ui-state-default ui-corner-all
                                            ui-button-text-only ui-state-hover recolher-equipamento-solicitado'
                                     type='submit' role='button' aria-disabled='false'>
  		                               <span class='ui-button-text'>Recolher</span>
                            </button>";
          $lote='';
					//$trocar="";

				}
				if($row['TIPO_SOL_EQUIPAMENTO'] == 3){
					$tipo_sol_eqp ="Substituir ";
					$select = $this->fornecedoresEquipamento();

				}
				$qtd=$row['autorizado']-$row['enviado'];
				$dados = "sol='{$row['sol']}' qtd='{$qtd}' cod='{$row['NUMERO_TISS']}' catalogo_id='{$row['CATALOGO_ID']}' n='{$row['item']}' sn='{$tipo_sol_eqp}' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['qtd']}' tipo='5' tipo_sol_equipamento='{$row['TIPO_SOL_EQUIPAMENTO']}'";
				$cancela = "<img align='right' sol='{$row['sol']}' class='cancela-item' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";


				$emergencia=0;
				$emergencia_label='';
				$data_entrega ='';
				$cor='';
                $background = $i % 2 != 0 ? 'bgcolor="#a9a9a9"' : '';

				if($row['status'] == '2') {
					$emergencia = '2';
					$emergencia_label = "<b><i>Entrega Imediata {$row['data_entrega']}</i></b>";
				}
				if ($row["emergencia"] == 1) {
					$cor = "style='color:red'";
					$data_entrega = "";
				} else if ($row["avulsa"] == 1) {
					$cor = "style='color:blue'";
					if ($row['data_entrega'] != '00/00/0000 00:00:00') {
						$data_entrega = "&nbsp;<span style='color:#000;'><b>Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";
					} else {
						$data_entrega = "";
					}
				} else if ($row["TIPO_SOLICITACAO"] == 2 || $row["TIPO_SOLICITACAO"] == 5) {
					$cor = "style='color:#006400'";
					$data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b>";
				} else if ($row["TIPO_SOLICITACAO"] == 3 || $row["TIPO_SOLICITACAO"] == 6) {
					$cor = "style='color:purple'";
					$data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b> <span style='color:#000;'><b>Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";
				}

				if(empty($row['kit'])){
					$kit = " ";

				}else{
					$kit = "<b>(kit-{$row['kit']})</b> ";
				}
				if($dataAuxiliar != $row['dataBd'] ){
					$dataAuxiliar = $row['dataBd'];
					echo "<tr class='backgrounSienna'><th colspan='5'><b><center>Equipamentos para ".implode('/',array_reverse(explode('/',$dataAuxiliar)))."</center></b></th></tr>";
				}
				$labelFlag = '';
				if(!empty($row['flag'])) {
					$labelFlag = "<div class='ui-state-highlight' style='margin-top: 5px;'>
                    <center><b>{$row['flag']}</center>
            </div>";
				}
				echo "<tr {$background} {$cor}><td><b>Equipamento: </b><span style='color:#000;'>{$kit}</span>{$tipo_sol_eqp}{$row['item']}
 {$data_entrega} {$emergencia_label} <br>
 {$labelFlag}
 <br>
 <b>OBS: </b>{$row['JUSTIFICATIVA_AVULSO']} {$cancela} {$trocar}</td>";
				echo "<td align='center'>{$row['enviado']}/{$row['autorizado']}</td><td class='dados' $dados >$envioEquipamento</td><td>$select<td>$lote</td></tr>";
			    $i++;
			}
			///jeferson 2012-10-24 fim

			echo "</table><br/>";

			echo "<button id='salvar-envio' primeira='{$primeira}' carater='{$carater}' style='display:inline;float:left;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
  		<span class='ui-button-text'>Enviar</span></button>";

		}else{

			$opcao = 'c.empresa';
			$row = mysql_fetch_array(mysql_query("SELECT e.nome FROM empresas as e WHERE e.id = '$p'"));


			echo "<div id='cancelar-item' title='Cancelar item' sol='' >";
			echo alerta();
			echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar o cancelamento, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
			echo "<p><b>Justificativa:</b><input type='text' name='justificativa' class='OBG' size='100' /></div>";
			//$this->formulario();

			$envio = "<input type='text' name='qtd-envio' class='numerico' style='text-align:right' size='4' />";

			echo "<center><h1>Pedido:<h1>";
			$paciente = ucwords(strtolower($row['nome']));
			echo "<h3>$paciente</h3></center>";
			echo "<center style='color: #8B0000; font-size: 12px;'><b>Os equipamentos a serem recolhidos não estão contemplados no Condensado! <br> Isso pode causar distorção entre a soma das quantidades por paciente e o condensado!</b></center>";
			echo "<div id='dialog-relatorio' title='Relat&oacute;rio' ><img src='../utils/logo.jpg' width=30% style='align:center' /><div id='pre-relatorio'></div>
  		  <form action='print.php' method='post' name='form_relatorio' id='print-relatorio'>
          <input type='hidden' name='relatorio' value='' id='relatorio' />
          <input type='submit' value='Imprimir' />
          </form></div>";
			echo "<div id='div-envio-paciente' p='$p' np='$paciente' >";
			echo alerta();
			$data = date("Ymd");
			echo "<input type ='hidden' id='data_atual' value = '{$data}'/>";
			///jeferson 14-06-2013
			if($p != 1){
				echo "<p><b>Data maxima para entrega:</b><input type='text' class='data OBG' id='data-maxima' name='data-maxima' /></p>";
				echo "<p><b>Observação:</b><br><span width=90% ><textarea name='obs' id='obs' rows='2' cols='100'/></textarea></span></p>";
			}
			///jeferson 14-06-2013
			echo "<table class='mytable' width=100% id='lista-enviados' ><thead><tr><th>Itens Solicitados</th><th align='center' width='1%'>enviado/pedido</th>
  			<th width='1%'>Pedido</th><th><span>Emerg&ecirc;ncia </span><span style='float:left;display:table;'><input type='checkbox' id='emergencia'/></span></th></tr></thead>";

			$sqlmed = "SELECT
	s.enfermeiro,
	(CASE WHEN COUNT(*)>1 THEN SUM(s.enviado) ELSE s.enviado END) AS enviados,
    sum((CASE s.status when 2 THEN  s.qtd ELSE  s.autorizado  END)) as autorizado,
        s.enviado, 
		s.NUMERO_TISS,
        s.CATALOGO_ID, 
        s.idSolicitacoes as sol, 
        s.status, 
        DATE_FORMAT(s.DATA_SOLICITACAO,'%d/%m/%Y') as data, 
        b.principio, 
        b.apresentacao,
        b.lab_desc,
        e.principio AS principioAtivo
FROM 
	solicitacoes as s 
	INNER JOIN catalogo as b ON b.ID = s.CATALOGO_ID
	LEFT JOIN catalogo_principio_ativo AS d ON d.catalogo_id = b.ID
	INNER JOIN principio_ativo AS e ON e.id = d.principio_ativo_id
	INNER JOIN clientes as c ON s.paciente = c.idClientes
WHERE 
	{$opcao} = '$p' AND 
	s.tipo = '0' AND 
	(CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND 
        (s.status = '1' OR s.status = '2') 
		{$compCarater}
GROUP BY 
	CATALOGO_ID ORDER BY principio, apresentacao,enviado";

			$rmed = mysql_query($sqlmed);
			while($row = mysql_fetch_array($rmed)){
				foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
                $background = $i % 2 != 0 ? 'bgcolor="#a9a9a9"' : '';
				$dados = "autorizado='{$row['autorizado']}' sol='{$row['sol']}' cod='{$row['NUMERO_TISS']}' catalogo_id='{$row['CATALOGO_ID']}' n='{$row['principio']}' sn='{$row['apresentacao']}' soldata='{$row['data']}' env='{$row['enviados']}' total='{$row['qtd']}' tipo='0'";
				echo "<tr {$background}>
                        <td>
                        <img src='/utils/fraction_64x64.png' width='16' class='fracionar' style='cursor: pointer;' title='Fracionar'> 
                        <b>MEDICAMENTO: </b>{$row['principio']} {$row['apresentacao']} (LAB: {$row['lab_desc']}) {$cancela} {$trocar}
                        <br>
                        <u style='margin-left: 19px;'><b>PRINCÍPIO ATIVO: </b> {$row['principioAtivo']} </u>
                        </td>";
				echo "<td align='center'>{$row['enviados']}/{$row['autorizado']}</td><td class='dados' $dados >
  		  					<input type='text' name='qtd-envio' class='numerico' style='text-align:right' size='4' value=''/>
  		  					</td><td><input type='checkbox' class='emergencia'  value='0'/></td></tr>";
			    $i++;
			}

			$sqldie="SELECT
	s.enfermeiro,
	(CASE WHEN COUNT(*)>1 THEN SUM(s.enviado) ELSE s.enviado END) AS enviados,
    sum((CASE s.status when 2 THEN  s.qtd ELSE  s.autorizado  END)) as autorizado,
        s.enviado, 
		s.NUMERO_TISS,
        s.CATALOGO_ID, 
        s.idSolicitacoes as sol, 
        s.status, 
        DATE_FORMAT(s.DATA_SOLICITACAO,'%d/%m/%Y') as data, 
        b.principio, 
        b.apresentacao
FROM 
	solicitacoes as s, 
	catalogo as b, 
	clientes as c
WHERE 
	{$opcao} = '$p' AND 
	s.paciente = c.idClientes AND 
	s.tipo = '3' AND 
	(CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND 
        (s.status = '1' OR s.status = '2') 
		AND b.ID = s.CATALOGO_ID
		{$compCarater}
GROUP BY 
	CATALOGO_ID ORDER BY principio, apresentacao,enviado";


			$rdie = mysql_query($sqldie);
			while($row = mysql_fetch_array($rdie)){
				foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
                $background = $i % 2 != 0 ? 'bgcolor="#a9a9a9"' : '';
				$dados = "autorizado='{$row['autorizado']}' sol='{$row['sol']}' cod='{$row['NUMERO_TISS']}' catalogo_id='{$row['CATALOGO_ID']}' n='{$row['principio']}' sn='{$row['apresentacao']}' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['qtd']}' autorizado='{$row['autorizado']}' tipo='3'";

				echo "<tr {$background}><td><img src='/utils/fraction_64x64.png' width='16' class='fracionar'  style='cursor: pointer;' title='Fracionar'> <b>DIETA: </b>{$row['principio']} {$row['apresentacao']} {$cancela} {$trocar}</td>";
				echo "<td align='center'>{$row['enviados']}/{$row['autorizado']}</td><td class='dados' $dados >
  				<input type='text' name='qtd-envio' class='numerico' style='text-align:right' size='4' value=''/></td>
  				<td><input type='checkbox' class='emergencia' value='0' /></td></tr>";
                $i++;
			}

			$sqlmat="SELECT
	s.enfermeiro,
	(CASE WHEN COUNT(*)>1 THEN SUM(s.enviado) ELSE s.enviado END) AS enviados,
    sum((CASE s.status when 2 THEN  s.qtd ELSE  s.autorizado  END)) as autorizado,
        s.enviado, 
		s.NUMERO_TISS,
        s.CATALOGO_ID, 
        s.idSolicitacoes as sol, 
        s.status, 
        DATE_FORMAT(s.DATA_SOLICITACAO,'%d/%m/%Y') as data, 
        b.principio, 
        b.apresentacao
FROM 
	solicitacoes as s, 
	catalogo as b, 
	clientes as c
WHERE 
	{$opcao} = '$p' AND 
	s.paciente = c.idClientes AND 
	s.tipo = '1' AND 
	(CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND 
        (s.status = '1' OR s.status = '2') 
		AND b.ID = s.CATALOGO_ID
		{$compCarater}
GROUP BY 
	CATALOGO_ID ORDER BY principio, apresentacao,enviado";

			$rmat = mysql_query($sqlmat);
			while($row = mysql_fetch_array($rmat)){
				foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
                $background = $i % 2 != 0 ? 'bgcolor="#a9a9a9"' : '';
				$dados = "autorizado='{$row['autorizado']}' sol='{$row['sol']}' cod='{$row['NUMERO_TISS']}' catalogo_id='{$row['CATALOGO_ID']}' n='{$row['principio']}' sn='{$row['apresentacao']}' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['qtd']}' tipo='1'";
				echo "<tr {$background}><td><img src='/utils/fraction_64x64.png' width='16' class='fracionar'  style='cursor: pointer;' title='Fracionar'> <b>MATERIAL: </b>{$row['principio']} {$row['apresentacao']} {$cancela} {$trocar}</td>";
				echo "<td align='center'>{$row['enviados']}/{$row['autorizado']}</td><td class='dados' $dados >
  		  		<input type='text' name='qtd-envio' class='numerico' style='text-align:right' size='4' value=''/></td>
  		  		<td><input type='checkbox' class='emergencia' value='0' /></td></tr>";
                $i++;
			}
			$sqlformula = "SELECT s.enfermeiro, (CASE WHEN COUNT(*)>1 THEN SUM(s.enviado) ELSE s.enviado END) AS enviados,
  		sum((CASE s.status when 2 THEN  s.qtd ELSE  s.autorizado  END)) as autorizado, 
  		 s.enviado, s.cod, s.idSolicitacoes as sol,
  		  s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data,
  		  f.formula
  		  	FROM solicitacoes as s,
  		  	 formulas as f,
  		  	 clientes as c
  		  	WHERE {$opcao} = '$p' AND s.paciente = c.idClientes AND s.tipo = '12' AND 
  		  	(CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END)
  		  	 AND (s.status = '1' OR s.status = '2')
  		  	AND f.id = s.cod GROUP BY cod
  		  	{$compCarater}
  		  	ORDER BY formula,enviado";
			$rformula = mysql_query($sqlformula);
			while($row = mysql_fetch_array($rformula)){
				foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
				$dados = "autorizado='{$row['autorizado']}' sol='{$row['sol']}' cod='{$row['NUMERO_TISS']}' catalogo_id='{$row['CATALOGO_ID']}' n='{$row['formula']}' sn='' soldata='{$row['data']}' env='{$row['enviados']}' total='{$row['qtd']}' tipo='12'";
				$pedidos = ($row['autorizado'] - $row['enviados']) -$dif;

				echo "<tr><td>{$row['formula']} {$cancela}</td>";
				echo "<td align='center'>{$row['enviados']}/{$row['autorizado']}</td><td class='dados' $dados >
	  		<input type='text' name='qtd-envio' class='numerico' style='text-align:right' size='4' value=''/></td>
	  		<td><input type='checkbox' class='emergencia' value='0' /></td></tr>";
			}
			///jeferson 2012-10-24
			$sqleqp = "SELECT s.enfermeiro,
		(CASE WHEN COUNT(*)>1 THEN SUM(s.enviado) ELSE s.enviado END) AS enviados,
		sum((CASE s.status when 2 THEN  s.qtd ELSE  s.autorizado  END)) as autorizado,
		s.enviado,
		s.NUMERO_TISS,
		s.CATALOGO_ID,
		s.idSolicitacoes as sol,
  		s.status, DATE_FORMAT(s.DATA_SOLICITACAO,'%d/%m/%Y') as data, cb.item,
		s.TIPO_SOL_EQUIPAMENTO
  		FROM
		solicitacoes as s, 
		cobrancaplanos as cb,
		clientes as c
  		WHERE {$opcao} = '$p' AND s.paciente = c.idClientes AND s.tipo = '5'
  		AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND (s.status = '1' OR s.status = '2')
  		AND cb.id = s.CATALOGO_ID
  		{$compCarater}
		GROUP BY CATALOGO_ID 
		ORDER BY TIPO_SOL_EQUIPAMENTO,item,enviado";

			$reqp = mysql_query($sqleqp);
			while($row = mysql_fetch_array($reqp)){
				foreach($row AS $key => $value) {
					$row[$key] = stripslashes($value);
				}

				if($row['TIPO_SOL_EQUIPAMENTO'] == 1){
					$tipo_sol_eqp ="Enviar ";
					$dados = "autorizado='{$row['autorizado']}' sol='{$row['sol']}' cod='{$row['NUMERO_TISS']}' catalogo_id='{$row['CATALOGO_ID']}' n='{$row['item']}' sn='{$tipo_sol_eqp}' soldata='{$row['data']}' env='{$row['enviados']}' total='{$row['qtd']}' tipo='5' tipo_sol_equipamento='{$row['TIPO_SOL_EQUIPAMENTO']}'";

					echo "<tr><td><b>Equipamento: </b>{$tipo_sol_eqp}{$row['item']}  {$cancela} {$trocar}</td>";
					echo "<td align='center'>{$row['enviados']}/{$row['autorizado']}</td><td class='dados' $dados >
		  		<input type='text' name='qtd-envio' class='numerico' style='text-align:right' size='4' value=''/>
		  		</td> <td><input type='checkbox' class='emergencia' value='0' /></td></tr>";

				}
			}
			///jeferson 2012-10-24 fim

			echo "</table><br/>";

			///jeferson 14-06-2013
			if($p != 1){
				echo "<button id='salvar-pedido' style='display:inline;float:left;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
  		<span class='ui-button-text'>Enviar Pedidos</span></button>";
			}
		}

		echo "<form action='imprimir_separacao2.php?p={$p}&tipo={$_GET['tipo']}&presc={$presc}&carater={$carater}' method='POST' target='_blank'>";
		echo "<button style='display:inline;float:left;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
  	<span class='ui-button-text'>Imprimir</span></button>";
		echo "</form>";
		echo "</p>";

		echo "</div>";


	}

	private function fornecedoresEquipamento(){
		$result = mysql_query("SELECT * FROM fornecedores where responsavel_equipamento = 'SIM' ORDER BY razaoSocial");

		$var .= "<select name='fornecedor-equipamento' style='font-size:6px' class='fornecedores ' style='background-color:transparent;' >";
		$var .= "<option value='-1'>Selecione</option>";
		while($row = mysql_fetch_array($result))

			$var .= "<option value='". $row['idFornecedores'] ."'>". $row['razaoSocial'] ."</option>";
		$var .= "</select>";
		return $var;
	}

}

?>