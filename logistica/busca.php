<?php
$id = session_id();
if(empty($id))
    session_start();
require('../db/config.php');
include_once('../utils/codigos.php');

use App\Helpers\StringHelper as String;

class Buscas{
    private function pacientes($selected,$name){
        if($_SESSION['empresa_principal'] == 1){
            $cond_paciente = '1 = 1';
        }else{
            $cond_paciente =" c.empresa = {$_SESSION['empresa_principal']} ";
        }
        $sql = "SELECT c.idClientes, c.codigo, c.nome AS nomeCliente, e.nome AS ur FROM clientes AS c INNER JOIN empresas AS e ON (c.empresa = e.id) WHERE {$cond_paciente} ORDER BY c.nome;";
        $result = mysql_query($sql);
        $var = "<p><select name='{$name}' class='pacientes' style='background-color:transparent;' >";
        $var .= "<option value='T'>Todos</option>";
        $pacientes = [];
        while($row = mysql_fetch_array($result))
        {
            $pacientes[$row['ur']][$row['idClientes']] = '(' . $row['codigo'] . ') ' . strtoupper(String::removeAcentosViaEReg($row['nomeCliente']));
        }
        foreach ($pacientes as $ur => $paciente) {
            $var .= "<optgroup label='{$ur}'>";
            foreach ($paciente as $id => $nome) {
                if(($selected <> NULL) && ($selected == $id)){
                    $var .= "<option value='{$id}' selected>{$nome}</option>";
                }  else {
                    $var .= "<option value='{$id}'>{$nome}</option>";
                }
            }
            $var .= "</optgroup>";
        }
        $var .= "</select>";
        return $var;
    }

    private function fornecedores($id,$name){
        $result = mysql_query("SELECT * FROM fornecedores ORDER BY razaoSocial");
        $var = "<p>";
        $var .= "<select name='{$name}' class='fornecedores' style='background-color:transparent;' >";
        while($row = mysql_fetch_array($result))
            $var .= "<option value='". $row['idFornecedores'] ."'>". $row['razaoSocial'] ."</option>";
        $var .= "</select>";
        return $var;
    }

    private function controlados(){
        $result = mysql_query("SELECT ID, concat(principio,' ',apresentacao,' LAB:',lab_desc ) as nome,ID
    FROM catalogo where tipo = 0 and ATIVO = 'A' and CONTROLADO = 'S' ORDER BY nome");
        $var = "<p>";
        $var .= "<select name='select-controlado' style='background-color:transparent;' id='select-controlado'>";
        while($row = mysql_fetch_array($result))
            $var .= "<option value='".$row['ID']."' >". $row['nome'] ."</option>";
        $var .= "</select>";
        return $var;
    }
    private function antibioticos(){
        $result = mysql_query("SELECT ID, concat(principio,' ',apresentacao,' LAB:',lab_desc) as nome,ID
    FROM catalogo where tipo = 0 and ATIVO = 'A' and ANTIBIOTICO = 'S' ORDER BY nome");
        $var = "<p>";
        $var .= "<select name='select-antibiotico' style='background-color:transparent;' id='select-antibiotico'>";
        while($row = mysql_fetch_array($result))
            $var .= "<option value='".$row['ID']."' >". $row['nome'] ."</option>";
        $var .= "</select>";
        return $var;
    }

    public function buscarSaidas($origem = null){
        $html .= (($origem == 1) ? "<form name=\"form\" action='?op=buscar&act=devolucao' method='POST'>" : "<form name=\"form\" action='?op=buscar&act=saida' method='POST'>") ;//se origem for 1, ele enviará o formulario para devolução
        $html .= "<input type='hidden' name='origem' id='origem' value='{$origem}' />";

        $html .= "<fieldset style='border: 1px solid; font-size: 14px;' >";
        $html .= "<legend><b>Tipo</b></legend>";

        $html .= "<table style='margin:5 5 25px;'>";
        $html .= "<tbody>";
        $html .= "<tr>
          <td><input type='radio' name='tipo-busca-saida' value='paciente' CHECKED /></td><td>Paciente</td>
          <td><input type='radio' name='tipo-busca-saida' value='med' /></td><td>Medicamento</td>
          <td><input type='radio' name='tipo-busca-saida' value='mat' /></td><td>Material</td>
          <td><input type='radio' name='tipo-busca-saida' value='die' /></td><td>Dieta</td>
          <td><input type='radio' name='tipo-busca-saida' value='eqp' /></td><td>Equipamento</td>";
        if($origem != 1 && $_SESSION['empresa_principal'] == 1){
            $html .="<td><input type='radio' name='tipo-busca-saida' value='ur' /></td><td>Unidade Regional</td>";
        }
        $html .="<br><br>
                </tr></tbody></table>
          <tbody><table>
          <td><div id='combo-busca-saida'>".$this->pacientes(NULL,"codigo")."</div>";
        $html .= "</tr>";
        $html .= "</tbody></table>";
        $html .= "</fieldset>";
        if($origem != 1){
            $html .= "<fieldset style='border: 1px solid; font-size: 14px;' >";
            $html .= "<legend><b>Resultado</b></legend>";
            $html .= "<table style='margin:5 5 25px;'>";
            $html .= "<tbody>";
            $html .= "<tr>";
            $html .= "<td><p><input type='radio' name='resultado' value='a' checked /></td><td>Analítico</td>
      <td><input type='radio' name='resultado' value='c' /></td><td>Condensado</td>";
            $html .= "</tbody></table>";
            $html .= "</fieldset>";
        }

        $html .= "<fieldset style='border: 1px solid; font-size: 14px;' >";
        $html .= "<legend><b>Per&iacute;odo</b></legend>";
        $html .= "<table style='margin:5 5 25px;'>";
        $html .= "<tbody>";
        $html .= "<tr>";
        $hoje = new \DateTime;
        $html .= "<td><b>In&iacute;cio</b></td><td><input id='inicio-devolucao' type='date'  name='inicio' value='{$hoje->format('Y-m-d')}' maxlength='10' size='10' /></td>";
        $html .= "<td><b>Fim</b></td><td><input id='fim-devolucao' type='date' name='fim' value='{$hoje->format('Y-m-d')}' maxlength='10' size='10' /></td></tr></table>";
        $html .= "</tbody></table>";
        $html .= "</fieldset><br>";


        $html .= "<input type='submit' value='Buscar' />";
        $html .= "</form>";
        echo $html;
    }

    public function form(){
        echo "<center><h1> Pesquisar Movimenta&ccedil;&otilde;es</h1></center>";
        echo "<fieldset style='border: 1px solid; font-size: 14px;' >";
        echo "<legend><b><h1>Nota</h1></b></legend>";

        //echo "<h1>Nota</h1>";
        echo "<form name=\"form\" action='?op=buscar&act=nota' method='POST'>";
        echo"<div>";
        echo "<p><span style='float:left;display:table;'><b>N&deg; TR: </b></span><span style='float:left;display:table;'><input type='text' name='numero' size='40'/></span>";
        echo "<span style='float:left;display:table;'>&nbsp;&nbsp;<b>Fornecedor: </b></span>";
        echo $this->fornecedores(NULL,"codigo");
        echo"</div>";
        echo "<input type='submit' value='Buscar' />";
        echo "</form>";
        echo "</fieldset><br>";

        // echo "<h1>Itens</h1>";
        // echo "<form name=\"form\" action='?op=buscar&act=item' method='POST'>";

        //echo "<b>Tipo:</b><input type='radio' name='tipo' value='0' CHECKED />Medicamento<BR /><input type='radio' name='tipo' value='1' />Material";
        //echo "<p><b>Item</b><input type='text' id='nome-busca-item' name='nome' size='50'/>";
        //echo "<input type='submit' value='Buscar' />";
        //echo "</form>";

        //Buscar Saídas
        echo "<fieldset style='border: 1px solid; font-size: 14px;' >";
        echo "<legend><b><h1>Buscar sa&iacute;das</h1></b></legend>";

        echo $this->buscarSaidas();
        echo "</fieldset>";


        echo "<fieldset style='border: 1px solid; font-size: 14px;' >";
        echo "<legend><b><h1>Rastrear itens</h1></b></legend>";

        echo "<form name=\"form\" action='?op=buscar&act=rastreamento' method='POST'>";
        echo "<fieldset style='border: 1px solid; font-size: 14px;' >";
        echo "<legend><b>Tipo</b></legend>";

        echo "<table style='margin:5 5 25px;'>";
        echo "<tbody>";
        echo "<tr>";

        echo "	<td><input type='radio' name='tipo-rastreamento' value='fornecedor' CHECKED /></td><td>Fornecedor
    		<td><input type='radio' name='tipo-rastreamento' value='med' /></td><td>Medicamento</td> 
    	 	<td><input type='radio' name='tipo-rastreamento' value='mat' /></td><td>Material</td>
            <td><input type='radio' name='tipo-rastreamento' value='die' /></td><td>Dieta</td>";
        echo"</tr></tbody></table>";

        echo "<tbody>";
        echo "<tr>";
        echo "<td><div id='combo-rastreamento'>".$this->fornecedores(NULL,"codigo")."</div></td>";
        echo"</tr></tbody>";
        echo "</fieldset><br>";

        echo "<fieldset style='border: 1px solid; font-size: 14px;' >";
        echo "<legend><b>Per&iacute;odo</b></legend>";
        echo "<table style='margin:5 5 25px;'>";
        echo "<tbody>";
        echo "<tr>";

        echo "<td><b>In&iacute;cio</b> </td><td>".input('text','ientrada','ientrada',"class='data' size='10' maxlength='10' ")."</td>".
            "<td><b>Fim</b> </td><td>".input('text','fentrada','fentrada',"class='data' size='10' maxlength='10' ")."</td>";

        echo "</tbody></table>";
        echo "</fieldset><br>";

        echo "<p><input type='submit' value='Buscar' />";
        echo "</form>";
        echo "</fieldset>
            <br>";

        echo "<fieldset style='border: 1px solid; font-size: 14px;' >
            <legend><b><h1>Rastrear Controlados</h1></b></legend>";
        echo "";
        echo "<form name=\"form\" action='?op=buscar&act=controlado' method='POST'>";
        echo "<fieldset style='border: 1px solid; font-size: 14px;' >";
        echo "<legend><b>Buscar Por:</b></legend>";
        echo "<table><tr><td>Nome</td><td> <input type='radio' name='tipo-controlado' class='radio-controlado' value='1' checked='checked' style='float:left;' /></td>";
        echo" <td>Todos&nbsp; </td><td> <input type='radio' name='tipo-controlado' class='radio-controlado' value='0' /></td></table>";
        echo $this-> controlados();
        echo "</fieldset><br>";
        echo "<fieldset style='border: 1px solid; font-size: 14px;' >";
        echo "<legend><b>Per&iacute;odo</b></legend>";
        echo "<table style='margin:5 5 25px;'>";
        echo "<tbody>";
        echo "<tr>";

        echo "<td><b>In&iacute;cio</b> </td><td>".input('text','ci-entrada','ci-entrada',"class='data' size='10' maxlength='10' ")."</td>".
            "<td><b>Fim</b> </td><td>".input('text','cf-entrada','cf-entrada',"class='data' size='10' maxlength='10' ")."</td>";

        echo "</tbody></table>";
        echo "</fieldset><br>";
        echo "<p><input type='submit' value='Buscar' />";
        echo "</form>";
        echo "</fieldset><br>";

        //// Rastrear antibiótico
        echo "<fieldset style='border: 1px solid; font-size: 14px;' >";
        echo "<legend><b><h1>Rastrear Antibióticos</h1></b></legend>";
        echo "<form name=\"form\" action='?op=buscar&act=antibiotico' method='POST'>";
        echo "<fieldset style='border: 1px solid; font-size: 14px;' >";
        echo "<legend><b>Buscar Por:</b></legend>";
        echo "<table><tr><td>Nome</td><td> <input type='radio' name='tipo-antibiotico' class='radio-antibiotico' value='1' checked='checked' style='float:left;' /></td>";
        echo" <td>Todos&nbsp; </td><td> <input type='radio' name='tipo-antibiotico' class='radio-antibiotico' value='0' /></td></table>";
        echo $this-> antibioticos();
        echo "</fieldset><br>";
        echo "<fieldset style='border: 1px solid; font-size: 14px;' >";
        echo "<legend><b>Per&iacute;odo</b></legend>";
        echo "<table style='margin:5 5 25px;'>";
        echo "<tbody>";
        echo "<tr>";

        echo "<td><b>In&iacute;cio</b> </td><td>".input('text','antibiotico-inicio-entrada','antibiotico-inicio-entrada',"class='data' size='10' maxlength='10' ")."</td>".
            "<td><b>Fim</b> </td><td>".input('text','antibiotico-fim-entrada','antibiotico-fim-entrada',"class='data' size='10' maxlength='10' ")."</td>";

        echo "</tbody></table>";
        echo "</fieldset><br>";
        echo "<p><input type='submit' value='Buscar' />";
        echo "</form>";
        echo "</fieldset><br>";

        if($_SESSION['empresa_principal'] == 1){
            echo "<fieldset style='border: 1px solid; font-size: 14px;' >";
            echo "<legend><b><h1>Rastrear Dietas</h1></b></legend>";
            echo "<form name=\"form\" action='?op=buscar&act=dieta' method='POST'>";
            echo "<fieldset style='border: 1px solid; font-size: 14px;' >";
            echo "<legend><b>Per&iacute;odo</b></legend>";
            echo "<table style='margin:5 5 25px;'>";
            echo "<tbody>";
            echo "<tr>";

            echo "<td><b>In&iacute;cio</b> </td><td>".input('text','di-entrada','di-entrada',"class='data' size='10' maxlength='10' ")."</td>".
                "<td><b>Fim</b> </td><td>".input('text','df-entrada','df-entrada',"class='data' size='10' maxlength='10' ")."</td>";

            echo "</tbody></table>";
            echo "</fieldset><br>";
            echo "<p><input type='submit' value='Buscar' />";
            echo "</form>";
            echo "</fieldset><br>";
        }
        ////jeferson
    }

    public function show_result($result){
        echo "<center><h1>Resultado</h1></center>";
        echo "<table class='mytable' width=100% >";
        echo "<thead><tr>";
        echo "<th><b>Nome</b></th>";
        echo "<th><b>Segundo Nome</b></th>";
        echo "<th><b>Quantidade</b></th>";
        echo "<th><b>Valor</b></th>";
        echo "<th><b>Valor Total</b></th>";
        echo "</tr></thead>";
        $cor = false;
        $total = 0;
        $f = true;
        while($row = mysql_fetch_array($result)){
            $f = false;
            foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
            if($cor) echo "<tr>";
            else echo "<tr class='odd'>";
            $cor = !$cor;
            echo "<td valign='top'>{$row['nome']}</td>";
            echo "<td valign='top'>{$row['segundoNome']}</td>";
            echo "<td valign='top'>{$row['quantidade']}</td>";
            echo "<td valign='top'>R$ " . round($row['valor'],2) . "</td>";
            echo "<td valign='top'>R$ " . round($row['quantidade']*$row['valor'],2) . "</td>";
            echo "</tr>";
        }
        echo "</table>";
        if($f) echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
    }

    public function buscar($nome) {

        $ur = $_SESSION['empresa_principal'];
        if(isset($ur) && !empty($ur)){
            $condur = 'e.empresas_id_'.$ur;
            $qtd = 'empresas_id_'.$ur;
            $qtd_min ='QTD_MIN_'.$ur;

        }else{
            echo 'Erro falta a Unidade Regional. Entre em contato com o TI.';
            return;
        }


        $condlike = "1";
        $flag = true;
        $strings = explode(" ", $nome);
        $comparar = "concat(b.principio,' ',b.apresentacao)";
        foreach ($strings as $str) {
            if ($str != "")
                $condlike .= " AND {$comparar} LIKE '%$str%' ";
        }
        $sql = "";
        $sql = "SELECT b.principio as nome, b.apresentacao as segundoNome, {$condur}, valorMedio as valor 
    		FROM catalogo as b inner join estoque as e on (b.ID = e.CATALOGO_ID) WHERE {$condlike}";
        $result = mysql_query($sql) or trigger_error(mysql_error());
        $this->show_result($result);
    }

    public function buscar_nota($numero, $codigo) {
        echo "<center><h1>Resultado</h1></center>";
        echo "<table class='mytable' width=100% >";
        echo "<thead><tr>";
        echo "<th><b>Nome</b></th>";
        echo "<th><b>Fonecedor</b></th>";
        echo "<th><b>Data</b></th>";
        echo "<th><b>Quantidade</b></th>";
        echo "<th><b>Valor " . htmlentities("Unitário") . "</b></th>";
        echo "<th><b>" . htmlentities("Opções") . "</b></th>";
        echo "</tr></thead>";
        $cor = false;
        $sql = "";
        $sql = "SELECT
              entrada.id as id,
              entrada.qtd as quantidade,
              entrada.valor,
              entrada.data,
              entrada.CANCELADO,
              fornecedores.razaoSocial as fornecedor,
              catalogo.principio as nome,
              catalogo.apresentacao as segundoNome,
               catalogo.lab_desc as LAB
    			FROM
              entrada,
              fornecedores,
              catalogo
    			WHERE
              fornecedores.idFornecedores = entrada.fornecedor AND
              catalogo.ID = entrada.CATALOGO_ID AND
              entrada.nota = '{$numero}' AND
              fornecedores.idFornecedores = '{$codigo}'  
    			ORDER BY
              entrada.data DESC, nome, segundoNome, fornecedor, quantidade DESC, entrada.valor DESC";
        $result = mysql_query($sql) or trigger_error(mysql_error());
        $f = true;
        while ($row = mysql_fetch_array($result)) {
            $f = false;
            foreach ($row AS $key => $value) {
                $row[$key] = stripslashes($value);
            }
            if ($cor)
                echo "<tr>";
            else
                echo "<tr class='odd'>";
            $cor = !$cor;
            $data = implode("/", array_reverse(explode("-", $row['data'])));
            echo "<td valign='top'>{$row['nome']} - {$row['segundoNome']} <b>LAB:</b> {$row['LAB']}</td>";
            echo "<td valign='top'>{$row['fornecedor']}</td>";
            echo "<td valign='top'>{$data}</td>";
            echo "<td valign='top'>{$row['quantidade']}</td>";
            echo "<td valign='top'> R$ {$row['valor']}</td>";
            echo "<form action='query.php?act=cancelar_entrada' method='POST'>";
            echo "<input type='hidden' name='idnota' value='{$row['id']}' />";
            echo "<td valign='top'> <center>" . (($row['CANCELADO'] == 1) ? "CANCELADA" : "<button type='submit' class='cancelar_entrada'><img src='../utils/excluir.png' width='16' id='cancelar_entrada' title='Cancelar Entrada'></button>") . "</center></td>";
            echo "</form>";
            echo "</tr>";
        }
        echo "</table>";
        //if($f) echo "<center><b>Nenhum resultado foi encontrado!</b></center>".$sql;
        if ($f)
            echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
    }

    public static function cabecalho($tipo, $inicio, $fim, $id){
        switch ($tipo){
            case 'eqp':
                $tabela = 'cobrancaplanos';
                $campo = 'item';
                $condicao = 'id';
                $label = 'Equipamento';
                break;
            case 'paciente':
                $tabela = 'clientes';
                $campo = 'nome';
                $condicao = 'idClientes';
                $label = 'Paciente';
                break;
            default:
                $tabela = 'catalogo';
                $campo = "concat(principio,' - ',apresentacao)";
                switch ($tipo){
                    case 'med':
                        $label = 'Medicamento';
                        $condicao = 'ID';
                        $condicao2 = 'AND tipo = 0';
                        break;
                    case 'mat':
                        $label = 'Material';
                        $condicao = 'ID';
                        $condicao2 = 'AND tipo = 1';
                        break;
                    case 'die':
                        $label = 'Dieta';
                        $condicao = 'ID';
                        $condicao2 = 'AND tipo = 3';
                        break;
                }
                break;
        }
        $sqlCabecalho = sprintf("SELECT %s FROM %s WHERE %s = %s %s", $campo, $tabela, $condicao, $id, $condicao2);
        $result = mysql_query($sqlCabecalho);
        $row = mysql_fetch_array($result, MYSQL_ASSOC);
        $html = "<p>";
        $html .= sprintf("<b>Buscando por %s:</b> %s <br/>", $label, $row[$campo]);
        $html .= sprintf("<b>De</b> %s <b>a</b> %s <br/>",  \DateTime::createFromFormat('Y-m-d', $inicio)->format('d/m/Y'),  \DateTime::createFromFormat('Y-m-d', $fim)->format('d/m/Y'));
        $html .= "</p>";

        return $html;
    }

    public function buscar_devolucao($post) {
        $condicao = '';
        $tipo = $post['tipo-busca-saida'];
        extract($post, EXTR_OVERWRITE);

        $cor = false;
        $total = 0;
        $condicao = '';

        if ($_SESSION['empresa_principal'] != '1') {
            $cond_empresa = " AND clientes.empresa = {$_SESSION['empresa_principal']} ";
        } else {
            $cond_empresa = "";
        }
        if ($tipo == 'eqp') {
            $condCod = $codigo != 'T' ? "AND devolucao.CATALOGO_ID = '{$codigo}' " : "";

            $sql = "SELECT 
      clientes.nome as paciente, 
      cobrancaplanos.id as cod, 
      cobrancaplanos.item as nome,
      devolucao.LOTE as LOTE,
      sum(devolucao.QUANTIDADE) as qtd ,
      devolucao.DATA_DEVOLUCAO,
      DATE_FORMAT(devolucao.DATA_DEVOLUCAO,'%Y-%m-%d') as dt,
      COALESCE(custo_servicos_plano_ur.CUSTO, 0.00) AS custo
      FROM cobrancaplanos
      INNER JOIN devolucao on (cobrancaplanos.id = devolucao.CATALOGO_ID) 
      INNER JOIN  clientes on (clientes.idClientes = devolucao.PACIENTE_ID)
      LEFT JOIN custo_servicos_plano_ur ON cobrancaplanos.id = custo_servicos_plano_ur.COBRANCAPLANO_ID AND custo_servicos_plano_ur.UR={$_SESSION['empresa_principal']} and custo_servicos_plano_ur.STATUS = 'A'
      where devolucao.TIPO = 5 {$condCod} and
      devolucao.DATA_DEVOLUCAO   BETWEEN '{$inicio}' AND '{$fim}' and
      devolucao.PACIENTE_ID != 0 
      {$cond_empresa}
      GROUP BY
	devolucao. DATA,
	nome,
	paciente      
      ORDER BY DATA_DEVOLUCAO ASC ";

        } else if ($tipo == 'paciente') {

            $condPaciente = $codigo != 'T' ? "AND devolucao.PACIENTE_ID = '{$codigo}' " : "";

            $sql = " SELECT 
                clientes.nome as paciente, 
                devolucao.NUMERO_TISS,
                devolucao.CATALOGO_ID,
                concat(catalogo.principio,' - ',catalogo.apresentacao, ' LAB: ', catalogo.lab_desc) as nome,
                devolucao.LOTE, 
                devolucao.QUANTIDADE as qtd,
                devolucao.DATA_DEVOLUCAO,
                catalogo.valorMedio AS custo
              FROM devolucao INNER JOIN clientes on ( clientes.idClientes = devolucao.PACIENTE_ID) 
                INNER JOIN catalogo on(catalogo.ID= devolucao.CATALOGO_ID)
              WHERE
                devolucao.TIPO in (0,1,3) 
                {$condPaciente}
                $cond_empresa
                AND devolucao.DATA_DEVOLUCAO BETWEEN '$inicio' AND '$fim'

      union
      SELECT
        clientes.nome as paciente, 
        devolucao.NUMERO_TISS,
        devolucao.CATALOGO_ID,
        cobrancaplanos.item as nome, 
        devolucao.LOTE, 
        devolucao.quantidade as qtd,
        devolucao.DATA_DEVOLUCAO,
        custo_servicos_plano_ur.CUSTO AS custo
      FROM 
        devolucao INNER JOIN
        clientes on ( clientes.idClientes = devolucao.PACIENTE_ID) INNER JOIN
        cobrancaplanos on (cobrancaplanos.id = devolucao.CATALOGO_ID) 
        LEFT JOIN custo_servicos_plano_ur ON cobrancaplanos.id = custo_servicos_plano_ur.COBRANCAPLANO_ID AND custo_servicos_plano_ur.UR={$_SESSION['empresa_principal']} and custo_servicos_plano_ur.STATUS = 'A'
      WHERE
        devolucao.TIPO = 5 
        {$condPaciente}
        AND devolucao.DATA_DEVOLUCAO   BETWEEN '$inicio' AND '$fim'
        {$cond_empresa}
      ORDER BY
        `nome` ASC";
        } else {

            if ($tipo == 'med') {
                $tip = 0;
            } else if ($tipo == 'mat') {
                $tip = 1;
            } else {
                $tip = 3;
            }

            $condCod = $codigo != 'T' ? "AND devolucao.CATALOGO_ID = '{$codigo}' " : "";

            if ($_SESSION['empresa_principal'] == 1) {

                $sql = "SELECT 
        clientes.nome as paciente, 
        devolucao.NUMERO_TISS,
        devolucao.CATALOGO_ID,
        concat(catalogo.principio,' - ',catalogo.apresentacao, ' LAB: ', catalogo.lab_desc ) as nome,
        devolucao.LOTE, 
        devolucao.QUANTIDADE as qtd,
        devolucao.DATA_DEVOLUCAO,
        catalogo.valorMedio AS custo
        FROM 
        devolucao INNER JOIN 
        clientes on ( clientes.idClientes = devolucao.PACIENTE_ID) INNER JOIN 
        catalogo on(catalogo.ID = devolucao.CATALOGO_ID)
        WHERE  
        devolucao.TIPO = '{$tip}'
        {$condCod} 
        AND devolucao.DATA_DEVOLUCAO BETWEEN '$inicio' AND '$fim' 
        {$condicao}
        ORDER BY DATA_DEVOLUCAO";
            } else {
                $sql = "SELECT 
        clientes.nome as paciente, 
        devolucao.NUMERO_TISS,
        devolucao.CATALOGO_ID,
        concat(catalogo.principio,' - ',catalogo.apresentacao, ' LAB: ', catalogo.lab_desc) as nome,
        devolucao.LOTE, 
        sum(devolucao.QUANTIDADE )as qtd,
        devolucao.DATA_DEVOLUCAO,
        catalogo.valorMedio
        FROM 
        devolucao INNER JOIN 
        clientes on ( clientes.idClientes = devolucao.PACIENTE_ID) INNER JOIN 
        catalogo on(catalogo.ID = devolucao.CATALOGO_ID)
        WHERE  
        devolucao.TIPO = '{$tip}'
        {$condCod} 
        AND devolucao.DATA_DEVOLUCAO BETWEEN '$inicio' AND '$fim' 
        {$condicao}
        AND clientes.empresa = {$_SESSION['empresa_principal']}
      group by devolucao.data,devolucao.CATALOGO_ID
      order by 
      DATA_DEVOLUCAO";
            }
        }

        echo "<center><h1>" . htmlentities("DEVOLUÇÕES") . "</h1></center>";
        echo self::cabecalho($tipo, $inicio, $fim, $codigo);
        echo "<form >";
        echo "<table class='mytable' width=100% >";
        echo "<thead><tr>";
        echo "<th><b>Paciente</b></th>";
        echo "<th><b>Data</b></th>";
        echo "<th><b>Item</b></th>";
        //echo "<th><b>Segundo Nome</b></th>";
        echo "<th><b>Lote</b></th>";
        echo "<th><b>Quantidade</b></th>";
        echo "<th><b>Custo Unitário</b></th>";
        echo "<th><b>Subtotal</b></th>";

        echo "</tr></thead>";
        $result = mysql_query($sql) or trigger_error(mysql_error());
        $f = true;
        $totalCusto = 0;
        while ($row = mysql_fetch_array($result)) {
            $f = false;
            foreach ($row AS $key => $value) {
                $row[$key] = stripslashes($value);
            }
            if ($cor)
                echo "<tr>";
            else
                echo "<tr class='odd'>";
            $cor = !$cor;
            $subtotal = round($row['custo'], 2) * $row['qtd'];
            echo "<td valign='top'>{$row['paciente']}</td>";
            $data = implode("/", array_reverse(explode("-", $row['DATA_DEVOLUCAO'])));
            echo "<td valign='top'>{$data}</td>";
            echo "<td valign='top'>{$row['nome']}</td>";
            echo "<td valign='top'>{$row['LOTE']}</td>";
            echo "<td valign='top'>{$row['qtd']}</td>";
            echo "<td valign='top'>R$ " . number_format($row['custo'], 2, ',', '.') . "</td>";
            echo "<td valign='top'>R$ " . number_format($subtotal, 2, ',', '.') . "</td>";
            echo "</tr>";
            $totalCusto += $subtotal;
        }
        echo "<tr style='background-color:lightgrey'>
                <td colspan='6' style='text-align: right;b'> <b>Custo Total:</b></td>
                <td>R$ " . number_format($totalCusto, 2, ',', '.') . "</td>
              </tr>";
        echo "</table>";

        echo "</form>";

        if ($f) {
            echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
        } else {
            echo"<form action='imprimir_devolucao.php?tipo=$tipo&codigo=$codigo&inicio=$inicio&fim=$fim&todos=$todos' target='_blank' method='post'><button id='imprimir_ficha_mederi' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button' type='submit' target='_blank'><span class='ui-button-text'>Imprimir</span></button></form>";
        }
    }

    public function buscar_saida($post, $empresa_principal = 0){
        $tipo = $post['tipo-busca-saida'];
        $aSelect = ""; $aGroupBy = "";
        extract($post,EXTR_OVERWRITE);

        $i = implode("-",array_reverse(explode("/",$inicio)));
        $f = implode("-",array_reverse(explode("/",$fim)));
        $fimP = $f;

        echo "<form >";

        echo "<center><h1>BUSCAR SA&Iacute;DA</h1></center> ";
        echo "<div style='position: relative; float: right;'>
            <a target='_blank' href='/logistica/relatorios/?action=excel-pesquisar-movimentacoes&tipo-busca-saida={$post['tipo-busca-saida']}&resultado={$resultado}&codigo={$codigo}&inicio={$i}&fim={$f}'>
                <img src='/utils/new-icons/excel-2013.ico' width='36' height='36' title='Exportar para o Excel'>
            </a>
          </div>";
        echo "<table class='mytable' width=100% >";
        echo "<thead><tr>";
        echo "<th><b>Paciente</b></th>";
        if($resultado == 'a'){
            echo "<th><b>Data</b></th>";
            $aSelect = ", s.data";
            $aGroupBy = "GROUP BY s.data,  nome, paciente";
        } else {
            $aGroupBy = "GROUP BY s.CATALOGO_ID";
        }
        echo "<th><b>Item</b></th>";
        //echo "<th><b>Segundo Nome</b></th>";
        echo "<th><b>Quantidade</b></th>";
        echo "<th><b>Lote</b></th>";
        echo "<th><b>Custo Unitário</b></th>";
        echo "<th><b>Subtotal</b></th>";
        echo "</tr></thead>";
        $cor = false;
        $total = 0;
        if($tipo == 'eqp'){
            $condCod = $codigo == 'T' ? '' : "s.CATALOGO_ID ={$codigo} and";

            $sql= "SELECT 
              		c.nome as paciente, 
              		cb.id as cod, 
              		cb.item as nome,
              		s.LOTE as LOTE,
              		SUM(s.quantidade) as qtd ,
            		  s.data,
                  DATE_FORMAT(s.data,'%Y-%m-%d') as dt,
                  COALESCE(cs.CUSTO, 0.00) as valor		
  		        FROM 
                  cobrancaplanos as cb inner join 
                  saida as s on (cb.id = s.CATALOGO_ID) inner join  
                  clientes as c on (c.idClientes = s.idCliente)
                  LEFT JOIN custo_servicos_plano_ur AS cs ON cb.id = cs.COBRANCAPLANO_ID AND cs.UR={$empresa_principal} and cs.STATUS = 'A'
		          WHERE 
                  s.tipo = 5 and 
                  {$condCod}
                  s.data BETWEEN '{$i}' AND '{$f}' and 
                  s.idCliente != 0 AND 
                  c.empresa = ".$empresa_principal." 
		          {$aGroupBy}
		  UNION ALL
      		SELECT 
      		        c.nome as paciente,
              		cb.id as cod, 
              		cb.item as nome,
              		s.LOTE as LOTE,
              		SUM(s.quantidade) as qtd , 
              		s.data,
              		DATE_FORMAT(s.data,'%Y-%m-%d') as dt,
              		COALESCE(cs.CUSTO, 0.00) as valor
      		FROM 
                  cobrancaplanos as cb inner join 
                  saida  as s on (cb.id = s.CATALOGO_ID) inner join  
                  empresas as c on (c.id = s.UR) 
                  LEFT JOIN custo_servicos_plano_ur AS cs ON cb.id = cs.COBRANCAPLANO_ID AND cs.UR={$empresa_principal} and cs.STATUS = 'A'
      		WHERE 
                  s.tipo = 5 and 
                  {$condCod}
                  s.data BETWEEN '{$i}' AND '{$f}' and 
                  s.UR != 0
      		{$aGroupBy}
      		  ORDER BY data ASC ";
            //echo '<pre>'; die($sql);
        }else if($tipo == 'ur'){

            if($resultado == 'a'){

                $aGroupBy = "GROUP BY data, catalogo_id";
            }else{
                $aGroupBy = "GROUP BY  nome";
            }

            if( $codigo== 1)
            {
                $sql =
                    "SELECT 
                p.empresa,   
    		        IF(s.UR = 0 ,p.nome,e.nome) as paciente, 
                s.NUMERO_TISS,
                s.CATALOGO_ID,
                concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                s.LOTE, 
                s.quantidade as qtd,
                cb.valorMedio as valor,
                s.data
          FROM 
                saida as s Left join 
                clientes as p on ( p.idClientes = s.idCliente) inner join 
                catalogo as cb on(cb.ID = s.CATALOGO_ID) Left join
    			      empresas as e on (s.UR = e.id)
    			WHERE  
                s.tipo in (0,1,3) and
                s.data BETWEEN '$i' AND '$f' AND 
    			      (                       
                  idCliente in(
                               select 
                                  idClientes 
                               from 
                                  clientes 
                               where 
                                  empresa = '1'
                              )
    		           OR s.UR <> 0
    			      )
    		
    		UNION ALL
    		SELECT 
                 p.empresa,   
    					   IF(s.UR = 0 ,p.nome,e.nome) as paciente, 
                 s.NUMERO_TISS,
                 s.CATALOGO_ID,
                 concat(cb.item,' -  ',if(s.segundoNome is null,'',s.segundonome)) as nome,
                 s.LOTE, 
                 s.quantidade as qtd,
                 cs.CUSTO as valor,
                 s.data
        FROM 
                 saida as s Left join 
                 clientes as p on ( p.idClientes = s.idCliente) inner join 
                 cobrancaplanos as cb on(cb.ID = s.CATALOGO_ID) Left join
    		         empresas as e on (s.UR = e.id)
    		         LEFT JOIN custo_servicos_plano_ur AS cs ON cb.id = cs.COBRANCAPLANO_ID AND cs.UR={$empresa_principal} and cs.STATUS = 'A'
    		 WHERE  
                 s.tipo =5 and
                 s.data BETWEEN '$i' AND '$f' AND 
    						(                       
                 idCliente in(
                             select 
                                idClientes 
                             from 
                                clientes 
                             where 
                                empresa = '1'
                            )
    					    OR s.UR <> 0
    						)
    		 ORDER BY 
              data ";
            }else{
                $condCod = $codigo == 'T' ? '' : "AND s.UR ={$codigo} ";

                $sql ="SELECT 
                  e.nome as paciente,
                  cb.id as cod,
                  cb.id as catalogo_id,
                  cb.item as nome,
                  s.LOTE as LOTE,
                  sum(s.quantidade) as qtd ,
                  cs.CUSTO as valor,
                  s.data
              FROM 
                  cobrancaplanos as cb inner join 
                  saida as s on (cb.id = s.CATALOGO_ID) inner join 
                  empresas as e on (e.id = s.UR)
                  LEFT JOIN custo_servicos_plano_ur AS cs ON cb.id = cs.COBRANCAPLANO_ID AND cs.UR={$empresa_principal} and cs.STATUS = 'A' 
              WHERE 
                  s.tipo = 5 and
                  s.data BETWEEN '{$i}' AND '{$f}' and
                  s.idCliente = 0 
                  {$condCod}
                  {$aGroupBy}
              UNION ALL
              SELECT 
                  e.nome as paciente, 
                  cb.NUMERO_TISS as cod, 
                  cb.ID as catalogo_id,
                  concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                  s.LOTE as LOTE,
                  sum(s.quantidade )as qtd ,
                  cb.valorMedio as valor,
                  s.data 
              FROM 
                  catalogo as cb  inner join 
                  saida as s on (cb.ID = s.CATALOGO_ID) inner join 
                  empresas as e on (e.id = s.UR) 
              WHERE 
                  s.tipo in(0,1,3)  and 
                  s.data BETWEEN '{$i}'  AND '{$f}'
                  {$condCod}
                  {$aGroupBy} 
              ORDER BY 
                  data ASC ";
            }


        }else if($tipo == 'paciente'){
            if($resultado == 'a'){

                $aGroupBy = "group by data,s.CATALOGO_ID";
            }else{
                $aGroupBy = "group by  s.CATALOGO_ID";
            }
            $condPaciente = $codigo == 'T' ? '' : "s.idCliente ={$codigo} and";
            $condPacienteEmpresa = $_SESSION['empresa_principal'] == 1 ? '' : "and p.empresa = {$_SESSION['empresa_principal']}" ;

            $sql =" SELECT 
		p.nome as paciente, 
		s.NUMERO_TISS,
         s.CATALOGO_ID,
		concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
		s.LOTE, 
		s.quantidade as qtd,
		s.data,
		cb.valorMedio AS valor
		FROM saida as s inner join clientes as p on ( p.idClientes = s.idCliente) 
		inner join catalogo as cb on(cb.ID= s.CATALOGO_ID)
		WHERE $condPaciente s.tipo in (0,1,3) $condPacienteEmpresa AND s.data BETWEEN '$i' AND '$f'
		
		union ALL
		SELECT
                p.nome as paciente, 
                s.numero_tiss,
                s.CATALOGO_ID,
		cb.item as nome, 
		s.LOTE, 
		s.quantidade as qtd,
		s.data ,
		cs.CUSTO AS valor
		FROM 
		saida as s inner join clientes as p on ( p.idClientes = s.idCliente) 
		inner join cobrancaplanos as cb on (cb.id = s.CATALOGO_ID)
		LEFT JOIN custo_servicos_plano_ur AS cs ON cb.id = cs.COBRANCAPLANO_ID AND cs.UR={$empresa_principal} and cs.STATUS = 'A' 
		WHERE $condPaciente s.tipo = 5 $condPacienteEmpresa
		AND s.data BETWEEN '$i' AND '$f'  
		ORDER BY `nome` ASC";
        }
        else{

            $condCod = $codigo == 'T' ? '' : "AND s.CATALOGO_ID= {$codigo} ";

            if($tipo == 'med'){
                $tip = 0;
            }else if($tipo == 'mat'){
                $tip = 1;
            }else{
                $tip = 3;
            }

            if( $_SESSION['empresa_principal']== 1){

                $sql="SELECT 
                        p.nome as paciente, 
                        s.NUMERO_TISS,
                        s.CATALOGO_ID,
                        concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                        s.LOTE, 
                        s.quantidade as qtd,
                        s.data,
                        cb.valorMedio AS valor
                    FROM 
                        saida as s inner join 
                        clientes as p on ( p.idClientes = s.idCliente) inner join 
                        catalogo as cb on(cb.ID = s.CATALOGO_ID)
                    WHERE  
                        s.tipo ={$tip} AND 
                        s.data BETWEEN '$i' AND '$f' 
                        {$condCod}
                        AND idCliente in(
                                     select 
                                        idClientes 
                                     from 
                                        clientes 
                                     where 
                                        empresa = '{$_SESSION['empresa_principal']}'
                                    )
		
		union all
		SELECT 
                    p.nome as paciente, 
                    s.NUMERO_TISS,
                    s.CATALOGO_ID,
                    concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                    s.LOTE, 
                    s.quantidade as qtd,
                    s.data,
                    cb.valorMedio AS valor
		FROM 
                    saida as s inner join 
                    empresas as p on ( p.id = s.UR) inner join 
                    catalogo as cb on (cb.ID = s.CATALOGO_ID)
		WHERE 
                    s.tipo ={$tip} AND 
                    s.data BETWEEN '$i' AND '$f' AND 
                    s.UR <> 0 AND 
                    s.idCliente = 0 
                    {$condCod}
		
		 order by data";



            }else{
                $sql="SELECT 
                                p.nome as paciente, 
                                s.NUMERO_TISS,
                                s.CATALOGO_ID,
                                concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                                s.LOTE, 
                                sum(s.quantidade )as qtd,
                                s.data,
                                cb.valorMedio AS valor
                            FROM 
                                saida as s inner join 
                                clientes as p on ( p.idClientes = s.idCliente) inner join 
                                catalogo as cb on(cb.ID = s.CATALOGO_ID)
                            WHERE  
                                s.tipo ={$tip} AND 
                                s.data BETWEEN '$i' AND '$f'
                                {$condCod}
                                AND idCliente in(
                                                    select 
                                                        idClientes 
                                                     from 
                                                        clientes 
                                                     where 
                                                        empresa ='{$_SESSION['empresa_principal']}'
                                                 )
                             $aGroupBy  
                             order by 
                                data";

            }

        }
        //echo '<pre>'; die($sql);
        $result = mysql_query($sql) or die(mysql_error());
        $f = true;
        $totalItens = 0;
        $totalCusto = 0;
        while($row = mysql_fetch_array($result))
        {
            $f = false;
            foreach($row AS $key => $value)
            {
                $row[$key] = stripslashes($value);
            }

            if($cor)
                echo "<tr>";
            else
                echo "<tr class='odd'>";

            $cor = !$cor;
            $subtotal = round($row['valor'], 2) * $row['qtd'];
            echo "<td valign='top'>{$row['paciente']}</td>";

            if($resultado == 'a')
            {
                $data = implode("/",array_reverse(explode("-",$row['data'])));
                echo "<td valign='top'>{$data}</td>";
            }
            echo "<td valign='top'>{$row['nome']}</td>";

            echo "<td valign='top'>{$row['qtd']}</td>";
            echo "<td valign='top'>{$row['LOTE']}</td>";
            $totalItens += $row['qtd'];
            $totalCusto += $subtotal;
            echo "<td valign='top'>R$ " . number_format($row['valor'], 2, ',', '.') . "</td>";
            echo "<td valign='top'>R$ " . number_format($subtotal, 2, ',', '.') . "</td>";

            echo "</tr>";
        }

        $colspan = $resultado == 'a' ? 3 : 2;
        echo "<tr style='background-color:lightgrey'>
                <td colspan='{$colspan}' style='text-align: right;b'> <b>Total de Itens:</b></td>
                <td>$totalItens</td>
                <td>&nbsp;</td>";
        echo "<td style='text-align: right;b'> <b>Custo Total:</b></td>
                <td>R$ " . number_format($totalCusto, 2, ',', '.') . "</td>
              </tr>";
        echo "</table>";
        echo "</form>";

        if ($f) {
            echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
            //echo "<center><b>Nenhum resultado foi encontrado!</b></center>.$sql";
        } else {
            echo "<form action='imprimir_saidas.php?resultado=$resultado&tipo=$tipo&codigo=$codigo&inicio=$inicio&fim=$fim&ur=" . $_SESSION['empresa_principal'] . "' target='_blank' method='post'>
<button id='imprimir_ficha_mederi' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button' type='submit' target='_blank'>
<span class='ui-button-text'>Imprimir</span>
</button>
</form>";
        }
    }

    public function rastrear($tipo,$cod,$ientrada,$fentrada){
        $tipo=$_POST['tipo-rastreamento'];
        echo "<center><h1>Resultado</h1></center>";
        echo "<table class='mytable' width=100% >";
        echo "<thead><tr>";
        echo "<th><b>Nome</b> </th>";
        echo "<th><b>Fonecedor</b></th>";
        echo "<th><b>Data</b></th>";
        echo "<th><b>Quantidade</b></th>";
        echo "<th><b>Valor unit&aacute;rio</b></th>";
        echo "</tr></thead>";
        $cor = false;
        $sql = "";
        $i = "1900-01-01";
        $f = "2200-01-01";
        if($ientrada != "") $i = implode("-",array_reverse(explode("/",$ientrada)));
        if($fentrada != "") $f = implode("-",array_reverse(explode("/",$fentrada)));
        if($tipo == "med"){
            $sql = "SELECT 
            e.qtd as quantidade,
            e.valor,
            e.data, 
            f.razaoSocial as fornecedor,
            b.principio as nome, 
            b.apresentacao as segundoNome,
             b.lab_desc
    	     FROM entrada as e,
             fornecedores as f,
             catalogo as b
    	    WHERE f.idFornecedores = e.fornecedor
            AND b.ID = e.CATALOGO_ID 
            AND e.data BETWEEN '{$i}' AND '{$f}' 
            AND b.tipo = 0 
            AND b.ID = '{$cod}'  
           ORDER BY e.data DESC, nome, segundoNome, fornecedor, quantidade DESC, valor DESC";
        } else if($tipo == "mat"){
            $sql = "SELECT 
            e.qtd as quantidade,
            e.valor, 
            e.data, 
            f.razaoSocial as fornecedor,
            b.principio as nome,
            b.apresentacao as segundoNome,
             b.lab_desc
    	    FROM entrada as e, 
            fornecedores as f, 
            catalogo as b
    	    WHERE f.idFornecedores = e.fornecedor 
            AND b.ID = e.CATALOGO_ID 
            AND e.data BETWEEN '{$i}' AND '{$f}' 
            AND b.tipo = 1 
            AND b.ID = '{$cod}'   
    	    ORDER BY e.data DESC, nome, segundoNome, fornecedor, quantidade DESC, valor DESC";
        } else if($tipo == "die"){
            $sql = "SELECT 
            e.qtd as quantidade, 
            e.valor, 
            e.data,
            f.razaoSocial as fornecedor,
            b.principio as nome,
            b.apresentacao as segundoNome,
             b.lab_desc
    	    FROM entrada as e, 
            fornecedores as f,
            catalogo as b
            WHERE f.idFornecedores = e.fornecedor 
             AND b.ID = e.CATALOGO_ID 
             AND e.data BETWEEN '{$i}' AND '{$f}'
             AND b.tipo = 3 
             AND b.ID = '{$cod}'   
    	     ORDER BY e.data DESC, nome, segundoNome, fornecedor, quantidade DESC, valor DESC";
        } else if($tipo == "fornecedor"){
            $sql = "SELECT 
            e.qtd as quantidade,
            e.valor, 
            e.data,
            f.razaoSocial as fornecedor, 
            b.principio as nome, 
            b.apresentacao as segundoNome 
    	    FROM entrada as e,
            fornecedores as f, 
            catalogo as b
    	    WHERE f.idFornecedores = e.fornecedor
            AND b.ID = e.CATALOGO_ID 
            AND e.data BETWEEN '{$i}' AND '{$f}' 
            AND f.idFornecedores = '{$cod}'  
    	   ORDER BY e.data DESC, nome, segundoNome, fornecedor, quantidade DESC, valor DESC";
        }
        $result = mysql_query($sql) or trigger_error(mysql_error());
        $f = true;
        while($row = mysql_fetch_array($result))
        {
            $f = false;
            foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
            if($cor) echo "<tr>";
            else echo "<tr class='odd'>";
            $cor = !$cor;
            $data = implode("/",array_reverse(explode("-",$row['data'])));
            echo "<td valign='top'>{$row['nome']} - {$row['segundoNome']} LAB: {$row['lab_desc']}</td>";
            echo "<td valign='top'>{$row['fornecedor']}</td>";
            echo "<td valign='top'>{$data}</td>";
            echo "<td valign='top'>{$row['quantidade']}</td>";
            echo "<td valign='top'> R$ {$row['valor']}</td>";
            echo "</tr>";
        }
        echo "</table>";

        if($f)
        {
            echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
        }
        else
        {
            echo "<form action='imprimir_rastreamento.php?tipo=$tipo&cod=$cod&inicio=$ientrada&fim=$fentrada' target='_blank' method='post'>
            <button id='imprimir_ficha_mederi' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button' type='submit' target='_blank'>
                <span class='ui-button-text'>Imprimir</span>
            </button>
          </form>";
        }

    }

    public function controlado_busca($id,$tipo,$inicio,$fim){
        if($inicio != "") $i = implode("-",array_reverse(explode("/",$inicio)));
        if($fim != "") $f = implode("-",array_reverse(explode("/",$fim)));
        echo "<center><h1>Resultado</h1></center>";
        echo "<table class='mytable' width=100% >";
        echo "<thead><tr>";
        echo "<th><b>Movimenta&ccedil;&atilde;o</b></th>";
        echo "<th><b>Nome</b></th>";
        echo"<th>Item</th>";
        echo "<th><b>Data</b></th>";
        echo "<th><b>Quantidade</b></th>";
        echo "<th><b>Lote</b></th>";
        echo "</tr></thead>";
        if( $_SESSION['empresa_principal']== 1){
            if($tipo == 1){
                $cond=" and s.CATALOGO_ID = '{$id}'";
                $cond2 =" and s.CATALOGO_ID= '{$id}'";
            }
            else
            {
                $cond=" ";
                $cond2=" ";
            }
            $sql="SELECT 
					p.nome as paciente, 
					concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
					s.LOTE, 
					'Saida' as tipo_mov,
					s.quantidade as qtd1,
					s.data
				FROM saida as s inner join clientes as p on ( p.idClientes = s.idCliente) 
					inner join catalogo as cb on(cb.ID = s.CATALOGO_ID)
				WHERE  
                                s.tipo =0 
                                AND s.data BETWEEN '$i' AND '$f' $cond
				and idCliente 
                                in(select 
                                idClientes from clientes where empresa ={$_SESSION['empresa_principal']})
				and cb.CONTROLADO='S'                                
				union                                
				SELECT 
					p.nome as paciente, 
					concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
					s.LOTE,
					'Saida' as tipo_mov, 
					s.quantidade as qtd1,
					s.data
				FROM saida as s inner join 
                                       empresas as p on ( p.id = s.UR) 
					inner join catalogo as cb on (cb.ID = s.CATALOGO_ID)
				WHERE s.tipo =0  
                                AND s.data BETWEEN '$i' AND '$f' 
                                and s.UR != 0
                                and s.idCliente = 0  $cond 
                                and cb.CONTROLADO='S'
				union 
				SELECT 
					p.razaoSocial as paciente, 
					concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
					s.LOTE, 
					'Entrada' as tipo_mov,
					s.qtd as qtd1,
					s.data
				FROM entrada as s inner join fornecedores as p on ( p.idFornecedores = s.fornecedor) 
					inner join catalogo as cb on(cb.ID = s.CATALOGO_ID)
					inner join usuarios as u on (s.USUARIO_ID = u.idUsuarios)
				WHERE  cb.tipo =0 
                                AND s.data BETWEEN '$i' AND '$f'
				and cb.CONTROLADO='S'  $cond2 
                                and u.empresa = {$_SESSION['empresa_principal']}
				order by data";



        }else{

            if($tipo == 1){
                $cond=" and s.CATALOGO_ID= '{$id}'";
                $cond3=" and i.CATALOGO_ID = '{$id}'";
                $cond2=" and s.CATALOGO_ID= '{$id}'";
            }
            else
            {
                $cond=" ";
                $cond2=" ";
                $cond3=" ";
            }

            $sql="SELECT 
                    p.nome as paciente, 
                    concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                    s.LOTE,
                    'Saida' as tipo_mov,
                    s.quantidade as qtd1, 
                    s.data 
                    FROM saida as s inner join 
                    clientes as p on ( p.idClientes = s.idCliente) 
                    inner join catalogo as cb on (cb.ID = s.CATALOGO_ID)
                    WHERE 
                    s.tipo =0 AND
                    s.data BETWEEN '$i' AND '$f'
                    {$cond}
                    and s.idCliente in 
                         (select 
                           idClientes
                           from 
                           clientes 
                           where empresa ={$_SESSION['empresa_principal']}) 
                    and cb.CONTROLADO='S'         
                    union
                    SELECT 
                    p.razaoSocial as paciente, 
                    concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                    s.LOTE, 
                    'Entrada' as tipo_mov, 
                    s.qtd as qtd1, 
                    s.data 
                    FROM entrada as s inner join fornecedores as p on ( p.idFornecedores = s.fornecedor) 
                    inner join catalogo as cb on(cb.ID = s.CATALOGO_ID)
                    inner join usuarios as u on (s.USUARIO_ID = u.idUsuarios) 
                    WHERE cb.tipo =0 AND s.data BETWEEN '$i' AND '$f' 
                     {$cond2}
                    and  cb.CONTROLADO='S' and u.empresa ={$_SESSION['empresa_principal']}
                    union 
                    SELECT 
                    'Mederi Central' as paciente,
                    concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                    s.LOTE,
                    'Entrada' as tipo_mov,
                    s.QTD as qtd1, 
                    DATE_FORMAT(s.data, '%Y-%m-%d') as data
                    FROM entrada_ur as s inner join itenspedidosinterno as i on (s.ITENSPEDIDOSINTERNOS_ID = i.ID)
                    inner join catalogo as cb on(cb.ID = i.CATALOGO_ID) 

                    inner join usuarios as u on (u.idUsuarios = s.USUARIO_ID) 
                    WHERE cb.tipo =0 AND s.data BETWEEN '$i' AND '$f' and cb.CONTROLADO='S' {$cond3}
                     and u.empresa = {$_SESSION['empresa_principal']} order by data,paciente";


        }
        $f = true;
        $result = mysql_query($sql) or trigger_error(mysql_error());

        while($row = mysql_fetch_array($result)){
            $f = false;
            foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
            if($cor) echo "<tr>";
            else echo "<tr class='odd'>";
            $cor = !$cor;
            $data = implode("/",array_reverse(explode("-",$row['data'])));
            echo "<td valign='top'>{$row['tipo_mov']}</td>";
            echo "<td valign='top'>{$row['paciente']}</td>";
            echo "<td valign='top'>{$row['nome']} </td>";
            echo "<td valign='top'>{$data}</td>";
            echo "<td valign='top'>{$row['qtd1']}</td>";
            echo "<td valign='top'> {$row['LOTE']}</td>";
            echo "</tr>";
        }
        echo "</table>";

        if($f){
            echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
        }
        else{

            echo"<form action='imprimir_controlados.php?id=$id&tipo=$tipo&inicio=$inicio&fim=$fim&ur=".$_SESSION['empresa_principal']."' target='_blank' method='post'>
<button id='imprimir_ficha_mederi' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button' type='submit' target='_blank'>
<span class='ui-button-text'>Imprimir</span>
</button>
</form>";
        }
    }

    public function antibiotico_busca($id,$tipo,$inicio,$fim){
        if($inicio != "") $i = implode("-",array_reverse(explode("/",$inicio)));
        if($fim != "") $f = implode("-",array_reverse(explode("/",$fim)));
        echo "<center><h1>Resultado</h1></center>";
        echo "<table class='mytable' width=100% >";
        echo "<thead><tr>";
        echo "<th><b>Movimenta&ccedil;&atilde;o</b></th>";
        echo "<th><b>Nome</b></th>";
        echo"<th>Item</th>";
        echo "<th><b>Data</b></th>";
        echo "<th><b>Quantidade</b></th>";
        echo "<th><b>Lote</b></th>";
        echo "</tr></thead>";
        if( $_SESSION['empresa_principal']== 1){
            if($tipo == 1){
                $cond=" and s.CATALOGO_ID = '{$id}'";
                $cond2 =" and s.CATALOGO_ID= '{$id}'";
            }
            else
            {
                $cond=" ";
                $cond2=" ";
            }
            $sql="SELECT
					p.nome as paciente,
					concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
					s.LOTE,
					'Saida' as tipo_mov,
					s.quantidade as qtd1,
					s.data
				FROM saida as s inner join clientes as p on ( p.idClientes = s.idCliente)
					inner join catalogo as cb on(cb.ID = s.CATALOGO_ID)
				WHERE
                                s.tipo =0
                                AND s.data BETWEEN '$i' AND '$f' $cond
				and idCliente
                                in(select
                                idClientes from clientes where empresa ={$_SESSION['empresa_principal']})
				and cb.ANTIBIOTICO='S'
				union
				SELECT
					p.nome as paciente,
					concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
					s.LOTE,
					'Saida' as tipo_mov,
					s.quantidade as qtd1,
					s.data
				FROM saida as s inner join
                                       empresas as p on ( p.id = s.UR)
					inner join catalogo as cb on (cb.ID = s.CATALOGO_ID)
				WHERE s.tipo =0
                                AND s.data BETWEEN '$i' AND '$f'
                                and s.UR != 0
                                and s.idCliente = 0  $cond
                                and cb.ANTIBIOTICO='S'
				union
				SELECT
					p.razaoSocial as paciente,
					concat(cb.principio,' - ',cb.apresentacao,' LAB: ', cb.lab_desc) as nome,
					s.LOTE,
					'Entrada' as tipo_mov,
					s.qtd as qtd1,
					s.data
				FROM entrada as s inner join fornecedores as p on ( p.idFornecedores = s.fornecedor)
					inner join catalogo as cb on(cb.ID = s.CATALOGO_ID)
					inner join usuarios as u on (s.USUARIO_ID = u.idUsuarios)
				WHERE  cb.tipo =0
                                AND s.data BETWEEN '$i' AND '$f'
				and cb.ANTIBIOTICO='S'  $cond2
                                and u.empresa = {$_SESSION['empresa_principal']}
				order by data";



        }else{

            if($tipo == 1){
                $cond=" and s.CATALOGO_ID= '{$id}'";
                $cond3=" and i.CATALOGO_ID = '{$id}'";
                $cond2=" and s.CATALOGO_ID= '{$id}'";
            }
            else
            {
                $cond=" ";
                $cond2=" ";
                $cond3=" ";
            }

            $sql="SELECT
                    p.nome as paciente,
                    concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                    s.LOTE,
                    'Saida' as tipo_mov,
                    s.quantidade as qtd1,
                    s.data
                    FROM saida as s inner join
                    clientes as p on ( p.idClientes = s.idCliente)
                    inner join catalogo as cb on (cb.ID = s.CATALOGO_ID)
                    WHERE
                    s.tipo =0 AND
                    s.data BETWEEN '$i' AND '$f'
                    {$cond}
                    and s.idCliente in
                         (select
                           idClientes
                           from
                           clientes
                           where empresa ={$_SESSION['empresa_principal']})
                    and cb.ANTIBIOTICO='S'
                    union
                    SELECT
                    p.razaoSocial as paciente,
                    concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                    s.LOTE,
                    'Entrada' as tipo_mov,
                    s.qtd as qtd1,
                    s.data
                    FROM entrada as s inner join fornecedores as p on ( p.idFornecedores = s.fornecedor)
                    inner join catalogo as cb on(cb.ID = s.CATALOGO_ID)
                    inner join usuarios as u on (s.USUARIO_ID = u.idUsuarios)
                    WHERE cb.tipo =0 AND s.data BETWEEN '$i' AND '$f'
                     {$cond2}
                    and  cb.ANTIBIOTICO='S' and u.empresa ={$_SESSION['empresa_principal']}
                    union
                    SELECT
                    'Mederi Central' as paciente,
                    concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                    s.LOTE,
                    'Entrada' as tipo_mov,
                    s.QTD as qtd1,
                    DATE_FORMAT(s.data, '%Y-%m-%d') as data
                    FROM entrada_ur as s inner join itenspedidosinterno as i on (s.ITENSPEDIDOSINTERNOS_ID = i.ID)
                    inner join catalogo as cb on(cb.ID = i.CATALOGO_ID)

                    inner join usuarios as u on (u.idUsuarios = s.USUARIO_ID)
                    WHERE cb.tipo =0 AND s.data BETWEEN '$i' AND '$f' and cb.ANTIBIOTICO='S' {$cond3}
                     and u.empresa = {$_SESSION['empresa_principal']} order by data,paciente";


        }
        $f = true;
        $result = mysql_query($sql) or trigger_error(mysql_error());


        while($row = mysql_fetch_array($result)){
            $f = false;
            foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
            if($cor) echo "<tr>";
            else echo "<tr class='odd'>";
            $cor = !$cor;
            $data = implode("/",array_reverse(explode("-",$row['data'])));
            echo "<td valign='top'>{$row['tipo_mov']}</td>";
            echo "<td valign='top'>{$row['paciente']}</td>";
            echo "<td valign='top'>{$row['nome']} </td>";
            echo "<td valign='top'>{$data}</td>";
            echo "<td valign='top'>{$row['qtd1']}</td>";
            echo "<td valign='top'> {$row['LOTE']}</td>";
            echo "</tr>";
        }
        echo "</table>";

        if($f){
            echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
        }
        else{

            echo"<form action='imprimir_antibioticos.php?id=$id&tipo=$tipo&inicio=$inicio&fim=$fim&ur=".$_SESSION['empresa_principal']."' target='_blank' method='post'>
<button id='imprimir_ficha_mederi' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button' type='submit' target='_blank'>
<span class='ui-button-text'>Imprimir</span>
</button>
</form>";
        }
    }


    public function buscar_dieta($inicio,$fim){
        if($inicio != "") $i = implode("-",array_reverse(explode("/",$inicio)));
        if($fim != "") $f = implode("-",array_reverse(explode("/",$fim)));
        echo "<center><h1>Resultado</h1></center>";
        echo "<table class='mytable' width=100% >";
        echo "<thead><tr>";

        echo "<th><b>Nome</b></th>";
        echo"<th>Dieta</th>";
        echo "<th><b>Data Saída</b></th>";
        echo "<th><b>Quantidade</b></th>";
        echo" <th><b>Lote</b></th>";
        echo "<th><b>Respons&aacute;vel</b></th>";
        echo "<th><b>Data Transação</b></th>";
        echo "</tr></thead>";
        if($_SESSION['empresa_principal'] != 1){
            $sql="SELECT 
		p.nome as paciente, 
		s.NUMERO_TISS,
                s.CATALOGO_ID,
		concat(cb.principio,' - ',cb.apresentacao) as nome, 
		s.LOTE, 
		s.quantidade as qtd,
		s.data,
		u.nome as resp,
                DATE_FORMAT(s.DATA_SISTEMA,'%d/%m/%Y') as dt_sistema
		FROM saida as s inner join clientes as p on ( p.idClientes = s.idCliente) 
		inner join catalogo as cb on(cb.ID = s.CATALOGO_ID)
		inner join usuarios as u on (u.idUsuarios = s.idUsuario)
		WHERE s.tipo =3 AND s.data BETWEEN '$i' AND '$f' 
                   and s.idCliente <> 0 and p.empresa = {$_SESSION['empresa_principal']}
		ORDER BY paciente ASC";
        }else{
            $sql="SELECT 
		p.nome as paciente, 
		s.NUMERO_TISS,
                s.CATALOGO_ID,
		concat(cb.principio,' - ',cb.apresentacao) as nome, 
		s.LOTE, 
		s.quantidade as qtd,
		s.data,
		u.nome as resp,
                DATE_FORMAT(s.DATA_SISTEMA,'%d/%m/%Y') as dt_sistema
		FROM
                saida as s inner join 
                clientes as p on ( p.idClientes = s.idCliente) 
		inner join catalogo as cb on(cb.ID = s.CATALOGO_ID)
		inner join usuarios as u on (u.idUsuarios = s.idUsuario)
		WHERE 
                s.tipo =3 
                AND s.data BETWEEN '$i' AND '$f' 
                and s.idCliente <> 0 and p.empresa ={$_SESSION['empresa_principal']}
                Union ALL
                SELECT 
		e.nome as paciente, 
		s.NUMERO_TISS,
                s.CATALOGO_ID,
		concat(cb.principio,' - ',cb.apresentacao) as nome, 
		s.LOTE, 
		s.quantidade as qtd,
		s.data,
		u.nome as resp,
                DATE_FORMAT(s.DATA_SISTEMA,'%d/%m/%Y') as dt_sistema
		FROM
                saida as s inner join 
                empresas as e on ( e.id = s.UR) 
		inner join catalogo as cb on(cb.ID = s.CATALOGO_ID)
		inner join usuarios as u on (u.idUsuarios = s.idUsuario)
		WHERE 
                s.tipo =3 
                AND s.data BETWEEN '$i' AND '$f' 
                and s.UR <> 0
		ORDER BY data ASC";

        }
        // echo $sql;
        $f = true;
        $result = mysql_query($sql) or trigger_error(mysql_error());
        while($row = mysql_fetch_array($result)){
            $f = false;
            foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
            if($cor) echo "<tr>";
            else echo "<tr class='odd'>";
            $cor = !$cor;
            $data = implode("/",array_reverse(explode("-",$row['data'])));


            echo "<td valign='top'>{$row['paciente']}</td>";
            echo "<td valign='top'>{$row['nome']} </td>";
            echo "<td valign='top'>{$data}</td>";
            echo "<td valign='top'>{$row['qtd']}</td>";
            echo "<td valign='top'>{$row['LOTE']} </td>";
            echo "<td valign='top'> {$row['resp']}</td>";
            echo "<td valign='top'> {$row['dt_sistema']}</td>";
            echo "</tr>";
        }
        echo "</table>";

        if($f){
            echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
        }
        else{
            echo"<form action='imprimir_dieta.php?inicio=$inicio&fim=$fim' target='_blank' method='post'>
<button id='imprimir_ficha_mederi' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button' type='submit' target='_blank'>
<span class='ui-button-text'>Imprimir</span>
</button>
</form>";
        }

    }


}

?>