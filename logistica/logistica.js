$(function($) {
    $.fn.posicaoCursor = function( posicao ) {

        return this.each(function() {

            var $this = $( this );

            if ( $this.get(0).setSelectionRange ) {

                $this.get(0).setSelectionRange( posicao, posicao );

            } else if ( $this.get(0).createTextRange ) {

                var intervalo = $this.get(0).createTextRange();

                intervalo.collapse( true );

                intervalo.moveEnd('character', posicao);

                intervalo.moveStart('character', posicao);

                intervalo.select();

            }

        });

    };

	$('.pacientes').chosen({search_contains: true});
	$('.convenios').chosen({search_contains: true});
	$('.fornecedores').chosen({search_contains: true});

    $("#rel-equip-ativo-por-convenio").hide();
	$(".filtro-rel-equip-ativo").change(function () {
		if($(this).val() == 'p') {
            $("#rel-equip-ativo-por-convenio").hide();
            $("#rel-equip-ativo-por-paciente").show();
            $(".convenios").removeClass("COMBO_OBG").val("");
            $(".pacientes").addClass("COMBO_OBG");
            $(".convenios").trigger("chosen:updated");
		} else {
            $("#rel-equip-ativo-por-convenio").show();
            $("#rel-equip-ativo-por-paciente").hide();
            $(".pacientes").removeClass("COMBO_OBG").val("");
            $(".convenios").addClass("COMBO_OBG");
            $(".pacientes").trigger("chosen:updated");
		}
    });

 $('#monitor').click(function(){    	
    	window.open("http://localhost/logistica/monitor.php?op=saida2&act=pendentes&m=1","fullscreen=yes");
    });
	
 
  ///Desabilitando o enter///

    $(document).ready(function () {
    	/* $.post("query.php",{query:'brasindice',ur:ur,item:$_SESSION['item_busca_brasindice'],tipo:tipo},function(r){
           $("#brasindice_resultado").html(r);    		
	}); */
 	 $('input').keypress(function (e) {
    	        var code = null;
    	        code = (e.keyCode ? e.keyCode : e.which);                
    	        //return (code == 13) ? false : true;
    	        
    	        if (code == 13) {
				
    	        	if ($(this).attr('p') == 'pes-item-brasindice') {
    	        		
    	        		pesquisa_brasindice();
    	        		
    				}else{
    					return false;
    				}
    	        	
				} else {
					return true;
				}
    	        
    });
 	 
   });  

/////////////////////////////Estoque por UR/////////////
    $("#busca_est_span").hide();
    /*$("#sel_estoque").change(function(){
    	$("res-b").remove();
    	var x = $(this).val();
    	if(x != 't'){
    		$("#busca_est_span").show();
    		$('#busca-item-est').addClass('OBG');
    	}else{
    		$("#busca_est_span").hide();
    		$('#busca-item-est').removeClass('OBG');
    		$('#busca-item-est').removeAttr('style');
    	}
    	
    });*/
    
    $("#sel_estoque").change(function(){
    	$("#res-b").remove();
    	$("#salvar_est").remove();
    	$('#busca-item-est').val('');
    	var x = $(this).val();
    	if(x != 't'){
    		$("#busca_est_span").show();
    		$('#busca-item-est').addClass('OBG');
    	}else{
    		$("#busca_est_span").hide();
    		$('#busca-item-est').removeClass('OBG');
    		$('#busca-item-est').removeAttr('style');
    	}
    	
    });
    
    $("#dialog-menssagem").dialog('close');
    $( "#dialog-menssagem" ).dialog({
    	
    	minWidth: 300,minHeight: 200,autoOpen: false,modal: true,
    	buttons: {
    	  Ok: function() {
    	    $( this ).dialog( "close" );
    	    window.location.href='?op=estoque';  
    	  }
    	  
    	}
      });

  // SM-942
  $("#solicitacoes-pendentes-tabs").tabs();

    $("#salvar_est").live('click',function(){

		var tam = $(".linha_est").size();
		var cont=0;
		var itens='';
		var quantidadiItemModificado = 0;
		$(".linha_est").each(function(){

			var id = $(this).attr('id');
			tipo = $(this).attr('tipo');
			qtd = $(this).parent().children('td').eq(1).children('input').val();
			qtd_anterior = $(this).parent().children('td').eq(1).children('input').attr('qtd_anterior');
			ocultar = $(this).parent().children('td').eq(2).children('input').attr('ocultar');
			qtd_min = $(this).parent().children('td').eq(2).children('input').val();
			qtd_min_anterior = $(this).parent().children('td').eq(2).children('input').attr('qtd_min_anterior');
			justificativa = $(this).parent().children('td').eq(3).children('textarea').val();
			var valor_anterior = $(this).attr('custo');

			var catalogo_id= $(this).attr('catalogo_id');
			if (ocultar == "N"){
				valor = $(this).parent().children('td').eq(4).children('input').val();

			}else{
				valor = $(this).parent().children('td').eq(2).children('input').attr('valor');
			}
			val= parseFloat(valor.replace('.','').replace(',','.'));
			cont++;
			if((qtd_min != qtd_min_anterior) || (qtd_anterior != qtd) || (valor_anterior != valor)){
				quantidadiItemModificado++;
				$(this).parent().children('td').eq(3).children('textarea').addClass('OBG');
				if(cont==tam)
					itens += id+'#'+qtd+'#'+val+'#'+qtd_min+'#'+catalogo_id+'#'+qtd_anterior+'#'+qtd_min_anterior+'#'+justificativa+'#'+tipo;
				else
					itens += id+'#'+qtd+'#'+val+'#'+qtd_min+'#'+catalogo_id+'#'+qtd_anterior+'#'+qtd_min_anterior+'#'+justificativa+'#'+tipo+'$';
			}else{
				$(this).parent().children('td').eq(3).children('textarea').removeClass('OBG');
				$(this).parent().children('td').eq(3).children('textarea').attr('style','');
			}




		});

		if(quantidadiItemModificado == 0){
			alert('Nenhum item foi modificado');
			return false;
		}
    	
    	if(validar_campos('res-b')){

    		$.post("query.php",{query:'edt_estoque',dados:itens},function(r){
    			if(r == '1'){
    				$("#msg-sucesso").show().html("Dados editados com sucesso!");
					setTimeout(function () {
						$("#msg-sucesso").hide()
					}, 4000);
    				//$("#dialog-menssagem").dialog('open');
    			}else{
                    alert(r);
                }
		});
    	}
    	
    	
    });
    
    $("#busca_brasindice_span").hide();
    $("#tipo_brasindice").change(function(){
    	$("res-b").remove();
    	var x = $(this).val();
    	
    	if(x != -1){
    		$("#busca_brasindice_span").show();
    		$('#item-busca-brasindice').addClass('OBG');
    	}else{
    		$("#busca_brasindice_span").hide();
    		$('#item-busca-brasindice').removeClass('OBG');
    		$('#item-busca-brasindice').removeAttr('style');
    	}
    	
    });
    
    $("#tag_brasindice").hide();
    $(".controlado").hide();
	$(".antibiotico").hide();
    
$(".controlado").each(function(i){
    	
    	var tipo = $("#tipo_brasindice_cadastro").val();
    	
    	if(tipo == 0){
    		$(".controlado").show();
			 $(".antibiotico").show();
    	}
    	
    });
    if($("select[id='tipo_brasindice_cadastro'] option:selected").val() == 1){
        $("#nome_simpro").attr("readonly","");
    }
    $("#tipo_brasindice_cadastro").change(function(){
                    if(!($("#cadastro-produto-interno").is(":checked"))){
    	              $('#principio').val('');
                      $('#apresentacao').val('');
						$('#laboratorio').val('');
                      $('#numero_tiss').val('');
                      $('#numero_tuss').val('');
                      $('#buscar-item-cadastro').val('');
                      $('#referencia').val('-1');
                      $('#nome-simpro-brasindice').val('');                     
                 }
                 $('#id-simpro-brasindice').val('');

    	var x = $(this).val();
        if(x == 0 || x == 1|| x == 3){
            $("#buscar-item-cadastro").attr("disabled","");
            $("#principio").attr("disabled","");
            $("#apresentacao").attr("disabled","");
        }else{
            $("#buscar-item-cadastro").attr("disabled","disabled");
            $("#principio").attr("disabled","disabled");
            $("#apresentacao").attr("disabled","disabled");            

        }
    	
    	if(x == 1){
            $("#nome_simpro").attr("readonly","");
            $("#nome_simpro").addClass("OBG");
            $("#tag_brasindice").show();
            $("#referencia").val('S');
            $("#categoria-material").addClass("OBG");
            $("#categoria-material-combo").show();
    	}else{
            $("#nome_simpro").attr("readonly","readonly");
            $("#tag_brasindice").hide();
            $("#nome_simpro").removeClass("OBG");
            $("#categoria-material").removeClass("OBG").val('');
            $("#categoria-material-combo").hide();
    	}
    	
    	if(x == 0){
    		$(".controlado").show();
			$(".antibiotico").show();
			$(".controlado").show();
			$("#controlado").addClass('COMBO_OBG');
			$("#antibiotico").addClass('COMBO_OBG');
			$(".antibiotico").show();
			$("#referencia").val('T');
			$("#buscar-principio").attr('disabled',false);
			$("#buscar-principio").addClass('OBG');
			$("#generico").attr('disabled',false);
			$("#generico").addClass('OBG');
            $(".possui-generico").show();
            $("#possui-generico")
                .attr('disabled', false)
				.addClass('OBG');
    	}else{
    		$(".controlado").hide();
                $(".antibiotico").hide();
			    $("#buscar-principio").attr('disabled',true);
			    $("#buscar-principio").val('');
				$("#id-principio").val('');
                $("#controlado").removeClass('COMBO_OBG');
                $("#antibiotico").removeClass('COMBO_OBG');
				$("#antibiotico").removeAttr('style');
				$("#controlado").removeAttr('style');
				$("#buscar-principio").removeClass('OBG');
				$("#buscar-principio").removeAttr('style');
			$("#generico").attr('disabled',true);
			$("#generico").removeClass('OBG');
			$("#generico").removeAttr('style');
            $(".possui-generico").hide();
            $("#possui-generico")
                .attr('disabled',true)
				.removeClass('OBG');
                if(x == 3){
                    $("#referencia").val('T');
                }
    	}
    	
    });
    
    $(".mostrar_tag").each(function(i){
    	
    	var tag = $("#tags").val();
    	var tipo = $("#tipo_brasindice_cadastro").val();
    	
    	if(tipo == 1){
    		$("#tag_brasindice").show();
    	}
    	
    });
    
  /*  $("input[name=busca_est]").autocomplete({
		source: function(request, response) {
          $.getJSON('/utils/busca_brasindice.php', {
              term: request.term,
              tipo: $('#sel_estoque').val(),
             
          }, response);
      },
		minLength: 3,
		select: function( event, ui ) {
				$('#busca-itemav').val('');
    			$('#busca-item-est').val(ui.item.value);
			    $('#cod').val(ui.item.id);
			    $('#apresentacao').val(ui.item.apresentacao);
			    $('#custo').val(ui.item.custo);
    			$('#qtd').focus();
    			$('#busca-itemav').val('');
    			return false;
		},
		extraParams: {
 			tipo: 2
		}
});*/
    
    /////////////////
    $('input[name=busca]').autocomplete({
		source: function(request, response) {
          $.getJSON('busca_brasindice.php', {
              term: request.term,
              tipo: $('#dialog-formularioav').attr('tipo'),
              plano:$('#dialog-formularioav').attr('plano')
          }, response);
      },
		minLength: 3,
		select: function( event, ui ) {
				$('#busca-itemav').val('');
    			$('#nome').val(ui.item.value);
			    $('#cod').val(ui.item.id);
                            $('#catalogo_id').val(ui.catalogo.id);
			    $('#apresentacao').val(ui.item.apresentacao);
			    $('#custo').val(ui.item.custo);
    			$('#qtd').focus();
    			$('#busca-itemav').val('');
    			return false;
		},
		extraParams: {
 			tipo: 2
		}
    });
	
	
	
    
    $("input[name=busca_pedido]").autocomplete({
    	
    	source: function(request, response) {
          $.getJSON('busca_brasindice_pedido.php', {
              term: request.term,
              tipo: $('input:radio[name=tipos]:checked').val()
          }, response);
      },
    	minLength: 3,
    	select: function( event, ui ) {
    	  
    			$('#busca-item-pedido').val('');
    			$('#numero_tiss').val(ui.item.numero_tiss);
                $('#catalogo_id').val(ui.item.catalogo_id);
    		    $('#nome').val(ui.item.value);
    			$('#qtd').focus();
    			$('#tipo').val(ui.item.tipo);
				
    			
    			return false;
    	},
    	extraParams: {
    			tipo_consulta: 8
    	}
    });
    
    $('#add-item-pedido').click(function(){
   	 
	     if(validar_campos('dialog-pedido-estoque')){
		      //var cod = $('#cod').val(); 
		      var nome = $('#dialog-pedido-estoque input[name=nome]').val();   
		      var apresentacao = $('#apresentacao').val();
		      var tipo = $('input:radio[name=tipos]:checked').val();
		      var numero_tiss = $('#numero_tiss').val();
              var catalogo_id=$('#catalogo_id').val();
		      
		      if(tipo == 'med'){
	              tipo = 0;
	              apresentacao = 'MEDICAMENTO';
	          }else if(tipo == 'mat'){ 
	        	  tipo = 1;
	        	  apresentacao = 'MATERIAL';
	          }else if(tipo == 'eqp'){ 
	        	  apresentacao = 'EQUIPAMENTO'; 
	        	  tipo = 5;//VERIFICAR
	          }else if(tipo == 'die'){
	        	  apresentacao = 'DIETA'; 
	        	  tipo = 3;

	          }
		      
		      var qtd = $('#dialog-pedido-estoque input[name=qtd]').val();
		      var paciente = $('#dialog-pedido-estoque').attr('cod');
		      
		      //var dados = nome+'#'+tipo+'#'+qtd + '#'+numero_tiss;
		      
		      var linha = nome;
		      	     	      
		      var botoes = "<img align='right' class='rem-item-pedido' src='../utils/delete_16x16.png' title='Remover' border='0'>";
		      $('#tabela-itens-pedido').append('<tr><td>'+linha+botoes+'</td><td>'+apresentacao+'</td>'+'<td>'+
		    		  '<input class=\'qtd dados\' size=\'7\' value='+qtd+' numero_tiss=' + numero_tiss + '  catalogo_id=' + catalogo_id + ' tipo=' + tipo + ' /></td></tr>');
	
		      var num_itens = $('#tabela-itens-pedido tr').length;
		      
		      if (num_itens > 1) {
		    	  $(".mostrar_botao_pedido").show();  
		      }

		      $("#nome").val('');
		      $("#qtd").val('');
		      
	     }	
	     atualiza_item_pedido();
	    return false;
	  });
    
    //Remove itens do pedido
    $(".rem-item-pedido").unbind("click",remove_item_pedido).bind("click",remove_item_pedido);
    
    function atualiza_item_pedido(){
  	  $(".rem-item-pedido").unbind("click",remove_item_pedido).bind("click",remove_item_pedido);
    }
    
    function remove_item_pedido(){
    	$(this).parent().parent().remove();
    	
    	return false;
    }


	$("#data_envio").datepicker({
		inline: true,
		changeMonth: true,
		changeYear: true,
		
		minDate: '+2d'

	});

	$("#data_envio").change(function(){
    	var data_max = $("input[name='data_envio']").get(0).value; 
  	  data_max1 = data_max.split("/").reverse().join("");
  	 data_at = $("#data_atual").val();
  		if((data_max1 - data_at) < 2) {
  			alert("ERRO: A data não pode ter menos de 48 Horas! Os Pedidos Emergenciais deverão ser feitas atrav\u00e9s das Solicitaç\u00f5es Pendentes, pois a mesma dever\u00e1 estar associada a uma Prescrição M\u00e9dica ou a uma Solicitaç\u00f5es de Enfermagem.");
  			$("input[name='data_envio']").attr("style", "border: 3px solid red;");
  			return false;
  			}else{
  			$("input[name='data_envio']").removeAttr("style");
  			}
    	
    });
    $("#salvar-pedido-estoque").click(function() {
    	
    	if(validar_campos('div-pedido-estoque')){
    		var data_max = $("input[name='data_envio']").get(0).value; 
    	  	  data_max1 = data_max.split("/").reverse().join("");
    	  	 data_at = $("#data_atual").val();
    	  		if((data_max1 - data_at) < 2) {
    	  			alert("ERRO: A data não pode ter menos de 48 Horas! Os Pedidos Emergenciais deverão ser feitas atrav\u00e9s das Solicitaç\u00f5es Pendentes, pois a mesma dever\u00e1 estar associada a uma Prescrição M\u00e9dica ou a uma Solicitaç\u00f5es de Enfermagem.");
    	  			$("input[name='data_envio']").attr("style", "border: 3px solid red;");
    	  			return false;
    	  			}else{
    	  			$("input[name='data_envio']").removeAttr("style");
    	  			}
    		var data_envio = $("#data_envio").val();
        	var observacao = $("#obs").val();
        	var dados = "";
        	var tamanho = $(".dados").size();
        	if(tamanho > 0){
        	$(".dados").each(function(i){
        		
        		dados += $(this).attr("numero_tiss") + "#" + $(this).attr("tipo") + "#" + $(this).val()+ "#" + $(this).attr("catalogo_id");
        		
        		if(i<tamanho - 1) dados += "$";
        	});
        	
        	console.log("dados", dados);
        	
        	$.post("query.php",{query: "salvar-pedido-estoque", dados: dados, data_envio:data_envio, observacao:observacao},function(r){
    	   		if (r == 1  ) {
				alert("Pedido gravado com sucesso!");
    				window.location = "/logistica/"
    				
    			} else {
    				alert("Falha ao gravar pedido!"+ r);
    			}
    	   	});
        	}
    	}
    	
    });
    
    $(".mostrar_botao_pedido").hide();
    
    $('#tipos').buttonset();
    
    $('#add-item-pedido-estoque').click(function(){  
   	 
  	  $('#dialog-pedido-estoque').dialog('open');
  	  
    });   
    
    $('#dialog-pedido-estoque').dialog('close');
    
    
    $('#dialog-pedido-estoque').dialog({
  	  	autoOpen: false, modal: true, width: 800, position: 'top',
  	  	open: function(event,ui){
  	  		$('input[name=busca]').val('');
  	  		$('#nome').val('');
  		    $('#nome').attr('readonly','readonly');
  		    $('#apr').val('');
  		    $('#apr').attr('readonly','readonly');
  		    $('#cod').val('');
  		    $('#custo').val('');
  		    $('#qtd').val('');
  		    $('input[name=busca]').focus();
  	  	},
  	  	buttons: {
  	  		'Fechar' : function(){
  	  			var item = $('#dialog-pedido-estoque').attr('item');
  	  			if(item != 'avulsa'){
  	  				$('#kits-'+item).hide();
  	  				$('#med-'+item).hide();
  	  				$('#mat-'+item).hide();
  	  			}
  	  			$(this).dialog('close');
  	  		}
  	  	}
  	  });
    
$("input[name=busca_est]").autocomplete({
    	
		source: function(request, response) {
          $.getJSON('/utils/busca_estoque.php', {
              term: request.term,
              tipo: $('#sel_estoque').val()
             
          }, response);
      },
		minLength: 3,
		select: function( event, ui ) {
				$('#busca-itemav').val('');
    			$('#busca-item-est').val(ui.item.value);
			    $('#cod').val(ui.item.id);
			    $('#apresentacao').val(ui.item.apresentacao);
			    $('#custo').val(ui.item.custo);
    			$('#qtd').focus();
    			$('#busca-itemav').val('');
    			return false;
		},
		extraParams: {
 			tipo: 2
		}
});
    
    /*$("#todos_est").click(function(){
    	if($(this).is(':checked')){
    		$('#busca-item-est').val('');
    		$('#busca-item-est').attr('readonly','readonly');
    		$('#busca-item-est').removeClass('OBG');
    		$('#busca-item-est').removeAttr('style');
    		
    	}else{
    		$('#busca-item-est').attr('readonly',false);
    		$('#busca-item-est').addClass('OBG');
    	}
    });*/

$("#todos_est").click(function(){
	$("#res-b").remove();
	$("#salvar_est").remove();
	if($(this).is(':checked')){
		$('#busca-item-est').val('');
		$('#busca-item-est').attr('readonly','readonly');
		$('#busca-item-est').removeClass('OBG');
		$('#busca-item-est').removeAttr('style');
		
	}else{
		$('#busca-item-est').attr('readonly',false);
		$('#busca-item-est').addClass('OBG');
	}
});



	$("#pesquisa-estoque").live('click', function(){
		if(validar_campos('estoque')){
			var ur = $("#ur").val();
			var cod = $("#nome_item").val();
			var tipo = $("#sel_estoque").val();
			var maiorQueZero = $("#maior_que_zero").val();
			if($("#todos_est").is(':checked')){
				var todos= 1;
			}else{
				var todos= 0;
			}

			$.post("query.php",
				{
					query: 'estoque',
					ur: ur,
					cod: cod,
					tipo: tipo,
					todos: todos,
					maiorQueZero:maiorQueZero
				},
				function(r){
					$("#est").html(r);

					$(".valor").priceFormat({
						prefix: '',
						centsSeparator: ',',
						thousandsSeparator: '.'
					});
				}
			);
		}
	});

	$('#exportar-excel-estoque').live('click',function(){

		var ur = $(this).attr('data-ur');
		var tipo = $(this).attr('data-tipo');
		var cod = $(this).attr('data-cod');
		var todos = $(this).attr('data-todos');
		var maiorQueZero = $(this).attr('data-maiorQueZero');


		window.open('exportarExcelEstoque.php?ur='+ur+'&tipo='+tipo+
			'&cod='+cod+'&todos='+todos+'&maiorQueZero='+maiorQueZero);
		// window.open('exportar_fatura_custo.php?id='+id+'&av='+av+'&zerados=1','_blank');
	});
///jeferson
$("#salvar-envio-pedido").click(function() {
     itens='';
     
      if($('#excluir-total').is(':checked')){
        if(validar_campos("div-justificativa-excluir-total")){
            if (confirm('Deseja cancelar todo o Pedido?')) {
                
                  $(".excluir-dados").each(function(i){
                       
  			var id_item = $(this).attr("id_item");
                        
                        itens += id_item+"#";
                        
                  });
                  
                 var justificativa = $('#text-justificatica-excluir-total').val();
                  $.post("query.php",{query: "excluir-pedido-total", dados: itens,
                      justificativa:justificativa,id_pedido:$("#salvar-envio-pedido").attr('id_pedido')},function(r){
                      
                      if(r == 1){
                          alert('Cancelado com sucesso.');
                          window.location = '?op=saida2&act=pendentes&nocache=' + (new Date()).getTime();
                          
                      }else{
                          alert(r);
                      }
  				
  			});
            }
            return false;
            
        }else{
        return false;
        } 
      }else{
    
	if(validar_campos("div-envio-ur")){
	var total1 = $(".dados").size();
	var dados = "";
	var data = $("#data-envio").val();
	var id_pedido = $(this).attr("id_pedido");
	var empresa = $(this).attr("emp");
	var atual = "";
	var cont = 0;
	
	
	$(".dados").each(function(i) {
		
		var quantidade = $(this).children('input').val();
		var lote = $(this).parent().children('td').eq(3).children('input').val();
		var id_item = $(this).attr("id_item");
		var tot = $(this).attr("total");
		var tiss = $(this).attr("tiss");
		var tipo = $(this).attr("tipo");
		var n = $(this).attr("n");
		var sn = $(this).attr("sn");
		var catalogo_id = $(this).attr("catalogo_id");
		
		if(quantidade > 0 && quantidade != "" && quantidade != null && quantidade != undefined ){
		dados += quantidade+"#"+id_item+"#"+tot+"#"+tiss+"#"+lote+"#"+tipo+"#"+n+"#"+sn+"#"+catalogo_id;
		if(i<total1 - 1) dados+= "$";
		cont++;
		}
				
	});
	if(cont >0){
	$.post("queryPedidoInterno.php", {query:'salvar-envio-pedido', dados:dados, id:id_pedido,data:data,empresa:empresa}, function(r) {
		
		//if(r >0){
		            $("#pre-relatorio-pedido").html(r);
  					$("#relatorio-pedido").val(r);
  					$("#dialog-relatorio-pedido").dialog("open");
			//window.location = "/logistica/";
	//	}else{
			//alert("Falha na gravação! Verifique os campos.");
			//alert(r);
		//}
			return false;
	});
	}
        }
  }
	return false;
	
});

$(".radio-controlado").click(function(){
	
	var p = $(this).val();
	
	if(p == 1){
		$("#select-controlado").show();
	}else{
		$("#select-controlado").hide();
	}
	
});
	$(".radio-antibiotico").click(function(){

		var p = $(this).val();

		if(p == 1){
			$("#select-antibiotico").show();
		}else{
			$("#select-antibiotico").hide();
		}

	});

$("#salvar-entrada-estoque-pedido").click(function() {
    
    if(validar_campos("div-envio-paciente")){
	
	var total1 = $(".dados").size();
	var dados = "";
	//var data = $("#data-envio").val();
	var id_pedido = $(this).attr("id_pedido");
	var empresa = $(this).attr("emp");
	var atual = "";
	var cont = 0;
	
	
	$(".dados").each(function(i) {
		
		var quantidade =  $(this).children('input').val();//qtd-envio
		var lote =$(this).parent().children('td').eq(4).children('input').val();
		var id_item = $(this).attr("id_item");
		var tot = $(this).attr("total");
		var tiss = $(this).attr("tiss");
		var catalogo_id = $(this).attr("catalogo_id");
        var tipo= $(this).attr("tipo");
		var saida_id = $(this).attr("saida_id");
		var saida_quantidade= $(this).attr("saida_quantidade");
		var saida_confirmados = $(this).attr("saida_confirmados");
		
		dados += quantidade+"#"+id_item+"#"+tot+"#"+tiss+"#"+lote+"#"+catalogo_id+"#"+tipo+"#"+saida_id+"#"+saida_quantidade+"#"+saida_confirmados;
		if(i<total1 - 1) dados+= "$";
		
				
	});
	
	$.post("queryPedidoInterno.php", {query:'salvar-entrada-estoque', dados:dados, id:id_pedido,empresa:empresa}, function(r) {
		
		if(r != 1){
			alert("Falha na gravação! Verifique os campos.");
		}else{
			alert ("Salvo com sucesso!!");
			window.location = "/logistica/";
		}
		
	});
    }
	return false;
    
	
});

//Enviar Pedidos - Ana Paula
$("#salvar-pedido").click(function() {
	
	if(validar_campos('div-envio-paciente')){
	 var data_max = $("input[name='data-maxima']").get(0).value; 
	  data_max1 = data_max.split("/").reverse().join("");
	 data_at = $("#data_atual").val();
		if((data_max1 - data_at) == 0) {
			alert("ERRO: A data não pode ter menos de 24 Horas!");
			$("input[name='data-maxima']").attr("style", "border: 3px solid red;");
			return false;
			}else{
			$("input[name='data-maxima']").removeAttr("style");
			}
	var total = $(".dados").size();
	var dados = "";
	//var data = $("#data-envio").val();
	var observacao = $("#obs").val();
	var atual = "";
	var cont = 0;
	
	$(".dados").each(function(i) {
		
		var quantidade = $(this).children('input').val();
		var emergencia = $(this).parent().children('td').eq(3).children('input').val();
		
		var tipo = $(this).attr("tipo");
		var codigo = $(this).attr("cod");
		var catalogo_id = $(this).attr("catalogo_id");
		if(quantidade >0){
		dados += quantidade+"#"+tipo+"#"+codigo+"#"+emergencia+"#"+catalogo_id;
		if(i<total - 1) dados+= "$";
		cont++;
		}	
	});
	if(cont >0){
	$.post("query.php", {query:'salvar-pedido', dados:dados, observacao:observacao,data_max:data_max}, function(r) {
		
		if(r == 1){
			alert("Falha na gravação! Verifique os campos.");
		}else{
                       // window.open( "imprimir_pedido_ur.php?p="+r,'_blank');
			alert("Pedido salvo com sucesso. Lembre de confirmar o pedido na aba Gerenciar Pedidos");
			window.location = "/logistica/?op=saida2&act=pendentes";
		}
		
	});
	
	}else{
		alert("Falha na gravação! Verifique os campos.");
		
	}
	}
	return false;
	
});
    
    $("#pesquisa-brasindice").click(function(){

    	pesquisa_brasindice();
    	
    });
    
    function pesquisa_brasindice() {
    	var ur = $("#ur").val();
		var tipo = $("#tipo_brasindice").val();
		var item = $("input[name='item_busca_brasindice']").val();
		var status  = $("#status_item").val();
		//$_SESSION['item_busca_brasindice'] = item;
		    		 
		$.post("query.php",{query:'brasindice',ur:ur,item:item,tipo:tipo, status:status},function(r){
                $("#brasindice_resultado").html(r);    		
		});
		
	}//pesquisa_brasindice
    
    $("#novo-brasindice").click(function() {
	
    	window.location = "?op=brasindice&act=cadastrar";
    	
	});
    
    $("#brasindice_salvar").click(function() {
    	
    	if (validar_campos("div-novo-brasindice")) {
                var principio = $("#principio").val();
                var apresentacao = $("#apresentacao").val();
                var laboratorio = $("#laboratorio").val();
                var numero_tiss = $("#numero_tiss").val();
                var numero_tuss = $("#numero_tuss").val();
                var nome_simpro_brasindice = $("#nome-simpro-brasindice").val();
                var tipo_brasindice = $("#tipo_brasindice_cadastro").val();
                var tag_brasindice = $("#tags").val();
                var status = $("#status").val();
                var usuario = $("#usuario").val();
                var data = $("#data").val();
                var controlado = $("#controlado").val();
                var antibiotico = $("#antibiotico").val();
                var referencia = $("#referencia").val();
                var id_simpro_brasindice = $("#id-simpro-brasindice").val();
			    var id_principio = $("#id-principio").val();
				var generico = $("#generico").val();
				var possui_generico = $("#possui-generico").val();
            	var categoria_material = $("#categoria-material").val();
			if(tipo_brasindice == 0 && (id_principio < 0 || id_principio == null || id_principio == '')){
				alert('Selecione um princípio na lista. Caso o princpio não esteja na lista´você deve inseri-lo antes de cadastrar o produto. ');
				return false;
			}

        	$.post("query.php",{query:'novo_brasindice',principio:principio,apresentacao:apresentacao,laboratorio:laboratorio, 
        		numero_tiss:numero_tiss, nome_simpro_brasindice:nome_simpro_brasindice,tipo_brasindice:tipo_brasindice, tag_brasindice:tag_brasindice, status:status,
        		usuario:usuario, data:data,controlado:controlado,antibiotico:antibiotico,referencia:referencia,id_simpro_brasindice:id_simpro_brasindice,
				id_principio:id_principio,numero_tuss:numero_tuss, generico:generico, categoria_material:categoria_material, possui_generico:possui_generico},function(r){
        			
        			if(r == 0){
        				window.location = "/logistica/?op=brasindice&act=buscar"
        			}else{
               alert(r)
        				//alert("Falha na gravação! Verifique os campos.");
        			}
        			
           });
        	
		}    	
    	return false;
    	
	});
    
    
  $("#emergencia").click(function(){
	  if($(this).is(':checked')){
		  $(".emergencia").each(function(){
			  $(".emergencia").attr('checked','checked');
			  $(".emergencia").val('1');
		  });
	  }else{
		  $(".emergencia").each(function(){
			  $(".emergencia").attr('checked',false);
			  $(".emergencia").val('0');
		  });
	  }
  });
  $(".emergencia").click(function(){
	  if($(this).is(':checked')){
		  $(this).val('1');
	  }else{
		  $(this).val('0');
	  }
  });
   
    
$("#brasindice_editar").click(function() {
    	
    	if (validar_campos("div-editar-brasindice")) {
    		var principio = $("#principio").val();
			var principio_antigo = $("#id-principio").attr('principio-antigo');
			var descontinuado = $("#descontinuado").val();

        	var apresentacao = $("#apresentacao").val();
        	var laboratorio = $("#laboratorio").val();
        	var numero_tiss = $("#numero_tiss").val();
        	var tipo_brasindice = $("#tipo_brasindice_cadastro").val();
        	var tag_brasindice = $("#tags").val();
        	var status = $("#status").val();
        	var usuario = $("#usuario").val();
        	var data = $("#data").val();
        	var controlado = $("#controlado").val();
		var antibiotico = $("#antibiotico").val();
		var catalogo_id=$("#catalogo_id").val();
		var referencia=$("#referencia").val();
                var nome_simpro = $("#nome_simpro").val();
                var numero_tuss = $("#numero_tuss").val();
			var id_principio = $("#id-principio").val();
			var generico = $("#generico").val();
			var possui_generico = $("#possui-generico").val();
			var categoria_material = $("#categoria-material").val();
			if(tipo_brasindice == 0 && (id_principio < 0 || id_principio == null || id_principio == '')){
				alert('Selecione um princípio na lista. Caso o princpio não esteja na lista´você deve inseri-lo antes de cadastrar o produto. ');
				return false;
			}
        	
        	$.post("query.php",{query:'editar_brasindice',principio:principio,apresentacao:apresentacao,laboratorio:laboratorio, 
        		numero_tiss:numero_tiss, tipo_brasindice:tipo_brasindice, tag_brasindice:tag_brasindice, status:status,
        		usuario:usuario, data:data, controlado:controlado,antibiotico:antibiotico,catalogo_id:catalogo_id,referencia:referencia,
                        nome_simpro:nome_simpro, numero_tuss:numero_tuss,id_principio:id_principio,
				generico:generico,principio_antigo:principio_antigo, descontinuado:descontinuado, categoria_material:categoria_material,
				possui_generico:possui_generico
                    },function(r){
        			
        			if(r == 1){
					alert('Alterado com sucesso.');
        				window.location = "/logistica/?op=brasindice&act=buscar"
        			}else{
					
        				alert("Falha na gravação! Verifique os campos.");
        			}
        			
        	});
		}
    	 
    	return false;
    	
	});
    
///////////////////////////////////////////////////////    
   $("#editar_nota").click(function(){
  
    	var notaVal  = $("#nota").attr("cod");
    	var valor= $("#nota").attr("valor");
    	var codFornecedor= $("#nota").attr("fornecedor");
    	
    	var descricao =$("#nota").attr("desc");
    	var val_produtos= $("#nota").attr("val_produtos");
    	var icms= $("#nota").attr("icms");
    	var frete = $("#nota").attr("frete");
    	var seguro = $("#nota").attr("seguro");
    	var ipi= $("#nota").attr("ipi");
    	var issqn= $("#nota").attr("issqn");
    	var des_acessorias= $("#nota").attr("des_acessorias");
    	var des_bon= $("#nota").attr("des_bon");
    
    	 $("#div-nova-entrada1").dialog("open");
    	
    	/*val_produtos = float2moeda(val_produtos);
    	 icms = float2moeda(icms);
    	 frete = float2moeda(frete);
    	 seguro = float2moeda(seguro);
    	 ipi = float2moeda(ipi);
    	 issqn = float2moeda(issqn);
    	 des_acessorias = float2moeda(des_acessorias);
    	 des_bon = float2moeda(des_bon);
    	 */
    	 			
			$("input[name='nota']").val(notaVal);
			$("input[name='valor_nota']").val(valor);
	    	$("input[name='descricao']").val(descricao);
	    	$("select[name='codFornecedor']").val(codFornecedor);
	   
	    	$("input[name='val_produtos']").val(val_produtos);
	    	$("input[name='icms']").val(icms);
	        $("input[name='frete']").val(frete);
	    	$("input[name='seguro']").val(seguro);
	    	$("input[name='ipi']").val(ipi);
	    	$("input[name='issqn']").val(issqn);
	    	$("input[name='des_acessorias']").val(des_acessorias);
	    	$("input[name='des_bon']").val(des_bon);
	    	
	    	
    	
    /*	$.post("entrada.php",{op:'entrada_nota', nota:notaVal,valor:valor,codFornecedor:codFornecedor,descricao:descricao,val_produtos:val_produtos,icms:icms,frete:frete,
    		seguro:seguro,ipi:ipi,issqn:issqn,des_acessorias:des_acessorias, des_bon:des_bon},function(r){
    			
    				$("#edicao").html(r);				
    				$("input[name='nota']").val(notaVal);
    				$("input[name='valor']").val(valor);
    		    	$("input[name='descricao']").val(descricao);
    		    	$("input[name='val_produtos']").val(val_produtos);
    		    	$("input[name='icms']").val(icms);
    		    	
    		    	$("input[name='frete']").val(frete);
    		    	$("input[name='seguro']").val(seguro);
    		    	$("input[name='ipi']").val(ipi);
    		    	$("input[name='issqn']").val(issqn);
    		    	$("input[name='des_acessorias']").val(des_acessorias);
    		    	$("input[name='des_bon']").val(des_bon);
    		    	
    		    	
    		    	
    		    	
    		    	
    		});*/
	  
    	
    	
    	
});
   
   $("#data-maxima").change(function(){
	
	  var data_max = $("input[name='data-maxima']").get(0).value; 
	  data_max = data_max.split("/").reverse().join("");
	  
	 data_at = $("#data_atual").val();
		if((data_max - data_at) == 0) {
			alert("ERRO: A data n&atilde;o pode ter menos de 24 Horas!");
			$("input[name='data-maxima']").attr("style", "border: 3px solid red;");
			}else{
			$("input[name='data-maxima']").removeAttr("style");
			}
	 
	   
   });
 
   $("#div-xml-nota").dialog({
    	autoOpen: false, modal: true, width: 600, position: 'top',
    	open: function(event,ui){
    		
    	},
    	buttons: {
    		"Fechar" : function(){
    			$(this).dialog("close");
    		}
    	}
    });
   
   $("#xml_nota").click(function(){
	   $("#div-xml-nota").dialog("open");
	   
   });

   
   $("#div-nova-entrada1").dialog({
     	autoOpen: false, modal: true, width: 900, position: 'top',
     	open: function(event,ui){
     		
     	},
     	buttons: {
     		"Fechar" : function(){
     			$(this).dialog("close");
     		}
     	}
     });
 
   $("#novaentrada1").click(function() {
	   var produto= $("input[name='val_produtos']").val();
		 // produto = parseFloat(produto.replace('.','').replace(',','.'));
		  $("input[name='val_produtos']").val(produto);
		  
		  var icms= verificaNaN($("input[name='icms']").val());
		  //icms = parseFloat(icms.replace('.','').replace(',','.'));
		  $("input[name='icms']").val(icms);
		  
		  var frete= verificaNaN($("input[name='frete']").val());
		 // frete = parseFloat(frete.replace('.','').replace(',','.'));
		  $("input[name='frete']").val(frete);
		  
		  
		  var seguro= verificaNaN($("input[name='seguro']").val());
		 // seguro = parseFloat(seguro.replace('.','').replace(',','.'));
		  $("input[name='seguro']").val(seguro);
		  
		  var ipi= verificaNaN($("input[name='ipi']").val());
		 // ipi= parseFloat(ipi.replace('.','').replace(',','.'));
		  $("input[name='ipi']").val(ipi);
		  
		  var issqn= verificaNaN($("input[name='issqn']").val());
		  //issqn= parseFloat(issqn.replace('.','').replace(',','.'));
		  $("input[name='issqn']").val(issqn);
		  
		  var des_acessorias= verificaNaN($("input[name='des_acessorias']").val());
		  //des_acessorias= parseFloat(des_acessorias.replace('.','').replace(',','.'));
		  $("input[name='des_acessorias']").val(des_acessorias);
		  
		  var des_bon = verificaNaN($("input[name='des_bon']").val());
		  //des_bon= parseFloat(des_bon.replace('.','').replace(',','.'));
		  $("input[name='des_bon']").val(des_bon);
		  
		  var valor = verificaNaN($("#valor_nota").val());
		  valor= parseFloat(valor.replace('.','').replace(',','.'));

	   $("#nota").attr("cod",$("input[name='nota']").val());
	   $("#nota").attr("valor",$("input[name='valor_nota']").val());
   	   $("#nota").attr("fornecedor",$("select[name='codFornecedor']").val());   	
   	   $("#nota").attr("descricao",$("input[name='descricao']").val());
       $("#nota").attr("val_produtos",	produto);
       $("#nota").attr("icms",icms);
       $("#nota").attr("frete",frete);
       $("#nota").attr("seguro",seguro);
   	   $("#nota").attr("ipi",ipi);
       $("#nota").attr("issqn",issqn);
       $("#nota").attr("des_acessorias",des_acessorias);
       $("#nota").attr("des_bon",des_bon);
      
       $("#vnota").html($("input[name='val_produtos']").val());
       $("#codnota").html($("input[name='nota']").val());
      
       $("#div-nova-entrada1").dialog("close");
   });  
   
 function verificaNaN (param){
 	if(param=='NaN' ||  param==null || param==''){
    		param = '0,00';
    	return param;
    	}else {
    		
    	return param;}
    	
    	
    };
    
    
   
    $('.data').datepicker('option','minDate',$("#tabela-itens").attr("inicio"));
    $('.data').datepicker('option','maxDate',$("#tabela-itens").attr("fim"));
    
    $(".data").datepicker({
  	defaultDate: "+1w",
  	changeMonth: true,
  	numberOfMonths: 1
    });
    
/******************** INICIALIZACOES E FUNCOES ***************************/
  $.ajaxSetup({cache: false});
   
   $(".valor").priceFormat({
    prefix: '',
    centsSeparator: ',',
    thousandsSeparator: '.'
  });
   
   $(".val").priceFormat({
	    prefix: '',
	    centsSeparator: '.',
	    thousandsSeparator: ''
	  });
  
  $(".data").datepicker({
    inline: true,
    changeMonth: true,
    changeYear: true
  });

	$(".data-devolucao").datepicker({
		inline: true,
		changeMonth: true,
		changeYear: true,
		maxDate: 0,
		minDate:-3

	});


  
  var dates = $( "#inicio, #fim" ).datepicker({
	defaultDate: "+1w",
	changeMonth: true,
	numberOfMonths: 3,
	onSelect: function( selectedDate ) {
		var option = this.id == "inicio" ? "minDate" : "maxDate",
		instance = $( this ).data( "datepicker" );
		date = $.datepicker.parseDate(
		instance.settings.dateFormat ||
		$.datepicker._defaults.dateFormat,
		selectedDate, instance.settings );
		dates.not( this ).datepicker( "option", option, date );
	}
  });
  
  $('.numerico').keyup(function(e){
    this.value = this.value.replace(/\D/g,'');
  });

    //$('.envio-equipamento-unico').mask(/[1]/g);
    $('.envio-equipamento-unico').keyup(function(e){
        var x = $(this).val();
        if(x != 1){
            this.value= this.value.replace(x,'');
        }
    });
  
    $(".div-aviso").hide();
  function validar_campos(div){
    var ok = true;
    $("#"+div+" .OBG").each(function (i){
      if(this.value == ""){
		ok = false;
		this.style.border = "3px solid red";
      } else {
		this.style.border = "";
      }
    });
    $("#"+div+" .COMBO_OBG option:selected").each(function (i){
      if(this.value == "-1"){
		ok = false;
		$(this).parent().css({"border":"3px solid red"});
      } else {
		$(this).parent().css({"border":""});
      }
    });

    if(!ok){
      $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
      $("#"+div+" .div-aviso").show();
    } else {
      $("#"+div+" .aviso").text("");
      $("#"+div+" .div-aviso").hide();
    }
    return ok;
  }
  
  $("#bonificados-itens").easyTooltip({
  	xOffset: -200,
  	yOffset: 20,
    content: "<h4>BONIFICAR TODOS</h4>"
  });
  
 ///Bot�o lan�ar produtos///
  
  $("#novaentrada").hide();
  $("#novaentrada1").hide();
  
/*########################AUTO SOMA##############################*/
   
  $("#icms").blur(function(){
	  
	  var icms   = verificaNaN($("input[name='icms']").val());
	  var frete  = verificaNaN($("input[name='frete']").val());
	  var seguro = verificaNaN($("input[name='seguro']").val());
	  var produtos = verificaNaN($("input[name='val_produtos']").val());
	  var des_acessorias = verificaNaN($("input[name='des_acessorias']").val());
	  var ipi = verificaNaN($("input[name='ipi']").val());
	  var issqn = verificaNaN($("input[name='issqn']").val());
	
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
	 
	  
	  	 
	  icms = parseFloat(icms.replace('.','').replace(',','.'));
	  
	  frete = parseFloat(frete.replace('.','').replace(',','.'));
	  seguro = parseFloat(seguro.replace('.','').replace(',','.'));
	  produtos = parseFloat(produtos.replace('.','').replace(',','.'));
	  
	  ipi = parseFloat(ipi.replace('.','').replace(',','.'));
	  issqn = parseFloat(issqn.replace('.','').replace(',','.'));
	  des_acessorias = parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  des_bon = parseFloat(des_bon.replace('.','').replace(',','.'));
	  var tot = icms+frete+seguro+produtos+ipi+issqn+des_acessorias-des_bon;
	 
	  if(tot >=0){
		  $("#novaentrada").show();
		  $("#novaentrada1").show();
		  
	  }else{
		  $("#novaentrada").hide();
		  $("#novaentrada1").hide();
	  }
	 
	  tot = float2moeda(tot);
	  $("input[name='valor_nota']").val(tot);
  });
  
  $("#frete").blur(function(){
	  var icms   = verificaNaN($("input[name='icms']").val());
	  var frete  = verificaNaN($("input[name='frete']").val());
	  var seguro = verificaNaN($("input[name='seguro']").val());
	  var produtos = verificaNaN($("input[name='val_produtos']").val());
	  var des_acessorias = verificaNaN($("input[name='des_acessorias']").val());
	  var ipi = verificaNaN($("input[name='ipi']").val());
	  var issqn = verificaNaN($("input[name='issqn']").val());
	
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
      icms     = parseFloat(icms.replace('.','').replace(',','.'));
	  frete    = parseFloat(frete.replace('.','').replace(',','.'));
	  seguro   = parseFloat(seguro.replace('.','').replace(',','.'));
	  produtos = parseFloat(produtos.replace('.','').replace(',','.'));
	  ipi      = parseFloat(ipi.replace('.','').replace(',','.'));
	  issqn    = parseFloat(issqn.replace('.','').replace(',','.'));
	  des_acessorias = parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  des_bon  = parseFloat(des_bon.replace('.','').replace(',','.'));
	 
	  var tot = icms+frete+seguro+produtos+ipi+issqn+des_acessorias-des_bon;
	  
	  if(tot >=0){
		  $("#novaentrada").show();
		  $("#novaentrada1").show();
	  }else{
		  $("#novaentrada").hide();
		  $("#novaentrada1").hide();
	  }
	  
tot = float2moeda(tot);
	  
	  $("input[name='valor_nota']").val(tot);
  });
  
  
$("#seguro").blur(function(){
	  
	  var icms   = verificaNaN($("input[name='icms']").val());
	  var frete  = verificaNaN($("input[name='frete']").val());
	  var seguro = verificaNaN($("input[name='seguro']").val());
	  var produtos = verificaNaN($("input[name='val_produtos']").val());
	  var des_acessorias = verificaNaN($("input[name='des_acessorias']").val());
	  var ipi = verificaNaN($("input[name='ipi']").val());
	  var issqn = verificaNaN($("input[name='issqn']").val());
	
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
	 
	  
	  	 
	  icms = parseFloat(icms.replace('.','').replace(',','.'));
	  
	  frete = parseFloat(frete.replace('.','').replace(',','.'));
	  seguro = parseFloat(seguro.replace('.','').replace(',','.'));
	  produtos = parseFloat(produtos.replace('.','').replace(',','.'));
	  
	  ipi = parseFloat(ipi.replace('.','').replace(',','.'));
	  issqn = parseFloat(issqn.replace('.','').replace(',','.'));
	  des_acessorias = parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  des_bon = parseFloat(des_bon.replace('.','').replace(',','.'));
	  
	  
	  
	  var tot = icms+frete+seguro+produtos+ipi+issqn+des_acessorias-des_bon;
	 
	  if(tot >=0){
		  $("#novaentrada").show();
		  $("#novaentrada1").show();
	  }else{
		  $("#novaentrada").hide();
		  $("#novaentrada1").hide();
	  }
	  
	  tot = float2moeda(tot);
	  $("input[name='valor_nota']").val(tot);
  });
  
$("#val_produtos").blur(function(){
	  
	  var icms   = verificaNaN($("input[name='icms']").val());
	  var frete  = verificaNaN($("input[name='frete']").val());
	  var seguro = verificaNaN($("input[name='seguro']").val());
	  var produtos = verificaNaN($("input[name='val_produtos']").val());
	  var des_acessorias = verificaNaN($("input[name='des_acessorias']").val());
	  var ipi = verificaNaN($("input[name='ipi']").val());
	  var issqn = verificaNaN($("input[name='issqn']").val());
	
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
	 
	  
	  	 
	  icms = parseFloat(icms.replace('.','').replace(',','.'));
	  
	  frete = parseFloat(frete.replace('.','').replace(',','.'));
	  seguro = parseFloat(seguro.replace('.','').replace(',','.'));
	  produtos = parseFloat(produtos.replace('.','').replace(',','.'));
	  
	  ipi = parseFloat(ipi.replace('.','').replace(',','.'));
	  issqn = parseFloat(issqn.replace('.','').replace(',','.'));
	  des_acessorias = parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  des_bon = parseFloat(des_bon.replace('.','').replace(',','.'));
	  
	  
	  
	  var tot = icms+frete+seguro+produtos+ipi+issqn+des_acessorias-des_bon;
	 
	  
	  if(tot >=0){
		  $("#novaentrada").show();
		  $("#novaentrada1").show();
	  }else{
		  $("#novaentrada").hide();
		  $("#novaentrada1").hide();
	  }
	  
	  tot = float2moeda(tot);
	  $("input[name='valor_nota']").val(tot);
});

$("#ipi").blur(function(){
	  var icms   = verificaNaN($("input[name='icms']").val());
	  var frete  = verificaNaN($("input[name='frete']").val());
	  var seguro = verificaNaN($("input[name='seguro']").val());
	  var produtos = verificaNaN($("input[name='val_produtos']").val());
	  var des_acessorias = verificaNaN($("input[name='des_acessorias']").val());
	  var ipi = verificaNaN($("input[name='ipi']").val());
	  var issqn = verificaNaN($("input[name='issqn']").val());
	
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
	 
	  	 
	  icms = parseFloat(icms.replace('.','').replace(',','.'));
	  
	  frete = parseFloat(frete.replace('.','').replace(',','.'));
	  seguro = parseFloat(seguro.replace('.','').replace(',','.'));
	  produtos = parseFloat(produtos.replace('.','').replace(',','.'));
	  
	  ipi = parseFloat(ipi.replace('.','').replace(',','.'));
	  issqn = parseFloat(issqn.replace('.','').replace(',','.'));
	  des_acessorias = parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  des_bon = parseFloat(des_bon.replace('.','').replace(',','.'));
	  
	  
	  
	  var tot = icms+frete+seguro+produtos+ipi+issqn+des_acessorias-des_bon;
	  
	  
	  if(tot >=0){
		  $("#novaentrada").show();
		  $("#novaentrada1").show();
	  }else{
		  $("#novaentrada").hide();
		  $("#novaentrada1").hide();
	  }
	  
	  tot = float2moeda(tot);
	  $("input[name='valor_nota']").val(tot);
});

$("#issqn").blur(function(){
	  
	  var icms   = verificaNaN($("input[name='icms']").val());
	  var frete  = verificaNaN($("input[name='frete']").val());
	  var seguro = verificaNaN($("input[name='seguro']").val());
	  var produtos = verificaNaN($("input[name='val_produtos']").val());
	  var des_acessorias = verificaNaN($("input[name='des_acessorias']").val());
	  var ipi = verificaNaN($("input[name='ipi']").val());
	  var issqn = verificaNaN($("input[name='issqn']").val());
	
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
	 
	  	 
	  icms = parseFloat(icms.replace('.','').replace(',','.'));
	  
	  frete = parseFloat(frete.replace('.','').replace(',','.'));
	  seguro = parseFloat(seguro.replace('.','').replace(',','.'));
	  produtos = parseFloat(produtos.replace('.','').replace(',','.'));
	  
	  ipi = parseFloat(ipi.replace('.','').replace(',','.'));
	  issqn = parseFloat(issqn.replace('.','').replace(',','.'));
	  des_acessorias = parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  des_bon = parseFloat(des_bon.replace('.','').replace(',','.'));
	  
	  var tot = icms+frete+seguro+produtos+ipi+issqn+des_acessorias-des_bon;
	  
	  if(tot >=0){
		  $("#novaentrada").show();
		  $("#novaentrada1").show();
	  }else{
		  $("#novaentrada").hide();
		  $("#novaentrada1").hide();
	  }
	  
	  tot = float2moeda(tot);
	  $("input[name='valor_nota']").val(tot);
});

$("#des_bon").blur(function(){
	  
	  var icms   = verificaNaN($("input[name='icms']").val());
	  var frete  = verificaNaN($("input[name='frete']").val());
	  var seguro = verificaNaN($("input[name='seguro']").val());
	  var produtos = verificaNaN($("input[name='val_produtos']").val());
	  var des_acessorias = verificaNaN($("input[name='des_acessorias']").val());
	  var ipi = verificaNaN($("input[name='ipi']").val());
	  var issqn = verificaNaN($("input[name='issqn']").val());
	
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
	 
	  
	  	 
	  icms = parseFloat(icms.replace('.','').replace(',','.'));
	  
	  frete = parseFloat(frete.replace('.','').replace(',','.'));
	  seguro = parseFloat(seguro.replace('.','').replace(',','.'));
	  produtos = parseFloat(produtos.replace('.','').replace(',','.'));
	  
	  ipi = parseFloat(ipi.replace('.','').replace(',','.'));
	  issqn = parseFloat(issqn.replace('.','').replace(',','.'));
	  des_acessorias = parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  des_bon = parseFloat(des_bon.replace('.','').replace(',','.'));
	  
	  
	  
	  var tot = icms+frete+seguro+produtos+ipi+issqn+des_acessorias-des_bon;
	  if(tot >=0){
		  $("#novaentrada").show();
		  $("#novaentrada1").show();
	  }else{
		  $("#novaentrada").hide();
		  $("#novaentrada1").hide();
	  }
	  
	  tot = float2moeda(tot);
	  $("input[name='valor_nota']").val(tot);
	  
});

$("#des_acessorias").blur(function(){
	  
	  var icms   = verificaNaN($("input[name='icms']").val());
	  var frete  = verificaNaN($("input[name='frete']").val());
	  var seguro = verificaNaN($("input[name='seguro']").val());
	  var produtos = verificaNaN($("input[name='val_produtos']").val());
	  var des_acessorias = verificaNaN($("input[name='des_acessorias']").val());
	  var ipi = verificaNaN($("input[name='ipi']").val());
	  var issqn = verificaNaN($("input[name='issqn']").val());
	
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
	 
	  
	  	 
	  icms = parseFloat(icms.replace('.','').replace(',','.'));
	  
	  frete = parseFloat(frete.replace('.','').replace(',','.'));
	  seguro = parseFloat(seguro.replace('.','').replace(',','.'));
	  produtos = parseFloat(produtos.replace('.','').replace(',','.'));
	  
	  ipi = parseFloat(ipi.replace('.','').replace(',','.'));
	  issqn = parseFloat(issqn.replace('.','').replace(',','.'));
	  des_acessorias = parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  des_bon = parseFloat(des_bon.replace('.','').replace(',','.'));
	  
	  
	  var tot = icms+frete+seguro+produtos+ipi+issqn+des_acessorias-des_bon;
	  
	  
	  if(tot >=0){
		  $("#novaentrada").show();
		  $("#novaentrada1").show();
	  }else{
		  $("#novaentrada").hide();
		  $("#novaentrada1").hide();
	  }
	  
	  tot = float2moeda(tot);
	  
	  $("input[name='valor_nota']").val(tot);
	 
});
  

  
  
/*************** SAIDA : ENVIO PARA O PACIENTE *****************/
  $("#cancelar-item").dialog({
  	autoOpen: false, modal: true, width: 800, position: 'top',
  	open: function(event,ui){
  		$("#cancelar-item .aviso").text("");
     	$("#cancelar-item .div-aviso").hide();
  		$("#cancelar-item input[name='justificativa']").val('');
  		$("#cancelar-item input[name='justificativa']").css({"border":""});
  	    $("#cancelar-item input[name='justificativa']").focus();
  	},
  	buttons: {
  		"Cancelar" : function(){
  			$(this).dialog("close");
  		},
  		"Salvar" : function(){
  			if(validar_campos('cancelar-item')){
  				var just = $("#cancelar-item input[name='justificativa']").val();
  				var sol = $("#cancelar-item").attr("sol");
  				$.post("query.php",{query: "cancelar-item-solicitacao", sol: sol, just: just},function(r){
  					if(r == '1'){
  						alert('Cancelamento salvo com sucesso!');
  						window.location.reload();
  					}
  				});
  				$(this).dialog("close");
  			}
  		}
  	}
  });
  
  $(".cancela-item").click(function(){
  	$("#cancelar-item").attr("sol",$(this).attr("sol"));
  	$("#cancelar-item").dialog("open");
  });
  
	$('.fracionar').live("click", function () {
		var $this = $(this), $tr = $this.parent().parent(),
			indice = $tr.index()+1, dados = $tr.find('td.dados'), bgcolor = $tr.attr('bgcolor') || '';

		var tdDados =
			'<td class="dados" ' +
			'	autorizado="' + dados.attr('autorizado') + '" ' +
			'	sol="' + dados.attr('sol') + '" ' +
			'	qtd="' + dados.attr('qtd') + '" ' +
			'	cod="' + dados.attr('cod') + '" ' +
			'	catalogo_id="' + dados.attr('catalogo_id') + '" ' +
			'	n="' + dados.attr('n') + '" ' +
			'	sn="' + dados.attr('sn') + '" ' +
			'	soldata="' + dados.attr('soldata') + '" ' +
			'	env="' + dados.attr('env') + '" ' +
			'	total="" ' +
			'	tipo="' + dados.attr('tipo') + '">' +
			'	<input type="text" name="qtd-envio" class="numerico quantidade" style="text-align:right" size="4" autocomplete="off">' +
			'</td>';
		var background = 'bgcolor="' + bgcolor + '"';
		$(".mytable tr").eq(indice).after(
			'<tr ' + background + '>' +
			'	<td><img src="/utils/delete_16x16.png" width="16" class="remover-linha-fracionar" style="cursor: pointer;" title="Remover fracionamento"></td>' +
			'	<td></td>' +
			tdDados +
			'<td></td>	<td><input type="text" name="lote" style="text-align:right" size="8" class="lote"></td>' +
			'</tr>'
		);
		$('.numerico').keyup(function(e){
			this.value = this.value.replace(/\D/g,'');
		});
	});

	$('.fracionar-pedido').live("click", function () {
		var $this = $(this), $tr = $this.parent().parent(),
			indice = $tr.index()+1, dados = $tr.find('td.dados'), bgcolor = $tr.attr('bgcolor') || '';

		var tdDados =
			'<td class="dados" ' +
			'	autorizado="' + dados.attr('autorizado') + '" ' +
			'	qtd="' + dados.attr('qtd') + '" ' +
			'	id_pedido="' + dados.attr('id_pedido') + '" ' +
			'	id_item="' + dados.attr('id_item') + '" ' +
			'	n="' + dados.attr('n') + '" ' +
			'	tiss="' + dados.attr('tiss') + '" ' +
			'	sn="' + dados.attr('sn') + '" ' +
			'	catalogo_id="' + dados.attr('catalogo_id') + '" ' +
			'	soldata="' + dados.attr('soldata') + '" ' +
			'	env="' + dados.attr('env') + '" ' +
			'	total="' + dados.attr('total') + '" ' +
			'	tipo="' + dados.attr('tipo') + '">' +
			'	<input type="text" name="qtd-envio" class="numerico quantidade" style="text-align:right" size="4" autocomplete="off">' +
			'</td>';
		var background = 'bgcolor="' + bgcolor + '"';
		$(".mytable tr").eq(indice).after(
			'<tr ' + background + '>' +
			'	<td><img src="/utils/delete_16x16.png" width="16" class="remover-linha-fracionar" style="cursor: pointer;" title="Remover fracionamento"></td>' +
			'	<td></td>' +
			tdDados +
			'	<td><input type="text" name="lote" style="text-align:right" size="8" class="lote"></td>' +
			'</tr>'
		);
		$('.numerico').keyup(function(e){
			this.value = this.value.replace(/\D/g,'');
		});
	});

	$(".remover-linha-fracionar").live("click", function () {
		$(this).parent().parent().remove();
	});

  $("#formulario-troca-med").dialog({
  	autoOpen: false, modal: true, width: 900, position: 'top',
  	open: function(event,ui){
  		$("#formulario-troca-med input[name='justificativa']").val('');
  		$("input[name='busca']").val('');
  		$('#nome').val('');
	    $('#nome').attr('readonly','readonly');
	    $('#apr').val('');
	    $('#apr').attr('readonly','readonly');
	    $('#cod').val('');
		$('#qtd').val('');
        $('#catalogo_id').val();
	    $("input[name='busca']").focus();
		$('#select-troca-item').chosen();
		$('#select-troca-item').trigger('chosen:updated');
  	},
  	buttons: {
  		"Cancelar" : function(){
			$('#select-troca-item').html('');
  			$(this).dialog("close");
  		},
  		"Trocar" : function(){
  			if(validar_campos('formulario-troca-med')){
  				var just = $("#formulario-troca-med input[name='justificativa']").val();
  				var sol = $("#formulario-troca-med").attr("sol");
  				var tipo = $("#formulario-troca-med").attr("tipo");

				var  item= $('#select-troca-item').val();
  				var qtd = $("#formulario-troca-med input[name='qtd']").val();
  				var n = $('#select-troca-item option:selected').text();
  				var p = $("#div-envio-paciente").attr("p");
				var tiss = $('#select-troca-item option:selected').attr('tiss');
  				$.post("query.php",{query: "trocar-item-solicitacao", sol: sol, just: just, tipo: tipo, item: item, qtd: qtd, p: p, n: n, tiss: tiss},
  					function(r){
  						if(r == '1'){
  							alert('Troca efetuada com sucesso!');
  							window.location.reload();
  						}else{
							alert(r);
						}
  				});
				$('#select-troca-item').html('');
	  			$(this).dialog("close");
  			}
  		}
  	}
  });

	$("#formulario-adicionar-medicamento-saida").dialog({
		autoOpen: false, modal: true, width: 900, position: 'top',
		open: function(event,ui){
			$("#formulario-adicionar-medicamento-saida input[name='lote']").val('');
			$("input[name='busca']").val('');
			$('#nome').val('');
			$('#nome').attr('readonly','readonly');
			$('#apr').val('');
			$('#apr').attr('readonly','readonly');
			$('#cod').val('');
			$('#qtd').val('');
			$('#catalogo_id').val();
			$("input[name='busca']").focus();
			$('#select-adicionar-medicamento').chosen();
			$('#select-adicionar-medicamento').trigger('chosen:updated');
		},
		buttons: {
			"Fechar" : function(){
				$('#select-adicionar-medicamento').html('');
				$(this).dialog("close");
			},
			"Adicionar" : function(){
				var ok = true;
				if(!$('#select-adicionar-medicamento').val()) {
					ok = false;
					alert('Selecione o item que será adicionado! Provavelmente o item da solicitação foi descontinuado.');
				}
				if(validar_campos('formulario-adicionar-medicamento-saida') && ok){
					var lote = $("#formulario-adicionar-medicamento-saida input[name='lote']").val();
					var sol = $("#formulario-adicionar-medicamento-saida").attr("sol");
					var tipo = 0;


					var total = $('#formulario-adicionar-medicamento-saida').attr('total');
					var autorizado =$('#formulario-adicionar-medicamento-saida').attr('autorizado');

					var item = $('#select-adicionar-medicamento').val();
					var qtd = $("#formulario-adicionar-medicamento-saida input[name='qtd']").val();
					var n = $('#select-adicionar-medicamento option:selected').text();
					var p = $("#div-envio-paciente").attr("p");
					var tiss = $('#select-adicionar-medicamento option:selected').attr('tiss');
					var sn = '';
					var env = $('#formulario-adicionar-medicamento-saida').attr('env');
					var soldata = $('#formulario-adicionar-medicamento-saida').attr('soldata');
					var tdDados =
						'<td class="dados" ' +
						'	autorizado="' + autorizado + '" '+
						'	sol="'+ sol+'" ' +
						'	qtd="'+qtd+'" ' +
						'	cod="'+tiss+'" ' +
						'	catalogo_id="' + item + '" ' +
						'	n="' + n + '" ' +
						'	sn="' + sn+ '" ' +
						'	soldata="' + soldata + '" ' +
						'	env="' + env + '" ' +
						'	total="' + total + '" ' +
						'	tipo="' +tipo + '">' +
						'	<input type="text" name="qtd-envio" value = "'+qtd+'"  class="numerico quantidade" style="text-align:right" size="4" autocomplete="off">' +
						'</td>';
					var background = 'bgcolor="#DCDCDC"';
					$("#solicitacao_"+sol).append(
						'<tr ' + background + '>' +
						'	<td>' +
						'		<img src="/utils/delete_16x16.png" width="16" class="remover-linha-fracionar" ' +
						'			style="cursor: pointer;" title="Remover Item"> '+
						n+
						'</td>' +
						'	<td></td>' +
						tdDados +
						'<td></td>	<td><input type="text" name="lote" value="'+lote+'" style="text-align:right" size="8" class="lote"></td>' +
						'</tr>'
					);
					$('.numerico').keyup(function(e){
						this.value = this.value.replace(/\D/g,'');
					});
					$("#formulario-adicionar-medicamento-saida input[name='qtd']").val('');
					$("#formulario-adicionar-medicamento-saida input[name='lote']").val('');
					//$('#select-troca-item').html('');
				}
			}
		}
	});

	$(".adicionar-medicamento-saida").click(function(){

		var tipo = $(this).attr("tipo");
		var sol = $(this).attr("sol");
		var soldata = $(this).attr("soldata");
		var env = $(this).attr("env") ;
		var total = $(this).attr("total") ;
		var autorizado = $(this).attr("autorizado");
		var option = $(this).attr("option");
		var qtd = $(this).attr("qtd");

		$('#formulario-adicionar-medicamento-saida').attr('sol',sol);
		$('#formulario-adicionar-medicamento-saida').attr('tipo',tipo);
		$('#formulario-adicionar-medicamento-saida').attr('somente-generico');
		$('#formulario-adicionar-medicamento-saida').attr('soldata',soldata);
		$('#formulario-adicionar-medicamento-saida').attr('env',env);
		$('#formulario-adicionar-medicamento-saida').attr('total',total);
		$('#formulario-adicionar-medicamento-saida').attr('autorizado',autorizado);
		$('#formulario-adicionar-medicamento-saida').attr('qtd',qtd);

		var catalogoId = $(this).attr("catalogo_id");
		$.getJSON('busca_item_troca.php', {
			tipo: tipo ,
			catalogo_id: catalogoId,
			somenteGenerico:$('#formulario-adicionar-medicamento-saida').attr('somente-generico')
		},function(data){
			if(data.msg == null || data.msg == 'undefined' ){
				$.each(data,function(i,item){
					cor = '';
					if(item.textRed == 'S'){
						cor = 'color:red';
					}
					var  selectedItemSolicitado = '';
					if(item.catalogo_id == catalogoId){
						selectedItemSolicitado = 'selected';
					}

					option = "<option  style='"+cor+"' tiss='"+item.id+"' "+selectedItemSolicitado+" value= "+item.catalogo_id+">"+item.value+"</option>";
					$('#select-adicionar-medicamento').append(option);

				});
				$("#formulario-adicionar-medicamento-saida").dialog("open");
			}else{

				return false;
			}
		});
	});


	$(".adicionar-material-dieta-saida").click(function(){
		var tipo = $(this).attr("tipo");
		var sol = $(this).attr("sol");
		var soldata = $(this).attr("soldata");
		var env = $(this).attr("env") ;
		var total = $(this).attr("total") ;
		var autorizado = $(this).attr("autorizado");
		var qtd = $(this).attr("qtd");
		var nome = $(this).attr("nome-item");
		var option = '';
		var catalogoId = $(this).attr("catalogo_id");
		var tiss =  $(this).attr("tiss");

		$('#formulario-adicionar-mat-die-saida').attr('sol',sol);
		$('#formulario-adicionar-mat-die-saida').attr('tipo',tipo);
		$('#formulario-adicionar-mat-die-saida').attr('soldata',soldata);
		$('#formulario-adicionar-mat-die-saida').attr('env',env);
		$('#formulario-adicionar-mat-die-saida').attr('total',total);
		$('#formulario-adicionar-mat-die-saida').attr('autorizado',autorizado);
		$('#formulario-adicionar-mat-die-saida').attr('qtd',qtd);
		$("input[name='busca-adicionar-mat-die-saida']").val(nome);
		$('#cod-mat-die').val(catalogoId);
		$('#qtd-mat-die').val('');
		$("#tiss-mat-die").val(tiss);




		$('#formulario-adicionar-mat-die-saida').dialog('open');

	});
	$("#busca-adicionar-mat-die-saida").autocomplete({
		///source: "/utils/busca_brasindice.php",

		source: function(request, response) {
			$.getJSON('busca_brasindice_pedido.php', {
				term: request.term,
				tipo: $("#formulario-adicionar-mat-die-saida").attr("tipo")
			}, response);},
		minLength: 3,
		select: function( event, ui ) {

			$("#cod-mat-die").val(ui.item.catalogo_id);
			$("#tiss-mat-die").val(ui.item.numero_tiss);
			$("input[name='qtd-mat-die']").focus();
		}
	});

	$("#formulario-adicionar-mat-die-saida").dialog({
		autoOpen: false, modal: true, width: 800, position: 'top',
		open: function(event,ui){

			$('#qtd-mat-die').val('');
			$("input[name='busca-adicionar-mat-die-saida']").focus();

		},
		buttons: {
			"Fechar" : function(){
				$("#formulario-adicionar-mat-die-saida input[name='lote']").val('');
				$("#formulario-adicionar-mat-die-saida input[name='qtd-mat-die']").val('');
				$("#formulario-adicionar-mat-die-saida input[name='lote']").val('');
				$("input[name='busca-adicionar-mat-die-saida']").val('');
				$('#cod-mat-die').val('');
				$("#tiss-mat-die").val('');
				$('#qtd-mat-die').val('');
				$(this).dialog("close");
			},
			"Adicionar" : function(){
				if(validar_campos('formulario-adicionar-mat-die-saida')){
					var lote = $("#formulario-adicionar-mat-die-saida input[name='lote']").val();
					var sol = $("#formulario-adicionar-mat-die-saida").attr("sol");
					var tipo = $("#formulario-adicionar-mat-die-saida").attr("tipo");
					tipo = tipo == 'die' ? 3 : 1;
					var  item= $('#cod-mat-die').val();
					var qtd = $('#qtd-mat-die').val();
					var n = $('#busca-adicionar-mat-die-saida').val();
					var p = $("#div-envio-paciente").attr("p");
					var tiss = $("#tiss-mat-die").val();
					var sn = '';
					var env = $('#formulario-adicionar-mat-die-saida').attr('env');
					var soldata = $('#formulario-adicionar-mat-die-saida').attr('soldata');
					var total = $('#formulario-adicionar-mat-die-saida').attr('total');
					var autorizado = $('#formulario-adicionar-mat-die-saida').attr('autorizado');


					var tdDados =
						'<td class="dados" ' +
						'	autorizado="'+autorizado+'" '+
						'	sol="'+sol+'" ' +
						'	qtd="'+qtd+  '" ' +
						'	cod="'+tiss+ '" ' +
						'	catalogo_id="' + item + '" ' +
						'	n="'+ n +'" ' +
						'	sn="' + sn+ '" ' +
						'	soldata="' + soldata + '" ' +
						'	env="'+env+'" ' +
						'	total="'+total+'" ' +
						'	tipo="' +tipo + '">' +
						'	<input type="text" name="qtd-envio" value = "'+qtd+'"  class="numerico quantidade" style="text-align:right" size="4" autocomplete="off">' +
						'</td>';
					var background = 'bgcolor="#DCDCDC"';
					$("#solicitacao_"+sol).append(
						'<tr ' + background + '>' +
						'	<td>' +
						'		<img src="/utils/delete_16x16.png" width="16" class="remover-linha-fracionar" ' +
						'			style="cursor: pointer;" title="Remover Item"> '+
						n+
						'</td>' +
						'	<td></td>' +
						tdDados +
						'<td></td>	<td><input type="text" name="lote" value="'+lote+'" style="text-align:right" size="8" class="lote"></td>' +
						'</tr>'
					);
					$('.numerico').keyup(function(e){
						this.value = this.value.replace(/\D/g,'');
					});
					$("#formulario-adicionar-mat-die-saida input[name='lote']").val('');
					$("#formulario-adicionar-mat-die-saida input[name='qtd-mat-die']").val('');



				}
			}
		}
	});


	$(".troca-item-med").click(function(){
  	var tipo = $(this).attr("tipo");
  	var sol = $(this).attr("sol");
	var option = '';
	  $('#formulario-troca-med').attr('sol',sol);
	  $('#formulario-troca-med').attr('tipo',tipo);
	  $('#formulario-troca-med').attr('somente-generico');

	  var catalogoId = $(this).attr("catalogo_id");
	 	$.getJSON('busca_item_troca.php', {
			tipo: tipo ,
			catalogo_id: catalogoId,
			somenteGenerico:$('#formulario-troca-med').attr('somente-generico')
		},function(data){
			if(data.msg == null || data.msg == 'undefined' ){
				$.each(data,function(i,item){
					cor = '';
					if(item.textRed == 'S')
					cor = 'color:red';
					option = "<option  style='"+cor+"' tiss='"+item.id+"' value= "+item.catalogo_id+">"+item.value+"</option>";
					$('#select-troca-item').append(option);

				});
				$("#formulario-troca-med").dialog("open");
			}else{

				return false;
			}
		});
  });
	$(".adicionar-medicamento-saida-pedido").click(function(){
		var tipo = $(this).attr("tipo");
		var ped = $(this).attr("ped");
		var option = '';
		$('#formulario-troca-med-pedido').attr('ped',ped);
		$('#formulario-troca-med-pedido').attr('tipo',tipo);
		$('#formulario-troca-med-pedido').attr('somente-generico');

		var catalogoId = $(this).attr("catalogo_id");
		$.getJSON('busca_item_troca.php', {
			tipo: tipo ,
			catalogo_id: catalogoId,
			somenteGenerico:$('#formulario-troca-med-pedido').attr('somente-generico')
		},function(data){
			if(data.msg == null || data.msg == 'undefined' ){
				$.each(data,function(i,item){
					cor = '';
					if(item.textRed == 'S')
						cor = 'color:red';
					option = "<option  style='"+cor+"' tiss='"+item.id+"' value= "+item.catalogo_id+">"+item.value+"</option>";
					$('#select-troca-item').append(option);

				});
				$("#formulario-troca-med-pedido").dialog("open");
			}else{

				return false;
			}
		});
	});
	$(".adicionar-medicamento-saida-pedido").click(function(){

		var tipo = $(this).attr("tipo");
		var sol = $(this).attr("sol");
		var soldata = $(this).attr("soldata");
		var env = $(this).attr("env") ;
		var total = $(this).attr("total") ;
		var autorizado = $(this).attr("autorizado");
		var option = $(this).attr("option");
		var qtd = $(this).attr("qtd");
		var catalogoId = $(this).attr("catalogo_id");
		var id_item_pedido = $(this).attr("id_item");
		var id_pedido = $(this).attr("id_pedido");

		$('#formulario-adicionar-medicamento-saida-pedido').attr('sol',sol);
		$('#formulario-adicionar-medicamento-saida-pedido').attr('tipo',tipo);
		$('#formulario-adicionar-medicamento-saida-pedido').attr('somente-generico');
		$('#formulario-adicionar-medicamento-saida-pedido').attr('soldata',soldata);
		$('#formulario-adicionar-medicamento-saida-pedido').attr('env',env);
		$('#formulario-adicionar-medicamento-saida-pedido').attr('total',total);
		$('#formulario-adicionar-medicamento-saida-pedido').attr('autorizado',autorizado);
		$('#formulario-adicionar-medicamento-saida-pedido').attr('qtd',qtd);
		$('#formulario-adicionar-medicamento-saida-pedido').attr('catalogo_id',catalogoId);
		$('#formulario-adicionar-medicamento-saida-pedido').attr('id_item_pedido',id_item_pedido);
		$('#formulario-adicionar-medicamento-saida-pedido').attr('id_pedido',id_pedido);
		var tipo = $(this).attr("tipo");
		var ped = $(this).attr("ped");
		var option = '';
		$('#formulario-adicionar-medicamento-saida-pedido').attr('ped',ped);
		$('#formulario-adicionar-medicamento-saida-pedido').attr('tipo',tipo);


		$.getJSON('busca_item_troca.php', {
			tipo: tipo ,
			catalogo_id: catalogoId,

		},function(data){
			if(data.msg == null || data.msg == 'undefined' ){
				$.each(data,function(i,item){
					var cor = '';
					var selected = '';
					if(item.textRed == 'S')
						cor = 'color:red';
					if(item.catalogo_id == catalogoId)
					selected = "selected";
					option = "<option  "+selected+" style='"+cor+"' tiss='"+item.id+"' value= "+item.catalogo_id+">"+item.value+"</option>";
					$('#select-saida-item').append(option);

				});
				$("#formulario-adicionar-medicamento-saida-pedido").dialog("open");
			}else{
				return false;
			}
		});
	});

	$("#formulario-adicionar-medicamento-saida-pedido").dialog({
		autoOpen: false, modal: true, width: 900, position: 'top',
		open: function(event,ui){
			$("#formulario-troca-med-pedido input[name='justificativa']").val('');
			$("input[name='busca']").val('');
			$('#nome').val('');
			$('#nome').attr('readonly','readonly');
			$('#apr').val('');
			$('#apr').attr('readonly','readonly');
			$('#cod').val('');
			$('#qtd').val('');
			$('#catalogo_id').val();
			$("input[name='busca']").focus();
			$('#select-troca-item').chosen();
			$('#select-troca-item').trigger('chosen:updated');
		},
		buttons: {
			"Cancelar" : function(){
				$('#select-saida-item').html('');
				$(this).dialog("close");
			},
			"Adicionar" : function(){
				if(validar_campos('formulario-adicionar-medicamento-saida-pedido')){
					var lote = $("#formulario-adicionar-medicamento-saida-pedido input[name='lote']").val();
					var ped = $("#formulario-adicionar-medicamento-saida-pedido").attr("ped");
					var tipo = $("#formulario-adicionar-medicamento-saida-pedido").attr("tipo");
					var  item= $('#select-saida-item').val();
					var qtd = $("#formulario-adicionar-medicamento-saida-pedido input[name='qtd']").val();
					var n = $('#select-saida-item option:selected').text();
					var ur = $("#div-envio-ur").attr("ur");
					var tiss = $('#select-saida-item option:selected').attr('tiss');
					var id_pedido = $('#formulario-adicionar-medicamento-saida-pedido').attr('id_pedido');
					var id_item_pedido = $('#formulario-adicionar-medicamento-saida-pedido').attr('id_item_pedido');

					var tdDados =
						'<td class="dados" ' +
						'	autorizado="' + ''+ '" ' +
						'	qtd="' + qtd + '" ' +
						'	id_pedido="' + id_pedido + '" ' +
						'	id_item="' + id_item_pedido + '" ' +
						'	n="' + n + '" ' +
						'	tiss="' + tiss + '" ' +
						'	sn="' + '' + '" ' +
						'	catalogo_id="' + item+ '" ' +
						'	soldata="' + ''+ '" ' +
						'	env="' + '' + '" ' +
						'	total="' + ''+ '" ' +
						'	tipo="' + tipo + '">' +
						'	<input type="text" name="qtd-envio" class="numerico quantidade" value="'+qtd+'" style="text-align:right" size="4" autocomplete="off">' +
						'</td>';
					var background = 'bgcolor="#DCDCDC"';
					$("#linha_"+id_item_pedido).append(
						'<tr ' + background + '>' +
						'	<td><img src="/utils/delete_16x16.png" width="16" class="remover-linha-fracionar"' +
						' style="cursor: pointer;" title="Remover fracionamento">'+n+'</td>' +
						'	<td></td>' +
						tdDados +
						'	<td><input type="text" name="lote" style="text-align:right" value="'+lote+'" size="8" class="lote"></td>' +
						'</tr>'
					);
					$('.numerico').keyup(function(e){
						this.value = this.value.replace(/\D/g,'');
					});
					$("#formulario-adicionar-medicamento-saida-pedido input[name='lote']").val('');
					$("#formulario-adicionar-medicamento-saida-pedido input[name='qtd']").val('');


				}
			}
		}
	});

  $(".bpaciente1").click(function(){
	  var p = $(this).attr("p");
	  var url = $(this).attr("url");
	  var presc = $(this).attr("presc");
	  var tipo = $(this).attr("tipo");
	  
		  $("#formulario-check").attr("p",p);
		  $("#formulario-check").attr("url",url);
		  $("#formulario-check").attr("tipo",tipo);
		  $("#formulario-check").attr("presc",presc);
		  if(tipo=='2')
			  window.location = "/logistica/"+url+"?op=saida2&p="+p+"&tipo="+tipo+"&presc="+presc;
		  else
			  $("#formulario-check").dialog("open");
	  
	  //window.location = "/logistica/?op=saida2&p="+$(this).attr("p");

  });
  
  $(".bpaciente, .bpaciente_prioridade").click(function(){
	  var p = $(this).attr("p");
	  var url = $(this).attr("url");
	  var presc = $(this).attr("presc");
	  var tipo = $(this).attr("tipo");
    var carater = $(this).attr("carater");
	  var primeira = $(this).attr("primeira-prescricao");

	  
		  $("#formulario-check").attr("p",p);
		  $("#formulario-check").attr("url",url);
		  $("#formulario-check").attr("tipo",tipo);
		  $("#formulario-check").attr("presc",presc);
      $("#formulario-check").attr("carater",carater);
	  $("#formulario-check").attr("primeira",primeira);

		  if(tipo=='2')
			  window.location = "/logistica/"+url+"?op=saida1&p="+p+"&tipo="+tipo+"&presc="+presc+"&primeira="+primeira;
		  else
			  $("#formulario-check").dialog("open");
	  
	  //window.location = "/logistica/?op=saida2&p="+$(this).attr("p");

  });
  
  $(".bpedido").click(function(){
	  var p = $(this).attr("p");
	  var empresa = $(this).attr("idempresa");
	  var tipo = $(this).attr("tipo");
	  var url = $(this).attr("url");
	  window.location = "/logistica/"+url+"?op=pedido&ur="+p+"&emp="+empresa;
		
	  
	  //window.location = "/logistica/?op=saida2&p="+$(this).attr("p");

  });
  
  $(".entrada_pedido").click(function(){
	 
	  var p = $(this).attr("p");
	  var empresa = $(this).attr("idempresa");
	  var tipo = $(this).attr("tipo");
	  var url = $(this).attr("url");
	  window.location = "/logistica/"+url+"?op=pedido_entrada_estoque&ur="+p+"&emp="+empresa;
		
	  
	  //window.location = "/logistica/?op=saida2&p="+$(this).attr("p");

  });
  
  $(".bpacotePaciente").click(function(){
	  var p = $(this).attr("p");
	  var url = $(this).attr("url");
	  var presc = $(this).attr("presc");
	  var tipo = $(this).attr("tipo");
    var carater = $(this).attr("carater");

	  
		  $("#formulario-check").attr("p",p);
		  $("#formulario-check").attr("url",url);
		  $("#formulario-check").attr("tipo",tipo);
		  $("#formulario-check").attr("presc",presc);
      $("#formulario-check").attr("carater",carater);
		  if(tipo=='2')
			  window.location = "/logistica/"+url+"?op=saida1&p="+p+"&tipo="+tipo+"&carater="+carater;
		  else
			  $("#formulario-check").dialog("open");
	  
	  //window.location = "/logistica/?op=saida2&p="+$(this).attr("p");

  });
  
  
  $(".bpacote").click(function(){
	  var p = $(this).attr("p");
			  window.location = "/logistica/?op=saida1&ur="+p+"&act=pendentes&nocache=" + (new Date()).getTime();
  });
  
  $(".bpacote_pedido").click(function(){
	  var p = $(this).attr("p");
	  var url = $(this).attr("url");
			  window.location = "/logistica/"+url+"?op=pedido&p="+p+"&nocache=" + (new Date()).getTime();;
  });
  
  $("#formulario-check").dialog({
	  	autoOpen: false, modal: true, width: 800, position: 'top',
	  	open: function(event,ui){
	  		 
	  	},
	  	buttons: {
	  		"Cancelar" : function(){
	  			var url = $(this).attr("url");
	  			window.location = "/logistica/"+url+"?op=saida1&p="+$(this).attr("p")+
					"&tipo="+$(this).attr("tipo")+'&carater='+$(this).attr("carater")+
					'&presc='+$(this).attr("presc")+'&primeira='+$(this).attr("primeira");
	  			$(this).dialog("close");
	  		},
	  		"Imprimir" : function(){
	  			
	  				var p = $("#formulario-check").attr("p");
	  				
	  				window.open('/logistica/checklist.php?p='+p);
	  			
	  			
		  			$(this).dialog("close");
	  			}
	  		}	  	
	  });
  
  $("#dialog-relatorio").dialog({
  	autoOpen: false, modal: true, position: 'top', height: 400, width: 600,
  	close : function(event,ui){ window.location = '?op=saida2&act=pendentes';}
  });
  
  
  $("#dialog-relatorio-pedido").dialog({
  	autoOpen: false, modal: true, position: 'top', height: 400, width: 600,
  	close : function(event,ui){ window.location = '?op=saida2&act=pendentes';}
  });
  
  $("#salvar-envio").live("click", function(){

     
      itens='';
      if($('#excluir-total').is(':checked')){
        if(validar_campos("div-justificativa-excluir-total")){
            if (confirm('Deseja cancelar todo o Pedido?')) {

                  $(".cancela-item").each(function(i){
                       
  			var sol = $(this).attr("sol");
                        
                        itens += sol+"#";
                        
                  });
                 var justificativa = $('#text-justificatica-excluir-total').val();
                  $.post("query.php",{query: "excluir-solicitacao-total", dados: itens,
                      justificativa:justificativa},function(r){
                      
                      if(r == 1){
                          alert('Salvo com sucesso.');
                          window.location = '?op=saida2&act=pendentes&nocache=' + (new Date()).getTime();
                          
                      }else{
                          alert(r);

                      }
  				
  			});
            }
            return false;
            
        }else{
        return false;
        } 
      }else{
  	if(validar_campos("div-envio-paciente")){
		var carater = $(this).attr('carater');
		var primeira = $(this).attr('primeira')
		var tipo_prescricao = '';
		if(carater == 'o'){
			tipo_prescricao = 'Aditivo/Avulsas/Emergencial'
		}else if(carater == 'p'){
			tipo_prescricao = primeira == 0 ? 'Periódica' : 'Periódica Avaliação';
		}
		$("#salvar-envio").attr('disabled','disabled');
  		var itens = "";
  		var vazio = true;
		var semFornecedor = false;
  		$(".dados").each(function(i){
  			var sol = $(this).attr("sol");
  			var cod = $(this).attr("cod");
			var catalogo_id = $(this).attr("catalogo_id");
  			var qtd = $(this).parent().children('td').eq(2).children("input").val();
  		    var lote = $(this).parent().children('td').eq(4).children("input").val();
  			var tipo_sol_equipamento = $(this).attr("tipo_sol_equipamento");
			var fornecedorId = 0;
			var fornecedorNome = 0;
  			var n = $(this).attr("n");
  			var sn = $(this).attr("sn");
  			var p = $("#div-envio-paciente").attr("p");
  			var t = $(this).attr("tipo");
  			var soldata = $(this).attr("soldata");
  			var env = $(this).attr("env");
  			var total = $(this).attr("total");
  			if(qtd != "" && tipo_sol_equipamento != 2 && qtd != undefined){
  				vazio = false;
				if(tipo_sol_equipamento == 1 || tipo_sol_equipamento ==3) {
					fornecedorId = $(this).parent().children('td').eq(3).children("select[name='fornecedor-equipamento']").val();
				}


                fornecedorNome = $(this).closest('tr').find("select[name='fornecedor-equipamento'] option:selected").text()
				if(fornecedorId == "-1"){
					$(this).parent().children('td').eq(3).children('div').css({"border":"3px solid red"});

					semFornecedor = true;
				} else {
					$(this).parent().children('td').eq(3).children('div').css({"border":""});

				}
  				itens += sol+"#"+qtd+"#"+cod+"#"+n+"#"+sn+"#"+p+"#"+t+"#"+soldata+"#"+env+"#"+total+"#"+lote+"#"+tipo_sol_equipamento+"#"+catalogo_id+"#"+fornecedorNome+"#"+fornecedorId+"$";
  			}
  			
  		});


		if(semFornecedor){
			return false;
		}
		if(vazio){
  			alert("Pedido vazio!");
  		} else {
  			var dia = $("#data-envio").val();
			var np = $("#div-envio-paciente").attr("np");
			var endereco = $("#div-envio-paciente").attr("endereco");
			var cuidador = $("#div-envio-paciente").attr("cuidador");
			var idEmpresaPaciente= $("#div-envio-paciente").attr("empresa-paciente");
  			var tel = $("#div-envio-paciente").attr("tel");
  			$.post("query.php",{query: "envio-paciente",
                dados: itens,
                dia: dia,
                np: np,
                endereco: endereco,
                cuidador: cuidador,
                tel:tel,
			 	idEmpresaPaciente:idEmpresaPaciente,
				tipo_prescricao:tipo_prescricao},function(r){
				var array = r.split('#&');
  				if(array[0] == "-1"){
  					alert("ERRO: Tente mais tarde!");
					$("#salvar-envio").attr('disabled','false');
  				} else {

  					$("#pre-relatorio").html(array[0]);
  					$("#relatorio").val(array[0]);
  					$("#dialog-relatorio").dialog("open");
  				}
  			});
  		}
  	}
      }
  	return false;
  });
  
 
  
 /**************** BUSCA : RASTREAMENTO ************************/  
  $("input[name='tipo-rastreamento']").change(function(){
	$("#combo-rastreamento").html("");
	$.post("query.php", {query: "combo-rastreamento", tipo: $(this).val() }, function(retorno){
		$("#combo-rastreamento").html(retorno);
	});  
  });
  
 /**************** BUSCA : SAIDA ************************/ 
  
  $("input[name='tipo-busca-saida']").live('change', function(){
	$("#combo-busca-saida").html("");
	$.post("query.php", {query: "combo-busca-saida", tipo: $(this).val(), origem: $("input[name='origem']").val() }, function(retorno){
		$("#combo-busca-saida").html(retorno);
        $(".combo-pesquisar-saidas").chosen({search_contains: true});
        $(".combo-pesquisar-saidas").trigger("chosen:updated");
	});  
  });
 
 /**************** BUSCA : RELATORIOS ************************/
  
  $("#relatorios").click(function(){
    if(!validar_campos("div-relatorios")) return false;
    var cmd = "query.php?query=relatorio&paciente=" + $("select option:selected").val() + "&data=" + $("input[name='data']").val() + 
    "&pacientenome=" + $("select[name='paciente'] option:selected ").text();
    window.location = cmd;
    return false;
  });
  //////////////dialog///////////////
  
 
  
  
  
/**************** DEVOLUCAO ************************/  
  
  $("#del-devolucao").click(function(){
    $("#tab-devolucao tr td .select").each(function(i){
      if($(this).attr("checked")){
	$(this).parent().parent().remove();
      }
    });
    $("#tab-devolucao tr:odd").css("background-color","#ebf3ff");
    $("#tab-devolucao tr").hover(
	function(){$(this).css("background-color","#3d80df");$(this).css("color","#ffffff");},
	function(){
	  $(this).css("background-color","");$(this).css("color","");
	  $("#tab-devolucao tr:odd").css("background-color","#ebf3ff");
	  $("#tab-devolucao tr:odd").css("color","");
	}
      );
    return false;
  });
  
  $("#finalizar-devolucao").click(function(){
  	var itens = "";
  	
  	var total = $("#tab-devolucao tr").size() - 1;
  	
  	$(".itens").each(function(i){
		var cod = $(this).attr("id");
		var qtd = $(this).attr("qtd");
		var paciente = $(this).attr("idpaciente");
		var data = $(this).attr("data");
		var tipo=$(this).attr("tipo");
		var lote=$(this).attr("lote");
		var catalogo_id = $(this).attr("catalogo_id");
		
		var linha = cod+"#"+qtd+"#"+data+"#"+paciente+"#"+tipo+"#"+lote+"#"+catalogo_id;
		if(i<total - 1) linha += "$";
		itens += linha;
		
    });
    $.post("inserir.php",{req: "devolucao", dados: itens},
	   function(retorno){
	     if(retorno != '1'){
	       alert("ERRO: "+retorno);
	       return false;
	     }
	     alert("Sucesso!");
	     window.location = "?op=devolucao";
	});
  });
  
  $("#add-devolucao").click(function(){
    if(validar_campos("div-devolucao") && $("#id-novo-item").val() != '-1'){ 
      var qtd = $("#qtd-dev").val(); var nome = $("#busca-item").val();
	  var lote = $("#lote-dev").val(); 
      var id = $("#id-novo-item").val(); var data = $("input[name='data']").val(); var idpaciente = $("select[name='paciente']").val();var paciente = $("select[name='paciente'] option:selected").text();
      var tipo=$("#id-tipo").val();
	  var catalogo_id = $("#catalogo_id").val();
      var dados = "id='"+id+"' idpaciente='"+idpaciente+"' data='"+data+"' qtd='"+qtd+"' tipo='"+tipo+"' lote='"+lote+"' catalogo_id='"+catalogo_id+"'";
      $("#tab-devolucao").append("<tr class='itens' "+dados+" ><td><input type='checkbox' name='select' class='select' /></td><td>"+
			   paciente+"</td><td>"+nome+"</td><td>"+qtd+"</td><td>"+lote+"</td></tr>");
      $("#tab-devolucao tr:odd").css("background-color","#ebf3ff");
      $("#tab-devolucao tr").hover(
		function(){$(this).css("background-color","#3d80df");$(this).css("color","#ffffff");},
		function(){
	  		$(this).css("background-color","");$(this).css("color","");
	  		$("#tab-devolucao tr:odd").css("background-color","#ebf3ff");
	  		$("#tab-devolucao tr:odd").css("color","");
		}
      );
      $("#qtd-dev").val(""); $("#busca-item").val("");$("#id-novo-item").val("-1");$("#busca-item").attr('readonly','');
      $("#busca-item").focus();$("#lote-dev").val("");
	  $("#catalogo_id").val("");
	  $("#id-tipo").val("-1");
      $("#div-devolucao").attr("tipo",''); $("#equipamento").attr('checked','');
    }else if($("#id-novo-item").val() == '-1'){
  		$("#busca-item").css({"border":"3px solid red"});
  		$("#div-devolucao .aviso").text("Os campos em vermelho são obrigatórios!");
      	$("#div-devolucao .div-aviso").show();
  	}
    return false;
  });
    
/***************** ENTRADA DE MEDICAMENTOS E MATERIAIS ****************************/
  
  $("#checktodos-itens").click(function(){
    var checked_status = this.checked;
    $(".select-item").each(function()
    {
      this.checked = checked_status;
    });
  });
  
  $("#bonificados-itens").click(function(){
    var checked_status = this.checked;
    $(".bonificado-item").each(function()
    {
      this.checked = checked_status;
    });
  });
  
  $("#limpar-busca").click(function(){
  	$("#id-novo-item").val('-1');
	$("#id-tipo").val('-1');
  	$("#busca-item").attr('readonly','');
  	$("#busca-item").val('');
  	$("#qtd").val('');
  	$("#valor").val('');
  	return false;
  });
  
  $("#nome-busca-item").autocomplete({
			source: "busca_brasindice_fatura.php",
			minLength: 3,
			select: function( event, ui ) {
			}
  });
  
  $("#equipamento").click(function(){
	 if($(this).is(":checked")){
		 if(confirm('Deseja realmente devolver um equipamento por essa funcionalidade? ')){
             $("#div-devolucao").attr('tipo','eqp');
         }else{
             $(this).attr('checked',false);
             return;
         }


	 }else{
	
		 $("#div-devolucao").attr('tipo','');

	 }
	  
  });
  
  $("#busca-item").autocomplete({
			///source: "/utils/busca_brasindice.php",
                        
	  source: function(request, response) {
	        $.getJSON('busca_brasindice_fatura.php', {                    
	            term: request.term,
	            tipo: $("#div-devolucao").attr("tipo")
	        }, response);},
			minLength: 3,
			select: function( event, ui ) {
					$("#id-novo-item").val(ui.item.id);
          $("#catalogo_id").val(ui.item.catalogo_id);
					$("#id-tipo").val(ui.item.tipo);
					$("#busca-item").attr('readonly','readonly');
					$("input[name='qtd']").focus();
			}
  });
  
  $("#adicionar-item").click(function(){
  	if(validar_campos("div-form-entrada") && $("#id-novo-item").val() != '-1'){
  		var cod = $("#id-novo-item").val(); 
                var catalogo_id =$("#catalogo_id").val();
  		var qtd = $("#qtd").val();
  		var item = $("#busca-item").val(); 
  		var valor = $("#valor").val();
  		var des_bon_item= verificaNaN($("#des_bon_item").val()); 
  		var lote =$("#lote").val(); 
  		var validade =$("#validade").val();
                var codigo_referencia  = $("#codigo-referencia").val();
  		var cfop =$("#cfop").val();
  		var idx = $("#tabela-itens tr .itens").length + 1;
  		var boni = 0;
  		var check='';
                var tipo = $("#id-tipo").val();
                var tipo_aquisicao =$("#tipo-aquisicao").val();
  		
  		
  		if(des_bon_item != '0,00'){ boni = 1; check = 'checked';}
  		if($("#nota").attr("flag")==1){
  			$("#tabela-itens").append("" +
  				       "<tr bgcolor='#3D80DF' class='itens' bonificado='0' id='"+cod+"' qtd='"+qtd+"'  catalogo_id='"+catalogo_id+"' valor='"+valor+"' lote='"+lote+"' des_bon_item='"+des_bon_item+"' \n\
                                    validade='"+validade+"' tipo='"+tipo+"' codigo-referencia='"+codigo_referencia+"' tipo-aquisicao='"+tipo_aquisicao+"' cfop='"+cfop+"' >"+
  					   "<td><input type='checkbox' name='select' class='select-item' /></td>"+
  					   "<td>"+idx+"</td>"+
				       "<td>"+item+"</td>" +
				       "<td>"+qtd+"</td><td class='valor'>"+valor+"</td>" +
				       "<td>"+des_bon_item+"</td>"+
				       "<td><input type='checkbox' name='select' class='bonificado-item' "+check+" /></td>" +
				       "</tr>");
  		
  			
  		}else{
  			$("#tabela-itens").append("" +
				       "<tr class='itens' bonificado='0' id='"+cod+"' qtd='"+qtd+"' catalogo_id='"+catalogo_id+"' valor='"+valor+"' lote='"+lote+"' des_bon_item='"+des_bon_item+"' validade='"+validade+"' \n\
                                          tipo='"+tipo+"' codigo-referencia='"+codigo_referencia+"' tipo-aquisicao='"+tipo_aquisicao+"' cfop='"+cfop+"' >"+
					   "<td><input type='checkbox' name='select' class='select-item' /></td>"+
					   "<td>"+idx+"</td>"+
				       "<td>"+item+"</td>" +
				       "<td>"+qtd+"</td><td class='valor'>"+valor+"</td>" +
				       "<td>"+des_bon_item+"</td>"+
				       "<td><input type='checkbox' name='select' class='bonificado-item' "+check+" /></td>" +
				       "</tr>");
  		}
		$("#id-novo-item").val('-1');
  		$("#busca-item").attr('readonly','');
  		$("#busca-item").val('');
  		$("#qtd").val(""); 
                $("#valor").val("");
  		$("#busca-item").focus();
  		$("#lote").val('');
                $("#id-tipo").val('');
                $("#codigo-referencia").val('');
                $("#tipo-aquisicao").val('1');
               
                $("#catalogo_id").val('');
  		$("#des_bon_item").val('');
  		$("#cfop").val('');
  		var validade =$("#validade").val('');
                var valor_atual1=parseFloat($("#nota").attr("valor_atual"));
                var valor_item=parseFloat(valor.replace('.','').replace(',','.'));
                var  desconto = parseFloat(des_bon_item.replace('.','').replace(',','.'));
                
  		var  valor_atual = valor_atual1+ valor_item - desconto;
               
	   
  		$("#nota").attr("valor_atual",valor_atual.toFixed(2));
	    $("#vatual").html("R$ "+float2moeda(valor_atual));
  	}else if($("#id-novo-item").val() == '-1'){
  		$("#busca-item").css({"border":"3px solid red"});
  		$("#div-form-entrada .aviso").text("Os campos em vermelho são obrigatórios!");
      	$("#div-form-entrada .div-aviso").show();
  	}
  	return false;
  });
  
  $("#novaentrada").click(function() {
	/*
	  var produto= $("input[name='val_produtos']").val();
	 
	  $("input[name='val_produtos']").val(produto);
	  
	  var icms= verificaNaN($("input[name='icms']").val());
	  icms = parseFloat(icms.replace('.','').replace(',','.'));
	  $("input[name='icms']").val(icms);
	  
	  var frete= verificaNaN($("input[name='frete']").val());
	  frete = parseFloat(frete.replace('.','').replace(',','.'));
	  $("input[name='frete']").val(frete);
	  
	  
	  var seguro= verificaNaN($("input[name='seguro']").val());
	  seguro = parseFloat(seguro.replace('.','').replace(',','.'));
	  $("input[name='seguro']").val(seguro);
	  
	  var ipi= verificaNaN($("input[name='ipi']").val());
	  ipi= parseFloat(ipi.replace('.','').replace(',','.'));
	  $("input[name='ipi']").val(ipi);
	  
	  var issqn= verificaNaN($("input[name='issqn']").val());
	  issqn= parseFloat(issqn.replace('.','').replace(',','.'));
	  $("input[name='issqn']").val(issqn);
	  
	  var des_acessorias= verificaNaN($("input[name='des_acessorias']").val());
	  des_acessorias= parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  $("input[name='des_acessorias']").val(des_acessorias);
	  
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
	  des_bon= parseFloat(des_bon.replace('.','').replace(',','.'));
	  $("input[name='des_bon']").val(des_bon);
	  
	  var valor = verificaNaN($("#valor_nota").val());
	  valor= parseFloat(valor.replace('.','').replace(',','.'));
		*/  
	  
    if(!validar_campos("div-nova-entrada")){
      return false;
    }
  });
  
  function validate_entrada(){
    //if(parseFloat($("#nota").attr("valor")) == parseFloat($("#nota").attr("valor_atual")))
      // $("#finalizar_entrada").show();
     //else $("#finalizar_entrada").hide();
  }
  
  function atualiza_idx(){
    $(".itens").each(function(i){
	  idx++;
	  $(this).children("td").eq(1).html(idx);
    });
  }
  
  $("#remove-item").click(function(){
    $("#tabela-itens tr td .select-item").each(function(i){
	  if($(this).attr("checked")){
		  
	    var  valor_atual =  parseFloat($("#nota").attr("valor_atual")) - parseFloat($(this).parent().parent().attr("valor").replace('.','').replace(',','.')- parseFloat($(this).parent().parent().attr("des_bon_item").replace('.','').replace(',','.')));
	    $("#nota").attr("valor_atual",valor_atual.toFixed(2));
	    v = float2moeda(valor_atual);
	    $("#vatual").html("R$ "+v);
	    $(this).parent().parent().remove();
	  }
    });
    atualiza_idx();
    validate_entrada();
    return false;
  });
  
  $("#finalizar_entrada").click(function(){
	 var valorn =  $("#nota").attr("val_produtos");

	 valorn= valorn.replace('.','').replace(',','.');
	 valorn=parseFloat(valorn);
	
	 var valorat =  $("#nota").attr("valor_atual");
	
	 
	  if(valorn!=valorat){
		  
		  $("#dialog-confirmar").dialog("open");
		  
	  }else{
	  
  	var itens = "";
  	var total = $(".itens").size();
  	
  	$(".itens").each(function(i){
		var cod = $(this).attr("id");
                var qtd = $(this).attr("qtd"); 
		var valor = $(this).attr("valor");
                var bonificado = 0;
                var lote= $(this).attr("lote");
                var des_bon_item = $(this).attr("des_bon_item");
		var validade =$(this).attr("validade");
		var cfop =$(this).attr("cfop");
		var catalogo_id =$(this).attr("catalogo_id");
		if($(this).find(".bonificado-item").is(":checked")) bonificado = 1;
                var codigo_referencia =$(this).attr("codigo-referencia");
                var tipo_aquisicao = $(this).attr("tipo-aquisicao");
                var tipo = $(this).attr("tipo");
		var linha = cod+"#"+qtd+"#"+valor+"#"+bonificado+"#"+lote+"#"+des_bon_item+"#"+validade+"#"+cfop+"#"+catalogo_id+"#"+codigo_referencia+"#"+tipo_aquisicao+"#"+tipo;
		if(i<total - 1) linha += "$";
		itens += linha;
		
    });
  	
    $.post("inserir.php",{req: "nota", nota: $("#nota").attr("cod"), fornecedor: $("#nota").attr("fornecedor"), desc: $("#nota").attr("desc"),
    	 dados: itens, val_produtos: $("#nota").attr("val_produtos"), icms: $("#nota").attr("icms"), frete: $("#nota").attr("frete"), seguro: $("#nota").attr("seguro"),
    	 ipi: $("#nota").attr("ipi"), issqn: $("#nota").attr("issqn"), des_acessorias: $("#nota").attr("des_acessorias"), des_bon: $("#nota").attr("des_bon"),valor_nota: $("#nota").attr("valor"),chave: $("#nota").attr("chave")},
	   function(retorno){
	     if(retorno != '1'){
	       alert("ERRO: "+retorno);
	       return false;
	     }
	     $("#finalizar_entrada").hide();
    	 $("#dialog-finalizar").dialog("open");
	});
    
	  }
  });
  
  $("#dialog-confirmar").dialog({
	    
	  	autoOpen: false, 
	  	modal: true, 
	  	position: 'top',	  	
	  	buttons: {	  		
	  		"Fechar" : function(){
	  			$(this).dialog("close");
	  			return;
	  		}
   }
  });
  
 
  
  $("#dialog-finalizar").dialog({
    modal: true,
    autoOpen: false,
    buttons: {
      Ok: function() {
		$( this ).dialog( "close" );
		window.location = "?op=entrada";
      }
    },
    close: function(event,ui) {
		window.location = "?op=entrada";
    }
  });
 
 /**************** BUSCA : INVENTARIO ************************/
  
  $("#radio_med,#radio_mat,#radio_die").click(function(){
    var tipo;
    if($("#radio_med").attr("checked")){ tipo = $("#radio_med").val();}
    if($("#radio_mat").attr("checked")){ tipo = $("#radio_mat").val();}
    if($("#radio_die").attr("checked")){ tipo = $("#radio_die").val();}
    $("#div_inicio_inventario").load("query.php",{query: "inicio_inventario", tipo: tipo});
  });
  
  $("#gerar_inventario").click(function(){
    var tipo = "none";
    var ur = $(this).attr('ur');
    if($("#radio_med").attr("checked")){ tipo = $("#radio_med").val();}
    if($("#radio_mat").attr("checked")){ tipo = $("#radio_mat").val();}
    if($("#radio_die").attr("checked")){ tipo = $("#radio_die").val();}
    var nome = $("#inicio_inventario").val();
    var zerados = $("#zerados").attr("checked");
    var offset = $("#offset").val();
    if(tipo=="none") return false;
    window.open( "imprimir_inventario.php?tipo=" + tipo + "&zerados=" + zerados + "&nome=" + nome + "&offset=" + offset+"&ur="+ur,'_blank');
   // var cmd = "query.php?query=inventario&tipo=" + tipo + "&zerados=" + zerados + "&nome=" + nome + "&offset=" + offset;
   // window.location = cmd;
    return false;
  });
 
 /**************** ESPECIAIS ************************/
  
  $( "#dialog:ui-dialog" ).dialog( "destroy" );
  
  $( "#dialog" ).dialog({
    autoOpen: false,
    modal: true,
    buttons: {
      Ok: function() {
	$( this ).dialog( "close" );  
      }
    }
  });
  
  $("#dialog").dialog({
    buttons: {
      "Sim" : function(){
	$("#tabela_especiais tr td input").each(function(i){
	  if($(this).attr("checked") && i>0){
	    $.post("query.php",{query: "remover_especial", id: $(this).parent().parent().attr("idespecial") });
	    $(this).parent().parent().remove();
	  }
	});
	$(this).dialog("close");
      },
      "Não" : function(){
	$(this).dialog("close");
      }
    }
  });
  
  $("#remover_especial").click(function(){
    $("#dialog").dialog("open");
  });
  
  $("#pdf_especiais").click(function() {
    var venc = $("#vencimento2").val();
    if(venc != ""){
      var cmd = "query.php?query=pdf_especiais&venc=" + venc;
      window.location = cmd;
    } else {
      alert("Escolha uma data!");
    }
    return false;
  });
  
  $("#checktodos").click(function(){
    var checked_status = this.checked;
    $(".select").each(function()
    {
      this.checked = checked_status;
    });
  });


  
  
  ////////////////cancela item pedido
  $("#cancelar-item-pedido").hide();
  $("#cancelar-item-pedido").dialog({
	  	autoOpen: false, modal: true, width: 800, position: 'top',
	  	open: function(event,ui){
	  		$("#cancelar-item-pedido .aviso").text("");
	     	$("#cancelar-item-pedido .div-aviso").hide();
	  		$("#cancelar-item-pedido input[name='justificativa']").val('');
	  		$("#cancelar-item-pedido input[name='justificativa']").css({"border":""});
	  	    $("#cancelar-item-pedido input[name='justificativa']").focus();
	  	},
	  	buttons: {
	  		"Cancelar" : function(){
	  			$(this).dialog("close");
	  		},
	  		"Salvar" : function(){
	  			if(validar_campos('cancelar-item-pedido')){
	  				var just = $("#cancelar-item-pedido input[name='justificativa']").val();
	  				var id_item = $("#cancelar-item-pedido").attr("id_item");
	  				$.post("query.php",{query: "cancelar-item-pedido", id_item: id_item, just: just},function(r){
	  					if(r == '1'){
	  						alert('Cancelamento salvo com sucesso!');
	  						window.location.reload();
	  					}
	  				});
	  				$(this).dialog("close");
	  			}
	  		}
	  	}
	  });
	  
	  $(".cancela-item-pedido").click(function(){
	  	$("#cancelar-item-pedido").attr("id_item",$(this).attr("id_item"));
	  	$("#cancelar-item-pedido").dialog("open");
	  });
  
  //////////////////////
////////////////cancela item entrada em estoque cmd
	  $("#cancelar-item-entrada-estoque").hide();
	  $("#cancelar-item-entrada-estoque").dialog({
		  	autoOpen: false, modal: true, width: 800, position: 'top',
		  	open: function(event,ui){
		  		$("#cancelar-item-entrada-estoque .aviso").text("");
		     	$("#cancelar-item-entrada-estoque .div-aviso").hide();
		  		$("#cancelar-item-entrada-estoque input[name='justificativa']").val('');
		  		$("#cancelar-item-entrada-estoque input[name='justificativa']").css({"border":""});
		  	    $("#cancelar-item-entrada-estoque input[name='justificativa']").focus();
		  	},
		  	buttons: {
		  		"Cancelar" : function(){
		  			$(this).dialog("close");
		  		},
		  		"Salvar" : function(){
		  			if(validar_campos('cancelar-item-entrada-estoque')){
		  				var just = $("#cancelar-item-entrada-estoque input[name='justificativa']").val();
		  				//var qtd = $("#cancelar-item-entrada-estoque input[name='quantidade']").val();
		  				var id_item = $("#cancelar-item-entrada-estoque").attr("id_item");
		  				$.post("query.php",{query: "cancelar-item-entrada-estoque", id_item: id_item, just: just},function(r){
		  					if(r == '1'){
		  						alert('Cancelamento salvo com sucesso!');
		  						window.location.reload();
		  					}
		  				});
		  				$(this).dialog("close");
		  			}
		  		}
		  	}
		  });
		  
		  $(".cancela-item-entrada-estoque").click(function(){
			 
		  	$("#cancelar-item-entrada-estoque").attr("id_item",$(this).attr("id_item"));
		  	$("#cancelar-item-entrada-estoque").dialog("open");
		  });
	  
	  //////////////////////
	  
  
    

    ////////////////jeferson 2013-11-26 colocar em saida do item do paciente para o lote ser obrigatorio
    $('.quantidade').live("blur", function(){
        var valor = $(this).val();
         //se valor da saida for diferente de '' olote têm que ser obrigatório.
        if(valor != ''){
            $(this).parent().parent().children('td').eq(3).children('input').addClass('OBG');
            $(this).parent().parent().children('td').eq(3).children('input').focus();

        }else{
             $(this).parent().parent().children('td').eq(3).children('input').removeClass('OBG');
             $(this).parent().parent().children('td').eq(3).children('input').attr('style','text-align: right;')

        }

     });
     
     ////////////////jeferson 2013-11-26 colocar em entrada do pedido por CMD  o lote para ser obrigatorio quando a quantidade for diferente de vazio.
    $('.qtd_entrada').blur(function(){
        var valor = $(this).val();
         //se valor da entrada for diferente de 0 olote têm que ser obrigatório.
        if(valor != 0){
            $(this).parent().parent().children('td').eq(4).children('input').addClass('OBG');
            $(this).parent().parent().children('td').eq(4).children('input').focus();

        }else{
             $(this).parent().parent().children('td').eq(4).children('input').removeClass('OBG');
             $(this).parent().parent().children('td').eq(4).children('input').attr('style','text-align: right;')

        }

     });
     
     ///////////////jeferson 2013-27-11 saida interna
    /// adicionar item a saida
     $("#add-saida-interna").click(function(){
    if(validar_campos("div-saida-interna") && $("#id-novo-item").val() != '-1'){ 
      var qtd = $("#qtd-saida-interna").val(); 
      var nome = $("#busca-item").val();
      var lote = $("#lote-saida-interna").val(); 
      var id = $("#id-novo-item").val(); 
      var data = $("input[name='data']").val();
      var colaborador = $("#nome-colaborador").val();
      var empresa = $("select[name='empresa'] option:selected").val();
      var tipo=$("#id-tipo").val();
      var catalogo_id = $("#catalogo_id").val();
      var vencido = $("#vencido").val();
      var vencimento =$("#vencimento").val();
      var dados = "id='"+id+"' nome='"+nome+"' colaborador='"+colaborador+"' vencido='"+vencido+"' data='"+data+"' qtd='"+qtd+"' tipo='"+tipo+"' lote='"+lote+"' catalogo_id='"+catalogo_id+"' vencimento='"+vencimento+"' empresa='"+empresa+"'";
      $("#tab-saida-interna").append("<tr class='itens' "+dados+" ><td><input type='checkbox' name='select' class='select' /></td><td>"+
			   colaborador+"</td><td>"+nome+"</td><td>"+qtd+"</td><td>"+lote+"</td></tr>");
      $("#tab-saida-interna tr:odd").css("background-color","#ebf3ff");
      $("#tab-saida-interna tr").hover(
		function(){$(this).css("background-color","#3d80df");$(this).css("color","#ffffff");},
		function(){
	  		$(this).css("background-color","");$(this).css("color","");
	  		$("#tab-saida-interna tr:odd").css("background-color","#ebf3ff");
	  		$("#tab-saida-interna tr:odd").css("color","");
		}
      );
          /////para usuarios de f3eira o select das URs deve ser zerado cada vez que adicione uma saida, para as outras urs ele mantem a seleção.
          if ($("#empresa_principal").val()==1){
               $("select[name='empresa'] ").val('-1');
          }
      $("#qtd-saida-interna").val("");
      $("#busca-item").val("");
      $("#vencido").val("0");
      $("#data").val("");
      $("#vencimento").val("");
      $("#id-novo-item").val("-1");
      $("#busca-item").attr('readonly','');
      $("#busca-item").focus();
      $("#lote-saida-interna").val("");
      $("#catalogo_id").val("");
      $("#id-tipo").val("-1");
      $("#nome-colaborador").val('');
      $("#nome-colaborador").attr('disabled', false);
      $("#checkbox-vencido").attr('checked',false);
     
     $("#div-saida-interna").attr("tipo",''); 
    }else if($("#id-novo-item").val() == '-1'){
  		$("#busca-item").css({"border":"3px solid red"});
  		$("#div-saida-interna .aviso").text("Os campos em vermelho são obrigatórios!");
      	$("#div-saida-interna .div-aviso").show();
  	}
    return false;
  });
  
  ////Removendo todos os itens da saida
   $("#del-saida-interna").click(function(){
    $("#tab-saida-interna tr td .select").each(function(i){
      if($(this).attr("checked")){
	$(this).parent().parent().remove();
      }
    });
    $("#tab-saida-interna tr:odd").css("background-color","#ebf3ff");
    $("#tab-saida-interna tr").hover(
	function(){$(this).css("background-color","#3d80df");$(this).css("color","#ffffff");},
	function(){
	  $(this).css("background-color","");$(this).css("color","");
	  $("#tab-saida-interna tr:odd").css("background-color","#ebf3ff");
	  $("#tab-saida-interna tr:odd").css("color","");
	}
      );
    return false;
  });
  //////////////salvar saida interna
   $("#salvar-saida-interna").click(function(){
  	var itens = "";
  	
  	var total = $("#tab-saida-interna tr").size() - 1;
  	
  	$(".itens").each(function(i){
		var cod = $(this).attr("id");
		var qtd = $(this).attr("qtd");
		var colaborador = $(this).attr("colaborador");
		var data = $(this).attr("data");
		var tipo=$(this).attr("tipo");
		var lote=$(this).attr("lote");
		var catalogo_id = $(this).attr("catalogo_id");
                var empresa = $(this).attr("empresa");
                var nome = $(this).attr("nome");
                var vencido =$(this).attr("vencido");
                var vencimento= $(this).attr("vencimento");
		var linha = cod+"#"+qtd+"#"+data+"#"+colaborador+"#"+tipo+"#"+lote+"#"+catalogo_id+"#"+empresa+"#"+nome+"#"+vencido+"#"+vencimento;
		if(i<total - 1) linha += "$";
		itens += linha;
		
    });
    $.post("inserir.php",{req: "saida-interna", dados: itens},
	   function(retorno){
	     if(retorno != '1'){
	       alert("ERRO: "+retorno);
	       return false;
	     }
	     alert("Sucesso!");
	     window.location = "?op=saida-interna";
	});
  });
  
  //////////////jeferson 2013-12-04 pesquisar itens de entrada menos o a saida para os pacientes da ur.
  $("#pesquisa-entrada-saida").click(function(){
     var ur= $("select[name='codigo']").val();
     var inicio = $("#inicio").val();
     var fim = $("#fim").val();
     $.post("query.php",{
         query: "pesquisa-entrada-saida", ur:ur, inicio:inicio, fim:fim},
	   function(retorno){
	     $("#tabela-entrada-saida").html(retorno);
	       
	     return false;
        });
  
  });
    //////////////jeferson 2013-12-18 pesquisar itens por lote.
  $("#pesquisa-relatorio-lote").click(function(){
      if(validar_campos("div-relatorio-lote")){
        var catalogo_id = $('#catalogo_id').val();
        var tipo= $('#id-tipo').val();
        var lote= $('#lote').val();
        var inicio = $("#inicio").val();
        var fim = $("#fim").val();
        $.post("query.php",{
            query: "pesquisa-relatorio-lote",
            catalogo_id:catalogo_id,
            lote:lote,                       
            inicio:inicio,
            fim:fim},
              function(retorno){
                $("#tabela-relatorio-lote").html(retorno);

                return false;
           });
      }
  });
  
///////////////pesquisar equipamentos ativos
$("#pesquisa-equipamento-ativo").click(function(){
  var paciente = $("#paciente-equipamento").val();
  var convenio = $("#convenio-equipamento").val();
  var tipo = $(this).attr('data-tipo');
   if (validar_campos('div-paciente-equipamento-ativo')) {
       $.post("query.php", {
               query: "pesquisa-equipamento-ativo",
               paciente: paciente,
               convenio: convenio,
               tipo: tipo
           },
           function (retorno) {
               $("#tabela-equipamento-ativo").html(retorno);
			   $(".data-devolucao").datepicker({
				   inline: true,
				   changeMonth: true,
				   changeYear: true,
				   maxDate: 0,
				   minDate:-3

			   });

               return false;
           });
   }
    return false;
    
});
///////////

//////////vencido em saida interna
$('#checkbox-vencido').click(function(){
    if($(this).is(':checked')){
        $('#vencido').val('1');
        $('#nome-colaborador').val('VENCIDO');
         $('#nome-colaborador').attr('disabled',true);
        
    }else{
        $('#vencido').val('0');
         $('#nome-colaborador').attr('disabled',false);
         $('#nome-colaborador').val('');
    }
});

////excluir pedido Interno
$('#excluir-total').click(function(){
    if($(this).is(':checked')){
        
        $('#span-justificatica-excluir-total').attr('style','display:on');
        $('#text-justificatica-excluir-total').addClass('OBG');
        $('#text-justificatica-excluir-total').attr('style','display:on');
        $("#cancelar-pedido-total").attr('style','display:on');
    }else{
        $('#span-justificatica-excluir-total').attr('style','display:none');
         $('#text-justificatica-excluir-total').removeClass('OBG');
         $('#text-justificatica-excluir-total').attr('style','display:none');
          $("#cancelar-pedido-total").attr('style','display:none');
    }
    
});

$('#pesquisar-gerenciar-pedidos').click(function(){
   
    $.post("query.php",{
            query: "pesquisar-gerenciar-pedidos",
            empresa:$('#empresa').val(),inicio:$("input[name='inicio']").val(),fim: $("input[name='fim']").val()},
              function(retorno){
                $("#resultado-pesquisa-gerenciar-pedidos").html(retorno);
                  
                return false;
           });
});

        $(".bpedido-gerenciar").live('click',function(){
                  var p = $(this).attr("p");
                 window.location = "?op=visualizar-pedido&p="+p;


                  

          });
          
    $("#confirmar-pedido").click(function(){
        var id_pedido = $(this).attr("id_pedido");
        $.post("inserir.php",{
            req: "confirmar-pedido",
            id_pedido:id_pedido},
              function(retorno){
               if(retorno == 1){
                   alert('Pedido confirmado com sucesso!');
                  window.location = "?op=gerenciar-pedidos";
               }else{
                   alert(retorno);
               }
                  
                return false;
           });
        });
        
        $("#cancelar-pedido-total").click(function(){
               if($('#excluir-total').is(':checked')){
        if(validar_campos("div-justificativa-excluir-total")){
            if (confirm('Deseja cancelar todo o Pedido?')) {
                  var justificativa = $('#text-justificatica-excluir-total').val();
                  $.post("inserir.php",{req: "cancelar-pedido-total", 
                      justificativa:justificativa,
                      id_pedido:$("#cancelar-pedido-total").attr('id_pedido')},function(r){
                      
                      if(r == 1){
                          alert('Cancelado com sucesso.');
                          window.location = "?op=gerenciar-pedidos";
                          
                      }else{
                          alert(r);
                      }
  				
  			});
            }
            return false;
            
        }else{
        return false;
        } 
      }
            
        });
        
    $("#busca-entrada-equipamento").click(function(){
         $("#div-devolucao").attr('tipo','');
           $("#codigo-referencia").removeClass('OBG');
           $("#codigo-referencia").attr('style','');
        if($(this).is(':checked')){
            $("#div-devolucao").attr('tipo','eqp');
            $("#codigo-referencia").addClass('OBG');
        }
    });
    
  
     $("input[name='buscar-item-cadastro']").autocomplete({
		source: function(request, response) {
          $.getJSON('busca_item_cadastro.php', {
              term: request.term,
              tipo: $('#tipo_brasindice_cadastro').val()
             
          }, response);
       
     },
		minLength: 3,
		select: function( event, ui ) {		
    			
		      			$('#principio').val(ui.item.principio);
                      $('#apresentacao').val(ui.item.apresentacao);	              
		   			   $('#laboratorio').val(ui.item.lab);
                      $('#numero_tiss').val(ui.item.tiss);
                      $('#numero_tuss').val(ui.item.tuss);
                      $('#buscar-item-cadastro').val('');
                      $('#referencia').val(ui.item.referencia);
                      $('#nome-simpro-brasindice').val(ui.item.nome);
                      $('#id-simpro-brasindice').val(ui.item.id);
					$("#generico").val(ui.item.generico);


			return false;
		},
		extraParams: {
			tipo: 2
		}
   });

    $('#cadastro-produto-interno').click(function(){
        $('#id-simpro-brasindice').val('');
        if($(this).is(':checked')){
                      $('#principio').attr('disabled',false);
                      $('#apresentacao').attr('disabled',false);	              
		      $('#laboratorio').attr('disabled',false);
                      $('#numero_tiss').attr('disabled',false);
                      $('#numero_tuss').attr('disabled',false);
                      $('#referencia').val('I');
                      $('#nome-simpro-brasindice').attr('disabled',false);
                     
                      
            
        }else{
                      $('#principio').attr('disabled',true);
                      $('#apresentacao').attr('disabled',true);	              
		            $('#laboratorio').attr('disabled',true);
                      $('#numero_tiss').attr('disabled',true);
                      $('#numero_tuss').attr('disabled',true);
                      $('#referencia').val('');
                      $('#nome-simpro-brasindice').attr('disabled',true);
                     
        }
        
    });
    $(".recolher-equipamento-solicitado").click(function(){
         var idPaciente = $(this).attr('paciente');
         var idSolicitacao= $(this).attr('sol');
         var idItem = $(this).attr('catalogo_id');
        $("#recolher-equipamento-solicitado").attr('id-paciente',idPaciente);
        $("#recolher-equipamento-solicitado").attr('id-solicitacao',idSolicitacao);
        $("#recolher-equipamento-solicitado").attr('id-item',idItem);
        $.post("query.php",{query: "pesquisar-equipamentos-ativos-recolher",
                idPaciente:idPaciente,
                idItem:idItem},
            function(r){
                $("#recolher-equipamento-solicitado").html(r);
                $(".data-devolucao").datepicker({
                    inline: true,
                    changeMonth: true,
                    changeYear: true,
                    maxDate: 0,
                    minDate:-3

                });
        });


      $("#recolher-equipamento-solicitado").dialog('open');

    });

    $('.check-recolhe-equipamento').live('click',  function() {
    	console.log('ok');
        $(this).closest('tr').find('.data-devolucao').removeClass('OBG').attr('disabled','disabled');
    	if($(this).is(':checked')){

           var element = $(this).closest('tr');
           element.find('.data-devolucao').addClass('OBG').attr('disabled',false);
		}

    });

    $("#recolher-equipamento-solicitado").dialog({
        autoOpen: false,
        modal: true,
        width: 900,
        position: 'top',
        open: function(event,ui){

        },
        buttons: {
            "Fechar" : function(){
                $("#recolher-equipamento-solicitado").attr('id-paciente','');
                $("#recolher-equipamento-solicitado").attr('id-solicitacao','');
                $("#recolher-equipamento-solicitado").attr('id-item','');
                $(this).dialog("close");
            },
            "Salvar" : function(){
                     var cont =0;
                     var dados = '';
                     var jsonData = '[';
                     $('.check-recolhe-equipamento').each(function(){
                         if($(this).is(':checked')){
                             if (cont > 0){
                                 dados += ',';
                                 jsonData += ',';
                             }
                             cont++;
                              dados += $(this).attr('id-equipamento-ativo');
                              jsonData += '{"id":"'+$(this).attr('id-equipamento-ativo')+'","data":"'+ $(this).closest('tr').find('.data-devolucao').val()+'"}';


                         }
                     });

                jsonData += ']';

              //  var jsonRecolher = JSON.parse(jsonData);

                if (cont > 0) {
                	if(validar_campos('div-tabela-recolher-equipamento')){
                        $.post("query.php", {
                                query: 'desativar-equipamentos-ativos-recolher',
                                idSolicitacao: $("#recolher-equipamento-solicitado").attr('id-solicitacao'),
                                dados:dados,
                                jsonData:jsonData

                            },
                            function (r) {
                                if(r==1){
                                    $("#recolher-equipamento-solicitado").attr('id-paciente','');
                                    $("#recolher-equipamento-solicitado").attr('id-solicitacao','');
                                    $("#recolher-equipamento-solicitado").attr('id-item','');
                                    $(this).dialog("close");
                                    window.location.reload();
                                }else{
                                    alert(r);
                                }

                            });
					}

                }else{
                    alert('Nenhum Item foi selecionado.');
                }

            }
        }
    });
    $('.finalizar-equipamento').live('click',function() {
        if (confirm("Deseja realmente finalizar esse equipamento?")){
           var id= $(this).attr('id-equipamento-ativo');
            $.post("query.php", {
                query: 'desativar-equipamentos-ativos-recolher',
                idSolicitacao: 0,
                dados: $(this).attr('id-equipamento-ativo')

            }, function (r) {
                if (r == 1) {
                    alert('Equipamento finalizado com sucesso.');
                    $('#tr-equipamento-'+id).remove();
                    //window.location.reload();
                } else {
                    alert(r);
                }

            });
    }
        return false;
    });

    $('.check-equipamento-ativo-devolver').live('click',function(){
        if($(this).is(':checked')){
            $(this).parent().parent().find("input[name='data']").addClass('OBG');
            $(this).parent().parent().find("input[name='lote']").addClass('OBG');
        }else{
            $(this).parent().parent().find("input[name='data']").removeClass('OBG');
            $(this).parent().parent().find("input[name='lote']").removeClass('OBG');
        }
    });
    $('.check-equipamento-ativo-finalizar').live('click',function(){
        if($(this).is(':checked')){
            $(this).parent().parent().find("input[name='data']").addClass('OBG');
        }else{
            $(this).parent().parent().find("input[name='data']").removeClass('OBG');
        }
    });

    $('#devolver-equipamento-ativo').live('click',function(){
        var cont = 0;
        var dados='';
        var qtd = $('.check-equipamento-ativo-devolver:checked ').size();
        var linha = '';
        var paciente = $(this).attr('data-idpaciente');
         $('.check-equipamento-ativo-devolver:checked').each(function(i){

               var idEquipamentoAtivo = $(this).attr('id-equipamento-ativo');
               var idEquipamento =   $(this).attr('id-equipamento');
               var nomeEquipamento= $(this).attr('nome-equipamento');
               var lote = $(this).parent().parent().find("input[name='lote']").val();
               var data =  $(this).parent().parent().find("input[name='data']").val();
               var idFornecedor = $(this).attr('id-fornecedor');
               var nomeFornecedor = $(this).attr('nome-fornecedor');
               var nomePaciente = $(this).attr('nome-paciente');
                linha = idEquipamentoAtivo+'#'+idEquipamento+'#'+lote+'#'+data+'#'+nomeEquipamento+'#'+idFornecedor+'#'+nomeFornecedor+'#'+nomePaciente;
               if(i +1 <qtd)
                 linha +='$';
                   dados += linha;
                 cont++;

         });
        if(cont == 0){
            alert('Nenhum item Foi selecionado.');
            return false;
        }
        if(validar_campos('tabela-equipamento-ativo')){
            $.post("query.php", {
                query: 'devolver-equipamento-ativo',
                paciente:paciente,
                dados: dados

            }, function (r) {
                if (r == 1) {
                    alert("Equipamento devolvido com sucesso.")
                    window.location.reload();
                } else {
                    alert(r);
                }

            });
        }
        return false;

    });
    $('#finalizar-equipamento-ativo').live('click',function(){
        var cont = 0;
        var dados='';
        var qtd = $('.check-equipamento-ativo-finalizar:checked ').size();
        var linha = '';
        var paciente = $(this).attr('data-idpaciente');
        $('.check-equipamento-ativo-finalizar:checked').each(function(i){
             var idEquipamentoAtivo = $(this).attr('id-equipamento-ativo');
            var data =  $(this).parent().parent().find("input[name='data']").val();
            linha = idEquipamentoAtivo+'#'+data;
            if(i +1 <qtd)
                linha +='$';
            dados += linha;
            cont++;

        });
        if(cont == 0){
            alert('Nenhum item Foi selecionado.');
            return false;
        }
        if(validar_campos('tabela-equipamento-ativo')){
            $.post("query.php", {
                query: 'finalizar-equipamento-ativo',
                paciente:paciente,
                dados: dados

            }, function (r) {
                if (r == 1) {
                    alert("Equipamento(s) finalizado(s) com sucesso.")
                    window.location.reload();
                } else {
                    alert(r);
                }

            });
        }
        return false;

    });

	// AUTOCOMPLETE PRINCIPIO ATIVO
	$("input.buscar-principio").autocomplete({
		source: function(request, response) {
			$.getJSON('../busca_principio_ativo.php', {
				term: request.term,
			}, response);
		},
		minLength: 3,
		select: function(event, ui) {
			$('#id-principio').val(ui.item.id);
			$(this).val(ui.item.value);
			$('#editar-principio').val(ui.item.value);
			$('#editar-principio').focus();
			return false;
		}
	});
	$("input.buscar-principio-cadastro").autocomplete({
		source: function(request, response) {
			$.getJSON('busca_principio_ativo.php', {
				term: request.term,
			}, response);
		},
		minLength: 3,
		select: function(event, ui) {
			$('#id-principio').val(ui.item.id);
			$(this).val(ui.item.value);
			$('#editar-principio').val(ui.item.value);
			$('#editar-principio').focus();
			return false;
		}
	});

	$("#busca-troca-mat-die").autocomplete({
		///source: "/utils/busca_brasindice.php",

		source: function(request, response) {
			$.getJSON('busca_brasindice_pedido.php', {
				term: request.term,
				tipo: $("#formulario-troca-mat-die").attr("tipo")
			}, response);},
		minLength: 3,
		select: function( event, ui ) {

			$("#cod-mat-die").val(ui.item.catalogo_id);
			$("#tiss-mat-die").val(ui.item.numero_tiss);
			$("input[name='qtd-mat-die']").focus();
		}
	});
	$("#busca-saida-mat-die-pedido").autocomplete({
		///source: "/utils/busca_brasindice.php",

		source: function(request, response) {
			$.getJSON('busca_brasindice_pedido.php', {
				term: request.term,
				tipo: $("#formulario-saida-mat-die-pedido").attr("tipo")
			}, response);},
		minLength: 3,
		select: function( event, ui ) {

			$("#cod-mat-die").val(ui.item.catalogo_id);
			$("#tiss-mat-die").val(ui.item.numero_tiss);
			$("input[name='qtd-mat-die']").focus();
		}
	});

	$(".troca-item-mat-die").click(function(){
		var tipo = $(this).attr("tipo");
		var sol = $(this).attr("sol");
		var option = '';
		$('#formulario-troca-mat-die').attr('sol',sol);
		$('#formulario-troca-mat-die').attr('tipo',tipo);
		var catalogoId = $(this).attr("catalogo_id");

		$('#formulario-troca-mat-die').dialog('open');

	});

	$("#formulario-troca-mat-die").dialog({
		autoOpen: false, modal: true, width: 800, position: 'top',
		open: function(event,ui){
			$("#formulario-troca-mat-die input[name='justificativa-mat-die']").val('');
			$("input[name='busca-troca-mat-die']").val('');
			$('#cod-mat-die').val('');
			$('#qtd-mat-die').val('');
			$("#tiss-mat-die").val('')
			$("input[name='busca-troca-mat-die']").focus();

		},
		buttons: {
			"Cancelar" : function(){

				$(this).dialog("close");
			},
			"Trocar" : function(){
				if(validar_campos('formulario-troca-mat-die')){
					var just = $("#formulario-troca-mat-die input[name='justificativa-mat-die']").val();
					var sol = $("#formulario-troca-mat-die").attr("sol");
					var tipo = $("#formulario-troca-mat-die").attr("tipo");
					var  item= $('#cod-mat-die').val();
					var qtd = $("#formulario-troca-mat-die input[name='qtd-mat-die']").val();
					var n = $('#busca-troca-mat-die').text();
					var p = $("#div-envio-paciente").attr("p");
					var tiss = $("#tiss-mat-die").val();
					$.post("query.php",{query: "trocar-item-solicitacao", sol: sol, just: just, tipo: tipo, item: item, qtd: qtd, p: p, n: n, tiss: tiss},
						function(r){
							if(r == '1'){
								alert('Troca efetuada com sucesso!');
								window.location.reload();
							}else{
								alert(r);
							}
						});
					//$('#select_troca_item_chosen').empty();
					$('#select-troca-item').html('');
					$(this).dialog("close");
				}
			}
		}
	});
	$(".adicionar-mat-die-saida-pedido").live('click',function(){

		var tipo = $(this).attr("tipo");
		var sol = $(this).attr("sol");
		var soldata = $(this).attr("soldata");
		var env = $(this).attr("env") ;
		var total = $(this).attr("total") ;
		var autorizado = $(this).attr("autorizado");
		var option = $(this).attr("option");
		var qtd = $(this).attr("qtd");
		var catalogoId = $(this).attr("catalogo_id");
		var id_item_pedido = $(this).attr("id_item");
		var id_pedido = $(this).attr("id_pedido");
		var nome = $(this).attr("nome");
		var tiss = $(this).attr("tiss");

		$('#formulario-saida-mat-die-pedido').attr('sol',sol);
		$('#formulario-saida-mat-die-pedido').attr('nome',nome);
		$('#formulario-saida-mat-die-pedido').attr('tiss',tiss);
		$('#formulario-saida-mat-die-pedido').attr('tipo',tipo);
		$('#formulario-saida-mat-die-pedido').attr('somente-generico');
		$('#formulario-saida-mat-die-pedido').attr('soldata',soldata);
		$('#formulario-saida-mat-die-pedido').attr('env',env);
		$('#formulario-saida-mat-die-pedido').attr('total',total);
		$('#formulario-saida-mat-die-pedido').attr('autorizado',autorizado);
		$('#formulario-saida-mat-die-pedido').attr('qtd',qtd);
		$('#formulario-saida-mat-die-pedido').attr('catalogo_id',catalogoId);
		$('#formulario-saida-mat-die-pedido').attr('id_item_pedido',id_item_pedido);
		$('#formulario-saida-mat-die-pedido').attr('id_pedido',id_pedido);
		var tipo = $(this).attr("tipo");
		var ped = $(this).attr("ped");
		var option = '';
		$('#formulario-saida-mat-die-pedido').attr('ped',ped);
		$('#formulario-saida-mat-die-pedido').attr('tipo',tipo);
		$('#formulario-saida-mat-die-pedido').dialog('open');
	});
	$("#formulario-saida-mat-die-pedido").dialog('close');

	$("#formulario-saida-mat-die-pedido").dialog({
		autoOpen: false, modal: true, width: 800, position: 'top',
		open: function(event,ui){
			$("#formulario-saida-mat-die-pedido input[name='lote']").val('');
			$("input[name='busca-saida-mat-die-pedido']").val($('#formulario-saida-mat-die-pedido').attr('nome'));
			$('#cod-mat-die').val($('#formulario-saida-mat-die-pedido').attr('catalogo_id'));
			$('#qtd-mat-die').val('');
			$("#tiss-mat-die").val($('#formulario-saida-mat-die-pedido').attr('tiss'));
			$("input[name='busca-saida-mat-die-pedido']").focus();
		},
		buttons: {
			"Cancelar" : function(){
				$("#formulario-saida-mat-die-pedido input[name='lote']").val('');
				$("input[name='busca-saida-mat-die-pedido']").val('');
				$('#cod-mat-die').val('');
				$('#qtd-mat-die').val('');
				$("#tiss-mat-die").val('')
				$(this).dialog("close");
			},
			"Adicionar" : function(){
				if(validar_campos('formulario-saida-mat-die-pedido')){
					var lote = $("#formulario-saida-mat-die-pedido input[name='lote-mat-die']").val();
					var ped = $("#formulario-saida-mat-die-pedido").attr("ped");
					var tipo = $("#formulario-saida-mat-die-pedido").attr("tipo");
					var  item= $("#formulario-saida-mat-die-pedido input[name='cod-mat-die']").val();
					var qtd = $("#formulario-saida-mat-die-pedido input[name='qtd-mat-die']").val();
					var n =$("#formulario-saida-mat-die-pedido input[name='busca-saida-mat-die-pedido']").val();
					var ur = $("#div-envio-ur").attr("ur");
					var tiss = $("#formulario-saida-mat-die-pedido input[name='tiss-mat-die']").val();
					var id_pedido = $('#formulario-saida-mat-die-pedido').attr('id_pedido');
					var id_item_pedido = $('#formulario-saida-mat-die-pedido').attr('id_item_pedido');

					var tdDados =
						'<td class="dados" ' +
						'	autorizado="' + ''+ '" ' +
						'	qtd="' + qtd + '" ' +
						'	id_pedido="' + id_pedido + '" ' +
						'	id_item="' + id_item_pedido + '" ' +
						'	n="' + n + '" ' +
						'	tiss="' + tiss + '" ' +
						'	sn="' + '' + '" ' +
						'	catalogo_id="' + item+ '" ' +
						'	soldata="' + ''+ '" ' +
						'	env="' + '' + '" ' +
						'	total="' + ''+ '" ' +
						'	tipo="' + tipo + '">' +
						'	<input type="text" name="qtd-envio" class="numerico quantidade" value="'+qtd+'" style="text-align:right" size="4" autocomplete="off">' +
						'</td>';
					var background = 'bgcolor="#DCDCDC"';
					$("#linha_"+id_item_pedido).append(
						'<tr ' + background + '>' +
						'	<td><img src="/utils/delete_16x16.png" width="16" class="remover-linha-fracionar"' +
						' style="cursor: pointer;" title="Remover Item">'+n+'</td>' +
						'	<td></td>' +
						tdDados +
						'	<td><input type="text" name="lote" style="text-align:right" value="'+lote+'" size="8" class="lote"></td>' +
						'</tr>'
					);
					$('.numerico').keyup(function(e){
						this.value = this.value.replace(/\D/g,'');
					});
					$("#formulario-saida-mat-die-pedido input[name='lote-mat-die']").val('');
					$("#formulario-saida-mat-die-pedido input[name='qtd-mat-die']").val('');

				}
			}
		}
	});

	$(".troca-item-mat-die-pedido").live('click',function(){

		var tipo = $(this).attr("tipo");
		var ped = $(this).attr("ped");
		var option = '';
		$('#formulario-troca-mat-die-pedido').attr('ped',ped);
		$('#formulario-troca-mat-die-pedido').attr('tipo',tipo);

		$('#formulario-troca-mat-die-pedido').dialog('open');
	});

	$("#formulario-troca-mat-die-pedido").dialog({
		autoOpen: false, modal: true, width: 800, position: 'top',
		open: function(event,ui){
			$("#formulario-troca-mat-die-pedido input[name='justificativa-mat-die']").val('');
			$("input[name='busca-troca-mat-die-pedido']").val('');
			$('#cod-mat-die').val('');
			$('#qtd-mat-die').val('');
			$("#tiss-mat-die").val('')
			$("input[name='busca-troca-mat-die']").focus();
		},
		buttons: {
			"Cancelar" : function(){
				$(this).dialog("close");
			},
			"Trocar" : function(){
				if(validar_campos('formulario-troca-mat-die-pedido')){
					var just = $("#formulario-troca-mat-die-pedido input[name='justificativa-mat-die']").val();
					var ped = $("#formulario-troca-mat-die-pedido").attr("ped");
					var tipo = $("#formulario-troca-mat-die-pedido").attr("tipo");
					var  item= $('#cod-mat-die').val();
					var qtd = $("#formulario-troca-mat-die-pedido input[name='qtd-mat-die']").val();
					var n = $('#busca-troca-mat-die').text();
					var ur = $("#div-envio-ur").attr("ur");
					var tiss = $("#tiss-mat-die").val();
					$.post("query.php",{query: "trocar-item-pedido-interno", ped: ped, just: just, tipo: tipo, item: item, qtd: qtd, ur: ur, n: n, tiss: tiss},
						function(r){
							if(r == '1'){
								alert('Troca efetuada com sucesso!');
								window.location.reload();
							}else{
								alert(r);
							}
						});
					//$('#select_troca_item_chosen').empty();
					$('#select-troca-item').html('');
					$(this).dialog("close");
				}
			}
		}
	});

	//pedido de compra condensado da UR.
	$('.pedido_condensado').click(function(){
		var empresa = $(this).attr("empresa");
		var nome = $(this).attr("nome-empresa");
		window.location = "/logistica/?op=pedido&condensadoUr="+empresa+"&emp="+nome;
	});

	///Permite o usuário ajustar a quantidade de itens que devem ser enviados caso a auditora solicite que ele envie itens a menos.
	$('.ajustar-quantidade-item').click(function(){
		$('#quantidade-subtrair').attr('max',$(this).attr("quantidade"));
		$("#ajustar-quantidade").attr("sol",$(this).attr("sol"));
		$("#ajustar-quantidade").dialog("open");
		$('#span-quantidade-maxima').html($(this).attr("quantidade"));

	});
	/*************** SAIDA : ENVIO PARA O PACIENTE *****************/
	$("#ajustar-quantidade").dialog({
		autoOpen: false, modal: true, width: 800, position: 'top',
		open: function(event,ui){
			$("#ajustar-quantidade .aviso").text("");
			$("#ajustar-quantidade .div-aviso").hide();
			$("#justificativa-ajuste").val('');
			$('#quantidade-subtrair').val('');

		},
		buttons: {
			"Cancelar" : function(){
				$(this).dialog("close");
			},
			"Salvar" : function(){
				if(validar_campos('ajustar-quantidade')){
					var just = $("#justificativa-ajuste").val();
					var sol = $("#ajustar-quantidade").attr("sol");
					var quantidadeSubtrair = parseInt($('#quantidade-subtrair').val());
					var quantidade = parseInt($('#quantidade-subtrair').attr('max'));
					var autorizado = parseInt($('#adicionar-solicitacao-'+sol).attr('autorizado'));
					var novaQuantidade = autorizado - quantidadeSubtrair;
					var novaQuantidadeMaxima = quantidade - quantidadeSubtrair;

					if(quantidadeSubtrair < 1 ||  quantidadeSubtrair > quantidade){
						alert("A quantidade deve ser entre 1 e "+quantidade);
						$('#quantidade-subtrair').val('').focus()
						return false;
					}
					$.post("query.php",{query: "ajustar-quantidade",
						sol: sol,
						just: just,
						quantidade: quantidadeSubtrair
					},function(r){
						if(r == '1'){
							alert('Ajuste salvo com sucesso!');
							$('#btn-ajustar-sol-'+sol).attr('quantidade',novaQuantidadeMaxima);
							$('#quantidade-autorizada-sol-'+sol).html(novaQuantidade);
							$('#adicionar-solicitacao-'+sol).attr('autorizado', novaQuantidade);
							if(novaQuantidadeMaxima == 0 ){
								$('#btn-ajustar-sol-'+sol).remove();
							}
						}else{
							alert(r);
							return false
						}
					});

					$(this).dialog("close");
				}
			}
		}
	});

});





function float2moeda(num) {

   x = 0;

   if(num<0) {
      num = Math.abs(num);
      x = 1;
   }
   if(isNaN(num)) num = "0";
      cents = Math.floor((num*100+0.5)%100);

   num = Math.floor((num*100+0.5)/100).toString();

   if(cents < 10) cents = "0" + cents;
      for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
         num = num.substring(0,num.length-(4*i+3))+'.'
               +num.substring(num.length-(4*i+3));
   ret = num + ',' + cents;
   if (x == 1) ret = ' - ' + ret;return ret;

}
