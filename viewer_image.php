<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/validar.php');

if (! isset($_GET['path']))
  die('Missing param: path');

require_once __DIR__ . '/vendor/autoload.php';

use App\Core\Config;
use App\Helpers\Storage;

if(!isset($_GET['src'])) {
  $src = Storage::getImageUrl($_GET['path']);
} elseif(isset($_GET['src']) && $_GET['src'] == 'exames') {
  $src = Storage::getImageUrlByBucketName($_GET['path']);
}
?>

<!doctype html>
<html lang="pt">
<head>
  <meta charset="UTF-8">
  <title>SISMederi - Visualizador de Imagem</title>
</head>
<body>

  <img src="<?= $src ?>" />

</body>
</html>
