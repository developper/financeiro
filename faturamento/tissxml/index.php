<?php

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

use \App\Controllers\GuiaTissXML as Controller,
    \App\Models\Faturamento\Guia,
    \App\Core\Url;

if (isset($_GET['action']) && !empty($_GET['action'])) {
    define('SP_SADT', Guia::SP_SADT);
    define('RESUMO_INTERNACAO', Guia::RESUMO_INTERNACAO);

    $action = $_GET['action'];

    switch ($action) {
        case "nova-guia" : {
            if (! isset($_GET['capalote']) || empty($_GET['capalote']))
                die('Parâmentro Inválido!');

            $capaLote = $_REQUEST['capalote'];
            $tipo     = $_GET['tipo'];

            $response = Controller::novaGuia($capaLote, $tipo);

            if (isset($response['errors']) && !empty($response['errors'])) {
                $_SESSION['errors'] = $response['errors'];
                header('Location: ' . Url::get('faturamento-gerenciar-lote'));
            }
            extract($response['data']);
            require 'templates/tissxml-form.phtml';
            break;
        }
        case "salvar-guia" : {
            $data = $_POST;
            if (isset($data) && !empty($data)) {

                $response = Controller::salvarGuia($data);

                if (isset($response['mensagemTemp']) && !empty($response['mensagemTemp'])) {
                    $response = Controller::processarXML($response['lote'], $data['guiaxml']['versao']);
                    $_SESSION['mensagemTemp'] = $response['mensagemTemp'];
                }
                header('Location: ' . Url::get('faturamento-gerenciar-lote'));
                exit;
            }
            break;
        }
        case "processar-xml" : {
            if (! isset($_GET['loteId']) || empty($_GET['loteId']))
                die('Parâmentro Inválido!');

            $loteId = $_REQUEST['loteId'];

            $response = Controller::processarXML(['id' => $loteId]);

            if (isset($response['mensagemTemp']) && !empty($response['mensagemTemp'])) {
                $response = Controller::processarXML($response['lote']);
                $_SESSION['mensagemTemp'] = $response['mensagemTemp'];
            }
            header('Location: ' . Url::get('faturamento-gerenciar-lote'));
            exit;
        }
        case "baixar-xml" : {
            $response = Controller::baixarXML($_GET['xmlId']);
            break;
        }
        case "guias-listar" : {
            $loteId = filter_input(INPUT_GET, 'loteId', FILTER_SANITIZE_NUMBER_INT);

            if (! isset($loteId))
                die('Número de lote inválido!');

            $response = Controller::guiasListar($loteId);
            extract($response);
            require 'templates/guias-listar.phtml';
            break;
        }
        case "imprimir-guia-sp-sadt" : {
            if (! isset($_GET['guiaId']))
                die('Número de Guia TISS inválido!');

            $guiaId   = $_GET['guiaId'];
            $response = Controller::versaoImpressao($guiaId);
            extract($response);
            require 'templates/tissxml-impressao-ans.phtml';
            break;
        }
        case "imprimir-outras-despesas" : {
            if (! isset($_GET['guiaId']))
                die('Número de Guia TISS inválido!');

            $guiaId   = $_GET['guiaId'];
            $response = Controller::versaoImpressao($guiaId);
            extract($response);
            require 'templates/tissxml-impressao-outras-despesas.phtml';
            break;
        }
        case "imprimir-guia-internacao" : {
            if (! isset($_GET['guiaId']))
                die('Número de Guia TISS inválido!');

            $guiaId   = $_GET['guiaId'];
            $response = Controller::versaoImpressao($guiaId);
            extract($response);
            require 'templates/tissxml-impressao-guia-internacao.phtml';
            break;
        }
    }
}


