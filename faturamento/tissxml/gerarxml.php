<?php

define('ROOT', dirname(dirname(__DIR__)));

require ROOT . '/app/Models/Faturamento/XML/GuiaTiss.php';

use App\Models\Faturamento\XML as Fatu;

try {
  if (empty($_POST) || !isset($_POST['guiaxml']))
    throw new \InvalidArgumentException('Parâmetros Inválidos');

  $guiasSPSADT[] = $_POST['guiaxml'];

  $cabecalho = array(
      'seqTransacao' => 2, // @FIXME tornar o incremental dinâmico
      'tpTransacao' => 'ENVIO_LOTE_GUIAS',
      'origem' => $_POST['guiaxml']['origem'],
      'destino' => $_POST['guiaxml']['destino']
  );

  $guia = Fatu\GuiaTiss::createFromData($cabecalho, $guiasSPSADT);

  $guiaXML = new Fatu\GuiaTissXML($guia);
  if ($guiaXML->isValid()) {
    echo $guiaXML->xml->asXML();
  }
} catch (\InvalidArgumentException $e) {
  echo htmlentities($e->getMessage());
}