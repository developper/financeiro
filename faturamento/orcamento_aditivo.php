<?php
$id = session_id();
if (empty($id))
    session_start();

include_once('../db/config.php');
include_once('../utils/codigos.php');
require __DIR__ . '/../vendor/autoload.php';
use \App\Models\Administracao\Filial;



class OrcamentoAditivo {
    
    public function pesquisarPrescricoesAditivas(){
         echo "<div id='div-pesquisar-precricoes-aditivas'>
                    <center> 
                       <h1>".htmlentities('Orçamento Aditivo')."</h1>
                    </center>

                    <label for='paciente-orcamento-aditivo'>
                           <b>Paciente:</b>
                    </label><br>";
                    $this->pacientesOrcamentoAditivo();              

              echo" <fieldset style='width:550px;'>
                          <legend>
                            <b>".htmlentities('Período da Prescrição Aditiva ')."</b>
                          </legend>
                          DE
                          <input type='text' name='inicio' value='' maxlength='10' size='15' class=' OBG inicio-periodo' />
                          ".htmlentities('Até')."
                          <input type='text' name='fim' value='' maxlength='10' size='15' class='  OBG fim-periodo' />
                     </fieldset>
                     <br/>
                     <button class='ui-button ui-widget ui-state-default ui-corner-all
                         ui-button-text-only ui-state-hover ' id='pesquisar-prescricoes-aditivas' 
                         role='button' aria-disabled='false' >
                            <span class='ui-button-text'>
                               Pesquisar
                            </span>
                    </button>
               </div>
               </br>
               </br>
                <div id='dialog-motivo-ajuste' title='Motivo do Ajuste'>
                    <div id = 'div-motivo-ajuste'></div>
                </div>
               <div id='resultado-pesquisa-prescricoes'>             
                   
               </div>";
         
     } 
     
     public function pacientesOrcamentoAditivo(){
         $filiaisAtivas = Filial::getUnidadesAtivas();
         foreach ($filiaisAtivas as  $ativa){
             $filiais[$ativa['id']] = $ativa['id'];
         }
         $pacienteOrcamentoAditivo = new Faturamento();
         $pacienteOrcamentoAditivo->pacientes('paciente-orcamento-aditivo','COMBO_OBG1', $filiais);
      }  
      
      public function formulario(){
          echo "<div id='dialog-formulario-aditivo' title='Formul&aacute;rio' tipo='' tabela = '' id_prescricao='' tipo_medicamento='' classe='' cod='' plano= '' empresa=''>";
        echo alerta();
          echo "<div id='msg-generico'></div>";
        echo "<center><div id='tipos'>
  	<input type='radio' id='bmed' name='tipos' checked='checked' value='med' /><label for='bmed'>Medicamentos</label>
  	<input type='radio' id='bdie' name='tipos' value='die' /><label for='bdie'>Dieta</label>
  	<input type='radio' id='bmat' name='tipos' value='mat' /><label for='bmat'>Materiais</label>
  	<input type='radio' id='bserv' name='tipos' value='serv' /><label for='bserv'>Servi&ccedil;os</label>
  	<input type='radio' id='bgas' name='tipos' value='gas' /><label for='bgas'>Gasoterapia</label>
  	<input type='radio' id='bequip' name='tipos' value='eqp' /><label for='bequip'>Equipamento</label>
    <input type='radio' id='bextra' name='tipos' value='ext' /><label for='bextra'> Extras</label>
    <input type='radio' id='bkit' name='tipos' value='kit' /><label for='bkit'> Kits</label>

  	</div></center>";

        echo "<p><b>Busca:</b><br/><input type='text' name='busca-item-aditivo' id='busca-item-aditivo' />";
        echo "<div id='opcoes-kits'></div>";
        echo "<input type='hidden' name='cod' id='cod' value='-1' />";
        echo "<input type='hidden' name='tipo' id='tipo' value='0' />";
        echo "<input type='hidden' name='catalogo_id' id='catalogo_id' value='-1' />";
        echo "<input type='hidden' name='apresentacao' id='apresentacao' value='-1' />";
        echo "<input type='hidden' name='custo' id='custo' value='-1' />";
        echo "<input type='hidden' name='valor_plano' id='valor_plano' value='-1' />";
        echo "<input type='hidden' name='inicio' id='inicio' value='-1' />";
        echo "<input type='hidden' name='fim' id='fim' value='-1' />";
        echo "<input type='hidden' name='contador' id='contador' value='-1' />";
        echo "<input type='hidden' name='tabela_origem' id='tabela_origem' value='' />";
        echo "<input type='hidden' name='tuss' id='tuss' value='' />";
        echo "<p><b>Nome:</b><input type='text' name='nome' id='nome' class='OBG' />";

        echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
        echo "<button id='add-item-aditivo' tipo='' >+</button></div>";
      }
      public function orcaAditivo($idPrescricao,$tipo){
          if(empty($idPrescricao)){
              echo 'Erro 01 entre em contato com o TI';
              return false;
          }
          if(empty($tipo)){
               echo 'Erro 02 entre em contato com o TI';
              return false;
          }
          
          
          $idPrescricao = base64_decode($idPrescricao);
          $idPrescricao = anti_injection($idPrescricao,'numerico');
          $tipo = base64_decode($tipo);
          $tipo = anti_injection($tipo,'literal');
          
          echo "<div id='div-finalizar-orcamento-aditivo'> 
                    <center>
                           <h1>
                               ".  htmlentities('Orçamento Aditivo')."
                           </h1>
                    </center>";
           
                
                        
            if($tipo == 'prescricao'){
                
                    $sql= "
                    SELECT
                        presc.id,
                        u.nome AS medico,
                        u.idUsuarios,
                        DATE_FORMAT(presc. DATA, '%d/%m/%Y - %T') AS sdata,
                        DATE_FORMAT(presc.inicio, '%d/%m/%Y') AS inicio1,
                        DATE_FORMAT(presc.fim, '%d/%m/%Y') AS fim1,
                        UPPER(c.nome) AS paciente,
                        c.idClientes AS CodPaciente,
                        (
                                CASE presc.carater
                                WHEN '3' THEN
                                        'Prescrição Aditiva Medica'
                                WHEN '6' THEN
                                        'Prescrição Aditiva de Nutri&ccedil;&atilde;o'
                                END
                        ) AS tipo,
                        FLOOR(
                                DATEDIFF(CURDATE(), c.`nascimento`) / 365.25
                        ) AS idade,
                        e.nome AS empresan,
                        c.`nascimento`,
                        p.nome AS Convenio,
                        NUM_MATRICULA_CONVENIO,
                        p.tipo_medicamento

                    FROM
                                prescricoes AS presc
                        INNER JOIN clientes AS c ON (
                                c.idClientes = presc.paciente
                        )
                        INNER JOIN usuarios AS u ON (presc.criador = u.idUsuarios)
                        INNER JOIN empresas AS e ON (e.id = c.empresa)
                        INNER JOIN planosdesaude AS p ON (p.id = c.convenio)
                    WHERE
                        presc.id ='{$idPrescricao}'" ;
                        
                    $sqlItens="SELECT
                            concat(B.principio,' - ',B.apresentacao) as principio,
                             S.NUMERO_TISS AS cod,
                            S.CATALOGO_ID as catalogo_id,
                            '' as TABELA_ORIGEM,
                            C.nome,
                            S.tipo,
                            'P' as flag,
                            S.idSolicitacoes,
                            C.idClientes AS Paciente,
                             C.convenio AS Plano,
                           coalesce(B.valorMedio,'0.00') AS custo,
                            (CASE S.tipo
                            WHEN '0' THEN 'Medicamento'
                            WHEN '1' THEN 'Material'
                            WHEN '3' THEN 'Dieta' END) as apresentacao,
                            (CASE WHEN COUNT(*)>1 THEN SUM(S.qtd) ELSE S.qtd END) AS quantidade,
                            coalesce(B.VALOR_VENDA,'0.00')  AS valor,
                            C.empresa,
                            B.TUSS as codTuss
                        FROM
                            `solicitacoes` S INNER JOIN
                            `clientes` C ON (S.paciente = C.idClientes)  INNER JOIN
                            `catalogo` B ON (S.CATALOGO_ID = B.ID) INNER JOIN
                             prescricoes P ON (S.`idPrescricao`=P.id)

                        WHERE

                            S.`idPrescricao`='{$idPrescricao}'
                             and S.tipo IN (0,1,3) 
                        GROUP BY
                            catalogo_id
                        UNION
                        SELECT

                            IF (
                                    BC.DESC_COBRANCA_PLANO IS NULL,
                                    B.item,
                                    BC.DESC_COBRANCA_PLANO
                            ) AS principio,

                            IF (
                                    BC.DESC_COBRANCA_PLANO IS NULL,
                                    B.id,
                                    BC.COD_COBRANCA_PLANO
                            ) AS cod,

                            IF (
                                    BC.DESC_COBRANCA_PLANO IS NULL,
                                    B.id,
                                    BC.id
                            ) AS catalogo_id,

                            IF (
                                    BC.DESC_COBRANCA_PLANO IS NULL,
                                    'cobrancaplano',
                                    'valorescobranca'
                            ) AS TABELA_ORIGEM,
                             C.nome,
                             2 AS tipo,
                             'S' AS flag,
                             S.idPrescricao AS idSolicitacoes,
                             C.idClientes AS Paciente,
                             C.convenio AS Plano,
                             COALESCE (
                                    CS.CUSTO,
                                    '0.00'
                            ) AS custo,
                             ('Servi&ccedil;os') AS apresentacao,
                             (
                                    SUM(
                                        CASE
                                        WHEN ESP.id IN (2, 3, 4, 5) THEN	

                                                (DATEDIFF(S.fim, S.inicio) + 1)
                                        ELSE
                                        IF (
                                                S.frequencia IN (
                                                        2,
                                                        3,
                                                        4,
                                                        5,
                                                        6,
                                                        7,
                                                        8,
                                                        9,
                                                        10,
                                                        11,
                                                        12,
                                                        13,
                                                        14,
                                                        15,
                                                        16,
                                                        17,
                                                        18,
                                                        19
                                                ),
                                                (DATEDIFF(S.fim, S.inicio) + 1) * F.qtd,
                                                CEILING(
                                                        (DATEDIFF(S.fim, S.inicio) + 1) / 7
                                                ) * F.SEMANA
                                        )				
                                        END	
                                   )
                               ) AS quantidade,
                             COALESCE (BC.valor, '0.00') AS valor,
                             C.empresa,
                             '' AS codTuss
                            FROM
                                    `itens_prescricao` S
                            LEFT JOIN `frequencia` AS F ON (S.frequencia = F.id)
                            INNER JOIN prescricoes P ON (S.`idPrescricao` = P.id)
                            INNER JOIN `clientes` C ON (P.paciente = C.idClientes)
                            INNER JOIN cuidadosespeciais AS ESP ON (S.CATALOGO_ID = ESP.id)
                            INNER JOIN `cobrancaplanos` B ON (ESP.COBRANCAPLANO_ID = B.id)
                            LEFT JOIN custo_servicos_plano_ur CS ON (
                                    CS.COBRANCAPLANO_ID = B.id
                                    AND CS.UR = C.empresa
                                    AND CS.STATUS = 'A'
                            )
                            LEFT JOIN `valorescobranca` BC ON (
                                    B.id = BC.idCobranca
                                    AND BC.idPlano = C.convenio
                            )
                            INNER JOIN planosdesaude AS PL ON (C.convenio = PL.id)
                            WHERE
                                    P.id ={$idPrescricao}
                            AND S.tipo IN (2)
                            AND S.CATALOGO_ID NOT IN (17, 18, 21, 24)
                            AND BC.excluido_por is NULL
                            GROUP BY
                                    B.id,
                                    BC.id
                    UNION
                    SELECT DISTINCT
                            IF(V.DESC_COBRANCA_PLANO IS NULL,CO.item,V.DESC_COBRANCA_PLANO)AS principio,
                            IF(V.DESC_COBRANCA_PLANO IS NULL,CO.id,V.COD_COBRANCA_PLANO) AS cod,
                            IF(V.DESC_COBRANCA_PLANO IS NULL,CO.id,V.id) AS catalogo_id,
                            IF(V.DESC_COBRANCA_PLANO IS NULL,'cobrancaplano','valorescobranca') AS TABELA_ORIGEM,
                            C.nome,
                            5 AS tipo,
                            'N' AS flag,
                            0 AS idSolicitacoes,
                            C.idClientes AS Paciente,
                            C.convenio AS Plano,
                            COALESCE (
                                    CS.CUSTO,
                                    '0.00'
                            ) AS custo,
                            ('Equipamentos') AS apresentacao,
                            (S.qtd * (DATEDIFF(P.fim, P.inicio) + 1)) AS quantidade,
                            coalesce(V.valor,'0.00') AS valor,
                            C.empresa,
                            '' as codTuss

                    FROM    
                            `solicitacoes` S INNER JOIN
                            `clientes` C ON (S.paciente = C.idClientes)  INNER JOIN                            
                             prescricoes P ON (S.`idPrescricao`=P.id) INNER JOIN
                             cobrancaplanos CO ON (S.CATALOGO_ID = CO.`id`) LEFT JOIN
                             valorescobranca V ON (CO.`id` = V.`idCobranca` AND  C.convenio = V.`idPlano`)  LEFT JOIN
                             custo_servicos_plano_ur CS ON (CS.COBRANCAPLANO_ID =CO.id and CS.UR=C.empresa AND CS.STATUS = 'A')
                    WHERE
                          P.id ={$idPrescricao} AND
                          S.tipo=5 AND
                          S.TIPO_SOL_EQUIPAMENTO =1 AND
                           V.excluido_por is NULL
                 GROUP BY
                              CO.id,V.id
                 ORDER BY
                              Paciente, tipo,principio";
            } else if($tipo == 'relAditivoEnf'){
                $sql= "
                    SELECT
                            rel_enf_adt.ID,
                            u.nome AS medico,
                            u.idUsuarios,
                            DATE_FORMAT(rel_enf_adt.confirmado_em, '%d/%m/%Y - %T') AS sdata,
                            DATE_FORMAT(rel_enf_adt.INICIO, '%d/%m/%Y') AS inicio1,
                            DATE_FORMAT(rel_enf_adt.FIM, '%d/%m/%Y') AS fim1,
                            UPPER(c.nome) AS paciente,
                            c.idClientes AS CodPaciente,
                            'Solicitação Aditiva de Enfermagem' AS tipo,
                            FLOOR(
                                    DATEDIFF(CURDATE(), c.`nascimento`) / 365.25
                            ) AS idade,
                            e.nome AS empresan,
                            c.`nascimento`,
                            p.nome AS Convenio,
                            NUM_MATRICULA_CONVENIO,
                           p.tipo_medicamento
                    FROM
                            relatorio_aditivo_enf AS rel_enf_adt
                    INNER JOIN clientes AS c ON (
                            c.idClientes = rel_enf_adt.PACIENTE_ID
                    )
                    INNER JOIN usuarios AS u ON (rel_enf_adt.USUARIO_ID = u.idUsuarios)
                    INNER JOIN empresas AS e ON (e.id = c.empresa)
                    INNER JOIN planosdesaude AS p ON (p.id = c.convenio)
                    WHERE
                      rel_enf_adt.ID ='{$idPrescricao}'" ;
                        
                $sqlItens="SELECT
                            concat(B.principio,' - ',B.apresentacao) as principio,
                             S.NUMERO_TISS AS cod,
                            S.CATALOGO_ID as catalogo_id,
                            '' as TABELA_ORIGEM,
                            C.nome,
                            S.tipo,
                            'P' as flag,
                            S.idSolicitacoes,
                            C.idClientes AS Paciente,
                             C.convenio AS Plano,
                             coalesce(B.valorMedio,'0.00') AS custo,
                            (CASE S.tipo
                            WHEN '0' THEN 'Medicamento'
                            WHEN '1' THEN 'Material'
                            WHEN '3' THEN 'Dieta' END) as apresentacao,
                            (CASE WHEN COUNT(*)>1 THEN SUM(S.qtd) ELSE S.qtd END) AS quantidade,
                            coalesce(B.VALOR_VENDA,'0.00')  AS valor,
                            C.empresa,
                            B.TUSS as codTuss
                        FROM
                            `solicitacoes` S INNER JOIN
                            `clientes` C ON (S.paciente = C.idClientes)  INNER JOIN
                            `catalogo` B ON (S.CATALOGO_ID = B.ID)                              
                        WHERE

                            S.`relatorio_aditivo_enf_id`='{$idPrescricao}'
                             and S.tipo IN (0,1,3) 
                        GROUP BY
                            S.CATALOGO_ID
                            
                            
                            UNION
                        SELECT

                            IF (
                                    BC.DESC_COBRANCA_PLANO IS NULL,
                                    B.item,
                                    BC.DESC_COBRANCA_PLANO
                            ) AS principio,

                            IF (
                                    BC.DESC_COBRANCA_PLANO IS NULL,
                                    B.id,
                                    BC.COD_COBRANCA_PLANO
                            ) AS cod,

                            IF (
                                    BC.DESC_COBRANCA_PLANO IS NULL,
                                    B.id,
                                    BC.id
                            ) AS catalogo_id,

                            IF (
                                    BC.DESC_COBRANCA_PLANO IS NULL,
                                    'cobrancaplano',
                                    'valorescobranca'
                            ) AS TABELA_ORIGEM,
                             C.nome,
                             2 AS tipo,
                             'S' AS flag,
                             $idPrescricao AS idSolicitacoes,
                             C.idClientes AS Paciente,
                             C.convenio AS Plano,
                             COALESCE (
                                    CS.CUSTO,
                                    '0.00'
                            ) AS custo,
                             ('Servi&ccedil;os') AS apresentacao,
                             (
                                    SUM(
                                        CASE
                                        WHEN ESP.id IN (2, 3, 4, 5) THEN	

                                                (DATEDIFF(P.FIM, P.INICIO) + 1)
                                        ELSE
                                        IF (
                                                S.frequencia IN (
                                                        2,
                                                        3,
                                                        4,
                                                        5,
                                                        6,
                                                        7,
                                                        8,
                                                        9,
                                                        10,
                                                        11,
                                                        12,
                                                        13,
                                                        14,
                                                        15,
                                                        16,
                                                        17,
                                                        18,
                                                        19
                                                ),
                                                (DATEDIFF(P.FIM, P.INICIO) + 1) * F.qtd,
                                                CEILING(
                                                        (DATEDIFF(P.FIM, P.INICIO) + 1) / 7
                                                ) * F.SEMANA
                                        )				
                                        END	
                                   )
                               ) AS quantidade,
                             COALESCE (BC.valor, '0.00') AS valor,
                             C.empresa,
                             '' AS codTuss
                            FROM
                                    `solicitacao_rel_aditivo_enf` S
                            LEFT JOIN `frequencia` AS F ON (S.frequencia = F.id)
                            INNER JOIN relatorio_aditivo_enf P ON (S.`relatorio_enf_aditivo_id` = P.ID)
                            INNER JOIN `clientes` C ON (P.PACIENTE_ID = C.idClientes)
                            INNER JOIN cuidadosespeciais AS ESP ON (S.CATALOGO_ID = ESP.id)
                            INNER JOIN `cobrancaplanos` B ON (ESP.COBRANCAPLANO_ID = B.id)
                            LEFT JOIN custo_servicos_plano_ur CS ON (
                                    CS.COBRANCAPLANO_ID = B.id
                                    AND CS.UR = C.empresa
                                    AND CS.STATUS = 'A'
                            )
                            LEFT JOIN `valorescobranca` BC ON (
                                    B.id = BC.idCobranca
                                    AND BC.idPlano = C.convenio and BC.excluido_por is null 
                            )
                            INNER JOIN planosdesaude AS PL ON (C.convenio = PL.id)
                            WHERE
                                    P.ID ={$idPrescricao}
                            AND S.tipo IN (4)
                            AND S.CATALOGO_ID NOT IN (17, 18, 21, 24)
                            AND BC.excluido_por is NULL
                            GROUP BY
                                    B.id,
                                    BC.id
                          
                      UNION
                      SELECT DISTINCT
                            IF(V.DESC_COBRANCA_PLANO IS NULL,CO.item,V.DESC_COBRANCA_PLANO)AS principio,
                            IF(V.DESC_COBRANCA_PLANO IS NULL,CO.id,V.COD_COBRANCA_PLANO) AS cod,
                            IF(V.DESC_COBRANCA_PLANO IS NULL,CO.id,V.id) AS catalogo_id,
                            IF(V.DESC_COBRANCA_PLANO IS NULL,'cobrancaplano','valorescobranca') AS TABELA_ORIGEM,
                            C.nome,
                            5 AS tipo,
                            'N' AS flag,
                            0 AS idSolicitacoes,
                            C.idClientes AS Paciente,
                            C.convenio AS Plano,
                            COALESCE (
                                    CS.CUSTO,
                                    '0.00'
                            ) AS custo,
                            ('Equipamentos') AS apresentacao,
                             (S.qtd *(DATEDIFF(rel_enf_adt.FIM, rel_enf_adt.INICIO) + 1))  AS quantidade,
                            coalesce(V.valor,'0.00') AS valor,
                            C.empresa,
                            '' as codTuss
                      FROM    
                            `solicitacoes` S INNER JOIN
                            `clientes` C ON (S.paciente = C.idClientes)  INNER JOIN                       
                            cobrancaplanos CO ON (S.CATALOGO_ID = CO.`id`) LEFT JOIN
                            valorescobranca V ON (CO.`id` = V.`idCobranca` AND  C.convenio = V.`idPlano`)  LEFT JOIN
                            custo_servicos_plano_ur CS ON (CS.COBRANCAPLANO_ID =CO.id  and CS.UR=C.empresa AND CS.STATUS = 'A') INNER JOIN
                             relatorio_aditivo_enf AS rel_enf_adt on (S.`relatorio_aditivo_enf_id`=rel_enf_adt.ID)
                      WHERE
                            S.`relatorio_aditivo_enf_id` ={$idPrescricao} AND
                            S.tipo=5 AND
                            S.TIPO_SOL_EQUIPAMENTO =1 AND
                            V.excluido_por is NULL
                      GROUP BY
                         CO.id,V.id
                      ORDER BY
                         Paciente, tipo,principio";
            }else if ($tipo == 'prescCurativoEnf'){
                $sql= "
                    SELECT
                        presc.id,
                        u.nome AS medico,
                        u.idUsuarios,
                        DATE_FORMAT(presc. registered_at, '%d/%m/%Y - %T') AS sdata,
                        DATE_FORMAT(presc.inicio, '%d/%m/%Y') AS inicio1,
                        DATE_FORMAT(presc.fim, '%d/%m/%Y') AS fim1,
                        UPPER(c.nome) AS paciente,
                        c.idClientes AS CodPaciente,
                        (
                         'Prescrição Aditiva de  Cutrativo de Enfermagem'
                        ) AS tipo,
                        FLOOR(
                                DATEDIFF(CURDATE(), c.`nascimento`) / 365.25
                        ) AS idade,
                        e.nome AS empresan,
                        c.`nascimento`,
                        p.nome AS Convenio,
                        NUM_MATRICULA_CONVENIO,
                        p.tipo_medicamento

                    FROM
                                prescricao_curativo AS presc
                        INNER JOIN clientes AS c ON (
                                c.idClientes = presc.paciente
                        )
                        INNER JOIN usuarios AS u ON (presc.profissional = u.idUsuarios)
                        INNER JOIN empresas AS e ON (e.id = c.empresa)
                        INNER JOIN planosdesaude AS p ON (p.id = c.convenio)
                    WHERE
                        presc.id ='{$idPrescricao}'" ;
                $sqlItens="SELECT
                            concat(B.principio,' - ',B.apresentacao) as principio,
                             S.NUMERO_TISS AS cod,
                            S.CATALOGO_ID as catalogo_id,
                            '' as TABELA_ORIGEM,
                            C.nome,
                            S.tipo,
                            'P' as flag,
                            S.idSolicitacoes,
                            C.idClientes AS Paciente,
                             C.convenio AS Plano,
                            coalesce(B.valorMedio,'0.00') AS custo,
                            (CASE S.tipo
                            WHEN '0' THEN 'Medicamento'
                            WHEN '1' THEN 'Material'
                            WHEN '3' THEN 'Dieta' END) as apresentacao,
                            (CASE WHEN COUNT(*)>1 THEN SUM(S.qtd) ELSE S.qtd END) AS quantidade,
                             coalesce(B.VALOR_VENDA ,'0.00') AS valor,
                            C.empresa,
                            B.TUSS as codTuss
                        FROM
                            `solicitacoes` S INNER JOIN
                            `clientes` C ON (S.paciente = C.idClientes)  INNER JOIN
                            `catalogo` B ON (S.CATALOGO_ID = B.ID) INNER JOIN
                             prescricao_curativo P ON (S.`id_prescricao_curativo`=P.id)

                        WHERE

                            S.`id_prescricao_curativo`='{$idPrescricao}'
                             and S.tipo IN (0,1,3)
                        GROUP BY
                            catalogo_id
                             ORDER BY
                         Paciente, tipo,principio";

            }else if($tipo == 'prescEnf'){
                       $sql = "SELECT
                        presc.id,
                        u.nome AS medico,
                        u.idUsuarios,
                        DATE_FORMAT(presc. data, '%d/%m/%Y - %T') AS sdata,
                        DATE_FORMAT(presc.inicio, '%d/%m/%Y') AS inicio1,
                        DATE_FORMAT(presc.fim, '%d/%m/%Y') AS fim1,
                        UPPER(c.nome) AS paciente,
                        c.idClientes AS CodPaciente,
                        (
                         'Prescrição Aditiva de Enfermagem'
                        ) AS tipo,
                        FLOOR(
                                DATEDIFF(CURDATE(), c.`nascimento`) / 365.25
                        ) AS idade,
                        e.nome AS empresan,
                        c.`nascimento`,
                        p.nome AS Convenio,
                        NUM_MATRICULA_CONVENIO,
                        p.tipo_medicamento

                    FROM
                                prescricao_enf AS presc
                        INNER JOIN clientes AS c ON (
                                c.idClientes = presc.cliente_id
                        )
                        INNER JOIN usuarios AS u ON (presc.user_id = u.idUsuarios)
                        INNER JOIN empresas AS e ON (e.id = c.empresa)
                        INNER JOIN planosdesaude AS p ON (p.id = c.convenio)
                    WHERE
                        presc.id  ='{$idPrescricao}'
                       ";
                $sqlItens = "SELECT
 IF (
                                    BC.DESC_COBRANCA_PLANO IS NULL,
                                    B.item,
                                    BC.DESC_COBRANCA_PLANO
                            ) AS principio,

                            IF (
                                    BC.DESC_COBRANCA_PLANO IS NULL,
                                    B.id,
                                    BC.COD_COBRANCA_PLANO
                            ) AS cod,

                            IF (
                                    BC.DESC_COBRANCA_PLANO IS NULL,
                                    B.id,
                                    BC.id
                            ) AS catalogo_id,

                            IF (
                                    BC.DESC_COBRANCA_PLANO IS NULL,
                                    'cobrancaplano',
                                    'valorescobranca'
                            ) AS TABELA_ORIGEM,
                             C.nome,
                             2 AS tipo,
                             'S' AS flag,
                             S.prescricao_enf_id AS idSolicitacoes,
                             C.idClientes AS Paciente,
                             C.convenio AS Plano,
                             COALESCE (
                                    CS.CUSTO,
                                    '0.00'
                            ) AS custo,
                             ('Servi&ccedil;os') AS apresentacao,
                             (
                                    SUM(
                                        CASE
                                        WHEN ESP.id IN (2, 3, 4, 5) THEN

                                                (DATEDIFF(S.fim, S.inicio) + 1)
                                        ELSE
                                        IF (
                                                S.frequencia_id IN (
                                                        2,
                                                        3,
                                                        4,
                                                        5,
                                                        6,
                                                        7,
                                                        8,
                                                        9,
                                                        10,
                                                        11,
                                                        12,
                                                        13,
                                                        14,
                                                        15,
                                                        16,
                                                        17,
                                                        18,
                                                        19
                                                ),
                                                (DATEDIFF(S.fim, S.inicio) + 1) * F.qtd,
                                                CEILING(
                                                        (DATEDIFF(S.fim, S.inicio) + 1) / 7
                                                ) * F.SEMANA
                                        )
                                        END
                                   )
                               ) AS quantidade,
                             COALESCE (BC.valor, '0.00') AS valor,
                             C.empresa,
                             '' AS codTuss
FROM
prescricao_enf as P
INNER JOIN prescricao_enf_itens_prescritos as S ON P.id = S.prescricao_enf_id
LEFT JOIN `frequencia` AS F ON (S.frequencia_id = F.id)
INNER JOIN prescricao_enf_cuidado ON S.presc_enf_cuidados_id = prescricao_enf_cuidado.id
INNER JOIN clientes as C ON P.cliente_id = C.idClientes
INNER JOIN cuidadosespeciais as ESP on prescricao_enf_cuidado.cuidado_especiais_id =ESP.id
INNER JOIN cobrancaplanos  as B on ESP.COBRANCAPLANO_ID = B.id
LEFT JOIN `valorescobranca` as BC ON (
                                    B.id = BC.idCobranca
                                    AND BC.idPlano = C.convenio

                            )
LEFT JOIN custo_servicos_plano_ur CS ON (CS.COBRANCAPLANO_ID =B.id  and CS.UR=C.empresa AND CS.STATUS = 'A')

WHERE
 P.id ={$idPrescricao}
AND prescricao_enf_cuidado.presc_enf_grupo_id = 7
AND prescricao_enf_cuidado.cuidado_especiais_id NOT IN (17, 18, 21, 24)
AND BC.excluido_por is NULL";
            }

           

            $result = mysql_query($sql);
            $num_row= mysql_num_rows($result);
             if(!$result){
                echo "<p> "
                      . "<b>ERRO: Problema na pesquisa entre em contato com o TI.</b>"
                   . "</p> </div>";
                return;
            }   
             if($num_row == 0){
                 echo "<p>"
                       . "<b>Nenhuma ".htmlentities('prescrição')." foi encontrada.</b>"
                   . "</p> </div>";
                 return;
             }
             while ($pessoa = mysql_fetch_array($result)) {
                    $tipoMedicamento = $pessoa['tipo_medicamento'];
                    $nomePlano = $pessoa['Convenio'];
                $idade = join("/",array_reverse(explode("-",$pessoa['nascimento'])))."(".$pessoa['idade']." anos)";

                           if($idade == "00/00/0000( anos)")
                                $idade='';
                                        $html.="<table width='100%'>";
                                        $html.= "<tr bgcolor='#EEEEEE'>";
                                        $html.= "<td  style='width:60%;'><b>PACIENTE:</b></td>";
                                        $html.= "<td style='width:20%;' ><b>SEXO </b></td>";
                                        $html.= "<td style='width:30%;'><b>IDADE:</b></td>";
                                        $html.= "</tr>";
                                        
                                        $html.= "<tr>";
                                        $html.= "<td style='width:60%;'>{$pessoa['paciente']}</td>";
                                        $html.= "<td style='width:20%;'>{$pessoa['sexo1']}</td>";
                                        $html.= "<td style='width:30%;'>{$idade}</td>";
                                        $html.= "</tr>";

                                        $html.= "<tr bgcolor='#EEEEEE'>";
                                        $html.= "<td style='width:60%;'><b>UNIDADE REGIONAL:</b></td>";
                                        $html.= "<td style='width:20%;'><b>CONV&Ecirc;NIO </b></td>";
                                        $html.= "<td style='width:30%;'><b>MATR&Iacute;CULA </b></td>";
                                        $html.= "</tr>";
                                        
                                        $html.= "<tr>";
                                        $html.= "<td style='width:60%;'>{$pessoa['empresan']}</td>";
                                        $html.= "<td style='width:20%;'>".strtoupper($pessoa['Convenio'])."</td>";
                                        $html.= "<td style='width:30%;'>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
                                        $html.= "</tr>";
                                        $html.= "<tr bgcolor='#EEEEEE'>";
                                        $html.=    "<td colspan='3'>"
                                                    . "{$pessoa['tipo']} "
                                                    . "feita por {$pessoa['medico']} em {$pessoa['sdata']}"
                                                    . " de {$pessoa['inicio1']} até {$pessoa['fim1']} "
                                                  . "</td>"
                                               . "</tr>";
                                         $html.= "<tr bgcolor=''>";
                                        $html.=    "<td colspan='3'>"
                                                    . " <b>Período do Orcamento:</b> {$pessoa['inicio1']} até {$pessoa['fim1']} "
                                                  . "</td>"
                                               . "</tr>";
                                        $html.='</table>';
                                        $html.="<input type='hidden' id='fim_fatura' value='{$pessoa['fim1']}'>";
                                        $html.="<input type='hidden' id='inicio_fatura' value='{$pessoa['inicio1']}' >";
                            
                    echo $html;
                    
            }
            
                 
            
           $resultItens = mysql_query($sqlItens);
            $num_row= mysql_num_rows($resultItens);
             if(!$resultItens){
                echo "<p> "
                      . "<b>ERRO: Problema na pesquisa de itens do orçamento, entre em contato com o TI.</b>"
                   . "</p></div>";
                return;
            }   
             if($num_row == 0){
                 echo "<p>"
                       . "<b>Nenhuma ".htmlentities('prescrição')." foi encontrada.</b>"
                   . "</p> </div>";
                 return;
             }
            $total=0.00;
    $cont=0;
    $nome_titulo='';
    $subtotal=0.00;
    $cod_ident = 0;

    $this->formulario();
    echo "<center><h2>{$label_tipo}</h2></center><br/>";
    $total=0.00;
    $cont=0;
    $nome_titulo='';
    $subtotal=0.00;
    $cod_ident = 0;
     echo "<div id='dialog-zerados-salvar-orcamento-aditivo' lote='' title='Salvar Fatura'
               dados='' inicio='' fim=''   cod='' av=''	 plano=''  cap='' obs= '' >
	       <p> 
                  <b>Deseja salvar fatura com  itens zerados?</b>
               </p>
           </div>";


    while($row = mysql_fetch_array($resultItens)){



      $total +=$row['quantidade']*$row['valor'];
      $plano = $row['Plano'];
      if($nome_titulo != $row['nome']){

        $subtotal=0.00;
        $subtotal_equipamento=0.00;
        $subtotal_material=0.00;
        $subtotal_medicamento=0.00;
        $subtotal_dieta=0.00;
        $subtotal_servico=0.00;
        $subtotal_gasoterapia=0.00;

        $cod_ident =$row['Paciente'];
        echo"<div id ='div_{$cod_ident}' >";
        echo "</br>
                         <table width=100% style='border:2px dashed;'>
                            <tr><td><center><b>OBSERVA&Ccedil;&Otilde;ES</b></center></td></tr>
                            <tr><td>Na coluna TAXA deve se colocar o percentual se for desconto colocar como negativo ex: -1,00. Caso seja acrescimo positivo ex: 1,00 </td></tr>
                            <tr><td>Na coluna deve-se marcar os itens que não vão ser cobrados porque fazem parte do Pacote</td></tr>
                            </table>
                          </br>";
        //echo "<button id='{$cod_ident}' style='float:left;' inicio='{$inicio}'  fim='{$fim}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover add-item-fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>+</span></button>";
        echo "<br/><table class='mytable' width=100%  >";
        echo "<a><thead class='mostrar' idpaciente='{$cod_ident}'><tr><th colspan='5' style='background-color:#ccc;'><center width='70%' style='vertical-align:middle;'><b>".$row['nome']."</b></center></th></tr></thead></a>";
        echo"</table><table  width=100% class='ocultar mytable' id='tabela_{$cod_ident}'>";
        echo "<tr>
		    	<th>Item</th>
		    	<th>Tipo</th>
		    	<th>Unidade</th>
		    	<th>Qtd</th>
		    	<th>Pre&ccedil;o Unit.</th>
		    	<th>Pre&ccedil;o Total</th>
		    	<th>Taxa</th>
		    	<th>Pacote</th>
		    	</tr>
		    	<tr>
		    	    <td colspan='4'>
		    	        <b>
		    	            <input type='checkbox' class='selecionar-todos'
		    	                paciente='{$cod_ident}' />
		    	                Selecionar todos para serem removidos
                        </b>
                    </td>
                    <td colspan='4' align='right' style='padding-right: 25px;'>
                        <b>Selecionar todos para pacote <input type='checkbox' class='selecionar-todos-pacote' paciente='{$cod_ident}'></b>
                    </td>
                </tr>";
        $nome_titulo = $row['nome'];

      }

      $subtotal +=$row['quantidade']*$row['valor'];
      if($row['tipo'] == '1'){
        $subtotal_material +=$row['quantidade']*$row['valor'];
      }
      if($row['tipo'] == '0'){
        $subtotal_medicamento +=$row['quantidade']*$row['valor'];
      }
      if($row['tipo'] == '2'){
        $subtotal_servico +=$row['quantidade']*$row['valor'];
      }
      if($row['tipo'] == '3'){
        $subtotal_dieta +=$row['quantidade']*$row['valor'];
      }
      if($row['tipo'] == '5'){
        $subtotal_equipamento +=$row['quantidade']*$row['valor'];
      }
    //  $botoes = '<img align=\'right\' class=\'rem-item-solicitacao\'  src=\'../utils/delete_16x16.png\' title=\'Remover\' border=\'0\'>';
      //    if($nome_titulo!=$row['nome']){
        $botoes = "<br><input type='checkbox' class=' excluir-item excluir-item-paciente-{$cod_ident}'/> <b> Excluir</b>";



        $codigoitem =$row['cod'];
      $tuss =$row['codTuss'];
      //////2014-01-16
      ///////// se for material coloca a descrição igual do Simpro que vai ser para fatura porque o principio é um ‘nome genérico’ que as enfermeiras usam.
      if($row['tipo'] == 1 && $row['segundonome']!= NULL){
        $nomeitem =$row['segundonome'];
      }else{
        $nomeitem = $row['principio'];
      }

      ////////Verifica se o plano é petrobras.
      if($plano == 80){
        $checked="checked='checked'";
      }else{
        $checked="";
      }
      //	if($codigoitem != 0 && $row['catalogo_id'] == 0){
      echo "<tr  class='itens_fatura {$cod_ident}' >
		    	<td width='50%'>TUSS-{$tuss} N-{$codigoitem} - {$nomeitem}{$botoes}</td>
		    	<td>{$row['apresentacao']}</td>
		    	<td>".unidades_fatura(NULL)."</td>

		    	<td><input class='qtd' size='7' value='{$row['quantidade']}' paciente='{$row['Paciente']}'/></td>
		    	<td>R$<input type='text' name='valor_plano' size='10' class='valor_plano valor OBG' tiss='{$row['cod']}' catalogo_id='{$row['catalogo_id']}' qtd='{$row['quantidade']}' paciente='{$row['Paciente']}' tuss='{$tuss}' tabela_origem='{$row['TABELA_ORIGEM']}' solicitacao='{$row['idSolicitacoes']}' tipo='{$row['tipo']}' inicio='{$inicio}' fim='{$fim}' custo = '{$row['custo']}' usuario='{$_SESSION["id_user"]}' ord = '{$cont}' flag='{$row['flag']}' value='".number_format($row['valor'],2,',','.')."'></td>
		    	<td><span style='float:right;' id='val_total{$cont}'><b>R$  <span nome='val_linha'>".number_format($row['valor']*$row['quantidade'],2,',','.')."</span></b></span></td>
		    	<td><input type='text' name='taxa' size='10' class='taxa OBG' value='0,00'></td>
                        <td><input type='checkbox' $checked name='incluso_pacote' size='10' class='incluso_pacote incluso-pacote-{$cod_ident} OBG'></td>

                           </tr>";
      $cont++;
      //}

      $empresa=$row['empresa'];



    }
    echo "</table>";
    
    echo "<button id_paciente='{$cod_ident}' id='{$cod_ident}' nomeplano = '{$nomePlano}' tipo_medicamento = '{$tipoMedicamento}' contador=0 plano='{$plano}' empresa='{$empresa}' style='float:left;' inicio='{$inicio}'  fim='{$fim}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover add-item-orcamento-aditivo'  role='button' aria-disabled='false' ><span class='ui-button-text'>+</span></button>";
          echo "<button  cod='{$cod_ident}'  plano='{$plano}' presc_av='0' nome='$nome_titulo' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover excluir-todos ' role='button' aria-disabled='false' ><span class='ui-button-text'>Excluir Itens</span></button>";

          echo "<button  cod='{$cod_ident}' tabela='{$tipo}' id-prescricao = '{$idPrescricao}'  plano='{$plano}' presc_av='3' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover ' id='finalizar-orcamento-aditivo' role='button' aria-disabled='false' ><span class='ui-button-text'>Or&ccedil;a. Aditivo</span></button><br/>";
    

    echo "<table class='mytable' width=100% id='lista-itens'>";
    echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_quipamento_".$cod_ident."'><b>SUBTOTAL EQUIPAMENTO: R$</b> <b><i id='equipamento_".$cod_ident."'>".number_format($subtotal_equipamento,2,',','.')."</i></b></span></td></tr>";
    echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_servico_".$cod_ident."'><b>SUBTOTAL SERVI&Ccedil;O: R$</b> <b><i id='servico_".$cod_ident."'>".number_format($subtotal_servico,2,',','.')."</i></b></span></td></tr>";
    echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_material_".$cod_ident."'><b>SUBTOTAL MATERIAL: R$</b> <b><i id='material_".$cod_ident."'>".number_format($subtotal_material,2,',','.')."</i></b></span></td></tr>";
    echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_medicamento_".$cod_ident."'><b>SUBTOTAL MEDICAMENTO: R$</b> <b><i id='medicamento_".$cod_ident."'>".number_format($subtotal_medicamento,2,',','.')."</i></b></span></td></tr>";
    echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_dieta_".$cod_ident."'><b>SUBTOTAL DIETA: R$</b> <b><i id='dieta_".$cod_ident."'>".number_format($subtotal_dieta,2,',','.')."</i></b></span></td></tr>";
    echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_gasoterapia_".$cod_ident."'><b>SUBTOTAL GASOTERAPIA</b> <b><i id='gasoterapia_".$cod_ident."'>".number_format($subtotal_gasoterapia,2,',','.')."</i></b></span></td></tr>";

    echo "<tr><td colspan='5' style='font-size:12px;'><span style='float:right;' class='sub_fatura_".$cod_ident."'><b>TOTAL PACIENTE: R$</b> <b><i>".number_format($subtotal,2,',','.')."</i></b></span></td></tr>";
    echo "<tr><td colspan='6' id='obs_".$cod_ident."'><span id='span_{$cod_ident}' class='span_obs'><b>OBSERVA&Ccedil;&Atilde;O:</b></br><textarea cols=120 rows=5 {$acesso} name='obs' class='' id='text_{$cod_ident}'></textarea></span></td></tr>";

    echo"</table><table class='mytable' width=100% >";
    echo "<tr><td colspan='5' style='font-size:12px;'><span style='float:right;' cont='{$cont}' cont1='0' id='total_fatura'><b>TOTAL: R$</b> <b><i>".number_format($total,2,',','.')."</i></b></span></td></tr>";
    echo "</table></div>";
            
      echo"</div>";
    }

}



