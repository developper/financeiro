<?php

$id = session_id();
if(empty($id))
    session_start();
include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/codigos.php');
require __DIR__ . '/../vendor/autoload.php';

//ini_set('display_errors',1);

use \App\Models\Faturamento\GuiaSADT;
use \App\Core\Config;
use \App\Services\Gateways\SendGridGateway;
use \App\Services\MailAdapter;
use \App\Models\Faturamento\Fatura as ModelFatura;
use \App\Models\Logistica\Saidas as ModelSaidas;
use \App\Models\Sistema\Equipamentos as ModelEquipamento;
use \App\Models\Logistica\Devolucao as ModelDevolucao;
use \App\Models\Faturamento\Consenso;
use \App\Models\Administracao\Filial;

function formulario(){
    echo "<div id='dialog-formulario' title='Formul&aacute;rio' tipo-documento='' tipo='' tipo_medicamento='' classe='' cod='' plano='' empresa=''>";
    echo alerta();
    echo "<div id='msg-generico'></div>";
    echo "<center><div id='tipos'>
	<input type='radio' id='bmed' name='tipos' checked='checked' value='med' /><label for='bmed'>Medicamentos</label>
	<input type='radio' id='bdie' name='tipos' value='die' /><label for='bdie'>Dieta</label>
	<input type='radio' id='bmat' name='tipos' value='mat' /><label for='bmat'>Materiais</label>
	<input type='radio' id='bserv' name='tipos' value='serv' /><label for='bserv'>Servi&ccedil;os</label>
	<input type='radio' id='bgas' name='tipos'  value='gas' /><label for='bgas'>Gasoterapia</label>
	<input type='radio' id='bequip' name='tipos' value='eqp' /><label for='bequip'>Equipamento</label>
	<input type='radio' id='bextra' name='tipos' value='ext' /><label for='bextra'>Extras</label>
	<input type='radio' id='bkit' name='tipos' value='kit' /><label for='bkit'> Kits</label>

	</div></center>";
    echo "<p><b>Busca:</b><br/><input type='text' name='busca' id='busca-item' />";
    echo "<div id='opcoes-kits'></div>";
    echo "<input type='hidden' name='cod' id='cod' value='-1' />";
    echo "<input type='hidden' name='tipo' id='tipo' value='0' />";
    echo "<input type='hidden' name='catalogo_id' id='catalogo_id' value='-1' />";
    echo "<input type='hidden' name='apresentacao' id='apresentacao' value='-1' />";
    echo "<input type='hidden' name='custo' id='custo' value='-1' />";
    echo "<input type='hidden' name='valor_plano' id='valor_plano' value='-1' />";
    echo "<input type='hidden' name='inicio' id='inicio' value='-1' />";
    echo "<input type='hidden' name='fim' id='fim' value='-1' />";
    echo "<input type='hidden' name='paciente' id='paciente' value='-1' />";
    echo "<input type='hidden' name='contador' id='contador' value='-1' />";
    echo "<input type='hidden' name='tuss' id='tuss' value='-1' />";
    echo "<input type='hidden' name='tabela_origem' id='tabela_origem' value='-1' />";
    echo "<p><b>Nome:</b><input type='text' name='nome' id='nome' class='OBG' />";

    echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
    echo "<button id='add-item' tipo='' >+</button></div>";
}

function formulario_simulacao(){
    echo "<div id='dialog-formulario-simulacao' title='Formul&aacute;rio' tipo='' tipo_medicamento='' classe='' cod='' plano='' empresa='' style='display:none;'>";
    echo alerta();
    echo "<div id='msg-generico'></div>";
    echo "<center><div id='tipos'>
	<input type='radio' id='bmed' name='tipos' value='med' /><label for='bmed'>Medicamentos</label>
	<input type='radio' id='bdie' name='tipos' value='die' /><label for='bdie'>Dieta</label>
	<input type='radio' id='bmat' name='tipos' value='mat' /><label for='bmat'>Materiais</label>
	<input type='radio' id='bserv' name='tipos' value='serv' /><label for='bserv'>Servi&ccedil;os</label>
	<input type='radio' id='bgas' name='tipos'  value='gas' /><label for='bgas'>Gasoterapia</label>
	<input type='radio' id='bequip' name='tipos' value='eqp' /><label for='bequip'>Equipamento</label>
	<input type='radio' id='bextra' name='tipos' value='ext' /><label for='bextra'>Extras</label>
	<input type='radio' id='bkit' name='tipos' value='kit' /><label for='bkit'> Kits</label>

	</div></center>";
    echo "<p><b>Busca:</b><br/><input type='text' name='busca' id='busca-item' />";
    echo "<div id='opcoes-kits'></div>";
    echo "<input type='hidden' name='cod' id='cod' value='-1' />";
    echo "<input type='hidden' name='tipo' id='tipo' value='0' />";
    echo "<input type='hidden' name='catalogo_id' id='catalogo_id' value='-1' />";
    echo "<input type='hidden' name='apresentacao' id='apresentacao' value='-1' />";
    echo "<input type='hidden' name='custo' id='custo' value='-1' />";
    echo "<input type='hidden' name='valor_plano' id='valor_plano' value='-1' />";
    echo "<input type='hidden' name='inicio' id='inicio' value='-1' />";
    echo "<input type='hidden' name='fim' id='fim' value='-1' />";
    echo "<input type='hidden' name='paciente' id='paciente' value='-1' />";
    echo "<input type='hidden' name='contador' id='contador' value='-1' />";
    echo "<input type='hidden' name='tuss' id='tuss' value='-1' />";
    echo "<input type='hidden' name='tabela_origem' id='tabela_origem' value='-1' />";
    echo "<p><b>Nome:</b><input type='text' name='nome' id='nome' class='OBG' />";

    echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
    echo "<button id='add-item-simulacao' tipo='' >+</button></div>";
}

function listar_itens($tipo, $cod, $inicio, $fim, $nome, $refaturar, $plano){
    $sql = "";
    $label_tipo = "";
    $tipo = anti_injection($tipo,'literal');
    $cod = anti_injection($cod,'numerico');
    $inicio = anti_injection($inicio,'literal');
    $fim = anti_injection($fim,'literal');
    $nome = anti_injection($nome,'literal');
    $plano = anti_injection($plano,'numerico');
    $refaturarmed ='';
    $refaturarserv ='';
    $refaturareqp ="AND E.DATA_INICIO <> '0000-00-00 00:00:00' AND
                    ( ( E.DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}' ) OR
                   ( E.DATA_FIM BETWEEN '{$inicio}' AND '{$fim}') OR
                   (E.DATA_INICIO < '{$inicio}' AND (  E.`DATA_FIM`='0000-00-00 00:00:00' OR E.`DATA_FIM` >'{$fim}' ))) ";
    switch($tipo){
        case 'paciente':
            ///Caso queira fazer a fatura do paciente para um plano diferente que ela esta associado no sistema $plano vai ser diferente de 0
            if($plano ==0){
                $cond1 = "C.convenio";
            }else{
                $cond1 = $plano;
            }

            $label_tipo = "FATURA DO PACIENTE {$nome}";
            $sql = ModelFatura::getItensPreFaturaByPaciente($cod, $cond1, $inicio, $fim, $refaturarmed, $refaturarserv, $refaturareqp );
            $paciente_id = $cod;
            break;
        case 'unidade':
            if($plano == 0){
                $cond1 = '';
            }else{
                $cond1 = "C.convenio ={$plano} ";
            }
            $label_tipo = "FATURA DA UNIDADE {$nome}";
            $sql = ModelFatura::getItensPreFaturaByUR($cod, $cond1, $inicio, $fim, $refaturarmed, $refaturarserv, $refaturareqp );
            break;
        case 'plano':
            $label_tipo = "FATURA DO PLANO {$nome}";
            $sql = ModelFatura::getItensPreFaturaByPlano($cod, $inicio, $fim, $refaturarmed, $refaturarserv, $refaturareqp );

            break;
    }

    echo formulario();

    //die($sql);

    $result = mysql_query($sql) or die (mysql_error());
    $resultado = mysql_num_rows($result);
    $htmlDevolucaoEnvio = "";
    if($tipo == 'paciente'){
        $devolucoes = ModelDevolucao::getDevolucaoByPaciente($cod, $inicio, $fim);
        $envios = ModelSaidas::getSaidaEquipamentoByPaciente($cod, $inicio, $fim);
        $inicioBr = implode('/',array_reverse(explode('-',$inicio)));
        $fimBr = implode('/',array_reverse(explode('-',$fim)));

        if(!empty($devolucoes)){
            $htmlDevolucaoEnvio = "<table class='mytable mostrar-devolucao' width='100%'>
										<thead>
											<tr>
												<th colspan='8'><center>Devoluções entre {$inicioBr} até {$fimBr} </center></th>
											</tr>
										</thead>
										<tbody id='tbody-devolucao' class='ocultar-devolucao'>
											<tr style='background-color:grey' >
												<td colspan='4'>ITEM</td>
												<td>TIPO</td>
												<td>QUANTIDADE</td>
												<td>LOTE</td>
												<td>DATA</td>
											</tr>";
            foreach($devolucoes as $d){
                $htmlDevolucaoEnvio .= "<tr>
												<td colspan='4'>{$d['nome']}</td>
												<td>{$d['nomeTipo']}</td>
												<td>{$d['qtd']}</td>
												<td>{$d['LOTE']}</td>
												<td>".implode('/',array_reverse(explode('-',$d['DATA_DEVOLUCAO'])))."</td>
											</tr>";

            }
            $htmlDevolucaoEnvio .= "</tbody>
									</table><br>";

        }

        if(!empty($envios) ){
            $htmlDevolucaoEnvio .= "<table class='mytable mostrar-envio' width='100%'>
										<thead>
											<tr>
												<th colspan='8'><center>Equipamentos enviados entre {$inicioBr} até {$fimBr}</center> </th>
											</tr>
										</thead>
										<tbody id='tbody-envio' class='ocultar-envio'>
											<tr style='background-color:grey'>
												<td colspan='4'>ITEM</td>
												<td>TIPO</td>
												<td>QUANTIDADE</td>
												<td>LOTE</td>
												<td>DATA</td>
											</tr>";

            foreach($envios as $e){
                $htmlDevolucaoEnvio .= "<tr>
												<td colspan='4'>{$e['nome']}</td>
												<td>Equipamento</td>
												<td>{$e['qtd']}</td>
												<td>{$e['LOTE']}</td>
												<td>".implode('/',array_reverse(explode('-',$e['data'])))."</td>
											</tr>";
            }
            $htmlDevolucaoEnvio .= "</tbody>
								</table><br>";
        }

        $equipamentoAtivos = ModelEquipamento::getEquipamentosAtivosByPaciente($cod);
        if(!empty($equipamentoAtivos) ){
            $htmlDevolucaoEnvio .= "<table class='mytable mostrar-equipamento-ativo' width='100%'>
										<thead>
											<tr>
												<th colspan='8'><center>Equipamentos ativos.</center> </th>
											</tr>
										</thead>
										<tbody id='tbody-equipamento-ativo' class='ocultar-equipamento-ativo'>
											<tr style='background-color:grey'>
												<td>Equipamento</td>
												<td>Data Envio</td>
											</tr>";

            foreach($equipamentoAtivos as $ativos){
                $htmlDevolucaoEnvio .= "<tr>
												<td>{$ativos['item']}</td>
												<td>{$ativos['datapt']}</td>
                                        </tr>";
            }
            $htmlDevolucaoEnvio .= "</tbody>
								</table><br>";

    }
    }

    if($resultado != 0){
        echo "</br>
              <table width=100% style='border:2px dashed;'>
                    <tr><td><center><b>OBSERVA&Ccedil;&Otilde;ES</b></center></td></tr>
                    <tr><td>Na coluna TAXA deve se colocar o percentual se for desconto colocar como negativo ex: -1,00. Caso seja acrescimo positivo ex: 1,00 </td></tr>
                    <tr><td>Na coluna deve-se marcar os itens que não vão ser cobrados porque fazem parte do Pacote</td></tr>
              </table>
              </br>";

        echo "<center><h2>{$label_tipo}</h2></center><br/>";

        $total=0.00;
        $cont=0;
        $nome_titulo='';
        $subtotal=0.00;
        $cod_ident = 0;

        while($row = mysql_fetch_array($result, MYSQL_ASSOC)){

            $total +=$row['quantidade']*$row['valor'];

            if($tipo=='plano' || $tipo=='unidade'){
                if($nome_titulo != $row['nome']){
                    if(!empty($nome_titulo)){
                        echo "</table>";
                        echo "<button id='{$cod_ident}' style='float:left;' contador=0 empresa='{$row['empresa']}' tipo_medicamento='{$tipo_medicamento}' plano='{$plano}'  nomeplano='{$nomeplano}' inicio='{$inicio}'  fim='{$fim}' tipo-documento = 0 class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover add-item-fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>+</span></button>";
                        echo "<button  cod='{$cod_ident}'  plano='{$plano}' presc_av='0' nome='$nome_titulo' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover excluir-todos ' role='button' aria-disabled='false' ><span class='ui-button-text'>Excluir Itens</span></button>";
                        echo "<button  cod='{$cod_ident}'  plano='{$plano}' empresa='{$row['empresa']}' presc_av='0' id-fatura='0' id='salvar-paciente-{$cod_ident}' nome='$nome_titulo' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover salvar_fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar Rascunho</span></button>";

                        echo "<button  cod='{$cod_ident}'  plano='{$plano}' presc_av='0'  id-fatura='0' id='finalizar-paciente-{$cod_ident}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover faturar' role='button' aria-disabled='false' ><span class='ui-button-text'>Faturar</span></button>";
                        echo "<button paciente='{$cod_ident}' style='float:left;' inicio='{$inicio}'  fim='{$fim}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover obs' role='button' aria-disabled='false' ><span class='ui-button-text'>OBSERVA&Ccedil;&Atilde;O</span></button>";
                        echo "<tr><td colspan='6' style='font-size:8px;'><span style='float:right;' class='sub_quipamento_".$cod_ident."'><font size ='0.3'><b>SUBTOTAL EQUIPAMENTO: R$</b> <b><i id='equipamento_".$cod_ident."'>".number_format($subtotal_equipamento,2,',','.')."</i></b></font></span></td></tr><br/>";
                        echo "<tr><td colspan='6' style='font-size:8px;'><span style='float:right;' class='sub_servico_".$cod_ident."'><font size ='0.3'><b>SUBTOTAL SERVI&Ccedil;O: R$</b> <b><i id='servico_".$cod_ident."'>".number_format($subtotal_servico,2,',','.')."</i></b></font></span></td></tr><br/>";
                        echo "<tr><td colspan='6' style='font-size:8px;'><span style='float:right;' class='sub_material_".$cod_ident."'><font size ='0.3'><b>SUBTOTAL MATERIAL: R$</b> <b><i id='material_".$cod_ident."'>".number_format($subtotal_material,2,',','.')."</i></b></font></span></td></tr><br/>";
                        echo "<tr><td colspan='6' style='font-size:8px;'><span style='float:right;' class='sub_medicamento_".$cod_ident."'><font size ='0.3'><b>SUBTOTAL MEDICAMENTO: R$</b> <b><i id='medicamento_".$cod_ident."'>".number_format($subtotal_medicamento,2,',','.')."</i></b></font></span></td></tr><br/>";
                        echo "<tr><td colspan='6' style='font-size:8px;'><span style='float:right;' class='sub_dieta_".$cod_ident."'><font size ='0.3'><b>SUBTOTAL DIETA: R$</b> <b><i id='dieta_".$cod_ident."'>".number_format($subtotal_dieta,2,',','.')."</i></b></font></span></td></tr><br/>";
                        echo "<tr><td colspan='6' style='font-size:8px;'><span style='float:right;' class='sub_gasoterapia_".$cod_ident."'><font size ='0.3'><b>SUBTOTAL GASOTERAPIA: R$</b> <b><i id='gasoterapia_".$cod_ident."'>".number_format($subtotal_gasoterapia,2,',','.')."</i></b></font></span></td></tr><br/>";

                        echo "<tr><td colspan='6' style='font-size:12px;'><span style='float:right;' class='sub_fatura_".$cod_ident."'><b>TOTAL PACIENTE: R$</b> <b><i>".number_format($subtotal,2,',','.')."</i></b></span></td></tr>";
                        echo "<tr><td colspan='6' id='obs_".$cod_ident."'><span id='span_{$cod_ident}' class='span_obs'>OBSERVA&Ccedil;&Atilde;O::<textarea cols=120 rows=5 {$acesso} id='text_{$cod_ident}'></textarea></span></td></tr>";
                        echo"</div>";
                    }

                    $subtotal=0.00;
                    $subtotal_equipamento=0.00;
                    $subtotal_material=0.00;
                    $subtotal_medicamento=0.00;
                    $subtotal_dieta=0.00;
                    $subtotal_servico=0.00;
                    $subtotal_gasoterapia=0.00;
                    $cod_ident =$row['Paciente'];
                    echo"<div class='container-fatura' id ='div_{$cod_ident}' >";
                    echo "<br/><table class='mytable' width=100%  >";
                    echo "<a><thead class='mostrar' idpaciente='{$cod_ident}'><tr><th colspan='5' style='background-color:#ccc;'><center width='70%' style='vertical-align:middle;'><b>".$row['nome']." (".$row['nomeplano'].")</b></center></th></tr></thead></a>";
                    echo"</table>
                        <table  width=100% class='ocultar mytable' id='tabela_{$cod_ident}'>";
                    echo "<tr>
			      	<th>Item</th>
			      	<th>Tipo</th>
			      	<th>Unidade</th>
			      	<th>Qtd</th>
			      	<th>Pre&ccedil;o Unit.</th>
			      	<th>Pre&ccedil;o Total</th>
			      	<th>Taxa</th>
		    	        <th>Pacote</th>
	      		  </tr>
                    <tr>
                        <td colspan='4'>
                            <b><input type='checkbox' class='selecionar-todos' paciente='{$cod_ident}'>Selecionar todos para serem removidos</b>
                        </td>
                        <td colspan='4' align='right' style='padding-right: 25px;'>
                            <b>Selecionar todos para pacote <input type='checkbox' class='selecionar-todos-pacote' paciente='{$cod_ident}'></b>
                        </td>
                    </tr>
                  ";
                    $nome_titulo = $row['nome'];
                    $plano = $row['Plano'];
                    $tipo_medicamento = $row['tipo_medicamento'];
                    $nomeplano = $row['nomeplano'];
                }

                $subtotal +=$row['quantidade']*$row['valor'];
                if($row['tipo'] == '1'){
                    $subtotal_material +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '0'){
                    $subtotal_medicamento +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '2'){
                    $subtotal_servico +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '3'){
                    $subtotal_dieta +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '5'){
                    $subtotal_equipamento +=$row['quantidade']*$row['valor'];
                }
                // $botoes = "<img align=right class=rem-item-solicitacao  src='../utils/delete_16x16.png' title='Remover' inicio={$inicio} fim={$fim} tiss='{$row['cod']}' paciente='{$row['Paciente']}' tipo='{$row['tipo']}' border='0'>";
                $botoes = "<br><input type='checkbox' class=' excluir-item excluir-item-paciente-{$cod_ident}'/> <b> Excluir</b>";
                //    if($nome_titulo!=$row['nome']){


                $codigoitem =$row['cod'];
                //////2014-01-16
                ///////// se for material coloca a descrição igual do Simpro que vai ser para fatura porque o principio é um ‘nome genérico’ que as enfermeiras usam.
                if($row['tipo'] == 1 && $row['segundonome']!= NULL){
                    $nomeitem =$row['segundonome'];
                }else{
                    $nomeitem = $row['principio'];

                }
                if($plano == 80){
                    $checked="checked='checked'";
                }else{
                    $checked="";
                }

                $label_item = preg_replace('/[^A-Za-z0-9]/', '', $row['catalogo_id']);
                $quantidade = $row['quantidade'] == '' ? 0 : $row['quantidade'];

                //if($codigoitem !=0){
                if($row['TABELA_ORIGEM'] != 'historico') {
                    echo "<tr  class='itens_fatura {$cod_ident}' cor='0'>
	      		<td width='50%'>
		    		<label id='label-novo-codigo-{$label_item}' style='display:none;'>
							<button cod='{$label_item}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover usar_tiss'>
								<span class='ui-button-text'>
									Usar Tiss
								</span>
							</button>
							<input type='text' id='codigo-proprio-{$label_item}' maxlength='11' name='codigo-proprio'>
						</label>
		    		<button id='bt-novo-codigo-{$label_item}' cod='{$label_item}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover bt_novo_codigo'>
		    			<span class='ui-button-text'>
		    				C&oacute;digo Pr&oacute;prio
							</span>
						</button>
						TUSS-" . $row['codTuss'] . "  N-" . $row['cod'] . "  {$nomeitem}{$botoes}</td>
	      		<td>{$row['apresentacao']}</td><td>" . unidades_fatura(NULL) . "</td>
	      		<td><input size='7' class='qtd ' value='{$quantidade}' paciente='{$row['Paciente']}'/></td>
	      		<td>R$<input type='text' name='valor_plano' size='10' class='valor_plano valor OBG' tiss='{$row['cod']}' label-item='{$label_item}' tuss='{$row['codTuss']}' tabela_origem='{$row['TABELA_ORIGEM']}' qtd='{$quantidade}' paciente='{$row['Paciente']}'  catalogo_id='{$row['catalogo_id']}' solicitacao='{$row['idSolicitacoes']}' tipo='{$row['tipo']}' inicio='{$inicio}' fim='{$fim}' custo = '{$row['custo']}'
                               usuario='{$_SESSION["id_user"]}' ord = {$cont} flag='{$row['flag']}' value='" . number_format($row['valor'], 2, ',', '.') . "'></td>
	      		<td><span style='float:right;' id='val_total{$cont}'><b>R$  <span nome='val_linha'>" . number_format($row['valor'] * $row['quantidade'], 2, ',', '.') . "</span></b></span></td>
	      		<td><input type='text' name='taxa' size='10' class='taxa OBG' value='0,00'></td>
                        <td><input type='checkbox' $checked name='incluso_pacote' size='10' class='incluso_pacote incluso-pacote-{$cod_ident} OBG'></td>
	      		</tr>";
                    $cont++;
                }
            }else{
                if($nome_titulo != $row['nome']){

                    $subtotal=0.00;
                    $subtotal_equipamento=0.00;
                    $subtotal_material=0.00;
                    $subtotal_medicamento=0.00;
                    $subtotal_dieta=0.00;
                    $subtotal_servico=0.00;
                    $subtotal_gasoterapia=0.00;
                    $tipo_medicamento = $row['tipo_medicamento'];
                    $nomeplano = $row['nomeplano'];
                    $plano = $row['Plano'];

                    $cod_ident =$row['Paciente'];


                    echo"<div class='container-fatura' id ='div_{$cod_ident}' >";
                    //echo "<button id='{$cod_ident}' style='float:left;' inicio='{$inicio}'  fim='{$fim}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover add-item-fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>+</span></button>";
                    echo "<br/>
{$htmlDevolucaoEnvio}
<table class='mytable' width=100%  >";
                    echo "<a><thead class='mostrar' idpaciente='{$cod_ident}'><tr><th colspan='5' style='background-color:#ccc;'><center width='70%' style='vertical-align:middle;'><b>".$row['nome']." (".$row['nomeplano'].")</b></center></th></tr></thead></a>";
                    echo"</table><table  width=100% class='ocultar mytable' id='tabela_{$cod_ident}'>";
                    echo "<tr>
		    	<th>Item</th>
		    	<th>Tipo</th>
		    	<th>Unidade</th>
		    	<th>Qtd</th>
		    	<th>Pre&ccedil;o Unit.</th>
		    	<th>Pre&ccedil;o Total</th>
                        <th>Taxa</th>
		    	<th>Pacote</th>
		    	</tr>

		    	<tr>
		    	    <td colspan='4'>
                        <b><input type='checkbox' class='selecionar-todos' paciente='{$cod_ident}'>Selecionar todos para serem removidos</b>
                    </td>
                    <td colspan='4' align='right' style='padding-right: 25px;'>
                        <b>Selecionar todos para pacote <input type='checkbox' class='selecionar-todos-pacote' paciente='{$cod_ident}'></b>
                    </td>
		    	</tr>";
                    $nome_titulo = $row['nome'];

                }

                $subtotal +=$row['quantidade']*$row['valor'];
                if($row['tipo'] == '1'){
                    $subtotal_material +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '0'){
                    $subtotal_medicamento +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '2'){
                    $subtotal_servico +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '3'){
                    $subtotal_dieta +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '5'){
                    $subtotal_equipamento +=$row['quantidade']*$row['valor'];
                }
                // $botoes = '<img align=\'right\' class=\'rem-item-solicitacao\'  src=\'../utils/delete_16x16.png\' title=\'Remover\' border=\'0\'>';
                //    if($nome_titulo!=$row['nome']){
                $botoes = "<br><input type='checkbox' class=' excluir-item excluir-item-paciente-{$cod_ident}'/> <b> Excluir</b>";

                $codigoitem =$row['cod'];
                //////2014-01-16
                ///////// se for material coloca a descrição igual do Simpro que vai ser para fatura porque o principio é um ‘nome genérico’ que as enfermeiras usam.

                $nomeitem = $row['principio'];
                if($plano == 80){
                    $checked="checked='checked'";
                }else{
                    $checked="";
                }

                $label_item = preg_replace('/[^A-Za-z0-9]/', '', $row['catalogo_id']);
                $quantidade = $row['quantidade'] == '' ? 0 : $row['quantidade'];

                if($row['TABELA_ORIGEM'] != 'historico') {
                    echo "<tr  class='itens_fatura {$cod_ident}' >
		    	<td width='50%'>
		    		<label id='label-novo-codigo-{$label_item}' style='display:none;'>
							<button cod='{$label_item}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover usar_tiss'>
								<span class='ui-button-text'>
									Usar Tiss
								</span>
							</button>
							<input type='text' id='codigo-proprio-{$label_item}' maxlength='11' name='codigo-proprio'>
						</label>
		    		<button id='bt-novo-codigo-{$label_item}' cod='{$label_item}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover bt_novo_codigo'>
		    			<span class='ui-button-text'>
		    				C&oacute;digo Pr&oacute;prio
							</span>
						</button>
		    		TUSS-" . $row['codTuss'] . " N-{$codigoitem} {$nomeitem}{$botoes}
					</td>
		    	<td>{$row['apresentacao']}</td>
		    	<td>" . unidades_fatura(NULL) . "</td>
		    	<td><input class='qtd ' size='7' value='{$quantidade}' paciente='{$row['Paciente']}'/></td>
		    	<td>R$<input type='text' name='valor_plano' size='10' class='valor_plano valor OBG' tiss='{$row['cod']}' tuss='{$row['codTuss']}' label-item='{$label_item}' tabela_origem='{$row['TABELA_ORIGEM']}' catalogo_id='{$row['catalogo_id']}' qtd='{$quantidade}' paciente='{$row['Paciente']}' solicitacao='{$row['idSolicitacoes']}' tipo='{$row['tipo']}' inicio='{$inicio}' fim='{$fim}' custo = '{$row['custo']}' usuario='{$_SESSION["id_user"]}' ord = {$cont} flag='{$row['flag']}' value='" . number_format($row['valor'], 2, ',', '.') . "'></td>
		    	<td><span style='float:right;' id='val_total{$cont}'><b>R$  <span nome='val_linha'>" . number_format($row['valor'] * $row['quantidade'], 2, ',', '.') . "</span></b></span></td>
		    	<td><input type='text' name='taxa' size='10' value='0,00' class='taxa OBG'></td>
                        <td><input type='checkbox' $checked name='incluso_pacote' size='10' class='incluso_pacote incluso-pacote-{$cod_ident} OBG'></td>
		    	</tr>";
                    $cont++;
                }
            }

            $empresa=$row['empresa'];
        }
        echo "</table>";
        echo "<button id='{$cod_ident}' contador=0 plano='{$plano}' style='float:left;' tipo_medicamento='{$tipo_medicamento}' nomeplano='{$nomeplano}' empresa='{$empresa}' inicio='{$inicio}'  fim='{$fim}' tipo-documento=0 class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover add-item-fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>+</span></button>";
        echo "<button  cod='{$cod_ident}'  plano='{$plano}' presc_av='0' nome='$nome_titulo' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover excluir-todos ' role='button' aria-disabled='false' ><span class='ui-button-text'>Excluir Itens</span></button>";
        echo "<button  cod='{$cod_ident}'  plano='{$plano}' nome='{$nome_titulo}' empresa='{$empresa}' presc_av='0' id-fatura='0' id='salvar-paciente-{$cod_ident}'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover salvar_fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar Rascunho</span></button>";
        echo "<button  cod='{$cod_ident}' plano='{$plano}'  presc_av='0' id-fatura='0' id='finalizar-paciente-{$cod_ident}'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover faturar' role='button' aria-disabled='false' ><span class='ui-button-text'>Faturar</span></button>";
        echo "<button paciente='{$cod_ident}' style='float:left;' inicio='{$inicio}'  fim='{$fim}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover obs' role='button' aria-disabled='false' ><span class='ui-button-text'>OBSERVA&Ccedil;&Atilde;O</span></button>";

        echo "<table class='mytable' width=100% id='lista-itens'>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_quipamento_".$cod_ident."'><b>SUBTOTAL EQUIPAMENTO: R$</b> <b><i id='equipamento_".$cod_ident."'>".number_format($subtotal_equipamento,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_servico_".$cod_ident."'><b>SUBTOTAL SERVI&Ccedil;O: R$</b> <b><i id='servico_".$cod_ident."'>".number_format($subtotal_servico,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_material_".$cod_ident."'><b>SUBTOTAL MATERIAL: R$</b> <b><i id='material_".$cod_ident."'>".number_format($subtotal_material,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_medicamento_".$cod_ident."'><b>SUBTOTAL MEDICAMENTO: R$</b> <b><i id='medicamento_".$cod_ident."'>".number_format($subtotal_medicamento,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_dieta_".$cod_ident."'><b>SUBTOTAL DIETA: R$</b> <b><i id='dieta_".$cod_ident."'>".number_format($subtotal_dieta,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_gasoterapia_".$cod_ident."'><b>SUBTOTAL GASOTERAPIA</b> <b><i id='gasoterapia_".$cod_ident."'>".number_format($subtotal_gasoterapia,2,',','.')."</i></b></span></td></tr>";

        echo "<tr><td colspan='5' style='font-size:12px;'><span style='float:right;' class='sub_fatura_".$cod_ident."'><b>TOTAL PACIENTE: R$</b> <b><i>".number_format($subtotal,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='6' id='obs_".$cod_ident."'><span id='span_{$cod_ident}' class='span_obs'><b>OBSERVA&Ccedil;&Atilde;O:</b></br><textarea cols=120 rows=5 {$acesso} name='obs' class='OBG' id='text_{$cod_ident}'></textarea></span></td></tr>";

        echo"</table></div><table class='mytable' width=100% >";
        echo "<tr><td colspan='5' style='font-size:12px;'><span style='float:right;' cont='{$cont}' cont1='0' id='total_fatura'><b>TOTAL: R$</b> <b><i>".number_format($total,2,',','.')."</i></b></span></td></tr>";
        echo "</table>";
    }else{
        $sql = <<<SQL
SELECT 
  clientes.nome, 
  planosdesaude.nome AS nomeplano,
  clientes.convenio AS Plano,
  planosdesaude.tipo_medicamento,
  clientes.empresa
FROM 
  clientes 
  INNER JOIN planosdesaude ON clientes.convenio = planosdesaude.id
WHERE
  clientes.idClientes = '{$paciente_id}' 
SQL;
        $rs = mysql_query($sql);
        $rowPaciente = mysql_fetch_array($rs);
        $cont=0;
        $cod_ident = $paciente_id;
        $subtotal=0.00;
        $subtotal_equipamento=0.00;
        $subtotal_material=0.00;
        $subtotal_medicamento=0.00;
        $subtotal_dieta=0.00;
        $subtotal_servico=0.00;
        $subtotal_gasoterapia=0.00;
        $nome_titulo = $rowPaciente['nome'];
        $tipo_medicamento = $rowPaciente['tipo_medicamento'];
        $nomeplano = $rowPaciente['nomeplano'];
        $plano = $rowPaciente['Plano'];
        $empresa = $rowPaciente['empresa'];
        echo"<div class='container-fatura' id='div_{$cod_ident}' >";
        echo "</br>
              <table width=100% style='border:2px dashed;'>
                    <tr><td><center><b>OBSERVA&Ccedil;&Otilde;ES</b></center></td></tr>
                    <tr><td>Na coluna TAXA deve se colocar o percentual se for desconto colocar como negativo ex: -1,00. Caso seja acrescimo positivo ex: 1,00 </td></tr>
                    <tr><td>Na coluna deve-se marcar os itens que não vão ser cobrados porque fazem parte do Pacote</td></tr>
              </table>
              </br>";

        echo "<center><h2>{$label_tipo}</h2></center><br/>";
        echo "<br/><table class='mytable' width=100%  >";
        echo "<a><thead class='mostrar' idpaciente='{$cod_ident}'>
                <tr>
                    <th colspan='5' style='background-color:#ccc;'>
                        <center width='70%' style='vertical-align:middle;'>
                            <b>".$rowPaciente['nome']." (".$rowPaciente['nomeplano'].")</b>
                        </center>
                    </th>
                </tr>
                </thead>
             </a>";
        echo"</table>
             <table  width=100% class='ocultar mytable' id='tabela_{$cod_ident}'>";
        echo "<tr>
                <th>Item</th>
                <th>Tipo</th>
                <th>Unidade</th>
                <th>Qtd</th>
                <th>Pre&ccedil;o Unit.</th>
                <th>Pre&ccedil;o Total</th>
                <th>Taxa</th>
                    <th>Pacote</th>
              </tr>
                <tr>
                    <td colspan='4'>
                        <b><input type='checkbox' class='selecionar-todos' paciente='{$cod_ident}'>Selecionar todos para serem removidos</b>
                    </td>
                    <td colspan='4' align='right' style='padding-right: 25px;'>
                        <b>Selecionar todos para pacote <input type='checkbox' class='selecionar-todos-pacote' paciente='{$cod_ident}'></b>
                    </td>
                </tr>
            </table>";
        echo "<button id='{$cod_ident}' contador=0 plano='{$plano}' style='float:left;' tipo_medicamento='{$tipo_medicamento}' nomeplano='{$nomeplano}' empresa='{$empresa}' inicio='{$inicio}'  fim='{$fim}' tipo-documento=0 class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover add-item-fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>+</span></button>";
        echo "<button  cod='{$cod_ident}'  plano='{$plano}' presc_av='0' nome='$nome_titulo' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover excluir-todos ' role='button' aria-disabled='false' ><span class='ui-button-text'>Excluir Itens</span></button>";
        echo "<button  cod='{$cod_ident}'  plano='{$plano}' nome='{$nome_titulo}' empresa='{$empresa}' presc_av='0' id-fatura='0' id='salvar-paciente-{$cod_ident}'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover salvar_fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar Rascunho</span></button>";
        echo "<button  cod='{$cod_ident}' plano='{$plano}'  presc_av='0' id-fatura='0' id='finalizar-paciente-{$cod_ident}'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover faturar' role='button' aria-disabled='false' ><span class='ui-button-text'>Faturar</span></button>";
        echo "<button paciente='{$cod_ident}' style='float:left;' inicio='{$inicio}'  fim='{$fim}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover obs' role='button' aria-disabled='false' ><span class='ui-button-text'>OBSERVA&Ccedil;&Atilde;O</span></button>";

        echo "<table class='mytable' width=100% id='lista-itens'>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_quipamento_".$cod_ident."'><b>SUBTOTAL EQUIPAMENTO: R$</b> <b><i id='equipamento_".$cod_ident."'>".number_format($subtotal_equipamento,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_servico_".$cod_ident."'><b>SUBTOTAL SERVI&Ccedil;O: R$</b> <b><i id='servico_".$cod_ident."'>".number_format($subtotal_servico,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_material_".$cod_ident."'><b>SUBTOTAL MATERIAL: R$</b> <b><i id='material_".$cod_ident."'>".number_format($subtotal_material,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_medicamento_".$cod_ident."'><b>SUBTOTAL MEDICAMENTO: R$</b> <b><i id='medicamento_".$cod_ident."'>".number_format($subtotal_medicamento,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_dieta_".$cod_ident."'><b>SUBTOTAL DIETA: R$</b> <b><i id='dieta_".$cod_ident."'>".number_format($subtotal_dieta,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_gasoterapia_".$cod_ident."'><b>SUBTOTAL GASOTERAPIA</b> <b><i id='gasoterapia_".$cod_ident."'>".number_format($subtotal_gasoterapia,2,',','.')."</i></b></span></td></tr>";

        echo "<tr><td colspan='5' style='font-size:12px;'><span style='float:right;' class='sub_fatura_".$cod_ident."'><b>TOTAL PACIENTE: R$</b> <b><i>".number_format($subtotal,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='6' id='obs_".$cod_ident."'><span id='span_{$cod_ident}' class='span_obs'><b>OBSERVA&Ccedil;&Atilde;O:</b></br><textarea cols=120 rows=5 {$acesso} name='obs' class='OBG' id='text_{$cod_ident}'></textarea></span></td></tr>";

        echo"</table>
             <table class='mytable' width=100% >";
        echo "<tr><td colspan='5' style='font-size:12px;'><span style='float:right;' cont='{$cont}' cont1='0' id='total_fatura'><b>TOTAL: R$</b> <b><i>".number_format($total,2,',','.')."</i></b></span></td></tr>";
        echo "</table>";
        echo "</div>";
    }
}

function listar_itens_fatura_salva($tipo, $cod, $inicio, $fim, $nome, $refaturar, $plano){
    $sql = "";
    $label_tipo = "";
    $tipo = anti_injection($tipo,'literal');
    $cod = anti_injection($cod,'numerico');
    $inicio = anti_injection($inicio,'literal');
    $fim = anti_injection($fim,'literal');
    $nome = anti_injection($nome,'literal');
    $plano = anti_injection($plano,'numerico');

    $label_tipo = "FATURA DO PACIENTE {$nome}";
    $sql = ModelFatura::getItensPreFaturaSalva($cod);

    echo formulario();

    //echo "<pre>"; die($sql);

    $result = mysql_query($sql);
    $resultado = mysql_num_rows($result);
    $htmlDevolucaoEnvio = "";
    if($tipo == 'paciente'){
        $devolucoes = ModelDevolucao::getDevolucaoByPaciente($cod, $inicio, $fim);
        $envios = ModelSaidas::getSaidaEquipamentoByPaciente($cod, $inicio, $fim);
        $inicioBr = implode('/',array_reverse(explode('-',$inicio)));
        $fimBr = implode('/',array_reverse(explode('-',$fim)));

        if(!empty($devolucoes)){
            $htmlDevolucaoEnvio = "<table class='mytable mostrar-devolucao' width='100%'>
										<thead>
											<tr>
												<th colspan='8'><center>Devoluções entre {$inicioBr} até {$fimBr} </center></th>
											</tr>
										</thead>
										<tbody id='tbody-devolucao' class='ocultar-devolucao'>
											<tr style='background-color:grey' >
												<td colspan='4'>ITEM</td>
												<td>TIPO</td>
												<td>QUANTIDADE</td>
												<td>LOTE</td>
												<td>DATA</td>
											</tr>";
            foreach($devolucoes as $d){
                $htmlDevolucaoEnvio .= "<tr>
												<td colspan='4'>{$d['nome']}</td>
												<td>{$d['nomeTipo']}</td>
												<td>{$d['qtd']}</td>
												<td>{$d['LOTE']}</td>
												<td>".implode('/',array_reverse(explode('-',$d['DATA_DEVOLUCAO'])))."</td>
											</tr>";

            }
            $htmlDevolucaoEnvio .= "</tbody>
									</table><br>";

        }

        if(!empty($envios) ){
            $htmlDevolucaoEnvio .= "<table class='mytable mostrar-envio' width='100%'>
										<thead>
											<tr>
												<th colspan='8'><center>Equipamentos enviados entre {$inicioBr} até {$fimBr}</center> </th>
											</tr>
										</thead>
										<tbody id='tbody-envio' class='ocultar-envio'>
											<tr style='background-color:grey'>
												<td colspan='4'>ITEM</td>
												<td>TIPO</td>
												<td>QUANTIDADE</td>
												<td>LOTE</td>
												<td>DATA</td>
											</tr>";

            foreach($envios as $e){
                $htmlDevolucaoEnvio .= "<tr>
												<td colspan='4'>{$e['nome']}</td>
												<td>Equipamento</td>
												<td>{$e['qtd']}</td>
												<td>{$e['LOTE']}</td>
												<td>".implode('/',array_reverse(explode('-',$e['data'])))."</td>
											</tr>";
            }
            $htmlDevolucaoEnvio .= "</tbody>
								</table><br>";
        }

    }

    if($resultado != 0){
        echo "</br>
              <table width=100% style='border:2px dashed;'>
                    <tr><td><center><b>OBSERVA&Ccedil;&Otilde;ES</b></center></td></tr>
                    <tr><td>Na coluna TAXA deve se colocar o percentual se for desconto colocar como negativo ex: -1,00. Caso seja acrescimo positivo ex: 1,00 </td></tr>
                    <tr><td>Na coluna deve-se marcar os itens que não vão ser cobrados porque fazem parte do Pacote</td></tr>
              </table>
              </br>";

        echo "<center><h2>{$label_tipo}</h2></center><br/>";

        $total=0.00;
        $cont=0;
        $nome_titulo='';
        $subtotal=0.00;
        $cod_ident = 0;

        while($row = mysql_fetch_array($result)){

            $total +=$row['quantidade']*$row['valor'];

            if($tipo=='plano' || $tipo=='unidade'){
                if($nome_titulo != $row['nome']){
                    if(!empty($nome_titulo)){
                        echo "</table>";
                        echo "<button id='{$cod_ident}' style='float:left;' contador=0 empresa='{$row['empresa']}' tipo_medicamento='{$tipo_medicamento}' plano='{$plano}'  nomeplano='{$nomeplano}' inicio='{$inicio}'  fim='{$fim}' tipo-documento = 0 class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover add-item-fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>+</span></button>";
                        echo "<button  cod='{$cod_ident}'  plano='{$plano}' presc_av='0' nome='$nome_titulo' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover excluir-todos ' role='button' aria-disabled='false' ><span class='ui-button-text'>Excluir Itens</span></button>";
                        echo "<button  cod='{$cod_ident}'  plano='{$plano}' presc_av='0' nome='$nome_titulo' id-fatura='0' empresa='{$row['empresa']}' id='finalizar-paciente-{$cod_ident}'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover salvar_fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button>";

                        echo "<button  cod='{$cod_ident}'  plano='{$plano}' presc_av='0' id-fatura='0' id='finalizar-paciente-{$cod_ident}'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover faturar' role='button' aria-disabled='false' ><span class='ui-button-text'>Faturar</span></button>";
                        echo "<button paciente='{$cod_ident}' style='float:left;' inicio='{$inicio}'  fim='{$fim}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover obs' role='button' aria-disabled='false' ><span class='ui-button-text'>OBSERVA&Ccedil;&Atilde;O</span></button>";
                        echo "<tr><td colspan='6' style='font-size:8px;'><span style='float:right;' class='sub_quipamento_".$cod_ident."'><font size ='0.3'><b>SUBTOTAL EQUIPAMENTO: R$</b> <b><i id='equipamento_".$cod_ident."'>".number_format($subtotal_equipamento,2,',','.')."</i></b></font></span></td></tr><br/>";
                        echo "<tr><td colspan='6' style='font-size:8px;'><span style='float:right;' class='sub_servico_".$cod_ident."'><font size ='0.3'><b>SUBTOTAL SERVI&Ccedil;O: R$</b> <b><i id='servico_".$cod_ident."'>".number_format($subtotal_servico,2,',','.')."</i></b></font></span></td></tr><br/>";
                        echo "<tr><td colspan='6' style='font-size:8px;'><span style='float:right;' class='sub_material_".$cod_ident."'><font size ='0.3'><b>SUBTOTAL MATERIAL: R$</b> <b><i id='material_".$cod_ident."'>".number_format($subtotal_material,2,',','.')."</i></b></font></span></td></tr><br/>";
                        echo "<tr><td colspan='6' style='font-size:8px;'><span style='float:right;' class='sub_medicamento_".$cod_ident."'><font size ='0.3'><b>SUBTOTAL MEDICAMENTO: R$</b> <b><i id='medicamento_".$cod_ident."'>".number_format($subtotal_medicamento,2,',','.')."</i></b></font></span></td></tr><br/>";
                        echo "<tr><td colspan='6' style='font-size:8px;'><span style='float:right;' class='sub_dieta_".$cod_ident."'><font size ='0.3'><b>SUBTOTAL DIETA: R$</b> <b><i id='dieta_".$cod_ident."'>".number_format($subtotal_dieta,2,',','.')."</i></b></font></span></td></tr><br/>";
                        echo "<tr><td colspan='6' style='font-size:8px;'><span style='float:right;' class='sub_gasoterapia_".$cod_ident."'><font size ='0.3'><b>SUBTOTAL GASOTERAPIA: R$</b> <b><i id='gasoterapia_".$cod_ident."'>".number_format($subtotal_gasoterapia,2,',','.')."</i></b></font></span></td></tr><br/>";

                        echo "<tr><td colspan='6' style='font-size:12px;'><span style='float:right;' class='sub_fatura_".$cod_ident."'><b>TOTAL PACIENTE: R$</b> <b><i>".number_format($subtotal,2,',','.')."</i></b></span></td></tr>";
                        echo "<tr><td colspan='6' id='obs_".$cod_ident."'><span id='span_{$cod_ident}' class='span_obs'>OBSERVA&Ccedil;&Atilde;O::<textarea cols=120 rows=5 {$acesso} id='text_{$cod_ident}'></textarea></span></td></tr>";
                        echo"</div>";
                    }

                    $subtotal=0.00;
                    $subtotal_equipamento=0.00;
                    $subtotal_material=0.00;
                    $subtotal_medicamento=0.00;
                    $subtotal_dieta=0.00;
                    $subtotal_servico=0.00;
                    $subtotal_gasoterapia=0.00;
                    $cod_ident =$row['Paciente'];
                    echo"<div id ='div_{$cod_ident}' >";
                    echo "<br/><table class='mytable' width=100%  >";
                    echo "<a><thead class='mostrar' idpaciente='{$cod_ident}'><tr><th colspan='5' style='background-color:#ccc;'><center width='70%' style='vertical-align:middle;'><b>".$row['nome']." (".$row['nomeplano'].")</b></center></th></tr></thead></a>";
                    echo"</table>
                        <table  width=100% class='ocultar mytable' id='tabela_{$cod_ident}'>";
                    echo "<tr>
			      	<th>Item</th>
			      	<th>Tipo</th>
			      	<th>Unidade</th>
			      	<th>Qtd</th>
			      	<th>Pre&ccedil;o Unit.</th>
			      	<th>Pre&ccedil;o Total</th>
			      	<th>Taxa</th>
		    	        <th>Pacote</th>
	      		  </tr>
	      		    <tr>
                        <td colspan='4'>
                            <b><input type='checkbox' class='selecionar-todos' paciente='{$cod_ident}'>Selecionar todos para serem removidos</b>
                        </td>
                        <td colspan='4' align='right' style='padding-right: 25px;'>
                            <b>Selecionar todos para pacote <input type='checkbox' class='selecionar-todos-pacote' paciente='{$cod_ident}'></b>
                        </td>
                    </tr>
                  ";
                    $nome_titulo = $row['nome'];
                    $plano = $row['Plano'];
                    $tipo_medicamento = $row['tipo_medicamento'];
                    $nomeplano = $row['nomeplano'];
                }

                $subtotal +=$row['quantidade']*$row['valor'];
                if($row['tipo'] == '1'){
                    $subtotal_material +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '0'){
                    $subtotal_medicamento +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '2'){
                    $subtotal_servico +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '3'){
                    $subtotal_dieta +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '5'){
                    $subtotal_equipamento +=$row['quantidade']*$row['valor'];
                }
                // $botoes = "<img align=right class=rem-item-solicitacao  src='../utils/delete_16x16.png' title='Remover' inicio={$inicio} fim={$fim} tiss='{$row['cod']}' paciente='{$row['Paciente']}' tipo='{$row['tipo']}' border='0'>";
                $botoes = "<br><input type='checkbox' class=' excluir-item excluir-item-paciente-{$cod_ident}'/> <b> Excluir</b>";
                //    if($nome_titulo!=$row['nome']){


                $codigoitem =$row['cod'];
                //////2014-01-16
                ///////// se for material coloca a descrição igual do Simpro que vai ser para fatura porque o principio é um ‘nome genérico’ que as enfermeiras usam.
                if($row['tipo'] == 1 && $row['segundonome']!= NULL){
                    $nomeitem =$row['segundonome'];
                }else{
                    $nomeitem = $row['principio'];

                }
                if($plano == 80){
                    $checked="checked='checked'";
                }else{
                    $checked="";
                }

                $label_item = preg_replace('/[^A-Za-z0-9]/', '', $row['catalogo_id']);
                $quantidade = $row['quantidade'] == '' ? 0 : $row['quantidade'];

                //if($codigoitem !=0){
                echo "<tr  class='itens_fatura {$cod_ident}' cor='0'>
	      		<td width='50%'>
		    		<label id='label-novo-codigo-{$label_item}' style='display:none;'>
							<button cod='{$label_item}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover usar_tiss'>
								<span class='ui-button-text'>
									Usar Tiss
								</span>
							</button>
							<input type='text' id='codigo-proprio-{$label_item}' maxlength='11' name='codigo-proprio'>
						</label>
		    		<button id='bt-novo-codigo-{$label_item}' cod='{$label_item}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover bt_novo_codigo'>
		    			<span class='ui-button-text'>
		    				C&oacute;digo Pr&oacute;prio
							</span>
						</button>
						TUSS-".$row['codTuss']."  N-".$row['cod']."  {$nomeitem}{$botoes}</td>
	      		<td>{$row['apresentacao']}</td><td>".unidades_fatura(NULL)."</td>
	      		<td><input size='7' class='qtd ' value='{$quantidade}' paciente='{$row['Paciente']}'/></td>
	      		<td>R$<input type='text' name='valor_plano' size='10' class='valor_plano valor OBG' tiss='{$row['cod']}' label-item='{$label_item}' tuss='{$row['codTuss']}' tabela_origem='{$row['TABELA_ORIGEM']}' qtd='{$quantidade}' paciente='{$row['Paciente']}'  catalogo_id='{$row['catalogo_id']}' solicitacao='{$row['idSolicitacoes']}' tipo='{$row['tipo']}' inicio='{$inicio}' fim='{$fim}' custo = '{$row['custo']}'
                               usuario='{$_SESSION["id_user"]}' ord = {$cont} flag='{$row['flag']}' value='".number_format($row['valor'],2,',','.')."'></td>
	      		<td><span style='float:right;' id='val_total{$cont}'><b>R$  <span nome='val_linha'>".number_format($row['valor']*$row['quantidade'],2,',','.')."</span></b></span></td>
	      		<td><input type='text' name='taxa' size='10' class='taxa OBG' value='0,00'></td>
                        <td><input type='checkbox'  $checked  name='incluso_pacote' size='10' class='incluso_pacote incluso-pacote-{$cod_ident} OBG'></td>
	      		</tr>";
                $cont++;
            }else{
                if($nome_titulo != $row['nome']){

                    $subtotal=0.00;
                    $subtotal_equipamento=0.00;
                    $subtotal_material=0.00;
                    $subtotal_medicamento=0.00;
                    $subtotal_dieta=0.00;
                    $subtotal_servico=0.00;
                    $subtotal_gasoterapia=0.00;
                    $tipo_medicamento = $row['tipo_medicamento'];
                    $nomeplano = $row['nomeplano'];
                    $plano = $row['Plano'];

                    $cod_ident =$row['Paciente'];


                    echo"<div id ='div_{$cod_ident}' >";
                    //echo "<button id='{$cod_ident}' style='float:left;' inicio='{$inicio}'  fim='{$fim}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover add-item-fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>+</span></button>";
                    echo "<br/>
{$htmlDevolucaoEnvio}
<table class='mytable' width=100%  >";
                    echo "<a><thead class='mostrar' idpaciente='{$cod_ident}'><tr><th colspan='5' style='background-color:#ccc;'><center width='70%' style='vertical-align:middle;'><b>".$row['nome']." (".$row['nomeplano'].")</b></center></th></tr></thead></a>";
                    echo"</table><table  width=100% class='ocultar mytable' id='tabela_{$cod_ident}'>";
                    echo "<tr>
		    	<th>Item</th>
		    	<th>Tipo</th>
		    	<th>Unidade</th>
		    	<th>Qtd</th>
		    	<th>Pre&ccedil;o Unit.</th>
		    	<th>Pre&ccedil;o Total</th>
                        <th>Taxa</th>
		    	<th>Pacote</th>
		    	</tr>

		    	<tr>
                    <td colspan='4'>
                        <b><input type='checkbox' class='selecionar-todos' paciente='{$cod_ident}'>Selecionar todos para serem removidos</b>
                    </td>
                    <td colspan='4' align='right' style='padding-right: 25px;'>
                        <b>Selecionar todos para pacote <input type='checkbox' class='selecionar-todos-pacote' paciente='{$cod_ident}'></b>
                    </td>
                </tr>";
                    $nome_titulo = $row['nome'];

                }

                $subtotal +=$row['quantidade']*$row['valor'];
                if($row['tipo'] == '1'){
                    $subtotal_material +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '0'){
                    $subtotal_medicamento +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '2'){
                    $subtotal_servico +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '3'){
                    $subtotal_dieta +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '5'){
                    $subtotal_equipamento +=$row['quantidade']*$row['valor'];
                }
                // $botoes = '<img align=\'right\' class=\'rem-item-solicitacao\'  src=\'../utils/delete_16x16.png\' title=\'Remover\' border=\'0\'>';
                //    if($nome_titulo!=$row['nome']){
                $botoes = "<br><input type='checkbox' class=' excluir-item excluir-item-paciente-{$cod_ident}'/> <b> Excluir</b>";

                $codigoitem =$row['cod'];
                //////2014-01-16
                ///////// se for material coloca a descrição igual do Simpro que vai ser para fatura porque o principio é um ‘nome genérico’ que as enfermeiras usam.

                $nomeitem = $row['principio'];
                if($plano == 80){
                    $checked="checked='checked'";
                }else{
                    $checked="";
                }

                $label_item = preg_replace('/[^A-Za-z0-9]/', '', $row['catalogo_id']);
                $quantidade = $row['quantidade'] == '' ? 0 : $row['quantidade'];
                //  if($codigoitem != 0){
                echo "<tr  class='itens_fatura {$cod_ident}' >
		    	<td width='50%'>
		    		<label id='label-novo-codigo-{$label_item}' style='display:none;'>
							<button cod='{$label_item}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover usar_tiss'>
								<span class='ui-button-text'>
									Usar Tiss
								</span>
							</button>
							<input type='text' id='codigo-proprio-{$label_item}' maxlength='11' name='codigo-proprio'>
						</label>
		    		<button id='bt-novo-codigo-{$label_item}' cod='{$label_item}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover bt_novo_codigo'>
		    			<span class='ui-button-text'>
		    				C&oacute;digo Pr&oacute;prio
							</span>
						</button>
		    		TUSS-".$row['codTuss']." N-{$codigoitem} {$nomeitem}{$botoes}
					</td>
		    	<td>{$row['apresentacao']}</td>
		    	<td>".unidades_fatura(NULL)."</td>
		    	<td><input class='qtd ' size='7' value='{$quantidade}' paciente='{$row['Paciente']}'/></td>
		    	<td>R$<input type='text' name='valor_plano' size='10' class='valor_plano valor OBG' tiss='{$row['cod']}' tuss='{$row['codTuss']}' label-item='{$label_item}' tabela_origem='{$row['TABELA_ORIGEM']}' catalogo_id='{$row['catalogo_id']}' qtd='{$quantidade}' paciente='{$row['Paciente']}' solicitacao='{$row['idSolicitacoes']}' tipo='{$row['tipo']}' inicio='{$inicio}' fim='{$fim}' custo = '{$row['custo']}' usuario='{$_SESSION["id_user"]}' ord = {$cont} flag='{$row['flag']}' value='".number_format($row['valor'],2,',','.')."'></td>
		    	<td><span style='float:right;' id='val_total{$cont}'><b>R$  <span nome='val_linha'>".number_format($row['valor']*$row['quantidade'],2,',','.')."</span></b></span></td>
		    	<td><input type='text' name='taxa' size='10' value='0,00' class='taxa OBG'></td>
                        <td><input type='checkbox' name='incluso_pacote' size='10' $checked class='incluso_pacote incluso-pacote-{$cod_ident} OBG'></td>
		    	</tr>";
                $cont++;
            }

            $empresa=$row['empresa'];
        }
        echo "</table>";
        echo "<button id='{$cod_ident}' contador=0 plano='{$plano}' style='float:left;' tipo_medicamento='{$tipo_medicamento}' nomeplano='{$nomeplano}' empresa='{$empresa}' inicio='{$inicio}'  fim='{$fim}' tipo-documento=0 class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover add-item-fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>+</span></button>";
        echo "<button  cod='{$cod_ident}'  plano='{$plano}' presc_av='0' nome='$nome_titulo' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover excluir-todos ' role='button' aria-disabled='false' ><span class='ui-button-text'>Excluir Itens</span></button>";
        echo "<button  cod='{$cod_ident}'  plano='{$plano}' nome='{$nome_titulo}' presc_av='0' empresa='{$empresa}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover salvar_fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button>";
        echo "<button  cod='{$cod_ident}' plano='{$plano}'  presc_av='0' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover faturar' role='button' aria-disabled='false' ><span class='ui-button-text'>Faturar</span></button>";
        echo "<button paciente='{$cod_ident}' style='float:left;' inicio='{$inicio}'  fim='{$fim}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover obs' role='button' aria-disabled='false' ><span class='ui-button-text'>OBSERVA&Ccedil;&Atilde;O</span></button>";

        echo "<table class='mytable' width=100% id='lista-itens'>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_quipamento_".$cod_ident."'><b>SUBTOTAL EQUIPAMENTO: R$</b> <b><i id='equipamento_".$cod_ident."'>".number_format($subtotal_equipamento,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_servico_".$cod_ident."'><b>SUBTOTAL SERVI&Ccedil;O: R$</b> <b><i id='servico_".$cod_ident."'>".number_format($subtotal_servico,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_material_".$cod_ident."'><b>SUBTOTAL MATERIAL: R$</b> <b><i id='material_".$cod_ident."'>".number_format($subtotal_material,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_medicamento_".$cod_ident."'><b>SUBTOTAL MEDICAMENTO: R$</b> <b><i id='medicamento_".$cod_ident."'>".number_format($subtotal_medicamento,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_dieta_".$cod_ident."'><b>SUBTOTAL DIETA: R$</b> <b><i id='dieta_".$cod_ident."'>".number_format($subtotal_dieta,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_gasoterapia_".$cod_ident."'><b>SUBTOTAL GASOTERAPIA</b> <b><i id='gasoterapia_".$cod_ident."'>".number_format($subtotal_gasoterapia,2,',','.')."</i></b></span></td></tr>";

        echo "<tr><td colspan='5' style='font-size:12px;'><span style='float:right;' class='sub_fatura_".$cod_ident."'><b>TOTAL PACIENTE: R$</b> <b><i>".number_format($subtotal,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='6' id='obs_".$cod_ident."'><span id='span_{$cod_ident}' class='span_obs'><b>OBSERVA&Ccedil;&Atilde;O:</b></br><textarea cols=120 rows=5 {$acesso} name='obs' class='OBG' id='text_{$cod_ident}'></textarea></span></td></tr>";

        echo"</table></div><table class='mytable' width=100% >";
        echo "<tr><td colspan='5' style='font-size:12px;'><span style='float:right;' cont='{$cont}' cont1='0' id='total_fatura'><b>TOTAL: R$</b> <b><i>".number_format($total,2,',','.')."</i></b></span></td></tr>";
        echo "</table>";
    }else{
        echo "<center><b>
		Nenhum resultado foi encontrado!</b></center>";
    }
}


function listar_fatura($tipo,$cod,$inicio,$fim,$nome){
    $sql = "";
    $label_tipo = "";

    switch($tipo){
        case 'paciente':
            $label_tipo = "FATURA DO PACIENTE {$nome}";
            $sql = "SELECT
			B.principio,
			S.NUMERO_TISS AS cod,
                        S.CATALOGO_ID as catalogo_id,
			S.tipo,
			S.idSolicitacoes,
			C.idClientes AS Paciente,
			C.convenio AS Plano,
			 coalesce(B.valorMedio,'0.00')  AS custo,
			B.apresentacao,
			(CASE WHEN COUNT(*)>1 THEN SUM(S.qtd) ELSE S.qtd END) AS quantidade,
			coalesce(B.VALOR_VENDA,'0.00')  AS valor

			FROM
			`solicitacoes` S INNER JOIN
			`clientes` C ON (S.paciente = C.idClientes)  INNER JOIN
			`catalogo` B ON (S.CATALOGO_ID = B.ID)
			WHERE
			S.paciente = '{$cod}' AND
			S.data BETWEEN '{$inicio}' AND '{$fim}' AND
			S.qtd>0 AND S.FATURADO='S'
			GROUP BY
			S.catalogo_id

			UNION
			SELECT
			B.item AS principio,
			BC.COD_COBRANCA_PLANO AS cod,
                        BC.id AS catalogo_id,
			S.tipo,
			S.idSolicitacoes,
			C.idClientes AS Paciente,
			C.convenio AS Plano,
			BC.CUSTO_COBRANCA_PLANO as custo,
			('Servi&ccedil;os') AS apresentacao,
			(CASE WHEN COUNT(*)>1 THEN SUM(S.qtd) ELSE S.qtd END) AS quantidade,
			BC.valor AS valor


			FROM
			`solicitacoes` S INNER JOIN
			`clientes` C ON (S.paciente = C.idClientes)  INNER JOIN
			`cobrancaplanos` B ON (S.CATALOGO_ID =B.id) INNER JOIN
			`valorescobranca` BC ON (B.id=BC.idCobranca AND BC.idPlano = C.convenio)
			WHERE
			S.paciente = '{$cod}' AND
			S.data BETWEEN '{$inicio}' AND '{$fim}' AND
			S.qtd>0 AND
			NOT S.tipo IN (0,1) AND S.FATURADO='S'
			GROUP BY
			S.catalogo_id";
            break;
        case 'unidade':
            $label_tipo = "FATURA DA UNIDADE {$nome}";
            $sql = "SELECT
			B.principio,
			S.NUMERO_TISS AS cod,
                        S.CATALOGO_ID as catalogo_id,
			S.tipo,
			S.idSolicitacoes,
			C.idClientes AS Paciente,
			C.convenio AS Plano,
			 coalesce(B.valorMedio,'0.00') AS custo,
			B.apresentacao,
			(CASE WHEN COUNT(*)>1 THEN SUM(S.qtd) ELSE S.qtd END) AS quantidade,
			coalesce(B.VALOR_VENDA,'0.00')  AS valor

			FROM
			`solicitacoes` S INNER JOIN
			`clientes` C ON (S.paciente = C.idClientes)  INNER JOIN
			`catalogo` B ON (S.CATALOGO_ID= B.ID)
			WHERE
			C.empresa = '{$cod}' AND
			S.data BETWEEN '{$inicio}' AND '{$fim}' AND
			S.qtd>0 AND S.FATURADO='S'
			GROUP BY
			S.catalogo_id

			UNION
			SELECT
			B.item AS principio,
			S.NUMERO_TISS AS cod,
                        S.CATALOGO_ID as catalogo_id,
			S.tipo,
			S.idSolicitacoes,
			C.idClientes AS Paciente,
			C.convenio AS Plano,
			BC.CUSTO_COBRANCA_PLANO as custo,
			('Servi&ccedil;os') AS apresentacao,
			(CASE WHEN COUNT(*)>1 THEN SUM(S.qtd) ELSE S.qtd END) AS quantidade,
			BC.valor AS valor


			FROM
			`solicitacoes` S INNER JOIN
			`clientes` C ON (S.paciente = C.idClientes)  INNER JOIN
			`cobrancaplanos` B ON (S.CATALOGO_ID =B.id) INNER JOIN
			`valorescobranca` BC ON (B.id=BC.idCobranca AND BC.idPlano = C.convenio)
			WHERE
			C.empresa = '{$cod}' AND
			S.data BETWEEN '{$inicio}' AND '{$fim}' AND
			S.qtd>0 AND
			NOT S.tipo IN (0,1) AND S.FATURADO='S'
			GROUP BY
			S.catalogo_id";
            break;
        case 'plano':
            $label_tipo = "FATURA DO PLANO {$nome}";
            $sql = "SELECT
			B.principio,
			S.NUMERO_TISS AS cod,
                        S.CATALOGO_ID as catalogo_id,
			S.tipo,
			S.idSolicitacoes,
			C.idClientes AS Paciente,
			C.convenio AS Plano,
			 coalesce(B.valorMedio,'0.00') AS custo,
			B.apresentacao,
			(CASE WHEN COUNT(*)>1 THEN SUM(S.qtd) ELSE S.qtd END) AS quantidade,
			coalesce(B.VALOR_VENDA ,'0.00') AS valor

			FROM
			`solicitacoes` S INNER JOIN
			`clientes` C ON (S.paciente = C.idClientes)  INNER JOIN
			`catalogo` B ON (S.CATALOGO_ID= B.ID)
			WHERE
			C.convenio = '{$cod}' AND
			S.data BETWEEN '{$inicio}' AND '{$fim}' AND
			S.qtd>0 AND S.FATURADO='S'
			GROUP BY
			S.catalogo_id

			UNION
			SELECT
			B.item AS principio,
			S.NUMERO_TISS AS cod,
                        S.CATALOGO_ID as catalogo_id,
			S.tipo,
			S.idSolicitacoes,
			C.idClientes AS Paciente,
			C.convenio AS Plano,
			BC.CUSTO_COBRANCA_PLANO as custo,
			('Servi&ccedil;os') AS apresentacao,
			(CASE WHEN COUNT(*)>1 THEN SUM(S.qtd) ELSE S.qtd END) AS quantidade,
			BC.valor AS valor


			FROM
			`solicitacoes` S INNER JOIN
			`clientes` C ON (S.paciente = C.idClientes)  INNER JOIN
			`cobrancaplanos` B ON (S.CATALOGO_ID =B.id) INNER JOIN
			`valorescobranca` BC ON (B.id=BC.idCobranca AND BC.idPlano = C.convenio)
			WHERE
			C.convenio = '{$cod}' AND
			S.data BETWEEN '{$inicio}' AND '{$fim}' AND
			S.qtd>0 AND
			NOT S.tipo IN (0,1) AND S.FATURADO='S'
			GROUP BY
			S.catalogo_id";
            break;
    }


    $result = mysql_query($sql);
    echo "<center><h2>{$label_tipo}</h2></center><br/>";
    echo "<table class='mytable' width=100% id='lista-itens'>
			<thead>
				<tr>
					<th>Item</th>
					<th>Tipo</th>
					<th>Qtd</th>
					<th nowrap='nowrap'>Pre&ccedil;o Total</th>
					<th>Custo</th>
					<th>Lucro</th>
					<th>Margem</th>
				</tr>
			</thead>";
    $total=0.00;
    $margem=0;
    while($row = mysql_fetch_array($result)){
        if($i++%2==0)
            $cor = "#E9F4F8";
        else
            $cor = "";

        $total +=$row['quantidade']*$row['valor'];
        $total_custo +=$row['quantidade']*$row['custo'];
        if($row['valor']!=0)
            $margem += (round(($row['valor']-$row['custo'])/$row['valor'],2)*100);
        else
            $margem +=0;

        echo "<tr  class='itens_fatura' bgcolor='{$cor}'>
					<td>{$row['principio']}</td>
					<td>{$row['apresentacao']}</td>
					<td>{$row['quantidade']}</td>
					<td>R$ ".number_format($row['valor']*$row['quantidade'],2,',','.')."</td>
					<td>R$ ".number_format($row['custo'],2,',','.')."</td>
					<td>R$ ".number_format($row['valor']-$row['custo'],2,',','.')."</td>
					<td><center>".@(round(($row['valor']-$row['custo'])/$row['valor'],2)*100)." %</center></td>
				</tr>";
    }

    echo "<tr bgcolor='#eee'>
<td colspan='3' style='font-size:12px;'><b>Total</b></td>
<td><b> R$</b> <b><i style='color:green'>".number_format($total,2,',','.')."</i></b></td>
<td><span id='total_fatura'><b>R$</b> <b style='color:red'><i>".number_format($total_custo,2,',','.')."</i></b></td>
<td><span id='total_fatura'><b>R$</b> <b style='color:orange'><i>".number_format($total-$total_custo,2,',','.')."</i></b></td>
<td><span id='total_fatura'><b style='color:black'><i><center>".number_format((($total-$total_custo)/$total)*100,2,',','.')." %</center></i></b></td>
</tr>";

    /*echo "<tr>
    <td colspan='7' style='font-size:12px;'><span style='float:right;' id='total_fatura'><b>Total Faturado: R$</b> <b><i style='color:green'>".number_format($total,2,',','.')."</i></b></span></br></br>
    <span style='float:right;' id='total_fatura'><b></b><b style='color:black'><i>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i></b></span></br></br>
    <span style='float:right;' id='total_fatura'><b>Total dos Custos: R$</b> <b style='color:red'><i>".number_format($total_custo,2,',','.')."</i></b></span></br>
    <span style='float:right;' id='total_fatura'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b> <b style='color:black'><i>________</i></b></span></br></br>
    <span style='float:right;' id='total_fatura'><b>Resultado: R$</b> <b style='color:orange'><i>".number_format($total-$total_custo,2,',','.')."</i></b></span></td>
    </tr>";*/

    echo "</table>";


}

/**
 * Status:
 * 2 => Pendente
 * 5 => Faturado
 */
function listar_fatura_lote ($tipo,$cod,$inicio,$fim,$nome){

    switch($tipo){

        case 'paciente':
            if($inicio == '' && $fim == ''){
                $condicao= "PACIENTE_ID = {$cod} and CAPA_LOTE = 0 and f.STATUS = 5  and f.CANCELADO_POR is NULL order by DATA DESC";
            }
            if($inicio != '' && $fim != ''){
                $condicao= "PACIENTE_ID = {$cod} and CAPA_LOTE = 0 and f.STATUS  = 5 and DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}' AND DATA_FIM BETWEEN '{$inicio}' AND '{$fim}' and f.CANCELADO_POR is NULL order by DATA DESC";
            }

            $sql="select f.*,DATE_FORMAT(f.DATA_FIM,'%d/%m/%Y' ) as datafim, DATE_FORMAT(f.DATA_INICIO,'%d/%m/%Y ' ) as datainicio,
			DATE_FORMAT(f.DATA,'%d/%m/%Y %h:%i:%s' ) as data, c.nome from fatura as f inner join clientes as c on (c.idClientes = f.PACIENTE_ID) where {$condicao} ";
            break;

        case 'operadora':
            if($inicio == '' && $fim == ''){
                $condicao= "op.OPERADORAS_ID = {$cod} and f.CAPA_LOTE = 0 and f.STATUS = 5 and f.CANCELADO_POR is NULL  order by f.DATA DESC";
            }
            if($inicio != '' && $fim != ''){
                $condicao= "op.OPERADORAS_ID = {$cod} and f.CAPA_LOTE = 0 and f.STATUS = 5 and f.DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}' AND f.DATA_FIM BETWEEN '{$inicio}' AND '{$fim}' AND f.CANCELADO_POR IS NULL  order by DATA DESC";
            }


            //$sql="select f.*, c.nome, c.convenio from fatura as f inner join clientes as c on (c.idClientes = f.PACIENTE_ID) inner join operadoras_planodesaude on (c.convenio = planosdesaude_id )  where {$condicao} ";

            $sql = "SELECT
	   	                f.*,DATE_FORMAT(f.DATA_FIM,'%d/%m/%Y' ) as datafim, DATE_FORMAT(f.DATA_INICIO,'%d/%m/%Y ' ) as datainicio,
	   	                c.nome,DATE_FORMAT(f.DATA,'%d/%m/%Y %h:%i:%s' ) as data
	   	         FROM
	   	               operadoras_planosdesaude as op Right Join
	   	               clientes as c on (c.convenio = op.PLANOSDESAUDE_ID) RIGHT JOIN
	   	               fatura as f on (c.idClientes = f.PACIENTE_ID)
	   	         WHERE
	   	         	{$condicao}";

            break;
    }

    $result = mysql_query($sql);

    $resposta = mysql_num_rows($result);


    if($resposta > 0){

        echo "<table class='mytable' width=100% >
<thead>
	<tr>
	<th>Paciente</th>
	<th>Guia</th>
	<th>Data</th>
	<th>Per&iacute;odo</th>
	<th>Visualizar</th>
	<th>Enviar</th>

	</tr>
	</thead>";

        while($row = mysql_fetch_array($result)){

            echo"<tr><td> {$row['nome']}</td><td> {$row['GUIA_TISS']}</td><td>{$row['data']}</td><td>{$row['datainicio']} at&eacute {$row['datafim']}</td><td><a href='#'><img src='../utils/fichas_24x24.png' width='16' width='20' title='Visualizar' class='v_glosa_2' empresa='{$row['UR']}' idfatura ='{$row['ID']}' av='{$row{'ORCAMENTO'}}' ></a></td><td><input type='checkbox' class='enviar' value='{$row['ID']}'/></td></tr>";

        }

        echo"</table>";
        echo "<button  cod='{$cod}' tipo='{$tipo}'  id='gerar_lote' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover ' role='button' aria-disabled='false' ><span class='ui-button-text'>Gerar Lote</span></button>";
    }else{
        echo "<center><b>Nenhum resultado foi encontrado!</b></center> ";

    }



}




function listar_fatura_glosa ($tipo,$cod,$inicio,$fim,$nome){

    switch($tipo){

        case 'paciente':
            if($inicio == '' && $fim == ''){
                $condicao= "PACIENTE_ID = {$cod} and CAPA_LOTE != 0 and f.STATUS in (1,3) order by DATA DESC";
            }
            if($inicio != '' && $fim != ''){
                $condicao= "PACIENTE_ID = {$cod} and CAPA_LOTE != 0 and f.STATUS in (1,3)  and DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}' AND DATA_FIM BETWEEN '{$inicio}' AND '{$fim}' order by DATA DESC";
            }

            $sql="select f.*, DATE_FORMAT(f.DATA_FIM,'%d/%m/%Y' ) as datafim, DATE_FORMAT(f.DATA_INICIO,'%d/%m/%Y ' ) as datainicio, c.nome from fatura as f inner join clientes as c on (c.idClientes = f.PACIENTE_ID) where {$condicao} ";
            break;

        case 'operadora':
            if($inicio == '' && $fim == ''){
                $condicao= "op.OPERADORAS_ID = {$cod} and f.CAPA_LOTE != 0 and f.STATUS in (1,3)  order by f.DATA DESC";
            }
            if($inicio != '' && $fim != ''){
                $condicao= "op.OPERADORAS_ID = {$cod} and f.CAPA_LOTE != 0 and f.STATUS in (1,3) and f.DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}' AND f.DATA_FIM BETWEEN '{$inicio}' AND '{$fim}' order by DATA DESC";
            }


            //$sql="select f.*, c.nome, c.convenio from fatura as f inner join clientes as c on (c.idClientes = f.PACIENTE_ID) inner join operadoras_planodesaude on (c.convenio = planosdesaude_id )  where {$condicao} ";

            $sql = "SELECT
			f.*,DATE_FORMAT(f.DATA_FIM,'%d/%m/%Y' ) as datafim, DATE_FORMAT(f.DATA_INICIO,'%d/%m/%Y ' ) as datainicio,
			c.nome
			FROM
			operadoras_planosdesaude as op Right Join
			clientes as c on (c.convenio = op.PLANOSDESAUDE_ID) RIGHT JOIN
			fatura as f on (c.idClientes = f.PACIENTE_ID)
			WHERE
			{$condicao}";

            break;
        case '':
            if($inicio == '' && $fim == ''){
                $condicao= "f.CAPA_LOTE != 0 and f.STATUS in (1,3,4)  order by f.DATA DESC";
            }
            if($inicio != '' && $fim != ''){
                $condicao= "f.CAPA_LOTE != 0 and f.STATUS in (1,3,4) and f.DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}' AND f.DATA_FIM BETWEEN '{$inicio}' AND '{$fim}' order by DATA DESC";
            }
            $sql = "SELECT
			f.*,DATE_FORMAT(f.DATA_FIM,'%d/%m/%Y' ) as datafim, DATE_FORMAT(f.DATA_INICIO,'%d/%m/%Y ' ) as datainicio,
			c.nome
			FROM
			operadoras_planosdesaude as op Right Join
			clientes as c on (c.convenio = op.PLANOSDESAUDE_ID) RIGHT JOIN
			fatura as f on (c.idClientes = f.PACIENTE_ID)
			WHERE
			{$condicao}";
            break;
    }

    $result = mysql_query($sql);

    $resposta = mysql_num_rows($result);


    if($resposta > 0){

        echo "<table class='mytable' width=100% ><thead>
			<tr>
			<th>Paciente</th>

			<th>Per&iacute;odo</th>
			<th>Status</th>
			<th>Op&ccedil;&otilde;es</th>


			</tr>
			</thead>";

        while($row = mysql_fetch_array($result)){

            if($row['STATUS'] == 4){
                $status= 'Processado';

            }
            if($row['STATUS'] == 1){
                $status= 'Enviado';

            }
            if($row['STATUS'] == 3){
                $status= 'Em An&aacute;lise';

            }


            /*echo"<tr><td>{$row['nome']}</td><td>{$row['datainicio']} at&eacute {$row['datafim']}</td><td>{$status}</td><td><a href=imprimir_fatura_glosa.php?id={$row['ID']}  target='_blank'><img src='../utils/fichas_24x24.png' width='16' width='20' title='Visualizar' ></a>
            <a href='?view=informar-glosa&id={$row['ID']}'  ><img src='../utils/edit_16x16.png' width='16' width='20' title='Informar Glosa' ></a></td></tr>";*/

            echo"<tr><td>{$row['nome']}</td><td>{$row['datainicio']} at&eacute {$row['datafim']}</td><td>{$status}</td><td><a href='#'><img src='../utils/fichas_24x24.png' width='16' width='20' title='Visualizar' class='v_glosa' idfatura ='{$row['ID']}'></a>";
            if($row['STATUS']!= 4){
                echo"&nbsp;&nbsp;&nbsp;<a href='?view=informar-glosa&id={$row['ID']}'  ><img src='../utils/edit_16x16.png' width='16' width='20' title='Informar Glosa' ></a>";
            }
            if($row['STATUS'] == 3){
                echo"&nbsp;&nbsp;&nbsp;<a href='#'><img src='../utils/menu_fatur_48x48.png'  idfatura={$row['ID']} width='20' class='finalizar-glosa' title='Finalizar' ></a>";
            }
            echo "</td></tr>";
        }

        echo"</table>";
    }else{

        echo "<center><b>Nenhum resultado foi encontrado!</b></center>";

    }



}

function listar_faturas ($tipo,$cod,$inicio,$fim,$nome,$inicio_feita,$fim_feita,$cancelado,$tipo_documento){

    $cond_cancelado= $cancelado ==0 ? "and f.CANCELADO_POR is NULL":'';
    $cond_tipo_documento= $tipo_documento =='t'?'':" and f.ORCAMENTO=".$tipo_documento;

    if($_SESSION["empresa_principal"]== 1 || in_array($_SESSION["empresa_principal"],[11, 2, 4, 5])){

        $condUrFsa ='';
        if($_SESSION["empresa_principal"] != 1){
            $condUrFsa = "AND f.UR = {$_SESSION["empresa_principal"]}";
        }
        switch($tipo){

            case 'paciente':
                if($inicio == '' && $fim == '' && $inicio_feita == '' && $fim_feita == ''){
                    $condicao= "PACIENTE_ID = {$cod} and f.STATUS <> -1 $cond_cancelado $cond_tipo_documento order by c.nome ASC,f.DATA   DESC";
                }else if($inicio != '' && $fim != '' && $inicio_feita == '' && $fim_feita == ''){
                    $condicao= "PACIENTE_ID = {$cod} and f.STATUS <> -1  and DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}' AND DATA_FIM BETWEEN '{$inicio}' AND '{$fim}' $cond_cancelado $cond_tipo_documento order by c.nome ASC,f.DATA DESC";
                }else{

                    $condicao= "PACIENTE_ID = {$cod} and f.STATUS <> -1  and DATE_FORMAT(f.DATA,'%Y-%m-%d' ) BETWEEN '{$inicio_feita}' AND '{$fim_feita}' $cond_cancelado $cond_tipo_documento order by c.nome ASC,f.DATA DESC";

                }

                $sql="select f.*,
                            (case when f.ORCAMENTO = 1 then 'OR&Ccedil;AMENTO'
                             when f.ORCAMENTO = 2 then 'OR&Ccedil;AMENTO PRO.'
                             when f.ORCAMENTO = 3 then 'OR&Ccedil;A. ADITIVO.'
                             else 'FATURA' end ) tipo,
                             f.DATA,
                            DATE_FORMAT(f.DATA,'%d/%m/%Y %H:%i:%s' ) as data1,
                            DATE_FORMAT(f.DATA_FIM,'%d/%m/%Y' ) as datafim,
                            DATE_FORMAT(f.DATA_INICIO,'%d/%m/%Y ' ) as datainicio,
                            c.nome,
                            c.status as status_paciente,
                              (Select
                                MAX(f2.ID)
                                 from
                                 fatura as f2
                                 where
                                 f2.ORCAMENTO = 1 and
                                 f2.PACIENTE_ID = f.PACIENTE_ID ) as max_id
                            from
                            fatura as f inner join
                            clientes as c on (c.idClientes = f.PACIENTE_ID)
                            where
                            {$condicao} ";
                break;

            case 'operadora':
                if($inicio == '' && $fim == '' && $inicio_feita == '' && $fim_feita == ''){
                    $condicao= "op.OPERADORAS_ID = {$cod} and f.STATUS <> -1 $condUrFsa $cond_cancelado $cond_tipo_documento order by c.nome ASC,f.DATA DESC";
                }else if($inicio != '' && $fim != '' && $inicio_feita == '' && $fim_feita == ''){
                    $condicao= "op.OPERADORAS_ID = {$cod} and f.STATUS <> -1   and f.DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}' AND f.DATA_FIM BETWEEN '{$inicio}' AND '{$fim}' $condUrFsa $cond_cancelado $cond_tipo_documento order by c.nome ASC,f.DATA DESC";
                }else{
                    $condicao= "op.OPERADORAS_ID = {$cod} and f.STATUS <> -1   and DATE_FORMAT(f.DATA,'%Y-%m-%d' ) BETWEEN '{$inicio_feita}' AND '{$fim_feita}' $cond_cancelado $condUrFsa $cond_tipo_documento order by c.nome ASC,f.DATA DESC";

                }


                //$sql="select f.*, c.nome, c.convenio from fatura as f inner join clientes as c on (c.idClientes = f.PACIENTE_ID) inner join operadoras_planodesaude on (c.convenio = planosdesaude_id )  where {$condicao} ";

                $sql = "SELECT
			f.*,
                        (case when f.ORCAMENTO = 1 then 'OR&Ccedil;AMENTO'
                         when f.ORCAMENTO = 2 then 'OR&Ccedil;AMENTO PRO.'
                         when f.ORCAMENTO = 3 then 'OR&Ccedil;A. ADITIVO.'
                         else 'FATURA' end ) tipo,
                         f.DATA,
                        DATE_FORMAT(f.DATA,'%d/%m/%Y %H:%i:%s' ) as data1,
                        DATE_FORMAT(f.DATA_FIM,'%d/%m/%Y' ) as datafim, DATE_FORMAT(f.DATA_INICIO,'%d/%m/%Y ' ) as datainicio,
			c.nome,
                        c.status as status_paciente,
                              (Select
                                MAX(f2.ID)
                                 from
                                 fatura as f2
                                 where
                                 f2.ORCAMENTO = 1 and
                                 f2.PACIENTE_ID = f.PACIENTE_ID ) as max_id
			FROM
			operadoras_planosdesaude as op Right Join
			 
			fatura as f on ( f.PLANO_ID = op.PLANOSDESAUDE_ID) RIGHT JOIN
			clientes as c on (c.idClientes= f.PACIENTE_ID)
			WHERE
			{$condicao}";


                break;

            case 'unidade':
                if($inicio == '' && $fim == '' && $inicio_feita == '' && $fim_feita == ''){
                    $condicao= "f.UR = {$cod} and f.STATUS <> -1 $cond_cancelado  $cond_tipo_documento order by c.nome ASC,f.DATA DESC";
                }else if($inicio != '' && $fim != '' && $inicio_feita == '' && $fim_feita == ''){
                    $condicao= "f.UR = {$cod} and  f.STATUS <> -1 and f.DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}' AND f.DATA_FIM BETWEEN '{$inicio}' AND '{$fim}' $cond_cancelado $cond_tipo_documento order by c.nome ASC,f.DATA DESC";
                }else{
                    $condicao= "f.UR= {$cod} and f.STATUS <> -1 and DATE_FORMAT(f.DATA,'%Y-%m-%d' ) BETWEEN '{$inicio_feita}' AND '{$fim_feita}' $cond_cancelado $cond_tipo_documento order by c.nome ASC,f.DATA DESC";

                }


                //$sql="select f.*, c.nome, c.convenio from fatura as f inner join clientes as c on (c.idClientes = f.PACIENTE_ID) inner join operadoras_planodesaude on (c.convenio = planosdesaude_id )  where {$condicao} ";

                $sql = "SELECT
			f.*,
                        (case when f.ORCAMENTO = 1 then 'OR&Ccedil;AMENTO'
                         when f.ORCAMENTO = 2 then 'OR&Ccedil;AMENTO PRO.'
                         when f.ORCAMENTO = 3 then 'OR&Ccedil;A. ADITIVO.'
                         else 'FATURA' end ) tipo,
                         f.DATA,
                        DATE_FORMAT(f.DATA,'%d/%m/%Y %H:%i:%s' ) as data1,
                        DATE_FORMAT(f.DATA_FIM,'%d/%m/%Y' ) as datafim, DATE_FORMAT(f.DATA_INICIO,'%d/%m/%Y ' ) as datainicio,
			c.nome,
                        c.status as status_paciente,
                              (Select
                                MAX(f2.ID)
                                 from
                                 fatura as f2
                                 where
                                 f2.ORCAMENTO = 1 and
                                 f2.PACIENTE_ID = f.PACIENTE_ID ) as max_id

			FROM
			fatura as f  left join clientes as c on (c.idClientes = f.PACIENTE_ID)
			WHERE
			{$condicao}";



                break;
            case '':
                if($inicio == '' && $fim == ''){
                    $condicao= " f.STATUS <> -1 $cond_cancelado $cond_tipo_documento $condUrFsa order by c.nome ASC,f.DATA DESC ";
                }
                if($inicio != '' && $fim != ''){
                    $condicao= " f.STATUS <> -1 f.DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}' $condUrFsa AND f.DATA_FIM BETWEEN '{$inicio}' AND '{$fim}' $cond_cancelado $cond_tipo_documento order by c.nome ASC,f.DATA DESC";
                }
                $sql = "SELECT
			f.*,
                        (case
                        when f.ORCAMENTO = 1 then 'OR&Ccedil;AMENTO'
                        when f.ORCAMENTO = 2 then 'OR&Ccedil;AMENTO PRO.'
                        when f.ORCAMENTO = 3 then 'OR&Ccedil;A. ADITIVO.'
                        else 'FATURA' end ) tipo,
                        f.DATA,
                        DATE_FORMAT(f.DATA,'%d/%m/%Y %H:%i:%s' ) as data1,
                        DATE_FORMAT(f.DATA_FIM,'%d/%m/%Y' ) as datafim, DATE_FORMAT(f.DATA_INICIO,'%d/%m/%Y ' ) as datainicio,
			c.nome,
                        c.status as status_paciente,
                              (Select
                                MAX(f2.ID)
                                 from
                                 fatura as f2
                                 where
                                 f2.ORCAMENTO = 1 and
                                 f2.PACIENTE_ID = f.PACIENTE_ID ) as max_id

			FROM
			operadoras_planosdesaude as op Right Join
			clientes as c on (c.convenio = op.PLANOSDESAUDE_ID) RIGHT JOIN
				fatura as f on (c.idClientes = f.PACIENTE_ID)
				WHERE
				{$condicao}";

                break;
        }

        $result = mysql_query($sql);

        $resposta = mysql_num_rows($result);


        if($resposta > 0){

            echo "<table class='mytable' width=100% ><thead>
	<tr>
	<th>Paciente</th>
        <th>Data</th>
	<th>Tipo</th>
	<th>Per&iacute;odo</th>
	<th>Status</th>
	<th>Op&ccedil;&otilde;es</th>


	</tr>
	</thead>";

            while($row = mysql_fetch_array($result)){


                $arrayStatus = ModelFatura::getStatusById($row['STATUS']);
                $status = $arrayStatus[0]['status'];
               // var_dump($arrayStatus);
                $responseConsenso = Consenso::getConsensoByFaturaId($row['ID']);

                $msgConsenso = '';
                $dataConsenso = '';
                if(!empty($responseConsenso)){
                   $dataConsenso = \DateTime::createFromFormat('Y-m-d H:i:s',$responseConsenso[0]['created_at']);
                    $msgConsenso = "Foi feito Consenso da fatura com a auditora externa {$responseConsenso[0]['auditora_externa']} em {$dataConsenso->format('d/m/Y')}";
                }

                if($row['CANCELADO_POR'] != NULL){
                    $status= 'Cancelado';
                }

                $riscar_palavra = $row['CANCELADO_POR']==NULL?'':'riscar-palavra';

                echo"<tr>
                  <td>";
                if($row['ORCAMENTO'] == 0){
                    if(empty($row['CAPA_LOTE'])){
                        if(!empty($msgConsenso)){
                            echo" <label>
                   			 		<img src='../../utils/details.png' width='16' height='16' title=' {$msgConsenso}' />
                				</label>";
                        }

                        echo "<img align='right' class='cancelar-fatura' idfatura ='{$row['ID']}' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";

                    }else{
                        echo " 	<label>
                   			 		<img src='../../utils/details.png' width='16' height='16' title='Fatura não pode ser cancelada, já inserida no Lote: {$row['CAPA_LOTE']}. {$msgConsenso}' />
                				</label>";
                    }
                }else{
                    echo "<img align='right' class='cancelar-fatura' idfatura ='{$row['ID']}' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";

                }


                echo"     <span class='{$riscar_palavra}'>{$row['nome']} <b>GUIA:{$row['GUIA_TISS']}</b></span>
                  </td>
                  <td>{$row['data1']}</td>
                  <td>{$row['tipo']} </td>
                  <td>{$row['datainicio']} at&eacute {$row['datafim']}</td>
                  <td class='status_fatura'>{$status}</td>
                  <td>";
                //if($row['STATUS'] == 3 || $row['STATUS'] == 4  ){
                //echo"<a href='#'><img src='../utils/fichas_24x24.png' width='16' width='20' title='Visualizar' class='v_glosa' idfatura ='{$row['ID']}'>";
                //}
                //if($row['STATUS'] != 3 && $row['STATUS'] != 4 ){
                echo"<a href='#'><img src='../utils/fichas_24x24.png' width='16' width='20' title='Visualizar' class='v_glosa_2' idfatura ='{$row['ID']}' empresa='{$row['UR']}' av='{$row{'ORCAMENTO'}}' ></a>";
                echo"&nbsp;&nbsp;&nbsp;<a href='#'><img src='../utils/excel_24x24.png' width='16' width='20' title='Exportar para Excel.' class='v_exportar' idfatura ='{$row['ID']}' av='{$row{'ORCAMENTO'}}' ></a>";
                if($row['status_paciente'] == 4 && $row['ID'] == $row['max_id'] && $row['CANCELADO_POR'] == NULL){
                    //echo"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=?view=edt_orcamento&id={$row['ID']}&av={$row['ORCAMENTO']}&n=1><img src='../utils/prescricao_16x16.png' width='16' width='20' title='Usar para novo Orçamento.'  ></a>";

                }

                if(($row['STATUS'] == 6 || $row['STATUS'] == 2 || $row['STATUS'] == 5) &&
                      $row['CANCELADO_POR'] == NULL && $row['ORCAMENTO'] != 1){

                    if(in_array($_SESSION["empresa_principal"], [11, 2, 4, 5]) && $row['ORCAMENTO'] == 0 ){

                    }else{
                        echo"&nbsp;&nbsp;&nbsp;&nbsp;<a href=?view=edt_fatura&id={$row['ID']}&av={$row['ORCAMENTO']}><img src='../utils/edit.png' width='16' width='20' title='Editar Fatura'  idfatura ='' ></a>";

                    }
                }

                $cond_status_fatura   = $row['STATUS'] == 3;
                if(
                    $cond_status_fatura      &&
                    ($_SESSION['modfat'] == 1 ||$_SESSION['adm_user']) &&
                    $row['CANCELADO_POR'] == NULL){

                    if(in_array($_SESSION["empresa_principal"],[11, 2, 4, 5]) && $row['ORCAMENTO'] ==0 ){

                    }else {

                        echo "&nbsp;&nbsp;&nbsp;&nbsp;<a class='reabrir_fatura' fatura='{$row['ID']}' paciente='{$row['nome']}'
 													tipo='{$row['tipo']}' periodo='{$row['datainicio']} at&eacute {$row['datafim']}' id='img-reabrir-{$row['ID']}'><img src='../utils/redimensionar.png' width='22' width='22' title='Reabrir Fatura'></a>";
                    }
                }

                if(!empty($msgConsenso) && $row['ORCAMENTO'] == 0){
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;<a  href='imprimir_fatura_original.php?id={$row['ID']}' target='_blank'><img src='../utils/nova_avaliacao_24x24' width='22' width='22' title='Imprimir Fatura Original'></a>";
                }



                echo "</td></tr>";
            }

            echo"</table>";
        }else{

            echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
        }
    }else{

        switch($tipo){

            case 'paciente':
                if($inicio == '' && $fim == '' && $inicio_feita == '' && $fim_feita == ''){
                    $condicao= "PACIENTE_ID = {$cod} and f.STATUS <> -1 $cond_cancelado $cond_tipo_documento order by c.nome,DATA,STATUS  DESC";
                }else if($inicio != '' && $fim != '' && $inicio_feita == '' && $fim_feita == ''){
                    $condicao= "PACIENTE_ID = {$cod}  and f.STATUS <> -1 and DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}' AND DATA_FIM BETWEEN '{$inicio}' AND '{$fim}' $cond_cancelado $cond_tipo_documento order by c.nome,DATA,STATUS  DESC";
                }else{

                    $condicao= "PACIENTE_ID = {$cod} and f.STATUS <> -1  and DATE_FORMAT(f.DATA,'%Y-%m-%d' ) BETWEEN '{$inicio_feita}' AND '{$fim_feita}' $cond_cancelado $cond_tipo_documento order by c.nome,DATA,STATUS  DESC";

                }

                $sql="select f.*, DATE_FORMAT(f.DATA_FIM,'%d/%m/%Y' ) as datafim, DATE_FORMAT(f.DATA_INICIO,'%d/%m/%Y ' ) as datainicio, c.nome from fatura as f inner join clientes as c on (c.idClientes = f.PACIENTE_ID) where {$condicao} ";
                break;

            case 'operadora':
                if($inicio == '' && $fim == '' && $inicio_feita == '' && $fim_feita == ''){
                    $condicao= "op.OPERADORAS_ID = {$cod} and f.STATUS <> -1 $cond_cancelado $cond_tipo_documento order by c.nome,f.DATA,STATUS DESC";
                }else if($inicio != '' && $fim != '' && $inicio_feita == '' && $fim_feita == ''){
                    $condicao= "op.OPERADORAS_ID = {$cod} and f.STATUS <> -1 and f.DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}' AND f.DATA_FIM BETWEEN '{$inicio}' AND '{$fim}' $cond_cancelado $cond_tipo_documento order by c.nome,DATA,STATUS DESC";
                }else{
                    $condicao= "op.OPERADORAS_ID = {$cod} and f.STATUS <> -1  and DATE_FORMAT(f.DATA,'%Y-%m-%d' ) BETWEEN '{$inicio_feita}' AND '{$fim_feita}' $cond_cancelado $cond_tipo_documento order by c.nome,DATA,STATUS  DESC";

                }


                //$sql="select f.*, c.nome, c.convenio from fatura as f inner join clientes as c on (c.idClientes = f.PACIENTE_ID) inner join operadoras_planodesaude on (c.convenio = planosdesaude_id )  where {$condicao} ";

                $sql = "SELECT
				f.*,DATE_FORMAT(f.DATA_FIM,'%d/%m/%Y' ) as datafim, DATE_FORMAT(f.DATA_INICIO,'%d/%m/%Y ' ) as datainicio,
				c.nome
				FROM
				operadoras_planosdesaude as op Right Join
				clientes as c on (c.convenio = op.PLANOSDESAUDE_ID) RIGHT JOIN
				fatura as f on (c.idClientes = f.PACIENTE_ID)
				WHERE
				{$condicao}";

                break;
            case '':
                if($inicio == '' && $fim == ''){
                    $condicao= " f.STATUS <> -1 and  c.empresa= ".$_SESSION['empresa_principal']." $cond_cancelado $cond_tipo_documento order by c.nome,f.DATA,STATUS  DESC";
                }
                if($inicio != '' && $fim != ''){
                    $condicao= " f.STATUS <> -1 and f.DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}' AND f.DATA_FIM BETWEEN '{$inicio}' AND '{$fim}' and c.empresa = ".$_SESSION["empresa_principal"]."  $cond_cancelado $cond_tipo_documento order by c.nome,DATA,STATUS  DESC";
                }
                $sql = "SELECT
		f.*,DATE_FORMAT(f.DATA_FIM,'%d/%m/%Y' ) as datafim, DATE_FORMAT(f.DATA_INICIO,'%d/%m/%Y ' ) as datainicio,
		c.nome
		FROM
		operadoras_planosdesaude as op Right Join
		clientes as c on (c.convenio = op.PLANOSDESAUDE_ID) RIGHT JOIN
		fatura as f on (c.idClientes = f.PACIENTE_ID)
					WHERE
				{$condicao}";
                break;
        }

        $result = mysql_query($sql);

        $resposta = mysql_num_rows($result);


        if($resposta > 0){

            echo "<table class='mytable' width=100% ><thead>
				<tr>
				<th>Paciente</th>

				<th>Per&iacute;odo</th>
				<th>Status</th>
				<th>Op&ccedil;&otilde;es</th>


				</tr>
				</thead>";

            while($row = mysql_fetch_array($result)){

                $status = $arrayStatus[0]['status'];

                echo"<tr><td>{$row['nome']}</td><td>{$row['datainicio']} at&eacute {$row['datafim']}</td><td>{$status}</td><td>";
                if($row['STATUS'] == 3 || $row['STATUS'] == 4  ){
                    echo"<a href='#'><img src='../utils/fichas_24x24.png' width='16' width='20' title='Visualizar' class='v_glosa_3' idfatura ='{$row['ID']}' av='{$row{'ORCAMENTO'}}'>";
                }
                if($row['STATUS'] != 3 && $row['STATUS'] != 4 ){
                    echo"<a href='#'><img src='../utils/fichas_24x24.png' width='16' width='20' title='Visualizar' class='v_glosa_3' idfatura ='{$row['ID']}' av='{$row{'ORCAMENTO'}}' >";
                }


                echo "</td></tr>";
            }

            echo"</table>";
        }else{

            echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
        }

    }

}


function gerenciar_lote ($tipo, $cod, $inicio, $fim, $nome, $tipo_documento){
    if($tipo_documento == -1){
        $condTipoDocumento= '';
    }else{
        $condTipoDocumento = "AND f.ORCAMENTO = '{$tipo_documento}'";
    }

    switch($tipo){

        case 'paciente':
            if($inicio == '' && $fim == ''){
                $condicao= "PACIENTE_ID = {$cod} and CAPA_LOTE != 0  $condTipoDocumento GROUP BY f.CAPA_LOTE order   by DATA DESC";
            }
            if($inicio != '' && $fim != ''){
                $condicao= "PACIENTE_ID = {$cod} and CAPA_LOTE != 0 and DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}' AND DATA_FIM BETWEEN '{$inicio}' AND '{$fim}'  $condTipoDocumento GROUP BY f.CAPA_LOTE order by DATA DESC";
            }

            $sql = "select f.*,
			          DATE_FORMAT(f.DATA_FIM,'%d/%m/%Y' ) as datafim,
			          DATE_FORMAT(f.DATA_INICIO,'%d/%m/%Y %H:%i:%s' ) as datainicio,
			          DATE_FORMAT(f.DATA_ENVIO_PLANO,'%d/%m/%Y' ) as dataenvio,
			          c.nome,
			          f.ORCAMENTO

			        from fatura as f
			          inner join clientes as c on (c.idClientes = f.PACIENTE_ID)
			        where {$condicao} ";
            break;

        case 'operadora':
            if($inicio == '' && $fim == ''){
                $condicao= "op.OPERADORAS_ID = {$cod} and f.CAPA_LOTE !=0  $condTipoDocumento GROUP BY f.CAPA_LOTE order by f.DATA DESC";
            }
            if($inicio != '' && $fim != ''){
                $condicao= "op.OPERADORAS_ID = {$cod} and f.CAPA_LOTE !=0 and f.DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}' AND f.DATA_FIM BETWEEN '{$inicio}' AND '{$fim}'  $condTipoDocumento GROUP BY f.CAPA_LOTE order by DATA DESC";
            }


            //$sql="select f.*, c.nome, c.convenio from fatura as f inner join clientes as c on (c.idClientes = f.PACIENTE_ID) inner join operadoras_planodesaude on (c.convenio = planosdesaude_id )  where {$condicao} ";

            $sql = "SELECT
						f.*,
						DATE_FORMAT(f.DATA_FIM,'%d/%m/%Y' ) as datafim,
						DATE_FORMAT(f.DATA_INICIO,'%d/%m/%Y ' ) as datainicio,
						c.nome,
			          DATE_FORMAT(f.DATA_ENVIO_PLANO,'%d/%m/%Y' ) as dataenvio,
			           f.ORCAMENTO
			FROM
			operadoras_planosdesaude as op Right Join
			clientes as c on (c.convenio = op.PLANOSDESAUDE_ID) RIGHT JOIN
			fatura as f on (c.idClientes = f.PACIENTE_ID)
			WHERE
			{$condicao}";

            break;
    }

    $result = mysql_query($sql);
    $resposta = mysql_num_rows($result);


    if($resposta > 0){



        echo "<table class='mytable' width=100% ><thead>
			<tr>
			<th>Lote</th>
			<th>Per&iacute;odo</th>
			<th>Dados</th>
			<th>Opções</th>
      <th>Tiss XML</th>
			<th>Opção </th>

			</tr>
			</thead>";

        while($row = mysql_fetch_array($result)){
            $dataEnvio ="<a href=#><img src='../utils/msg_24x24.png' width='16' fatura='{$row['ID']}' lote={$row['CAPA_LOTE']} class='enviar-email' width='20' title='Enviar Fatura' ></a>";
            $protocoloEnvio ='';
            if($row['dataenvio'] != '00/00/0000'){
                $dataEnvio = " Enviado em:" .$row['dataenvio'];
                $protocoloEnvio = "Protocolo: {$row['protocolo_envio']}";

            }
            if($row['STATUS'] == 2){
                $status= 'Pendente';
            }

            if($row['STATUS'] == 1){
                $status= 'Enviado';
            }
            if($row['STATUS'] == 5){
                $status= 'Faturado';
            }

            if($row['STATUS'] == 3){
                $status= 'Em An&aacute;lise.';
            }

            if($row['ORCAMENTO'] == 0 ){
                $tipoLote ="Fatura";
            }else if($row['ORCAMENTO'] == 1  ){
                $tipoLote ="Orçamento Inicial";
            }else if($row['ORCAMENTO'] == 2  ){
                $tipoLote ="Orçamento Prorrogação";
            }else if($row['ORCAMENTO'] == 3  ){
                $tipoLote ="Orçamento Aditivo";
            }

            $guias = GuiaSADT::getLoteIdByCapaLote($row['CAPA_LOTE']);
            $xml   = GuiaSADT::getXMLByGuiaId($guias[0]['id']);

            $temGuiaSADT = isset($guias) && !empty($guias);
            $temXML      = isset($xml)   && !empty($xml);

            $acaoXML = $temGuiaSADT && $temXML ?
                "<li><a href='tissxml/index.php?action=baixar-xml&capalote={$row['CAPA_LOTE']}&xmlId={$xml[0]['id']}'>Baixar XML</a></li>" :
                '';

            $btVImpressao = $temGuiaSADT && $temXML ?
                "<li><a href='tissxml/index.php?action=guias-listar&loteId={$guias[0]['id']}&capalote={$row['CAPA_LOTE']}'>Listar Guias</a></li>" :
                null;

            /*
             * $btReprocessarXML = $temGuiaSADT && $temXML ?
                "<li><a target='_blank' href='tissxml/index.php?action=processar-xml&loteId={$guias[0]['id']}'>Reprocessar XML</a></li>" :
                null;
            */
            $faturas = ModelFatura::getFaturasByCapaLote($row['CAPA_LOTE']);

            $conteudo = '';
            foreach($faturas as $f){
                $nomePaciente = $f['nome'];
                $inicio = implode('/',array_reverse(explode('-',$f['DATA_INICIO'])));
                $fim = implode('/',array_reverse(explode('-',$f['DATA_FIM'])));
                //$conteudo .= "<b>".$nomePaciente."</b><br> ".$inicio." - ".$fim." <br> ";
                $conteudo .= "<span ><p><b>".ucwords(strtolower($nomePaciente))."</b></p><p>".$inicio." - ".$fim."</p></span>";
            }


            echo  "<tr>
				      <td>";
            if($f['ORCAMENTO'] == 0){
                echo  " <img src='../utils/delete.png' width='16' title='Cancelar Lote: {$row['CAPA_LOTE']}'
 						lote = '{$row['CAPA_LOTE']}' class='paciente-lote-mouse-over cancelar-lote' > ";

            }
            echo " {$row['CAPA_LOTE']}

				       				<img src='../utils/details.png' width='16' title='{$conteudo}' class='paciente-lote-mouse-over' >

								</img>
							</span>
					</td>
				      <td>{$row['datainicio']} at&eacute {$row['datafim']}</td>
				      <td id='{$row['CAPA_LOTE']}'>
				      		<b>Tipo:</b> $tipoLote
				      	<br>
				      		<b>Status:</b> $status
						</td>
				      <td>
				      	<a href=imprimir_lote.php?id={$row['CAPA_LOTE']}  target='_blank'>
				      	<img src='../utils/fichas_24x24.png' width='16' width='20' title='Visualizar' ></a>
				      	</td>
				      	 <td>
                <ul>
                  <li><a href='tissxml/index.php?action=nova-guia&tipo=spsadt&capalote={$row['CAPA_LOTE']}'>Nova Guia SADT</a></li>
                  <li><a href='tissxml/index.php?action=nova-guia&tipo=resumointernacao&capalote={$row['CAPA_LOTE']}'>Resumo de Internação</a></li>
                  {$acaoXML}
                  {$btReprocessarXML}
                  {$btVImpressao}
                </ul>
				      </td>
				      <td id='fatura-{$row['ID']}'>{$dataEnvio} <br> $protocoloEnvio</td>

				     </tr>"
            ;

        }

        echo"</table>";

    }else{
        echo "<center><b>Nenhum resultado foi encontrado!  </b></center>";
    }


}

function createDialogSimulacaoCusto()
{
    $html = <<<HTML
<div id="dialog-simulacao" title="Simular Custo" style="display:none;">
	<div class="wrap">
	</div>
</div>
HTML;
    return $html;
}

function cabecalhoFatura($id)
{
    $html = "";
    $dadosPaciente = [];
    $sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		UPPER(u.nome) AS usuario,DATE_FORMAT(f.DATA,'%Y-%m-%d') as DATA,f.DATA_INICIO,f.DATA_FIM,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo1,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresan,
		c.`nascimento`,
		c.diagnostico,
		c.codigo,
		p.nome as Convenio,
		p.id,
		p.id AS planodesaude,
		c.*,
		NUM_MATRICULA_CONVENIO,
		f.imposto_iss
		FROM
		fatura as f inner join
		clientes AS c on (f.PACIENTE_ID = c.idClientes) LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN 
		planosdesaude as p ON (p.id = c.convenio) INNER JOIN usuarios as u on (f.USUARIO_ID = u.idUsuarios)
		
		WHERE
		f.ID = {$id}
		ORDER BY
		c.nome DESC LIMIT 1";

    $result2 = mysql_query($sql2);
    $html.= "<table style='width:100%;' >";
    while($pessoa = mysql_fetch_array($result2)){
        foreach($pessoa AS $chave => $valor) {
            $pessoa[$chave] = stripslashes($valor);

        }

        $dadosPaciente['paciente'] = $pessoa['idClientes'];
        $dadosPaciente['plano'] = $pessoa['planodesaude'];
        $dadosPaciente['nome_plano'] = $pessoa['Convenio'];
        $dadosPaciente['empresa'] = $pessoa['empresa'];

        $idade = join("/",array_reverse(explode("-",$pessoa['nascimento'])))."(".$pessoa['idade']." anos)";

        if($idade == "00/00/0000( anos)"){
            $idade='';
        }
        $html.= "<tr bgcolor='#EEEEEE'>";
        $html.= "<td  style='width:60%;'><b>PACIENTE:</b></td>";
        $html.= "<td style='width:20%;' ><b>SEXO </b></td>";
        $html.= "<td style='width:30%;'><b>IDADE:</b></td>";
        $html.= "</tr>";
        $html.= "<tr>";
        $html.= "<td style='width:60%;'>".utf8_decode($pessoa['paciente'])."</td>";
        $html.= "<td style='width:20%;'>{$pessoa['sexo1']}</td>";
        $html.= "<td style='width:30%;'>{$idade}</td>";
        $html.= "</tr>";

        $html.= "<tr bgcolor='#EEEEEE'>";

        $html.= "<td style='width:60%;'><b>UNIDADE REGIONAL:</b></td>";
        $html.= "<td style='width:20%;'><b>CONV&Ecirc;NIO </b></td>";
        $html.= "<td style='width:30%;'><b>MATR&Iacute;CULA </b></td>";
        $html.= "</tr>";
        $html.= "<tr>";

        $html.= "<td style='width:60%;'>{$pessoa['empresan']}</td>";
        $html.= "<td style='width:20%;'>".strtoupper($pessoa['Convenio'])."</td>";
        $html.= "<td style='width:30%;'>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
        $html.= "</tr>";

        $html.= "<tr bgcolor='#EEEEEE'>";

        $html.= "<td style='width:60%;'><b>Faturista:</b></td>";
        $html.= "<td style='width:20%;'><b>Data faturado: </b></td>";
        $html.= "<td style='width:30%;'><b>Per&iacute;odo</b></td>";
        $html.= "</tr>";
        $html.= "<tr>";

        $html.= "<td style='width:60%;'>".utf8_decode($pessoa['usuario'])."</td>";
        $html.= "<td style='width:20%;'>".join("/",array_reverse(explode("-",$pessoa['DATA'])))."</td>";
        $html.= "<td style='width:30%;'>".join("/",array_reverse(explode("-",$pessoa['DATA_INICIO'])))." at&eacute; ".join("/",array_reverse(explode("-",$pessoa['DATA_FIM'])))."</td>";
        $html.= "</tr>";

        $html.= "<tr bgcolor='#EEEEEE'>";

        $html.= "<td style='width:60%;'><b>CID:</b></td>";
        $html.= "<td style='width:20%;'><b>CONTA MEDERI: </b></td>";
        $html.= "<td style='width:30%;'></td>";
        $html.= "</tr>";
        $html.= "<tr>";

        $html.= "<td style='width:60%;'>{$pessoa['diagnostico']}</td>";
        $html.= "<td style='width:20%;'>{$pessoa['codigo']}</td>";
        $html.= "<td style='width:30%;'></td>";
        $html.= "</tr>";
        $impostoIss = $pessoa['imposto_iss'];
    }
    $html.= "</table>";

    $html.= "<p>
                    <label for='iss'><b>ISS:</b></label>
                    {$impostoIss} %
                </p>";
    return [$html, $dadosPaciente];
}

function sqlDetalheFatura($id)
{
    $sql="
                    SELECT
                            f.*,
                            fa.OBS,
                            IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),B.DESC_MATERIAIS_FATURAR) AS principio,
                            (CASE f.TIPO
                            WHEN '0' THEN 'Medicamento'
                            WHEN '1' THEN 'Material'
                             WHEN '3' THEN 'Dieta' END) as apresentacao,
                             (CASE f.TIPO
                            WHEN '0' THEN '3'
                            WHEN '1' THEN '4'
                             WHEN '3' THEN '5' END) as ordem,
                            1 as cod,
                            COALESCE(custo_item.custo,0.00) as custo_item,
                            f.CATALOGO_ID as cod_item,
                            COALESCE(custo_item.id,0) as custo_fatura_id,
                            fa.PLANO_ID,
                            unidades_fatura.UNIDADE AS unidade_medida
		    FROM
                                faturamento as f  INNER JOIN
                                `catalogo` B ON (f.CATALOGO_ID = B.ID)
                                LEFT JOIN unidades_fatura ON f.UNIDADE = unidades_fatura.ID
                                inner join fatura fa on (f.FATURA_ID=fa.ID) LEFT JOIN
                                custo_item_fatura as custo_item on (fa.PACIENTE_ID = custo_item.paciente_id 
                                                                    and custo_item.catalogo_id =f.CATALOGO_ID
                                                                    and custo_item.tipo = f.TIPO)
		    WHERE
			f.FATURA_ID = {$id} and
			f.TIPO IN (0,1,3) 
                        and f.CANCELADO_POR is NULL
							
		UNION		
			SELECT 
                            f.*,
                            fa.OBS,
			    vc.DESC_COBRANCA_PLANO AS principio,                           
		           ('Servi&ccedil;os') AS apresentacao,	
                            1 as ordem,						  
			    vc.COD_COBRANCA_PLANO as cod,
                            COALESCE(custo_item.custo,0.00) as custo_item,
                            vc.id as cod_item,
                            COALESCE(custo_item.id,0) as custo_fatura_id,
                            fa.PLANO_ID,
                            unidades_fatura.UNIDADE AS unidade_medida
		      FROM 
                            faturamento as f INNER JOIN				      
                             valorescobranca as vc on (f.CATALOGO_ID =vc.id) 
                             LEFT JOIN unidades_fatura ON f.UNIDADE = unidades_fatura.ID
                             inner join fatura fa on (f.FATURA_ID=fa.ID) LEFT JOIN
                            custo_item_fatura as custo_item on (fa.PACIENTE_ID = custo_item.paciente_id 
                                                            and custo_item.valorescobranca_id =vc.id
                                                            and custo_item.tipo = f.TIPO)
                      WHERE 
                            f.FATURA_ID= {$id} and 
                            f.TIPO =2 and 
                            f.TABELA_ORIGEM='valorescobranca'	
                              and f.CANCELADO_POR is NULL
                     UNION           
                      SELECT 
                            f.*,
                            fa.OBS,
			                 vc.item AS principio,
		                  ('Servi&ccedil;os') AS apresentacao,
                            1 as ordem,						  
			               '' as cod,
                            COALESCE(custo_item.custo,0.00) as custo_item,
                            vc.id as cod_item,
                            COALESCE(custo_item.id,0) as custo_fatura_id,
                            fa.PLANO_ID,
                            unidades_fatura.UNIDADE AS unidade_medida
		      FROM 
                            faturamento as f INNER JOIN
                             cobrancaplanos as vc on (f.CATALOGO_ID =vc.id) 
                             LEFT JOIN unidades_fatura ON f.UNIDADE = unidades_fatura.ID
                             inner join fatura fa on (f.FATURA_ID=fa.ID)  LEFT JOIN
                            custo_item_fatura as custo_item on (fa.PACIENTE_ID = custo_item.paciente_id 
                                                            and custo_item.cobrancaplano_id =vc.id
                                                            and custo_item.tipo = f.TIPO)
                      WHERE 
                            f.FATURA_ID= {$id} and 
                            f.TIPO =2 and 
                            f.TABELA_ORIGEM='cobrancaplano' 
                             and f.CANCELADO_POR is NULL
			
			UNION
			SELECT DISTINCT
			                 f.*,
                                         fa.OBS,
                                        CO.item AS principio,
                                        
                                        ('Equipamentos') AS apresentacao,
                                            2 as ordem,	
                                         '' as cod, 
                            COALESCE(custo_item.custo,0.00) as custo_item,
                            CO.id as cod_item,
                            COALESCE(custo_item.id,0) as custo_fatura_id,
                            fa.PLANO_ID,
                            unidades_fatura.UNIDADE AS unidade_medida
					      	
			FROM 
                                    faturamento as f INNER JOIN
                                    cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)  
                                    LEFT JOIN unidades_fatura ON f.UNIDADE = unidades_fatura.ID
                                    inner join fatura fa on (f.FATURA_ID=fa.ID) LEFT JOIN
                                   custo_item_fatura as custo_item on (fa.PACIENTE_ID = custo_item.paciente_id
                                    and custo_item.cobrancaplano_id =CO.id
                                    and custo_item.tipo = f.TIPO)
						
		      WHERE 
                                    f.FATURA_ID= {$id} and
                                    f.TIPO =5  and  
                                    f.TABELA_ORIGEM='cobrancaplano' 
                                     and f.CANCELADO_POR is NULL
                                
                        UNION
			SELECT DISTINCT
			                             f.*,
                                         fa.OBS,
                                        CO.DESC_COBRANCA_PLANO AS principio,
                                                                          
                                        ('Equipamentos') AS apresentacao,
                                            2 as ordem,	
                                         CO.COD_COBRANCA_PLANO as cod, 
                            COALESCE(custo_item.custo,0.00) as custo_item,
                            CO.id as cod_item,
                            COALESCE(custo_item.id,0) as custo_fatura_id,
                            fa.PLANO_ID,
                            unidades_fatura.UNIDADE AS unidade_medida
					      	
			FROM 
                                    faturamento as f INNER JOIN
                                    valorescobranca CO ON (f.CATALOGO_ID = CO.`id`)   
                                    LEFT JOIN unidades_fatura ON f.UNIDADE = unidades_fatura.ID
                                    inner join fatura fa on (f.FATURA_ID=fa.ID) LEFT JOIN
                                   custo_item_fatura as custo_item on (fa.PACIENTE_ID = custo_item.paciente_id
                                                            and custo_item.valorescobranca_id =CO.id
                                                            and custo_item.tipo = f.TIPO)
						
		      WHERE 
                                    f.FATURA_ID= {$id} and
                                    f.TIPO =5  and  
                                    f.TABELA_ORIGEM='valorescobranca' 
                                     and f.CANCELADO_POR is NULL

					
		UNION
			SELECT DISTINCT
			                 f.*,fa.OBS,
					 CO.item AS principio,				       
					   
					  ('Gasoterapia') AS apresentacao,
					 6 as ordem,	
					'' as cod, 
                            COALESCE(custo_item.custo,0.00) as custo_item,
                            CO.id as cod_item,
                            COALESCE(custo_item.id,0) as custo_fatura_id,
                            fa.PLANO_ID,
                            unidades_fatura.UNIDADE AS unidade_medida
					      	
				FROM 
                                        faturamento as f INNER JOIN
                                        cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)
                                         LEFT JOIN unidades_fatura ON f.UNIDADE = unidades_fatura.ID
                                        inner join fatura fa on (f.FATURA_ID=fa.ID) LEFT JOIN
                            custo_item_fatura as custo_item on (fa.PACIENTE_ID = custo_item.paciente_id 
                                                            and custo_item.cobrancaplano_id =CO.id
                                                            and custo_item.tipo = f.TIPO)
				WHERE 
					     f.FATURA_ID= {$id} and
					      f.TIPO =6  and f.TABELA_ORIGEM='cobrancaplano' 
                                               and f.CANCELADO_POR is NULL
              UNION
			SELECT DISTINCT
			                 f.*,fa.OBS,
					 vc.DESC_COBRANCA_PLANO AS principio,				       
					  
					  ('Gasoterapia') AS apresentacao,
					 6 as ordem,	
					 vc.COD_COBRANCA_PLANO as cod,
                            COALESCE(custo_item.custo,0.00) as custo_item,
                            vc.id as cod_item,
                            COALESCE(custo_item.id,0) as custo_fatura_id,
                            fa.PLANO_ID,
                            unidades_fatura.UNIDADE AS unidade_medida
					      	
				FROM 
                                        faturamento as f  inner join 
                                        valorescobranca as vc on (vc.id = f.CATALOGO_ID) 
                                        LEFT JOIN unidades_fatura ON f.UNIDADE = unidades_fatura.ID
                                        inner join fatura fa on (f.FATURA_ID=fa.ID) LEFT JOIN
                            custo_item_fatura as custo_item on (fa.PACIENTE_ID = custo_item.paciente_id 
                                                            and custo_item.valorescobranca_id =vc.id
                                                            and custo_item.tipo = f.TIPO)
				WHERE 
					     f.FATURA_ID= {$id} and
					      f.TIPO =6  and 
                                              f.TABELA_ORIGEM='valorescobranca' 	
                                               and f.CANCELADO_POR is NULL
					      			
	ORDER BY 
        ordem,
        tipo,
        principio";

    return $sql;
}

function detalheFatura($idFatura)
{
    list($html, $dadosPaciente) = cabecalhoFatura($idFatura);

    $sql = sqlDetalheFatura($idFatura);
    $result = mysql_query($sql);
    $num_row= mysql_num_rows($result);

    if(!$result){
        $html .= "<p>
			<b>ERRO: Problema na pesquisa dos itens da fatura. Entre em contato com o TI</b>
			</p>";
        return;
    }
    if($num_row == 0 ){
        $html .= "<p>
			<b>Nenhuma fatura foi encontrada.</b>
			</p>";
        return;
    }

    $html .= form_nova_simulacao($result, $dadosPaciente);

    echo $html;
}

function form_nova_simulacao($result, $dadosPaciente)
{
    $html = "<form id='form-simulacao-custo' method='post' action='query.php'>
                  <b>Indique o período da simulação:</b>
                  <br>
                  <input name='inicio_simulacao' type='date' required> - 
                  <input name='fim_simulacao' type='date' required>
                  <input name='query' type='hidden' value='salvar-simulacao'>
                  <button type='submit' style='display: none;'></button>
                  <input name='paciente' type='hidden' value='{$dadosPaciente['paciente']}'>
                  <br><br>
				  <table  width=100% class='mytable'>
						  <thead>
						<tr>		     	
							<th style='font-size:12px;'>Item</th>
							<th style='font-size:12px'>Qtd</th>
							<th style='font-size:12px'>Unidade</th>
							<th style='font-size:12px;'>Pre&ccedil;o Unt.</th>
							<th width='10%' style='font-size:12px;'>Custo Unit.</th>
							<th width='10%' style='font-size:12px;'>Taxa Tot.</th>
							<th style='font-size:12px;'>Margem do Item</th>
						</tr>
				   </thead>";
    $valorTotal = 0;
    $custoTotal = 0;
    $i = 1;
    while($row = mysql_fetch_array($result)){
        $fatura = $row['FATURA_ID'];
        if($i % 2 == 0){
            $bgColor = 'white';
        } else {
            $bgColor = '#dcdcdc';
        }
        $quantidade = $row['QUANTIDADE'];
        $custoItem = $row['VALOR_CUSTO'];
        $custoItemCalculo = $row['VALOR_CUSTO'];
        $desconto_acrescimo = ($quantidade*$row['VALOR_FATURA'])*($row['DESCONTO_ACRESCIMO'] /100);
        $valorTotal += ($quantidade*$row['VALOR_FATURA'])+$desconto_acrescimo;
        $custoTotal += $quantidade*$custoItemCalculo;
        $identItem = $row['TIPO'] . '-' . $row['cod_item'];
        if($quantidade == 0){
            $margemItem = 0;
            $corItem = 'orange';
        }else if($row['VALOR_FATURA'] == 0 && $custoItem == 0) {
            $margemItem = 0.00;
            $corItem = 'orange';
        }elseif($row['VALOR_FATURA'] > 0 && $custoItem > 0) {
            $margemItem = ((($row['VALOR_FATURA']*$quantidade) - ($custoItem*$quantidade) + $desconto_acrescimo ) / ($row['VALOR_FATURA']*$quantidade) ) * 100;
            $corItem = "green";
            if($margemItem < 25) {
                $corItem = 'red';
            }
        } else {
            $margemItem = $row['VALOR_FATURA'] > $custoItem ? '100.00' : '-100.00';
            $corItem = $row['VALOR_FATURA'] > $custoItem ? 'green' : 'red';
        }



        $html .= "<tr bgcolor='{$bgColor}'>
					<td width='50%'>{$row['NUMERO_TISS']} - {$row['principio']} </td>
					<td>
						<input type='number' class='qtd-simulacao' item='{$row['cod_item']}' required name='qtd[{$identItem}]' value='{$row['QUANTIDADE']}' >
						<input type='hidden' class='tipo' name='tipo[{$identItem}]' value='{$row['TIPO']}' >
						<input type='hidden' class='valor-custo' name='valor_custo[{$identItem}]' value='{$row['VALOR_CUSTO']}' >
						<input type='hidden' class='acrescimo-desconto' name='acrescimo_desconto[{$identItem}]' value='{$row['DESCONTO_ACRESCIMO']}' >
						<input type='hidden' class='tabela_origem' name='tabela_origem[{$identItem}]' value='{$row['TABELA_ORIGEM']}' >
						<input type='hidden' class='unidade' name='unidade[{$identItem}]' value='{$row['unidade_medida']}' >
						<input type='hidden' class='custo-item' name='custo_item[{$identItem}]' value='{$row['custo_item']}' >
					</td>
					<td>{$row['unidade_medida']}</td>
					<td>R$ <input type='text' class='preco-simulacao valor'  item='{$row['cod_item']}' required name='preco[{$identItem}]' value='" . number_format($row['VALOR_FATURA'], 2, ',', '.') . "' ></td>
					<td>R$ " . number_format($custoItem, 2, ',', '.') . "</td>
					<td>R$ " . number_format($desconto_acrescimo, 2, ',', '.') . "</td>
					<td style='color: {$corItem}'>" . number_format($margemItem, 2, ',', '.') . " %</td></tr>";
        $tipoDocumento = $row['ORCAMENTO'];
        $i++;
    }
    $pissCofins = 4.95;
    $ir = 3.5;
    $sqlIssFatura =  "select imposto_iss from fatura where id = {$fatura}";
    $resultIssFatura = mysql_query($sqlIssFatura);
    $issFatura = mysql_result($resultIssFatura,0);
    $issFatura = empty($issFatura) ? 0 :$issFatura;

    $custoTotal = ($valorTotal * ($pissCofins + $ir + $issFatura) / 100) + $custoTotal;
    $lucroReais = $valorTotal - $custoTotal;
    $lucroPorcento = ($valorTotal - $custoTotal) / $valorTotal;
    $lucroPorcento *= 100;

    if($valorTotal >= 4000) {
        $alerta = "Orçamento aprovado com ressalva(s)";
        $corAlerta = "orange";
        if ($lucroPorcento < 25 && $lucroReais < 1000) {
            $alerta = "Orçamento reprovado";
            $corAlerta = "red";
        } elseif ($lucroPorcento >= 25 && $lucroReais >= 1000) {
            $alerta = "Orçamento aprovado";
            $corAlerta = "green";
        }
    } elseif($valorTotal > 1000 && $valorTotal < 4000) {
        if ($lucroPorcento < 25) {
            $alerta = "Orçamento reprovado";
            $corAlerta = "red";
        } elseif ($lucroPorcento >= 25) {
            $alerta = "Orçamento aprovado";
            $corAlerta = "green";
        }
    } elseif($valorTotal <= 1000) {
        if ($lucroPorcento >= 15) {
            $alerta = "Orçamento aprovado";
            $corAlerta = "green";
        } elseif ($lucroPorcento < 15) {
            $alerta = "Orçamento reprovado";
            $corAlerta = "red";
        }
    }

    $html .= "</table>
              <br>
              <center>
				  <button id='calcular-simulacao'
					type='button'	
					class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' 
					role='button' 
					aria-disabled='false' >
					<span class='ui-button-text'>Calcular</span>
				  </button>
			  </center>
			  <br><br>
                <table class='mytable' width=100% >
                <thead>
                <tr>
					<th colspan='2' align='center' style='font-size: 10px !important;'>
						<input type='hidden' name='pis_cofins' id='pis-cofins' value='{$pissCofins}' >
						<input type='hidden' name='ir' id='ir' value='{$ir}' >
						<input type='hidden' name='iss' id='iss' value='{$issFatura}' >
						Valores dos encargos adiconados ao custo total: PISCOFINS {$pissCofins} %, IR {$ir} % mais o ISS informado acima.
						<br><b>OBS:</b>
						O valor total da fatura leva em consideração os descontos ou acréscimos
					</th>
				</tr>
				</thead>
				<tr>
					<td>
						<b> Valor Total Faturado:</b>
					</td>
					<td>
						R$ <span id='valor-total'>".number_format($valorTotal,2,',','')."</span>
					</td>
				</tr>
				 <tr>
					<td>
						<b>Custo Total + Encargos:</b>
					</td>
					<td>
						 R$ <span id='custo-encargo'> ".number_format($custoTotal,2,',','')."</span>
					</td>
				</tr>
				
				<tr>
					<td>
						<b>Margem:</b>
					</td>
					<td>
						 R$ <span id='margem'>".number_format($lucroReais,2,',','')."</span>
					</td>
				</tr>
				<tr>
					<td>
						<b>Margem Percentual &cong;</b>
					</td>
					<td>
						 <span id='margem-percentual'>".number_format($lucroPorcento,2,',','')."</span>%
					</td>
				</tr>
				<tr>
					<td colspan='2' id='alerta-margem' align='center' bgcolor='{$corAlerta}'>
						 <b>{$alerta}</b>
					</td>
				</tr>
                </table>
                 <input type='hidden' value='$tipoDocumento' name ='tipo-documento' id ='tipo-documento'>
                 <input type='hidden' value='{$fatura}' name ='id-fatura' id ='id-fatura'>
			 </form>";
    return $html;
}

function sqlSimulacaoVisualizar($simulacaoId)
{
    return <<<SQL
SELECT 
  orcamento_simulacao_item.*,
  orcamento_simulacao_item.id AS simItemId,
  IF(catalogo.DESC_MATERIAIS_FATURAR IS NULL, concat(catalogo.principio, ' - ', catalogo.apresentacao), catalogo.DESC_MATERIAIS_FATURAR) AS principio,
  CASE orcamento_simulacao_item.tipo
    WHEN '0' THEN 'Medicamento'
    WHEN '1' THEN 'Material'
    WHEN '3' THEN 'Dieta' 
  END as apresentacao,
  catalogo.ID as cod_item
FROM
  orcamento_simulacao_item
  INNER JOIN catalogo ON orcamento_simulacao_item.catalogo_id = catalogo.ID
WHERE
  orcamento_simulacao_item.simulacao_id = '{$simulacaoId}'
  AND orcamento_simulacao_item.tipo IN (0, 1, 3) 

UNION
  SELECT 
    orcamento_simulacao_item.*,
    orcamento_simulacao_item.id AS simItemId,
    valorescobranca.DESC_COBRANCA_PLANO AS principio,
    'Servi&ccedil;os' AS apresentacao,
    valorescobranca.id as cod_item
  FROM
    orcamento_simulacao_item
    INNER JOIN valorescobranca ON orcamento_simulacao_item.valorescobranca_id = valorescobranca.id
  WHERE
    orcamento_simulacao_item.simulacao_id = '{$simulacaoId}'
    AND orcamento_simulacao_item.tipo = '2'
    AND orcamento_simulacao_item.tabela_origem = 'valorescobranca'
    
  UNION
    SELECT 
      orcamento_simulacao_item.*,
      orcamento_simulacao_item.id AS simItemId,
      cobrancaplanos.item AS principio,
      'Servi&ccedil;os' AS apresentacao,
      cobrancaplanos.id as cod_item
    FROM
      orcamento_simulacao_item
      INNER JOIN cobrancaplanos ON orcamento_simulacao_item.cobrancaplano_id = cobrancaplanos.id
    WHERE
      orcamento_simulacao_item.simulacao_id = '{$simulacaoId}'
      AND orcamento_simulacao_item.tipo = '2'
      AND orcamento_simulacao_item.tabela_origem = 'cobrancaplano'

    UNION
      SELECT 
        orcamento_simulacao_item.*,
        orcamento_simulacao_item.id AS simItemId,
        cobrancaplanos.item AS principio,
        'Equipamentos' AS apresentacao,
        cobrancaplanos.id as cod_item
      FROM
        orcamento_simulacao_item
        INNER JOIN cobrancaplanos ON orcamento_simulacao_item.cobrancaplano_id = cobrancaplanos.id
      WHERE
        orcamento_simulacao_item.simulacao_id = '{$simulacaoId}'
        AND orcamento_simulacao_item.tipo = '5'
        AND orcamento_simulacao_item.tabela_origem = 'cobrancaplano'
      
      UNION
        SELECT 
          orcamento_simulacao_item.*,
          orcamento_simulacao_item.id AS simItemId,
          valorescobranca.DESC_COBRANCA_PLANO AS principio,
          'Equipamentos' AS apresentacao,
          valorescobranca.id as cod_item
        FROM
          orcamento_simulacao_item
          INNER JOIN valorescobranca ON orcamento_simulacao_item.valorescobranca_id = valorescobranca.id
        WHERE
          orcamento_simulacao_item.simulacao_id = '{$simulacaoId}'
          AND orcamento_simulacao_item.tipo = '5'
          AND orcamento_simulacao_item.tabela_origem = 'valorescobranca'

        UNION
          SELECT 
            orcamento_simulacao_item.*,
            orcamento_simulacao_item.id AS simItemId,
            cobrancaplanos.item AS principio,
            'Gasoterapia' AS apresentacao,
            cobrancaplanos.id as cod_item
          FROM
            orcamento_simulacao_item
            INNER JOIN cobrancaplanos ON orcamento_simulacao_item.cobrancaplano_id = cobrancaplanos.id
          WHERE
            orcamento_simulacao_item.simulacao_id = '{$simulacaoId}'
            AND orcamento_simulacao_item.tipo = '6'
            AND orcamento_simulacao_item.tabela_origem = 'cobrancaplano'

          UNION
            SELECT 
              orcamento_simulacao_item.*,
              orcamento_simulacao_item.id AS simItemId,
              valorescobranca.DESC_COBRANCA_PLANO AS principio,
              'Gasoterapia' AS apresentacao,
              valorescobranca.id as cod_item
            FROM
              orcamento_simulacao_item
              INNER JOIN valorescobranca ON orcamento_simulacao_item.valorescobranca_id = valorescobranca.id
            WHERE
              orcamento_simulacao_item.simulacao_id = '{$simulacaoId}'
              AND orcamento_simulacao_item.tipo = '6'
              AND orcamento_simulacao_item.tabela_origem = 'valorescobranca'
ORDER BY 
  simItemId
SQL;
}

function visualizar_simulacao($simulacaoId)
{
    $sql = <<<SQL
SELECT 
  orcamento_simulacao.*
FROM
  orcamento_simulacao
WHERE
  id = '{$simulacaoId}'
SQL;
    $rs = mysql_query($sql);
    $row = mysql_fetch_array($rs);
    $fatura = $row['fatura'];

    list($html, $dadosPaciente) = cabecalhoFatura($fatura);

    $inicio = \DateTime::createFromFormat('Y-m-d', $row['inicio'])->format('d/m/Y');
    $fim = \DateTime::createFromFormat('Y-m-d', $row['fim'])->format('d/m/Y');

    $html = "    <b>De {$inicio} à {$fim}</b>
                 <br><br>
				  <table  width=100% class='mytable'>
						  <thead>
						<tr>		     	
							<th style='font-size:12px;'>Item</th>
							<th style='font-size:12px'>Qtd</th>
							<th style='font-size:12px'>Unidade</th>
							<th style='font-size:12px;'>Pre&ccedil;o Unt.</th>
							<th style='font-size:12px;'>Custo Unit.</th>
							<th style='font-size:12px;'>Taxa Tot.</th>
							<th style='font-size:12px;'>Margem do Item</th>							                           
						</tr>
				   </thead>";

    $sql = sqlSimulacaoVisualizar($simulacaoId);
    $result = mysql_query($sql);

    $valorTotal = 0;
    $custoTotal = 0;
    $i = 1;
    while($row = mysql_fetch_array($result)){
        if($i % 2 == 0){
            $bgColor = 'white';
        } else {
            $bgColor = '#dcdcdc';
        }
        $quantidade = $row['qtd'];
        $custoItem = $row['custo'];
        $desconto_acrescimo = ($quantidade*$row['preco'])*($row['acrescimo_desconto'] /100);
        $valorTotal += ($quantidade * $row['preco'])+$desconto_acrescimo;
        $custoTotal += $quantidade * $custoItem;
        $margemItem = 0.00;
        if($quantidade == 0){
            $margemItem = 0;
        }elseif($row['preco'] > 0) {
            $margemItem = (((($row['preco'] - $custoItem)*$quantidade)+ $desconto_acrescimo) / ($row['preco']*$quantidade)) * 100;
        }
        $corItem = "green";
        if($margemItem < 25) {
            $corItem = "red";
        }

        $html .= "<tr bgcolor='{$bgColor}'>
					<td width='50%'>{$row['principio']} </td>
					<td>{$row['qtd']}</td>
					<td>{$row['unidade']}</td>
					<td>R$ " . number_format($row['preco'], 2, ',', '.') . "</td>
					<td>R$ " . number_format($custoItem, 2, ',', '.') . "</td>
					<td>R$ " . number_format($desconto_acrescimo, 2, ',', '.') . "</td>
					<td style='color: {$corItem}'>" . number_format($margemItem, 2, ',', '.') . " %</td>
				  </tr>";
        $i++;
    }
    $pissCofins = 4.95;
    $ir = 3.5;
    $sqlIssFatura =  "select imposto_iss from fatura where id = {$fatura}";
    $resultIssFatura = mysql_query($sqlIssFatura);
    $issFatura = mysql_result($resultIssFatura,0);
    $issFatura = empty($issFatura) ? 0 : $issFatura;

    $custoTotal = ($valorTotal * ($pissCofins + $ir + $issFatura) / 100) + $custoTotal;
    $lucroReais = $valorTotal - $custoTotal;
    $lucroPorcento = ($valorTotal - $custoTotal) / $valorTotal;
    $lucroPorcento *= 100;

    if($valorTotal >= 4000) {
        $alerta = "Orçamento aprovado com ressalva(s)";
        $corAlerta = "orange";
        if ($lucroPorcento < 25 && $lucroReais < 1000) {
            $alerta = "Orçamento reprovado";
            $corAlerta = "red";
        } elseif ($lucroPorcento >= 25 && $lucroReais >= 1000) {
            $alerta = "Orçamento aprovado";
            $corAlerta = "green";
        }
    } elseif($valorTotal > 1000 && $valorTotal < 4000) {
        if ($lucroPorcento < 25) {
            $alerta = "Orçamento reprovado";
            $corAlerta = "red";
        } elseif ($lucroPorcento >= 25) {
            $alerta = "Orçamento aprovado";
            $corAlerta = "green";
        }
    } elseif($valorTotal <= 1000) {
        if ($lucroPorcento >= 15) {
            $alerta = "Orçamento aprovado";
            $corAlerta = "green";
        } elseif ($lucroPorcento < 15) {
            $alerta = "Orçamento reprovado";
            $corAlerta = "red";
        }
    }

    $html .= "</table>
              <br>
                <table class='mytable' width=100% >
                <thead>
                <tr>
					<th colspan='2' align='center' style='font-size: 10px !important;'>
						Valores dos encargos adiconados ao custo total: PISCOFINS {$pissCofins} %, IR {$ir} % mais o ISS informado acima.
						<br><b>OBS:</b>
						O valor total da fatura leva em consideração os descontos ou acréscimos.
					</th>
				</tr>
				</thead>
				<tr>
					<td>
						<b> Valor Total Faturado:</b>
					</td>
					<td>
						R$ <span id='valor-total'>".number_format($valorTotal,2,',','')."</span>
					</td>
				</tr>
				 <tr>
					<td>
						<b>Custo Total + Encargos:</b>
					</td>
					<td>
						 R$ <span id='custo-encargo'> ".number_format($custoTotal,2,',','')."</span>
					</td>
				</tr>
				
				<tr>
					<td>
						<b>Margem:</b>
					</td>
					<td>
						 R$ <span id='margem'>".number_format($lucroReais,2,',','')."</span>
					</td>
				</tr>
				<tr>
					<td>
						<b>Margem Percentual &cong;</b>
					</td>
					<td>
						 <span id='margem-percentual'>".number_format($lucroPorcento,2,',','')."</span>%
					</td>
				</tr>
				<tr>
					<td colspan='2' id='alerta-margem' align='center' bgcolor='{$corAlerta}'>
						 <b>{$alerta}</b>
					</td>
				</tr>
                </table>
			 </form>";
    echo $html;
}

function salvar_simulacao($data)
{
    $done = true;
    mysql_query("BEGIN");
    $inicio = $data['inicio_simulacao'];
    $fim = $data['fim_simulacao'];
    $paciente = $data['paciente'];
    $idFatura = $data['id-fatura'];
    $usuario = $_SESSION['id_user'];

    $pis_cofins = $data['pis_cofins'];
    $ir = $data['ir'];
    $iss = $data['iss'];

    $sql = <<<SQL
INSERT INTO orcamento_simulacao(id, fatura, inicio, fim, created_at, created_by)
VALUES (NULL, '{$idFatura}', '{$inicio}', '{$fim}', NOW(), '{$usuario}');
SQL;
    $rs = mysql_query($sql);
    $idSimulacao = mysql_insert_id();
    if(!$rs) {
        $done = false;
        mysql_query("ROLLBACK");
    }else {
        $sql = <<<SQL
INSERT INTO orcamento_simulacao_item (
    id,
    simulacao_id,
    catalogo_id,    
    cobrancaplano_id,
    valorescobranca_id,
    paciente_id,
    tipo,
    usuario_id,
    data,
    qtd,
    preco,
    custo,
    custo_anterior,
    acrescimo_desconto,
    unidade,
    tabela_origem
) VALUES 
SQL;
        foreach($data['qtd'] as $key => $value){
            $splitKey = explode('-', $key);
            $tipo = $splitKey[0];
            $codItem = $splitKey[1];
            $custo = $data['valor_custo'][$key];
            $acrescimoDesconto = $data['acrescimo_desconto'][$key];
            $tabelaOrigem = $data['tabela_origem'][$key];
            $unidade = $data['unidade'][$key];
            $custoAnterior = $data['custo_item'][$key];
            $preco = str_replace(',', '.', str_replace('.', '', $data['preco'][$key]));

            $comp = "'{$codItem}', '', ''";

            if('cobrancaplano' === $tabelaOrigem) {
                $comp = "'', '{$codItem}', ''";
            } elseif('valorescobranca' === $tabelaOrigem) {
                $comp = "'', '', '{$codItem}'";
            }

            $sql .= "(NULL, '{$idSimulacao}', {$comp}, '{$paciente}', '{$tipo}', '{$usuario}', NOW(), '{$value}', '{$preco}', '{$custo}', '{$custoAnterior}', '{$acrescimoDesconto}', '{$unidade}', '{$tabelaOrigem}'),";
        }

        $sql = substr_replace($sql, ';', -1, 1);
        $rs = mysql_query($sql);
        if(!$rs) {
            $done = false;
            mysql_query("ROLLBACK");
        }
    }
    mysql_query("COMMIT");
    $type = 's';
    $msg = base64_encode('Simulação salva com sucesso!');
    if(!$done){
        $type = 'f';
        $msg = base64_encode("Houve um problema ao salvar a simulação!");
    }
    header ("Location: /faturamento/?view=faturaravaliacao&msg={$msg}&type={$type}");
}

function faturar_av($cod,$nome,$op_avaliacao,$inicio,$fim)
{

    $inicio = join("-",array_reverse(explode("/",$inicio)));
    $fim = join("-",array_reverse(explode("/",$fim)));

    if($op_avaliacao == 1)
    {
        $sql = "select MAX(id) from capmedica where paciente = {$cod}";
        $result = mysql_query($sql);
        $x = mysql_result($result, 0);
        $y = mysql_num_rows($result);

        $compAvaMedSol = "";
        $compAvaMedPresc = "";
        $compAvaMedEquipAtivo = "";
        if($y > 0) {
            $compAvaMedSol = " AND ID_CAPMEDICA = '{$x}' ";
            $compAvaMedPresc = " and P.ID_CAPMEDICA = '{$x}' ";
            $compAvaMedEquipAtivo = " AND E.CAPMEDICA_ID = '{$x}' ";
        }
        ///////////jeferson marques verificar qual � a maior prescri��o de avalia��o do paciente para a capmedica 2013-08-13
        $sql_prescricao = "select MAX(id) from prescricoes where paciente = {$cod} and Carater=4 and ID_CAPMEDICA='{$x}' ";
        $result_prescricao = mysql_query($sql_prescricao);
        $maior_presc = mysql_result($result_prescricao, 0);
        $y_prescricao = mysql_num_rows($result_prescricao);

        $sql_presc_enfermagem = "select max(id) from prescricao_enf
                                  where (( inicio BETWEEN '$inicio'
														AND '$fim')
														OR (fim BETWEEN '$inicio'
														AND '$fim') )and cliente_id = $cod and carater=10";
        $result_prescricao_enf = mysql_query($sql_presc_enfermagem);
        $maior_presc_enf = mysql_result($result_prescricao_enf, 0);

        $sql_presc_enf_curativo = "select max(id) from prescricao_curativo
                                  where (( inicio BETWEEN '$inicio'
														AND '$fim')
														OR (fim BETWEEN '$inicio'
														AND '$fim') ) and paciente = $cod and carater=10";
        $result_presc_enf_curativo = mysql_query($sql_presc_enf_curativo);
        $maior_presc_enf_curativo = mysql_result($result_presc_enf_curativo, 0);



        $sql = "SELECT
                                IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),B.DESC_MATERIAIS_FATURAR) AS principio,
                                S.NUMERO_TISS AS cod,
                                S.CATALOGO_ID as catalogo_id,
                                'catalogo' as TABELA_ORIGEM,
                                C.nome,
                                S.tipo,
                                'P' as flag,
                                S.idSolicitacoes,
                                C.idClientes AS Paciente,
                                C.convenio AS Plano,
                                coalesce(B.valorMedio,'0.00') AS custo,
                                (CASE S.tipo WHEN '0' THEN 'Medicamento' WHEN '1' THEN 'Material' WHEN '3' THEN 'Dieta' END) as apresentacao,
                                (CASE WHEN COUNT(*)>1 THEN SUM(S.qtd) ELSE S.qtd END) AS quantidade,
                                coalesce(B.VALOR_VENDA,'0.00') AS valor,
                                pr.inicio,
                                pr.fim,
                                PL.nome as nomeplano,
                                 C.empresa,
                                 B.TUSS as cadTuss,
                                 PL.tipo_medicamento
                        FROM
                             `solicitacoes` S INNER JOIN
                             `clientes` C ON (S.paciente = C.idClientes) INNER JOIN
                             `catalogo` B ON (S.CATALOGO_ID = B.ID) INNER JOIN
                             `prescricoes` pr on (pr.ID = S.idPrescricao) INNER JOIN
                              planosdesaude as PL on (C.convenio=PL.id)
                        WHERE
                              C.idClientes = '{$cod}' AND
                              S.qtd>0 AND
                              C.idClientes = S.paciente AND
                              S.tipo IN (0,1,3) AND
                              pr.id = S.idPrescricao 
                              {$compAvaMedSol} AND 
                              pr.Carater in (4,7)  AND 
                              pr.DATA_DESATIVADA is null AND 
                              pr.STATUS <> 1
                        GROUP BY
                                Paciente,
                                S.catalogo_id
          UNION
                        SELECT
                               IF(BC.DESC_COBRANCA_PLANO IS NULL,B.item,BC.DESC_COBRANCA_PLANO)AS principio,
                                 IF(BC.DESC_COBRANCA_PLANO IS NULL,B.id,BC.COD_COBRANCA_PLANO) AS cod,
			         IF(BC.DESC_COBRANCA_PLANO IS NULL,B.id,BC.id) AS catalogo_id,
			         IF(BC.DESC_COBRANCA_PLANO IS NULL,'cobrancaplano','valorescobranca') AS TABELA_ORIGEM,
                                C.nome,
                                S.tipo,
                                'S' as flag,
                                S.idPrescricao AS idSolicitacoes,
                                C.idClientes AS Paciente,
                                C.convenio AS Plano,
                               coalesce(CS.CUSTO,'0.00') AS custo,
                                ('Serviços ') AS apresentacao,
                               coalesce( (
                                    SUM(CASE WHEN ESP.id IN(2,3,4,5,28,29,30) THEN DATEDIFF(S.fim,S.inicio)+1
                                        WHEN S.frequencia IN(2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19) THEN
                                            (DATEDIFF(S.fim,S.inicio)+1)*F.qtd
                                     END)
                                ),'0') AS quantidade,
                               coalesce(BC.valor,'0.00') AS valor,
                                P.inicio,
                                P.fim,
                                PL.nome as nomeplano,
                                 C.empresa,
                                 '' as cadTuss,
                                 PL.tipo_medicamento
                       FROM
                                `itens_prescricao` S INNER JOIN
                                `prescricoes` P ON (S.`idPrescricao`=P.id) INNER JOIN
                                `frequencia` as F ON(S.frequencia = F.id) INNER JOIN
                                `clientes` C ON (P.paciente = C.idClientes) INNER JOIN
                                `cuidadosespeciais` AS ESP ON (S.CATALOGO_ID = ESP.id) INNER JOIN
                                `cobrancaplanos` B ON (ESP.COBRANCAPLANO_ID =B.id) LEFT JOIN
                                custo_servicos_plano_ur CS ON (CS.COBRANCAPLANO_ID =B.id  and CS.UR=C.empresa) LEFT JOIN
                                `valorescobranca` BC ON (B.id=BC.idCobranca AND BC.idPlano = C.convenio AND BC.excluido_por is NULL) INNER JOIN
                                 planosdesaude as PL on (C.convenio=PL.id)
                        WHERE
                                C.idClientes = '{$cod}'
                                AND S.tipo IN (2)
                                AND P.Carater in (4,7)
                                {$compAvaMedPresc}                                
                                AND P.Carater in (4,7) 
                                AND  P.DATA_DESATIVADA is null
                                AND P.STATUS <> 1
                        GROUP BY
                                B.id,BC.id
        UNION
                        SELECT DISTINCT
                                 IF(V.DESC_COBRANCA_PLANO IS NULL,CO.item,V.DESC_COBRANCA_PLANO)AS principio,
                                 IF(V.DESC_COBRANCA_PLANO IS NULL,CO.id,V.COD_COBRANCA_PLANO) AS cod,
			         IF(V.DESC_COBRANCA_PLANO IS NULL,CO.id,V.id) AS catalogo_id,
			         IF(V.DESC_COBRANCA_PLANO IS NULL,'cobrancaplano','valorescobranca') AS TABELA_ORIGEM,
                                C.nome,
                                5 AS tipo,
                                'N' AS flag,
                                0 AS idSolicitacoes,
                                C.idClientes AS Paciente,
                                C.convenio AS Plano,
                                coalesce(CS.CUSTO,'0.00') AS custo,
                                ('Equipamentos') AS apresentacao,
                                (DATEDIFF(E.DATA_FIM,E.DATA_INICIO)+1) AS quantidade,
                                coalesce(V.valor,'0.00') AS valor,
                                P.inicio,
                                P.fim,
                                PL.nome as nomeplano,
                                C.empresa,
                                 '' as cadTuss,
                                 PL.tipo_medicamento
                          FROM
                                clientes C INNER JOIN
                                equipamentosativos E ON (C.idClientes = E.`PACIENTE_ID`) INNER JOIN
                                cobrancaplanos CO ON (E.`COBRANCA_PLANOS_ID` = CO.`id`) LEFT JOIN
                                custo_servicos_plano_ur CS ON (CS.COBRANCAPLANO_ID =CO.id  and CS.UR=C.empresa) LEFT JOIN
                                valorescobranca V ON (CO.`id` = V.`idCobranca` AND C.`convenio` = V.`idPlano` AND V.excluido_por is NULL) INNER JOIN
                                prescricoes as P ON (E.PRESCRICAO_ID=P.id) INNER JOIN
                                planosdesaude as PL on (C.convenio=PL.id)
                          WHERE
                                C.idClientes = '{$cod}' AND
                                E.PRESCRICAO_AVALIACAO = 's' 
                                {$compAvaMedEquipAtivo}
                          GROUP BY
                                CO.id,V.id
              UNION Select
IF (
	BC.DESC_COBRANCA_PLANO IS NULL,
	B.item,
	BC.DESC_COBRANCA_PLANO
) AS principio,

IF (
	BC.DESC_COBRANCA_PLANO IS NULL,
	B.id,
	BC.COD_COBRANCA_PLANO
) AS cod,

IF (
	BC.DESC_COBRANCA_PLANO IS NULL,
	B.id,
	BC.id
) AS catalogo_id,

IF (
	BC.DESC_COBRANCA_PLANO IS NULL,
	'cobrancaplano',
	'valorescobranca'
) AS TABELA_ORIGEM,
 C.nome,
 2 as tipo,
 'S' AS flag,
 S.prescricao_enf_id AS idSolicitacoes,
 C.idClientes AS Paciente,
 1 AS Plano,
  '0.00' AS custo,
 ('Servi&ccedil;os') AS apresentacao,
 (
   SUM(
                                        CASE
                                        WHEN ESP.id IN (2, 3, 4, 5) THEN

                                                (DATEDIFF(S.fim, S.inicio) + 1)
                                        ELSE
                                        IF (
                                                S.frequencia_id IN (
                                                        2,
                                                        3,
                                                        4,
                                                        5,
                                                        6,
                                                        7,
                                                        8,
                                                        9,
                                                        10,
                                                        11,
                                                        12,
                                                        13,
                                                        14,
                                                        15,
                                                        16,
                                                        17,
                                                        18,
                                                        19
                                                ),
                                                (DATEDIFF(S.fim, S.inicio) + 1) * F.qtd,
                                                CEILING(
                                                        (DATEDIFF(S.fim, S.inicio) + 1) / 7
                                                ) * F.SEMANA
                                        )
                                        END


	)
) AS quantidade,
 COALESCE (BC.valor, '0.00') AS valor,
 P.inicio,
 P.fim,
 PL.nome AS nomeplano,
 C.empresa,
 '' AS codTuss,
                                 PL.tipo_medicamento
FROM
	`prescricao_enf_itens_prescritos` AS S
LEFT JOIN `frequencia` AS F ON (S.frequencia_id = F.id)
INNER JOIN prescricao_enf P ON (S.prescricao_enf_id = P.id)
INNER JOIN `clientes` C ON (P.cliente_id = C.idClientes)
INNER JOIN prescricao_enf_cuidado AS ESP ON (S.presc_enf_cuidados_id = ESP.id)
INNER JOIN cuidadosespeciais as CUI on ESP.cuidado_especiais_id = CUI.id
INNER JOIN `cobrancaplanos` B ON (CUI.COBRANCAPLANO_ID = B.id)
LEFT JOIN `valorescobranca` BC ON (
		B.id = BC.idCobranca
		AND BC.idPlano = C.convenio
		AND BC.excluido_por IS NULL
)
INNER JOIN planosdesaude AS PL ON (C.convenio = PL.id)
WHERE
  ESP.presc_enf_grupo_id = 7
  AND ESP.cuidado_especiais_id NOT IN (17, 18, 21, 24)
	AND P.id = '{$maior_presc_enf}'


GROUP BY

	B.id,
	BC.id
	UNION
	SELECT
                                IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),B.DESC_MATERIAIS_FATURAR) AS principio,
                                S.NUMERO_TISS AS cod,
                                S.CATALOGO_ID as catalogo_id,
                                'catalogo' as TABELA_ORIGEM,
                                C.nome,
                                S.tipo,
                                'P' as flag,
                                S.idSolicitacoes,
                                C.idClientes AS Paciente,
                                C.convenio AS Plano,
                                coalesce(B.valorMedio,'0.00') AS custo,
                                (CASE S.tipo WHEN '0' THEN 'Medicamento' WHEN '1' THEN 'Material' END) as apresentacao,
                                (CASE WHEN COUNT(*)>1 THEN SUM(S.qtd) ELSE S.qtd END) AS quantidade,
                                coalesce(B.VALOR_VENDA,'0.00') AS valor,
                                pr.inicio,
                                pr.fim,
                                PL.nome as nomeplano,
                                 C.empresa,
                                 B.TUSS as cadTuss,
                                 PL.tipo_medicamento
                        FROM
                             `solicitacoes` S INNER JOIN
                             `clientes` C ON (S.paciente = C.idClientes) INNER JOIN
                             `catalogo` B ON (S.CATALOGO_ID = B.ID) INNER JOIN
                             `prescricao_curativo` pr on (pr.id = S.id_prescricao_curativo) INNER JOIN
                              planosdesaude as PL on (C.convenio=PL.id)
                        WHERE
                              pr.id = '{$maior_presc_enf_curativo}' AND
                              S.qtd>0 AND
                              C.idClientes = S.paciente AND
                              S.tipo IN (0,1) AND
                              pr.carater in (10)
                        GROUP BY
                                Paciente,
                                S.catalogo_id
                          ORDER BY
                                Paciente,
                                tipo,
                                principio";

        echo formulario();
        $result = mysql_query($sql);
        $resultado = mysql_num_rows($result);

        if($resultado != 0) {
            echo "</br>
              <table width=100% style='border:2px dashed;'>
                    <tr><td><center><b>OBSERVA&Ccedil;&Otilde;ES</b></center></td></tr>
                    <tr><td>Na coluna TAXA deve se colocar o percentual se for desconto colocar como negativo ex: -1,00. Caso seja acrescimo positivo ex: 1,00 </td></tr>
                    <tr><td>Na coluna deve-se marcar os itens que não vão ser cobrados porque fazem parte do Pacote</td></tr>
              </table>
              </br>";
            echo "<center><h2>{$label_tipo}</h2></center><br/>";
            $total=0.00;
            $cont=0;
            $nome_titulo='';
            $subtotal=0.00;
            $cod_ident = 0;

            while($row = mysql_fetch_array($result)){

                ////////Verifica se o plano é petrobras.
                if($plano == 80){
                    $checked="checked='checked'";
                }else{
                    $checked="";
                }

                $total +=$row['quantidade']*$row['valor'];
                $plano = $row['Plano'];
                $empresa=$row['empresa'];


                if($nome_titulo != $row['nome'])
                {

                    $subtotal=0.00;
                    $subtotal_equipamento=0.00;
                    $subtotal_material=0.00;
                    $subtotal_medicamento=0.00;
                    $subtotal_dieta=0.00;
                    $subtotal_servico=0.00;

                    $cod_ident =$row['Paciente'];
                    echo"<div id ='div_{$cod_ident}' >";
                    echo "<button id='{$cod_ident}' plano='{$plano}'  empresa='{$empresa}' nomeplano = '{$row['nomeplano']}' tipo_medicamento = '{$row['tipo_medicamento']}' tipo-documento ='1' contador=0 style='float:left;' inicio='{$inicio}'  fim='{$fim}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover add-item-fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>+</span></button>";
                    echo "<br/><table class='mytable' width=100%  >";
                    echo "<a><thead class='mostrar' idpaciente='{$cod_ident}'><tr><th colspan='5' style='background-color:#ccc;'><center width='70%' style='vertical-align:middle;'><b>".$row['nome']." ( ".$row['nomeplano'].")</b></center></th></tr></thead></a>";
                    echo"</table><table  width=100% class='ocultar mytable' id='tabela_{$cod_ident}'>";
                    echo "<tr>
                                                <th>Item</th>
                                                <th>Apresenta&ccedil;&atilde;o</th>
                                                <th>Unidade</th>
                                                <th>Qtd</th>
                                                <th>Pre&ccedil;o Unit.</th>
                                                <th>Pre&ccedil;o Total</th>
                                                <th>Taxa</th>
		    	                        <th>Pacote</th>

                                                <!--<th>Custo</th>-->
                                                </tr>
                          <tr>
                            <td colspan='4'>
                                <b><input type='checkbox' class='selecionar-todos' paciente='{$cod_ident}'>Selecionar todos para serem removidos</b>
                            </td>
                            <td colspan='4' align='right' style='padding-right: 25px;'>
                                <b>Selecionar todos para pacote <input type='checkbox' class='selecionar-todos-pacote' paciente='{$cod_ident}'></b>
                            </td>
                        </tr>";
                    $nome_titulo = $row['nome'];

                }

                $subtotal +=$row['quantidade']*$row['valor'];
                if($row['tipo'] == '1')
                {
                    $subtotal_material +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '0')
                {
                    $subtotal_medicamento +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '2')
                {
                    $subtotal_servico +=$row['quantidade']*$row['valor'];
                }
                if($row['tipo'] == '5')
                {
                    $subtotal_equipamento +=$row['quantidade']*$row['valor'];
                }
                //$botoes = '<img align=\'right\' class=\'rem-item-solicitacao\'  src=\'../utils/delete_16x16.png\' title=\'Remover\' border=\'0\'>';
                //    if($nome_titulo!=$row['nome']){
                $botoes = "<br><input type='checkbox' class=' excluir-item excluir-item-paciente-{$cod_ident}'/> <b> Excluir</b>";



                $codigoitem =$row['cod'];
                $codTuss= $row['codTuss'];
                //////2014-01-16
                ///////// se for material coloca a descrição igual do Simpro que vai ser para fatura porque o principio é um ‘nome genérico’ que as enfermeiras usam.
                if($row['tipo'] == 1 && $row['segundonome']!= NULL){
                    $nomeitem =$row['segundonome'];
                }else{
                    $nomeitem = $row['principio'];
                }


                $tr = "<tr  class='itens_fatura {$cod_ident}' >
                                <td width='50%'>TUSS-{$codTuss} N-{$codigoitem}  {$nomeitem}{$botoes}</td>
                                <td>{$row['apresentacao']}</td>
                                <td>".unidades_fatura(NULL)."</td>
                                <td><input class='qtd' size='7' value='{$row['quantidade']}' paciente='{$row['Paciente']}'/></td>
                                <td>R$<input type='text' name='valor_plano' size='10' class='valor_plano valor OBG' tiss='{$row['cod']}' catalogo_id='{$row['catalogo_id']}' tabela_origem='{$row['TABELA_ORIGEM']}' qtd='{$row['quantidade']}' paciente='{$row['Paciente']}' solicitacao='{$row['idSolicitacoes']}' tipo='{$row['tipo']}' inicio='{$inicio}' fim='{$fim}' custo = '{$row['custo']}' usuario='{$_SESSION["id_user"]}' ord = {$cont} flag='{$row['flag']}' value='".number_format($row['valor'],2,',','.')."'></td>
                                <td><span style='float:right;' id='val_total{$cont}'><b>R$  <span nome='val_linha'>".number_format($row['valor']*$row['quantidade'],2,',','.')."</span></b></span></td>
                                <td><input type='text' name='taxa' size='10' class='taxa OBG' value='0,00'></td>
                                <td><input type='checkbox' $checked name='incluso_pacote' size='10' class='incluso_pacote incluso-pacote-{$cod_ident} OBG'></td>

                                </tr>";
                if($row['tipo'] == 0 ){
                    $trMedicamento .= $tr;
                }elseif($row['tipo'] == 3 ){
                    $trDieta .= $tr;
                }elseif($row['tipo'] == 1 ){
                    $trMaterial .= $tr;
                }elseif($row['tipo'] == 2 ){
                    $trServico .= $tr;
                }elseif($row['tipo'] == 5 ){
                    $trEquipamento .= $tr;
                }
                $cont++;

                //}

                $inicio=$row['inicio'];
                $fim=$row['fim'];

            }
            $divs = "<tbody id='tipo_0_paciente_{$cod_ident}'>
							{$trMedicamento}
						</tbody>
						<tbody id='tipo_3_paciente_{$cod_ident}'>
										{$trDieta}
						</tbody>
						<tbody id='tipo_1_paciente_{$cod_ident}'>
						{$trMaterial}

						</tbody>
						<tbody id='tipo_2_paciente_{$cod_ident}'>
						{$trServico}

						</tbody>
						<tbody id='tipo_5_paciente_{$cod_ident}'>
						{$trEquipamento}

						</tbody>
						<tbody id='tipo_6_paciente_{$cod_ident}'>

						</tbody>
						<tbody id='tipo_12_paciente_{$cod_ident}'>

						</tbody>";
            echo $divs;
            echo "</table>";
            echo "<button id='{$cod_ident}' contador=0 plano='{$plano}' inicio='{$inicio}' empresa='{$row['empresa']}' fim='{$fim}' style='float:left;'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover add-item-fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>+</span></button>";
            echo "<button  cod='{$cod_ident}'  plano='{$plano}' presc_av='0' nome='$nome_titulo' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover excluir-todos ' role='button' aria-disabled='false' ><span class='ui-button-text'>Excluir Itens</span></button>";

            echo "<button  cod='{$cod_ident}' id-fatura=0 id='finalizar-paciente-{$cod_ident}' plano='{$plano}' inicio='{$inicio}'  fim='{$fim}' presc_av='1' cap='{$x}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover faturar' role='button' aria-disabled='false' ><span class='ui-button-text'>Faturar</span></button>";
            echo "<button  cod='{$cod_ident}'  id-fatura=0 id='salvar-paciente-{$cod_ident}' empresa='{$empresa}' plano='{$plano}' nome='{$nome_titulo}' presc_av='1' cap='{$x}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover salvar_fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button>";

            echo "<table class='mytable' width=100% id='lista-itens'>";
            echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_quipamento_".$cod_ident."'><b>SUBTOTAL EQUIPAMENTO: R$</b> <b><i id='equipamento_".$cod_ident."'>".number_format($subtotal_equipamento,2,',','.')."</i></b></span></td></tr>";
            echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_servico_".$cod_ident."'><b>SUBTOTAL SERVI&Ccedil;O: R$</b> <b><i id='servico_".$cod_ident."'>".number_format($subtotal_servico,2,',','.')."</i></b></span></td></tr>";
            echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_material_".$cod_ident."'><b>SUBTOTAL MATERIAL: R$</b> <b><i id='material_".$cod_ident."'>".number_format($subtotal_material,2,',','.')."</i></b></span></td></tr>";
            echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_medicamento_".$cod_ident."'><b>SUBTOTAL MEDICAMENTO: R$</b> <b><i id='medicamento_".$cod_ident."'>".number_format($subtotal_medicamento,2,',','.')."</i></b></span></td></tr>";
            echo "<tr><td colspan='5' style='font-size:12px;'><span style='float:right;' class='sub_fatura_".$cod_ident."'><b>TOTAL PACIENTE: R$</b> <b><i>".number_format($subtotal,2,',','.')."</i></b></span></td></tr>";
            echo "<tr><td colspan='5' style='font-size:12px;'><span style='float:right;' cont='{$cont}' cont1='0' id='total_fatura'><b>TOTAL: R$</b> <b><i>".number_format($total,2,',','.')."</i></b></span></td></tr>";
            echo "<tr><td colspan='5' id='obs_".$cod_ident."'>OBSERVA&Ccedil;&Atilde;O::<textarea cols=120 rows=5 {$acesso} id='text_{$cod_ident}'></textarea></td></tr>";
            echo"</table></div>";

        }
        else {
            $sql = "SELECT
                        C.nome,
                        C.idClientes AS Paciente,
                        C.convenio AS Plano,
                        PL.nome as nomeplano,
                        C.empresa
                FROM
                     `clientes` C INNER JOIN
                      planosdesaude as PL on (C.convenio=PL.id)
                WHERE
                      C.idClientes = '{$cod}'";
            $result = mysql_query($sql);

            $nome_titulo='';
            $cod_ident = 0;
            while ($row = mysql_fetch_array($result)) {
                echo "</br>
                      <table width=100% style='border:2px dashed;'>
                            <tr><td><center><b>OBSERVA&Ccedil;&Otilde;ES</b></center></td></tr>
                            <tr><td>Na coluna TAXA deve se colocar o percentual se for desconto colocar como negativo ex: -1,00. Caso seja acrescimo positivo ex: 1,00 </td></tr>
                            <tr><td>Na coluna deve-se marcar os itens que não vão ser cobrados porque fazem parte do Pacote</td></tr>
                      </table>
                      </br>";
                $plano = $row['Plano'];
                $empresa = $row['empresa'];

                if ($plano == 80) {
                    $checked = "checked='checked'";
                } else {
                    $checked = "";
                }

                if ($nome_titulo != $row['nome']) {
                    $cod_ident = $row['Paciente'];
                    echo "<div id ='div_{$cod_ident}' >";
                    echo "<button id='{$cod_ident}' plano='{$plano}'  empresa='{$empresa}' nomeplano = '{$row['nomeplano']}' tipo_medicamento = '' tipo-documento ='1' contador=0 style='float:left;' inicio='{$inicio}'  fim='{$fim}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover add-item-fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>+</span></button>";
                    echo "<br/><table class='mytable' width=100%  >";
                    echo "<a><thead class='mostrar' idpaciente='{$cod_ident}'><tr><th colspan='5' style='background-color:#ccc;'><center width='70%' style='vertical-align:middle;'><b>" . $row['nome'] . " ( " . $row['nomeplano'] . ")</b></center></th></tr></thead></a>";
                    echo "</table><table  width=100% class='ocultar mytable' id='tabela_{$cod_ident}'>";
                    echo "<tr>
                            <th>Item</th>
                            <th>Apresenta&ccedil;&atilde;o</th>
                            <th>Unidade</th>
                            <th>Qtd</th>
                            <th>Pre&ccedil;o Unit.</th>
                            <th>Pre&ccedil;o Total</th>
                            <th>Taxa</th>
                            <th>Pacote</th>
                          </tr>
                          <tr>
                            <td colspan='4'>
                                <b><input type='checkbox' class='selecionar-todos' paciente='{$cod_ident}'>Selecionar todos para serem removidos</b>
                            </td>
                            <td colspan='4' align='right' style='padding-right: 25px;'>
                                <b>Selecionar todos para pacote <input type='checkbox' class='selecionar-todos-pacote' paciente='{$cod_ident}'></b>
                            </td>
                        </tr>";
                    $nome_titulo = $row['nome'];

                }
                $botoes = "<br><input type='checkbox' class=' excluir-item excluir-item-paciente-{$cod_ident}'/> <b> Excluir</b>";

                $inicio = $row['inicio'];
                $fim = $row['fim'];

            }
            $divs = "<tbody id='tipo_0_paciente_{$cod_ident}'>
						</tbody>
						<tbody id='tipo_3_paciente_{$cod_ident}'>
						</tbody>
						<tbody id='tipo_1_paciente_{$cod_ident}'>
						</tbody>
						<tbody id='tipo_2_paciente_{$cod_ident}'>
						</tbody>
						<tbody id='tipo_5_paciente_{$cod_ident}'>
						</tbody>
						<tbody id='tipo_6_paciente_{$cod_ident}'>
						</tbody>
						<tbody id='tipo_12_paciente_{$cod_ident}'>
						</tbody>";
            echo $divs;
            echo "</table>";
            echo "<button id='{$cod_ident}' contador=0 plano='{$plano}' inicio='{$inicio}' empresa='{$row['empresa']}' fim='{$fim}' style='float:left;'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover add-item-fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>+</span></button>";
            echo "<button  cod='{$cod_ident}'  plano='{$plano}' presc_av='0' nome='$nome_titulo' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover excluir-todos ' role='button' aria-disabled='false' ><span class='ui-button-text'>Excluir Itens</span></button>";

            echo "<button  cod='{$cod_ident}' id-fatura=0 id='finalizar-paciente-{$cod_ident}' plano='{$plano}' inicio='{$inicio}'  fim='{$fim}' presc_av='1' cap='{$x}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover faturar' role='button' aria-disabled='false' ><span class='ui-button-text'>Faturar</span></button>";
            echo "<button  cod='{$cod_ident}'  id-fatura=0 id='salvar-paciente-{$cod_ident}' plano='{$plano}' nome='{$nome_titulo}' empresa='{$empresa}' presc_av='1' cap='' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover salvar_fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button>";

            echo "<table class='mytable' width=100% id='lista-itens'>";
            echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_quipamento_" . $cod_ident . "'><b>SUBTOTAL EQUIPAMENTO: R$</b> <b><i id='equipamento_" . $cod_ident . "'>" . number_format($subtotal_equipamento, 2, ',', '.') . "</i></b></span></td></tr>";
            echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_servico_" . $cod_ident . "'><b>SUBTOTAL SERVI&Ccedil;O: R$</b> <b><i id='servico_" . $cod_ident . "'>" . number_format($subtotal_servico, 2, ',', '.') . "</i></b></span></td></tr>";
            echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_material_" . $cod_ident . "'><b>SUBTOTAL MATERIAL: R$</b> <b><i id='material_" . $cod_ident . "'>" . number_format($subtotal_material, 2, ',', '.') . "</i></b></span></td></tr>";
            echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_medicamento_" . $cod_ident . "'><b>SUBTOTAL MEDICAMENTO: R$</b> <b><i id='medicamento_" . $cod_ident . "'>" . number_format($subtotal_medicamento, 2, ',', '.') . "</i></b></span></td></tr>";
            echo "<tr><td colspan='5' style='font-size:12px;'><span style='float:right;' class='sub_fatura_" . $cod_ident . "'><b>TOTAL PACIENTE: R$</b> <b><i>" . number_format($subtotal, 2, ',', '.') . "</i></b></span></td></tr>";
            echo "<tr><td colspan='5' style='font-size:12px;'><span style='float:right;' cont='{$cont}' cont1='0' id='total_fatura'><b>TOTAL: R$</b> <b><i>" . number_format($total, 2, ',', '.') . "</i></b></span></td></tr>";
            echo "<tr><td colspan='5' id='obs_" . $cod_ident . "'>OBSERVA&Ccedil;&Atilde;O::<textarea cols=120 rows=5 {$acesso} id='text_{$cod_ident}'></textarea></td></tr>";
            echo "</table></div>";
        }
        //echo"<center><b>Nenhum resultado foi encontrado!</b></center>";
    }else{
        echo createDialogSimulacaoCusto();
        $sql = "select MAX(id) from capmedica where paciente = {$cod}";
        $result = mysql_query($sql);
        $x = mysql_result($result, 0);

        $sql = "SELECT
		f.*,DATE_FORMAT(f.DATA,'%d/%m/%Y %H:%i:%s' ) as datah,
		c.nome
		FROM
		operadoras_planosdesaude as op Right Join
		clientes as c on (c.convenio = op.PLANOSDESAUDE_ID) RIGHT JOIN
		fatura as f on (c.idClientes = f.PACIENTE_ID)
		WHERE
		f.PACIENTE_ID = {$cod} AND f.ORCAMENTO = '1' and f.STATUS <> -1 and f.CANCELADO_POR is NULL order by ID desc";
        $result = mysql_query($sql);

        $resposta = mysql_num_rows($result);


        if($resposta > 0){
            formulario_simulacao();
            echo "<br><br>
            <table class='mytable' width=100% ><thead>
			<tr>
			<th>Paciente</th>
			<th>Data</th>
			<th>Visualizar</th>
			<th>Op&ccedil;&otilde;es</th>
			</tr>
			</thead>";
            while($row = mysql_fetch_array($result)){
                echo "<tr>
						<td>{$row['nome']}</td>
						<td>{$row['datah']}</td>
						<td>
							<a href='#'>
							<img src='../utils/fichas_24x24.png' width='16' width='20' title='Visualizar Orçamento' class='v_glosa_2' idfatura ='{$row['ID']}' empresa='{$row['UR']}' av='{$row{'ORCAMENTO'}}' >

							</a>
						</td>
						<td>
							<a href='#'>
								<img src='../utils/msg_24x24.png' width='16' lote={$row['CAPA_LOTE']} class='enviar-email-1' width='20' title='Enviar Or&ccedil;amento'>
							</a> &nbsp;&nbsp;
							<a href='?view=edt_orcamento&id={$row['ID']}&av=1'>
								<img src='../utils/edit.png' width='16' lote={$row['CAPA_LOTE']} class='editar_orcamento' width='20' title='Editar Or&ccedil;amento'>
							</a>&nbsp;&nbsp;
				<a href=?view=edt_orcamento&id={$row['ID']}&av={$row['ORCAMENTO']}&n=1><img src='../utils/prescricao_16x16.png' width='16' width='20' title='Usar para novo Orçamento.'  ></a>";
                if($row['ORCAMENTO'] == '1') {
                    echo "&nbsp;&nbsp;<img src = '../utils/simulacao_32x32.png' orcamento='{$row['ID']}' class='simular' tipo='novo' width = '20' title = 'Simular Custo' >";
                }
                echo "</td>
					  </tr>";
            }
            echo"</table>";
        }else{
            echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
        }

        $sql = <<<SQL
SELECT 
  orcamento_simulacao.*,
  clientes.nome AS paciente,
  usuarios.nome AS usuario
FROM
  orcamento_simulacao_item
  INNER JOIN orcamento_simulacao ON orcamento_simulacao_item.simulacao_id = orcamento_simulacao.id
  INNER JOIN clientes ON orcamento_simulacao_item.paciente_id = clientes.idClientes
  INNER JOIN usuarios ON orcamento_simulacao.created_by = usuarios.idUsuarios
WHERE
  paciente_id = '{$cod}'
GROUP BY
  simulacao_id
ORDER BY
  simulacao_id DESC
LIMIT 5
SQL;

        $rs = mysql_query($sql);
        $numRows = mysql_num_rows($rs);
        echo "<br><center><h4><b>ÚLTIMAS 5 SIMULAÇÕES</b></h4></center>";
        echo "<table class='mytable' width=100% ><thead>
			<tr>
			<th>Paciente</th>
			<th>Usu&aacute;rio</th>
			<th>Data</th>
			<th>Per&iacute;odo</th>
			<th>Op&ccedil;&otilde;es</th>
			</tr>
			</thead>";
        if($numRows > 0){

            while($row = mysql_fetch_array($rs, MYSQL_ASSOC)){
                $dataSistema = \DateTime::createFromFormat('Y-m-d H:i:s', $row['created_at'])->format('d/m/Y à\s H:i:s');
                $inicio = \DateTime::createFromFormat('Y-m-d', $row['inicio'])->format('d/m/Y');
                $fim = \DateTime::createFromFormat('Y-m-d', $row['fim'])->format('d/m/Y');
                echo "<tr>
						<td>{$row['paciente']}</td>
						<td>{$row['usuario']}</td>
						<td>{$dataSistema}</td>
						<td>{$inicio} &agrave; {$fim}</td>
						<td>
							<img src='../utils/look_24x24.png' width='16' width='20' title='Visualizar Simulação' class='simular' orcamento='{$row['id']}' tipo='visualizar' >
							<a target='_blank' href='imprimir_simulacao.php?paciente={$cod}&simulacao={$row['id']}&inicio={$inicio}&fim={$fim}'>
								<img src='../utils/pdf_24x24.png' width='16' title='Imprimir Simulação'>
							</a>
						</td>
                    </tr>";
            }
        }else{
            echo "<tr><td align='center' colspan='5'><b>Nenhuma simulação foi encontrada!</b></td></tr>";
        }
        echo"</table>";
    }
}

function buscar_saida($post){
    extract($post,EXTR_OVERWRITE);
    $aSelect = ""; $aGroupBy = "";
    echo "<center><h1>Resultado</h1></center>";
    echo "<table class='mytable' width=100% >";
    echo "<thead><tr>";
    echo "<th><b>Paciente</b></th>";
    if($resultado == 'a'){
        echo "<th><b>Data</b></th>";
        $aSelect = ", s.data";
        $aGroupBy = " s.data,";
    }
    echo "<th><b>Nome</b></th>";
    echo "<th><b>Segundo Nome</b></th>";
    echo "<th><b>Quantidade</b></th>";
    echo "</tr></thead>";
    $cor = false;
    $total = 0;
    $i = anti_injection(implode("-",array_reverse(explode("/",$inicio))),'literal');
    $f = anti_injection(implode("-",array_reverse(explode("/",$fim))),'literal');
    $cond = "s.idCliente = '$codigo'";
    if($codigo == "todos" ) $cond = "1";
    $sql = "SELECT p.nome as paciente, s.nome, s.segundoNome, sum(s.quantidade) as quantidade $aSelect FROM saida as s, clientes as p WHERE p.idClientes = s.idCliente AND $cond AND s.data BETWEEN '$i' AND '$f' GROUP BY $aGroupBy s.segundoNome, s.nome, p.nome ORDER BY p.nome, s.nome, s.segundoNome ASC";
    if($tipo == 'med' || $tipo == 'mat'){
        $nomes = explode("#",$codigo);
        $nomes[0] = anti_injection($nomes[0],'literal');
        $nomes[1] = anti_injection($nomes[1],'literal');
        $sql = "SELECT p.nome as paciente, s.nome, s.segundoNome, sum(s.quantidade) as quantidade $aSelect FROM saida as s, clientes as p WHERE p.idClientes = idCliente AND s.nome = '{$nomes[0]}' AND s.segundoNome = '{$nomes[1]}' AND s.data BETWEEN '$i' AND '$f' GROUP BY $aGroupBy s.segundoNome, s.nome, p.nome ORDER BY p.nome, s.nome, s.segundoNome ASC";
    }
    $result = mysql_query($sql) or trigger_error(mysql_error());
    $f = true;
    while($row = mysql_fetch_array($result)){
        $f = false;
        foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
        if($cor) echo "<tr>";
        else echo "<tr class='odd'>";
        $cor = !$cor;
        echo "<td valign='top'>{$row['paciente']}</td>";
        if($resultado == 'a'){
            $data = implode("/",array_reverse(explode("-",$row['data'])));
            echo "<td valign='top'>$data</td>";
        }
        echo "<td valign='top'>{$row['nome']}</td>";
        echo "<td valign='top'>{$row['segundoNome']}</td>";
        echo "<td valign='top'>{$row['quantidade']}</td>";
        echo "</tr>";
    }
    echo "</table>";
    if($f) echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
}


///////////atualiza pre�o
function brasindice($ur, $item, $tipo){

    if ($tipo == 4) {//Todos
        $sql = "SELECT lab_desc, principio, apresentacao, NUMERO_TISS, ativo, tipo, VALOR_VENDA, ID FROM catalogo WHERE ATIVO='A' and VALOR_VENDA = '0.00' order by tipo,principio";
    }

    if ($tipo == 0) {//Medicamento
        $sql = "SELECT lab_desc, principio, apresentacao, NUMERO_TISS, ativo, tipo, VALOR_VENDA, ID FROM catalogo WHERE tipo = 0 and principio like '%$item%'  and ATIVO='A' and VALOR_VENDA = '0.00' order by principio limit 10";
    }

    if ($tipo == 1) {//Material
        $sql = "SELECT lab_desc, principio, apresentacao, NUMERO_TISS, ativo, tipo, VALOR_VENDA, ID FROM catalogo WHERE tipo = 1 and principio like '%$item%' and ATIVO='A' and VALOR_VENDA = '0.00' order by principio limit 10";
    }

    if ($tipo == 3) {//Dieta
        $sql = "SELECT lab_desc, principio, apresentacao, NUMERO_TISS, ativo, VALOR_VENDA, ID FROM catalogo WHERE tipo = 3 and principio like '%$item%' and ATIVO='A' and VALOR_VENDA= '0.00' order by principio limit 10";
    }

    $resultado = mysql_query($sql);

    $resposta = mysql_num_rows($resultado);


    if($resposta > 0){

        //Para todos, exibe os tipos
        $resultado_consulta .= "<table id='res-b'  class='mytable' width=100% >
 			<thead>
 			<tr>
 			<th>Laboratorio</th>
 			<th>Principio</th>
 			<th>Apresentacao</th>
 			<th>TISS</th>
 			<th>Status</th>
 			<th>Tipo</th>
 			<th>Valor</th>
 			<th>Editar</th>
 			</tr>";
        $i=0;
        while($row = mysql_fetch_array($resultado)){
            if($i++%2==0)
                $cor = "#E9F4F8";
            else
                $cor = "";
            $resultado_consulta.="
 				<tr id='{$row['numero_tiss']}' bgcolor='{$cor}'>
 				<td>{$row['lab_desc']}</td>
 				<td>{$row['principio']}</td>
 				<td>{$row['apresentacao']}</td>
 				<td>{$row['numero_tiss']}</td>
 				<td>".(($row['ativo']=='A')?"Ativo":"Inativo")."</td>
 				<td>";
            if ($row['tipo']== 0) {
                $resultado_consulta.= "Medicamento";
            }
            if($row['tipo']== 1){
                $resultado_consulta.= "Material";
            }if($row['tipo']== 3){
                $resultado_consulta.= "Dieta";
            }
            $resultado_consulta.= "</td>
 			<td><input value='".number_format($row['VALOR_VENDA'],2,',','.')."' class='valor' tiss='{$row['NUMERO_TISS']}' catalogo_id='{$row['ID']}' tipo='{$row['tipo']}' /></td>
 			<td><a href='#'class='brasindice_editar'><img border='0' title='Editar' src='../utils/edit_16x16.png'></a></td>
 			</tr>";

        }

        $resultado_consulta .= "</table>";
    }else{
        $resultado_consulta .="<p id='res-b'>";
        $resultado_consulta .=  "<center><b>Nenhum resultado foi encontrado!</b></center></p>";

    }
    print_r($resultado_consulta);

}

//! Carrega uma fatura salva.
function load_fatura_salva($arq,$fatura) {
    if($fatura == 0){
        $dir = "salvas/fatura/";
        $titulo="FATURAS SALVA";

    }else if($fatura == 2){
        $dir = "salvas/orcamento-prorrogacao/";
        $titulo= "OR&Ccedil;AMENTO DE PRORROGAÇÂO SALVO";
    }else{
        $dir = "salvas/orcamento/";
        $titulo= "OR&Ccedil;AMENTO SALVO";
    }
    $filename = $dir . "/" . $arq . ".htm";
    $str = stripslashes(file_get_contents($filename));
    $string = explode('@|#', $str);
    echo formulario();
    echo "<div id='corpo-fatura'>";
    echo "<center><h1>{$titulo}</h1></center>";
    echo "<div id='div_".$arq."'>";
    echo $string[2];
    echo "</div>";
    echo "</div>$*|*$".$string[0]."$*|*$".$string[1];
}
///////////////////jeferson 2013-12-11 pesquisar itens do orçamento de  prorrogação
function itens_orcamento_prorrogacao($tipo,$cod,$inicio,$fim,$nome,$refaturar,$plano){

    $sql = "
    
    
    SELECT
                            concat(B.principio,' - ',B.apresentacao) as principio,
                             S.NUMERO_TISS AS cod,
                            S.CATALOGO_ID as catalogo_id,
                            '' as TABELA_ORIGEM,
                            C.nome,
                            S.tipo,
                            'P' as flag,
                            S.idSolicitacoes,
                            C.idClientes AS Paciente,
                             C.convenio AS Plano,

                            coalesce(B.valorMedio,'0.00') AS custo,
                            (CASE S.tipo
                            WHEN '0' THEN 'Medicamento'
                            WHEN '1' THEN 'Material'
                            WHEN '3' THEN 'Dieta' END) as apresentacao,
                            (CASE WHEN COUNT(*)>1 THEN SUM(S.qtd) ELSE S.qtd END) AS quantidade,
                           coalesce( B.VALOR_VENDA,'0.00')  AS valor,
                            C.empresa,
                            B.TUSS as codTuss,
                            PL.nome as nomeplano,
                            PL.tipo_medicamento

               FROM
                            `solicitacoes` S INNER JOIN
                            `clientes` C ON (S.paciente = C.idClientes)  INNER JOIN
                            `catalogo` B ON (S.CATALOGO_ID = B.ID) INNER JOIN
                             prescricoes P ON (S.`idPrescricao`=P.id) INNER JOIN
                             planosdesaude PL on  (C.convenio = PL.ID)

                WHERE

                            C.idClientes = '{$cod}' AND
                            (P.inicio BETWEEN '{$inicio}' AND '{$fim}' AND P.fim BETWEEN '{$inicio}' AND '{$fim}') AND
                             (C.idClientes = S.paciente) and S.`idPrescricao`=P.id
                             and S.tipo IN (0,1,3) and P.Carater not in (4,7)
               GROUP BY
                            Paciente,catalogo_id
               UNION
               SELECT
                            IF(BC.DESC_COBRANCA_PLANO IS NULL,B.item,BC.DESC_COBRANCA_PLANO)AS principio,
                            IF(BC.DESC_COBRANCA_PLANO IS NULL,B.id,BC.COD_COBRANCA_PLANO) AS cod,
                            IF(BC.DESC_COBRANCA_PLANO IS NULL,B.id,BC.id) AS catalogo_id,
                            IF(BC.DESC_COBRANCA_PLANO IS NULL,'cobrancaplano','valorescobranca') AS TABELA_ORIGEM,
                            C.nome,
                            2 as tipo,
                            'S' as flag,
                            S.idPrescricao AS idSolicitacoes,
                            C.idClientes AS Paciente,
                            C.convenio AS Plano,
                            coalesce(CS.CUSTO,'0.00') AS custo,
                            ('Servi&ccedil;os') AS apresentacao,
                           (SUM(CASE
                            WHEN ESP.id IN(2,3,4,5) THEN IF(('{$inicio}'<=S.inicio and '{$fim}'>=S.inicio) and ('{$inicio}'<=S.fim and '{$fim}'>=S.fim), (DATEDIFF(S.fim,S.inicio)+1), IF(('{$inicio}'<=S.inicio and '{$fim}'>=S.inicio) and ('{$fim}'<=S.fim),(DATEDIFF('{$fim}',S.inicio)+1),IF(('{$inicio}'>S.inicio) and ('{$fim}'>=S.fim and '{$inicio}'<=S.fim),(DATEDIFF(S.fim,'{$inicio}')+1),'1')))
                            ELSE IF(('{$inicio}'<=S.inicio and '{$fim}'>=S.inicio) and ('{$inicio}'<=S.fim and '{$fim}'>=S.fim), IF(S.frequencia IN(2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19),(DATEDIFF(S.fim,S.inicio)+1)*F.qtd,CEILING((DATEDIFF(S.fim,S.inicio)+1)/7)*F.SEMANA), IF(('{$inicio}'<=S.inicio and '{$fim}'>=S.inicio) and ('{$fim}'<=S.fim),IF(S.frequencia IN(2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19),(DATEDIFF('{$fim}',S.inicio)+1)*F.qtd,CEILING((DATEDIFF('{$fim}',S.inicio)/7)+1)*F.SEMANA),IF(('{$inicio}'>S.inicio) and ('{$fim}'>=S.fim and '{$inicio}'<=S.fim),IF(S.frequencia IN(2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19),DATEDIFF(S.fim,'{$inicio}')*F.qtd,CEILING((DATEDIFF(S.fim,'{$inicio}')/7)+1)*F.SEMANA),'1')))
                             END)) AS quantidade,
                            coalesce(BC.valor,'0.00') AS valor,
                            C.empresa,
                            '' as codTuss,
                            PL.nome as nomeplano,
                            PL.tipo_medicamento

               FROM
                            `itens_prescricao` S LEFT JOIN
                            `frequencia` as F ON (S.frequencia = F.id) INNER JOIN
                            prescricoes P ON (S.`idPrescricao`=P.id) INNER JOIN
                            `clientes` C ON (P.paciente = C.idClientes)  INNER JOIN
                            cuidadosespeciais AS ESP ON (S.CATALOGO_ID = ESP.id) INNER JOIN
                           `cobrancaplanos` B ON (ESP.COBRANCAPLANO_ID =B.id) LEFT JOIN
                            custo_servicos_plano_ur CS ON 
                            (CS.COBRANCAPLANO_ID = B.id AND CS.UR = C.empresa AND CS.STATUS = 'A') LEFT JOIN
                           `valorescobranca` BC ON (B.id = BC.idCobranca
                            AND BC.idPlano = C.convenio
                            AND BC.excluido_por IS NULL)
                          INNER JOIN
                            planosdesaude as PL on (C.convenio=PL.id)
               WHERE
                            C.idClientes = '{$cod}' AND
                            (P.inicio BETWEEN '{$inicio}' AND '{$fim}' AND P.fim BETWEEN '{$inicio}' AND '{$fim}') AND
                            S.tipo IN (2)	 AND P.Carater not in (4,7)  and S.CATALOGO_ID  not in (17,18,21,24)
                            AND P.DESATIVADA_POR = 0
                            AND BC.excluido_por is NULL
                            GROUP BY
                            Paciente,B.id,BC.id
            UNION
            SELECT DISTINCT
                            IF(V.DESC_COBRANCA_PLANO IS NULL,CO.item,V.DESC_COBRANCA_PLANO)AS principio,
                            IF(V.DESC_COBRANCA_PLANO IS NULL,CO.id,V.COD_COBRANCA_PLANO) AS cod,
                            IF(V.DESC_COBRANCA_PLANO IS NULL,CO.id,V.id) AS catalogo_id,
                            IF(V.DESC_COBRANCA_PLANO IS NULL,'cobrancaplano','valorescobranca') AS TABELA_ORIGEM,
                            C.nome,
                            5 AS tipo,
                            'N' AS flag,
                            0 AS idSolicitacoes,
                            C.idClientes AS Paciente,
                            C.convenio AS Plano,
                            coalesce(CS.CUSTO,'0.00') AS custo,
                            ('Equipamentos') AS apresentacao,
                            (CASE
                             WHEN COUNT(*)>1 
                             THEN 
                             SUM(S.qtd) * (DATEDIFF('{$fim}', '{$inicio}') + 1)
                              ELSE 
                              S.qtd * (DATEDIFF('{$fim}', '{$inicio}') + 1) END) AS quantidade,
                            coalesce(V.valor,'0.00') AS valor,
                            C.empresa,
                            '' as codTuss,
                            PL.nome as nomeplano,
                            PL.tipo_medicamento

               FROM    
                            `solicitacoes` S INNER JOIN
                            `clientes` C ON (S.paciente = C.idClientes)  INNER JOIN                            
                             prescricoes P ON (S.`idPrescricao`=P.id) INNER JOIN
                       
                        cobrancaplanos CO ON (S.CATALOGO_ID = CO.`id`) LEFT JOIN
                        valorescobranca V ON (CO.id = V.idCobranca
                                AND V.idPlano = C.convenio
                                AND V.excluido_por IS NULL)  LEFT JOIN
                        custo_servicos_plano_ur CS ON 
                        (CS.COBRANCAPLANO_ID = CO.id AND CS.UR = C.empresa AND CS.STATUS = 'A')
                        
                         INNER JOIN    planosdesaude PL on  (C.convenio = PL.ID)
             WHERE
                     C.idClientes ='{$cod}' AND
                     S.tipo=5 AND
                     S.TIPO_SOL_EQUIPAMENTO =1 AND
                      V.excluido_por is NULL
            GROUP BY
                         CO.id,V.id
			UNION
            SELECT DISTINCT
                            IF(V.DESC_COBRANCA_PLANO IS NULL,CO.item,V.DESC_COBRANCA_PLANO)AS principio,
                            IF(V.DESC_COBRANCA_PLANO IS NULL,CO.id,V.COD_COBRANCA_PLANO) AS cod,
                            IF(V.DESC_COBRANCA_PLANO IS NULL,CO.id,V.id) AS catalogo_id,
                            IF(V.DESC_COBRANCA_PLANO IS NULL,'cobrancaplano','valorescobranca') AS TABELA_ORIGEM,
                            C.nome,
                            5 AS tipo,
                            'N' AS flag,
                            0 AS idSolicitacoes,
                            C.idClientes AS Paciente,
                            C.convenio AS Plano,
                            coalesce(CS.CUSTO,'0.00') AS custo,
                            ('Equipamentos') AS apresentacao,
                            (CASE WHEN COUNT(*)>1 THEN SUM(S.QTD) * (DATEDIFF('{$fim}', '{$inicio}') + 1) ELSE S.QTD * (DATEDIFF('{$fim}', '{$inicio}') + 1) END) AS quantidade,
                            coalesce(V.valor,'0.00') AS valor,
                            C.empresa,
                            '' as codTuss,
                            PL.nome as nomeplano,
                            PL.tipo_medicamento
               FROM    
                            `equipamentosativos` S INNER JOIN
                            `clientes` C ON (S.PACIENTE_ID = C.idClientes)  LEFT JOIN                            
                             prescricoes P ON (S.`PRESCRICAO_ID`=P.id) INNER JOIN
                       
                        cobrancaplanos CO ON (S.COBRANCA_PLANOS_ID = CO.`id`) LEFT JOIN
                        valorescobranca V ON (CO.id = V.idCobranca
                                AND V.idPlano = C.convenio
                                AND V.excluido_por IS NULL)  LEFT JOIN
                        custo_servicos_plano_ur CS ON (CS.COBRANCAPLANO_ID = CO.id AND CS.UR = C.empresa AND CS.STATUS = 'A')
                         INNER JOIN    planosdesaude PL on  (C.convenio = PL.ID)
             WHERE
			 	S.PACIENTE_ID ='{$cod}'
			 	AND S.`PRESCRICAO_AVALIACAO` <> 's'
			 	AND (S.`DATA_FIM` = '0000-00-00' OR S.`DATA_FIM` IS NULL)
			 	AND S.`DATA_INICIO` <> '0000-00-00 00:00:00'
			 	AND V.excluido_por is NULL
			  	AND S.finalizado_em IS NULL
			  	AND S.finalizado_por IS NULL
            GROUP BY
			 	CO.id,V.id
            UNION
            Select
IF (
	BC.DESC_COBRANCA_PLANO IS NULL,
	B.item,
	BC.DESC_COBRANCA_PLANO
) AS principio,

IF (
	BC.DESC_COBRANCA_PLANO IS NULL,
	B.id,
	BC.COD_COBRANCA_PLANO
) AS cod,

IF (
	BC.DESC_COBRANCA_PLANO IS NULL,
	B.id,
	BC.id
) AS catalogo_id,

IF (
	BC.DESC_COBRANCA_PLANO IS NULL,
	'cobrancaplano',
	'valorescobranca'
) AS TABELA_ORIGEM,
 C.nome,
 2 as tipo,
 'S' AS flag,
 S.prescricao_enf_id AS idSolicitacoes,
 C.idClientes AS Paciente,
 C.convenio AS Plano,
  coalesce(CS.CUSTO,'0.00') AS custo,
 ('Servi&ccedil;os') AS apresentacao,
 (
   SUM(
                                        CASE
                                        WHEN ESP.id IN (2, 3, 4, 5) THEN

                                                (DATEDIFF(S.fim, S.inicio) + 1)
                                        ELSE
                                        IF (
                                                S.frequencia_id IN (
                                                        2,
                                                        3,
                                                        4,
                                                        5,
                                                        6,
                                                        7,
                                                        8,
                                                        9,
                                                        10,
                                                        11,
                                                        12,
                                                        13,
                                                        14,
                                                        15,
                                                        16,
                                                        17,
                                                        18,
                                                        19
                                                ),
                                                (DATEDIFF(S.fim, S.inicio) + 1) * F.qtd,
                                                CEILING(
                                                        (DATEDIFF(S.fim, S.inicio) + 1) / 7
                                                ) * F.SEMANA
                                        )
                                        END


	)
) AS quantidade,
 COALESCE (BC.valor, '0.00') AS valor,
 C.empresa,
 '' AS codTuss,
   PL.nome as nomeplano,
   PL.tipo_medicamento
FROM
	`prescricao_enf_itens_prescritos` AS S
LEFT JOIN `frequencia` AS F ON (S.frequencia_id = F.id)
INNER JOIN prescricao_enf P ON (S.prescricao_enf_id = P.id)
INNER JOIN `clientes` C ON (P.cliente_id = C.idClientes)
INNER JOIN prescricao_enf_cuidado AS ESP ON (S.presc_enf_cuidados_id = ESP.id)
INNER JOIN cuidadosespeciais as CUI on ESP.cuidado_especiais_id = CUI.id
INNER JOIN `cobrancaplanos` B ON (CUI.COBRANCAPLANO_ID = B.id)
LEFT JOIN `valorescobranca` BC ON (
		B.id = BC.idCobranca
		AND BC.idPlano = C.convenio
		AND BC.excluido_por IS NULL
)
LEFT JOIN custo_servicos_plano_ur CS ON (CS.COBRANCAPLANO_ID = B.id AND CS.UR = C.empresa AND CS.STATUS = 'A')
INNER JOIN planosdesaude AS PL ON (C.convenio = PL.id)
WHERE
  ESP.presc_enf_grupo_id = 7
  AND ESP.cuidado_especiais_id NOT IN (17, 18, 21, 24)
  AND P.cliente_id = '{$cod}'
 AND (P.inicio BETWEEN '{$inicio}' AND '{$fim}' AND P.fim BETWEEN '{$inicio}' AND '{$fim}')
  AND P.carater = 9
GROUP BY
	B.id,
	BC.id
	UNION
	SELECT
                                IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),B.DESC_MATERIAIS_FATURAR) AS principio,
                                S.NUMERO_TISS AS cod,
                                S.CATALOGO_ID as catalogo_id,
                                'catalogo' as TABELA_ORIGEM,
                                C.nome,
                                S.tipo,
                                'P' as flag,
                                S.idSolicitacoes,
                                C.idClientes AS Paciente,
                                C.convenio AS Plano,
                                coalesce(B.valorMedio,'0.00') AS custo,
                                (CASE S.tipo WHEN '0' THEN 'Medicamento' WHEN '1' THEN 'Material' END) as apresentacao,
                                (CASE WHEN COUNT(*)>1 THEN SUM(S.qtd) ELSE S.qtd END) AS quantidade,
                                coalesce(B.VALOR_VENDA,'0.00') AS valor,
                                 C.empresa,
                                 B.TUSS as cadTuss,
                            PL.nome as nomeplano,
                            PL.tipo_medicamento
                        FROM
                             `solicitacoes` S INNER JOIN
                             `clientes` C ON (S.paciente = C.idClientes) INNER JOIN
                             `catalogo` B ON (S.CATALOGO_ID = B.ID) INNER JOIN
                             `prescricao_curativo` pr on (pr.id = S.id_prescricao_curativo) INNER JOIN
                              planosdesaude as PL on (C.convenio=PL.id)
                        WHERE

                              S.qtd>0 AND
                              C.idClientes = S.paciente AND
                              S.tipo IN (0,1) AND
                              pr.carater =9
                              AND S.paciente = '{$cod}'
                              AND (pr.inicio BETWEEN '{$inicio}' AND '{$fim}' AND pr.fim BETWEEN '{$inicio}' AND '{$fim}')

                        GROUP BY
                                Paciente,
                                S.catalogo_id
                                UNION                               
     

SELECT
                            concat(B.principio,' - ',B.apresentacao) as principio,
                             S.NUMERO_TISS AS cod,
                            S.CATALOGO_ID as catalogo_id,
                            '' as TABELA_ORIGEM,
                            C.nome,
                            S.tipo,
                            'P' as flag,
                            S.idSolicitacoes,
                            C.idClientes AS Paciente,
                             C.convenio AS Plano,
                             coalesce(B.valorMedio,'0.00') AS custo,
                            (CASE S.tipo
                            WHEN '0' THEN 'Medicamento'
                            WHEN '1' THEN 'Material'
                            WHEN '3' THEN 'Dieta' END) as apresentacao,
                            (CASE WHEN COUNT(*)>1 THEN SUM(S.qtd) ELSE S.qtd END) AS quantidade,
                            coalesce(B.VALOR_VENDA,'0.00')  AS valor,
                            C.empresa,
                            B.TUSS as codTuss,
                             PL.nome as nomeplano,
                            PL.tipo_medicamento
                        FROM
                            `solicitacoes` S INNER JOIN
                            `clientes` C ON (S.paciente = C.idClientes)  INNER JOIN
                             planosdesaude AS PL ON (C.convenio = PL.id) INNER JOIN  
                            `catalogo` B ON (S.CATALOGO_ID = B.ID)  INNER JOIN
                            relatorio_aditivo_enf AS rel_enf_adt on (S.`relatorio_aditivo_enf_id`=rel_enf_adt.ID)                            
                        WHERE
                         rel_enf_adt.PACIENTE_ID = '{$cod}'
                              AND (rel_enf_adt.INICIO BETWEEN '{$inicio}' AND '{$fim}' AND rel_enf_adt.FIM BETWEEN '{$inicio}' AND '{$fim}')
                          
                             and S.tipo IN (0,1,3) 
                        GROUP BY
                            S.CATALOGO_ID
                            
                            
                            UNION
                        SELECT

                            IF (
                                    BC.DESC_COBRANCA_PLANO IS NULL,
                                    B.item,
                                    BC.DESC_COBRANCA_PLANO
                            ) AS principio,

                            IF (
                                    BC.DESC_COBRANCA_PLANO IS NULL,
                                    B.id,
                                    BC.COD_COBRANCA_PLANO
                            ) AS cod,

                            IF (
                                    BC.DESC_COBRANCA_PLANO IS NULL,
                                    B.id,
                                    BC.id
                            ) AS catalogo_id,

                            IF (
                                    BC.DESC_COBRANCA_PLANO IS NULL,
                                    'cobrancaplano',
                                    'valorescobranca'
                            ) AS TABELA_ORIGEM,
                             C.nome,
                             2 AS tipo,
                             'S' AS flag,
                             '{$cod}' AS idSolicitacoes,
                             C.idClientes AS Paciente,
                             C.convenio AS Plano,
                             COALESCE (
                                    CS.CUSTO,
                                    '0.00'
                            ) AS custo,
                             ('Servi&ccedil;os') AS apresentacao,
                             (
                                    SUM(
                                        CASE
                                        WHEN ESP.id IN (2, 3, 4, 5) THEN	

                                                (DATEDIFF(P.FIM, P.INICIO) + 1)
                                        ELSE
                                        IF (
                                                S.frequencia IN (
                                                        2,
                                                        3,
                                                        4,
                                                        5,
                                                        6,
                                                        7,
                                                        8,
                                                        9,
                                                        10,
                                                        11,
                                                        12,
                                                        13,
                                                        14,
                                                        15,
                                                        16,
                                                        17,
                                                        18,
                                                        19
                                                ),
                                                (DATEDIFF(P.FIM, P.INICIO) + 1) * F.qtd,
                                                CEILING(
                                                        (DATEDIFF(P.FIM, P.INICIO) + 1) / 7
                                                ) * F.SEMANA
                                        )				
                                        END	
                                   )
                               ) AS quantidade,
                             COALESCE (BC.valor, '0.00') AS valor,
                             C.empresa,
                             '' AS codTuss,
                              PL.nome as nomeplano,
                            PL.tipo_medicamento
                            FROM
                                    `solicitacao_rel_aditivo_enf` S
                            LEFT JOIN `frequencia` AS F ON (S.frequencia = F.id)
                            INNER JOIN relatorio_aditivo_enf P ON (S.`relatorio_enf_aditivo_id` = P.ID)
                            INNER JOIN `clientes` C ON (P.PACIENTE_ID = C.idClientes)
                            INNER JOIN cuidadosespeciais AS ESP ON (S.CATALOGO_ID = ESP.id)
                            INNER JOIN `cobrancaplanos` B ON (ESP.COBRANCAPLANO_ID = B.id)
                            LEFT JOIN custo_servicos_plano_ur CS ON (
                                    CS.COBRANCAPLANO_ID = B.id
                                    AND CS.UR = C.empresa
                                   AND CS.STATUS = 'A' 
                            )
                            LEFT JOIN `valorescobranca` BC ON (
                                    B.id = BC.idCobranca
                                    AND BC.idPlano = C.convenio  and BC.excluido_por is null
                            )
                            INNER JOIN planosdesaude AS PL ON (C.convenio = PL.id)
                            WHERE
                                    P.PACIENTE_ID = '{$cod}'
                            AND S.tipo IN (4)
                            AND S.CATALOGO_ID NOT IN (17, 18, 21, 24)
                            AND BC.excluido_por is NULL
                              AND (P.INICIO BETWEEN '{$inicio}' AND '{$fim}' AND P.FIM BETWEEN '{$inicio}' AND '{$fim}')
                            GROUP BY
                                    B.id,
                                    BC.id
                          
                      UNION
                      SELECT DISTINCT
                            IF(V.DESC_COBRANCA_PLANO IS NULL,CO.item,V.DESC_COBRANCA_PLANO)AS principio,
                            IF(V.DESC_COBRANCA_PLANO IS NULL,CO.id,V.COD_COBRANCA_PLANO) AS cod,
                            IF(V.DESC_COBRANCA_PLANO IS NULL,CO.id,V.id) AS catalogo_id,
                            IF(V.DESC_COBRANCA_PLANO IS NULL,'cobrancaplano','valorescobranca') AS TABELA_ORIGEM,
                            C.nome,
                            5 AS tipo,
                            'N' AS flag,
                            0 AS idSolicitacoes,
                            C.idClientes AS Paciente,
                            C.convenio AS Plano,
                            COALESCE (
                                    CS.CUSTO,
                                    '0.00'
                            ) AS custo,
                            ('Equipamentos') AS apresentacao,
                             (S.qtd *(DATEDIFF(rel_enf_adt.FIM, rel_enf_adt.INICIO) + 1))  AS quantidade,
                            coalesce(V.valor,'0.00') AS valor,
                            C.empresa,
                            '' as codTuss,
                             PL.nome as nomeplano,
                            PL.tipo_medicamento
                      FROM    
                            `solicitacoes` S INNER JOIN
                            `clientes` C ON (S.paciente = C.idClientes)  INNER JOIN    
                             planosdesaude AS PL ON (C.convenio = PL.id) INNER JOIN                   
                            cobrancaplanos CO ON (S.CATALOGO_ID = CO.`id`) LEFT JOIN
                            valorescobranca V ON (CO.`id` = V.`idCobranca` AND  C.convenio = V.`idPlano` and V.excluido_por is null)  LEFT JOIN
                            custo_servicos_plano_ur CS ON (CS.COBRANCAPLANO_ID =CO.id  and CS.UR=C.empresa  AND CS.STATUS = 'A') INNER JOIN
                             relatorio_aditivo_enf AS rel_enf_adt on (S.`relatorio_aditivo_enf_id`=rel_enf_adt.ID)
                      WHERE
                             rel_enf_adt.PACIENTE_ID = '{$cod}' AND
                            S.tipo=5 AND
                            S.TIPO_SOL_EQUIPAMENTO =1 AND
                            V.excluido_por is NULL
                               AND (rel_enf_adt.INICIO BETWEEN '{$inicio}' AND '{$fim}' AND rel_enf_adt.FIM BETWEEN '{$inicio}' AND '{$fim}')
                      GROUP BY
                         CO.id,V.id                        

            ORDER BY
                         Paciente, tipo,principio";


    echo formulario();

    $result = mysql_query($sql);
    $resultado = mysql_num_rows($result);


    if($resultado != 0){
        echo "<center><h2>{$label_tipo}</h2></center><br/>";
        $total=0.00;
        $cont=0;
        $nome_titulo='';
        $subtotal=0.00;
        $cod_ident = 0;


        while($row = mysql_fetch_array($result)){



            $total +=$row['quantidade']*$row['valor'];
            $plano = $row['Plano'];
            if($nome_titulo != $row['nome']){

                $subtotal=0.00;
                $subtotal_equipamento=0.00;
                $subtotal_material=0.00;
                $subtotal_medicamento=0.00;
                $subtotal_dieta=0.00;
                $subtotal_servico=0.00;
                $subtotal_gasoterapia=0.00;

                $cod_ident =$row['Paciente'];
                echo"<div id ='div_{$cod_ident}' >";
                echo "</br>
                         <table width=100% style='border:2px dashed;'>
                            <tr><td><center><b>OBSERVA&Ccedil;&Otilde;ES</b></center></td></tr>
                            <tr><td>Na coluna TAXA deve se colocar o percentual se for desconto colocar como negativo ex: -1,00. Caso seja acrescimo positivo ex: 1,00 </td></tr>
                            <tr><td>Na coluna deve-se marcar os itens que não vão ser cobrados porque fazem parte do Pacote</td></tr>
                            </table>
                          </br>";
                //echo "<button id='{$cod_ident}' style='float:left;' inicio='{$inicio}'  fim='{$fim}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover add-item-fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>+</span></button>";
                echo "<br/><table class='mytable' width=100%  >";
                echo "<a><thead class='mostrar' idpaciente='{$cod_ident}'><tr><th colspan='5' style='background-color:#ccc;'><center width='70%' style='vertical-align:middle;'><b>".$row['nome']." (".$row['nomeplano'].")</b></center></th></tr></thead></a>";
                echo"</table><table  width=100% class='ocultar mytable' id='tabela_{$cod_ident}'>";
                echo "<tr>
		    	<th>Item</th>
		    	<th>Tipo</th>
		    	<th>Unidade</th>
		    	<th>Qtd</th>
		    	<th>Pre&ccedil;o Unit.</th>
		    	<th>Pre&ccedil;o Total</th>
		    	<th>Taxa</th>
		    	<th>Pacote</th>
		    	</tr>
              
              <tr>
                <td colspan='4'>
                    <b><input type='checkbox' class='selecionar-todos' paciente='{$cod_ident}'>Selecionar todos para serem removidos</b>
                </td>
                <td colspan='4' align='right' style='padding-right: 25px;'>
                    <b>Selecionar todos para pacote <input type='checkbox' class='selecionar-todos-pacote' paciente='{$cod_ident}'></b>
                </td>
              </tr>";

                $nome_titulo = $row['nome'];

            }

            $subtotal +=$row['quantidade']*$row['valor'];
            if($row['tipo'] == '1'){
                $subtotal_material +=$row['quantidade']*$row['valor'];
            }
            if($row['tipo'] == '0'){
                $subtotal_medicamento +=$row['quantidade']*$row['valor'];
            }
            if($row['tipo'] == '2'){
                $subtotal_servico +=$row['quantidade']*$row['valor'];
            }
            if($row['tipo'] == '3'){
                $subtotal_dieta +=$row['quantidade']*$row['valor'];
            }
            if($row['tipo'] == '5'){
                $subtotal_equipamento +=$row['quantidade']*$row['valor'];
            }
            //$botoes = '<img align=\'right\' class=\'rem-item-solicitacao\'  src=\'../utils/delete_16x16.png\' title=\'Remover\' border=\'0\'>';
            $botoes = "<br><input type='checkbox' class=' excluir-item excluir-item-paciente-{$cod_ident}'/> <b> Excluir</b>";



            $codigoitem =$row['cod'];
            $tuss =$row['codTuss'];
            //////2014-01-16
            ///////// se for material coloca a descrição igual do Simpro que vai ser para fatura porque o principio é um ‘nome genérico’ que as enfermeiras usam.
            if($row['tipo'] == 1 && $row['segundonome']!= NULL){
                $nomeitem =$row['segundonome'];
            }else{
                $nomeitem = $row['principio'];
            }

            ////////Verifica se o plano é petrobras.
            if($plano == 80){
                $checked="checked='checked'";
            }else{
                $checked="";
            }
            //	if($codigoitem != 0 && $row['catalogo_id'] == 0){
            echo "<tr  class='itens_fatura {$cod_ident}' >
		    	<td width='50%'>
		    	    <label id='label-novo-codigo-{$label_item}' style='display:none;'>
                        <button cod='{$label_item}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover usar_tiss'>
                            <span class='ui-button-text'>
                                Usar Tiss
                            </span>
                        </button>
                        <input type='text' id='codigo-proprio-{$label_item}' maxlength='11' name='codigo-proprio'>
                    </label>
		    		<button id='bt-novo-codigo-{$label_item}' cod='{$label_item}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover bt_novo_codigo'>
                        <span class='ui-button-text'>
                            C&oacute;digo Pr&oacute;prio
                        </span>
                    </button>
		    	    TUSS-{$tuss} N-{$codigoitem} - {$nomeitem}{$botoes}
                </td>
		    	<td>{$row['apresentacao']}</td>
		    	<td>".unidades_fatura(NULL)."</td>

		    	<td><input class='qtd' size='7' value='{$row['quantidade']}' paciente='{$row['Paciente']}'/></td>
		    	<td>R$<input type='text' name='valor_plano' size='10' class='valor_plano valor OBG' tiss='{$row['cod']}' catalogo_id='{$row['catalogo_id']}' qtd='{$row['quantidade']}' paciente='{$row['Paciente']}' tuss='{$tuss}' tabela_origem='{$row['TABELA_ORIGEM']}' solicitacao='{$row['idSolicitacoes']}' tipo='{$row['tipo']}' inicio='{$inicio}' fim='{$fim}' custo = '{$row['custo']}' usuario='{$_SESSION["id_user"]}' ord = '{$cont}' flag='{$row['flag']}' value='".number_format($row['valor'],2,',','.')."'></td>
		    	<td><span style='float:right;' id='val_total{$cont}'><b>R$  <span nome='val_linha'>".number_format($row['valor']*$row['quantidade'],2,',','.')."</span></b></span></td>
		    	<td><input type='text' name='taxa' size='10' class='taxa OBG' value='0,00'></td>
                        <td><input type='checkbox'  $checked  name='incluso_pacote' size='10' class='incluso_pacote incluso-pacote-{$cod_ident} OBG'></td>

                           </tr>";
            $cont++;
            //}

            $empresa=$row['empresa'];
            $nome= $row['nome'];
            $tipoMedicamento = $row['tipo_medicamento'];
            $nomeplano = $row['nomeplano'];



        }
        echo "</table>";
        echo "<button id='{$cod_ident}' contador=0 plano='{$plano}' tipo_medicamento='{$tipoMedicamento}' nomeplano = '{$nomeplano}' empresa='{$empresa}' style='float:left;' inicio='{$inicio}'  fim='{$fim}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover add-item-fatura' role='button' aria-disabled='false' ><span class='ui-button-text'>+</span></button>";
        echo "<button  cod='{$cod_ident}'  plano='{$plano}' presc_av='0' nome='$nome_titulo' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover excluir-todos ' role='button' aria-disabled='false' ><span class='ui-button-text'>Excluir Itens</span></button><br/>";
       // salvar_orcamento_prorrogacao
        echo "<button  cod='{$cod_ident}'  plano='{$plano}' presc_av='2' empresa='{$empresa}' id-fatura=0 id='salvar-paciente-{$cod_ident}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover salvar_fatura' nome='{$nome}' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button><br/>";
        echo "<button  cod='{$cod_ident}'  plano='{$plano}' presc_av='2' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover faturar' id-fatura=0 id='finalizar-paciente-{$cod_ident}' role='button' aria-disabled='false' ><span class='ui-button-text'>Finalizar</span></button><br/>";
        echo "<button paciente='{$cod_ident}' style='float:left;' inicio='{$inicio}'  fim='{$fim}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover obs' role='button' aria-disabled='false' ><span class='ui-button-text'>OBSERVA&Ccedil;&Atilde;O</span></button>";

        echo "<table class='mytable' width=100% id='lista-itens'>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_quipamento_".$cod_ident."'><b>SUBTOTAL EQUIPAMENTO: R$</b> <b><i id='equipamento_".$cod_ident."'>".number_format($subtotal_equipamento,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_servico_".$cod_ident."'><b>SUBTOTAL SERVI&Ccedil;O: R$</b> <b><i id='servico_".$cod_ident."'>".number_format($subtotal_servico,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_material_".$cod_ident."'><b>SUBTOTAL MATERIAL: R$</b> <b><i id='material_".$cod_ident."'>".number_format($subtotal_material,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_medicamento_".$cod_ident."'><b>SUBTOTAL MEDICAMENTO: R$</b> <b><i id='medicamento_".$cod_ident."'>".number_format($subtotal_medicamento,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_dieta_".$cod_ident."'><b>SUBTOTAL DIETA: R$</b> <b><i id='dieta_".$cod_ident."'>".number_format($subtotal_dieta,2,',','.')."</i></b></span></td></tr>";
        echo "<tr><td colspan='5' style='font-size:8px;'><span style='float:right;' class='sub_gasoterapia_".$cod_ident."'><b>SUBTOTAL GASOTERAPIA</b> <b><i id='gasoterapia_".$cod_ident."'>".number_format($subtotal_gasoterapia,2,',','.')."</i></b></span></td></tr>";

        echo "<tr><td colspan='5' style='font-size:12px;'><span style='float:right;' class='sub_fatura_".$cod_ident."'><b>TOTAL PACIENTE: R$</b> <b><i>".number_format($subtotal,2,',','.')."</i></b></span></td></tr>";
        //echo "<tr><td colspan='5' style='font-size:12px;'></td></tr>";
        echo "<tr><td colspan='6' id='obs_".$cod_ident."'><span id='span_{$cod_ident}' class='span_obs'><b>OBSERVA&Ccedil;&Atilde;O:</b></br><textarea cols=120 rows=5 {$acesso} name='obs' class='OBG' id='text_{$cod_ident}'></textarea></span></td></tr>";

        echo"</table><table class='mytable' width=100% >";
        echo "<tr><td colspan='5' style='font-size:12px;'><span style='float:right;' cont='{$cont}' cont1='0' id='total_fatura'><b>TOTAL: R$</b> <b><i>".number_format($total,2,',','.')."</i></b></span></td></tr>";
        echo "</table></div>";


    }else{
        echo "<center><b> Nenhum resultado foi encontrado!</b></center>";
    }

}
/////////////////////////////////es

function pesquisar_custo_plano_ur($plano,$ur){
    //escolha =1 cadastrar valores dos custos dos servios, caso escolha =1 ele vai poder vizualizar os precos cadastrados.

    $ur=anti_injection($ur,'numerico');
    $sql="SELECT
       cp.id,
       cp.item,
       cs .ID,
       cs.CUSTO as custo_servico
       FROM
       cobrancaplanos AS cp    LEFT JOIN
      `custo_servicos_plano_ur` AS cs ON ( cp.id = cs.COBRANCAPLANO_ID AND  cs.`UR` ={$ur} and STATUS='A')
      where
      cp.ATIVO = 1
       ORDER BY cp.item ";



    echo"<table  width=100% class=' mytable'>";
    echo "<thead><tr><th>Item</th><th>Custo</th></tr></thead>";
    $result=  mysql_query($sql);
    $i = 0;
    while($row = mysql_fetch_array($result)){
        if($row['ID'] == null){
            $id=-1;
            $custo_anterior='0.00';
            $custo='0,00';
        }else{
            $id=$row['ID'];
            $custo_anterior="{$row['custo_servico']}";
            $custo=number_format($row['custo_servico'], 2, ',', '.');
        }
        if($i % 2 == 0){
            $bgColor = 'white';
        } else {
            $bgColor = '#dcdcdc';
        }
        $i++;
        echo"<tr  bgcolor='{$bgColor}' ><td>".$row['item']."</td><td><input class='valor' ur='{$ur}' id_atual='{$id}' custo_anterior='{$custo_anterior}' id_item='{$row['id']}' value='{$custo}'/></td></tr>";
    }

    echo "</table>";

    echo "<br/ ><br/ ><button  id='salvar-custo-plano-ur' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover ' role='button'  aria-disabled='false' >";
    echo "<span class='ui-button-text'>Salvar</span>";




}

function pesquisar_historico_custo_plano_ur($ur,$item){
    //escolha =1 cadastrar valores dos custos dos servios, caso escolha =1 ele vai poder vizualizar os precos cadastrados.

    $ur=anti_injection($ur,'numerico');
    $condItem = $item == 'todos'? '' : " and cp.id = {$item}";
    $sql="SELECT
       cp.item,
       cs.CUSTO as custo_servico,
		DATE_FORMAT(cs.DATA,'%d/%m/%Y') as dt,
		cs.DATA as dataUS
       FROM
       `custo_servicos_plano_ur`  AS cs INNER JOIN
       cobrancaplanos AS cp  ON ( cs.COBRANCAPLANO_ID = cp.id   AND  cs.`UR` ={$ur} {$condItem})
       UNION
       Select
       cp.item,
       cs.custo as custo_servico,
       DATE_FORMAT(cs.inicio,'%d/%m/%Y') as dt,
       cs.inicio as dataUS
       FROM
       historico_custo_servico_plano_ur as cs INNER JOIN
       cobrancaplanos AS cp  ON ( cs.cobrancaplano_id = cp.id   AND  cs.`empresa_id` ={$ur} {$condItem})
       ORDER BY item, dataUS ";

    $result=  mysql_query($sql);
    $numLinhas =mysql_num_rows($result);


    if(empty($numLinhas)){
        echo "<center><b>Não encontrou nenhum resultado para os filtros utilizados.</b></center>";
        return;

    }
    echo"<table  width=100% class=' mytable'>";
    echo "<thead>
<tr>
<th>Item</th>
<th>Custo</th>
<th>Início</th>
</tr>
</thead>";
    $i = 0;
    while($row = mysql_fetch_array($result)){
        if($i % 2 == 0){
            $bgColor = 'white';
        } else {
            $bgColor = '#dcdcdc';
        }
        $i++ ;

        $id=$row['ID'];
        $custo_anterior="{$row['custo_servico']}";
        $custo=number_format($row['custo_servico'], 2, ',', '.');

        echo"<tr bgcolor='{$bgColor}' >
				<td>".$row['item']."</td>
				<td>{$custo}</td>
				<td>{$row['dt']}</td>
			</tr>";
    }

    echo "</table>";


}

///////////////////////////////SELECT UNIDADE dos itens faturados.
function unidades_fatura($id,$echo){
    $result = mysql_query("SELECT * FROM unidades_fatura ORDER BY UNIDADE;");
    $var = "<select name='unidade'  class='unidade'>";

    $var .= "<option value='-1' selected='selected'>SELECIONE</option>";
    while (@$row = mysql_fetch_array($result)) {
        if (($id <> NULL) && ($id == $row['ID']))
            $var .= "<option value='{$row['ID']}' selected='selected'>" . strtoupper($row['UNIDADE']) . "</option>";
        else
            $var .= "<option value='{$row['ID']}'>" . strtoupper($row['UNIDADE']) . "</option>";
    }
    $var .= "</select>";
    if($echo !=1){
        return $var;
    }else{
        echo $var;
    }


}

function load_fatura_salva_editar($id,$fatura){
    if($fatura == 0){
        $dir = "salvas/fatura_editar/";

    }else{
        $dir = "salvas/orcamento_editar/";

    }
    $filename = $dir . "/" . $arq . ".htm";
    $str = stripslashes(file_get_contents($filename));

    echo formulario();
    echo $str;

}

function  pesquisarPrescricoesAditivas($post){
    extract($post,EXTR_OVERWRITE);


    if(empty($id_paciente) || $id_paciente == -1 ){
        echo "<p>"
            . "<b>ERRO: Selecione o paciente.</b>"
            . "</p>";
        return;
    }
    if(empty($inicio)){
        echo "<p>"
            . "<b>ERRO: Indique a data início.</b>"
            . "</p>";
        return;
    }
    if(empty($fim)){
        echo "<p>"
            . "<b>ERRO: Indique a data fim.</b>"
            . "</p>";
        return;
    }

    $inicio = anti_injection(implode("-",array_reverse(explode("/",$inicio))),'literal');
    $fim = anti_injection(implode("-",array_reverse(explode("/",$fim))),'literal');
    if($inicio > $fim ){
        echo "<p>"
            . "<b>ERRO: A data de início deve ser menor ou igual a data final</b>"
            . "</p>";
        return;
    }
    $idPaciente = anti_injection($id_paciente,'numerico');
    $sql= "
           SELECT
	presc.id AS idp,
	u.nome AS usuario,
	u.idUsuarios,
	DATE_FORMAT(presc. DATA, '%d/%m/%Y - %T') AS sdata,
	DATE_FORMAT(presc.inicio, '%d/%m/%Y') AS inicio1,
	DATE_FORMAT(presc.fim, '%d/%m/%Y') AS fim1,
	UPPER(c.nome) AS paciente,
	c.idClientes AS CodPaciente,
	(
		CASE presc.carater
		WHEN '3' THEN
			concat('Prescrição Aditiva ',presc.flag)
		WHEN '6' THEN
			concat('Prescrição Aditiva Nutrição ',presc.flag)
		END
	) AS tipo,
	presc.carater AS carater,
        presc. DATA as data_us,
        'prescricao' as tabela,
        presc.motivo_ajuste as motivo
FROM
	prescricoes AS presc
INNER JOIN clientes AS c ON (
	c.idClientes = presc.paciente
)
INNER JOIN usuarios AS u ON (presc.criador = u.idUsuarios)
WHERE
	(
		presc.inicio BETWEEN '{$inicio}'
		AND '{$fim}'
		OR presc.fim BETWEEN '{$inicio}'
		AND '{$fim}'
	)
AND presc.Carater IN (3, 6)
AND presc.paciente = {$idPaciente}
GROUP BY
	presc.id 
UNION SELECT
		solicitacoes.relatorio_aditivo_enf_id AS idp,
		usuarios.nome AS usuario,
		usuarios.idUsuarios ,
    DATE_FORMAT(solicitacoes.DATA_SOLICITACAO,'%d/%m/%Y - %T') AS sdata,
		DATE_FORMAT(rel_enf_adt.INICIO,'%d/%m/%Y') AS inicio1,
		DATE_FORMAT(rel_enf_adt.FIM, '%d/%m/%Y') AS fim1,
		UPPER(clientes.nome) AS paciente,
    clientes.idClientes AS CodPaciente, 
		'Solicitação Aditiva de Enfermagem' AS tipo,
		'8' AS carater,
                solicitacoes.DATA_SOLICITACAO as data_us,
        'relAditivoEnf' as tabela,
        '' as motivo
	FROM
		solicitacoes
	INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
	INNER JOIN usuarios ON solicitacoes.enfermeiro = usuarios.idUsuarios
	INNER JOIN relatorio_aditivo_enf AS rel_enf_adt ON (
		solicitacoes.`relatorio_aditivo_enf_id` = rel_enf_adt.ID
	)
	WHERE
		solicitacoes.paciente = '{$idPaciente}'
	AND solicitacoes.`data` BETWEEN '{$inicio}'
	AND '{$fim}'
	AND solicitacoes.relatorio_aditivo_enf_id IS NOT NULL
	GROUP BY
		solicitacoes.relatorio_aditivo_enf_id
		UNION
select
relatorio_aditivo_enf.ID AS idp,
		usuarios.nome AS usuario,
		usuarios.idUsuarios ,
    DATE_FORMAT(relatorio_aditivo_enf.`DATA`,'%d/%m/%Y - %T') AS sdata,
		DATE_FORMAT(relatorio_aditivo_enf.INICIO,'%d/%m/%Y') AS inicio1,
		DATE_FORMAT(relatorio_aditivo_enf.FIM, '%d/%m/%Y') AS fim1,
		UPPER(clientes.nome) AS paciente,
    clientes.idClientes AS CodPaciente, 
		'Solicitação Aditiva de Enfermagem' AS tipo,
		'8' AS carater,
                relatorio_aditivo_enf.`DATA` as data_us,
        'relAditivoEnf' as tabela,
        '' as motivo

from
relatorio_aditivo_enf inner join
solicitacao_rel_aditivo_enf on ( relatorio_aditivo_enf.ID = solicitacao_rel_aditivo_enf.relatorio_enf_aditivo_id and tipo = 4) 
left join solicitacoes on ( relatorio_aditivo_enf.ID = solicitacoes.relatorio_aditivo_enf_id)
INNER JOIN clientes ON relatorio_aditivo_enf.PACIENTE_ID = clientes.idClientes
	INNER JOIN usuarios ON  relatorio_aditivo_enf.USUARIO_ID = usuarios.idUsuarios
where
relatorio_aditivo_enf_id is null 
AND (relatorio_aditivo_enf.INICIO BETWEEN '{$inicio}'
	AND '{$fim}' or relatorio_aditivo_enf.FIM BETWEEN '{$inicio}'
	AND '{$fim}' )
group by relatorio_aditivo_enf.ID
	Union SELECT
prescricao_curativo.id as idp,
usuarios.nome as usuario,
usuarios.idUsuarios,
DATE_FORMAT(prescricao_curativo.registered_at, '%d/%m/%Y - %T') AS sdata,
DATE_FORMAT( prescricao_curativo.inicio, '%d/%m/%Y') AS inicio1,
DATE_FORMAT(prescricao_curativo.fim,'%d/%m/%Y') AS fim1,
UPPER(clientes.nome) AS paciente,
clientes.idClientes AS CodPaciente,
'Presc. de Curativo Aditivo de  Enf.' as tipo,
prescricao_curativo.carater,
prescricao_curativo.registered_at as data_us,
'prescCurativoEnf' as tabela,
'' as motivo
FROM
prescricao_curativo
INNER JOIN clientes ON prescricao_curativo.paciente = clientes.idClientes
INNER JOIN usuarios ON prescricao_curativo.profissional = usuarios.idUsuarios
INNER JOIN carater_prescricao ON prescricao_curativo.carater = carater_prescricao.id
WHERE
(
		prescricao_curativo.inicio BETWEEN '{$inicio}'
		AND '{$fim}'
		OR prescricao_curativo.fim BETWEEN '{$inicio}'
		AND '{$fim}'
	)
AND prescricao_curativo.carater = 8
AND prescricao_curativo.paciente = {$idPaciente}
AND prescricao_curativo.enviado = 1
	GROUP BY
	prescricao_curativo.id
	UNION
	SELECT
prescricao_enf.id as idp,
usuarios.nome as usuario,
usuarios.idUsuarios ,
DATE_FORMAT(prescricao_enf.`data`,'%d/%m/%Y - %T') AS sdata,
DATE_FORMAT(prescricao_enf.inicio,'%d/%m/%Y') AS inicio1,
DATE_FORMAT(prescricao_enf.fim, '%d/%m/%Y') AS fim1,
UPPER(clientes.nome) as paciente,
clientes.idClientes AS CodPaciente,
'Prescrição de Enfermagem' AS tipo,
prescricao_enf.carater,
prescricao_enf.`data` as data_us,
'prescEnf' as tabela,
'' as motivo
FROM
prescricao_enf
INNER JOIN prescricao_enf_itens_prescritos ON prescricao_enf.id = prescricao_enf_itens_prescritos.prescricao_enf_id
INNER JOIN prescricao_enf_cuidado ON prescricao_enf_itens_prescritos.presc_enf_cuidados_id = prescricao_enf_cuidado.id
INNER JOIN clientes ON prescricao_enf.cliente_id = clientes.idClientes
INNER JOIN usuarios ON prescricao_enf.user_id = usuarios.idUsuarios
WHERE
(
		prescricao_enf.inicio BETWEEN '{$inicio}'
		AND '{$fim}'
		OR prescricao_enf.fim BETWEEN '{$inicio}'
		AND '{$fim}'
	)
AND prescricao_enf.carater = 8
AND prescricao_enf_cuidado.presc_enf_grupo_id = 7
and prescricao_enf_cuidado.cuidado_especiais_id NOT IN (17, 18, 21, 24)
AND prescricao_enf.cliente_id = {$idPaciente} AND
prescricao_enf.cancelado_por is NULL
	GROUP BY
	prescricao_enf.id
	ORDER BY
		data_us DESC";


    $result = mysql_query($sql);
    $num_row= mysql_num_rows($result);

    if(!$result){
        echo "<p> "
            . "<b>ERRO: Problema na pesquisa de ".htmlentities('prescrição')." aditivas entre em contato com o TI.</b>"
            . "</p>";
        return;
    }



    if($num_row == 0 ){
        echo "<p>"
            . "<b>Nenhuma ".htmlentities('prescrição ou relatório aditivo')." foram encontrados.</b>"
            . "</p>";
        return;
    }
    $contadorRel=0;

    echo "<table class='mytable' width='100%' >";
    while( $row = mysql_fetch_array($result)){

        $tabela= $row['tabela'];
        if($contador ==0){
            echo "<thead>
                       <tr>
                          <th>Tipo</th>
                          <th>Paciente</th>
                          <th>Profissional</th>
                          <th>Data</th>
                          <th>".htmlentities('Período')."</th>
                          <th>".htmlentities('Ação')."</th>
                       </tr>
                     </thead>";
        }
       $prescricao = ModelFatura::getByPrescricaoIdAndTabela($row['idp'],$tabela);
        $existeOrcamento = 0;
        $info ='';
        $label = '';
        $infoMotivo= '';
        if(!empty($row['motivo'])){
            $infoMotivo = "<button class='ui-button ui-widget ui-state-default ui-corner-all
                         ui-button-text-only ui-state-hover prescricao-motivo-ajuste'                           
                         data-motivo='{$row['motivo']}'                         
                         role='button' aria-disabled='false'
                          style='float:right'>
                            <span class='ui-button-text'>                            
                               ".htmlentities('Motivo')." 
                            </span>
                         </button>";

        }

        if(!empty($prescricao)){
            $existeOrcamento = 1;
            $label = "<div class='ui-state-highlight' style='margin-top: 1px;'>
                                <center><b>Orçado</b></center>
                                </div>";
            $label = "<span class='ui-state-highlight' ><b>Orçado</b></span>";

            $info = "<img src='../utils/details.png' width='16' title='Orçamento feito por {$prescricao[0]['nomeUser']} em {$prescricao[0]['dataBr']}' class='' ></img>";
}
        echo "<tr>
                    <td>{$info} {$row['tipo']} {$label} {$infoMotivo} </td>
                    <td>{$row['paciente']}</td>
                    <td>{$row['usuario']}</td>
                    <td>{$row['sdata']}</td>
                    <td>{$row['inicio1']} ".htmlentities('até')." {$row['fim1']}</td>
                    <td>
                        <button class='ui-button ui-widget ui-state-default ui-corner-all
                         ui-button-text-only ui-state-hover orcar-aditivo' 
                         data-id-prescricao='{$row['idp']}' 
                         data-tipo='{$tabela}'
                         existe-orcamento='{$existeOrcamento}'
                         role='button' aria-disabled='false' >
                            <span class='ui-button-text'>
                               ".htmlentities('Orçar')." 
                            </span>
                         </button>
                    </td>
                    
                  </tr>";
        $contador++;

    }
    echo "</table>";



}

function reabrirFatura($fatura, $justificativa, $paciente, $tipo, $periodo)
{
    $just = addslashes($justificativa);

    $sql = <<<SQL
UPDATE
  fatura
SET
  STATUS = 6,
  data_margem_contribuicao = NULL,
  enviado_margem_contribuicao_por = NULL,
  enviado_margem_contribuicao_em = NULL,
  justificativa_reabertura = '{$just}',
  reabertura_por = '{$_SESSION['id_user']}',
  reabertura_em = NOW(),
  `data_envio_plano_by`  = NULL,
`data_envio_plano_at` = NULL ,
DATA_ENVIO_PLANO = NULL
WHERE
  ID = '{$fatura}'
SQL;
    $rs = mysql_query($sql);
    if($rs) {
        $titulo = 'Avisos do Sistema - Reabertura de Fatura';
        $para[] = ['email' =>'coord.auditoria@mederi.com.br', 'nome' => 'Coord Auditoria'];
        $para[] = ['email' =>'financeiro02@mederi.com.br', 'nome' => 'Financeiro'];

        $texto = "<p>{$tipo} No. <b>{$fatura}</b> no período {$periodo} do paciente {$paciente} foi reaberta por <b>{$_SESSION['nome_user']}</b>!</p> <p>Justificativa: <b>" . htmlentities($justificativa) . "</b></p>";
        enviarEmailSimples($titulo, $texto, $para);
        echo $rs;
    }
}

function enviarEmailSimples($titulo, $texto, $para)
{
    $api_data = (object) Config::get('sendgrid');

    $gateway = new MailAdapter(
        new SendGridGateway(
            new \SendGrid($api_data->user, $api_data->pass),
            new \SendGrid\Email()
        )
    );

    $gateway->adapter->sendSimpleMail($titulo, $texto, $para);
}

function envioFatura($lote, $dataEnvio, $protocolo){

    $sql = "UPDATE
  fatura
	SET
`data_envio_plano_by`  = '{$_SESSION['id_user']}',
`data_envio_plano_at` = now() ,
DATA_ENVIO_PLANO = '{$dataEnvio}',
protocolo_envio = '{$protocolo}'

WHERE
 CAPA_LOTE= '{$lote}'";
    $rs = mysql_query($sql);
    if($rs) {
        echo $lote;
        return;
    }else{
        echo 0;
        return;
    }

}

function cancelar_lote($lote, $justificativa)
{
    mysql_query('begin');
    $msg = '';

    $sql = "select
	*,
clientes.nome as paciente,
planosdesaude.nome as plano
	from
	fatura inner join
	clientes on fatura.PACIENTE_ID = clientes.idClientes INNER JOIN
  planosdesaude on fatura.PLANO_ID = planosdesaude.id
	where
	CAPA_LOTE = '{$lote}'
	";
    $result = mysql_query($sql);

    $msg .= "<p><b>OBS:</b> Quando a coluna <b>Reaberta</b> estiver com o valor  <b>Não</b> é porque essa fatura ainda não foi
                    finalizada pelo financeiro, por isso ela não foi reaberta.
            </p>
<table width='100%' border='1px' bordercolor='black'>
                   <thead>
                   <tr style='text-aling: center;'>
                       <th>Fatura</th>
                       <th>Paciente</th>
                       <th>Período</th>
                       <th>Plano</th>
                       <th>Reaberta</th>
                    </tr>

                   </thead>
                   <tbody>";


    while( $row = mysql_fetch_array($result)) {
        $reaberta = 'Não';
        $reaberturaJustificativa = '';
        if($row['STATUS'] == 3){
            $reaberta = "Sim";
            $reaberturaJustificativa = "Cancelamento do Lote {$row['CAPA_LOTE']}. {$justificativa}";
        }

        $sqlLote = " INSERT INTO
historico_lote_fatura
(id ,
`fatura_id`,
`capa_lote`  ,
`data_lote`  ,
`justificativa`,
`data_envio_plano`  ,
`data_envio_plano_by`,
`data_envio_plano_at` ,
protocolo_envio,
`cancelado_by`,
`cancelado_at`,
`status`,
`data_margem_contribuicao`,
`enviado_margem_contribuicao_por`,
`enviado_margem_contribuicao_em`,
`justificativa_reabertura`
)
VALUES
( null,
{$row['ID']},
'{$row['CAPA_LOTE']}',
'{$row['DATA_LOTE']}',
'{$justificativa}',
'{$row['DATA_ENVIO_PLANO']}',
'{$row['data_envio_plano_by']}',
'{$row['data_envio_plano_at']}',
'{$row['protocolo_envio']}',
{$_SESSION['id_user']},
NOW(),
'{$row['STATUS']}',
'{$row['data_margem_contribuicao']}',
'{$row['enviado_margem_contribuicao_por']}',
'{$row['enviado_margem_contribuicao_em']}',
'{$reaberturaJustificativa}'
)";

        $protocoloEnvio = $row['protocolo_envio'];
        $dataEnvio = DateTime::createFromFormat('Y-m-d H:i:s', $row['DATA_ENVIO_PLANO'])->format('d/m/Y');;

        $inicio = implode('/',array_reverse(explode('-',$row['DATA_INICIO'])));
        $fim = implode('/',array_reverse(explode('-',$row['DATA_FIM'])));

        $msg .= "<tr style='text-aling: center;' >
                        <td>#{$row['ID']}</td>
                        <td>{$row['paciente']}</td>
                        <td>{$inicio} - {$fim} </td>
                        <td>{$row['plano']}</td>
                        <td>{$reaberta}</td>
                </tr>";
        $resultLote = mysql_query($sqlLote);
        if(!$resultLote){
            echo "Erro ao salvar histórico do lote ".$sqlLote;
            mysql_query("rollback");
            return;
        }
        savelog(mysql_escape_string(addslashes($sqlLote)));

    }
    $msg .="</tbody>
    </table>";

    $sqlUpdate = "UPDATE
fatura
SET
CAPA_LOTE = '',
DATA_LOTE = '',
DATA_ENVIO_PLANO = NULL,
data_envio_plano_at = NULL,
data_envio_plano_by = NULL,
STATUS = 5,
data_margem_contribuicao = NULL,
enviado_margem_contribuicao_por = NULL,
enviado_margem_contribuicao_em = NULL,
justificativa_reabertura = '{$justificativa}',
reabertura_por = '{$_SESSION['id_user']}',
reabertura_em = NOW()
WHERE
CAPA_LOTE = '{$lote}'";

    $resultUpdate = mysql_query($sqlUpdate);
    if(!$resultUpdate){
        echo "Erro ao fazer update na tabela fatura. ";
        mysql_query("rollback");
        return;
    }
    savelog(mysql_escape_string(addslashes($sqlUpdate)));

    mysql_query('commit');
    $msgProtocolo = '';
    if(!empty($protocoloEnvio)){

        $msgProtocolo = "Enviado ao plano em {$dataEnvio}  protocolo: {$protocoloEnvio}.<br>";
    }
    $hoje = date('d/m/Y H:i');
    $msgTitulo = "O lote de fatura(s) <b>{$lote}</b> foi cancelado por <b>{$_SESSION['nome_user']}</b> em <b>{$hoje}</b> as faturas finalizadas foram reabertas para poderem ser inseridas em um novo lote.
            <br> <b>Justificativa: </b> {$justificativa}";
    $titulo = 'Avisos do Sistema - Cancelamento de Lote / Reabertura de Fatura';
    $para[] = ['email' =>'financeiro02@mederi.com.br', 'nome' => 'Financeiro'];
    $texto = $msgTitulo.' '.$msgProtocolo.' '.$msg;
    enviarEmailSimples($titulo, $texto, $para);
    echo 1;
}

function getUnidadesMedidasFatura() {
    $sql = <<<SQL
SELECT * FROM unidades_fatura ORDER BY UNIDADE
SQL;
    $rs = mysql_query($sql);

    $json = [];
    while($row = mysql_fetch_array($rs)) {
        $json[$row['ID']] = [
            'unidade' => $row['UNIDADE'],
            'tiss' => $row['tiss_codigo'],
        ];
    }

    return json_encode($json);
}

function buscarItensKitFatura($kit_id, $plano, $tipo_documento) {

    $sql = <<<SQL
SELECT
  planosdesaude.tipo_medicamento
FROM
  planosdesaude
WHERE 
  id = '{$plano}'
SQL;
    $rs = mysql_query($sql);
    $rowPlano = mysql_fetch_array($rs);

    $condGenerico = $rowPlano['tipo_medicamento'] == 'G' ?
        " AND ( catalogo.GENERICO = 'S' OR catalogo.possui_generico = 'N' ) " :
        "";

    $condCusto = '';
    $condAtivo = " AND ATIVO = 'A'";
    if($tipo_documento === '0'){
        $condCusto = " AND valorMedio > 0";
        $condAtivo = " AND (ATIVO = 'A' OR descontinuado = 'S')";
    }

    $sql = <<<SQL
SELECT
  concat(catalogo.principio,' ',catalogo.apresentacao) as name,
  CASE 
    WHEN tipo = 0 THEN 'MEDICAMENTO' 
    WHEN tipo = 1 THEN 'MATERIAL'
    WHEN tipo = 3 THEN 'DIETA'
  END AS apresentacao,
  catalogo.NUMERO_TISS as cod,
  DESC_MATERIAIS_FATURAR AS desc_faturar,
  valorMedio as custo,
  catalogo.ID as catalogo_id,
  VALOR_VENDA as val,
  TUSS as codTuss,
  GENERICO,
  catalogo.tipo,
  itens_kit.qtd
FROM
  itens_kit
  INNER JOIN catalogo ON itens_kit.CATALOGO_ID = catalogo.ID
WHERE
  itens_kit.idKit = '{$kit_id}'
  {$condCusto}
  {$condAtivo}
  {$condGenerico}
SQL;
    $rs = mysql_query($sql);

    $json = [];
    while($row = mysql_fetch_array($rs)) {
        $val = number_format($row['val'], 2, ',', '.');
        $nome = $row['cod'] . "  " . $row['name'];

        $json[$row['catalogo_id']] = [
            'nome' => $nome,
            'id' => $row['cod'],
            'apresentacao' => $row['apresentacao'],
            'tiss' => $row['cod'],
            'desc_faturar' => $row['tipo'] == '1' && $row['desc_faturar'] != NULL ? $row['desc_faturar'] : $row['name'],
            'custo' => $row['custo'],
            'val' => $row['val'],
            'tuss' => $row['codTuss'],
            'generico' => $row['GENERICO'],
            'tipo_aba' => 'kit',
            'tipo' => $row['tipo'],
            'qtd' => $row['qtd'],
        ];
    }

    return json_encode($json);
}

function buscarFiliaisAtivas() {
    $json = Filial::getUnidadesAtivas();
    return json_encode($json);
}

$querySwitch = filter_input(INPUT_POST, 'query');

switch($querySwitch){
    case "listar-itens":
        $inicio = join("-",array_reverse(explode("/",$_POST['inicio'])));
        $fim = join("-",array_reverse(explode("/",$_POST['fim'])));
        listar_itens($_POST['tipo'],$_POST['cod'],$inicio,$fim,$_POST['nome'],$_POST['refaturar'],$_POST['plano']);
        break;
    case "listar-fatura":
        $inicio = join("-",array_reverse(explode("/",$_POST['inicio'])));
        $fim = join("-",array_reverse(explode("/",$_POST['fim'])));
        listar_fatura($_POST['tipo'],$_POST['cod'],$inicio,$fim,$_POST['nome']);
        break;
    case "listar-fatura-lote":
        $inicio = join("-",array_reverse(explode("/",$_POST['inicio'])));
        $fim = join("-",array_reverse(explode("/",$_POST['fim'])));
        listar_fatura_lote($_POST['tipo'],$_POST['cod'],$inicio,$fim,$_POST['nome']);
        break;
    case "gerenciar-lote":
        $inicio = join("-",array_reverse(explode("/",$_POST['inicio'])));
        $fim = join("-",array_reverse(explode("/",$_POST['fim'])));
        gerenciar_lote($_POST['tipo'],$_POST['cod'],$inicio,$fim,$_POST['nome'],$_POST['tipo_documento']);
        break;
    case "fatura-av":
        faturar_av($_POST['cod'],$_POST['nome'],$_POST['op_avaliacao'],$_POST['inicio'],$_POST['fim']);
        break;
    case "listar-fatura-glosa":
        $inicio = join("-",array_reverse(explode("/",$_POST['inicio'])));
        $fim = join("-",array_reverse(explode("/",$_POST['fim'])));
        listar_fatura_glosa($_POST['tipo'],$_POST['cod'],$inicio,$fim,$_POST['nome']);
        break;
    case "listar-faturas":
        $inicio = join("-",array_reverse(explode("/",$_POST['inicio'])));
        $fim = join("-",array_reverse(explode("/",$_POST['fim'])));
        $inicio_feita = join("-",array_reverse(explode("/",$_POST['inicio_feita'])));
        $fim_feita = join("-",array_reverse(explode("/",$_POST['fim_feita'])));
        listar_faturas($_POST['tipo'],$_POST['cod'],$inicio,$fim,$_POST['nome'],$inicio_feita,$fim_feita,$_POST['cancelado'],$_POST['tipo_documento']);
        break;

    case "busca-saida":
        buscar_saida($_POST);
        break;
    case "brasindice":
        brasindice($_POST['ur'],$_POST['item'], $_POST['tipo']);
        break;
    case "load_fatura_salva":
        load_fatura_salva($_POST['id_paciente'], $_POST['fatura'], $_POST['tipo']);
        break;
    case "itens-orcamento-prorrogacao":
        $inicio = join("-",array_reverse(explode("/",$_POST['inicio'])));
        $fim = join("-",array_reverse(explode("/",$_POST['fim'])));

        itens_orcamento_prorrogacao($_POST['tipo'],$_POST['cod'],$inicio,$fim,$_POST['nome'],$_POST['refaturar'],$_POST['plano']);
        break;
    case "pesquisar-custo-plano-ur":
        pesquisar_custo_plano_ur($_POST['plano'],$_POST['ur']);
        break;
    case "pesquisar-historico-custo-plano-ur":
        pesquisar_historico_custo_plano_ur($_POST['ur'],$_POST['item']);
        break;
    case "unidades-fatura":
        unidades_fatura(NULL,1);
        break;
    case "load_fatura_salva_editar":
        load_fatura_salva_editar($_POST['arquivo'],$_POST['fatura']);
        break;
    case "pesquisar-prescricoes-aditivas":
        pesquisarPrescricoesAditivas($_POST);
        break;
    case "reabrir-fatura":
        $fatura = filter_input(INPUT_POST, 'fatura', FILTER_SANITIZE_NUMBER_INT);
        $justificativa = filter_input(INPUT_POST, 'justificativa', FILTER_SANITIZE_STRING);
        $paciente = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_STRING);
        $tipo = filter_input(INPUT_POST, 'tipo', FILTER_SANITIZE_STRING);
        $periodo = filter_input(INPUT_POST, 'periodo', FILTER_SANITIZE_STRING);
        reabrirFatura($fatura, $justificativa, $paciente, $tipo, $periodo);
        break;
    case "envio-fatura":
        $dataEnvio = join("-",array_reverse(explode("/",$_POST['envio'])));
        $lote = $_POST['lote'];
        $protocolo = $_POST['protocolo'];
        envioFatura($lote, $dataEnvio, $protocolo);
        break;
    case "salvar-simulacao":
        salvar_simulacao($_POST);
        break;
    case "cancelar-lote":
        cancelar_lote($_POST['lote'], $_POST['justificativa']);
        break;
}

$queryGet = filter_input(INPUT_GET, 'query');

switch($queryGet) {
    case "buscar-custo-orcamento":
        if($_GET['tipo'] == 'novo') {
            detalheFatura($_GET['orcamento']);
        } else {
            visualizar_simulacao($_GET['orcamento']);
        }
        break;
    case "buscar-unidade-medida-fatura":
        echo getUnidadesMedidasFatura();
        break;
    case "buscar-itens-kit-fatura":
        echo buscarItensKitFatura($_GET['kit_id'], $_GET['plano_id'], $_GET['tipo_documento']);
        break;
    case "buscar-filiais-ativas":
        $a = 1;
        echo buscarFiliaisAtivas();
        break;
}
