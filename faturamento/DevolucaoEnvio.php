<?php

/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 13/09/2017
 * Time: 09:35
 */
class DevolucaoEnvio
{

    public function hmtlDevolucaoEnvio($paciente, $inicio, $fim){
            $sqlDevolucao = $this->getDevolucaoByPacientePeriodo($paciente, $inicio, $fim);
            $sqlEnvio = $this->getEnvioEquipamento($paciente, $inicio, $fim);

        var_dump($sqlDevolucao, $sqlEnvio );


    }
    public function getDevolucaoByPacientePeriodo($paciente, $inicio, $fim)
    {
        $sql = " SELECT

                concat(catalogo.principio,' - ',catalogo.apresentacao, ' LAB: ', catalogo.lab_desc) as nome,
                devolucao.LOTE,
                devolucao.QUANTIDADE as qtd,
                devolucao.DATA_DEVOLUCAO
              FROM devolucao INNER JOIN clientes on ( clientes.idClientes = devolucao.PACIENTE_ID)
                INNER JOIN catalogo on(catalogo.ID= devolucao.CATALOGO_ID)
              WHERE
                devolucao.PACIENTE_ID ={$paciente} and
                devolucao.TIPO in (0,1,3) AND
                devolucao.DATA_DEVOLUCAO   BETWEEN '$inicio' AND '$fim'

      union
      SELECT
        cobrancaplanos.item as nome,
        devolucao.LOTE,
        devolucao.quantidade as qtd,
        devolucao.DATA_DEVOLUCAO
      FROM
        devolucao INNER JOIN
        clientes on ( clientes.idClientes = devolucao.PACIENTE_ID) INNER JOIN
        cobrancaplanos on (cobrancaplanos.id = devolucao.CATALOGO_ID)
      WHERE
        devolucao.PACIENTE_ID = {$paciente} and
        devolucao.TIPO = 5 AND
        devolucao.DATA_DEVOLUCAO   BETWEEN '$inicio' AND '$fim'

      ORDER BY
        `nome` ASC";

        return $sql;
    }

    public function getEnvioEquipamento($paciente, $inicio, $fim)
    {
        $sql = "SELECT
		cb.item as nome,
		s.LOTE,
		s.quantidade as qtd,
		s.data
		FROM
		saida as s inner join clientes as p on ( p.idClientes = s.idCliente)
		inner join cobrancaplanos as cb on (cb.id = s.CATALOGO_ID)
		WHERE s.idCliente ={$paciente} and s.tipo = 5
      AND s.data BETWEEN '$inicio' AND '$fim'
		ORDER BY `nome` ASC";
        return $sql;
    }

}