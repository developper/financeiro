<?php
$id = session_id();
if(empty($id))
	session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once ('tipos_faturas.php');
require __DIR__ . '/../app/Models/Faturamento/Fatura.php';

use App\Models\Faturamento\Fatura;
use App\Models\Administracao\Filial;

class imprimir_fatura{

	public $inicio;
	public $fim;

	public function cabecalho($id, $empresa = null){


		$sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		UPPER(u.nome) AS usuario,DATE_FORMAT(f.DATA,'%Y-%m-%d') as DATA,f.DATA_INICIO,f.DATA_FIM,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo1,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresan,
		c.`nascimento`,
		c.diagnostico,
		c.codigo,
		p.nome as Convenio,p.id,c.*,
		NUM_MATRICULA_CONVENIO,
                f.GUIA_TISS,
                cpf
		FROM
		fatura as f inner join
		clientes AS c on (f.PACIENTE_ID = c.idClientes) LEFT JOIN
		empresas AS e ON (e.id = c.empresa) LEFT JOIN 
		planosdesaude as p ON (p.id = f.PLANO_ID) INNER JOIN
                usuarios as u on (f.USUARIO_ID = u.idUsuarios)
		
		WHERE
		f.ID = {$id}
		ORDER BY
		c.nome DESC LIMIT 1";

		$result = mysql_query($sql);
		$result2 = mysql_query($sql2);
		$html.= "<table style='width:100%;' >";
		while($pessoa = mysql_fetch_array($result2)){
			foreach($pessoa AS $chave => $valor) {
				$pessoa[$chave] = strtoupper(stripslashes($valor));
			}
            $pessoa['empresan'] = !empty($empresa) ? strtoupper($empresa) : $pessoa['empresan'];
			$this->inicio = $pessoa['DATA_INICIO'];
			$this->fim = $pessoa['DATA_FIM'];
			$idade = join("/",array_reverse(explode("-",$pessoa['nascimento'])))."(".$pessoa['idade']." anos)";

			if($idade == "00/00/0000( anos)"){
				$idade='';
			}
			$html.= "<tr bgcolor='#EEEEEE'>";
			$html.= "<td  style='width:60%;'><b>PACIENTE:</b></td>";
			$html.= "<td style='width:20%;' ><b>SEXO </b></td>";
			$html.= "<td style='width:30%;'><b>IDADE:</b></td>";
			$html.= "</tr>";
			$html.= "<tr>";
			$html.= "<td style='width:60%;'>{$pessoa['paciente']}</td>";
			$html.= "<td style='width:20%;'>{$pessoa['sexo1']}</td>";
			$html.= "<td style='width:30%;'>{$idade}</td>";
			$html.= "</tr>";

			$html.= "<tr bgcolor='#EEEEEE'>";

			$html.= "<td style='width:60%;'><b>UNIDADE REGIONAL:</b></td>";
			$html.= "<td style='width:20%;'><b>CONV&Ecirc;NIO </b></td>";
			$html.= "<td style='width:30%;'><b>MATR&Iacute;CULA </b></td>";
			$html.= "</tr>";
			$html.= "<tr>";

			$html.= "<td style='width:60%;'>{$pessoa['empresan']}</td>";
			$html.= "<td style='width:20%;'>".strtoupper($pessoa['Convenio'])."</td>";
			$html.= "<td style='width:30%;'>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
			$html.= "</tr>";

			$html.= "<tr bgcolor='#EEEEEE'>";

			$html.= "<td style='width:60%;'><b>FATURISTA:</b></td>";
			$html.= "<td style='width:20%;'><b>DATA FATURADO: </b></td>";
			$html.= "<td style='width:30%;'><b>PERÍODO</b></td>";
			$html.= "</tr>";
			$html.= "<tr>";

			$html.= "<td style='width:60%;'>{$pessoa['usuario']}</td>";
			$html.= "<td style='width:20%;'>".join("/",array_reverse(explode("-",$pessoa['DATA'])))."</td>";
			$html.= "<td style='width:30%;'>".join("/",array_reverse(explode("-",$pessoa['DATA_INICIO'])))." at&eacute; ".join("/",array_reverse(explode("-",$pessoa['DATA_FIM'])))."</td>";
			$html.= "</tr>";

			$html.= "<tr bgcolor='#EEEEEE'>";

			$html.= "<td style='width:60%;'><b>CID:</b></td>";
			$html.= "<td style='width:20%;'><b>CONTA MEDERI: </b></td>";
			$html.= "<td style='width:30%;'><b>N° DA GUIA</b></td>";
			$html.= "</tr>";
			$html.= "<tr>";

			$html.= "<td style='width:60%;'>{$pessoa['diagnostico']}</td>";
			$html.= "<td style='width:20%;'>{$pessoa['codigo']}</td>";
			$html.= "<td style='width:30%;'>{$pessoa['GUIA_TISS']}</td>";
			$html.= "</tr>";
			$html.= " <tr bgcolor='#EEEEEE'>
                          <td COLSPAN='3'>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
			$this->plan =  $pessoa['id'];
		}
		$html.= "</table>";

		return $html;
	}

	public function detalhe_fatura($id,$av,$zerados,$modelo,$diarias,$observacao, $empresa){
		$html .="<form>";

        $responseEmpresa = Filial::getEmpresaById($empresa);
        $logo = "../utils/logos/{$responseEmpresa['logo']}";

		if($_GET['av'] == 0){
			$html .= "<h1><a><img src='{$logo}'  width='100' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> Fatura de Assist&ecirc;ncia Domiciliar  </h1>";
		}else if($_GET['av'] == 1) {
			$html .= "<h1><a><img src='{$logo}'  width='100' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> Or&ccedil;amento </h1>";
		}else if($_GET['av'] == 2){
			$html .= "<h1><a><img src='{$logo}'  width='100' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> Or&ccedil;amento de Prorroga&ccedil;&atilde;o </h1>";
		}else if($_GET['av'] == 3){
			$html .= "<h1><a><img src='{$logo}'  width='100' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> Or&ccedil;amento Aditivo </h1>";
		}

		$html .= $this->cabecalho($id, $responseEmpresa['nome']);
		$html .="<br></br>";
		if ($zerados == 0){
			$condicao = "AND VALOR_FATURA != '0.00' ";
		}else{
			$condicao = " ";
		}
		$sql = Fatura::getSQL($id, $condicao);

		$layout = new LayoutFaturas();
		switch($modelo){
			case 1:
				$html.= $layout->conta_aberta($sql, $av,$this->inicio,$this->fim,$diarias,$observacao);
				break;
			case 2:
				$html.= $layout->conta_fechada($sql, $av,$this->inicio,$this->fim,$diarias,$observacao);
				break;
			case 3:
				$html.= $layout->sulamerica_liminar($sql, $av,$this->inicio,$this->fim,$diarias,$observacao);
				break;
		}

		$paginas []= $header.$html.= "</form>";

		//print_r($paginas);

		$mpdf=new mPDF('pt','A4',9);
		$mpdf->SetHeader('página {PAGENO} de {nbpg}');
		$ano = date("Y");
		$mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']} &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));
		$mpdf->WriteHTML("<html><body>");
		$flag = false;
		foreach($paginas as $pag){
			if($flag) $mpdf->WriteHTML("<formfeed>");
			$mpdf->WriteHTML($pag);
			$flag = true;
		}
		$mpdf->WriteHTML("</body></html>");
		$mpdf->Output('prescricao_avaliacao.pdf','I');
		exit;
	}
}

$p = new imprimir_fatura();
$p->detalhe_fatura($_GET['id'],$_GET['av'],$_GET['zerados'],$_GET['modelo'],$_GET['diarias'],$_GET['observacao'], $_GET['empresa']);

