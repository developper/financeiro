<?php
$id = session_id();
if(empty($id))
  session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
require __DIR__ . '/../vendor/autoload.php';

use App\Models\Faturamento\LoteFatura,
    App\Models\Faturamento\Fatura;

class imprimir_lote{
		
	public function cabecalho($id){
		
		
		$sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		UPPER(u.nome) AS usuario,DATE_FORMAT(f.DATA_LOTE,'%Y-%m-%d') as DATA,f.DATA_INICIO,f.DATA_FIM,f.CAPA_LOTE,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresan,
		c.`nascimento`,
		p.nome as Convenio,p.id,c.*,
		NUM_MATRICULA_CONVENIO
		FROM
		fatura as f inner join
		clientes AS c on (f.PACIENTE_ID = c.idClientes) LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN
		planosdesaude as p ON (p.id = c.convenio) INNER JOIN usuarios as u on (f.USUARIO_ID = u.idUsuarios)
		WHERE
		f.CAPA_LOTE = '{$id}' and CANCELADO_EM ='0000-00-00 00:00:00'
		ORDER BY
		c.nome DESC LIMIT 1";
	
		$result = mysql_query($sql);
		$result2 = mysql_query($sql2);
		$html.= "<table style='width:100%;' >";
		while($pessoa = mysql_fetch_array($result2)){
		foreach($pessoa AS $chave => $valor) {
		$pessoa[$chave] = stripslashes($valor);
	
		}
	
	
				
	
				$html.= "<tr bgcolor='#EEEEEE'>";
	
				$html.= "<td colspan='2' ><b>Operadora:</b></td>";
				$html.= "<td><b>Numero do Lote:</b></td>";
				
				$html.= "</tr>";
				$html.= "<tr>";
	
				
				$html.= "<td colspan='2' >".strtoupper($pessoa['Convenio'])."</td>";
				$html.= "<td  >".$pessoa['CAPA_LOTE']."</td>";
				
				$html.= "</tr>";
				
				$html.= "<tr bgcolor='#EEEEEE'>";
				
				$html.= "<td style='width:60%;'><b>Faturista:</b></td>";
				$html.= "<td style='width:20%;' colspan='2'><b>Data do Lote: </b></td>";
				//$html.= "<td style='width:30%;'><b>Per&iacute;odo</b></td>";
				$html.= "</tr>";
				$html.= "<tr>";
				
				$html.= "<td style='width:60%;'>{$pessoa['usuario']}</td>";
				$html.= "<td style='width:20%;' colspan='2'>".join("/",array_reverse(explode("-",$pessoa['DATA'])))."</td>";
				//$html.= "<td style='width:30%;'>".join("/",array_reverse(explode("-",$pessoa['DATA_INICIO'])))." at&eacute; ".join("/",array_reverse(explode("-",$pessoa['DATA_FIM'])))."</td>";
				$html.= "</tr>";
			$html.= " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
	
				
	
	
	
	$this->plan =  $pessoa['id'];
	}
	$html.= "</table>";
	
	return $html;
	}
	
	
	
	
	
	
	public function cabecalho_fatura($id){
		
		
		$sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		UPPER(u.nome) AS usuario,DATE_FORMAT(f.DATA,'%Y-%m-%d') as DATA,f.DATA_INICIO,f.DATA_FIM,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo1,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresan,
		c.`nascimento`,
		c.diagnostico,
		c.codigo,
		p.nome as Convenio,p.id,c.*,
		NUM_MATRICULA_CONVENIO,
                f.GUIA_TISS
		FROM
		fatura as f inner join
		clientes AS c on (f.PACIENTE_ID = c.idClientes) LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN 
		planosdesaude as p ON (p.id = c.convenio) INNER JOIN usuarios as u on (f.USUARIO_ID = u.idUsuarios)
		
		WHERE
		f.ID = {$id} and CANCELADO_EM = '0000-00-00 00:00:00'
		ORDER BY
		c.nome DESC LIMIT 1";
	
		$result = mysql_query($sql);
		$result2 = mysql_query($sql2);
		$faturas.= "<table style='width:100%;' >";
		while($pessoa = mysql_fetch_array($result2)){
		foreach($pessoa AS $chave => $valor) {
		$pessoa[$chave] = stripslashes($valor);
	
		}
	           $idade = join("/",array_reverse(explode("-",$pessoa['nascimento'])))."(".$pessoa['idade']." anos)";
	          
	           if($idade == "00/00/0000( anos)"){
	           	$idade='';
	           }
				$faturas.= "<tr bgcolor='#EEEEEE'>";
				$faturas.= "<td  style='width:60%;'><b>PACIENTE:</b></td>";
				$faturas.= "<td style='width:20%;' ><b>SEXO </b></td>";
				$faturas.= "<td style='width:30%;'><b>IDADE:</b></td>";
				$faturas.= "</tr>";
				$faturas.= "<tr>";
				$faturas.= "<td style='width:60%;'>{$pessoa['paciente']}</td>";
				$faturas.= "<td style='width:20%;'>{$pessoa['sexo1']}</td>";
				$faturas.= "<td style='width:30%;'>{$idade}</td>";
				$faturas.= "</tr>";
	
				$faturas.= "<tr bgcolor='#EEEEEE'>";
	
				$faturas.= "<td style='width:60%;'><b>UNIDADE REGIONAL:</b></td>";
				$faturas.= "<td style='width:20%;'><b>CONV&Ecirc;NIO </b></td>";
				$faturas.= "<td style='width:30%;'><b>MATR&Iacute;CULA </b></td>";
				$faturas.= "</tr>";
				$faturas.= "<tr>";
	
				$faturas.= "<td style='width:60%;'>{$pessoa['empresan']}</td>";
				$faturas.= "<td style='width:20%;'>".strtoupper($pessoa['Convenio'])."</td>";
				$faturas.= "<td style='width:30%;'>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
				$faturas.= "</tr>";
				
				$faturas.= "<tr bgcolor='#EEEEEE'>";
				
				$faturas.= "<td style='width:60%;'><b>Faturista:</b></td>";
				$faturas.= "<td style='width:20%;'><b>Data faturado: </b></td>";
				$faturas.= "<td style='width:30%;'><b>Per&iacute;odo</b></td>";
				$faturas.= "</tr>";
				$faturas.= "<tr>";
				
				$faturas.= "<td style='width:60%;'>{$pessoa['usuario']}</td>";
				$faturas.= "<td style='width:20%;'>".join("/",array_reverse(explode("-",$pessoa['DATA'])))."</td>";
				$faturas.= "<td style='width:30%;'>".join("/",array_reverse(explode("-",$pessoa['DATA_INICIO'])))." at&eacute; ".join("/",array_reverse(explode("-",$pessoa['DATA_FIM'])))."</td>";
				$faturas.= "</tr>";
				
				$faturas.= "<tr bgcolor='#EEEEEE'>";
				
				$faturas.= "<td style='width:60%;'><b>CID:</b></td>";
				$faturas.= "<td style='width:20%;'><b>CONTA MEDERI: </b></td>";
				$faturas.= "<td style='width:30%;'>N° DA GUIA</td>";
				$faturas.= "</tr>";
				$faturas.= "<tr>";
				
				$faturas.= "<td style='width:60%;'>{$pessoa['diagnostico']}</td>";
				$faturas.= "<td style='width:20%;'>{$pessoa['codigo']}</td>";
				$faturas.= "<td style='width:30%;'>{$pessoa['GUIA_TIS']}</td>";
				$faturas.= "</tr>";
			$faturas.= " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
				
	
				
	
	
	
	$this->plan =  $pessoa['id'];
	}
	$faturas.= "</table>";
	
	return $faturas;
	}
	
	
	
	public function detalhe_fatura($id,$av,$zerados){
		$faturas.="<form>";

		if($av == 0){
		$faturas.= "<h1><a><img src='../utils/logo2.jpg' width='200' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> Fatura de Assist&ecirc;ncia Domiciliar  </h1>";
		}else if($av == 1){
			$faturas.= "<h1><a><img src='../utils/logo2.jpg' width='200' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> Orçamento Inicial </h1>";
		}else if($av == 2){
			$faturas.= "<h1><a><img src='../utils/logo2.jpg' width='200' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> Orçamento Prorrogação  </h1>";
		}else if($av == 3){
			$faturas.= "<h1><a><img src='../utils/logo2.jpg' width='200' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> Orçamento  Aditivo</h1>";
		}
		$faturas.= $this->cabecalho_fatura($id);
		$faturas.="<br></br>";
		if ($zerados == 0){
		$condicao = "AND VALOR_FATURA != '0.00' ";
		}else{
		$condicao = " ";
		}
		
                $sql= "SELECT
                            f.*,
                            fa.OBS,
                            IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),B.DESC_MATERIAIS_FATURAR) AS principio,
                            (CASE f.TIPO
                            WHEN '0' THEN 'Medicamento'
                            WHEN '1' THEN 'Material'
                             WHEN '3' THEN 'Dieta' END) as apresentacao,
                             (CASE f.TIPO
                            WHEN '0' THEN '3'
                            WHEN '1' THEN '4'
                             WHEN '3' THEN '5' END) as ordem,
                            1 as cod,
                          B.REFERENCIA as ref
		    FROM
                                faturamento as f  INNER JOIN
                                `catalogo` B ON (f.CATALOGO_ID = B.ID)
                                inner join fatura fa on (f.FATURA_ID=fa.ID)
		    WHERE
			f.FATURA_ID= {$id} and
			f.TIPO IN (0,1,3) {$condicao}
                         and f.CANCELADO_POR is NULL

		UNION
			SELECT
                            f.*,
                            fa.OBS,
			    vc.DESC_COBRANCA_PLANO AS principio,
		           ('Servi&ccedil;os') AS apresentacao,
                            1 as ordem,
			    vc.COD_COBRANCA_PLANO as cod,
                           'X' as ref
		      FROM
                            faturamento as f INNER JOIN
                             valorescobranca as vc on (f.CATALOGO_ID =vc.id) inner join
                             fatura fa on (f.FATURA_ID=fa.ID)
                      WHERE
                            f.FATURA_ID= {$id} and
                            f.TIPO =2 and
                            f.TABELA_ORIGEM='valorescobranca'	{$condicao}
                             and f.CANCELADO_POR is NULL
                     UNION
                      SELECT
                            f.*,
                            fa.OBS,
			    vc.item AS principio,
		           ('Servi&ccedil;os') AS apresentacao,
                            1 as ordem,
			    '' as cod,
                           'X' as ref
		      FROM
                            faturamento as f INNER JOIN
                             cobrancaplanos as vc on (f.CATALOGO_ID =vc.id) inner join
                             fatura fa on (f.FATURA_ID=fa.ID)
                      WHERE
                            f.FATURA_ID= {$id} and
                            f.TIPO =2 and
                            f.TABELA_ORIGEM='cobrancaplano'	{$condicao}
			     and f.CANCELADO_POR is NULL
			UNION
			SELECT DISTINCT
			                 f.*,
                                         fa.OBS,
                                        CO.item AS principio,

                                        ('Equipamentos') AS apresentacao,
                                            2 as ordem,
                                         '' as cod,
                           'X' as ref

			FROM
                                    faturamento as f INNER JOIN
                                    cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)
                                    inner join fatura fa on (f.FATURA_ID=fa.ID)

		      WHERE
                                    f.FATURA_ID= {$id} and
                                    f.TIPO =5  and
                                    f.TABELA_ORIGEM='cobrancaplano' {$condicao}
                                     and f.CANCELADO_POR is NULL

                        UNION
			SELECT DISTINCT
			                 f.*,
                                         fa.OBS,
                                        CO.DESC_COBRANCA_PLANO AS principio,

                                        ('Equipamentos') AS apresentacao,
                                            2 as ordem,
                                         CO.COD_COBRANCA_PLANO as cod,
                           'X' as ref

			FROM
                                    faturamento as f INNER JOIN
                                    valorescobranca CO ON (f.CATALOGO_ID = CO.`id`)
                                    inner join fatura fa on (f.FATURA_ID=fa.ID)

		      WHERE
                                    f.FATURA_ID= {$id} and
                                    f.TIPO =5  and
                                    f.TABELA_ORIGEM='valorescobranca' {$condicao}
                                     and f.CANCELADO_POR is NULL


		UNION
			SELECT DISTINCT
			                 f.*,fa.OBS,
					 CO.item AS principio,

					  ('Gasoterapia') AS apresentacao,
					 6 as ordem,
					'' as cod,
                           'X' as ref

				FROM
                                        faturamento as f INNER JOIN
                                        cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)
                                        inner join fatura fa on (f.FATURA_ID=fa.ID)
				WHERE
					     f.FATURA_ID= {$id} and
					     f.TIPO =6  and f.TABELA_ORIGEM='cobrancaplano' {$condicao}
                                             and f.CANCELADO_POR is NULL
              UNION
			SELECT DISTINCT
			                 f.*,fa.OBS,
					 vc.DESC_COBRANCA_PLANO AS principio,

					  ('Gasoterapia') AS apresentacao,
					 6 as ordem,
					 vc.COD_COBRANCA_PLANO as cod,
                           'X' as ref

				FROM
                                        faturamento as f  inner join
                                        valorescobranca as vc on (vc.id = f.CATALOGO_ID) inner join
                                        fatura fa on (f.FATURA_ID=fa.ID)
				WHERE
					     f.FATURA_ID= {$id} and
					      f.TIPO =6  and
                                              f.TABELA_ORIGEM='valorescobranca' {$condicao}
                                               and f.CANCELADO_POR is NULL

	ORDER BY
        ordem,
        tipo,
        principio";

		$faturas.= "<table  width=100% class='mytable'>";
		/*<tr bgcolor='#EEEEEE'>
		<td>N</td>
		<td>Item</td>
		<th>Qtd</th>
		<th>Pre&ccedil;o</th>
		
		
		</tr>";*/
		$result = mysql_query($sql);
		$total=0.00;
		$cont_cor=0;
		$subtotal=0.00;
		$subtotal_equipamento=0.00;
		$subtotal_material=0.00;
		$subtotal_medicamento=0.00;
		$subtotal_gas=0.00;
		$subtotal_dieta=0.00;
		
		$subtotal_servico=0.00;
	     $cont1=0;
	     $cont2=0;
	     $cont3=0;
	     $cont4=0;
	     $cont5=0;
	     $cont6=0;
             $desconto =0;
             $acrescimo =0;
		while($row = mysql_fetch_array($result)){
                     $desconto += $row['DESCONTO_ACRESCIMO']<= 0 ? ($row['DESCONTO_ACRESCIMO']/100)*($row['QUANTIDADE']*$row['VALOR_FATURA']):0.00;
                        $acrescimo += $row['DESCONTO_ACRESCIMO']>= 0 ? ($row['DESCONTO_ACRESCIMO']/100)*($row['QUANTIDADE']*$row['VALOR_FATURA']):0.00;
			
			$subtotal +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
			
			
			
			if($row['TIPO'] == '6'){
				$subtotal_gas +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
				$parcial=$subtotal_gas;
				$cont5++;
				$n=$row['cod'];
				$tip = $row['TIPO'];
				$tipo_tot="TOTAL GASOTERAPIA";
			
			}
			
			if($row['TIPO'] == '5'){
				$subtotal_equipamento +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
				$parcial=$subtotal_equipamento;
				$cont4++;
				$n=$row['cod'];
				$tip = $row['TIPO'];
				$tipo_tot="TOTAL EQUIPAMENTO";
				
			}
if($row['TIPO'] == '3'){
				$subtotal_dieta +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
				$cont6++;
				$n="Bras. ".$row['NUMERO_TISS'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_dieta;
				$tipo_tot="TOTAL DIETA";
			}
			if($row['TIPO'] == '2'){
				$subtotal_servico +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
				$cont3++;
				$n=$row['cod'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_servico;
				$tipo_tot="TOTAL SERVI&Ccedil;O";
			}
			if($row['TIPO'] == '1'){
				$subtotal_material +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
				$cont1++;
				$n="Simp. ".$row['NUMERO_TISS'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_material;
				$tipo_tot="TOTAL MATERIAL";
			}
			if($row['TIPO'] == '0'){
				$subtotal_medicamento +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
				$cont2++;
				$n="Bras. ".$row['NUMERO_TISS'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_medicamento;
				$tipo_tot="TOTAL MEDICAMENTO";
			}
			
			if( $cont_cor%2 ==0 ){
				$cor = '';
			}else{
				$cor="bgcolor='#EEEEEE'";
			}
			
			
		if($row['UNIDADE']=='1')
		  {     $und='Comprimido';
		     }
		     
		     if($row['UNIDADE']=='2')
		     {
		     	$und='Caixa';
		     }
		     
		     if($row['UNIDADE']=='3')
		     {
		     	$und='Frasco';
		     }
		     if($row['UNIDADE']=='4')
		     {
		     	$und='Ampola';
		     }
		     if($row['UNIDADE']=='5')
		     {
		     	$und='Bisnaga';
		     }
		     if($row['UNIDADE']=='6')
		     {
		     	$und='Sach&ecirc;';
		     }
		     if($row['UNIDADE']=='7')
		     {
		     	$und='Flaconete';
		     }
		     if($row['UNIDADE']=='8')
		     {
		     	$und='Pacote';
		     }
		     if($row['UNIDADE']=='9')
		     {
		     	$und='Di&aacute;ria';
		     }
		     if($row['UNIDADE']=='10')
		     {
		     	$und='Sess&atilde;o';
		     }
		     if($row['UNIDADE']=='11')
		     {
		     	$und='Visita';
		     }if($row['UNIDADE']=='12')
		     {
		     	$und='Unidade';
		     }if($row['UNIDADE']=='13')
		     {
		     	$und='Lata';
		     }
		     
			
		     if($cont5 == 1){
		     	if($tip != $tipo && $cont_cor != 0){
		     		$faturas.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
					$faturas.="<tr ><td colspan='6'><td></tr>";
		     	}
		     	$faturas.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;border:1px solid #000;'>Gasoterapia</td></tr>";
		     	$faturas.= "<tr bgcolor=bgcolor='#cccccc'>
		     	<td style='font-size:12px;float:right;'>N&deg;</td>
		     	<td style='font-size:12px;float:right;'>Unidade</td>
		     	<td style='font-size:12px;float:right;'>Item</td>
		     	<th style='font-size:12px;float:right;'>Qtd</th>
		     	<th style='font-size:12px;float:right;'>Pre&ccedil;o Unt.</th>
		     	<th style='font-size:12px;float:right;'>Pre&ccedil;o Tot.</th>
		     	</tr>";
		     
		     
		     }
			if($cont4 == 1){
				if($tip != $tipo && $cont_cor != 0){
					$faturas.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr><br/>";
					$faturas.="<tr ><td colspan='6'><td></tr>";
				}
				$faturas.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>MATERIAIS PERMANENTES</td></tr>";
				$faturas.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
				</tr>";
				
			
			}
			if($cont6 == 1 ){
				if($tip != $tipo && $cont_cor != 0){
					$faturas.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;bgcolor:#EEEEEE;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr><br/>";
					$faturas.="<tr ><td colspan='6'><td></tr>";
				}
				$faturas.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>DIETA</td></tr>";
				$faturas.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
			
			
				</tr>";
			
			}
			if($cont3 == 1 ){
				if($tip != $tipo && $cont_cor != 0){
					$faturas.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;bgcolor:#EEEEEE;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr><br/>";
					$faturas.="<tr ><td colspan='6'><td></tr>";
				}
				$faturas.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>SERVI&Ccedil;OS</td></tr>";
				$faturas.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
			
			
				</tr>";
			
			}
			if($cont1 == 1){
				if($tip != $tipo && $cont_cor != 0){
					$faturas.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;bgcolor:#EEEEEE;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr><br/>";
					$faturas.="<tr ><td colspan='6'><td></tr>";
				}
				$faturas.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>MATERIAIS</td></tr>";
				$faturas.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot. </th>
				</tr>";
			
			}
			if($cont2 == 1){
				if($tip != $tipo && $cont_cor != 0 ){
					$faturas.= "<tr><td colspan='6'  style='font-size:12px;float:right bgcolor:#EEEEEE;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr><br/>";
					$faturas.="<tr><td colspan='6'><td></tr>";
				}
				$faturas.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE; '>MEDICAMENTOS</td></tr>";
				$faturas.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
				</tr>";
			
			}
			
			
			//
			 $tipo = $tip;
			$par= $parcial;
			$tipo_tot1=$tipo_tot;
                        if($row['ref']== 'I'){
                              $n = "INTERNO";
                 
                              }
			$faturas.= "<tr $cor>
			<td>{$n}</td>
			<td>{$und}</td>
			<td width='50%'>".strtoupper(strtr($row['principio'] ,"áéíóúâêôãõàèìòùç","ÁÉÍÓÚÂÊÔÃÕÀÈÌÒÙÇ"))."</td>
			<td>{$row['QUANTIDADE']}</td>
			<td>R$ ".number_format($row['VALOR_FATURA'],2,',','.')."</td>";
			$vt=$row['VALOR_FATURA']*$row['QUANTIDADE'];
			$vtotal += $vt; 
			$faturas.= "<td>R$ ".number_format($vt,2,',','.')."</td>
			</tr>";
			
			if($cont4 == 1){
				$cont4++;
			
			}
			if($cont3 == 1){
				$cont3++;
			
			}
			if($cont1 == 1){
				$cont1++;
			
			}
			if($cont2 == 1){
				$cont2++;
			
			}
			$cont_cor++;
			$obs=$row['OBS'];
		}
		
		$faturas.= "<tr ><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
		$faturas.="<tr><td colspan='6'><td></tr>";
		$faturas.= "<tr bgcolor='#cccccc' ><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>Valor Parcial: R$</b> <b><i>".number_format($vtotal,2,',','.')."</i></b></td></tr>";
                $faturas.= "<tr><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>Desconto:  R$ </b><b><i>".number_format(-1*$desconto,2,',','.')."</i></b></td></tr>";   
                $faturas.= "<tr><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>Acrescimo:  R$ </b><b><i>".number_format($acrescimo,2,',','.')."</i></b></td></tr>";
                $faturas.= "<tr bgcolor='#cccccc' ><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>Total: R$</b> <b><i>".number_format($vtotal + $acrescimo + $desconto,2,',','.')."</i></b></td></tr>";
		$faturas.= "<tr bgcolor='#cccccc' ><td colspan='6'  ><b>OBSERVA&Ccedil;&Atilde;O: </br> {$obs}</td></tr>";
		
		$faturas.= "</table>";
		$faturas.= "</form>";
		return $faturas;
	
	}
	
	
	
	
	public function detalhe_lote($id){
		$html .="<form>";
		$html .= "<h1><a><img src='../utils/logo2.jpg' width='200' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> Lote # {$id} </h1>";
		$html .= $this->cabecalho($id);
		$html .="<br></br>";

	  $html.= "<table  width=100% class='mytable'>";

    $result = LoteFatura::getDetalhe($id, true);

		$total_geral= 0.00;
		$paciente = 0;
		$cont = 0;
		$cont_cor = 0;
		while ($row = mysql_fetch_array($result)) {
		$total_geral += $row['total'];
		$total = $row['total'];
		$data = join("/",array_reverse(explode("-",$row['DATA_INICIO'])))." AT&Eacute; ".join("/",array_reverse(explode("-",$row['DATA_FIM'])));
			if ($cont_cor % 2 == 0) {
				$cor = '';
			} else {
				$cor="bgcolor='#EEEEEE'";
			}

			if($paciente != $row['PACIENTE_ID']  ){
				$html.= "<tr><td colspan='5' bgcolor='#D3D3D3' style='font-size:12px;float:right;'><b>Paciente: ".strtoupper($row['nome'])."</b></td></tr>";
				$html.= "<tr><td colspan='5' bgcolor='#E8E8E8' style='font-size:12px;float:right;text-align:right;'><b>FATURA
				".join("/",array_reverse(explode("-",$row['DATA_INICIO'])))." AT&Eacute; ".join("/",array_reverse(explode("-",$row['DATA_FIM'])))."
				TOTAL: R$</b> <b><i>".number_format($total,2,',','.')."</i></b></td></tr>";
			}else{
			$html.= "<tr><td colspan='5' bgcolor='#E8E8E8' style='font-size:12px;float:right;text-align:right;'><b>FATURA
				".join("/",array_reverse(explode("-",$row['DATA_INICIO'])))." AT&Eacute; ".join("/",array_reverse(explode("-",$row['DATA_FIM'])))."
				TOTAL: R$</b> <b><i>".number_format($total,2,',','.')."</i></b></td></tr>";
			}
			$paciente = $row['PACIENTE_ID'];
			
			$cont++;
			$cont_cor++;
			
		}
		
		
		
		$html.= "</table><br>";
		$html.= "<p style='font-size:12px;float:right;border:1px solid #000;text-align:right;'><b>TOTAL: R$</b><b><i>".number_format($total_geral,2,',','.')."</i></b></p>";
		
		
	$paginas []= $header.$html.= "</form>";
	
	///////////faturas//////////////
	$result2 = Fatura::getByCapaLote($id, true);
	
	$f1=0; //contador de faturas
	while($row2 = mysql_fetch_array($result2)){
	//$faturas .= $this->cabecalho_fatura($row2['ID']);
	if($f1==0){
	$faturas[$f1++]=$html;
	}

	$avaliacao = $row2['ORCAMENTO'];

	
	$faturas[$f1++] = $this->detalhe_fatura($row2['ID'],$avaliacao,1);
	
	//$faturas .= $mpdf->addPage('pt','A4',9);
	///$faturas .=$mpdf->addPage('pt','A4',9);
		//$faturas .= "<form><p>{$row2["ID"]} teste</p></form>";
		
	}
	//print_r($faturas);
	
	/////////////////fim detalhes faturas
	//print_r($paginas2);
	//print_r($paginas);
	//$paginas2[] = $header.$faturas;
	$mpdf=new mPDF('pt','A4',9);
	$mpdf->SetHeader('página {PAGENO} de {nbpg}');
	$ano = date("Y");
	$mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
	$mpdf->WriteHTML("<html><body>");
	/*$flag = false;
	foreach($paginas as $pag){
		if($flag) $mpdf->WriteHTML("<formfeed>");
		$mpdf->WriteHTML($pag);
		$flag = true;
	}*/
	
	//$mpdf->WriteHTML("</body></html>");
/*	$mpdf->addPage('pt','A4',9);
	$ano = date("Y");*/
	//$mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
	//$mpdf->WriteHTML($paginas2);
  
         $flag2 = false;
		 $cont=0;
 foreach($faturas as $pag2){
		if($flag2) $mpdf->WriteHTML("<formfeed>");
		$mpdf->WriteHTML($pag2);
		$cont++;
		
		$flag2 = true;
	}
	//$mpdf->WriteHTML("<form><p>{$cont}</p></form>");
	//$mpdf->WriteHTML($paginas2);

  $mpdf->WriteHTML("</body></html>");
  
	$mpdf->Output('capa_lote.pdf','I');
	exit;
	
	
	
	/*$mpdf=new mPDF('pt','A4',9);
	$mpdf->WriteHTML($html);
	$mpdf->Output('pacientes.pdf','I');*/
	
	
	
	}





}




$p = new imprimir_lote();
$p->detalhe_lote($_GET['id']);






?>