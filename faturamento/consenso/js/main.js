$(function($){
    $(".chosen-select").chosen({search_contains: true});
    $("#consenso-pesquisar-fatura").click(function(){
        if(!validar_campos('div-pesquisar-fatura')){
            return false;
        }

    });

    $(".criar-consenso").live('click',function(){
        var idFatura = $(this).attr('id-fatura');
       window.location.href="?action=formGerarConsenso&idFatura="+idFatura;


    });

    $('.valor').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $(".editar-consenso").change(function () {
        var faturamentoId = $(this).attr('faturamento-id');
        var target =  $(this).closest('td');

        if($(this).val() == 'S'){
            target.find('.habilitar').attr('disabled',false);
            target.find('.habilitar').attr('required',true);
            target.find('.habilitar').addClass('OBG');
        }else{
            target.find('.habilitar').each(function () {
                target.find('.habilitar').attr('required',false);
                target.find('.habilitar').attr('disabled',true);
                target.find('.habilitar').removeClass('OBG');

            });
            $('.valor').priceFormat({
                prefix: '',
                centsSeparator: ',',
                thousandsSeparator: '.'
            });
            //target.find('.habilitar').attr('disabled',true);

        }



    });

    $("#adicionar-item").on('click', function () {
        var nome, catalogo_id, tiss, tipo, qtd, tipoList, $buscar, $qtd, valor, tuss, custo, optionUnidade, tabelaOrigem;
        $buscar = $("#buscar-item");
        $qtd = $("#qtd-item");
        tipoList = {
            0: 'Medicamento',
            2: 'Serviço',
            1: 'Material',
            3: 'Dieta',
            5: 'Equipamento',
            6: 'Gasoterapia',
            12: 'Extra'
        };
        if ($qtd.val() > 0 && typeof $buscar.attr('catalogo-id') != 'undefined') {
            nome = $buscar.val();
            catalogo_id = $buscar.attr('catalogo-id');
            tipo = $buscar.attr('item-tipo');
            tiss = $buscar.attr('item-tiss');
            tuss = $buscar.attr('item-tuss');
            valor = $buscar.attr('item-valor');
            custo = $buscar.attr('item-custo');
            qtd = $qtd.val();
            tabelaOrigem = $buscar.attr('item-tabela-origem');
            optionUnidade = $('#option-unidade').html();


            $("#itens-fatura").append(
                '<tr>' +
                '   <td>' +
                '<div class="row">' +

                    '<div class="col-lg-2">' +
                         '<label>Remover</label> <br>'+
                        '<span title="Remover Item" style="font-size: 6px" class="btn btn-danger remover-item-add-item glyphicon glyphicon-remove"></span>'+
                    '</div> '+
                    '<div class="col-lg-8">' +
                        '<label>Item</label><br>'+
                        'TUSS-' + tuss + ' N-' + tiss +
                        nome +
                    '</div> '+
                    '<div class="col-lg-2"><br>' +
                        '<label>Tipo</label><br>'+
                        tipoList[tipo] +
                    '</div> '+
                    '<div class="col-lg-3">' +
                    '<label>Unidade</label><br>'+
                    '<select class="form-control OBG" required name="novoItemUnidade[]">'+
                        optionUnidade+
                    '</select>'+
                    '</div> '+
                    '<div class="col-lg-3">' +
                    '<label>Quantidade</label><br>'+
                    '<input type="number" class="form-control" value="'+ qtd +'" name="novoItemQtd[]"/> ' +
                    '</div> '+
                    '<div class="col-lg-3">' +
                    '<label>Valor</label><br>'+
                    '<input type="text" class=" valor form-control" value="'+ valor+'" name="novoItemValor[]"/> ' +
                    '</div> '+
                    '<div class="col-lg-3">' +
                    '<label>Taxa</label><br>'+
                    '<input type="text"  class="valor form-control" value="'+ 0 +'" name="novoItemTaxa[]"/> ' +
                    '</div> '+


                '<input name="novoItemCatalogoId[]" type="hidden" value="' + catalogo_id + '">' +
                '<input name="novoItemTiss[]" type="hidden" value="' + tiss + '">' +
                '<input name="novoItemTuss[]" type="hidden" value="' + tuss + '">' +
                '<input name="novoItemTipo[]" type="hidden" value="' + tipo + '">' +
                '<input name="novoItemCusto[]" type="hidden" value="' + custo + '">' +
                '<input name="novoItemTabela[]" type="hidden" value="' + tabelaOrigem + '">' +
                '</div>'+
                '   </td>' +

                '   </tr>'
            );
            $('.valor').priceFormat({
                prefix: '',
                centsSeparator: ',',
                thousandsSeparator: '.'
            });


            $buscar.removeAttr('catalogo-id item-tipo item-tiss').val('');
            $("#qtd-item").val('');
            return false;
        }
        alert('Escolha um item e coloque uma quantidade maior do que zero!');
    });

    $(".remover-item-add-item").live('click',function(){
        $(this).closest('tr').remove();
    });

    $("#salvar-consenso-faturamento").click(function () {
        var cont = 0;
        $(".editar-consenso").each(function () {
            $(this).val() == 'S'  ? cont++ : cont;
        });

        if(cont == 0 && $('.remover-item-add-item').length == 0){
            alert('Para salvar um consenso ao menos um item deve ter sido editado ou inserido!!');
            return;
        }

        if(validar_campos('form-consenso')){
            var data = $("#form-consenso").serialize();
            $.ajax({
                url: "/faturamento/consenso/?action=salvarConsensoFatura",
                method: 'POST',

                data: data,
                success: function (response) {
                    if(response == 1){
                        alert('Consenso salvo com sucesso');
                        window.location.href="?action=consensoPesquisarFatura";
                    }else{
                        alert('Entre em contato com TI. '+ response);
                        return false;
                    }



                }
            });

        }
    });




});
