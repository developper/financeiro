<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';


define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);


$routes = [
    "GET" => [
        '/faturamento/consenso/?action=consensoPesquisarFatura' => '\App\Controllers\ConsensoDeFaturamento::pesquisarFatura',
        '/faturamento/consenso/?action=formGerarConsenso' => '\App\Controllers\ConsensoDeFaturamento::formGerarConsenso',


    ],
    "POST" => [
        '/faturamento/consenso/?action=consensoPesquisarFatura' => '\App\Controllers\ConsensoDeFaturamento::pesquisarFatura',
        '/faturamento/consenso/?action=salvarConsensoFatura' => '\App\Controllers\ConsensoDeFaturamento::salvarConsensoFatura',



    ]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
    $app = new App\Application;
    $app->setRoutes($routes);
    $app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
    echo $e->getMessage();
} catch(App\NotFoundException $e) {
    http_response_code(404);
    echo $e->getMessage();
}