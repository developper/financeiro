<?php
require_once('../db/config.php');

$like = $_GET['term'];
$plano = $_GET['plano'];
$empresa = $_GET['empresa'];
$tipoMedicamento = $_GET['tipo_medicamento'];
$tipo_documento = $_GET['tipo_documento'];

$sql = <<<SQL
SELECT
  planosdesaude.tipo_medicamento
FROM
  planosdesaude
WHERE 
  id = '{$plano}'
SQL;
$rs = mysql_query($sql);
$rowPlano = mysql_fetch_array($rs);

$condCusto ='';
$cond2="AND ATIVO = 'A'";
if($tipo_documento === '0'){
    $condCusto = "AND valorMedio > 0";
    $cond2="AND (ATIVO = 'A' OR descontinuado = 'S')";
}

if (isset($_REQUEST['tipo']) && empty($_REQUEST['tipo'])) {
  header('Content-type: application/json');
  die(json_encode(['message' => 'Tipo é obrigatório!']));
}

$condlike = "1";
$flag = true;
$strings = explode(" ",$like);
if(isset($_REQUEST['tipo'])) $_GET['tipo'] = $_REQUEST['tipo'];
$comparar = "concat('(',principio_ativo.principio,') ',catalogo.principio,' ',catalogo.apresentacao)";
if(isset($_GET['tipo']) && ($_GET['tipo'] == '2' || $_GET['tipo'] == 'kits')) $comparar = "k.nome";
if(isset($_GET['tipo']) && ($_GET['tipo'] == 'serv')) $comparar = "V.DESC_COBRANCA_PLANO";
if(isset($_GET['tipo']) && ($_GET['tipo'] == 'eqp')) $comparar = "V.DESC_COBRANCA_PLANO";
if(isset($_GET['tipo']) && ($_GET['tipo'] == 'gas')) $comparar = "V.DESC_COBRANCA_PLANO";

if(isset($_GET['tipo']) && ($_GET['tipo'] == 'ext')) $comparar = "V.DESC_COBRANCA_PLANO";
if(isset($_GET['tipo']) && ($_GET['tipo'] ==  'med_pedido' || $_GET['tipo'] ==  'die_pedido' || $_GET['tipo'] ==  'mat_pedido')) $comparar = "principio";
//verificar comparação de equipamento
foreach($strings as $str){
	if($str !=  "") $condlike .= " AND {$comparar} LIKE '%$str%' ";
}
$tipo = "1";
$t =  "-1";

if(isset($_GET['tipo'])){
	if($_GET['tipo'] ==  '0' || $_GET['tipo'] == 'med' || $_GET['tipo'] ==  'med_pedido')
		$t = "0";
	elseif($_GET['tipo'] ==  '1' || $_GET['tipo'] == 'mat' || $_GET['tipo'] ==  'mat_pedido')
			$t = "1";
	elseif($_GET['tipo'] ==  'serv' )
		$t = "2";
	elseif($_GET['tipo'] ==  'eqp'  || $_GET['tipo'] ==  'eqp_pedido' )
		$t = "5";
	elseif($_GET['tipo'] ==  'gas' )
		$t = "6";
	elseif($_GET['tipo'] ==  '3' || $_GET['tipo'] == 'die' || $_GET['tipo'] ==  'die_pedido')
		$t = "3";
	elseif($_GET['tipo'] ==  'ext')
		$t  = "12";
    elseif($_GET['tipo'] ==  'kit')
        $t  = "kit";
}

$itens = array();
$result = "";
$sql = "";

$tipo_aba = $t;
if($t == "1" || $t == "-1" || $t == "3")
{
	$tipo = " tipo = {$t} ";
	if($t ==  "-1")
		$tipo = "1";

            $t = 90;
	$sql = "SELECT
concat(catalogo.principio,' ',catalogo.apresentacao)as name,
apresentacao,
NUMERO_TISS as cod,
DESC_MATERIAIS_FATURAR,
valorMedio as custo,
catalogo.ID as catalogo_id,
VALOR_VENDA as val,
TUSS as codTuss
FROM
catalogo
WHERE
  {$tipo}
   AND concat(catalogo.principio,' ',catalogo.apresentacao) like '%$str%'
  $cond2
  $condCusto
 ORDER BY
 catalogo.principio,
 catalogo.apresentacao";
}elseif($t == '0'){
    $condGenerico = $rowPlano['tipo_medicamento'] == 'G' ?
        " AND ( catalogo.GENERICO = 'S' OR catalogo.possui_generico = 'N' ) " :
        "";
    $sql = "SELECT
concat('(',principio_ativo.principio,') ',catalogo.principio,' ',catalogo.apresentacao)as name,
apresentacao,
NUMERO_TISS as cod,
DESC_MATERIAIS_FATURAR,
valorMedio as custo,
catalogo.ID as catalogo_id,
VALOR_VENDA as val,
TUSS as codTuss,
GENERICO
FROM
catalogo
LEFT JOIN catalogo_principio_ativo ON catalogo.ID = catalogo_principio_ativo.catalogo_id
LEFT JOIN principio_ativo ON principio_ativo.id = catalogo_principio_ativo.principio_ativo_id
 WHERE
  tipo = 0
   AND concat('(',principio_ativo.principio,') ',catalogo.principio,' ',catalogo.apresentacao) like '%$str%'
   {$condGenerico}
  $cond2
  $condCusto
 ORDER BY
 catalogo.principio,
 catalogo.apresentacao";
}
elseif($t == "2") {

   $sql=" SELECT
                        IF(V.DESC_COBRANCA_PLANO IS NULL,CB.item,V.DESC_COBRANCA_PLANO)AS name,
                        IF(V.DESC_COBRANCA_PLANO IS NULL,CB.id,V.COD_COBRANCA_PLANO) AS cod,
			IF(V.DESC_COBRANCA_PLANO IS NULL,CB.id,V.id) AS catalogo_id,
			IF(V.DESC_COBRANCA_PLANO IS NULL,'cobrancaplano','valorescobranca') AS TABELA_ORIGEM,
                        'SERVI&Ccedil;OS' as apresentacao,

                        coalesce(CS.CUSTO,'0.00') as custo,
                        coalesce(V.valor,'0.00') as val,
                        '' as codTuss
                 FROM
			cobrancaplanos as CB LEFT JOIN
            custo_servicos_plano_ur as CS on ( CB.id = CS.COBRANCAPLANO_ID and CS.UR={$empresa} and CS.STATUS = 'A') LEFT JOIN
			valorescobranca as V on (CB.id=V.idCobranca and
                        V.idPlano = {$plano} AND V.excluido_por is NULL)
                 WHERE
                        (V.DESC_COBRANCA_PLANO LIKE '%$str%' OR CB.item LIKE '%$str%')
                            
                 ORDER BY
                        name";

}
elseif($t == "5")
{
    $sql=" SELECT
                        IF(V.DESC_COBRANCA_PLANO IS NULL,CB.item,V.DESC_COBRANCA_PLANO)AS name,
                        IF(V.DESC_COBRANCA_PLANO IS NULL,CB.id,V.COD_COBRANCA_PLANO) AS cod,
			IF(V.DESC_COBRANCA_PLANO IS NULL,CB.id,V.id) AS catalogo_id,
			IF(V.DESC_COBRANCA_PLANO IS NULL,'cobrancaplano','valorescobranca') AS TABELA_ORIGEM,
                        'EQUIPAMENTO' as apresentacao,

                        coalesce(CS.CUSTO,'0.00') as custo,
                        coalesce(V.valor,'0.00') as val,
                        '' as codTuss
                 FROM
			cobrancaplanos as CB LEFT JOIN
                        custo_servicos_plano_ur as CS on ( CB.id=CS.COBRANCAPLANO_ID and CS.UR={$empresa} and CS.STATUS = 'A') LEFT JOIN
			valorescobranca as V on (CB.id=V.idCobranca and
                        V.idPlano = {$plano} AND V.excluido_por is NULL )
                 WHERE
                        (V.DESC_COBRANCA_PLANO LIKE '%$str%' OR CB.item LIKE '%$str%') and CB.tipo=1
                            
                 ORDER BY
                        name";
}
elseif($t == "6")
{
     $sql=" SELECT
                        IF(V.DESC_COBRANCA_PLANO IS NULL,CB.item,V.DESC_COBRANCA_PLANO)AS name,
                        IF(V.DESC_COBRANCA_PLANO IS NULL,CB.id,V.COD_COBRANCA_PLANO) AS cod,
			IF(V.DESC_COBRANCA_PLANO IS NULL,CB.id,V.id) AS catalogo_id,
			IF(V.DESC_COBRANCA_PLANO IS NULL,'cobrancaplano','valorescobranca') AS TABELA_ORIGEM,
                        'GASOTERAPIA' as apresentacao,

                        coalesce(CS.CUSTO,'0.00') as custo,
                        coalesce(V.valor,'0.00') as val,
                        '' as codTuss
                 FROM
			cobrancaplanos as CB LEFT JOIN
                        custo_servicos_plano_ur as CS on ( CB.id=CS.COBRANCAPLANO_ID and CS.UR={$empresa} and CS.STATUS = 'A') LEFT JOIN
			valorescobranca as V on (CB.id=V.idCobranca and
                        V.idPlano = {$plano} AND V.excluido_por is NULL)
                 WHERE
                        (V.DESC_COBRANCA_PLANO LIKE '%$str%' OR CB.item LIKE '%$str%') and CB.id in (28,29,30)
                            
                 ORDER BY
                        name";
	//$sql = "SELECT C.item as name,'GASOTERAPIA' as apresentacao ,C.id as cod, CUSTO as custo,C.id  as catalogo_id FROM cobrancaplanos as C WHERE {$condlike} and C.id in(28,29,30) ORDER BY C.item";
}
elseif ($t == "12")
{
	$sql ="SELECT
                        IF(V.DESC_COBRANCA_PLANO IS NULL,CB.item,V.DESC_COBRANCA_PLANO)AS name,
                        IF(V.DESC_COBRANCA_PLANO IS NULL,CB.id,V.COD_COBRANCA_PLANO) AS cod,
			IF(V.DESC_COBRANCA_PLANO IS NULL,CB.id,V.id) AS catalogo_id,
			IF(V.DESC_COBRANCA_PLANO IS NULL,'cobrancaplano','valorescobranca') AS TABELA_ORIGEM,
                        'SERVI&Ccedil;OS' as apresentacao,
                        coalesce(CS.CUSTO,'0.00') as custo,
                        coalesce(V.valor,'0.00') as val,
                        '' as codTuss
                 FROM
			cobrancaplanos as CB LEFT JOIN
                        custo_servicos_plano_ur as CS on ( CB.id=CS.COBRANCAPLANO_ID and CS.UR={$empresa} and CS.STATUS = 'A') LEFT JOIN
			valorescobranca as V on (CB.id=V.idCobranca and
                        V.idPlano = {$plano}  AND V.excluido_por is NULL )
                 WHERE
                        (V.DESC_COBRANCA_PLANO LIKE '%$str%' OR CB.item LIKE '%$str%')
                            and CB.tipo=2
                           
                 ORDER BY
                        name";
} elseif ($t == 'kit') {
    $sql = <<<SQL
SELECT
  UPPER(nome) AS name,
  idKit AS cod,
  idKit AS catalogo_id,
  'kitsmaterial' AS TABELA_ORIGEM,
  'KIT' as apresentacao,
  '0.00' as custo,
  '0.00' as val,
  COD_REF as codTuss
FROM
  kitsmaterial
WHERE 
  nome LIKE '%{$str}%'
SQL;

}



$result = mysql_query($sql);

if($row['tipo']== 1 && $row['DESC_MATERIAIS_FATURAR']!= NULL ){
        $nome=$row['DESC_MATERIAIS_FATURAR'];
}else{
     $nome=$row['name'];
}

while ($row = mysql_fetch_array($result)) {
    $textRed= 'N';
    $val=number_format($row['val'], 2, ',', '.');
    $nome=$row['cod']."  ".$row['name'];
	$item['value'] = $nome;
	$item['id'] = $row['cod'];
	$item['cod_ref'] = $row['cod_ref'];
	$item['apresentacao'] = $row['apresentacao'];
	$item['custo'] = $row['custo'];
	$item['numero_tiss'] = $row['numero_tiss'];
	$item['tipo_aba'] = $tipo_aba;
    $item['catalogo_id']=$row['catalogo_id'];
	$item['valor_receber']= $val;
    $item['tabela_origem'] = $row['TABELA_ORIGEM'];
    $item['tuss'] = $row['codTuss'];
    if($tipoMedicamento == 'G' && $row['GENERICO'] != 'S' && $row['tipo']== 0 )
        $textRed = 'S';
    $item['textRed'] = $textRed;

	array_push($itens, $item);
}


echo json_encode($itens);

?>