$(function($){

  function ExitPageConfirmer(message) {
    this.message = message;
    this.needToConfirm = false;
    var myself = this;
    window.onbeforeunload = function() {
      if (myself.needToConfirm) {
        return myself.message;
      }
    }
  }

    $('#cpf-avulsa').mask('999.999.999-99');

    $('#cpf-avulsa').blur(function(){
        var cpf = $(this).val(),
            isValid;
        if(cpf != '') {
            isValid = verificarCPF(cpf);

            if (!isValid) {
                alert('CPF inválido!');
                $(this).val('');
            }
        }
    });

    function verificarCPF(strCPF) {
        var soma = 0;
        var resto;
        var exp = /\.|\-/g
        strCPF = strCPF.toString().replace( exp, "" );
        if (strCPF == "00000000000")
            return false;
        for (i = 1; i <= 9; i++)
            soma = soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(strCPF.substring(9, 10)) )
            return false;
        soma = 0;
        for (i = 1; i <= 10; i++)
            soma = soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(strCPF.substring(10, 11) ) )
            return false;
        return true;
    }

  var exitPage = new ExitPageConfirmer('Deseja realmente sair dessa página? os dados não salvos serão perdidos!');

  $('.numerico').keyup(function(e){
    this.value = this.value.replace(/\D/g,'');
  });

  $('.excluir-documento-salvo').live('click',function(){
    id_fatura= $(this).attr('idfatura');
    var elemento = this;
    if(confirm('Deseja realmente excluir esse documento?')){
      $.post("inserir.php", {query: 'cancelar-fatura', id_fatura:id_fatura
      }, function(r){
        if(r == 1){

          $(elemento).closest('center').remove();
          alert('Excluido com Sucesso!');


        }else{
          alert(r);
        }
      });
    }

    return false;

  });




    $("#dialog-motivo-ajuste").dialog({
        autoOpen: false,
        modal: true,
        width: 500,
        buttons: {

            "Fechar": function () {
                $("#div-motivo-ajuste").html('');
                $(this).dialog('close');
            }
        }
    });

    $(".prescricao-motivo-ajuste").live('click',function(){

        var motivoAjuste = $(this).attr('data-motivo');
        $("#div-motivo-ajuste").html(motivoAjuste);
        $("#dialog-motivo-ajuste").dialog('open');

    });


  //$(".paciente-lote-mouse-over").live('easyTooltip',function(){});

  //$(".paciente-lote-mouse-over").live('mouseout',function(){
  //  $(this).hide();
  //});

  /*
   $("#sair-sem-salvar").click(function(){
   exitPage.needToConfirm = false;
   $.post("query.php",{query: "release-lock", p: $("#solicitacao").attr("prescricao")}, function(r){window.location = '?view=solicitacoes';});
   });
   */
// 02-02-2015'#select_item_custo_plano_ur').chosen({search_contains: true});
  $('.selecionar-todos').live('click',function(){
      var paciente = $(this).attr('paciente');
      if($(this).is(':checked')){
           $(this).attr('defaultChecked',true);
          $('.excluir-item-paciente-'+paciente).each(function(){
            $(this).attr('checked','checked');
            $(this).attr('defaultChecked',true);

          });
      }else{
          $('.excluir-item-paciente-'+paciente).each(function(){
            $(this).attr('checked',false);
            $(this).attr('defaultChecked',false);
          });
          $(this).attr('defaultChecked',false);
      }
  });

    $('.selecionar-todos-pacote').live('click',function(){
        var paciente = $(this).attr('paciente');
        if($(this).is(':checked')){
            $(this).attr('defaultChecked',true);
            $('.incluso-pacote-'+paciente).each(function(){
                $(this).attr('checked', true);
                $(this).attr('defaultChecked',true);

            });
        }else{
            $('.incluso-pacote-'+paciente).each(function(){
                $(this).attr('checked',false);
                $(this).attr('defaultChecked',false);
            });
            $(this).attr('defaultChecked',false);
        }
    });

    $('.selecionar-todos-pacote-editar').live('click',function(){
        var paciente = $(this).attr('paciente');
        if($(this).is(':checked')){
            $(this).attr('defaultChecked',true);
            $('.incluso-pacote-'+paciente).each(function(){
                if(!$(this).is(':checked')) {
                    $(this).attr('checked', true);
                    $(this).parent().parent().find('.valor_plano').attr('editado', '1');
                    $(this).attr('defaultChecked', true);
                }
            });
        }else{
            $('.incluso-pacote-'+paciente).each(function(){
                $(this).attr('checked',false);
                $(this).parent().parent().find('.valor_plano').attr('editado', '0');
                $(this).attr('defaultChecked',false);
            });
            $(this).attr('defaultChecked',false);
        }
    });

  $('.excluir-item').live('click',function(){
      if($(this).is(':checked')){
        $(this).attr('defaultChecked',true);
      }else{
        $(this).attr('defaultChecked',false);
      }
  });

  $('.excluir-todos').live('click',function(){
        var paciente = $(this).attr('cod');
    if(confirm('Deseja realmente excluir os itens selecionados?')){
      $('.excluir-item-paciente-'+paciente).each(function(){
        if($(this).is(':checked')){
          $(this).parent().parent().remove();
          remove_item_novo();

        }
      });
    }

  });
  $(".ocultar-devolucao").hide();
  $(".mostrar-devolucao").toggle(
      function(){

        $("#tbody-devolucao").show();

      }, function(){


        $("#tbody-devolucao").hide();

      });
  $(".ocultar-envio").hide();
  $(".mostrar-envio").toggle(
      function(){

        $("#tbody-envio").show();

      }, function(){


        $("#tbody-envio").hide();

      });
  $(".ocultar-equipamento-ativo").hide();
  $(".mostrar-equipamento-ativo").toggle(
      function(){

        $("#tbody-equipamento-ativo").show();

      }, function(){


        $("#tbody-equipamento-ativo").hide();

      });


  $('#select_paciente_avu,#select_item_custo_plano_ur' ).chosen({search_contains: true});

  $('.bt_novo_codigo').live('click', function(){
    var cod = $(this).attr('cod');
    $('#label-novo-codigo-' + cod).show();
    $(this).hide();
  });

  $('.usar_tiss').live('click', function(){
    var cod = $(this).attr('cod');
    $('#bt-novo-codigo-' + cod).show();
    $('#label-novo-codigo-' + cod).css('display', 'none');
    $('#codigo-proprio-' + cod).val('');
  });

    $('.cancelar-lote').live('click',function(){
        var lote = $(this).attr('lote');
        var vm = $(this);
        var $dialog = $("#dialog-justificativa-cancelar-lote");
        $dialog.attr("title", "Por qual motivo o lote " + lote + " está sendo cancelado?");
        $dialog.find('.jutificativa-cancelar-lote-textarea').val("").attr('lote', lote);

        $dialog.dialog({
            autoOpen: false,
            modal: true,
            width: 500,
            buttons: {
                "Salvar": function () {
                    var justificativa = $(this).find('.jutificativa-cancelar-lote-textarea').val();
                    if(justificativa != '') {
                        $.post("query.php", {
                            query: 'cancelar-lote',
                            lote: lote,
                            justificativa: justificativa
                        }, function (r) {
                            if (r == 1) {
                                alert("Lote " + lote + " cancelado com sucesso!")
                                vm.parent().parent().remove();
                            } else {
                                alert(r);
                                return false;
                            }
                        });
                        $(this).dialog("close");
                    } else {
                        alert('Favor preencher a justificativa!');
                    }
                },
                "Cancelar": function () {
                    $(this).find('.jutificativa-cancelar-lote-textarea').val('');
                    $(this).dialog('close').dialog('destroy');
                }
            }
        });
        $dialog.dialog("open");
        return false;
    });

  $(".filtrar_por").click(function(){


    if($(this).attr('t')=='plano'){
      $("#plan").show();
      $("#mudar_plano").hide();
      $("#select_plano").addClass('COMBO_OBG');
      $("#select_unidade").removeClass('COMBO_OBG');
      $("#select_unidade").removeAttr('style');
      $("#select_paciente").removeAttr('style');
      $("#select_unidade").val('');
      $("#select_paciente").val('');
      $("#select_paciente").removeClass('COMBO_OBG');
      $('#select_paciente_chosen').hide();
      $("#ure").hide();
      $("#pac").hide();
      $("#label-pac").hide();
      $("#check_plano").attr('checked',false);
      $("#select_paciente").hide();
    }else if($(this).attr('t')=='unidade'){
      $("#ure").show();

      $("#select_unidade").show();
      $("#mudar_plano").show();
      $("#plan").hide();
      $("#pac").hide();
      $("#label-pac").hide();
      $("#select_unidade").addClass('COMBO_OBG');
      $("#select_plano").removeClass('COMBO_OBG');
      $("#select_plano").removeAttr('style');
      $("#select_paciente").removeAttr('style');
      $("#select_plano").val('');
      $("#select_paciente").val('');
      $('#select_paciente_chosen').hide();
      $("#select_paciente").removeClass('COMBO_OBG');
      $("#check_plano").attr('checked',false);
      $("#select_paciente").hide();
    }else{
      $("#pac").show();
      $("#label-pac").show();
      $("#mudar_plano").show();
      $("#ure").hide();
      $("#plan").hide();
      $('#select_paciente_chosen').show();
      $("#select_paciente").addClass('COMBO_OBG');
      $("#select_unidade").removeClass('COMBO_OBG');
      $("#select_unidade").removeAttr('style');
      $("#select_plano").removeAttr('style');
      $("#select_unidade").val('');
      $("#select_plano").val('');
      $("#select_plano").removeClass('COMBO_OBG');
      $("#check_plano").attr('checked',false);
      $("#select_paciente").hide();
    }
  });



  ////////jeferson 15-10-2013 mostrar a opção de selecionar plano caso o check esteja marcado para que possa fazer consultas de pacientexplano e URxplano.
  $("#check_plano").click(function(){

    if($(this).is(":checked")){
      $("#plan").show();
      $("#select_plano").addClass('COMBO_OBG');

    }else{
      $("#plan").hide();
      $("#select_plano").removeAttr('style');
      $("#select_plano").val('');
      $("#select_plano").removeClass('COMBO_OBG');
    }

  });

  //05-01-2015
  $('#pesquisar-prescricoes-aditivas').click(function(){
    if(validar_campos('div-pesquisar-precricoes-aditivas')){

      var inicio = $('.inicio-periodo').val();
      var fim    = $('.fim-periodo').val();
      var id_paciente = $('#paciente-orcamento-aditivo').val();



      $.post("query.php", {query: 'pesquisar-prescricoes-aditivas',
        id_paciente: id_paciente,
        fim:fim,
        inicio:inicio}, function(r) {
        $('#resultado-pesquisa-prescricoes').html(r);

      });

    }else{
      return false;
    }
  });
  $('#paciente-orcamento-aditivo').chosen({search_contains: true});

  $(".inicio-periodo").datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 1,
    onClose: function(selectedDate){
      $(".fim-periodo").datepicker(
        'option',
        'minDate',
        selectedDate
      )
    }
  });

  $(".fim-periodo").datepicker({
    minDate: $(".inicio-periodo").val(),
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 1
  });





  $(".inicio-periodo-editar").datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 1,
    onClose: function(selectedDate){
      $(".fim-periodo-editar").datepicker(
          'option',
          'minDate',
          selectedDate
      )
    }
  });

  $(".fim-periodo-editar").datepicker({
    minDate: $(".inicio-periodo-editar").val(),
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 1
  });



  $('.orcar-aditivo').live('click',function(){
    var id_prescricao = $(this).attr('data-id-prescricao');
    var tipo = $(this).attr('data-tipo');
    var caminho_id_prescricao = btoa(id_prescricao);
    var caminho_tipo = btoa(tipo);
    var existe_orcamento = $(this).attr('existe-orcamento');
    if(existe_orcamento == 1){
      if(confirm("Já existe uma orçamento para essa prescrição, deseja realmente fazer outro?")){
        window.open('/faturamento/?view=orcar-aditivo&idPresc='+caminho_id_prescricao+'&tipo='+caminho_tipo,'_blank');
        //window.location.href='?view=orcar-aditivo&idPresc='+caminho_id_prescricao+'&tipo='+caminho_tipo;
      }
      return false;
    }
    window.open('/faturamento/?view=orcar-aditivo&idPresc='+caminho_id_prescricao+'&tipo='+caminho_tipo,'_blank');
  });
  $('.add-item-orcamento-aditivo').click(function(){


    $('#dialog-formulario-aditivo').attr('cod',$(this).attr('id_paciente'));
    $('#dialog-formulario-aditivo').attr('plano',$(this).attr('plano'));
    $('#dialog-formulario-aditivo').attr('empresa',$(this).attr('empresa'));
    // $('#dialog-formulario-orcamento').attr('tipo','ext');
    $('#contador').val($(this).attr('contador'));
    $('#dialog-formulario-aditivo').attr('tipo_medicamento',$(this).attr('tipo_medicamento'));

    if($(this).attr('tipo_medicamento') == 'G'){
      $('#msg-generico').append("<div style='border: 2px dotted #999'>"
          +"<span class = 'text-red'>Paciente do plano "+$(this).attr('nomeplano')+" é recomendado prescrever itens Genéricos."
          +" Na busca de medicamento deve evitar selecionar os itens em vermelho.</span></div><br>");
    }

    $('#dialog-formulario-aditivo').attr('plano',$(this).attr('plano'));

    $('#dialog-formulario-aditivo').dialog('open');


  });

  $('#dialog-formulario-aditivo').dialog('close');



  $('#dialog-formulario-aditivo').dialog({
    autoOpen: false, modal: true, width: 800, position: 'top',
    open: function(event,ui){
      $('#tipos').buttonset();
      $('#dialog-formulario-aditivo').attr('tipo', $('#tipos input[name=tipos]:checked').val());
      $('input[name=busca-item-aditivo]').val('');
      $('#nome').val('');
      $('#nome').attr('readonly','readonly');
      $('#apr').val('');
      $('#catalogo_id').val('');
      $('#apr').attr('readonly','readonly');
      $('#cod').val('');
      $('#tipo').val(0);
      $('#custo').val('');
      $('#tuss').val('');
      $('#qtd').val('');
      $('input[name=busca-orcamento]').focus();
    },
    buttons: {
      'Fechar' : function(){
        var item = $('#dialog-formulario-aditivo').attr('item');
        $('#msg-generico').html('');
        if(item != 'avulsa'){
          $('#kits-'+item).hide();
          $('#med-'+item).hide();
          $('#mat-'+item).hide();
          $('#serv-'+item).hide();
        }
        $(this).dialog('close');
      }
    }
  });
  $('input[name=busca-item-aditivo]').keypress(function(){
    $(this).autocomplete({
      source: function(request, response) {
        $.getJSON('busca_brasindice_fatura.php', {
          term: request.term,
          tipo: $('#dialog-formulario-aditivo').attr('tipo'),
          plano:$('#dialog-formulario-aditivo').attr('plano'),
          empresa:$('#dialog-formulario-aditivo').attr('empresa'),
          tipo_medicamento: $('#dialog-formulario-aditivo').attr('tipo_medicamento')

        }, response);
      },
      minLength: 3,
      select: function( event, ui ) {

        $('#busca-item-orcamento').val('');
        $('#nome').val(ui.item.value);
        $('#cod').val(ui.item.id);
        $('#tipo').val(ui.item.tipo_aba);
        $('#catalogo_id').val(ui.item.catalogo_id);
        $('#apresentacao').val(ui.item.apresentacao);
        $('#custo').val(ui.item.custo);
        $('#valor_plano').val(ui.item.valor_receber);
        $('#tabela_origem').val(ui.item.tabela_origem);
        $('#tuss').val(ui.item.tuss);
        $('#qtd').focus();
        $('#busca-item-orcamento').val('');
        return false;
      },
      extraParams: {
        tipo: 2
      }
    }).data('autocomplete').
        _renderItem = function (ul, item) {
      var cor = '';
      if (item.textRed == 'S')
        cor = 'color:red';
      return $("<li>").data('item.autocomplete', item)
          .append("<a style='" + cor + "'>" + item.value + "</a>")
          .appendTo(ul);
    }
  });

  $('#add-item-aditivo').click(function() {

    if (validar_campos('dialog-formulario-aditivo'))
    {
      var cod = $('#cod').val();
      var catalogo_id = $('#catalogo_id').val();
      var nome = $('#dialog-formulario-aditivo input[name=nome]').val();
      var tabela_origem = $('#tabela_origem').val();
      var tipo = $('#tipo').val();
      var custo = $('#custo').val();
      var contador = parseInt($('#contador').val())+1;
      if (tipo == 'med' || tipo==0) {
        tipo = 0;
        apresentacao = 'MEDICAMENTO';
      } else if (tipo == 'mat' || tipo == 1)
      {
        tipo = 1;
        apresentacao = 'MATERIAL';
      } else if (tipo == 'serv' || tipo == 12 || tipo == 'ext' || tipo == 2)
      {
        tipo = 2;
        apresentacao = 'SERVI&Ccedil;OS';
      } else if (tipo == 'eqp' || tipo == 5)
      {
        tipo = 5;
        apresentacao = 'EQUIPAMENTO';
      } else if (tipo == 'gas' || tipo == 6)
      {
        tipo = 6;
        apresentacao = 'GASOTERAPIA';
      } else if (tipo == 'die' || tipo == 3)
      {
        tipo = 3;
        apresentacao = 'DIETA';
      }
      var tuss =$('#tuss').val();
      var qtd = $('#dialog-formulario-aditivo input[name=qtd]').val();
      var paciente = $('#dialog-formulario-aditivo').attr('cod');

      var inicio = $('#inicio').val();
      var fim = $('#fim').val();
      var valor_plano = $('#valor_plano').val();
      if(tipo != 'kit') {
          var valor_total = qtd * parseFloat(valor_plano.replace('.', '').replace(',', '.'));
          var formatter = new Intl.NumberFormat('pt-BR', {
              style: 'currency',
              currency: 'BRL',
              minimumFractionDigits: 2,
          });
          var valor_total = formatter.format(valor_total);

          var dados = 'nome=' + nome + ' tipo=' + tipo + ' cod=' + cod + ' qtd=' + qtd + 'catalogo_id=' + catalogo_id;
          var linha = nome;
          $('#contador').val(contador);
          $('.add-item-orcamento-aditivo').attr('contador', contador);
          var botoes = "<br><input type='checkbox' class=' excluir-item excluir-item-paciente-" + paciente + "' /> <b> Excluir</b>";
          $('#tabela_' + paciente).append('<tr class=\'itens_fatura\' bgcolor=\'\' >' +
              '<td>TUSS-' + tuss + ' N-' + linha + botoes + '</td><td>' + apresentacao + '</td>' +
              '<td class=\'unidade_fat\' id=' + paciente + '_' + contador + '></td>' +
              '<td><input class=\'qtd OBG\' size=\'7\' value=' + qtd + ' paciente=' + paciente + ' /></td>' +
              '<td>R$<input type=text name=valor_plano size=10 class=\'valor_plano valor OBG\' tiss=\'' + cod + '\' catalogo_id=\'' + catalogo_id + '\' qtd=\'' + qtd + '\' paciente=\'' + paciente + '\' solicitacao=\'\' inicio=' + inicio + ' fim=' + fim + ' tipo=\'' + tipo + '\' tuss=\'' + tuss + '\' tabela_origem=\'' + tabela_origem + '\'  custo =\'' + custo + '\' usuario=\'\' ord =\'\' flag=\'\'  value=\'' + valor_plano + '\' editado=\'0\' codf=\'0\' ></td>' +
              '<td><span style=\'float:right;\' id=\'val_total\'><b>R$  <span nome=\'val_linha\'>0,00</span></b></span></td>' +
              '<td><input type=\'text\' name=\'taxa\' size=\'10\' value=\'0,00\' class=\'taxa OBG\'></td>' +
              '<td><input type=\'checkbox\' name=\'incluso_pacote\' size=\'10\' class=\'incluso_pacote incluso-pacote-' + paciente + ' OBG\'></td></tr>');

          $.ajax({
              url: '/faturamento/query.php?query=buscar-unidade-medida-fatura',
              success: function (response) {
                  response = JSON.parse(response);
                  var select_unidades = '<select name="unidade" class="unidade">' +
                      '<option value="-1">Selecione</option>';
                  $.each(response, function (index, value) {
                      select_unidades += '<option value="' + index + '">' + value.unidade + '</option>';
                  });
                  select_unidades += '</select>';
                  $('#' + paciente + '_' + contador).append(select_unidades);
              },
              async: true
          });

          $('.valor').priceFormat({
              prefix: '',
              centsSeparator: ',',
              thousandsSeparator: '.'
          });
          // $('.taxa').priceFormat({
          //     prefix: '',
          //     centsSeparator: ',',
          //     thousandsSeparator: '.',
          //     allowNegative: true
          // });
          $('.taxa').maskMoney({
              prefix: '',
              decimal: ',',
              thousands: '.',
              allowNegative: true,
              allowZero: true
          });

          $('.valor_plano').unbind('blur', calcula_linha).bind('blur', calcula_linha);
          $('.qtd').unbind('blur', calcula_linha2).bind('blur', calcula_linha2);

          $('.rem-item-solicitacao').unbind('click', remove_item)
              .bind('click', remove_item);

          calcula_linha();
      } else {
          var plano = $('#dialog-formulario-aditivo').attr('plano');
          var select_unidades = '';
          $.ajax({
              url: '/faturamento/query.php?query=buscar-unidade-medida-fatura',
              success: function (response) {
                  response = JSON.parse(response);
                  select_unidades = '<select name="unidade" class="unidade">' +
                      '<option value="-1">Selecione</option>';
                  $.each(response, function (index, value) {
                      select_unidades += '<option value="' + index + '">' + value.unidade + '</option>';
                  });
                  select_unidades += '</select>';
              },
              async: false
          });
          $.ajax({
              url: '/faturamento/query.php?query=buscar-itens-kit-fatura&kit_id=' + catalogo_id + '&plano_id=' + plano + '&tipo_documento=',
              success: function (response) {
                  response = JSON.parse(response);
                  //console.table(response);
                  $.each(response, function (index, value) {
                      console.log(select_unidades);
                      var contador = parseInt($('#contador').val());
                      qtd_item = parseInt(value.qtd) * parseInt(qtd);
                      catalogo_id = index;
                      var valor_total = qtd_item * parseFloat(value.val.replace('.', '').replace(',', '.'));
                      var formatter = new Intl.NumberFormat('pt-BR', {
                          style: 'currency',
                          currency: 'BRL',
                          minimumFractionDigits: 2,
                      });
                      var valor_total = formatter.format(valor_total);

                      var dados = 'nome=' + value.nome + ' tipo=' + value.tipo + ' cod=' + value.tiss + ' qtd=' + qtd_item + 'catalogo_id=' + catalogo_id;
                      var linha = value.nome;
                      var botoes = "<br><input type='checkbox' class=' excluir-item excluir-item-paciente-" + paciente + "' /> <b> Excluir</b>";
                      $('#tabela_' + paciente).append(
                          '<tr class=\'itens_fatura\' bgcolor=\'\' >' +
                          '<td>TUSS-' + value.tuss + ' N-' + linha + botoes + '</td><td>' + value.apresentacao + '</td>' +
                          '<td class=\'unidade_fat\'>' + select_unidades + '</td>' +
                          '<td><input class=\'qtd OBG\' size=\'7\' value=' + qtd_item + ' paciente=' + paciente + ' /></td>' +
                          '<td>R$<input type=text name=valor_plano size=10 class=\'valor_plano valor OBG\' tiss=\'' + value.tiss + '\' catalogo_id=\'' + catalogo_id + '\' qtd=\'' + qtd_item + '\' paciente=\'' + paciente + '\' solicitacao=\'\' inicio=' + inicio + ' fim=' + fim + ' tipo=\'' + value.tipo + '\' tuss=\'' + value.tuss + '\' tabela_origem=\'\'  custo =\'' + value.custo + '\' usuario=\'\' ord =\'\' flag=\'\'  value=\'' + value.val + '\' editado=\'0\' codf=\'0\' ></td>' +
                          '<td><span style=\'float:right;\' id=\'val_total\'><b>R$  <span nome=\'val_linha\'>0,00</span></b></span></td>' +
                          '<td><input type=\'text\' name=\'taxa\' size=\'10\' value=\'0,00\' class=\'taxa OBG\'></td>' +
                          '<td><input type=\'checkbox\' name=\'incluso_pacote\' size=\'10\' class=\'incluso_pacote incluso-pacote-' + paciente + ' OBG\'></td></tr>');

                      contador = parseInt($('#contador').val())+1;
                      $('#contador').val(contador);
                      $('.add-item-orcamento-aditivo').attr('contador', contador);

                      $('.valor').priceFormat({
                          prefix: '',
                          centsSeparator: ',',
                          thousandsSeparator: '.'
                      });

                      // $('.taxa').priceFormat({
                      //     prefix: '',
                      //     centsSeparator: ',',
                      //     thousandsSeparator: '.',
                      //     allowNegative: true
                      // });
                      $('.taxa').maskMoney({
                          prefix: '',
                          decimal: ',',
                          thousands: '.',
                          allowNegative: true,
                          allowZero: true
                      });


                      $('.valor_plano')
                          .unbind('blur', calcula_linha)
                          .bind('blur', calcula_linha);

                      $('.qtd')
                          .unbind('blur', calcula_linha2)
                          .bind('blur', calcula_linha2);

                      $('.rem-item-solicitacao')
                          .unbind('click', remove_item)
                          .bind('click', remove_item);

                      calcula_linha();
                  });
              },
              async: false
          });
      }
      var obj=paciente+'_'+contador;

      $('#tabela_origem').val('');
      $('#cod').val('-1');
      $('#catalogo_id').val('');
      $('#dialog-formulario-aditivo input[name=nome]').val('');
      $('#dialog-formulario-aditivo input[name=qtd]').val('');
      $('#busca-item-aditivo').val('');
      $('#nome').val('');
      $('#qtd').val('');
      $('#tuss').val('');
      $('#tipo').val('');

    }
    return false;
  });
  $('#finalizar-orcamento-aditivo').click(function(event){

    if(!validar_campos("div-finalizar-orcamento-aditivo")){
      return false;
    }

    tam = $('.valor_plano').size();
    cod=$(this).attr('cod');
    id_prescricao = $(this).attr('id-prescricao');

    av=$(this).attr('presc_av');
    cap=$(this).attr('cap');
    plano=$(this).attr('plano');
    obs=$('#text_'+cod).val();
    inicio_av=$('#inicio_fatura').val();
    fim_av=$('#fim_fatura').val();
    itens='';
    cont=0;
    cont_valor_fatura=0;
    tabela = $(this).attr('tabela');


    $('.valor_plano').each(function(){

      paciente = $(this).attr('paciente');
      if( cod == paciente){

        sol = $(this).attr('solicitacao');
        tiss = $(this).attr('tiss');
        catalogo_id=$(this).attr('catalogo_id');
        qtd = $(this).attr('qtd');
        tipo = $(this).attr('tipo');
        flag = $(this).attr('flag');
        paciente = $(this).attr('paciente');
        usuario = $(this).attr('usuario');
        tabela_origem=$(this).attr('tabela_origem');
        valor_fatura = $(this).val();
        valor_fatura = parseFloat(valor_fatura.replace('.','').replace(',','.'));
        taxa= $(this).parent().parent().children('td').eq(6).children('input').val();
        taxa = parseFloat(taxa.replace('.','').replace(',','.'));
        incluso_pacote= $(this).parent().parent().children('td').eq(7).children('input').is(':checked');
        incluso_pacote= new Number(incluso_pacote);
        var tuss = $(this).attr('tuss');

        if (valor_fatura== '0.00'){
          cont_valor_fatura++;
        }
        valor_custo = $(this).attr('custo');
        fat_inicio = $('#inicio_fatura').val();
        fat_fim = $('#fim_fatura').val();
        unidade= $(this).parent().parent().children('td').eq(2).children('select').children('option:selected').val();


        cont++;

        if(cont==tam)
          itens += tiss+'#'+qtd+'#'+paciente+'#'+sol+'#'+tipo+'#'+valor_fatura+'#'+usuario+'#'+valor_custo+'#'+flag+'#'+fat_inicio+'#'+fat_fim+'#'+unidade+'#'+catalogo_id+'#'+tabela_origem+'#'+taxa+'#'+incluso_pacote+'#'+tuss;
        else
          itens += tiss+'#'+qtd+'#'+paciente+'#'+sol+'#'+tipo+'#'+valor_fatura+'#'+usuario+'#'+valor_custo+'#'+flag+'#'+fat_inicio+'#'+fat_fim+'#'+unidade+'#'+catalogo_id+'#'+tabela_origem+'#'+taxa+'#'+incluso_pacote+'#'+tuss+'$';
      }
    });
    inicio = $('#inicio_fatura').val();
    fim = $('#fim_fatura').val();

    if(cont_valor_fatura >0 ){
      $('#dialog-zerados-salvar-orcamento-aditivo').dialog('open');
      $('#dialog-zerados-salvar-orcamento-aditivo').attr('dados',itens);
      $('#dialog-zerados-salvar-orcamento-aditivo').attr('inicio',inicio);
      $('#dialog-zerados-salvar-orcamento-aditivo').attr('fim',fim);
      $('#dialog-zerados-salvar-orcamento-aditivo').attr('cod',cod);
      $('#dialog-zerados-salvar-orcamento-aditivo').attr('av',av);
      $('#dialog-zerados-salvar-orcamento-aditivo').attr('plano',plano);
      $('#dialog-zerados-salvar-orcamento-aditivo').attr('cap',cap);
      $('#dialog-zerados-salvar-orcamento-aditivo').attr('obs',obs);
      $('#dialog-zerados-salvar-orcamento-aditivo').attr('id_prescricao',id_prescricao);
      $('#dialog-zerados-salvar-orcamento-aditivo').attr('tabela',tabela);

    }else{

      $.post('inserir.php',{query:'gravar-fatura',
        dados: itens,
        fat_inicio:inicio,
        fat_fim:fim,
        fat_paciente:cod,
        fat_plano:plano,
        av:av,
        cap:cap,
        obs:obs,
        inicio_av:inicio_av,
        fim_av:fim_av,
        idPrescricao:id_prescricao,
        tabela:tabela},function(r){
        if(r ==1){
          window.location.href='?view=orcamento-aditivo';
        }else{
          alert(r);
        }


      });
    }

  });
  $('#dialog-zerados-salvar-orcamento-aditivo').dialog('close');
  $('#dialog-zerados-salvar-orcamento-aditivo').dialog({
    autoOpen: false, modal: true, position: 'top',
    buttons: {
      'Sim' : function(){
        $('#dialog-zerados-salvar-orcamento-aditivo').dialog('open');
        var dados= $(this).attr('dados');
        var fat_inicio= $(this).attr('inicio');
        var fat_fim= $(this).attr('fim');
        var cod= $(this).attr('cod');
        var  av= $(this).attr('av');
        var plano= $(this).attr('plano');
        var  cap= $(this).attr('cap');
        var   obs= $(this).attr('obs');
        var idPrescricao = $(this).attr('id_prescricao');
        var tabela = $(this).attr('tabela');

        $.post('inserir.php',{query:'gravar-fatura', dados: itens, fat_inicio:inicio, fat_fim:fim, fat_paciente:cod, fat_plano:plano, av:av,cap:cap, obs:obs,idPrescricao:idPrescricao, tabela:tabela},function(r){
          if (r == 1){

            if(av == 0){
              window.location.href='?view=faturar';
            }else if(av == 1){
              window.location.href='?view=faturaravaliacao';
            }else if(av == 2){
              window.location.href='?view=orcamento-prorrogacao';
            }else if(av == 3){
              window.location.href='?view=orcamento-aditivo';
            }


          }else{
            alert(r);
          }
        });


        $(this).dialog('close');
      },
      'Nao' : function(){

        //return false;
        $(this).dialog('close');

      }
    }
  });


    $('.salvar_fatura').live('click', function(event){
        tam = $('.valor_plano').size();
        cod=$(this).attr('cod');
        empresa=$(this).attr('empresa');
        av=$(this).attr('presc_av');
        cap=$(this).attr('cap');
        plano=$(this).attr('plano');
        obs=$('#text_'+cod).val();
        inicio_av=$('#inicio_fatura').val();
        fim_av=$('#fim_fatura').val();
        itens='';
        cont=0;
        id_fatura = $(this).attr('id-fatura');

        $('.valor_plano').each(function () {
            paciente = $(this).attr('paciente');
            if (cod == paciente) {
                sol = $(this).attr('solicitacao');
                tiss = $(this).attr('tiss');
                catalogo_id = $(this).attr('catalogo_id');
                qtd = $(this).attr('qtd');
                tipo = $(this).attr('tipo');
                flag = $(this).attr('flag');
                paciente = $(this).attr('paciente');
                usuario = $(this).attr('usuario');
                tabela_origem = $(this).attr('tabela_origem');
                valor_fatura = $(this).val();
                valor_fatura = parseFloat(valor_fatura.replace('.', '').replace(',', '.'));
                taxa = $(this).parent().parent().children('td').eq(6).children('input').val();
                taxa = parseFloat(taxa.replace('.', '').replace(',', '.'));
                incluso_pacote = $(this).parent().parent().children('td').eq(7).children('input').is(':checked');
                incluso_pacote = new Number(incluso_pacote);
                var tuss = $(this).attr('tuss');
                var label_item = $(this).attr('label-item');
                var codigo_proprio = $('#codigo-proprio-' + label_item).val();
                valor_custo = $(this).attr('custo');
                fat_inicio = $('#inicio_fatura').val();
                fat_fim = $('#fim_fatura').val();
                unidade = $(this).parent().parent().children('td').eq(2).children('select').children('option:selected').val();


                cont++;

                if (cont == tam)
                    itens += tiss + '#' + qtd + '#' + paciente + '#' + sol + '#' + tipo + '#' + valor_fatura + '#' + usuario + '#' + valor_custo + '#' + flag + '#' + fat_inicio + '#' + fat_fim + '#' + unidade + '#' + catalogo_id + '#' + tabela_origem + '#' + taxa + '#' + incluso_pacote + '#' + tuss + '#' + codigo_proprio;
                else
                    itens += tiss + '#' + qtd + '#' + paciente + '#' + sol + '#' + tipo + '#' + valor_fatura + '#' + usuario + '#' + valor_custo + '#' + flag + '#' + fat_inicio + '#' + fat_fim + '#' + unidade + '#' + catalogo_id + '#' + tabela_origem + '#' + taxa + '#' + incluso_pacote + '#' + tuss + '#' + codigo_proprio + '$';
            }
        });
        if(cont == 0){
            alert("A fatura não pode ser salva sem nenhum item informado.");
            return false;
        }
        inicio = $('#inicio_fatura').val();
        fim = $('#fim_fatura').val();


        $.post('inserir.php', {
            query: 'salvar-fatura', dados: itens, fat_inicio: inicio, fat_fim: fim, fat_paciente: cod,
            fat_plano: plano, av: av, cap: cap, obs: obs, inicio_av: inicio_av,
            fim_av: fim_av, id_fatura:id_fatura
        }, function (r) {
            if (!isNaN(r) && r >0) {

              $('#finalizar-paciente-'+cod).attr('id-fatura',r);
              $('#salvar-paciente-'+cod).attr('id-fatura',r);
              alert('Fatura salva com sucesso!');
                if(confirm('Deseja pré-visualizar a fatura atual?')) {
                  window.open('/faturamento/imprimir_fatura2.php?id=' + r + '&av='+av+'&zerados=1&modelo=1&diarias=0&observacao=1&empresa='+empresa, '_blank');
                }
            } else {
                alert(r);
            }
        });
    });

    $('#salvar-usar-novo-orcamento').live('click', function(event){
        var tam = $('.valor_plano').size();
        var cod = $(this).attr('paciente');
        var av = 1;
        var cap = $(this).attr('cap');
        var empresa = $(this).attr('empresa');
        var plano = $(this).attr('plano');
        var obs = $('#text_'+cod).val();
        var inicio_av = $("input[name='inicio']").val();
        var fim_av = $("input[name='fim']").val();
        var itens ='';
        var cont = 0;
        var id_fatura = $(this).attr('id_fatura');
        var fatura_origem = $(this).attr('fatura_origem');
        var finalizar = $(this).attr('finalizar');
        var fat_inicio = $("input[name='inicio']").val();
        var fat_fim = $("input[name='fim']").val();
        var paciente = $(this).attr('paciente');

        $('.valor_plano').each(function () {


                sol = $(this).attr('solicitacao');
                tiss = $(this).attr('tiss');
                catalogo_id = $(this).attr('catalogo_id');
                qtd = $(this).attr('qtd');
                tipo = $(this).attr('tipo');
                flag = $(this).attr('flag');
                paciente = $(this).attr('paciente');
                usuario = $(this).attr('usuario');
                tabela_origem = $(this).attr('tabela_origem');
                valor_fatura = $(this).val();
                valor_fatura = parseFloat(valor_fatura.replace('.', '').replace(',', '.'));
                taxa = $(this).parent().parent().children('td').eq(6).children('input').val();
                taxa = parseFloat(taxa.replace('.', '').replace(',', '.'));
                incluso_pacote = $(this).parent().parent().children('td').eq(7).children('input').is(':checked');
                incluso_pacote = new Number(incluso_pacote);
                 tuss = $(this).attr('tuss');
                 label_item = $(this).attr('label-item');
                 codigo_proprio = $('#codigo-proprio-' + label_item).val();
                valor_custo = $(this).attr('custo');
                unidade = $(this).parent().parent().children('td').eq(2).children('select').children('option:selected').val();
                fat_inicio = $("input[name='inicio']").val();
                fat_fim = $("input[name='fim']").val();
                cont++;

                if (cont == tam)
                    itens += tiss + '#' + qtd + '#' + paciente + '#' + sol + '#' + tipo + '#' + valor_fatura + '#' + usuario + '#' + valor_custo + '#' + flag + '#' + fat_inicio + '#' + fat_fim + '#' + unidade + '#' + catalogo_id + '#' + tabela_origem + '#' + taxa + '#' + incluso_pacote + '#' + tuss + '#' + codigo_proprio;
                else
                    itens += tiss + '#' + qtd + '#' + paciente + '#' + sol + '#' + tipo + '#' + valor_fatura + '#' + usuario + '#' + valor_custo + '#' + flag + '#' + fat_inicio + '#' + fat_fim + '#' + unidade + '#' + catalogo_id + '#' + tabela_origem + '#' + taxa + '#' + incluso_pacote + '#' + tuss + '#' + codigo_proprio + '$';

        });

        if(cont == 0){
            alert("A fatura não pode ser salva sem nenhum item informado.");
            return false;
        }



        $.post('inserir.php', {
           query: 'salvar-fatura',
            dados: itens,
            fat_inicio: inicio_av,
            fat_fim: fim_av,
            fat_paciente: paciente,
            fat_plano: plano,
            av: av,
            cap: cap,
            obs: obs,
            inicio_av: inicio_av,
            fim_av: fim_av,
            id_fatura: id_fatura,
            fatura_origem: fatura_origem
        }, function (r) {

            if (!isNaN(r) && r >0) {

                $('#salvar-usar-novo-orcamento').attr('id_fatura',r);
                $('#finalizar-usar-novo-orcamento').attr('id_fatura',r);
                alert('Fatura salva com sucesso!');

                    if(confirm('Deseja pré-visualizar a fatura atual?')) {
                        window.open('/faturamento/imprimir_fatura2.php?id=' + r + '&av='+1+'&zerados=1&modelo=1&diarias=0&observacao=1&empresa='+empresa, '_blank');
                    }

            } else {
                alert(r);
            }
        });
    });

    $('#finalizar-usar-novo-orcamento ').live('click', function(event){
        tam = $('.valor_plano').size();
        cod=$(this).attr('cod');
        av=1;
        cap=$(this).attr('paciente');
        plano=$(this).attr('plano');
        obs=$('#text_'+cod).val();
        inicio_av=$("input[name='inicio']").val();
        fim_av= $("input[name='fim']").val();
        itens='';
        cont = 0;
        id_fatura = $(this).attr('id_fatura');
         fatura_origem = $(this).attr('fatura_origem');
        finalizar = $(this).attr('finalizar');
        fat_inicio = $("input[name='inicio']").val();
        fat_fim = $("input[name='fim']").val();
        var paciente = $(this).attr('paciente');

        $('.valor_plano').each(function () {
            paciente = $(this).attr('paciente');
            sol = $(this).attr('solicitacao');
            tiss = $(this).attr('tiss');
            catalogo_id = $(this).attr('catalogo_id');
            qtd = $(this).attr('qtd');
            tipo = $(this).attr('tipo');
            flag = $(this).attr('flag');
            paciente = $(this).attr('paciente');
            usuario = $(this).attr('usuario');
            tabela_origem = $(this).attr('tabela_origem');
            valor_fatura = $(this).val();
            valor_fatura = parseFloat(valor_fatura.replace('.', '').replace(',', '.'));
            taxa = $(this).parent().parent().children('td').eq(6).children('input').val();
            taxa = parseFloat(taxa.replace('.', '').replace(',', '.'));
            incluso_pacote = $(this).parent().parent().children('td').eq(7).children('input').is(':checked');
            incluso_pacote = new Number(incluso_pacote);
             tuss = $(this).attr('tuss');
             label_item = $(this).attr('label-item');
             codigo_proprio = $('#codigo-proprio-' + label_item).val();
            valor_custo = $(this).attr('custo');
            unidade = $(this).parent().parent().children('td').eq(2).children('select').children('option:selected').val();
            cont++;

            if (cont == tam)
                itens += tiss + '#' + qtd + '#' + paciente + '#' + sol + '#' + tipo + '#' + valor_fatura + '#' + usuario + '#' + valor_custo + '#' + flag + '#' + fat_inicio + '#' + fat_fim + '#' + unidade + '#' + catalogo_id + '#' + tabela_origem + '#' + taxa + '#' + incluso_pacote + '#' + tuss + '#' + codigo_proprio;
            else
                itens += tiss + '#' + qtd + '#' + paciente + '#' + sol + '#' + tipo + '#' + valor_fatura + '#' + usuario + '#' + valor_custo + '#' + flag + '#' + fat_inicio + '#' + fat_fim + '#' + unidade + '#' + catalogo_id + '#' + tabela_origem + '#' + taxa + '#' + incluso_pacote + '#' + tuss + '#' + codigo_proprio + '$';

        });

        if(cont == 0){
            alert("A fatura não pode ser salva sem nenhum item informado.");
            return false;
        }

        $.post('inserir.php', {
            query: 'gravar-fatura',
            dados: itens,
            fat_inicio: inicio_av,
            fat_fim: fim_av,
            fat_paciente: paciente,
            fat_plano: plano,
            av: av,
            cap: cap,
            obs: obs,
            inicio_av: inicio_av,
            fim_av: fim_av,
            remocao: 0,
            id_fatura: id_fatura
            // query: 'salvar-fatura',
            // dados: itens,
            // fat_inicio:  inicio_av,
            // fat_fim: fim_av,
            // fat_paciente: paciente,
            // fat_plano: plano,
            // av: av,
            // cap: cap,
            // obs: obs,
            // inicio_av: inicio_av,
            // fim_av: fim_av,
            // id_fatura: id_fatura,
            // fatura_origem: fatura_origem,
            // finalizar: finalizar
        }, function (r) {


            if (!isNaN(r) && r >0) {

                $('#salvar-usar-novo-orcamento').attr('id_fatura',r);
                $('#finalizar-usar-novo-orcamento').attr('id_fatura',r);
                alert('Fatura salva com sucesso!');
                    window.location.href = '?view=faturaravaliacao';
            } else {
                alert(r);
            }
        });
    });

    /*$("#salvar_editar_fatura").live('click', function(){
        var id_fatura = $(this).attr('idfat');
        var paciente = '';
        var remocao = $("#select-remocao").val();
        tam = $('.valor_plano').size();
        itens='';
        cont=0;
        tipo=  $(this).attr('tipo');
        status='';

        if($("#status_faturado").is(":checked")){
            status = 5;
        }else{
            status = 2;
        }

        $('.valor_plano').each(function(){
            paciente = $(this).attr('paciente');

            sol = $(this).attr('solicitacao');
            codf = $(this).attr('codf');
            qtd = $(this).attr('qtd');
            tipo = $(this).attr('tipo');
            flag = $(this).attr('flag');
            paciente = $(this).attr('paciente');
            usuario = $(this).attr('usuario');
            valor_fatura = $(this).val();
            valor_fatura = parseFloat(valor_fatura.replace('.','').replace(',','.'));
            valor_custo = $(this).attr('custo');
            inicio = $(this).attr('inicio');
            fim = $(this).attr('fim');
            unidade= $(this).parent().parent().children('td').eq(2).children('select').children('option:selected').val();
            custo = $(this).attr('custo');
            numero_tiss= $(this).attr('numero_tiss');
            catalogo_id = $(this).attr('catalogo_id');
            tabela_origem=$(this).attr('tabela_origem');
            taxa= $(this).parent().parent().children('td').eq(6).children('input').val();
            taxa = parseFloat(taxa.replace('.','').replace(',','.'));
            incluso_pacote= $(this).parent().parent().children('td').eq(7).children('input').is(':checked');
            incluso_pacote= new Number(incluso_pacote);
            editado = $(this).attr('editado');
            var tuss = $(this).attr('tuss');
            var label_item = $(this).attr('label-item');
            var codigo_proprio = $('#codigo-proprio-' + label_item).val();
            cont++;

            if(cont==tam)
                itens += codf+'#'+qtd+'#'+paciente+'#'+sol+'#'+tipo+'#'+valor_fatura+'#'+usuario+'#'+valor_custo+'#'+flag+'#'+inicio+'#'+fim+'#'+unidade+'#'+custo+'#'+numero_tiss+'#'+catalogo_id+'#'+tabela_origem+'#'+taxa+'#'+incluso_pacote+'#'+editado+'#'+tuss+'#'+codigo_proprio;
            else
                itens += codf+'#'+qtd+'#'+paciente+'#'+sol+'#'+tipo+'#'+valor_fatura+'#'+usuario+'#'+valor_custo+'#'+flag+'#'+inicio+'#'+fim+'#'+unidade+'#'+custo+'#'+numero_tiss+'#'+catalogo_id +'#'+tabela_origem+'#'+taxa+'#'+incluso_pacote+'#'+editado+'#'+tuss+'#'+codigo_proprio+'$';

        });


        $.post('inserir.php',{query:'salvar-editar-fatura', dados:itens,id_fatura:id_fatura,inicio:$("input[name=inicio]").val(),
            fim:$("input[name=fim]").val(),empresa:$(this).attr('empresa'),plano:$(this).attr('plan'),
            guia_tiss:$('#guia_tiss').val(),status:status,tipo_fatura:$(this).attr('tipo'),
            obs:$('#text_'+paciente).val(),remocao:remocao},function(r){
            if(r == 1){
                alert("Dados editados com sucesso.");
                window.location.href='?view=listar';
            }

        });
    });

  /////////////////
  $("#salvar_editar_fatura").live('click',function() {

    var tipo_fatura =$(this).attr('tipo');
    var paciente_id =$(this).attr('paciente_id');
    this_obs=$('#text_'+paciente_id);
    obs = this_obs.val();
    this_obs.attr('defaultValue',obs);
    var fatura_id   =  $(this).attr('fatura_id');
    var idInicio = $("input[name='inicio']").attr('id');
    var idfim = $("input[name='fim']").attr('id');
    $("input[name='inicio']").attr('id',null);
    $("input[name='fim']").attr('id',null);
    $("input[name='inicio']").removeClass('hasDatepicker');
    $("input[name='fim']").removeClass('hasDatepicker');
    var html = $("#div-geral-editar-fatura").html();
    var inicio = $("input[name='inicio']").val();
    var fim = $("input[name='fim']").val();





    $.post("inserir.php", {query: "salvar-fatura-editar", html:html,
                            paciente_id: paciente_id,tipo_fatura:tipo_fatura,
                          fatura_id:fatura_id, fim:fim, inicio:inicio}, function(r) {
      alert(r);
    });

    $("input[name='inicio']").attr('id',idInicio );
    $("input[name='fim']").attr('id',idfim);
    $("input[name='inicio']").addClass('hasDatepicker');
    $("input[name='fim']").addClass('hasDatepicker');
    return false;



  });*/


  $(".nav-listar-fat").click(function(){
    window.location.href='?view=listar';
  });
  $(".nav-criar-fat").click(function(){
    window.location.href='?view=faturar';
  });
  $(".nav-gerar-lot").click(function(){
    window.location.href='?view=lote';
  });
  $(".nav-gerenciar-lot").click(function(){
    window.location.href='?view=gerenciarlote';
  });


  //////////////////////jeferson editar orcamento 17-5-2013

  $("#salvar_editar_orcamento,#finalizar_editar_orcamento,#finalizar_novo_orcamento").click(function() {

    if(!validar_campos("div-geral-editar-fatura")){
      return false;
    }

    id_fatura = $(this).attr('id_fatura');
    tipo_fatura = $(this).attr('tipo');
    empresa = $(this).attr('empresa');
    inicio = $("input[name=inicio]").val();
    fim = $("input[name=fim]").val();
    id_capmedica = $(this).attr('id_capmedica');
    paciente_id = $(this).attr('paciente');
    empresa =  $(this).attr('empresa');
    plano=$(this).attr('plano');
    status=$(this).attr('status');
    tam = $('.valor_plano').size();
    itens = '';
    cont = 0;
    obs= $('#text_'+paciente_id).val();
    av = $(this).attr('av');


    //guia_tiss= $('#guia_tiss').val();
    /// se finalizar = 0 ele salva se for igual a 1 ele finaliza.
    finalizar=$(this).attr('finalizar');

    $('.valor_plano').each(function() {
      paciente = $(this).attr('paciente');

      sol = $(this).attr('solicitacao');
      codf = $(this).attr('codf');
      qtd = $(this).attr('qtd');
      tipo = $(this).attr('tipo');
      numero_tiss = $(this).attr('numero_tiss');
      flag = $(this).attr('flag');
      catalogo_id=$(this).attr('catalogo_id');
      //plano = $(this).attr('plano');
      usuario = $(this).attr('usuario');
      valor_fatura = $(this).val();
      valor_fatura = parseFloat(valor_fatura.replace('.', '').replace(',', '.'));
      valor_custo = $(this).attr('custo');
      tabela_origem=$(this).attr('tabela_origem');
      taxa= $(this).parent().parent().children('td').eq(6).children('input').val();
      taxa = parseFloat(taxa.replace('.','').replace(',','.'));
      incluso_pacote= $(this).parent().parent().children('td').eq(7).children('input').is(':checked');
      incluso_pacote= new Number(incluso_pacote);
      var tuss= $(this).attr('tuss');
      var editado = $(this).attr('editado');

      unidade = $(this).parent().parent().children('td').eq(2).children('select').children('option:selected').val();
      cont++;


      if (cont == tam)
        itens += codf + '#' + qtd + '#' + paciente + '#' + sol + '#' + tipo + '#' + valor_fatura + '#' + usuario + '#' + valor_custo + '#' + flag + '#' + inicio + '#' + fim + '#' + unidade + '#' + numero_tiss+ '#' +catalogo_id+ '#' +tabela_origem+'#'+taxa+'#'+incluso_pacote+'#'+tuss+'#'+editado;
      else
        itens += codf + '#' + qtd + '#' + paciente + '#' + sol + '#' + tipo + '#' + valor_fatura + '#' + usuario + '#' + valor_custo + '#' + flag + '#' + inicio + '#' + fim + '#' + unidade + '#' + numero_tiss+ '#' +catalogo_id + '#' +tabela_origem+'#'+taxa+'#'+incluso_pacote+'#'+tuss+'#'+editado+ '$';

    });





    $.post('inserir.php', {query: 'editar-orcamento', dados: itens, id_fatura: id_fatura, inicio: inicio, fim: fim,
      paciente: paciente_id, id_capmedica: id_capmedica, fat_plano:plano,finalizar:finalizar,tipo_fatura:tipo_fatura,obs:obs,
      empresa:empresa, status:status,av:av
    }, function(r) {
      if (r == 1 && finalizar==1) {
        alert("Dados editados com sucesso.");
        if(av == 1){
          window.location.href = '?view=faturaravaliacao';
        }else if(av ==2 ){
          window.location.href ='?view=orcamento-prorrogacao';
        }

      }else if (r == 1 && finalizar==0){
        $('#finalizar_editar_orcamento').attr('disabled','disabled');
        if(confirm('Orçamento salvo. Deseja pré-visualizar a fatura atual?')) {
          window.open('/faturamento/imprimir_fatura2.php?id=' + id_fatura + '&av='+av+'&zerados=1&modelo=1&diarias=0&observacao=1&empresa='+empresa, '_blank');
        }
        $("#aguarde-reload").dialog('open');
        window.location.reload();
        return false;

      }else{
        alert(r);
        return false
      }

    });
  });


    $("#salvar_usar_para_novo_orcamento,#finalizar_usar_novo_orcamento").click(function() {

        if(!validar_campos("div-geral-editar-fatura")){
            return false;
        }

        id_fatura = $(this).attr('id_fatura');
        fatura_origem = $(this).attr('fatura_origem');
        tipo_fatura = $(this).attr('tipo');
        inicio = $("input[name=inicio]").val();
        fim = $("input[name=fim]").val();
        id_capmedica = $(this).attr('id_capmedica');
        paciente_id = $(this).attr('paciente');
        empresa =  $(this).attr('empresa');
        plano=$(this).attr('plano');
        status=$(this).attr('status');
        tam = $('.valor_plano').size();
        itens = '';
        cont = 0;
        obs= $('#text_'+paciente_id).val();
        av = $(this).attr('av');


        //guia_tiss= $('#guia_tiss').val();
        /// se finalizar = 0 ele salva se for igual a 1 ele finaliza.
        finalizar=$(this).attr('finalizar');

        $('.valor_plano').each(function() {
            paciente = $(this).attr('paciente');

            sol = $(this).attr('solicitacao');
            codf = $(this).attr('codf');
            qtd = $(this).attr('qtd');
            tipo = $(this).attr('tipo');
            numero_tiss = $(this).attr('numero_tiss');
            flag = $(this).attr('flag');
            catalogo_id=$(this).attr('catalogo_id');
            //plano = $(this).attr('plano');
            usuario = $(this).attr('usuario');
            valor_fatura = $(this).val();
            valor_fatura = parseFloat(valor_fatura.replace('.', '').replace(',', '.'));
            valor_custo = $(this).attr('custo');
            tabela_origem=$(this).attr('tabela_origem');
            taxa= $(this).parent().parent().children('td').eq(6).children('input').val();
            taxa = parseFloat(taxa.replace('.','').replace(',','.'));
            incluso_pacote= $(this).parent().parent().children('td').eq(7).children('input').is(':checked');
            incluso_pacote= new Number(incluso_pacote);
            var tuss= $(this).attr('tuss');
            var editado = $(this).attr('editado');

            unidade = $(this).parent().parent().children('td').eq(2).children('select').children('option:selected').val();
            cont++;


            if (cont == tam)
                itens += codf + '#' + qtd + '#' + paciente + '#' + sol + '#' + tipo + '#' + valor_fatura + '#' + usuario + '#' + valor_custo + '#' + flag + '#' + inicio + '#' + fim + '#' + unidade + '#' + numero_tiss+ '#' +catalogo_id+ '#' +tabela_origem+'#'+taxa+'#'+incluso_pacote+'#'+tuss+'#'+editado;
            else
                itens += codf + '#' + qtd + '#' + paciente + '#' + sol + '#' + tipo + '#' + valor_fatura + '#' + usuario + '#' + valor_custo + '#' + flag + '#' + inicio + '#' + fim + '#' + unidade + '#' + numero_tiss+ '#' +catalogo_id + '#' +tabela_origem+'#'+taxa+'#'+incluso_pacote+'#'+tuss+'#'+editado+ '$';

        });





        $.post('inserir.php', {query: 'editar-orcamento', dados: itens, id_fatura: id_fatura, inicio: inicio, fim: fim,
            paciente: paciente_id, id_capmedica: id_capmedica, fat_plano:plano,finalizar:finalizar,tipo_fatura:tipo_fatura,obs:obs,
            empresa:empresa, status:status,av:av, usarnovo:1, 
        }, function(r) {
            if (r == 1 && finalizar==1) {
                alert("Dados editados com sucesso.");
                if(av == 1){
                    window.location.href = '?view=faturaravaliacao';
                }else if(av ==2 ){
                    window.location.href ='?view=orcamento-prorrogacao';
                }

            }else if (r == 1 && finalizar==0){
                $('#finalizar_editar_orcamento').attr('disabled','disabled');
                if(confirm('Orçamento salvo. Deseja pré-visualizar a fatura atual?')) {
                    window.open('/faturamento/imprimir_fatura2.php?id=' + id_fatura + '&av='+av+'&zerados=1&modelo=1&diarias=0&observacao=1&empresa='+empresa, '_blank');
                }
                $("#aguarde-reload").dialog('open');
                window.location.reload();
                return false;

            }else{
                alert(r);
                return false
            }

        });
    });



  $('#add_item_orcamento').click(function(){


    $('#dialog-formulario-orcamento').attr('cod',$(this).attr('id_paciente'));
    $('#dialog-formulario-orcamento').attr('plano',$(this).attr('plano'));
    $('#dialog-formulario-orcamento').attr('empresa',$(this).attr('empresa'));
    // $('#dialog-formulario-orcamento').attr('tipo','ext');
    $('#contador').val($(this).attr('contador'));
    $('#dialog-formulario-orcamento').attr('tipo_medicamento',$(this).attr('tipo_medicamento'));
    $('#dialog-formulario-orcamento').attr('tipo-documento',$(this).attr('tipo-documento'));

    if($(this).attr('tipo_medicamento') == 'G'){
      $('#msg-generico').append("<div style='border: 2px dotted #999'>"
          +"<span class = 'text-red'>Paciente do plano "+$(this).attr('nomeplano')+" é recomendado prescrever itens Genéricos."
          +" Na busca de medicamento deve evitar selecionar os itens em vermelho.</span></div><br>");
    }

    $('#dialog-formulario-orcamento').dialog('open');


  });

  $('#dialog-formulario-orcamento').dialog('close');



  $('#dialog-formulario-orcamento').dialog({
    autoOpen: false, modal: true, width: 800, position: 'top',
    open: function(event,ui){
      $('#tipos').buttonset();
      $('#dialog-formulario-orcamento').attr('tipo', $('#tipos input[name=tipos]:checked').val());
      $('input[name=busca-orcamento]').val('');
      $('#nome').val('');
      $('#nome').attr('readonly','readonly');
      $('#apr').val('');
      $('#catalogo_id').val('');
      $('#apr').attr('readonly','readonly');
      $('#cod').val('');
      $('#tipo').val(0);
      $('#custo').val('');
      $('#tuss').val('');
      $('#qtd').val('');
      $('input[name=busca-orcamento]').focus();
    },
    buttons: {
      'Fechar' : function(){
        var item = $('#dialog-formulario-orcamento').attr('item');
        $('#msg-generico').html('');
        if(item != 'avulsa'){
          $('#kits-'+item).hide();
          $('#med-'+item).hide();
          $('#mat-'+item).hide();
          $('#serv-'+item).hide();
        }
        $(this).dialog('close');
      }
    }
  });

  $('#add-itemorcamento').click(function(){

    if(validar_campos('dialog-formulario-orcamento'))
    {
      var cod = $('#cod').val();
      var tuss =$('#tuss').val();
      var catalogo_id = $('#catalogo_id').val();
      var nome = $('#dialog-formulario-orcamento input[name=nome]').val();
      var valor_plano = $('#valor_plano').val();
      var tipo= $('#tipo').val();
      var tabela_origem = $('#tabela_origem').val();
      var custo = $('#custo').val();
      var contador = parseInt($('#contador').val())+1;
      var tipo_documento = $('#dialog-formulario ').attr('tipo-documento');
      $('#'+paciente)
      if(tipo == 'med' || tipo==0)
      {
        tipo = 0;
        apresentacao = 'MEDICAMENTO';
      }
      else if(tipo == 'mat' || tipo == 1)
      {
        tipo = 1;
        apresentacao = 'MATERIAL';
      }
      else if(tipo == 'serv' || tipo == 12 ||  tipo=='ext' || tipo == 2 )
      {
        tipo = 2;
        apresentacao = 'SERVI&Ccedil;OS';
      }
      else if(tipo == 'eqp' || tipo == 5)
      {
        tipo = 5;
        apresentacao = 'EQUIPAMENTO';
      }
      else if(tipo == 'gas' || tipo == 6)
      {
        tipo = 6;
        apresentacao = 'GASOTERAPIA';
      }
      else if(tipo == 'die' || tipo == 3)
      {
        tipo = 3;
        apresentacao = 'DIETA';
      }

      var qtd = $('#dialog-formulario-orcamento input[name=qtd]').val();
      var paciente = $('#dialog-formulario-orcamento').attr('cod');
      var inicio= $('#inicio').val();
      var fim = $('#fim').val();
        $('#contador').val(contador);
        $('#add_item_orcamento').attr('contador', contador);
      if(tipo != 'kit') {
          var dados = 'nome=' + nome + ' tipo=' + tipo + ' cod=' + cod + ' qtd=' + qtd + ' paciente=' + paciente + 'catalogo_id=' + catalogo_id;
          var linha = nome;

          var label_item = cod.replace(/\W/g, '');
          var botoes = '<img align=\'right\' class=\'rem-item-solicitacao\'  src=\'../utils/delete_16x16.png\' title=\'Remover\' border=\'0\'>';

          $('#tabela_orcamento').append(
              "<tr class='itens_fatura " + paciente + " editado' >" +
              "<td>" +
              "<label id='label-novo-codigo-" + label_item + "' style='display:none;'>" +
              "<button cod='" + label_item + "' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover usar_tiss'>" +
              "<span class='ui-button-text'>Usar Tiss</span></button>" +
              "<input type='text' maxlength='11' id='codigo-proprio-" + label_item + "' name='codigo-proprio'></label>" +
              "<button id='bt-novo-codigo-" + label_item + "' cod='" + label_item + "' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover bt_novo_codigo'>" +
              "<span class='ui-button-text'>C&oacute;digo Pr&oacute;prio</span></button>" +
              "TUSS-" + tuss + "  N-" + linha + botoes + "</td><td>" + apresentacao + "</td><td class='unidade_fat' id='" + paciente + "_" + contador + "'></td><td><input class='qtd OBG' size='7' value='" + qtd + "' editado='0' codf='0' paciente='" + paciente + "' /></td>" +
              "<td>R$<input type=text name=valor_plano size=10 class='valor_plano valor OBG' numero_tiss='" + cod + "' qtd='" + qtd + "' label-item='" + label_item + "'  paciente='" + paciente + "' tipo='" + tipo + "' custo ='" + custo + "' tuss='" + tuss + "' tabela_origem='" + tabela_origem + "' catalogo_id='" + catalogo_id + "' ord ='' flag=''  value='" + valor_plano + "' editado='0' codf='0'></td><td><span style='float:right;' id='val_total'><b>R$  <span nome='val_linha'>0,00</span></b></span></td>" +
              "<td><input type='text' name='taxa' size='10' class='taxa OBG' value='0,00'></td>" +
              "<td><input type='checkbox' name='incluso_pacote' size='10' class='incluso_pacote incluso-pacote-" + paciente + " OBG'></td>" +
              "</tr>");
          $.ajax({
              url: '/faturamento/query.php?query=buscar-unidade-medida-fatura',
              success: function (response) {
                  response = JSON.parse(response);
                  var select_unidades = '<select name="unidade" class="unidade">' +
                      '<option value="-1">Selecione</option>';
                  $.each(response, function (index, value) {
                      select_unidades += '<option value="' + index + '">' + value.unidade + '</option>';
                  });
                  select_unidades += '</select>';
                  $('#' + paciente + '_' + contador).append(select_unidades);
              },
              async: true
          });

          $('.valor').priceFormat({
              prefix: '',
              centsSeparator: ',',
              thousandsSeparator: '.'
          });

          // $('.taxa').priceFormat({
          //     prefix: '',
          //     centsSeparator: ',',
          //     thousandsSeparator: '.',
          //     allowNegative: true
          // });
          $('.taxa').maskMoney({
              prefix: '',
              decimal: ',',
              thousands: '.',
              allowNegative: true,
              allowZero: true
          });


          $('.bt_novo_codigo').live('click', function () {
              var cod = $(this).attr('cod');
              $('#label-novo-codigo-' + cod).show();
              $(this).hide();
          });

          $('.usar_tiss').live('click', function () {
              var cod = $(this).attr('cod');
              $('#bt-novo-codigo-' + cod).show();
              $('#label-novo-codigo-' + cod).css('display', 'none');
              $('#codigo-proprio-' + cod).val('');
          });

          $('.valor_plano')
              .unbind('blur', calcula_linha)
              .bind('blur', calcula_linha);

          $('.qtd')
              .unbind('blur', calcula_linha2)
              .bind('blur', calcula_linha2);

          $('.rem-item-solicitacao')
              .unbind('click', remove_item)
              .bind('click', remove_item);

          calcula_linha();
      } else {
          var plano = $('#dialog-formulario').attr('plano');
          var select_unidades = '';
          $.ajax({
              url: '/faturamento/query.php?query=buscar-unidade-medida-fatura',
              success: function (response) {
                  response = JSON.parse(response);
                  select_unidades = '<select name="unidade" class="unidade">' +
                      '<option value="-1">Selecione</option>';
                  $.each(response, function (index, value) {
                      select_unidades += '<option value="' + index + '">' + value.unidade + '</option>';
                  });
                  select_unidades += '</select>';
              },
              async: false
          });
          $.ajax({
              url: '/faturamento/query.php?query=buscar-itens-kit-fatura&kit_id=' + catalogo_id + '&plano_id=' + plano + '&tipo_documento=' + tipo_documento,
              success: function (response) {
                  response = JSON.parse(response);
                  //console.table(response);
                  $.each(response, function (index, value) {
                      var contador = parseInt($('#contador').val()) || 0;
                      qtd_item = parseInt(value.qtd) * parseInt(qtd);
                      catalogo_id = index;
                      var dados = 'nome=' + value.nome + ' tipo=' + value.tipo + ' cod=' + value.tiss + ' qtd=' + qtd_item + ' paciente=' + paciente + 'catalogo_id=' + catalogo_id;
                      var linha = value.nome;

                      var label_item = value.tiss.replace(/\W/g, '');
                      var botoes = '<img align=\'right\' class=\'rem-item-solicitacao\'  src=\'../utils/delete_16x16.png\' title=\'Remover\' border=\'0\'>';

                      $('#tabela_orcamento').append(
                          "<tr class='itens_fatura " + paciente + " editado' >" +
                          "<td>" +
                          "<label id='label-novo-codigo-" + label_item + "' style='display:none;'>" +
                          "<button cod='" + label_item + "' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover usar_tiss'>" +
                          "<span class='ui-button-text'>Usar Tiss</span></button>" +
                          "<input type='text' maxlength='11' id='codigo-proprio-" + label_item + "' name='codigo-proprio'></label>" +
                          "<button id='bt-novo-codigo-" + label_item + "' cod='" + label_item + "' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover bt_novo_codigo'>" +
                          "<span class='ui-button-text'>C&oacute;digo Pr&oacute;prio</span></button>" +
                          "TUSS-" + value.tuss + "  N-" + linha + botoes + "</td>" +
                          "<td>" + value.apresentacao + "</td>" +
                          "<td class='unidade_fat'>" + select_unidades + "</td>" +
                          "<td><input class='qtd OBG' size='7' value='" + qtd_item + "' editado='0' codf='0' paciente='" + paciente + "' /></td>" +
                          "<td>R$<input type=text name=valor_plano size=10 class='valor_plano valor OBG' numero_tiss='" + value.tiss + "' qtd='" + qtd_item + "' label-item='" + label_item + "'  paciente='" + paciente + "' tipo='" + value.tipo + "' custo ='" + value.custo + "' tuss='" + value.tuss + "' tabela_origem='' catalogo_id='" + catalogo_id + "' ord ='' flag=''  value='" + value.val + "' editado='0' codf='0'></td>" +
                          "<td><span style='float:right;' id='val_total'><b>R$  <span nome='val_linha'>0,00</span></b></span></td>" +
                          "<td><input type='text' name='taxa' size='10' class='taxa OBG' value='0,00'></td>" +
                          "<td><input type='checkbox' name='incluso_pacote' size='10' class='incluso_pacote incluso-pacote-" + paciente + " OBG'></td>" +
                          "</tr>");

                      $('.valor').priceFormat({
                          prefix: '',
                          centsSeparator: ',',
                          thousandsSeparator: '.'
                      });

                      // $('.taxa').priceFormat({
                      //     prefix: '',
                      //     centsSeparator: ',',
                      //     thousandsSeparator: '.',
                      //     allowNegative: true
                      // });
                      $('.taxa').maskMoney({
                          prefix: '',
                          decimal: ',',
                          thousands: '.',
                          allowNegative: true,
                          allowZero: true
                      });


                      $('.bt_novo_codigo').live('click', function () {
                          var cod = $(this).attr('cod');
                          $('#label-novo-codigo-' + cod).show();
                          $(this).hide();
                      });

                      $('.usar_tiss').live('click', function () {
                          var cod = $(this).attr('cod');
                          $('#bt-novo-codigo-' + cod).show();
                          $('#label-novo-codigo-' + cod).css('display', 'none');
                          $('#codigo-proprio-' + cod).val('');
                      });

                      $('.valor_plano')
                          .unbind('blur', calcula_linha)
                          .bind('blur', calcula_linha);

                      $('.qtd')
                          .unbind('blur', calcula_linha2)
                          .bind('blur', calcula_linha2);

                      $('.rem-item-solicitacao')
                          .unbind('click', remove_item)
                          .bind('click', remove_item);

                      contador = parseInt($('#contador').val())+1;
                      $('#contador').val(contador);
                      $('#add_item_orcamento').attr('contador', contador);

                      calcula_linha();
                  });
              },
              async: false
          });
      }

      var obj=paciente+'_'+contador;
      $('#cod').val('-1');
      $('#catalogo_id').val('-1');
      $('#dialog-formulario-orcamento input[name=nome]').val('');
      $('#dialog-formulario-orcamento input[name=qtd]').val('');
      $('#busca-itemav').val('');

    }
    return false;
  });

////////////////////// fim jeferson editar fatura 17-5-2013


  $(".filtrar_por_lote").click(function(){


    if($(this).attr('t')=='operadora'){
      $("#operadora_lote").show();
      $("#pac_lote").hide();
      $("#label-pac").hide();
      $("#ure").hide();
      $("#select_paciente").find('option').filter(':selected').attr('selected','');
      $("#select_unidade").find('option').filter(':selected').attr('selected','');
      $('#select_paciente_chosen').hide();
    }else if($(this).attr('t')=='unidade'){
      $("#operadora_lote").hide();
      $("#pac_lote").hide();
      $("#label-pac").hide();
      $("#ure").show();
      $("#select_paciente").find('option').filter(':selected').attr('selected','');
      $("#select_operadora").find('option').filter(':selected').attr('selected','');
      $('#select_paciente_chosen').hide();
    }else{
      $('#select_paciente_chosen').show();
      $("#label-pac").show();
      $("#pac_lote").show();
      $("#ure").hide();
      $("#operadora_lote").hide();
      $("#select_operadora").find('option').filter(':selected').attr('selected','');
      $("#select_unidade").find('option').filter(':selected').attr('selected','');

    }
  });


  $("#select_plano").change(function(){
    if($("#check_plano").is(":checked")){

    }else{
      $(".bloco2").attr('cod',$(this).val());
      $(".bloco2").attr('nome',$(this).find('option').filter(':selected').text());
    }
  });
  $("#select_operadora").change(function(){

    $(".bloco2").attr('cod',$(this).val());
    $(".bloco2").attr('nome',$(this).find('option').filter(':selected').text());
  });
  $("#status").change(function(){

    $(".bloco2").attr('status',$(this).val());

  });

  $("#select_unidade").change(function(){

    $(".bloco2").attr('cod',$(this).val());
    $(".bloco2").attr('nome',$(this).find('option').filter(':selected').text());
  });

  $("#select_paciente").change(function(){

    $(".bloco2").attr('cod',$(this).val());
    $(".bloco2").attr('nome',$(this).find('option').filter(':selected').text());
    if($(this).find('option').filter(':selected').attr('id_status') != 4){
      var status =$(this).find('option').filter(':selected').attr('nome_status');
      alert("Status do paciente: "+status+". Deseja continuar?");
    }
  });

  $("#select_paciente_prorrogacao").change(function(){

    $(".bloco2").attr('cod',$(this).val());
    $(".bloco2").attr('nome',$(this).find('option').filter(':selected').text());
    if($(this).find('option').filter(':selected').attr('id_status') != 4){
      var status =$(this).find('option').filter(':selected').attr('nome_status');

    }
  });

  $("#select_paciente_avaliacao").change(function(){

    $("#bloco1_av").attr('cod',$(this).val());
    $("#bloco1_av").attr('nome',$(this).find('option').filter(':selected').text());

  });

  $("#op_avaliacao").change(function(){

    $("#bloco1_av").attr('op_avaliacao',$(this).val());

    if($(this).val() == '2') {
      $("#select_paciente_avaliacao option").each(function () {
          $(this).attr('disabled', '');
          $(".chosen-select").trigger("chosen:updated");
      });
    } else {
        if($("#select_paciente_avaliacao option:selected").attr('status-paciente') == '4'){
            $("#select_paciente_avaliacao").val('-1');
        }
        $("#select_paciente_avaliacao option[status-paciente='4']").attr('disabled', true);
        $(".chosen-select").trigger("chosen:updated");
    }


  });
  /////////////////////jeferson fim


  $(".numerico").keyup(function(e){
    this.value = this.value.replace(/\D/g,"");
  });

  $(".valor").priceFormat({
    prefix: "",
    centsSeparator: ",",
    thousandsSeparator: "."
  });

  function calcula_linha(){

    //e.preventDefault();
    var total=0.00;
    var total2=0.00;
    var material = 0.00;
    var medicamento = 0.00;
    var dieta = 0.00;
    var equipamento = 0.00;
    var servico = 0.00;
    var gas = 0.00;


    $('.valor_plano').each(function(){
        var pacientevar = $(this).attr('paciente');
      var  valor1=$(this).val();
      valor = $(this).val().replace('.','');
      $(this).val(valor1);

      valor = valor.replace(',','.');
      var paciente = $(this).attr('paciente');

      if(paciente == pacientevar){
        tipo = $(this).attr('tipo');


        if(tipo == 1){
          material += ( valor*$(this).attr('qtd'));
        } if(tipo == 2){
          servico += ( valor*$(this).attr('qtd'));
        }if(tipo == 5){
          equipamento += ( valor*$(this).attr('qtd'));
        }if(tipo == 0){
          medicamento += ( valor*$(this).attr('qtd'));
        }if(tipo == 3){
          dieta += ( valor*$(this).attr('qtd'));
        }if(tipo == 6){
          gas += ( valor*$(this).attr('qtd'));
        }



        total2 += ( valor*$(this).attr('qtd'));
        //$('#val_total'+$(this).attr('ord')).html('<b>R$  <span nome='val_linha'> '+float2moeda(( valor*$(this).attr('qtd')))+'</span></b>');
        total21 = float2moeda(total2);
        material2 = float2moeda(material);
        equipamento2 = float2moeda(equipamento);
        medicamento2 = float2moeda(medicamento);
        servico2 = float2moeda(servico);
        dieta2 = float2moeda(dieta);
        gas2= float2moeda(gas);



        $('#equipamento_'+paciente).text(equipamento2);
        $('#medicamento_'+paciente).text(medicamento2);
        $('#servico_'+paciente).text(servico2);
        $('#material_'+paciente).text(material2);
        $('#dieta_'+paciente).text(dieta2);
        $('#gasoterapia_'+paciente).text(gas2);


        $('.sub_fatura_'+paciente).html('<b>TOTAL PACIENTE: R$</b> <b><i>'+total21+'</i></b>');
        pacientevar = paciente;
      }else{
        total2=0;

        material = 0.00;
        medicamento = 0.00;
        dieta = 0.00;
        equipamento = 0.00;
        servico = 0.00;
        gas = 0.00;
        pacientevar = paciente;
        total2 += ( valor*$(this).attr('qtd'));

        tipo = $(this).attr('tipo');


        if(tipo == 1){
          material += ( valor*$(this).attr('qtd'));
        } if(tipo == 2){
          servico += ( valor*$(this).attr('qtd'));
        }if(tipo == 5){
          equipamento += ( valor*$(this).attr('qtd'));
        }if(tipo == 0){
          medicamento += ( valor*$(this).attr('qtd'));
        }if(tipo == 3){
          dieta += ( valor*$(this).attr('qtd'));
        }if(tipo == 6){
          gas += ( valor*$(this).attr('qtd'));
        }

        //$('#val_total'+$(this).attr('ord')).html('<b>R$  '+float2moeda(( valor*$(this).attr('qtd')))+'</b>');
        total21 = float2moeda(total2);
        material2 = float2moeda(material);
        equipamento2 = float2moeda(equipamento);
        medicamento2 = float2moeda(medicamento);
        servico2 = float2moeda(servico);
        dieta2 = float2moeda(dieta);
        gas2= float2moeda(gas);

        $('#equipamento_'+paciente).text(equipamento2);
        $('#medicamento_'+paciente).text(medicamento2);
        $('#servico_'+paciente).text(servico2);
        $('#material_'+paciente).text(material2);
        $('#dieta_'+paciente).text(dieta2);
        $('#gasoterapia_'+paciente).text(gas2);


        $('.sub_fatura_'+paciente).html('<b>TOTAL PACIENTE: R$</b> <b><i>'+total21+'</i></b>');


      }

      total += ( valor*$(this).attr('qtd'));
      //$('#val_total'+$(this).attr('ord')).html('<b>R$  <span nome=val_linha>'+float2moeda(( valor*$(this).attr('qtd')))+'</span></b>');
      $(this).parent().parent().children('td').eq(5).children('span').html('<b>R$  <span nome=val_linha>'+float2moeda(( valor*$(this).attr('qtd')))+'</span></b>');
    });
    total = float2moeda(total);
    $('#total_fatura').html('<b>TOTAL: R$</b> <b><i>'+total+'</i></b>');
  }


  function calcula_linha2(){

    //e.preventDefault();
    var total=0.00;
    var total2=0.00;
    var material = 0.00;
    var medicamento = 0.00;
    var dieta = 0.00;
    var equipamento = 0.00;
    var servico = 0.00;
    var gas = 0.00;

    $('.valor_plano').each(function(){
        var pacientevar = $(this).attr('paciente');

      valor =  $(this).parent().parent().children('td').eq(4).children('input').val().replace('.','');
      valor = valor.replace(',','.');

      paciente = $(this).attr('paciente');

      if(paciente == pacientevar){
        tipo = $(this).parent().parent().children('td').eq(4).children('input').attr('tipo');


        if(tipo == 1){
          material += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
        } if(tipo == 2){
          servico += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
        }if(tipo == 5){
          equipamento += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
        }if(tipo == 0){
          medicamento += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
        }if(tipo == 3){
          dieta += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
        }if(tipo == 6){
          gas += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
        }



        total2 += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
        //$('#val_total'+$(this).parent().parent().children('td').eq(4).children('input').attr('ord')).html('<b>R$  <span nome='val_linha'> '+float2moeda(( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd')))+'</span></b>');
        total21 = float2moeda(total2);
        material2 = float2moeda(material);
        equipamento2 = float2moeda(equipamento);
        medicamento2 = float2moeda(medicamento);
        servico2 = float2moeda(servico);
        dieta2 = float2moeda(dieta);
        gas2 = float2moeda(gas);



        $('#equipamento_'+paciente).text(equipamento2);
        $('#medicamento_'+paciente).text(medicamento2);
        $('#servico_'+paciente).text(servico2);
        $('#material_'+paciente).text(material2);
        $('#dieta_'+paciente).text(dieta2);
        $('#gasoterapia_'+paciente).text(gas2);


        $('.sub_fatura_'+paciente).html('<b>TOTAL PACIENTE: R$</b> <b><i>'+total21+'</i></b>');
        pacientevar = paciente;
      }else{
        total2=0;

        material = 0.00;
        medicamento = 0.00;
        dieta = 0.00;
        equipamento = 0.00;
        servico = 0.00;
        gas = 0.00;
        pacientevar = paciente;
        total2 += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));

        tipo = $(this).parent().parent().children('td').eq(4).children('input').attr('tipo');


        if(tipo == 1){
          material += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
        } if(tipo == 2){
          servico += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
        }if(tipo == 5){
          equipamento += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
        }if(tipo == 0){
          medicamento += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
        }if(tipo == 3){
          dieta += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
        }if(tipo == 6){
          gas += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
        }

        //$('#val_total'+$(this).attr('ord')).html('<b>R$  '+float2moeda(( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd')))+'</b>');
        total21 = float2moeda(total2);
        material2 = float2moeda(material);
        equipamento2 = float2moeda(equipamento);
        medicamento2 = float2moeda(medicamento);
        servico2 = float2moeda(servico);
        dieta2 = float2moeda(dieta);
        gas2 = float2moeda(gas);

        $('#equipamento_'+paciente).text(equipamento2);
        $('#medicamento_'+paciente).text(medicamento2);
        $('#servico_'+paciente).text(servico2);
        $('#material_'+paciente).text(material2);
        $('#dieta_'+paciente).text(dieta2);
        $('#gasoterapia_'+paciente).text(gas2);


        $('.sub_fatura_'+paciente).html('<b>TOTAL PACIENTE: R$</b> <b><i>'+total21+'</i></b>');


      }

      total += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
      //$('#val_total'+$(this).attr('ord')).html('<b>R$  <span nome=val_linha>'+float2moeda(( valor*$(this).attr('qtd')))+'</span></b>');
      $(this).parent().parent().children('td').eq(5).children('span').html('<b>R$<span nome=val_linha>'+float2moeda(( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd')))+'</span></b>');
    });
    total = float2moeda(total);
    $('#total_fatura').html('<b>TOTAL: R$</b> <b><i>'+total+'</i></b>');
  }


  $(".ocultar").hide();
  $(".mostrar").toggle(
    function(){
      var item = $(this).attr("idpaciente");

      $("#tabela_"+item).show();
      $("#"+item).show();

    }, function(){
      var item = $(this).attr("idpaciente");

      $("#tabela_"+item).hide();
      $("#"+item).hide();
    });

  $(".valor_plano").live("blur",calcula_linha);
  $(".qtd").keyup(function(e){
    this.value = this.value.replace(/[^0-9]/g,"");

  });
  $(".qtd").live("click",calcula_linha2);

  /*trabalhando*/
  ////////////////2013-11-04 colocar o valor digitado nos inputs no value no html
  $(".unidade").live("change",function(){
    var valor =$(this).val();
    $(this).find('option').each(function(){

      if($(this).val() != valor){
        $(this).removeAttr('selected');
      }

    });

    $(this).children('option:selected').attr('defaultSelected',true);

  });

  $(".incluso_pacote").live("click",function(){
    if($(this).is(':checked')){

      $(this).attr('defaultChecked',true);

    }else{

      $(this).attr('defaultChecked',false);
    }

  });

  $(".valor_plano").live("blur",function(){
    var valor = $(this).val();
    $(this).attr('defaultValue',valor);

  });
  $(".taxa").live("blur",function(){
    var valor = $(this).val();
    $(this).attr('defaultValue',valor);
    valor = valor.replace('.','').replace(',','.');
    if(valor < 0){
      $(this).addClass('text-red');
      $(this).removeClass('text-green');
    }else if (valor > 0){
      $(this).addClass('text-green');
      $(this).removeClass('text-red');
    }else{
      $(this).removeClass('text-red');
      $(this).removeClass('text-green');
    }


  });
  $(".qtd").live("blur",function(){
    var valor = $(this).val();
    $(this).parent().parent().children("td").eq(4).children("input").attr("qtd",valor);
    $(this).trigger("click");
    // $(this).removeAttr('value');
    $(this).attr('defaultValue',valor);

  });
  ////fim


  $("#finalizar_editar_fatura, #salvar_editar_fatura").click(function(){
    if(!validar_campos("div-geral-editar-fatura") && ('div-select-remocao')){
      return false;
    }

    var id_fatura = $(this).attr('idfat');
    var id_paciente = $(this).attr('paciente');
    var empresa = $(this).attr('empresa');

    var remocao = $("#select-remocao").val();
    tam = $('.valor_plano').size();
    itens='';
    cont=0;
    tipo=  $(this).attr('tipo');
    status='';

    if($("#status_faturado").is(":checked")){
      status = 5;
    }else{
      status = $(this).attr('status-fatura');
      if(status == 5){
          status =2;
      }
    }


    $('.valor_plano').each(function(){
       paciente = $(this).attr('paciente');

      sol = $(this).attr('solicitacao');
      codf = $(this).attr('codf');
      qtd = $(this).attr('qtd');
      tipo = $(this).attr('tipo');
      flag = $(this).attr('flag');
      paciente = $(this).attr('paciente');
      usuario = $(this).attr('usuario');
      valor_fatura = $(this).val();
      valor_fatura = parseFloat(valor_fatura.replace('.','').replace(',','.'));
      valor_custo = $(this).attr('custo');
      inicio = $(this).attr('inicio');
      fim = $(this).attr('fim');
      unidade= $(this).parent().parent().children('td').eq(2).children('select').children('option:selected').val();
      custo = $(this).attr('custo');
      numero_tiss= $(this).attr('numero_tiss');
      catalogo_id = $(this).attr('catalogo_id');
      tabela_origem=$(this).attr('tabela_origem');
      taxa= $(this).parent().parent().children('td').eq(6).children('input').val();
      taxa = parseFloat(taxa.replace('.','').replace(',','.'));
      incluso_pacote= $(this).parent().parent().children('td').eq(7).children('input').is(':checked');
      incluso_pacote= new Number(incluso_pacote);
      editado = $(this).attr('editado');
      var tuss = $(this).attr('tuss');
      var label_item = $(this).attr('label-item');
      var codigo_proprio = $('#codigo-proprio-' + label_item).val();
      cont++;

      if(cont==tam)
        itens += codf+'#'+qtd+'#'+paciente+'#'+sol+'#'+tipo+'#'+valor_fatura+'#'+usuario+'#'+valor_custo+'#'+flag+'#'+inicio+'#'+fim+'#'+unidade+'#'+custo+'#'+numero_tiss+'#'+catalogo_id+'#'+tabela_origem+'#'+taxa+'#'+incluso_pacote+'#'+editado+'#'+tuss+'#'+codigo_proprio;
      else
        itens += codf+'#'+qtd+'#'+paciente+'#'+sol+'#'+tipo+'#'+valor_fatura+'#'+usuario+'#'+valor_custo+'#'+flag+'#'+inicio+'#'+fim+'#'+unidade+'#'+custo+'#'+numero_tiss+'#'+catalogo_id +'#'+tabela_origem+'#'+taxa+'#'+incluso_pacote+'#'+editado+'#'+tuss+'#'+codigo_proprio+'$';

    });
    var tag = $(this).attr('tag');
    tipo_fatura = $(this).attr('tipo');



    $.post('inserir.php',{query:'editar-fatura',
      dados:itens,
      id_fatura:id_fatura,
      fat_paciente: id_paciente,
      inicio:$("input[name=inicio]").val(),
      fim:$("input[name=fim]").val(),
      empresa:$(this).attr('empresa'),
      plano:$(this).attr('plan'),
      guia_tiss:$('#guia_tiss').val(),
      status:status,
      tipo_fatura:$(this).attr('tipo'),
      obs:$('#text_'+paciente).val(),
        tag: tag,
      remocao:remocao},function(r){
      if(r == 1){
        if(tag == 'finalizar') {
            alert("Dados editados com sucesso.");
            window.location.href = '?view=listar';
        } else {
            alert("Dados salvos com sucesso.");
          $('#finalizar_editar_fatura').attr('disabled','disabled');
            if(confirm('Deseja pré-visualizar a fatura atual?')) {
                window.open('/faturamento/imprimir_fatura2.php?id=' + id_fatura + '&av='+tipo_fatura+'&zerados=1&modelo=1&diarias=0&observacao=1&empresa='+empresa, '_blank');
            }
          $("#aguarde-reload").dialog('open');
          window.location.reload();

        }
      }else{
        alert(r);
      }

    });
  });

  $("#aguarde-reload").dialog('close');
  $("#aguarde-reload").dialog({
    autoOpen: false, modal: true, position: 'top'

  });




  ///////////////////////

  /* $("#fatura").click(function(){
   $.post("query.php",{query: "listar-itens", cod:$("#bloco2").attr('cod'),tipo: $("input:radio[name=filtrar_por]:checked").attr("t"),inicio:$("input[name=inicio]").val(),fim:$("input[name=fim]").val(),nome:$("#bloco2").attr('nome')}, function(r){
   $("#tabela_faturar").html(r);
   });
   });*/



  $("#refaturar").dialog('close');
  $("#refaturar").dialog({
    autoOpen: false, modal: true, position: 'top',
    buttons: {
      "Sim" : function(){
        listar_itens2();
        $(this).dialog("close");

      },
      "Não" : function(){
        listar_itens();
        $(this).dialog("close");
      }
    }
  });

  $(".enviar-email").live('click',function(){
    $("#dialog-email").attr('lote',$(this).attr('lote'));
    $("#dialog-email").attr('fatura',$(this).attr('fatura'));
    $("#dialog-email").dialog('open');
  });

  $(".enviar-email-1").click(function(){
    $("#dialog-email-1").attr('lote',$(this).attr('lote'));
    $("#dialog-email-1").dialog('open');


  });

  $("#dialog-email-1").dialog('close');
  $("#dialog-email-1").dialog({
    autoOpen: false, modal: true, position: 'top',
    buttons: {
      "Gerar PDF" : function(){
        listar_itens2();
        $(this).dialog("close");
      },
      "Enviar e-mail" : function(){
        listar_itens();
        $(this).dialog("close");
      }
    }
  });

    $("#dialog-email").dialog('close');
    $("#dialog-email").dialog({
        autoOpen: false, modal: true, position: 'top',
        buttons: {
            "Cancelar" : function(){
                $(this).dialog("close");
            },
            "Salvar" : function(){
                var envio = $('.data-envio').val();
                var protocolo = $('#protocolo-envio').val();
                var lote = $("#dialog-email").attr('lote');
                var fatura = $("#dialog-email").attr('fatura');
                var dialog = $("#dialog-email");
                if(confirm('Deseja confirmar o protocolo ' + protocolo + ' em ' + envio + ' para o lote ' + lote + '?')) {
                    if (envio != '') {
                        $.post("query.php", {
                            query: "envio-fatura",
                            lote: lote,
                            envio: envio,
                            protocolo: protocolo,
                        }, function (r) {
                            if (r == 0) {
                                alert("Ocorreu um erro na hora de salvar a data de envio do Lote.");
                                return false;
                            } else {
                                alert("O protocolo foi salvo para o lote " + r + ".");
                                $('.data-envio').val('');
                                $('#protocolo-envio').val('')
                                $('#fatura-' + fatura).html('Enviado em: ' + envio + '<br>' + 'Protocolo: ' + protocolo);
                                dialog.dialog("close");
                            }
                        });
                    } else {
                        alert('O campo Data de envio, deve ser preenchido');
                    }
                }
            }
        }
    });

  $("#dialog-zerados-fatura").dialog('close');
  $('#dialog-zerados-fatura').dialog({
    autoOpen: false, modal: true, position: 'top',
    buttons: {
      'Sim' : function(){
        var id = $(this).attr('id_fatura');
        var av=$(this).attr('av');
        window.open('imprimir_fatura.php?id='+id+'&av='+av+'&zerados=1','_blank');


        $(this).dialog('close');
      },
      'Nao' : function(){
        var id = $(this).attr('id_fatura');
        var av=$(this).attr('av');

        window.open('imprimir_fatura.php?id='+id+'&av='+av+'&zerados=0','_blank');

        $(this).dialog('close');

      }
    }
  });


  /////// fim
  //////////////////////////jeferson 2013-12-11 orçamento prorrogacao
  $("#gerar-orcamento").live('click', function(){
    if(validar_campos("formulario")){
      $.post("query.php",{query: "itens-orcamento-prorrogacao", cod:$(".bloco2").attr('cod'),inicio:$("input[name=inicio]").val(),fim:$("input[name=fim]").val(),nome:$(".bloco2").attr('nome')}, function(r){
        $("#dialog-formulario").remove();
        $("#tabela_faturar").html(r);
        load_function();
      });
    }
  });
  //////////////////////////

  $("#fatura").click(function(){
    if(validar_campos("formulario")){
      if($("#check_plano").is(':checked')){
        var plano=$("#select_plano").val();

      }else{
        var plano = 0;
      }

     // $.post("faturamento.php",{query: "re-faturar", cod:$(".bloco2").attr('cod'),tipo: $("input:radio[name=filtrar_por]:checked").attr("t"),inicio:$("#inicio_fatura").val(),fim:$("#fim_fatura").val(),nome:$(".bloco2").attr('nome'),plano:plano}, function(r){

          //$("#refaturar").dialog('open');
        listar_itens();
      //});
    }
  });

  function validar_campos(div){
    var ok = true;
    $("#"+div+" .OBG").each(function (i){
      if(this.value == ""){
        ok = false;
        this.style.border = "3px solid red";
      } else {
        this.style.border = "";
      }
    });
    $("#"+div+" .COMBO_OBG option:selected").each(function (i){

      if(this.value == "-1" || this.value == ""){
        ok = false;
        $(this).parent().css({"border":"3px solid red"});
      } else {
        $(this).parent().css({"border":""});

      }
    });

    $("#"+div+" .COMBO_OBG1 option:selected").each(function (i){

      if(this.value == "-1" || this.value == ""){
        ok = false;
        $(this).parent().parent().children('div').css({"border":"3px solid red"});
      } else {
        $(this).parent().parent().parent().children('div').css({"border":""});

      }
    });
    $("#"+div+" .OBG_CHOSEN option:selected").each(function (i){

      if(this.value == "" || this.value == -1){
        ok = false;
        $(this).parent().parent().children('div').css({"border":"3px solid red"});
      } else {

        var estilo = $(this).parent().parent().children('div').attr('style');
        $(this).parent().parent().children('div').removeAttr('style');
        $(this).parent().parent().children('div').attr('style',estilo.replace("border: 3px solid red",''));
      }
    });


    if(!ok){
      $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
      $("#"+div+" .div-aviso").show();
    } else {
      $("#"+div+" .aviso").text("");
      $("#"+div+" .div-aviso").hide();
    }
    return ok;
  }
  function listar_itens(){
    if($("#check_plano").is(':checked')){
      var plano=$("#select_plano").val();

    }else{
      var plano = 0;
    }
    $.post("query.php",{query: "listar-itens", cod:$(".bloco2").attr('cod'),tipo: $("input:radio[name=filtrar_por]:checked").attr("t"),inicio:$("#inicio_fatura").val(),fim:$("#fim_fatura").val(),nome:$(".bloco2").attr('nome'),refaturar:0,plano:plano}, function(r){
      $("#dialog-formulario").remove();
      $("#tabela_faturar").html(r);
      load_function();
    });

  }
  function listar_itens2(){
    if($("#check_plano").is(':checked')){
      var plano=$("#select_plano").val();


    }else{
      var plano = 0;
    }
    $.post("query.php",{query: "listar-itens", cod:$(".bloco2").attr('cod'),tipo: $("input:radio[name=filtrar_por]:checked").attr("t"),inicio:$("#inicio_fatura").val(),fim:$("#fim_fatura").val(),nome:$(".bloco2").attr('nome'),refaturar:1,plano:plano}, function(r){
      $("#dialog-formulario").remove();
      $("#tabela_faturar").html(r);
      load_function();
    });

  }

    $(".simular").live('click', function() {
        var $this = $(this);
        var orcamento = $this.attr('orcamento');
        var tipo = $this.attr('tipo');
        var $dialog = $("#dialog-simulacao");
        $dialog.find("div.wrap").html("");

        $.get("query.php",
            {
                query: "buscar-custo-orcamento",
                orcamento: orcamento,
                tipo: tipo
            },
            function(r){
                $dialog.find("div.wrap").html(r);
                $('.valor').priceFormat({
                    prefix: '',
                    centsSeparator: ',',
                    thousandsSeparator: '.'
                });
            }
        );

        if('novo' == tipo) {
            $("#dialog-simulacao").dialog({
                autoOpen: false,
                modal: true,
                width: 1200,
                position: 'top',
                buttons: {
                    "Salvar Simulação": function () {
                        $("#form-simulacao-custo").find('[type="submit"]').trigger('click');
                    },
                    "Cancelar": function () {
                        $(this).dialog("close");
                    }
                }
            });
        } else {
            $("#dialog-simulacao").dialog({
                autoOpen: false,
                modal: true,
                width: 1200,
                position: 'top',
                buttons: {
                    "Cancelar": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
        $dialog.dialog("open");
    });

    $("#calcular-simulacao").live('click', function () {
        var subtotal = 0;
        var valor_total = 0;
        var custo_total = 0;
        var margem_item = 0;

        $(".qtd-simulacao").each(function () {
            var $tr = $(this).parents('tr');
            var qtd = parseInt($(this).val());
            var preco = $tr.find('.preco-simulacao').val();
            var custo_item = parseFloat($tr.find('.valor-custo').val());
            var acrescimo_desconto = parseFloat($tr.find('.acrescimo-desconto').val());

            preco = parseFloat(preco.replace('.', '').replace(',', '.'));

            subtotal = qtd * preco;
            acrescimo_desconto = subtotal * (acrescimo_desconto / 100);
            valor_total += (subtotal + acrescimo_desconto);
            custo_total += (qtd * custo_item);

          if(qtd == 0){
            $tr.find('td:last').css('color', 'orange').html('0,00 %');
          }else if(preco == 0 && custo_item == 0) {
            $tr.find('td:last').css('color', 'orange').html('0,00 %');
          }else if(preco > 0 && custo_item > 0) {
                margem_item = ((((preco - custo_item)*qtd) + acrescimo_desconto ) / (preco*qtd)) * 100;

                if (margem_item < 25) {
                    $tr.find('td:last').css('color', 'red').html(margem_item.toFixed(2).toString().replace('.', ',') + ' %');
                } else {
                    $tr.find('td:last').css('color', 'green').html(margem_item.toFixed(2).toString().replace('.', ',') + ' %');
                }
            } else  {
                var label_custo = preco > custo_item ? '100,00' : '0,00';
                var cor_label_custo = preco > custo_item ? 'green' : 'red';
                $tr.find('td:last').css('color', cor_label_custo).html(label_custo + ' %');
            }


        });
        var pis_cofins = parseFloat($("#pis-cofins").val());
        var ir = parseFloat($("#ir").val());
        var iss = parseFloat($("#iss").val());

        var margem;
        var margem_percentual;

        custo_total = (valor_total * ((pis_cofins + ir + iss) / 100)) + custo_total;
        margem = valor_total - custo_total;
        margem_percentual = (margem / valor_total) * 100;

        if(valor_total >= 4000) {
            $("#alerta-margem").attr('bgcolor', 'orange').html('<b>Orçamento aprovado com ressalva(s)</b>');

            if (margem_percentual < 25 && margem < 1000) {
                $("#alerta-margem").attr('bgcolor', 'red').html('<b>Orçamento reprovado</b>');
            }

            if (margem_percentual >= 25 && margem >= 1000) {
                $("#alerta-margem").attr('bgcolor', 'green').html('<b>Orçamento aprovado</b>');
            }
        } else if(valor_total > 1000 && valor_total < 4000){
            if (margem_percentual < 25) {
                $("#alerta-margem").attr('bgcolor', 'red').html('<b>Orçamento reprovado</b>');
            }

            if (margem_percentual >= 25) {
                $("#alerta-margem").attr('bgcolor', 'green').html('<b>Orçamento aprovado</b>');
            }
        } else if(valor_total <= 1000){
            if (margem_percentual < 15) {
                $("#alerta-margem").attr('bgcolor', 'red').html('<b>Orçamento reprovado</b>');
            }

            if (margem_percentual >= 15) {
                $("#alerta-margem").attr('bgcolor', 'green').html('<b>Orçamento aprovado</b>');
            }
        }

        $("#valor-total").html(valor_total.toFixed(2).toString().replace('.', ','));
        $("#custo-encargo").html(custo_total.toFixed(2).toString().replace('.', ','));
        $("#margem").html(margem.toFixed(2).toString().replace('.', ','));
        $("#margem-percentual").html(margem_percentual.toFixed(2).toString().replace('.', ','));
    });

  $("#faturar_avaliacao").live('click',function(){

    if(validar_campos('div-data-1') && validar_campos('div-data') ) {
      faturar_av1();
    }
    return false;
  });

  function faturar_av1(){

      $.post("query.php",
          {
            query: "fatura-av",
            cod: $("#bloco1_av").attr('cod'),
            nome: $("#bloco1_av").attr('nome'),
            op_avaliacao: $("#bloco1_av").attr('op_avaliacao'),
            inicio: $("#inicio_fatura").val(),
            fim:$("#fim_fatura").val()
          },
          function (r) {
            $("#dialog-formulario").remove();
            $("#tabela_faturar_av").html(r);
            load_function();
          }
      );


  }

  $('.valor').priceFormat({
    prefix: '',
    centsSeparator: ',',
    thousandsSeparator: '.'
  });

  $("#listar_fatura").click(function(){
    $.post("query.php",{query: "listar-fatura", cod:$(".bloco2").attr('cod'),tipo: $("input:radio[name=filtrar_por]:checked").attr("t"),inicio:$("input[name=inicio]").val(),fim:$("input[name=fim]").val(),nome:$(".bloco2").attr('nome')}, function(r){
      $("#tabela_faturar").html(r);
      load_function();
    });
  });

  $("#gerenciar_lote").click(function(){
    $.post("query.php",{query: "gerenciar-lote",tipo_documento:$("#tipo_documento").val(), cod:$(".bloco2").attr('cod'),tipo: $("input:radio[name=filtrar_por]:checked").attr("t"),inicio:$("input[name=inicio]").val(),fim:$("input[name=fim]").val(),nome:$(".bloco2").attr('nome')}, function(r){
      $("#tabela_gerenciar_lote").html(r);
      $(".paciente-lote-mouse-over").easyTooltip();
      load_function();
    });
  });


  $("#listar_fatura_lote").click(function(){
    $.post("query.php",{query: "listar-fatura-lote", cod:$(".bloco2").attr('cod'),tipo: $("input:radio[name=filtrar_por]:checked").attr("t"),inicio:$("input[name=inicio]").val(),fim:$("input[name=fim]").val(),nome:$(".bloco2").attr('nome')}, function(r){
      $("#tabela_faturar_lote").html(r);
      load_function();
    });
  });

  $("#listar_fatura_glosa").click(function(){
    $.post("query.php",{query: "listar-fatura-glosa", cod:$(".bloco2").attr('cod'),tipo: $("input:radio[name=filtrar_por]:checked").attr("t"),inicio:$("input[name=inicio]").val(),fim:$("input[name=fim]").val(),nome:$(".bloco2").attr('nome')}, function(r){
      $("#tabela_faturar_glosa").html(r);
      load_function();
    });
  });

  $("#listar_faturas").click(function(){
    var cancelado=0;
    if($("#visualizar_cancelado").is(":checked")){
      cancelado = 1;
    }
    $.post("query.php",{query: "listar-faturas", cod:$(".bloco2").attr('cod'),tipo: $("input:radio[name=filtrar_por]:checked").attr("t"),
      inicio:$("input[name=inicio]").val(),fim:$("input[name=fim]").val(),nome:$(".bloco2").attr('nome'),inicio_feita:$("#inicio_fatura").val(),
      fim_feita:$("#fim_fatura").val(),cancelado:cancelado,tipo_documento:$('#tipo_documento').val()}, function(r){
      $("#tabela_visualizar_faturar").html(r);
      load_function();
    });
  });

  $(".val_glosa").blur(function(){
    tot_glosa= 0.00;
    val=$(this).val();
    val_fat = $(this).attr('val_fat');
    val1=parseFloat(val.replace('.','').replace(',','.'));
    val_fat1=parseFloat(val_fat.replace('.','').replace(',','.'));
    if( val1 > val_fat1){
      alert('Valor da glosa maior que o valor faturado.');
      $(this).val('0,00')
      return
    }
    tot=$("#tot").attr('tot');
    //  tot = parseFloat(tot);

    if(val !='0,00'){
      $(this).parent().parent().children('td').eq(5).children('select').addClass("OBG");
    }else{
      $(this).parent().parent().children('td').eq(5).children('select').removeClass("OBG");
      $(this).parent().parent().children('td').eq(5).children('select').removeAttr("style");
    }
    $(".val_glosa").each(function(){
      val=$(this).val();
      if(val != '0,00'){

        val =parseFloat(val.replace('.','').replace(',','.'));
        tot_glosa += val;
      }
    });

    tot_glosa= parseFloat(tot_glosa);

    tot_glosa2 = tot_glosa - val1;

    tot_glosa2 =  tot_glosa2 + val1;

    tot_glosa2 = parseFloat(tot_glosa2);
    if(tot_glosa2 > tot){

      // alert("Valor da glosa maior que o da fatura.");
      $("#dialog-glosamaior").dialog('open');
      $(this).val('0,00');
      $(this).focus();

    }

    tot_glosa = float2moeda(tot_glosa);


    $("#tot_glosa").html('<b>TOTAL GLOSA: R$</b> <b><i>'+tot_glosa+'</i></b>');
  });

  function float2moeda(num) {

    x = 0;

    if(num<0) {
      num = Math.abs(num);
      x = 1;
    }
    if(isNaN(num)) num = '0';
    cents = Math.floor((num*100+0.5)%100);

    num = Math.floor((num*100+0.5)/100).toString();

    if(cents < 10) cents = '0' + cents;
    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
      num = num.substring(0,num.length-(4*i+3))+'.'
      +num.substring(num.length-(4*i+3));
    ret = num + ',' + cents;
    if (x == 1) ret = ' - ' + ret;
    return ret;

  }

  $("#salvar_glosa").click(function(){
    if(validar_campos("div-glosa")){
      var total = $(".val_glosa").size();

      glosa='';
      $(".val_glosa").each(function(i){

        val=$(this).val();
        if(val != '0,00'){

          val=parseFloat(val.replace('.','').replace(',','.'));
          id=$(this).attr("id_faturamento");

          motivo=$(this).parent().parent().children('td').eq(5).children('select').val();

          glosa += id+"#"+val+"#"+motivo;


          if(i<total - 1) glosa += "$";
        }

      });

      data= $(".data").val();
      id= $("#div-glosa").attr('id_fatura');


      $.post("inserir.php",{query: "salvar-glosa", glosa:glosa, data:data, idfatura:id }, function(r){
        if (r == 1){
          $("#glosa_fim").html('<b>Previs&atilde;o  informada com sucesso!</b>');
          $("#glosa_fim").dialog('open');
        }else{
          alert(r);
        }

      });
    }
  });

  $(".div-aviso").hide();
  function validar_campos(div){
    var ok = true;
    $("#"+div+" .OBG").each(function (i){
      if(this.value == ""){
        ok = false;
        this.style.border = "3px solid red";
      } else {
        this.style.border = "";
      }
    });
    $("#"+div+" .COMBO_OBG option:selected").each(function (i){
      if(this.value == "-1"){
        ok = false;
        $(this).parent().css({"border":"3px solid red"});
      } else {
        $(this).parent().css({"border":""});

      }
    });

    $("#"+div+" .COMBO_OBG1 option:selected").each(function (i){

      if(this.value == "-1"){
        ok = false;
        $(this).parent().parent().children('div').css({"border":"3px solid red"});
      } else {
        $(this).parent().parent().parent().children('div').css({"border":""});

      }
    });
    $("#"+div+" .OBG_CHOSEN option:selected").each(function (i){

      if(this.value == "" || this.value == -1){
        ok = false;
        $(this).parent().parent().children('div').css({"border":"3px solid red"});
      } else {

        var estilo = $(this).parent().parent().children('div').attr('style');
        $(this).parent().parent().children('div').removeAttr('style');
        $(this).parent().parent().children('div').attr('style',estilo.replace("border: 3px solid red",''));
      }
    });

    if(!ok){
      $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
      $("#"+div+" .div-aviso").show();
    } else {
      $("#"+div+" .aviso").text("");
      $("#"+div+" .div-aviso").hide();
    }
    return ok;
  }

    $("#dialog-justificativa-reabrir").dialog('close');
    $("#dialog-justificativa-reabrir").dialog({
        autoOpen: false,
        modal: true,
        position: 'center',
        buttons: {
            "Reabrir" : function() {
                var fatura = $(this).attr('fatura');
                var paciente = $(this).attr('paciente');
                var tipo = $(this).attr('tipo');
                var periodo = $(this).attr('periodo');
                var justificativa = $("#justificativa-reabrir").val();
                if (fatura != '' && justificativa != '') {
                    $.post(
                        'query.php',
                        {
                            query: 'reabrir-fatura',
                            fatura: fatura,
                            paciente: paciente,
                            tipo: tipo,
                            periodo: periodo,
                            justificativa: justificativa
                        },
                        function (r) {
                            if (r == '1') {
                                var elm_reabrir_img = $("#img-reabrir-" + fatura);
                                elm_reabrir_img
                                    .parent()
                                    .parent()
                                    .find('.status_fatura')
                                    .fadeOut(400)
                                    .fadeIn(400)
                                    .html('PENDENTE');
                                elm_reabrir_img.parent().append("<a href='?view=edt_fatura&id=" + fatura + "&av=0'><img src='../utils/edit.png' width='16' width='20' title='Editar Fatura' idfatura =''></a>");
                                elm_reabrir_img.remove();
                                $("#justificativa-reabrir").val('');
                                alert('Fatura reaberta com sucesso! Favor comunicar ao setor financeiro para refazer o cálculo das margens de contribuição!');
                            } else {
                                alert('Não foi possível reabrir a fatura! Comunique ao setor de TI.');

                            }
                        }
                    );
                    $(this).dialog("close");
                }
            },
          "Cancelar":function(){
            $(this).dialog('close');
            return false;
          }
        }
    });

    $(".reabrir_fatura").live('click', function(){
        $("#dialog-justificativa-reabrir").dialog('open');
        $("#dialog-justificativa-reabrir").attr('fatura', $(this).attr('fatura'));
      $("#dialog-justificativa-reabrir").attr('tipo', $(this).attr('tipo'));
      $("#dialog-justificativa-reabrir").attr('paciente', $(this).attr('paciente'));
      $("#dialog-justificativa-reabrir").attr('periodo', $(this).attr('periodo'));

    });



  $("#glosa_fim").dialog('close');
  $("#glosa_fim").dialog({
    autoOpen: false, modal: true, position: 'top',
    buttons: {
      "OK" : function(){

        $(this).dialog("close");
        window.location.href='?view=informarglosa';
      },

    }
  });
  $("#dialog-glosamaior").dialog('close');
  $("#dialog-glosamaior").dialog({
    autoOpen: false, modal: true, position: 'top',
    buttons: {
      "OK" : function(){

        $(this).dialog("close");

      },

    }
  });



  $("#dialog-visualizar-glosa").hide();

  $(".tipo_v").click(function(){
    $("#dialog-visualizar-glosa").attr('tipo',$(this).val());
  });


  $('#tipos').buttonset();

  $('#tipos input[name=tipos]').click(function(){
    tipo= $('#tipos input[name=tipos]:checked').val();

    $('#dialog-formulario').attr('tipo', tipo);
    $('#dialog-formularioav').attr('tipo', tipo);
    $('#dialog-formulario-orcamento').attr('tipo',tipo);
    $('#dialog-formulario-aditivo').attr('tipo',tipo);
  });



  $('#add-item-faturaav').click(function(){
    if (validar_campos('div_fat_avulsa')){
      $('#inicio').attr('value',$(this).attr('inicio'));
      $('#fim').attr('value',$(this).attr('fim'));
      $('#dialog-formularioav').attr('cod', $(this).attr('id'));
      $('#dialog-formularioav').attr('catalogo_id',$(this).attr('catalogo_id'));
      $('#dialog-formularioav').attr('plano',$('#select_planoavu').val());
      var empresa = $('#select_unidade').val();
      if(empresa === undefined){
        empresa = $('#select_unidadeav').val();
      }
      $('#dialog-formularioav').attr('empresa',empresa);
      $('#dialog-formularioav').dialog('open');
    }else {
      return false;
    }
  });

  $('#dialog-formularioav').dialog('close');


  $('#dialog-formularioav').dialog({
    autoOpen: false, modal: true, width: 800, position: 'top',
    open: function(event,ui){
      $('#tipos').buttonset();
      $('#dialog-formularioav').attr('tipo', $('#tipos input[name=tipos]:checked').val());
      $('input[name=busca]').val('');
      $('#nome').val('');
      $('#nome').attr('readonly','readonly');
      $('#apr').val('');
      $('#apr').attr('readonly','readonly');
      $('#cod').val('');
      $('#tipo').val(0);
      $('#catalogo_id').val('');
      $('#custo').val('');
      $('#valor_plano').val('');
      $('#qtd').val('');
      $('#tuss').val('');
      $('input[name=buscaav]').focus();
    },
    buttons: {
      'Fechar' : function(){
        var item = $('#dialog-formularioav').attr('item');
        if(item != 'avulsa'){
          $('#kits-'+item).hide();
          $('#med-'+item).hide();
          $('#mat-'+item).hide();
          $('#serv-'+item).hide();
        }
        $(this).dialog('close');
      }
    }
  });


  $('#add-itemav').click(function() {

    if (validar_campos('dialog-formularioav'))
    {
      var cod = $('#cod').val();
      var catalogo_id = $('#catalogo_id').val();
      var nome = $('#dialog-formularioav input[name=nome]').val();
      var tabela_origem = $('#tabela_origem').val();
      var tipo = $('#tipo').val();
      var custo = $('#custo').val();
      var contador = parseInt($('#contador').val())+1;

      if (tipo == 'med' || tipo==0) {
        tipo = 0;
        apresentacao = 'MEDICAMENTO';
      } else if (tipo == 'mat' || tipo == 1)
      {
        tipo = 1;
        apresentacao = 'MATERIAL';
      } else if (tipo == 'serv' || tipo == 12 || tipo == 'ext' || tipo == 2)
      {
        tipo = 2;
        apresentacao = 'SERVI&Ccedil;OS';
      } else if (tipo == 'eqp' || tipo == 5)
      {
        tipo = 5;
        apresentacao = 'EQUIPAMENTO';
      } else if (tipo == 'gas' || tipo == 6)
      {
        tipo = 6;
        apresentacao = 'GASOTERAPIA';
      } else if (tipo == 'die' || tipo == 3)
      {
        tipo = 3;
        apresentacao = 'DIETA';
      }
      var tuss =$('#tuss').val();
      var qtd = $('#dialog-formularioav input[name=qtd]').val();
      var paciente = $('#dialog-formularioav').attr('cod');
      var inicio = $('#inicio').val();
      var fim = $('#fim').val();
      var valor_plano = $('#valor_plano').val();

      if(tipo != 'kit') {
          var valor_total = qtd * parseFloat(valor_plano.replace('.', '').replace(',', '.'));
          var valor_total = float2moeda(valor_total);

          var dados = 'nome=' + nome + ' tipo=' + tipo + ' paciente=' + paciente + ' cod=' + cod + ' qtd=' + qtd + 'catalogo_id=' + catalogo_id;
          var linha = nome;
          $('#' + paciente).attr('contador', contador);
          $('#contador').val(contador);
          $('#add-item-faturaav').attr('contador', contador);
          var cont_unid = $('#cont-unid') ? $('#cont-unid').val() : 1;
          var botoes = '<img align=\'right\' class=\'rem-item-orcamento-avulso\' editar=\'0\'  src=\'../utils/delete_16x16.png\' title=\'Remover\' border=\'0\'>';
          $('#tabela_fatavulsa').append(
              '<tr><td>TUSS-' + tuss + '  N-' + linha + botoes + '</td><td>' + apresentacao + '</td>' +
              '<td class=\'unidade_fat\' id=' + paciente + '_' + contador + '></td>' +
              '<td><input class=\'qtd\' size=\'7\' value=' + qtd + ' paciente=' + paciente + ' /></td>' +
              '<td>R$<input type=text name=valor_plano size=10 class=\'valor_plano paciente valor OBG\' tiss=\'' + cod + '\'qtd=\'' + qtd + '\'  catalogo_id=\'' + catalogo_id + '\' tuss=\'' + tuss + '\'  tabela_origem=\'' + tabela_origem + '\' tipo=\'' + tipo + '\' custo =\'' + custo + '\'  ord =\'\' flag=\'\'  value=\'' + valor_plano + '\' cod_ref=\'0\' editado=\'0\'></td>' +
              '<td><span style=\'float:right;\' id=\'val_total\'><b>R$  <span nome=\'val_linha\'>' + valor_total + '</span></b></span></td>' +
              '</tr>');

          $.ajax({
              url: '/faturamento/query.php?query=buscar-unidade-medida-fatura',
              success: function (response) {
                  response = JSON.parse(response);
                  var select_unidades = '<select name="unidade" class="unidade">' +
                      '<option value="-1">Selecione</option>';
                  $.each(response, function (index, value) {
                      select_unidades += '<option value="' + index + '">' + value.unidade + '</option>';
                  });
                  select_unidades += '</select>';
                  $('#' + paciente + '_' + contador).append(select_unidades);
              },
              async: true
          });

          $('.valor').priceFormat({
              prefix: '',
              centsSeparator: ',',
              thousandsSeparator: '.'
          });

          $('.rem-item-solicitacao').unbind('click', remove_item).bind('click', remove_item);
      } else {
          var plano = $('#dialog-formulario').attr('plano');
          var select_unidades = '';
          $.ajax({
              url: '/faturamento/query.php?query=buscar-unidade-medida-fatura',
              success: function (response) {
                  response = JSON.parse(response);
                  select_unidades = '<select name="unidade" class="unidade">' +
                      '<option value="-1">Selecione</option>';
                  $.each(response, function (index, value) {
                      select_unidades += '<option value="' + index + '">' + value.unidade + '</option>';
                  });
                  select_unidades += '</select>';
              }
          });
          $.ajax({
              url: '/faturamento/query.php?query=buscar-itens-kit-fatura&kit_id=' + catalogo_id + '&plano_id=' + plano + '&tipo_documento=',
              success: function (response) {
                  response = JSON.parse(response);
                  console.log(select_unidades);
                  $.each(response, function (index, value) {
                      var qtd_item = value.qtd * qtd;
                      catalogo_id = index;
                      var valor_total = qtd_item * parseFloat(value.val);
                      var formatter = new Intl.NumberFormat('pt-BR', {
                          style: 'currency',
                          currency: 'BRL',
                          minimumFractionDigits: 2,
                      });
                      var valor_total = formatter.format(valor_total);

                      var dados = 'nome=' + value.nome + ' tipo=' + value.tipo + ' paciente=' + paciente + ' cod=' + value.tiss + ' qtd=' + qtd_item + 'catalogo_id=' + catalogo_id;
                      var linha = value.nome;
                      $('#' + paciente).attr('contador', contador);
                      $('#contador').val(contador);
                      $('#add-item-faturaav').attr('contador', contador);
                      var cont_unid = $('#cont-unid') ? $('#cont-unid').val() : 1;
                      var botoes = '<img align=\'right\' class=\'rem-item-orcamento-avulso\' editar=\'0\'  src=\'../utils/delete_16x16.png\' title=\'Remover\' border=\'0\'>';
                      $('#tabela_fatavulsa').append(
                          '<tr><td>TUSS-' + value.tuss + '  N-' + linha + botoes + '</td>' +
                          '<td>' + value.apresentacao + '</td>' +
                          '<td class=\'unidade_fat\'>' + select_unidades + '</td>' +
                          '<td><input class=\'qtd\' size=\'7\' value=' + qtd_item + ' paciente=' + paciente + ' /></td>' +
                          '<td>R$<input type=text name=valor_plano size=10 class=\'valor_plano paciente valor OBG\' tiss=\'' + value.tiss + '\'qtd=\'' + qtd_item + '\'  catalogo_id=\'' + catalogo_id + '\' tuss=\'' + value.tuss + '\'  tabela_origem=\'\' tipo=\'' + value.tipo + '\' custo =\'' + value.custo + '\'  ord =\'\' flag=\'\'  value=\'' + value.val + '\' cod_ref=\'0\' editado=\'0\'></td>' +
                          '<td><span style=\'float:right;\' id=\'val_total\'><b><span nome=\'val_linha\'>' + valor_total + '</span></b></span></td>' +
                          '</tr>'
                      );

                      contador = parseInt($('#contador').val())+1;

                      $('.valor').priceFormat({
                          prefix: '',
                          centsSeparator: ',',
                          thousandsSeparator: '.'
                      });

                      $('.rem-item-solicitacao').unbind('click', remove_item).bind('click', remove_item);
                  });
              }
          });
      }


      $('#tabela_origem').val('');
      $('#cod').val('-1');
      $('#catalogo_id').val('');
      $('#dialog-formularioav input[name=nome]').val('');
      $('#dialog-formularioav input[name=qtd]').val('');
      $('#busca-itemav').val('');
      $('#nome').val('');
      $('#qtd').val('');
      $('#tuss').val('');
      $('#tipo').val('');
      calcula_linha();
      calcula_linha2();
    }
    return false;
  });

  $('input[name=buscaav]').autocomplete({
    source: function(request, response) {
      $.getJSON('busca_brasindice_fatura.php', {
        term: request.term,
        tipo: $('#dialog-formularioav').attr('tipo'),
        plano:$('#dialog-formularioav').attr('plano'),
        empresa:$('#dialog-formularioav').attr('empresa')
      }, response);
    },
    minLength: 3,
    select: function( event, ui ) {
      $('#busca-itemav').val('');
      $('#nome').val(ui.item.value);
      $('#cod').val(ui.item.id);
      $('#tipo').val(ui.item.tipo_aba);
      $('#catalogo_id').val(ui.item.catalogo_id);
      $('#apresentacao').val(ui.item.apresentacao);
      $('#custo').val(ui.item.custo);
      $('#valor_plano').val(ui.item.valor_receber);
      $('#tabela_origem').val(ui.item.tabela_origem);
      $('#tuss').val(ui.item.tuss);
      $('#qtd').focus();
      $('#busca-itemav').val('');
      return false;
    },
    extraParams: {
      //tipo: 2
    }
  });


  $('input[name=busca-orcamento]').keypress(function() {
    $(this).autocomplete({
      source: function (request, response) {
        $.getJSON('busca_brasindice_fatura.php', {
          term: request.term,
          tipo: $('#dialog-formulario-orcamento').attr('tipo'),
          plano: $('#dialog-formulario-orcamento').attr('plano'),
          empresa: $('#dialog-formulario-orcamento').attr('empresa'),
          tipo_medicamento: $('#dialog-formulario-orcamento').attr('tipo_medicamento'),
          tipo_documento: $('#dialog-formulario-orcamento').attr('tipo-documento'),
        }, response);
      },
      minLength: 3,
      select: function (event, ui) {

        $('#busca-item-orcamento').val('');
        $('#nome').val(ui.item.value);
        $('#cod').val(ui.item.id);
        $('#tipo').val(ui.item.tipo_aba);
        $('#catalogo_id').val(ui.item.catalogo_id);
        $('#apresentacao').val(ui.item.apresentacao);
        $('#custo').val(ui.item.custo);
        $('#valor_plano').val(ui.item.valor_receber);
        $('#tabela_origem').val(ui.item.tabela_origem);
        $('#tuss').val(ui.item.tuss);
        $('#qtd').focus();
        $('#busca-item-orcamento').val('');
        return false;
      },
      extraParams: {
        tipo: 2
      }
    }).data('autocomplete').
        _renderItem = function (ul, item) {
      var cor = '';
      if (item.textRed == 'S')
        cor = 'color:red';
      return $("<li>").data('item.autocomplete', item)
          .append("<a style='" + cor + "'>" + item.value + "</a>")
          .appendTo(ul);
    }
  });



  $('.rem-item-solicitacao').unbind('click',remove_item)
    .bind('click',remove_item);
  function atualiza_rem_item(){
    $('.rem-item-solicitacao').unbind('click',remove_item)
      .bind('click',remove_item);
  }
  function remove_item(){
    if (confirm('Deseja realmente remover?')){
      var data = $(this).parent().parent().children('td').eq(4).children('input');

      paciente        = data.attr('paciente');
      tipo            = data.attr('tipo');
      tiss            = data.attr('tiss');
      catalogo_id     = data.attr('catalogo_id');
      fim             = data.attr('fim');
      inicio          = data.attr('inicio');
      faturamento_id  = data.attr('codf');
      val             = $(this).parent().parent().children('td').eq(5).children('span').children('b').children('span').text();

      if (tipo == 5) {
        sub_equipamento = $('#equipamento_'+paciente).text();
        sub_equipamento =  parseFloat(sub_equipamento.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));
        sub_equipamento=float2moeda(sub_equipamento);
        $('#equipamento_'+paciente).text(sub_equipamento);
      }

      if (tipo == 1) {
        sub_material = $('#material_'+paciente).text();
        sub_material =  parseFloat(sub_material.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));
        sub_material=float2moeda(sub_material);
        $('#material_'+paciente).text(sub_material);
      }
      if (tipo == 2) {
        sub_servico = $('#servico_'+paciente).text();
        sub_servico =  parseFloat(sub_servico.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));
        sub_servico=float2moeda(sub_servico);
        $('#servico_'+paciente).text(sub_servico);
      }
      if (tipo == 0) {
        sub_medicamento = $('#medicamento_'+paciente).text();
        sub_medicamento =  parseFloat(sub_medicamento.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));
        sub_medicamento=float2moeda(sub_medicamento);
        $('#medicamento_'+paciente).text(sub_medicamento);
      }
      sub_total=$('.sub_fatura_'+paciente).children('b').eq(1).children('i').text();
      sub_valor=  parseFloat(sub_total.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));
      sub_valor=float2moeda(sub_valor);
      $('.sub_fatura_'+paciente).html('<b>TOTAL PACIENTE: R$</b> <b><i>'+sub_valor+'</i></b>');
      val_total=$('#total_fatura').children('b').children('i').text();
      total_valor=  parseFloat(val_total.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));
      total_valor= float2moeda(total_valor);
      $('#total_fatura').html('<b>TOTAL: R$</b> <b><i>'+total_valor+'</i></b>');
      $(this).parent().parent().remove();
      $.post('inserir.php',{query:'update-remove', inicio:inicio, fim:fim, paciente:paciente, tipo:tipo, tiss:tiss,catalogo_id:catalogo_id,faturamento_id:faturamento_id},function(r){
      });

    }
    return false;
  }

  function remove_item_novo(){

      var data = $(this).parent().parent().children('td').eq(4).children('input');

      paciente        = data.attr('paciente');
      tipo            = data.attr('tipo');
      tiss            = data.attr('tiss');
      catalogo_id     = data.attr('catalogo_id');
      fim             = data.attr('fim');
      inicio          = data.attr('inicio');
      faturamento_id  = data.attr('codf');
    
      val             = $(this).parent().parent().children('td').eq(5).children('span').children('b').children('span').text();

      if (tipo == 5) {
        sub_equipamento = $('#equipamento_'+paciente).text();
        sub_equipamento =  parseFloat(sub_equipamento.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));
        sub_equipamento=float2moeda(sub_equipamento);
        $('#equipamento_'+paciente).text(sub_equipamento);
      }

      if (tipo == 1) {
        sub_material = $('#material_'+paciente).text();
        sub_material =  parseFloat(sub_material.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));
        sub_material=float2moeda(sub_material);
        $('#material_'+paciente).text(sub_material);
      }
      if (tipo == 2) {
        sub_servico = $('#servico_'+paciente).text();
        sub_servico =  parseFloat(sub_servico.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));
        sub_servico=float2moeda(sub_servico);
        $('#servico_'+paciente).text(sub_servico);
      }
      if (tipo == 0) {
        sub_medicamento = $('#medicamento_'+paciente).text();
        sub_medicamento =  parseFloat(sub_medicamento.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));
        sub_medicamento=float2moeda(sub_medicamento);
        $('#medicamento_'+paciente).text(sub_medicamento);
      }
      sub_total=$('.sub_fatura_'+paciente).children('b').eq(1).children('i').text();
      sub_valor=  parseFloat(sub_total.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));
      sub_valor=float2moeda(sub_valor);
      $('.sub_fatura_'+paciente).html('<b>TOTAL PACIENTE: R$</b> <b><i>'+sub_valor+'</i></b>');
      val_total=$('#total_fatura').children('b').children('i').text();
      total_valor=  parseFloat(val_total.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));
      total_valor= float2moeda(total_valor);
      $('#total_fatura').html('<b>TOTAL: R$</b> <b><i>'+total_valor+'</i></b>');
      $(this).parent().parent().remove();



  }

  $('.rem-item').bind('click',remove_item);
  $('.div-aviso').hide();

  $("#select_planoavu").change(function(){

    $(".bloco2").attr('cod',$(this).val());
    // $(".bloco2").attr('nome',$(this).find('option').filter(':selected').text());
  });
  $("#salvar-faturaav").click(function(){

    if(validar_campos("div_fat_avulsa")){
      itens='';
      tam=$('.valor_plano').size();
      cont=0;
      var paciente = $("#paciente_avulsa").val();
      var cpf = $("#cpf-avulsa").val();
      var endereco = $("#endereco-avulsa").val();

      $('.valor_plano').each(function(){

        sol = $(this).attr('solicitacao');
        tiss = $(this).attr('tiss');
        qtd = $(this).attr('qtd');
        tipo = $(this).attr('tipo');
        flag = $(this).attr('flag');
        catalogo_id=$(this).attr('catalogo_id');

        usuario = $(this).attr('usuario');
        valor_fatura = $(this).val();
        valor_fatura = parseFloat(valor_fatura.replace('.','').replace(',','.'));
        valor_custo = $(this).attr('custo');
        inicio = $(this).attr('inicio');
        fim = $(this).attr('fim');
        unidade= $(this).parent().parent().children('td').eq(2).children('select').children('option:selected').val();
        tabela_origem = $(this).attr('tabela_origem');
        tuss = $(this).attr('tuss');

        cont++;

        if(cont==tam)
          itens += tiss+'#'+qtd+'#'+tipo+'#'+valor_fatura+'#'+valor_custo+'#'+unidade+'#'+catalogo_id+'#'+tabela_origem+'#'+tuss;
        else
          itens += tiss+'#'+qtd+'#'+tipo+'#'+valor_fatura+'#'+valor_custo+'#'+unidade+'#'+catalogo_id+'#'+tabela_origem+'#'+tuss+'$';

      });

      $.post('inserir.php',{query:'gravar-fatura-avulsa',
        dados: itens,
        inicio:$("input[name=inicio]").val(),
        fim:$("input[name=fim]").val(),
        paciente:paciente,
        cpf:cpf,
        endereco:endereco,
        plano:$("#select_planoavu").val(),
        empresa:$("#select_unidade").val(),
        observacao:$("#observacao_avulsa").val()},function(r){
        if( r == 1){
          alert("Salvo com sucesso!");
          window.location.href='?view=faturaavulsa';
        }else{
          alert(r);
        }
      });
    }
  });
  $("#div_fat_avulsa").hide();
  $("#div_list_avulsa").hide();
  $("#tpacient").click(function(){
    $("#tb_list").remove();
    if($(this).is(':checked')){
      $("#select_paciente_avu").hide();

    }else{
      $("#select_paciente_avu").show();
    }

  });

  $("#pesquisar_avulsa").click(function(){
    if($("#tpacient").is(':checked')){
      val='t';

    }else{
      val= $("#select_paciente_avu").val()
    }
    $.post('faturamento.php',{query:'list-avulsa',val:val},function(r){
      $("#div-list").html(r);
      load_function();
    });
  });

  $("#sel_fat_avu").change(function(){
    $("#tb_list").remove();
    $("#paciente_avulsa").val('');
    $("#select_planoavu").val('');
    $("input[name=inicio]").val('');
    $("input[name=fim]").val('');
    $(".valor_plano").parent().parent().remove();
    val= $(this).val();

    if(val == 1){
      $("#div_fat_avulsa").hide();
      $("#div_list_avulsa").show();
    }else{
      $("#div_fat_avulsa").show();
      $("#div_list_avulsa").hide();
    }

  });

  $("#select_paciente_avu").change(function(){
    $("#tb_list").remove();

  });

  ////atualiza pre�o jeferson
  $("#tipo_brasindice").change(function(){
    $("res-b").remove();
    var x = $(this).val();

    if(x != -1){
      $("#busca_brasindice_span").show();
      $('#item-busca-brasindice').addClass('OBG');
    }else{
      $("#busca_brasindice_span").hide();
      $('#item-busca-brasindice').removeClass('OBG');
      $('#item-busca-brasindice').removeAttr('style');
    }

  });

  $("#pesquisa-brasindice").click(function(){

    pesquisa_brasindice();

  });

  function pesquisa_brasindice() {
    var ur = $("#ur").val();
    var tipo = $("#tipo_brasindice").val();
    var item = $("input[name='item_busca_brasindice']").val();
    //$_SESSION['item_busca_brasindice'] = item;

    $.post("query.php",{query:'brasindice',ur:ur,item:item,tipo:tipo},function(r){
      $("#brasindice_resultado").html(r);
      load_function();
    });

  }//pesquisa_brasindice
  function pesquisa_unidade(obj) {


    $.post("query.php",{query:'unidades-fatura'},function(r){
      $("#"+obj).html(r);
      load_function();
    });

  }//pesquisa_brasindice

  $(".brasindice_editar").live('click',function() {



    var numero_tiss = $(this).parent().parent().children('td').eq(6).children('input').attr('tiss');
    var catalogo_id = $(this).parent().parent().children('td').eq(6).children('input').attr('catalogo_id');
    var tipo_brasindice = $(this).parent().parent().children('td').eq(6).children('input').attr('tipo');

    var valor_item =  parseFloat($(this).parent().parent().children('td').eq(6).children('input').val().replace('.','').replace(',','.'));

    $.post("inserir.php",{query:'editar-brasindice',valor_item:valor_item,
      numero_tiss:numero_tiss, tipo_brasindice:tipo_brasindice, catalogo_id:catalogo_id
    },function(r){

      if(r >= 0){
        $("#"+r).remove();
      }else{

        alert("Falha na gravação! Verifique os campos.");
      }

    });


    return false;

  });

    $('.load_salva').click(function() {
        var id_paciente = $(this).attr('id_paciente');
        var fatura = $(this).attr('fatura');
        $.post("query.php", {query: 'load_fatura_salva', id_paciente: id_paciente,fatura:fatura
        }, function(r) {
            string=r.split('$*|*$');

            $('#inicio_fatura').val(string[1]);
            $('#fim_fatura').val(string[2]);
            $("#tabela_faturar").html(string[0]);
            load_function();
        });
    });
  //tualiza pre�o
  ///////////////////////////////////////////////faturar para funcionar no salvar

// IMPRIMIR ORCAMENTO COM VALOR DA DIÁRIA, PASSANDO O PARAMETRO DE DIAS
  $('#imprimir_orcamento_dias').live('click',function(){

    var cod = $(this).attr('cod');

    $('#dialog-formulario-orcamento-dias').attr('cod',cod);
    $('#dialog-formulario-orcamento-dias').dialog('open');
  });

//$('#dialog-formulario-orcamento-dias').dialog('close');

  $('#dialog-formulario-orcamento-dias').dialog({
    autoOpen: false,
    modal: true,
    width: 400,
    position: 'top',
    buttons: {
      'Gerar' : function(){
        var dias = $('#dias').val();
        var cod = $(this).attr('cod');
        window.open('imprimir_fatura.php?id='+cod+'&av=1&zerados=1&dias='+dias,'_blank');

        $(this).dialog('close');
      }
    }
  });

  /* USANDO A FUNÇAO SELECTIZE*/

////////////////mudar cor do item já editado na fatura acontece quando muda o valor 13-01-2014
  $('.valor_plano').blur(function(){
    $(this).parent().parent().attr('bgcolor','#87CEFA');

  });


////////////////////////////

/////////busca no select paciente 20-01-2013
  $('#select_paciente').chosen({search_contains: true});
  $('#select_paciente_prorrogacao').chosen({search_contains: true});
  $('#select_paciente_avaliacao').chosen({search_contains: true});
///
  $('#select_paciente_chosen').hide();
  $('#select_paciente').hide();

  /////////////////////////////////jeferson cadastro de custo plano/ur 23-01-2013
  $('#pesquisar-custo-plano-ur').click(function(){
    if (validar_campos('custo-plano-ur')){

      var unidade = $('#select_unidade').val();
      $.post("query.php", {query: 'pesquisar-custo-plano-ur', ur: unidade
      }, function(r){
        $("#tabela-custo-plano-ur").html(r);
        load_function();
      });
    }else{
      return false;
    }
  });

  $('#pesquisar-historico-custo-plano-ur').click(function(){

    if (validar_campos('custo-plano-ur')){

      var unidade = $('#select_unidade').val();
      var item = $('#select_item_custo_plano_ur').val();
      $.post("query.php", {query: 'pesquisar-historico-custo-plano-ur', ur: unidade, item:item
      }, function(r){
        $("#tabela-custo-plano-ur").html(r);
        load_function();
      });
    }else{
      return false;
    }
  });

  $('#exportar_fatura_condensada').click(function(){
    if (validar_campos('div_fatura_condensada')) {

      window.open('exportar_fatura_condensada.php?ur='+$('#select_unidade').val()+'&inicio='+$('#inicio_fatura_condensada').val()+'&fim='+$('#fim_fatura_condensada').val(),'_blank');
    }else{
      return false;
    }
  });

  $('.cancelar-fatura').live("click",function(){
    id_fatura= $(this).attr('idfatura');
    var elemento = this;
    // $(this).closest('tr').remove();
    $.post("inserir.php", {query: 'cancelar-fatura', id_fatura:id_fatura
    }, function(r){
      if(r == 1){

        $(elemento).closest('tr').remove();
        alert('Cancelado com Sucesso!');


      }else{
        alert(r);
      }
    });
    return false;
  });

  $('.editado').click(function(){
    $(this).children('td').eq(4).children('input').attr('editado','1');

  });

  $('.editado').keydown(function(){
    $(this).children('td').eq(4).children('input').attr('editado','1');

  });

  // $('.taxa').priceFormat({
  //
  //   prefix: '',
  //   centsSeparator: ',',
  //   thousandsSeparator: '.',
  //   allowNegative: true
  // });
    $('.taxa').maskMoney({
        prefix: '',
        decimal: ',',
        thousands: '.',
        allowNegative: true,
        allowZero: true
    });


    $('#salvar-custo-plano-ur').live('click',function(){
    var total = $('.valor').size();
    var cont=0;
    var itens='';
    $('.valor').each(function(){
      var ur =$(this).attr('ur');
      var plano=$(this).attr('plano');
      var custo=$(this).val().replace('.','').replace(',','.');
      var cod = $(this).attr('cod');
      var item =$(this).attr('id_item');
      var id_atual =$(this).attr('id_atual');
      var custo_anterior = $(this).attr('custo_anterior');
      cont++;
      if(cont==total)
        itens += ur+'#'+custo+'#'+plano+'#'+item+'#'+id_atual+'#'+custo_anterior;
      else
        itens += ur+'#'+custo+'#'+plano+'#'+item+'#'+id_atual+'#'+custo_anterior+'$';
    });

    $.post("inserir.php", {query: 'salvar-custo-plano-ur', itens:itens
    }, function(r){
      if(r == 1){
        alert('Salvo com Sucesso!');
        $('#select_plano').val('-1');
        $('#select_unidade').val('-1');
        $("#tabela-custo-plano-ur").html('');
      }else{
        alert(r);
      }
    });


  });



  $(".rem-item-orcamento-avulso").live('click',function(){

    var editar =$(this).attr('editar');
    var  cod_referencia =$(this).parent().parent().children('td').eq(4).children('input').attr('cod_ref');
    var objeto =  $(this)
    if(editar==1){
      $.post("inserir.php",{query:'cancelar-item-orcamento-avulso',cod_referencia:cod_referencia},
        function(r){
          if(r==1){
            alert('Item cancelado');
            objeto.parent().parent().remove();
          }else{
            alert(r);
          }
        }
      );

    }else{
      $(this).parent().parent().remove();
    }

  });

  $("#editar-faturaav").click(function(){
      tam=$('.valor_plano').size();
      cont=0;
      itens='';
      id_orcamento =$(this).attr('id_orcamento');
        empresa = $(this).attr('empresa');
      observacao=$("#observacao_avulsa").val();
      if(validar_campos('editar-orcamento-avulsa')){
        $('.valor_plano').each(function(){


          tiss = $(this).attr('tiss');
          qtd = $(this).attr('qtd');
          tipo = $(this).attr('tipo');
          flag = $(this).attr('flag');
          catalogo_id=$(this).attr('catalogo_id');

          usuario = $(this).attr('usuario');
          valor_fatura = $(this).val();
          valor_fatura = parseFloat(valor_fatura.replace('.','').replace(',','.'));
          valor_custo = $(this).attr('custo');
          inicio = $(this).attr('inicio');
          fim = $(this).attr('fim');
          unidade= $(this).parent().parent().children('td').eq(2).children('select').children('option:selected').val();
          tabela_origem = $(this).attr('tabela_origem');
          editado = $(this).attr('editado');
          cod_referencia= $(this).attr('cod_ref');
          var tuss =$(this).attr('tuss');

          cont++;

          if(cont==tam)
            itens += tiss+'#'+qtd+'#'+tipo+'#'+valor_fatura+'#'+valor_custo+'#'+unidade+'#'+catalogo_id+'#'+tabela_origem+'#'+editado+'#'+cod_referencia+'#'+tuss;
          else
            itens += tiss+'#'+qtd+'#'+tipo+'#'+valor_fatura+'#'+valor_custo+'#'+unidade+'#'+catalogo_id+'#'+tabela_origem+'#'+editado+'#'+cod_referencia+'#'+tuss+'$';

        });

        $.post('inserir.php',{query:'editar-orcamento-avulso', dados: itens, inicio:$("input[name=inicio]").val(),
          fim:$("input[name=fim]").val(),id_orcamento:id_orcamento,observacao:observacao, empresa:empresa},function(r){
          if( r == 1){
            alert("Editado com sucesso!");
            window.location.href='?view=faturaavulsa';
          }else{
            alert(r);
          }
        });

      }else{
        return false;
      }

    }
  );

  $('.load_salva_editar').click(function() {
    var arquivo = $(this).attr('arquivo');
    var fatura = $(this).attr('fatura');
    $.post("query.php", {query: 'load_fatura_salva_editar', arquivo: arquivo,fatura:fatura
    }, function(r) {
      string=r.split('$*|*$');
      $('#inicio_fatura').val(string[1]);
      $('#fim_fatura').val(string[2]);
      $("#tabela_faturar").html(string[0]);

    });


  });
  $('.qtd_diaria_orcamento_avulso').live('click',function(){
    var id_orcamento_avulso= $(this).attr('orcamento_avulso_id');
    $('#dialog-diaria-orcamento-avulso').attr('idorcamento',id_orcamento_avulso);
    $('#dialog-diaria-orcamento-avulso').dialog('open');

  });
  $('#dialog-diaria-orcamento-avulso').dialog({
    autoOpen: false, modal: true, position: 'top',
    buttons: {
      'Visualizar' : function(){
        var p = $('#dialog-diaria-orcamento-avulso').attr('idorcamento');
        var diaria= $('#diarias_orcamento_avulso').val();
        var observacao = $("#dialog-diaria-orcamento-avulso select[name='observacao']").val();
        var empresa = $("#select-escolher-empresa-imprimir-orcamento").val();
        window.open('imprimir_orcamento_avulso.php?id='+p+'&av=0&d='+diaria+'&observacao='+observacao+'&empresa='+empresa,'_blank');
        $('#dialog-diaria-orcamento-avulso').dialog('close');

      }
    }
  });


  function load_function(){


///////// busca no select paciente 20-01-2013
    $('#select_paciente').chosen({search_contains: true});
///////


    $(".ocultar-devolucao").hide();
    $(".mostrar-devolucao").toggle(
        function(){

          $("#tbody-devolucao").show();

        }, function(){


          $("#tbody-devolucao").hide();

        });
    $(".ocultar-envio").hide();
    $(".mostrar-envio").toggle(
        function(){

          $("#tbody-envio").show();

        }, function(){


          $("#tbody-envio").hide();

        });
    $(".ocultar-equipamento-ativo").hide();
    $(".mostrar-equipamento-ativo").toggle(
        function(){

          $("#tbody-equipamento-ativo").show();

        }, function(){


          $("#tbody-equipamento-ativo").hide();

        });




    function email_fatura(){
      p = $('#dialog-email').attr('lote');
      $.post('inserir.php',{query:'enviar-email-fatura', lote:p},function(r){
        alert(r);
      });

    }

    function update_status(){
      p = $('#dialog-email').attr('lote');
      $.post('inserir.php',{query:'update-status-fatura', lote:p},function(r){

        $('#'+r).html('<b>Enviado</b>');

      });

    }



    $('.finalizar-glosa').click(function(){
      x=$(this).attr('idfatura');
      $.post('inserir.php',{query:'finalizar-glosa', fatura:x},function(r){
        $.post('query.php',{query: 'listar-fatura-glosa',cod:$('.bloco2').attr('cod'),tipo: $('input:radio[name=filtrar_por]:checked').attr('t'),inicio:$('input[name=inicio]').val(),fim:$('input[name=fim]').val(),nome:$('.bloco2').attr('nome')},function(y){
          $('#tabela_faturar_glosa').html(y);

        });
      });


    });

    $('.itens_fatura').live('click',function(){

      $('.itens_fatura').each(function(){
        $(this).attr('bgcolor','');
        if($(this).children('td').eq(2).children('select[name=unidade]').val() != ''
          && $(this).children('td').eq(2).children('select[name=unidade]').val() != -1 ){
          $(this).attr('bgcolor','#87CEFA');
        }

      })

      $(this).attr('bgcolor','#FAF0E6');
    });



    function validar_campos(div){
      var ok = true;
      $('#'+div+' .OBG').each(function (i){
        if(this.value == ''){
          ok = false;
          this.style.border = '3px solid red';
        } else {
          this.style.border = '';
        }
      });
      $('#'+div+' .COMBO_OBG option:selected').each(function (i){
        if(this.value == '-1'){
          ok = false;
          $(this).parent().css({'border':'3px solid red'});
        } else {
          $(this).parent().css({'border':''});
        }
      });
      $("#"+div+" .OBG_CHOSEN option:selected").each(function (i){

        if(this.value == "" || this.value == -1){
          ok = false;
          $(this).parent().parent().children('div').css({"border":"3px solid red"});
        } else {

          var estilo = $(this).parent().parent().children('div').attr('style');
          $(this).parent().parent().children('div').removeAttr('style');
          $(this).parent().parent().children('div').attr('style',estilo.replace("border: 3px solid red",''));
        }
      });
      $("#"+div+" .COMBO_OBG1 option:selected").each(function (i){

        if(this.value == "-1"){
          ok = false;
          $(this).parent().parent().children('div').css({"border":"3px solid red"});
        } else {
          $(this).parent().parent().parent().children('div').css({"border":""});

        }
      });
      if(!ok){
        $('#'+div+' .aviso').text('Os campos em vermelho são obrigatórios!');
        $('#'+div+' .div-aviso').show();
      } else {
        $('#'+div+' .aviso').text('');
        $('#'+div+' .div-aviso').hide();
      }
      return ok;
    }

    $('.div-aviso').hide();

    $('#tipos input[name=tipos]').click(function(){
      tipo = $('#tipos input[name=tipos]:checked').val();
      $('#dialog-formulario').attr('tipo',tipo);

    });


    $('.add-item-fatura').click(function(){
      $('#inicio').attr('value',$(this).attr('inicio'));
      $('#fim').attr('value',$(this).attr('fim'));
      $('#dialog-formulario').attr('cod',$(this).attr('id'));
      $('#dialog-formulario').attr('tipo_medicamento',$(this).attr('tipo_medicamento'));
      $('#dialog-formulario').attr('tipo-documento',$(this).attr('tipo-documento'));

      if($(this).attr('tipo_medicamento') == 'G'){
        $('#msg-generico').append("<div style='border: 2px dotted #999'>"
            +"<span class = 'text-red'>Paciente do plano "+$(this).attr('nomeplano')+" é recomendado prescrever itens Genéricos."
           +" Na busca de medicamento deve evitar selecionar os itens em vermelho.</span></div><br>");
      }

      $('#dialog-formulario').attr('plano',$(this).attr('plano'));
      $('#dialog-formulario').attr('empresa',$(this).attr('empresa'));
      $('#dialog-formulario').dialog('open');
      $('#contador').val($(this).attr('contador'));
    });

    $('#dialog-formulario').dialog('close');

    $('#dialog-formulario').dialog({
      autoOpen: false, modal: true, width: 800, position: 'top',
      open: function(event,ui){
        $('#tipos').buttonset();
        $('#dialog-formulario').attr('tipo', $('#tipos input[name=tipos]:checked').val());
        $('input[name=busca]').val('');
        $('#nome').val('');
        $('#nome').attr('readonly','readonly');
        $('#apr').val('');
        $('#apr').attr('readonly','readonly');
        $('#cod').val('');
        $('#tipo').val(0);
        $('#catalogo_id').val('');
        $('#tuss').val('');
        $('#custo').val('');
        $('#tabela_origem').val('');
        $('#valor_plano').val('');
        $('#qtd').val('');
        $('input[name=busca]').focus();
        //$('#contador').val('');
      },
      buttons: {
        'Fechar' : function(){
          var item = $('#dialog-formulario').attr('item');
          $("#msg-generico").html('');
          if(item != 'avulsa'){
            $('#kits-'+item).hide();
            $('#med-'+item).hide();
            $('#mat-'+item).hide();
            $('#serv-'+item).hide();
          }
          $(this).dialog('close');
        }
      }
    });

    /////////
    function pesquisa_unidade(obj) {


      $.post('query.php',{query:'unidades-fatura'},function(ri){
        $('#'+obj).append(ri);
        return false;
      });

    }


/////////
    $('#add-item').click(function(){

      if(validar_campos('dialog-formulario')){
        var cod = $('#cod').val();
        var catalogo_id = $('#catalogo_id').val();
        var nome = $('#dialog-formulario input[name=nome]').val();
        var apresentacao = $('#apresentacao').val();
        var tuss =$('#tuss').val();
        var tipo=$('#tipo').val();
        var custo = $('#custo').val();
        var tabela_origem =$('#tabela_origem').val();
        var contador=parseInt($('#contador').val())+1;
        var tipo_documento = $('#dialog-formulario ').attr('tipo-documento');
        var paciente = $('#dialog-formulario').attr('cod');


        if(tipo == 'med' || tipo==0)
        {
          tipo = 0;
          apresentacao = 'MEDICAMENTO';
        }
        else if(tipo == 'mat' || tipo == 1)
        {
          tipo = 1;
          apresentacao = 'MATERIAL';
        }
        else if(tipo == 'serv' || tipo=='ext'  || tipo == 12 || tipo==2)
        {
          tipo = 2;
        }
        else if(tipo == 'eqp'  || tipo == 5)
        {
          tipo = 5;
        }
        else if(tipo == 'gas'  || tipo == 6)
        {
          tipo = 6;
        }
        else if(tipo =='die'  || tipo == 3){
          tipo = 3;
          apresentacao = 'DIETA';
        }

        var inicio = $('#inicio').val();
        var fim = $('#fim').val();
        var qtd = $('#dialog-formulario input[name=qtd]').val();
        if(tipo != 'kit') {
            $('#contador').val(contador);
            $('.add-item-fatura').attr('contador', contador);
            var valor_receber = $('#valor_plano').val();
            var dados = 'nome=' + nome + ' tipo=' + tipo + ' cod=' + cod + ' qtd=' + qtd + 'calatalogo_id=' + catalogo_id;
            var linha = nome;

            var label_item = cod.replace(/\W/g, '');

            var botoes = "<br><input type='checkbox' class=' excluir-item excluir-item-paciente-" + paciente + "'/> <b> Excluir</b>"

            var tr = '<tr class=\'itens_fatura\' bgcolor=\'\' >' +
                '<td>' +
                "<label id='label-novo-codigo-" + label_item + "' style='display:none;'>" +
                "<button cod='" + label_item + "' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover usar_tiss'>" +
                "<span class='ui-button-text'>Usar Tiss</span></button>" +
                "<input type='text' maxlength='11' id='codigo-proprio-" + label_item + "' name='codigo-proprio'></label>" +
                "<button id='bt-novo-codigo-" + label_item + "' cod='" + label_item + "' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover bt_novo_codigo'>" +
                "<span class='ui-button-text'>C&oacute;digo Pr&oacute;prio</span></button>" +
                'TUSS-' + tuss + ' N-' + linha + botoes + '</td><td>' + apresentacao + '</td>' +
                '<td class=\'unidade_fat\' id=' + paciente + '_' + contador + '></td>' +
                '<td><input class=\'qtd\' size=\'7\' value=' + qtd + ' paciente=' + paciente + ' /></td>' +
                '<td>R$<input type=text name=valor_plano size=10 class=\'valor_plano valor OBG\' tiss=\'' + cod + '\' catalogo_id=\'' + catalogo_id + '\' label-item=\'' + label_item + '\' qtd=\'' + qtd + '\' paciente=\'' + paciente + '\' solicitacao=\'\' inicio=' + inicio + ' fim=' + fim + ' tipo=\'' + tipo + '\' tuss=\'' + tuss + '\' tabela_origem=\'' + tabela_origem + '\'  custo =\'' + custo + '\' usuario=\'\' ord =\'\' flag=\'\'  value=\'' + valor_receber + '\' editado=\'0\' codf=\'0\' ></td>' +
                '<td><span style=\'float:right;\' id=\'val_total\'><b>R$  <span nome=\'val_linha\'>0,00</span></b></span></td>' +
                '<td><input type=\'text\' name=\'taxa\' size=\'10\' value=\'0,00\' class=\'taxa OBG\'></td>' +
                '<td><input type=\'checkbox\' name=\'incluso_pacote\' size=\'10\' class=\'incluso_pacote incluso-pacote-' + paciente + ' OBG\'></td></tr>';
            $.ajax({
                url: '/faturamento/query.php?query=buscar-unidade-medida-fatura',
                success: function (response) {
                    response = JSON.parse(response);
                    var select_unidades = '<select name="unidade" class="unidade">' +
                        '<option value="-1">Selecione</option>';
                    $.each(response, function (index, value) {
                        select_unidades += '<option value="' + index + '">' + value.unidade + '</option>';
                    });
                    select_unidades += '</select>';
                    $('#' + paciente + '_' + contador).append(select_unidades);
                },
                async: true
            });

            if (tipo_documento == 1) {
                $('#tipo_' + tipo + '_paciente_' + paciente).append(tr);

            } else {
                $('#tabela_' + paciente).append(tr);
            }

            $('.valor').priceFormat({
                prefix: '',
                centsSeparator: ',',
                thousandsSeparator: '.'
            });

            // $('.taxa').priceFormat({
            //
            //     prefix: '',
            //     centsSeparator: ',',
            //     thousandsSeparator: '.',
            //     allowNegative: true
            // });
            $('.taxa').maskMoney({
                prefix: '',
                decimal: ',',
                thousands: '.',
                allowNegative: true,
                allowZero: true
            });

            $('.valor_plano')
                .unbind('blur', calcula_linha)
                .bind('blur', calcula_linha);
            $('.qtd')
                .unbind('blur', calcula_linha2)
                .bind('blur', calcula_linha2);

            $('.rem-item-solicitacao')
                .unbind('click', remove_item)
                .bind('click', remove_item);

            calcula_linha();
        } else {
            var plano = $('#dialog-formulario').attr('plano');
            $.ajax({
                url: '/faturamento/query.php?query=buscar-unidade-medida-fatura',
                success: function (data) {
                    data = JSON.parse(data);
                    select_unidades = '<select name="unidade" class="unidade">' +
                        '<option value="-1">Selecione</option>';
                    $.each(data, function (index, value) {
                        select_unidades += '<option value="' + index + '">' + value.unidade + '</option>';
                    });
                    select_unidades += '</select>';
                }
            });
            $.ajax({
                url: '/faturamento/query.php?query=buscar-itens-kit-fatura&kit_id=' + catalogo_id + '&plano_id=' + plano + '&tipo_documento=' + tipo_documento,
                success: function (response) {
                    response = JSON.parse(response);
                    console.log(select_unidades);
                    $.each(response, function (index, value) {
                        qtd_item = parseInt(value.qtd) * parseInt(qtd);
                        catalogo_id = index;
                        var contador = parseInt($('#contador').val())+1;
                        $('#contador').val(contador);
                        $('.add-item-fatura').attr('contador', contador);
                        var valor_receber = value.val;
                        var dados = 'nome=' + value.nome + ' tipo=' + value.tipo + ' cod=' + value.tiss + ' qtd=' + qtd + 'calatalogo_id=' + catalogo_id;
                        var linha = value.nome;

                        var label_item = value.tiss.replace(/\W/g, '');

                        var botoes = "<br><input type='checkbox' class=' excluir-item excluir-item-paciente-" + paciente + "'/> <b> Excluir</b>";

                        var tr = '<tr class=\'itens_fatura\' bgcolor=\'\' >' +
                            '<td>' +
                            "<label id='label-novo-codigo-" + label_item + "' style='display:none;'>" +
                            "<button cod='" + label_item + "' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover usar_tiss'>" +
                            "<span class='ui-button-text'>Usar Tiss</span></button>" +
                            "<input type='text' maxlength='11' id='codigo-proprio-" + label_item + "' name='codigo-proprio'></label>" +
                            "<button id='bt-novo-codigo-" + label_item + "' cod='" + label_item + "' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover bt_novo_codigo'>" +
                            "<span class='ui-button-text'>C&oacute;digo Pr&oacute;prio</span></button>" +
                            'TUSS-' + value.tuss + ' N-' + linha + botoes + '</td><td>' + value.apresentacao + '</td>' +
                            '<td class=\'unidade_fat\'>' + select_unidades + '</td>' +
                            '<td><input class=\'qtd\' size=\'7\' value=' + qtd_item + ' paciente=' + paciente + ' /></td>' +
                            '<td>R$<input type=text name=valor_plano size=10 class=\'valor_plano valor OBG\' tiss=\'' + value.tiss + '\' catalogo_id=\'' + catalogo_id + '\' label-item=\'' + label_item + '\' qtd=\'' + qtd_item + '\' paciente=\'' + paciente + '\' solicitacao=\'\' inicio=' + inicio + ' fim=' + fim + ' tipo=\'' + value.tipo + '\' tuss=\'' + value.tuss + '\' tabela_origem=\'catalogo\'  custo =\'' + value.custo + '\' usuario=\'\' ord =\'\' flag=\'\'  value=\'' + value.val + '\' editado=\'0\' codf=\'0\' ></td>' +
                            '<td><span style=\'float:right;\' id=\'val_total\'><b>R$  <span nome=\'val_linha\'>0,00</span></b></span></td>' +
                            '<td><input type=\'text\' name=\'taxa\' size=\'10\' value=\'0,00\' class=\'taxa OBG\'></td>' +
                            '<td><input type=\'checkbox\' name=\'incluso_pacote\' size=\'10\' class=\'incluso_pacote incluso-pacote-' + paciente + ' OBG\'></td></tr>';

                        if (tipo_documento == 1) {
                            $('#tipo_' + value.tipo + '_paciente_' + paciente).append(tr);

                        } else {
                            $('#tabela_' + paciente).append(tr);
                        }

                        $('.valor').priceFormat({
                            prefix: '',
                            centsSeparator: ',',
                            thousandsSeparator: '.'
                        });

                        // $('.taxa').priceFormat({
                        //
                        //     prefix: '',
                        //     centsSeparator: ',',
                        //     thousandsSeparator: '.',
                        //     allowNegative: true
                        // });
                        $('.taxa').maskMoney({
                            prefix: '',
                            decimal: ',',
                            thousands: '.',
                            allowNegative: true,
                            allowZero: true
                        });


                        $('.valor_plano')
                            .unbind('blur', calcula_linha)
                            .bind('blur', calcula_linha);

                        $('.qtd')
                            .unbind('blur', calcula_linha2)
                            .bind('blur', calcula_linha2);

                        $('.rem-item-solicitacao')
                            .unbind('click', remove_item)
                            .bind('click', remove_item);

                        calcula_linha();
                    });
                }
            });
        }
        var obj=paciente+'_'+contador;
        $('#tabela_origem').val('');
        $('#cod').val('-1');
        $('#catalogo_id').val('-1');
        $('#dialog-formulario input[name=nome]').val('');
        $('#dialog-formulario input[name=qtd]').val('');
        $('#busca-item').val('');
        //exitPage.needToConfirm = true;
      }
      return false;
    });

    $('.editado').click(function(){
      $(this).children('td').eq(4).children('input').attr('editado','1');

    });
    $("input[name='busca']").keypress(function() {
          $(this).autocomplete({
            source: function (request, response) {
              $.getJSON('busca_brasindice_fatura.php', {
                term: request.term,
                tipo: $('#dialog-formulario').attr('tipo'),
                plano: $('#dialog-formulario').attr('plano'),
                empresa: $('#dialog-formulario').attr('empresa'),
                tipo_medicamento: $('#dialog-formulario').attr('tipo_medicamento'),
                tipo_documento: $('#dialog-formulario').attr('tipo-documento')
              }, response);
            },
            minLength: 3,
            select: function (event, ui) {
              $('#busca-item').val('');
              $('#nome').val(ui.item.value);
              $('#cod').val(ui.item.id);
              $('#tipo').val(ui.item.tipo_aba);
              $('#catalogo_id').val(ui.item.catalogo_id);
              $('#apresentacao').val(ui.item.apresentacao);
              $('#custo').val(ui.item.custo);
              $('#valor_plano').val(ui.item.valor_receber);
              $('#tabela_origem').val(ui.item.tabela_origem);
              $('#tuss').val(ui.item.tuss);
              $('#qtd').focus();
              $('#busca-item').val('');
              return false;
            },
            extraParams: {
              //tipo: 2
            }
          }).data('autocomplete').
              _renderItem = function (ul, item) {
            var cor = '';
            if (item.textRed == 'S')
              cor = 'color:red';
            return $("<li>").data('item.autocomplete', item)
                .append("<a style='" + cor + "'>" + item.value + "</a>")
                .appendTo(ul);
          };
    });

    $('.rem-item-solicitacao').unbind('click',remove_item)
      .bind('click',remove_item);
    function atualiza_rem_item(){
      $('.rem-item-solicitacao').unbind('click',remove_item)
        .bind('click',remove_item);
    }


    /*
     function remove_item(){
     if (confirm('Deseja realmente remover?')){
     $(this).parent().parent().remove();

     }
     return false;
     }*/

    //////jeferson
    function remove_item(){
      if (confirm('Deseja realmente remover?')){
        paciente=$(this).parent().parent().children('td').eq(4).children('input').attr('paciente');
        tipo=$(this).parent().parent().children('td').eq(4).children('input').attr('tipo');
        tiss = $(this).attr('tiss');
        catalogo_id=$(this).attr('catalogo_id');
        fim = $(this).attr('fim');
        inicio = $(this).attr('inicio');
        val=$(this).parent().parent().children('td').eq(5).children('span').children('b').children('span').text();

        if(tipo == 5){
          sub_equipamento = $('#equipamento_'+paciente).text();
          sub_equipamento =  parseFloat(sub_equipamento.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));
          sub_equipamento=float2moeda(sub_equipamento);
          $('#equipamento_'+paciente).text(sub_equipamento);
        }

        if(tipo == 1){
          sub_material = $('#material_'+paciente).text();
          sub_material =  parseFloat(sub_material.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));
          sub_material=float2moeda(sub_material);
          $('#material_'+paciente).text(sub_material);
        }
        if(tipo == 2){
          sub_servico = $('#servico_'+paciente).text();
          sub_servico =  parseFloat(sub_servico.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));
          sub_servico=float2moeda(sub_servico);
          $('#servico_'+paciente).text(sub_servico);
        }
        if(tipo == 0){
          sub_medicamento = $('#medicamento_'+paciente).text();
          sub_medicamento =  parseFloat(sub_medicamento.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));
          sub_medicamento=float2moeda(sub_medicamento);
          $('#medicamento_'+paciente).text(sub_medicamento);
        }
        sub_total=$('.sub_fatura_'+paciente).children('b').eq(1).children('i').text();
        sub_valor=  parseFloat(sub_total.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));


        sub_valor=float2moeda(sub_valor);

        $('.sub_fatura_'+paciente).html('<b>TOTAL PACIENTE: R$</b> <b><i>'+sub_valor+'</i></b>');
        val_total=$('#total_fatura').children('b').children('i').text();
        total_valor=  parseFloat(val_total.replace('.','').replace(',','.')) -  parseFloat(val.replace('.','').replace(',','.'));
        total_valor= float2moeda(total_valor);
        $('#total_fatura').html('<b>TOTAL: R$</b> <b><i>'+total_valor+'</i></b>');
        $(this).parent().parent().remove();
        $.post('inserir.php',{query:'update-remove', inicio:inicio, fim:fim, paciente:paciente, tipo:tipo, tiss:tiss,catalogo_id:catalogo_id},function(r){
        });

      }
      return false;
    }

    $('.rem-item').bind('click',remove_item);




//////////////////////////////////////////////////////////////////////

    $('.numerico').keyup(function(e){
      this.value = this.value.replace(/\D/g,'');
    });

    $('.valor').priceFormat({
      prefix: '',
      centsSeparator: ',',
      thousandsSeparator: '.'
    });

    // $('.taxa').priceFormat({
    //
    //   prefix: '',
    //   centsSeparator: ',',
    //   thousandsSeparator: '.',
    //   allowNegative: true
    // });
      $('.taxa').maskMoney({
          prefix: '',
          decimal: ',',
          thousands: '.',
          allowNegative: true,
          allowZero: true
      });

      function calcula_linha(){

      //e.preventDefault();
      var total=0.00;
      var total2=0.00;
      var material = 0.00;
      var medicamento = 0.00;
      var dieta = 0.00;
      var equipamento = 0.00;
      var servico = 0.00;
      var gas = 0.00;
      var pacientevar = $(this).attr('paciente');
      $('.valor_plano').each(function(){
        valor = $(this).val().replace('.','');
        valor = valor.replace(',','.');
        paciente = $(this).attr('paciente');

        if(paciente == pacientevar){
          tipo = $(this).attr('tipo');


          if(tipo == 1){
            material += ( valor*$(this).attr('qtd'));
          } if(tipo == 2){
            servico += ( valor*$(this).attr('qtd'));
          }if(tipo == 5){
            equipamento += ( valor*$(this).attr('qtd'));
          }if(tipo == 0){
            medicamento += ( valor*$(this).attr('qtd'));
          }if(tipo == 3){
            dieta += ( valor*$(this).attr('qtd'));
          }if(tipo == 6){
            gas += ( valor*$(this).attr('qtd'));
          }

          total2 += ( valor*$(this).attr('qtd'));
          //$('#val_total'+$(this).attr('ord')).html('<b>R$  <span nome='val_linha'> '+float2moeda(( valor*$(this).attr('qtd')))+'</span></b>');
          total21 = float2moeda(total2);
          material2 = float2moeda(material);
          equipamento2 = float2moeda(equipamento);
          medicamento2 = float2moeda(medicamento);
          servico2 = float2moeda(servico);
          dieta2 = float2moeda(dieta);
          gas2= float2moeda(gas);

          $('#equipamento_'+paciente).text(equipamento2);
          $('#medicamento_'+paciente).text(medicamento2);
          $('#servico_'+paciente).text(servico2);
          $('#material_'+paciente).text(material2);
          $('#dieta_'+paciente).text(dieta2);
          $('#gasoterapia_'+paciente).text(gas2);


          $('.sub_fatura_'+paciente).html('<b>TOTAL PACIENTE: R$</b> <b><i>'+total21+'</i></b>');
          pacientevar = paciente;
        }else{
          total2=0;

          material = 0.00;
          medicamento = 0.00;
          dieta = 0.00;
          equipamento = 0.00;
          servico = 0.00;
          gas = 0.00;
          pacientevar = paciente;
          total2 += ( valor*$(this).attr('qtd'));

          tipo = $(this).attr('tipo');

          if(tipo == 1){
            material += ( valor*$(this).attr('qtd'));
          } if(tipo == 2){
            servico += ( valor*$(this).attr('qtd'));
          }if(tipo == 5){
            equipamento += ( valor*$(this).attr('qtd'));
          }if(tipo == 0){
            medicamento += ( valor*$(this).attr('qtd'));
          }if(tipo == 3){
            dieta += ( valor*$(this).attr('qtd'));
          }if(tipo == 6){
            gas += ( valor*$(this).attr('qtd'));
          }

          //$('#val_total'+$(this).attr('ord')).html('<b>R$  '+float2moeda(( valor*$(this).attr('qtd')))+'</b>');
          total21 = float2moeda(total2);
          material2 = float2moeda(material);
          equipamento2 = float2moeda(equipamento);
          medicamento2 = float2moeda(medicamento);
          servico2 = float2moeda(servico);
          dieta2 = float2moeda(dieta);
          gas2= float2moeda(gas);

          $('#equipamento_'+paciente).text(equipamento2);
          $('#medicamento_'+paciente).text(medicamento2);
          $('#servico_'+paciente).text(servico2);
          $('#material_'+paciente).text(material2);
          $('#dieta_'+paciente).text(dieta2);
          $('#gasoterapia_'+paciente).text(gas2);


          $('.sub_fatura_'+paciente).html('<b>TOTAL PACIENTE: R$</b> <b><i>'+total21+'</i></b>');


        }

        total += ( valor*$(this).attr('qtd'));
        //$('#val_total'+$(this).attr('ord')).html('<b>R$  <span nome=val_linha>'+float2moeda(( valor*$(this).attr('qtd')))+'</span></b>');
        $(this).parent().parent().children('td').eq(5).children('span').html('<b>R$  <span nome=val_linha>'+float2moeda(( valor*$(this).attr('qtd')))+'</span></b>');
      });
      total = float2moeda(total);
      $('#total_fatura').html('<b>TOTAL: R$</b> <b><i>'+total+'</i></b>');
    }


    function calcula_linha2(){

      //e.preventDefault();
      var total=0.00;
      var total2=0.00;
      var material = 0.00;
      var medicamento = 0.00;
      var dieta = 0.00;
      var equipamento = 0.00;
      var servico = 0.00;
      var gas = 0.00;

      var pacientevar = $(this).attr('paciente');

      $('.valor_plano').each(function(){

        valor =  $(this).parent().parent().children('td').eq(4).children('input').val().replace('.','');
        valor = valor.replace(',','.');

        paciente = $(this).attr('paciente');

        if(paciente == pacientevar){
          tipo = $(this).parent().parent().children('td').eq(4).children('input').attr('tipo');


          if(tipo == 1){
            material += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
          } if(tipo == 2){
            servico += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
          }if(tipo == 5){
            equipamento += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
          }if(tipo == 0){
            medicamento += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
          }if(tipo == 3){
            dieta += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
          }if(tipo == 6){
            gas += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
          }



          total2 += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
          //$('#val_total'+$(this).parent().parent().children('td').eq(4).children('input').attr('ord')).html('<b>R$  <span nome='val_linha'> '+float2moeda(( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd')))+'</span></b>');
          total21 = float2moeda(total2);
          material2 = float2moeda(material);
          equipamento2 = float2moeda(equipamento);
          medicamento2 = float2moeda(medicamento);
          servico2 = float2moeda(servico);
          dieta2 = float2moeda(dieta);
          gas2 = float2moeda(gas);



          $('#equipamento_'+paciente).text(equipamento2);
          $('#medicamento_'+paciente).text(medicamento2);
          $('#servico_'+paciente).text(servico2);
          $('#material_'+paciente).text(material2);
          $('#dieta_'+paciente).text(dieta2);
          $('#gasoterapia_'+paciente).text(gas2);


          $('.sub_fatura_'+paciente).html('<b>TOTAL PACIENTE: R$</b> <b><i>'+total21+'</i></b>');
          pacientevar = paciente;
        }else{
          total2=0;

          material = 0.00;
          medicamento = 0.00;
          dieta = 0.00;
          equipamento = 0.00;
          servico = 0.00;
          gas = 0.00;
          pacientevar = paciente;
          total2 += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));

          tipo = $(this).parent().parent().children('td').eq(4).children('input').attr('tipo');


          if(tipo == 1){
            material += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
          } if(tipo == 2){
            servico += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
          }if(tipo == 5){
            equipamento += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
          }if(tipo == 0){
            medicamento += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
          }if(tipo == 3){
            dieta += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
          }if(tipo == 6){
            gas += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
          }

          //$('#val_total'+$(this).attr('ord')).html('<b>R$  '+float2moeda(( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd')))+'</b>');
          total21 = float2moeda(total2);
          material2 = float2moeda(material);
          equipamento2 = float2moeda(equipamento);
          medicamento2 = float2moeda(medicamento);
          servico2 = float2moeda(servico);
          dieta2 = float2moeda(dieta);
          gas2 = float2moeda(gas);

          $('#equipamento_'+paciente).text(equipamento2);
          $('#medicamento_'+paciente).text(medicamento2);
          $('#servico_'+paciente).text(servico2);
          $('#material_'+paciente).text(material2);
          $('#dieta_'+paciente).text(dieta2);
          $('#gasoterapia_'+paciente).text(gas2);


          $('.sub_fatura_'+paciente).html('<b>TOTAL PACIENTE: R$</b> <b><i>'+total21+'</i></b>');


        }

        total += ( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd'));
        //$('#val_total'+$(this).attr('ord')).html('<b>R$  <span nome=val_linha>'+float2moeda(( valor*$(this).attr('qtd')))+'</span></b>');
        $(this).parent().parent().children('td').eq(5).children('span').html('<b>R$<span nome=val_linha>'+float2moeda(( valor*$(this).parent().parent().children('td').eq(4).children('input').attr('qtd')))+'</span></b>');
      });
      total = float2moeda(total);
      $('#total_fatura').html('<b>TOTAL: R$</b> <b><i>'+total+'</i></b>');
    }



    $('.span_obs').hide();


    $('.obs').toggle(
      function(){
        valor =$(this).attr('paciente');
        $('#span_'+valor).show();
      },function(){
        valor =$(this).attr('paciente');
        $('#span_'+valor).hide();

      });
    $('.add-item-fatura').hide();
    $('.ocultar').hide();
    $('.mostrar').toggle(
      function(){
        var item = $(this).attr('idpaciente');

        $('#tabela_'+item).show();
        $('#'+item).show();


      }, function(){
        var item = $(this).attr('idpaciente');

        $('#tabela_'+item).hide();
        $('#'+item).hide();
      });

    $('.valor_plano').live('blur',calcula_linha);

    $('.qtd').keyup(function(e){
      this.value = this.value.replace(/[^0-9]/g,'');

    });



//$('.qtd').live('click',calcula_linha2);


    $('.qtd').live('blur',function(){
      val = $(this).val();
      $(this).parent().parent().children('td').eq(4).children('input').attr('qtd',val);
      $(this).trigger('click');

    });

      $('#dialog-zerados-salvar-fatura').hide();

////////////////////
    $('.faturar,.orcamento_prorrogacao').click(function(event){
        console.log('abc');
      tam = $('.valor_plano').size();
      cod=$(this).attr('cod');
      av=$(this).attr('presc_av');
      cap=$(this).attr('cap');
      plano=$(this).attr('plano');
      obs=$('#text_'+cod).val();
      inicio_av=$('#inicio_fatura').val();
      fim_av=$('#fim_fatura').val();
      itens='';
      cont=0;
      cont_valor_fatura=0;
      id_fatura = $(this).attr('id-fatura');

        $('.valor_plano').each(function () {
          paciente = $(this).attr('paciente');
          if (cod == paciente) {
            sol = $(this).attr('solicitacao');
            tiss = $(this).attr('tiss');
            catalogo_id = $(this).attr('catalogo_id');
            qtd = $(this).attr('qtd');
            tipo = $(this).attr('tipo');
            flag = $(this).attr('flag');
            paciente = $(this).attr('paciente');
            usuario = $(this).attr('usuario');
            tabela_origem = $(this).attr('tabela_origem');
            valor_fatura = $(this).val();
            valor_fatura = parseFloat(valor_fatura.replace('.', '').replace(',', '.'));
            taxa = $(this).parent().parent().children('td').eq(6).children('input').val();
            taxa = parseFloat(taxa.replace('.', '').replace(',', '.'));
            incluso_pacote = $(this).parent().parent().children('td').eq(7).children('input').is(':checked');
            incluso_pacote = new Number(incluso_pacote);
            var tuss = $(this).attr('tuss');
            var label_item = $(this).attr('label-item');
            var codigo_proprio = $('#codigo-proprio-' + label_item).val();

            if (valor_fatura == '0.00') {
              cont_valor_fatura++;
            }
            valor_custo = $(this).attr('custo');
            fat_inicio = $('#inicio_fatura').val();
            fat_fim = $('#fim_fatura').val();
            unidade = $(this).parent().parent().children('td').eq(2).children('select').children('option:selected').val();

            cont++;

            if (cont == tam)
              itens += tiss + '#' + qtd + '#' + paciente + '#' + sol + '#' + tipo + '#' + valor_fatura + '#' + usuario + '#' + valor_custo + '#' + flag + '#' + fat_inicio + '#' + fat_fim + '#' + unidade + '#' + catalogo_id + '#' + tabela_origem + '#' + taxa + '#' + incluso_pacote + '#' + tuss + '#' + codigo_proprio;
            else
              itens += tiss + '#' + qtd + '#' + paciente + '#' + sol + '#' + tipo + '#' + valor_fatura + '#' + usuario + '#' + valor_custo + '#' + flag + '#' + fat_inicio + '#' + fat_fim + '#' + unidade + '#' + catalogo_id + '#' + tabela_origem + '#' + taxa + '#' + incluso_pacote + '#' + tuss + '#' + codigo_proprio + '$';
          }
        });
      if(cont == 0){
        alert("A fatura não pode ser salva sem nenhum item informado.");
        return false;
      }
        inicio = $('#inicio_fatura').val();
        fim = $('#fim_fatura').val();

        if (cont_valor_fatura > 0) {
          $('#dialog-zerados-salvar-fatura').dialog('open');
          $('#dialog-zerados-salvar-fatura').attr('dados', itens);
          $('#dialog-zerados-salvar-fatura').attr('inicio', inicio);
          $('#dialog-zerados-salvar-fatura').attr('fim', fim);
          $('#dialog-zerados-salvar-fatura').attr('cod', cod);
          $('#dialog-zerados-salvar-fatura').attr('av', av);
          $('#dialog-zerados-salvar-fatura').attr('plano', plano);
          $('#dialog-zerados-salvar-fatura').attr('cap', cap);
          $('#dialog-zerados-salvar-fatura').attr('obs', obs);
          $('#dialog-zerados-salvar-fatura').attr('id-fatura', id_fatura);
        } else {
          // só faz isso quando for fatura.
          var remocao = 'N';
          if (av == 0) {
            if (confirm("Essa fatura é de remoção?")) {
              remocao = 'S'
            }
          }

          $.post('inserir.php', {
            query: 'gravar-fatura', dados: itens, fat_inicio: inicio, fat_fim: fim, fat_paciente: cod,
            fat_plano: plano, av: av, cap: cap, obs: obs, inicio_av: inicio_av,
            fim_av: fim_av, remocao: remocao, id_fatura: id_fatura
          }, function (r) {

            if (r == 1) {

              $('#div_' + cod).remove();
              sub_total = $('.sub_fatura_' + cod).children('b').eq(1).children('i').text();
              val_total = $('#total_fatura').children('b').children('i').text();
              total_valor = parseFloat(val_total.replace('.', '').replace(',', '.')) - parseFloat(sub_total.replace('.', '').replace(',', '.'));
              total_valor = float2moeda(total_valor);
              $('#total_fatura').html('<b>TOTAL: R$</b> <b><i>' + total_valor + '</i></b>');

              var cont1 = $('#total_fatura').attr('cont');
              var cont2 = parseInt($('#total_fatura').attr('cont1'));
              cont2 = cont2 + cont;
                if (av == 0) {
                    if($('.container-fatura').length == 0) {
                        window.location.href = '?view=faturar';
                    }
                }

              if (cont2 == cont1) {
                if (av == 0) {
                  window.location.href = '?view=faturar';
                } else if (av == 1) {
                  window.location.href = '?view=faturaravaliacao';
                } else if (av == 2) {
                  window.location.href = '?view=orcamento-prorrogacao';
                }
              }
              $('#total_fatura').attr('cont1', cont2);
            } else {
              alert(r);
            }
          });
        }

    });


    ///// dialog desalvar item zerados da fatura

      $('#dialog-zerados-salvar-fatura').hide();
    $('#dialog-zerados-salvar-fatura').dialog('close');
    $('#dialog-zerados-salvar-fatura').dialog({
      autoOpen: false, modal: true, position: 'top',
      buttons: {
        'Sim' : function(){
          $('#dialog-zerados-salvar-fatura').dialog('open');
          dados= $(this).attr('dados');
          var fat_inicio= $(this).attr('inicio');
          var fat_fim= $(this).attr('fim');
          cod= $(this).attr('cod');
          av= $(this).attr('av');
          plano= $(this).attr('plano');
          cap= $(this).attr('cap');
          obs= $(this).attr('obs');
          id_fatura = $(this).attr('id-fatura');

          // só faz isso quando for fatura.
          var remocao = 'N';
          if(av == 0){
            if(confirm("Essa fatura é de remoção?")){
              remocao = 'S'
            }
          }


          $.post('inserir.php',{query:'gravar-fatura', dados: itens, fat_inicio:inicio, fat_fim:fim, fat_paciente:cod,
            fat_plano:plano, av:av,cap:cap, obs:obs, remocao:remocao,id_fatura:id_fatura},function(r){
            if (r == 1){

              $('#div_'+cod).hide('slow');
              sub_total=$('.sub_fatura_'+cod).children('b').eq(1).children('i').text();
              val_total=$('#total_fatura').children('b').children('i').text();
              total_valor=  parseFloat(val_total.replace('.','').replace(',','.')) -  parseFloat(sub_total.replace('.','').replace(',','.'));
              total_valor= float2moeda(total_valor);
              $('#total_fatura').html('<b>TOTAL: R$</b> <b><i>'+total_valor+'</i></b>');

              var cont1= $('#total_fatura').attr('cont');
              var cont2= parseInt($('#total_fatura').attr('cont1'));
              cont2= cont2+cont;
              if(cont2 == cont1){
                if(av == 0){
                  window.location.href='?view=faturar';
                }else if(av == 1){
                  window.location.href='?view=faturaravaliacao';
                }else{
                  window.location.href='?view=orcamento-prorrogacao';
                }
              }
              $('#total_fatura').attr('cont1',cont2);
            }else{
              alert(r);
            }



          });


          $(this).dialog('close');
        },
        'Nao' : function(){

          //return false;
          $(this).dialog('close');

        }
      }
    });


    /////// fim

    $('.enviar-email-1').click(function(){
      $('#dialog-email-1').attr('lote',$(this).attr('lote'));

      $('#dialog-email-1').dialog('open');


    });


    $('#dialog-visualizar-glosa').dialog('close');

    $('.v_glosa').click(function(){
      var id = $(this).attr('idfatura');


      $('#dialog-visualizar-glosa').attr('idfatura',id);
      $('#dialog-visualizar-glosa').dialog('open');


    });


    $('.v_glosa_3').click(function(){

      var id = $(this).attr('idfatura');
      var av = $(this).attr('av');


      window.open('imprimir_fatura_ur.php?id='+id+'&av='+av,'_blank');


    });
    /////////////////////jeferson 09-12-2013 exportar fatura para excel
    $('.v_exportar').click(function(){
      var id = $(this).attr('idfatura');
      var av = $(this).attr('av');
      window.open('exportar_fatura_excel.php?id='+id+'&av='+av+'&zerados=1');



    });
/////////////

    $('.v_glosa_2').click(function(){
      var id = $(this).attr('idfatura');
      var av = $(this).attr('av');
      var empresa = $(this).attr('empresa');
      $('#dialog-zerados-fatura').attr('id_fatura',id);
      $('#dialog-zerados-fatura').attr('av',av);
      $('#dialog-zerados-fatura').attr('empresa',empresa);
      $('#dialog-zerados-fatura').dialog('open');
      // window.open('imprimir_fatura.php?id='+id+'&av='+av,'_blank');



    });
    ///// dialog de retirar item zerados da fatura

    $('#dialog-zerados-fatura').dialog('close');
    $('#dialog-zerados-fatura').dialog({
      autoOpen: false, modal: true, position: 'top',
      open: function(event, ui) {
        $('#dialog-zerados-fatura').html('<br> <fieldset style=\'width:95%;\'><legend><b>Imprimir Zerados</b></legend>'+
        '<input type=\'radio\' name=\'visualizar\' value=0 checked/>N&atilde;o<br>' +
        '<input type=\'radio\' name=\'visualizar\' value=1 />Sim<br><br></fieldset>'+
        '<fieldset style=\'width:95%;\'><legend><b>Modelo de Fatura</b></legend>'+
        '<input type=\'radio\' name=\'modelo_fatura\' value=1 checked/> Fatura Aberta<br>' +
        '<input type=\'radio\' name=\'modelo_fatura\' value=2 />Fatura Fechada<br>' +
        '<input type=\'radio\' name=\'modelo_fatura\' value=3 />Sulamerica Liminar<br>' +
        '</fieldset>'+
        '<fieldset style=\'width:95%;\'><legend><b>Numero de diárias.</b></legend>'+
        '<input type=\'text\' name=\'diarias\' value=\'0\' size=\'3\' /> diárias<br>' +
        '</fieldset>'+
        '<fieldset style=\'width:95%;\'><legend><b>Imprimir a observação.</b></legend>'+
        '<select name=\'observacao\'>'+
        '<option value=\'0\' selected>Não</option>'+
        '<option value=\'1\'>Sim</option>'+
        '</select><br>' +
        '</fieldset>' +
        '<fieldset style=\'width:95%;\'><legend><b>Escolha a UR da Impressão</b></legend>'+
        '<p>' +
        '   <select id="select-escolher-empresa-imprimir-fatura">' +
        '   </select>' +
        '</p>' +
        '</fieldset>'
        );
          $.ajax({
              url: '/faturamento/query.php?query=buscar-filiais-ativas',
              success: function (data) {
                  data = JSON.parse(data);
                  $.each(data, function (index, value) {
                      var empresa = $('#dialog-zerados-fatura').attr('empresa');
                      var selected = value.id == empresa ? 'selected' : '';
                      option_filiais = '<option value="' + value.id + '" ' + selected + ' logo="' + value.logo + '" nome-empresa="' + value.nome + '">' + value.nome + '</option>';
                      $("#select-escolher-empresa-imprimir-fatura").append(option_filiais);
                  });
              },
              async: true
          });

      },
      buttons: {
        'Visualizar' : function(){
          var id = $(this).attr('id_fatura');
          var av=$(this).attr('av');
          var visualizar= $('#dialog-zerados-fatura input[name=\'visualizar\']:checked').val();
          var modelo_fatura =  $('#dialog-zerados-fatura input[name=\'modelo_fatura\']:checked').val();
          var diarias= $('#dialog-zerados-fatura input[name=\'diarias\']').val();
          var observacao = $('#dialog-zerados-fatura select[name=\'observacao\']').val();
          var empresa = $('#select-escolher-empresa-imprimir-fatura').val();
          window.open('imprimir_fatura2.php?id='+id+'&av='+av+'&zerados='+visualizar+'&modelo='+modelo_fatura+'&diarias='+diarias+'&observacao='+observacao+'&empresa='+empresa,'_blank');
          $(this).dialog('close');
        },
        Cancelar: function() {

          $(this).dialog('close');

        }
      }
    });


    /////// fim


    $('#dialog-visualizar-glosa').dialog({
      autoOpen: false, modal: true, position: 'top',
      buttons: {
        'Visualizar' : function(){
          var p = $('#dialog-visualizar-glosa').attr('idfatura');
          var tipo= $('#dialog-visualizar-glosa').attr('tipo');
          window.open('imprimir_fatura_glosa.php?id='+p+'&tipo='+tipo,'_blank');
        }
      }
    });
    $('#dialog-email-1').dialog('close');
    $('#dialog-email-1').dialog({
      autoOpen: false, modal: true, position: 'top',
      buttons: {
        'Gerar PDF' : function(){
          var p = $('#dialog-email-1').attr('lote');
          window.open('imprimir_lote.php?id='+p,'_blank');
          update_status();

          $(this).dialog('close');
        },
        'Enviar e-mail' : function(){
          email_fatura();
          update_status();

          $(this).dialog('close');

        }
      }
    });


    $('.edt_fat').click(function(){
      id= $(this).attr('idfatura');

      $.post('faturamento.php',{query:'edt_fatura', idfatura: id},function(r){
      });
    });



    $('#gerar_lote').click(function(){

      var id='';

      tam = $('.enviar').size();
      $('.enviar').each(function(){
        if($(this).is(':checked')){

          ids = $(this).val();

          id +=ids+'#';
        }
      });


      if(id != '' && id != null && id != undefined){

        var tipo= $(this).attr('tipo');
        var cod= $(this).attr('cod');

        $.post('inserir.php',{query:'gerar-lote', dados: id, cod:cod, tipo:tipo},function(r){

          if(r != '' || r != null){
            //alert('Capa de lote: '+r+' foi criada com sucesso');
            $('#num_lote').attr('lote',r);
            $('#num_lote').html('<p>Capa de lote: '+r+' foi criada com sucesso</p><br>'+
            '<button  class=\'ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\' id=\'salvar_guia_tiss\' role=\'button\'  aria-disabled=\'false\' ><span class=\'ui-button-text\'>OK</span></button>');
            $('#num_lote').dialog(open);
            $('.enviar').each(function(){
              if($(this).is(':checked')){
                ids = $(this).val();



                $(this).parent().parent().remove();
              }
            });
          }



        });
      }else{
        alert('Nenhum item selecionado');
      }

    });

    $('#salvar_guia_tiss').live('click',function(){
      $('#num_lote').dialog('close');
    });




    $('.doc-completo').click(function(){
      var cap= $(this).attr('cap');
      var id= $(this).attr('id_fatura');
      var idpac= $(this).attr('idpac');


      window.open('imprimir_score.php?id='+cap,'_blank');
      window.open('imprimir_fatura.php?id='+id,'_blank');
      window.open('imprimir_ficha.php?id='+cap,'_blank');
      window.open('imprimir_prescricao.php?id='+cap+'&idpac='+idpac,'_blank');





    });

    $('.informar_glosa').click(function(){
      idfataura = $(this).attr('idfatura');





      $.post('faturamento.php',{query:'informar-glosa', idfatura: idfatura},function(r){
      });

    });

    function float2moeda(num) {

      x = 0;

      if(num<0) {
        num = Math.abs(num);
        x = 1;
      }
      if(isNaN(num)) num = '0';
      cents = Math.floor((num*100+0.5)%100);
      num = Math.floor((num*100+0.5)/100).toString();

      if(cents < 10) cents = '0' + cents;
      for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
        num = num.substring(0,num.length-(4*i+3))+'.'
        +num.substring(num.length-(4*i+3));
      ret = num + ',' + cents;
      if (x == 1) ret = ' - ' + ret;return ret;

    }

  }

 //salvar fatura orçamento
    $(".salvar_orcamento_prorrogacao").live('click',function() {
        var inicio=$('#inicio_fatura').val();
        var fim= $('#fim_fatura').val();



        //// Se fatura = 0 eh fatura se for 1 eh orcamento
        var fatura =$(this).attr('presc_av');
        var paciente =$(this).attr('cod');
        this_obs=$('#text_'+paciente);
        obs = this_obs.val();
        this_obs.attr('defaultValue',obs);
        var p = $("#div_"+paciente).html();
        var p= inicio+"@|#"+fim+"@|#"+p;
        var nome = $(this).attr('nome');


        $.post("inserir.php", {query: "salvar-orcamento-prorrogacao", p: p, paciente: paciente, nome: nome ,fim:fim,inicio:inicio,fatura:fatura}, function(r) {
            alert(r);
        });
        return false;



    });

  ////Salvar um orçamento baseado no anterior.
  $("#salvar_novo_orcamento").click(function() {
    if(!validar_campos("div-geral-editar-fatura")){
      return false;
    }
    id_fatura = $(this).attr('id_fatura');
    tipo_fatura = $(this).attr('tipo');
    inicio = $("input[name=inicio]").val();
    fim = $("input[name=fim]").val();
    id_capmedica = $(this).attr('id_capmedica');
    paciente = $(this).attr('paciente');
    plano=$(this).attr('plano');
    tam = $('.valor_plano').size();
    itens = '';
    cont = 0;
    obs= $('#text_'+paciente).val();
    //guia_tiss= $('#guia_tiss').val();
    /// se finalizar = 0 ele salva se for igual a 1 ele finaliza.
    finalizar=$(this).attr('finalizar');

    $('.valor_plano').each(function() {
      paciente = $(this).attr('paciente');

      sol = $(this).attr('solicitacao');
      codf = $(this).attr('codf');
      qtd = $(this).attr('qtd');
      tipo = $(this).attr('tipo');
      numero_tiss = $(this).attr('numero_tiss');
      flag = $(this).attr('flag');
      catalogo_id=$(this).attr('catalogo_id');
      //plano = $(this).attr('plano');
      usuario = $(this).attr('usuario');
      valor_fatura = $(this).val();
      valor_fatura = parseFloat(valor_fatura.replace('.', '').replace(',', '.'));
      valor_custo = $(this).attr('custo');
      tabela_origem=$(this).attr('tabela_origem');
      taxa= $(this).parent().parent().children('td').eq(6).children('input').val();
      taxa = parseFloat(taxa.replace('.','').replace(',','.'));
      incluso_pacote= $(this).parent().parent().children('td').eq(7).children('input').is(':checked');
      incluso_pacote= new Number(incluso_pacote);
      var tuss= $(this).attr('tuss');
      var editado = $(this).attr('editado');

      unidade = $(this).parent().parent().children('td').eq(2).children('select').children('option:selected').val();
      cont++;


      if (cont == tam)
        itens += codf + '#' + qtd + '#' + paciente + '#' + sol + '#' + tipo + '#' + valor_fatura + '#' + usuario + '#' + valor_custo + '#' + flag + '#' + inicio + '#' + fim + '#' + unidade + '#' + numero_tiss+ '#' +catalogo_id+ '#' +tabela_origem+'#'+taxa+'#'+incluso_pacote+'#'+tuss+'#'+editado;
      else
        itens += codf + '#' + qtd + '#' + paciente + '#' + sol + '#' + tipo + '#' + valor_fatura + '#' + usuario + '#' + valor_custo + '#' + flag + '#' + inicio + '#' + fim + '#' + unidade + '#' + numero_tiss+ '#' +catalogo_id + '#' +tabela_origem+'#'+taxa+'#'+incluso_pacote+'#'+tuss+'#'+editado+ '$';

    });


    $.post('inserir.php', {query: 'salvar-novo-orcamento', dados: itens, id_fatura: id_fatura, inicio: inicio, fim: fim, paciente: paciente, id_capmedica: id_capmedica, fat_plano:plano,finalizar:finalizar,tipo_fatura:tipo_fatura,obs:obs}, function(r) {
      if(isNumber(r)){
          alert("Orçamento criado com sucesso!");
          window.location.href = '?view=faturaravaliacao';
      }else{
        alert(r);
        return false

      }

    });
  });

  function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
});