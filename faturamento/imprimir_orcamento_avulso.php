<?php
$id = session_id();
if(empty($id))
  session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');

use \App\Models\Administracao\Filial;

class Imprimir_orcamento_avulso{
	
public $inicio;
public $fim;
	
	
	
	public function detalhe_orcamento_avulso($id,$av,$numero_diaria,$observacao, $empresa){
		$html .="<form>";
        $responseEmpresa = Filial::getEmpresaById($empresa);
        $logo = "../utils/logos/{$responseEmpresa['logo']}";
	
			$html .= "<h1><a><img src='{$logo}'  width='100' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> Or&ccedil;amento  </h1>";
                        $conteudo = cabecalho_orcamento_avulso($id, $responseEmpresa['nome']);
                       $html .= $conteudo['html'];
                       $this->inicio=$conteudo['inicio'];
                       $this->fim=$conteudo['fim'];
                       $this->obs=$conteudo['observacao'];
               
                
		
	$html .="<br></br>";
		$sql= "SELECT
		o.*,
		IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),B.DESC_MATERIAIS_FATURAR) AS principio,
                 (CASE o.TIPO
		WHEN '0' THEN 'Medicamento'
		WHEN '1' THEN 'Material' WHEN '3' THEN 'Dieta' END) as apresentacao,
		o.NUMERO_TISS as cod,
                (       CASE o.TIPO
                        WHEN '0' THEN '3'
                        WHEN '1' THEN '4'
                        WHEN '3' THEN '5' 
		END) as ordem,
                uf.UNIDADE as und
		FROM
		orcamento_avulso_item as o  INNER JOIN
		`catalogo` B ON (o.CATALOGO_ID = B.ID) left join 
                 unidades_fatura as uf on (o.UNIDADE=uf.ID)
		WHERE
		 o.ORCAMENTO_AVU_ID= {$id} and
		 o.TIPO IN (0,1,3) and
                          o.CANCELADO_POR is NULL
							
			UNION		
			SELECT 
                        o.*,
			 vc.DESC_COBRANCA_PLANO AS principio,
			('Servi&ccedil;os') AS apresentacao,	      							
			 vc.COD_COBRANCA_PLANO as cod,
                         1 as ordem,
                         uf.UNIDADE as und
					
			FROM 
                          orcamento_avulso_item as o  INNER JOIN				      
                          valorescobranca as vc on  (o.CATALOGO_ID =vc.id) inner join
                          orcamento_avulso as oa  on (o.ORCAMENTO_AVU_ID = oa.ID)left join 
                          unidades_fatura as uf on (o.UNIDADE=uf.ID)
                         WHERE 

                          o.ORCAMENTO_AVU_ID= {$id} and
                          o.TIPO =2 and
                          o.TABELA_ORIGEM='valorescobranca' and
                          o.CANCELADO_POR is NULL
                          
                        UNION		
			SELECT 
                        o.*,
			 vc.item AS principio,
			('Servi&ccedil;os') AS apresentacao,	      							
			 vc.id as cod,
                         1 as ordem,
                         uf.UNIDADE as und
					
			FROM 
                          orcamento_avulso_item as o  INNER JOIN				      
                          cobrancaplanos as vc on  (o.CATALOGO_ID =vc.id) inner join
                          orcamento_avulso as oa  on (o.ORCAMENTO_AVU_ID = oa.ID)left join 
                          unidades_fatura as uf on (o.UNIDADE=uf.ID)
                         WHERE 

                          o.ORCAMENTO_AVU_ID= {$id} and
                          o.TIPO =2 and
                          o.TABELA_ORIGEM='cobrancaplano' and
                          o.CANCELADO_POR is NULL
                          
                        UNION		
			SELECT 
                        o.*,
			 vc.DESC_COBRANCA_PLANO AS principio,
			('Equipamentos') AS apresentacao,	      							
			 vc.COD_COBRANCA_PLANO as cod,
                         2 as ordem,
                         uf.UNIDADE as und
					
			FROM 
                          orcamento_avulso_item as o  INNER JOIN				      
                          valorescobranca as vc on  (o.CATALOGO_ID =vc.id) inner join
                          orcamento_avulso as oa  on (o.ORCAMENTO_AVU_ID = oa.ID)left join 
                          unidades_fatura as uf on (o.UNIDADE=uf.ID)
                         WHERE 

                          o.ORCAMENTO_AVU_ID= {$id} and
                          o.TIPO =5 and
                          o.TABELA_ORIGEM='valorescobranca' and
                          o.CANCELADO_POR is NULL
                          
                        UNION		
			SELECT 
                        o.*,
			 vc.item AS principio,
			('Equipamentos') AS apresentacao,	      							
			 vc.id as cod,
                         2 as ordem,
                         uf.UNIDADE as und
					
			FROM 
                          orcamento_avulso_item as o  INNER JOIN				      
                          cobrancaplanos as vc on  (o.CATALOGO_ID =vc.id) inner join
                          orcamento_avulso as oa  on (o.ORCAMENTO_AVU_ID = oa.ID)left join 
                          unidades_fatura as uf on (o.UNIDADE=uf.ID)
                         WHERE 

                          o.ORCAMENTO_AVU_ID= {$id} and
                          o.TIPO =5 and
                          o.TABELA_ORIGEM='cobrancaplano' and
                          o.CANCELADO_POR is NULL
                          
                         UNION		
			SELECT 
                        o.*,
			 vc.DESC_COBRANCA_PLANO AS principio,
			('Gasoterapia') AS apresentacao,	      							
			 vc.COD_COBRANCA_PLANO as cod,
                         6 as ordem,
                         uf.UNIDADE as und
					
			FROM 
                          orcamento_avulso_item as o  INNER JOIN				      
                          valorescobranca as vc on  (o.CATALOGO_ID =vc.id) inner join
                          orcamento_avulso as oa  on (o.ORCAMENTO_AVU_ID = oa.ID)left join 
                          unidades_fatura as uf on (o.UNIDADE=uf.ID)
                         WHERE 

                          o.ORCAMENTO_AVU_ID= {$id} and
                          o.TIPO =6 and
                          o.TABELA_ORIGEM='valorescobranca' and
                          o.CANCELADO_POR is NULL
                          
                        UNION		
			SELECT 
                        o.*,
			 vc.item AS principio,
			('Gasoterapia') AS apresentacao,	      							
			 vc.id as cod,
                         6 as ordem,
                         uf.UNIDADE as und
					
			FROM 
                          orcamento_avulso_item as o  INNER JOIN				      
                          cobrancaplanos as vc on  (o.CATALOGO_ID =vc.id) inner join
                          orcamento_avulso as oa  on (o.ORCAMENTO_AVU_ID = oa.ID)left join 
                          unidades_fatura as uf on (o.UNIDADE=uf.ID)
                         WHERE 

                          o.ORCAMENTO_AVU_ID= {$id} and
                          o.TIPO =6 and
                          o.TABELA_ORIGEM='cobrancaplano' and
                          o.CANCELADO_POR is NULL
                          
                         ORDER BY ordem,tipo,principio";
                         // echo $sql;
		
		$html.= "<table  width=100% class='mytable'>";
		
		$result = mysql_query($sql);
		$total=0.00;
		$cont_cor=0;
		$subtotal=0.00;
		$subtotal_equipamento=0.00;
		$subtotal_material=0.00;
		$subtotal_medicamento=0.00;
		$subtotal_gas=0.00;
		$subtotal_dieta=0.00;
		$subtotal_servico=0.00;
                $cont1=0;
                $cont2=0;
                $cont3=0;
                $cont4=0;
                $cont5=0;
                $cont6=0;
		while($row = mysql_fetch_array($result)){
			
			
		$subtotal +=$row['VAL_PRODUTO']*$row['QTD'];;
			
			
			
			if($row['TIPO'] == '6'){
				$subtotal_gas +=$row['VAL_PRODUTO']*$row['QTD'];;
				$parcial=$subtotal_gas;
				$cont5++;
				$n=$row['cod'];
				$tip = $row['TIPO'];
				$tipo_tot="TOTAL GASOTERAPIA";
			
			}
			
			if($row['TIPO'] == '5'){
				$subtotal_equipamento +=$row['VAL_PRODUTO']*$row['QTD'];;
				$parcial=$subtotal_equipamento;
				$cont4++;
				$n=$row['cod'];
				$tip = $row['TIPO'];
				$tipo_tot="TOTAL EQUIPAMENTO";
				
			}if($row['TIPO'] == '3'){
				$subtotal_dieta +=$row['VAL_PRODUTO']*$row['QTD'];;
				$cont6++;
                                $n = $row['TUSS']!=NULL && $row['TUSS']!=0 ?"TUSS. ".$row['TUSS']:"Bras. ".$row['NUMERO_TISS'];
				
				$tip = $row['TIPO'];
				$parcial=$subtotal_dieta;
				$tipo_tot="TOTAL DIETA";
			}
			if($row['TIPO'] == '2'){
                            
				$subtotal_servico +=$row['VAL_PRODUTO']*$row['QTD'];;
				$cont3++;
				$n=$row['cod'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_servico;
				$tipo_tot="TOTAL SERVI&Ccedil;O";
			}
			if($row['TIPO'] == '1'){
				$subtotal_material +=$row['VAL_PRODUTO']*$row['QTD'];;
				$cont1++;
                                $n = $row['TUSS']!=NULL && $row['TUSS']!=0 ?"TUSS. ".$row['TUSS']:"SIMP. ".$row['NUMERO_TISS'];
				
				$tip = $row['TIPO'];
				$parcial=$subtotal_material;
				$tipo_tot="TOTAL MATERIAL";
			}
			if($row['TIPO'] == '0'){
				$subtotal_medicamento +=$row['VAL_PRODUTO']*$row['QTD'];;
				$cont2++;
				$n = $row['TUSS']!=NULL && $row['TUSS']!=0 ?"TUSS. ".$row['TUSS']:"Bras. ".$row['NUMERO_TISS'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_medicamento;
				$tipo_tot="TOTAL MEDICAMENTO";
			}
			
			if( $cont_cor%2 ==0 ){
				$cor = '';
			}else{
				$cor="bgcolor='#EEEEEE'";
			}
			
			
                     if($row['UNIDADE']=='1')
                     {     
                        $und='Comprimido';
		     }
		     
		     if($row['UNIDADE']=='2')
		     {
		     	$und='Caixa';
		     }
		     
		     if($row['UNIDADE']=='3')
		     {
		     	$und='Frasco';
		     }
		     if($row['UNIDADE']=='4')
		     {
		     	$und='Ampola';
		     }
		     if($row['UNIDADE']=='5')
		     {
		     	$und='Bisnaga';
		     }
		     if($row['UNIDADE']=='6')
		     {
		     	$und='Sach&ecirc;';
		     }
		     if($row['UNIDADE']=='7')
		     {
		     	$und='Flaconete';
		     }
		     if($row['UNIDADE']=='8')
		     {
		     	$und='Pacote';
		     }
		     if($row['UNIDADE']=='9')
		     {
		     	$und='Di&aacute;ria';
		     }
		     if($row['UNIDADE']=='10')
		     {
		     	$und='Sess&atilde;o';
		     }
		     if($row['UNIDADE']=='11')
		     {
		     	$und='Visita';
		     }if($row['UNIDADE']=='12')
		     {
		     	$und='Unidade';
		     }if($row['UNIDADE']=='13')
		     {
		     	$und='Lata';
		     }if($row['UNIDADE']=='14')
		     {
		     	$und='Hora';
		     }if($row['UNIDADE']=='15')
		     {
		     	$und='Avalia&ccedl;&atilde;o';
		     }
		     
			
		     if($cont5 == 1){
		     	if($tip != $tipo && $cont_cor != 0){
		     		$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
		     	}
		     	$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>Gasoterapia</td></tr>";
		     	$html.= "<tr bgcolor='#cccccc'>
		     	<td style='font-size:12px;float:right;'>N&deg;</td>
		     	<td style='font-size:12px;float:right;'>Unidade</td>
		     	<td style='font-size:12px;float:right;'>Item</td>
		     	<th style='font-size:12px;float:right;'>Qtd</th>
		     	<th style='font-size:12px;float:right;'>Pre&ccedil;o Unt.</th>
		     	<th style='font-size:12px;float:right;'>Pre&ccedil;o Tot.</th>
		     	</tr>";
		     
		     
		     }
			if($cont4 == 1){
				if($tip != $tipo && $cont_cor != 0){
					$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>MATERIAIS PERMANENTES</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
				</tr>";
				
			
			}
			if($cont6 == 1 ){
				if($tip != $tipo && $cont_cor != 0){
					$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;bgcolor:#EEEEEE;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>DIETA</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
			        </tr>";
			
			}
			if($cont3 == 1 ){
				if($tip != $tipo && $cont_cor != 0){
					$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;bgcolor:#EEEEEE;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>SERVI&Ccedil;OS</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
			
			
				</tr>";
			
			}
			if($cont1 == 1){
				if($tip != $tipo && $cont_cor != 0){
					$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;bgcolor:#EEEEEE;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>MATERIAIS</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot. </th>
				</tr>";
			
			}
			if($cont2 == 1){
				if($tip != $tipo && $cont_cor != 0 ){
					$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE; '>MEDICAMENTOS</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
				</tr>";
			
			}
			
			
			//
			$tipo = $tip;
			$par= $parcial;
			$tipo_tot1=$tipo_tot;
                           //////2014-01-16
             ///////// se for material coloca a descrição igual do Simpro que vai ser para fatura porque o principio é um ‘nome genérico’ que as enfermeiras usam. 
             if($row['tipo'] == 1 && $row['segundonome']!= NULL){
                 $nomeitem =$row['segundonome'];
             }else{
	       $nomeitem = $row['principio'];
             } 
			$html.= "<tr $cor>
			<td>{$n}</td>
			<td>{$row['und']}</td>
			<td width='50%'>".strtoupper(strtr($nomeitem,"áéíóúâêôãõàèìòùç","ÁÉÍÓÚÂÊÔÃÕÀÈÌÒÙÇ"))."</td>
			<td>{$row['QTD']}</td>
			<td>R$ ".number_format($row['VAL_PRODUTO'],2,',','.')."</td>";
			$vt=$row['VAL_PRODUTO']*$row['QTD'];
			$vtotal += $vt; 
			$html.= "<td>R$ ".number_format($vt,2,',','.')."</td>
			</tr>";
			
                        if($cont6 == 1){
				$cont6++;
			
			}
                       if($cont5 == 1){
				$cont5++;
			
			}
			if($cont4 == 1){
				$cont4++;
			
			}
			if($cont3 == 1){
				$cont3++;
			
			}
			if($cont1 == 1){
				$cont1++;
			
			}
			if($cont2 == 1){
				$cont2++;
			
			}
			$cont_cor++;
			
		}
                
                //while termina aqui
                $obj_data_inicio = new DateTime($this->inicio);
                $obj_data_fim = new DateTime($this->fim);
                $diferenca_dias = $obj_data_inicio->diff($obj_data_fim);
                
                if($diferenca_dias->days)
                {    $diferenca = (int)$diferenca_dias->days;
                    $diferenca++;
                    $diaria = $vtotal/$diferenca;
                    $diaria_ceil =  ceil($diaria * 100) / 100;
                   
                }
                else
                {
                   $diaria = 0.00; 
                }
		$html.= "<tr ><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
		$html.= "<tr bgcolor='#cccccc' ><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>TOTAL: R$</b> <b><i>".number_format($vtotal,2,',','.')."</i></b></td></tr>";
                if($numero_diaria>0){
		$html.= "<tr bgcolor='#cccccc' ><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>Valor da Diária: R$</b> <b><i>".number_format($diaria_ceil,2,',','.')."</i></b></td></tr>";
		$html.= "<tr bgcolor='#cccccc' ><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>Valor para {$numero_diaria} dia(s): R$</b> <b><i>".number_format(ceil(($diaria*$numero_diaria)*100)/100,2,',','.')."</i></b></td></tr>";
                }
		$html.= "</table>";
                if($observacao == 1){
                  $html.= "<p><b>Observação:</b> </br>{$this->obs}</p>";  
                }
		
	$paginas []= $header.$html.= "</form>";
	
	
	//print_r($paginas);
	//print_r($paginas);
	
	$mpdf=new mPDF('pt','A4',9);
	$mpdf->SetHeader('página {PAGENO} de {nbpg}');
	$ano = date("Y");
	$mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']} &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));
	$mpdf->WriteHTML("<html><body>");
	$flag = false;
	foreach($paginas as $pag){
		if($flag) $mpdf->WriteHTML("<formfeed>");
		$mpdf->WriteHTML($pag);
		$flag = true;
	}
	$mpdf->WriteHTML("</body></html>");
	$mpdf->Output('orcamento_avulso.pdf','I');
	exit;
	
	
	/*$mpdf=new mPDF('pt','A4',9);
	$mpdf->WriteHTML($html);
	$mpdf->Output('pacientes.pdf','I');*/
	
	
	
	}





}




$p = new Imprimir_orcamento_avulso();
$p->detalhe_orcamento_avulso($_GET['id'],$_GET['av'],$_GET['d'],$_GET['observacao'], $_GET['empresa']);






?>
