<?php
$id = session_id();
if(empty($id))
  session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');

class imprimir_prescricao{
	private $abas = array('0'=>'Repouso','1'=>'Dieta','2'=>'Cuidados Especiais','3'=>'Soros e Eletr&oacute;litos',
			'4'=>'Antibi&oacute;ticos Injet&aacute;veis','5'=>'Injet&aacute;veis IV','6'=>'Injet&aacute;veis IM',
			'7'=>'Injet&aacute;veis SC','8'=>'Drogas inalat&oacute;rias','9'=>'Nebuliza&ccedil;&atilde;o',
			'10'=>'Drogas Orais/Enterais','11'=>'Drogas T&oacute;picas', '12'=>'F&oacute;mulas','13'=>'Oxigenoterapia');

		
	public function cabecalho($idcap){
		
			$id = $idcap;
		$condp1= "c.idClientes = '{$id}'";
		$sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresan,
		c.`nascimento`,
		p.nome as Convenio,p.id,c.*,(CASE c.acompSolicitante
		WHEN 's' THEN 'Sim'
		WHEN 'n' THEN 'N�o'
		END) AS acomp,
		NUM_MATRICULA_CONVENIO
		FROM
		clientes AS c LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN
		planosdesaude as p ON (p.id = c.convenio)
		WHERE
		{$condp1}
		ORDER BY
		c.nome DESC LIMIT 1";
	
		$result = mysql_query($sql);
		$result2 = mysql_query($sql2);
		$html.= "<table style='width:100%;' >";
		while($pessoa = mysql_fetch_array($result2)){
		foreach($pessoa AS $chave => $valor) {
		$pessoa[$chave] = stripslashes($valor);
	
		}
	
	
				$html.= "<tr bgcolor='#EEEEEE'>";
				$html.= "<td  style='width:60%;'><b>PACIENTE:</b></td>";
				$html.= "<td style='width:20%;' ><b>SEXO </b></td>";
				$html.= "<td style='width:30%;'><b>IDADE:</b></td>";
				$html.= "</tr>";
				$html.= "<tr>";
				$html.= "<td style='width:60%;'>{$pessoa['paciente']}</td>";
				$html.= "<td style='width:20%;'>{$pessoa['sexo']}</td>";
				$html.= "<td style='width:30%;'>".join("/",array_reverse(explode("-",$pessoa['nascimento']))).' ('.$pessoa['idade']." anos)</td>";
				$html.= "</tr>";
	
				$html.= "<tr bgcolor='#EEEEEE'>";
	
				$html.= "<td style='width:60%;'><b>UNIDADE REGIONAL:</b></td>";
				$html.= "<td style='width:20%;'><b>CONV&Ecirc;NIO </b></td>";
				$html.= "<td style='width:30%;'><b>MATR&Iacute;CULA </b></td>";
				$html.= "</tr>";
				$html.= "<tr>";
	
				$html.= "<td style='width:60%;'>{$pessoa['empresan']}</td>";
				$html.= "<td style='width:20%;'>".strtoupper($pessoa['Convenio'])."</td>";
				$html.= "<td style='width:30%;'>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
				$html.= "</tr>";
			$html.= " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
	
				
	
	
	
	$this->plan =  $pessoa['id'];
	}
	$html.= "</table>";
	
	return $html;
	}
	
	
	
	
	
	
	
	
	
	
	public function detalhe_prescricao($id,$idpac){
		$html .="<form>";
		$html .= "<h1><a><img src='../utils/logo2.jpg' width='200' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> Prescri&ccedil;&atilde;o  </h1>";
		$html .= $this->cabecalho($idpac);
		
		
		/*$sql2="select id,criador from prescricoes where  ID_CAPMEDICA='{$id}'";
		$x = mysql_query($sql2);
		$row2 = mysql_fetch_array($x);
		$id2 = $row2['id'];
		$medico = $row2['criador'];
		$sql3= "select nome from usuarios where idUsuarios='{$medico}'";
		$x1 = mysql_query($sql3);
		$row3 = mysql_fetch_array($x1);
		$med=$row3['nome'];
	    $html .="<h4 align='center'>Solicitante: {$med}</h4>";*/
	    $html .= "<br></br><table width=100% class='mytable'><tr bgcolor='#D3D3D3'>";
	    $html .= "<th><b>Itens prescritos</b></th>";
	    $html .= "</tr>";
	    
		/*$sql = "SELECT v.via as nvia, f.frequencia as nfrequencia,i.*,  i.inicio,
		i.fim, DATE_FORMAT(i.aprazamento,'%H:%i') as apraz, u.unidade as um
		FROM itens_prescricao as i LEFT OUTER JOIN frequencia as f ON (i.frequencia = f.id)
		LEFT OUTER JOIN viaadm as v ON (i.via = v.id)
		LEFT OUTER JOIN umedidas as u ON (i.umdose = u.id)
		WHERE idPrescricao = '$id2'
		ORDER BY tipo,inicio,aprazamento,nome,apresentacao ASC";
		$result = mysql_query($sql);
		$n=0;*/
		$sql = "SELECT
		I.*,U.nome as nomecriador,U.idUsuarios,P.Carater,
		v.via as nvia, f.frequencia as nfrequencia,I.inicio,
		I.fim, DATE_FORMAT(I.aprazamento,'%H:%i') as apraz, ume.unidade as um
		FROM
		prescricoes AS P INNER JOIN
		itens_prescricao AS I ON (P.id = I.idPrescricao)  LEFT JOIN
		brasindice AS B ON ( I.cod = B.numero_tiss) INNER JOIN
		usuarios AS U ON (U.idUsuarios=P.criador) LEFT OUTER JOIN frequencia as f ON (I.frequencia = f.id) LEFT OUTER JOIN viaadm as v ON (I.via = v.id)
		LEFT OUTER JOIN umedidas as ume ON (I.umdose = ume.id)
		WHERE
		P.Carater IN (4,7) AND
		P.ID_CAPMEDICA = {$id}";
		
		$result = mysql_query($sql);
		$n=0;
		$iduser=0;
		$valCarater=0;
		while($row = mysql_fetch_array($result)){
			if($row["Carater"] == 4){
				$carater= " -Prescri&ccedil;&atilde;o Peri&oacute;dica de Avalia&ccedil;&atilde;o";
			}
			if($row["Carater"] == 7){
				$carater= " -Prescri&ccedil;&atilde;o Aditiva de Avalia&ccedil;&atilde;o";
			}
			
			if($n++%2==0)
			$cor = '#E9F4F8';
			else
			$cor = '#FFFFFF';
	
			$tipo = $row['tipo'];
			$i = implode("/",array_reverse(explode("-",$row['inicio'])));
			$f = implode("/",array_reverse(explode("-",$row['fim'])));
			$aprazamento = "";
			if($row['tipo'] != "0") $aprazamento = $row['apraz'];
			$linha = "";
			if($row['tipo'] != "-1"){
			$aba = $this->abas[$tipo];
			$dose = "";
			if($row['dose'] != 0) $dose = $row['dose']." ".$row['um'];
			$linha = "<b>$aba:</b> {$row['nome']} {$row['apresentacao']} {$row['nvia']} {$dose} {$row['nfrequencia']} Per&iacute;odo: de $i até $f OBS: {$row['obs']}";
		}
		else $linha = $row['descricao'];
		
	if($iduser != $row['idUsuarios'] || $valCarater != $row["Carater"]){
  			$iduser= $row['idUsuarios'];
  			$valCarater=$row["Carater"];
			$html.="<tr bgcolor='grey'><td><center><b>Solicitante {$row['nomecriador']}{$carater}</center></b></td></tr>";
		}
		$html .= "<tr bgcolor={$cor} >";
		$html .= "<td>$linha</td>";
		$html .= "</tr>";
	}
	$html .= "</table>";
	$paginas []= $header.$html.= "</form>";
	
	
	//print_r($paginas);
	//print_r($paginas);
	
	$mpdf=new mPDF('pt','A4',9);
	$mpdf->SetHeader('página {PAGENO} de {nbpg}');
	$ano = date("Y");
	$mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
	$mpdf->WriteHTML("<html><body>");
	$flag = false;
	foreach($paginas as $pag){
		if($flag) $mpdf->WriteHTML("<formfeed>");
		$mpdf->WriteHTML($pag);
		$flag = true;
	}
	$mpdf->WriteHTML("</body></html>");
	$mpdf->Output('prescricao_avaliacao.pdf','I');
	exit;
	
	
	
	/*$mpdf=new mPDF('pt','A4',9);
	$mpdf->WriteHTML($html);
	$mpdf->Output('pacientes.pdf','I');*/
	
	
	
	}





}




$p = new imprimir_prescricao();
$p->detalhe_prescricao($_GET['id'],$_GET['idpac']);






?>