<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';
require $_SERVER['DOCUMENT_ROOT'] . '/utils/codigos.php';

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

//ini_set('display_errors', 1);

$routes = [
    "GET" => [
        '/faturamento/faturar-por-paciente/?action=iniciar-fatura' => '\App\Controllers\Faturamento\FaturarPorPaciente::formFatura',
        '/faturamento/faturar-por-paciente/?action=form-buscar-pacientes-faturar' => '\App\Controllers\Faturamento\FaturarPorPaciente::formBuscarPacientesFaturar',
        '/faturamento/faturar-por-paciente/?action=buscar-pacientes-faturar' => '\App\Controllers\Faturamento\FaturarPorPaciente::formBuscarPacientesFaturar',
    ],
    "POST" => [
        '/faturamento/faturar-por-paciente/?action=form-relatorio-pacientes-faturar' => '\App\Controllers\Faturamento\Relatorios::relatorioPacientesFaturar',
        '/faturamento/faturar-por-paciente/?action=buscar-pacientes-faturar' => '\App\Controllers\Faturamento\FaturarPorPaciente::buscarPacientesFaturar',
    ]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
    $app = new App\Application;
    $app->setRoutes($routes);
    $app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
    echo $e->getMessage();
} catch(App\NotFoundException $e) {
    http_response_code(404);
    echo $e->getMessage();
}