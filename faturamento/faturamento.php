<?php

$id = session_id();
if (empty($id))
	session_start();

include_once('../db/config.php');
include_once('../utils/codigos.php');
include_once('query.php');
include_once('orcamento_aditivo.php');
require __DIR__ . '/../vendor/autoload.php';

use App\Helpers\StringHelper as String;
use App\Models\Logistica\Devolucao as ModelDevolucao;
use App\Models\Logistica\Saidas as ModelSaidas;
use \App\Models\Sistema\Equipamentos as ModelEquipamento;
use \App\Models\Administracao\Filial;

class Faturamento {

	private function menu() {
		echo "<div>
				<center><h1>Faturamento</h1></center>		
			</div>";
		if ($_SESSION["empresa_principal"] == 1) {
			echo "<p><a href='../faturamento/?view=faturar'>Faturar</a></p>";
			echo "<p><a href='../faturamento/?view=listar'>Listar Faturas/Orçamentos</a></p>";
            echo "<p><a href='../faturamento/consenso/?action=consensoPesquisarFatura'>Consenso de Faturamento</a></p>";

			echo "<p><a href='../faturamento/?view=lote'>Gerar Lote</a></p>";
			echo "<p><a href='../faturamento/?view=gerenciarlote'>Gerenciar Lotes</a></p>";
			echo "<p><a href='../faturamento/?view=faturaravaliacao'>Or&ccedil;amento Inicial</a></p>";
			echo " <p><a href='../faturamento/?view=orcamento-prorrogacao'>Or&ccedil;amento Prorroga&ccedil;&atilde;o</a></p>";
			echo"<p><a href='../faturamento/?view=orcamento-aditivo'>".htmlentities('Orçamento Aditivo')." </a></p>";
			echo "<p><a href='../faturamento/?view=faturaavulsa'>Or&ccedil;amento Avulso</a></p>";
			echo "<p><a href='../faturamento/?view=informarglosa'>Previs&atilde;o de Recebimento</a></p>";
			echo "<p><a href='../faturamento/?view=atualiza-preco'>Atualizar pre&ccedil;o</a></p>";
			echo"<p><a href='../faturamento/?view=fatura_condensada'>Exportar Fatura Condensada </a></p>";
			echo"<p><a href='../faturamento/?view=relatorios'>Relatórios</a></p>";


		} else {
			echo "<p><a href='../faturamento/?view=listar'>Listar Faturas</a></p>";
			echo "<p><a href='../faturamento/?view=faturaavulsa'>Or&ccedil;amento Avulso</a></p>";

			if(in_array($_SESSION["empresa_principal"],[11, 2, 4, 5])){
                echo "<p><a href='../faturamento/?view=faturaravaliacao'>Or&ccedil;amento Inicial</a></p>";
                echo " <p><a href='../faturamento/?view=orcamento-prorrogacao'>Or&ccedil;amento Prorroga&ccedil;&atilde;o</a></p>";
            }
		}

	}

	private function formulario() {
		echo "<div id='dialog-formularioav' title='Formul&aacute;rio' tipo='' classe='' cod='' plano= '' empresa=''>";
		echo alerta();
		echo "<center><div id='tipos'>
				<input type='radio' id='bmed' name='tipos' checked='checked' value='med' /><label for='bmed'>Medicamentos</label>
				<input type='radio' id='bdie' name='tipos' value='die' /><label for='bdie'>Dieta</label>
				<input type='radio' id='bmat' name='tipos' value='mat' /><label for='bmat'>Materiais</label>
				<input type='radio' id='bserv' name='tipos' value='serv' /><label for='bserv'>Servi&ccedil;os</label>
				<input type='radio' id='bgas' name='tipos' value='gas' /><label for='bgas'>Gasoterapia</label>
				<input type='radio' id='bequip' name='tipos' value='eqp' /><label for='bequip'>Equipamento</label>
				<input type='radio' id='bextra' name='tipos' value='ext' /><label for='bextra'> Extras</label>
				<input type='radio' id='bkit' name='tipos' value='kit' /><label for='bkit'> Kits</label>
				</div>
  			</center>";
		echo "<p><b>Busca:</b><br/><input type='text' name='buscaav' id='busca-itemav' />";
		echo "<div id='opcoes-kits'></div>";
		echo "<input type='hidden' name='cod' id='cod' value='-1' />";
		echo "<input type='hidden' name='tipo' id='tipo' value='0' />";
		echo "<input type='hidden' name='catalogo_id' id='catalogo_id' value='-1' />";
		echo "<input type='hidden' name='apresentacao' id='apresentacao' value='-1' />";
		echo "<input type='hidden' name='custo' id='custo' value='-1' />";
		echo "<input type='hidden' name='valor_plano' id='valor_plano' value='-1' />";
		echo "<input type='hidden' name='inicio' id='inicio' value='-1' />";
		echo "<input type='hidden' name='fim' id='fim' value='-1' />";
		echo "<input type='hidden' name='contador' id='contador' value='-1' />";
		echo "<input type='hidden' name='tabela_origem' id='tabela_origem' value='' />";
		echo "<input type='hidden' name='tuss' id='tuss' value='' />";
		echo "<p><b>Nome:</b><input type='text' name='nome' id='nome' class='OBG' />";

		echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
		echo "<button id='add-itemav' tipo='' >+</button></div>";
	}

	// Gera dois campos inputs para inserir um período de data
	private function periodo() {
		echo "<tr style='width:450px;' >
  	          <td >
  	            <fieldset style='width:550px;'>
  	               <legend><b>Per&iacute;odo</b></legend>";
		echo "DE" .
			"<input type='text' name='inicio' value='{$this->inicio}' maxlength='10' size='15' class='OBG data periodo' />" .
			" AT&Eacute; <input type='text' name='fim' value='{$this->fim}' maxlength='10' size='15' class='OBG data' />
  	</fieldset></td></tr>";
	}

	public function pacientes($id_select, $comboObg1 = NULL, $filiais = null) {
        $cond = '';
	    if(!empty($filiais)){
            $filiais = implode(',',$filiais);
            $cond = " and c.empresa in ({$filiais})";
        }

        $condEmpresa = $_SESSION['empresa_principal'] != '1' ? " AND c.empresa IN {$_SESSION["empresa_user"]} " : '';

        $result = mysql_query("SELECT c.*, s.status as st FROM clientes as c inner join statuspaciente as s on (c.status=s.id) WHERE 1 = 1 {$condEmpresa} ORDER BY nome");
		if (!isset($name)){
			$name='';
		};
		$var = "<select name='{$name}'  data-placeholder='Selecione um Paciente' style='background-color:transparent;' class='$comboObg1 chosen-select' id='{$id_select}'>";
		if (!isset($todos)){
			$todos='';

		};
		if ($todos == "t")
			$var .= "<option value='todos' selected>Todos</option>";
		$var .= "<option value='-1' selected></option>";

		$aux = 0;
		$paciente_status = array();
		$status = array('1' => '::     Novo    ::', '4' => '::   Ativo/Autorizado   ::', '6' => '::     Óbito    ::');
		while ($row = mysql_fetch_array($result)) {
			$paciente = strtoupper(String::removeAcentosViaEReg($row['nome']));
			if (array_key_exists($row['status'], $paciente_status)) {
				$pacientes_por_status = array('idCliente' => $row['idClientes'], 'nome' => $paciente, 'codigo' => $row['codigo'], 'status' => $row['status'], 'nstatus' => $row['st']);
				# Verifica se a chave existe dentro do array $status,
				# caso não esteja, ele adiciona ao código 7 para que
				# os outros possam ser ordenados juntos no select
				if (array_key_exists($row['status'], $status)) {
					array_push($paciente_status[$row['status']], $pacientes_por_status);
				} else {
					if (!array_key_exists(7, $paciente_status)) {
						$paciente_status[7] = array();
					}
					array_push($paciente_status[7], $pacientes_por_status);
				}
			} else {
				$paciente_status[$row['status']] = array();
				$pacientes_por_status = array('idCliente' => $row['idClientes'], 'nome' => $paciente, 'codigo' => $row['codigo'], 'status' => $row['status'], 'nstatus' => $row['st']);
				# Verifica se a chave existe dentro do array $status,
				# caso não esteja, ele adiciona ao código 7 para que
				# os outros possam ser ordenados juntos no select
				if (array_key_exists($row['status'], $status)) {
					array_push($paciente_status[$row['status']], $pacientes_por_status);
				} else {
					if (!array_key_exists(7, $paciente_status)) {
						$paciente_status[7] = array();
					}
					array_push($paciente_status[7], $pacientes_por_status);
				}
			}
		}


		$cont = 0;
		ksort($paciente_status);
		foreach ($paciente_status as $chave => $dados) {
			#Verifica se é a primeira vez que ta rodando
			if (array_key_exists($chave, $status) && $cont == 0) {
				$var .= "<optgroup></optgroup>";
				$var .= "<optgroup label='{$status[$chave]}'>";
			}#Da segunda vez em diante passará por aqui
			elseif (array_key_exists($chave, $status) && $cont > 0) {
				$var .= "</optgroup>";
				$var .= "<optgroup></optgroup>";
				$var .= "<optgroup label='{$status[$chave]}'>";
			} elseif ($chave == 7) {
				$var .= "</optgroup>";
				$var .= "<optgroup></optgroup>";
				$var .= "<optgroup label='::     Outros     ::'>";
			}
			foreach ($dados as $chave2 => $dados2) {
				$var .= $chave;

				$var .= "<option value='{$dados2['idCliente']}' status='{$chave}' nome_status='{$dados2['nstatus']}'>({$dados2['codigo']}) {$dados2['nome']}</option>";
			}
			$cont++;
		}
		$var .= "</optgroup>";
		$var .= "</select>";


		echo $var;
	}

	private function pacientes_avaliacao() {
		$result = mysql_query(
		    "SELECT 
                    DISTINCT(c.idClientes),
                    c.status AS statusPaciente, 
                    c.* 
                  FROM 
                    clientes as c 
                    -- inner join capmedica as cap on (c.idClientes = cap.paciente ) 
                    -- inner join prescricoes as p on (p.ID_CAPMEDICA = cap.id) 
                  WHERE 
                    c.status = 1 
                    OR c.status = 4 
                  ORDER BY 
                  statusPaciente, nome;");
		$var = "<p><select name='{$name}' data-placeholder='Selecione um Paciente' style='background-color:transparent; width: 400px;' class='COMBO_OBG1  chosen-select' id='select_paciente_avaliacao'>";
		if ($todos == "t")
			$var .= "<option value='todos' selected>Todos</option>";
		$var .= "<option value='-1' selected></option>";
		$grupo = 0;
		$i = 0;
		while (@$row = mysql_fetch_array($result)) {
			if($grupo != $row['statusPaciente']) {
				if($i > 0){
                    $var .= "</optgroup>";
				}
            	$var .= "<optgroup label='" . ($row['statusPaciente'] == '1' ? '::: Novo :::' : '::: Ativo/Autorizado :::') . "'>";
                $grupo = $row['statusPaciente'];
            }
			if (($id <> NULL) && ($id == $row['idClientes']))
				$var .= "<option " . ($row['statusPaciente'] == '4' ? "disabled" : "") . " value='{$row['idClientes']}' status-paciente='{$row['statusPaciente']}' selected>({$row['codigo']}) " . strtoupper(String::removeAcentosViaEReg($row['nome'])) . "</option>";
			else
				$var .= "<option " . ($row['statusPaciente'] == '4' ? "disabled" : "") . " value='{$row['idClientes']}' status-paciente='{$row['statusPaciente']}'>({$row['codigo']}) " . strtoupper(String::removeAcentosViaEReg($row['nome'])) . "</option>";
			$i++;
		}
        $var .= "</optgroup>";
		$var .= "</select>";
		return $var;
	}

	private function unidades() {
		$result = mysql_query("SELECT * FROM empresas ORDER BY nome;");
		$var = "<select name='{$name}' style='background-color:transparent;' class='COMBO_OBG ' id='select_unidade'>";
		if ($todos == "t")
			$var .= "<option value='todos' selected>Todos</option>";
		$var .= "<option value='-1' selected></option>";
		while (@$row = mysql_fetch_array($result)) {
			if (($id <> NULL) && ($id == $row['id']))
				$var .= "<option value='{$row['id']}' selected>" . strtoupper($row['nome']) . "</option>";
			else
				$var .= "<option value='{$row['id']}'>" . strtoupper($row['nome']) . "</option>";
		}
		$var .= "</select>";
		return $var;
	}

	private function planos() {
		$result = mysql_query("SELECT * FROM planosdesaude  ORDER BY nome;");
		$var = "<select name='{$name}' style='background-color:transparent;' class='COMBO_OBG' id='select_plano'>";
		if ($todos == "t")
			$var .= "<option value='todos' selected>Todos</option>";
		$var .= "<option value='-1' selected></option>";
		while (@$row = mysql_fetch_array($result)) {
			if (($id <> NULL) && ($id == $row['id']))
				$var .= "<option value='{$row['id']}' selected>" . strtoupper($row['nome']) . "</option>";
			else
				$var .= "<option value='{$row['id']}'>" . strtoupper($row['nome']) . "</option>";
		}
		$var .= "</select>";
		return $var;
	}

	private function operadora() {
		$result = mysql_query("SELECT * FROM operadoras ORDER BY NOME;");
		$var = "<p><select name='{$name}' style='background-color:transparent;' class='COMBO_OBG' id='select_operadora'>";
		if ($todos == "t")
			$var .= "<option value='todos' selected>Todos</option>";
		$var .= "<option value='-1' selected></option>";
		while (@$row = mysql_fetch_array($result)) {
			if (($id <> NULL) && ($id == $row['ID']))
				$var .= "<option value='{$row['ID']}' selected>" . strtoupper($row['NOME']) . "</option>";
			else
				$var .= "<option value='{$row['ID']}'>" . strtoupper($row['NOME']) . "</option>";
		}
		$var .= "</select>";
		return $var;
	}

    private function orcamentos_prorrogacao_salvos($fatura) {

        $dir = "salvas/orcamento-prorrogacao/";
        $titulo= "OR&Ccedil;AMENTO DE PROROGAÇÂO SALVO";

        if (!is_dir($dir))
            return;
        $ext = array('htm');
        $arquivos = get_files_dir($dir, $ext);
        if (is_array($arquivos)) {
            $tam = count($arquivos);
            $cont = 0;

            echo "<br/>";
            echo "<table width='100%' class='mytable'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th colspan='2'><center>{$titulo}</center></th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            echo "<tr>";
            $quantidadeItensSalvos = count($arquivos);
            $colspan ='';
            $filename ='';
            foreach ($arquivos as $arquivo) {
                $dir = "salvas/orcamento-prorrogacao/";
                $filename = $dir."/".$arquivo;
                $str = stripslashes(file_get_contents($filename));
                $string = explode('@|#', $str);
                $nome = explode(".", $arquivo);

                $paciente = $this->pacienteNome($nome[0]);
                if ($cont % 2 == 0 && $cont !=0) {
                    echo "</tr>";
                    echo "<tr>";
                }
                if($quantidadeItensSalvos == ($cont + 1) && $quantidadeItensSalvos % 2 != 0){
                    $colspan =2;
                }

                echo "<td colspan='{$colspan}'>
						<center>
							<a href='#' class='load_salva' id_paciente={$nome[0]} fatura='{$fatura}'>
							<b>" . strtoupper($paciente) . "</b>
							 </a>
							 <br>
							  <span >Período: {$string[0]} - {$string[1]}
							  <br>
							 	{$string[3]} as {$string[4]}
							 </span>
						 </center>
						</td>";
                $cont++;
            }
            echo "</tr>";
            echo "</tbody>";
            echo "</table>";
        }
    }

	//! Exibe a lista de faturas salvas, mas não finalizadas jeferson 2013-10-17.
	private function faturas_salvas($fatura) {
	    $view = 'edt_fatura';
        if($fatura == 0){
            $titulo = "FATURAS SALVAS COMO RASCUNHO";
        } else if($fatura == 2) {
            $titulo = "OR&Ccedil;AMENTO DE PROROGAÇÂO SALVO";
            $view = 'edt_fatura';
        } else{
            $titulo = "OR&Ccedil;AMENTO SALVOS";
            $view = 'edt_orcamento';
        }

        $sql = <<<SQL
SELECT
  fatura.ID,
  fatura.DATA_INICIO AS inicio,
  fatura.DATA_FIM AS fim,
  fatura.DATA AS data_hora_salva,
  clientes.nome AS paciente,
  usuarios.nome as usuario
FROM
  fatura
  INNER JOIN clientes ON fatura.PACIENTE_ID = clientes.idClientes
  INNER JOIN usuarios on fatura.USUARIO_ID = usuarios.idUsuarios
WHERE
  fatura.STATUS = -1
  AND fatura.ORCAMENTO = '{$fatura}'
  AND fatura.cancelado_por is null
SQL;
        $rs = mysql_query($sql) or die(mysql_error());

        $quantidadeItensSalvos = mysql_num_rows($rs);

		if ($quantidadeItensSalvos > 0) {
			$cont = 0;

			echo "<br/>";
			echo "<table width='100%' class='mytable'>";
			echo "<thead>";
			echo "<tr>";
			echo "<th colspan='2'><center>{$titulo}</center></th>";
			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			echo "<tr>";
			$colspan = '';
			while ($row = mysql_fetch_array($rs)) {
				$inicio = \DateTime::createFromFormat('Y-m-d', $row['inicio'])->format('d/m/Y');
				$fim = \DateTime::createFromFormat('Y-m-d', $row['fim'])->format('d/m/Y');
				$dataHora = \DateTime::createFromFormat('Y-m-d H:i:s', $row['data_hora_salva'])->format('d/m/Y à\s H:i:s');
				$usuario = $row['usuario'];
				if ($cont % 2 == 0 && $cont !=0) {
					echo "</tr>";
					echo "<tr>";
				}
				if($quantidadeItensSalvos == ($cont + 1) && $quantidadeItensSalvos % 2 != 0){
					$colspan = 2;
				}

				echo "<td colspan='{$colspan}'>
						<center>
							<a href='/faturamento/?view={$view}&id={$row['ID']}&av={$fatura}' tipo='{$fatura}' id_paciente='{$row['paciente']}' fatura='{$row['ID']}'>
							<b>" . strtoupper($row['paciente']) . "</b>
							 </a>
							 <br>
							  <span >
							  {$usuario}
							  <br>
							  Período: {$inicio} - {$fim}
							  <br>
							 	{$dataHora}
							 	<br>
							 	<img class='excluir-documento-salvo' idFatura='{$row['ID']}' src='../utils/delete_16x16.png' title='Excluir' border='0'>
							 </span>
						 </center>
						</td>";
				$cont++;
			}
			echo "</tr>";
			echo "</tbody>";
			echo "</table>";
		}
	}

	//! Retorna o nome de um paciente com determinado ID
	private function pacienteNome($id) {
		$result = mysql_query("SELECT nome FROM clientes WHERE idClientes='$id' ORDER BY nome;");
		while ($row = mysql_fetch_array($result)) {
			return ucwords(strtolower(String::removeAcentosViaEReg($row['nome'])));
		}
	}

	private function faturar() {
		echo "<div name='div-faturar' id='formulario' title='Formul&aacute;rio' tipo='' classe='' >";

		echo "<div id='refaturar' title='Refaturar' ><p>Alguns pacientes j&aacute foram faturados neste per&iacute;odo, deseja refatura-los?</p></div>";
		echo "<div id='dialog-zerados-salvar-fatura' style='display: none;' lote='' title='Salvar Fatura'   dados= ''	 inicio= ''	  fim= ''	  cod= ''	  av= ''	 plano= ''  cap=''    obs= '' >
		<p><b>Deseja salvar fatura com  itens zerados?</b></p></div>";
		echo "<button  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover nav-listar-fat' role='button'  aria-disabled='false' ><span class='ui-button-text'>Listar Fatura</span></button>";

		echo "<center><h1> Faturamento</h1></center>";
		echo "<br/>";
		echo "<div>{$this->faturas_salvas(0)}</div>";
		echo "<table width='100%'>";
		echo "<tr>";
		echo "<td style='width:750px;'>";
		echo"<fieldset style='width:750px;'>";
		echo "<legend><b>Filtrar por:<b></legend>";
		$filiaisAtivas = Filial::getUnidadesAtivas();
		foreach ($filiaisAtivas as  $ativa){
		    $filiais[$ativa['id']] = $ativa['id'];
        }

		echo "<div id='bloco1' style='display:table;float:left;;width:750px;'>";
		echo "<p><input type='radio' class='filtrar_por' name='filtrar_por' t='unidade'><label>Por Unidade Regional</label></p>";
		echo "<p><input type='radio' class='filtrar_por' name='filtrar_por' t='paciente'><label>Por Paciente</label></p>";
		echo "<p><input type='radio' class='filtrar_por' name='filtrar_por' t='plano'><label>Por Plano</label></p>";

		echo "</div>";
		echo "<br><div id='label-pac' style='display: none;'><label>Paciente</label></div>";
		echo "<div class='bloco2' style='display:table;float:left;width:750px;' cod='' nome=''>";
		echo "<br/>";
		echo "<span id='ure' style='display:none;'><label><p>Unidade</p></label><p>{$this->unidades()}</p></span>";
        echo "<span id='pac' style='display:none;'>{$this->pacientes('select_paciente',null, $filiais )}</span>";
		echo "<span id='mudar_plano' style='display:none;'><label><p><input type='checkbox' id='check_plano'/> Alterar Plano.</p></label></span>";
		echo "<span id='plan' style='display:none;'><label><p>Plano</p></label><p>{$this->planos()}</p></span>";


		echo "</div>";
		echo "</td>";

		echo "</fieldset>";

		echo "</tr>";

		echo "</tr>";
		echo "<tr style='width:450px;' >
  	          <td >
  	            <fieldset style='width:550px;'>
  	               <legend><b>Per&iacute;odo da Fatura</b></legend>";
		echo "<p><b>Inicio:</b><input type='text' name='inicio_fatura' value='' maxlength='10' size='15' class='OBG data periodo' id='inicio_fatura'/>" .
			" <b>Fim:</b> <input type='text' name='fim_fatura' value='' maxlength='10' size='15' class='OBG data' id='fim_fatura'/></p>
  	</fieldset></td></tr>";
		echo "</table>";
		echo "<button id='fatura' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'  aria-disabled='false' ><span class='ui-button-text'>Gerar Fatura</span></button></div>";
		echo "<div id='tabela_faturar'></div>";
		echo "<div id='teste-fatura'></div>";
	}

	////////////////////////Jeferson Marques orçamento prorrogação
	private function orcamento_prorrogacao() {

		echo "<div name='div-faturar' id='formulario' title='Formul&aacute;rio' tipo='' classe='' >";

		echo "<div id='dialog-zerados-salvar-fatura' style='display: none;' lote='' title='Salvar Or&ccedil;amento Prorroga&ccedil;&atilde;o' id-fatura='0'  dados= ''	 inicio= ''	  fim= ''	  cod= ''	  av= ''	 plano= ''  cap=''    obs= '' >
	            <p><b>Deseja salvar Or&ccedil;amento com  itens zerados?</b></p>
              </div>";
		echo "<button  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover nav-listar-fat' role='button'  aria-disabled='false' ><span class='ui-button-text'>Listar Fatura</span></button>";

		echo "<center><h1> Or&ccedil;amento Prorroga&ccedil;&atilde;o</h1></center>";
		echo "<br/>";
    //echo "<div>{$this->orcamentos_prorrogacao_salvos(2)}</div>";
		echo "</br><div>{$this->faturas_salvas(2)}</div></br>";
    echo "<br/>";
		echo "<table width='100%'>";
		echo "<tr>";
		echo "<td style='width:450px;'>";
		echo"<fieldset style='width:550px;'>";
		echo "<legend><b>Filtrar:<b></legend>";
		echo "<p>Paciente:</p>";
		echo "<div class='bloco2' style='display:table;float:left;width:200px;' cod='' nome=''>";
        $filiaisAtivas = Filial::getUnidadesAtivas();
        foreach ($filiaisAtivas as  $ativa){
            $filiais[$ativa['id']] = $ativa['id'];
        }
		echo $this->pacientes('select_paciente_prorrogacao',null, $filiais);
		echo "</div>";
		echo "</td>";
		echo "</fieldset>";

		echo "</tr>";
		$this->periodo();
		echo "</tr>";
		echo "<tr style='width:450px;' >
  	       <td >
  	       <fieldset style='width:550px;'>
  	       <legend><b>Per&iacute;odo do Or&ccedil;amento de Prorrogação</b></legend>";
		echo "<p><b>Inicio:</b><input type='text' name='inicio_fatura' value='' maxlength='10' size='15' class='OBG data periodo' id='inicio_fatura'/>" .
			" <b>Fim:</b> <input type='text' name='fim_fatura' value='' maxlength='10' size='15' class='OBG data' id='fim_fatura'/></p>
  	</fieldset></td></tr>";
		echo "</table>";
		echo "<button id='gerar-orcamento' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'  aria-disabled='false' ><span class='ui-button-text'>Gerar Or&ccedil;amento</span></button></div>";
		echo "<div id='tabela_faturar'></div>";
		echo "<div id='teste-fatura'></div>";
	}

	////////////////////////

	private function listar_faturas() {

		echo "<div id='formulario' title='Formul&aacute;rio' tipo='' classe='' >";

		echo "<center><h1>Listar Faturas/Orçamentos 123 </h1></center>";
		echo "<br>";
		echo "<table width='100%'>";
		echo "<tr>";
		echo "<td style='width:450px;'>";
		echo"<fieldset style='width:550px;'>";
		echo "<legend><b>Filtrar por:<b></legend>";
		echo "<div id='bloco1' style='display:table;float:left;;width:200px;'>";
		echo "<p><input type='radio' class='filtrar_por' name='filtrar_por' t='plano'><label>Por Plano</label></p>";
		echo "<p><input type='radio' class='filtrar_por' name='filtrar_por' t='unidade'><label>Por Unidade Regional</label></p>";
		echo "<p><input type='radio' class='filtrar_por' name='filtrar_por' t='paciente'><label>Por Paciente</label></p>";
		echo "</div>";
        echo "<br><div id='label-pac' style='display: none;'><label>Paciente</label></div>";
		echo "<div class='bloco2' style='display:table;float:left;width:200px;' cod='' nome=''>";
		echo "<br/>";
		echo "<span id='plan' style='display:none;'><label><p>Plano</p></label><p>{$this->planos()}</p></span>";
		echo "<span id='ure' style='display:none;'><label><p>Unidade</p></label><p>{$this->unidades()}</p></span>";
		echo "<span id='pac' style='display:none;'>{$this->pacientes('select_paciente')}</span>";
		echo "</div>";
		echo "</td>";

		echo "<td>";

		echo "</td>";
		echo "</tr>";
		$this->periodo();
		echo "</tr>";
		echo "</table>";
		echo "<button id='listar_fatura' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Exibir Faturas</span></button></div>";
		echo "<div id='tabela_faturar'></div>";
	}

	private function capa_lote() {

		echo "<div id='formulario_lote' title='Formul&aacute;rio' tipo='' classe='' >";
		echo"<div id='num_lote' lote=''  title= 'LOTE'></div>";
		echo "<div id='dialog-zerados-fatura' lote='' title='Imprimir Fatura'  id_fatura='' av=''>
                <p>
                    <b>Deseja imprimir os itens zerados dessa fatura?</b>
                </p>
              </div>";
		echo "<button  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover nav-criar-fat' role='button'  aria-disabled='false' ><span class='ui-button-text'>Faturar</span></button>";
		echo "<button  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover nav-gerenciar-lot' role='button'  aria-disabled='false' ><span class='ui-button-text'>Gerenciar Lote</span></button>";

		echo "<center><h1>Gerar Lote</h1></center>";
		echo "<br>";
        echo "<div class='ui-state-highlight'>
                <span class='ui-icon  ui-icon-info' style='float:left;padding:2px;'>                
                </span> 
                Obs: Nessa funcionalidade só listas as faturas com status igual a <b>Faturado</b> e que <b>não foram inseridas</b> em um lote.                
                </div>
                <br>";
		echo "<table width='100%'>";
		echo "<tr>";
		echo "<td style='width:450px;'>";
		echo"<fieldset style='width:550px;'>";
		echo "<legend><b>Filtrar por:<b></legend>";
		echo "<div id='bloco1_lote' style='display:table;float:left;;width:200px;'>";
		echo "<p><input type='radio' class='filtrar_por_lote' name='filtrar_por' t='operadora'><label>Por Operadora </label></p>";
		//echo "<p><input type='radio' class='filtrar_por' name='filtrar_por' t='unidade'><label>Por Unidade Regional</label></p>";
		echo "<p><input type='radio' class='filtrar_por_lote' name='filtrar_por' t='paciente'><label>Por Paciente</label></p>";
		echo "</div>";
		echo "<div class='bloco2' style='display:table;float:left;width:400px;' cod='' nome=''>";
		echo "<br/>";
		echo "<span id='operadora_lote' style='display:none;'><label><p>Operadora</p></label><p>{$this->operadora()}</p></span>";
		//echo "<span id='ure' style='display:none;'><label><p>Unidade</p></label><p>{$this->unidades()}</p></span>";
        echo "<div id='label-pac' style='display: none;'><label>Paciente</label></div><br>";
        echo "<span id='pac_lote' style='display:none;'>{$this->pacientes('select_paciente')}</span>";
		echo "</div>";
		echo "</td>";

		echo "<td>";

		echo "</td>";
		echo "</tr>";
		$this->periodo();
		echo "</tr>";
		echo "</table>";
		echo "<button id='listar_fatura_lote' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Exibir Faturas</span></button></div>";
		echo "<div id='tabela_faturar_lote'></div>";
	}

    private function createDialogJustificativaCancelarLote()
    {
        $html = <<<HTML
<div id="dialog-justificativa-cancelar-lote" title="" style="display:none;">
	<div class="wrap" style="font-size: 12pt;">
		<p>
			<label for="justificativa-cancelar-lote"><b>Justificativa:</b></label>
		</p>
		<p>
			<textarea cols="50" rows="5" id="justificativa-cancelar-lote" name="justificativa_cancelar_lote" lote="" class="jutificativa-cancelar-lote-textarea"></textarea>
		</p>
	</div>
</div>
HTML;
        return $html;
    }

	private function gerarenciar_lote() {

		echo $this->createDialogJustificativaCancelarLote();

		echo "<div id='formulario_lote' title='Formul&aacute;rio' tipo='' classe='' >";

		echo "<div id='dialog-email' lote='' title='Envio de Lote' >
				<p>
					<label for='data-envio-lote' ><b>Data de envio</b></label>
				</p>
				<p>
					<input class='data data-envio' id='data-envio-lote' name = 'data-envio'>
				</p>
				<p>
					<label for='protocolo-envio' ><b>Número de Protocolo</b></label>
				</p>
				<p>
					<input class='protocolo-envio' id='protocolo-envio' name = 'protocolo-envio'>
				</p>
				</div>";
		if (isset($_SESSION['mensagemTemp'])) {
			echo \App\Core\Mensagem::getHTML($_SESSION['mensagemTemp']);
			unset($_SESSION['mensagemTemp']);
		}
		echo "<button  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover nav-gerar-lot' role='button'  aria-disabled='false' ><span class='ui-button-text'>Gerar Lote</span></button>";

		echo "<center><h1>Gerenciar Lote</h1></center>";
		echo "<br>";
		echo "<table>";
		echo "<tr>";
		echo "<td style='width:450px;'>";
		echo"<fieldset style='width:550px;'>";
		echo "<legend><b>Filtrar por:<b></legend>";
		echo "<div id='bloco1_lote' style='display:table;float:left;;width:200px;'>";
		echo "<p><input type='radio' class='filtrar_por_lote' name='filtrar_por' t='operadora'><label>Por Operadora </label></p>";
		//echo "<p><input type='radio' class='filtrar_por' name='filtrar_por' t='unidade'><label>Por Unidade Regional</label></p>";
		echo "<p><input type='radio' class='filtrar_por_lote' name='filtrar_por' t='paciente'><label>Por Paciente</label></p>";
		echo "</div>";
		echo "<div class='bloco2' style='display:table;float:left;width:200px;' cod='' nome='' status=''>";
		echo "<br/>";
		echo "<span id='operadora_lote' style='display:none;'><label><p>Operadora</p></label><p>{$this->operadora()}</p></span>";
		//echo "<span id='ure' style='display:none;'><label><p>Unidade</p></label><p>{$this->unidades()}</p></span>";
        echo "<div id='label-pac' style='display: none;'><label>Paciente</label></div><br>";
        echo "<span id='pac_lote' style='display:none;'>{$this->pacientes('select_paciente')}</span>";
		echo "</div>";
		echo "</td>";

		echo "<td>";
		echo"<fieldset style='height:95px;width:200px;'>";
		echo "<legend><b>Por tipo de documento:</b></legend>
		
 	<p>
 	<select name='tipo_documento' id='tipo_documento'>
        <option value='-1' selected >Selecione</option>
        <option value='0' >Fatura</option>
        <option value='1'>Orç. Inicial</option>
        <option value='2'>Orç. Prorrogação</option>
        <option value='3'>Orç. Aditivo</option>
 	</select></p>";
		echo "</fieldset>";
		echo "</td>";
		echo "</tr>";
		$this->periodo();
		echo "</tr>";
		echo "</table>";
		echo "<button id='gerenciar_lote' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Pesquisar</span></button></div>";
		echo "<div id='tabela_gerenciar_lote'></div>";

		echo "</div>";
	}

	private function formulario_dialog_orcamento_dias()
	{
		echo "<div id='dialog-formulario-orcamento-dias' title='Formul&aacute;rio'  classe='' cod=''>";
		echo alerta();
		echo "<p><b>Gerar valor de orçamento para quantos dias?</b><br/><input type='text' name='dias' id='dias' size='4'/>";
		echo "<input type='hidden' name='cod' id='cod' value='-1' />";
		echo "</div>";

	}

	private function faturar_avaliacao() {
		echo "<div id='dialog-email-1' lote='' av='1' title='Modo de emiss&atilde;o' ></div>";
		echo "<div id='dialog-zerados-fatura' lote='' title='Imprimir Fatura'  id_fatura='' av=''>
                <p>
                    <b>Deseja imprimir os itens zerados dessa fatura?</b>
                </p>
              </div>";
		echo "<div id='formulario_lote' title='Formul&aacute;rio' tipo='' classe='' >";

		echo "<div style='display:none;' id='dialog-zerados-salvar-fatura' lote='' title='Salvar Fatura'   dados= ''	 inicio= ''	  fim= ''	  cod= ''	  av= ''	 plano= ''  cap=''    obs= '' >
            <p><b>Deseja salvar fatura com  itens zerados?</b></p></div>";
		echo "<center><h1>Or&ccedil;amento Inicial</h1></center>";
		$this->orcamento_salvas_editar();
		$this->formulario_dialog_orcamento_dias();
		echo "</br><div>{$this->faturas_salvas(1)}</div></br>";
		echo "<style>";
		echo <<<CSS
div.msg {
    width: 100%;
    position: relative;
    float: left;
    text-align: center;
    font-size: 12px;
    margin-bottom: 20px;
}
.success {
    color: #006400;
    background-color: #C0FFC0;
}
.failed {
    color: #AA0000;
    background-color: #FDCCCC;
}
CSS;
		echo "</style>";
		$display = "none";
		if(isset($_GET['msg']) && !empty($_GET['msg'])) {
			$msg = base64_decode($_GET['msg']);
			$display = "block";
			$type = $_GET['type'];
			$class = "failed";
			if ($type === 's') {
				$class = "success";
			}
		}
		echo "<div style='display: $display ' class='msg $class'>
			<strong> $msg </strong>
		</div>";
		echo "<table>";
		echo "<tr>";
		echo "<td style='width:450px;'>";
		echo"<fieldset style='width:550px;'>";
		echo "<legend><b>Or&ccedil;amento<b></legend>";
		echo "<div id='bloco1_av' style='display:table;float:left;;width:200px;' cod='' nome='' op_avaliacao='1' >";
		echo "<span ><label><p>O que deseja fazer?</p></label><p><select id=op_avaliacao style='background-color:transparent;' class='COMBO_OBG'><option value='1'>Fazer Or&ccedil;amento</option><option value='2'>Visualizar Or&ccedil;amentos</option></select></p></span>";
		echo "
<span id='pac_avaliacao'><label><p>Paciente</p></label><p><div id='div-data-1'>{$this->pacientes_avaliacao()}</div></p></span>
";

		echo "</div>";
		echo "</fieldset>";
		echo "</td>";
		echo "</tr>";
		echo "</tr>";
		echo "<tr style='width:450px;' >
                <td>
                <div id='div-data'>
                    <fieldset style='width:550px;'>
                         <legend><b>Per&iacute;odo da Fatura</b></legend>";
		echo "<p><b>Inicio:</b><input type='text' name='inicio_fatura' value='' maxlength='10' size='15' class='OBG data periodo' id='inicio_fatura'/>" .
			"<b>Fim:</b> <input type='text' name='fim_fatura' value='' maxlength='10' size='15' class='OBG data' id='fim_fatura'/></p>
                    </fieldset>
                    </div>
                </td>
              </tr>";
		echo "</table>";
		echo "<button id='faturar_avaliacao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Pesquisar</span></button></div>";
		echo "<div id='tabela_faturar_av'><div id='tabela_faturar'></div></div>";
	}

	private function informar_glosa() {

		echo "<div id='formulario_lote' title='Formul&aacute;rio' tipo='' classe='' >";
		echo"<div id='num_lote' title= 'LOTE'></div>";
		echo"<div id='dialog-visualizar-glosa' title= 'Visualisar Fatura' idfatura='' tipo='1'>
    <p><b>Itens:</b><br/>
    <input type='radio' class='tipo_v'   name='tipo_v' checked=checked value=1></input>Todos.<br/>
    <input type='radio' class='tipo_v'    name='tipo_v' value=2></input>Glosados.
    <br/><input type='radio' class='tipo_v' name='tipo_v' value=3></input>N&atilde;o Glosados.
    </p></div>";
		echo "<center><h1>Previs&atilde;o de Recebimento</h1></center>";
		echo "<br>";
		echo "<table width='100%'>";
		echo "<tr>";
		echo "<td style='width:450px;'>";
		echo"<fieldset style='width:550px;'>";
		echo "<legend><b>Filtrar por:<b></legend>";
		echo "<div id='bloco1_lote' style='display:table;float:left;;width:200px;'>";
		echo "<p><input type='radio' class='filtrar_por_lote' name='filtrar_por' t='operadora'><label>Por Operadora </label></p>";
		//echo "<p><input type='radio' class='filtrar_por' name='filtrar_por' t='unidade'><label>Por Unidade Regional</label></p>";
		echo "<p><input type='radio' class='filtrar_por_lote' name='filtrar_por' t='paciente'><label>Por Paciente</label></p>";
		echo "</div>";
		echo "<div class='bloco2' style='display:table;float:left;width:200px;' cod='' nome=''>";
		echo "<br/>";
		echo "<span id='operadora_lote' style='display:none;'><label><p>Operadora</p></label><p>{$this->operadora()}</p></span>";
		//echo "<span id='ure' style='display:none;'><label><p>Unidade</p></label><p>{$this->unidades()}</p></span>";
		echo "<span id='pac_lote' style='display:none;'><label><p>Paciente</p></label><p>{$this->pacientes('select_paciente')}</p></span>";
		echo "</div>";
		echo "</td>";

		echo "<td>";

		echo "</td>";
		echo "</tr>";
		$this->periodo();
		echo "</tr>";
		echo "</table>";
		echo "<button id='listar_fatura_glosa' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Exibir Faturas</span></button></div>";
		echo "<div id='tabela_faturar_glosa'></div>";
	}

	private function add_glosa($id) {
		echo "<div id='div-glosa' id_fatura='{$id}'>";
		echo "<center><h1>Previs&atilde;o de Recebimento</h1></center>";
		$sql2 = "SELECT
 	UPPER(c.nome) AS paciente,
 	UPPER(u.nome) AS usuario,DATE_FORMAT(f.DATA,'%Y-%m-%d') as DATA,f.DATA_INICIO,f.DATA_FIM,
 	(CASE c.sexo
 	WHEN '0' THEN 'Masculino'
 	WHEN '1' THEN 'Feminino'
 	END) AS sexo1,
 	FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
 	e.nome as empresan,
 	c.`nascimento`, DATE_FORMAT(g.DATA_PAGAMENTO,'%d/%m/%Y ' ) as datapagamento,
 	p.nome as Convenio,p.id,c.*,
 	NUM_MATRICULA_CONVENIO
 	FROM
 	fatura as f inner join
 	clientes AS c on (f.PACIENTE_ID = c.idClientes) LEFT JOIN
 	empresas AS e ON (e.id = c.empresa) INNER JOIN
 	planosdesaude as p ON (p.id = c.convenio) INNER JOIN usuarios as u on (f.USUARIO_ID = u.idUsuarios) inner join glosa as g on (f.ID =g.FATURA_ID)
 	WHERE
 	f.ID = {$id}
 	ORDER BY
 	c.nome,datapagamento DESC LIMIT 1";

		$result = mysql_query($sql);
		$result2 = mysql_query($sql2);
		echo "<table style='width:100%;' >";
		while ($pessoa = mysql_fetch_array($result2)) {
			foreach ($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);
			}
			$idade = join("/", array_reverse(explode("-", $pessoa['nascimento']))) . "(" . $pessoa['idade'] . " anos)";

			if ($idade == "00/00/0000( anos)") {
				$idade = '';
			}
			echo "<tr bgcolor='#EEEEEE'>";
			echo "<td  style='width:60%;'><b>PACIENTE:</b></td>";
			echo "<td style='width:20%;' ><b>SEXO </b></td>";
			echo "<td style='width:30%;'><b>IDADE:</b></td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td style='width:60%;'>{$pessoa['paciente']}</td>";
			echo "<td style='width:20%;'>{$pessoa['sexo1']}</td>";
			echo "<td style='width:30%;'>{$idade}</td>";
			echo "</tr>";

			echo "<tr bgcolor='#EEEEEE'>";

			echo "<td style='width:60%;'><b>UNIDADE REGIONAL:</b></td>";
			echo "<td style='width:20%;'><b>CONV&Ecirc;NIO </b></td>";
			echo "<td style='width:30%;'><b>MATR&Iacute;CULA </b></td>";
			echo "</tr>";
			echo "<tr>";

			echo "<td style='width:60%;'>{$pessoa['empresan']}</td>";
			echo "<td style='width:20%;'>" . strtoupper($pessoa['Convenio']) . "</td>";
			echo "<td style='width:30%;'>" . strtoupper($pessoa['NUM_MATRICULA_CONVENIO']) . "</td>";
			echo "</tr>";

			echo "<tr bgcolor='#EEEEEE'>";

			echo "<td style='width:60%;'><b>Faturista:</b></td>";
			echo "<td style='width:20%;'><b>Data faturado: </b></td>";
			echo "<td style='width:30%;'><b>Per&iacute;odo</b></td>";
			echo "</tr>";
			echo "<tr>";

			echo "<td style='width:60%;'>{$pessoa['usuario']}</td>";
			echo "<td style='width:20%;'>" . join("/", array_reverse(explode("-", $pessoa['DATA']))) . "</td>";
			echo "<td style='width:30%;'>" . join("/", array_reverse(explode("-", $pessoa['DATA_INICIO']))) . " at&eacute; " . join("/", array_reverse(explode("-", $pessoa['DATA_FIM']))) . "</td>";
			echo "</tr>";



			$datapagamento = $pessoa['datapagamento'];
		}
		echo "</table>";


		$sql = "SELECT
                    f.*,
                    concat(B.principio,' - ',B.apresentacao) as principio,
                    B.DESC_MATERIAIS_FATURAR as segundonome,
                    (CASE f.TIPO
                    WHEN '0' THEN 'Medicamento'
                    WHEN '1' THEN 'Material'
                    WHEN '3' THEN 'DIETA' END) as apresentacao
		FROM
                    faturamento as f  INNER JOIN
                    catalogo B ON (f.CATALOGO_ID = B.ID)
		WHERE

                    f.FATURA_ID= {$id} and
                    f.TIPO IN (0,1,3)

	UNION
                SELECT
                    f.*,
                    B.DESC_COBRANCA_PLANO AS principio,
                     'NULL' as segundonome,
                    ('Servi&ccedil;os') AS apresentacao
                FROM
                    faturamento as f INNER JOIN
                    valorescobranca B ON (f.CATALOGO_ID =B.id)
                WHERE
                    f.FATURA_ID= {$id} AND
                    f.TIPO =2
         UNION
		SELECT DISTINCT
		    f.*,
                    CO.item AS principio,
                    'NULL' as segundonome,
                    ('Equipamentos') AS apresentacao
		FROM
                    faturamento as f INNER JOIN
                    cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)
		WHERE
                    f.FATURA_ID= {$id} AND
                    f.TIPO =5
	UNION
		SELECT DISTINCT
                    f.*,
                    CO.item AS principio,
                    'NULL' as segundonome,
                    ('Gasoterapia') as apresentacao
		FROM
                    faturamento as f inner join
                    cobrancaplanos as CO on (f.CATALOGO_ID = CO.id)
		WHERE
			f.FATURA_ID = {$id} AND
			f.TIPO = 6
                ORDER BY
                        tipo,
                        principio";

		echo "<p><b>Data Pagamento:</b><input type='text' name='inicio' id='inicio' value='{$datapagamento}' maxlength='10' size='15' qtd='0' class='OBG data periodo' /></p>";
		echo alerta();
		echo"<div id=glosa_fim Title= 'Previs&atilde;o de Recebimento'></div>";
		echo"<div id=dialog-glosamaior Title= 'Glosa'><b>O valor da glosa esta maior que o da fatura.</b></div>";
		echo"</fieldset>";
		echo "<table  width=100% class='mytable'>
		<thead >
		<th>Item</td>
		<th>Tipo</th>
		<th>Qtd</th>
		<th width=10% >Pre&ccedil;o Total</th>
		<th width=10% >Valor Glosa</th>
		<th width=15% >Motivo</th>


		</thead>";
		$result = mysql_query($sql);
		$total = 0.00;
		$total_glosa = 0.00;
		$cont_cor = 0;

		while ($row = mysql_fetch_array($result)) {
			$total += $row['VALOR_FATURA'] * $row['QUANTIDADE'];
			$total_glosa += $row['VALOR_GLOSA'];
			$valor_total = $row['VALOR_FATURA'] * $row['QUANTIDADE'];



			if ($cont_cor % 2 == 0) {
				$cor = '';
			} else {
				$cor = "bgcolor='#EEEEEE'";
			}

			//////2014-01-16
			///////// se for material coloca a descrição igual do Simpro que vai ser para fatura porque o principio é um ‘nome genérico’ que as enfermeiras usam.
			if($row['tipo'] == 1 && $row['segundonome']!= NULL){
				$nomeitem =$row['segundonome'];
			}else{
				$nomeitem = $row['principio'];
			}

			echo "<tr $cor>
			<td width='50%'>{$nomeitem}</td>
				<td>{$row['apresentacao']}</td>
					<td>{$row['QUANTIDADE']}</td>
					<td>R$ " . number_format($valor_total, 2, ',', '.') . "</td>
					<td>R$<input size='9' class='valor val_glosa'  val_fat=" . number_format($valor_total, 2, ',', '.') . " id_faturamento='{$row['ID']}' value='" . number_format($row['VALOR_GLOSA'], 2, ',', '.') . "'></input></td>
					<td width=15% ><select name='tipo'><option value='' " . (($row['MOTIVO_GLOSA'] == '') ? "selected" : "") . "></option>
			 	<option value='1' " . (($row['MOTIVO_GLOSA'] == '1') ? "selected" : "") . " >Cobran&ccedil;a n&atilde;o Contratada.</option>
			 	<option value='2' " . (($row['MOTIVO_GLOSA'] == '2') ? "selected" : "") . " >N&atilde;o Autorizado Previamente.</option>
			 	<option value='3' " . (($row['MOTIVO_GLOSA'] == '3') ? "selected" : "") . " >Valor em desacordo com tabela contratada.</option>
			 	<option value='4'" . (($row['MOTIVO_GLOSA'] == '4') ? "selected" : "") . " >Cobran&ccedil;a n&atilde;o confirmada em prontuario.</option>
			 	</select></td>


						</tr>";
			$cont_cor++;
		}


		echo "<tr><td colspan='4'  tot='{$total}' id='tot' align='right'><b>TOTAL: R$</b> <b><i>" . number_format($total, 2, ',', '.') . "</i></b></td><td colspan='2' id='tot_glosa' value='0.00'><b>Total Glosa: R$</b> <b><i>" . number_format($total_glosa, 2, ',', '.') . "</i></b></td></tr>";

		echo "</table>";
		echo "<button id='salvar_glosa' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></div>";

		echo "</div>";
	}

    private function edt_fatura($id,$av) {

        echo "<div id='div-geral-editar-fatura'>";
		echo"<div id='aguarde-reload'>Aguarde...</div>";
        if(isset($av)){
			$sqlFatura = "SELECT
fatura.`STATUS`,
fatura.DATA_INICIO,
fatura.DATA_FIM
FROM
fatura
WHERE
fatura.ID = {$id}";

			$result = mysql_query($sqlFatura);
			$statusFatura = mysql_result($result,0);
			$inicioFatura = mysql_result($result,0,1);
			$fimFatura = mysql_result($result,0,2);


            echo "<div id='div-glosa' id_fatura='{$id}'>";
            if ($av == 2){
                echo "<center><h1>Editar Orçamento Prorrogação</h1></center>";
            }else if ($av == 0) {
				$label = $statusFatura == '-1' ? 'Editar Pré-Fatura ' : 'Editar Fatura';
                echo "<center><h1>{$label}</h1></center>";
            }else if ($av == 3) {
                echo "<center><h1>Editar ".  htmlentities('Orçamento Aditivo')."</h1></center>";
            }
            $sql2 = "
            SELECT
                UPPER(c.nome) AS paciente,
                UPPER(u.nome) AS usuario,DATE_FORMAT(f.DATA,'%Y-%m-%d') as data_faturado,
                f.DATA_INICIO,
                f.DATA_FIM,
                (
                    CASE c.sexo
                    WHEN '0' THEN 'Masculino'
                    WHEN '1' THEN 'Feminino'
                    END
                ) AS sexo1,
                FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
                e.nome as empresan,
                c.`nascimento`,
                p.nome as Convenio,
                p.id,
                c.*,
                NUM_MATRICULA_CONVENIO,
                f.*,
                p.tipo_medicamento

 	FROM
                fatura as f INNER JOIN
                clientes AS c on (f.PACIENTE_ID = c.idClientes) LEFT JOIN
                empresas AS e ON (e.id = f.UR) INNER JOIN
                planosdesaude as p ON (p.id = f.PLANO_ID) INNER JOIN
                usuarios as u on (f.USUARIO_ID = u.idUsuarios)
 	WHERE
                f.ID = {$id}
 	ORDER BY
                c.nome DESC LIMIT 1";

            $result2 = mysql_query($sql2);
            echo "<table style='width:100%;'>";
            while ($pessoa = mysql_fetch_array($result2))
            {
                $tipoMedicamento = $pessoa['tipo_medicamento'];
                $nomePlano = $pessoa['Convenio'];
				$status=$pessoa['STATUS'];
                foreach ($pessoa AS $chave => $valor)
                {
                    $pessoa[$chave] = stripslashes($valor);
                }
                $idade = join("/", array_reverse(explode("-", $pessoa['nascimento']))) . "(" . $pessoa['idade'] . " anos)";

                if ($idade == "00/00/0000( anos)")
                {
                    $idade = '';
                }

                $cod_ident = $pessoa['idClientes'];
                echo "<tr bgcolor='#EEEEEE'>";
                echo "<td  style='width:60%;'><b>PACIENTE:</b></td>";
                echo "<td style='width:20%;' ><b>SEXO </b></td>";
                echo "<td style='width:30%;'><b>IDADE:</b></td>";
                echo "</tr>";

                echo "<tr>";
                echo "<td style='width:60%;'>{$pessoa['paciente']}</td>";
                echo "<td style='width:20%;'>{$pessoa['sexo1']}</td>";
                echo "<td style='width:30%;'>{$idade}</td>";
                echo "</tr>";

                echo "<tr bgcolor='#EEEEEE'>";
                echo "<td style='width:60%;'><b>UNIDADE REGIONAL:</b></td>";
                echo "<td style='width:20%;'><b>CONV&Ecirc;NIO </b></td>";
                echo "<td style='width:30%;'><b>MATR&Iacute;CULA </b></td>";
                echo "</tr>";

                echo "<tr>";
                echo "<td style='width:60%;'>{$pessoa['empresan']}</td>";
                echo "<td style='width:20%;'>" . strtoupper($pessoa['Convenio']) . "</td>";
                echo "<td style='width:30%;'>" . strtoupper($pessoa['NUM_MATRICULA_CONVENIO']) . "</td>";
                echo "</tr>";

                echo "<tr bgcolor='#EEEEEE'>";
                echo "<td style='width:60%;'><b>Faturista:</b></td>";
                echo "<td style='width:20%;'><b>Data faturado: </b></td>";
                echo "<td style='width:30%;'><b>Per&iacute;odo</b></td>";
                echo "</tr>";

                echo "<tr>";
                echo "<td style='width:60%;'>{$pessoa['usuario']}</td>";
                echo "<td style='width:20%;'>" . join("/", array_reverse(explode("-", $pessoa['data_faturado']))) . "</td>";
                echo "<td style='width:30%;'>" . join("/", array_reverse(explode("-", $pessoa['DATA_INICIO']))) . " at&eacute; " . join("/", array_reverse(explode("-", $pessoa['DATA_FIM']))) . "</td>";
                echo "</tr>";
                echo "<tr><td colspan='3'><b>Guia Tiss</b> <input type='text' id='guia_tiss' class='OBG' value='{$pessoa['GUIA_TISS']}'/></td><tr>";
                echo " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
                $plano = $pessoa['convenio'];
                $empresa= $pessoa['empresa'];
                $inicio = join("/", array_reverse(explode("-", $pessoa['DATA_INICIO'])));
                $fim = join("/", array_reverse(explode("-", $pessoa['DATA_FIM'])));

                $tipo=$pessoa['ORCAMENTO'];
                $obs=$pessoa['OBS'];
                $remocaoExterna = $pessoa['remocao'];
            }
            echo "</table>";

            $sql = "SELECT
                    f.*,
                     IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),B.DESC_MATERIAIS_FATURAR) AS principio,
                      (
                        CASE f.TIPO
                            WHEN '0' THEN 'Medicamento'
                            WHEN '1' THEN 'Material'
                            WHEN '3' THEN 'Dieta'
                         END
                    ) as apresentacao,
                    B.lab_desc as lab
                FROM
                    faturamento as f  INNER JOIN
                    catalogo as B ON (f.CATALOGO_ID = B.ID)
                WHERE
                    f.FATURA_ID= {$id} and
                    f.TIPO IN (0,1,3) and f.CANCELADO_POR is NULL
         UNION
                SELECT
                    f.*,
                    B.DESC_COBRANCA_PLANO AS principio,
                   ('Servi&ccedil;os') AS apresentacao,
                    '' as lab
                FROM
                    faturamento as f INNER JOIN
                    valorescobranca B ON (f.CATALOGO_ID =B.id)
                WHERE
                    f.FATURA_ID= {$id} AND
                    f.TIPO =2 AND f.TABELA_ORIGEM='valorescobranca'  and f.CANCELADO_POR is NULL
          UNION
                SELECT
                    f.*,
                    B.item AS principio,
                    ('Servi&ccedil;os') AS apresentacao,
                    '' as lab
                 FROM
                    faturamento as f INNER JOIN
                    cobrancaplanos B ON (f.CATALOGO_ID =B.id)
                 WHERE
                    f.FATURA_ID= {$id} AND
                    f.TIPO =2 AND f.TABELA_ORIGEM='cobrancaplano'  and f.CANCELADO_POR is NULL
           UNION
                  SELECT DISTINCT
                      f.*,
                      CO.item AS principio,
                      ('Equipamentos') AS apresentacao,
                    '' as lab
                   FROM
                      faturamento as f INNER JOIN
                      cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)
                   WHERE
                      f.FATURA_ID= {$id} AND
                      f.TIPO =5 AND f.TABELA_ORIGEM='cobrancaplano'  and f.CANCELADO_POR is NULL
             UNION
                  SELECT DISTINCT
                      f.*,
                      B.DESC_COBRANCA_PLANO AS principio,
                      ('Equipamentos') AS apresentacao,
                    '' as lab
                   FROM
                      faturamento as f INNER JOIN
                      valorescobranca B ON (f.CATALOGO_ID =B.id)
                   WHERE
                      f.FATURA_ID= {$id} AND
                      f.TIPO =5 AND f.TABELA_ORIGEM='valorescobranca'  and f.CANCELADO_POR is NULL
            UNION
                    SELECT DISTINCT
                      f.*,
                      CO.item AS principio,
                      ('Gasoterapia') AS apresentacao,
                    '' as lab
                    FROM
                      faturamento as f INNER JOIN
                      cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)
                    WHERE
                       f.FATURA_ID= {$id} AND
                       f.TIPO =6 AND f.TABELA_ORIGEM='cobrancaplano'  and f.CANCELADO_POR is NULL
          UNION
                    SELECT DISTINCT
                      f.*,
                       B.DESC_COBRANCA_PLANO AS principio,
                      ('Gasoterapia') AS apresentacao,
                    '' as lab
                    FROM
                      faturamento as f INNER JOIN
                      valorescobranca B ON (f.CATALOGO_ID =B.id)
                    WHERE
                       f.FATURA_ID= {$id} AND
                       f.TIPO =6 AND f.TABELA_ORIGEM='valorescobranca'  and f.CANCELADO_POR is NULL
                    ORDER BY
                       tipo,
                       principio";

            echo alerta();
            echo $this->formulario_editar();
            $disable = '';
            $style = "style='float:left;'";


            echo "<br/>
              <tr style='width:450px;'>
                <td>
                    <fieldset style='width:550px;'>
                        <legend><b>Per&iacute;odo da Fatura</b></legend> DE".
                "<input type='text' name='inicio' value='$inicio' maxlength='10' size='15' class='OBG  inicio-periodo-editar periodo-editar' {$disable} />" .
                " AT&Eacute; <input type='text' name='fim' value='$fim' maxlength='10' size='15' class='OBG  fim-periodo-editar periodo-editar' {$disable} />
                    </fieldset>
                </td>
              </tr>
              <br/>";
            if($status ==5){
                $checked="checked='checked'";

            }else{

                $checked="";
            }
            if(empty($remocaoExterna)){
                $selectedRemocaoExterna = '';
            }else {
                $selectedRemocaoExterna = $remocaoExterna;
            }
            $remocaoObg = $tipo == 0 ? 'COMBO_OBG' : '';
            echo "<br/><p> <input type='checkbox' $checked id='status_faturado' value='5' /><b><label for='status_faturado'> Status Faturado.</label></b></p>";
            echo "
					<p>
						<div id='div-select-remocao'>
						<label for='select-remocao'><b>Fatura de Remoção:</b></label>
							<select name='remocao' required id='select-remocao' class='{$remocaoObg}'>
								<option value='-1' ".($selectedRemocaoExterna == '' ? 'selected' : '')." ></option>
								<option value='N' ".($selectedRemocaoExterna == 'N' ? 'selected' : '')." >Não</option>
								<option value='S' ".($selectedRemocaoExterna == 'S' ? 'selected' : '')." >Sim</option>
							</select>
						</div>
					</p>
					";
			if($statusFatura == -1){
				$devolucoes = ModelDevolucao::getDevolucaoByPaciente($cod_ident, $inicioFatura, $fimFatura);
				$envios = ModelSaidas::getSaidaEquipamentoByPaciente($cod_ident, $inicioFatura, $fimFatura);
				$inicioBr = implode('/',array_reverse(explode('-',$inicioFatura)));
				$fimBr = implode('/',array_reverse(explode('-',$fimFatura)));

				if(!empty($devolucoes)){
					$htmlDevolucaoEnvio = "<table class='mytable mostrar-devolucao' width='100%'>
										<thead>
											<tr>
												<th colspan='8'><center>Devoluções entre {$inicioBr} até {$fimBr} </center></th>
											</tr>
										</thead>
										<tbody id='tbody-devolucao' class='ocultar-devolucao'>
											<tr style='background-color:grey' >
												<td colspan='4'>ITEM</td>
												<td>TIPO</td>
												<td>QUANTIDADE</td>
												<td>LOTE</td>
												<td>DATA</td>
											</tr>";
					foreach($devolucoes as $d){
						$htmlDevolucaoEnvio .= "<tr>
												<td colspan='4'>{$d['nome']}</td>
												<td>{$d['nomeTipo']}</td>
												<td>{$d['qtd']}</td>
												<td>{$d['LOTE']}</td>
												<td>".implode('/',array_reverse(explode('-',$d['DATA_DEVOLUCAO'])))."</td>
											</tr>";

					}
					$htmlDevolucaoEnvio .= "</tbody>
									</table><br>";

				}

				if(!empty($envios) ){
					$htmlDevolucaoEnvio .= "<table class='mytable mostrar-envio' width='100%'>
										<thead>
											<tr>
												<th colspan='8'><center>Equipamentos enviados entre {$inicioBr} até {$fimBr}</center> </th>
											</tr>
										</thead>
										<tbody id='tbody-envio' class='ocultar-envio'>
											<tr style='background-color:grey'>
												<td colspan='4'>ITEM</td>
												<td>TIPO</td>
												<td>QUANTIDADE</td>
												<td>LOTE</td>
												<td>DATA</td>
											</tr>";

					foreach($envios as $e){
						$htmlDevolucaoEnvio .= "<tr>
												<td colspan='4'>{$e['nome']}</td>
												<td>Equipamento</td>
												<td>{$e['qtd']}</td>
												<td>{$e['LOTE']}</td>
												<td>".implode('/',array_reverse(explode('-',$e['data'])))."</td>
											</tr>";
					}
					$htmlDevolucaoEnvio .= "</tbody>
								</table><br>";
				}

				$equipamentoAtivos = ModelEquipamento::getEquipamentosAtivosByPaciente($cod_ident);
				if(!empty($equipamentoAtivos) ){
					$htmlDevolucaoEnvio .= "<table class='mytable mostrar-equipamento-ativo' width='100%'>
										<thead>
											<tr>
												<th colspan='8'><center>Equipamentos ativos.</center> </th>
											</tr>
										</thead>
										<tbody id='tbody-equipamento-ativo' class='ocultar-equipamento-ativo'>
											<tr style='background-color:grey'>
												<td>Equipamento</td>
												<td>Data Envio</td>
											</tr>";

					foreach($equipamentoAtivos as $ativos){
						$htmlDevolucaoEnvio .= "<tr>
												<td>{$ativos['item']}</td>
												<td>{$ativos['datapt']}</td>
                                        </tr>";
					}
					$htmlDevolucaoEnvio .= "</tbody>
								</table><br>";

				}
				echo $htmlDevolucaoEnvio;
			}

            echo "<button id_paciente='{$cod_ident}' plano='{$plano}'  {$style} inicio='{$inicio}' nomeplano = '{$nomePlano}' tipo_medicamento = '{$tipoMedicamento}' tipo-documento ='{$tipo}'  fim='{$fim}' empresa='{$empresa}' contador=0 class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' id='add_item_orcamento' role='button' aria-disabled='false' >
                <span class='ui-button-text'>+</span>
              </button>";


            echo "<table  width=100% class='mytable' id='tabela_orcamento'>
                <thead >
                <tr>
                    <th>Item</th>
                    <th>Apresenta&ccedil;&atilde;o</th>
                    <th>Unidade</th>
                    <th>Qtd</th>
                    <th width=10% >Pre&ccedil;o</th>
                    <th width=10% >Pre&ccedil;o Tot.</th>
                    <th>Taxa</th>
                    <th>Pacote</th>
                </tr>
                <tr>
                    <td colspan='8' align='right' style='padding-right: 25px;'>
                        <b>Selecionar todos para pacote <input type='checkbox' class='selecionar-todos-pacote-editar' paciente='{$cod_ident}'></b>
                    </td>
                </tr>    
                </thead>";
            $result = mysql_query($sql);
            $total = 0.00;
            $total_glosa = 0.00;
            $cont_cor = 0;
            while ($row = mysql_fetch_array($result)) {

                $total += $row['VALOR_FATURA'] * $row['QUANTIDADE'];
                $total_glosa += $row['VALOR_GLOSA'];
                if ($cont_cor % 2 == 0) {
                    $cor = '';
                } else {
                    $cor = "bgcolor='#EEEEEE'";
                }
                $r = ereg_replace("[^a-zA-Z0-9_]", "", strtr($row['UNIDADE'], "�������������������������� ", "aaaaeeiooouucAAAAEEIOOOUUC_"));
                if($row['INCLUSO_PACOTE']==1){
                    $checked="checked='checked'";
                    $riscar_palavra = "riscar-palavra";
                }else{
                    $riscar_palavra ="";
                    $checked="";
                }

                ///////indentificar se eh desconto ou acrescimo pela cor.
                $text_cor='';
                if($row['DESCONTO_ACRESCIMO']< 0){
                    $text_cor='text-red';

                }else if ($row['DESCONTO_ACRESCIMO'] > 0){
                    $text_cor='text-green';

                }
                $laboratorio ='';
                if($row['TIPO']== 0){
                    $laboratorio ="<b>(".$row['lab'].")</b>";
                }

                $label_item = preg_replace('/[^A-Za-z0-9]/', '', $row['CATALOGO_ID']);

                echo "<tr $cor class='itens_fatura {$row['PACIENTE_ID']} editado' >
 	<td width='50%' >
 	<span class='{$riscar_palavra}' >" .
                    "<label id='label-novo-codigo-{$label_item}' style='display:none;'>
							<button cod='{$label_item}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover usar_tiss'>
								<span class='ui-button-text'>
									Usar Tiss
								</span>
							</button>
							<input type='text' id='codigo-proprio-{$label_item}' maxlength='11' name='codigo-proprio'>
						</label>
		    		<button id='bt-novo-codigo-{$label_item}' cod='{$label_item}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover bt_novo_codigo'>
		    			<span class='ui-button-text'>
		    				C&oacute;digo Pr&oacute;prio
							</span>
						</button>" .
                    ($row['CODIGO_PROPRIO'] != 0 ? "C&oacute;digo Pr&oacute;prio: " . $row['CODIGO_PROPRIO'] : "") .
                    " TUSS-" . $row['TUSS'] . "  N-" . $row['NUMERO_TISS'] . "
 	{$row['principio']}  $laboratorio </span> <img align='right' class='rem-item-solicitacao'  src='../utils/delete_16x16.png' title='Remover' border='0'></td>
 	<td>{$row['apresentacao']}</td>
 	<td>".  unidades_fatura($row['UNIDADE'])."</td>
 	<td ><input type='text'  value ={$row['QUANTIDADE']}  class='qtd OBG' size='7'  paciente='{$row['PACIENTE_ID']}'></input></td>
 	<td>R$ <input type='text'   name='valor_plano' size='10' class='valor_plano valor OBG' label-item='{$label_item}' codf='{$row['ID']}' plano='{$row['PLANO_ID']}' inicio='{$inicio}'  fim='{$fim}'  numero_tiss='{$row['NUMERO_TISS']}' qtd='{$row['QUANTIDADE']}'
 		paciente='{$row['PACIENTE_ID']}' catalogo_id='{$row['CATALOGO_ID']}' tuss='{$row['TUSS']}' tabela_origem='{$row['TABELA_ORIGEM']}' solicitacao='{$row['idSolicitacoes']}' tipo='{$row['TIPO']}' editado='0' custo='{$row['VALOR_CUSTO']}'
 		   value =" . number_format($row['VALOR_FATURA'], 2, ',', '.') . " ></input></td>
 	<td><span style='float:right;' id='val_total{$cont}'><b>R$  <span nome='val_linha'>" . number_format($row['VALOR_FATURA'] * $row['QUANTIDADE'], 2, ',', '.') . "</span></b></span></td>
        <td><input type='text' name='taxa' size='10' class='taxa OBG' value=" . number_format($row['DESCONTO_ACRESCIMO'], 2, ',', '.') . "></td>
            <td><input type='checkbox' $checked name='incluso_pacote' size='10' class='incluso_pacote incluso-pacote-{$cod_ident} OBG'></td>
 		   </tr>";

                $paciente = $row['PACIENTE_ID'];
                $cont_cor++;
            }
            echo "</table>";
            echo "<table width=100%>";

            echo "<tr><td colspan='6' style='font-size:12px;'><span style='float:right;' class='sub_fatura_" . $paciente . "'><b>TOTAL R$</b> <b><i>" . number_format($total, 2, ',', '.') . "</i></b></span></td></tr>";
            echo "<tr><td colspan='6' id='obs_".$paciente."'>OBSERVA&Ccedil;&Atilde;O: <br><textarea cols=115 rows=5 id='text_{$paciente}'>{$obs}</textarea></td></tr>";

            echo "</table>";

            //echo "<button class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover salvar_fatura' plano='{$plano}' tipo='$tipo' cod='$paciente' fatura_id='$id' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button>";
			$labelSalvar = 'Salvar';
			$labelFinalizar = 'Finalizar';
			if($av == 0 && $statusFatura == -1){
                $labelSalvar = 'Salvar Rascunho';
                $labelFinalizar = 'Faturar ';
            }
            echo "<button id='salvar_editar_fatura' status-fatura='{$status}' paciente='{$paciente}' idfat='{$id}' plano='{$plano}' empresa='{$empresa}'
						tipo='$tipo'  tag='salvar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only
						 ui-state-hover' role='button' aria-disabled='false' >
						 <span class='ui-button-text'>{$labelSalvar}</span>
						 </button>";

			echo "&nbsp;&nbsp
					<button id='finalizar_editar_fatura' tag='finalizar' paciente='{$paciente}' status-fatura='{$status}' idfat='{$id}' plano='{$plano}' empresa='{$empresa}' tipo='$tipo'
					class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'
					 role='button' aria-disabled='false' >
					 <span class='ui-button-text'>{$labelFinalizar}</span>
					 </button>
					 </div>";

            echo "</div>";
        }else{
            echo $this->formulario_editar();

        }
        echo "</div>";
    }

	private function formulario_editar() {

		echo "<div id='dialog-formulario-orcamento' title='Formul&aacute;rio' tipo_medicamento='' tipo-documento='' tipo='' classe='' cod=''>";
		echo alerta();
		echo "<div id='msg-generico'></div>";
		echo "<center><div id='tipos'>
  	<input type='radio' id='bmed' name='tipos' checked='checked' value='med' /><label for='bmed'>Medicamentos</label>
  	<input type='radio' id='bdie' name='tipos' value='die' /><label for='bdie'>Dieta</label>
  	<input type='radio' id='bmat' name='tipos' value='mat' /><label for='bmat'>Materiais</label>
  	<input type='radio' id='bserv' name='tipos' value='serv' /><label for='bserv'>Servi&ccedil;os</label>
  	<input type='radio' id='bgas' name='tipos' value='gas' /><label for='bgas'>Gasoterapia</label>
  	<input type='radio' id='bequip' name='tipos' value='eqp' /><label for='bequip'>Equipamento</label>
	<input type='radio' id='bextra' name='tipos' value='ext' /><label for='bextra'>Extras</label>
	<input type='radio' id='bkit' name='tipos' value='kit' /><label for='bkit'> Kits</label>

  	</div></center>";
		echo "<p><b>Busca:</b><br/><input type='text' name='busca-orcamento' id='busca-item-orcamento' />";
		echo "<div id='opcoes-kits'></div>";
		echo "<input type='hidden' name='cod' id='cod' value='-1' />";
		echo "<input type='hidden' name='tipo' id='tipo' value='0' />";
		echo "<input type='hidden' name='catalogo_id' id='catalogo_id' value='-1' />";
		echo "<input type='hidden' name='apresentacao' id='apresentacao' value='-1' />";
		echo "<input type='hidden' name='custo' id='custo' value='-1' />";
		echo "<input type='hidden' name='contador' id='contador' value='0.00' />";
		echo "<input type='hidden' name='valor_plano' id='valor_plano' value='-1' />";
		echo "<input type='hidden' name='tabela_origem' id='tabela_origem' value='-1' />";
		echo "<input type='hidden' name='inicio' id='inicio' value='-1' />";
		echo "<input type='hidden' name='fim' id='fim' value='-1' />";
		echo "<input type='hidden' name='tuss' id='tuss' value='-1' />";
		echo "<p><b>Nome:</b><input type='text' name='nome' id='nome' class='OBG' />";

		echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
		echo "<button id='add-itemorcamento' tipo='' >+</button></div>";
	}

	//////////////jeferson editar orcamento 17-5-2013

	private function edt_orcamento($id,$av,$n) {



		echo "<div id='div-geral-editar-fatura'>";
		echo"<div id='aguarde-reload'>Aguarde...</div>";
		if(isset($av)){
			echo "<div id='div-glosa' id_fatura='{$id}'>";
			if($n == 1 && $av == 1){
				echo "<center><h1>Usar para novo Orçamento Inicial</h1></center>";
			}elseif($av == 1){
				echo "<center><h1>Editar Orçamento Inicial</h1></center>";
			}elseif($av == 2){
				echo "<center><h1>Editar Orçamento Prorrogaçao</h1></center>";
			}
			$sql2 = "SELECT
             	UPPER(c.nome) AS paciente,
             	UPPER(u.nome) AS usuario,DATE_FORMAT(f.DATA,'%Y-%m-%d') as data_faturado,
                        f.DATA_INICIO,
                        f.DATA_FIM,
             	(CASE c.sexo
             	WHEN '0' THEN 'Masculino'
             	WHEN '1' THEN 'Feminino'
             	END) AS sexo1,
             	FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
             	e.nome as empresan,
             	c.`nascimento`,
             	p.nome as Convenio,
             	p.id,
             	c.*,
             	f.*,
             	NUM_MATRICULA_CONVENIO,
                f.OBS,
                p.tipo_medicamento
 	FROM
             	fatura as f inner join
             	clientes AS c on (f.PACIENTE_ID = c.idClientes) LEFT JOIN
             	empresas AS e ON (e.id = f.UR) INNER JOIN
             	planosdesaude as p ON (p.id = f.PLANO_ID) INNER JOIN usuarios as u on (f.USUARIO_ID = u.idUsuarios)
 	WHERE
 	           f.ID = {$id}
 	ORDER BY
 	      c.nome DESC LIMIT 1";


			$result2 = mysql_query($sql2);
			echo "<table style='width:100%;' >";
			while ($pessoa = mysql_fetch_array($result2)) {
				foreach ($pessoa AS $chave => $valor) {
					$pessoa[$chave] = stripslashes($valor);
				}
				$tipoMedicamento = $pessoa['tipo_medicamento'];
				$status = $pessoa['STATUS'];
				$nomePlano = $pessoa['Convenio'];
				$idade = join("/", array_reverse(explode("-", $pessoa['nascimento']))) . "(" . $pessoa['idade'] . " anos)";

				if ($idade == "00/00/0000( anos)") {
					$idade = '';
				}
				echo "<tr bgcolor='#EEEEEE'>";
				echo "<td  style='width:60%;'><b>PACIENTE:</b></td>";
				echo "<td style='width:20%;' ><b>SEXO </b></td>";
				echo "<td style='width:30%;'><b>IDADE:</b></td>";
				echo "</tr>";

				echo "<tr>";
				echo "<td style='width:60%;'>{$pessoa['paciente']}</td>";
				echo "<td style='width:20%;'>{$pessoa['sexo1']}</td>";
				echo "<td style='width:30%;'>{$idade}</td>";
				echo "</tr>";

				echo "<tr bgcolor='#EEEEEE'>";
				echo "<td style='width:60%;'><b>UNIDADE REGIONAL:</b></td>";
				echo "<td style='width:20%;'><b>CONV&Ecirc;NIO </b></td>";
				echo "<td style='width:30%;'><b>MATR&Iacute;CULA </b></td>";
				echo "</tr>";

				echo "<tr>";
				echo "<td style='width:60%;'>{$pessoa['empresan']}</td>";
				echo "<td style='width:20%;'>" . strtoupper($pessoa['Convenio']) . "</td>";
				echo "<td style='width:30%;'>" . strtoupper($pessoa['NUM_MATRICULA_CONVENIO']) . "</td>";
				echo "</tr>";

				echo "<tr bgcolor='#EEEEEE'>";
				echo "<td style='width:60%;'><b>Faturista:</b></td>";
				echo "<td style='width:20%;'><b>Data faturado: </b></td>";
				echo "<td style='width:30%;'><b>Per&iacute;odo</b></td>";
				echo "</tr>";

				echo "<tr>";
				echo "<td style='width:60%;'>{$pessoa['usuario']}</td>";
				echo "<td style='width:20%;'>" . join("/", array_reverse(explode("-", $pessoa['data_faturado']))) . "</td>";
				echo "<td style='width:30%;'>" . join("/", array_reverse(explode("-", $pessoa['DATA_INICIO']))) . " at&eacute; " . join("/", array_reverse(explode("-", $pessoa['DATA_FIM']))) . "</td>";
				echo "</tr>";
				echo " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
				//  echo "<tr><td colspan='3'><b>Guia Tiss</b> <input type='text' id='guia_tiss' class='OBG' value='{$pessoa['GUIA_TISS']}'/></td><tr>";
				$cod_ident = $pessoa['idClientes'];
				$plano = $pessoa['PLANO_ID'];
				$empresa= $pessoa['UR'];
				$inicio = join("/", array_reverse(explode("-", $pessoa['DATA_INICIO'])));
				$fim = join("/", array_reverse(explode("-", $pessoa['DATA_FIM'])));
				$capmedica_id = $pessoa['CAPMEDICA_ID'];
				$obs=$pessoa['OBS'];
			}
			echo "</table>";
			echo $this->formulario_editar();
			$sql = "SELECT
                    f.*,
                     IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),B.DESC_MATERIAIS_FATURAR) AS principio,
                      (
                        CASE f.TIPO
                            WHEN '0' THEN 'Medicamento'
                            WHEN '1' THEN 'Material'
                            WHEN '3' THEN 'Dieta'
                         END
                    ) as apresentacao,
                    B.lab_desc as lab
                FROM
                    faturamento as f  INNER JOIN
                    catalogo as B ON (f.CATALOGO_ID = B.ID)
                WHERE
                    f.FATURA_ID= {$id} and
                    f.TIPO IN (0,1,3)
                    AND f.CANCELADO_POR IS NULL
         UNION
                SELECT
                    f.*,
                    B.DESC_COBRANCA_PLANO AS principio,
                   ('Servi&ccedil;os') AS apresentacao,
                    '' as lab
                FROM
                    faturamento as f INNER JOIN
                    valorescobranca B ON (f.CATALOGO_ID =B.id)
                WHERE
                    f.FATURA_ID= {$id} AND
                    f.TIPO =2 AND f.TABELA_ORIGEM='valorescobranca'
                    AND f.CANCELADO_POR is NULL
          UNION
                SELECT
                    f.*,
                    B.item AS principio,
                    ('Servi&ccedil;os') AS apresentacao,
                    '' as lab
                 FROM
                    faturamento as f INNER JOIN
                    cobrancaplanos B ON (f.CATALOGO_ID =B.id)
                 WHERE
                    f.FATURA_ID= {$id} AND
                    f.TIPO =2 AND f.TABELA_ORIGEM='cobrancaplano'
                    AND f.CANCELADO_POR is NULL
           UNION
                  SELECT DISTINCT
                      f.*,
                      CO.item AS principio,
                      ('Equipamentos') AS apresentacao,
                    '' as lab
                   FROM
                      faturamento as f INNER JOIN
                      cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)
                   WHERE
                      f.FATURA_ID= {$id} AND
                      f.TIPO =5 AND f.TABELA_ORIGEM='cobrancaplano'
                      AND f.CANCELADO_POR is NULL
             UNION
                  SELECT DISTINCT
                      f.*,
                      B.DESC_COBRANCA_PLANO AS principio,
                      ('Equipamentos') AS apresentacao,
                    '' as lab
                   FROM
                      faturamento as f INNER JOIN
                      valorescobranca B ON (f.CATALOGO_ID =B.id)
                   WHERE
                      f.FATURA_ID= {$id} AND
                      f.TIPO =5 AND f.TABELA_ORIGEM='valorescobranca'
                      AND f.CANCELADO_POR is NULL
            UNION
                    SELECT DISTINCT
                      f.*,
                      CO.item AS principio,

                      ('Gasoterapia') AS apresentacao,
                    '' as lab
                    FROM
                      faturamento as f INNER JOIN
                      cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)
                    WHERE
                       f.FATURA_ID= {$id} AND
                       f.TIPO =6 AND f.TABELA_ORIGEM='cobrancaplano'
                       AND f.CANCELADO_POR is NULL
          UNION
                    SELECT DISTINCT
                      f.*,
                       B.DESC_COBRANCA_PLANO AS principio,

                      ('Gasoterapia') AS apresentacao,
                    '' as lab
                    FROM
                      faturamento as f INNER JOIN
                      valorescobranca B ON (f.CATALOGO_ID =B.id)
                    WHERE
                       f.FATURA_ID= {$id} AND
                       f.TIPO =6 AND f.TABELA_ORIGEM='valorescobranca'
                       AND f.CANCELADO_POR is NULL
                    ORDER BY
                       tipo,
                       principio";

			;

			echo alerta();

			echo "<br/><tr style='width:450px;' ><td ><fieldset style='width:550px;'><legend><b>Per&iacute;odo da Fatura</b></legend>";
			echo "DE" .
				"<input type='text' name='inicio' value='$inicio' maxlength='10' size='15' class='OBG data periodo' {$disable} />" .
				" AT&Eacute; <input type='text' name='fim' value='$fim' maxlength='10' size='15' class='OBG data' {$disable} />
  	</fieldset></td></tr> <br/>";
			echo "<button id_paciente='{$cod_ident}' plano='{$plano}'  style='float:left;' inicio='{$inicio}' nomeplano = '{$nomePlano}' tipo_medicamento = '{$tipoMedicamento}' fim='{$fim}' empresa='{$empresa}' contador=0 class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover ' id='add_item_orcamento' role='button' aria-disabled='false' ><span class='ui-button-text'>+</span></button>";

			echo "<table  width=100% class='mytable' id='tabela_orcamento'>
 	<thead >
 	<th>Item</th>
 	<th>Apresenta&ccedil;&atilde;o</th>
 	<th>Unidade</th>
 	<th>Qtd</th>
 	<th width=10% >Pre&ccedil;o</th>
 	<th width=10% >Pre&ccedil;o Tot.</th>
        <th>Taxa</th>
        <th>Pacote</th>
        <tr>
            <td colspan='8' align='right' style='padding-right: 25px;'>
                <b>Selecionar todos para pacote <input type='checkbox' class='selecionar-todos-pacote-editar' paciente='{$cod_ident}'></b>
            </td>
        </tr> 
        </thead>";
			$result = mysql_query($sql);
			$total = 0.00;
			$total_glosa = 0.00;
			$cont_cor = 0;
			$botoes = '<img align=\'right\' class=\'rem-item-solicitacao\'  src=\'../utils/delete_16x16.png\' title=\'Remover\' border=\'0\'>';
			while ($row = mysql_fetch_array($result)) {
				$total += $row['VALOR_FATURA'] * $row['QUANTIDADE'];
				$total_glosa += $row['VALOR_GLOSA'];
				if ($cont_cor % 2 == 0) {
					$cor = '';
				} else {
					$cor = "bgcolor='#EEEEEE'";
				}

				if($row['INCLUSO_PACOTE']==1){
					$checked="checked='checked'";
					$riscar_palavra = "riscar-palavra";
				}else{
					$riscar_palavra ="";
					$checked="";
				}

				///////indentificar se eh desconto ou acrescimo pela cor.
				$text_cor='';
				if($row['DESCONTO_ACRESCIMO']< 0){
					$text_cor='text-red';

				}else if ($row['DESCONTO_ACRESCIMO'] > 0){
					$text_cor='text-green';

				}

				//////2014-01-16
				///////// se for material coloca a descrição igual do Simpro que vai ser para fatura porque o principio é um ‘nome genérico’ que as enfermeiras usam.
				if($row['tipo'] == 1 && $row['segundonome']!= NULL){
					$nomeitem =$row['segundonome'];
				}else{
					$nomeitem = $row['principio'];
				}

				$laboratorio ='';
				if($row['TIPO']== 0){
					$laboratorio ="<b>(".$row['lab'].")</b>";
				}

				$r = ereg_replace("[^a-zA-Z0-9_]", "", strtr($row['UNIDADE'], "�������������������������� ", "aaaaeeiooouucAAAAEEIOOOUUC_"));
				echo "<tr $cor class='itens_fatura {$row['PACIENTE_ID']} editado' >
 	              <td width='50%'  >
                         <span class='{$riscar_palavra}' >TUSS-{$row['TUSS']}  N-" . $row['NUMERO_TISS'] . "
 	                     {$nomeitem}" . $laboratorio. ""
					. "</span> " . $botoes . "
                </td>
                <td>{$row['apresentacao']}</td>
 	              <td>".  unidades_fatura($row['UNIDADE'])."</td>
                <td >
                    <input type='text'  value ={$row['QUANTIDADE']}  class='qtd OBG' size='7'
                    paciente='{$row['PACIENTE_ID']}'></input>
                </td>
 		            <td>
                  R$
                  <input type='text'   name='valor_plano' size='10' class='valor_plano valor OBG' editado='0'
                    codf='{$row['ID']}' numero_tiss='{$row['NUMERO_TISS']}'  qtd='{$row['QUANTIDADE']}'
                    catalogo_id='{$row['CATALOGO_ID']}' paciente='{$row['PACIENTE_ID']}' tuss='{$row['TUSS']}'
                     solicitacao='{$row['idSolicitacoes']}' tabela_origem ='{$row['TABELA_ORIGEM']}' tipo='{$row['TIPO']}'
                      plano ='{$row['PLANO_ID']}' 	custo='{$row['VALOR_CUSTO']}'	   value =" . number_format($row['VALOR_FATURA'], 2, ',', '.') . " ></input>
              </td>
 		  <td><span style='float:right;' id='val_total{$cont}'><b>R$  <span nome='val_linha'>" . number_format($row['VALOR_FATURA'] * $row['QUANTIDADE'], 2, ',', '.') . "</span></b></span></td>
 		    <td><input type='text' name='taxa' size='10' class='taxa OBG $text_cor' value=" . number_format($row['DESCONTO_ACRESCIMO'], 2, ',', '.') . "></td>
            <td><input type='checkbox'  $checked  name='incluso_pacote' size='10' class='incluso_pacote incluso-pacote-{$cod_ident} OBG'></td>
 		  </tr>";
				$paciente = $row['PACIENTE_ID'];



				$cont_cor++;

			}
			echo "</table>";
			echo "<br/>";
			echo "<table  width=100% class='mytable' >";
			echo "<tr><td colspan='6' style='font-size:12px;'><span style='float:right;' class='sub_fatura_" . $paciente . "'><b>TOTAL R$</b> <b><i>" . number_format($total, 2, ',', '.') . "</i></b></span></td></tr>";

			echo "<tr><td colspan='6' ><label>OBSERVA&Ccedil;&Atilde;O::</label></td></tr>";
			echo "<tr><td colspan='6' id='obs_".$paciente."'><textarea cols='100%'  rows=5 id='text_{$paciente}'>{$obs}</textarea></td></tr>";
			echo "</table>";
//			echo "<button class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover ' av='{$av}' tipo='1' status = '{$status}' paciente='$cod_ident' id_fatura='$id' empresa='{$empresa}' id='salvar_editar_orcamento' finalizar='0' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button>";

			if($n == 1){
                echo "<button id='salvar-usar-novo-orcamento' cap='{$capmedica_id}' plano='{$plano}' class='  ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover ' av='{$av}' tipo='1' status = '{$status}' paciente='$cod_ident' fatura_origem='$id'  id_fatura=0 empresa='{$empresa}'  finalizar='0' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button>";
				echo "<button id='finalizar-usar-novo-orcamento'  cap='{$capmedica_id}' plano='{$plano}' av='{$av}' fatura_origem='$id'  id_fatura=0 inicio='{$inicio}' fim='{$fim}' paciente='{$cod_ident}' status = '{$status}' empresa='{$empresa}' tipo='1' id_capmedica='{$capmedica_id}' plano='{$plano}' finalizar='1' class='  ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Finalizar</span></button></div>";

			}else{
				echo "<button id='finalizar_editar_orcamento' av='{$av}'  id_fatura='{$id}' inicio='{$inicio}' fim='{$fim}' paciente='{$cod_ident}' tipo='1' status = '{$status}' empresa='{$empresa}' id_capmedica='{$capmedica_id}' plano='{$plano}' finalizar='1' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Finalizar</span></button></div>";
                echo "<button class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover ' av='{$av}' tipo='1' status = '{$status}' paciente='$cod_ident' id_fatura='$id' empresa='{$empresa}' id='salvar_editar_orcamento' finalizar='0' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button>";

			}




			echo "</div>";
		}else{

			echo $this->formulario_editar();
			$dir = "salvas/orcamento_editar";

			$filename = $dir . "/" . $id . ".htm";
			$str = stripslashes(file_get_contents($filename));
			$str = stripslashes(file_get_contents($filename));
			$string = explode('@|#', $str);
			echo $string[0];






		}
		echo "</div>";
	}

	///////////////fim jeferson editar orcamento

	private function fatura_avulsa() {
		echo"<div id='fatav'>";
        $responseEmpresas = Filial::getUnidadesAtivas();

		echo"<div id='dialog-diaria-orcamento-avulso' title='Imprimir Orçamento Avulso' idorcamento='' tipo='1'>
            <p><b>Numero de diárias :</b><br/>
             <input type='text' class='qtd'   name='diarias_orcamento_avulso' id='diarias_orcamento_avulso' value='0'>
            </p>
            <fieldset style='width:95%;'><legend><b>Imprimir a observação.</b></legend>
                   <select name='observacao'>
                      <option value='0' selected>Não</option>
                      <option value='1'>Sim</option>
                  </select><br>
            </fieldset>
            <fieldset style='width:95%;'><legend><b>Escolha a UR da Impressão</b></legend>
            <select id='select-escolher-empresa-imprimir-orcamento'>";
        foreach($responseEmpresas as $empresa){
            $selected = $_SESSION['empresa_principal'] == $empresa['id'] ? 'selected' : '';
            echo "<option value='{$empresa['id']}' {$selected} logo='{$empresa['logo']}' nome-empresa='{$empresa['nome']}'>{$empresa['nome']}</option>";
        }
        echo "</select>
              </fieldset>
              </div>";
		echo "<center><h1>Or&ccedil;amento Avulso</h1></center>";
		echo "<p><b>Op&ccedil;&atilde;o</b><select id='sel_fat_avu' style='background-color:transparent;' >
            <option value=''>Selecione</option><option value='1'>Visualizar Or&ccedil;amento</option>
            <option value='0'>Fazer Or&ccedil;amento</option></select><p>";
		echo "<div id='div_list_avulsa'>";
		$cont = 1;
		if ($_SESSION["empresa_principal"] == 1) {
			$result = mysql_query("SELECT PACIENTE FROM orcamento_avulso group by PACIENTE ORDER BY PACIENTE ");
		} else {
			$result = mysql_query("SELECT
                o.PACIENTE,
                u.empresa
                 FROM
                 orcamento_avulso as o inner join
                 usuarios as u on (o.USUARIO_ID = u.idUsuarios)
                 where u.empresa = " . $_SESSION["empresa_principal"] . "
                  group by PACIENTE ORDER BY PACIENTE ");
		}
		echo "<p>
                <b>Pacientes:</b>
                <select  style='background-color:transparent;' class='OBG' id='select_paciente_avu'>";

		while (@$row = mysql_fetch_array($result)) {
			if ($cont == 1) {
				echo "<option value='' selected>Selecione</option>";
			}
			echo "<option value='{$row['PACIENTE']}'>" . strtoupper($row['PACIENTE']) . "</option>";
			$cont++;
		}
		echo"</select><input id='tpacient' type='checkbox' />Todos</p></br>";
		echo "<input type ='hidden' id='select_unidadeavu' value='{$_SESSION["empresa_principal"]}' />";
		echo "<button id='pesquisar_avulsa' style='float:left;' inicio='{$inicio}'  fim='{$fim}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover ' role='button' aria-disabled='false' >
               <span class='ui-button-text'>Pesquisar</span>
              </button>";
		echo"<div id='div-list'></div>";
		echo "</div>";

		echo "<div id='div_fat_avulsa'>";
		echo "<p>
                  <b>Nome:</b>
                  <input type='text' size ='60' class='OBG' id='paciente_avulsa' name='paciente'/>
                  <b>CPF:</b>
                  <input type='text' size ='20' maxlength='14' class='cpf OBG' id='cpf-avulsa' name='cpf'/>
                  <input type ='hidden' class= 'bloco2' cod='' />
              </p>";
        echo "<p>
                  <b>Endereço:</b>
                  <input type='text' size ='90' class='OBG' id='endereco-avulsa' name='endereco'/>
              </p>";

		$cont = 1;
		$result = mysql_query("SELECT
                                *
                                FROM
                                    planosdesaude
                                WHERE
                                    planosdesaude.ATIVO = 's'
                                ORDER BY
                                    nome");
		echo "<p>
                <b>Plano:</b>
                <select name='{$name}' style='background-color:transparent;' class='OBG' id='select_planoavu'>";

		while (@$row = mysql_fetch_array($result)) {
			if ($cont == 1) {
				echo "<option value='' selected>Selecione</option>";
			}
			echo "<option value='{$row['id']}'>" . strtoupper($row['nome']) . "</option>";
			$cont++;
		}
		echo"</select></p>";
		echo "<span><label><b>Unidade</b></label>{$this->unidades()}</span>";
		$this->periodo();
		$this->formulario();
		echo "<button id='add-item-faturaav' style='float:left;' inicio='{$inicio}'  fim='{$fim}' empresa='{$_SESSION["empresa_principal"]}' contador=0 class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover ' role='button' aria-disabled='false' ><span class='ui-button-text'>+</span></button>";
		echo "<br/><br/><br/><table  width=100% class='mytable' id='tabela_fatavulsa'><thead>";
		echo "<tr>
 	<th>Item</th>
 	<th>Apresenta&ccedil;&atilde;o</th>
 	<th>Unidade</th>
 	<th>Qtd</th>
 	<th>Pre&ccedil;o Unit.</th>
 	<th>Pre&ccedil;o Total</th>

 	</tr></thead>

 	</table>";
		echo"<p><span style='float:right;' id= 'total_fatura' ><b>TOTAL PACIENTE: R$</b> <b><i>" . number_format($subtotal, 2, ',', '.') . "</i></b></span></p>";
		echo "<p>
                  <b>Observação:</b>
                   <textarea cols=120 rows=5 id='observacao_avulsa'></textarea>
                   </p>";
		echo "<button id='salvar-faturaav' style='float:left;' inicio='{$inicio}'  fim='{$fim}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover ' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button>";
		echo "</div>";
		echo "</div>";
	}

	private function visualizar_faturas() {

		echo "<div id='div_visualizar_faturas' title='Formul&aacute;rio' tipo='' classe='' >";
		echo "<div id='dialog-zerados-fatura' lote='' title='Imprimir Fatura' empresa='' id_fatura='' av=''>
                <p>
                    <b>Deseja imprimir os itens zerados dessa fatura?</b>
                </p>
              </div>";
		if ($_SESSION["empresa_principal"] == 1) {
			echo "<button  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover nav-criar-fat' role='button'  aria-disabled='false' ><span class='ui-button-text'>Faturar</span></button>";
			echo "<button  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover nav-gerar-lot' role='button'  aria-disabled='false' ><span class='ui-button-text'>Gerar Lote</span></button>";
		}
		echo"<div id='num_lote' title= 'LOTE'></div>";
		echo"<div id='dialog-visualizar-glosa' title= 'Visualisar Fatura' idfatura='' tipo='1'>
 	<p><b>Itens:</b><br/>
 	<input type='radio' class='tipo_v'   name='tipo_v' checked=checked value=1>Todos.<br/>
 	<input type='radio' class='tipo_v'    name='tipo_v' value=2>Glosados.
 	<br/><input type='radio' class='tipo_v' name='tipo_v' value=3>N&atilde;o Glosados.
 	</p></div>";
		echo "
<div id='dialog-justificativa-reabrir' title= 'Reabrir Fatura' fatura='' paciente='' tipo='' periodo=''>
 	<p>
		<b>Justificativa:</b>
		<br/><br/>
		<textarea id='justificativa-reabrir' col='50' rows='3'></textarea>
 	</p>
</div>";
		echo "<center><h1>Visualizar Faturas/Orçamentos</h1></center>";
		echo "<br>";
		$this->fatura_salvas_editar();
		$this->orcamento_salvas_editar();
		echo "<table width='100%'>";
		echo "<tr>";
		echo "<td style='width:450px;'>";
		echo"<fieldset style='width:550px;'>";
		echo "<legend><b>Filtrar por:<b></legend>";
		echo "<div id='bloco1_lote' style='display:table;float:left;;width:200px;'>";
		echo "<p><input type='radio' class='filtrar_por_lote' name='filtrar_por' t='operadora'><label>Por Operadora </label></p>";
		if($_SESSION["empresa_principal"]== 1){
			echo "<p><input type='radio' class='filtrar_por_lote' name='filtrar_por' t='unidade'><label>Por Unidade Regional</label></p>";
		}
		echo "<p><input type='radio' class='filtrar_por_lote' name='filtrar_por' t='paciente'><label>Por Paciente</label></p>";
		echo "</div>";
		echo "<div class='bloco2' style='display:table;float:left;width:200px;' cod='' nome=''>";
		echo "<br/>";
        echo "<div id='label-pac' style='display: none;'><label>Paciente</label></div><br>";
		echo "<span id='operadora_lote' style='display:none;'><label><p>Operadora</p></label><p>{$this->operadora()}</p></span>";
		echo "<span id='ure' style='display:none;'><label><p>Unidade</p></label><p>{$this->unidades()}</p></span>";
		echo "<span id='pac_lote' style='display:none;'>{$this->pacientes('select_paciente')}</span>";
		echo "</div>";
		echo "</td>";

		echo "<td>";

		echo "</td>";
		echo "</tr>";
		echo "<tr></tr>";
		echo "<tr></tr>";
		echo"<tr>
                <td>
                  <label for='tipo_documento'><b>Tipo Documento:</b></label>
                  <select style='background-color:transparent;'  id='tipo_documento'>
                     <option value='t' selected>Todos</option>
                     <option value='0' >Fatura</option>
                     <option value='1' >Orçamento Inicial</option>
                     <option value='2' >Orçamento de Prorrogação</option>
                     <option value='3' >Orçamento de Aditivo</option>
                   </select>
                </td>
             </tr>";
		echo "<tr></tr>";
		echo "<tr></tr>";

		echo "<tr><td><input type='checkbox' id='visualizar_cancelado' name='visualizar_cancelado' ><b>Mostrar faturas/orçamentos cancelados</b></td></tr>";
		echo "<tr></tr>";
		echo "<tr></tr>";
		$this->periodo();
		echo "</tr>";
		echo "<tr style='width:450px;' >
  	          <td >
  	            <fieldset style='width:550px;'>
  	               <legend><b>Per&iacute;odo que foi feita a fatura.</b></legend>";
		echo "<p><b>Inicio:</b><input type='text' name='inicio_fatura' value='' maxlength='10' size='15' class='OBG data periodo' id='inicio_fatura'/>" .
			" <b>Fim:</b> <input type='text' name='fim_fatura' value='' maxlength='10' size='15' class='OBG data' id='fim_fatura'/></p>
  	</fieldset></td></tr>";
		echo "</table>";
		echo "<button id='listar_faturas' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Exibir Faturas</span></button></div>";
		echo "<div id='tabela_visualizar_faturar'></div>";
	}

	///atualiza pre�o
	private function atualiza_preco() {
		echo "<center><h1>Buscar Item</h1></center>";
		echo "<div id='brasindice_busca'>";
		echo "<p><b>Buscar: </b>
 	<select style='background-color:transparent;' id='tipo_brasindice'>
 	<option value='4'>Todos</option>
 	<option value='0'>Medicamento</option>
 	<option value='1'>Material</option>
 	<option value='3'>Dieta</option>
 	</select></p>";
		echo "<p>
 	<span id='busca_brasindice_span'>
 	<b>Item:</b>
 	<input type='text' name='item_busca_brasindice' id='item-busca-brasindice' p='pes-item-brasindice'></input>
 	</span></p>";
		echo"<p><input type='hidden' name='UR' id='ur' value='{$_SESSION["empresa_principal"]}'/></p>";
		echo"<p><input type='hidden' name='cod' id='cod' value=''/></p>";
		echo"<p><input type='hidden' name='catalogo_id' id='catalogo_id' value=''/></p>";
		echo "<p><button type='submit' id='pesquisa-brasindice' name='pesquisa-brasindice' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Pesquisar</span></button>";
		echo "<p><div id='brasindice_resultado'></div></p>";
		echo "</div>";
		echo "<div id='tabela'>";
		echo "</div>";
	}

	private function fatura_condensada(){
		echo "<center><h1>Exportar Faturas Condensadas</h1></center>";
		echo "<div id='div_fatura_condensada'>
	        <span ><label><p>Unidade</p></label><p>{$this->unidades()}</p></span>
	        <fieldset style='width:550px;'>
             <legend><b>Per&iacute;odo da Fatura</b></legend>
             <p><b>Inicio:</b><input type='text' name='inicio_fatura_condensada' value='' maxlength='10' size='15' class='OBG data periodo' id='inicio_fatura_condensada'/>
             <b>Fim:</b> <input type='text' name='fim_fatura_condensada' value='' maxlength='10' size='15' class='OBG data' id='fim_fatura_condensada'/></p>
           </fieldset>
		 </div>";
		echo "<p><button type='submit' id='exportar_fatura_condensada'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Exportar</span></button>";



	}
	private function editar_orcamento_avulso($id){
		echo "<div id='editar-orcamento-avulsa'>
                       <center><h1>Editar Orçamento Avulso</h1></center>";
		$conteudo=cabecalho_orcamento_avulso($id);
		echo $conteudo['html'];
		$html .="<br><br>";
		$sql= "SELECT
                    oa.OBSERVACAO,
		o.*,
		IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),B.DESC_MATERIAIS_FATURAR) AS principio,
                 (CASE o.TIPO
		WHEN '0' THEN 'Medicamento'
		WHEN '1' THEN 'Material' WHEN '3' THEN 'Dieta' END) as apresentacao,
		o.NUMERO_TISS as cod,
                (       CASE o.TIPO
                        WHEN '0' THEN '3'
                        WHEN '1' THEN '4'
                        WHEN '3' THEN '5'
		END) as ordem,

                         oa.PLANO_ID,
                         oa.UR,
                    B.lab_desc as lab
		FROM
		orcamento_avulso_item as o  INNER JOIN
		`catalogo` B ON (o.CATALOGO_ID = B.ID) left join
                orcamento_avulso as oa  on (o.ORCAMENTO_AVU_ID = oa.ID)left join
                 unidades_fatura as uf on (o.UNIDADE=uf.ID)
		WHERE
		 o.ORCAMENTO_AVU_ID= {$id} and
		 o.TIPO IN (0,1,3) and
                 o.CANCELADO_POR is NULL

			UNION
			SELECT
                        oa.OBSERVACAO,
                        o.*,
			 vc.DESC_COBRANCA_PLANO AS principio,
			('Servi&ccedil;os') AS apresentacao,
			 vc.COD_COBRANCA_PLANO as cod,
                         1 as ordem,

                         oa.PLANO_ID,
                         oa.UR,
                    '' as lab

			FROM
                          orcamento_avulso_item as o  INNER JOIN
                          valorescobranca as vc on  (o.CATALOGO_ID =vc.id) inner join
                          orcamento_avulso as oa  on (o.ORCAMENTO_AVU_ID = oa.ID)left join
                          unidades_fatura as uf on (o.UNIDADE=uf.ID)
                         WHERE

                          o.ORCAMENTO_AVU_ID= {$id} and
                          o.TIPO =2 and
                          o.TABELA_ORIGEM='valorescobranca' and
                          o.CANCELADO_POR is NULL

                        UNION
			SELECT
                        oa.OBSERVACAO,
                        o.*,
			 vc.item AS principio,
			('Servi&ccedil;os') AS apresentacao,
			 vc.id as cod,
                         1 as ordem,

                         oa.PLANO_ID,
                         oa.UR,
                    '' as lab

			FROM
                          orcamento_avulso_item as o  INNER JOIN
                          cobrancaplanos as vc on  (o.CATALOGO_ID =vc.id) inner join
                          orcamento_avulso as oa  on (o.ORCAMENTO_AVU_ID = oa.ID)left join
                          unidades_fatura as uf on (o.UNIDADE=uf.ID)
                         WHERE

                          o.ORCAMENTO_AVU_ID= {$id} and
                          o.TIPO =2 and
                          o.TABELA_ORIGEM='cobrancaplano' and
                          o.CANCELADO_POR is NULL

                        UNION
			SELECT
                        oa.OBSERVACAO,
                        o.*,
			 vc.DESC_COBRANCA_PLANO AS principio,
			('Equipamentos') AS apresentacao,
			 vc.COD_COBRANCA_PLANO as cod,
                         2 as ordem,

                         oa.PLANO_ID,
                         oa.UR,
                    '' as lab

			FROM
                          orcamento_avulso_item as o  INNER JOIN
                          valorescobranca as vc on  (o.CATALOGO_ID =vc.id) inner join
                          orcamento_avulso as oa  on (o.ORCAMENTO_AVU_ID = oa.ID)left join
                          unidades_fatura as uf on (o.UNIDADE=uf.ID)
                         WHERE

                          o.ORCAMENTO_AVU_ID= {$id} and
                          o.TIPO =5 and
                          o.TABELA_ORIGEM='valorescobranca' and
                          o.CANCELADO_POR is NULL

                        UNION
			SELECT
                        oa.OBSERVACAO,
                        o.*,
			 vc.item AS principio,
			('Equipamentos') AS apresentacao,
			 vc.id as cod,
                         2 as ordem,

                         oa.PLANO_ID,
                         oa.UR,
                    '' as lab

			FROM
                          orcamento_avulso_item as o  INNER JOIN
                          cobrancaplanos as vc on  (o.CATALOGO_ID =vc.id) inner join
                          orcamento_avulso as oa  on (o.ORCAMENTO_AVU_ID = oa.ID)left join
                          unidades_fatura as uf on (o.UNIDADE=uf.ID)
                         WHERE

                          o.ORCAMENTO_AVU_ID= {$id} and
                          o.TIPO =5 and
                          o.TABELA_ORIGEM='cobrancaplano' and
                          o.CANCELADO_POR is NULL

                         UNION
			SELECT
                        oa.OBSERVACAO,
                        o.*,
			 vc.DESC_COBRANCA_PLANO AS principio,
			('Gasoterapia') AS apresentacao,
			 vc.COD_COBRANCA_PLANO as cod,
                         6 as ordem,

                         oa.PLANO_ID,
                         oa.UR,
                    '' as lab

			FROM
                          orcamento_avulso_item as o  INNER JOIN
                          valorescobranca as vc on  (o.CATALOGO_ID =vc.id) inner join
                          orcamento_avulso as oa  on (o.ORCAMENTO_AVU_ID = oa.ID)left join
                          unidades_fatura as uf on (o.UNIDADE=uf.ID)
                         WHERE

                          o.ORCAMENTO_AVU_ID= {$id} and
                          o.TIPO =6 and
                          o.TABELA_ORIGEM='valorescobranca' and
                          o.CANCELADO_POR is NULL

                        UNION
			SELECT
                        oa.OBSERVACAO,
                        o.*,
			 vc.item AS principio,
			('Gasoterapia') AS apresentacao,
			 vc.id as cod,
                         6 as ordem,

                         oa.PLANO_ID,
                         oa.UR,
                    '' as lab

			FROM
                          orcamento_avulso_item as o  INNER JOIN
                          cobrancaplanos as vc on  (o.CATALOGO_ID =vc.id) inner join
                          orcamento_avulso as oa  on (o.ORCAMENTO_AVU_ID = oa.ID)left join
                          unidades_fatura as uf on (o.UNIDADE=uf.ID)
                         WHERE

                          o.ORCAMENTO_AVU_ID= {$id} and
                          o.TIPO =6 and
                          o.TABELA_ORIGEM='cobrancaplano' and
                          o.CANCELADO_POR is NULL

                         ORDER BY ordem,tipo,principio";
		$this->formulario();
		$inicio = join("/", array_reverse(explode("-", $conteudo['inicio'])));
		$fim = join("/", array_reverse(explode("-", $conteudo['fim'])));
		$empresa= $conteudo['empresa'];

		echo "<br/>
                    <tr style='width:450px;' >
                       <td>
                          <fieldset style='width:550px;'>
                               <legend><b>Per&iacute;odo do Orçamento</b></legend>
                                DE
                               <input type='text' name='inicio' value='$inicio' maxlength='10' size='15' class='OBG data periodo' {$disable} />
                               AT&Eacute;
                               <input type='text' name='fim' value='$fim' maxlength='10' size='15' class='OBG data' {$disable} />
  	                  </fieldset>
                       </td>
                    </tr>";
		echo "<br/>
                     <br/>
                     <input type='hidden' id='select_planoavu' value='{$conteudo['plano']}'>
                     <input type='hidden' id='select_unidadeav'  value='{$conteudo['empresa']}'>

                   <button id='add-item-faturaav' style='float:left;' inicio='{$conteudo['inicio']}'  fim='{$conteudo['fim']}' empresa='{$conteudo['UR']}' contador=0 class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover ' role='button' aria-disabled='false' >
                           <span class='ui-button-text'>+</span>
                    </button>";
		echo "<table  width=100% class='mytable' id='tabela_fatavulsa'>
                            <thead>
                               <tr bgcolor='#cccccc'>
                                    <th >Item</th>
                                    <th>Apresentação</th>
                                    <th >Unidade</th>
                                    <th >Qtd</th>
                                    <th >Pre&ccedil;o Unt.</th>
                                    <th >Pre&ccedil;o Tot.</th>
		     	       </thead>
                             </tr>";
		$valor_tota=0;
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result))
		{
			$teste = $row['VAL_PRODUTO']*$row['QTD'];
			$valor_tota += $row['VAL_PRODUTO']*$row['QTD'];

			$botoes = '<img align=\'right\' class=\'rem-item-orcamento-avulso\' editar=1  src=\'../utils/delete_16x16.png\' title=\'Remover\' border=\'0\'>';
			$laboratorio ='';
			if($row['TIPO']== 0){
				$laboratorio ="<b>(".$row['lab'].")</b>";
			}
			echo"<tr class='editado'>
                                                           <td>TUSS-{$row['TUSS']}  N-{$row['cod']} - {$row['principio']} {$laboratorio} {$botoes}</td>
                                                           <td>{$row['apresentacao']}</td>
                                                           <td class='unidade_fat' id='{$row['PACIENTE']}'_'+contador+'>".  unidades_fatura($row['UNIDADE'])." </td>
                                                            <td><input class='qtd OBG' size='7' value='{$row['QTD']}' paciente='{$row['PACIENTE']}' /></td>

                                                           <td>
                                                                R$
                                                                <input type=text name=valor_plano size=10 class='valor_plano valor OBG'
                                                                        tiss='{$row['cod']}'
                                                                        qtd='{$row['QTD']}'
                                                                        catalogo_id='{$row['CATALOGO_ID']}'
                                                                        tabela_origem='{$row['TABELA_ORIGEM']}'
                                                                        tipo='{$row['TIPO']}'
                                                                        custo ='{$row['CUSTO']}'
                                                                        ord =''
                                                                        flag= ''
                                                                        editado='0'
                                                                        cod_ref='{$row['ID']}'
                                                                        value='{$row['VAL_PRODUTO']}'
                                                                        tuss='{$row['TUSS']}'>
                                                           </td>
                                                           <td>
                                                                <span style='float:right;' id='val_total'>
                                                                    <b>  R$
                                                                        <span nome='val_linha'>".number_format($row['VAL_PRODUTO']*$row['QTD'],2,',','.')."</span>
                                                                    </b>
                                                                 </span>
                                                           </td>
                                                         </tr>";

			$observacao=$row['OBSERVACAO'];

		}
		echo"</table>
                            <p>
                                <span id='total_fatura' style='float:right;'>
                                <b>TOTAL PACIENTE: R$ ".number_format($valor_tota,2,',','.')."</b>
                                <b>
                                </span>
                            </p>";
		echo "<p>
                  <b>Observação:</b>
                   <textarea cols=120 rows=5 id='observacao_avulsa'>{$observacao}</textarea>
                   </p>";



		echo "<button id='editar-faturaav' style='float:left;' inicio='{$inicio}'  fim='{$fim}' id_orcamento={$id} empresa='{$empresa}'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover ' role='button' aria-disabled='false' >
                      <span class='ui-button-text'>Salvar</span>
                  </button>";

		echo"</div>";
	}

	//! Exibe a lista de faturas salvas, mas não finalizadas jeferson 2013-10-17.
	private function fatura_salvas_editar(){
		$this->salvas_editar(0);
	}
	private function orcamento_salvas_editar(){
		$this->salvas_editar(1);
	}
	private function salvas_editar($fatura) {
		if($fatura == 0){
			$dir = "salvas/fatura_editar/" ;
			$titulo="FATURAS E OR&Ccedil;AMENTOS DE PRORROGAÇÃO/ADITIVOS SALVOS";

		}else{
			$dir = "salvas/orcamento_editar/";
			$titulo= "ORÇAMENTOS INICIAIS SALVOS NA HORA DE EDITAR";
		}
		if (!is_dir($dir))
			return;
		$ext = array('htm');
		$arquivos = get_files_dir($dir, $ext);
		if (is_array($arquivos)) {
			$tam = count($arquivos);
			$cont = 0;

			echo "<br/>";
			echo "<table width='100%' class='mytable'>";
			echo "<thead>";
			echo "<tr>";
			echo "<th colspan='2'><center>{$titulo}</center></th>";
			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			echo "<tr>";
			$quantidadeItensSalvos =count($arquivos);
			$colspan ='';



			foreach ($arquivos as $arquivo) {
				$filename = $dir."/".$arquivo;
				$str = stripslashes(file_get_contents($filename));
				$string = explode('@|#', $str);



				$nome_arquivo = explode(".", $arquivo);
				$id=$nome_arquivo[0];
				$nome= explode("_",$nome_arquivo[0]);
				$paciente = $this->pacienteNome($nome[0]);
				if ($cont % 2 == 0) {

					echo "</tr>";
					echo "<tr>";
				}
				if($quantidadeItensSalvos == ($cont + 1) && $quantidadeItensSalvos % 2 != 0){
					$colspan =2;
				}
				if($fatura == 0){
					if($nome[1]==0){
						$tipo= "FATURA";
					}else if($nome[1]==2){
						$tipo= "ORÇAMENTO PRORROGAÇÃO";
					}else if($nome[1]==3){
						$tipo= htmlentities( "ORÇAMENTO ADITIVO");
					}
					echo "<td colspan='{$colspan}' >
							<center>
								<b><a href='../faturamento/?view=edt_fatura&id={$id}'>" . strtoupper($paciente) . " ($tipo)</a></b>
								 <br>
							  <span >Período: {$string[1]} - {$string[2]}
							  		<br>
							 	{$string[3]} as {$string[4]}
							 	</span>
							 	</center>
						</td>";
				}else{

					echo "<td colspan='{$colspan}'>
							<center>
								<b><a href='../faturamento/?view=edt_orcamento&id={$id}'>" . strtoupper($paciente) . " </a></b>
								 <br>
								  <span >Período: {$string[1]} - {$string[2]}
								  		<br>
									{$string[3]} as {$string[4]}
									</span>
							 	</center>
							</td>";
				}

				$cont++;
			}
			echo "</tr>";
			echo "</tbody>";
			echo "</table>";
		}
	}

	private function menuRelatorios ()
	{

		echo "<p><a href='../faturamento/relatorios/?action=form-relatorio-pacientes-faturar''>Relat&oacute;rio de Pacientes a Faturar</a></p>";
		echo "<p><a href='../auditoria/relatorios/?action=form-relatorio-faturamento-enviado'>Relat&oacute;rio de Faturamento Enviado</a></p>";
		echo "<p><a href='../remocao/?action=relatorio-remocoes-realizadas&from=faturamento'>Relat&oacute;rio de Remoções Realizadas</a></p>";
	}
	///atualiza pre�o fim
	public function view($v) {

		switch ($v) {
			case "menu":
				$this->menu();
				break;
			case "faturar":
				$this->faturar();
				break;
			case "listar":
				$this->visualizar_faturas();
				break;
			case "lote":
				$this->capa_lote();
				break;
			case "buscas":
				$this->buscas();
				break;
			case "gerenciarlote":
				$this->gerarenciar_lote();
				break;
			case "faturaavulsa":
				$this->fatura_avulsa();
				break;

			case "faturaravaliacao":
				$this->faturar_avaliacao();
				break;

			case "informarglosa":
				$this->informar_glosa();
				break;
			case "edt_fatura":
				$this->edt_fatura($_GET['id'],$_GET['av']);
				break;

			case "edt_orcamento":

				$this->edt_orcamento($_GET['id'],$_GET['av'],$_GET['n']);

				break;

			case "informar-glosa":
				$this->add_glosa($_GET['id']);
				break;

			case "atualiza-preco":
				$this->atualiza_preco();
				break;

			case "preco-editar":
				$this->preco_editar($_GET['id']);
				break;
			case "fatura_salva";
				$this->load_salva($_GET['id']);
				break;
			case "orcamento-prorrogacao";
				$this->orcamento_prorrogacao();
				break;
			case "cadastro-custo";
				$c = new Cadastro_custo_plano_ur;
				$c->cadastrar_custo();
				break;
			case "historico-cadastro-custo";
				$c = new Cadastro_custo_plano_ur;
				$c->historico_custo_plano_ur();
				break;

			case "fatura_condensada";
				$this->fatura_condensada();
				break;
			case "editar_orcamento_avulso";
				$this->editar_orcamento_avulso($_GET['id']);
				break;
			case "orcamento-aditivo";
				$orcamentoAditivo = new OrcamentoAditivo;
				$orcamentoAditivo-> pesquisarPrescricoesAditivas();
				break;
			case 'orcar-aditivo';
				$orca= new OrcamentoAditivo();
				$orca->orcaAditivo($_GET['idPresc'],$_GET['tipo']);
				break;
			case 'relatorios';
				$this->menuRelatorios();
				break;


		}
	}

}

##############
#   PORQUE TODOS ESSES MÉTODOS ABAIXO ESTÃO FORA DA CLASSE ?
###
function refaturar($tipo, $cod, $inicio, $fim, $nome, $plano) {
	$sql = "";
	$cont = 0;
	$cont2 = 0;
	switch ($tipo) {

		case 'unidade':
			$sql = "select idClientes as id from  clientes where empresa = '{$cod}' ";

			break;

		case 'plano':
			$sql = "select idClientes  as id from  clientes where  convenio = '{$cod}'";

			break;
	}
	if ($tipo == 'unidade' || $tipo == 'plano') {

		$result = mysql_query($sql);
		$cont = mysql_num_rows($result);
		$i = 1;

		while ($row = mysql_fetch_array($result)) {

			$pac = $row['id'];

			if ($i < $cont) {
				$pac .= ",";
			}
			$paciente .= $pac;


			$i++;
		}
	} else {
		$paciente = $cod;
	}

	$sql2 = "select PACIENTE_ID as id from fatura  where PACIENTE_ID in ({$paciente}) and '{$inicio}'  < DATA_FIM ";
	$result2 = mysql_query($sql2);
	while ($row = mysql_fetch_array($result2)) {

		$cont2++;
	}

	echo $cont2;
}

function list_avulsa($post) {

	if ($post == 't') {
		if ($_SESSION["empresa_principal"] == 1) {
			$cond = '1';
			$cond1 = '';
		} else {
			$cond = " u.empresa = " . $_SESSION["empresa_principal"] . " ";
			$cond1 = "inner join usuarios as u on (o.USUARIO_ID = u.idUsuarios)";
		}
	}
	if ($post != 't' && $post != '') {
		$cond1 = '';
		$cond = "Paciente like '{$post}'";
	}

	$sql = " select o.*,DATE_FORMAT(o.DATA,'%d-%m-%Y %k:%i') as data from orcamento_avulso as o  {$cond1} where {$cond}";
	$result = mysql_query($sql);
	$cont = 1;
	while ($row = mysql_fetch_array($result)) {
		$inicio = join("/", array_reverse(explode("-", $row['DATA_INICIO'])));
		$fim = join("/", array_reverse(explode("-", $row['DATA_FIM'])));

		if ($cont == 1) {
			$t .= "</br>

                   <table id='tb_list' class='mytable' width=100% >
                     <thead>
                       <tr>
                           <th>Paciente</th>
                           <th>Per&iacute;odo</th>
                           <th>Data</th>
                           <th>Opções</th>
                       </tr>
                      </thead>";
		}
		$t .="<tr>
               <td>{$row['PACIENTE']}</td>
               <td>{$inicio} at&eacute; {$fim}</td>
               <td>{$row['data']}</td>
               <td>
                <a href='#'>
                       <img src='../utils/fichas_24x24.png' width='16' width='20' title='Visualizar' orcamento_avulso_id='{$row['ID']}' class='qtd_diaria_orcamento_avulso' />
                   </a>
                   &nbsp
                   <a href='?view=editar_orcamento_avulso&id={$row['ID']}'>
                       <img src='../utils/edit.png' width='16' width='20' title='Editar Orçamento Avulso' />
                   </a>

               </td>
              </tr>";
		$cont++;
	}
	$t .= "</table>";
	print_r($t);
}
$querySwitch = filter_input(INPUT_POST, 'query');
switch ($querySwitch ) {
	case "re-faturar":
		$inicio = join("-", array_reverse(explode("/", $_POST['inicio'])));
		$fim = join("-", array_reverse(explode("/", $_POST['fim'])));
		refaturar($_POST['tipo'], $_POST['cod'], $inicio, $fim, $_POST['nome'], $_POST['plano']);
		break;

	case "informar-glosa":
		add_glosa($_POST['idfatura']);
		break;
	case "list-avulsa":
		list_avulsa($_POST['val']);
		break;
}
