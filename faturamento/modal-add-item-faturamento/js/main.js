$(function($) {
    var iCanUseOn = !!$.fn.on;

    $('#tipo-item').change(function () {
        $('#buscar-item').val('');
        $('#qtd-item').val('');


    });


    $('#buscar-item').typeahead({
        hint: true,
        highlight: true,
        minLength: 3,

}, {

        name: 'itens',
        display: 'value',
        limit: 100,
        source: function(query,teste,response) {
            var root = document.location.hostname;


            var x = $.getJSON('http://'+root+'/faturamento/busca_brasindice_fatura.php', {
                term: $('#buscar-item').val(),
                tipo: $('#tipo-item').val(),
                empresa: $('#ur-id').val(),
                plano : $('#plano-id').val(),

            },response);

            return x;
        }


    }).on('typeahead:select', function(event, data) {
        $('#buscar-item')
            .val(data.value)
            .attr('catalogo-id', data.catalogo_id)
            .attr('item-tipo', data.tipo_aba)
            .attr('item-tuss', data.tuss || '')
            .attr('item-lab', data.lab)
            .attr('item-custo', data.custo)
            .attr('item-valor', data.valor)
            .attr('item-tabela-origem', data.tabela_origem)
            .attr('item-tiss', data.id || '');
        $('#qtd-item').focus();
    });

});