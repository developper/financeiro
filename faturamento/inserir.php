<?php
$id = session_id();
if(empty($id))
    session_start();
include_once('../validar.php');
include_once('../utils/codigos.php');
require('../db/config.php');

use App\Core\Config;
use App\Services\Gateways\SendGridGateway;
use App\Services\MailAdapter;
use App\Controllers\Fatura;

function gravar_fatura($post){
    extract($post,EXTR_OVERWRITE);
    mysql_query("begin");
    $c = 0;
    $itens = explode("$",$dados);
    $usuario = $_SESSION['id_user'];
    $nomeUsuario = $_SESSION['nome_user'];
    $inicioPtBr = $fat_inicio;
    $fimPtBr = $fat_fim;
    $fat_inicio       = anti_injection(implode("-",array_reverse(explode("/",$fat_inicio))),'literal');
    $fat_fim          = anti_injection(implode("-",array_reverse(explode("/",$fat_fim))),'literal');
    $fat_plano = anti_injection($fat_plano,'numerico');
    $obs =anti_injection($obs,'literal');
    $idFaturaSalva = anti_injection($id_fatura,'literal');

    $sql = "SELECT empresa FROM clientes WHERE idClientes = {$fat_paciente}";
    $rs = mysql_query($sql);
    $row = mysql_fetch_array($rs);
    $empresa_paciente = $row['empresa'];
    $arayPaciente = \App\Models\Administracao\Paciente::getById($fat_paciente);
    $nomePaciente = $arayPaciente['nome'];

//    $sql = "SELECT ID FROM fatura WHERE PACIENTE_ID = '{$fat_paciente}' AND STATUS = '-1' AND ORCAMENTO = '{$av}'";
//    $rs = mysql_query($sql);
//    $existeSalvo = mysql_num_rows($rs);
//
//    $rowFaturaSalva = mysql_fetch_array($rs);
//
//    $idFaturaSalva = $rowFaturaSalva['ID'];

    if( $av == 1 ||$av == 2 || $av == 3){

        $sql ="select OPERADORAS_ID as idop from
		operadoras_planosdesaude as op Right Join
		clientes as c on (c.convenio = op.PLANOSDESAUDE_ID)
		WHERE c.idClientes = $fat_paciente";
        $result=mysql_query($sql);
        $x=mysql_result($result, 0);
        $num= strlen($x);
        if($num == 1){
            $p1='00'.$x.'.'.date(ymd).'.'.date(His);
        }
        if($num == 2){
            $p1='0'.$x.'.'.date(ymd).'.'.date(His);
        }
        if($num == 3){
            $p1=$x.'.'.date(ymd).'.'.date(His);
        }
        $av1=$av;
        if($av1 == 2 || $av1 == 3){
            $status= 2;
        }else{
            $status= 0;
        }
        $condPrescricaoCampo = '';
        $condPrescricaoValue = '';

        if($av == 3){
            $condPrescricaoCampo = " ,prescricao_id, tag_prescricao ";
            $condPrescricaoValue = ",{$idPrescricao}, '{$tabela}'";
        }

        if($idFaturaSalva == 0) {
            $sql = "INSERT INTO fatura (ID,PACIENTE_ID,USUARIO_ID, UR, DATA_INICIO,DATA_FIM,DATA,ORCAMENTO,CAPA_LOTE,CAPMEDICA_ID,OBS,PLANO_ID,STATUS, created_at, created_by $condPrescricaoCampo)
                    VALUES (null,'{$fat_paciente}','{$usuario}', '{$empresa_paciente}','{$fat_inicio}','{$fat_fim}',now(),'{$av}','{$p1}','{$cap}','{$obs}','{$fat_plano}','{$status}', now(), '{$usuario}' $condPrescricaoValue)";
            $r = mysql_query($sql);
            if(!$r){
                echo "Erro ao inserir fatura.".$sql;
                mysql_query("rollback");
                return;
            }
            $id_fatura = mysql_insert_id();
            savelog(mysql_escape_string(addslashes($sql)));
        } else {
            $sql = "Select * from faturamento where FATURA_ID = '{$idFaturaSalva}' ";
            $result =  mysql_query($sql);
            if(!$result){
                echo "-20";
                mysql_query("ROLLBACK");
                return;
            }
            while($row = mysql_fetch_array($result)){
                $sqlHistorico = "INSERT INTO historico_faturamento (SOLICITACOES_ID, NUMERO_TISS, CODIGO_PROPRIO, QUANTIDADE, TIPO,
                            DATA_FATURADO, PACIENTE_ID, USUARIO_ID, VALOR_FATURA, VALOR_CUSTO, FATURA_ID, UNIDADE, CATALOGO_ID,
                             TABELA_ORIGEM, DESCONTO_ACRESCIMO, INCLUSO_PACOTE, TUSS, CODIGO_REFERENCIA,
                             CANCELADO_POR,CANCELADO_EM,custo_atualizado_em,custo_atualizado_por,faturamento_id)
                            VALUES ('{$row['SOLICITACOES_ID']}', '{$row['NUMERO_TISS']}','{$row['CODIGO_PROPRIO']}','{$row['QUANTIDADE']}','{$row['TIPO']}',
                           '{$row['DATA_FATURADO']}','{$row['PACIENTE_ID']}','{$row['USUARIO_ID']}','{$row['VALOR_FATURA']}','{$row['VALOR_CUSTO']}','{$row['FATURA_ID']}','{$row['UNIDADE']}','{$row['CATALOGO_ID']}',
                            '{$row['TABELA_ORIGEM']}','{$row['DESCONTO_ACRESCIMO']}','{$row['INCLUSO_PACOTE']}','{$row['TUSS']}','{$row['ID']}',
                            '{$usuario}', now(),'{$row['custo_atualizado_em']}','{$row['custo_atualizado_por']}','{$row['ID']}')";
                $rHistorico = mysql_query($sqlHistorico);
                if(!$rHistorico){
                    echo "-30".$sqlHistorico;
                    mysql_query("ROLLBACK");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }
            $sql = "UPDATE fatura SET DATA_INICIO = '{$fat_inicio}', DATA_FIM = '{$fat_fim}', STATUS = '2', OBS = '{$obs}' WHERE ID = '{$idFaturaSalva}'";
            $r = mysql_query($sql);
            if(!$r){
                echo "Erro ao atualizar fatura.";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));

            $sql = "DELETE FROM faturamento WHERE FATURA_ID = '{$idFaturaSalva}'";
            $r = mysql_query($sql);
            if(!$r){
                echo "Erro ao remover itens da  fatura.";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
            $id_fatura = $idFaturaSalva;
        }



        foreach($itens as $item){
            $c++;
            if(!empty($item)){

                $i = explode("#",str_replace("undefined","",$item));

                $tiss         = anti_injection($i[0],'literal');
                $qtd          = anti_injection($i[1],'numerico');
                $paciente     = $fat_paciente;
                $sol          = anti_injection($i[3],'literal');
                $tipo         = anti_injection($i[4],'numerico');

                $valor_fatura = anti_injection($i[5],'numerico');
                $valor_custo  = anti_injection($i[7],'numerico');
                $inicio       = anti_injection(implode("-",array_reverse(explode("/",$i[9]))),'literal');
                $fim          = anti_injection(implode("-",array_reverse(explode("/",$i[10]))),'literal');
                $unidade         = anti_injection($i[11],'literal');
                $catalogo_id = anti_injection($i[12],'numerico');
                $tabela_origem= anti_injection($i[13], 'literal');
                $taxa          = anti_injection($i[14], 'numerico');
                $incluso_pacote=anti_injection($i[15], 'numerico');
                $tuss =anti_injection($i[16], 'literal');
                $codigo_proprio = anti_injection($i[17], 'literal');
                $codigo_proprio = $codigo_proprio == '' || $codigo_proprio == null ? 0 : $codigo_proprio;

                // Pegar custo real dos serviços e equuipamentos 21-03-2017 .

                if($tipo != 0 && $tipo != 1 && $tipo != 3){
                    $custoReal = Fatura::custoServicoEquipamentoByURAndPeriodo($catalogo_id, $empresa_paciente, $tabela_origem, $inicio);
                    if(!empty($custoReal)){
                        $valor_custo = $custoReal;

                    }

                }
                // Pegar custo real de material, dieta e medicamento 23-03-2017 .

                if($tipo == 0 || $tipo == 1 || $tipo == 3){
                    $custoReal = Fatura::custoCatalogoByPeriodo($catalogo_id, $inicio);
                    if(!empty($custoReal)){
                        $valor_custo = $custoReal;
                    }
                }

                $sql = "INSERT INTO faturamento (ID,SOLICITACOES_ID,NUMERO_TISS,CODIGO_PROPRIO,QUANTIDADE,TIPO,DATA_FATURADO,PACIENTE_ID,USUARIO_ID,VALOR_FATURA,VALOR_CUSTO,FATURA_ID,UNIDADE,CATALOGO_ID,TABELA_ORIGEM,DESCONTO_ACRESCIMO,INCLUSO_PACOTE,TUSS)
                                    VALUES (null,'{$sol}','{$tiss}','{$codigo_proprio}','{$qtd}','{$tipo}',now(),'{$paciente}','{$usuario}','{$valor_fatura}','{$valor_custo}','{$id_fatura}','{$unidade}','{$catalogo_id}','{$tabela_origem}','{$taxa}','{$incluso_pacote}','{$tuss}')";

                $r = mysql_query($sql);
                if(!$r){
                    echo "-161".$sql ;
                    mysql_query("rollback");
                    return;
                }

                savelog(mysql_escape_string(addslashes($sql)));
                ////Atualizar preço de venda
//                if(($tipo == 0 || $tipo== 1 || $tipo== 3) && $valor_fatura >0 ){
//                    $sql = "UPDATE catalogo set VALOR_VENDA = '{$valor_fatura}' where ID = '{$catalogo_id}' ";
//                    $r = mysql_query($sql);
//                    if(!$r){
//                        echo "-162";
//                        mysql_query("rollback");
//                        return;
//                    }
//                    savelog(mysql_escape_string(addslashes($sql)));
//                }
            }
        }
    }else{
        $fat_inicio       = anti_injection(implode("-",array_reverse(explode("/",$fat_inicio))),'literal');
        $fat_fim          = anti_injection(implode("-",array_reverse(explode("/",$fat_fim))),'literal');

        if($idFaturaSalva == 0) {
             $sql = "INSERT INTO fatura (ID,PACIENTE_ID,USUARIO_ID,UR,DATA_INICIO,DATA_FIM,DATA,STATUS,OBS,PLANO_ID, created_at, created_by, remocao)
 						VALUES (null,'{$fat_paciente}','{$usuario}', '{$empresa_paciente}','{$fat_inicio}','{$fat_fim}',now(),'6','{$obs}','{$fat_plano}', now(), '{$usuario}', '{$remocao}')";
            $r = mysql_query($sql);
            if(!$r){
                echo "Erro ao inserir fatura.";
                mysql_query("rollback");
                return;
            }
            $id_fatura = mysql_insert_id();
            savelog(mysql_escape_string(addslashes($sql)));
        } else {
            //monta historico do itens anteriores na fatura
            $sql = "Select * from faturamento where FATURA_ID = '{$idFaturaSalva}' ";
            $result =  mysql_query($sql);
            if(!$result){
                echo "-20";
                mysql_query("ROLLBACK");
                return;
            }
            while($row = mysql_fetch_array($result)){
                $sqlHistorico = "INSERT INTO historico_faturamento (SOLICITACOES_ID, NUMERO_TISS, CODIGO_PROPRIO, QUANTIDADE, TIPO,
                            DATA_FATURADO, PACIENTE_ID, USUARIO_ID, VALOR_FATURA, VALOR_CUSTO, FATURA_ID, UNIDADE, CATALOGO_ID,
                             TABELA_ORIGEM, DESCONTO_ACRESCIMO, INCLUSO_PACOTE, TUSS, CODIGO_REFERENCIA,
                             CANCELADO_POR,CANCELADO_EM,custo_atualizado_em,custo_atualizado_por,faturamento_id)
                            VALUES ('{$row['SOLICITACOES_ID']}', '{$row['NUMERO_TISS']}','{$row['CODIGO_PROPRIO']}','{$row['QUANTIDADE']}','{$row['TIPO']}',
                           '{$row['DATA_FATURADO']}','{$row['PACIENTE_ID']}','{$row['USUARIO_ID']}','{$row['VALOR_FATURA']}','{$row['VALOR_CUSTO']}','{$row['FATURA_ID']}','{$row['UNIDADE']}','{$row['CATALOGO_ID']}',
                            '{$row['TABELA_ORIGEM']}','{$row['DESCONTO_ACRESCIMO']}','{$row['INCLUSO_PACOTE']}','{$row['TUSS']}','{$row['ID']}',
                            '{$usuario}', now(),'{$row['custo_atualizado_em']}','{$row['custo_atualizado_por']}','{$row['ID']}')";
                $rHistorico = mysql_query($sqlHistorico);
                if(!$rHistorico){
                    echo "-30";
                    mysql_query("ROLLBACK");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }

            $sql = "UPDATE fatura SET DATA_INICIO = '{$fat_inicio}', STATUS = 6, DATA_FIM = '{$fat_fim}', remocao = '{$remocao}', OBS = '{$obs}' WHERE ID = '{$idFaturaSalva}'";
            $r = mysql_query($sql);
            if(!$r){
                echo "Erro ao atualizar fatura.";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));

            $sql = "DELETE FROM faturamento WHERE FATURA_ID = '{$idFaturaSalva}'";
            $r = mysql_query($sql);
            if(!$r){
                echo "Erro ao deletar itens da fatura.";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
            $id_fatura = $idFaturaSalva;
        }



        foreach($itens as $item){
            $c++;
            if(!empty($item)){

                $i = explode("#",str_replace("undefined","",$item));

                $tiss         = anti_injection($i[0],'literal');
                $qtd          = anti_injection($i[1],'numerico');
                $paciente     = $fat_paciente;
                $sol          = anti_injection($i[3],'literal');
                $tipo         = anti_injection($i[4],'numerico');
                $valor_fatura = anti_injection($i[5],'numerico');
                ///$usuario      = anti_injection($i[6],'numerico');
                $valor_custo  = anti_injection($i[7],'numerico');
                $inicio       = anti_injection(implode("-",array_reverse(explode("/",$i[9]))),'literal');
                $fim          = anti_injection(implode("-",array_reverse(explode("/",$i[10]))),'literal');
                $unidade         = anti_injection($i[11],'literal');
                $catalogo_id  = anti_injection($i[12],'numerico');
                $tabela_origem = anti_injection($i[13], 'literal');
                $taxa          = anti_injection($i[14], 'numerico');
                $incluso_pacote=anti_injection($i[15], 'numerico');
                $tuss =anti_injection($i[16], 'literal');
                $codigo_proprio = anti_injection($i[17], 'literal');
                $codigo_proprio = $codigo_proprio == '' || $codigo_proprio == null ? 0 : $codigo_proprio;

                ////Atualizar preço de venda
//                if(($tipo == 0 || $tipo== 1 || $tipo== 3) && $valor_fatura >0 ){
//                    $sql = "UPDATE catalogo set VALOR_VENDA = '{$valor_fatura}' where ID = '{$catalogo_id}' ";
//                    $r = mysql_query($sql);
//                    if(!$r){
//                        echo "-16";
//                        mysql_query("rollback");
//                        return;
//                    }
//                    savelog(mysql_escape_string(addslashes($sql)));
//                }

                if(($tipo == 0 || $tipo == 1 || $tipo== 3) && $av == 0){

                    $sql = "UPDATE  `saida` SET  `FATURADO` =  'S' WHERE  `idCliente` = {$paciente} and `CATALOGO_ID` = {$catalogo_id} and DATE_FORMAT(`data`,'%Y-%m-%d')  BETWEEN '$inicio' AND '$fim'";
                    $r = mysql_query($sql);

                    if(!$r){
                        echo "-11";
                        mysql_query("rollback");
                        return;
                    }
                }

                if($tipo == 2){
                    $sql = "UPDATE itens_prescricao SET `FATURADO` = 'S' WHERE `idPrescricao` in (Select Presc.id from prescricoes as Presc where Presc.paciente='{$paciente}' and ((Presc.inicio Between '{$inicio}' and '{$fim}' and Presc.fim Between '{$inicio}' and '{$fim}') or (Presc.inicio Between '{$inicio}' and '{$fim}' and Presc.fim >'{$fim}') or (Presc.fim Between '{$inicio}' and '{$fim}' and Presc.inicio<'{$inicio}')))
                    and `CATALOGO_ID` = '{$catalogo_id}' and `tipo` = '{$tipo}'";
                    $r = mysql_query($sql);
                    if(!$r){
                        echo "-12".$sql;
                        mysql_query("rollback");
                        return;
                    }
                }

                if( $tipo == 5 && $sol == 1){

                    $sql ="Select * from equipamentosativos as E where  COBRANCA_PLANOS_ID='{$catalogo_id}' and PACIENTE_ID = '{$paciente}' AND ( ( E.DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}' AND E.`DATA_FIM`='0000-00-00 00:00:00' ) OR  (E.DATA_INICIO < '{$inicio}' and E.`DATA_FIM`='0000-00-00 00:00:00' ))";

                    $r = mysql_query($sql);

                    $rid = mysql_result($r,0,0);
                    $rcob_plano = mysql_result($r,0,1);
                    $rcap_id = mysql_result($r,0,2);
                    $rqtd = mysql_result($r,0,3);
                    $robs = mysql_result($r,0,4);
                    $rfim = mysql_result($r,0,6);

                    if(!$r){
                        echo "-13";
                        mysql_query("rollback");
                        return;
                    }
                    if($rfim == '0000-00-00 00:00:00'){

                        $sql = "UPDATE  `equipamentosativos` SET  `FATURADO` =  'S', DATA_FIM ='{$fim}' WHERE ID ='{$rid}' ";
                        $r = mysql_query($sql);
                        if(!$r){
                            echo "-14".$sql;
                            mysql_query("rollback");
                            return;
                        }

                        $r = mysql_query("INSERT INTO equipamentosativos (ID,COBRANCA_PLANOS_ID,CAPMEDICA_ID,QTD,OBS,DATA_INICIO,DATA_FIM,USUARIO,DATA,PACIENTE_ID) VALUES (null,'{$rcob_plano}','{$rcap_id}','{$rqtd}','{$robs}','{$fim}','0000-00-00 00:00:00','$usuario',now(),'$paciente')");


                        if(!$r){
                            echo "-15";
                            mysql_query("rollback");
                            return;
                        }
                    }else{

                        $sql = "UPDATE  `equipamentosativos` SET  `FATURADO` =  'S' WHERE ID ='{$rid}' ";
                        $r = mysql_query($sql);
                        if(!$r){
                            echo "-14".$sql;
                            mysql_query("rollback");
                            return;
                        }
                    }
                }

                // Pegar custo real dos serviços e equuipamentos 21-03-2017 .
                if($tipo != 0 && $tipo != 1 && $tipo != 3){
                    $custoReal = Fatura::custoServicoEquipamentoByURAndPeriodo($catalogo_id, $empresa_paciente, $tabela_origem, $inicio);
                    if(!empty($custoReal)){
                        $valor_custo = $custoReal;
                    }
                }

                // Pegar custo real de material, dieta e medicamento 23-03-2017 .
                if($tipo == 0 || $tipo == 1 || $tipo == 3){
                    $custoReal = Fatura::custoCatalogoByPeriodo($catalogo_id, $inicio);
                    if(!empty($custoReal)){
                        $valor_custo = $custoReal;
                    }
                }

                $sql = "INSERT INTO faturamento (ID,SOLICITACOES_ID,NUMERO_TISS,CODIGO_PROPRIO,QUANTIDADE,TIPO,DATA_FATURADO,PACIENTE_ID,USUARIO_ID,VALOR_FATURA,VALOR_CUSTO,FATURA_ID,UNIDADE,CATALOGO_ID,TABELA_ORIGEM,DESCONTO_ACRESCIMO,INCLUSO_PACOTE,TUSS)
                    VALUES (null,'{$sol}','{$tiss}','{$codigo_proprio}','{$qtd}','{$tipo}',now(),'{$paciente}','{$usuario}','{$valor_fatura}','{$valor_custo}','{$id_fatura}','{$unidade}','{$catalogo_id}','{$tabela_origem}','{$taxa}','{$incluso_pacote}','{$tuss}')";
                $r = mysql_query($sql);
                if(!$r){
                    echo "-167".$sql;
                    mysql_query("rollback");
                    return;
                }

                savelog(mysql_escape_string(addslashes($sql)));
            }
        }
    }

    mysql_query("commit");
    if($av == 1){
        $titulo = 'Avisos do Sistema - Novo Orçamento Inicial para lançamento de preço.';
        $para[] = ['email' =>'saad@mederi.com.br', 'nome' => 'Faturamento'];
        $texto = "<p>Encontra-se no sistema um novo orçamento inicial para lançamento de preço.</p>
					<p>Orçamento Inicial #{$id_fatura} do paciente {$nomePaciente}, período {$inicioPtBr} - {$fimPtBr}
 						feito por {$nomeUsuario}.</p>";

        $response = enviarEmailSimples($titulo, $texto, $para);

        if($response != 200){
            dispararEmails($response);
        }
    }

    if($av==0){
        $titulo = 'Avisos do Sistema - Nova Fatura para lançamento de preço.';
        $para[] = ['email' =>'faturamento@mederi.com.br', 'nome' => 'Faturamento'];
        $texto = "<p>Encontra-se no sistema uma nova Fatura para lançamento de preço.</p>
					<p>Fatura #{$id_fatura} do paciente {$nomePaciente}, período {$inicioPtBr} - {$fimPtBr}
 						feita por {$nomeUsuario}.</p>";

        enviarEmailSimples($titulo, $texto, $para);

        $arquivo = "salvas/fatura/". $paciente . ".htm";
    }else if($av == 2){
        $arquivo = "salvas/orcamento-prorrogacao/". $paciente . ".htm";
    }else{
        $arquivo = "salvas/orcamento/". $paciente . ".htm";
    }
    if (file_exists($arquivo))
        unlink($arquivo);

    echo '1';

}

function gravar_avulsa($post){
    extract($post,EXTR_OVERWRITE);
    mysql_query("begin");
    $inicio       = anti_injection(implode("-",array_reverse(explode("/",$inicio))),'literal');
    $fim          = anti_injection(implode("-",array_reverse(explode("/",$fim))),'literal');
    $empresa =anti_injection($empresa,'numerico');
    $plano =anti_injection($plano,'numerico');
    $usuario = $_SESSION['id_user'];
    $r = mysql_query("INSERT INTO orcamento_avulso (ID,PACIENTE,CPF,ENDERECO,USUARIO_ID,DATA_INICIO,DATA_FIM,DATA,PLANO_ID,UR,OBSERVACAO) VALUES (null,'{$paciente}','{$cpf}','{$endereco}','{$usuario}','{$inicio}','{$fim}',now(),'{$plano}','{$empresa}','{$observacao}')");

    if(!$r){
        echo "-155";

        mysql_query("rollback");
        return;
    }

    $id_fatura = mysql_insert_id();
    $itens = explode("$",$dados);
    foreach($itens as $item){

        if(!empty($item)){

            $i = explode("#",str_replace("undefined","",$item));
            $tiss= anti_injection($i[0],'literal');
            $qtd          = anti_injection($i[1],'numerico');
            $tipo         = anti_injection($i[2],'numerico');
            $valor_fatura = anti_injection($i[3],'numerico');
            $valor_custo  = anti_injection($i[4],'numerico');
            $unidade      = anti_injection($i[5],'literal');
            $catalogo_id   = anti_injection($i[6],'numerico');
            $tabela_origem = anti_injection($i[7],'literal');
            $tuss          = anti_injection($i[8],'literal');
            // Pegar custo real dos serviços e equuipamentos 21-03-2017 .


            if($tipo != 0 && $tipo != 1 && $tipo != 3){
                $custoReal = Fatura::custoServicoEquipamentoByURAndPeriodo($catalogo_id, $empresa, $tabela_origem, $inicio);
                if(!empty($custoReal)){
                    $valor_custo = $custoReal;

                }

            }

            // Pegar custo real de material, dieta e medicamento 23-03-2017 .

            if($tipo == 0 || $tipo == 1 || $tipo == 3){
                $custoReal = Fatura::custoCatalogoByPeriodo($catalogo_id, $inicio);
                if(!empty($custoReal)){
                    $valor_custo = $custoReal;

                }

            }

            $sql = "INSERT INTO orcamento_avulso_item (NUMERO_TISS,QTD,TIPO,VAL_PRODUTO,CUSTO,ORCAMENTO_AVU_ID,UNIDADE,CATALOGO_ID,TABELA_ORIGEM,TUSS) VALUES ('{$tiss}','{$qtd}','{$tipo}','{$valor_fatura}','{$valor_custo}','{$id_fatura}','{$unidade}','{$catalogo_id}','{$tabela_origem}','{$tuss}')";
            $r = mysql_query($sql);
            if(!$r){
                echo mysql_error();
                mysql_query("rollback");
                return;
            }

        }
    }
    mysql_query("commit");
    echo '1';
}


function gerar_lote($post){
    extract($post,EXTR_OVERWRITE);
    mysql_query("begin");


    switch($tipo){
        case "paciente":
            $sql ="select OPERADORAS_ID as idop from 
	   	               operadoras_planosdesaude as op Right Join
	   	               clientes as c on (c.convenio = op.PLANOSDESAUDE_ID) 
	   	         WHERE c.idClientes = {$cod}";

            $result=mysql_query($sql);
            $x=mysql_result($result, 0);

            $num= strlen($x);
            if($num == 1){
                $p1='00'.$x.'.'.date(ymd).'.'.date(His);
            }
            if($num == 2){
                $p1='0'.$x.'.'.date(ymd).'.'.date(His);
            }
            if($num == 3){
                $p1=$x.'.'.date(ymd).'.'.date(His);
            }

            if ($x !='' || $x != null){

                $itens = explode("#",$dados);
                foreach($itens as $item){
                    if($item != '' || $item != null ){
                        $sql = "UPDATE  `fatura` SET  CAPA_LOTE = '{$p1}' , DATA_LOTE = now() WHERE ID ='{$item}'";

                        $r = mysql_query($sql);

                        savelog(mysql_escape_string(addslashes($sql)));
                        if(!$r){
                            echo "-14";
                            mysql_query("rollback");
                            return;
                        }

                    }
                }
            }
            break;
        case "operadora":



            $num= strlen($cod);
            if($num == 1){
                $p1='00'.$cod.'.'.date(ymd).'.'.date(His);
            }
            if($num == 2){
                $p1='0'.$cod.'.'.date(ymd).'.'.date(His);
            }
            if($num == 3){
                $p1=$cod.'.'.date(ymd).'.'.date(His);
            }

            $itens = explode("#",$dados);
            foreach($itens as $item){

                if($item != '' || $item != null ){
                    $sql = "UPDATE  `fatura` SET  CAPA_LOTE = '{$p1}', DATA_LOTE = now() WHERE ID ='{$item}'";
                    $r = mysql_query($sql);
                    if(!$r){
                        echo "-14";
                        mysql_query("rollback");
                        return;
                    }
                    savelog(mysql_escape_string(addslashes($sql)));
                }
            }


            break;
    }


    mysql_query('commit');
    echo $p1;


}

function update_remove($post){
    extract($post,EXTR_OVERWRITE);
    mysql('begin');
    $sql = "Select * from faturamento where ID = {$faturamento_id} ";
    $result =  mysql_query($sql);
    if(!$result){
        echo "-20";
        mysql_query("ROLLBACK");
        return;
    }
    while($row = mysql_fetch_array($result)){
        $sqlHistorico = "INSERT INTO historico_faturamento (SOLICITACOES_ID, NUMERO_TISS, CODIGO_PROPRIO, QUANTIDADE, TIPO,
                            DATA_FATURADO, PACIENTE_ID, USUARIO_ID, VALOR_FATURA, VALOR_CUSTO, FATURA_ID, UNIDADE, CATALOGO_ID,
                             TABELA_ORIGEM, DESCONTO_ACRESCIMO, INCLUSO_PACOTE, TUSS, CODIGO_REFERENCIA,
                             CANCELADO_POR,CANCELADO_EM,custo_atualizado_em,custo_atualizado_por,faturamento_id)
                            VALUES ('{$row['SOLICITACOES_ID']}', '{$row['NUMERO_TISS']}','{$row['CODIGO_PROPRIO']}','{$row['QUANTIDADE']}','{$row['TIPO']}',
                           '{$row['DATA_FATURADO']}','{$row['PACIENTE_ID']}','{$row['USUARIO_ID']}','{$row['VALOR_FATURA']}','{$row['VALOR_CUSTO']}','{$row['FATURA_ID']}','{$row['UNIDADE']}','{$row['CATALOGO_ID']}',
                            '{$row['TABELA_ORIGEM']}','{$row['DESCONTO_ACRESCIMO']}','{$row['INCLUSO_PACOTE']}','{$row['TUSS']}','{$faturamento_id}',
                            '{$_SESSION['id_user']}', now(),'{$row['custo_atualizado_em']}','{$row['custo_atualizado_por']}','{$faturamento_id}')";
        $rHistorico = mysql_query($sqlHistorico);
        if(!$rHistorico){
            echo "-30";
            mysql_query("ROLLBACK");
            return;
        }
    }

    $sql = "DELETE from   `faturamento` where ID = {$faturamento_id} ";
    $r = mysql_query($sql);

    if(!$r){
        echo "-11";
        mysql_query("rollback");
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));
    mysql_query("commit");
    echo 1;
}

function update_status_fatura($post){
    extract($post,EXTR_OVERWRITE);
    mysql_query('begin');

    $sql = "UPDATE  `fatura` SET STATUS = '1' ,DATA_ENVIO_PLANO= now() where CAPA_LOTE = '{$lote}' ";
    $r = mysql_query($sql);

    if(!$r){
        echo "-14";
        mysql_query("rollback");
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));
    echo $lote;

    mysql_query('commit');

}

function finalizar_glosa($post){
    extract($post,EXTR_OVERWRITE);
    mysql_query('begin');


    $sql = "UPDATE  `fatura` SET STATUS = '4' where ID = '{$fatura}' ";
    $r = mysql_query($sql);
    if(!$r){
        echo "-14";
        mysql_query("rollback");
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));

    $sql = "select * from fatura where ID ='{$fatura}'";
    $r = mysql_query($sql);
    $row = mysql_fetch_array($r);

    $sql = "SELECT empresa FROM clientes WHERE idClientes = {$row['PACIENTE_ID']}";
    $rs = mysql_query($sql);
    $linha = mysql_fetch_array($rs);
    $empresa_paciente = $linha['empresa'];

    $sql1 = "INSERT INTO fatura (ID,PACIENTE_ID,USUARIO_ID, UR,DATA_INICIO,DATA_FIM,DATA,STATUS,OBS,ANTERIOR_ID) VALUES
		   (null,'{$row['PACIENTE_ID']}','{$_SESSION["id_user"]}', '{$empresa_paciente}','{$row['DATA_INICIO']}','{$row['DATA_FIM']}',now(),'2','Recurso de glosa.','{$fatura}')";
    $r1 = mysql_query($sql1);
    if(!$r1){
        echo $sql1;
        mysql_query("rollback");
        return;
    }
    $id_fatura = mysql_insert_id();
    savelog(mysql_escape_string(addslashes($sql)));

    $id_fatura = mysql_insert_id();
    $sql2 = "select * from faturamento where FATURA_ID ='{$fatura}'";
    $r2 = mysql_query($sql2);

    while($row = mysql_fetch_array($r2)){

        if($row['VALOR_GLOSA'] != '0.00'){

            $qtd = $row['QUANTIDADE'] - ((($row['VALOR_FATURA'] *$row['QUANTIDADE']) -$row['VALOR_GLOSA'])/$row['VALOR_FATURA']) ;

            $sql3 = "INSERT INTO faturamento (ID,SOLICITACOES_ID,NUMERO_TISS,QUANTIDADE,TIPO,DATA_FATURADO,PACIENTE_ID,USUARIO_ID,VALOR_FATURA,VALOR_CUSTO,FATURA_ID,UNIDADE,CATALOGO_ID) VALUES
                (null,'','{$row['NUMERO_TISS']}','{$qtd}','{$row['TIPO']}',now(),'{$row['PACIENTE_ID']}','{$_SESSION["id_user"]}','{$row['VALOR_FATURA']}','{$row['VALOR_CUSTO']}','{$id_fatura}','{$row['UNIDADE']}',{$row['CATALOGO']})";
            $r3 = mysql_query($sql3);
            if(!$r3){
                echo "-16";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    mysql_query('commit');
    echo 1;

}


function enviar_email_fatura($post){
    extract($post,EXTR_OVERWRITE);
    $sql ="SELECT opp.OPERADORAS_ID, op.EMAIL
FROM fatura AS f
INNER JOIN faturamento AS fm ON ( f.ID = fm.FATURA_ID )
INNER JOIN clientes AS c ON ( f.PACIENTE_ID = c.idClientes )
INNER JOIN operadoras_planosdesaude AS opp ON ( c.convenio = opp.PLANOSDESAUDE_ID )
INNER JOIN operadoras AS op ON ( op.ID = opp.OPERADORAS_ID )
WHERE f.CAPA_LOTE = '{$lote}'
ORDER BY f.PACIENTE_ID
LIMIT 1";
    $result = mysql_query($sql);
    $x= mysql_result($result,0,1);


    $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
    $mail->IsSMTP(); // telling the class to use SMTP

    try {
        // $mail->Host       = "mail.yourdomain.com"; // SMTP server
        //	  $mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->SMTPSecure = "TLS";                 // sets the prefix to the servier
        $mail->Host       = "smtp.mederi.com.br";      // sets GMAIL as the SMTP server
        $mail->Port       = 587;                   // set the SMTP port for the GMAIL server
        $mail->Username   = "ti@mederi.com.br";  // GMAIL username
        $mail->Password   = "meder430";            // GMAIL password
        //	  $mail->AddReplyTo('name@yourdomain.com', 'First Last');
        $mail->AddAddress('ti@mederi.com.br', 'fatura');
        $mail->SetFrom('ti@mederi.com.br', 'fatura');
        //	  $mail->AddReplyTo('name@yourdomain.com', 'First Last');
        $mail->Subject = 'AVISO';
        $mail->AltBody = 'Para ver essa mensagem, use um visualizador de email compatível com HTML'; // optional - MsgHTML will create an alternate automatically
        $solicitante = ucwords(strtolower($_SESSION["nome_user"]));
        $mail->MsgHTML("Email operadora :<b>$x<b>");
        //	  $mail->AddAttachment('images/phpmailer.gif');      // attachment
        //	  $mail->AddAttachment('images/phpmailer_mini.gif'); // attachment
        $enviado=$mail->Send();

        if ($enviado) {
            echo "E-mail enviado com sucesso!";
            //window.location.href='?adm=paciente&act=listfichaevolucao&id='{$paciente};
        } else {
            echo "N�o foi poss�vel enviar o e-mail.<br /><br />";
            echo "<b>Informa��es do erro:</b> <br />" . $mail->ErrorInfo;
        }
    }
    catch (phpmailerException $e) {
        echo $e->errorMessage(); //Pretty error messages from PHPMailer
    } catch (Exception $e) {
        echo $e->getMessage(); //Boring error messages from anything else!
    }
}


function salvar_glosa($post){
    extract($post,EXTR_OVERWRITE);
    $itens = explode("$",$glosa);
    mysql_query("begin");
    foreach($itens as $item){
        if($item!="" && $item!=null){
            $i = explode("#",$item);
            $id = anti_injection($i[0],"numerico");
            $valor = anti_injection($i[1],"literal");
            $motivo = anti_injection($i[2],"literal");

            $r = mysql_query("UPDATE faturamento set VALOR_GLOSA = '{$valor}', MOTIVO_GLOSA='{$motivo}' where ID='{$id}'");
            if(!$r){
                echo "ERRO: ";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }


    $usuario = $_SESSION['id_user'];
    $data = anti_injection(implode("-",array_reverse(explode("/",$data))),'literal');

    $sql = "select ID from  glosa where FATURA_ID ='{$idfatura}'";
    $r = mysql_query($sql);
    $id_glosa=mysql_num_rows($r);

    if($id_glosa == 0){

        $r = mysql_query("INSERT INTO glosa values(null,'{$idfatura}','{$usuario}','{$data}',now())");
        if(!$r){
            echo "ERRO1: ";
            mysql_query("rollback");
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));
    }else{

        $r = mysql_query("UPDATE glosa set DATA_PAGAMENTO='{$data}',DATA=now(),CRIADOR_ID='{$usuario}' where FATURA_ID='{$idfatura}'");
        if(!$r){
            echo "ERRO2: ";
            mysql_query("rollback");
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));
    }
    $r = mysql_query("UPDATE fatura set STATUS = '3' where ID='{$idfatura}'");
    if(!$r){
        echo "ERRO:3 ";
        mysql_query("rollback");
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));
    mysql_query("commit");
    echo '1';
}

function editar_fatura($post){

    extract($post,EXTR_OVERWRITE);
    $tipo_fatura = $tipo_fatura;
    $inicioPtBr = $inicio;
    $fimPtBr = $fim;
    $inicio       = anti_injection(implode("-",array_reverse(explode("/",$inicio))),'literal');
    $fim          = anti_injection(implode("-",array_reverse(explode("/",$fim))),'literal');
    $obs =anti_injection($obs,'literal');
    $remocao = anti_injection($remocao,'literal');
    mysql_query("BEGIN");
    $c = 0;
    $itens = explode("$",$dados);
    $usuario = $_SESSION['id_user'];
    $alertFinanc = false;
    $empresa = anti_injection($empresa,'numerico');
    $tag = anti_injection($tag,'literal');

    $nomeUsuario = $_SESSION['nome_user'];



    $sql = "SELECT empresa FROM clientes WHERE idClientes = {$fat_paciente}";
    $rs = mysql_query($sql);
    $row = mysql_fetch_array($rs);
    $arayPaciente = \App\Models\Administracao\Paciente::getById($fat_paciente);
    $nomePaciente = $arayPaciente['nome'];

    if($status == '5') {
        $sql = <<<SQL
SELECT 
	fatura.reabertura_por, 
	fatura.reabertura_em,
	usuarios.nome AS usuario,
	clientes.nome AS paciente,
	fatura.DATA_INICIO,
	fatura.DATA_FIM,
	fatura.CAPA_LOTE
FROM 
	fatura 
	LEFT JOIN usuarios ON fatura.reabertura_por = usuarios.idUsuarios
	LEFT JOIN clientes ON fatura.PACIENTE_ID = clientes.idClientes
WHERE 
	fatura.ID = '{$id_fatura}'
SQL;
        $rs = mysql_query($sql);
        $row = mysql_fetch_array($rs, MYSQL_ASSOC);
        if ($row['reabertura_por'] != '' && $row['reabertura_em'] != '') {
            $alertFinanc = !$alertFinanc;
            $usuarioReab = $row['usuario'];
            $pacienteReab = $row['paciente'];
            $dataReab = \DateTime::createFromFormat('Y-m-d H:i:s', $row['reabertura_em'])->format('d/m/Y \a\s H:i:s');
            $inicioReab = \DateTime::createFromFormat('Y-m-d', $row['DATA_INICIO'])->format('d/m/Y');
            $fimReab = \DateTime::createFromFormat('Y-m-d', $row['DATA_FIM'])->format('d/m/Y');
        }
    }
    $condRemocao = $remocao == -1 ? '' : "remocao = '{$remocao}' ,";
    $condStatus = $status;

    if($status == '-1') {
        if($tag == 'finalizar') {
            $condStatus = '2';
        }
    }

    if($status == -1 && $tipo_fatura == 0 ) {
        if($tag == 'finalizar') {
            $condStatus = '6';
        }
    }




    $sql = "Update fatura set {$condRemocao} USUARIO_ID='{$usuario}',DATA= now() ,DATA_INICIO ='{$inicio}',DATA_FIM='{$fim}',GUIA_TISS ={$guia_tiss},STATUS={$condStatus },OBS='{$obs}' where ID='{$id_fatura}'";

    $r = mysql_query($sql);
    if(!$r){
        echo "-14";
        mysql_query("ROLLBACK");
        return;

    }
    savelog(mysql_escape_string(addslashes($sql)));

    foreach($itens as $item){
        $c++;
        if(!empty($item)){

            $i = explode("#",str_replace("undefined","",$item));

            $codf         = anti_injection($i[0],'numerico');
            $qtd          = anti_injection($i[1],'numerico');
            $paciente     = anti_injection($i[2],'numerico');
            $sol          = anti_injection($i[3],'literal');
            $tipo         = anti_injection($i[4],'numerico');
            $valor_fatura = anti_injection($i[5],'numerico');
            $unidade      = anti_injection($i[11],'literal');
            $valor_custo  = anti_injection($i[12],'numerico');
            $numero_tiss  = anti_injection($i[13],'literal');
            $catalogo_id    = anti_injection($i[14],'numerico');
            $tabela_origem  = anti_injection($i[15],'literal');
            $taxa           = anti_injection($i[16], 'numerico');
            $incluso_pacote = anti_injection($i[17], 'numerico');
            $editado       	= anti_injection($i[18], 'numerico');
            $tuss          	= anti_injection($i[19], 'literal');
            $codigo_proprio = anti_injection($i[20], 'literal');
            $codigo_proprio = $codigo_proprio == '' || $codigo_proprio == null ? 0 : $codigo_proprio;

            if($editado == 1){
                $sql = "Select * from faturamento where ID = $codf ";
                $result =  mysql_query($sql);
                if(!$result){
                    echo "-20";
                    mysql_query("ROLLBACK");
                    return;
                }
                    while($row = mysql_fetch_array($result)){
                        $sqlHistorico = "INSERT INTO historico_faturamento (SOLICITACOES_ID, NUMERO_TISS, CODIGO_PROPRIO, QUANTIDADE, TIPO,
                            DATA_FATURADO, PACIENTE_ID, USUARIO_ID, VALOR_FATURA, VALOR_CUSTO, FATURA_ID, UNIDADE, CATALOGO_ID,
                             TABELA_ORIGEM, DESCONTO_ACRESCIMO, INCLUSO_PACOTE, TUSS, CODIGO_REFERENCIA,
                             CANCELADO_POR,CANCELADO_EM,custo_atualizado_em,custo_atualizado_por,faturamento_id)
                            VALUES ('{$row['SOLICITACOES_ID']}', '{$row['NUMERO_TISS']}','{$row['CODIGO_PROPRIO']}','{$row['QUANTIDADE']}','{$row['TIPO']}',
                           '{$row['DATA_FATURADO']}','{$row['PACIENTE_ID']}','{$row['USUARIO_ID']}','{$row['VALOR_FATURA']}','{$row['VALOR_CUSTO']}','{$row['FATURA_ID']}','{$row['UNIDADE']}','{$row['CATALOGO_ID']}',
                            '{$row['TABELA_ORIGEM']}','{$row['DESCONTO_ACRESCIMO']}','{$row['INCLUSO_PACOTE']}','{$row['TUSS']}','{$codf}',
                            '{$usuario}', now(),'{$row['custo_atualizado_em']}','{$row['custo_atualizado_por']}','{$codf}')";
                        $rHistorico = mysql_query($sqlHistorico);
                        if(!$rHistorico){
                            echo "-30";
                            mysql_query("ROLLBACK");
                            return;
                        }
                        savelog(mysql_escape_string(addslashes($sql)));
                    }
                $sqlUpdate = "update
                        faturamento
                        set SOLICITACOES_ID = '{$sol}',
                        NUMERO_TISS = '{$numero_tiss}',
                        CODIGO_PROPRIO = '{$codigo_proprio}',
                        QUANTIDADE = '{$qtd}',
                        TIPO = '{$tipo}',
                        DATA_FATURADO = now(),
                        PACIENTE_ID = '{$paciente}',
                        USUARIO_ID = '{$usuario}',
                        VALOR_FATURA = '{$valor_fatura}',
                        VALOR_CUSTO = '{$valor_custo}',
                        FATURA_ID = '{$id_fatura}',
                        UNIDADE = '{$unidade}',
                        CATALOGO_ID = '{$catalogo_id}',
                        TABELA_ORIGEM =  '{$tabela_origem}',
                        DESCONTO_ACRESCIMO = '{$taxa}',
                        INCLUSO_PACOTE = '{$incluso_pacote}',
                        TUSS = '{$tuss}',
                        CODIGO_REFERENCIA = '{$codf}'
                        where
                        ID = '{$codf}'";

                $rUpdate = mysql_query($sqlUpdate);
                if(!$rUpdate){
                    echo "-16";
                    mysql_query("ROLLBACK");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));


            }
            if( $codf == 0 ){

                // Pegar custo real dos serviços e equuipamentos 21-03-2017 .
                if($codf == 0) {
                    if ($tipo != 0 && $tipo != 1 && $tipo != 3) {
                        $custoReal = Fatura::custoServicoEquipamentoByURAndPeriodo($catalogo_id, $empresa, $tabela_origem, $inicio);
                        if (!empty($custoReal)) {
                            $valor_custo = $custoReal;

                        }

                    }

                    // Pegar custo real de material, dieta e medicamento 23-03-2017 .

                    if ($tipo == 0 || $tipo == 1 || $tipo == 3) {
                        $custoReal = Fatura::custoCatalogoByPeriodo($catalogo_id, $inicio);
                        if (!empty($custoReal)) {
                            $valor_custo = $custoReal;

                        }

                    }
                }

                $sql = "INSERT INTO faturamento (ID,SOLICITACOES_ID,NUMERO_TISS,CODIGO_PROPRIO,QUANTIDADE,TIPO,DATA_FATURADO,PACIENTE_ID,USUARIO_ID,VALOR_FATURA,VALOR_CUSTO,FATURA_ID,UNIDADE,CATALOGO_ID,TABELA_ORIGEM,DESCONTO_ACRESCIMO,INCLUSO_PACOTE,TUSS,CODIGO_REFERENCIA)
                            VALUES (null,'{$sol}','{$numero_tiss}','{$codigo_proprio}','{$qtd}','{$tipo}',now(),'{$paciente}','{$usuario}','{$valor_fatura}','{$valor_custo}','{$id_fatura}','{$unidade}','{$catalogo_id}','{$tabela_origem}','{$taxa}','{$incluso_pacote}','{$tuss}','{$codf}')";

                $r = mysql_query($sql);
                if(!$r){
                    echo "-16";
                    mysql_query("ROLLBACK");
                    return;
                }


                savelog(mysql_escape_string(addslashes($sql)));
            }

            ////Atualizar preço de venda
//            if(($tipo == 0 || $tipo== 1 || $tipo== 3) && $valor_fatura >0 && $status != '-1'){
//                $sql = "UPDATE catalogo set VALOR_VENDA = '{$valor_fatura}' where ID = '{$catalogo_id}' ";
//                $r = mysql_query($sql);
//                if(!$r){
//                    echo "-15";
//                    mysql_query("rollback");
//                    return;
//                }
//                savelog(mysql_escape_string(addslashes($sql)));
//
//            }
        }
    }

    mysql_query("COMMIT");

    if($status == '-1') {

        $arquivo = "salvas/fatura_editar/" . $paciente . "_" . $tipo_fatura . ".htm";

        if (file_exists($arquivo))
            unlink($arquivo);
    }

    if($status == -1 && $tag == 'finalizar'){
        if($tipo_fatura == 0){
            $titulo = 'Avisos do Sistema - Nova Fatura para lançamento de preço.';
            $para[] = ['email' =>'faturamento@mederi.com.br', 'nome' => 'Faturamento'];
            $texto = "<p>Encontra-se no sistema uma nova Fatura para lançamento de preço.</p>
					<p>Fatura #{$id_fatura} do paciente {$nomePaciente}, período {$inicioPtBr} - {$fimPtBr}
 						feita por {$nomeUsuario}.</p>";

            enviarEmailSimples($titulo, $texto, $para);

            $arquivo = "salvas/fatura/". $paciente . ".htm";
        }


    }

    if($alertFinanc) {
        $titulo = 'Avisos do Sistema - Uma fatura reaberta foi finalizada';
        $para[] = ['email' =>'financeiro02@mederi.com.br', 'nome' => 'Financeiro'];
        $texto = "<p>A fatura <b>#{$id_fatura}</b> reaberta por <b>{$usuarioReab}</b>, paciente <b>{$pacienteReab}</b>, foi finalizada!</p>
				  <p>
				  	Reabertura em: {$dataReab} <br>
				  	Periodo da fatura: {$inicioReab} a {$fimReab} 
				  </p>
				  ";

        enviarEmailSimples($titulo, $texto, $para);
    }

    echo "1";
}


function editar_orcamento($post){
    extract($post,EXTR_OVERWRITE);
    mysql_query("begin");
    $user = $_SESSION["id_user"];
    $inicioPtBr = $inicio;
    $fimPtBr = $fim;
    $inicio       = anti_injection(implode("-",array_reverse(explode("/",$inicio))),'literal');
    $fim          = anti_injection(implode("-",array_reverse(explode("/",$fim))),'literal');
    $empresa          = anti_injection($empresa,'numerico');
    $condStatus = '';
    $nomeUsuario = $_SESSION['nome_user'];
    $fat_paciente = anti_injection($paciente,'numerico');



    $sql = "SELECT empresa FROM clientes WHERE idClientes = {$fat_paciente}";
    $rs = mysql_query($sql);
    $row = mysql_fetch_array($rs);
    $arayPaciente = \App\Models\Administracao\Paciente::getById($fat_paciente);
    $nomePaciente = $arayPaciente['nome'];


    if($status == '-1') {
        if($finalizar == 1) {
            $condStatus = ',STATUS = 0';
        }
    }

    $sql = " Update
                fatura set
                USUARIO_ID='{$user}',
                DATA= now() ,
                DATA_INICIO ='{$inicio}',
                DATA_FIM='{$fim}',
                OBS='{$obs}'
                {$condStatus}
                where ID='{$id_fatura}'";

    $r = mysql_query($sql);
    if(!$r){
        echo "-14";
        mysql_query("ROLLBACK");
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));

    $itens = explode("$",$dados);
    foreach($itens as $item){
        $c++;
        if(!empty($item)){

            $i = explode("#",str_replace("undefined","",$item));
            $codf        = anti_injection($i[0],'literal');
            $tiss         = anti_injection($i[12],'literal');
            $qtd          = anti_injection($i[1],'numerico');
            $paciente     = anti_injection($i[2],'numerico');
            $sol          = anti_injection($i[3],'literal');
            $tipo         = anti_injection($i[4],'numerico');
            $valor_fatura = anti_injection($i[5],'literal');
            $valor_custo  = anti_injection($i[7],'literal');
            $unidade         = anti_injection($i[11],'literal');
            $catalogo_id = anti_injection($i[13],'numerico');
            $tabela_origem=anti_injection($i[14],'literal');
            $taxa          = anti_injection($i[15], 'numerico');
            $incluso_pacote=anti_injection($i[16], 'numerico');
            $tuss = anti_injection($i[17], 'literal');
            $editado = anti_injection($i[18], 'literal');


            if($editado == 1){
                $sql = "Select * from faturamento where ID = $codf ";
                $result =  mysql_query($sql);
                if(!$result){
                    echo "-20";
                    mysql_query("ROLLBACK");
                    return;
                }
                while($row = mysql_fetch_array($result)){
                    $sqlHistorico = "INSERT INTO historico_faturamento (SOLICITACOES_ID, NUMERO_TISS, CODIGO_PROPRIO, QUANTIDADE, TIPO,
                            DATA_FATURADO, PACIENTE_ID, USUARIO_ID, VALOR_FATURA, VALOR_CUSTO, FATURA_ID, UNIDADE, CATALOGO_ID,
                             TABELA_ORIGEM, DESCONTO_ACRESCIMO, INCLUSO_PACOTE, TUSS, CODIGO_REFERENCIA,
                             CANCELADO_POR,CANCELADO_EM,custo_atualizado_em,custo_atualizado_por,faturamento_id)
                            VALUES ('{$row['SOLICITACOES_ID']}', '{$row['NUMERO_TISS']}','{$row['CODIGO_PROPRIO']}','{$row['QUANTIDADE']}','{$row['TIPO']}',
                           '{$row['DATA_FATURADO']}','{$row['PACIENTE_ID']}','{$row['USUARIO_ID']}','{$row['VALOR_FATURA']}','{$row['VALOR_CUSTO']}','{$row['FATURA_ID']}','{$row['UNIDADE']}','{$row['CATALOGO_ID']}',
                            '{$row['TABELA_ORIGEM']}','{$row['DESCONTO_ACRESCIMO']}','{$row['INCLUSO_PACOTE']}','{$row['TUSS']}','{$codf}',
                            '{$user}', now(),'{$row['custo_atualizado_em']}','{$row['custo_atualizado_por']}','{$codf}')";
                    $rHistorico = mysql_query($sqlHistorico);
                    if(!$rHistorico){
                        echo "-30";
                        mysql_query("ROLLBACK");
                        return;
                    }
                    savelog(mysql_escape_string(addslashes($sql)));
                }
                $sqlUpdate = "update
                        faturamento
                        set SOLICITACOES_ID = '{$sol}',
                        NUMERO_TISS = '{$tiss}',
                        QUANTIDADE = '{$qtd}',
                        TIPO = '{$tipo}',
                        DATA_FATURADO = now(),
                        PACIENTE_ID = '{$paciente}',
                        USUARIO_ID = '{$user}',
                        VALOR_FATURA = '{$valor_fatura}',
                        VALOR_CUSTO = '{$valor_custo}',
                        FATURA_ID = '{$id_fatura}',
                        UNIDADE = '{$unidade}',
                        CATALOGO_ID = '{$catalogo_id}',
                        TABELA_ORIGEM =  '{$tabela_origem}',
                        DESCONTO_ACRESCIMO = '{$taxa}',
                        INCLUSO_PACOTE = '{$incluso_pacote}',
                        TUSS = '{$tuss}',
                        CODIGO_REFERENCIA = '{$codf}'
                        where
                        ID = '{$codf}'";

                $rUpdate = mysql_query($sqlUpdate);
                if(!$rUpdate){
                    echo "-16";
                    mysql_query("ROLLBACK");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }
            if( $codf == 0 ){

                if($codf == 0) {
                    // Pegar custo real dos serviços e equuipamentos 21-03-2017 .
                    if ($tipo != 0 && $tipo != 1 && $tipo != 3) {
                        $custoReal = Fatura::custoServicoEquipamentoByURAndPeriodo($catalogo_id, $empresa, $tabela_origem, $inicio);
                        if (!empty($custoReal)) {
                            $valor_custo = $custoReal;

                        }
                    }

                    // Pegar custo real de material, dieta e medicamento 23-03-2017 .

                    if ($tipo == 0 || $tipo == 1 || $tipo == 3) {
                        $custoReal = Fatura::custoCatalogoByPeriodo($catalogo_id, $inicio);
                        if (!empty($custoReal)) {
                            $valor_custo = $custoReal;

                        }

                    }
                }

                $sql = "INSERT INTO
               faturamento
               (ID,
               SOLICITACOES_ID,
               NUMERO_TISS,
               QUANTIDADE,
               TIPO,
               DATA_FATURADO,
               PACIENTE_ID,
               USUARIO_ID,
               VALOR_FATURA,
               VALOR_CUSTO,
               FATURA_ID,
               UNIDADE,
               CATALOGO_ID,
               TABELA_ORIGEM,
               DESCONTO_ACRESCIMO,
               INCLUSO_PACOTE,
               TUSS,
               CODIGO_REFERENCIA)
                  VALUES
                  (null,
                  '{$sol}',
                  '{$tiss}',
                  '{$qtd}',
                  '{$tipo}',
                  now(),
                  '{$paciente}',
                  '{$user}',
                  '{$valor_fatura}',
                  '{$valor_custo}',
                  '{$id_fatura}',
                  '{$unidade}',
                  '{$catalogo_id}',
                  '{$tabela_origem}',
                  '{$taxa}',
                  '{$incluso_pacote}',
                  '{$tuss}',
                  '{$codf}')";


                $r = mysql_query($sql);
                if(!$r){
                    echo "-16";
                    mysql_query("ROLLBACK");
                    return;
                }


                savelog(mysql_escape_string(addslashes($sql)));
            }


            if($status <> -1) {
                ////Atualizar preço de venda
//                if (($tipo == 0 || $tipo == 1 || $tipo == 3) && $valor_fatura > 0) {
//                    $sql = "UPDATE catalogo set VALOR_VENDA = '{$valor_fatura}' where ID = '{$catalogo_id}' ";
//                    $r = mysql_query($sql);
//                    if (!$r) {
//                        echo "-16";
//                        mysql_query("rollback");
//                        return;
//                    }
//                    savelog(mysql_escape_string(addslashes($sql)));
//
//                }
            }
        }
    }
    mysql_query("commit");
//    if($tipo_fatura==1){
//        $arquivo = "salvas/orcamento_editar/" . $paciente ."_".$tipo_fatura.".htm";
//        if (file_exists($arquivo))
//            unlink($arquivo);
//    }
    if($status == '-1' && $finalizar == 1 && $av=1) {


            $titulo = 'Avisos do Sistema - Novo Orçamento Inicial para lançamento de preço.';
            $para[] = ['email' =>'saad@mederi.com.br', 'nome' => 'Faturamento'];
            $texto = "<p>Encontra-se no sistema um novo orçamento inicial para lançamento de preço.</p>
					<p>Orçamento Inicial #{$id_fatura} do paciente {$nomePaciente}, período {$inicioPtBr} - {$fimPtBr}
 						feito por {$nomeUsuario}.</p>";

            $response = enviarEmailSimples($titulo, $texto, $para);

            if($response != 200){
                dispararEmails($response);
            }

    }

    echo '1';


}



function editar_brasindice($valor_item, $numero_tiss, $tipo_brasindice,$catalogo_id) {
    mysql_query("begin");
    $sql = "update catalogo set  VALOR_VENDA = '{$valor_item}' where NUMERO_TISS ='{$numero_tiss}' and tipo = '{$tipo_brasindice}' and CATALOGO_ID={$catalogo_id}";

    $resultado = mysql_query($sql);

    if(!$resultado){
        echo "ERRO: Tente mais tarde!";
        mysql_query("rollback");
        return;
        savelog(mysql_escape_string(addslashes($sql)));
    }
    mysql_query("commit");

    echo $catalogo_id;

}

function inserir_guia_tiss($post){
    extract($post,EXTR_OVERWRITE);
    mysql_query("begin");
    $sql = "UPDATE fatura set GUIA_TISS = '{$guia_tiss}' where CAPA_LOTE='{$lote}'";

    $resultado = mysql_query($sql);

    if(!$resultado){
        echo "ERRO: Tente mais tarde!";
        mysql_query("rollback");
        return;
        savelog(mysql_escape_string(addslashes($sql)));
    }
    mysql_query("commit");

    echo 2;
}

function salvar_fatura($post){
  //  ini_set('display_errors', 1);
    extract($post,EXTR_OVERWRITE);
    mysql_query("begin");
    $c = 0;
    $cap =anti_injection($cap,'literal');
    $idFaturaSalva = anti_injection($id_fatura,'numerico');
    $itens = explode("$",$dados);
    $usuario = $_SESSION['id_user'];
    $fat_inicio = anti_injection(implode("-",array_reverse(explode("/",$fat_inicio))),'literal');
    $fat_fim = anti_injection(implode("-",array_reverse(explode("/",$fat_fim))),'literal');
    $fat_plano = anti_injection($fat_plano,'numerico');
    $obs =anti_injection($obs,'literal');



    $sql = "SELECT empresa FROM clientes WHERE idClientes = {$fat_paciente}";
    $rs = mysql_query($sql);
    $row = mysql_fetch_array($rs);
    $empresa_paciente = $row['empresa'];

//    $sql = "SELECT ID FROM fatura WHERE PACIENTE_ID = '{$fat_paciente}' AND STATUS = '-1' AND ORCAMENTO = '{$av}'";
//    $rs = mysql_query($sql);
//    $existeSalvo = mysql_num_rows($rs);
//
//    $rowFaturaSalva = mysql_fetch_array($rs);

   // $idFaturaSalva = $rowFaturaSalva['ID'];

    if( $av == 1 ||$av == 2 || $av == 3){



        $sql ="select OPERADORAS_ID as idop from
		operadoras_planosdesaude as op Right Join
		clientes as c on (c.convenio = op.PLANOSDESAUDE_ID)
		WHERE c.idClientes = $fat_paciente";
        $result=mysql_query($sql);
        $x=mysql_result($result, 0);
        $num= strlen($x);
        if($num == 1){
            $p1='00'.$x.'.'.date(ymd).'.'.date(His);
        }
        if($num == 2){
            $p1='0'.$x.'.'.date(ymd).'.'.date(His);
        }
        if($num == 3){
            $p1=$x.'.'.date(ymd).'.'.date(His);
        }

        if($idFaturaSalva == 0) {
            $sql = "INSERT INTO fatura (ID,PACIENTE_ID,USUARIO_ID, UR, DATA_INICIO,DATA_FIM,DATA,ORCAMENTO,CAPA_LOTE,CAPMEDICA_ID,OBS,PLANO_ID,STATUS, created_at, created_by)
                    VALUES (null,'{$fat_paciente}','{$usuario}', '{$empresa_paciente}','{$fat_inicio}','{$fat_fim}',now(),'{$av}','{$p1}','{$cap}','{$obs}','{$fat_plano}','-1', now(), '{$usuario}')";
            mysql_query($sql);

            $id_fatura = mysql_insert_id();
        } else {

            // atualiza fatura
            $sql = "UPDATE fatura SET DATA_INICIO='{$fat_inicio}', DATA_FIM = '{$fat_fim}', OBS = '{$obs}' WHERE ID = '{$idFaturaSalva}'";
            $r = mysql_query($sql);
            if(!$r){
                echo "Erro ao atualizar fatura.";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
            //salva historico doss itens da fatura
            $sql = "Select * from faturamento where FATURA_ID = '{$idFaturaSalva}' ";
            $result =  mysql_query($sql);
            if(!$result){
                echo "-20";
                mysql_query("ROLLBACK");
                return;
            }
            while($row = mysql_fetch_array($result)){
                $sqlHistorico = "INSERT INTO historico_faturamento (SOLICITACOES_ID, NUMERO_TISS, CODIGO_PROPRIO, QUANTIDADE, TIPO,
                            DATA_FATURADO, PACIENTE_ID, USUARIO_ID, VALOR_FATURA, VALOR_CUSTO, FATURA_ID, UNIDADE, CATALOGO_ID,
                             TABELA_ORIGEM, DESCONTO_ACRESCIMO, INCLUSO_PACOTE, TUSS, CODIGO_REFERENCIA,
                             CANCELADO_POR,CANCELADO_EM,custo_atualizado_em,custo_atualizado_por,faturamento_id)
                            VALUES ('{$row['SOLICITACOES_ID']}', '{$row['NUMERO_TISS']}','{$row['CODIGO_PROPRIO']}','{$row['QUANTIDADE']}','{$row['TIPO']}',
                           '{$row['DATA_FATURADO']}','{$row['PACIENTE_ID']}','{$row['USUARIO_ID']}','{$row['VALOR_FATURA']}','{$row['VALOR_CUSTO']}','{$row['FATURA_ID']}','{$row['UNIDADE']}','{$row['CATALOGO_ID']}',
                            '{$row['TABELA_ORIGEM']}','{$row['DESCONTO_ACRESCIMO']}','{$row['INCLUSO_PACOTE']}','{$row['TUSS']}','{$row['ID']}',
                            '{$usuario}', now(),'{$row['custo_atualizado_em']}','{$row['custo_atualizado_por']}','{$row['ID']}')";
                $rHistorico = mysql_query($sqlHistorico);
                if(!$rHistorico){
                    echo "-30";
                    mysql_query("ROLLBACK");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }
            $sql = "DELETE FROM faturamento WHERE FATURA_ID = '{$idFaturaSalva}'";
            mysql_query($sql);

            $id_fatura = $idFaturaSalva;
        }

        foreach($itens as $item){
            $c++;
            if(!empty($item)){

                $i = explode("#",str_replace("undefined","",$item));

                $tiss         	= anti_injection($i[0],'literal');
                $qtd          	= anti_injection($i[1],'numerico');
                $paciente     	= $fat_paciente;
                $sol          	= anti_injection($i[3],'literal');
                $tipo         	= anti_injection($i[4],'numerico');
                $valor_fatura 	= anti_injection($i[5],'numerico');
                $valor_custo  	= anti_injection($i[7],'numerico');
                $inicio       	= anti_injection(implode("-",array_reverse(explode("/",$i[9]))),'literal');
                $fim          	= anti_injection(implode("-",array_reverse(explode("/",$i[10]))),'literal');
                $unidade        = anti_injection($i[11],'literal');
                $catalogo_id 	= anti_injection($i[12],'numerico');
                $tabela_origem 	= anti_injection($i[13], 'literal');
                $taxa          	= anti_injection($i[14], 'numerico');
                $incluso_pacote	= anti_injection($i[15], 'numerico');
                $tuss 			= anti_injection($i[16], 'literal');
                $codigo_proprio = anti_injection($i[17], 'literal');
                $codigo_proprio = $codigo_proprio == '' || $codigo_proprio == null ? 0 : $codigo_proprio;

                // Pegar custo real dos serviços e equuipamentos 21-03-2017 .
                if($tipo != 0 && $tipo != 1 && $tipo != 3){
                    $custoReal = Fatura::custoServicoEquipamentoByURAndPeriodo($catalogo_id, $empresa_paciente, $tabela_origem, $inicio);
                    if(!empty($custoReal)){
                        $valor_custo = $custoReal;
                    }
                }
                // Pegar custo real de material, dieta e medicamento 23-03-2017 .

                if($tipo == 0 || $tipo == 1 || $tipo == 3){
                    $custoReal = Fatura::custoCatalogoByPeriodo($catalogo_id, $inicio);
                    if(!empty($custoReal)){
                        $valor_custo = $custoReal;
                    }
                }

                $sql = "INSERT INTO faturamento (ID,SOLICITACOES_ID,NUMERO_TISS,CODIGO_PROPRIO,QUANTIDADE,TIPO,DATA_FATURADO,PACIENTE_ID,USUARIO_ID,VALOR_FATURA,VALOR_CUSTO,FATURA_ID,UNIDADE,CATALOGO_ID,TABELA_ORIGEM,DESCONTO_ACRESCIMO,INCLUSO_PACOTE,TUSS)
                                    VALUES (null,'{$sol}','{$tiss}','{$codigo_proprio}','{$qtd}','{$tipo}',now(),'{$paciente}','{$usuario}','{$valor_fatura}','{$valor_custo}','{$id_fatura}','{$unidade}','{$catalogo_id}','{$tabela_origem}','{$taxa}','{$incluso_pacote}','{$tuss}')";

                $r = mysql_query($sql);
                if(!$r){
                    echo "-161".$sql ;
                    mysql_query("rollback");
                    return;
                }

                ////Atualizar preço de venda
//                if(($tipo == 0 || $tipo== 1 || $tipo== 3) && $valor_fatura >0 ){
//                    $sql = "UPDATE catalogo set VALOR_VENDA = '{$valor_fatura}' where ID = '{$catalogo_id}' ";
//                    $r = mysql_query($sql);
//                    if(!$r){
//                        echo "-162";
//                        mysql_query("rollback");
//                        return;
//                    }
//                }
            }
        }
    }else{
        $fat_inicio	= anti_injection(implode("-",array_reverse(explode("/",$fat_inicio))),'literal');
        $fat_fim   	= anti_injection(implode("-",array_reverse(explode("/",$fat_fim))),'literal');

        if($idFaturaSalva == 0) {
            $r = mysql_query("INSERT INTO fatura (ID,PACIENTE_ID,USUARIO_ID,UR,DATA_INICIO,DATA_FIM,DATA,STATUS,OBS,PLANO_ID, created_at, created_by, remocao)
 						VALUES (null,'{$fat_paciente}','{$usuario}', '{$empresa_paciente}','{$fat_inicio}','{$fat_fim}',now(),'-1','{$obs}','{$fat_plano}', now(), '{$usuario}', '{$remocao}')");
            if (!$r) {
                echo "-155";

                mysql_query("rollback");
                return;
            }

            $id_fatura = mysql_insert_id();
        } else {
            // atualiza fatura
            $sql = "UPDATE fatura SET  OBS = '{$obs}' WHERE ID = '{$idFaturaSalva}'";
            $r = mysql_query($sql);
            if(!$r){
                echo "Erro ao atualizar fatura.";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));

            //salva historico doss itens da fatura
            $sql = "Select * from faturamento where FATURA_ID = '{$idFaturaSalva}' ";
            $result =  mysql_query($sql);
            if(!$result){
                echo "-20";
                mysql_query("ROLLBACK");
                return;
            }
            while($row = mysql_fetch_array($result)){
                $sqlHistorico = "INSERT INTO historico_faturamento (SOLICITACOES_ID, NUMERO_TISS, CODIGO_PROPRIO, QUANTIDADE, TIPO,
                            DATA_FATURADO, PACIENTE_ID, USUARIO_ID, VALOR_FATURA, VALOR_CUSTO, FATURA_ID, UNIDADE, CATALOGO_ID,
                             TABELA_ORIGEM, DESCONTO_ACRESCIMO, INCLUSO_PACOTE, TUSS, CODIGO_REFERENCIA,
                             CANCELADO_POR,CANCELADO_EM,custo_atualizado_em,custo_atualizado_por,faturamento_id)
                            VALUES ('{$row['SOLICITACOES_ID']}', '{$row['NUMERO_TISS']}','{$row['CODIGO_PROPRIO']}','{$row['QUANTIDADE']}','{$row['TIPO']}',
                           '{$row['DATA_FATURADO']}','{$row['PACIENTE_ID']}','{$row['USUARIO_ID']}','{$row['VALOR_FATURA']}','{$row['VALOR_CUSTO']}','{$row['FATURA_ID']}','{$row['UNIDADE']}','{$row['CATALOGO_ID']}',
                            '{$row['TABELA_ORIGEM']}','{$row['DESCONTO_ACRESCIMO']}','{$row['INCLUSO_PACOTE']}','{$row['TUSS']}','{$row['ID']}',
                            '{$usuario}', now(),'{$row['custo_atualizado_em']}','{$row['custo_atualizado_por']}','{$row['ID']}')";
                $rHistorico = mysql_query($sqlHistorico);
                if(!$rHistorico){
                    echo "Erro ao salvar historico".$sqlHistorico;
                    mysql_query("ROLLBACK");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }

            //deleta itens da tabela fatura anterior
            $sql = "DELETE FROM faturamento WHERE FATURA_ID = '{$idFaturaSalva}'";
            mysql_query($sql);

            $id_fatura = $idFaturaSalva;
        }

        foreach($itens as $item){
            $c++;
            if(!empty($item)){

                $i = explode("#",str_replace("undefined","",$item));

                $tiss         	= anti_injection($i[0],'literal');
                $qtd          	= anti_injection($i[1],'numerico');
                $paciente     	= $fat_paciente;
                $sol          	= anti_injection($i[3],'literal');
                $tipo         	= anti_injection($i[4],'numerico');
                $valor_fatura 	= anti_injection($i[5],'numerico');
                $valor_custo  	= anti_injection($i[7],'numerico');
                $inicio       	= anti_injection(implode("-",array_reverse(explode("/",$i[9]))),'literal');
                $fim          	= anti_injection(implode("-",array_reverse(explode("/",$i[10]))),'literal');
                $unidade      	= anti_injection($i[11],'literal');
                $catalogo_id  	= anti_injection($i[12],'numerico');
                $tabela_origem 	= anti_injection($i[13], 'literal');
                $taxa          	= anti_injection($i[14], 'numerico');
                $incluso_pacote = anti_injection($i[15], 'numerico');
                $tuss 		  	= anti_injection($i[16], 'literal');
                $codigo_proprio = anti_injection($i[17], 'literal');
                $codigo_proprio = $codigo_proprio == '' || $codigo_proprio == null ? 0 : $codigo_proprio;

                // Pegar custo real dos serviços e equuipamentos 21-03-2017 .
                if($tipo != 0 && $tipo != 1 && $tipo != 3){
                    $custoReal = Fatura::custoServicoEquipamentoByURAndPeriodo($catalogo_id, $empresa_paciente, $tabela_origem, $inicio);
                    if(!empty($custoReal)){
                        $valor_custo = $custoReal;
                    }
                }

                // Pegar custo real de material, dieta e medicamento 23-03-2017 .
                if($tipo == 0 || $tipo == 1 || $tipo == 3){
                    $custoReal = Fatura::custoCatalogoByPeriodo($catalogo_id, $inicio);
                    if(!empty($custoReal)){
                        $valor_custo = $custoReal;
                    }
                }

                $sql = "INSERT INTO faturamento (ID,SOLICITACOES_ID,NUMERO_TISS,CODIGO_PROPRIO,QUANTIDADE,TIPO,DATA_FATURADO,PACIENTE_ID,USUARIO_ID,VALOR_FATURA,VALOR_CUSTO,FATURA_ID,UNIDADE,CATALOGO_ID,TABELA_ORIGEM,DESCONTO_ACRESCIMO,INCLUSO_PACOTE,TUSS)
                    VALUES (null,'{$sol}','{$tiss}','{$codigo_proprio}','{$qtd}','{$tipo}',now(),'{$paciente}','{$usuario}','{$valor_fatura}','{$valor_custo}','{$id_fatura}','{$unidade}','{$catalogo_id}','{$tabela_origem}','{$taxa}','{$incluso_pacote}','{$tuss}')";
                $r = mysql_query($sql);
                if(!$r){
                    echo "-162" . $sql;
                    mysql_query("rollback");
                    return;
                }
            }
        }
    }
    mysql_query("commit");

    echo $id_fatura;

}

function salvar_fatura_editar($post){

    extract($post,EXTR_OVERWRITE);
    $inicio       	= anti_injection(implode("-",array_reverse(explode("/",$inicio))),'literal');
    $fim          	= anti_injection(implode("-",array_reverse(explode("/",$fim))),'literal');
    $obs =anti_injection($obs,'literal');
    $remocao = anti_injection($remocao,'literal');
    mysql_query("BEGIN");
    $c = 0;
    $itens = explode("$",$dados);
    $usuario = $_SESSION['id_user'];
    $empresa =anti_injection($empresa,'numerico');

    $condRemocao = $remocao == -1 ? '' : "remocao = '{$remocao}' ,";
    $sql = "Update fatura set {$condRemocao} USUARIO_ID='{$usuario}',DATA= now() ,DATA_INICIO ='{$inicio}',DATA_FIM='{$fim}',GUIA_TISS ={$guia_tiss},STATUS={$status},OBS='{$obs}' where ID='{$id_fatura}'";

    $r = mysql_query($sql);
    if(!$r){
        echo "-14";
        mysql_query("ROLLBACK");
        return;
    }

    foreach($itens as $item){
        $c++;
        if(!empty($item)){

            $i = explode("#",str_replace("undefined","",$item));

            $codf         = anti_injection($i[0],'numerico');
            $qtd          = anti_injection($i[1],'numerico');
            $paciente     = anti_injection($i[2],'numerico');
            $sol          = anti_injection($i[3],'literal');
            $tipo         = anti_injection($i[4],'numerico');
            $valor_fatura = anti_injection($i[5],'numerico');
            $unidade      = anti_injection($i[11],'literal');
            $valor_custo  = anti_injection($i[12],'numerico');
            $numero_tiss  = anti_injection($i[13],'literal');
            $catalogo_id    = anti_injection($i[14],'numerico');
            $tabela_origem  = anti_injection($i[15],'literal');
            $taxa           = anti_injection($i[16], 'numerico');
            $incluso_pacote = anti_injection($i[17], 'numerico');
            $editado       	= anti_injection($i[18], 'numerico');
            $tuss          	= anti_injection($i[19], 'literal');
            $codigo_proprio = anti_injection($i[20], 'literal');
            $codigo_proprio = $codigo_proprio == '' || $codigo_proprio == null ? 0 : $codigo_proprio;

            if($editado == 1){
                $sql="Update faturamento set CANCELADO_POR=$usuario, CANCELADO_EM= now() where ID= $codf ";
                $r = mysql_query($sql);
                if(!$r){
                    echo "-13";
                    mysql_query("ROLLBACK");
                    return;
                }
            }
            if($editado ==1 || $codf == 0 ){
                // Pegar custo real dos serviços e equuipamentos 21-03-2017 .
                if($codf == 0) {
                    if ($tipo != 0 && $tipo != 1 && $tipo != 3) {
                        $custoReal = Fatura::custoServicoEquipamentoByURAndPeriodo($catalogo_id, $empresa, $tabela_origem, $inicio);
                        if (!empty($custoReal)) {
                            $valor_custo = $custoReal;
                        }
                    }

                    // Pegar custo real de material, dieta e medicamento 23-03-2017 .
                    if ($tipo == 0 || $tipo == 1 || $tipo == 3) {
                        $custoReal = Fatura::custoCatalogoByPeriodo($catalogo_id, $inicio);
                        if (!empty($custoReal)) {
                            $valor_custo = $custoReal;
                        }
                    }
                }

                $sql = "INSERT INTO faturamento (ID,SOLICITACOES_ID,NUMERO_TISS,CODIGO_PROPRIO,QUANTIDADE,TIPO,DATA_FATURADO,PACIENTE_ID,USUARIO_ID,VALOR_FATURA,VALOR_CUSTO,FATURA_ID,UNIDADE,CATALOGO_ID,TABELA_ORIGEM,DESCONTO_ACRESCIMO,INCLUSO_PACOTE,TUSS,CODIGO_REFERENCIA)
                            VALUES (null,'{$sol}','{$numero_tiss}','{$codigo_proprio}','{$qtd}','{$tipo}',now(),'{$paciente}','{$usuario}','{$valor_fatura}','{$valor_custo}','{$id_fatura}','{$unidade}','{$catalogo_id}','{$tabela_origem}','{$taxa}','{$incluso_pacote}','{$tuss}','{$codf}')";

                $r = mysql_query($sql);
                if(!$r){
                    echo "-16";
                    mysql_query("ROLLBACK");
                    return;
                }
            }
        }
    }

    mysql_query("COMMIT");

    echo "1";
}

//! Salva fatura jeferson 2013-10-11.
/*function salvar_fatura($post) {
	extract($post, EXTR_OVERWRITE);
	date_default_timezone_set('America/Sao_Paulo');
	$criadaEm = date('d/m/Y H:i');


	try {
		if($fatura==0){
			$dir = "salvas/fatura/";
		}else{
			$dir = "salvas/orcamento/";
		}
		if (!is_dir($dir))
			mkdir($dir);
		$filename = $dir . "/" . $paciente.".htm";
		$arq = fopen($filename, "w");
		$p = $p."@|#".$_SESSION['nome_user']."@|#".$criadaEm;
		$escreve = fwrite($arq, $p);
		fclose($arq);

		echo "Salvo com sucesso!" . mkdir($dir);
	} catch (Exception $e) {
		echo "Falha ao gravar fatura: ", $e->getMessage(), "\n";
	}
}

function salvar_fatura_editar($post) {
	extract($post, EXTR_OVERWRITE);

	try {
		if($tipo_fatura==0 || $tipo_fatura==2 || $tipo_fatura==3 ){
			$dir = "salvas/fatura_editar/";
		}else{
			$dir = "salvas/orcamento_editar/";
		}
		date_default_timezone_set('America/Sao_Paulo');
		$criadaEm = date('d/m/Y H:i');
		if (!is_dir($dir))
			mkdir($dir);
		$filename = $dir . "/" . $paciente_id."_".$tipo_fatura.".htm";
		$arq = fopen($filename, "w");
		$html = $html."@|#".$inicio."@|#".$fim."@|#".$_SESSION['nome_user']."@|#".$criadaEm;
		$escreve = fwrite($arq, $html);
		fclose($arq);

		echo "Salvo com sucesso!" . mkdir($dir);
	} catch (Exception $e) {
		echo "Falha ao gravar fatura: ", $e->getMessage(), "\n";
	}
}*/

///////////////salvar custo_servico_plano_fatura 24-01-2014
function salvar_custo_plano_ur($post){
    extract($post, EXTR_OVERWRITE);
    // echo $itens;
    $dados= explode("$",$itens);
    $user = $_SESSION['id_user'];
    mysql_query("begin");
    foreach($dados as $dad){
        if(!empty($dad)){
            $i = explode("#",str_replace("undefined","",$dad));
            $ur= anti_injection($i[0],'numerico');
            $custo= anti_injection($i[1],'literal');

            $item=anti_injection($i[3],'numerico');
            $id_atual=anti_injection($i[4],'numerico');
            $custo_anteriror=anti_injection($i[5],'numerico');
            /////Se id_atual = -1 o custo do servico do plano para essa ur ainda n foi cadastrado na tabela  custo_servicos_plano_ur
            if($id_atual == -1){
                $sql="INSERT INTO `custo_servicos_plano_ur` (`ID`, `PLANO_ID`, `UR`, `COBRANCAPLANO_ID`, `DATA`, `USUARIO_ID`, `CUSTO`, `STATUS`)
                                   VALUES (null, 1, {$ur}, {$item}, now(),{$user} , {$custo}, 'A');";
                if(!mysql_query($sql)){
                    echo "ERRO ao inserir novo item";
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }
            if($custo !=$custo_anteriror && $id_atual!=-1 ){

                $sql = <<<SQl
				Select
				*
				from
				custo_servicos_plano_ur
				where
				ID={$id_atual}

SQl;

                $result=  mysql_query($sql);

                while($row = mysql_fetch_array($result)){

                    $sqlInsert = <<<SQl
			INSERT INTO
			historico_custo_servico_plano_ur
			(
			`id`,
			`cobrancaplano_id`,
			`empresa_id`,
			`inicio`,
			`custo`,
			`usuario_id`,
			`plano_id`,
			`created_at`,
			created_by

			)
			values
			(
			null,
			'{$row['COBRANCAPLANO_ID']}',
			'{$row['UR']}',
			'{$row['DATA']}',
			'{$row['CUSTO']}',
			'{$row['USUARIO_ID']}',
			1,
			now(),
			'{$user}'
			)

SQl;


                }

                if(!mysql_query($sqlInsert)){
                    echo "ERRO ao inserir historico de custo.".$sqlInsert;
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));


                $sql="Update
 						custo_servicos_plano_ur set
						`DATA` = now(),
						`USUARIO_ID` = {$user} ,
						`CUSTO` = {$custo}
						where
						ID={$id_atual}";
                if(!mysql_query($sql)){
                    echo "ERRO ao atualizar custo do item.";
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }
        }
    }
    mysql_query("commit");
    echo 1;
}

function cancelar_fatura($idfatura){
    $idfatura=anti_injection($idfatura,'numerico');
    mysql_query('begin');
    $sql = "Update fatura set CANCELADO_POR={$_SESSION['id_user']} , CANCELADO_EM= now() where ID={$idfatura}";

    if(!mysql_query($sql)){
        echo "Erro ao fazer cancelar.";
        mysql_query("rollback");
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));
    $sql = "Update faturamento set CANCELADO_POR ='{$_SESSION['id_user']}' and CANCELADO_EM = now() where FATURA_ID = {$idfatura} AND (CANCELADO_POR IS NULL OR CANCELADO_POR = 0)";
    $r = mysql_query($sql);
    if(!$r){
        echo "Erro ao cancelar itens da  fatura";
        mysql_query("rollback");
        return;
    }

    savelog(mysql_escape_string(addslashes($sql)));
    mysql_query('commit');
    echo '1';

}

function cancelar_item_orcamento_avulso($cod_ref){
    $cod_ref=anti_injection($cod_ref,'numerico');

    $sql ="Update `orcamento_avulso_item` set CANCELADO_POR='{$_SESSION['id_user']}' ,`CANCELADO_EM`=now() where ID={$cod_ref}";
    $r = mysql_query($sql);
    if(!$r){
        echo "Erro ao cancelar o item do orçamento avulso.";
        return;
    }

    savelog(mysql_escape_string(addslashes($sql)));
    echo 1;

}

function editar_orcamento_avulso($post){
    extract($post, EXTR_OVERWRITE);
    $inicio       = anti_injection(implode("-",array_reverse(explode("/",$inicio))),'literal');
    $fim          = anti_injection(implode("-",array_reverse(explode("/",$fim))),'literal');
    $observacao = anti_injection($observacao,'literal');
    $empresa = anti_injection($empresa,'numerico');
    mysql_query('begin');
    $sql ="Update `orcamento_avulso` set USUARIO_ID='{$_SESSION['id_user']}' ,`DATA`=now(),DATA_INICIO='{$inicio}',DATA_FIM='{$fim}',OBSERVACAO='{$observacao}' where ID={$id_orcamento}";
    $r = mysql_query($sql);
    if(!$r){
        echo "Erro ao modificar data do orçamento.";
        return;
    }

    savelog(mysql_escape_string(addslashes($sql)));

    $id_orcamento = anti_injection($id_orcamento,'numerico');
    $itens= explode("$",$dados);
    foreach($itens as $item){

        if(!empty($item)){

            $i = explode("#",str_replace("undefined","",$item));
            $tiss= anti_injection($i[0],'literal');
            $qtd           = anti_injection($i[1],'numerico');
            $tipo          = anti_injection($i[2],'numerico');
            $valor_fatura  = anti_injection($i[3],'numerico');
            $valor_custo   = anti_injection($i[4],'numerico');
            $unidade       = anti_injection($i[5],'literal');
            $catalogo_id   = anti_injection($i[6],'numerico');
            $tabela_origem = anti_injection($i[7],'literal');
            $editado       = anti_injection($i[8],'numerico');
            $cod_referencia = anti_injection($i[9],'numerico');
            $tuss = anti_injection($i[10],'literal');
            if($editado== 1){
                $sql ="Update `orcamento_avulso_item` set CANCELADO_POR='{$_SESSION['id_user']}' ,`CANCELADO_EM`=now() where ID={$cod_referencia}";
                $r = mysql_query($sql);
                if(!$r){
                    echo "Erro ao cancelar o item do orçamento avulso.";
                    return;
                }

                savelog(mysql_escape_string(addslashes($sql)));

            }
            if($editado== 1|| $cod_referencia==0){
                if($cod_referencia==0) {
                    // Pegar custo real dos serviços e equuipamentos 21-03-2017 .
                    if ($tipo != 0 && $tipo != 1 && $tipo != 3) {
                        $custoReal = Fatura::custoServicoEquipamentoByURAndPeriodo($catalogo_id, $empresa, $tabela_origem, $inicio);
                        if (!empty($custoReal)) {
                            $valor_custo = $custoReal;

                        }
                    }

                    // Pegar custo real de material, dieta e medicamento 23-03-2017 .

                    if ($tipo == 0 || $tipo == 1 || $tipo == 3) {
                        $custoReal = Fatura::custoCatalogoByPeriodo($catalogo_id, $inicio);
                        if (!empty($custoReal)) {
                            $valor_custo = $custoReal;

                        }

                    }
                }
                $sql = "INSERT INTO orcamento_avulso_item (NUMERO_TISS,QTD,TIPO,VAL_PRODUTO,CUSTO,ORCAMENTO_AVU_ID,UNIDADE,CATALOGO_ID,TABELA_ORIGEM,TUSS) VALUES ('{$tiss}','{$qtd}','{$tipo}','{$valor_fatura}','{$valor_custo}','{$id_orcamento}','{$unidade}','{$catalogo_id}','{$tabela_origem}','{$tuss}')";
                $r = mysql_query($sql);
                if(!$r){
                    echo "Erro ao inserir o item do orçamento avulso.";
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }

        }
    }
    mysql_query("commit");
    echo 1;

}

function salvar_orcamento_prorrogacao($post) {
    extract($post, EXTR_OVERWRITE);

    try {

        $dir = "salvas/orcamento-prorrogacao";
        date_default_timezone_set('America/Sao_Paulo');
        $criadaEm = date('d/m/Y H:i');


        if (!is_dir($dir))
            mkdir($dir);
        $filename = $dir . "/" . $paciente.".htm";
        $arq = fopen($filename, "w");
        $p = $p."@|#".$_SESSION['nome_user']."@|#".$criadaEm;
        $escreve = fwrite($arq, $p);
        fclose($arq);

        echo "Salvo com sucesso!" . mkdir($dir);
    } catch (Exception $e) {
        echo "Falha ao gravar fatura: ", $e->getMessage(), "\n";
    }
}

function gravar_novo_orcamento($post)
{
    extract($post, EXTR_OVERWRITE);
    mysql_query("begin");
    $c = 0;
    $itens = explode("$", $dados);
    $usuario = $_SESSION['id_user'];
    $inicio       = anti_injection(implode("-",array_reverse(explode("/",$inicio))),'literal');
    $fim          = anti_injection(implode("-",array_reverse(explode("/",$fim))),'literal');
    $fat_plano = anti_injection($fat_plano,'numerico');
    $obs =anti_injection($obs,'literal');

    $sql = "SELECT empresa FROM clientes WHERE idClientes = {$fat_paciente}";
    $rs = mysql_query($sql);
    $row = mysql_fetch_array($rs);
    $empresa_paciente = $row['empresa'];


    $sql ="select OPERADORAS_ID as idop from
		operadoras_planosdesaude as op Right Join
		clientes as c on (c.convenio = op.PLANOSDESAUDE_ID)
		WHERE c.idClientes = $paciente";
    $result=mysql_query($sql);
    $x=mysql_result($result, 0);
    $num= strlen($x);
    if($num == 1){
        $p1='00'.$x.'.'.date(ymd).'.'.date(His);
    }
    if($num == 2){
        $p1='0'.$x.'.'.date(ymd).'.'.date(His);
    }
    if($num == 3){
        $p1=$x.'.'.date(ymd).'.'.date(His);
    }

    $status= 0;
    $sql = <<<SQL
	INSERT INTO fatura
(ID,
PACIENTE_ID,
USUARIO_ID,
UR,
DATA_INICIO,
DATA_FIM,
DATA,
ORCAMENTO,
CAPA_LOTE,
DATA_LOTE,
CAPMEDICA_ID,
OBS,
PLANO_ID,
STATUS,
created_at,
created_by)
VALUES
(null,
'{$paciente}',
'{$usuario}',
'{$empresa_paciente}',
'{$inicio}',
'{$fim}',
now(),
1,
'{$p1}',
now(),
'{$id_capmedica}',
'{$obs}',
'{$fat_plano}',
2,
now(),
'{$usuario}')

SQL;

    $r = mysql_query($sql);
    if(!$r){
        echo "Erro ao salvar orçamento inicial.";
        mysql_query("ROLLBACK");
        return;
    }
    $id_fatura = mysql_insert_id();
    savelog(mysql_escape_string(addslashes($sql)));

    $itens = explode("$",$dados);
    $novosItens = 0;
    foreach($itens as $item){

        if(!empty($item)) {

            $i = explode("#", str_replace("undefined", "", $item));
            $codf = anti_injection($i[0], 'literal');
            $tiss = anti_injection($i[12], 'literal');
            $qtd = anti_injection($i[1], 'numerico');
            $paciente = anti_injection($i[2], 'numerico');
            $sol = anti_injection($i[3], 'literal');
            $tipo = anti_injection($i[4], 'numerico');
            $valor_fatura = anti_injection($i[5], 'literal');
            $valor_custo = anti_injection($i[7], 'literal');
            $unidade = anti_injection($i[11], 'literal');
            $catalogo_id = anti_injection($i[13], 'numerico');
            $tabela_origem = anti_injection($i[14], 'literal');
            $taxa = anti_injection($i[15], 'numerico');
            $incluso_pacote = anti_injection($i[16], 'numerico');
            $tuss = anti_injection($i[17], 'literal');

            if($codf == 0 )
                $novosItens++;

            $sql = "INSERT INTO
					   faturamento
					   (ID,
					   SOLICITACOES_ID,
					   NUMERO_TISS,
					   QUANTIDADE,
					   TIPO,
					   DATA_FATURADO,
					   PACIENTE_ID,
					   USUARIO_ID,
					   VALOR_FATURA,
					   VALOR_CUSTO,
					   FATURA_ID,
					   UNIDADE,
					   CATALOGO_ID,
					   TABELA_ORIGEM,
					   DESCONTO_ACRESCIMO,
					   INCLUSO_PACOTE,
					   TUSS,
					   CODIGO_REFERENCIA)
						  VALUES
						  (null,
						  '{$sol}',
						  '{$tiss}',
						  '{$qtd}',
						  '{$tipo}',
						  now(),
						  '{$paciente}',
						  '{$usuario}',
						  '{$valor_fatura}',
						  '{$valor_custo}',
						  '{$id_fatura}',
						  '{$unidade}',
						  '{$catalogo_id}',
						  '{$tabela_origem}',
						  '{$taxa}',
						  '{$incluso_pacote}',
						  '{$tuss}',
						  '{$codf}')";


            $r = mysql_query($sql);
            if (!$r) {
                echo "Erro ao inserir itens do orçamento.";
                mysql_query("ROLLBACK");
                return;
            }

            savelog(mysql_escape_string(addslashes($sql)));
        }

    }

    mysql_query('commit');
    $arquivo = "salvas/orcamento_editar/" . $usuario . "/" . $paciente . "_1.htm";
    if (file_exists($arquivo))
        unlink($arquivo);

    if($novosItens == 0){
        echo 1;
    }else{
        email_novo_orcamento($id_fatura);
        echo $id_fatura;
    }
}

function email_novo_orcamento($idFatura){
    $idFatura = anti_injection($idFatura, 'numerico');

    $sql= "SELECT
			f.id,
			UPPER(c.nome) AS paciente,
			UPPER(u.nome) AS usuario,
			DATE_FORMAT(f.DATA,'%d/%m/%Y %H:%i:%s') as data_faturado,
			DATE_FORMAT(f.DATA_INICIO,'%d/%m/%Y') as inicio,
			DATE_FORMAT(f.DATA_FIM,'%d/%m/%Y') as fim

 	FROM
             	fatura as f inner join
             	clientes AS c on (f.PACIENTE_ID = c.idClientes)
             	INNER JOIN usuarios as u on (f.USUARIO_ID = u.idUsuarios)
 	WHERE
 	           f.ID = {$idFatura}
 	ORDER BY
 	      c.nome DESC LIMIT 1";

    $result = mysql_query($sql);
    if(mysql_num_rows($result) == 0){
        echo "Erro na consulta";

    }else{

        $paciente = mysql_result($result,0,1);
        $usuario = mysql_result($result,0,2);
        $data	= mysql_result($result,0,3);
        $inicio = mysql_result($result,0,4);
        $fim = mysql_result($result,0,5);
        $titulo = 'Avisos do Sistema - Novo Orçamento Inicial para lançamento de custo.';
        $para[] = ['email' =>'faturamento@mederi.com.br', 'nome' => 'Faturamento'];

        $texto = "<p>Encontra-se no sistema um novo orçamento inicial para lançamento de custo.</p>
					<p>Orçamento Inicial #{$idFatura} do paciente {$paciente}, período {$inicio} - {$fim}
 						feito por {$usuario} em {$data}.</p>";

        enviarEmailSimples($titulo, $texto, $para);

        echo 1;

    }
}


function enviarEmailSimples($titulo, $texto, $para)
{
    $api_data = (object) Config::get('sendgrid');

    $gateway = new MailAdapter(
        new SendGridGateway(
            new \SendGrid($api_data->user, $api_data->pass),
            new \SendGrid\Email()
        )
    );

    $response = $gateway->adapter->sendSimpleMail($titulo, $texto, $para);

    return $response;
}

switch ($_POST['query']) {
    case "gravar-fatura":
        gravar_fatura($_POST);
        break;
    case "gerar-lote":
        gerar_lote($_POST);
        break;
    case "inserir_guia_tiss":
        inserir_guia_tiss($_POST);
        break;

    case "update-status-fatura":
        update_status_fatura($_POST);
        break;
    case "enviar-email-fatura":
        enviar_email_fatura($_POST);
        break;
    case "salvar-glosa":
        salvar_glosa($_POST);
        break;
    case "finalizar-glosa":
        finalizar_glosa($_POST);
        break;
    case "editar-fatura":
        editar_fatura($_POST);
        break;

    case "editar-orcamento":
        editar_orcamento($_POST);
        break;
    case "update-remove":
        update_remove($_POST);
        break;
    case "gravar-fatura-avulsa":
        gravar_avulsa($_POST);
        break;
    case "editar-brasindice":
        editar_brasindice($_POST['valor_item'], $_POST['catalogo_id'], $_POST['tipo_brasindice']);
        break;
    case "salvar-fatura":
        salvar_fatura($_POST);
        break;
    case "salvar-custo-plano-ur":
        salvar_custo_plano_ur($_POST);
        break;
    case "cancelar-fatura":
        cancelar_fatura($_POST['id_fatura']);
        break;
    case "cancelar-item-orcamento-avulso":
        cancelar_item_orcamento_avulso($_POST['cod_referencia']);
        break;
    case "editar-orcamento-avulso":
        editar_orcamento_avulso($_POST);
        break;
    case "salvar-fatura-editar":
        salvar_fatura_editar($_POST);
        break;
    case "salvar-orcamento-prorrogacao":
        salvar_orcamento_prorrogacao($_POST);
        break;
    case "salvar-novo-orcamento":
        gravar_novo_orcamento($_POST);
        break;

}
