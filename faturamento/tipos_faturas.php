<?php

require __DIR__ . '/../vendor/autoload.php';

use App\Models\Faturamento\Fatura;

class LayoutFaturas
{

	public function conta_aberta($sql,$av,$inicio,$fim,$diarias,$observacao){
		$html.= "<table  width=100% class='mytable'>";

		$result = mysql_query($sql);
		$total=0.00;
		$cont_cor=0;
		$subtotal=0.00;
		$subtotal_equipamento=0.00;
		$subtotal_material=0.00;
		$subtotal_medicamento=0.00;
		$subtotal_gas=0.00;
		$subtotal_dieta=0.00;

		$subtotal_servico=0.00;
		$cont1=0;
		$cont2=0;
		$cont3=0;
		$cont4=0;
		$cont5=0;
		$cont6=0;
		while($row = mysql_fetch_array($result)){

			$descontoAcrescimo = Fatura::calculaDescontoAcrescimo(
				$row['VALOR_FATURA'],
				$row['DESCONTO_ACRESCIMO'],
				$row['QUANTIDADE']
			);

			if ($row['DESCONTO_ACRESCIMO'] < 0)
				$desconto += $descontoAcrescimo;

			if ($row['DESCONTO_ACRESCIMO'] > 0)
				$acrescimo += $descontoAcrescimo;

			$subtotal +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
			$paciente=$row['PACIENTE_ID'];


			if($row['TIPO'] == '6'){
				$subtotal_gas +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
				$parcial=$subtotal_gas;
				$cont5++;
				$n=$row['cod'];
				$tip = $row['TIPO'];
				$tipo_tot="TOTAL GASOTERAPIA";

			}

			if($row['TIPO'] == '5'){
				$subtotal_equipamento +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
				$parcial=$subtotal_equipamento;
				$cont4++;
				$n=$row['cod'];
				$tip = $row['TIPO'];
				$tipo_tot="TOTAL MATERIAIS PERMANENTES";

			}
			if($row['TIPO'] == '3'){
				$subtotal_dieta +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
				$cont6++;
				$n = $row['TUSS']!=NULL && $row['TUSS']!=0 ?"TUSS. ".$row['TUSS']:"Bras. ".$row['NUMERO_TISS'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_dieta;
				$tipo_tot="TOTAL DIETA";
			}
			if($row['TIPO'] == '2'){
				$subtotal_servico +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
				$cont3++;
				$n=$row['cod'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_servico;
				$tipo_tot="TOTAL SERVI&Ccedil;O";
			}
			if($row['TIPO'] == '1'){
				$subtotal_material +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
				$cont1++;
				$n = $row['TUSS'] != NULL && $row['TUSS'] != 0  ?"TUSS. ".$row['TUSS']:"Simp. ".$row['NUMERO_TISS'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_material;
				$tipo_tot="TOTAL MATERIAL";
			}
			if($row['TIPO'] == '0'){
				$subtotal_medicamento +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
				$cont2++;
				$n = $row['TUSS'] != NULL && $row['TUSS'] != 0 ?"TUSS. ".$row['TUSS']:"Bras. ".$row['NUMERO_TISS'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_medicamento;
				$tipo_tot="TOTAL MEDICAMENTO";
			}

			if( $cont_cor%2 ==0 ){
				$cor = '';
			}else{
				$cor="bgcolor='#EEEEEE'";
			}

			$und=$row['UNIDADE'];

			if($cont5 == 1){
				if($tip != $tipo && $cont_cor != 0){
					$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
					$html .="<tr ><td colspan='6'><td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;border:1px solid #000;'>Gasoterapia</td></tr>";
				$html.= "<tr bgcolor=bgcolor='#cccccc'>
		     	<td style='font-size:12px;float:right;'>N&deg;</td>
		     	<td style='font-size:12px;float:right;'>Unidade</td>
		     	<td style='font-size:12px;float:right;'>Item</td>
		     	<th style='font-size:12px;float:right;'>Qtd</th>
		     	<th style='font-size:12px;float:right;'>Pre&ccedil;o Unt.</th>
		     	<th style='font-size:12px;float:right;'>Pre&ccedil;o Tot.</th>
		     	</tr>";


			}
			if($cont4 == 1){
				if($tip != $tipo && $cont_cor != 0){
					$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr><br/>";
					$html .="<tr ><td colspan='6'><td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>MATERIAIS PERMANENTES</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
				</tr>";


			}
			if($cont6 == 1 ){
				if($tip != $tipo && $cont_cor != 0){
					$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;bgcolor:#EEEEEE;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr><br/>";
					$html .="<tr ><td colspan='6'><td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>DIETA</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
			
			
				</tr>";

			}
			if($cont3 == 1 ){
				if($tip != $tipo && $cont_cor != 0){
					$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;bgcolor:#EEEEEE;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr><br/>";
					$html .="<tr ><td colspan='6'><td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>SERVI&Ccedil;OS</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
			
			
				</tr>";

			}
			if($cont1 == 1){
				if($tip != $tipo && $cont_cor != 0){
					$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;bgcolor:#EEEEEE;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr><br/>";
					$html .="<tr ><td colspan='6'><td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>MATERIAIS</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot. </th>
				</tr>";

			}
			if($cont2 == 1){
				if($tip != $tipo && $cont_cor != 0 ){
					$html.= "<tr><td colspan='6'  style='font-size:12px;float:right bgcolor:#EEEEEE;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr><br/>";
					$html .="<tr><td colspan='6'><td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE; '>MEDICAMENTOS</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
				</tr>";

			}


			//
			$tipo = $tip;
			$par= $parcial;
			$tipo_tot1=$tipo_tot;

			//////2014-01-16
			///////// se for material coloca a descrição igual do Simpro que vai ser para fatura porque o principio é um ‘nome genérico’ que as enfermeiras usam.
			if($row['tipo'] == 1 && $row['segundonome']!= NULL){
				$nomeitem =$row['segundonome'];
			}else{
				$nomeitem = $row['principio'];
			}
			if($row['ref']== 'I'){
				$n = "INTERNO";

			}
			if($row['CODIGO_PROPRIO'] != '0'){
				$n = $row['CODIGO_PROPRIO'];
			}

            $sqlOperadora = "
                SELECT
                    operadoras_planosdesaude.OPERADORAS_ID
                FROM
                    operadoras_planosdesaude				 
                WHERE
                    operadoras_planosdesaude.PLANOSDESAUDE_ID = '{$row['PLANO_ID']}'
                ORDER BY operadoras_planosdesaude.OPERADORAS_ID LIMIT 1 ";

            $rowOperadora = mysql_fetch_array(mysql_query($sqlOperadora));

			if($row['TIPO'] == '0' && $row['atb'] == 'S' && $rowOperadora['OPERADORAS_ID'] == '56'){
                $nomeitem = sprintf('(FAB: %s) %s', $row['fabricante'], $nomeitem);
            }

			$html.= "<tr $cor>
			<td>{$n}</td>
			<td>{$und}</td>
			<td width='50%'>".strtoupper(strtr($nomeitem ,"áéíóúâêôãõàèìòùç","ÁÉÍÓÚÂÊÔÃÕÀÈÌÒÙÇ"))."</td>
			<td>{$row['QUANTIDADE']}</td>
			<td>R$ ".number_format($row['VALOR_FATURA'],2,',','.')."</td>";
			$vt=$row['VALOR_FATURA']*$row['QUANTIDADE'];
			$vtotal += $vt;
			$html.= "<td>R$ ".number_format($vt,2,',','.')."</td>
			</tr>";
			if($cont6 == 1){
				$cont6++;

			}
			if($cont5 == 1){
				$cont5++;
			}
			if($cont4 == 1){
				$cont4++;

			}
			if($cont3 == 1){
				$cont3++;

			}
			if($cont1 == 1){
				$cont1++;

			}
			if($cont2 == 1){
				$cont2++;

			}
			$cont_cor++;
			$obs=$row['OBS'];
		}

		$obj_data_inicio = new DateTime($inicio);
		$obj_data_fim = new DateTime($fim);
		$diferenca_dias = $obj_data_inicio->diff($obj_data_fim);
		$days = $diferenca_dias->days;

		if($days)
		{

			$diferenca = $days;
			$diferenca++;
			$diaria = ($vtotal+$desconto+$acrescimo)/$diferenca;
			$diaria_ceil =  ceil($diaria * 100) / 100;
		}
		else
		{
			$diaria = 0.00;
		}
		if($av==1)
		{
			$label_total = "TOTAL DO PERÍODO {$obj_data_inicio->format("d/m/Y")} ATÉ {$obj_data_fim->format("d/m/Y")}";

		}
		else
		{
			$label_total = "TOTAL DA FATURA";
		}
		$html.= "<tr ><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
		$html .="<tr><td colspan='6'><td></tr>";
		$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'></td></tr>";
		$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'></td></tr>";
		$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'></td></tr>";
		$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>Valor Parcial:  R$ </b><b><i>".number_format($vtotal,2,',','.')."</i></b></td></tr>";
		if($desconto != 0.00) {
            $html .= "<tr><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>Desconto:  R$ </b><b><i>" . number_format(-1 * $desconto, 2, ',', '.') . "</i></b></td></tr>";
        }
		if($acrescimo != 0.00) {
            $html .= "<tr><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>Acrescimo:  R$ </b><b><i>" . number_format($acrescimo, 2, ',', '.') . "</i></b></td></tr>";
        }
		$html.= "<tr bgcolor='#eee' ><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>$label_total:  R$ </b><b><i>".number_format($vtotal+$desconto+$acrescimo,2,',','.')."</i></b></td></tr>";
		if($av==1)
		{
			//$label_total = "TOTAL DO PERÍODO {$obj_data_inicio->format("d/m/Y")} ATÉ {$obj_data_fim->format("d/m/Y")}";
			$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'></td></tr>";
			$html.= "<tr ><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>VALOR DA DIÁRIA: R$</b> <b><i>".number_format($diaria_ceil,2,',','.')."</i></b></td></tr>";
			if($diarias>0){
				$html.= "<tr bgcolor='#eee'><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;font-size:14px;'><b>VALOR PARA {$diarias} DIA(S): R$ </b><b><i>".number_format(ceil(($diaria*$diarias)*100)/100,2,',','.')."</i></b></td></tr>";

			}

		}

		$html.= "</table>";
		if($observacao==1){
			$html.="<p><b>Observação:</b></br>
                               $obs
                            </p>";
		}

		return $html;
	}

	public function conta_fechada($sql,$av,$inicio,$fim,$diarias,$observacao){
		$html.= "<table  width=100% class='mytable'>";


		$total=0.00;
		$cont_cor=0;
		$subtotal=0.00;
		$subtotal_equipamento=0.00;
		$subtotal_material=0.00;
		$subtotal_medicamento=0.00;
		$subtotal_gas=0.00;
		$subtotal_dieta=0.00;

		$subtotal_servico=0.00;
		$cont1=0;
		$cont2=0;
		$cont3=0;
		$cont4=0;
		$cont5=0;
		$cont6=0;
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result)){
			if($row['INCLUSO_PACOTE']==0){
				$subtotal +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
				$paciente=$row['PACIENTE_ID'];


				if($row['TIPO'] == '6'){
					$subtotal_gas +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
					$parcial=$subtotal_gas;
					$cont5++;
					$n=$row['cod'];
					$tip = $row['TIPO'];
					$tipo_tot="TOTAL GASOTERAPIA";

				}

				if($row['TIPO'] == '5'){
					$subtotal_equipamento +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
					$parcial=$subtotal_equipamento;
					$cont4++;
					$n=$row['cod'];
					$tip = $row['TIPO'];
					$tipo_tot="TOTAL MATERIAIS PERMANENTES";

				}
				if($row['TIPO'] == '3'){
					$subtotal_dieta +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
					$cont6++;
					$n = $row['TUSS']!=NULL ?"TUSS. ".$row['TUSS']:"Bras. ".$row['NUMERO_TISS'];

					$tip = $row['TIPO'];
					$parcial=$subtotal_dieta;
					$tipo_tot="TOTAL DIETA";
				}
				if($row['TIPO'] == '2'){
					$subtotal_servico +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
					$cont3++;
					$n=$row['cod'];
					$tip = $row['TIPO'];
					$parcial=$subtotal_servico;
					$tipo_tot="TOTAL SERVI&Ccedil;O";
				}
				if($row['TIPO'] == '1'){
					$subtotal_material +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
					$cont1++;
					$n = $row['TUSS']!=NULL ?"TUSS. ".$row['TUSS']:"Simp. . ".$row['NUMERO_TISS'];

					$tip = $row['TIPO'];
					$parcial=$subtotal_material;
					$tipo_tot="TOTAL MATERIAL";
				}
				if($row['TIPO'] == '0'){
					$subtotal_medicamento +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
					$cont2++;
					$n = $row['TUSS']!=NULL ?"TUSS. ".$row['TUSS']:"Bras. ".$row['NUMERO_TISS'];
					$tip = $row['TIPO'];
					$parcial=$subtotal_medicamento;
					$tipo_tot="TOTAL MEDICAMENTO";
				}

				if( $cont_cor%2 ==0 ){
					$cor = '';
				}else{
					$cor="bgcolor='#EEEEEE'";
				}

				$und=$row['UNIDADE'];

				if($cont5 == 1){
					if($tip != $tipo && $cont_cor != 0){
						$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
						$html .="<tr ><td colspan='6'><td></tr>";
					}
					$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;border:1px solid #000;'>Gasoterapia</td></tr>";
					$html.= "<tr bgcolor=bgcolor='#cccccc'>
		     	<td style='font-size:12px;float:right;'>N&deg;</td>
		     	<td style='font-size:12px;float:right;'>Unidade</td>
		     	<td style='font-size:12px;float:right;'>Item</td>
		     	<th style='font-size:12px;float:right;'>Qtd</th>
		     	<th style='font-size:12px;float:right;'>Pre&ccedil;o Unt.</th>
		     	<th style='font-size:12px;float:right;'>Pre&ccedil;o Tot.</th>
		     	</tr>";


				}
				if($cont4 == 1){
					if($tip != $tipo && $cont_cor != 0){
						$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr><br/>";
						$html .="<tr ><td colspan='6'><td></tr>";
					}
					$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>MATERIAIS PERMANENTES</td></tr>";
					$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
				</tr>";


				}
				if($cont6 == 1 ){
					if($tip != $tipo && $cont_cor != 0){
						$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;bgcolor:#EEEEEE;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr><br/>";
						$html .="<tr ><td colspan='6'><td></tr>";
					}
					$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>DIETA</td></tr>";
					$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
			
			
				</tr>";

				}
				if($cont3 == 1 ){
					if($tip != $tipo && $cont_cor != 0){
						$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;bgcolor:#EEEEEE;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr><br/>";
						$html .="<tr ><td colspan='6'><td></tr>";
					}
					$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>SERVI&Ccedil;OS</td></tr>";
					$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
			
			
				</tr>";

				}
				if($cont1 == 1){
					if($tip != $tipo && $cont_cor != 0){
						$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;bgcolor:#EEEEEE;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr><br/>";
						$html .="<tr ><td colspan='6'><td></tr>";
					}
					$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>MATERIAIS</td></tr>";
					$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot. </th>
				</tr>";

				}
				if($cont2 == 1){
					if($tip != $tipo && $cont_cor != 0 ){
						$html.= "<tr><td colspan='6'  style='font-size:12px;float:right bgcolor:#EEEEEE;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr><br/>";
						$html .="<tr><td colspan='6'><td></tr>";
					}
					$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE; '>MEDICAMENTOS</td></tr>";
					$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
				</tr>";

				}


				//
				$tipo = $tip;
				$par= $parcial;
				$tipo_tot1=$tipo_tot;

				//////2014-01-16
				///////// se for material coloca a descrição igual do Simpro que vai ser para fatura porque o principio é um ‘nome genérico’ que as enfermeiras usam.
				if($row['tipo'] == 1 && $row['segundonome']!= NULL){
					$nomeitem =$row['segundonome'];
				}else{
					$nomeitem = $row['principio'];
				}

                $sqlOperadora = "
                SELECT
                    operadoras_planosdesaude.OPERADORAS_ID
                FROM
                    operadoras_planosdesaude				 
                WHERE
                    operadoras_planosdesaude.PLANOSDESAUDE_ID = '{$row['PLANO_ID']}'
                LIMIT 1 DESC ";

                $rowOperadora = mysql_fetch_array(mysql_query($sqlOperadora));

                if($row['tipo'] == '0' && $row['atb'] == 'S' && $rowOperadora['OPERADORAS_ID'] == '56'){
                    $nomeitem = sprintf('(FAB: %s) %s', $row['fabricante'], $nomeitem);
                }

				$html.= "<tr $cor>
			<td>{$n}</td>
			<td>{$und}</td>
			<td width='50%'>".strtoupper(strtr($nomeitem ,"áéíóúâêôãõàèìòùç","ÁÉÍÓÚÂÊÔÃÕÀÈÌÒÙÇ"))."</td>
			<td>{$row['QUANTIDADE']}</td>
			<td>R$ ".number_format($row['VALOR_FATURA'],2,',','.')."</td>";
				$vt=$row['VALOR_FATURA']*$row['QUANTIDADE'];
				$vtotal += $vt;
				$desconto += $row['DESCONTO_ACRESCIMO']<= 0 ? ($row['DESCONTO_ACRESCIMO']/100)*($row['QUANTIDADE']*$row['VALOR_FATURA']):0.00;
				$acrescimo += $row['DESCONTO_ACRESCIMO']>= 0 ? ($row['DESCONTO_ACRESCIMO']/100)*($row['QUANTIDADE']*$row['VALOR_FATURA']):0.00;

				$html.= "<td>R$ ".number_format($vt,2,',','.')."</td>
			</tr>";
				if($cont6 == 1){
					$cont6++;

				}
				if($cont5 == 1){
					$cont5++;
				}
				if($cont4 == 1){
					$cont4++;

				}
				if($cont3 == 1){
					$cont3++;

				}
				if($cont1 == 1){
					$cont1++;

				}
				if($cont2 == 1){
					$cont2++;

				}
				$cont_cor++;
				$obs=$row['OBS'];
			}

		}

		$obj_data_inicio = new DateTime($inicio);
		$obj_data_fim = new DateTime($fim);
		$diferenca_dias = $obj_data_inicio->diff($obj_data_fim);
		$days = $diferenca_dias->days;
		if($days)
		{
			$diferenca = $days;
			$diferenca++;
			$diaria = ($vtotal+$desconto+$acrescimo)/$diferenca;
			$diaria_ceil =  ceil($diaria * 100) / 100;

		}
		else
		{
			$diaria = 0.00;
		}
		if($av!=0)
		{
			$label_total = "TOTAL DO PERÍODO {$obj_data_inicio->format("d/m/Y")} ATÉ {$obj_data_fim->format("d/m/Y")}";

		}
		else
		{
			$label_total = "TOTAL DA FATURA";
		}
		$html.= "<tr ><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
		$html .="<tr><td colspan='6'><td></tr>";
		$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'></td></tr>";
		$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'></td></tr>";
		$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>Valor Parcial:  R$ </b><b><i>".number_format($vtotal,2,',','.')."</i></b></td></tr>";
		if($desconto != 0.00) {
            $html .= "<tr><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>Desconto:  R$ </b><b><i>" . number_format(-1 * $desconto, 2, ',', '.') . "</i></b></td></tr>";
        }
		if($acrescimo != 0.00) {
            $html .= "<tr><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>Acrescimo:  R$ </b><b><i>" . number_format($acrescimo, 2, ',', '.') . "</i></b></td></tr>";
        }
		$html.= "<tr bgcolor='#eee' ><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>$label_total:  R$ </b><b><i>".number_format($vtotal+$desconto+$acrescimo,2,',','.')."</i></b></td></tr>";
		if($av==1)
		{

			$html.= "<tr><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'></td></tr>";
			$html.= "<tr ><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>VALOR DA DIÁRIA: R$</b> <b><i>".number_format($diaria_ceil,2,',','.')."</i></b></td></tr>";
			if($diarias>0){
				$html.= "<tr bgcolor='#eee'><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;font-size:14px;'><b>VALOR PARA {$diarias} DIA(S): R$ </b><b><i>".number_format(ceil(($diaria*$diarias)*100)/100,2,',','.')."</i></b></td></tr>";


			}
		}
		$html.= "</table>";
		if($observacao==1){
			$html.="<p><b>Observação:</b></br>
                               $obs
                            </p>";
		}

		return $html;

	}


	public function sulamerica_liminar($sql,$av,$inicio,$fim,$observacao){
		$html.= "<table  width=100% class='mytable'>";
		$html.= "<tr bgcolor='#cccccc'>
                    <td style='font-size:12px;float:right; '>N&deg;</td>
                    <td style='font-size:12px;float:right; '>Unidade</td>
                    <td style='font-size:12px;float:right; '>Item</td>
                    <th style='font-size:12px;float:right; '>Qtd</th>
                    <th style='font-size:12px;float:right; '>Pre&ccedil;o</th>
                    <th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
                </tr>";
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result)){
			$valor_total +=$row['QUANTIDADE']*$row['VALOR_FATURA'];
			$desconto += $row['DESCONTO_ACRESCIMO']<= 0 ? ($row['DESCONTO_ACRESCIMO']/100)*($row['QUANTIDADE']*$row['VALOR_FATURA']):0.00;
			$acrescimo += $row['DESCONTO_ACRESCIMO']>= 0 ? ($row['DESCONTO_ACRESCIMO']/100)*($row['QUANTIDADE']*$row['VALOR_FATURA']):0.00;
			$obs=$row['OBS'];
			if($row['INCLUSO_PACOTE']==0){
				$sql_valorescobranca= "Select * from valorescobranca where id ={$row['CATALOGO_ID']} ";
				$result_valorescobranca = mysql_query($sql_valorescobranca);
				while($row2 = mysql_fetch_array($result_valorescobranca)){
					$html.= "<tr >
                            <td>{$row2['COD_COBRANCA_PLANO']}</td>
                            <td>{$row['UNIDADE']}</td>
                            <td width='50%'>".strtoupper(strtr($row['principio'],"áéíóúâêôãõàèìòùç","ÁÉÍÓÚÂÊÔÃÕÀÈÌÒÙÇ"))."</td>
                            <td>{$row['QUANTIDADE']}</td>
                            <td>R$ ".number_format($row2['CUSTO_COBRANCA_PLANO'],2,',','.')."</td>";
					$valor_total_item=$row2['CUSTO_COBRANCA_PLANO']*$row['QUANTIDADE'];

					$html.= "<td>R$ ".number_format($valor_total_item,2,',','.')."</td>
                            </tr>";
					$quantidade_diaria = $row['QUANTIDADE'];

				}
			}
		}
		//////verifacar quanto vai ser a díaria de ajuste.
		$valor_diaria_judicial=( ($valor_total+$desconto+$acrescimo) - $valor_total_item ) /$quantidade_diaria;
		$html.= "<tr>
                             <td>61.21.009.9</td>                             
                             <td>DI&Aacute;RIA</td>
                             <td>DI&Aacute;RIA DE AJUSTE SOB LIMINAR JUDICIAL</td>
                             <td>$quantidade_diaria</td>
                             <td>R$ ".  number_format($valor_diaria_judicial,2,',','.')."</td>
                             <td>R$ ".  number_format($valor_diaria_judicial*$quantidade_diaria,2,',','.')."</td>
                             </tr>";
		$html.= "<tr bgcolor='#cccccc'>
                             <td></td>                             
                             <td></td>
                             <td></td>
                             <td colspan='2'><b>VALOR TOTAL</b></td>
                             <td>R$ ".  number_format($valor_total,2,',','.')."</td>
                             </tr>
                             </table>";
		if($observacao==1){
			$html.="<p><b>Observação:</b></br>
                               $obs
                            </p>";
		}
		return $html;

	}

}
?>
