<?php

$id = session_id();
if(empty($id))
	session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once ('tipos_faturas.php');
require __DIR__ . '/../app/Models/Faturamento/Fatura.php';

use App\Models\Faturamento\Fatura;

class ImprimirFaturaItensOriginais{

	public $inicio;
	public $fim;

	public function cabecalho($id){


		$sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		UPPER(u.nome) AS usuario,DATE_FORMAT(f.DATA,'%Y-%m-%d') as DATA,f.DATA_INICIO,f.DATA_FIM,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo1,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresan,
		c.`nascimento`,
		c.diagnostico,
		c.codigo,
		p.nome as Convenio,p.id,c.*,
		NUM_MATRICULA_CONVENIO,
                f.GUIA_TISS,
                cpf
		FROM
		fatura as f inner join
		clientes AS c on (f.PACIENTE_ID = c.idClientes) LEFT JOIN
		empresas AS e ON (e.id = c.empresa) LEFT JOIN 
		planosdesaude as p ON (p.id = f.PLANO_ID) INNER JOIN
                usuarios as u on (f.USUARIO_ID = u.idUsuarios)
		
		WHERE
		f.ID = {$id}
		ORDER BY
		c.nome DESC LIMIT 1";

		$result = mysql_query($sql);
		$result2 = mysql_query($sql2);
		$html.= "<table style='width:100%;' >";
		while($pessoa = mysql_fetch_array($result2)){
			foreach($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);

			}
			$this->inicio = $pessoa['DATA_INICIO'];
			$this->fim = $pessoa['DATA_FIM'];
			$idade = join("/",array_reverse(explode("-",$pessoa['nascimento'])))."(".$pessoa['idade']." anos)";

			if($idade == "00/00/0000( anos)"){
				$idade='';
			}
			$html.= "<tr bgcolor='#EEEEEE'>";
			$html.= "<td  style='width:60%;'><b>PACIENTE:</b></td>";
			$html.= "<td style='width:20%;' ><b>SEXO </b></td>";
			$html.= "<td style='width:30%;'><b>IDADE:</b></td>";
			$html.= "</tr>";
			$html.= "<tr>";
			$html.= "<td style='width:60%;'>{$pessoa['paciente']}</td>";
			$html.= "<td style='width:20%;'>{$pessoa['sexo1']}</td>";
			$html.= "<td style='width:30%;'>{$idade}</td>";
			$html.= "</tr>";

			$html.= "<tr bgcolor='#EEEEEE'>";

			$html.= "<td style='width:60%;'><b>UNIDADE REGIONAL:</b></td>";
			$html.= "<td style='width:20%;'><b>CONV&Ecirc;NIO </b></td>";
			$html.= "<td style='width:30%;'><b>MATR&Iacute;CULA </b></td>";
			$html.= "</tr>";
			$html.= "<tr>";

			$html.= "<td style='width:60%;'>{$pessoa['empresan']}</td>";
			$html.= "<td style='width:20%;'>".strtoupper($pessoa['Convenio'])."</td>";
			$html.= "<td style='width:30%;'>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
			$html.= "</tr>";

			$html.= "<tr bgcolor='#EEEEEE'>";

			$html.= "<td style='width:60%;'><b>Faturista:</b></td>";
			$html.= "<td style='width:20%;'><b>Data faturado: </b></td>";
			$html.= "<td style='width:30%;'><b>Per&iacute;odo</b></td>";
			$html.= "</tr>";
			$html.= "<tr>";

			$html.= "<td style='width:60%;'>{$pessoa['usuario']}</td>";
			$html.= "<td style='width:20%;'>".join("/",array_reverse(explode("-",$pessoa['DATA'])))."</td>";
			$html.= "<td style='width:30%;'>".join("/",array_reverse(explode("-",$pessoa['DATA_INICIO'])))." at&eacute; ".join("/",array_reverse(explode("-",$pessoa['DATA_FIM'])))."</td>";
			$html.= "</tr>";

			$html.= "<tr bgcolor='#EEEEEE'>";

			$html.= "<td style='width:60%;'><b>CID:</b></td>";
			$html.= "<td style='width:20%;'><b>CONTA MEDERI: </b></td>";
			$html.= "<td style='width:30%;'><b>N° DA GUIA</b></td>";
			$html.= "</tr>";
			$html.= "<tr>";

			$html.= "<td style='width:60%;'>{$pessoa['diagnostico']}</td>";
			$html.= "<td style='width:20%;'>{$pessoa['codigo']}</td>";
			$html.= "<td style='width:30%;'>{$pessoa['GUIA_TISS']}</td>";
			$html.= "</tr>";
			$html.= " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";






			$this->plan =  $pessoa['id'];
		}
		$html.= "</table>";

		return $html;
	}





	public function detalhe_fatura($id){
		$html .="<form>";

			$html .= "<h1><a><img src='../utils/logo2.jpg' width='200' >
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>Fatura Original #{$id} </h1>";


		$html .= $this->cabecalho($id);
        $html .= "<br>
					<div width='100%'>
						
						<span style='text-align: center'><b>OBS: Essa fatura contém apenas os itens que seriam cobrados ao convênio antes do consenso.</b></span>
						
						</div>";

        $condicao = " ";

		$sql = Fatura::getSQLItensFaturaOriginal($id, $condicao);


		$layout = new LayoutFaturas();

				$html.= $layout->conta_aberta($sql, $av = null,
					$this->inicio  = null,
				$this->fim = null,
				$diarias  = null,
				$observacao = null);




		$paginas []= $header.$html.= "</form>";


		//print_r($paginas);
		//print_r($paginas);

		$mpdf=new mPDF('pt','A4',9);
		$mpdf->SetHeader('página {PAGENO} de {nbpg}');
		$ano = date("Y");
		$mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
		$mpdf->WriteHTML("<html><body>");
		$flag = false;
		foreach($paginas as $pag){
			if($flag) $mpdf->WriteHTML("<formfeed>");
			$mpdf->WriteHTML($pag);
			$flag = true;
		}
		$mpdf->WriteHTML("</body></html>");
		$mpdf->Output('prescricao_avaliacao.pdf','I');
		exit;



		/*$mpdf=new mPDF('pt','A4',9);
		$mpdf->WriteHTML($html);
		$mpdf->Output('pacientes.pdf','I');*/



	}





}




$p = new  ImprimirFaturaItensOriginais();
$p->detalhe_fatura($_GET['id']);






?>

