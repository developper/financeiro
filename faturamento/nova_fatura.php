<?php
class NovaFatura
{
    
    public function getMedicamento($paciente, $inicio, $fim, $operadora)
    {
        $tabelaContrato = $this->getTabelaMedicamento($operadora);
        $itemTabelaCobranca = "IF(B.DESC_MATERIAIS_FATURAR IS NULL, concat(B.principio, ' - ', B.apresentacao), B.DESC_MATERIAIS_FATURAR) AS principio,";
        $valorItem = "COALESCE(B.VALOR_VENDA, '0.00') AS valor,";
        $codigoProprio = ", 0 AS codigo_proprio";
        $tabelaOrigem = "'catalogo'";

        if ($tabelaContrato != false) {
            $leftTabelaCobrancaMedicamento = "left join tabelas_cobrancas_medicamentos as ctm on 
                                        (B.ID = ctm.catalogo_id and ctm.tabelas_cobrancas_id = $tabelaContrato )";
            $itemTabelaCobranca = "COALESCE(ctm.item,IF(B.DESC_MATERIAIS_FATURAR IS NULL, concat(B.principio, ' - ', B.apresentacao), B.DESC_MATERIAIS_FATURAR)) AS principio,";
            $valorItem = "COALESCE(ctm.valor,COALESCE(B.VALOR_VENDA, '0.00')) AS valor,";
            $codigoProprio = ", ctm.codigo_fatura AS codigo_proprio";
            $tabelaOrigem =  "if(ctm.item is null,'catalogo','tabelas_cobrancas_medicamentos')";
        }
        $sql ="SELECT
        principal.principio,
        principal.cod,
        principal.catalogo_id,
        principal.TABELA_ORIGEM,
        principal.nome,
        principal.tipo,
        principal.flag,
        principal.idSaida,
        principal.Paciente,
        principal.Plano,
        COALESCE(
                ROUND(
                    SUM( principal.quantidade * principal.custo  )
                    / 
                    SUM( principal.quantidade),
                    2
                  ), 
                  '0.00'  
              )
         AS custo,
        principal.apresentacao,
        SUM(principal.quantidade) AS quantidade,
        principal.valor,
        principal.nomeplano,
        principal.empresa,
        principal.codTuss,
        principal.tipo_medicamento,
        principal.codigo_proprio
      FROM
        (

          SELECT      
      $itemTabelaCobranca
      S.NUMERO_TISS AS cod,
      S.CATALOGO_ID AS catalogo_id,
      $tabelaOrigem AS TABELA_ORIGEM,
      C.nome,
      S.tipo,
      'P' AS flag,
      S.idSaida,
      C.idClientes AS Paciente,
      C.convenio AS Plano,      
      COALESCE(valorMedio , '0.00') AS custo,
      (
        CASE
          S.tipo
          WHEN
            '0'
          THEN
            'Medicamento'
         
        END
      )
      AS apresentacao,
      (
        CASE
          WHEN
            COUNT(*) > 1
          THEN
            SUM(S.quantidade)
          ELSE
            S.quantidade
        END
      )
      AS quantidade,
      $valorItem
      PL.nome AS nomeplano, 
      C.empresa,
      B.TUSS AS codTuss,
      PL.tipo_medicamento
      $codigoProprio
    FROM
      `saida` S
      INNER JOIN
        `clientes` C
        ON (S.idCliente = C.idClientes)
      INNER JOIN
        `catalogo` B
        ON (S.CATALOGO_ID = B.ID)
      INNER JOIN
        planosdesaude AS PL
        ON ( C.convenio = PL.id)
      INNER JOIN
        solicitacoes AS Sol
        ON (S.idSolicitacao = Sol.idSolicitacoes)
      INNER JOIN
        prescricoes AS P
        ON (Sol.idPrescricao = P.id)
        $leftTabelaCobrancaMedicamento
    WHERE
      C.idClientes = '{$paciente}'
      AND
      (
        P.inicio BETWEEN '{$inicio}' AND '{$fim}'
        OR P.fim BETWEEN '{$inicio}' AND '{$fim}'
      )
      AND S.quantidade > 0 
      AND
      (
        C.idClientes = S.idCliente
      )
      AND S.tipo IN
      (
        0, 1, 3
      )
      and ( S.catalogo_id_escolhido is null or S.catalogo_id_escolhido = 0 or S.catalogo_id_escolhido ='')
      group by Paciente, CATALOGO_ID
      Union
      SELECT      
      $itemTabelaCobranca
      S.NUMERO_TISS AS cod,
      B.ID AS catalogo_id,
      $tabelaOrigem AS TABELA_ORIGEM,
      C.nome,
      S.tipo,
      'P' AS flag,
      S.idSaida,
      C.idClientes AS Paciente,
      C.convenio AS Plano,  
      COALESCE(
                ROUND(
                    SUM( S.quantidade * (( SELECT cat.valorMedio FROM catalogo AS cat WHERE cat.ID = S.CATALOGO_ID ))  )
                    / 
                    SUM( S.quantidade),
                    2
                  ), 
                  '0.00'  
              )
         AS custo,    
      (
        CASE
          S.tipo
          WHEN
            '0'
          THEN
            'Medicamento'
          
        END
      )
      AS apresentacao,
      (
        CASE
          WHEN
            COUNT(*) > 1
          THEN
            SUM(S.quantidade)
          ELSE
            S.quantidade
        END
      )
      AS quantidade,
      $valorItem
      PL.nome AS nomeplano, 
      C.empresa,
      B.TUSS AS codTuss,
      PL.tipo_medicamento
      $codigoProprio
    FROM
      `saida` S
      INNER JOIN
        `clientes` C
        ON (S.idCliente = C.idClientes)
      INNER JOIN
        `catalogo` B
        ON (S.catalogo_id_escolhido = B.ID)
      INNER JOIN
        planosdesaude AS PL
        ON ( C.convenio = PL.id)
      INNER JOIN
        solicitacoes AS Sol
        ON (S.idSolicitacao = Sol.idSolicitacoes)
      INNER JOIN
        prescricoes AS P
        ON (Sol.idPrescricao = P.id)
        $leftTabelaCobrancaMedicamento
    WHERE
      C.idClientes = '{$paciente}'
      AND
      (
        P.inicio BETWEEN '{$inicio}' AND '{$fim}'
        OR P.fim BETWEEN '{$inicio}' AND '{$fim}'
      )
      AND S.quantidade > 0 
      AND
      (
        C.idClientes = S.idCliente
      )
      AND S.tipo = 0
      and ( S.catalogo_id_escolhido is not null and S.catalogo_id_escolhido <> 0 and S.catalogo_id_escolhido <> '')
      group by Paciente, catalogo_id
      
      UNION
      SELECT
        $itemTabelaCobranca
      S.NUMERO_TISS AS cod,
      S.CATALOGO_ID AS catalogo_id,
      $tabelaOrigem AS TABELA_ORIGEM,
      C.nome,
      S.tipo,
      'P' AS flag,
      S.idSaida,
      C.idClientes AS Paciente,
      C.convenio AS Plano,
      COALESCE(B.valorMedio, '0.00') AS custo,
      (
        CASE
          S.tipo
          WHEN
            '0'
          THEN
            'Medicamento'
          
        END
      )
      AS apresentacao,
      (
        CASE
          WHEN
            COUNT(*) > 1
          THEN
            SUM(S.quantidade)
          ELSE
            S.quantidade
        END
      )
      AS quantidade,
      $valorItem 
       PL.nome AS nomeplano, 
       C.empresa,
        B.TUSS AS codTuss,
         PL.tipo_medicamento
         $codigoProprio
    FROM
      `saida` S
      INNER JOIN
        `clientes` C
        ON (S.idCliente = C.idClientes)
      INNER JOIN
        `catalogo` B
        ON (S.CATALOGO_ID = B.ID)
      INNER JOIN
        planosdesaude AS PL
        ON ( C.convenio = PL.id)
      INNER JOIN
        solicitacoes AS Sol
        ON (S.idSolicitacao = Sol.idSolicitacoes)
      LEFT JOIN
        prescricoes AS presc
        ON (Sol.id_periodica_complemento = presc.id)
        $leftTabelaCobrancaMedicamento
    WHERE
      C.idClientes = '{$paciente}'
      AND IF
        (
            Sol.JUSTIFICATIVA_AVULSO_ID = 2,
            (
                presc.inicio BETWEEN '{$inicio}' AND '{$fim}'
                OR presc.fim BETWEEN '{$inicio}' AND '{$fim}'
            ),
            S.data BETWEEN '{$inicio}' AND '{$fim}'
        )
      AND S.quantidade > 0 {$refaturarmed}
      AND
      (
        C.idClientes = S.idCliente
      )
      AND S.tipo = 0
      AND Sol.idPrescricao = -1
      AND Sol.TIPO_SOLICITACAO = -1
      and ( S.catalogo_id_escolhido is null or S.catalogo_id_escolhido = 0 or S.catalogo_id_escolhido ='')

    GROUP BY
      Paciente, catalogo_id

      UNION

      SELECT
        $itemTabelaCobranca
      S.NUMERO_TISS AS cod,
      S.catalogo_id_escolhido AS catalogo_id,
      $tabelaOrigem AS TABELA_ORIGEM,
      C.nome,
      S.tipo,
      'P' AS flag,
      S.idSaida,
      C.idClientes AS Paciente,
      C.convenio AS Plano,
      COALESCE(
                ROUND(
                    SUM( S.quantidade * (( SELECT cat.valorMedio FROM catalogo AS cat WHERE cat.ID = S.CATALOGO_ID ))  )
                    / 
                    SUM( S.quantidade),
                    2
                  ), 
                  '0.00'  
              )
         AS custo,
      (
        CASE
          S.tipo
          WHEN
            '0'
          THEN
            'Medicamento'
          
        END
      )
      AS apresentacao,
      (
        CASE
          WHEN
            COUNT(*) > 1
          THEN
            SUM(S.quantidade)
          ELSE
            S.quantidade
        END
      )
      AS quantidade,
      $valorItem 
       PL.nome AS nomeplano, 
       C.empresa,
        B.TUSS AS codTuss,
         PL.tipo_medicamento
         $codigoProprio
    FROM
      `saida` S
      INNER JOIN
        `clientes` C
        ON (S.idCliente = C.idClientes)
      INNER JOIN
        `catalogo` B
        ON (S.catalogo_id_escolhido = B.ID)
      INNER JOIN
        planosdesaude AS PL
        ON ( C.convenio = PL.id)
      INNER JOIN
        solicitacoes AS Sol
        ON (S.idSolicitacao = Sol.idSolicitacoes)
      LEFT JOIN
        prescricoes AS presc
        ON (Sol.id_periodica_complemento = presc.id)
        $leftTabelaCobrancaMedicamento
    WHERE
      C.idClientes = '{$paciente}'
      AND IF
        (
            Sol.JUSTIFICATIVA_AVULSO_ID = 2,
            (
                presc.inicio BETWEEN '{$inicio}' AND '{$fim}'
                OR presc.fim BETWEEN '{$inicio}' AND '{$fim}'
            ),
            S.data BETWEEN '{$inicio}' AND '{$fim}'
        )
      AND S.quantidade > 0 {$refaturarmed}
      AND
      (
        C.idClientes = S.idCliente
      )
      AND S.tipo = 0
      AND Sol.idPrescricao = -1
      AND Sol.TIPO_SOLICITACAO = -1
      and ( S.catalogo_id_escolhido is not null and S.catalogo_id_escolhido <> 0 and S.catalogo_id_escolhido <> '')
    GROUP BY
      Paciente, catalogo_id
      UNION
    SELECT
    $itemTabelaCobranca
      S.NUMERO_TISS AS cod,
      S.CATALOGO_ID AS catalogo_id,
      $tabelaOrigem AS TABELA_ORIGEM,
      C.nome,
      S.tipo,
      'P' AS flag,
      S.idSaida,
      C.idClientes AS Paciente,
     C.convenio AS Plano,
      COALESCE(B.valorMedio, '0.00') AS custo,
      (
        CASE
          S.tipo
          WHEN
            '0'
            THEN
            'Medicamento'
          
        END
      )
      AS apresentacao,
      (
        CASE
          WHEN
            COUNT(*) > 1
          THEN
            SUM(S.quantidade)
          ELSE
            S.quantidade
        END
      )
      AS quantidade,
      $valorItem
       PL.nome AS nomeplano, 
       C.empresa, 
       B.TUSS AS codTuss,
        PL.tipo_medicamento
        $codigoProprio
    FROM
      `saida` S
      INNER JOIN
        `clientes` C
        ON (S.idCliente = C.idClientes)
      INNER JOIN
        `catalogo` B
        ON (S.CATALOGO_ID = B.ID)
      INNER JOIN
        planosdesaude AS PL
        ON ( C.convenio = PL.id)
      INNER JOIN
        solicitacoes AS Sol
        ON (S.idSolicitacao = Sol.idSolicitacoes)
      INNER JOIN
        relatorio_aditivo_enf AS P
        ON (Sol.relatorio_aditivo_enf_id = P.ID)
        $leftTabelaCobrancaMedicamento
    WHERE
      C.idClientes = '{$paciente}'
      AND
      (
        P.INICIO BETWEEN '{$inicio}' AND '{$fim}'
        OR P.FIM BETWEEN '{$inicio}' AND '{$fim}'
      )
      AND S.quantidade > 0 {$refaturarmed}
      AND
      (
        C.idClientes = S.idCliente
      )
      AND S.tipo = 0
      and ( S.catalogo_id_escolhido is null or S.catalogo_id_escolhido = 0 or S.catalogo_id_escolhido ='')

    GROUP BY
      Paciente, catalogo_id
      UNION
    SELECT
    $itemTabelaCobranca
      B.NUMERO_TISS AS cod,
      S.catalogo_id_escolhido AS catalogo_id,
      $tabelaOrigem AS TABELA_ORIGEM,
      C.nome,
      S.tipo,
      'P' AS flag,
      S.idSaida,
      C.idClientes AS Paciente,
     C.convenio AS Plano,
     COALESCE(
                ROUND(
                    SUM( S.quantidade * (( SELECT cat.valorMedio FROM catalogo AS cat WHERE cat.ID = S.CATALOGO_ID ))  )
                    / 
                    SUM( S.quantidade),
                    2
                  ), 
                  '0.00'  
              )
         AS custo,
      (
        CASE
          S.tipo
          WHEN
            '0'
            THEN
            'Medicamento'
          
        END
      )
      AS apresentacao,
      (
        CASE
          WHEN
            COUNT(*) > 1
          THEN
            SUM(S.quantidade)
          ELSE
            S.quantidade
        END
      )
      AS quantidade,
      $valorItem
       PL.nome AS nomeplano, 
       C.empresa, 
       B.TUSS AS codTuss,
        PL.tipo_medicamento
        $codigoProprio
    FROM
      `saida` S
      INNER JOIN
        `clientes` C
        ON (S.idCliente = C.idClientes)
      INNER JOIN
        `catalogo` B
        ON (S.catalogo_id_escolhido = B.ID)
      INNER JOIN
        planosdesaude AS PL
        ON ( C.convenio = PL.id)
      INNER JOIN
        solicitacoes AS Sol
        ON (S.idSolicitacao = Sol.idSolicitacoes)
      INNER JOIN
        relatorio_aditivo_enf AS P
        ON (Sol.relatorio_aditivo_enf_id = P.ID)
        $leftTabelaCobrancaMedicamento
    WHERE
      C.idClientes = '{$paciente}'
      AND
      (
        P.INICIO BETWEEN '{$inicio}' AND '{$fim}'
        OR P.FIM BETWEEN '{$inicio}' AND '{$fim}'
      )
      AND S.quantidade > 0 {$refaturarmed}
      AND
      (
        C.idClientes = S.idCliente
      )
      AND S.tipo = 0
      and ( S.catalogo_id_escolhido is not null and S.catalogo_id_escolhido <> 0 and S.catalogo_id_escolhido <> '')

    GROUP BY
      Paciente, catalogo_id
          Union
          SELECT
            IF(B.DESC_MATERIAIS_FATURAR IS NULL, concat(B.principio, ' - ', B.apresentacao), B.DESC_MATERIAIS_FATURAR) AS principio,
            S.NUMERO_TISS AS cod,
            S.CATALOGO_ID AS catalogo_id,
            'catalogo' AS TABELA_ORIGEM,
            C.nome,
            S.tipo,
            'P' AS flag,
            S.idSaida,
            C.idClientes AS Paciente,
            C.convenio  AS Plano,
            COALESCE(B.valorMedio, '0.00') AS custo,
            (
              CASE
                S.tipo
                
                WHEN
                  '1'
                THEN
                  'Material'
                WHEN
                  '3'
                THEN
                  'Dieta'
              END
            )
            AS apresentacao,
            (
              CASE
                WHEN
                  COUNT(*) > 1
                THEN
                  SUM(S.quantidade)
                ELSE
                  S.quantidade
              END
            )
            AS quantidade, 
            COALESCE(B.VALOR_VENDA, '0.00') AS valor,
             PL.nome AS nomeplano,
              C.empresa,
               B.TUSS AS codTuss, 
               PL.tipo_medicamento
               , 0 AS codigo_proprio
          FROM
            `saida` S
            INNER JOIN
              `clientes` C
              ON (S.idCliente = C.idClientes)
            INNER JOIN
              `catalogo` B
              ON (S.CATALOGO_ID = B.ID)
            INNER JOIN
              planosdesaude AS PL
              ON ( C.convenio  = PL.id)
            INNER JOIN
              solicitacoes AS Sol
              ON (S.idSolicitacao = Sol.idSolicitacoes)
            LEFT JOIN
              prescricoes AS presc
              ON (Sol.id_periodica_complemento = presc.id)
          WHERE
            C.idClientes = '{$paciente}'
            AND IF
              (
                  Sol.JUSTIFICATIVA_AVULSO_ID = 2,
                  (
                      presc.inicio BETWEEN '{$inicio}' AND '{$fim}'
                      OR presc.fim BETWEEN '{$inicio}' AND '{$fim}'
                  ),
                  S.data BETWEEN '{$inicio}' AND '{$fim}'
              )
            AND S.quantidade > 0 {$refaturarmed}
            AND
            (
              C.idClientes = S.idCliente
            )
            AND S.tipo IN
            (
               1, 3
            )
            AND Sol.idPrescricao = -1
            AND Sol.TIPO_SOLICITACAO = -1
          GROUP BY
            Paciente, catalogo_id
          UNION
          SELECT
            IF(B.DESC_MATERIAIS_FATURAR IS NULL, concat(B.principio, ' - ', B.apresentacao), B.DESC_MATERIAIS_FATURAR) AS principio,
            S.NUMERO_TISS AS cod,
            S.CATALOGO_ID AS catalogo_id,
            'catalogo' AS TABELA_ORIGEM,
            C.nome,
            S.tipo,
            'P' AS flag,
            S.idSaida,
            C.idClientes AS Paciente,
            C.convenio  AS Plano,
            COALESCE(B.valorMedio, '0.00') AS custo,
            (
              CASE
                S.tipo
                
                WHEN
                  '1'
                THEN
                  'Material'
                WHEN
                  '3'
                THEN
                  'Dieta'
              END
            )
            AS apresentacao,
            (
              CASE
                WHEN
                  COUNT(*) > 1
                THEN
                  SUM(S.quantidade)
                ELSE
                  S.quantidade
              END
            )
            AS quantidade, 
            COALESCE(B.VALOR_VENDA, '0.00') AS valor, 
            PL.nome AS nomeplano, 
            C.empresa, 
            B.TUSS AS codTuss, 
            PL.tipo_medicamento
            , 0 AS codigo_proprio
          FROM
            `saida` S
            INNER JOIN
              `clientes` C
              ON (S.idCliente = C.idClientes)
            INNER JOIN
              `catalogo` B
              ON (S.CATALOGO_ID = B.ID)
            INNER JOIN
              planosdesaude AS PL
              ON ( C.convenio  = PL.id)
            INNER JOIN
              solicitacoes AS Sol
              ON (S.idSolicitacao = Sol.idSolicitacoes)
            INNER JOIN
              relatorio_aditivo_enf AS P
              ON (Sol.relatorio_aditivo_enf_id = P.ID)
          WHERE
            C.idClientes = '{$paciente}'
            AND
            (
              P.INICIO BETWEEN '{$inicio}' AND '{$fim}'
              OR P.FIM BETWEEN '{$inicio}' AND '{$fim}'
            )
            AND S.quantidade > 0 {$refaturarmed}
            AND
            (
              C.idClientes = S.idCliente
            )
            AND S.tipo IN
            (
               1, 3
            )
          GROUP BY
            Paciente, catalogo_id
          UNION
          SELECT
            IF(B.DESC_MATERIAIS_FATURAR IS NULL, concat(B.principio, ' - ', B.apresentacao), B.DESC_MATERIAIS_FATURAR) AS principio,
            S.NUMERO_TISS AS cod,
            S.CATALOGO_ID AS catalogo_id,
            'catalogo' AS TABELA_ORIGEM,
            C.nome,
            S.tipo,
            'P' AS flag,
            S.idSaida,
            C.idClientes AS Paciente,
            C.convenio  AS Plano,
            COALESCE(B.valorMedio, '0.00') AS custo,
            (
              CASE
                S.tipo
                
                WHEN
                  '1'
                THEN
                  'Material'
                WHEN
                  '3'
                THEN
                  'Dieta'
              END
            )
            AS apresentacao,
            (
              CASE
                WHEN
                  COUNT(*) > 1
                THEN
                  SUM(S.quantidade)
                ELSE
                  S.quantidade
              END
            )
            AS quantidade, 
            COALESCE(B.VALOR_VENDA, '0.00') AS valor, 
            PL.nome AS nomeplano, 
            C.empresa, 
            B.TUSS AS codTuss,
             PL.tipo_medicamento
             , 0 AS codigo_proprio
          FROM
            `saida` S
            INNER JOIN
              `clientes` C
              ON (S.idCliente = C.idClientes)
            INNER JOIN
              `catalogo` B
              ON (S.CATALOGO_ID = B.ID)
            INNER JOIN
              planosdesaude AS PL
              ON ( C.convenio  = PL.id)
            INNER JOIN
              solicitacoes AS Sol
              ON (S.idSolicitacao = Sol.idSolicitacoes)
            INNER JOIN
              prescricoes AS P
              ON (Sol.idPrescricao = P.id)
          WHERE
            C.idClientes = '{$paciente}'
            AND
            (
              P.inicio BETWEEN '{$inicio}' AND '{$fim}'
              OR P.fim BETWEEN '{$inicio}' AND '{$fim}'
            )
            AND S.quantidade > 0 {$refaturarmed}
            AND
            (
              C.idClientes = S.idCliente
            )
            AND S.tipo IN
            (
               1, 3
            )
          GROUP BY
            Paciente, catalogo_id
            
        
        
        


)  AS principal
GROUP BY
  Paciente, catalogo_id
  UNION
SELECT
  principal.principio,
  principal.cod,
  principal.catalogo_id,
  principal.TABELA_ORIGEM,
  principal.nome,
  principal.tipo,
  principal.flag,
  principal.idSolicitacoes,
  principal.Paciente,
  principal.Plano,
  principal.custo,
  principal.apresentacao,
  SUM(principal.quantidade) AS quantidade,
  principal.valor,
  principal.nomeplano,
  principal.empresa,
  principal.codTuss,
  principal.tipo_medicamento,
  principal.codigo_proprio
FROM
  (
SELECT

	IF (
					BC.DESC_COBRANCA_PLANO IS NULL,
					B.item,
					BC.DESC_COBRANCA_PLANO
	) AS principio,

	IF (
					BC.DESC_COBRANCA_PLANO IS NULL,
					B.id,
					BC.COD_COBRANCA_PLANO
	) AS cod,

	IF (
					BC.DESC_COBRANCA_PLANO IS NULL,
					B.id,
					BC.id
	) AS catalogo_id,

	IF (
					BC.DESC_COBRANCA_PLANO IS NULL,
					'cobrancaplano',
					'valorescobranca'
	) AS TABELA_ORIGEM,
	 C.nome,
	 2 AS tipo,
	 'S' AS flag,
	 P.ID AS idSolicitacoes,
	 C.idClientes AS Paciente,
	 C.convenio AS Plano,
	 COALESCE (
					CS.CUSTO,
					'0.00'
	) AS custo,
	 ('Servi&ccedil;os') AS apresentacao,
	 
	 (
SUM(
CASE
WHEN
ESP.id IN
(
2,
3,
4,
5
)
THEN
IF ( ( '{$inicio}' <= P.INICIO
AND '{$fim}' >= P.INICIO )
AND
(
'{$inicio}' <= P.FIM
AND '{$fim}' >= P.FIM
)
,
(
DATEDIFF(P.FIM, P.INICIO) + 1
)
, IF ( ( '{$inicio}' <= P.INICIO
AND '{$fim}' >= P.INICIO )
AND
(
'{$fim}' <= P.FIM
)
,
(
DATEDIFF('{$fim}', P.INICIO) + 1
)
, IF ( ('{$inicio}' > P.INICIO)
AND
(
'{$fim}' >= P.FIM
AND '{$inicio}' <= P.FIM
)
,
(
DATEDIFF(P.FIM, '{$inicio}') + 1
)
, '1' ) ) )
ELSE
IF ( ( '{$inicio}' <= P.INICIO
AND '{$fim}' >= P.INICIO )
AND
(
'{$inicio}' <= P.FIM
AND '{$fim}' >= P.FIM
)
, IF ( S.frequencia IN
(
2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
)
,
(
DATEDIFF(P.FIM, P.INICIO) + 1
)
* F.qtd, CEILING( (DATEDIFF(P.FIM, P.INICIO) + 1) / 7 ) * F.SEMANA ), IF ( ( '{$inicio}' <= P.INICIO
AND '{$fim}' >= P.INICIO )
AND
(
'{$fim}' <= P.FIM
)
, IF ( S.frequencia IN
(
2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
)
,
(
DATEDIFF('{$fim}', P.INICIO) + 1
)
* F.qtd, CEILING( ( DATEDIFF('{$fim}', P.INICIO) / 7 ) + 1 ) * F.SEMANA ), IF ( ('{$inicio}' > P.INICIO)
AND
(
'{$fim}' >= P.FIM
AND '{$inicio}' <= P.FIM
)
, IF ( S.frequencia IN
(
2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
)
, (DATEDIFF(P.FIM, '{$inicio}')+1) * F.qtd, CEILING( ( DATEDIFF(P.FIM, '{$inicio}') / 7 ) + 1 ) * F.SEMANA ), '1' ) ) )
END
)
)
AS quantidade,
	 COALESCE (BC.valor, '0.00') AS valor,
	 PL.nome AS nomeplano,
	 C.empresa, 
	 '' AS codTuss,
	 PL.tipo_medicamento, 
   0 AS codigo_proprio
	
	FROM
					`solicitacao_rel_aditivo_enf` S
	LEFT JOIN `frequencia` AS F ON (S.frequencia = F.id)
	INNER JOIN relatorio_aditivo_enf P ON (S.`relatorio_enf_aditivo_id` = P.ID)
	INNER JOIN `clientes` C ON (P.PACIENTE_ID = C.idClientes)
	INNER JOIN cuidadosespeciais AS ESP ON (S.CATALOGO_ID = ESP.id)
	INNER JOIN `cobrancaplanos` B ON (ESP.COBRANCAPLANO_ID = B.id)
	LEFT JOIN custo_servicos_plano_ur CS ON (
					CS.COBRANCAPLANO_ID = B.id
					AND CS.UR = C.empresa
					AND CS.STATUS = 'A'
	)
	LEFT JOIN `valorescobranca` BC ON (
					B.id = BC.idCobranca
					AND BC.idPlano = C.convenio and BC.excluido_por is null 
	)
	INNER JOIN planosdesaude AS PL ON (C.convenio = PL.id)
	WHERE
		 C.idClientes = '{$paciente}'
  AND
  (
( P.INICIO BETWEEN '{$inicio}' AND '{$fim}' )
    OR
    (
      P.FIM BETWEEN '{$inicio}' AND '{$fim}'
    )
  )
	AND S.tipo IN (4)
	AND S.CATALOGO_ID NOT IN (17, 18, 21, 24)
	AND BC.excluido_por is NULL
	GROUP BY
	Paciente,
	B.id,
	BC.id
	UNION
SELECT
  IF(BC.DESC_COBRANCA_PLANO IS NULL, B.item, BC.DESC_COBRANCA_PLANO)AS principio,
  IF(BC.DESC_COBRANCA_PLANO IS NULL, B.id, BC.COD_COBRANCA_PLANO) AS cod,
  IF(BC.DESC_COBRANCA_PLANO IS NULL, B.id, BC.id) AS catalogo_id,
  IF(BC.DESC_COBRANCA_PLANO IS NULL, 'cobrancaplano', 'valorescobranca') AS TABELA_ORIGEM,
  C.nome,
  S.tipo,
  'S' AS flag,
  S.idPrescricao AS idSolicitacoes,
  C.idClientes AS Paciente,
  C.convenio AS Plano,
  COALESCE(CS.CUSTO, '0.00') AS custo,
  (
    'Servi&ccedil;os'
  )
  AS apresentacao,
  (
    SUM(
    CASE
      WHEN
        ESP.id IN
        (
          2,
          3,
          4,
          5
        )
      THEN
        IF(('{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio)
        AND
        (
          '{$inicio}' <= S.fim
          AND '{$fim}' >= S.fim
        )
,
        (
          DATEDIFF(S.fim, S.inicio) + 1
        )
, IF(('{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio)
        AND
        (
          '{$fim}' <= S.fim
        )
,
        (
          DATEDIFF('{$fim}', S.inicio) + 1
        )
, IF(('{$inicio}' > S.inicio)
        AND
        (
          '{$fim}' >= S.fim
          AND '{$inicio}' <= S.fim
        )
,
        (
          DATEDIFF(S.fim, '{$inicio}') + 1
        )
, '1')))
      ELSE
        IF(('{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio)
        AND
        (
          '{$inicio}' <= S.fim
          AND '{$fim}' >= S.fim
        )
, IF(S.frequencia IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
,
        (
          DATEDIFF(S.fim, S.inicio) + 1
        )
        *F.qtd, CEILING((DATEDIFF(S.fim, S.inicio) + 1) / 7)*F.SEMANA), IF(('{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio)
        AND
        (
          '{$fim}' <= S.fim
        )
, IF(S.frequencia IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
,
        (
          DATEDIFF('{$fim}', S.inicio) + 1
        )
        *F.qtd, CEILING((DATEDIFF('{$fim}', S.inicio) / 7) + 1)*F.SEMANA), IF(('{$inicio}' > S.inicio)
        AND
        (
          '{$fim}' >= S.fim
          AND '{$inicio}' <= S.fim
        )
, IF(S.frequencia IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
, (DATEDIFF(S.fim, '{$inicio}')+1) * F.qtd, CEILING((DATEDIFF(S.fim, '{$inicio}') / 7) + 1)*F.SEMANA), '1')))
    END
)
  )
  AS quantidade, 
  COALESCE(BC.valor, '0.00') AS valor,
   PL.nome AS nomeplano,
    C.empresa, 
    '' AS codTuss, 
    PL.tipo_medicamento, 
    0 AS codigo_proprio
FROM
  `itens_prescricao` S
  LEFT JOIN
    `frequencia` AS F
    ON (S.frequencia = F.id)
  INNER JOIN
    prescricoes P
    ON (S.`idPrescricao` = P.id)
  INNER JOIN
    `clientes` C
    ON (P.paciente = C.idClientes)
  INNER JOIN
    cuidadosespeciais AS ESP
    ON (S.CATALOGO_ID = ESP.id)
  INNER JOIN
    `cobrancaplanos` B
    ON (ESP.COBRANCAPLANO_ID = B.id)
  LEFT JOIN
    custo_servicos_plano_ur CS
    ON (CS.COBRANCAPLANO_ID = B.id
    AND CS.UR = C.empresa AND CS.STATUS = 'A')
  LEFT JOIN
    `valorescobranca` BC
    ON (B.id = BC.idCobranca
    AND BC.idPlano = C.convenio
    AND BC.excluido_por IS NULL)
  INNER JOIN
    planosdesaude AS PL
    ON ( C.convenio = PL.id)
WHERE
  C.idClientes = '{$paciente}'
  AND
  (
    P.inicio BETWEEN '{$inicio}' AND '{$fim}'
    OR P.fim BETWEEN '{$inicio}' AND '{$fim}'
  )
  AND S.tipo IN
  (
    2
  )
  AND P.Carater NOT IN
  (
    4, 7
  )
  {$refaturarserv}
  AND S.CATALOGO_ID NOT IN
  (
    17, 18, 21, 24
  )
  AND P.DESATIVADA_POR = 0
GROUP BY
  Paciente, B.id, BC.id
UNION
SELECT
  B.item AS principio,
  B.id AS cod,
  B.id AS catalogo_id,
  'cobrancaplano' AS TABELA_ORIGEM,
  C.nome,
  2 AS tipo,
  'S' AS flag,
  remocoes.id AS idSolicitacoes,
  C.idClientes AS Paciente,
  C.convenio AS Plano,
  COALESCE(remocoes.outros_custos + remocoes.outras_despesas + remocoes.valor_medico + remocoes.valor_tecnico + remocoes.valor_condutor + remocoes.valor_enfermeiro, '0.00') AS custo,
  (
    'Servi&ccedil;os'
  )
  AS apresentacao,
  1 AS quantidade,
  COALESCE(remocoes.valor_remocao, '0.00') AS valor,
  planosdesaude.nome AS nomeplano,
  C.empresa,
  '' AS codTuss,
  planosdesaude.tipo_medicamento, 
  0 AS codigo_proprio
FROM
  remocoes
  INNER JOIN
    clientes AS C
    ON remocoes.paciente_id = C.idClientes
  INNER JOIN
    empresas
    ON remocoes.empresa_id = empresas.id
  INNER JOIN
    planosdesaude
    ON remocoes.convenio_id = planosdesaude.id
  INNER JOIN
    cobrancaplanos AS B
    ON remocoes.cobrancaplano_id = B.id
WHERE
  remocoes.data_saida BETWEEN '{$inicio}' AND '{$fim}'
  AND remocoes.paciente_id = $paciente
  AND remocoes.modo_cobranca = 'F'
  AND remocoes.convenio_id = C.convenio
  AND remocoes.finalized_by IS NOT NULL
  AND remocoes.canceled_by IS NULL
  AND remocoes.tabela_origem_item_fatura <> 'valorescobranca'
GROUP BY
  Paciente,
  B.id
UNION
SELECT
  valorescobranca.DESC_COBRANCA_PLANO AS principio,
  valorescobranca.COD_COBRANCA_PLANO AS cod,
  valorescobranca.id AS catalogo_id,
  'valorescobranca' AS TABELA_ORIGEM,
  C.nome,
  2 AS tipo,
  'S' AS flag,
  remocoes.id AS idSolicitacoes,
  C.idClientes AS Paciente,
  C.convenio AS Plano,
  COALESCE(remocoes.outros_custos + remocoes.outras_despesas + remocoes.valor_medico + remocoes.valor_tecnico + remocoes.valor_condutor + remocoes.valor_enfermeiro, '0.00') AS custo,
  (
    'Servi&ccedil;os'
  )
  AS apresentacao,
  1 AS quantidade,
  COALESCE(remocoes.valor_remocao, '0.00') AS valor,
  planosdesaude.nome AS nomeplano,
  C.empresa,
  '' AS codTuss,
  planosdesaude.tipo_medicamento, 
  0 AS codigo_proprio
FROM
  remocoes
  INNER JOIN
    clientes AS C
    ON remocoes.paciente_id = C.idClientes
  INNER JOIN
    empresas
    ON remocoes.empresa_id = empresas.id
  INNER JOIN
    planosdesaude
    ON remocoes.convenio_id = planosdesaude.id
  INNER JOIN
    valorescobranca
    ON remocoes.valorescobranca_id = valorescobranca.id
WHERE
  remocoes.data_saida BETWEEN '{$inicio}' AND '{$fim}'
  AND remocoes.paciente_id = $paciente
  AND remocoes.modo_cobranca = 'F'
  AND remocoes.convenio_id = C.convenio
  AND remocoes.finalized_by IS NOT NULL
  AND remocoes.canceled_by IS NULL
  AND remocoes.tabela_origem_item_fatura = 'valorescobranca'
GROUP BY
  Paciente,
  valorescobranca.id
	UNION
SELECT
  IF ( BC.DESC_COBRANCA_PLANO IS NULL, B.item, BC.DESC_COBRANCA_PLANO ) AS principio,
  IF ( BC.DESC_COBRANCA_PLANO IS NULL, B.id, BC.COD_COBRANCA_PLANO ) AS cod,
  IF ( BC.DESC_COBRANCA_PLANO IS NULL, B.id, BC.id ) AS catalogo_id,
  IF ( BC.DESC_COBRANCA_PLANO IS NULL, 'cobrancaplano', 'valorescobranca' ) AS TABELA_ORIGEM,
  C.nome,
  2 AS tipo,
  'S' AS flag,
  S.prescricao_enf_id AS idSolicitacoes,
  C.idClientes AS Paciente,
  C.convenio AS Plano,
  COALESCE(CS.CUSTO, '0.00') AS custo,
  (
    'Servi&ccedil;os'
  )
  AS apresentacao,
  (
    SUM(
    CASE
      WHEN
        ESP.id IN
        (
          2,
          3,
          4,
          5
        )
      THEN
        IF ( ( '{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio )
        AND
        (
          '{$inicio}' <= S.fim
          AND '{$fim}' >= S.fim
        )
,
        (
          DATEDIFF(S.fim, S.inicio) + 1
        )
, IF ( ( '{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio )
        AND
        (
          '{$fim}' <= S.fim
        )
,
        (
          DATEDIFF('{$fim}', S.inicio) + 1
        )
, IF ( ('{$inicio}' > S.inicio)
        AND
        (
          '{$fim}' >= S.fim
          AND '{$inicio}' <= S.fim
        )
,
        (
          DATEDIFF(S.fim, '{$inicio}') + 1
        )
, '1' ) ) )
      ELSE
        IF ( ( '{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio )
        AND
        (
          '{$inicio}' <= S.fim
          AND '{$fim}' >= S.fim
        )
, IF ( S.frequencia_id IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
,
        (
          DATEDIFF(S.fim, S.inicio) + 1
        )
        * F.qtd, CEILING( (DATEDIFF(S.fim, S.inicio) + 1) / 7 ) * F.SEMANA ), IF ( ( '{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio )
        AND
        (
          '{$fim}' <= S.fim
        )
, IF ( S.frequencia_id IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
,
        (
          DATEDIFF('{$fim}', S.inicio) + 1
        )
        * F.qtd, CEILING( ( DATEDIFF('{$fim}', S.inicio) / 7 ) + 1 ) * F.SEMANA ), IF ( ('{$inicio}' > S.inicio)
        AND
        (
          '{$fim}' >= S.fim
          AND '{$inicio}' <= S.fim
        )
, IF ( S.frequencia_id IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
, (DATEDIFF(S.fim, '{$inicio}')+1) * F.qtd, CEILING( ( DATEDIFF(S.fim, '{$inicio}') / 7 ) + 1 ) * F.SEMANA ), '1' ) ) )
    END
)
  )
  AS quantidade, 
  COALESCE (BC.valor, '0.00') AS valor,
   PL.nome AS nomeplano, 
   C.empresa, '' AS codTuss,
    PL.tipo_medicamento, 
    0 AS codigo_proprio
FROM
  `prescricao_enf_itens_prescritos` AS S
  LEFT JOIN
    `frequencia` AS F
    ON (S.frequencia_id = F.id)
  INNER JOIN
    prescricao_enf P
    ON (S.prescricao_enf_id = P.id)
  INNER JOIN
    `clientes` C
    ON (P.cliente_id = C.idClientes)
  INNER JOIN
    prescricao_enf_cuidado AS ESP
    ON ( S.presc_enf_cuidados_id = ESP.id)
  INNER JOIN
    cuidadosespeciais AS CUI
    ON ESP.cuidado_especiais_id = CUI.id
  INNER JOIN
    `cobrancaplanos` B
    ON (CUI.COBRANCAPLANO_ID = B.id)
  LEFT JOIN
    `valorescobranca` BC
    ON ( B.id = BC.idCobranca
    AND BC.idPlano = C.convenio
    AND BC.excluido_por IS NULL )
  LEFT JOIN
    custo_servicos_plano_ur CS
    ON (CS.COBRANCAPLANO_ID = B.id
    AND CS.UR = C.empresa AND CS.STATUS = 'A')
  INNER JOIN
    planosdesaude AS PL
    ON ( C.convenio = PL.id)
WHERE
  C.idClientes = '{$paciente}'
  AND
  (
( P.inicio BETWEEN '{$inicio}' AND '{$fim}' )
    OR
    (
      P.fim BETWEEN '{$inicio}' AND '{$fim}'
    )
  )
  AND P.cancelado_por IS NULL
  AND ESP.presc_enf_grupo_id = 7
  AND P.carater IN
  (
    8, 9
  )
GROUP BY
  Paciente, B.id, BC.id
 )
	 AS principal
GROUP BY
  Paciente, TABELA_ORIGEM, catalogo_id
	

UNION
SELECT DISTINCT
  IF(V.DESC_COBRANCA_PLANO IS NULL, CO.item, V.DESC_COBRANCA_PLANO)AS principio,
  IF(V.DESC_COBRANCA_PLANO IS NULL, CO.id, V.COD_COBRANCA_PLANO) AS cod,
  IF(V.DESC_COBRANCA_PLANO IS NULL, CO.id, V.id) AS catalogo_id,
  IF(V.DESC_COBRANCA_PLANO IS NULL, 'cobrancaplano', 'valorescobranca') AS TABELA_ORIGEM,
  C.nome,
  5 AS tipo,
  'N' AS flag,
  0 AS idSolicitacoes,
  C.idClientes AS Paciente,
  C.convenio AS Plano,
  COALESCE(CS.CUSTO, '0.00') AS custo,
  (
    'Equipamentos'
  )
  AS apresentacao,
  (SUM(
  IF(
  ('{$inicio}' <= E.DATA_INICIO AND '{$fim}' >= E.DATA_INICIO) 
  AND 
  ('{$inicio}' <= E.DATA_FIM AND '{$fim}' >= E.DATA_FIM),
  (DATEDIFF(E.DATA_FIM, E.DATA_INICIO) + 1),
  IF(
  ('{$inicio}' <= E.DATA_INICIO AND '{$fim}' >= E.DATA_INICIO) 
  AND ('{$fim}' <= E.DATA_FIM), 
  (DATEDIFF('{$fim}', E.DATA_INICIO) + 1),
  IF(
  ('{$inicio}' > E.DATA_INICIO) AND ('{$fim}' >= E.DATA_FIM AND '{$inicio}' <= E.DATA_FIM),
  (DATEDIFF(E.DATA_FIM, '{$inicio}') + 1),
  IF(('{$inicio}' > E.DATA_INICIO) AND ('{$fim}' >= E.DATA_FIM AND '{$inicio}' >= E.DATA_FIM) 
  AND (E.DATA_FIM = '0000-00-00 00:00:00' OR E.DATA_FIM LIKE '%0000-00-00%'),
  (DATEDIFF('{$fim}', '{$inicio}') + 1),
  '1'
  )
  )
  )
  )
  )
  ) AS quantidade,
  COALESCE(V.valor, '0.00') AS valor,
  PL.nome AS nomeplano,
  C.empresa,
  '' AS codTuss,
  PL.tipo_medicamento,
   0 AS codigo_proprio
FROM
  clientes C
  INNER JOIN
    equipamentosativos E
    ON (C.idClientes = E.`PACIENTE_ID`)
  INNER JOIN
    cobrancaplanos CO
    ON (E.`COBRANCA_PLANOS_ID` = CO.`id`)
  LEFT JOIN
    custo_servicos_plano_ur CS
    ON (CS.COBRANCAPLANO_ID = CO.id
    AND CS.UR = C.empresa AND CS.STATUS = 'A')
  LEFT JOIN
    valorescobranca V
    ON (CO.`id` = V.`idCobranca`
    AND C.convenio = V.`idPlano`
    AND V.excluido_por IS NULL)
  INNER JOIN
    planosdesaude AS PL
    ON ( C.convenio = PL.id)
WHERE
  C.idClientes = '{$paciente}'
  AND E.PRESCRICAO_AVALIACAO != 'S' {$refaturareqp}
GROUP BY
  Paciente,
  CO.id,
  V.id

ORDER BY
  Paciente, tipo, principio
      

    ";

        echo $sql;
    }
    public static function getItensSolicitacaoAvulsa($paciente, $inicio, $fim, $operadora)
    {
        $tabelaContrato = $this->getTabelaMedicamento($operadora);
        $itemTabelaCobranca = "IF(B.DESC_MATERIAIS_FATURAR IS NULL, concat(B.principio, ' - ', B.apresentacao), B.DESC_MATERIAIS_FATURAR) AS principio,";
        $valorItem = "COALESCE(B.VALOR_VENDA, '0.00') AS valor,";
        $codigoProprio = ", 0 AS codigo_proprio";
        $tabelaOrigem = "'catalogo'";

        if ($tabelaContrato != false) {
            $leftTabelaCobrancaMedicamento = "left join tabelas_cobrancas_medicamentos as ctm on 
                                        (B.ID = ctm.catalogo_id and ctm.tabelas_cobrancas_id = $tabelaContrato )";
            $itemTabelaCobranca = "COALESCE(ctm.item,IF(B.DESC_MATERIAIS_FATURAR IS NULL, concat(B.principio, ' - ', B.apresentacao), B.DESC_MATERIAIS_FATURAR)) AS principio,";
            $valorItem = "COALESCE(ctm.valor,COALESCE(B.VALOR_VENDA, '0.00')) AS valor,";
            $codigoProprio = ", ctm.codigo_fatura AS codigo_proprio";
            $tabelaOrigem =  "if(ctm.item is null,'catalogo','tabelas_cobrancas_medicamentos')";
        }
        $sql ="
     
        SELECT      
        $itemTabelaCobranca
        S.NUMERO_TISS AS cod,
        S.CATALOGO_ID AS catalogo_id,
        $tabelaOrigem AS TABELA_ORIGEM,
        C.nome,
        S.tipo,
        'P' AS flag,
        S.idSaida,
        C.idClientes AS Paciente,
        C.convenio AS Plano,      
        COALESCE(valorMedio , '0.00') AS custo,
        (
          CASE
            S.tipo
            WHEN
              '0'
            THEN
              'Medicamento'
           
          END
        )
        AS apresentacao,
        (
          CASE
            WHEN
              COUNT(*) > 1
            THEN
              SUM(S.quantidade)
            ELSE
              S.quantidade
          END
        )
        AS quantidade,
        $valorItem
        PL.nome AS nomeplano, 
        C.empresa,
        B.TUSS AS codTuss,
        PL.tipo_medicamento
        $codigoProprio
      FROM
        `saida` S
        INNER JOIN
          `clientes` C
          ON (S.idCliente = C.idClientes)
        INNER JOIN
          `catalogo` B
          ON (S.CATALOGO_ID = B.ID)
        INNER JOIN
          planosdesaude AS PL
          ON ( C.convenio = PL.id)
        INNER JOIN
          solicitacoes AS Sol
          ON (S.idSolicitacao = Sol.idSolicitacoes)
        INNER JOIN
          prescricoes AS P
          ON (Sol.idPrescricao = P.id)
          $leftTabelaCobrancaMedicamento
      WHERE
        C.idClientes = '{$paciente}'
        AND
        (
          P.inicio BETWEEN '{$inicio}' AND '{$fim}'
          OR P.fim BETWEEN '{$inicio}' AND '{$fim}'
        )
        AND S.quantidade > 0 
        AND
        (
          C.idClientes = S.idCliente
        )
        AND S.tipo IN
        (
          0, 1, 3
        )
        and ( S.catalogo_id_escolhido is null or S.catalogo_id_escolhido = 0 or S.catalogo_id_escolhido ='')
        group by Paciente, CATALOGO_ID
        Union
        SELECT      
        $itemTabelaCobranca
        S.NUMERO_TISS AS cod,
        B.ID AS catalogo_id,
        $tabelaOrigem AS TABELA_ORIGEM,
        C.nome,
        S.tipo,
        'P' AS flag,
        S.idSaida,
        C.idClientes AS Paciente,
        C.convenio AS Plano,  
        COALESCE(
                  ROUND(
                      SUM( S.quantidade * (( SELECT cat.valorMedio FROM catalogo AS cat WHERE cat.ID = S.CATALOGO_ID ))  )
                      / 
                      SUM( S.quantidade),
                      2
                    ), 
                    '0.00'  
                )
           AS custo,    
        (
          CASE
            S.tipo
            WHEN
              '0'
            THEN
              'Medicamento'
            
          END
        )
        AS apresentacao,
        (
          CASE
            WHEN
              COUNT(*) > 1
            THEN
              SUM(S.quantidade)
            ELSE
              S.quantidade
          END
        )
        AS quantidade,
        $valorItem
        PL.nome AS nomeplano, 
        C.empresa,
        B.TUSS AS codTuss,
        PL.tipo_medicamento
        $codigoProprio
      FROM
        `saida` S
        INNER JOIN
          `clientes` C
          ON (S.idCliente = C.idClientes)
        INNER JOIN
          `catalogo` B
          ON (S.catalogo_id_escolhido = B.ID)
        INNER JOIN
          planosdesaude AS PL
          ON ( C.convenio = PL.id)
        INNER JOIN
          solicitacoes AS Sol
          ON (S.idSolicitacao = Sol.idSolicitacoes)
        INNER JOIN
          prescricoes AS P
          ON (Sol.idPrescricao = P.id)
          $leftTabelaCobrancaMedicamento
      WHERE
        C.idClientes = '{$paciente}'
        AND
        (
          P.inicio BETWEEN '{$inicio}' AND '{$fim}'
          OR P.fim BETWEEN '{$inicio}' AND '{$fim}'
        )
        AND S.quantidade > 0 
        AND
        (
          C.idClientes = S.idCliente
        )
        AND S.tipo = 0
        and ( S.catalogo_id_escolhido is not null and S.catalogo_id_escolhido <> 0 and S.catalogo_id_escolhido <> '')
        group by Paciente, catalogo_id
        
        UNION
        SELECT
          $itemTabelaCobranca
        S.NUMERO_TISS AS cod,
        S.CATALOGO_ID AS catalogo_id,
        $tabelaOrigem AS TABELA_ORIGEM,
        C.nome,
        S.tipo,
        'P' AS flag,
        S.idSaida,
        C.idClientes AS Paciente,
        C.convenio AS Plano,
        COALESCE(B.valorMedio, '0.00') AS custo,
        (
          CASE
            S.tipo
            WHEN
              '0'
            THEN
              'Medicamento'
            
          END
        )
        AS apresentacao,
        (
          CASE
            WHEN
              COUNT(*) > 1
            THEN
              SUM(S.quantidade)
            ELSE
              S.quantidade
          END
        )
        AS quantidade,
        $valorItem 
         PL.nome AS nomeplano, 
         C.empresa,
          B.TUSS AS codTuss,
           PL.tipo_medicamento
           $codigoProprio
      FROM
        `saida` S
        INNER JOIN
          `clientes` C
          ON (S.idCliente = C.idClientes)
        INNER JOIN
          `catalogo` B
          ON (S.CATALOGO_ID = B.ID)
        INNER JOIN
          planosdesaude AS PL
          ON ( C.convenio = PL.id)
        INNER JOIN
          solicitacoes AS Sol
          ON (S.idSolicitacao = Sol.idSolicitacoes)
        LEFT JOIN
          prescricoes AS presc
          ON (Sol.id_periodica_complemento = presc.id)
          $leftTabelaCobrancaMedicamento
      WHERE
        C.idClientes = '{$paciente}'
        AND IF
          (
              Sol.JUSTIFICATIVA_AVULSO_ID = 2,
              (
                  presc.inicio BETWEEN '{$inicio}' AND '{$fim}'
                  OR presc.fim BETWEEN '{$inicio}' AND '{$fim}'
              ),
              S.data BETWEEN '{$inicio}' AND '{$fim}'
          )
        AND S.quantidade > 0 {$refaturarmed}
        AND
        (
          C.idClientes = S.idCliente
        )
        AND S.tipo = 0
        AND Sol.idPrescricao = -1
        AND Sol.TIPO_SOLICITACAO = -1
        and ( S.catalogo_id_escolhido is null or S.catalogo_id_escolhido = 0 or S.catalogo_id_escolhido ='')
  
      GROUP BY
        Paciente, catalogo_id
  
        UNION
  
        SELECT
          $itemTabelaCobranca
        S.NUMERO_TISS AS cod,
        S.catalogo_id_escolhido AS catalogo_id,
        $tabelaOrigem AS TABELA_ORIGEM,
        C.nome,
        S.tipo,
        'P' AS flag,
        S.idSaida,
        C.idClientes AS Paciente,
        C.convenio AS Plano,
        COALESCE(
                  ROUND(
                      SUM( S.quantidade * (( SELECT cat.valorMedio FROM catalogo AS cat WHERE cat.ID = S.CATALOGO_ID ))  )
                      / 
                      SUM( S.quantidade),
                      2
                    ), 
                    '0.00'  
                )
           AS custo,
        (
          CASE
            S.tipo
            WHEN
              '0'
            THEN
              'Medicamento'
            
          END
        )
        AS apresentacao,
        (
          CASE
            WHEN
              COUNT(*) > 1
            THEN
              SUM(S.quantidade)
            ELSE
              S.quantidade
          END
        )
        AS quantidade,
        $valorItem 
         PL.nome AS nomeplano, 
         C.empresa,
          B.TUSS AS codTuss,
           PL.tipo_medicamento
           $codigoProprio
      FROM
        `saida` S
        INNER JOIN
          `clientes` C
          ON (S.idCliente = C.idClientes)
        INNER JOIN
          `catalogo` B
          ON (S.catalogo_id_escolhido = B.ID)
        INNER JOIN
          planosdesaude AS PL
          ON ( C.convenio = PL.id)
        INNER JOIN
          solicitacoes AS Sol
          ON (S.idSolicitacao = Sol.idSolicitacoes)
        LEFT JOIN
          prescricoes AS presc
          ON (Sol.id_periodica_complemento = presc.id)
          $leftTabelaCobrancaMedicamento
      WHERE
        C.idClientes = '{$paciente}'
        AND IF
          (
              Sol.JUSTIFICATIVA_AVULSO_ID = 2,
              (
                  presc.inicio BETWEEN '{$inicio}' AND '{$fim}'
                  OR presc.fim BETWEEN '{$inicio}' AND '{$fim}'
              ),
              S.data BETWEEN '{$inicio}' AND '{$fim}'
          )
        AND S.quantidade > 0 {$refaturarmed}
        AND
        (
          C.idClientes = S.idCliente
        )
        AND S.tipo = 0
        AND Sol.idPrescricao = -1
        AND Sol.TIPO_SOLICITACAO = -1
        and ( S.catalogo_id_escolhido is not null and S.catalogo_id_escolhido <> 0 and S.catalogo_id_escolhido <> '')
      GROUP BY
        Paciente, catalogo_id
        UNION
      SELECT
      $itemTabelaCobranca
        S.NUMERO_TISS AS cod,
        S.CATALOGO_ID AS catalogo_id,
        $tabelaOrigem AS TABELA_ORIGEM,
        C.nome,
        S.tipo,
        'P' AS flag,
        S.idSaida,
        C.idClientes AS Paciente,
       C.convenio AS Plano,
        COALESCE(B.valorMedio, '0.00') AS custo,
        (
          CASE
            S.tipo
            WHEN
              '0'
              THEN
              'Medicamento'
            
          END
        )
        AS apresentacao,
        (
          CASE
            WHEN
              COUNT(*) > 1
            THEN
              SUM(S.quantidade)
            ELSE
              S.quantidade
          END
        )
        AS quantidade,
        $valorItem
         PL.nome AS nomeplano, 
         C.empresa, 
         B.TUSS AS codTuss,
          PL.tipo_medicamento
          $codigoProprio
      FROM
        `saida` S
        INNER JOIN
          `clientes` C
          ON (S.idCliente = C.idClientes)
        INNER JOIN
          `catalogo` B
          ON (S.CATALOGO_ID = B.ID)
        INNER JOIN
          planosdesaude AS PL
          ON ( C.convenio = PL.id)
        INNER JOIN
          solicitacoes AS Sol
          ON (S.idSolicitacao = Sol.idSolicitacoes)
        INNER JOIN
          relatorio_aditivo_enf AS P
          ON (Sol.relatorio_aditivo_enf_id = P.ID)
          $leftTabelaCobrancaMedicamento
      WHERE
        C.idClientes = '{$paciente}'
        AND
        (
          P.INICIO BETWEEN '{$inicio}' AND '{$fim}'
          OR P.FIM BETWEEN '{$inicio}' AND '{$fim}'
        )
        AND S.quantidade > 0 {$refaturarmed}
        AND
        (
          C.idClientes = S.idCliente
        )
        AND S.tipo = 0
        and ( S.catalogo_id_escolhido is null or S.catalogo_id_escolhido = 0 or S.catalogo_id_escolhido ='')
  
      GROUP BY
        Paciente, catalogo_id
        UNION
      SELECT
      $itemTabelaCobranca
        B.NUMERO_TISS AS cod,
        S.catalogo_id_escolhido AS catalogo_id,
        $tabelaOrigem AS TABELA_ORIGEM,
        C.nome,
        S.tipo,
        'P' AS flag,
        S.idSaida,
        C.idClientes AS Paciente,
       C.convenio AS Plano,
       COALESCE(
                  ROUND(
                      SUM( S.quantidade * (( SELECT cat.valorMedio FROM catalogo AS cat WHERE cat.ID = S.CATALOGO_ID ))  )
                      / 
                      SUM( S.quantidade),
                      2
                    ), 
                    '0.00'  
                )
           AS custo,
        (
          CASE
            S.tipo
            WHEN
              '0'
              THEN
              'Medicamento'
            
          END
        )
        AS apresentacao,
        (
          CASE
            WHEN
              COUNT(*) > 1
            THEN
              SUM(S.quantidade)
            ELSE
              S.quantidade
          END
        )
        AS quantidade,
        $valorItem
         PL.nome AS nomeplano, 
         C.empresa, 
         B.TUSS AS codTuss,
          PL.tipo_medicamento
          $codigoProprio
      FROM
        `saida` S
        INNER JOIN
          `clientes` C
          ON (S.idCliente = C.idClientes)
        INNER JOIN
          `catalogo` B
          ON (S.catalogo_id_escolhido = B.ID)
        INNER JOIN
          planosdesaude AS PL
          ON ( C.convenio = PL.id)
        INNER JOIN
          solicitacoes AS Sol
          ON (S.idSolicitacao = Sol.idSolicitacoes)
        INNER JOIN
          relatorio_aditivo_enf AS P
          ON (Sol.relatorio_aditivo_enf_id = P.ID)
          $leftTabelaCobrancaMedicamento
      WHERE
        C.idClientes = '{$paciente}'
        AND
        (
          P.INICIO BETWEEN '{$inicio}' AND '{$fim}'
          OR P.FIM BETWEEN '{$inicio}' AND '{$fim}'
        )
        AND S.quantidade > 0 {$refaturarmed}
        AND
        (
          C.idClientes = S.idCliente
        )
        AND S.tipo = 0
        and ( S.catalogo_id_escolhido is not null and S.catalogo_id_escolhido <> 0 and S.catalogo_id_escolhido <> '')
  
      GROUP BY
        Paciente, catalogo_id
      ";
    }

    

    
}
