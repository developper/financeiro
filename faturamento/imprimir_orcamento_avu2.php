<?php
$id = session_id();
if(empty($id))
    session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');

class imprimir_fatura{

    public $inicio;
    public $fim;

    public function cabecalho($id){


        $sql2 = "SELECT
                                UPPER(o.PACIENTE) AS paciente,
                                UPPER(u.nome) AS usuario,DATE_FORMAT(o.DATA,'%Y-%m-%d') as DATA,o.DATA_INICIO,o.DATA_FIM,
                                p.nome as Convenio
                          FROM
                                orcamento_avulso as o inner join
                                planosdesaude as p ON (p.id = o.PLANO_ID) INNER JOIN usuarios as u on (o.USUARIO_ID = u.idUsuarios)
		
                           WHERE
                                o.ID = {$id}
                           ORDER BY
                                o.Paciente, DATA DESC LIMIT 1";

        $result = mysql_query($sql);
        $result2 = mysql_query($sql2);
        $html.= "<table style='width:100%;' >";
        while($pessoa = mysql_fetch_array($result2)){
            foreach($pessoa AS $chave => $valor) {
                $pessoa[$chave] = stripslashes($valor);

            }

            $html.= "<tr bgcolor='#EEEEEE'>";
            $html.= "<td  style='width:60%;' colspan='2'><b>PACIENTE:</b></td>";
            $html.= "<td style='width:20%;'><b>CONV&Ecirc;NIO </b></td>";

            $html.= "</tr>";
            $html.= "<tr>";
            $html.= "<td style='width:60%;' colspan='2'>{$pessoa['paciente']}</td>";
            $html.= "<td style='width:20%;'>".strtoupper($pessoa['Convenio'])."</td>";
            $html.= "</tr>";


            $html.= "<tr bgcolor='#EEEEEE'>";

            $html.= "<td style='width:60%;'><b>Faturista:</b></td>";
            $html.= "<td style='width:20%;'><b>Data or&ccedil;ado: </b></td>";
            $html.= "<td style='width:30%;'><b>Per&iacute;odo</b></td>";
            $html.= "</tr>";
            $html.= "<tr>";

            $this->inicio = $pessoa['DATA_INICIO'];
            $this->fim = $pessoa['DATA_FIM'];

            $html.= "<td style='width:60%;'>{$pessoa['usuario']}</td>";
            $html.= "<td style='width:20%;'>".join("/",array_reverse(explode("-",$pessoa['DATA'])))."</td>";
            $html.= "<td style='width:30%;'>".join("/",array_reverse(explode("-",$pessoa['DATA_INICIO'])))." at&eacute; ".join("/",array_reverse(explode("-",$pessoa['DATA_FIM'])))."</td>";
            $html.= "</tr>";








            $this->plan =  $pessoa['id'];
        }
        $html.= "</table>";

        return $html;
    }


    public function detalhe_fatura($id,$av){
        $html .="<form>";

        $html .= "<h1><a><img src='../utils/logo2.jpg' width='200' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> Orcamento  </h1>";

        $html .= $this->cabecalho($id);
        $html .="<br></br>";
        $sql= "SELECT
		o.*,
		concat(B.principio,' - ',B.apresentacao) as principio,
		
		(CASE o.TIPO
		WHEN '0' THEN 'Medicamento'
		WHEN '1' THEN 'Material' WHEN '3' THEN 'Dieta' END) as apresentacao,
		1 as cod
		FROM
		orcamento_avulso_item as o  INNER JOIN
		`catalogo` B ON (o.CATALOGO_ID = B.ID)
		WHERE
			
					       o.ORCAMENTO_AVU_ID= {$id} and
					       o.TIPO IN (0,1,3) 
							
			UNION		
			        SELECT  o.*,
					       vc.DESC_COBRANCA_PLANO AS principio,
					      ('Servi&ccedil;os') AS apresentacao,	      							
					      vc.COD_COBRANCA_PLANO as cod
					
					FROM orcamento_avulso_item as o
                        INNER JOIN `cuidadosespeciais` as CS ON (o.CATALOGO_ID = CS.id)
                        INNER JOIN valorescobranca as vc on (vc.idCobranca = CS.COBRANCAPLANO_ID)
                        INNER JOIN orcamento_avulso as oa  on (o.ORCAMENTO_AVU_ID = oa.ID)
					WHERE
					      o.ORCAMENTO_AVU_ID= {$id}
					      AND o.TIPO =2 and oa.PLANO_ID= vc.idPlano
					       
					UNION
			SELECT DISTINCT
			                 o.*,
					       CO.item AS principio,
					       
					    			       
					       ('Equipamentos') AS apresentacao,
					        vc.COD_COBRANCA_PLANO as cod
					      	
					FROM 
						orcamento_avulso_item as o  INNER JOIN
						cobrancaplanos CO ON (o.CATALOGO_ID = CO.`id`)
                                                inner join valorescobranca as vc on
					(vc.idCobranca = CO.id) inner join orcamento_avulso as oa  on (o.ORCAMENTO_AVU_ID = oa.ID)
						
					WHERE 
					     o.ORCAMENTO_AVU_ID= {$id} and
					       o.TIPO =5 and oa.PLANO_ID= vc.idPlano			      
					      			
					
		UNION
			SELECT DISTINCT
			                 o.*,
					       CO.item AS principio,
					       
					    			       
					       ('Gasoterapia') AS apresentacao,
					        vc.COD_COBRANCA_PLANO as cod
					      	
					FROM 
						orcamento_avulso_item as o INNER JOIN
						cobrancaplanos CO ON (o.CATALOGO_ID = CO.`id`) 
                                                inner join valorescobranca as vc on
					(vc.idCobranca = CO.id) inner join orcamento_avulso as oa  on (o.ORCAMENTO_AVU_ID = oa.ID)
						
					WHERE 
					     o.ORCAMENTO_AVU_ID= {$id} and
					       o.TIPO =6 and oa.PLANO_ID= vc.idPlano			      
					      						      
					      			
					ORDER BY tipo,principio";

        $html.= "<table  width=100% class='mytable'>";

        $result = mysql_query($sql);
        $total=0.00;
        $cont_cor=0;
        $subtotal=0.00;
        $subtotal_equipamento=0.00;
        $subtotal_material=0.00;
        $subtotal_medicamento=0.00;
        $subtotal_gas=0.00;
        $subtotal_dieta=0.00;

        $subtotal_servico=0.00;
        $cont1=0;
        $cont2=0;
        $cont3=0;
        $cont4=0;
        $cont5=0;
        $cont6=0;
        while($row = mysql_fetch_array($result)){

            /*$subtotal +=$row['QTD']*$row['VAL_PRODUTO'];



            if($row['TIPO'] == '6'){
                $subtotal_gas +=$row['QTD']*$row['VAL_PRODUTO'];
                $parcial = $subtotal_gas;
                $cont5++;
                $n=$row['cod'];
                $tip = $row['TIPO'];

            }

            if($row['TIPO'] == '5'){
                $subtotal_equipamento +=$row['QTD']*$row['VAL_PRODUTO'];
                $parcial=$subtotal_equipamento;
                $cont4++;
                $n=$row['cod'];
                $tip = $row['TIPO'];

            }
            if($row['TIPO'] == '3'){
                $subtotal_dieta +=$row['QTD']*$row['VAL_PRODUTO'];
                $cont6++;
                $n=$row['cod'];
                $tip = $row['TIPO'];
                $parcial=$subtotal_dieta;
            }
            if($row['TIPO'] == '2'){
                $subtotal_servico +=$row['QTD']*$row['VAL_PRODUTO'];
                $cont3++;
                $n=$row['cod'];
                $tip = $row['TIPO'];
                $parcial=$subtotal_servico;
            }
            if($row['TIPO'] == '1'){
                $subtotal_material +=$row['QTD']*$row['VAL_PRODUTO'];
                $cont1++;
                $n='';
                $tip = $row['TIPO'];
                $parcial=$subtotal_material;
            }
            if($row['TIPO'] == '0'){
                $subtotal_medicamento +=$row['QTD']*$row['VAL_PRODUTO'];
                $cont2++;
                $n="Bras. ".$row['NUM_TISS'];
                $tip = $row['TIPO'];
                $parcial=$subtotal_medicamento;
            }

            if( $cont_cor%2 ==0 ){
                $cor = '';
            }else{
                $cor="bgcolor='#EEEEEE'";
            }


        if($row['UNIDADE']=='1')
          {     $und='Comprimido';
             }

             if($row['UNIDADE']=='2')
             {
                 $und='Caixa';
             }

             if($row['UNIDADE']=='3')
             {
                 $und='Frasco';
             }
             if($row['UNIDADE']=='4')
             {
                 $und='Ampola';
             }
             if($row['UNIDADE']=='5')
             {
                 $und='Bisnaga';
             }
             if($row['UNIDADE']=='6')
             {
                 $und='Sach&ecirc;';
             }
             if($row['UNIDADE']=='7')
             {
                 $und='Flaconete';
             }
             if($row['UNIDADE']=='8')
             {
                 $und='Pacote';
             }
             if($row['UNIDADE']=='9')
             {
                 $und='Di&aacute;ria';
             }
             if($row['UNIDADE']=='10')
             {
                 $und='Sess&atilde;o';
             }
             if($row['UNIDADE']=='11')
             {
                 $und='Visita';
             }if($row['UNIDADE']=='12')
             {
                 $und='Unidade';
             }


             if($cont5 == 1){
                 if($tip != $tipo && $cont_cor != 0){
                     $html.= "<tr bgcolor='#EEEEEE'><td colspan='6'  style='font-size:12px;float:right;border:1px solid #000;text-align:right;'><b>Parcial: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
                 }
                 $html.= "<tr></tr><tr bgcolor='#EEEEEE'><td colspan='6' style='font-size:12px;float:right;'>Gasoterapia</td></tr>";
                 $html.= "<tr >
                 <td style='font-size:12px;float:right;border:1px solid #000;'>N&deg;</td>
                 <td style='font-size:12px;float:right;border:1px solid #000;'>Unidade</td>
                 <td style='font-size:12px;float:right;border:1px solid #000;'>Item</td>
                 <th style='font-size:12px;float:right;border:1px solid #000;'>Qtd</th>
                 <th style='font-size:12px;float:right;border:1px solid #000;'>Pre&ccedil;o Unt.</th>
                 <th style='font-size:12px;float:right;border:1px solid #000;'>Pre&ccedil;o Tot.</th>
                 </tr>";


             }
            if($cont4 == 1){
                if($tip != $tipo && $cont_cor != 0){
                    $html.= "<tr bgcolor='#EEEEEE' ><td colspan='6'  style='font-size:12px;float:right;border:1px solid #000;text-align:right;'><b>Parcial: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
                }
                $html.= "<tr></tr><tr bgcolor='#EEEEEE'><td colspan='6' style='font-size:12px;float:right;'>MATERIAIS PERMANENTES</td></tr>";
                $html.= "<tr >
                <td style='font-size:12px;float:right;border:1px solid #000;'>N&deg;</td>
                <td style='font-size:12px;float:right;border:1px solid #000;'>Unidade</td>
                <td style='font-size:12px;float:right;border:1px solid #000;'>Item</td>
                <th style='font-size:12px;float:right;border:1px solid #000;'>Qtd</th>
                <th style='font-size:12px;float:right;border:1px solid #000;'>Pre&ccedil;o Unt.</th>
                <th style='font-size:12px;float:right;border:1px solid #000;'>Pre&ccedil;o Tot.</th>
                </tr>";


            }
            if($cont3 == 1 ){
                if($tip != $tipo && $cont_cor != 0){
                    $html.= "<tr bgcolor='#cccccc'><td colspan='6'  style='font-size:12px;float:right;border:1px solid #000;text-align:right;'><b>Parcial: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
                }
                $html.= "<tr></tr><tr bgcolor='#EEEEEE'><td colspan='6' style='font-size:12px;float:right;'>SERVI&Ccedil;OS</td></tr>";
                $html.= "<tr >
                <td style='font-size:12px;float:right;border:1px solid #000;'>N&deg;</td>
                <td style='font-size:12px;float:right;border:1px solid #000;'>Unidade</td>
                <td style='font-size:12px;float:right;border:1px solid #000;'>Item</td>
                <th style='font-size:12px;float:right;border:1px solid #000;'>Qtd</th>
                <th style='font-size:12px;float:right;border:1px solid #000;'>Pre&ccedil;o Unt.</th>
                <th style='font-size:12px;float:right;border:1px solid #000;'>Pre&ccedil;o Tot.</th>


                </tr>";

            }
            if($cont1 == 1){
                if($tip != $tipo && $cont_cor != 0){
                    $html.= "<tr  ><td colspan='6'  style='font-size:12px;float:right;border:1px solid #000;text-align:right;'><b>Parcial: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
                }
                $html.= "<tr></tr><tr  bgcolor='#EEEEEE'><td colspan='6' style='font-size:12px;float:right;'>MATERIAIS</td></tr>";
                $html.= "<tr>
                <td style='font-size:12px;float:right;border:1px solid #000;'>N&deg;</td>
                <td style='font-size:12px;float:right;border:1px solid #000;'>Unidade</td>
                <td style='font-size:12px;float:right;border:1px solid #000;'>Item</td>
                <th style='font-size:12px;float:right;border:1px solid #000;'>Qtd</th>
                <th style='font-size:12px;float:right;border:1px solid #000;'>Pre&ccedil;o Unt.</th>
                <th style='font-size:12px;float:right;border:1px solid #000;'>Pre&ccedil;o Tot. </th>
                </tr>";

            }
            if($cont2 == 1){
                if($tip != $tipo && $cont_cor != 0 ){
                    $html.= "<tr bgcolor='#EEEEEE' ><td colspan='6'  style='font-size:12px;float:right;border:1px solid #000;text-align:right;'><b>Parcial: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
                }
                $html.= "<tr></tr><tr bgcolor='#EEEEEE'><td colspan='6' style='font-size:12px;float:right;'>MEDICAMENTOS</td></tr>";
                $html.= "<tr>
                <td style='font-size:12px;float:right;border:1px solid #000;'>N&deg;</td>
                <td style='font-size:12px;float:right;border:1px solid #000;'>Unidade</td>
                <td style='font-size:12px;float:right;border:1px solid #000;'>Item</td>
                <th style='font-size:12px;float:right;border:1px solid #000;'>Qtd</th>
                <th style='font-size:12px;float:right;border:1px solid #000;'>Pre&ccedil;o</th>
                <th style='font-size:12px;float:right;border:1px solid #000;'>Pre&ccedil;o Tot.</th>
                </tr>";

            }



             $tipo = $tip;
            $par= $parcial;
            $html.= "<tr $cor>
            <td>{$n}</td>
            <td>{$und}</td>
            <td width='50%'>".strtoupper(
            $row['principio'])."</td>
            <td>{$row['QTD']}</td>
            <td>R$ ".number_format($row['VAL_PRODUTO'],2,',','.')."</td>";
            $vt=$row['VAL_PRODUTO']*$row['QTD'];
            $vtotal += $vt;
            $html.= "<td>R$ ".number_format($vt,2,',','.')."</td>
            </tr>";

            if($cont4 == 1){
                $cont4++;

            }
            if($cont3 == 1){
                $cont3++;

            }
            if($cont1 == 1){
                $cont1++;

            }
            if($cont2 == 1){
                $cont2++;

            }
            $cont_cor++;
        }

        $html.= "<tr  ><td colspan='6'  style='font-size:12px;float:right;border:1px solid #000;text-align:right;'><b>PARCIAL: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
        $html.= "<tr  ><td colspan='6'  style='font-size:12px;float:right;border:1px solid #000;text-align:right;'><b>TOTAL: R$</b> <b><i>".number_format($vtotal,2,',','.')."</i></b></td></tr>";
        */
            $subtotal +=$row['QUANTIDADE']*$row['VALOR_FATURA'];



            if($row['TIPO'] == '6'){
                $subtotal_gas +=$row['QTD']*$row['VAL_PRODUTO'];
                $parcial=$subtotal_gas;
                $cont5++;
                $n=$row['cod'];
                $tip = $row['TIPO'];
                $tipo_tot="TOTAL GASOTERAPIA";

            }

            if($row['TIPO'] == '5'){
                $subtotal_equipamento +=$row['QTD']*$row['VAL_PRODUTO'];
                $parcial=$subtotal_equipamento;
                $cont4++;
                $n=$row['cod'];
                $tip = $row['TIPO'];
                $tipo_tot="TOTAL EQUIPAMENTO";

            }if($row['TIPO'] == '3'){
                $subtotal_dieta +=$row['QTD']*$row['VAL_PRODUTO'];
                $cont6++;
                $n="Bras. ".$row['NUMERO_TISS'];
                $tip = $row['TIPO'];
                $parcial=$subtotal_dieta;
                $tipo_tot="TOTAL DIETA";
            }
            if($row['TIPO'] == '2'){
                $subtotal_servico +=$row['QTD']*$row['VAL_PRODUTO'];
                $cont3++;
                $n=$row['cod'];
                $tip = $row['TIPO'];
                $parcial=$subtotal_servico;
                $tipo_tot="TOTAL SERVI&Ccedil;O";
            }
            if($row['TIPO'] == '1'){
                $subtotal_material +=$row['QTD']*$row['VAL_PRODUTO'];
                $cont1++;
                $n="Simp. ".$row['NUMERO_TISS'];
                $tip = $row['TIPO'];
                $parcial=$subtotal_material;
                $tipo_tot="TOTAL MATERIAL";
            }
            if($row['TIPO'] == '0'){
                $subtotal_medicamento +=$row['QTD']*$row['VAL_PRODUTO'];
                $cont2++;
                $n="Bras. ".$row['NUMERO_TISS'];
                $tip = $row['TIPO'];
                $parcial=$subtotal_medicamento;
                $tipo_tot="TOTAL MEDICAMENTO";
            }

            if( $cont_cor%2 ==0 ){
                $cor = '';
            }else{
                $cor="bgcolor='#EEEEEE'";
            }


            if($row['UNIDADE']=='1')
            {
                $und='Comprimido';
            }

            if($row['UNIDADE']=='2')
            {
                $und='Caixa';
            }

            if($row['UNIDADE']=='3')
            {
                $und='Frasco';
            }
            if($row['UNIDADE']=='4')
            {
                $und='Ampola';
            }
            if($row['UNIDADE']=='5')
            {
                $und='Bisnaga';
            }
            if($row['UNIDADE']=='6')
            {
                $und='Sach&ecirc;';
            }
            if($row['UNIDADE']=='7')
            {
                $und='Flaconete';
            }
            if($row['UNIDADE']=='8')
            {
                $und='Pacote';
            }
            if($row['UNIDADE']=='9')
            {
                $und='Di&aacute;ria';
            }
            if($row['UNIDADE']=='10')
            {
                $und='Sess&atilde;o';
            }
            if($row['UNIDADE']=='11')
            {
                $und='Visita';
            }if($row['UNIDADE']=='12')
            {
                $und='Unidade';
            }if($row['UNIDADE']=='13')
            {
                $und='Lata';
            }


            if($cont5 == 1){
                if($tip != $tipo && $cont_cor != 0){
                    $html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
                }
                $html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>Gasoterapia</td></tr>";
                $html.= "<tr bgcolor='#cccccc'>
		     	<td style='font-size:12px;float:right;'>N&deg;</td>
		     	<td style='font-size:12px;float:right;'>Unidade</td>
		     	<td style='font-size:12px;float:right;'>Item</td>
		     	<th style='font-size:12px;float:right;'>Qtd</th>
		     	<th style='font-size:12px;float:right;'>Pre&ccedil;o Unt.</th>
		     	<th style='font-size:12px;float:right;'>Pre&ccedil;o Tot.</th>
		     	</tr>";


            }
            if($cont4 == 1){
                if($tip != $tipo && $cont_cor != 0){
                    $html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
                }
                $html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>MATERIAIS PERMANENTES</td></tr>";
                $html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
				</tr>";


            }
            if($cont6 == 1 ){
                if($tip != $tipo && $cont_cor != 0){
                    $html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;bgcolor:#EEEEEE;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
                }
                $html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>DIETA</td></tr>";
                $html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
			
			
				</tr>";

            }
            if($cont3 == 1 ){
                if($tip != $tipo && $cont_cor != 0){
                    $html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;bgcolor:#EEEEEE;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
                }
                $html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>SERVI&Ccedil;OS</td></tr>";
                $html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
			
			
				</tr>";

            }
            if($cont1 == 1){
                if($tip != $tipo && $cont_cor != 0){
                    $html.= "<tr><td colspan='6'  style='font-size:12px;float:right;text-align:right;bgcolor:#EEEEEE;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
                }
                $html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>MATERIAIS</td></tr>";
                $html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot. </th>
				</tr>";

            }
            if($cont2 == 1){
                if($tip != $tipo && $cont_cor != 0 ){
                    $html.= "<tr><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
                }
                $html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='6' style='font-size:12px;float:right;bgcolor:#EEEEEE; '>MEDICAMENTOS</td></tr>";
                $html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
				</tr>";

            }


            //
            $tipo = $tip;
            $par= $parcial;
            $tipo_tot1=$tipo_tot;
            $html.= "<tr $cor>
			<td>{$n}</td>
			<td>{$und}</td>
			<td width='50%'> ".strtoupper(strtr($row['principio'] ,"áéíóúâêôãõàèìòùç","ÁÉÍÓÚÂÊÔÃÕÀÈÌÒÙÇ"))."</td>
			<td>{$row['QTD']}</td>
			<td>R$ ".number_format($row['VAL_PRODUTO'],2,',','.')."</td>";
            $vt=$row['VAL_PRODUTO']*$row['QTD'];
            $vtotal += $vt;
            $html.= "<td>R$ ".number_format($vt,2,',','.')."</td>
			</tr>";

            if($cont5 == 1){
                $cont5++;

            }
            if($cont4 == 1){
                $cont4++;

            }
            if($cont3 == 1){
                $cont3++;

            }
            if($cont1 == 1){
                $cont1++;

            }
            if($cont2 == 1){
                $cont2++;

            }
            $cont_cor++;
            $obs=$row['OBS'];
        }
        $obj_data_inicio = new DateTime($this->inicio);
        $obj_data_fim = new DateTime($this->fim);
        $diferenca_dias = $obj_data_inicio->diff($obj_data_fim);

        if($diferenca_dias->format("%r%d"))
        {
            $diaria = $vtotal/(int)$diferenca_dias->format("%r%d");
        }
        else
        {
            $diaria = 0.00;
        }
        $html.= "<tr ><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>{$tipo_tot1}: R$</b> <b><i>".number_format($par,2,',','.')."</i></b></td></tr>";
        $html.= "<tr bgcolor='#cccccc' ><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>TOTAL: R$</b> <b><i>".number_format($vtotal,2,',','.')."</i></b></td></tr>";
        $html.= "<tr bgcolor='#cccccc' ><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>Valor da Diária: R$</b> <b><i>".number_format($diaria,2,',','.')."</i></b></td></tr>";
        $html.= "<tr bgcolor='#cccccc' ><td colspan='6'  style='font-size:12px;float:right;bgcolor:#EEEEEE;text-align:right;'><b>Valor para 30 dias: R$</b> <b><i>".number_format($diaria*30,2,',','.')."</i></b></td></tr>";

        $html.= "</table>";

        $paginas []= $header.$html.= "</form>";


        //print_r($paginas);
        //print_r($paginas);

        $mpdf=new mPDF('pt','A4',9);
        $mpdf->SetHeader('página {PAGENO} de {nbpg}');
        $ano = date("Y");
        $mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
        $mpdf->WriteHTML("<html><body>");
        $flag = false;
        foreach($paginas as $pag){
            if($flag) $mpdf->WriteHTML("<formfeed>");
            $mpdf->WriteHTML($pag);
            $flag = true;
        }
        $mpdf->WriteHTML("</body></html>");
        $mpdf->Output('prescricao_avaliacao.pdf','I');
        exit;




        /*$mpdf=new mPDF('pt','A4',9);
        $mpdf->WriteHTML($html);
        $mpdf->Output('pacientes.pdf','I');*/



    }





}




$p = new imprimir_fatura();
$p->detalhe_fatura($_GET['id'],$_GET['av']);






?>
