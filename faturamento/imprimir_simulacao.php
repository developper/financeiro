<?php
//ini_set('display_errors', 1);
$id = session_id();
if(empty($id))
	session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');

class imprimir_simulacao{

	public $inicio;
	public $fim;

	public function cabecalho($id){

		$sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		UPPER(u.nome) AS usuario,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo1,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresan,
		c.`nascimento`,
		c.diagnostico,
		c.codigo,
		p.nome as Convenio,
		p.id,
		c.*,
		NUM_MATRICULA_CONVENIO,
        f.GUIA_TISS,
        DATE_FORMAT(s.created_at, '%d/%m/%Y') AS criado_em,
        cpf
		FROM
		fatura as f inner join
		orcamento_simulacao AS s ON f.ID = s.fatura LEFT JOIN
		orcamento_simulacao_item AS si ON s.id = si.simulacao_id LEFT JOIN
		clientes AS c on (f.PACIENTE_ID = c.idClientes) LEFT JOIN
		empresas AS e ON (e.id = c.empresa) LEFT JOIN 
		planosdesaude as p ON (p.id = f.PLANO_ID) INNER JOIN
        usuarios as u on (si.usuario_id = u.idUsuarios)		
		WHERE
		c.idClientes = {$id}
		ORDER BY
		c.nome DESC LIMIT 1";

		$result = mysql_query($sql);
		$result2 = mysql_query($sql2);
		$html.= "<table style='width:100%;' >";
		while($pessoa = mysql_fetch_array($result2)){
			foreach($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);

			}
			$this->inicio = $_GET['inicio'];
			$this->fim = $_GET['fim'];
			$idade = join("/",array_reverse(explode("-",$pessoa['nascimento'])))."(".$pessoa['idade']." anos)";

			if($idade == "00/00/0000( anos)"){
				$idade='';
			}
			$html.= "<tr bgcolor='#EEEEEE'>";
			$html.= "<td  style='width:60%;'><b>PACIENTE:</b></td>";
			$html.= "<td style='width:20%;' ><b>SEXO </b></td>";
			$html.= "<td style='width:30%;'><b>IDADE:</b></td>";
			$html.= "</tr>";
			$html.= "<tr>";
			$html.= "<td style='width:60%;'>{$pessoa['paciente']}</td>";
			$html.= "<td style='width:20%;'>{$pessoa['sexo1']}</td>";
			$html.= "<td style='width:30%;'>{$idade}</td>";
			$html.= "</tr>";

			$html.= "<tr bgcolor='#EEEEEE'>";

			$html.= "<td style='width:60%;'><b>UNIDADE REGIONAL:</b></td>";
			$html.= "<td style='width:20%;'><b>CONV&Ecirc;NIO </b></td>";
			$html.= "<td style='width:30%;'><b>MATR&Iacute;CULA </b></td>";
			$html.= "</tr>";
			$html.= "<tr>";

			$html.= "<td style='width:60%;'>{$pessoa['empresan']}</td>";
			$html.= "<td style='width:20%;'>".strtoupper($pessoa['Convenio'])."</td>";
			$html.= "<td style='width:30%;'>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
			$html.= "</tr>";

			$html.= "<tr bgcolor='#EEEEEE'>";

			$html.= "<td style='width:60%;'><b>Faturista:</b></td>";
			$html.= "<td style='width:20%;'><b>Criado Em: </b></td>";
			$html.= "<td style='width:30%;'><b>Per&iacute;odo</b></td>";
			$html.= "</tr>";
			$html.= "<tr>";

			$html.= "<td style='width:60%;'>{$pessoa['usuario']}</td>";
			$html.= "<td style='width:20%;'>".join("/",array_reverse(explode("-",$pessoa['criado_em'])))."</td>";
			$html.= "<td style='width:30%;'>{$this->inicio} at&eacute; {$this->fim}</td>";
			$html.= "</tr>";

			$html.= "<tr bgcolor='#EEEEEE'>";

			$html.= "<td style='width:60%;'><b>CID:</b></td>";
			$html.= "<td style='width:20%;'><b>CONTA MEDERI: </b></td>";
			$html.= "<td style='width:30%;'><b>N° DA GUIA</b></td>";
			$html.= "</tr>";
			$html.= "<tr>";

			$html.= "<td style='width:60%;'>{$pessoa['diagnostico']}</td>";
			$html.= "<td style='width:20%;'>{$pessoa['codigo']}</td>";
			$html.= "<td style='width:30%;'>{$pessoa['GUIA_TISS']}</td>";
			$html.= "</tr>";
			$html.= " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
			$this->plan =  $pessoa['id'];
		}
		$html.= "</table>";

		return $html;
	}

	private function sqlSimulacaoVisualizar($simulacaoId)
	{
		return <<<SQL
SELECT 
  orcamento_simulacao_item.*,
  orcamento_simulacao_item.id AS simItemId,
  IF(catalogo.DESC_MATERIAIS_FATURAR IS NULL, concat(catalogo.principio, ' - ', catalogo.apresentacao), catalogo.DESC_MATERIAIS_FATURAR) AS principio,
  CASE orcamento_simulacao_item.tipo
    WHEN '0' THEN 'Medicamento'
    WHEN '1' THEN 'Material'
    WHEN '3' THEN 'Dieta' 
  END as apresentacao,
  catalogo.ID as cod_item
FROM
  orcamento_simulacao_item
  INNER JOIN catalogo ON orcamento_simulacao_item.catalogo_id = catalogo.ID
WHERE
  orcamento_simulacao_item.simulacao_id = '{$simulacaoId}'
  AND orcamento_simulacao_item.tipo IN (0, 1, 3) 

UNION
  SELECT 
    orcamento_simulacao_item.*,
    orcamento_simulacao_item.id AS simItemId,
    valorescobranca.DESC_COBRANCA_PLANO AS principio,
    'Servi&ccedil;os' AS apresentacao,
    valorescobranca.id as cod_item
  FROM
    orcamento_simulacao_item
    INNER JOIN valorescobranca ON orcamento_simulacao_item.valorescobranca_id = valorescobranca.id
  WHERE
    orcamento_simulacao_item.simulacao_id = '{$simulacaoId}'
    AND orcamento_simulacao_item.tipo = '2'
    AND orcamento_simulacao_item.tabela_origem = 'valorescobranca'
    
  UNION
    SELECT 
      orcamento_simulacao_item.*,
      orcamento_simulacao_item.id AS simItemId,
      cobrancaplanos.item AS principio,
      'Servi&ccedil;os' AS apresentacao,
      cobrancaplanos.id as cod_item
    FROM
      orcamento_simulacao_item
      INNER JOIN cobrancaplanos ON orcamento_simulacao_item.cobrancaplano_id = cobrancaplanos.id
    WHERE
      orcamento_simulacao_item.simulacao_id = '{$simulacaoId}'
      AND orcamento_simulacao_item.tipo = '2'
      AND orcamento_simulacao_item.tabela_origem = 'cobrancaplano'

    UNION
      SELECT 
        orcamento_simulacao_item.*,
        orcamento_simulacao_item.id AS simItemId,
        cobrancaplanos.item AS principio,
        'Equipamentos' AS apresentacao,
        cobrancaplanos.id as cod_item
      FROM
        orcamento_simulacao_item
        INNER JOIN cobrancaplanos ON orcamento_simulacao_item.cobrancaplano_id = cobrancaplanos.id
      WHERE
        orcamento_simulacao_item.simulacao_id = '{$simulacaoId}'
        AND orcamento_simulacao_item.tipo = '5'
        AND orcamento_simulacao_item.tabela_origem = 'cobrancaplano'
      
      UNION
        SELECT 
          orcamento_simulacao_item.*,
          orcamento_simulacao_item.id AS simItemId,
          valorescobranca.DESC_COBRANCA_PLANO AS principio,
          'Equipamentos' AS apresentacao,
          valorescobranca.id as cod_item
        FROM
          orcamento_simulacao_item
          INNER JOIN valorescobranca ON orcamento_simulacao_item.valorescobranca_id = valorescobranca.id
        WHERE
          orcamento_simulacao_item.simulacao_id = '{$simulacaoId}'
          AND orcamento_simulacao_item.tipo = '5'
          AND orcamento_simulacao_item.tabela_origem = 'valorescobranca'

        UNION
          SELECT 
            orcamento_simulacao_item.*,
            orcamento_simulacao_item.id AS simItemId,
            cobrancaplanos.item AS principio,
            'Gasoterapia' AS apresentacao,
            cobrancaplanos.id as cod_item
          FROM
            orcamento_simulacao_item
            INNER JOIN cobrancaplanos ON orcamento_simulacao_item.cobrancaplano_id = cobrancaplanos.id
          WHERE
            orcamento_simulacao_item.simulacao_id = '{$simulacaoId}'
            AND orcamento_simulacao_item.tipo = '6'
            AND orcamento_simulacao_item.tabela_origem = 'cobrancaplano'

          UNION
            SELECT 
              orcamento_simulacao_item.*,
              orcamento_simulacao_item.id AS simItemId,
              valorescobranca.DESC_COBRANCA_PLANO AS principio,
              'Gasoterapia' AS apresentacao,
              valorescobranca.id as cod_item
            FROM
              orcamento_simulacao_item
              INNER JOIN valorescobranca ON orcamento_simulacao_item.valorescobranca_id = valorescobranca.id
            WHERE
              orcamento_simulacao_item.simulacao_id = '{$simulacaoId}'
              AND orcamento_simulacao_item.tipo = '6'
              AND orcamento_simulacao_item.tabela_origem = 'valorescobranca'
ORDER BY 
  simItemId
SQL;
	}

	public function detalhe_simulacao($id, $paciente){
		$html .="<form>";
		$html .= "<h1><a><img src='../utils/logo2.jpg' width='200' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> Simulação de Custo  </h1>";

		$html .= $this->cabecalho($paciente);
		$html .="<br></br>";

		$sql = <<<SQL
SELECT 
  orcamento_simulacao.*
FROM
  orcamento_simulacao
WHERE
  id = '{$id}'
SQL;
		$rs = mysql_query($sql);
		$row = mysql_fetch_array($rs);
		$fatura = $row['fatura'];

		$inicio = \DateTime::createFromFormat('Y-m-d', $row['inicio'])->format('d/m/Y');
		$fim = \DateTime::createFromFormat('Y-m-d', $row['fim'])->format('d/m/Y');

		$html .= "    <b>De {$inicio} à {$fim}</b>
                 <br><br>
				  <table  width=100% class='mytable'>
						  <thead>
						<tr>		     	
							<th style='font-size:12px;'>Item</th>
							<th style='font-size:12px'>Qtd</th>
							<th style='font-size:12px'>Unidade</th>
							<th style='font-size:12px;'>Pre&ccedil;o Unt.</th>
							<th style='font-size:12px;'>Custo Unit.</th>
							 <th style='font-size:12px;'>Taxa Tot.</th>
							<th style='font-size:12px;'>Margem do Item</th>
						</tr>
				   </thead>";

		$sql = $this->sqlSimulacaoVisualizar($id);
		$result = mysql_query($sql) or die (mysql_error());

		$valorTotal = 0;
		$custoTotal = 0;
		$i = 1;
		while($row = mysql_fetch_array($result)){
			if($i % 2 == 0){
				$bgColor = 'white';
			} else {
				$bgColor = '#dcdcdc';
			}
			$quantidade = $row['qtd'];
			$custoItem = $row['custo'];
			$desconto_acrescimo = ($quantidade*$row['preco'])*($row['acrescimo_desconto'] /100);
			$valorTotal += ($quantidade * $row['preco'])+$desconto_acrescimo;
			$custoTotal += $quantidade * $custoItem;
			$margemItem = 0.00;
			if($quantidade == 0){
				$margemItem = 0;
			}else if($row['preco'] > 0) {
				$margemItem = (((($row['preco'] - $custoItem)*$quantidade) + $desconto_acrescimo) /  ($quantidade*$row['preco'])) * 100;
			}
			$corItem = "green";
			if($margemItem < 25) {
				$corItem = "red";
			}

			$html .= "<tr bgcolor='{$bgColor}'>
					<td width='50%'>" . strtoupper($row['principio']) . " </td>
					<td>{$row['qtd']}</td>
					<td>{$row['unidade']}</td>
					<td>R$ " . number_format($row['preco'], 2, ',', '.') . "</td>
					<td>R$ " . number_format($custoItem, 2, ',', '.') . "</td>
					<td>R$ " . number_format($desconto_acrescimo, 2, ',', '.') . "</td>
					<td style='color: {$corItem}'>" . number_format($margemItem, 2, ',', '.') . " %</td>
				  </tr>";
			$i++;
		}
        $pissCofins = 4.95;
        $ir = 3.5;
		$sqlIssFatura =  "select imposto_iss from fatura where id = {$fatura}";
		$resultIssFatura = mysql_query($sqlIssFatura);
		$issFatura = mysql_result($resultIssFatura,0);

		$custoTotal = ($valorTotal * ($pissCofins + $ir + $issFatura) / 100) + $custoTotal;
		$lucroReais = $valorTotal - $custoTotal;
		$lucroPorcento = ($valorTotal - $custoTotal) / $valorTotal;
		$lucroPorcento *= 100;

        if($valorTotal >= 4000) {
            $alerta = "Orçamento aprovado com ressalva(s)";
            $corAlerta = "orange";
            if ($lucroPorcento < 25 && $lucroReais < 1000) {
                $alerta = "Orçamento reprovado";
                $corAlerta = "red";
            } elseif ($lucroPorcento >= 25 && $lucroReais >= 1000) {
                $alerta = "Orçamento aprovado";
                $corAlerta = "green";
            }
        } elseif($valorTotal > 1000 && $valorTotal < 4000) {
            if ($lucroPorcento < 25) {
                $alerta = "Orçamento reprovado";
                $corAlerta = "red";
            } elseif ($lucroPorcento >= 25) {
                $alerta = "Orçamento aprovado";
                $corAlerta = "green";
            }
        } elseif($valorTotal <= 1000) {
            if ($lucroPorcento >= 15) {
                $alerta = "Orçamento aprovado";
                $corAlerta = "green";
            } elseif ($lucroPorcento < 15) {
                $alerta = "Orçamento reprovado";
                $corAlerta = "red";
            }
        }

		$html .= "</table>
              <br>
                <table class='mytable' width=100% >
                <thead>
                <tr>
					<th colspan='2' align='center' style='font-size: 10px !important;'>
						Valores dos encargos adiconados ao custo total: PISCOFINS {$pissCofins} %, IR {$ir} % mais o ISS informado acima.
						<br><b>OBS:</b>
						O valor total da fatura leva em consideração os descontos ou acréscimos.
					</th>
				</tr>
				</thead>
				<tr>
					<td>
						<b> Valor Total Faturado:</b>
					</td>
					<td>
						R$ <span id='valor-total'>".number_format($valorTotal,2,',','')."</span>
					</td>
				</tr>
				 <tr>
					<td>
						<b>Custo Total + Encargos:</b>
					</td>
					<td>
						 R$ <span id='custo-encargo'> ".number_format($custoTotal,2,',','')."</span>
					</td>
				</tr>
				
				<tr>
					<td>
						<b>Margem:</b>
					</td>
					<td>
						 R$ <span id='margem'>".number_format($lucroReais,2,',','')."</span>
					</td>
				</tr>
				<tr>
					<td>
						<b>Margem Percentual (aprox.)</b>
					</td>
					<td>
						 <span id='margem-percentual'>".number_format($lucroPorcento,2,',','')."</span>%
					</td>
				</tr>
				<tr>
					<td colspan='2' id='alerta-margem' align='center' bgcolor='{$corAlerta}'>
						 <b>{$alerta}</b>
					</td>
				</tr>
                </table>
			 </form>";

		$paginas []= $header.$html.= "</form>";

		$mpdf=new mPDF('pt','A4',9);
		$mpdf->SetHeader('página {PAGENO} de {nbpg}');
		$ano = date("Y");
		$mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
		$mpdf->WriteHTML("<html><body>");
		$flag = false;
		foreach($paginas as $pag){
			if($flag) $mpdf->WriteHTML("<formfeed>");
			$mpdf->WriteHTML($pag);
			$flag = true;
		}
		$mpdf->WriteHTML("</body></html>");
		$mpdf->Output('simulacao.pdf','I');
		exit;
	}
}

$p = new imprimir_simulacao();
$p->detalhe_simulacao($_GET['simulacao'], $_GET['paciente']);

