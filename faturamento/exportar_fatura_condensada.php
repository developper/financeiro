

<?php
$id = session_id();
if(empty($id))
  session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');

header("Content-type: application/x-msexce");
header("Content-type: application/force-download;");
header("Content-Disposition: attachment; filename=fatura.xls");
header("Pragma: no-cache");

class Exportar_fatura_condensada{	

	
	
	public function cabecalho($ur,$inicio,$fim){
		
		
	$sql2 = "SELECT	   
		         nome 
		FROM
		
		    empresas 	
		
		WHERE
		    id ={$ur}";
	
		
		$result2 = mysql_query($sql2);
		$html.= "<table style='width:100%;' >";
		while($pessoa = mysql_fetch_array($result2)){
		
	           
				$html.= "<tr bgcolor='#EEEEEE'>";
				$html.= "<td  ><b>Unidade Regional:</b></td>";
				$html.= "<td  ><b>Per&iacute;odo </b></td>";
				
				$html.= "</tr>";
				$html.= "<tr>";
				$html.= "<td >".utf8_decode($pessoa['nome'])."</td>";
				$html.= "<td >{$inicio} a {$fim}</td>";
				
				$html.= "</tr>";
	
	      }
	     $html.= "</table>";
	
	return $html;
	}
	
	
	public function fatura_condensada($ur,$inicio,$fim){
		//$html .="<form>";
		
		$html .= "<h1> Fatura de Assist&ecirc;ncia Domiciliar  </h1>";
		
		$html .= $this->cabecalho($ur,$inicio,$fim);
                 
                
		$html .="<br></br>";
		$inicio=join("-",array_reverse(explode("/",$inicio)));
                $fim=join("-",array_reverse(explode("/",$fim)));
		$sql= "
                    SELECT
                            f.*,
                            fa.OBS,
                            IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),B.DESC_MATERIAIS_FATURAR) AS principio,
                            (CASE f.TIPO
                                    WHEN '0' THEN 'Medicamento'
                                    WHEN '1' THEN 'Material'
                                    WHEN '3' THEN 'Dieta' END
                             ) as apresentacao,
                            (CASE f.TIPO
                                    WHEN '0' THEN '3'
                                    WHEN '1' THEN '4'
                                    WHEN '3' THEN '5' END
                            ) as ordem,
                            1 as cod,
                            sum(f.QUANTIDADE) as qtd,
                            sum(f.QUANTIDADE * f.VALOR_FATURA) as valor_receber,
                            sum(f.QUANTIDADE * f.VALOR_CUSTO) as custo,
                            sum((f.QUANTIDADE * f.VALOR_CUSTO)*(f.DESCONTO_ACRESCIMO/100)) as desc_acrescimo
		FROM
                        faturamento as f  INNER JOIN
                        `catalogo` B ON (f.CATALOGO_ID = B.ID)
                        inner join fatura fa on (f.FATURA_ID=fa.ID) INNER JOIN
                        clientes as cli on (fa.PACIENTE_ID = cli.idClientes)
		WHERE
                        
                        f.TIPO IN (0,1,3) 
                        and cli.empresa = {$ur} and 
                        fa.DATA between '{$inicio}' and '{$fim}' and
                        fa.ORCAMENTO=0
                         and f.CANCELADO_POR is NULL
                GROUP BY	
                        f.CATALOGO_ID

	UNION		
		SELECT 
                        f.*,
                        fa.OBS,
                        B.item AS principio,
                        ('Servi&ccedil;os') AS apresentacao,	
                        1 as ordem,						  
                         B.id as cod,
                         sum(f.QUANTIDADE) as qtd,
                         sum(f.QUANTIDADE * f.VALOR_FATURA) as valor_receber,
                         sum(f.QUANTIDADE * f.VALOR_CUSTO) as custo,
                         sum((f.QUANTIDADE * f.VALOR_CUSTO)*(f.DESCONTO_ACRESCIMO/100)) as desc_acrescimo
		FROM 
                        faturamento as f INNER JOIN				      
                        `cobrancaplanos` B ON (f.CATALOGO_ID =B.id) inner join 
                        clientes as c  on (c.idClientes = f.PACIENTE_ID)  inner join
                        fatura fa on (f.FATURA_ID=fa.ID)
                        
                WHERE 

                       
                        f.TIPO =2 and 
                        c.empresa = {$ur} and 
                        fa.DATA between '{$inicio}' and '{$fim}' and 
                        f.TABELA_ORIGEM = 'cobrancaplanos' and
                        fa.ORCAMENTO=0
                         and f.CANCELADO_POR is NULL
                 GROUP BY	
                        B.id
           UNION		
		SELECT 
                        f.*,
                        fa.OBS,
                        B.item AS principio,
                        ('Servi&ccedil;os') AS apresentacao,	
                        1 as ordem,						  
                         B.id as cod,
                         sum(f.QUANTIDADE) as qtd,
                         sum(f.QUANTIDADE * f.VALOR_FATURA) as valor_receber,
                         sum(f.QUANTIDADE * f.VALOR_CUSTO) as custo,
                         sum((f.QUANTIDADE * f.VALOR_CUSTO)*(f.DESCONTO_ACRESCIMO/100)) as desc_acrescimo
		FROM 
                        faturamento as f INNER JOIN	
                         valorescobranca as vc on (f.CATALOGO_ID =vc.id) inner join 
                        `cobrancaplanos` B ON (vc.idCobranca = B.id) inner join 
                        clientes as c  on (c.idClientes = f.PACIENTE_ID)  inner join
                        fatura fa on (f.FATURA_ID=fa.ID)
                WHERE 
                                              
                        f.TIPO =2 and 
                        c.empresa = {$ur} and 
                        fa.DATA between '{$inicio}' and '{$fim}' and 
                        f.TABELA_ORIGEM = 'valorescobranca' and
                        fa.ORCAMENTO=0
                         and f.CANCELADO_POR is NULL
                 GROUP BY	
                        B.id
                        
             UNION		
		SELECT 
                        f.*,
                        fa.OBS,
                        B.item AS principio,
                        ('Equipamento') AS apresentacao,	
                        2 as ordem,						  
                         B.id as cod,
                         sum(f.QUANTIDADE) as qtd,
                         sum(f.QUANTIDADE * f.VALOR_FATURA) as valor_receber,
                         sum(f.QUANTIDADE * f.VALOR_CUSTO) as custo,
                          sum((f.QUANTIDADE * f.VALOR_CUSTO)*(f.DESCONTO_ACRESCIMO/100)) as desc_acrescimo
		FROM 
                        faturamento as f INNER JOIN				      
                        `cobrancaplanos` B ON (f.CATALOGO_ID =B.id) inner join 
                        clientes as c  on (c.idClientes = f.PACIENTE_ID)  inner join
                        fatura fa on (f.FATURA_ID=fa.ID)
                WHERE 

                       
                        f.TIPO =5 and 
                        c.empresa = {$ur} and 
                        fa.DATA between '{$inicio}' and '{$fim}' and 
                        f.TABELA_ORIGEM = 'cobrancaplanos' and
                        fa.ORCAMENTO=0
                         and f.CANCELADO_POR is NULL
                 GROUP BY	
                        B.id
           UNION		
		SELECT 
                        f.*,
                        fa.OBS,
                        B.item AS principio,
                        ('Equipamento') AS apresentacao,	
                        2 as ordem,						  
                         B.id as cod,
                         sum(f.QUANTIDADE) as qtd,
                         sum(f.QUANTIDADE * f.VALOR_FATURA) as valor_receber,
                         sum(f.QUANTIDADE * f.VALOR_CUSTO) as custo,
                          sum((f.QUANTIDADE * f.VALOR_CUSTO)*(f.DESCONTO_ACRESCIMO/100)) as desc_acrescimo
                        
		FROM 
                        faturamento as f INNER JOIN	
                         valorescobranca as vc on (f.CATALOGO_ID =vc.id) inner join 
                        `cobrancaplanos` B ON (vc.idCobranca = B.id) inner join 
                        clientes as c  on (c.idClientes = f.PACIENTE_ID)  inner join
                        fatura fa on (f.FATURA_ID=fa.ID)
                WHERE 
                                              
                        f.TIPO =5 and 
                        c.empresa = {$ur} and 
                        fa.DATA between '{$inicio}' and '{$fim}' and 
                        f.TABELA_ORIGEM = 'valorescobranca' and
                        fa.ORCAMENTO=0
                         and f.CANCELADO_POR is NULL
                 GROUP BY	
                        B.id
                UNION		
		SELECT 
                        f.*,
                        fa.OBS,
                        B.item AS principio,
                        ('Gasoterapia') AS apresentacao,	
                        6 as ordem,						  
                         B.id as cod,
                         sum(f.QUANTIDADE) as qtd,
                         sum(f.QUANTIDADE * f.VALOR_FATURA) as valor_receber,
                         sum(f.QUANTIDADE * f.VALOR_CUSTO) as custo,
                            sum((f.QUANTIDADE * f.VALOR_CUSTO)*(f.DESCONTO_ACRESCIMO/100)) as desc_acrescimo
		FROM 
                        faturamento as f INNER JOIN				      
                        `cobrancaplanos` B ON (f.CATALOGO_ID =B.id) inner join 
                        clientes as c  on (c.idClientes = f.PACIENTE_ID)  inner join
                        fatura fa on (f.FATURA_ID=fa.ID)
                WHERE 

                       
                        f.TIPO =6 and 
                        c.empresa = {$ur} and 
                        fa.DATA between '{$inicio}' and '{$fim}' and 
                        f.TABELA_ORIGEM = 'cobrancaplanos' and
                        fa.ORCAMENTO=0
                         and f.CANCELADO_POR is NULL
                 GROUP BY	
                        B.id
           UNION		
		SELECT 
                        f.*,
                        fa.OBS,
                        B.item AS principio,
                        ('Gasoterapia') AS apresentacao,	
                        6 as ordem,						  
                         B.id as cod,
                         sum(f.QUANTIDADE) as qtd,
                         sum(f.QUANTIDADE * f.VALOR_FATURA) as valor_receber,
                         sum(f.QUANTIDADE * f.VALOR_CUSTO) as custo,
                            sum((f.QUANTIDADE * f.VALOR_CUSTO)*(f.DESCONTO_ACRESCIMO/100)) as desc_acrescimo
		FROM 
                        faturamento as f INNER JOIN	
                         valorescobranca as vc on (f.CATALOGO_ID =vc.id) inner join 
                        `cobrancaplanos` B ON (vc.idCobranca = B.id) inner join 
                        clientes as c  on (c.idClientes = f.PACIENTE_ID)  inner join
                        fatura fa on (f.FATURA_ID=fa.ID)
                WHERE 
                                              
                        f.TIPO =6 and 
                        c.empresa = {$ur} and 
                        fa.DATA between '{$inicio}' and '{$fim}' and 
                        f.TABELA_ORIGEM = 'valorescobranca' and
                        fa.ORCAMENTO=0
                         and f.CANCELADO_POR is NULL
                 GROUP BY	
                        B.id
                    ORDER BY 
                    ordem,tipo,principio,cod";
                    

		$html.= "<table  width=100% class='mytable'>";
		
		$result = mysql_query($sql);
		$total=0.00;
		$cont_cor=0;
		$subtotal=0.00;
		$subtotal_equipamento=0.00;
		$subtotal_material=0.00;
		$subtotal_medicamento=0.00;
		$subtotal_gas=0.00;
		$subtotal_dieta=0.00;
                
                $custo=0.00;
                $custo1=0.00;
		$subcusto_equipamento=0.00;
		$subcusto_material=0.00;
		$subcusto_medicamento=0.00;
		$subcusto_gas=0.00;
		$subcusto_dieta=0.00;
		$subcusto_servico=0.00;
		$subtotal_servico=0.00;
	     $cont1=0;
	     $cont2=0;
	     $cont3=0;
	     $cont4=0;
	     $cont5=0;
	     $cont6=0;
		while($row = mysql_fetch_array($result)){
			
			$subtotal +=$row['valor_receber']+$row['desc_acrescimo'];
			
			
			
			if($row['TIPO'] == '6'){
				$subtotal_gas +=$row['valor_receber']+$row['desc_acrescimo'];
                                $subcusto_gas +=$row['custo'];
                                $custo=$subcusto_gas;
				$parcial=$subtotal_gas;
                                
				$cont5++;
				$n=$row['cod'];
				$tip = $row['TIPO'];
				$tipo_tot="GASOTERAPIA TOTAL ";
                                $tipo_custo="CUSTO ";
			
			}
			
			if($row['TIPO'] == '5'){
				$subtotal_equipamento +=$row['valor_receber']+$row['desc_acrescimo'];
				$parcial=$subtotal_equipamento;
                                $subcusto_equipamento +=$row['custo'];
                                $custo=$subcusto_equipamento;
				$cont4++;
				$n=$row['cod'];
				$tip = $row['TIPO'];
				$tipo_tot="EQUIPAMENTO TOTAL ";
                                $tipo_custo="CUSTO ";
				
			}
                        if($row['TIPO'] == '3'){
				$subtotal_dieta +=$row['valor_receber']+$row['desc_acrescimo'];
				$cont6++;
				$n="Bras. ".$row['NUMERO_TISS'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_dieta;
                                $subcusto_dieta +=$row['custo'];
                                $custo=$subcusto_dieta;
				$tipo_tot="DIETA TOTAL ";
                                $tipo_custo="CUSTO ";
			}
			if($row['TIPO'] == '2'){
				$subtotal_servico +=$row['valor_receber']+$row['desc_acrescimo'];
				$cont3++;
				$n=$row['cod'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_servico;
                                $subcusto_servico +=$row['custo'];
                                $custo=$subcusto_servico;
				$tipo_tot="SERVI&Ccedil;O TOTAL ";
                                $tipo_custo="CUSTO ";
			}
			if($row['TIPO'] == '1'){
				$subtotal_material +=$row['valor_receber']+$row['desc_acrescimo'];;
				$cont1++;
				$n="Simp. ".$row['NUMERO_TISS'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_material;
				$tipo_tot="MATERIAL TOTAL ";
                                 $subcusto_material +=$row['custo'];;
                                $custo=$subcusto_material;
                                $tipo_custo="CUSTO ";
			}
			if($row['TIPO'] == '0'){
				$subtotal_medicamento +=$row['valor_receber']+$row['desc_acrescimo'];
				$cont2++;
				$n="Bras. ".$row['NUMERO_TISS'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_medicamento;
				$tipo_tot="MEDICAMENTO TOTAL ";
                                
                                $subcusto_medicamento +=$row['custo'];
                                $custo=$subcusto_medicamento;
                                $tipo_custo="CUSTO ";
			}
			
			if( $cont_cor%2 ==0 ){
				$cor = '';
			}else{
				$cor="bgcolor='#EEEEEE'";
			}
			
			
		if($row['UNIDADE']=='1')
		  {     $und='Comprimido';
		     }
		     
		     if($row['UNIDADE']=='2')
		     {
		     	$und='Caixa';
		     }
		     
		     if($row['UNIDADE']=='3')
		     {
		     	$und='Frasco';
		     }
		     if($row['UNIDADE']=='4')
		     {
		     	$und='Ampola';
		     }
		     if($row['UNIDADE']=='5')
		     {
		     	$und='Bisnaga';
		     }
		     if($row['UNIDADE']=='6')
		     {
		     	$und='Sach&ecirc;';
		     }
		     if($row['UNIDADE']=='7')
		     {
		     	$und='Flaconete';
		     }
		     if($row['UNIDADE']=='8')
		     {
		     	$und='Pacote';
		     }
		     if($row['UNIDADE']=='9')
		     {
		     	$und='Di&aacute;ria';
		     }
		     if($row['UNIDADE']=='10')
		     {
		     	$und='Sess&atilde;o';
		     }
		     if($row['UNIDADE']=='11')
		     {
		     	$und='Visita';
		     }if($row['UNIDADE']=='12')
		     {
		     	$und='Unidade';
		     }if($row['UNIDADE']=='13')
		     {
		     	$und='Lata';
		     }
		     
			
		     if($cont5 == 1){
		     	if($tip != $tipo && $cont_cor != 0){
                                  $submargem_rs=$par-$custo1;
                                  $submargem_p=($submargem_rs/$par)*100;
		     		$html.= "<tr>
                                      <td></td>
                                     <td><b>SUBTOTAL</b></td> 
                                     <td></td>
                                     <td><b>FATURADO</b></td>
                                     <td><b>R$".number_format($par,2,',','.')."</b></td>
                                     <td></td>
                                     <td><b>CUSTO</b></td>
                                     <td><b>R$".number_format($custo1,2,',','.')."</b></td>
                                     <td><b>R$".number_format($submargem_rs,2,',','.')."</b></td>
                                     <td><b>".number_format($submargem_p,2,',','.')."%</b></td>
                                    </tr>";
                                $html .="<tr ><td><td></tr>";
			
		     	}
                        
		     	$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='8' style='font-size:12px;float:right;border:1px solid #000;'>Gasoterapia</td><td colspan='2'>Margem</td></tr>";
		     	$html.= "<tr bgcolor=bgcolor='#cccccc'>
		     	<td style='font-size:12px;float:right;'>N&deg;</td>
		     	<td style='font-size:12px;float:right;'>Unidade</td>
		     	<td style='font-size:12px;float:right;'>Item</td>
		     	<th style='font-size:12px;float:right;'>Qtd</th>
		     	<th style='font-size:12px;float:right;'>Pre&ccedil;o Unt.</th>
		     	<th style='font-size:12px;float:right;'>Pre&ccedil;o Tot.</th>
                        <th style='font-size:12px;float:right;'>Custo Unit.</th>
                        <th style='font-size:12px;float:right;'>Custo Total</th>
                        <th style='font-size:12px;float:right;'>R$</th>
                        <th style='font-size:12px;float:right;'>%</th>
		     	</tr>";
		     
		     
		     }
                     if($cont4 == 1){
				if($tip != $tipo && $cont_cor != 0){
					 $submargem_rs=$par-$custo1;
                                  $submargem_p=($submargem_rs/$par)*100;
		     		$html.= "<tr>
                                      <td></td>
                                     <td><b>SUBTOTAL</b></td> 
                                     <td></td>
                                     <td><b>FATURADO</b></td>
                                     <td><b>R$".number_format($par,2,',','.')."</b></td>
                                     <td></td>
                                     <td><b>CUSTO</b></td>
                                     <td><b>R$".number_format($custo1,2,',','.')."</b></td>
                                     <td><b>R$".number_format($submargem_rs,2,',','.')."</b></td>
                                     <td><b>".number_format($submargem_p,2,',','.')."%</b></td>
                                    </tr>";
                                $html .="<tr ><td><td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='8' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>MATERIAIS PERMANENTES</td><td colspan='2'>Margem</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
                                <th style='font-size:12px;float:right;'>Custo Unit.</th>
                                <th style='font-size:12px;float:right;'>Custo Total</th>
                                <th style='font-size:12px;float:right;'>R$</th>
                        <th style='font-size:12px;float:right;'>%</th>
				</tr>";
				
			
			}
			if($cont6 == 1 ){
				if($tip != $tipo && $cont_cor != 0){
					 $submargem_rs=$par-$custo1;
                                  $submargem_p=($submargem_rs/$par)*100;
		     		$html.= "<tr>
                                      <td></td>
                                     <td><b>SUBTOTAL</b></td> 
                                     <td></td>
                                     <td><b>FATURADO</b></td>
                                     <td><b>R$".number_format($par,2,',','.')."</b></td>
                                     <td></td>
                                     <td><b>CUSTO</b></td>
                                     <td><b>R$".number_format($custo1,2,',','.')."</b></td>
                                     <td><b>R$".number_format($submargem_rs,2,',','.')."</b></td>
                                     <td><b>".number_format($submargem_p,2,',','.')."%</b></td>
                                    </tr>";
                                    $html .="<tr><td><td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='8' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>DIETA</td><td colspan='2'>Margem</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
                                <th style='font-size:12px;float:right;'>Custo Unit.</th>
                                <th style='font-size:12px;float:right;'>Custo Total</th>
                                <th style='font-size:12px;float:right;'>R$</th>
                               <th style='font-size:12px;float:right;'> %</th>
			
			
				</tr>";
			
			}
			if($cont3 == 1 ){
				if($tip != $tipo && $cont_cor != 0){
					 $submargem_rs=$par-$custo1;
                                  $submargem_p=($submargem_rs/$par)*100;
		     		$html.= "<tr>
                                      <td></td>
                                     <td><b>SUBTOTAL</b></td> 
                                     <td></td>
                                     <td><b>FATURADO</b></td>
                                     <td><b>R$".number_format($par,2,',','.')."</b></td>
                                     <td></td>
                                     <td><b>CUSTO</b></td>
                                     <td><b>R$".number_format($custo1,2,',','.')."</b></td>
                                     <td><b>R$".number_format($submargem_rs,2,',','.')."</b></td>
                                     <td><b>".number_format($submargem_p,2,',','.')."%</b></td>
                                    </tr>";
                                   $html .="<tr ><td><td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='8' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>SERVI&Ccedil;OS</td><td colspan='2'>Margem</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
                                <th style='font-size:12px;float:right;'>Custo Unit.</th>
                                <th style='font-size:12px;float:right;'>Custo Total</th>
                                <th style='font-size:12px;float:right;'>R$</th>
                        <th style='font-size:12px;float:right;'>%</th>
			</tr>";
                                
			
			}
			if($cont1 == 1){
				if($tip != $tipo && $cont_cor != 0){
					 $submargem_rs=$par-$custo1;
                                  $submargem_p=($submargem_rs/$par)*100;
		     		$html.= "<tr>
                                      <td></td>
                                     <td><b>SUBTOTAL</b></td> 
                                     <td></td>
                                     <td><b>FATURADO</b></td>
                                     <td><b>R$".number_format($par,2,',','.')."</b></td>
                                     <td></td>
                                     <td><b>CUSTO</b></td>
                                     <td><b>R$".number_format($custo1,2,',','.')."</b></td>
                                     <td><b>R$".number_format($submargem_rs,2,',','.')."</b></td>
                                     <td><b>".number_format($submargem_p,2,',','.')."%</b></td>
                                    </tr>";
			$html .="<tr ><td><td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='8' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>MATERIAIS</td><td colspan='2'>Margem</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot. </th>
                                <th style='font-size:12px;float:right;'>Custo Unit.</th>
                                <th style='font-size:12px;float:right;'>Total</th>
                                <th style='font-size:12px;float:right;'>R$</th>
                        <th style='font-size:12px;float:right;'> %</th>
				</tr>";
			
			}
			if($cont2 == 1){
				if($tip != $tipo && $cont_cor != 0 ){
					 $submargem_rs=$par-$custo1;
                                  $submargem_p=($submargem_rs/$par)*100;
		     		$html.= "<tr>
                                      <td></td>
                                     <td><b>SUBTOTAL</b></td> 
                                     <td></td>
                                     <td><b>FATURADO</b></td>
                                     <td><b>R$".number_format($par,2,',','.')."</b></td>
                                     <td></td>
                                     <td><b>CUSTO</b></td>
                                     <td><b>R$".number_format($custo1,2,',','.')."</b></td>
                                     <td><b>R$".number_format($submargem_rs,2,',','.')."</b></td>
                                     <td><b>".number_format($submargem_p,2,',','.')."%</b></td>
                                    </tr>";
			$html .="<tr ><td><td></tr>";
			
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='8' style='font-size:12px;float:right;bgcolor:#EEEEEE; '>MEDICAMENTOS</td><td colspan='2'>Margem</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Tot.</th>
                                <th style='font-size:12px;float:right;'>Custo Unit.</th>
                                <th style='font-size:12px;float:right;'>Custo Total</th>
                                <th style='font-size:12px;float:right;'> R$</th>
                                <th style='font-size:12px;float:right;'> %</th>
				</tr>";
			
			}
			
			
			//
			 $tipo = $tip;
			$par= $parcial;
                        $custo1= $custo;
			$tipo_tot1=$tipo_tot;
                        $tipo_custo1=$tipo_custo;
			$html.= "<tr $cor>
			<td>{$n}</td>
			<td>{$und}</td>
			<td width='50%'>".utf8_decode($row['principio'])."</td>
			<td>{$row['qtd']}</td>
			<td>R$ </td>";
			$vt=$row['valor_receber']+$row['desc_acrescimo'];
			$vtotal += $vt; 
			$html.= "<td>R$ ".number_format($vt,2,',','.')."</td>";
                        $html.= "<td>R$ </td>";
                        $vt_custo = $row['custo'];
                        $vtotal_custo += $vt_custo;
                        $marg_rs = $vt - $vt_custo;
                        $marg_porcento = ($marg_rs/$vt)*100;
                        $html.= "<td>R$ ".number_format($vt_custo,2,',','.')."</td>";
                        $html.= "<td>R$ ".number_format($marg_rs,2,',','.')."</td>";
                        $html.= "<td>".number_format($marg_porcento,2,',','.')."%</td></tr>";
			
			if($cont4 == 1){
				$cont4++;
			
			}
			if($cont3 == 1){
				$cont3++;
			
			}
			if($cont1 == 1){
				$cont1++;
			
			}
			if($cont2 == 1){
				$cont2++;
			
			}
			$cont_cor++;
			$obs=$row['OBS'];
		}
		
		
                 $submargem_rs=$par-$custo1;
                                  $submargem_p=$submargem_rs/$par;
		     		  $html.= "<tr>
                                      <td></td>
                                     <td><b>SUBTOTAL</b></td> 
                                     <td></td>
                                    <td><b>FATURADO</b></td>
                                     <td><b>R$".number_format($par,2,',','.')."</b></td>
                                     <td><b>CUSTO</b></td>
                                      <td></td>
                                     <td><b>R$".number_format($custo1,2,',','.')."</b></td>
                                     <td><b>R$".number_format($submargem_rs,2,',','.')."</b></td>
                                     <td><b>".number_format($submargem_p,2,',','.')."%</b></td>
                                    </tr>";
                                  $ir = $vtotal*0.035;
                                  $issqn = $vtotal*0.02;
                                  $piscofinscsll=$vtotal*0.0495;
                                  $total_encargos = $vtotal*0.1045;
                                  $faturado_liquido = $vtotal-$total_encargos;
                                  $margem_contribuicao = $faturado_liquido - $vtotal_custo;
                                  $tir=($margem_contribuicao/$vtotal_custo)*100;
                                  $repase_40 = $margem_contribuicao*0.4;
                                  $porcento_margem = ($margem_contribuicao/$vtotal)*100;
                $html .="<tr><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td></tr>";                  
                $html .= "<tr>
                    <td></td>
                    <td><b>FECHAMENTO</b></td>
                    <td></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'><b>TOTAL FATURADO</b></td>
                    <td style='border-bottom: 1px solid' bgcolor='#cccccc'></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'><b>R$".number_format($vtotal,2,',','.')."</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
		$html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style='border-bottom: 1px solid'>IR</td>
                    <td style='border-bottom: 1px solid'>3,5%</td>
                    <td style='border-bottom: 1px solid'>R$".number_format($ir,2,',','.')."</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
                $html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style='border-bottom: 1px solid'>ISSQN</td>
                    <td style='border-bottom: 1px solid'>2%</td>
                    <td style='border-bottom: 1px solid'>R$".number_format($issqn,2,',','.')."</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
                $html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style='border-bottom: 1px solid'>PIS/COFINS/CSLL</td>
                    <td style='border-bottom: 1px solid'>4,95%</td>
                    <td style='border-bottom: 1px solid'>R$".number_format($piscofinscsll,2,',','.')."</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
                $html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style='border-bottom: 1px solid' bgcolor='#cccccc'><b>TOTAL ENCARGOS</b></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'><b>8,15%</b></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'> <b>R$".number_format($total_encargos,2,',','.')."</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
		$html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style='border-bottom: 1px solid'>FATURADO LIQUIDO</td>
                    <td style='border-bottom: 1px solid'></td>
                    <td style='border-bottom: 1px solid'>R$".number_format($faturado_liquido,2,',','.')."</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
                $html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style='border-bottom: 1px solid' bgcolor='#cccccc'><b>TOTAL CUSTO</b></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'><b>R$".number_format($vtotal_custo,2,',','.')."</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
		$html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style='border-bottom: 1px solid'>MARGEM DE CONTRIBUI&Ccedil;&Atilde;O</td>
                    <td style='border-bottom: 1px solid'></td>
                    <td style='border-bottom: 1px solid'>R$".number_format($margem_contribuicao,2,',','.')."</td>
                    <td> ".number_format($porcento_margem,2,',','.')."%</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
                $html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style='border-bottom: 1px solid'>RENTABILIDADE</td>
                    <td style='border-bottom: 1px solid'></td>
                    <td style='border-bottom: 1px solid'>".number_format($tir,2,',','.')."%</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
                $html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'><b>REPASSE UNIDADE</b></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'><b>40%</b></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'><b>R$".number_format($repase_40,2,',','.')."</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";

                $html.= "<tr bgcolor='#cccccc' ><td colspan='7'  ><b>OBSERVA&Ccedil;&Atilde;O: </br> {$obs}</td></tr>";
		
		$html.= "</table>";
	echo $html;
	//echo $sql;
	//$paginas []= $header.$html.= "</form>";
	
	
	//print_r($paginas);
	//print_r($paginas);
	/*
	$mpdf=new mPDF('pt','A4',9);
	$mpdf->SetHeader('página {PAGENO} de {nbpg}');
	$ano = date("Y");
	$mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
	$mpdf->WriteHTML("<html><body>");
	$flag = false;
	foreach($paginas as $pag){
		if($flag) $mpdf->WriteHTML("<formfeed>");
		$mpdf->WriteHTML($pag);
		$flag = true;
	}
	$mpdf->WriteHTML("</body></html>");
	$mpdf->Output('prescricao_avaliacao.pdf','I');
	exit;
	
	*/
	
	/*$mpdf=new mPDF('pt','A4',9);
	$mpdf->WriteHTML($html);
	$mpdf->Output('pacientes.pdf','I');*/
	
	
	
	}





}

$p = new Exportar_fatura_condensada();
$p->fatura_condensada($_GET['ur'],$_GET['inicio'],$_GET['fim']);

?>
   
