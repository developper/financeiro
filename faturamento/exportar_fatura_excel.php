<?php
$id = session_id();
if(empty($id))
	session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');

use App\Models\Faturamento\UnidadeMedida;

header("Content-type: application/x-msexce");
header("Content-type: application/force-download;");
header("Content-Disposition: attachment; filename=fatura.xls");
header("Pragma: no-cache");
//ini_set('display_errors',1);
class imprimir_fatura{


	public function percentualRepaseUnidadeRegional($id)
	{
		$sql="SELECT
                    (empresas.percentual/100) as percentual
                    FROM
                    fatura
                    INNER JOIN clientes ON fatura.PACIENTE_ID = clientes.idClientes
                    INNER JOIN empresas ON clientes.empresa = empresas.id
                    WHERE
                    fatura.ID ='{$id}'";
		$percentual = mysql_result(mysql_query($sql), 0, 0);

		return $percentual;

	}

	public function cabecalho($id){


		$sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		UPPER(u.nome) AS usuario,DATE_FORMAT(f.DATA,'%Y-%m-%d') as DATA,f.DATA_INICIO,f.DATA_FIM,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo1,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresan,
		c.`nascimento`,
		c.diagnostico,
		c.codigo,
		p.nome as Convenio,p.id,c.*,
		NUM_MATRICULA_CONVENIO,
		cpf
		FROM
		fatura as f inner join
		clientes AS c on (f.PACIENTE_ID = c.idClientes) LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN 
		planosdesaude as p ON (p.id = c.convenio) INNER JOIN usuarios as u on (f.USUARIO_ID = u.idUsuarios)
		
		WHERE
		f.ID = {$id}
		ORDER BY
		c.nome DESC LIMIT 1";

		$result = mysql_query($sql);
		$result2 = mysql_query($sql2);
		$html.= "<table style='width:100%;' >";
		while($pessoa = mysql_fetch_array($result2)){
			foreach($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);

			}
			$idade = join("/",array_reverse(explode("-",$pessoa['nascimento'])))."(".$pessoa['idade']." anos)";

			if($idade == "00/00/0000( anos)"){
				$idade='';
			}
			$html.= "<tr bgcolor='#EEEEEE'>";
			$html.= "<td  style='width:60%;'><b>PACIENTE:</b></td>";
			$html.= "<td style='width:20%;' ><b>SEXO </b></td>";
			$html.= "<td style='width:30%;'><b>IDADE:</b></td>";
			$html.= "</tr>";
			$html.= "<tr>";
			$html.= "<td style='width:60%;'>".utf8_decode($pessoa['paciente'])."</td>";
			$html.= "<td style='width:20%;'>{$pessoa['sexo1']}</td>";
			$html.= "<td style='width:30%;'>{$idade}</td>";
			$html.= "</tr>";

			$html.= "<tr bgcolor='#EEEEEE'>";

			$html.= "<td style='width:60%;'><b>UNIDADE REGIONAL:</b></td>";
			$html.= "<td style='width:20%;'><b>CONV&Ecirc;NIO </b></td>";
			$html.= "<td style='width:30%;'><b>MATR&Iacute;CULA </b></td>";
			$html.= "</tr>";
			$html.= "<tr>";

			$html.= "<td style='width:60%;'>{$pessoa['empresan']}</td>";
			$html.= "<td style='width:20%;'>".strtoupper($pessoa['Convenio'])."</td>";
			$html.= "<td style='width:30%;'>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
			$html.= "</tr>";

			$html.= "<tr bgcolor='#EEEEEE'>";

			$html.= "<td style='width:60%;'><b>Faturista:</b></td>";
			$html.= "<td style='width:20%;'><b>Data faturado: </b></td>";
			$html.= "<td style='width:30%;'><b>Per&iacute;odo</b></td>";
			$html.= "</tr>";
			$html.= "<tr>";

			$html.= "<td style='width:60%;'>".utf8_decode($pessoa['usuario'])."</td>";
			$html.= "<td style='width:20%;'>".join("/",array_reverse(explode("-",$pessoa['DATA'])))."</td>";
			$html.= "<td style='width:30%;'>".join("/",array_reverse(explode("-",$pessoa['DATA_INICIO'])))." at&eacute; ".join("/",array_reverse(explode("-",$pessoa['DATA_FIM'])))."</td>";
			$html.= "</tr>";

			$html.= "<tr bgcolor='#EEEEEE'>";

			$html.= "<td style='width:60%;'><b>CID:</b></td>";
			$html.= "<td style='width:20%;'><b>CONTA MEDERI: </b></td>";
			$html.= "<td style='width:30%;'></td>";
			$html.= "</tr>";
			$html.= "<tr>";

			$html.= "<td style='width:60%;'>{$pessoa['diagnostico']}</td>";
			$html.= "<td style='width:20%;'>{$pessoa['codigo']}</td>";
			$html.= "<td style='width:30%;'></td>";
			$html.= "</tr>";
			$html.= " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";

			$this->plan =  $pessoa['id'];
		}
		$html.= "</table>";

		return $html;
	}


	public function detalhe_fatura($id,$av,$zerados){
		//$html .="<form>";
		if($_GET['av'] == 0){
			$html .="<h1>".htmlentities( "Fatura de Assistência Domiciliar ")."</h1>";
		}else if($_GET['av'] == 1) {
			$html .="<h1>".htmlentities( "Orçamento Inicial ")."</h1>";
		}else if($_GET['av'] == 2){
			$html .="<h1>".htmlentities( "Orçamento de Prorrogação")."</h1>";
		}else if($_GET['av'] == 3){
			$html .="<h1>".htmlentities( "Orçamento Aditivo")."</h1>";
		}

		$html .= $this->cabecalho($id);
		$html .="<br></br>";
		if ($zerados == 0){
			$condicao = "AND VALOR_FATURA != '0.00' ";
		}else{
			$condicao = " ";
		}
		$sql="
					SELECT
							f.*,
							fa.OBS,
							IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),B.DESC_MATERIAIS_FATURAR) AS principio,
							(CASE f.TIPO
							WHEN '0' THEN 'Medicamento'
							WHEN '1' THEN 'Material'
							 WHEN '3' THEN 'Dieta' END) as apresentacao,
							 (CASE f.TIPO
							WHEN '0' THEN '3'
							WHEN '1' THEN '4'
							 WHEN '3' THEN '5' END) as ordem,
							1 as cod,
							fa.imposto_iss
		    FROM
					faturamento as f  INNER JOIN
					`catalogo` B ON (f.CATALOGO_ID = B.ID)
					inner join fatura fa on (f.FATURA_ID=fa.ID)
		    WHERE
			f.FATURA_ID= {$id} and
			f.TIPO IN (0,1,3) {$condicao}
                        and f.CANCELADO_POR is NULL
							
		UNION		
			SELECT 
                            f.*,
                            fa.OBS,
			    vc.DESC_COBRANCA_PLANO AS principio,                           
		           ('Servi&ccedil;os') AS apresentacao,	
                            1 as ordem,						  
			    vc.COD_COBRANCA_PLANO as cod,
							fa.imposto_iss
		      FROM 
                            faturamento as f INNER JOIN				      
                             valorescobranca as vc on (f.CATALOGO_ID =vc.id) inner join
                             fatura fa on (f.FATURA_ID=fa.ID)
                      WHERE 
                            f.FATURA_ID= {$id} and 
                            f.TIPO =2 and 
                            f.TABELA_ORIGEM='valorescobranca'	{$condicao}
                              and f.CANCELADO_POR is NULL
                     UNION           
                      SELECT 
                            f.*,
                            fa.OBS,
			    vc.item AS principio,                           
		           ('Servi&ccedil;os') AS apresentacao,	
                            1 as ordem,						  
			    '' as cod,
							fa.imposto_iss
		      FROM 
                            faturamento as f INNER JOIN				      
                             cobrancaplanos as vc on (f.CATALOGO_ID =vc.id) inner join
                             fatura fa on (f.FATURA_ID=fa.ID)
                      WHERE 
                            f.FATURA_ID= {$id} and 
                            f.TIPO =2 and 
                            f.TABELA_ORIGEM='cobrancaplano'	{$condicao}
                             and f.CANCELADO_POR is NULL
			
			UNION
			SELECT DISTINCT
			                 f.*,
                                         fa.OBS,
                                        CO.item AS principio,
                                        
                                        ('Equipamentos') AS apresentacao,
                                            2 as ordem,	
                                         '' as cod,
							fa.imposto_iss
					      	
			FROM 
                                    faturamento as f INNER JOIN
                                    cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)                                    
                                    inner join fatura fa on (f.FATURA_ID=fa.ID)
						
		      WHERE 
                                    f.FATURA_ID= {$id} and
                                    f.TIPO =5  and  
                                    f.TABELA_ORIGEM='cobrancaplano' {$condicao}	
                                     and f.CANCELADO_POR is NULL
                                
                        UNION
			SELECT DISTINCT
			                 f.*,
                                         fa.OBS,
                                        CO.DESC_COBRANCA_PLANO AS principio,                  
                                                                          
                                        ('Equipamentos') AS apresentacao,
                                            2 as ordem,	
                                         CO.COD_COBRANCA_PLANO as cod,
							fa.imposto_iss
					      	
			FROM 
                                    faturamento as f INNER JOIN
                                    valorescobranca CO ON (f.CATALOGO_ID = CO.`id`)                                    
                                    inner join fatura fa on (f.FATURA_ID=fa.ID)
						
		      WHERE 
                                    f.FATURA_ID= {$id} and
                                    f.TIPO =5  and  
                                    f.TABELA_ORIGEM='valorescobranca' {$condicao}
                                     and f.CANCELADO_POR is NULL

					
		UNION
			SELECT DISTINCT
			                 f.*,fa.OBS,
					 CO.item AS principio,				       
					   
					  ('Gasoterapia') AS apresentacao,
					 6 as ordem,	
					'' as cod,
							fa.imposto_iss
				FROM 
                                        faturamento as f INNER JOIN
                                        cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`) 
                                        inner join fatura fa on (f.FATURA_ID=fa.ID)
				WHERE 
					     f.FATURA_ID= {$id} and
					      f.TIPO =6  and f.TABELA_ORIGEM='cobrancaplano' {$condicao}
                                               and f.CANCELADO_POR is NULL
              UNION
			SELECT DISTINCT
			                 f.*,fa.OBS,
					 vc.DESC_COBRANCA_PLANO AS principio,				       
					  
					  ('Gasoterapia') AS apresentacao,
					 6 as ordem,	
					 vc.COD_COBRANCA_PLANO as cod,
							fa.imposto_iss
					      	
				FROM 
                                        faturamento as f  inner join 
                                        valorescobranca as vc on (vc.id = f.CATALOGO_ID) inner join 
                                        fatura fa on (f.FATURA_ID=fa.ID)
				WHERE 
					     f.FATURA_ID= {$id} and
					      f.TIPO =6  and 
                                              f.TABELA_ORIGEM='valorescobranca' {$condicao}	
                                               and f.CANCELADO_POR is NULL
					      			
	ORDER BY 
        ordem,
        tipo,
        principio";



		///////////////
		$html.= "<table  width=100% class='mytable'>
                    
		<tr bgcolor='#EEEEEE'>
		<td colspan='6'>* Pre&ccedil;o Total já com o desconto ou acr&eacute;scimo</td>
		</tr>";
		$result = mysql_query($sql);
		$total=0.00;
		$cont_cor=0;
		$subtotal=0.00;
		$subtotal_equipamento=0.00;
		$subtotal_material=0.00;
		$subtotal_medicamento=0.00;
		$subtotal_gas=0.00;
		$subtotal_dieta=0.00;

		$custo=0.00;
		$custo1=0.00;
		$subcusto_equipamento=0.00;
		$subcusto_material=0.00;
		$subcusto_medicamento=0.00;
		$subcusto_gas=0.00;
		$subcusto_dieta=0.00;
		$subcusto_servico=0.00;
		$subtotal_servico=0.00;
		$cont1=0;
		$cont2=0;
		$cont3=0;
		$cont4=0;
		$cont5=0;
		$cont6=0;
		while($row = mysql_fetch_array($result)){
			$issFatura = $row['imposto_iss'];

			$desconto_acrescimo =  ($row['QUANTIDADE']*$row['VALOR_FATURA'])*($row['DESCONTO_ACRESCIMO']/100);
			$subtotal +=($row['QUANTIDADE']*$row['VALOR_FATURA'])+$desconto_acrescimo;



			if($row['TIPO'] == '6'){
				$subtotal_gas +=($row['QUANTIDADE']*$row['VALOR_FATURA'])+$desconto_acrescimo;
				$subcusto_gas +=($row['QUANTIDADE']*$row['VALOR_CUSTO'])+$desconto_acrescimo;
				$custo=$subcusto_gas;
				$parcial=$subtotal_gas;

				$cont5++;
				$n=$row['cod'];
				$tip = $row['TIPO'];
				$tipo_tot="GASOTERAPIA TOTAL ";
				$tipo_custo="CUSTO ";

			}

			if($row['TIPO'] == '5'){
				$subtotal_equipamento +=$row['QUANTIDADE']*$row['VALOR_FATURA']+$desconto_acrescimo;
				$parcial=$subtotal_equipamento;
				$subcusto_equipamento +=$row['QUANTIDADE']*$row['VALOR_CUSTO']+$desconto_acrescimo;
				$custo=$subcusto_equipamento;
				$cont4++;
				$n=$row['cod'];
				$tip = $row['TIPO'];
				$tipo_tot="EQUIPAMENTO TOTAL ";
				$tipo_custo="CUSTO ";

			}
			if($row['TIPO'] == '3'){
				$subtotal_dieta +=$row['QUANTIDADE']*$row['VALOR_FATURA']+$desconto_acrescimo;
				$cont6++;
				$n="Bras. ".$row['NUMERO_TISS'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_dieta;
				$subcusto_dieta +=$row['QUANTIDADE']*$row['VALOR_CUSTO']+$desconto_acrescimo;
				$custo=$subcusto_dieta;
				$tipo_tot="DIETA TOTAL ";
				$tipo_custo="CUSTO ";
			}
			if($row['TIPO'] == '2'){
				$subtotal_servico +=$row['QUANTIDADE']*$row['VALOR_FATURA']+$desconto_acrescimo;
				$cont3++;
				$n=$row['cod'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_servico;
				$subcusto_servico +=$row['QUANTIDADE']*$row['VALOR_CUSTO']+$desconto_acrescimo;
				$custo=$subcusto_servico;
				$tipo_tot="SERVI&Ccedil;O TOTAL ";
				$tipo_custo="CUSTO ";
			}
			if($row['TIPO'] == '1'){
				$subtotal_material +=$row['QUANTIDADE']*$row['VALOR_FATURA']+$desconto_acrescimo;
				$cont1++;
				$n="Simp. ".$row['NUMERO_TISS'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_material;
				$tipo_tot="MATERIAL TOTAL ";
				$subcusto_material +=$row['QUANTIDADE']*$row['VALOR_CUSTO']+$desconto_acrescimo;
				$custo=$subcusto_material;
				$tipo_custo="CUSTO ";
			}
			if($row['TIPO'] == '0'){
				$subtotal_medicamento +=$row['QUANTIDADE']*$row['VALOR_FATURA']+$desconto_acrescimo;
				$cont2++;
				$n="Bras. ".$row['NUMERO_TISS'];
				$tip = $row['TIPO'];
				$parcial=$subtotal_medicamento;
				$tipo_tot="MEDICAMENTO TOTAL ";

				$subcusto_medicamento +=$row['QUANTIDADE']*$row['VALOR_CUSTO']+$desconto_acrescimo;
				$custo=$subcusto_medicamento;
				$tipo_custo="CUSTO ";
			}

			if( $cont_cor%2 ==0 ){
				$cor = '';
			}else{
				$cor="bgcolor='#EEEEEE'";
			}

			$responseUnidade = $row['UNIDADE'] > 0 ? UnidadeMedida::getById($row['UNIDADE']) : '';
			$und = empty($responseUnidade) ? '' : $responseUnidade['UNIDADE'];





			if($row['CODIGO_PROPRIO'] != '0'){
				$n = $row['CODIGO_PROPRIO'];
			}


			if($cont5 == 1){
				if($tip != $tipo && $cont_cor != 0){
					$submargem_rs=$par-$custo1;
					$submargem_p=($submargem_rs/$par)*100;
					$html.= "<tr>
                                      <td></td>
                                     <td><b>SUBTOTAL</b></td> 
                                     <td></td>
                                     <td><b>FATURADO</b></td>
                                     <td><b>R$".number_format($par,2,',','.')."</b></td>
                                     <td></td>
                                     <td><b>CUSTO</b></td>
                                     <td><b>R$".number_format($custo1,2,',','.')."</b></td>
                                     <td><b>R$".number_format($submargem_rs,2,',','.')."</b></td>
                                     <td><b>".number_format($submargem_p,2,',','.')."%</b></td>
                                    </tr>";
					$html .="<tr ><td><td></tr>";

				}

				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='8' style='font-size:12px;float:right;border:1px solid #000;'>Gasoterapia</td><td colspan='2'>Margem</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
		     	<td style='font-size:12px;float:right;'>N&deg;</td>
		     	<td style='font-size:12px;float:right;'>Unidade</td>
		     	<td style='font-size:12px;float:right;'>Item</td>
		     	<th style='font-size:12px;float:right;'>Qtd</th>
		     	<th style='font-size:12px;float:right;'>Pre&ccedil;o Unt.</th>
		     	<th style='font-size:12px;float:right;'>* Pre&ccedil;o Tot.</th>
                        <th style='font-size:12px;float:right;'>Custo Unit.</th>
                        <th style='font-size:12px;float:right;'>Custo Total</th>
                        <th style='font-size:12px;float:right;'>R$</th>
                        <th style='font-size:12px;float:right;'>%</th>
		     	</tr>";


			}
			if($cont4 == 1){
				if($tip != $tipo && $cont_cor != 0){
					$submargem_rs=$par-$custo1;
					$submargem_p=($submargem_rs/$par)*100;
					$html.= "<tr>
                                      <td></td>
                                     <td><b>SUBTOTAL</b></td> 
                                     <td></td>
                                     <td><b>FATURADO</b></td>
                                     <td><b>R$".number_format($par,2,',','.')."</b></td>
                                     <td></td>
                                     <td><b>CUSTO</b></td>
                                     <td><b>R$".number_format($custo1,2,',','.')."</b></td>
                                     <td><b>R$".number_format($submargem_rs,2,',','.')."</b></td>
                                     <td><b>".number_format($submargem_p,2,',','.')."%</b></td>
                                    </tr>";
					$html .="<tr ><td><td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='8' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>MATERIAIS PERMANENTES</td><td colspan='2'>Margem</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>* Pre&ccedil;o Tot.</th>
                                <th style='font-size:12px;float:right;'>Custo Unit.</th>
                                <th style='font-size:12px;float:right;'>Custo Total</th>
                                <th style='font-size:12px;float:right;'>R$</th>
                        <th style='font-size:12px;float:right;'>%</th>
				</tr>";


			}
			if($cont6 == 1 ){
				if($tip != $tipo && $cont_cor != 0){
					$submargem_rs=$par-$custo1;
					$submargem_p=($submargem_rs/$par)*100;
					$html.= "<tr>
                                      <td></td>
                                     <td><b>SUBTOTAL</b></td> 
                                     <td></td>
                                     <td><b>FATURADO</b></td>
                                     <td><b>R$".number_format($par,2,',','.')."</b></td>
                                     <td></td>
                                     <td><b>CUSTO</b></td>
                                     <td><b>R$".number_format($custo1,2,',','.')."</b></td>
                                     <td><b>R$".number_format($submargem_rs,2,',','.')."</b></td>
                                     <td><b>".number_format($submargem_p,2,',','.')."%</b></td>
                                    </tr>";
					$html .="<tr><td><td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='8' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>DIETA</td><td colspan='2'>Margem</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>* Pre&ccedil;o Tot.</th>
                                <th style='font-size:12px;float:right;'>Custo Unit.</th>
                                <th style='font-size:12px;float:right;'>Custo Total</th>
                                <th style='font-size:12px;float:right;'>R$</th>
                        <th style='font-size:12px;float:right;'> %</th>
			
			
				</tr>";

			}
			if($cont3 == 1 ){
				if($tip != $tipo && $cont_cor != 0){
					$submargem_rs=$par-$custo1;
					$submargem_p=($submargem_rs/$par)*100;
					$html.= "<tr>
                                      <td></td>
                                     <td><b>SUBTOTAL</b></td> 
                                     <td></td>
                                     <td><b>FATURADO</b></td>
                                     <td><b>R$".number_format($par,2,',','.')."</b></td>
                                     <td></td>
                                     <td><b>CUSTO</b></td>
                                     <td><b>R$".number_format($custo1,2,',','.')."</b></td>
                                     <td><b>R$".number_format($submargem_rs,2,',','.')."</b></td>
                                     <td><b>".number_format($submargem_p,2,',','.')."%</b></td>
                                    </tr>";
					$html .="<tr ><td><td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='8' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>SERVI&Ccedil;OS</td><td colspan='2'>Margem</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>* Pre&ccedil;o Tot.</th>
                                <th style='font-size:12px;float:right;'>Custo Unit.</th>
                                <th style='font-size:12px;float:right;'>Custo Total</th>
                                <th style='font-size:12px;float:right;'>R$</th>
                        <th style='font-size:12px;float:right;'>%</th>
			</tr>";


			}
			if($cont1 == 1){
				if($tip != $tipo && $cont_cor != 0){
					$submargem_rs=$par-$custo1;
					$submargem_p=($submargem_rs/$par)*100;
					$html.= "<tr>
                                      <td></td>
                                     <td><b>SUBTOTAL</b></td> 
                                     <td></td>
                                     <td><b>FATURADO</b></td>
                                     <td><b>R$".number_format($par,2,',','.')."</b></td>
                                     <td></td>
                                     <td><b>CUSTO</b></td>
                                     <td><b>R$".number_format($custo1,2,',','.')."</b></td>
                                     <td><b>R$".number_format($submargem_rs,2,',','.')."</b></td>
                                     <td><b>".number_format($submargem_p,2,',','.')."%</b></td>
                                    </tr>";
					$html .="<tr ><td><td></tr>";
				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='8' style='font-size:12px;float:right;bgcolor:#EEEEEE;'>MATERIAIS</td><td colspan='2'>Margem</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o Unt.</th>
				<th style='font-size:12px;float:right; '>* Pre&ccedil;o Tot. </th>
                                <th style='font-size:12px;float:right;'>Custo Unit.</th>
                                <th style='font-size:12px;float:right;'>Total</th>
                                <th style='font-size:12px;float:right;'>R$</th>
                        <th style='font-size:12px;float:right;'> %</th>
				</tr>";

			}
			if($cont2 == 1){
				if($tip != $tipo && $cont_cor != 0 ){
					$submargem_rs=$par-$custo1;
					$submargem_p=($submargem_rs/$par)*100;
					$html.= "<tr>
                                      <td></td>
                                     <td><b>SUBTOTAL</b></td> 
                                     <td></td>
                                     <td><b>FATURADO</b></td>
                                     <td><b>R$".number_format($par,2,',','.')."</b></td>
                                     <td></td>
                                     <td><b>CUSTO</b></td>
                                     <td><b>R$".number_format($custo1,2,',','.')."</b></td>
                                     <td><b>R$".number_format($submargem_rs,2,',','.')."</b></td>
                                     <td><b>".number_format($submargem_p,2,',','.')."%</b></td>
                                    </tr>";
					$html .="<tr ><td><td></tr>";

				}
				$html.= "<tr></tr><tr bgcolor='#cccccc'><td colspan='8' style='font-size:12px;float:right;bgcolor:#EEEEEE; '>MEDICAMENTOS</td><td colspan='2'>Margem</td></tr>";
				$html.= "<tr bgcolor='#cccccc'>
				<td style='font-size:12px;float:right; '>N&deg;</td>
				<td style='font-size:12px;float:right; '>Unidade</td>
				<td style='font-size:12px;float:right; '>Item</td>
				<th style='font-size:12px;float:right; '>Qtd</th>
				<th style='font-size:12px;float:right; '>Pre&ccedil;o</th>
				<th style='font-size:12px;float:right; '>* Pre&ccedil;o Tot.</th>
                                <th style='font-size:12px;float:right;'>Custo Unit.</th>
                                <th style='font-size:12px;float:right;'>Custo Total</th>
                                <th style='font-size:12px;float:right;'> R$</th>
                                <th style='font-size:12px;float:right;'> %</th>
				</tr>";

			}


			//
			$tipo = $tip;
			$par= $parcial;
			$custo1= $custo;
			$tipo_tot1=$tipo_tot;
			$tipo_custo1=$tipo_custo;
			$html.= "<tr $cor>
			<td align='left'>{$n}</td>
			<td>{$und}</td>
			<td width='50%'>".utf8_decode($row['principio'])."</td>
			<td>{$row['QUANTIDADE']}</td>
			<td>R$ ".number_format($row['VALOR_FATURA'],2,',','.')."</td>";
			$vt=$row['VALOR_FATURA']*$row['QUANTIDADE']+$desconto_acrescimo;
			$vtotal += $vt;
			$html.= "<td>R$ ".number_format($vt,2,',','.')."</td>";
			$html.= "<td>R$ ".number_format($row['VALOR_CUSTO'],2,',','.')."</td>";

			$vt_custo = $row['VALOR_CUSTO']*$row['QUANTIDADE'];
			$vtotal_custo += $vt_custo;
			$marg_rs = $vt - $vt_custo;
			$marg_porcento = ($marg_rs/$vt)*100;
			$html.= "<td>R$ ".number_format($vt_custo,2,',','.')."</td>";
			$html.= "<td>R$ ".number_format($marg_rs,2,',','.')."</td>";
			$html.= "<td>".number_format($marg_porcento,2,',','.')."%</td></tr>";

			if($cont4 == 1){
				$cont4++;

			}
			if($cont3 == 1){
				$cont3++;

			}
			if($cont1 == 1){
				$cont1++;

			}
			if($cont2 == 1){
				$cont2++;

			}
			$cont_cor++;
			$obs=$row['OBS'];
		}


		$submargem_rs=$par-$custo1;
		$submargem_p=($submargem_rs/$par)*100;
		$html.= "<tr>
                                      <td></td>
                                     <td><b>SUBTOTAL</b></td> 
                                     <td></td>
                                    <td><b>FATURADO</b></td>
                                     <td><b>R$".number_format($par,2,',','.')."</b></td>
                                         <td></td>
                                     <td><b>CUSTO</b></td>
                                      
                                     <td><b>R$".number_format($custo1,2,',','.')."</b></td>
                                     <td><b>R$".number_format($submargem_rs,2,',','.')."</b></td>
                                     <td><b>".number_format($submargem_p,2,',','.')."%</b></td>
                                    </tr>";
		$ir = $vtotal*0.035;
		$issqn = $vtotal* ($issFatura/100);
		$piscofinscsll=$vtotal*0.0495;
		$total_encargos = $vtotal*0.1045;
		$faturado_liquido = $vtotal-$total_encargos;
		$margem_contribuicao = $faturado_liquido - $vtotal_custo;
		$tir=($margem_contribuicao/$vtotal_custo)*100;
		$percentualContribuicao=$this->percentualRepaseUnidadeRegional($id);
		$repase_40 = $margem_contribuicao*$percentualContribuicao;
		$porcento_margem = ($margem_contribuicao/$vtotal)*100;
		$html .="<tr><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td></tr>";
		$html .= "<tr>
                    <td></td>
                    <td><b>FECHAMENTO</b></td>
                    <td></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'><b>TOTAL FATURADO</b></td>
                    <td style='border-bottom: 1px solid' bgcolor='#cccccc'></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'><b>R$".number_format($vtotal,2,',','.')."</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
		$html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style='border-bottom: 1px solid'>IR</td>
                    <td style='border-bottom: 1px solid'>3,5%</td>
                    <td style='border-bottom: 1px solid'>R$".number_format($ir,2,',','.')."</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
		$html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style='border-bottom: 1px solid'>ISSQN</td>
                    <td style='border-bottom: 1px solid'>{$issFatura}%</td>
                    <td style='border-bottom: 1px solid'>R$".number_format($issqn,2,',','.')."</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
		$html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style='border-bottom: 1px solid'>PIS/COFINS/CSLL</td>
                    <td style='border-bottom: 1px solid'>4,95%</td>
                    <td style='border-bottom: 1px solid'>R$".number_format($piscofinscsll,2,',','.')."</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
		$html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style='border-bottom: 1px solid' bgcolor='#cccccc'><b>TOTAL ENCARGOS</b></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'><b>8,15%</b></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'> <b>R$".number_format($total_encargos,2,',','.')."</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
		$html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style='border-bottom: 1px solid'>FATURADO LIQUIDO</td>
                    <td style='border-bottom: 1px solid'></td>
                    <td style='border-bottom: 1px solid'>R$".number_format($faturado_liquido,2,',','.')."</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
		$html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style='border-bottom: 1px solid' bgcolor='#cccccc'><b>TOTAL CUSTO</b></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'><b>R$".number_format($vtotal_custo,2,',','.')."</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
		$html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style='border-bottom: 1px solid'>MARGEM DE CONTRIBUI&Ccedil;&Atilde;O</td>
                    <td style='border-bottom: 1px solid'></td>
                    <td style='border-bottom: 1px solid'>R$".number_format($margem_contribuicao,2,',','.')."</td>
                    <td> ".number_format($porcento_margem,2,',','.')."%</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
		$html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style='border-bottom: 1px solid'>RENTABILIDADE</td>
                    <td style='border-bottom: 1px solid'></td>
                    <td style='border-bottom: 1px solid'>".number_format($tir,2,',','.')."%</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
		$html .= "<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'><b>REPASSE UNIDADE</b></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'><b>".$percentualContribuicao*100 ."%</b></td>
                    <td bgcolor='#cccccc' style='border-bottom: 1px solid'><b>R$".number_format($repase_40,2,',','.')."</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";

		$html.= "<tr bgcolor='#cccccc' ><td colspan='7'  ><b>OBSERVA&Ccedil;&Atilde;O: </br> {$obs}</td></tr>";

		$html.= "</table>";
		echo $html;
		///////////////////////////////////////////////////////////








	}





}

$p = new imprimir_fatura();
$p->detalhe_fatura($_GET['id'],$_GET['av'],$_GET['zerados']);

?>
   