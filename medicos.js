$(function($){

  //Pede confirmação para deixar a página atual
  function ExitPageConfirmer(message) 
  {
  	 this.message = message;
  	 this.needToConfirm = false;
  	 var myself = this;
  	 window.onbeforeunload = function() 
	{
  		if (myself.needToConfirm) 
		{
  			return myself.message;
  		}
  	}
  }
  
  //CARREGA A BUSCA
	$(document).ready(function(){
		var p = null;
		var i =null;
		var f = null;
		
		$.post("query.php",{query: "buscar-prescricoes", paciente: p, inicio: i, fim: f},function(r){
	   		$("#div-resultado-busca").html(r);
	   	});
	   	return false;
	});

   var exitPage = new ExitPageConfirmer('Deseja realmente sair dessa página? os dados não salvos serão perdidos!');
   
   $(".div-aviso").hide();
   
   $("#novoBtn").hide();
   
   $('.data').datepicker('option','minDate',$("#tabela-itens").attr("inicio"));$('.data').datepicker('option','maxDate',$("#tabela-itens").attr("fim"));
   
   $(".numerico").priceFormat
  ({
    prefix: '',
    centsSeparator: ',',
    thousandsSeparator: '.'
   });
   
   $(".incluir").click(function()
   {
   	//$("input[name='inicio']").datepicker( "setDate" , $("#tabela-itens").attr("inicio") );
   	//$("input[name='fim']").datepicker( "setDate" , $("#tabela-itens").attr("fim") );
   	
   	if($("#nome").val()!='')
	{
    	$.post("query.php",{query: "gravar-outro", nome: $("#nome").val()});
   	}	
   });
   
   
   
   $("#buscar").click(function(){
   	var p = $("select[name='paciente'] option:selected").val();
   	var i = $("#div-busca input[name='inicio']").val();
   	var f = $("#div-busca input[name='fim']").val();
   	
   	$.post("query.php",{query: "buscar-prescricoes", paciente: p, inicio: i, fim: f},function(r){
   		$("#div-resultado-busca").html(r);
   	});
   	return false;
   });
   
   $("select[name='frequencia']").change(function(){
   		if($(this).find("option:selected").html()=='ACM'){
   			$(this).parent().find("input[name='obs']").addClass("OBG");
   		}
   		else{ 
   			$(this).parent().find("input[name='obs']").removeClass("OBG");
   			$(this).parent().find("input[name='obs']").css({"border":""});
   		}
   	});
   
   $(".comp-nebulizacao").click(function(){
   	

	  if( $("#check-outro-neb").is(":checked")){
		  $("#check-outro-neb").attr("checked",false);
		  $("#nome-outro-neb").removeClass("OBG");
		  $("#nome-outro-neb").removeAttr("style");
		  $("#nome-outro-neb").val("");
	   	  $("#dose-outro-neb").attr('disabled','disabled');
	   	 $("#dose-outro-neb").removeClass("OBG");
	   	 $("#dose-outro-neb").val("");
	   	 $("#dose-outro-neb").removeAttr("style");
	   	  $('#um-outro-neb').removeClass("COMBO_OBG");
	   	$('#um-outro-neb').removeAttr("style");
		$('#um-outro-neb').val("-1");
	   	$("#busca-outro-neb").attr('disabled','disabled');
		$("#busca-outro-neb").val("");
	  }else{
	   
	   if($("#Atrovent").is(":checked") || $("#Berotec").is(":checked") ){
		    $("#frequencia").addClass("COMBO_OBG");
		    $("#um-diluicao-neb").addClass("COMBO_OBG");
	  		$("#qtd-diluicao").addClass("OBG");
	  		$("#diluicao-nebulizacao").addClass("OBG");
	  		
		   
		   
	   
		   }else{
			   $("#um-diluicao-neb").val("-1");
		  	   $("#qtd-diluicao").val("");
		  	   $("#diluicao-nebulizacao").val("");
		  	   $("#frequencia").val("-1");
		  		
		  		//$("#qtd-diluicao").removeClass("OBG");
				//$("#um-diluicao-neb").removeClass("COMBO_OBG");
               // $("#diluicao-nebulizacao").removeClass("OBG");
				//$("#frequencia").removeClass("COMBO_OBG");
				
				//$("#um-diluicao-neb").removeAttr("style");
				//$("#frequencia").removeAttr("style");
				//$("#qtd-diluicao").removeAttr("style");
				//$("#diluicao-nebulizacao").removeAttr("style");
		   }
	  }
	   var mark = this.checked;
   	if(mark){ 
   		$(this).parent().parent().children('td').eq(2).children('input').attr('disabled','');
   		$(this).parent().parent().children('td').eq(2).children('input').focus();
   		$(this).parent().parent().children('td').eq(2).children('input').addClass("OBG");
   		$(this).parent().parent().children('td').eq(3).children('select').addClass("COMBO_OBG");
   		
   	   
   		
   	} else {
   		$(this).parent().parent().children('td').eq(2).children('input').attr('disabled','disabled');
   		$(this).parent().parent().children('td').eq(2).children('input').removeClass("OBG");
   		$(this).parent().parent().children('td').eq(2).children('input').val("");
   		$(this).parent().parent().children('td').eq(2).children('input').removeAttr("style");
   		$(this).parent().parent().children('td').eq(3).children('select').removeClass("COMBO_OBG");
   		$(this).parent().parent().children('td').eq(3).children('select').removeAttr("style");
   		$(this).parent().parent().children('td').eq(3).children('select').val("-1");
   		
		
   		
   	}
   });
   
   $(".comp-nebulizacao-outro").click(function(){
	   
	   
	   if($(".comp-nebulizacao").is(":checked"))
		   {
		   
		   
		   $("#Atrovent").attr("checked",false); 
		   $("#Berotec").attr("checked",false);
		   
		   $(".comp-nebulizacao").parent().parent().children('td').eq(2).children('input').attr('disabled','disabled');
	   		$(".comp-nebulizacao").parent().parent().children('td').eq(2).children('input').removeClass("OBG");
	   		$(".comp-nebulizacao").parent().parent().children('td').eq(2).children('input').val("");
	   		$(".comp-nebulizacao").parent().parent().children('td').eq(2).children('input').removeAttr("style");
	   		$(".comp-nebulizacao").parent().parent().children('td').eq(3).children('select').removeClass("COMBO_OBG");
	   		$(".comp-nebulizacao").parent().parent().children('td').eq(3).children('select').removeAttr("style");
	   		$(".comp-nebulizacao").parent().parent().children('td').eq(3).children('select').val("-1");
		   
	   		

			   $("#comp-nebulizacao2 input[name='neb']").attr('disabled','disabled');
			   $("#comp-nebulizacao2 input[name='neb']").removeClass("OBG");
			   $("#comp-nebulizacao2 input[name='neb']").val("");
			   $("#comp-nebulizacao2 input[name='neb']").removeAttr("style");
			   $("#comp-nebulizacao2select").removeClass("COMBO_OBG");
			   $("#comp-nebulizacao2select").val("-1");
			   $("#comp-nebulizacao2select").removeAttr("style");
			   
		   		
		   
		   }else{
	   
	   
	   if( $("#check-outro-neb").is(":checked")){
		    $("#frequencia").addClass("COMBO_OBG");
		    $("#um-diluicao-neb").addClass("COMBO_OBG");
	  		$("#qtd-diluicao").addClass("OBG");
	  		$("#diluicao-nebulizacao").addClass("OBG");
	  		
		   
		   
	   
		   }else{
			   $("#um-diluicao-neb").val("-1");
		  	   $("#qtd-diluicao").val("");
		  	   $("#diluicao-nebulizacao").val("");
		  	   $("#frequencia").val("-1");
		  		
		  		//$("#qtd-diluicao").removeClass("OBG");
				//$("#um-diluicao-neb").removeClass("COMBO_OBG");
               //$("#diluicao-nebulizacao").removeClass("OBG");
				//$("#frequencia").removeClass("COMBO_OBG");
				
				//$("#um-diluicao-neb").removeAttr("style");
				//$("#frequencia").removeAttr("style");
				//$("#qtd-diluicao").removeAttr("style");
				//$("#diluicao-nebulizacao").removeAttr("style");
		   }
		   }
   	var mark = this.checked;
   	if(mark){ 
   		$("#nome-outro-neb").addClass("OBG");
   		$("#dose-outro-neb").addClass("OBG").attr('disabled','');
   		$('#um-outro-neb').addClass("COMBO_OBG");
   		$("#busca-outro-neb").attr('disabled','').focus();
   		
   		
   		
   	} else {
   		$("#nome-outro-neb").removeClass("OBG").css({"border":""}).val("");
   		$("#dose-outro-neb").removeClass("OBG").attr('disabled','').css({"border":""}).val("");
   		$('#um-outro-neb').removeClass("COMBO_OBG").css({"border":""});
   		$("#busca-outro-neb").attr('disabled','disabled');
		$("#busca-outro-neb").val("");
   		
		
   	}
   });
   
   $("#busca-outro-neb").autocomplete({
			source: "/utils/busca_principio.php",
			minLength: 3,
			select: function( event, ui ) {
					$("#busca-outro-neb").val('');
	      			$('#nome-outro-neb').val(ui.item.value);
	      			$('#busca-outro-neb').val('');
	      			$("#dose-outro-neb").focus();
	      			return false;
			}
  });
   
   
   
   ///////////problemas ativos
   $(".outro_ativo").hide();
   $("#outros-prob-ativos").click(function(){
 	if($(this).is(":checked")){

 		$(".outro_ativo").show();
 	}else{
 		$(".outro_ativo").hide();
 	}
 	
 });
   
   $("#add-prob-ativo").click(function(){
		var x = $("input[name='outro-prob-ativo']").val();
		
		var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prob-ativos' title='Excluir' border='0' >";
		$('#problemas_ativos').append("<tr><td colspan='2' class='prob-ativos' cod='1111' desc='"+x+"'> "+bexcluir+" "+x+"</td></tr>");
		$("input[name='busca-problemas-ativos']").val("");
		$("input[name='outro-prob-ativo']").val("");
		 $("#outros-prob-ativos").attr('checked',false);
		 $(".outro_ativo").hide();
		$("input[name='busca-problemas-ativos']").focus();
		atualiza_rem_problemas_ativos();
		return false;
		
		
	});
 
 
 $("input[name='busca-problemas-ativos']").autocomplete({
			source: "/utils/busca_cid.php",
			minLength: 3,
			select: function( event, ui ) {
				
				var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prob-ativos' title='Excluir' border='0' >";
				$('#problemas_ativos').append("<tr><td colspan='2' class='prob-ativos' cod='"+ui.item.id+"' desc='"+ui.item.name+"'> "+bexcluir+" "+ui.item.name+"</td></tr>");
				$("input[name='busca-problemas-ativos']").val("");
				$("input[name='busca-problemas-ativos']").focus();
				atualiza_rem_problemas_ativos();
				
				return false;
			},
 });
 
 $(".del-prob-ativos").unbind("click",remove_problemas_ativos).bind("click",remove_problemas_ativos);


 function atualiza_rem_problemas_ativos(){
	  $(".del-prob-ativos").unbind("click",remove_problemas_ativos)
	  				.bind("click",remove_problemas_ativos);
 }
 
 function remove_problemas_ativos(){
 	$(this).parent().parent().remove();
 	return false;
 }
 
 
 
 $("#salvar-problemas").click(function(){
 	if(validar_campos("div-prob-ativo")){
	  	var dados = $("form").serialize();
 		var probativos = "";
	  	var total = $(".prob-ativos").size();
	  	
	  	
 		$(".prob-ativos").each(function(i){
 			var cid = $(this).attr("cod");
 			var desc = $(this).attr("desc");
 			probativos += cid+"#"+desc;
			if(i<total - 1) probativos += "$";
	  	});
 		
 		
 		var prob = "query=prob-ativos&"+dados+"&probativos="+probativos;

	  	$.ajax({
 			type: "POST",
 			url: "query.php",
	  		data: prob,
 			success: function(msg){
 				
 				
 				if(msg >= 0){
 					alert("Salvo com sucesso!!");
 							window.location.href='?medico';
 				}else{
 					alert("DADOS: "+msg);
 				}
 				
 								
	  		}
	  	
 		});
 	}
 	return false;
 });
  
	   
   
  ////fim prob ativos 
   
  
   
   
  
 ///oxigenoterapia jeferson
   
   
   
   $(".oxi").click(function(){
	   $(".oxi2").each(function(){
		  
		   $("input[name='dose']").removeClass("OBG");
		   $("input[class='oxi2']").attr('disabled','disabled');
		   $("input[class='oxi2']").val("");
		   $("select[name='dose']").removeClass("OBG");
		   $("select[class='oxi2']").attr('disabled','disabled');
		   $("select[class='oxi2']").val("");
		   
	   });
	   $(this).parent().parent().children('td').eq(1).children('input').attr('disabled','');
	   $(this).parent().parent().children('td').eq(1).children('input').addClass("OBG");
	   $(this).parent().parent().children('td').eq(1).children('input').focus();
	   if($(this).attr('tipo')==1){
		   $("select[id='s']").attr('disabled','');
	       $("select[id='s']").focus();
	       $("select[id='s']").addClass("OBG");
	    }
	   
	   if($(this).attr('tipo')==3){
		   $("select[id='s1']").attr('disabled','');
	       $("select[id='s1']").focus();
	       $("select[id='s1']").addClass("OBG");
	    }
	   
	   
	   if($(this).attr('tipo')==0){
		  
			   $("#u").keyup(function() {
				   var valor = $(this).val().replace(/[^1-5]/g,'');
				   $(this).val(valor);
				});
		  
			   $("#u1").keyup(function() {
				   var valor = $(this).val().replace(/[^1-5]/g,'');
				   $(this).val(valor);
				});
		  
			   $("#u2").keyup(function() {
				   var valor = $(this).val().replace(/[^1-6]/g,'');
				   $(this).val(valor);
				});
		
			  $("#u4").keyup(function() {
				   var valor = $(this).val().replace(/[^5-8]/g,'');
				   $(this).val(valor);
				});
		  
	   }
	   $("#frequenciaoxi").addClass("COMBO_OBG");
   });
  
   ///fim oxigenoterapia jeferson
   
   $("#iniciar-prescricao").click(function(){
   	if(!validar_campos("div-iniciar-prescricao"))
   		return false;
   });
   
   /// Criado por Ricardo para a grid 
   $("#iniciarprescricao").click(function(){
	   	if(!validar_campos("pre-presc")){	   		
	   	return false;
	   	}
	   
	   
	   });
   
   $("#container-presc").hide();
   /*
   $("input:radio[name=tipo]").click(function(){	   
	   $("#iniciar-prescricao").hide();
	   $("#antigas").hide();
	   $("#container-presc").show();
   });
   $("#dialog-salvo1").dialog({
	  	autoOpen: false, modal: true, position: 'top',close: function(event,ui){window.location = "?view=buscar";},
	  	buttons: {
	  		"Sim" : function(){
	  			$.post("query.php",{query: "email-aviso", p: $("#presc_emergencia").attr("pnome")},function(r){
	  				$("#dialog-salvo").dialog("close");
	  				
	  			});
	  		},
	  		"Não" : function(){
	  			$(this).dialog("close");
	  			
	  		}
	  	}
	  });*/
   $("#dialog-salvo1").dialog({
	  	autoOpen: false, modal: true, position: 'top',
	  	buttons: {
	  		"Sim" : function(){
	  			$.post("query.php",{query: "email-aviso", p: $("#prescricao").attr("pnome")},function(r){
	  				$("#dialog-salvo").dialog("close");
	  				$("#dialog-imprimir1").dialog("open");
	  			});
	  		},
	  		"Não" : function(){
	  			$(this).dialog("close");
	  			$("#dialog-imprimir1").dialog("open");
	  		}
	  	}
	  });
	   
	   $("#dialog-imprimir1").dialog({
		  	autoOpen: false, modal: true, position: 'top',
		  	close: function(event,ui){window.location = "?view=buscar";},
		  	buttons: {
		  		"Visualizar":function(){
		  			var p = $("#dialog-imprimir1 input[name='prescricao_emergencia']").val();
		  			window.open('imprimir_emergencia.php?prescricao='+p,'_blank');
		  			$(this).dialog("close");
		  			
		  		},
		  		"Fechar" : function(){
		  			$(this).dialog("close");
		  		}
		  	}
		  });
   
   
   
   
   $("#dialog-salvo").dialog({
	  	autoOpen: false, modal: true, position: 'top',
	  	buttons: {
	  		"Sim" : function(){
	  			$.post("query.php",{query: "email-aviso", p: $("#prescricao").attr("pnome")},function(r){
	  				$("#dialog-salvo").dialog("close");
	  				$("#dialog-imprimir").dialog("open");
	  			});
	  		},
	  		"Não" : function(){
	  			$(this).dialog("close");
	  			$("#dialog-imprimir").dialog("open");
	  		}
	  	}
	  });
	   
	   $("#dialog-imprimir").dialog({
		  	autoOpen: false, modal: true, position: 'top',
		  	close: function(event,ui){window.location = "?view=buscar";},
		  	buttons: {
		  		"Visualizar":function(){
		  			var p = $("#dialog-imprimir input[name='prescricao']").val();
		  			window.open('imprimir.php?prescricao='+p,'_blank');
		  			$(this).dialog("close");
		  			
		  		},
		  		"Fechar" : function(){
		  			$(this).dialog("close");
		  		}
		  	}
		  });
	   
   
	   
	   function salvar_emergencial(){
		   	var itens = "";
		   	var total = $(".dados").size();
		   	$(".dados").each(function(i){
//		   		var apr = $(this).attr("apr");
				var cod = $(this).attr("cod"); var nome = $(this).attr("nome"); var inicio = $(this).attr("inicio"); var id_item = $(this).attr("id_item");
				var via = $(this).attr("via");  var fim = $(this).attr("fim"); var obs = $(this).attr("obs");
				var tipo = $(this).attr("tipo"); var freq = $(this).attr("frequencia");
				
				var cod_ref = $(this).attr('cod_ref');
				var tipo_bras = $(this).attr('tipo_bras');
				
				
					desc= $(this).html();
					apraz = $(this).attr("hora");
					
					
				
				
				var dose = $(this).attr("dose"); var um = $(this).attr("um");
				var linha = tipo+"#"+cod+"#"+nome+"#"+via+"#"+freq+"#"+apraz+"#"+inicio+"#"+fim+"#"+obs+"#"+desc+"#"+dose+"#"+um+'#'+id_item+'#'+cod_ref+'#'+tipo_bras;
				
				if(i<total - 1) linha += "$";
				itens += linha;
		    });
		    if(total > 0){
		    	var tipo_presc= $("#prescricao").attr("tipo_presc");
		    	var paciente= $("#prescricao").attr("paciente");
		    	var inicio= $("#prescricao").attr("inicio");
		    	var fim= $("#prescricao").attr("fim");
		    	if(tipo_presc == null && paciente == null){
		    
		    tipo_presc= $("#presc_emergencia").attr("tipo_presc");
			 paciente= $("#presc_emergencia").attr("paciente");
			 inicio= $("#presc_emergencia").attr("inicio");
			 fim= $("#presc_emergencia").attr("fim");
			
		    	
		    	
		    	}
		    	
		    
		    	$.post("query.php",{query: "finalizar-prescricao-emergencial", paciente: paciente, 
		    		 tipo_presc: tipo_presc, inicio: inicio, fim: fim,  dados: itens
		    		
		    	
		    	},function(r){
		    		if(r != "-1"){
		    			
		    			
		    			exitPage.needToConfirm = false;
		    			
		    			
		    			//$("#dialog-imprimir input[name='prescricao']").val(r);
		    			
		    			$("#dialog-salvo").dialog("open");
		    			$("#dialog-imprimir1 input[name='prescricao_emergencia']").val(r);
		    			$("#dialog-salvo1").dialog("open");
		    		}
		    		else{ alert("ERRO: Tente mais tarde!"); }
		    	});
		    } else {
		    	alert("ERRO: A prescrição está vazia.");
		    }
		   }	   
	   
	   
   function salvar(){
   	var itens = "";
   	var total = $(".dados").size();
   	$(".dados").each(function(i){
//   		var apr = $(this).attr("apr");
		var cod = $(this).attr("cod"); var nome = $(this).attr("nome"); var inicio = $(this).attr("inicio"); var id_item = $(this).attr("id_item");
		var via = $(this).attr("via"); var apraz = '00:00'; var fim = $(this).attr("fim"); var obs = $(this).attr("obs");
		var tipo = $(this).attr("tipo"); var freq = $(this).attr("frequencia");
		var desc = $(this).children('td').eq(0).html();
		var cod_ref = $(this).attr('cod_ref');
		var tipo_bras = $(this).attr('tipo_bras');
		//var desc = $(this).html();
		if(desc== null){
			desc= $(this).html();
			apraz = $(this).attr("hora");
			
			
		}
		
		var dose = $(this).attr("dose"); var um = $(this).attr("um");
		var linha = tipo+"#"+cod+"#"+nome+"#"+via+"#"+freq+"#"+apraz+"#"+inicio+"#"+fim+"#"+obs+"#"+desc+"#"+dose+"#"+um+'#'+id_item+'#'+cod_ref+'#'+tipo_bras;
		
		if(i<total - 1) linha += "$";
		itens += linha;
    });
    if(total > 0){
    	var tipo_presc= $("#prescricao").attr("tipo_presc");
    	var paciente= $("#prescricao").attr("paciente");
    	var inicio= $("#prescricao").attr("inicio");
    	var fim= $("#prescricao").attr("fim");
    	var id_cap = $("#prescricao").attr("id_cap");
    	if(tipo_presc == null && paciente == null){
    
    tipo_presc= $("#presc_emergencia").attr("tipo_presc");
	 paciente= $("#presc_emergencia").attr("paciente");
	 inicio= $("#presc_emergencia").attr("inicio");
	 fim= $("#presc_emergencia").attr("fim");
	
    	
    	
    	}
    	
    
    	$.post("query.php",{query: "finalizar-prescricao", paciente: paciente, 
    		 tipo_presc: tipo_presc, inicio: inicio, fim: fim,id_cap: id_cap ,  dados: itens
    		
    	
    	},function(r){
    		if(r != "-1"){
    			
    			
    			exitPage.needToConfirm = false;
    			
    			
    			$("#dialog-imprimir input[name='prescricao']").val(r);
    			
    			$("#dialog-salvo").dialog("open");
    			//$("#dialog-imprimir1 input[name='prescricao']").val(r);
    			//$("#dialog-salvo1").dialog("open");
    		}
    		else{ alert("ERRO: Tente mais tarde!"); }
    	});
    } else {
    	alert("ERRO: A prescrição está vazia.");
    }
   }
   
   function atualiza_qtd(tipo,op){
   	exitPage.needToConfirm = true;
   	var div = "div[tipo='"+tipo+"']";
   	var qtd = $(div).attr("qtd");
   	if(op == "+")
   	  $(div).attr("qtd",parseInt(qtd)+1);
   	else
   	  $(div).attr("qtd",parseInt(qtd)-1);
   }
   
   function validar_campos(div){
    var ok = true;
    $("#"+div+" .OBG").each(function (i){
      if(this.value == ""){
		ok = false;
		this.style.border = "3px solid red";
      } else {
		this.style.border = "";
      }
    });
    $("#"+div+" .COMBO_OBG option:selected").each(function (i){
      if(this.value == "-1"){
		ok = false;
		$(this).parent().css({"border":"3px solid red"});
      } else {
		$(this).parent().css({"border":""});
      }
    });
    if(!ok){
//      $("#"+div+" .aviso").css({"background-color":"yellow", "font-weight":"bolder"});
      $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
      $("#"+div+" .div-aviso").show();
    } else {
//      $("#"+div+" .aviso").css({"background-color":"", "font-weight":""});
      $("#"+div+" .aviso").text("");
      $("#"+div+" .div-aviso").hide();
    }
    return ok;
  }
  
  function validar_tipos(){
  	var ok = true;
  	$("#aviso").html("<center><b>Nenhum dos itens abaixo foi prescrito: </b></center><ul>");
  	$("div .tab-presc").each(function(i){
  		if($(this).attr("qtd") == 0){
  			ok = false;
  			$("#aviso").append("<li>"+$(this).attr("title")+"</li>"); 
  		}
  	});
  	$("#aviso").append("</ul><br/><b>Deseja realmente continuar?</b>");
  	return ok;
  }
  
  function reload_css_table(){
  	$("#tabela-itens tr:odd").css("background-color","#ebf3ff");
      $("#tabela-itens tr").hover(
		function(){$(this).css("background-color","#3d80df");$(this).css("color","#ffffff");},
		function(){
	  		$(this).css("background-color","");$(this).css("color","");
	 		$("#tabela-itens tr:odd").css("background-color","#ebf3ff");
	  		$("#tabela-itens tr:odd").css("color","");
		});
  }
  
  $("select[name='paciente']").change(function(){
  	var p = $("select[name='paciente'] option:selected").val();
  	var a = $('input:radio[name=tipo]:checked').val();
  	$("button[name=antigas]").attr('paciente_btn_nova',p);
  	var i='null'; var f= 'null';
  	//$.post("query.php",{query: "prescricoes-antigas", p: p,tipo:a},function(r){
	$.post("query.php",{query: "buscar-prescricoes", paciente: p, inicio: i, fim: f,op:1},function(r){
   		
  		if(p!=-1){
  			$("#iniciar-prescricao").show();
  	  		$("#novoBtn").show();
  	  		$("#antigas").show();
  	  		$("#antigas").html(r);
  		}else{
  			$("#iniciar-prescricao").hide();
  	  		$("#novoBtn").hide();
  	  		$("#antigas").hide();
  	  		//$("#antigas").html(r);
  	  		
  		}
  		$("#div-resultado-busca").html(r);	
  	});
  });
  
  $("button[name=antigas]").click(function(){
	  
	  $("#paciente").val($(this).attr('paciente_btn_nova'));
	  $("input[name='antigas']").val(-1);
	  $("#NomePacientePresc").html($("#selPaciente").find('option').filter(':selected').text());	  	  
	  $("#pre-presc").dialog("open");
  });
  
  $("input[name='busca']").autocomplete({
			source: "/utils/busca_principio_obs.php",
			minLength: 3,
			select: function( event, ui ) {
					$("input[name='busca']").val('');
	      			$('#nome').val(ui.item.princ);
	      			$('#nome').attr('readonly','readonly');
	      			$('#cod').val(ui.item.id);
	      			$('#busca').val('');
	      			$("#formulario input[name='dose']").focus();
	      			return false;
			}
  });
  
  $("#formulario select[name='vadm-formulario']").change(function(){
  	if($("#formulario").attr("via") != $(this).val()){
  		if(!confirm("Via de adm diferente do padrão, deseja continuar?")){
  			var via = $("#formulario").attr("via");
  			$("#formulario select[name='vadm-formulario'] option[value='"+via+"']").attr('selected','selected');
  		}
  	}
  });
   
  $(".show-form").click(function(){
  	$("#add-item").attr("tipo",$(this).attr("tipo"));
  	$("#add-item").attr("label",$(this).attr("label"));
  	$("#formulario").attr("tipo",$(this).attr("tipo"));
  	$("#ui-dialog-title-formulario").html($(this).attr("label"));  	
  	$("#formulario").attr("classe",$(this).attr("classe"));
  	var via = $(this).attr("via");
  	$("#formulario").attr("via",via);
  	$("#formulario select[name='vadm-formulario'] option[value='"+via+"']").attr('selected','selected');
  	$("#formulario").dialog("open");
  });
  
  $("#formulario").dialog({
  	autoOpen: false, modal: true, width: 800, position: 'top',
  	open: function(event,ui){
  		$("#formulario input[name='busca']").val('');
  		$('#nome').val('');
	    $('#nome').attr('readonly','readonly');
//	    $('#apr').val('');
//	    $('#apr').attr('readonly','readonly');
	    $('#cod').val('');
	    $("#formulario input[name='aprazamento']").val('');
	    $("#formulario input[name='dose']").val('');
	    $("#formulario input[name='obs']").val('');
	    $("#formulario select[name='frequencia'] option").eq(0).attr('selected','selected');
	    $("#formulario select[name='um'] option").eq(0).attr('selected','selected');
  	},
  	buttons: {
  		"Fechar" : function(){
  			$(this).dialog("close");
  		}
  	}
  });
  
  $("#aviso").dialog({
  	autoOpen: false, modal: true, position: 'top',
  	buttons: {
  		"Voltar" : function(){
  			$(this).dialog("close");
  		},
  		"Continuar" : function(){
  			salvar();
    		$(this).dialog("close");
  		}
  	}
  });
  
  $("#outro").click(function(){
    $("input[name='nome']").attr("readonly","");
//    $("input[name='apr']").attr("readonly","");
    $("#cod").val("-1");
    $("input[name='nome']").focus();
    
    $("input[name='nome']").blur(function(){
    	if($("#nome").val()!=''){
	    	$.post("query.php",{query: "testar-outro", nome: $("#nome").val()},function(r){
	      		if(r!=''){
	      			alert(r);
	      			 $("input[name='nome']").focus();
	      		}
	      	});
    	}	
    });
  });
  
  
  /*-----------------------jeferson 30/07/12 presc avulasa-------------------------------------*/
  
  $("input[name='busca1']").autocomplete({
		source: function(request, response) {
          $.getJSON('/utils/busca_brasindice.php', {
              term: request.term,
              tipo: $("#formulario2").attr("tipo")
          }, response);
      },
		minLength: 3,
		select: function( event, ui ) {
				$('#busca-item').val('');
    			$('#nome').val(ui.item.value);
			    $('#cod').val(ui.item.id);
			    $('#cod_ref').val(ui.item.cod_ref);
    			$('#qtd').focus();
    			$('#busca-item').val('');
    			return false;
		},
		extraParams: {
 			tipo: 2
			}
});
  
  
  
  
  $(".add-kit").click(function(){
	  
	  	var item = $(this).attr("item");
	  	var tipo = $(this).attr("tipo");
	  	$("#kits-"+item).show();
	  	$("#formulario2").attr("item",item);
	  	$("#formulario2").attr("tipo",tipo);
	  	$("#b"+tipo).attr('checked','checked');
	  	$("#b"+tipo).button('refresh');
	  	$("#formulario2").dialog("open");
	  });
	  
	  $(".look-kit").toggle(
	  function(){
	  	var item = $(this).attr("item");
	  	$("#kits-"+item).show();
	  	
	  }, function(){
	  	var item = $(this).attr("item");
	  	$("#kits-"+item).hide();
	  	
	  });
	  
	  $("#formulario2").dialog({
	  	autoOpen: false, modal: true, width: 800, position: 'top',
	  	open: function(event,ui){
	  		$("input[name='busca']").val('');
	  		$('#nome').val('');
		    $('#nome').attr('readonly','readonly');
		    $('#apr').val('');
		    $('#apr').attr('readonly','readonly');
		    $('#cod').val('');
		    $('#cod_ref').val('');
		    $('#qtd').val('');
		    
		    
		    $("input[name='busca']").focus();
	  	},
	  	buttons: {
	  		"Fechar" : function(){
	  			var item = $("#formulario2").attr("item");
	  			if(item != "presc_emergencia"){
	  				$("#kits-"+item).hide();
	  				
	  			}
	  			$(this).dialog("close");
	  		}
	  	}
	  });
	  
	  /*function atualiza_rem_item(){
		  $(".rem-item-solicitacao").unbind("click",remove_item)
		  				.bind("click",remove_item);
	  }*/
	  
	  $("#add-item1").click(function(){
		     if(validar_campos("formulario2")){
		      var cod = $("#cod").val(); var nome = $("#formulario2 input[name='nome']").val();       
		      var item = $("#formulario2").attr("item");
		     // var qtd = $("#formulario2 input[name='qtd']").val();
		      var tipo = $("#formulario2").attr("tipo");
		      var cod_ref= $("#formulario2 input[name='cod_ref']").val();
		      var qtd= $("#formulario2 input[name='qtd_kit']").val();
		      var inicio = $("#formulario2 input[name='inicio']").val(); var fim = $("#formulario2 input[name='fim']").val();
		      var um= $("#formulario2 select[name='um'] option:selected").val();
		      var num_kit= $("#formulario2 select[name='um'] option:selected").html();
		      var obs = $("#formulario2 input[name='obs']").val();
		      var frequencia = $("#formulario2  select[name='frequencia'] option:selected").val();
		      var nfrequencia = $("#formulario2  select[name='frequencia'] option:selected").html();
		      var hora = $("#formulario2 input[name='hora']").val();
		     
		      var dados = "nome='"+nome+"' tipo='"+tipo+"' cod='"+cod+"' dose='"+qtd+"'frequencia='"+frequencia+"'um='"+um+"'inicio='"+inicio+"'fim='"+fim+"'obs='"+obs+"'hora='"+hora+"'";
		   
		      var linha = nome+". Dosagem: "+qtd+" "+num_kit+". Frequncia: "+nfrequencia+". Per&iacute;odo: "+inicio+" at&eacute; "+fim+". OBS: "+obs+"  Hora:"+hora;	
		     var botoes = "<img align='right' class='rem-item-solicitacao' item='"+item+"' cod='"+cod+"' src='../utils/delete_16x16.png' title='Remover' border='0'>";
		      
		     /**/
		    
		      $("#cod").val("-1"); $("#formulario2 input[name='nome']").val('');
		      $("#formulario2 input[name='qtd']").val('');
		      $("#formulario2 input[name='qtd_kit']").val('');
		      $("#formulario2 input[name='obs']").val('');
		      $("#formulario2 input[name='hora']").val('');
                $('#busca-item').val('');
		      
		      $("#formulario2 select[name='frequencia'] option").eq(0).attr('selected','selected');
			  $("#formulario2 select[name='um'] option").eq(0).attr('selected','selected');
			 // $("#itens-"+tipo).after("<tr bgcolor='#ebf3ff' class='dados' "+dados+" ><td><b>Oxigenoterapia: </b>"+linha+"</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
		     $("#"+tipo+"-"+item).append("<tr bgcolor='#ffffff' cod="+cod+" inicio="+inicio+"><td >"+linha+"</td><td>"+botoes+"</td>/tr>");
		     $.post("query.php",{query: "consultar-itens-kit", cod: cod, inicio: inicio, fim: fim, cod_ref: cod_ref, qtd: qtd, nfrequencia: nfrequencia, 
		    	 frequencia: frequencia, um: um, hora:hora, obs: obs,item: item},function(r){
		    	 $("#"+tipo+"-"+item).append(r);
			   	}); 
		      reload_css_table();
				
		      atualiza_qtd(item,'+');
		      atualiza_rem_item();
		      
		     
		    
		     
		      
		      exitPage.needToConfirm = true;
		     }	
		    return false;
		  });
	  
	  function atualiza_rem_item(){
		  $(".rem-item-solicitacao").unbind("click",remove_item1)
		  				.bind("click",remove_item1);
	  }
	  
	  function remove_item1(){
		 
		  	if (confirm("Deseja realmente remover?")){
		  		$("#kits-presc_emergencia").find("tr[cod="+$(this).parent().parent().attr('cod')+"]").each(function(){
		  			
		  				$(this).remove();
		  			
		  		});
		  		atualiza_qtd($(this).attr('item'),'-');
				//*$(this).parent().parent().remove();
				//atualiza_qtd($(this).attr('item'),'-');
		  	}
		    return false;
		  }
	  
	  function remove_item(){
		  	if (confirm("Deseja realmente remover?")){
				$(this).parent().parent().remove();
				atualiza_qtd($(this).attr('item'),'-');
		  	}
		    return false;
		  }
  
  /*-----------------------------------fim jeferson presc 30/07/12---------------------*/
  
  $("#add-item").click(function(){
     if(validar_campos("formulario")){
      var cod = $("#cod").val(); var nome = $("#formulario input[name='nome']").val(); 
//      var apr = $("#formulario input[name='apr']").val(); 
	  var inicio = $("#formulario input[name='inicio']").val();
      var freq = $("#formulario  select[name='frequencia'] option:selected").val();
      var nfreq = $("#formulario  select[name='frequencia'] option:selected").html();
      var um = $("#formulario  select[name='um'] option:selected").val();
      var label_um = $("#formulario  select[name='um'] option:selected").html();
      var fim = $("#formulario input[name='fim']").val(); var obs = $("#formulario input[name='obs']").val();
      var dose = $("#formulario input[name='dose']").val();
      var viadm = $("#formulario select[name='vadm-formulario'] option:selected").val();
      var nvia = $("#formulario select[name='vadm-formulario'] option:selected").html();
      var label = $(this).attr("label");
	  var linha = "<b>"+label+":</b> "+nome+". "+dose+" "+label_um+" "+nfreq+" "+nvia+". Per&iacute;odo: "+inicio+" até "+fim+" OBS: "+obs;
	 
	  var tipo = $(this).attr("tipo");
	  
      var dados = " tipo='"+tipo+"' nome='"+nome+"' frequencia='"+freq+"' inicio='"+inicio+"' fim='"+fim+"' obs='"+obs+"' cod='"+cod+"' um='"+um+"' dose='"+dose+"' via='"+viadm+"'";
      var inicio1= $("#formulario input[name='inicio']").attr('i');
      var fim1 = $("#formulario input[name='fim']").attr('f');
      
      atualiza_qtd(tipo,"+");
      $("#itens-"+tipo).after("<tr bgcolor='#ebf3ff' class='dados' "+dados+" ><td>"+linha+"</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
      reload_css_table();
      $("#cod").val("-1"); $("#formulario input[name='nome']").val(""); $("#formulario input[name='nome']").attr("readonly","readonly"); 
//      $("#formulario input[name='apr']").val(""); $("#formulario input[name='apr']").attr("readonly","readonly");
      $("#formulario input[name='aprazamento']").val(""); $("#formulario input[name='obs']").val("");
      $("#formulario select[name='frequencia'] option").eq(0).attr('selected','selected');
	  $("#formulario select[name='um'] option").eq(0).attr('selected','selected');
      $("input[name='busca']").val("");$("input[name='busca']").focus();$("#formulario input[name='dose']").val('');
      $("#formulario input[name='inicio']").val(""+inicio1+"");
      $("#formulario input[name='fim']").val(""+fim1+"");
     }	
    return false;
  });
  
  $("#rem-item").click(function(){
  	var qtd = $(".dados td .select-item:checked").size();
  	var i = 'o item'; if(qtd > 1) i = 'os '+qtd+' itens';
  	if(qtd == 0) return;
  	if (confirm("Deseja realmente deletar "+i+" ?")){
    	$(".dados td .select-item").each(function(i){
      		if($(this).attr("checked")){
      			atualiza_qtd($(this).parent().parent().attr("tipo"),"-");
				$(this).parent().parent().remove();
      		}
    	});
    	$("#tabela-itens tr:odd").css("background-color","#ebf3ff");
    	$("#tabela-itens tr").hover(
			function(){$(this).css("background-color","#3d80df");$(this).css("color","#ffffff");},
			function(){
	  			$(this).css("background-color","");$(this).css("color","");
	  			$("#tabela-itens tr:odd").css("background-color","#ebf3ff");
	  			$("#tabela-itens tr:odd").css("color","");
			}
      	);
  	}
    return false;
  });
  
  $("#susp-item").click(function(){
  	var qtd = $(".suspender td .select-item-susp:checked").size();
  	var i = 'o item'; if(qtd > 1) i = 'os '+qtd+' itens';
  	if(qtd == 0) return;
  	if (confirm("Deseja realmente suspender "+i+" ?")){
    	$(".suspender td .select-item-susp").each(function(i){
      		if($(this).attr("checked")){
      			this.checked = false;
      			$(this).removeClass("select-item-susp");
      			$(this).addClass("select-item");
      			$(this).parent().parent().children("td").eq(0).prepend("<b><i>SUSPENDER: </i></b>");
      			var item = $(this).parent().parent().attr('id_item');
      			
      			$(this).parent().parent().removeClass("suspender");
      			$(this).parent().parent().addClass("dados");
      			$("table").find("tr:#id-"+item).each(function(){
      				$(this).remove();
      			});
      			$("#tabela-itens").append($(this).parent().parent().remove());
      			
      			
				
				atualiza_qtd('s','+');
      		}
    	});
  	}
    return false;
  });
  
  $("#salvar").click(function(){
  	var p = $("#corpo-prescricao").html();
  	var paciente = $("#prescricao").attr("paciente");
  	var nome = $("#prescricao").attr("pnome");
  	$.post("query.php",{query: "salvar-prescricao", p: p, paciente: paciente, nome: nome},function(r){
  		alert(r);
  	});
  	return false;
  });
  
  $("#finalizar").click(function(){
  	if(!validar_tipos()){
  		$("#aviso").dialog("open");
  		return false;
  	}
  	salvar();
    return false;
  });
  
  $("#finalizar_presc_emergencia").click(function(){
	  	if(!validar_tipos()){
	  		$("#aviso").dialog("open");
	  		return false;
	  		
	  	}
	  	
	  	salvar_emergencial();
	    return false;
	  });
  
  $("input[name='nome']").attr("readonly","readonly");
  $("input[name='apr']").attr("readonly","readonly");
  
  $(".data").datepicker({
	defaultDate: "+1w",
	changeMonth: true,
	numberOfMonths: 1
  });
  
  $(".hora").timeEntry({show24Hours: true, defaultTime: '08:00'});
  
  $("#checktodos").click(function(){
    var checked_status = this.checked;
    $(".select-item").each(function()
    {
      this.checked = checked_status;
    });
  });
  
  $("#checktodos-susp").click(function(){
    var checked_status = this.checked;
    $(".select-item-susp").each(function()
    {
      this.checked = checked_status;
    });
  });
    
  var $tabs = $("#prescricao").tabs();
  
  $(".next-tab,.prev-tab").click(function(){
  	$tabs.tabs('select',parseInt($(this).attr('rel')));
  	return false;
  });
  
  $("#add-formula").click(function(){
  	if(validar_campos("div-formulas")){
  		var formula = $("#formula").val(); var tipo = $(this).attr("tipo");
  		var inicio = $("#div-formulas input[name='inicio']").val(); var fim = $("#div-formulas input[name='fim']").val();
  		var obs = $("#div-formulas input[name='obs']").val();
  		var via = $("#div-formulas select[name='vadm-formula'] option:selected").val();
  		var nvia = $("#div-formulas select[name='vadm-formula'] option:selected").html();
  		var freq = $("#div-formulas select[name='frequencia'] option:selected").val();
  		var nfreq = $("#div-formulas select[name='frequencia'] option:selected").html();
  		atualiza_qtd(tipo,"+");
  		var label = $(this).attr("label");
  		var linha = "<b>"+label+":</b> "+formula+" "+nfreq+" "+nvia+". Per&iacute;odo: "+inicio+" até "+fim+" OBS: "+obs;
  		var dados = " nome='"+formula+"' tipo='"+tipo+"' frequencia='"+freq+"' via='"+via+"' inicio='"+inicio+"' fim='"+fim+"' obs='"+obs+"'";
  		$("#itens-"+tipo).after("<tr bgcolor='#ebf3ff' class='dados' "+dados+" ><td>"+linha+"</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
	  	reload_css_table();
	  	$("#formula").val("");
	  	$("#div-formulas select option").eq(0).attr('selected','selected');;
	  	$("#div-formulas input[name='obs']").val("");
  	}
  });
  
  $("#add-repouso").click(function(){
  	if(validar_campos("div-repouso")){
	  	var nome = $("#div-repouso select[name='repouso'] option:selected").html();
	  	var freq = $("#div-repouso select[name='frequencia'] option:selected").val();
  		var nfreq = $("#div-repouso select[name='frequencia'] option:selected").html();
	  	var cod = $("#div-repouso select[name='repouso'] option:selected").val();
  		var obs = $("#div-repouso input[name='obs']").val(); 
	    var inicio = $("#div-repouso input[name='inicio']").val(); var fim = $("#div-repouso input[name='fim']").val();
	    var label = $(this).attr("label");
	    var linha = "<b>"+label+":</b> "+nome+" "+nfreq+". Per&iacute;odo: "+inicio+" até "+fim+" OBS: "+obs;
	    var tipo = $(this).attr("tipo");
	    var apr = "";
	    var dados = " tipo='"+tipo+"' nome='"+nome+"' frequencia='"+freq+"' inicio='"+inicio+"' fim='"+fim+"' obs='"+obs+"' cod='"+cod+"'";
	    atualiza_qtd(tipo,"+");
	  	$("#itens-"+tipo).after("<tr bgcolor='#ebf3ff' class='dados' "+dados+" ><td>"+linha+"</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
	  	reload_css_table();
	  	$("#div-repouso input[name='obs']").val(""); $("#div-repouso input[name='aprazamento']").val("");
	  	$("select[name='repouso'] option").eq(0).attr('selected','selected');
	  	$("#div-repouso select[name='frequencia'] option").eq(0).attr('selected','selected');
  	}
  });
  
  $("#add-dieta").click(function(){
  	if(validar_campos("div-dieta")){
	  	var nome = $("#div-dieta select[name='dieta'] option:selected").html();
	  	var cod = $("#div-dieta select[name='dieta'] option:selected").val();
	  	var obs = $("#div-dieta input[name='obs']").val();
	  	var pos = $("#div-dieta select[name='vadm-dieta'] option:selected").html() + " " + $("#div-dieta select[name='frequencia'] option:selected").html(); 
	  	var vadm = $("#div-dieta select[name='vadm-dieta'] option:selected").val(); var freq = $("#div-dieta select[name='frequencia'] option:selected").val(); 
	    var inicio = $("#div-dieta input[name='inicio']").val(); var fim = $("#div-dieta input[name='fim']").val();
	    var label = $(this).attr("label");
	    var linha = "<b>"+label+":</b> "+nome+" "+pos+". Per&iacute;odo: "+inicio+" até "+fim+" OBS: "+obs;
	    var tipo = $(this).attr("tipo");
	    var dados = " tipo='"+tipo+"' nome='"+nome+"' via='"+vadm+"' frequencia='"+freq+"' inicio='"+inicio+"' fim='"+fim+"' obs='"+obs+"' cod='"+cod+"'";
		atualiza_qtd(tipo,"+");
  		$("#itens-"+tipo).after("<tr bgcolor='#ebf3ff' class='dados' "+dados+" ><td>"+linha+"</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
  		reload_css_table();
  		$("#div-dieta input[name='obs']").val(""); $("#div-dieta input[name='aprazamento']").val("");
  		$("select[name='dieta'] option").eq(0).attr('selected','selected');
  		$("#div-dieta select[name='frequencia'] option").eq(0).attr('selected','selected');
  	}
  });
  
  $("#add-soro").click(function(){
  	if(validar_campos("div-soros")){
	  	var nome = $("#div-soros select[name='soros'] option:selected").html();
	  	var cod = $("#div-soros select[name='soros'] option:selected").val();
  		var obs = $("#div-soros input[name='obs']").val();
	  	var pos = $("#div-soros select[name='vadm-soro'] option:selected").html() + " " + $("#div-soros select[name='frequencia'] option:selected").html();
	  	var via = $("#div-soros select[name='vadm-soro'] option:selected").val(); var freq = $("#div-soros select[name='frequencia'] option:selected").val(); 
	    var inicio = $("#div-soros input[name='inicio']").val(); var fim = $("#div-soros input[name='fim']").val();
	    var fluxo = $("#div-soros input[name='fluxo']").val()+" "+$("#div-soros select[name='um'] option:selected").html();
	    var label = $(this).attr("label");
	    var linha = "<b>"+label+":</b> "+nome+" "+pos+" "+fluxo+". Per&iacute;odo: "+inicio+" até "+fim+" OBS: "+obs;
	    var tipo = $(this).attr("tipo");
   		var dados = " tipo='"+tipo+"' nome='"+nome+"' via='"+via+"' frequencia='"+freq+"' inicio='"+inicio+"' fim='"+fim+"' obs='"+obs+"' cod='"+cod+"'";
	    atualiza_qtd(tipo,"+");
	  	$("#itens-"+tipo).after("<tr bgcolor='#ebf3ff' class='dados' "+dados+" ><td>"+linha+"</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
  		reload_css_table();
	  	$("#div-soros input[name='obs']").val(""); $("#div-soros input[name='aprazamento']").val("");
	  	$("#div-soros input[name='fluxo']").val("");
	  	$("select[name='soros'] option").eq(0).attr('selected','selected');
	  	$("#div-soros select[name='frequencia'] option").eq(0).attr('selected','selected');
	  	$("#div-soros select[name='um'] option").eq(0).attr('selected','selected');
  	}
  });
  
  $("#add-cuidados").click(function(){
  	if(validar_campos("div-cuidados")){
	  	var cod = $("#div-cuidados select[name='cuidados'] option:selected").val();
	  	var nome = $("#div-cuidados select[name='cuidados'] option:selected").html();
  		var obs = $("#div-cuidados input[name='obs']").val();
	  	var nfreq = $("#div-cuidados select[name='frequencia'] option:selected").html();
	  	var freq = $("#div-cuidados select[name='frequencia'] option:selected").val(); 
	    var inicio = $("#div-cuidados input[name='inicio']").val(); var fim = $("#div-cuidados input[name='fim']").val();
	    var label = $(this).attr("label");
	    var linha = "<b>"+label+":</b> "+nome+". "+nfreq+" Per&iacute;odo: "+inicio+" até "+fim+" OBS: "+obs;
	    var tipo = $(this).attr("tipo");
   		var dados = " tipo='"+tipo+"' nome='"+nome+"' frequencia='"+freq+"' inicio='"+inicio+"' fim='"+fim+"' obs='"+obs+"' cod='"+cod+"'";
	    atualiza_qtd(tipo,"+");
	  	$("#itens-"+tipo).after("<tr bgcolor='#ebf3ff' class='dados' "+dados+" ><td>"+linha+"</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
  		reload_css_table();
	  	$("#div-cuidados input[name='obs']").val(""); $("#div-cuidados input[name='aprazamento']").val("");
	  	$("#div-cuidados select[name='frequencia'] option").eq(0).attr('selected','selected');
	  	$("#div-cuidados select[name='cuidados'] option").eq(0).attr('selected','selected');
	  	$("#div-cuidados select[name='frequencia'] option").eq(0).attr('selected','selected');
  	}
  });
  
  var flag = 0;
 
  
  $("#dialog-incluir-diluente").dialog({
	  	autoOpen: false, modal: true, position: 'top',
	  
	  	buttons: {
	  		"Sim":function(){
	  		
	  			$(this).dialog("close");
				verificarNeb(1);
	  			return flag;
	  			
	  			
	  		},
	  		"N\u00E3o":function(){
		  		
	  			$(this).dialog("close");
	  			verificarNeb(0);
	  			return flag;
	  			
	  			
	  		}
	  	
	  	}
	  });
  
  function verificarNeb(flag1){
		  
if(flag1==1){
			  
			  
		  
		  $("#div-nebulizacao").each(function(){ 
			  
			  
			  
	  
	         
	        	
			     var tipo = $(this).attr("tipo");
		  		 var diluicao = $("select[name='diluicao-nebulizacao'] option:selected").val();
		  		
		  		var qtd = $("input[name='qtd-diluicao']").val();
		  		var um = $("#um-diluicao-neb option:selected").html();
		  		var neb = diluicao+" "+qtd+" "+um; 
		  		
		  		//$(".comp-nebulizacao").each(function(){
		  			if($("#Atrovent").is(":checked")){
		  				neb += " + ";
		  				
		  				var med = $("input[name='Atrovent']").attr('name');
		  				var qtd_med = $("#comp-nebulizacao input[name='neb']").val();
		  				var um_med = $("#comp-nebulizacao select option:selected").html();
		  				//var qtd_med = $(this).parent().parent().children('td').eq(2).children('input').val();;
		  				//var um_med = $(this).parent().children('td').eq(3).find("select option:selected").html();
		  				neb += med+" "+qtd_med+" "+um_med;
		  				
		  			}
		  			
		  			if($("#Berotec").is(":checked")){
		  				neb += " + ";
		  				var med = $("#comp-nebulizacao2 input[name='Berotec']").attr('name');
		  				var qtd_med = $("#comp-nebulizacao2 input[name='neb']").val();
		  				var um_med = $("#comp-nebulizacao2 select option:selected").html();
		  				neb += med+" "+qtd_med+" "+um_med;
		  				
		  			}
		  		//});
		  			
		  		if($("#check-outro-neb").attr("checked")){
		  			if(neb != "") neb += " + ";
		  			var med = $("#nome-outro-neb").val();
		  			var qtd_med = $("#dose-outro-neb").val();
		  			var um_med = $("#um-outro-neb option:selected").html();
		  			neb += med+" "+qtd_med+" "+um_med;
		  			
		  		}
		  		
		  		var obs = $("#div-nebulizacao input[name='obs']").val();
		  		var inicio = $("#div-nebulizacao input[name='inicio']").val(); var fim = $("#div-nebulizacao input[name='fim']").val();
		  		var freq = $("#div-nebulizacao select[name='frequencia'] option:selected").val();
		  		var nfreq = $("#div-nebulizacao select[name='frequencia'] option:selected").html();
		  		var linha = neb+". "+nfreq+" Per&iacute;odo: "+inicio+" até "+fim+" OBS: "+obs;
		  		
		  		var dados = " tipo='"+tipo+"' nome='"+neb+"' frequencia='"+freq+"' inicio='"+inicio+"' fim='"+fim+"' obs='"+obs+"'";
		  		
		  		atualiza_qtd(tipo,"+");
		  		$("#itens-"+tipo).after("<tr bgcolor='#ebf3ff' class='dados' "+dados+" ><td><b>Nebuliza&ccedil;&atilde;o: </b>"+linha+"</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
		  		reload_css_table();
		  		$("input[name='qtd-diluicao']").val(''); $("#um-diluicao-neb option").eq(0).attr('selected','selected');
		  		$("#div-nebulizacao select[name='frequencia'] option").eq(0).attr('selected','selected');
		  		$("input[name='obs']").val('');
		  		$(".comp-nebulizacao").each(function(){
		  			if($(this).children('td').eq(0).children('input').attr("checked")){
		  				$(this).children('td').eq(2).children('input').val('');
		  				$(this).children('td').eq(2).children('input').attr('disabled','diabled');
		  				$(this).children('td').eq(2).children('input').removeClass("OBG");
		  				$(this).children('td').eq(2).children('input').removeAttr('style');
		  				$(this).children('td').eq(3).children('select').removeClass("COMBO_OBG");
		  				$(this).children('td').eq(3).children('select').removeAttr('style');
		  				$(this).children('td').eq(3).find("select option").eq(0).attr('selected','selected');
		  				$(this).children('td').eq(0).children('input').attr("checked","");
		  				$("#diluicao-nebulizacao ").val("");
				   		//$("#frequencia").removeClass("COMBO_OBG");
						$("#frequencia").removeAttr("style");
						$("#frequencia").val("-1");
		  			}
		  		});
		  		$("#nome-outro-neb").removeClass("OBG").val("");
		   		$("#dose-outro-neb").removeClass("OBG").attr('disabled','').val("");
		   		$('#um-outro-neb').removeClass("COMBO_OBG");
		   		$("#um-outro-neb option").eq(0).attr('selected','selected');
		   		$("#check-outro-neb").attr("checked","");
		   		$("#diluicao-nebulizacao ").val("");
		   		
				
				   $("#um-diluicao-neb").val("-1");
			  	   $("#qtd-diluicao").val("");
			  	   $("#diluicao-nebulizacao").val("");
			  	   $("#frequencia").val("-1");
			  		
			  		//$("#qtd-diluicao").removeClass("OBG");
					//$("#um-diluicao-neb").removeClass("COMBO_OBG");
	                //$("#diluicao-nebulizacao").removeClass("OBG");
					//$("#frequencia").removeClass("COMBO_OBG");
					
					$("#um-diluicao-neb").removeAttr("style");
					$("#frequencia").removeAttr("style");
					$("#qtd-diluicao").removeAttr("style");
					$("#diluicao-nebulizacao").removeAttr("style");
				
				
				
				nome='';
		  		dosecomp='';
		  		dose='';
		  		freq='';
		  		obs='';
		  		inicio='';
		  		oxi='';
		  		nfreq='';
		  		linha='';
		  		dados='';
		  	
		 
	
		 	});
	  }
  }
  
  
  $("#add-nebulizacao").click(function(){
	  
	
	  if(validar_campos("div-nebulizacao")){
		 /* if($("#Atrovent").not(":checked")&& $("#Berotec").not(":checked") && $("#check-outro-neb").not(":checked") && $("select[name='diluicao-nebulizacao'] option:selected").val()!="-1"){		      		   
			  $("#dialog-incluir-diluente").dialog("open");
			  alert('erro ak');
			  
	      }else{
	    	  flag = 1;
	    	  verificarNeb(flag);
	      }
	      jeferson nebulizacao 
	      */
		  
 if($("#Atrovent").is(":checked") || $("#Berotec").is(":checked") || $("#check-outro-neb").is(":checked") ){		      		   
			  
			  flag = 1;
	    	  verificarNeb(flag);
			  
	      }else{
	    	  $("#dialog-incluir-diluente").dialog("open");
	      }
	}
	  
	 
	  
  });
  
  
  
  ///add oxigenoterapia jeferson
  
  $("#add-oxigenoterapia").click(function(){
	  	if(validar_campos("div-oxigenoterapia")){
	  		$(".oxi").each(function(){
	  			
	  			if($(this).is(':checked')){
	  				
	  				
	  		
	  		
	  		var tipo = 13;
	  		var  d;
	  		var oxigen;
	  		
	  		
	  				if($(this).attr('tipo')==0){
	  					//var nome = $(this).parent().children('b').html();
		  				//var dose = $(this).parent().parent().children('td').eq(1).children('input').val();
	  					nome= $(this).parent().children('b').html();
	  					d= $(this).parent().parent().children('td').eq(1).children('input').val();
	  					var m = "l/min";
	  					oxigen= (nome+" "+d+""+m);
	  					
	  				}
	  				if($(this).attr('tipo')==1){
	  					nome=$(this).parent().children('b').html();
	  					
	  					qtd= $(this).parent().parent().children('td').eq(1).children('select').find('option').filter(':selected').text();
	  					oxigen= nome+" "+qtd;
	  					d = $(this).parent().parent().children('td').eq(1).children('select').find('option').filter(':selected').attr('med');
	  				
	  					///alert($(this).parent().children('b').html());
	  					//alert( $(this).parent().parent().children('td').eq(1).children('select').find('option').filter(':selected').attr('med'));
	  					///alert($(this).parent().parent().children('td').eq(1).children('select').val());
	  					///alert($(this).parent().parent().children('td').eq(1).children('select').find('option').filter(':selected').text());
	  				} 
	  				if($(this).attr('tipo')==3){
	  					nome=$(this).parent().children('b').html();
	  					
	  					qtd= $(this).parent().parent().children('td').eq(1).children('select').find('option').filter(':selected').text();
	  					oxigen= nome+" "+qtd;
	  					d = $(this).parent().parent().children('td').eq(1).children('select').find('option').filter(':selected').attr('med');
	  				
	  					///alert($(this).parent().children('b').html());
	  					//alert( $(this).parent().parent().children('td').eq(1).children('select').find('option').filter(':selected').attr('med'));
	  					///alert($(this).parent().parent().children('td').eq(1).children('select').val());
	  					///alert($(this).parent().parent().children('td').eq(1).children('select').find('option').filter(':selected').text());
	  				} 
	  				
	  				if($(this).attr('tipo')==2){
	  					
	  					 nome= $("input:radio[tipo=2]").attr('amb');
	  					oxigen = nome;
	  					//alert($("input:radio[tipo=2]").attr('amb'));
	  					//alert($(this).parent().parent().children('td').eq(1).children('select').find('option').filter(':selected').text());
	  				} 
	  			
	  		
	  		
	  		
			  		var dose=d;
			  		var um = "28";
			  		var obs = $("#div-oxigenoterapia input[name='obs']").val();
			  		var inicio = $("#div-oxigenoterapia input[name='inicio']").val(); var fim = $("#div-oxigenoterapia input[name='fim']").val();
			  		var freq = $("#div-oxigenoterapia select[name='frequencia'] option:selected").val();
			  		var nfreq = $("#div-oxigenoterapia select[name='frequencia'] option:selected").html();
			  		var linha = oxigen+". "+nfreq+" Per&iacute;odo: "+inicio+" até "+fim+" OBS: "+obs;
			  		var dados = " tipo='"+tipo+"' nome='"+oxigen+"' frequencia='"+freq+"' inicio='"+inicio+"' fim='"+fim+"' obs='"+obs+"' dose='"+dose+"' um='"+um+"'";
			  		
			  		atualiza_qtd(tipo,"+");
			  		$("#itens-"+tipo).after("<tr bgcolor='#ebf3ff' class='dados' "+dados+" ><td><b>Oxigenoterapia: </b>"+linha+"</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
			  		reload_css_table();
			
			  		$("#div-oxigenoterapia select[name='frequencia'] option").eq(0).attr('selected','selected');
			  		$("input[name='obs']").val('');
			  		
			  		$(".oxi2").each(function(){
			  		  
			 		   $("input[name='dose']").removeClass("OBG");
			 		   $("input[class='oxi2']").attr('disabled','disabled');
			 		   $("input[class='oxi2']").val("");
			 		  $("input[name='dose']").removeAttr("style");
			 		   
			 		   $("select[name='dose']").removeClass("OBG");
			 		   $("select[class='oxi2']").attr('disabled','disabled');
			 		   $("select[class='oxi2']").val("");
			 		  $("select[name='dose']").removeAttr("style");
			 		  	 		   
			 	   });
			  		$("#frequenciaoxi").removeAttr("style");
			  		$("#frequenciaoxi").removeClass("COMBO_OBG");
			  		$("#frequenciaoxi").val("-1");
			  		$("input[class='oxi']").attr("checked",false);
			  		
			  		nome='';
			  		dosecomp='';
			  		dose='';
			  		freq='';
			  		obs='';
			  		inicio='';
			  		oxi='';
			  		nfreq='';
			  		linha='';
			  		dados='';
	  			}	  			 			
	  			
	  		});	  			
	  	}
	  });
  
  $('.criar-presc').live('click',function(){
	 // var view = this.attr('view');  
	  
	  $("input[name='paciente']").val($(this).attr('CodPaciente'));
	  $("input[name='antigas']").val($(this).attr('p'));
	  $("#NomePacientePresc").html($(this).attr('paciente'));	  
	  $("#pre-presc").attr('busca',$(this).attr('busca'));
	  $("#pre-presc").dialog("open");
  });
  
  $("#pre-presc").dialog({
  	autoOpen: false, modal: true, width: 400, position: 'top',
  	open: function(event,ui){
  		
  	},
  	buttons: {
  		"Fechar" : function(){
  			$(this).dialog("close");
  		}
  	}
  });

  $("#voltar").click(function(){
	 history.go(-1); 
  });
  
});