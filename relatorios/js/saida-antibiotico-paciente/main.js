$(function($) {

    $(".chosen-select").chosen({search_contains: true});

    $("#pesquisar-saida-antibiotico-paciente").click(function(){
        if(!validar_campos('right')){
            return false;

        }

    });
    function validar_campos(div){
        var ok = true;
        $("#"+div+" .OBG").each(function (i){

            if(this.value == ""){
                ok = false;
                this.style.border = "3px solid red";
            } else {
                this.style.border = "";
            }

        });
        $("#"+div+" .COMBO_OBG option:selected").each(function (i){
            if(this.value == "-1"){
                ok = false;
                $(this).parent().css({"border":"3px solid red"});
            } else {
                $(this).parent().css({"border":""});
            }
        });

        $("#"+div+" .OBG_CHOSEN option:selected").each(function (i){

            if(this.value == "" || this.value == -1){

                $(this).parent().parent().children('div').css({"border":"3px solid red"});
            } else {
                console.log($(this).parent().parent().children('div').attr('style'));
                var estilo = $(this).parent().parent().children('div').attr('style');
                console.log(estilo.replace("border: 3px solid red",''));
                $(this).parent().parent().children('div').removeAttr('style');
                $(this).parent().parent().children('div').attr('style',estilo.replace("border: 3px solid red",''));
            }
        });

        if(!ok){
            $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
            $("#"+div+" .div-aviso").show();
        } else {
            $("#"+div+" .aviso").text("");
            $("#"+div+" .div-aviso").hide();
        }
        return ok;
    }
});
