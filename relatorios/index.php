<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

//ini_set('display_errors', 1);

use \App\Controllers\RelatoriosMedicoEnfermagemUr;

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
	"GET" => [
		'/relatorios/?medico-enfermagem-ur=form' => '\App\Controllers\RelatoriosMedicoEnfermagemUr::form',
		'/relatorios/?medico-enfermagem-ur=exportar-excel' => '\App\Controllers\RelatoriosMedicoEnfermagemUr::exportarExcel',
		'/relatorios/?saida-antibiotico-paciente=form' => '\App\Controllers\RelatorioSaidaAntibioticoPaciente::form',
		'/relatorios/?saida-antibiotico-paciente=exportar-excel' => '\App\Controllers\RelatorioSaidaAntibioticoPaciente::exportarExcel'
	],
	"POST" => [
		'/relatorios/?medico-enfermagem-ur=pesquisar' => '\App\Controllers\RelatoriosMedicoEnfermagemUr::pesquisar',
		'/relatorios/?saida-antibiotico-paciente=pesquisar' => '\App\Controllers\RelatorioSaidaAntibioticoPaciente::pesquisar'
    ]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}
