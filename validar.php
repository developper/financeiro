<?php
@session_start();
if(((empty($_SESSION['id_user']) && !isset($_SESSION['id_user'])) || (empty($_SESSION['empresa_principal']) && !isset($_SESSION['empresa_principal'])))) {
  session_destroy();
}

if(!function_exists('redireciona')){
	function redireciona($link){
		if ($link==-1){
			echo" <script>history.go(-1);</script>";
		}else{
      echo" <script>document.location.href='$link'</script>";
		}
	}
}

  function validar_tipo($mod, $session_id_usuario = null, $session_id_ur = null)
  {
    if(($_SESSION[$mod] == '1' || $_SESSION["adm_user"] == "1" || $_SESSION["adm_ur"]==1))
      return true;

    return false;
  }
  
  function validar_adm(){
    if($_SESSION["adm_user"] == "1")
      return true;
    return false;
  }

  if(!isset($_SESSION["logado"]) ) 
  	redireciona('/');
  else if($_SESSION["logado"] <> "1" ){
    redireciona('/');
  }
?>
