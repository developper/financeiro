$(function($) {
    $(".chosen-select").chosen({search_contains: true});

    $("input[name='cidade_cliente']").autocomplete({
        source: "/utils/busca_cidade.php",
        minLength: 3,
        select: function( event, ui ) {
            //$("input[name='busca-cidade']").val('');
            $("input[name='id_cidade_cliente']").val(ui.item.id);
            $("input[name='cidade_cliente']").val(ui.item.value);
            $('#local').focus();
            ///$('#busca-cidade').val('');
            return false;
        }
    });

    $("input[name='cidade_cliente']").keypress(function(){
        $("input[name='id_cidade_cliente']").val('');

    });


    $(".valor").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $('.telefone').mask('(99) 9999-9999');
    $('.cnpj').mask('99.999.999/9999-99');
    $('.cpf').mask('999.999.999-99');

    $(".cadastro-paciente").hide();

    if($("#paciente-existe").val() == 'S'){
        $('#nome-paciente').attr('disabled',true).removeClass('OBG').attr('style','').val('');
        $('#sexo').attr('disabled',true).removeClass('OBG').attr('style','').val('');
        $('.cpf').attr('disabled',true).attr('style','').val('').removeClass('OBG');
        $('#nascimento').attr('disabled',true).removeClass('OBG').attr('style','').val('');
        $('.paciente').attr('disabled',false).addClass('OBG_CHOSEN');
        $(".cadastro-paciente").hide();
        $("#paciente").trigger("chosen:updated");

    }


    $("#paciente-existe").change(function(){

        $('#nome-paciente').attr('disabled',true).removeClass('OBG').attr('style','').val('');
        $('#sexo').attr('disabled',true).removeClass('OBG').attr('style','').val('');
        $('.cpf').attr('disabled',true).attr('style','').val('').removeClass('OBG');
        $('#nascimento').attr('disabled',true).removeClass('OBG').attr('style','').val('');
        $('.paciente').attr('disabled',false).addClass('OBG_CHOSEN');
        $(".cadastro-paciente").hide();
        if($(this).val() == 'N'){
            $(".cadastro-paciente").show();
            $('#nome-paciente').attr('disabled',false).addClass('OBG');
            $('#sexo').attr('disabled',false).addClass('OBG');
            $('.cpf').attr('disabled',false).addClass('OBG');
            $('#nascimento').attr('disabled',false).addClass('OBG');
            $('.paciente').attr('disabled',true).removeClass('OBG_CHOSEN').val('');
            $('.chosen-select').trigger('chosen:updated');
            $('#convenio_chosen').val('');
            $('#matricula').val('');
            $('#telefone-paciente').val('');
        }
        $("#paciente").trigger("chosen:updated");

    });

    $(".paciente").change(function(){
        var paciente = $(this).val();
        $.get( '/remocao/?action=dados-paciente',
            {
                paciente: paciente
            },
            function(r){
                var obj = jQuery.parseJSON(r);
                $('#convenio').val(obj.idPlano);

                if(obj.idPlano == 1) {
                    $("#matricula").attr('style', '');
                    $("#matricula").attr('disabled', 'disabled').removeClass('OBG').val('');
                }else{
                    $('#matricula').val(obj.matricula);

                }
                var nomePlano = ($('#convenio').children('option:selected').html());
                $('#convenio_chosen').find('.chosen-single').find('span').text(nomePlano);
               // $('.cpf').val(obj.cpf);

               // $('#nascimento').val(obj.nascimento.split('-').reverse().join('/'));
                $('#telefone-paciente').val(obj.telefone_paciente);
            });
    });

    //Cpf
    function verificarCPF(strCPF) {
        var soma = 0;
        var resto;
        if (strCPF == "00000000000")
            return false;
        for (i=1; i<=9; i++)
            soma = soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(strCPF.substring(9, 10)) )
            return false;
        soma = 0;
        for (i = 1; i <= 10; i++)
            soma = soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(strCPF.substring(10, 11) ) )
            return false;

        return true;
    }


    $('.cpf').blur(function(){
        var cpf = $(this).val();
        var validarNaReceita = $(this).attr('validar-receita');
        if(cpf != '') {
            var cpfInput = cpf;
            var tipoCpf = $(this).attr('tipo');
            exp = /\.|\-/g
            cpf = cpf.toString().replace(exp, "");

            var isValido = verificarCPF(cpf);
            var CPFExistente = false;
            var action = tipoCpf == 'paciente' ? 'consultar-cpf-paciente' : 'consultar-cpf-cnpj-cliente';

            $.get(
                '../remocao/',
                {
                    action: action,
                    cpf: cpfInput
                },
                function (response) {
                    if ((response == cpfInput || response == cpf) && cpf != '11111111111') {
                        CPFExistente = !CPFExistente;
                    }

                    if (!isValido) {
                        alert('CPF Invalido!');
                        $('.cpf').val('').css({"border": "3px solid red"});
                    } else {
                        if (CPFExistente) {
                            alert('Esse CPF já foi cadastrado!');
                            $('.cpf').val('').css({"border": "3px solid orange"});
                        } else {
                            $('.cpf').css({"border": "3px solid green"});
                            //if(validarNaReceita == 'sim'){
                            //    consultarCPFReceita();
                            //}

                        }
                    }
                }
            );
        }
    });
// Parou de funcionar a api em 23/10/2017


    $("#salvar-remocao-particular").click(function(){
        if(validar_campos('right')){
            var dados = $('form').serialize();

            $.post( '/remocao/?action=salvar-remocao-particular',
                dados,
                function(r){
                    if(r == 1){
                        alert('Remoção salva com sucesso!');
                        window.location = "?action=particular-form";
                    }else{
                        alert("Erro ao salvar remoção. Entre em contato com o TI."+r);
                        return false;
                    }
                }
            );
        }
        return false;
    });

    $("#cliente_existe").change(function () {
        if($(this).val() == 'N'){
            $("#fornecedor-cliente").attr('disabled', true).val('');

            $("#fornecedor-cliente").removeClass('OBG_CHOSEN');
            var estilo = $("#fornecedor-cliente").parent().children('div').attr('style');
            $("#fornecedor-cliente").parent().children('div').removeAttr('style');
            $("#fornecedor-cliente").parent().children('div').attr('style', estilo.replace("border: 3px solid red",''));
            $("#fornecedor-cliente-nao-existe").show();
            $("#fornecedor-cliente-nao-existe input").each(function () {
                $(this).addClass('OBG');
            });
            $('#cnpj').removeClass('OBG');
            $('#cpf').attr('disabled', false);
            $('#im').removeClass('OBG');
            $(".fisica-juridica").each(function(){
               if($(this).val() == 0){
                   $(this).attr('checked', true);
               }else{
                   $(this).attr('checked', false);
               }
            });

        }else{
            $("#fornecedor-cliente").attr('disabled', false).val('');
            $("#fornecedor-cliente").addClass('OBG_CHOSEN');
            $("#fornecedor-cliente-nao-existe").hide();
            $("#fornecedor-cliente-nao-existe input[type!='radio']").each(function () {
                $(this).removeClass('OBG');
                $(this).val('');
                $(this).attr('style','');
            });
            $("#fornecedor-cliente-nao-existe input[type='radio']").each(function () {
                $(this).removeClass('OBG');
                $(this).removeClass('OBG_RADIO');
                $(this).attr('checked', false);
                $(this).attr('style','');
            });

        }
        $("#fornecedor-cliente").trigger("chosen:updated");

    });

    $("#cancelar-novo").click(function () {
        $("#fornecedor-cliente").removeAttr('disabled').val('');
        $("#fornecedor-cliente-nao-existe").hide();
        $("#fornecedor-cliente-nao-existe input").each(function () {
            $(this).val('').removeClass('OBG');
            $("#cpf_cnpj_0").prop('checked', true);
            $("#cpf").attr('disabled', 'disabled');
        });
    });

    if($("#recebido").val() != 'S'){
        $("#forma-pagamento").attr('disabled', 'disabled').val('');
        $("#forma-pagamento").removeClass('OBG');
        $("#forma-pagamento").removeAttr('style');
        $("#conta-recebido").attr('disabled', 'disabled').val('');
        $("#conta-recebido").removeClass('OBG_CHOSEN');
        $("#conta-recebido").trigger("chosen:updated");
        $("#recebido-em").removeAttr('style');
        $("#recebido-em").attr('disabled', 'disabled').val('');
        $("#recebido-em").removeClass('OBG');
    }else{
        $("#forma-pagamento").removeAttr('disabled');
        $("#forma-pagamento").addClass('OBG');
        $("#conta-recebido").removeAttr('disabled');
        $("#conta-recebido").addClass('OBG_CHOSEN');

        $("#recebido-em").removeAttr('disabled');
        $("#recebido-em").addClass('OBG');

    }

    $("#recebido").change(function () {
        if($(this).val() != 'S') {
            $("#forma-pagamento").attr('disabled', 'disabled').val('');
            $("#forma-pagamento").removeClass('OBG');
            $("#forma-pagamento").removeAttr('style');
            $("#conta-recebido").attr('disabled', 'disabled').val('');
            $("#conta-recebido").removeClass('OBG_CHOSEN');
            var estilo = $("#conta-recebido").parent().children('div').attr('style');
            $("#conta-recebido").parent().children('div').removeAttr('style');
            $("#conta-recebido").parent().children('div').attr('style', estilo.replace("border: 3px solid red",''));

            $("#recebido-em").removeAttr('style');
            $("#recebido-em").attr('disabled', 'disabled').val('');
            $("#recebido-em").removeClass('OBG');
        } else {
            $("#forma-pagamento").removeAttr('disabled');
            $("#forma-pagamento").addClass('OBG');
            $("#conta-recebido").removeAttr('disabled');
            $("#conta-recebido").addClass('OBG_CHOSEN');

            $("#recebido-em").removeAttr('disabled');
            $("#recebido-em").addClass('OBG');

        }
        $("#conta-recebido").trigger("chosen:updated");
    });

    $(".fisica-juridica").change(function() {
        var $this = $(this);
        var selected = $this.val();
        if(selected == '1'){
            $('#cpf').attr('disabled', true).val('');
            $('#cpf').removeClass('OBG');
            $('#cnpj').removeAttr('disabled');
            $('#cnpj').addClass('OBG');
            $("#im").removeAttr('disabled');
            $('#im').addClass('OBG');
            $('#cpf').attr('style','');

            $(".label-nome-fantasia").text('Nome Fantasia:');
            $(".label-razao-social").text('Razão Social:');
            $(".label-ies-rg").text('Insc. Estadual:');
        } else {
            $('#cnpj').attr('disabled', true).val('');
            $('#cpf').removeAttr('disabled');
            $('#cnpj').removeClass('OBG');
            $('#im').removeClass('OBG');
            $("#im").attr('disabled', true).val('');
            $('#cpf').addClass('OBG');
            $('#im').attr('style','');
            $('#cnpj').attr('style','');

            $(".label-nome-fantasia").text('Nome:');
            $(".label-razao-social").text('Apelido:');
            $(".label-ies-rg").text('RG:');
        }
    });

    $('#cnpj').blur(function(){
        var cnpj = $(this).val(),
            isValid;
        if(cnpj != '') {
            isValid = verificarCNPJ(cnpj);
            var CNPJExistente = false;
            var action = 'consultar-cpf-cnpj-cliente';

            $.get(
                '../remocao/',
                {
                    action: action,
                    cnpj: cnpj
                },
                function (response) {
                    exp = /\.|\-|\//g
                    var cnpjFiltrado = cnpj.toString().replace( exp, "" );
                    if(response == cnpj || response == cnpjFiltrado){
                        CNPJExistente = !CNPJExistente;
                    }

                    if (!isValid){
                        alert('CNPJ Invalido!');
                        $('#cnpj').val('').css({"border":"3px solid orange"});
                    } else {
                        if(CNPJExistente) {
                            alert('Esse CNPJ já foi cadastrado!');
                            $('#cnpj').val('').css({"border":"3px solid orange"});
                        } else {
                            $('#cnpj').css({"border": "3px solid green"});
                        }
                    }
                }
            );
        }
    });

    function verificarCNPJ(strCNPJ) {
        var valida = new Array(6,5,4,3,2,9,8,7,6,5,4,3,2);
        var dig1 = new Number;
        var dig2 = new Number;

        var exp = /\.|\-|\//g
        var cnpj = strCNPJ.toString().replace( exp, "" );
        var digito = new Number(eval(cnpj.charAt(12) + cnpj.charAt(13)));

        for(var i = 0; i < valida.length; i++){
            dig1 += (i > 0 ? (cnpj.charAt(i - 1) * valida[i]) : 0);
            dig2 += cnpj.charAt(i) * valida[i];
        }
        dig1 = (((dig1 % 11) < 2) ? 0 : (11 - (dig1 % 11)));
        dig2 = (((dig2 % 11) < 2) ? 0 : (11 - (dig2 % 11)));

        if(((dig1 * 10) + dig2) != digito) {
            return false;
        }
        return true;
    }

    $("#finalizar-remocao-particular").click(function(){
        if($('#cliente_existe').val() == 'N'){
            if($('#id_cidade_cliente').val() == '' || $('#id_cidade_cliente').val() == ''){
                alert('O campo Cidade do Cliente deve ser escolhido na lista e não digitado completamente.');
                return false;
            }
        }
        if(validar_campos('right')){

            var dados = $('form').serialize();
            $.post( '/remocao/?action=finalizar-remocao-particular',
                dados,
                function(r){
                    if(r == 1){
                        alert('Remoção finalizada com sucesso!');
                        window.location = "?action=pesquisar-form";
                    }else{
                        alert("Erro ao finalizar a remoção. "+r);
                        return false;
                    }
                });
        }
        return false;
    });

    function validar_campos(div){
        var ok = true;
        $("#"+div+" .OBG").each(function (){
            if(this.value == ""){
               console.log($(this));
                ok = false;
                this.style.border = "3px solid red";
            } else {
                this.style.border = "";
            }
        });

        $("#"+div+" .COMBO_OBG option:selected").each(function (){
            if(this.value == "-1" ||this.value == ""  ){
                ok = false;
                console.log($(this));
                $(this).parent().css({"border":"3px solid red"});
            } else {
                $(this).parent().css({"border":""});
            }
        });

        $("#"+div+" .OBG_CHOSEN option:selected").each(function (){
            if(this.value == "" || this.value == -1){
                console.log($(this));
                ok = false;
                $(this).parent().parent().children('div').css({"border":"3px solid red"});
            } else {
                var estilo = $(this).parent().parent().children('div').attr('style');
                $(this).parent().parent().children('div').removeAttr('style');
                $(this).parent().parent().children('div').attr('style', estilo.replace("border: 3px solid red",''));
            }
        });

        if(!ok){
            $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
            $("#"+div+" .div-aviso").show();
        } else {
            $("#"+div+" .aviso").text("");
            $("#"+div+" .div-aviso").hide();
        }
        return ok;
    }

    $(".visualizar").live('click',function(){
        var remocao = $(this);
        var id = remocao.attr('id-remocao');
        $.post( '/remocao/?action=excluir-remocao',
            {id:id},

            function(r){

                if(r == 1){
                    remocao.parent().parent().parent().remove();
                    alert('Remocão cancelada com sucesso!');
                }else{
                    alert('Erro ao cancelar remoção. Entre em contato com a equipe do TI');
                }
            });
    });

    $(".cancelar").live('click',function(){
        var remocao = $(this);
        var id = remocao.attr('id-remocao');
        $.post( '/remocao/?action=excluir-remocao',
            {id:id},

            function(r){

                if(r == 1){
                    remocao.parent().parent().parent().remove();
                    alert('Remocão cancelada com sucesso!');
                }else{
                    alert('Erro ao cancelar remoção. Entre em contato com a equipe do TI');
                }
            });

    });

    $("#convenio").change(function () {
        var val = $(this).val();

        if(val == '1') {
            $("#matricula").attr('style', '');
            $("#matricula").attr('disabled', 'disabled').removeClass('OBG').val('');
        } else {
            $("#matricula").removeAttr('disabled').addClass('OBG');

        }
    });

    $("#remocao_sera_cobrada").change(function(){
        var val = $(this).val();
        $("#justificativa_nao_cobrar").attr('style', '');
        $("#justificativa_nao_cobrar").attr('disabled', 'disabled').removeClass('OBG').val('');
        $("#modo_cobranca").attr('style', '');
        $("#modo_cobranca").attr('disabled', 'disabled').removeClass('OBG').val('');

        if(val == 'S') {
            $("#modo_cobranca").removeAttr('disabled').addClass('OBG');

        } else if(val == 'N') {

            $("#justificativa_nao_cobrar").removeAttr('disabled').addClass('OBG');
            $("select[name ='remocao-fatura']").removeClass('OBG_CHOSEN');
            $("select[name ='remocao-fatura']").attr('disabled','disabled');
            //$("select[name ='remocao-fatura']").attr('style','');
            $("select[name ='remocao-fatura']").find('option').eq(0).attr('selected',true);
            $("select[name ='remocao-fatura']").parent().children('div').eq(0).children('a').children('span').html('Selecione...');
            $("select[name ='remocao-fatura']").trigger("chosen:updated");


        }
    });

    $("#modo_cobranca").change(function(){
        var val = $(this).val();
        $("select[name ='remocao-fatura']").removeClass('OBG_CHOSEN');
        $("select[name ='remocao-fatura']").attr('disabled','disabled');
        //$("select[name ='remocao-fatura']").attr('style','');
        $("select[name ='remocao-fatura']").find('option').eq(0).attr('selected',true);
        $("select[name ='remocao-fatura']").parent().children('div').eq(0).children('a').children('span').html('Selecione...');



        if(val == 'F') {
            $("select[name ='remocao-fatura']").removeAttr('disabled').addClass('OBG_CHOSEN');


        }
        $("select[name ='remocao-fatura']").trigger("chosen:updated");
    });

    $("#editar-remocao-particular-finalizada").click(function () {

        if($('#cliente_existe').val() == 'N'){
            if($('#id_cidade_cliente').val() == '' || $('#id_cidade_cliente').val() == -1){
                alert('O campo Cidade do Cliente deve ser escolhido na lista e não digitado completamente.');
                return false;
            }
        }
        if(validar_campos('right')){

            var dados = $('form').serialize();
            $.post( '/remocao/?action=editar-remocao-particular-finalizada',
                dados,
                function(r){
                    //console.log(r);
                    if(r == 1){
                        alert('Remoção editada com sucesso!');
                        window.location = "?action=pesquisar-form";
                    }else{
                        alert("Erro ao editar a remoção. Entre em contato com o TI. "+r);
                        return false;
                    }
                });
        }
        return false;

    });

    $("#editar-remocao-particular-nao-finalizada").click(function () {
        if(validar_campos('right')){
            var dados = $('form').serialize();
            $.post( '/remocao/?action=editar-remocao-particular-nao-finalizada',
                dados,
                function(r){

                    if(r == 1){
                        alert('Remoção editada com sucesso!');
                        window.location = "?action=pesquisar-form";
                    }else{
                        alert("Erro ao editar a remoção. Entre em contato com o TI. "+r);
                        return false;
                    }
                });
        }
        return false;

    });

    if($("#remocao_sera_cobrada").val() == 'S'){
        $("#modo_cobranca").removeAttr('disabled').addClass('OBG');

    }

    if($("#remocao_sera_cobrada").val() == 'N') {
        $("#justificativa_nao_cobrar").removeAttr('disabled').addClass('OBG');
    }


    if($("#modo_cobranca").val() == 'F'){
        console.log('ok');
        $("select[name ='remocao-fatura']").removeAttr('disabled').addClass('OBG_CHOSEN');
        $("select[name ='remocao-fatura']").trigger("chosen:updated");
    }


});
