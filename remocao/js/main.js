$(function($) {
    $(".chosen-select").chosen({search_contains: true});

    $('.telefone').mask('(99) 9999-9999');

    $("#paciente-convenio-form").change(function(){
        var paciente = $(this).val();
        $.get( '/remocao/?action=dados-paciente',
            {
                paciente: paciente
            },
            function(r){
                var obj = jQuery.parseJSON(r);
                $('#convenio').val(obj.idPlano);
                var nomePlano = ($('#convenio').children('option:selected').html());
                $('#convenio_chosen').find('.chosen-single').find('span').text(nomePlano);
                $('#cpf').val(obj.cpf);
                $('#matricula').val(obj.matricula);
                $('#nascimento').val(obj.nascimento.split('-').reverse().join('/'));
                $('#telefone-paciente').val(obj.telefone_paciente);
        });
    });

    $("#salvar-remocao-convenio").click(function(){
       if(validar_campos('right')){
           var solicitante = $("solicitante").val();
           var telefone_solicitante = $("telefone-solicitante").val();
           var paciente = $("paciente").val();
           var matricula = $("matricula").val();
           var cpf = $("cpf").val();
           var nascimento = $("nascimento").val();
           var telefone_paciente= $("telefone-paciente").val();
           var convenio = $("convenio").val();
           var local_origem = $("local-origem").val();
           var endereco_origem = $("endereco-origem").val();
           var local_destino= $("local-destino").val();
           var endereco_destino= $("endereco-destino").val();
           var tipo_remocao = $("tipo-remocao").val();
           var tipo_ambulancia = $("tipo-ambulancia").val();
           var retorno = $("retorno").val();
           var dados = $('form').serialize();

           $.post( '/remocao/?action=salvar-remocao-convenio',
               dados,
               function(r){
                    if(r == 1){
                        alert('Remoção salva com sucesso!');
                        window.location = "?action=convenio-form";
                    }else{
                        alert("Erro ao salvar remoção. Entre em contato com o TI");
                        return false;
                    }
               });
       }
        return false;
    });

    $('#span-remocao-finalizada-convenio').hide();
    $('#motivo-nao-realizada').hide();



    $("#remocao-realizada").change(function(){
        $('#span-remocao-finalizada-convenio').hide();
        $("#motivo").attr('style','background-color:transparent;');
        $("#motivo").removeClass('OBG');
        $("#motivo").val('');
        $('#motivo-nao-realizada').hide();

        $("#tabela-propria").removeClass('OBG');
        $("#tabela-propria").attr('style','background-color:transparent;');
        $("#tabela-propria").val('');

        $("select[name ='remocao-fatura']").removeClass('OBG_CHOSEN');
        $("select[name ='remocao-fatura']").find('option').eq(0).attr('selected',true);
        $("select[name ='remocao-fatura']").parent().children('div').eq(0).children('a').children('span').html('Selecione...');

        $("#valor-remocao").attr('style','background-color:transparent;');
        $("#valor-remocao").removeClass('OBG');
        $("#valor-remocao").val('');

        $("#km").attr('style','background-color:transparent;');
        $("#km").removeClass('OBG');
        $("#km").val('');

        $("#data-saida").attr('style','background-color:transparent;');
        $("#data-saida").removeClass('OBG');
        $("#data-saida").val('');

        $("#hora-saida").attr('style','background-color:transparent;');
        $("#hora-saida").removeClass('OBG');
        $("#hora-saida").val('');

        $("#data-chegada").attr('style','background-color:transparent;');
        $("#data-chegada").removeClass('OBG');
        $("#data-chegada").val('');

        $("#hora-chegada").attr('style','background-color:transparent;');
        $("#hora-chegada").removeClass('OBG');
        $("#hora-chegada").val('');

        $("#perimetro").attr('style','background-color:transparent;');
        $("#perimetro").removeClass('OBG');
        $("#perimetro").val('');


        $("#remocao_sera_cobrada").attr('style','background-color:transparent;');
        $("#remocao_sera_cobrada").removeClass('OBG');
        $("#remocao_sera_cobrada").val('');


        $("select[name ='empresa']").removeClass('OBG_CHOSEN');
        $("select[name ='empresa']").find('option').eq(0).attr('selected',true);
        $("select[name ='empresa']").parent().children('div').eq(0).children('a').children('span').html('Selecione...');
        if ($(this).val() == 'S'){
            $("#itens-remocao").show();
            $('#span-remocao-finalizada-convenio').show();
            $("#tabela-propria").addClass('OBG');
            $("select[name ='remocao-fatura']").addClass('OBG_CHOSEN');
            $("#valor-remocao").addClass('OBG');
            $("#km").addClass('OBG');
            $("#data-saida").addClass('OBG');
            $("#hora-saida").addClass('OBG');
            $("#data-chegada").addClass('OBG');
            $("#hora-chegada").addClass('OBG');
            $("#perimetro").addClass('OBG');
            $("#remocao_sera_cobrada").addClass('OBG');
            $("select[name ='empresa']").addClass('OBG_CHOSEN');
        }
        var estiloRemocaoFatura = $("select[name ='remocao-fatura']").parent().children('div').attr('style');

        $("select[name ='remocao-fatura']").parent().children('div').removeAttr('style');
        $("select[name ='remocao-fatura']").parent().children('div').attr('style',estiloRemocaoFatura.replace("border: 3px solid red",''));
        var estiloEmpresa = $("select[name ='empresa']").parent().children('div').attr('style');
        $("select[name ='empresa']").parent().children('div').removeAttr('style');
        $("select[name ='empresa']").parent().children('div').attr('style',estiloEmpresa.replace("border: 3px solid red",''));
        var nao = 'N';

        if ($(this).val() == nao){
            $('#motivo-nao-realizada').show();
            $("#motivo").addClass('OBG');
            $("#itens-remocao").hide();
        }
        return;
    });

    $(".valor").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $("#finalizar-remocao-convenio").click(function(){
        if(validar_campos('right')){
            var dados = $('form').serialize();
            $.post( '/remocao/?action=finalizar-remocao-convenio',
                dados,
                function(r){
                    //console.log(r);
                    if(r == 1){
                        alert('Remoção finalizada com sucesso!');
                        window.location = "?action=pesquisar-form";
                    }else{
                        alert("Erro ao finalizar a remoção. Entre em contato com o TI");
                        return false;
                    }
                });
        }
        return false;
    });

    $("select[name ='remocao-fatura']").change(function(){
        if($("#tabela-origem-item-fatura").val() == 'valorescobranca'){
           var valor = $("select[name ='remocao-fatura'] option:selected").attr('valor');
            $('#valor-remocao').val(valor);

        }

    });

    function validar_campos(div){
        var ok = true;
        $("#"+div+" .OBG").each(function (i){

            if(this.value == ""){
                ok = false;
                this.style.border = "3px solid red";
            } else {
                this.style.border = "";
            }

        });
        $("#"+div+" .COMBO_OBG option:selected").each(function (i){
            if(this.value == "-1"){
                ok = false;
                $(this).parent().css({"border":"3px solid red"});
            } else {
                $(this).parent().css({"border":""});
            }
        });

        $("#"+div+" .OBG_CHOSEN option:selected").each(function (i){

            if(this.value == "" || this.value == -1){
                ok = false;
                $(this).parent().parent().children('div').css({"border":"3px solid red"});
            } else {

               var estilo = $(this).parent().parent().children('div').attr('style');
                $(this).parent().parent().children('div').removeAttr('style');
                $(this).parent().parent().children('div').attr('style',estilo.replace("border: 3px solid red",''));
            }
        });

        if(!ok){
            $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
            $("#"+div+" .div-aviso").show();
        } else {
            $("#"+div+" .aviso").text("");
            $("#"+div+" .div-aviso").hide();
        }
        return ok;
    }

    $(".cancelar").live('click',function(){
        var remocao = $(this);
        var id = remocao.attr('id-remocao');
        $.post( '/remocao/?action=excluir-remocao',
            {id:id},
            function(r){
                if(r == 1){
                    remocao.parent().parent().parent().remove();
                    alert('Remocão cancelada com sucesso!');
                }else{
                    alert('Erro ao cancelar remoção. Entre em contato com a equipe do TI');
                }
            }
        );
    });



    $("#convenio").change(function () {
        var val = $(this).val();

        if(val == '1') {
            $("#matricula").attr('style', '');
            $("#matricula").attr('disabled', 'disabled').removeClass('OBG').val('');
        } else {
            $("#matricula").removeAttr('disabled').addClass('OBG');

        }
    });
    $("#remocao_sera_cobrada").change(function(){
        var val = $(this).val();
        $("#justificativa_nao_cobrar").attr('style', '');
        $("#justificativa_nao_cobrar").attr('disabled', 'disabled').removeClass('OBG').val('');

         if(val == 'N') {

            $("#justificativa_nao_cobrar").removeAttr('disabled').addClass('OBG');


        }
    });

    // editar remoção convenio
    if ($('#remocao-realizada').val() == 'S'){
        $("#itens-remocao").show();
        $('#span-remocao-finalizada-convenio').show();
        $("#tabela-propria").addClass('OBG');
        $("select[name ='remocao-fatura']").addClass('OBG_CHOSEN');
        $("#valor-remocao").addClass('OBG');
        $("#km").addClass('OBG');
        $("#data-saida").addClass('OBG');
        $("#hora-saida").addClass('OBG');
        $("#data-chegada").addClass('OBG');
        $("#hora-chegada").addClass('OBG');
        $("#perimetro").addClass('OBG');
        $("#remocao_sera_cobrada").addClass('OBG');
        $("select[name ='empresa']").addClass('OBG_CHOSEN');
    }

    if($("#remocao_sera_cobrada").val() == 'N') {

        $("#justificativa_nao_cobrar").attr('style', '');
        $("#justificativa_nao_cobrar").attr('disabled', false).addClass('OBG');
    }

    $('#editar-remocao-convenio-finalizada').click(function () {

        if(validar_campos('right')){
            var dados = $('form').serialize();
            $.post( '/remocao/?action=editar-remocao-convenio-finalizada',
                dados,
                function(r){
                    //console.log(r);
                    if(r == 1){
                        alert('Remoção editada com sucesso!');
                        window.location = "?action=pesquisar-form";
                    }else{
                        alert("Erro ao editar a remoção. Entre em contato com o TI");
                        return false;
                    }
                });
        }
        return false;

    });

    $('#editar-remocao-convenio-nao-finalizada').click(function () {

        if(validar_campos('right')){
            var dados = $('form').serialize();
            $.post( '/remocao/?action=editar-remocao-convenio-nao-finalizada',
                dados,
                function(r){
                    //console.log(r);
                    if(r == 1){
                        alert('Remoção editada com sucesso!');
                        window.location = "?action=pesquisar-form";
                    }else{
                        alert("Erro ao editar a remoção. Entre em contato com o TI");
                        return false;
                    }
                });
        }
        return false;

    });








});
