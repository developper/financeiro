<?php

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../db/config.php';

require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

// ini_set('display_errors', 1);

use \App\Controllers\Remocao;

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
	"GET" => [
		'/remocao/?action=menu'  => '\App\Controllers\Remocao::menu',
        '/remocao/?action=dados-paciente'  => '\App\Controllers\Remocao::dadosPaciente',
		'/remocao/?action=convenio-form'  => '\App\Controllers\Remocao::convenioForm',
		'/remocao/?action=particular-form'  => '\App\Controllers\Remocao::particularForm',
		'/remocao/?action=pesquisar-form'  => '\App\Controllers\Remocao::pesquisarForm',
		'/remocao/?action=finalizar-remocao-convenio-form' => '\App\Controllers\Remocao::finalizarRemocaoConvenioForm',
		'/remocao/?action=finalizar-remocao-particular-form' => '\App\Controllers\Remocao::finalizarRemocaoParticularForm',
		'/remocao/?action=visualizar-remocao-convenio' => '\App\Controllers\Remocao::visualizarRemocaoConvenio',
		'/remocao/?action=visualizar-remocao-particular' => '\App\Controllers\Remocao::visualizarRemocaoParticular',
		'/remocao/?action=relatorio-remocoes-realizadas' => '\App\Controllers\Remocao::relatorioRemocoesRealizadas',
		'/remocao/?action=excel-relatorio-remocoes-realizadas' => '\App\Controllers\Remocao::excelRelatorioRemocoesRealizadas',
        '/remocao/?action=consultar-cpf-paciente'  => '\App\Controllers\Administracao::consultarCPFPaciente',
        '/remocao/?action=consultar-cpf-cnpj-cliente'  => '\App\Controllers\Financeiro::consultarCPFCNPJCliente',
        '/remocao/?action=get-cpf-captcha-json'  => '\App\Controllers\Remocao::getCPFCaptchaJson',
        '/remocao/?action=get-dados-pessoa' => '\App\Controllers\Remocao::getDadosPessoaCPF',
        "/remocao/?action=editar-remocao-particular"  => '\App\Controllers\Remocao::editarFormRemocaoParticular',
        "/remocao/?action=editar-remocao-convenio"  => '\App\Controllers\Remocao::editarFormRemocaoConvenio',
	],
	"POST" => [
		'/remocao/?action=salvar-remocao-convenio'  => '\App\Controllers\Remocao::salvarRemocaoConvenio',
		'/remocao/?action=salvar-remocao-particular'  => '\App\Controllers\Remocao::salvarRemocaoParticular',
		'/remocao/?action=pesquisar-remocao'  => '\App\Controllers\Remocao::pesquisarRemocao',
		'/remocao/?action=finalizar-remocao-convenio' => '\App\Controllers\Remocao::finalizarRemocaoConvenio',
		'/remocao/?action=finalizar-remocao-particular' => '\App\Controllers\Remocao::finalizarRemocaoParticular',
		'/remocao/?action=excluir-remocao' => '\App\Controllers\Remocao::excluirRemocao',
        '/remocao/?action=relatorio-remocoes-realizadas' => '\App\Controllers\Remocao::buscarRelatorioRemocoesRealizadas',
        "/remocao/?action=editar-remocao-convenio-finalizada"  => '\App\Controllers\Remocao::updateRemocaoConvenioFinalizada',
        "/remocao/?action=editar-remocao-convenio-nao-finalizada"  => '\App\Controllers\Remocao::updateRemocaoConvenioNaoFinalizada',
        "/remocao/?action=editar-remocao-particular-finalizada"  => '\App\Controllers\Remocao::updateRemocaoParticularFinalizada',
        "/remocao/?action=editar-remocao-particular-nao-finalizada"  => '\App\Controllers\Remocao::updateRemocaoParticularNaoFinalizada',

	]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}
