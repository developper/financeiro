<?php

$id = session_id();
if (empty($id))
    session_start();
Class Tutorial{

    private function menuTutorial()
    {
        echo <<<HTML
<div>
<center><h1>Ajuda e Publicações</h1></center>
</div> 
<div>
    <b>Ajuda:</b>
    <ul style='margin-top: -3px;'>
        <li> <b>Módulo Médico: </b>
            <ul>
                <li><a href='/tutorial/index.php?view=suspender-medicamentos' target='_blank'>Suspender Medicamentos</a></li>
                <li><a href='/tutorial/index.php?view=exames' target='_blank'>Exames</a></li>
            </ul>                    
        </li>
    </ul>
    <br>
    
    <b>Documentos:</b>
    <ul style='margin-top: -3px;'>
        <li>
            <a href='/tutorial/documentos/1-etapa-pesquisa-socioeconomica-4-pesquisa-de-clima-mederi-matriz-ur-feira-2016.pdf' target='_blank'>Pesquisa de Clima 2016 – 1ª etapa: Demografia Socio-econômica</a>
        </li>
        <li>
            <a href='/tutorial/documentos/2-etapa-4-pesquisa-de-clima-organizacional-mederi-feira-matriz-2016.pdf' target='_blank'>Pesquisa de Clima 2016 – 2ª Etapa: Opinião e Relacionamento</a>
        </li>
        <li>
            <a href='/tutorial/documentos/regimento-interno-2016.pdf' target='_blank'>Regimento Interno e Governança Mederi (2016)</a>
        </li>
        <li>
            <a href='/tutorial/documentos/organograma-mederi-2016.pdf' target='_blank'>Organograma Mederi (2016)</a>
        </li>
        <li>
            <a href='/tutorial/documentos/regimento-programa-beneficios-2016.pdf' target='_blank'>Regimento do Programa de Benefícios (2015)</a>
        </li>
        <li>
            <a href='/tutorial/documentos/tabela-pontuacoes-benmederi-2016.pdf' target='_blank'>Tabela atualizada do Programa de Benefícios (2016)</a>
        </li>
        <li>
            <a href='/tutorial/documentos/plano-cargos-salario-2016.pdf' target='_blank'>Plano de Carreiras Mederi (2016)</a>
        </li>
    </ul>
    <br>
    
    <b>Processos e Fluxos Operacionais:</b>
    <ul style='margin-top: -3px;'>
        <li>
            <a href='/tutorial/fluxoUR/index.php' target='_blank'>Fluxo das Unidades Regionais (Versão Beta)</a>
        </li>
        <li>
            <a href='/tutorial/mapa_e_processo/index.html' target='_blank'>Mapa e Processos</a>
        </li>
    </ul>    
</div>
HTML;
        
    }
    
    
    
     public function viewTutorial($v) {

        switch ($v) {
            case "menu":
                $this->menuTutorial();
        break;
    
     }
     
    }
}

