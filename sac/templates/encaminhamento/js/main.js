$(function () {
    $(".chosen-select").chosen({search_contains: true});
    console.log($("#select-tipo").val());
    $("#div-paciente").hide();
    $("#div-contrato").hide();
    if($("#select-tipo").val() == 'externo' ){
        $("#div-contrato").show();
    }else if($("#select-tipo").val() == 'interno' ){
        $("#div-paciente").show();
    }



    $("#select-tipo").change(function(){
        $("#div-paciente").hide();
        $("#div-contrato").hide();
        $("#contrato").val('');
        $("#contrato").find('option').eq(0).attr('selected',true);
        $("#contrato").parent().children('div').eq(0).children('a').children('span').html('Selecione...');
        $("#paciente").val('');
        $("#paciente").find('option').eq(0).attr('selected',true);
        $("#paciente").parent().children('div').eq(0).children('a').children('span').html('Selecione...');

        if($(this).val() == 'externo' ){
            $("#div-contrato").show();
        }else if($(this).val() == 'interno' ){
            $("#div-paciente").show();
        }

    });


    $("#queixas").change(function () {

        $("#queixa-outros-secao").hide();

        if($(this).val() != null) {
            if ($(this).val().indexOf('18') != -1) {
                $("#queixa-outros-secao").show();
            }
        }
    });

    $("#gerar-protocolo").click(function () {
        if(validar_campos('form-encaminhamento-atendimento')) {
            var data = $("#form-encaminhamento-atendimento").serialize();

            $.ajax({
                url: "/sac/?action=gerar-protocolo-encaminhamento",
                type: 'POST',
                data: data,
                success: function (response) {
                    response = JSON.parse(response);

                    if (response.erro == '') {
                        $("#dados-atendimento-secao").hide();
                        $("#form-encaminhamento-conduta").show();
                        $("select[name='medico']").parent().children('div').eq(0).attr('style','width:100%');
                        $("#protocolo").html('Protocolo deste Atendimento: <br> <b>ECM-' + response.protocolo + '</b>').show();
                        $("#protocolo-id").val(response.protocolo);
                    } else {
                        alert(response.erro);
                    }
                }
            });
        }
    });

    $("#conduta").change(function () {

        $("#remocao-secao").hide().removeClass('OBG');
        $("#conduta-medica-secao").hide().removeClass('OBG');

        if($(this).val() == 1) {
            $("#remocao-secao").hide().removeClass('OBG');
            $("#conduta-medica-secao").show().addClass('OBG');
        }

        if($(this).val() == 2) {
            $("#remocao-secao").show().addClass('OBG');
            $("#conduta-medica-secao").hide().removeClass('OBG');
        }
    });

    $("#salvar-conduta").on('click', function () {
        if(validar_campos('form-encaminhamento-conduta')) {
            var data = $("#form-encaminhamento-conduta").serialize();

            $.ajax({
                url: "/sac/?action=salvar-conduta-encaminhamento",
                type: 'POST',
                data: data,
                success: function (response) {
                    if (response == '1') {
                        alert('Atendimento salvo com sucesso!');
                        window.location.href = "?action=encaminhamento-listar";
                    } else {
                        alert(response);
                    }
                }
            });
        }
    });

    //atualizar-conduta-encaminhamento

    $("#atualizar-conduta").on('click', function () {
        if(validar_campos('form-encaminhamento-conduta')) {
            var data = $("#form-encaminhamento-conduta").serialize();

            $.ajax({
                url: "/sac/?action=atualizar-conduta-encaminhamento",
                type: 'POST',
                data: data,
                success: function (response) {
                    if (response == '1') {
                        alert('Atendimento atualizado com sucesso!');
                        window.location.href = "?action=encaminhamento-listar";
                    } else {
                        alert(response);
                    }
                }
            });
        }
    });

    function validar_campos(div){
        var ok = true;
        $("#"+div+" .OBG").each(function (i){

            if(this.value == ""){
                ok = false;
                this.style.border = "3px solid red";
            } else {
                this.style.border = "";
            }

        });
        $("#"+div+" .COMBO_OBG option:selected").each(function (i){
            if(this.value == "-1"){
                ok = false;
                this.parent().css({"border":"3px solid red"});
            } else {
                this.parent().css({"border":""});
            }
        });

        $("#"+div+" .OBG_CHOSEN option:selected").each(function (i){

            if($(this).val() == "" || $(this).val() == -1){
                ok = false;
                $(this).parent().parent().children('div').css({"border":"3px solid red"});
            } else {

                var estilo = $(this).parent().parent().children('div').attr('style');
                $(this).parent().parent().children('div').removeAttr('style');
                $(this).parent().parent().children('div').attr('style',estilo.replace("border: 3px solid red",''));
            }
        });

        $("#"+div+" .OBG_CHOSEN_MULTIPLE").each(function (i) {
            var hasEmptySelect = $(this)
                .find("option:selected")
                .map(function (idx, elem) {
                    return elem.value;
                })
                .filter(function (value) {
                    return value === 0;
                })
                .length > 0;

            if (!hasEmptySelect) {
                ok = false;
                $(this).parent().children('div').css({"border": "3px solid red"});
            } else {
                var estilo = $(this).parent().children('div').attr('style');
                $(this).parent().children('div').removeAttr('style');
                $(this).parent().children('div').attr('style', estilo.replace("border: 3px solid red", ''));
            }
        });

        if(!ok){
            $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
            $("#"+div+" .div-aviso").show();
        } else {
            $("#"+div+" .aviso").text("");
            $("#"+div+" .div-aviso").hide();
        }
        return ok;
    }
});