<?php

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../db/config.php';

require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';


use \App\Controllers\SAC;

// ini_set('display_errors', 1);

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
    "GET" => [
        '/sac/?action=menu'  => '\App\Controllers\SAC::menu',
        // ENCAMINHAMENTO MÉDICO
        '/sac/?action=encaminhamento-listar'  => '\App\Controllers\SAC::buscarEncaminhamentoMedico',
        '/sac/?action=encaminhamento-novo'  => '\App\Controllers\SAC::encaminhamentoMedicoForm',
        '/sac/?action=visualizar-encaminhamento'  => '\App\Controllers\SAC::visualizarEncaminhamentoMedico',
        '/sac/?action=atualizar-encaminhamento'  => '\App\Controllers\SAC::atualizarEncaminhamentoMedico',
    ],
    "POST" => [
        '/sac/?action=buscar-lista-encaminhamentos'  => '\App\Controllers\SAC::buscarListaEncaminhamentos',
        '/sac/?action=gerar-protocolo-encaminhamento'  => '\App\Controllers\SAC::gerarProtocoloEncaminhamento',
        '/sac/?action=salvar-conduta-encaminhamento'  => '\App\Controllers\SAC::salvarCondutaEncaminhamento',
        '/sac/?action=atualizar-conduta-encaminhamento'  => '\App\Controllers\SAC::atualizarCondutaEncaminhamento',
    ]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
    $app = new App\Application;
    $app->setRoutes($routes);
    $app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
    echo $e->getMessage();
} catch(App\NotFoundException $e) {
    http_response_code(404);
    echo $e->getMessage();
}
