<h3 class="titulo-versao">Versão 2.19.5 (28/07/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1575'>SM-1575</a>] -         Problema no aprazamento de itens na prescrição de curativos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.19.4 (14/08/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1576'>SM-1576</a>] -         Problema no Checklist
    </li>
</ul> 
<hr>
<h3 class="titulo-versao">Versão 2.19.3 (28/07/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1570'>SM-1570</a>] -         Erro ao cancelar uma remoção externa
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.19.2 (27/07/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1565'>SM-1565</a>] -         Modificações pontuais no módulo de Auditoria 
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.19.0 (19/07/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1553'>SM-1553</a>] -         Modificar rotina de lançamento de Custo Fatura/Orçamento.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1558'>SM-1558</a>] -         Modificação na rotina de saída de itens para paciente.
    </li>
</ul>
