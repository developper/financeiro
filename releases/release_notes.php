<style>
    .titulo-versao { font-size: 18px; color: #8B0000; }
</style>

<h1><center>Mudanças do Sistema</center></h1>
<hr/>
<?php
include 'releases/pagination.php';
if(!isset($_GET['page']) || $_GET['page'] !== '2'): ?>
    <?php include 'releases/v2_65.php' ?>
    <hr/>
    <?php include 'releases/v2_63.php' ?>
    <hr/>
    <?php include 'releases/v2_62.php' ?>
    <hr/>
    <?php include 'releases/v2_61.php' ?>
    <hr/>
    <?php include 'releases/v2_60.php' ?>
    <hr/>
    <?php include 'releases/v2_59.php' ?>
    <hr/>
    <?php include 'releases/v2_58.php' ?>
    <hr/>
    <?php include 'releases/v2_57.php' ?>
    <hr/>
    <?php include 'releases/v2_56.php' ?>
    <hr/>
    <?php include 'releases/v2_55.php' ?>
    <hr/>
    <?php include 'releases/v2_54.php' ?>
    <hr/>
    <?php include 'releases/v2_53.php' ?>
    <hr/>
    <?php include 'releases/v2_52.php' ?>
    <hr/>
    <?php include 'releases/v2_51.php' ?>
    <hr/>
    <?php include 'releases/v2_50.php' ?>
    <hr/>
    <?php include 'releases/v2_49.php' ?>
    <hr/>
    <?php include 'releases/v2_48.php' ?>
    <hr/>
    <?php include 'releases/v2_47.php' ?>
    <hr/>
    <?php include 'releases/v2_46.php' ?>
    <hr/>
    <?php include 'releases/v2_45.php' ?>
    <hr/>
    <?php include 'releases/v2_44.php' ?>
    <hr/>
    <?php include 'releases/v2_43.php' ?>
    <hr/>
    <?php include 'releases/v2_42.php' ?>
    <hr/>
    <?php include 'releases/v2_41.php' ?>
    <hr/>
    <?php include 'releases/v2_40.php' ?>
    <hr/>
    <?php include 'releases/v2_39.php' ?>
    <hr/>
    <?php include 'releases/v2_38.php' ?>
    <hr/>
    <?php include 'releases/v2_37.php' ?>
    <hr/>
    <?php include 'releases/v2_36.php' ?>
    <hr/>
    <?php include 'releases/v2_35.php' ?>
    <hr/>
    <?php include 'releases/v2_34.php' ?>
    <hr/>
    <?php include 'releases/v2_33.php' ?>
    <hr/>
    <?php include 'releases/v2_32.php' ?>
    <hr/>
    <?php include 'releases/v2_31.php' ?>
    <hr/>
    <?php include 'releases/v2_30.php' ?>
    <hr/>
    <?php include 'releases/v2_29.php' ?>
    <hr/>
    <?php include 'releases/v2_28.php' ?>
    <hr/>
    <?php include 'releases/v2_27.php' ?>
    <hr/>
    <?php include 'releases/v2_26.php' ?>
    <hr/>
    <?php include 'releases/v2_25.php' ?>
    <hr/>
    <?php include 'releases/v2_24.php' ?>
    <hr/>
    <?php include 'releases/v2_23.php' ?>
    <hr/>
    <?php include 'releases/v2_22.php' ?>
    <hr/>
    <?php include 'releases/v2_21.php' ?>
    <hr/>
    <?php include 'releases/v2_20.php' ?>
    <hr>
    <?php include 'releases/v2_19.php' ?>
    <hr/>
    <?php include 'releases/v2_18.php' ?>
    <hr/>
    <?php include 'releases/v2_17.php' ?>
    <hr/>
    <?php include 'releases/v2_16.php' ?>
    <hr/>
    <?php include 'releases/v2_15.php' ?>
    <hr/>
    <?php include 'releases/v214.php' ?>
    <hr/>
    <?php include 'releases/v213.php' ?>
    <hr/>
    <?php include 'releases/v212.php' ?>
    <hr/>
    <?php include 'releases/v211.php' ?>
    <hr/>
    <?php include 'releases/v210.php' ?>
    <hr/>
    <?php include 'releases/v29.php' ?>
    <hr/>
    <?php include 'releases/v28.php' ?>
    <hr/>
    <?php include 'releases/v27.php' ?>
    <hr/>
    <?php include 'releases/v26.php' ?>
    <hr/>
    <?php include 'releases/v25.php' ?>
    <hr/>
    <?php include 'releases/v24.php' ?>
    <hr/>
    <?php include 'releases/v23.php' ?>
    <hr/>
    <?php include 'releases/v22.php' ?>
    <hr/>
    <?php include 'releases/v21.php' ?>
    <hr/>
    <?php include 'releases/v2.php' ?>
    <hr/>
    <?php include 'releases/v199.php' ?>
    <hr/>
    <?php include 'releases/v198.php' ?>
    <hr/>
    <?php include 'releases/v197.php' ?>
    <hr/>
    <?php include 'releases/v196.php' ?>
    <hr/>
    <?php include 'releases/v195.php' ?>
    <hr/>
    <?php include 'releases/v194.php' ?>
    <hr/>
    <?php include 'releases/v193.php' ?>
    <hr/>
    <?php include 'releases/v192.php' ?>
    <hr/>
    <?php include 'releases/v191.php' ?>
    <hr/>
    <?php include 'releases/v190.php' ?>
    <hr/>
    <?php include 'releases/v189.php' ?>
    <hr/>
    <?php include 'releases/v188.php' ?>
    <hr/>
    <?php include 'releases/v187.php' ?>
    <hr/>
    <?php include 'releases/v186.php' ?>
    <hr/>
    <?php include 'releases/v185.php' ?>

    <?php
endif;
if(isset($_GET['page']) && $_GET['page'] === '2'):
    ?>
    <hr/>
    <?php include 'releases/v184.php' ?>
    <hr/>
    <?php include 'releases/v183.php' ?>
    <hr/>
    <?php include 'releases/v182.php' ?>
    <hr/>
    <?php include 'releases/v181.php' ?>
    <hr/>
    <?php include 'releases/v180.php' ?>
    <hr/>
    <?php include 'releases/v179.php' ?>
    <hr/>
    <?php include 'releases/v178.php' ?>
    <hr/>
    <?php include 'releases/v177.php' ?>
    <hr/>
    <?php include 'releases/v176.php' ?>
    <hr/>
    <?php include 'releases/v175.php' ?>
    <hr/>
    <?php include 'releases/v174.php' ?>
    <hr/>
    <?php include 'releases/v173.php' ?>
    <hr/>
    <?php include 'releases/v172.php' ?>
    <hr/>
    <?php include 'releases/v171.php' ?>
    <hr/>
    <?php include 'releases/v170.php' ?>
    <hr/>
<?php endif; ?>
<br>
<?php include 'releases/pagination.php';