<h3 class="titulo-versao">Versão 2.1.0 (24/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1382'>SM-1382</a>] -         Inserir Mat/Med/Equipamento na tela de finalizar remoção
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1384'>SM-1384</a>] -         Usar para nova da prescrição de enfermagem
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1386'>SM-1386</a>] -         Modificações nas telas de Avaliação e Evolução de enfermagem
  </li>
</ul>