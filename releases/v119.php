<h3 class="titulo-versao">Versão 1.19.9 (05/03/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-721'>SM-721</a>] -         Problema no Relatório Analítico de Fluxo de Caixa (Exportar)
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.19.8 (04/03/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-719'>SM-719</a>] -         Problema no Visualizar Lote no módulo do faturamento.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.19.7 (03/03/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-715'>SM-715</a>] -         Agrupar as saídas no relatório de custos por UR
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.19.6 (03/03/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-717'>SM-717</a>] -         Problema no Relatório Analítico de Fluxo de Caixa (Filtro por Todos)
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.19.5 (26/02/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-713'>SM-713</a>] -         Erro ao gerar Faturas pelo plano Particular.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.19.3 (25/02/2015)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-711'>SM-711</a>] -         Identificar se o Relatório Aditivo de Enfermagem foi confirmado ou não.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.19.2 (24/02/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-709'>SM-709</a>] -         Em buscar solicitações está aparecendo o nome da auditora em vez da enfermaria que solicitou a prescrição.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.19.1 (23/02/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-706'>SM-706</a>] -         Permitir que o usuário ADM &#39;administrador geral&#39; possa  adicionar sugestões na ficha de avaliação e prescrição médica.
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.19.0 (20/02/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-704'>SM-704</a>] -         Exibição de data da evolução médica na listagem do paciente
	</li>
</ul>

<h2>        New Feature
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-685'>SM-685</a>] -         Retirar o calendário de cima dos campos do popup de prescrição
	</li>
</ul>