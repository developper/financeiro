<h3 class="titulo-versao">Versão 1.71.0 (02/09/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1166'>SM-1166</a>] -         Melhorar a busca de fornecedores e clientes
    </li>
</ul>