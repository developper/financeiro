
<h3 class="titulo-versao">Versão 1.22.13 (05/05/2015)</h3>

<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-785'>SM-785</a>] -         Problema ao desativar medicamento no Módulo da Logística.
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.22.12 (05/05/2015)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-784'>SM-784</a>] -         Tornar visível e editável a coluna &quot;Tipo&quot; nos procedimentos ao tentar cadastrar uma GUIA TISS, dessa maneira seria possível ajusta-lo de acordo com o convênio
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.22.11 (30/04/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-782'>SM-782</a>] -         O campo &quot;número da guia na operadora&quot; deve permitir letras e números
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.22.10 (29/04/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-780'>SM-780</a>] -         Erro no Relatório de Prorrogação Médico.
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.22.9 (23/04/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-774'>SM-774</a>] -         Erro ao tentar abrir uma fatura salva.
  </li>
</ul>
<hr>

<h3 class="titulo-versao">Versão 1.22.8 (22/04/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-771'>SM-771</a>] -         Problema ao tentar  Sair  (Log Out) do Sistema.
  </li>

</ul>
<hr>
<h3 class="titulo-versao">Versão 1.22.7 (22/04/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-766'>SM-766</a>] -         Fichas de Prorrogação são editadas sem manter histórico
	</li>

</ul>
<hr>
  <h3 class="titulo-versao">Versão 1.22.7 (22/04/2015)</h3>
  <h2>        Bug
  </h2>
  <ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-766'>SM-766</a>] -         Fichas de Prorrogação são editadas sem manter histórico
    </li>
</ul>
<hr>
    <h3 class="titulo-versao">Versão 1.22.6 (20/04/2015)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-769'>SM-769</a>] -         Problema ao clicar em voltar na prescrição médica.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.22.5 (20/04/2015)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-768'>SM-768</a>] -         Colocar a lista dos usuários em ordem crescente em Importar Assinaturas.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.22.4 (20/04/2015)</h3>

<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-767'>SM-767</a>] -         Centralizar o título do  Relatório de Encaminhamento de Urgência/Emergência nesse mesmo relatório.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.22.3 (17/04/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-765'>SM-765</a>] -         Problema em Prescrições Médicas
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.22.2 (16/04/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-763'>SM-763</a>] -         Problema no envio de mensagens através da API do HipChat
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.22.1 (16/04/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-762'>SM-762</a>] -         Problema em Editar Planos
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.22.0 (15/04/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-748'>SM-748</a>] -         Erros no Relatório de Fluxo de Caixa Realizado
	</li>
</ul>

<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-617'>SM-617</a>] -         Adicionar opção de bloqueio de usuário
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-747'>SM-747</a>] -         Remodelar o listar faturas e orçamentos salvos.
	</li>
</ul>