<h3 class="titulo-versao">Versão 1.87.4 (06/12/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1299'>SM-1299</a>] -         Adicionar a versão 03.03.00 na Guia TISS XML
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.87.3 (06/12/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1298'>SM-1298</a>] -         Erro na Impressão da separação de pedidos das Unidades Regionais
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1296'>SM-1296</a>] -         Modificações na prescrição de curativo
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.87.2 (06/12/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1295'>SM-1295</a>] -         Erro em listar Avaliação Médica.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.87.1 (06/12/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1294'>SM-1294</a>] -         Erro na visualização de itens da prescrição na solicitação de enfermagem.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.87.0 (02/12/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1293'>SM-1293</a>] -         Implementar a versão 03.03.01 do TISS XML
    </li>
</ul>