<h3 class="titulo-versao">Versão 2.52.10 (25/05/2018)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1936'>SM-1936</a>] -         Permitir que um médico use uma avaliação para nova dentro de 24H
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.52.9 (24/05/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1931'>SM-1931</a>] -         Identificar códigos nos campos tabela e tipo na hora de gerar XML
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.52.8 (24/05/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1922'>SM-1922</a>] -         Atualização das alíquotas nos relatórios de faturas fechadas e relatório de margem
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.52.7 (23/05/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1930'>SM-1930</a>] -         Adicionar Tag &lt;ans:numeroGuiaOperadora&gt; em &lt;ans:dadosAutorizacao&gt;
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.52.5 (23/05/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1926'>SM-1926</a>] -         Erro ao adicionar resposta na ocorrência.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.52.4 (23/05/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1928'>SM-1928</a>] -         Ao importar XML o sistema Planserv espera a tag &lt;ans:numeroGuiaOperadora&gt;
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.52.3 (22/05/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1925'>SM-1925</a>] -         Erro ao gerar XML quando modifica tipo de identificação.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.52.2 (22/05/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1917'>SM-1917</a>] -         Mudança em envio de emails para o SAAD
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.52.1 (22/05/2018)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1905'>SM-1905</a>] -         Duplicidade no aprazamento da prescrição de curativo
    </li>
</ul>
<h3 class="titulo-versao">Versão 2.52.0 (21/05/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1911'>SM-1911</a>] -         Permitir editar remoção externa
    </li>
</ul>

<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1906'>SM-1906</a>] -         Corrigir datas e acrescentar comentários no relatório de não conformidade da auditoria
    </li>
</ul>

<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1895'>SM-1895</a>] -         Melhorias do Faturamento
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1904'>SM-1904</a>] -         Inserir o campo profissionais envolvidos no relatório de Inconformidade da Auditoria
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1907'>SM-1907</a>] -         Trazer o aprazamento da prescrição de enfermagem ao usar para nova
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1912'>SM-1912</a>] -         Criar feature de anexar arquivos no módulo de ocorrências
    </li>
</ul>
