<h3 class="titulo-versao">Versão 2.41.7 (23/02/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1808'>SM-1808</a>] -         Erro ao criar Faturar.
    </li>
</ul>

<hr>
<h3 class="titulo-versao">Versão 2.41.6 (23/02/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1807'>SM-1807</a>] -         Executar mesma modificação da ativação na edição de item ativo
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.41.5 (23/02/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1805'>SM-1805</a>] -         Criar campo de categoria de materiais na ativação do catalogo
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.41.4 (22/02/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1803'>SM-1803</a>] -         Em gerar Lote a busca por paciente não está funcionando.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.41.3 (22/02/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1802'>SM-1802</a>] -         Problema no aviso de e-mail de nova Fatura criada para lançamento de valores.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.41.2 (20/02/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1801'>SM-1801</a>] -         Editar fatura não traz tuss e tiss
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.41.1 (20/02/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1798'>SM-1798</a>] -         Criar visualizar Faturas Originais
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.41.0 (19/02/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1780'>SM-1780</a>] -         Melhoria no cadastro de Operadoras
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1784'>SM-1784</a>] -         Criar novos status para o Faturamento.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1786'>SM-1786</a>] -         Criar consenso de faturas
    </li>
</ul>

<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1711'>SM-1711</a>] -         Corrigir o relatório de fluxo de caixa realizado
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1724'>SM-1724</a>] -         Modificar o filtro da tela de fluxo de caixa realizado
    </li>
</ul>
