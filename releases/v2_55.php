<h3 class="titulo-versao">Versão 2.55.2(18/06/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1966'>SM-1966</a>] -         Erro ao visualizar prescrição de médica de avaliação.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.55.2(15/06/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1959'>SM-1959</a>] -         Remoções internas não aparecem no faturamento.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.55.1(15/06/2018)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1927'>SM-1927</a>] -         Atualizar simpro mensagens 119 até 128
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.55.0(11/06/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1958'>SM-1958</a>] -         Permitir escolher empresa ao gerar PDF da Avaliação do Paciente.
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1956'>SM-1956</a>] -         Ajustar regras de avisos do sistema enviados por e-mail.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1957'>SM-1957</a>] -         Ajuste na rotina de Avaliação Médica.
    </li>
</ul>