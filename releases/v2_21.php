<h3 class="titulo-versao">Versão 2.21.4 (31/08/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1595'>SM-1595</a>] -         Ajustes no plano de tratamento de feridas do planserv
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.21.3 (31/08/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1596'>SM-1596</a>] -         Retirar o score da avaliação inicial do planserv
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.21.2 (30/08/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1589'>SM-1589</a>] -         Erro ao gerar fatura.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.21.1 (29/08/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1588'>SM-1588</a>] -         Erro ao editar orçamento inicial.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.21.0 (29/08/2017)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1573'>SM-1573</a>] -         Ficha de Feridas de Enfermagem (Planserv)
    </li>
</ul>

<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1460'>SM-1460</a>] -         Atualizar tabela de fornecedores.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1584'>SM-1584</a>] -         Relatório com a quantidade de diárias faturadas ID/AD em 2016
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1572'>SM-1572</a>] -         Identificar Remoções Externas  nos Relatório de Faturamento Enviado e Margem de contribuição.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1583'>SM-1583</a>] -         Modificação na rotina de editar Fatura.
    </li>
</ul>
