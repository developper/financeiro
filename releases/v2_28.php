<h3 class="titulo-versao">Versão 2.28.0 (16/10/2017)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-1514'>SM-1514</a>] -         Criar rotina de cancelamento de pedido para as URS
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-1567'>SM-1567</a>] -         Modificação na saída de itens para Pedidos Internos.
</li>
</ul>