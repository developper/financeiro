<h3 class="titulo-versao">Versão 2.11.0 (15/05/2017)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-1469'>SM-1469</a>] -         Acrescentar os campos Unidade Regional e Enfermeiro.
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-1482'>SM-1482</a>] -         Alterar o filtro do faturar
</li>
</ul>