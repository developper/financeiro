<h3 class="titulo-versao">Versão 1.70.5 (01/09/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1160'>SM-1160</a>] -         Modificação no e-mail de itens negados pela auditoria.

    </li>
</ul>
<h3 class="titulo-versao">Versão 1.70.4 (01/09/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1162'>SM-1162</a>] -         Correção no relatório de pedidos liberados
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.70.3 (30/08/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1161'>SM-1161</a>] -         Adicionar chosen nos campos de pacientes
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.70.1 (30/08/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1159'>SM-1159</a>] -         Modificar tela de Estoque.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.70.0 (25/08/2016)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1139'>SM-1139</a>] -         Criar o Relatório de não Conformidades no modulo de Auditoria.
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1152'>SM-1152</a>] -         Modificações na tela de saída da logística
    </li>
</ul>