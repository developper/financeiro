<h3 class="titulo-versao">Versão 1.81.3 (20/10/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1246'>SM-1246</a>] -         Problema no valor do  total faturado no Relatório de Faturamento Enviado.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.81.2 (18/10/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1243'>SM-1243</a>] -         Adicionar links para Mapa de Contexto e Macro Processo
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.81.1 (18/10/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1242'>SM-1242</a>] -         Trazer pacientes que tiveram no mínimo uma avaliação no período, no relatório Epidemiológico
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.81.0 (18/10/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1235'>SM-1235</a>] -         Relatório de não Conformidades.
    </li>
</ul>