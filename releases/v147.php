<h3 class="titulo-versao">Versão 1.47.1 (11/03/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1036'>SM-1036</a>] -         Erro no envio de email de itens negados pela auditoria
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.47.0 (04/03/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1031'>SM-1031</a>] -         Modificação no XML das  Faturas
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1032'>SM-1032</a>] -         Criar área para cadastrar o código do prestador na operadora.
    </li>
</ul>