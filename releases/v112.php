<h3 class="titulo-versao">Versão 1.12.14 (19/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-586'>SM-586</a>] -         Problema na impressão da ficha de Avaliação de Enfermagem no Tópico Dispositivos para Eliminação e/ou Excreções
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.12.13 (18/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-585'>SM-585</a>] -         Problema no registro ANS do plano após validar o XML
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.12.12 (18/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-584'>SM-584</a>] -         Problema na impressão da ficha de Avaliação de Enfermagem  no tópico Tipo de Ventilação. 
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.12.11 (18/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-583'>SM-583</a>] -         Erro de validação após enviar o XML da guia resumo de internação
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.12.10 (18/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-582'>SM-582</a>] -         Em Gerar Lote no módulo do Faturamento está trazendo as faturas canceladas para serem inclusas no lote.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.12.9 (18/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-577'>SM-577</a>] -         No buscar solicitação de Enfermagem o imprimir não traz as informações pertinentes a tela
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.12.8 (18/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-575'>SM-575</a>] -         O visualizar faturas na hora de gerar um lote está no modelo antigo.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.12.7 (18/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-576'>SM-576</a>] -         Problema no valor total do procedimento Guia Tiss
</li>
</ul>
<h3 class="titulo-versao">Versão 1.12.6 (17/11/2014)</h3>  
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-573'>SM-573</a>] -         Na ficha de Avaliação de enfermagem consertar o label *&quot;Grau de Humanidade&quot;* para *&quot;Grau de Umidade&quot;*
</li>
</ul>
 <hr>
 <h3 class="titulo-versao">Versão 1.12.5 (17/11/2014)</h3>               
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-574'>SM-574</a>] -         Erro na hora de solicitar equipamentos para uma prescrição de avaliação. 
</li>
</ul>
 <hr>
 <h3 class="titulo-versao">Versão 1.12.4 (17/11/2014)</h3> 
 <h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-571'>SM-571</a>] -         Emails de relatórios médicos sendo enviados para o email do TI
</li>
</ul>
 <hr>
<h3 class="titulo-versao">Versão 1.12.3 (12/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-567'>SM-567</a>] -         Não está salvando o histórico de modificação do status do paciente.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.12.2 (12/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-562'>SM-562</a>] -         Criar uma rota alternativa para a saída de emails
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.12.1 (12/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-563'>SM-563</a>] -         Não está trazendo os itens para a auditora solicitar.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.12.0 (12/11/2014)</h3>
<hr>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-538'>SM-538</a>] -         Acrescentar aos  sub-tópicos de Ausculta Alterada, no tópico Tórax da ficha de evolução de enfermagem a opção &quot;parede anterior&quot;.  
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-541'>SM-541</a>] -         Reorganizar a tela de auditoria de solicitações
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-545'>SM-545</a>] -         Colocar justificativa padrão para os itens pedidos na Solicitação Avulsa de enfermagem.
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-560'>SM-560</a>] -         Colocar para salvar a data, hora e o usuário  que cancelou um item solicitado na logística e na auditoria
</li>
</ul>
        
<h2>        New Feature
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-513'>SM-513</a>] -         Acrescentar no relatório de prorrogação médica a Tabela de Manutenção em Internação Domiciliar NEAD.
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-539'>SM-539</a>] -         Adicionar um relógio com horário de Brasília próximo às informações de usuário
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-552'>SM-552</a>] -         Colocar o Fluxo  das Unidades Regionais  para poderem serem acessados  no Sismederi.
</li>
</ul>