<h3 class="titulo-versao">Versão 2.50.2 (09/05/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1902'>SM-1902</a>] -         Busca de autorizações de serviços por usuário de UR, traz serviços de pacientes de outras URs
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.50.1 (09/05/2018)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1900'>SM-1900</a>] -         Modificar acesso do faturamento para enfermeiras de algumas Unidades
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.50.0 (07/05/2018)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1888'>SM-1888</a>] -         Desvincular prescrição de avaliação médica do orçamento inicial
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1889'>SM-1889</a>] -         Integrar os Scores NEAD com Médico e Enfermeiro
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1793'>SM-1793</a>] -         Atualização do TISSXML para a versões 3.03.02 e 3.03.03
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1859'>SM-1859</a>] -         Melhoria no Módulo de Ocorrência, permitir reabrir ocorrência finalizada.
    </li>
</ul>