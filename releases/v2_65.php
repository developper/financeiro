<h3 class="titulo-versao">Versão 2.65.6 (20/02/2019)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2103'>SM-2103</a>] -         Limitar a pesquisa do ESUS para trazer somente a produção da empresa do usuário
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.65.6 (20/02/2019)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2100'>SM-2100</a>] -         Melhorias na rotina de ficha de atendimento domiciliar do ESUS
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.65.5 (13/02/2019)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2101'>SM-2101</a>] -         Remover palavra Mederi da impressão de avaliação médica
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.65.4 (12/02/2019)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2099'>SM-2099</a>] -         Problema ao conectar-se ao banco postgres do ESUS
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.65.3 (11/02/2019)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2097'>SM-2097</a>] -         Ajustar conexões do ESUS para a Assiste Vida SSA e Assiste Vida Sul BA
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.65.2 (04/02/2019)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2098'>SM-2098</a>] -         Adicionar opção de escolher título da impressão no formulário condensado
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.65.1 (23/01/2019)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2096'>SM-2096</a>] -         Problema no login
    </li>
</ul>>
<hr>
<h3 class="titulo-versao">Versão 2.65.0 (23/01/2019)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2093'>SM-2093</a>] -         Criar ficha de Atendimento Domiciliar Modelo SESAB para importar no E-SUS.
    </li>
</ul>
