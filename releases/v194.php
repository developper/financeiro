<h3 class="titulo-versao">Versão 1.94.2 (03/02/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1345'>SM-1345</a>] -         Imagens das feridas não estão fazendo upload
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.94.1 (03/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1344'>SM-1344</a>] -         Criar o visualizar e cancelar remoções.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.94.0 (02/02/2017)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1327'>SM-1327</a>] -         Remoção de pacientes por convênio.
    </li>
</ul>