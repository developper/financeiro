<h3 class="titulo-versao">Versão 2.59.5 (31/07/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2015'>SM-2015</a>] -         Página sem restrição.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.59.4 (31/07/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2013'>SM-2013</a>] -         Corrigir rodapé da impressão das faturas
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.59.3 (25/07/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2010'>SM-2010</a>] -         Melhorar a escolha da UR ao gerar pdf das faturas e orçamentos.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.59.2 (24/07/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2008'>SM-2008</a>] -         Corrigir insert dos codigos de barras de fornecedores
    </li>
</ul>

<hr>
<h3 class="titulo-versao">Versão 2.59.1 (24/07/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2009'>SM-2009</a>] -         Ajuste no campo Taxa  no modulo do Faturamento.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.59.0 (24/07/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1992'>SM-1992</a>] -         Melhorias da auditoria
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1996'>SM-1996</a>] -         Acrescentar o filtro de &quot;Solicitado Por:&quot; no filtro de ocorrência e Inserir o Setor do usuário que abriu no título das ocorrências
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2003'>SM-2003</a>] -         Modificar forma de gravar cidade para Cliente na hora de Finalizar e Editar Remoção.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2005'>SM-2005</a>] -         Modificar forma de gravar cidade para Cliente na hora de Finalizar Evento e APH
    </li>
</ul>