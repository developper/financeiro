<h3 class="titulo-versao">Versão 2.8.1 (11/04/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1461'>SM-1461</a>] -         Permitir que usuários da logística possam visualizar cadastro de pacientes.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.8.0 (10/04/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1436'>SM-1436</a>] -         Visualizar e remover itens do kit em uma solicitação avulsa
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1439'>SM-1439</a>] -         Criar opção de somente pendentes no filtro de buscar solicitações
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1443'>SM-1443</a>] -         Notificação de pendencia de item aditivo
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1451'>SM-1451</a>] -         Validação entre os campo de data de Emissão vs Data de vencimento da nota
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1459'>SM-1459</a>] -         Criar componente de seleção de pacientes
    </li>
</ul>

