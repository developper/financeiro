<h3 class="titulo-versao">Versão 2.48.6 (25/04/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1874'>SM-1874</a>] -         Usar para novo orçamento inicial não está funcionando corretamente.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.48.3 (19/04/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1874'>SM-1874</a>] -         Usar para novo orçamento inicial não está funcionando corretamente.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.48.2 (19/04/2018)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1887'>SM-1887</a>] -         Atualizar quantidade autorizada em algumas prescrições.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.48.1 (17/04/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1881'>SM-1881</a>] -         Permitir adicionar dietas no Relatório Aditivo de Enfermagem.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.48.0 (16/04/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1880'>SM-1880</a>] -         Melhorias no módulo da logística.
    </li>
</ul>