<h3 class="titulo-versao">Versão 2.31.4 (13/11/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1683'>SM-1683</a>] -         Item repetido no menu da logística.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.31.3 (09/11/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1664'>SM-1664</a>] -         Problema na hora de escolher médico na hora de finalizar Encaminhamento Médico.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.31.2 (07/11/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1679'>SM-1679</a>] -         Colocar Analytics no projeto.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.31.1 (07/11/2017)</h3>
        Bug
</h2>

<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1674'>SM-1674</a>] -         Verificar rotina de consultar exames médicos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.31.0 (06/11/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1678'>SM-1678</a>] -         Problema ao validar CPF na rotina de remoção.
    </li>
</ul>

<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1662'>SM-1662</a>] -         Confirmação do pedido físico da UR para Central
    </li>
</ul>

<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1661'>SM-1661</a>] -         Tela de liberação do pedido físico da UR para central.
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1660'>SM-1660</a>] -         Aceite de solicitação de Devolução Interna
    </li>
</ul>