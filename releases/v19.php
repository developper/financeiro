
<h3 class="titulo-versao">Versão 1.9.0 (16/09/2014)</h3>

<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-462'>SM-462</a>] -         Erro ao dar entrada em uma Nota Fiscal no módulo de Logística
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-465'>SM-465</a>] -         Problema no visualizar relatório aditivo do paciente Ian Souza Novais.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-468'>SM-468</a>] -         Modificar o label &quot;Equipamentos ativos&quot; e modificar a rotina de Buscar Solicitações de Enfermagem
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-469'>SM-469</a>] -         Rever resultado da busca do  Demonstrativo Financeiro no modulo do Financeiro.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-478'>SM-478</a>] -         O sistema deve incluir as faturas com status &quot;faturada&quot; na listagem ao gerar um lote
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-479'>SM-479</a>] -         Quando faz a importação do arquivo XML obtenho a mensagem &quot;Hash não confere&quot;
  </li>
</ul>

<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-427'>SM-427</a>] -         Modificar o label &quot;Descrição da Compra&quot; para &quot;Descrição&quot; das telas de Inserir/Editar Nota no módulo Financeiro
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-428'>SM-428</a>] -         Limpar os campos da Tela de transferência bancária no módulo financeiro
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-435'>SM-435</a>] -         Adequar a função imprimir lista dos pacientes com a lista dos pacientes exibida na tela.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-473'>SM-473</a>] -         Alterar o host das configurações SMTP para smtp.mederi.com.br
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-474'>SM-474</a>] -         Permitir que o medico escolha qual tipo de Score Avulso ele quer preencher para o paciente.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-475'>SM-475</a>] -         Zebrar a grid de solicitações de equipamentos no Modulo de Enfermagem.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-476'>SM-476</a>] -         Realizar algumas melhorias na página de cadastro da Guia SP/SADT
  </li>
</ul>

<h2>        New Feature
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-456'>SM-456</a>] -         Colocar Score NEAD na ficha de Avaliação Medica.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-477'>SM-477</a>] -         Criar cancelar Transferência Bancarias no modulo do financeiro.
  </li>
</ul>

<h2>        Story
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-464'>SM-464</a>] -         Sendo um Faturista, posso imprimir a Guia TISS SP-SADT, pois assim ganho agilidade ao enviar para o plano de saúde
  </li>
</ul>