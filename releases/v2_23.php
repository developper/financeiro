<h3 class="titulo-versao">Versão 2.23.4 (15/09/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1622'>SM-1622</a>] -         Limitar tamanho da assinatura nas prescrições de enfermagem.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.23.3 (14/09/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1619'>SM-1619</a>] -         Faturas trazendo o serviço CPAP.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.23.2 (14/09/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1617'>SM-1617</a>] -         Assinatura do nutricionista não aparece no documento PDF da prescrição.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.23.1 (13/09/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1616'>SM-1616</a>] -         Problema ao atualizar um plano de saúde
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.23.0 (13/09/2017)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1557'>SM-1557</a>] -         Módulo de autorização do CID
    </li>
</ul>
