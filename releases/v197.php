<h3 class="titulo-versao">Versão 1.97.1 (14/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1365'>SM-1365</a>] -         Correções no módulo de exames
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.97.0 (14/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1358'>SM-1358</a>] -         Garantir o envio dos arquivos de exames para o Google Storage
    </li>
</ul>
