<h3 class="titulo-versao">Versão 2.30.6 (06/10/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1672'>SM-1672</a>] -         Erro no relatório de Eventos e APH
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.30.5 (06/10/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1671'>SM-1671</a>] -         Problema na integração contábil com o sistema Domínio.
    </li>
</ul>
<h3 class="titulo-versao">Versão 2.30.4 (01/10/2017)</h3>
<hr>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1669'>SM-1669</a>] -         Orçamento inicial duplicando itens ao clicar em salvar.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.30.3 (30/10/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1668'>SM-1668</a>] -         Link errado ao orçar prescrição aditiva
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.30.2 (30/10/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1667'>SM-1667</a>] -         Erro ao cadastrar Remoção Externa
    </li>
</ul>
<h3 class="titulo-versao">Versão 2.30.1 (30/10/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1666'>SM-1666</a>] -         Ajustes da release  v2.30.0
    </li>
</ul>

<hr>
<h3 class="titulo-versao">Versão 2.30.0 (30/10/2017)</h3>
<h2>        Story
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-377'>SM-377</a>] -         Criar rotina de devolução das Unidades Regionais para Mederi Central.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-382'>SM-382</a>] -         Listar pedidos de devolução das unidades regionais para mederi central
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1651'>SM-1651</a>] -         Criar identificação dos orçamentos aditivos gerados na tela de orçamento Aditivo
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1659'>SM-1659</a>] -         Colocar a cidade do paciente na tela de saída da Logística
    </li>
</ul>