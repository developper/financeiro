<h3 class="titulo-versao">Versão 2.13.6 (08/06/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1536'>SM-1536</a>] -         Verificar duplicidade na saída de materiais
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.13.5 (08/06/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1535'>SM-1535</a>] -         Itens Avulsos não aparecem na fatura
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.13.4 (06/06/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1532'>SM-1532</a>] -         Criar filtro no estoque &quot;Maior que Zero&quot;
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.13.3 (06/06/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1530'>SM-1530</a>] -         Erro ao da entrada de uma nota fiscal pelo xml.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.13.2 (06/06/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1529'>SM-1529</a>] -         Verificando fatura de maio de Francisco Carneiro
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.13.1 (05/06/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1528'>SM-1528</a>] -         Erro ao visualizar a pontuação do scrore Nead 2016
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.13.0 (05/06/2017)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1515'>SM-1515</a>] -         Relatório de aluguel de equipamentos por fornecedor
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1527'>SM-1527</a>] -         Tela de custos de equipamentos por fornecedor
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1520'>SM-1520</a>] -         Eventos e Aph
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1522'>SM-1522</a>] -         Modificação no módulo de Remoção
    </li>
</ul>