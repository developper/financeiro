
<h3 class="titulo-versao">Versão 1.23.2 (11/05/2015)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-788'>SM-788</a>] -         Erro no prazo para editar relatório de prorrogação medica.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.23.1 (07/05/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-786'>SM-786</a>] -         Remover envio de mensagem automática de erro SMTP para o HipChat
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.23.0 (06/05/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-738'>SM-738</a>] -         Adição de regras na geração do arquivo da Domínio Sistemas
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-760'>SM-760</a>] -         Isolar o envio das mensagens do sistema para o HipChat
	</li>
</ul>