<h3 class="titulo-versao">Versão 2.46.6 (06/04/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1866'>SM-1866</a>] -         Erro no faturamento, algumas faturas sendo salvas como Particular sendo de outros planos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.46.5 (05/04/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1863'>SM-1863</a>] -         Adicionar filtro de tipo de parcela no relatório de contas a pagar e receber da Mederi Central
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.46.4 (05/04/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1862'>SM-1862</a>] -         Erro no Listar Faturas quando usa a opção Operadoras.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.46.3 (04/04/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1860'>SM-1860</a>] -         Erro ao gerar um relatório de devoluções para todos os pacientes
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.46.2 (04/04/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1861'>SM-1861</a>] -         Atualizar informações no score nead 2016
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.46.1 (04/04/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1847'>SM-1847</a>] -         Atualizar Relatórios financeiros para pegar a previsão de recebimento.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.46.0 (02/04/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1849'>SM-1849</a>] -         Criar informe de recebimento de faturas.
    </li>
</ul>

<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1760'>SM-1760</a>] -         Criação do módulo de autorização do CID para as UR
    </li>
</ul>