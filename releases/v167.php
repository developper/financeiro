<h3 class="titulo-versao">Versão 1.67.6 (10/08/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1141'>SM-1141</a>] -         Bug ao filtrar pacientes por convênio
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.67.5 (05/08/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1142'>SM-1142</a>] -         Coordenadoras de enfermagem não recebem o e-mail de Parecer Sobre Itens de Entrega Imediata
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.67.4 (01/08/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1140'>SM-1140</a>] -         Erro ao fazer solicitação de compra para Matriz.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.67.3 (01/08/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1138'>SM-1138</a>] -         Melhoria no Relatório de Faturamento Enviado.
    </li>
</ul>

<hr>
<h3 class="titulo-versao">Versão 1.67.2 (28/07/2016)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1137'>SM-1137</a>] -         Erro ao editar uma prescrição de curativo.
    </li>
</ul>

<hr>
<h3 class="titulo-versao">Versão 1.67.1 (27/07/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1135'>SM-1135</a>] -         Relatório de faturas enviadas.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.67.0 (18/07/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1134'>SM-1134</a>] -         Pertir edição no campo data das fotos das  feridas no Relatório Aditivo de Enfermagem.
    </li>
</ul>