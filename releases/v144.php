<h3 class="titulo-versao">Versão 1.44.7 (05/02/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1012'>SM-1012</a>] -         Erro ao salvar uma evolução de enfermagem
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.44.6 (02/02/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1011'>SM-1011</a>] -         Remover espaços em branco da matricula no XML da Guia Tiss
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.44.5 (02/02/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1010'>SM-1010</a>] -         Adicionar link de acesso às prescrições de curativo no módulo de Logística
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.44.3 (02/02/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1007'>SM-1007</a>] -         Adicionar id&#39;s das salas de notificação do Hipchat
    </li>
</ul>
<h3 class="titulo-versao">Versão 1.44.2 (01/02/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1006'>SM-1006</a>] -         Erro na data de Emissão das notas lançadas pela logística.

    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.44.1 (23/01/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1004'>SM-1004</a>] -         Erro em data de admissão na ficha de prorrogação Planserv
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.44.0 (22/01/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1002'>SM-1002</a>] -         Adicionar total geral na entrada de produtos
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1003'>SM-1003</a>] -         Corrigir somatórios no fluxo de caixa projetado diário
    </li>
</ul>