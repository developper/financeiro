<h3 class="titulo-versao">Versão 1.75.2 (21/09/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1197'>SM-1197</a>] -         Erro ao editar um orçamento inicial.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.75.1 (20/09/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1196'>SM-1196</a>] -         Permitir ativar itens no catalogo.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.75.0 (20/09/2016)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1186'>SM-1186</a>] -         Editar entradas da logística
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1164'>SM-1164</a>] -         Criar a possibilidade do médico suspender itens futuros
    </li>
</ul>
