<h3 class="titulo-versao">Versão 2.0.3 (23/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1391'>SM-1391</a>] -         Alterar custo e valor médio de item no Editar Estoque
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.0.2 (22/02/2017)</h3>
<h2>        Task
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1390'>SM-1390</a>] -         Associar os itens da tabela particular ao convênio Unimed Belo Horizonte
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.0.1 (21/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1381'>SM-1381</a>] -         Tornar matrícula como campo obrigatório quando a remoção for por convênio
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.0.0 (21/02/2017)</h3>
<h2>        New Feature
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1368'>SM-1368</a>] -         Adequar a feature de estoque
  </li>
</ul>

<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1377'>SM-1377</a>] -         Adicionar na tela de cadastro os campos de Conselho regional
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1378'>SM-1378</a>] -         Adicionar o número do conselho de enfermagem nas telas de listagem de relatórios
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1380'>SM-1380</a>] -         Retirar itens zerados das telas de auditar solicitações e solicitações pendentes da logística
  </li>
</ul>