<h3 class="titulo-versao">Versão 2.12.6 (25/05/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1523'>SM-1523</a>] -         Erro ao alterar custos de material no estoque
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.12.5 (24/05/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1521'>SM-1521</a>] -         Modificação nas fichas de Avaliação Médica e de Enfermagem.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.12.4 (24/05/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1519'>SM-1519</a>] -         Erro ao salvar solicitação de enfermagem.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.12.3 (24/05/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1517'>SM-1517</a>] -         Erro ao ajustar estoque
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.12.2 (23/05/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1512'>SM-1512</a>] -         Zerar score Nead2016 na ficha de avaliação Médica
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.12.1 (18/05/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1510'>SM-1510</a>] -         Modificação no Relatório Assistencial.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.12.0 (17/05/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1507'>SM-1507</a>] -         Criar opção de cancelar lote.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1508'>SM-1508</a>] -         Adicionar filtro de operadora na tela de relatórios assistenciais
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1509'>SM-1509</a>] -         Colocar botão de reabertura de fatura para o CID
    </li>
</ul>