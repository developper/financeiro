<h3 class="titulo-versao">Versão 1.54.0 (09/05/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1071'>SM-1071</a>] -         Permitir que o usuário informe qual o tipo de medicamento que o plano libera.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1075'>SM-1075</a>] -         Ao cadastrar um novo item, criar o registro na tabela estoque
    </li>
</ul>