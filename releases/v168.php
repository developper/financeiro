<h3 class="titulo-versao">Versão 1.68.1 (18/08/2016)</h3>
<h2>        BUG
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1147'>SM-1147</a>] -         Erro ao gerar prorrogação no modelo Planserv
    </li>
</ul>
<h3 class="titulo-versao">Versão 1.68.0 (11/08/2016)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1143'>SM-1143</a>] -         Adicionar protocolo na mudança de capa de lote
    </li>
</ul>