<h3 class="titulo-versao">Versão 1.45.12 (29/02/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1029'>SM-1029</a>] -         Erro ao editar Usuário
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.45.11 (29/02/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1028'>SM-1028</a>] -         Erro na hora de cadastrar custo de serviços por plano.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.45.10 (26/02/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1026'>SM-1026</a>] -         Erro ao tentar gerar Prescrições de Curativos no formato Planserv para paciente Ramiro.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.45.9 (23/02/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1024'>SM-1024</a>] -         Problema na Prorrogação Planserv.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.45.8 (23/02/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1023'>SM-1023</a>] -         Problema no relatório de Margem de Contribuição.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.45.7 (22/02/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1022'>SM-1022</a>] -         Erro na exibição da data de admissao da prorrogação planserv
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.45.6 (19/02/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1021'>SM-1021</a>] -         Pacientes não são listados quando usuário do SECMED não for UR Central
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.45.5 (18/02/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1020'>SM-1020</a>] -         Colocar o link da evolução de enfermagem na lista de pacientes do módulo médico
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.45.4 (17/02/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1019'>SM-1019</a>] -         Erro no calculo de Outras Despesas na entrada em estoque
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.45.3 (15/02/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1018'>SM-1018</a>] -         Bugs na prorrogação Planserv
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.45.2 (11/02/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1016'>SM-1016</a>] -         Quando o parecer for contrário, o paciente não deverá ter modalidade
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.45.1 (10/02/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1015'>SM-1015</a>] -         Corrigir cálculo de subtotais no relatório de custos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.45.0 (10/02/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1014'>SM-1014</a>] -         Modificar funcionalidades na Prescrição de Curativo de Enfermagem
    </li>
</ul>