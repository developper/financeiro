<h3 class="titulo-versao">Versão 1.37.7 (27/10/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-950'>SM-950</a>] -         Remover acentos dos nomes de pacientes do sistema
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.37.6 (26/10/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-949'>SM-949</a>] -         Adicionar campo Origem no Relatório de Fluxo de Caixa Analítico
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.37.5 (26/10/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-948'>SM-948</a>] -         No imprimir de prescrições Médicas e Nutrição a profissão está aparecendo de acordo com a tela e não com o perfil
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.37.4 (20/10/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-945'>SM-945</a>] -         Mostrar data das feridas nos relatórios de enfermagem
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.37.3 (20/10/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-946'>SM-946</a>] -         Problema para fazer um Orçamento de Avaliação.
	</li>
</ul>
<h3 class="titulo-versao">Versão 1.37.2 (19/10/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-944'>SM-944</a>] -         Aba todos em Solicitações Pendentes da Logística traz todos os pacientes como emergência
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.37.1 (19/10/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-943'>SM-943</a>] -         Problema na Prescrição de Nutrição.
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.37.0 (16/10/2015)</h3>
<h2>        Task
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-939'>SM-939</a>] -         Relatório baseado em nos relatórios deflagrados e prorrogações de  enfermagem e médico, solicitado por Daniele Diniz
	</li>
</ul>

<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-940'>SM-940</a>] -         Melhorar o desempenho da pagina inicial de solicitações pendentes na Logística.
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-941'>SM-941</a>] -         Agrupar por UR, os itens a serem auditados
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-942'>SM-942</a>] -         Modificar rotina de envio de solicitações pendentes para pacientes na Logística.
	</li>
</ul>