<h3 class="titulo-versao">Versão 1.34.10 (30/10/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-926'>SM-926</a>] -         Erro ao excluir ferida e tentar salvar ficha de Avaliação de enfermagem
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.34.8 (29/09/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-924'>SM-924</a>] -         Centro de Custos de Débito e Crédito de tarifas adicionais estão invertidos
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.34.7 (24/09/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-918'>SM-918</a>] -         Verificar duplicidade de lançamentos em arquivo de baixa
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.34.6 (23/09/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-916'>SM-916</a>] -         Adicionar prazo máximo de entrega na prescrição de curativo
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.34.5 (22/09/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-911'>SM-911</a>] -         Trazer prescrições de curativos na auditoria e na Logística
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.34.4 (19/09/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-912'>SM-912</a>] -         Erro na parte das feridas  no Relatório de Prorrogação de Enfermagem
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.34.3 (18/09/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-909'>SM-909</a>] -         Realizar modificações na prescrição de curativo
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.34.2 (16/09/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-910'>SM-910</a>] -         Erro de encode no campo local da ferida nos Relatórios de Prorrogação e Aditivo de Enfermagem.
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.34.1 (16/09/2015)</h3>
<h2>        Bug
	
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-907'>SM-907</a>] -         Inverter o centro de custo das retenções a credito
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.34.0 (16/09/2015)</h3>
<h2>        New Feature
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-904'>SM-904</a>] -         Implementar o SendGrid
	</li>
</ul>