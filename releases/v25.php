<h3 class="titulo-versao">Versão 2.5.6 (27/03/2017)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1430'>SM-1430</a>] -         Cancelamento de Lote.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.5.5 (20/03/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1417'>SM-1417</a>] -         Criar email de fechamento de fatura
<hr>
<h3 class="titulo-versao">Versão 2.5.4 (17/03/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1424'>SM-1424</a>] -         Erro ao tentar imprimir prescrição de enfermagem.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.5.2 (14/03/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1416'>SM-1416</a>] -         Adicionar o email de Tamires no email de reabertura das faturas
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.5.2 (13/03/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1406'>SM-1406</a>] -         Retirar os itens zerados da tela de buscar solicitação da enfermagem
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.5.1 (13/03/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1412'>SM-1412</a>] -         Problema no e-mail de aviso de nova Avaliação Médica.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.5.0 (09/03/2017)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1405'>SM-1405</a>] -         Relatório de saída interna por vencimento, BenMelhor ou Ur
    </li>
</ul>
