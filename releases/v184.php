<h3 class="titulo-versao">Versão 1.84.10 (22/11/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1280'>SM-1280</a>] -         Solicitação de enfermagem e solicitação avulsa gerando duplicidade
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.84.9 (18/11/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1279'>SM-1279</a>] -         Erro na contagem de pacientes sem modalidade
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.84.8 (09/11/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1277'>SM-1277</a>] -         Adicionar filtro por laboratório no relatório de vencidos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.84.7 (08/11/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1276'>SM-1276</a>] -         Erro ao adicionar um material na prescrição aditiva
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.84.6 (07/11/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1275'>SM-1275</a>] -         Problema na impressão de prescrição quando o tipo for Suspensão
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.84.5 (07/11/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1273'>SM-1273</a>] -         Impressão de relatório de procedimentos em GTM
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.84.4 (07/11/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1274'>SM-1274</a>] -         Erro na hora de modificar período de item na prescrição Médica.
    </li>
</ul>
<h3 class="titulo-versao">Versão 1.84.3 (04/11/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1270'>SM-1270</a>] -         Mudança em soros e eletrólitos e fluxo não aparecendo na descrição do item
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.84.2 (04/11/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1272'>SM-1272</a>] -         Mudanças nos procedimentos em gastrostomia
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.84.1 (04/11/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1271'>SM-1271</a>] -         Ajustar a prescrição de Suspensão.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.84.0 (03/11/2016)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1263'>SM-1263</a>] -         Lista de procedimentos em gastrostomia
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1264'>SM-1264</a>] -         Visualização de procedimento em gastrostomia
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1265'>SM-1265</a>] -         Edição de procedimentos em gastrostomia
    </li>
</ul>

<h2>        Story
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1262'>SM-1262</a>] -         Criar relatório de procedimentos em gastrostomia
    </li>
</ul>

<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1260'>SM-1260</a>] -         Adicionar Aba de Procedimentos de Gastrostomia na prescrição Aditiva
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1258'>SM-1258</a>] -         Colocar as prescrições feitas para suspender itens, do tipo &quot;Suspensão&quot;.
    </li>
</ul>