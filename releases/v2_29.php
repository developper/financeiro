<h3 class="titulo-versao">Versão 2.29.2 (26/10/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1663'>SM-1663</a>] -         Erro ao finalizar fatura e orçamento de prorrogação
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.29.1 (24/10/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1654'>SM-1654</a>] -         Erro ao enviar e-mail quando um relatório de não conformidade é reaberto.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.29.0 (24/10/2017)</h3>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-278'>SM-278</a>] -         Criar botão de Voltar para as telas do Módulo Financeiro
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1643'>SM-1643</a>] -         Ordenar lista de equipamentos em solicitação e solicitação avulsa de enfermagem
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1644'>SM-1644</a>] -         Criar feature de visualizar pdf no salvar de pré-fatura
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1646'>SM-1646</a>] -         Modificação no Salvar Fatura/Orçamento
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1648'>SM-1648</a>] -         Criar funcionalidade de ao cancelar lote reabrir todas as faturas
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1649'>SM-1649</a>] -         Trazer os equipamentos ativos na tela de pre-fatura
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1650'>SM-1650</a>] -         Identificar as solicitações avulsas complementares a uma periódica na tela da logística
    </li>
</ul>
     