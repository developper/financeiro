<h3 class="titulo-versao">Versão 1.32.7 (14/09/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-906'>SM-906</a>] -         Problema no Extrato Bancário
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.32.6 (10/09/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-905'>SM-905</a>] -         Verificar problema na composição do arquivo de importação da Dominio Sistemas
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.32.5 (08/09/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-900'>SM-900</a>] -         Problema no envio de emails
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.32.4 (02/09/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-899'>SM-899</a>] -         Erro ao criar uma prescrição de curativo
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.32.3 (01/09/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-896'>SM-896</a>] -         Revisar total das contas sintéticas no relatório de Fluxo de Caixa Realizado
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.32.1 (28/08/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-895'>SM-895</a>] -         Mudar as frequências e locais de feridas da prescrição de curativos
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.32.0 (28/08/2015)</h3>
<h2>        Epic
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-878'>SM-878</a>] -         Permitir que a equipe de enfermagem crie, visualize, edite e imprima uma prescrição de curativo
	</li>
</ul>