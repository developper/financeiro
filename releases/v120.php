<h3 class="titulo-versao">Versão 1.20.5 (23/03/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-732'>SM-732</a>] -         Mudar o select de IPCC do cadastro de Fonecedores/Clientes
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.20.4 (16/03/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-727'>SM-727</a>] -         Alguns serviços prescritos pelos médicos nas Avaliações não estão aparecendo na hora de fazer o orçamento inicial.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.20.3 (13/03/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-726'>SM-726</a>] -         Atualizar consultas do gerenciamento do plano contabil
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.20.2 (12/03/2015)</h3>
<h2>              Bug

</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-725'>SM-725</a>] -         Problema para salvar uma avaliação de enfermagem devido ao uso de ‘  no campo: HISTÓRIA PREGRESSA
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.20.1 (11/03/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-722'>SM-722</a>] -         O valor do "max_input_vars" do php.ini deve ser igual a 3000
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.20.0 (05/03/2015)</h3>
<h2>        Sub-task
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-658'>SM-658</a>] -         Criar tela de associação do Item do plano de contas Financeiro para o Item do plano de contas Contábil
	</li>
</ul>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-687'>SM-687</a>] -         Modificar a ficha de evolução médica na área de &quot;Evolução dos Sinais Vitais&quot;
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-697'>SM-697</a>] -         Problema nas fichas médicas dos pacientes (Alergias [Medicamentosas e Alimentares])
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-718'>SM-718</a>] -         Problema no Relatório de Fluxo de Caixa Realizado
	</li>
</ul>
<h2>        New Feature
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-705'>SM-705</a>] -         Criação de tela para lançamento de custos dos serviços baseando-se em faturas enviadas
	</li>
</ul>
<h2>        Task
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-707'>SM-707</a>] -         Rever a lista de equipamentos ativos  da paciente Joana Batista Paula
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-708'>SM-708</a>] -         Adicionar nova opção em Cuidados Especiais (Vista de Técnico)
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-710'>SM-710</a>] -         Relatório com a data da solicitação e  a data de auditado  dos itens solicitados em 2015 para alguns pacientes.
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-712'>SM-712</a>] -         Investigar porque alguns pacientes com status diferente de ativo ainda estão com equipamentos ativos.
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-720'>SM-720</a>] -         Cadastrar novos operadores e fazer o &#39;De&#39; &#39;Para&#39; com os novos Planos de Saúde.
	</li>
</ul>