<h3 class="titulo-versao">Versão 2.40.19 (16/02/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1797'>SM-1797</a>] -         Inserir os campos endereço e e CPF do paciente no orçamento avulso
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.17 (14/02/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1796'>SM-1796</a>] -         Corrigir filtro no visualizar autorizações
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.17 (14/02/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1794'>SM-1794</a>] -         Possibilitar filtro por classificação e subclassificação no módulo de Ocorrências
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.16 (09/02/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1790'>SM-1790</a>] -         Erro ao mudar status de paciente da UR Sulbahia
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1792'>SM-1792</a>] -         Problema no xml
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.15 (05/02/2018)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1787'>SM-1787</a>] -         Melhoria no relatório de Remoções Realizadas.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.14 (02/02/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1768'>SM-1768</a>] -         Corrigir a listagem de pacientes do CID
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.13 (01/02/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1785'>SM-1785</a>] -         Corrigir div de numero de pacientes encontrados em tela de pacientes de enfermagem
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.12 (31/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1783'>SM-1783</a>] -         Melhoria na rotina da ficha de Admissão do CID
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.11 (31/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1782'>SM-1782</a>] -         Melhorias na parte de Nutrição.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.10 (30/01/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1779'>SM-1779</a>] -         Erro ao negar item na Auditoria.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.9 (30/01/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1778'>SM-1778</a>] -         Erro na busca de usuários.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.8 (29/01/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1777'>SM-1777</a>] -         Erro na tela de lançar Autorizações.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.7 (29/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1733'>SM-1733</a>] -         Criar melhorias de Nutrição
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.6 (29/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1735'>SM-1735</a>] -         Melhorias do CID
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.5 (27/01/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1776'>SM-1776</a>] -         Problema em permissão de usuário para prorrogação de enfermagem
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.4 (26/01/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1774'>SM-1774</a>] -         Erro ao solicitar equipamento.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.3 (26/01/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1773'>SM-1773</a>] -         Erro ao editar usuário.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.2 (26/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1772'>SM-1772</a>] -         Melhoria no relatório de saídas condensadas.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.1 (25/01/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1771'>SM-1771</a>] -         Erro na permissão de prescrição do módulo médico.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.40.0 (25/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1756'>SM-1756</a>] -         Configuração de setores e permissões para Módulo Médico e Enfermagem
    </li>
</ul>