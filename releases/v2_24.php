<h3 class="titulo-versao">Versão 2.24.0 (19/09/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1624'>SM-1624</a>] -         Erro na operador do plano.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.24.0 (18/09/2017)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1612'>SM-1612</a>] -         Criar o módulo de encaminhamento médico
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1603'>SM-1603</a>] -         Melhorar a  rotina de identificação de faturas apenas de remoção.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1606'>SM-1606</a>] -         Aparecer na tela de faturamento as devoluções do período.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1607'>SM-1607</a>] -         Criar funcionalidade de exclusão em lote para itens da fatura
    </li>
</ul>
