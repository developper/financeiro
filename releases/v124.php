<h3 class="titulo-versao">Versão 1.24.3 (13/05/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-790'>SM-790</a>] -         Erro ao salvar uma Avaliação Médica
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.24.2 (12/05/2015)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-791'>SM-791</a>] -         Criar um arquivo de configuração do Supervisor adaptado ao ambiente da Under
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.24.1 (12/05/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-789'>SM-789</a>] -         Remover os arquivos da pasta vendor e executar o composer install durante o processo de deploy
  </li>
</ul>
<h3 class="titulo-versao">Versão 1.24.0 (12/05/2015)</h3>
<h2>        New Feature
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-772'>SM-772</a>] -         Integrar o cadastro de pacientes do SISMederi com o Obsequium
  </li>
</ul>