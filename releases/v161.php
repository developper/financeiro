<h3 class="titulo-versao">Versão 1.61.0 (31/05/2016)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1099'>SM-1099</a>] -         Separação de pedidos por tipo na tela de liberação de pedidos da auditoria
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1101'>SM-1101</a>] -         Análise de itens de entrega imediata no módulo de auditar solicitações
    </li>
</ul>