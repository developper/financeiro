<h3 class="titulo-versao">Versão 1.79.4 (11/10/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1232'>SM-1232</a>] -         Descrição da dieta não é trazida na listagem do Catálogo
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.79.3 (11/10/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1231'>SM-1231</a>] -         Filtrar o usar para nova de prescrições para não trazer itens suspensos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.79.2 (11/10/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1230'>SM-1230</a>] -         Trazer somente os itens ativos ao usar para nova uma prescrição
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.79.1 (10/10/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1229'>SM-1229</a>] -         Armazenar as prováveis variações de apresentação de medicamentos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.79.0 (10/10/2016)</h3>
<h2>        New Feature
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-1216'>SM-1216</a>] -         Relatório de movimentação de antibióticos
</li>
</ul>

<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-1228'>SM-1228</a>] -         Modificar o tipo do campo das datas do relatório de não conformidades, para funcionar no firefox
</li>
</ul>
<hr>