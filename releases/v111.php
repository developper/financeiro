<h3 class="titulo-versao">Versão 1.11.16 (10/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-558'>SM-558</a>] -         As guias SADTs estão sendo duplicadas no arquivo XML
</li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.11.15 (10/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-479'>SM-479</a>] -         Quando faz a importação do arquivo XML obtenho a mensagem &quot;Hash não confere&quot;
  </li>
</ul>
<hr/>

<h3 class="titulo-versao">Versão 1.11.14 (10/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-557'>SM-557</a>] -         Usar para nova do orçamento inicial no modo de faturamento está trazendo os itens cancelados
  </li>
</ul>
<hr/>

<h3 class="titulo-versao">Versão 1.11.13 (05/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-554'>SM-554</a>] -         Ao editar uma fatura ou orçamento, as modificações não estam sendo salvas.
</li>
</ul>
<hr/>

<h3 class="titulo-versao">Versão 1.11.12 (04/11/2014)</h3>                                
<h2>        New Feature
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-548'>SM-548</a>] -         Sendo um faturista, posso selecionar mais de um profissional executante na Guia SP/SADT, pois assim consigo atender as exigências do padrão ANS 
</li>
</ul>
<hr/>

<h3 class="titulo-versao">Versão 1.11.11 (04/11/2014)</h3>                                
<h2>        New Feature
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-548'>SM-548</a>] -         Sendo um faturista, posso selecionar mais de um profissional executante na Guia SP/SADT, pois assim consigo atender as exigências do padrão ANS 
</li>
</ul>
<hr/>

<h3 class="titulo-versao">Versão 1.11.10 (04/11/2014)</h3> 
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-549'>SM-549</a>] -         Modificar a ordem das informações opções e status na grid de buscar prescrições no Módulo médico.
</li>
</ul>
<hr/>

<h3 class="titulo-versao">Versão 1.11.9 (04/11/2014)</h3>                        
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-551'>SM-551</a>] -         Colocar o relatório de fluxo de caixa realizado para trazer os itens conciliados.
</li>
</ul>
<hr/>

<h3 class="titulo-versao">Versão 1.11.8 (03/11/2014)</h3> 
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-550'>SM-550</a>] -         Quando a fatursita edita um orçamento e clica em finalizar ele não está  excluindo o arquivo gerado ao clicar em salvar.
</li>
</ul>


<h3 class="titulo-versao">Versão 1.11.7 (29/10/2014)</h3> 
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-549'>SM-549</a>] -         Modificar a ordem das informações opções e status na grid de buscar prescrições no Módulo médico.
</li>
</ul>
<hr/>
     
<h3 class="titulo-versao">Versão 1.11.6 (29/10/2014)</h3> 
<h2>        New Feature
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-548'>SM-548</a>] -         Sendo um faturista, posso selecionar mais de um profissional executante na Guia SP/SADT, pois assim consigo atender as exigências do padrão ANS 
</li>
</ul>
<hr/>

<h3 class="titulo-versao">Versão 1.11.5 (29/10/2014)</h3> 
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-547'>SM-547</a>] -         Ao tentar fazer uma solicitação avulsa para o paciente Jorge Luiz Dantas do Rei da erro.
    </li>
</ul>
<hr/>

<h3 class="titulo-versao">Versão 1.11.4 (29/10/2014)</h3> 
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-546'>SM-546</a>] -         Verificar porque o botão cancelar prescrição não está aparecendo na parte que listar prescrições.
    </li>
</ul>
<hr/>

<h3 class="titulo-versao">Versão 1.11.3 (29/10/2014)</h3> 
<h2>        Bug
</h2>
<ul>
   <li>[<a href='https://mederi.atlassian.net/browse/SM-544'>SM-544</a>] -         Solicitação de equipamento não grava a observação
   </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.11.2 (29/10/2014)</h3> 
<h2>        Task
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-543'>SM-543</a>] -         Adicionar item no menu de indicadores de qualidade
  </li>
</ul>
<hr/>
 
  <h3 class="titulo-versao">Versão 1.11.1 (29/2014)</h3>              
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-542'>SM-542</a>] -         Problema ao tentar finalizar a edição de uma fatura.
</li>
</ul>
  <hr/>

<h3 class="titulo-versao">Versão 1.11.0 (28/10/2014)</h3>
<h2>        Sub-task
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-507'>SM-507</a>] -         Criar e salvar o formulário de inconformidades e erros de preenchimento no banco
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-508'>SM-508</a>] -         Permitir que o funcionário do SECMED audite e valide as alterações feitas pelo médico
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-509'>SM-509</a>] -         Enviar dados do formulário de inconformidades e erros para o médico
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-510'>SM-510</a>] -         Criar uma tela de visualização das inconformidades e erros de preenchimento para o médico
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-511'>SM-511</a>] -         Mudar a forma como o SECMED visualiza as fichas de avaliação e prescrições na área de liberação
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-514'>SM-514</a>] -         &quot;Entrega Imediata&quot; nas solicitações avulsas de enfermagem
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-524'>SM-524</a>] -         Entrega imediata para solicitação de enfremagem baseada em uma prescrição aditiva.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-527'>SM-527</a>] -         Criar e salvar o formulário de inconformidades e erros de preenchimento no banco
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-528'>SM-528</a>] -         Permitir que o funcionário do SECMED audite e valide as alterações feitas pelo médico
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-529'>SM-529</a>] -         Enviar dados do formulário de inconformidades e erros para o médico
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-530'>SM-530</a>] -         Criar uma tela de visualização das inconformidades e erros de preenchimento para o médico
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-532'>SM-532</a>] -         Criar o formulário com os filtros do relatório
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-533'>SM-533</a>] -         Criar a consulta dos indicadores
  </li>
</ul>

<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-519'>SM-519</a>] -         Segundo Manoela o relatório de rastreamento de controlados não está filtrando por UR.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-520'>SM-520</a>] -         Erro ao salvar Relatório de Prorrogação de Enfermagem
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-531'>SM-531</a>] -         O Check-List gerado na logística na parte de liberar solicitações pendentes do pacientes está trazendo itens duplicados.
  </li>
</ul>

<h2>        Epic
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-503'>SM-503</a>] -         Criar indicadores de inconformidades, erros e tempo de resposta nas Fichas de Avaliação e Prescrições
  </li>
</ul>

<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-523'>SM-523</a>] -         Identificar de que Unidade Regional o Paciente faz parte na tela de liberar solicitações de auditoria.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-535'>SM-535</a>] -         No módulo do faturamento colocar para aparecer o numero TUSS
  </li>
</ul>

<h2>        New Feature
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-494'>SM-494</a>] -         Acrescentando a opção de &quot;Entrega Imediata&quot; nas solicitações avulsas de enfermagem e Solicitações Aditivas
  </li>
</ul>

<h2>        Story
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-487'>SM-487</a>] -         Gerar um arquivo XML a partir de uma Guia de Resumo de Internação
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-504'>SM-504</a>] -         Criar formulário de inconformidades e erros de preenchimento na ficha de avaliação
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-505'>SM-505</a>] -         Criar formulário de inconformidades e erros de preenchimento na prescrição do paciente
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-506'>SM-506</a>] -         Relatório de indicadores do SECMED
  </li>
</ul>

