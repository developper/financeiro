<h3 class="titulo-versao">Versão 2.16.1 (27/06/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1545'>SM-1545</a>] -         Criar a opção para desmarcar o score nead 2016.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.16.0 (26/06/2017)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1537'>SM-1537</a>] -         Integrar as remoções ao módulo de faturamento Interno/Externo
    </li>
</ul>