
<h3 class="titulo-versao">Versão 1.10.0 (08/10/2014)</h3>

<h2>        Sub-task
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-356'>SM-356</a>] -         Exportar para Excel o relatório &quot;Demonstrativo Financeiro&quot; baseado no modelo da issue SM-338
  </li>
</ul>

<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-492'>SM-492</a>] -         Problema ao editar nota no modulo financeiro. Aparece a  mensagem informando que o valor rateio é maior que o valor da  parcela
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-493'>SM-493</a>] -         Retirar o nome &#39;teste&#39; que está aparecendo no formulário de saída dos itens enviados para o paciente.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-496'>SM-496</a>] -         A logística da Mederi Central está visualizando as saídas da UR Salvador na tela &quot;Buscar Saída&quot; no módulo de Logística
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-498'>SM-498</a>] -         Nenhuma fatura encontrada na tela de faturamento
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-512'>SM-512</a>] -         Não permitir que Dr. Nilson edite as prorrogações depois de 24 hrs.
  </li>
</ul>

<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-292'>SM-292</a>] -         Na grid de pacientes do módulo do Secmed trazer a última observação associada ao status do Paciente
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-405'>SM-405</a>] -         Acrescentar o campo de busca por descrição da nota nas telas de &quot;BuscaContas a Pagar/Receber&quot; e &quot;Buscar parcelas a Pagar/Receber&quot;
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-423'>SM-423</a>] -         Acrescentar a informação &#39;DATA&#39; do último processamento na tela de Processar parcela
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-446'>SM-446</a>] -         Editar o label da aba da prescrição de &quot;Dietas Orais/Enterais&quot; para &quot;Dietas Orais/Enterais/Parenteral&quot;
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-480'>SM-480</a>] -         Colocar os campos datas e horas nos recibos para conferencia.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-481'>SM-481</a>] -         Acrescentar na ficha de Avaliação de enfermagem o campo Cateter de Picc
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-482'>SM-482</a>] -         Acrescentar na ficha de Evolução de enfermagem o campo Cateter de Picc
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-486'>SM-486</a>] -         Colocar solicitação avulsa de enfermagem com prazo minimo para 24H
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-488'>SM-488</a>] -         Colocar a opção Relatórios no submenu lateral do módulo Financeiro
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-495'>SM-495</a>] -         Na tela &quot;processar parcelas&quot; a ordem das parcelas devem estar ordenadas pela data de vencimento da parcela
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-497'>SM-497</a>] -         Aumentar o limite de caracteres no cadastro de contas bancarias no módulo Financeiro.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-499'>SM-499</a>] -         Limitar a alguns usuários a opção de editar medicamentos e dietas no catálogo.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-501'>SM-501</a>] -         Não permitir que ao salvar uma prescrição de nutrição ou médica, passe tag html para o banco de dados.
  </li>
</ul>

<h2>        New Feature
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-455'>SM-455</a>] -         Colocar o campo quantidade e lote para aparecer em saída na logística quando a opção for substituir equipamento.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-457'>SM-457</a>] -         Criar padrão de justificativa para a tela de Auditar solicitações no módulo da Auditoria
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-458'>SM-458</a>] -         Criar rotina de justificativa única para o negar todos no auditar solicitações no módulo de Auditoria
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-459'>SM-459</a>] -         Criar uma tela de aviso listando todos os pacientes que foram liberados pela auditoria
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-490'>SM-490</a>] -         Gerar o imprimir e exportar para excel do relatório analítico de fluxo de caixa.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-491'>SM-491</a>] -         Registrar na base de dados a informação de quem auditou uma solicitação de enfermagem
  </li>
</ul>

<h2>        Story
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-483'>SM-483</a>] -         Sendo um faturista, posso imprimir outras despesas da guia SP/SADT, pois assim ganho agilidade ao enviar a cobrança para o plano já que não há necessidade de preencher a guia manualmente
  </li>
</ul>

