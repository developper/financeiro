<h3 class="titulo-versao">Versão 2.36.3 (15/12/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1723'>SM-1723</a>] -         Erro na função Buscar Solicitações.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.36.2 (13/12/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1722'>SM-1722</a>] -         Mudar o acesso da importação de assinaturas
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.36.1 (12/12/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1721'>SM-1721</a>] -         Erro ao dar entrada de uma nota fiscal pela Logística.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.36.0 (11/12/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1684'>SM-1684</a>] -         Melhorias na rotina de devolução interna.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1709'>SM-1709</a>] -         Disparar email para coordenação do Sac após finalizar intercorrências médicas e do Sesc
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1712'>SM-1712</a>] -         Retirar o relatório de demonstrativo financeiro do módulo financeiro
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1713'>SM-1713</a>] -         Acrescentar a opção todos em conta origem e destino de transferência bancária
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1716'>SM-1716</a>] -         Criar campo de motivo de juros a tela de processar parcela
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1717'>SM-1717</a>] -         Refatorar componente add-item
    </li>
</ul>