<h3 class="titulo-versao">Versão 1.46.2 (04/03/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1034'>SM-1034</a>] -         Corrigir email de itens negados para a coordenação de enfermagem da UR FSA
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.46.1 (04/03/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1033'>SM-1033</a>] -         Erro ao baixar XML.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.46.0 (01/03/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1030'>SM-1030</a>] -         Problema na impressão da Ficha de Prorrogação modelo PLANSERV
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1027'>SM-1027</a>] -         Relatório de Alta Médica
    </li>
</ul>