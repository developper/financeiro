<h3 class="titulo-versao">Versão 2.57.8 (10/07/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1989'>SM-1989</a>] -         Problema no relatório de aluguel de equipamentos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.57.7 (10/07/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1990'>SM-1990</a>] -         Problema na funcionalidade  usar Para Nova da Prescrição de Enfermagem
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.57.6 (04/07/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1988'>SM-1988</a>] -         Permitir cancelar Prescrição de Nutrição.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.57.5 (04/07/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1987'>SM-1987</a>] -         Erro ao salvar Prescrição de Enfermagem.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.57.4 (03/07/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1985'>SM-1985</a>] -         Erro na cron de pacientes com 3 ou mais aditivas
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.57.3 (03/07/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1986'>SM-1986</a>] -         Relatório de Devoluções trazendo pacientes de outras Unidades.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.57.2 (29/06/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1984'>SM-1984</a>] -         Erro ao visualizar prescrição em Buscar Solicitações/Prescrições.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.57.1 (26/06/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1982'>SM-1982</a>] -         Adicionar email do TI na cron de aditivas
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.57.0 (26/06/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1974'>SM-1974</a>] -         Melhorias na rotina de Remoções.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1976'>SM-1976</a>] -         Melhoria na rotina de eventos e APH
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1979'>SM-1979</a>] -         Melhoria no visualizar do Relatório de Remoções Realizadas.
    </li>
</ul>

<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1970'>SM-1970</a>] -         Criar cron para alertar sobre pacientes com 3 ou mais aditivas no período
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1980'>SM-1980</a>] -         Retirar o checkbox `todos` do listar pacientes em Administração
    </li>
</ul>