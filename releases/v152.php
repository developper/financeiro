<h3 class="titulo-versao">Versão 1.52.0 (25/04/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1066'>SM-1066</a>] -         Permitir troca de medicamento na saída para paciente.
    </li>
</ul>