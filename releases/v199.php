<h3 class="titulo-versao">Versão 1.99.4 (20/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1379'>SM-1379</a>] -         Modificar permissão para cancelar anexo de Solicitação Exame.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.99.3 (20/02/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1376'>SM-1376</a>] -         Erro na data de entrega de itens feito em  um Relatório Aditivo de Enfermagem.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.99.2 (20/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1375'>SM-1375</a>] -         Mudar a consulta de cuidados especiais
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.99.1 (17/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1374'>SM-1374</a>] -         Visualizar justificativa de enfermagem no buscar solicitação de enfermagem
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.99.0 (16/02/2017)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1371'>SM-1371</a>] -         Relatório de Movimentação de medicamentos por UR
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1372'>SM-1372</a>] -         Adicionar CID  na hora de solicitar exame.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1373'>SM-1373</a>] -         Reduzir a tela de impressão da prescrição de curativo
    </li>
</ul>
