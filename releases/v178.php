<h3 class="titulo-versao">Versão 1.78.9 (07/10/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1221'>SM-1221</a>] -         Reformar o relatório epidemiológico
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.78.2 (05/10/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1218'>SM-1218</a>] -         Valor unitário sem centavos no XML
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.78.1 (05/10/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1217'>SM-1217</a>] -         Buscar o valor unitário baseado no subtotal e quantidade do item na importação do XML
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.78.0 (05/10/2016)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1155'>SM-1155</a>] -         Relatório com os dados do perfil Epidemiológico
    </li>
</ul>
