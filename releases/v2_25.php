<h3 class="titulo-versao">Versão 2.25.2 (26/09/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1634'>SM-1634</a>] -         Problema na listagem de pacientes ao prescrever exames
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.25.1 (26/09/2017)</h3>
<h2>        Bug
</h2>
<ul>
    
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1632'>SM-1632</a>] -         Erro ao cadastrar os custos dos fornecedores da logística
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.25.0 (25/09/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1600'>SM-1600</a>] -         Modificar rotina de identificação do Conselho Regional do usuários.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1626'>SM-1626</a>] -         Melhorias no módulo de autorização
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1627'>SM-1627</a>] -         Permitir que Marcio Andrade tenha acesso a funcionalidade de cadastrar custo de equipamento por fornecedor
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1630'>SM-1630</a>] -         Adicionar filtro por UR no relatório de aluguel de equipamentos
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1631'>SM-1631</a>] -         Acrescentar segunda regra de ordenação nos relatórios assistenciais
    </li>
</ul> 
