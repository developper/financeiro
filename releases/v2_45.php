<h3 class="titulo-versao">Versão 2.45.2 (27/03/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1851'>SM-1851</a>] -         Identificar que uma solicitação foi cancelada parcialmente.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.45.1 (27/03/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1852'>SM-1852</a>] -         Remover abas de medicamentos e dietas da tela de solicitação avulsa
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.45.0 (26/03/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1719'>SM-1719</a>] -         Criar fluxo de contas a receber do financeiro baseado no faturamento.
    </li>
</ul>