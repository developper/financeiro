<h3 class="titulo-versao">Versão 1.76.4 (27/09/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1203'>SM-1203</a>] -         Trazer o CPF no cadastro do paciente
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.76.3 (26/09/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1202'>SM-1202</a>] -         Imprimir separação do paciente de acordo com a prescrição
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.76.2 (23/09/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1200'>SM-1200</a>] -         Colocar laboratório entre parênteses na saída de paciente e na impressão da separação
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.76.1 (23/09/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1199'>SM-1199</a>] -         Ao ativar um item, o mesmo fica duplicado no estoque
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.76.0 (23/09/2016)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1184'>SM-1184</a>] -         Usar para novo relatório de não conformidades.
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1191'>SM-1191</a>] -         Sistema permite alterar na troca de itens, a quantidade autorizada pela Auditoria
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1198'>SM-1198</a>] -         Permitir fragmentação de um item na saída para o paciente
    </li>
</ul>
