<h3 class="titulo-versao">Versão 2.53.6(01/06/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1946'>SM-1946</a>] -         Erro ao adicionar profissional no relatório de Não Conformidade.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.53.5(01/06/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1921'>SM-1921</a>] -         Corrigir o imprimir de listar pacientes na tela de administração
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.53.4 (01/06/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1943'>SM-1943</a>] -         Status faturado não está sendo atualizado no editar de orçamento de prorrogação
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.53.3 (30/05/2018)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1903'>SM-1903</a>] -         Corrigir entrada de notas do xml da logística
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.53.2 (30/05/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1944'>SM-1944</a>] -         Adicionar filtro de convênio para relatório de equipamentos ativos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.53.1 (29/05/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1941'>SM-1941</a>] -         Erro ao salvar prescrição de Curativo de enfermagem
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.53.0 (28/05/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1938'>SM-1938</a>] -         Problema de duplicar aprazamento continua na prescrição de curativo.
    </li>
</ul>

<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1924'>SM-1924</a>] -         Corrigir o Editar remoções
    </li>
</ul>

<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1934'>SM-1934</a>] -         Colocar a opção de enviar email para os prestadores no módulo de ocorrência
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1932'>SM-1932</a>] -         Incluir aba de serviços no relatório aditivo de enfermagem
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1933'>SM-1933</a>] -         Retirar a opção ocorrência do botão Nova e colocar a ação diretamente no botão
    </li>
</ul>
