<h3 class="titulo-versao">Versão 1.73.1 (14/09/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1178'>SM-1178</a>] -         Erro ao editar um orçamento avulso
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.73.0 (13/09/2016)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1170'>SM-1170</a>] -         Habilitar importação o XML na entrada de produtos
    </li>
</ul>