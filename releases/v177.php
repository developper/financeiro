<h3 class="titulo-versao">Versão 1.77.7(04/10/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1214'>SM-1214</a>] -         Problema ao trazer as parcelas do XML
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.77.5(03/10/2016)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1212'>SM-1212</a>] -         Relatório do total em R$ de medicamentos baseados no estoque.
    </li>
</ul>
<h3 class="titulo-versao">Versão 1.77.4 (03/10/2016)</h3>
<h2>       Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1211'>SM-1211</a>] -         Atualizar valor dos custos dos equipamentos.
    </li>
</ul>
<h3 class="titulo-versao">Versão 1.77.3 (30/09/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1210'>SM-1210</a>] -         Corrigir fragmentar da correção de estoque
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.77.2 (30/09/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1209'>SM-1209</a>] -         Colocar uma coluna com estoque na tabela da correção de estoque
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.77.1 (30/09/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1208'>SM-1208</a>] -         Limpar campos no ajuste de estoque
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.77.0 (30/09/2016)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1207'>SM-1207</a>] -         Ferramenta para ajuste de estoque
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1205'>SM-1205</a>] -         Adicionar opção de troca de item na tela de pedido de compra
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1206'>SM-1206</a>] -         Adicionar opção de fragmentar um item na tela de pedido de compra
    </li>
</ul>
