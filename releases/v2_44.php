<h3 class="titulo-versao">Versão 2.44.9 (26/03/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1846'>SM-1846</a>] -         Adicionar item Transferências Bancárias no relatório de fluxo de caixa realizado
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.44.8 (24/03/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1848'>SM-1848</a>] -         Erro em algumas funcionalidades no sistema na hora de salvar.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.44.7 (22/03/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1845'>SM-1845</a>] -         Corrigir extrato bancário
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.44.6 (22/03/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1844'>SM-1844</a>] -         Importar OFX conciliação Bancária.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.44.5 (22/03/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1823'>SM-1823</a>] -         Corrigir relatório de Fluxo de Caixa realizado
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.44.4 (21/03/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1843'>SM-1843</a>] -         Erro ao editar prescrição de Avaliação.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.44.3 (20/03/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1842'>SM-1842</a>] -         Remover abas de medicamentos e dietas da tela de solicitação avulsa
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.44.2 (19/03/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1839'>SM-1839</a>] -         Colocar restrição de genéricos dentro do fluxo de medicamentos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.44.1 (16/03/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1840'>SM-1840</a>] -         Modificação no Modulo do Faturamento.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.44.0 (15/03/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1831'>SM-1831</a>] -         Criar cron para liberar itens baseados nas autorizações.
    </li>
</ul>
