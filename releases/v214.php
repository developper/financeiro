<h3 class="titulo-versao">Versão 2.14.4 (19/06/2017)</h3>
<h2>        Bug 
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1541'>SM-1541</a>] -         Visualizar prorrogação não traz campos novos de justificativa médica
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.14.3 (13/06/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1533'>SM-1533</a>] -         Acrescentar o campo peso na ficha de evolução médica
    </li>
</ul>
<hr>

<h3 class="titulo-versao">Versão 2.14.2 (13/06/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1539'>SM-1539</a>] -         Colocar validação do cpf nas telas de remoções
    </li>
</ul>

<hr>
<h3 class="titulo-versao">Versão 2.14.1 (13/06/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1538'>SM-1538</a>] -         Alterar o label de REGIME DE ATENDIMENTO ATUAL da Prorrogação médica
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.14.0 (12/06/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1525'>SM-1525</a>] -         Alteração na tela de cadastrar Custo Orçamento/Fatura
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1531'>SM-1531</a>] -         Adicionar novos campos de justificativa médica na prorrogação
    </li>
</ul>