<h3 class="titulo-versao">Versão 1.56.0 (17/05/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1071'>SM-1071</a>] -         Permitir que o usuário informe qual o tipo de medicamento que o plano libera.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1082'>SM-1082</a>] -         Modificar cor do item, na busca de medicamento na prescrição médica.
    </li>
</ul>