<h3 class="titulo-versao">Versão 1.26.1 (27/05/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-802'>SM-802</a>] -         Corrigir a página de changelog da versão 1.26.0
</li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.26.0 (26/05/2015)</h3>
<h2>        Sub-task
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-778'>SM-778</a>] -         Adicionar a rotina de LocalStorage na ficha de avaliação
	</li>
</ul>

<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-578'>SM-578</a>] -         Os descontos ou acréscimos dado na fatura dos pacientes não estão sendo levados em consideração no xml gerado.
	</li>
</ul>

<h2>        New Feature
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-770'>SM-770</a>] -         Criar o relatório deflagrado no módulo Médico.
	</li>
</ul>