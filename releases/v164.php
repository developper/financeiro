<h3 class="titulo-versao">Versão 1.64.4 (05/07/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1122'>SM-1122</a>] -         Correção permanente de duplicidade itens em custo fatura
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.64.3 (01/07/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1123'>SM-1123</a>] -         Modalidade não aparece na visualização e nem na impressão da Prorrogação Médica
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.64.0 (29/06/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1118'>SM-1118</a>] -         Adicionar role Fisioterapeuta no cadastro de usuários
    </li>
</ul>