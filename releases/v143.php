<h3 class="titulo-versao">Versão 1.43.4 (19/01/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1001'>SM-1001</a>] -         Ao excluir um item na entrada de produtos, o valor total não atualiza
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.43.3 (19/01/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-999'>SM-999</a>] -         Adicionar permissão para a Logística na tela de cadastro de fornecedores
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1000'>SM-1000</a>] -         Corrigir o valor total na entrada de produtos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.43.2 (18/01/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-994'>SM-994</a>] -         Ratear o valor de &quot;Outras Despesas&quot; no valor unitário dos produtos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.43.1 (18/01/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-998'>SM-998</a>] -         Problema para  finalizar o lançamento do custo de uma fatura.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.43.0 (18/01/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-966'>SM-966</a>] -         Fluxo de Caixa Projetado
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-986'>SM-986</a>] -         Adicionar campo Registro ANS no cadastro de planos de saúde
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-988'>SM-988</a>] -         Aviso de novas notas de entrada para o setor financeiro
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-991'>SM-991</a>] -         Inserir os encargos no alerta final da margem de contribuição
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-992'>SM-992</a>] -         Trazer o campo de senha da tela de auto-login do sismederi preenchido
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-996'>SM-996</a>] -         Trocar o cálculo da tela de lançamento de Entrada em estoque para achar o valor unitário do item
    </li>
</ul>