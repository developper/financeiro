<h3 class="titulo-versao">Versão 1.35.0 (05/10/2015)</h3>
<h2>        Sub-task
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-914'>SM-914</a>] -         Atualizar Orçamento de avaliação.
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-915'>SM-915</a>] -         Atualizar Orçamento Aditivo
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-920'>SM-920</a>] -         Atualizar Faturamento.
	</li>
</ul>

<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-917'>SM-917</a>] -         Prescrição de curativo do tipo Avaliação deverá ir somente para orçamento inicial
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-919'>SM-919</a>] -         Adicionar a opção de &quot;Cancelar&quot; em uma prescrição de curativo
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-928'>SM-928</a>] -         Criar novos campos na ficha de Evolução de Enfermagem.
	</li>
</ul>