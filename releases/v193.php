<h3 class="titulo-versao">Versão 1.93.1 (01/02/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1339'>SM-1339</a>] -         Erro ao listar e excluir exame.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.93.0 (01/02/2017)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1328'>SM-1328</a>] -         Relatório dos pacientes ativos  com feridas da UR Feira.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1332'>SM-1332</a>] -         Relatório de solicitações nos últimos  6 meses para pacientes da UR Feira.
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1335'>SM-1335</a>] -         Modificações no módulo de exames.
    </li>
</ul>
<hr>