<h3 class="titulo-versao">Versão 2.17.4 (06/07/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1550'>SM-1550</a>] -         Modificar relatório de remoções realizadas.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.17.3 (04/07/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1549'>SM-1549</a>] -         Problema ao usar para nova o relatório aditivo de enfermagem
    </li>
</ul>

<hr>
<h3 class="titulo-versao">Versão 2.17.1 (04/07/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1548'>SM-1548</a>] -         Erro na busca do Relatório de Aluguel de Equipamentos Ativos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.17.0 (03/07/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1547'>SM-1547</a>] -         Verificar a a possibilidade de criar uma redundância para os avisos por e-mail.
    </li>
</ul>