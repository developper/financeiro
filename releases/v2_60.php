<h3 class="titulo-versao">Versão 2.60.16 (28/09/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2049'>SM-2049</a>] -         Problema ao selecionar todos para pacote em faturamento
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.60.15 (19/09/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2045'>SM-2045</a>] -         Permitir gerar parcelas automaticamente
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.60.14 (03/09/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2044'>SM-2044</a>] -         Problema em impressão de prorrogação
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.60.12 (03/09/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2034'>SM-2034</a>] -         Erro ao listar pacientes
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.60.11 (03/09/2018)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2032'>SM-2032</a>] -         Verificar nomenclatura de funcionalidades do sistema.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.60.10 (31/08/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2033'>SM-2033</a>] -         Replicar layout da listagem de pacientes do Módulo de Administração para o módulo médico, enfermagem e nutrição
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.60.9 (31/08/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2031'>SM-2031</a>] -         Exportar para excel o resultado do buscar saídas da logística
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.60.8 (30/08/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2030'>SM-2030</a>] -         Erro em orçamento de prorrogação
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.60.7 (24/08/2018)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2027'>SM-2027</a>] -         Relatório de pedido de enfermagem
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.60.6 (24/08/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2028'>SM-2028</a>] -         Problema ao salvar ocorrência
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.60.5 (23/08/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2026'>SM-2026</a>] -         Adicionar botão de desmarcar nas opções de GTM da ficha de avaliação de enfermagem
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.60.4 (15/08/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2020'>SM-2020</a>] -         Adicionar envio de nova avaliação médica e de enfermagem pelo hipchat
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.60.3 (02/08/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2018'>SM-2018</a>] -         Disponibilizar relatório de pacientes a serem faturados na tela do &#39;Faturar&#39;
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.60.2 (02/08/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2017'>SM-2017</a>] -         Corrigir labels do visualizar/editar fornecedores
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.60.1 (01/08/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2016'>SM-2016</a>] -         Permitir devolução de pacientes das URs de SAJ
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.60.0 (31/07/2018)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2012'>SM-2012</a>] -         Verificar regra do liberar automático para o plano CASSI
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1995'>SM-1995</a>] -         Desmembramento das Aditivas médicas em Aditivas de Ajuste
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2014'>SM-2014</a>] -         Identificar que a prescrição aditiva que originou o Orçamento Aditivo é de ajuste.
    </li>
</ul>