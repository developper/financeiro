<h3 class="titulo-versao">Versão 2.18.5 (19/07/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1563'>SM-1563</a>] -         Avaliação de paciente Petrobrás está trazendo o score Nead 2016
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.18.4 (19/07/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1562'>SM-1562</a>] -         Trazer justificativa da ferida no editar Prescrição de Curativo
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.18.3 (18/07/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1560'>SM-1560</a>] -         Input de hora da intercorrência médica não aceita padrão 24h
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.18.2 (18/07/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1556'>SM-1556</a>] -         Verificar existência de CPF no cadastro de clientes e pacientes da remoção extena
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.18.1 (18/07/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1559'>SM-1559</a>] -         Erro no relatório de devolução. 
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.18.0 (13/07/2017)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1552'>SM-1552</a>] -         Relatório de saídas de pacientes por Unidade regional em um período.
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1511'>SM-1511</a>] -         Modificação na rotina de troca da Logística.
    </li>
</ul>
