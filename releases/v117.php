<h3 class="titulo-versao">Versão 1.17.22 (05/02/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-692'>SM-692</a>] -         Problema em ficha de avaliação inicial (Observação de Equipamentos)
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.21 (05/02/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-691'>SM-691</a>] -         Problema na Guia TISS (Planos de Saúde)
  </li>
</ul>
<h3 class="titulo-versao">Versão 1.17.20 (02/02/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-684'>SM-684</a>] -         Problema na validação das datas em Buscar Parcelas a Pagar/Receber
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.19 (02/02/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-683'>SM-683</a>] -         Versão para impressão da tela &quot;Buscar Parcelas&quot; está em branco
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.18 (29/01/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-680'>SM-680</a>] -         Orçamento inicial está trazendo itens de uma prescrição de avaliação não validada. 
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.17 (28/01/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-677'>SM-677</a>] -         Problema no relatório Rastrear Dietas em Pesquisar Movimentação no módulo da Logística
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.16 (28/01/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-677'>SM-677</a>] -         Problema no relatório Rastrear Dietas em Pesquisar Movimentação no módulo da Logística
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.15 (28/01/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-676'>SM-676</a>] -         Problema em Detalhes de uma Solicitação (Itens Enviados)
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.14 (28/01/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-675'>SM-675</a>] -         Problema ao imprimir o relatório na funcionalidade: Buscar Parcelas a Pagar/Receber
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.13 (28/01/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-673'>SM-673</a>] -         Sistema não está mostrando o número da TR após lançar a NF e gerando duplicidade
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.12 (28/01/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-672'>SM-672</a>] -         Problema no relatório de evolução médica (Alergia Medicamentosa)
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.11 (23/01/2015)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-258'>SM-258</a>] -         Modificar a ordenação do resultado da busca de &quot;Visualizar Faturas/Orçamentos&quot;
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.10 (22/01/2015)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-671'>SM-671</a>] -         Manter o Status &quot;Faturado&quot; nos orçamentos e faturas mesmo depois que seja criado o Lote.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.9 (22/01/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-670'>SM-670</a>] -         Relatório Analítico de Fluxo de Caixa traz tarifas/impostos sem solicitá-las
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.7 (19/01/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-665'>SM-665</a>] -         A pesquisa não está sendo disparada na tela de fatura, quando a opção de filtro é por Plano 
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.5 e 1.17.6 (15/01/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-663'>SM-663</a>] -         Rever rotina de Equipamentos na Solicitação de Enfermagem.
</li>
</ul>      
<hr>
<h3 class="titulo-versao">Versão 1.17.4 (15/01/2015)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-662'>SM-662</a>] -         Melhorar a busca do paciente no Orçamento Aditivo.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.3 (15/01/2015)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-651'>SM-651</a>] -         Adicionar a opção de editar um rateio após a conciliação bancária
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.2 (15/01/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-661'>SM-661</a>] -         Verifica rotina de Histórico de Status dos Pacientes.  
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.1 (15/01/2015)</h3>
<h2>        New Feature
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-263'>SM-263</a>] -         Criar campo de observação na solicitação de equipamentos
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.17.0 (14/01/2015)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-569'>SM-569</a>] -         Criar no Modulo de Faturamento o Orçamento Aditivo.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-656'>SM-656</a>] -         Não trazer itens Inativos no Catalogo, já pré-selecionados nas Solicitações de Enfermagem.
  </li>
</ul>