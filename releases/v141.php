<h3 class="titulo-versao">Versão 1.41.1 (28/12/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-982'>SM-982</a>] -         Problema ao processar uma parcela
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.41.0 (24/12/2015)</h3>
<h2>        New Feature
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-965'>SM-965</a>] -         Edição de data de vencimento das parcelas de uma nota
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-976'>SM-976</a>] -         Criar funcionalidade para poder adicionar/editar Princípio Ativo na base de dados.
	</li>
</ul>

<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-978'>SM-978</a>] -         Buscar Relatório de curativos ao marcar lesões na prorrogação Planserv
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-980'>SM-980</a>] -         Permitir associar Princípio Ativo na hora de ativar um medicamento.
	</li>
</ul>
