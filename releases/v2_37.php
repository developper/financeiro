<h3 class="titulo-versao">Versão 2.37.1 (20/12/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1731'>SM-1731</a>] -         Criar cron para excluir os arquivos da pasta financeiros/ofx
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.37.1 (20/12/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1728'>SM-1728</a>] -         Colocar orçamento avulso no módulo do SAC
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.37.0 (19/12/2017)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1726'>SM-1726</a>] -         Atualizar Simpro.
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1714'>SM-1714</a>] -         Conciliação bancária semi automatizada
    </li>
</ul>
