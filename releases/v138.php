<h3 class="titulo-versao">Versão 1.38.11 (09/12/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-970'>SM-970</a>] -         Modificar a solicitação e o imprimir das prescrições de feridas de enfermagem.
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.38.10 (09/12/2015)</h3>
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-971'>SM-971</a>] -         Padronizar a quantidade de caracteres do campo TISS/TUSS no XML somente para Dieta, Medicação e Material.
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.38.8 (25/11/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-960'>SM-960</a>] -         Padronizar quantidade de caracteres de TISS e TUSS no XML
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.38.6 (25/11/2015)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-961'>SM-961</a>] -         Exames físicos deverão ser trazidos da última evolução médica na ficha de prorrogação
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.38.5 (20/11/2015)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-959'>SM-959</a>] -         Centro de custo crédito vazio em arquivo de inclusão
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.38.4 (20/11/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-958'>SM-958</a>] -         Data da solicitação não aparece no &quot;Auditar Solicitações&quot;
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.38.2 (19/11/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-956'>SM-956</a>] -         Divergência no saldo inicial do extrato de contas bancárias e exportação para excel
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.38.1 (17/11/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-955'>SM-955</a>] -         Médico não consegue acessar avaliações de enfermagem
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.38.0 (16/11/2015)</h3>
<h2>        New Feature
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-922'>SM-922</a>] -         Adicionar ficha de impressão no padrão Planserv na prescrição de curativos
	</li>
</ul>

<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-951'>SM-951</a>] -         Inserir tópico Serviço Multidisciplinar hospitalar na Ficha de avaliação de enfermagem.
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-953'>SM-953</a>] -         Adicionar Score NEAD na ficha de avaliação de enfermagem
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-954'>SM-954</a>] -         Acrescentar acesso às avaliações de enfermagem pela listagem de paciente no módulo médico
	</li>
</ul>