<h3 class="titulo-versao">Versão 2.34.3 (29/11/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1707'>SM-1707</a>] -         Colocar as quantidade de diárias de equipamentos de acordo com numero de dias do periodo
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.34.2 (29/11/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1708'>SM-1708</a>] -         Itens repetidos na tela de Cadastro de Custo por UR
    </li>
</ul>

<hr>
<h3 class="titulo-versao">Versão 2.34.1 (28/11/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1704'>SM-1704</a>] -         Arquivos das autorizações não estão sendo excluídos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.34.0(27/11/2017)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1695'>SM-1695</a>] -         Relatório de ajuste de Estoque.
    </li>
</ul>

<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1701'>SM-1701</a>] -         Atualizar brasindice.
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1702'>SM-1702</a>] -         Remover e-mail do aviso de reabertura de conta.
    </li>
</ul>