<h3 class="titulo-versao">Versão 2.54.4(08/06/2018)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1955'>SM-1955</a>] -         Modificar regra de e-mail de aditivo medico para  secretaria da Ur
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.54.3(08/06/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1954'>SM-1954</a>] -         Saldo do fluxo de caixa realizado não bate com o extrato.
    </li>
</ul>

<hr>
<h3 class="titulo-versao">Versão 2.54.2(07/06/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1952'>SM-1952</a>] -         O SAAD solicitou acrescentar no corpo do email de aditivo médico a UR do paciente
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.54.1(07/06/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1951'>SM-1951</a>] -         Opção de escolher empresa ao gerar PDF das prescrições Médicas e Nutrição.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.54.0(05/06/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1939'>SM-1939</a>] -         Trazer itens de serviço do relatório aditivo no orçamento de prorrogação
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1948'>SM-1948</a>] -         Permitir mudar empresa ao gerar PDF nos relatórios de prorrogação e aditivo
    </li>
</ul>

<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1947'>SM-1947</a>] -         Remover e-mails da lista de avisos do sistema.
    </li>
</ul>