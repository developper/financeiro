<h3 class="titulo-versao">Versão 2.9.11 (05/05/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1491'>SM-1491</a>] -         Correção do imprimir Lote
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.9.10 (04/05/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1487'>SM-1487</a>] -         Resolver problema de parecer médico na ficha de avaliação.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.9.9 (03/05/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1484'>SM-1484</a>] -         Modificações no Módulo de Remoções.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.9.8 (28/04/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1483'>SM-1483</a>] -         Adicionar KM percorrido, CEP e com retorno no relatório de remoções
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.9.7 (28/04/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1468'>SM-1468</a>] -         Validação dos dados do paciente
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.9.6 (25/04/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1478'>SM-1478</a>] -         Estão ficando trocado os campos de endereços da funcionalidade remoção.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.9.5 (25/04/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1477'>SM-1477</a>] -         Permitir que as enfermeiras vejam o novo score NEAD da ficha de avaliação médica.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.9.4 (24/04/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1476'>SM-1476</a>] -         Acrescentar um filtro de ativos em catálogo
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.9.3 (24/04/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1474'>SM-1474</a>] -         Corrigir relatório de antibióticos da logística
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.9.2 (20/04/2017)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1473'>SM-1473</a>] -         Cancelar lote 007.170420.132435
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.9.1 (19/04/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1472'>SM-1472</a>] -         Criar relatório de Saídas Condensadas.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.9.0 (17/04/2017)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1465'>SM-1465</a>] -         Melhorar adicionar item no orçamento inicial.
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1462'>SM-1462</a>] -         Retirar os emails de notificação do CID
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1466'>SM-1466</a>] -         Modificar editar Fatura, Orçamento.
    </li>
</ul>

