<h3 class="titulo-versao">Versão 2.33.4(22/11/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1703'>SM-1703</a>] -         Listar soros ativos na seção de soros e eletrólitos da prescrição médica
    </li>
</ul>
<h3 class="titulo-versao">Versão 2.33.3(21/11/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1699'>SM-1699</a>] -         Erro ao salvar fatura.
    </li>
</ul>
<h3 class="titulo-versao">Versão 2.33.2(21/11/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1697'>SM-1697</a>] -         Erro ao salvar cadastro inicial de uma remoção.
    </li>
</ul>
<h3 class="titulo-versao">Versão 2.33.1(20/11/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1698'>SM-1698</a>] -         Modificar rotina de pesquisar Movimentações.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.33.0(20/11/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1652'>SM-1652</a>] -         Limitar a data do Pedido de Compra
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1686'>SM-1686</a>] -         Modificar a rotina de devolução de itens vindo de um paciente.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1690'>SM-1690</a>] -         Modificar rotina Pesquisar Movimentações na Logística.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1691'>SM-1691</a>] -         Melhorar rotina de editar Plano de Saúde.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1693'>SM-1693</a>] -         Notificação por e-mail no módulo de Evento/APH
    </li>
</ul>