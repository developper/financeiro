<h3 class="titulo-versao">Versão 1.30.7 (27/07/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-864'>SM-864</a>] -         Logo da empresa não aparece no relatório
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.30.6 (23/07/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-863'>SM-863</a>] -         Revisar módulo Administrativo
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.30.5 (22/07/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-862'>SM-862</a>] -         Erro ao inserir um novo item no Plano de Conta Contábil
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.30.4 (21/07/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-861'>SM-861</a>] -         Problema ao editar uma ficha de prorrogação médica.
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.30.3 (20/07/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-856'>SM-856</a>] -         A imagem da ferida de um paciente está aparecendo na ficha de evolução de enfermagem de outro paciente
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.30.2 (17/07/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-855'>SM-855</a>] -         Incluir os itens com valor zerado nas Guias TISS
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.30.1 (16/07/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-854'>SM-854</a>] -         Erro ao enviar o XML para a Golden Cross
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.30.0 (16/07/2015)</h3>
<h2>        Sub-task
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-829'>SM-829</a>] -         Adicionar um campo para fazer upload das logos no cadastro de filiais
	</li>
</ul>

<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-425'>SM-425</a>] -         Acrescentar um campo &#39;Encargos Financeiros&#39; na tela de Processar parcela no módulo financeiro.
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-850'>SM-850</a>] -         Incluir &quot;Encargos Financeiros&quot; no arquivo de inclusão da Dominio Sistemas
	</li>
</ul>

<h2>        New Feature
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-843'>SM-843</a>] -         Criar a opção de devolução de equipamentos em um novo menu.
	</li>
</ul>

<h2>        Task
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-837'>SM-837</a>] -         Criar base de dados inicial do SISMederi
	</li>
</ul>