<h3 class="titulo-versao">Versão 1.74.12 (20/09/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1195'>SM-1195</a>] -         Trazer apenas medicamentos que o plano libere.
        >>>>>>> hotfix/SM-1195
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.74.11 (20/09/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1194'>SM-1194</a>] -         Problema ao trazer medicamentos na impressão da saída.
    </li>
</ul>
<hr>

<h3 class="titulo-versao">Versão 1.74.10 (19/09/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1192'>SM-1192</a>] -         Trazer o laboratório na impressão da saída
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.74.9 (16/09/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1190'>SM-1190</a>] -         Erro ao inserir itens em um orçamento avulso
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.74.8 (16/09/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1189'>SM-1189</a>] -         Não permitir adicionar itens extras na entrada quando importar XML
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.74.6 (16/09/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1187'>SM-1187</a>] -         Permitir que apenas quem tenha permissão ao Módulo Administrativo possa editar cadastro de pacientes
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.74.5 (16/09/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1185'>SM-1185</a>] -         Evitar que um item do Simpro e Brasindice possa ser ativado mais de uma vez
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.74.4 (15/09/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1183'>SM-1183</a>] -         Trazer parcelas diretamente do XML na entrada de produtos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.74.3 (15/09/2016)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1181'>SM-1181</a>] -         Permitir reabrir um relatório finalizado.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.74.2 (14/09/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1180'>SM-1180</a>] -         Adicionar laboratório nos buscar itens da entrada de produtos e filtrar por ativos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.74.1 (14/09/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1179'>SM-1179</a>] -         Remover medicamentos não permitidos na lista de troca da logística
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.74.0 (14/09/2016)</h3>
<h2>        Sub-task
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-1174'>SM-1174</a>] -         Modificar obrigatoriedade na justificativa.
</li>
</ul>
