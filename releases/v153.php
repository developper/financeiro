<h3 class="titulo-versao">Versão 1.53.3 (05/05/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1074'>SM-1074</a>] -         Erro ao salvar Avaliação médica.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.53.2 (03/05/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1073'>SM-1073</a>] -         Erro na funcionalidade usar para nova quando a prescrição é do tipo periódica
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.53.1 (03/05/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1072'>SM-1072</a>] -         Linkar os filtros da busca ao &#39;Imprimir Lista&#39; na listagem de pacientes
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.53.0 (28/04/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1069'>SM-1069</a>] -         Problema ao trocar imagem de feridas na evolução de enfermagem
    </li>
</ul>

<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1067'>SM-1067</a>] -         Adicionar botão para abertura de faturas já finalizadas
    </li>
</ul>