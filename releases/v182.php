<h3 class="titulo-versao">Versão 1.82.1 (21/10/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1249'>SM-1249</a>] -         Erro em Solicitações Pendentes na logística.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.82.0 (20/10/2016)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1247'>SM-1247</a>] -         Reformular a área de tutoriais do sistema e adicionar novas opções
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1245'>SM-1245</a>] -         Modificações no cadastro de pacientes
    </li>
</ul>