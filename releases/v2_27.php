<h3 class="titulo-versao">Versão 2.27.0 (09/10/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1025'>SM-1025</a>] -         Mudar semântica da exclusão de transferências bancárias
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1642'>SM-1642</a>] -         Colocar um validador de CPF junto a receita federal nos cadastros do sistema
    </li>
</ul>
