<h3 class="titulo-versao">Versão 1.55.0 (17/05/2016)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1078'>SM-1078</a>] -         Relatório de Evoluções Médicas
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1079'>SM-1079</a>] -         Relatório de Evoluções de Enfermagem
    </li>
</ul>

<h2>        Story
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1076'>SM-1076</a>] -         Relatório de Evoluções
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1077'>SM-1077</a>] -         Colocar a opção de buscar medicamento pelo Princípio Ativo
    </li>
</ul>