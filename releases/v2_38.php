<h3 class="titulo-versao">Versão 2.39.1 (08/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1752'>SM-1752</a>] -         Modificações nas opções de Cuidados de enfermagem
    </li>
</ul>
<h3 class="titulo-versao">Versão 2.38.10 (08/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1748'>SM-1748</a>] -         Melhorias na Intercorrências Médicas
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.38.9 (08/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1747'>SM-1747</a>] -         Modificar rotina de e-mail de Negativa da Auditoria.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.38.8 (08/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1746'>SM-1746</a>] -         Melhoria na ficha de Adimissão criada pelo CID
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.38.6 (04/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1744'>SM-1744</a>] -         Mudar campo de observação/justificativa da prescrição para texto
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.38.5 (04/01/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1743'>SM-1743</a>] -         Problema no pdf da ficha de Admissão do paciente.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.38.4 (04/01/2018)</h3>

<h2>        Improvement
</h2>
    <ul>
        <li>[<a href='https://mederi.atlassian.net/browse/SM-1734'>SM-1734</a>] -         Melhorias do módulo médico

        </li>
    </ul>
<hr>
<h3 class="titulo-versao">Versão 2.38.3 (04/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1742'>SM-1742</a>] -         Correções no salvar do cadastro de ocorrências

    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.38.2 (03/01/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1741'>SM-1741</a>] -         Correções no módulo de ocorrências
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.38.0 (03/01/2018)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1730'>SM-1730</a>] -         Criar tela de cadastro de  ocorrências.
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1732'>SM-1732</a>] -         Melhorias de enfermagem
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1737'>SM-1737</a>] -         Melhorias no módulo de ocorrência
    </li>
</ul>