<h3 class="titulo-versao">Versão 1.66.0 (15/07/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1132'>SM-1132</a>] -         Permitir que usuários de outras Unidades Regionais possam acessar a funcionalidade Custo Orçamento.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1133'>SM-1133</a>] -         Modificar mensagem enviada por e-mail quando uma fatura é reaberta.
    </li>
</ul>