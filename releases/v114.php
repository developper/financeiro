<h3 class="titulo-versao">Versão 1.14.8 (09/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-630'>SM-630</a>] -         Editar o item do catálogo permite ativar/inativar
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.14.7 (09/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-628'>SM-628</a>] -         Problema ao alterar valor de parcela
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.14.6 (08/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-625'>SM-625</a>] -         O campo referência não é preenchi do automaticamente quando o usuário cadastra um novo medicamento no catalogo módulo de logística
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.14.5 (08/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-623'>SM-623</a>] -         Error ao tentar cadastrar um material sem o cod. TUSS
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.14.4 (08/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-622'>SM-622</a>] -         Problema ao alterar uma parcela
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.14.3 (02/12/2014)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-614'>SM-614</a>] -         Adicionar a opção de edição e exclusão de tarifas e impostos bancários
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.14.2 (01/12/2014)</h3>
<h2>        Bug
</h2>
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-616'>SM-616</a>] -         Pacientes auditados por completo, não saem da lista de pacientes a serem auditados
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.14.1 (01/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-615'>SM-615</a>] -         Código TUSS não está sendo salvo no cadastro de itens no catálogo
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.14.0 (01/12/2014)</h3>             
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-609'>SM-609</a>] -         Guia SP/SADT não está abrindo devido a problema na validade da senha
</li>
</ul>
        
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-580'>SM-580</a>] -         As pendências da auditoria deverão ser sinalizadas por email para as coordenações locais
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-581'>SM-581</a>] -         Acrescentar as justificativas das pendências e saída da logística na tela de buscar solicitações de enfermagem
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-597'>SM-597</a>] -         Permitir que as Unidades Regionais, tenham acesso aos Relatórios de Devolução e Equipamentos/Ativos
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-600'>SM-600</a>] -         Modificar a forma de  cadastrar um novo item no Catálogo.
</li>
</ul>
        
<h2>        New Feature
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-431'>SM-431</a>] -         Criar tela de lançamento de tarifas bancárias e IOF dentro da tela de extrato no módulo financeiro
</li>
</ul>
        
<h2>        Task
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-555'>SM-555</a>] -         Criar a base de itens exportados do SIMPRO.
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-592'>SM-592</a>] -         Modificar e adicionar os status de feridas
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-596'>SM-596</a>] -         Modificar uma das justificativas utilizadas pelas auditoras para negar um item solicitado.
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-599'>SM-599</a>] -         Importar as Dietas exportadas do sistema do Brasindice.
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-603'>SM-603</a>] -         Gerar um relatório do mês de outubro e novembro constando todas as justificativas usadas pelas enfermeiras no momento de fazer pedido avulso
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-612'>SM-612</a>] -         Relatório de solicitações avulsas do mês de novembro para os pacientes da Unidade de Feira de Santana
</li>
</ul>
