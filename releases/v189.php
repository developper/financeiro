<h3 class="titulo-versao">Versão 1.89.7 (22/12/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1311'>SM-1311</a>] -         Adicionar unidade de medida na tela de lançamento de custo
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.89.6 (21/12/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1310'>SM-1310</a>] -         Impressão da ficha de evolução de enfermagem ilegível
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.89.5 (21/12/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1309'>SM-1309</a>] -         Problema na impressão do checklist
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.89.3 (15/12/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1306'>SM-1306</a>] -         Problema ao usar relatório de não conformidades como novo
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.89.2 (13/12/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1305'>SM-1305</a>] -         Trazer equipamentos ativos do paciente no orçamento de prorrogação
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.89.1 (13/12/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1304'>SM-1304</a>] -         Pegar o id da solicitação que substitui uma troca
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.89.0 (13/12/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1301'>SM-1301</a>] -         Criar campo para identificação do fornecedor do equipamento na saída para pacientes.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1303'>SM-1303</a>] -         Troca de mat/med/die no buscar solicitação de enfermagem
    </li>
</ul>