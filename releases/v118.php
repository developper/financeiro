<h3 class="titulo-versao">Versão 1.18.7 (18/02/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-703'>SM-703</a>] -         As porcentagens de descontos ou acréscimos de uma fatura não estão sendo levadas em considerações na hora de imprimir lote.
</li>
</ul>
<hr>
</ul>
<h3 class="titulo-versao">Versão 1.18.6 (12/02/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-702'>SM-702</a>] -         Problema no Relatório de Fluxo de Caixa Analítico
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.18.5 (12/02/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-701'>SM-701</a>] -         Problema no Gerenciamento de IPCF
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.18.4 (12/02/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-700'>SM-700</a>] -         Erro na pesquisa que retorna os itens enviados para uma Unidade Regional no Relatório de Custos Por UR
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.18.3 (11/02/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-699'>SM-699</a>] -         Erro no valor a ser cobrado para as Unidades Regionais  no Relatório de Custos Por UR
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.18.2 (09/02/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-693'>SM-693</a>] -         Problema na ficha de avaliação (Alergia Alimentar e Medicamentosa)
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.18.1 (09/02/2015)</h3>
<h2>        Task
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-696'>SM-696</a>] -         Modificar as versões do JS nos index de enfermagem, financeiro,médico,faturamento.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.18.0 (09/02/2015)</h3>
<h2>        Sub-task
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-657'>SM-657</a>] -         Criar campo de classificação do plano de contas Contábil no módulo financeiro
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-679'>SM-679</a>] -         Modificar as justificativas para Solicitações  Avulsas de Enfermagem
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-689'>SM-689</a>] -         Script para importar o plano de contas contábil
  </li>
</ul>
<h2>        Epic
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-667'>SM-667</a>] -         Elaboração do novo Relatório Aditivo de Enfermagem
  </li>
</ul>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-426'>SM-426</a>] -         Permitir que o usuário possa alterar o número da nota mesmo existindo parcelas já processadas para a mesma
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-681'>SM-681</a>] -         Permitir realizar Orçamento Aditivo com base no Relatório Aditivo de Enfermagem.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-690'>SM-690</a>] -         Alterar o botão incluir da janela materiais (em prescrições) conforme outros tipos
  </li>
</ul>
<h2>        New Feature
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-429'>SM-429</a>] -         Criar uma tela de gerenciamento dos IPCF no módulo financeiro
  </li>
</ul>
<h2>        Task
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-666'>SM-666</a>] -         Fazer manutenção dos equipamentos ativos de alguns pacientes.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-669'>SM-669</a>] -         Gerar um relatório com os Relatórios Aditivo/Prorrogação Médico e de Enfermagem do dia 20/01/2015 a 21/01/2015
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-674'>SM-674</a>] -         Modificar o aprazamento de um item  na prescrição de Nutrição de fevereiro/2015 de  Carlos Viana Filho.
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-678'>SM-678</a>] -         Relatório para Logística (Total de Itens solicitados e emergência)
  </li>
</ul>