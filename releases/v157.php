<h3 class="titulo-versao">Versão 1.57.3 (20/05/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1091'>SM-1091</a>] -         Problema no filtro por auditor no relatório de faturas liberadas
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.57.2 (19/05/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1088'>SM-1088</a>] -         Erro no relatório de pedidos liberados
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.57.1 (19/05/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1087'>SM-1087</a>] -         Erro no lançamento das parcelas na entrada de notas
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.57.0 (19/05/2016)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1085'>SM-1085</a>] -         Relatório de Faturas Liberadas
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1086'>SM-1086</a>] -         Relatório de Pedidos Liberados
    </li>
</ul>

<h2>        Story
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1084'>SM-1084</a>] -         Relatório para Coordenação da Auditoria
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1083'>SM-1083</a>] -         Permitir identificar se um medicamento é genérico ou não.
    </li>
</ul>