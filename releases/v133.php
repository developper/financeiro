<h3 class="titulo-versao">Versão 1.33.0 (14/09/2015)</h3>
<h2>        Sub-task
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-902'>SM-902</a>] -         Criar botão finalizar na tela de lançamento de custo orçamentos/fatura.
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-903'>SM-903</a>] -         Criar buscar e exportar  Relatório de Margem de Contribuição.
	</li>
</ul>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-897'>SM-897</a>] -         Colocar a opção salvar no Orçamento de Prorrogação no módulo do Faturamento.
	</li>
</ul>