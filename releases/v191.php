<h3 class="titulo-versao">Versão 1.91.2 (02/01/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1317'>SM-1317</a>] -         Modificar busca de equipamentos nas faturas.
    </li>
</ul>

<hr>
<h3 class="titulo-versao">Versão 1.91.1 (29/12/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1316'>SM-1316</a>] -         Problema ao enviar emails de negativas da auditoria
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.91.0 (27/12/2016)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1312'>SM-1312</a>] -         Listar e Visualizar últimas simulações de um orçamento
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1313'>SM-1313</a>] -         Gerar PDF de uma simulação
    </li>
</ul>