<h3 class="titulo-versao">Versão 1.69.1 (24/08/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1149'>SM-1149</a>] -         Colocar o código do paciente em todas as buscas de pacientes do sistema
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.69.0 (23/08/2016)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1148'>SM-1148</a>] -         Relatório de vencidos
    </li>
</ul>