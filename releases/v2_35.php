<h3 class="titulo-versao">Versão 2.35.1 (05/12/2017)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1710'>SM-1710</a>] -         Modificar matrícula cadastrada em uma remoção.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.35.0 (04/12/2017)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-1685'>SM-1685</a>] -         Modificar a rotina de cancelar itens na hora de dar saída para paciente.
</li>
</ul>
