<h3 class="titulo-versao">Versão 1.21.12 (09/04/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-748'>SM-748</a>] -         Erros no Relatório de Fluxo de Caixa Realizado
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.21.11 (09/04/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-757'>SM-757</a>] -         O campo de justificativa do primeiro item da tela de auditar pendência sempre aparece como obrigatório
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.21.10 (08/04/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-754'>SM-754</a>] -         Erro ao tentar enviar arquivo XML para a Unimed
	</li>
</ul>

<h3 class="titulo-versao">Versão 1.21.9 (31/03/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-745'>SM-745</a>] -         Erro no cálculo do total por conta (Relatório de Fluxo de Caixa Realizado)
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.21.8 (31/03/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-744'>SM-744</a>] -         Disponibilizar permantentemente a alteração de relatório de prorrogação para coordenação e diretoria médica
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.21.7 (31/03/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-740'>SM-740</a>] -         Disponibilizar campo de Codigo Dominio nos IPCC
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.21.6 (31/03/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-743'>SM-743</a>] -         Validação de datas na hora de fazer uma prescrição.
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.21.5 (28/03/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-742'>SM-742</a>] -         Bug na edição de Fonecedores (CPF Obrigatório)
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.21.4 (28/03/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-741'>SM-741</a>] -         Remover verificação de CPF/CNPJ na edição de Fornecedores
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.21.3 (27/03/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-739'>SM-739</a>] -         Erro ao tentar salvar a liberação de itens na auditoria.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.21.1 (26/03/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-737'>SM-737</a>] -         Erro em Gerenciar Lotes (Busca por Paciente)
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.21.0 (24/03/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-728'>SM-728</a>] -         Criar rotina de marcar todos para Pendência na tela da Auditoria
	</li>
</ul>

<h2>        New Feature
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-500'>SM-500</a>] -         Criar arquivo de exportação dos dados financeiros para o Sistema &quot;Domínio&quot; (Software utilizado por Jean)
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-536'>SM-536</a>] -         Separação de informações do contas a pagar/receber por UR
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-688'>SM-688</a>] -         Concluir a construção da ficha de &quot;Encaminhamento de Urgência e Emergência&quot;
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-730'>SM-730</a>] -         Mudança de Modalidade de Home Care (Pacientes)
	</li>
</ul>