<h3 class="titulo-versao">Versão 1.72.5 (12/09/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1177'>SM-1177</a>] -         Adicionar edição das datas de autorização e admissão do paciente
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.72.4 (09/09/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1176'>SM-1176</a>] -         Erro na funcionalidade custo orçamento/fatura
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.72.3 (09/09/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1167'>SM-1167</a>] -         Relatório de margem de contribuição.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.72.2 (09/09/2016)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1175'>SM-1175</a>] -         Relatório de não Conformidades.
    </li>
</ul>
<h3 class="titulo-versao">Versão 1.72.1 (09/09/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1173'>SM-1173</a>] -         Mudar caminho do Cadastrar Custo Plano para o Financeiro e colocar o chosen nos campos selects
    </li>
</ul>
<h3 class="titulo-versao">Versão 1.72.0 (09/09/2016)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1154'>SM-1154</a>] -         Permitir adicionar o imposto ISS ao documento faturado (Faturas e Orçamentos).
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1168'>SM-1168</a>] -         Relatório de não Conformidades.
    </li>
</ul>