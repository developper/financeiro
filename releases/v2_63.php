<h3 class="titulo-versao">Versão 2.63.13 (07/01/2019)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2094'>SM-2094</a>] -         Mudar logo no checklist da Logística.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.63.12 (17/12/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2092'>SM-2092</a>] -         Problema ao trazer imagens de feridas no plano de tratamento de feridas do PLANSERV
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.63.11 (14/12/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2091'>SM-2091</a>] -         Problema ao trazer informações do paciente no formulário condensado
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.63.10 (11/12/2018)</h3>
<h2>        Task
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2090'>SM-2090</a>] -         Modificar e-mail em funcionalidades da logística.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.63.9 (29/11/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2089'>SM-2089</a>] -         Adicionar nome do laboratório de antibiótico na impressão de fatura do SESAB
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.63.8 (28/11/2018)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2087'>SM-2087</a>] -         Permitir que médicos e secretárias tenham acesso ao relatório de não conformidades.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.63.7 (20/11/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2086'>SM-2086</a>] -         Problema ao visualizar formulário condensado
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.63.6 (20/11/2018)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2085'>SM-2085</a>] -         Erro na conciliação bancária quando o usuário não da central.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.63.5 (19/11/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2082'>SM-2082</a>] -         Problema em impressão de formulário condensado
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.63.4 (19/11/2018)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2084'>SM-2084</a>] -         Erro ao cancelar lote.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.63.3 (16/11/2018)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2081'>SM-2081</a>] -         Erro ao salvar evolução de nutrição em formulário condensado
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.63.2 (13/11/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2080'>SM-2080</a>] -         Remover simbolo de moeda das planilhas do módulo financeiro
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.63.1 (13/11/2018)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2079'>SM-2079</a>] -         Erro ao dar entrada em NF  pela Logística.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.63.0 (12/11/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2068'>SM-2068</a>] -         Controle de Faturamento
    </li>
</ul>

<h2>        Story
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2052'>SM-2052</a>] -         Melhorias no financeiro.
    </li>
</ul>
