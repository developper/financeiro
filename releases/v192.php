<h3 class="titulo-versao">Versão 1.92.11 (27/01/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1337'>SM-1337</a>] -         Impressão de Prescrição de Curativo.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.92.10 (26/01/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1336'>SM-1336</a>] -         Erro na funcionalidade Buscar Solicitações
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.92.7 (17/01/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1331'>SM-1331</a>] -         Modificação no módulo de exames.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.92.6 (17/01/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1330'>SM-1330</a>] -         Modificações no Módulo de exames.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.92.5 (16/01/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1329'>SM-1329</a>] -         Modificar Label de &quot;N° Nota&quot; para &quot;N° TR&quot; em Pesquisar Movimentações.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.92.4 (11/01/2017)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1325'>SM-1325</a>] -         Correção de faturas.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.92.1 (10/01/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1322'>SM-1322</a>] -         Erro nos itens do pedido condensado das Unidades Regionais.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.92.0 (10/01/2017)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1314'>SM-1314</a>] -         Usar um orçamento inicial para novo
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1315'>SM-1315</a>] -         Módulo de exames
    </li>
</ul>
<hr>
