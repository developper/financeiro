<h3 class="titulo-versao">Versão 2.49.2 (04/04/2018)</h3><h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1899'>SM-1899</a>] -         Modificar email de aviso para os orçamentos iniciais
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.49.1 (03/04/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1898'>SM-1898</a>] -         Problema ao salvar uma saída para paciente
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.49.0 (30/04/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1891'>SM-1891</a>] -         Adicionar plano e UR nos relatórios assistenciais do módulo médico
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1892'>SM-1892</a>] -         Adicionar plano e UR nos relatórios assistenciais do módulo de enfermagem
    </li>
</ul>

<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1884'>SM-1884</a>] -         Relatório de remessa da logística
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1885'>SM-1885</a>] -         Criar remessa das saídas do sistema
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1421'>SM-1421</a>] -         Gravar UR e Plano do paciente no momento que é feito um documento assistencial.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1767'>SM-1767</a>] -         Trocar o componente do CID10 de chosen para typeahead
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1893'>SM-1893</a>] -         Modificar lista de paciente da funcionalidade do módulo de Exames.
    </li>
</ul>