<h3 class="titulo-versao">Versão 2.62.8 (12/11/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2078'>SM-2078</a>] -         Problema ao salvar formulário condensado
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.62.7 (12/11/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2076'>SM-2076</a>] -         Melhorias no formulário condensado
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.62.6 (08/11/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2075'>SM-2075</a>] -         Erro ao salvar edição de relato de não conformidade
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.62.5 (06/11/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2074'>SM-2074</a>] -         Problema na select de pacientes do formulário condensado
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.62.4 (29/10/2018)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2073'>SM-2073</a>] -         Erro ao cadastrar tarifas/ Imposto para um conta bancária.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.62.3 (26/10/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2071'>SM-2071</a>] -         Adicionar assinaturas da equipe multi na impressão do formulário condensado
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.62.2 (26/10/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2070'>SM-2070</a>] -         Problema ao cadastrar evolução de paciente na área do prestador
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.62.1 (24/10/2018)</h3>
<h2> Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2069'>SM-2069</a>] - Mensagem de erro de e-mail ao ativar
        paciente da UR SAJ
    </li>
</ul>
<h3 class="titulo-versao">Versão 2.62.0 (24/10/2018)</h3>
<h2> Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2055'>SM-2055</a>] - Melhorias em transferências bancarias.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2057'>SM-2057</a>] - Melhoria na rotina de Bancos
    </li>
</ul>

<h2> Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2067'>SM-2067</a>] - Melhoria na edição de formulários
        condensados
    </li>
</ul>
