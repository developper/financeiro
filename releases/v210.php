<h3 class="titulo-versao">Versão 2.10.7 (12/05/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1505'>SM-1505</a>] -         Erro ao tentar fazer a troca de um material.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.10.6 (12/05/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1503'>SM-1503</a>] -         Criar uma busca de estoque por princípio ativo
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.10.5 (11/05/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1493'>SM-1493</a>] -         Modificar relatório aditivo de enfermagem
    </li>
</ul>
<h3 class="titulo-versao">Versão 2.10.4 (11/05/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1501'>SM-1501</a>] -         Adicionar o um campo para o condutor assinar na via de impressão da logística
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.10.3 (11/05/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1500'>SM-1500</a>] -         Erro ao visualizar detalhes do Paciente.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.10.2 (11/05/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1499'>SM-1499</a>] -         Retirar a trava da avaliação inicial do antigo Secmed
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.10.1 (10/05/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1496'>SM-1496</a>] -         Verificar itens das tabelas Catalogo x Estoque
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.10.0 (09/05/2017)</h3>
<h2>        Task
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-1485'>SM-1485</a>] -         Emitir relatórios de remoções finalizadas para consertar os clientes.
</li>
</ul>

<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-1470'>SM-1470</a>] -         Enviar e-mail de notificação de finalização de remoção Externa e Particular.
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-1479'>SM-1479</a>] -         Permitir visualizar os equipamentos solicitados em uma Avaliação.
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-1480'>SM-1480</a>] -         Criar relatório de Antibióticos por paciente
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-1486'>SM-1486</a>] -         Melhoria no modulo de remoção.
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-1488'>SM-1488</a>] -         Modificação ficha de Avaliação de Enfermagem.
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-1489'>SM-1489</a>] -         Remover Score ABMID de todas as fichas do sistema.
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-1492'>SM-1492</a>] -         Modificação no cadastro de equipamentos ativos.
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-1494'>SM-1494</a>] -         Identificar faturas e listar pacientes na tela de gerenciar lotes
</li>
</ul>
