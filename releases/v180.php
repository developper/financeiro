<h3 class="titulo-versao">Versão 1.80.8 (17/10/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1241'>SM-1241</a>] -         Aumentar o número de scores trazidos na impressão do relatório de prorrogação
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.80.7 (17/10/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1240'>SM-1240</a>] -         Limitar o quadro de últimos scores para os últimos cinco, na impressão do relatório de prorrogação
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.80.6 (17/10/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1239'>SM-1239</a>] -         Corrigir a consulta do relatório de faturamento enviado
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.80.5 (14/10/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1238'>SM-1238</a>] -         Erro ao usar prescrições para nova
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.80.4 (14/10/2016)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1237'>SM-1237</a>] -         Tutorial da nova rotina de Suspensão de Medicamentos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.80.3 (14/10/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1236'>SM-1236</a>] -         Itens vazios na saída de pedidos de compras das Unidades para central.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.80.2 (13/10/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1234'>SM-1234</a>] -         Consultar a tabela de histórico de mudança de modalidades no relatório epidemiológico
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.80.1 (13/10/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1233'>SM-1233</a>] -         Correções na suspensão de medicamentos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.80.0 (10/10/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1220'>SM-1220</a>] -         Separar itens na tela de logística por mês
    </li>
</ul>