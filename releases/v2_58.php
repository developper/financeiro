<h3 class="titulo-versao">Versão 2.58.9 (20/07/2018)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2007'>SM-2007</a>] -         Investigar mudança de preço de matérias, dietas e medicamentosos na hora de faturar.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.58.8 (20/07/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2006'>SM-2006</a>] -         Avaliar preenchimento do score NEAD
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.58.7 (18/07/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2004'>SM-2004</a>] -         Prorrogação não traz a evolução dos problemas ativos na edição
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.58.6 (17/07/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2002'>SM-2002</a>] -         Melhoria na tela de Solicitações Pendentes do menu da Logística.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.58.5 (16/07/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2001'>SM-2001</a>] -         Problema ao finalizar uma remoção externa
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.58.4 (16/07/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2000'>SM-2000</a>] -         Problema ao usar SOMA/SUM em planilha do relatório de aluguel de equipamentos
    </li>
</ul>

<hr>
<h3 class="titulo-versao">Versão 2.58.3 (13/07/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1998'>SM-1998</a>] -         Alterar visualização dos pacientes de pedidos pendentes na logística
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.58.2 (13/07/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1997'>SM-1997</a>] -         Corrigir nomenclatura do Relato de não conformidades
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.58.1 (11/07/2018)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1993'>SM-1993</a>] -         Erro ao Imprimir Prescrição na hora que termina de solicitar os itens da Prescrição.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.58.0 (10/07/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1981'>SM-1981</a>] -         Melhoria no relatório de Eventos/APH Finalizados
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1983'>SM-1983</a>] -         Permitir gerenciar e-mails de fornecedores de equipamentos.
    </li>
</ul>

<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1950'>SM-1950</a>] -         Criar email para os fornecedores toda vez que um equipamento for movimentado no sistema
    </li>
</ul>

<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1991'>SM-1991</a>] -         Atualizar SIMPRO versões 130 ate 135 de 2018
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1963'>SM-1963</a>] -         Melhorias da Auditoria
    </li>
</ul>