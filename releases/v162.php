<h3 class="titulo-versao">Versão 1.62.1 (01/06/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1106'>SM-1106</a>] -         Conta Contábil não está sendo trazida corretamente na edição de bancos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.62.0 (01/06/2016)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1104'>SM-1104</a>] -         Enviar previsão de compras para Logística n primeira prescrição periódica
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1102'>SM-1102</a>] -         Adicionar cálculo de tempo entre solicitação e liberação no Relatório de Pedidos Liberados
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1105'>SM-1105</a>] -         Aumentar o prazo para Edição da Ficha de Avaliação Médica somente na Sexta-Feira
    </li>
</ul>