<h3 class="titulo-versao">Versão 1.16.5 (13/01/2015)</h3> 
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-660'>SM-660</a>] -         Problema com o cálculo do valor da porcentagem do rateio de uma nota.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.16.4 (13/01/2015)</h3> 
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-659'>SM-659</a>] -         Problema na validação da porcentagem do Rateio
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.16.3 (12/01/2015)</h3>              
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-655'>SM-655</a>] -         XML não traz o contratado corretamente
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.16.2 (09/01/2015)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-654'>SM-654</a>] -         Problema no filtro de busca para ativar  Dietas no menu Catálogo.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.16.1 (30/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-650'>SM-650</a>] -         Problema ao editar a data de vencimento de parcela.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.16.0 (30/12/2014)</h3>
<h2>        Sub-task
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-566'>SM-566</a>] -         Criar local onde se possa cadastrar o percentual contratual de cada Unidade Regional.
  </li>
</ul>

<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-646'>SM-646</a>] -         Relatório de Fluxo de Caixa Realizado não permite grandes intervalos de data
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-648'>SM-648</a>] -         Divergência ao exportar extrato bancário para o excel
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-649'>SM-649</a>] -         Problema no relatório analítico de fluxo de caixa
  </li>
</ul>

<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-643'>SM-643</a>] -         Adicionar o módulo de faturamento à lista de perfis em Usuários
  </li>
</ul>

<h2>        New Feature
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-565'>SM-565</a>] -         Criar relatório de custo de medicamentos, materiais, dietas e equipamentos enviados  para Unidades Regionais.
  </li>
</ul>

<h2>        Story
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-421'>SM-421</a>] -         Criação do relatório Fluxo de caixa Realizado
  </li>
</ul>