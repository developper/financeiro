<h3 class="titulo-versao">Versão 1.88.1 (08/12/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1296'>SM-1296</a>] -         Modificações na prescrição de curativo
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.88.0 (07/12/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1297'>SM-1297</a>] -         Criar o condensado de pedido de compra das Unidades Regionais
    </li>
</ul>