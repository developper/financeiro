<h3 class="titulo-versao">Versão 2.39.13 (25/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1770'>SM-1770</a>] -         Liberar o orçamento de Pedido para todos da auditoria
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.39.12 (24/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1763'>SM-1763</a>] -         Permitir que os usuários do financeiro vejam os custos de itens no estoque.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.39.11 (24/01/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1769'>SM-1769</a>] -         Corrigir status de autorização quando negado ou autorizado por completo
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.39.10 (19/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1766'>SM-1766</a>] -         Atualizar somatório dos impostos no excel do custo orçamento/fatura
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.39.9 (19/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1765'>SM-1765</a>] -         Atualizar imposto do relatório de custo fatura
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.39.8 (18/01/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1764'>SM-1764</a>] -         Erro ficha Tratamento de Feridas do Planserv.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.39.7 (18/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1759'>SM-1759</a>] -         Melhorias do CID proposta por Milena
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.39.6 (15/01/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1761'>SM-1761</a>] -         Erro ao salvar uma ocorrência.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.39.5 (12/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1758'>SM-1758</a>] -         Melhoria no Módulo de Ocorrências.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.39.4 (11/01/2018)</h3>
<h2>       Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1757'>SM-1757</a>] -         Permitir vários setores por usuários.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.39.3 (10/01/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1755'>SM-1755</a>] -         Correção na seção de respostas do módulo de ocorrências
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.39.2 (10/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1753'>SM-1753</a>] -         Permitir desativar ocorrência
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.39.0 (09/01/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1738'>SM-1738</a>] -         Modificações na listagem de usuários
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1739'>SM-1739</a>] -         Cria tabelas de Permissões
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1749'>SM-1749</a>] -         Melhorias no módulo de Ocorrência
    </li>
</ul>