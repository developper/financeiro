<h3 class="titulo-versao">Versão 1.60.0 (31/05/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1103'>SM-1103</a>] -         Separar consulta do Relatório de pedidos liberados por tipo.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.60.0 (27/05/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1098'>SM-1098</a>] -         Relatório de faturamento com status enviado
    </li>
</ul>