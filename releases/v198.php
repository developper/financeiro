<h3 class="titulo-versao">Versão 1.98.1 (16/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1370'>SM-1370</a>] -         Adicionar a prescrição aditiva laboratorial ao tutorial de Exames
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.98.0 (15/02/2017)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1363'>SM-1363</a>] -         Criar prescrição laboratorial
    </li>
</ul>
