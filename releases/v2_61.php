<h3 class="titulo-versao">Versão 2.61.13 (11/10/2018)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2065'>SM-2065</a>] -         Relatório Condensado ajuste no pdf do relatório que feridas.
</li>
</ul>
<h3 class="titulo-versao">Versão 2.61.12 (11/10/2018)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2064'>SM-2064</a>] -         Ajuste na modalidade do Relatório Condensado de Prorrogação.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.61.11 (09/10/2018)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2062'>SM-2062</a>] -         Problema na imagem da ferida no Formulário Condensado de Prorrogação.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.61.10 (08/10/2018)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2063'>SM-2063</a>] -         Erro ao cadastrar novo item no catálogo
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.61.9 (05/10/2018)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2061'>SM-2061</a>] -         Erro ao cadastrar e editar conta a Pagar/Receber.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.61.8 (05/10/2018)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2060'>SM-2060</a>] -         Modificar tela inicial do Sistema.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.61.7 (03/10/2018)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2059'>SM-2059</a>] -         Acrescentar a logo do planserve para os relatórios condensados deflagrados e evoluçao
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.61.6 (03/10/2018)</h3>
<h2>        Task
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2058'>SM-2058</a>] -         Permitir que todas as Unidades tenham acesso ao Formulários Condensados
</li>
</ul>
<hr>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2056'>SM-2056</a>] -         Melhoria no relatório de prorrogação médica.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.61.4 (02/10/2018)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2053'>SM-2053</a>] -         Ficha de Admissão de paciente.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.61.3 (01/10/2018)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2054'>SM-2054</a>] -         Permitir importar assinatura de fonoaudiólogo, psicologo e assistente social.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.61.2 (01/10/2018)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2051'>SM-2051</a>] -         Botão atualizar vencimento da parcela não ta aparecendo.
</li>
</ul>
<hr>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-2050'>SM-2050</a>] -         Erro ao salvar conta
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.61.0 (28/09/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2036'>SM-2036</a>] -         Formulário Condensado de Prorrogação
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2039'>SM-2039</a>] -         Formulário Condensado Deflagrado
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2040'>SM-2040</a>] -         Criar acesso de inclusão de evolução de prestadores
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2041'>SM-2041</a>] -         Impressão de formulário condensado
    </li>
</ul>

<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2035'>SM-2035</a>] -         Formulários Condensados do SAAD
    </li>
</ul>

<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2021'>SM-2021</a>] -         Relatório de itens pendentes na saída dos pacientes SESAB/SUS da UR SALVADOR
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2029'>SM-2029</a>] -         Melhoria no relatório de pacientes a faturar
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2037'>SM-2037</a>] -         Correção na rotina de Faturar
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2043'>SM-2043</a>] -         Melhorias na rotina de Fornecedores/Clientes.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-2048'>SM-2048</a>] -         Mudanças no módulo financeiro
    </li>
</ul>