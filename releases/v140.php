<h3 class="titulo-versao">Versão 1.40.0 (18/12/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-977'>SM-977</a>] -         Adicionar Visita Médica, de Enfermagem e de Nutricionista na ficha de prorrogação Planserv
	</li>
</ul>
