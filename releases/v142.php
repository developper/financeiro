<h3 class="titulo-versao">Versão 1.42.4 (15/01/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-997'>SM-997</a>] -         Erro ao imprimir uma solicitação
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.42.3 (15/01/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-993'>SM-993</a>] -         Não é possível excluir um único item na tela de &quot;Entrada em Estoque&quot;
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.42.2 (13/01/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-990'>SM-990</a>] -         Erro na entrada de produtos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.42.1 (13/01/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-989'>SM-989</a>] -         Restringir a alteração dos vencimentos somente das parcelas pendentes
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.42.0 (12/01/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-967'>SM-967</a>] -         Cadastro de nota de entrada na forma financeira pela logística
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-984'>SM-984</a>] -         Alterar vencimento das parcelas, diretamente na nota
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-985'>SM-985</a>] -         Acrescentar as Prescrições de Curativos na funcionalidade Buscar Solicitações no módulo de Enfermagem.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-987'>SM-987</a>] -         Identificar se os itens solicitados na Prescrição de Curativo são de entrega imediata.
    </li>
</ul>