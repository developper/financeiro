<h3 class="titulo-versao">Versão 2.7.8 (07/04/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1458'>SM-1458</a>] -         Erro no Relatório de faturamento enviado.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.7.7 (07/04/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1456'>SM-1456</a>] -         Erro ao editar item em Catalogo
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.7.6 (06/04/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1455'>SM-1455</a>] -         Corrigir a suspensão de medicamentos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.7.5 (06/04/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1454'>SM-1454</a>] -         Problema em pedido interno de compra.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.7.4 (06/04/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1453'>SM-1453</a>] -         Nome do fornecedor em branco no cadastro
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.7.3 (05/04/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1452'>SM-1452</a>] -         Validação de CNPJ/CPF no cadastro do fornecedor
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.7.2 (04/04/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1448'>SM-1448</a>] -         Inserir nome do Auditor na tela de liberação de pedidos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.7.1 (04/04/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1440'>SM-1440</a>] -         Bloquear data retroativa de devolução
    </li>
</ul>

<hr>
<h3 class="titulo-versao">Versão 2.7.0 (03/04/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1418'>SM-1418</a>] -         Bloquear o adicionar itens zerados para pre-auditar da fatura
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1425'>SM-1425</a>] -         Trazer pacientes ativos e novos no orçamento inicial
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1431'>SM-1431</a>] -         Modificar relatório administrativo
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1432'>SM-1432</a>] -         Acrescentar o campo de data da liberação da Auditoria na coluna de STATUS DO ITEM
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1433'>SM-1433</a>] -         Acrescentar um modal com um botão de  imprimir na prescrição de cuidados
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1435'>SM-1435</a>] -         Modificação no Relatório Assistenciais Médicos/Enfermagem
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1437'>SM-1437</a>] -         Acrescentar o relatório de buscar movimentação da logística no módulo de enfermagem
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1438'>SM-1438</a>] -         Limpar menu de enfermagem
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1447'>SM-1447</a>] -         Atualizar regras de aprovação das simulações de orçamentos
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1450'>SM-1450</a>] -         Tornar o campo &#39;Condução&#39; obrigatório
    </li>
</ul>

