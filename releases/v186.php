<h3 class="titulo-versao">Versão 1.86.0 (30/11/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1289'>SM-1289</a>] -         Retirar obrigatoriedade de todas as abas do relatório de não conformidade
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1290'>SM-1290</a>] -         Visualizar relatório de não conformidade
    </li>
</ul>