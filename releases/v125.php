<h3 class="titulo-versao">Versão 1.25.4 (26/05/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-796'>SM-796</a>] -         Corrigir leiaute do arquivo a ser importado pelo Sistema da Domínio
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.25.3 (20/05/2015)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-795'>SM-795</a>] -         Rever permissões de acesso no Módulo de Enfermagem.
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.25.2 (20/05/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-794'>SM-794</a>] -         Corrigir a tabela para qual são inseridos os itens do IPCC
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.25.1 (20/05/2015)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-793'>SM-793</a>] -         Retirar a validação da Ficha de Avaliação Médica.
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.25.0 (18/05/2015)</h3>
<h2>        Sub-task
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-779'>SM-779</a>] -         Modificar Solicitações pendentes no módulo da Logística
</li>
</ul>

<h2>        New Feature
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-777'>SM-777</a>] -         Criara a Unidade Feira de Santana e migrar os pacientes e seus usuários para a mesma.
</li>
</ul>