<h3 class="titulo-versao">Versão 2.20.4 (25/08/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1581'>SM-1581</a>] -         Aumentar o tamanho do campo DESCRIÇÃO/HISTÓRICO no arquivo de exportação para Dominio
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.20.2 (21/08/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1574'>SM-1574</a>] -         Corrigir o percentual negativo na simulação quando a quantidade do item é zero
    </li>
</ul>
<h3 class="titulo-versao">Versão 2.20.1 (16/08/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1579'>SM-1579</a>] -         Remover aviso de Refaturar no pré-faturar 
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.20.0 (16/08/2017)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1561'>SM-1561</a>] -         Atualização da rotina de Kits de enfermagem
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1571'>SM-1571</a>] -         Associar prescrição em pedido avulso de complemento de periódico
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.20.2 (17/08/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1582'>SM-1582</a>] -         Compartilhamento de faturas salvas entre as auditoras
    </li>
</ul>