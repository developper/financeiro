<h3 class="titulo-versao">Versão 1.31.14 (26/08/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-894'>SM-894</a>] -         Problema ao visualizar modalidade do paciente na ficha de avaliação inicial
	</li>
</ul>
<h3 class="titulo-versao">Versão 1.31.13 (25/08/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-893'>SM-893</a>] -         Corrigir o tipo de profissional na impressão da prescrição de nutrição
	</li>
</ul>
<h3 class="titulo-versao">Versão 1.31.12 (13/08/2015)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-885'>SM-885</a>] -         Acrescentar um novo campo na planilha de excel da funcionalidade Custo Orçamento/Fatura
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.31.11 (12/08/2015)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-883'>SM-883</a>] -         Permitir que o usuário cadastre o imposto ISS para o Plano de Saúde.
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.31.10 (12/08/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-872'>SM-872</a>] -         Tornar visível a data da evolução nas imagens das feridas
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.31.9 (11/08/2015)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-881'>SM-881</a>] -         Atualizar a funcionalidade Custo Orçamento/Fatura
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.31.8 (05/08/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-880'>SM-880</a>] -         Erro nos proplemas ativos do relatório deflagrado  Médico.
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.31.7 (04/08/2015)</h3>
<h2>        Improvement
</h2>

<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-877'>SM-877</a>] -         Atualização de acesso para as prescrições de enfermagem
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.31.6 (03/08/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-875'>SM-875</a>] -         Conciliação Bancária não traz os Encargos Financeiros
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.31.5 (01/08/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-874'>SM-874</a>] -         Lista de parcelas no processamento está desordenada e não aparece &#39;Desprocessar&#39; para a última
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.31.4 (31/07/2015)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-873'>SM-873</a>] -         Retirar a opção equipamentos no menu devolução do módulo de Logística
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.31.3 (31/07/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-871'>SM-871</a>] -         Imagem da ferida está sendo duplicada no relatório de prorrogação
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.31.2 (30/07/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-870'>SM-870</a>] -         Acrescentar opção de modificar o código próprio dos itens em editar fatura
	</li>
</ul>

<hr>
<h3 class="titulo-versao">Versão 1.31.0 (28/07/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-867'>SM-867</a>] -         Trazer o código próprio dos itens na impressão e no excel da fatura
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-868'>SM-868</a>] -         Trazer o código próprio dos itens na Guia TISS e no Resumo de Internação
	</li>
</ul>

<h2>        New Feature
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-853'>SM-853</a>] -         Editar o código do item ao fechar uma fatura
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-858'>SM-858</a>] -         Possibilitar que o usuário adicione opcionalmente imagens de feridas no relatório aditivo
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-866'>SM-866</a>] -         Possibilitar que o usuário remova os itens zerados ao preencher a Guia TISS
	</li>
</ul>