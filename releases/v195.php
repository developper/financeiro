<h3 class="titulo-versao">Versão 1.95.10 (10/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1356'>SM-1356</a>] -         Mudança de permissão da funcionalidade dos  Kits
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.95.9 (10/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1355'>SM-1355</a>] -         Colocar lote no impresso de saída da logística pra pacientes.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.95.8 (09/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1354'>SM-1354</a>] -         Tornar o campo de matrícula obrigatório
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.95.6 (08/02/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1351'>SM-1351</a>] -         Página não encontrada ao clicar no botão de visualizar da remoção
    </li>
</ul>
<hr>

<h3 class="titulo-versao">Versão 1.95.5 (08/02/2017)</h3>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1347'>SM-1347</a>] -         Visualizar remoção particular
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.95.4 (08/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1338'>SM-1338</a>] -         Acrescentar campo de custo na tela de editar entrada da logística
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.95.3 (08/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1343'>SM-1343</a>] -         Verificar entrada de medicamentos no módulo da logística
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.95.2 (07/02/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1348'>SM-1348</a>] -         Erro ao selecionar um paciente no módulo de remoção
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.95.1 (07/02/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1346'>SM-1346</a>] -         Erro na importação da nota.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.95.0 (07/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1340'>SM-1340</a>] -         Remoção de pacientes particular
    </li>
</ul>