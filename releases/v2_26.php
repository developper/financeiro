<h3 class="titulo-versao">Versão 2.26.0 (02/10/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1633'>SM-1633</a>] -         Melhoria na listagem de pacientes do CID
    </li>
</ul>
