<h3 class="titulo-versao">Versão 2.56.2 (20/06/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1973'>SM-1973</a>] -         Usar para novo em planos de saúde
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.56.1 (19/06/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1969'>SM-1969</a>] -         Problema ao excluir serviço em planos de saúde
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.56.0 (19/06/2017)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1940'>SM-1940</a>] -         Trazer itens de serviço do relatório aditivo na fatura
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1960'>SM-1960</a>] -         Escolher empresa nas impressões do módulo de enfermagem
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1961'>SM-1961</a>] -         Permitir escolher empresa ao gerar PDF da evolução médica.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1962'>SM-1962</a>] -         Permitir escolher empresa ao gerar PDF do relatório de Alta Médica
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1968'>SM-1968</a>] -         Escolher empresa nas impressões do módulo médico
    </li>
</ul>

<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1901'>SM-1901</a>] -         Impresso com a logo da Assiste Vida.
    </li>
</ul>