<h3 class="titulo-versao">Versão 1.65.2 (13/07/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1124'>SM-1124</a>] -         Nutricionista não consegue acessar avaliação e evolução médica
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.65.1 (12/07/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1131'>SM-1131</a>] -         Erro ao fazer relatório de prorrogação Modelo Planserv
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.65.0 (11/07/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1130'>SM-1130</a>] -         Erro na funcionalidade que dispara e-mail sobre &quot;parecer&quot; da auditoria para itens de entrega imediata.
    </li>
</ul>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1127'>SM-1127</a>] -         Adicionar o modal com a opção exportar para xlsx no lançamento de custo.
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1117'>SM-1117</a>] -         Permitir edição do campo data das fotos de ferida do relatório de prorogação de enfermagem
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1125'>SM-1125</a>] -         Trazer serviços com suas respectivas frequências e justificativas na evolução médica da prorrogação Planserv
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1126'>SM-1126</a>] -         Adicionar novos e-mails, no aviso de fatura reaberta.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1128'>SM-1128</a>] -         Modificações no Relatório de Faturamento Enviado, no modulo da Auditoria.
    </li>
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1129'>SM-1129</a>] -         Permitir que na funcionalidade de lançar custo possa existir itens duplicados.
    </li>
</ul>