<h3 class="titulo-versao">Versão 1.63.5 (10/06/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1115'>SM-1115</a>] -         Corrigir email de envio de parecer de entrega imediata
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.63.1 (06/06/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1112'>SM-1112</a>] -         Permitir que as outras UR&#39;s possam utilizar a opção trocar na saída de itens para pacientes.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.63.1 (03/06/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1110'>SM-1110</a>] -         Sistema não identifica se o plano aceita somente Genérico quando a fatura é feita por paciente.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.63.0 (03/06/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1090'>SM-1090</a>] -         Preview nas listagens do Sismederi
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1100'>SM-1100</a>] -         Identificar se o plano do paciente permite utilizar somente medicamento Genérico.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1109'>SM-1109</a>] -         Mostrar o botão de Opções de SECMED para qualquer UR
    </li>
</ul>