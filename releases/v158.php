<h3 class="titulo-versao">Versão 1.58.1 (24/05/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1094'>SM-1094</a>] -         Erro ao inserir nota de entrada
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.58.0 (23/05/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1089'>SM-1089</a>] -         Identificar o tipo de medicamento que deve ser solicitado de acordo com o plano.
    </li>
</ul>