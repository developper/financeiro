<h3 class="titulo-versao">Versão 2.4.1 (09/03/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1410'>SM-1410</a>] -         Duplicidade de emails de negativas da auditoria para UR Feira
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.4.0 (09/03/2017)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1404'>SM-1404</a>] -         Criar relatório de notas fiscais lançadas pela logística.
    </li>
</ul>

<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1402'>SM-1402</a>] -         Relatório de vencidos como saída interna
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1385'>SM-1385</a>] -         Relatório Administrativo por UR
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1392'>SM-1392</a>] -         Criar um relatório das simulações realizadas em um determinado período
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1407'>SM-1407</a>] -         Restrição no cadastro e edição de Materiais.
    </li>
</ul>
