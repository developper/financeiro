<h3 class="titulo-versao">Versão 1.85.2 (29/11/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1288'>SM-1288</a>] -         Relatório de Separação da Logística
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.85.1 (28/11/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1291'>SM-1291</a>] -         Falha ao gerar relatório de pedidos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.85.0 (25/11/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1285'>SM-1285</a>] -         Documento vazio ao clicar no botão de imprimir na tela de saída da logística
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1287'>SM-1287</a>] -         Resolver problema do filtro do Secmed
    </li>
</ul>

<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1281'>SM-1281</a>] -         Incluir período das prescrições no aviso de pendências do sistema
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1282'>SM-1282</a>] -         Farmácia visualizar justificativa de troca de equipamento
    </li>
</ul>

<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1278'>SM-1278</a>] -         Relatórios Assistenciais
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1146'>SM-1146</a>] -         Mostrar imagem da assinatura do profissional
    </li>
</ul>