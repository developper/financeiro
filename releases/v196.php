<h3 class="titulo-versao">Versão 1.96.3 (13/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1362'>SM-1362</a>] -         Adicionar novo item no quadro de feridas da evolução de enfermagem.
    </li>
</ul>

<hr>
<h3 class="titulo-versao">Versão 1.96.2 (13/02/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1361'>SM-1361</a>] -         Modificar permissão de ativação de Material.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.96.1 (13/02/2017)</h3>
<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1360'>SM-1360</a>] -         Ativar faturas.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.96.0 (13/02/2017)</h3>

<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1353'>SM-1353</a>] -         Tutorial do Módulo de Exames
    </li>
</ul>

<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1349'>SM-1349</a>] -         Relatório de materiais descartáveis
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1350'>SM-1350</a>] -         Relatório de materiais descartáveis vencidos
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1357'>SM-1357</a>] -         Relatório de movimentação de medicamentos.
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1359'>SM-1359</a>] -         Módulo do faturamento rever a funcionalidade cancelar fatura do módulo do faturamento.
    </li>
</ul>
