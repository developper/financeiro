<h3 class="titulo-versao">Versão 1.36.1 (09/10/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-935'>SM-935</a>] -         Enviar email para a coordenadoria de enfermagem da UR quando um paciente mudar para óbito, alta ou suspensão
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.36.0 (08/10/2015)</h3>
<h2>        New Feature
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-931'>SM-931</a>] -         Gerar solicitação de recolhimento de equipamentos ativos, quando houver óbito, alta ou suspensão de pacientes
	</li>
</ul>

<h2>        Task
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-723'>SM-723</a>] -         Atualizar custo médio dos produtos baseando na tabela de entradas.
	</li>
</ul>

<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-327'>SM-327</a>] -         Alterações no Cadastro de Pacientes para facilitar o preenchimento e garantir a integridade das informações que de fato interessam.
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-932'>SM-932</a>] -         Adicionar novo ícone para paciente que tenha primeira periódica pós-avaliação
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-933'>SM-933</a>] -         Colocar o CPF do paciente, para aparecer nos cabeçalhos das fichas gerados pelo sistema.
	</li>
</ul>