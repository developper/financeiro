<h3 class="titulo-versao">Versão 2.2.3 (03/03/2017)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1396'>SM-1396</a>] -         Transferir os emails de avisos de avaliação inicial para a auditoria
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1400'>SM-1400</a>] -         Colocar Camila para receber os emails de exames da UR Feira
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.2.2 (03/03/2017)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1397'>SM-1397</a>] -         Modificações na impressão da prescrição de curativo
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.2.1 (02/03/2017)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1393'>SM-1393</a>] -         Acrescentar a ur no cabeçalho da fatura e na tabela de simulação
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.2.0 (01/03/2017)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1387'>SM-1387</a>] -         Permitir a inserção de dois ou mais itens na coberturas de feridas das Fichas de Avaliação e Evolução de enfermagem
  </li>
</ul>