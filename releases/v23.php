<h3 class="titulo-versao">Versão 2.3.2 (03/03/2017)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1388'>SM-1388</a>] -         Acrescentar o campo de observação da auditoria na tela de custos do financeiro
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.3.1 (03/03/2017)</h3>
<h2>        Task
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1399'>SM-1399</a>] -         Desativar os itens sem movimentação nos últimos 6 meses e com estoque zerado.
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.3.0 (03/03/2017)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1383'>SM-1383</a>] -         Gerar links de pdf após finalizar remoção para o financeiro e faturamento
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-1398'>SM-1398</a>] -         Acrescentar o campo de observação ao selecionar a opção outros em local da lesão
  </li>
</ul>
