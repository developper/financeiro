<h3 class="titulo-versao">Versão 1.51.6 (20/04/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1068'>SM-1068</a>] -         Problema no excel do extrato bancário
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.51.5 (19/04/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1065'>SM-1065</a>] -         Nome da auditora parou de aparecer no e-mail de item negado.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.51.4 (18/04/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1064'>SM-1064</a>] -         Adicionar assinatura do médico na prescrição médica, modelo aprazamento.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.51.3 (15/04/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1063'>SM-1063</a>] -         Adicionar localstorage nos filtros da busca de pacientes do SECMED
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.51.2 (15/04/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1062'>SM-1062</a>] -         Problemas ao criar orçamentos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.51.1 (15/04/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1061'>SM-1061</a>] -         Erro ao tentar inserir um item no orçamento avulso.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.51.0 (13/04/2016)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1057'>SM-1057</a>] -         Relatório de Retenções
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1059'>SM-1059</a>] -         Adicionar &quot;Todos&quot; nos filtros de pacientes
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1060'>SM-1060</a>] -         Manter o histórico dos filtros na busca de pacientes
    </li>
</ul>