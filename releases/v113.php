<h3 class="titulo-versao">Versão 1.13.14 (27/11/2014)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-611'>SM-611</a>] -         Milena solicitou receber os e-mails de avisos dos relatórios e fichas feitos pelos enfermeiros no Sistema
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.13.13 (27/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-610'>SM-610</a>] -         Solicitação da Logística não traz o nome do paciente
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.13.12 (27/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-608'>SM-608</a>] -         O título Prescrição está se repetindo ao abrir uma prescrição que está salva.
</li>
</ul>
<h3 class="titulo-versao">Versão 1.13.11 (27/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-606'>SM-606</a>] -         Avaliar o motivo pelo qual o código do usuário não foi adicionado no html que é gerado ao clicar em &quot;Salvar Prescrição&quot; do módulo de Médico
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.13.10 (27/11/2014)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-607'>SM-607</a>] -         Adição de destinatário de e-mail em itens negados para pacientes de Alagoinhas
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.13.9 (26/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-605'>SM-605</a>] -         Erro ao finalizar uma prescrição criada através do &quot;Usar para Nova&quot; de uma Prescrição de Avaliação
<<<<<<< HEAD
/li>
=======
</li>
>>>>>>> hotfix/SM-608
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.13.8 (26/11/2014)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-604'>SM-604</a>] -         Trazer equipamentos solicitados na prescrição de avaliação já marcados na 1° solicitação periódica.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.13.7 (26/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-602'>SM-602</a>] -         Em alguns recibos gerados na logística o endereço do paciente está vindo o do domicilio o correto seria do local de internação. 
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.13.6 (25/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-601'>SM-601</a>] -         Relatório aditivo de enfermagem não traz os problemas ativos de acordo com a evolução médica
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.13.5 (24/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-598'>SM-598</a>] -         Erro ao adicionar um item na hora de editar um Orçamento Avulso.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.13.4 (20/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-594'>SM-594</a>] -         Problema ao imprimir uma prescrição de avaliação no módulo de enfermagem.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.13.3 (20/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-591'>SM-591</a>] -         Problema com a Ficha de Avaliação e Evolução de Enfermagem nos tópicos Oxigênios e Aspirações 
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.13.2 (20/11/2014)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-590'>SM-590</a>] -         Guia de Internação não possui campos de validade de senha e No. atribuído pela operadora
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.13.1 (19/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-589'>SM-589</a>] -         Problema na  funcionalidade de informar que chegou um item novo na auditoria.
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.13.0 (19/11/2014)</h3>
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-588'>SM-588</a>] -         Problema na impressão da ficha de Evolução de Enfermagem no Tópico Dispositivos para Eliminação e/ou Excreções
</li>
</ul>
        
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-570'>SM-570</a>] -         Sendo uma faturista posso imprimir as  Guias de Resumo de  Internação, para ganhar agilidade na hora de enviar para o plano.
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-587'>SM-587</a>] -         Acrescenta na ficha de Avaliação e evolução de enfermagem no  &quot;tópico feridas&quot; no sub item &quot;Local&quot; a opção &quot;COTO&quot;
</li>
</ul>
        
<h2>        New Feature
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-568'>SM-568</a>] -         Adicionar opções em “Feridas” nos relatórios e fichas de enfermagem
</li>
<li>[<a href='https://mederi.atlassian.net/browse/SM-579'>SM-579</a>] -         Na tela de Auditar Solicitações do módulo da Auditoria, deverá aparecer um identificador indicando os ítens novos para aquele paciente
</li>
</ul>
       

