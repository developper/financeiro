Erro ao modificar status do paciente.
<h3 class="titulo-versao">Versão 1.48.5 (24/03/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1042'>SM-1042</a>] -         Erro ao modificar status do paciente.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.48.3 (16/03/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1041'>SM-1041</a>] -         Error ao enviar email de recolhimento dos equipamentos ao desativar paciente.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.48.2 (15/03/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1040'>SM-1040</a>] -         Erro ao tentar salvar Prescrição de Avaliação Medica e Prescrição de Curativo.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.48.1 (14/03/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1038'>SM-1038</a>] -         Erro ao tentar substituir um equipamento para um paciente.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.48.0 (11/03/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1037'>SM-1037</a>] -         Enviar emails da auditoria via Sendgrid
    </li>
</ul>