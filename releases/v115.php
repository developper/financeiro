  <h3 class="titulo-versao">Versão 1.15.19 (30/12/2014)</h3>  
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-647'>SM-647</a>] -         Problema ao &quot;desprocessar&quot; uma parcela de uma &quot;Sub-Nota de Imposto&quot;
</li>
</ul>
<hr>
  <h3 class="titulo-versao">Versão 1.15.18 (29/12/2014)</h3>                
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-645'>SM-645</a>] -         Erro ao imprimir o relatório  Buscar de Saídas no módulo de Logística.
</li>
</ul>
  <hr>
<h3 class="titulo-versao">Versão 1.15.17 (26/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-645'>SM-645</a>] -         Erro ao imprimir o relatório  Buscar de Saídas no módulo de Logística.
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.15.16 (24/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-644'>SM-644</a>] -         Relatório de devoluções não traz resultados do dia final da consulta
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.15.15 (19/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-641'>SM-641</a>] -         A data do score Nead do editar, visualizar e usar para nova da &quot;Ficha de Prorrogação Médica&quot; não está aparecendo
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.15.14 (19/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-640'>SM-640</a>] -         Problema em pesquisar movimentações
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.15.13 (19/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-637'>SM-637</a>] -         Input type date não funciona no Safari
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.15.12 (17/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-639'>SM-639</a>] -         Erro em XML do Resumo de Internação
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.15.12 (17/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-639'>SM-639</a>] -         Erro em XML do Resumo de Internação
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.15.11 (17/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-624'>SM-624</a>] -         Contagem de itens pendentes da logística não corresponde com a real
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.15.10 (15/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-637'>SM-637</a>] -         Input type date não funciona no Safari
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.15.9 (15/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-638'>SM-638</a>] -         Select de novo item do catálogo não permite inativar
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.15.8 (15/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-627'>SM-627</a>] -         Verificar divergência entre os resultados gerados na tela de *Conciliação Bancária* e a tela de *Extrato Bancário*
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.15.7 (15/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-634'>SM-634</a>] -         MysqlError collected by New Relic at 14:58:40 (GMT-03:00) Brasilia
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.15.6 (11/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-636'>SM-636</a>] -         MysqlError collected by New Relic at 16:46:35 (GMT-03:00) Brasilia
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.15.5 (11/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-635'>SM-635</a>] -         MysqlError collected by New Relic at 16:35:35 (GMT-03:00) Brasilia
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.15.4 (11/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-635'>SM-635</a>] -         MysqlError collected by New Relic at 16:35:35 (GMT-03:00) Brasilia
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.15.3 (11/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-634'>SM-634</a>] -         MysqlError collected by New Relic at 14:58:40 (GMT-03:00) Brasilia
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.15.2 (11/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-632'>SM-632</a>] -         MysqlError collected by New Relic at 12:25:35 (GMT-03:00) Brasilia
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.15.1 (10/12/2014)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-631'>SM-631</a>] -         Últimas modificações não estão sendo exibidas na página inicial
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.15.0 (10/12/2014)</h3>
<h2>        Sub-task
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-618'>SM-618</a>] -         Exibir o movimento, saldo anterior e saldo final de cada dia/mês
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-621'>SM-621</a>] -         Permitir que o relatório seja emitido por movimento diário
  </li>
</ul>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-613'>SM-613</a>] -         Na hora de criar a guia SP/SADT ou Resumo de Internação os dados da autorização deve ser por fatura.
  </li>
</ul>