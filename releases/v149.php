<h3 class="titulo-versao">Versão 1.49.4 (04/04/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1052'>SM-1052</a>] -         Problema na composição dos valores das parcelas em notas de entrada
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.49.3 (04/04/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1051'>SM-1051</a>] -         Erro ao inserir um custo de fatura
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.49.2 (04/04/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1050'>SM-1050</a>] -         Erro ao iniciar uma solicitação avulsa
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.49.1 (04/04/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1049'>SM-1049</a>] -         Problema na busca de pacientes por usuários de outras Ur&#39;s
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.49.0 (01/04/2016)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1045'>SM-1045</a>] -         Melhorar a busca de pacientes no módulo médico
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1046'>SM-1046</a>] -         Melhorar a busca de pacientes no módulo de enfermagem
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1047'>SM-1047</a>] -         Melhorar a busca de pacientes no módulo administrativo
    </li>
</ul>

<h2>        Story
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1044'>SM-1044</a>] -         Melhorar a busca de pacientes
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1043'>SM-1043</a>] -         Acrescentar o campo &#39;justificativa&#39; no quadro de feridas na Prescrição de Curativo de Enfermagem.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1048'>SM-1048</a>] -         Na hora de cadastrar custo alguns itens estão vindo duplicados.
    </li>
</ul>