<h3 class="titulo-versao">Versão 1.39.1 (16/12/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-973'>SM-973</a>] -         Permitir que os usuários do Secmed tenham acesso as Prescrições de Curativos de Enfermagem.
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.39.0 (15/12/2015)</h3>
<h2>        New Feature
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-964'>SM-964</a>] -         Criar ficha de prorrogação do Planserv
	</li>
</ul>
