<h3 class="titulo-versao">Versão 2.47.7 (16/04/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1879'>SM-1879</a>] -         Erro na solicitação de enfermagem.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.47.6 (13/04/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1878'>SM-1878</a>] -         Problema na soma dos rateios das notas fiscais.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.47.5 (12/04/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1877'>SM-1877</a>] -         Ajustes de e-mails enviados para auditoria.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.47.4 (11/04/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1875'>SM-1875</a>] -         Erro ao tentar adicionar itens em orçamento de prorrogação.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.47.2 (10/04/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1871'>SM-1871</a>] -         Problema ao finalizar orçamento
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.47.1 (10/04/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1870'>SM-1870</a>] -         Lentidão na busca da funcionalidade  Novo na opção de Catalogo.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.47.0 (09/04/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1857'>SM-1857</a>] -         Melhoria no na rotina de conciliação bancária.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1868'>SM-1868</a>] -         Atualizar relatório de itens vencidos.
    </li>
</ul>