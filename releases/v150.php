<h3 class="titulo-versao">Versão 1.50.3 (08/04/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1056'>SM-1056</a>] -         Adicionar email da coordenadoria de enfermagem da UR Feira no email de itens negados pela auditoria
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.50.2 (08/04/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1055'>SM-1055</a>] -         Problema no envio do email de itens negados para a coordenadoria da UR Feira
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.50.1 (07/04/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1054'>SM-1054</a>] -         Erro no menu de listar pacientes.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.50.0 (06/04/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1053'>SM-1053</a>] -         Adicionar cache na listagem de pacientes
    </li>
</ul>