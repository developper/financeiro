<h3 class="titulo-versao">Versão 1.27.3 (08/06/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-810'>SM-810</a>] -         Problema ao gerar arquivo de importação Dominio
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.27.2 (03/06/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-809'>SM-809</a>] -         Sonda Gastro não mostra número em visualizar e imprimir evolução de enfermagem
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.27.1 (02/06/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-807'>SM-807</a>] -         Corrigir o registro 5 do arquivo de exportação para a Dominio Sistemas
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.27.0 (29/05/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-798'>SM-798</a>] -         Modificações na ficha de Avaliação Médica
	</li>
</ul>