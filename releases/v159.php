<h3 class="titulo-versao">Versão 1.59.0 (25/05/2016)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1095'>SM-1095</a>] -         Permitir a troca de materiais e dietas na saída para paciente.
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1092'>SM-1092</a>] -         Identificar o tipo de medicamento na troca de medicamentos na Logística.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1096'>SM-1096</a>] -         Permitir que a faturista indique a data que uma fatura foi enviada para a operadora.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1097'>SM-1097</a>] -         Adicionar opção de Analítico e Sintético no relatório de Pedidos Liberados
    </li>
</ul>