<h3 class="titulo-versao">Versão 2.22.6 (12/09/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1615'>SM-1615</a>] -         Problema na tela da auditoria.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.22.5 (11/09/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1614'>SM-1614</a>] -         Erro no módulo de Eventos/APH
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.22.4 (11/09/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1613'>SM-1613</a>] -         A tela de listar usuário está quebrando
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.22.3 (06/09/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1611'>SM-1611</a>] -        Erro ao lançar custo de faturas /orçamento e na busca por cpf em fornecedores/clientes
    </li>
</ul>
<h3 class="titulo-versao">Versão 2.22.2 (06/09/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1609'>SM-1609</a>] -         Botão de cancelar evento não funciona
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.22.1 (04/09/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1605'>SM-1605</a>] -         Problema na listagem de faturas
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.22.0 (04/09/2017)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1592'>SM-1592</a>] -         Limpar a listagem dos itens que aparece na aba todos de logística
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1604'>SM-1604</a>] -         Atualizar listar paciente do CID
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1481'>SM-1481</a>] -         Atualizar tabela de convênios
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1591'>SM-1591</a>] -         Acrescentar o tipo de de prescrição no recibo de impressão da logística
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1593'>SM-1593</a>] -         Modificar a busca do modal de Fatura e acrescentar a opção descontinuado na tela de catalogo
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1602'>SM-1602</a>] -         Acrescentar um filtro de remoções que não serão cobradas da tela relatório de Remoções Finalizadas
    </li>
</ul>