<h3 class="titulo-versao">Versão 2.51.5 (17/05/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1920'>SM-1920</a>] -         Modificar valor padrão da tag reducaoAcrescimo no XML da guia TISS
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.51.4 (16/05/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1916'>SM-1916</a>] -         Problema na rotina de Autorizações.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.51.3 (16/05/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1915'>SM-1915</a>] -         Problema no Orçamento de prorrogação de Acssa Arley.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.51.2 (15/05/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1913'>SM-1913</a>] -         Corrigir tempo de criação de comentário
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.51.1 (14/05/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1910'>SM-1910</a>] -         Problema ao adicionar comentários
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.51.0 (14/05/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1908'>SM-1908</a>] -         Permitir editar remoção interna
    </li>
</ul>

<h2>        Task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1896'>SM-1896</a>] -         Melhorias do sistema de autorização
    </li>
</ul>