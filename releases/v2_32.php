<h3 class="titulo-versao">Versão 2.32.4(17/11/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1694'>SM-1694</a>] -         Exportar estoque para excel.
    </li>
</ul>

<hr>
<h3 class="titulo-versao">Versão 2.32.3 (17/11/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1692'>SM-1692</a>] -         Erro ao finalizar um Orçamento de Prorrogação.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.32.2 (16/11/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1689'>SM-1689</a>] -         Erro ao enviar e-mail de orçamento inicial finalizado.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.32.1 (16/11/2017)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1687'>SM-1687</a>] -         Erro ao fazer uma simulação em um orçamento inicial.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.32.0 (15/11/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1665'>SM-1665</a>] -         Inserir o período da prescrição pai na justificativa das solicitações avulsas complementares do buscar solicitações
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1673'>SM-1673</a>] -         Refatorar a funcionalidade de margem de contribuição.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1675'>SM-1675</a>] -         Adaptar a rotina do Sec no sistema de encaminhamento do sac
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1676'>SM-1676</a>] -         Colocar o campo veículo no recibo de saída da logística
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1677'>SM-1677</a>] -         Colocar a justificativa nas solicitações aditivas de equipamentos
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1681'>SM-1681</a>] -         Permitir salvar mais de uma Fatura e Orçamento Inicial para um mesmo paciente.
    </li>
</ul>