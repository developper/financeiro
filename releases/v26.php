<h3 class="titulo-versao">Versão 2.6.5 (31/03/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1449'>SM-1449</a>] -         Erro ao ativar um item no simpro/brasindice
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.6.4 (30/03/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1446'>SM-1446</a>] -         Modificações no Módulo de Intercorrências.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.6.3 (29/03/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1445'>SM-1445</a>] -         Adicionar Visita de Enfermeiro Assistente na prescrição aditiva Laboratorial
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.6.2 (29/03/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1444'>SM-1444</a>] -         Modificações na rotina de Intercorrências
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.6.1 (29/03/2017)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1429'>SM-1429</a>] -         Modificar algumas contas utilizadas pela rotina de avisos de e-mail do sistema.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.6.0 (28/03/2017)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1423'>SM-1423</a>] -         Modificar consultas que traz o custo de serviços e equipamentos dos orçamento e faturas.
    </li>
</ul>

<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1419'>SM-1419</a>] -         Transformar Encaminhamento de Urgência e emergência em Intercorrência Médica
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1389'>SM-1389</a>] -         Atualizar o score NEAD do sismederi para o novo formato
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1413'>SM-1413</a>] -         Criar histórico de custos por compra para equipamentos e de serviço para o Financeiro
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1414'>SM-1414</a>] -         Alterar a tabela da origem dos custos para as Faturas
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1415'>SM-1415</a>] -         Alterar a origem dos custos dos orçamentos.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1420'>SM-1420</a>] -         Inserir o documento de Intercorrência no Relatório Administrativo Médico/Enfermagem.
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1422'>SM-1422</a>] -         Criar a opção de coleta de exames
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1427'>SM-1427</a>] -         Pegar custo de material, dieta e medicamento por período. Nas faturas e orçamentos.
    </li>
</ul>
