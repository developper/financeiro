<h3 class="titulo-versao">Versão 2.42.2 (05/03/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1818'>SM-1818</a>] -         Modificar regras de avisos por e-mail de algumas funcionalidades do sistema.
    
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.42.0 (19/02/2018)</h3>
<h2>        Sub-task
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1806'>SM-1806</a>] -         Criar uma tela de CRUD para as categorias de materiais
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1618'>SM-1618</a>] -         Modificações no plano de feridas Planserv
    </li>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1715'>SM-1715</a>] -         Associar o campo de Juros/Multa ao plano de contas financeiro cód. 2.5.1.02
    </li>
</ul>
