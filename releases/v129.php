<h3 class="titulo-versao">Versão 1.29.14 (15/07/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-851'>SM-851</a>] -         Problema na funcionalidade  Custo Orçamento/Fatura no Módulo Financeiro
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.29.13 (10/07/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-848'>SM-848</a>] -         Erro de validação de dados na ficha de Alta Médica
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.29.12 (09/07/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-846'>SM-846</a>] -         Erro no avatar de identificação de setor do usuário.
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.29.11 (09/07/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-845'>SM-845</a>] -         Erro ao exportar fatura para excel.
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.29.10 (08/07/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-841'>SM-841</a>] -         Aplicar novas regras para arquivo de baixa da Dominio Sistemas
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.29.9 (07/07/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-836'>SM-842</a>] -         Alterar as opções do campo "Status da Ferida" apenas na ficha de avaliação
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.29.8 (07/07/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-836'>SM-831</a>] -         Rateios estão sendo duplicados nos registros 05 e somatórios estão invertidos
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.29.7 (07/07/2015)</h3>
<h2>        New Feature
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-838'>SM-838</a>] -         Colocar a opção de visualizar Relatório Deflagrado Médico no Módulo Administração.
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.29.6 (06/07/2015)</h3>
<h2>        New Feature
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-839'>SM-839</a>] -         Erro ao gerar pdf do relatório Deflagrado de Médico.
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.29.5 (03/07/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-836'>SM-836</a>] -         Erro ao gerar Fatura não está trazendo equipamentos.
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.29.4 (03/07/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-835'>SM-835</a>] -         Erro ao dar saída de itens para paciente no módulo da Logística.
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.29.3 (01/07/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-834'>SM-834</a>] -         Adicionar o caminho público do storage.googleapis.com no config.php
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.29.2 (01/07/2015)</h3>
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://mederi.atlassian.net/browse/SM-833'>SM-833</a>] -         Melhorar o worker que processa a fila das imagens de ferida
</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.29.1 (30/06/2015)</h3>
<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-832'>SM-832</a>] -         Colocar rotina de aviso por e-mail para o Secmed no relatório deflagrado médico.
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.29.0 (30/06/2015)</h3>
<h2>        Sub-task
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-830'>SM-830</a>] -         Mudar a forma como é trazida a logo da empresa nos relatórios do sistema
  </li>
</ul>

<h2>        Improvement
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-686'>SM-686</a>] -         Colocar todos os nomes dos pacientes do Sismederi em caixa alta
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-750'>SM-750</a>] -         Adicionar campo de confirmar senha em cadastro de Usuarios
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-811'>SM-811</a>] -         Modificar a Solicitação Aditiva de Enfermagem.
  </li>
</ul>

<h2>        New Feature
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-694'>SM-694</a>] -         Acrescentar a possibilidade de inserir foto das feridas dos pacientes nos relatórios de &quot;Avaliação inicial&quot;, &quot;Evolução&quot;, &quot;Aditivo&quot;, &quot;Prorrogação&quot; de enfermagem
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-752'>SM-752</a>] -         Criar dispositivo para associar planos de saúde à operadoras
  </li>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-817'>SM-817</a>] -         Criar Prescrição de Enfermagem.
  </li>
</ul>
