<h3 class="titulo-versao">Versão 2.43.11 (14/03/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1835'>SM-1835</a>] -         Adicionar flag de Possui Genérico na ativação de medicamentos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.43.11 (14/03/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1837'>SM-1837</a>] -         Problema com data da ultima evolução no plano de feridas do Planserv
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.43.9 (09/03/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1833'>SM-1833</a>] -         Erro ao tentar modificar dados na tabela de tradução do plano de saúde
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.43.6 (09/03/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1827'>SM-1827</a>] -         Erro ao adicionar um item de regras de fluxo
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.43.5 (09/03/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1824'>SM-1824</a>] -         Sistema não permite gravar mais de um item mesmo com caráteres diferentes
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.43.4 (08/03/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1822'>SM-1822</a>] -         Modificar regras de menus do sistema.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.43.3 (08/03/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1821'>SM-1821</a>] -         Problema ao acessar uma avaliação médica pela listagem de usuários da enfermagem
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.43.2 (07/03/2018)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1820'>SM-1820</a>] -         Replicar as modificações do salvar solicitações de enfermagem para o Relatório Aditivo
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.43.1 (06/03/2018)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1819'>SM-1819</a>] -         Permitir redirecionar ao editar um plano de saúde
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 2.43.0 (06/03/2018)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1811'>SM-1811</a>] -         Criar fluxo de liberação da auditoria com base nas autorizações do CID
    </li>
</ul>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1815'>SM-1815</a>] -         Criar regras de fluxo para planos de saúde
    </li>
</ul>
