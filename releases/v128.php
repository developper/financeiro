<h3 class="titulo-versao">Versão 1.28.14 (23/06/2015)</h3>
<h2>        Sub-task
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-830'>SM-830</a>] -         Mudar a forma como é trazida a logo da empresa nos relatórios do sistema
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.28.13 (22/06/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-826'>SM-826</a>] -         Correção de Permissões no Relatório de Fluxo de Caixa Realizado
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.28.12 (20/06/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-825'>SM-825</a>] -         Remover informação duplicada no campo &quot;Status da Fatura&quot; em Evolução e Avaliação de enfermagem
  </li>
</ul>
<hr/>
<h3 class="titulo-versao">Versão 1.28.11 (19/06/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-824'>SM-824</a>] -         Utilizar campo DESCRIÇÃO ao invés de OBSERVAÇÃO no arquivo da Dominio Sistemas
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.28.10 (19/06/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-823'>SM-823</a>] -         Problema ao gerar fatura.
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.28.9 (19/06/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-822'>SM-822</a>] -         Acrescentar orçamento na rotina de Fatura/ Custo
  </li>

</ul>
<hr>
<h3 class="titulo-versao">Versão 1.28.8 (19/06/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-820'>SM-820</a>] -         Corrigir descontos e juros no arquivo de importação para a Dominio Sistemas
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.28.7 (17/06/2015)</h3>
<h2>        Bug
</h2>
<ul>
  <li>[<a href='https://mederi.atlassian.net/browse/SM-821'>SM-821</a>] -         Problema no orçamento inicial.
  </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.28.6 (17/06/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-812'>SM-812</a>] -         Ajustes nos arquivos de importação da Dominio Sistemas
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.28.5 (16/06/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-819'>SM-819</a>] -         Problema ao visualizar extrato bancário
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.28.4 (16/06/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-818'>SM-818</a>] -         Problema ao enviar equipamento, no módulo da logística.
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-818'>SM-818</a>] -         Problema ao enviar equipamento, no módulo da logística.
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.28.3 (15/06/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-816'>SM-816</a>] -         Relatório analítico de fluxo de caixa não traz nenhum resultado
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.28.2 (12/06/2015)</h3>
<h2>        Bug
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-814'>SM-814</a>] -         Problema ao salvar ficha de evolução médica.
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-814'>SM-814</a>] -         Problema ao salvar ficha de evolução médica.
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.28.1 (11/06/2015)</h3>
<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-813'>SM-813</a>] -         Modificar permissão para o cancelamento de  prescrições no módulo de enfermagem.
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-813'>SM-813</a>] -         Modificar permissão para o cancelamento de  prescrições no módulo de enfermagem.
	</li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.28.0 (09/06/2015)</h3>
<h2>        Sub-task
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-804'>SM-804</a>] -         Modificar Solicitações de Enfermagem.
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-805'>SM-805</a>] -         Atender uma solicitação de envio/recolhimento/susbituição de equipamento.
	</li>
</ul>

<h2>        Improvement
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-799'>SM-799</a>] -         Modificações na Ficha de Evolução Médica
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-800'>SM-800</a>] -         Editar da Ficha de Evolução Médica por 24 horas
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-801'>SM-801</a>] -         Ajustar os scores dos alertas de sinais vitais
	</li>
</ul>

<h2>        New Feature
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-803'>SM-803</a>] -         Modificar a rotina de equipamentos ativos para os pacientes no sistema.
	</li>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-806'>SM-806</a>] -         Permitir remover  equipamentos ativos.
	</li>
</ul>

<h2>        Task
</h2>
<ul>
	<li>[<a href='https://mederi.atlassian.net/browse/SM-808'>SM-808</a>] -         Relatório com os medicamentos prescritos nos últimos  3 meses na Unidade Feira.
	</li>
</ul>
