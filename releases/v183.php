<h3 class="titulo-versao">Versão 1.83.10 (31/10/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1268'>SM-1268</a>] -         Gerar solicitação de recolhimento quando o paciente for modificado para Inativo.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.83.9 (31/10/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1267'>SM-1267</a>] -         Melhorias visuais no relatório de pedidos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.83.8 (27/10/2016)</h3>

<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1261'>SM-1261</a>] -         Remover pop-up de confirmação de permanência da tela em solicitações de enfermagem na hora de salvar .
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.83.7 (26/10/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1259'>SM-1259</a>] -         Excluir pacientes de teste do relatório epidemiológico
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.83.5 (25/10/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1255'>SM-1255</a>] -         Melhorias no relatório sintético de pedidos
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.83.4 (25/10/2016)</h3>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1254'>SM-1254</a>] -         Melhorias no relatório de saídas
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.83.3 (25/10/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1253'>SM-1253</a>] -         Erro na troca de item na funcionalidade de Solicitações Pendentes na Auditoria.
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.83.1 (24/10/2016)</h3>
<h2>        Bug
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1251'>SM-1251</a>] -         Erro na cron de indexação das tabelas temporárias
    </li>
</ul>
<hr>
<h3 class="titulo-versao">Versão 1.83.0 (24/10/2016)</h3>
<h2>        New Feature
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1248'>SM-1248</a>] -         Relatório de Saída de Itens para paciente.
    </li>
</ul>
<h2>        Improvement
</h2>
<ul>
    <li>[<a href='https://mederi.atlassian.net/browse/SM-1250'>SM-1250</a>] -         Modificação na busca de solicitações de enfermagem para os itens que foram Trocados.
    </li>
</ul>