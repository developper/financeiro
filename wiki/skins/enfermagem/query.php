<?php
$id = session_id();
if(empty($id))
  session_start();

include_once('../db/config.php');
include_once('../utils/codigos.php');

function opcoes($like){
    echo "<table id='tabela-opcoes' class='mytable' width=100% >"; 
    echo "<thead><tr>"; 
    echo "<th><b>Nome</b></th>";
    echo "</tr></thead>";
    $like = anti_injection($like,'literal');
    $cor = false;
    $total = 0;
    $sql = "SELECT DISTINCT m.nome, m.descricao, m.idMaterial FROM material as m WHERE m.nome LIKE '%$like%' OR m.descricao LIKE '%$like%' ORDER BY m.nome, m.descricao";
//    echo $sql."<br/>";
    $result = mysql_query($sql) or trigger_error(mysql_error());
    while($row = mysql_fetch_array($result)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      if($cor) echo "<tr cod='{$row['idMaterial']}' >";
      else echo "<tr cod='{$row['idMaterial']}' class='odd'>";
      $cor = !$cor;
      echo "<td valign='top'>{$row['nome']} - {$row['descricao']}</td>";
      echo "</tr>";
    }
    echo "</table>";
    echo "<script type='text/javascript' >";
    
    echo " $('#tabela-opcoes tr').dblclick(function(){
	      $('#nome').val($(this).text());
	      $('#cod').val($(this).attr('cod'));
	      $('#qtd').focus();
	      $('#tabela-opcoes').html('');
	      $('#busca').val('');
	  });";
    echo "</script>";
}

function busca_itens($like,$tipo){
	$sql = "";
	$label_tipo = "";
	$like = anti_injection($like,'literal');
	switch($tipo){
		case 'kits':
			$label_tipo = "KITS";
			$sql = "SELECT k.nome as nome, k.idKit as cod FROM kitsmaterial as k WHERE k.nome LIKE '%$like%' ORDER BY k.nome";
		break;
		case 'mat':
			$label_tipo = "MATERIAIS";
			$sql = "SELECT concat(mat.nome,' ',mat.descricao) as nome, mat.idMaterial as cod FROM material as mat WHERE mat.nome LIKE '%$like%' OR mat.descricao LIKE '%like%' ORDER BY nome";
		break;
		case 'med':
			$label_tipo = "MEDICAMENTOS";
			$sql = "SELECT concat(med.principioAtivo,' ',com.nome) as nome, com.idNomeComercialMed as cod FROM medicacoes as med, nomecomercialmed as com WHERE (med.principioAtivo LIKE '%$like%' OR com.nome LIKE '%$like%') AND med.idMedicacoes = com.Medicacoes_idMedicacoes ORDER BY nome";
		break;
	}
	$result = mysql_query($sql);
	echo "<table class='mytable' width=100% id='lista-itens' ><thead><tr><th>{$label_tipo}</th></tr></thead>";
	while($row = mysql_fetch_array($result)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      echo "<tr cod='{$row['cod']}' ><td>{$row['nome']}</td></tr>";
	}
	echo "</table>";
	echo "<script type='text/javascript' >";
	echo " $('#lista-itens tr').dblclick(function(){
	      $('#nome').val($(this).text());
	      $('#cod').val($(this).attr('cod'));
	      $('#lista-itens').html('');
	      $('#busca-item').val('');
	      $(\"#formulario input[name='qtd']\").focus();
	  });";
	echo "</script>";
}

function opcoes_kits($like){
	$like = anti_injection($like,'literal');
	echo "<table class='mytable' width=100% id='lista-kits' ><thead><tr><th>KITS</th></tr></thead>";
	$sql = "SELECT * FROM kitsmaterial as k WHERE k.nome LIKE '%$like%' ORDER BY k.nome";
	$result = mysql_query($sql);
	while($row = mysql_fetch_array($result)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      $add = "<img align='right' class='edit-kit' kit='{$row['idKit']}' name='{$row['nome']}' cod_ref='{$row['COD_REF']}' src='../utils/add_16x16.png' title='Adicionar material ao kit' border='0'>";
      $rem = "<img align='right' class='rem-kit' kit='{$row['idKit']}' name='{$row['nome']}' src='../utils/delete_16x16.png' title='Excluir kit' border='0'>";
      echo "<tr><td>{$row['nome']} $rem $add</td></tr>";
	}
	echo "</table>";
	echo "<script type='text/javascript' >";
	echo "$('.edit-kit').click(function(){ 
		var nome = $(this).attr('name'); 
		var kit = $(this).attr('kit'); 
		var cod = $(this).attr('cod_ref');
		$('#h-nome-kit').text(nome); 
		$('#dialog-kit').attr('cod', cod); 
		$('#dialog-kit').attr('kit', kit); 
		$('#dialog-kit').dialog('open'); });";
	echo "$('.rem-kit').click(function(){ 
		var nome = $(this).attr('name'); 
		var kit = $(this).attr('kit'); 
		var obj = this;
		if(confirm('Deseja realmente excluir '+nome+'?')){
			$.post('query.php',{query: 'remover-kit', kit: kit},function(r){if(r=='1') $(obj).parent().parent().remove(); else alert(r);})
		}});";
	echo "</script>";
}

function novo_kit($kit){
	$kit = anti_injection($kit,'literal');
	$sql = "INSERT INTO kitsmaterial (nome) VALUES ('{$kit}')";
	$a = mysql_query($sql);
	//echo  mysql_insert_id();
	if($a){
		$r= mysql_insert_id();
		echo $r;
		savelog(mysql_escape_string(addslashes($sql)));
	
		 
		
	}
	else
		echo "ERRO: Tente mais tarde!";
}

function remover_kit($kit){
	$kit = anti_injection($kit,'numerico');
	$sql = "DELETE FROM kitsmaterial WHERE idKit = '{$kit}' ";
	$a = mysql_query($sql);
	if($a){
		savelog(mysql_escape_string(addslashes($sql)));
		echo "1";
	}
	else
		echo "ERRO: Tente mais tarde!";
}

function novo_item_kit($post){
	extract($post,EXTR_OVERWRITE);
	$kit = anti_injection($kit,'numerico');
	$cod = anti_injection($cod,'numerico');
	$qtd = anti_injection($qtd,'numerico');
	$sql = "INSERT INTO itens_kit (idKit, idMaterial, qtd) VALUES ('{$kit}', '{$cod}', '{$qtd}')";	
	$a = mysql_query($sql);
	
	
	if($a){
		savelog(mysql_escape_string(addslashes($sql)));
		lista_itens_kit($post);
	}
	else
		echo "-1";
}

function atualiza_cod_ref_kit($cod_tiss,$idKit){
	
	$sql = "UPDATE `kitsmaterial` SET `COD_REF` = '{$cod_tiss}' WHERE `kitsmaterial`.`idKit` ='{$idKit}'";
	$a = mysql_query($sql);
	if($a){
		savelog(mysql_escape_string(addslashes($sql)));		
	}
	else
		echo "-1";
}

function del_item_kit($post){
	extract($post,EXTR_OVERWRITE);
	$id = anti_injection($id,'numerico');
	$sql = "DELETE FROM itens_kit WHERE id = '{$id}' ";
	$a = mysql_query($sql);
	if($a){
		savelog(mysql_escape_string(addslashes($sql)));
		echo "1";
	}
	else
		echo "ERRO: Tente mais tarde!";
}

function lista_itens_kit($post){
	extract($post,EXTR_OVERWRITE);
	$sql = "SELECT i.id, i.qtd,i.idMaterial as cod,concat(b.principio,' (APR) ',b.apresentacao) as nome FROM `itens_kit` as i, `brasindice` as b WHERE i.idMaterial = b.numero_tiss AND i.idKit = {$kit}  ORDER BY `principio`, `apresentacao` ";
	$result = mysql_query($sql);
	echo "<table class='mytable' width=100% >";
	echo "<thead><tr>"; 
	echo "<th><b>Itens</b></th><th>Principal</th><th>Excluir</th>";
	echo "</tr></thead>";
	$checked="";
	
	while($row = mysql_fetch_array($result)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      if($cod_ref==$row['cod'])
      	$checked="checked='checked'";
      else
      	$checked="";
      
      echo "<tr><td>{$row['nome']}. Quantidade: {$row['qtd']}</td><td><input type='radio' class='cod_ref' name='cod_ref' kit='{$kit}' cod='{$row['cod']}' $checked /></td><td><img align='right' class='del-item-kit' item='{$row['id']}' src='../utils/delete_16x16.png' title='Remover item do kit' border='0'></td></tr>";
	}
	
	echo "</table>";
	echo "<script>$('.del-item-kit').click(function(){
		 var obj = \$(this).parent().parent();
		 \$.post('query.php',{query: 'del-item-kit', id: \$(this).attr('item') },function(r){if(r == 1) obj.remove(); else alert(r) });
		 return false;}
		 );
	$('input:radio[name=cod_ref]').click(function(){
	  
	  
	  var cod = \$(this).attr('cod');
	  var kit = \$(this).attr('kit');
	  
	  \$.post('query.php', {query: 'atualiza-cod-ref-kit', cod_tiss:cod,idkit:kit }, function(retorno){
			
		});  
	  
	  
  });
	</script>";
}

function salvar_solicitacao($post){
	extract($post,EXTR_OVERWRITE);
	$presc = anti_injection($presc,'numerico');
	$paciente = anti_injection($paciente,'numerico');
	$enfermeiro = $_SESSION["id_user"];
	$enfermeiro = anti_injection($enfermeiro,'numerico');
	if(isset($carater))
		$carater = anti_injection($carater,'numerico');
	else 
		$carater = 0;
	//	mysql_query("begin");
	$c = 0;
	$itens = explode("$",$dados);
	$status = 0;
	if(isset($emergencia)){
		$status = $emergencia;
	}
	if($presc==-1){
		$ano = substr(date('Y'),2,2);
		$chave_avulsa = ",".date("dm").$ano.date("is");
		$campo_chave = ",CHAVE_AVULSA";
	}else{
		$chave_avulsa = "";
		$campo_chave = "";
	}
	
	if($carater == "4"){
	foreach($itens as $item){
		$c++;
		$i = explode("#",str_replace("undefined","",$item));
		$i[0] = anti_injection($i[0],'numerico');
		$i[1] = anti_injection($i[1],'numerico');
		$i[2] = anti_injection($i[2],'numerico');
		if($i[2] == "2"){
			$sql = "SELECT bras.numero_tiss as cod, i.qtd as qtdmat FROM kitsmaterial as kit, brasindice as bras, itens_kit as i	WHERE kit.idKit = '{$i[0]}' AND kit.idKit = i.idKit AND bras.numero_tiss = i.idMaterial";
			$result = mysql_query($sql);
			if(!$result){ 
				echo "-1";
				//mysql_query("rollback");
				return;
			}
			
			while($mat = mysql_fetch_array($result)){
				$qtd = $i[1]*$mat['qtdmat'];
				$sql = "INSERT INTO solicitacoes (paciente,enfermeiro,cod,qtd,tipo,data,status,idPrescricao$campo_chave) VALUES ('$paciente','$enfermeiro','{$mat['cod']}','{$qtd}','1',now(),'4','{$presc}'{$chave_avulsa})";
				$r = mysql_query($sql);
				if(!$r){ 
					echo "-1";
					//mysql_query("rollback");
					return;
				}
				savelog(mysql_escape_string(addslashes($sql)));
			}
		} else {
			$sql = "INSERT INTO solicitacoes (paciente,enfermeiro,cod,qtd,tipo,data,status,idPrescricao$campo_chave) VALUES ('$paciente','$enfermeiro','{$i[0]}','{$i[1]}','{$i[2]}',now(),'4','{$presc}'{$chave_avulsa})";
			$r = mysql_query($sql);
			if(!$r){ 
				echo "-1";
				//mysql_query("rollback");
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}
	}else{
		
		foreach($itens as $item){
			$c++;
			$i = explode("#",str_replace("undefined","",$item));
			$i[0] = anti_injection($i[0],'numerico');
			$i[1] = anti_injection($i[1],'numerico');
			$i[2] = anti_injection($i[2],'numerico');
			if($i[2] == "2"){
				$sql = "SELECT bras.numero_tiss as cod, i.qtd as qtdmat FROM kitsmaterial as kit, brasindice as bras, itens_kit as i	WHERE kit.idKit = '{$i[0]}' AND kit.idKit = i.idKit AND bras.numero_tiss = i.idMaterial";
				$result = mysql_query($sql);
				if(!$result){
					echo "-1";
					//mysql_query("rollback");
					return;
				}
				while($mat = mysql_fetch_array($result)){
					$qtd = $i[1]*$mat['qtdmat'];
					$sql = "INSERT INTO solicitacoes (paciente,enfermeiro,cod,qtd,tipo,data,status,idPrescricao{$campo_chave},DATA_SOLICITACAO) VALUES ('$paciente','$enfermeiro','{$mat['cod']}','{$qtd}','1',now(),'{$status}','{$presc}'{$chave_avulsa},now())";
					$r = mysql_query($sql);
					if(!$r){
						echo "-1";
						//mysql_query("rollback");
						return;
					}
					savelog(mysql_escape_string(addslashes($sql)));
				}
			} else {
				$sql = "INSERT INTO solicitacoes (paciente,enfermeiro,cod,qtd,tipo,data,status,idPrescricao{$campo_chave},DATA_SOLICITACAO) VALUES ('$paciente','$enfermeiro','{$i[0]}','{$i[1]}','{$i[2]}',now(),'{$status}','{$presc}'{$chave_avulsa},now())";
				$r = mysql_query($sql);
				if(!$r){
					echo "-1";
					//mysql_query("rollback");
					return;
				}
				savelog(mysql_escape_string(addslashes($sql)));
			}
		}
		
	}
	
	if($aprazamentos != ""){
		$array_apraz = explode("$",$aprazamentos);
		foreach($array_apraz as $apraz){
			$a = explode("#",str_replace("undefined","",$apraz));
			$a[0] = anti_injection($a[0],'numerico');
			$a[1] = anti_injection($a[1],'literal');
			$sql = "UPDATE itens_prescricao SET aprazamento = '{$a['1']}' WHERE id = '{$a['0']}'";
			$r = mysql_query($sql);
			if(!$r){ 
				echo "-1";
				//mysql_query("rollback");
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}
	if($presc != "-1"){
		$sql = "UPDATE prescricoes SET lockp = '9999-12-31 23:59:59', solicitado = now() WHERE id = '$presc' ";
		$r = mysql_query($sql);
		if(!$r){ 
			echo "-1";
			//mysql_query("rollback");
			return;
		}
		savelog(mysql_escape_string(addslashes($sql)));
	}
	//mysql_query("commit"); 
	if($presc==-1)
		echo 1;
	else
		echo $presc;
}

function combo_busca_saida($tipo){
	$sql = "SELECT idClientes as cod, nome as nome FROM clientes ORDER BY nome";
	$t = -1;
	if($tipo == "med")
		$t = 0;
	else if($tipo == "mat")
		$t = 1;
	if($t != -1) $sql = "SELECT b.numero_tiss as cod, concat(b.principio,' - ',b.apresentacao) as nome FROM brasindice as b WHERE tipo = '{$t}' ORDER BY principio,apresentacao";
	$result = mysql_query($sql);
    echo "<p>";
    echo "<select name='codigo' style='background-color:transparent;' >";
    if($tipo == "paciente") echo "<option value='todos' selected>Todos</option>";
    while($row = mysql_fetch_array($result))
    	echo "<option value='". $row['cod'] ."'>". $row['nome'] ."</option>";     
    echo "</select>";

}

function buscar_saida($post){
  	extract($post,EXTR_OVERWRITE);
  	$aSelect = ""; $aGroupBy = "";
  	echo "<center><h1>Resultado</h1></center>";
    echo "<table class='mytable' width=100% >"; 
    echo "<thead><tr>";
    echo "<th><b>Paciente</b></th>";
    if($resultado == 'a'){
    	echo "<th><b>Data</b></th>";
    	$aSelect = ", s.data";
    	$aGroupBy = " s.data,";
    }
    echo "<th><b>Nome</b></th>";
    echo "<th><b>Segundo Nome</b></th>";
    echo "<th><b>Quantidade</b></th>";
    echo "</tr></thead>";
    $cor = false;
    $total = 0;
    $i = anti_injection(implode("-",array_reverse(explode("/",$inicio))),'literal');
    $f = anti_injection(implode("-",array_reverse(explode("/",$fim))),'literal');
    $cond = "s.idCliente = '$codigo'";
    if($codigo == "todos" ) $cond = "1";
    $sql = "SELECT p.nome as paciente, s.nome, s.segundoNome, sum(s.quantidade) as quantidade $aSelect FROM saida as s, clientes as p WHERE p.idClientes = s.idCliente AND $cond AND s.data BETWEEN '$i' AND '$f' GROUP BY $aGroupBy s.segundoNome, s.nome, p.nome ORDER BY p.nome, s.nome, s.segundoNome ASC";
    if($tipo == 'med' || $tipo == 'mat'){
    	$nomes = explode("#",$codigo);
    	$nomes[0] = anti_injection($nomes[0],'literal');
    	$nomes[1] = anti_injection($nomes[1],'literal');
    	$sql = "SELECT p.nome as paciente, s.nome, s.segundoNome, sum(s.quantidade) as quantidade $aSelect FROM saida as s, clientes as p WHERE p.idClientes = idCliente AND s.nome = '{$nomes[0]}' AND s.segundoNome = '{$nomes[1]}' AND s.data BETWEEN '$i' AND '$f' GROUP BY $aGroupBy s.segundoNome, s.nome, p.nome ORDER BY p.nome, s.nome, s.segundoNome ASC";
    }
    $result = mysql_query($sql) or trigger_error(mysql_error());
    $f = true;
    while($row = mysql_fetch_array($result)){ 
      $f = false;
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      if($cor) echo "<tr>";
      else echo "<tr class='odd'>"; 
      $cor = !$cor;
      echo "<td valign='top'>{$row['paciente']}</td>";
      if($resultado == 'a'){
      	$data = implode("/",array_reverse(explode("-",$row['data'])));
      	echo "<td valign='top'>$data</td>";  
      }
      echo "<td valign='top'>{$row['nome']}</td>";  
      echo "<td valign='top'>{$row['segundoNome']}</td>";  
      echo "<td valign='top'>{$row['quantidade']}</td>";
      echo "</tr>";      
    }
    echo "</table>";
    if($f) echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
 }
 
function refresh_lock($post){
	extract($post,EXTR_OVERWRITE);
	if(isset($p)){
		$p = anti_injection($p,'numerico');
		mysql_query("UPDATE prescricoes SET lockp = DATE_ADD(now(),INTERVAL 1 MINUTE) WHERE id = '$p' ");
	}
}

function release_lock($post){
	extract($post,EXTR_OVERWRITE);
	if(isset($p)){
		$p = anti_injection($p,'numerico');
		mysql_query("UPDATE prescricoes SET lockp = '0001-01-01 00:00:00.000' WHERE id = '$p' ");
	}
}

function buscar_prescricoes($post){

	extract($post,EXTR_OVERWRITE);
	if(!isset($op))
		$op = 0;

	if($i!='null' && $f!='null' && $paciente!='null'){
		$i = implode("-",array_reverse(explode("/",$inicio)));
		$f = implode("-",array_reverse(explode("/",$fim)));

		$_SESSION['i'] = $i;
		$_SESSION['f'] = $f;
		$_SESSION['paciente'] = $paciente;
		$limit = "";
		$conddata = " AND presc.data BETWEEN '{$i}' AND '{$f}'";

	}
	$condp = "1";



	if($paciente != "-1"){
		$condp = "presc.paciente = '{$paciente}'";
		$condp1= "c.idClientes = '{$paciente}'";
	}
	/*$sql = "SELECT presc.*, u.nome as medico, DATE_FORMAT(presc.data,'%d/%m/%Y - %T') as sdata, c.nome as paciente
	 FROM prescricoes as presc, clientes as c, usuarios as u
	WHERE {$condp} AND c.idClientes = presc.paciente AND presc.criador = u.idUsuarios
	AND data BETWEEN '{$i}' AND '{$f}' AND solicitado > data
	ORDER BY data DESC";*/

	$sql = "SELECT
	presc.*,
	u.nome AS medico,
	DATE_FORMAT(presc.data,'%d/%m/%Y - %T') AS sdata,
	DATE_FORMAT(presc.inicio,'%d/%m/%Y') AS inicio,
	DATE_FORMAT(presc.fim,'%d/%m/%Y') AS fim,
	UPPER(c.nome) AS paciente,
	c.idClientes as CodPaciente,
	(CASE presc.carater
	WHEN '1' THEN '<label style=\'color:red;\'><b>Emergencial</b></label>'
	WHEN '2' THEN '<label style=\'color:green;\'><b>Peri&oacute;dica</b></label>'
	WHEN '3' THEN '<label style=\'color:blue;\'><b>Aditiva</b></label>'
	WHEN '4' THEN '<label style=\'color:orange;\'><b>Avalia&ccedil;&atilde;o</b></label>'
	END) AS tipo,
	FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade
	FROM
	solicitacoes AS s LEFT JOIN
	prescricoes AS presc ON (s.idPrescricao = presc.id)  INNER JOIN
	clientes AS c ON (c.idClientes = presc.paciente) INNER JOIN
	usuarios AS u ON (presc.criador = u.idUsuarios)
	WHERE
	{$condp}
	{$conddata}
	ORDER BY
	presc.data DESC {$limit}";
	
	
	/*presc.data BETWEEN '{$i}' AND '{$f}' AND*/
	$sql2 = "SELECT
	UPPER(c.nome) AS paciente,
	(CASE c.sexo
	WHEN '0' THEN 'Masculino'
	WHEN '1' THEN 'Feminino'
	END) AS sexo,
	FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
	e.nome as empresa,
	c.`nascimento`,
	p.nome as Convenio,
	NUM_MATRICULA_CONVENIO
	FROM
	clientes AS c LEFT JOIN
	empresas AS e ON (e.id = c.empresa) INNER JOIN
	planosdesaude as p ON (p.id = c.convenio)
	WHERE
	{$condp1}
	ORDER BY
	c.nome DESC LIMIT 1";

	$result = mysql_query($sql);
			$result2 = mysql_query($sql2);
			echo "<table width=100% style='border:2px dashed;'>";
				while($pessoa = mysql_fetch_array($result2)){
				foreach($pessoa AS $chave => $valor) {
					$pessoa[$chave] = stripslashes($valor);

				}
				echo "<br/><tr>";
				echo "<td colspan='2'><label style='font-size:14px;color:#8B0000'><b><center> INFORMA&Ccedil;&Otilde;ES DO PACIENTE </center></b></label></td>";
					echo "</tr>";

					echo "<tr style='background-color:#EEEEEE;'>";
					echo "<td width='70%'><label><b>PACIENTE </b></label></td>";
					echo "<td><label><b>SEXO </b></label></td>";
					echo "</tr>";
					echo "<tr>";
					echo "<td>{$pessoa['paciente']}</td>";
					echo "<td>{$pessoa['sexo']}</td>";
					echo "</tr>";

					echo "<tr style='background-color:#EEEEEE;'>";
					echo "<td width='70%'><label><b>IDADE </b></label></td>";
					echo "<td><label><b>UNIDADE REGIONAL </b></label></td>";
					echo "</tr>";
					echo "<tr>";
					echo "<td>".join("/",array_reverse(explode("-",$pessoa['nascimento']))).' ('.$pessoa['idade']." anos)</td>";
					echo "<td>{$pessoa['empresa']}</td>";
					echo "</tr>";

					echo "<tr style='background-color:#EEEEEE;'>";
					echo "<td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>";
					echo "<td><label><b>MATR&Iacute;CULA </b></label></td>";
					echo "</tr>";
					echo "<tr>";
					echo "<td>".strtoupper($pessoa['Convenio'])."</td>";
					echo "<td>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
					echo "</tr>";
			}
			echo "</table><br/>";

					$flag = false;
					//echo "<button name='antigas' value='-1'>Nova</button><br/>";

					echo $row['paciente'];
					echo "<table class='mytable' width=100% >";
					echo "<thead><tr>";
					//echo "<th><b>Paciente</b></th>";
					echo "<th><b>Tipo</b></th>";
					echo "<th><b>Data</b></th>";
					echo "<th><b>Profissional</b></th>";
					echo "<th><b>Per&iacute;odo</b></th>";
					echo "<th colspan='3'><b>Op&ccedil;&otilde;es</b></th>";
					$n = 0;
					echo "</tr></thead>";
					
					while($row = mysql_fetch_array($result)){
					if($n++%2==0)
					$cor = '#E9F4F8';
					else
					$cor = '#FFFFFF';
					foreach($row AS $key => $value) {
					$row[$key] = stripslashes($value);
					}
					$flag = true;
					$d = "<a href='?view=detalhes&p={$row['id']}'><img src='../utils/details_16x16.png' title='Detalhes' border='0' class='print-presc'></a>";

					


echo "<tr bgcolor={$cor}>
<td>".utf8_decode($row['tipo'])."</td>
<td>{$row['sdata']}</td>
<td>{$row['medico']}</td>
<td>{$row['inicio']} AT&Eacute; {$row['fim']}</td>
<td>{$d} </td>
<td>{$p} </td>
<!--<td>{$d}</td>-->
</tr>";
					}
echo "</table>";

}

switch($_POST['query']){
  case "del-item-kit":
  	del_item_kit($_POST);
  break;
  case "novo-item-kit":
  	novo_item_kit($_POST);
  break;
  case "lista-itens-kit":
  	lista_itens_kit($_POST);
  break;
  case "novo-kit":
    novo_kit($_POST['kit']);
  break;
  case "remover-kit":
    remover_kit($_POST['kit']);
  break;  
  case "opcoes-kits":
    opcoes_kits($_POST['like']);
  break;
  case "busca-itens":
    busca_itens($_POST['like'],$_POST['tipo']);
  break;
  case "opcoes-itens":
    opcoes($_POST['like']);
  break;
  case "prescricoes-antigas":
  	prescricoes_antigas($_POST['p']);
  break;
  case "salvar-solicitacao":
    salvar_solicitacao($_POST);
  break;
  case "combo-busca-saida":
    combo_busca_saida($_POST['tipo']);
  break;
  case "busca-saida":
  	buscar_saida($_POST);
  break;
  case "refresh-lock":
  	refresh_lock($_POST);
  break;
  case "release-lock":
  	release_lock($_POST);
  break;
  case "atualiza-cod-ref-kit":
  	atualiza_cod_ref_kit($_POST['cod_tiss'],$_POST['idkit']);
  	break;
  case "buscar-prescricoes":
  		buscar_prescricoes($_POST);
  break;
}

?>