$(function($){
	
  function ExitPageConfirmer(message) {
  	this.message = message;
  	this.needToConfirm = false;
  	var myself = this;
  	window.onbeforeunload = function() {
  		if (myself.needToConfirm) {
  			return myself.message;
  		}
  	}
   }

  $("#teste").click(function(){
	  $.post("query.php", {query: "atualiza-cod-ref-kit", cod_tiss: cod,idkit:kit }, function(retorno){
			alert("Atualizado com Sucesso!");
		});  
  });
  
  
  var exitPage = new ExitPageConfirmer('Deseja realmente sair dessa página? os dados não salvos serão perdidos!');	
	
  $('.numerico').keyup(function(e){
    this.value = this.value.replace(/\D/g,'');
  });
	
//  $( "button, input:submit").button();
   
  $("input[name='nome']").attr("readonly","readonly");
  $("input[name='apr']").attr("readonly","readonly");
  $(".data").datepicker({
	defaultDate: "+1w",
	changeMonth: true,
	numberOfMonths: 1
  });
  $(".hora").timeEntry({show24Hours: true, defaultTime: '08:00'});
  $(".tabela-kits,.tabela-med,.tabela-mat,.tabela-serv").hide();
  $("#tipos").buttonset();
   
  var dates = $( "#inicio, #fim" ).datepicker({
	defaultDate: "+1w",
	changeMonth: true,
	numberOfMonths: 3,
	onSelect: function( selectedDate ) {
		var option = this.id == "inicio" ? "minDate" : "maxDate",
		instance = $( this ).data( "datepicker" );
		date = $.datepicker.parseDate(
		instance.settings.dateFormat ||
		$.datepicker._defaults.dateFormat,
		selectedDate, instance.settings );
		dates.not( this ).datepicker( "option", option, date );
	}
  });
  
  $("input[name='antigas']").change(function(){
  	$("input[name='paciente']").val($(this).parent().parent().attr('paciente'));
  	$("input[name='label']").val($(this).parent().parent().attr('label'));
  });
  
  $("#iniciar-avulsa").click(function(){
  	if(!validar_campos("div-iniciar-avulsa"))
  		return false;
  });
  
  $(".iniciar-solicitacao").click(function(){
	 // alert($(".trPrescricao").attr('antiga'));
	  $(".formPresc").submit(function(){
		
		  //  $.post("?view=solicitacoes", {antigas: $(".trPrescricao").attr('antiga'), paciente: $(".trPrescricao").attr('paciente'),label:$(".trPrescricao").attr('label')});
	  });
  });
  
  $("select[name='pacientes']").change(function(){
  	$("input[name='npaciente']").val($("select[name='pacientes'] option:selected").html());
  });
   
   function atualiza_qtd(id,op){
   	var item = "#"+id;
   	var qtd = $(item).attr("qtd");
   	if(op == "+")
   	  $(item).attr("qtd",parseInt(qtd)+1);
   	else
   	  $(item).attr("qtd",parseInt(qtd)-1);
   	if($(item).attr("qtd") > 0){
   		$(item).find(".status").eq(0).html("<img src='../utils/processada_16x16.png' title='solicitada' />");
   	} else {
   		$(item).find(".status").eq(0).html("");
   	}
   }
   
   function refreshlock(){
   	$.post("query.php",{query: "refresh-lock", p: $("#solicitacao").attr("prescricao")});
   }
   
   setInterval(function(i){refreshlock();},120000);
   
   $("input[name='tipo-busca-saida']").change(function(){
	$("#combo-busca-saida").html("");
	$.post("query.php", {query: "combo-busca-saida", tipo: $(this).val() }, function(retorno){
		$("#combo-busca-saida").html(retorno);
	});  
  });
  
  $("#tipos input[name='tipos']").change(function(){
  	$("#formulario").attr('tipo',$(this).val());
  });
  
  $("#buscar-saida").click(function(){
  	var tipo = $("input[name='tipo-busca-saida']:checked").val(); var cod = $("select[name='codigo'] option:selected").val();
  	var inicio = $("input[name='inicio']").val(); var fim = $("input[name='fim']").val();
  	var resultado = $("input[name='resultado']:checked").val();
  	$.post("query.php",{query: "busca-saida", tipo: tipo, codigo: cod, inicio: inicio, fim: fim, resultado: resultado},function(r){
  		$("#resultado-busca").html(r);
  	});
  	return false;
  });
   
   $(".div-aviso").hide();
   
   function validar_campos(div){
    var ok = true;
    $("#"+div+" .OBG").each(function (i){
      if(this.value == ""){
		ok = false;
		this.style.border = "3px solid red";
      } else {
		this.style.border = "";
      }
    });
    $("#"+div+" .COMBO_OBG option:selected").each(function (i){
      if(this.value == "-1"){
		ok = false;
		$(this).parent().css({"border":"3px solid red"});
      } else {
		$(this).parent().css({"border":""});
      }
    });
    if(!ok){
      $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
      $("#"+div+" .div-aviso").show();
    } else {
      $("#"+div+" .aviso").text("");
      $("#"+div+" .div-aviso").hide();
    }
    return ok;
  }
  
//  $("#busca-item").keyup(function() {
//    if($(this).val().length > 2){
//		$.post("query.php", {query: "busca-itens", like: $(this).val(), tipo: $("#formulario").attr("tipo")}, 
//		function(lista){
//	  		$("#opcoes-kits").html(lista);
//		});
//    } else {
//      $("#opcoes-kits").html("");
//    }
//  });
  
//  $("#busca-item").autocomplete({
//			source: "/utils/busca_brasindice.php?tipo="+$("#formulario").attr("tipo"),
//			minLength: 3,
//			select: function( event, ui ) {
//	      			$('#nome').val(ui.item.value);
//				    $('#cod').val(ui.item.id);
//	      			$('#qtd').focus();
//	      			$('#busca').val('');
//			}
//  });
  
  $("input[name='busca']").autocomplete({
			source: function(request, response) {
                $.getJSON('/utils/busca_brasindice.php', {
                    term: request.term,
                    tipo: $("#formulario").attr("tipo")
                }, response);
            },
			minLength: 3,
			select: function( event, ui ) {
					$('#busca-item').val('');
	      			$('#nome').val(ui.item.value);
				    $('#cod').val(ui.item.id);
	      			$('#qtd').focus();
	      			$('#busca-item').val('');
	      			return false;
			},
			extraParams: {
       			tipo: 2
   			}
  });
  
  $("input[name='nome-kit']").keyup(function() {
    if($(this).val().length > 2){
		$.post("query.php", {query: "opcoes-kits", like: $(this).val()}, 
		function(lista){
	  		$("#div-lista-kits").html(lista);
		});
    } else {
      $("#div-lista-kits").html("");
    }
    $(".edit-kit").click(function(){
  			$("#dialog-kit").dialog("open");
  	});
  });
  
  $("#criar-novo-kit").click(function(){
  	var kit = $("input[name='nome-kit']").val();
  	if(kit != ""){
  		if (confirm("Deseja realmente criar o kit "+kit+"?")){
			$.post("query.php", {query: "novo-kit", kit: kit}, 
			function(r){
				if(isNaN(r))
	  				alert(r);
	  			else{
	  				//alert(r+"122");
	  				//alert('4566');	  	
	  				$('#h-nome-kit').text(kit); 
					$('#dialog-kit').attr('kit', r); 
					$('#dialog-kit').dialog('open');
	  			}
			});
  		}
  	}
    return false;
  });
   
  $(".add-kit,.add-med,.add-mat").click(function(){
  	var item = $(this).attr("item");
  	var tipo = $(this).attr("tipo");
  	$("#kits-"+item).show();
  	$("#med-"+item).show();
  	$("#mat-"+item).show();
  	$("#serv-"+item).show();
  	$("#formulario").attr("item",item);
  	$("#formulario").attr("tipo",tipo);
  	$("#b"+tipo).attr('checked','checked');
  	$("#b"+tipo).button('refresh');
  	$("#formulario").dialog("open");
  });
  
  $(".look-kit").toggle(
  function(){
  	var item = $(this).attr("item");
  	$("#kits-"+item).show();
  	$("#med-"+item).show();
  	$("#mat-"+item).show();
  	$("#serv-"+item).show();
  }, function(){
  	var item = $(this).attr("item");
  	$("#kits-"+item).hide();
  	$("#med-"+item).hide();
  	$("#mat-"+item).hide();
  	$("#serv-"+item).hide();
  });
  
  $("#formulario").dialog({
  	autoOpen: false, modal: true, width: 800, position: 'top',
  	open: function(event,ui){
  		$("input[name='busca']").val('');
  		$('#nome').val('');
	    $('#nome').attr('readonly','readonly');
	    $('#apr').val('');
	    $('#apr').attr('readonly','readonly');
	    $('#cod').val('');
	    $('#qtd').val('');
	    $("input[name='busca']").focus();
  	},
  	buttons: {
  		"Fechar" : function(){
  			var item = $("#formulario").attr("item");
  			if(item != "avulsa"){
  				$("#kits-"+item).hide();
  				$("#med-"+item).hide();
  				$("#mat-"+item).hide();
  				$("#serv-"+item).hide();
  			}
  			$(this).dialog("close");
  		}
  	}
  });
  
  $(".add-serv").click(function(){
  	var item = $(this).attr("item");
  	if(item != "avulsa"){
	  	$("#kits-"+item).show();
  		$("#med-"+item).show();
	  	$("#mat-"+item).show();
	  	$("#serv-"+item).show();
  	}
  	$("#dialog-servicos").attr("item",item);
  	$("#dialog-servicos").dialog("open");
  	return false;
  });
  
  $("#dialog-servicos").dialog({
  	autoOpen: false, modal: true, width: 800, position: 'top',
  	open: function(event,ui){
  		$("#qtd-serv").val("");
  		$("#add-serv").attr("item",$(this).attr("item"));
  	},
  	buttons: {
  		"Fechar" : function(){
  			var item = $("#dialog-servicos").attr("item");
  			if(item != "avulsa"){
  				$("#kits-"+item).hide();
  				$("#med-"+item).hide();
  				$("#mat-"+item).hide();
  				$("#serv-"+item).hide();
  			}
  			$(this).dialog("close");
  		}
  	}
  });
  
  $("#add-serv").click(function(){
  	if(validar_campos("dialog-servicos")){
  		var cod = $("select[name='servicos'] option:selected").val(); var nome = $("select[name='servicos'] option:selected").text();       
        var qtd = $("#qtd-serv").val();
        var tipo = "serv"; var item = $("#add-serv").attr("item");
        var dados = "nome='"+nome+"' tipo='"+tipo+"' cod='"+cod+"' qtd='"+qtd+"'";
        var linha = nome+". Quantidade: "+qtd;
        var botoes = "<img align='right' class='rem-item-solicitacao' item='"+item+"' src='../utils/delete_16x16.png' title='Remover' border='0'>";
  		$("#"+tipo+"-"+item).append("<tr bgcolor='#ffffff' class='dados' "+dados+" ><td>"+linha+botoes+"</td></tr>");
	    atualiza_qtd(item,'+');
        atualiza_rem_item();
        $("#qtd-serv").val("");
        $("select[name='servicos'] option").eq(0).attr("selected","selected");
        exitPage.needToConfirm = true;
  	}
  });
  
  $("#dialog-kit").dialog({
  	autoOpen: false, modal: true, width: 800, position: 'top',
  	close: function(event,ui){window.location = "?view=kits";},
  	
  	open: function(event,ui){
  		$("input[name='busca']").val('');
  		$('#nome').val('');
	    $('#nome').attr('readonly','readonly');
	    $('#apr').val('');
	    $('#apr').attr('readonly','readonly');
	    $('#cod').val('');
	  
	    $.post("query.php", { query: 'lista-itens-kit', kit: $(this).attr('kit'), cod_ref:$(this).attr('cod') }, function(r){
	    	$("#tabela-itens-kit").html(r);
	    });
  	},
  	buttons: {
  		"Fechar" : function(){
  			var item = $("#formulario").attr("item");
  			$("#kits-"+item).hide();
  			$(this).dialog("close");
  			
  		}
  	}
  });
    
  $("#outro").click(function(){
    $("input[name='nome']").attr("readonly","");
    $("input[name='apr']").attr("readonly","");
    $("#cod").val("-1");
    $("input[name='nome']").focus();
  });
  
  $("#add-item-kit").click(function(){
     if(validar_campos("dialog-kit")){
      var cod = $("#cod").val(); var qtd = $("#dialog-kit input[name='qtd']").val(); var kit = $("#dialog-kit").attr('kit');var cod_ref = $("#dialog-kit").attr('cod');
      $.post("query.php",{query: 'novo-item-kit', cod: cod, qtd: qtd, kit: kit,cod_ref:cod_ref},function(r){
      	if(r == '-1') alert('ERRO: Tente mais tarde!');
      	else $("#tabela-itens-kit").html(r);
      });    
      $("#cod").val("-1"); $("#dialog-kit input[name='nome']").val(""); 
      $("#dialog-kit input[name='apr']").val("");
      $("#dialog-kit input[name='qtd']").val("");
      $("#dialog-kit input[name='busca']").focus();
     }	
    return false;
  });
  
  function atualiza_rem_item(){
	  $(".rem-item-solicitacao").unbind("click",remove_item)
	  				.bind("click",remove_item);
  }
  
  function remove_item(){
  	if (confirm("Deseja realmente remover?")){
		$(this).parent().parent().remove();
		atualiza_qtd($(this).attr('item'),'-');
  	}
    return false;
  }
  
  $(".rem-item").bind("click",remove_item);
    
  $("#add-item").click(function(){
     if(validar_campos("formulario")){
      var cod = $("#cod").val(); var nome = $("#formulario input[name='nome']").val();       
      var item = $("#formulario").attr("item");
      var qtd = $("#formulario input[name='qtd']").val();
      var tipo = $("#formulario").attr("tipo");
      var dados = "nome='"+nome+"' tipo='"+tipo+"' cod='"+cod+"' qtd='"+qtd+"'";
      var linha = nome+". Quantidade: "+qtd;
      var botoes = "<img align='right' class='rem-item-solicitacao' item='"+item+"' src='../utils/delete_16x16.png' title='Remover' border='0'>";
      $("#"+tipo+"-"+item).append("<tr bgcolor='#ffffff' class='dados' "+dados+" ><td>"+linha+botoes+"</td></tr>");
      atualiza_qtd(item,'+');
      atualiza_rem_item();
      $("#cod").val("-1"); $("#formulario input[name='nome']").val(""); $("#formulario input[name='qtd']").val("");
      $('#busca-item').val('');
      exitPage.needToConfirm = true;
     }	
    return false;
  });
  
  $("#dialog-salvo").dialog({
  	autoOpen: false, modal: true, position: 'top',
  	close: function(event,ui){window.location = "?view=solicitacoes";},
  	buttons: {
  		"Fechar" : function(){
  			$(this).dialog("close");
  		}
  	}
  });
  
  $("#salvar-solicitacao").click(function(){
  	if(!validar_campos("tabela-solicitacoes")) return false;
  	if(confirm('Deseja realmente salvar a prescrição?')){
  		var itens = "";
   		var total = $(".dados").size() + $(".formula").size();
  		$(".dados").each(function(i){
  			var nome = $(this).attr("nome"); var tipo = -1;
  			switch($(this).attr("tipo")){
  				case 'serv':
  					tipo = 3;
  				break;
  				case 'kits':
  					tipo = 2;
  				break;
  				case 'mat':
  					tipo = 1;
  				break;
	  			case 'med':
	  				tipo = 0;
  				break;
  			}
	  		var qtd = $(this).attr("qtd"); var cod = $(this).attr("cod");
	  		var linha = cod+"#"+qtd+"#"+tipo;
			if(i<total - 1) linha += "$";
			itens += linha;
  		});
  		var erro_formula = false;
  		var total_formulas = $(".formula").size();
  		$(".formula").each(function(i){
  			var tipo = 12;
	  		var qtd = $(this).val(); var cod = $(this).attr("cod");
	  		if(qtd == "0" || qtd == ""){
	  			erro_formula = true;
	  		}
	  		var linha = cod+"#"+qtd+"#"+tipo;
			if(i<total_formulas - 1) linha += "$";
			itens += linha;
  		});
  		var aprazamentos = "";
  		var total_apraz = $(".aprazamento").size();
  		$(".aprazamento").each(function(i){
  			var item = $(this).attr("item");
  			var apraz = " ";
  			$(this).find("input").each(function(i){
  				apraz += " " + $(this).val();
  			});
  			var linha = item+"#"+apraz;
  			if(i<total_apraz - 1) linha += "$";
			aprazamentos += linha;
  		});
  		if(total > 0 && !erro_formula){
   		 	$.post("query.php",{query: "salvar-solicitacao", paciente: $("#solicitacao").attr("paciente"),carater: $("#solicitacao").attr("carater"), 
    						dados: itens, presc: $("#solicitacao").attr("prescricao"), aprazamentos: aprazamentos },function(r){
    				if(r != "-1"){
    					exitPage.needToConfirm = false;
    					$("#dialog-salvo input[name='prescricao']").val(r);
    					$("#dialog-salvo").dialog("open");
    				}
		    		else{ alert("ERRO: Tente mais tarde!"); }
    		});
    	} else {
    		var aviso = "ERRO: A solicitação está vazia.";
    		if(erro_formula){
    			aviso = "Fórmula não solicitada!";
    		}
    		alert(aviso);
    	}
  	}
  	return false;
  });
  
  $("#sair-sem-salvar").click(function(){
  	exitPage.needToConfirm = false;
  	$.post("query.php",{query: "release-lock", p: $("#solicitacao").attr("prescricao")}, function(r){window.location = '?view=solicitacoes';});
  });
  
  $("#salvar-avulsa").click(function(){
  	if(confirm('Deseja realmente salvar a prescrição?')){
  		var itens = "";
   		var total = $(".dados").size();
  		$(".dados").each(function(i){
  			var nome = $(this).attr("nome"); var tipo = -1;
  			switch($(this).attr("tipo")){
  				case 'serv':
  					tipo = 3;
  				break;
  				case 'kits':
  					tipo = 2;
  				break;
  				case 'mat':
  					tipo = 1;
  				break;
	  			case 'med':
	  				tipo = 0;
  				break;
  			}
	  		var qtd = $(this).attr("qtd"); var cod = $(this).attr("cod");
	  		var linha = cod+"#"+qtd+"#"+tipo;
			if(i<total - 1) linha += "$";
			itens += linha;
  		});
  		if(total > 0){
   		 	$.post("query.php",{query: "salvar-solicitacao", paciente: $("#avulsa").attr("paciente"), emergencia: $("#avulsa").attr("e"),   
    						dados: itens, presc: "-1" },function(r){
    				if(r=="1"){alert("Salvo com sucesso!"); window.location = "?view=avulsa";}
    				else{ alert("ERRO: "+r); }
    		});
    	} else {
    		alert("ERRO: A solicitação está vazia.");
    	}
  	}
  	return false;
  });
  
  $("#voltar").click(function(){
		 history.go(-1); 
	  });
	  
  
 
  $("#buscar").click(function(){
	   	var p = $("select[name='paciente'] option:selected").val();
	   	
	   	var i = $("#div-busca input[name='inicio']").val();
	   	var f = $("#div-busca input[name='fim']").val();
	   	
	   	$.post("query.php",{query: "buscar-prescricoes", paciente: p, inicio: i, fim: f},function(r){
	   		
	   		$("#div-resultado-busca").html(r);
	   	});
	   	return false;
	   });
  
});