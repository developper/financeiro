<?php
/*
 * @autor Ricardo T�ssio
 * 
 * 
 * 
 * */

$id = session_id();
if(empty($id))
  session_start();

include_once('../db/config.php');
include_once('../utils/codigos.php');

class Enfermagem{
  
  private function menu(){
  	echo "<p><a href='../adm/?adm=paciente'>Pacientes</a></p>";
    echo "<p><a href='?view=solicitacoes'>Solicita&ccedil;&otilde;es</a></p>";
    echo "<p><a href='?view=avulsa'>Solicita&ccedil;&atilde;o avulsa</a></p>";
    echo "<p><a href='?view=buscar'>Buscar Solicita&ccedil;&otilde;es </a></p>";
    echo "<p><a href='../medico/?view=buscar'>Buscar Prescri&ccedil;&otilde;es</a></p>";
    echo "<p><a href='?view=kits'>Kits</a></p>";
    echo "<p><a href='?view=buscas'>Envio materiais</a></p>";
    echo "<p><a href='../auditoria/?view=arquivos'>Acessar Prontu&aacute;rios</a></p>";
  }
  
  private function formulario(){
  	echo "<div id='formulario' title='Formul&aacute;rio' tipo='' classe='' >";
  	echo alerta();
    echo "<center><div id='tipos'>
		<input type='radio' id='bmed' name='tipos' value='med' /><label for='bmed'>Medicamentos</label>
		<input type='radio' id='bmat' name='tipos' value='mat' /><label for='bmat'>Materiais</label>
		<input type='radio' id='bkits' name='tipos' value='kits' /><label for='bkits'>Kits</label>
	</div></center>";
    echo "<p><b>Busca:</b><br/><input type='text' name='busca' id='busca-item' />";
    echo "<div id='opcoes-kits'></div>";
    echo "<input type='hidden' name='cod' id='cod' value='-1' />";
    echo "<p><b>Nome:</b><input type='text' name='nome' id='nome' class='OBG' />";
    echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
    echo "<button id='add-item' tipo='' >+</button></div>";
  }
  
  private function pacientes($id,$name,$todos){
    $result = mysql_query("SELECT * FROM clientes as c WHERE c.status = 4 AND (c.empresa = '{$_SESSION['empresa_user']}' OR '{$_SESSION['empresa_user']}' = '1' ) ORDER BY nome;");
    $var = "<p><select name='{$name}' style='background-color:transparent;width:400px;' class='COMBO_OBG' >";
    if($todos == "t") $var .= "<option value='todos' selected>Todos</option>";
    if($todos == "n") $var .= "<option value='-1' selected></option>";
    while($row = mysql_fetch_array($result))
    {
      if(($id <> NULL) && ($id == $row['idClientes']))
		$var .= "<option value='{$row['idClientes']}' selected>{$row['nome']}</option>";
      else
		$var .= "<option value='{$row['idClientes']}'>{$row['nome']}</option>";     
    }
    $var .= "</select>";
    return $var;
  }
  
  private function pacientes_presc(){
  
  	$medico = $_SESSION['id_user'];
  	$result = mysql_query("SELECT c.* FROM clientes as c, equipePaciente as e WHERE e.paciente = c.idClientes AND e.funcionario = {$medico} AND e.fim  IS NULL ORDER BY nome;");
  	$result = mysql_query("SELECT c.* FROM clientes as c WHERE c.status = 4 AND (c.empresa = '{$_SESSION['empresa_user']}' OR '{$_SESSION['empresa_user']}' = '1' ) ORDER BY nome;");
  
  	echo "<p><b>Localizar Paciente</b></p>";
  	echo "<select name='paciente' style='background-color:transparent;width:400px' id='selPaciente'>";
  	echo "<option value='-1'></option>";
  	while($row = mysql_fetch_array($result))
  	{
  		$paciente = ucwords(strtolower($row['nome']));
  		echo "<option value='{$row['idClientes']}'>{$paciente}</option>";
  }
  echo "</select>";
  }
  
  private function solicitacoes_pendentes(){
  	$sql = "SELECT 
			       presc.*,
			       u.nome,
			       CONCAT(DATE_FORMAT(presc.inicio,'%d/%m/%Y'),'  at�  ',DATE_FORMAT(presc.fim,'%d/%m/%Y')) AS sdata,
			       c.nome AS paciente,
			       idClientes 
			FROM 
			       prescricoes AS presc INNER JOIN  
			       clientes AS c ON (presc.paciente = c.idClientes) INNER JOIN 
			       usuarios AS u ON (presc.criador = u.idUsuarios)
			WHERE        
			       (c.empresa = '{$_SESSION['empresa_user']}' OR 
			       '{$_SESSION['empresa_user']}' = '1' ) AND
			       presc.lockp < NOW() 
			ORDER BY
			       DATA DESC";
	$result = mysql_query($sql);
	$flag = false;
	//echo "<input type='hidden' name='label' value='' /><input type='hidden' name='paciente' value='-1' />";
	echo "<table class='mytable' width=100% ><thead><tr><th>Paciente</th><th>Prescri&ccedil;&atilde;o</th><th>A&ccedil;&atilde;o</th></tr></thead><tfoot></tfoot>";
	$i=0;
	while($row = mysql_fetch_array($result)){
		
		echo "<input type='hidden' name='label' value='{$row['label']}' /><input type='hidden' name='paciente' value='{$row['paciente']}' />";
		if($z++%2==0)
    		$cor = '#E9F4F8';
    	else 
    		$cor = '#FFFFFF';
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      $checked = "";
//      if(!$flag) $checked = "checked";
//      $flag = true;
	  $profissional = ucwords(strtolower($row['nome']));
	  $paciente = ucwords(strtolower($row['paciente']));
	  if($row['Carater'] == 4){
      $label = "<h1>Paciente: {$paciente}</h1><h3>Profissional: {$profissional} - ".htmlentities($row['sdata'])."</h3>";
      echo "<tr label='{$label}' paciente='{$row['idClientes']}' antiga='{$row['id']}' class='trPrescricao' bgcolor='$cor'>";
      echo "<td><!--<input type='radio' name='antigas' value='{$row['id']}' $checked />-->{$row['paciente']}</td>";
      echo "<td>{$row['nome']} - ".utf8_encode($row['sdata'])."&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:orange'><b><i> ( AVALIA&Ccedil;&Atilde;o )</i></b></span></td>";
      echo "<td><p><a href='?view=solicitacoes&label={$label}&paciente={$row['idClientes']}&antigas={$row['id']}&carater={$row['Carater']}'><center><img src='../utils/solicitacao_enf.png' width='16' title='Iniciar Solicita��o'></center></a></td>"; 
      echo "</tr>";
	  }else{
	  	$label = "<h1>Paciente: {$paciente}</h1><h3>Profissional: {$profissional} - ".htmlentities($row['sdata'])."</h3>";
	  	echo "<tr label='{$label}' paciente='{$row['idClientes']}' antiga='{$row['id']}' class='trPrescricao' bgcolor='$cor'>";
	  	echo "<td><!--<input type='radio' name='antigas' value='{$row['id']}' $checked />-->{$row['paciente']}</td>";
	  	echo "<td>{$row['nome']} - ".utf8_encode($row['sdata'])."</td>";
	  	echo "<td><p><a href='?view=solicitacoes&label={$label}&paciente={$row['idClientes']}&antigas={$row['id']}&carater={$row['Carater']}'><center><img src='../utils/solicitacao_enf.png' width='16' title='Iniciar Solicita&ccedil;&atilde;o'></center></a></td>";
	  	echo "</tr>";
	  	
	  }
	}
	echo "</table>";
  }
  
  private function aprazamento($id,$freq,$cod,$cod1,$apraz1){
  	
  	$html = "<br/><b>Aprazamento: </b><div class='aprazamento' item='{$id}'>";
  	if($cod==$cod1)
  		$apraz1 = explode(" ",$apraz1);
  	else
  		$apraz1 ="";
  	
  	if($cod <= 0 || $cod == 15 || $cod == 16 || $cod == 18 || $cod>23){
  		
	  	for($i = 0; $i < $freq; $i++){
	  		$html .= "<input type='text' class='hora OBG' size='4' maxlength='5' />&nbsp;";
	  	}
	  	$html .= "<br>";
	  	for($j = 0; $j < $freq; $j++){
	  		$html .= "<b style='color:red;margin-left:17px;margin-right:17px;'>{$apraz1[$j]}</style></b><b style='padding:10px;'> </b>";
	  	}
  	}else{
  		$html = "";
  	}
  	if($freq == 0) $html = "";
  	else $html .= "</div>";
  	return $html;
  }
  
  private function load_table($prescricao,$paciente){
  	$prescricao = anti_injection($prescricao,'numerico');
  	$html = "<table class='mytable' width=100% ><thead><tr>";
    $html .= "<th><b>Itens prescritos</b></th>";
//    $sql = "SELECT v.via as nvia, f.frequencia as nfrequencia,i.*, i.inicio, 
//    		i.fim, DATE_FORMAT(i.aprazamento,'%H:%i') as apraz 
//    		FROM itens_prescricao as i LEFT OUTER JOIN frequencia as f ON (i.frequencia = f.id) LEFT OUTER JOIN viaadm as v ON (i.via = v.id)
//    		WHERE idPrescricao = '$prescricao' 
//    		ORDER BY tipo,inicio,aprazamento,nome,apresentacao ASC";
	
    /* SQL ORIGINAL COMENTADO POR RICARDO  05/07/2012 */
   //$sql = "SELECT i.*, f.qtd FROM itens_prescricao as i, frequencia as f WHERE idPrescricao = '$prescricao' AND f.id = i.frequencia ORDER BY tipo,inicio,aprazamento,nome,apresentacao ASC";
   $sql2="SELECT p.`id`, p.`Carater` FROM prescricoes p WHERE  p.id='{$prescricao}' ";
    $result2=mysql_query($sql2);
    $x= mysql_result($result2,0,1);
   
    if($x == 2 || $x == 5){
    	$y = $x;
    }
   
    $sql = "SELECT i.*, f.qtd,1 AS col FROM itens_prescricao AS i, frequencia AS f WHERE idPrescricao = (SELECT p.`id` FROM prescricoes p WHERE p.`Carater`='{$y}' AND p.id<>'{$prescricao}' AND p.`paciente`='{$paciente}' ORDER BY p.`id` DESC LIMIT 1 ) AND f.id = i.frequencia
   			 UNION
    		SELECT i.*, f.qtd,2 AS col FROM itens_prescricao AS i, frequencia AS f WHERE idPrescricao = '{$prescricao}' AND f.id = i.frequencia ORDER BY tipo,cod,col,inicio,aprazamento,nome,apresentacao ASC";

   /* if($x == 5)
    	{
    		$sql = "SELECT i.*, f.qtd,1 AS col FROM itens_prescricao AS i, frequencia AS f WHERE idPrescricao = (SELECT p.`id` FROM prescricoes p WHERE p.`Carater`= 5 AND p.id<>'{$prescricao}' AND p.`paciente`='{$paciente}' ORDER BY p.`id` DESC LIMIT 1 ) AND f.id = i.frequencia
    		UNION
    		SELECT i.*, f.qtd,2 AS col FROM itens_prescricao AS i, frequencia AS f WHERE idPrescricao = '{$prescricao}' AND f.id = i.frequencia ORDER BY tipo,cod,col,inicio,aprazamento,nome,apresentacao ASC";
    	}*/
    $result = mysql_query($sql);
   
//    $cor = false;
	$qtd = 0;
    $lastType = 0;
    $cont = 0;
    $html .= "</tr></thead>";
    //$row1 = array();
    
    while($row = mysql_fetch_array($result)){
    	
    	if($row['col']==1){
    		$row1 = $row;
    	}
    	
    	if($row['col']==2){
    	
	    	$i = implode("/",array_reverse(explode("-",$row['inicio'])));
	    	$f = implode("/",array_reverse(explode("-",$row['fim'])));
	    	
	    	
	//    	$aprazamento = "";
	//    	if($row['tipo'] <> 0) $aprazamento = $row['apraz'];
	      	$dados = " id='{$row['id']}' ";
	//      	$linha = "{$row['nome']} {$row['apresentacao']} {$row['nvia']} {$row['nfrequencia']} {$aprazamento} Per&iacute;odo: $i até $f OBS: {$row['obs']}";
			$html .= "<tr class='item-prescrito' qtd='0' bgcolor='#ebf3ff' $dados >";
			$botoes = "<img class='add-kit' tipo='kits' item='{$row['id']}' src='../utils/kit_16x16.png' title='Adicionar kit' border='0'>&nbsp;&nbsp;";
			$botoes .= "<img class='add-med' tipo='med' item='{$row['id']}' src='../utils/medicamento_16x16.png' title='Adicionar medicamento' border='0'>&nbsp;&nbsp;";
			$botoes .= "<img class='add-mat' tipo='mat' item='{$row['id']}' src='../utils/material_16x16.png' title='Adicionar material' border='0'>&nbsp;&nbsp;";
			$botoes .= "<img class='add-serv' tipo='serv' item='{$row['id']}' src='../utils/servicos_16x16.png' title='Servi&ccedil;os' border='0'>&nbsp;&nbsp;";
			$botoes .= "<img class='look-kit' item='{$row['id']}' src='../utils/look_16x16.png' title='Visualizar' border='0'>&nbsp;&nbsp;";
			$kits = "<table class='mytable tabela-kits' width=100% id='kits-{$row['id']}'><thead><tr><th>KITS</th></tr></thead></table>";
			$med = "<table class='mytable tabela-med' width=100% id='med-{$row['id']}'><thead><tr><th>Medicamentos</th></tr></thead></table>";
			$mat = "<table class='mytable tabela-mat' width=100% id='mat-{$row['id']}'><thead><tr><th>Materiais</th></tr></thead></table>";
			$serv = "<table class='mytable tabela-serv' width=100% id='serv-{$row['id']}'><thead><tr><th>Servi&ccedil;os</th></tr></thead></table>";
			
			$aprazamento = $this->aprazamento($row['id'],$row['qtd'],$row['cod'],$row1['cod'],$row1['aprazamento']);
			
			/* COMENTADO POR RICARDO PARA FAZER APARECER A BARRA DE KITS, MEDICAMENTOS E MATERIAL PARA F�RMULA*/
			if($row['tipo'] == '12'){
				
				$campo_formula = "<br/>Quantidade: <input type='text' size='4' value='' name='qtd-formula' cod='{$row['cod']}' tipo='12' class='numerico' >";
				//$html .= "<td><span class='status'></span>{$row['descricao']}{$aprazamento}<br/>{$campo_formula}</td>";
				$html .= "<td><span class='status'></span> $botoes {$row['descricao']}{$aprazamento}<br/>{$campo_formula}<br/>$kits<br/>$med<br/>$mat<br/>$serv</td>";
			} else { 
				$html .= "<td><span class='status'></span> $botoes {$row['descricao']}{$aprazamento}<br/>$kits<br/>$med<br/>$mat<br/>$serv</td>";
			}
			
			
	    	$html .= "</tr>";
	    	$cont++;
    	}
    
    }
    
    $html .= "</table>";
    
    return $html;    
  }
 
  
  private function prescricoes($label,$data,$antiga,$paciente,$carater){
    if($antiga == NULL){
      echo "<center><h1>Solicita&ccedil;&otilde;es Pendentes</h1></center>";
      echo "<form id='fromPresc' method='post' >";
      $this->solicitacoes_pendentes();
      //echo "<p><button id='iniciar-solicitacao'>Iniciar</button></form>";
      echo "</form>";
    } else {
      $antiga = anti_injection($antiga,'numerico');
      $r = mysql_query("SELECT lockp FROM prescricoes WHERE id = '$antiga' AND lockp < now() ");
      if(mysql_num_rows($r) == 0){
      	redireciona('index.php?view=solicitacoes');
      } else {
      	$r = mysql_query("UPDATE prescricoes SET lockp = DATE_ADD(now(),INTERVAL 1 MINUTE) WHERE id = '$antiga' ");
      }
      echo "<div id='dialog-salvo' title='Finalizada'>";
      echo "<center><h3>Prescri&ccedil;&atilde;o salva com sucesso!</h3></center>";
      echo "<form action='../medico/imprimir.php' method='post'><input type='hidden' value='-1' name='prescricao'><center><button>Imprimir</button></center></form>";
      echo "</div>";
      echo "<div id='solicitacao' paciente='$paciente' prescricao='$antiga' carater='$carater' >";
      echo "<center>$label</center>";
      $this->formulario();
      echo "<div id='dialog-servicos' Title='Adicionar Servi&ccedil;os'>";
  	  echo alerta();
  	  echo "<b>Servi&ccedil;o:</b>".combo_servicos("servicos");
  	  echo "<br/><b>Quantidade:</b> <input type='text' id='qtd-serv' name='qtd-serv' class='numerico OBG' size='3'/>";
      echo "<br/><button id='add-serv' tipo='' >+</button>";
  	  echo "</div><div id='tabela-solicitacoes'>";
  	  echo alerta()."<br/>";
      $tabela = $this->load_table($antiga,$paciente);
      echo $tabela;
      echo "<br/><button id='salvar-solicitacao' tipo='' style='float:left;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></form></div>";
      echo "<button id='sair-sem-salvar' tipo='' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></form></div>";
      
    }
  }
  
  private function kits(){
  	echo "<div id='dialog-kit' title='Lista Materiais' kit=''>";
  	echo "<div id='div-tabela-fantasias'></div>";
	echo alerta();
	echo "<center><h3 style='text-transform:uppercase' id='h-nome-kit'></h3></center>";
	echo "<p><b>Busca:</b><br/><input type='text' name='busca' id='busca' />";
    echo "<div id='opcoes-itens'></div>";
    echo "<input type='hidden' name='cod' id='cod' value='-1' />";
    echo "<p><b>Nome:</b><input type='text' name='nome' id='nome' class='OBG' size='50' />";
//    echo "<b>Descri&ccedil;&atilde;o:</b><input type='text' name='apr' id='apr' class='OBG' />";
    echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
    echo "<button id='add-item-kit' tipo='' >+</button>";
    echo "<div id='tabela-itens-kit'></div>";
  	echo "</div>";
  	echo "<center><h1>Kits de material</h1></center>";
  	echo "<input type='text' name='nome-kit' maxlength='50' /> <button id='criar-novo-kit'>Criar Novo</button>";
  	echo "<div id='div-lista-kits'></div>";
  }
  
  private function buscas(){
  	echo "<h1><center>Envio de materiais</center></h1>";
    echo "<b>Buscar por:</b><input type='radio' name='tipo-busca-saida' value='med' />Medicamento 
    	 <input type='radio' name='tipo-busca-saida' value='mat' />Material 
    	 <input type='radio' name='tipo-busca-saida' value='paciente' CHECKED />Paciente";
   	echo "<div id='combo-busca-saida'>".$this->pacientes(NULL,"codigo","t")."</div>";
    echo "<p><b>Resultado:</b><input type='radio' name='resultado' value='a' checked />Analítico<input type='radio' name='resultado' value='c' />Condensado";
    echo "<table><tr><td colspan='4'>";
    $hoje = date("j/n/Y");
    echo "</td></tr><tr><td><b>In&iacute;cio do per&iacute;odo:</b></td><td><input id='inicio' type='text' name='inicio' value='$hoje' maxlength='10' size='10' /></td>";
    echo "<td><b>Fim do per&iacute;odo:</b></td><td><input id='fim' type='text' name='fim' value='$hoje' maxlength='10' size='10' /></td></tr></table>";
    echo "<button id='buscar-saida' >Mostrar</button>";
    echo "<div id='resultado-busca' ></div>";
  }
  
  private function avulsa($paciente,$nome,$e){
  	echo "<h1><center>Solicita&ccedil;&atilde;o Avulsa</center></h1>";
  	if($paciente == NULL){
  		echo "<div id='div-iniciar-avulsa'>";
  		echo alerta();
  		echo "<form method='post' >";
  		echo "<p><b>Paciente:</b>".$this->pacientes(NULL,"pacientes","n");
  		echo "<input type='hidden' name='npaciente' value='' />";
  		echo "<input type='checkbox' name='emergencia' value='2' />Emerg&ecirc;ncia";
  		echo "<p><button id='iniciar-avulsa'>Iniciar</button></form></div>";
  	} else {
  		$this->formulario();
  		echo "<div id='dialog-servicos' Title='Adicionar Servi&ccedil;os'>";
  		echo alerta();
  		echo "<b>Servi&ccedil;o:</b>".combo_servicos("servicos");
  		echo "<br/><b>Quantidade:</b> <input type='text' id='qtd-serv' name='qtd-serv' class='numerico OBG' size='3'/>";
    	echo "<br/><button id='add-serv' tipo='' item='avulsa' >+</button>";
  		echo "</div>";
  		echo "<div id='avulsa' paciente='$paciente' e='$e'>";
  		echo "<h3><center>$nome</center></h3>";
  		$botoes = "<img class='add-kit' tipo='kits' item='avulsa' src='../utils/kit_48x48.png' title='Adicionar kit' border='0'>&nbsp;&nbsp;";
		$botoes .= "<img class='add-med' tipo='med' item='avulsa' src='../utils/medicamento_48x48.png' title='Adicionar medicamento' border='0'>&nbsp;&nbsp;";
		$botoes .= "<img class='add-mat' tipo='mat' item='avulsa' src='../utils/material_48x48.png' title='Adicionar material' border='0'>&nbsp;&nbsp;";
		$botoes .= "<img class='add-serv' tipo='serv' item='avulsa' src='../utils/servicos_48x48.png' title='Servi&ccedil;os' border='0'>&nbsp;&nbsp;";
  		echo "<center>$botoes</center>";
  		$kits = "<table class='mytable' width=100% id='kits-avulsa'><thead><tr><th>KITS</th></tr></thead></table>";
		$med = "<table class='mytable' width=100% id='med-avulsa'><thead><tr><th>Medicamentos</th></tr></thead></table>";
		$mat = "<table class='mytable' width=100% id='mat-avulsa'><thead><tr><th>Materiais</th></tr></thead></table>";
		$serv = "<table class='mytable' width=100% id='serv-avulsa'><thead><tr><th>Servi&ccedil;os</th></tr></thead></table>";
		echo "$kits<br/>$med<br/>$mat<br/>$serv<br/>";
  		echo "<button id='salvar-avulsa'>Salvar</button></div>";
  	}
  }
  
  private function periodo(){
  	echo "<br/><fieldset style='width:375px;text-align:center;'><legend><b>Per&iacute;odo</b></legend>";
  	echo "DE".
  			"<input type='text' name='inicio' value='{$this->inicio}' maxlength='10' size='10' class='OBG data periodo' />".
  			" AT&Eacute; <input type='text' name='fim' value='{$this->fim}' maxlength='10' size='10' class='OBG data' /></fieldset>";
  }
  
 
  
  private function buscar_solicitacoes($paciente,$inicio,$fim){
  
  	//echo "<center><h1>Buscar Prescri&ccedil;&otilde;es</h1></center>";
  	echo "<fieldset style='margin:0 auto;'>";
  	echo "<div id='div-busca' >";
  
  	
  	echo alerta();
  	
  	echo "<div style='float:right;width:100%;'>";
  	echo "<center><h2 style='background-color:#8B0101;color:#ffffff;height:25px;vertical-align: middle;'><label style='vertical-align: middle;'>BUSCAR SOLICITA&Ccedil;&Otilde;ES</label></h2></center>";
  	echo $this->pacientes_presc();
  	echo $this->periodo();
  
  	echo "<p><button id='buscar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Buscar</span></button>";
  
  	
  	echo "</div>";
  
  	echo "</div>";
  	echo "</fieldset>";
  	echo "<div id='div-resultado-busca'></div>";
  }
  
 
  
  
  private function check(){
  	$html = "<table width='100%' border='0'>";
	  	$html .= "<thead>";
		  	$html .= "<tr>";
			  	$html .= "<th>Produto</th>";
			  	$html .= "<th></th>";
			  	$html .= "<th></th>";
		  	$html .= "</tr>";
	  	$html .= "</thead>";
  	$html .= "</table>";
  }
  
  
  //! Exibe os detalhes de um prescrição finalizada.
  private function detalhes($p){
  	echo "<center><h1>Detalhes das Solicita&ccedil&otilde;es</h1></center>";
  	echo "<table width=100% class='mytable'><thead><tr>";
  	echo "<th><b>Itens prescritos</b></th>";
  	echo "</tr></thead>";
  	/*$sql = "SELECT v.via as nvia, f.frequencia as nfrequencia,i.*,  i.inicio,
  	i.fim, DATE_FORMAT(i.aprazamento,'%H:%i') as apraz, u.unidade as um
  	FROM itens_prescricao as i LEFT OUTER JOIN frequencia as f ON (i.frequencia = f.id)
  	LEFT OUTER JOIN viaadm as v ON (i.via = v.id)
  	LEFT OUTER JOIN umedidas as u ON (i.umdose = u.id)
  	WHERE idPrescricao = '$p'
  	ORDER BY tipo,inicio,aprazamento,nome,apresentacao ASC";*/

$sql = "SELECT 
      s.enfermeiro,
      s.tipo,
      S.qtd,
      UPPER(c.nome),
      s.autorizado,
      s.enviado,
      s.cod,
      s.idSolicitacoes AS sol,
      s.status,
      DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
      f.formula AS produto 
FROM 
      solicitacoes AS s,
      formulas AS f,
      clientes AS c 
WHERE 
      s.idPrescricao = {$p} AND
      c.idclientes = s.paciente AND
      s.tipo = '12' AND
      
      f.id = s.cod 
UNION 
SELECT 
       s.enfermeiro,
       s.tipo,
       S.qtd,
       UPPER(c.nome),
       s.autorizado,
       s.enviado,
       s.cod,
       s.idSolicitacoes AS sol,
       s.status,
       DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
       CONCAT(b.principio,' - ',b.apresentacao) AS produto
FROM 
        solicitacoes AS s,
        brasindice AS b,
        clientes AS c 
WHERE 
        s.idPrescricao = {$p} AND
        c.idclientes = s.paciente AND
        s.tipo = '1' AND 
        
        b.numero_tiss = s.cod 
UNION 
SELECT 
        s.enfermeiro,
        s.tipo, 
        S.qtd,
        UPPER(c.nome), 
        s.autorizado, 
        s.enviado, 
        s.cod, 
        s.idSolicitacoes AS sol, 
        s.status, 
        DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA, 
        CONCAT(b.principio,' - ',b.apresentacao) AS produto 
FROM 
	solicitacoes AS s, 
	brasindice AS b, 
	clientes AS c 
WHERE 
	s.idPrescricao = {$p} AND 
	c.idclientes = s.paciente AND 
	s.tipo = '0' AND 
	
	b.numero_tiss = s.cod 
ORDER BY 
	tipo, 
	produto, 
	autorizado, 
	enviado";
  	
  	
  	$result = mysql_query($sql);
  	$n=0;
  	while($row = mysql_fetch_array($result)){
  	if($n++%2==0)
  		$cor = '#E9F4F8';
  		else
  		$cor = '#FFFFFF';
  
  		$tipo = $row['tipo'];
  		$i = implode("/",array_reverse(explode("-",$row['inicio'])));
  		$f = implode("/",array_reverse(explode("-",$row['fim'])));
  		$aprazamento = "";
  		if($row['tipo'] != "0") $aprazamento = $row['apraz'];
  		$linha = "";
  		if($row['tipo'] != "-1"){
  		$aba = $this->abas[$row['tipo']];
  		$dose = "";
  		
  		$linha = "<b>Item:</b> {$row['produto']} <b>QTD:</b> {$row['qtd']}";
  	}
  	
  	echo "<tr bgcolor={$cor} >";
  	echo "<td>$linha</td>";
  	echo "</tr>";
  }
  echo "</table>";
  echo "<span style='float:left;'><form action='imprimir.php' target='_blank' method='post'><input type='hidden' value='{$p}' name='prescricao'>
  <button class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Imprimir</span></button>
  </form></span>";
  echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
  
  }
  
  
  
  
  public function view($v){
    switch ($v) {
      case "menu":
          $this->menu();
          break;
      case "solicitacoes":
	  	$l = NULL;
	  	$antiga = NULL;
	  	$data = date("d/m/Y");
	  	$pantiga = -1;
	  	$paciente = -1;
	  	$carater='';
	  	if(isset($_REQUEST['label'])) $l = $_REQUEST['label'];
	    if(isset($_REQUEST["data"])) $data = $_REQUEST["data"];
	    if(isset($_REQUEST["antigas"])) $antiga = $_REQUEST["antigas"];
	    if(isset($_REQUEST["paciente"])) $paciente = $_REQUEST["paciente"];
	    if(isset($_REQUEST["carater"])) $carater = $_REQUEST["carater"];
        $this->prescricoes($l,$data,$antiga,$paciente,$carater);
      break;  
      case "kits":
      	$this->kits();
      break;
      case "buscas":
      	$this->buscas();
      break;
      case "detalhes":
      	$this->detalhes($_REQUEST['p']);
      	break;
      case "avulsa":
      	$emergencia = 0;
	  	if(isset($_POST['emergencia'])) $emergencia = $_POST['emergencia'];
      	$paciente = NULL;
      	if(isset($_POST['pacientes'])) $paciente = $_POST['pacientes'];
      	$npaciente = NULL;
      	if(isset($_POST['npaciente'])) $npaciente = ucwords(strtolower($_POST['npaciente']));
      	$this->avulsa($paciente,$npaciente,$emergencia);
      break;
      case "buscar":
      	$paciente = NULL;
      	if(isset($_POST['pacientes'])) $paciente = $_POST['pacientes'];
      	$inicio = NULL;
      	if(isset($_POST['pacientes'])) $inicio = $_POST['inicio'];
      	$fim = NULL;
      	if(isset($_POST['pacientes'])) $fim = $_POST['fim'];
      	
      	$this->buscar_solicitacoes($paciente,$inicio,$fim);
      	break;
      case "checklist":      	
      	$this->checklist($paciente,$npaciente,$emergencia);
      	break;
    }
    
  }
}

?>