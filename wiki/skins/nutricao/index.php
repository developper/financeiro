<?php
include_once('../validar.php');
include_once('nutricao.php');

if(!function_exists('redireciona')){
	function redireciona($link){
		if ($link==-1){
			echo" <script>history.go(-1);</script>";
		}else{
			echo" <script>document.location.href='$link'</script>";
		}
	}
}

include($_SERVER['DOCUMENT_ROOT'].'/cabecalho.php');
?>

<?php
include($_SERVER['DOCUMENT_ROOT'].'/cabecalho_fim.php');
?>
</div>
<div id="content">
<div id="right">
<?php
   $view = "menu";
   if(isset($_GET["view"])) $view = $_GET["view"];
  if((!validar_tipo("modnutri") && !(validar_tipo("modenf") || validar_tipo("modlog")) && ($view == "buscar" || $view == "detalhes"))){
	 redireciona('/inicio.php');
   }
   $m = new Nutricionistas;
   $m->view($view);   
?>
</div>
<div id="left">
<?php include($_SERVER['DOCUMENT_ROOT'].'/painel_login.php'); ?>
<div class="box">
  <ul>
   <!-- <li><a href='?view=prescricoes'>Prescri&ccedil;&otilde;es</a></li> -->
   <li><a href='?view=buscar'>Prescri&ccedil;&otilde;es</a></li>
   
  </ul>
</div>
<div class="box">
<div id='calculadora'></div>
</div>
</div>
</div>
<script src="nutricao.js" type="text/javascript"></script>
</body>
</html>
