<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/validar.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/alertas.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/cabecalho.php');
?>
<script type="text/javascript">
$(function($){
	function refresh_msg(){
  		$.post("/query.php",{query: "get-msg"},function(r){
  			if(r=='-1') alert('ERRO: Tente mais tarde!');
  			else {
				$("#caixa-msg").html(r);
				atualiza_apaga_msg();
			  	atualiza_ler_msg();
  			}
  	 });
  	}
	setInterval(function(i){refresh_msg();},120000);
	$("input[name='antigas']").change(function(){
  		$("input[name='paciente']").val($(this).parent().parent().attr('paciente'));
  		$("input[name='label']").val($(this).parent().parent().attr('label'));
  	});
});
</script>
<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/cabecalho_fim.php');
?>

<div id="content">
<div id="right">
<?php
echo "<div id='dialog-atualiza' cod='' title='Atualiza&ccedil;&otilde;es do sistema.' >";
echo "<div id='div-atualiza' ></div>";
echo "</div>";
echo "<div id='dialog-show-msg' title='Mensagem'>
	  <center><b><span></span></b></center>
	  <div id='corpo-msg' style='border:1px solid red' ></div>
	  </div>";
echo "<div id='alertas' >";
alerta_msgs();
if(validar_tipo("modenf"))
  alerta_enfermagem();
if(validar_tipo("modfin"))
  alerta_financeiro();
if(validar_tipo("modlog"))
  alerta_logistica();
echo "</div>";
?>
</div>
<div id="left">
<?php include($_SERVER['DOCUMENT_ROOT'].'/painel_login.php'); ?>
</div>
</div>
</body>
</html>
