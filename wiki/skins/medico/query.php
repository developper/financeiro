<?php
$id = session_id();
if(empty($id))
  session_start();

include_once('../db/config.php');
include_once('../utils/codigos.php');
include_once("../utils/class.phpmailer.php");

//! Funcão não utilizada. Referente ao modelo anterior ao uso do brasindice.
/*! Mantive a função pois existiu uma tendência dos clientes em retornar para o formato antes do brasindice*/
function opcoes($like,$tipo){
    echo "<table id='tabela-opcoes' class='mytable' width=100% >"; 
    echo "<thead><tr>"; 
    echo "<th><b>Princ&iacute;pio Ativo</b></th>";
    echo "<th><b>Apresenta&ccedil;&atilde;o</b></th>";
    echo "<th><b>Estoque</b></th>";
    echo "</tr></thead>";
    $cor = false;
    $total = 0;
    $sql = "SELECT DISTINCT med.idMedicacoes, med.principioAtivo, com.nome, com.estoque, com.idNomeComercialMed FROM medicacoes as med, nomecomercialmed as com LEFT OUTER JOIN nomesfantasia as f ON f.apr = com.idNomeComercialMed WHERE (med.principioAtivo LIKE '%$like%' OR com.nome LIKE '%$like%' OR f.fantasia LIKE '%$like%') AND med.idMedicacoes = com.Medicacoes_idMedicacoes AND com.classe = '$tipo' ORDER BY med.principioAtivo";
//    echo $sql."<br/>";
    $result = mysql_query($sql) or trigger_error(mysql_error());
    while($row = mysql_fetch_array($result)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      if($cor) echo "<tr pnome='{$row['principioAtivo']}' snome='{$row['nome']}' cod='{$row['idNomeComercialMed']}' >";
      else echo "<tr pnome='{$row['principioAtivo']}' snome='{$row['nome']}' cod='{$row['idNomeComercialMed']}' class='odd'>";
      $cor = !$cor;
      echo "<td valign='top'>{$row['principioAtivo']}</td>";
      echo "<td valign='top'>{$row['nome']}</td>";
      echo "<td valign='top'>{$row['estoque']}</td>";
      echo "</tr>";
    }
    echo "</table>";
    echo "<script type='text/javascript' >";
    
    echo " $('#tabela-opcoes tr').dblclick(function(){
	      $('#nome').val($(this).attr('pnome'));
	      $('#nome').attr('readonly','readonly');
	      $('#apr').val($(this).attr('snome'));
	      $('#apr').attr('readonly','readonly');
	      $('#cod').val($(this).attr('cod'));
	      $('#tabela-opcoes').html('');
	      $('#posologia').focus();
	      $('#medicamento').val('');
	  });";
    echo "</script>";
}

function prescricoes_antigas($p,$tipo){
	$sql = "SELECT presc.*, UPPER(u.nome) as nome, DATE_FORMAT(presc.data,'%d/%m/%Y - %T') as sdata, DATE_FORMAT(presc.inicio, '%d/%m/%y') as sinicio, DATE_FORMAT(presc.fim,'%d/%m/%y') as sfim, (CASE presc.carater WHEN 1 THEN  'Emerg�ncia' WHEN 2
THEN  'Peri�dica' WHEN 3 THEN  'Aditiva' ELSE  'NOT' END) as tipo FROM prescricoes as presc, clientes as c, usuarios as u 
			WHERE presc.paciente = '{$p}' AND c.idClientes = presc.paciente AND presc.criador = u.idUsuarios ORDER BY data DESC";
	$result = mysql_query($sql);
	$flag = false;
	echo "<table width='100%' >";
	echo "<tr>";
	echo "<td>";
	echo "<input type='radio' name='antigas' value='-1' checked /><b>Nova</b><br/>";
	echo "</td>";
	echo "</tr>";
	$cor = true;
	if($tipo==2){
		while($row = mysql_fetch_array($result)){
			foreach($row AS $key => $value) {
				$row[$key] = stripslashes($value);
			}
			if($cor)
				echo "<tr style='background-color:#EDEEE0;'>";
			else
				echo "<tr >";
			$cor = !$cor;
			$flag = true;
			echo "<td><input type='radio' name='antigas' value='{$row['id']}' /><b>{$row['sdata']}</b><b style='color:green'> ( {$row['nome']} DE: {$row['sinicio']} AT&Eacute; {$row['sfim']})</b></td>";
			
			echo "</tr>";
	}
	}
	echo "</table>";
}

//! Finaliza uma prescrição
function finalizar_prescricao($post){
	
	extract($post,EXTR_OVERWRITE);
	mysql_query("begin");
	$criador = $_SESSION["id_user"];
	$i = implode("-",array_reverse(explode("/",$inicio)));
	$f = implode("-",array_reverse(explode("/",$fim)));
	
	
	$sql = "INSERT INTO prescricoes (paciente,criador,data,lockp,inicio,fim,Carater,ID_CAPMEDICA) VALUES ('{$paciente}','{$criador}',now(),now(),'{$i}','{$f}','{$tipo_presc}','{$id_cap}')";

	$sql1 = mysql_query($sql);
	$c = 0;
	$itens = explode("$",$dados);
	$id = mysql_insert_id();
	savelog(mysql_escape_string(addslashes($sql)));
	foreach($itens as $item){
		$c++;
		$i = explode("#",str_replace("undefined","",$item));
		$inicio = implode("-",array_reverse(explode("/",$i[6])));
		$fim = implode("-",array_reverse(explode("/",$i[7])));
		$codigo = $i[1];
		
		if($i[0] == "12"){
			$sql = "INSERT INTO formulas (`formula`) VALUES ('{$i[9]}')";
			$r = mysql_query($sql);
			if(!$r){ 
				echo "-1";
				mysql_query("rollback");
				return;
			}
			$codigo = mysql_insert_id(); 
			savelog(mysql_escape_string(addslashes($sql)));
		}
		$dose = $i[10];
		
		$sql = "INSERT INTO itens_prescricao (idPrescricao,tipo,cod,nome,via,frequencia,aprazamento,inicio,fim,obs,descricao,dose,umdose) ". 
						 "VALUES ('{$id}','{$i[0]}','{$codigo}','{$i[2]}','{$i[3]}','{$i[4]}','{$apr}','{$inicio}','{$fim}','{$i[8]}','{$i[9]}','{$dose}','{$i[11]}')";
		
		
		
		if($i[0] == "s"){
			$sql = "UPDATE itens_prescricao SET fim = curdate() WHERE id = '{$i[12]}'";
			$lockp1 = '9999-12-31';
			$r = mysql_query("INSERT INTO itens_prescricao (idPrescricao,tipo,cod,nome,via,frequencia,aprazamento,inicio,fim,obs,descricao,dose,umdose) ". 
						 "VALUES ('{$id}','-1','{$codigo}','{$i[2]}','{$i[3]}','{$i[4]}','-',curdate(),curdate(),'{$i[8]}','{$i[9]}','{$dose}','{$i[11]}')");
			if(!$r){ 
				echo "-1";
				mysql_query("rollback");
				return;
			}
		}
		$r = mysql_query($sql);
			
		if(!$r){ 
			echo "-1";
			mysql_query("rollback");
			return;
		}
		savelog(mysql_escape_string(addslashes($sql)));
	}
	if($sql1){ 
		mysql_query("commit");
		$arquivo = "salvas/".$criador."/".$paciente.".htm";
		if(file_exists($arquivo)) unlink($arquivo); 
		echo $id;
	}
	else{ mysql_query("rollback"); echo "-1";}
}


////////funcao aprazamento
function aprz($hora,$qtdi,$data1,$data2){
	//$hora = '08:15';
	//$qtdi=3;
	$x = $qtdi;
	$h = explode(":",$hora);
	if($h[0] >= 10)
		$hi = $h[0];
	else
		$hi = substr($h[0],1,1);
	$aux_h = $hi;
	//$data1 ='2012-01-01';
	//$data2 = '2012-01-03';

	$dias = ((strtotime($data2) - strtotime($data1))/86400);

	$tot = round(($dias*24));


	$ap = array();
	$ap_tudo = array();
	$j = 0;
	$a = 0;
	$x=0;
	for($i = 0; $i < $tot;){
		while($hi + $x <= 23){

			if($i != 0){
				$a = $hi+$qtdi;
				$hi = $hi+$qtdi;
			}else{
				$a = $aux_h;
				$hi = $hi;
			}
            $x=$qtdi;
            	
			$i += $qtdi;
			$hora_formatada .= $a.":".$h[1]." ";


		}
		array_push($ap_tudo,$hora_formatada);
		$hora_formatada = '';

		$j++;
		$hi = $hi-24;
	}

	return $ap_tudo;

}

///!!!! gravar emergencial
function finalizar_prescricao_emergencial($post){

	extract($post,EXTR_OVERWRITE);
	mysql_query("begin");
	$criador = $_SESSION["id_user"];
	$i = implode("-",array_reverse(explode("/",$inicio)));
	$f = implode("-",array_reverse(explode("/",$fim)));

	if($tipo_presc == 1 ){

		$lockp = '9999-12-31 23:59:59';
		$sql = "INSERT INTO prescricoes (paciente,criador,data,lockp,inicio,fim,Carater,solicitado) VALUES ('{$paciente}','{$criador}',now(),'{$lockp}','{$i}','{$f}','{$tipo_presc}',now())";


	}else{
		$sql = "INSERT INTO prescricoes (paciente,criador,data,lockp,inicio,fim,Carater) VALUES ('{$paciente}','{$criador}',now(),now(),'{$i}','{$f}','{$tipo_presc}')";
	}
	$sql1 = mysql_query($sql);
	$c = 0;
	$itens = explode("$",$dados);
	$id = mysql_insert_id();
	savelog(mysql_escape_string(addslashes($sql)));
	foreach($itens as $item){
		$c++;
		$i = explode("#",str_replace("undefined","",$item));
		$inicio = implode("-",array_reverse(explode("/",$i[6])));
		$fim = implode("-",array_reverse(explode("/",$i[7])));
		$codigo = $i[1];

		if($i[0] == "12"){
			$sql = "INSERT INTO formulas (`formula`) VALUES ('{$i[9]}')";
			$r = mysql_query($sql);
			if(!$r){
				echo "-1";
				mysql_query("rollback");
				return;
			}
			$codigo = mysql_insert_id();
			savelog(mysql_escape_string(addslashes($sql)));
		}
		
		$dose = $i[10];
		
		if( $codigo == $i[13] ){
			
			$sql = "Select HORA from frequencia where id = {$i[4]} limit 1";
			$res= mysql_query($sql);


			while($row = mysql_fetch_array($res)){
				$qtd_i = $row['HORA'];
			}
			
			$apz ='';
			$ap = aprz($i[5],$qtd_i,$inicio,$fim);
			
			
		
			$ct = 0;
			foreach($ap as $ap1){
				if($ct != 0)
					$apz .= "$".$ap1;
				else
					$apz .= $ap1;
				$ct++;
			}
			
		
		} else { $apz= '-';  }
			/*
			$hi = explode(":",$i[5]);
			while($hi[0] >=0 ){
				$pi = $hi[0]-(24/$qtd_i);
				$hi[0] = $pi;
			}
			$hi1 = $hi[0];
			$apr =" ";
			for($x=1; $x<=$qtd_i;$x++){
				if($x==1){
					$y = $hi1;
				}else{
					$y = $y+$qtd_i;
				}
				if($y>=24){
					$y=$y-24;
				}
				$apr .= " ".$y.":".$hi[1]. " ";
			}


		}else{
			$apr ="-";
		}*/

		$sql = "INSERT INTO itens_prescricao (idPrescricao,tipo,cod,nome,via,frequencia,aprazamento,inicio,fim,obs,descricao,dose,umdose) ".
				"VALUES ('{$id}','{$i[0]}','{$codigo}','{$i[2]}','{$i[3]}','{$i[4]}','{$apz}','{$inicio}','{$fim}','{$i[8]}','{$i[9]}','{$dose}','{$i[11]}')";

		if($tipo_presc == 1 ){

			$data1=$inicio;
			$data2=$fim;

			$horas = ((strtotime($data2)-strtotime($data1))/86400)*24;
			$qtd4 = @round($horas/$qtd_i);

			$sql2 = "INSERT INTO solicitacoes (paciente,enfermeiro,cod,qtd,autorizado,tipo,data,status,idPrescricao,data_auditado) VALUES ('{$paciente}','{$criador}','{$codigo}','{$qtd4}','{$qtd4}','{$i[14]}',curdate(),'2','{$id}',now())";
			$r = mysql_query($sql2);
		}

		if($i[0] == "s"){
			$sql = "UPDATE itens_prescricao SET fim = curdate() WHERE id = '{$i[12]}'";
			$lockp1 = '9999-12-31';
			$r = mysql_query("INSERT INTO itens_prescricao (idPrescricao,tipo,cod,nome,via,frequencia,aprazamento,inicio,fim,obs,descricao,dose,umdose) ".
					"VALUES ('{$id}','-1','{$codigo}','{$i[2]}','{$i[3]}','{$i[4]}','-',curdate(),curdate(),'{$i[8]}','{$i[9]}','{$dose}','{$i[11]}')");
			if(!$r){
				echo "-1";
				mysql_query("rollback");
				return;
			}
		}
		$r = mysql_query($sql);

		if(!$r){
			echo "-1";
			mysql_query("rollback");
			return;
		}
		savelog(mysql_escape_string(addslashes($sql)));
	}
	if($sql1){
		mysql_query("commit");
		$arquivo = "salvas/".$criador."/".$paciente.".htm";
		if(file_exists($arquivo)) unlink($arquivo);
		echo $id;
	}
	else{ mysql_query("rollback"); echo "-1";
	}
	
}
//! Grava Outro em Principio
function gravar_outro($post){
	extract($post,EXTR_OVERWRITE);
	
	#Grava o nome digitado em Principio caso n�o exista
	$ConsultPrinc = "Select count(*) as total from principioativo where principio ='{$nome}' collate utf8_unicode_ci";
	$res = mysql_query($ConsultPrinc);
	$tot =  mysql_fetch_array($res);
	
	if($tot['total']<=1){
		
		$sq = "INSERT INTO principioativo (id,principio,DescricaoMedico,AddMederi,status,usuario,data) Values(null,'{$nome}','',1,0,'{$_SESSION["id_user"]}',now())";
		$sql = "(Select MAX(numero_tiss)+1 as maior from brasindice LIMIT 1)";
		$tiss1 = mysql_query($sql);
		$tiss =  mysql_fetch_array($tiss1);
		$sq1 = "INSERT INTO brasindice (lab_desc,principio,apresentacao,numero_tiss,estoque,ValorMedio,tipo,AddMederi,usuario,data) Values('MEDERI','{$nome}','Produto Interno',{$tiss['maior']},0,0,0,1,'{$_SESSION["id_user"]}',now())";
		mysql_query($sq) or die("Erro!");
		mysql_query($sq1) or die("Erro 2!");
		
	}
	else{
		echo utf8_encode("Este Princ�pio j� existe!");
	}
}

function testar_outro($post){
	extract($post,EXTR_OVERWRITE);

	#Grava o nome digitado em Principio caso n�o exista
	$ConsultPrinc = "Select count(*) as total from principioativo where principio ='{$nome}' collate utf8_unicode_ci";
	$res = mysql_query($ConsultPrinc);
	$tot =  mysql_fetch_array($res);

	if($tot['total']>1){
		echo utf8_encode("Este Princ�pio j� existe!");
	}	

	
}


//! Retorna o resultado da busca por prescrições
function buscar_prescricoes($post){
	
	extract($post,EXTR_OVERWRITE);
	if(!isset($op))
		$op = 0;
		
	if($i!='null' && $f!='null' && $paciente!='null' && $op==0){
		$i = implode("-",array_reverse(explode("/",$inicio)));
		$f = implode("-",array_reverse(explode("/",$fim)));
		
		$_SESSION['i'] = $i;
		$_SESSION['f'] = $f;
		$_SESSION['paciente'] = $paciente;	
		$limit = "";
		$conddata = " AND presc.data BETWEEN '{$i}' AND '{$f}'";
	
	}elseif($paciente!=null || $paciente!=-1 && $op==1){
		unset($_SESSION['i']);
		unset($_SESSION['f']);
		
		$i = null;
		$f = null;
		if($paciente!='null'){
			unset($_SESSION['paciente']);
			$_SESSION['paciente'] = $paciente;
		}
		
		$paciente = $_SESSION['paciente'];
		
		$limit = " LIMIT 5";
		$conddata = "";
	}else{	
		$i = $_SESSION['i'];
		$f = $_SESSION['f'];
		$paciente = $_SESSION['paciente'];
		
		$conddata = " AND presc.data BETWEEN '{$i}' AND '{$f}' ";
		$limit = "";
	}
		
	$condp = "1";

	
	
	if($paciente != "-1"){
		$condp = "presc.paciente = '{$paciente}'";
		$condp1= "c.idClientes = '{$paciente}'";
	} 
	/*$sql = "SELECT presc.*, u.nome as medico, DATE_FORMAT(presc.data,'%d/%m/%Y - %T') as sdata, c.nome as paciente  
			FROM prescricoes as presc, clientes as c, usuarios as u 
			WHERE {$condp} AND c.idClientes = presc.paciente AND presc.criador = u.idUsuarios 
				  AND data BETWEEN '{$i}' AND '{$f}' AND solicitado > data
			ORDER BY data DESC";*/
	
	$sql = "SELECT 
				presc.*,
				u.nome AS medico,
				DATE_FORMAT(presc.data,'%d/%m/%Y - %T') AS sdata,
				DATE_FORMAT(presc.inicio,'%d/%m/%Y') AS inicio,
				DATE_FORMAT(presc.fim,'%d/%m/%Y') AS fim,
				UPPER(c.nome) AS paciente,
				c.idClientes as CodPaciente,				
				(CASE presc.carater 
				  WHEN '1' THEN '<label style=\'color:red;\'><b>Emergencial</b></label>'
				  WHEN '2' THEN '<label style=\'color:green;\'><b>Peri�dica</b></label>' 
				  WHEN '3' THEN '<label style=\'color:blue;\'><b>Aditiva</b></label>'
				  WHEN '4' THEN '<label style=\'color:orange;\'><b>Avalia��o</b></label>' 
				 WHEN '5' THEN '<label style=\'color:#B8860B;\'><b>Peri�dica Nutri��o</b></label>'
				  WHEN '6' THEN '<label style=\'color:#6959CD ;\'><b>Aditiva Nutri��o</b></label>'
				   END) AS tipo,	
				FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade
			FROM
				prescricoes AS presc INNER JOIN 
				clientes AS c ON (c.idClientes = presc.paciente) INNER JOIN
				usuarios AS u ON (presc.criador = u.idUsuarios)
			WHERE 
				{$condp} 
				{$conddata}	
			ORDER BY 
				presc.data DESC {$limit}";
				/*presc.data BETWEEN '{$i}' AND '{$f}' AND*/
	$sql2 = "SELECT 
				UPPER(c.nome) AS paciente,				
				(CASE c.sexo 
				  WHEN '0' THEN 'Masculino'
				  WHEN '1' THEN 'Feminino' 
				  END) AS sexo,					
				FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
				e.nome as empresa,
				c.`nascimento`,
				p.nome as Convenio,
				NUM_MATRICULA_CONVENIO 
			FROM
				clientes AS c LEFT JOIN 								
				empresas AS e ON (e.id = c.empresa) INNER JOIN
				planosdesaude as p ON (p.id = c.convenio)
			WHERE 
				{$condp1}
			ORDER BY 
				c.nome DESC LIMIT 1"; 
	
	$result = mysql_query($sql);
	$result2 = mysql_query($sql2);
	echo "<table width=100% style='border:2px dashed;'>";	
	while($pessoa = mysql_fetch_array($result2)){
		foreach($pessoa AS $chave => $valor) {
			$pessoa[$chave] = stripslashes($valor);
			
		}
		echo "<br/><tr>";
		echo "<td colspan='2'><label style='font-size:14px;color:#8B0000'><b><center> INFORMA&Ccedil;&Otilde;ES DO PACIENTE </center></b></label></td>";
		echo "</tr>";
		
		echo "<tr style='background-color:#EEEEEE;'>";
			echo "<td width='70%'><label><b>PACIENTE </b></label></td>";
			echo "<td><label><b>SEXO </b></label></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td>{$pessoa['paciente']}</td>";
			echo "<td>{$pessoa['sexo']}</td>";
		echo "</tr>";
		
		echo "<tr style='background-color:#EEEEEE;'>";
			echo "<td width='70%'><label><b>IDADE </b></label></td>";
			echo "<td><label><b>UNIDADE REGIONAL </b></label></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td>".join("/",array_reverse(explode("-",$pessoa['nascimento']))).' ('.$pessoa['idade']." anos)</td>";
			echo "<td>{$pessoa['empresa']}</td>";
		echo "</tr>";
		
		echo "<tr style='background-color:#EEEEEE;'>";
			echo "<td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>";
			echo "<td><label><b>MATR&Iacute;CULA </b></label></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td>".strtoupper($pessoa['Convenio'])."</td>";
			echo "<td>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
		echo "</tr>";
	}
	echo "</table><br/>";	
	
	$flag = false;
	//echo "<button name='antigas' value='-1'>Nova</button><br/>";
	
	echo $row['paciente'];
	echo "<table class='mytable' width=100% >"; 
    echo "<thead><tr>"; 
    //echo "<th><b>Paciente</b></th>";
    echo "<th><b>Tipo</b></th>";
    echo "<th><b>Data</b></th>";
    echo "<th><b>Profissional</b></th>";
    echo "<th><b>Per&iacute;odo</b></th>";
    echo "<th colspan='3'><b>Op&ccedil;&otilde;es</b></th>";
    $n = 0;
    echo "</tr></thead>";
	while($row = mysql_fetch_array($result)){ 
		if($n++%2==0)
			$cor = '#E9F4F8';
		else
			$cor = '#FFFFFF';
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      $flag = true;
      $d = "<a href='?view=detalhes&p={$row['id']}'><img src='../utils/details_16x16.png' title='Detalhes' border='0' class='print-presc'></a>";

      if($_SESSION['modmed']==1 || $_SESSION['adm_user']==1){
      	$p = "<a href='#'><img src='../utils/prescricao_16x16.png' title='Usar para nova Prescri&ccedil;&atilde;o' border='0' class='criar-presc' view='prescricoes' p='{$row['id']}' paciente='{$row['paciente']}' CodPaciente='{$row['CodPaciente']}' busca='true'></a>";
      }else{
      	$p='';
      }
      echo "<tr bgcolor={$cor}>
      			<td>".utf8_encode($row['tipo'])."</td>
      			<td>{$row['sdata']}</td>
      			<td>{$row['medico']}</td>
      			<td>{$row['inicio']} AT&Eacute; {$row['fim']}</td>
      			<td>{$d} </td>
      			<td>{$p} </td>
      			<!--<td>{$d} </td>-->
      		</tr>";
	}    
    echo "</table>";
}

//! Envia um email para as enfermeiras caso o médico solicite.
function enviar_email_aviso($post){
	extract($post,EXTR_OVERWRITE);
	$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
	$mail->IsSMTP(); // telling the class to use SMTP

	try {
	  $mail->Host       = "mail.yourdomain.com"; // SMTP server
//	  $mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
	  $mail->SMTPAuth   = true;                  // enable SMTP authentication
	  $mail->SMTPSecure = "tls";                 // sets the prefix to the servier
	  $mail->Host       = "mail.mederi.com.br";      // sets GMAIL as the SMTP server
	  $mail->Port       = 587;                   // set the SMTP port for the GMAIL server
	  $mail->Username   = "avisos@sistema.mederi.com.br";  // GMAIL username
	  $mail->Password   = "x3]Dt2:Gw1?Rp6[V";            // GMAIL password
//	  $mail->AddReplyTo('name@yourdomain.com', 'First Last');
	  $mail->AddAddress('enfermagem@mederi.com.br', 'Avisos Enfermagem Mederi');
	  $mail->SetFrom('avisos@sistema.mederi.com.br', 'Avisos de Sistema');
//	  $mail->AddReplyTo('name@yourdomain.com', 'First Last');
	  $mail->Subject = 'AVISO';
	  $mail->AltBody = 'Para ver essa mensagem, use um visualizador de email compatível com HTML'; // optional - MsgHTML will create an alternate automatically
	  $solicitante = ucwords(strtolower($_SESSION["nome_user"]));
	  $mail->MsgHTML("<b>Aviso de nova prescri&ccedil;&atilde;o</b><br/><b>Paciente: </b>{$p}<br/><b>Solicitante: </b>{$solicitante}.");
//	  $mail->AddAttachment('images/phpmailer.gif');      // attachment
//	  $mail->AddAttachment('images/phpmailer_mini.gif'); // attachment
	  $mail->Send();
	  echo "Message Sent OK<p></p>\n";
	} catch (phpmailerException $e) {
	  echo $e->errorMessage(); //Pretty error messages from PHPMailer
	} catch (Exception $e) {
	  echo $e->getMessage(); //Boring error messages from anything else!
	}
}

//! Salva uma prescrição.
function salvar_prescricao($post){
	extract($post,EXTR_OVERWRITE);
	$dir = "salvas/".$_SESSION["id_user"];
	if(!is_dir($dir)) mkdir($dir); 
	$filename = $dir."/".$paciente.".htm";
	$arq = fopen($filename, "w");
	$escreve = fwrite($arq, $p);
	fclose($arq);
	echo "Salvo com sucesso!";
}

switch($_POST['query']){
  case "opcoes-itens":
    opcoes($_POST['like'],$_POST['classe']);
    break;
  case "prescricoes-antigas":
  	prescricoes_antigas($_POST['p'],$_POST['tipo']);
    break;
  case "salvar-prescricao":
    salvar_prescricao($_POST);
    break;
  case "finalizar-prescricao":
    finalizar_prescricao($_POST);
    break;
    case "finalizar-prescricao-emergencial":
    	finalizar_prescricao_emergencial($_POST);
    	break;
  case "buscar-prescricoes":
  	buscar_prescricoes($_POST);
  	break;
  case "email-aviso":
  	enviar_email_aviso($_POST);
  	break;
  	case "nova-capmed":
  		nova_capmed($_POST);
  		break;
  	case "show-status-paciente":
  		show_status_paciente($_POST);
  		break;
  	case "salvar-status-paciente":
  		salvar_status_paciente($_POST);
  		break;
  	case "salvar-paciente":
  			salvar_paciente($_POST);
  			break;
  	case "editar-paciente":
  			editar_paciente($_POST);
  			break;
  	case "excluir-paciente":
  			excluir_paciente($_POST);
  			break;
  	case "nova-capmed":
  			nova_capmed($_POST);
  				break;
  	case "gravar-outro":
  			gravar_outro($_POST);
  				break;
  	case "testar-outro":
  			testar_outro($_POST);
  	break;
	case "consultar-itens-kit":
	    	consultar_itens_kit($_POST);
	break;
  				
}

function salvar_paciente($post){
	extract($post,EXTR_OVERWRITE);
	$nascimento = implode("-",array_reverse(explode("/",$nascimento)));
	$nome = anti_injection($nome,'literal'); $codigo = anti_injection($codigo,'literal'); $diagnostico = anti_injection($diagnostico,'literal');
	$local = anti_injection($local,'literal');$solicitante = anti_injection($solicitante,'literal');
	$sexo = anti_injection($sexo,'numerico');$csexo = anti_injection($csexo,'numerico');$rsexo = anti_injection($rsexo,'numerico');
	$resp = anti_injection($resp,'literal');$relresp = anti_injection($relresp,'literal');
	$cuid = anti_injection($cuid,'literal');$relcuid = anti_injection($relcuid,'literal');
	$endereco = anti_injection($endereco,'literal'); $especialidade = anti_injection($especialidade,'literal');
	$acomp = anti_injection($acomp,'literal'); $bairro = anti_injection($bairro,'literal'); $referencia = anti_injection($referencia,'literal');
	$sql = "INSERT INTO clientes (`nome`,`nascimento`,`sexo`,`codigo`,`convenio`,`dataInternacao`,`localInternacao`,`diagnostico`,
	`medicoSolicitante`,`empresa`,`responsavel`,`relresp`,`rsexo`,`cuidador`,`relcuid`,`csexo`,
	`espSolicitante`,`acompSolicitante`,`endereco`,`bairro`,`referencia`)
	VALUES ('$nome','$nascimento','$sexo','$codigo','$convenio',now(),'$local','$diagnostico','$solicitante','$empresa',
	'$resp','$relresp','$rsexo','$cuid','$relcuid','$csexo','$especialidade','$acomp','$endereco','$bairro','$referencia') ";
	if(mysql_query($sql)){
		savelog(mysql_escape_string(addslashes($sql)));
		echo 1;
	}
	else echo "erro";
}

function editar_paciente($post){
	extract($post,EXTR_OVERWRITE);
	$id = anti_injection($paciente,'numerico');
	$nascimento = implode("-",array_reverse(explode("/",$nascimento)));
	$nome = anti_injection($nome,'literal'); $codigo = anti_injection($codigo,'literal'); $diagnostico = anti_injection($diagnostico,'literal');
	$local = anti_injection($local,'literal');$solicitante = anti_injection($solicitante,'literal');
	$sexo = anti_injection($sexo,'numerico');$csexo = anti_injection($csexo,'numerico');$rsexo = anti_injection($rsexo,'numerico');
	$resp = anti_injection($resp,'literal');$relresp = anti_injection($relresp,'literal');
	$cuid = anti_injection($cuid,'literal');$relcuid = anti_injection($relcuid,'literal');
	$endereco = anti_injection($endereco,'literal'); $especialidade = anti_injection($especialidade,'literal');
	$acomp = anti_injection($acomp,'literal'); $bairro = anti_injection($bairro,'literal'); $referencia = anti_injection($referencia,'literal');
	$sql = "UPDATE clientes SET `nome` = '$nome',`nascimento` = '$nascimento',`sexo` = '$sexo',`codigo` = '$codigo',`convenio` = '$convenio',
	`localInternacao` = '$local',`diagnostico` = '$diagnostico',`medicoSolicitante` = '$solicitante',
	`empresa` = '$empresa', `responsavel` = '$resp', `relresp` = '$relresp', `rsexo` = '$rsexo',
	`cuidador` = '$cuid', `relcuid` = '$relcuid', `csexo` = '$csexo', `espSolicitante` = '$especialidade',`acompSolicitante` = '$acomp',
	`endereco` = '$endereco', `bairro` = '$bairro', `referencia` = '$referencia'
	WHERE `idClientes` = '$id'";
	if(mysql_query($sql)){
		savelog(mysql_escape_string(addslashes($sql)));
		echo 1;
	}
	else echo "erro";
}

function excluir_paciente($id){
}

function del_acompanhamento($post){
	extract($post,EXTR_OVERWRITE);
	$sql = "DELETE FROM equipePaciente WHERE id = '{$id}' ";
	if(mysql_query($sql)){
		savelog(mysql_escape_string(addslashes($sql)));
		echo 1;
	}
	else echo "erro";
}

function salvar_ficha($post,$file){
	extract($post,EXTR_OVERWRITE);
	$paciente = anti_injection($paciente,'numerico');
	$tipoficha = anti_injection($tipo,'numerico');
	$trava = 0;
	if(isset($post["trava"])) $trava = 1;
	$tipoficha = $tipo;
	$arquivo = $file["ficha"]["tmp_name"];
	$tamanho = $file["ficha"]["size"];
	$tipo = $file["ficha"]["type"];
	$nome = $file["ficha"]["name"];

	if ( $arquivo != "none" ){
		$fp = fopen($arquivo, "rb");
		$conteudo = fread($fp, $tamanho);
		$conteudo = addslashes($conteudo);
		fclose($fp);
		$sql = "INSERT INTO fichapaciente (`paciente`, `lockf`, `tipoficha`, `data`, `nome`, `tipo`, `size`, `content`) VALUES ('$paciente', '$trava' , '$tipoficha', now(), '$nome', '$tipo', '$tamanho', '$conteudo')";
		if(mysql_num_rows(mysql_query("SELECT 1 FROM fichapaciente WHERE `paciente` = '{$paciente}' AND `tipoficha` = '{$tipoficha}' "))){
			$sql = "UPDATE fichapaciente SET `lockf` = '{$trava}', `data` = now(), `nome` = '{$nome}', `tipo` = '{$tipo}', `size` = '{$tamanho}', `content` = '{$conteudo}' WHERE `paciente` = '{$paciente}' AND `tipoficha` = '{$tipoficha}' ";
		}
		$r = mysql_query($sql);
		if($r){
			redireciona("-1");
		}
	}
}

function show_ficha($p,$t){
	$p = anti_injection($p,'numerico');
	$t = anti_injection($t,'numerico');
	$sql = "SELECT tipo, content, nome FROM fichapaciente WHERE paciente='$p' AND tipoficha='$t' ";
	$result = mysql_query($sql);
	if($row = mysql_fetch_array($result)){
		header("Content-type: \"{$row['tipo']}\"");
		header("Content-Disposition: attachment; filename=\"{$row['nome']}\";" );
		print $row['content'];
	} else {
		echo "<script>alert('sem ficha');</script>";
		redireciona("?adm=paciente&act=show&id={$p}");
	}
}



function nova_capmed($post){
	extract($post,EXTR_OVERWRITE);
	$paciente = anti_injection($paciente,"numerico");
	$motivo = anti_injection($motivo,"literal");

	$cancer = anti_injection($cancer,"literal"); $psiquiatrico = anti_injection($psiquiatrico,"literal");
	$neurologica = anti_injection($neurologica,"literal");
	$galucoma = anti_injection($glaucoma,"literal"); $hepatopatia = anti_injection($hepatopatia,"literal"); $obesidade = anti_injection($obesidade,"literal");
	$cardiopatia = anti_injection($cardiopatia,"literal"); $dm = anti_injection($dm,"literal"); $reumatologica = anti_injection($reumatologica,"literal");
	$has = anti_injection($has,"literal"); $nefropatia = anti_injection($nefropatia,"literal"); $pneumopatia = anti_injection($pneumopatia,"literal");

	$palergiamed = anti_injection($palergiamed,"literal"); $alergiamed = anti_injection($alergiamed,"literal");
	$palergiaalim = anti_injection($palergiaalim,"literal");$alergiaalim = anti_injection($alergiaalim,"literal");

	$prelocohosp = anti_injection($prelocohosp,"numerico"); $prehigihosp = anti_injection($prehigihosp,"numerico");
	$preconshosp = anti_injection($preconshosp,"numerico");
	$prealimhosp = anti_injection($prealimhosp,"numerico"); $preulcerahosp = anti_injection($preulcerahosp,"numerico");
	$prelocodomic = anti_injection($prelocodomic,"numerico"); $prehigidomic = anti_injection($prehigidomic,"numerico");
	$preconsdomic = anti_injection($preconsdomic,"numerico");
	$prealimdomic = anti_injection($prealimdomic,"numerico"); $preulceradomic = anti_injection($preulceradomic,"numerico");
	$objlocomocao = anti_injection($objlocomocao,"numerico"); $objhigiene = anti_injection($objhigiene,"numerico");
	$objcons = anti_injection($objcons,"numerico");
	$objalimento = anti_injection($objalimento,"numerico"); $objulcera = anti_injection($objulcera,"numerico");

	$historicointernacao = anti_injection($historicointernacao,"literal"); $qtdanteriores = anti_injection($qtdanteriores,"numerico");
	$parecer = anti_injection($parecer,"numerico"); $justparecer = anti_injection($justparecer,"literal");

	mysql_query("begin");
	$sql = "INSERT INTO capmedica VALUES (NULL,'{$paciente}','{$motivo}','{$palergiamed}','{$palergiaalim}','{$alergiamed}','{$alergiaalim}',
	'{$prelocohosp}','{$prehigihosp}','{$preconshosp}','{$prealimhosp}','{$preulcerahosp}',
	'{$prelocodomic}','{$prehigidomic}','{$preconsdomic}','{$prealimdomic}','{$preulceradomic}',
	'{$objlocomocao}','{$objhigiene}','{$objcons}','{$objalimento}','{$objulcera}',
		'{$qtdanteriores}','{$historicointernacao}','{$parecer}','{$justparecer}')";
		$r = mysql_query($sql);
		if(!$r){
		mysql_query("rollback");
		echo "ERRO: Tente mais tarde!";
		return;
}
$idcapmedica = mysql_insert_id();
savelog(mysql_escape_string(addslashes($sql)));
		$sql = "INSERT INTO comorbidades VALUES ('{$idcapmedica}','{$cancer}','{$psiquiatrico}',
		'{$neurologica}','{$glaucoma}','{$hepatopatia}','{$obesidade}','{$cardiopatia}','{$dm}','{$reumatologica}',
		'{$has}','{$nefropatia}','{$pneumopatia}') ";
		$r = mysql_query($sql);
		if(!$r){
		mysql_query("rollback");
		echo "ERRO: Tente mais tarde!";
		return;
}
savelog(mysql_escape_string(addslashes($sql)));
		$problemas = explode("$",$probativos);
		foreach($problemas as $prob){
			$prob = anti_injection($prob,"literal");
			$sql = "INSERT INTO problemasativos VALUES ('{$idcapmedica}','{$prob}') ";
			$r = mysql_query($sql);
			if(!$r){
			mysql_query("rollback");
			echo "ERRO: Tente mais tarde!";
			return;
}
savelog(mysql_escape_string(addslashes($sql)));
}
$status = 0;
switch($parecer){
case "0":
$status = 1;
break;
case "1":
$status = 2;
break;
case "2":
$status = 2;
break;
}
$sql = "UPDATE clientes SET status = '{$status}' WHERE idClientes = '{$paciente}'";
$r = mysql_query($sql);
if(!$r){
mysql_query("rollback");
echo "ERRO: Tente mais tarde!";
return;
}
savelog(mysql_escape_string(addslashes($sql)));
mysql_query("commit");
echo "Salvo com sucesso!";
}

function show_status_paciente($post){
	extract($post,EXTR_OVERWRITE);
	$cod = anti_injection($cod,'numerico');
	$row = mysql_fetch_array ( mysql_query("SELECT nome, status FROM `clientes` WHERE `idClientes` = '{$cod}' "));
	$s = $row['status'];
	$p = ucwords(strtolower($row['nome']));
	$var = "<p><b>Status do paciente {$p}:</b><br />";
	$r = mysql_query("SELECT * FROM statuspaciente");
	while($row = mysql_fetch_array($r)){
		$var .= "<input type='radio' name='status' s='{$row['id']}'  ".(($row['id']=="{$s}")?"checked":"")." />{$row['status']}<br/>";
	}
	echo $var;
}

function salvar_status_paciente($post){
	extract($post,EXTR_OVERWRITE);
	$cod = anti_injection($cod,'numerico');
	$pstatus = anti_injection($pstatus,'numerico');
	$sql = "UPDATE clientes SET status = '{$pstatus}' WHERE idClientes = '{$cod}' ";
	$r = mysql_query($sql);
	if(!$r){
		echo "ERRO: tente mais tarde!";
		return;
	}
	echo "1";
}

function consultar_itens_kit($post){
	extract($post,EXTR_OVERWRITE);
$sql = "SELECT	
	concat(b.principio,' - ',b.apresentacao,'  Qtd ',ik.qtd) as itens
	,ik.idMaterial,b.principio,ik.qtd,b.tipo
	FROM
	kitsmaterial as k inner join
	itens_kit as ik ON (k.idKit = ik.idKit) inner join
	brasindice as b ON (ik.idMaterial = b.numero_tiss)
	
	WHERE
	k.idKit= {$cod}
	ORDER BY k.nome";
	
	$result = mysql_query($sql);
	////////////tipo pra kit ////////////
	$tipo=15;
	 ///$dados = "nome='"+nome+"' tipo='"+tipo+"' cod='"+cod+"' dose='"+qtd+"'frequencia='"+frequencia+"'um='"+um+"'inicio='"+inicio+"'fim='"+fim+"'obs='"+obs+"'";
	while ($row = mysql_fetch_array($result)) {
		$a[]= $row['cod'];
		$tipo_bras=$row['tipo'];
		
		//print $inicio;
		if($row['idMaterial']== $cod_ref){
			$nome= $row['principio'];
			
			$cod_b= $row['idMaterial'];
			$qtd_item=$row['qtd'];
			
			$linha= $nome.".  QTD: ".$qtd_item.". Dosagem: ".$qtd.". Frequncia: ".$nfrequencia.". Per&iacute;odo: ".$inicio." at&eacute; ".$fim.". OBS: ".$obs."  Hora:".$hora;
			$dados = "nome='".$nome."' tipo='".$tipo."' cod='".$cod_b."'dose='".$qtd."'frequencia='".$frequencia."'um='".$um."'inicio='".$inicio."'fim='".$fim."'obs='".$obs."'hora='".$hora."'cod_ref='".$cod_ref."'tipo_bras='".$tipo_bras."'";
			
			echo "<tr bgcolor='#ffffff' cod='{$cod}' ><td><span style='margin-left:15px;' class= 'dados' {$dados} > - {$linha}</span></td></tr>";
		}else{
			$cod_b= $row['idMaterial'];
			$nome= $row['principio'];
			$qtd_item=$row['qtd'];
			$dados = "nome='".$nome."' tipo='".$tipo."' cod='".$cod_b."'inicio='".$inicio."' fim='".$fim."'cod_ref='".$cod_ref."'tipo_bras='".$tipo_bras."'";
			$linha= $nome.".  QTD: ".$qtd_item.". Per&iacute;odo: ".$inicio." at&eacute; ".$fim;
		echo "<tr bgcolor='#ffffff' cod='{$cod}' ><td><span style='margin-left:15px;' class= 'dados' {$dados} > - {$linha}</span></td></tr>";	
		}
	}
	
}
?>