<?php

include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');

//! Classe para armazenar os dados de um paciente.
/*! utilizado na hora de exibir os detalhes */
class DadosPaciente{
	public $codigo = ""; public $nome = ""; public $nasc = ""; public $sexo = "0"; public $plano = NULL;
	public $diagnostico = ""; public $cid = ""; public $origem = "";
	public $solicitante = ""; public $especialidade = ""; public $acomp = "";
	public $cnome = ""; public $csexo = "0"; public $crel = "";
	public $rnome = ""; public $rsexo = "0"; public $rrel = "";
	public $endereco = ""; public $bairro = ""; public $referencia = ""; 
	public $empresa = NULL;
}
//! Classe para armazenar as informações de uma ficha de captação
class DadosFicha{
	public $usuario = ""; public $data = ""; public $pacienteid = ""; public $pacientenome = ""; public $motivo = ""; 
	public $alergiamed = ""; public $alergiaalim = ""; public $tipoalergiamed = ""; public $tipoalergiaalim = "";
	public $prelocohosp = "3"; public $prehigihosp = "2"; public $preconshosp = "3"; public $prealimhosp = "4"; public $preulcerahosp = "2";
	public $prelocodomic = "3"; public $prehigidomic = "2"; public $preconsdomic = "3"; public $prealimdomic = "4"; public $preulceradomic = "2";
	public $objlocomocao = "3"; public $objhigiene = "2"; public $objcons = "3"; public $objalimento = "4"; public $objulcera = "2";
	public $shosp = "14"; public $sdom = "14"; public $sobj = "14";
	
	public $qtdinternacoes = ""; public $historico = "";
	public $parecer = "0"; public $justificativa = "";
	
	public $cancer = ""; public $psiquiatrico = ""; public $neuro = ""; public $glaucoma = "";
	public $hepatopatia = ""; public $obesidade = ""; public $cardiopatia = ""; public $dm = "";
	public $reumatologica = ""; public $has = ""; public $nefropatia = ""; public $pneumopatia = "";
	
	public $ativo;
}

//! Classe para gerenciar os Pacientes.
class Paciente{
  
  //! Retorna a equipe que acompanha o paciente. Não utilizado.
  /*! Os clientes ainda não definiram se iriam utilizar esse recurso, ficou para depois */
  private function equipe($paciente,$tipo){
    $has = false;
    $result = mysql_query("SELECT e.*, u.nome FROM equipePaciente as e, usuarios as u WHERE e.funcionario = u.idUsuarios AND u.tipo = '$tipo' AND paciente = '$paciente' AND fim IS NULL ORDER BY nome");
    $cor = false;
    while($row = mysql_fetch_array($result))
    {
      $has = true;
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
      if($cor) echo "<tr cod='{$row['id']}' >";
      else echo "<tr cod='{$row['id']}' class='odd' >";
      $cor = !$cor;
      echo "<td>{$row['nome']}</td>";
      $inicio = implode("/",array_reverse(explode("-",$row['inicio'])));
      echo "<td>{$inicio}</td>";
      echo "<td>-</td>";
      echo "<td><button class='end' >Finalizar</button></td><td><button class='del' >Remover</button></td>";
      echo "</tr>";
    }
    if(!$has) echo "<tr><td colspan='5' ><center><b>Nenhuma equipe está acompanhando o paciente.</b></center></td></tr>";
  }
  
  //! Gera dropbox com as filiais.
  private function empresas($id,$extra = NULL){
  	$result = mysql_query("SELECT * FROM empresas AND id NOT IN (1, 3, 9, 10) ORDER BY nome");
  	$var = "<select name='empresa' style='background-color:transparent;' class='COMBO_OBG' {$extra}>";
  	$var .= "<option value='-1'></option>";
  	while($row = mysql_fetch_array($result))
  	{
  	  if(($id <> NULL) && ($id == $row['id']))
  	    $var .= "<option value='{$row['id']}' selected>{$row['nome']}</option>";
   	 else 
  	 	$var .= "<option value='{$row['id']}'>{$row['nome']}</option>";
  	}
  	$var .= "</select>";
  	return $var;
  }
  
  public function menu(){
    echo "<a href='?adm=paciente&act=listar'>Listar</a><br>";
    echo "<a href='?adm=paciente&act=novo'>Novo paciente</a><br>";
  }
  
  //! Preenche os dados de um paciente.
  private function preencheDados($id){
  	$id = anti_injection($id,"numerico");
    $result = mysql_query("SELECT c.*, p.id as plano, cid.codigo as codcid, cid.descricao as descid 
    					   FROM clientes as c 
    					   LEFT OUTER JOIN planosdesaude as p ON c.convenio = p.id  
    					   LEFT OUTER JOIN cid10 as cid ON c.diagnostico collate utf8_general_ci = cid.codigo collate utf8_general_ci  
    					   WHERE idClientes = '$id'");
    $dados = new DadosPaciente();
    echo $sql." ".mysql_error();
    while($row = mysql_fetch_array($result)){
    	$dados->codigo = $row['codigo']; $dados->nome = $row['nome']; $dados->sexo = $row['sexo']; $dados->plano = $row['plano'];
    	$dados->diagnostico = $row['descid']; $dados->cid = $row['codcid']; $dados->origem = $row['localInternacao'];
    	$dados->solicitante = $row['medicoSolicitante']; $dados->especialidade = $row['espSolicitante']; $dados->acomp = $row['acompSolicitante'];
    	$dados->cnome = $row['cuidador']; $dados->csexo = $row['csexo']; $dados->crel = $row['relcuid'];
    	$dados->rnome = $row['responsavel']; $dados->rsexo = $row['rsexo']; $dados->rrel = $row['relresp'];
    	$dados->endereco = $row['endereco']; $dados->bairro = $row['bairro']; $dados->referencia = $row['referencia'];
    	$dados->empresa = $row['empresa'];
    	$dados->nasc = implode("/",array_reverse(explode("-",$row['nascimento'])));  
    }
    return $dados;
  }
  
  //! Formulário para criação de um novo paciente.
  public function form($id = NULL,$visao = ''){
  	$dados = new DadosPaciente();
  	$modo = "salvar-paciente";
  	$titulo = "Novo paciente";
  	if($id != NULL){
  		$dados = $this->preencheDados($id);
  		$modo = "editar-paciente";
  		$titulo = "Editar Paciente";
  	}
  	$visao_sexo = '';
  	if($visao != ''){ 
  		$visao_sexo = 'disabled';
  		$titulo = "Detalhes Paciente";
  	}
  	echo "<input type='hidden' name='paciente' value='{$id}' />";
    echo "<center><h1>{$titulo}</h1></center>";
    echo "<div id='div-novo-paciente'>";
    echo alerta();
    echo "<table>";
    echo "<tr><tr><td><b>C&oacute;digo</b></td></tr>"; 
    echo "<tr><td><input class='OBG' type='text' name='codigo' value='{$dados->codigo}' {$visao} /></td></tr>";
    echo "<tr><td><b>Nome</b></td><td><b>Data de nascimento</b></td><td><b>Sexo</b></td></tr>";
    echo "<tr><td><input class='OBG' type='text' size=50 name='nome' value='{$dados->nome}' {$visao} /></td>"; 
    echo "<td><input class='data OBG' maxlength='10' size='10' type='text' name='nascimento' value='{$dados->nasc}' {$visao} /></td>";
    echo "<td><input type='radio' name='sexo' value='0' id='masc' ".(($dados->sexo=='0')?"checked":"")." {$visao_sexo} />Masculino";
    echo "<input type='radio' name='sexo' value='1' ".(($dados->sexo=='1')?"checked":"")." {$visao_sexo} />Feminino</td></tr>";
    echo "<tr><td><b>Conv&ecirc;nio</b></td></tr>";
    $r = mysql_query("SELECT p.* FROM planosdesaude as p");
    echo "<tr><td><select name='convenio' class='COMBO_OBG' style='background-color:transparent;' {$visao_sexo} >";
    echo "<option value='-1' selected ></option>";
    while($row = mysql_fetch_array($r)){
		foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
		if($row['id'] == $dados->plano) echo "<option value='{$row['id']}' selected >{$row['nome']}</option>";
    	else echo "<option value='{$row['id']}'>{$row['nome']}</option>";
    }
    echo "</select></td></tr>";
//    echo "<b> Data de cadastro:</b><input class='data' maxlength='10' size='10' type='text' name='internacao' />";
    echo "<tr><td><b>Diagn&oacute;stico</b></td></tr>";
    if($visao == '') echo "<tr><td><b>Busca:</b><input type='text' name='busca-diagnostico' id='busca-diagnostico' {$visao} /></td></tr>";
    echo "<tr><td><input class='OBG' type='text' size=50 name='diagnostico' readonly=readonly value='{$dados->diagnostico}' /></td>";
    echo "<td><input class='OBG' type='text' name='cid-diagnostico' readonly=readonly value='{$dados->cid}' /></td></tr>";
    echo "<tr><td><b>Unidade de Interna&ccedil;&atilde;o de Origem</b></td></tr>";
    echo "<tr><td><input class='OBG' type='text' size=50 name='local' id='local' value='{$dados->origem}' {$visao} /></td></tr>";
    echo "<tr><td><b>M&eacute;dico solicitante</b></td><td><b>Especialidade</b></td><td><b>Acompanhar&aacute; o paciente em domic&iacute;lio (S/N)?</b></td></tr>";
    echo "<tr><td><input type='text' size=50 name='solicitante' class='OBG' value='{$dados->solicitante}' {$visao} /></td>";
    echo "<td><input type='text' name='sol-especialidade' class='OBG' value='{$dados->especialidade}' {$visao} /></td>";
    echo "<td><input type='text' name='sol-acompanhamento' class='OBG boleano' value='{$dados->acomp}' {$visao} /></td></tr>";
    echo "<tr><td><b>Cuidador Familiar</b></td></tr>";
	echo "<tr><td>Nome</td><td>Sexo</td><td>Rela&ccedil;&atilde;o</td><tr>";
    echo "<tr><td><input type='text' size=50 name='cuidador' class='OBG' value='{$dados->cnome}' {$visao} /></td>";
    echo "<td><input type='radio' name='csexo' value='0' ".(($dados->csexo=='0')?"checked":"")." {$visao_sexo} />M";
    echo "<input type='radio' name='csexo' value='1' ".(($dados->csexo=='1')?"checked":"")." {$visao_sexo} />F</td>";
    echo "<td><input type='text' name='relcuid' class='OBG' value='{$dados->crel}' {$visao} /></td></tr>";
    echo "<tr><td><b>Respons&aacute;vel Familiar</b></td></tr>";
	echo "<tr><td>Nome</td><td>Sexo</td><td>Rela&ccedil;&atilde;o</td><tr>";
    echo "<tr><td><input type='text' size=50 name='responsavel' class='OBG' value='{$dados->rnome}' {$visao} /></td>";
    echo "<td><input type='radio' name='rsexo' value='0' ".(($dados->rsexo=='0')?"checked":"")." {$visao_sexo} />M";
    echo "<input type='radio' name='rsexo' value='1' ".(($dados->rsexo=='1')?"checked":"")." {$visao_sexo} />F</td>";
    echo "<td><input type='text' name='relresp' class='OBG' value='{$dados->rrel}' {$visao} /></td></tr>";
    echo "<tr><td><b>Endere&ccedil;o</b></td><td><b>Bairro</b></td><td><b>Ponto de Referência</b></td></tr>";
    echo "<tr><td><input type='text' size=50 name='endereco' class='OBG' value='{$dados->endereco}' {$visao} /></td>";
    echo "<td><input type='text' name='bairro' class='OBG' value='{$dados->bairro}' {$visao} /></td>";
    echo "<td><input type='text' name='referencia' class='OBG' value='{$dados->referencia}' {$visao} /></td></tr>";
    echo "<tr><td><b>Empresa</b></td</tr>";
    echo "<tr><td>".$this->empresas($dados->empresa,$visao_sexo)."</td></tr></table>";
    if($visao == '') echo "<p><button id='botao-salvar-formulario' modo='{$modo}' >Inserir</button>";
    echo "</div>";
  }
  
  //! Lista todos so pacientes.
  public function listar(){
    $result = mysql_query("SELECT *, s.status as nstatus FROM clientes as c, statuspaciente as s WHERE c.status = s.id ORDER BY nome");
    echo "<div id='dialog-status' title='Status do paciente' cod='' ></div>";
    echo "<a href='imprimir.php'>Imprimir lista</a><br>";
    echo "<table class='mytable' width=100% >"; 
    echo "<thead><tr>";
    echo "<th><b>C&oacute;digo</b></th>";
    echo "<th><b>Paciente</b></th>";
    echo "<th width=1%><b>Status</b></th>"; 
    echo "<th colspan='4' ><b>Op&ccedil;&otilde;es</b></th>";
    echo "</tr></thead>";
    $cor = false;
    while($row = mysql_fetch_array($result))
    {
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
      if($cor) echo "<tr>";
      else echo "<tr class='odd' >";
      $cor = !$cor;
      echo "<td>{$row['codigo']}</td>";
      echo "<td>{$row['nome']}</td>";
      echo "<td>{$row['nstatus']}</td>";
      echo "<td><a href='?adm=paciente&act=listcapmed&id={$row['idClientes']}'><img src='../utils/capmed_16x16.png' title='ficha m&eacute;dica' border='0'></a></td>";
      echo "<td><img src='../utils/status_16x16.png' class='statusp' cod='{$row['idClientes']}' title='Mudar status' border='0'></td>";
      echo "<td><a href=?adm=paciente&act=show&id={$row['idClientes']}><img src='../utils/details_16x16.png' title='Detalhes' border='0'></a></td>";
      echo "<td><a href=?adm=paciente&act=editar&id={$row['idClientes']}><img src='../utils/edit_16x16.png' title='Editar' border='0'></a></td>";
      echo "</tr>";
    }
    echo "</table>";
  }
  
  //! Inicializa uma nova captação
  public function nova_capitacao_medica($id){
  	$id = anti_injection($id,"numerico");
  	$result = mysql_query("SELECT p.nome FROM clientes as p WHERE p.idClientes = '{$id}'");
  	$d = new DadosFicha();
    while($row = mysql_fetch_array($result)){
    	$d->pacienteid = $id;
    	$d->pacientenome = ucwords(strtolower($row["nome"]));
    }
    $this->capitacao_medica($d,"");
  }
  
  //! Mostra detalhes de uma captação
  public function detalhes_capitacao_medica($id){
  	$id = anti_injection($id,"numerico");
  	$sql = "SELECT cap.*, pac.nome as pnome, usr.nome as unome, DATE_FORMAT(cap.data,'%d/%m/%Y') as fdata, comob.* 
  			FROM capmedica as cap 
  			LEFT OUTER JOIN comorbidades as comob ON cap.id = comob.capmed
  			LEFT OUTER JOIN problemasativos as prob ON cap.id = prob.capmed 
  			LEFT OUTER JOIN usuarios as usr ON cap.usuario = usr.idUsuarios 
  			LEFT OUTER JOIN clientes as pac ON cap.paciente = pac.idClientes  
  			WHERE cap.id = '{$id}'";
  	$d = new DadosFicha();
  	$result = mysql_query($sql);
    while($row = mysql_fetch_array($result)){
    	$d->usuario = ucwords(strtolower($row["unome"])); $d->data = $row['fdata']; $d->pacientenome = ucwords(strtolower($row["pnome"])); 
    	$d->motivo = $row['motivo']; 
		$d->alergiamed = $row['alergiamed']; $d->alergiaalim = $row['alergiaalim']; 
		$d->tipoalergiamed = $row['tipoalergiamed']; $d->tipoalergiaalim = $row['tipoalergiaalim'];
		$d->prelocohosp = $row['hosplocomocao']; $d->prehigihosp = $row['hosphigiene']; 
		$d->preconshosp = $row['hospcons']; $d->prealimhosp = $row['hospalimentacao']; $d->preulcerahosp = $row['hospulcera'];
		$d->prelocodomic = $row['domlocomocao']; $d->prehigidomic = $row['domhigiene']; $d->preconsdomic = $row['domcons']; 
		$d->prealimdomic = $row['domalimentacao']; $d->preulceradomic = $row['domulcera'];
		$d->objlocomocao = $row['objlocomocao']; $d->objhigiene = $row['objhigiene']; $d->objcons = $row['objcons']; 
		$d->objalimento = $row['objalimentacao']; $d->objulcera = $row['objulcera'];
		$d->shosp = $d->prelocohosp + $d->prehigihosp +	$d->preconshosp + $d->prealimhosp + $d->preulcerahosp; 
		$d->sdom = $d->prelocodomic + $d->prehigidomic +	$d->preconsdomic + $d->prealimdomic + $d->preulceradomic;; 
		$d->sobj = $d->objlocomocao + $d->objhigiene + $d->objcons + $d->objalimento + $d->objulcera;
	
		$d->qtdinternacoes = $row['qtdinternacoes']; $d->historico = $row['historicointernacao'];
		$d->parecer = $row['parecer']; $d->justificativa = $row['justificativa'];
	
		$d->cancer = $row['cancer']; $d->psiquiatrico = $row['psiquiatrico']; $d->neuro = $row['neuro']; $d->glaucoma = $row['glaucoma'];
		$d->hepatopatia = $row['hepatopatia']; $d->obesidade = $row['obesidade']; $d->cardiopatia = $row['cardiopatia']; $d->dm = $row['dm'];
		$d->reumatologica = $row['reumatologica']; $d->has = $row['has']; $d->nefropatia = $row['nefropatia']; $d->pneumopatia = $row['pneumopatia'];
    }
    $sql = "SELECT c.descricao as nome FROM problemasativos as p, cid10 as c WHERE p.cid = c.codigo AND p.capmed = '{$id}' ";
    $result = mysql_query($sql);
    while($row = mysql_fetch_array($result)){
    	$d->ativos[] = $row['nome'];
    }
    $this->capitacao_medica($d,"readonly");
  }
  
  //! Exibe uma captação, seja apenas os detalhes (modo read only) ou para criação.
  public function capitacao_medica($dados,$acesso){
  	  $enabled = "";
  	  if($acesso == "readonly") $enabled = "disabled";
      echo "<center><h1>Ficha m&eacute;dica do paciente</h1></center>";
      echo "<center><h2>{$dados->pacientenome}</h2></center>";
      if($acesso == "readonly"){
      	echo "<center><h2>Respons&aacute;vel: {$dados->usuario} - Data: {$dados->data}</h2></center>";
      }
      echo "<div id='div-ficha-medica' ><br/>";
      echo alerta();
      echo "<form>";
      echo "<input type='hidden' name='paciente' value='{$dados->pacienteid}' />";
      echo "<br/><table class='mytable' width=100% >";
      echo "<thead><tr>"; 
      echo "<th colspan='2' >Motivo da hospitaliza&ccedil;&atilde;o</th>";
      echo "</tr></thead>";
      echo "<tr><td colspan='2' ><textarea cols=100 rows=5 {$acesso} name='motivo' class='OBG' >{$dados->motivo}</textarea>";
      //<input type='text' size=100 value='{$row['motivo']}' name='motivo' class='OBG' /></td></tr>";
      echo "<thead><tr>"; 
      echo "<th colspan='2' >Problemas Ativos</th>";
      echo "</tr></thead>";
      if($acesso != "readonly") echo "<tr><td colspan='2' ><input type='text' name='busca-problemas-ativos' {$acesso} size=100 /></td></tr>";
      echo "<tr><td colspan='2' ><table id='problemas_ativos'>";
      if(!empty($dados->ativos)){
      	foreach($dados->ativos as $a){
      		echo "<tr><td>{$a}</td></tr>";
      	}
      }
      echo "</table></td></tr>";
      echo "<thead><tr>"; 
      echo "<th colspan='2' >Comorbidades (S/N)</th>";
      echo "</tr></thead>";
      echo "<tr><td><input type='text' class='boleano OBG' name='cancer' value='{$dados->cancer}' {$acesso} /><b>Câncer</b></td>";
      echo "<td><input type='text' class='boleano OBG' name='cardiopatia' value='{$dados->cardiopatia}' {$acesso} /><b>Cardiopatia</b></td>";
      echo "</tr><tr><td><input type='text' class='boleano OBG' name='psiquiatrico' value='{$dados->psiquiatrico}' {$acesso} /><b>Dist&uacute;rbio Psiqui&aacute;trico</b></td>";
      echo "<td><input type='text' class='boleano OBG' name='dm' value='{$dados->dm}' {$acesso} /><b>DM</b></td>";
      echo "</tr><tr><td><input type='text' class='boleano OBG' name='neurologica' value='{$dados->neuro}' {$acesso} /><b>Doen&ccedil;a Neurol&oacute;gica</b></td>";
      echo "<td><input type='text' class='boleano OBG' name='reumatologica' value='{$dados->reumatologica}' {$acesso} /><b>Doen&ccedil;a Reumatol&oacute;gica</b></td>";
      echo "</tr><tr><td><input type='text' class='boleano OBG' name='glaucoma' value='{$dados->glaucoma}' {$acesso} /><b>Glaucoma</b></td>";
      echo "<td><input type='text' class='boleano OBG' name='has' value='{$dados->has}' {$acesso} /><b>HAS</b></td>";
      echo "</tr><tr><td><input type='text' class='boleano OBG' name='hepatopatia' value='{$dados->hepatopatia}' {$acesso} /><b>Hepatopatia</b></td>";
      echo "<td><input type='text' class='boleano OBG' name='nefropatia' value='{$dados->nefropatia}' {$acesso} /><b>Nefropatia</b></td>";
      echo "</tr><tr><td><input type='text' class='boleano OBG' name='obesidade' value='{$dados->obesidade}' {$acesso} /><b>Obesidade</b></td>";
      echo "<td><input type='text' class='boleano OBG' name='pneumopatia' value='{$dados->pneumopatia}' {$acesso} /><b>Pneumopatia</b></td>";
      echo "</tr><thead><tr>"; 
      echo "<th>Alergia medicamentosa (S/N) <input type='text' class='boleano OBG' name='palergiamed' value='{$dados->alergiamed}' {$acesso} /></th>";
      echo "<th>Alergia alimentar (S/N) <input type='text' class='boleano OBG' name='palergiaalim' value='{$dados->alergiaalim}' {$acesso} /></th>";
      echo "</tr></thead>";
      echo "<tr><td><input type='text' name='alergiamed' size=50 readonly='readonly' value='{$dados->tipoalergiamed}' /></td>";
      echo "<td><input type='text' name='alergiaalim' size=50 readonly='readonly' value='{$dados->tipoalergiaalim}' /></td></tr>";
      echo "<thead><tr>"; 
      $score = "SCORE<input type='text' size='2' name='shosp' value='{$dados->shosp}' {$acesso} />";
      echo "<th colspan='2' >Condi&ccedil;&otilde;es do paciente pr&eacute;-interna&ccedil;&atilde;o hospitalar {$score} </th>";
      echo "</tr></thead>";
      echo "<tr><td colspan='2' ><b>Locomo&ccedil;&atilde;o:</b>";
      echo "<input type='radio' class='hosp' name='prelocohosp' value=3 ".(($dados->prelocohosp=='3')?"checked":"")." {$enabled} /> Deambulava";
      echo "<input type='radio' class='hosp' name='prelocohosp' value=2 ".(($dados->prelocohosp=='2')?"checked":"")." {$enabled} /> Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)";
      echo "<input type='radio' class='hosp' name='prelocohosp' value=1 ".(($dados->prelocohosp=='1')?"checked":"")." {$enabled} /> Restrito ao leito";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>Autonomia para higiene pessoal:</b>";
      echo "<input type='radio' class='hosp' name='prehigihosp' value=2 ".(($dados->prehigihosp=='2')?"checked":"")." {$enabled} /> Sim";
      echo "<input type='radio' class='hosp' name='prehigihosp' value=1 ".(($dados->prehigihosp=='1')?"checked":"")." {$enabled} /> N&atilde;o (Oral, deje&ccedil;&otilde;es, mic&ccedil&atilde;o e banho)";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>N&iacute;vel de consci&ecirc;ncia:</b>";
      echo "<input type='radio' class='hosp' name='preconshosp' value=3 ".(($dados->preconshosp=='3')?"checked":"")." {$enabled} /> LOTE";
      echo "<input type='radio' class='hosp' name='preconshosp' value=2 ".(($dados->preconshosp=='2')?"checked":"")." {$enabled} /> Desorientado";
      echo "<input type='radio' class='hosp' name='preconshosp' value=1 ".(($dados->preconshosp=='1')?"checked":"")." {$enabled} /> Inconsciente";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>Alimenta&ccedil;&atilde;o:</b>";
      echo "<input type='radio' class='hosp' name='prealimhosp' value=4 ".(($dados->prealimhosp=='4')?"checked":"")." {$enabled} /> N&atilde;o assistida";
      echo "<input type='radio' class='hosp' name='prealimhosp' value=3 ".(($dados->prealimhosp=='3')?"checked":"")." {$enabled} /> Assistida oral";
      echo "<input type='radio' class='hosp' name='prealimhosp' value=2 ".(($dados->prealimhosp=='2')?"checked":"")." {$enabled} /> Enteral";
      echo "<input type='radio' class='hosp' name='prealimhosp' value=1 ".(($dados->prealimhosp=='1')?"checked":"")." {$enabled} /> Parenteral";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>&Uacute;lceras de press&atilde;o:</b>";
      echo "<input type='radio' class='hosp' name='preulcerahosp' value=2 ".(($dados->preulcerahosp=='2')?"checked":"")." {$enabled} /> N&atilde;o";
      echo "<input type='radio' class='hosp' name='preulcerahosp' value=1 ".(($dados->preulcerahosp=='1')?"checked":"")." {$enabled} /> Sim";
      echo "</td></tr>";
      echo "<thead><tr>";
      $score = "SCORE<input type='text' readonly size='2' name='sdom' value='{$dados->sdom}' {$acesso} />";
      echo "<th colspan='2' >Condi&ccedil;&otilde;es do paciente pr&eacute;-interna&ccedil;&atilde;o domiciliar. {$score}</th>";
      echo "</tr></thead>";
      echo "<tr><td colspan='2' ><b>Locomo&ccedil;&atilde;o:</b>";
      echo "<input type='radio' class='domic' name='prelocodomic' value=3 ".(($dados->prelocodomic=='3')?"checked":"")." {$enabled} /> Deambulava";
      echo "<input type='radio' class='domic' name='prelocodomic' value=2 ".(($dados->prelocodomic=='2')?"checked":"")." {$enabled} /> Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)";
      echo "<input type='radio' class='domic' name='prelocodomic' value=1 ".(($dados->prelocodomic=='1')?"checked":"")." {$enabled} /> Restrito ao leito";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>Autonomia para higiene pessoal:</b>";
      echo "<input type='radio' class='domic' name='prehigidomic' value=2 ".(($dados->prehigidomic=='2')?"checked":"")." {$enabled} /> Sim";
      echo "<input type='radio' class='domic' name='prehigidomic' value=1 ".(($dados->prehigidomic=='1')?"checked":"")." {$enabled} /> N&atilde;o (Oral, deje&ccedil;&otilde;es, mic&ccedil&atilde;o e banho)";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>N&iacute;vel de consci&ecirc;ncia:</b>";
      echo "<input type='radio' class='domic' name='preconsdomic' value=3 ".(($dados->preconsdomic=='3')?"checked":"")." {$enabled} /> LOTE";
      echo "<input type='radio' class='domic' name='preconsdomic' value=2 ".(($dados->preconsdomic=='2')?"checked":"")." {$enabled} /> Desorientado";
      echo "<input type='radio' class='domic' name='preconsdomic' value=1 ".(($dados->preconsdomic=='1')?"checked":"")." {$enabled} /> Inconsciente";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>Alimenta&ccedil;&atilde;o:</b>";
      echo "<input type='radio' class='domic' name='prealimdomic' value=4 ".(($dados->prealimdomic=='4')?"checked":"")." {$enabled} /> N&atilde;o assistida";
      echo "<input type='radio' class='domic' name='prealimdomic' value=3 ".(($dados->prealimdomic=='3')?"checked":"")." {$enabled} /> Assistida oral";
      echo "<input type='radio' class='domic' name='prealimdomic' value=2 ".(($dados->prealimdomic=='2')?"checked":"")." {$enabled} /> Enteral";
      echo "<input type='radio' class='domic' name='prealimdomic' value=1 ".(($dados->prealimdomic=='1')?"checked":"")." {$enabled} /> Parenteral";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>&Uacute;lceras de press&atilde;o:</b>";
      echo "<input type='radio' class='domic' name='preulceradomic' value=2 ".(($dados->preulceradomic=='2')?"checked":"")." {$enabled} /> N&atilde;o";
      echo "<input type='radio' class='domic' name='preulceradomic' value=1 ".(($dados->preulceradomic=='1')?"checked":"")." {$enabled} /> Sim";
      echo "</td></tr>";
      echo "<thead><tr>"; 
      $score = "SCORE<input type='text' readonly size='2' name='sobj' value='{$dados->sobj}' {$acesso} />";
      echo "<th colspan='2' >Objetivo da continuidade da interna&ccedil;&atilde;o domiciliar. {$score}</th>";
      echo "</tr></thead>";
      echo "<tr><td colspan='2' ><b>Locomo&ccedil;&atilde;o:</b>";
      echo "<input type='radio' class='obj' name='objlocomocao' value=3 ".(($dados->objlocomocao=='3')?"checked":"")." {$enabled} /> Deambulava";
      echo "<input type='radio' class='obj' name='objlocomocao' value=2 ".(($dados->objlocomocao=='2')?"checked":"")." {$enabled} /> Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)";
      echo "<input type='radio' class='obj' name='objlocomocao' value=1 ".(($dados->objlocomocao=='1')?"checked":"")." {$enabled} /> Restrito ao leito";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>Autonomia para higiene pessoal:</b>";
      echo "<input type='radio' class='obj' name='objhigiene' value=2 ".(($dados->objhigiene=='2')?"checked":"")." {$enabled} /> Sim";
      echo "<input type='radio' class='obj' name='objhigiene' value=1 ".(($dados->objhigiene=='1')?"checked":"")." {$enabled} /> N&atilde;o (Oral, deje&ccedil;&otilde;es, mic&ccedil&atilde;o e banho)";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>N&iacute;vel de consci&ecirc;ncia:</b>";
      echo "<input type='radio' class='obj' name='objcons' value=3 ".(($dados->objcons=='3')?"checked":"")." {$enabled} /> LOTE";
      echo "<input type='radio' class='obj' name='objcons' value=2 ".(($dados->objcons=='2')?"checked":"")." {$enabled} /> Desorientado";
      echo "<input type='radio' class='obj' name='objcons' value=1 ".(($dados->objcons=='1')?"checked":"")." {$enabled} /> Inconsciente";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>Alimenta&ccedil;&atilde;o:</b>";
      echo "<input type='radio' class='obj' name='objalimento' value=4 ".(($dados->objalimento=='4')?"checked":"")." {$enabled} /> N&atilde;o assistida";
      echo "<input type='radio' class='obj' name='objalimento' value=3 ".(($dados->objalimento=='3')?"checked":"")." {$enabled} /> Assistida oral";
      echo "<input type='radio' class='obj' name='objalimento' value=2 ".(($dados->objalimento=='2')?"checked":"")." {$enabled} /> Enteral";
      echo "<input type='radio' class='obj' name='objalimento' value=1 ".(($dados->objalimento=='1')?"checked":"")." {$enabled} /> Parenteral";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>&Uacute;lceras de press&atilde;o:</b>";
      echo "<input type='radio' class='obj' name='objulcera' value=2 ".(($dados->objulcera=='2')?"checked":"")." {$enabled} /> N&atilde;o";
      echo "<input type='radio' class='obj' name='objulcera' value=1 ".(($dados->objulcera=='1')?"checked":"")." {$enabled} /> Sim";
      echo "</td></tr>";
      echo "<thead><tr>"; 
      echo "<th colspan='2' >Interna&ccedil;&otilde;es dos &uacute;ltimos 12 meses</th>";
      echo "</tr></thead>";
      echo "<tr><td colspan='2' ><b>Qtd:</b><input type='text' size='2' class='OBG numerico' name='qtdanteriores' value='{$dados->qtdinternacoes}' {$acesso}/>";
      echo "<b>Motivos:</b><input type='text' name='historicointernacao' size=100 readonly='readonly' value='{$dados->historico}' {$acesso} /></td></tr>";
      echo "<thead><tr>"; 
      echo "<th colspan='2' >Parecer M&eacute;dico</th>";
      echo "</tr></thead>";
      echo "<tr><td colspan='2' >";
      echo "<input type='radio' name='parecer' value='0' ".(($dados->parecer=='0')?"checked":"")." {$enabled} /> Favor&aacute;vel";
      echo "<input type='radio' name='parecer' value='1' ".(($dados->parecer=='1')?"checked":"")." {$enabled} /> Favor&aacute;vel com resalvas";
      echo "<input type='radio' name='parecer' value='2' ".(($dados->parecer=='2')?"checked":"")." {$enabled} /> Contr&aacute;rio";
      echo "<br/>Justificativa:<br/><textarea cols=100 rows=5 name='justparecer' class='OBG' {$acesso} >{$dados->justificativa}</textarea>";
      echo "</td></tr>";
      echo "</table></div>";
      echo "</form>";
      if($acesso != "readonly") echo "<br/><button id='salvar-capmed'>Salvar</button>";
  }
  
  //! Lista as captações de um paciente com ID
  public function listar_capitacao_medica($id){
  	$id = anti_injection($id,"numerico");
  	$sql = "SELECT DATE_FORMAT(c.data,'%d/%m/%Y') as fdata, c.id, u.nome, p.nome as paciente FROM capmedica as c, usuarios as u, clientes as p 
  			WHERE paciente = '{$id}' AND c.usuario = u.idUsuarios AND p.idClientes = c.paciente ORDER BY data ";
  	$r = mysql_query($sql);
  	$flag = true;
  	while($row = mysql_fetch_array($r)){
  		if($flag) {
  			$p = ucwords(strtolower($row["paciente"]));
  			echo "<center><h2>{$p}</h2></center>";
  			echo "<br/><br/>";
  			echo "<a href='?adm=paciente&act=newcapmed&id={$id}'>Nova ficha</a>";
	  		echo "<br/><table class='mytable' width=100% >";
    	  	echo "<thead><tr>"; 
	    	echo "<th>Usu&aacute;rio</th>";
		    echo "<th>Data</th>";
		    echo "<th>Op&ccedil;&otilde;es</th>";
      		echo "</tr></thead>";
      		$flag = false;
  		}
  		$u = ucwords(strtolower($row["nome"]));
  		echo "<tr><td>{$u}</td><td>{$row['fdata']}</td>";
  		echo "<td><a href=?adm=paciente&act=capmed&id={$row['id']}><img src='../utils/details_16x16.png' title='Detalhes' border='0'></a></td></tr>";
  	}
  	if($flag){
  		$this->nova_capitacao_medica($id);
  		return;
  	}
  	echo "</table>";
  }
}

?>