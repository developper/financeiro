<?php
$id = session_id();
if(empty($id))
  session_start();

include_once('db/config.php');

if(!function_exists('combo_usuarios')){
function combo_usuarios($name){
	$result = mysql_query("SELECT * FROM usuarios ORDER BY nome");
    $code = "<select name='{$name}' style='background-color:transparent;' >";
    $code .= "<option value='-1'>TODOS</option>";
    while($row = mysql_fetch_array($result)){
		$code .= "<option value='{$row['idUsuarios']}'>{$row['nome']}</option>";
    }
    $code .= "</select>";
    return $code;
}
}

function formulario_login(){
  if($_SESSION["logado"] <> "1"){
    $var =  "<form action='?auth=1' method='post'>";
    $var .=  "Login: <input name='login' type='text' /><br>";
    $var .=  "Senha: <input name='senha' type='password' /><br>";
    $var .=  "<input type='submit' value='Acessar' /></form>";   
  } else {
    $var = show_login();
  }
  return $var;
}

function auth_login(){

  $login = addslashes($_POST["login"]);
  $senha = md5(addslashes($_POST["senha"]));
  
  $sql = "SELECT u.*, e.nome as nempresa FROM usuarios as u, empresas as e WHERE UPPER(u.login) = UPPER('$login') AND u.senha = '$senha' AND u.empresa = e.id";
  $rs = mysql_query($sql);
  $err = "";

  if(mysql_num_rows($rs) == 1) {
    $user = mysql_fetch_array($rs);
    //conferindo o login e senha para segurança
    if($login == $user['login']){
      //se entrou, entao o login é igual
      if($senha == $user['senha']){
	//se entrou, então a senha também é igual
		$logado = "1";
		$id_user = $user['idUsuarios'];
		$tipo_user = $user['tipo'];
		$nome_user = $user['nome'];
		$adm = $user['adm'];
		$nempresa = $user['nempresa'];
		$empresa = $user['empresa'];
		$_SESSION["id_user"] = $id_user;
		$_SESSION["logado"] = $logado;
		$_SESSION["tipo_user"] = $tipo_user;
		$_SESSION["login_user"] = $login;
		$_SESSION["nome_user"] = $nome_user;
		$_SESSION["adm_user"] = $adm;
		$_SESSION["empresa_user"] = $empresa;
		$_SESSION["avisos"] = true;
		$_SESSION["nome_empresa"] = $nempresa;
		$_SESSION["modadm"] = $user['modadm'];
		$_SESSION["modaud"] = $user['modaud'];
		$_SESSION["modenf"] = $user['modenf'];
		$_SESSION["modfin"] = $user['modfin'];
		$_SESSION["modlog"] = $user['modlog'];
		$_SESSION["modmed"] = $user['modmed'];
		$_SESSION["modrem"] = $user['modrem']; 
		$_SESSION["modnutri"] = $user['modnutri'];
		header('Location: inicio.php');
      } else {
		$err .= "A senha n&atilde;o confere!"; 
      }
    } else {
      $err .= "O usu&aacute;rio n&atilde;o confere!";
    }
  }else {
    $err .= "Usu&aacute;rio ou senha inv&aacute;lidos. Tente novamente.";
  }
  
  // $var = formulario_login();
  $var = "<br><h3 align='center' ><font color='red' >".$err."</font></h3>";
  return $var;
}

function show_login(){
  $var = "<div id='dialog-troca-senha' title='Trocar senha.'>
  		 <div id='div-aviso-troca' class='ui-widget'><div class='ui-state-error ui-corner-all' style='padding: 0 .7em;'>
  		 <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;'></span>
  		 <span id='aviso-troca-senha' ></span></p></div></div>	
  		 <br/><b>Senha atual:</b><br/><input type='password' class='OBG' name='senha-atual'/>
  		 <br/><b>Nova senha:</b><br/><input type='password' class='OBG' name='nova-senha'/>
  		 <br/><b>Confirma senha:</b><br/><input type='password' class='OBG' name='conf-senha'/></div>";
  $var .= "<div id='dialog-enviar-msg' title='Enviar mensagem.'>
  		 <div id='div-aviso-msg' class='ui-widget'><div class='ui-state-error ui-corner-all' style='padding: 0 .7em;'>
  		 <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;'></span>
  		 <span id='aviso-msg' ></span></p></div></div>	
  		 <br/><b>Destinatário:</b><br/>".combo_usuarios("usuarios")."
  		 <br/><b>Título:</b><br/><input type='text' name='titulo' />
  		 <br/><b>Mensagem:</b><br/><textarea rows='5' cols='50' name='msg'></textarea></div>";
  $var .= "<table border=0 width=100% cellpadding='1' style='border-spacing:0 0;border-collapse: collapse'' >";
  switch($_SESSION["tipo_user"]){
  	case "administrativo":
  	$var .= "<tr><td style='vertical-align:top;padding-top:15px;'><img class='img_icon_menu_usu' src='/utils/usr_administrador_48x48.png' title='Setor Administrativo' /></td>";
  	break;
  	case "enfermagem":
  	$var .= "<tr><td style='vertical-align:top;padding-top:15px;'><img class='img_icon_menu_usu' src='/utils/usr_enfermeira_48x48.png' title='Setor Enfermagem' /></td>";
  	break;
  	case "financeiro":
  	$var .= "<tr><td style='vertical-align:top;padding-top:15px;'><img class='img_icon_menu_usu' src='/utils/usr_financeiro_48x48.png' title='Setor Financeiro' /></td>";
  	break;
  	case "logistica":
  	$var .= "<tr><td style='vertical-align:top;padding-top:15px;'><img class='img_icon_menu_usu' src='/utils/usr_logistica_48x48.png' title='Setor Logística' /></td>";
  	break;
  	case "medico":
  	$var .= "<tr><td style='vertical-align:top;padding-top:15px;'><img class='img_icon_menu_usu' src='/utils/usr_medico_48x48.png' title='Setor Médico' /></td>";
  	break;
  	case "nutricionista":
  		$var .= "<tr><td style='vertical-align:top;padding-top:15px;'><img class='img_icon_menu_usu' src='/utils/usr_medico_48x48.png' title='Setor Nuticionista' /></td>";
  		break;
  }
  $var .= "<td id='texto_menu'><p>Ol&aacute; ".ucwords(strtolower($_SESSION["nome_user"]))."</p>";
  $var .= "<p><b>Empresa</b><br/>{$_SESSION['nome_empresa']}<br/><br/><a href=?logoff=1 style='color:blue;font-size:12px;float:right;padding:3px;border:1px solid #ccc;margin-right:5px;background:#eeeeee;'>SAIR</a></p></p></td></tr>";
  $var .= "<tr><td colspan='2'></td></tr>";
  $var .= "<tr class='linha_menu'><td><a href='/inicio.php'><img class='img_icon_menu' style='float:middle' src='/utils/menu_home_48x48.png' title='Início' /></td><td><a href='/inicio.php' style='a:visited:color:#000;}'><b>In&iacute;cio</b></a></td></tr>";
  $var .= "<tr class='linha_menu'><td><a href='/nutricao'><img class='img_icon_menu' style='float:middle' src='/utils/frutas03.png' title='Nutri&ccedil;&atilde;o' /></td><td><a href='/nutricao' style='a:visited:color:#000;}'><b>Nutri&ccedil;&atilde;o</b></a></td></tr>";
  $var .= "<tr class='linha_menu'><td><a href='/medico'><img class='img_icon_menu' src='/utils/menu_medico_48x48.png' title='Menu Médico' /></a></td><td><a href='/medico'><b>M&eacute;dicos</b></a></td></tr>";
  $var .= "<tr class='linha_menu'><td><a href='/enfermagem'><img class='img_icon_menu' src='/utils/menu_enf_48x48.png' title='Menu Enfermagem' /></a></td><td><a href='/enfermagem'><b>Enfermagem</b></a></td></tr>";
  $var .= "<tr class='linha_menu'><td><a href='/auditoria'><img class='img_icon_menu' src='/utils/menu-audit_48x48.png' title='Menu Auditoria' /></a></td><td><a href='/auditoria'><b>Auditoria Interna</b></a></td></tr>";
  $var .= "<tr class='linha_menu'><td><a href='/remocoes'><img class='img_icon_menu' src='/utils/menu_remocoes_48x48.png' title='Menu Remoções' /></a></td><td><a href='/remocoes'><b>Remo&ccedil;&otilde;es</b></a></td></tr>";
  $var .= "<tr class='linha_menu'><td><a href='/financeiro'><img class='img_icon_menu' src='/utils/menu_fin_48x48.png' title='Menu Financeiro' /></a></td><td><a href='/financeiro'><b>Financeiro</b></a></td></tr>";
  $var .= "<tr class='linha_menu'><td><a href='/logistica'><img class='img_icon_menu' src='/utils/menu_log_48x48.png' title='Menu Logística' /></a></td><td><a href='/logistica'><b>Log&iacute;stica</b></a></td></tr>";
  $var .= "<tr class='linha_menu'><td><a href='/faturamento'><img class='img_icon_menu' src='/utils/menu_fatur_48x48.png' title='Menu Faturamento' /></a></td><td><a href='/faturamento'><b>Faturamento</b></a></td></tr>";
  //$var .= "<tr class='linha_menu'><td><a href='/lairotut_421__332167854327984375349857329847534'><img class='img_icon_menu' src='/utils/menu_tutorial_64x64.png' width='48' title='Menu Tutorial' /></a></td><td><a href='/lairotut_421__332167854327984375349857329847534'><b>Tutorial</b></a></td></tr>";
  $var .= "<tr class='linha_menu'><td><a href='#'><img class='img_icon_menu' src='/utils/menu_tutorial_64x64.png' width='48' title='Menu Tutorial' /></a></td><td><a href='#'><b>Tutorial</b></a></td></tr>";
  $var .= "<tr class='linha_menu'><td><img class='img_icon_menu' src='/utils/key_24x24.png' title='Trocar de Senha' id='trocar-senha' /></td><td><b>Trocar de Senha</b></a></td></tr>";
  $var .= "<tr class='linha_menu'><td><img class='img_icon_menu' src='/utils/msg_48x48.png' title='Enviar mensagem' id='enviar-msg' /></td><td><b>Mensagens</b></a></td></tr>";
  $var .= "<tr class='linha_menu'><td><a href='/adm'><img class='img_icon_menu' src='/utils/menu_adm_48x48.png' title='Menu Administrativo' /></a></td><td><a href='/adm'><b>Administra&ccedil;&atilde;o</b></a></td></tr></table>";
  return $var;
}

function logoff(){
	session_destroy();
	echo "<script>window.location.reload()</script>";
// 	header("Location: index.php");
}

?>
