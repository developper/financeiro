<?php
session_start();
include_once('../utils/mpdf/mpdf.php');
include_once('../db/config.php');

class Itens{
	private $tipo = "";
	private $nome = "";
	private $apresentacao = "";
	private $via = "";
	private $aprazamento = "";
	private $obs = "";
	private $dose = "";
	private $freq = "";
	
	function Itens($t,$n,$a,$v,$apraz,$o,$d,$f){
		$this->tipo = $t;
		$this->nome = $n;
		$this->apresentacao = $a;
		$this->via = $v;
		$this->aprazamento = $apraz;
		$this->obs = $o;
		$this->dose = $d;
		$this->freq = $f;
	}
	
	public function getAprazamento($dia){
		if(array_key_exists($dia,$this->aprazamento))
			return $this->aprazamento[$dia];
		return "&nbsp;";
	}
	
	public function getLabel(){
		return "{$this->tipo}{$this->nome} {$this->apresentaca} {$this->dose} {$this->via} {$this->freq} {$this->obs}";
	}
}

function aprazamento($i,$f,$inc,$hora,$ip,$fp){
//	$a = "-";
//	switch($inc){
//		case 'SN':
//		case 'ACM':
//		case 'Conforme HGT':
//		case '3 vezes por semana':
//		case '1 vez cada 15 dias':
//		case '1x mês':
//		case 'Contínuo':
//		case '1 vez ao dia':
//		case '2 vezes ao dia':
//		case '3 vezes ao dia':
//		case '4 vezes ao dia':
//		case '5 vezes ao dia':
//		case '6 vezes ao dia':
//			$a = $inc;
////			$v = 24/substr($inc,0,strpos($inc,'x'));
////			$s = "+".$v." hour";
////			$h = new DateTime($hora);
////			$limite = new DateTime('23:59:59');
////			$a = "";
////			while($h < $limite){
////				$a .= $h->format("H:i\n");
////				$h->modify($s);
////			}
//		break;
//		case '1/1h':
//		case '2/2h':
//		case '3/3h':
//		case '4/4h':
//		case '5/5h':
//		case '6/6h':
//		case '7/7h':
//		case '8/8h':
//		case '9/9h':
//		case '10/10h':
//		case '12/12h':
//		case '24/24h':
//			$s = "+".substr($inc,0,strpos($inc,'/'))." hour";
//			$h = new DateTime($hora);
//			$limite = new DateTime('23:59:59');
//			$a = "";
//			while($h < $limite){
//				$a .= $h->format("H:i\n");
//				$h->modify($s);
//			}
//		break;
//	}
	$apraz = array();
	$inicio = new DateTime($i);
	$fim = new DateTime($f);
	$atual = new DateTime($ip);
	$fim_periodo = new DateTime($fp);
	$a = $hora;
	while($atual <= $fim_periodo){ 
		if($atual >= $inicio && $atual <= $fim)
			$apraz[$atual->format('Y-m-d')] = $a;
		else $apraz[$atual->format('Y-m-d')] = "&nbsp;";
		$atual->modify("+1 day");
	}
	return $apraz;
}

$p = $_REQUEST['prescricao'];

$sql = "SELECT 
      s.enfermeiro,
      u.nome as profissional,
      s.tipo,
      s.qtd,
      p.inicio,
      p.fim,
      UPPER(c.nome) as nome,
      c.cuidador,
      c.relcuid, 
      s.autorizado,
      s.enviado,
      s.cod,
      s.idSolicitacoes AS sol,
      s.status,
      DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
      f.formula AS produto 
FROM 
      solicitacoes AS s,
      prescricoes AS p,
      formulas AS f,
      clientes AS c,
	  usuarios AS u  
WHERE 
      s.idPrescricao = {$p} AND
      s.enfermeiro = u.idUsuarios AND
      s.idPrescricao = p.id AND
      c.idclientes = s.paciente AND
      s.tipo = '12' AND      
      f.id = s.cod       
UNION 
SELECT 
       s.enfermeiro,
       u.nome as profissional,
       s.tipo,
       s.qtd,
       p.inicio,
       p.fim,
       UPPER(c.nome) as nome,
       c.cuidador,
       c.relcuid, 
       s.autorizado,
       s.enviado,
       s.cod,
       s.idSolicitacoes AS sol,
       s.status,
       DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
       CONCAT(b.principio,' - ',b.apresentacao) AS produto
FROM 
        solicitacoes AS s,
        prescricoes AS p,
        brasindice AS b,
        clientes AS c,
	    usuarios AS u  
WHERE 
        s.idPrescricao = {$p} AND
        s.enfermeiro = u.idUsuarios AND
        s.idPrescricao = p.id AND
        c.idclientes = s.paciente AND
        s.tipo = '1' AND         
        b.numero_tiss = s.cod 
UNION 
SELECT 
        s.enfermeiro,
        u.nome as profissional,
        s.tipo, 
        s.qtd,
        p.inicio,
        p.fim,
        UPPER(c.nome) as nome,
        c.cuidador,
        c.relcuid, 
        s.autorizado, 
        s.enviado, 
        s.cod, 
        s.idSolicitacoes AS sol, 
        s.status, 
        DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA, 
        CONCAT(b.principio,' - ',b.apresentacao) AS produto 
FROM 
	solicitacoes AS s, 
	prescricoes AS p,
	brasindice AS b, 
	clientes AS c,
	usuarios AS u 
WHERE 
	s.idPrescricao = {$p} AND 
	s.enfermeiro = u.idUsuarios AND
	s.idPrescricao = p.id AND
	c.idclientes = s.paciente AND 
	s.tipo = '0' AND 	
	b.numero_tiss = s.cod 
ORDER BY 
	tipo, 
	produto, 
	autorizado, 
	enviado";


$result = mysql_query($sql);
$result1 = mysql_query($sql);





$row = mysql_fetch_array($result);
	$ip = $row['inicio'];
	$fp = $row['fim'];
	$img = "<img src='../utils/logo.jpg' width='30%' style='align:center' />";
	$hoje = date("d/m/Y");
	$header = "<table border='1' width='100%' style='border:1px solid #000;border-collapse:collapse;'><thead><tr><th width='40%' rowspan='3' colspan='3' >{$img}<br/>{$_SESSION['nome_empresa']}</th><th width=20% colspan='5'>Prescrição ". join("/",array_reverse(explode("-",$row['inicio'])))." a ". join("/",array_reverse(explode("-",$row['fim'])))." - ".ucwords(strtolower($row['nome']))."</th>";
	$header .= "<th rowspan='3' colspan='2' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th></tr></tr>";
//	$header .= "<tr><td colspan='7'><b>Nome: </b>".ucwords(strtolower($row['paciente']))."</td></tr>";
	$header .= "<tr><td colspan='5'><b>Cuidador: </b>".ucwords(strtolower($row['cuidador']))." (".strtolower($row['relcuid']).")</td></tr>";
	$tipo = "Enfermeira";
	
	$header .= "<tr><td colspan='5' ><b>Profissional: </b>".ucwords(strtolower(htmlentities($row['profissional'])))." ({$tipo})</td></tr></table><br/>";
	


$flag = true;





$tabela = "<table style='border:1px solid #000;border-collapse:collapse;' width='100%' ><thead><tr><th colspan='10' >Solicita&ccedil;&otilde;es </th></tr>";
$tabela .= "<tr><th style='border:1px solid #000;'>Ord.</th><th colspan='8' style='width:500px;border:1px solid #000;'>Itens</th><th style='border:1px solid #000;'>Qtd.</th></tr></thead>";

$i = 0;
while($row1 = mysql_fetch_array($result1)){
	
  $tabela .= "<tr><td align='center' style='width:5px;border:1px solid #000;'>{$i}</td><td style='border:1px solid #000;' colspan='8'>{$row1['produto']}</td><td style='border:1px solid #000;' align='center'>{$row1['qtd']}</td></tr>";	
  $i++;
  
}
$paginas []= $header.$tabela."</table>";
//print_r($paginas);
//print_r($paginas);

$mpdf=new mPDF('pt','A4-L',9);
$mpdf->SetHeader('página {PAGENO} de {nbpg}');
$ano = date("Y");
$mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
$mpdf->WriteHTML("<html><body>");
$flag = false;
foreach($paginas as $pag){
	if($flag) $mpdf->WriteHTML("<formfeed>");
	$mpdf->WriteHTML($pag);
	$flag = true;
}
$mpdf->WriteHTML("</body></html>");
$mpdf->Output('prescricao.pdf','I');
exit;
?>