<?php

require_once('../db/config.php');
require_once('../utils/codigos.php');

class Saida{
  
  private function formulario(){
  	echo "<div id='formulario-troca' title='Formul&aacute;rio de troca' sol='' tipo='' >";
  	echo alerta();
  	echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar a troca de item, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
    echo "<p><b>Busca:</b><br/><input type='text' name='busca' id='busca-item-troca' />";
    echo "<div id='opcoes-troca'></div>";
    echo "<input type='hidden' name='cod' id='cod' value='-1' />";
    echo "<p><b>Nome:</b><input type='text' readonly=readonly name='nome' id='nome' class='OBG' size='50' />";
    echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
    echo "<b>Justificativa:</b><input type='text' name='justificativa' class='OBG' size='100' /></div>";
  }
    
  public function pendentes(){
  	$vazio = true;
  	echo "<div><center><h1>Solicita&ccedil;&otilde;es Pendentes</h1></center><br/>";
  	$sql = "SELECT DISTINCT c.nome, s.paciente FROM solicitacoes as s, clientes as c WHERE s.paciente = c.idClientes AND s.qtd > s.enviado AND (s.status = '1' OR s.status = '2') ORDER BY c.nome";
  	$r = mysql_query($sql);
  	while($row = mysql_fetch_array($r)){ 
    	foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
    	$vazio = false;
    	echo "<button class='bpaciente' p='{$row['paciente']}' >{$row['nome']}</button>";
  	}
  	if($vazio) echo "<p>Nenhuma solicita&ccedil;&atilde;o pendente!</div>";
  	else echo "</div>";
  }
  
  public function saida_paciente($p){
  	$p = anti_injection($p,'numerico');
  	echo "<div id='cancelar-item' title='Cancelar item' sol='' >";
  	echo alerta();
  	echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar o cancelamento, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
  	echo "<p><b>Justificativa:</b><input type='text' name='justificativa' class='OBG' size='100' /></div>";
  	$this->formulario();
  	$row = mysql_fetch_array(mysql_query("SELECT c.nome FROM clientes as c WHERE c.idClientes = '$p'"));
  	$envio = "<input type='text' name='qtd-envio' class='numerico' style='text-align:right' size='4' />";
  	echo "<center><h1>Sa&iacute;da para o paciente:<h1>";
  	$paciente = ucwords(strtolower($row['nome']));
  	echo "<h3>$paciente</h3></center>";
  	echo "<div id='dialog-relatorio' title='Relat&oacute;rio' ><img src='../utils/logo.jpg' width=30% style='align:center' /><div id='pre-relatorio'></div>
  		  <form action='print.php' method='post' name='form_relatorio' id='print-relatorio'>
          <input type='hidden' name='relatorio' value='' id='relatorio' />
          <input type='submit' value='Imprimir' />
          </form></div>";
  	echo "<div id='div-envio-paciente' p='$p' np='$paciente' >";
  	echo alerta();
  	echo "<p><b>Data:</b><input type='text' class='data OBG' id='data-envio' name='data_envio' >";
  	echo "<table class='mytable' width=100% id='lista-enviados' ><thead><tr><th>Itens Solicitados</th><th align='center' width='1%'>enviado/pedido</th><th width='1%'>Enviar</th></tr></thead>";
  	$sqlmed = "SELECT s.enfermeiro, s.qtd, s.enviado, s.cod, s.idSolicitacoes as sol, s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data, b.principio, b.apresentacao 
  			   FROM solicitacoes as s, brasindice as b 
  			   WHERE s.paciente = '$p' AND s.tipo = '0' AND qtd > enviado AND (s.status = '1' OR s.status = '2')
  			   AND b.numero_tiss = s.cod ORDER BY principio, apresentacao, qtd, enviado";
//  	$sqlmed = "SELECT med.principioAtivo, com.nome, s.qtd, s.enviado, s.cod, s.idSolicitacoes as sol, DATE_FORMAT(s.data,'%d/%m/%Y') as data  
//  			   FROM solicitacoes as s, medicacoes as med, nomecomercialmed as com 
//  			   WHERE s.paciente = '$p' AND s.tipo = '0' AND qtd > enviado AND (s.status = '1' OR s.status = '2')
//  			   AND com.idNomeComercialMed = s.cod AND com.Medicacoes_idMedicacoes = med.idMedicacoes 
//  			   ORDER BY principioAtivo, nome, qtd, enviado";
  	$rmed = mysql_query($sqlmed);
  	while($row = mysql_fetch_array($rmed)){
  		foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
  		$dados = "sol='{$row['sol']}' cod='{$row['cod']}' n='{$row['principio']}' sn='{$row['apresentacao']}' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['qtd']}' tipo='0'";
  		$cancela = "<img align='right' sol='{$row['sol']}' class='cancela-item' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
  		$trocar = "<img align='right' sol='{$row['sol']}' tipo='med' class='troca-item' src='../utils/exchange_16x16.png' title='Trocar' border='0'>";
  		echo "<tr><td><b>MEDICAMENTO: </b>{$row['principio']} {$row['apresentacao']} {$cancela} {$trocar}</td>";
  		echo "<td align='center'>{$row['enviado']}/{$row['qtd']}</td><td class='dados' $dados >$envio</td></tr>";
  	}
  	$sqlmat = "SELECT s.enfermeiro, s.qtd, s.enviado, s.cod, s.idSolicitacoes as sol, s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data, b.principio, b.apresentacao 
  			   FROM solicitacoes as s, brasindice as b 
  			   WHERE s.paciente = '$p' AND s.tipo = '1' AND qtd > enviado AND (s.status = '1' OR s.status = '2')
  			   AND b.numero_tiss = s.cod ORDER BY principio, apresentacao, qtd, enviado";
//  	$sqlmat = "SELECT mat.nome, mat.descricao, s.qtd, s.enviado, s.cod, s.idSolicitacoes as sol, DATE_FORMAT(s.data,'%d/%m/%Y') as data  
//  			   FROM solicitacoes as s, material as mat 
//  			   WHERE s.paciente = '$p' AND s.tipo = '1' AND qtd > enviado AND (s.status = '1' OR s.status = '2')
//  			   AND mat.idMaterial = s.cod 
//  			   ORDER BY nome, descricao, qtd, enviado";
  	$rmat = mysql_query($sqlmat);
  	while($row = mysql_fetch_array($rmat)){
  		foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
  		$dados = "sol='{$row['sol']}' cod='{$row['cod']}' n='{$row['principio']}' sn='{$row['apresentacao']}' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['qtd']}' tipo='1'";
  		$cancela = "<img align='right' sol='{$row['sol']}' class='cancela-item' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
  		$trocar = "<img align='right' sol='{$row['sol']}' tipo='mat' class='troca-item' src='../utils/exchange_16x16.png' title='Trocar' border='0'>";
  		echo "<tr><td><b>MATERIAL: </b>{$row['principio']} {$row['apresentacao']} {$cancela} {$trocar}</td>";
  		echo "<td align='center'>{$row['enviado']}/{$row['qtd']}</td><td class='dados' $dados >$envio</td></tr>";
  	}
  	$sqlformula = "SELECT s.enfermeiro, s.qtd, s.enviado, s.cod, s.idSolicitacoes as sol, s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data, f.formula  
  			   FROM solicitacoes as s, formulas as f 
  			   WHERE s.paciente = '$p' AND s.tipo = '12' AND qtd > enviado AND (s.status = '1' OR s.status = '2')
  			   AND f.id = s.cod ORDER BY formula, qtd, enviado";
  	$rformula = mysql_query($sqlformula);
  	while($row = mysql_fetch_array($rformula)){
  		foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
  		$dados = "sol='{$row['sol']}' cod='{$row['cod']}' n='{$row['formula']}' sn='' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['qtd']}' tipo='12'";
  		$cancela = "<img align='right' sol='{$row['sol']}' class='cancela-item' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
  		echo "<tr><td>{$row['formula']} {$cancela}</td>";
  		echo "<td align='center'>{$row['enviado']}/{$row['qtd']}</td><td class='dados' $dados >$envio</td></tr>";
  	}
  	echo "</table><br/><button id='salvar-envio'>Enviar</button>";
  	echo "</div>";
  }
  
}

?>