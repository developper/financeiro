$(function($) {
    $.fn.posicaoCursor = function( posicao ) {

        return this.each(function() {

            var $this = $( this );

            if ( $this.get(0).setSelectionRange ) {

                $this.get(0).setSelectionRange( posicao, posicao );

            } else if ( $this.get(0).createTextRange ) {

                var intervalo = $this.get(0).createTextRange();

                intervalo.collapse( true );

                intervalo.moveEnd('character', posicao);

                intervalo.moveStart('character', posicao);

                intervalo.select();

            }

        });

    };
    
  ///Desabilitando o enter///

    $(document).ready(function () {
    	   $('input').keypress(function (e) {
    	        var code = null;
    	        code = (e.keyCode ? e.keyCode : e.which);                
    	        return (code == 13) ? false : true;
    	   });
    	});  

    
   $("#editar_nota").click(function(){
  
    	var notaVal  = $("#nota").attr("cod");
    	var valor= $("#nota").attr("valor");
    	var codFornecedor= $("#nota").attr("fornecedor");
    	
    	var descricao =$("#nota").attr("desc");
    	var val_produtos= $("#nota").attr("val_produtos");
    	var icms= $("#nota").attr("icms");
    	var frete = $("#nota").attr("frete");
    	var seguro = $("#nota").attr("seguro");
    	var ipi= $("#nota").attr("ipi");
    	var issqn= $("#nota").attr("issqn");
    	var des_acessorias= $("#nota").attr("des_acessorias");
    	var des_bon= $("#nota").attr("des_bon");
    
    	 $("#div-nova-entrada1").dialog("open");
    	
    	/*val_produtos = float2moeda(val_produtos);
    	 icms = float2moeda(icms);
    	 frete = float2moeda(frete);
    	 seguro = float2moeda(seguro);
    	 ipi = float2moeda(ipi);
    	 issqn = float2moeda(issqn);
    	 des_acessorias = float2moeda(des_acessorias);
    	 des_bon = float2moeda(des_bon);
    	 */
    	 			
			$("input[name='nota']").val(notaVal);
			$("input[name='valor_nota']").val(valor);
	    	$("input[name='descricao']").val(descricao);
	    	$("select[name='codFornecedor']").val(codFornecedor);
	   
	    	$("input[name='val_produtos']").val(val_produtos);
	    	$("input[name='icms']").val(icms);
	        $("input[name='frete']").val(frete);
	    	$("input[name='seguro']").val(seguro);
	    	$("input[name='ipi']").val(ipi);
	    	$("input[name='issqn']").val(issqn);
	    	$("input[name='des_acessorias']").val(des_acessorias);
	    	$("input[name='des_bon']").val(des_bon);
	    	
	    	
    	
    /*	$.post("entrada.php",{op:'entrada_nota', nota:notaVal,valor:valor,codFornecedor:codFornecedor,descricao:descricao,val_produtos:val_produtos,icms:icms,frete:frete,
    		seguro:seguro,ipi:ipi,issqn:issqn,des_acessorias:des_acessorias, des_bon:des_bon},function(r){
    			
    				$("#edicao").html(r);				
    				$("input[name='nota']").val(notaVal);
    				$("input[name='valor']").val(valor);
    		    	$("input[name='descricao']").val(descricao);
    		    	$("input[name='val_produtos']").val(val_produtos);
    		    	$("input[name='icms']").val(icms);
    		    	
    		    	$("input[name='frete']").val(frete);
    		    	$("input[name='seguro']").val(seguro);
    		    	$("input[name='ipi']").val(ipi);
    		    	$("input[name='issqn']").val(issqn);
    		    	$("input[name='des_acessorias']").val(des_acessorias);
    		    	$("input[name='des_bon']").val(des_bon);
    		    	
    		    	
    		    	
    		    	
    		    	
    		});*/
	  
    	
    	
    	
});
 
   $("#div-xml-nota").dialog({
    	autoOpen: false, modal: true, width: 600, position: 'top',
    	open: function(event,ui){
    		
    	},
    	buttons: {
    		"Fechar" : function(){
    			$(this).dialog("close");
    		}
    	}
    });
   
   $("#xml_nota").click(function(){
	   $("#div-xml-nota").dialog("open");
	   
   });

   
   $("#div-nova-entrada1").dialog({
     	autoOpen: false, modal: true, width: 900, position: 'top',
     	open: function(event,ui){
     		
     	},
     	buttons: {
     		"Fechar" : function(){
     			$(this).dialog("close");
     		}
     	}
     });
 
   $("#novaentrada1").click(function() {
	   var produto= $("input[name='val_produtos']").val();
		 // produto = parseFloat(produto.replace('.','').replace(',','.'));
		  $("input[name='val_produtos']").val(produto);
		  
		  var icms= verificaNaN($("input[name='icms']").val());
		  //icms = parseFloat(icms.replace('.','').replace(',','.'));
		  $("input[name='icms']").val(icms);
		  
		  var frete= verificaNaN($("input[name='frete']").val());
		 // frete = parseFloat(frete.replace('.','').replace(',','.'));
		  $("input[name='frete']").val(frete);
		  
		  
		  var seguro= verificaNaN($("input[name='seguro']").val());
		 // seguro = parseFloat(seguro.replace('.','').replace(',','.'));
		  $("input[name='seguro']").val(seguro);
		  
		  var ipi= verificaNaN($("input[name='ipi']").val());
		 // ipi= parseFloat(ipi.replace('.','').replace(',','.'));
		  $("input[name='ipi']").val(ipi);
		  
		  var issqn= verificaNaN($("input[name='issqn']").val());
		  //issqn= parseFloat(issqn.replace('.','').replace(',','.'));
		  $("input[name='issqn']").val(issqn);
		  
		  var des_acessorias= verificaNaN($("input[name='des_acessorias']").val());
		  //des_acessorias= parseFloat(des_acessorias.replace('.','').replace(',','.'));
		  $("input[name='des_acessorias']").val(des_acessorias);
		  
		  var des_bon = verificaNaN($("input[name='des_bon']").val());
		  //des_bon= parseFloat(des_bon.replace('.','').replace(',','.'));
		  $("input[name='des_bon']").val(des_bon);
		  
		  var valor = verificaNaN($("#valor_nota").val());
		  valor= parseFloat(valor.replace('.','').replace(',','.'));

	   $("#nota").attr("cod",$("input[name='nota']").val());
	   $("#nota").attr("valor",$("input[name='valor_nota']").val());
   	   $("#nota").attr("fornecedor",$("select[name='codFornecedor']").val());   	
   	   $("#nota").attr("descricao",$("input[name='descricao']").val());
       $("#nota").attr("val_produtos",	produto);
       $("#nota").attr("icms",icms);
       $("#nota").attr("frete",frete);
       $("#nota").attr("seguro",seguro);
   	   $("#nota").attr("ipi",ipi);
       $("#nota").attr("issqn",issqn);
       $("#nota").attr("des_acessorias",des_acessorias);
       $("#nota").attr("des_bon",des_bon);
      
       $("#vnota").html($("input[name='val_produtos']").val());
       $("#codnota").html($("input[name='nota']").val());
      
       $("#div-nova-entrada1").dialog("close");
   });  
   
 function verificaNaN (param){
 	if(param=='NaN' ||  param==null || param==''){
    		param = '0,00';
    	return param;
    	}else {
    		
    	return param;}
    	
    	
    };
    
    
   
    $('.data').datepicker('option','minDate',$("#tabela-itens").attr("inicio"));
    $('.data').datepicker('option','maxDate',$("#tabela-itens").attr("fim"));
    
    $(".data").datepicker({
  	defaultDate: "+1w",
  	changeMonth: true,
  	numberOfMonths: 1
    });
    
/******************** INICIALIZACOES E FUNCOES ***************************/
  $.ajaxSetup({cache: false});
   $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
   
   $(".valor").priceFormat({
    prefix: '',
    centsSeparator: ',',
    thousandsSeparator: '.'
  });
   
   $(".val").priceFormat({
	    prefix: '',
	    centsSeparator: '.',
	    thousandsSeparator: ''
	  });
  
  $(".data").datepicker({
    inline: true,
    changeMonth: true,
    changeYear: true
  });
  
  var dates = $( "#inicio, #fim" ).datepicker({
	defaultDate: "+1w",
	changeMonth: true,
	numberOfMonths: 3,
	onSelect: function( selectedDate ) {
		var option = this.id == "inicio" ? "minDate" : "maxDate",
		instance = $( this ).data( "datepicker" );
		date = $.datepicker.parseDate(
		instance.settings.dateFormat ||
		$.datepicker._defaults.dateFormat,
		selectedDate, instance.settings );
		dates.not( this ).datepicker( "option", option, date );
	}
  });
  
  $('.numerico').keyup(function(e){
    this.value = this.value.replace(/\D/g,'');
  });
  
    $(".div-aviso").hide();
  function validar_campos(div){
    var ok = true;
    $("#"+div+" .OBG").each(function (i){
      if(this.value == ""){
		ok = false;
		this.style.border = "3px solid red";
      } else {
		this.style.border = "";
      }
    });
    $("#"+div+" .COMBO_OBG option:selected").each(function (i){
      if(this.value == "-1"){
		ok = false;
		$(this).parent().css({"border":"3px solid red"});
      } else {
		$(this).parent().css({"border":""});
      }
    });
    if(!ok){
      $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
      $("#"+div+" .div-aviso").show();
    } else {
      $("#"+div+" .aviso").text("");
      $("#"+div+" .div-aviso").hide();
    }
    return ok;
  }
  
  $("#bonificados-itens").easyTooltip({
  	xOffset: -200,
  	yOffset: 20,
    content: "<h4>BONIFICAR TODOS</h4>"
  });
  
 ///Bot�o lan�ar produtos///
  
  $("#novaentrada").hide();
  $("#novaentrada1").hide();
  
/*########################AUTO SOMA##############################*/
   
  $("#icms").blur(function(){
	  
	  var icms   = verificaNaN($("input[name='icms']").val());
	  var frete  = verificaNaN($("input[name='frete']").val());
	  var seguro = verificaNaN($("input[name='seguro']").val());
	  var produtos = verificaNaN($("input[name='val_produtos']").val());
	  var des_acessorias = verificaNaN($("input[name='des_acessorias']").val());
	  var ipi = verificaNaN($("input[name='ipi']").val());
	  var issqn = verificaNaN($("input[name='issqn']").val());
	
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
	 
	  
	  	 
	  icms = parseFloat(icms.replace('.','').replace(',','.'));
	  
	  frete = parseFloat(frete.replace('.','').replace(',','.'));
	  seguro = parseFloat(seguro.replace('.','').replace(',','.'));
	  produtos = parseFloat(produtos.replace('.','').replace(',','.'));
	  
	  ipi = parseFloat(ipi.replace('.','').replace(',','.'));
	  issqn = parseFloat(issqn.replace('.','').replace(',','.'));
	  des_acessorias = parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  des_bon = parseFloat(des_bon.replace('.','').replace(',','.'));
	  var tot = icms+frete+seguro+produtos+ipi+issqn+des_acessorias-des_bon;
	 
	  if(tot >=0){
		  $("#novaentrada").show();
		  $("#novaentrada1").show();
		  
	  }else{
		  $("#novaentrada").hide();
		  $("#novaentrada1").hide();
	  }
	 
	  tot = float2moeda(tot);
	  $("input[name='valor_nota']").val(tot);
  });
  
  $("#frete").blur(function(){
	  var icms   = verificaNaN($("input[name='icms']").val());
	  var frete  = verificaNaN($("input[name='frete']").val());
	  var seguro = verificaNaN($("input[name='seguro']").val());
	  var produtos = verificaNaN($("input[name='val_produtos']").val());
	  var des_acessorias = verificaNaN($("input[name='des_acessorias']").val());
	  var ipi = verificaNaN($("input[name='ipi']").val());
	  var issqn = verificaNaN($("input[name='issqn']").val());
	
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
      icms     = parseFloat(icms.replace('.','').replace(',','.'));
	  frete    = parseFloat(frete.replace('.','').replace(',','.'));
	  seguro   = parseFloat(seguro.replace('.','').replace(',','.'));
	  produtos = parseFloat(produtos.replace('.','').replace(',','.'));
	  ipi      = parseFloat(ipi.replace('.','').replace(',','.'));
	  issqn    = parseFloat(issqn.replace('.','').replace(',','.'));
	  des_acessorias = parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  des_bon  = parseFloat(des_bon.replace('.','').replace(',','.'));
	 
	  var tot = icms+frete+seguro+produtos+ipi+issqn+des_acessorias-des_bon;
	  
	  if(tot >=0){
		  $("#novaentrada").show();
		  $("#novaentrada1").show();
	  }else{
		  $("#novaentrada").hide();
		  $("#novaentrada1").hide();
	  }
	  
tot = float2moeda(tot);
	  
	  $("input[name='valor_nota']").val(tot);
  });
  
  
$("#seguro").blur(function(){
	  
	  var icms   = verificaNaN($("input[name='icms']").val());
	  var frete  = verificaNaN($("input[name='frete']").val());
	  var seguro = verificaNaN($("input[name='seguro']").val());
	  var produtos = verificaNaN($("input[name='val_produtos']").val());
	  var des_acessorias = verificaNaN($("input[name='des_acessorias']").val());
	  var ipi = verificaNaN($("input[name='ipi']").val());
	  var issqn = verificaNaN($("input[name='issqn']").val());
	
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
	 
	  
	  	 
	  icms = parseFloat(icms.replace('.','').replace(',','.'));
	  
	  frete = parseFloat(frete.replace('.','').replace(',','.'));
	  seguro = parseFloat(seguro.replace('.','').replace(',','.'));
	  produtos = parseFloat(produtos.replace('.','').replace(',','.'));
	  
	  ipi = parseFloat(ipi.replace('.','').replace(',','.'));
	  issqn = parseFloat(issqn.replace('.','').replace(',','.'));
	  des_acessorias = parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  des_bon = parseFloat(des_bon.replace('.','').replace(',','.'));
	  
	  
	  
	  var tot = icms+frete+seguro+produtos+ipi+issqn+des_acessorias-des_bon;
	 
	  if(tot >=0){
		  $("#novaentrada").show();
		  $("#novaentrada1").show();
	  }else{
		  $("#novaentrada").hide();
		  $("#novaentrada1").hide();
	  }
	  
	  tot = float2moeda(tot);
	  $("input[name='valor_nota']").val(tot);
  });
  
$("#val_produtos").blur(function(){
	  
	  var icms   = verificaNaN($("input[name='icms']").val());
	  var frete  = verificaNaN($("input[name='frete']").val());
	  var seguro = verificaNaN($("input[name='seguro']").val());
	  var produtos = verificaNaN($("input[name='val_produtos']").val());
	  var des_acessorias = verificaNaN($("input[name='des_acessorias']").val());
	  var ipi = verificaNaN($("input[name='ipi']").val());
	  var issqn = verificaNaN($("input[name='issqn']").val());
	
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
	 
	  
	  	 
	  icms = parseFloat(icms.replace('.','').replace(',','.'));
	  
	  frete = parseFloat(frete.replace('.','').replace(',','.'));
	  seguro = parseFloat(seguro.replace('.','').replace(',','.'));
	  produtos = parseFloat(produtos.replace('.','').replace(',','.'));
	  
	  ipi = parseFloat(ipi.replace('.','').replace(',','.'));
	  issqn = parseFloat(issqn.replace('.','').replace(',','.'));
	  des_acessorias = parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  des_bon = parseFloat(des_bon.replace('.','').replace(',','.'));
	  
	  
	  
	  var tot = icms+frete+seguro+produtos+ipi+issqn+des_acessorias-des_bon;
	 
	  
	  if(tot >=0){
		  $("#novaentrada").show();
		  $("#novaentrada1").show();
	  }else{
		  $("#novaentrada").hide();
		  $("#novaentrada1").hide();
	  }
	  
	  tot = float2moeda(tot);
	  $("input[name='valor_nota']").val(tot);
});

$("#ipi").blur(function(){
	  var icms   = verificaNaN($("input[name='icms']").val());
	  var frete  = verificaNaN($("input[name='frete']").val());
	  var seguro = verificaNaN($("input[name='seguro']").val());
	  var produtos = verificaNaN($("input[name='val_produtos']").val());
	  var des_acessorias = verificaNaN($("input[name='des_acessorias']").val());
	  var ipi = verificaNaN($("input[name='ipi']").val());
	  var issqn = verificaNaN($("input[name='issqn']").val());
	
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
	 
	  	 
	  icms = parseFloat(icms.replace('.','').replace(',','.'));
	  
	  frete = parseFloat(frete.replace('.','').replace(',','.'));
	  seguro = parseFloat(seguro.replace('.','').replace(',','.'));
	  produtos = parseFloat(produtos.replace('.','').replace(',','.'));
	  
	  ipi = parseFloat(ipi.replace('.','').replace(',','.'));
	  issqn = parseFloat(issqn.replace('.','').replace(',','.'));
	  des_acessorias = parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  des_bon = parseFloat(des_bon.replace('.','').replace(',','.'));
	  
	  
	  
	  var tot = icms+frete+seguro+produtos+ipi+issqn+des_acessorias-des_bon;
	  
	  
	  if(tot >=0){
		  $("#novaentrada").show();
		  $("#novaentrada1").show();
	  }else{
		  $("#novaentrada").hide();
		  $("#novaentrada1").hide();
	  }
	  
	  tot = float2moeda(tot);
	  $("input[name='valor_nota']").val(tot);
});

$("#issqn").blur(function(){
	  
	  var icms   = verificaNaN($("input[name='icms']").val());
	  var frete  = verificaNaN($("input[name='frete']").val());
	  var seguro = verificaNaN($("input[name='seguro']").val());
	  var produtos = verificaNaN($("input[name='val_produtos']").val());
	  var des_acessorias = verificaNaN($("input[name='des_acessorias']").val());
	  var ipi = verificaNaN($("input[name='ipi']").val());
	  var issqn = verificaNaN($("input[name='issqn']").val());
	
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
	 
	  	 
	  icms = parseFloat(icms.replace('.','').replace(',','.'));
	  
	  frete = parseFloat(frete.replace('.','').replace(',','.'));
	  seguro = parseFloat(seguro.replace('.','').replace(',','.'));
	  produtos = parseFloat(produtos.replace('.','').replace(',','.'));
	  
	  ipi = parseFloat(ipi.replace('.','').replace(',','.'));
	  issqn = parseFloat(issqn.replace('.','').replace(',','.'));
	  des_acessorias = parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  des_bon = parseFloat(des_bon.replace('.','').replace(',','.'));
	  
	  var tot = icms+frete+seguro+produtos+ipi+issqn+des_acessorias-des_bon;
	  
	  if(tot >=0){
		  $("#novaentrada").show();
		  $("#novaentrada1").show();
	  }else{
		  $("#novaentrada").hide();
		  $("#novaentrada1").hide();
	  }
	  
	  tot = float2moeda(tot);
	  $("input[name='valor_nota']").val(tot);
});

$("#des_bon").blur(function(){
	  
	  var icms   = verificaNaN($("input[name='icms']").val());
	  var frete  = verificaNaN($("input[name='frete']").val());
	  var seguro = verificaNaN($("input[name='seguro']").val());
	  var produtos = verificaNaN($("input[name='val_produtos']").val());
	  var des_acessorias = verificaNaN($("input[name='des_acessorias']").val());
	  var ipi = verificaNaN($("input[name='ipi']").val());
	  var issqn = verificaNaN($("input[name='issqn']").val());
	
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
	 
	  
	  	 
	  icms = parseFloat(icms.replace('.','').replace(',','.'));
	  
	  frete = parseFloat(frete.replace('.','').replace(',','.'));
	  seguro = parseFloat(seguro.replace('.','').replace(',','.'));
	  produtos = parseFloat(produtos.replace('.','').replace(',','.'));
	  
	  ipi = parseFloat(ipi.replace('.','').replace(',','.'));
	  issqn = parseFloat(issqn.replace('.','').replace(',','.'));
	  des_acessorias = parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  des_bon = parseFloat(des_bon.replace('.','').replace(',','.'));
	  
	  
	  
	  var tot = icms+frete+seguro+produtos+ipi+issqn+des_acessorias-des_bon;
	  if(tot >=0){
		  $("#novaentrada").show();
		  $("#novaentrada1").show();
	  }else{
		  $("#novaentrada").hide();
		  $("#novaentrada1").hide();
	  }
	  
	  tot = float2moeda(tot);
	  $("input[name='valor_nota']").val(tot);
	  
});

$("#des_acessorias").blur(function(){
	  
	  var icms   = verificaNaN($("input[name='icms']").val());
	  var frete  = verificaNaN($("input[name='frete']").val());
	  var seguro = verificaNaN($("input[name='seguro']").val());
	  var produtos = verificaNaN($("input[name='val_produtos']").val());
	  var des_acessorias = verificaNaN($("input[name='des_acessorias']").val());
	  var ipi = verificaNaN($("input[name='ipi']").val());
	  var issqn = verificaNaN($("input[name='issqn']").val());
	
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
	 
	  
	  	 
	  icms = parseFloat(icms.replace('.','').replace(',','.'));
	  
	  frete = parseFloat(frete.replace('.','').replace(',','.'));
	  seguro = parseFloat(seguro.replace('.','').replace(',','.'));
	  produtos = parseFloat(produtos.replace('.','').replace(',','.'));
	  
	  ipi = parseFloat(ipi.replace('.','').replace(',','.'));
	  issqn = parseFloat(issqn.replace('.','').replace(',','.'));
	  des_acessorias = parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  des_bon = parseFloat(des_bon.replace('.','').replace(',','.'));
	  
	  
	  var tot = icms+frete+seguro+produtos+ipi+issqn+des_acessorias-des_bon;
	  
	  
	  if(tot >=0){
		  $("#novaentrada").show();
		  $("#novaentrada1").show();
	  }else{
		  $("#novaentrada").hide();
		  $("#novaentrada1").hide();
	  }
	  
	  tot = float2moeda(tot);
	  
	  $("input[name='valor_nota']").val(tot);
	 
});
  

  
  
/*************** SAIDA : ENVIO PARA O PACIENTE *****************/
  $("#cancelar-item").dialog({
  	autoOpen: false, modal: true, width: 800, position: 'top',
  	open: function(event,ui){
  		$("#cancelar-item .aviso").text("");
     	$("#cancelar-item .div-aviso").hide();
  		$("#cancelar-item input[name='justificativa']").val('');
  		$("#cancelar-item input[name='justificativa']").css({"border":""});
  	    $("#cancelar-item input[name='justificativa']").focus();
  	},
  	buttons: {
  		"Cancelar" : function(){
  			$(this).dialog("close");
  		},
  		"Salvar" : function(){
  			if(validar_campos('cancelar-item')){
  				var just = $("#cancelar-item input[name='justificativa']").val();
  				var sol = $("#cancelar-item").attr("sol");
  				$.post("query.php",{query: "cancelar-item-solicitacao", sol: sol, just: just},function(r){
  					if(r == '1'){
  						alert('Cancelamento salvo com sucesso!');
  						window.location.reload();
  					}
  				});
  				$(this).dialog("close");
  			}
  		}
  	}
  });
  
  $(".cancela-item").click(function(){
  	$("#cancelar-item").attr("sol",$(this).attr("sol"));
  	$("#cancelar-item").dialog("open");
  });
  
//  $("#busca-item-troca").keyup(function() {
//    if($(this).val().length > 2){
//		$.post("query.php", {query: "busca-itens-troca", like: $(this).val(), tipo: $("#formulario-troca").attr("tipo")}, 
//		function(lista){
//	  		$("#opcoes-troca").html(lista);
//		});
//    } else {
//      $("#opcoes-troca").html("");
//    }
//  });
  
  $("#busca-item-troca").autocomplete({
			source: "/utils/busca_brasindice.php",
			minLength: 3,
			select: function( event, ui ) {
					$('#nome').val(ui.item.value);
	      			$('#cod').val(ui.item.id);
				    $('#busca-item-troca').val('');
				    $("input[name='qtd']").focus();
			}
  });

  $("#formulario-troca").dialog({
  	autoOpen: false, modal: true, width: 800, position: 'top',
  	open: function(event,ui){
  		$("#formulario-troca input[name='justificativa']").val('');
  		$("input[name='busca']").val('');
  		$('#nome').val('');
	    $('#nome').attr('readonly','readonly');
	    $('#apr').val('');
	    $('#apr').attr('readonly','readonly');
	    $('#cod').val('');
	    $("input[name='busca']").focus();
  	},
  	buttons: {
  		"Cancelar" : function(){
  			$(this).dialog("close");
  		},
  		"Trocar" : function(){
  			if(validar_campos('formulario-troca')){
  				var just = $("#formulario-troca input[name='justificativa']").val();
  				var sol = $("#formulario-troca").attr("sol");
  				var tipo = $("#formulario-troca").attr("tipo");
  				var item = $("#formulario-troca input[name='cod']").val(); 
  				var qtd = $("#formulario-troca input[name='qtd']").val();
  				var n = $("#formulario-troca input[name='n']").val();
  				var p = $("#div-envio-paciente").attr("p");
  				$.post("query.php",{query: "trocar-item-solicitacao", sol: sol, just: just, tipo: tipo, item: item, qtd: qtd, p: p, n: n},
  					function(r){
  						if(r == '1'){
  							alert('Troca efetuada com sucesso!');
  							window.location.reload();
  						}
  				});
	  			$(this).dialog("close");
  			}
  		}
  	}
  });
  
 
  
  $(".troca-item").click(function(){
  	var tipo = $(this).attr("tipo");
  	var sol = $(this).attr("sol");
  	$("#formulario-troca").attr("sol",sol);
  	$("#formulario-troca").attr("tipo",tipo);
  	$("#formulario-troca").dialog("open");
  });

  $(".bpaciente").click(function(){
	  var p = $(this).attr("p");
	  var presc = $(this).attr("presc");
	  
		  $("#formulario-check").attr("p",p);
		  $("#formulario-check").attr("presc",presc);
		  $("#formulario-check").dialog("open");
	  
	  //window.location = "/logistica/?op=saida2&p="+$(this).attr("p");

  });
  
  $("#formulario-check").dialog({
	  	autoOpen: false, modal: true, width: 800, position: 'top',
	  	open: function(event,ui){
	  		
	  	},
	  	buttons: {
	  		"Cancelar" : function(){
	  			window.location = "/logistica/?op=saida2&p="+$(this).attr("p");
	  			$(this).dialog("close");
	  		},
	  		"Imprimir" : function(){
	  			
	  				var p = $("#formulario-check").attr("p");
	  				
	  				window.open('/logistica/checklist.php?p='+p);
	  			
	  			
		  			$(this).dialog("close");
	  			}
	  		}	  	
	  });
  
  $("#dialog-relatorio").dialog({
  	autoOpen: false, modal: true, position: 'top', height: 400, width: 600,
  	close : function(event,ui){ window.location = '?op=saida2&act=pendentes';}
  });
  
  $("#salvar-envio").click(function(){
  	if(validar_campos("div-envio-paciente")){
  		var itens = "";
  		var vazio = true;
  		$(".dados").each(function(i){
  			var sol = $(this).attr("sol");
  			var cod = $(this).attr("cod");
  			var qtd = $(this).find("input").val();
  			var n = $(this).attr("n");
  			var sn = $(this).attr("sn");
  			var p = $("#div-envio-paciente").attr("p");
  			var t = $(this).attr("tipo");
  			var soldata = $(this).attr("soldata");
  			var env = $(this).attr("env");
  			var total = $(this).attr("total");
  			if(qtd != ""){
  				vazio = false;
  				itens += sol+"#"+qtd+"#"+cod+"#"+n+"#"+sn+"#"+p+"#"+t+"#"+soldata+"#"+env+"#"+total+"$";
  			}
  		});
  		if(vazio){
  			alert("Pedido vazio!");
  		} else {
  			var dia = $("#data-envio").val();
  			var np = $("#div-envio-paciente").attr("np");
  			$.post("query.php",{query: "envio-paciente", dados: itens, dia: dia, np: np},function(r){
  				if(r == "-1"){
  					alert("ERRO: Tente mais tarde!");
  				} else {
  					$("#pre-relatorio").html(r);
  					$("#relatorio").val(r);
  					$("#dialog-relatorio").dialog("open");
  				}
  			});
  		}
  	}
  	return false;
  });
  
 
  
 /**************** BUSCA : RASTREAMENTO ************************/  
  $("input[name='tipo-rastreamento']").change(function(){
	$("#combo-rastreamento").html("");
	$.post("query.php", {query: "combo-rastreamento", tipo: $(this).val() }, function(retorno){
		$("#combo-rastreamento").html(retorno);
	});  
  });
  
 /**************** BUSCA : SAIDA ************************/ 
  
  $("input[name='tipo-busca-saida']").change(function(){
	$("#combo-busca-saida").html("");
	$.post("query.php", {query: "combo-busca-saida", tipo: $(this).val() }, function(retorno){
		$("#combo-busca-saida").html(retorno);
	});  
  });
 
 /**************** BUSCA : RELATORIOS ************************/
  
  $("#relatorios").click(function(){
    if(!validar_campos("div-relatorios")) return false;
    var cmd = "query.php?query=relatorio&paciente=" + $("select option:selected").val() + "&data=" + $("input[name='data']").val() + 
    "&pacientenome=" + $("select[name='paciente'] option:selected ").text();
    window.location = cmd;
    return false;
  });
  //////////////dialog///////////////
  
 
  
  
  
/**************** DEVOLUCAO ************************/  
  
  $("#del-devolucao").click(function(){
    $("#tab-devolucao tr td .select").each(function(i){
      if($(this).attr("checked")){
	$(this).parent().parent().remove();
      }
    });
    $("#tab-devolucao tr:odd").css("background-color","#ebf3ff");
    $("#tab-devolucao tr").hover(
	function(){$(this).css("background-color","#3d80df");$(this).css("color","#ffffff");},
	function(){
	  $(this).css("background-color","");$(this).css("color","");
	  $("#tab-devolucao tr:odd").css("background-color","#ebf3ff");
	  $("#tab-devolucao tr:odd").css("color","");
	}
      );
    return false;
  });
  
  $("#finalizar-devolucao").click(function(){
  	var itens = "";
  	var total = $("#tab-devolucao tr").size() - 1;
  	$(".itens").each(function(i){
		var cod = $(this).attr("id"); var qtd = $(this).attr("qtd"); 
		var paciente = $(this).attr("paciente"); var data = $(this).attr("data");
		var linha = cod+"#"+qtd+"#"+data+"#"+paciente;
		if(i<total - 1) linha += "$";
		itens += linha;
    });
    $.post("inserir.php",{req: "devolucao", dados: itens},
	   function(retorno){
	     if(retorno != '1'){
	       alert("ERRO: "+retorno);
	       return false;
	     }
	     alert("Sucesso!");
	     window.location = "?op=devolucao";
	});
  });
  
  $("#add-devolucao").click(function(){
    if(validar_campos("div-devolucao") && $("#id-novo-item").val() != '-1'){ 
      var qtd = $("#qtd-dev").val(); var nome = $("#busca-item").val();
      var id = $("#id-novo-item").val(); var data = $("input[name='data']").val(); var paciente = $("select option:selected").val();
      var dados = "id='"+id+"' paciente='"+paciente+"' data='"+data+"' qtd='"+qtd+"'";
      $("#tab-devolucao").append("<tr class='itens' "+dados+" ><td><input type='checkbox' name='select' class='select' /></td><td>"+
			   $("select option:selected").text()+"</td><td>"+nome+"</td><td>"+qtd+"</td></tr>");
      $("#tab-devolucao tr:odd").css("background-color","#ebf3ff");
      $("#tab-devolucao tr").hover(
		function(){$(this).css("background-color","#3d80df");$(this).css("color","#ffffff");},
		function(){
	  		$(this).css("background-color","");$(this).css("color","");
	  		$("#tab-devolucao tr:odd").css("background-color","#ebf3ff");
	  		$("#tab-devolucao tr:odd").css("color","");
		}
      );
      $("#qtd-dev").val(""); $("#busca-item").val("");$("#id-novo-item").val("-1");$("#busca-item").attr('readonly','');
      $("#busca-item").focus();
    }else if($("#id-novo-item").val() == '-1'){
  		$("#busca-item").css({"border":"3px solid red"});
  		$("#div-devolucao .aviso").text("Os campos em vermelho são obrigatórios!");
      	$("#div-devolucao .div-aviso").show();
  	}
    return false;
  });
    
/***************** ENTRADA DE MEDICAMENTOS E MATERIAIS ****************************/
  
  $("#checktodos-itens").click(function(){
    var checked_status = this.checked;
    $(".select-item").each(function()
    {
      this.checked = checked_status;
    });
  });
  
  $("#bonificados-itens").click(function(){
    var checked_status = this.checked;
    $(".bonificado-item").each(function()
    {
      this.checked = checked_status;
    });
  });
  
  $("#limpar-busca").click(function(){
  	$("#id-novo-item").val('-1');
  	$("#busca-item").attr('readonly','');
  	$("#busca-item").val('');
  	$("#qtd").val('');
  	$("#valor").val('');
  	return false;
  });
  
  $("#nome-busca-item").autocomplete({
			source: "/utils/busca_brasindice.php",
			minLength: 3,
			select: function( event, ui ) {
			}
  });
  
  $("#busca-item").autocomplete({
			source: "/utils/busca_brasindice.php",
			minLength: 3,
			select: function( event, ui ) {
					$("#id-novo-item").val(ui.item.id);
					$("#busca-item").attr('readonly','readonly');
					$("input[name='qtd']").focus();
			}
  });
  
  $("#adicionar-item").click(function(){
  	if(validar_campos("div-form-entrada") && $("#id-novo-item").val() != '-1'){
  		var cod = $("#id-novo-item").val(); 
  		var qtd = $("#qtd").val();
  		var item = $("#busca-item").val(); 
  		var valor = $("#valor").val();
  		var des_bon_item= verificaNaN($("#des_bon_item").val()); 
  		var lote =$("#lote").val(); 
  		var validade =$("#validade").val();
  		var cfop =$("#cfop").val();
  		var idx = $("#tabela-itens tr .itens").length + 1;
  		var boni = 0;
  		var check='';
  		
  		
  		if(des_bon_item != '0,00'){ boni = 1; check = 'checked';}
  		if($("#nota").attr("flag")==1){
  			$("#tabela-itens").append("" +
  				       "<tr bgcolor='#3D80DF' class='itens' bonificado='0' id='"+cod+"' qtd='"+qtd+"' valor='"+valor+"' lote='"+lote+"' des_bon_item='"+des_bon_item+"' validade='"+validade+"' cfop='"+cfop+"' >"+
  					   "<td><input type='checkbox' name='select' class='select-item' /></td>"+
  					   "<td>"+idx+"</td>"+
				       "<td>"+item+"</td>" +
				       "<td>"+qtd+"</td><td class='valor'>"+valor+"</td>" +
				       "<td>"+des_bon_item+"</td>"+
				       "<td><input type='checkbox' name='select' class='bonificado-item' "+check+" /></td>" +
				       "</tr>");
  		
  			
  		}else{
  			$("#tabela-itens").append("" +
				       "<tr class='itens' bonificado='0' id='"+cod+"' qtd='"+qtd+"' valor='"+valor+"' lote='"+lote+"' des_bon_item='"+des_bon_item+"' validade='"+validade+"' cfop='"+cfop+"' >"+
					   "<td><input type='checkbox' name='select' class='select-item' /></td>"+
					   "<td>"+idx+"</td>"+
				       "<td>"+item+"</td>" +
				       "<td>"+qtd+"</td><td class='valor'>"+valor+"</td>" +
				       "<td>"+des_bon_item+"</td>"+
				       "<td><input type='checkbox' name='select' class='bonificado-item' "+check+" /></td>" +
				       "</tr>");
  		}
		$("#id-novo-item").val('-1');
  		$("#busca-item").attr('readonly','');
  		$("#busca-item").val('');
  		$("#qtd").val(""); $("#valor").val("");
  		$("#busca-item").focus();
  		$("#lote").val('');
  		$("#des_bon_item").val('');
  		$("#cfop").val('');
  		var validade =$("#validade").val('');
  		var  valor_atual = parseFloat($("#nota").attr("valor_atual")) + parseFloat(valor.replace('.','').replace(',','.')) - parseFloat(des_bon_item.replace('.','').replace(',','.'));
	   
  		$("#nota").attr("valor_atual",valor_atual);
	    $("#vatual").html("R$ "+float2moeda(valor_atual));
  	}else if($("#id-novo-item").val() == '-1'){
  		$("#busca-item").css({"border":"3px solid red"});
  		$("#div-form-entrada .aviso").text("Os campos em vermelho são obrigatórios!");
      	$("#div-form-entrada .div-aviso").show();
  	}
  	return false;
  });
  
  $("#novaentrada").click(function() {
	/*
	  var produto= $("input[name='val_produtos']").val();
	 
	  $("input[name='val_produtos']").val(produto);
	  
	  var icms= verificaNaN($("input[name='icms']").val());
	  icms = parseFloat(icms.replace('.','').replace(',','.'));
	  $("input[name='icms']").val(icms);
	  
	  var frete= verificaNaN($("input[name='frete']").val());
	  frete = parseFloat(frete.replace('.','').replace(',','.'));
	  $("input[name='frete']").val(frete);
	  
	  
	  var seguro= verificaNaN($("input[name='seguro']").val());
	  seguro = parseFloat(seguro.replace('.','').replace(',','.'));
	  $("input[name='seguro']").val(seguro);
	  
	  var ipi= verificaNaN($("input[name='ipi']").val());
	  ipi= parseFloat(ipi.replace('.','').replace(',','.'));
	  $("input[name='ipi']").val(ipi);
	  
	  var issqn= verificaNaN($("input[name='issqn']").val());
	  issqn= parseFloat(issqn.replace('.','').replace(',','.'));
	  $("input[name='issqn']").val(issqn);
	  
	  var des_acessorias= verificaNaN($("input[name='des_acessorias']").val());
	  des_acessorias= parseFloat(des_acessorias.replace('.','').replace(',','.'));
	  $("input[name='des_acessorias']").val(des_acessorias);
	  
	  var des_bon = verificaNaN($("input[name='des_bon']").val());
	  des_bon= parseFloat(des_bon.replace('.','').replace(',','.'));
	  $("input[name='des_bon']").val(des_bon);
	  
	  var valor = verificaNaN($("#valor_nota").val());
	  valor= parseFloat(valor.replace('.','').replace(',','.'));
		*/  
	  
    if(!validar_campos("div-nova-entrada")){
      return false;
    }
  });
  
  function validate_entrada(){
    //if(parseFloat($("#nota").attr("valor")) == parseFloat($("#nota").attr("valor_atual")))
      // $("#finalizar_entrada").show();
     //else $("#finalizar_entrada").hide();
  }
  
  function atualiza_idx(){
    $(".itens").each(function(i){
	  idx++;
	  $(this).children("td").eq(1).html(idx);
    });
  }
  
  $("#remove-item").click(function(){
    $("#tabela-itens tr td .select-item").each(function(i){
	  if($(this).attr("checked")){
		  
	    var  valor_atual =  parseFloat($("#nota").attr("valor_atual")) - parseFloat($(this).parent().parent().attr("valor").replace('.','').replace(',','.')- parseFloat($(this).parent().parent().attr("des_bon_item").replace('.','').replace(',','.')));
	    $("#nota").attr("valor_atual",valor_atual.toFixed(2));
	    v = float2moeda(valor_atual);
	    $("#vatual").html("R$ "+v);
	    $(this).parent().parent().remove();
	  }
    });
    atualiza_idx();
    validate_entrada();
    return false;
  });
  
  $("#finalizar_entrada").click(function(){
	 var valorn =  $("#nota").attr("val_produtos");

	 valorn= valorn.replace('.','').replace(',','.');
	 valorn=parseFloat(valorn);
	
	 var valorat =  $("#nota").attr("valor_atual");
	
	 
	  if(valorn!=valorat){
		  
		  $("#dialog-confirmar").dialog("open");
		  
	  }else{
	  
  	var itens = "";
  	var total = $(".itens").size();
  	
  	$(".itens").each(function(i){
		var cod = $(this).attr("id"); var qtd = $(this).attr("qtd"); 
		var valor = $(this).attr("valor"); var bonificado = 0; var lote= $(this).attr("lote"); var des_bon_item = $(this).attr("des_bon_item");
		var validade =$(this).attr("validade");
		var cfop =$(this).attr("cfop");
		if($(this).find(".bonificado-item").is(":checked")) bonificado = 1;
		var linha = cod+"#"+qtd+"#"+valor+"#"+bonificado+"#"+lote+"#"+des_bon_item+"#"+validade+"#"+cfop;
		if(i<total - 1) linha += "$";
		itens += linha;
		
    });
  	
    $.post("inserir.php",{req: "nota", nota: $("#nota").attr("cod"), fornecedor: $("#nota").attr("fornecedor"), desc: $("#nota").attr("desc"),
    	 dados: itens, val_produtos: $("#nota").attr("val_produtos"), icms: $("#nota").attr("icms"), frete: $("#nota").attr("frete"), seguro: $("#nota").attr("seguro"),
    	 ipi: $("#nota").attr("ipi"), issqn: $("#nota").attr("issqn"), des_acessorias: $("#nota").attr("des_acessorias"), des_bon: $("#nota").attr("des_bon"),valor_nota: $("#nota").attr("valor"),chave: $("#nota").attr("chave")},
	   function(retorno){
	     if(retorno != '1'){
	       alert("ERRO: "+retorno);
	       return false;
	     }
	     $("#finalizar_entrada").hide();
    	 $("#dialog-finalizar").dialog("open");
	});
    
	  }
  });
  
  $("#dialog-confirmar").dialog({
	    
	  	autoOpen: false, 
	  	modal: true, 
	  	position: 'top',	  	
	  	buttons: {	  		
	  		"Fechar" : function(){
	  			$(this).dialog("close");
	  			return;
	  		}
   }
  });
  
 
  
  $("#dialog-finalizar").dialog({
    modal: true,
    autoOpen: false,
    buttons: {
      Ok: function() {
		$( this ).dialog( "close" );
		window.location = "?op=entrada";
      }
    },
    close: function(event,ui) {
		window.location = "?op=entrada";
    }
  });
 
 /**************** BUSCA : INVENTARIO ************************/
  
  $("#radio_med,#radio_mat").click(function(){
    var tipo;
    if($("#radio_med").attr("checked")){ tipo = $("#radio_med").val();}
    if($("#radio_mat").attr("checked")){ tipo = $("#radio_mat").val();}
    $("#div_inicio_inventario").load("query.php",{query: "inicio_inventario", tipo: tipo});
  });
  
  $("#gerar_inventario").click(function(){
    var tipo = "none";
    if($("#radio_med").attr("checked")){ tipo = $("#radio_med").val();}
    if($("#radio_mat").attr("checked")){ tipo = $("#radio_mat").val();}
    var nome = $("#inicio_inventario").val();
    var zerados = $("#zerados").attr("checked");
    var offset = $("#offset").val();
    if(tipo=="none") return false;
    var cmd = "query.php?query=inventario&tipo=" + tipo + "&zerados=" + zerados + "&nome=" + nome + "&offset=" + offset;
    window.location = cmd;
    return false;
  });
 
 /**************** ESPECIAIS ************************/
  
  $( "#dialog:ui-dialog" ).dialog( "destroy" );
  
  $( "#dialog" ).dialog({
    autoOpen: false,
    modal: true,
    buttons: {
      Ok: function() {
	$( this ).dialog( "close" );  
      }
    }
  });
  
  $("#dialog").dialog({
    buttons: {
      "Sim" : function(){
	$("#tabela_especiais tr td input").each(function(i){
	  if($(this).attr("checked") && i>0){
	    $.post("query.php",{query: "remover_especial", id: $(this).parent().parent().attr("idespecial") });
	    $(this).parent().parent().remove();
	  }
	});
	$(this).dialog("close");
      },
      "Não" : function(){
	$(this).dialog("close");
      }
    }
  });
  
  $("#remover_especial").click(function(){
    $("#dialog").dialog("open");
  });
  
  $("#pdf_especiais").click(function() {
    var venc = $("#vencimento2").val();
    if(venc != ""){
      var cmd = "query.php?query=pdf_especiais&venc=" + venc;
      window.location = cmd;
    } else {
      alert("Escolha uma data!");
    }
    return false;
  });
  
  $("#checktodos").click(function(){
    var checked_status = this.checked;
    $(".select").each(function()
    {
      this.checked = checked_status;
    });
  });
    
//  $("button, input:submit").button();
});

/*$("#editar_nota").click(function(){
	
	var notaVal  = $("#nota").attr("numero_nota");
	var valor= $("#nota").attr("valor_nota");
	var codFornecedor= $("#nota").attr("fornecedor");
	var descricao =$("#nota").attr("descricao");
	var val_produtos= $("#nota").attr("val_produtos");
	var icms= $("#nota").attr("icms");
	var frete = $("#nota").attr("frete");
	var seguro = $("#nota").attr("seguro");
	var ipi= $("#nota").attr("ipi");
	var issqn= $("#nota").attr("issqn");
	var des_acessorias= $("#nota").attr("des_acessorias");
	var des_bon= $("#nota").attr("des_bon");
	
	
	/*$.post("entrada.php",{op:"entrada_editar", nota: $("#nota").attr("cod"), fornecedor: $("#nota").attr("fornecedor"), desc: $("#nota").attr("desc"),
	  val_produtos: $("#nota").attr("val_produtos"), icms: $("#nota").attr("icms"), frete: $("#nota").attr("frete"), seguro: $("#nota").attr("seguro"),
	 ipi: $("#nota").attr("ipi"), issqn: $("#nota").attr("issqn"), des_acessorias: $("#nota").attr("des_acessorias"), des_bon: $("#nota").attr("des_bon")},function(r){*/
	/*$.post("entrada.php",{op:"entrada_editar", nota:notaVal,valor:valor,codFornecedor:codFornecedor,descricao:descricao,val_produtos:val_produtos,icms:icms,frete:frete,
		seguro:seguro,ipi:ipi,issqn:issqn,des_acessorias:des_acessorias, des_bon:des_bon},function(r){
			
				$("#edicao").html(r).sl;				
			
		});
	
});*/



function float2moeda(num) {

   x = 0;

   if(num<0) {
      num = Math.abs(num);
      x = 1;
   }
   if(isNaN(num)) num = "0";
      cents = Math.floor((num*100+0.5)%100);

   num = Math.floor((num*100+0.5)/100).toString();

   if(cents < 10) cents = "0" + cents;
      for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
         num = num.substring(0,num.length-(4*i+3))+'.'
               +num.substring(num.length-(4*i+3));
   ret = num + ',' + cents;
   if (x == 1) ret = ' - ' + ret;return ret;

}
