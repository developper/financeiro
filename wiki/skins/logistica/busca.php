<?php

require('../db/config.php');
include_once('../utils/codigos.php');

class Buscas{
  private function pacientes($id,$name){
    $result = mysql_query("SELECT * FROM clientes ORDER BY nome;");
    $var = "<p><select name='{$name}' style='background-color:transparent;' >";
    $var .= "<option value='todos' selected>Todos</option>";
    while($row = mysql_fetch_array($result))
    {
      if(($id <> NULL) && ($id == $row['idClientes']))
		$var .= "<option value='". $row['idClientes'] ."' selected>". $row['nome'] ."</option>";
      else
		$var .= "<option value='". $row['idClientes'] ."'>". $row['nome'] ."</option>";     
    }
    $var .= "</select>";
    return $var;
  }
  
  private function fornecedores($id,$name){
    $result = mysql_query("SELECT * FROM fornecedores ORDER BY razaoSocial");
    $var = "<p>";
    $var .= "<select name='{$name}' style='background-color:transparent;' >";
    while($row = mysql_fetch_array($result))
		$var .= "<option value='". $row['idFornecedores'] ."'>". $row['razaoSocial'] ."</option>";     
    $var .= "</select>";
    return $var;
  }
    
  public function form(){
  	echo "<center><h1>Buscas</h1></center>";
  	echo "<h1>Buscar Nota</h1>";
    echo "<form name=\"form\" action='?op=buscar&act=nota' method='POST'>";
    echo "<p><b>Nota</b><input type='text' name='numero' size='50'/>";
    echo $this->fornecedores(NULL,"codigo");
    echo "<input type='submit' value='Buscar' />";
    echo "</form>";
    echo "<h1>Buscar itens</h1>";
    echo "<form name=\"form\" action='?op=buscar&act=item' method='POST'>";
    //echo "<b>Tipo:</b><input type='radio' name='tipo' value='0' CHECKED />Medicamento<BR /><input type='radio' name='tipo' value='1' />Material";
    echo "<p><b>Item</b><input type='text' id='nome-busca-item' name='nome' size='50'/>";
    echo "<input type='submit' value='Buscar' />";
    echo "</form>";
    echo "<h1>Buscar sa&iacute;das</h1>";
    echo "<form name=\"form\" action='?op=buscar&act=saida' method='POST'>";
    echo "<b>Tipo:</b><input type='radio' name='tipo-busca-saida' value='med' />Medicamento 
    	 <input type='radio' name='tipo-busca-saida' value='mat' />Material 
    	 <input type='radio' name='tipo-busca-saida' value='paciente' CHECKED />Paciente";
    echo "<p><b>Resultado:</b><input type='radio' name='resultado' value='a' checked />Analítico<input type='radio' name='resultado' value='c' />Condensado";
    echo "<table><tr><td colspan='4'>";
    echo "<div id='combo-busca-saida'>".$this->pacientes(NULL,"codigo")."</div>";
    $hoje = date("j/n/Y");
    echo "</td></tr><tr><td><b>In&iacute;cio do per&iacute;odo:</b></td><td><input id='inicio' type='text' name='inicio' value='$hoje' maxlength='10' size='10' /></td>";
    echo "<td><b>Fim do per&iacute;odo:</b></td><td><input id='fim' type='text' name='fim' value='$hoje' maxlength='10' size='10' /></td></tr></table>";
    echo "<input type='submit' value='Buscar' />";
    echo "</form>";
    echo "<h1>Rastrear itens</h1>";
    echo "<form name=\"form\" action='?op=buscar&act=rastreamento' method='POST'>";
    echo "<b>Tipo:</b><input type='radio' name='tipo-rastreamento' value='med' />Medicamento 
    	 <input type='radio' name='tipo-rastreamento' value='mat' />Material 
    	 <input type='radio' name='tipo-rastreamento' value='fornecedor' CHECKED />Fornecedor";
    echo "<div id='combo-rastreamento'>".$this->fornecedores(NULL,"codigo")."</div>";
    echo "<p>De ".input('text','ientrada','ientrada',"class='data' size='10' maxlength='10' ")." At&eacute; ".input('text','fentrada','fentrada',"class='data' size='10' maxlength='10' ");
    echo "<p><input type='submit' value='Buscar' />";
    echo "</form>";
  }
  
  public function show_result($result){
  	echo "<center><h1>Resultado</h1></center>";
    echo "<table class='mytable' width=100% >"; 
    echo "<thead><tr>"; 
    echo "<th><b>Nome</b></th>";
    echo "<th><b>Segundo Nome</b></th>";
    echo "<th><b>Quantidade</b></th>"; 
    echo "<th><b>Valor</b></th>";
    echo "<th><b>Valor Total</b></th>";
    echo "</tr></thead>";
    $cor = false;
    $total = 0;
    $f = true;
    while($row = mysql_fetch_array($result)){ 
      $f = false;
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      if($cor) echo "<tr>";
      else echo "<tr class='odd'>"; 
      $cor = !$cor;
      echo "<td valign='top'>{$row['nome']}</td>";  
      echo "<td valign='top'>{$row['segundoNome']}</td>";  
      echo "<td valign='top'>{$row['quantidade']}</td>";
      echo "<td valign='top'>R$ " . round($row['valor'],2) . "</td>";
      echo "<td valign='top'>R$ " . round($row['quantidade']*$row['valor'],2) . "</td>";
      echo "</tr>";      
    }
    echo "</table>";
    if($f) echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
  }
  
  public function buscar($nome){
  	$condlike = "1";
    $flag = true;
	$strings = explode(" ",$nome);
	$comparar = "concat(principio,' ',apresentacao)";
	foreach($strings as $str){
		if($str !=  "") $condlike .= " AND {$comparar} LIKE '%$str%' ";
	}
    $sql = "";
    $sql = "SELECT principio as nome, apresentacao as segundoNome, estoque as quantidade, valorMedio as valor 
    		FROM brasindice WHERE {$condlike}";
    $result = mysql_query($sql) or trigger_error(mysql_error());
    $this->show_result($result);
  }
  
  public function buscar_nota($numero,$codigo){
    echo "<center><h1>Resultado</h1></center>";
  	echo "<table class='mytable' width=100% >"; 
    echo "<thead><tr>";
    echo "<th><b>Nome</b></th>";
    echo "<th><b>Fonecedor</b></th>";
    echo "<th><b>Data</b></th>";
    echo "<th><b>Quantidade</b></th>";
    echo "<th><b>Valor unit&aacute;rio</b></th>";
    echo "</tr></thead>";
    $cor = false;
    $sql = "";
    $sql = "SELECT e.qtd as quantidade, e.valor, e.data, f.razaoSocial as fornecedor, b.principio as nome, b.apresentacao as segundoNome 
    			FROM entrada as e, fornecedores as f, brasindice as b
    			WHERE f.idFornecedores = e.fornecedor AND b.numero_tiss = e.item AND nota = '{$numero}' AND f.idFornecedores = '{$codigo}'  
    			ORDER BY data DESC, nome, segundoNome, fornecedor, quantidade DESC, valor DESC";
    $result = mysql_query($sql) or trigger_error(mysql_error());
    $f = true;
    while($row = mysql_fetch_array($result)){ 
      $f = false;
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      if($cor) echo "<tr>";
      else echo "<tr class='odd'>"; 
      $cor = !$cor;
      $data = implode("/",array_reverse(explode("-",$row['data'])));  
      echo "<td valign='top'>{$row['nome']} - {$row['segundoNome']}</td>";  
      echo "<td valign='top'>{$row['fornecedor']}</td>";  
      echo "<td valign='top'>{$data}</td>";
      echo "<td valign='top'>{$row['quantidade']}</td>";
      echo "<td valign='top'> R$ {$row['valor']}</td>";
      echo "</tr>";      
    }
    echo "</table>";
    if($f) echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
  }
  
  public function buscar_saida($post){
  	$tipo = $post['tipo-busca-saida'];
  	$aSelect = ""; $aGroupBy = "";
  	extract($post,EXTR_OVERWRITE);
  	echo "<center><h1>Resultado</h1></center>";
    echo "<table class='mytable' width=100% >"; 
    echo "<thead><tr>";
    echo "<th><b>Paciente</b></th>";
    if($resultado == 'a'){
    	echo "<th><b>Data</b></th>";
    	$aSelect = ", s.data";
    	$aGroupBy = " s.data,";
    }
    echo "<th><b>Nome</b></th>";
    echo "<th><b>Segundo Nome</b></th>";
    echo "<th><b>Quantidade</b></th>";
    echo "</tr></thead>";
    $cor = false;
    $total = 0;
    $i = implode("-",array_reverse(explode("/",$inicio)));
    $f = implode("-",array_reverse(explode("/",$fim)));
    $cond = "s.idCliente = '$codigo'";
    if($codigo == "todos" ) $cond = "1";
    $sql = "SELECT p.nome as paciente, s.nome, s.segundoNome, sum(s.quantidade) as quantidade $aSelect FROM saida as s, clientes as p WHERE p.idClientes = s.idCliente AND $cond AND s.data BETWEEN '$i' AND '$f' GROUP BY $aGroupBy s.segundoNome, s.nome, p.nome ORDER BY s.data ASC";
    if($tipo == 'med' || $tipo == 'mat'){
    	$nomes = explode("#",$codigo);
    	$sql = "SELECT p.nome as paciente, s.nome, s.segundoNome, sum(s.quantidade) as quantidade $aSelect FROM saida as s, clientes as p WHERE p.idClientes = idCliente AND s.nome = '{$nomes[0]}' AND s.segundoNome = '{$nomes[1]}' AND s.data BETWEEN '$i' AND '$f' GROUP BY $aGroupBy s.segundoNome, s.nome, p.nome ORDER BY s.data ASC";
    }
    $result = mysql_query($sql) or trigger_error(mysql_error());
    $f = true;
    while($row = mysql_fetch_array($result)){ 
      $f = false;
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      if($cor) echo "<tr>";
      else echo "<tr class='odd'>"; 
      $cor = !$cor;
      echo "<td valign='top'>{$row['paciente']}</td>";
      if($resultado == 'a'){
      	$data = implode("/",array_reverse(explode("-",$row['data'])));
      	echo "<td valign='top'>{$data}</td>";  
      }
      echo "<td valign='top'>{$row['nome']}</td>";  
      echo "<td valign='top'>{$row['segundoNome']}</td>";  
      echo "<td valign='top'>{$row['quantidade']}</td>";
      echo "</tr>";      
    }
    echo "</table>";
    if($f) echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
  }
  
  public function rastrear($cod,$tipo,$ientrada,$fentrada){
  	echo "<center><h1>Resultado</h1></center>";
  	echo "<table class='mytable' width=100% >"; 
    echo "<thead><tr>";
    echo "<th><b>Nome</b></th>";
    echo "<th><b>Fonecedor</b></th>";
    echo "<th><b>Data</b></th>";
    echo "<th><b>Quantidade</b></th>";
    echo "<th><b>Valor unit&aacute;rio</b></th>";
    echo "</tr></thead>";
    $cor = false;
    $sql = "";
    $i = "1900-01-01";
    $f = "2200-01-01";
    if($ientrada != "") $i = implode("-",array_reverse(explode("/",$ientrada)));
    if($fentrada != "") $f = implode("-",array_reverse(explode("/",$fentrada)));
    if($tipo == "med"){
    	$sql = "SELECT e.qtd as quantidade, e.valor, e.data, f.razaoSocial as fornecedor, b.principio as nome, b.apresentacao as segundoNome 
    			FROM entrada as e, fornecedores as f, brasindice as b
    			WHERE f.idFornecedores = e.fornecedor AND b.numero_tiss = e.item AND data BETWEEN '{$i}' AND '{$f}' AND b.tipo = 0 AND b.numero_tiss = '{$cod}'  
    			ORDER BY data DESC, nome, segundoNome, fornecedor, quantidade DESC, valor DESC";
//    	$sql = "SELECT med.principioAtivo as nome, f.razaoSocial as fornecedor, n.dataEntrada as data, i.quantidade, i.valor, i.segundoNome 
//    			FROM itens as i, fornecedores as f, notas as n, medicacoes as med 
//    			WHERE i.codNota = n.codigo AND i.codOrigem = f.idFornecedores 
//    			AND n.codFornecedor = f.idFornecedores AND i.codItem = '{$cod}'
//    			AND i.tipo = '0' AND med.idMedicacoes = i.codItem AND n.dataEntrada BETWEEN '{$i}' AND '{$f}'
//    			ORDER BY n.dataEntrada DESC, nome, i.segundonome, f.razaoSocial, i.quantidade DESC, i.valor DESC";
    } else if($tipo == "mat"){
    	$sql = "SELECT e.qtd as quantidade, e.valor, e.data, f.razaoSocial as fornecedor, b.principio as nome, b.apresentacao as segundoNome 
    			FROM entrada as e, fornecedores as f, brasindice as b
    			WHERE f.idFornecedores = e.fornecedor AND b.numero_tiss = e.item AND data BETWEEN '{$i}' AND '{$f}' AND b.tipo = 1 AND b.numero_tiss = '{$cod}'   
    			ORDER BY data DESC, nome, segundoNome, fornecedor, quantidade DESC, valor DESC";
//    	$sql = "SELECT mat.nome as nome, f.razaoSocial as fornecedor, n.dataEntrada as data, i.quantidade, i.valor, i.segundoNome 
//    			FROM itens as i, fornecedores as f, notas as n, material as mat 
//    			WHERE i.codNota = n.codigo AND i.codOrigem = f.idFornecedores 
//    			AND n.codFornecedor = f.idFornecedores AND i.codItem = '{$cod}'
//    			AND i.tipo = '1' AND mat.idMaterial = i.codItem AND n.dataEntrada BETWEEN '{$i}' AND '{$f}'
//    			ORDER BY n.dataEntrada DESC, nome, i.segundonome, f.razaoSocial, i.quantidade DESC, i.valor DESC";
    } else if($tipo == "fornecedor"){
    	$sql = "SELECT e.qtd as quantidade, e.valor, e.data, f.razaoSocial as fornecedor, b.principio as nome, b.apresentacao as segundoNome 
    			FROM entrada as e, fornecedores as f, brasindice as b
    			WHERE f.idFornecedores = e.fornecedor AND b.numero_tiss = e.item AND data BETWEEN '{$i}' AND '{$f}' AND f.idFornecedores = '{$cod}'  
    			ORDER BY data DESC, nome, segundoNome, fornecedor, quantidade DESC, valor DESC";
//    	$sql = "SELECT DISTINCT if( i.tipo = '1', mat.nome, med.principioAtivo ) as nome, f.razaoSocial as fornecedor, n.dataEntrada as data, i.* 
//    			FROM itens as i, fornecedores as f, notas as n, medicacoes as med, material as mat 
//    			WHERE i.codNota = n.codigo AND i.codOrigem = '{$cod}' 
//    			AND n.codFornecedor = '{$cod}' AND f.idFornecedores = '{$cod}' AND n.dataEntrada BETWEEN '{$i}' AND '{$f}'
//    			AND ((i.tipo = '0' AND med.idMedicacoes = i.codItem) OR (i.tipo = '1' AND mat.idMaterial = i.codItem))
//    			ORDER BY n.dataEntrada DESC, nome, i.segundonome, i.quantidade DESC, i.valor DESC";
    } 
  	$result = mysql_query($sql) or trigger_error(mysql_error());
    $f = true;
    while($row = mysql_fetch_array($result)){ 
      $f = false;
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      if($cor) echo "<tr>";
      else echo "<tr class='odd'>"; 
      $cor = !$cor;
      $data = implode("/",array_reverse(explode("-",$row['data'])));  
      echo "<td valign='top'>{$row['nome']} - {$row['segundoNome']}</td>";  
      echo "<td valign='top'>{$row['fornecedor']}</td>";  
      echo "<td valign='top'>{$data}</td>";
      echo "<td valign='top'>{$row['quantidade']}</td>";
      echo "<td valign='top'> R$ {$row['valor']}</td>";
      echo "</tr>";      
    }
    echo "</table>";
    if($f) echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
//  	echo $cod." ".$tipo." ".$sql;
  }
}

?>