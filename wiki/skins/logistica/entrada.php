<?php
require('../db/config.php');

class Entrada{
  
  private function fornecedores($id){
    $result = mysql_query("SELECT * FROM fornecedores ORDER BY razaoSocial;");
    echo "<p><b>Fornecedor:</b><br/>";
    echo "<select name='codFornecedor' style='background-color:transparent;' class='COMBO_OBG' >";
    echo "<option value='-1'>SELECIONE</option>";
    while($row = mysql_fetch_array($result))
    {
      if(($id <> NULL) && ($id == $row['idFornecedores']))
	echo "<option value='". $row['idFornecedores'] ."' selected>". $row['razaoSocial'] ."</option>";
      else
	echo "<option value='". $row['idFornecedores'] ."'>". $row['razaoSocial'] ."</option>";     
    }
    echo "</select>";
  }
  
  public function xml_nota(){
  	echo "<div id='div-xml-nota' title='Importar Nota'>";
  	echo "<form name='xml' action='?op=importar' method='post' enctype='multipart/form-data'>";
  	echo "<div style='width:53px;float:left;display:table;padding-right:10px;'><label><b>Arquivo:</b></label></div>";
  	echo"<div style='display:table;width:200px;padding-right:100px;' ><input name='xml' type='file'/></div>";
  	echo"<br/><button id='enviar_xml_nota' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
    <span class='ui-button-text' id='enviar_xml_nota'>Enviar</span></button></div></form></div>";
 
  }
  
/************************************************
 * ROTINA DE IMPORTA��O DE NOTAS ATRAV�S DO XML *
 ************************************************/  
  
  public function importar($file){
  	
  	// Pasta onde o arquivo vai ser salvo
  	$nota['pasta'] = 'xml/';
  	
  	// Tamanho m�ximo do arquivo (em Bytes)
  	$nota['tamanho'] = 1024 * 1024 *1024 * 1024 *1024 * 1024 * 2; 
  	
  	// Array com as extens�es permitidas
  	$nota['extensoes'] = array('xml');
  	
  	// Renomeia o arquivo? (Se true, o arquivo ser� salvo como .xml e um nome �nico)
  	$nota['renomeia'] = false;
  	
  	// Array com os tipos de erros de upload do PHP
  	$nota['erros'][0] = 'Importada com Sucesso!';
  	$nota['erros'][1] = 'O arquivo no upload � maior do que o permitido';
  	$nota['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado';
  	$nota['erros'][3] = 'O upload do arquivo foi feito parcialmente';
  	$nota['erros'][4] = 'N�o foi feito o upload do arquivo!';
  	
  	// Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
  	if ($file['xml']['error'] != 0) {
  		die("N�o foi poss�vel fazer o upload, erro:<br />" . $nota['erros'][$file['xml']['error']]);
  		exit; // Para a execu��o do script
  	}
  	
  	// Caso script chegue a esse ponto, n�o houve erro com o upload e o PHP pode continuar
  	
  	// Faz a verifica��o da extens�o do arquivo
  	$extensao = strtolower(end(explode('.', $file['xml']['name'])));
  	
  	if (array_search($extensao, $nota['extensoes']) === false) {
  		echo "Por favor, envie arquivos com no formato XML";
  	}
  	
  	// Faz a verifica��o do tamanho do arquivo
  	else if ($nota['tamanho'] < $file['xml']['size']) {
  		echo "O arquivo enviado � muito grande, envie arquivos de at� 2Mb.";
  	}
  	
  	// O arquivo passou em todas as verifica��es, hora de tentar mov�-lo para a pasta
  	else {
  		// Primeiro verifica se deve trocar o nome do arquivo
  		if ($nota['renomeia'] == true) {
  			// Cria um nome baseado no UNIX TIMESTAMP atual e com extens�o .jpg
  			$nome_final = time().'.jpg';
  		} else {
  			// Mant�m o nome original do arquivo
  			$nome_final = $file['xml']['name'];
  		}
  	
  		// Depois verifica se � poss�vel mover o arquivo para a pasta escolhida
  		if (@move_uploaded_file($file['xml']['tmp_name'], $nota['pasta'] . $nome_final)) {
  			// Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
  			//echo "Upload efetuado com sucesso!";
  			//echo '<br /><a href="' . $nota['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
  			$arq_xml = $nota['pasta'] . $nome_final; 	
  				
  			
  			$dados = $this->processar_importacao($arq_xml);
  			
  			$this->show_itens($dados[numero_nota],$dados[valor_nota],$dados[fornecedor],'Nota Importada '.date("d/m/Y"),$dados[val_produtos],$dados[icms],$dados[frete],$dados[seguro],$dados[ipi],$dados[issqn],$dados[outros],$dados[des_bon],$dados[produtos],$dados[chave]);
  		} else {
  			// N�o foi poss�vel fazer o upload, provavelmente a pasta est� incorreta
  			echo "N�o foi poss�vel enviar o arquivo, tente novamente";
  		}
  	
  	}
  			return $dados;
  }
  
  public function processar_importacao($arq_xml){
  	$xml1 = @simplexml_load_file($arq_xml);  			

  	//ID
  	$numero_nota = $xml1->NFe->infNFe->ide->nNF;
  	//chave
  	foreach($xml1->NFe->infNFe[0]->attributes() as $id=>$val){  		
  		if($id=="Id")
  			$chave = $val;
  	}
  		
  	  

  	//DADOS DO FORNECEDOR	
  	$fornecedor = $xml1->NFe->infNFe->emit->xNome;  
  	$fantasia = $xml1->NFe->infNFe->emit->xFant;
  	$cnpj =  $xml1->NFe->infNFe->emit->CNPJ;
  	$cidade =  $xml1->NFe->infNFe->emit->enderEmit->xMun;
  	$logradouro =  $xml1->NFe->infNFe->emit->enderEmit->xLgr;
  	$bairro =  $xml1->NFe->infNFe->emit->enderEmit->xBairro;
  	$telefone =  $xml1->NFe->infNFe->emit->enderEmit->fone;
  	$email =  $xml1->NFe->infNFe->emit->enderEmit->xBairro;
  	
  	$des_bon =  (double) $xml1->NFe->infNFe->total->ICMSTot->vDesc;
  	$valor_nota = (double) $xml1->NFe->infNFe->total->ICMSTot->vNF;
  	$outros = (double) $xml1->NFe->infNFe->total->ICMSTot->vOutro;
  	  	
  	//IMPOSTOS
  	$icms = (double) $xml1->NFe->infNFe->total->ICMSTot->vST;
  	$frete = (double) $xml1->NFe->infNFe->total->ICMSTot->vFrete;
  	$seguro = (double) $xml1->NFe->infNFe->total->ICMSTot->vSeg;
  	$ipi = (double) $xml1->NFe->infNFe->total->ICMSTot->vIPI;
  	$val_produtos =  (double) $xml1->NFe->infNFe->total->ICMSTot->vProd;
  	if($xml1->NFe->infNFe->total->ICMSTot->vISSQN)
  		$issqn =  (double) $xml1->NFe->infNFe->total->ICMSTot->vISSQN;
  	else   	
  		$issqn = 0.00;

  	

  	
  	foreach($xml1->NFe->infNFe->det as $xmlProd){
  		
  		$produtos[]= $xmlProd->prod->xProd."#".$xmlProd->prod->vProd."#".$xmlProd->prod->qCom."#".$xmlProd->prod->CFOP;
  		
  	}
  	
  	
  	$forn = $this->verificar_fornecedor($cnpj);
  	
  	
  	if($forn==0){  		
  		$fornecedor = $this->cadastrar_fornecedor($fornecedor,$fantasia,$cnpj,$telefone,$logradouro,$bairro,$cidade);  		
  	}else{
  		$fornecedor = $this->buscar_fornecedor($cnpj);
  	}
  	
  	$dados = array(
	  			"numero_nota"=>$numero_nota,
	  			"fornecedor"=>(int) $fornecedor,
	  			"cnpj"=>$cnpj,
	  			"des_bon"=>number_format($des_bon,2,",","."),
	  			"valor_nota"=>number_format($valor_nota,2,",","."),
	  			"icms"=>number_format($icms,2,",","."),
	  			"frete"=>number_format($frete,2,",","."),
	  			"seguro"=>number_format($seguro,2,",","."),
	  			"ipi"=>number_format($ipi,2,",","."),
	  			"val_produtos"=>number_format($val_produtos,2,",","."),
	  			"issqn"=>number_format($issqn,2,",","."),
  			    "outros"=>number_format($outros,2,",","."),
  			    "chave"=>$chave, 
  			    "produtos"=>$produtos 
  			);
  	
  
  	  	return $dados;
  }
  
  public function verificar_fornecedor($cnpj){
  	$sql = "Select count(*) as total from fornecedores where cnpj =$cnpj LIMIT 1";  	 	
  	$res = mysql_query($sql);
  	while($row = mysql_fetch_array($res))
  	{
  		return $row['total'];
  	}
  }
  
  public function verificar_cidade($cidade){
	  	$sql = "Select ID from CIDADES where NOME = '{$cidade}' LIMIT 1";
	  	$res = mysql_query($sql);
	  	if($res){
	  	while($row = mysql_fetch_array($res))
	  	{
	  		return $row['ID'];
	  	}
	  }
  }
  
  public function cadastrar_fornecedor($fornecedor,$fantasia,$cnpj,$telefone,$logradouro,$bairro,$cidade){
  	$cid_cod = $this->verificar_cidade($cidade);  
  	$sql = "INSERT INTO fornecedores (nome,razaoSocial,cnpj,telefone,endereco,bairro,cidade_id) VALUES('{$fornecedor}','{$fantasia}','{$cnpj}','{$telefone}','{$logradouro}','{$bairro}','{$cid_cod}')";
  	
  	$res = mysql_query($sql);
  	
 
  	return mysql_insert_id();	
  }
  
  public function buscar_fornecedor($cnpj){
  	$sql = "Select idFornecedores from fornecedores where cnpj = '{$cnpj}' LIMIT 1";
  	$res = mysql_query($sql);
  	if($res){
  		while($row = mysql_fetch_array($res))
  		{
  			return $row['idFornecedores'];
  		}
  	}
  }
  
  /********************* FIM CADASTRAR NOTA **************************/  
  
  public function select_nota(){
   
  	echo "<div id='div-nova-entrada'>";
    echo alerta(); 
    echo "<div style='display:table;width:200px;'><button id='xml_nota' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='button' role='button' aria-disabled='false'>
    <span class='ui-button-text' id='xml_nota'>Importar</span></button></div>";
    $this->xml_nota();
    echo "<form name='form' action='' method='POST'>";
    echo "<div style='width:200px;float:left;display:table;'><b>N&uacute;mero da nota:</b><input type='text' class='OBG' id='numero_nota' name='nota'/></div>";
    echo"<div style='display:table;width:200px;'>". $this->fornecedores(null)."</div>";   
    echo "<div style='width:200px;float:left;display:table;'><b>Descri&ccedil;&atilde;o da Compra:</b><input type='text' id='descricao' name='descricao' size='100' /></div>";
    echo"<table>";
    echo"<tr><td><b>ICMS:</b></td><td><b>Frete:</b></td><td><b>Seguro:</b></td><td><b>Valor dos Produtos:</b></td></tr>";
    echo"<tr><td><input id='icms' name='icms' class='valor'style='text-align:right' /></td>
    <td><input id='frete'  name='frete' style='text-align:right' class='valor' /></td>
    <td><input  id='seguro' name='seguro' style='text-align:right' class='valor'/></td>
    <td><input id='val_produtos'  name='val_produtos' class='valor OBG' style='text-align:right' /></td></tr>";
    echo"<tr><td><b>IPI:</b></td><td><b>ISSQN:</b></td><td><b>Despesas Acess&oacute;rias:</b></td>
    <td><b>Desconto/Bonifica&ccedil;&atilde;o</b></td></tr>";
    echo"<tr><td><input id='ipi' class='valor' name='ipi' style='text-align:right'  /></td>
    <td><input id='issqn' name='issqn'  style='text-align:right' class='valor'/></td>
    <td><input id='des_acessorias'  class='valor' name='des_acessorias' style='text-align:right'/></td>
    <td><input id='des_bon' name='des_bon' style='text-align:right'  class='valor'/></td></tr>";
   echo"</table>";
    echo "<p><b>Valor da nota:</b><input type='text' class='valor' value='0' id='valor_nota' name='valor_nota'  readonly style='text-align:right'/>";
    echo "<button id='novaentrada' name='novaentrada' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
    <span class='ui-button-text'>Lan&ccedil;ar Produtos</span></button>";
    ///echo "<input id='novaentrada' type='submit' value='Lan&ccedil;ar Produtos.' />";
    echo "</form>";
    echo "</div>";
  }
  
  public function select_edt_nota(){
  
  	echo "<div id='div-nova-entrada1'>";
  	echo alerta();
  	
  	echo "<div style='width:200px;float:left;display:table;'><b>N&uacute;mero da nota:</b><input type='text' class='OBG' id='numero_nota' name='nota'/></div>";
  	echo"<div style='display:table;width:200px;'>". $this->fornecedores(null)."</div>";
  	echo "<p><b>Descri&ccedil;&atilde;o da Compra:</b><input type='text' id='descricao' name='descricao' size='100' />";
  	echo"<table>";
  	echo"<tr><td><b>ICMS:</b></td><td><b>Frete:</b></td><td><b>Seguro:</b></td><td><b>Valor dos Produtos:</b></td></tr>";
  	echo"<tr><td><input id='icms' name='icms' class='valor'style='text-align:right' /></td>
  	<td><input id='frete'  name='frete' style='text-align:right' class='valor' /></td>
  	<td><input  id='seguro' name='seguro' style='text-align:right' class='valor'/></td>
  	<td><input id='val_produtos'  name='val_produtos' class='valor OBG' style='text-align:right' /></td></tr>";
  	echo"<tr><td><b>IPI:</b></td><td><b>ISSQN:</b></td><td><b>Despesas Acess&oacute;rias:</b></td>
  	<td><b>Desconto/Bonifica&ccedil;&atilde;o</b></td></tr>";
  	echo"<tr><td><input id='ipi' class='valor' name='ipi' style='text-align:right'  /></td>
  	<td><input id='issqn' name='issqn'  style='text-align:right' class='valor'/></td>
  	<td><input id='des_acessorias'  class='valor' name='des_acessorias' style='text-align:right'/></td>
  	<td><input id='des_bon' name='des_bon' style='text-align:right'  class='valor'/></td></tr>";
  	echo"</table>";
  	echo "<p><b>Valor da nota:</b><input type='text' class='valor' value='0' id='valor_nota' name='valor_nota'  readonly style='text-align:right'/>";
  	
  	echo "<button id='novaentrada1'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false'>
  	<span class='ui-button-text'>Lan&ccedil;ar Produtos</span></button>";
  	///echo "<input id='novaentrada' type='submit' value='Lan&ccedil;ar Produtos.' />";
  	
  	echo "</div>";
  }
  
  
  public function show_itens($nota,$valor_nota,$fornecedor,$descricao,$val_produtos,$icms,$frete,$seguro,$ipi,$issqn,$des_acessorias,$des_bon,$produtos=null,$chave=null){
  	if(is_null($produtos)){
  		$flag = 0;
  		$valor_atual = 0.00;
  	}else{
  		$flag = 1;
  		$cont = 0;
  		$valor = 0;
  		foreach($produtos as $prod){
  			
  			list($desc,$val,$qtd,$cfop) = explode("#",$prod);
  		
  			$qtd = (int) round($qtd,1);
  					
  			$valor += $val;
  		}
  		$valor_atual = (double) round($valor,2);
  	}
  	
  	echo "<div id='t'></div>";
    $titulo="Lan�amentos de Itens em Estoque";
    $this->select_edt_nota();
    echo "<div id='edicao'></div>";
    echo "<center><h1>".utf8_encode($titulo)."</h1></center>";
    echo "<div id='dialog-finalizar' title='Sucesso!' >";
    echo "<p><span class='ui-icon ui-icon-circle-check' style='float:left; margin:0 7px 50px 0;'></span>";
    echo "A entrada no estoque foi conclu&iacute;da com sucesso!</p>";
    echo "</div>";
    echo "<div id='dialog-confirmar' title='Confirmar'>";
    echo "<center><h3>Valor atual diferente do valor da nota.</h3></center>";
    // echo "<form action='imprimir.php' method='post' target='_blank'><input type='hidden' value='-1' name='prescricao'><center></center></form>";
    echo "</div>";
   
    
    echo "<div id='nota' cod='$nota' fornecedor='$fornecedor' valor='$valor_nota' desc='$descricao'  val_produtos='$val_produtos' icms='$icms' frete='$frete' seguro='$seguro'
    ipi='$ipi'  issqn='$issqn' des_acessorias='$des_acessorias'  des_bon='$des_bon' valor_atual='{$valor_atual}' tmed=0 tmat=0 flag='{$flag}' chave='{$chave}'>";
    echo "<table width=100% class='mytable'><thead><tr><th>C&oacute;digo da nota</th><th>Valor dos Produtos</th><th>Valor Lan&ccedil;ado</th></tr></thead>";
    echo "<tr class='odd'><td id='codnota'><b>$nota</b></td><td id='vnota'><b>R$ $val_produtos</b></td><td id='vatual' >R$ ".number_format($valor_atual,2,",",".")."</td></tr></table>";
   /* echo "<button id=finalizar_entrada class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='button' role='button' aria-disabled='false'>
    <span class='ui-button-text'>Finalizar</span></button>";*/
    
    ///echo "<button id=finalizar_entrada>Finalizar</button>";
    
    echo "<div id='div-form-entrada' >";
    echo alerta();
    echo "<table><tr><td><b>Item: </b></td><td><b>Lote:</b></td><td><b>Validade:</b></td></tr>";
    echo "<tr><td><input type='text' name='busca-item' class='OBG' id='busca-item' size='80' /></td>
    <td><input type='text' name='lote' id='lote' size='15' /></td>
    <td><input type='text' name='validade' class='data' id='validade' /></td></tr></table>";
    ///<td><input type='text' name='busca-item' class='OBG' id='busca-item' size='80' /></td>
    /*<button id='limpar-busca'>limpar</button>*/
    echo "<input type='hidden' id='id-novo-item' value='-1' />";
    echo "</br><table><tr><td><b>Unidade:</b></td><td><b>CFOP:</b></td></tr>
    <tr><td><input id='qtd' type='text' class='OBG numerico' name='qtd' size='10' style='text-align:right' /></td>
    <td><input id='cfop' type='text' class='OBG numerico' name='cfop' size='10' style='text-align:right' /></td></tr>";
    echo "<tr><td><b>Valor Total do item:</b></td><td><b>Desconto/Bonifica&ccedil;&atilde;o:</b></td></tr>
    <tr><td><input id='valor' type='text' class='valor OBG' name='valor' size='10' style='text-align:right' /></td>
    <td><input id='des_bon_item' type='text' class='valor' name='des_bon_item' size='10' style='text-align:right' /></td></tr>";      
    echo "</table></div>";
    echo "</br><button id='limpar-busca' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='button' role='button' aria-disabled='false'>
    <span class='ui-button-text'>Limpar</span></button>";
   
    echo "<button id='adicionar-item' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='button' role='button' aria-disabled='false'>
    <span class='ui-button-text'>+Incluir</span></button>";
    echo "<button id='editar_nota' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='button' role='button' aria-disabled='false'>
    <span class='ui-button-text' id='editar_nota'>Editar</span></button>";
    
    
    echo "</div>";
   /// echo "<br/><button id='adicionar-item'>Adicionar</button>";
    
    echo "<h3>Itens</h3>";
    ///echo "<button id='remove-item'>Remover</button>";
    echo "<button id='remove-item' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='button' role='button' aria-disabled='false'>
    <span class='ui-button-text'>Remover</span></button>";
    echo "<table id='tabela-itens' class='mytable' width=100% >"; 
    echo "<thead><tr>"; 
    echo "<th width=1%><input type='checkbox' id='checktodos-itens' /></th>";
    echo "<th width=1%><b>N&ordm;</b></th>";
    echo "<th><b>Item</b></th>";
    echo "<th width=9%><b>Quantidade</b></th>";
    echo "<th width=9%><b>Valor</b></th>";
    echo "<th width=9%><b>Desconto/Bonifica&ccedil;&atilde;o</b></th>";
    echo "<th width=1%><input type='checkbox' id='bonificados-itens' /></th>";
    echo "</tr></thead>";
    if($flag==1){
    	foreach($produtos as $prod){
    		$cont++;
    		list($desc,$valor,$qtd,$cfop) = explode("#",$prod);
    		
    		$qtd = (int) round($qtd,1);
    		$valor = (double) round($valor,2);
    		$valor = number_format($valor,2,",",".");
    		
    		echo "<tr class='itens' cfop='{$cfop}' bonificado='0' id='' qtd='{$qtd}' valor='{$valor}' lote='{$lote}' des_bon_item='0' validade='' >
    		<td><input type='checkbox' name='select' class='select-item' /></td>
    		<td>{$cont}</td>
    		<td>{$desc}</td>
    		<td>{$qtd}</td>
    		<td class='valor'>{$valor}</td>
    		<td class='valor'>0,00</td>
    		<td><input type='checkbox' name='select' class='bonificado-item'/></td>
    		</tr>";
    }
    
    }
    echo "</table>";
   
    echo "<button id='finalizar_entrada' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='button' role='button' aria-disabled='false'>
    <span class='ui-button-text'>Finalizar</span></button>";
    echo "</div>";
  }
 
  public function remove_item($id){
    mysql_query("DELETE FROM `itens` WHERE `idItens` = '$id' ") ;
  }
}

?>