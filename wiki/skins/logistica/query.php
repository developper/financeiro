<?php
$id = session_id();
if(empty($id))
  session_start();

require_once('../db/config.php');
require_once('../utils/codigos.php');
include_once('../utils/mpdf/mpdf.php');

function inicio_inventario($tipo){
  if($tipo == 0){
    $sql = $sql = "SELECT numero_tiss as cod, concat(`principio`,' - ',`apresentacao`) as nome FROM brasindice WHERE tipo = 0 ORDER BY nome ASC";
  } else if($tipo == 1){
    $sql = $sql = "SELECT numero_tiss as cod, concat(`principio`,' - ',`apresentacao`) as nome FROM brasindice WHERE tipo = 1 ORDER BY nome ASC";
  }
  $result = mysql_query($sql);
  echo "<p><b>Nome:</b><br/>";
  echo "<select name='nome' id='inicio_inventario' style='background-color:transparent;' >";
  while($row = mysql_fetch_array($result))
  {
    foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
    echo "<option value='". $row['nome'] ."'>". $row['nome'] ."</option>";
  }
  echo "</select>";
}

function inventario($tipo,$zerados,$offset,$inicio){
  $sql = ""; $print = "<html><body>";
  if($zerados == "true") $condicao = "1"; else $condicao = " estoque > '0' ";
  if($tipo == 0){
      $sql = "SELECT principio, apresentacao, estoque as qtd FROM brasindice   
	      WHERE concat(`principio`,' - ',`apresentacao`) >= '$inicio' AND tipo = 0 AND $condicao  
	      ORDER BY LOWER(principio), LOWER(apresentacao) ASC LIMIT 0,$offset";
  } else if($tipo == 1){
    $sql = "SELECT principio, apresentacao, estoque as qtd FROM brasindice   
	      WHERE concat(`principio`,' - ',`apresentacao`) >= '$inicio' AND tipo = 1 AND $condicao  
	      ORDER BY LOWER(principio), LOWER(apresentacao) ASC LIMIT 0,$offset";
  }
  $print .= "<table border=1 cellspacing=0 cellpadding=0 >".
  			"<thead><tr><th>N</th><th>Nome</th><th>Apresenta&ccedil;&atilde;o</th><th>Estoque</th><th>OBS</th></tr></thead>";
  $result = mysql_query($sql);
  $c = 0;
//  echo $sql;
  while($row = mysql_fetch_array($result))
  {
    $c++;
    $print .= "<tr>";
    foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
    $print .= "<td>$c</td>";
    $print .= "<td>". $row['principio']. "</td><td>". $row['apresentacao']. "</td><td>" . $row['qtd'] . "</td>";
    $print .= "<td width=25% >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
  }
  $print .= "</table></body></html>";
  $html = $c."<br>".$print;
  $mpdf=new mPDF('pt','A4',9);
  $mpdf->WriteHTML($print);
  $mpdf->Output('inventario.pdf','I');
  exit;
}

function pdf_especiais($venc){
  $sql = "SELECT * FROM especiais WHERE vencimento <= '$venc' ORDER BY vencimento ASC";
  $print = "<html><body>";
  $result = mysql_query($sql);
  $print .= "<table border=0 width=100% >";
  $print .= "<thead><tr><th><b>Princ&iacute;pio Ativo</b></th><th><b>Vencimento</b></th><th><b>Lote</b></th><th><b>Quantidade</b></th></tr></thead>";
  while($row = mysql_fetch_array($result))
  {
    foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
    $vencimento = implode("/",array_reverse(explode("-",$row['vencimento'])));
    $print .= "<tr><th>{$row['principio']}</td><td>{$vencimento}</td><td>{$row['lote']}</td><td>{$row['qtd']}</td></tr>";
  }
  $print .= "</table></body></html>";
  $mpdf=new mPDF('pt','A4',9);
  $mpdf->WriteHTML($print);
  $mpdf->Output('especiais.pdf','I');
  exit;
}

function remover_especial($id){
  mysql_query("DELETE FROM especiais WHERE id='$id' ");
}

function opcoes_itens($post) {
  extract($post,EXTR_OVERWRITE);
  echo "<table id='tabela_itens' class='mytable' width=100% >"; 
  echo "<thead><tr>"; 
  echo "<th><b>Princ&iacute;pio Ativo</b></th>";
  echo "<th><b>Apresenta&ccedil;&atilde;o</b></th>";
  echo "</tr></thead>";
  $cor = false;
  $total = 0;
  $sql = "SELECT DISTINCT med.principioAtivo as nome, com.nome as apr, com.idNomeComercialMed as item FROM medicacoes as med, nomecomercialmed as com 
	 WHERE (med.principioAtivo LIKE '%$like%' OR com.nome LIKE '%$like%' OR com.codigoBarras = '$like') AND med.idMedicacoes = com.Medicacoes_idMedicacoes 
	 ORDER BY med.principioAtivo";
  if($tipo == "1")
    $sql = "SELECT DISTINCT mat.idMaterial as item, mat.nome as nome, mat.descricao as apr FROM material as mat 
			  WHERE (mat.nome LIKE '%$like%' OR mat.descricao LIKE '%$like%' OR mat.codigoBarras = '$like') ORDER BY mat.nome";
  $result = mysql_query($sql) or trigger_error(mysql_error());
  while($row = mysql_fetch_array($result)){ 
    foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
    if($cor) echo "<tr pnome='{$row['nome']}' snome='{$row['apr']}' cod='{$row['item']}' >";
    else echo "<tr pnome='{$row['nome']}' snome='{$row['apr']}' cod='{$row['item']}' class='odd'>";
    $cor = !$cor;
    echo "<td valign='top'>" . $row['nome'] . "</td>";
    echo "<td valign='top'>" . $row['apr'] . "</td>";
    echo "</tr>";
  }
  echo "</table>";
  echo "<script type='text/javascript' >";
  echo " $('#tabela_itens tr').dblclick(function(){
	      $('#nome-item').val($(this).attr('pnome'));
	      $('#apr-item').val($(this).attr('snome'));
	      $('#item').val($(this).attr('cod'));
	      $('#op').html('');
	      $('#qtd-dev').focus();
	      $('#busca-item').val('');
	  });";
    echo "</script>";
}

function  salvar_devolucao($post) {
  extract($post,EXTR_OVERWRITE);
  $dta = implode("-",array_reverse(explode("/",$data)));
  mysql_query("begin");
  $sql = "INSERT INTO saida (tipo, nome, segundoNome, quantidade, idCliente, idUsuario, data, obs)  
	  VALUES ('{$tipo}','{$nome}','{$snome}','-{$qtd}','{$paciente}','{$_SESSION['id_user']}','{$dta}','Retorno') ";
  $a = mysql_query($sql);
  if($tipo == 0)
    $sql = "UPDATE nomecomercialmed SET estoque = (estoque + $qtd) WHERE idNomeComercialMed = '$id' ";
  else $sql = "UPDATE material SET estoque = (estoque + $qtd) WHERE idMaterial = '$id' ";
  $b = mysql_query($sql);
  if($a && $b){
    echo "OK";
    mysql_query("commit");
  }
  else{
    echo "erro";
    mysql_error("rollback");
  }
}


function relatorio($get){
    extract($get,EXTR_OVERWRITE);
    $dta = implode("-",array_reverse(explode("/",$data)));
    $tabela = "<table border=1 width=100% style='font-size:x-small' >";
    $tabela .= "<tr><th><b>Nome</b></th>";
    $tabela .= "<th><b>Apresenta&ccedil;&atilde;o</b></th>";
    $tabela .= "<th><b>Quantidade</b></th>";
    $tabela .= "<th><b>Observa&ccedil;&atilde;o</b></th></tr></thead>";
    
    $sql = "SELECT s.nome as n, s.segundoNome as sn, s.obs, s.quantidade as qtd, p.nome as paciente  FROM saida as s, clientes as p 
	    WHERE s.idCliente = '{$paciente}' AND s.idCliente = p.idClientes AND s.data = '{$dta}' AND s.idUsuario = '{$_SESSION["id_user"]}' AND quantidade > 0 
	    ORDER BY s.tipo, s.nome, s.segundoNome ASC";
    
    $result = mysql_query($sql) or trigger_error(mysql_error());
    while($row = mysql_fetch_array($result)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      $tabela .= "<tr><td>{$row['n']}</td><td>{$row['sn']}</td><td>{$row['qtd']}</td><td>{$row['obs']}</td></tr>";
    }
    $tabela .= "</table>";
    
    $html = "<html><meta http-equiv='Content-Type' content='text/html; charset='utf-8'><body>";
    $html .= "<img src='../utils/logo.jpg' width=30% style='align:center' /><div style='font-size:x-small'>";
    $html .= "<table align='center' width=100% border=0 >";
    $html .= "<tr><td>Paciente:</td><td>Data:</td><td>Respons&aacute;vel pela sa&iacute;da:</td></tr>";
    $usuario = ucwords(strtolower($_SESSION["nome_user"]));
    $html .= "<tr><td>{$pacientenome}</td><td>{$data}</td><td>{$usuario}</td></tr>";
    $html .= "</table>";
    $html .= "<p><b>Relat&oacute;rio</b>";
    $html .= $tabela;
    $html .= "<br /><br /><br />________________________________________";
    $html .= "<p>Assinatura do respons&aacute;vel";
    $html .= "</div></body></html>";
	$mpdf=new mPDF('pt','A4',11);
  	$mpdf->WriteHTML($html);
  	$mpdf->Output('relatorio.pdf','I');
  	exit;
}

function combo_rastreamento($tipo){
	$sql = "SELECT idFornecedores as cod, razaoSocial as nome FROM fornecedores ORDER BY razaoSocial";
	if($tipo == "med")
		$sql = "SELECT numero_tiss as cod, concat(principio,' - ',apresentacao) as nome FROM brasindice WHERE tipo = 0 ORDER BY nome";
	else if($tipo == "mat")
		$sql = "SELECT numero_tiss as cod, concat(principio,' - ',apresentacao) as nome FROM brasindice WHERE tipo = 1 ORDER BY nome";
	$result = mysql_query($sql);
    echo "<p>";
    echo "<select name='codigo' style='background-color:transparent;width:400px' >";
    while($row = mysql_fetch_array($result))
    	echo "<option style='width:400px' value='{$row['cod']}'>{$row['nome']}</option>";     
    echo "</select>";

}

function combo_busca_saida($tipo){
	$sql = "SELECT idClientes as cod, nome as nome FROM clientes ORDER BY nome";
	if($tipo == "med")
		$sql = "SELECT numero_tiss as cod, concat(principio,' - ',apresentacao) as nome FROM brasindice WHERE tipo = 0 ORDER BY nome";
	else if($tipo == "mat")
		$sql = "SELECT numero_tiss as cod, concat(principio,' - ',apresentacao) as nome FROM brasindice WHERE tipo = 1 ORDER BY nome";
	$result = mysql_query($sql);
    echo "<p>";
    echo "<select name='codigo' style='background-color:transparent;width:400px' >";
    while($row = mysql_fetch_array($result))
    	echo "<option style='width:400px' value='{$row['cod']}'>{$row['nome']}</option>";     
    echo "</select>";
}

function envio_paciente($post){
	extract($post,EXTR_OVERWRITE);
	mysql_query("begin");
	$c = 0;
	$dados = substr($dados,0,-1);
	$itens = explode("$",$dados);
	$data = implode("-",array_reverse(explode("/",$dia)));
	$tabela = "<table border=1 width=100% style='font-size:x-small' >";
    $tabela .= "<tr><th><b>Nome</b></th>";
    $tabela .= "<th><b>Apresenta&ccedil;&atilde;o</b></th>";
    $tabela .= "<th><b>Quantidade</b></th>";
    $tabela .= "<th><b>Solicita&ccedil;&atilde;o</b></th>";
    $tabela .= "<th><b>Separa&ccedil;&atilde;o</b></th>";
    $tabela .= "<th><b>Observa&ccedil;&atilde;o</b></th></tr></thead>"; 
	foreach($itens as $item){
		$c++;
		$i = explode("#",str_replace("undefined","",$item));
		
		$sql = "UPDATE solicitacoes SET enviado = (enviado + '{$i[1]}') WHERE idSolicitacoes = '{$i[0]}'";
		$r = mysql_query($sql);
		$sql = "SELECT (CASE WHEN autorizado=enviado or autorizado<enviado THEN '1' ELSE '2' END) as teste FROM `solicitacoes` WHERE `solicitacoes`.`idSolicitacoes`={$i[0]}";
		$r = mysql_query($sql);
		while($row = mysql_fetch_array($r))
			$teste = $row['teste'];
		
		if($teste=='1')
			$r = @mysql_query("UPDATE solicitacoes SET `envioCompleto` = now() WHERE idSolicitacoes = '{$i[0]}'");
		$sql = "INSERT INTO saida (numero_tiss,tipo, nome, segundoNome, quantidade, idCliente, idUsuario, data)  
	  			VALUES ('{$i[2]}','{$i[6]}','{$i[3]}','{$i[4]}','{$i[1]}','{$i[5]}','{$_SESSION['id_user']}','{$data}') ";
  		$a = mysql_query($sql);
  		$sql = "UPDATE brasindice SET estoque = (estoque - {$i[1]}) WHERE numero_tiss = '{$i[2]}' ";
//  		if($tipo == 0)
//    		$sql = "UPDATE nomecomercialmed SET estoque = (estoque + {$i[1]}) WHERE idNomeComercialMed = '{$i[2]}' ";
//  		else $sql = "UPDATE material SET estoque = (estoque + {$i[1]}) WHERE idMaterial = '{$i[2]}' ";
  		$b = mysql_query($sql);
		if(!$r || !$a || !$b){ 
			echo "-1";
			mysql_query("rollback");
			return;
		}
		$enviado = $i[1]+$i[8];
		$tabela .= "<tr><td>{$i[3]}</td><td>{$i[4]}</td><td>{$i[1]} TOTAL: $enviado/{$i[9]}</td><td>{$i[7]}</td><td>{$dia}</td><td></td></tr>";
	}
	$tabela .= "</table>";
//	$html = "<html><meta http-equiv='Content-Type' content='text/html; charset='utf-8'><body>";
//    $html .= "<img src='../utils/logo.jpg' width=30% style='align:center' /><div style='font-size:x-small'>";
    $html = "<table align='center' width=100% border=0 >";
    $html .= "<tr><td>Paciente:</td><td>Data:</td><td>Respons&aacute;vel pela sa&iacute;da:</td></tr>";
    $usuario = ucwords(strtolower($_SESSION["nome_user"]));
    $html .= "<tr><td>{$np}</td><td>{$dia}</td><td>{$usuario}</td></tr>";
    $html .= "</table>";
    $html .= "<p><b>Relat&oacute;rio</b>";
    $html .= $tabela;
    $html .= "<br /><br /><br />________________________________________";
    $html .= "<p>Assinatura do respons&aacute;vel";
    $html .= "</div></body></html>";
	mysql_query("commit"); echo $html;
}

function cancelar_item_solicitacao($post){
	extract($post,EXTR_OVERWRITE);
	$sol = anti_injection($sol,'numerico');
	$just = anti_injection($just,'literal');
	$just = "CANCELADO PELA LOG&Iacute;STICA: ".$just; 
	$r = mysql_query("UPDATE solicitacoes SET status = '-2', obs = '{$just}' WHERE idSolicitacoes = '{$sol}' ");
	if(!$r){
		echo "ERRO: Tente mais tarde!";
		return; 
	}
	echo "1";
	return;
}

function busca_itens($like,$tipo){
	$sql = "";
	$label_tipo = "";
	$like = anti_injection($like,'literal');
	switch($tipo){
		case 'kits':
			$label_tipo = "KITS";
			$sql = "SELECT k.nome as nome, k.idKit as cod FROM kitsmaterial as k WHERE k.nome LIKE '%$like%' ORDER BY k.nome";
		break;
		case 'mat':
			$label_tipo = "MATERIAIS";
			$sql = "SELECT concat(mat.nome,' ',mat.descricao) as nome, mat.idMaterial as cod FROM material as mat WHERE mat.nome LIKE '%$like%' OR mat.descricao LIKE '%like%' ORDER BY nome";
		break;
		case 'med':
			$label_tipo = "MEDICAMENTOS";
			$sql = "SELECT concat(med.principioAtivo,' ',com.nome) as nome, com.idNomeComercialMed as cod FROM medicacoes as med, nomecomercialmed as com WHERE (med.principioAtivo LIKE '%$like%' OR com.nome LIKE '%$like%') AND med.idMedicacoes = com.Medicacoes_idMedicacoes ORDER BY nome";
		break;
	}
	$result = mysql_query($sql);
	echo "<table class='mytable' width=100% id='lista-itens' ><thead><tr><th>{$label_tipo}</th></tr></thead>";
	while($row = mysql_fetch_array($result)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      echo "<tr cod='{$row['cod']}' ><td>{$row['nome']}</td></tr>";
	}
	echo "</table>";
	echo "<script type='text/javascript' >";
	echo " $('#lista-itens tr').dblclick(function(){
	      $('#nome').val($(this).text());
	      $('#cod').val($(this).attr('cod'));
	      $('#lista-itens').html('');
	      $('#busca-item-troca').val('');
	      $(\"#formulario-troca input[name='qtd']\").focus();
	  });";
	echo "</script>";
}

function trocar_item_solicitacao($post){
	extract($post,EXTR_OVERWRITE);
	$sol = anti_injection($sol,'numerico');
	$just = anti_injection($just,'literal');
	$p = anti_injection($p,'numerico');
	$item = anti_injection($item,'numerico');
	$qtd = anti_injection($qtd,'numerico');
	$n = anti_injection($n,'literal');
	$just = "TROCADO PELA LOG&Iacute;STICA POR: ".$n." MOTIVO: ".$just; 
	//a pres foi alterade de A para B por causa de C. mandar por email para msgsistema.
	mysql_query("begin");
	$r = mysql_query("UPDATE solicitacoes SET qtd = enviado, status = '-2', obs = '{$just}' WHERE idSolicitacoes = '{$sol}' ");
	if(!$r){
		echo "ERRO: Tente mais tarde!";
		mysql_query("rollback");
		return; 
	}
	$t = 0;
	if($tipo == 'mat') $t = 1;
	$solicitante = $_SESSION["id_user"];
	$sql = "INSERT INTO solicitacoes (paciente,enfermeiro,cod,qtd,tipo,data,status) VALUES ('$p','$solicitante','{$item}','{$qtd}','{$t}',now(),'1')";
	$r = mysql_query($sql);
	if(!$r){
		echo "ERRO: Tente mais tarde!";
		mysql_query("rollback");
		return; 
	}
	mysql_query("commit");
	echo "1";
	return;
}

if(isset($_POST['query'])){
  switch($_POST['query']){
    case "inicio_inventario":
      inicio_inventario($_POST['tipo']);
      break;
    case "remover_especial":
      remover_especial($_POST['id']);
      break;
    case  "op-itens":
      opcoes_itens($_POST);
      break;
    case "salvar-devolucao":
      salvar_devolucao($_POST);
      break;
    case "combo-rastreamento":
      combo_rastreamento($_POST['tipo']);
      break;
    case "combo-busca-saida":
      combo_busca_saida($_POST['tipo']);
      break;
    case "envio-paciente":
      envio_paciente($_POST);
      break;
    case "cancelar-item-solicitacao":
      cancelar_item_solicitacao($_POST);
      break;
    case "busca-itens-troca":
    	busca_itens($_POST['like'],$_POST['tipo']);
  	break;
  	case "trocar-item-solicitacao":
      trocar_item_solicitacao($_POST);
    break;
  }
}

if(isset($_GET['query'])){
  switch($_GET['query']){
    case "pdf_especiais":
      pdf_especiais(implode("-",array_reverse(explode("/",$_GET['venc']))));
      break;
    case "inicio_inventario":
      inicio_inventario($_GET['tipo']);
      break;
    case "inventario":
      inventario($_GET['tipo'],$_GET['zerados'],$_GET['offset'],$_GET['nome']);
      break;
    case "relatorio":
      relatorio($_GET);
      break;
  }
}

?>