<?php

require_once('../db/config.php');
include_once('../utils/codigos.php');

class MED{
  public function menu(){
    echo "<p><a href='?op=med&act=cadastrar'>Cadastrar</a>";
    //echo "<button id='cadastrar'>Cadastrar</button>";
    echo "<p><a href='?op=med&act=buscar'>Buscar</a>";    
  }
  
  private function classificacao($id){
    $result = mysql_query("SELECT * FROM classmedicamentos ORDER BY classe;");
    echo "<p><b>Classifica&ccedil;&atilde;o:</b><br/>";
    echo "<select name='classe' class='COMBO_OBG' style='background-color:transparent;' >";
    echo "<option value='-1'>SELECIONE</option>";
    while($row = mysql_fetch_array($result))
    {
      if(($id <> NULL) && ($id == $row['id']))
		echo "<option value='{$row['id']}' selected>{$row['classe']}</option>";
      else
		echo "<option value='{$row['id']}'>{$row['classe']}</option>";     
    }
    echo "</select>";
  }
  
  public function nova_classe($nome){
    if($nome == NULL){
      echo "<form name=\"form\" action='' method='POST'>";
      echo "<p><b>Classifica&ccedil;&atilde;o</b><input type='text' name='nome'/>";
      echo "<input type='submit' value='Inserir' />";
      echo "</form>";
    } else {
      $sql = "INSERT INTO ClassMedicamentos (`classe`) VALUES('{$nome}') "; 
      mysql_query($sql) or die(mysql_error());
    }
  }
  
  public function cadastrar($nome,$classe){
    if($nome == NULL){
      echo "<div id='cadastro-medicacoes'>".alerta();
      echo "<form name=\"form\" action='' method='POST'>";
      echo "<p><b>Princ&iacute;pio Ativo</b><input class='OBG' id='principioAtivo' type='text' name='nome'/>";
      //echo $this->classificacao(NULL);
      echo "<input id='novomed' type='submit' value='Inserir' />";
      echo "</form></div>";
    } else {
      $sql = "INSERT INTO `medicacoes` ( `principioAtivo` ) VALUES(  '$nome') "; 
      mysql_query($sql) or die(mysql_error());
      $this->buscar($nome);
    }
  }
  
  public function editar($id,$nome,$classe){
    if($nome == NULL){
      $row = mysql_fetch_array ( mysql_query("SELECT * FROM `medicacoes` WHERE `idMedicacoes` = '$id' "));
      echo "<form name=\"form\" action='' method='POST'>";
      echo "<p><b>Princ&iacute;pio Ativo</b><input type='text' name='nome' value='".$row['principioAtivo']."' />";
      echo "<input type='hidden' name='id' value='$id' />";
      echo "<input type='submit' value='Salvar' /><input type='hidden' value='1' name='submitted' />";
      echo "</form>";
    } else {
      $sql = "UPDATE `medicacoes` SET `principioAtivo` = '{$nome}' WHERE idMedicacoes = '{$id}' "; 
      mysql_query($sql) or die(mysql_error());
      $this->buscar($nome);
    }
  }
  
  public function show_result($result,$nome){
    echo "<table class='mytable' width=100% >"; 
    echo "<thead><tr>"; 
    echo "<th><b>Princ&iacute;pio Ativo</b></th>";
//    echo "<th><b>Classifica&ccedil;&atilde;o</b></th>";
    echo "<th><b>M&iacute;nimo</b></th>"; 
    echo "<th colspan='3' ><b>Op&ccedil;&otilde;es</b></th>";
    echo "</tr></thead>";
    $cor = false;
    $total = 0;
    while($row = mysql_fetch_array($result)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      if($cor) echo "<tr>";
      else echo "<tr class='odd'>"; 
      $cor = !$cor;
      $total++;
      echo "<td valign='top'>" . $row['principioAtivo'] . "</td>";
//      echo "<td valign='top'>" . $row['classe'] . "</td>";
      echo "<td valign='top'>" . $row['minimo'] . "</td>";
      $del = "index.php?op=med&act=excluir&id={$row['idMedicacoes']}&confirm=ok";
      echo "<td valign='top'><a href=?op=med&act=detalhes&id={$row['idMedicacoes']}><img src='../utils/details_16x16.png' title='Detalhes' border='0'></a></td><td valign='top'><a href=?op=med&act=editar&id={$row['idMedicacoes']}><img src='../utils/edit_16x16.png' title='Editar' border='0'></a></td><td><a href=\"javascript:ConfirmChoiceMat('excluir','{$row['idMedicacoes']}','{$row['principioAtivo']}','{$del}')\"><img src='../utils/delete_16x16.png' title='Excluir' border='0'></a></td> ";
      echo "</tr>";      
    }
    echo "</table>";
    if($total == 0) echo "<center>Nenhum resultado para <b><u>$nome</u></b> foi encontrado!</center>";
  }
  
  public function buscar($nome){
    if($nome == NULL){
      echo "<form name=\"form\" action='' method='POST'>";
      echo "<p><b>Princ&iacute;pio Ativo / Nome Comercial / C&oacute;digo de Barras</b><input type='text' name='nome'/>";
      echo "<input type='submit' value='Buscar' /><input type='hidden' value='1' name='submitted' />";
      echo "</form>";
    } else {
      $sql = "SELECT DISTINCT m.idMedicacoes, m.principioAtivo, m.minimo FROM `medicacoes` as m LEFT OUTER JOIN `nomecomercialmed` as n on m.idMedicacoes = n.Medicacoes_idMedicacoes WHERE (m.principioAtivo LIKE '%{$nome}%' OR n.nome LIKE '%{$nome}%' OR n.codigobarras = '{$nome}')";
//      echo $sql;
      $result = mysql_query($sql) or trigger_error(mysql_error());
      $this->show_result($result,$nome);
    }
  }
  
  public function excluir($id){
    mysql_query("DELETE FROM `medicacoes` WHERE `idMedicacoes` = '$id' ") ;
    echo "Medicamento exclu&iacute;do.<a href='?op=med&act=buscar'>(voltar)</a><br />";
  }
  
  public function detalhes($id){
//  	SELECT m.idMedicacoes, m.principioAtivo, m.minimo, n.nome, n.codigobarras, n.idNomeComercialMed FROM `medicacoes` as m LEFT OUTER JOIN (SELECT n.nome, n.codigobarras, n.idNomeComercialMed, n.Medicacoes_idMedicacoes, c.classe FROM `nomecomercialmed` as n, classmedicamentos as c WHERE n.classe = c.id) as n ON m.idMedicacoes = n.Medicacoes_idMedicacoes WHERE m.idMedicacoes = 14
//  	SELECT m.idMedicacoes, m.principioAtivo, m.minimo, n.nome, n.codigobarras, n.idNomeComercialMed FROM `medicacoes` as m LEFT OUTER JOIN `nomecomercialmed` as n ON m.idMedicacoes = n.Medicacoes_idMedicacoes, `classmedicamentos` as c WHERE m.idMedicacoes = 14 AND n.classe = c.id
  	$sql = "SELECT m.idMedicacoes, m.principioAtivo, m.minimo, n.nome, n.codigobarras, n.idNomeComercialMed, n.classe FROM `medicacoes` as m LEFT OUTER JOIN (SELECT n.nome, n.codigobarras, n.idNomeComercialMed, n.Medicacoes_idMedicacoes, c.classe FROM `nomecomercialmed` as n, classmedicamentos as c WHERE n.classe = c.id) as n ON m.idMedicacoes = n.Medicacoes_idMedicacoes WHERE m.idMedicacoes = '{$id}'";
    $result = mysql_query($sql) or trigger_error(mysql_error());
    echo "<table class='mytable' width=100% >"; 
    echo "<thead><tr>"; 
    echo "<th><b>Princ&iacute;pio Ativo</b></th>"; 
    echo "<th><b>Estoque M&iacute;nimo</b></th>"; 
    echo "<th colspan='2' ><b>Op&ccedil;&otilde;es</b></th>";
    echo "</tr></thead>";
    $cor = false;
    $total = 0;
    $flag = true;
    while($row = mysql_fetch_array($result)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      if($cor) echo "<tr>";
      else echo "<tr class='odd'>"; 
      $cor = !$cor;
      if($flag){
		echo "<td valign='top'>" . $row['principioAtivo'] . "</td>";  
		echo "<td valign='top'>" . $row['minimo'] . "</td>";
		$del = "index.php?op=med&act=excluir&id={$row['idMedicacoes']}&confirm=ok";
		echo "<td valign='top'><a href=?op=med&act=editar&id={$row['idMedicacoes']}><img src='../utils/edit_16x16.png' title='Editar' border='0'></a></td><td><a href=\"javascript:ConfirmChoiceMat('excluir','{$row['idMedicacoes']}','{$row['principioAtivo']}','{$del}')\"><img src='../utils/delete_16x16.png' title='Excluir' border='0'></a></td> ";
		echo "</tr>";
		echo "</table>";
	
		echo "<div id='dialog-edit-apr' title='Editar Apresenta&ccedil;&atilde;o'>";
		echo "<span class='aviso'></span>";
		echo "<form id='fomulario' action='javascript:func()' method='post'>";
		echo "<fieldset>";
		echo "<input type='hidden' name='edit-id'/>";
		echo "<label><b>Apresenta&ccedil;&atilde;o</b></label>";
		echo "<input type='text' name='edit-apr' class='OBG' />";
		echo "<label><b>C&oacute;digo de Barras</b></label>";
		echo "<input type='text' name='edit-barras' maxlength='13' />";
		echo $this->classificacao(NULL);
		echo "</form></div>";
		
		echo "<div id='dialog-add-fantasia' title='Adicionar nome fantasia'>";
		echo "<div id='div-tabela-fantasias' ></div>";
		echo "<span class='aviso'></span>";
		echo "<form id='fomulario' action='javascript:func()' method='post'>";
		echo "<fieldset>";
		echo "<input type='hidden' name='apr-id'/>";
		echo "<label><b>Novo nome fantasia</b></label>";
		echo "<input type='text' name='fantasia' class='OBG' />";
		echo "</form></div>";
	
		echo "<div id='apresentacao-form' title='Cadastrar Apresenta&ccedil;&atilde;o'>";
		echo "<span class='aviso'></span>";
		echo "<form id='fomulario' action='javascript:func()' method='post'>";
		echo "<fieldset>";
		echo "<input type='hidden' id='IDMED' value='{$row['idMedicacoes']}' />";
		echo "<label for='name'><b>Apresenta&ccedil;&atilde;o</b></label>";
		echo "<input type='text' name='name' id='name' class='OBG' />";
		echo "<label for='name'><b>C&oacute;digo de Barras</b></label>";
		echo "<input type='text' name='name' id='codigobarras' maxlength='13' />";
		echo $this->classificacao(NULL);
		echo "<p><input value='Adicionar' id='inserir' type='submit'/>";
		echo "</fieldset>";
		echo "</form>";
		echo "<h2><center><span id='aviso'></span></center></h2>";
		echo "</div>";
	
		echo "<br/><table id='tabela' class='mytable' width=100% >"; 
		echo "<thead><tr>"; 
		echo "<th><b>Apresenta&ccedil;&atilde;o</b></th>"; 
		echo "<th><b>C&oacute;digo de Barras</b></th>";
		echo "<th><b>Classifica&ccedil;&atilde;o</b></th>";
		echo "<th colspan='3' ><b>Op&ccedil;&otilde;es</b></th>";
		echo "</tr></thead>";
		if($row['nome'] <> NULL){
	  		echo "<tr>";
	  		echo "<td valign='top'>{$row['nome']}</td>";
	  		echo "<td valign='top'>{$row['codigobarras']}</td>";
	  		echo "<td valign='top'>{$row['classe']}</td>";
	  		$del = "index.php?op=med&act=excluir&id={$row['idMedicacoes']}&confirm=ok";
	  		echo "<td><a href='' class='add-fantasia' apr='{$row['idNomeComercialMed']}' ><img src='../utils/add_16x16.png' title='Incluir nome fantasia' border='0'></a></td>";
	  		echo "<td><a href='' class='edit-apresentacao' apr='{$row['idNomeComercialMed']}' ><img src='../utils/edit_16x16.png' title='Editar' border='0'></a></td><td><a href=\"javascript:ConfirmChoice('excluir','{$row['idNomeComercialMed']}','{$row['nome']}','excluir')\"><img src='../utils/delete_16x16.png' title='Excluir' border='0'></a></td>";
	  		echo "</tr>";
		}
		$flag = false;
      } else if($row['nome'] <> NULL) {
			if($cor) echo "<tr>";
			else echo "<tr class='odd'>";
			echo "<td valign='top'>{$row['nome']}</td>";
			echo "<td valign='top'>{$row['codigobarras']}</td>";
			echo "<td valign='top'>{$row['classe']}</td>";
			$del = "index.php?op=med&act=excluir&id={$row['idMedicacoes']}&confirm=ok";
			echo "<td><a href='' class='add-fantasia' apr='{$row['idNomeComercialMed']}' ><img src='../utils/add_16x16.png' title='Incluir nome fantasia' border='0'></a></td>";
			echo "<td><a href='#' class='edit-apresentacao' apr='{$row['idNomeComercialMed']}' ><img src='../utils/edit_16x16.png' title='Editar' border='0'></a></td><td><a href=\"javascript:ConfirmChoice('excluir','{$row['idNomeComercialMed']}','{$row['nome']}','excluir')\"><img src='../utils/delete_16x16.png' title='Excluir' border='0'></a></td>";
			echo "</tr>";
      	}
    }
    echo "</table>";
  }
}

?>
