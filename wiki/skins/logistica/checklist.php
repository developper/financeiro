<?php
session_start();
include_once('../utils/mpdf/mpdf.php');
include_once('../db/config.php');

class Itens{
	private $tipo = "";
	private $nome = "";
	private $apresentacao = "";
	private $via = "";
	private $aprazamento = "";
	private $obs = "";
	private $dose = "";
	private $freq = "";
	
	function Itens($t,$n,$a,$v,$apraz,$o,$d,$f){
		$this->tipo = $t;
		$this->nome = $n;
		$this->apresentacao = $a;
		$this->via = $v;
		$this->aprazamento = $apraz;
		$this->obs = $o;
		$this->dose = $d;
		$this->freq = $f;
	}
	
	public function getAprazamento($dia){
		if(array_key_exists($dia,$this->aprazamento))
			return $this->aprazamento[$dia];
		return "&nbsp;";
	}
	
	public function getLabel(){
		return "{$this->tipo}{$this->nome} {$this->apresentaca} {$this->dose} {$this->via} {$this->freq} {$this->obs}";
	}
}



$p = $_REQUEST['p'];

$sq = "SELECT 
		       p.inicio,
		       p.fim 
		FROM 
		       prescricoes AS p
		WHERE 
		       p.paciente = {$p} AND p.fim>=NOW() AND p.Carater=2
		ORDER BY 
		       p.id DESC LIMIT 1";

$res = mysql_query($sq);
$r = mysql_fetch_array($res);



$inicio = $r['inicio'];
$fim = $r['fim'];

$sql = "SELECT
			s.enfermeiro,
			u.nome AS profissional,
			s.tipo,
			s.qtd,
			p.inicio,
			p.fim,
			UPPER(c.nome) AS nome,
			c.cuidador,
			c.relcuid,
			s.autorizado,
			s.enviado,
			s.cod,
			s.idSolicitacoes AS sol,
			s.status,
			DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
			f.formula AS produto
		FROM
			solicitacoes AS s,
			prescricoes AS p,
			formulas AS f,
			clientes AS c,
			usuarios AS u
		WHERE
			s.paciente = '{$p}' AND
			s.enfermeiro = u.idUsuarios AND
			s.idPrescricao = p.id AND
			c.idclientes = s.paciente AND
			s.tipo = '12' AND
			f.id = s.cod AND
			(CASE s.idPrescricao WHEN '-1' THEN s.data BETWEEN '{$inicio}' AND '{$fim}' ELSE p.inicio>='{$inicio}' AND '{$fim}'<= p.fim END)
		UNION
		SELECT
			s.enfermeiro,						
			u.nome AS profissional,
			s.tipo,
			s.qtd,
			p.inicio,
			p.fim,
			UPPER(c.nome) AS nome,
			c.cuidador,
			c.relcuid,
			s.autorizado,
			s.enviado,
			s.cod,
			s.idSolicitacoes AS sol,
			s.status,
			DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
			CONCAT(b.principio,' - ',b.apresentacao) AS produto
		FROM
			solicitacoes AS s,
			prescricoes AS p,
			brasindice AS b,
			clientes AS c,
			usuarios AS u
		WHERE
			s.paciente = '{$p}' AND
			s.enfermeiro = u.idUsuarios AND
			s.idPrescricao = p.id AND
			c.idclientes = s.paciente AND
			s.tipo = '1' AND
			b.numero_tiss = s.cod AND
			(CASE s.idPrescricao WHEN '-1' THEN s.data BETWEEN '{$inicio}' AND '{$fim}' ELSE p.inicio>='{$inicio}' AND '{$fim}'<= p.fim END)
		UNION
		SELECT
			s.enfermeiro,
			u.nome AS profissional,
			s.tipo,
			s.qtd,
			p.inicio,
			p.fim,
			UPPER(c.nome) AS nome,
			c.cuidador,
			c.relcuid,
			s.autorizado,
			s.enviado,
			s.cod,
			s.idSolicitacoes AS sol,
			s.status,
			DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
			CONCAT(b.principio,' - ',b.apresentacao) AS produto
		FROM
			solicitacoes AS s,
			prescricoes AS p,
			brasindice AS b,
			clientes AS c,
			usuarios AS u
		WHERE
			s.paciente = '{$p}' AND
			s.enfermeiro = u.idUsuarios AND
			s.idPrescricao = p.id AND
			c.idclientes = s.paciente AND
			s.tipo = '0' AND
			b.numero_tiss = s.cod AND
			(CASE s.idPrescricao WHEN '-1' THEN s.data BETWEEN '{$inicio}' AND '{$fim}' ELSE p.inicio>='{$inicio}' AND '{$fim}'<= p.fim END)
		GROUP BY
			s.cod
		ORDER BY
			tipo,
			produto,
			autorizado,
			enviado";


$result = mysql_query($sql);
$result1 = mysql_query($sql);

$row = mysql_fetch_array($result);
$paciente = $row['nome'];




	$img = "<img src='../utils/logo.jpg' width='30%' style='align:center' />";
	$hoje = date("d/m/Y");
	$header = "<table width='100%' style='border:1px solid #000;border-collapse:collapse;'>
	              <thead>
	                  <tr>
	                  	<th rowspan='2' style='float:left;padding:10px;width:20%;'>
	                  		{$img}
	                  	</th>
	                  	<th style='float:left;padding:10px;'>
	                  		<span style='font-size:16px;'>CONTROLE DE ESTOQUE DOMICILIAR DE MAT/MED  </span><br>DE ".join('/',array_reverse(explode("-",$row['inicio'])))." AT&Eacute; ".join('/',array_reverse(explode("-",$row['fim'])))."<br>
	                  		<br><span style='font-size:14px;'>PACIENTE: <i>".$row['nome']."</i></span>
	                  		<br>
	                  		<br><span style='font-size:12px;'>PROFISSIONAL: <i>".$row['profissional']."</i></span>
	                  	</th>
					  </tr>
					</thead>  
	          </table>";
	
	
	

$flag = true;





$tabela = "<br><table style='border:1px solid #000;border-collapse:collapse;' width='100%' >
           <thead>
             <tr>
               <th ></th>
               <th ></th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
               <th colspan='2' style='width:10%; font-size:10px;'>DIA/MES</th>
            </tr>
			<tr>
               <th style='border:1px solid #000;'>Ord.</th>
               <th style='border:1px solid #000;'>Itens</th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               <th colspan='2' style='border:1px solid #000; width:8%'> </th>
               
            </tr>
           ";
$tabela .= "<tr>
				<th style='border:1px solid #000;'></th>
				<th style='border:1px solid #000;'></th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
				<th style='border:1px solid #000;'>MT</th>
				<th style='border:1px solid #000;'>SN</th>
			</tr>
</thead>";
$i = 0;
while($row1 = mysql_fetch_array($result1)){
	
  $tabela .= "<tr>
                  <td align='center' style='width:5px;border:1px solid #000;'>{$i}</td>
                  <td style='border:1px solid #000;height:20px;'>{$row1['produto']}</td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
                  <td style='border:1px solid #000;' align='center'></td>
              </tr>";	
  $i++;
  
}

/*ANOTA��ES*/
$tabela .= "<tr>
<td align='center' style='width:5px;border:1px solid #000;'>{$i}</td>
<td style='border:1px solid #000; height:20px;'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
</tr>";
$i++;
$tabela .= "<tr>
<td align='center' style='width:5px;border:1px solid #000;'>{$i}</td>
<td style='border:1px solid #000; height:20px;'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
</tr>";
$i++;
$tabela .= "<tr>
<td align='center' style='width:5px;border:1px solid #000;'>{$i}</td>
<td style='border:1px solid #000; height:20px;'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
</tr>";
$i++;
$tabela .= "<tr>
<td align='center' style='width:5px;border:1px solid #000;'>$i</td>
<td style='border:1px solid #000; height:20px;'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
</tr>";
$i++;
$tabela .= "<tr>
<td align='center' style='width:5px;border:1px solid #000;'>$i</td>
<td style='border:1px solid #000; height:20px;'><b>Assinatura do T&eacute;cnico(a)</b></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
<td style='border:1px solid #000;' align='center'></td>
</tr>";
$tabela .="</table><br>
<table style='width:100%;border:1px solid #000000;border-collapse:collapse;'>
<tr>
	<td style='border:1px solid #000;height:20px;text-align:center'><b>ANOTA&Ccedil;&Otilde;ES</b></td>
</tr>
<tr>
	<td style='border:1px solid #000;height:25px;'></td>
</tr>
<tr>
	<td style='border:1px solid #000;height:25px;'></td>
</tr>
<tr>
	<td style='border:1px solid #000;height:25px;'></td>
</tr>
<tr>
	<td style='border:1px solid #000;height:25px;'></td>
</tr>
";
$paginas []= $header.$tabela."</table>";
//print_r($paginas);
//print_r($paginas);
//print_r($paginas);

$mpdf=new mPDF('pt','A4-L',9);
$mpdf->SetHeader('página {PAGENO} de {nbpg}');
$ano = date("Y");
$mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
$mpdf->WriteHTML("<html><body>");
$flag = false;
foreach($paginas as $pag){
	if($flag) $mpdf->WriteHTML("<formfeed>");
	$mpdf->WriteHTML($pag);
	$flag = true;
}
$mpdf->WriteHTML("</body></html>");
$mpdf->Output('prescricao.pdf','I');
exit;
?>