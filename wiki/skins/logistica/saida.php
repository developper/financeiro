<?php

require_once('../db/config.php');
require_once('../utils/codigos.php');

class Saida{
  
  private function formulario(){
  	echo "<div id='formulario-troca' title='Formul&aacute;rio de troca' sol='' tipo='' >";
  	echo alerta();
  	echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar a troca de item, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
    echo "<p><b>Busca:</b><br/><input type='text' name='busca' id='busca-item-troca' />";
    echo "<div id='opcoes-troca'></div>";
    echo "<input type='hidden' name='cod' id='cod' value='-1' />";
    echo "<p><b>Nome:</b><input type='text' readonly=readonly name='nome' id='nome' class='OBG' size='50' />";
    echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
    echo "<b>Justificativa:</b><input type='text' name='justificativa' class='OBG' size='100' /></div>";
  }
    
  public function pendentes(){
  	$vazio = true;
  	$this->imprimir_check();
  	
  	echo "<div><center><h1>Solicita&ccedil;&otilde;es Pendentes</h1></center><br/>";
  	$sql = "SELECT DISTINCT c.nome, s.paciente,s.idPrescricao,(case idPrescricao WHEN -1 THEN MIN(DATE_FORMAT(s.data, '%d/%m/%Y')) ELSE MIN(DATE_FORMAT(s.data_auditado, '%d/%m/%Y')) END) as data,
  	(case s.status WHEN 2 THEN '<b>EMERG&Ecirc;NCIA</b> <img src=\'../utils/sirene_ativa.gif\' style=\'width:10px;\'>' ELSE MIN(DATE_FORMAT(s.data_auditado, '%H:%i:%S')) END)
  	 as hora,U.nome as usuario,
			(SELECT count(*)
			  			   FROM solicitacoes as X, brasindice as b
			  			   WHERE X.paciente = s.paciente AND (X.tipo = 0 OR X.tipo = 1 OR X.tipo = 12) AND (case X.status WHEN 2 THEN qtd > enviado ELSE autorizado > enviado END) AND (X.status = '1' OR X.status = '2')
			  			   AND b.numero_tiss = X.cod ) as itens
			 FROM solicitacoes as s, clientes as c, usuarios as U  WHERE s.paciente = c.idClientes AND U.idUsuarios = s.enfermeiro AND (case s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND (s.status = '1' OR s.status = '2') Group BY s.paciente
			 ORDER BY MIN(s.data) ASC";
  	$r = mysql_query($sql);
  	
  	while($row = mysql_fetch_array($r)){ 
    	foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
    	$vazio = false;
    	$presc=0;
    	if($row['idPrescricao']!=-1){
    		$presc = $row['idPrescricao'];
    	}
    	//echo "<button class='bpaciente' p='{$row['paciente']}' >{$row['nome']}</button>";
    	echo "<div style='border:1px solid #fff;width:240px;display:table;float:left;margin:0 auto;padding:15px; font-size:12px;'>";
	    	echo "<table width='100%'>";
		    	echo "<tr>";
			    	echo "<td>";
			    		echo "<center><button class='bpaciente' p='{$row['paciente']}' presc='{$presc}' ></button></center>";
			    	echo "</td>";
		    	echo "</tr>";
		    	echo "<tr>";
			    	echo "<td>";
			    		echo "<center><b>".strtoUpper($row['nome'])."</b></center>";
			    	echo "</td>";
		    	echo "</tr>";
			    	echo "<tr>";
				    	echo "<td>";
				    		echo "<center style='font-size:9px;'>(".implode('/',array_reverse(explode("-",$row['usuario']))).")</center>";
				    	echo "</td>";
		    	echo "</tr>";
		    	echo "<tr>";
		    		echo "<td>";
		    			echo "<center>".$row['data']."</center>";
		    		echo "</td>";	
		    	echo "</tr>";
		    	echo "<tr>";
			    	echo "<td>";
			    		echo "<center style='font-size:9px;'>".$row['hora']."</center>";
			    	echo "</td>";
		    	echo "</tr>";
		    	echo "</tr>";
		    	echo "<tr>";
			    	echo "<td>";
			    		echo utf8_encode("<center style='color:red;'>�tens (".$row['itens'].")</center>");
			    	echo "</td>";
		    	echo "</tr>";
	    	echo "</table>";
    	echo "</div>";
  	}
  	
  	if($vazio) echo "<p style='text-align:center;color:red;'><b>Nenhuma solicita&ccedil;&atilde;o pendente!</b></p></div>";
  	else echo "</div>";
  	
  }
  
  private function imprimir_check(){
  	echo "<div id='formulario-check' title='Check-List' p='' presc='0'>";
  	echo alerta();
  	echo info("<b>Aten&ccedil;&atilde;o:</b> A log&iacute;stica deve imprimir o Check-List antes de fazer qualquer lan&ccedil;amento no estoque.");
    echo "<p><center><b>Deseja imprimir o Check-List Agora?</b></center></p><br/></div>";
    
  }
  
  public function saida_paciente($p){
  	$p = anti_injection($p,'numerico');  	
  	echo "<div id='cancelar-item' title='Cancelar item' sol='' >";
  	echo alerta();
  	echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar o cancelamento, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
  	echo "<p><b>Justificativa:</b><input type='text' name='justificativa' class='OBG' size='100' /></div>";
  	$this->formulario();
  	$row = mysql_fetch_array(mysql_query("SELECT c.nome FROM clientes as c WHERE c.idClientes = '$p'"));
  	$envio = "<input type='text' name='qtd-envio' class='numerico' style='text-align:right' size='4' />";
  	echo "<center><h1>Sa&iacute;da para o paciente:<h1>";
  	$paciente = ucwords(strtolower($row['nome']));
  	echo "<h3>$paciente</h3></center>";
  	echo "<div id='dialog-relatorio' title='Relat&oacute;rio' ><img src='../utils/logo.jpg' width=30% style='align:center' /><div id='pre-relatorio'></div>
  		  <form action='print.php' method='post' name='form_relatorio' id='print-relatorio'>
          <input type='hidden' name='relatorio' value='' id='relatorio' />
          <input type='submit' value='Imprimir' />
          </form></div>";
  	echo "<div id='div-envio-paciente' p='$p' np='$paciente' >";
  	echo alerta();
  	
  	echo "<p><b>Data:</b><input type='text' class='data OBG' id='data-envio' name='data_envio' >";
  	echo "<table class='mytable' width=100% id='lista-enviados' ><thead><tr><th>Itens Solicitados</th><th align='center' width='1%'>enviado/pedido</th><th width='1%'>Enviar</th></tr></thead>";

  	$sqlmed = "SELECT s.enfermeiro,(CASE s.status when 2 THEN s.qtd ELSE s.autorizado END) as autorizado, s.enviado, s.cod, s.idSolicitacoes as sol, s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data, b.principio, b.apresentacao 
  			   FROM solicitacoes as s, brasindice as b 
  			   WHERE s.paciente = '$p' AND s.tipo = '0' AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND (s.status = '1' OR s.status = '2')
  			   AND b.numero_tiss = s.cod ORDER BY principio, apresentacao,enviado";
//  	$sqlmed = "SELECT med.principioAtivo, com.nome, s.qtd, s.enviado, s.cod, s.idSolicitacoes as sol, DATE_FORMAT(s.data,'%d/%m/%Y') as data  
//  			   FROM solicitacoes as s, medicacoes as med, nomecomercialmed as com 
//  			   WHERE s.paciente = '$p' AND s.tipo = '0' AND qtd > enviado AND (s.status = '1' OR s.status = '2')
//  			   AND com.idNomeComercialMed = s.cod AND com.Medicacoes_idMedicacoes = med.idMedicacoes 
//  			   ORDER BY principioAtivo, nome, qtd, enviado";
  	$rmed = mysql_query($sqlmed);
  	while($row = mysql_fetch_array($rmed)){
  		foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
  		$dados = "sol='{$row['sol']}' cod='{$row['cod']}' n='{$row['principio']}' sn='{$row['apresentacao']}' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['qtd']}' tipo='0'";
  		$cancela = "<img align='right' sol='{$row['sol']}' class='cancela-item' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
  		$trocar = "<img align='right' sol='{$row['sol']}' tipo='med' class='troca-item' src='../utils/exchange_16x16.png' title='Trocar' border='0'>";
  		echo "<tr><td><b>MEDICAMENTO: </b>{$row['principio']} {$row['apresentacao']} {$cancela} {$trocar}</td>";
  		echo "<td align='center'>{$row['enviado']}/{$row['autorizado']}</td><td></td><td class='dados' $dados >$envio</td></tr>";
  	}
  	$sqlmat = "SELECT s.enfermeiro, (CASE s.status when 2 THEN s.qtd ELSE s.autorizado END) as autorizado, s.enviado, s.cod, s.idSolicitacoes as sol, s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data, b.principio, b.apresentacao 
  			   FROM solicitacoes as s, brasindice as b 
  			   WHERE s.paciente = '$p' AND s.tipo = '1' AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND (s.status = '1' OR s.status = '2') 
  			   AND b.numero_tiss = s.cod ORDER BY principio, apresentacao, enviado";
//  	$sqlmat = "SELECT mat.nome, mat.descricao, s.qtd, s.enviado, s.cod, s.idSolicitacoes as sol, DATE_FORMAT(s.data,'%d/%m/%Y') as data  
//  			   FROM solicitacoes as s, material as mat 
//  			   WHERE s.paciente = '$p' AND s.tipo = '1' AND qtd > enviado AND (s.status = '1' OR s.status = '2')
//  			   AND mat.idMaterial = s.cod 
//  			   ORDER BY nome, descricao, qtd, enviado";
  	$rmat = mysql_query($sqlmat);
  	while($row = mysql_fetch_array($rmat)){
  		foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
  		$dados = "sol='{$row['sol']}' cod='{$row['cod']}' n='{$row['principio']}' sn='{$row['apresentacao']}' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['qtd']}' tipo='1'";
  		$cancela = "<img align='right' sol='{$row['sol']}' class='cancela-item' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
  		$trocar = "<img align='right' sol='{$row['sol']}' tipo='mat' class='troca-item' src='../utils/exchange_16x16.png' title='Trocar' border='0'>";
  		echo "<tr><td><b>MATERIAL: </b>{$row['principio']} {$row['apresentacao']} {$cancela} {$trocar}</td>";
  		echo "<td align='center'>{$row['enviado']}/{$row['autorizado']}</td><td class='dados' $dados >$envio</td></tr>";
  	}
  	$sqlformula = "SELECT s.enfermeiro, (CASE s.status when 2 THEN s.qtd ELSE s.autorizado END) as autorizado, s.enviado, s.cod, s.idSolicitacoes as sol, s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data, f.formula  
  			   FROM solicitacoes as s, formulas as f 
  			   WHERE s.paciente = '$p' AND s.tipo = '12' AND (CASE s.status WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END) AND (s.status = '1' OR s.status = '2') 
  			   AND f.id = s.cod ORDER BY formula,enviado";
  	$rformula = mysql_query($sqlformula);
  	while($row = mysql_fetch_array($rformula)){
  		foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
  		$dados = "sol='{$row['sol']}' cod='{$row['cod']}' n='{$row['formula']}' sn='' soldata='{$row['data']}' env='{$row['enviado']}' total='{$row['qtd']}' tipo='12'";
  		$cancela = "<img align='right' sol='{$row['sol']}' class='cancela-item' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
  		echo "<tr><td>{$row['formula']} {$cancela}</td>";
  		echo "<td align='center'>{$row['enviado']}/{$row['autorizado']}</td><td class='dados' $dados >$envio</td></tr>";
  	}
  	
  	
  	echo "</table><br/>";  	
  	echo "<button id='salvar-envio' style='display:inline;float:left;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
  	<span class='ui-button-text'>Enviar</span></button>";
  	echo "<form action='imprimir_separacao.php?p={$p}' method='POST' target='_blank'>";
  	echo "<button style='display:inline;float:left;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
  	<span class='ui-button-text'>Imprimir</span></button>";
  	echo "</form>";
  	echo "</p>";
  	echo "</div>";
  	
  	
  }
  
  
 
  
}

?>