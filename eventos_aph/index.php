<?php

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../db/config.php';

require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';


use \App\Controllers\EventosAPH;

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
	"GET" => [
		'/eventos_aph/?action=menu'  => '\App\Controllers\EventosAPH::menu',
		'/eventos_aph/?action=form-eventos-aph'  => '\App\Controllers\EventosAPH::form',
		'/eventos_aph/?action=pesquisar-evento-aph'  => '\App\Controllers\EventosAPH::pesquisarForm',
		'/eventos_aph/?action=finalizar-evento-aph'  => '\App\Controllers\EventosAPH::finalizarForm',
		'/eventos_aph/?action=visualizar-evento-aph'  => '\App\Controllers\EventosAPH::visualizar',
		'/eventos_aph/?action=relatorio-eventos-aph-realizados' => '\App\Controllers\EventosAPH::relatorioFinalizadosForm',
		'/eventos_aph/?action=excel-relatorio-eventos-aph-realizados' => '\App\Controllers\EventosAPH::relatorioFinalizadosExcel'
		],
	"POST" => [
		'/eventos_aph/?action=salvar-evento-aph'  => '\App\Controllers\EventosAPH::salvar',
		'/eventos_aph/?action=pesquisar-evento-aph' => '\App\Controllers\EventosAPH::pesquisar',
		'/eventos_aph/?action=finalizar-evento-aph'  => '\App\Controllers\EventosAPH::finalizar',
		'/eventos_aph/?action=cancelar-evento-aph'  => '\App\Controllers\EventosAPH::cancelar',
		'/eventos_aph/?action=relatorio-eventos-aph-realizados' =>'\App\Controllers\EventosAPH::buscarRelatorioEventosAPHFinalizados'
		]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}
