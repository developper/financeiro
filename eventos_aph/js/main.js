$(function($) {
    $(".chosen-select").chosen({search_contains: true});


    $('.telefone').mask('(99) 9999-9999');
    $('.hora').mask('99:99');
    $('.cnpj').mask('99.999.999/9999-99');
    $('.cpf').mask('999.999.999-99');
    //$('.rg').mask('**.***.***-**', {
    //    translation: {
    //        '*': {
    //            pattern: /[A-Za-z0-9]/, optional: true
    //        }
    //    }
    //});

    $("input[name='cidade_cliente']").autocomplete({
        source: "/utils/busca_cidade.php",
        minLength: 3,
        select: function( event, ui ) {
            //$("input[name='busca-cidade']").val('');
            $("input[name='id_cidade_cliente']").val(ui.item.id);
            $("input[name='cidade_cliente']").val(ui.item.value);
            $('#local').focus();
            ///$('#busca-cidade').val('');
            return false;
        }
    });
    $("input[name='cidade_cliente']").keypress(function(){
        $("input[name='id_cidade_cliente']").val('');

    });

    $("#salvar-evento-aph").click(function(){

       if(validar_campos('right')){
           var dados = $('form').serialize();

           $.post( '/eventos_aph/?action=salvar-evento-aph',
               dados,
               function(r){
                    if(r == 1){
                        alert('Evento/APH salvo com sucesso!');
                        window.location = "?action=menu";
                    }else{
                        alert("Erro ao salvar Evento/APH. Entre em contato com o TI");
                        return false;
                    }
               });
       }
        return false;
    });

    $('.data-evento').datepicker({
            dateFormat: "dd/mm/yy"

    });
    $("#cliente_existe").change(function () {
        if($(this).val() == 'N'){
            $("#fornecedor-cliente").attr('disabled', true).val('');

            $("#fornecedor-cliente").removeClass('OBG_CHOSEN');
            var estilo = $("#fornecedor-cliente").parent().children('div').attr('style');
            $("#fornecedor-cliente").parent().children('div').removeAttr('style');
            $("#fornecedor-cliente").parent().children('div').attr('style', estilo.replace("border: 3px solid red",''));
            $("#fornecedor-cliente-nao-existe").show();
            $("#fornecedor-cliente-nao-existe input").each(function () {
                $(this).addClass('OBG');
            });
            $('#cnpj').removeClass('OBG');
            $('#im').removeClass('OBG');
            $(".fisica-juridica").each(function(){
                if($(this).val() == 0){
                    $(this).attr('checked', true);
                }else{
                    $(this).attr('checked', false);
                }
            });

        }else{
            $("#fornecedor-cliente").attr('disabled', false).val('');
            $("#fornecedor-cliente").addClass('OBG_CHOSEN');
            $("#fornecedor-cliente-nao-existe").hide();
            $("#fornecedor-cliente-nao-existe input[type!='radio']").each(function () {
                $(this).removeClass('OBG');
                $(this).val('');
                $(this).attr('style','');
            });
            $("#fornecedor-cliente-nao-existe input[type='radio']").each(function () {
                $(this).removeClass('OBG');
                $(this).removeClass('OBG_RADIO');
                $(this).attr('checked', false);
                $(this).attr('style','');
            });

        }
        $("#fornecedor-cliente").trigger("chosen:updated");

    });
    $(".fisica-juridica").change(function() {
        var $this = $(this);
        var selected = $this.val();
        if(selected == '1'){
            $('#cpf').attr('disabled', true).val('');
            $('#cpf').removeClass('OBG');
            $('#cnpj').removeAttr('disabled');
            $('#cnpj').addClass('OBG');
            $("#im").removeAttr('disabled');
            $('#im').addClass('OBG');
            $('#cpf').attr('style','');

            $(".label-nome-fantasia").text('Nome Fantasia:');
            $(".label-razao-social").text('Razão Social:');
            $(".label-ies-rg").text('Insc. Estadual:');
        } else {
            $('#cnpj').attr('disabled', true).val('');
            $('#cpf').removeAttr('disabled');
            $('#cnpj').removeClass('OBG');
            $('#im').removeClass('OBG');
            $("#im").attr('disabled', true).val('');
            $('#cpf').addClass('OBG');
            $('#im').attr('style','');
            $('#cnpj').attr('style','');

            $(".label-nome-fantasia").text('Nome:');
            $(".label-razao-social").text('Apelido:');
            $(".label-ies-rg").text('RG:');
        }
    });

    $('#cpf').blur(function(){
        var cpf = $(this).val(),
            isValid;
        if(cpf != '') {
            isValid = verificarCPF(cpf);

            if (!isValid) {
                alert('CPF inválido!');
                $(this).val('');
            }
        }
    });

    $('#cnpj').blur(function(){
        var cnpj = $(this).val(),
            isValid;
        if(cnpj != '') {
            isValid = verificarCNPJ(cnpj);

            if (!isValid) {
                alert('CNPJ inválido!');
                $(this).val('');
            }
        }
    });

    function verificarCPF(strCPF) {
        var soma = 0;
        var resto;
        var exp = /\.|\-/g
        strCPF = strCPF.toString().replace( exp, "" );
        if (strCPF == "00000000000")
            return false;
        for (i = 1; i <= 9; i++)
            soma = soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(strCPF.substring(9, 10)) )
            return false;
        soma = 0;
        for (i = 1; i <= 10; i++)
            soma = soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(strCPF.substring(10, 11) ) )
            return false;
        return true;
    }

    function verificarCNPJ(strCNPJ) {
        var valida = new Array(6,5,4,3,2,9,8,7,6,5,4,3,2);
        var dig1 = new Number;
        var dig2 = new Number;

        var exp = /\.|\-|\//g
        var cnpj = strCNPJ.toString().replace( exp, "" );
        var digito = new Number(eval(cnpj.charAt(12) + cnpj.charAt(13)));

        for(var i = 0; i < valida.length; i++){
            dig1 += (i > 0 ? (cnpj.charAt(i - 1) * valida[i]) : 0);
            dig2 += cnpj.charAt(i) * valida[i];
        }
        dig1 = (((dig1 % 11) < 2) ? 0 : (11 - (dig1 % 11)));
        dig2 = (((dig2 % 11) < 2) ? 0 : (11 - (dig2 % 11)));

        if(((dig1 * 10) + dig2) != digito) {
            return false;
        }
        return true;
    }

    $("#sera_cobrada").change(function(){
        var val = $(this).val();
        $("#justificativa_nao_cobrar").attr('style', '');
        $("#justificativa_nao_cobrar").attr('disabled', 'disabled').removeClass('OBG').val('');

       if(val == 'N') {
            $("#justificativa_nao_cobrar").removeAttr('disabled').addClass('OBG');

        }
    });
    if($("#recebido").val() != 'S'){
        $("#forma-pagamento").removeAttr('style');
        $("#forma-pagamento").attr('disabled', 'disabled').val('');
        $("#forma-pagamento").removeClass('OBG');

        $("#recebido-em").removeAttr('style');
        $("#recebido-em").attr('disabled', 'disabled').val('');
        $("#recebido-em").removeClass('OBG');

        $("#conta-recebido").attr('disabled', 'disabled').val('');
        $("#conta-recebido").removeClass('OBG_CHOSEN');
        $("#conta-recebido").trigger("chosen:updated");
    }

    $("#recebido").change(function () {
        if($(this).val() != 'S') {
            $("#forma-pagamento").attr('disabled', 'disabled').val('');
            $("#forma-pagamento").removeClass('OBG');
            $("#forma-pagamento").removeAttr('style');

            $("#recebido-em").removeAttr('style');
            $("#recebido-em").attr('disabled', 'disabled').val('');
            $("#recebido-em").removeClass('OBG');

            $("#conta-recebido").attr('disabled', 'disabled').val('');
            $("#conta-recebido").removeClass('OBG_CHOSEN');
            var estilo = $("#conta-recebido").parent().children('div').attr('style');
            $("#conta-recebido").parent().children('div').removeAttr('style');
            $("#conta-recebido").parent().children('div').attr('style', estilo.replace("border: 3px solid red",''));

        } else {
            $("#forma-pagamento").removeAttr('disabled');
            $("#forma-pagamento").addClass('OBG');


            $("#recebido-em").removeAttr('disabled');
            $("#recebido-em").addClass('OBG');

            $("#conta-recebido").removeAttr('disabled');
            $("#conta-recebido").addClass('OBG_CHOSEN');

        }
        $("#conta-recebido").trigger("chosen:updated");
    });
    $("#finalizar-evento-aph").click(function(){
        if($('#cliente_existe').val() == 'N'){
            if($('#id_cidade_cliente').val() == '' || $('#id_cidade_cliente').val() == ''){
                alert('O campo Cidade do Cliente deve ser escolhido na lista e não digitado completamente.');
                return false;
            }
        }
        if(validar_campos('right')){

            var dados = $('form').serialize();
            $.post( '/eventos_aph/?action=finalizar-evento-aph',
                dados,
                function(r){
                    if(r == 1){
                        alert('Evento/APH finalizado com sucesso!');
                        window.location = "?action=menu";
                    }else{
                        alert("Erro ao finalizar. "+r);
                        return false;
                    }
                });
        }
        return false;
    });
    //
    //$('#span-remocao-finalizada-convenio').hide();
    //$('#motivo-nao-realizada').hide();
    //
    //$("#remocao-realizada").change(function(){
    //    $('#span-remocao-finalizada-convenio').hide();
    //    $("#motivo").attr('style','background-color:transparent;');
    //    $("#motivo").removeClass('OBG');
    //    $("#motivo").val('');
    //    $('#motivo-nao-realizada').hide();
    //
    //    $("#tabela-propria").removeClass('OBG');
    //    $("#tabela-propria").attr('style','background-color:transparent;');
    //    $("#tabela-propria").val('');
    //
    //    $("select[name ='remocao-fatura']").removeClass('OBG_CHOSEN');
    //    $("select[name ='remocao-fatura']").find('option').eq(0).attr('selected',true);
    //    $("select[name ='remocao-fatura']").parent().children('div').eq(0).children('a').children('span').html('Selecione...');
    //
    //    $("#valor-remocao").attr('style','background-color:transparent;');
    //    $("#valor-remocao").removeClass('OBG');
    //    $("#valor-remocao").val('');
    //
    //    $("#km").attr('style','background-color:transparent;');
    //    $("#km").removeClass('OBG');
    //    $("#km").val('');
    //
    //    $("#data-saida").attr('style','background-color:transparent;');
    //    $("#data-saida").removeClass('OBG');
    //    $("#data-saida").val('');
    //
    //    $("#hora-saida").attr('style','background-color:transparent;');
    //    $("#hora-saida").removeClass('OBG');
    //    $("#hora-saida").val('');
    //
    //    $("#data-chegada").attr('style','background-color:transparent;');
    //    $("#data-chegada").removeClass('OBG');
    //    $("#data-chegada").val('');
    //
    //    $("#hora-chegada").attr('style','background-color:transparent;');
    //    $("#hora-chegada").removeClass('OBG');
    //    $("#hora-chegada").val('');
    //
    //    $("#perimetro").attr('style','background-color:transparent;');
    //    $("#perimetro").removeClass('OBG');
    //    $("#perimetro").val('');
    //
    //
    //    $("select[name ='empresa']").removeClass('OBG_CHOSEN');
    //    $("select[name ='empresa']").find('option').eq(0).attr('selected',true);
    //    $("select[name ='empresa']").parent().children('div').eq(0).children('a').children('span').html('Selecione...');
    //
    //
    //    if ($(this).val() == 'S'){
    //        $("#itens-remocao").show();
    //        $('#span-remocao-finalizada-convenio').show();
    //        $("#tabela-propria").addClass('OBG');
    //        $("select[name ='remocao-fatura']").addClass('OBG_CHOSEN');
    //        $("#valor-remocao").addClass('OBG');
    //        $("#km").addClass('OBG');
    //        $("#data-saida").addClass('OBG');
    //        $("#hora-saida").addClass('OBG');
    //        $("#data-chegada").addClass('OBG');
    //        $("#hora-chegada").addClass('OBG');
    //        $("#perimetro").addClass('OBG');
    //        $("select[name ='empresa']").addClass('OBG_CHOSEN');
    //
    //
    //
    //    }
    //    var estiloRemocaoFatura = $("select[name ='remocao-fatura']").parent().children('div').attr('style');
    //
    //    $("select[name ='remocao-fatura']").parent().children('div').removeAttr('style');
    //    $("select[name ='remocao-fatura']").parent().children('div').attr('style',estiloRemocaoFatura.replace("border: 3px solid red",''));
    //    var estiloEmpresa = $("select[name ='empresa']").parent().children('div').attr('style');
    //    $("select[name ='empresa']").parent().children('div').removeAttr('style');
    //    $("select[name ='empresa']").parent().children('div').attr('style',estiloEmpresa.replace("border: 3px solid red",''));
    //    var nao = 'N';
    //
    //    if ($(this).val() == nao){
    //        $('#motivo-nao-realizada').show();
    //        $("#motivo").addClass('OBG');
    //        $("#itens-remocao").hide();
    //    }
    //    return;
    //});

    $(".valor").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    //$("#finalizar-remocao-convenio").click(function(){
    //    if(validar_campos('right')){
    //        var dados = $('form').serialize();
    //        $.post( '/remocao/?action=finalizar-remocao-convenio',
    //            dados,
    //            function(r){
    //                //console.log(r);
    //                if(r == 1){
    //                    alert('Remoção finalizada com sucesso!');
    //                    window.location = "?action=pesquisar-form";
    //                }else{
    //                    alert("Erro ao finalizar a remoção. Entre em contato com o TI");
    //                    return false;
    //                }
    //            });
    //    }
    //    return false;
    //});



    function validar_campos(div){
        var ok = true;
        $("#"+div+" .OBG").each(function (i){

            if(this.value == ""){
                ok = false;
                this.style.border = "3px solid red";
            } else {
                this.style.border = "";
            }

        });
        $("#"+div+" .COMBO_OBG option:selected").each(function (i){
            if(this.value == "-1"){
                ok = false;
                $(this).parent().css({"border":"3px solid red"});
            } else {
                $(this).parent().css({"border":""});
            }
        });

        $("#"+div+" .OBG_CHOSEN option:selected").each(function (i){

            if(this.value == "" || this.value == -1){

                $(this).parent().parent().children('div').css({"border":"3px solid red"});
            } else {

               var estilo = $(this).parent().parent().children('div').attr('style');
                $(this).parent().parent().children('div').removeAttr('style');
                $(this).parent().parent().children('div').attr('style',estilo.replace("border: 3px solid red",''));
            }
        });

        if(!ok){
            $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
            $("#"+div+" .div-aviso").show();
        } else {
            $("#"+div+" .aviso").text("");
            $("#"+div+" .div-aviso").hide();
        }
        return ok;
    }

    $(".cancelar").live('click',function(){
        var eventoAPH = $(this);
        var id = eventoAPH.attr('id-evento-aph');
        $.post( '/eventos_aph/?action=cancelar-evento-aph',
            {id:id},
            function(r){
                if(r == 1){
                    eventoAPH.parent().parent().parent().remove();
                    alert('Evento/APH cancelado com sucesso!');
                }else{
                    alert('Erro ao cancelar Evento/APH. Entre em contato com a equipe do TI');
                }
            }
        );
    });





});
