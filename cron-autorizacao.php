<?php


require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/db/config.php';


use App\Services\Autorizacoes\CronLiberarItem;
use App\Services\Autorizacoes\RegraFluxoAutorizacoes;


ini_set('display_errors',1);
 $cron = new CronLiberarItem;
 $cron->verificar_itens_restritos();