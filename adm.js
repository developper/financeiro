$(function($){
//  $( "button, input:submit").button();
  
  $(".data").datepicker({
    inline: true,
    changeYear: true,
    changeMonth: true,
    yearRange: '1900:2100'
  });
  
  $(".valor").priceFormat({
    prefix: '',
    centsSeparator: ',',
    thousandsSeparator: '.'
  });
  

  
  
  $(".div-aviso").hide();
  
  function validar_campos(div){
    var ok = true;
    $("#"+div+" .OBG").each(function (i){
      if(this.value == ""){
		ok = false;
		this.style.border = "3px solid red";
      } else {
		this.style.border = "";
      }
    });
    $("#"+div+" .COMBO_OBG option:selected").each(function (i){
      if(this.value == "-1"){
		ok = false;
		$(this).parent().css({"border":"3px solid red"});
      } else {
		$(this).parent().css({"border":""});
      }
    });
    if(!ok){
      $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
      $("#"+div+" .div-aviso").show();
    } else {
      $("#"+div+" .aviso").text("");
      $("#"+div+" .div-aviso").hide();
    }
    return ok;
  }
  
  $(".permissoes").click(function(){
  	$("#dialog-acesso").attr("cod",$(this).attr("cod"));
  	$("#dialog-acesso").dialog("open");
  });
  
  $("#dialog-acesso").dialog({
  	autoOpen: false, modal: true, height: 300,
  	open: function(event, ui) {
      $.post("query.php",{query: "show-permissoes", cod: $("#dialog-acesso").attr("cod")},function(r){
				$("#dialog-acesso").html(r);
      });
    },
    buttons: {
      Salvar: function() {
      	var modadm = $("#dialog-acesso input[name='modadm']").attr("checked"); var modaud = $("#dialog-acesso input[name='modaud']").attr("checked");
      	var modenf = $("#dialog-acesso input[name='modenf']").attr("checked"); var modfin = $("#dialog-acesso input[name='modfin']").attr("checked");
      	var modlog = $("#dialog-acesso input[name='modlog']").attr("checked"); var modmed = $("#dialog-acesso input[name='modmed']").attr("checked");
      	var modrem = $("#dialog-acesso input[name='modrem']").attr("checked");
  		$.post("query.php",{query: "salvar-permissoes", cod: $("#dialog-acesso").attr("cod"), 
  							modadm: modadm, modaud: modaud, modenf: modenf, modfin: modfin, modlog: modlog, modmed: modmed, modrem: modrem},
	 		function(r){alert(r);});
	  	$( this ).dialog( "close" );
      },
      Cancelar: function(){
		$(this).dialog( "close" );
      }
    }
  });
    
  $("#inserir-usuario,#salvar-usuario").click(function(){
    if(!validar_campos("div-novo-usuario"))
      return false;
  });
  
  $("#salvar-paciente").click(function(){
    if(validar_campos("div-novo-paciente")){
      $.post("query.php",{query: "salvar-paciente", nome: $("input[name='nome']").val(), nascimento: $("input[name='nascimento']").val(), 
      		  sexo: $("input[name='sexo']:checked").val(), codigo: $("input[name='codigo']").val(), convenio: $("select[name='convenio'] option:selected").val(), 
      		  internacao: $("input[name='internacao']").val(), local: $("input[name='local']").val(), diagnostico: $("input[name='diagnostico']").val(), 
      		  solicitante: $("input[name='solicitante']").val(), empresa: $("select[name='empresa'] option:selected").val(), 
      		  cuid: $("input[name='cuidador']").val(), relcuid: $("input[name='relcuid']").val(), csexo: $("input[name='csexo']:checked").val(),
      		  resp: $("input[name='responsavel']").val(), relresp: $("input[name='relresp']").val(), rsexo: $("input[name='rsexo']:checked").val()},
			  function(retorno){
			    if(retorno == "erro") alert("Erro ao inserir novo paciente, tente mais tarde!");
			    else window.location = '?adm=paciente&act=listar';
			  }
	    );
    }
    return false;
   });
  
  $("#editar-paciente").click(function(){
    if(validar_campos("div-editar-paciente")){
      $.post("query.php",{query: "editar-paciente", id: $("input[name='id']").val(), nome: $("input[name='nome']").val(), nascimento: $("input[name='nascimento']").val(), sexo: $("input[name='sexo']:checked").val(), 
			  codigo: $("input[name='codigo']").val(), convenio: $("select[name='convenio'] option:selected").val(), internacao: $("input[name='internacao']").val(),
			  local: $("input[name='local']").val(),diagnostico: $("input[name='diagnostico']").val(), solicitante: $("input[name='solicitante']").val(),
			  empresa: $("select[name='empresa'] option:selected").val(),
			  cuid: $("input[name='cuidador']").val(), relcuid: $("input[name='relcuid']").val(), csexo: $("input[name='csexo']:checked").val(),
      		  resp: $("input[name='responsavel']").val(), relresp: $("input[name='relresp']").val(), rsexo: $("input[name='rsexo']:checked").val()},
			  function(retorno){
			    if(retorno == "erro") alert("Erro ao inserir novo paciente, tente mais tarde!");
			    else window.location = '?adm=paciente&act=listar';
			  }
	    );
    }
    return false;
   });
  
  $("#dialog-add-acompanhamento").dialog({
    autoOpen: false, modal: true, width: 400, height: 230,
    open: function(event, ui) {
      $("#dialog-add-acompanhamento .aviso").css({"background-color":"", "font-weight":""});
      $("#dialog-add-acompanhamento .aviso").text("");
      $(".OBG").each(function (i){this.style.border = "";});
      $.post("query.php",{query: "funcionarios", id: $("#div-detalhes-paciente").attr("cod"), tipo: $(this).attr("tipo")},function(retorno){
	$("#dialog-add-acompanhamento .nomes").html(retorno);
      });
    },
    buttons: {
      Ok: function() {
	if(validar_campos("dialog-add-acompanhamento")){
	  $.post("query.php",{query: "adicionar-acompanhamento", paciente: $("#div-detalhes-paciente").attr("cod"), 
		 funcionario: $("#dialog-add-acompanhamento .nomes option:selected").val(), inicio: $("input[name='inicio']").val()},
		 function(retorno){if(retorno == "erro") alert("ERRO: Não foi possível salvar. Tente mais tarde!")});
	  $( this ).dialog( "close" );
	}
      },
      Cancelar: function(){
	$( this ).dialog( "close" );
	window.reload;
      }
    },
    close: function(ev, ui) { window.location.reload() }
  });
  
  $("#dialog-del-acompanhamento").dialog({
    autoOpen: false, modal: true, width: 400, height: 230,
    buttons: {
      Ok: function() {
	$.post("query.php",{query: "remover-acompanhamento", id: $(this).attr("cod")},
		 function(retorno){if(retorno == "erro") alert("ERRO: Não foi possível remover. Tente mais tarde!")});
	  $( this ).dialog( "close" );
      },
      Cancelar: function(){
	$( this ).dialog( "close" );
      }
    },
    close: function(ev, ui) { window.location.reload() }
  });
  
  $("#dialog-end-acompanhamento").dialog({
    autoOpen: false, modal: true, width: 400, height: 230,
    open: function(event, ui) {
      $("#dialog-end-acompanhamento .aviso").css({"background-color":"", "font-weight":""});
      $("#dialog-end-acompanhamento .aviso").text("");
      $(".OBG").each(function (i){this.style.border = "";});
    },
    buttons: {
      Ok: function() {
	if(validar_campos("dialog-end-acompanhamento")){
	  $.post("query.php",{query: "finalizar-acompanhamento", id: $(this).attr("cod"), fim: $("input[name='fim']").val()},
		 function(retorno){if(retorno == "erro") alert("ERRO: Não foi possível finalizar. Tente mais tarde!")});
	  $( this ).dialog( "close" );
	}
      },
      Cancelar: function(){
	$( this ).dialog( "close" );
      }
    },
    close: function(ev, ui) { window.location.reload() }
  });
  
  $("#dialog-history-acompanhamento").dialog({
    autoOpen: false, modal: true, width: 600, height: 400,
    open: function(event, ui) {
      $("#div-history").html("<img src='../utils/load.gif' /> Aguarde ...");
      $.post("query.php",{query: "history", id: $("#div-detalhes-paciente").attr("cod"), tipo: $(this).attr("tipo")},function(retorno){
	$("#div-history").html(retorno);
      });
    }
  });
  
  $(".add").click(function(){
    $("#dialog-add-acompanhamento").attr("tipo",$(this).attr("tipo"));
    $("#dialog-add-acompanhamento").dialog("open");
    return false;
  });
  
  $(".del").click(function(){
    $("#dialog-del-acompanhamento").attr("cod",$(this).parent().parent().attr("cod"));
    $("#dialog-del-acompanhamento").dialog("open");
    return false;
  });
  
  $(".end").click(function(){
    $("#dialog-end-acompanhamento").attr("cod",$(this).parent().parent().attr("cod"));
    $("#dialog-end-acompanhamento").dialog("open");
    return false;
  });
  
  $(".history").click(function(){
    $("#dialog-history-acompanhamento").attr("tipo",$(this).attr("tipo"));
    $("#dialog-history-acompanhamento").dialog("open");
    return false;
  });
  
 //***************** SEGUROS SAUDE **********************/
  
  $("#salvar-novo-plano").click(function(){
  	if(validar_campos("div-novo-plano")){
  		var nome = $("input[name='plano']").val();
  		var itens = "";
   		var total = $(".valor").size();
  		$(".valor").each(function(i){
  			var cobranca = $(this).attr("cod");
  			var valor = $(this).val();
	  		var linha = cobranca+"#"+valor;
			if(i<total - 1) linha += "$";
			itens += linha;
  		});
  		$.post("query.php",{query: "salvar-novo-plano", plano: nome, dados: itens},function(r){
  			if(r == 1){
  				window.location = "?adm=planos";
  			} else {
  				alert(r);
  			}
  		});
  	}
  	return false;
  });
  
  $("#salvar-edicao-plano").click(function(){
  	if(validar_campos("div-novo-plano")){
  		var nome = $("input[name='plano']").val();
  		var id = $("input[name='plano']").attr("cod");
  		var itens = "";
   		var total = $(".valor").size();
  		$(".valor").each(function(i){
  			var cobranca = $(this).attr("cod");
  			var valor = $(this).val();
	  		var linha = cobranca+"#"+valor;
			if(i<total - 1) linha += "$";
			itens += linha;
  		});
  		$.post("query.php",{query: "salvar-edicao-plano", id: id, plano: nome, dados: itens},function(r){
  			if(r == 1){
  				window.location = "?adm=planos";
  			} else {
  				alert(r);
  			}
  		});
  	}
  	return false;
  });
  
//*************** FICHAS DE CAPTACAO ******************//
  
  $(".enviar-ficha").click(function(){
  	$("#dialog-upload-ficha input[name='tipo']").val($(this).attr("tipo"));
  	$("#dialog-upload-ficha").dialog("open");
  });
  
  $("#dialog-upload-ficha").dialog({
  	autoOpen: false, modal: true, width: 600, position: 'top',
  	buttons: {
  		"Fechar" : function(){
  			var item = $("#formulario").attr("item");
  			$("#kits-"+item).hide();
  			$(this).dialog("close");
  		}
  	}
  });
  
//**************** FILIAIS *******************//

  $("#nova-filial").click(function(){
  	$("#salvar-filial").attr("op","novo");
  	$("#salvar-filial").attr("cod","-1");
  	$("#dialog-filial input[name='nome-filial']").val("");
  	$("#dialog-filial").dialog("open");
  });
  
  $(".editar-filial").click(function(){
  	$("#salvar-filial").attr("op","editar");
  	$("#salvar-filial").attr("cod",$(this).attr("cod"));
  	$("#dialog-filial input[name='nome-filial']").val($(this).attr("nome"));
  	$("#dialog-filial").dialog("open");
  });
  
  $("#salvar-filial").click(function(){
  	if(validar_campos("dialog-filial")){
	  	var nome = $("#dialog-filial input[name='nome-filial']").val();
  		var cod = $("#salvar-filial").attr("cod");
	  	var op = $("#salvar-filial").attr("op");
  		$.post("query.php",{query: "salvar-filial", nome: nome, cod: cod, op: op},function(r){
  			if(r==1)
  				window.location = '?adm=filiais';
	  		else alert(r);
  		});
  	}
  	return false;
  });

  $("#dialog-filial").dialog({
  	autoOpen: false, modal: true, width: 800, position: 'top',
  	buttons: {
  		"Fechar" : function(){
  			$(this).dialog("close");
  		}
  	}
  });

  $(".excluir-filial").click(function(){
  	if(confirm("Deseja realmente remover a filial?")){
  		$.post("query.php",{query: "excluir-filial", cod: $(this).attr("cod")},function(r){
  			if(r==1)
  				window.location = '?adm=filiais';
  			else alert(r);
  		});
  	}
  	return false;
  });
  
});