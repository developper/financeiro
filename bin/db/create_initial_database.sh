!#/bin/bash
export PATH_BASE="./def/refresh/"
export CREATE_TABLES_PATH="$PATH_BASE/02_createTables.sql"
export REFERENCES_PATH="$PATH_BASE/03_references.sql"
export USER=sismederi
export PASS=sismederi
export SCHEMA=sismederi
export REFERENCES_TABLES=(
  agenda_frequencia
  aplicacaofundos
  assinaturas
  banco_fornecedor
  brasindice
  centrocustos
  cfop
  cid10
  cidades
  classmedicamentos  
  cobrancaplanos
  cuidadosespeciais
  dietas
  especialidade_medica
  estados
  exame_medico
  exame_medico_tipo
  examesegmentar
  frequencia
  grupo_nead
  grupos
  justificativa_itens_negados
  justificativa_profissional_relatorio
  justificativa_solicitacao_avulsa
  kitsmaterial
  MATERIAIS_TEMP
  medicacoes
  modalidade_home_care
  naturezanotas
  nomesfantasia
  orientacoes_alta_enfermagem
  origemfundos  
  pastasProntuarios
  prescricao_enf_cuidado
  prescricao_enf_grupo
  principioativo
  procedimentos
  quadro_clinico
  quadro_feridas_enf_contaminacao
  quadro_feridas_enf_exsudato
  quadro_feridas_enf_grau
  quadro_feridas_enf_humidade
  quadro_feridas_enf_lesao
  quadro_feridas_enf_local
  quadro_feridas_enf_pele
  quadro_feridas_enf_periodo
  quadro_feridas_enf_profundidade
  quadro_feridas_enf_status
  quadro_feridas_enf_tamanho
  quadro_feridas_enf_tecido
  quadro_feridas_enf_tempo
  quadro_feridas_enf_tipo
  relacionamento_pessoa
  repousos
  score
  score_item
  servicos_terceirizados
  simpro
  soros
  statuspaciente
  subnatureza
  tipo_conta_bancaria
  tipo_logradouro
  tiss_acomodacao
  tiss_carater_atendimento
  tiss_conselho_profissional
  tiss_indicador_acidente
  tiss_motivo_saida
  tiss_outras_despesas_tipo
  tiss_outras_despesas_tipo_interno
  tiss_regime_internacao
  tiss_tabela
  tiss_tipo_atendimento
  tiss_tipo_faturamento
  tiss_tipo_internacao
  tiss_tipo_saida
  tiss_unidade_medida
  umedidas
  unidades_fatura
  viaadm
)

REFERENCES_TABLES_CMD=''
for REFERENCE_TABLE_NAME in "${REFERENCES_TABLES[@]}"
do :
   REFERENCES_TABLES_CMD+=" ${REFERENCE_TABLE_NAME}"
done

# Gera APENAS a estrutura
mysqldump --no-data --user=$USER --password=$PASS --skip-add-drop-table --compact --skip-add-locks --skip-comments --verbose $SCHEMA | sed 's/ AUTO_INCREMENT=[0-9]*\b//' > $CREATE_TABLES_PATH

# Gera APENAS os dados básicos de referência
mysqldump --user=$USER --password=$PASS --compact --skip-add-locks --skip-comments --extended-insert --no-create-info $SCHEMA $REFERENCES_TABLES_CMD --verbose > $REFERENCES_PATH

