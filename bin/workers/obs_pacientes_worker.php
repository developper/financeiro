<?php

require __DIR__ . '/../../vendor/autoload.php';

define('OBS_CONFIG_KEY', 'obsequium_api');
define('QUEUE_OBS_PACIENTE_NAME', 'obsequium.pacientes');

use App\Services\Queue;
use App\Core\Config;
use App\Integration\Obsequium\PacientesAPI;
use App\Integration\Obsequium\PacienteAction;

$queue = new Queue;
$pacienteApi = new PacientesAPI((object) Config::get(OBS_CONFIG_KEY));

$tasks[PacienteAction::ACTION_CREATE] = function($data) use ($pacienteApi) {
  $isCreated = $pacienteApi->create($data->user, [(array) $data->paciente]);

  if ($isCreated) {
    echo 'Paciente foi criado: ', json_encode($data), PHP_EOL;
  }
};

$tasks[PacienteAction::ACTION_UPDATE] = function($data) use ($pacienteApi) {
  $isUpdated = $pacienteApi->update($data->user, [(array) $data->paciente]);

  if ($isUpdated) {
    echo 'Paciente foi atualizado: ', json_encode($data), PHP_EOL;
  }
};

while(true) {
  $job = $queue->listen(QUEUE_OBS_PACIENTE_NAME);

  $data = json_decode($job->getData());

  if (isset($tasks[$data->action]))
    call_user_func($tasks[$data->action], $data);

  $queue->delete($job);
}