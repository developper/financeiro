<?php

require __DIR__ . '/../../vendor/autoload.php';

use App\Services\Queue;
use App\Core\Config;
use App\Helpers\Storage;
use App\Integration\GoogleCloud\StorageFactory;

$gstorage = StorageFactory::getDefault();

define('QUEUE_NAME', Config::get('GS_FERIDAS_QUEUE'));
define('PRIVATE_KEY', Config::get('GAE_PRIVATE_KEY'));
define('GS_BUCKET_NAME', Config::get('GS_BUCKET_NAME'));
define('ENV', Config::get('APP_ENV'));
define('USER', Config::get('SHELL_EXEC_USER'));

$queue = new Queue;

try {
  while(true) {
    $job = $queue->listen(QUEUE_NAME);
    echo '==> Processing...', PHP_EOL, $dataJson = $job->getData(), PHP_EOL;

    if (false !== GS_BUCKET_NAME) {
      $data = json_decode($dataJson);

      if (isset($data->images) && !empty($data->images)) {
        foreach ($data->images as $image) {

          if (! is_readable($image->source)) {
            echo "Não foi possível ler o arquivo: $image->source", PHP_EOL;
            continue;
          }

          $object = $gstorage->sendFile(GS_BUCKET_NAME, $image->source, $image->target, ['predefinedAcl' => 'publicRead']);

          //echo var_dump($object);

          if (false === unlink($image->source)) {
            echo "Ocorreu algum erro ao excluir o arquivo: $image->source", PHP_EOL;
            continue;
          }
        }
      }

      if (isset($job) && !empty($job))
        $queue->delete($job);

      echo '==> Processed!', PHP_EOL;
    }
  }
} catch (\Exception $e) {
  echo '==> Error Exception: ' . $e->getMessage();

  if (ENV !== 'dev') {
    $mail = new App\Services\Mail;
    $subject = '(Feridas-Worker-' . ENV . ') ' . 'Exception: ' . $e->getMessage();
    $message = $e->getMessage();
    $enviado = $mail->send(['ti@mederi.com.br'], $subject, $message);
  }
}
