$(function($){
    $('#pacientes').chosen({search_contains: true});
    $('#itens-ativos').chosen({search_contains: true});

    $('#pacientes').live('change', function () {
        if($(this).val() != '') {
            var data = {action: 'buscar-prescricoes-ativas', paciente: $(this).val()};
            $.ajax(
                {
                    url: '/medico/prescricao/',
                    method: 'GET',
                    data: $.param(data),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    beforeSend: function () {
                        $("#loading").append('<b>Buscando...</b> <img src="/utils/load.gif">');
                    },
                    success: function (response) {
                        response = JSON.parse(response);

                        $("#loading").html('');
                        $("#bloco-itens-ativos").show();

                        fillItensAtivos(response);
                    }
                }
            );
        }
    });

    function fillItensAtivos(response) {
        var prescricao = 0, inicioPrescricao, fimPrescricao;

        $("#bloco-itens-ativos").html('');

        $.each(response, function (key, value) {
            inicioPrescricao    = value.inicioPrescricao.split('-').reverse().join('/');
            fimPrescricao       = value.fimPrescricao.split('-').reverse().join('/');

            if(prescricao != value.prescricao){
                if(prescricao != 0) {
                    $("#itens-ativos-" + prescricao).trigger("chosen:updated");
                }
                prescricao = value.prescricao;

                $("#bloco-itens-ativos").append(
                    '<p>' +
                    '   <strong>Itens ativos da Prescrição ' + value.carater + ' #' + prescricao + ' de ' + inicioPrescricao + ' à ' + fimPrescricao + ':</strong><br>' +
                    '   <select name="itens_ativos" multiple class="itens-ativos" data-placeholder="Escolha os itens que pretende suspender..." id="itens-ativos-' + prescricao + '" style="width: 700px;">' +
                    '   </select>' +
                    '</p>'
                );

                $('#itens-ativos-' + prescricao).chosen({search_contains: true});
            }
            inicioPrescricao    = value.inicioItemPrescricao.split('-').reverse().join('/');
            fimPrescricao       = value.fimItemPrescricao.split('-').reverse().join('/');

            $("#itens-ativos-" + prescricao).append(
                '<option nome="' + value.nome + '" obs="' + value.obs + '" prescricao="' + value.prescricao + '" itemid="' + value.itemId + '" tiss="' + value.tiss + '" inicio="' + value.inicioPrescricao + '" ' +
                'fim="' + value.fimPrescricao + '" value="' + value.itemPrescricaoId + '" inicioItem="' + value.inicioItemPrescricao + '" fimItem="' + value.fimItemPrescricao + '" tipoItem ="'+value.tipo+'">' +
                value.nomeItem + ' (' + inicioPrescricao + ' à ' + fimPrescricao + ')' +
                '</option>'
            );
        });

        $("#itens-ativos-" + prescricao).trigger("chosen:updated");
    }

    $("#suspender").click( function () {
        var $itensAtivosElem    = $('.itens-ativos option:selected');
        var numSelected         = $itensAtivosElem.length;
        var prescAux            = 0;

        $("#response").show();

        if(numSelected > 0) {
            $itensAtivosElem.each( function () {
                var paciente, prescricao, itemId, tiss, nome, obs, fimPrescricao,
                    inicioPrescricao, itemPrescricaoId, fimItem, inicioItem, nomeItem,tipoItem,
                $this = $(this), $responseElem;

                $responseElem       = $("#response");
                paciente            = $("#pacientes option:selected").val();
                prescricao          = $this.attr('prescricao');
                itemId              = $this.attr('itemid');
                tiss                = $this.attr('tiss');
                nome                = $this.attr('nome');
                obs                 = $this.attr('obs');
                inicioPrescricao    = $this.attr('inicio');
                fimPrescricao       = $this.attr('fim');
                itemPrescricaoId    = $this.val();
                inicioItem          = $this.attr('inicioitem');
                fimItem             = $this.attr('fimitem');
                nomeItem            = $this.text();
                tipoItem            = $this.attr('tipoItem');

                $("#paciente-id").val(paciente);
                $("#botao-salvar").show();

                if(prescAux != prescricao && $("#itens-suspensos-" + prescricao).length == 0) {
                    prescAux = prescricao;
                    $responseElem.append(
                        criarInputHidden('prescricao', prescricao) +
                        '<table class="mytable" id="itens-suspensos-' + prescricao + '" width="100%">' +
                        '<thead>' +
                        '<tr>' +
                        '<th align="center" colspan="3">PRESCRIÇÃO DE ' + criarInputDate('inicio_prescricao', inicioPrescricao, 'inicio') + ' À ' + criarInputDate('fim_prescricao', fimPrescricao, 'fim') + '</th>' +
                        '</tr>' +
                        '<tr>' +
                        '<th align="center">ITEM</th>' +
                        '<th align="center">INÍCIO</th>' +
                        '<th align="center">FIM</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                        '</table>'
                    );
                }

                $("#itens-suspensos-" + prescricao + " tbody").append(
                    '<tr>' +
                    '   <td>' +
                    '       <img src="/utils/delete_16x16.png" class="remover-item" nome="' + nome + '" obs="' + obs + '" prescricao="' + prescricao + '" itemprescricao="' + itemPrescricaoId + '" itemid="' + itemId + '" tiss="' + tiss + '" inicio="' + inicioPrescricao + '" fim="' + fimPrescricao + '" inicioitem="' + inicioItem + '" fimitem="' + fimItem + '" itemid="' + itemPrescricaoId + '" nomeitem="' + nomeItem + '" tipoItem ="'+tipoItem+'">' +
                    nomeItem + criarInputHiddenComChave('obs', prescricao, obs) + criarInputHiddenComChave('nome', prescricao, nome) + criarInputHiddenComChave('item_prescricao_id', prescricao, itemPrescricaoId) + criarInputHiddenComChave('item_id', prescricao, itemId) +
                    criarInputHiddenComChave('tiss', prescricao, tiss) + criarInputHiddenComChave('nome_item', prescricao, nomeItem) +   criarInputHiddenComChave('tipo_item', prescricao, tipoItem) + '</td>' +
                    '   <td>' + criarInputDateComChave('inicio_item', prescricao, inicioItem, 'inicio') + '</td>' +
                    '   <td>' + criarInputDateComChave('fim_item', prescricao, fimItem, 'fim') + '</td>' +
                    '</tr>'
                );

                $("#itens-ativos-" + prescricao + " option[value='" + itemPrescricaoId + "']").remove();
                $("#itens-ativos-" + prescricao).val('').trigger("chosen:updated");
            });

            $(".inicio_prescricao").datepicker({
                defaultDate: "+1w",
                minDate: new Date(),
                changeMonth: true,
                numberOfMonths: 1
            });

            $(".fim_prescricao").datepicker({
                defaultDate: "+1w",
                minDate: new Date(),
                changeMonth: true,
                numberOfMonths: 1
            });

            $(".inicio_item").datepicker({
                defaultDate: "+1w",
                minDate: new Date(),
                changeMonth: true,
                numberOfMonths: 1
            });

            $(".fim_item").datepicker({
                defaultDate: "+1w",
                minDate: new Date(),
                changeMonth: true,
                numberOfMonths: 1
            });
        }
    });

    $(".remover-item").live("click", function () {
        var $this = $(this),
            numTrs = $this.closest('tbody').find('tr').length,
            $itensAtivos = $("#itens-ativos-" + $this.attr('prescricao'));

        $("#itens-ativos-" + $this.attr('prescricao') + " option").each(function () {
            var item    = $this.attr('itemprescricao');
            var current = $(this).val();
            var prev    = $(this).prev().val();
            var next    = $(this).next().val();

            if(typeof prev == 'undefined' && item < next && item < current){
                $itensAtivos.prepend(
                    '<option nome="' + $this.attr('nome') + '" obs="' + $this.attr('obs') + '" prescricao="' + $this.attr('prescricao') + '" itemid="' + $this.attr('itemid') + '" tiss="' + $this.attr('tiss') + '" inicio="' + $this.attr('inicio') + '" fim="' + $this.attr('fim') + '" value="' + $this.attr('itemprescricao') + '" inicioitem="' + $this.attr('inicioitem') + '" fimitem="' + $this.attr('fimitem') + '">' + $this.attr('nomeitem') + '</option>'
                );
                return false;
            } else if(item > prev && item < next){
                $(this).after(
                    '<option nome="' + $this.attr('nome') + '" obs="' + $this.attr('obs') + '" prescricao="' + $this.attr('prescricao') + '" itemid="' + $this.attr('itemid') + '" tiss="' + $this.attr('tiss') + '" inicio="' + $this.attr('inicio') + '" fim="' + $this.attr('fim') + '" value="' + $this.attr('itemprescricao') + '" inicioitem="' + $this.attr('inicioitem') + '" fimitem="' + $this.attr('fimitem') + '">' + $this.attr('nomeitem') + '</option>'
                );
                return false;
            } else if(item > prev && typeof next == 'undefined'){
                $itensAtivos.append(
                    '<option nome="' + $this.attr('nome') + '" obs="' + $this.attr('obs') + '" prescricao="' + $this.attr('prescricao') + '" itemid="' + $this.attr('itemid') + '" tiss="' + $this.attr('tiss') + '" inicio="' + $this.attr('inicio') + '" fim="' + $this.attr('fim') + '" value="' + $this.attr('itemprescricao') + '" inicioitem="' + $this.attr('inicioitem') + '" fimitem="' + $this.attr('fimitem') + '">' + $this.attr('nomeitem') + '</option>'
                );
                return false;
            }
        });

        $itensAtivos.trigger("chosen:updated");

        if(numTrs > 1){
            $this.closest('tr').remove();
        } else {
            $this.closest('.mytable').remove();
            $("#botao-salvar").hide();
        }
    });

    function criarInputDate(nome, data, tipo) {
        var valor, newDate = new Date(), day, month, year;

        data = data.split('-');
        if(tipo == 'inicio') {
            day     = newDate.getDate();
            month   = parseInt(newDate.getMonth()) + 1;
            year    = newDate.getFullYear();
            if (parseInt(data[1]) == month) {
                valor = (day < 10 ? "0" + day : day) + '/' + (month < 10 ? "0" + month : month) + '/' + year;
            } else {
                valor = data.reverse().join('/');
            }
        } else {
            valor = data.reverse().join('/');
        }

        return '<input type="text" maxlength="10" name="' + nome + '[]" value="' + valor + '" class="' + nome + '">';
    }

    function criarInputDateComChave(nome, chave, data, tipo) {
        var valor, newDate = new Date(), day, month, year;

        data = data.split('-');
        if(tipo == 'inicio') {
            day     = newDate.getDate();
            month   = parseInt(newDate.getMonth()) + 1;
            year    = newDate.getFullYear();
            if (parseInt(data[1]) == month) {
                valor = (day < 10 ? "0" + day : day) + '/' + (month < 10 ? "0" + month : month) + '/' + year;
            } else {
                valor = data.reverse().join('/');
            }
        } else {
            valor = data.reverse().join('/');
        }

        return '<input type="text" maxlength="10" name="' + nome + '[' + chave + '][]" value="' + valor + '" class="' + nome + '">';
    }

    function criarInputHidden(nome, valor) {
        return '<input type="hidden" name="' + nome + '[]" value="' + valor + '" class="' + nome + '">';
    }

    function criarInputHiddenComChave(nome, chave, valor) {
        return '<input type="hidden" name="' + nome + '[' + chave + '][]" value="' + valor + '" class="' + nome + '">';
    }
});