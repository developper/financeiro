<?php
require $_SERVER['DOCUMENT_ROOT'] . '/utils/codigos.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

ini_set('display_errors', 1);

use \App\Controllers\Medico;

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
	"GET" => [
		'/medico/prescricao/?action=suspender-itens-prescricoes' => '\App\Controllers\Medico::formSuspenderItensPrescricoes',
		'/medico/prescricao/?action=buscar-prescricoes-ativas' => '\App\Controllers\Medico::buscarPrescricoesAtivas',
	],
	"POST" => [
        '/medico/prescricao/?action=suspender-itens-prescricoes' => '\App\Controllers\Medico::suspenderItensPrescricoesAtivas',
	]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
    require_once("templates/404.phtml");
} catch(App\NotFoundException $e) {
	http_response_code(404);
	require_once("templates/404.phtml");
}
