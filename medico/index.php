<?php
include_once('../validar.php');
include_once('medico.php');



if(!function_exists('redireciona')){
	function redireciona($link){
		if ($link==-1){
			echo" <script>history.go(-1);</script>";
		}else{
			echo" <script>document.location.href='$link'</script>";
		}
	}
}

include($_SERVER['DOCUMENT_ROOT'].'/cabecalho.php');
?>

<?php
include($_SERVER['DOCUMENT_ROOT'].'/cabecalho_fim.php');

?>
</div>
<div id="content">
<div id="right">
<?php
   $view = "menu";
   if(isset($_GET["view"])) $view = $_GET["view"];

  if(
      (
          !validar_tipo("modmed","id_user","empresa_principal") &&
          !(validar_tipo("modenf","id_user","empresa_principal") ||
              validar_tipo("modlog","id_user","empresa_principal")
          ) &&
          ($view == "buscar" || $view == "detalhes")
          &&
          !isset($_SESSION['permissoes']['medico'])
      )
  ){
	 redireciona('/inicio.php');
   }
   $m = new Medicos;
   $m->view($view);
?>
</div>
<div id="left">
<?php include($_SERVER['DOCUMENT_ROOT'].'/painel_login.php'); ?>
<div class="box">
<!--  <ul>-->
<!--   <li><a href='?view=buscar'>Prescri&ccedil;&otilde;es</a></li>-->
<!--   <li><a href='../adm/?adm=paciente'>Pacientes</a></li>-->
<!--  </ul>-->
</div>
<br>
<!-- <div class="box">

<div id='calculadora'></div>
</div> -->
</div>
</div>

<script src="medicos.js?v=46.8" type="text/javascript"></script>
<script src="/components/scores/score-nead-2016/js/main.js?v=2.9" type="text/javascript"></script>

</body>
</html>
