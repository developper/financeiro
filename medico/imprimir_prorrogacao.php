<?php

$id = session_id();
if (empty($id))
	session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');
require __DIR__ . '/../vendor/autoload.php';

use \App\Controllers\Administracao as Administracao;
use \App\Models\Administracao\Filial;


require_once('relatorios.php');

$id = session_id();
if (empty($id))
	session_start();

class ImprimirProrrogacao {

	public function cabecalho($id, $empresa = null, $convenio = null) {

		$condp1 = "c.idClientes = '{$id}'";
		$sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresa,
		c.`nascimento`,
		p.nome as Convenio,p.id,
		NUM_MATRICULA_CONVENIO,
		cpf
		FROM
		clientes AS c LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN
		planosdesaude as p ON (p.id = c.convenio)
		WHERE
		{$condp1}
		ORDER BY
		c.nome DESC LIMIT 1";

		$result = mysql_query($sql);
		$result2 = mysql_query($sql2);
		$html .= "<table width=100% >";
		while ($pessoa = mysql_fetch_array($result2)) {
			foreach ($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);
			}

            $pessoa['empresa'] = !empty($empresa) ? $empresa : $pessoa['empresa'];
            $pessoa['Convenio'] = !empty($convenio) ? $convenio : $pessoa['Convenio'];

			$html .= "<br/><tr>";
			$html .= "<td colspan='2'><label style='font-size:10px;color:#8B0000'><b></b></label></td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>PACIENTE </b></label></td>";
			$html .= "<td><label><b>SEXO </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>{$pessoa['paciente']}</td>";
			$html .= "<td>{$pessoa['sexo']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>IDADE </b></label></td>";
			$html .= "<td><label><b>UNIDADE REGIONAL </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>" . join("/", array_reverse(explode("-", $pessoa['nascimento']))) . ' (' . $pessoa['idade'] . " anos)</td>";
			$html .= "<td>{$pessoa['empresa']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>";
			$html .= "<td><label><b>MATR&Iacute;CULA </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>" . strtoupper($pessoa['Convenio']) . "</td>";
			$html .= "<td>" . strtoupper($pessoa['NUM_MATRICULA_CONVENIO']) . "</td>";
			$html .= "</tr>";
			$html.= " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
		}
		$html .= "</table>";

		return $html;
	}

	public function imprimir_prorrogacao($id, $idr, $empresa) {

		$responseEmpresa = Filial::getEmpresaById($empresa);

        		$html .= "<h1><a><img src='../utils/logos/{$responseEmpresa['logo']}' width='100' ></a></h1>";

        $sql = "SELECT 
                  empresas.nome AS empresa, 
                  planosdesaude.nome AS plano 
                FROM 
                  relatorio_prorrogacao_med
                  LEFT JOIN empresas ON relatorio_prorrogacao_med.empresa = empresas.id
                  LEFT JOIN planosdesaude ON relatorio_prorrogacao_med.plano = planosdesaude.id
                WHERE 
                  relatorio_prorrogacao_med.ID = '{$idr}'";
		$rs = mysql_query($sql);
		$rowRel = mysql_fetch_array($rs);

		$html .= $this->cabecalho($id, $responseEmpresa['nome'], $rowRel['plano']);
		$html .= "<form>";

		$sql = "
		SELECT
		r.*,
		DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
		DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
		DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
		u.nome as user,
        u.tipo as utipo, 
        u.conselho_regional
		FROM
		relatorio_prorrogacao_med as r LEFT JOIN
		usuarios as u ON (r.USUARIO_ID = u.idUsuarios)
		WHERE
		r.ID = '{$idr}'
		ORDER BY
		r.DATA DESC";

		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {
            $compTipo = $row['conselho_regional'];

			$id_usuario = $row['USUARIO_ID'];
			$inicio = $row['inicio'];
			$fim = $row['fim'];
			$user = $row['user'] . $compTipo;
			$estd_geral = $row['ESTADO_GERAL'];
			$mucosa = $row['MUCOSA'];
			$mucosaobs = $row['MUCOSA_OBS'];
			$escleras = $row['ESCLERAS'];
			$esclerasobs = $row['ESCLERAS_OBS'];
			$respiratorio = $row['PADRAO_RESPIRATORIO'];
			$respiratorioobs = $row['PADRAO_RESPIRATORIO_OBS'];
			$pa_sistolica = $row['PA_SISTOLICA'];
			$pa_diastolica = $row['PA_DIASTOLICA'];
			$fc = $row['FC'];
			$fr = $row['FR'];
			$temperatura = $row['TEMPERATURA'];
			$dor_sn = $row['DOR'];
			$outro_resp = $row['OUTRO_RESP'];
			$oximetria_pulso = $row['OXIMETRIA_PULSO'];
			$tipo_oximetria = $row['OXIMETRIA_PULSO_TIPO'];
			$litros = $row['OXIMETRIA_PULSO_LITROS'];
			$via_oximetria = $row['OXIMETRIA_PULSO_VIA'];
			$mod_home_care = $row['MOD_HOME_CARE'];
			$deslocamento = $row['DESLOCAMENTO'];
			$deslocamento_distancia = $row['DESLOCAMENTO_DISTANCIA'];
			$justificativaProrrogacao = $row['justificativa'];
            $quadroClinico = $row['quadro_clinico'];
            $regimeAtual = $row['regime_atual'];
            $indicacaoTecnica = $row['indicacao_tecnica'];
			$classificacao_paciente = $row['CLASSIFICACAO_NEAD'];
			$modeloNead = $row['MODELO_NEAD'];
		}

		$sql_problemas = "
			SELECT
				p.*
			FROM
			problemas_relatorio_prorrogacao_med as p
			WHERE
			p.RELATORIO_PRORROGACAO_MED_ID = {$idr}
			ORDER BY
			ID
			";

		$html .= "<p style='text-align:center'><b>RELAT&Oacute;RIO DE PRORROGA&Ccedil;&Atilde;O M&Eacute;DICA DO PER&Iacute;ODO {$inicio} AT&Eacute; {$fim}</b></p>";
		$html .= "<p style='background-color:#EEEEEE;'><b>PROFISSIONAL RESPONS&Aacute;VEL: " . mb_strtoupper($user) . "</b></p>";

		$html .= "<table class='' style='width:100%;'>";
		$html .= "<tr>";
		$html .= "<td><b>Problemas Ativos</td><td colspan='2'><b>Observa&ccedil;&atilde;o</b></td>";
		$html .= "</tr>";
		$result_problemas = mysql_query($sql_problemas);
		while ($row = mysql_fetch_array($result_problemas)) {
			$cid_id = $row['CID_ID'];
			$descricao = $row['DESCRICAO'];
			$observacao = $row['OBSERVACAO'];
			$html .= "<tr><td width='30%'>{$descricao}</td><td colspan='2'>{$observacao}</td></tr>";
		}

		$html .= "</table>";

		$html .= "<table class='' style='width:100%;'>";
		$html .= "<tr style='background-color:#EEEEEE;'>";
		$html .= "<td colspan='5' ><b>EXAMES F&Iacute;SICOS</b></td>";
		$html .= "</tr>";

		$x = '';
		$y1 = '';
		if ($estd_geral == 1) {
			$x = "BOM";
		}
		if ($estd_geral == 2) {
			$x = "REGULAR";
		}
		if ($estd_geral == 3) {
			$x = "RUIM";
		}

		$html .= "<tr><td><b>Estado Geral:</b>  </td><td colspan='4'>{$x}</td></tr>";

		$x = '';
		$y1 = '';
		if ($mucosa == 'n') {
			$x = 'DESCORADAS';
			$y1 = $mucosaobs;
		}
		if ($mucosa == 's') {
			$x = 'Coradas';
			$y1 = $mucosaobs;
		}
		$html .= "<tr><td><b>Mucosas:</b>  </td><td colspan='4'>{$x}  {$y1}</td></tr>";
		$x = '';
		if ($escleras == 'n') {
			$x = 'Ict&eacute;ricas';
			$y1 = $esclerasobs;
		}
		if ($escleras == 's') {
			$x = 'Anict&eacute;ricas';
			$y1 = $esclerasobs;
		}
		$html .= "<tr><td><b>Escleras:</b>  </td><td colspan='4'>{$x}  {$y1}</td></tr>";
		$x = '';
		$y1 = '';
		if ($respiratorio == 'n') {
			$x = 'Taqui/Dispn&eacute;ico';
			$y1 = $respiratorioobs;
		}
		if ($respiratorio == 's') {
			$x = 'Eupn&eacute;ico';
			$y1 = $respiratorioobs;
		}
		if ($respiratorio == '1') {
			$x = 'Outro';
			$y1 = $outro_resp;
		}
		$html .= "<tr><td><b>Padr&atilde;o Respirat&oacute;rio:</b>  </td><td colspan='4'>{$x}  {$y1}</td></tr>";

		$html .= "<tr><td width=20% ><b>Oximetria de Pulso: </b></td>
			<td width=25% colspan='4'>{$oximetria_pulso}&nbsp;&nbsp;&nbsp;";
		$tipo = '';
		if ($tipo_oximetria == 1) {
			$tipo = 'Ar Ambiente';
		}
		if ($tipo_oximetria == 2) {
			$tipo = 'O2 Suplementar';
		}

		$html .= "<b> Tipo: </b>{$tipo}&nbsp;&nbsp;&nbsp;";
		if ($tipo_oximetria == 2) {
			$via = '';
			if ($via_oximetria == 1) {
				$via = 'Cateter Nasal';
			}
			if ($via_oximetria == 2) {
				$via = 'Cateter em Traqueostomia';
			}
			if ($via_oximetria == 3) {
				$via = 'Máscara de Venturi';
			}
			if ($via_oximetria == 4) {
				$via = 'Ventilação Mecânica';
			}
			$html .= "<b> Dosagem: </b>{$litros} l/min &nbsp;&nbsp;&nbsp;";
			$html .= "<b> Via: </b>{$via} &nbsp;&nbsp;&nbsp;";
		}

		$html .= "</td></tr>";

		$html .= "<tr><td width=20%><b>Pressão Arterial (sist&oacute;lica):</b></td><td colspan='4'> {$pa_sistolica} mmhg</td></tr>";
		$html .= "<tr><td width=20%><b>Pressão Arterial (diast&oacute;lica):</b></td><td colspan='4'> {$pa_diastolica} mmhg</td></tr>";
		$html .= "<tr><td width=20%><b>Frequência Cardíaca: </b></td><td colspan='4'> {$fc} bpm</td></tr>";
		$html .= "<tr><td width=20%><b>Frequência Respiratória:</b></td><td colspan='4'> {$fr} irpm</td></tr>";
		$html .= "<tr><td width=20%><b>Temperatura:</b></td><td colspan='4'> {$temperatura} &deg;C</td></tr>";

		if ($dor_sn == 's') {
			$dor_sn = "Sim";
		}
		if ($dor_sn == 'n') {
			$dor_sn = "N&atilde;o";
		}
		$html .= "<tr><td colspan='5' ><b>DOR:</b> {$dor_sn}</td></tr>";

		$html .= "<tr><td colspan='5' >";
		$html .= "<table id='dor' style='width:100%;'>";
		$sql = "select * from dor_relatorio_prorrogacao_med where RELATORIO_PRORROGACAO_MED_ID = {$idr} ";
		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {
			$html .= "<tr><td> <b>Local:</b> {$row['DESCRICAO']}&nbsp;&nbsp; <b>Escala visual:</b> {$row['ESCALA']}&nbsp;&nbsp;<b>Padr&atilde;o:</b>{$row['PADRAO']}&nbsp;&nbsp; </td></tr>";
		}
		$html .= "</table>";
		$html .= "</table></td></tr>";
		//Mostra MODALIDADE HOME CARE
		$html .= "<table class='mytable' style='width:100%;'>";
		$html .= "<tr style='background-color:#EEEEEE;'>";
		$html .= "<td colspan='4'><b>MODALIDADE DE HOME CARE</b></td>";
		if ($mod_home_care == 1) {
			$modalidade = "ASSISTENCIA DOMICILIAR";
		} elseif ($mod_home_care == 2) {
			$modalidade = "INTERNACAO DOMICILIAR 6h";
		} elseif ($mod_home_care == 3) {
			$modalidade = "INTERNACAO DOMICILIAR 12h";
		} elseif ($mod_home_care == 4) {
			$modalidade = "INTERNACAO DOMICILIAR 24H COM RESPIRADOR";
		} elseif ($mod_home_care == 5) {
			$modalidade = "INTERNACAO DOMICILIAR 24H SEM RESPIRADOR";
		}
		$html .= "</tr>";
		$html .= "<tr><td colspan='4'>{$modalidade}</td></tr>";
		$html .= "</table>";

		$html .= "<p><b>Materiais e medica&ccedil;&otilde;es conforme c&oacute;pias das prescri&ccedil;&otilde;es m&eacute;dica e de enfermagem enviados.</b></p>";
		//Mostra PROFISSIONAIS
		$html .= "<table class='mytable' style='width:100%;'>";
		$html .= "<tr>";
		$html .= "<td colspan='3'  style='background-color:#EEEEEE;'><b>PROFISSIONAIS</b></td>";
		$html .= "</tr>";
		$sql = "
				SELECT
					p.*,
					f.frequencia as frequencia
				FROM
					profissionais_relatorio_prorrogacao_med as p INNER JOIN
					frequencia as f ON (p.FREQUENCIA_ID = f.id)
				WHERE
					RELATORIO_PRORROGACAO_MED_ID = {$idr} ";
		$flag_prof = TRUE;
		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {
			if ($flag_prof) {
				$html .= "<tr><td width='45%'> <b>Profissional</b></td> <td><b>Frequencia</b></td> <td><b>Justificativa</b></td></tr>";
				$flag_prof = FALSE;
			}
			$html .= "<tr><td width='45%'> {$row['DESCRICAO']}</td><td>{$row['frequencia']}</td><td>{$row['JUSTIFICATIVA']}</td></tr>";
		}
		$html .= "</table>";
        $html .= "<div style='page-break-before: always;'></div>";
		//Mostra PROCEDIMENTO
		$html .= "<table class='mytable' style='width:100%;'>";
		$html .= "<tr style='background-color:#EEEEEE;'>";
		$html .= "<td colspan='3' ><b>PROCEDIMENTO</b></td>";
		$html .= "</tr>";
		$html .= "</table>";

		//Mostra deslocamento
		if ($deslocamento == 'S') {
			$html .= "<table class='mytable' style='width:100%;'>";
			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td colspan='3' ><b>DESLOCAMENTO</b></td>";
			$html .= "</tr>";
			$html .= "<tr><td colspan='3'>Houve deslocamento da equipe por tratar-se de uma interna&ccedil;&atilde;o localizada a {$deslocamento_distancia}km da sede da empresa</tr></td>";
			$html .= "</table>";
		}
		$html .= "<table style='width:100%;' >";
        if($justificativaProrrogacao != '') {
            $html .= "    <tr><td style='background-color:#EEEEEE;'><b>JUSTIFICATIVA</b></td></tr>
				          <tr><td>{$justificativaProrrogacao}</td></tr>";
        } else {
            $html .= "    <tr><td style='background-color:#EEEEEE;'><b>QUADRO CLÍNICO</b></td></tr>
				          <tr><td>{$quadroClinico}</td></tr></table>";
            $html .= "<table style='width:100%; ' >";
            $html .= "    <tr><td style='background-color:#EEEEEE;'><b>MODALIDADE DE ATENDIMENTO ATUAL</b></td></tr>
				          <tr><td>{$regimeAtual}</td></tr>";
            $html .= "    <tr><td style='background-color:#EEEEEE;'><b>INDICAÇÃO TÉCNICA PARA O PROXÍMO PERÍODO</b></td></tr>
				          <tr><td>{$indicacaoTecnica}</td></tr>";
        }
        $html .= "</table>";

		$html .= assinaturaProResponsavel($id_usuario);

        $htmlNead = '';

        $sqlOperadora = "
                SELECT
                    operadoras_planosdesaude.OPERADORAS_ID
                FROM
                    clientes inner join
                    operadoras_planosdesaude ON (clientes.convenio = operadoras_planosdesaude.PLANOSDESAUDE_ID)				 
                WHERE
                    idClientes = '{$id}'";
        $resultOperadora = mysql_fetch_array(mysql_query($sqlOperadora));

        if (!in_array($resultOperadora['OPERADORAS_ID'], [11])) {

            if ($modeloNead == '2016') {
                $htmlNead = \App\Controllers\Score::scoreNead2016Imprimir($id, $idr, "prorrogacao", $classificacao_paciente, 'print');
            } else {
                $htmlNead = Relatorios::tabelaNEAD($idr, $id, true, true);
            }
        }

		$paginas [] = $header . $html.= "</form>";

		//echo $html; die();

		$mpdf = new mPDF('pt', 'A4', 8);
		$mpdf->SetHeader('página {PAGENO} de {nbpg}');
		$ano = date("Y");
        $mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']}"." &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));
		$mpdf->WriteHTML("<html><body>");
		$flag = false;
		foreach ($paginas as $pag) {
			if ($flag)
				$mpdf->WriteHTML("<formfeed>");
			$mpdf->WriteHTML($pag);
			$flag = true;
		}
        if($htmlNead != '') {
            $mpdf->AddPage('L');
            $mpdf->WriteHTML($htmlNead);
        }
		$mpdf->WriteHTML("</body></html>");
		$mpdf->Output('ficha_prorrogacao.pdf', 'I');
		exit;
	}

//alta
}

//imprimir_alta

$p = new ImprimirProrrogacao();
$p->imprimir_prorrogacao($_GET['id'], $_GET['idr'], $_GET['empresa']);
?>