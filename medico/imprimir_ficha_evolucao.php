<?php

$id = session_id();
if(empty($id))
	session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
require __DIR__ . '/../vendor/autoload.php';

use \App\Controllers\Administracao as Administracao;
use \App\Models\Administracao\Filial;

class imprimir_ficha_evolucao{

	public function cabecalho($dados, $empresa = null, $convenio = null){

		$id = $dados;
		$condp1= "c.idClientes = '{$id}'";
		$sql2 = <<<SQL
SELECT
		UPPER(c.nome) AS paciente,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresa,
		c.`nascimento`,
		p.nome as Convenio,p.id,
		NUM_MATRICULA_CONVENIO,
		cpf
		FROM
		clientes AS c LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN
		planosdesaude as p ON (p.id = c.convenio)
		WHERE
		{$condp1}
		ORDER BY
		c.nome DESC LIMIT 1
SQL;

		$html= "";
		$result2 = mysql_query($sql2);
		$html .= "<table width=100% >";
		while($pessoa = mysql_fetch_array($result2)){
			foreach($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);
			}

            $pessoa['empresa'] = !empty($empresa) ? $empresa : $pessoa['empresa'];
            $pessoa['Convenio'] = !empty($convenio) ? $convenio : $pessoa['Convenio'];

			$html .= "<br/><tr>";
			$html .= "<td colspan='2'><label style='font-size:14px;color:#8B0000'><b></b></label></td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>PACIENTE </b></label></td>";
			$html .= "<td><label><b>SEXO </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>{$pessoa['paciente']}</td>";
			$html .= "<td>{$pessoa['sexo']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>IDADE </b></label></td>";
			$html .= "<td><label><b>UNIDADE REGIONAL </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>".join("/",array_reverse(explode("-",$pessoa['nascimento']))).' ('.$pessoa['idade']." anos)</td>";
			$html .= "<td>{$pessoa['empresa']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>";
			$html .= "<td><label><b>MATR&Iacute;CULA </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>".strtoupper($pessoa['Convenio'])."</td>";
			$html .= "<td>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
			$html .= "</tr>";
			$html.= " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";

		}
		$html .= "</table>";

		return $html;
	}

	public function detalhes_ficha_evolucao_medica($dados, $empresa_id){
		$id = $dados;
		$html = "";

		$sql = "SELECT
  	DATE_FORMAT(fev.DATA,'%d/%m/%Y %H:%i:%s') as fdata,
  	fev.ID,
  	fev.PACIENTE_ID as idpaciente,
  	fev.MOD_HOME_CARE,
  	u.nome,
  	u.tipo as utipo, 
    u.conselho_regional,
  	p.nome as paciente,
  	e.nome AS empresa_rel,
  	fev.empresa AS empresa_id,
  	pds.nome AS plano
  	FROM
  	fichamedicaevolucao as fev LEFT JOIN
  	usuarios as u ON (fev.USUARIO_ID = u.idUsuarios) INNER JOIN
  	clientes as p ON (fev.PACIENTE_ID = p.idClientes)
  	LEFT JOIN empresas AS e ON fev.empresa = e.id
  	LEFT JOIN planosdesaude AS pds ON fev.plano = pds.id
  
  	WHERE
  	fev.ID = '{$id}'";
		$r = mysql_query($sql);

		while($row = mysql_fetch_array($r)){
            $compTipo = $row['conselho_regional'];

			$usuario =ucwords(strtolower($row["nome"])) . $compTipo;
			$data = $row['fdata'];
			$paciente=$row['idpaciente'];
			//$empresa=$row['empresa_rel'];
			$convenio=$row['plano'];
			$mod_home_care=$row['MOD_HOME_CARE'];
		}

        $responseEmpresa = Filial::getEmpresaById($empresa_id);

		$html .= "<h1><a><img src='/utils/logos/{$responseEmpresa['logo']}' width='100' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> Ficha de Evolu&ccedil;&atilde;o M&eacute;dica  </h1>";
		$html .=$this->cabecalho($paciente, $responseEmpresa['nome'], $convenio);
		$html .= "<center><h3>Respons&aacute;vel: {$usuario} - Data: {$data}</h3></center>";

		$html .= "<form method='post' target='_blank'>";

		$html .= "<table width=100%>";

		$html .= "<tr  style='border:1px solid #000;' >";
		$html .="<td colspan='3' style='border:1px solid #000;' ><b>Problemas Ativos</b></td>";
		$html .="</tr>";

		$sql2="select pae.DESCRICAO as nome, pae.CID_ID,(CASE pae.STATUS
  	WHEN '1' THEN 'RESOLVIDO'
  	WHEN '2' THEN 'MELHOR'
  	WHEN '3' THEN 'EST&Aacute;VEL'
  	WHEN '4' THEN 'PIOR' END) as sta , pae.STATUS,pae.OBSERVACAO from problemaativoevolucao as pae where pae.EVOLUCAO_ID = {$id} ";
		$result2 = mysql_query($sql2);
		$cont=1;

		while($row = mysql_fetch_array($result2)){
			if($cont%2 == 0){
				$cor = '#EEEEEE';
			}else{
				$cor ='#FFFFFF' ;
			}
			if($row['STATUS'] != 0){
				$html .= "<tr bgcolor='{$cor}'>";
				$html .="<td cid={$row['cid']} prob_atv='{$row['nome']}' colspan='2' class='probativo1' style='width:70%'> <b>P{$cont} - ".mb_strtoupper($row['nome'],'UTF-8')."</b> </td>";
				$html .="<td colspan=''> <b>Estado:</b> {$row['sta']}</td>";
				$html .="</tr>";
				if( $row['STATUS'] != 1 && $row['STATUS'] != 3){
					$html .="<tr bgcolor='{$cor}'><td colspan='3'><b>Observa&ccedil;&atilde;o: </b>{$row['OBSERVACAO']}</td></tr>";
				}
				$cont++;
			}
		}

		switch ($mod_home_care){
			case 1:
				$labelHomeCare = "ASSISTÊNCIA DOMICILIAR";
				break;
			case 2:
				$labelHomeCare = "INTERNAÇÃO DOMICILIAR 6H";
				break;
			case 3:
				$labelHomeCare = "INTERNAÇÃO DOMICILIAR 12H";
				break;
			case 4:
				$labelHomeCare = "INTERNAÇÃO DOMICILIAR 24H COM RESPIRADOR";
				break;
			case 5:
				$labelHomeCare = "INTERNAÇÃO DOMICILIAR 24H SEM RESPIRADOR";
				break;
		}

		$html .= "<tr  style='border:1px solid #000;' ><td colspan='3' style='border:1px solid #000;' > <b>Modalidade Home Care</b></td></tr>";
		$html .= "<tr><td  colspan='3'><b>{$labelHomeCare}</b></td></tr>";


		$html .= "<tr  style='border:1px solid #000;' ><td colspan='3' style='border:1px solid #000;' > <b>Alergia Medicamentosa</b></td></tr>";
		$sql_alergias = "	SELECT `DESCRICAO` as medicamento
											FROM alergia_medicamentosa
											WHERE ID_PACIENTE = {$paciente}
											AND ATIVO = 's'
											GROUP BY NUMERO_TISS
											ORDER BY medicamento";
		$resultado_alergias = mysql_query($sql_alergias);

		while($row_alergias = mysql_fetch_array($resultado_alergias)){
			$html .=  "<tr><td  colspan='3'><b>{$row_alergias['medicamento']}</b></td></tr>";
		}


		$html .= "<tr  style='border:1px solid #000;' ><td colspan='3' style='border:1px solid #000;' > <b>Novos Problemas </b></td></tr>";
		$sql2="select pae.DESCRICAO as nome, pae.CID_ID, pae.STATUS,pae.OBSERVACAO from problemaativoevolucao as pae where pae.EVOLUCAO_ID = {$id} ";
		$result2 = mysql_query($sql2);
		$cont=1;
		while($row = mysql_fetch_array($result2)){
			if($cont%2 == 0){
				$cor = '#EEEEEE';
			}else{
				$cor ='#FFFFFF' ;
			}
			if($row['STATUS'] == 0){
				if($cont%2 == 0){
					$cor = '#EEEEEE';
				}else{
					$cor ='#FFFFFF' ;
				}
				$html .="<tr bgcolor='{$cor}' ><td colspan='3'><b>P{$cont}- ".mb_strtoupper($row['nome'],'UTF-8')."</b> <br/><b>Obs:</b> {$row['OBSERVACAO']}</td></tr>";
				$cont++;
			}

		}


		$sql= "select * from fichamedicaevolucao where ID = {$id}";
		$result=mysql_query($sql);
		while($row = mysql_fetch_array($result)){

			$pa_sistolica_min =$row['PA_SISTOLICA_MIN'];
			$pa_sistolica_max =$row['PA_SISTOLICA_MAX'];
			$pa_sistolica =$row['PA_SISTOLICA'];
			$pa_sistolica_sinal =$row['PA_SISTOLICA_SINAL'];
			$pa_diastolica_min = $row['PA_DIASTOLICA_MIN'];
			$pa_diastolica_max = $row['PA_DIASTOLICA_MAX'];
			$pa_diastolica  = $row['PA_DIASTOLICA'];
			$pa_diastolica_sinal = $row['PA_DIASTOLICA_SINAL'];
			$fc =$row["FC"];
			$fc_max =$row["FC_MAX"];
			$fc_min =$row["FC_MIN"];
			$fc_sinal =$row["FC_SINAL"];
			$fr =$row["FR"];
			$fr_max =$row["FR_MAX"];
			$fr_min =$row["FR_MIN"];
			$fr_sinal =$row["FR_SINAL"];
			$temperatura_max = $row["TEMPERATURA_MAX"];
			$temperatura_min = $row["TEMPERATURA_MIN"];
			$temperatura = $row["TEMPERATURA"];
            $peso_paciente = $row["PESO_PACIENTE"];
			$temperatura_sinal = $row["TEMPERATURA_SINAL"];
			$estd_geral = $row["ESTADO_GERAL"];
			$mucosa = $row['MUCOSA'];
			$mucosaobs = $row['MUCOSA_OBS'];
			$escleras = $row['ESCLERAS'];
			$esclerasobs = $row['ESCLERAS_OBS'];
			$respiratorio = $row['PADRAO_RESPIRATORIO'];
			$respiratorioobs = $row['PADRAO_RESPIRATORIO_OBS'];
			$novo_exame = $row['NOVOS_EXAMES'];
			$impressao = $row['IMPRESSAO'];
			$diagnostico= $row['PLANO_DIAGNOSTICO'];
			$terapeutico= $row['PLANO_TERAPEUTICO'];
			$dor_sn =  $row['DOR'];
			$outro_resp = $row['OUTRO_RESP'];
			$oximetria_pulso_max = $row["OXIMETRIA_PULSO_MAX"];
			$oximetria_pulso_min = $row["OXIMETRIA_PULSO_MIN"];
			$tipo_oximetria_min = $row["OXIMETRIA_PULSO_TIPO_MIN"];
			$litros_min = $row["OXIMETRIA_PULSO_LITROS_MIN"];
			$via_oximetria_min = $row["OXIMETRIA_PULSO_VIA_MIN"];
			$tipo_oximetria_max = $row["OXIMETRIA_PULSO_TIPO_MAX"];
			$litros_max = $row["OXIMETRIA_PULSO_LITROS_MAX"];
			$via_oximetria_max = $row["OXIMETRIA_PULSO_VIA_MAX"];

		}

		$html .= "<tr style='border:1px solid #000;' >";
		$html .="<td  colspan='3' style='border:1px solid #000;' ><b>Evolu&ccedil;&atilde;o dos sinais vitais (Registro da semana)</b></td>";
		$html .="</tr>";
		$html .= "<tr >";
		$html .="<td><b>PA (sist&oacute;lica) </b></td>";
		$html .="<td width=30%>min:<input type='texto' readonly class='numerico1 OBG' name='pa_sistolica_min' value='{$pa_sistolica_min}' readonly size='10' /> m&aacute;x:
  	<input type='texto' class='numerico1 OBG' name='pa_sistolica_max' value='{$pa_sistolica_max}' readonly  readonly  size='10' />";
		$html .="</td>";
		if($pa_sistolica_sinal == 1){
			$x="AMARELO";
		}

		if($pa_sistolica_sinal == 2){
			$x="VERMELHO";
		}
		$html .= "<td><b>ALERTA:</b> {$x} </td>";
		$html .= "</tr>";
		$html .= "<tr bgcolor='#EEEEEE'>";
		$html .="<td><b>PA (diast&oacute;lica) </b> </td>";
		$html .="<td> min:<input type='texto' class='numerico1 OBG' name='pa_diastolica_min' value='{$pa_diastolica_min}' readonly  size='10'  /> m&aacute;x:
  	<input type='texto' class='numerico1 OBG' name='pa_diastolica_max' value='{$pa_diastolica_max}' size='10' readonly />";
		$html .="</td>";
		$x='';
		if($pa_diastolica_sinal == 1){
			$x="AMARELO";
		}
		if($pa_diastolica_sinal == 2){
			$x="VERMELHO";
		}
		$html .= "<td><b>ALERTA:</b> {$x}</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .="<td><b>FC </b> </td>";
		$html .="<td> min:<input type='texto' class='numerico1 OBG' name='fc_min' value='{$fc_min}'  size='10' readonly /> m&aacute;x:
  	<input type='texto' class='numerico1 OBG' name='fc_max' value='{$fc_max}' readonly size='10' />";
		$html .="</td>";
		$x='';
		if($fc_sinal == 1){
			$x="AMARELO";
		}
		if($fc_sinal == 2){
			$x="VERMELHO";
		}
		$html .= "<td><b>ALERTA:</b> {$x}</td>";
		$html .= "</tr>";
		$html .= "<tr bgcolor='#EEEEEE'>";
		$html .="<td><b>FR </b> </td>";
		$html .="<td>min:<input type='texto' class='numerico1 OBG' name='fr_min' value='{$fr_min}' size='10' readonly /> m&aacute;x:
  	<input type='texto' class='numerico1 OBG' name='fr_max' value='{$fr_max}' size='10'readonly />";
		$html .="</td>";
		$x='';
		if($fr_sinal == 1){
			$x="AMARELO";
		}
		if($fr_sinal == 2){
			$x="VERMELHO";
		}
		$html .= "<td><b>ALERTA:</b> {$x}</td>";
		$html .= "</tr>";
		$html .= "<tr >";
		$html .="<td width=20% ><b>Temperatura: </b></td>";
		$html .="<td width=30% >min:<input type='texto' class='numerico1 OBG' name='temperatura_min' size='10' value='{$temperatura_min}' readonly  /> m&aacute;x:
  	<input type='texto' class='numerico1 OBG' name='temperatura_max' size='10' value='{$temperatura_max}' readonly  />";
		$html .="</td>";
		$x='';
		if($ptemperatura_sinal == 1){
			$x="AMARELO";
		}
		if($temperatura_sinal == 2){
			$x="VERMELHO";
		}
		$html .= "<td ><b>ALERTA:</b> {$x}</td>";
		$html .= "</tr>";

		//Oximetria
		$html .= "<tr><td width=20% ><b>Oximetria de Pulso: </b></td>
  	<td width=25% colspan='3'>
  	min:
  	<input type='texto' class='numerico1' name='oximetria_pulso_min' size='4' value='{$oximetria_pulso_min}' readonly size='4' />";
		$tipo_min = '';
		if($tipo_oximetria_min == 1){
			$tipo_min = 'Ar Ambiente';
		}
		if($tipo_oximetria_min == 2){
			$tipo_min = 'O2 Suplementar';
		}
		if($tipo_oximetria_min == 3){
			$tipo_min = 'Saturação de Oxigênio';
		}
		$html .= "<b> Tipo</b><input type='texto' value='{$tipo_min}' readonly/>";

		if($tipo_oximetria_min == 2){
			$via_min = '';
			if ($via_oximetria_min == 1) {
				$via_min = 'Cateter Nasal';
			}
			if ($via_oximetria_min == 2) {
				$via_min = 'Cateter em Traqueostomia';
			}
			if ($via_oximetria_min == 3) {
				$via_min = 'Máscara de Venturi';
			}
			if ($via_oximetria_min == 4) {
				$via_min = 'Ventilação Mecânica';
			}
			$html .= "<b> Dosagem</b><input type='texto' size='3' value='{$litros_min}' readonly/>l/min";
			$html .= "<b> Via</b><input type='texto' value='{$via_min}' readonly/>";
		}

		$html .= "</td></tr>";

		$html .= "<tr><td width=20% ></td>
  	<td width=25% colspan='3'>
  	max:
  	<input type='texto' class='numerico1' name='oximetria_pulso_min' size='4' value='{$oximetria_pulso_max}' readonly size='4' />";
		$tipo_max = '';
		if($tipo_oximetria_max == 1){
			$tipo_max = 'Ar Ambiente';
		}
		if($tipo_oximetria_max == 2){
			$tipo_max = 'O2 Suplementar';
		}
		if($tipo_oximetria_max == 3){
			$tipo_min = 'Saturação de Oxigênio';
		}
		$html .= "<b> Tipo</b><input type='texto' value='{$tipo_max}' readonly/>";

		if($tipo_oximetria_max == 2){
			$via_max = '';
			if ($via_oximetria_max == 1) {
				$via_max = 'Cateter Nasal';
			}
			if ($via_oximetria_max == 2) {
				$via_max = 'Cateter em Traqueostomia';
			}
			if ($via_oximetria_max == 3) {
				$via_max = 'Máscara de Venturi';
			}
			if ($via_oximetria_max == 4) {
				$via_max = 'Ventilação Mecânica';
			}
			$html .= "<b> Dosagem</b><input type='texto' size='3' value='{$litros_max}' readonly/>l/min";
			$html .= "<b> Via</b><input type='texto' value='{$via_max}' readonly/>";
		}

		$html .= "</td></tr>";

		//Exame Físico
		$html .= "<tr ><td colspan='3' style='border:1px solid #000;' ><b>Exame F&iacute;sico</b></td></tr>";
		$x='';
		$y1='';
		if($estd_geral == 1){
			$x="BOM";
		}
		if($estd_geral == 2){
			$x="REGULAR";
		}
		if($estd_geral == 3){
			$x="RUIM";
		}

		$html .= "<tr>";
		$html .="<td><b>Estado Geral:</b></td><td colspan='2'>{$x}</td>";
		$html .="</tr>";
		$x='';
		$y1='';
		if ($mucosa =='n'){
			$x= 'DESCORADAS';
			$y1= $mucosaobs;
		}
		if ($mucosa =='s'){
			$x= 'Coradas';
			$y1= $mucosaobs;
		}
		$html .= "<tr bgcolor='#EEEEEE'>";
		$html .= "<td><b>Mucosa:</b>  </td><td colspan='2'>{$x}  {$y1}</td>";
		$html .= "</tr>";
		$x='';
		if ($escleras =='n'){
			$x= 'Ict&eacute;ricas';
			$y1= $esclerasobs;
		}
		if ($escleras =='s'){
			$x= 'Anict&eacute;ricas';
			$y1= $esclerasobs;
		}
		$html .= "<tr>";
		$html .= "<td><b>Escleras:</b>  </td><td colspan='2'>{$x}  {$y1}</td>";
		$html .= "</tr>";
		$x='';
		$y1='';
		if ($respiratorio =='n'){
			$x= 'Dispn&eacute;ico';
			$y1= $respiratorioobs;
		}
		if ($respiratorio =='s'){
			$x= 'Eupn&eacute;ico';
			$y1= $respiratorioobs;
		}
		if ($respiratorio =='1'){
			$x= 'Outro';
			$y1= $outro_resp;
		}
		$html .= "<tr bgcolor='#EEEEEE'><td><b>Padr&atilde;o Respirat&oacute;:</b>  </td><td colspan='2'>{$x}  {$y1}</td></tr>";
		if ($respiratorio =='n') {
			$html .= "<tr bgcolor='#EEEEEE'>";
			$html .= "<td colspan='3'><b>FR:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='10' class='numerico1 OBG' readonly name='fr' value='{$fr}' {$acesso} />irpm</td>";
			$html .= "</tr>";
		}
		$html .= "<tr>";
		$html .= "<td colspan='3'><b>PA (sist&oacute;lica):</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' class='numerico1 OBG' size='10' name='pa_sistolica' value='{$pa_sistolica}'  readonly {$acesso} />mmhg</td>";
		$html .= "</tr>";
		$html .= "<tr bgcolor='#EEEEEE'>";
		$html .= "<td colspan='3'><b>PA (diast&oacute;lica):</b>&nbsp;&nbsp;&nbsp;<input type='texto' class='numerico1 OBG' size='10' name='pa_diastolica' value='{$pa_diastolica}' readonly {$acesso} />mmhg</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td colspan='3'><b>FC: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='10' class='numerico1 OBG' name='fc' readonly value='{$fc}' {$acesso} />bpm</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td colspan='3'><b>Temperatura:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='10' class='numerico1 OBG' name='temperatura' value='{$temperatura}' readonly {$acesso} />&deg;C</td>";
		$html .= "</tr>";
        $html .= "<td colspan='3'><b>Peso:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='10' class='numerico1 OBG' name='peso_paciente' value='{$peso_paciente}' readonly {$acesso} />KG</td>";
        $html .= "</tr>";
		if($dor_sn == 's'){
			$dor_sn = 'Sim';
		}
		if($dor_sn == 'n'){
			$dor_sn = 'N&atilde;o';
		}

		$html .="<tr bgcolor='#EEEEEE'><td colspan='3'  ><b>DOR:</b> {$dor_sn}</td></tr>";

		$html .= "<tr><td colspan='3' ><table id='dor' style='width:100%;'>";
		$sql = "select * from dorfichaevolucao where FICHAMEDICAEVOLUCAO_ID = {$id} ";
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result)){
			$html .= "<tr><td> <b>Local:</b> {$row['DESCRICAO']}&nbsp;&nbsp; <b>Escala visual:</b> {$row['ESCALA']}&nbsp;&nbsp;<b>Padr&atilde;o:</b>{$row['PADRAO']}&nbsp;&nbsp; </td></tr>";

		}

		$html .= "</table></td></tr>";

		$html .= "<tr style='border:1px solid #000;'  ><td colspan='3' style='border:1px solid #000;'  ><b>Cateteres e Sondas </b> </td></tr>";
		$sql = "select DESCRICAO from catetersondaevolucao where FICHA_EVOLUCAO_ID = {$id}";
		$result= mysql_query($sql);
		while ($row= mysql_fetch_array($result)){

			$html .= "<tr><td colspan='3' ><b>- ".mb_strtoupper($row['DESCRICAO'],'UTF-8')."</b></td></tr>";
		}

		$html .= "<tr style='border:1px solid #000;'  ><td colspan='3' style='border:1px solid #000;' ><b>Exame segmentar</b></td></tr>";
		$sql="select exv.*,(CASE exv.AVALIACAO
  	WHEN '1' THEN 'Normal'
  	WHEN '2' THEN 'Alterado'
  	WHEN '3' THEN 'N&atilde;o examinado' END) as av,ex.DESCRICAO from examesegmentarevolucao as exv LEFT JOIN examesegmentar as ex ON (exv.EXAME_SEGMENTAR_ID = ex.ID) where exv.FICHA_EVOLUCAO_ID = {$id}";
		$result=mysql_query($sql);
		$cont1=1;
		while($row = mysql_fetch_array($result)){
			if($cont1++%2 == 0){
				$cor ='#EEEEEE';
			}else{
				$cor ='#FFFFFF' ;
			}

			$html .= "<tr bgcolor='{$cor}'>";
			$html .= "<td idexame={$row['ID']} class='exame1' colspan='2'> <b>".mb_strtoupper($row['DESCRICAO'],'UTF-8')."</b></td>";
			$html .= "<td >{$row['av']}</td>";
			$html .= "</tr>";
			if($row['AVALIACAO'] == 2 || $row['AVALIACAO'] == 3){
				$html .="<tr bgcolor='{$cor}'><td colspan='3'><b>OBS: </b>{$row['OBSERVACAO']}</td></tr>";
			}
		}
		$html .= "<tr style='border:1px solid #000;' > <td colspan='3' style='border:1px solid #000;' ><b>Exames Complementares Novos</b></td></tr>";
		$html .= "<tr><td colspan='3' >{$novo_exame} </td></tr>";

		$html .= "<tr style='border:1px solid #000;'  ><td colspan='3' style='border:1px solid #000;' ><b>Impress&atilde;o</b></td></tr>";
		$html .= "<tr ><td colspan='3' >{$impressao} </td></tr>";

		$html .= "<tr style='border:1px solid #000;'  ><td colspan='3' style='border:1px solid #000;'  ><b>Plano Diagn&otilde;stico</b></td></tr>";
		$html .= "<tr><td colspan='3' >{$diagnostico} </td></tr>";

		$html .= "<tr  ><td colspan='3' style='border:1px solid #000;' ><b>Plano Teraup&ecirc;utico</b></td></tr>";
		$html .= "<tr><td colspan='3' >{$terapeutico}</td></tr>";

		$html .= "</table>";
		$paginas []= $header.$html.= "</form>";

		//print_r($paginas);

		$mpdf=new mPDF('pt','A4',8);
		$mpdf->SetHeader('página {PAGENO} de {nbpg}');
		$ano = date("Y");
        $mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']}"." &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));
		$mpdf->WriteHTML("<html><body>");
		$flag = false;
		foreach($paginas as $pag){
			if($flag) $mpdf->WriteHTML("<formfeed>");
			$mpdf->WriteHTML($pag);
			$flag = true;
		}
		$mpdf->WriteHTML("</body></html>");
		$mpdf->Output('ficha_evolucao.pdf','I');
		exit;

	}

}
$p = new imprimir_ficha_evolucao();
$p->detalhes_ficha_evolucao_medica($_GET['id'], $_GET['empresa']);
