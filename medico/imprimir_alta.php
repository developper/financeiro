<?php

$id = session_id();
if(empty($id))
	session_start();
include_once('../validar.php');
include_once('../utils/codigos.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
require __DIR__ . '/../vendor/autoload.php';

use \App\Controllers\Administracao as Administracao;
use \App\Models\Administracao\Filial;

class imprimir_alta{

	public function alta($id, $idr, $empresa){

        $responseEmpresa = Filial::getEmpresaById($empresa);

		//Título
		$html .= "<a><img src='../utils/logos/{$responseEmpresa['logo']}' width='100' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>";

		$html .= "<br><h1><center>". htmlentities("Relatório Padrão") ." de Alta do Home Care e Encaminhamento para Atendimentos Ambulatoriais</center></h1>";

		//Cabeçalho

		$sql = "
                SELECT
                    r.*,
                    DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
                    DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
                    DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
                    u.nome as user,
                    u.tipo as utipo, 
                    u.conselho_regional,
                    e.nome AS empresa,
                    p.nome AS plano
                FROM
                    relatorio_alta_med as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios)
                    LEFT JOIN empresas AS e ON r.empresa = e.id
                    LEFT JOIN planosdesaude AS p ON r.plano = p.id          
                WHERE
                    r.ID = '{$idr}'
                ORDER BY
                    r.DATA DESC";

		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {
            $compTipo = $row['conselho_regional'];

			$inicio = $row['inicio'];
			$fim = $row['fim'];
			$empresa = $row['empresa'];
			$convenio = $row['plano'];
			$user = $row['user'] . $compTipo;
		}

        $html .=$this->cabecalho($id, $responseEmpresa['nome'], $convenio);

		$data = date("d/m/Y G:i:s");

		$html .= "<h3>Data da ".htmlentities("impressão").": {$data}</h3>";
		$html .="<p><b>".htmlentities("Relatório de Alta Médica  feito por: {$user}.")."</b></p>";
		$html .= "<p><b>".htmlentities("Data da Alta Médica: {$fim}.")."</b></p>";
		$html .= "<form method='post' target='_blank'>";

		$sql_problemas_ativos ="SELECT
                                    pae.DESCRICAO as nome,
                                    pae.CID_ID as cid,
                                    pae.OBSERVACAO as sta,                                    
                                    pae.OBSERVACAO
                                FROM
                                    problemas_relatorio_prorrogacao_med as pae
                                WHERE  
                                    pae.RELATORIO_ALTA_MED_ID = {$idr}";

		$result_problemas_ativos = mysql_query($sql_problemas_ativos);

		$html .= "<table width=100%>";
		$html .= "<tr bgcolor='#D3D3D3'><td colspan='4'><b>RESUMO DA ". htmlentities("HISTÓRIA") ."</b></td></tr>";

		$html .= "<tr><td><table style='width:100%;'>";
		$html .= "<tr bgcolor='#D3D3D3'><td colspan='4' style='width:100%'><b>PROBLEMAS ENCONTRADOS NA ".htmlentities("ÚLTIMA VISITA MÉDICA")." </b></td></tr>";
		if(mysql_num_rows($result_problemas_ativos) > 0){
			while($row = mysql_fetch_array($result_problemas_ativos)){
				$html .= "<tr><td><b>{$row['cid']} - {$row['nome']}</b> </td><td colspan='3'><b>Estado: </b>{$row['sta']}</td></tr>";
			}
		}else{
			$html .= "<tr><td colspan='4'>".htmlentities("Não")." foram registrados problemas na ".htmlentities("última visita médica")."</td></tr>";
		}
		$html .= "</table></td></tr>";

		//Intercorrências
		$sql_intercorrencias = "SELECT * FROM `intercorrenciasfichaevolucao` WHERE `RELATORIOALTA_ID` = {$idr}";

		$resultado_intercorrencias = mysql_query($sql_intercorrencias);
		$cont_intercorrencias = 1;

		$html .= "<tr><td><table style='width:100%;'>";
		$html .= "<tr bgcolor='#D3D3D3'><td colspan='4' style='width:100%'><b>".htmlentities("INTERCORRÊNCIAS DA ÚLTIMA VISITA MÉDICA")."</b></td></tr>";
		if(mysql_num_rows($resultado_intercorrencias) > 0){
			while ($row = mysql_fetch_array($resultado_intercorrencias)) {
				$html .= "<tr><td ><b>".htmlentities("Descrição").": </b>{$row['DESCRICAO']}</td><td colspan='3'></td></tr>";
				$cont_intercorrencias++;
			}
		}else{
			$html .= "<tr><td>".htmlentities("Não")." foram registradas ".htmlentities("intercorrências na última visita médica")."</td><td colspan='3'></td></tr>";
		}
		$html .= "</table></td></tr>";


		$sqlAntibioticos = "SELECT
                                    MEDICAMENTO AS medicamento,
                                    CATALOGO_ID AS CATALOGO_ID,
                                    NUMERO_TISS AS NUMERO_TISS
                                FROM
                                    relatorio_alta_med_antibiotico
                                WHERE
                                    relatorio_alta_id = $idr";
		$resultadoAntibioticos = mysql_query($sqlAntibioticos);
		$html .= "<tr><td><table style='width:100%;'>";
		$html .= "<tr bgcolor='#D3D3D3'><td colspan='4' style='width:100%'><b>".htmlentities("ANTIBIÓTICOS").":</b></td></tr>";

		if(mysql_num_rows($resultadoAntibioticos) > 0){
			while ($row = mysql_fetch_array($resultadoAntibioticos)){
				$html .= "<tr><td> {$row['medicamento']}</td><td colspan='3'></td></tr>";
			}
		}else{
			$html .= "<tr><td>".htmlentities("Não")." ".htmlentities("há antibióticos prescritos")."</td><td colspan='3'></td></tr>";
		}
		$html .= "</table>";

		$html .= "</table>";


		$html .= "<table width=100%>";
		$html .= "<tr  style='border:1px solid #000;' ><td colspan='3' style='background-color:#CDC9C9;'> <b>EXAMES ".htmlentities("FÍSICOS DESDE A ÚLTIMA VISITA MÉDICA")."</b></td></tr>";

		//Exame Físico

		$sql_exame_fisico = "SELECT
			USUARIO_ID,
			ESTADO_GERAL,
			MUCOSA, MUCOSA_OBS,
			ESCLERAS, ESCLERAS_OBS,
			PADRAO_RESPIRATORIO, PADRAO_RESPIRATORIO_OBS, OUTRO_RESP, 
			OXIMETRIA_PULSO, OXIMETRIA_PULSO_TIPO, OXIMETRIA_PULSO_LITROS, OXIMETRIA_PULSO_VIA,
			PA_SISTOLICA,
			PA_DIASTOLICA,
			FC,
			FR,
			TEMPERATURA,
			DOR 
		FROM 
			`relatorio_alta_med` WHERE id = {$idr}";
		$resultado_exame_fisico = mysql_query($sql_exame_fisico);

		while($row = mysql_fetch_array($resultado_exame_fisico)){
			$medico = $row['USUARIO_ID'];

			//Estado Geral
			switch ($row['ESTADO_GERAL']) {
				case 1:
					$estado_geral = "Bom";
					break;
				case 2:
					$estado_geral = "Regular";
					break;
				case 3:
					$estado_geral = "Ruim";
					break;
				default:
					$estado_geral = "-";
					break;
			}

			$html .=  "<tr><td><b>Estado Geral:</b>  </td><td>{$estado_geral}</td></tr>";
			//Mucosa
			$mucosa_observacao = "-";
			if ($row['MUCOSA'] == 's') {
				$mucosa = "Coradas";
			}else if ($row['MUCOSA'] == 'n') {
				$mucosa = "Descoradas";
			}

			$mucosa_observacao = $row['MUCOSA_OBS'];

			$html .=  "<tr><td><b>Mucosa:</b>  </td><td>{$mucosa}</td><td>{$mucosa_observacao}</td></tr>";

			//Escleras
			$escleras_observacao = "-";
			if ($row['ESCLERAS'] == 's') {
				$escleras = "Anict&eacute;ricas";
			}else if ($row['ESCLERAS'] == 'n') {
				$escleras = "Ict&eacute;ricas";
			}

			$escleras_observacao = $row['ESCLERAS_OBS'];

			$html .=  "<tr><td><b>Escleras:</b>  </td><td>{$escleras}</td><td>{$escleras_observacao}</td></tr>";

			//Padrão Respiratório
			$padrao_respiratorio_observacao = "-";
			if ($row['PADRAO_RESPIRATORIO'] == 's') {
				$padrao_respiratorio = "Eupn&eacute;ico";
				$padrao_respiratorio_observacao = $row['PADRAO_RESPIRATORIO_OBS'];
			}else if ($row['PADRAO_RESPIRATORIO'] == 'n') {
				$padrao_respiratorio = "Taqui/Dispn&eacute;ico";
			}else if ($row['PADRAO_RESPIRATORIO'] == '1') {
				$padrao_respiratorio = "Outro";
				$padrao_respiratorio_observacao = $row['OUTRO_RESP'];
			}

			$html .=  "<tr><td><b>" . htmlentities("Padrão Respiratório") . ":</b>  </td><td>{$padrao_respiratorio}</td><td>{$padrao_respiratorio_observacao}</td></tr>";

			//Oximetria de Pulso
			$via_oximetria_pulso = "";
			$observacao_oximetria = "";
			$oximetria_pulso = $row['OXIMETRIA_PULSO'];
			$tipo_oximetria = "";

			if ($row['OXIMETRIA_PULSO_TIPO'] == '1') {
				$tipo_oximetria = "Ar Ambiente";

			}else if ($row['OXIMETRIA_PULSO_TIPO'] == '2') {
				$tipo_oximetria = "O2 Suplementar";

				switch ($row['OXIMETRIA_PULSO_VIA']) {
					case 1:
						$via_oximetria_pulso = "Cateter Nasal";
						break;
					case 2:
						$via_oximetria_pulso = "Cateter em Traqueostomia";
						break;
					case 3:
						$via_oximetria_pulso = "Máscara de Venturi";
						break;
					case 4:
						$via_oximetria_pulso = "Ventilação Mecânica";
						break;
					default:
						$via_oximetria_pulso = "-";
						break;
				}

				$observacao_oximetria = "Via: {$via_oximetria_pulso} - Dosagem: {$row['OXIMETRIA_PULSO_LITROS']}";

			}

			$html .=  "<tr><td><b>Oximetria de Pulso:</b>  </td><td>{$tipo_oximetria} {$oximetria_pulso}</td><td>{$observacao_oximetria}</td></tr>";

			$html .=  "<tr><td><b>Pressão Arterial Sist&oacute;lica:</b>  </td><td>{$row['PA_SISTOLICA']} mmhg</td></tr>";

			$html .=  "<tr><td><b>Pressão Arterial Diast&oacute;lica:</b>  </td><td>{$row['PA_DIASTOLICA']} mmhg</td></tr>";

			$html .=  "<tr><td><b>Frequência Cardíaca:</b>  </td><td>{$row['FC']} bpm</td></tr>";

			$html .=  "<tr><td><b>Frequência Respiratória:</b>  </td><td>{$row['FR']} irpm</td></tr>";

			$html .=  "<tr><td><b>Temperatura:</b>  </td><td>{$row['TEMPERATURA']} °C</td></tr>";

			//Dor
			if ($row['DOR'] == 's') {
				$dor = "Sim";
			}else if ($row['DOR'] == 'n') {
				$dor = "N&atilde;o";
			}


			$html .=  "<tr><td><b>Dor:</b></td><td>{$dor}</td>";
			$sql_dor = "SELECT * FROM dorfichaevolucao WHERE FICHAMEDICAEVOLUCAO_ID = {$ultima_evolucao} ";
			$resultado_dor = mysql_query($sql_dor);
			$html .= "<td>";

			while($row = mysql_fetch_array($resultado_dor)){
				$html .= "<br><b>Local:</b> {$row['DESCRICAO']}&nbsp;&nbsp; <b>Escala visual:</b> {$row['ESCALA']}&nbsp;&nbsp; <b>Padr&atilde;o:</b> {$row['PADRAO']}&nbsp;&nbsp;";
			}

			$html .= "</td>";
			$html .= "</tr>";

		}
		$html .= "</table>";

		$html .= "<table width=100%>";
		$html .= "<tr  style='border:1px solid #000;' ><td colspan='3' style='background-color:#CDC9C9;' > <b>JUSTIFICATIVA E " . htmlentities("RECOMENDAÇÃO") . "</b></td></tr>";

		$html .= "</table>";

		$html .= "<h3><b>Paciente recebe alta da assistência domiciliar e necessita de continuidade em regime ambulatorial dos seguintes cuidados:</b></h3>";
		//Cuidados especiais com frequência e justificativa

		$html .= "<table class='mytable' style='width:100%;'>";
		$sql = "
				SELECT
					p.*,
					f.frequencia as frequencia
				FROM
					profissionais_relatorio_prorrogacao_med as p INNER JOIN
					frequencia as f ON (p.FREQUENCIA_ID = f.id)
				WHERE
					RELATORIO_ALTA_MED_ID = {$idr} ";
		$flag_prof = TRUE;
		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {
			if ($flag_prof) {
				$html .= "<tr><td width='35%'> <b>Profissional</b></td> <td><b>".htmlentities("Sugestão de Frequência")."</b></td> <td><b>Justificativa</b></td></tr>";
				$flag_prof = FALSE;
			}
			$html .= "<tr><td width='35%'> {$row['DESCRICAO']}</td><td>{$row['frequencia']}</td><td>{$row['JUSTIFICATIVA']}</td></tr>";
		}
		$html .= "</table>";


		$html .= assinaturaProResponsavel($medico);

		$paginas []= $header.$html.= "</form>";

		$mpdf=new mPDF('pt','A4',8);
		$mpdf->SetHeader('página {PAGENO} de {nbpg}');
		$ano = date("Y");
        $mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']}"." &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));

        $mpdf->WriteHTML("<html><body>");
		$flag = false;
		foreach($paginas as $pag){
			if($flag) $mpdf->WriteHTML("<formfeed>");
			$mpdf->WriteHTML($pag);
			$flag = true;
		}
		$mpdf->WriteHTML("</body></html>");
		$mpdf->Output('ficha_alta.pdf','I');
		exit;
	}//alta

	private function cabecalho($id, $empresa = null, $convenio = null){

		$condp1= "c.idClientes = '{$id}'";
		$sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresa,
		c.`nascimento`,
		p.nome as Convenio,p.id,
		NUM_MATRICULA_CONVENIO,
		cpf
		FROM
		clientes AS c LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN
		planosdesaude as p ON (p.id = c.convenio)
		WHERE
		{$condp1}
		ORDER BY
		c.nome DESC LIMIT 1";

		$result2 = mysql_query($sql2);
		$html .= "<table width=100% >";
		while($pessoa = mysql_fetch_array($result2)){
			foreach($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);
			}

            $pessoa['empresa'] = !empty($empresa) ? $empresa : $pessoa['empresa'];
            $pessoa['Convenio'] = !empty($convenio) ? $convenio : $pessoa['Convenio'];

			$html .= "<br/><tr>";
			$html .= "<td colspan='2'><label style='font-size:14px;color:#8B0000'><b></b></label></td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>PACIENTE </b></label></td>";
			$html .= "<td><label><b>SEXO </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>{$pessoa['paciente']}</td>";
			$html .= "<td>{$pessoa['sexo']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>IDADE </b></label></td>";
			$html .= "<td><label><b>UNIDADE REGIONAL </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>".join("/",array_reverse(explode("-",$pessoa['nascimento']))).' ('.$pessoa['idade']." anos)</td>";
			$html .= "<td>{$pessoa['empresa']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>";
			$html .= "<td><label><b>MATR&Iacute;CULA </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>".strtoupper($pessoa['Convenio'])."</td>";
			$html .= "<td>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
			$html .= "</tr>";
			$html.= " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";

		}
		$html .= "</table>";

		return $html;
	}

}//imprimir_alta

$p = new imprimir_alta();
$p->alta($_GET['id'], $_GET['idr'], $_GET['empresa']);