<?php
$id = session_id();
if(empty($id))
	session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');
require __DIR__ . '/../vendor/autoload.php';

use \App\Controllers\Administracao as Administracao;
use \App\Models\Administracao\Filial;

Class ImprimirIntercorrencia
{
	public function pdfIntercorrencia($idPaciente,$idEncaminhamento, $empresa){
		$html.="<form>";

        $responseEmpresa = Filial::getEmpresaById($empresa);

		$html .= "<h><a><img src='../utils/logos/{$responseEmpresa['logo']}' width='100' >"
			. "<b>Relat&oacute;rio Intercorrência Médica.</b>"
			. "</h>";

        $sql = "SELECT 
                  empresas.nome AS empresa, 
                  planosdesaude.nome AS plano 
                FROM 
                  intercorrencia_med
                  LEFT JOIN empresas ON intercorrencia_med.empresa = empresas.id
                  LEFT JOIN planosdesaude ON intercorrencia_med.plano = planosdesaude.id
                WHERE 
                  intercorrencia_med.id = '{$idEncaminhamento}'";
        $rs = mysql_query($sql);
        $rowRel = mysql_fetch_array($rs);

		$html.= $this->cabecalho($idPaciente, $responseEmpresa['nome'], $rowRel['plano']);
		$html.=  $this->detalhesIntercorrencia($idEncaminhamento);
		$paginas []= $header.$html.= "</form>";


		//print_r($paginas);
		//print_r($paginas);

		$mpdf=new mPDF('pt','A4',9);
		$stylesheet = file_get_contents('../utils/relatorio.css');
		$mpdf->WriteHTML($stylesheet, 1);
		$mpdf->SetHeader('página {PAGENO} de {nbpg}');
		$ano = date("Y");
        $mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']}"." &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));
		$mpdf->WriteHTML("<html><body>");
		$flag = false;
		foreach($paginas as $pag){
			if($flag) $mpdf->WriteHTML("<formfeed>");
			$mpdf->WriteHTML($pag);
			$flag = true;
		}
		$mpdf->WriteHTML("</body></html>");
		$mpdf->Output("encaminhamento_".$idEncaminhamento.".pdf",'I');
		exit;

	}
	public function cabecalho($idPaciente, $empresa = null, $convenio = null)
	{

		$id = $idPaciente;
		$condp1= "c.idClientes = '{$id}'";
		$sql2 = <<<SQL
                SELECT
		UPPER(c.nome) AS paciente,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresa,
		c.`nascimento`,
		UPPER(p.nome) as Convenio,p.id,
		NUM_MATRICULA_CONVENIO,
		cpf
		FROM
		clientes AS c LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN
		planosdesaude as p ON (p.id = c.convenio)
		WHERE
		{$condp1}
		ORDER BY
		c.nome DESC LIMIT 1
SQL;

		$html= "";
		$result2 = mysql_query($sql2);
		$html .= "<table width=100% >";
		while($pessoa = mysql_fetch_array($result2)){
			foreach($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);
			}

            $pessoa['empresa'] = !empty($empresa) ? $empresa : $pessoa['empresa'];
            $pessoa['Convenio'] = !empty($convenio) ? $convenio : $pessoa['Convenio'];

			$html .= "<br/><tr>";
			$html .= "<td colspan='2'><label style='font-size:14px;color:#8B0000'><b></b></label></td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>PACIENTE </b></label></td>";
			$html .= "<td><label><b>SEXO </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>{$pessoa['paciente']}</td>";
			$html .= "<td>{$pessoa['sexo']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>IDADE </b></label></td>";
			$html .= "<td><label><b>UNIDADE REGIONAL </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>".join("/",array_reverse(explode("-",$pessoa['nascimento']))).' ('.$pessoa['idade']." anos)</td>";
			$html .= "<td>{$pessoa['empresa']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>";
			$html .= "<td><label><b>MATR&Iacute;CULA </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>".$pessoa['Convenio']."</td>";
			$html .= "<td>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
			$html .= "</tr>";
			$html.= " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";

		}
		$html .= "</table>";

		return $html;
	}

	public function detalhesIntercorrencia($idEncaminhamento)
	{   $idr = anti_injection($idEncaminhamento,'numerico');
		$html .= "<div>";

		$sql = "SELECT
                    enc_ur_emer.*,
                    usuarios.nome,
                    usuarios.tipo as utipo, 
                    usuarios.conselho_regional,
                    DATE_FORMAT(enc_ur_emer.data_encaminhamento,'%d/%m/%y') as encaminhado,
                    DATE_FORMAT(enc_ur_emer.`data`,'%d/%m/%Y %H:%i:%s') as feito_em
                    FROM
                    intercorrencia_med as enc_ur_emer

                    INNER JOIN usuarios ON enc_ur_emer.usuario_id = usuarios.idUsuarios
                    WHERE
                        enc_ur_emer.id = '{$idr}'
                    ";

		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {
            $compTipo = $row['conselho_regional'];

			$user = $row['nome'] . $compTipo;
			$data = $row['encaminhado'];
			$tipo_ambulancia = $row['tipo_ambulancia'];
			$vaga_uti = $row['vaga_uti'];
			$hora = $row['hora'];
			$hospital = $row['hospital'];
		}
		if($tipo_ambulancia == 1){
			$ambulancia ="AMBULÂNCIA BÁSICA";
		}else if($tipo_ambulancia == 2){
			$ambulancia = "AMBULÂNCIA AVANÇADA - UTI MÓVEL. ";

		}else if($tipo_ambulancia == 3){
			$ambulancia = "VEÍCULO DE INTERVENÇÃO RÁPIDA";
		}
		$sql_problemas = "SELECT
                            intercorrencia_med_problema.descricao,
                            intercorrencia_med_problema.observacao
                            FROM
                            intercorrencia_med_problema
                            WHERE
                            intercorrencia_med_problema.intercorrencia_med_id={$idr}
                            ";

		$html .= "<p><b>Data: {$data} {$hora}</p>";
		$html .= "<p>PROFISSIONAL RESPONS&Aacute;VEL: <b>{$user}</b></p>";

		$html .= "<table class='mytable' style='width:95%;'>";
		$html .= "<tr>";
		$html .= "<td colspan='3' style='background-color:#EEEEEE;' ><b>PROBLEMAS ATIVOS</b></td>";
		$html .= "</tr>";
		$result_problemas = mysql_query($sql_problemas);
		while ($row = mysql_fetch_array($result_problemas)) {

			$descricao = $row['descricao'];
			$observacao = $row['observacao'];
			$html .= "<tr>"
				. "<td width='30%'>"
				.htmlentities($descricao)
				. "</td>"
				. "<td colspan='2'>"
				.htmlentities($observacao)
				. "</td>"
				. "</tr>";
		}

		$html .= "</table>";

		$html .= "<table class='mytable'  style='table-layout:fixed; width:95%'>";
		$sql_urgencia = "SELECT
                            intercorrencia_med_quadro_clinico.descricao
                            FROM
                            intercorrencia_med_quadro_clinico
                            WHERE
                            intercorrencia_med_quadro_clinico.intercorrencia_med_id = {$idr}";
		$result_urgencia = mysql_query($sql_urgencia);

		$html .= "<tr style='background-color:#EEEEEE;'>";
		$html .= "<td colspan='3'>"
						. "<b>SITUA&Ccedil;&Atilde;O DO ENCAMINHAMENTO</b>"
				. "</td>"
				. "</tr>";

//		$html .=  htmlentities('O paciente está sendo atendido em caráter
//                          de urgência no domicílio por equipe em Ambulância '.$ambulancia.'
//                              e será encaminado para este hospital dando entrada
//                              pela emergência por quadro clínico sugestivo de:')."</td></tr>";

		while ($row = mysql_fetch_array($result_urgencia)) {
			$descricao = $row['descricao'];
			$html .= "<tr><td colspan='3' class='wrapword'>".htmlentities($descricao)."</td></tr>";
		}
		$html .= "</table>";

		//Mostra EXAMES
		$sqlExame ="SELECT
                    intercorrencia_med_exame.descricao
                    FROM
                    intercorrencia_med_exame
                    WHERE
                    intercorrencia_med_exame.intercorrencia_med_id={$idr}";
		$resultExame=  mysql_query( $sqlExame);
		$html .= "<table class='mytable'  style='table-layout:fixed; width:95%'>";
		$html .= "<tr style='background-color:#EEEEEE;'>";
		$html .= "<td colspan='4'><b>NECESSIDADE DE EXAMES:</b></td>";
		$html .= "</tr>";
		while ($row = mysql_fetch_array($resultExame)) {
			$descricao = $row['descricao'];
			$html .= "<tr><td colspan='3' class='wrapword'>".htmlentities($descricao)."</td></tr>";
		}
		$html .= "</table>";

		//Mostra ENCAMINHAMENTO
		$html .= "<table class='mytable'  style='table-layout:fixed; width:95%'>";
		$sql_encaminhamento ="SELECT
                intercorrencia_med_procedimento.descricao
                FROM
                intercorrencia_med_procedimento
                WHERE
                intercorrencia_med_procedimento.intercorrencia_med_id={$idr}";
		$result_encaminhamento = mysql_query($sql_encaminhamento);

		$html .= "<tr style='background-color:#EEEEEE;'>";
		$html .= "<td colspan='3' class='wrapword'><b> CONDUTA:</b></td></tr>";

		while ($row = mysql_fetch_array($result_encaminhamento)) {
			$descricao = $row['descricao'];
			$html .= "<tr><td colspan='3'>".htmlentities($descricao)."</td></tr>";
		}
		$html .= "</table>";
		$html .= "<table style='width:95%;'>
		<tr style='background-color:#EEEEEE;'> <td><b>CONDUÇÃO: </b> {$ambulancia}</td></tr>
          </table>";

		//Mostra VAGA EM UTI
		$html .= "<table class='mytable' style='width:95%;'>";
		$html .= "<tr style='background-color:#EEEEEE;'>";
		if ($vaga_uti == 'S') {
			$uti = "Sim";
		} else {
			$uti = "N&atilde;o";
		}
		$html .= "<td colspan='3' ><b>NECESSITA DE INTERNAMENTO HOSPITALAR: </b>{$uti}</td></tr>";
		if(!empty($hospital)){
			$html .= "<tr><td><b>HOSPITAL/ TIPO DE LEITO: </b> {$hospital}</td></tr>";
		}

		$html .= "</table>";
		$html .= "</div>";
		return $html;
	}
}

$p = new ImprimirIntercorrencia();
$p->pdfIntercorrencia($_GET['id'],$_GET['idr'], $_GET['empresa']);

