<?php
$id = session_id();
if(empty($id))
	session_start();
include_once('../validar.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../db/config.php');
require __DIR__ . '/../vendor/autoload.php';
//ini_set('display_errors', 1);

use \App\Controllers\Administracao as Administracao;
use \App\Models\Administracao\Filial;

class Itens{
	private $tipo = "";
	private $nome = "";
	private $apresentacao = "";
	private $via = "";
	private $aprazamento = "";
	private $obs = "";
	private $dose = "";
	private $freq = "";

	function Itens($t,$n,$a,$v,$apraz,$o,$d,$f,$obEnf){
		$this->tipo = $t;
		$this->nome = $n;
		$this->apresentacao = $a;
		$this->via = $v;
		$this->aprazamento = $apraz;
		$this->obs = $o;
		$this->dose = $d;
		$this->freq = $f;

		if($obEnf != '' && $obEnf != null){
			$this->OBS_ENFERMAGEM = $obEnf;
			$this->obenf="<br/><b>OBS ENF.: </b>".$this->OBS_ENFERMAGEM;
		}
	}

	public function getAprazamento($dia){
		if(array_key_exists($dia,$this->aprazamento))
			return $this->aprazamento[$dia];
		return "&nbsp;";
	}

	public function getLabel(){
		return "{$this->tipo}{$this->nome} {$this->apresentacao} {$this->dose} {$this->via} {$this->freq} {$this->obs} {$this->obenf}";
	}
}

function aprazamento($i,$f,$inc,$hora,$ip,$fp){
//	$a = "-";
//	switch($inc){
//		case 'SN':
//		case 'ACM':
//		case 'Conforme HGT':
//		case '3 vezes por semana':
//		case '1 vez cada 15 dias':
//		case '1x mês':
//		case 'Contínuo':
//		case '1 vez ao dia':
//		case '2 vezes ao dia':
//		case '3 vezes ao dia':
//		case '4 vezes ao dia':
//		case '5 vezes ao dia':
//		case '6 vezes ao dia':
//			$a = $inc;
////			$v = 24/substr($inc,0,strpos($inc,'x'));
////			$s = "+".$v." hour";
////			$h = new DateTime($hora);
////			$limite = new DateTime('23:59:59');
////			$a = "";
////			while($h < $limite){
////				$a .= $h->format("H:i\n");
////				$h->modify($s);
////			}
//		break;
//		case '1/1h':
//		case '2/2h':
//		case '3/3h':
//		case '4/4h':
//		case '5/5h':
//		case '6/6h':
//		case '7/7h':
//		case '8/8h':
//		case '9/9h':
//		case '10/10h':
//		case '12/12h':
//		case '24/24h':
//			$s = "+".substr($inc,0,strpos($inc,'/'))." hour";
//			$h = new DateTime($hora);
//			$limite = new DateTime('23:59:59');
//			$a = "";
//			while($h < $limite){
//				$a .= $h->format("H:i\n");
//				$h->modify($s);
//			}
//		break;
//	}
	$apraz = array();
	$inicio = new DateTime($i);
	$fim = new DateTime($f);
	$atual = new DateTime($ip);
	$fim_periodo = new DateTime($fp);
	$a = $hora;
	while($atual <= $fim_periodo){
		if($atual >= $inicio && $atual <= $fim)
			$apraz[$atual->format('Y-m-d')] = $a;
		else $apraz[$atual->format('Y-m-d')] = "&nbsp;";
		$atual->modify("+1 day");
	}
	return $apraz;
}

$p = $_REQUEST['prescricao'];
$empresa = $_REQUEST['empresa'];
$responseEmpresa = Filial::getEmpresaById($empresa);



$sql = "SELECT u.nome as medico, u.tipo as utipo, u.conselho_regional, DATE_FORMAT(presc.data,'%d/%m/%Y - %T') as sdata, presc.inicio, presc.fim, 
			c.nome as paciente, c.idClientes, c.cuidador, c.relcuid as relacao,
			DATE_FORMAT(presc.inicio,'%d/%m/%Y') as strinicio, DATE_FORMAT(presc.fim,'%d/%m/%Y') as strfim,
			u.ASSINATURA as assinatura,
			presc.Carater
			FROM prescricoes as presc, clientes as c, usuarios as u 
			WHERE presc.id = '{$p}' AND c.idClientes = presc.paciente AND presc.criador = u.idUsuarios ";
$result = mysql_query($sql);
$row = mysql_fetch_array($result);
$ip = $row['inicio'];
$fp = $row['fim'];
$caraterPresc = $row['Carater'];

$img = "<img src='../utils/logos/{$responseEmpresa['logo']}' width='20%' style='align:center' />";
$hoje = date("d/m/Y");
$compTipo = $row['conselho_regional'];

$header = "<table border='1' width='100%' ><thead><tr><th width='40%' rowspan='3' colspan='3' >{$img}<br/>{$responseEmpresa['nome']}</th><th width=20% colspan='5'>Prescrição {$row['strinicio']} a {$row['strfim']} - ".ucwords(strtolower($row['paciente']))."</th>";
$header .= "<th rowspan='3' colspan='2' ><img src='{$row['assinatura']}' width='20%'></th></tr></tr>";
//	$header .= "<tr><td colspan='7'><b>Nome: </b>".ucwords(strtolower($row['paciente']))."</td></tr>";


$header .= "<tr><td colspan='5'><b>Cuidador: </b>".utf8_encode(ucwords(strtolower($row['cuidador'])))." (".utf8_encode($row['relacao']).")</td></tr>";
$tipo = $row['utipo'] == 'medico'?'Médico(a)':'Nutricionista';

$header .= "<tr><td colspan='5' ><b>Profissional: </b>".utf8_encode(ucwords(strtolower($row['medico'])))."{$compTipo} ({$tipo})</td></tr>";

$sql = "SELECT v.via as nvia, f.frequencia as nfrequencia,i.*, i.inicio, 
    		i.fim, i.aprazamento as apraz, u.unidade as um 
    		FROM itens_prescricao as i LEFT OUTER JOIN frequencia as f ON (i.frequencia = f.id) 
    		LEFT OUTER JOIN viaadm as v ON (i.via = v.id) 
    		LEFT OUTER JOIN umedidas as u ON (i.umdose = u.id) 
    		WHERE idPrescricao = '$p'  
    		ORDER BY tipo,inicio,aprazamento,nome,apresentacao ASC";
$result = mysql_query($sql);
$flag = true;

$datas_periodo_prescrito = array();
$itens_periodo_prescrito = array();
$inicio_periodo_prescrito = new DateTime($ip);
$fim_periodo_prescrito = new DateTime($fp);

if($tip != -1){
	//$inicio_periodo_prescrito;

	while($inicio_periodo_prescrito <= $fim_periodo_prescrito){
		$tmp = new DateTime();
		$tmp = clone $inicio_periodo_prescrito;
		$datas_periodo_prescrito[] = $tmp;

		$inicio_periodo_prescrito->modify("+1 day");
	}
}

$suspensoes = array();
while($row = mysql_fetch_array($result)){
	$tip = $row['tipo'];
	if($row['tipo'] != "-1"){
		$obs = "";
		if($row['obs'] != "") $obs = "OBS: ".$row['obs'];
		$tipo = "";
		if($row['tipo'] == "0") $tipo = "Repouso ";
		if($row['tipo'] == "1") $tipo = "Dieta ";
		if($row['tipo'] == "9") $tipo = "Nebulização ";
		if($row['tipo'] == "13") $tipo = "Oxigenoterapia ";
		$aprazamento = aprazamento($row['inicio'],$row['fim'],$row['nfrequencia'],$row['apraz'],$ip,$fp);
		$dose = "";
		if($row['tipo'] != "0" && $row['tipo'] != "1" && $row['tipo'] != "2" && $row['tipo'] != "9" && $row['tipo'] != "12")
			$dose = $row['dose']." ".$row['um'];
		$itens_periodo_prescrito[] = new Itens($tipo,$row['nome'],$row['apresentacao'],$row['nvia'],$aprazamento,$obs,$dose,$row['nfrequencia'],$row['OBS_ENFERMAGEM']);
	} else {
		//$itens_periodo_prescrito[] = new Itens($tipo,$row['nome'],$row['apresentacao'],$row['nvia'],$aprazamento,$obs,$dose,$row['nfrequencia']);
		$suspensoes[] = "<tr><td colspan='10' align='center'>{$row['descricao']}</td></tr>";
	}
}



$tabela = "<tr><th colspan='3' >Prescrição Semanal</th><th colspan='7'>Aprazamento</th></tr>";
//print_r($suspensoes);
$mes_atual = substr($ip,5,2);
$paginas = array();

$inicio_periodo_prescrito = new DateTime($ip);
$aviso_suspensoes = true;
if($caraterPresc == '11') {
    $tabela = "<tr><th colspan='3' >Prescrição Semanal</th><th colspan='7'>Aprazamento</th></tr>";
    if ($aviso_suspensoes) {
        foreach ($suspensoes as $susp) {
            $tabela .= $susp;
        }
        $aviso_suspensoes = false;
    }
    $paginas[] = $header . $tabela . "</table>";
} else {
    while($inicio_periodo_prescrito <= $fim_periodo_prescrito) {
        $tabela = "<tr><th colspan='3' >Prescrição Semanal</th><th colspan='7'>Aprazamento</th></tr>";
        if ($aviso_suspensoes) {
            foreach ($suspensoes as $susp) {
                $tabela .= $susp;
            }
            $aviso_suspensoes = false;
        }
        $date = clone $inicio_periodo_prescrito;
        $data_inicial_tabela = clone $inicio_periodo_prescrito;
        $dias = "";
        $mes_atual = substr($inicio_periodo_prescrito->format('Y-m-d'), 5, 2);

        for ($d = 0; $d < 7; $d++) {
            $dia_coluna = "&nbsp;";
            $dia = $date->format("Y-m-d");

            if (($mes_atual == substr($dia, 5, 2)) && ($date <= $fim_periodo_prescrito)) {
                $dia_coluna = $date->format("d/m/Y");

                $inicio_periodo_prescrito->modify("+1 day");
            }
            if ($tip != "-1") {
                $dias .= "<th width=10%>{$dia_coluna}</th>";
                $date->modify("+1 day");
            }
        }

        if ($tip != "-1") {
            $tabela .= "<tr><th>N&ordm;</th><th>Descrição</th><th>Uso/Observações</th>$dias</tr></thead>";
            $i = 1;

            foreach ($itens_periodo_prescrito as $item) {
                $date = clone $data_inicial_tabela;
                $aprazamento = "&nbsp;";
                for ($d = 0; $d < 7; $d++) {
                    $dia = $date->format("Y-m-d");
                    if ($mes_atual == substr($dia, 5, 2)) {
                        $aprazamento .= "<td align='center' width=10% >" . $item->getAprazamento($dia) . '</td>';
                    } else {
                        $aprazamento .= "<td align='center' width=10% >&nbsp;</td>";
                    }
                    $date->modify("+1 day");
                }

                $cor = "#FFFFFF";
                if ($i % 2) $cor = "#DCDCDC";
                $label = $item->getLabel();
                $tabela .= "<tr bgcolor='{$cor}' ><td>{$i}</td><td colspan='2' >{$label}</td>";
                $tabela .= $aprazamento;
                $tabela .= "</tr>";
                $i++;
            }
        }
        $paginas[] = $header . $tabela . "</table>";
    }
}

//print_r($paginas);

$mpdf=new mPDF('pt','A4-L',9);
$mpdf->SetHeader('página {PAGENO} de {nbpg}');
$ano = date("Y");
$mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']}"." &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));
$mpdf->WriteHTML("<html><body>");
$flag = false;
foreach($paginas as $pag){
	if($flag) $mpdf->WriteHTML("<formfeed>");
	$mpdf->WriteHTML($pag);
	$flag = true;
}
$mpdf->WriteHTML("</body></html>");
$mpdf->Output('prescricao.pdf','I');
exit;
?>