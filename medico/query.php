<?php

$id = session_id();
if (empty($id))
    session_start();
//unset($_SESSION['id_user']);
//session_destroy();
//session_regenerate_id();
include_once('../db/config.php');
include_once('../utils/codigos.php');
include_once("../utils/class.phpmailer.php");
include_once('../validar.php');

use App\Core\Config;
use App\Services\Gateways\SendGridGateway;
use App\Services\MailAdapter;
use \App\Models\Administracao\Filial;

//! Funcão não utilizada. Referente ao modelo anterior ao uso do brasindice.
/* ! Mantive a função pois existiu uma tendência dos clientes em retornar para o formato antes do brasindice */
function opcoes($like, $tipo) {
    echo "<table id='tabela-opcoes' class='mytable' width=100% >";
    echo "<thead><tr>";
    echo "<th><b>Princ&iacute;pio Ativo</b></th>";
    echo "<th><b>Apresenta&ccedil;&atilde;o</b></th>";
    echo "<th><b>Estoque</b></th>";
    echo "</tr></thead>";
    $cor = false;
    $total = 0;
    $sql = "SELECT DISTINCT med.idMedicacoes, med.principioAtivo, com.nome, com.estoque, com.idNomeComercialMed FROM medicacoes as med, nomecomercialmed as com LEFT OUTER JOIN nomesfantasia as f ON f.apr = com.idNomeComercialMed WHERE (med.principioAtivo LIKE '%$like%' OR com.nome LIKE '%$like%' OR f.fantasia LIKE '%$like%') AND med.idMedicacoes = com.Medicacoes_idMedicacoes AND com.classe = '$tipo' ORDER BY med.principioAtivo";
//    echo $sql."<br/>";
    $result = mysql_query($sql);
    //or trigger_error(mysql_error());
    while ($row = mysql_fetch_array($result)) {
        foreach ($row AS $key => $value) {
            $row[$key] = stripslashes($value);
        }
        if ($cor)
            echo "<tr pnome='{$row['principioAtivo']}' snome='{$row['nome']}' cod='{$row['idNomeComercialMed']}' >";
        else
            echo "<tr pnome='{$row['principioAtivo']}' snome='{$row['nome']}' cod='{$row['idNomeComercialMed']}' class='odd'>";
        $cor = !$cor;
        echo "<td valign='top'>{$row['principioAtivo']}</td>";
        echo "<td valign='top'>{$row['nome']}</td>";
        echo "<td valign='top'>{$row['estoque']}</td>";
        echo "</tr>";
    }
    echo "</table>";
    echo "<script type='text/javascript' >";

    echo " $('#tabela-opcoes tr').dblclick(function(){
	      $('#nome').val($(this).attr('pnome'));
	      $('#nome').attr('readonly','readonly');
	      $('#apr').val($(this).attr('snome'));
	      $('#apr').attr('readonly','readonly');
	      $('#cod').val($(this).attr('cod'));
	      $('#tabela-opcoes').html('');
	      $('#posologia').focus();
	      $('#medicamento').val('');
	  });";
    echo "</script>";
}

function createDialogPreviewPrescricao()
{
    $html = <<<HTML
<div id="dialog-preview-prescricao" title="" style="display:none;">
	<div class="wrap" style="font-size: 10pt;">
	</div>
</div>
HTML;
    return $html;
}

function prescricoes_antigas($p, $tipo) {
    $sql = "SELECT presc.*, UPPER(u.nome) as nome, DATE_FORMAT(presc.data,'%d/%m/%Y - %T') as sdata, DATE_FORMAT(presc.inicio, '%d/%m/%y') as sinicio, DATE_FORMAT(presc.fim,'%d/%m/%y') as sfim, (CASE presc.carater WHEN 1 THEN  'Emerg�ncia' WHEN 2
THEN  'Peri�dica' WHEN 3 THEN  'Aditiva' ELSE  'NOT' END) as tipo FROM prescricoes as presc, clientes as c, usuarios as u
			WHERE presc.paciente = '{$p}' AND c.idClientes = presc.paciente AND presc.criador = u.idUsuarios ORDER BY data DESC";
    $result = mysql_query($sql);
    $flag = false;
    echo "<table width='100%' >";
    echo "<tr>";
    echo "<td>";
    echo "<input type='radio' name='antigas' value='-1' checked /><b>Nova</b><br/>";
    echo "</td>";
    echo "</tr>";
    $cor = true;
    if ($tipo == 2) {
        while ($row = mysql_fetch_array($result)) {
            foreach ($row AS $key => $value) {
                $row[$key] = stripslashes($value);
            }
            if ($cor)
                echo "<tr style='background-color:#EDEEE0;'>";
            else
                echo "<tr >";
            $cor = !$cor;
            $flag = true;
            echo "<td><input type='radio' name='antigas' value='{$row['id']}' /><b>{$row['sdata']}</b><b style='color:green'> ( {$row['nome']} DE: {$row['sinicio']} AT&Eacute; {$row['sfim']})</b></td>";

            echo "</tr>";
        }
    }
    echo "</table>";
}

//! Finaliza uma prescrição
function finalizar_prescricao($post) {
    extract($post, EXTR_OVERWRITE);
    mysql_query("begin");
    $usuario_atual = $_SESSION["id_user"];
    $inicio = anti_injection($inicio, "literal");
    $i = implode("-", array_reverse(explode("/", $inicio)));
    $fim = anti_injection($fim, "literal");
    $f = implode("-", array_reverse(explode("/", $fim)));
    $empresa = $_SESSION["empresa_principal"];
    $num_sugestao = anti_injection($num_sugestao, 'literal');
    $idexame = anti_injection($idexame, "literal");
    $tipoAditivo = anti_injection($tipoAditivo, "literal");
    $tipoAditivo = $tipoAditivo == 'NOVO ITEM'? '' : $tipoAditivo;
    $motivoAjusteAditivo =  anti_injection($motivoAjusteAditivo, "literal");


    if($num_sugestao > 0){
        $sql = "UPDATE sugestoes_secmed SET data_resposta = NOW() WHERE id_capmedica = {$id_cap}  AND local_sugestao = 'prsc'";
        $rs = mysql_query($sql);

        $sql = "SELECT id FROM sugestoes_secmed WHERE id_capmedica = {$id_cap} AND local_sugestao = 'prsc'";
        $rs = mysql_query($sql);
        while($row = mysql_fetch_array($rs, MYSQL_ASSOC)){
            $sql_alteracao = "INSERT INTO sugestoes_alteracao (id_sugestao, alterado_por, data_alteracao)
                              VALUES ({$row['id']}, {$_SESSION['id_user']}, NOW())";
            $rs_alteracao = mysql_query($sql_alteracao);
        }
    }

    if ($edt == 1) {
        $id_origem = $presc;
    } else {
        $id_origem = "0";
    }

    $sql_criador = "SELECT max(`id`), criador FROM `prescricoes` WHERE `ID_CAPMEDICA`='{$id_cap}' AND (lockp <> '9999-12-30 23-59-59'  and (DESATIVADA_POR = 0 or DESATIVADA_POR is null)) ";
    $r = mysql_query($sql_criador);
    if (!$r) {
        echo "-23";
        mysql_query("rollback");
        return;
    }
    $id = mysql_result($r, 0);
    $criador = mysql_result($r, 0, 1);

    //$informacoes = "edt: $edt - criador: $criador - atual: $usuario_atual - id: $id";
    //Validar se o usuário é igual ao criador
    //Se sim, edita. Se não, cria uma nova e adiciona o código da anterior.



    if ($edt == 1 && $criador == $usuario_atual) {

        $itens = explode("$", $dados);
        $presc = anti_injection($presc, "numerico");
        $id = $presc;

        $sql = "DELETE FROM itens_prescricao WHERE idPrescricao ='{$id}' ";
        $r = mysql_query($sql);
        if (!$r) {
            echo "-22";
            mysql_query("rollback");
            return;
        }
        $sql = "UPDATE prescricoes SET data= now(), fim = '{$f}', inicio='{$i}',criador='{$usuario_atual}' WHERE id = '{$id}'";
        $r = mysql_query($sql);
        if (!$r) {
            echo "-21";
            mysql_query("rollback");
            return;
        }

        $c = 0;
        //$itens = explode("$",$dados);

        savelog(mysql_escape_string(addslashes($sql)));
        foreach ($itens as $item) {
            $c++;
            $i = explode("#", str_replace("undefined", "", $item));
            $i[6] = anti_injection($i[6], "literal");
            $inicio = implode("-", array_reverse(explode("/", $i[6])));
            $i[7] = anti_injection($i[7], "literal");
            $fim = implode("-", array_reverse(explode("/", $i[7])));
            $i[1] = anti_injection($i[1], "literal");
            $codigo = $i[1];
            $i[0] = anti_injection($i[0], "literal");

             $i[2] = anti_injection($i[2], "literal");
             $i[3] = anti_injection($i[3], "literal");
             $i[4] = anti_injection($i[4], "literal");
             $i[5] = anti_injection($i[5], "literal");
             $i[8] = anti_injection($i[8], "literal");
             $i[9] = anti_injection($i[9], "literal");
             $i[10] = anti_injection($i[10], "literal");
             $i[11] = anti_injection($i[11], "literal");
             $i[12] = anti_injection($i[12], "literal");
             $i[13] = anti_injection($i[13], "literal");
             $i[14] = anti_injection($i[14], "literal");
             $i[15] = anti_injection($i[15], "literal");

            if ($i[0] == "12") {
                $sql = "INSERT INTO formulas (`formula`,`PACIENTE_ID`,`USUARIO_ID`,`USUARIO_UR`,`DATA`) VALUES ('{$i[9]}',{$paciente},'{$criador}','{$_SESSION['empresa_principal']}',now())";
                $r = mysql_query($sql);
                if (!$r) {
                    echo "-1";
                    mysql_query("rollback");
                    return;
                }
                $codigo = mysql_insert_id();
                savelog(mysql_escape_string(addslashes($sql)));
            }
            $i[10] = anti_injection($i[10], "literal");
            $dose = $i[10];

            $quantidade_diarias = $i[13];
            $diarias_dieta = $i[12];

            $sql = "INSERT INTO itens_prescricao
				(idPrescricao,tipo,NUMERO_TISS,nome,via,frequencia,aprazamento,inicio,fim,obs,descricao,dose,umdose,CATALOGO_ID)
				VALUES
				('{$id}','{$i[0]}','{$codigo}','{$i[2]}','{$i[3]}','{$i[4]}','{$apr}','{$inicio}','{$fim}','{$i[8]}',
				'{$i[9]}','{$dose}','{$i[11]}','{$i[15]}')";

            if ($i[0] == "s") {
                $sql = "UPDATE itens_prescricao SET fim = curdate() WHERE id = '{$i[12]}'";
                $lockp1 = '9999-12-31';
                $r = mysql_query("INSERT INTO itens_prescricao (idPrescricao,tipo,NUMERO_TISS,nome,via,frequencia,aprazamento,inicio,fim,obs,descricao,dose,umdose,CATALOGO_ID) " .
                        "VALUES ('{$id}','-1','{$codigo}','{$i[2]}','{$i[3]}','{$i[4]}','-',curdate(),curdate(),'{$i[8]}','{$i[9]}','{$dose}','{$i[11]}','{$i[15]}')");
                if (!$r) {
                    echo "-2";
                    mysql_query("rollback");
                    return;
                }
            }
            $r = mysql_query($sql);


            if (!$r) {
                echo "-3";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }

        if ($r) {
            mysql_query("commit");
            $arquivo = "salvas/" . $usuario_atual . "/" . $paciente . ".htm";

            if (file_exists($arquivo))
                unlink($arquivo);
            echo $id;
        }else {
            mysql_query("rollback");
            echo "-99";
        }
    } else {
        $sql = "SELECT nome, convenio, empresa FROM clientes WHERE idClientes = '{$paciente}'";
        $rs = mysql_query($sql);
        $rowPaciente = mysql_fetch_array($rs);
        $empresa = null;
        $convenio = null;
        if(count($rowPaciente) > 0) {
            $empresa = $rowPaciente['empresa'];
            $convenio = $rowPaciente['convenio'];
            $nomePaciente = $rowPaciente['nome'];
        }



        $responseEmpresa = Filial::getEmpresaById($empresa);

        //liberado

        if ($tipo_presc == 4 || $tipo_presc == 7) {
            ////jeferson 2013-05-22
            if ($tipo_presc == 4 && $edt == 1) {
                $sql = "UPDATE prescricoes SET lockp = '9999-12-30 23-59-59', STATUS = 1 ,DATA_DESATIVADA=now(),DESATIVADA_POR={$usuario_atual} WHERE id = '{$id}'";
                $r = mysql_query($sql);
                if (!$r) {
                    echo "-25";
                    mysql_query("rollback");
                    return;
                }
            }
            ////

            $sql = "INSERT INTO prescricoes (paciente,empresa,plano,criador,data,lockp,inicio,fim,Carater,ID_CAPMEDICA, LIBERADO, PRESCRICAO_ORIGEM, LIBERADO_POR,DATA_LIBERADO)
		VALUES ('{$paciente}','{$empresa}','{$convenio}','{$usuario_atual}',now(),now(),'{$i}','{$f}','{$tipo_presc}','{$id_cap}', 'S', '{$id_origem}',$usuario_atual,now())";
        } else {

            $flag = $tipoAditivo;
            if(!empty($idexame)){
                $flag = 'LABORATORIAL';
            }
            if(!empty($idIntercorrencia)){
                $flag = 'INTERCORRÊNCIA';
            }
            $sql = "INSERT INTO prescricoes (paciente,empresa,plano,criador,data,lockp,inicio,fim,Carater,
ID_CAPMEDICA, LIBERADO, PRESCRICAO_ORIGEM,flag,exames_laboratoriais_paciente_id,intercorrencia_med_id, motivo_ajuste)
		VALUES ('{$paciente}','{$empresa}','{$convenio}','{$usuario_atual}',now(),now(),'{$i}','{$f}','{$tipo_presc}','{$id_cap}', 'S',
		'{$id_origem}','{$flag}','{$idexame}','{$idIntercorrencia}', '{$motivoAjusteAditivo}')";
        }


        $sql1 = mysql_query($sql);
        if (!$sql1) {
            echo "-24";
            mysql_query("rollback");
            return;
        }
        $c = 0;
        $itens = explode("$", $dados);
        $id = mysql_insert_id();

        savelog(mysql_escape_string(addslashes($sql)));

        // INSERINDO OS PROCEDIMENTOS DE GTM CASO EXISTA E SE A PRESCRIÇÃO SEJA ADITIVA
        if($tipo_presc == 3 && count($post['gtm']) > 0) {
            foreach ($post['gtm'] as $gtm) {
                $dataTroca = \DateTime::createFromFormat('d/m/Y', $gtm['data_troca'])->format('Y-m-d');
                $sqlGtm = <<<SQL
INSERT INTO procedimentos_gtm
(
  id,
  prescricao_id,
  catalogo_id,
  data_troca,
  tipo,
  furo,
  cm,
  fr,
  marca,
  dano,
  origem_dano,
  descricao_origem_dano
) VALUES (
  NULL,
  '{$id}',
  '{$gtm['brasindice_id']}',
  '{$dataTroca}',
  '{$gtm['tipo_gtm']}',
  '{$gtm['furo']}',
  '{$gtm['cm']}',
  '{$gtm['fr']}',
  '{$gtm['marca_gtm']}',
  '{$gtm['dano_gtm']}',
  '{$gtm['origem_dano_gtm']}',
  '{$gtm['descricao_origem_dano_gtm']}'
)
SQL;
                $rsGtm = mysql_query($sqlGtm);
                if (!$rsGtm) {
                    echo "Problema ao inserir um procedimento de GTM.";
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sqlGtm)));
            }
        }
        $hasServicos = false;
        $listServicos = '<ul> <strong>Serviços: </strong>';
        foreach ($itens as $item) {
            $c++;
            $i = explode("#", str_replace("undefined", "", $item));
            $i[6] = anti_injection($i[6], "literal");
            $inicio = implode("-", array_reverse(explode("/", $i[6])));
            $i[7] = anti_injection($i[7], "literal");
            $fim = implode("-", array_reverse(explode("/", $i[7])));
            $i[1] = anti_injection($i[1], "literal");
            $codigo = $i[1];

           $i[0] = anti_injection($i[0], "literal");
             $i[2] = anti_injection($i[2], "literal");
             $i[3] = anti_injection($i[3], "literal");
             $i[4] = anti_injection($i[4], "literal");
             $i[5] = anti_injection($i[5], "literal");
             $i[8] = anti_injection($i[8], "literal");
             $i[9] = anti_injection($i[9], "literal");
             $i[10] = anti_injection($i[10], "literal");
             $i[11] = anti_injection($i[11], "literal");
             $i[12] = anti_injection($i[12], "literal");
             $i[13] = anti_injection($i[13], "literal");
             $i[14] = anti_injection($i[14], "literal");
             $i[15] = anti_injection($i[15], "literal");

             if($tipo_presc == 3 && ($i[0] == '2' ||  $i[0] == '0')) {
                 $hasServicos = true;
                 $listServicos .= "<li>{$i[2]}</li>";
             }

            if ($i[0] == "12") {
                $sql = "INSERT INTO formulas (`formula`,`PACIENTE_ID`,`USUARIO_ID`,`USUARIO_UR`,`DATA`) VALUES ('{$i[9]}',{$paciente},'{$criador}','{$_SESSION['empresa_principal']}',now())";
                $r = mysql_query($sql);
                if (!$r) {
                    echo "-4";
                    mysql_query("rollback");
                    return;
                }
                $codigo = mysql_insert_id();
                savelog(mysql_escape_string(addslashes($sql)));
            }
            $i[10] = anti_injection($i[10], "literal");
            $dose = $i[10];

            $sql = "INSERT INTO itens_prescricao (idPrescricao,tipo,NUMERO_TISS,nome,via,frequencia,aprazamento,inicio,fim,obs,descricao,dose,umdose,CATALOGO_ID) " .
                    "VALUES ('{$id}','{$i[0]}','{$codigo}','{$i[2]}','{$i[3]}','{$i[4]}','{$apr}','{$inicio}','{$fim}','{$i[8]}','{$i[9]}','{$dose}','{$i[11]}','{$i[15]}')";

            if ($i[0] == "s") {
                $i[12] = anti_injection($i[12], "numerico");
                $sql = "UPDATE itens_prescricao SET fim = curdate() WHERE id = '{$i[12]}'";
                $lockp1 = '9999-12-31';
                $r = mysql_query("INSERT INTO itens_prescricao (idPrescricao,tipo,NUMERO_TISS,nome,via,frequencia,aprazamento,inicio,fim,obs,descricao,dose,umdose,CATALOGO_ID) " .
                        "VALUES ('{$id}','-1','{$codigo}','{$i[2]}','{$i[3]}','{$i[4]}','-',curdate(),curdate(),'{$i[8]}','{$i[9]}','{$dose}','{$i[11]}','{$i[11]}')");
                if (!$r) {
                    echo "-5";
                    mysql_query("rollback");
                    return;
                }
            }
            $r = mysql_query($sql);

            if (!$r) {
                echo "-6 {$sql} - ";
                //. mysql_error();
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
        if ($sql1) {
            mysql_query("commit");

            if($tipo_presc == 3 )
            {
                enviarEmail($paciente, 8, $empresa);
                if($hasServicos){
                    $para = [];
                    $msg = "<p>Encontra-se dispon&iacute;vel um(a) novo(a) <b>Relat&oacute;rio Aditivo M&eacute;dico</b> para <b>{$nomePaciente}</b>
                        <br/> feito por <b>{$_SESSION['nome_user']}</b>.
                        <br>Empresa: <b>{$responseEmpresa['nome']}</b>";
                    $msg .= $listServicos . '</ul>';
                    $titulo = "Avisos de Sistema - Sistema de Gerenciamento da Mederi";
                    //$emails = require $_SERVER['DOCUMENT_ROOT'] . '/app/configs/email.php';
                    $para[] = ['email' => 'saad@mederi.com.br', 'nome' => 'SAAD Mederi'];
                    enviarEmailSimples($titulo, $msg, $para);
                }


            }

            $arquivo = "salvas/" . $usuario_atual . "/" . $paciente . ".htm";
            if (file_exists($arquivo))
                unlink($arquivo);
            echo $id;
        }
        else {
            mysql_query("rollback");
            echo "-7";
        }
    }
}

function enviar_email_formula($dados1, $paciente, $criador, $ur) {
    extract($dados1, EXTR_OVERWRITE);

    $result = mysql_query("SELECT nome FROM `clientes` WHERE `idClientes` ={$paciente}");
    $paciente = mysql_result($result, 0, 0);
    //print_r($paciente);
    $result1 = mysql_query("Select nome  from usuarios where idUsuarios ={$criador}");
    $medico = mysql_result($result1, 0, 0);
    $itens = explode("$", $dados);
    $formulas = " ";
    foreach ($itens as $item) {
        $c++;
        $i = explode("#", str_replace("undefined", "", $item));


        ////jeferson Formula 05-05-2013
        if ($i[0] == "12") {

            $formulas .= "{$i[9]}<br/>";
        }
    }

    $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
    $mail->IsSMTP(); // telling the class to use SMTP

    try {
        // $mail->Host       = "mail.yourdomain.com"; // SMTP server
        //	  $mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
        $mail->SMTPAuth = true;                  // enable SMTP authentication
        //$mail->SMTPSecure = "TLS";                 // sets the prefix to the servier
        $mail->Host = "smtp.mederi.com.br";      // sets GMAIL as the SMTP server
        $mail->Port = 587;                   // set the SMTP port for the GMAIL server
        $mail->Username = "avisos@mederi.com.br";  // GMAIL username
        $mail->Password = "2017avisosistema";            // GMAIL password
        //	  $mail->AddReplyTo('name@yourdomain.com', 'First Last');

        $mail->AddAddress('avisos@mederi.com.br', "Teste");
        //--$mail->AddAddress('dirmed@mederi.com.br', "Diretoria medica");
        //--$mail->AddAddress('direnf@mederi.com.br', "Diretoria enfermagem"); // Retorno da UR: (1)
        //$mail->AddAddress('ti@mederi.com.br');
        $ur = "({$ur})";
        /*
          if($ur == '(1)'){//email por unidade regional

          //	$mail->AddAddress('coordmed.fsa@mederi.com.br', 'Coordenação Médica - Feira de Santana');
          //	$mail->AddAddress('coordenf.fsa@mederi.com.br', 'Coordenação de Enfermagem - Feira de Santana');
          $mail->AddAddress('ti@mederi.com.br');
          } else if($ur == '(2)'){

          $mail->AddAddress('coordmed.salvador@mederi.com.br', 'Coordenação Médica - Salvador');
          $mail->AddAddress('coordenf.salvador@mederi.com.br', 'Coordenação de Enfermagem - Salvador');
          } else if($ur == '(4)'){
          $mail->AddAddress('coordmed.sulbahia@mederi.com.br', 'Coordenação Médica - Sul/Bahia');
          $mail->AddAddress('coordenf.sulbahia@mederi.com.br', 'Coordenação de Enfermagem - Sul/Bahia');

          } else if($ur == '(5)'){
          $mail->AddAddress('coordenf.alagoinhas@mederi.com.br', 'Coordenação de Enfermagem - Alagoinhas');

          } else if($ur == '(6)'){
          $mail->AddAddress('coordenf.rms@mederi.com.br', 'Coordenação de Enfermagem - RMS');

          } else if($ur == '(7)'){
          $mail->AddAddress('coordmed.sudoeste@mederi.com.br', 'Coordenação Médica - Sudoeste');
          $mail->AddAddress('coordenf.sudoeste@mederi.com.br', 'Avisos M&eacute;dicos Mederi');
          }
         */
        $mail->SetFrom('avisos@mederi.com.br', 'Avisos de Sistema');
        //	  $mail->AddReplyTo('name@yourdomain.com', 'First Last');
        $mail->Subject = "TESTE - Alerta " . utf8_decode("Fórmulas");
        $mail->AltBody = 'Para ver essa mensagem, use um visualizador de email compat&iacute;vel com HTML'; // optional - MsgHTML will create an alternate automatically
        //$solicitante = ucwords(strtolower($_SESSION["nome_user"]));
        $mail->MsgHTML(utf8_decode($medico) . " prescreveu " . utf8_decode('fórmula') . " para o paciente: " . utf8_decode($paciente) . "<br/> " . utf8_decode($formulas));
        //	  $mail->AddAttachment('images/phpmailer.gif');      // attachment
        //	  $mail->AddAttachment('images/phpmailer_mini.gif'); // attachment
        $enviado = $mail->Send();

        /* if ($enviado) {
          echo "E-mail enviado com sucesso!";
          window.location.href='?adm=paciente&act=listfichaevolucao&id='{$paciente};
          } else {
          echo "N�o foi poss�vel enviar o e-mail.<br /><br />";
          echo "<b>Informa��es do erro:</b> <br />" . $mail->ErrorInfo;
          } */
    } catch (phpmailerException $e) {
        echo $e->errorMessage(); //Pretty error messages from PHPMailer
    } catch (Exception $e) {
        echo $e->getMessage(); //Boring error messages from anything else!
    }
    header();
}

////////funcao aprazamento
function aprz($hora, $qtdi, $data1, $data2) {
    //$hora = '08:15';
    //$qtdi=3;
    $x = $qtdi;
    $h = explode(":", $hora);
    if ($h[0] >= 10)
        $hi = $h[0];
    else
        $hi = substr($h[0], 1, 1);
    $aux_h = $hi;
    //$data1 ='2012-01-01';
    //$data2 = '2012-01-03';
    $data1 = anti_injection($data1, "literal");
    $data2 = anti_injection($data1, "literal");
    $dias = ((strtotime($data2) - strtotime($data1)) / 86400);

    $tot = round(($dias * 24));


    $ap = array();
    $ap_tudo = array();
    $j = 0;
    $a = 0;
    $x = 0;
    for ($i = 0; $i < $tot;) {
        while ($hi + $x <= 23) {

            if ($i != 0) {
                $a = $hi + $qtdi;
                $hi = $hi + $qtdi;
            } else {
                $a = $aux_h;
                $hi = $hi;
            }
            $x = $qtdi;

            $i += $qtdi;
            $hora_formatada .= $a . ":" . $h[1] . " ";
        }
        array_push($ap_tudo, $hora_formatada);
        $hora_formatada = '';

        $j++;
        $hi = $hi - 24;
    }

    return $ap_tudo;
}

////gravar problemas ativos
function prob_ativos($post) {
    extract($post, EXTR_OVERWRITE);

    $id_paciente = anti_injection($paciente, "numerico");

    $usuario = $_SESSION["id_user"];
    mysql_query("begin");
    $sql = "INSERT INTO capmedica (`id`,`paciente`,`data`,`usuario`) VALUES (NULL,'{$id_paciente}',now(),'{$usuario}')";
    $r = mysql_query($sql);
    if (!$r) {
        mysql_query("rollback");
        echo "ERRO: Tente mais tarde!";
        return;
    }
    $idcapmedica = mysql_insert_id();
    savelog(mysql_escape_string(addslashes($sql)));
    $problemas = explode("$", $probativos);
    foreach ($problemas as $prob) {
        if ($prob != "" && $prob != null) {
            $i = explode("#", $prob);
            $cid = anti_injection($i[0], "literal");
            $desc = anti_injection($i[1], "literal");
            $prob = anti_injection($prob, "literal");
            $sql = "INSERT INTO problemasativos VALUES ('{$idcapmedica}','{$cid}','{$desc}') ";
            $r = mysql_query($sql);
            if (!$r) {
                mysql_query("rollback");
                echo "ERRO: Tente mais tarde!";
                return;
            }
        }
    }
    savelog(mysql_escape_string(addslashes($sql)));
    mysql_query("commit");
}

///jeferson2012-10-22
function equipamentos_verificar($post) {
    extract($post, EXTR_OVERWRITE);
    $id_cap = anti_injection($id_cap, "numerico");
    $sql = "SELECT
	C.id,
	C.item,
	eqp.QTD,
	eqp.OBSERVACAO
	FROM
	equipamentos_capmedica as eqp INNER JOIN
	cobrancaplanos as C ON (C.id = eqp.COBRANCAPLANOS_ID)

	WHERE
	eqp.CAPMEDICA_ID = " . $id_cap;


    $result = mysql_query($sql);

    //$num_equipamentos = mysql_num_rows($result);

    $equipamento = '';
    $msg = '';
    $tabela = array(
        'cod_item_equip' => array(
            /* Cod. do Item => Aparelho */
            '17' => 'Aspirador',
            '18' => 'Glicosimetro',
            '21' => 'Ox�metro',
            '24' => 'BIPAP',
            '5988' => 'Concentrador de O2',
            '9' => 'Nebulizador'
        ),
        'item' => array(
            /* Cod. do Item => Descricao do Item */
            '17' => 'Aspira&ccedil;&atilde;o',
            '18' => 'HGT',
            '21' => 'Ox&iacute;metria de Pulso',
            '24' => 'Ventila&ccedil;&atilde;o Mec&acirc;nica',
            '5988' => 'Concentrador de O2',
            '9' => 'Nebuliza&ccedil;&atilde;o'
        ),
        'equip' => array(
            /* cod. do Equip => Descricao do Equipamento */
            '12' => 'Aspirador',
            '33' => 'Glicosimetro',
            '35' => 'Ox&iacute;metro',
            '60' => 'BIPAP',
            '17' => 'Concentrador de O2',
            '34' => 'Nebulizador'
        ),
        'cod_equip_item' => array(
            /* cod. do Equip => Descricao do Item */
            '12' => 'Aspira&ccedil;&atilde;o',
            '33' => 'HGT',
            '35' => 'Oximetria de Pulso',
            '60' => 'Ventila&ccedil;&atilde;o Mec&acirc;nica',
            '17' => 'Concentrador de O2',
            '34' => 'Nebuliza&ccedil;&atilde;o'
        ),
        'equip_item' => array(
            /* cod. do Equip => Cod. do Item */
            '12' => '17',
            '33' => '18',
            '35' => '21',
            '60' => '24',
            '17' => '5988',
            '34' => '9'
        ),
        'item_equip' => array(
            /* cod. do Item => Cod. do Equip */
            '17' => '12',
            '18' => '33',
            '21' => '35',
            '24' => '60',
            '5988' => '17',
            '9' => '34'
        )
    );
    //echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
    while ($row = mysql_fetch_array($result)) {
        $equipamento[$row['id']] = utf8_decode($row['item']);
    }

    if ($dados < 1) {

        foreach ($equipamento as $cod => $item) {

            if ($cod == '34') {
                $msg .= '<p> - Foi solicitado um Nebulizador sem prescri&ccedil;&atilde;o de Nebuliza&ccedil;&atilde;o!</p>';
                $num_equipamentos++;
            } elseif ($cod == '35') {
                $msg .= '<p> - Foi solicitado um Ox&iacute;metro sem prescri&ccedil;&atilde;o de Oximetria de Pulso!</p>';
                $num_equipamentos++;
            } elseif ($cod == '12') {
                $msg .= '<p> - Foi solicitado um Aspirador sem prescri&ccedil;&atilde;o de Aspira&ccedil;&atilde;o!</p>';
                $num_equipamentos++;
            } elseif ($cod == '33') {
                $msg .= '<p> - Foi solicitado um Glicosimetro sem prescri&ccedil;&atilde;o de HGT!</p>';
                $num_equipamentos++;
            } elseif ($cod == '17') {
                $msg .= '<p> - Foi solicitado um Concentrador de O2 sem prescri&ccedil;&atilde;o de Concetrador de o2!</p>';
                $num_equipamentos++;
            }
        }
    } else {

        $itens = explode("$", $dados);
        $lista_itens = '';

        foreach ($itens as $item) {
            $con++;
            $i = explode("#", str_replace("undefined", "", $item));

            /* Aqui ele verifica se o tipo � 9, sendo ele atribui o tipo para o cod por conta do nebulizador */
            if ($i[0] == 9)
                $i[1] = $i[0];

            $lista_itens[$i[1]] = $i[2];

            if (!array_key_exists($tabela['item_equip'][$i[1]], $equipamento)) {//código: '.$cod.' equip: '.$equipamento.'
                $msg .= '<p> -  Foi feita uma  prescri&ccedil;&atilde;o de ' . ucfirst($tabela['item'][$i[1]]) . ' sem solicitar ' . $tabela['cod_item_equip'][$i[1]] . '!</p>';
            }
        }

        foreach ($equipamento as $cod => $equip) {

            if (!array_key_exists($tabela['equip_item'][$cod], $lista_itens)) {
                if ($cod == 12 || $cod == 34 || $cod == 33 || $cod == 35 || $cod == 60 || $cod == 17) {//código: '.$cod.' equip: '.$equipamento.'
                    $msg .= '<p> -  Foi solicitado um ' . ucfirst($tabela['equip'][$cod]) . ' sem prescri&ccedil;&atilde;o de ' . $tabela['cod_equip_item'][$cod] . '!</p>';
                    $num_equipamentos++;
                }
            }
        }
    }

    print_r($msg); //$msg
}

///!!!! gravar emergencial
function finalizar_prescricao_emergencial($post) {

    extract($post, EXTR_OVERWRITE);
    mysql_query("begin");
    $criador = $_SESSION["id_user"];
    $inicio = anti_injection($inicio, "literal");
    $i = implode("-", array_reverse(explode("/", $inicio)));
    $fim = anti_injection($fim, "literal");
    $f = implode("-", array_reverse(explode("/", $fim)));
    $sql = "SELECT nome, convenio, empresa FROM clientes WHERE idClientes = '{$paciente}'";
    $rs = mysql_query($sql);
    $rowPaciente = mysql_fetch_array($rs);
    $empresa = null;
    $convenio = null;
    if(count($rowPaciente) > 0) {
        $empresa = $rowPaciente['empresa'];
        $convenio = $rowPaciente['convenio'];
        $nomePaciente = $rowPaciente['nome'];
    }

    if ($tipo_presc == 1) {

        $lockp = '9999-12-31 23:59:59';
        $sql = "INSERT INTO prescricoes (paciente,criador,data,lockp,inicio,fim,Carater,solicitado,empresa,plano)
          VALUES
           ('{$paciente}','{$criador}',now(),'{$lockp}','{$i}','{$f}','{$tipo_presc}',now(),{$empresa},{$convenio})";
    }
    $r = mysql_query($sql);


    if (!$r) {
        echo "-21";
        mysql_query("rollback");
        return;
    }
    $c = 0;
    $itens = explode("$", $dados);
    $id = mysql_insert_id();
    savelog(mysql_escape_string(addslashes($sql)));
    foreach ($itens as $item) {
        $c++;
        $i = explode("#", str_replace("undefined", "", $item));
        $i[6] = anti_injection($i[6], "literal");
        $inicio = implode("-", array_reverse(explode("/", $i[6])));
        $i[7] = anti_injection($i[7], "literal");
        $fim = implode("-", array_reverse(explode("/", $i[7])));
        $codigo = $i[1];

        if ($i[0] == "12") {
            $sql = "INSERT INTO formulas (`formula`) VALUES ('{$i[9]}')";
            $r = mysql_query($sql);
            if (!$r) {
                echo "-1";
                mysql_query("rollback");
                return;
            }
            $codigo = mysql_insert_id();
            savelog(mysql_escape_string(addslashes($sql)));
        }
        $i[10] = anti_injection($i[10], "literal");
        $dose = $i[10];

        if ($i[15] == $i[13]) {

            $sql = "Select HORA from frequencia where id = {$i[4]} limit 1";

            $res = mysql_query($sql);
            if (!$res) {
                echo "-212";
                mysql_query("rollback");
                return;
            }


            while ($row = mysql_fetch_array($res)) {
                $qtd_i = $row['HORA'];
            }

            $apz = '';
            $ap = aprz($i[5], $qtd_i, $inicio, $fim);



            $ct = 0;
            foreach ($ap as $ap1) {
                if ($ct != 0)
                    $apz .= "$" . $ap1;
                else
                    $apz .= $ap1;
                $ct++;
            }
        } else {
            $apz = '-';
        }


        $sql = "INSERT INTO itens_prescricao (idPrescricao,tipo,NUMERO_TISS,nome,via,frequencia,aprazamento,inicio,fim,obs,descricao,dose,umdose,CATALOGO_ID) " .
                "VALUES ('{$id}','{$i[0]}','{$codigo}','{$i[2]}','{$i[3]}','{$i[4]}','{$apz}','{$inicio}','{$fim}','{$i[8]}','{$i[9]}','{$dose}','{$i[11]}','{$i[15]}')";

        if ($tipo_presc == 1) {

            $data1 = $inicio;
            $data2 = $fim;

            $horas = ((strtotime($data2) - strtotime($data1)) / 86400) * 24;
            $qtd4 = @round($horas / $qtd_i);

            $sql2 = "INSERT INTO solicitacoes (paciente,enfermeiro,NUMERO_TISS,qtd,autorizado,tipo,data,status,idPrescricao,data_auditado,CATALOGO_ID,KIT_ID,DATA_SOLICITACAO,TIPO_SOLICITACAO) VALUES ('{$paciente}','{$criador}','{$codigo}','{$i[17]}','0','{$i[14]}',curdate(),'2','{$id}','0','{$i[15]}','{$i[16]}', now(), '1')";

            $r = mysql_query($sql2);
            if (!$r) {
                echo "-23";
                mysql_query("rollback");
                return;
            }


            /* PEGANDO O NOME DO PACIENTE PARA SETAR NO PAINEL DA LOGISTICA */
            $consulta = mysql_fetch_array(mysql_query("SELECT C.nome FROM solicitacoes AS S INNER JOIN clientes AS C ON (S.paciente = C.idClientes) WHERE S.`idSolicitacoes`='" . mysql_insert_id() . "'"));
            $_SESSION['NON'] = $consulta['nome'];
            $_SESSION['status'] = $status;
        }

        if ($i[0] == "s") {
            $sql = "UPDATE itens_prescricao SET fim = curdate() WHERE id = '{$i[12]}'";
            $lockp1 = '9999-12-31';

            $sql2 = "INSERT INTO itens_prescricao (idPrescricao,tipo,NUMERO_TISS,nome,via,frequencia,aprazamento,inicio,fim,obs,descricao,dose,umdose,CATALOGO_ID) " .
                "VALUES ('{$id}','-1','{$codigo}','{$i[2]}','{$i[3]}','{$i[4]}','-',curdate(),curdate(),'{$i[8]}','{$i[9]}','{$dose}','{$i[11]}','{$i[15]}')";
            $r = mysql_query($sql2);
          echo $sql2;
            if (!$r) {
                echo "-1";
                mysql_query("rollback");
                return;
            }
        }
        $r = mysql_query($sql);

        if (!$r) {
            echo "-3";
            mysql_query("rollback");
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));
    }

        mysql_query("commit");

        echo $id;

}

//! Grava Outro em Principio
function gravar_outro($post) {
    extract($post, EXTR_OVERWRITE);

    #Grava o nome digitado em Principio caso n�o exista
    $ConsultPrinc = "Select count(*) as total from principioativo where principio ='{$nome}' collate utf8_unicode_ci";
    $res = mysql_query($ConsultPrinc);
    $tot = mysql_fetch_array($res);

    if ($tot['total'] <= 1) {

        $sq = "INSERT INTO principioativo (id,principio,DescricaoMedico,AddMederi,status,usuario,data) Values(null,'{$nome}','',1,0,'{$_SESSION["id_user"]}',now())";
        $sql = "(Select MAX(NUMERO_TISS)+1 as maior from CATALOGO LIMIT 1)";
        $tiss1 = mysql_query($sql);
        $tiss = mysql_fetch_array($tiss1);
        $sq1 = "INSERT INTO CATALOGO (lab_desc,principio,apresentacao,NUMERO_TISS,estoque,ValorMedio,tipo,AddMederi,usuario,data) Values('MEDERI','{$nome}','Produto Interno',{$tiss['maior']},0,0,0,1,'{$_SESSION["id_user"]}',now())";
        mysql_query($sq) or die("Erro!");
        mysql_query($sq1) or die("Erro 2!");
    } else {
        echo utf8_encode("Este Princ�pio j� existe!");
    }
}

function testar_outro($post) {
    extract($post, EXTR_OVERWRITE);

    #Grava o nome digitado em Principio caso n�o exista
    $ConsultPrinc = "Select count(*) as total from principioativo where principio ='{$nome}' collate utf8_unicode_ci";
    $res = mysql_query($ConsultPrinc);
    $tot = mysql_fetch_array($res);

    if ($tot['total'] > 1) {
        echo utf8_encode("Este Princ�pio j� existe!");
    }
}


function busca_medicamento($post) {
    extract($post, EXTR_OVERWRITE);
    //$medicamento = anti_injection($medicamento, 'literal');
    $sql = "SELECT
	id,
        CONCAT(principio, ' ', apresentacao) as nome,
	lab_desc as laboratorio,
	principio_ativo as principio_ativo,
        antibiotico,
        controlado,
        generico,
        ATIVO
FROM
	catalogo
where
	ATIVO = 'A' and
	tipo = 0 and
        ATIVO_TEMP ='S' and
	principio like '%{$medicamento}%'
ORDER BY
	nome asc";

    $resultado = mysql_query($sql);

    $html = "<table class='mytable' style='width:95%;'>";
    $html.= "<thead><tr>";
    $html.= "<th colspan='8'>Medicamento</th>";
    $html.= "</tr></thead>";
    //$html.= "<tr bgcolor='grey'><td width='48%'>Item</td><td width='7%'>Quantidade</td><td width='18%'>Per&iacute;odo</td><td>Semana 1</td><td>Semana 2</td><td>Semana 3</td><td>Semana 4</td></tr>";
    $html.= "<tr bgcolor='grey'><td width='30%'>Nome</td><td width='7%'>Laboratorio</td><td>Principio Ativo</td><td>Antibiotico</td><td>Controlado</td><td>Gen&eacute;rico</td><td>Ativado</td></tr>";
    $linha = 0;
    while ($row = mysql_fetch_array($resultado)) {

        $html.= "<tr><td id='{$row['id']}'>{$row['nome']}</td>
            <td>{$row['laboratorio']}</td>
            <td><input type='text' class='principio_ativo'  name='principio_ativo' value='{$row['principio_ativo']}' size='35' /></td>";
        $html.= "<td><input type='radio' value='S' " . (($row['antibiotico'] == 'S') ? "checked" : "") . " name='antibiotico"."{$linha}"."' >Sim</input> <input type='radio' value='N' " . (($row['antibiotico'] == 'N') ? "checked" : "") . " name='antibiotico"."{$linha}"."' >N&atilde;o</input></td>";
        $html.= "<td><input type='radio' value='S' " . (($row['controlado'] == 'S') ? "checked" : "") . " name='controlado"."{$linha}"."' >Sim</input> <input type='radio' value='N' " . (($row['controlado'] == 'N') ? "checked" : "") . " name='controlado"."{$linha}"."' >N&atilde;o</input></td>";
        $html.= "<td><input type='radio' value='S' " . (($row['generico'] == 'S') ? "checked" : "") . " name='generico"."{$linha}"."' >Sim</input> <input type='radio' value='N' " . (($row['generico'] == 'N') ? "checked" : "") . " name='generico"."{$linha}"."' >N&atilde;o</input></td>";
        $html.= "<td><input type='radio' value='S' " . (($row['ATIVO'] == 'A') ? "checked" : "") . " name='ativo"."{$linha}"."' >Sim</input> <input type='radio' value='N' " . (($row['ATIVO'] == 'D') ? "checked" : "") . " name='ativo"."{$linha}"."' >N&atilde;o</input></td></tr>";

        ++$linha;
    }
    $html.= "</table>";
    $html.= "<br>";
    $html.= "<button id='salvar_associacao' id_paciente='{$id_paciente}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button'><span class='ui-button-text'>Salvar</span></button>";
    echo $html;
}


function salva_associacao($post) {
    extract($post, EXTR_OVERWRITE);
    $item = explode("$", $itens);
    foreach ($item as $i) {
        if ($i != "" && $i != null) {
            $e = explode("#", $i);
            $id = anti_injection($e[0], "numerico");
            $principio_ativo = anti_injection($e[1], "literal");
            $antibiotico = anti_injection($e[2], "literal");
            $controlado = anti_injection($e[3], "literal");
            $generico = anti_injection($e[4], "literal");
            $ativado= anti_injection($e[5], "literal");

            $sql = "UPDATE catalogo SET PRINCIPIO_ATIVO = '{$principio_ativo}',ANTIBIOTICO = '{$antibiotico}',CONTROLADO = '{$controlado}',GENERICO = '{$generico}',ATIVO_TEMP='{$ativado}' WHERE ID = {$id}";
           // echo $sql;
            $r = mysql_query($sql);

            if (!$r) {
                echo "Erro: tente novamente 1";
                mysql_query("rollback");

                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }
    mysql_query("commit");
    echo "1";
}

//! Retorna o resultado da busca por prescrições
function buscar_prescricoes($post) {

    extract($post, EXTR_OVERWRITE);

    if (!isset($op)){
        $op = 0;
    }

    if ((isset($inicio) && $inicio != 'null') && (isset($fim) && $fim != 'null') && $paciente != 'null' && $op == 0) {
        $inicio = anti_injection($inicio, "literal");
        $i = implode("-", array_reverse(explode("/", $inicio)));
        $fim = anti_injection($fim, "literal");
        $f = implode("-", array_reverse(explode("/", $fim)));

        $_SESSION['i'] = $i;
        $_SESSION['f'] = $f;
        $_SESSION['paciente'] = $paciente;
        $limit = "";
        $conddata = " AND presc.data BETWEEN '{$i}' AND '{$f}'";
    } elseif ($paciente != null || $paciente != -1 && $op == 1) {
        unset($_SESSION['i']);
        unset($_SESSION['f']);

        $i = null;
        $f = null;
        if ($paciente != 'null') {
            unset($_SESSION['paciente']);
            $_SESSION['paciente'] = $paciente;
        }

        if (isset($_SESSION['paciente']))
          $paciente = $_SESSION['paciente'];

        $limit = " LIMIT 5";
        $conddata = "";
    } else {
        $i = $_SESSION['i'];
        $f = $_SESSION['f'];
        $paciente = $_SESSION['paciente'];

        $conddata = " AND presc.data BETWEEN '{$i}' AND '{$f}' and DATA_DESATIVADO = '0000-00-00 00:00:00'";
        $limit = "";
    }

    $condp = "1";



    if ($paciente != "-1") {
        $condp = "presc.paciente = '{$paciente}'";
        $condp1 = "c.idClientes = '{$paciente}'";
    }


    $sql = "SELECT
				presc.*,
				u.nome AS medico,
				u.tipo as utipo,
				u.conselho_regional,
				u.idUsuarios,
				DATE_FORMAT(presc.data,'%d/%m/%Y - %T') AS sdata,
				DATE_FORMAT(presc.inicio,'%d/%m/%Y') AS inicio,
				DATE_FORMAT(presc.fim,'%d/%m/%Y') AS fim,
				UPPER(c.nome) AS paciente,
				c.idClientes as CodPaciente,
				(CASE presc.carater
				  WHEN '1' THEN '<label style=\'color:red;\'><b>Emergencial</b></label>'
				  WHEN '2' THEN '<label style=\'color:green;\'><b>Peri&oacute;dica</b></label>'
				  WHEN '3' THEN concat('<label style=\'color:blue;\'><b>Aditiva ', COALESCE(presc.flag, ''),'</b></label>')
				  WHEN '4' THEN '<label style=\'color:orange;\'><b>Avalia&ccedil;&atilde;o</b></label>'
				  WHEN '5' THEN '<label style=\'color:#B8860B;\'><b>Peri&oacute;dica Nutri&ccedil;&atilde;o</b></label>'
				   WHEN '6' THEN concat('<label style=\'color:#6959CD ;\'><b>Aditiva " .htmlentities("Nutrição"). " ', COALESCE(presc.flag, ''),'</b></label>')
				  WHEN '7' THEN '<label style=\'color:#9ACD32 ;\'><b>Aditiva Avalia&ccedil;&atilde;o</b></label>'
				  WHEN '11' THEN '<label ><b>Suspens&atilde;o </b></label>'
				   END) AS tipo,
				FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
                presc.carater as carater
			FROM
				prescricoes AS presc INNER JOIN
				clientes AS c ON (c.idClientes = presc.paciente) INNER JOIN
				usuarios AS u ON (presc.criador = u.idUsuarios)
			WHERE
				{$condp}
				{$conddata}
			ORDER BY
				presc.data DESC {$limit}";
    /* presc.data BETWEEN '{$i}' AND '{$f}' AND */
    $sql2 = "SELECT
				UPPER(c.nome) AS paciente,
				(CASE c.sexo
				  WHEN '0' THEN 'Masculino'
				  WHEN '1' THEN 'Feminino'
				  END) AS sexo,
				FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
				e.nome as empresa,
				c.`nascimento`,
				p.nome as Convenio,
				NUM_MATRICULA_CONVENIO,
				cpf
			FROM
				clientes AS c LEFT JOIN
				empresas AS e ON (e.id = c.empresa) INNER JOIN
				planosdesaude as p ON (p.id = c.convenio)
			WHERE
				{$condp1}
			ORDER BY
				c.nome DESC LIMIT 1";

    $result = mysql_query($sql);
    $result2 = mysql_query($sql2);
    echo createDialogPreviewPrescricao();
    echo "<table width=100% style='border:2px dashed;'>";
    while ($pessoa = mysql_fetch_array($result2)) {
        foreach ($pessoa AS $chave => $valor) {
            $pessoa[$chave] = stripslashes($valor);
        }

        echo "<br/><tr>";
        echo "<td colspan='2'><label style='font-size:14px;color:#8B0000'><b><center> INFORMA&Ccedil;&Otilde;ES DO PACIENTE </center></b></label></td>";
        echo "</tr>";

        echo "<tr style='background-color:#EEEEEE;'>";
        echo "<td width='70%'><label><b>PACIENTE </b></label></td>";
        echo "<td><label><b>SEXO </b></label></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>{$pessoa['paciente']}</td>";
        echo "<td>{$pessoa['sexo']}</td>";
        echo "</tr>";

        echo "<tr style='background-color:#EEEEEE;'>";
        echo "<td width='70%'><label><b>IDADE </b></label></td>";
        echo "<td><label><b>UNIDADE REGIONAL </b></label></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>" . join("/", array_reverse(explode("-", $pessoa['nascimento']))) . ' (' . $pessoa['idade'] . " anos)</td>";
        echo "<td>{$pessoa['empresa']}</td>";
        echo "</tr>";

        echo "<tr style='background-color:#EEEEEE;'>";
        echo "<td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>";
        echo "<td><label><b>MATR&Iacute;CULA </b></label></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>" . strtoupper($pessoa['Convenio']) . "</td>";
        echo "<td>" . strtoupper($pessoa['NUM_MATRICULA_CONVENIO']) . "</td>";
        echo "</tr>";
        echo "<tr><td><b>CPF:</b>{$pessoa['cpf']}</td></tr>";
    }
    echo "</table><br/>";

    $flag = false;
    //echo "<button name='antigas' value='-1'>Nova</button><br/>";

    if (isset($row) && isset($row['paciente']))
      echo $row['paciente'];
    echo "<table class='mytable' width=100% >";
    echo "<thead><tr>";
    //echo "<th><b>Paciente</b></th>";
    echo "<th><b>Tipo</b></th>";
    echo "<th><b>Data</b></th>";
    echo "<th><b>Profissional</b></th>";
    echo "<th><b>Per&iacute;odo</b></th>";
    echo "<th><b>Status</b></th>";
    echo "<th colspan='3'><b>Op&ccedil;&otilde;es</b></th>";
    $n = 0;
    echo "</tr></thead>";

    if (isset($paciente) && !empty($paciente)) {
      $sql_ultima = "
            SELECT
                max(ID)
            FROM
                prescricoes
            WHERE
                paciente = {$paciente} and carater = 2
            ";
      $result_ultima = mysql_query($sql_ultima);
      $ultima_periodica = mysql_result($result_ultima, 0);

      $sql_ultima_avaliacao = "
            SELECT
                max(ID)
            FROM
                prescricoes
            WHERE
                paciente = {$paciente} and carater = 4
            ";
      $result_ultima_avaliacao = mysql_query($sql_ultima_avaliacao);
      $ultima_avaliacao = mysql_result($result_ultima_avaliacao, 0);
    }

    $data = [];
    $prescricoes = [];
    $procedimentosGtm = [];

    while ($row = mysql_fetch_array($result)) {
        $data[] = $row;
        $prescricoes[] = $row['id'];
    }

    $prescricoes = implode(',', $prescricoes);
    $sqlProcedimentos = <<<SQL
SELECT
  id,
  prescricao_id
FROM
  procedimentos_gtm
WHERE
  prescricao_id IN ({$prescricoes})
SQL;
    $rsProcedimentos = mysql_query($sqlProcedimentos);
    $numRows = mysql_num_rows($rsProcedimentos);
    if($numRows > 0) {
        while ($proc = mysql_fetch_array($rsProcedimentos)) {
            $procedimentosGtm[$proc['prescricao_id']] = $proc['id'];
        }
    }

    foreach ($data as $row) {
        $compTipo = $row['conselho_regional'];

        $hasProcedimentoGtm = '';
        if ($n++ % 2 == 0)
            $cor = '#E9F4F8';
        else
            $cor = '#FFFFFF';
        foreach ($row AS $key => $value) {
            $row[$key] = stripslashes($value);
        }
        $flag = true;
        $d = "<a href='?view=detalhes&p={$row['id']}&idp={$paciente}'><img src='../utils/details_16x16.png' title='Visualizar Prescrição' border='0' class='print-presc'></a>";

        if($row['solicitado'] == '0000-00-00 00:00:00' && $row['DATA_DESATIVADA'] == '0000-00-00 00:00:00' && ($_SESSION['modmed'] == 1 || $_SESSION['adm_user'] == 1)){
            $desativar = "<span style='float:left;padding-top:2px;'><a href='javascript:return false;' class='desativar-presc' presc='{$row['id']}'><center><img style='padding-left:10px;' src='../utils/excluir.png' width='16' title='Desativar Prescri&ccedil;&atilde;o'></center></a></span>";
        }else{
            $desativar = '';
        }
        if ( $status == 4) {

            if ($row['carater'] != 3   || $_SESSION['adm_user']==1) {
                if($row['carater'] != 11) {
                    if (isset($_SESSION['permissoes']['medico']['medico_prescricao']['medico_prescricao_usar_para_nova']) || $_SESSION['adm_user'] == 1) {
                        $p = "<a href='#'><img src='../utils/prescricao_16x16.png' title='Usar para nova Prescri&ccedil;&atilde;o' border='0' class='criar-presc' view='prescricoes' p='{$row['id']}' carater='{$row['carater']}' paciente='{$row['paciente']}' CodPaciente='{$row['CodPaciente']}' busca='true'></a>";
                    }
                }
                } else {
                $p = '';
                $edp = '';
            }
        }

        if(array_key_exists($row['id'], $procedimentosGtm)){
            $hasProcedimentoGtm = "<a target='_blank' href='/enfermagem/relatorios/?action=procedimentos-gtm-view&id={$row['id']}'><img src='/utils/estomago_24x24.png' width='16' title='Procedimentos em Gastrostomia' border='0'></a>";
        }

        if($row['DATA_DESATIVADA'] == '0000-00-00 00:00:00' && $row['solicitado'] == '0000-00-00 00:00:00' ){
            $statusPrescricao= 'Enviada p/ solicitação';
        }
        if($row['DATA_DESATIVADA'] == '0000-00-00 00:00:00' && $row['solicitado'] != '0000-00-00 00:00:00' ){
            $statusPrescricao= 'Solicitada';
        }
        if($row['DATA_DESATIVADA'] != '0000-00-00 00:00:00'){
            $statusPrescricao= 'Cancelada';
        }

        echo "<tr bgcolor={$cor}>
      			<td>" . $row['tipo'] . "</td>
      			<td>{$row['sdata']}</td>
      			<td>{$row['medico']}{$compTipo} </td>
      			<td>{$row['inicio']} AT&Eacute; {$row['fim']}</td>
                        <td>{$statusPrescricao}</td>
      			<td>
      			    <a id='{$row['id']}'
      			        title='Pré-visualizar esta prescrição'
      			        module='medico'
      			        tipo=''
      			        tipo-prescricao='" . strip_tags($row['tipo']) . "'
      			        inicio='{$row['inicio']}'
      			        fim='{$row['fim']}'
      			        class='preview'>
      			        <img src='../utils/look_16x16.png' />
      			    </a>
      			    {$d}
                </td>
      			<td>{$p} {$hasProcedimentoGtm}</td>

                <td>{$desativar}</td>

      			<!--<td>{$d} </td>-->
      		</tr>";
    }
    echo "</table>";
}

function desativar_prescricao($post) {
    extract($post, EXTR_OVERWRITE);
    if (isset($presc)) {
        $presc = anti_injection($presc, 'numerico');
        if (mysql_query("UPDATE prescricoes SET lockp = '9999-12-31 23:59:59', DATA_DESATIVADA = now(), DESATIVADA_POR ='{$_SESSION['id_user']}',STATUS=1 WHERE id = '$presc' "))
            echo '1';
        else
            echo '0';
    }
}

//! Envia um email para as enfermeiras caso o médico solicite.
function enviar_email_aviso($post) {
    extract($post, EXTR_OVERWRITE);
    $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
    $mail->IsSMTP(); // telling the class to use SMTP

    try {
        // $mail->Host       = "mail.yourdomain.com"; // SMTP server
//	  $mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
        $mail->SMTPAuth = true;                  // enable SMTP authentication
        $mail->SMTPSecure = "TLS";                 // sets the prefix to the servier
        $mail->Host = "smtp.mederi.com.br";      // sets GMAIL as the SMTP server
        $mail->Port = 587;                   // set the SMTP port for the GMAIL server
        $mail->Username = "avisos@mederi.com.br";  // GMAIL username
        $mail->Password = "2017avisosistema";            // GMAIL password
//	  $mail->AddReplyTo('name@yourdomain.com', 'First Last');
        $mail->AddAddress('enfplantonista@mederi.com.br', 'Avisos Enfermagem Mederi');
        $mail->SetFrom('avisos@mederi.com.br', 'Avisos de Sistema');
//	  $mail->AddReplyTo('name@yourdomain.com', 'First Last');
        $mail->Subject = 'AVISO';
        $mail->AltBody = 'Para ver essa mensagem, use um visualizador de email compatível com HTML'; // optional - MsgHTML will create an alternate automatically
        $solicitante = ucwords(strtolower($_SESSION["nome_user"]));
        $mail->MsgHTML("<b>Aviso de nova prescri&ccedil;&atilde;o</b><br/><b>Paciente: </b>{$p}<br/><b>Solicitante: </b>{$solicitante}.");
//	  $mail->AddAttachment('images/phpmailer.gif');      // attachment
//	  $mail->AddAttachment('images/phpmailer_mini.gif'); // attachment
        $enviado = $mail->Send();
        echo "Message Sent OK<p></p>\n";
        if ($enviado) {
            echo "E-mail enviado com sucesso!";
        } else {
            echo "N�o foi poss�vel enviar o e-mail.<br /><br />";
            echo "<b>Informa��es do erro:</b> <br />" . $mail->ErrorInfo;
        }
    } catch (phpmailerException $e) {
        echo $e->errorMessage(); //Pretty error messages from PHPMailer
    } catch (Exception $e) {
        echo $e->getMessage(); //Boring error messages from anything else!
    }
}

//! Salva uma prescrição.
function salvar_prescricao($post) {
    extract($post, EXTR_OVERWRITE);

    try {
        $dir = "salvas/" . $_SESSION["id_user"];
        if (!is_dir($dir))
            mkdir($dir);
        $filename = $dir . "/" . $paciente . ".htm";
        $arq = fopen($filename, "w");
        $escreve = fwrite($arq, $p);
        fclose($arq);

        echo "Salvo com sucesso!" . mkdir($dir);
    } catch (Exception $e) {
        echo "Falha ao gravar prescrição: ", $e->getMessage(), "\n";
    }
}

function nova_ficha_evolucao($post) {

    extract($post, EXTR_OVERWRITE);
    $usuario = $_SESSION["id_user"];
    $ur = $_SESSION["empresa_user"];
    $paciente = anti_injection($paciente, "literal");
    $sql = "SELECT nome, convenio, empresa FROM clientes WHERE idClientes = '{$paciente}'";
    $rs = mysql_query($sql);
    $rowPaciente = mysql_fetch_array($rs);
    $empresa = null;
    $convenio = null;
    $nomepaciente = '';
    if(count($rowPaciente) > 0) {
        $empresa = $rowPaciente['empresa'];
        $convenio = $rowPaciente['convenio'];
        $nomepaciente = $rowPaciente['nome'];
    }
    $capmed = anti_injection($capmed, "literal");
    $temperatura = anti_injection($temperatura, "literal");
    $peso_paciente = anti_injection($peso_paciente, "literal");
    $temperatura_sinal = anti_injection($temperatura_sinal, "literal");
    $temperatura_max = anti_injection($temperatura_max, "literal");
    $temperatura_min = anti_injection($temperatura_min, "literal");

    $oximetria_pulso_max = anti_injection($oximetria_pulso_max, "literal");
    $oximetria_pulso_min = anti_injection($oximetria_pulso_min, "literal");
    $tipo_oximetria_min = anti_injection($tipo_oximetria_min, "literal");
    $litros_min = anti_injection($litros_min, "literal");
    $via_oximetria_min = anti_injection($via_oximetria_min, "literal");
    $tipo_oximetria_max = anti_injection($tipo_oximetria_max, "literal");
    $litros_max = anti_injection($litros_max, "literal");
    $via_oximetria_max = anti_injection($via_oximetria_max, "literal");

    $pa_diastolica = anti_injection($pa_diastolica, "literal");
    $pa_diastolica_sinal = anti_injection($pa_diastolica_sinal, "literal");
    $pa_diastolica_min = anti_injection($pa_diastolica_min, "literal");
    $pa_diastolica_max = anti_injection($pa_diastolica_max, "literal");
    $pa_sistolica = anti_injection($pa_sistolica, "literal");
    $pa_sistolica_sinal = anti_injection($pa_sistolica_sinal, "literal");
    $pa_sistolica_max = anti_injection($pa_sistolica_max, "literal");
    $pa_sistolica_min = anti_injection($pa_sistolica_min, "literal");
    $fc = anti_injection($fc, "literal");
    $fc_sinal = anti_injection($fc_sinal, "literal");

    $fc_max = anti_injection($fc_max, "literal");
    $fc_min = anti_injection($fc_min, "literal");
    $fr = anti_injection($fr, "literal");
    $fr_sinal = anti_injection($fr_sinal, "literal");
    $fr_max = anti_injection($fr_max, "literal");
    $fr_min = anti_injection($fr_min, "literal");
    $estd_geral = anti_injection($estd_geral, "literal");
    $outro_respiratorio = anti_injection($outro_respiratorio, "literal");
    $glicemia_capilar_min = anti_injection($glicemia_capilar_min, "literal");
    $glicemia_capilar_max = anti_injection($glicemia_capilar_max, "literal");

    $oximetria_pulso = anti_injection($oximetria_pulso, "literal");
    $tipo_oximetria = anti_injection($tipo_oximetria, "literal");
    $litros = anti_injection($litros, "literal");
    $via_oximetria = anti_injection($via_oximetria, "literal");

    $oximetria_sinal = anti_injection($oximetria_sinal, "literal");

		$mod_home_care = anti_injection($modalidade, "literal");

    //$empresa = $_SESSION["empresa_principal"];

    if ($fc_sinal == 1) {
        $fc_msg = 'Alerta <font color="#FFD700">Amarelo</font> devido ao Frequ&ecirc;ncia Card&iacute;aca.';
    }
    if ($fc_sinal == 2) {
        $fc_msg = 'Alerta <font color="#EE0000">Vermelho</font> devido ao Frequ&ecirc;ncia Card&iacute;aca.';
    }

    if ($fr_sinal == 1) {
        $fr_msg = 'Alerta <font color="#FFD700">Amarelo</font> devido ao Frequ&ecirc;ncia Respirat&oacute;ria.';
    }
    if ($fr_sinal == 2) {
        $fr_msg = 'Alerta <font color="#EE0000">Vermelho</font> devido ao Frequ&ecirc;ncia Respirat&oacute;ria.';
    }

    if ($pa_sistolica_sinal == 1) {
        $pa_sistolica_msg = 'Alerta <font color="#FFD700">Amarelo</font> devido a Press&atilde;o Arterial Sist&oacute;lica.';
    }
    if ($pa_sistolica_sinal == 2) {
        $pa_sistolica_msg = 'Alerta <font color="#EE0000">Vermelho</font> devido a Press&atilde;o Arterial Sist&oacute;lica.';
    }

    if ($pa_diastolica_sinal == 1) {
        $pa_diastolica_msg = 'Alerta <font color="#FFD700">Amarelo</font> devido a Press&atilde;o Arterial Diast&oacute;lica.';
    }
    if ($pa_diastolica_sinal == 2) {
        $pa_diastolica_msg = 'Alerta <font color="#EE0000">Vermelho</font> devido a Press&atilde;o Arterial Diast&oacute;lica.';
    }

    if ($temperatura_sinal == 1) {
        $temperatura_msg = 'Alerta <font color="#FFD700">Amarelo</font> devido a Temperatura.';
    }
    if ($temperatura_sinal == 2) {
        $temperatura_msg = 'Alerta <font color="#EE0000">Vermelho</font> devido a Temperatura.';
    }

    if ($oximetria_sinal == 1) {
        $oximetria_msg = 'Alerta <font color="#FFD700">Amarelo</font> devido a Oximetria.';
    }
    if ($oximetria_sinal == 2) {
        $oximetria_msg = 'Alerta <font color="#EE0000">Vermelho</font> devido a Oximetria.';
    }

    $dor_sn = anti_injection($dor_sn, "literal");
    $mucosa = anti_injection($mucosa, "literal");
    $impressao = anti_injection($impressao, "literal");
    $mucosaobs = anti_injection($mucosaobs, "literal");
    $novo_exame = anti_injection($novo_exame, "literal");
    $esclerasobs = anti_injection($esclerasobs, "literal");
    $escleras = anti_injection($escleras, "literal");
    $plano_diagnostico = anti_injection($plano_diagnostico, "literal");
    $respiratorioobs = anti_injection($respiratorioobs, "literal");
    $respiratorio = anti_injection($respiratorio, "literal");
    $terapeutico = anti_injection($terapeutico, "literal");
    $data = anti_injection($data, "literal");
    $hora = anti_injection($hora, "literal");
    $data_evolucao = implode("-", array_reverse(explode("/", $data)));
    $hora_evolucao = anti_injection($hora, "literal");
    $data = $data_evolucao." ".$hora_evolucao;

    mysql_query("begin");
    $sql = <<<SQL
INSERT INTO fichamedicaevolucao (
	`ID`,
	`USUARIO_ID`,
	`empresa`,
	`plano`,
	`CAPMEDICA_ID`,
	`PACIENTE_ID`,
	`DATA`,
	`DATA_SISTEMA`,
	`MOD_HOME_CARE`,
	`PA_SISTOLICA_MIN`,
	`PA_SISTOLICA_MAX`,
	`PA_DIASTOLICA_MIN`,
	`PA_DIASTOLICA_MAX`,
	`FC_MIN`,
	`FC_MAX`,
	`FR_MIN`,
	`FR_MAX`,
	`TEMPERATURA_MIN`,
	`TEMPERATURA_MAX`,
	`PA_SISTOLICA_SINAL`,
	`PA_DIASTOLICA_SINAL`,
	`FC_SINAL`,
	`FR_SINAL`,
	`TEMPERATURA_SINAL`,
	`ESTADO_GERAL`,
	`MUCOSA`,
	`MUCOSA_OBS`,
	`ESCLERAS`,
	`ESCLERAS_OBS`,
	`PADRAO_RESPIRATORIO`,
	`PADRAO_RESPIRATORIO_OBS`,
	`PA_SISTOLICA`,
	`PA_DIASTOLICA`,
	`FC`,
	`FR`,
	`TEMPERATURA`,
	`PESO_PACIENTE`,
	`NOVOS_EXAMES`,
	`IMPRESSAO`,
	`PLANO_DIAGNOSTICO`,
	`PLANO_TERAPEUTICO`,
	`DOR`,
	`OUTRO_RESP`,
	`OXIMETRIA_PULSO`,
	`OXIMETRIA_PULSO_MAX`,
	`OXIMETRIA_PULSO_MIN`,
	`OXIMETRIA_PULSO_TIPO_MIN`,
	`OXIMETRIA_PULSO_LITROS_MIN`,
	`OXIMETRIA_PULSO_VIA_MIN`,
	`OXIMETRIA_PULSO_TIPO_MAX`,
	`OXIMETRIA_PULSO_LITROS_MAX`,
	`OXIMETRIA_PULSO_VIA_MAX`,
	`GLICEMIA_CAPILAR_MIN`,
	`GLICEMIA_CAPILAR_MAX`,
	`OXIMETRIA_PULSO_TIPO`,
	`OXIMETRIA_PULSO_LITROS`,
	`OXIMETRIA_PULSO_VIA`,
	`OXIMETRIA_SINAL`
)
VALUES
	(
		NULL,
		'{$usuario}',
		'{$empresa}',
		'{$convenio}',
		'{$capmed}',
		'{$paciente}',
		'{$data}',
		now(),
		'{$mod_home_care}',
		'{$pa_sistolica_min}',
		'{$pa_sistolica_max}',
		'{$pa_diastolica_min}',
		'{$pa_diastolica_max}',
		'{$fc_min}',
		'{$fc_max}',
		'{$fr_min}',
		'{$fr_max}',
		'{$temperatura_min}',
		'{$temperatura_max}',
		'{$pa_sistolica_sinal}',
		'{$pa_diastolica_sinal}',
		'{$fc_sinal}',
		'{$fr_sinal}',
		'{$temperatura_sinal}',
		'{$estd_geral}',
		'{$mucosa}',
		'{$mucosaobs}',
		'{$escleras}',
		'{$esclerasobs}',
		'{$respiratorio}',
		'{$respiratorioobs}',
		'{$pa_sistolica}',
		'{$pa_diastolica}',
		'{$fc}',
		'{$fr}',
		'{$temperatura}',
		'{$peso_paciente}',
		'{$novo_exame}',
		'{$impressao}',
		'{$plano_diagnostico}',
		'{$terapeutico}',
		'{$dor_sn}',
		'{$outro_respiratorio}',
		'{$oximetria_pulso}',
		'{$oximetria_pulso_max}',
		'{$oximetria_pulso_min}',
		'{$tipo_oximetria_min}',
		'{$litros_min}',
		'{$via_oximetria_min}',
		'{$tipo_oximetria_max}',
		'{$litros_max}',
		'{$via_oximetria_max}',
		'{$glicemia_capilar_min}',
		'{$glicemia_capilar_max}',
		'{$tipo_oximetria}',
		'{$litros}',
		'{$via_oximetria}',
		'{$oximetria_sinal}'
	)
SQL;

    $r = mysql_query($sql);
    if (!$r) {
        mysql_query("rollback");
        //echo "ERRO1: Tente mais tarde!";
        echo "ERRO1: ";
        //. mysql_error();
        return;
    }
    $idfichaevolucao = mysql_insert_id();
    savelog(mysql_escape_string(addslashes($sql)));
    ///problemas ativos
    $problemas = explode("$", $probativos);

    foreach ($problemas as $prob) {
        if ($prob != "" && $prob != null) {
            $i = explode("#", $prob);
            $cid = anti_injection($i[0], "literal");
            $desc = anti_injection($i[1], "literal");
            $status = anti_injection($i[2], "numerico");
            $obs = anti_injection($i[3], "literal");

            $sql = "INSERT INTO problemaativoevolucao VALUES (NULL,'{$idfichaevolucao}','{$cid}','{$desc}','{$status}','{$obs}',now()) ";
            $r = mysql_query($sql);
            if (!$r) {
                mysql_query("rollback");
                echo "ERRO2: Tente mais tarde!";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }
    ///problemas ativos fim
    savelog(mysql_escape_string(addslashes($sql)));
    ///problemas novos
   
    $problemasnovos = explode("$", $novosprobativos);
    foreach ($problemasnovos as $prob1) {
        if ($prob1 != "" && $prob1 != null) {
            $i = explode("#", $prob1);
            $cid = anti_injection($i[0], "literal");
            $desc = anti_injection($i[1], "literal");
            $obs = anti_injection($i[2], "literal");

            $sql = "INSERT INTO problemaativoevolucao VALUES (NULL,'{$idfichaevolucao}','{$cid}','{$desc}','0','{$obs}',now()) ";
            $r = mysql_query($sql);
            if (!$r) {
                mysql_query("rollback");
                echo "ERRO3: Tente mais tarde!";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }
    ///fim problemas novos

    savelog(mysql_escape_string(addslashes($sql)));

	//Alergia medicamentosa
	$medicamentos_alergia_ativos = explode("$", $medicamentos_alergia_ativos);
	$medicamentos_alergia_inativos = explode("$", $medicamentos_alergia_inativos);
	atualizarAlergiasMedicamentosas($medicamentos_alergia_ativos, $medicamentos_alergia_inativos, $paciente, $usuario);

    ///exames segmentar evolucao
    $exame = explode("$", $exames);
    foreach ($exame as $ex) {
        if ($ex != "" && $ex != null) {
            $i = explode("#", $ex);
            $id = anti_injection($i[0], "literal");
            $val = anti_injection($i[1], "numerico");
            $obs = anti_injection($i[2], "literal");


            $sql = "INSERT INTO examesegmentarevolucao VALUES (NULL,'{$idfichaevolucao}','{$id}','{$val}','{$obs}') ";
            $r = mysql_query($sql);
            if (!$r) {
                mysql_query("rollback");
                echo "ERRO4: Tente mais tarde!";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }
    ////exames segmentar evolucao

    savelog(mysql_escape_string(addslashes($sql)));
    ///dor
 
    $dor = explode("$", $dor123);
    foreach ($dor as $dores) {
        if ($dores != "" && $dores != null) {
            $i = explode("#", $dores);
            $nome = anti_injection($i[0], "literal");
            $padrao_id = anti_injection($i[1], "numerico");
            $padrao = anti_injection($i[2], "literal");
            $escala = anti_injection($i[3], "numerico");

            $sql = "INSERT INTO dorfichaevolucao (`DESCRICAO`, `ESCALA`, `PADRAO`, `PADRAO_ID`, `FICHAMEDICAEVOLUCAO_ID`, `DATA`)VALUES ('{$nome}', '{$escala}', '{$padrao}', '{$padrao_id}', '{$idfichaevolucao}', now())";
            $r = mysql_query($sql);
            if (!$r) {
                mysql_query("ROLLBACK");
                echo "ERRO 5: Tente novamente mais tarde!";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    //Intercorrências
    $intercorrencia = explode("#", $intercorrencias);
    foreach ($intercorrencia as $interc) {
        if ($interc != "" && $interc != null) {

            $descricao = anti_injection($interc, "literal");

            $sql = "INSERT INTO intercorrenciasfichaevolucao (`fichamedicaevolucao_id`,`descricao`) VALUES ('{$idfichaevolucao}','{$descricao}')";
            $r = mysql_query($sql);
            if (!$r) {

                echo "ERRO: " . $err . " ";
                //. mysql_error();
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    savelog(mysql_escape_string(addslashes($sql)));
    $cat_sond = explode("#", $cat_sond);

    foreach ($cat_sond as $cat) {
        if ($cat != "") {
            $nome = anti_injection($cat, "literal");
            $sql = "INSERT INTO catetersondaevolucao VALUES (NULL,'{$idfichaevolucao}','{$nome}') ";
            $r = mysql_query($sql);
            if (!$r) {
                mysql_query("rollback");
                echo "ERRO5: Tente mais tarde!";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    if ($pa_sistolica_msg != '' || $pa_diastolica_msg != '' || $fc_msg != '' || $fr_msg != '' || $temperatura_msg != '' || $oximetria_msg != '') {
        enviar_email_alerta($pa_sistolica_msg, $pa_diastolica_msg, $fc_msg, $fr_msg, $temperatura_msg, $oximetria_msg, $nomepaciente, $ur,$paciente);
    }
    if(mysql_query("commit")) {
			enviarEmail($paciente, 7, $empresa);

			echo '1';
			//echo enviar_email_relatorio ($paciente, 7, $empresa);
		}
}

function editar_ficha_evolucao($post) {

	extract($post, EXTR_OVERWRITE);
	$usuario = $_SESSION["id_user"];
	$ur = $_SESSION["empresa_user"];
	$paciente = anti_injection($paciente, "literal");
	$sql = "select nome from clientes where idClientes ={$paciente}";
	$result = mysql_query($sql);
	$nomepaciente = mysql_result($result, 0);
	$capmed = anti_injection($capmed, "literal");
	$temperatura = anti_injection($temperatura, "literal");
	$peso_paciente = anti_injection($peso_paciente, "literal");
	$temperatura_sinal = anti_injection($temperatura_sinal, "literal");
	$temperatura_max = anti_injection($temperatura_max, "literal");
	$temperatura_min = anti_injection($temperatura_min, "literal");

	$oximetria_pulso_max = anti_injection($oximetria_pulso_max, "literal");
	$oximetria_pulso_min = anti_injection($oximetria_pulso_min, "literal");
	$tipo_oximetria_min = anti_injection($tipo_oximetria_min, "literal");
	$litros_min = anti_injection($litros_min, "literal");
	$via_oximetria_min = anti_injection($via_oximetria_min, "literal");
	$tipo_oximetria_max = anti_injection($tipo_oximetria_max, "literal");
	$litros_max = anti_injection($litros_max, "literal");
	$via_oximetria_max = anti_injection($via_oximetria_max, "literal");

	$pa_diastolica = anti_injection($pa_diastolica, "literal");
	$pa_diastolica_sinal = anti_injection($pa_diastolica_sinal, "literal");
	$pa_diastolica_min = anti_injection($pa_diastolica_min, "literal");
	$pa_diastolica_max = anti_injection($pa_diastolica_max, "literal");
	$pa_sistolica = anti_injection($pa_sistolica, "literal");
	$pa_sistolica_sinal = anti_injection($pa_sistolica_sinal, "literal");
	$pa_sistolica_max = anti_injection($pa_sistolica_max, "literal");
	$pa_sistolica_min = anti_injection($pa_sistolica_min, "literal");
	$fc = anti_injection($fc, "literal");
	$fc_sinal = anti_injection($fc_sinal, "literal");

	$fc_max = anti_injection($fc_max, "literal");
	$fc_min = anti_injection($fc_min, "literal");
	$fr = anti_injection($fr, "literal");
	$fr_sinal = anti_injection($fr_sinal, "literal");
	$fr_max = anti_injection($fr_max, "literal");
	$fr_min = anti_injection($fr_min, "literal");
	$estd_geral = anti_injection($estd_geral, "literal");
	$outro_respiratorio = anti_injection($outro_respiratorio, "literal");
	$glicemia_capilar_min = anti_injection($glicemia_capilar_min, "literal");
	$glicemia_capilar_max = anti_injection($glicemia_capilar_max, "literal");

	$oximetria_pulso = anti_injection($oximetria_pulso, "literal");
	$tipo_oximetria = anti_injection($tipo_oximetria, "literal");
	$litros = anti_injection($litros, "literal");
	$via_oximetria = anti_injection($via_oximetria, "literal");

	$oximetria_sinal = anti_injection($oximetria_sinal, "literal");

	$mod_home_care = anti_injection($modalidade, "literal");

	$empresa = $_SESSION["empresa_principal"];

	if ($fc_sinal == 1) {
		$fc_msg = 'Alerta <font color="#FFD700">Amarelo</font> devido ao Frequ&ecirc;ncia Card&iacute;aca.';
	}
	if ($fc_sinal == 2) {
		$fc_msg = 'Alerta <font color="#EE0000">Vermelho</font> devido ao Frequ&ecirc;ncia Card&iacute;aca.';
	}

	if ($fr_sinal == 1) {
		$fr_msg = 'Alerta <font color="#FFD700">Amarelo</font> devido ao Frequ&ecirc;ncia Respirat&oacute;ria.';
	}
	if ($fr_sinal == 2) {
		$fr_msg = 'Alerta <font color="#EE0000">Vermelho</font> devido ao Frequ&ecirc;ncia Respirat&oacute;ria.';
	}

	if ($pa_sistolica_sinal == 1) {
		$pa_sistolica_msg = 'Alerta <font color="#FFD700">Amarelo</font> devido a Press&atilde;o Arterial Sist&oacute;lica.';
	}
	if ($pa_sistolica_sinal == 2) {
		$pa_sistolica_msg = 'Alerta <font color="#EE0000">Vermelho</font> devido a Press&atilde;o Arterial Sist&oacute;lica.';
	}

	if ($pa_diastolica_sinal == 1) {
		$pa_diastolica_msg = 'Alerta <font color="#FFD700">Amarelo</font> devido a Press&atilde;o Arterial Diast&oacute;lica.';
	}
	if ($pa_diastolica_sinal == 2) {
		$pa_diastolica_msg = 'Alerta <font color="#EE0000">Vermelho</font> devido a Press&atilde;o Arterial Diast&oacute;lica.';
	}

	if ($temperatura_sinal == 1) {
		$temperatura_msg = 'Alerta <font color="#FFD700">Amarelo</font> devido a Temperatura.';
	}
	if ($temperatura_sinal == 2) {
		$temperatura_msg = 'Alerta <font color="#EE0000">Vermelho</font> devido a Temperatura.';
	}

	if ($oximetria_sinal == 1) {
		$oximetria_msg = 'Alerta <font color="#FFD700">Amarelo</font> devido a Oximetria.';
	}
	if ($oximetria_sinal == 2) {
		$oximetria_msg = 'Alerta <font color="#EE0000">Vermelho</font> devido a Oximetria.';
	}

	$dor_sn = anti_injection($dor_sn, "literal");
	$mucosa = anti_injection($mucosa, "literal");
	$impressao = anti_injection($impressao, "literal");
	$mucosaobs = anti_injection($mucosaobs, "literal");
	$novo_exame = anti_injection($novo_exame, "literal");
	$esclerasobs = anti_injection($esclerasobs, "literal");
	$escleras = anti_injection($escleras, "literal");
	$plano_diagnostico = anti_injection($plano_diagnostico, "literal");
	$respiratorioobs = anti_injection($respiratorioobs, "literal");
	$respiratorio = anti_injection($respiratorio, "literal");
	$terapeutico = anti_injection($terapeutico, "literal");
	$data = anti_injection($data, "literal");
	$hora = anti_injection($hora, "literal");
	$data_evolucao = implode("-", array_reverse(explode("/", $data)));
	$hora_evolucao = anti_injection($hora, "literal");
	$data = $data_evolucao." ".$hora_evolucao;

	mysql_query("begin");
	$sql = <<<SQL
UPDATE fichamedicaevolucao SET
	`CAPMEDICA_ID` = '{$capmed}',
	`PACIENTE_ID` = '{$paciente}',
	`DATA` = '{$data}',
	`DATA_SISTEMA` = NOW(),
	`MOD_HOME_CARE` = '{$mod_home_care}',
	`PA_SISTOLICA_MIN` = '{$pa_sistolica_min}',
	`PA_SISTOLICA_MAX` = '{$pa_sistolica_max}',
	`PA_DIASTOLICA_MIN` = '{$pa_diastolica_min}',
	`PA_DIASTOLICA_MAX` = '{$pa_diastolica_max}',
	`FC_MIN` = '{$fc_min}',
	`FC_MAX` = '{$fc_max}',
	`FR_MIN` = '{$fr_min}',
	`FR_MAX` = '{$fr_max}',
	`TEMPERATURA_MIN` = '{$temperatura_min}',
	`TEMPERATURA_MAX` = '{$temperatura_max}',
	`PA_SISTOLICA_SINAL` = '{$pa_diastolica_sinal}',
	`PA_DIASTOLICA_SINAL` = '{$pa_sistolica_sinal}',
	`FC_SINAL` = '{$fc_sinal}',
	`FR_SINAL` = '{$fr_sinal}',
	`TEMPERATURA_SINAL` = '{$temperatura_sinal}',
	`ESTADO_GERAL` = '{$estd_geral}',
	`MUCOSA` = '{$mucosa}',
	`MUCOSA_OBS` = '{$mucosaobs}',
	`ESCLERAS` = '{$escleras}',
	`ESCLERAS_OBS` = '{$esclerasobs}',
	`PADRAO_RESPIRATORIO` = '{$respiratorio}',
	`PADRAO_RESPIRATORIO_OBS` = '{$respiratorioobs}',
	`PA_SISTOLICA` = '{$pa_sistolica}',
	`PA_DIASTOLICA` = '{$pa_diastolica}',
	`FC` = '{$fc}',
	`FR` = '{$fr}',
	`TEMPERATURA` = '{$temperatura}',
	`PESO_PACIENTE` = '{$peso_paciente}',
	`NOVOS_EXAMES` = '{$novo_exame}',
	`IMPRESSAO` = '{$impressao}',
	`PLANO_DIAGNOSTICO` = '{$plano_diagnostico}',
	`PLANO_TERAPEUTICO` = '{$terapeutico}',
	`DOR` = '{$dor_sn}',
	`OUTRO_RESP` = '{$outro_respiratorio}',
	`OXIMETRIA_PULSO` = '{$oximetria_pulso}',
	`OXIMETRIA_PULSO_MAX` = '{$oximetria_pulso_max}',
	`OXIMETRIA_PULSO_MIN` = '{$oximetria_pulso_min}',
	`OXIMETRIA_PULSO_TIPO_MIN` = '{$tipo_oximetria_min}',
	`OXIMETRIA_PULSO_LITROS_MIN` = '{$litros_min}',
	`OXIMETRIA_PULSO_VIA_MIN` = '{$via_oximetria_min}',
	`OXIMETRIA_PULSO_TIPO_MAX` = '{$tipo_oximetria_max}',
	`OXIMETRIA_PULSO_LITROS_MAX` = '{$litros_max}',
	`OXIMETRIA_PULSO_VIA_MAX` = '{$via_oximetria_max}',
	`GLICEMIA_CAPILAR_MIN` = '{$glicemia_capilar_min}',
	`GLICEMIA_CAPILAR_MAX` = '{$glicemia_capilar_max}',
	`OXIMETRIA_PULSO_TIPO` = '{$tipo_oximetria}',
	`OXIMETRIA_PULSO_LITROS` = '{$litros}',
	`OXIMETRIA_PULSO_VIA` = '{$via_oximetria}',
	`OXIMETRIA_SINAL` ='{$oximetria_sinal}'
WHERE
	ID = {$idevolucao}
SQL;

	$r = mysql_query($sql);
	if (!$r) {
		mysql_query("rollback");
		//echo "ERRO1: Tente mais tarde!";
		echo "ERRO1a: " . $sql;
		return;
	}
	savelog(mysql_escape_string(addslashes($sql)));
	///problemas ativos



	$sqlprobativo = "DELETE FROM problemaativoevolucao WHERE EVOLUCAO_ID = {$idevolucao}";
	$result = mysql_query($sqlprobativo);

	if (!$result) {
		mysql_query("ROLLBACK");
		echo "ERRO: Tente mais tarde!";
		return;
	}

	$problemas = explode("$", $probativos);

	foreach ($problemas as $prob) {
		if ($prob != "" && $prob != null) {
			$i = explode("#", $prob);
			$cid = anti_injection($i[0], "literal");
			$desc = anti_injection($i[1], "literal");
			$status = anti_injection($i[2], "numerico");
			$obs = anti_injection($i[3], "literal");

			$sql = "INSERT INTO problemaativoevolucao VALUES (NULL,'{$idevolucao}','{$cid}','{$desc}','{$status}','{$obs}',now()) ";
			$r = mysql_query($sql);
			if (!$r) {
				mysql_query("rollback");
				echo "ERRO2: Tente mais tarde!";
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}
	///problemas ativos fim
	savelog(mysql_escape_string(addslashes($sql)));
	///problemas novos

	$problemasnovos = explode("$", $novosprobativos);
	foreach ($problemasnovos as $prob1) {
		if ($prob1 != "" && $prob1 != null) {
			$i = explode("#", $prob1);
			$cid = anti_injection($i[0], "literal");
			$desc = anti_injection($i[1], "literal");
			$obs = anti_injection($i[2], "literal");

			$sql = "INSERT INTO problemaativoevolucao VALUES (NULL,'{$idevolucao}','{$cid}','{$desc}','0','{$obs}',now()) ";
			$r = mysql_query($sql);
			if (!$r) {
				mysql_query("rollback");
				echo "ERRO3: Tente mais tarde!";
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}
	///fim problemas novos

	savelog(mysql_escape_string(addslashes($sql)));

	//Alergia medicamentosa
	$medicamentos_alergia_ativos = explode("$", $medicamentos_alergia_ativos);
	$medicamentos_alergia_inativos = explode("$", $medicamentos_alergia_inativos);
	atualizarAlergiasMedicamentosas($medicamentos_alergia_ativos, $medicamentos_alergia_inativos, $paciente, $usuario);

	///exames segmentar evolucao
	$sqlexames = "DELETE FROM examesegmentarevolucao WHERE FICHA_EVOLUCAO_ID = {$idevolucao}";
	$result = mysql_query($sqlexames);

	if (!$result) {
		mysql_query("ROLLBACK");
		echo "ERRO: Tente mais tarde!";
		return;
	}

	$exame = explode("$", $exames);
	foreach ($exame as $ex) {
		if ($ex != "" && $ex != null) {
			$i = explode("#", $ex);
			$id = anti_injection($i[0], "literal");
			$val = anti_injection($i[1], "numerico");
			$obs = anti_injection($i[2], "literal");


			$sql = "INSERT INTO examesegmentarevolucao VALUES (NULL,'{$idevolucao}','{$id}','{$val}','{$obs}') ";
			$r = mysql_query($sql);
			if (!$r) {
				mysql_query("rollback");
				echo "ERRO4: Tente mais tarde!";
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}
	////exames segmentar evolucao

	savelog(mysql_escape_string(addslashes($sql)));
	///dor

	$sqldor = "DELETE FROM dorfichaevolucao WHERE FICHAMEDICAEVOLUCAO_ID = {$idevolucao}";
	$result = mysql_query($sqldor);

	if (!$result) {
		mysql_query("ROLLBACK");
		echo "ERRO: Tente mais tarde!";
		return;
	}

	$dor = explode("$", $dor123);
	foreach ($dor as $dores) {
		if ($dores != "" && $dores != null) {
			$i = explode("#", $dores);
			$nome = anti_injection($i[0], "literal");
			$padrao_id = anti_injection($i[1], "numerico");
			$padrao = anti_injection($i[2], "literal");
			$escala = anti_injection($i[3], "numerico");

			$sql = "INSERT INTO dorfichaevolucao (`DESCRICAO`, `ESCALA`, `PADRAO`, `PADRAO_ID`, `FICHAMEDICAEVOLUCAO_ID`, `DATA`)VALUES ('{$nome}', '{$escala}', '{$padrao}', '{$padrao_id}', '{$idevolucao}', now())";
			$r = mysql_query($sql);
			if (!$r) {
				mysql_query("ROLLBACK");
				echo "ERRO 5: Tente novamente mais tarde!";
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}

	//Intercorrências
	$sqlinter = "DELETE FROM intercorrenciasfichaevolucao WHERE fichamedicaevolucao_id = {$idevolucao}";
	$result = mysql_query($sqlinter);

	if (!$result) {
		mysql_query("ROLLBACK");
		echo "ERRO: Tente mais tarde!";
		return;
	}

	$intercorrencia = explode("#", $intercorrencias);
	foreach ($intercorrencia as $interc) {
		if ($interc != "" && $interc != null) {

			$descricao = anti_injection($interc, "literal");

			$sql = "INSERT INTO intercorrenciasfichaevolucao (`fichamedicaevolucao_id`,`descricao`) VALUES ('{$idevolucao}','{$descricao}')";
			$r = mysql_query($sql);
			if (!$r) {

				echo "ERRO: " . $err . " ";
				//. mysql_error();
				mysql_query("rollback");
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}

	savelog(mysql_escape_string(addslashes($sql)));

	// cateteres e sondas
	$sqlcatson = "DELETE FROM catetersondaevolucao WHERE FICHA_EVOLUCAO_ID = {$idevolucao}";
	$result = mysql_query($sqlcatson);

	if (!$result) {
		mysql_query("ROLLBACK");
		echo "ERRO: Tente mais tarde!";
		return;
	}

	$cat_sond = explode("#", $cat_sond);

	foreach ($cat_sond as $cat) {
		if ($cat != "") {
			$nome = anti_injection($cat, "literal");
			$sql = "INSERT INTO catetersondaevolucao VALUES (NULL,'{$idevolucao}','{$nome}') ";
			$r = mysql_query($sql);
			if (!$r) {
				mysql_query("rollback");
				echo "ERRO5: Tente mais tarde!";
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}

	if ($pa_sistolica_msg != '' || $pa_diastolica_msg != '' || $fc_msg != '' || $fr_msg != '' || $temperatura_msg != '' || $oximetria_msg != '') {
		enviar_email_alerta($pa_sistolica_msg, $pa_diastolica_msg, $fc_msg, $fr_msg, $temperatura_msg, $oximetria_msg, $nomepaciente, $ur,$paciente);
	}
	if(mysql_query("commit")) {
		enviarEmail($paciente, 7, $empresa);

		echo "1";
		//echo enviar_email_relatorio ($paciente, 7, $empresa);
	}

}

function prorrogacao($post) {
    echo $post;
}

function inativar_alergias($post) {

    extract($post, EXTR_OVERWRITE);

    $id = anti_injection($id, 'literal');
    $numero_tiss = anti_injection($numero_tiss, 'literal');

    if ($numero_tiss == 0) {//Outros
        $sql = "UPDATE `alergia_medicamentosa` SET `ATIVO`='n' WHERE `ID`={$id}";
    } else {//Medicamentos cadastrados no sistema
        $sql = "UPDATE `alergia_medicamentosa` SET `ATIVO`='n' WHERE `NUMERO_TISS`={$numero_tiss}";
    }

    $resultado = mysql_query($sql);
    if (!$resultado) {
        mysql_query("rollback");
        echo "ERRO: Tente mais tarde!";
        return 1;
    }
}

//inativar_alergias

function salvar_paciente($post) {
    extract($post, EXTR_OVERWRITE);
    $nascimento = anti_injection($nascimento, "literal");
    $nascimento = implode("-", array_reverse(explode("/", $nascimento)));
    $dataInternacao = anti_injection($dataInternacao, "literal");
    $dataInternacao = implode("-", array_reverse(explode("/", $dataInternacao)));

    $dataInternacao = $dataInternacao . ' ' . $horaInternacao;

    $nome = anti_injection($nome, 'literal');
    $codigo = anti_injection($codigo, 'literal');
    $diagnostico = anti_injection($diagnostico, 'literal');
    $local = anti_injection($local, 'literal');
    $solicitante = anti_injection($solicitante, 'literal');
    $sexo = anti_injection($sexo, 'literal');
    $csexo = anti_injection($csexo, 'literal');
    $rsexo = anti_injection($rsexo, 'literal');
    $resp = anti_injection($resp, 'literal');
    $relresp = anti_injection($relresp, 'literal');
    $cuid = anti_injection($cuid, 'literal');
    $relcuid = anti_injection($relcuid, 'literal');
    $endereco = anti_injection($endereco, 'literal');
    $especialidade = anti_injection($especialidade, 'literal');
    $acomp = anti_injection($acomp, 'literal');
    $bairro = anti_injection($bairro, 'literal');
    $referencia = anti_injection($referencia, 'literal');
    $leito = anti_injection($leito, 'literal');
    $telPosto = anti_injection($telPosto, 'literal');
    $contato = anti_injection($contato, 'literal');
    $telResponsavel = anti_injection($telResponsavel, 'literal');
    $telCuidador = anti_injection($telCuidador, 'literal');
    $enderecoInternacao = anti_injection($enderecoInternacao, 'literal');
    $bairroInternacao = anti_injection($bairroInternacao, 'literal');
    $telInternacao = anti_injection($telInternacao, 'literal');
    $telDomiciliar = anti_injection($telDomiciliar, 'literal');
    $id_cidade_und = anti_injection($id_cidade_und, 'literal');
    $id_cidade_domiciliar = anti_injection($id_cidade_domiciliar, 'literal');
    $id_cidade_internado = anti_injection($id_cidade_internado, 'literal');

    $referenciaInternacao = anti_injection($referenciaInternacao, 'literal');
    $especialidadeResponsavel = anti_injection($especialidadeResponsavel, 'literal');
    $especialidadeCuidador = anti_injection($especialidadeCuidador, 'literal');
    $end_und_origem = anti_injection($end_und_origem, 'literal');
    $num_matricula_convenio = anti_injection($num_matricula_convenio, 'literal');

    $sql = "INSERT INTO clientes (`nome`,`nascimento`,`sexo`,`codigo`,`convenio`,`dataInternacao`,`localInternacao`,`diagnostico`,`medicoSolicitante`,`empresa`,`responsavel`,`relresp`,`rsexo`,`cuidador`,`relcuid`,`csexo`,`espSolicitante`,`acompSolicitante`,`endereco`,`bairro`,`referencia`,LEITO,TEL_POSTO,PES_CONTATO_POSTO,TEL_DOMICILIAR,TEL_LOCAL_INTERNADO,TEL_RESPONSAVEL,TEL_CUIDADOR,END_INTERNADO,BAI_INTERNADO,REF_ENDERECO_INTERNADO,CUIDADOR_ESPECIALIDADE, RESPONSAVEL_ESPECIALIDADE,CIDADE_ID_UND,CIDADE_ID_DOM,CIDADE_ID_INT,END_UND_ORIGEM,NUM_MATRICULA_CONVENIO)
	  VALUES ('$nome','$nascimento','$sexo','$codigo','$convenio','$dataInternacao','$local','$diagnostico','$solicitante','$empresa','$resp','$relresp','$rsexo','$cuid','$relcuid','$csexo','$especialidade','$acomp','$endereco','$bairro','$referencia','$leito','$telPosto','$contato','$telDomiciliar','$telInternacao','$telResponsavel','$telCuidador','$enderecoInternacao','$bairroInternacao','$referenciaInternacao','$especialidadeCuidador', '$especialidadeResponsavel','$id_cidade_und','$id_cidade_domiciliar','$id_cidade_internado','$end_und_origem','$num_matricula_convenio') ";
    if (mysql_query($sql)) {
        savelog(mysql_escape_string(addslashes($sql)));
        //echo $sexo;
    }
    else
        echo "erro";
}

function editar_ficha_avaliacao($post) {

    extract($post, EXTR_OVERWRITE);
    $paciente = anti_injection($paciente, "numerico");
    $motivo = anti_injection($motivo, "literal");
    $capmedicaid = anti_injection($capmedicaid, "numerico");
    $cancer = anti_injection($cancer, "literal");
    $psiquiatrico = anti_injection($psiquiatrico, "literal");
    $neurologica = anti_injection($neurologica, "literal");
    $galucoma = anti_injection($glaucoma, "literal");
    $hepatopatia = anti_injection($hepatopatia, "literal");
    $obesidade = anti_injection($obesidade, "literal");
    $cardiopatia = anti_injection($cardiopatia, "literal");
    $dm = anti_injection($dm, "literal");
    $reumatologica = anti_injection($reumatologica, "literal");
    $has = anti_injection($has, "literal");
    $nefropatia = anti_injection($nefropatia, "literal");
    $pneumopatia = anti_injection($pneumopatia, "literal");

    $palergiamed = anti_injection($palergiamed, "literal");
    $alergiamed = anti_injection($alergiamed, "literal");
    $palergiaalim = anti_injection($palergiaalim, "literal");
    $alergiaalim = anti_injection($alergiaalim, "literal");

    $prelocohosp = anti_injection($prelocohosp, "literal");
    $prehigihosp = anti_injection($prehigihosp, "literal");
    $preconshosp = anti_injection($preconshosp, "literal");
    $prealimhosp = anti_injection($prealimhosp, "literal");
    $preulcerahosp = anti_injection($preulcerahosp, "literal");
    $prelocodomic = anti_injection($prelocodomic, "literal");
    $prehigidomic = anti_injection($prehigidomic, "literal");
    $preconsdomic = anti_injection($preconsdomic, "literal");
    $prealimdomic = anti_injection($prealimdomic, "literal");
    $preulceradomic = anti_injection($preulceradomic, "literal");
    $objlocomocao = anti_injection($objlocomocao, "literal");
    $objhigiene = anti_injection($objhigiene, "literal");
    $objcons = anti_injection($objcons, "literal");
    $objalimento = anti_injection($objalimento, "literal");
    $objulcera = anti_injection($objulcera, "literal");

    $historicointernacao = anti_injection($historicointernacao, "literal");
    $parecer = anti_injection($parecer, "numerico");
    $justparecer = anti_injection($justparecer, "literal");
    $usuario = $_SESSION["id_user"];
    $modalidade = anti_injection($modalidade, "numerico");
    $local_av = anti_injection($local_av, "numerico");
    $pre_justificativa = anti_injection($pre_justificativa, "literal");
    $plano_desmame = anti_injection($plano_desmame, "literal");
    $num_sugestao = anti_injection($num_sugestao, 'numerico');
    $classificacao_paciente = anti_injection($classificacao_paciente, "literal");

    mysql_query("begin");
    if ($modalidade == 2) {
        $sql = "UPDATE capmedica SET paciente = '{$paciente}', data = now(), motivo = '{$motivo}', alergiamed = '{$palergiamed}',
	alergiaalim = '{$palergiaalim}', tipoalergiamed = '{$alergiamed}', tipoalergiaalim = '{$alergiaalim}', hosplocomocao = '{$prelocohosp}',hosphigiene = '{$prehigihosp}',
	hospcons = '{$preconshosp}', hospalimentacao = '{$prealimhosp}', hospulcera = '{$preulcerahosp}', domlocomocao = '{$prelocodomic}', domhigiene = '{$prehigidomic}',
	domcons = '{$preconsdomic}', domalimentacao = '{$prealimdomic}', domulcera = '{$preulceradomic}',  objlocomocao = '{$objlocomocao}', objhigiene = '{$objhigiene}',
	objcons = '{$objcons}', objalimentacao = '{$objalimento}', objulcera = '{$objulcera}', qtdinternacoes = '{$qtdanteriores}', historicointernacao = '{$historicointernacao}',
	parecer = '{$parecer}', justificativa = '{$justparecer}', usuario = '{$usuario}', MODALIDADE = '{$modalidade}', LOCAL_AV= '{$local_av}', PRE_JUSTIFICATIVA= '{$pre_justificativa}', PLANO_DESMAME= '{$plano_desmame}', EQUIPAMENTO_OBS = '', CLASSIFICACAO_NEAD = '{$classificacao_paciente}' WHERE id = '{$capmedicaid}'";
    } else {
        $sql = "UPDATE capmedica SET paciente = '{$paciente}', data = now(), motivo = '{$motivo}', alergiamed = '{$palergiamed}',
	alergiaalim = '{$palergiaalim}', tipoalergiamed = '{$alergiamed}', tipoalergiaalim = '{$alergiaalim}', hosplocomocao = '{$prelocohosp}',hosphigiene = '{$prehigihosp}',
	hospcons = '{$preconshosp}', hospalimentacao = '{$prealimhosp}', hospulcera = '{$preulcerahosp}', domlocomocao = '{$prelocodomic}', domhigiene = '{$prehigidomic}',
	domcons = '{$preconsdomic}', domalimentacao = '{$prealimdomic}', domulcera = '{$preulceradomic}',  objlocomocao = '{$objlocomocao}', objhigiene = '{$objhigiene}',
	objcons = '{$objcons}', objalimentacao = '{$objalimento}', objulcera = '{$objulcera}', qtdinternacoes = '{$qtdanteriores}', historicointernacao = '{$historicointernacao}',
	parecer = '{$parecer}', justificativa = '{$justparecer}', usuario = '{$usuario}', MODALIDADE = '{$modalidade}', LOCAL_AV= '{$local_av}', PRE_JUSTIFICATIVA= '{$pre_justificativa}', PLANO_DESMAME= '{$plano_desmame}', EQUIPAMENTO_OBS = '{$equipamentos}', CLASSIFICACAO_NEAD = '{$classificacao_paciente}' WHERE id = '{$capmedicaid}'";
    }

    $r = mysql_query($sql);
    if (!$r) {
        mysql_query("rollback");
        echo "ERRO1: Tente mais tarde! '{$capmedicaid}'";
        return;
    }

    savelog(mysql_escape_string(addslashes($sql)));
    $sql = "UPDATE comorbidades SET cancer='{$cancer}',psiquiatrico='{$psiquiatrico}',
            neuro='{$neurologica}',glaucoma='{$glaucoma}',hepatopatia='{$hepatopatia}',obesidade='{$obesidade}',cardiopatia='{$cardiopatia}',dm='{$dm}',reumatologica='{$reumatologica}',
            has='{$has}',nefropatia='{$nefropatia}',pneumopatia='{$pneumopatia}' where capmed='{$capmedicaid}'";
    $r = mysql_query($sql);
    if (!$r) {
        mysql_query("rollback");
        echo "ERRO: Tente mais tarde!2";
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));
    $sql = "Delete FROM problemasativos where capmed='{$capmedicaid}' ";
    $r = mysql_query($sql);
    if (!$r) {
        mysql_query("rollback");
        echo "ERRO: Tente mais tarde!3";
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));
    $problemas = explode("$", $probativos);
    foreach ($problemas as $prob) {
        if ($prob != "" && $prob != null) {
            $i = explode("#", $prob);
            $cid = anti_injection($i[0], "literal");
            $desc = anti_injection($i[1], "literal");

            $sql = "INSERT INTO problemasativos VALUES ('{$capmedicaid}','{$cid}','{$desc}') ";
            $r = mysql_query($sql);
            if (!$r) {
                mysql_query("rollback");
                echo "ERRO: Tente mais tarde!";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }
    $sql = "Delete FROM score_paciente where ID_CAPMEDICA='{$capmedicaid}' ";
    $r = mysql_query($sql);
    if (!$r) {
        mysql_query("rollback");
        echo "ERRO: Tente mais tarde!5";
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));

    ////--score---//
    savelog(mysql_escape_string(addslashes($sql)));
    $score = explode("#", $score);
    foreach ($score as $sco) {
        if ($sco != '') {
            $sco = anti_injection($sco, "numerico");
            $sql = "INSERT INTO score_paciente (`ID_PACIENTE`, `ID_USUARIOS`, `DATA`, `ID_ITEM_SCORE`, `ID_CAPMEDICA`, `OBSERVACAO`) VALUES ('{$paciente}','{$usuario}',now(),'{$sco}','{$capmedicaid}','') ";
            $r = mysql_query($sql);
            if (!$r) {
                mysql_query("rollback");
                echo "ERRO: Tente mais tarde!6";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    $score_nead = explode("#", $score_nead);
    foreach ($score_nead as $sco_nead) {
        if ($sco_nead != '') {
            $sco_nead = anti_injection($sco_nead, "numerico");
            $sql = "INSERT INTO score_paciente (`ID_PACIENTE`, `ID_USUARIOS`, `DATA`, `ID_ITEM_SCORE`, `ID_CAPMEDICA`, `OBSERVACAO`) VALUES ('{$paciente}','{$usuario}',now(),'{$sco_nead}','{$capmedicaid}','') ";
            $r = mysql_query($sql);
            if (!$r) {
                mysql_query("rollback");
                echo "ERRO: Tente mais tarde! Problema no score NEAD";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }


    if(\App\Controllers\Score::excluirScorePaciente('capmedica', $capmedicaid)) {
        if (!empty($score_katz) && $score_katz != '') {
            $score_katz = \App\Controllers\Score::salvarScorePaciente($score_katz, $paciente, 'capmedica', $capmedicaid);
            if (!$score_katz) {
                mysql_query("rollback");
                echo "ERRO: Tente mais tarde! Problema no score Katz";
                return;
            }
        }

        if (!empty($score_nead2016) && $score_nead2016 != '') {
            $score_nead2016 = \App\Controllers\Score::salvarScorePaciente($score_nead2016, $paciente, 'capmedica', $capmedicaid);
            if (!$score_nead2016) {
                mysql_query("rollback");
                echo "ERRO: Tente mais tarde! Problema no score NEAD2016!";
                return;
            }
        }
    }


    $sql = "Delete FROM equipamentos_capmedica where CAPMEDICA_ID='{$capmedicaid}' ";
    $r = mysql_query($sql);
    if (!$r) {
        mysql_query("rollback");
        echo "ERRO: Tente mais tarde!7";
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));
    ////equipamentos
    if ($modalidade == 2) {
        $equipe = explode("$", $equipamentos);

        foreach ($equipe as $equip) {
            if ($equip != "" && $equip != null) {
                $i = explode("#", $equip);
                $cod_equip = anti_injection($i[0], "numerico");
                $obs_equip = anti_injection($i[1], "literal");
                $qtd_equip = anti_injection($i[2], "literal");

                $r = mysql_query("INSERT INTO equipamentos_capmedica (ID,COBRANCAPLANOS_ID,CAPMEDICA_ID,QTD,OBSERVACAO) VALUES (null,'{$cod_equip}','{$capmedicaid}','{$qtd_equip}','{$obs_equip}')");
                if (!$r) {
                    echo "ERRO: " . $err . " ";
                    //. mysql_error();
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }
        }
    }

    $sql = "DELETE FROM internacoescapmedica WHERE ID_CAPMEDICA='{$capmedicaid}' ";
    $r = mysql_query($sql);
    savelog(mysql_escape_string(addslashes($sql)));
    if (!$r) {
        mysql_query("rollback");
        echo "ERRO: Tente mais tarde!";
        return;
    }

    $internacao = explode("$", $internacoes);
    foreach ($internacao as $inter) {
        if ($inter != "" && $inter != null) {
            $nome = anti_injection($inter, "literal");
            $sql5 = "INSERT INTO internacoescapmedica VALUES ('','{$capmedicaid}','{$nome}') ";
            $r = mysql_query($sql5);
            if (!$r) {
                mysql_query("rollback");
                echo "ERRO: Tente mais tarde!";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    $cab_score = explode("$", $cab_score);

    foreach ($cab_score as $cab_sc) {
        if ($cab_sc != "" && $cab_sc != null) {
            $i = explode("#", $cab_sc);
            $cod_item = anti_injection($i[0], "numerico");
            $obs_item = anti_injection($i[1], "literal");
            //$qtd_equip = anti_injection($i[2],"literal");


            $r = mysql_query("INSERT INTO score_paciente (ID,ID_PACIENTE,ID_USUARIOS,DATA,ID_ITEM_SCORE,ID_CAPMEDICA,OBSERVACAO)VALUES (NULL,'{$paciente}','{$usuario}',now(),'{$cod_item}','{$capmedicaid}','{$obs_item}')");
            if (!$r) {
                echo "ERRO: " . $err . " ";
                //. mysql_error();
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    $status = 0;
    switch ($parecer) {
        case "0":
            $status = 1;
            break;
        case "1":
            $status = 2;
            break;
        case "2":
            $status = 2;
            break;
    }

	////--alergia-alimentos--//
	$alergia_alimentos_ativos = explode("$", $alergia_alimentar_ativos);
	$alergia_alimentos_inativos = explode("$", $alergia_alimentar_inativos);
	atualizarAlegiaAlimentar($alergia_alimentos_ativos, $alergia_alimentos_inativos, $paciente, $usuario);

	//Alergia medicamentosa
	$medicamentos_alergia_ativos = explode("$", $medicamentos_alergia_ativos);
	$medicamentos_alergia_inativos = explode("$", $medicamentos_alergia_inativos);
	atualizarAlergiasMedicamentosas($medicamentos_alergia_ativos, $medicamentos_alergia_inativos, $paciente, $usuario);

//    $sql = "UPDATE clientes SET status = '{$status}' WHERE idClientes = '{$paciente}'";
//    $r = mysql_query($sql);
//    if (!$r) {
//        mysql_query("rollback");
//        echo "ERRO: Tente mais tarde!10";
//        return;
//    }
//
//      $sql= "Insert
//             into historico_status_paciente
//             (USUARIO_ID,
//              PACIENTE_ID,
//              DATA,
//              OBSERVACAO,
//              STATUS_PACIENTE_ID,
//              CAPMEDICA_ID)
//               values
//               ('{$_SESSION['id_user']}',
//               '$paciente',
//                now(),
//                'Ficha de Avaliação Medica',
//                '$status',
//                '$capmedicaid')";
//               $r = mysql_query($sql);
//
//	if(!$r){
//		echo "ERRO: ao inserir na tabela de historico, tente mais tarde!";
//                mysql_query("rollback");
//		return;
//	}

    if($num_sugestao > 0){
        $sql = "UPDATE sugestoes_secmed SET data_resposta = NOW() WHERE id_capmedica = {$capmedicaid}  AND local_sugestao = 'fcav'";
        $rs = mysql_query($sql);

        $sql = "SELECT id FROM sugestoes_secmed WHERE id_capmedica = {$capmedicaid} AND local_sugestao = 'fcav'";
        $rs = mysql_query($sql);
        while($row = mysql_fetch_array($rs, MYSQL_ASSOC)){
            $sql_alteracao = "INSERT INTO sugestoes_alteracao (id_sugestao, alterado_por, data_alteracao)
                              VALUES ({$row['id']}, {$_SESSION['id_user']}, NOW())";
            $rs_alteracao = mysql_query($sql_alteracao);
        }
    }
    savelog(mysql_escape_string(addslashes($sql)));
    mysql_query("commit");
    //echo $equipamentos ;
    //$capmedicaid;
}

function editar_paciente($post) {
    extract($post, EXTR_OVERWRITE);
    $id = anti_injection($paciente, 'numerico');
    $nascimento = implode("-", array_reverse(explode("/", $nascimento)));
    $nome = anti_injection($nome, 'literal');
    $codigo = anti_injection($codigo, 'literal');
    $diagnostico = anti_injection($diagnostico, 'literal');
    $local = anti_injection($local, 'literal');
    $solicitante = anti_injection($solicitante, 'literal');
    $sexo = anti_injection($sexo, 'literal');
    $csexo = anti_injection($csexo, 'literal');
    $rsexo = anti_injection($rsexo, 'literal');
    $resp = anti_injection($resp, 'literal');
    $relresp = anti_injection($relresp, 'literal');
    $cuid = anti_injection($cuid, 'literal');
    $relcuid = anti_injection($relcuid, 'literal');
    $endereco = anti_injection($endereco, 'literal');
    $especialidade = anti_injection($especialidade, 'literal');
    $acomp = anti_injection($acomp, 'literal');
    $bairro = anti_injection($bairro, 'literal');
    $referencia = anti_injection($referencia, 'literal');
    $leito = anti_injection($leito, 'literal');
    $telPosto = anti_injection($telPosto, 'literal');
    $telContato = anti_injection($telContato, 'literal');
    $contato = anti_injection($contato, 'literal');
    $telResponsavel = anti_injection($telResponsavel, 'literal');
    $telCuidador = anti_injection($telCuidador, 'literal');
    $enderecoInternacao = anti_injection($enderecoInternacao, 'literal');
    $bairroInternacao = anti_injection($bairroInternacao, 'literal');
    $referenciaInternacao = anti_injection($referenciaInternacao, 'literal');
    $telInternacao = anti_injection($telInternacao, 'literal');
    $telDomiciliar = anti_injection($telDomiciliar, 'literal');
    $especialidadeCuidador = anti_injection($especialidadeCuidador, 'literal');
    $especialidadeResponsavel = anti_injection($especialidadeResponsavel, 'literal');
    $id_cidade_und = anti_injection($id_cidade_und, 'literal');
    $id_cidade_domiciliar = anti_injection($id_cidade_domiciliar, 'literal');
    $id_cidade_internado = anti_injection($id_cidade_internado, 'literal');
    $end_und_origem = anti_injection($end_und_origem, 'literal');
    $num_matricula_convenio = anti_injection($num_matricula_convenio, 'literal');
    $sql = "UPDATE
                clientes
            SET
                `nome` = '$nome',`nascimento` = '$nascimento',`sexo` = '$sexo',`codigo` = '$codigo',`convenio` = '$convenio',`localInternacao` = '$local',`diagnostico` = '$diagnostico',`medicoSolicitante` = '$solicitante',`empresa` = '$empresa', `responsavel` = '$resp', `relresp` = '$relresp', `rsexo` = '$rsexo',`cuidador` = '$cuid', `relcuid` = '$relcuid', `csexo` = '$csexo', `espSolicitante` = '$especialidade',`acompSolicitante` = '$acomp',`endereco` = '$endereco', `bairro` = '$bairro', `referencia` = '$referencia', `LEITO` = '$leito', `TEL_POSTO`= '$telPosto', `PES_CONTATO_POSTO`= '$contato', `TEL_DOMICILIAR`='$telDomiciliar',`TEL_LOCAL_INTERNADO`='$telInternacao',`TEL_RESPONSAVEL`= '$telResponsavel', `TEL_CUIDADOR`= '$telCuidador',`END_INTERNADO`='$enderecoInternacao',`BAI_INTERNADO`='$bairroInternacao', `REF_ENDERECO_INTERNADO`='$referenciaInternacao', `CUIDADOR_ESPECIALIDADE`= '$especialidadeCuidador',`RESPONSAVEL_ESPECIALIDADE`= '$especialidadeResponsavel', `CIDADE_ID_UND`='$id_cidade_und', `CIDADE_ID_INT`='$id_cidade_internado', `CIDADE_ID_DOM`='$id_cidade_domiciliar', `END_UND_ORIGEM`=' $end_und_origem', `NUM_MATRICULA_CONVENIO`='$num_matricula_convenio'
              WHERE
                  `idClientes` = '$id'";


    if (mysql_query($sql)) {
        savelog(mysql_escape_string(addslashes($sql)));
        echo 1;
    }
    else
        echo "erro";
}

function excluir_paciente($id) {

}

function del_acompanhamento($post) {
    extract($post, EXTR_OVERWRITE);
    $sql = "DELETE FROM equipePaciente WHERE id = '{$id}' ";
    if (mysql_query($sql)) {
        savelog(mysql_escape_string(addslashes($sql)));
        echo 1;
    }
    else
        echo "erro";
}

function salvar_ficha($post, $file) {
    extract($post, EXTR_OVERWRITE);
    $paciente = anti_injection($paciente, 'numerico');
    $tipoficha = anti_injection($tipo, 'numerico');
    $trava = 0;
    if (isset($post["trava"]))
        $trava = 1;
    $tipoficha = $tipo;
    $arquivo = $file["ficha"]["tmp_name"];
    $tamanho = $file["ficha"]["size"];
    $tipo = $file["ficha"]["type"];
    $nome = $file["ficha"]["name"];

    if ($arquivo != "none") {
        $fp = fopen($arquivo, "rb");
        $conteudo = fread($fp, $tamanho);
        $conteudo = addslashes($conteudo);
        fclose($fp);
        $sql = "INSERT INTO fichapaciente (`paciente`, `lockf`, `tipoficha`, `data`, `nome`, `tipo`, `size`, `content`) VALUES ('$paciente', '$trava' , '$tipoficha', now(), '$nome', '$tipo', '$tamanho', '$conteudo')";
        if (mysql_num_rows(mysql_query("SELECT 1 FROM fichapaciente WHERE `paciente` = '{$paciente}' AND `tipoficha` = '{$tipoficha}' "))) {
            $sql = "UPDATE fichapaciente SET `lockf` = '{$trava}', `data` = now(), `nome` = '{$nome}', `tipo` = '{$tipo}', `size` = '{$tamanho}', `content` = '{$conteudo}' WHERE `paciente` = '{$paciente}' AND `tipoficha` = '{$tipoficha}' ";
        }
        $r = mysql_query($sql);
        if ($r) {
            redireciona("-1");
        }
    }
}

function show_ficha($p, $t) {
    $p = anti_injection($p, 'numerico');
    $t = anti_injection($t, 'numerico');
    $sql = "SELECT tipo, content, nome FROM fichapaciente WHERE paciente='$p' AND tipoficha='$t' ";
    $result = mysql_query($sql);
    if ($row = mysql_fetch_array($result)) {
        header("Content-type: \"{$row['tipo']}\"");
        header("Content-Disposition: attachment; filename=\"{$row['nome']}\";");
        print $row['content'];
    } else {
        echo "<script>alert('sem ficha');</script>";
        redireciona("?view=paciente_med&act=show&id={$p}");
    }
}

function novo_score($post) {
    extract($post, EXTR_OVERWRITE);
    $paciente = anti_injection($paciente, "literal");
    $usuario = anti_injection($usuario, "numerico");
    $justificativa = anti_injection($justificativa, "literal");
    mysql_query("begin");

    $sql_id_maximo = "SELECT max(`ID_AVULSO`) as max FROM `score_avulso` WHERE `ID_PACIENTE` = '{$paciente}'";
    $r = mysql_query($sql_id_maximo);
    $ultimo_id = mysql_result($r, 0);
    $id_avulso = $ultimo_id + 1;

    //Itens do score


    if(!empty($score_katz) && $score_katz != '' || !empty($score_nead2016) && $score_nead2016 != '') {
        $r = mysql_query("INSERT INTO score_avulso
(ID,ID_PACIENTE,ID_USUARIOS,DATA,versao_score,CLASSIFICACAO_NEAD,ID_AVULSO)
 VALUES (NULL,'{$paciente}','{$usuario}',now(),'nead2016','{$classificacaoNead}','{$id_avulso}')");
        if (!$r) {
            echo "ERRO na observação: " . $err . " ";
            mysql_query("rollback");
            return;
        }
        $idScoreAvulso = $id_avulso;
        savelog(mysql_escape_string(addslashes($sql)));

        if(!empty($score_katz) && $score_katz != '') {
            $score_katz = \App\Controllers\Score::salvarScorePaciente($score_katz, $paciente, 'scoreavulso', $idScoreAvulso);
            if (!$score_katz) {
                mysql_query("rollback");
                echo "ERRO: Tente mais tarde! Problema no score Katz";
                return;
            }
        }

        if(!empty($score_nead2016) && $score_nead2016 != '') {
            $score_nead2016 = \App\Controllers\Score::salvarScorePaciente($score_nead2016, $paciente, 'scoreavulso', $idScoreAvulso);
            if (!$score_nead2016) {
                mysql_query("rollback");
                echo "ERRO: Tente mais tarde! Problema no score NEAD2016!";
                return;
            }
        }



    }else{
        $score = explode("#", $score);
        foreach ($score as $sco) {
            if ($sco != '') {
                $sco = anti_injection($sco, "numerico");
                $sql = "INSERT INTO
                    score_avulso
                    (ID,ID_PACIENTE,ID_USUARIOS,DATA,ID_ITEM_SCORE,OBSERVACAO,ID_AVULSO,JUSTIFICATIVA,versao_score)
                    VALUES
                    (NULL,'{$paciente}','{$usuario}',now(),'{$sco}','', '{$id_avulso}', '{$justificativa}','petrobras') ";
                $r = mysql_query($sql);
                if (!$r) {
                    mysql_query("rollback");
                    echo "ERRO: Tente mais tarde!";
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }
        }
        //Cabeçalho
        $observacao = explode("$", $observacao);

        foreach ($observacao as $cab_sc) {
            if ($cab_sc != "" && $cab_sc != null) {
                $i = explode("#", $cab_sc);
                $cod_item = anti_injection($i[0], "literal");
                $obs_item = anti_injection($i[1], "literal");

                $r = mysql_query("INSERT INTO score_avulso
(ID,ID_PACIENTE,ID_USUARIOS,DATA,ID_ITEM_SCORE,OBSERVACAO,ID_AVULSO,JUSTIFICATIVA,versao_score)
 VALUES (NULL,'{$paciente}','{$usuario}',now(),'{$cod_item}','{$obs_item}','{$id_avulso}','','petrobras')");
                if (!$r) {
                    echo "ERRO na observação: " . $err . " ";
                    //. mysql_error();
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }
        }
    }



    mysql_query("commit");

    echo 1;
}

//novo_score

function remover_score($post) {
    extract($post, EXTR_OVERWRITE);
    $cod = anti_injection($cod, "literal");
    $tipo = anti_injection($tipo, "literal");

    /* if ($tipo == 1) {//Capmedica

      $sql = "DELETE FROM score_paciente WHERE ID_CAPMEDICA ='{$cod}' ";
      $r = mysql_query($sql);
      if(!$r){
      mysql_query("rollback");
      echo "ERRO: Tente mais tarde!";
      return;
      }
      savelog(mysql_escape_string(addslashes($sql)));

      }else if ($tipo == 0){//Avulso
     */
    $sql = "DELETE FROM score_avulso WHERE ID_AVULSO ='{$cod}' ";
    $r = mysql_query($sql);
    if (!$r) {
        mysql_query("rollback");
        echo "ERRO: Tente mais tarde!";
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));

    //}

    echo 1;
}

function nova_capmed($post) {
    extract($post, EXTR_OVERWRITE);
    $paciente = anti_injection($paciente, "numerico");

    $sql = "SELECT convenio, empresa FROM clientes WHERE idClientes = '{$paciente}'";
    $rs = mysql_query($sql);
    $rowPaciente = mysql_fetch_array($rs);
    $empresa = null;
    $convenio = null;
    if(count($rowPaciente) > 0) {
        $empresa = $rowPaciente['empresa'];
        $convenio = $rowPaciente['convenio'];
    }

    $motivo = anti_injection($motivo, "literal");

    $cancer = anti_injection($cancer, "literal");
    $psiquiatrico = anti_injection($psiquiatrico, "literal");
    $neurologica = anti_injection($neurologica, "literal");
    $galucoma = anti_injection($glaucoma, "literal");
    $hepatopatia = anti_injection($hepatopatia, "literal");
    $obesidade = anti_injection($obesidade, "literal");
    $cardiopatia = anti_injection($cardiopatia, "literal");
    $dm = anti_injection($dm, "literal");
    $reumatologica = anti_injection($reumatologica, "literal");
    $has = anti_injection($has, "literal");
    $nefropatia = anti_injection($nefropatia, "literal");
    $pneumopatia = anti_injection($pneumopatia, "literal");

    $palergiamed = anti_injection($palergiamed, "literal");
    $alergiamed = anti_injection($alergiamed, "literal");
    $palergiaalim = anti_injection($palergiaalim, "literal");
    $alergiaalim = anti_injection($alergiaalim, "literal");

    $prelocohosp = anti_injection($prelocohosp, "literal");
    $prehigihosp = anti_injection($prehigihosp, "literal");
    $preconshosp = anti_injection($preconshosp, "literal");
    $prealimhosp = anti_injection($prealimhosp, "literal");
    $preulcerahosp = anti_injection($preulcerahosp, "literal");
    $prelocodomic = anti_injection($prelocodomic, "literal");
    $prehigidomic = anti_injection($prehigidomic, "literal");
    $preconsdomic = anti_injection($preconsdomic, "literal");
    $prealimdomic = anti_injection($prealimdomic, "literal");
    $preulceradomic = anti_injection($preulceradomic, "literal");
    $objlocomocao = anti_injection($objlocomocao, "literal");
    $objhigiene = anti_injection($objhigiene, "literal");
    $objcons = anti_injection($objcons, "literal");
    $objalimento = anti_injection($objalimento, "literal");
    $objulcera = anti_injection($objulcera, "literal");
    $pre_justificativa = anti_injection($pre_justificativa, "literal");
    $plano_desmame = anti_injection($plano_desmame, "literal");

    $parecer = anti_injection($parecer, "numerico");
    $justparecer = anti_injection($justparecer, "literal");
    $usuario = $_SESSION["id_user"];
    $modalidade = anti_injection($modalidade, "numerico");
    $classificacao_paciente = anti_injection($classificacao_paciente, "literal");
    $local_av = anti_injection($local_av, "numerico");
    $empresa = $_SESSION["empresa_principal"];
    mysql_query("begin");
    if ($modalidade == 1) {
        $sql = "INSERT INTO capmedica VALUES (NULL,'{$paciente}', '{$empresa}', '{$convenio}', now(),'{$motivo}','{$palergiamed}',
	'{$palergiaalim}','{$alergiamed}','{$alergiaalim}','{$prelocohosp}','{$prehigihosp}',
	'{$preconshosp}','{$prealimhosp}','{$preulcerahosp}','{$prelocodomic}','{$prehigidomic}',
	'{$preconsdomic}','{$prealimdomic}','{$preulceradomic}','{$objlocomocao}','{$objhigiene}',
	'{$objcons}','{$objalimento}','{$objulcera}','','',
	'{$parecer}','{$justparecer}','{$usuario}','{$modalidade}','{$local_av}','{$pre_justificativa}','{$plano_desmame}','{$equipamentos}', '2016', '{$classificacao_paciente}')";
    } else {
        $sql = "INSERT INTO capmedica VALUES (NULL,'{$paciente}','{$empresa}', '{$convenio}', now(),'{$motivo}','{$palergiamed}',
	'{$palergiaalim}','{$alergiamed}','{$alergiaalim}','{$prelocohosp}','{$prehigihosp}',
	'{$preconshosp}','{$prealimhosp}','{$preulcerahosp}','{$prelocodomic}','{$prehigidomic}',
	'{$preconsdomic}','{$prealimdomic}','{$preulceradomic}','{$objlocomocao}','{$objhigiene}',
	'{$objcons}','{$objalimento}','{$objulcera}','','',
	'{$parecer}','{$justparecer}','{$usuario}','{$modalidade}','{$local_av}','{$pre_justificativa}','{$plano_desmame}','', '2016', '{$classificacao_paciente}')";
    }
    $r = mysql_query($sql);
    if (!$r) {
        mysql_query("rollback");
        echo "ERRO: Tente mais tarde!";
        return;
    }


  $idcapmedica = mysql_insert_id();
    savelog(mysql_escape_string(addslashes($sql)));
    $sql = "INSERT INTO comorbidades VALUES ('{$idcapmedica}','{$cancer}','{$psiquiatrico}',
	'{$neurologica}','{$glaucoma}','{$hepatopatia}','{$obesidade}','{$cardiopatia}','{$dm}','{$reumatologica}',
	'{$has}','{$nefropatia}','{$pneumopatia}') ";
    $r = mysql_query($sql);
    if (!$r) {
        mysql_query("rollback");
        echo "ERRO: Tente mais tarde!";
        return;
    }

    savelog(mysql_escape_string(addslashes($sql)));

    $problemas = explode("$", $probativos);
    foreach ($problemas as $prob) {
        if ($prob != "" && $prob != null) {
            $i = explode("#", $prob);
            $cid = anti_injection($i[0], "literal");
            $desc = anti_injection($i[1], "literal");
            $prob = anti_injection($prob, "literal");
            $sql = "INSERT INTO problemasativos VALUES ('{$idcapmedica}','{$cid}','{$desc}') ";
            $r = mysql_query($sql);
            if (!$r) {
                mysql_query("rollback");
                echo "ERRO: Tente mais tarde!";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    ////--numero de internacoes---//
    $internacao = explode("$", $internacoes);
    foreach ($internacao as $inter) {
        if ($inter != "" && $inter != null) {
            $nome = anti_injection($inter, "literal");
            $sql5 = "INSERT INTO internacoescapmedica VALUES ('','{$idcapmedica}','{$nome}') ";
            $r = mysql_query($sql5);
            if (!$r) {
                mysql_query("rollback");
                echo "ERRO: Tente mais tarde!";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    ////--score---//
    savelog(mysql_escape_string(addslashes($sql)));
    $score = explode("#", $score);
    foreach ($score as $sco) {
        if ($sco != '') {
            $sco = anti_injection($sco, "numerico");
            $sql = "INSERT INTO score_paciente (`ID_PACIENTE`, `ID_USUARIOS`, `DATA`, `ID_ITEM_SCORE`, `ID_CAPMEDICA`, `OBSERVACAO`) VALUES ('{$paciente}','{$usuario}',now(),'{$sco}','{$idcapmedica}','') ";
            $r = mysql_query($sql);
            if (!$r) {
                mysql_query("rollback");
                echo "ERRO: Tente mais tarde!1";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

     $score_nead = explode("#", $score_nead);
    foreach ($score_nead as $sco_nead) {
        if ($sco_nead != '') {
            $sco_nead = anti_injection($sco_nead, "numerico");
            $sql = "INSERT INTO score_paciente (`ID_PACIENTE`, `ID_USUARIOS`, `DATA`, `ID_ITEM_SCORE`, `ID_CAPMEDICA`, `OBSERVACAO`) VALUES ('{$paciente}','{$usuario}',now(),'{$sco_nead}','{$idcapmedica}','') ";
            $r = mysql_query($sql);
            if (!$r) {
                mysql_query("rollback");
                echo "ERRO: Tente mais tarde! Problema no score NEAD";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    if(!empty($score_katz) && $score_katz != '') {
        $score_katz = \App\Controllers\Score::salvarScorePaciente($score_katz, $paciente, 'capmedica', $idcapmedica);
        if (!$score_katz) {
            mysql_query("rollback");
            echo "ERRO: Tente mais tarde! Problema no score Katz";
            return;
        }
    }

    if(!empty($score_nead2016) && $score_nead2016 != '') {
        $score_nead2016 = \App\Controllers\Score::salvarScorePaciente($score_nead2016, $paciente, 'capmedica', $idcapmedica);
        if (!$score_nead2016) {
            mysql_query("rollback");
            echo "ERRO: Tente mais tarde! Problema no score NEAD2016!";
            return;
        }
    }

    if ($modalidade == 2) {
        $equipe = explode("$", $equipamentos);

        foreach ($equipe as $equip) {
            if ($equip != "" && $equip != null) {
                $i = explode("#", $equip);
                $cod_equip = anti_injection($i[0], "numerico");
                $obs_equip = anti_injection($i[1], "literal");
                $qtd_equip = anti_injection($i[2], "literal");

                $r = mysql_query("INSERT INTO equipamentos_capmedica (ID, COBRANCAPLANOS_ID, CAPMEDICA_ID,QTD,OBSERVACAO) VALUES (null,'{$cod_equip}','{$idcapmedica}','{$qtd_equip}','{$obs_equip}')");
                if (!$r) {
                    echo "ERRO: " . $err . " ";
                    //. mysql_error();
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }
        }
    }

    ///cabecario score
    savelog(mysql_escape_string(addslashes($sql)));
    $cab_score = explode("$", $cab_score);

    foreach ($cab_score as $cab_sc) {
        if ($cab_sc != "" && $cab_sc != null) {
            $i = explode("#", $cab_sc);
            $cod_item = anti_injection($i[0], "numerico");
            $obs_item = anti_injection($i[1], "literal");
            //$qtd_equip = anti_injection($i[2],"literal");


            $r = mysql_query("INSERT INTO score_paciente (`ID_PACIENTE`, `ID_USUARIOS`, `DATA`, `ID_ITEM_SCORE`, `ID_CAPMEDICA`, `OBSERVACAO`)
			VALUES ('{$paciente}','{$usuario}',now(),'{$cod_item}','{$idcapmedica}','{$obs_item}')");
            if (!$r) {
                echo "ERRO: " . $err . " ";
                //. mysql_error();
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    $status = 0;
    switch ($parecer) {
        case "0":
            $status = 1;
            break;
        case "1":
            $status = 2;
            break;
        case "2":
            $status = 2;
            break;
    }
//    $sql = "UPDATE clientes SET status = '{$status}' WHERE idClientes = '{$paciente}'";
//    $r = mysql_query($sql);
//    if (!$r) {
//        mysql_query("rollback");
//        echo "ERRO: Tente mais tarde!";
//        return;
//    }
//    savelog(mysql_escape_string(addslashes($sql)));
//      $sql= "Insert
//             into historico_status_paciente
//             (USUARIO_ID,
//              PACIENTE_ID,
//              DATA,
//              OBSERVACAO,
//              STATUS_PACIENTE_ID,
//              CAPMEDICA_ID)
//               values
//               ('{$_SESSION['id_user']}',
//               '$paciente',
//                now(),
//                'Ficha de Avaliação Medica',
//                '$status',
//                '$idcapmedica')";
//               $r = mysql_query($sql);
//
//	if(!$r){
//		echo "ERRO: ao inserir na tabela de historico, tente mais tarde!";
//		mysql_query("rollback");
//		return;
//	}
//	savelog(mysql_escape_string(addslashes($sql)));

	////--alergia-alimentos--//
	$alergia_alimentos_ativos = explode("$", $alergia_alimentar_ativos);
	$alergia_alimentos_inativos = explode("$", $alergia_alimentar_inativos);
	atualizarAlegiaAlimentar($alergia_alimentos_ativos, $alergia_alimentos_inativos, $paciente, $usuario);

	//Alergia medicamentosa
	$medicamentos_alergia_ativos = explode("$", $medicamentos_alergia_ativos);
	$medicamentos_alergia_inativos = explode("$", $medicamentos_alergia_inativos);
	atualizarAlergiasMedicamentosas($medicamentos_alergia_ativos, $medicamentos_alergia_inativos, $paciente, $usuario);

	mysql_query("commit");

    $gateway = enviarEmail($paciente, 6, $empresa);

    $env = Config::get('APP_ENV');
    $mensagem = utf8_decode($gateway->adapter->mail->html);
    $token = Config::get('TOKEN_HIPCHAT_AVALIACOES');
    $hip = new \App\Controllers\HipChatSismederi($token, $env);
    $roomId = 4706984;
    $hip->sendNotificationRoom($roomId, $mensagem);

    //enviar_email_relatorio($paciente,6,$empresa);
	echo $idcapmedica;
}

//nova_capmed

function show_status_paciente($post) {
    extract($post, EXTR_OVERWRITE);
    $cod = anti_injection($cod, 'numerico');
    $row = mysql_fetch_array(mysql_query("SELECT nome, status FROM `clientes` WHERE `idClientes` = '{$cod}' "));
    $s = $row['status'];
    $p = ucwords(strtolower($row['nome']));
    $var = "<p><b>Status do paciente {$p}:</b><br />";
    $r = mysql_query("SELECT * FROM statuspaciente");
    while ($row = mysql_fetch_array($r)) {
        $var .= "<input type='radio' name='status' s='{$row['id']}'  " . (($row['id'] == "{$s}") ? "checked" : "") . " />{$row['status']}<br/>";
    }
    echo $var;
}

function salvar_status_paciente($post) {
    extract($post, EXTR_OVERWRITE);
    $cod = anti_injection($cod, 'numerico');
    $pstatus = anti_injection($pstatus, 'numerico');
    $sql = "UPDATE clientes SET status = '{$pstatus}' WHERE idClientes = '{$cod}' ";
    $r = mysql_query($sql);
    if (!$r) {
        echo "ERRO: tente mais tarde!";
        return;
    }
    echo "1";
}

function consultar_itens_kit($post) {
    extract($post, EXTR_OVERWRITE);
    $sql = "SELECT
            concat(b.principio,' - ',b.apresentacao,'  Qtd ',ik.qtd) as itens,
            ik.NUMERO_TISS,b.principio,ik.qtd,b.tipo, ik.CATALOGO_ID
	FROM
            kitsmaterial as k inner join
            itens_kit as ik ON (k.idKit = ik.idKit) inner join
            catalogo as b ON (ik.CATALOGO_ID = b.ID)
	WHERE
            k.idKit= {$cod}
	ORDER BY k.nome";
    //echo "$sql";
    $result = mysql_query($sql);
    ////////////tipo pra kit ////////////
    $tipo = 15;
    ///$dados = "nome='"+nome+"' tipo='"+tipo+"' cod='"+cod+"' dose='"+qtd+"'frequencia='"+frequencia+"'um='"+um+"'inicio='"+inicio+"'fim='"+fim+"'obs='"+obs+"'";
    while ($row = mysql_fetch_array($result)) {
        $a[] = $row['cod'];
        $tipo_bras = $row['tipo'];

        //print $inicio;
        if ($row['CATALOGO_ID'] == $cod_ref) {
            $nome = $row['principio'];
            $cod_b = $row['NUMERO_TISS'];
            $catalogo_id = $row['CATALOGO_ID'];
            $qtd_item = $row['qtd'];

            $linha = $nome . ".  QTD: " . $qtd_item . ". Dosagem: " . $qtd . ". Frequncia: " . $nfrequencia . ". Per&iacute;odo: " . $inicio . " at&eacute; " . $fim . ". OBS: " . $obs . "  Hora:" . $hora;
            $dados = "nome='" . $nome . "' tipo='" . $tipo . "' qtd='{$qtd_item}' cod='" . $cod_b . "'dose='" . $qtd . "'frequencia='" . $frequencia . "'um='" . $um . "'inicio='" . $inicio . "'fim='" . $fim . "'obs='" . $obs . "'hora='" . $hora . "'cod_ref='" . $cod_ref . "'tipo_bras='" . $tipo_bras . "' catalogo_id='" . $catalogo_id . "'";

            echo "<tr bgcolor='#ffffff' cod='{$cod}' ><td><span style='margin-left:15px;' class= 'dados' {$dados} > - {$linha}</span></td></tr>";
        } else {
            $cod_b = $row['NUMERO_TISS'];
            $catalogo_id = $row['CATALOGO_ID'];
            $nome = $row['principio'];
            $qtd_item = $row['qtd'];
            $dados = "nome='" . $nome . "' tipo='" . $tipo . "' qtd='{$qtd_item}' cod='" . $cod_b . "'inicio='" . $inicio . "' fim='" . $fim . "'cod_ref='" . $cod_ref . "'tipo_bras='" . $tipo_bras . "' catalogo_id='" . $catalogo_id . "'";
            $linha = $nome . ".  QTD: " . $qtd_item . ". Per&iacute;odo: " . $inicio . " at&eacute; " . $fim;
            echo "<tr bgcolor='#ffffff' cod='{$cod}' ><td><span style='margin-left:15px;' class= 'dados' {$dados} > - {$linha}</span></td></tr>";
        }
    }
}

//////////////////////////////////Eriton 19/06/2013///////////////////////////////////////

function salvar_relatorio_alta($post) {
    extract($post, EXTR_OVERWRITE);
    $inicio = implode("-", array_reverse(explode("/", $inicio)));
    $fim = implode("-", array_reverse(explode("/", $fim)));
    $empresa = $_SESSION['empresa_principal'];
    mysql_query('BEGIN');

    $sql = "SELECT convenio, empresa FROM clientes WHERE idClientes = '{$id_paciente}'";
    $rs = mysql_query($sql);
    $rowPaciente = mysql_fetch_array($rs);
    $empresa = null;
    $convenio = null;
    if(count($rowPaciente) > 0) {
        $empresa = $rowPaciente['empresa'];
        $convenio = $rowPaciente['convenio'];
    }

    $sql = "INSERT into relatorio_alta_med (`USUARIO_ID`, `PACIENTE_ID`, `empresa`, `plano`, `DATA`, `INICIO`, `ESTADO_GERAL`, `MUCOSA`, `MUCOSA_OBS`, `ESCLERAS`, `ESCLERAS_OBS`, `PADRAO_RESPIRATORIO`, `PADRAO_RESPIRATORIO_OBS`, `PA_SISTOLICA`, `PA_DIASTOLICA`, `FC`, `FR`, `TEMPERATURA`, `DOR`, `OUTRO_RESP`, `OXIMETRIA_PULSO`, `OXIMETRIA_PULSO_TIPO`, `OXIMETRIA_PULSO_LITROS`, `OXIMETRIA_PULSO_VIA`,`FIM`)
     VALUES ('{$_SESSION["id_user"]}', '{$id_paciente}', '{$empresa}', '{$convenio}', now(), '{$inicio}', '{$estd_geral}', '{$mucosa}', '{$mucosaobs}', '{$escleras}', '{$esclerasobs}', '{$respiratorio}', '{$respiratorioobs}', '{$pa_sistolica}', '{$pa_diastolica}', '{$fc}', '{$fr}', '{$temperatura}', '{$dor_sn}', '{$outro_respiratorio}', '{$oximetria_pulso}', '{$tipo_oximetria}', '{$litros}', '{$via_oximetria}','{$fim}')";
    $r = mysql_query($sql);
    if (!$r) {
        mysql_query("ROLLBACK");
        echo "Erro ao salvar exames fisicos!";
        return;
    }
    $id_relatorio = mysql_insert_id();

    savelog(mysql_escape_string(addslashes($sql)));
    //Grava Dor
    $dor = explode("$", $dor123);
    foreach ($dor as $dores) {
        if ($dores != "" && $dores != null) {
            $i = explode("#", $dores);
            $nome = anti_injection($i[0], "literal");
            $padrao_id = anti_injection($i[1], "numerico");
            $padrao = anti_injection($i[2], "literal");
            $escala = anti_injection($i[3], "numerico");

            $sql2 = "INSERT INTO dor_relatorio_prorrogacao_med (`DESCRICAO`, `ESCALA`, `PADRAO`, `PADRAO_ID`, `RELATORIO_ALTA_MED_ID`, `DATA`)VALUES ('{$nome}', '{$escala}', '{$padrao}', '{$padrao_id}', '{$id_relatorio}', now())";
            $r = mysql_query($sql2);
            if (!$r) {
                mysql_query("ROLLBACK");
                echo "Erro ao salvar dor!";
                return;
            }
        }
    }

    //Grava problemas_ativos
        $probativo = explode("$", $probativos);
        foreach ($probativo as $prob) {
        if ($prob != "" && $prob != null) {
            $i = explode("#", $prob);
            $cod_probativo = anti_injection($i[0], "literal");
            $desc_probativo = anti_injection($i[1], "literal");
            $just_procedimento = anti_injection($i[2], "literal");

            $sql3 = "INSERT INTO `problemas_relatorio_prorrogacao_med`(`RELATORIO_ALTA_MED_ID`, `CID_ID`, `DESCRICAO`, `OBSERVACAO`, `DATA`)
            VALUES ('{$id_relatorio}', '{$cod_probativo}', '{$desc_probativo}', '{$just_procedimento}', now())";
            $r = mysql_query($sql3);
            if (!$r) {
                mysql_query("ROLLBACK");
                echo "Erro ao salvar problemas!";
                return;
            }
        }
    }

    //Grava encaminhamento
    $intercorrencia = explode("$", $intercorrencias);
    foreach ($intercorrencia as $inter) {
        if ($inter != "" && $inter != null) {
            $i = explode("#", $inter);
            $descricao = anti_injection($i[0], "literal");

            $sql4 = "INSERT INTO `intercorrenciasfichaevolucao` (`DESCRICAO`, `RELATORIOALTA_ID`)
                VALUES ('{$descricao}', '{$id_relatorio}') ";
            $r = mysql_query($sql4);
            if (!r) {
                mysql_query("ROLLBACK");
                echo "Erro ao salvar intercorrencias!";
                return;
            }
        }
    }

    //Grava antibiotico
    $antibiotico = explode("$", $antibioticos);
    foreach ($antibiotico as $med) {
        if ($med != "" && $med != null) {
            $i = explode("#", $med);
            $catalogo_id = anti_injection($i[0], "numerico");
            $numero_tiss = anti_injection($i[1], "numerico");
            $medicamento = anti_injection($i[2], "literal");

            $sql5 = "INSERT INTO `relatorio_alta_med_antibiotico` (`CATALOGO_ID`, `MEDICAMENTO`,`NUMERO_TISS`,`RELATORIO_ALTA_ID`)
                VALUES ('{$catalogo_id}', '{$medicamento}', '{$numero_tiss}','{$id_relatorio}') ";
            $r = mysql_query($sql5);
            if (!r) {
                mysql_query("ROLLBACK");
                echo "Erro ao salvar antibiotico!";
                return;
            }
        }
    }

    //Grava profissional
    $profissionais = explode("$", $profissional);
    foreach ($profissionais as $profi) {
        if ($profi != "" && $profi != null) {
            $i = explode("#", $profi);
            $cod_especialidade = anti_injection($i[0], "numerico");
            $especialidade = anti_injection($i[1], "literal");
            $frequencia = anti_injection($i[2], "numerico");
            $justificativa = anti_injection($i[3], "literal");
            $id_especialidade = anti_injection($i[4], "numerico");
            if ($id_especialidade > 0) {
                $id_especialidade = $id_especialidade;
            } else {
                $id_especialidade = 0;
            }

            $sql6 = "INSERT INTO profissionais_relatorio_prorrogacao_med (`RELATORIO_ALTA_MED_ID`, `CUIDADOSESPECIAIS_ID`, `DESCRICAO`, `JUSTIFICATIVA`, `FREQUENCIA_ID`,`ESPECIALIDADE_MEDICA_ID`)
            VALUES ('{$id_relatorio}', '{$cod_especialidade}', '{$especialidade}', '{$justificativa}', '{$frequencia}', '{$id_especialidade}')";
            $r = mysql_query($sql6);
            if (!$r) {
                mysql_query("ROLLBACK");

                echo "Erro ao salvar profissionais!";
                return;
            }
        }
    }



    mysql_query('commit');

		enviarEmail($id_paciente, 12, $empresa);

    //enviar_email_relatorio($id_paciente,9,$empresa);
    echo "1";
}


function salvar_relatorios_prorrogacao($post) {
    extract($post, EXTR_OVERWRITE);
    $inicio = implode("-", array_reverse(explode("/", $inicio)));
    $fim = implode("-", array_reverse(explode("/", $fim)));
    // $empresa = $_SESSION['empresa_principal'];

    $sql = "SELECT convenio, empresa FROM clientes WHERE idClientes = '{$id_paciente}'";
    $rs = mysql_query($sql);
    $rowPaciente = mysql_fetch_array($rs);
    $empresa = null;
    $convenio = null;
    if(count($rowPaciente) > 0) {
        $empresa = $rowPaciente['empresa'];
        $convenio = $rowPaciente['convenio'];
    }

    mysql_query('BEGIN');
    if ($deslocamento == TRUE) {
        $deslocamento = "S";
    } else {
        $deslocamento = "N";
    }
    $sql = "INSERT into relatorio_prorrogacao_med (`USUARIO_ID`, `EVOLUCAO_ID`, `PACIENTE_ID`, `empresa`, `plano`, `DATA`, `INICIO`, `FIM`, `ESTADO_GERAL`, `MUCOSA`, `MUCOSA_OBS`, `ESCLERAS`, `ESCLERAS_OBS`, `PADRAO_RESPIRATORIO`, `PADRAO_RESPIRATORIO_OBS`, `PA_SISTOLICA`, `PA_DIASTOLICA`, `FC`, `FR`, `TEMPERATURA`, `DOR`, `OUTRO_RESP`, `OXIMETRIA_PULSO`, `OXIMETRIA_PULSO_TIPO`, `OXIMETRIA_PULSO_LITROS`, `OXIMETRIA_PULSO_VIA`, `MOD_HOME_CARE`, `DESLOCAMENTO`, `DESLOCAMENTO_DISTANCIA`,justificativa, quadro_clinico, regime_atual, indicacao_tecnica, MODELO_NEAD, CLASSIFICACAO_NEAD)
	 VALUES ('{$_SESSION["id_user"]}', '{$id_evolucao}', '{$id_paciente}', '{$empresa}', '{$convenio}', now(), '{$inicio}', '{$fim}', '{$estd_geral}', '{$mucosa}', '{$mucosaobs}', '{$escleras}', '{$esclerasobs}', '{$respiratorio}', '{$respiratorioobs}', '{$pa_sistolica}', '{$pa_diastolica}', '{$fc}', '{$fr}', '{$temperatura}', '{$dor_sn}', '{$outro_respiratorio}', '{$oximetria_pulso}', '{$tipo_oximetria}', '{$litros}', '{$via_oximetria}', '{$modalidade}', '{$deslocamento}', '{$distancia}','{$justificativaProrrogacao}', '{$quadro_clinico}', '{$regime_atual}', '{$indicacao_tecnica}', '2016', '{$classificacao_paciente}')";

    $r = mysql_query($sql);
    if (!$r) {
        mysql_query("ROLLBACK");
        echo "ERRO: Tente mais tarde!";
        return;
    }
    $id_relatorio = mysql_insert_id();

    if(!empty($score_katz) && $score_katz != '') {
        $score_katz = \App\Controllers\Score::salvarScorePaciente($score_katz, $id_paciente, 'prorrogacao', $id_relatorio);
        if (!$score_katz) {
            mysql_query("rollback");
            echo "ERRO: Tente mais tarde! Problema no score Katz";
            return;
        }
    }

    if(!empty($score_nead2016) && $score_nead2016 != '') {
        $score_nead2016 = \App\Controllers\Score::salvarScorePaciente($score_nead2016, $id_paciente, 'prorrogacao', $id_relatorio);
        if (!$score_nead2016) {
            mysql_query("rollback");
            echo "ERRO: Tente mais tarde! Problema no score NEAD2016!";
            return;
        }
    }

    savelog(mysql_escape_string(addslashes($sql)));
    //Grava Dor
    $dor = explode("$", $dor123);
    foreach ($dor as $dores) {
        if ($dores != "" && $dores != null) {
            $i = explode("#", $dores);
            $nome = anti_injection($i[0], "literal");
            $padrao_id = anti_injection($i[1], "numerico");
            $padrao = anti_injection($i[2], "literal");
            $escala = anti_injection($i[3], "numerico");

            $sql2 = "INSERT INTO dor_relatorio_prorrogacao_med (`DESCRICAO`, `ESCALA`, `PADRAO`, `PADRAO_ID`, `RELATORIO_PRORROGACAO_MED_ID`, `DATA`)VALUES ('{$nome}', '{$escala}', '{$padrao}', '{$padrao_id}', '{$id_relatorio}', now())";
            $r = mysql_query($sql2);
            if (!$r) {
                mysql_query("ROLLBACK");
                echo "ERRO 2: Tente novamente mais tarde!";
                return;
            }
        }
    }
    //Grava profissional
    $profissionais = explode("$", $profissional);
    foreach ($profissionais as $profi) {
        if ($profi != "" && $profi != null) {
            $i = explode("#", $profi);
            $cod_especialidade = anti_injection($i[0], "numerico");
            $especialidade = anti_injection($i[1], "literal");
            $frequencia = anti_injection($i[2], "numerico");
            $justificativa = anti_injection($i[3], "literal");
            $justificativa_id = anti_injection($i[4], "numerico");
            $id_especialidade = anti_injection($i[5], "numerico");
            if ($id_especialidade > 0) {
                $id_especialidade = $id_especialidade;
            } else {
                $id_especialidade = 0;
            }

            $sql3 = "INSERT INTO profissionais_relatorio_prorrogacao_med (`RELATORIO_PRORROGACAO_MED_ID`, `CUIDADOSESPECIAIS_ID`, `DESCRICAO`, `JUSTIFICATIVA`, `FREQUENCIA_ID`,`ESPECIALIDADE_MEDICA_ID`,`JUSTIFICATIVA_ID`)
			VALUES ('{$id_relatorio}', '{$cod_especialidade}', '{$especialidade}', '{$justificativa}', '{$frequencia}', '{$id_especialidade}', '{$justificativa_id}')";
            $r = mysql_query($sql3);
            if (!$r) {
                mysql_query("ROLLBACK");

                echo "ERRO 3: Tente novamente mais tarde!";
                return;
            }
        }
    }

    //Grava procedimentos
    $procedimentos = explode("$", $procedimento);
    foreach ($procedimentos as $proced) {
        if ($proced != "" && $proced != null) {
            $i = explode("#", $proced);
            $desc_procedimento = anti_injection($i[0], "literal");
            $just_procedimento = anti_injection($i[1], "literal");
            $cod_procedimento = anti_injection($i[2], "numerico");


            $sql4 = "INSERT INTO procedimentos_relatorio_prorrogacao_med (`RELATORIO_PRORROGACAO_MED_ID`, `PROCEDIMENTOS_ID`, `DESCRICAO`, `JUSTIFICATIVA`)
      VALUES ('{$id_relatorio}', '{$cod_procedimento}', '{$desc_procedimento}', '{$just_procedimento}')";

            $r = mysql_query($sql4);

            if (!$r) {
                mysql_query("ROLLBACK");
                echo "ERRO 4: Tente novamente mais tarde!";
                return;
            }
        }
    }

//Grava encaminhamento
    $intercorrencia = explode("$", $intercorrencias);
    foreach ($intercorrencia as $inter) {
        if ($inter != "" && $inter != null) {
            $i = explode("#", $inter);
            $descricao = anti_injection($i[0], "literal");

            $sql7 = "INSERT INTO `intercorrenciasfichaevolucao` (`DESCRICAO`, `FICHAMEDICAPRORROGACAO_ID`)
                VALUES ('{$descricao}', '{$id_relatorio}') ";
            $r = mysql_query($sql7);
            if (!r) {
                mysql_query("ROLLBACK");
                echo "ERRO 7: Tente novamente mais tarde!";
                return;
            }
        }
    }

    $probativo = explode("$", $probativos);
    foreach ($probativo as $prob) {
        if ($prob != "" && $prob != null) {
            $i = explode("#", $prob);
            $cod_probativo = anti_injection($i[0], "literal");
            $desc_probativo = anti_injection($i[1], "literal");
            $just_procedimento = anti_injection($i[2], "literal");

            $sql5 = "INSERT INTO `problemas_relatorio_prorrogacao_med`(`RELATORIO_PRORROGACAO_MED_ID`, `CID_ID`, `DESCRICAO`, `OBSERVACAO`, `DATA`)
			VALUES ('{$id_relatorio}', '{$cod_probativo}', '{$desc_probativo}', '{$just_procedimento}', now())";
            $r = mysql_query($sql5);
            if (!$r) {
                mysql_query("ROLLBACK");
                echo "ERRO 5: Tente novamente mais tarde!";
                return;
            }
        }
    }

    mysql_query('commit');

		enviarEmail($id_paciente, 9, $empresa);

    //$rsEmail = enviar_email_relatorio($id_paciente,9,$empresa);
    echo "1";

}

//////////////////////////////////Fim Eriton 19/06/2013///////////////////////////////////////
//////////////////////////////////Eriton 02/08/2013///////////////////////////////////////
function editar_relatorios_prorrogacao($post) {
    extract($post, EXTR_OVERWRITE);
    $inicio = implode("-", array_reverse(explode("/", $inicio)));
    $fim = implode("-", array_reverse(explode("/", $fim)));
    mysql_query('BEGIN');
    if ($deslocamento == TRUE) {
        $deslocamento = "S";
    } else {
        $deslocamento = "N";
    }

		$sql = "UPDATE relatorio_prorrogacao_med
						SET
							EDITADO_POR = {$_SESSION['id_user']},
							STATUS = 'n'
						WHERE
							ID = {$id_relatorio}";
		$rs = mysql_query($sql);

		if (!$rs) {
			mysql_query("ROLLBACK");
			echo "ERRO: Tente mais tarde!";
			return;
		}

		$sql = "INSERT into relatorio_prorrogacao_med
						(	`USUARIO_ID`,
							`EVOLUCAO_ID`,
							`PACIENTE_ID`,
							 `DATA`,
							 `INICIO`,
							 `FIM`,
							 `ESTADO_GERAL`,
							 `MUCOSA`,
							 `MUCOSA_OBS`,
							 `ESCLERAS`,
							 `ESCLERAS_OBS`,
							 `PADRAO_RESPIRATORIO`,
							 `PADRAO_RESPIRATORIO_OBS`,
							 `PA_SISTOLICA`,
							 `PA_DIASTOLICA`,
							 `FC`,
							 `FR`,
							 `TEMPERATURA`,
							 `DOR`,
							 `OUTRO_RESP`,
							 `OXIMETRIA_PULSO`,
							 `OXIMETRIA_PULSO_TIPO`,
							 `OXIMETRIA_PULSO_LITROS`,
							 `OXIMETRIA_PULSO_VIA`,
							 `MOD_HOME_CARE`,
							 `DESLOCAMENTO`,
							 `DESLOCAMENTO_DISTANCIA`,
							 `PRORROGACAO_ORIGEM`,
							 justificativa,
							 quadro_clinico,
							 regime_atual,
							 indicacao_tecnica,
							 MODELO_NEAD,
							 CLASSIFICACAO_NEAD)
		 				VALUES
		 				(	'{$_SESSION["id_user"]}',
		 				'{$id_evolucao}',
		 				'{$id_paciente}',
		 				 NOW(),
		 				'{$inicio}',
		 				'{$fim}',
		 				'{$estd_geral}',
		 				'{$mucosa}',
		 				'{$mucosaobs}',
		 				'{$escleras}',
		 				'{$esclerasobs}',
		 				'{$respiratorio}',
		 				'{$respiratorioobs}',
		 				'{$pa_sistolica}',
		 				 '{$pa_diastolica}',
		 				 '{$fc}',
		 				 '{$fr}',
		 				 '{$temperatura}',
		 				 '{$dor_sn}',
		 				 '{$outro_respiratorio}',
		 				 '{$oximetria_pulso}',
		 				 '{$tipo_oximetria}',
		 				 '{$litros}',
		 				 '{$via_oximetria}',
		 				 '{$modalidade}',
		 				 '{$deslocamento}',
		 				 '{$distancia}',
		 				 '{$id_relatorio}',
		 				 '{$justificativaProrrogacao}',
		 				 '{$quadro_clinico}',
		 				 '{$regime_atual}',
		 				 '{$indicacao_tecnica}',
		 				 '2016',
		 				 '{$classificacao_paciente}')";
    $r = mysql_query($sql);
		$id_relatorio = mysql_insert_id();

    if (!$r) {
        mysql_query("ROLLBACK");
        echo "ERRO: Tente mais tarde!1";
        return;
    }
    if($score_nead != '{') {
        $sql_nead = "DELETE FROM score_paciente WHERE ID_RELATORIO = {$id_relatorio}";
        $result = mysql_query($sql_nead);

        $sql_nead = "INSERT INTO score_paciente
                            (`ID_PACIENTE`,
                             `ID_USUARIOS`,
                             `DATA`,
                             `ID_ITEM_SCORE`,
                             `OBSERVACAO`,
                             `ID_CAPMEDICA`,
                             `ID_AVALIACAO_ENFERMAGEM`,
                             `ID_RELATORIO`)
                  VALUES ";
        if ($total_score < 6) {
            if ($alta !== '') {
                $date = \DateTime::createFromFormat('Y-m-d', $alta);
                $alta = "PROGRAMAÇÃO DE ALTA EM " . $date->format('d/m/Y');
            } else
                $alta = "";
        } else {
            $alta = "";
        }
        foreach (json_decode($score_nead) as $cod_item => $peso) {
            $sql_nead .= "  ({$paciente}, {$_SESSION["id_user"]}, NOW(), '{$cod_item}', '{$alta}', '0', '0', {$id_relatorio}),";
        }
        $sql_nead = substr_replace($sql_nead, "", -1);
        $rs_nead = mysql_query($sql_nead);
        if (!$rs_nead) {
            mysql_query("ROLLBACK");
            echo "ERRO 1: Tente mais tarde!";
            return;
        }
        //FIM SCORE NEAD7

        savelog(mysql_escape_string(addslashes($sql)));
    }
    //Grava Dor
    $sql2 = "DELETE
            FROM
                dor_relatorio_prorrogacao_med
            WHERE
                `RELATORIO_PRORROGACAO_MED_ID` = '$id_relatorio'
            ";
    $r = mysql_query($sql2);
    if (!$r) {
        mysql_query("ROLLBACK");
        echo "ERRO 2: Tente novamente mais tarde!";
        return;
    }

    $dor = explode("$", $dor123);
    $dor = explode("$", $dor123);
    foreach ($dor as $dores) {
        if ($dores != "" && $dores != null) {
            $i = explode("#", $dores);
            $nome = anti_injection($i[0], "literal");
            $padrao_id = anti_injection($i[1], "numerico");
            $padrao = anti_injection($i[2], "literal");
            $escala = anti_injection($i[3], "numerico");

            $sql2 = "INSERT INTO dor_relatorio_prorrogacao_med (`DESCRICAO`, `ESCALA`, `PADRAO`, `PADRAO_ID`, `RELATORIO_PRORROGACAO_MED_ID`, `DATA`)VALUES ('{$nome}', '{$escala}', '{$padrao}', '{$padrao_id}', '{$id_relatorio}', now())";
            $r = mysql_query($sql2);
            if (!$r) {
                mysql_query("ROLLBACK");
                echo "ERRO 2: Tente novamente mais tarde!";
                return;
            }
        }
    }
    //Grava profissional
    $profissionais = explode("$", $profissional);
    $sql3 = "DELETE
                    FROM
                        profissionais_relatorio_prorrogacao_med
                    WHERE
                        `RELATORIO_PRORROGACAO_MED_ID` = '$id_relatorio'
                        ";
    $r = mysql_query($sql3);
    if (!$r) {
        mysql_query("ROLLBACK");
        echo "ERRO 3: Tente novamente mais tarde!";
        return;
    }
    foreach ($profissionais as $profi) {
        if ($profi != "" && $profi != null) {
            $i = explode("#", $profi);
            $cod_especialidade = anti_injection($i[0], "literal");
            $especialidade = anti_injection($i[1], "literal");
            $frequencia = anti_injection($i[2], "literal");
            $justificativa = anti_injection($i[3], "literal");
            $justificativa_id = anti_injection($i[4], "numerico");
            $id_especialidade = anti_injection($i[5], "numerico");
            if ($id_especialidade > 0) {
                $id_especialidade = $id_especialidade;
            } else {
                $id_especialidade = 0;
            }


            $sql3 = "INSERT INTO profissionais_relatorio_prorrogacao_med (`RELATORIO_PRORROGACAO_MED_ID`, `CUIDADOSESPECIAIS_ID`, `DESCRICAO`, `JUSTIFICATIVA`, `FREQUENCIA_ID`,`ESPECIALIDADE_MEDICA_ID`,JUSTIFICATIVA_ID)
			VALUES ('{$id_relatorio}', '{$cod_especialidade}', '{$especialidade}', '{$justificativa}', '{$frequencia}', '{$id_especialidade}','{$justificativa_id}')
                        ";
//            echo $sql3;
            $r = mysql_query($sql3);
            if (!$r) {
                mysql_query("ROLLBACK");
                echo "ERRO 4: Tente novamente mais tarde!";
                return;
            }
        }
    }


    $procedimentos = explode("$", $procedimento);
    $sql4 = "DELETE
                    FROM
                        procedimentos_relatorio_prorrogacao_med
                    WHERE
                        `RELATORIO_PRORROGACAO_MED_ID` = '$id_relatorio'
                        ";
    $r = mysql_query($sql4);
    if (!$r) {
        mysql_query("ROLLBACK");
        echo "ERRO 5: Tente novamente mais tarde!";
        return;
    }
    foreach ($procedimentos as $proced) {
        if ($proced != "" && $proced != null) {
            $i = explode("#", $proced);
            $desc_procedimento = anti_injection($i[0], "literal");
            $just_procedimento = anti_injection($i[1], "literal");
            $cod_procedimento = anti_injection($i[2], "literal");


            $sql4 = "INSERT INTO procedimentos_relatorio_prorrogacao_med (`RELATORIO_PRORROGACAO_MED_ID`, `PROCEDIMENTOS_ID`, `DESCRICAO`, `JUSTIFICATIVA`)
      VALUES ('{$id_relatorio}', '{$cod_procedimento}', '{$desc_procedimento}', '{$just_procedimento}')
              ";
            $r = mysql_query($sql4);
            if (!$r) {
                mysql_query("ROLLBACK");
                echo "ERRO 6: Tente novamente mais tarde!";
                return;
            }
        }
    }


    $probativo = explode("$", $probativos);
    $sql5 = "DELETE
                    FROM
                        problemas_relatorio_prorrogacao_med
                    WHERE
                        `RELATORIO_PRORROGACAO_MED_ID` = '$id_relatorio'
                        ";
    $r = mysql_query($sql5);
    if (!$r) {
        mysql_query("ROLLBACK");
        echo "ERRO 7: Tente novamente mais tarde!";
        return;
    }
    foreach ($probativo as $prob) {
        if ($prob != "" && $prob != null) {
            $i = explode("#", $prob);
            $cod_probativo = anti_injection($i[0], "literal");
            $desc_probativo = anti_injection($i[1], "literal");
            $just_procedimento = anti_injection($i[2], "literal");


            $sql5 = "INSERT INTO `problemas_relatorio_prorrogacao_med`(`RELATORIO_PRORROGACAO_MED_ID`, `CID_ID`, `DESCRICAO`, `OBSERVACAO`, `DATA`)
			VALUES ('{$id_relatorio}', '{$cod_probativo}', '{$desc_probativo}', '{$just_procedimento}', now())
                        ";
            $r = mysql_query($sql5);
            if (!$r) {
                mysql_query("ROLLBACK");
                echo "ERRO 8: Tente novamente mais tarde!";
                return;
            }
        }
    }


    //Grava Alteração de Serviço
    $servico = explode("$", $servicos);
    $sql6 = "DELETE
                    FROM
                        alterar_servicos_cliente
                    WHERE
                        `ID_ORIGEM` = '$id_relatorio'
                        ";
    $r = mysql_query($sql6);
    if (!$r) {
        mysql_query("ROLLBACK");
        echo "ERRO 9: Tente novamente mais tarde!";
        return;
    }
    foreach ($servico as $serv) {
        if ($serv != "" && $serv != null) {
            $i = explode("#", $serv);
            $descricao = anti_injection($i[0], "literal");
            $cod_servico = anti_injection($i[1], "literal");
            $tipo = anti_injection($i[2], "literal");

            $sql6 = "INSERT INTO alterar_servicos_cliente (`COD_SERVICO`, `DATA`, `TIPO`, `USUARIO_ID`, `ORIGEM`, `ID_ORIGEM`, `PACIENTE_ID`)
                VALUES ('{$cod_servico}', now(), '{$tipo}', '{$_SESSION["id_user"]}', '2', '{$id_relatorio}', '{$id_paciente}')";
            $r = mysql_query($sql6);

            if (!$r) {
                mysql_query("ROLLBACK");
                echo "ERRO 10: Tente novamente mais tarde!";
                return;
            }
        }
    }

//Grava encaminhamento
    $intercorrencia = explode("$", $intercorrencias);
    $sql6 = "DELETE
                    FROM
                        intercorrenciasfichaevolucao
                    WHERE
                        `FICHAMEDICAPRORROGACAO_ID` = '$id_relatorio'
                        ";
    $r = mysql_query($sql6);
    if (!$r) {
        mysql_query("ROLLBACK");
        echo "ERRO 11: Tente novamente mais tarde!";
        return;
    }
    foreach ($intercorrencia as $inter) {
        if ($inter != "" && $inter != null) {
            $i = explode("#", $inter);
            $descricao = anti_injection($i[0], "literal");

            $sql7 = "INSERT INTO `intercorrenciasfichaevolucao` (`DESCRICAO`, `FICHAMEDICAPRORROGACAO_ID`)
                VALUES ('{$descricao}', '{$id_relatorio}') ";
            $r = mysql_query($sql7);
            if (!r) {
                mysql_query("ROLLBACK");
                echo "ERRO 12: Tente novamente mais tarde!";
                return;
            }
        }
    }

    if(\App\Controllers\Score::excluirScorePaciente('prorrogacao', $id_relatorio)) {
        if (!empty($score_katz) && $score_katz != '') {
            $score_katz = \App\Controllers\Score::salvarScorePaciente($score_katz, $id_paciente, 'prorrogacao', $id_relatorio);
            if (!$score_katz) {
                mysql_query("rollback");
                echo "ERRO: Tente mais tarde! Problema no score Katz";
                return;
            }
        }

        if (!empty($score_nead2016) && $score_nead2016 != '') {
            $score_nead2016 = \App\Controllers\Score::salvarScorePaciente($score_nead2016, $id_paciente, 'prorrogacao', $id_relatorio);
            if (!$score_nead2016) {
                mysql_query("rollback");
                echo "ERRO: Tente mais tarde! Problema no score NEAD2016!";
                return;
            }
        }
    }

    mysql_query('commit');
    echo "1";
}


function salvar_intercorrencia_medica($post) {
    extract($post, EXTR_OVERWRITE);
    $inicio = implode("-", array_reverse(explode("/", $inicio)));
    $fim = implode("-", array_reverse(explode("/", $fim)));
    mysql_query('BEGIN');

    $sql = "SELECT convenio, empresa FROM clientes WHERE idClientes = '{$id_paciente}'";
    $rs = mysql_query($sql);
    $rowPaciente = mysql_fetch_array($rs);
    $empresa = null;
    $convenio = null;
    if(count($rowPaciente) > 0) {
        $empresa = $rowPaciente['empresa'];
        $convenio = $rowPaciente['convenio'];
    }

    $sql = " INSERT 
             INTO intercorrencia_med
             (id,
             `usuario_id`,
             `cliente_id`, 
             `empresa`, 
             `plano`, 
             `data`, 
             `tipo_ambulancia`,              
             `vaga_uti`,
             data_encaminhamento,
             hora,
             hospital)
	      VALUES 
              (null,
              '{$_SESSION["id_user"]}',"
              . "'{$id_paciente}',"
              . "'{$empresa}',"
              . "'{$convenio}',"
              . " now(),"
              . " '{$ambulancia}', "
              ."'{$terapia_intensiva}',"
                    . "'{$inicio}',
                    '{$hora}',
                    '{$hospital_intercorrencia}')";
       
    $r = mysql_query($sql);
    if (!$r) {
        mysql_query("ROLLBACK");
        echo "ERRO1: Tente mais tarde!";
        return;
    }
    $id_relatorio = mysql_insert_id();

    savelog(mysql_escape_string(addslashes($sql)));

    //Grava urgencias
    $clinico = explode("$", $quadro_clinico);
    foreach ($clinico as $clinicos) {
        if ($clinicos != "" && $clinicos != null) {
            $i = explode("#", $clinicos);
            $quadro_id = anti_injection($i[0], "numerico");
            $descricao = anti_injection($i[1], "literal");

            $sql2 = "INSERT INTO "
                    . "intercorrencia_med_quadro_clinico "
                    . "(id,"
                    . "`quadro_clinico_id`, "
                    . "`descricao`,"
                    . " `intercorrencia_med_id`, "
                    . "`data`) "
                    . "VALUES"
                    . " (null,"
                    . "'{$quadro_id}',"
                    . " '{$descricao}', "
                    . "'{$id_relatorio}',"
                    . " now())";
                     
            $r = mysql_query($sql2);
            if (!$r) {
                mysql_query("ROLLBACK");
                echo "ERRO 2: Tente novamente mais tarde!";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql2)));
        }
    }

    //Grava encaminhamento
    $encaminhamento = explode("$", $quadro_encaminhamento);
    foreach ($encaminhamento as $encaminhamentos) {
        if ($encaminhamentos != "" && $encaminhamentos != null) {
            $i = explode("#", $encaminhamentos);
            $encaminhamento_id = anti_injection($i[0], "numerico");
            $encaminhamento_descricao = anti_injection($i[1], "literal");

            $sql3 = "INSERT INTO intercorrencia_med_procedimento ("
                    ."id,"
                    . "`procedimentos_id`,"
                    . " `descricao`, "
                    . "`intercorrencia_med_id`, "
                    . "`data`)"
                    . "VALUES "
                    . "("
                    ."null,"
                    . "'{$encaminhamento_id}',"
                    . " '{$encaminhamento_descricao}', "
                    . "'{$id_relatorio}',"
                    . " now())";
                    
            $r = mysql_query($sql3);
            if (!$r) {
                mysql_query("ROLLBACK");
                echo "ERRO 3: Tente novamente mais tarde!";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql3)));
        }
    }
    $exame= explode("$", $exame_encaminhamento);
    foreach ($exame as $exam) {
        if ($exam != "" && $exam != null) {
            $i = explode("#", $exam);
            $exame_id = anti_injection($i[0], "numerico");
            $exame_descricao = anti_injection($i[1], "literal");

            $sql4 = "INSERT INTO intercorrencia_med_exame ("
                    ."id,"
                    . "`exame_medico_id`,"
                    . " `descricao`, "
                    . "`intercorrencia_med_id`, "
                    . "`data`)"
                    . "VALUES "
                    . "("
                    ."null,"
                    . "'{$exame_id}',"
                    . " '{$exame_descricao}', "
                    . "'{$id_relatorio}',"
                    . " now())";
                    
            $r = mysql_query($sql4);
            if (!$r) {
                mysql_query("ROLLBACK");
                echo "ERRO 4: Tente novamente mais tarde!";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql4)));
        }
    }

    $probativo = explode("$", $probativos);
    foreach ($probativo as $prob) {
       if ($prob != "" && $prob != null) {
            $i = explode("#", $prob);
            $cod_probativo = anti_injection($i[0], "literal");
            $desc_probativo = anti_injection($i[1], "literal");
           $just_procedimento = anti_injection($i[2], "literal");

           $sql5 = "INSERT INTO 
               `intercorrencia_med_problema`
               (`intercorrencia_med_id`,
               `cid_id`, 
               `descricao`, 
               `observacao`, 
               `data`)
			VALUES
                        ('{$id_relatorio}', "
                        . "'{$cod_probativo}', "
                        . "'{$desc_probativo}',"
                        . " '{$just_procedimento}', "
                        . "now())";
                      
           $r = mysql_query($sql5);
            if (!$r) {
                mysql_query("ROLLBACK");
                echo "ERRO 5: Tente novamente mais tarde!";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql5)));
       }
    }

    mysql_query('commit');
    echo $id_relatorio;
}

//////////////////////////////////Fim Eriton 02/08/2013///////////////////////////////////////


function salvar_obs($post) {
    echo "0";
    extract($post, EXTR_OVERWRITE);
    $cod = anti_injection($cod, 'numerico');
    $obs = anti_injection($obs, 'literal');
    $sql = "UPDATE principioativo SET DescricaoMedico = '{$obs}' WHERE id = '{$cod}' ";
    $r = mysql_query($sql);
    if (!$r) {
        echo "ERRO: tente mais tarde!";
        return;
    }
    echo "1";
}

//salvar_obs - Salvar Observações

function editar_produto_interno($post) {
    echo "0";
    extract($post, EXTR_OVERWRITE);
    $cod = anti_injection($cod, 'numerico');
    $obs = anti_injection($nome, 'literal');
    $sql = "UPDATE principioativo SET principio = '{$nome}' WHERE id = '{$cod}' and status = 0 and AddMederi = 1 ";
    $r = mysql_query($sql);
    if (!$r) {
        echo "ERRO: tente mais tarde!";
        return;
    }
    echo "1";
}

function enviar_email_alerta($pa_sistolica_msg, $pa_diastolica_msg, $fc_msg, $fr_msg, $temperatura_msg, $oximetria_msg, $nomepaciente, $ur, $paciente) {

    $pa_sistolica_msg = $pa_sistolica_msg;
    $paciente = $paciente;
    $pa_diastolica_msg = $pa_diastolica_msg;
    $fc_msg = $fc_msg;
    $fr_msg = $fr_msg;
    $temperatura_msg = $temperatura_msg;
    $oximetria_msg = $oximetria_msg;

    $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
    $mail->IsSMTP(); // telling the class to use SMTP

    try {
        // $mail->Host       = "mail.yourdomain.com"; // SMTP server
        //	  $mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
        $mail->SMTPAuth = true;                  // enable SMTP authentication
        //$mail->SMTPSecure = "TLS";                 // sets the prefix to the servier
        $mail->Host = "smtp.mederi.com.br";      // sets GMAIL as the SMTP server
        $mail->Port = 587;                   // set the SMTP port for the GMAIL server
        $mail->Username = "avisos@mederi.com.br";  // GMAIL username
        $mail->Password = "2017avisosistema";            // GMAIL password
        //	  $mail->AddReplyTo('name@yourdomain.com', 'First Last');

        $mail->AddAddress('avisos@mederi.com.br', "Alertas"); // Retorno da UR: (1)
        /*
          if($ur == '(1)'){//email por unidade regional

          $mail->AddAddress('coordmed.fsa@mederi.com.br', 'Coordenação Médica - Feira de Santana');
          $mail->AddAddress('coordenf.fsa@mederi.com.br', 'Coordenação de Enfermagem - Feira de Santana');

          } else if($ur == '(2)'){

          $mail->AddAddress('coordmed.salvador@mederi.com.br', 'Coordenação Médica - Salvador');
          $mail->AddAddress('coordenf.salvador@mederi.com.br', 'Coordenação de Enfermagem - Salvador');
          } else if($ur == '(4)'){
          $mail->AddAddress('coordmed.sulbahia@mederi.com.br', 'Coordenação Médica - Sul/Bahia');
          $mail->AddAddress('coordenf.sulbahia@mederi.com.br', 'Coordenação de Enfermagem - Sul/Bahia');

          } else if($ur == '(5)'){
          $mail->AddAddress('coordenf.alagoinhas@mederi.com.br', 'Coordenação de Enfermagem - Alagoinhas');

          } else if($ur == '(6)'){
          $mail->AddAddress('coordenf.rms@mederi.com.br', 'Coordenação de Enfermagem - RMS');

          } else if($ur == '(7)'){
          $mail->AddAddress('coordmed.sudoeste@mederi.com.br', 'Coordenação Médica - Sudoeste');
          $mail->AddAddress('coordenf.sudoeste@mederi.com.br', 'Avisos M&eacute;dicos Mederi');
          }
         */
        $mail->SetFrom('avisos@mederi.com.br', 'Avisos de Sistema');
        //	  $mail->AddReplyTo('name@yourdomain.com', 'First Last');
        $mail->Subject = 'TESTE - Alerta do Sistema de Gerenciamento da Mederi';
        $mail->AltBody = 'Para ver essa mensagem, use um visualizador de email compat&iacute;vel com HTML'; // optional - MsgHTML will create an alternate automatically
        $solicitante = ucwords(strtolower($_SESSION["nome_user"]));
        $mail->MsgHTML("<b>Aviso de alerta de visita</b> <b>Paciente: </b>{$nomepaciente}<br/><b>Solicitante: </b>{$solicitante}<br> {$pa_sistolica_msg} {$pa_diastolica_msg} {$fc_msg} {$fr_msg} {$temperatura_msg} {$oximetria_msg} </br>");
        //	  $mail->AddAttachment('images/phpmailer.gif');      // attachment
        //	  $mail->AddAttachment('images/phpmailer_mini.gif'); // attachment
        $enviado = $mail->Send();

        /* if ($enviado) {
          echo "E-mail enviado com sucesso!";
          window.location.href='?adm=paciente&act=listfichaevolucao&id='{$paciente};
          } else {
          echo "N�o foi poss�vel enviar o e-mail.<br /><br />";
          echo "<b>Informa��es do erro:</b> <br />" . $mail->ErrorInfo;
          } */
    } catch (phpmailerException $e) {
        echo $e->errorMessage(); //Pretty error messages from PHPMailer
    } catch (Exception $e) {
        echo $e->getMessage(); //Boring error messages from anything else!
    }
    //header("LOCATION:?view=paciente_med&act=listfichaevolucao&id={$paciente}");
}

function salvarSugestao($post) {
    extract($post, EXTR_OVERWRITE);
    $capmedica = anti_injection($capmedica, 'numerico');
    $paciente = anti_injection($paciente, 'numerico');

    //INSERINDO OS DADOS DE QUEM SOLICITOU AS CORREÇÕES DE QUAL FICHA SE TRATA
    $sql = "INSERT INTO sugestoes_secmed(id_capmedica, id_paciente, id_usuario, local_sugestao, data_sugestao, data_resposta, status)
            VALUES ({$capmedica}, {$paciente}, {$_SESSION['id_user']}, '{$local}', NOW(), '0000-00-00 00:00:00', 0)";
    $rs = mysql_query($sql) or die("ERRO 1: ".mysql_error());
    $id = mysql_insert_id();

    //BUSCANDO O NOME DO PACIENTE
    $sql_paciente = "SELECT nome, empresa FROM clientes WHERE idClientes = {$paciente}";
    $rs_paciente = mysql_query($sql_paciente) or die("ERRO 1: ".mysql_error());
    $row_paciente = mysql_fetch_array($rs_paciente, MYSQL_ASSOC);

    $path_emails = 'app/configs/email.php';
    $config_emails = include($path_emails);

    if(isset($config_emails['lista']['emails']['medico'][$row_paciente['empresa']])){
        $email = $config_emails['lista']['emails']['medico'][$row_paciente['empresa']]['email'];
        $nome = $config_emails['lista']['emails']['medico'][$row_paciente['empresa']]['nome'];
    }

    $html = '';
    $html .= "Ola, {$nome}<br><br>";
    $html .= "Nós encontramos algumas inconformidades e/ou erros de preenchimentos na ficha do paciente {$row_paciente['nome']}. Sugerimos que a(s) seguinte(s) mudança(s) seja(m) feita(s): <br>";
    $html .= "<br><br>";
    if($rs){
        $i = 1;
        $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
        $mail->IsSMTP(); // telling the class to use SMTP
        foreach($dados as $dado){
            $html .= "<b>Sugestão No. {$i}</b><br>";
            $tipo = anti_injection($dado[0], 'literal');
            $secao = anti_injection($dado[1], 'literal');
            $sugestao = anti_injection($dado[2], 'literal');
            $sql = "INSERT INTO sugestoes_secmed_lista(id_sugestao, tipo_sugestao, secao_sugestao, sugestao, status)
                    VALUES ({$id},'{$tipo}','{$secao}','{$sugestao}', 0)";
            $rs = mysql_query($sql) or die("ERRO 2: ".mysql_error());
            $html .= "Tipo: ".($tipo === 'inc' ? "Incoformidade" : "Erro de Preenchimento")."<br>";
            if($local === 'fcav'){
                $local_texto = 'Ficha de Avaliação';
                $link = "medico/?view=paciente_med&act=editar_capmed&id={$capmedica}";
                switch ($secao){
                    case 'fam':
                        $secaotexto = 'Ficha de Avaliação Mederi';
                        break;
                    case 'mh':
                        $secaotexto = 'Motivo da Hospitalização';
                        break;
                    case 'pa':
                        $secaotexto = 'Problemas Ativos';
                        break;
                    case 'am':
                        $secaotexto = 'Alergia Medicamentosa';
                        break;
                    case 'aa':
                        $secaotexto = 'Alergia Alimentar';
                        break;
                    case 'com':
                        $secaotexto = 'Comorbidades';
                        break;
                    case 'ium':
                        $secaotexto = 'Internações nos Últimos 12 Meses';
                        break;
                    case 'pm':
                        $secaotexto = 'Parecer Médico';
                        break;
                    case 'eqp':
                        $secaotexto = 'Equipamentos';
                        break;
                }
            }else{
                $local_texto = 'Prescrição';
                $link = "medico/?view=paciente_med&act=listcapmed&id={$paciente}";
                switch ($secao){
                    case 'rep':
                        $secaotexto = 'Repouso';
                        break;
                    case 'die':
                        $secaotexto = 'Dieta';
                        break;
                    case 'ce':
                        $secaotexto = 'Cuidados Especiais';
                        break;
                    case 'se':
                        $secaotexto = 'Soros e Eletrólitos';
                        break;
                    case 'ai':
                        $secaotexto = 'Antibióticos Injetáveis';
                        break;
                    case 'iv':
                        $secaotexto = 'Injetáveis IV';
                        break;
                    case 'im':
                        $secaotexto = 'Injetáveis IM';
                        break;
                    case 'sc':
                        $secaotexto = 'Injetáveis SC';
                        break;
                    case 'di':
                        $secaotexto = 'Drogas Inalatórias';
                        break;
                    case 'neb':
                        $secaotexto = 'Nebulização';
                        break;
                    case 'doe':
                        $secaotexto = 'Drogas Orais/Enterais';
                        break;
                    case 'dt':
                        $secaotexto = 'Drogas Tópicas';
                        break;
                    case 'susp':
                        $secaotexto = 'Suspender';
                        break;
                    case 'oxi':
                        $secaotexto = 'Oxigenoterapia';
                        break;
                    case 'doep':
                        $secaotexto = 'Dietas Orais/Enterais/Parenterais';
                        break;
                    case 'mat':
                        $secaotexto = 'Materiais';
                        break;
                }
            }
            $html .= "Em (Local na {$local_texto}) {$secaotexto} <br>";
            $html .= "Sugestão: {$sugestao} <br>";
            $html .= "------------------------------------------- <br><br>";
            $i++;
        }
        $html .= "Para acessar o recurso supracitado, clique <a href='http://sistema.mederi.dev/{$link}' target='_blank'>aqui</a>.<br>";
        $html .= "Att., <br>";
        $html .= $_SESSION['nome_user']."<br>";
        $html .= "SECMED Mederi. <br>";
    }
    if($rs){
        $erros = true;
        try {
            $mail->SMTPAuth = true;                  // enable SMTP authentication
            $mail->CharSet = 'UTF-8';
            $mail->Host = "smtp.mederi.com.br";      // sets GMAIL as the SMTP server
            $mail->Port = 587;                   // set the SMTP port for the GMAIL server
            $mail->Username = "avisos@mederi.com.br";  // GMAIL username
            $mail->Password = "2017avisosistema";            // GMAIL password

            $mail->AddAddress($email,$nome);
            $mail->SetFrom('avisos@mederi.com.br', 'Avisos de Sistema');
            $mail->Subject = "Envio da(s) correção(ões) a ser(em) feita(s) pelo médico";
            $mail->AltBody = 'Para ver essa mensagem, use um visualizador de email compatível com HTML'; // optional - MsgHTML will create an alternate automatically
            $mail->MsgHTML($html);
            $enviado = $mail->Send();
        } catch (phpmailerException $e) {
            $erros .= $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            $erros .= $e->getMessage(); //Boring error messages from anything else!
        }
        //$erros = $rs;
        //print_r($erros);
        echo 1;
    }else{
        echo "ERRO: Tente mais tarde!";
        return;
    }
}

function salvarCorrecoes($post) {
    extract($post, EXTR_OVERWRITE);
    $capmedica = anti_injection($capmedica, 'numerico');
    $paciente = anti_injection($paciente, 'numerico');

    //ATUALIZANDO O STATUS DAS SUGESTÕES QUE FORAM MARCADAS COMO CORRIGIDAS
    if($dados[0][0] !==  'true'){
        foreach ($dados as $dado)
            $sugestoes_lista[] = $dado[1];

        $sql = "UPDATE sugestoes_secmed_lista SET status = 1 WHERE id IN (".implode(',', $sugestoes_lista).")";
        $rs1 = mysql_query($sql);
    }else{
        $sugestao = $dados[0][2];
        $sql_sugestoes = "SELECT id FROM sugestoes_secmed WHERE id_capmedica = {$capmedica}";
        $rs_sugestoes = mysql_query($sql_sugestoes);
        while($row_sugestoes = mysql_fetch_array($rs_sugestoes, MYSQL_ASSOC))
            $sugestoes_lista[] = $row_sugestoes['id'];
        $sql = "UPDATE sugestoes_secmed_lista SET status = 1 WHERE id_sugestao IN (".implode(',', $sugestoes_lista).")";
        $sql2 = "UPDATE sugestoes_secmed SET status = 1 WHERE id IN (".implode(',', $sugestoes_lista).")";
        $rs1 = mysql_query($sql);
        $rs2 = mysql_query($sql2);
    }
    if($rs1 || ((isset($rs2)) && ($rs1 && $rs2)))
        echo 1;
    else
        echo 0;
}

function atualizarAlegiaAlimentar($alergias_ativas, $alergias_inativas, $paciente, $usuario){
	foreach ($alergias_ativas as $alimento) {
		if(isset($alimento) && $alimento != null){
			$sql_verifica = "SELECT ID FROM alergia_alimentar WHERE ALIMENTO = '{$alimento}' WHERE PACIENTE_ID = '{$paciente}'";
			$rs_verifica = mysql_query($sql_verifica);
			$num_verifica = mysql_num_rows($rs_verifica);
			if($num_verifica > 0){
				continue;
			}else{
				$alergiaalim = anti_injection($alimento, "literal");
				$sql5 = "INSERT into alergia_alimentar VALUES ('','{$paciente}','{$usuario}','{$alergiaalim}',now(),1,1)";
				$r = mysql_query($sql5);
				if (!$r) {
					mysql_query("rollback");
					echo "Erro ao salvar alergia alimentar";
					return;
				}
				savelog(mysql_escape_string(addslashes($sql6)));
			}
		}
	}

	foreach ($alergias_inativas as $alimento) {
		if($alimento != "" && $alimento != null){
			$alergiaalim = anti_injection($alimento, "literal");
			$sql6 = "UPDATE alergia_alimentar SET ATIVO = 2 WHERE PACIENTE_ID = '{$paciente}' AND ALIMENTO = '{$alergiaalim}'";
			$r = mysql_query($sql6);
			if (!$r) {
				mysql_query("rollback");
				echo "Erro ao salvar alergia alimentar";
				return;
			}
			savelog(mysql_escape_string(addslashes($sql6)));
		}
	}
}

function atualizarAlergiasMedicamentosas($alergias_ativas, $alergias_inativas, $paciente, $usuario){
	foreach ($alergias_ativas as $m) {
		if ($m != "" && $m != null) {
			$i = explode("#", $m);
			$cod = anti_injection($i[0], "numerico");
			$desc = anti_injection($i[1], "literal");
			$catalogo_id = anti_injection($i[2], "literal");

			if ($cod == 0) {

				$sql_medicamento_outros = "SELECT `DESCRICAO` FROM `alergia_medicamentosa` WHERE `ID_PACIENTE`= '{$paciente}'
                    AND `DESCRICAO`='{$desc}'  AND `ATIVO`='s'";
				$resultado_medicamento_outros = mysql_query($sql_medicamento_outros);

				$num_rows = mysql_num_rows($resultado_medicamento_outros);

				if ($num_rows < 1) {
					$sql = "INSERT INTO alergia_medicamentosa (`ID_PACIENTE`, `NUMERO_TISS`, `DESCRICAO`, `ATIVO`, `DATA`, `USUARIO_ID`, `CATALOGO_ID`)
                            VALUES ('{$paciente}','{$cod}','{$desc}', 's', now(), '{$usuario}', '{$catalogo_id}')";
					$r = mysql_query($sql);

					if (!$r) {
						mysql_query("rollback");
						echo "ERRO40: Tente mais tarde! {$sql}";
						//. mysql_error();
						return;
					}
					savelog(mysql_escape_string(addslashes($sql)));
				}
			} else {
				$sql_medicamento = "SELECT `CATALOGO_ID` FROM `alergia_medicamentosa` WHERE `ID_PACIENTE`= '{$paciente}' AND `CATALOGO_ID`='{$catalogo_id}'"; //Verifica se o código já existe
				$resultado_medicamento = mysql_query($sql_medicamento);

				$codigo = mysql_result($resultado_medicamento, 0);

				if ($codigo == false) {//Se não existir, insere um novo registro
					$sql = "INSERT INTO alergia_medicamentosa (`ID_PACIENTE`, `NUMERO_TISS`, `DESCRICAO`, `ATIVO`, `DATA`, `USUARIO_ID`, `CATALOGO_ID`)
                                VALUES ('{$paciente}','{$cod}','{$desc}', 's', now(), '{$usuario}', '{$catalogo_id}')";
					$r = mysql_query($sql);

					if (!$r) {
						mysql_query("rollback");
						echo "ERRO41: Tente mais tarde! ";
						//. mysql_error();
						return;
					}
					savelog(mysql_escape_string(addslashes($sql)));
				} else {//Se existir, atualiza o registro
					$sql = "UPDATE `alergia_medicamentosa` SET `DESCRICAO`='{$desc}', `ATIVO`='s' WHERE `ID_PACIENTE`= '{$paciente}' AND `CATALOGO_ID`='{$catalogo_id}'";
					$r = mysql_query($sql);

					if (!$r) {
						mysql_query("rollback");
						echo "ERRO42: Tente mais tarde! ";
						//. mysql_error();
						return;
					}
					savelog(mysql_escape_string(addslashes($sql)));
				}
			}//
		}
	}//Alergia Medicamentosa

	foreach ($alergias_inativas as $m) {
		if ($m != "" && isset($m)) {
			$i = explode ("#", $m);
			$cod = anti_injection ($i[0], "literal");
			$desc = anti_injection ($i[1], "literal");
			$catalogo_id = anti_injection ($i[2], "literal");

			if ($cod == 0) {
				$sql = "UPDATE `alergia_medicamentosa` SET `ATIVO`='n' WHERE `ID_PACIENTE`= '{$paciente}' AND `DESCRICAO`='{$desc}'";
				$r = mysql_query ($sql);

				if (!$r) {
					mysql_query ("rollback");
					echo "ERRO40: Tente mais tarde! {$sql}";
					//. mysql_error();
					return;
				}
				savelog (mysql_escape_string (addslashes ($sql)));
			} else {
				$sql = "UPDATE `alergia_medicamentosa` SET `ATIVO`='n' WHERE `ID_PACIENTE`= '{$paciente}' AND `CATALOGO_ID`='{$catalogo_id}'";
				$r = mysql_query ($sql);

				if (!$r) {
					mysql_query ("rollback");
					echo "ERRO41: Tente mais tarde! ";
					//. mysql_error();
					return;
				}
				savelog (mysql_escape_string (addslashes ($sql)));
			}
		}//
	}
}

function salvarRelatorioDeflagrado($post){
  extract($post, EXTR_OVERWRITE);

  $inicio = implode("-",array_reverse(explode("/",$inicio)));
  $fim = implode("-",array_reverse(explode("/",$fim)));
  $historiaClinica = anti_injection($historia_clinica,'literal');
  $servicosPrestados = anti_injection($servicos_prestados,'literal');
  $pacienteId = anti_injection($paciente_id,'numerico');
  $editar = anti_injection($editar,'numerico');

    $sql = "SELECT convenio, empresa FROM clientes WHERE idClientes = '{$pacienteId}'";
    $rs = mysql_query($sql);
    $rowPaciente = mysql_fetch_array($rs);
    $empresa = null;
    $convenio = null;
    if(count($rowPaciente) > 0) {
        $empresa = $rowPaciente['empresa'];
        $convenio = $rowPaciente['convenio'];
    }

  mysql_query("begin");
  $user = $_SESSION["id_user"];
  //$empresa = $_SESSION["empresa_principal"];
  $idRelatorioOrigem ='';
  if($editar == 1){
    $idRelatorioOrigem =anti_injection($id_relatorio,'numerico');
      $sqlUpdate = " UPDATE
                    relatorio_deflagrado_med
                    SET
                    editado_em = now(),
                    editado_por ='{$user}'
                    WHERE
                    id ={$idRelatorioOrigem} ";
      $resultUpdate = mysql_query($sqlUpdate);
      if (!$resultUpdate) {
          echo "Erro ao editar relatório.";
          mysql_query("rollback");
          return;
      }
      savelog(mysql_escape_string(addslashes($sqlUpdate)));
  }
  $sql = "INSERT INTO
          relatorio_deflagrado_med
          (usuario_id,
          cliente_id,
          empresa,
          plano,
          data,
          inicio,
          fim,
          historia_clinica,
          servicos_prestados,
          relatorio_deflagrado_origem)
           VALUES
            ('{$user}',
            '{$pacienteId}',
            '{$empresa}',
            '{$convenio}',
            now(),
            '{$inicio}',
            '{$fim}',
            '{$historiaClinica}',
            '{$servicosPrestados}',
            '{$idRelatorioOrigem}')";

  $a = mysql_query($sql);
  if (!$a) {
    echo "Erro ao tentar salvar Relatório Deflagrado";
    mysql_query("rollback");
    return;
  }
  $idRelatorioDeflagrado = mysql_insert_id();
  savelog(mysql_escape_string(addslashes($sql)));

  $problemaAtivo = explode('$',$problema_ativo);

  foreach( $problemaAtivo as $prob){
    $i = explode('#',$prob);
    $codCid10  = anti_injection($i[0],'literal');
    $descricao = anti_injection($i[1],'literal');

    $sqlInsert = " INSERT INTO
                               relatorio_deflagrado_med_problema_ativo
                               (cid10_id,
                                descricao,
                                relatorio_deflagrado_med_id)
                                values(
                                '{$codCid10}',
                                '{$descricao}',
                                '{$idRelatorioDeflagrado}'

                                )";
    $result = mysql_query($sqlInsert);
    if (!$result) {
      echo "Erro ao tentar salvar problemas ativos do Relatório Deflagrado";
      mysql_query("rollback");
      return;
    }
    savelog(mysql_escape_string(addslashes($sqlInsert)));
  }

mysql_query('commit');

	enviarEmail($pacienteId, 10, $empresa);

  //$rsEmail = enviar_email_relatorio($pacienteId,10,$empresa);

	echo '1';
}

function enviarEmail($paciente, $tipo, $empresa)
{
	$api_data = (object) Config::get('sendgrid');

	$gateway = new MailAdapter(
		new SendGridGateway(
			new \SendGrid($api_data->user, $api_data->pass),
			new \SendGrid\Email()
		)
	);
	$gateway->adapter->setSendData($paciente, 'medico', $tipo, $empresa);

	$gateway->adapter->sendMail();

	enviarNotificacaoHipchat($gateway);

	return $gateway;
}

function enviarNotificacaoHipchat($gateway)
{
	$env = Config::get('APP_ENV');
    $token = $env == 'prd' ? Config::get('TOKEN_HIPCHAT') : Config::get('TOKEN_HIPCHAT_DEBUG');
	$hip = new \App\Controllers\HipChatSismederi($token, $env);
	$mensagem = utf8_decode($gateway->adapter->mail->html);

	$gateway->sendNotificationHipChat($hip, $mensagem);
}

function enviarEmailSimples($titulo, $texto, $para)
{
    $api_data = (object) Config::get('sendgrid');

    $gateway = new MailAdapter(
        new SendGridGateway(
            new \SendGrid($api_data->user, $api_data->pass),
            new \SendGrid\Email()
        )
    );

    $gateway->adapter->sendSimpleMail($titulo, $texto, $para);
}

function previewPrescricao($id)
{
    $abas = array(
        '0' => 'Repouso',
        '1' => 'Dieta',
        '2' => 'Cuidados Especiais',
        '3' => 'Soros e Eletr&oacute;litos',
        '4' => 'Antibi&oacute;ticos Injet&aacute;veis',
        '5' => 'Injet&aacute;veis IV',
        '6' => 'Injet&aacute;veis IM',
        '7' => 'Injet&aacute;veis SC',
        '8' => 'Drogas inalat&oacute;rias',
        '9' => 'Nebuliza&ccedil;&atilde;o',
        '10' => 'Drogas Orais/Enterais',
        '11' => 'Drogas T&oacute;picas',
        '12' => 'F&oacute;mulas',
        '13' => 'Oxigenoterapia',
        '14' => 'Dietas Enterais/Parenterais',
        '15' => 'Materiais',
        '16' => 'Procedimentos em Gastrostomia'
    );
    $html = "<table width=100% class='mytable'><thead><tr>";
    $html.= "<th><b>Itens prescritos</b></th>";
    $html.= "</tr></thead>";
    $sql = "SELECT v.via as nvia, f.frequencia as nfrequencia,i.*,  i.inicio,
    		i.fim, DATE_FORMAT(i.aprazamento,'%H:%i') as apraz, u.unidade as um,
    		p.motivo_ajuste
    		FROM itens_prescricao as i LEFT OUTER JOIN frequencia as f ON (i.frequencia = f.id)
    		LEFT OUTER JOIN viaadm as v ON (i.via = v.id)
    		LEFT OUTER JOIN umedidas as u ON (i.umdose = u.id)
    		inner join prescricoes as p on (i.idPrescricao = p.id)
    		WHERE idPrescricao = '{$id}'
    		ORDER BY tipo,inicio,aprazamento,nome,apresentacao ASC";
    $result = mysql_query($sql);
    $n = 0;
    while ($row = mysql_fetch_array($result)) {

        $motivoAjuste = $row['motivo_ajuste'];
        if ($n++ % 2 == 0)
            $cor = '#E9F4F8';
        else
            $cor = '#FFFFFF';

        $tipo = $row['tipo'];
        $i = implode("/", array_reverse(explode("-", $row['inicio'])));
        $f = implode("/", array_reverse(explode("-", $row['fim'])));
        $aprazamento = "";
        if ($row['tipo'] != "0")
            $aprazamento = $row['apraz'];
        $linha = "";
        if ($row['tipo'] != "-1") {
            $aba = $abas[$row['tipo']];
            $dose = "";
            if ($row['dose'] != 0)
                $dose = $row['dose'] . " " . $row['um'];
            $linha = "<b>$aba:</b> {$row['nome']} {$row['apresentacao']} {$row['nvia']} {$dose} {$row['nfrequencia']} Per&iacute;odo: de $i até $f OBS: {$row['obs']}";
        }
        else
            $linha = $row['descricao'];
        $html.= "<tr bgcolor={$cor} >";
        $html.= "<td>$linha</td>";
        $html.= "</tr>";
    }
    $html.= "</table>";

    if(!empty($motivoAjuste)){
        $hmtlAjuste = "
<div class='ui-state-highlight' style='padding: 5px;'>
                <span class='ui-icon  ui-icon-alert icon-inline' ></span>
                <b>Motivo do Ajuste da Prescrição Aditiva: </b> 
                {$motivoAjuste}
            </div>
            <br>
";
        $hmtlAjuste.= $html;
        return $hmtlAjuste;
    }

    return $html;
}

switch ($_POST['query']) {
    case "opcoes-itens":
        opcoes($_POST['like'], $_POST['classe']);
        break;
    case "prescricoes-antigas":
        prescricoes_antigas($_POST['p'], $_POST['tipo']);
        break;
    case "salvar-prescricao":
        salvar_prescricao($_POST);
        break;
    case "finalizar-prescricao":
        finalizar_prescricao($_POST);
        break;
    case "desativar-prescricao":
        desativar_prescricao($_POST);
        break;
    case "finalizar-prescricao-emergencial":
        finalizar_prescricao_emergencial($_POST);
        break;
    case "buscar-prescricoes":
        buscar_prescricoes($_POST);
        break;
    case "buscar_medicamento":
        busca_medicamento($_POST);
        break;
    case "salvar_associacao":
        salva_associacao($_POST);
        break;
    case "email-aviso":
        enviar_email_aviso($_POST);
        break;
    case "nova-capmed":
        nova_capmed($_POST);
        break;
    case "novo-score":
        novo_score($_POST);
        break;
    case "remover-score":
        remover_score($_POST);
        break;
    case "show-status-paciente":
        show_status_paciente($_POST);
        break;
    case "salvar-status-paciente":
        salvar_status_paciente($_POST);
        break;
    case "salvar-paciente":
        salvar_paciente($_POST);
        break;
    case "editar-paciente":
        editar_paciente($_POST);
        break;
    case "excluir-paciente":
        excluir_paciente($_POST);
        break;
    case "gravar-outro":
        gravar_outro($_POST);
        break;
    case "testar-outro":
        testar_outro($_POST);
        break;
    case "consultar-itens-kit":
        consultar_itens_kit($_POST);
        break;
    case "prob-ativos":
        prob_ativos($_POST);
        break;
    case "equipamentos-verificar":
        equipamentos_verificar($_POST);
        break;
    case "salvar-obs":
        salvar_obs($_POST);
        break;
    case "editar-produto-interno":
        editar_produto_interno($_POST);
        break;
    case "nova-ficha-evolucao":
        nova_ficha_evolucao($_POST);
        break;
		case "editar-ficha-evolucao":
			editar_ficha_evolucao($_POST);
			break;
    case "editar-ficha-avaliacao":
        editar_ficha_avaliacao($_POST);
        break;
    case "inativar-alergias":
        inativar_alergias($_POST);
        break;
    case 'relatorio_alta':
        salvar_relatorio_alta($_POST);
        break;
    case "prorrogacao":
        salvar_relatorios_prorrogacao($_POST);
        break;
    case "editar_prorrogacao":
        editar_relatorios_prorrogacao($_POST);
        break;
    case "salvar_intercorrencia_medica":
        salvar_intercorrencia_medica($_POST);
        break;
    case "salvar-sugestao":
        salvarSugestao($_POST);
        break;
    case "salvar-correcoes":
        salvarCorrecoes($_POST);
        break;
  case "salvar-rel-deflagrado":
    salvarRelatorioDeflagrado($_POST);
    break;
}

switch ($_GET['query']) {
    case "preview-prescricao":
        echo previewPrescricao($_GET['id']);
        break;
}