<?php

$id = session_id();
if (empty($id))
    session_start();

include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');
require_once('paciente_med.php');
require_once('medico.php');
require __DIR__ . '/../vendor/autoload.php';

use \App\Models\Administracao\Paciente as ModelPaciente;

class Relatorios {

////////////////Eriton 17-06-2013////////////////////


    public function listar($id) {

        echo "<div>";
        Paciente::listar_capitacao_medica($id);
        echo "</div>";
    }

    public function listar_alta_medica($id) {




        $sql = "
        SELECT
                    DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
                    DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
                    u.nome as user,
                    u.tipo as utipo,
                    u.conselho_regional,
                    u.idUsuarios,
                    r.ID as id_rel,
                    p.nome as paciente
        FROM
                    relatorio_alta_med as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios) LEFT JOIN
                    clientes as p ON (r.PACIENTE_ID = p.idClientes)  
        WHERE
                    r.PACIENTE_ID = '{$id}'
        ORDER BY
                    r.DATA DESC";
        $r = mysql_query($sql);
        $flag = true;
        $existe_prescricao = 0;
        $penultimo = 0; //valor falso
        //echo "<div>";

        $sql1 = "
            SELECT
                max(ID)
            FROM
                relatorio_alta_med
            WHERE
                PACIENTE_ID= {$id}
            ";
        $result1 = mysql_query($sql1);
        $ultima_prorrogacao = mysql_result($result1, 0);

        $sql2 = "select DATE_FORMAT(data,'%Y-%m-%d %H:%i:%s') from relatorio_alta_med where id = '{$ultima_prorrogacao}' ";

        $result2 = mysql_query($sql2);
        $data_relatorio = mysql_result($result2, 0);
        $data_atual = date('Y/m/d');
        $prazoEditar = date('Y/m/d', strtotime("+2 days", strtotime($data_relatorio)));

        while ($row = mysql_fetch_array($r)) {
            $compTipo = $row['conselho_regional'];

            if ($flag) {
                $p = ucwords(strtolower($row["paciente"]));
                echo "<div><center><h1>Alta Médica</h1></center></div>";
                Paciente::cabecalho($id);

                if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_alta']['medico_paciente_alta_novo']) || $_SESSION['adm_user'] == 1) {
                    echo "<button id='novo_rel_alta' paciente={$id} class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'><span class='ui-button-text'>Nova</span></button>";
                }

                echo "<table class='mytable' style='width:100%' >";
                echo "<thead><tr>";
                echo "<th width='35%'>Usu&aacute;rio</th>";
                echo "<th>Data</th>";
                echo "<th>Data da Alta</th>";
                echo "<th width='10%'>Detalhes</th>";
                echo "<th>Op&ccedil;&otilde;es</th>";
                echo "</tr></thead>";
                $flag = false;
            }

            echo "<tr><td>{$row['user']}{$compTipo}</td>";
            echo "<td>{$row['rdata']}</td>";
            echo "<td>{$row['fim']} </td>";
            echo "<td><a href=?view=visualizar_rel_alta&id={$id}&idr=" . $row['id_rel'] . "><img src='../utils/capmed_16x16.png' title='Visualizar Alta'></td>";
            if ($row['id_rel'] == $ultima_prorrogacao) {
                if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_alta']['medico_paciente_alta_usar_para_nova']) || $_SESSION['adm_user'] == 1) {
                    echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=relatorio&opcao=1&id={$id}&idr=" . $row['id_rel'] . "&editar=0><img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para nova alta.' border='0'></a></td></tr>";
                    if($data_atual < $prazoEditar){
                        //echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=relatorio&opcao=1&id={$id}&idr=" . $row['id_rel'] . "&editar=1><img src='../utils/edit_16x16.png' title='Editar' border='0'></a></td></tr>";
                    }
                }
            }else{
                echo "<td></td></tr>";
            }
        }

        if ($flag) {
            if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_alta']['medico_paciente_alta_novo']) || $_SESSION['adm_user'] == 1) {
                $this->relatorio_alta($id);
            } else {
                echo "N&atilde;o possui Relat&oacute;rio de Alta.";
            }
            return;
        }
        echo "</table>";

        echo "</div>";
    }

    public function listar_prorrogacao_medica($id) {

        echo "<div><center><h1>Prorrogação Médica</h1></center></div>";

        $sql = "
		SELECT
                    DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
                    DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
                    DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
                    u.nome as user,
                    u.tipo as utipo,
                    u.conselho_regional,
                    u.idUsuarios,
                    r.ID as id_rel,
                    p.nome as paciente
		FROM
                    relatorio_prorrogacao_med as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios) LEFT JOIN
                    clientes as p ON (r.PACIENTE_ID = p.idClientes)	 
		WHERE
                    r.PACIENTE_ID = '{$id}'
                    AND r.STATUS = 's'
		ORDER BY
                    r.DATA DESC";
        $r = mysql_query($sql);
        $flag = true;
        $existe_prescricao = 0;
        $penultimo = 0; //valor falso
        //echo "<div>";

        $sql1 = "
            SELECT
                max(ID)
            FROM
                relatorio_prorrogacao_med
            WHERE
                PACIENTE_ID= {$id}
            ";
        $result1 = mysql_query($sql1);
        $ultima_prorrogacao = mysql_result($result1, 0);

        $sql2 = "select DATE_FORMAT(data,'%Y-%m-%d %H:%i:%s') from relatorio_prorrogacao_med where id = '{$ultima_prorrogacao}' ";

        $result2 = mysql_query($sql2);
        $data_relatorio = mysql_result($result2, 0);
        $data_atual = date('Y/m/d');
        $prazoEditar = date('Y/m/d', strtotime("+3 days", strtotime($data_relatorio)));

        while ($row = mysql_fetch_array($r)) {
            $compTipo = $row['conselho_regional'];

            if ($flag) {
                $p = ucwords(strtolower($row["paciente"]));
                Paciente::cabecalho($id);

                if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_prorrogacao']['medico_paciente_prorrogacao_novo']) || $_SESSION['adm_user'] == 1) {
                    echo "<button id='novo_rel_prorrogacao' paciente={$id} class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'><span class='ui-button-text'>Nova</span></button>";
                }

                echo "<table class='mytable' style='width:100%' >";
                echo "<thead><tr>";
                echo "<th width='35%'>Usu&aacute;rio</th>";
                echo "<th>Data</th>";
                echo "<th>Per&iacute;odo</th>";
                echo "<th width='10%'>Visualizar</th>";
                echo "<th>Op&ccedil;&otilde;es</th>";
                echo "</tr></thead>";
                $flag = false;
            }

            echo "<tr><td>{$row['user']}{$compTipo}</td>";
            echo "<td>{$row['rdata']}</td>";
            echo "<td>{$row['inicio']} AT&Eacute; {$row['fim']}</td>";
            echo "<td><a href=?view=visualizar_rel_prorrogacao&id={$id}&idr=" . $row['id_rel'] . "><img src='../utils/capmed_16x16.png' title='Visualizar prorrogacao'></td>";
            echo "<td>";
            if ($_SESSION['adm_user'] == 1 || $_SESSION['modmed'] == 1) {
                if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_prorrogacao']['medico_paciente_prorrogacao_usar_para_nova']) || $_SESSION['adm_user'] == 1) {
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=relatorio&opcao=3&id={$id}&idr=" . $row['id_rel'] . "&editar=0><img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para novo relat&oacute;rio.' border='0'></a>";
                }
                if((isset($_SESSION['id_user']) && in_array($_SESSION['id_user'], array(167,15)))) { // Dr. Paloma, Dr. Nilson & Dr. Lúcio
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=relatorio&opcao=3&id={$id}&idr=" . $row['id_rel'] . "&editar=1><img src='../utils/edit_16x16.png' title='Editar' border='0'></a>";
                }else{
                    if ($row['id_rel'] == $ultima_prorrogacao && $_SESSION["adm_ur"] == 1) {
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=relatorio&opcao=3&id={$id}&idr=" . $row['id_rel'] . "&editar=1><img src='../utils/edit_16x16.png' title='Editar' border='0'></a>";
                    }else if ($prazoEditar > $data_atual && $row['id_rel'] == $ultima_prorrogacao){
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=relatorio&opcao=3&id={$id}&idr=" . $row['id_rel'] . "&editar=1><img src='../utils/edit_16x16.png' title='Editar' border='0'></a>";
                    }
                }
            }else{
                echo "</td></tr>";
            }
        }

        if ($flag) {
            if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_prorrogacao']['medico_paciente_prorrogacao_novo']) || $_SESSION['adm_user'] == 1) {
                $this->relatorio_prorrogacao($id);
            } else {
                echo "N&atilde;o possui Relat&oacute;rio de Prorroga&ccedil;&atilde;o.";
            }
            return;
        }
        echo "</table>";

        echo "</div>";
    }

    public function listar_relatorio_aditivo($id) {

        echo "<div><center><h1>Relatório Aditivo Médico</h1></center></div>";

        $sql = "
		SELECT
                    DATE_FORMAT(r.data,'%d/%m/%Y %H:%i:%s') as rdata,
                    DATE_FORMAT(r.inicio,'%d/%m/%Y') as inicio,
                    DATE_FORMAT(r.fim,'%d/%m/%Y') as fim,
                    u.nome as user,
                    u.tipo as utipo,
                    u.conselho_regional,
                    u.idUsuarios,
                    r.id as id_rel,
                    p.nome as paciente,
                    r.flag
		FROM
                    prescricoes as r LEFT JOIN
                    usuarios as u ON (r.criador = u.idUsuarios) LEFT JOIN
                    clientes as p ON (r.paciente = p.idClientes)	 
		WHERE
                    r.paciente = '{$id}' and
                    r.carater = 3 AND
                    r.DATA_DESATIVADA = '00-00-0000'
		ORDER BY
                    r.DATA DESC";
        $r = mysql_query($sql);
        $flag = true;
        $existe_prescricao = 0;
        $penultimo = 0; //valor falso


        while ($row = mysql_fetch_array($r)) {
            $compTipo = $row['conselho_regional'];

            if ($flag) {
                $p = ucwords(strtolower($row["paciente"]));
                Paciente::cabecalho($id);


                echo "<table class='mytable' style='width:100%' >";
                echo "<thead><tr>";
                echo "<th width='35%'>Usu&aacute;rio</th>";
                echo "<th>Tipo</th>";
                echo "<th>Data</th>";
                echo "<th>Per&iacute;odo</th>";
                echo "<th width='10%'>Visualizar</th>";
                echo "</tr></thead>";
                $flag = false;
            }

            $tipo = empty($row['flag']) ? 'Aditivo': 'Aditivo '.$row['flag'];

            echo "<tr><td>{$row['user']}{$compTipo}</td>";
            echo "<td>{$tipo}</td>";
            echo "<td>{$row['rdata']}</td>";
            echo "<td>{$row['inicio']} AT&Eacute; {$row['fim']}</td>";
            echo "<td><a href=?view=visualizar_relatorio_aditivo&id={$id}&idr=" . $row['id_rel'] . "><img src='../utils/capmed_16x16.png' title='Visualizar Relatorio Aditivo'></td>";
//            
        }

        if ($flag) {
            echo htmlentities("NÃO POSSUI RELATÓRIO") . " ADITIVO";
            return;
        }
        echo "</table>";

        echo "</div>";
    }

    public function listar_intercorrencia_medico($id) {
        echo"<div><h1><center>Intercorrência Médica</center></h1></div>";

        $sql = "SELECT
                enc_ur_emer.id as id_rel,
                clientes.nome as paciente,
                usuarios.nome as user,
                usuarios.tipo as utipo,
                usuarios.conselho_regional,
                DATE_FORMAT(enc_ur_emer.data_encaminhamento,'%d/%m/%Y') as encaminhado,
                DATE_FORMAT(enc_ur_emer.`data`,'%d/%m/%Y %H:%i:%s') as feito_em,
                p.id as prescricaoId
                FROM
                intercorrencia_med as enc_ur_emer
                INNER JOIN clientes ON enc_ur_emer.cliente_id = clientes.idClientes
                INNER JOIN usuarios ON enc_ur_emer.usuario_id = usuarios.idUsuarios
                LEFT JOIN prescricoes as p  on enc_ur_emer.id = p.intercorrencia_med_id

                WHERE
                enc_ur_emer.cliente_id = '{$id}'
		ORDER BY
                    enc_ur_emer.DATA DESC";
        $r = mysql_query($sql);
        $flag = true;
        $existe_prescricao = 0;
        $penultimo = 0; //valor falso
        //echo "<div>";

        $sql1 = "
            SELECT
                max(id)
            FROM
                intercorrencia_med
            WHERE
                cliente_id= {$id}
            ";
        $result1 = mysql_query($sql1);
        $ultima_prorrogacao = mysql_result($result1, 0);

        while ($row = mysql_fetch_array($r)) {
            $compTipo = $row['conselho_regional'];

            if ($flag) {
                $p = ucwords(strtolower($row["paciente"]));
                Paciente::cabecalho($id);

                if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_intercorrencia']['medico_paciente_intercorrencia_novo']) || $_SESSION['adm_user'] == 1) {
                    echo "<button id='novo_rel_encaminhamento' paciente={$id} class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
				<span class='ui-button-text'>Nova</span></button>";
                }

                echo "<table class='mytable' style='width:100%' >";
                echo "<thead><tr>";
                echo "<th width='35%'>Usu&aacute;rio</th>";
                echo "<th>Data</th>";
                echo "<th>Data Sistema</th>";
                echo "<th width='10%'>Detalhes</th>";
                // echo "<th>Op&ccedil;&otilde;es</th>";
                echo "</tr></thead>";
                $flag = false;
            }

            echo "<tr>"
                . "<td>{$row['user']}{$compTipo}</td>";
            echo "<td>{$row['encaminhado']}</td>";
            echo "<td>{$row['feito_em']}</td>";
            echo "<td>"
                . " <a href=?view=visualizar_rel_intercorrencia&id={$id}&idr=" . $row['id_rel'] . " target='_blank'>"
                . "<img src='../utils/capmed_16x16.png' title='Visualizar da Intercorrência'>"
                ."</a>";
            if(!empty($row['prescricaoId'])){
                echo " <a href=../medico/?view=detalhes&p={$row['prescricaoId']}&idp={$id} target='_blank'>"
                    . "<img src='../utils/details_16x16.png' title='Visualizar da Prescrição'>"
                    ."</a>";
            }
            echo  "</td>";
        }

        if ($flag) {
            if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_intercorrencia']['medico_paciente_intercorrencia_novo']) || $_SESSION['adm_user'] == 1) {
                $this->relatorio_intercorrencia($id);
            } else {
                echo "N&atilde;o possui Relat&oacute;rio de Encaminhamento.";
            }
            return;
        }
        echo "</table>";

        echo "</div>";
    }

    public function justificativa ($disabled, $justificativa_id, $justificativa, $id_grupo) {
        $retorno = $disabled;
        if ($disabled == 2 || $disabled == 3) {
            $disabled = '';
        } else {
            $disabled = "disabled='disabled'";
        }

        $sql = "SELECT
                    *
                FROM
                    justificativa_profissional_relatorio
                WHERE
                    ID_SERVICO = $id_grupo";

        $result = mysql_query($sql);

        $var .= "<select name='justificativa' {$disabled} style='width:95%; background-color:transparent'>";
        $var .= "<option value='-1'>Selecione</option>";
        while ($row = mysql_fetch_array($result)) {
            if ($justificativa_id == -2) {
                $textarea = "<textarea cols=65 rows=2 name='justificativa' size='100' style='display:inline'>{$justificativa}</textarea>";
            }else{
                $textarea = "<textarea cols=65 rows=2 name='justificativa' size='100' style='display:none'>{$justificativa}</textarea>";
            }
            $var .= "<option value='{$row['ID']}'" . (($row['ID'] == $justificativa_id) ? "selected" : "") . " title='{$row['JUSTIFICATIVA']}' data-justificativa='{$row['JUSTIFICATIVA']}'>" . substr($row['JUSTIFICATIVA'],0,95) . "...</option>";
        }
        $var .= "<option value='-2' " . (($justificativa_id == -2) ? "selected" : "") . ">OUTRO</option></select>{$textarea}";
        if ($retorno == 2) {
            echo $var;
        } else {
            return $var;
        }
    }


    public function frequencia($disabled, $id_frequencia) {
        //quando disabled = 2, vem da função post do .js e vem habilitado. Caso contr�rio vem da fun��o relat�rios.php
        $retorno = $disabled;
        if ($disabled == 2 || $disabled == 3) {
            $disabled = '';
        } else {
            $disabled = "disabled='disabled'";
        }
        $result = mysql_query("SELECT * FROM frequencia ORDER BY frequencia;");
        $var = "<select name='frequencia' {$disabled}  style='background-color:transparent;'>";
        $var .= "<option value='-1'>Selecione</option>";
        while ($row = mysql_fetch_array($result)) {
            if (($id_frequencia <> NULL) && ($id_frequencia == $row['id']))
                $var .= "<option value='{$row['id']}' selected>{$row['frequencia']}</option>";
            else
                $var .= "<option value='{$row['id']}'>{$row['frequencia']}</option>";
        }
        $var .= "</select>";
        if ($retorno == 2) {
            echo $var;
        } else {
            return $var;
        }
    }

    public function especialidade_medica($disabled) {
        //quando disabled = 2, vem da fun��o post do .js e vem habilitado. Caso contr�rio vem da fun��o relat�rios.php
        $retorno = $disabled;
        if ($disabled == 2) {
            $disabled = '';
        } else {
            $disabled = "disabled='disabled'";
        }
        $result = mysql_query("SELECT * FROM especialidade_medica ORDER BY ESPECIALIDADE;");
        $var = "<span class='especialista'><select name='especialidade' {$disabled}  style='background-color:transparent;'>";
        $var .= "<option value='-1'>Selecione</option>";
        while ($row = mysql_fetch_array($result)) {
            /* if(($id <> NULL) && ($id == $row['forma']))
              $var .= "<option value='{$row['id']}' selected>{$row['frequencia']}</option>";
              else */
            $var .= "<option value='{$row['ID']}'>{$row['ESPECIALIDADE']}</option>";
        }
        $var .= "</select></span>";
        if ($retorno == 2) {
            echo $var;
        } else {
            return $var;
        }
    }

    public function resumoHistoria($id_paciente,$id_relatorio,$visualizar){
        if(isset($id_relatorio)){
            $sql_problemas_ativos ="SELECT
                                    pae.DESCRICAO as nome,
                                    pae.CID_ID as cid,
                                    pae.OBSERVACAO as sta,                                    
                                    pae.OBSERVACAO
                                FROM
                                    problemas_relatorio_prorrogacao_med as pae
                                WHERE  
                                    pae.RELATORIO_ALTA_MED_ID = {$id_relatorio}";
        }else{
            $sql_ultima_evolucao = "SELECT max(`ID`) as id FROM `fichamedicaevolucao` WHERE `PACIENTE_ID` = '{$id_paciente}'";
            $result_ultima_evolucao = mysql_query($sql_ultima_evolucao);
            $ultima_evolucao = mysql_result($result_ultima_evolucao, 0);

            $sql_problemas_ativos ="SELECT
                                    pae.DESCRICAO as nome,
                                    pae.CID_ID as cid,
                                    (CASE pae.STATUS
                                        WHEN '0' THEN 'NÃO INFORMADO'
                                        WHEN '1' THEN 'RESOLVIDO'
                                        WHEN '2' THEN 'MELHOR'
                                        WHEN '3' THEN 'ESTÁVEL'
                                        WHEN '4' THEN 'PIOR' END) as sta,
                                    --pae.STATUS as status,
                                    pae.OBSERVACAO
                                FROM
                                    problemaativoevolucao as pae
                                WHERE  
                                    pae.EVOLUCAO_ID = {$ultima_evolucao}
                                ORDER BY
                                    pae.status DESC";
        }
        $result_problemas_ativos = mysql_query($sql_problemas_ativos);

        echo "<table class='mytable' width=95%>";
        echo "<thead><tr><th colspan='4'>Resumo da ". htmlentities("História") ."</th></tr>";

        echo "<tr><td colspan='4'><table style='width:100%;'>";
        echo "<tr bgcolor='#D3D3D3'><td colspan='4' ><b>PROBLEMAS ENCONTRADOS NA ÚLTIMA VISITA MÉDICA </b></td></tr>";
        if(mysql_num_rows($result_problemas_ativos) > 0){
            while($row = mysql_fetch_array($result_problemas_ativos)){
                echo "<tr><td class='probativo1' cid='{$row['cid']}' prob_atv='{$row['nome']}' estado='{$row['sta']}' obs='{$row['OBSERVACAO']}' ><b>{$row['cid']} - {$row['nome']}</b> </td><td colspan='3'><b>Estado: </b>{$row['sta']}</td></tr>";
                /*if( $row['STATUS'] != 1 && $row['OBSERVACAO'] != '' ){
                    echo "<tr><td colspan='4'><b>" . htmlentities("Observação") . ": </b>{$row['OBSERVACAO']}</td></tr>";
                }*/
            }
        }else{
            echo "<tr><td colspan='4'>Não foram registrados problemas na última visita médica</td></tr>";
        }
        echo "</table></td></tr>";
        //Antibióticos venosos/orais com período de uso

        //Intercorrências
        if(isset($id_relatorio)){
            $sql_intercorrencias = "SELECT * FROM `intercorrenciasfichaevolucao` WHERE `RELATORIOALTA_ID` = {$id_relatorio}";
        }
        $sql_intercorrencias = "SELECT * FROM `intercorrenciasfichaevolucao` WHERE `FICHAMEDICAEVOLUCAO_ID` = {$ultima_evolucao}";
        $resultado_intercorrencias = mysql_query($sql_intercorrencias);
        $cont_intercorrencias = 1;

        echo "<tr><td colspan='4'><table style='width:100%;'>";
        echo "<tr  bgcolor='#D3D3D3'><td colspan='4' ><b>INTERCORRÊNCIAS DA ÚLTIMA VISITA MÉDICA</b></td></tr>";
        if(mysql_num_rows($resultado_intercorrencias) > 0){
            while ($row = mysql_fetch_array($resultado_intercorrencias)) {
                echo "<tr><td colspan='4' class='interc' desc='{$row['DESCRICAO']}'><b>Descrição: </b>{$row['DESCRICAO']}</td></tr>";
                $cont_intercorrencias++;
            }
        }else{
            echo "<tr><td colspan='4'>Não foram registradas intercorrências na última visita médica</td></tr>";
        }
        echo "</table></td></tr>";
        if(isset($id_relatorio)){
            $sqlAntibioticos = "SELECT
                                    MEDICAMENTO AS medicamento,
                                    CATALOGO_ID AS CATALOGO_ID,
                                    NUMERO_TISS AS NUMERO_TISS
                                FROM
                                    relatorio_alta_med_antibiotico
                                WHERE
                                    relatorio_alta_id = $id_relatorio";
        }else{
            $sqlAntibioticos = "SELECT
                                itens_prescricao.nome as medicamento,
                                itens_prescricao.CATALOGO_ID as CATALOGO_ID,
                                itens_prescricao.NUMERO_TISS
                            FROM
                                itens_prescricao
                            WHERE
                                 itens_prescricao.tipo = 4 AND itens_prescricao.idPrescricao IN (
                                    SELECT
                                        prescricoes.id
                                    FROM
                                        prescricoes
                                    WHERE
                                        prescricoes.paciente = $id_paciente
                                    AND prescricoes.id >= (
                                        SELECT
                                            MAX(prescricoes.id)
                                        FROM
                                            prescricoes
                                        WHERE
                                            prescricoes.paciente = $id_paciente
                                        AND prescricoes.Carater = 2
                                    )
                                )
                            UNION
                            SELECT
                                catalogo.principio COLLATE utf8_unicode_ci AS medicamento,
                                catalogo.id AS CATALOGO_ID,
                                catalogo.NUMERO_TISS
                            FROM
                                catalogo
                            INNER JOIN itens_prescricao ON (
                                itens_prescricao.CATALOGO_ID = catalogo.id
                            )
                            WHERE
                                catalogo.ANTIBIOTICO = 'S' AND itens_prescricao.idPrescricao IN (
                                    SELECT
                                        prescricoes.id
                                    FROM
                                        prescricoes
                                    WHERE
                                        prescricoes.paciente = $id_paciente
                                    AND prescricoes.id >= (
                                        SELECT
                                            MAX(prescricoes.id)
                                        FROM
                                            prescricoes
                                        WHERE
                                            prescricoes.paciente = $id_paciente
                                        AND prescricoes.Carater = 2
                                    )
                                )
        ";
        }
        $resultadoAntibioticos = mysql_query($sqlAntibioticos);
        echo "<tr><td colspan='4'><table id='medicamentos_alergia_medicamentosa' style='width:100%;'>";
        echo "<tr  bgcolor='#D3D3D3'><td colspan='4' ><b>ANTIBIÓTICOS</b></td></tr>";
        if(!$visualizar){
            echo "<tr><td colspan='4' ><b>Busca  </b><input type='text' name='busca_medicamento_alergia' id='busca-medicamento-alergia' {$acesso} size=100 /></td></tr>";
            echo"<tr><td colspan='4' >
            <input type='checkbox' id='outros-medicamento-alergia' name='outros-medicamento-alergia' {$acesso} /> Outros
            <span class='outro_medicamento_alergia' >
                <input type='text' name='outro-medicamento-alergia' class='outro_medicamento_alergia' {$acesso} size=75/>
                <button id='add-medicamento-alergia'> + </button>
            </span></td></tr>";
            $excluir = "<img src='../utils/delete_16x16.png' class='del-medicamento-alergia' title='Excluir' border='0'>";
        }
        while ($row = mysql_fetch_array($resultadoAntibioticos)){
            echo "<tr><td colspan='4' class='medicamentos-alergia' catalogo_id='{$row['CATALOGO_ID']}' cod='{$row['NUMERO_TISS']}' desc='{$row['medicamento']}'> $excluir {$row['medicamento']}</td></tr>";
        }
        echo "</table>";

        echo "</table>";
    }

    public function relatorio_alta($id_paciente, $id_prorrogacao, $editar) {
        $sqlInternacao = "SELECT DATE_FORMAT(dataInternacao,'%d/%m/%Y') as dataInternacao FROM clientes WHERE idClientes = $id_paciente";
        $resultInternacao = mysql_query($sqlInternacao);
        while ($row = mysql_fetch_array($resultInternacao)) {
            $inicio = $row['dataInternacao'];
        }

        $sql = "SELECT 
                  pds.nome AS plano_rel, 
                  e.nome AS empresa_rel 
                FROM 
                  relatorio_alta_med
                  LEFT JOIN empresas AS e ON relatorio_alta_med.empresa = e.id
                  LEFT JOIN planosdesaude AS pds ON relatorio_alta_med.plano = pds.id 
                WHERE 
                  relatorio_alta_med.ID = '{$id_prorrogacao}'";
        $rs = mysql_query($sql);
        $rowPaciente = mysql_fetch_array($rs);
        $empresa = null;
        $convenio = null;
        if(count($rowPaciente) > 0) {
            $empresa = $rowPaciente['empresa_rel'];
            $convenio = $rowPaciente['plano_rel'];
        }

        echo "<div id='relatorio_alta'>";
        echo alerta();
        echo "<form>";
        echo "<input type='hidden' value='{$id_relatorio}' name='id_relatorio' />";
        echo "<input type='hidden' value='{$id_paciente}' name='id_paciente' />";
//        echo "<br><h1><center>". htmlentities("Relatório Padrão") ." de Alta do Home Care e Encaminhamento para Atendimentos Ambulatoriais</center></h1>";
        echo "<br><h1><center>". htmlentities("Nova Alta Médica") ."</center></h1>";

        Paciente::cabecalho($id_paciente, $empresa, $convenio);
        echo "<table style='width:95%;'>";
        echo "<tr><td><b>". htmlentities("Data de Alta") .": </b><input class='data OBG' type='hidden' disabled ='disabled' name='inicio' maxlength='10' size='10' value='{$inicio}' /><input class='data OBG' type='text' name='fim' maxlength='10' size='10' value='{$fim}' />";
        echo "</table>";


        //Intercorrências
        $sql = "select max(ID) from fichamedicaevolucao where PACIENTE_ID = {$id_paciente}";
        $result = mysql_query($sql);
        $ultima_evolucao = mysql_result($result, 0);

        $sql_intercorrencias = "SELECT * FROM intercorrenciasfichaevolucao WHERE fichamedicaevolucao_id = {$ultima_evolucao}";
        $resultado_intercorrencias = mysql_query($sql_intercorrencias);

        //echo "</table>";

        $this->resumoHistoria($id_paciente,$id_prorrogacao);

        //Exame F�sico

        $this->exames_fisicos($id_paciente);

        //Cuidados especiais com frequência e justificativa
        $this->profissionais($id_prorrogacao,1);

        echo alerta();

        echo "<button id='salvar_rel_alta' paciente='{$id_paciente}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
        echo "</form>";
        echo "</div>";
    }

    public function relatorio_prorrogacao($id_paciente, $id_prorrogacao, $editar) {

        if (isset($id_prorrogacao)) {
            $sql = "
                SELECT
                    r.*,
                    DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
                    DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
                    DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
                    u.nome as user,
                    e.nome AS empresa_rel,
                    pds.nome AS plano_rel		
                FROM
                    relatorio_prorrogacao_med as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios)		
                    LEFT JOIN empresas AS e ON r.empresa = e.id
                    LEFT JOIN planosdesaude AS pds ON r.plano = pds.id		 
                WHERE
                    r.ID = '{$id_prorrogacao}'
                ORDER BY
                    r.DATA DESC";



            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {

                $inicio = $row['inicio'];
                $fim = $row['fim'];
                $user = $row['user'];
                $empresa = $row['empresa_rel'];
                $convenio = $row['plano_rel'];
                $estd_geral = $row['ESTADO_GERAL'];
                $mucosa = $row['MUCOSA'];
                $mucosaobs = $row['MUCOSA_OBS'];
                $escleras = $row['ESCLERAS'];
                $esclerasobs = $row['ESCLERAS_OBS'];
                $respiratorio = $row['PADRAO_RESPIRATORIO'];
                $respiratorioobs = $row['PADRAO_RESPIRATORIO_OBS'];
                $pa_sistolica = $row['PA_SISTOLICA'];
                $pa_diastolica = $row['PA_DIASTOLICA'];
                $fc = $row['FC'];
                $fr = $row['FR'];
                $temperatura = $row['TEMPERATURA'];
                $dor_sn = $row['DOR'];
                $outro_resp = $row['OUTRO_RESP'];
                $oximetria_pulso = $row['OXIMETRIA_PULSO'];
                $tipo_oximetria = $row['OXIMETRIA_PULSO_TIPO'];
                $litros = $row['OXIMETRIA_PULSO_LITROS'];
                $via_oximetria = $row['OXIMETRIA_PULSO_VIA'];
                $mod_home_care = $row['MOD_HOME_CARE'];
                $deslocamento = $row['DESLOCAMENTO'];
                $deslocamento_distancia = $row['DESLOCAMENTO_DISTANCIA'];
                $justificativaProrrogacao = $row['justificativa'];
                $quadroClinico = $row['quadro_clinico'];
                $regimeAtual = $row['regime_atual'];
                $indicacaoTecnica = $row['indicacao_tecnica'];
                $modeloNead = $row['MODELO_NEAD'];
                $classificacao_paciente = $row['CLASSIFICACAO_NEAD'];
                $inicioRel = $row['INICIO'];
                $fimRel = $row['FIM'];
            }
        }
        echo "<div id='relatorio-prorrogacao'>";
        echo "<form>";
        echo "<input type='hidden' value={$id_prorrogacao} name='id_relatorio' />";
        echo "<input type='hidden' value='{$id_paciente}' name='id_paciente' />";
        $titulo = 'Nova';
         if($editar == 1){
            $titulo = 'Editar';
        }
        echo "<center><h1>{$titulo} Prorroga&ccedil;&atilde;o Médica</h1></center>";

        Paciente::cabecalho($id_paciente, $empresa, $convenio);
        $inicioRel = '';
        $fimRel = '';

        if($editar == 1){
            $inicioRel = $inicio;
            $fimRel = $fim;
        }

        echo "<p><b>Relat&oacute;rio de Prorroga&ccedil;&atilde;o do Per&iacute;odo:</b><input class='data OBG' type='text' value='$inicioRel' name='inicio' maxlength='10' size='10' />";
        echo "&nbsp;<b>at&eacute;</b><input class='data OBG' type='text' name='fim' value='$fimRel' maxlength='10' size='10' /></p>";

        //////////////problemas ativos
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th colspan='3' >Problemas Ativos</th>";
        echo "</tr></thead>";

        echo "<tr><td colspan='3' ><b>Busca  </b><input type='text' name='busca-problemas-ativos-evolucao' {$acesso} size=100 /></td></tr>";
        echo"<tr><td colspan='3' ><input type='checkbox' id='outros-prob-ativos-evolucao' name='outros-prob-ativos-evolucao' {$acesso} />Outros.<span class='outro_ativo_evolucao' ><input type='text' name='outro-prob-ativo-evolucao' class='outro_ativo_evolucao' {$acesso} size=75/><button id='add-prob-ativo-evolucao' tipo=''  >+ </button></span></td></tr>";
        echo "<tr><td colspan='3' >";

        echo "<table id='problemas_ativos' style='width:95%;'>";

        $this->problemas_ativos($id_paciente, $id_prorrogacao, $editar);

        echo "</table>";
        echo "</td></tr>";
        echo "</table>";
        $this->intercorrencias($id_paciente, $id_prorrogacao, $editar);
        echo "</table>";

        $sql2 = "
                SELECT
                    operadoras_planosdesaude.OPERADORAS_ID
                FROM
                    clientes inner join
                    operadoras_planosdesaude ON (clientes.convenio = operadoras_planosdesaude.PLANOSDESAUDE_ID)				 
                WHERE
                    idClientes = '{$id_paciente}'";
        $result = mysql_fetch_array(mysql_query($sql2));

        if (!in_array($result['OPERADORAS_ID'], [11]))
            \App\Controllers\Score::scoreNead2016($id_paciente, $id_prorrogacao, "prorrogacao", $classificacao_paciente);
        echo "<table class='mytable' width='100%'>";

        $this->exames_fisicos($id_paciente, $id_prorrogacao, $editar);
        $this->modalidade_homecare($mod_home_care);
        echo "<p><b>Materiais e medica&ccedil;&otilde;es conforme c&oacute;pias das prescri&ccedil;&otilde;es m&eacute;dica e de enfermagem enviados.</b></p>";
        $this->profissionais($id_prorrogacao);
        $this->procedimentos($id_prorrogacao);
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th>Deslocamento</th>";
        echo "</tr></thead>";
        if (isset($deslocamento)) {
            $check = "checked='checked'";
        } else {
            $check = '';
        }
        echo"<tr><td ><input type='checkbox' $check name='deslocamento' id='deslocamento'/>Deslocamento da equipe por se tratar de internação em município distante <input name='distancia' class='numerico1' value='{$deslocamento_distancia}' type='text'/>km da sede da empresa.	</td></td></tr>";
        echo "</table>";
        echo "<table class='mytable' style='width:95%;'>";
        if($justificativaProrrogacao != '' & $editar == '1') {
            echo "<thead><tr>";
            echo "<th>Justificativa</th>";
            echo "</tr></thead>";
            echo "<tr><td><textarea cols=65 rows=2 type=text class='OBG' name='justificativaProrrogacao'>{$justificativaProrrogacao}</textarea></td></tr>";
        } else {
            echo "<thead><tr>";
            echo "<th>Quadro Clínico</th>";
            echo "</tr></thead>";
            echo "<tr><td><textarea cols=65 rows=2 type=text class='OBG' name='quadro_clinico'>{$quadroClinico}</textarea></td></tr>";
            echo "<thead><tr>";
            echo "<th>Modalidade de Atendimento Atual</th>";
            echo "</tr></thead>";
            echo "<tr><td><textarea cols=65 rows=2 type=text class='OBG' name='regime_atual'>{$regimeAtual}</textarea></td></tr>";
            echo "<thead><tr>";
            echo "<th>Indicação Técnica para o próximo período</th>";
            echo "</tr></thead>";
            echo "<tr><td><textarea cols=65 rows=2 type=text class='OBG' name='indicacao_tecnica'>{$indicacaoTecnica}</textarea></td></tr>";
        }
        echo "</table>";
        echo alerta();
        if ($editar == 1) {
            echo "<button id='editar_prorrogacao' paciente='{$id_paciente}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
        } else {
            echo "<button id='salvar_prorrogacao' paciente='{$id_paciente}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
        }
        echo "</form>";
        echo" </div>";
    }

    public function modalidade_homecare($mod_home_care) {


        echo "<table width='95%' class='mytable'><thead><tr><th>Modalidade de Home Care</th></tr></thead>
   <tr><td><input type='radio' class='modalidade OBG_RADIO' name='modalidade' value='1' " . (($mod_home_care == '1') ? "checked" : "") . " {$enabled}>Assistencia Domiciliar</input></td></tr>
   <tr><td><input type='radio' class='modalidade' name='modalidade' value='2' " . (($mod_home_care == '2') ? "checked" : "") . " {$enabled}>Interna&ccedil;&atilde;o Domiciliar 6h</input></td></tr>
   <tr><td><input type='radio' class='modalidade' name='modalidade' value='3' " . (($mod_home_care == '3') ? "checked" : "") . " {$enabled}>Interna&ccedil;&atilde;o Domiciliar 12h</input></td></tr>
   <tr><td><input type='radio' class='modalidade' name='modalidade' value='4' " . (($mod_home_care == '4') ? "checked" : "") . " {$enabled}>Interna&ccedil;&atilde;o Domiciliar 24h com respirador</input></td></tr>
   <tr><td><input type='radio' class='modalidade' name='modalidade' value='5' " . (($mod_home_care == '5') ? "checked" : "") . " {$enabled}>Interna&ccedil;&atilde;o Domiciliar 24h sem respirador</input></td></tr>
</table>";
    }

    public function problemas_ativos($id_paciente, $id_prorrogacao, $editar) {
        $sql = "SELECT
                    *
                FROM
                    (SELECT
                            relatorio_prorrogacao_med.id as id,
                            'problemas_relatorio_prorrogacao_med' AS tabela,
                            'RELATORIO_PRORROGACAO_MED_ID' as condicao,
                            DATA
                        FROM
                            relatorio_prorrogacao_med
                        WHERE
                            relatorio_prorrogacao_med.ID = (
                                SELECT
                                    MAX(ID)
                                FROM
                                    relatorio_prorrogacao_med
                                WHERE
                                    relatorio_prorrogacao_med.PACIENTE_ID = $id_paciente
                            )
                        UNION ALL
                            SELECT
                                fichamedicaevolucao.id as id,
                                'problemaativoevolucao' AS tabela,
                                'EVOLUCAO_ID' as condicao,
                                DATA
                            FROM
                                fichamedicaevolucao
                            WHERE
                                fichamedicaevolucao.ID = (
                                    SELECT
                                        MAX(ID)
                                    FROM
                                        fichamedicaevolucao
                                    WHERE
                                        fichamedicaevolucao.PACIENTE_ID = $id_paciente
                                )
                    ) AS result
                ORDER BY
                    result.DATA DESC
                    LIMIT 1 ";

        $result = mysql_query($sql);

        while ($row = mysql_fetch_array($result)) {
            $id = $row['id'];
            $tabela = $row['tabela'];
            $condicao = $row['condicao'];
        }



        if ($editar == 1) {
            $sql1 = "
                SELECT
                    *
                FROM
                    problemas_relatorio_prorrogacao_med
                WHERE
                    problemas_relatorio_prorrogacao_med.RELATORIO_PRORROGACAO_MED_ID = {$id_prorrogacao}
                ";


        }else{
            $sql1 = "
                SELECT
                    *
                FROM
                    $tabela
                WHERE
                    $condicao = '$id'
                ";

        }

        $html = "";
        $result1 = mysql_query($sql1);
        while ($row1 = mysql_fetch_array($result1)) {
            $html .= "<tr>
        <td class='prob-ativos-evolucao' desc='{$row1['DESCRICAO']}' cod='{$row1['CID_ID']}' colspan='2'>
        <img class='del-prob-ativos' border='0' title='Excluir' src='../utils/delete_16x16.png'>  CID - {$row1['CID_ID']}  {$row1['DESCRICAO']}
        <textarea class='OBG' rows='3' cols='70'>{$row1['OBSERVACAO']}</textarea>
        </td>
        </tr>";
        }
        echo $html;
    }

    public function intercorrencias($id_prorrogacao, $editar) {

        $sql_intercorrencias = "SELECT * FROM `intercorrenciasfichaevolucao` WHERE `FICHAMEDICAEVOLUCAO_ID` = {$id_prorrogacao}";
        $resultado_intercorrencias = mysql_query($sql_intercorrencias);
        $cont_intercorrencias = 1;

        $num_linhas_intercorrencias = mysql_num_rows($resultado_intercorrencias);
        echo "<table class='mytable' width='95%'>";
        echo "<thead><tr>";
        echo "<th colspan='3' >Intercorrências desde a última visita médica<th>";
        echo "</tr></thead>";

        echo "<tr><td>";

        echo"<select name=intercorrencias class='COMBO_OBG'><option value ='-1'>  </option><option value ='n' " . (($num_linhas_intercorrencias <= 0) ? "selected" : "") . ">N&atilde;o</option><option value ='s' " . (($num_linhas_intercorrencias > 0) ? "selected" : "") . ">Sim</option></select></td><td colspan='2'><span name='intercorrencias' id='intercorrencias_span'>";
        echo "<b>Descrição:</b><input type='text' name='intercorrencias' class='intercorrencias' size=50/><button id='add-intercorrencias' tipo=''  > + </button></span></td></tr>";

        while ($row = mysql_fetch_array($resultado_intercorrencias)) {
            echo "<tr><td id='inter' class='interc' name='int{$cont_intercorrencias}'><img src='../utils/delete_16x16.png' class='del-intercorrencias-ativas' title='Excluir' border='0'><b>Descrição: </b>{$row['DESCRICAO']}</td></tr>";
            $cont_intercorrencias++;
        }
        echo "<tr><thead id='intercorrencias_tabela'></thead></tr>";
        echo "</table>";
    }

    public function exames_fisicos($id_paciente, $id_prorrogacao, $editar) {
        if (isset($editar)) {
            $sql = "
                SELECT
                    *
                FROM
                    relatorio_prorrogacao_med
                WHERE
                    `ID` = {$id_prorrogacao}
                ";
            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {
                $pa_sistolica_min = $row['PA_SISTOLICA_MIN'];
                $pa_sistolica_max = $row['PA_SISTOLICA_MAX'];
                $pa_sistolica = $row['PA_SISTOLICA'];
                $pa_sistolica_sinal = $row['PA_SISTOLICA_SINAL'];
                $pa_diastolica_min = $row['PA_DIASTOLICA_MIN'];
                $pa_diastolica_max = $row['PA_DIASTOLICA_MAX'];
                $pa_diastolica = $row['PA_DIASTOLICA'];
                $pa_diastolica_sinal = $row['PA_DIASTOLICA_SINAL'];
                $fc = $row["FC"];
                $fc_max = $row["FC_MAX"];
                $fc_min = $row["FC_MIN"];
                $fc_sinal = $row["FC_SINAL"];
                $fr = $row["FR"];
                $fr_max = $row["FR_MAX"];
                $fr_min = $row["FR_MIN"];
                $fr_sinal = $row["FR_SINAL"];
                $temperatura_max = $row["TEMPERATURA_MAX"];
                $temperatura_min = $row["TEMPERATURA_MIN"];
                $temperatura = $row["TEMPERATURA"];
                $temperatura_sinal = $row["TEMPERATURA_SINAL"];
                $estd_geral = $row["ESTADO_GERAL"];
                $mucosa = $row['MUCOSA'];
                $mucosaobs = $row['MUCOSA_OBS'];
                $escleras = $row['ESCLERAS'];
                $esclerasobs = $row['ESCLERAS_OBS'];
                $respiratorio = $row['PADRAO_RESPIRATORIO'];
                $respiratorioobs = $row['PADRAO_RESPIRATORIO_OBS'];
                $novo_exame = $row['NOVOS_EXAMES'];
                $impressao = $row['IMPRESSAO'];
                $diagnostico = $row['PLANO_DIAGNOSTICO'];
                $terapeutico = $row['PLANO_TERAPEUTICO'];
                $dor_sn = $row['DOR'];
                $outro_resp = $row['OUTRO_RESP'];
                $oximetria_pulso_max = $row["OXIMETRIA_PULSO_MAX"];
                $oximetria_pulso_min = $row["OXIMETRIA_PULSO_MIN"];
                $tipo_oximetria_min = $row["OXIMETRIA_PULSO_TIPO_MIN"];
                $litros_min = $row["OXIMETRIA_PULSO_LITROS_MIN"];
                $via_oximetria_min = $row["OXIMETRIA_PULSO_VIA_MIN"];
                $tipo_oximetria_max = $row["OXIMETRIA_PULSO_TIPO_MAX"];
                $litros_max = $row["OXIMETRIA_PULSO_LITROS_MAX"];
                $via_oximetria_max = $row["OXIMETRIA_PULSO_VIA_MAX"];
                $glicemia_capilar_min = $row["GLICEMIA_CAPILAR_MIN"];
                $glicemia_capilar_max = $row["GLICEMIA_CAPILAR_MAX"];
                $oximetria_sinal = $row["OXIMETRIA_SINAL"];
                $oximetria_pulso = $row["OXIMETRIA_PULSO"];
                $tipo_oximetria = $row["OXIMETRIA_PULSO_TIPO"];
                $litros = $row["OXIMETRIA_PULSO_LITROS"];
                $via_oximetria = $row["OXIMETRIA_PULSO_VIA"];
            }
        } else {
            $sql = "select * from fichamedicaevolucao where PACIENTE_ID = {$id_paciente} ORDER BY DATA DESC LIMIT 1";
            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {
                $id_evolucao      = $row['ID'];
                $pa_sistolica_min = $row['PA_SISTOLICA_MIN'];
                $pa_sistolica_max = $row['PA_SISTOLICA_MAX'];
                $pa_sistolica = $row['PA_SISTOLICA'];
                $pa_sistolica_sinal = $row['PA_SISTOLICA_SINAL'];
                $pa_diastolica_min = $row['PA_DIASTOLICA_MIN'];
                $pa_diastolica_max = $row['PA_DIASTOLICA_MAX'];
                $pa_diastolica = $row['PA_DIASTOLICA'];
                $pa_diastolica_sinal = $row['PA_DIASTOLICA_SINAL'];
                $fc = $row["FC"];
                $fc_max = $row["FC_MAX"];
                $fc_min = $row["FC_MIN"];
                $fc_sinal = $row["FC_SINAL"];
                $fr = $row["FR"];
                $fr_max = $row["FR_MAX"];
                $fr_min = $row["FR_MIN"];
                $fr_sinal = $row["FR_SINAL"];
                $temperatura_max = $row["TEMPERATURA_MAX"];
                $temperatura_min = $row["TEMPERATURA_MIN"];
                $temperatura = $row["TEMPERATURA"];
                $temperatura_sinal = $row["TEMPERATURA_SINAL"];
                $estd_geral = $row["ESTADO_GERAL"];
                $mucosa = $row['MUCOSA'];
                $mucosaobs = $row['MUCOSA_OBS'];
                $escleras = $row['ESCLERAS'];
                $esclerasobs = $row['ESCLERAS_OBS'];
                $respiratorio = $row['PADRAO_RESPIRATORIO'];
                $respiratorioobs = $row['PADRAO_RESPIRATORIO_OBS'];
                $novo_exame = $row['NOVOS_EXAMES'];
                $impressao = $row['IMPRESSAO'];
                $diagnostico = $row['PLANO_DIAGNOSTICO'];
                $terapeutico = $row['PLANO_TERAPEUTICO'];
                $dor_sn = $row['DOR'];
                $outro_resp = $row['OUTRO_RESP'];
                $oximetria_pulso_max = $row["OXIMETRIA_PULSO_MAX"];
                $oximetria_pulso_min = $row["OXIMETRIA_PULSO_MIN"];
                $tipo_oximetria_min = $row["OXIMETRIA_PULSO_TIPO_MIN"];
                $litros_min = $row["OXIMETRIA_PULSO_LITROS_MIN"];
                $via_oximetria_min = $row["OXIMETRIA_PULSO_VIA_MIN"];
                $tipo_oximetria_max = $row["OXIMETRIA_PULSO_TIPO_MAX"];
                $litros_max = $row["OXIMETRIA_PULSO_LITROS_MAX"];
                $via_oximetria_max = $row["OXIMETRIA_PULSO_VIA_MAX"];
                $glicemia_capilar_min = $row["GLICEMIA_CAPILAR_MIN"];
                $glicemia_capilar_max = $row["GLICEMIA_CAPILAR_MAX"];
                $oximetria_sinal = $row["OXIMETRIA_SINAL"];
                $oximetria_pulso = $row["OXIMETRIA_PULSO"];
                $tipo_oximetria = $row["OXIMETRIA_PULSO_TIPO"];
                $litros = $row["OXIMETRIA_PULSO_LITROS"];
                $via_oximetria = $row["OXIMETRIA_PULSO_VIA"];
            }
        }
        $html .= "<input type='hidden' name='id_evolucao' value={$id_evolucao} />"; //Input para passar o id da ultima evolu��o
        $html .= "<input type='hidden' name='id_paciente' value={$id_paciente} />"; //Input para passar o id do paciente
        $html.= "<table class='mytable' width='95%'>";
        $html.= "<thead><tr><th colspan='4'>EXAMES ".htmlentities("FÍSICOS DESDE A ÚLTIMA VISITA MÉDICA")."</th></tr></thead>";

        $html.= "<tr><td><b>Estado Geral:</b>  </td><td colspan='3'>
  		<input type='radio' name='estd_geral' size=50 value='1'" . (($estd_geral == 1) ? " checked" : "") .
            "/>Bom &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type='radio' name='estd_geral' size=50 value='2'" . (($estd_geral == 2) ? " checked" : "") . "/>Regular &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  		<input type='radio' name='estd_geral' size=50 value='3'" . (($estd_geral == 3) ? " checked" : "") . "/>Ruim
		</td></tr>";

        $html.= "<tr><td><b>Mucosas:</b>  </td><td colspan='3'>
		<input type='radio' name='mucosa' class='mucosa' size=50 value='s' " . (($mucosa == 's') ? "checked" : "") . " {$enabled} />Coradas &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  		<input type='radio' name='mucosa' class='mucosa' size=50 value='n' " . (($mucosa == 'n') ? "checked" : "") . " {$enabled} />Descoradas
  		<span class='mucosa1'><select name='mucosaobs' class='COMBO_OBG' style='background-color:transparent;'>
  		<option value=''></option><option value='+' " . (($mucosaobs == '+') ? "selected" : "") . " >+</option><option value='++' " . (($mucosaobs == '++') ? "selected" : "") . " >++</option><option value='+++' " . (($mucosaobs == '+++') ? "selected" : "") . ">+++</option><option value='++++'" . (($mucosaobs == '++++') ? "selected" : "") . ">++++</option></select></span></td></tr>";

        $html.= "<tr><td><b>Escleras:</b>  </td><td colspan='3'>
		<input type='radio' name='escleras' class='escleras' size=50 value='s' " . (($escleras == 's') ? "checked" : "") . " {$enabled} />Anict&eacute;ricas &nbsp;&nbsp;&nbsp;&nbsp; <input type='radio' name='escleras' class='escleras' size=50 value='n' " . (($escleras == 'n') ? "checked" : "") . " {$enabled} />Ict&eacute;ricas
  		<span class='escleras1'><select name='esclerasobs' class='COMBO_OBG' style='background-color:transparent;'>
  		<option value=''></option><option value='+' " . (($esclerasobs == '+') ? "selected" : "") . " >+</option><option value='++' " . (($esclerasobs == '++') ? "selected" : "") . " >++</option><option value='+++' " . (($esclerasobs == '+++') ? "selected" : "") . ">+++</option><option value='++++'" . (($esclerasobs == '++++') ? "selected" : "") . ">++++</option></select></span></td></tr>";

        $html.= "<tr><td><b>Padr&atilde;o Respirat&oacute;iro:</b>  </td>
  		<td colspan='3'><input type='radio' name='respiratorio' class='respiratorio' size=50 value='s' "
            . (($respiratorio == 's') ? "checked" : "") . " {$enabled} />
  		Eupn&eacute;ico &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  		<input type='radio' name='respiratorio' class='respiratorio' size=50 value='n' " . (($respiratorio == 'n') ? "checked" : "") . " {$enabled} />
  		Taqui/Dispn&eacute;ico
  		<select class='exibir_selecao respiratorio1' name='respiratorioobs' style='background-color:transparent;'>
  		<option value=''></option>
  		<option value='+' " . (($respiratorioobs == '+') ? "selected" : "") . " >+</option>
  		<option value='++' " . (($respiratorioobs == '++') ? "selected" : "") . " >++</option>
  		<option value='+++' " . (($respiratorioobs == '+++') ? "selected" : "") . ">+++</option>
  		<option value='++++'" . (($respiratorioobs == '++++') ? "selected" : "") . ">++++</option>
  		</select>
		<input type='radio' name='respiratorio' class='respiratorio' size=50 value='1' " . (($respiratorioobs == '1') ? "checked" : "") . " {$enabled} />
  		Outro<br/><span class='outro_respiratorio obs_exame'><textarea cols=30 rows=2 name='outro_respiratorio' >{$row['OUTRO_RESP']}</textarea></span></td></tr>";

        //Oximetria de Pulso
        $html.= "<tr><td width=20% ><b>Oximetria de Pulso: </b></td>
  		<td width=25% colspan='3'>
  		<input type='texto' class='numerico1' name='oximetria_pulso' size='4' value='$oximetria_pulso' {$acesso} size='4' />
  		<select name='tipo_oximetria' class='COMBO_OBG'>
  		<option value=''></option>
  		<option value='1'" . (($tipo_oximetria == '1') ? "selected" : "") . ">Ar Ambiente</option>
  		<option value='2'" . (($tipo_oximetria == '2') ? "selected" : "") . ">O2 Suplementar</option>
  		</select>
  		<span id='suplementar_span' name='suplementar' style='display: inline;'>
  		<input type='texto' size='3' class='numerico1' value='$litros' name='litros'/>l/min
  		<b>Via</b>
  		<select name='via_oximetria' class='COMBO_OBG' >
  		<option value=''></option>
  		<option value='1'" . (($oximetria_via == '2') ? "selected" : "") . ">Cateter Nasal</option>
  		<option value='2'" . (($oximetria_via == '2') ? "selected" : "") . ">Cateter em Traqueostomia</option>
  		<option value='3'" . (($oximetria_via == '2') ? "selected" : "") . ">Máscara de Venturi</option>
  		<option value='4'" . (($oximetria_via == '2') ? "selected" : "") . ">Ventilação Mecânica</option>
  		</select>
  		</span>
  		</td></tr>";

        $html.= "<tr><td width=20%><b>Pressão Arterial (sist&oacute;lica):</b></td><td colspan='3'><input type='texto' class='numerico1 OBG' size='6' name='pa_sistolica' value='{$pa_sistolica}'   />mmhg</td></tr>";
        $html.= "<tr><td width=20%><b>Pressão Arterial (diast&oacute;lica):</b></td><td colspan='3'><input type='texto' class='numerico1 OBG' size='6' name='pa_diastolica' value='{$pa_diastolica}'   />mmhg</td></tr>";
        $html.= "<tr><td width=20%><b>Frequência Cardíaca: </b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='fc'  value='{$fc}'  />bpm</td></tr>";
        $html.= "<tr><td width=20%><b>Frequência Respiratória:</b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='fr' value='{$fr}'  />irpm</td></tr>";
        $html.= "<tr><td width=20%><b>Temperatura:</b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='temperatura' value='{$temperatura}'  {$acesso} />&deg;C</td></tr>";

        $html.= "<tr><td colspan='4' ><b>DOR:</b><select name=dor_sn class='COMBO_OBG'>
		<option value =''>  </option>
		<option value ='n'" . (($dor_sn == 'n') ? "selected" : "") . ">N&atilde;o</option>
		<option value ='s'" . (($dor_sn == 's') ? "selected" : "") . ">Sim</option></select>&nbsp;&nbsp;<span name='dor' id='dor_span'>
		<b>Local:</b><input type='text' name='dor' class='dor' {$acesso} size=50/><button id='add-dor' tipo=''  > + </button></span></td></tr>";

        $html.= "<tr><td colspan='4' ><table id='dor' style='width:100%;'>";

        if (isset($editar)) {
            $sql = "select * from dor_relatorio_prorrogacao_med where `RELATORIO_PRORROGACAO_MED_ID` = {$id_prorrogacao} ";
            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {
                if ($row['PADRAO_ID'] == 5) {
                    $display = "style='display:inline'";
                    $outro_value=$row['PADRAO'];
                } else {
                    $display = "style='display:none'";
                    $outro_value='';
                }
                $html.= "<tr>
                         <td class='dor1' name='{$row['DESCRICAO']}'>
                             <img src='../utils/delete_16x16.png' class='del-dor-ativos' title='Excluir' border='0' >
                             <b>Local: </b>
                             <input type='text' value='{$row['DESCRICAO']}' disabled='disabled'></input>
                             &nbsp;&nbsp; 
                             Escala analogica: 
                             <select name='dor" . "{$row['DESCRICAO']}" . "' class='COMBO_OBG' style='background-color:transparent;'>
                                <option value='-1' selected></option>
                                <option value='1' " . (($row['ESCALA'] == '1') ? "selected" : "") . ">1</option>
                                <option value='2' " . (($row['ESCALA'] == '2') ? "selected" : "") . ">2</option>
                                <option value='3' " . (($row['ESCALA'] == '3') ? "selected" : "") . ">3</option>
                                <option value='4' " . (($row['ESCALA'] == '4') ? "selected" : "") . ">4</option>
                                <option value='5' " . (($row['ESCALA'] == '5') ? "selected" : "") . ">5</option>
                                <option value='6' " . (($row['ESCALA'] == '6') ? "selected" : "") . ">6</option>
                                <option value='7' " . (($row['ESCALA'] == '7') ? "selected" : "") . ">7</option>
                                <option value='8' " . (($row['ESCALA'] == '8') ? "selected" : "") . ">8</option>
                                <option value='9' " . (($row['ESCALA'] == '9') ? "selected" : "") . ">9</option>
                                <option value='10' " . (($row['ESCALA'] == '10') ? "selected" : "") . ">10</option>
                             </select> 
                             &nbsp;&nbsp;
                             Padr&atilde;o da dor
                             <select name='padrao_dor' class='padrao_dor COMBO_OBG' style='background-color:transparent;'>
                                <option value='-1' selected></option>
                                <option value='1' " . (($row['PADRAO_ID'] == '1') ? "selected" : "") . ">Pontada</option>
                                <option value='2' " . (($row['PADRAO_ID'] == '2') ? "selected" : "") . ">Queimação</option>
                                <option value='3' " . (($row['PADRAO_ID'] == '3') ? "selected" : "") . ">Latejante</option>
                                <option value='4' " . (($row['PADRAO_ID'] == '4') ? "selected" : "") . ">Peso</option>
                                <option value='5' " . (($row['PADRAO_ID'] == '5') ? "selected" : "") . ">Outro</option>
                            </select></b>
                            <input type='text' {$display} value='{$outro_value}' name='padraodor'></input>
                            </td></tr>";
            }
        } else {
            $sql = "select * from dorfichaevolucao where FICHAMEDICAEVOLUCAO_ID = {$maior_evolucao} ";
            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {
                if($row['PADRAO_ID'] == 5 || $row['PADRAO_ID'] == 0) {
                    $display = "style='display:inline'";
                    $outro_value=$row['PADRAO'];
                } else {
                    $display = "style='display:none'";
                    $outro_value='';
                }

                $html.= "<tr>
                            <td class='dor1' name='{$row['DESCRICAO']}'>
                                 <img src='../utils/delete_16x16.png' class='del-dor-ativos' title='Excluir' border='0' >
                                 <b>Local: </b>
                                 <input type='text' value='{$row['DESCRICAO']}' disabled='disabled'></input>&nbsp;&nbsp; 
                                 Escala analogica: 
                                 <select name='dor" . "{$row['DESCRICAO']}" . "' class='COMBO_OBG' style='background-color:transparent;'>
                                    <option value='-1' selected></option>
                                    <option value='1' " . (($row['ESCALA'] == '1') ? "selected" : "") . ">1</option>
                                    <option value='2' " . (($row['ESCALA'] == '2') ? "selected" : "") . ">2</option>
                                    <option value='3' " . (($row['ESCALA'] == '3') ? "selected" : "") . ">3</option>
                                    <option value='4' " . (($row['ESCALA'] == '4') ? "selected" : "") . ">4</option>
                                    <option value='5' " . (($row['ESCALA'] == '5') ? "selected" : "") . ">5</option>
                                    <option value='6' " . (($row['ESCALA'] == '6') ? "selected" : "") . ">6</option>
                                    <option value='7' " . (($row['ESCALA'] == '7') ? "selected" : "") . ">7</option>
                                    <option value='8' " . (($row['ESCALA'] == '8') ? "selected" : "") . ">8</option>
                                    <option value='9' " . (($row['ESCALA'] == '9') ? "selected" : "") . ">9</option>
                                    <option value='10' " . (($row['ESCALA'] == '10') ? "selected" : "") . ">10</option>
                                 </select> &nbsp;&nbsp;
                            Padr&atilde;o da dor: 
                            <select name='padraodor' class='padrao_dor COMBO_OBG' style='background-color:transparent;'>
                                    <option value='-1' selected></option>
                                    <option value='0' " . (($row['PADRAO_ID'] == '0') ? "selected" : "") . ">Outro</option>
                                    <option value='1' " . (($row['PADRAO_ID'] == '1') ? "selected" : "") . ">Pontada</option>
                                    <option value='2' " . (($row['PADRAO_ID'] == '2') ? "selected" : "") . ">Queimação</option>
                                    <option value='3' " . (($row['PADRAO_ID'] == '3') ? "selected" : "") . ">Latejante</option>
                                    <option value='4' " . (($row['PADRAO_ID'] == '4') ? "selected" : "") . ">Peso</option>
                                    <option value='5' " . (($row['PADRAO_ID'] == '5') ? "selected" : "") . ">Outro</option>
                             </select>
                             <input type='text' {$display} value='{$outro_value}' name='padraodor'></input>
                          </td>
                        </tr>";
            }
        }
        $html.= "</table></td></tr>";

        $html.= "</table>";

        echo $html;
    }

    public function profissionais($id_prorrogacao,$alta) {
        $frequencia = "Frequência";
        $tabela = "RELATORIO_PRORROGACAO_MED_ID";
        if(isset($alta)){
            $condicao = "AND ALTA = 1";
            $frequencia = "Sugestão de Frequência";
            $tabela = "RELATORIO_ALTA_MED_ID";
        }
        $sql_profissionais = "
            SELECT
                *
            FROM
                profissionais_relatorio_prorrogacao_med
            WHERE
                $tabela = {$id_prorrogacao}
                ";
        $result_profissional = mysql_query($sql_profissionais);
        $profissional = '';

        while ($row = mysql_fetch_array($result_profissional)) {
            $profissional[$row['CUIDADOSESPECIAIS_ID']] = array("id_cuidado" => $row['CUIDADOSESPECIAIS_ID'], "descricao" => $row['DESCRICAO'], "justificativa" => $row['JUSTIFICATIVA'], "frequencia" => $row['FREQUENCIA_ID'], "justificativa_id" => $row['JUSTIFICATIVA_ID']);
        }

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th >Profissionais</th>";
        echo "</tr></thead>";
        echo "<tr><td>Outros<span class='outro_profissionais' ><input type='text' name='outro-prof' class='outros-prof' {$acesso} size=75/><button id='add-profissional' tipo=''  >+ </button></span>";
        echo"&nbsp;&nbsp;&nbsp;<input type='checkbox' id='check_especialista' name='check_especialista' >Especialista</input><span class='especialista'><button id='add-especialista' tipo=''  >+ </button>{$this->especialidade_medica(2)}</span></td></tr>";
        echo "</table>";


        echo "<table id='profissionais' class='mytable' style='width:95%;'>";
        echo "<tr><td>";
        echo "<tr bgcolor='grey'><td width='30%'>Profissional</td><td width='17%'>".htmlentities("{$frequencia}")."</td><td width='40%'>Justificativa</td></tr>";

        $sql = "
            SELECT
                * 
            FROM 
                cuidadosespeciais 
            WHERE 
                id not in (15,16,17,18,19,20,21) {$condicao}";
        $result = mysql_query($sql);
        $disabled = "disabled";
        while ($row = mysql_fetch_array($result)) {
            $id_grupo = $row['GRUPO'];
            if (array_key_exists($row['id'], $profissional)) {
                $check = "checked='checked'";
                $id_frequencia = $profissional[$row['id']]['frequencia'];
                $justificativa = $profissional[$row['id']]['justificativa'];
                $justificativa_id = $profissional[$row['id']]['justificativa_id'];
                $disabled = 3;
            } else {
                $check = '';
                $id_frequencia = '';
                $justificativa = '';
                $justificativa_id = '';
                $disabled = '';
            }
            if ($row['JUSTIFICATIVA'] == 'N') {
                echo "<tr><td><input type='checkbox' $check value={$row['id']} id_especialidade='-1' class='profissional'>" . $row['cuidado'] . "</input></td><td>" . $this->frequencia($disabled, $id_frequencia) . "</td><td><textarea cols=62 rows=2 type=text  name='justificativa' size='100' opcao='1' readonly=''>{$justificativa}</textarea></td></tr>";
            }else{
                echo "<tr><td><input type='checkbox' $check value={$row['id']} id_especialidade='-1' class='profissional'>" . $row['cuidado'] . "</input></td><td>" . $this->frequencia($disabled, $id_frequencia) . "</td><td>" . $this->justificativa($disabled, $justificativa_id, $justificativa, $id_grupo) . "</td></tr>";
            }
        }
        foreach ($profissional as $chave => $profi) {
            if ($chave == '1111') {
                $check = "checked='checked'";
                $id_frequencia = $profi['frequencia'];
                $justificativa = $profi['justificativa'];
                $disabled = 3;
                echo "<tr><td><input type='checkbox' $check value='1111' id_especialidade='-1' class='profissional'>" . $profi['descricao'] . "</input></td><td>" . $this->frequencia($disabled, $id_frequencia) . "</td><td><textarea cols=65 rows=2 type=text  name='justificativa' size='100' >{$justificativa}</textarea></td></tr>";
            }
        }

        echo "</td></tr>";
        echo "</table>";
    }

    public function procedimentos($id_prorrogacao) {
        $sql_procedimentos = "
            SELECT
                *
            FROM
                procedimentos_relatorio_prorrogacao_med
            WHERE
                RELATORIO_PRORROGACAO_MED_ID = {$id_prorrogacao}
                ";
        $result_procedimentos = mysql_query($sql_procedimentos);
        $procedimento = '';
        $num_linhas_procedimento = mysql_num_rows($result_procedimentos);

        while ($row = mysql_fetch_array($result_procedimentos)) {
            $procedimento[$row['PROCEDIMENTOS_ID']] = array("id_procedimento" => $row['PROCEDIMENTOS_ID'], "descricao" => $row['DESCRICAO'], "justificativa" => $row['JUSTIFICATIVA']);
        }

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th >Procedimentos - <select name='procedimento_sn' class='procedimento_sn COMBO_OBG'><option value ='-1' >  </option><option value ='1' " . (($num_linhas_procedimento > 0) ? "selected" : "") . ">Sim</option><option value ='2' " . (($num_linhas_procedimento <= 0) ? "selected" : "") . ">N&atilde;o</option></select></th>";
        echo "</tr></thead>";

        //echo "<tr><td>";
        echo "</table>";
        echo "<table id='procedimento' class='mytable' style='width:95%;'>";
        echo "<tr bgcolor='grey'><td width='30%'>Procedimento</td><td>Justificativa</td></tr>";

        $sql = "
            SELECT
                * 
            FROM 
                procedimentos
            WHERE
                ID <= 7";
        $result = mysql_query($sql);
        $disabled = "disabled";
        while ($row = mysql_fetch_array($result)) {
            if (array_key_exists($row['ID'], $procedimento)) {
                $check = "checked='checked'";
                $justificativa = $procedimento[$row['ID']]['justificativa'];
                $disabled = 3;
            } else {
                $check = '';
                $justificativa = '';
                $disabled = '';
            }
            echo "<tr><td><input type='checkbox' $check value='{$row['ID']}' class='procedimento-check'>" . $row['PROCEDIMENTO'] . "</input></td><td><textarea cols=65 rows=2 type=text  name='just_procedimento' size='100'>{$justificativa}</textarea></td></tr>";
        }


        echo "</td></tr>";
        echo "</table>";
    }

    /* echo "<table class='mytable' style='width:95%;'>";
      echo "<thead><tr>";
      echo "<th >Procedimento</th>";
      echo "</tr></thead>";
      echo"<tr><td>Outros<span class='span-procedimentos' ><input type='text' name='nome-procedimento' class='outros-prof' {$acesso} size=75/><button id='add-procedimento' tipo=''  >+ </button></span></td></tr>";

      $sql_procedimento = "
      SELECT
     *
      FROM
      procedimentos_relatorio_prorrogacao_med
      WHERE
      ID = {$id_prorrogacao}
      ";
      $result_procedimento = mysql_query($sql_procedimento);
      $procedimento = '';


      echo"<tr><td><table style='width:95%;' id='procedimentos'>";
      echo "<tr bgcolor='grey'><td width='39%'>Procedimento</td><td>Justificativa</td></tr>";
      echo" <tr><td  cod='0010'><input type='checkbox' class='procedimento-check'> Troca de c&acirc;nula de traqueostomia e do fixador da mesma.  </input></td><td><textarea cols=65 rows=2 type=text  name='justificativa' readonly='readonly' size='100' ></textarea></td></tr>";
      while ($row = mysql_fetch_array($result_procedimento)) {
      if (isset($row['ID'])) {
      $check = "checked='checked'";
      } else {
      $check = '';
      }
      echo "<tr><td  cod='0010'><input type='checkbox' $check class='procedimento-check'> {$row['DESCRICAO']} </input></td><td><textarea cols=65 rows=2 type=text  name='justificativa' readonly='readonly' size='100' >{$row['JUSTIFICATIVA']}</textarea></td></tr>";
      }

      echo"</table></td></tr> ";
      echo "</table>";
      } */

////////////////////////////jeferson 2013-06-12 fim
////////////////Eriton 19-06-2013////////////////////

    public static function tabelaNEAD($id = 0, $paciente = 0, $viewmode = false, $printmode = false) {
        if($printmode === false){
            $html = "   <table class='mytable' style='width:95%;'>";
            $html .= "      <thead>
                                <tr>
                                   <th colspan='3'>Tabela de Manutenção de Internação Domiciliar-NEAD<sup>7<sup></th>
                                </tr>
                            </thead>";
        }

        $ultimosRelatorios = [];
        $relatorioIds = self::getUltimosRelatoriosByPacienteId($paciente);
        if($relatorioIds){
            while($row = mysql_fetch_array($relatorioIds)){
                $ultimosRelatorios[] = $row['relId'];
            }
        }

        if ($id !== 0) {
            if ($printmode === false && $paciente === 0) {
                $result2 = self::getScorePacienteById($id, $printmode);
            } else {
                $result2 = self::getScorePacienteById($paciente, $printmode, $ultimosRelatorios);
            }

            $elemento = [];
            $quadros = [];
            $id_relatorio = 0;

            while ($row = mysql_fetch_array($result2)) {


                if ($printmode === false) {
                    $elemento[$row['ID_ITEM_SCORE']] = array("id_elemento" => $row['ID_ITEM_SCORE'], "peso" => $row['PESO']);
                } else {
                    $elemento[$row['ID_RELATORIO']][$row['ID_ITEM_SCORE']] = true;
                    $elemento[$row['ID_RELATORIO']]['observacao'] = $row['OBSERVACAO'];
                    if ($id_relatorio !== $row['ID_RELATORIO'] || count($quadros) === 0) {
                        $quadros[] = array("id_relatorio" => $row['ID_RELATORIO'], "data" => $row['DATA']);
                        $id_relatorio = $row['ID_RELATORIO'];
                    }
                }
                $observacao = $row['OBSERVACAO'];
            }

        }

        $cont = 0;

        $i = 0;
        $cont1 = 1;
        if($printmode === false){
            $html .= "<tr bgcolor='grey'><td><b>Descri&ccedil;&atilde;o</b></td><td colspan='3'><b>Itens da Avalia&ccedil;&atilde;o</b></td></tr>";
            $result = self::fillScoreNEAD7();
            while ($row = mysql_fetch_array($result)) {
                if (is_array($elemento)) {
                    if (array_key_exists($row['ID'], $elemento)) {
                        $total_score_nead += $row['PESO']*3;
                        $check = "checked='checked'";
                    } else {
                        $check = '';
                    }
                } else {
                    $check = '';
                }

                if ($cont1 == 1) {
                    $i = $row['QTD'] + 1;
                    $cont = $cont1;
                } else {
                    $i = 0;
                }

                if ($cont1 == 1) {
                    if ($x == "#9de1de")
                        $x = '';
                    else
                        $x = "#9de1de";
                    $html .= "<tr bgcolor='{$x}'><td ROWSPAN='{$i}'>{$row['dsc']}</td></tr>";
                }

                $val_score = '';
                if (!empty($id)) {
                    if ($total_score_nead < 6) {
                        $justificativa = "Programação de Alta em ".( $viewmode === false ? "<input type='date' name='alta' value='".implode("-",array_reverse(explode("/",substr($observacao,-10))))."' size='10' />" : "<b>".substr($observacao,-10)."</b>");
                    } else if ($total_score_nead >= 6 && $total_score_nead <= 15) {
                        $justificativa = "Manter com até 06 h ou visitas";
                    } else if ($total_score_nead >= 16 && $total_score_nead <= 24) {
                        $justificativa = "Manter com até 12 h";
                    } else if ($total_score_nead >= 24) {
                        $justificativa = "Manter com mais de 12 h";
                    }
                    $val_score = "Score do paciente segundo tabela NEAD=" . $total_score_nead . ". " . $justificativa;
                }
                if($viewmode === false){
                    $html .= "<tr bgcolor='{$x}' >
                                    <td colspan='2'>
                                       <span class='limpar_nead7' title='Desmarcar'>
                                         <img src='../utils/desmarcar16x16.png' width='16' title='Desmarcar' border='0'>
                                       </span>
                                        <input type='radio' {$check} class='nead7' name='" . $row['dsc'] . "' value='" . $row['PESO'] . "' cod_item='" . $row['ID'] . "' > 
                                        {$row['DESCRICAO']} - <b>Peso: {$row['PESO']}</b>
                                    </td>
                                </tr>";
                } else{
                    $html .= "<tr bgcolor='{$x}' >
                                    <td colspan='2'>
                                       <span>
                                         ".($check !== '' ? "<img src='../utils/processada_16x16.png' width='16' border='0'>" : "<img src='../utils/cancelada_16x16.png' width='16' border='0'>")."
                                       </span>
                                        {$row['DESCRICAO']} - <b>Peso: {$row['PESO']}</b>
                                    </td>
                                </tr>";
                }
                $cont1++;

                if ($cont1 > $row['QTD'])
                    $cont1 = 1;
            }
        }else{
            $html .= self::fillGridProrrogacoes($quadros, $elemento);
        }
        if($printmode === false){
            $html .= "<tr bgcolor='grey'>
                    <td colspan='3' aling='center'>
                      <b>Total do Score Segundo a Tabela NEAD<sup>7</sup></b>    
                      <input type='text' readonly size='2' value='{$total_score_nead}' id='total_score_nead' />
                      <label id='justificativa_nead'>{$val_score}</label>
                    </td>
                </tr>";
        }
        $html .= "</table>";
        return $html;
    }

    private static function getUltimosRelatoriosByPacienteId($paciente, $tipoRel = "P", $limit = 6)
    {
        $data = "ID";
        $table = "relatorio_prorrogacao_med";
        $cond = "PACIENTE_ID = {$paciente}";
        if($tipoRel != "P") {
            switch ($tipoRel) {
                case "C":
                    $data = "id";
                    $table = "capmedica";
                    $cond = "paciente = {$paciente}";
                    break;
                case "E":
                    $data = "ID";
                    $table = "fichamedicaevolucao";
                    $cond = "PACIENTE_ID = {$paciente}";
                    break;
            }
        }
        return mysql_query("SELECT
                                {$data} AS relId
                            FROM
                                {$table}
                            WHERE
                                {$cond}
                            ORDER BY {$data} DESC
                            LIMIT {$limit}");
    }

    private static function getScorePacienteById($id,$printmode, $ultimosRelatorios = 0) {
        $cond = " sp.ID_PACIENTE = {$id} ";
        if($ultimosRelatorios != 0) {
            if (count($ultimosRelatorios) > 0) {
                $ultimosRelatorios = implode(',', $ultimosRelatorios);
                $cond = "sp.ID_PACIENTE = {$id} AND sp.ID_RELATORIO IN ({$ultimosRelatorios}) ";
            }
        }
        $sql = "SELECT
                    sp.*
                FROM
                    score_item AS si
                INNER JOIN score_paciente AS sp ON (si.ID = sp.ID_ITEM_SCORE)
                INNER JOIN relatorio_prorrogacao_med AS rpm ON (sp.ID_RELATORIO = rpm.ID)
                WHERE
                    si.SCORE_ID = 4
                    AND ".(!$printmode ? " sp.ID_RELATORIO = '{$id}'" : $cond);
        return mysql_query($sql);
    }

    private static function fillScoreNEAD7($mode = null){
        if($mode === 'onlyitens'){
            return mysql_query("SELECT
                                    sc_item.*
                                FROM
                                    score_item AS sc_item
                                WHERE
                                    sc_item.SCORE_ID = 4
                                ORDER BY
                                    sc_item.GRUPO_ID,
                                    sc_item.ID");
        }  else {
            return mysql_query("SELECT
                                    sc_item.*, g.DESCRICAO AS dsc,
                                    g.QTD
                                FROM
                                    score_item AS sc_item
                                    INNER JOIN grupos AS g ON (sc_item.GRUPO_ID = g.ID)
                                WHERE
                                    sc_item.SCORE_ID = 4
                                ORDER BY
                                    sc_item.GRUPO_ID,
                                    sc_item.ID");
        }
    }
    private static function fillGridProrrogacoes($quadros, $elementos) {
        $html = '';
        $relatorios = array();
        foreach ($quadros as $quadro) {
            $result = self::fillScoreNEAD7();
            while ($itemRelatorio = mysql_fetch_array($result)) {
                $dataRelatorio                  = $quadro['data'];
                $relatorioId                    = $quadro['id_relatorio'];
                $itemId                         = $itemRelatorio['ID'];
                $itemRelatorio['observacoes']   = $elementos[$relatorioId]['observacao'];
                $itemRelatorio['estaMarcado']   = isset($elementos[$relatorioId])
                    && isset($elementos[$relatorioId][$itemId])
                    && true === $elementos[$relatorioId][$itemId];

                $relatorios[$itemRelatorio['dsc']][$dataRelatorio][$itemId] = $itemRelatorio;
            }
        }

        $rows = '';
        $totalNead = [];
        $observacoes = [];
        foreach ($relatorios as $desc => $neads) {
            $colunas = '<td bgcolor="#ccc">Descrição</td>';
            $tdsRows = sprintf('<td style="border-style: solid; border-top: 1px double #000;" width="60">%s</td>', $desc);
            foreach ($neads as $coluna => $nead) {
                $dataRelatorio = \DateTime::createFromFormat('Y-m-d H:i:s', $coluna);
                $colunas .= sprintf('<td bgcolor="#ccc">%s</td>', $dataRelatorio->format('d/m/Y'));
                $tdItens = '';
                $i = 0;
                foreach ($nead as $item) {
                    if($item['estaMarcado'] === true) {
                        $itemMarcado = "<img src='../utils/processada_16x16.jpg' width='12'>";
                        $scoreItem = $item['PESO']*3;
                        $totalNead[$dataRelatorio->format('d/m/Y H:i:s')] += $scoreItem;
                    }  else {
                        $itemMarcado = "<img src='../utils/cancelada_16x16.jpg' width='12'>";
                    }
                    $observacao[$i] = $item['observacoes'];
                    $tdItens .= sprintf('<tr><td>%s %s</td></tr>', $itemMarcado, $item['DESCRICAO']);
                    $i++;
                }
                $tdsRows .= sprintf('<td><table>%s</table></td>', $tdItens);
            }
            $rows .= sprintf('<tr>%s</tr>', $tdsRows);
        }
        $rows .='<tr><td bgcolor="#ccc"><b> TOTAIS </b></td>';
        $i = 0;
        foreach ($totalNead as $total){
            switch($total){
                case $total < 6:
                    $resultado = $observacao[$i];
                    break;
                case $total >= 6 && $total <= 15:
                    $resultado = 'Manter com até 06 h ou visitas';
                    break;
                case $total > 15 && $total <= 24:
                    $resultado = 'Manter com até 12 h';
                    break;
                case $total > 24:
                    $resultado = 'Manter com mais de 12 h';
                    break;
            }
            $rows .= sprintf('<td bgcolor="#ccc">%s: %s</td>', $total, $resultado);
            $i++;
        }
        $rows .= '</tr>';
        $topo = '<tr><td align="center" colspan="10"><b>Últimas Tabelas de Manutenção de Internação Domiciliar-NEAD<sup>7<sup></b></td></tr>';
        $table = sprintf('<table align="center" style="font-size: 9px;">%s<tr>%s</tr>%s</table>', $topo, $colunas, $rows);
        return $table;
    }

    public function visualizar_rel_alta($id_paciente, $idr) {
        echo "<div><center><h1>Visualizar Alta Médica</h1></center></div>";
        echo "<div>";

        $sql = "SELECT
                  pds.nome AS plano_rel,
                  e.nome AS empresa_rel,
                  if(relatorio_alta_med.empresa = 0 , clientes.empresa, relatorio_alta_med.empresa) as empresa_id
                FROM
                  relatorio_alta_med
                  LEFT JOIN empresas AS e ON relatorio_alta_med.empresa = e.id
                  LEFT JOIN planosdesaude AS pds ON relatorio_alta_med.plano = pds.id
                  inner join clientes on relatorio_alta_med.PACIENTE_ID = clientes.idClientes 
                WHERE
                  relatorio_alta_med.ID = '{$idr}'";
        $rs = mysql_query($sql);
        $rowPaciente = mysql_fetch_array($rs);
        $empresa = null;
        $convenio = null;
        if(count($rowPaciente) > 0) {
            $empresa = $rowPaciente['empresa_rel'];
            $convenio = $rowPaciente['plano_rel'];
            $empresaId = $rowPaciente['empresa_id'];
        }

        Paciente::cabecalho($id_paciente, $empresa, $convenio);

        $sql = "
                SELECT
                    r.*,
                    DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
                    DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
                    DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
                    u.nome as user          
                FROM
                    relatorio_alta_med as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios)               
                WHERE
                    r.ID = '{$idr}'
                ORDER BY
                    r.DATA DESC";

        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {

            $inicio = $row['inicio'];
            $fim = $row['fim'];
            $user = $row['user'];
            $empresa = $row['empresa'];
            $estd_geral = $row['ESTADO_GERAL'];
            $mucosa = $row['MUCOSA'];
            $mucosaobs = $row['MUCOSA_OBS'];
            $escleras = $row['ESCLERAS'];
            $esclerasobs = $row['ESCLERAS_OBS'];
            $respiratorio = $row['PADRAO_RESPIRATORIO'];
            $respiratorioobs = $row['PADRAO_RESPIRATORIO_OBS'];
            $pa_sistolica = $row['PA_SISTOLICA'];
            $pa_diastolica = $row['PA_DIASTOLICA'];
            $fc = $row['FC'];
            $fr = $row['FR'];
            $temperatura = $row['TEMPERATURA'];
            $dor_sn = $row['DOR'];
            $outro_resp = $row['OUTRO_RESP'];
            $oximetria_pulso = $row['OXIMETRIA_PULSO'];
            $tipo_oximetria = $row['OXIMETRIA_PULSO_TIPO'];
            $litros = $row['OXIMETRIA_PULSO_LITROS'];
            $via_oximetria = $row['OXIMETRIA_PULSO_VIA'];
            $mod_home_care = $row['MOD_HOME_CARE'];
            $deslocamento = $row['DESLOCAMENTO'];
            $deslocamento_distancia = $row['DESLOCAMENTO_DISTANCIA'];
        }
        echo "<p><b>".htmlentities("Relatório de Alta Médica  feito por: {$user}.")."</b></p>";
        echo "<p><b>".htmlentities("Data da Alta Médica: {$fim}.")."</b></p>";


        $this->resumoHistoria($id_paciente,$idr,1);

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th colspan='5' >EXAMES ".htmlentities("FÍSICOS DESDE A ÚLTIMA VISITA MÉDICA")."</th>";
        echo "</tr></thead>";

        $x = '';
        $y1 = '';
        if ($estd_geral == 1) {
            $x = "BOM";
        }
        if ($estd_geral == 2) {
            $x = "REGULAR";
        }
        if ($estd_geral == 3) {
            $x = "RUIM";
        }

        echo "<tr><td><b>Estado Geral:</b>  </td><td colspan='4'>{$x}</td></tr>";
        $x = '';
        $y1 = '';
        if ($mucosa == 'n') {
            $x = 'DESCORADAS';
            $y1 = $mucosaobs;
        }
        if ($mucosa == 's') {
            $x = 'Coradas';
            $y1 = $mucosaobs;
        }
        echo "<tr><td><b>Mucosas:</b>  </td><td colspan='4'>{$x}  {$y1}</td></tr>";
        $x = '';
        if ($escleras == 'n') {
            $x = 'Ict&eacute;ricas';
            $y1 = $esclerasobs;
        }
        if ($escleras == 's') {
            $x = 'Anict&eacute;ricas';
            $y1 = $esclerasobs;
        }
        echo "<tr><td><b>Escleras:</b>  </td><td colspan='4'>{$x}  {$y1}</td></tr>";
        $x = '';
        $y1 = '';
        if ($respiratorio == 'n') {
            $x = 'Taqui/Dispn&eacute;ico';
            $y1 = $respiratorioobs;
        }
        if ($respiratorio == 's') {
            $x = 'Eupn&eacute;ico';
            $y1 = $respiratorioobs;
        }
        if ($respiratorio == '1') {
            $x = 'Outro';
            $y1 = $outro_resp;
        }
        echo "<tr><td><b>Padr&atilde;o Respirat&oacute;rio:</b>  </td><td colspan='4'>{$x}  {$y1}</td></tr>";

        echo "<tr><td width=20% ><b>Oximetria de Pulso: </b></td>
            <td width=25% colspan='4'>
            <input type='texto' class='numerico1' name='oximetria_pulso' size='4' value='{$oximetria_pulso}' readonly size='4' />";
        $tipo = '';
        if ($tipo_oximetria == 1) {
            $tipo = 'Ar Ambiente';
        }
        if ($tipo_oximetria == 2) {
            $tipo = 'O2 Suplementar';
        }

        echo "<b> Tipo </b><input type='texto' value='{$tipo}' readonly/>";
        if ($tipo_oximetria == 2) {
            $via = '';
            if ($via_oximetria == 1) {
                $via = 'Cateter Nasal';
            }
            if ($via_oximetria == 2) {
                $via = 'Cateter em Traqueostomia';
            }
            if ($via_oximetria == 3) {
                $via = 'Máscara de Venturi';
            }
            if ($via_oximetria == 4) {
                $via = 'Ventilação Mecânica';
            }
            echo "<b> Dosagem</b><input type='texto' size='3' value='{$litros}' readonly/>l/min";
            echo "<b> Via</b><input type='texto' value='{$via}' readonly/>";
        }

        echo "</td></tr>";

        echo "<tr><td width=20%><b>Pressão Arterial (sist&oacute;lica):</b></td><td colspan='4'><input type='texto' class='numerico1 OBG' size='6' name='pa_sistolica' value='{$pa_sistolica}'  readonly {$acesso} />mmhg</td></tr>";
        echo "<tr><td width=20%><b>Pressão Arterial (diast&oacute;lica):</b></td><td colspan='4'><input type='texto' class='numerico1 OBG' size='6' name='pa_diastolica' value='{$pa_diastolica}' readonly {$acesso} />mmhg</td></tr>";
        echo "<tr><td width=20%><b>Frequência Cardíaca: </b></td><td colspan='4'><input type='texto' size='6' class='numerico1 OBG' name='fc' readonly value='{$fc}' {$acesso} />bpm</td></tr>";
        echo "<tr><td width=20%><b>Frequência Respiratória:</b></td><td colspan='4'><input type='texto' size='6' class='numerico1 OBG' readonly name='fr' value='{$fr}' {$acesso} />irpm</td></tr>";
        echo "<tr><td width=20%><b>Temperatura:</b></td><td colspan='4'><input type='texto' size='6' class='numerico1 OBG' name='temperatura' value='{$temperatura}' readonly {$acesso} />&deg;C</td></tr>";

        if ($dor_sn == 's') {
            $dor_sn = "Sim";
        }
        if ($dor_sn == 'n') {
            $dor_sn = "N&atilde;o";
        }
        echo"<tr><td colspan='5' ><b>DOR:</b>{$dor_sn}</td></tr>";

        echo "<tr><td colspan='5' ><table id='dor' style='width:100%;'>";
        $sql = "select * from dor_relatorio_prorrogacao_med where RELATORIO_ALTA_MED_ID = {$idr} ";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            echo "<tr><td> <b>Local:</b> {$row['DESCRICAO']}&nbsp;&nbsp; <b>Escala visual:</b> {$row['ESCALA']}&nbsp;&nbsp;<b>Padr&atilde;o:</b>{$row['PADRAO']}&nbsp;&nbsp; </td></tr>";
        }
        echo "</table>";
        echo "</table></td></tr>";


        //echo "<p><b>Materiais e medica&ccedil;&otilde;es conforme c&oacute;pias das prescri&ccedil;&otilde;es m&eacute;dica e de enfermagem enviados.</b></p>";
        //Mostra PROFISSIONAIS
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th colspan='3' >PROFISSIONAIS</th>";
        echo "</tr></thead>";
        $sql = "
                SELECT
                    p.*,
                    f.frequencia as frequencia
                FROM
                    profissionais_relatorio_prorrogacao_med as p INNER JOIN
                    frequencia as f ON (p.FREQUENCIA_ID = f.id)
                WHERE
                    p.RELATORIO_ALTA_MED_ID = {$idr} ";
        $flag_prof = TRUE;
        $result = mysql_query($sql);
        if(mysql_num_rows($result) > 0){
            while ($row = mysql_fetch_array($result)) {
                if ($flag_prof) {
                    echo "<tr bgcolor='#D3D3D3'><td width='45%'> <b>Profissional</b></td> <td><b>".htmlentities("Sugestão de Frequência")."</b></td> <td><b>Justificativa</b></td></tr>";
                    $flag_prof = FALSE;
                }
                echo "<tr><td width='45%'> {$row['DESCRICAO']}</td><td>{$row['frequencia']}</td><td>{$row['JUSTIFICATIVA']}</td></tr>";
            }
        }else{
            echo "<tr><td colspan='2'>" . htmlentities("Não") . " foi registrado serviço para a alta do paciente</td></tr>";
        }
        echo "</table>";

        $responsePaciente = ModelPaciente::getById($id_paciente);

        $caminho = "/medico/imprimir_alta.php?id={$id_paciente}&idr={$idr}";
        echo "<button caminho='$caminho' empresa-relatorio='" . (!empty($empresaId) ? $empresaId : $responsePaciente['empresa']) . "' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  role='button' aria-disabled='false'>
                <span class='ui-button-text'>
                    Imprimir
                </span>
              </button>";


//        echo"<form method=post target='_blank' action='imprimir_alta.php?id={$id_paciente}&idr={$idr}'>";
//        echo"<input type=hidden value={$paciente} name='paciente'</input>";
//        echo "<button id='imprimir_alta' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'><span class='ui-button-text'>Imprimir</span></button>";
//        echo "</form>";

        echo "</div>";
    }


    public function visualizar_rel_prorrogacao($id_paciente, $idr) {

        echo "<div><center><h1>Visualizar Prorrogação Médica</h1></center>";


        $sql = "SELECT 
                  pds.nome AS plano_rel, 
                  e.nome AS empresa_rel 
                FROM 
                  relatorio_prorrogacao_med
                  LEFT JOIN empresas AS e ON relatorio_prorrogacao_med.empresa = e.id
                  LEFT JOIN planosdesaude AS pds ON relatorio_prorrogacao_med.plano = pds.id 
                WHERE 
                  relatorio_prorrogacao_med.ID = '{$idr}'";
        $rs = mysql_query($sql);
        $rowPaciente = mysql_fetch_array($rs);
        $empresa = null;
        $convenio = null;
        if(count($rowPaciente) > 0) {
            $empresa = $rowPaciente['empresa_rel'];
            $convenio = $rowPaciente['plano_rel'];
        }

        Paciente::cabecalho($id_paciente, $empresa, $convenio);

        $sql = "
                SELECT
                    r.*,
                    DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
                    DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
                    DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
                    u.nome as user,
                    c.empresa as empresaCliente		
                FROM
                    relatorio_prorrogacao_med as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios)	
                    inner join clientes as c on (r.PACIENTE_ID = c.idClientes)			 
                WHERE
                    r.ID = '{$idr}'
                ORDER BY
                    r.DATA DESC";

        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {

            $inicio = $row['inicio'];
            $fim = $row['fim'];
            $user = $row['user'];
            $estd_geral = $row['ESTADO_GERAL'];
            $mucosa = $row['MUCOSA'];
            $mucosaobs = $row['MUCOSA_OBS'];
            $escleras = $row['ESCLERAS'];
            $esclerasobs = $row['ESCLERAS_OBS'];
            $respiratorio = $row['PADRAO_RESPIRATORIO'];
            $respiratorioobs = $row['PADRAO_RESPIRATORIO_OBS'];
            $pa_sistolica = $row['PA_SISTOLICA'];
            $pa_diastolica = $row['PA_DIASTOLICA'];
            $fc = $row['FC'];
            $fr = $row['FR'];
            $temperatura = $row['TEMPERATURA'];
            $dor_sn = $row['DOR'];
            $outro_resp = $row['OUTRO_RESP'];
            $oximetria_pulso = $row['OXIMETRIA_PULSO'];
            $tipo_oximetria = $row['OXIMETRIA_PULSO_TIPO'];
            $litros = $row['OXIMETRIA_PULSO_LITROS'];
            $via_oximetria = $row['OXIMETRIA_PULSO_VIA'];
            $mod_home_care = $row['MOD_HOME_CARE'];
            $deslocamento = $row['DESLOCAMENTO'];
            $deslocamento_distancia = $row['DESLOCAMENTO_DISTANCIA'];
            $justificativaProrrogacao = $row['justificativa'];
            $quadroClinico = $row['quadro_clinico'];
            $regimeAtual = $row['regime_atual'];
            $indicacaoTecnica = $row['indicacao_tecnica'];
            $classificacao_paciente = $row['CLASSIFICACAO_NEAD'];
            $modeloNead = $row['MODELO_NEAD'];
            $empresaRelatorio = empty($row['empresa']) ? $row['empresaCliente'] : $row['empresa'];

        }

        $sql_problemas = "
                            SELECT
                                p.*
                            FROM
                                problemas_relatorio_prorrogacao_med as p
                            WHERE
                                p.RELATORIO_PRORROGACAO_MED_ID = {$idr}
                            ORDER BY
                                ID
                            ";

        echo "<p><b>RELAT&Oacute;RIO DE PRORROGA&Ccedil;&Atilde;O M&Eacute;DICA DO PER&Iacute;ODO {$inicio} AT&Eacute; {$fim}</p>";
        echo "<p>PROFISSIONAL RESPONS&Aacute;VEL: {$user}</b></p>";

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th colspan='3' >Problemas Ativos</th>";
        echo "</tr></thead>";
        $result_problemas = mysql_query($sql_problemas);
        while ($row = mysql_fetch_array($result_problemas)) {
            $cid_id = $row['CID_ID'];
            $descricao = $row['DESCRICAO'];
            $observacao = $row['OBSERVACAO'];
            echo "<tr><td width='30%'>{$descricao}</td><td colspan='2'>{$observacao}</td></tr>";
        }

        echo "</table>";

        echo "<table class='mytable' style='width:95%'>";
        $sql_inter = "SELECT * FROM intercorrenciasfichaevolucao WHERE FICHAMEDICAPRORROGACAO_ID = '{$idr}'";
        $result_inter = mysql_query($sql_inter);
        $inter = mysql_num_rows($result_inter);
        if ($inter != 0) {
            $inter = "Sim";
        } else {
            $inter = "N&atilde;o";
        }
        echo "<thead><tr>";
        echo "<th colspan='3'>INTERCORR&Ecirc;NCIAS - {$inter}</th>";
        echo "</tr></thead>";


        while ($row = mysql_fetch_array($result_inter)) {
            echo "<tr><td colspan='3'>{$row['DESCRICAO']}</td></tr>";
        }

        if($modeloNead == '2016') {
            echo "</table>";
            echo \App\Controllers\Score::scoreNead2016Imprimir($id_paciente, $idr, "prorrogacao", $classificacao_paciente, 'view');
            echo "<table class='mytable' width='100%'>";
        } else {
            echo "</table>";
            echo self::tabelaNEAD($idr, 0, true);
        }

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th colspan='5' >Exames F&iacute;sicos</th>";
        echo "</tr></thead>";

        $x = '';
        $y1 = '';
        if ($estd_geral == 1) {
            $x = "BOM";
        }
        if ($estd_geral == 2) {
            $x = "REGULAR";
        }
        if ($estd_geral == 3) {
            $x = "RUIM";
        }

        echo "<tr><td><b>Estado Geral:</b>  </td><td colspan='4'>{$x}</td></tr>";
        $x = '';
        $y1 = '';
        if ($mucosa == 'n') {
            $x = 'DESCORADAS';
            $y1 = $mucosaobs;
        }
        if ($mucosa == 's') {
            $x = 'Coradas';
            $y1 = $mucosaobs;
        }
        echo "<tr><td><b>Mucosas:</b>  </td><td colspan='4'>{$x}  {$y1}</td></tr>";
        $x = '';
        if ($escleras == 'n') {
            $x = 'Ict&eacute;ricas';
            $y1 = $esclerasobs;
        }
        if ($escleras == 's') {
            $x = 'Anict&eacute;ricas';
            $y1 = $esclerasobs;
        }
        echo "<tr><td><b>Escleras:</b>  </td><td colspan='4'>{$x}  {$y1}</td></tr>";
        $x = '';
        $y1 = '';
        if ($respiratorio == 'n') {
            $x = 'Taqui/Dispn&eacute;ico';
            $y1 = $respiratorioobs;
        }
        if ($respiratorio == 's') {
            $x = 'Eupn&eacute;ico';
            $y1 = $respiratorioobs;
        }
        if ($respiratorio == '1') {
            $x = 'Outro';
            $y1 = $outro_resp;
        }
        echo "<tr><td><b>Padr&atilde;o Respirat&oacute;rio:</b>  </td><td colspan='4'>{$x}  {$y1}</td></tr>";

        echo "<tr><td width=20% ><b>Oximetria de Pulso: </b></td>
			<td width=25% colspan='4'>
			<input type='texto' class='numerico1' name='oximetria_pulso' size='4' value='{$oximetria_pulso}' readonly size='4' />";
        $tipo = '';
        if ($tipo_oximetria == 1) {
            $tipo = 'Ar Ambiente';
        }
        if ($tipo_oximetria == 2) {
            $tipo = 'O2 Suplementar';
        }

        echo "<b> Tipo </b><input type='texto' value='{$tipo}' readonly/>";
        if ($tipo_oximetria == 2) {
            $via = '';
            if ($via_oximetria == 1) {
                $via = 'Cateter Nasal';
            }
            if ($via_oximetria == 2) {
                $via = 'Cateter em Traqueostomia';
            }
            if ($via_oximetria == 3) {
                $via = 'Máscara de Venturi';
            }
            if ($via_oximetria == 4) {
                $via = 'Ventilação Mecânica';
            }
            echo "<b> Dosagem</b><input type='texto' size='3' value='{$litros}' readonly/>l/min";
            echo "<b> Via</b><input type='texto' value='{$via}' readonly/>";
        }

        echo "</td></tr>";

        echo "<tr><td width=20%><b>Pressão Arterial (sist&oacute;lica):</b></td><td colspan='4'><input type='texto' class='numerico1 OBG' size='6' name='pa_sistolica' value='{$pa_sistolica}'  readonly {$acesso} />mmhg</td></tr>";
        echo "<tr><td width=20%><b>Pressão Arterial (diast&oacute;lica):</b></td><td colspan='4'><input type='texto' class='numerico1 OBG' size='6' name='pa_diastolica' value='{$pa_diastolica}' readonly {$acesso} />mmhg</td></tr>";
        echo "<tr><td width=20%><b>Frequência Cardíaca: </b></td><td colspan='4'><input type='texto' size='6' class='numerico1 OBG' name='fc' readonly value='{$fc}' {$acesso} />bpm</td></tr>";
        echo "<tr><td width=20%><b>Frequência Respiratória:</b></td><td colspan='4'><input type='texto' size='6' class='numerico1 OBG' readonly name='fr' value='{$fr}' {$acesso} />irpm</td></tr>";
        echo "<tr><td width=20%><b>Temperatura:</b></td><td colspan='4'><input type='texto' size='6' class='numerico1 OBG' name='temperatura' value='{$temperatura}' readonly {$acesso} />&deg;C</td></tr>";

        if ($dor_sn == 's') {
            $dor_sn = "Sim";
        }
        if ($dor_sn == 'n') {
            $dor_sn = "N&atilde;o";
        }
        echo"<tr><td colspan='5' ><b>DOR:</b>{$dor_sn}</td></tr>";

        echo "<tr><td colspan='5' ><table id='dor' style='width:100%;'>";
        $sql = "select * from dor_relatorio_prorrogacao_med where RELATORIO_PRORROGACAO_MED_ID = {$idr} ";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            echo "<tr><td> <b>Local:</b> {$row['DESCRICAO']}&nbsp;&nbsp; <b>Escala visual:</b> {$row['ESCALA']}&nbsp;&nbsp;<b>Padr&atilde;o:</b>{$row['PADRAO']}&nbsp;&nbsp; </td></tr>";
        }
        echo "</table>";
        echo "</table></td></tr>";

        //Mostra MODALIDADE HOME CARE
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th colspan='4' >MODALIDADE DE HOME CARE</th>";
        if ($mod_home_care == 1) {
            $modalidade = "ASSISTENCIA DOMICILIAR";
        } elseif ($mod_home_care == 2) {
            $modalidade = "INTERNACAO DOMICILIAR 6h";
        } elseif ($mod_home_care == 3) {
            $modalidade = "INTERNACAO DOMICILIAR 12h";
        } elseif ($mod_home_care == 4) {
            $modalidade = "INTERNACAO DOMICILIAR 24H COM RESPIRADOR";
        } elseif ($mod_home_care == 5) {
            $modalidade = "INTERNACAO DOMICILIAR 24H sem RESPIRADOR";
        }
        echo "</tr></thead>";
        echo "<tr><td colspan='4'>{$modalidade}</td></tr>";
        echo "</table>";

        echo "<p><b>Materiais e medica&ccedil;&otilde;es conforme c&oacute;pias das prescri&ccedil;&otilde;es m&eacute;dica e de enfermagem enviados.</b></p>";
        //Mostra PROFISSIONAIS
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th colspan='3' >PROFISSIONAIS</th>";
        echo "</tr></thead>";
        $sql = "
                SELECT
                    p.*,
                    f.frequencia as frequencia
                FROM
                    profissionais_relatorio_prorrogacao_med as p INNER JOIN
                    frequencia as f ON (p.FREQUENCIA_ID = f.id)
                WHERE
                    RELATORIO_PRORROGACAO_MED_ID = {$idr} ";
        $flag_prof = TRUE;
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            if ($flag_prof) {
                echo "<tr bgcolor='gray'><td width='45%'> <b>Profissional</b></td> <td><b>Frequencia</b></td> <td><b>Justificativa</b></td></tr>";
                $flag_prof = FALSE;
            }
            echo "<tr><td width='45%'> {$row['DESCRICAO']}</td><td>{$row['frequencia']}</td><td>{$row['JUSTIFICATIVA']}</td></tr>";
        }
        echo "</table>";
        //Mostra PROCEDIMENTO	
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th colspan='3' >PROCEDIMENTOS</th>";
        echo "</tr></thead>";
        $sql = "
                SELECT
                    *
                FROM
                    procedimentos_relatorio_prorrogacao_med                    
                WHERE
                    RELATORIO_PRORROGACAO_MED_ID = {$idr} ";
        $flag_proce = TRUE;
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            if ($flag_proce) {
                echo "<tr bgcolor='gray'><td width='45%'> <b>Procedimento</b></td><td><b>Justificativa</b></td></tr>";
                $flag_proce = FALSE;
            }
            echo "<tr><td width='45%'> {$row['DESCRICAO']}</td><td>{$row['JUSTIFICATIVA']}</td></tr>";
        }
        echo "</table>";

        //Mostra deslocamento
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th colspan='3' >DESLOCAMENTO</th>";
        echo "</tr></thead>";
        if ($deslocamento == S) {
            echo "<tr><td colspan='3'>Houve deslocamento da equipe por tratar-se de uma interna&ccedil;&atilde;o localizada a {$deslocamento_distancia}km da sede da empresa</tr></td>";
        }
        echo "</table>";
        echo "<table class='mytable' style='width:95%;'>";
        if($justificativaProrrogacao != '') {
            echo "    <thead><tr><td><b>Justificativa</b></td></tr></thead>
				          <tr><td>{$justificativaProrrogacao}</td></tr>";
        } else {
            echo "    <thead><tr><th><b>Quadro Clínico</b></th></tr></thead>
				          <tr><td>{$quadroClinico}</td></tr>";
            echo "    <thead><tr><th><b>Modalidade de Atendimento Atual</b></th></tr></thead>
				          <tr><td>{$regimeAtual}</td></tr>";
            echo "    <thead><tr><th><b>Indicação Técnica para o próximo período</b></th></tr></thead>
				          <tr><td>{$indicacaoTecnica}</td></tr>";
        }

        echo "</table>";

        $caminho = "/medico/imprimir_prorrogacao.php?id={$id_paciente}&idr={$idr}";
        echo "<button id='imprimir_prorrogacao'  caminho='$caminho' empresa-relatorio='{$empresaRelatorio}' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  role='button' aria-disabled='false'><span class='ui-button-text'>Imprimir</span></button>";

        echo "</div>";
    }

    private $abas = array('0' => 'Repouso', '1' => 'Dieta', '2' => 'Cuidados Especiais', '3' => 'Soros e Eletr&oacute;litos',
        '4' => 'Antibi&oacute;ticos Injet&aacute;veis', '5' => 'Injet&aacute;veis IV', '6' => 'Injet&aacute;veis IM',
        '7' => 'Injet&aacute;veis SC', '8' => 'Drogas inalat&oacute;rias', '9' => 'Nebuliza&ccedil;&atilde;o',
        '10' => 'Drogas Orais/Enterais', '11' => 'Drogas T&oacute;picas', '12' => 'F&oacute;mulas', '13' => 'Oxigenoterapia', '14' => 'Dieta Enterais', '15' => 'Materiais');

    public function visualizar_relatorio_aditivo($id_paciente, $idr) {
        echo "<div><center><h1>Visualizar Relatório Aditivo Médico</h1></center></div>";
        echo "<div>";

        $sql = "SELECT 
                  pds.nome AS plano_rel, 
                  e.nome AS empresa_rel,
                  prescricoes.empresa as empresaId,
                  prescricoes.motivo_ajuste                 
                FROM 
                  prescricoes
                  LEFT JOIN empresas AS e ON prescricoes.empresa = e.id
                  LEFT JOIN planosdesaude AS pds ON prescricoes.plano = pds.id 
                WHERE 
                  prescricoes.id = '{$idr}'";
        $rs = mysql_query($sql);
        $rowPaciente = mysql_fetch_array($rs);
        $empresa = null;
        $convenio = null;
        $empresaId = null;
        $htmlMotivo = '';
        if(count($rowPaciente) > 0) {
            $empresa = $rowPaciente['empresa_rel'];
            $convenio = $rowPaciente['plano_rel'];
            $empresaId = $rowPaciente['empresaId'];
            $htmlMotivo = "<br><br>
<div class='ui-state-highlight' style='padding: 5px;'>
                <span class='ui-icon  ui-icon-alert icon-inline' ></span>
                <b>Motivo do Ajuste da Prescrição Aditiva: </b> 
                {$rowPaciente['motivo_ajuste']}
            </div>
            <br>
";
        }

        Paciente::cabecalho($id_paciente, $empresa, $convenio);


        $responsePaciente = ModelPaciente::getById($id_paciente);
        $empresaRelatorio = empty($empresaId) ? $responsePaciente['empresa'] : $empresaId;

        echo "<button id='imprimir_relatorio_aditivo' empresa-relatorio='{$empresaRelatorio}' caminho='/medico/imprimir_relatorio_aditivo.php?id={$idr}&idpac={$id_paciente}' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'><span class='ui-button-text'>Imprimir</span></button>";
echo $htmlMotivo;
//        echo"<form method=post target='_blank' action='imprimir_relatorio_aditivo.php?id={$idr}&idpac={$id_paciente}'>";
//        echo"<input type=hidden value={$id_paciente} name='imprimir_relatorio_aditivo'</input>";
//        echo "<button id='imprimir_relatorio_aditivo' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'><span class='ui-button-text'>Imprimir</span></button>";
//        echo "</form>";

        echo "<table width=100% class='mytable'>";
        echo "<thead><tr><th><b>Itens prescritos</b></th></tr></thead>";

        $sql = "SELECT 
                I.*,U.nome as nomeuser,U.idUsuarios,P.Carater,
                v.via as nvia, f.frequencia as nfrequencia,I.inicio,
                I.fim, DATE_FORMAT(I.aprazamento,'%H:%i') as apraz, ume.unidade as um
                
            FROM
               prescricoes AS P INNER JOIN 
               itens_prescricao AS I ON (P.id = I.idPrescricao)  LEFT JOIN
               catalogo AS B ON ( I.CATALOGO_ID = B.ID) INNER JOIN
               
               usuarios AS U ON (U.idUsuarios=P.criador) LEFT OUTER JOIN
               frequencia as f ON (I.frequencia = f.id) LEFT OUTER JOIN
               viaadm as v ON (I.via = v.id) LEFT OUTER JOIN
               umedidas as ume ON (I.umdose = ume.id)
            WHERE
                P.id = {$idr} GROUP BY I.id";

        $result = mysql_query($sql);
        $n = 0;
        $iduser = 0;
        $valCarater = 0;
        while ($row = mysql_fetch_array($result)) {


            if ($row["Carater"] == 4) {
                $carater = " -Prescri&ccedil;&atilde;o Peri&oacute;dica de Avalia&ccedil;&atilde;o";
            }
            if ($row["Carater"] == 7) {
                $carater = " -Prescri&ccedil;&atilde;o Aditiva de Avalia&ccedil;&atilde;o";
            }
            if ($n++ % 2 == 0)
                $cor = '#E9F4F8';
            else
                $cor = '#FFFFFF';

            $tipo = $row['tipo'];
            $i = implode("/", array_reverse(explode("-", $row['inicio'])));
            $f = implode("/", array_reverse(explode("-", $row['fim'])));
            $aprazamento = "";
            if ($row['tipo'] != "0")
                $aprazamento = $row['apraz'];
            $linha = "";
            if ($row['tipo'] != "-1") {
                $aba = $this->abas[$tipo];
                $dose = "";
                if ($row['dose'] != 0)
                    $dose = $row['dose'] . " " . $row['um'];
                $linha = "<b>$aba:</b> {$row['nome']} {$row['apresentacao']} {$row['nvia']} {$dose} {$row['nfrequencia']} Per&iacute;odo: de $i até $f OBS: {$row['obs']}";
            } else
                $linha = $row['descricao'];

            if ($iduser != $row['idUsuarios'] || $valCarater != $row["Carater"]) {
                $iduser = $row['idUsuarios'];
                $valCarater = $row["Carater"];
                echo"<tr bgcolor='grey'><td><center><b>M&eacute;dico: {$row['nomeuser']}{$carater}</center></b></td></tr>";
            }
            echo "<tr bgcolor={$cor} >";
            echo "<td>$linha</td>";
            echo "</tr>";
        }
        echo "</table>";

        echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
        echo "</div>";
    }

    public function servicos($id_prorrogacao) {
        $sql_servicos = "SELECT * FROM `alterar_servicos_cliente` WHERE `ID` = {$id_prorrogacao}";
        $resultado_servicos = mysql_query($sql_servicos);
        $cont_servicos = 1;

        $num_linhas_servicos = mysql_num_rows($resultado_servicos);

        $sql_profissionais = "SELECT * FROM `cuidadosespeciais` WHERE 1";
        $resultado_profissionais = mysql_query($sql_profissionais);

        echo "<table class='mytable' width='95%'>";
        echo "<thead><tr>";
        echo "<th colspan='3' >Altera&ccedil&atilde;o de Servi&ccedil;o - <select name=servicos class='alterar_servico COMBO_OBG'><option value ='-1'>  </option><option value ='1' " . (($num_linhas_servicos > 0) ? "selected" : "") . ">Sim</option><option value ='2' " . (($num_linhas_servicos <= 0) ? "selected" : "") . ">N&atilde;o</option></select></td></th></tr></thead>";

        echo "<tr span class='servicos_span'>";
        echo "<td><select name='profissionais'";
        while ($row = mysql_fetch_array($resultado_profissionais)) {
            echo "<option value='{$row['id']}'>{$row['cuidado']}</option>";
        }
        echo "</select>&nbsp;&nbsp;<button id='add-servico' tipo=''  >+ </button></td>";
        echo "<td>Outros<input type='text' name='outro-profi' class='outros-profi' {$acesso} size=50/><button id='add-outro-servico' tipo=''  >+ </button></tr>";
        $sql_mudanca = "SELECT
                * 
            FROM 
                profissionais_relatorio_prorrogacao_med 
            WHERE 
                RELATORIO_PRORROGACAO_MED_ID = '{$id_prorrogacao}'";
        $resultado_mudanca = mysql_query($sql_mudanca);
        echo "<table id='alteracoes' class='servicos_span' width='95%'>";
        echo "<tr bgcolor='grey'><td width='30%'>SERVI&Ccedil;O</td><td width='25%'>MUDAN&Ccedil;A</td></tr>";
        while ($row = mysql_fetch_array($resultado_mudanca)) {
            echo "<tr><td class='servicos_td' id_servico='{$row['CUIDADOSESPECIAIS_ID']}' desc='{$row['DESCRICAO']}'>{$row['DESCRICAO']} </td><td><select name='servicos_excluir' class='servicos' ><option value='-1'> </option><option value='1'>Manter</option><option value='3'>Exclus&atilde;o</option></td></tr>";
        }
        echo "</select>";
        echo "</table>";
    }

    public function quadro_clinico_urgencia() {
        $sql = "
                SELECT
                    *
                FROM
                    `quadro_clinico`
                WHERE
                    1
                    ORDER BY item ASC";
        $result = mysql_query($sql);
        echo "<table class='mytable'   style='table-layout:fixed; width:95%' id='tabela-quadro-clinico'>";
        echo "<thead><tr>";
        echo "<th colspan='3' >QUADRO CLINICO ATUAL/SUSPEITA DIAGNÓSTICA</th></tr></thead>";
//        echo "<tr><td colspan='3'><b>".htmlentities('O PACIENTE ESTÁ SENDO ATENDIDO EM CARÁTER
//            DE URGÊNCIA NO DOMICÍLIO POR EQUIPE EM AMBULÂNCIA E SERÁ ENCAMINHADO PARA ESTE HOSPITAL
//        DANDO ENTRADA PELA EMERGÊNCIA POR QUADRO CLÍNICO SUGESTIVO DE:')."</b></td></tr>";
//        echo "<tr>"

        while ($row = mysql_fetch_array($result)) {
            echo "<tr>"
                . "<td colspan='3'>"
                . "<input type='checkbox' name='quadro_clinico{$row['id']}'"
                . " class='quadro_clinico' id='{$row['item']}' value ='{$row['id']}'>"
                . "<label for='{$row['item']}'>{$row['item']}</label>"
                . "</td>"
                . "</tr>";
        }

        echo"<tr>"
            . "<td colspan='3'>"
            . "<b>Outro quadro clínico: </b><br>"
            . "  <textarea rows='5' cols='80' id='outro-quadro-clinico'></textarea><br>"
            . "<button id='incluir-outro-quadro-clinico' "
            . "class='ui-button ui-widget ui-state-default "
            . "ui-corner-all ui-button-text-only ui-state-hover' role='button' "
            . "aria-disabled='false' title='Incluir quadro clínico'>"
            . "<span class='ui-button-text'>"
            . "Adicionar +</span>"
            . "</button>"
            ."</td>"
            . "</tr>";

        echo "</table>";
    }

    public function quadro_clinico_encaminhamento() {
        $sql = "
                SELECT
                    *
                FROM
                    `procedimentos`
                WHERE
                    1
                ORDER bY PROCEDIMENTO ASC    ";
        $result = mysql_query($sql);
        echo "<table class='mytable' id='tabela-procedimento-encaminhamento' style='table-layout:fixed; width:95%'>";
        echo "<thead>"
            . "<tr>";
        echo "<th colspan='3' >"
            . "CONDUTA:"
            . "</th>"
            . "</tr>"
            . "</thead>";
        while ($row = mysql_fetch_array($result)) {
            echo "<tr>"
                . " <td colspan='3'>"
                . "   <input type='checkbox' name='quadro_encaminhamento{$row['ID']}' "
                . "  class='quadro_encaminhamento' id='{$row['PROCEDIMENTO']}' value ='{$row['ID']}'>"
                . "  <label for='{$row['PROCEDIMENTO']}'>{$row['PROCEDIMENTO']}</label>"
                . "</td>"
                . "</tr>";
        }
        echo"<tr>"
            . "<td colspan='3'>"
            . "<b>Outra conduta: </b><br> "
            . "  <textarea rows='5' cols='80' name='outro-procedimento' id='outro-procedimento-encaminhamento'></textarea> <br>"
            . "<button id='incluir-outro-procedimento-encaminhamento' "
            . "class='ui-button ui-widget ui-state-default "
            . "ui-corner-all ui-button-text-only ui-state-hover' role='button' "
            . "aria-disabled='false' title='Incluir procedimento' >"
            . "<span class='ui-button-text'>"
            . "Adicionar +</span>"
            . "</button>"
            ."</td>"
            . "</tr>";
        echo "</table>";
    }

    public function exames() {
        $sql = "SELECT
                    *
                FROM
                    exame_medico
                WHERE
                    1
                ORDER bY nome ASC ";
        $result = mysql_query($sql);
        echo "<table class='mytable' id='tabela-outro-exame-encaminhamento' style='table-layout:fixed; width:95%'>"
            ."<thead>"
            . "<tr>"
            . "<th colspan='3'>"
            . "Necessidade de realizar exames:"
            . "</th>"
            . "</tr>"
            . "</thead>";
        while ($row = mysql_fetch_array($result)) {
            echo "<tr>"
                . " <td colspan='3'>"
                . "<input type='checkbox' name='exame_encaminhamento{$row['id']}'"
                . " class='exame_encaminhamento' id='{$row['nome']}' value ='{$row['id']}'>"
                . "<label for='{$row['nome']}' >{$row['nome']}</label>"
                . "</td>"
                . "</tr>";
        }
        echo "<tr>"
            . "<td colspan='3'>"
            . "  <b>Outro exame: </b><br>"
            . "  <textarea rows='2' cols='80' name='outro-exame' id='outro-exame-encaminhamento'></textarea><br>"
            . "<button id='incluir-outro-exame-encaminhamento' "
            . "class='ui-button ui-widget ui-state-default "
            . "ui-corner-all ui-button-text-only ui-state-hover' role='button' "
            . "aria-disabled='false' type='button' title='Incluir exame'>"
            . "<span class='ui-button-text'>"
            . "Adicionar +</span>"
            . "</button>"
            . "</td>"
            . "</tr>";
        echo "</table>";
    }

    public function relatorio_intercorrencia($id_paciente, $id_relatorio, $editar) {

        if (isset($id_relatorio)) {
            $sql = "
                SELECT
                    r.*,
                    DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
                    u.nome as user,
                    e.nome AS empresa_rel,
                    pds.nome AS plano_rel                    		
                FROM
                    intercorrencia_med as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios)
                    LEFT JOIN empresas AS e ON r.empresa = empresas.id
                    LEFT JOIN planosdesaude AS pds ON r.plano = pds.id
                WHERE
                    r.ID = '{$id_relatorio}'
                ORDER BY
                    r.DATA DESC";

            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {

                $user = $row['USUARIO_ID'];
                $empresa = $row['empresa_rel'];
                $convenio = $row['plano_rel'];
                $ambulancia = $row['AMBULANCIA'];
                $exame_sn = $row['EXAME_SN'];
                $tomo_cranio = $row['TOMO_CRANIO'];
                $tomo_abdome = $row['TOMO_ABDOME'];
                $tomo_torax = $row['TOMO_TORAX'];
                $tomo_outros = $row['TOMO_OUTROS'];
                $outra_tomografia = $row['OUTRA_TOMOGRAFIA'];
                $rx_abdome = $row['RX_ABDOME'];
                $rx_torax = $row['RX_TORAX'];
                $rx_outros = $row['RX_OUTROS'];
                $outros_rx = $row['OUTROS_RX'];
                $laboratorio = $row['LABORATORIO'];
                $outros_exames = $row['OUTROS_EXAMES'];
                $vaga_uti = $row['VAGA_UTI'];
            }
        }
        echo "<div id='relatorio-encaminhamento'>
                    <center><h1>Nova Intercorrência Médica</h1></center>";
        echo "<form>";
        echo "<input type='hidden' value='{$id_relatorio}' name='id_relatorio' />";
        echo "<input type='hidden' name='id_paciente' id='id_paciente' value={$id_paciente} />";

        Paciente::cabecalho($id_paciente, $empresa, $convenio);
        $this->dialogPrescricaoAditiva($id_paciente, 'dialog-presc-aditiva-intercorrencia');

        echo "<p><b>Intercorência em:</b>
            <input class='data OBG' type='text' name='inicio' maxlength='10' size='10' />
            <input class='hora OBG' type='text' name='hora' value='00:00' maxlength='5' size='6' />
            </p>";
//        echo "&nbsp;<b>at&eacute;</b><input class='data OBG' type='text' name='fim' maxlength='10' size='10' /></p>";
        //////////////problemas ativos
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th colspan='3' >Problemas Ativos</th>";
        echo "</tr></thead>";
        echo "<tr><td colspan='3' >";
        echo "<table id='problemas_ativos' style='width:95%;'>";

        $this->problemas_ativos($id_paciente, $id_relatorio, $editar);

        echo "</table>";
        echo "</td></tr>";
        echo "</table>";
        $this->quadro_clinico_urgencia();

        $this->exames();

        $this->quadro_clinico_encaminhamento();


        echo "<table class='mytable' width='95%'>";
        echo "<thead>
                  <tr>
                  <th colspan='3'>Condução: </th>
                  </tr>
                  </thead>";
        echo "<tr>
<td>
<input type='radio' name='ambulancia' class='OBG_RADIO' value='3'>VEÍCULO DE INTERVENÇÃO RÁPIDA
<input type='radio' name='ambulancia' class='OBG_RADIO' value='1' /> AMBULÂNCIA BÁSICA
<input type='radio' name='ambulancia' class='OBG_RADIO' value='2'/>
AMBULÂNCIA AVANÇADA - UTI MÓVEL. "
            ."</td></tr>";
        echo "</table>";

        echo "<table class='mytable' width='95%'>";
        echo "<thead><tr><th colspan='3'>NECESSITA DE INTERNAMENTO HOSPITALAR: <select id='terapia_intensiva_select' name='terapia_intensiva' class='COMBO_OBG'><option value='-1'></option><option value='S' " . (($vaga_uti == 'S') ? "selected" : "") . ">Sim</option><option value='N'" . (($vaga_uti == 'N') ? "selected" : "") . ">N&atilde;o</option></select></th></tr></thead>";
        echo "<tr id='tr_hospital_intercorrencia'><td><label for ='hospital_intercorrencia'><b>HOSPITAL/ TIPO DE LEITO: </b></label><input id='hospital_intercorrencia' name='hospital_intercorrencia' type='text' size='50px' /></td></tr>";
        echo "</table>";

        echo alerta();

        echo "<br><button id='salvar_intercorrencia_medica' prescricao='sim'
                        class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'
                        role='button' aria-disabled='false' >
                        <span class='ui-button-text'>Gerar Prescrição Aditiva</span>
                        </button>
                        <button id='salvar_intercorrencia_medica_sem_prescricao' prescricao='nao'
                        class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'
                        role='button' aria-disabled='false' >
                        <span class='ui-button-text'>Finalizar sem prescrição</span>
                        </button>
                </span>";

        echo "</form>";
        echo" </div>";
    }

    public function dialogPrescricaoAditiva($idPaciente, $idDiaolog) {

        $sql5 = "select nome FROM clientes where idClientes ={$idPaciente}";
        $x = mysql_query($sql5);
        $y = mysql_fetch_array($x);

        echo "<div id='{$idDiaolog}' title='Prescri&ccedil;&atilde;o Aditiva'>";
        echo"<input type='hidden' id='paciente' value='{$idPaciente}' />";


        echo"<input type='hidden' id='intercorrencia' value=''  />";
        echo "<div class='ui-state-highlight' style='margin-top: 5px;'>
                    <center><b>Incluir todo MAT/MED utilizado durante o atendimento.</b></center>
            </div>";

        echo "<fieldset><legend><b>Paciente</b></legend><span id='NomePacientePresc' >" . $y['nome'] . "</span></fieldset><br/>";

        echo "<br/><fieldset style='width:340px;text-align:center;'><legend><b>Per&iacute;odo</b></legend>";
        echo "DE" .
            "<input type='text' name='inicio' value='' maxlength='10' size='10' i='' class='OBG inicio-periodo' />" .
            " AT&Eacute; <input type='text' name='fim' value='' maxlength='10' size='10' f='' class='OBG fim-periodo' /></fieldset>";

        echo "<button id='iniciar-presc-aditiva-intercorrencia' class='ui-button ui-widget ui-state-default ui-corner-all
            ui-button-text-only' role='button' aria-disabled='false' >
            <span class='ui-button-text'>iniciar</span>
            </button>";

        echo "</div>";
    }

    public function visualizar_rel_intercorrencia($id_paciente, $idr) {
        echo "<div>";
        echo "<center><h1>Visualizar Intercorrência Médica</h1></center>";

        $sql = "SELECT 
                  pds.nome AS plano_rel, 
                  e.nome AS empresa_rel, 
                  e.id AS empresa_id 
                FROM 
                  intercorrencia_med
                  LEFT JOIN empresas AS e ON intercorrencia_med.empresa = e.id
                  LEFT JOIN planosdesaude AS pds ON intercorrencia_med.plano = pds.id 
                WHERE 
                  intercorrencia_med.id = '{$idr}'";
        $rs = mysql_query($sql);
        $rowPaciente = mysql_fetch_array($rs);
        $empresa = null;
        $convenio = null;
        if(count($rowPaciente) > 0) {
            $empresa = $rowPaciente['empresa_rel'];
            $empresa_id = $rowPaciente['empresa_id'];
            $convenio = $rowPaciente['plano_rel'];
        }

        Paciente::cabecalho($id_paciente, $empresa, $convenio);

        $sql = "SELECT 
                enc_ur_emer.*,
                usuarios.nome,
                DATE_FORMAT(enc_ur_emer.data_encaminhamento,'%d/%m/%Y') as encaminhado,
                DATE_FORMAT(enc_ur_emer.`data`,'%d/%m/%Y %H:%i:%s') as feito_em
                FROM
                intercorrencia_med as enc_ur_emer
                
                INNER JOIN usuarios ON enc_ur_emer.usuario_id = usuarios.idUsuarios
                WHERE
                    enc_ur_emer.id = '{$idr}'
                ";

        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {

            $user = $row['nome'];
            $hora = $row['hora'];
            $data = $row['encaminhado'];
            $tipo_ambulancia = $row['tipo_ambulancia'];
            $vaga_uti = $row['vaga_uti'];
            $hospital = $row['hospital'];
        }
        if($tipo_ambulancia == 1){
            $ambulancia ="AMBULÂNCIA BÁSICA";
        }else if($tipo_ambulancia == 2){
            $ambulancia = "AMBULÂNCIA AVANÇADA - UTI MÓVEL. ";

        }else if($tipo_ambulancia == 3){
            $ambulancia = "VEÍCULO DE INTERVENÇÃO RÁPIDA";
        }

        $sql_problemas = "SELECT
                        intercorrencia_med_problema.descricao,
                        intercorrencia_med_problema.observacao
                        FROM
                        intercorrencia_med_problema
                        WHERE
                        intercorrencia_med_problema.intercorrencia_med_id={$idr}
                        ";

        echo "<center><b>Intercorrência do dia {$data} às {$hora}</center>";
        echo "<p>PROFISSIONAL RESPONS&Aacute;VEL: {$user}</b></p>";

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead>"
            . "<tr>";
        echo "<th colspan='3' >Problemas Ativos</th>";
        echo "</tr>"
            . "</thead>";
        $result_problemas = mysql_query($sql_problemas);
        while ($row = mysql_fetch_array($result_problemas)) {

            $descricao = $row['descricao'];
            $observacao = $row['observacao'];
            echo "<tr>"
                . "<td width='30%'>"
                . "  {$descricao}"
                . "</td>"
                . "<td colspan='2'>"
                . "{$observacao}"
                . "</td>"
                . "</tr>";
        }

        echo "</table>";

        echo "<table class='mytable' style='table-layout:fixed; width:95%'>";
        $sql_urgencia = "SELECT
                        intercorrencia_med_quadro_clinico.descricao
                        FROM
                        intercorrencia_med_quadro_clinico
                        WHERE
                        intercorrencia_med_quadro_clinico.intercorrencia_med_id = {$idr}";
        $result_urgencia = mysql_query($sql_urgencia);

        echo "<thead>"
            . "<tr>";
        echo "<th colspan='3'>"
            . "QUADRO CLINICO ATUAL/SUSPEITA DIAGNÓSTICA"
            . "</th>"
            . "</tr>"
            . "</thead>";
        echo "<tr>"
            . "<td colspan='3'>"
            . "<b>";
//                  echo  htmlentities('O PACIENTE ESTÁ SENDO ATENDIDO EM CARÁTER
//            DE URGÊNCIA NO DOMICÍLIO POR EQUIPE EM AMBULÂNCIA '.$ambulancia.'  E SERÁ ENCAMINHADO PARA ESTE HOSPITAL
//        DANDO ENTRADA PELA EMERGÊNCIA POR QUADRO CLÍNICO SUGESTIVO DE:')."</b></td></tr>";

        while ($row = mysql_fetch_array($result_urgencia)) {
            echo "<tr><td colspan='3' class='wrapword' >{$row['descricao']}</td></tr>";
        }
        echo "</table>";

        //Mostra EXAMES
        $sqlExame ="SELECT
                intercorrencia_med_exame.descricao
                FROM
                intercorrencia_med_exame
                WHERE
                intercorrencia_med_exame.intercorrencia_med_id={$idr}";
        $resultExame=  mysql_query( $sqlExame);
        echo "<table class='mytable' style='table-layout:fixed; width:95%'>";
        echo "<thead><tr>";
        echo "<th colspan='3'>NECESSIDADE DE EXAMES:</th>";
        echo "</tr></thead>";
        while ($row = mysql_fetch_array($resultExame)) {
            echo "<tr><td colspan='3' class='wrapword'>{$row['descricao']}</td></tr>";
        }
        echo "</table>";

        //Mostra ENCAMINHAMENTO
        echo "<table class='mytable' style='table-layout:fixed; width:95%'>";
        $sql_encaminhamento ="SELECT
            intercorrencia_med_procedimento.descricao
            FROM
            intercorrencia_med_procedimento
            WHERE
            intercorrencia_med_procedimento.intercorrencia_med_id={$idr}";
        $result_encaminhamento = mysql_query($sql_encaminhamento);

        echo "<thead><tr>";
        echo "<th colspan='3'>CONDUTA:</th></tr></thead>";

        while ($row = mysql_fetch_array($result_encaminhamento)) {
            echo "<tr><td colspan='3' class='wrapword'>{$row['descricao']}</td></tr>";
        }
        echo "</table>";

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr> <th>CONDUÇÃO:</th></thead>
                    <tr><td>{$ambulancia}</td></tr></table>";

        //Mostra VAGA EM UTI
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        if ($vaga_uti == 'S') {
            $uti = "Sim";
        } else {
            $uti = "N&atilde;o";
        }
        echo "<th colspan='3' ><b>NECESSITA DE INTERNAMENTO HOSPITALAR:</b> {$uti}</th></tr></thead>";
        if(!empty($hospital)){
            echo "<tr><td><b>HOSPITAL/ TIPO DE LEITO: </b> {$hospital}</td></tr>";
        }

        echo "</table>";

        $responsePaciente = ModelPaciente::getById($id_paciente);
        $empresaRelatorio = empty($empresa_id) ? $responsePaciente['empresa'] : $empresa_id;

        $caminho = "/medico/ImprimirIntercorrencia.php?id={$id_paciente}&idr={$idr}";
        echo "<button caminho='{$caminho}' empresa-relatorio='{$empresaRelatorio}' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  role='button' aria-disabled='false'>
                <span class='ui-button-text'>
                    Imprimir
                </span>
              </button>";

        echo "</div>";
    }

    public function relatorioIndicadorQualidade($post)
    {
        extract($post, EXTR_OVERWRITE);
        $comp_ur = $ur !== 't' ? " AND cli.empresa = ".anti_injection($ur, 'numerico') : "";
        $comp_medico = $medico !== 't' ? " AND cpm.usuario = ".anti_injection($medico, 'numerico') : "";
        $comp_tipo = $tipo_indicador === 'erros-ficha-avaliacao' ? " AND ss.local_sugestao = 'fcav'" : " AND ss.local_sugestao = 'prsc'";
        $inicio = \DateTime::createFromFormat("d/m/Y", $inicio);
        $fim = \DateTime::createFromFormat("d/m/Y", $fim);

        $sql = "SELECT 
                    cli.nome AS paciente,
                    usu.nome AS medico,
                    DATE_FORMAT(cpm.data,'%d/%m/%Y à\s %H:%i:%s') AS data_avaliacao,
                    DATE_FORMAT(ss.data_sugestao,'%d/%m/%Y à\s %H:%i:%s') AS criado_em,
                    IF (data_resposta = '0000-00-00 00:00:00', 'Sem resposta' , DATE_FORMAT(TIMEDIFF(data_resposta,data_sugestao),'%H:%i')) AS tempo_resposta,
                    sub_count_sugestoes.qntd_sugestoes,
                    COUNT(sa.id) AS qntd_alteracoes
                FROM
                    sugestoes_secmed AS ss
                    LEFT JOIN sugestoes_alteracao AS sa ON (ss.id = sa.id_sugestao)
                    INNER JOIN capmedica AS cpm ON (cpm.id = ss.id_capmedica)
                    INNER JOIN usuarios AS usu ON (cpm.usuario = usu.idUsuarios)
                    INNER JOIN clientes AS cli ON (ss.id_paciente = cli.idClientes)
                    INNER JOIN (
                                SELECT 	
                                        id_sugestao, count(*) as qntd_sugestoes
                                FROM			 
                                        sugestoes_secmed_lista AS lista1				
                                GROUP BY
                                        lista1.id_sugestao		
                                ) as sub_count_sugestoes ON (ss.id = sub_count_sugestoes.id_sugestao)
                WHERE
                    data_sugestao BETWEEN '{$inicio->format('Y-m-d')} 00:00:00' AND '{$fim->format('Y-m-d')} 00:00:00' 
                    {$comp_ur} 
                    {$comp_medico}
                    {$comp_tipo}
                GROUP BY
                    ss.id";
        $rs = mysql_query($sql);
        $num_rows = mysql_num_rows($rs);
        if($num_rows > 0){
            $html .= "  <br></br>
                        <table class='mytable font12'>
                            <thead>
                                <tr>
                                    <th>PACIENTE</th>                            
                                    <th>M&Eacute;DICO</th>
                                    <th>DATA DA AVALIA&Ccedil;&Atilde;O</th>
                                    <th>DATA DA SUGESTÃO DOS ERROS</th>
                                    <th>TEMPO DE RESPOSTA DO M&Eacute;DICO</th>
                                    <th>QUANTIDADE DE ERROS</th>
                                    <th>QUANTIDADE DE ALTERAÇÕES</th>
                                </tr>
                                </thead>";
            $cont=1;
            while($row = mysql_fetch_array($rs)){
                $cor= $cont%2 ==0? "bgcolor='#DCDCDC'":'';
                $html .= "<tr $cor >
                            <td>{$row['paciente']}</td>
                            <td>{$row['medico']}</td>
                            <td>{$row['data_avaliacao']}</td>
                            <td>{$row['criado_em']}</td>
                            <td>{$row['tempo_resposta']}</td>
                            <td>{$row['qntd_sugestoes']}</td>
                            <td>{$row['qntd_alteracoes']}</td>
                        </tr>";
                $cont++;
            }
            $html .= "</table>";
        }else{
            $html .= "<br/><br/><p style='text-align:center;'><b>Nenhum resultado foi encontrado</b></p>";
        }
        echo $html;
    }

}

if (isset($_POST['query'])) {
    switch ($_POST['query']) {
        case "frequencia":
            $p = new Relatorios();
            $p->frequencia($_POST['disabled']);
            break;
        case "justificativa":
            $p = new Relatorios();
            $p->justificativa($_POST['disabled']);
        case "indicador-qualidade":
            $p = new Relatorios();
            $p->relatorioIndicadorQualidade($_POST);
    }
}