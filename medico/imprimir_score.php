<?php
$id = session_id();
if(empty($id))
	session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
require __DIR__ . '/../vendor/autoload.php';

use \App\Controllers\Administracao as Administracao;
use \App\Models\Administracao\Filial;

include_once('paciente_med.php');

class imprimir_score{
	public function detalhe_score($id, $tipo, $paciente_id, $empresa){
		//$id = $id;
		/*$sql = "SELECT sc_pac.OBSERVACAO,sc_pac.ID_PACIENTE, sc_item.DESCRICAO ,sc_item.PESO ,sc_item.GRUPO_ID ,
		sc_item.ID ,sc.id as tbplano,cap.justificativa,DATE(sc_pac.DATA) as DATA,
		grup.DESCRICAO as grdesc  FROM score_paciente sc_pac
		LEFT OUTER JOIN score_item sc_item ON sc_pac.ID_ITEM_SCORE = sc_item.ID
		LEFT OUTER JOIN capmedica cap ON sc_pac.ID_CAPMEDICA = cap.id
		LEFT OUTER JOIN grupos grup ON sc_item.GRUPO_ID = grup.ID
		LEFT OUTER JOIN score sc ON sc_item.SCORE_ID = sc.ID
		WHERE sc_pac.ID_CAPMEDICA = '{$id}'";
		$result = mysql_query($sql);
		$result1 = mysql_query($sql);
		$result2 = mysql_query($sql);
		$row3 = mysql_fetch_array($result2);*/

		if ($tipo == 0) {//Avulso
			$sql = "SELECT sa.justificativa, sa.OBSERVACAO, sa.ID_PACIENTE, si.DESCRICAO , si.PESO , si.GRUPO_ID ,
		si.ID , sc.id as tbplano, DATE(sa.DATA) as DATA,
		grup.DESCRICAO as grdesc
		FROM score_avulso sa
		LEFT OUTER JOIN score_item si ON sa.ID_ITEM_SCORE = si.ID
		LEFT OUTER JOIN grupos grup ON si.GRUPO_ID = grup.ID
		LEFT OUTER JOIN score sc ON si.SCORE_ID = sc.ID
		WHERE sa.ID_AVULSO = '{$id}' and sa.ID_PACIENTE = '{$paciente_id}'";

			$result = mysql_query($sql);
			$result1 = mysql_query($sql);
			$result2 = mysql_query($sql);
			$row3 = mysql_fetch_array($result2);

			$sql_usuario = "Select u.nome, sa.ID_USUARIOS as usuario from  score_avulso as sa LEFT OUTER JOIN usuarios as u ON u.idUsuarios = sa.ID_USUARIOS where sa.id = {$id}";

		}elseif ($tipo == 1){//Capmedica
			$sql = "SELECT sc_pac.OBSERVACAO,sc_pac.ID_PACIENTE, sc_item.DESCRICAO ,sc_item.PESO ,sc_item.GRUPO_ID ,
		sc_item.ID ,sc.id as tbplano,cap.justificativa,DATE(sc_pac.DATA) as DATA,
		grup.DESCRICAO as grdesc  FROM score_paciente sc_pac
		LEFT OUTER JOIN score_item sc_item ON sc_pac.ID_ITEM_SCORE = sc_item.ID
		LEFT OUTER JOIN capmedica cap ON sc_pac.ID_CAPMEDICA = cap.id
		LEFT OUTER JOIN grupos grup ON sc_item.GRUPO_ID = grup.ID
		LEFT OUTER JOIN score sc ON sc_item.SCORE_ID = sc.ID
		WHERE sc_pac.ID_CAPMEDICA = '{$id}' and grup.GRUPO_NEAD_ID IS NULL";
			$result = mysql_query($sql);
			$result1 = mysql_query($sql);
			$result2 = mysql_query($sql);
			$row3 = mysql_fetch_array($result2);

			$sql_usuario = "Select u.nome, cap.usuario, u.tipo as utipo, u.conselho_regional from  capmedica as cap LEFT OUTER JOIN usuarios as u ON u.idUsuarios = cap.usuario where cap.id = {$id} ";

		}

		$resultnome = mysql_query($sql_usuario);
		$tipo= mysql_result($resultnome, 0,2);
		$conselho= mysql_result($resultnome, 0,3);

        $compTipo = $conselho;


        $nomemedico= mysql_result($resultnome, 0,0) . $compTipo;

		$sql2="SELECT
	UPPER(c.nome) AS paciente,
	(CASE c.sexo
	WHEN '0' THEN 'Masculino'
	WHEN '1' THEN 'Feminino'
	END) AS sexo,
	FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
	e.nome as empresa,
	c.`nascimento`,
	p.nome as Convenio,p.id,
	NUM_MATRICULA_CONVENIO,
	cpf
	FROM
	clientes AS c LEFT JOIN
	empresas AS e ON (e.id = c.empresa) INNER JOIN
	planosdesaude as p ON (p.id = c.convenio)
	WHERE
	c.idClientes ={$row3['ID_PACIENTE']}
	ORDER BY
	c.nome DESC LIMIT 1";


		$result3 = mysql_query($sql2);

		$html .="<form>";
		$html .="<table style='width:100%;'>";
		$html .= "
	<a href=imprimir_score.php?id={$id}>imprimir</a></br>";
		$html .="<h1><center>Detalhes Score</center></h1>";
		if ($row3['tbplano'] == 1){
            $responseEmpresa = Filial::getEmpresaById($empresa);
			$logo = Administracao::buscarLogoPeloIdPaciente($row3['ID_PACIENTE']);
			$html .= "<tr><td><img src='../utils/logos/{$responseEmpresa['logo']}' width='100' title='' border='0' ></td></tr>";
			$html .="<tr  bgcolor='grey'><td colspan='3'><center><b>Tabela de Avalia&ccedil;&atilde;o Petrobras.</b>&nbsp;&nbsp;<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> Efetuada em: </b>".join("/",array_reverse(explode("-",$row3['DATA'])))."</center></td></tr>";
		}else{
			$html .= "<tr><td><img src='../utils/abmid.jpg' title=''  width='100' border='0' ></td></tr>";
			$html .="<tr bgcolor='grey'><td colspan='3'  ><center><b>Tabela de Avalia&ccedil;&atilde;o ABEMID.</b> &nbsp;&nbsp;<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> Efetuada em: </b>".join("/",array_reverse(explode("-",$row3['DATA'])))."</center></td></tr>";
		}

		while($pessoa = mysql_fetch_array($result3)){
			foreach($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);

			}

			$html .= "<tr >";
			$html .= "<td colspan='3'><label><b>M&eacute;dico Respns&aacute;vel:  </b>".$nomemedico."</label></td>";
			$html .= "</tr></br>";
			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td colspan='3'><label><b>PACIENTE: </b>".$pessoa['paciente']."</label></td>";
			$html .= "</tr></br>";


			$html .= "<tr><td colspan='3'><b>SEXO: </b>".$pessoa['sexo']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IDADE: </b>".join("/",array_reverse(explode("-",$pessoa['nascimento']))).' ('.$pessoa['idade']." anos)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
			$html .= "</tr></br>";
			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td colspan='3'><label><b>CONV&Ecirc;NIO: </b>".strtoupper($pessoa['Convenio'])."</label>&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATR&Iacute;CULA: </b>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</label></td>";
			$html .=  "</tr><tr><td><b>CPF:</b>{$pessoa['cpf']}</td>
                                  </tr>";
		}


		while($row = mysql_fetch_array($result)){
			if($row['OBSERVACAO'] != null && $row['OBSERVACAO'] != '')
				$html .="<tr><td width=50% ><b>".$row['DESCRICAO']."</b></td><td colspan='2'>".$row['OBSERVACAO']."</td></tr>";
		}
		$html .="<tr bgcolor='grey'><td><b>Descri&ccedil;&atilde;o</b></td><td><b>Itens da Avalia&ccedil;&atilde;o</b></td><td><b>Peso</b></td></tr>";
		while($row = mysql_fetch_array($result1)){
			if($row['OBSERVACAO'] == null && $row['OBSERVACAO'] == '')

				$html .="<tr><td width=40% >".$row['grdesc']."</td><td width=45% >".$row['DESCRICAO']."</td><td colspan='2' >".$row['PESO']."</td></tr>";
			$x += $row['PESO'];
		}
		$html .="<tr bgcolor='grey'><td colspan='3' aling='center'><b>TOTAL DO SCORE : {$x}</b></td></tr>";
		$html .="<tr bgcolor='grey'><td colspan='3' aling='center'><b>Justificativa para a interna&ccedil;&atilde;o:</b></td></tr>";
		$html .="<tr ><td colspan='3' aling='center'>".$row3['justificativa']."</td></tr>";
		$html .="</table>";
		$html .="</form>";


		$mpdf=new mPDF('pt','A4',9);
		$mpdf->WriteHTML($html);
		$mpdf->Output('pacientes.pdf','I');

	}

	public function pdfNead($id,$tipo,$paciente_id,$empresa){

		$html .="<form>";
        $responseEmpresa = Filial::getEmpresaById($empresa);

		//$logo = Administracao::buscarLogoPeloIdPaciente($paciente_id);

		$html .= "<tr><td><img src='../utils/logos/{$responseEmpresa['logo']}' width='100' title='' border='0' ></td></tr>";
		$nead = new Paciente();
		$html .= $nead->visualizarScoreNead($id, $tipo, $paciente_id,null, 'print');
		$html .="</form>";

        ob_clean(); //Limpa o buffer de saída

		$mpdf=new mPDF('pt','A4',9);
		$mpdf->WriteHTML($html);
		$mpdf->Output('pacientes.pdf','I');

	}
	public function redirecionarTipoScore($id,$tipo,$paciente_id,$tipo_score,$empresa){
		if($tipo==1){

			if ($tipo_score == 0) {
				$this->detalhe_score($id, $tipo, $paciente_id,$empresa);
			} else if ($tipo_score == 1){
				$this->pdfNead($id,$tipo,$paciente_id,$empresa);
			}else{
				die();
			}



		}else{
			if ($tipo_score == 0) {
				$this->detalhe_score($id, $tipo, $paciente_id,$empresa);
			} else if ($tipo_score == 1){
				$this->pdfNead($id,$tipo,$paciente_id,$empresa);
			}else{
				die();
			}
		}

	}

}

$p = new imprimir_score();
//$p->detalhe_score($_GET['id'], $_GET['tipo'], $_GET['paciente_id']);
$p->redirecionarTipoScore($_GET['id'], $_GET['tipo'], $_GET['paciente_id'], $_GET['tipo_score'], $_GET['empresa']);

?>