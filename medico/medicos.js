$(document).ready(function () {

  //Pede confirmaÃ§Ã£o para deixar a pÃ¡gina atual
  function ExitPageConfirmer(message)
  {
    this.message = message;
    this.needToConfirm = false;
    var myself = this;
    window.onbeforeunload = function ()
    {
      if(myself.needToConfirm)
      {
        return myself.message;
      }
    }
  }

  //CARREGA A BUSCA
  $(document).ready(function () {
    var p = null;
    var i = null;
    var f = null;

    $.post("query.php", {
      query: "buscar-prescricoes",
      paciente: p,
      inicio: i,
      fim: f
    }, function (r) {
      $("#div-resultado-busca").html(r);
    });
    return false;
  });

  var exitPage = new ExitPageConfirmer('Deseja realmente sair dessa pÃ¡gina? os dados Não salvos serão perdidos!');

  $(".div-aviso").hide();

  $("#novoBtn").hide();

  $('.data').datepicker('option', 'minDate', $("#tabela-itens").attr("inicio"));
  $('.data').datepicker('option', 'maxDate', $("#tabela-itens").attr("fim"));



  $(".numerico").priceFormat({
    prefix: '',
    centsSeparator: ',',
    thousandsSeparator: '.'
  });

  $(".obs_probativo").hide();

  $(".probativo").click(function () {

    if($(this).is(":checked")) {
      var val = $(this).val();
      var nome = $(this).attr('name');

      if(val == 1 || val == 3) {
        $("span[name='" + nome + "']").hide();
        $("span[name='" + nome + "']").children('textarea').removeClass('OBG');
        $("span[name='" + nome + "']").children('textarea').removeAttr("style");

      } else {
        $("span[name='" + nome + "']").show();
        $("span[name='" + nome + "']").children('textarea').addClass('OBG');
      }
    }

  });

  $(".obs_exame").hide();

  $(".obs_exame").each(function (i) { //Mostra os campos que tem observaÃ§Ã£o
    var obs = $(this).children('textarea').val();

    if(obs != '') {
      $(this).show();
    }

  });

  $(".respiratorio1").hide();
  /*$(".respiratorio").click(function(){

   if($(this).is(":checked")){
   var val= $(this).val();


   if(val == 's'){
   $(".respiratorio1").hide();
   }else{
   $(".respiratorio1").show();
   $(".respiratorio1").focus();
   }
   }

   });*/

  var val = $("input[name=respiratorio]:checked").val();
  if(val != '1') {
    $(".outro_respiratorio").hide();
  } else {
    $(".outro_respiratorio").show();
  }

  $(".respiratorio").click(function () {

    if($(this).is(":checked")) {
      var val = $(this).val();



      if(val == 's') {
        $("#frequencia_respiratoria").hide();
        $(".outro_respiratorio").hide();
        $("input[name='fr']").removeClass('OBG');
          $("input[name='fr']").removeAttr("style");
        $(".outro_respiratorio").children('textarea').removeClass('OBG');
        $(".outro_respiratorio").children('textarea').removeAttr("style");
      }


      if(val == 'n') {
        $("#frequencia_respiratoria").fadeIn(50).fadeOut(50).fadeIn(50);
        $("input[name='fr']").addClass('OBG');
        $(".outro_respiratorio").hide();
        $(".outro_respiratorio").children('textarea').removeClass('OBG');
        $(".outro_respiratorio").children('textarea').removeAttr("style");
      }
      if(val == '1') {
        $("#frequencia_respiratoria").hide();
        $(".outro_respiratorio").show();
        $(".outro_respiratorio").children('textarea').addClass("OBG");
          $("input[name='fr']").removeClass('OBG');
          $("input[name='fr']").removeAttr("style");
        // $("textarea[name=outro_respiratorio]").addClass('OBG');

      }

    }

  });

  $(".servicos_span").hide();
  if($("select[name='servicos'] option:selected").val() == 1) {
    $(".servicos_span").show();
  }
  $("select[name='servicos']").change(function () {
    var selecao = $("select[name='servicos'] option:selected").val();
    if(selecao == '1') {
      $(".servicos_span").show();
      $("select[name='servicos']").focus();
    } else {
      $(".servicos_span").hide();
      $("select[name='servicos']").val();
    }
  });

  $("#add-servico").click(function () {
    var id = $("select[name='profissionais'] option:selected").val();
    var nome = $("select[name='profissionais'] option:selected").html();
    //var cont = $(".profissional").size();
    //var cont = cont + 1;
    var x = $("input[name='add-outro-servico']").val();

    var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prof' title='Excluir' border='0' />&nbsp;&nbsp;&nbsp;";
    $('#alteracoes').append("<tr><td  class='servicos_td' id_servico='" + id + "' desc='" + nome + "'>" + bexcluir +
    "" + nome + "</td><td><select name='servicos_incluir' class='servicos' ><option value='-1'> <option value='2'>Inclus&atilde;o</option></select></td></tr>");

    $("input[name='add-outro-servico']").val("");
    $("input[name='add-outro-servico']").focus();


    return false;
  });

  $("#add-outro-servico").click(function () {

    var nome = $("input[name='outro-profi']").val();

    if(nome == "") {

      $("input[name='outro-profi']").css({
        "border": "3px solid red"
      });

    } else {
      $("input[name='outro-profi']").css({
        "border": ""
      });

      var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prof' title='Excluir' border='0' />&nbsp;&nbsp;&nbsp;";
      $('#alteracoes').append("<tr><td  class='servicos_td' id_servico='1111' desc='" + nome + "'>" + bexcluir + nome + "</td><td><select name='servicos_incluir' class='servicos' ><option value='-1'> <option value='2'>Inclus&atilde;o</option></select></td></tr>");

      $("input[name='outro-profi']").val("");
      $("input[name='outro-profi']").focus();

    }

    return false;

  });

  $("#intercorrencias_span").hide();

  $("select[name='intercorrencias']").change(function () {
    var X = $("select[name='intercorrencias'] option:selected").val();

    if(X == 's') {
      $("#intercorrencias_span").show();
      $("input[name='intercorrencias']").focus();
    } else {
      $("#intercorrencias_span").hide();
      $("input[name='intercorrencias']").val('');
    }
  });

  $("#add-intercorrencias").click(function () {
    var x = $("input[name='intercorrencias']").val();

    var bexcluir = "<img src='../utils/delete_16x16.png' class='del-intercorrencias-ativas' title='Excluir' border='0' >";

    $('#intercorrencias_tabela').append("<tr><td id='inter' class='interc' name='" + x + "' >" + bexcluir + "<b>Descri&ccedil;&atilde;o: </b>" + x + "</td></tr>");

    $("input[name='intercorrencias']").val("");
    $("input[name='intercorrencias']").focus();
    atualiza_intercorrencias_ativas();
    return false;


  });

  $(".del-intercorrencias-ativas").unbind("click", remove_intercorrencias_ativas).bind("click", remove_intercorrencias_ativas);


  function atualiza_intercorrencias_ativas() {
    $(".del-intercorrencias-ativas").unbind("click", remove_intercorrencias_ativas).bind("click", remove_intercorrencias_ativas);
  }

  function remove_intercorrencias_ativas() {
    $(this).parent().parent().remove();
    return false;
  }

  $('#add-internacao').click(function () {
    var contador = $(".internacao").size();
    contador++;
    var x = $("input[name='historicointernacao']").val();

    var bexcluir = "<img src='../utils/delete_16x16.png' class='del-internacao' title='Excluir' border='0' style='cursor:pointer;'>";
    if(x != '' && x != undefined) {
      $('#internacao').append(
        "<tr><td colspan='4' class='internacao item_internacao'  desc='" + x + "'> " + bexcluir + "  " + "  <b >Motivo Internação " + contador + ":</b><label name='motivo_internacao' class='internacoes'> " + x + "<label/> </td> </tr>");
    }
    $("input[name='historicointernacao']").val("");

    atualiza_rem_internacao();
    return false;

  });

  atualiza_rem_internacao();

  function atualiza_rem_internacao() {
    $(".del-internacao").unbind("click", remove_internacao).bind("click",
      remove_internacao);
  }

  function remove_internacao() {
    $(this).parent().parent().remove();
    return false;
  }

  $(".data").datepicker({ //Atualizado conforme adm.js - 19/12/2012
    inline: true,
    changeYear: true,
    changeMonth: true,
    yearRange: '1900:2100'
  });

  $('.numerico').keyup(function (e) {
    this.value = this.value.replace(/\D/g, '');
  });

  $('.numerico1').keyup(function (e) {
    this.value = this.value.replace(/[^0-9.]/g, '');
  });


  $(".incluir").click(function ()
  {
    //$("input[name='inicio']").datepicker( "setDate" , $("#tabela-itens").attr("inicio") );
    //$("input[name='fim']").datepicker( "setDate" , $("#tabela-itens").attr("fim") );

    if($("#nome").val() != '') {
      $.post("query.php", {
        query: "gravar-outro",
        nome: $("#nome").val()
      });
    }
  });

  $("#buscar").click(function () {
    var p = $("select[name='paciente'] option:selected").val();
    var status = $("select[name='paciente'] option:selected").attr('status');
    var i = $("#div-busca input[name='inicio']").val();
    var f = $("#div-busca input[name='fim']").val();

    $.post("query.php", {
      query: "buscar-prescricoes",
      paciente: p,
      inicio: i,
      fim: f,
      status: status
    }, function (r) {
      $("#div-resultado-busca").html(r);
    });
    return false;
  });

  $(".desativar-presc").live("click", function () {
    var element = $(this);
    var presc = $(this).attr('presc');
    $.post("query.php", {
      query: "desativar-prescricao",
      presc: presc
    }, function (r) {
      if(r == 1) {
        alert("Prescrição desativada com sucesso!");
          element.closest('tr').children('td').eq(4).html('Cancelada');
          element.hide();
      }
    });
  });

  //-----------------------------||||||||||||||||||VALIDAÇÃO DAS PRESCRIÇÕES||||||||||||||||||-----------------------------
  $(".validacao_periodo_busca").change(function () {

    var inicio = $("input[name='inicio']").val();
    var fim = $("input[name='fim']").val();

    inicio = inicio.split("/").reverse().join("");
    fim = fim.split("/").reverse().join("");


    if(fim < inicio) {
      alert("ERRO: A data final estÃ¡ menor do que a data inicial!");
      $("input[name='fim']").attr("style", "border: 3px solid red;");
    } else {
      $("input[name='fim']").removeAttr("style");
    }

  });

  $(".inicio-periodo").datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 1,
    onClose: function (selectedDate) {
      $(".fim-periodo").datepicker(
        'option',
        'minDate',
        selectedDate
      )
    }
  });
  $(".fim-periodo").datepicker({
    minDate: $(".inicio-periodo").val(),
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 1
  });

  if($("input[name=inicio]").val() !== undefined && $("input[name=fim]").val() !== undefined)
  {
    var inicio = new Date($("input[name=inicio]").val().split("/").reverse().join(","));
    var fim = new Date($("input[name=fim]").val().split("/").reverse().join(","));
    $(".prescricao-inicio").datepicker({
      defaultDate: "+1w",
      minDate: inicio,
      maxDate: fim,
      changeMonth: true,
      numberOfMonths: 1
    });
    $(".prescricao-fim").datepicker({
      minDate: inicio,
      maxDate: fim,
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1
    });
  }

  function validacao_prescricao() {
    if($("#fim").attr('dialog') === 'true') {
      var inicio = $("input[name='inicio']").get(1).value;
      var fim = $("input[name='fim']").get(1).value;
      inicio = inicio.split("/").reverse().join("");
      fim = fim.split("/").reverse().join("");
      if(fim < inicio) {
        alert("ERRO: A data final está menor do que a data inicial!");
        $("#fim").attr("style", "border: 3px solid red;");
        return false;
      } else {
        $("input[name='fim']").removeAttr("style");
        return true;
      }
    } else {
      var inicio = $("input[name='inicio']").get(0).value;
      var fim = $("input[name='fim']").get(0).value;
      inicio = inicio.split("/").reverse().join("");
      fim = fim.split("/").reverse().join("");
      if(fim < inicio) {
        alert("ERRO: A data final está menor do que a data inicial!");
        $("input[name='fim']").attr("style", "border: 3px solid red;");
        $("input[name='fim']").attr('value', $("input[name='inicio']").val())
        return false;
      } else {
        $("input[name='fim']").removeAttr("style");
        return true;
      }
    }
  }
  //-----------------------------||||||||||||||||||FIM VALIDAÇÃO DAS PRESCRIÇÕES||||||||||||||||||-----------------------------


  //    $(".validacao_periodo").change(function() {
  //
  //        var inicio = $("input[name='inicio']").get(0).value;
  //        var fim = $("input[name='fim']").get(0).value;
  //
  //        inicio = inicio.split("/").reverse().join("");
  //        fim = fim.split("/").reverse().join("");
  //
  //        if (fim < inicio) {
  //            alert("ERRO: A data final estÃ¡ menor do que a data inicial!");
  //            $("input[name='fim']").attr("style", "border: 3px solid red;");
  //
  //        } else {
  //            $("input[name='fim']").removeAttr("style");
  //        }
  //
  //    });


  $("select[name='frequencia']").change(function () {
    if($(this).find("option:selected").html() == 'ACM') {
      $(this).parent().find("input[name='obs']").addClass("OBG");
    }
    else {
      $(this).parent().find("input[name='obs']").removeClass("OBG");
      $(this).parent().find("input[name='obs']").css({
        "border": ""
      });
    }
  });

  $("#manter-data").click(function () {

    if($(this).is(':checked')) {
      $("#manter_data").val('1');

    } else {
      $("#manter_data").val('');
    }
  });


  $(".comp-nebulizacao").click(function () {


    if($("#check-outro-neb").is(":checked")) {
      $("#check-outro-neb").attr("checked", false);
      $("#nome-outro-neb").removeClass("OBG");
      $("#nome-outro-neb").removeAttr("style");
      $("#nome-outro-neb").val("");
      $("#dose-outro-neb").attr('disabled', 'disabled');
      $("#dose-outro-neb").removeClass("OBG");
      $("#dose-outro-neb").val("");
      $("#dose-outro-neb").removeAttr("style");
      $('#um-outro-neb').removeClass("COMBO_OBG");
      $('#um-outro-neb').removeAttr("style");
      $('#um-outro-neb').val("-1");
      $("#busca-outro-neb").attr('disabled', 'disabled');
      $("#busca-outro-neb").val("");
    } else {

      if($("#Atrovent").is(":checked") || $("#Berotec").is(":checked")) {
        $("#frequencia").addClass("COMBO_OBG");
        $("#um-diluicao-neb").addClass("COMBO_OBG");
        $("#qtd-diluicao").addClass("OBG");
        $("#diluicao-nebulizacao").addClass("OBG");




      } else {
        $("#um-diluicao-neb").val("-1");
        $("#qtd-diluicao").val("");
        $("#diluicao-nebulizacao").val("");
        $("#frequencia").val("-1");

        //$("#qtd-diluicao").removeClass("OBG");
        //$("#um-diluicao-neb").removeClass("COMBO_OBG");
        // $("#diluicao-nebulizacao").removeClass("OBG");
        //$("#frequencia").removeClass("COMBO_OBG");

        //$("#um-diluicao-neb").removeAttr("style");
        //$("#frequencia").removeAttr("style");
        //$("#qtd-diluicao").removeAttr("style");
        //$("#diluicao-nebulizacao").removeAttr("style");
      }
    }
    var mark = this.checked;
    if(mark) {
      $(this).parent().parent().children('td').eq(2).children('input').attr('disabled', '');
      $(this).parent().parent().children('td').eq(2).children('input').focus();
      $(this).parent().parent().children('td').eq(2).children('input').addClass("OBG");
      $(this).parent().parent().children('td').eq(3).children('select').addClass("COMBO_OBG");



    } else {
      $(this).parent().parent().children('td').eq(2).children('input').attr('disabled', 'disabled');
      $(this).parent().parent().children('td').eq(2).children('input').removeClass("OBG");
      $(this).parent().parent().children('td').eq(2).children('input').val("");
      $(this).parent().parent().children('td').eq(2).children('input').removeAttr("style");
      $(this).parent().parent().children('td').eq(3).children('select').removeClass("COMBO_OBG");
      $(this).parent().parent().children('td').eq(3).children('select').removeAttr("style");
      $(this).parent().parent().children('td').eq(3).children('select').val("-1");



    }
  });

  $(".comp-nebulizacao-outro").click(function () {


    if($(".comp-nebulizacao").is(":checked"))
    {


      $("#Atrovent").attr("checked", false);
      $("#Berotec").attr("checked", false);

      $(".comp-nebulizacao").parent().parent().children('td').eq(2).children('input').attr('disabled', 'disabled');
      $(".comp-nebulizacao").parent().parent().children('td').eq(2).children('input').removeClass("OBG");
      $(".comp-nebulizacao").parent().parent().children('td').eq(2).children('input').val("");
      $(".comp-nebulizacao").parent().parent().children('td').eq(2).children('input').removeAttr("style");
      $(".comp-nebulizacao").parent().parent().children('td').eq(3).children('select').removeClass("COMBO_OBG");
      $(".comp-nebulizacao").parent().parent().children('td').eq(3).children('select').removeAttr("style");
      $(".comp-nebulizacao").parent().parent().children('td').eq(3).children('select').val("-1");



      $("#comp-nebulizacao2 input[name='neb']").attr('disabled', 'disabled');
      $("#comp-nebulizacao2 input[name='neb']").removeClass("OBG");
      $("#comp-nebulizacao2 input[name='neb']").val("");
      $("#comp-nebulizacao2 input[name='neb']").removeAttr("style");
      $("#comp-nebulizacao2select").removeClass("COMBO_OBG");
      $("#comp-nebulizacao2select").val("-1");
      $("#comp-nebulizacao2select").removeAttr("style");



    } else {


      if($("#check-outro-neb").is(":checked")) {
        $("#frequencia").addClass("COMBO_OBG");
        $("#um-diluicao-neb").addClass("COMBO_OBG");
        $("#qtd-diluicao").addClass("OBG");
        $("#diluicao-nebulizacao").addClass("OBG");




      } else {
        $("#um-diluicao-neb").val("-1");
        $("#qtd-diluicao").val("");
        $("#diluicao-nebulizacao").val("");
        $("#frequencia").val("-1");

        //$("#qtd-diluicao").removeClass("OBG");
        //$("#um-diluicao-neb").removeClass("COMBO_OBG");
        //$("#diluicao-nebulizacao").removeClass("OBG");
        //$("#frequencia").removeClass("COMBO_OBG");

        //$("#um-diluicao-neb").removeAttr("style");
        //$("#frequencia").removeAttr("style");
        //$("#qtd-diluicao").removeAttr("style");
        //$("#diluicao-nebulizacao").removeAttr("style");
      }
    }
    var mark = this.checked;
    if(mark) {
      $("#nome-outro-neb").addClass("OBG");
      $("#dose-outro-neb").addClass("OBG").attr('disabled', '');
      $('#um-outro-neb').addClass("COMBO_OBG");
      $("#busca-outro-neb").attr('disabled', '').focus();



    } else {
      $("#nome-outro-neb").removeClass("OBG").css({
        "border": ""
      }).val("");
      $("#dose-outro-neb").removeClass("OBG").attr('disabled', '').css({
        "border": ""
      }).val("");
      $('#um-outro-neb').removeClass("COMBO_OBG").css({
        "border": ""
      });
      $("#busca-outro-neb").attr('disabled', 'disabled');
      $("#busca-outro-neb").val("");


    }
  });

  $("#busca-outro-neb").autocomplete({
    source: function (request, response) {
      $.getJSON('busca_brasindice.php', {
        term: request.term,
        tipo: 0
      }, response);
    },
    minLength: 3,
    select: function (event, ui) {
      $("#busca-outro-neb").val('');
      $('#nome-outro-neb').val(ui.item.value);
      $('#brasindice_id').val(ui.item.catalogo_id);
      $('#busca-outro-neb').val('');
      $("#dose-outro-neb").focus();
      return false;
    }
  });

  ///////////problemas ativos
  $(".outro_ativo").hide();
  $("#outros-prob-ativos").click(function () {
    if($(this).is(":checked")) {

      $(".outro_ativo").show();
    } else {
      $(".outro_ativo").hide();
    }

  });

  $("#add-prob-ativo").click(function () {
    var x = $("input[name='outro-prob-ativo']").val();

    if(x == "") {

      $("input[name='outro-prob-ativo']").css({
        "border": "3px solid red"
      });

    } else {
      $("input[name='outro-prob-ativo']").css({
        "border": ""
      });

      var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prob-ativos' title='Excluir' border='0' >";
      $('#problemas_ativos').append("<tr><td colspan='2' class='prob-ativos' cod='1111' desc='" + x + "'> " + bexcluir + " " + x + "</td></tr>");
      $("input[name='busca-problemas-ativos']").val("");
      $("input[name='outro-prob-ativo']").val("");
      $("#outros-prob-ativos").attr('checked', false);
      $(".outro_ativo").hide();
      $("input[name='busca-problemas-ativos']").focus();
      atualiza_rem_problemas_ativos();

    }

    return false;

  });



  //habilitar o campo de frequencia e justificativa de profissional
  $(".profissional").live("change", function () {
    if($(this).is(':checked')) {
      $(this).parent().parent().children('td').eq(2).children('textarea').attr('readonly', false);
      $(this).parent().parent().children('td').eq(1).children('select').attr('disabled', false);
      $(this).parent().parent().children('td').eq(2).children('select').attr('disabled', false);
      $(this).parent().parent().children('td').eq(2).children('select').addClass('COMBO_OBG');
      $(this).parent().parent().children('td').eq(2).children('textarea').addClass('OBG');
      if($(this).parent().parent().children('td').eq(2).children("textarea[name='justificativa']").attr('opcao') == 1)
        $(this).parent().parent().children('td').eq(2).children("textarea[name='justificativa']").attr('style', 'display:inline');
      $(this).parent().parent().children('td').eq(1).children('select').addClass('COMBO_OBG');
    } else {
      $(this).parent().parent().children('td').eq(2).children('textarea').attr('value', '');
      $(this).parent().parent().children('td').eq(2).children('textarea').attr('readonly', true);
      $(this).parent().parent().children('td').eq(1).children("select option[value='-1']").attr('selected:selected');
      $(this).parent().parent().children('td').eq(2).children("select option[value='-1']").attr('selected:selected');
      $(this).parent().parent().children('td').eq(1).children('select').attr('disabled', true);
      $(this).parent().parent().children('td').eq(2).children('select').attr('disabled', true);
      $(this).parent().parent().children('td').eq(2).children('textarea').removeClass('OBG');
      $(this).parent().parent().children('td').eq(2).children('select').removeClass('COMBO_OBG');
      $(this).parent().parent().children('td').eq(1).children('select').removeClass('COMBO_OBG');
      $(this).parent().parent().children('td').eq(2).children('textarea').removeAttr("style");
      $(this).parent().parent().children('td').eq(1).children('select').removeAttr("style");
      $(this).parent().parent().children('td').eq(2).children("textarea[name='justificativa']").attr('style', 'display:none');
      $(this).parent().parent().children('td').eq(2).children("select[name='justificativa']").attr('value', '-1');
      $(this).parent().parent().children('td').eq(1).children("select[name='frequencia']").attr('value', '-1');
    }
  });

  function justificativa(x) {
    $.post("relatorios.php", {
      query: "justificativa",
      disabled: "2"
    }, function (r) {
      $(".justificativa" + x).html(r);
    });
  }

  function frequencia(x) {
    $.post("relatorios.php", {
      query: "frequencia",
      disabled: "2"
    }, function (r) {
      $(".frequencia_" + x).html(r);
    });
  }
  /////////18-06-2013
  //adicionar profissional
  $("#add-profissional").click(function () {
    var cont = $(".profissional").size();
    var cont = cont + 1;
    var x = $("input[name='outro-prof']").val();

    if(x == "") {

      $("input[name='outro-prof']").css({
        "border": "3px solid red"
      });

    } else {
      $("input[name='outro-prof']").css({
        "border": ""
      });

      var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prof' title='Excluir' border='0' />&nbsp;&nbsp;&nbsp;";
      $('#profissionais').append("<tr><td  class='prob-ativos' id_especialidade='1111' desc='" + x + "'>" + bexcluir + "<input type='checkbox' checked='checked' value='1111' id_especialidade='1111' class='profissional'>" +
      "" + x + "</td><td class='frequencia_" + cont + "'>" +
      "</td><td><textarea cols=65 rows=2 type=text  name='justificativa' class='OBG' size='100' ></textarea></td></tr>");

      $("input[name='outro-prof']").val("");

      $(".outro_ativo").hide();
      $("input[name='outro-prof']").focus();

      frequencia(cont);
    }

    return false;

  });

  $(".del-prof").live("click", function () {
    $(this).parent().parent().remove();
    return false;
  });
  //adicionar procedimentos

  $("#procedimento").hide();
  if($(".procedimento_sn").val() == 1) {
    $("#procedimento").show();
  }
  $(".procedimento_sn").change(function () {
    if($(this).val() == 1) {
      $("#procedimento").show();
    } else {
      $("#procedimento").hide();
      $(".procedimento-check").each(function () {
        $(this).attr('checked', false);
        $(this).parent().parent().children('td').eq(1).children('textarea').val('');

      });
    }
  });

  $(".procedimento").click(function () {
    if($(this).is(':checked')) {
      $(this).parent().parent().children('td').eq(1).children('textarea').attr('readonly', false);
      $(this).parent().parent().children('td').eq(1).children('textarea').addClass('OBG');

    } else {
      $(this).parent().parent().children('td').eq(1).children('textarea').attr('value', '');
      $(this).parent().parent().children('td').eq(1).children('textarea').attr('readonly', true);
      $(this).parent().parent().children('td').eq(1).children('textarea').removeClass('OBG');
      $(this).parent().parent().children('td').eq(1).children('textarea').removeAttr("style");

    }
  });

  $("#add-procedimento").click(function () {

    var x = $("input[name='nome-procedimento']").val();

    if(x == "") {

      $("input[name='nome-procedimento']").css({
        "border": "3px solid red"
      });

    } else {
      $("input[name='nome-procedimento']").css({
        "border": ""
      });

      var bexcluir = "<img src='../utils/delete_16x16.png' class='del-procedimento' title='Excluir' border='0' />&nbsp;&nbsp;&nbsp;";
      $('#procedimentos').append("<tr><td  class='' cod='1111' desc='" + x + "'>" + bexcluir + "<input type='checkbox' checked='checked' class='procedimento-check'>  " + x + "</input></td><td><textarea cols=65 rows=2 type=text  name='justicicativa' class='OBG' size='100' ></textarea></td></tr>");

      $("input[name='nome-procedimento']").val("");

      $(".outro_ativo").hide();
      $("input[name='nome-procedimento']").focus();


    }

    return false;

  });

  $(".procedimento-check").live('click', function () {
    if($(this).is(':checked')) {
      $(this).parent().parent().children('td').eq(1).children('textarea').addClass('OBG');
      $(this).parent().parent().children('td').eq(1).children('textarea').attr('readonly', false);
    } else {
      $(this).parent().parent().children('td').eq(1).children('textarea').removeClass('OBG');
      $(this).parent().parent().children('td').eq(1).children('textarea').removeAttr("style");
      $(this).parent().parent().children('td').eq(1).children('textarea').attr('readonly', true);
      $(this).parent().parent().children('td').eq(1).children('textarea').val('');
    }

  });

  $(".del-procedimento").live("click", function () {
    $(this).parent().parent().remove();
    return false;
  });

  ////////17-06-2013 jeferson relatÃ³rio prorogaÃ§Ã£o fim

  $("#cuid2").hide();
  $("select[name=cuidados]").change(function () {
    x = $(this).val();
    if(x == 2 || x == 3 || x == 4 || x == 5 || x == 31 || x == 46 || x == 43 || x == 44 || x == 45) { //|| x == 28 || x == 29 || x == 30
      $("#cuid1").hide();
      $("#cuid1").attr("n", 0);
      $("#cuid2").attr("n", 1);
      $("#cuid1").children('select').removeClass("COMBO_OBG");
      $("#cuid1").children('select').removeClass("style");
      $("#cuid2").show();

    } else {
      $("#cuid2").hide();
      $("#cuid1").children('select').addClass("COMBO_OBG");
      $("#cuid1").show();
      $("#cuid2").attr("n", 0);
      $("#cuid1").attr("n", 1);
    }

  });

  $("select[name='justificativa']").live("change", function ()
  {
    if($(this).val() == -2)
    {
      $(this).parent().children("textarea[name='justificativa']").attr('style', 'display:inline');
      $(this).parent().children("textarea[name='justificativa']").addClass("OBG");
    }
    else
    {
      $(this).parent().children("textarea[name='justificativa']").attr('style', 'display:none');
      $(this).parent().children("textarea[name='justificativa']").removeClass("OBG");
    }
  });

  ////////////////Eriton 02-08-2013/////////////////
  $("#salvar_prorrogacao").click(function () {
    if(validar_campos("relatorio-prorrogacao")) {
      var paciente_id = $(this).attr("paciente");
      var total = $(".prob-ativos-evolucao").size();
      var dados = $("form").serialize();
      var probativos = "";
      $(".prob-ativos-evolucao").each(function (i) {
        var cid = $(this).attr("cod");
        var desc = $(this).attr("desc");
        var just = $(this).children('textarea').val();
        probativos += cid + "#" + desc + "#" + just;
        if(i < total - 1)
          probativos += "$";
      });


      var total_intercorrencia = $(".interc").size();
      var intercorrencias = "";
      $(".interc").each(function (i) {
        var interc = $(this).attr("name");
        intercorrencias += interc;
        if(i < total_intercorrencia - 1) {
          intercorrencias += "$";
        }
      });

      var total_dor = $(".dor1").size();
      var dor123 = "";
      $(".dor1").each(function (i) {
        var nome = $(this).attr('name');
        var padrao_id = $(this).children('select[name=padrao_dor]').val();
        var padrao = $(this).children('select[name=padrao_dor]').find('option').filter(':selected').text();
        var escala = $("select[name='dor" + nome + "'] option:selected").val();
        ///05/08
        if(padrao_id == 5) {
          padrao = $(this).children('input[name=padraodor]').val();
        }
        dor123 += nome + "#" + padrao_id + "#" + padrao + "#" + escala;
        if(i < total_dor - 1) {
          dor123 += "$";
        }
      });


      var total_profissionais = $(".profissional").size();
      var profissional = "";

      $("input.profissional:checked").each(function (i) {
        var cod_especialidade = $(this).val();
        var especialidade = $(this).parent().text();
        var frequencia = $(this).parent().parent().children('td').eq(1).children('select[name=frequencia]').val();
        var justificativa_id = $(this).parent().parent().children('td').eq(2).children('select[name=justificativa]').val();
        if(justificativa_id == -2) {
          var justificativa = $(this).parent().parent().children('td').eq(2).children('textarea').val();
        } else {
          var justificativa = $(this).parent().parent().children('td').eq(2).children('select[name=justificativa]').find('option').filter(':selected').attr("data-justificativa");
        }
        var id_especialidade = $(this).attr('id_especialidade');
        if(justificativa_id == undefined) {
          justificativa_id = 0;
          justificativa = $(this).parent().parent().children('td').eq(2).children('textarea').val();
        }
        profissional += cod_especialidade + "#" + especialidade + "#" + frequencia + "#" + justificativa + "#" + justificativa_id + "#" + id_especialidade;
        if(i < total_profissionais - 1)
          profissional += "$";
      });

      var procedimento = "";
      var total_procedimento = $(".procedimento-check").size();
      $("input.procedimento-check:checked").each(function (i) {

        var desc_procedimento = $(this).parent().text();
        var just_procedimento = $(this).parent().parent().children('td').eq(1).children('textarea').val();
        var cod_procedimento = $(this).val();

        procedimento += desc_procedimento + "#" + just_procedimento + "#" + cod_procedimento;
        if(i < total_procedimento - 1) {
          procedimento += "$";
        }
      });

        var score_katz = '';
        $("input.katz:checked").each(function (i) {
            score_katz += 'katz#' + $(this).attr('name') + '#' + $(this).val() + "$";
        });

        var score_nead2016 = '';
        $("input.grupo1:checked").each(function (i) {
            score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).val() + "$";
        });

        $("input.grupo2:checked").each(function (i) {
            score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).val() + "$";
        });

        $("input.grupo3:checked").each(function (i) {
            score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).attr('peso') + "$";
        });

      var alta = $('input[name=alta]').val();
        score_katz = score_katz.substring(0,(score_katz.length - 1));
        score_nead2016 = score_nead2016.substring(0,(score_nead2016.length - 1));

      var prob = "query=prorrogacao&" + dados + "&probativos=" + probativos + "&dor123=" + dor123 + "&profissional=" + profissional + "&procedimento=" + procedimento + "&intercorrencias=" + intercorrencias + "&score_katz=" + score_katz + "&score_nead2016=" + score_nead2016 + "&paciente=" + paciente_id + "&alta=" + alta;

      $.ajax({
        type: "POST",
        url: "query.php",
        data: prob,
        success: function (msg) {
          if(msg == 1) {
            alert("Salvo com sucesso!!");
            window.location.href = '?view=relatorio&opcao=3&id=' + paciente_id;
          } else {
            alert(msg);
            window.location.href = '?view=relatorio&opcao=3&id=' + paciente_id;
          }
        }
      });
      return false;
    }
    return false;
  });

  ///////////////Eriton 20-06-2013//////////////////
  $("#editar_prorrogacao").click(function () {
    if(validar_campos("relatorio-prorrogacao")) {
      var paciente_id = $(this).attr("paciente");
      var total = $(".prob-ativos-evolucao").size();
      var dados = $("form").serialize();
      var probativos = "";
      $(".prob-ativos-evolucao").each(function (i) {
        var cid = $(this).attr("cod");
        var desc = $(this).attr("desc");
        var just = $(this).children('textarea').val();
        probativos += cid + "#" + desc + "#" + just;
        if(i < total - 1)
          probativos += "$";
      });

      var total_dor = $(".dor1").size();
      var dor123 = "";
      $(".dor1").each(function (i) {
        var nome = $(this).children('input').val();
        var padrao_id = $(this).children('select[name=padrao_dor]').val();
        var padrao = $(this).children('select[name=padrao_dor]').find('option').filter(':selected').text();
        var escala = $("select[name='dor" + nome + "'] option:selected").val();
        if(padrao_id == 5) {
          padrao = $(this).children('input[name=padraodor]').val();
        }
        dor123 += nome + "#" + padrao_id + "#" + padrao + "#" + escala;
        if(i < total_dor - 1) {
          dor123 += "$";
        }
      });

      var total_profissionais = $(".profissional").size();
      var profissional = "";

      $("input.profissional:checked").each(function (i) {
        var cod_especialidade = $(this).val();
        var especialidade = $(this).parent().text();
        var frequencia = $(this).parent().parent().children('td').eq(1).children('select[name=frequencia]').val();
        var justificativa_id = $(this).parent().parent().children('td').eq(2).children('select[name=justificativa]').val();
        if(justificativa_id == -2) {
          var justificativa = $(this).parent().parent().children('td').eq(2).children('textarea').val();
        } else {
          var justificativa = $(this).parent().parent().children('td').eq(2).children('select[name=justificativa]').find('option').filter(':selected').attr("data-justificativa");
        }
        var id_especialidade = $(this).attr('id_especialidade');
        if(justificativa_id == undefined) {
          justificativa_id = 0;
          justificativa = $(this).parent().parent().children('td').eq(2).children('textarea').val();
        }
        profissional += cod_especialidade + "#" + especialidade + "#" + frequencia + "#" + justificativa + "#" + justificativa_id + "#" + id_especialidade;
        if(i < total_profissionais - 1)
          profissional += "$";
      });

      var procedimento = "";
      var total_procedimento = $(".procedimento-check").size();
      $("input.procedimento-check:checked").each(function (i) {
        var desc_procedimento = $(this).parent().text();
        var just_procedimento = $(this).parent().parent().children('td').eq(1).children('textarea').val();
        var cod_procedimento = $(this).val();
        procedimento += desc_procedimento + "#" + just_procedimento + "#" + cod_procedimento;
        if(i < total_procedimento - 1)
          procedimento += "$";
      });

      var servicos = "";
      var total_servico = $(".servicos").size();
      if($("select[name='servicos'] option:selected").val() == 1) {
        $(".servicos").each(function (i) {
          //var alterar = $(this).val();
          if($(this).val() != 1) {
            var servico = $(this).parent().parent().children("td").eq(0).attr("desc");
            var id_servico = $(this).parent().parent().children("td").eq(0).attr("id_servico");
            var mudanca = $(this).val();

            servicos += servico + "#" + id_servico + "#" + mudanca;
            if(i < total_servico - 1) {
              servicos += "$";
            }
          }
        });
      }

      var total_intercorrencia = $(".interc").size();
      var intercorrencias = "";
      $(".interc").each(function (i) {
        var interc = $(this).attr("name");
        intercorrencias += interc;
        if(i < total_intercorrencia - 1) {
          intercorrencias += "$";
        }
      });

      var score_nead7 = {};
      var codigo_item = 0;
      var texto_nead = "{";
      var count = $('.nead7:checked').size();
      var i = 1;
      $('.nead7:checked').each(function () {
        codigo_item = $(this).attr('cod_item');
        texto_nead += '"' + codigo_item + '":"' + $(this).val() + (count != i ? '",' : '"}');
        i++;
      });
        var score_katz = '';
        $("input.katz:checked").each(function (i) {
            score_katz += 'katz#' + $(this).attr('name') + '#' + $(this).val() + "$";
        });

        var score_nead2016 = '';
        $("input.grupo1:checked").each(function (i) {
            score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).val() + "$";
        });

        $("input.grupo2:checked").each(function (i) {
            score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).val() + "$";
        });

        $("input.grupo3:checked").each(function (i) {
            score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).attr('peso') + "$";
        });

        var alta = $('input[name=alta]').val();
        score_katz = score_katz.substring(0,(score_katz.length - 1));
        score_nead2016 = score_nead2016.substring(0,(score_nead2016.length - 1));

      var total_score_nead = $('#total_score_nead').val();
      var prob = "query=editar_prorrogacao&" + dados + "&probativos=" + probativos + "&dor123=" + dor123 + "&profissional=" + profissional + "&procedimento=" + procedimento + "&servicos=" + servicos + "&intercorrencias=" + intercorrencias + "&score_nead=" + texto_nead + "&score_katz=" + score_katz + "&score_nead2016=" + score_nead2016 + "&paciente=" + paciente_id + "&alta=" + alta + "&total_score=" + total_score_nead;
      //            alert(prob);

      $.ajax({
        type: "POST",
        url: "query.php",
        data: prob,
        success: function (msg) {

          if(msg == 1) {
            alert("Salvo com sucesso!!");
            window.location.href = '?view=relatorio&opcao=3&id=' + paciente_id;
          } else {
            alert("DADOS: " + msg);
          }
        }
      });
      return false;
    }
    return false;
  });
  ///////////////Eriton 20-06-2013//////////////////

  function antibioticoAlta() {
    var medicamento = '';
    var total_medicamento = $(".medicamentos-alergia").size();
    $(".medicamentos-alergia").each(function (i) {
      var cod = $(this).attr('cod');
      var catalogo_id = $(this).attr('catalogo_id');
      if(cod == 0)
        catalogo_id = 0;
      var item = $(this).attr('desc');
      medicamento += catalogo_id + "#" + cod + "#" + item;
      if(i < total_medicamento - 1)
        medicamento += "$";
    });
    return medicamento;
  };

  function problemasAtivosAlta() {
    var probativos = '';
    var total_probativos = $(".probativo1").size();
    $(".probativo1").each(function (i) {
      var cid = $(this).attr('cid');
      var desc = $(this).attr('prob_atv');
      var obs = $(this).attr('estado');
      probativos += cid + "#" + desc + "#" + obs;
      if(i < total_probativos - 1)
        probativos += "$";
    });
    return probativos;
  };

  $("#salvar_rel_alta").click(function () {
    if(validar_campos("relatorio_alta")) {
      var paciente_id = $(this).attr("paciente");
      var dados = $("form").serialize();
      var total_profissionais = $(".profissional").size();
      var profissional = "";

      var total_dor = $(".dor1").size();
      var dor123 = "";
      $(".dor1").each(function (i) {
        var nome = $(this).attr("name");
        var padrao_id = $(this).parent().children('td').eq(0).children('.padrao_dor').val();
        var padrao = $(this).parent().children('td').eq(0).children('select[name=padraodor]').find('option').filter(':selected').text();
        var escala = $("select[name='dor" + nome + "'] option:selected").val();
        if(padrao_id == 5) {
          padrao = $(this).parent().children('td').eq(0).children('input').val();
        }
        dor123 += nome + "#" + padrao_id + "#" + padrao + "#" + escala;
        if(i < total_dor - 1) {
          dor123 += "$";
        }
      });

      var antibiotico = antibioticoAlta();
      var probativos = problemasAtivosAlta();

      var total_intercorrencia = $(".interc").size();
      var intercorrencias = "";
      $(".interc").each(function (i) {
        var interc = $(this).attr("desc");
        intercorrencias += interc;
        if(i < total_intercorrencia - 1) {
          intercorrencias += "$";
        }
      });

      $("input.profissional:checked").each(function (i) {
        var cod_especialidade = $(this).val();
        var especialidade = $(this).parent().text();
        var frequencia = $(this).parent().parent().children('td').eq(1).children('select[name=frequencia]').val();
        var justificativa = $(this).parent().parent().children('td').eq(2).children('textarea').val();
        var id_especialidade = $(this).attr('id_especialidade');
        profissional += cod_especialidade + "#" + especialidade + "#" + frequencia + "#" + justificativa + "#" + id_especialidade;
        if(i < total_profissionais - 1)
          profissional += "$";
      });


      var alta = "query=relatorio_alta&" + dados + "&dor123=" + dor123 + "&profissional=" + profissional + "&antibioticos=" + antibiotico + "&probativos=" + probativos + "&intercorrencias=" + intercorrencias;

      $.ajax({
        type: "POST",
        url: "query.php",
        data: alta,
        success: function (msg) {

          if(msg == 1) {
            alert("Salvo com sucesso!!");
            alert("Emita guias com a prescrição dos serviços para o paciente, garantindo dessa forma a segurança na continuidade do tratamento prescrito.");
            window.location.href = '?view=relatorio&opcao=1&id=' + paciente_id;
          } else {
            alert("DADOS: " + msg);
          }
        }
      });
      return false;
    }
    alert("Algum item não foi marcado");
    return false;
  });

  $("input[name='tomografia_outros']").hide();
  if($("input[name='tomo_outros']").is(":checked")) {
    $("input[name='tomografia_outros']").show();
  }
  $("input[name='tomo_outros']").click(function () {
    if($("input[name='tomo_outros']").is(":checked")) {
      $("input[name='tomografia_outros']").show();
    } else {
      $("input[name='tomografia_outros']").hide();
    }
  });

  $("input[name='outros_rx']").hide();
  if($("input[name='rx_outros']").is(":checked")) {
    $("input[name='outros_rx']").show();
  }
  $("input[name='rx_outros']").click(function () {
    if($("input[name='rx_outros']").is(":checked")) {
      $("input[name='outros_rx']").show();
    } else {
      $("input[name='outros_rx']").hide();
    }
  });

  //SM-688
  $('#incluir-outro-quadro-clinico').click(function () {

    var item = $('#outro-quadro-clinico').val();
    if(item == '' || item == null) {
      alert('O campo outros está vazio');

    } else {
      $('#tabela-quadro-clinico').append("<tr ><td colspan='3' class='wrapword' >" + "<input  type='hidden' value='-1' class='quadro_clinico' width = '50%' id='" + item + "'>" + "<div style='word-wrap: break-word:width: 200px'><span  style='word-wrap: break-word:width: 200px' >" + item + "</span>" + "  <img src='../utils/delete_16x16.png' class='del-outro-quadro-clinico' title='Excluir' border='0'>" + "</div></td>" + "</tr>");
      $('#outro-quadro-clinico').val('');

    }
    return false;

  });
  $('.del-outro-quadro-clinico').live('click', function () {
    $(this).parent().parent().parent().remove();
  });

  $('#incluir-outro-procedimento-encaminhamento').click(function () {

    var item = $('#outro-procedimento-encaminhamento').val();
    if(item == '' || item == null) {
      alert('O campo outros está vazio');

    } else {
      $('#tabela-procedimento-encaminhamento').append("<tr><td colspan='3' class='wrapword'>" + "<input type='hidden'  value='-1' class='quadro_encaminhamento' id='" + item + "'>" + "<div><span class='outro-alinhado-topo'>" + item + "</span>" + "  <img src='../utils/delete_16x16.png' class='del-outro-quadro-encaminhamento' title='Excluir' border='0'>" + "</div></td>" + "</tr>");
      $('#outro-procedimento-encaminhamento').val('');

    }
    return false;

  });
  $('.del-outro-quadro-encaminhamento').live('click', function () {
    $(this).parent().parent().parent().remove();

  });
  $('#incluir-outro-exame-encaminhamento').click(function () {

    var item = $('#outro-exame-encaminhamento').val();
    if(item == '' || item == null) {
      alert('O campo outros está vazio');

    } else {
      $('#tabela-outro-exame-encaminhamento').append("<tr><td colspan='3' class='wrapword'>" + "<input type='hidden'  value='-1' class='exame_encaminhamento' id='" + item + "'>" + "<div><span class='outro-alinhado-topo'>" + item + "</span>" + "  <img src='../utils/delete_16x16.png' class='del-outro-exame-encaminhamento' title='Excluir' border='0'>" + "</div></td>" + "</tr>");
      $('#outro-exame-encaminhamento').val('');

    }
    return false;


  });
  $('.del-outro-exame-encaminhamento').live('click', function () {
    $(this).parent().parent().parent().remove();

  });
  $("#salvar_intercorrencia_medica, #salvar_intercorrencia_medica_sem_prescricao").click(function () {

    if(validar_campos("relatorio-encaminhamento")) {
     var fazerPrescricao = $(this).attr('prescricao');
      var pacienteId = $('#id_paciente').val();
      console.log(pacienteId,fazerPrescricao);
      if($('#outro-quadro-clinico').val() != '') {
        alert('Para incluir Outro Quadro Clínico é necessario clicar no + (símbolo)');
        return false;
      }
      if($('#outro-exame-encaminhamento').val() != '') {
        alert('Para incluir Outro Exame é necessario clicar no + (símbolo)');
        return false;
      }
      if($('#outro-procedimento-encaminhamento').val() != '') {
        alert('Para incluir Outro Procedimento é necessario clicar no + (símbolo)');
        return false;
      }
      var total = $(".prob-ativos-evolucao").size();
      var dados = $("form").serialize();
      var probativos = "";
      $(".prob-ativos-evolucao").each(function (i) {
        var cid = $(this).attr("cod");
        var desc = $(this).attr("desc");
        var just = $(this).children('textarea').val();
        probativos += cid + "#" + desc + "#" + just;
        if(i < total - 1)
          probativos += "$";
      });

      var quadro_clinico = "";
      $(".quadro_clinico").each(function (i) {
        if($(this).is(":checked") || $(this).val() == -1) {
          var quadro_id = $(this).val();
          var descricao = $(this).attr('id');
          quadro_clinico += quadro_id + "#" + descricao;
          quadro_clinico += "$";

        }
      });


      var quadro_encaminhamento = "";
      $(".quadro_encaminhamento").each(function (i) {

        if($(this).is(":checked") || $(this).val() == -1) {
          var encaminhamento_id = $(this).val();
          var encaminhamento_descricao = $(this).attr('id');
          quadro_encaminhamento += encaminhamento_id + "#" + encaminhamento_descricao;
          quadro_encaminhamento += "$";
        }

      });

      var exame_encaminhamento = "";
      $(".exame_encaminhamento").each(function (i) {

        if($(this).is(":checked") || $(this).val() == -1) {
          var exame_encaminhamento_id = $(this).val();
          var exame_encaminhamento_descricao = $(this).attr('id');
          exame_encaminhamento += exame_encaminhamento_id + "#" + exame_encaminhamento_descricao;
          exame_encaminhamento += "$";
        }

      });

      //alert(quadro_encaminhamento);
      var encaminhamento = "query=salvar_intercorrencia_medica&" + dados + "&probativos=" + probativos + "&quadro_clinico=" +
          quadro_clinico + "&quadro_encaminhamento=" + quadro_encaminhamento + "&exame_encaminhamento=" + exame_encaminhamento;
      //            alert(encaminhamento);

      $.ajax({
        type: "POST",
        url: "query.php",
        data: encaminhamento,
        success: function (msg) {


          if(!isNaN(parseInt(msg)) ){
            if(fazerPrescricao == 'nao'){
              alert('Relatório salvo com sucesso!');
              window.document.location.href = "?view=relatorio&opcao=4&id="+pacienteId;

            }else {
              $("#intercorrencia").val(msg);
              $("#dialog-presc-aditiva-intercorrencia").dialog("open");
            }
          } else {
            alert("DADOS: " + msg);
          }
        }
      });
      return false;
    } else {
      return false;
    }
  });

  $("input[name='busca-problemas-ativos']").autocomplete({
    source: "/utils/busca_cid.php",
    minLength: 3,
    select: function (event, ui) {

      var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prob-ativos' title='Excluir' border='0' />";
      $('#problemas_ativos').append("<tr><td colspan='2' class='prob-ativos' cod='" + ui.item.id + "' desc='" + ui.item.name + "'> " + bexcluir + " " + ui.item.name + "</td></tr>");
      $("input[name='busca-problemas-ativos']").val("");
      $("input[name='busca-problemas-ativos']").focus();
      atualiza_rem_problemas_ativos();

      return false;
    },
  });

  $("input[name='diagnostico_principal']").autocomplete({
    source: "/utils/busca_cid.php",
    minLength: 3,
    select: function (event, ui) {

      $("input[name='diagnostico_principal']").val(ui.item.name);

      return false;
    },
  });

  $("input[name='diagnostico_secundario']").autocomplete({
    source: "/utils/busca_cid.php",
    minLength: 3,
    select: function (event, ui) {

      $("input[name='diagnostico_secundario']").val(ui.item.name);

      return false;
    },
  });

  $(".del-prob-ativos").unbind("click", remove_problemas_ativos).bind("click", remove_problemas_ativos);


  function atualiza_rem_problemas_ativos() {
    $(".del-prob-ativos").unbind("click", remove_problemas_ativos).bind("click", remove_problemas_ativos);
  }

  function remove_problemas_ativos() {
    $(this).parent().parent().remove();
    return false;
  }

  $("#salvar-problemas").click(function () {
    if(validar_campos("div-prob-ativo")) {
      var dados = $("form").serialize();
      var probativos = "";
      var total = $(".prob-ativos").size();

      $(".prob-ativos").each(function (i) {
        var cid = $(this).attr("cod");
        var desc = $(this).attr("desc");
        probativos += cid + "#" + desc;
        if(i < total - 1)
          probativos += "$";
      });

      var prob = "query=prob-ativos&" + dados + "&probativos=" + probativos;

      $.ajax({
        type: "POST",
        url: "query.php",
        data: prob,
        success: function (msg) {

          if(msg >= 0) {
            alert("Salvo com sucesso!!");
            window.location.href = '?medico';
          } else {
            alert("DADOS: " + msg);
          }
        }
      });
    }
    return false;
  });

  ////fim prob ativos 


  ///oxigenoterapia jeferson   

  $(".oxi").click(function () {
    $(".oxi2").each(function () {
      $("input[name='dose']").removeClass("OBG");
      $("input.oxi2").attr('disabled', 'disabled');
      $("input.oxi2").val("");
      $("select[name='dose']").removeClass("OBG");
      $("select.oxi2").attr('disabled', 'disabled');
      $("select.oxi2").val("");

    });
    $(this).parent().parent().children('td').eq(1).children('input').attr('disabled', '');
    $(this).parent().parent().children('td').eq(1).children('input').addClass("OBG");
    $(this).parent().parent().children('td').eq(1).children('input').focus();
    if($(this).attr('tipo') == 1) {
      $("select[id='s']").attr('disabled', '');
      $("select[id='s']").focus();
      $("select[id='s']").addClass("OBG");
    }

    if($(this).attr('tipo') == 3) {
      $("select[id='s1']").attr('disabled', '');
      $("select[id='s1']").focus();
      $("select[id='s1']").addClass("OBG");
    }

    if($(this).attr('tipo') == 0) {

      $("#u").keyup(function () {
        var valor = $(this).val().replace(/[^1-5]/g, '');
        $(this).val(valor);
      });

      $("#u1").keyup(function () {
        var valor = $(this).val().replace(/[^1-5]/g, '');
        $(this).val(valor);
      });

      $("#u2").keyup(function () {
        var valor = $(this).val().replace(/[^1-6]/g, '');
        $(this).val(valor);
      });

      $("#u4").keyup(function () {
        var valor = $(this).val().replace(/[^5-8]/g, '');
        $(this).val(valor);
      });
      $("#u5").keyup(function () {
        var valor = $(this).val().replace(/[^1-8]/g, '');
        $(this).val(valor);
      });

    }
    $("#frequenciaoxi").addClass("COMBO_OBG");
  });

  ///fim oxigenoterapia jeferson

  $("#iniciar-prescricao").click(function () {
    if(!validar_campos("div-iniciar-prescricao")) {

      return false;
    }

  });

  /// Criado por Ricardo para a grid 
  $("#iniciarprescricao").click(function () {
    if(!validar_campos("pre-presc") && !validacao_prescricao())
      return false;

  });

  $("#container-presc").hide();

  $("#dialog-salvo").dialog({
    autoOpen: false,
    modal: true,
    buttons: {
      "Sim": function () {
        $.post("query.php", {
          query: "email-aviso",
          p: $("#prescricao").attr("pnome")
        }, function (r) {
          $("#dialog-salvo").dialog("close");
          $("#dialog-imprimir").dialog("open");
        });
      },
      "Não": function () {
        $(this).dialog("close");
        $("#dialog-imprimir").dialog("open");
      }
    }
  });

  $("#dialog-salvo1").dialog({
    autoOpen: false,
    modal: true,
    buttons: {
      "Sim": function () {
        $.post("query.php", {
          query: "email-aviso",
          p: $("#prescricao").attr("pnome")
        }, function (r) {
          $("#dialog-salvo").dialog("close");
          $("#dialog-imprimir1").dialog("open");
        });
      },
      "Não": function () {
        $(this).dialog("close");
        $("#dialog-imprimir1").dialog("open");
      }
    }
  });

  //FUNCOES E EVENTOS DA JANELA DE INCONFORMIDADES E PRESCRIÇÕES
  $("#sugerir-correcoes-ficha-avaliacao").click(function () {
    $("#dialog-erros-ficha-avaliacao").attr("cap", $(this).attr("cap"));
    $("#dialog-erros-ficha-avaliacao").attr("pac", $(this).attr("pac"));
    $("#dialog-erros-ficha-avaliacao").attr("med", $(this).attr("med"));
    $("#dialog-erros-ficha-avaliacao").attr("local", $(this).attr("local"));
    $("#dialog-erros-ficha-avaliacao").dialog("open");
  });

  //FORMULARIO PARA INSERIR UMA NOVA LISTA DE SUGESTÕES
  var dialogFichaAvaliacao = "#dialog-erros-ficha-avaliacao";
  $(dialogFichaAvaliacao).dialog({
    autoOpen: false,
    modal: false,
    height: 350,
    width: 600,
    open: function (event, ui) {
      var html = '';
      html += "<form action='' method='post' id='form-sugestao'>";
      html += "<div class='wrap' style='font-size: 9pt;'>";
      html += "<p style='width: 100%; border=1px solid #ccc;' class='blocos'>";
      html += "<b>Sugestão Nº 1</b><br>";
      html += "<select name='tipos' class='tipos'>";
      html += "   <option value=''>Selecione um tipo...</option>";
      html += "   <option value='inc'>Inconformidade</option> ";
      html += "   <option value='erp'>Erro de Preenchimento</option> ";
      html += "</select> ";
      html += "<select name='secao' id='secao0' class='secao'>";
      if($(dialogFichaAvaliacao).attr('local') === 'fcav') {
        html += "   <option value=''>Selecione uma informação da ficha...</option> ";
        html += "   <option value='fam'>Ficha de Avaliação Mederi</option> ";
        html += "   <option value='mh'>Motivo da Hospitalização</option> ";
        html += "   <option value='pa'>Problemas Ativos</option> ";
        html += "   <option value='am'>Alergia Medicamentosa</option> ";
        html += "   <option value='aa'>Alergia Alimentar</option> ";
        html += "   <option value='com'>Comorbidades</option> ";
        html += "   <option value='ium'>Internações Últimos 12 Meses</option> ";
        html += "   <option value='pm'>Parecer Médico</option> ";
        html += "   <option value='eqp'>Equipamentos</option> ";
      } else {
        html += "   <option value=''>Selecione uma informação da prescrição...</option> ";
        html += "   <option value='rep'>Repouso</option> ";
        html += "   <option value='die'>Dieta</option> ";
        html += "   <option value='ce'>Cuidados Especiais</option> ";
        html += "   <option value='se'>Soros e Eletrólitos</option> ";
        html += "   <option value='ai'>Antibióticos Injetáveis</option> ";
        html += "   <option value='iv'>Injetáveis IV</option> ";
        html += "   <option value='im'>Injetáveis IM</option> ";
        html += "   <option value='sc'>Injetáveis SC</option> ";
        html += "   <option value='di'>Drogas Inalatórias</option> ";
        html += "   <option value='neb'>Nebulização</option> ";
        html += "   <option value='doe'>Drogas Orais/Enterais</option> ";
        html += "   <option value='dt'>Drogas Tópicas</option> ";
        html += "   <option value='susp'>Suspender</option> ";
        html += "   <option value='oxi'>Oxigenoterapia</option> ";
        html += "   <option value='doep'>Dietas Orais/Enterais/Parenterais</option> ";
        html += "   <option value='mat'>Materiais</option> ";
      }
      html += "</select> ";
      html += "<br>";
      html += "<textarea name='sugestao' id='sugestao0' class='sugestao' cols='50' rows='2' wrap='Sugestão'></textarea> ";
      html += "</p>";
      html += "</div>";
      html += "</form>";
      html += "<button class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover adicionar' role='button' aria-disabled='false'>";
      html += "<span class='ui-button-text'>Adicionar +</span></button> ";
      html += "<button class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover remover' role='button' aria-disabled='false'>";
      html += "<span class='ui-button-text'>Remover -</span></button> ";

      $("#dialog-erros-ficha-avaliacao").html(html);

      $(this).parent().draggable().css({
        "right": "15px",
        "position": "fixed",
        "top": "17px",
        "box-shadow": "0 1px 1px 0"
      }, "fast");

    },
    buttons: {
      Salvar: function () {
        var capmedica = $("#dialog-erros-ficha-avaliacao").attr("cap");
        var paciente = $("#dialog-erros-ficha-avaliacao").attr("pac");
        var medico = $("#dialog-erros-ficha-avaliacao").attr("med");
        var local = $("#dialog-erros-ficha-avaliacao").attr("local");
        var tipos = [];
        var secoes = [];
        var sugestoes = [];
        var dados = [];
        var i = 0;
        $('.tipos').each(function () {
          if($(this).val() !== '' && $('#secao' + i).val() !== '' && $('#sugestao' + i).val() !== '') {
            tipos[i] = $(this).val();
            secoes[i] = $('#secao' + i).val();
            sugestoes[i] = $('#sugestao' + i).val();
            dados[i] = [tipos[i], secoes[i], sugestoes[i]];
          }
          i++;

        });
        if(dados.length > 0) {
          var data = {
            query: "salvar-sugestao",
            local: local,
            medico: medico,
            capmedica: capmedica,
            paciente: paciente,
            dados: dados
          };

          $.post("query.php", data,
            function (r) {
              if(r == 1) {
                alert('Sugestão(ões) salva(s) com sucesso.');
                $(dialogFichaAvaliacao).dialog('close');
                location.reload();
              } else {
                alert('Um erro inesperado aconteceu. Contate o setor de TI.');
              }
            }); //.post
        } else {
          alert('Existem campos sem preencher!');
        }
      },
      Cancelar: function () {
        if(confirm('Fechando a tela de sugestões, todas as informações inseridas até o momento serão perdidas. Deseja continuar?') === true) {
          $(this).dialog('close');
        } else {
          return false;
        }
      }
    }
  });

  var j = 0;
  //REMOVE UMA SUGESTAO DE CORREÇÃO
  function removeCampo() {
    $(".remover").unbind("click");
    $(".remover").bind("click", function () {
      if($("p.blocos").length > 1) {
        $("p.blocos:last").remove();
      }
    });
    j--;
  }
  //ADICIONA UMA SUGESTÃO DE CORREÇÃO
  $(".adicionar").live('click', function () {
    j++;
    novoCampo = $("p.blocos:first").clone();
    novoCampo.find("b").html("Sugestão Nº " + j);
    novoCampo.find("select.secao").attr('id', 'secao' + j);
    novoCampo.find("textarea.sugestao").attr('id', 'sugestao' + j);
    novoCampo.insertAfter("p.blocos:last");
    removeCampo();
    j++;
  });
  //PERMITE O SECMED AUDITAR AS CORREÇÕES
  if(window.location.href.indexOf('editar_capmed') > 0 || window.location.href.indexOf('prescricao-avaliacao') > 0 || window.location.href.indexOf('view=paciente_med&act=capmed') > 0 || window.location.href.indexOf('view=paciente_med&act=detalhesprescricao') > 0) {

    $("#correcoes-ficha-avaliacao").click(function () {
      $("#erros-ficha-avaliacao").parent().show().draggable().css({
        "right": "15px",
        "position": "fixed",
        "top": "17px",
        "box-shadow": "0 1px 1px 0"
      }, "fast");;
    });
    $("#salvar-sugestao").click(function () {
      if(confirm('Deseja salvar as alterações?')) {
        var capmedica = $("#erros-ficha-avaliacao").attr("cap");
        var paciente = $("#erros-ficha-avaliacao").attr("pac");
        var medico = $("#erros-ficha-avaliacao").attr("med");
        var local = $("#erros-ficha-avaliacao").attr("local");
        var i = 0;
        var j = 0;
        if($('.corrigidas:checked').size() !== parseInt($('#totalsugestoes').val())) {
          //Parcialmente corrigido
          var dados = [];
          $('.corrigidas').each(function () {
            if($(this).is(':checked')) {
              dados[j] = [$("#idsugestao" + i).val(), $("#idsugestaolista" + i).val()];
              j++;
            }
            i++;
          });
        } else {
          //Totalmente corrigido
          var dados = [];
          dados[0] = [true, 1, $("#idsugestao0").val()];
        }

        if(dados.length > 0) {
          var data = {
            query: "salvar-correcoes",
            local: local,
            medico: medico,
            capmedica: capmedica,
            paciente: paciente,
            dados: dados
          };

          $.post("query.php", data,
            function (r) {
              if(r == 1) {
                alert('Correção(ões) salva(s) com sucesso!');

                location.reload();
              } else {
                alert('Ocorreu um erro. Entre em contato com o setor de TI.');

              }
            }); //.post
        } else
          return false;
        //$("#erros-ficha-avaliacao").parent().hide();
      }
    });
    $('#fechar-sugestao').click(function () {
      $("#erros-ficha-avaliacao").parent().hide();
    });
  }

  //REFERENTE AO RELATORIO DO INDICADOR DE QUALIDADE DE ERROS DE PREENCHIMENTO DE FICHA

  $("#pesquisa-indicador").click(function () {
    var ur = $("#todos-ur").is(":checked") !== true ? $("#ur").val() : 't';
    var medico = $("#todos-medico").is(":checked") !== true ? $("#medico").val() : 't';
    var inicio = $("input[name=inicio]").val();
    var fim = $("input[name=fim]").val();
    var tipo_indicador = $("#tipo-indicador").val();

    var data = {
      query: "indicador-qualidade",
      ur: ur,
      medico: medico,
      inicio: inicio,
      fim: fim,
      tipo_indicador: tipo_indicador
    };

    $.post("relatorios.php", data,
      function (r) {
        //if(r == 1){
        $("#resultado").html(r);
        //}
      }); //.post
  });
  //FIM *FUNCOES E EVENTOS DA ROTINA DE INCONFORMIDADES E ERROS DE PREENCHIMENTO

  $("#dialog-imprimir1").dialog({
    autoOpen: false,
    modal: true,

    close: function (event, ui) {
      window.location = "?view=buscar";
    },
    buttons: {
      "Visualizar": function () {
        var p = $("#dialog-imprimir1 input[name='prescricao_emergencia']").val();
        var empresa = $("#dialog-imprimir1 input[name='empresa']").val();
        window.open('imprimir_emergencia.php?prescricao=' + p+'&empresa='+empresa, '_blank');
        $(this).dialog("close");

      },
      "Fechar": function () {
        $(this).dialog("close");
      }
    }
  });

  //Busca de alergia a medicamentos
  $("input[name=busca_medicamento_alergia]").autocomplete({
    source: function (request, response) {
      $.getJSON('busca_brasindice.php', {
        term: request.term,
        tipo: 0,
      }, response);
    },
    minLength: 3,
    select: function (event, ui) {
      $('#busca-medicamento-alergia').val('');

      var bexcluir = "<img src='../utils/delete_16x16.png' class='del-medicamento-alergia' title='Excluir' border='0' >";

      $('#medicamentos_alergia_medicamentosa').append("<tr><td colspan='2' inativo='false' class='medicamentos-alergia' catalogo_id ='" + ui.item.catalogo_id + "' cod='" + ui.item.id + "' desc='" + ui.item.value + "'> " + bexcluir + " " + ui.item.value + "</td></tr>");
      $("input[name=busca_medicamento_alergia]").focus();
      atualiza_rem_medicamentos_alergia();

      return false;
    },
    extraParams: {
      tipo_consulta: 8
    }
  });

  $(".outro_medicamento_alergia").hide();
  $("#outros-medicamento-alergia").click(function () {
    if($(this).is(":checked")) {

      $(".outro_medicamento_alergia").show();
    } else {
      $(".outro_medicamento_alergia").hide();
    }

  });

  $("#add-medicamento-alergia").click(function () {
    var medicamento = $("input[name='outro-medicamento-alergia']").val();
    var existe_medicamento = 0; //Valida se o medicamento jÃ¡ estÃ¡ na listagem ou Não de acordo com a sua descriÃ§Ã£o

    if(medicamento == "") {

      $("input[name='outro-medicamento-alergia']").css({
        "border": "3px solid red"
      });

    } else {
      $("input[name='outro-medicamento-alergia']").css({
        "border": ""
      });

      $(".medicamentos-alergia").each(function (i) {

        var desc = $(this).attr("desc");

        if(medicamento == desc) { //Verifica se jÃ¡ existe na listagem
          existe_medicamento = 1;
        }

      });

      if(existe_medicamento != 1) {
        var bexcluir = "<img src='../utils/delete_16x16.png' class='del-medicamento-alergia' title='Excluir' border='0' >";
        $('#medicamentos_alergia_medicamentosa').append("<tr><td colspan='2' class='medicamentos-alergia' inativo='false' cod='0' desc='" + medicamento + "'> " + bexcluir + " " + medicamento + "</td></tr>");
        $("input[name='busca-medicamento-alergia']").val("");
        $("input[name='outro-medicamento-alergia']").val("");
        $("#outros-medicamento-alergia").attr('checked', false);
        $(".outro_medicamento_alergia").hide();
        $("input[name='busca-medicamento-alergia']").focus();
        atualiza_rem_medicamentos_alergia();
      } else {
        alert("Alergia jÃ¡ cadastrada!");
      }

    }
    return false;

  });

  $(".del-medicamento-alergia").unbind("click", remove_medicamentos_alergia).bind("click", remove_medicamentos_alergia);

  function atualiza_rem_medicamentos_alergia() {
    $(".del-medicamento-alergia").unbind("click", remove_medicamentos_alergia).bind("click", remove_medicamentos_alergia);

  }

  function remove_medicamentos_alergia() {

    var numero_tiss = $(this).parent().attr("cod");
    var id = $(this).parent().attr("id");

    $(this).parent().attr('inativo', 'true').hide();

    return false;
  } //remove_medicamentos_alergia

  $('#add-alimento').click(function () {
    var contador = $(".alimento").size();
    contador++;
    var x = $("input[name='alimento']").val();

    var bexcluir = "<img src='../utils/delete_16x16.png' class='del-alimento' title='Excluir' border='0' style='cursor:pointer;'>";
    if(x != '' && x != undefined) {
      $('#alimento').append(
        "<tr><td colspan='4' inativo='false' class='alimento item_alimento'  desc='" + x + "'> " + bexcluir + "  " + "  <b >Alimento " + contador + ":</b><label name='alimento' class='alergia_alimento'> " + x + "<label/> </td> </tr>");
    }
    $("input[name='alimento']").val("");

    atualiza_rem_alimento();
    $("input[name='alimento']").focus();
    return false;

  });

  atualiza_rem_alimento();

  function atualiza_rem_alimento() {
    $(".del-alimento").unbind("click", remove_alimento).bind("click", remove_alimento);
  }

  function remove_alimento() {
    $(this).parent().attr('inativo', 'true').hide();

    return false;
  }

  $("#dialog-imprimir").dialog({
    autoOpen: false,
    modal: true,
    close: function (event, ui) {
      window.location = "?view=buscar";
    },
    buttons: {
      "Visualizar": function () {
        var p = $("#dialog-imprimir input[name='prescricao']").val();
          var empresa = $("#dialog-imprimir input[name='empresa']").val();
          window.open('imprimir.php?prescricao=' + p+'&empresa='+empresa, '_blank');

        $(this).dialog("close");

      },
      "Fechar": function () {
        $(this).dialog("close");
      }
    }
  });

  function salvar_emergencial() {
    var itens = "";
    var total = $(".dados").size();
    $(".dados").each(function (i) {
      //		   		var apr = $(this).attr("apr");
      var catalogo_id = $(this).attr("catalogo_id");
      var cod = $(this).attr("cod");
      var kit_id = $(this).parent().parent().attr("cod");
      var nome = $(this).attr("nome");
      var inicio = $(this).attr("inicio");
      var id_item = $(this).attr("id_item");
      var via = $(this).attr("via");
      var fim = $(this).attr("fim");
      var obs = $(this).attr("obs");
      var tipo = $(this).attr("tipo");
      var freq = $(this).attr("frequencia");
      var qtd = $(this).attr("qtd");

      var cod_ref = $(this).attr('cod_ref');
      var tipo_bras = $(this).attr('tipo_bras');

      desc = $(this).html();
      apraz = $(this).attr("hora");

      var dose = $(this).attr("dose");
      var um = $(this).attr("um");
      var linha = tipo + "#" + cod + "#" + nome + "#" + via + "#" + freq + "#" + apraz + "#" + inicio + "#" + fim + "#" + obs + "#" + desc + "#" + dose + "#" + um + '#' + id_item + '#' + cod_ref + '#' + tipo_bras + '#' + catalogo_id + '#' + kit_id + "#" + qtd;

      if(i < total - 1)
        linha += "$";
      itens += linha;
    });
    if(total > 0) {
      var tipo_presc = $("#prescricao").attr("tipo_presc");
      var paciente = $("#prescricao").attr("paciente");
      var inicio = $("#prescricao").attr("inicio");
      var fim = $("#prescricao").attr("fim");
      if(tipo_presc == null && paciente == null) {

        tipo_presc = $("#presc_emergencia").attr("tipo_presc");
        paciente = $("#presc_emergencia").attr("paciente");
        inicio = $("#presc_emergencia").attr("inicio");
        fim = $("#presc_emergencia").attr("fim");

      }


      $.post("query.php", {
        query: "finalizar-prescricao-emergencial",
        paciente: paciente,
        tipo_presc: tipo_presc,
        inicio: inicio,
        fim: fim,
        dados: itens


      }, function (r) {
          console.log(r > 0);
        if(r > 0) {


          exitPage.needToConfirm = false;


          //$("#dialog-imprimir input[name='prescricao']").val(r);

          $("#dialog-salvo").dialog("open");
          $("#dialog-imprimir1 input[name='prescricao_emergencia']").val(r);
            $("#dialog-imprimir1 input[name='empresa']").val($("#presc_emergencia").attr("empresa-id"));
          $("#dialog-salvo1").dialog("open");
        }
        else {
          alert("ERRO: Tente mais tarde! Cod"+r);
        }
      });
    } else {
      alert("ERRO: A prescrição está vazia.");
    }
  }

  ////jeferson2012-10-22
  function verificar_equipamento() {
    total = $(".dados").size();
    itens = '';
    linha = '';
    //Dados da tabela que carrega os itens prescritos
    $(".dados").each(function (i) {

      var cod = $(this).attr("cod");
      var tipo = $(this).attr("tipo");
      var nome = $(this).attr("nome");

      if((cod == 17 && tipo == 2) || (cod == 18 && tipo == 2) || (cod == 21 && tipo == 2) || (cod == 24 && tipo == 2) || (cod == 5988 && tipo == 13) || tipo == 9) {
        var linha = tipo + "#" + cod + "#" + nome;

        if(i < (total - 1)) {
          linha += "$";

          //total++;
        }

        itens += linha;

      }
    }); //dados
    if(itens == '')
      itens = -1;

    var id_cap = $("#prescricao").attr("id_cap");

    $.post("query.php", {
        query: "equipamentos-verificar",
        id_cap: id_cap,
        dados: itens
      },
      function (r) {

        if(r != "") { //
          $("#equip_item").html(r);
          $("#equip_item").dialog('open');
        } else {
          salvar();
        }
      }); //.post

  } //verificar_equipamento

  $("#equip_item").dialog({
    autoOpen: false,
    modal: true,
    buttons: {
      "Voltar": function () {
        $(this).dialog("close");
      },
      "Continuar": function () {
        salvar();
        $(this).dialog("close");
      }
    }
  });

  $("#equip_item").dialog('close');

  function salvar() {
    var itens = "";
    var total = $(".dados").size();
    var gtm = new Array();
    $(".dados").each(function (i) {
      //   		var apr = $(this).attr("apr");
      var brasindice_id = $(this).attr("brasindice_id");
      var cod = $(this).attr("cod");
      var tipo = $(this).attr("tipo");
      if(tipo == 9) {
        cod = 0;
      }
      var nome = $(this).attr("nome");
      var inicio = $(this).attr("inicio");
      var id_item = $(this).attr("id_item");
      var via = $(this).attr("via");
      var apraz = '00:00';
      var fim = $(this).attr("fim");
      var obs = $(this).attr("obs");
      var freq = $(this).attr("frequencia");
      var desc = $(this).children('td').eq(0).html();
      var cod_ref = $(this).attr('cod_ref');
      var tipo_bras = $(this).attr('tipo_bras');
      //var desc = $(this).html();
      if(desc == null) {
        desc = $(this).html();
        apraz = $(this).attr("hora");
      }

      var dose = $(this).attr("dose");
      var um = $(this).attr("um");
      var linha = tipo + "#" + cod + "#" + nome + "#" + via + "#" + freq + "#" + apraz + "#" + inicio + "#" + fim + "#" + obs + "#" + desc + "#" + dose + "#" + um + '#' + id_item + '#' + cod_ref + '#' + tipo_bras + '#' + brasindice_id;
      if(tipo == '16') {
        gtm.push({
          item_id: $(this).attr('cod'),
          brasindice_id: $(this).attr('brasindice_id'),
          data_troca: $(this).attr('data_troca'),
          tipo_gtm: $(this).attr('tipo_gtm'),
          furo: $(this).attr('furo'),
          cm: $(this).attr('cm'),
          fr: $(this).attr('fr'),
          marca_gtm: $(this).attr('marca_gtm'),
          dano_gtm: $(this).attr('dano_gtm'),
          origem_dano_gtm: $(this).attr('origem_dano_gtm'),
          descricao_origem_dano_gtm: $(this).attr('descricao_origem_dano_gtm')
        });
      }

      if(i < total - 1)
        linha += "$";
      itens += linha;
    });
    if(total > 0) {
      var tipo_presc = $("#prescricao").attr("tipo_presc");
      var paciente = $("#prescricao").attr("paciente");
      var inicio = $("#prescricao").attr("inicio");
      var fim = $("#prescricao").attr("fim");
      var presc = $('#prescricao').attr("presc");
      var edt = $('#prescricao').attr("edt");
      var id_cap = $("#prescricao").attr("id_cap");
      var criador = $("#prescricao").attr("criador");
      var idexame = $("#prescricao").attr("idexame");
      var idIntercorrencia = $("#prescricao").attr("idIntercorrencia");
      var empresaId = $("#prescricao").attr("empresa-id");
      var tipoAditivo = $("#tipo-aditiva").val();
      var motivoAjusteAditivo = $("#motivo-ajuste-aditivo").val();


      if(tipo_presc == null && paciente == null) {
        tipo_presc = $("#presc_emergencia").attr("tipo_presc");
        paciente = $("#presc_emergencia").attr("paciente");
        inicio = $("#presc_emergencia").attr("inicio");
        fim = $("#presc_emergencia").attr("fim");
        criador = $("#prescricao").attr("criador");
      }

      var num_sugestao = $("input[name='num_sugestao']").val();

      $.post("query.php", {
        query: "finalizar-prescricao",
        paciente: paciente,
        edt: edt,
        presc: presc,
        tipo_presc: tipo_presc,
        inicio: inicio,
        fim: fim,
        id_cap: id_cap,
        dados: itens,
        criador: criador,
        num_sugestao: num_sugestao,
        gtm: gtm,
        idexame:idexame,
        idIntercorrencia: idIntercorrencia,
        tipoAditivo: tipoAditivo,
        motivoAjusteAditivo:  motivoAjusteAditivo
      }, function (r) {
        if(r > "-1") {
          exitPage.needToConfirm = false;
          $("#dialog-imprimir input[name='prescricao']").val(r);
            $("#dialog-imprimir input[name='empresa']").val(empresaId);

          $("#dialog-salvo").dialog("open");
        }
        else {
          alert("ERRO: Tente mais tarde! " + r);
        }
      });
    } else {
      alert("ERRO: A prescrição está vazia.");
    }
  }

  function atualiza_qtd(tipo, op) {
    exitPage.needToConfirm = true;
    var div = "div[tipo='" + tipo + "']";
    var qtd = $(div).attr("qtd");

    if(op == "+")
      $(div).attr("qtd", parseInt(qtd) + 1);
    else
      $(div).attr("qtd", parseInt(qtd) - 1);
  }

  function validar_campos(div) {
    var ok = true;
    $("#" + div + " .OBG").each(function (i) {
      if(this.value == "") {
        ok = false;
        this.style.border = "3px solid red";
      } else {
        this.style.border = "";
      }
    });
    $("#" + div + " .COMBO_OBG option:selected").each(function (i) {
      if(this.value == "-1") {
        ok = false;
        $(this).parent().css({
          "border": "3px solid red"
        });
      } else {
        $(this).parent().css({
          "border": ""
        });
      }
    });
    if(!ok) {
      $("#" + div + " .aviso").text("Os campos em vermelho são obrigatórios!");
      $("#" + div + " .div-aviso").show();
    } else {
      $("#" + div + " .aviso").text("");
      $("#" + div + " .div-aviso").hide();
    }

    $("#" + div + " .OBG_RADIO").each(function (i) {
      classe = $(this).attr("class").split(" ");
      classe = classe[0];
      classe_aux = "";
      aux = 0;

      if(classe != classe_aux) {
        classe_aux = classe;
        aux = 0;
        if($("." + classe).is(':checked')) {
          aux = 1;
        }

        if(aux == 0) {
          $("." + classe).parent().parent().css("border", "3px solid red");

        } else {
          $("." + classe).parent().parent().css("border", "");
        }
      } else {
        if($("." + classe).is(':checked')) {
          aux = 1;
        }

        if(aux == 0) {
          $("." + classe).parent().parent().css("border", "3px solid red");
        } else {
          $("." + classe).parent().parent().css("border", "");
        }
      }
    });

    return ok;
  }

  function validar_tipos() {
    var ok = true;
    $("#aviso").html("<center><b>Pacientes Sulamérica devem utilizar as denominações:</b></center><li>DIÁRIA COM BAIXA COMPLEXIDADE</li><li>DIÁRIA COM MÉDIA COMPLEXIDADE</li><li>DIÁRIA COM ALTA COMPLEXIDADE</li><li>DIÁRIA COM VENTILAÇÃO MECÂNICA</li><center><b>Nenhum dos itens abaixo foi prescrito: </b></center><ul>");
    $("div .tab-presc").each(function (i) {
      if($(this).attr("qtd") == 0) {
        ok = false;
        $("#aviso").append("<li>" + $(this).attr("title") + "</li>");
      }
    });
    $("#aviso").append("</ul><br/><b>Deseja realmente continuar?</b>");
    return ok;
  }

  function reload_css_table() {

    //$("#tabela-itens tr:odd").css("background-color", "#ebf3ff");
    $("#tabela-itens tr").hover(
      function () {
        $(this).css("background-color", "#87CEEB");
        $(this).css("color", "#000000");
      },
      function () {
        $(this).css("background-color", "");
        $(this).css("color", "");
        $("#tabela-itens tr:odd").css("background-color", "");
        $("#tabela-itens tr:odd").css("color", "");
      });

    // console.log("reload_css_table","" + resultado);
  }

  $("select[name='paciente']").change(function () {
    var p = $("select[name='paciente'] option:selected").val();
    var status = $("select[name='paciente'] option:selected").attr('status');
    var a = $('input:radio[name=tipo]:checked').val();
    $("button[name=antigas]").attr('paciente_btn_nova', p);
    var i = 'null';
    var f = 'null';
    //$.post("query.php",{query: "prescricoes-antigas", p: p,tipo:a},function(r){
    $.post("query.php", {
      query: "buscar-prescricoes",
      paciente: p,
      inicio: i,
      fim: f,
      op: 1,
      status: status
    }, function (r) {

      if(p != -1 && status == 4) {
        $("#iniciar-prescricao").show();
        $("#novoBtn").show();
        $('.usar_nova').show();
        $("#antigas").show();
        $("#antigas").html(r);
      } else {
        $("#iniciar-prescricao").hide();
        $("#novoBtn").hide();
        $("#antigas").hide();
      }
      $("#div-resultado-busca").html(r);
    });
  });

  $("button[name=antigas]").click(function () {
    $("#fim").removeAttr("style");
    $('.usar_nova').show();

    $("#paciente").val($(this).attr('paciente_btn_nova'));
    $("input[name='antigas']").val(-1);
    $("#NomePacientePresc").html($("#selPaciente").find('option').filter(':selected').text());
    $("#pre-presc").dialog("open");
  });



  $("#buscar_medicamento").click(function () {
    if($("input[name='todos']").is(":checked")) {
      var medicamento = '';
    } else {
      var medicamento = $("#busca_medicamento").val();
    }
    //alert(medicamento);
    $.post("query.php", {
      query: "buscar_medicamento",
      medicamento: medicamento
    }, function (r) {
      $("#result").html(r);
      $("#result").append();
    });
    return false;
  });

  $("#salvar_associacao").live('click', function () {
    var total = $(".principio_ativo").size();
    var itens = "";

    $(".principio_ativo").each(function (i) {
      if(($(this).val() == '') || ($(this).val() == 'NULL') || ($(this).val() == 'undefined')) {
        //alert("nada aqui");
      } else {
        var id = $(this).parent().parent().children("td").eq("0").attr("id");
        var principio_ativo = $(this).val();
        //alert("principio ativo: " + principio_ativo);
        var antibiotico = $("input[name='antibiotico" + i + "']:checked").val();
        //alert("antibiotico: " + antibiotico);
        var controlado = $("input[name='controlado" + i + "']:checked").val();
        // alert("controlado: " + controlado);
        var generico = $("input[name='generico" + i + "']:checked").val();
        // alert("generico: " + generico);
        var ativado = $("input[name='ativo" + i + "']:checked").val();
        var linha = id + "#" + principio_ativo + "#" + antibiotico + "#" + controlado + "#" + generico + "#" + ativado;
        if(i < total - 1) {
          linha += "$";
        }
        itens += linha;
      }
    });
    var dados = "query=salvar_associacao" + "&itens=" + itens;
    //alert(dados);
    $.ajax({
      type: "POST",
      url: "query.php",
      data: dados,
      success: function (msg) {

        if(msg == 1) {
          alert("Associacao salva com sucesso.");
          return false;
        } else {
          alert(msg);
          return false;
        }
      }

    });
  });

  ///04-06-2013 jeferson busaca da aba de Material 
  $("input#busca-item").autocomplete({
    source: function (request, response) {
      $.getJSON('busca_brasindice.php', {
        term: request.term,
        tipo: 1
      }, response);
    },
    minLength: 3,
    select: function (event, ui) {
      $("input[name=busca-item-material]").val('');
      $('#nome-mat').val(ui.item.value);
      $('#nome-mat').attr('readonly', 'readonly');
      $('#cod').val(ui.item.id);
      $('#brasindice_id').val(ui.item.catalogo_id);
      $('#busca').val('');
      $('#busca-item').val('');
      $("#formulario_material input[name=qtd]").focus();
      return false;
    }
  });

  $("input#busca-item-gtm").autocomplete({
    source: function (request, response) {
      $.getJSON('busca_brasindice.php', {
        term: request.term,
        tipo: 1
      }, response);
    },
    minLength: 3,
    select: function (event, ui) {
      $("input[name=busca-item-material]").val('');
      $('#nome-mat-gtm').val(ui.item.value);
      $('#nome-mat-gtm').attr('readonly', 'readonly');
      $('#cod-gtm').val(ui.item.id);
      $('#brasindice-id-gtm').val(ui.item.catalogo_id);
      $('#busca-gtm').val('');
      $('#busca-item-gtm').val('');
      return false;
    }
  });


  $("#formulario select[name='vadm-formulario']").change(function () {
    if($("#formulario").attr("via") != $(this).val()) {
      if(!confirm("Via de adm diferente do padrÃ£o, deseja continuar?")) {
        var via = $("#formulario").attr("via");
        $("#formulario select[name='vadm-formulario'] option[value='" + via + "']").attr('selected', 'selected');
      }
    }
  });

  $(".show-form").click(function () {
    $("#add-item").attr("tipo", $(this).attr("tipo"));
    $("#formulario").attr('busca', 'die');
    $("#add-item").attr("label", $(this).attr("label"));
    $("#formulario").attr("tipo", $(this).attr("tipo"));
    $("#ui-dialog-title-formulario").html($(this).attr("label"));
    $("#formulario").attr("classe", $(this).attr("classe"));
    var via = $(this).attr("via");
    $("#formulario").attr("via", via);
    $("#formulario select[name='vadm-formulario'] option[value='" + via + "']").attr('selected', 'selected');
    var carater = $("#prescricao").attr('tipo_presc');
    var plano = $("#prescricao").attr('plano');
    var medicamentoAceito = $("#prescricao").attr('medicamentoaceito');
    var text ='';
    if($(this).attr("tipo") != 14){
      if( medicamentoAceito == 'G'){
        text += " <span class='text-red'><b>Paciente do plano "+ plano +" deve prescrever medicamentos GENÉRICOS.</b></span><br>";
        $("#formulario").attr("somenteGenerico", 'S');
      }
      text += "<br><b>Buscar por:</b> <select name='tipo_busca' id='tipo_busca' ><option value='N'>Nome</option><option value='P'>Princípio ativo</option></select>";

      $("#textFormulario").html(text);
    }
    if(carater == 3) {
      $("input[name='obs']").addClass("OBG");
    }
    $("#formulario").dialog("open");
  });

  $("#formulario").dialog({
    autoOpen: false,
    modal: true,
    width: 800,
    open: function (event, ui) {
      $("#formulario input[name='busca']").val('');
      $('#nome').val('');
      $('#nome').attr('readonly', 'readonly');
      //	    $('#apr').val('');
      //	    $('#apr').attr('readonly','readonly');
      $('#cod').val('');
      $("#formulario input[name='aprazamento']").val('');
      $("#formulario input[name='dose']").val('');
      $("#formulario input[name='obs']").val('');
      $("#formulario select[name='frequencia'] option").eq(0).attr('selected', 'selected');
      $("#formulario select[name='um'] option").eq(0).attr('selected', 'selected');
    },
    buttons: {
      "Fechar": function () {
        $(this).dialog("close");
      }
    }
  });

  /* FORMULARIO DE MATERIAL DA PRESCRIÃ‡ÃƒO MÃ‰DICA */

  $(".show-form-material").click(function () {
    $("#formulario_material").dialog("open");
  });

  $("#formulario_material").dialog({
    autoOpen: false,
    modal: true,
    width: 800,
    open: function (event, ui) {
    },
    buttons: {
      "Fechar": function () {
        $(this).dialog("close");
      }
    }
  });



  /************************************************/

  $("#aviso").dialog({
    autoOpen: false,
    modal: true,
    buttons: {
      "Voltar": function () {
        $(this).dialog("close");
      },
      "Continuar": function () {

        var tipo_presc = $("#prescricao").attr("tipo_presc");

        if(tipo_presc == 4 || tipo_presc == 7) {
          verificar_equipamento();

        } else {
          salvar();

        }

        $(this).dialog("close");
      }
    }
  });

  /*-----------------------jeferson 30/07/12 presc avulasa-------------------------------------*/

  $("input[name='busca1']").autocomplete({
    source: function (request, response) {
      $.getJSON('busca_brasindice.php', {
        term: request.term,
        tipo: $("#formulario2").attr("tipo")
      }, response);
    },
    minLength: 3,
    select: function (event, ui) {
      $('#busca-item').val('');
      $('#nome').val(ui.item.value);
      $('#cod').val(ui.item.id);
      $('#brasindice_id').val(ui.item.catalogo_id);
      $('#cod_ref').val(ui.item.cod_ref);
      $('#qtd').focus();
      $('#busca-item').val('');
      return false;
    },
    extraParams: {
      tipo: 2
    }
  });




  $(".add-kit").click(function () {

    var item = $(this).attr("item");
    var tipo = $(this).attr("tipo");
    $("#kits-" + item).show();
    $("#formulario2").attr("item", item);
    $("#formulario2").attr("tipo", tipo);
    $("#b" + tipo).attr('checked', 'checked');
    $("#b" + tipo).button('refresh');
    $("#formulario2").dialog("open");
  });

  $(".look-kit").toggle(
    function () {
      var item = $(this).attr("item");
      $("#kits-" + item).show();

    },
    function () {
      var item = $(this).attr("item");
      $("#kits-" + item).hide();

    });

  $("#formulario2").dialog({
    autoOpen: false,
    modal: true,
    width: 800,
    open: function (event, ui) {
      $("input[name='busca']").val('');
      $('#nome').val('');
      $('#nome').attr('readonly', 'readonly');
      $('#apr').val('');
      $('#apr').attr('readonly', 'readonly');
      $('#cod').val('');
      $('#cod_ref').val('');
      $('#qtd').val('');


      $("input[name='busca']").focus();
    },
    buttons: {
      "Fechar": function () {
        var item = $("#formulario2").attr("item");
        if(item != "presc_emergencia") {
          $("#kits-" + item).hide();

        }
        $(this).dialog("close");
      }
    }
  });

  /*function atualiza_rem_item(){
   $(".rem-item-solicitacao").unbind("click",remove_item)
   .bind("click",remove_item);
   }*/

  $(".botao_relatorios").live('click', function () {
    $("#dialog-relatorios").attr("cod", $(this).attr("cod"));
    $("#dialog-relatorios").dialog("open");
  });

  $("#add-item1").click(function () {
    if(validar_campos("formulario2")) {
      var brasindice_id = $("#brasindice_id").val();
      var cod = $("#cod").val();
      var nome = $("#formulario2 input[name='nome']").val();
      var item = $("#formulario2").attr("item");
      // var qtd = $("#formulario2 input[name='qtd']").val();
      var tipo = $("#formulario2").attr("tipo");
      var cod_ref = $("#formulario2 input[name='cod_ref']").val();
      var qtd = $("#formulario2 input[name='qtd_kit']").val();
      var inicio = $("#formulario2 input[name='inicio']").val();
      var fim = $("#formulario2 input[name='fim']").val();
      var um = $("#formulario2 select[name='um'] option:selected").val();
      var num_kit = $("#formulario2 select[name='um'] option:selected").html();
      var obs = $("#formulario2 input[name='obs']").val();
      var frequencia = $("#formulario2  select[name='frequencia'] option:selected").val();
      var nfrequencia = $("#formulario2  select[name='frequencia'] option:selected").html();
      var hora = $("#formulario2 input[name='hora']").val();

      var dados = "nome='" + nome + "' brasindice_id='" + brasindice_id + "' tipo='" + tipo + "' cod='" + cod + "' dose='" + qtd + "' frequencia='" + frequencia + "'um='" + um + "'inicio='" + inicio + "'fim='" + fim + "'obs='" + obs + "'hora='" + hora + "'";

      var linha = nome + ". Dosagem: " + qtd + " " + num_kit + ". Frequncia: " + nfrequencia + ". Per&iacute;odo: " + inicio + " at&eacute; " + fim + ". OBS: " + obs + "  Hora:" + hora;
      var botoes = "<img align='right' class='rem-item-solicitacao' brasindice_id='" + brasindice_id + "' item='" + item + "' cod='" + cod + "' src='../utils/delete_16x16.png' title='Remover' border='0'>";

      /**/

      $("#cod").val("-1");
      $("#formulario2 input[name='nome']").val('');
      $("#formulario2 input[name='qtd']").val('');
      $("#formulario2 input[name='qtd_kit']").val('');
      $("#formulario2 input[name='obs']").val('');
      $("#formulario2 input[name='hora']").val('');
      $('#busca-item').val('');

      $("#formulario2 select[name='frequencia'] option").eq(0).attr('selected', 'selected');
      $("#formulario2 select[name='um'] option").eq(0).attr('selected', 'selected');
      // $("#itens-"+tipo).after("<tr bgcolor='#ebf3ff' class='dados' "+dados+" ><td><b>Oxigenoterapia: </b>"+linha+"</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
      $("#" + tipo + "-" + item).append("<tr bgcolor='#ffffff' cod=" + cod + " inicio=" + inicio + "><td >" + linha + "</td><td>" + botoes + "</td>/tr>");
      $.post("query.php", {
        query: "consultar-itens-kit",
        cod: cod,
        inicio: inicio,
        fim: fim,
        cod_ref: cod_ref,
        qtd: qtd,
        nfrequencia: nfrequencia,
        frequencia: frequencia,
        um: um,
        hora: hora,
        obs: obs,
        item: item
      }, function (r) {
        $("#" + tipo + "-" + item).append(r);
      });
      reload_css_table();

      atualiza_qtd(item, '+');
      atualiza_rem_item();




      exitPage.needToConfirm = true;
    }
    return false;
  });

  function atualiza_rem_item() {
    $(".rem-item-solicitacao").unbind("click", remove_item1)
      .bind("click", remove_item1);
  }

  function remove_item1() {

    if(confirm("Deseja realmente remover?")) {
      $("#kits-presc_emergencia").find("tr[cod=" + $(this).parent().parent().attr('cod') + "]").each(function () {

        $(this).remove();

      });
      atualiza_qtd($(this).attr('item'), '-');
      //*$(this).parent().parent().remove();
      //atualiza_qtd($(this).attr('item'),'-');
    }
    return false;
  }

  function remove_item() {
    if(confirm("Deseja realmente remover?")) {
      $(this).parent().parent().remove();
      atualiza_qtd($(this).attr('item'), '-');
    }
    return false;
  }

  /*-----------------------------------fim jeferson presc 30/07/12---------------------*/

  $("#add-item").click(function () {
    if(validar_campos("formulario")) {
      var brasindice_id = $("#brasindice_id").val();
      var cod = $("#cod").val();
      var nome = $("#formulario input[name='nome']").val();
      //      var apr = $("#formulario input[name='apr']").val(); 
      var inicio = $("#formulario input[name='inicio']").val();
      var freq = $("#formulario  select[name='frequencia'] option:selected").val();
      var nfreq = $("#formulario  select[name='frequencia'] option:selected").html();
      var um = $("#formulario  select[name='um'] option:selected").val();
      var label_um = $("#formulario  select[name='um'] option:selected").html();
      var fim = $("#formulario input[name='fim']").val();
      var obs = $("#formulario input[name='obs']").val();
      var dose = $("#formulario input[name='dose']").val();
      var viadm = $("#formulario select[name='vadm-formulario'] option:selected").val();
      var nvia = $("#formulario select[name='vadm-formulario'] option:selected").html();
      var label = $(this).attr("label");
      var linha = "<b>" + label + ":</b> " + nome + ". " + dose + " " + label_um + " " + nfreq + " " + nvia + ". Per&iacute;odo: " + inicio + " at&eacute; " + fim + " OBS: " + obs;

      var tipo = $(this).attr("tipo");

      var dados = " tipo='" + tipo + "' nome='" + nome + "' frequencia='" + freq + "' brasindice_id='" + brasindice_id + "' inicio='" + inicio + "' fim='" + fim + "' obs='" + obs + "' cod='" + cod + "' um='" + um + "' dose='" + dose + "' via='" + viadm + "'";
      var inicio1 = $("#formulario input[name='inicio']").attr('i');
      var fim1 = $("#formulario input[name='fim']").attr('f');

      atualiza_qtd(tipo, "+");
      $("#itens-" + tipo).after("<tr bgcolor='#ebf3ff' class='dados' " + dados + " ><td>" + linha + "</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
      reload_css_table();
      $("#cod").val("-1");
      $("#formulario input[name='nome']").val("");
      $("#formulario input[name='nome']").attr("readonly", "readonly");
      //      $("#formulario input[name='apr']").val(""); $("#formulario input[name='apr']").attr("readonly","readonly");
      $("#formulario input[name='aprazamento']").val("");
      $("#formulario input[name='obs']").val("");
      $("#formulario select[name='frequencia'] option").eq(0).attr('selected', 'selected');
      $("#formulario select[name='um'] option").eq(0).attr('selected', 'selected');
      $("input[name='busca']").val("");
      $("input[name='busca']").focus();
      $("#formulario input[name='dose']").val('');
      $("#formulario input[name='inicio']").val("" + inicio1 + "");
      $("#formulario input[name='fim']").val("" + fim1 + "");
    }
    return false;
  });

  ///////jeferson 03-06-2013 add item material
  $("#add-item-material").click(function () {
    if(validar_campos("formulario_material")) {
      var brasindice_id = $("#brasindice_id").val();
      var cod = $("#cod").val();
      var nome = $("#formulario_material input[name='nome']").val();
      //	      var apr = $("#formulario input[name='apr']").val(); 
      var inicio = $("#formulario_material input[name='inicio']").val();
      var freq = $("#formulario_material select[name='frequencia'] option:selected").val();
      var nfreq = $("#formulario_material select[name='frequencia'] option:selected").html();
      var um = $("#formulario_material select[name='um'] option:selected").val();
      var label_um = $("#formulario_material select[name='um'] option:selected").html();
      var fim = $("#formulario_material input[name='fim']").val();
      var obs = $("#formulario_material input[name='obs']").val();
      var dose = $("#formulario_material input[name='dose']").val();
      var viadm = $("#formulario_material select[name='vadm-formulario'] option:selected").val();
      var nvia = $("#formulario_material select[name='vadm-formulario'] option:selected").html();
      var label = $(this).attr("label");
      var linha = "<b>Material:</b> " + nome + ".  " + nfreq + " . Per&iacute;odo: " + inicio + " at&eacute; " + fim + " OBS: " + obs;

      var tipo = 15; //tipo Ã© o valor da aba nesse caso Ã© 16 pq Ã© a aba material

      var dados = " tipo='" + tipo + "' nome='" + nome + "' brasindice_id='" + brasindice_id + "' frequencia='" + freq + "' inicio='" + inicio + "' fim='" + fim + "' obs='" + obs + "' cod='" + cod + "' um='" + um + "' dose='" + dose + "' via='" + viadm + "'";
      var inicio1 = $("#formulario_material input[name='inicio']").attr('i');
      var fim1 = $("#formulario_material input[name='fim']").attr('f');

      atualiza_qtd(tipo, "+");
      $("#itens-15").after("<tr bgcolor='#ebf3ff' class='dados' " + dados + " ><td>" + linha + "</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
      reload_css_table();
      $("#cod").val("-1");
      $("#formulario_material input[name='nome']").val("");
      $("#formulario_material input[name='nome']").attr("readonly", "readonly");
      //	      $("#formulario_material input[name='apr']").val(""); $("#formulario_material input[name='apr']").attr("readonly","readonly");
      $("#formulario_material input[name='aprazamento']").val("");
      $("#formulario_material input[name='obs']").val("");
      $("#formulario_material select[name='frequencia'] option").eq(0).attr('selected', 'selected');
      $("#formulario_material select[name='um'] option").eq(0).attr('selected', 'selected');
      $("input[name='busca']").val("");
      $("input[name='busca']").focus();
      $("#formulario_material input[name='dose']").val('');
      $("#formulario_material input[name='inicio']").val("" + inicio1 + "");
      $("#formulario_material input[name='fim']").val("" + fim1 + "");
    }
    return false;
  });
  /////fim add-item-material

  $("#rem-item").click(function () {
    var qtd = $(".dados td .select-item:checked").size();
    var i = 'o item';
    if(qtd > 1)
      i = 'os ' + qtd + ' itens';
    if(qtd == 0)
      return;
    if(confirm("Deseja realmente deletar " + i + " ?")) {
      $(".dados td .select-item").each(function (i) {
        if($(this).attr("checked")) {
          atualiza_qtd($(this).parent().parent().attr("tipo"), "-");
          $(this).parent().parent().remove();
        }
      });
      $("#tabela-itens tr:odd").css("background-color", "#ebf3ff");
      $("#tabela-itens tr").hover(
        function () {
          $(this).css("background-color", "#3d80df");
          $(this).css("color", "#ffffff");
        },
        function () {
          $(this).css("background-color", "");
          $(this).css("color", "");
          $("#tabela-itens tr:odd").css("background-color", "#ebf3ff");
          $("#tabela-itens tr:odd").css("color", "");
        }
      );
    }
    return false;
  });

  $("#susp-item").click(function () {
    var qtd = $(".suspender td .select-item-susp:checked").size();
    var i = 'o item';
    if(qtd > 1)
      i = 'os ' + qtd + ' itens';
    if(qtd == 0)
      return;
    if(confirm("Deseja realmente suspender " + i + " ?")) {
      $(".suspender td .select-item-susp").each(function (i) {
        if($(this).attr("checked")) {
          this.checked = false;
          $(this).removeClass("select-item-susp");
          $(this).addClass("select-item");
          $(this).parent().parent().children("td").eq(0).prepend("<b><i>SUSPENDER: </i></b>");
          var item = $(this).parent().parent().attr('id_item');

          $(this).parent().parent().removeClass("suspender");
          $(this).parent().parent().addClass("dados");
          $("table").find("tr:#id-" + item).each(function () {
            $(this).remove();
          });
          $("#tabela-itens").append($(this).parent().parent().remove());



          atualiza_qtd('s', '+');
        }
      });
    }
    return false;
  });

  $("#salvar").click(function () {
    var p = $("#corpo-prescricao").html();
    var paciente = $("#prescricao").attr("paciente");
    var nome = $("#prescricao").attr("pnome");

    $.post("query.php", {
      query: "salvar-prescricao",
      p: p,
      paciente: paciente,
      nome: nome
    }, function (r) {
      alert(r);
    });
    return false;
  });

  $("#finalizar").click(function () {
    if(!validar_campos('div-tipo-aditivo')){
      return false;
    }
    if(!validar_tipos()) {

      $("#aviso").dialog("open");
      return false;
    } else {
      if(tipo_presc == 4 || tipo_presc == 7) {
        verificar_equipamento();

      } else {
        salvar();

      }
    }

    return false;
  });

  $("#finalizar_presc_emergencia").click(function () {
    if(!validar_tipos()) {
      $("#aviso").dialog("open");
      return false;
    }

    salvar_emergencial();
    return false;
  });

  $("input[name='nome']").attr("readonly", "readonly");
  $("input[name='apr']").attr("readonly", "readonly");

  $(".teste").datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 1
  });

  $(".data").datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 1
  });

  $(".hora").timeEntry({
    show24Hours: true,
    defaultTime: '08:00'
  });

  $("#checktodos").click(function () {
    var checked_status = this.checked;
    $(".select-item").each(function ()
    {
      this.checked = checked_status;
    });
  });

  $("#checktodos-susp").click(function () {
    var checked_status = this.checked;
    $(".select-item-susp").each(function ()
    {
      this.checked = checked_status;
    });
  });

  var $tabs = $("#prescricao").tabs();

  $(".next-tab,.prev-tab").click(function () {
    $tabs.tabs('select', parseInt($(this).attr('rel')));
    return false;
  });

  $("#add-formula").click(function () {
    if(validar_campos("div-formulas")) {
      var formula = $("#formula").val();
      var tipo = $(this).attr("tipo");
      var inicio = $("#div-formulas input[name='inicio']").val();
      var fim = $("#div-formulas input[name='fim']").val();
      var obs = $("#div-formulas input[name='obs']").val();
      var via = $("#div-formulas select[name='vadm-formula'] option:selected").val();
      var nvia = $("#div-formulas select[name='vadm-formula'] option:selected").html();
      var freq = $("#div-formulas select[name='frequencia'] option:selected").val();
      var nfreq = $("#div-formulas select[name='frequencia'] option:selected").html();
      atualiza_qtd(tipo, "+");
      var label = $(this).attr("label");
      var linha = "<b>" + label + ":</b> " + formula + " " + nfreq + " " + nvia + ". Per&iacute;odo: " + inicio + " at&eacute; " + fim + " OBS: " + obs;
      var dados = " nome='" + formula + "' tipo='" + tipo + "' frequencia='" + freq + "' via='" + via + "' inicio='" + inicio + "' fim='" + fim + "' obs='" + obs + "'";
      $("#itens-" + tipo).after("<tr bgcolor='#ebf3ff' class='dados' " + dados + " ><td>" + linha + "</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
      reload_css_table();
      $("#formula").val("");
      $("#div-formulas select option").eq(0).attr('selected', 'selected');;
      $("#div-formulas input[name='obs']").val("");
    }
  });

  $("#div-repouso select[name='frequencia']").change(function () {
    var carater = $("#prescricao").attr('tipo_presc');
    var frequencia = $("#div-repouso select[name='frequencia'] option:selected").val();
    if(carater == 3 && frequencia > 0) {
      $("#div-repouso input[name='obs']").addClass("OBG");
    } else {
      $("#div-repouso input[name='obs']").removeClass("OBG");
    }
  });
  $("#div-dieta select[name='frequencia']").change(function () {
    var carater = $("#prescricao").attr('tipo_presc');
    var frequencia = $("#div-dieta select[name='frequencia'] option:selected").val();
    if(carater == 3 && frequencia > 0) {
      $("#div-dieta input[name='obs']").addClass("OBG");
    } else {
      $("#div-dieta input[name='obs']").removeClass("OBG");
    }
  });
  $("#div-cuidados select[name='cuidados']").change(function () {
    var carater = $("#prescricao").attr('tipo_presc');
    var cuidados = $("#div-cuidados select[name='cuidados'] option:selected").val();
    if((carater == 3 || carater == 4) && cuidados > 0) {
      $("#div-cuidados input[name='obs']").addClass("OBG");
    } else {
      $("#div-cuidados input[name='obs']").removeClass("OBG");
    }
  });
  $("#div-soros select[name='um']").change(function () {
    var carater = $("#prescricao").attr('tipo_presc');
    var unidade = $("#div-cuidados select[name='um'] option:selected").val();
    if(carater == 3 && unidade > 0) {
      $("#div-soros input[name='obs']").addClass("OBG");
    } else {
      $("#div-soros input[name='obs']").removeClass("OBG");
    }
  });
  $("#div-nebulizacao select[name='frequencia']").change(function () {
    var carater = $("#prescricao").attr('tipo_presc');
    var frequencia = $("#div-nebulizacao select[name='frequencia'] option:selected").val();
    if(carater == 3 && frequencia > 0) {
      $("#div-nebulizacao input[name='obs']").addClass("OBG");
    } else {
      $("#div-nebulizacao input[name='obs']").removeClass("OBG");
    }
  });


  $("#add-repouso").click(function () {
    if(validar_campos("div-repouso")) {
      var nome = $("#div-repouso select[name='repouso'] option:selected").html();
      var freq = $("#div-repouso select[name='frequencia'] option:selected").val();
      var nfreq = $("#div-repouso select[name='frequencia'] option:selected").html();
      var cod = $("#div-repouso select[name='repouso'] option:selected").val();
      var brasindice_id = $("#div-repouso select[name='repouso'] option:selected").val();
      var obs = $("#div-repouso input[name='obs']").val();
      var inicio = $("#div-repouso input[name='inicio']").val();
      var fim = $("#div-repouso input[name='fim']").val();
      var label = $(this).attr("label");
      var linha = "<b>" + label + ":</b> " + nome + " " + nfreq + ". Per&iacute;odo: " + inicio + " at&eacute; " + fim + " OBS: " + obs;
      var tipo = $(this).attr("tipo");
      var apr = "";
      var dados = " tipo='" + tipo + "' nome='" + nome + "' frequencia='" + freq + "' inicio='" + inicio + "' fim='" + fim + "' obs='" + obs + "' cod='" + cod + "' brasindice_id='" + brasindice_id + "'";
      atualiza_qtd(tipo, "+");
      $("#itens-" + tipo).after("<tr bgcolor='#ebf3ff' class='dados' " + dados + " ><td>" + linha + "</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
      //console.log("resposta", resposta);
      reload_css_table();
      $("#div-repouso input[name='obs']").val("");
      $("#div-repouso input[name='aprazamento']").val("");
      $("select[name='repouso'] option").eq(0).attr('selected', 'selected');
      $("#div-repouso select[name='frequencia'] option").eq(0).attr('selected', 'selected');
    }
  });

  $("#tipo-gtm").live('change', function(){
    $("#extras-tipo-gtm").html("");
    if($(this).val() == '1'){
      $("#extras-tipo-gtm").html(
          "Furo: <input type='text' name='furo' id='furo' size='4' class='OBG'> " +
          "CM: <input type='text' name='cm' id='cm' size='4' class='OBG'>"
      );
    } else {
      $("#extras-tipo-gtm").html(
          "FR: <input type='text' name='fr' id='fr' size='4' class='OBG'> "
      );
    }
  });

  $("#add-gtm").live('click', function () {
    if(validar_campos("div-gastrostomia")) {
      var cod = $("#div-gastrostomia input[name='cod']").val();
      var brasindice_id = $("#div-gastrostomia input[name='brasindice_id']").val();
      var nome = $("#div-gastrostomia input[name='nome']").val();
      var data_troca = $("#div-gastrostomia input[name='data_troca']").val();
      var inicio = $("#div-gastrostomia input[name='inicio']").val();
      var fim = $("#div-gastrostomia input[name='fim']").val();
      var tipo_gtm = $("#div-gastrostomia select[name='tipo_gtm'] option:selected").val();
      var furo = $("#div-gastrostomia input[name='furo']").val() || '';
      var cm = $("#div-gastrostomia input[name='cm']").val() || '';
      var fr = $("#div-gastrostomia input[name='fr']").val() || '';
      var marca_gtm = $("#div-gastrostomia input[name='marca_gtm']").val();
      var dano_gtm = $("#div-gastrostomia textarea[name='dano_gtm']").val();
      var origem_dano_gtm = $("#div-gastrostomia select[name='origem_dano_gtm'] option:selected").val();
      var descricao_origem_dano_gtm = $("#div-gastrostomia textarea[name='descricao_origem_dano_gtm']").val();
      var linha = "<b>Procedimentos em Gastrostomia:</b> " + nome + ". Troca em: " + data_troca +  ". Per&iacute;odo: " + inicio + " at&eacute; " + fim;
      var tipo = $(this).attr("tipo");
      var dados = " tipo='" + tipo +
                  "' nome='" + nome +
                  "' data_troca='" + data_troca +
                  "' tipo_gtm='" + tipo_gtm +
                  "' furo='" + furo +
                  "' cm='" + cm +
                  "' fr='" + fr +
                  "' marca_gtm='" + marca_gtm +
                  "' dano_gtm='" + dano_gtm +
                  "' origem_dano_gtm='" + origem_dano_gtm +
                  "' descricao_origem_dano_gtm='" + descricao_origem_dano_gtm +
                  "' inicio='" + inicio +
                  "' fim='" + fim +
                  "' cod='" + cod +
                  "' brasindice_id='" + brasindice_id + "'";
      atualiza_qtd(tipo, "+");
      $("#itens-" + tipo).after("<tr bgcolor='#ebf3ff' class='dados' " + dados + " ><td>" + linha + "</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
      reload_css_table();
      $("#extras-tipo-gtm").html("");
      $("#div-gastrostomia input[name='cod']").val("-1");
      $("#div-gastrostomia input[name='brasindice_id']").val("-1");
      $("#div-gastrostomia input[name='nome']").val("");
      $("#div-gastrostomia input[name='data_troca']").val("");
      $("#div-gastrostomia input[name='tipo_gtm'] option:selected").val("");
      $("#div-gastrostomia input[name='furo']").val("");
      $("#div-gastrostomia input[name='cm']").val("");
      $("#div-gastrostomia input[name='fr']").val("");
      $("#div-gastrostomia input[name='marca_gtm']").val("");
      $("#div-gastrostomia textarea[name='dano_gtm']").val("");
      $("#div-gastrostomia textarea[name='descricao_origem_dano_gtm']").val("");
      $("#div-gastrostomia select[name='tipo_gtm'] option").eq(0).attr('selected', 'selected');
      $("#div-gastrostomia select[name='origem_dano_gtm'] option").eq(0).attr('selected', 'selected');
    }
  });

  $(".dias_dieta").keyup(function () {
    var valor = $(this).val().replace(/[^0-9]/g, '');
    $(this).val(valor);
  });
  $(".num_dias").hide();

  $("#selcuidados").change(function () {
    var X = $("#div-cuidados select[name='cuidados']").val();

    if(X == '25') {
      $(".num_dias").show();
      $("input[name='dias_dieta']").focus();
    } else {
      $(".num_dias").hide();
      $("input[name='dias_dieta']").val('');
    }
  });
  $("#add-dieta").click(function () {
    if(validar_campos("div-dieta")) {
      var nome = $("#div-dieta select[name='dieta'] option:selected").html();
      var cod = $("#div-dieta select[name='dieta'] option:selected").val();
      var brasindice_id = $("#div-dieta select[name='dieta'] option:selected").val();

      var diarias = $('#diarias :selected').val();

      var quantidade_diarias = $("#quantidade_diarias").val();
      var mensagem_diarias = "";

      if(diarias == 's') {

        if(quantidade_diarias > 1) {
          mensagem_diarias = quantidade_diarias + " diÃ¡rias de dieta enteral ";
        } else if(quantidade_diarias == 1) {
          mensagem_diarias = "Uma diÃ¡ria de dieta enteral ";
        }
      } else {
        quantidade_diarias = 0;
      }

      var obs = mensagem_diarias + $("#div-dieta input[name='obs']").val();

      var pos = $("#div-dieta select[name='vadm-dieta'] option:selected").html() + " " + $("#div-dieta select[name='frequencia'] option:selected").html();
      var vadm = $("#div-dieta select[name='vadm-dieta'] option:selected").val();
      var freq = $("#div-dieta select[name='frequencia'] option:selected").val();
      var inicio = $("#div-dieta input[name='inicio']").val();
      var fim = $("#div-dieta input[name='fim']").val();
      var label = $(this).attr("label");




      var linha = "<b>" + label + ":</b> " + nome + " " + pos + ". Per&iacute;odo: " + inicio + " at&eacute; " + fim + " OBS: " + obs;
      var tipo = $(this).attr("tipo");
      var dados = " tipo='" + tipo + "' nome='" + nome + "' via='" + vadm + "' frequencia='" + freq + "' inicio='" + inicio + "' fim='" + fim + "' obs='" + obs + "' cod='" + cod + "' brasindice_id='" + brasindice_id + "'";
      atualiza_qtd(tipo, "+");
      $("#itens-" + tipo).after("<tr bgcolor='#ebf3ff' class='dados' " + dados + " ><td>" + linha + "</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
      reload_css_table();
      $("#div-dieta input[name='obs']").val("");
      $("#div-dieta input[name='aprazamento']").val("");
      $("select[name='dieta'] option").eq(0).attr('selected', 'selected');
      $("#div-dieta select[name='frequencia'] option").eq(0).attr('selected', 'selected');

      $("#div-dieta select[name='vadm-dieta'] option:selected").attr('selected', '');
    }
  });

  $("#add-soro").click(function () {
    if(validar_campos("div-soros")) {
      var nome = $("#div-soros select[name='soros'] option:selected").html();
      var cod = $("#div-soros select[name='soros'] option:selected").val();
      var brasindice_id = $("#div-soros select[name='soros'] option:selected").val();
      var obs = $("#div-soros input[name='obs']").val();
      var pos = $("#div-soros select[name='vadm-soro'] option:selected").html() + " " + $("#div-soros select[name='frequencia'] option:selected").html();
      var via = $("#div-soros select[name='vadm-soro'] option:selected").val();
      var freq = $("#div-soros select[name='frequencia'] option:selected").val();
      var inicio = $("#div-soros input[name='inicio']").val();
      var fim = $("#div-soros input[name='fim']").val();
      var fluxo = $("#div-soros input[name='fluxo']").val() + " " + $("#div-soros select[name='um'] option:selected").html();
      var volume_total = $("#div-soros input[name='volume_total']").val();
      nome += " " + fluxo + ". <b>Volume Total:</b> " + volume_total;
      var label = $(this).attr("label");
      var linha = "<b>" + label + ":</b> " + nome + " " + pos + ". Per&iacute;odo: " + inicio + " at&eacute; " + fim + " OBS: " + obs;
      var tipo = $(this).attr("tipo");
      var dados = " tipo='" + tipo + "' nome='" + nome + "' via='" + via + "' frequencia='" + freq + "' inicio='" + inicio + "' fim='" + fim + "' obs='" + obs + "' cod='" + cod + "' brasindice_id='" + brasindice_id + "'";
      atualiza_qtd(tipo, "+");
      $("#itens-" + tipo).after("<tr bgcolor='#ebf3ff' class='dados' " + dados + " ><td>" + linha + "</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
      reload_css_table();
      $("#div-soros input[name='obs']").val("");
      $("#div-soros input[name='aprazamento']").val("");
      $("#div-soros input[name='fluxo']").val("");
      $("#div-soros input[name='volume_total']").val("");
      $("#div-soros select[name='vadm-soro'] option").eq(0).attr('selected', 'selected');
      $("select[name='soros'] option").eq(0).attr('selected', 'selected');
      $("#div-soros select[name='frequencia'] option").eq(0).attr('selected', 'selected');
      $("#div-soros select[name='um'] option").eq(0).attr('selected', 'selected');
    }
  });

  $("#add-cuidados").click(function () {
    if(validar_campos("div-cuidados")) {
      var cod = $("#div-cuidados select[name='cuidados'] option:selected").val();
      var brasindice_id = $("#div-cuidados select[name='cuidados'] option:selected").val();
      var nome = $("#div-cuidados select[name='cuidados'] option:selected").html();
      var obs = $("#div-cuidados textarea[name='obs']").val();

      var x = $("#cuid1").attr("n");
      if(x == 1) {
        var nfreq = $("#div-cuidados select[name='frequencia'] option:selected").html();
        var freq = $("#div-cuidados select[name='frequencia'] option:selected").val();
      } else {
        var nfreq = $("#div-cuidados select[name='frequencia1'] option:selected").html();
        var freq = $("#div-cuidados select[name='frequencia1'] option:selected").val();
      }
      var diaria_enteral = $("#diaria_enteral option:selected").val();

      var inicio = $("#div-cuidados input[name='inicio']").val();
      var fim = $("#div-cuidados input[name='fim']").val();
      var label = $(this).attr("label");
      var linha = "<b>" + label + ":</b> " + nome + ". " + nfreq + " Per&iacute;odo: " + inicio + " at&eacute; " + fim + " OBS.: " + obs;
      var tipo = $(this).attr("tipo");
      var dados = " tipo='" + tipo + "' nome='" + nome + "' frequencia='" + freq + "' inicio='" + inicio + "' fim='" + fim + "' obs='" + obs + "' cod='" + cod + "' brasindice_id='" + brasindice_id + "'";
      atualiza_qtd(tipo, "+");
      $("#itens-" + tipo).after("<tr bgcolor='#ebf3ff' class='dados' " + dados + " ><td>" + linha + "</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
      reload_css_table();
      $("#div-cuidados textarea[name='obs']").val("");
      $("#div-cuidados input[name='aprazamento']").val("");
      $("#div-cuidados select[name='frequencia'] option").eq(0).attr('selected', 'selected');
      $("#div-cuidados select[name='cuidados'] option").eq(0).attr('selected', 'selected');
      $("#div-cuidados select[name='frequencia'] option").eq(0).attr('selected', 'selected');
      $("#div-cuidados input[name='qtd_dias']").val("");
      $("#diaria_enteral option").attr('selected', '');
    }
  });

  var flag = 0;


  $("#dialog-incluir-diluente").dialog({
    autoOpen: false,
    modal: true,
    buttons: {
      "Sim": function () {

        $(this).dialog("close");
        verificarNeb(1);
        return flag;


      },
      "Não": function () {

        $(this).dialog("close");
        verificarNeb(0);
        return flag;


      }

    }
  });

  function verificarNeb(flag1) {

    if(flag1 == 1) {



      $("#div-nebulizacao").each(function () {




        var tipo = $(this).attr("tipo");
        var diluicao = $("select[name='diluicao-nebulizacao'] option:selected").val();

        var qtd = $("input[name='qtd-diluicao']").val();
        var um = $("#um-diluicao-neb option:selected").html();
        var neb = diluicao + " " + qtd + " " + um;

        //$(".comp-nebulizacao").each(function(){
        if($("#Atrovent").is(":checked")) {
          neb += " + ";

          var med = $("input[name='Atrovent']").attr('name') + " (Brometo de Ipratrópio)";
          var qtd_med = $("#comp-nebulizacao input[name='neb']").val();
          var um_med = $("#comp-nebulizacao select option:selected").html();
          //var qtd_med = $(this).parent().parent().children('td').eq(2).children('input').val();;
          //var um_med = $(this).parent().children('td').eq(3).find("select option:selected").html();
          neb += med + " " + qtd_med + " " + um_med;

        }

        if($("#Berotec").is(":checked")) {
          neb += " + ";
          var med = $("#comp-nebulizacao2 input[name='Berotec']").attr('name') + " (Bromidrato de Fenoterol)";
          var qtd_med = $("#comp-nebulizacao2 input[name='neb']").val();
          var um_med = $("#comp-nebulizacao2 select option:selected").html();
          neb += med + " " + qtd_med + " " + um_med;

        }
        //});

        if($("#check-outro-neb").attr("checked")) {
          if(neb != "")
            neb += " + ";
          var med = $("#nome-outro-neb").val();
          var qtd_med = $("#dose-outro-neb").val();
          var um_med = $("#um-outro-neb option:selected").html();
          neb += med + " " + qtd_med + " " + um_med;

        }

        var obs = $("#div-nebulizacao input[name='obs']").val();
        var inicio = $("#div-nebulizacao input[name='inicio']").val();
        var fim = $("#div-nebulizacao input[name='fim']").val();
        var freq = $("#div-nebulizacao select[name='frequencia'] option:selected").val();
        var nfreq = $("#div-nebulizacao select[name='frequencia'] option:selected").html();
        var linha = neb + ". " + nfreq + " Per&iacute;odo: " + inicio + " at&eacute; " + fim + " OBS: " + obs;

        var dados = " tipo='" + tipo + "' nome='" + neb + "' frequencia='" + freq + "' inicio='" + inicio + "' fim='" + fim + "' obs='" + obs + "'";

        atualiza_qtd(tipo, "+");
        $("#itens-" + tipo).after("<tr bgcolor='#ebf3ff' class='dados' " + dados + " ><td><b>Nebuliza&ccedil;&atilde;o: </b>" + linha + "</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
        reload_css_table();
        $("input[name='qtd-diluicao']").val('');
        $("#um-diluicao-neb option").eq(0).attr('selected', 'selected');
        $("#div-nebulizacao select[name='frequencia'] option").eq(0).attr('selected', 'selected');
        $("input[name='obs']").val('');
        $(".comp-nebulizacao").each(function () {
          if($(this).children('td').eq(0).children('input').attr("checked")) {
            $(this).children('td').eq(2).children('input').val('');
            $(this).children('td').eq(2).children('input').attr('disabled', 'diabled');
            $(this).children('td').eq(2).children('input').removeClass("OBG");
            $(this).children('td').eq(2).children('input').removeAttr('style');
            $(this).children('td').eq(3).children('select').removeClass("COMBO_OBG");
            $(this).children('td').eq(3).children('select').removeAttr('style');
            $(this).children('td').eq(3).find("select option").eq(0).attr('selected', 'selected');
            $(this).children('td').eq(0).children('input').attr("checked", "");
            $("#diluicao-nebulizacao ").val("");
            //$("#frequencia").removeClass("COMBO_OBG");
            $("#frequencia").removeAttr("style");
            $("#frequencia").val("-1");
          }
        });
        çbd
        $("#nome-outro-neb").removeClass("OBG").val("");
        $("#dose-outro-neb").removeClass("OBG").attr('disabled', '').val("");
        $('#um-outro-neb').removeClass("COMBO_OBG");
        $("#um-outro-neb option").eq(0).attr('selected', 'selected');
        $("#check-outro-neb").attr("checked", "");
        $("#diluicao-nebulizacao ").val("");


        $("#um-diluicao-neb").val("-1");
        $("#qtd-diluicao").val("");
        $("#diluicao-nebulizacao").val("");
        $("#frequencia").val("-1");

        //$("#qtd-diluicao").removeClass("OBG");
        //$("#um-diluicao-neb").removeClass("COMBO_OBG");
        //$("#diluicao-nebulizacao").removeClass("OBG");
        //$("#frequencia").removeClass("COMBO_OBG");

        $("#um-diluicao-neb").removeAttr("style");
        $("#frequencia").removeAttr("style");
        $("#qtd-diluicao").removeAttr("style");
        $("#diluicao-nebulizacao").removeAttr("style");



        nome = '';
        dosecomp = '';
        dose = '';
        freq = '';
        obs = '';
        inicio = '';
        oxi = '';
        nfreq = '';
        linha = '';
        dados = '';



      });
    }
  }


  $("#add-nebulizacao").click(function () {


    if(validar_campos("div-nebulizacao")) {

      if($("#Atrovent").is(":checked") || $("#Berotec").is(":checked") || $("#check-outro-neb").is(":checked")) {

        flag = 1;
        verificarNeb(flag);

      } else {
        $("#dialog-incluir-diluente").dialog("open");
      }
    }



  });



  ///add oxigenoterapia jeferson

  $("#add-oxigenoterapia").click(function () {
    if(validar_campos("div-oxigenoterapia")) {
      $(".oxi").each(function () {

        if($(this).is(':checked')) {
          var tipo = 13;
          var d;
          var oxigen;


          if($(this).attr('tipo') == 0) {
            cod = $(this).attr('cod');
            nome = $(this).parent().children('b').html();
            d = $(this).parent().parent().children('td').eq(1).children('input').val();
            var m = "l/min";
            oxigen = (nome + " " + d + "" + m);
          }

          if($(this).attr('tipo') == 1) {
            nome = $(this).parent().children('b').html();
            qtd = $(this).parent().parent().children('td').eq(1).children('select').find('option').filter(':selected').text();
            oxigen = nome + " " + qtd;
            d = $(this).parent().parent().children('td').eq(1).children('select').find('option').filter(':selected').attr('med');
          }

          if($(this).attr('tipo') == 3) {
            nome = $(this).parent().children('b').html();

            qtd = $(this).parent().parent().children('td').eq(1).children('select').find('option').filter(':selected').text();
            oxigen = nome + " " + qtd;
            d = $(this).parent().parent().children('td').eq(1).children('select').find('option').filter(':selected').attr('med');
          }

          if($(this).attr('tipo') == 2) {
            nome = $("input:radio[tipo=2]").attr('amb');
            oxigen = nome;
          }

          if($(this).attr('tipo') == 4) {
            nome = $("input:radio[tipo=4]").attr('amb');
            oxigen = nome;
          }

          var dose = d;
          var um = "28";
          var obs = $("#div-oxigenoterapia input[name='obs']").val();
          var inicio = $("#div-oxigenoterapia input[name='inicio']").val();
          var fim = $("#div-oxigenoterapia input[name='fim']").val();
          var freq = $("#div-oxigenoterapia select[name='frequencia'] option:selected").val();
          var nfreq = $("#div-oxigenoterapia select[name='frequencia'] option:selected").html();
          var linha = oxigen + ". " + nfreq + " Per&iacute;odo: " + inicio + " at&eacute; " + fim + " OBS: " + obs;
          var dados = " tipo='" + tipo + "' cod='" + cod + "' nome='" + oxigen + "' frequencia='" + freq + "' inicio='" + inicio + "' fim='" + fim + "' obs='" + obs + "' dose='" + dose + "' um='" + um + "'";

          atualiza_qtd(tipo, "+");
          $("#itens-" + tipo).after("<tr bgcolor='#ebf3ff' class='dados' " + dados + " ><td><b>Oxigenoterapia: </b>" + linha + "</td><td><input type='checkbox' name='select' class='select-item' /></td></tr>");
          reload_css_table();

          $("#div-oxigenoterapia select[name='frequencia'] option").eq(0).attr('selected', 'selected');
          $("input[name='obs']").val('');

          $(".oxi2").each(function () {

            $("input[name='dose']").removeClass("OBG");
            $("input.oxi2").attr('disabled', 'disabled');
            $("input.oxi2").val("");
            $("input[name='dose']").removeAttr("style");

            $("select[name='dose']").removeClass("OBG");
            $("select.oxi2").attr('disabled', 'disabled');
            $("select.oxi2").val("");
            $("select[name='dose']").removeAttr("style");

          });
          $("#frequenciaoxi").removeAttr("style");
          $("#frequenciaoxi").removeClass("COMBO_OBG");
          $("#frequenciaoxi").val("-1");
          $("input.oxi").attr("checked", false);

          nome = '';
          dosecomp = '';
          dose = '';
          freq = '';
          obs = '';
          inicio = '';
          oxi = '';
          nfreq = '';
          linha = '';
          dados = '';
        }

      });
    }
  });

  $('.criar-presc').live('click', function () {
    // var view = this.attr('view');  
    $(this).attr('carater');
    $("input[name='paciente']").val($(this).attr('CodPaciente'));
    $("input[name='antigas']").val($(this).attr('p'));

    //var p = $(this).attr('p');
    var status = $("select[name='paciente'] option:selected").attr('status');
    var p = $("input[name='antigas']").val();

    if(p != '-1' && status == 4) { //Se Ã© usada para nova

      $(".tipo_presc").each(function () {

        if($(this).val() == 1) {

          $(this).parent('span.usar_nova').hide();
        }

      });

    }

    $("#NomePacientePresc").html($(this).attr('paciente'));
    $("#pre-presc").attr('busca', $(this).attr('busca'));
    $("#pre-presc").dialog("open");
  });

  $("#pre-presc").dialog({
    autoOpen: false,
    modal: true,
    width: 400,
    open: function (event, ui) {

    },
    buttons: {
      "Fechar": function () {
        $(this).dialog("close");
      }
    }
  });



  //Paciente - inÃ­cio
  $("#nova_ficha").click(function () {
    var id = $("input[name='nova_ficha']").val();

    window.document.location.href = '?view=paciente_med&act=newcapmed&id=' + id + '';

  });
  $("#nova_ficha_evolucao").click(function () {
    var id = $("input[name='nova_ficha_evolucao']").val();

    window.document.location.href = '?view=paciente_med&act=newfichaevolucao&id=' + id + '';

  });
  // ////////////dados iniciais da ficha/////////////
  // / se paciente ï¿½ id ou ad;
  $(".modalidade").click(function () {

    if($("input.modalidade:checked")) {
      var x = $("input.modalidade:checked").val();
    }
     
    if($("input.local_av:checked")) {
      var y = $("input.local_av:checked").val();
    }

      if (x == 1) {
          $("#score-id1").hide();
          $("#score-id1 input").removeClass('OBG_RADIO');
          $("#score-id2").hide();
          $("#score-id2 input").removeClass('OBG_RADIO');

      }
      if (x == 1 && y == 1) {
          $("#score-id1").hide();
          $("#score-id1 input").removeClass('OBG_RADIO');
          $("#score-id2").hide();
          $("#score-id2 input").removeClass('OBG_RADIO');


      }
      if (x == 1 && y == 2) {
          $("#score-id1").hide();
          $("#score-id1 input").removeClass('OBG_RADIO');
          $("#score-id2").hide();
          $("#score-id2 input").removeClass('OBG_RADIO');

      }
      if (x == 2 && y == 1) {
          $("#score-id2").hide();
          $("#score-id2 input").removeClass('OBG_RADIO');
          $("#score-id1").show();
          $("#score-id1 input").addClass('OBG_RADIO');


      }
      if (x == 2 && y == 2) {
          $("#score-id1").show();
          $("#score-id1 input").addClass('OBG_RADIO');
          $("#score-id2").show();
          $("#score-id2 input").addClass('OBG_RADIO');

      }




  });

  $("#imprimir_ficha_mederi").click(function () {

    var x = 0;
    x = $("input[name='paciente']").val();
    window.location.href = '?view=paciente_med&act=listcapmed&id=' + x;


  });
  $("#imprimir_ficha_evolucao").click(function () {

    var x = 0;
    x = $("input[name='paciente']").val();
    window.location.href = '?view=paciente_med&act=listfichaevolucao&id=' + x;


  });

  $("#imprimir_score").click(function () {

    var x = 0;
    x = $("input[name='imprimir_score']").val();
    var tipo = $(this).attr("tipo");

    //window.location.href='?view=paciente_med&act=listcapmed&id='+x+'&tipo='+tipo;

  });

  $("#imprimir_det_presc").click(function () {

    var x = 0;
    x = $("input[name='imprimir_det_presc']").val();

    window.location.href = '?view=paciente_med&act=listcapmed&id=' + x;


  });

  $(".local_av").click(function () {
    var x;
    var y;
    if($("input.modalidade:checked")) {
      x = $("input.modalidade:checked").val();
    }

    if($("input.local_av:checked")) {
      y = $("input.local_av:checked").val();
    }




    if(x == 1) {
      $("#score-id1").hide();
      $("#score-id2").hide();

    }
    if(x == 1 && y == 1) {
      $("#score-id1").hide();
      $("#score-id2").hide();

    }
    if(x == 1 && y == 2) {
      $("#score-id1").hide();
      $("#score-id2").hide();

    }
    if(x == 2 && y == 1) {
      $("#score-id2").hide();
      $("#score-id1").show();


    }
    if(x == 2 && y == 2) {
      $("#score-id1").show();
      $("#score-id2").show();

    }


  });

  // ////////////////////ficha evolucao///////////////////////////////////

  $("#dialog-aviso-sinaissemana").dialog({
    autoOpen: false,
    modal: true,
    width: 500,
    buttons: {
      "Sim": function () {
        $(".sinais_vitais").show();
        $(this).dialog("close");
      },
      "Não": function () {
        $(".sinais_vitais").hide();
        $(this).dialog("close");
      }
    }
  });

  $("#dialog-aviso-exames-complementares").dialog({
    autoOpen: false,
    modal: true,
    width: 500,
    buttons: {
      "Sim": function () {
        $(".novo-exame").show();
        $(this).dialog("close");
      },
      "Não": function () {
        $(".novo-exame").hide();
        $(this).dialog("close");
      }
    }
  });

  $(".modalidade-evolucao").change(function(){
    if($(this).val() == '1'){
      $(".sinais_vitais").hide();
      $("#dialog-aviso-exames-complementares").dialog("open");
    }else{
      $("#dialog-aviso-sinaissemana").dialog("open");
      $("#dialog-aviso-exames-complementares").dialog("open");
    }
  });

  /* ParametrizaÃ§Ã£o de Alertas
   * 
   * 		SINAL VITAL			| 				ALERTA AMARELO					| 			ALERTA VERMELHO
   PRESSÃƒO ARTERIAL SISTÃ“LICA	| 	70-90 (MIN);  145-180 (MAX)					| 	< 70 (MIN); > 180 MAX
   PRESSÃƒO ARTERIAL DIASTÃ“LICA	| 	50-70 (MIN);  90-110 (MAX)					| 	< 50 (MIN); > 110 (MAX)
   FREQUÃŠNCIA CARDÃ�ACA			| 	50-60 (MIN); 100-115 (MAX)					| 	< 50 (MIN); >115 (MAX)
   FREQUÃŠNCIA RESPIRATÃ“RIA		| 	12-16 (MIN); 20-24 (MAX)					| 	< 12 (MIN); >24 (MAX)
   TEMPERATURA AXILAR			| 	34,5 - 35,5 (MIN); 37,5 - 37,9 (MAX)		| 	< 34,5 (MIN); > 37,9 (MAX)
   OXIMETRIA DE PULSO			| 	88-92 EM AR AMBIENTE; 92-95 O2 SUPLEMENTAR	| 	< 88 EM AR AMBIENTE; < 92 COM O2 SUPLEMENTAR

   Ao preencher os campos supracitados, o sistema deve selecionar automaticamente o campo Alerta, de acordo com a faixa especificada.

   * ParÃ¢metros definidos por dr. Augusto em 15/02/2013
   * */

  $(".alertas").focusout(function () {

    //Verificar se os campos estÃ£o vazios

    var pa_sistolica_min = $("input[name=pa_sistolica_min]").val();
    var pa_sistolica_max = $("input[name=pa_sistolica_max]").val();
    var pa_diastolica_min = $("input[name=pa_diastolica_min]").val();
    var pa_diastolica_max = $("input[name=pa_diastolica_max]").val();
    var fc_min = $("input[name=fc_min]").val();
    var fc_max = $("input[name=fc_max]").val();
    var fr_min = $("input[name=fr_min]").val();
    var fr_max = $("input[name=fr_max]").val();
    var temperatura_min = $("input[name=temperatura_min]").val();
    var temperatura_max = $("input[name=temperatura_max]").val();
    var oximetria_pulso_min = $("input[name=oximetria_pulso_min]").val();
    var oximetria_pulso_max = $("input[name=oximetria_pulso_max]").val();

    //console.log("oximetria_pulso_min", oximetria_pulso_min);

    //PA Sistolica
    var pa_sistolica_amarelo = 0;
    var pa_sistolica_vermelho = 0;

    if(pa_sistolica_min != '' || pa_sistolica_max != '') {

      if(pa_sistolica_min < 70) { //Alerta Vermelho
        pa_sistolica_vermelho = 1;
      }
      if(pa_sistolica_min >= 70 && pa_sistolica_min <= 85) { //Alerta Amarelo
        pa_sistolica_amarelo = 1;
      }

      if(pa_sistolica_max >= 180) { //Alerta Vermelho
        pa_sistolica_vermelho = 1;
      } else if(pa_sistolica_max >= 145 && pa_sistolica_max <= 180) { //Alerta Amarelo
        pa_sistolica_amarelo = 1;
      }

      if(pa_sistolica_vermelho == 1) {
        $('input[name="pa_sistolica_sinal"]')[1].checked = true;
      } else if(pa_sistolica_amarelo == 1) {
        $('input[name="pa_sistolica_sinal"]')[0].checked = true;
      } else {
        $("input:radio[name=pa_sistolica_sinal]").attr("checked", false);
      }

    } else {
      $("input:radio[name=pa_sistolica_sinal]").attr("checked", false);
    }


    //PA Diastolica
    var pa_diastolica_amarelo = 0;
    var pa_diastolica_vermelho = 0;

    if(pa_diastolica_min != '' || pa_diastolica_max != '') {
      if(pa_diastolica_min < 50) { //Alerta Vermelho
        pa_diastolica_vermelho = 1;
      }
      if(pa_diastolica_min >= 50 && pa_diastolica_min <= 60) { //Alerta Amarelo
        pa_diastolica_amarelo = 1;
      }

      if(pa_diastolica_max > 110) { //Alerta Vermelho
        pa_diastolica_vermelho = 1;
      } else if(pa_diastolica_max >= 90 && pa_diastolica_max <= 110) { //Alerta Amarelo
        pa_diastolica_amarelo = 1;
      }

      if(pa_diastolica_vermelho == 1) {
        $('input[name="pa_diastolica_sinal"]')[1].checked = true;
      } else if(pa_diastolica_amarelo == 1) {
        $('input[name="pa_diastolica_sinal"]')[0].checked = true;
      } else {
        $("input:radio[name=pa_diastolica_sinal]").attr("checked", false);
      }
    } else {
      $("input:radio[name=pa_diastolica_sinal]").attr("checked", false);
    }

    //FrequÃªncia CardÃ­aca
    var fc_amarelo = 0;
    var fc_vermelho = 0;

    if(fc_min != '' || fc_max != '') {
      if(fc_min < 50) { //Alerta Vermelho
        fc_vermelho = 1;
      }

      if(fc_max >= 140) { //Alerta Vermelho
        fc_vermelho = 1;
      } else if(fc_max >= 125 && fc_max <= 139) { //Alerta Amarelo
        fc_amarelo = 1;
      }

      if(fc_vermelho == 1) {
        $('input[name="fc_sinal"]')[1].checked = true;
      } else if(fc_amarelo == 1) {
        $('input[name="fc_sinal"]')[0].checked = true;
      } else {
        $("input:radio[name=fc_sinal]").attr("checked", false);
      }
    } else {
      $("input:radio[name=fc_sinal]").attr("checked", false);
    }

    //FrequÃªncia RespiratÃ³ria
    var fr_amarelo = 0;
    var fr_vermelho = 0;

    if(fr_min != '' || fr_max != '') {
      if(fr_min < 6) { //Alerta Vermelho
        fr_vermelho = 1;
      }

      if(fr_max > 24) { //Alerta Vermelho
        fr_vermelho = 1;
      } else if(fr_max >= 20 && fr_max <= 24) { //Alerta Amarelo
        fr_amarelo = 1;
      }

      if(fr_vermelho == 1) {
        $('input[name="fr_sinal"]')[1].checked = true;
      } else if(fr_amarelo == 1) {
        $('input[name="fr_sinal"]')[0].checked = true;
      } else {
        $("input:radio[name=fr_sinal]").attr("checked", false);
      }
    } else {
      $("input:radio[name=fr_sinal]").attr("checked", false);
    }

    //Temperatura
    var temperatura_amarelo = 0;
    var temperatura_vermelho = 0;

    if(temperatura_min != '' || temperatura_max != '') {

      if(temperatura_min < 34.5) { //Alerta Vermelho
        temperatura_vermelho = 1;
      }
      if(temperatura_min >= 34.5 && temperatura_min <= 35.5) { //Alerta Amarelo
        temperatura_amarelo = 1;
      }

      if(temperatura_max > 38.5) { //Alerta Vermelho
        temperatura_vermelho = 1;
      } else if(temperatura_max >= 37.9 && temperatura_max <= 38.4) { //Alerta Amarelo
        temperatura_amarelo = 1;
      }

      if(temperatura_vermelho == 1) {
        $('input[name="temperatura_sinal"]')[1].checked = true;
      } else if(temperatura_amarelo == 1) {
        $('input[name="temperatura_sinal"]')[0].checked = true;
      } else {
        $("input:radio[name=temperatura_sinal]").attr("checked", false);
      }
    } else {
      $("input:radio[name=temperatura_sinal]").attr("checked", false);
    }

    // Oximetria
    // este campo sÃ³ tem o alerta no campo mÃ­nimo (dr. LÃºcio)
    var oximetria_pulso_amarelo = 0;
    var oximetria_pulso_vermelho = 0;

    var tipo_oximetria_min = $("select[name='tipo_oximetria_min'] option:selected").val();

    if(oximetria_pulso_min != '') {
      /*if (tipo_oximetria_min == 1) {

       }else if (tipo_oximetria_min == 2) {

       }*/

      if(oximetria_pulso_min < 90) { //Alerta Vermelho
        oximetria_pulso_vermelho = 1;
      }

      if(oximetria_pulso_vermelho == 1) {
        $('input[name="oximetria_sinal"]')[1].checked = true;
      } else if(oximetria_pulso_amarelo == 1) {
        $('input[name="oximetria_sinal"]')[0].checked = true;
      } else {
        $("input:radio[name=oximetria_sinal]").attr("checked", false);
      }
    } else {
      $("input:radio[name=oximetria_sinal]").attr("checked", false);
    }

  });

  $(".alertas_sinais_vitais").focusout(function () { //focusout - blur (n)

    //Verifica se uma das opÃ§Ãµes estÃ¡ marcada e abre um textarea para o mÃ©dico digitar as impressÃµes
    //PA sistolica
    if($('input[name="pa_sistolica_sinal"]')[0].checked == true || $('input[name="pa_sistolica_sinal"]')[1].checked == true) {
      $("textarea[name='pa_sistolica_observacao_alerta']").show();
    } else {
      $("textarea[name='pa_sistolica_observacao_alerta']").hide();
    }

    //PA Diastolica
    if($('input[name="pa_diastolica_sinal"]')[0].checked == true || $('input[name="pa_diastolica_sinal"]')[1].checked == true) {
      $("textarea[name='pa_diastolica_observacao_alerta']").show();
    } else {
      $("textarea[name='pa_diastolica_observacao_alerta']").hide();
    }

    //FC
    if($('input[name="fc_sinal"]')[0].checked == true || $('input[name="fc_sinal"]')[1].checked == true) {
      $("textarea[name='fc_observacao_alerta']").show();
    } else {
      $("textarea[name='fc_observacao_alerta']").hide();
    }

    //FR
    if($('input[name="fr_sinal"]')[0].checked == true || $('input[name="fr_sinal"]')[1].checked == true) {
      $("textarea[name='fr_observacao_alerta']").show();
    } else {
      $("textarea[name='fr_observacao_alerta']").hide();
    }

    //Temperatura
    if($('input[name="temperatura_sinal"]')[0].checked == true || $('input[name="temperatura_sinal"]')[1].checked == true) {
      $("textarea[name='temperatura_observacao_alerta']").show();
    } else {
      $("textarea[name='temperatura_observacao_alerta']").hide();
    }

    //Oximetria
    if($('input[name="oximetria_sinal"]')[0].checked == true || $('input[name="oximetria_sinal"]')[1].checked == true) {
      $("textarea[name='oximetria_pulso_observacao_alerta']").show();
    } else {
      $("textarea[name='oximetria_pulso_observacao_alerta']").hide();
    }

  });


  $(".exame").click(function () {

    if($(this).is(":checked")) {
      var val = $(this).val();
      var nome = $(this).attr('name');

      if(val == 1) {
        $("span[name='" + nome + "']").hide();
        $("span[name='" + nome + "']").children('textarea').removeClass('OBG');
        $("span[name='" + nome + "']").children('textarea').removeAttr("style");
        $("span[name='" + nome + "']").children('textarea').val('');
      } else if(val == 3) {
        $("span[name='" + nome + "']").show();
        $("span[name='" + nome + "']").children('textarea').addClass('OBG');
        $("span[name='" + nome + "']").children('textarea').val('Sem queixas locais.');
      } else {
        $("span[name='" + nome + "']").show();
        $("span[name='" + nome + "']").children('textarea').addClass('OBG');
        $("span[name='" + nome + "']").children('textarea').val('');
      }
    }

  });

  $("#sp_quantidade_diarias").hide();

  $("#diarias").change(function () {

    var x = $(this).val();

    if(x == 's') {
      $("#sp_quantidade_diarias").show();
    } else {
      $("#sp_quantidade_diarias").hide();
    }

  });
  /////////////////////17/7/2013 especialista

  $(".especialista").hide();
  $("#check_especialista").click(function () {

    if($(this).is(':checked')) {
      $(".especialista").show();
    } else {
      $(".especialista").hide();
    }
  });

  $("#add-especialista").click(function () {
    var id = $("select[name='especialidade'] option:selected").val();
    var nome = $("select[name='especialidade'] option:selected").html();
    var cont = $(".profissional").size();
    var cont = cont + 1;
    var x = $("input[name='outro-prof']").val();




    var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prof' title='Excluir' border='0' />&nbsp;&nbsp;&nbsp;";
    $('#profissionais').append("<tr><td  class='profissional' value='1111' id_especialidade='" + id + "' desc='" + nome + "'>" + bexcluir + "<input type='checkbox' checked='checked' id_especialidade='" + id + "' value='1111' class='profissional'>" +
    "" + nome + "</td><td class='frequencia_" + cont + "'>" +
    "</td><td><textarea cols=65 rows=2 type=text  name='justificativa' class='OBG' size='100' ></textarea></td></tr>");

    $("input[name='outro-prof']").val("");

    $(".outro_ativo").hide();
    $("input[name='outro-prof']").focus();

    frequencia(cont);


    return false;
  });

  /////////////Atualização Eriton 25-07-2013 Ocultar-mostrar opções////////////////////////
  $(".mucosa").click(function () {
    if($(this).is(":checked")) {
      $(".mucosa1").show();
    } else {
      $(".mucosa1").hide();
    }
  });

  $(".escleras").click(function () {
    if($(this).is(":checked")) {
      $(".escleras1").show();
    } else {
      $(".escleras1").hide();
    }

  });

  $(".respiratorio").click(function () {
    if($(this).is(":checked")) {
      $(".exibir_selecao respiratorio1").show();
    } else {
      $(".exibir_selecao respiratorio1").hide();
    }
  });
  /////////////Fim Atualização Eriton 25-07-2013 Ocultar-mostrar opções////////////////////////

  $("#suplementar_min_span").hide();

  $("select[name='tipo_oximetria_min']").change(function () {

    var valor = $("select[name='tipo_oximetria_min'] option:selected").val();

    if(valor == '2') { //O2 suplementar
      $("#suplementar_min_span").show();
      $("input[name='litros_min']").focus();
    } else {
      $("#suplementar_min_span").hide();
      $("input[name='litros_min']").val('');
      $("select[name='via_oximetria_min']").val('');
    }

  });

  $("#suplementar_span").hide();

  $("select[name='tipo_oximetria']").change(function () {

    var valor = $("select[name='tipo_oximetria'] option:selected").val();

    if(valor == '2') { //O2 suplementar
      $("#suplementar_span").show();
      $("input[name='litros']").focus();
    } else {
      $("#suplementar_span").hide();
      $("input[name='litros']").val('');
      $("select[name='via_oximetria']").val('');
    }

  });

  $("#suplementar_max_span").hide();

  $("select[name='tipo_oximetria_max']").change(function () {

    var valor = $("select[name='tipo_oximetria_max'] option:selected").val();

    if(valor == '2') { //O2 suplementar
      $("#suplementar_max_span").show();
      $("input[name='litros_max']").focus();
    } else {
      $("#suplementar_max_span").hide();
      $("input[name='litros_max']").val('');
      $("select[name='via_oximetria_max']").val('');
    }

  });

  $(".tipo_oximetria_adicionais").each(function (i) { //Mostra os campos que tem informaÃ§Ãµes de litro e via (o2 suplementar)
    var tipo_oximetria_min = $("select[name='tipo_oximetria_min'] option:selected").val();
    var tipo_oximetria_max = $("select[name='tipo_oximetria_max'] option:selected").val();

    if(tipo_oximetria_min == '2') {
      $("#suplementar_min_span").show();
    }
    if(tipo_oximetria_max == '2') {
      $("#suplementar_max_span").show();
    }

  });

  //$("#dor_span").hide();
  if($("select[name='dor_sn'] option:selected").val() == 's') {
    $("#dor_span").show();
  } else {
    $("#dor_span").hide();
  }

  $("select[name='dor_sn']").change(function () {
    var X = $("select[name='dor_sn'] option:selected").val();

    if(X == 's') {
      $("#dor_span").show();
      $("input[name='dor']").focus();
    } else {
      $("#dor_span").hide();
      $("input[name='dor']").val('');
    }
  });


  $("#add-dor").click(function () {
    var x = $("input[name='dor']").val();

    var bexcluir = "<img src='../utils/delete_16x16.png' class='del-dor-ativos' title='Excluir' border='0' >";
    $('#dor').append("<tr><td width='40%' class='dor1' name='" + x + "' > " + bexcluir + " <b>Local: </b><input type='text' name='local-dor' disabled='disabled' value='" + x + "'name='" + x + "' />&nbsp;&nbsp;Escala analogica:<select name='dor" + x + "' class='COMBO_OBG' style='background-color:transparent;'><option value='-1' selected></option>" +
    "<option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option>" +
    "<option value='6'>6</option><option value='7'>7</option><option value='8'>8</option><option value='9'>9</option><option value='10'>10</option></select>Padr&atilde;o da dor:<select name='padrao_dor' class='padrao_dor COMBO_OBG' style='background-color:transparent;'><option value='-1' selected></option><option value='1'>Pontada</option><option value='2'>Queimação</option><option value='3'>Latejante</option><option value='4'>Peso</option><option value='5'>Outro</option></select><input type='text' name='padraodor' style='display:none'></td></tr>");
    $("input[name='dor']").val("");

    $("input[name='dor']").focus();
    atualiza_dor_ativos();
    return false;

  });

  $(".padrao_dor").live("change", function ()
  {

    if($(this).val() == 5)
    {
      $(this).parent().children("input[name='padraodor']").attr('style', 'display:inline');
    }
    else
    {
      $(this).parent().children("input[name='padraodor']").attr('style', 'display:none');
      $(this).parent().children("input[name='padraodor']").val('');
    }
  });

  $(".del-dor-ativos").unbind("click", remove_dor_ativos).bind("click", remove_dor_ativos);


  function atualiza_dor_ativos() {
    $(".del-dor-ativos").unbind("click", remove_dor_ativos)
      .bind("click", remove_dor_ativos);
  }

  function remove_dor_ativos() {
    $(this).parent().parent().remove();
    return false;
  }

  $("#dreno_span").hide();

  $("select[name='dreno_sn']").change(function () {
    var X = $("select[name='dreno_sn'] option:selected").val();

    if(X == 's') {
      $("#dreno_span").show();
      $("input[name='dreno']").focus();
    } else {
      $("#dreno_span").hide();
      $("input[name='dreno']").val('');
    }
  });


  $("#add-dreno").click(function () {
    var x = $("input[name='dreno']").val();

    var bexcluir = "<img src='../utils/delete_16x16.png' class='del-dreno-ativos' title='Excluir' border='0' >";
    $('#dreno').append("<tr><td  class='dreno1' name='" + x + "' > " + bexcluir + " <b>Local: </b>" + x + "</td></tr>");
    $("input[name='dreno']").val("");


    $("input[name='dreno']").focus();
    atualiza_dreno_ativos();
    return false;

  });

  $(".del-dreno-ativos").unbind("click", remove_dreno_ativos).bind("click", remove_dreno_ativos);

  function atualiza_dreno_ativos() {
    $(".del-dreno-ativos").unbind("click", remove_dreno_ativos)
      .bind("click", remove_dreno_ativos);
  }

  function remove_dreno_ativos() {
    $(this).parent().parent().remove();
    return false;
  }

  // //////cateter
  $("#add-cateter").click(function () {
    var x = $("input[name='cateter']").val();

    var bexcluir = "<img src='../utils/delete_16x16.png' class='del-cateter-ativos' title='Excluir' border='0' >";
    $('#cateter').append("<tr><td  class='cateter1' name='" + x + "' colspan='3'> <input type=hidden class='cat_sond1' name='" + x + "'/>" + bexcluir + " " + x + "</td></tr>");
    $("input[name='cateter']").val("");


    $("input[name='cateter']").focus();
    atualiza_cateter_ativos();
    return false;

  });

  $(".del-cateter-ativos").unbind("click", remove_cateter_ativos).bind("click", remove_cateter_ativos);

  function atualiza_cateter_ativos() {
    $(".del-cateter-ativos").unbind("click", remove_cateter_ativos)
      .bind("click", remove_cateter_ativos);
  }

  function remove_cateter_ativos() {
    $(this).parent().parent().remove();
    return false;
  }

  $("#dialog-validar-radio").dialog({
    autoOpen: false,
    modal: true,
    buttons: {
      "Fechar": function () {

        $(this).dialog("close");

      }

    }
  });

  /*$(".modalidade").change(function () {
    if($(this).val() == '1') {
      $(".alertas_sinais_vitais").each(function () {
        $(this).attr('disabled', 'disabled');
        $(this).val('');
        $(this).removeClass('OBG');
      });
      $(".alertas_radio").each(function () {
        $(this).attr('disabled', 'disabled');
        $(this).removeClass('OBG');
        $(this).attr('checked', false);
      });
      $(".glicemia_capilar").each(function () {
        $(this).attr('disabled', 'disabled');
        $(this).removeClass('OBG');
      });
      $(".oximetria_pulso").each(function () {
        $(this).attr('disabled', 'disabled');
        $(this).removeClass('OBG');
      });
      $(".observacao_alertas").each(function () {
        $(this).attr('disabled', 'disabled');
        $(this).val('');
      });
    } else {
      $(".alertas_sinais_vitais").each(function () {
        $(this).attr('disabled', '');
        $(this).addClass('OBG');
      });
      $(".alertas_radio").each(function () {
        $(this).attr('disabled', '');
      });
      $(".glicemia_capilar").each(function () {
        $(this).attr('disabled', '');
      });
      $(".oximetria_pulso").each(function () {
        $(this).attr('disabled', '');
      });
      $(".observacao_alertas").each(function () {
        $(this).attr('disabled', '');
      });
    }
  });*/

  $("#salvar-ficha-evolucao").click(function () {
    if(validar_campos("div-ficha-evolucao-medica")) {
      var dados = $("form").serialize();
      var impressao = $("textarea[name='impressao']").val();

      $(".observacao_alertas").each(function () {
        var observacao = $(this).attr('value');
        impressao += " " + observacao;
      });

      var probativos = "";
      var novosprobativos = "";
      var exames = "";
      var cat_sond = '';
      var dor123 = "";
      var total = $(".prob-ativos-evolucao").size();
      var total2 = $(".probativo1").size();
      var total3 = $(".exame").size();
      var total4 = $(".dor1").size();
      var mod_home_care = $(".modalidade:checked").val();
      var score = '';
      var flag = 0;
      var data = $("#data-evolucao").val();
      var hora = $("#hora-evolucao").val();

      $(".exame1").each(function (i) {
        var val = $(this).parent().children('td').eq(1).children('input.exame:checked').val();
        var id = $(this).attr("idexame");
        // var val = $(this).val();
        //var obs  = $('.obsexame'+id+'').val();
        var obs = $('textarea#obsexame' + id).val();
        if(val == '' || val == undefined) {
          //alert('exames');
          flag = 1;
        }

        exames += id + "#" + val + "#" + obs;

        if(i < total3 - 1)
          exames += "$";

      });

      var idpaciente = $("input[name='paciente']").val();

      $(".cat_sond").each(function (i) {
        if($(this).is(':checked')) {
          var cat = $(this).attr('name');
          cat_sond += cat + "#";

        }

      });
      $(".cat_sond1").each(function (i) {

        var cat = $(this).attr('name');
        cat_sond += cat + "#";

      });

      if(!$("input[name='escleras']").is(":checked")) {
        flag = 1;
      }

      if(!$("input[name='respiratorio']").is(":checked")) {
        flag = 1;
      }

      if(!$("input[name='mucosa']").is(":checked")) {
        flag = 1;
      }

      if(!$("input[name='estd_geral']").is(":checked")) {
        flag = 1;
      }

      $(".dor1").each(function (i) {

        var nome = $(this).attr('name');
        var padrao_id = $(this).children('select[name=padrao_dor]').val();
        var padrao = $(this).children('select[name=padraodor]').find('option').filter(':selected').text();
        var escala = $("select[name='dor" + nome + "'] option:selected").val();
        ///05/08
        if(padrao_id == 5) {
          padrao = $(this).children('input[name=padraodor]').val();
        }
        dor123 += nome + "#" + padrao_id + "#" + padrao + "#" + escala;
        if(i < total4 - 1) {
          dor123 += "$";
        }

      });

      $(".prob-ativos-evolucao").each(function (i) {
        var cid = $(this).attr("cod");
        var desc = $(this).attr("desc");
        var obs = $(this).parent().children('td').children('textarea').val();
        novosprobativos += cid + "#" + desc + "#" + obs;
        if(i < total - 1)
          novosprobativos += "$";
      });

      var medicamentos_alergia_ativos = "";
      var medicamentos_alergia_inativos = "";
      var total_medicamentos_alergia = $(".medicamentos-alergia").size();

      $(".medicamentos-alergia[inativo='false']").each(function (i) {
        var cod = $(this).attr("cod");
        var catalogo_id = $(this).attr("catalogo_id");
        var desc = $(this).attr("desc");
        medicamentos_alergia_ativos += cod + "#" + desc + "#" + catalogo_id;
        if(i < total_medicamentos_alergia - 1)
          medicamentos_alergia_ativos += "$";
      });

      $(".medicamentos-alergia[inativo='true']").each(function (i) {
        var cod = $(this).attr("cod");
        var catalogo_id = $(this).attr("catalogo_id");
        var desc = $(this).attr("desc");
        medicamentos_alergia_inativos += cod + "#" + desc + "#" + catalogo_id;
        if(i < total_medicamentos_alergia - 1)
          medicamentos_alergia_inativos += "$";
      });

      var y1 = 1;

      $(".probativo1").each(function (i) {

        var tipo = $(this).parent().children('td').eq(1).children('input.probativo:checked').val();
        var cid = $(this).attr("cid");
        var desc = $(this).attr("prob_atv");
        var x = $(this).attr("x");

        if(tipo == '' || tipo == undefined || tipo == "NaN" || tipo == "NaN1") {

          flag = 1;
        }

        var obs = $('.obs' + x + '').val();
        y1++;
        probativos += cid + "#" + desc + "#" + tipo + "#" + obs;
        if(i < total2 - 1)
          probativos += "$";
      });

      var capmed = "query=nova-ficha-evolucao&" + dados + "&probativos=" + probativos + "&mod_home_care=" + mod_home_care + "&novosprobativos=" + novosprobativos + "&exames=" + exames + "&dor123=" + dor123 + "&cat_sond=" + cat_sond + "&impressao=" + impressao +
        "&medicamentos_alergia_ativos=" + medicamentos_alergia_ativos +
        "&medicamentos_alergia_inativos=" + medicamentos_alergia_inativos + "&data=" + data + "&hora=" + hora;
      if(flag == 0) {

        $.ajax({
          type: "POST",
          url: "query.php",
          data: capmed,
          success: function (msg) {
            if(msg == "1") {
              for (var key in localStorage){
                if(key.indexOf(document.location) > 0){
                  localStorage.removeItem(key);
                }
              }
              alert("SALVO COM SUCESSO!");
              window.location.href = '?view=paciente_med&act=listfichaevolucao&id=' + $("input[name='paciente']").val();
            } else {
              alert(msg);
            }
          }
        });
      } else {
        $("#dialog-validar-radio").dialog('open');
      }

    }
    return false;
  });

  $("#editar-ficha-evolucao").click(function () {
    if(validar_campos("div-ficha-evolucao-medica")) {
      var dados = $("form").serialize();
      var impressao = $("textarea[name='impressao']").val();

      $(".observacao_alertas").each(function () {
        var observacao = $(this).attr('value');
        impressao += " " + observacao;
      });

      var probativos = "";
      var novosprobativos = "";
      var exames = "";
      var cat_sond = '';
      var dor123 = "";
      var total = $(".prob-ativos-evolucao").size();
      var total2 = $(".probativo1").size();
      var total3 = $(".exame").size();
      var total4 = $(".dor1").size();
      var mod_home_care = $(".modalidade:checked").val();
      var score = '';
      var flag = 0;
      var data = $("#data-evolucao").val();
      var hora = $("#hora-evolucao").val();

      $(".exame1").each(function (i) {
        var val = $(this).parent().children('td').eq(1).children('input.exame:checked').val();
        var id = $(this).attr("idexame");
        // var val = $(this).val();
        //var obs  = $('.obsexame'+id+'').val();
        var obs = $('textarea#obsexame' + id).val();
        if(val == '' || val == undefined) {
          //alert('exames');
          flag = 1;
        }

        exames += id + "#" + val + "#" + obs;

        if(i < total3 - 1)
          exames += "$";

      });

      var idpaciente = $("input[name='paciente']").val();
      var idevolucao = $("input[name='idevolucao']").val();

      $(".cat_sond").each(function (i) {
        if($(this).is(':checked')) {
          var cat = $(this).attr('name');
          cat_sond += cat + "#";

        }

      });
      $(".cat_sond1").each(function (i) {

        var cat = $(this).attr('name');
        cat_sond += cat + "#";

      });

      if(!$("input[name='escleras']").is(":checked")) {
        flag = 1;
      }

      if(!$("input[name='respiratorio']").is(":checked")) {
        flag = 1;
      }

      if(!$("input[name='mucosa']").is(":checked")) {
        flag = 1;
      }

      if(!$("input[name='estd_geral']").is(":checked")) {
        flag = 1;
      }

      $(".dor1").each(function (i) {

        var nome = $(this).attr('name');
        var padrao_id = $(this).children('select[name=padrao_dor]').val();
        var padrao = $(this).children('select[name=padraodor]').find('option').filter(':selected').text();
        var escala = $("select[name='dor" + nome + "'] option:selected").val();
        ///05/08
        if(padrao_id == 5) {
          padrao = $(this).children('input[name=padraodor]').val();
        }
        dor123 += nome + "#" + padrao_id + "#" + padrao + "#" + escala;
        if(i < total4 - 1) {
          dor123 += "$";
        }

      });

      $(".prob-ativos-evolucao").each(function (i) {
        var cid = $(this).attr("cod");
        var desc = $(this).attr("desc");
        var obs = $(this).parent().children('td').children('textarea').val();
        novosprobativos += cid + "#" + desc + "#" + obs;
        if(i < total - 1)
          novosprobativos += "$";
      });

      var medicamentos_alergia_ativos = "";
      var medicamentos_alergia_inativos = "";
      var total_medicamentos_alergia = $(".medicamentos-alergia").size();

      $(".medicamentos-alergia[inativo='false']").each(function (i) {
        var cod = $(this).attr("cod");
        var catalogo_id = $(this).attr("catalogo_id");
        var desc = $(this).attr("desc");
        medicamentos_alergia_ativos += cod + "#" + desc + "#" + catalogo_id;
        if(i < total_medicamentos_alergia - 1)
          medicamentos_alergia_ativos += "$";
      });

      $(".medicamentos-alergia[inativo='true']").each(function (i) {
        var cod = $(this).attr("cod");
        var catalogo_id = $(this).attr("catalogo_id");
        var desc = $(this).attr("desc");
        medicamentos_alergia_inativos += cod + "#" + desc + "#" + catalogo_id;
        if(i < total_medicamentos_alergia - 1)
          medicamentos_alergia_inativos += "$";
      });

      var y1 = 1;

      $(".probativo1").each(function (i) {

        var tipo = $(this).parent().children('td').eq(1).children('input.probativo:checked').val();
        var cid = $(this).attr("cid");
        var desc = $(this).attr("prob_atv");
        var x = $(this).attr("x");

        if(tipo == '' || tipo == undefined || tipo == "NaN" || tipo == "NaN1") {

          flag = 1;
        }

        var obs = $('.obs' + x + '').val();
        y1++;
        probativos += cid + "#" + desc + "#" + tipo + "#" + obs;
        if(i < total2 - 1)
          probativos += "$";
      });

      var capmed = "query=editar-ficha-evolucao&" + dados + "&probativos=" + probativos + "&mod_home_care=" + mod_home_care + "&novosprobativos=" + novosprobativos + "&exames=" + exames + "&dor123=" + dor123 + "&cat_sond=" + cat_sond + "&impressao=" + impressao +
        "&medicamentos_alergia_ativos=" + medicamentos_alergia_ativos +
        "&medicamentos_alergia_inativos=" + medicamentos_alergia_inativos + "&data=" + data + "&hora=" + hora;
      if(flag == 0) {

        $.ajax({
          type: "POST",
          url: "query.php",
          data: capmed,
          success: function (msg) {
            if(msg == "1") {
              alert("SALVO COM SUCESSO!");
              //window.location.href = '?view=paciente_med&act=listfichaevolucao&id=' + $("input[name='paciente']").val();
            } else {
              alert(msg);
            }
          }
        });
      } else {
        $("#dialog-validar-radio").dialog('open');
      }

    }
    return false;
  });

  $("select[name='conduta']").change(function () {
    var opcao = $("select[name='conduta'] option:selected").val();

    if(opcao == 2) {
      $("#terapeutico").show();
      $("textarea[name='terapeutico']").val('');
      $("textarea[name='terapeutico']").focus();
      $("textarea[name='terapeutico']").addClass('OBG');
    } else {
      $("#terapeutico").hide();
      $("textarea[name='terapeutico']").removeClass("OBG");
    }
  });

  /*function validar_campos(div) {
    var ok = true;
    $("#" + div + " .OBG").each(function (i) {
      if(this.value == "") {
        ok = false;
        this.style.border = "3px solid red";
      } else {
        this.style.border = "";
      }
    });
    $("#" + div + " .COMBO_OBG option:selected").each(function (i) {
      if(this.value == "-1") {
        ok = false;
        $(this).parent().css({
          "border": "3px solid red"
        });
      } else {
        $(this).parent().css({
          "border": ""
        });
      }
    });

    $("#" + div + " .OBG_RADIO").each(function (i) {
      //if (this.value == "-1") {
      classe = $(this).attr("class").split(" ");
      classe = classe[0];
      classe_aux = "";
      aux = 0;

      if(classe != classe_aux) {
        classe_aux = classe;
        aux = 0;
        if($("." + classe).is(':checked')) {
          aux = 1;
        }

        if(aux == 0) {
          $("." + classe).parent().css("border", "3px solid red");

        } else {
          $("." + classe).parent().css("border", "");
        }
      } else {
        if($("." + classe).is(':checked')) {
          aux = 1;
        }

        if(aux == 0) {
          $("." + classe).parent().css("border", "3px solid red");
          alert("teste2");
          ok = false;
        } else {
          $("." + classe).parent().p.css("border", "");
          ok = true;
        }
      }
    });
    if(!ok) {
      $("#" + div + " .aviso").text("Os campos em vermelho sÃ£o obrigatÃ³rios!");
      $("#" + div + " .div-aviso").show();
    } else {
      $("#" + div + " .aviso").text("");
      $("#" + div + " .div-aviso").hide();
    }
    return ok;
  }*/

  $("input[name='busca-diagnostico']").autocomplete({
    source: "/utils/busca_cid.php",
    minLength: 3,
    select: function (event, ui) {
      $("input[name='busca-diagnostico']").val('');
      $("input[name='cid-diagnostico']").val(ui.item.id);
      $("input[name='diagnostico']").val(ui.item.name);
      $('#local').focus();
      $('#busca-diagnostico').val('');
      return false;
    },
  });

  $(".outro_ativo").hide();
  $("#outros-prob-ativos").click(function () {
    if($(this).is(":checked")) {

      $(".outro_ativo").show();
    } else {
      $(".outro_ativo").hide();
    }

  });

  $(".outro_ativo_evolucao").hide();
  $("#outros-prob-ativos-evolucao").click(function () {
    if($(this).is(":checked")) {

      $(".outro_ativo_evolucao").show();
    } else {
      $(".outro_ativo_evolucao").hide();
    }

  });

  /* Duplicado
   $("#add-prob-ativo").click(function(){
   var x = $("input[name='outro-prob-ativo']").val();

   console.log("x", x);

   var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prob-ativos' title='Excluir' border='0' >";
   $('#problemas_ativos').append("<tr><td colspan='2' class='prob-ativos' cod='1111' desc='"+x+"'> "+bexcluir+" "+x+"</td></tr>");
   $("input[name='busca-problemas-ativos']").val("");
   $("input[name='outro-prob-ativo']").val("");
   $("#outros-prob-ativos").attr('checked',false);
   $(".outro_ativo").hide();
   $("input[name='busca-problemas-ativos']").focus();
   //atualiza_rem_problemas_ativos();
   return false;


   }); */


  $("input[name='busca-problemas-ativos']").autocomplete({
    source: "/utils/busca_cid.php",
    minLength: 3,
    select: function (event, ui) {

      var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prob-ativos' title='Excluir' border='0' >";
      $('#problemas_ativos').append("<tr><td colspan='2' class='prob-ativos' cod='" + ui.item.id + "' desc='" + ui.item.name + "'> " + bexcluir + " " + ui.item.name + "</td></tr>");
      $("input[name='busca-problemas-ativos']").val("");
      $("input[name='busca-problemas-ativos']").focus();
      atualiza_rem_problemas_ativos();

      return false;
    },
  });

  $("#add-prob-ativo-evolucao").click(function () {
    var x = $("input[name='outro-prob-ativo-evolucao']").val();

    if(x == "") {

      $("input[name='outro-prob-ativo-evolucao']").css({
        "border": "3px solid red"
      });

    } else {
      $("input[name='outro-prob-ativo-evolucao']").css({
        "border": ""
      });


      var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prob-ativos' title='Excluir' border='0' >";
      $('#problemas_ativos').append("<tr><td colspan='2' class='prob-ativos-evolucao' cod='1111' desc='" + x + "'> " + bexcluir + " " + x + " <textarea cols=70 rows=2  class='OBG' ></textarea></td></tr>");
      $("input[name='busca-problemas-ativos-evolucao']").val("");
      $("input[name='outro-prob-ativo-evolucao']").val("");
      $("#outros-prob-ativos-evolucao").attr('checked', false);
      $(".outro_ativo").hide();
      $("input[name='busca-problemas-ativos-evolucao']").focus();
      atualiza_rem_problemas_ativos_evolucao();

    }
    return false;


  });



  $("input[name='busca-problemas-ativos-evolucao']").autocomplete({
    source: "/utils/busca_cid.php",
    minLength: 3,
    select: function (event, ui) {

      var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prob-ativos' title='Excluir' border='0' >";
      $('#problemas_ativos').append("<tr><td colspan='2' class='prob-ativos-evolucao' cod='" + ui.item.id + "' desc='" + ui.item.name + "'> " + bexcluir + " " + ui.item.name + " <textarea cols=70 rows=3  class='OBG' ></textarea></td></tr>");
      $("input[name='busca-problemas-ativos-evolucao']").val("");
      $("input[name='busca-problemas-ativos-evolucao']").focus();
      atualiza_rem_problemas_ativos_evolucao();

      return false;
    },
  });
  /* ############## equipamento capmed/////////// */


  $(".qtd_equipamento").keyup(function () {
    var valor = $(this).val().replace(/[^0-9]/g, '');
    $(this).val(valor);
  });

  $(".equipamento").click(function () {
    x = $(this).attr('cod_equipamento');

    if($(this).is(":checked")) {
      $("#obs" + x).attr('readonly', false);
      $("#obs" + x).focus();
      $("#qtd" + x).attr('readonly', false);
      $("#qtd" + x).addClass("OBG");
      $("#qtd" + x).attr('value', '1');

    } else {
      $("#obs" + x).attr('readonly', true);
      $("#qtd" + x).attr('readonly', true);
      $("#qtd" + x).removeClass("OBG");
      $("#qtd" + x).removeAttr("style");
      $("#qtd" + x).attr('value', '');
    }

  });

  // ////////editar prescricao/////////////
  $("#editar_ficha").click(function () {
    var cont_internacao = 0;
    $('.item_internacao').each(function () {
      cont_internacao = cont_internacao + 1;

    });
    if(cont_internacao > 0) {
      $('.internacao_add').removeClass("OBG");
    }

    //if(validar_campos("div-ficha-medica")) {
      var dados = $("form").serialize();
      var probativos = "";
      var total = $(".prob-ativos").size();
      var j = 0;

      var medicamentos_alergia_ativos = "";
      var medicamentos_alergia_inativos = "";
      var total_medicamentos_alergia = $(".medicamentos-alergia").size();

      $(".medicamentos-alergia[inativo='false']").each(function (i) {
        var cod = $(this).attr("cod");
        var catalogo_id = $(this).attr("catalogo_id");
        var desc = $(this).attr("desc");
        medicamentos_alergia_ativos += cod + "#" + desc + "#" + catalogo_id;
        if(i < total_medicamentos_alergia - 1)
          medicamentos_alergia_ativos += "$";
      });

      $(".medicamentos-alergia[inativo='true']").each(function (i) {
        var cod = $(this).attr("cod");
        var catalogo_id = $(this).attr("catalogo_id");
        var desc = $(this).attr("desc");
        medicamentos_alergia_inativos += cod + "#" + desc + "#" + catalogo_id;
        if(i < total_medicamentos_alergia - 1)
          medicamentos_alergia_inativos += "$";
      });

      var score = '';
      $("input.abmid:checked").each(function (i) {
        score = score + $(this).attr('cod_item') + "#";

      });

      var score_nead = '';
      $("input.nead:checked").each(function (i) {
        score_nead = score_nead + $(this).attr('cod_item') + "#";

      });

      $("input.petro:checked").each(function (i) {
        score = score + $(this).attr('cod_item') + "#";

      });

      var score_katz = '';
      $("input.katz:checked").each(function (i) {
          score_katz += 'katz#' + $(this).attr('name') + '#' + $(this).val() + "$";
      });

      var score_nead2016 = '';
      $("input.grupo1:checked").each(function (i) {
          score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).val() + "$";
      });

      $("input.grupo2:checked").each(function (i) {
          score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).val() + "$";
      });

      $("input.grupo3:checked").each(function (i) {
          score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).attr('peso') + "$";
      });

      $(".prob-ativos").each(function (i) {
        var cid = $(this).attr("cod");
        var desc = $(this).attr("desc");
        probativos += cid + "#" + desc;
        if(i < total - 1)
          probativos += "$";
      });
      var equipamentos = '';
      var linha2 = '';

      var total2 = $(".equipamento").size();

      modalidade = $("input.modalidade:checked").val();
      if(modalidade == 2) {
        $("input.equipamento:checked").each(function (i) {
          var x = $(this).attr('cod_equipamento');
          linha2 = $(this).attr('cod_equipamento') + "#" + $("#obs" + x).val() + "#" + $("#qtd" + x).val();
          if(i < total2 - 1)
            linha2 += "$";
          equipamentos += linha2;
        });
      } else {
        $("input.equipamento:checked").each(function (i) {
          var x = $(this).attr('cod_equipamento');
          linha2 = $(this).parent().text() + "  Obs:" + $("#obs" + x).val() + " Qtd" + $("#qtd" + x).val();
          if(i < total2 - 1)
            linha2 += "\n";
          equipamentos += linha2;
        });
      }

      var totalcab = $(".cabec1").size();
      var linhax = '';
      var cab = '';
      $(".cabec1").each(function (i) {

        if($(this).val() != null && $(this).val() != '') {
          linhax = $(this).attr('cod_item') + "#" + $(this).val();

          if(i < totalcab - 1)
            linhax += "$";
          cab += linhax;
        }
      });

      var internacao = '';
      var total6 = $(".internacao").size();
      $(".internacao").each(function (i) {
        var motivo = $(this).attr("desc");
        if(i < total6 - 1)
          motivo += "$";
        internacao += motivo;
      });

      var alergia_alimento_ativos = alergiaAlimentarAtivos();
      var alergia_alimento_inativos = alergiaAlimentarInativos();

      var num_sugestao = $('input[name=num_sugestao]').val();

      var mod_home_care = $(".modalidade:checked").val();
      score_katz = score_katz.substring(0,(score_katz.length - 1));
      score_nead2016 = score_nead2016.substring(0,(score_nead2016.length - 1));

      var capmed = "query=editar-ficha-avaliacao&" + dados + "&probativos=" + probativos +
        "&score=" + score + "&equipamentos=" + equipamentos + "&mod_home_care=" + mod_home_care +
        "&cab_score=" + cab + "&medicamentos_alergia_ativos=" + medicamentos_alergia_ativos +
        "&medicamentos_alergia_inativos=" + medicamentos_alergia_inativos +
        "&internacoes=" + internacao + "&alergia_alimentar_ativos=" + alergia_alimento_ativos +
        "&alergia_alimentar_inativos=" + alergia_alimento_inativos + "&score_nead=" + score_nead +
        "&score_katz=" + score_katz + "&score_nead2016=" + score_nead2016 + "&num_sugestao=" + num_sugestao;
      $.ajax({
        type: "POST",
        url: "query.php",
        data: capmed,
        success: function (msg) {
          if(msg >= 0) {

            alert("Dados editados com sucesso! ");
            window.location.href = '?view=paciente_med&act=listcapmed&id=' + $("input[name='paciente']").val();

          } else {
            alert("DADOS: " + msg);
          }
        }
      });
   // }
    return false;
  });


  /* Scores q vï¿½o aparecer */


  $(".del-prob-ativos").unbind("click", remove_problemas_ativos).bind("click", remove_problemas_ativos);


  function atualiza_rem_problemas_ativos() {
    $(".del-prob-ativos").unbind("click", remove_problemas_ativos)
      .bind("click", remove_problemas_ativos);
  }

  function remove_problemas_ativos() {
    $(this).parent().parent().remove();
    return false;
  }



  $(".del-prob-ativos").unbind("click", remove_problemas_ativos_evolucao).bind("click", remove_problemas_ativos_evolucao);


  function atualiza_rem_problemas_ativos_evolucao() {
    $(".del-prob-ativos").unbind("click", remove_problemas_ativos_evolucao)
      .bind("click", remove_problemas_ativos_evolucao);
  }

  function remove_problemas_ativos_evolucao() {
    $(this).parent().parent().remove();
    return false;
  }

  $("input[name='palergiamed']").change(function () {
    if($(this).val() == "S" || $(this).val() == "s") {
      $("input[name='alergiamed']").addClass("OBG");
      $("input[name='alergiamed']").attr("readonly", "");
    } else {
      $("input[name='alergiamed']").removeClass("OBG");
      $("input[name='alergiamed']").attr("readonly", "readonly");
      $("input[name='alergiamed']").val("");
    }
  });

  $("input[name='palergiaalim']").change(function () {
    if($(this).val() == "S" || $(this).val() == "s") {
      $("input[name='alergiaalim']").addClass("OBG");
      $("input[name='alergiaalim']").attr("readonly", "");
    } else {
      $("input[name='alergiaalim']").removeClass("OBG");
      $("input[name='alergiaalim']").attr("readonly", "readonly");
      $("input[name='alergiaalim']").val("");
    }
  });

  $("input[name='qtdanteriores']").change(function () {
    if($(this).val() != "0" && $(this).val() != "") {
      $("input[name='historicointernacao']").addClass("OBG");
      $("input[name='historicointernacao']").attr("readonly", "");
    } else {
      $("input[name='historicointernacao']").removeClass("OBG");
      $("input[name='historicointernacao']").attr("readonly", "readonly");
      $("input[name='historicointernacao']").val("");
    }
  });

  $(".prelocohosp,.prehigihosp,.preconshosp,.prealimhosp,.preulcerahosp").click(function () {

    var score = 0;
    $(".prelocohosp:checked,.prehigihosp:checked,.preconshosp:checked,.prealimhosp:checked,.preulcerahosp:checked").each(function (i) {
      score = score + parseInt($(this).val());
    });
    $("input[name='shosp']").val(score.toString());
  });

  $(".prelocodomic,.prehigidomic,.preconsdomic,.prealimdomic,.preulceradomic").click(function () {
    var score = 0;
    $(".prelocodomic:checked,.prehigidomic:checked,.preconsdomic:checked,.prealimdomic:checked,.preulceradomic:checked").each(function (i) {
      score = score + parseInt($(this).val());
    });
    $("input[name='sdom']").val(score.toString());
  });

  $(".objlocomocao,.objhigiene,.objcons,.objalimento,.objulcera").click(function () {
    var score = 0;
    $(".objlocomocao:checked,.objhigiene:checked,.objcons:checked,.objalimento:checked,.objulcera:checked").each(function (i) {
      score = score + parseInt($(this).val());
    });
    $("input[name='sobj']").val(score.toString());
  });


  $(".limpar").click(function () {

    $(this).parent().children('input').attr('checked', false);
    var score = 0;
    var cont = 0;
    var texto = '';
    var j = 0;

    $("input.abmid:checked").each(function () {

      if($(this).val() == 5) {
        cont++;
      }
      score = score + parseInt($(this).val());

      j = score;

      if(j <= 7 && cont == 0) {
        texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente Não elegível para Internação Domiciliar.";
      } else if(j <= 7 && cont == 1) {
        texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Média Complexidade.";
      } else if(j >= 8 && j <= 12 && cont == 0) {
        texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Baixa Complexidade.";
      } else if(j >= 8 && j <= 12 && cont == 1) {
        texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Média Complexidade.";
      } else if(j >= 8 && j <= 12 && cont == 2) {
        texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Alta Complexidade.";
      } else if(j >= 13 && j <= 18 && cont < 2) {
        texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Média Complexidade.";
      } else if(j >= 13 && j <= 18 && cont >= 2) {
        texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Alta Complexidade.";
      } else if(j >= 19) {
        texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Alta Complexidade.";
      }
    });



    $("input[name='sabmid']").val(score.toString());
    $("input[name='sabmid1']").val(cont.toString());
    texto = texto + " " + $("#justificativa_nead").text();
    $("#prejustparecer").val(texto.toString());

  });


  $(".limpar2").click(function () {

    $(this).parent().children('input').attr('checked', false);
    var score = 0;
    var cont = 0;
    var texto = '';
    var j = 0;
    var x = 0;
    $("input.petro:checked").each(function (i) {

      if($(this).val() == 5) {
        cont++;
      }

      if($("input[name='supvent']:checked").val() == 5) {
        x = 1;

      }


      score = score + parseInt($(this).val());

      j = score;



      if(j <= 7 && cont == 0 && x != 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente Não elegível para Internação Domiciliar.";
      }
      else if(j <= 7 && cont == 0 && x == 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
      }
      else if(j <= 7 && cont == 1 && x != 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Média Complexidade.";
      }
      else if(j <= 7 && cont == 1 && x == 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
      }
      else if(j >= 8 && j <= 12 && cont == 0 && x != 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Baixa Complexidade.";
      }
      else if(j >= 8 && j <= 12 && cont == 0 && x == 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
      }
      else if(j >= 8 && j <= 12 && cont == 1 && x != 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Média Complexidade.";
      }
      else if(j >= 8 && j <= 12 && cont == 1 && x == 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
      }
      else if(j >= 8 && j <= 12 && cont == 2 && x != 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ".  Paciente de Alta Complexidade.";
      }
      else if(j >= 8 && j <= 12 && cont == 2 && x == 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
      }
      else if(j >= 13 && j <= 18 && cont == 0 && x != 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Média Complexidade.";
      }
      else if(j >= 13 && j <= 18 && cont == 0 && x == 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
      }
      else if(j >= 13 && j <= 18 && cont >= 2 && x != 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade.";
      }
      else if(j >= 13 && j <= 18 && cont >= 2 && x == 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
      }
      else if(j >= 19 && x != 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade.";
      }




    });
    $("input[name='spetro']").attr('value', score.toString());
    $("input[name='spetro1']").val(cont.toString());
    /* $("input[name='spetro2']").val(texto.toString()); */

    texto = texto + " " + $('#justificativa_nead').text();


    $("#prejustparecer").val(texto.toString());

  });

  $(".abmid").click(function () {
    var score = 0;
    var cont = 0;
    var texto = '';
    var j = 0;

    $("input.abmid:checked").each(function (i) {
      if($(this).val() == 5) {
        cont++;
      }

      score = score + parseInt($(this).val());

      j = score;

      if(j <= 7 && cont == 0) {
        texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente Não elegível para Internação Domiciliar.";
      } else if(j <= 7 && cont == 1) {
        texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Média Complexidade.";
      } else if(j >= 8 && j <= 12 && cont == 0) {
        texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Baixa Complexidade.";
      } else if(j >= 8 && j <= 12 && cont == 1) {
        texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Média Complexidade.";
      } else if(j >= 8 && j <= 12 && cont == 2) {
        texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Alta Complexidade.";
      } else if(j >= 13 && j <= 18 && cont < 2) {
        texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Média Complexidade.";
      } else if(j >= 13 && j <= 18 && cont >= 2) {
        texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Alta Complexidade.";
      } else if(j >= 19) {
        texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Alta Complexidade.";
      }
    });

    $(".observacao_score").text("  " + texto);

    $("input[name='sabmid']").val(score.toString());
    $("input[name='sabmid1']").val(cont.toString());


    texto = texto + " " + $("#parecer-nead2016").html();
    $("#prejustparecer").val(texto.toString());
  });

  $("#voltar").click(function () {
    history.go(-1);
  });

  $(".petro").click(function () {
    var score = 0;
    var cont = 0;
    var j = 0;
    var x = 0;
    var texto = "";

    $("input.petro:checked").each(function (i) {

      if($(this).val() == 5) {
        cont++;
      }

      if($("input[name='supvent']:checked").val() == 5) {
        x = 1;

      }
      //Ordem de prioridade:
      // ventilação mecânica, score e pontuaÃ§Ã£o. (Ana Paula)

      score = score + parseInt($(this).val());

      j = score;

      if(j <= 7 && cont == 0 && x != 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente Não elegível para Internação Domiciliar.";
      }
      else if(j <= 7 && cont == 0 && x == 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
      }
      else if(j <= 7 && cont == 1 && x != 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Média Complexidade.";
      }
      else if(j <= 7 && cont == 1 && x == 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
      }
      else if(j >= 8 && j <= 12 && cont == 0 && x != 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Baixa Complexidade.";
      }
      else if(j >= 8 && j <= 12 && cont == 0 && x == 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
      }
      else if(j >= 8 && j <= 12 && cont == 1 && x != 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Média Complexidade.";
      }
      else if(j >= 8 && j <= 12 && cont == 1 && x == 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
      }
      else if(j >= 8 && j <= 12 && cont == 2 && x != 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ".  Paciente de Alta Complexidade.";
      }
      else if(j >= 8 && j <= 12 && cont == 2 && x == 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
      }
      else if(j >= 13 && j <= 18 && cont == 0 && x != 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Média Complexidade.";
      }
      else if(j >= 13 && j <= 18 && cont == 0 && x == 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
      }
      else if(j >= 13 && j <= 18 && cont >= 2 && x != 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade.";
      }
      else if(j >= 13 && j <= 18 && cont >= 2 && x == 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
      }
      else if(j >= 19 && x != 1) {
        texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade.";
      }

    });
    $("input[name='spetro']").val(score.toString());
    $("input[name='spetro1']").val(cont.toString());
    /* $("input[name='spetro2']").val(texto.toString()); */

    $(".observacao_score").text("  " + texto);
    texto = texto + " " + $("#parecer-nead2016").html();

    $("#prejustparecer").val(texto.toString());
  });


  $("#novo_score").click(function () {
    var id = $("input[name='novo_score']").val();
    var convenio = $("input[name='convenio']").val();
    $("#dialog-escolha-score").attr('data-convenio', convenio);
    $("#dialog-escolha-score").dialog('open');

    ///  window.document.location.href = '?view=paciente_med&act=score_paciente&id=' + id + '';

  });

  $("#salvar_score").click(function () {

    if(validar_campos("div-score")) {
      var paciente = $("input[name='paciente']").val();
      var usuario = $("input[name='usuario']").val();
      var observacao = "";
      var justificativa = $(".observacao_score").html();

      var itens_score = '';
      $("input.abmid:checked").each(function (i) {
        itens_score = itens_score + $(this).attr('cod_item') + "#";
      });

      $("input.petro:checked").each(function (i) {
        itens_score = itens_score + $(this).attr('cod_item') + "#";

      });

      $("input.nead:checked").each(function (i) {
        itens_score = itens_score + $(this).attr('cod_item') + "#";
        justificativa = $("#justificativa_nead").html();

      });

      /*$(".cabec1").each(function(i){

       if ($(this).val() != '') {
       observacao = $(this).val() + " " + observacao;
       }

       });*/

      var totalcab = $(".cabec1").size();
      var linhax = '';
      var cab = '';
      $(".cabec1").each(function (i) {

        if($(this).val() != null && $(this).val() != '') {
          linhax = $(this).attr('cod_item') + "#" + $(this).val();

          if(i < totalcab - 1)
            linhax += "$";
          cab += linhax;

        }

      });

      var score_katz = '';
      $("input.katz:checked").each(function (i) {
        score_katz += 'katz#' + $(this).attr('name') + '#' + $(this).val() + "$";
      });

      var score_nead2016 = '';
      $("input.grupo1:checked").each(function (i) {
        score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).val() + "$";
      });

      $("input.grupo2:checked").each(function (i) {
        score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).val() + "$";
      });

      $("input.grupo3:checked").each(function (i) {
        score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).attr('peso') + "$";
      });

      var classificacaoNead =  $("input.classificacao-paciente:checked").val();

      var score = "query=novo-score&score=" + itens_score + "&paciente=" + paciente + "&usuario=" + usuario +
          "&observacao=" + cab + "&justificativa=" + justificativa + "&classificacaoNead="+classificacaoNead +
          "&score_nead2016="+score_nead2016+"&score_katz="+score_katz;


      $.ajax({
        type: "POST",
        url: "query.php",
        data: score,
        success: function (msg) {

          if(msg >= 1) {
            alert("Salvo com sucesso!!");
            window.location.href = '?view=paciente_med&act=listar';
          } else {
            alert("Falha ao salvar score: " + msg);
          }
        }

      });
    }
    return false;
  });

  $(".remover_score").click(function () {

    $("#dialog-remover-score").attr("cod", $(this).attr("cod"));
    $("#dialog-remover-score").attr("tipo", $(this).attr("tipo"));
    $("#dialog-remover-score").dialog("open");
  });

  $("#dialog-remover-score").dialog({
    autoOpen: false,
    modal: true,
    height: 250,
    width: 400,
    open: function (event, ui) {
      $("#dialog-remover-score").html("<br><br><center><b>Deseja remover este score?</center></b>");
    },
    buttons: {
      "Remover": function () {

        $.post("query.php", {
            query: "remover-score",
            cod: $("#dialog-remover-score").attr("cod"),
            tipo: $("#dialog-remover-score").attr("tipo")
          },
          function (r) {
            if(r == "1")
              location.reload();
            else
              alert("ERRO: Tente mais tarde!");
          });
        $(this).dialog("close");
      },
      "Cancelar": function () {
        $(this).dialog("close");
      }
    }
  });

  function alergiaAlimentarAtivos() {
    var alergia_alimento = "";
    var total_alergia_alimentar = $(".alimento").size();
    $(".alimento[inativo='false']").each(function (i) {
      var alimento = $(this).attr('desc');
      alergia_alimento += alimento;
      if(i < total_alergia_alimentar - 1)
        alergia_alimento += "$";
    });
    return alergia_alimento;
  };

  function alergiaAlimentarInativos() {
    var alergia_alimento = "";
    var total_alergia_alimentar = $(".alimento").size();
    $(".alimento[inativo='true']").each(function (i) {
      var alimento = $(this).attr('desc');
      alergia_alimento += alimento;
      if(i < total_alergia_alimentar - 1)
        alergia_alimento += "$";
    });
    return alergia_alimento;
  };

  $("#salvar-capmed").click(function () {
    var cont_internacao = 0;
    $('.item_internacao').each(function () {
      cont_internacao = cont_internacao + 1;
    });
    if(cont_internacao > 0) {
      $('.internacao_add').removeClass("OBG");
    }
    if(validar_campos("div-ficha-medica")) {
      var dados = $("form").serialize();
      var probativos = "";
      var total = $(".prob-ativos").size();

      var medicamentos_alergia_ativos = "";
      var medicamentos_alergia_inativos = "";
      var total_medicamentos_alergia = $(".medicamentos-alergia").size();

      $(".medicamentos-alergia[inativo='false']").each(function (i) {
        var cod = $(this).attr("cod");
        var catalogo_id = $(this).attr("catalogo_id");
        var desc = $(this).attr("desc");
        medicamentos_alergia_ativos += cod + "#" + desc + "#" + catalogo_id;
        if (i < total_medicamentos_alergia - 1)
          medicamentos_alergia_ativos += "$";
      });

      $(".medicamentos-alergia[inativo='true']").each(function (i) {
        var cod = $(this).attr("cod");
        var catalogo_id = $(this).attr("catalogo_id");
        var desc = $(this).attr("desc");
        medicamentos_alergia_inativos += cod + "#" + desc + "#" + catalogo_id;
        if (i < total_medicamentos_alergia - 1)
          medicamentos_alergia_inativos += "$";
      });

      var alergia_alimento_ativos = alergiaAlimentarAtivos();
      var alergia_alimento_inativos = alergiaAlimentarInativos();

      var score = '';
      $("input.abmid:checked").each(function (i) {
        score = score + $(this).attr('cod_item') + "#";
      });

      $("input.petro:checked").each(function (i) {
        score = score + $(this).attr('cod_item') + "#";
      });

      var score_nead = '';
      $("input.nead:checked").each(function (i) {
        score_nead = score_nead + $(this).attr('cod_item') + "#";
      });

      var score_nead7 = '';
      $("input.nead7:checked").each(function (i) {
        score_nead7 = score_nead7 + $(this).attr('cod_item') + "#";
      });

      var score_katz = '';
      $("input.katz:checked").each(function (i) {
        score_katz += 'katz#' + $(this).attr('name') + '#' + $(this).val() + "$";
      });

      var score_nead2016 = '';
      $("input.grupo1:checked").each(function (i) {
        score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).val() + "$";
      });

      $("input.grupo2:checked").each(function (i) {
        score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).val() + "$";
      });

      $("input.grupo3:checked").each(function (i) {
        score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).attr('peso') + "$";
      });

      $(".prob-ativos").each(function (i) {
        var cid = $(this).attr("cod");
        var desc = $(this).attr("desc");

        probativos += cid + "#" + desc;
        if (i < total - 1)
          probativos += "$";
      });

      var totalcab = $(".cabec1").size();
      var linhax = '';
      var cab = '';
      $(".cabec1").each(function (i) {

        if ($(this).val() != null && $(this).val() != '') {
          linhax = $(this).attr('cod_item') + "#" + $(this).val();
          if (i < totalcab - 1)
            linhax += "$";
          cab += linhax;

        }
      });
      var internacao = '';
      var total6 = $(".internacao").size();
      $(".internacao").each(function (i) {
        var motivo = $(this).attr("desc");
        if (i < total6 - 1)
          motivo += "$";
        internacao += motivo;
      });

      var mod_home_care = $(".modalidade:checked").val();
      score_katz = score_katz.substring(0, (score_katz.length - 1));
      score_nead2016 = score_nead2016.substring(0, (score_nead2016.length - 1));

      var capmed = "query=nova-capmed&" + dados + "&probativos=" + probativos +
          "&score=" + score + "&cab_score=" + cab + "&medicamentos_alergia_ativos=" + medicamentos_alergia_ativos +
          "&medicamentos_alergia_inativos=" + medicamentos_alergia_inativos + "&mod_home_care=" + mod_home_care +
          "&internacoes=" + internacao + "&alergia_alimentar_ativos=" + alergia_alimento_ativos +
          "&alergia_alimentar_inativos=" + alergia_alimento_inativos + "&score_nead=" + score_nead + "&score_katz=" + score_katz +
          "&score_nead2016=" + score_nead2016;
      $.ajax({
        type: "POST",
        url: "query.php",
        data: capmed,
        success: function (msg) {
          if (msg >= 0) {
            $("#capmed_ava").attr('idcap', msg);
            for (var key in localStorage) {
              if (key.indexOf(document.location) > 0) {
                localStorage.removeItem(key);
              }
            }
            var parecer = $("input[name='parecer']:checked").val();
            if (parecer != 2)
              $("#dialog-presc-avaliacao").dialog("open");
            else
              window.location.href = '/medico/?view=paciente_med&act=listcapmed&id=' + $("input[name='paciente']").val();

          } else {
            alert(msg);
          }
        }
      });
    }
    return false;
  });

  $("input[name='parecer']").change( function() {
    if($(this).val() == '2'){
      $(".modalidade").removeAttr("checked");
      $(".modalidade_homecare").hide();
    } else {
      $(".modalidade_homecare").show();
    }
  });


  $(".presc_avaliacao").click(function () {
    $("#dialog-presc-avaliacao").dialog("open");
    var val = $(this).attr('val');
    $("#capmed_ava").attr('idcap', val);
    //$("#tipoav").attr('value',tipo);


  });

  $("#presc_adt_avaliacao").click(function () {
    $("#dialog-presc-avaliacao-aditiva").dialog("open");
    //tipo= $(this).attr('tipo');

    var val = $(this).attr('id_cap');
    $("#capmed_ava").attr('idcap', val);



  });



  $(".editar_prescricao").click(function () {
    $("#dialog-editar-presc-avaliacao").dialog("open");
    paciente = $(this).attr('paciente');
    antigas = $(this).attr('antiga');
    idcap = $(this).attr('id_cap');
    criador = $(this).attr('criador');
    $("#edt").attr('edt', 1);
    $("#edtidcap").attr('value', idcap);
    $("#edt_idpresc").attr('value', antigas);
    $("#paciente").attr('value', paciente);
    $("#criador").attr('value', criador);

  });

  $("#dialog-editar-presc-avaliacao").dialog({
    autoOpen: false,
    modal: true,
    width: 400,
    close: function (event, ui)
    {
      window.location.href = '/medico/?view=paciente_med&act=listar';
    },
    open: function (event, ui) {

    }
  });

  $("#dialog-presc-avaliacao-aditiva").dialog({
    autoOpen: false,
    modal: true,
    width: 400,
    close: function (event, ui)
    {
      window.location.href = '/medico/?view=paciente_med&act=listar';
    },
    open: function (event, ui) {

    }
  });
  $("#iniciar_editar_pres_avaliacao").click(function () {

    //alert($("#edt_idpresc").val());

    if(validar_campos("dialog-editar-presc-avaliacao")) {
      window.location.href = '/medico/?view=prescricao-avaliacao&paciente=' + $("#paciente").val() +
      '&inicio=' + $("input[name='inicio']").val() + '&fim=' + $("input[name='fim']").val() + '&tipo=4&id_cap=' + $("#edtidcap").val() + '&antigas=' + $("#edt_idpresc").val() + '&edt=1&criador=' + $('#criador').val();;
    }
  });

  $("#sp_usar_ultima_prescricao").hide();

  $("#iniciar_pres_avaliacao").click(function () {

    var inicio = $("#dialog-presc-avaliacao input[name='inicio']").val();
    var fim = $("#dialog-presc-avaliacao input[name='fim']").val();

    inicio = inicio.split("/").reverse().join("");
    fim = fim.split("/").reverse().join("");

    var usar_ultima_prescricao = $("#usar_ultima_prescricao").is(":checked");

    if(fim < inicio) {
      alert("ERRO: O data final estÃ¡ menor do que a data inicial!");
      $("input[name='fim']").attr("style", "border: 3px solid red;");
    } else {
      $("input[name='fim']").removeAttr("style");

      if(validar_campos("dialog-presc-avaliacao")) {

        if(usar_ultima_prescricao) {
          //editar-prescricao-avaliacao
          window.location.href = '/medico/?view=editar-prescricao-avaliacao&paciente=' + $("#idpacav").val() +
          '&inicio=' + $("#dialog-presc-avaliacao input[name='inicio']").val() + '&fim=' + $("#dialog-presc-avaliacao input[name='fim']").val() +
          '&tipo= 4' + '&antigas=-1' + '&id_cap=' + $("#capmed_ava").attr('idcap') + '&ultima=' + usar_ultima_prescricao;;
        } else {
          window.location.href = '/medico/?view=prescricao-avaliacao&paciente=' + $("#idpacav").val() +
          '&inicio=' + $("#dialog-presc-avaliacao input[name='inicio']").val() + '&fim=' + $("#dialog-presc-avaliacao input[name='fim']").val() +
          '&tipo= 4' + '&antigas=-1' + '&id_cap=' + $("#capmed_ava").attr('idcap') + '&ultima=' + usar_ultima_prescricao;;
        }

      }
    }


  });


  $("#iniciar_pres_avaliacao_aditiva").click(function () {
    if(validar_campos("dialog-presc-avaliacao-aditiva")) {
      window.location.href = '/medico/?view=prescricao-avaliacao&paciente=' + $("#idpacav").val() +
      '&inicio=' + $("#dialog-presc-avaliacao-aditiva input[name='inicio']").val() + '&fim=' + $("#dialog-presc-avaliacao-aditiva input[name='fim']").val() + '&tipo=' + $("#tipoav").val() + '&antigas=-1' + '&id_cap=' + $("#capmed_ava").attr('idcap');;
    }
  });

  $("#dialog-presc-avaliacao").dialog({
    autoOpen: false,
    modal: true,
    width: 400,
    close: function (event, ui)
    {
      window.location.href = '/medico/?view=paciente_med&act=listar';
    },
    open: function (event, ui) {
      $("#sp_usar_ultima_prescricao").show();
    }


    /*
     * buttons: { "Iniciar" : function(){
     * window.location.href='/medico/?view=prescricao-avaliacao&paciente='+$("#idpacav").val()+
     * '&inicio='+$("input[name='inicio']").val()+'&fim='+$("input[name='fim']").val()+'&tipo='+$("#tipoav").val()+'&antigas=-1'; }
     *  }
     */
  });

  $(".nova_presc_avaliacao").click(function () {
    $("#dialog-novapresc").dialog("open");
    paciente = $(this).attr('paciente');
    antigas = $(this).attr('antiga');
    id_cap = $(this).attr('id_cap');
    tipo = $(this).attr('tipo');

    $("#nedt_idpresc").attr('value', antigas);
    $("#npaciente").attr('value', paciente);
    $("#ncapmed_ava").attr('value', id_cap);
    $("#tipoav").attr('value', tipo);

  });

  $("#iniciar_nova_presc_avaliacao").click(function () {
    if(validar_campos("dialog-novapresc")) {
      window.location.href = '/medico/?view=prescricao-avaliacao&paciente=' + $("#npaciente").val() + '&inicio=' + $("#dialog-novapresc input[name='inicio']").val() + '&fim=' + $("#dialog-novapresc input[name='fim']").val() + '&tipo=' + $("#tipoav").val() + '&antigas=' + $('#nedt_idpresc').val() + '&id_cap=' + $("#ncapmed_ava").val();;
    }
  });

  $("#dialog-novapresc").dialog({
    autoOpen: false,
    modal: true,
    width: 400,
    close: function (event, ui)
    {
      window.location.href = '/medico/?view=paciente_med&act=listar';
    },
    open: function (event, ui) {

    }



  });

  $(".statusp").click(function () {
    $("#dialog-status").attr("cod", $(this).attr("cod"));
    $("#dialog-status").dialog("open");
  });

  $("#dialog-status").dialog({
    autoOpen: false,
    modal: true,
    height: 350,
    width: 400,
    open: function (event, ui) {
      $.post("query.php", {
        query: "show-status-paciente",
        cod: $("#dialog-status").attr("cod")
      }, function (r) {
        $("#dialog-status").html(r);
      });
    },
    buttons: {
      Salvar: function () {
        var pstatus = $("#dialog-status input[name='status']:checked").attr("s");
        $.post("query.php", {
            query: "salvar-status-paciente",
            cod: $("#dialog-status").attr("cod"),
            pstatus: pstatus
          },
          function (r) {
            if(r == "1")
              window.document.location.href = '';
            else
              alert("ERRO: Tente mais tarde!");
          });
        $(this).dialog("close");
      },
      Cancelar: function () {
        $(this).dialog("close");
      }
    }
  });

  $('#novo_rel_alta').click(function () {
    var id = $(this).attr('paciente');
    window.location.href = '?view=relatorio&opcao=1&idp=' + id;
  });
  $('#novo_rel_prorrogacao').click(function () {
    var id = $(this).attr('paciente');
    window.location.href = '?view=relatorio&opcao=3&idp=' + id;
  });
  $('#novo_rel_encaminhamento').click(function () {
    var id = $(this).attr('paciente');
    window.location.href = '?view=relatorio&opcao=4&idp=' + id;
  });

  /////////////////////////////jeferson marques 2013-12-17 auto complet de associação de medicamento 
  $('.principio_ativo').live('click', function () {
    $(this).autocomplete({
      source: function (request, response) {
        $.getJSON('busca_principio.php', {
          term: request.term

        }, response);
      },
      minLength: 3,
      select: function (event, ui) {
        $(this).val(ui.item.principio);


      }

    });
  });

  $('#selPaciente').chosen({search_contains: true});

  /////////////////////////////////////fim 

  $(".nead").click(function () {

    var score_grupo1 = 0;
    var score_grupo2 = 0;
    var score_grupo3 = 0;
    var total_score_nead = 0;
    var justificativa = '';
    var val_score = '';
    $("input.nead:checked").each(function () {
      if($(this).attr('fator') == 1) {
        score_grupo1 = score_grupo1 + (parseInt($(this).val()) * 1);
      } else if($(this).attr('fator') == 2) {
        score_grupo2 = score_grupo2 + (parseInt($(this).val()) * 2);
      } else if($(this).attr('fator') == 3) {
        score_grupo3 = score_grupo3 + (parseInt($(this).val()) * 3);
      }


    });
    total_score_nead = score_grupo1 + score_grupo2 + score_grupo3;
    if(total_score_nead < 8) {
      justificativa = "Sem indicação para Internação Domiciliar.";
    } else if(total_score_nead >= 8 && total_score_nead <= 15) {
      justificativa = "Internação Domiciliar com visita de enfermagem.";
    } else if(total_score_nead >= 16 && total_score_nead <= 20) {
      justificativa = "Internação Domiciliar com até 6 horas de enfermagem.";
    } else if(total_score_nead >= 21 && total_score_nead <= 30) {
      justificativa = "Internação Domiciliar com até 12 horas de enfermagem.";
    } else if(total_score_nead > 30) {
      justificativa = "Internação Domiciliar com até 24 horas de enfermagem.";
    }
    val_score = "Score do paciente segundo tabela NEAD=" + total_score_nead + ". " + justificativa

    $("#grupo1").val(score_grupo1);
    $("#grupo2").val(score_grupo2);
    $("#grupo3").val(score_grupo3);
    $("#total_score_nead").val(total_score_nead);
    $("#justificativa_nead").text(val_score);
    var observacao_outro_score = $(".observacao_score").text() + " " + val_score;

    $("#prejustparecer").val(observacao_outro_score);

  });

  $(".nead7").click(function () {
    var total_score_nead = 0;
    var justificativa = '';
    var val_score = '';
    $("input.nead7:checked").each(function () {
      total_score_nead = total_score_nead + (parseInt($(this).val()) * 3);
    });

    if(total_score_nead < 6) {
      justificativa = "Programação de Alta em <input type='date' name='alta' size='10' />";
    } else if(total_score_nead >= 6 && total_score_nead <= 15) {
      justificativa = "Manter com até 06 horas ou visitas";
    } else if(total_score_nead >= 16 && total_score_nead <= 24) {
      justificativa = "Manter com até 12 horas";
    } else if(total_score_nead >= 26) {
      justificativa = "Manter com mais de 12 horas";
    }

    val_score = "Score do paciente segundo tabela NEAD<sup>7</sup> = " + total_score_nead + ". " + justificativa

    $("#total_score_nead").val(total_score_nead);
    $("#justificativa_nead").html(val_score);
  });

  $('.limpar_nead').click(function () {

    $(this).parent().children('input').attr('checked', false);
    var score_grupo1 = 0;
    var score_grupo2 = 0;
    var score_grupo3 = 0;
    var total_score_nead = 0;
    var justificativa = '';
    var val_score = '';
    var cont = 0;
    $("input.nead:checked").each(function () {
      cont++;
      if($(this).attr('fator') == 1) {
        score_grupo1 = score_grupo1 + (parseInt($(this).val()) * 1);
      } else if($(this).attr('fator') == 2) {
        score_grupo2 = score_grupo2 + (parseInt($(this).val()) * 2);
      } else if($(this).attr('fator') == 3) {
        score_grupo3 = score_grupo3 + (parseInt($(this).val()) * 3);
      }

    });
    total_score_nead = score_grupo1 + score_grupo2 + score_grupo3;
    if(total_score_nead < 8) {
      justificativa = "Sem indicação para Internação Domiciliar.";
    } else if(total_score_nead >= 8 && total_score_nead <= 15) {
      justificativa = "Internação Domiciliar com visita de enfermagem.";
    } else if(total_score_nead >= 16 && total_score_nead <= 20) {
      justificativa = "Internação Domiciliar com até 6 horas de enfermagem.";
    } else if(total_score_nead >= 21 && total_score_nead <= 30) {
      justificativa = "Internação Domiciliar com até 12 horas de enfermagem.";
    } else if(total_score_nead > 30) {
      justificativa = "Internação Domiciliar com até 24 horas de enfermagem.";
    }
    if(cont != 0) {
      val_score = "Score do paciente segundo tabela NEAD=" + total_score_nead + ". " + justificativa
    }
    $("#grupo1").val(score_grupo1);
    $("#grupo2").val(score_grupo2);
    $("#grupo3").val(score_grupo3);
    $("#total_score_nead").val(total_score_nead);
    $("#justificativa_nead").text(val_score);
    var observacao_outro_score = $(".observacao_score").text() + " " + val_score;
    $("#prejustparecer").val(observacao_outro_score);


  });

  $('.limpar_nead7').click(function () {
    $(this).parent().children('input').attr('checked', false);
    var justificativa = '';
    var val_score = '';
    $("input.nead:checked").each(function () {
      total_score_nead = total_score_nead + (parseInt($(this).val()) * 3);
    });
    if(total_score_nead < 6) {
      justificativa = "Programação de Alta em <input type='date' name='alta' size='10' />";
    } else if(total_score_nead >= 6 && total_score_nead <= 15) {
      justificativa = "Manter com até 06 horas ou visitas";
    } else if(total_score_nead >= 16 && total_score_nead <= 24) {
      justificativa = "Manter com até 12 horas";
    } else if(total_score_nead >= 26) {
      justificativa = "Manter com mais de 12 horas";
    }


    val_score = "Score do paciente segundo tabela NEAD<sup>7</sup> = " + total_score_nead + ". " + justificativa


    $("#total_score_nead").val(total_score_nead);
    $("#justificativa_nead").html(val_score);
  });



        $("#dialog-relatorios").dialog({
            autoOpen: false, modal: true, height: 250, width: 400,
            open: function(event, ui) {
                /*$.post("query.php",{query: "show-status-paciente", cod: $("#dialog-relatorios").attr("cod")},function(r){
                 $("#dialog-relatorios").html(r);
                 });*/

                $("#dialog-relatorios").html("<br><input type='radio' name='opcao_relatorio' value=1 checked/>Alta m&eacute;dica<br>" +
                "<input type='radio' name='opcao_relatorio' value=2 />Aditivo<br>" +
                "<input type='radio' name='opcao_relatorio' value=3 />Prorroga&ccedil;&atilde;o<br>" +
                "<input type='radio' name='opcao_relatorio' value=5 />Deflagrado<br>");
            },
            buttons: {
                Visualizar: function() {
                    var opcao_escolhida = $("#dialog-relatorios input[name='opcao_relatorio']:checked").val();
                    var id = $("#dialog-relatorios").attr("cod");

                    if (opcao_escolhida == 1) {
                        //window.open('imprimir_alta.php?id='+id+'&opcao='+opcao_escolhida,'_blank');?view=paciente_med&act=listcapmed&id=
                        window.location.href = '?view=relatorio&opcao=' + opcao_escolhida + '&id=' + id;
                    } else if (opcao_escolhida == 2) {
                        window.location.href = '?view=relatorio&opcao=' + opcao_escolhida + '&id=' + id;
                    } else if (opcao_escolhida == 3) {
                        window.location.href = '?view=relatorio&opcao=' + opcao_escolhida + '&id=' + id;
                    } else if (opcao_escolhida == 4) {
                        window.location.href = '?view=relatorio&opcao=' + opcao_escolhida + '&id=' + id;
                    }else if (opcao_escolhida == 5) {
                        window.location.href = '?view=relatorio&opcao=' + opcao_escolhida + '&id=' + id;
                    } else {
                        alert("Em fase de implementaÃ§Ã£o!");
                    }

                    $(this).dialog("close");
                },
                Cancelar: function() {

                    $(this).dialog("close");
                }
            }
        });
    /////////////////////////////jeferson marques 2013-12-17 auto complet de associação de medicamento


  $("#dialog-escolha-score").dialog({
    autoOpen: false,
    modal: true,
    height: 250,
    width: 400,
    open: function (event, ui) {
      var convenio = $(this).attr('data-convenio');
      var butao = "<input type='radio' class='tipo_score' checked='checked' name='tipo_score' value='3' id='score_nead'/>\n\
                             <label for='score_nead' >NEAD2016</label></br>";
      if(convenio == 7 || convenio == 27 || convenio == 28 || convenio == 29 || convenio == 80) {
        butao += "<input type='radio' class='tipo_score' name='tipo_score' value='1' id='score_petrobras'/>\n\
                           <label for='score_petrobras'>Petrobras</label>";
      }


      $("#dialog-escolha-score").html(butao);
    },
    buttons: {
      Iniciar: function () {
        var tipo_score = $("input[name='tipo_score']:checked").val();
        var id = $("input[name='novo_score']").val();
        window.document.location.href = '?view=paciente_med&act=score_paciente&id=' + id + '&tipo_score=' + tipo_score + '';

        $(this).dialog("close");
      },
      Cancelar: function () {
        $(this).dialog("close");
      }
    }
  });

    $('#salvar-rel-deflagrado').click(function(){
        if (validar_campos("relatorio-deflagrado")) {
            var problemaAtivo='';
            var qtdAtivo = $('.prob-ativos-deflagrado-medico').size();
            var paciente_id = $(this).attr("data-paciente");
            var dadosForm = $("form").serialize();
            var editar = $(this).attr('data-editar');
            $('.prob-ativos-deflagrado-medico').each(function(i){
                var cod = $(this).attr('cod');
                var descricao = $(this).attr('desc');
                problemaAtivo += cod + "#"+descricao;
                if(qtdAtivo -1 > i){
                    problemaAtivo +="$";
                }
            });



            var dados = "query=salvar-rel-deflagrado&" + dadosForm + "&paciente_id=" + paciente_id+"&problema_ativo="+problemaAtivo
                       +"&editar="+editar;

           $.ajax({
                type: "POST",
                url: "query.php",
                data: dados,
                success: function(msg) {

                    if (msg == 1) {
                        alert("Salvo com sucesso!!");
                        window.location.href = '?view=relatorio&opcao=5&id=' + paciente_id;
                    }else{
                        alert("DADOS: " + msg);
                    }
                }
            });
            return false
        }
        return false;
    });
    $('#novo-rel-deflagrado').click(function(){

        window.location.href =  '?view=novo-rel-deflagrado&id=' + $(this).attr('paciente-id');
    });
  $("input[name='busca']").autocomplete({
    source: function (request, response) {

      if($("#formulario").attr('tipo') == 14) {
        cod = '3';
      } else {
        cod = '0';
      }

      $.getJSON('busca_brasindice.php', {
        term: request.term,
        tipo: cod,
        tipo_busca: $('#tipo_busca').val(),
        somenteGenerico: $("#formulario").attr('somenteGenerico')


      }, response);
    },
    minLength: 3,
    select: function (event, ui) {
      $("input[name='busca']").val('');
      $('#nome').val(ui.item.value);
      $('#nome').attr('readonly', 'readonly');
      $('#cod').val(ui.item.id);
      $('#brasindice_id').val(ui.item.catalogo_id);
      $('#busca').val('');
      $("#formulario input[name='dose']").focus();
      return false;
    }
  });

  $("#tr_hospital_intercorrencia").hide();

  $("#terapia_intensiva_select").change(function(){
    if($(this).val() == 'S'){
      $("#tr_hospital_intercorrencia").show();
      $("#hospital_intercorrencia").addClass('OBG');
    }else{
      $("#tr_hospital_intercorrencia").hide();
      $("#hospital_intercorrencia").val('');
      $("#hospital_intercorrencia").removeClass('OBG');
      $("#hospital_intercorrencia").removeAttr('style');
    }

  });

  $("#dialog-presc-aditiva-intercorrencia").dialog({
    autoOpen: false,
    modal: true,
    width: 400,
    close: function (event, ui)
    {
      window.location.href = '/medico/?view=paciente_med&act=listar';
    },
    open: function (event, ui) {
      $("#sp_usar_ultima_prescricao").show();
    }
  });
  $("#iniciar-presc-aditiva-intercorrencia").click(function () {

   if(validar_campos("dialog-presc-aditiva-intercorrencia")) {
      window.location.href = "/medico/?view=prescricoes&paciente=" + $("#paciente").val() +
          "&inicio=" + $("input[name='inicio']").val() + "&fim=" + $("input[name='fim']").val()
          + "&tipo=3&intercorrencia="+ $("#intercorrencia").val();
    }
  });

  $('.container-motivo-ajuste').hide();
  $('#tipo-aditiva').change(function(){
      $('.container-motivo-ajuste').hide();
      $('#motivo-ajuste-aditivo').removeClass('OBG').val('');

     if($(this).val() == 'AJUSTE'){
         $('.container-motivo-ajuste').show();
         $('#motivo-ajuste-aditivo').addClass('OBG');

     }
  });

});

