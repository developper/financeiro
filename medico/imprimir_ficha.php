<?php

$id = session_id();
if(empty($id))
	session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
require __DIR__ . '/../vendor/autoload.php';
ini_set('memory_limit', '512M');
use \App\Controllers\Administracao as Administracao;
use \App\Models\Administracao\Filial;

class DadosFicha{

	public $usuario = ""; public $data = ""; public $pacienteid = ""; public $pacientenome = ""; public $motivo = "";
	public $alergiamed = ""; public $alergiaalim = ""; public $tipoalergiamed = ""; public $tipoalergiaalim = "";
	public $prelocohosp = "3"; public $prehigihosp = "2"; public $preconshosp = "3"; public $prealimhosp = "4"; public $preulcerahosp = "2";
	public $prelocodomic = "3"; public $prehigidomic = "2"; public $preconsdomic = "3"; public $prealimdomic = "4"; public $preulceradomic = "2";
	public $objlocomocao = "3"; public $objhigiene = "2"; public $objcons = "3"; public $objalimento = "4"; public $objulcera = "2";
	public $shosp = "14"; public $sdom = "14"; public $sobj = "14";
	public $modalidade="2";
	public $local_av="2";

	public $qtdinternacoes = ""; public $historico = "";
	public $parecer = "0"; public $justificativa = "";

	public $cancer = ""; public $psiquiatrico = ""; public $neuro = ""; public $glaucoma = "";
	public $hepatopatia = ""; public $obesidade = ""; public $cardiopatia = ""; public $dm = "";
	public $reumatologica = ""; public $has = ""; public $nefropatia = ""; public $pneumopatia = "";

	public $ativo;
	public $idcapmedica="";
	public $pre_justificativa="";
	public $plano_desmame="";
	public $usuarioid="";
}


class Paciente_imprimir_ficha{
	public function detalhes_capitacao_medica($id, $empresa){

		$html.= $id;
		$sql = "SELECT cap.*, emp.nome AS empresa, pds.nome AS plano, pac.nome as pnome,pac.idClientes,usr.nome as unome, usr.conselho_regional, usr.tipo as utipo, DATE_FORMAT(cap.data,'%d/%m/%Y') as fdata, comob.*
	FROM capmedica as cap
	LEFT OUTER JOIN comorbidades as comob ON cap.id = comob.capmed
	LEFT OUTER JOIN problemasativos as prob ON cap.id = prob.capmed
	LEFT OUTER JOIN usuarios as usr ON cap.usuario = usr.idUsuarios
	LEFT OUTER JOIN clientes as pac ON cap.paciente = pac.idClientes
	LEFT JOIN empresas AS emp ON cap.empresa = emp.id
	LEFT JOIN planosdesaude AS pds ON cap.plano = pds.id

	WHERE cap.id = '{$id}'";

		$d = new DadosFicha();

		$d->capmedica = $id;
		$result = mysql_query($sql);


		while($row = mysql_fetch_array($result)){
			$d->pacienteid= $row['idClientes'];
			$d->usuarioid= $row['usuario'];
			$d->empresa = $row['empresa'];
			$d->convenio = $row['plano'];
            $compTipo = $row['conselho_regional'];

			$d->usuario = ucwords(strtolower($row["unome"])) . $compTipo; $d->data = $row['fdata']; $d->pacientenome = ucwords(strtolower($row["pnome"]));
			$d->motivo = $row['motivo'];
			$d->alergiamed = $row['alergiamed']; $d->alergiaalim = $row['alergiaalim'];
			$d->tipoalergiamed = $row['tipoalergiamed']; $d->tipoalergiaalim = $row['tipoalergiaalim'];
			$d->prelocohosp = $row['hosplocomocao']; $d->prehigihosp = $row['hosphigiene'];
			$d->preconshosp = $row['hospcons']; $d->prealimhosp = $row['hospalimentacao']; $d->preulcerahosp = $row['hospulcera'];
			$d->prelocodomic = $row['domlocomocao']; $d->prehigidomic = $row['domhigiene']; $d->preconsdomic = $row['domcons'];
			$d->prealimdomic = $row['domalimentacao']; $d->preulceradomic = $row['domulcera'];
			$d->objlocomocao = $row['objlocomocao']; $d->objhigiene = $row['objhigiene']; $d->objcons = $row['objcons'];
			$d->objalimento = $row['objalimentacao']; $d->objulcera = $row['objulcera'];
			$d->shosp = $d->prelocohosp + $d->prehigihosp +	$d->preconshosp + $d->prealimhosp + $d->preulcerahosp;
			$d->sdom = $d->prelocodomic + $d->prehigidomic +	$d->preconsdomic + $d->prealimdomic + $d->preulceradomic;;
			$d->sobj = $d->objlocomocao + $d->objhigiene + $d->objcons + $d->objalimento + $d->objulcera;

			$d->qtdinternacoes = $row['qtdinternacoes']; $d->historico = $row['historicointernacao'];
			$d->parecer = $row['parecer']; $d->justificativa = $row['justificativa'];

			$d->cancer = $row['cancer']; $d->psiquiatrico = $row['psiquiatrico']; $d->neuro = $row['neuro']; $d->glaucoma = $row['glaucoma'];
			$d->hepatopatia = $row['hepatopatia']; $d->obesidade = $row['obesidade']; $d->cardiopatia = $row['cardiopatia']; $d->dm = $row['dm'];
			$d->reumatologica = $row['reumatologica']; $d->has = $row['has']; $d->nefropatia = $row['nefropatia']; $d->pneumopatia = $row['pneumopatia'];
			$d->modalidade = $row['MODALIDADE']; $d->local_av = $row['LOCAL_AV']; $d->pre_justificativa = $row['PRE_JUSTIFICATIVA']; $d->plano_desmame = $row['PLANO_DESMAME'];

		}

		$sql = "SELECT p.DESCRICAO as nome FROM problemasativos as p  WHERE  p.capmed = '{$id}' ";
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result)){
			$d->ativos[] = $row['nome'];
		}

		$this->capitacao_medica($d,"readonly",$empresa);
	}


	public function cabecalho($dados, $empresa = null, $convenio = null){
		if(isset($dados->pacienteid))
			$id = $dados->pacienteid;
		else
			$id = $dados->pacienteid;
		$condp1= "c.idClientes = '{$id}'";
		$sql2 = "SELECT
	UPPER(c.nome) AS paciente,
	(CASE c.sexo
	WHEN '0' THEN 'Masculino'
	WHEN '1' THEN 'Feminino'
	END) AS nomesexo,
	FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
	e.nome as empresan,
	c.`nascimento`,
	p.nome as Convenio,p.id,c.*,(CASE c.acompSolicitante
	WHEN 's' THEN 'Sim'
	WHEN 'n' THEN 'N&atilde;o'
	END) AS acomp,
	NUM_MATRICULA_CONVENIO,
	cpf
	FROM
	clientes AS c LEFT JOIN
	empresas AS e ON (e.id = c.empresa) INNER JOIN
	planosdesaude as p ON (p.id = c.convenio)
	WHERE
	{$condp1}
	ORDER BY
	c.nome DESC LIMIT 1";

		$result = mysql_query($sql);
		$result2 = mysql_query($sql2);
		$html.= "<table class='mytable' style='width:100%'>";
		while($pessoa = mysql_fetch_array($result2)){
			foreach($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);
			}

            $pessoa['empresan'] = !empty($empresa) ? $empresa : $pessoa['empresan'];
            $pessoa['Convenio'] = !empty($convenio) ? $convenio : $pessoa['Convenio'];

			$html.= "<tr bgcolor='#EEEEEE'>";
			$html.= "<td><h1 class='relatorio-texto'><b>PACIENTE:</b></h1></td>";
			$html.= "<td><h1 class='relatorio-texto'><b>SEXO </b></h1></td>";
			$html.= "<td><h1 class='relatorio-texto'><b>IDADE:</b></h1></td>";
			$html.= "</tr>";
			$html.= "<tr>";
			$html.= "<td><h1 class='relatorio-texto'>{$pessoa['paciente']}</h1></td>";
			$html.= "<td><h1 class='relatorio-texto'>{$pessoa['nomesexo']}</h1></td>";
			$html.= "<td><h1 class='relatorio-texto'>".join("/",array_reverse(explode("-",$pessoa['nascimento']))).' ('.$pessoa['idade']." anos)</h1></td>";
			$html.= "</tr>";

			$html.= "<tr bgcolor='#EEEEEE'>";

			$html.= "<td><h1 class='relatorio-texto'><b>UNIDADE REGIONAL:</b></h1></td>";
			$html.= "<td><h1 class='relatorio-texto'><b>CONV&Ecirc;NIO </b></h1></td>";
			$html.= "<td><h1 class='relatorio-texto'><b>MATR&Iacute;CULA </b></h1></td>";
			$html.= "</tr>";
			$html.= "<tr>";

			$html.= "<td><h1 class='relatorio-texto'>{$pessoa['empresan']}</h1></td>";
			$html.= "<td><h1 class='relatorio-texto'>".strtoupper($pessoa['Convenio'])."</h1></td>";
			$html.= "<td><h1 class='relatorio-texto'>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</h1></td>";
			$html.= "</tr>";

			$html.= "<tr bgcolor='#EEEEEE'>";
			$html.= "<td><h1 class='relatorio-texto'><b>M&Eacute;DICO ASSISTENTE:</b></h1></td>";
			$html.= "<td><h1 class='relatorio-texto'><b>ESPECIALIDADE:</b></h1></td>";
			$html.= "<td><h1 class='relatorio-texto'><b>ACOMPANHARÁ O PACIENTE (Sim ou N&atilde;o)</b></h1></td>";
			$html.= "</tr>";
			$html.= "<tr>";
			$html.= "<td><h1 class='relatorio-texto'>{$pessoa['medicoSolicitante']}</h1></td>";
			$html.= "<td><h1 class='relatorio-texto'>{$pessoa['espSolicitante']}</h1></td>";
			$html.= "<td><h1 class='relatorio-texto'>{$pessoa['acomp']}</h1></td>";
			$html.= "</tr>";
			$html.= "<tr bgcolor='#EEEEEE'>";
			$html.= "<td><h1 class='relatorio-texto'><b>UNIDADE DE " . htmlentities("Internação") . "/HOSPITAL</b></h1></td>";
			$html.= "<td><h1 class='relatorio-texto'><b>TELEFONE:</b></h1></td>";
			$html.= "<td><h1 class='relatorio-texto'><b>TELEFONE RESPONSÁVEL:</b></h1></td>";
			$html.= "</tr>";
			$html.= "<tr>";
			$html.= "<td><h1 class='relatorio-texto'>{$pessoa['localInternacao']}</h1></td>";
			$html.= "<td><h1 class='relatorio-texto'>{$pessoa['TEL_DOMICILIAR']}</h1></td>";
			$html.= "<td><h1 class='relatorio-texto'>{$pessoa['TEL_RESPONSAVEL']}</h1></td>";
			$html.= "</tr>";
			$html.= " <tr>
                          <td>
                            <h1 class='relatorio-texto'> <b>CPF:</b>{$pessoa['cpf']}</h1>
                          </td>
                     </tr>";



			$this->plan =  $pessoa['id'];
		}
		$html.= "</table>";

		return $html;
	}

	public function cond_pre_ihosp($dados){

		$html.= "<tr style='width:100% ' bgcolor='#D3D3D3'>";
		//$score = "SCORE<input type='text' size='2' name='shosp' value='{$dados->shosp}' {$acesso} />";
		$html.= "<td colspan='3'><h1 class='relatorio-texto'><b>" . htmlentities("CONDIÇÕES DO PACIENTE PRÉ INTERNAÇÃO") . " HOSPITALAR. Score = {$dados->shosp}. </b></h1></td>";
		$html.= "</tr>";
		$html.= "<tr><td colspan='3' ><h1 class='relatorio-texto'><b>" . htmlentities("Locomoção") . ":</b>";
		if($dados->prelocohosp=='3'){
			$html.= " &nbsp; <b>(x)</b> Deambulava&nbsp;&nbsp;";
			$html.= " <b>( )</b>Locomovia-se com " . htmlentities("auxílio") . " (Muletas ou Cadeiras)&nbsp;&nbsp;";
			$html.= " <b>( )</b>Restrito ao leito";
			$html.= "</h1></td></tr>";
		}
		if($dados->prelocohosp=='2'){
			$html.= " &nbsp; <b>( )</b> Deambulava&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Locomovia-se com " . htmlentities("auxílio") . " (Muletas ou Cadeiras)&nbsp;&nbsp;";
			$html.= " <b>( )</b>Restrito ao leito";
			$html.= "</h1></td></tr>";
		}
		if($dados->prelocohosp=='1'){

			$html.= " &nbsp; <b>( )</b> Deambulava&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Locomovia-se com " . htmlentities("auxílio") . " (Muletas ou Cadeiras)&nbsp;&nbsp;";
			$html.= " <b>( )</b>Restrito ao leito";
			$html.= "</h1></td></tr>";
		}
		$html.= "<tr><td colspan='3'><h1 class='relatorio-texto'><b>Autonomia para higiene pessoal:</b>";
		if($dados->prehigihosp=='2'){
			$html.= " &nbsp; <b>(x)</b>Sim&nbsp;&nbsp;";
			$html.= "  <b>( )</b>N&atilde;o (Oral, " . htmlentities("dejeções, micção") . " e banho)";
			$html.= "</h1></td></tr>";
		}
		if($dados->prehigihosp=='1'){
			$html.= " &nbsp; <b>( )</b>Sim&nbsp;&nbsp;";
			$html.= "  <b>(x)</b>N&atilde;o (Oral, " . htmlentities("dejeções, micção") . " e banho)";
			$html.= "</h1></td></tr>";
		}

		$html.= "<tr><td colspan='3' ><h1 class='relatorio-texto'><b>" . htmlentities("Nível de Consciência") . ":</b>";
		if($dados->preconshosp=='3'){
			$html.= "&nbsp;<b>(x)</b>LOTE&nbsp;&nbsp;";
			$html.= "<b>( )</b>Desorientado&nbsp;&nbsp;";
			$html.= "<b>( )</b>Inconsciente";
			$html.= "</h1></td></tr>";
		}
		if($dados->preconshosp=='2'){
			$html.= "&nbsp;<b>( )</b>LOTE&nbsp;&nbsp;";
			$html.= "<b>(x)</b>Desorientado&nbsp;&nbsp;";
			$html.= "<b>( )</b>Inconsciente";
			$html.= "</h1></td></tr>";
		}


		if($dados->preconshosp=='1'){
			$html.= "&nbsp;<b>( )</b>LOTE&nbsp;&nbsp;";
			$html.= "<b>( )</b>Desorientado&nbsp;&nbsp;";
			$html.= "<b>(x)</b>Inconsciente";
			$html.= "</h1></td></tr>";
		}

		$html.= "<tr><td colspan='3'><h1 class='relatorio-texto'><b>" . htmlentities("Alimentação") . ":</b>";
		if($dados->prealimhosp=='4'){
			$html.= " &nbsp;<b>(x)</b>N&atilde;o assistida&nbsp;&nbsp;";
			$html.= " <b>( )</b>Assistida oral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Enteral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Parenteral";
			$html.= "</h1></td></tr>";
		}
		if($dados->prealimhosp=='3'){
			$html.= " &nbsp;<b>( )</b>N&atilde;o assistida&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Assistida oral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Enteral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Parenteral";
			$html.= "</h1></td></tr>";
		}


		if($dados->prealimhosp=='2'){
			$html.= " &nbsp;<b>( )</b>N&atilde;o assistida&nbsp;&nbsp;";
			$html.= " <b>( )</b>Assistida oral&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Enteral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Parenteral";
			$html.= "</h1></td></tr>";
		}

		if($dados->prealimhosp=='1'){
			$html.= " &nbsp;<b>( )</b>N&atilde;o assistida&nbsp;&nbsp;";
			$html.= " <b>( )</b>Assistida oral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Enteral&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Parenteral";
			$html.= "</h1></td></tr>";
		}
		$html.= "<tr><td colspan='3'><h1 class='relatorio-texto'><b>" . htmlentities("Úlceras de Pressão") . ":</b>";
		if($dados->preulcerahosp=='2'){
			$html.= " &nbsp;<b>(x)</b>N&atilde;o&nbsp;&nbsp;";
			$html.= "<b>( )</b>Sim";
			$html.= "</h1></td></tr>";
		}
		if($dados->preulcerahosp=='1'){
			$html.= " &nbsp;<b>( )</b>N&atilde;o&nbsp;&nbsp;";
			$html.= "<b>(x)</b>Sim";
			$html.= "</h1></td></tr>";

		}
		return $html;
	}

	public function cond_pre_id($dados){

		$html.= "<tr  style='width:100% 'bgcolor='#D3D3D3'>";
//$score = "SCORE<input type='text' readonly size='2' name='sdom' value='{$dados->sdom}' {$acesso} />";
		$html.= "<td  colspan='3'><h1 class='relatorio-texto'><b>" . htmlentities("CONDIÇÕES DO PACIENTE PRÉ INTERNAÇÃO") . " DOMICILIAR. Score = {$dados->sdom}. </b></h1></td>";
		$html.= "</tr>";
		$html.= "<tr><td colspan='3' ><h1 class='relatorio-texto'><b>" . htmlentities("Locomoção") . ":</b>";
		if($dados->prelocodomic == '3'){
			$html.= " &nbsp; <b>(x)</b>Deambulava&nbsp;&nbsp;";
			$html.= "<b>( )</b>Locomovia-se com " . htmlentities("Auxílio") . " (Muletas ou Cadeiras)&nbsp;&nbsp;";
			$html.= "<b>( )</b>Restrito ao leito";
			$html.= "</h1></td></tr>";
		}
		if($dados->prelocodomic == '2'){
			$html.= " &nbsp; <b>( )</b>Deambulava&nbsp;&nbsp;";
			$html.= "<b>(x)</b>Locomovia-se com " . htmlentities("Auxílio") . " (Muletas ou Cadeiras)&nbsp;&nbsp;";
			$html.= "<b>( )</b>Restrito ao leito";
			$html.= "</h1></td></tr>";
		}
		if($dados->prelocodomic == '1')	{
			$html.= " &nbsp; <b>( )</b>Deambulava&nbsp;&nbsp;";
			$html.= "<b>( )</b>Locomovia-se com " . htmlentities("Auxílio") . " (Muletas ou Cadeiras)&nbsp;&nbsp;";
			$html.= "<b>(x)</b>Restrito ao leito";
			$html.= "</h1></td></tr>";
		}

		$html.= "<tr><td colspan='3' ><h1 class='relatorio-texto'><b>Autonomia para higiene pessoal:</b>";
		if($dados->prehigidomic=='2'){
			$html.= "  &nbsp; <b>(x)</b>Sim &nbsp;&nbsp;";
			$html.= "  <b>( )</b>N&atilde;o (Oral, " . htmlentities("dejeções, micção") . " e banho)";
			$html.= "</h1></td></tr>";
		}
		if($dados->prehigidomic=='1'){
			$html.= "  &nbsp; <b>( )</b>Sim &nbsp;&nbsp;";
			$html.= "  <b>(x)</b>N&atilde;o (Oral, " . htmlentities("dejeções, micção") . " e banho)";
			$html.= "</h1></td></tr>";
			$html.= "<tr><td colspan='3' ><h1 class='relatorio-texto'><b>" . htmlentities("Nível de Consciência") . ":</b>";
		}
		if($dados->preconsdomic=='3'){
			$html.= " &nbsp; <b>(x)</b>LOTE&nbsp;&nbsp;";
			$html.= " <b>( )</b>Desorientado&nbsp;&nbsp;";
			$html.= " <b>( )</b>Inconsciente";
			$html.= "</h1></td></tr>";
		}
		if($dados->preconsdomic=='2'){
			$html.= " &nbsp; <b>( )</b>LOTE&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Desorientado&nbsp;&nbsp;";
			$html.= " <b>( )</b>Inconsciente";
			$html.= "</h1></td></tr>";
		}

		if($dados->preconsdomic=='1'){
			$html.= " &nbsp; <b>( )</b>LOTE&nbsp;&nbsp;";
			$html.= " <b>( )</b>Desorientado&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Inconsciente";
			$html.= "</h1></td></tr>";
		}

		$html.= "<tr><td colspan='3' ><h1 class='relatorio-texto'><b>" . htmlentities("Alimentação") . ":</b>";
		if($dados->prealimdomic=='4'){
			$html.= " &nbsp;<b>(x)</b>N&atilde;o assistida.&nbsp;&nbsp;";
			$html.= " <b>( )</b>Assistida oral.&nbsp;&nbsp;";
			$html.= " <b>( )</b>Enteral.&nbsp;&nbsp;";
			$html.= " <b>( )</b>Parenteral";
			$html.= "</h1></td></tr>";
		}
		if($dados->prealimdomic=='3'){
			$html.= " &nbsp;<b>( )</b>N&atilde;o assistida.&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Assistida oral.&nbsp;&nbsp;";
			$html.= " <b>( )</b>Enteral.&nbsp;&nbsp;";
			$html.= " <b>( )</b>Parenteral";
			$html.= "</h1></td></tr>";
		}
		if($dados->prealimdomic=='2'){
			$html.= " &nbsp;<b>( )</b>N&atilde;o assistida.&nbsp;&nbsp;";
			$html.= " <b>( )</b>Assistida oral.&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Enteral.&nbsp;&nbsp;";
			$html.= " <b>( )</b>Parenteral";
			$html.= "</h1></td></tr>";
		}
		if($dados->prealimdomic=='1'){
			$html.= " &nbsp;<b>( )</b>N&atilde;o assistida.&nbsp;&nbsp;";
			$html.= " <b>( )</b>Assistida oral.&nbsp;&nbsp;";
			$html.= " <b>( )</b>Enteral.&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Parenteral";
			$html.= "</h1></td></tr>";
		}

		$html.= "<tr><td colspan='3' ><h1 class='relatorio-texto'><b>" . htmlentities("Úlceras de Pressão") . ":</b>";
		if($dados->preulceradomic=='2'){
			$html.= "&nbsp;<b>(x)</b>N&atilde;o&nbsp;&nbsp;";
			$html.= " <b>( )</b>Sim";
			$html.= "</h1></td></tr>";
		}
		if($dados->preulceradomic=='1'){
			$html.= "&nbsp;<b>( )</b>N&atilde;o&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Sim";
			$html.= "</h1></td></tr>";
		}
		return $html;
	}

	public function cont_inter_domiciliar($dados){
		$html.= "<tr bgcolor='#D3D3D3'>";
		$html.= "<td colspan='3' ><h1 class='relatorio-texto'><b>OBJETIVO DA CONTINUIDADE DA " . htmlentities("INTERNAÇÃO") . " DOMICILIAR. Score = {$dados->sobj}. </b></h1></td>";
		$html.= "</tr>";

		$html.= "<tr><td colspan='3' ><h1 class='relatorio-texto'><b>" . htmlentities("Locomoção") . ":</b>";
		if($dados->objlocomocao == '3'){
			$html.= "&nbsp;<b>(x)</b>Deambular&nbsp;&nbsp;";
			$html.= "<b>( )</b>Locomover-se com " . htmlentities("Auxílio") . " (Muletas ou Cadeiras)&nbsp;&nbsp;";
			$html.= "<b>( )</b>Restrito ao leito";
			$html.= "</h1></td></tr>";
		}

		if($dados->objlocomocao == '2'){
			$html.= "&nbsp;<b>( )</b>Deambular&nbsp;&nbsp;";
			$html.= "<b>(x)</b>Locomover-se com " . htmlentities("Auxílio") . " (Muletas ou Cadeiras)&nbsp;&nbsp;";
			$html.= "<b>( )</b>Restrito ao leito";
			$html.= "</h1></td></tr>";
		}

		if($dados->objlocomocao == '1'){
			$html.= "&nbsp;<b>( )</b>Deambular&nbsp;&nbsp;";
			$html.= "<b>( )</b>Locomover-se com " . htmlentities("Auxílio") . " (Muletas ou Cadeiras)&nbsp;&nbsp;";
			$html.= "<b>(x)</b>Restrito ao leito";
			$html.= "</h1></td></tr>";
		}
		$html.= "<tr><td colspan='3' ><h1 class='relatorio-texto'><b>Autonomia para higiene pessoal:</b>";
		if($dados->objhigiene=='2'){
			$html.= " &nbsp;<b>(x)</b>Sim&nbsp;&nbsp;";
			$html.= " <b>( )</b>N&atilde;o (Oral, " . htmlentities("dejeções, micção") . " e banho)";
			$html.= "</h1></td></tr>";
		}
		if($dados->objhigiene=='1'){
			$html.= " &nbsp;<b>( )</b>Sim&nbsp;&nbsp;";
			$html.= " <b>(x)</b>N&atilde;o (Oral, " . htmlentities("dejeções, micção") . " e banho)";
			$html.= "</h1></td></tr>";
		}

		$html.= "<tr><td colspan='3' ><h1 class='relatorio-texto'><b>" . htmlentities("Nível de Consciência") . ":</b>";
		if($dados->objcons=='3'){
			$html.= " &nbsp;<b>(x)</b>LOTE&nbsp;&nbsp;";
			$html.= " <b>( )</b>Desorientado&nbsp;&nbsp;";
			$html.= " <b>( )</b>Inconsciente";
			$html.= "</h1></td></tr>";
		}

		if($dados->objcons=='2'){
			$html.= " &nbsp;<b>( )</b>LOTE&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Desorientado&nbsp;&nbsp;";
			$html.= " <b>( )</b>Inconsciente";
			$html.= "</h1></td></tr>";
		}
		if($dados->objcons=='1'){
			$html.= " &nbsp;<b>( )</b>LOTE&nbsp;&nbsp;";
			$html.= " <b>( )</b>Desorientado&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Inconsciente";
			$html.= "</h1></td></tr>";
		}

		$html.= "<tr><td colspan='2' ><h1 class='relatorio-texto'><b>" . htmlentities("Alimentação") . ":</b>";
		if($dados->objalimento=='4'){
			$html.= " &nbsp;<b>(x)</b>N&atilde;o assistida&nbsp;&nbsp;";
			$html.= " <b>( )</b>Assistida oral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Enteral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Parenteral";
			$html.= "</h1></td></tr>";
		}

		if($dados->objalimento=='3'){
			$html.= " &nbsp;<b>( )</b>N&atilde;o assistida&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Assistida oral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Enteral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Parenteral";
			$html.= "</h1></td></tr>";
		}
		if($dados->objalimento=='2'){
			$html.= " &nbsp;<b>( )</b>N&atilde;o assistida&nbsp;&nbsp;";
			$html.= " <b>( )</b>Assistida oral&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Enteral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Parenteral";
			$html.= "</h1></td></tr>";
		}
		if($dados->objalimento=='1'){
			$html.= " &nbsp;<b>( )</b>N&atilde;o assistida&nbsp;&nbsp;";
			$html.= " <b>( )</b>Assistida oral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Enteral&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Parenteral";
			$html.= "</h1></td></tr>";
		}

		$html.= "<tr><td colspan='3' ><h1 class='relatorio-texto'><b>" . htmlentities("Úlceras de Pressão") . ":</b>";
		if($dados->objulcera=='2'){
			$html.= " &nbsp;<b>(x)</b>N&atilde;o&nbsp;&nbsp;";
			$html.= " <b>( )</b>Sim";
			$html.= "</h1></td></tr>";
		}
		if($dados->objulcera=='1'){
			$html.= " &nbsp;<b>( )</b>N&atilde;o&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Sim";
			$html.= "</h1></td></tr>";
		}

		return $html;

	}

	public function equipamentos($id=null){
		$html .= "<table class='mytable' style='width:100%;'>";
		$html.= "<tr bgcolor='#D3D3D3'><td colspan='3' ><h1 class='relatorio-texto'><b>EQUIPAMENTOS</b></h1></td></tr>";

		$sql = "SELECT
	C.id,
	C.item,
	C.unidade,
	C.tipo,
	eqp.QTD,
	eqp.OBSERVACAO
	FROM
	equipamentos_capmedica as eqp INNER JOIN
	cobrancaplanos as C ON (C.id = eqp.COBRANCAPLANOS_ID)
	WHERE
	eqp.CAPMEDICA_ID = ".$id;

		$result = mysql_query($sql);
		$html.="<tr bgcolor='#D3D3D3'><td><h1 class='relatorio-texto'><b>ITEM</b></h1></td><td><h1 class='relatorio-texto'><b>" . htmlentities("OBSERVAÇÃO") . "</b></h1></td><td><h1 class='relatorio-texto'><b>QTD</b></h1></td></tr>";
		$cor = '';
		$contador = 0;
		while($row=mysql_fetch_array($result)){
			if(++$contador%2 == 0){
				$cor = "bgcolor='#EEEEEE'";
			}else{
				$cor = '';
			}
			$html.="<tr $cor><td><h1 class='relatorio-texto'>{$row['item']}</h1></td>
		<td style='width:33%'><h1 class='relatorio-texto'><label>{$row['OBSERVACAO']} </label></h1></td>
		<td><h1 class='relatorio-texto'><label>{$row['QTD']}</label></h1></td></tr>";
		}
		$html .= "</table>";
		return $html;
	}

	public function alergiaMedicamentosa($dados) {

		$html .= "<table class='mytable' style='width:100%'>";
		$html .= "<tr bgcolor='#D3D3D3'><td colspan='3'><h1 class='relatorio-texto'><b>ALERGIA MEDICAMENTOSA</b></h1></td></tr>";

		$sql = "SELECT DESCRICAO as medicamento FROM alergia_medicamentosa WHERE ID_PACIENTE = {$dados->pacienteid} AND NUMERO_TISS = 0 AND ATIVO='s'
	UNION
	SELECT DESCRICAO as medicamento FROM alergia_medicamentosa WHERE ID_PACIENTE = {$dados->pacienteid} AND ATIVO='s' AND NUMERO_TISS != 0
	group by NUMERO_TISS order by medicamento";

		$result = mysql_query($sql);
		$contador = 0;
		if(mysql_num_rows($result) > 0){
			while ($row = mysql_fetch_array($result)) {
				$medicamento = $row['medicamento'];
				$contador++;
				$html .= "<tr><td colspan='3'><h1 class='relatorio-texto'><b>Medicamento $contador:</b> $medicamento</h1></td></tr>";
			}
		}else{
			$html .= "<tr><td colspan='3'><h1 class='relatorio-texto'><b>Não há registros de alergias medicamentosas</h1></td></tr>";
		}
		$html.= "</table>";

		return $html;
	}

	public function alergiaAlimentar($dados) {
		$html.= "<table class='mytable' style='width:100%'>";
		$html.= "<tr bgcolor='#D3D3D3'><td colspan='3'  ><h1 class='relatorio-texto'><b>ALERGIA ALIMENTAR</b></h1></td></tr>";
		$sql = "SELECT ALIMENTO AS alimento from alergia_alimentar where PACIENTE_ID = {$dados->pacienteid} GROUP BY ALIMENTO";
		$result = mysql_query($sql);
		$contador = 0;
		if(mysql_num_rows($result) > 0){
			while ($row = mysql_fetch_array($result)) {
				$nome = $row['alimento'];
				$contador++;
				$html.= "<tr><td colspan='3'><h1 class='relatorio-texto'><b>Alimento $contador:</b> $nome</h1></td></tr>";
			}
		}else{
			$html .= "<tr><td colspan='3'><h1 class='relatorio-texto'><b>Não há registros de alergia alimentar</h1></td></tr>";
		}
		$html.= "</table>";
	}

	public function capitacao_medica($dados,$acesso, $empresa){
		$enabled = "";


		$this->plan;

		$html.= "<form method='post' target='_blank'>";

		$html.= "<table class='mytable' style='width:100%'>";
        $responseEmpresa = Filial::getEmpresaById($empresa);

		//$logo = Administracao::buscarLogoPeloIdPaciente($dados->pacienteid);

		$html.= "<tr><td colspan ='3'><h2><a><img src='../utils/logos/{$responseEmpresa['logo']}' width='100' ></a>FICHA DE " . htmlentities("AVALIAÇÃO") . "</h2></td></tr>";
		$html .= "</table><br></br>";
		$html.=$this->cabecalho($dados, $responseEmpresa['nome'], $dados->convenio);
		$html.= "<br></br><table class='mytable' style='width:100%'>";

		$html.= "<tr bgcolor='#D3D3D3'><td colspan='4'><h1 class='relatorio-texto'><center><b>" . htmlentities("Responsável pela Avaliação") . ": ".utf8_decode(htmlentities($dados->usuario))." - DATA: {$dados->data} </b></center></h1></td></tr>";

		$html.= "<tr><td colspan='4'><h1 class='relatorio-texto'><b>Modalidade de Home Care:";
		if($dados->modalidade =='1')
			$html.= "</b><b>(x)</b>&nbsp; Assistencia Domiciliar <b>( )</b>&nbsp; " . htmlentities("Internação") . " Domiciliar</td></h1></tr>";
		else
			$html.= "</b><b>( )</b>&nbsp; Assistencia Domiciliar <b>(x)</b>&nbsp; " . htmlentities("Internação") . " Domiciliar</h1></td></tr>";

		$html.= "<tr><td colspan='3'><h1 class='relatorio-texto'><b>Local de " . htmlentities("Avaliação") . " do paciente:</b>";
		if($dados->local_av=='1')
			$html.= " <b>(x)</b>&nbsp;Domicilio/" . htmlentities("Instituição") . " de Apoio. <b>( )</b>&nbsp; Hospital</h1></td></tr>";
		else
			$html.= " <b>( )</b>&nbsp; Domicilio/" . htmlentities("Instituição") . " de Apoio. <b>(x)</b>&nbsp; Hospital</h1></td></tr>";
		$html.= "<tr><td colspan='3'></td></tr>";
		$html.= "<tr bgcolor='#D3D3D3'><td colspan='4' ><h1 class='relatorio-texto'><b>MOTIVO DA " . htmlentities("HOSPITALIZAÇÃO") . "</b></h1></td></tr>";
		$html.= "<tr><td colspan='3' ><h1 class='relatorio-texto'>{$dados->motivo}</tr>";
		$html.= "<tr><td colspan='3'></td></tr>";
		$html.= "<tr bgcolor='#D3D3D3'><td colspan='4' ><h1 class='relatorio-texto'><b>PROBLEMAS ATIVOS</b></h1></td></tr>";
		$html.= "<tr><td colspan='3' ><table id='problemas_ativos'>";
		if(!empty($dados->ativos)){
			foreach($dados->ativos as $a){
				$html.= "<tr><td><h1 class='relatorio-texto'>{$a}</h1></td></tr>";
			}
		}else{
			$html.= "<tr><td><h1 class='relatorio-texto'>Não há registros de problemas ativos</h1></td></tr>";
		}

		$html.= "</table></td></tr>";
		$html.= "</table>";

		$html.= "<table class='mytable' style='width:100%'>";

		$html.= "<tr><td colspan='5'></td></tr>";
		$html.= "<tr bgcolor='#D3D3D3'><td colspan='4' ><h1 class='relatorio-texto'><b>COMORBIDADES (S/N)</b></h1></td></tr>";
		$html.= "<tr><td><h1 class='relatorio-texto'><input type='text' size='8' name='cancer' value='{$dados->cancer}' {$acesso} /><b>Câncer</b></h1></td>";
		$html.= "<td colspan='3'><h1 class='relatorio-texto'><input type='text' size='8' name='cardiopatia' value='{$dados->cardiopatia}' {$acesso} /><b>Cardiopatia</b></h1></td></tr>";
		$html.= "<tr><td><h1 class='relatorio-texto'><input type='text' size='8' name='psiquiatrico' value='{$dados->psiquiatrico}' {$acesso} /><b>" . htmlentities("Distúrbio Psiquiátrico") . "</b></h1></td>";
		$html.= "<td colspan='3'><h1 class='relatorio-texto'><input type='text' size='8' name='dm' value='{$dados->dm}' {$acesso} /><b>DM</b></h1></td></tr>";
		$html.= "<tr><td><h1 class='relatorio-texto'><input type='text' size='8' name='neurologica' value='{$dados->neuro}' {$acesso} /><b>" . htmlentities("Doença Neurológica") . "</b></h1></td>";
		$html.= "<td colspan='3'><h1 class='relatorio-texto'><input type='text' size='8' name='reumatologica' value='{$dados->reumatologica}' {$acesso} /><b>" . htmlentities("Doença Reumatológica") . "</b></h1></td></tr>";
		$html.= "<tr><td><h1 class='relatorio-texto'><input type='text' size='8' name='glaucoma' value='{$dados->glaucoma}' {$acesso} /><b>Glaucoma</b></h1></td>";
		$html.= "<td colspan='3'><h1 class='relatorio-texto'><input type='text' size='8' name='has' value='{$dados->has}' {$acesso} /><b>HAS</b></h1></td></tr>";
		$html.= "<tr><td><h1 class='relatorio-texto'><input type='text' size='8' name='hepatopatia' value='{$dados->hepatopatia}' {$acesso} /><b>Hepatopatia</b></h1></td>";
		$html.= "<td colspan='3'><h1 class='relatorio-texto'><input type='text' size='8' name='nefropatia' value='{$dados->nefropatia}' {$acesso} /><b>Nefropatia</b></h1></td></tr>";
		$html.= "<tr><td><h1 class='relatorio-texto'><input type='text' size='8' name='obesidade' value='{$dados->obesidade}' {$acesso} /><b>Obesidade</b></h1></td>";
		$html.= "<td colspan='3'><h1 class='relatorio-texto'><input type='text' size='8' name='pneumopatia' value='{$dados->pneumopatia}' {$acesso} /><b>Pneumopatia</b></h1></td></tr>";

		$html .= "</table>";

////////hospita///////
		$html .= $this->alergiaMedicamentosa($dados);
		$html .= $this->alergiaAlimentar($dados);

		if($dados->modalidade==2 && $dados->local_av==2){
			$html.= "<tr><td></td></tr><tr><td colspan='3'><table id='score-id2' class='mytable'>style='width:100%'";
			$html .= $this->cond_pre_ihosp($dados);
			$html.= "</table></td></tr>";
		}

		if($dados->modalidade==2 && $dados->local_av==2){
			$html.= "<tr><td></td></tr><tr><td colspan='3'><table id='score-id1' class='mytable'>style='width:100%'";
			$html .= $this->cond_pre_id($dados);
			$html .= $this->cont_inter_domiciliar($dados);
			$html.= "</table></td></tr>";
		}
		if($dados->modalidade==2 && $dados->local_av==1){
			$html.= "<tr><td></td></tr><tr><td colspan='3'><table id='score-id1' class='mytable'>style='width:100%'";
			$html .= $this->cond_pre_id($dados);
			$html .= $this->cont_inter_domiciliar($dados);
			$html.= "</table></td></tr>";
		}
		$html.= "<table class='mytable' style='width:100%'>";

		$html.= "<tr><td colspan='3' bgcolor='#D3D3D3'><h1 class='relatorio-texto'><b>" . htmlentities("INTERNAÇÕES NOS ÚLTIMOS") . " 12 MESES</b></h1></td></tr>";
		$sql4 = "SELECT MOTIVO_INTERNACAO AS descricao from internacoescapmedica where ID_CAPMEDICA = {$dados->capmedica} UNION SELECT historicointernacao AS descricao FROM capmedica WHERE id = {$dados->capmedica}";
		$result4 = mysql_query($sql4);
		$contador = 0;
		while ($row4 = mysql_fetch_array($result4)) {
			$nome = $row4['descricao'];$contador2++;
			if($nome != ''){
				$contador++;
				$html .= "<tr><td colspan='2' class='internacao' desc='{$nome}' ><h1 class='relatorio-texto'> <b>Motivo Internação $contador:</b> {$nome} </h1></td></tr>";
			}
		}
		if($contador == 0){
			$html .= "<tr><td colspan='2'><h1 class='relatorio-texto'><b>Não há registros de internações</b></td>/tr>";
		}
		$html.= "<tr><td colspan='3'></td></tr><tr bgcolor='#D3D3D3'>";
		$html.= "<td colspan='3' ><h1 class='relatorio-texto'><b>PARECER " . htmlentities("MÉDICO") . "</b></h1></td>";
		$html.= "</tr>";
		$html.= "<tr><td colspan='2' ><h1 class='relatorio-texto'>";

		if($dados->parecer== '0'){
			$html.= "&nbsp;<b>(x)</b>" . htmlentities("Favorável") . "&nbsp;&nbsp;";
			$html.= "<b>( )</b>" . htmlentities("Favorável") . " com ressalvas&nbsp;&nbsp;";
			$html.= "<b>( )</b>" . htmlentities("Contrário") . "";}
		if($dados->parecer== '1'){
			$html.= "&nbsp;<b>( )</b>" . htmlentities("Favorável") . "&nbsp;&nbsp;";
			$html.= "<b>(x)</b>" . htmlentities("Favorável") . " com ressalvas&nbsp;&nbsp;";
			$html.= "<b>( )</b>" . htmlentities("Contrário") . "";
		}
		if($dados->parecer=='2'){
			$html.= "&nbsp;<b>( )</b>" . htmlentities("Favorável") . "&nbsp;&nbsp;";
			$html.= "<b>( )</b>" . htmlentities("Favorável") . " com ressalvas&nbsp;&nbsp;";
			$html.= "<b>(x)</b>" . htmlentities("Contrário") . "";
		}

		$html.= "<br/><b>Justificativa:</b><br/>{$dados->pre_justificativa}</br>{$dados->justificativa}";
		$html.= "</h1></td></tr>";
		$html.= "<tr bgcolor='#D3D3D3'><td colspan='3'><h1 class='relatorio-texto'><b>Plano Desmame:</b></h1></td></tr><tr><td colspan='3'><h1 class='relatorio-texto'>{$dados->plano_desmame}</h1></td></tr>";
		$html.= "<tr><td colspan='3'></h1></td></tr>";
		$html.= "</table>";

		$html .=  $this->equipamentos($dados->capmedica);



		////////////////jeferson 6-5-2013 colocar assinatura medica
		$id_usuario = $dados->usuarioid;
		$sql = "SELECT usr.ASSINATURA
	FROM usuarios as usr
	WHERE idUsuarios = '{$id_usuario}'";
	$result = mysql_query($sql);
	$assinatura = mysql_result($result,0,0);
	$html .= "<br/><br/><table width='100%'><tr><td><center><img src='{$assinatura}' width='20%'></center></td></tr></table>";

	$paginas []= $header.$html.= "</form>";


	//print_r($html);
	//print_r($paginas);

	$mpdf=new mPDF('pt','A4',8);
	$stylesheet = file_get_contents('../utils/relatorio.css');
	$mpdf->WriteHTML($stylesheet, 1);
	$mpdf->SetHeader('página {PAGENO} de {nbpg}');
	$ano = date("Y");
        $mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']}"." &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));


	$flag = false;
	foreach($paginas as $pag){
		if($flag) $mpdf->WriteHTML("<formfeed>");
		$mpdf->WriteHTML($pag);
		$flag = true;
	}
	$mpdf->WriteHTML("</body></html>");
	$mpdf->Output('pacientes.pdf','I');
	exit;

}

}

$p = new Paciente_imprimir_ficha();
$p->detalhes_capitacao_medica($_GET['id'],$_GET['empresa']);

?>