<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';
ini_set('display_errors', 1);

use \App\Controllers\Medico;

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
	"GET" => [
		'/medico/relatorios/?action=form-relatorio-evolucoes' => '\App\Controllers\Medico::formRelatorioEvolucoes',
		'/medico/relatorios/?action=excel-relatorio-evolucoes' => '\App\Controllers\Medico::excelRelatorioEvolucoes',
	],
	"POST" => [
        '/medico/relatorios/?action=form-relatorio-evolucoes' => '\App\Controllers\Medico::relatorioEvolucoes',
	]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}
