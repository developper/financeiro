<?php
$id = session_id();
if (empty($id))
    session_start();

include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/codigos.php');
require_once('associacao_catalogo.php');
require_once('paciente_med.php');
require_once('relatorios.php');
require_once('RelatorioDeflagradoMedico.php');

require __DIR__ . '/../vendor/autoload.php';

use \App\Models\Administracao\Paciente as ModelPaciente;
use \App\Helpers\StringHelper as String;
/* !
 * Classe Medicos, responsável por exibir as opções relacionadas com o módulo "médicos" do sistema.
 *
 */

class Medicos {
    /* ! Quantidade de itens prescritos, serve de apoio no momento de carregar uma prescrição antiga. 
     * Primeiro é preenchido o array com as quantidades, posteriormente é gerado o código html com as respectivas quantidades.
     * No códgio html, essas quantidades são armazenadas como atributos do div correspondente a sua aba.
     */

    private $qtds = array('0' => '0', '1' => '0', '2' => '0', '3' => '0', '4' => '0', '5' => '0', '6' => '0', '7' => '0', '8' => '0', '9' => '0', '10' => '0', '11' => '0', '12' => '0', 's' => '0', '14' => '0', '15' => '0', '16' => '0');
    //! Abas e seus respectivos códigos
    private $abas = array(
        '0' => 'Repouso',
        '1' => 'Dieta',
        '2' => 'Cuidados Especiais',
        '3' => 'Soros e Eletr&oacute;litos',
        '4' => 'Antibi&oacute;ticos Injet&aacute;veis',
        '5' => 'Injet&aacute;veis IV',
        '6' => 'Injet&aacute;veis IM',
        '7' => 'Injet&aacute;veis SC',
        '8' => 'Drogas inalat&oacute;rias',
        '9' => 'Nebuliza&ccedil;&atilde;o',
        '10' => 'Drogas Orais/Enterais',
        '11' => 'Drogas T&oacute;picas',
        '12' => 'F&oacute;mulas',
        '13' => 'Oxigenoterapia',
        '14' => 'Dietas Enterais/Parenterais',
        '15' => 'Materiais',
        '16' => 'Procedimentos em Gastrostomia'
    );
    private $frequencia = array();
    private $vadm = array();
    private $frequenciaoxi = array();
    private $frequencia_emergencial = array();
    private $equipe = array();
    private $soros = array();
    private $dietas = array();
    private $repousos = array();
    private $inicio = "";
    private $fim = "";
    private $tipo_presc = "";
    private $mant_data = "";

    //!contrutor da classe, inicializa os dropbox.
    function Medicos() {
        $this->inicio = date("d/m/Y");
        $this->fim = date("d/m/Y", strtotime("+1 Week " . date("Y-m-d")));
        $sql = "SELECT * FROM frequencia ORDER BY frequencia";
        $result = mysql_query($sql);
        $this->frequencia[''] = -1;
        while ($row = mysql_fetch_array($result)) {
            $this->frequencia[$row['frequencia']] = $row['id'];
        }
        $sql = "SELECT * FROM frequencia WHERE id='21' OR id='20' OR id='26' ORDER BY frequencia;";
        $result = mysql_query($sql);
        $this->frequenciaoxi[''] = -1;
        while ($row = mysql_fetch_array($result)) {
            $this->frequenciaoxi[$row['frequencia']] = $row['id'];
        }
        $sql = "SELECT * FROM frequencia WHERE  id in (2,3,4,5,6,7,8,9,10,11,12,13) ORDER BY id;";
        $result = mysql_query($sql);
        $this->frequencia_emergencial[''] = -1;
        while ($row = mysql_fetch_array($result)) {
            $this->frequencia_emergencial[$row['frequencia']] = $row['id'];
        }
        $sql = "SELECT * FROM viaadm";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $this->vadm[$row['via']] = $row['id'];
        }
        $sql = "SELECT * FROM cuidadosespeciais WHERE ATIVO = 'S' ORDER BY cuidado";
        $result = mysql_query($sql);
        $this->equipe[''] = -1;
        while ($row = mysql_fetch_array($result)) {
            $this->equipe[$row['cuidado']] = $row['id'];
        }
        $sql = "SELECT c.idClientes as id ,c.nome from clientes as c  where not idClientes in (Select paciente from capmedica) and idClientes in(1,2,5,16) ";
        $result = mysql_query($sql);
        $this->pacienteprob[''] = -1;
        while ($row = mysql_fetch_array($result)) {
            $this->pacienteprob[$row['nome']] = $row['id'];
        }
        $sql = "SELECT * FROM soros WHERE ativo = 1 ORDER BY soro";
        $result = mysql_query($sql);
        $this->soros[''] = -1;
        while ($row = mysql_fetch_array($result)) {
            $this->soros[$row['soro']] = $row['id'];
        }
        $sql = "SELECT * FROM dietas ORDER BY dieta";
        $result = mysql_query($sql);
        $this->dietas[''] = -1;
        while ($row = mysql_fetch_array($result)) {
            $this->dietas[$row['dieta']] = $row['id'];
        }
        $sql = "SELECT * FROM repousos ORDER BY repouso";
        $result = mysql_query($sql);
        $this->repousos[''] = -1;
        while ($row = mysql_fetch_array($result)) {
            $this->repousos[$row['repouso']] = $row['id'];
        }
    }

    //! Gera dropbox com as unidades de medida
    private function combo_um($extra = NULL) {
        $html = "<select name='um'  $extra >";
        $html .= "<option value='-1'></option>";
        $sql = "SELECT * FROM umedidas ORDER BY unidade";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $html .= "<option value='{$row['id']}' >{$row['unidade']}</optioin>";
        }
        $html .= "</select>";
        return $html;
    }

    //! Gera os botões de navegação das abas de prescrição
    private function botoes_navegacao($a, $p) {
        $b = "";
        if ($p <> -1)
            $b .= "<button class='next-tab' rel='$p' style='float:right' >Pr&oacute;ximo >></button>";
        if ($a <> -1)
            $b .= "<button class='prev-tab' rel='$a' style='float:right' ><< Anterior</button>";
        return $b;
    }

    //! Código do dialog para prescrição de medicamentos.
    private function formulario() {
        echo "<div id='formulario'  tipo='' classe='' somenteGenerico = 'N'>";
        echo alerta();
        echo "<div id='textFormulario'></div>";
        echo "<p><b>Busca:</b><br/><input type='text' name='busca' id='busca' />";
        echo "<div id='opcoes-itens'></div>";
        echo "<input type='hidden' name='cod' id='cod' value='-1' />";
        echo "<input type='hidden' name='brasindice_id' id='brasindice_id' value='-1' />";

        echo "<p><b>Item:</b><input type='text' name='nome' id='nome' class='OBG' size='80' />";
        //echo "<b>Apresenta&ccedil;&atilde;o:</b><input type='text' name='apr' id='apr' class='OBG' />";
        echo "<br/><b>Dosagem:</b> <input size='5' type='text' name='dose' class='OBG ' /> " . $this->combo_um("class='COMBO_OBG'");
        echo " <b>Frequ&ecirc;ncia: </b>" . combo_box('frequencia', $this->frequencia, '-1', "class='COMBO_OBG'");
        echo " <b>Via ADM:</b> " . $this->viaadm('vadm-formulario', '1', '-1', "class='COMBO_OBG'");
  //    echo $this->aprazamento();
        echo $this->periodo(true,true);
        echo "<br/><b>" . htmlentities("Observação/Justificativa") . ":</b> <input type='text' placeholder='máximo 250 caracteres' maxlength='250' name='obs' id='obs' size='60' />";
        echo "<br/><button id='add-item' tipo='' class='incluir' >+ incluir</button></div>";
    }

    //! Formulário para solicitação de material.
    private function formulario_material() {
        echo "<div id='formulario_material' title='Materiais' tipo='15' classe='' >";
        echo alerta();
        echo "<p><b>Busca:</b><br/><input type='text' name='busca-item-material' id='busca-item' />";
        echo "<div id='opcoes-kits'></div>";
        echo "<input type='hidden' name='cod' id='cod' value='-1' />";
        echo "<input type='hidden' name='brasindice_id' id='brasindice_id' value='-1' />";
        echo "<p><b>Nome:</b><input type='text' name='nome' id='nome-mat' class='OBG' />";
        echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
        echo "<br/><b>Frequ&ecirc;ncia:</b> " . combo_box('frequencia', $this->frequencia, '-1', "class='COMBO_OBG'");
        echo $this->periodo(true, true);
        echo "<br/><b>" . htmlentities("Observação/Justificativa") . ":</b> <input type='text' placeholder='máximo 250 caracteres' maxlength='250' name='obs' id='obs' size='60' />";
        echo "<br/><button id='add-item-material' tipo='' class='incluir' >+ incluir</button></div>";
    }

    //! Tela de dialog para pre_prescri��o de medicamentos.
    private function pre_prescricao() {

        echo "<div id='pre-presc'   classe='' presc=''   CodPaciente='' title='Criar Prescri&ccedil;&atilde;o'>";
        echo alerta();
        echo "<form method='post' action='?view=prescricoes'>";
        echo "<input type='hidden' name='paciente' id='paciente' value=''></input>";
        echo "<input type='hidden' name='antigas' id='antiga' value=''></input>";
        echo "<input type='hidden' name='manter_data' id='manter_data' value=''></input>";

        echo "<fieldset><legend><b>Paciente</b></legend><span id='NomePacientePresc' ></span></fieldset><br/>";

        
        echo $this->tipo_prescricao();
        echo $this->manter_data();
        echo $this->periodo(true);
        echo "<br/><button id='iniciarprescricao' tipo='' class='validacao_periodo ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Iniciar</span></button></form></div>";
    }

    //! Gera as opções de via de administração
    private function viaadm($name, $tipo, $checked, $extra = NULL) {
        $sql = "SELECT v.id, UPPER(v.via) as via FROM viaadm as v WHERE $tipo = '1' ORDER BY via";
//  	echo $sql;
        $html = "<select name='{$name}'  $extra >";
        $html .= "<option value='-1'></option>";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            if ($row['via'] == $checked)
                $html .= "<option value='{$row['id']}' SELECTED />{$row['via']}</option>";
            else
                $html .= "<option value='{$row['id']}' />{$row['via']}</option>";
        }
        $html .= "</select>";
        return $html;
    }

    //! Gera o compo de aprazamento, hora início.
    /* ! NÃO UTILIZADO, a responsabilidade pelo aprazamento passou para as enfermeiras. */
    private function aprazamento() {
        echo "<b>Hora In&iacute;cio:</b>";
        echo "<input type='text' name='aprazamento' class='hora OBG' maxlength='5' size='5' />";
    }

    // Gera dois compos inputs para inserir um período de data
    private function periodo($dialog = false, $interno = false){
        //SE A ROTINA QUE ESTÁ CHAMANDO É O DIALOG DE INICIAR UMA PRESCRIÇÃO
        $inicioDialog = $dialog ? "id='inicio' dialog='true'" : '';
        $fimDialog = $dialog ? " id='fim' dialog='true'" : '';

        //SE A ROTINA QUE ESTÁ CHAMANDO É A PÁGINA INTERNA DE PRESCRIÇÕES
        $inicioInterno = $interno ? "prescricao-inicio' readonly" : "inicio-periodo'";
        $fimInterno = $interno ? "prescricao-fim' readonly" : "fim-periodo'";

        echo "<br/><fieldset><legend><b>Per&iacute;odo</b></legend>";
        echo "DE". 
          "<input type='text' name='inicio' value='{$this->inicio}' i='{$this->inicio}' maxlength='10' size='10' class='OBG periodo {$inicioInterno} {$inicioDialog} />".
          " AT&Eacute; <input type='text' name='fim' value='{$this->fim}' f='{$this->fim}' maxlength='10' size='10' class='OBG periodo {$fimInterno} {$fimDialog} /></fieldset>";
    }

    private function tipo_prescricao() {
        echo "<p></p>";
        echo "<fieldset style='width:340px;'>";
        echo "<legend><b>Tipo da Prescri&ccedil;&atilde;o: </b></legend>";
        echo "<span class='usar_nova'><input type='radio'  name='tipo' value='2' class='OBG tipo_presc' checked />Peri&oacute;dica</br></span>";
        echo "<span class='usar_nova'><input type='radio'  name='tipo' value='3' class='OBG tipo_presc' />Aditiva</br></span>";
        echo "<span class='usar_nova'><input type='radio'  name='tipo' value='1' class='OBG tipo_presc' />Emergencial</br></span>";

        echo "</fieldset>";
    }

    private function manter_data() {
        echo "<input id='manter-data' type='checkbox' ><b>Manter data da anterior.</b></input>";
    }

    /* ------------------------////////jeferson///////---------------- */

    private function presc_emergencia($id, $data, $inicio, $fim, $tipo) {
        echo "<div id='dialog-salvo1' title='Finalizada'>";
        echo "<center><h3>Prescri&ccedil;&atilde;o salva com sucesso!</h3></center>";
        //      echo "<form action='imprimir.php' method='post'><input type='hidden' value='-1' name='prescricao'><center><button>Imprimir</button></center></form>";
        echo "<br/><center>Enviar email de aviso?</center>";
        echo "</div>";
        echo "<div id='dialog-imprimir1' title='Imprimir'>";
        echo "<center><h3>Visualizar Prescri&ccedil;&atilde;o?</h3></center>";
        echo "<form action='imprimir_emergencia.php' method='post' target='_blank'>
<input type='hidden' value='-1' name='prescricao_emergencia'>
<input type='hidden' value='-1' name='empresa'>
<center></center></form>";
        echo "</div>";
        echo "<h1><center>Prescri&ccedil;&atilde;o Emergencial</center></h1>";

        $pnome = $this->pacienteNome($id);

        $this->formulario2();
        $sqlTpoMedicamento = "SELECT
planosdesaude.nome,
planosdesaude.tipo_medicamento,
clientes.empresa
FROM
clientes
INNER JOIN planosdesaude ON clientes.convenio = planosdesaude.id
WHERE
clientes.idClientes = $id ";
        $result=  mysql_query($sqlTpoMedicamento);
        $tipoMedicamento = mysql_result($result,0,1);
        $nomePlano = mysql_result($result,0,0);
        $empresaId = mysql_result($result,0,2);


        echo "<div id='presc_emergencia' paciente='{$id}' pnome='{$pnome}' data='{$data}' empresa-id = '{$empresaId}' inicio='{$inicio}' fim='{$fim}' tipo_presc='{$tipo}'>";
        echo "<h3><center>{$pnome}</center></h3>";
        $botoes = "<img class='add-kit' tipo='kits' item='presc_emergencia' src='../utils/kit_48x48.png' title='Prescri&ccedil;&atilde;o' border='0'>&nbsp;&nbsp;";


        echo "<center>$botoes</center>";
        $kits = "<table class='mytable' width=100% id='kits-presc_emergencia'><thead><tr ><th colspan='2'>Prescri&ccedil;&atilde;o</th></tr></thead></table>";


        echo "$kits<br/>";
        echo" <button id='finalizar_presc_emergencia' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' type= role='button' aria-disabled='false'>
  		<span class='ui-button-text' id='finalizar_presc_emergencia'>+ Finalizar</span></button></div>";
        //echo "<button id='finalizar-presc_emergencia'>+ Finalizar</button></div>";
    }

    private function formulario2() {


        echo "<div id='formulario2' title='Prescri&ccedil;&atilde;o de Emerg&ecirc;ncial' tipo='' classe='' >";
        echo alerta();
        /* <input type='radio' id='bmed' name='tipos' value='med' /><label for='bmed'>Medicamentos</label>
          <input type='radio' id='bmat' name='tipos' value='mat' /><label for='bmat'>Materiais</label> */
        echo "<center><div id='tipos'>
  
  	<input type=hidden id='bkits' name='tipos' value='kits' />
  	</div></center>";
        echo "<p><b>Busca:</b><br/><input type='text' name='busca1' id='busca-item' />";
        echo "<div id='opcoes-kits'></div>";
        echo "<input type='hidden' name='brasindice_id' id='brasindice_id' value='-1' />";
        echo "<input type='hidden' name='cod' id='cod' value='-1' />";
        echo "<input type='hidden' name='cod_ref' id='cod_ref' value='-1' />";

        echo "<p><b>Nome:</b><input type='text' name='nome' id='nome' class='OBG' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        //echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
        echo "<p><b>Dosagem:</b><input class=' OBG' name='qtd_kit' id='qtd_kit' size='5' type='text'/>" . $this->combo_um(" name='um_kit' class='COMBO_OBG' ");
        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Frequ&ecirc;ncia:</b> " . combo_box('frequencia', $this->frequencia_emergencial, '-1', "id='frequencia' name='frequencia' class='COMBO_OBG'") . "&nbsp;&nbsp;&nbsp;&nbsp;
  	&nbsp;<b>Hora de Inicio:</b>&nbsp;&nbsp;&nbsp;<input type='text' class='hora' name='hora' id='hora' size='5' />";
        echo $this->periodo();
        echo "<br/><b>" . htmlentities("Observação/Justificativa") . ":</b>" . input('text', 'obs', 'obs', "size='50'");
        echo "<p/><button id='add-item1' tipo='' >+</button></div>";
    }

    private function posologia() {
        echo "<div class='posologia' >";
        echo "<br/><b>Posologia:</b>";
        echo "<br/><b>Via ADM:</b> " . radio_box('vadm', $this->vadm, 'iv');
        echo "<br/><b>Frequ&ecirc;ncia:</b> " . combo_box('frequencia', $this->frequencia, '1h');
        echo "</div>";
    }

    //! Gera o dropbox com a lista de pacientes que pode ser prescrito pelo médico.
    private function pacientes() {

        $medico = $_SESSION['id_user'];
//        $result = mysql_query("SELECT c.* FROM clientes as c, equipePaciente as e WHERE e.paciente = c.idClientes AND e.funcionario = {$medico} AND e.fim  IS NULL ORDER BY nome;");
//        $result = mysql_query("SELECT c.* FROM clientes as c WHERE c.status = 4 AND (c.empresa in{$_SESSION['empresa_user']} OR '1' in{$_SESSION['empresa_user']}) ORDER BY nome;");
				$condEmpresa = $_SESSION['empresa_principal'] != 1 ? "WHERE c.empresa IN {$_SESSION['empresa_user']}" : "";
				$sql = "SELECT c.* FROM clientes as c {$condEmpresa} ORDER BY status, nome";
        $result = mysql_query($sql);

        echo "<p><b>Localizar Paciente</b></p>";
        echo "<select name='paciente' data-placeholder='Selecione um paciente...' style='background-color:transparent;width:362px' id='selPaciente'>";
        echo "<option value='-1'></option>";
        $aux = 0;
        $paciente_status = array();
        $status =array('1'=>'::     Novo    ::','4'=>'::   Ativo/Autorizado   ::','6'=>'::     Óbito    ::');
        while ($row = mysql_fetch_array($result)) 
        {
            $paciente = strtoupper($row['nome']);
            if(array_key_exists($row['status'], $paciente_status))
            {   
                $pacientes_por_status = array('idCliente'=>$row['idClientes'],'nome'=>$paciente, 'codigo'=>$row['codigo'], 'status'=>$row['status']);
                # Verifica se a chave existe dentro do array $status, 
                # caso nÃ£o esteja, ele adiciona ao cÃ³digo 7 para que 
                # os outros possam ser ordenados juntos no select 
                if(array_key_exists($row['status'], $status))
                {
                    array_push($paciente_status[$row['status']],$pacientes_por_status);
                }
                else
                {
                    if(!array_key_exists(7,$paciente_status))
                    {
                        $paciente_status[7] = array();
                    }        
                    array_push($paciente_status[7],$pacientes_por_status);
                }    
                
            }
            else 
            {   
                $paciente_status[$row['status']] = array();
                $pacientes_por_status = array('idCliente'=>$row['idClientes'],'nome'=>$paciente, 'codigo'=>$row['codigo'], 'status'=>$row['status']);
                # Verifica se a chave existe dentro do array $status, 
                # caso nÃ£o esteja, ele adiciona ao cÃ³digo 7 para que 
                # os outros possam ser ordenados juntos no select 
                if(array_key_exists($row['status'], $status))
                {
                    array_push($paciente_status[$row['status']],$pacientes_por_status);
                }
                else
                {
                    if(!array_key_exists(7,$paciente_status))
                    {
                        $paciente_status[7] = array();
                    }          
                    array_push($paciente_status[7],$pacientes_por_status);
                }    
                
            }
        }
      
       
       $cont=0;
       asort($paciente_status);
       foreach($paciente_status as $chave=>$dados)
       {   
            #Verifica se Ã© a primeira vez que ta rodando
            if(array_key_exists($chave, $status) && $cont==0)
            {
                echo "<optgroup></optgroup>";
                echo "<optgroup label='{$status[$chave]}'>";  
            }#Da segunda vez em diante passarÃ¡ por aqui
            elseif(array_key_exists($chave, $status) && $cont>0)
            {
                echo "</optgroup>";
                echo "<optgroup></optgroup>";
                echo "<optgroup label='{$status[$chave]}'>";  
            }
            elseif($chave==7)
            {
                echo "</optgroup>";
                echo "<optgroup></optgroup>";
                echo "<optgroup label='::     Outros     ::'>";  
            }
            foreach($dados as $chave2=>$dados2)
            {
                echo $chave;  
                
                echo "<option value='{$dados2['idCliente']}' status='{$chave}'> ({$dados2['codigo']}) " . String::removeAcentosViaEReg($dados2['nome']) . "</option>";
            }
            $cont++;
       }    
        echo "</optgroup>";
        echo "</select>";
    }


    //! Retorna o nome de um paciente com determinado ID
    private function pacienteNome($id) {
        $result = mysql_query("SELECT nome FROM clientes WHERE idClientes='$id' ORDER BY nome;");
        while ($row = mysql_fetch_array($result)) {
            return ucwords(strtolower($row['nome']));
        }
    }

    //! Gera o menu da parte central.
    private function menu() {
       echo " <div>
                    <center><h1>Médicos</h1></center>
                </div>";
        if(isset($_SESSION['permissoes']['medico']['medico_paciente']) || $_SESSION['adm_user'] == 1) {
            echo "<p><a href='?view=paciente_med&act=listar'>Pacientes</a>";
        }
        if(isset($_SESSION['permissoes']['medico']['medico_prescricao']) || $_SESSION['adm_user'] == 1) {
            echo "<p><a href='?view=buscar'>Prescri&ccedil;&otilde;es</a>";
        }
        if(isset($_SESSION['permissoes']['medico']['medico_suspender_item_prescricao_ativa']) || $_SESSION['adm_user'] == 1) {
            echo "<p><a href='/medico/prescricao/?action=suspender-itens-prescricoes'>Suspender Itens em Prescri&ccedil;&otilde;es Ativas</a>";
        }
        /*if ($_SESSION["adm_user"]) {
            echo "<p><a href='?view=desc_medico'>Observa&ccedil;&atilde;o Associada</a>";
            echo "<p><a href='?view=reparar_produto_interno'>Reparar Produto Interno</a>";
            echo "<p><a href='?view=associar_medicamento'>Associar Medicamento</a>";
            echo "<p><a href='?view=indicadores'>Indicadores de Qualidade</a>";
        }*/

        if(isset($_SESSION['permissoes']['medico']['medico_relatorio']) || $_SESSION['adm_user'] == 1) {
            echo "<p><a href='?view=relatorios'>Relatórios</a>";
        }
    }
    
    //! Gera o menu da parte central.
    private function menuIndicadores() {
        if ($_SESSION["adm_user"]) {
            echo "<p><a href='?view=indicadores&rel=erros-ficha-avaliacao'>Preenchimento da Avaliação Inicial</a></p>";
            echo "<p><a href='?view=indicadores&rel=erros-prescricao'>Preenchimento da Prescrição Inicial</a></p>";
        }
    }

    private function menuRelatorios() {
        if(isset($_SESSION['permissoes']['medico']['medico_relatorio']['medico_relatorio_assistencial']) || $_SESSION['adm_user'] == 1) {
            echo "<p><a href='../relatorios/?medico-enfermagem-ur=form'>Relatórios Assistenciais</a></p>";
        }
        if(isset($_SESSION['permissoes']['medico']['medico_relatorio']['medico_relatorio_nao_conformidade']) || $_SESSION['adm_user'] == 1) {
            echo "<p><a href='../auditoria/relatorios/?action=relatorio-nao-conformidades-gerenciar&from=enfermagem'>Relatório de Não Conformidades</a></p>";
        }
        // echo "<p><a href='../relatorios/?medico-enfermagem-ur=form'>Relatórios Assistenciais</a></p>";
    }
    
    private function addFiltersRelatorioIndicador($tipo) {
        $html = "";
        $html .= "<div>";
        $html .= "<h1><center>Relatório de Erros na ".($tipo === 'erros-ficha-avaliacao' ? 'Ficha de Avaliação' : 'Prescrição')." Inicial</center></h1>";
        
        //UR's-------------------------------------
        $html .= "<p><b>Empresa:</b></p>";
        $html .= "<p>";        
        $sql = "SELECT id, nome FROM empresas WHERE ATIVO='S' AND id NOT IN (1, 3, 9, 10)";
        $rs = mysql_query($sql);
        $html .= "<select name='ur' id='ur' style='background-color:transparent;width:400px;display:inline;' >";
        while ($row = mysql_fetch_array($rs))
            $html .= "<option style='width:400px' value='{$row['id']}'>{$row['nome']}</option>";
        $html .= "</select>";
        $html .= "<input type='checkbox' id='todos-ur' name='todos-ur' value='t' /> <label for='todos'><b> Todos </b></label></p>";
        //-----------------------------------------
        
        //MÉDICOS----------------------------------
        $html .= "<p><b>M&eacute;dico:</b></p>";
        $html .= "<p>";        
        $sql = "SELECT idUsuarios, nome FROM usuarios WHERE tipo='medico' ORDER BY nome";
        $rs = mysql_query($sql);        
        $html .= "<select name='medico' id='medico' style='background-color:transparent;width:400px;display:inline;' >";
        while ($row = mysql_fetch_array($rs))
            $html .= "<option style='width:400px' value='{$row['idUsuarios']}'>{$row['nome']}</option>";
        $html .= "</select>";
        $html .= "<input type='checkbox' id='todos-medico' name='todos-medico' value='t' /> <label for='todos'><b> Todos </b></label></p>";
        //-----------------------------------------
        
        $html .= "<fieldset style='border: 1px solid; font-size: 14px;width:30%;' >";
        $html .= "<legend><b>Per&iacute;odo</b></legend>";
        echo $html;
        $this->periodo(false, false);
        $html = "</fieldset><br>";
        $html .= "<input type='hidden' id='tipo-indicador' name='tipoindicador' value='{$tipo}' />";
        $html .= "<p><button type='submit' id='pesquisa-indicador' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Pesquisar</span></button>";
        $html .= "</div>";
        $html .= "<div id='resultado'>";
        $html .= "</div>";
        
        echo $html;
    }

    //! Gera a aba repouso
    private function repouso() {
        echo "<div id='div-repouso' class='tab-presc' qtd='{$this->qtds[0]}' title='Repouso' tipo='0' >";
        echo "<center><h2>Repouso</h2></center>";
        echo alerta();
        echo "<br/><b>Repouso:</b>" . combo_box('repouso', $this->repousos, '-1', "class='COMBO_OBG' ");
        echo "<br/><b>Frequ&ecirc;ncia:</b> " . combo_box('frequencia', $this->frequencia, '-1', "class='COMBO_OBG'");
        echo $this->periodo(false, true);
        echo "<br/><b>" . htmlentities("Observação/Justificativa") . ":</b>" . input('text', 'obs', 'obs', " size='50' placeholder='máximo 250 caracteres' maxlength='250' ");
        echo "<br/>" . button('add-repouso', '+ incluir', "tipo='0' label='Repouso' class='incluir' ");
        echo $this->botoes_navegacao(-1, 1);
        echo "</div>";
    }

    //! Gera a aba dieta
    private function dieta() {
        echo "<div id='div-dieta' class='tab-presc' qtd='{$this->qtds[1]}' title='Dieta' tipo='1' >";
        echo "<center><h2>Dieta</h2></center>";
        echo alerta();
        echo "<br/><b>Dieta:</b>" . combo_box('dieta', $this->dietas, '-1', "class='COMBO_OBG' ");
        echo "<br/><b>Via ADM:</b> " . $this->viaadm('vadm-dieta', 'dieta', '-1');
        echo "<br/><b>Frequ&ecirc;ncia:</b> " . combo_box('frequencia', $this->frequencia, '-1', "class='COMBO_OBG'");
        echo "<br/><b>Diárias de Dieta Enteral:</b>";
        echo "<select id='diarias'  >";
        echo "<option value='-1' ></option>";
        echo "<option value='s' >Sim</option>";
        echo "<option value='n' >Não</option>";
        echo"</select><br/>";
        echo "<span id='sp_quantidade_diarias'>";
        echo "<b>Quantidade:</b><input type='text' name='quantidade_diarias' id='quantidade_diarias'/>";
        echo "</span>";

        //  	echo $this->aprazamento();
        echo $this->periodo(false, true);
        echo "<br/><b>" . htmlentities("Observação/Justificativa") . ":</b>" . input('text', 'obs', 'obs', "size='50' placeholder='máximo 250 caracteres' maxlength='250'");
        echo "<br/>" . button('add-dieta', '+ incluir', "tipo='1' label='Dieta' ");
        echo $this->botoes_navegacao(0, 2);
        echo "</div>";
    }

    //! Gera a aba equipe multidiscilinar
    private function equipe_multidisciplinar() {
        echo "<div id='div-cuidados' class='tab-presc' qtd='{$this->qtds[2]}' title='Cuidados Especiais' tipo='2' >";
        echo "<center><h2>Cuidados Especiais</h2></center>";
        echo alerta();
        echo "<p><b>Cuidados Especiais:</b>" . combo_box('cuidados', $this->equipe, '-1', "class='COMBO_OBG' id=selcuidados ");
        echo "<br/><span id='cuid1' n='1'><b>Frequ&ecirc;ncia: </b>" . combo_box('frequencia', $this->frequencia, '-1', "class='COMBO_OBG'") . "</span>";
        echo "<br/><span id='cuid2' n='0'><b>Frequ&ecirc;ncia: </b><select name='frequencia1'><option value='26' selected>Cont&iacute;nuo</option></select></span><br/>";
        echo $this->periodo(false, true);
        echo "<br/><b>" . htmlentities("Observação/Justificativa") . ":</b>" .
            "<br/><textarea name='obs' id='obs' rows='5' cols='110'></textarea>"; // .input('text', 'obs', 'obs', "size='50' placeholder='máximo 250 caracteres' maxlength='250'");
        ///echo "<br/><b>Di&aacute;ria de dieta enteral?</b><select id='diaria_enteral' class='OBG'   ><option  value=''></option><option value='n'>N&atilde;o</option><option value='s'>Sim</option></select>";
        echo "<br/><span class='num_dias'><b>Numero de dias:<input type='texto' name='qtd_dias' class='dias_dieta'></input></span>";
        echo "<p>" . button('add-cuidados', '+ incluir', "tipo='2' label='Cuidados' class='incluir'");
        echo $this->botoes_navegacao(1, 3);
        echo "</div>";
    }

    //! Gera a aba de soros e eltrólitos
    private function soros_eletrolitos() {
        echo "<div id='div-soros' class='tab-presc' qtd='{$this->qtds[3]}' title='Soros e Eletr&oacute;litos' tipo='3' >";
        echo "<center><h2>Soros e Eletr&oacute;litos</h2></center>";
        echo alerta();
        echo "<p><b>Soros e Eletr&oacute;litos:</b>" . combo_box('soros', $this->soros, '-1', "class='COMBO_OBG' ");
        echo "<p><b>Via ADM:</b> " . $this->viaadm('vadm-soro', 'soro', '-1') . "</p>";
        echo "<p><b>Frequ&ecirc;ncia:</b> " . combo_box('frequencia', $this->frequencia, '-1', "class='COMBO_OBG'") . "</p>";
        echo "<p><b>Fluxo:</b> <input size='5' type='text' class=' OBG' name='fluxo' /> " . $this->combo_um("class='COMBO_OBG'") . "</p>";
        echo "<p><b>Volume Total:</b> <input size='10' type='text' class=' OBG' name='volume_total' /></p>";
        echo $this->periodo(false, true);
        echo "<br/><b>" . htmlentities("Observação/Justificativa") . ":</b>" . input('text', 'obs', 'obs', "size='50' placeholder='máximo 250 caracteres' maxlength='250'");
        echo "<p>" . button('add-soro', '+ incluir', "tipo='3' label='Soros' class='incluir' ");
        echo $this->botoes_navegacao(2, 4);
        echo "</div>";
    }

    //! Gera a aba de antibióticos injetáveis
    private function antibioticos_injetaveis() {

        echo "<div id='div-antibio-inj' class='tab-presc' qtd='{$this->qtds[4]}' title='Antibi&oacute;ticos Injet&aacute;veis' tipo='4' >";
        echo "<center><h2>Antibi&oacute;ticos Injet&aacute;veis</h2></center>";
        echo "<button class='show-form' tipo='4' classe='1' label='Antibi&oacute;ticos Injet&aacute;veis' via='{$this->vadm['IV']}' >Novo</button>";
        echo $this->botoes_navegacao(3, 5);
        echo "</div>";
    }

    //! Gera a aba de injetáveis EV
    private function injetaveis_ev() {
        echo "<div id='div-inj-ev' class='tab-presc' qtd='{$this->qtds[5]}' title='Injet&aacute;veis IV' tipo='5' >";
        echo "<center><h2>Injet&aacute;veis IV</h2></center>";
        echo "<button class='show-form' tipo='5' classe='2' label='Injet&aacute;veis IV' via='{$this->vadm['IV']}' >Novo</button>";
        echo $this->botoes_navegacao(4, 6);
        echo "</div>";
    }

    //! Gera a aba de injetáveis IM
    private function injetaveis_im() {
        echo "<div id='div-inj-im' class='tab-presc' qtd='{$this->qtds[6]}' title='Injet&aacute;veis IM' tipo='6' >";
        echo "<center><h2>Injet&aacute;veis IM</h2></center>";
        echo "<button class='show-form' tipo='6' classe='3' label='Injet&aacute;veis IM' via='{$this->vadm['IM']}' >Novo</button>";
        echo $this->botoes_navegacao(5, 7);
        echo "</div>";
    }

    //! Gera a aba de injetáveis SC
    private function injetaveis_sc() {
        echo "<div id='div-inj-sc' class='tab-presc' qtd='{$this->qtds[7]}' title='Injet&aacute;veis SC' tipo='7' >";
        echo "<center><h2>Injet&aacute;veis SC</h2></center>";
        echo "<button class='show-form' tipo='7' classe='4' label='Injet&aacute;veis SC' via='{$this->vadm['SC']}' >Novo</button>";
        echo $this->botoes_navegacao(6, 8);
        echo "</div>";
    }

    //! Gera a aba de drogas inalatórias
    private function drogas_inalatorias() {
        echo "<div id='div-drogas-inalatorias' class='tab-presc' qtd='{$this->qtds[8]}' title='Drogas Inalat&oacute;rias' tipo='8' >";
        echo "<center><h2>Drogas Inalat&oacute;rias </h2></center>";
        echo "<button class='show-form' tipo='8' classe='5' label='Drogas Inalat&oacute;rias' via='-1' via='{$this->vadm['SC']}'>Novo</button>";
        echo $this->botoes_navegacao(7, 9);
        echo "</div>";
    }

    //! Gera a aba de nebulização
    private function nebulizacao() {
        ob_start();
        echo "<div id='div-nebulizacao' class='tab-presc' qtd='{$this->qtds[9]}' title='Nebuliza&ccedil;&atilde;o' tipo='9' >";
        echo "<center><h2>Nebuliza&ccedil;&atilde;o</h2></center>";
        echo alerta();
        echo "<br/><b>Composi&ccedil;&atilde;o</b>";
        echo "<table>";
        echo "<tr class='comp-nebulizacao' id='comp-nebulizacao'><td><input type='checkbox' class='comp-nebulizacao' id='Atrovent' name='Atrovent'></td>
  		 <td>Atrovent (Brometo de Ipratrópio)</td><td><input size='5' type='text' class='' disabled='disabled' name='neb'/></td><td>" . $this->combo_um() . "</td></tr>";
        echo "<tr class='comp-nebulizacao' id='comp-nebulizacao2'><td><input type='checkbox' class='comp-nebulizacao'  id='Berotec' name='Berotec'></td>
  		 <td>Berotec (Bromidrato de Fenoterol)</td><td><input size='5' type='text' class='' disabled='disabled'  name='neb'/></td><td>" . $this->combo_um("id='comp-nebulizacao2select'") . "</td></tr>";

        echo "</table>";
        echo "<div id='nebulizacao-outros' ><table><tr><td><input type='checkbox' id='check-outro-neb' class='comp-nebulizacao-outro'></td><td>Outros</td></tr>";
        echo "<tr><td></td><td>Busca:<input type='text' name='busca-outro-neb' id='busca-outro-neb' disabled='disabled' /></td></tr>";
        echo "<tr><td></td><td><input type='text' name='nome-outro-neb' id='nome-outro-neb' size='80' disabled='disabled' /></td>";
        echo "<td></td><td><input size='5' type='text' name='dose-outro-neb' id='dose-outro-neb' class='' disabled='disabled' /></td><td>" . $this->combo_um("id='um-outro-neb' class='nebul-outro'") . "</td></tr></table></div>";
        echo "<b>Dilui&ccedil;&atilde;o</b><select class='OBG'  name='diluicao-nebulizacao' id='diluicao-nebulizacao'>
  	<option></option>
  	<option dil='SF 0,9%'>SF 0,9% </option>
  	<option dil='AD'> AD </option></selecte>";
        /* echo "<input name='diluicao-nebulizacao' type='radio' value='sf' dil='SF 0,9%' checked />SF 0,9% 
          <input name='diluicao-nebulizacao' value='ad' dil='AD' type='radio' />AD"; */
        echo "<input class='OBG' name='qtd-diluicao' id='qtd-diluicao' size='5' type='text' />" . $this->combo_um("id='um-diluicao-neb' class='COMBO_OBG' ");
        echo "<br/><b>Frequ&ecirc;ncia:</b> " . combo_box('frequencia', $this->frequencia, '-1', "id='frequencia' name='frequencia' class='COMBO_OBG'");
        echo $this->periodo(false, true);
        echo "<br/><b>" . htmlentities("Observação/Justificativa") . ":</b>" . input('text', 'obs', 'obs', "size='50' placeholder='máximo 250 caracteres' maxlength='250'");
        echo "<br/>" . button('add-nebulizacao', '+ incluir', "tipo='9' label='Nebuliza&ccedil;&atilde;o' class='incluir'");
        echo $this->botoes_navegacao(8, 10);
        
        echo "<div id='dialog-incluir-diluente' title='Incluir Nebuliza&ccedil;&atilde;o'>";
        echo "<center><h3>Deseja prescrever uma nebuliza&ccedil;&atilde;o apenas com diluente?</h3></center>";

        //      echo "<form action='imprimir.php' method='post'><input type='hidden' value='-1' name='prescricao'><center><button>Imprimir</button></center></form>";

        echo "</div>";
        echo "</div>";

        ob_end_flush();
    }

    ///Fim aba nebuliza��o
    //! Gera a aba de drogas orais
    private function drogas_orais() {
        echo "<div id='div-drogas-orais' class='tab-presc' qtd='{$this->qtds[10]}' title='Drogas Orais/Enterais' tipo='10' >";
        echo "<center><h2>Drogas Orais/Enterais</h2></center>";
        echo "<button class='show-form' tipo='10' classe='6' label='Drogas Orais' via='{$this->vadm['VO']}' >Novo</button>";
        echo $this->botoes_navegacao(9, 11);
        echo "</div>";
    }

    //! Gera a aba de dietas orais
    private function dietas_orais() {
        echo "<div id='div-dietas-orais' class='tab-presc' qtd='{$this->qtds[14]}' title='Dietas Orais/Enterais/Parenterais' tipo='14' >";
        echo "<center><h2>Dietas Orais/Enterais/Parenterais</h2></center>";
        echo "<button class='show-form' tipo='14' classe='6' label='Dietas Orais / Enterais / Parenterais' via='{$this->vadm['VO']}' >Novo</button>";
        echo $this->botoes_navegacao(9, 11);
        echo "</div>";
    }

    ///Gerar aba Oxigenoterapia
    private function oxigenoterapia() {
        ob_start();
        echo "<div id='div-oxigenoterapia' class='tab-presc' qtd='{$this->qtds[13]}' title='Oxigenoterapia' tipo='13' >";
        echo "<center><h2>Oxigenoterapia</h2></center>";
        echo alerta();
        echo "<br/><b>Equipamentos:</b>";
        echo "<table >";
        echo "<tr class='comp-oxigenoterapia' >
    <td colspan= '3'>
    <input type='radio' id = '1' class='oxi' name='02'  tipo='2' amb='DBVMR (AMBU)' >
  	<b>DBVMR (AMBU)</b></td></tr>";
        echo "<tr class='comp-oxigenoterapia' >
    <td colspan= '3'>
    <input type='radio' id='u6' class='oxi' name='02'  tipo='4' amb='VNI Intermitente' >
  	<b>VNI Intermitente</b></td></tr>";
        echo "<tr class='comp-oxigenoterapia' >
  	<td ><input type='radio' class='oxi' name='02' tipo='1'  /> <b>M&aacute;scara de Venturi.</b></td><td><select  class = 'oxi2' id='s' disabled='disabled' name='dose'>
    <option></option>
  	<option value='1'  med='10'  n='50% (10 L/min)' >50% (10 L/min)</option>
  	<option value='2'  med='8'   n='40% (8 L/min)' >40% (8 L/min)</option>
  	<option value='3'  med='8'    n='35% (8 L/min)'>35% (8 L/min)</option>
  	<option value='4'  med='6'    n='31% (6 L/min)'>31% (6 L/min)</option>
  	<option value='5'  med='4'    n='28% (4L/min)'>28% (4 L/min)</option>
  	<option value='6'  med='4'    n='24% (4 L/min)'>24% (4 L/min)</option>
  	</select> </td>
  	
  	</tr>";

        echo "<tr class='comp-oxigenoterapia' >
    <td>
    <input type='radio' name='02'class='oxi' tipo='3' >
  	<b>M&aacute;scara Facial com Reservat&oacute;rio.</b></td><td><select  class = 'oxi2' id='s1' disabled='disabled' name='dose'>
    <option></option>
  	<option value='1'  med='7'    n='7 L/min' > 7  L/min</option>
  	<option value='2'  med='8'    n='8 L/min' > 8  L/min</option>
  	<option value='3'  med='9'    n='9 L/min' > 9  L/min</option>
  	<option value='4'  med='10'   n='10 L/min'> 10 L/min</option>
  	<option value='5'  med='11'   n='11 L/min'> 11 L/min</option>
  	<option value='5'  med='12'   n='12 L/min'> 12 L/min</option>
  	</td></tr>";


        echo "<tr class='comp-oxigenoterapia' colspan= ''>
               <td><input type='radio' name='02' class='oxi'  tipo='0'>
  	           <b>Cateter Nasal Tipo Simples.</b>
               </td>
               <td><input name='dose' size='3' toxi='1' type='text'  disabled='disabled' class='oxi2' id='u1' maxlength='1' /><b>1 a 5 l/min</b>
               </td><td width='30%'></td></tr>";
        echo "<tr class='comp-oxigenoterapia' colspan= ''><td><input type='radio'name='02' class='oxi' tipo='0' >
  	<b>M&aacute;scara Facial Simples.</b></td><td><input name='dose' size='3' toxi='4' type='text'  disabled='disabled' class='oxi2' id='u4' maxlength='1'  /><b>5 a 8 l/min</b></td><td width='30%'></td></tr>";
        echo "<tr class='comp-oxigenoterapia' colspan= ''><td><input type='radio'name='02' class='oxi' tipo='0' >
  	<b>Cateter Nasal Tipo &Oacute;culos.</b></td><td><input name='dose' size='3' toxi='3' type='text'  disabled='disabled' class='oxi2' id='u' maxlength='1'  /><b>1 a 5 l/min</b></td><td width='30%'></td></tr>";
        echo "<tr class='comp-oxigenoterapia' colspan= ''><td><input type='radio' name='02' class='oxi' cod='5988' tipo='0'>
  	<b>Concentrador de Oxig&eacute;nio.</b></td><td><input name='dose' size='3'  type='text'  disabled='disabled' class='oxi2' id='u2' maxlength='1' /><b>1 a 6 l/min</b></td><td width='30%'></td></tr>";
       echo "<tr class='comp-oxigenoterapia' colspan= ''>
           <td><input type='radio' name='02' class='oxi' cod='0176883' tipo='0'>
  	<b>Máscara p/ Macronebulizador BESMED PN-1117 </b></td>
        <td><input name='dose' size='3'  type='text'  disabled='disabled' class='oxi2' id='u5' maxlength='1' /><b>1 a 8 l/min</b></td>
        <td width='30%'></td></tr>";
        echo "</table>";
        echo "<br/><b>Frequ&ecirc;ncia:</b> " . combo_box('frequencia', $this->frequenciaoxi, '-1', "name='frequenciaoxi' id='frequenciaoxi'");
        echo $this->periodo(false, true);
        echo "<br/><b>" . htmlentities("Observação/Justificativa") . ":</b>" . input('text', 'obs', 'obs', "size='50' placeholder='máximo 250 caracteres' maxlength='250'");
        echo "<br/>" . button('add-oxigenoterapia', '+ incluir', "tipo='13' label='Oxigenoterapia' class='incluir'");
        echo $this->botoes_navegacao(13, -1);
        echo "</div>";
        ob_end_flush();
    }

    //! Gera a aba de drogas tópicas
    private function drogas_topicas() {


        echo "<div id='div-drogas-topicas' class='tab-presc' qtd='{$this->qtds[11]}' title='Drogas T&oacute;picas' tipo='11' >";
        echo "<center><h2>Drogas T&oacute;picas</h2></center>";
        echo "<button class='show-form' tipo='11' classe='8' label='Drogas T&oacute;picas' via='{$this->vadm['Topico']}'>Novo</button>";
        echo $this->botoes_navegacao(10, 12);
        echo "</div>";
    }

    //! Gera a aba de fórmulas
    private function formulas() {
        echo "<div id='div-formulas' class='tab-presc' qtd='{$this->qtds[12]}' title='F&oacute;rmulas' tipo='12' >";
        echo "<center><h2>F&oacute;rmulas</h2></center>";
        echo alerta();
        echo "<p><b>Descri&ccedil;&atilde;o</b>" . input('text', 'formula', 'formula', "class='OBG' size='50'");
        echo "<br/><b>Frequ&ecirc;ncia:</b> " . combo_box('frequencia', $this->frequencia, '-1', "class='COMBO_OBG'");
        echo " <b>Via ADM:</b> " . $this->viaadm('vadm-formula', '1', '-1');
//  	echo $this->aprazamento();
        echo $this->periodo(false, true);
        echo "<br/><b>" . htmlentities("Observação/Justificativa") . ":</b>" . input('text', 'obs', 'obs', "size='50' placeholder='máximo 250 caracteres' maxlength='250'");
        echo "<p>" . button('add-formula', '+ incluir', "tipo='12' label='F&oacute;rmula' class='incluir'");
        echo $this->botoes_navegacao(11, 13);
        echo "</div>";
    }

    //! Gera a aba com as drogas ativas, passíveis de suspensão.
    /* ! Essa aba é responsável por exibir os medicamentos em uso pelo paciente. */
    private function drogas_ativas($id) {
        echo "<div id='div-drogas-ativas' class='tab-presc' qtd='{$this->qtds['s']}' title='Suspender' tipo='s' >";
        echo "<center><h2>Suspender itens</h2></center>";
        $html = "<table width=100% id='itens-ativo' class='mytable'><thead><tr>";
        $html .= "<th><b>Itens Ativos</b></th>";
        $html .= "<th width=1% ><input type='checkbox' id='checktodos-susp' name='select' /></th>";
        $sql = "SELECT 
            v.via as nvia,
            f.frequencia as nfrequencia,
            i . *,
            i.id as id,
            i.inicio,
            i.fim,
            DATE_FORMAT(aprazamento, '%H:%i') as apraz,
            u.unidade as um
        FROM
            prescricoes as p,
            itens_prescricao as i
                LEFT OUTER JOIN
            frequencia as f ON (i.frequencia = f.id)
                LEFT OUTER JOIN
            viaadm as v ON (i.via = v.id)
                LEFT OUTER JOIN
            umedidas as u ON (i.umdose = u.id)
        WHERE
            i.tipo <> '-1' AND p.paciente = '$id' and p.status = 0
            AND ADDDATE(curdate(), INTERVAL 1 DAY) BETWEEN i.inicio AND i.fim
            AND p.id = i.idPrescricao
        ORDER BY tipo , inicio , aprazamento , nome , apresentacao ASC";

        $result = mysql_query($sql);
        $qtd = 0;
        $lastType = 0;
        $n = 0;
        while ($row = mysql_fetch_array($result)) {
            if ($n++ % 2 == 0)
                $cor = '#E9F4F8';
            else
                $cor = '#FFFFFF';

            $html .= "<tr></tr>";
            $i = implode("/", array_reverse(explode("-", $row['inicio'])));
            $f = implode("/", array_reverse(explode("-", $row['fim'])));
            $aprazamento = "";
            ///jeferson 3-8-2012 id_item
            $id_item = $row['id'];
            $dose = $row['dose'] . " " . $row['um'];
            $dose1 = $row['dose'];


            $aba = $this->abas[$row['tipo']];
            if ($row['tipo'] <> 0)
                $aprazamento = $row['apraz'];

            $dados = " tipo='s' cod='{$row['NUMERO_TISS']}' nome='{$row['nome']}' dose='{$dose1}'  um='{$row['umdose']}' frequencia='{$row['frequencia']}' inicio='{$i}' fim='{$f}' obs='{$row['obs']}' id_item='{$id_item}'";
            $linha = "<b>$aba:</b> {$row['nome']} {$row['nvia']} Dose: {$dose} {$row['nfrequencia']} {$aprazamento}. Per&iacute;odo: $i até $f OBS: {$row['obs']}";
            $html .= "<tr bgcolor=$cor $dados  class='suspender' >";
            $html .= "<td >$linha </td><td><input type='checkbox' name='select' class='select-item-susp' /></td>";
            $html .= "</tr>";
            $qtd++;
        }
        if ($qtd == 0) {
            $html .= "<tr><td colspan='2' align='center'><b>N&atilde;o existe drogras ativas.</b></td></tr>";
        }
        $html .= "</table>";
        echo $html;
        echo "<br/><button id='susp-item' >Suspender</button>";
        echo $this->botoes_navegacao(12, 14);
        echo "</div>";
    }

    ///-materiais 2013-06-11
    private function materiais() {

        echo "<div id='div-materiais' class='tab-presc' qtd='{$this->qtds[15]}' title='Materiais' tipo='15' >";
        echo "<center><h2>Materias</h2></center>";
        echo "<button class='show-form-material' tipo='15' classe='1' label='Materiais' >Novo</button>";
        echo $this->botoes_navegacao(13, 15);
        echo "</div>";
    }

    private function procedimentosGastrostomia()
    {
        echo "<div id='div-gastrostomia' class='tab-presc' qtd='{$this->qtds[16]}' title='Procedimentos em Gastrostomia' tipo='16' >";
        echo "<center><h2>Procedimentos em Gastrostomia</h2></center>";
        echo $this->periodo(false,false);
        echo "<p><b>Buscar Material:</b> <input type='text' name='busca-item-material' id='busca-item-gtm' />";
        echo "<input type='hidden' name='cod' id='cod-gtm' value='-1' />";
        echo "<input type='hidden' name='brasindice_id' id='brasindice-id-gtm' value='-1' />";
        echo "<p><b>Nome:</b> <input type='text' name='nome' id='nome-mat-gtm' size='80' class='OBG' /></p>";
        echo "<p><b>Data da Troca:</b> <input type='text' name='data_troca' maxlength='10' size='10' class='OBG inicio-periodo' /></p>";
        echo "<p>
                <b>Tipo:</b>
                <select name='tipo_gtm' id='tipo-gtm' class='OBG'>
                    <option selected disabled value=''>Selecione...</option>
                    <option value='1'>Button</option>
                    <option value='2'>Sonda de Foley</option>
                    <option value='3'>Sonda de GTM</option>
                </select>
                <br><br>
                <span id='extras-tipo-gtm'></span>
              </p>";
        echo "<p><b>Marca do Produto danificado: </b><input type='text' name='marca_gtm' id='marca-gtm' size='40' class='OBG' /></p>";
        echo "<p>
                <b>Descreva o dano do dispositivo: </b>
                <br>
                <textarea name='dano_gtm' id='dano-gtm' class='OBG' cols='66' placeholder='Descreva o dano do dispositivo...'></textarea>
              </p>";
        echo "<p>
                <b>O que levou a esse dano?</b>
                <br>
                <select name='origem_dano_gtm' id='origem-dano-gtm' class='OBG'>
                    <option selected disabled value=''>Selecione...</option>
                    <option value='1'>Manuseio indevido</option>
                    <option value='2'>Alteração clínica do paciente</option>
                    <option value='3'>Tração</option>
                    <option value='4'>Outro</option>
                </select>
                <br>
                <br>
                <textarea name='descricao_origem_dano_gtm' id='descricao-origem-dano-gtm' cols='66' class='OBG' placeholder='Descreva mais especificamente o que levou ao dano...'></textarea>
              </p>";
        echo "<br/><button id='add-gtm' tipo='16' class='incluir' >+ incluir</button>";
        echo $this->botoes_navegacao(14, -1);
        echo "</div>";
    }


    //! Inicializa uma prescrição nova com base em uma anterior.
    private function load_table($prescricao, $id, $mant_data) {
        $html = "";
        $flag = true;

        $baseinicio = implode("-", array_reverse(explode("/", $this->inicio)));
        $basefim = implode("-", array_reverse(explode("/", $this->fim)));
        $sql3 = "select Carater,inicio,fim,DATE_FORMAT(data,'%Y-%m-%d')  from prescricoes where id = '$prescricao' ";
        $r2 = mysql_query($sql3);
        $carater = mysql_result($r2, 0);
        $data_cap = mysql_result($r2, 0, 3);
        $datalimit = date('Y-m-d', strtotime("+2 days", strtotime($data_cap)));
        if ($carater == 2) {
            $carateraditivo = 3;
        }
        if ($carater == 4) {
            $carateraditivo = 7;
        }
        $dinicio = mysql_result($r2, 0, 1);
        $dfim = mysql_result($r2, 0, 2);

        if($carater == 3){
            $sql = "SELECT
                      	I.*,
                      	v.via as nvia, f.frequencia as nfrequencia,'$baseinicio' as inicio,P.id as idp,P.Carater,
                      	'$basefim' as fim, DATE_FORMAT(I.aprazamento,'%H:%i') as apraz, ume.unidade as um,
                  		B.ID AS idItem
                    FROM
                        prescricoes AS P INNER JOIN
                        itens_prescricao AS I ON (P.id = I.idPrescricao) LEFT JOIN
                        catalogo AS B ON ( I.CATALOGO_ID = B.ID) LEFT OUTER JOIN
                        frequencia as f ON (I.frequencia = f.id) LEFT OUTER JOIN
                        viaadm as v ON (I.via = v.id) LEFT OUTER JOIN
                        umedidas as ume ON (I.umdose = ume.id)
                    WHERE
                        P.id ='$prescricao' ORDER BY
                        tipo, Carater, tipo ASC";
        }
        if ($carater == 2) {
            $sql = "SELECT
                      	I.*,
                      	v.via as nvia, f.frequencia as nfrequencia,'$baseinicio' as inicio,P.id as idp,P.Carater,
                      	'$basefim' as fim, DATE_FORMAT(I.aprazamento,'%H:%i') as apraz, ume.unidade as um,
                  		B.ID AS idItem
                    FROM
                        prescricoes AS P INNER JOIN
                        itens_prescricao AS I ON (P.id = I.idPrescricao) LEFT JOIN
                        catalogo AS B ON ( I.CATALOGO_ID = B.ID) LEFT OUTER JOIN
                        frequencia as f ON (I.frequencia = f.id) LEFT OUTER JOIN
                        viaadm as v ON (I.via = v.id) LEFT OUTER JOIN
                        umedidas as ume ON (I.umdose = ume.id)
                    WHERE
                        P.id ='$prescricao'
                    UNION
                    SELECT
                      	I.*,
                      	v.via as nvia, f.frequencia as nfrequencia,'$baseinicio' as inicio,P.id as idp,P.Carater,
                      	'$basefim' as fim, DATE_FORMAT(I.aprazamento,'%H:%i') as apraz, ume.unidade as um,
                      	B.ID AS idItem
                    FROM
                        prescricoes AS P INNER JOIN
                        itens_prescricao AS I ON (P.id = I.idPrescricao) LEFT JOIN
                        catalogo AS B ON ( I.CATALOGO_ID = B.ID) LEFT OUTER JOIN
                        frequencia as f ON (I.frequencia = f.id) LEFT OUTER JOIN
                        viaadm as v ON (I.via = v.id) LEFT OUTER JOIN
                        umedidas as ume ON (I.umdose = ume.id)
                    WHERE
                        (P.Carater = {$carateraditivo} OR P.Carater = 11) AND
                        (DATE_FORMAT(P.inicio,'%Y-%m-%d') >= '$dinicio' AND
                        DATE_FORMAT(P.inicio,'%Y-%m-%d') <= '$dfim' AND
                        DATE_FORMAT(P.fim,'%Y-%m-%d') >='$dinicio' AND
                        DATE_FORMAT(P.fim,'%Y-%m-%d') <='$dfim') AND
                        paciente='$id' AND
                        P.DESATIVADA_POR = 0
                    ORDER BY
                        tipo, Carater, tipo ASC
  	        ";
        }
        if ($carater == 4 && $mant_data != 1) {

            $sql = "SELECT
  		I.*,
  		v.via as nvia, f.frequencia as nfrequencia,'$baseinicio' as inicio,'$basefim' as fim,P.id as idp,P.Carater,
  		DATE_FORMAT(I.aprazamento,'%H:%i') as apraz, ume.unidade as um,
        B.ID AS idItem
  		FROM prescricoes AS P
  		INNER JOIN itens_prescricao AS I ON (P.id = I.idPrescricao)
  		LEFT JOIN	catalogo AS B ON ( I.CATALOGO_ID = B.ID)
  	
  		LEFT OUTER JOIN frequencia as f ON (I.frequencia = f.id)
  		LEFT OUTER JOIN viaadm as v ON (I.via = v.id)
  		LEFT OUTER JOIN umedidas as ume ON (I.umdose = ume.id)
  		WHERE  P.id ='$prescricao'
  		UNION
  		SELECT
  		I.*,
  		v.via as nvia, f.frequencia as nfrequencia,'$baseinicio' as inicio,'$basefim' as fim,P.id as idp,P.Carater,
  	    DATE_FORMAT(I.aprazamento,'%H:%i') as apraz, ume.unidade as um,
  	    B.ID AS idItem
  		FROM prescricoes AS P
  		INNER JOIN itens_prescricao AS I ON (P.id = I.idPrescricao)
  		LEFT JOIN	catalogo AS B ON ( I.CATALOGO_ID = B.ID)
  	
  		LEFT OUTER JOIN frequencia as f ON (I.frequencia = f.id)
  		LEFT OUTER JOIN viaadm as v ON (I.via = v.id)
  		LEFT OUTER JOIN umedidas as ume ON (I.umdose = ume.id)
  		WHERE  P.Carater = {$carateraditivo} 
  		AND DATE_FORMAT(P.data,'%Y-%m-%d') <= '$datalimit' 
  		AND P.id >'$prescricao' 
  		AND paciente='$id'
  		order by tipo, Carater,idp asc
  	";
        }
        if ($carater == 4 && $mant_data == 1) {

            $sql = "SELECT
                  		I.*,
                  		v.via as nvia, f.frequencia as nfrequencia,P.id as idp,P.Carater,
                  		DATE_FORMAT(I.aprazamento,'%H:%i') as apraz, ume.unidade as um,
                  		B.ID AS idItem
                  	FROM
                        prescricoes AS P INNER JOIN
                        itens_prescricao AS I ON (P.id = I.idPrescricao) LEFT JOIN
                        catalogo AS B ON ( I.CATALOGO_ID = B.ID) LEFT OUTER JOIN
                        frequencia as f ON (I.frequencia = f.id) LEFT OUTER JOIN
                        viaadm as v ON (I.via = v.id) LEFT OUTER JOIN
                        umedidas as ume ON (I.umdose = ume.id)
                  	WHERE
                        P.id ='$prescricao'
                  	UNION
                  	SELECT
                  		I.*,
                  		v.via as nvia, f.frequencia as nfrequencia,P.id as idp,P.Carater,
                  		DATE_FORMAT(I.aprazamento,'%H:%i') as apraz, ume.unidade as um
                  	FROM
                        prescricoes AS P INNER JOIN
                        itens_prescricao AS I ON (P.id = I.idPrescricao) LEFT JOIN
                        catalogo AS B ON ( I.CATALOGO_ID = B.ID) LEFT OUTER JOIN
                        frequencia as f ON (I.frequencia = f.id) LEFT OUTER JOIN
                        viaadm as v ON (I.via = v.id) LEFT OUTER JOIN
                        umedidas as ume ON (I.umdose = ume.id)
                  	WHERE
                        P.Carater = {$carateraditivo} AND
                        DATE_FORMAT(P.data,'%Y-%m-%d') <= '$datalimit' AND
                        P.id >'$prescricao' AND
                        paciente='$id'
                    ORDER BY
                    tipo, Carater,idp asc
  	        ";
        }

        $result = mysql_query($sql);

        $qtd = 0;
        $lastType = 0;
        $z = 0;
        $tipocarater = '-1';
        $data = [];
        $suspensos = [];
        while ($row = mysql_fetch_array($result)) {
            $data[] = $row;
            if($row['tipo'] == -1 && $row['tipo_item_origem'] != null) {
                $suspensos[$row['idItem']][$row['tipo_item_origem']] = true;
            }
        }
        $html .= "<br><table width=100% id='tabela-itens' inicio='{$this->inicio}' fim='{$this->fim}' class='mytable'><thead><tr>";
        $html .= "<th align='center'><b>Itens prescritos</b></th>";
        $html .= "<th width=1% ><input type='checkbox' id='checktodos' name='select' /></th>";
        foreach($data as $row) {
            if (!isset($suspensos[$row['idItem']][$row['tipo']]) && $row['tipo'] != -1) {
                if ($z++ % 2 == 0)
                    $cor1 = '#E9F4F8';
                else
                    $cor1 = '#FFFFFF';

                $tipo = $row['tipo'];
                for ($t = $lastType; $t <= $tipo; $t++) {
                    $aba = $this->abas[$t];
                    $html .= "<tr  id='itens-$t' ></tr>";
                }

                $this->qtds[$tipo]++;
                $lastType = $row['tipo'] + 1;
                $i = implode("/", array_reverse(explode("-", $row['inicio'])));
                $f = implode("/", array_reverse(explode("-", $row['fim'])));
                $aprazamento = "";
                $aba = $this->abas[$row['tipo']];

                if ($row['tipo'] <> 0)
                    $aprazamento = $row['apraz'];
                $dose = "";
                $dados_dose = "";
                if ($row['dose'] != 0) {
                    $dose = $row['dose'] . " " . $row['um'];

                    $dose1 = $row['dose'];

                    $dados_dose = " dose='{$dose1}' um='{$row['umdose']}' ";
                }
                $dados = " tipo='{$row['tipo']}' cod='{$row['NUMERO_TISS']}' brasindice_id='{$row['CATALOGO_ID']}' nome='{$row['nome']}' via='{$row['via']}' frequencia='{$row['frequencia']}' inicio='{$i}' fim='{$f}' obs='{$row['obs']}' {$dados_dose}";
                if ($row['tipo'] == -1) {
                    $linha = $row['descricao'];
                } else {
                    $linha = "<b>$aba:</b> {$row['nome']} {$row['nvia']} {$dose} {$row['nfrequencia']} " . htmlentities("Período") . ": $i até $f OBS: {$row['obs']} ";
                }

                if ($tipocarater != $row["Carater"]) {
                    $tipocarater = $row["Carater"];
                    if ($row["Carater"] == 3 || $row["Carater"] == 7) {
                        $tipopresc = "Aditiva";
                    }
                    if ($row["Carater"] == 2 || $row["Carater"] == 4) {
                        $tipopresc = "Periódica";
                    }
                    $html .= "<tr bgcolor='grey'><td colspan='2'><center><b>  " . htmlentities("Prescrição $tipopresc") . " de " . date('d/m/Y', strtotime($row[12])) . " até " . date('d/m/Y', strtotime($row[13])) . "</b></center></td></tr>";
                }
                $html .= "<tr bgcolor=$cor1 class='dados' $dados id='id-{$row['id']}' >";
                $html .= "<td style='color:#708090;'>$linha</td><td><input type='checkbox' name='select' class='select-item' /></td>";
                $html .= "</tr>";
            }
        }
        if ($prescricao == "-1") {
            foreach ($this->abas as $op => $aba) {
                $html .= "<tr id='itens-$op' ></tr>";
            }
        } else {
            for ($t = $lastType; $t < count($this->abas); $t++) {
                $aba = $this->abas[$t];
                $html .= "<tr id='itens-$t' ></tr>";
            }
        }
        $html .= "</tr></thead>";
        $html .= "</table><br>";
        return $html;
    }

    //! Exibe a lista de prescrições salvas, mas não finalizadas.
    private function prescricoes_salvas() {
        $dir = "salvas/" . $_SESSION["id_user"];
        if (!is_dir($dir))
            return;
        $ext = array('htm');
        $arquivos = get_files_dir($dir, $ext);
        if (is_array($arquivos)) {
            $tam = count($arquivos);
            $cont = 1;
            echo "<br/>";
            echo "<table width='100%' class='mytable'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th colspan='2'><center>PACIENTES</center></th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            echo "<tr>";
            foreach ($arquivos as $arquivo) {

                $nome = explode(".", $arquivo);

                $paciente = $this->pacienteNome($nome[0]);
                if ($cont == 3) {
                    echo "</tr>";
                    echo "<tr>";
                }
                echo "<td><a href='?view=prescricoes&salva={$nome[0]}'>" . strtoupper($paciente) . "</a></td>";

                //echo "<p style='display:table;float:left;padding-right:20px;'></p>";
                $cont++;
            }
            echo "</tr>";
            echo "</tbody>";
            echo "</table>";
        }
    }

    //! Carrega uma prescrição salva.
    private function load_salva($arq) {
        $dir = "salvas/" . $_SESSION["id_user"];
        $filename = $dir . "/" . $arq . ".htm";
        $str = stripslashes(file_get_contents($filename));
        echo "<div id='corpo-prescricao'>";
        echo "<div id='dialog-salvo' title='Finalizada'>";
        echo "<div id='dialog-imprimir' title='Imprimir'>";
                    echo "<center><h3>Visualizar Prescri&ccedil;&atilde;o?</h3></center>";
                    echo "<form action='imprimir.php' method='post' target='_blank'>
                            <input type='hidden' value='-1' name='prescricao'>
                            <input type='hidden' value='-1' name='empresa'>
                            <center></center></form>";
                    echo "</div>";
        echo "<center><h3>Prescri&ccedil;&atilde;o salva com sucesso!</h3></center>";
//      echo "<form action='imprimir.php' method='post'><input type='hidden' value='-1' name='prescricao'><center><button>Imprimir</button></center></form>";
        echo "<br/><center>Enviar email de aviso?</center>";
        echo "</div>";
        echo $this->formulario();
        echo "<div id='aviso' title='Avisos' ></div>";
        echo "<div id='equip_item' title='Equipamento vs Itens Prescritos' ></div>";
     
        echo $str;
        echo "</div>";
        echo "<button id='salvar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
        echo "<button id='finalizar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Finalizar</span></button></span>";
        echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
        //echo "<button id='salvar'>Salvar</button>&nbsp;&nbsp;&nbsp;<button id='finalizar' >Finalizar</button>";
    }

    private function problemas_ativos() {
        echo "<center><h1>Incluir Problemas Ativos</h1></center>";

        echo"<div id='div-prob-ativo' >";
        echo "<form>";
        echo"<b>Paciente :</b>" . combo_box('paciente', $this->pacienteprob, '-1', "class='COMBO_OBG'");
        //<select name='paciente'><option name='id-paciente-prob' value='1'>Zacarias</option><option name='id-paciente-prob' value='83'>Alaide</option></select>";
        echo "</br>";
        echo "<b>Problemas Ativos:</b>";
        echo "<input type='text' name='busca-problemas-ativos' {$acesso} size=20 />";
        echo"<input type='checkbox' id='outros-prob-ativos' name='outros-prob-ativos' {$acesso} />Outros.<span class='outro_ativo' ><input type='text' name='outro-prob-ativo' class='outro_ativo' {$acesso} size='20'/><button id='add-prob-ativo' tipo=''  >+ </button></span>";

        echo "<table id='problemas_ativos' style='border:1px solid #000000;wudth:20%' >";
        if (!empty($dados->ativos)) {
            foreach ($dados->ativos as $a) {
                echo "<tr><td>{$a}</td></tr>";
            }
        }
        echo "</table>";
        echo "<button id='salvar-problemas' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
        echo"</form></div>";
    }

    private function desc_medico() {
        echo "<center><h1>Cadastrar Observa&ccedil;&otilde;es Associadas</h1></center>";
        //echo "<div id='formulario'  tipo='' classe='' >";
        echo alerta();
        echo "<form>";
        echo "<p><b>Busca:</b><br/><input type='text' name='busca' id='busca' />";
        echo "<div id='opcoes-itens'></div>";
        echo "<input type='hidden' name='cod' id='cod' value='-1' />";
        echo "<p><b>Princ&iacute;pio:</b></br>
	    		<input type='text' name='nome' id='nome' class='OBG' size='80' />";
        echo "<br/><b>" . htmlentities("Observação/Justificativa") . ":</b><br>
	    		<textarea name='obs' id='obs' rows='2' cols='61'/></textarea>";
        echo "<br/><button id='add-item' tipo='' class='incluir_desc' >+ incluir</button></form>";
    }

    private function reparar_produto_interno() {
        echo utf8_encode("<center><h1>Edicao dos Produtos Internos </h1></center>");
        echo alerta();
        echo "<p><b>Busca:</b><br/><input type='text' name='busca' id='busca' />";
        echo "<div id='opcoes-itens'></div>";
        echo "<input type='hidden' name='cod' id='cod' value='-1' />";
        echo "<p><b>Princ&iacute;pio Atual:</b></br><input type='text' name='nomeAtual' id='nomeAtual' class='OBG' size='80' />";
        echo "<p><b>Princ&iacute;pio Novo:</b></br><input type='text' name='nome' id='nome' class='OBG' size='80' />";
        echo "<br/><button id='add-item' tipo='' class='incluir_produto_interno' >+ Editar</button>";
    }

    private function incluir_diluente() {
        echo "<div id='dialog-incluir-diluente' title='Incluir Nebuliza��o'>";
        echo "<center><h3>nebuliza��o apenas com diluente? !</h3></center>";
        //      echo "<form action='imprimir.php' method='post'><input type='hidden' value='-1' name='prescricao'><center><button>Imprimir</button></center></form>";
        echo "<br/><center>Enviar email de aviso?</center>";
        echo "</div>";
    }

    //! Método de controle, determina se a prescrição será nova, nova baseada em antiga ou um prescrição salva.

    private function prescricoes($id, $data, $antiga, $inicio, $fim, $salva, $tipo, $id_cap, $edt, $manter_data, $ultima,$idExame,$criador,$intercorrencia = null) {

        $nova_tabela = 1;


        if ($salva != NULL) {
            $this->load_salva($salva);
            return;
        }
        if ($tipo == 1) {
            $this->presc_emergencia($id, $data, $inicio, $fim, $tipo);
        } else {
            if ($id == NULL || $id == "-1") {
                echo "<center><h1>Prescri&ccedil;&otilde;es</h1></center>";
                echo "<div id='div-iniciar-prescricao' >";
                echo alerta();
                echo "<center><h3>Prescri&ccedil;&otilde;es Salvas</h3></center>";
                $this->prescricoes_salvas();
                echo "<center><h3>Prescri&ccedil;&atilde;o Nova</h3></center>";


                echo "<form method='post' >";
                echo $this->tipo_prescricao();
                echo "<div id='container-presc'>";
                $this->pacientes();
                echo $this->periodo();
//      echo "<b>Data:</b><input type='text' name='data' value='$data' maxlength='10' size='10' class='OBG data' />";
                echo "<p><b>&Uacute;ltimas prescri&ccedil;&otilde;es:</b>";
                echo "<div id='antigas' ></div>";

                echo "</div>"; # Fim do div container-presc !

                echo "<button id='iniciar-prescricao' >Iniciar</button></form>";

                echo "</div>";
            } else {
                //$this->tipo_presc =  $tipo_presc;
                $this->inicio = $inicio;
                $this->fim = $fim;
                echo "<div id='corpo-prescricao'>";
                echo "<div id='dialog-salvo' title='Finalizada'>";
                echo "<center><h3>Prescri&ccedil;&atilde;o salva com sucesso!</h3></center>";
//      echo "<form action='imprimir.php' method='post'><input type='hidden' value='-1' name='prescricao'><center><button>Imprimir</button></center></form>";
                echo "<br/><center>Enviar email de aviso?</center>";
                echo "</div>";
                echo "<div id='equip_item' title='Equipamento vs Itens Prescritos' ></div>";
                echo "<div id='dialog-imprimir' title='Imprimir'>";
                    echo "<center><h3>Visualizar Prescri&ccedil;&atilde;o?</h3></center>";
                    echo "<form action='imprimir.php' method='post' target='_blank'>
<input type='hidden' value='-1' name='prescricao'>
<input type='hidden' value='-1' name='empresa'><center></center></form>";
                    echo "</div>";
                ///jeferson

                echo $this->formulario();
                echo $this->formulario_material();
                echo "<div id='aviso' title='Avisos' ></div>";
                echo "<center><h1>Nova Prescri&ccedil;&atilde;o</h1></center>";
                $pnome = $this->pacienteNome($id);
                echo "<center><h2>Paciente: {$pnome}</h2></center>";
                $sqlTpoMedicamento = "SELECT
planosdesaude.nome,
planosdesaude.tipo_medicamento,
clientes.empresa
FROM
clientes
INNER JOIN planosdesaude ON clientes.convenio = planosdesaude.id
WHERE
clientes.idClientes = $id ";
              $result=  mysql_query($sqlTpoMedicamento);
                $tipoMedicamento = mysql_result($result,0,1);
                $nomePlano = mysql_result($result,0,0);
                $empresaId = mysql_result($result,0,2);
                echo "<div id='prescricao' paciente='{$id}' plano='$nomePlano' medicamentoAceito='$tipoMedicamento'
                            pnome='{$pnome}' data='{$data}' inicio='{$inicio}' fim='{$fim}'
                            tipo_presc='{$tipo}' id_cap='{$id_cap}' criador='{$criador}'
                             presc='{$antiga}' edt='{$edt}' manter_data='{$manter_data}'
                             idexame='{$idExame}' idIntercorrencia='{$intercorrencia}'
                             empresa-id='{$empresaId}'>";
                if($tipo == 3 ){
                    echo"
<div id='div-tipo-aditivo'>
<p>
                         <label><b>Selecione o tipo de prescrição Aditiva:</b></label>
                         <br>
                         <select name='tipo-aditiva' id='tipo-aditiva' class='OBG'>";
                         if(!empty($idExame)){
                             echo "<option value='LABORATORIAL'>Laboratórial </option>";
                         }else if(!empty($intercorrencia)){
                             echo "<option value='INTERCORRÊNCIA'>Intercorrência </option>";

                         }else  {
                            echo "<option value=''>Selecione</option>
                             <option value='NOVO ITEM'>Novo Item</option>
                             <option value='AJUSTE'>Ajuste</option>
                             ";
                            }
                        echo "</select>
                    </p>";
                     if(empty($idExame)){
                         echo "
<p class='container-motivo-ajuste'>
                                    <label><b>Motivo do ajuste:</b></label>
                                    <br>
                                    <textarea id='motivo-ajuste-aditivo' name='motivo-ajuste-aditivo' cols='60'></textarea>
                                </p>";

                     }
                     echo '</div>';

                }


                echo "<ul>";
                echo "<li><a href='#div-repouso' ><span>Repouso</span></a></li>";
                echo "<li><a href='#div-dieta' ><span>Dieta</span></a></li>";
                echo "<li><a href='#div-cuidados' ><span>Cuidados Especiais</span></a></li>";
                echo "<li><a href='#div-soros' ><span>Soros e Eletr&oacute;litos</span></a></li>";
                echo "<li><a href='#div-antibio-inj' ><span>Antibi&oacute;ticos Injet&aacute;veis</span></a></li>";
                echo "<li><a href='#div-inj-ev' ><span>Injet&aacute;veis IV</span></a></li>";
                echo "<li><a href='#div-inj-im' ><span>Injet&aacute;veis IM</span></a></li>";
                echo "<li><a href='#div-inj-sc' ><span>Injet&aacute;veis SC</span></a></li>";
                echo "<li><a href='#div-drogas-inalatorias' ><span>Drogas Inalat&oacute;rias</span></a></li>";
                echo "<li><a href='#div-nebulizacao' ><span>Nebuliza&ccedil;&atilde;o</span></a></li>";
                echo "<li><a href='#div-drogas-orais' ><span>Drogas Orais/Enterais</span></a></li>";
                echo "<li><a href='#div-drogas-topicas' ><span>Drogas T&oacute;picas</span></a></li>";
                //echo "<li><a href='#div-formulas' ><span>F&oacute;rmulas</span></a></li>";
                //echo "<li><a href='#div-drogas-ativas' ><span>Suspender</span></a></li>";
                echo "<li><a href='#div-oxigenoterapia' ><span>Oxigenoterapia</span></a></li>";
                echo "<li><a href='#div-dietas-orais' ><span>Dietas Orais/Enterais/Parenterais</span></a></li>";
                //materiais 2013-06-11
                echo "<li><a href='#div-materiais' ><span>Materiais</span></a></li>";
                if($tipo == '3'){
                    echo "<li><a href='#div-gastrostomia' ><span>Procedimentos em Gastrostomia</span></a></li>";
                }

                echo "</ul>";

                if ($ultima == "true" && $tipo == 4) {//Usar última prescrição
                    //if($ultima == "true" ){
                    //$sql_ultima = "SELECT max(id) FROM `prescricoes` WHERE `paciente`={$id} AND Carater IN (2, 4)";
                    $sql_ultima = "SELECT max(id) FROM `prescricoes` WHERE `paciente`={$id} AND Carater = 4";
                    $resultado_ultima = mysql_query($sql_ultima);

                    $prescricao = mysql_result($resultado_ultima, 0);

                    $nova_tabela = 0;
                } elseif ($antiga <> -1) {
                    $prescricao = $antiga;
                    $nova_tabela = 0;
                } else {
                    $prescricao = $antiga;
                    $nova_tabela = 1;
                }

                //Caso de edição
                if ($edt == 1) {
                    $nova_tabela = 0;
                }

                if ($manter_data == "") {
                    $manter_data = 0;
                }

                $tabela = $this->load_table($prescricao, $id, $manter_data);
                $this->repouso();
                $this->dieta();
                $this->equipe_multidisciplinar();
                $this->soros_eletrolitos();
                $this->antibioticos_injetaveis();
                $this->injetaveis_ev();
                $this->injetaveis_im();
                $this->injetaveis_sc();
                $this->drogas_inalatorias();
                $this->nebulizacao();
                $this->drogas_orais();
                $this->drogas_topicas();
                //$this->formulas();
                $this->oxigenoterapia();
                //$this->drogas_ativas($id);
                $this->dietas_orais();
                //materiais 2013-06-11
                $this->materiais();
                if($tipo == '3') {
                    $this->procedimentosGastrostomia();
                }

                echo "</div>";                
                echo "<br/><button id='rem-item' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Remover</span></button></span><br><br>";
                echo "<div>";
                echo "<form><fieldset ><legend>LEGENDA:</legend>";
                echo "<center><label><FONT COLOR='#708090'>PALAVRAS NA COR CINZA: " . htmlentities("ÍTENS DA PRESCRIÇÃO") . " ANTERIOR</font color></label> <br/>";
                echo "PALAVRAS NA COR PRETA: "  . htmlentities("ÍTENS DA PRESCRIÇÃO") . " ATUAL <br/></center>";
                echo "</fieldset></form>";
                echo "</div>";
                //echo "<button id='rem-item' >remover</button><br/>";

                if ($nova_tabela === 1) {

                    echo "<br>";

                    echo "<table width='100%' class='mytable' fim='{$this->fim}' inicio='{$this->inicio}' id='tabela-itens'><thead><tr><th><b>Itens prescritos</b></th><th width='1%'><input type='checkbox' name='select' id='checktodos'></th></tr>";


                    echo "<tr></tr>";

                    for ($t = 0; $t <= sizeof($this->abas); $t++) {
                        $aba = $this->abas[$t];
                        echo "<tr  id='itens-$t' ></tr>";
                    }
                    echo "</thead>";
                    echo "</table>";

                    echo "<br>";
                } else {
                    //echo "prescricao $prescricao, id $id, manter_data $manter_data";
                    echo $tabela;
                }


                echo "</div>";
                if($tipo != 4){
                    echo "<button id='salvar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
                }
                echo "<button id='finalizar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Finalizar</span></button></span>";
                echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
                Paciente::showVerificacaoSugestoes('prsc', $id_cap, $id, $criador, $num_sugestao);
                echo "<input type='hidden' name='num_sugestao'  value='" . (isset($num_sugestao) ? $num_sugestao : 0) . "' />";
                //echo "<button id='salvar'>Salvar</button>&nbsp;&nbsp;&nbsp;<button id='finalizar' >Finalizar</button>";
            }
        }//fim do else teste tipo ==1
    }

    //! Busca prescrições finalizadas.
    private function buscar() {
        if(isset($_GET['msg']) and $_GET['msg'] !== ''){ 
            echo "<center><h4 style='color:#fff !important; background-color:#8b0101; border: 1px solid #8b0101;'>".base64_decode($_GET['msg'])."</h4></center>";
        }
        echo "<center><h1>Prescri&ccedil;&otilde;es</h1></center>";
        echo "<fieldset style='margin:0 auto;'>";
        echo "<div id='div-busca' >";

        echo $this->pre_prescricao();
        echo alerta();
        echo "<div style='float:left;width:400px;'>";
        echo "<center><h2 style='background-color:#8B0101;color:#ffffff;height:25px;'><label style='vertical-align: middle;'>PRESCRI&Ccedil;&Otilde;ES SALVAS</label></h2></center>";
        $this->prescricoes_salvas();

        echo "</div>";
        echo "<div style='float:right;width:500px;'>";
        echo "<center><h2 style='background-color:#8B0101;color:#ffffff;height:25px;vertical-align: middle;'><label style='vertical-align: middle;'>PRESCRI&Ccedil;&Otilde;ES</label></h2></center>";
        echo $this->pacientes();
        echo $this->periodo();
        echo "<div class='popover-content'></div>";

        echo "<p>
                <button id='buscar' 
                        class='validacao_periodo_busca ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' 
                        role='button' 
                        aria-disabled='false' >
                        <span class='ui-button-text'>Buscar</span>
                </button>";


            if(isset($_SESSION['permissoes']['medico']['medico_prescricao']['medico_prescricao_novo']) || $_SESSION['adm_user'] == 1) {
                echo "<button name='antigas' id='novoBtn' paciente_btn_nova='' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Prescrever</span></button>";
            }

        echo "</div>";

        echo "</div>";
        echo "</fieldset>";
        echo "<div id='div-resultado-busca'></div>";
    }

    //! Exibe os detalhes de um prescrição finalizada.
    private function detalhes($p,$idp) {
        echo "<center><h1>Visualizar Prescri&ccedil;&atilde;o</h1></center>";
        $r = new Paciente();
        $r->cabecalho($idp);

        $responsePrescricao = \App\Models\Medico\Prescricao::getById($p);





        if(!empty($responsePrescricao['motivo_ajuste'])){
            echo  "
<div class='ui-state-highlight' style='padding: 5px;'>
                <span class='ui-icon  ui-icon-alert icon-inline' ></span>
                <b>Motivo do Ajuste da Prescrição Aditiva: </b> 
                {$responsePrescricao['motivo_ajuste']}
            </div>
            <br>
";
        }
        echo "<table width=100% class='mytable'><thead><tr>";
        echo "<th><b>Itens prescritos</b></th>";
        echo "</tr></thead>";
        $sql = "SELECT v.via as nvia, f.frequencia as nfrequencia,i.*,  i.inicio,
    		i.fim, DATE_FORMAT(i.aprazamento,'%H:%i') as apraz, u.unidade as um,
    		p.empresa, p.paciente, p.motivo_ajuste 
    		FROM itens_prescricao as i LEFT OUTER JOIN frequencia as f ON (i.frequencia = f.id) 
    		inner join prescricoes as p on (i.idPrescricao = p.id)
    		LEFT OUTER JOIN viaadm as v ON (i.via = v.id) 
    		LEFT OUTER JOIN umedidas as u ON (i.umdose = u.id) 
    		WHERE idPrescricao = '$p'  
    		ORDER BY tipo,inicio,aprazamento,nome,apresentacao ASC";
        $result = mysql_query($sql);
        $n = 0;
        while ($row = mysql_fetch_array($result)) {
            if ($n++ % 2 == 0)
                $cor = '#E9F4F8';
            else
                $cor = '#FFFFFF';

            $tipo = $row['tipo'];
            $i = implode("/", array_reverse(explode("-", $row['inicio'])));
            $f = implode("/", array_reverse(explode("-", $row['fim'])));
            $aprazamento = "";
            if ($row['tipo'] != "0")
                $aprazamento = $row['apraz'];
            $linha = "";
            if ($row['tipo'] != "-1") {
                $aba = $this->abas[$row['tipo']];
                $dose = "";
                if ($row['dose'] != 0)
                    $dose = $row['dose'] . " " . $row['um'];
                $linha = "<b>$aba:</b> {$row['nome']} {$row['apresentacao']} {$row['nvia']} {$dose} {$row['nfrequencia']} Per&iacute;odo: de $i até $f OBS: {$row['obs']}";
            }
            else
                $linha = $row['descricao'];
            echo "<tr bgcolor={$cor} >";
            echo "<td>$linha</td>";
            echo "</tr>";

            $idPaciente = $row['paciente'];
            $empresaId = $row['empresa'];
        }
        echo "</table>";
        $responsePaciente = ModelPaciente::getById($idPaciente);
        $empresaRelatorio = empty($empresaId) ? $responsePaciente['empresa'] : $empresaId;
        echo "
        <button id='' empresa-relatorio='{$empresaRelatorio}' caminho='/medico/imprimir.php?prescricao={$p}' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'><span class='ui-button-text'>Imprimir</span></button>";
        echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
    }

    //! Controla qual página será exibida ao usuário.
    public function view($v){
        switch ($v) {
          case "menu":
            unset($_SESSION['paciente']);
            $this->menu();
            break;
          case "indicadores":
            if (isset($_GET['rel']) && $_GET['rel'] != '') {
              switch ($_GET['rel']) {
                case 'erros-ficha-avaliacao':
                  $this->addFiltersRelatorioIndicador($_GET['rel']);
                  break;
                case 'erros-prescricao':
                  $this->addFiltersRelatorioIndicador($_GET['rel']);
                  break;
              }
            } else
              $this->menuIndicadores();
            break;
            case "relatorios":
                $this->menuRelatorios();
                break;
          case "prescricoes":

            $id = NULL;
            $tipo = null;
            $antiga = NULL;
            $data = date("d/m/Y");
            $pantiga = -1;
            $inicio = NULL;
            $fim = NULL;
            $salva = NULL;
            $manter_data = NULL;
            $id_cap = NULL;
            $edt = NULL;
            $idExame = NULL;
            $criador = NULL;
            $intercorrencia = NULL;
              $ultima = NULL;
          if (isset($_POST['idexame']))
              $idExame = $_POST['idexame'];
            if (isset($_POST['paciente']))
              $id = $_POST['paciente'];
              if (isset($_GET['paciente']))
                  $id = $_GET['paciente'];
            if (isset($_POST['tipo']))
              $tipo = $_POST['tipo'];
              if (isset($_GET['tipo']))
                  $tipo = $_GET['tipo'];
            if (isset($_GET["data"]))
              $data = $_GET["data"];
            if (isset($_POST["antigas"]))
              $antiga = $_POST["antigas"];
            if (isset($_POST["inicio"]))
              $inicio = $_POST["inicio"];
              if (isset($_POST["fim"]))
                  $fim = $_POST["fim"];
              if (isset($_GET["inicio"]))
                  $inicio = $_GET["inicio"];
              if (isset($_GET["fim"]))
                  $fim = $_GET["fim"];
            if (isset($_GET["salva"]))
              $salva = $_GET["salva"];
            if (isset($_POST['id_cap']))
              $id_cap = $_POST['id_cap'];
            if (isset($_POST['id_cap']))
              $id_cap = $_POST['id_cap'];
            if (isset($_POST['manter_data']))
              $manter_data = $_POST['manter_data'];
            if (isset($_REQUEST["edt"]))
              $edt = $_REQUEST["edt"];
            if (isset($_REQUEST["ultima"]))
              $ultima = $_REQUEST["ultima"];
              if (isset($_REQUEST["intercorrencia"]))
                  $intercorrencia = $_GET["intercorrencia"];

            $data_inicio = \DateTime::createFromFormat('d/m/Y', $inicio);
            $data_final = \DateTime::createFromFormat('d/m/Y', $fim);
           
            if ($data_inicio <= $data_final)
               // var_dump($id, $data, $antiga, $inicio, $fim, $salva, $tipo, $id_cap, $edt, $manter_data, $ultima,$idExame,$criador,$intercorrencia);
              $this->prescricoes($id, $data, $antiga, $inicio, $fim, $salva, $tipo, $id_cap, $edt,
                  $manter_data, $ultima,$idExame,$criador,$intercorrencia);
            else
              header("Location: /medico/?view=buscar&msg=" . base64_encode('A data inicial é maior que a final. Corrija a data para prosseguir.'));

            break;
          case "destroi":
            $p = new Paciente();
            $p->teste_session();
            break;
          case "prescricao-avaliacao":
            $id = NULL;
            $tipo = null;
            $antiga = NULL;
            $data = date("d/m/Y");
            $pantiga = -1;
            $inicio = NULL;
            $fim = NULL;
            $salva = NULL;
            $ultima = false; //Usar a última prescrição de avaliação
            if (isset($_REQUEST['paciente']))
              $id = $_REQUEST['paciente'];
            if (isset($_REQUEST['tipo']))
              $tipo = $_REQUEST['tipo'];
            if (isset($_REQUEST["data"]))
              $data = $_REQUEST["data"];
            if (isset($_REQUEST["antigas"]))
              $antiga = $_REQUEST["antigas"];
            if (isset($_REQUEST["inicio"]))
              $inicio = $_REQUEST["inicio"];
            if (isset($_REQUEST["fim"]))
              $fim = $_REQUEST["fim"];
            if (isset($_REQUEST["salva"]))
              $salva = $_REQUEST["salva"];
            if (isset($_REQUEST["id_cap"]))
              $id_cap = $_REQUEST["id_cap"];
            if (isset($_REQUEST["edt"]))
              $edt = $_REQUEST["edt"];
            if (isset($_REQUEST["ultima"]))
              $ultima = $_REQUEST["ultima"];
            if (isset($_REQUEST["criador"]))
              $criador = $_REQUEST["criador"];
            $this->prescricoes($id, $data, $antiga, $inicio, $fim, $salva, $tipo, $id_cap, $edt, null, $ultima,null, $criador);
            break;
          case "editar-prescricao-avaliacao":
            $id = NULL;
            $tipo = null;
            $antiga = NULL;
            $data = date("d/m/Y");
            $pantiga = -1;
            $inicio = NULL;
            $fim = NULL;
            $salva = NULL;
            $criador = NULL;
            $ultima = false; //Usar a última prescrição de avaliação
            if (isset($_REQUEST['paciente']))
              $id = $_REQUEST['paciente'];
            if (isset($_REQUEST['tipo']))
              $tipo = $_REQUEST['tipo'];
            if (isset($_REQUEST["data"]))
              $data = $_REQUEST["data"];
            if (isset($_REQUEST["antigas"]))
              $antiga = $_REQUEST["antigas"];
            if (isset($_REQUEST["inicio"]))
              $inicio = $_REQUEST["inicio"];
            if (isset($_REQUEST["fim"]))
              $fim = $_REQUEST["fim"];
            if (isset($_REQUEST["salva"]))
              $salva = $_REQUEST["salva"];
            if (isset($_REQUEST["id_cap"]))
              $id_cap = $_REQUEST["id_cap"];
            if (isset($_REQUEST["edt"]))
              $edt = $_REQUEST["edt"];
            if (isset($_REQUEST["ultima"]))
              $ultima = $_REQUEST["ultima"];
            if (isset($_REQUEST["criador"]))
              $criador = $_REQUEST["criador"];
            $this->prescricoes($id, $data, $antiga, $inicio, $fim, $salva, $tipo, $id_cap, $edt, null, $ultima,null, $criador);
            break;
          case "buscar":
              ini_set('display_errors', 1);
              if(!isset($_SESSION['permissoes']['medico_prescricao'])) {
                http_response_code(401);
              }
            $this->buscar();
            break;
          case "problemas_ativos":

            $this->problemas_ativos();
            break;
          case "desc_medico":
            $this->desc_medico();
            break;
          case "reparar_produto_interno":
            $this->reparar_produto_interno();
            break;
          case "associar_medicamento":
            $associar = new associacao_catalogo;
            $associar->buscar_medicamento();
            break;
          case "detalhes":
            if (isset($_GET['p']) && isset($_GET['idp']))
              $this->detalhes($_GET['p'], $_GET['idp']);
            else
              $this->menu();
            break;
          case "paciente_med":

            if (isset($_GET['act'])) {
              switch ($_GET['act']) {
                case "listar":
                  $p = new Paciente();
                  $p->listar();
                  break;
                case "novo":
                  $p = new Paciente();
                  $p->form();
                  break;
                case "editar":
                  if (isset($_GET['id'])) {
                    $p = new Paciente();
                    $p->form($_GET['id']);
                  }
                  break;
                case "show":
                  if (isset($_GET['id'])) {
                    $p = new Paciente();
                    $p->form($_GET['id'], 'readonly=readonly');
                  }
                  break;
                case "newcapmed":
                  if (isset($_GET['id'])) {
                    $p = new Paciente();
                    $p->nova_capitacao_medica($_GET['id']);
                  }
                  break;
                case "newfichaevolucao":
                  if (isset($_GET['id'])) {
                    $p = new Paciente();
                    $p->nova_evolucao_medica($_GET['id']);
                  }
                  break;
                case "score_paciente":
                  if (isset($_GET['id'])) {
                    $p = new Paciente();
                    $p->score_paciente($_GET['id'], $_GET['tipo_score']);
                  }
                  break;
                case "listar_score_paciente":
                  if (isset($_GET['id'])) {
                    $p = new Paciente();
                    $p->listar_score_paciente($_GET['id'], $_GET['convenio']);
                  }
                  break;
                case "listfichaevolucao":
                  if (isset($_GET['id'])) {
                    $p = new Paciente();
                    $p->listar_evolucao_medica($_GET['id']);
                  }
                  break;
                case "listcapmed":
                  if (isset($_GET['id'])) {
                    $p = new Paciente();
                    $p->listar_capitacao_medica($_GET['id']);
                  }
                  break;
                case "listavaliacaenfermagem":
                  if (isset($_GET['id'])) {
                    $p = new Paciente();
                    $p->listar_ficha_avaliacao_enfermagem($_GET['id']);
                  }
                  break;
                case "capmed":
                  if (isset($_GET['id'])) {
                    $p = new Paciente();
                    $p->detalhes_capitacao_medica($_GET['id']);
                  }
                  break;
                case "fichaevolucao":
                  if (isset($_GET['id'])) {
                    $p = new Paciente();
                    $p->detalhes_ficha_evolucao_medica($_GET['id']);
                  }
                  break;
                case "editarprescricao":
                  if (isset($_GET['paciente'])) {
                    $p = new Paciente();
                    $p->editar_prescricao($_GET['paciente']);
                  }
                  break;
                case "editar_capmed":
                  if (isset($_GET['id'])) {
                    $p = new Paciente();
                    $p->detalhes_capitacao_medica_editar($_GET['id'], 'editar');
                  }
                  break;
                case "anterior_capmed":
                  if (isset($_GET['id'])) {
                    $p = new Paciente();
                    $p->detalhes_capitacao_medica_editar($_GET['id']);
                  }
                  break;
                case "anterior_evolucao":
                  if (isset($_GET['id'])) {
                    $p = new Paciente();
                    $p->anterior_evolucao_medica($_GET['id']);
                  }
                  break;
								case "editar_evolucao":
									if (isset($_GET['id'])) {
										$p = new Paciente();
										$p->anterior_evolucao_medica($_GET['id'], "editar");
									}
									break;
                case "detalhescore":
                  if (isset($_GET['id']) && isset($_GET['tipo']) && isset ($_GET['paciente_id'])) {
                    $p = new Paciente();
                    $p->detalhe_score($_GET['id'], $_GET['tipo'], $_GET['paciente_id']);
                  }
                  break;
                case "detalhesprescricao":
                  if (isset($_GET['id']) && isset($_GET['idpac'])) {
                    $p = new Paciente();
                    $p->detalhe_prescricao($_GET['id'], $_GET['idpac'], $_GET['medico']);
                  }
                  break;
              }
            }//if


            break;
          case "relatorio":
            if (isset($_GET['opcao'])) {
              switch ($_GET['opcao']) {
                case "1":
                  if (isset($_GET['idp']) && !isset($_GET['idr'])) {
                    $r = new Relatorios();
                    $r->relatorio_alta($_GET['idp']);
                  } elseif (isset($_GET['idr'])) {
                    $r = new Relatorios();
                    $r->relatorio_alta($_GET['id'], $_GET['idr'], $_GET['editar']);
                  } else {
                    $r = new Relatorios();
                    $r->listar_alta_medica($_GET['id']);
                  }
                  break;
                case "2":
                  $r = new Relatorios();
                  $r->listar_relatorio_aditivo($_GET['id']);
                  break;
                case "3":
                  if (isset($_GET['idp']) && !isset($_GET['idr'])) {
                    $r = new Relatorios();
                    $r->relatorio_prorrogacao($_GET['idp']);
                  } elseif (isset($_GET['idr'])) {
                    $r = new Relatorios();
                    $r->relatorio_prorrogacao($_GET['id'], $_GET['idr'], $_GET['editar']);
                  } else {
                    $r = new Relatorios();
                    $r->listar_prorrogacao_medica($_GET['id']);
                  }
                  break;
                case "4":
                  if (isset($_GET['idp']) && !isset($_GET['idr'])) {
                    $r = new Relatorios();
                    $r->relatorio_intercorrencia($_GET['idp']);
                  } elseif (isset($_GET['idr'])) {
                    $r = new Relatorios();
                    $r->relatorio_intercorrencia($_GET['id'], $_GET['idr'], $_GET['editar']);
                  } else {
                    $r = new Relatorios();
                    $r->listar_intercorrencia_medico($_GET['id']);
                  }
                  break;
                case "5":
                  if (isset($_GET['id'])) {
                    $r = new RelatorioDeflagradoMedico();
                    $r->listarRelatorioDeflagradoMedico($_GET['id']);
                  }
              }
            }
            /* $r =new Relatorios();
              $r->relatorio_alta($_GET['id']); */
            break;
            case 'novo-rel-deflagrado':
                $r = new RelatorioDeflagradoMedico();
                $r->fichaRelatorioDeflagradoMedico($idPaciente =$_GET['id'],$idRelatorioDeflagrado=null,$editarRelatorioDeflagrado=0,$visualizarRelatorioDeflagrado =null);

            case "visualizar-rel-deflagrado":
            if (isset($_GET['idr']) && isset($_GET['id'])) {

              $r = new RelatorioDeflagradoMedico();
              $r->fichaRelatorioDeflagradoMedico($idPaciente =$_GET['id'],$idRelatorioDeflagrado=$_GET['idr'],$editarRelatorioDeflagrado=0,$visualizarRelatorioDeflagrado =1);

             }
                break;
          case "usar-novo-rel-deflagrado":
            if (isset($_GET['idr']) && isset($_GET['id'])) {

              $r = new RelatorioDeflagradoMedico();
              $r->fichaRelatorioDeflagradoMedico($idPaciente =$_GET['id'],$idRelatorioDeflagrado=$_GET['idr'],$editarRelatorioDeflagrado=0,$visualizarRelatorioDeflagrado = null);

            }
            break;
          case "edt-rel-deflagrado":
            if (isset($_GET['idr']) && isset($_GET['id'])) {

              $r = new RelatorioDeflagradoMedico();
              $r->fichaRelatorioDeflagradoMedico($idPaciente =$_GET['id'],$idRelatorioDeflagrado=$_GET['idr'],$editarRelatorioDeflagrado=1,$visualizarRelatorioDeflagrado = null);

            }
            break;
            case "visualizar_rel_alta":
                $r = new Relatorios();
                $r->visualizar_rel_alta($_GET['id'], $_GET['idr']);
                break;
            case "visualizar_rel_prorrogacao":
                $r = new Relatorios();
                $r->visualizar_rel_prorrogacao($_GET['id'], $_GET['idr']);
                break;
            case "visualizar_rel_intercorrencia":
                $r = new Relatorios();
                $r->visualizar_rel_intercorrencia($_GET['id'], $_GET['idr']);
                break;
            case "visualizar_relatorio_aditivo":
                $r = new Relatorios();
                $r->visualizar_relatorio_aditivo($_GET['id'], $_GET['idr']);
                break;
        }
    }

}

?>