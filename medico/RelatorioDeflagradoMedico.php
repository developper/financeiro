<?php

use \App\Models\Administracao\Paciente as ModelPaciente;

class RelatorioDeflagradoMedico
{
    public function listarRelatorioDeflagradoMedico($idPaciente)
    {  if(!isset($idPaciente) || !is_numeric($idPaciente)){
        echo "Aconteceu um erro, tente novamente.";
        return;

    }
        $listaRelatorioDeflagardoPaciente = $this->pesquisaRelatorioDeflagradoParaPaciente($idPaciente);

        if($listaRelatorioDeflagardoPaciente == false){
            if(!isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_deflagrado']['medico_paciente_deflagrado_novo']) && $_SESSION['adm_user'] != 1) {
                echo "<b>Paciente não possui Relatório de Deflagarado.</b>";
            }else{

                $this->fichaRelatorioDeflagradoMedico($idPaciente,$idRelatorioDeflagrado = null,
                    $editarRelatorioDeflagrado =0,$visualizarRelatorioDeflagrado = null);

            }
        }else{
            echo $listaRelatorioDeflagardoPaciente;

        }
        return;

    }

    public function pesquisaRelatorioDeflagradoParaPaciente($idPaciente)
    {
        if (!isset($idPaciente) || !is_numeric($idPaciente)) {
            $html= "Aconteceu um erro, tente novamente.";
            return;

        }
        $sqlPesquisaRelatorioDeflagradoPaciente = "SELECT
                                              DATE_FORMAT(relatorio_deflagrado_med.inicio,'%d/%m/%Y') as inicio,
                                              DATE_FORMAT(relatorio_deflagrado_med.fim,'%d/%m/%Y') as fim ,
                                              usuarios.nome,
                                              usuarios.tipo as utipo,
                                              usuarios.conselho_regional,
                                              DATE_FORMAT(relatorio_deflagrado_med.`data`,'%d/%m/%Y %H:%i:%s') as datapt,
                                              relatorio_deflagrado_med.id,
                                              relatorio_deflagrado_med.`data`
                                              FROM
                                              relatorio_deflagrado_med
                                              INNER JOIN usuarios ON relatorio_deflagrado_med.usuario_id = usuarios.idUsuarios
                                              WHERE
                                              relatorio_deflagrado_med.cliente_id = '$idPaciente' AND
                                              relatorio_deflagrado_med.cancelado_em IS NULL AND
                                              relatorio_deflagrado_med.editado_por IS NULL
                                                ORDER BY relatorio_deflagrado_med.id DESC ";

        $resultPesquisaRelatorioDeflagradoPaciente = mysql_query($sqlPesquisaRelatorioDeflagradoPaciente);
        if (!$resultPesquisaRelatorioDeflagradoPaciente) {
            echo  'Erro ao fazer pesquisa dos Relatórios Deflagrados';
            return error;
        }

        if(mysql_num_rows($resultPesquisaRelatorioDeflagradoPaciente) == 0) {
            return false;
        }
        $flag = true;

        while ($row = mysql_fetch_array($resultPesquisaRelatorioDeflagradoPaciente)) {
            $compTipo = $row['conselho_regional'];
            $empresa = $row['empresa_rel'];
            $plano = $row['plano_rel'];

            if ($flag) {
                echo "<center><h1>" . htmlentities("Relatório") . " Deflagrado Médico</h1></center>";

                Paciente::cabecalho($idPaciente, $empresa, $plano);


                if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_deflagrado']['medico_paciente_deflagrado_usar_para_nova']) || $_SESSION['adm_user'] == 1) {
                    $usarParaNovoRelatorioDeflagrado ="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <a href=?view=usar-novo-rel-deflagrado&id={$idPaciente}&idr=" . $row['id'] . ">
              <img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para novo relat&oacute;rio.' border='0'></a>";
                    $data_atual = date('Y/m/d');
                    $prazoEditar = date('Y/m/d', strtotime("+2 days", strtotime($row['data'])));
                    if($data_atual < $prazoEditar && (isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_deflagrado']['medico_paciente_deflagrado_editar']) || $_SESSION['adm_user'] == 1)){
                        $editarRelatorioDeflagrado = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=edt-rel-deflagrado&id={$idPaciente}&idr=" . $row['id'] . "><img src='../utils/edit_16x16.png' title='Editar' border='0'></a>";
                    }
                }
                if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_deflagrado']['medico_paciente_deflagrado_novo']) || $_SESSION['adm_user'] == 1) {
                    $html .= "<button id='novo-rel-deflagrado' paciente-id='{$idPaciente}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'><span class='ui-button-text'>Novo " . htmlentities("Relatório") . "</span></button>";
                }
                $html .= "<table class='mytable' style='width:100%' >
                      <thead>
                       <tr>
                          <th width='35%'>" . htmlentities("Usuário") . "</th>
                          <th>Data</th>
                          <th>Período</th>
                          <th width='10%'>Detalhes</th>
                          <th>" . htmlentities("Opções") . "</th>
                      </tr>
                      </thead>";
                $flag = false;
            }
            $html .= "      <tr>
                          <td>{$row['nome']}{$compTipo}</td>
                          <td>{$row['datapt']}</td>
                          <td>De {$row['inicio']} até {$row['fim']}</td>
                          <td>
                               <a href=?view=visualizar-rel-deflagrado&id={$idPaciente}&idr=" . $row['id'] . ">
                               <img src='../utils/capmed_16x16.png' title='Visualizar'>
                          </td>

                          <td>
                           $usarParaNovoRelatorioDeflagrado
                           $editarRelatorioDeflagrado
                          </td>";
            $usarParaNovoRelatorioDeflagrado ='';
            $editarRelatorioDeflagrado ='';


        }
        $html .= "</table>";


        return $html;

    }

    public function fichaRelatorioDeflagradoMedico($idPaciente,$idRelatorioDeflagrado,$editarRelatorioDeflagrado,$visualizarRelatorioDeflagrado)
    {
        $nomeUser ='';

        if (!isset($idPaciente) || !is_numeric($idPaciente)) {
            echo "Aconteceu um erro, ao tentar gerar o relatório.";
            return;
        }

        if(!empty($idRelatorioDeflagrado) && is_numeric($idRelatorioDeflagrado)){

            $sqlRelDeflagradoMedicoHistClinica="SELECT
                    DATE_FORMAT(relatorio_deflagrado_med.inicio,'%d/%m/%Y') as inicio,
                    DATE_FORMAT(relatorio_deflagrado_med.fim,'%d/%m/%Y') as fim,
                    relatorio_deflagrado_med.historia_clinica,
                    relatorio_deflagrado_med.servicos_prestados,
                    usuarios.nome,
                    empresas.nome AS empresa_rel,
                    empresas.id AS empresa_id,
                    planosdesaude.nome AS plano_rel
                    FROM
                    relatorio_deflagrado_med
                    INNER JOIN usuarios ON relatorio_deflagrado_med.usuario_id = usuarios.idUsuarios
                    LEFT JOIN empresas ON relatorio_deflagrado_med.empresa = empresas.id
                    LEFT JOIN planosdesaude ON relatorio_deflagrado_med.plano = planosdesaude.id
                    WHERE
                    relatorio_deflagrado_med.id= $idRelatorioDeflagrado";
            $result = mysql_query($sqlRelDeflagradoMedicoHistClinica);
            if(!$result){
                echo "erro na consulta";
                return;
            }
            while($row = mysql_fetch_array($result)){
                $dataInicial = $row['inicio'];
                $dataFinal = $row['fim'];
                $historiaClinica = $row['historia_clinica'];
                $servicosPprestados = $row['servicos_prestados'];
                $empresa = $row['empresa_rel'];
                $empresa_id = $row['empresa_id'];
                $convenio = $row['plano_rel'];
                $nomeUser = "<p><b>Responsável pelo relatório : ".$row['nome']."</b></p>";
            }
        }
        $titulo = 'Novo';

        if(!empty($editarRelatorioDeflagrado)){
            $titulo = 'Editar';
        }
        if(!empty($visualizarRelatorioDeflagrado)){
            $titulo = 'Visualizar';
        }


        echo  "<center><h1>" . htmlentities(" {$titulo} Relatório") . " Deflagrado Médico</h1></center>";

        Paciente::cabecalho($idPaciente, $empresa, $convenio);
        if(!empty($visualizarRelatorioDeflagrado)){
            $responsePaciente = ModelPaciente::getById($id_paciente);
            $empresaRelatorio = empty($empresa_id) ? $responsePaciente['empresa'] : $empresa_id;

            $caminho = "/medico/ImprimirRelatorioDeflagrado.php?view=imprimir&id={$idPaciente}&idr={$idRelatorioDeflagrado}";
            echo "<button caminho='{$caminho}' empresa-relatorio='{$empresaRelatorio}' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  role='button' aria-disabled='false'>
                    <span class='ui-button-text'>
                        Imprimir
                    </span>
                  </button>";
        }
        $html  = "<div id='relatorio-deflagrado'>
              <form>";


        $html .= "<input type='hidden' value={$idRelatorioDeflagrado} name='id_relatorio' />";

        $html .= !empty($visualizarRelatorioDeflagrado)? $nomeUser:'';
        echo $html;
        $this->relatorioDeflagardoProblemasAtivos($idPaciente,$idRelatorioDeflagrado);
        $this->relatorioDeflagardoHistoriaClinica($dataInicial,$dataFinal,$historiaClinica,$visualizarRelatorioDeflagrado);
        $this->relatorioDeflagardoServicosPrestados($servicosPprestados,$visualizarRelatorioDeflagrado);

        if(empty($visualizarRelatorioDeflagrado)){
            echo "<br><button id='salvar-rel-deflagrado' data-editar='{$editarRelatorioDeflagrado}' data-paciente='{$idPaciente}'
                     class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'
                     role='button' aria-disabled='false' >
                     <span class='ui-button-text'>Salvar</span>
                    </button>";

        }
        echo "</form></div>";



    }

    public function relatorioDeflagardoProblemasAtivos($idPaciente,$idRelatorioDeflagrado)
    {  if (isset($idPaciente) && empty($idRelatorioDeflagrado)){
        // caso seja uma nova
        $sqlrelatorioDeflagardoProblemasAtivos = "
             SELECT
                    pae.DESCRICAO AS nome,
                    pae.CID_ID,
                    pae. STATUS,
                    pae.OBSERVACAO
              FROM
                    problemaativoevolucao AS pae
              WHERE
                    pae.EVOLUCAO_ID = (
                        SELECT
                          fichamedicaevolucao.ID
                          FROM
                          fichamedicaevolucao
                          WHERE
                            fichamedicaevolucao.PACIENTE_ID = $idPaciente
                          AND fichamedicaevolucao.`DATA` = (
                               SELECT
                                MAX(fichamedicaevolucao.`DATA`)
                               FROM
                                fichamedicaevolucao
                               WHERE
                                fichamedicaevolucao.PACIENTE_ID = $idPaciente
                                                                  ))
               ORDER BY
               pae. STATUS DESC
              ";



    }else{
        $sqlrelatorioDeflagardoProblemasAtivos = "
               SELECT
                  relatorio_deflagrado_med_problema_ativo.cid10_id as CID_ID,
                  relatorio_deflagrado_med_problema_ativo.descricao as nome,
                  relatorio_deflagrado_med_problema_ativo.observacao as OBSERVACAO
            FROM
                  relatorio_deflagrado_med_problema_ativo
            WHERE
                  relatorio_deflagrado_med_problema_ativo.relatorio_deflagrado_med_id ='$idRelatorioDeflagrado' ";

    }

        $html ="<table class='mytable' style='width:95%;'>
              <thead>
                  <tr>
                     <th colspan='3' >Problemas Ativos</th>
                  </tr>
              </thead>
              <tr>
                  <td colspan='3' >
                    <table id='problemas_ativos' style='width:100%;'>
                       <tr style='background-color:#ccc;'>
                             <td width='50%'><b>CID/PROBLEMA</b></td>
                             <td><b>DESCRICAO MEDICA</b></td>
                       </tr>";

        $resultRelatorioDeflagardoProblemasAtivos = mysql_query($sqlrelatorioDeflagardoProblemasAtivos);
        $cont = 1;
        while ($row = mysql_fetch_array($resultRelatorioDeflagardoProblemasAtivos)) {
            $html .= "<tr style='border-bottom: 1px solid #ccc;'>
                             <td class='prob-ativos-deflagrado-medico' desc='{$row['nome']}' cod='{$row['CID_ID']}' width='50%'>
                                         <label>CID - {$row['CID_ID']}  {$row['nome']}</label>
                             </td>
                             <td>
                                         <label>{$row['OBSERVACAO']}</label>
                             </td>
                      </tr>";
        }
        $html  .=" </table>
               </td>
           </tr>
         </table>";
        echo $html;

    }

    public function relatorioDeflagardoHistoriaClinica($dataInicial,$dataFinal,$historiaClinica,$visualizarRelatorioDeflagrado)
    {

        if(empty($visualizarRelatorioDeflagrado)){
            $inico ="<input class=' OBG inicio-periodo' value='{$dataInicial}' type='text' name='inicio' maxlength='10' size='10' />";
            $fim = "<input class='OBG fim-periodo' value='{$dataFinal}' type='text' name='fim' maxlength='10' size='10' />";
            $historiaClinica ="<textarea cols='120' rows='5' class='OBG' name='historia_clinica' id='historia_clinica'>$historiaClinica</textarea>";
        }else{
            $inico = $dataInicial;
            $fim = $dataFinal;

        }
        $html = "<table class='mytable' style='width:95%;'>
                 <thead>
                     <tr>
                       <th colspan='3' >História Clínica</th>
                     </tr>
                 </thead>
                 <tr>
                     <td colspan='3'>
                         <b>" . htmlentities("História Clínica do período") . " </b>
                        {$inico}
                         a
                        {$fim}
                     </td>
                 </tr>
                 <tr>
                     <td colspan='3'>
                         {$historiaClinica}
                     </td>
                 </tr>
              </table>";
        echo $html;
    }
    public function relatorioDeflagardoServicosPrestados($servicosPrestados,$visualizarRelatorioDeflagrado)
    {

        if(empty($visualizarRelatorioDeflagrado)){
            $servicosPrestados ="<textarea cols='120' rows='5' class='OBG' name='servicos_prestados' id='servicos-prestados'>$servicosPrestados</textarea>";
        }
        $html = "<table class='mytable' style='width:95%;'>
                 <thead>
                     <tr>
                       <th colspan='3' >Serviços Prestados</th>
                     </tr>
                 </thead>

                 <tr>
                     <td colspan='3'>
                         {$servicosPrestados}
                     </td>
                 </tr>
              </table>";
        echo $html;
    }


}

