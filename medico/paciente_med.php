<?php

$id = session_id();
if (empty($id))
	session_start();

include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');

use App\Helpers\StringHelper as String;
use App\Models\Administracao\Filial;
use \App\Models\Administracao\Paciente as ModelPaciente;

class DadosPaciente {

	public $codigo = "";
	public $nome = "";
	public $nasc = "";
	public $sexo = "";
	public $plano = NULL;
	public $diagnostico = "";
	public $cid = "";
	public $origem = "";
	public $leito = "";
	public $telPosto = "";
	public $solicitante = "";
	public $especialidade = "";
	public $acomp = "";
	public $cnome = "";
	public $csexo = "";
	public $relcuid = "";
	public $rnome = "";
	public $rsexo = "";
	public $relresp = "";
	public $endereco = "";
	public $bairro = "";
	public $referencia = "";
	public $contato = "";
	public $telResponsavel = "";
	public $telCuidador = "";
	public $enderecoInternacao = "";
	public $bairroInternacao = "";
	public $referenciaInternacao = "";
	public $telDomiciliar = "";
	public $telInternacao = "";
	public $especialidadeCuidador = "";
	public $especialidadeResponsavel = "";
	public $empresa = NULL;
	public $id_cidade_und = "";
	public $cidade_und = "";
	public $id_cidade_internado = "";
	public $cidade_internado = "";
	public $id_cidade_domiciliar = "";
	public $cidade_domiciliar = "";
	public $end_und_origem = "";
	public $num_matricula_convenio = "";

}

class DadosFicha {

	public $usuario = "";
	public $data = "";
	public $pacienteid = "";
	public $empresa = "";
	public $convenio = "";
	public $pacientenome = "";
	public $motivo = "";
	public $alergiamed = "";
	public $alergiaalim = "";
	public $tipoalergiamed = "";
	public $tipoalergiaalim = "";
	public $prelocohosp = "";
	public $prehigihosp = "";
	public $preconshosp = "";
	public $prealimhosp = "";
	public $preulcerahosp = "";
	public $prelocodomic = "";
	public $prehigidomic = "";
	public $preconsdomic = "";
	public $prealimdomic = "";
	public $preulceradomic = "";
	public $objlocomocao = "";
	public $objhigiene = "";
	public $objcons = "";
	public $objalimento = "";
	public $objulcera = "";
	public $shosp = "";
	public $sdom = "";
	public $sobj = "";
	public $modalidade = "";
	public $local_av = "2";
	public $qtdinternacoes = "";
	public $historico = "";
	public $parecer = "0";
	public $justificativa = "";
	public $cancer = "";
	public $psiquiatrico = "";
	public $neuro = "";
	public $glaucoma = "";
	public $hepatopatia = "";
	public $obesidade = "";
	public $cardiopatia = "";
	public $dm = "";
	public $reumatologica = "";
	public $has = "";
	public $nefropatia = "";
	public $pneumopatia = "";
	public $ativo;
	public $idcapmedica = "";
	public $diag_principal = '';
	public $diag_secundario = '';
	public $atend_24 = '';
	public $atend_24_2 = '';
	public $atend_12 = '';
	public $atend_06 = '';
	public $atend_ad = '';
	public $pre_justificativa = '';
	public $plano_desmame = '';
	public $pacintePlano = '';
	public $modeloNead = '';
	public $classificacao_paciente = '';
	public $dataAvaliacao = '';
    public $nomeEmpresa = '';
    public $nomePlano = '';

}

class FichaEvolucao {

	public $usuario = "";
	public $data = "";
	public $pacienteid = "";
	public $pacientenome = "";
	public $pa_sistolica_min = "";
	public $pa_sistolica_max = "";
	public $pa_diastolica_max = "";
	public $pa_diastolica_min = "";
	public $fc_max = "";
	public $fc_min = "";
	public $fr_max = "";
	public $fr_min = "";
	public $temperatura_max = "";
	public $temperatura_min = "";
	public $estd_geral = "";
	public $mucosa = "";
	public $escleras = "";
	public $respiratorio = "";
	public $pa_sistolica = "";
	public $pa_diastolica = "";
	public $fc = "";
	public $fr = "";
	public $temperatura = "";
	public $peso_paciente = "";
	public $novo_exame = "";
	public $impressao = "";
	public $plano_diagnostico = "";
	public $planoterapeutico = "";

}

class Paciente {
	/*     * ***** */

	public function teste_session() {
		unset($_SESSION['empresa_principal']);
		echo 'teste';
		//echo $_SESSION['id_user'].'---';
		//header('location:?view=paciente_med&act=listcapmed&id=83');
	}

	/*     * ****************** */

	private $abas = array('0' => 'Repouso', '1' => 'Dieta', '2' => 'Cuidados Especiais', '3' => 'Soros e Eletr&oacute;litos',
		'4' => 'Antibi&oacute;ticos Injet&aacute;veis', '5' => 'Injet&aacute;veis IV', '6' => 'Injet&aacute;veis IM',
		'7' => 'Injet&aacute;veis SC', '8' => 'Drogas inalat&oacute;rias', '9' => 'Nebuliza&ccedil;&atilde;o',
		'10' => 'Drogas Orais/Enterais', '11' => 'Drogas T&oacute;picas', '12' => 'F&oacute;mulas', '13' => 'Oxigenoterapia', '14' => 'Dieta Enterais', '15' => 'Materiais');
	public $plan;

    public $modeloNead;
    public $classificacao_paciente;

	private function createDialogLocalStorageWarning()
	{
		$html = <<<HTML
<div id="dialog-aviso-localstorage" title="Aviso!" style="display:none;">
	<div class="wrap" style="font-size: 12pt;">
		<p>
			Você possui dados anteriormente armazenados no relatório desse paciente.
			Para continuar usando os dados, clique em <strong>"Manter"</strong>, senão,
			clique em <strong>"Limpar"</strong> para removê-los.
		</p>
	</div>
</div>
HTML;
		return $html;
	}

	private function createDialogSinaisSemanaWarning()
	{
		$html = <<<HTML
<div id="dialog-aviso-sinaissemana" title="Aviso!" style="display:none;">
	<div class="wrap" style="font-size: 12pt;">
		<p>
			O registro da semana está disponível na casa do paciente?
		</p>
	</div>
</div>
HTML;
		return $html;
	}

	private function createDialogExamesComplementaresWarning()
	{
		$html = <<<HTML
<div id="dialog-aviso-exames-complementares" title="Aviso!" style="display:none;">
	<div class="wrap" style="font-size: 12pt;">
		<p>
			Existem novos exames complementares?
		</p>
	</div>
</div>
HTML;
		return $html;
	}

	private function equipe($paciente, $tipo) {
		$has = false;
		$result = mysql_query("SELECT e.*, u.nome FROM equipePaciente as e, usuarios as u WHERE e.funcionario = u.idUsuarios AND u.tipo = '$tipo' AND paciente = '$paciente' AND fim IS NULL ORDER BY nome");
		$cor = false;
		while ($row = mysql_fetch_array($result)) {
			$has = true;
			foreach ($row AS $key => $value) {
				$row[$key] = stripslashes($value);
			}
			if ($cor)
				echo "<tr cod='{$row['id']}' >";
			else
				echo "<tr cod='{$row['id']}' class='odd' >";
			$cor = !$cor;
			echo "<td>{$row['nome']}</td>";
			$inicio = implode("/", array_reverse(explode("-", $row['inicio'])));
			echo "<td>{$inicio}</td>";
			echo "<td>-</td>";
			echo "<td><button class='end' >Finalizar</button></td><td><button class='del' >Remover</button></td>";
			echo "</tr>";
		}
		if (!$has)
			echo "<tr><td colspan='5' ><center><b>Nenhuma equipe está acompanhando o paciente.</b></center></td></tr>";
	}

//equipe

	private function empresas($id, $extra = NULL) {
		$result = mysql_query("SELECT * FROM empresas AND id NOT IN (1, 3, 9, 10) ORDER BY nome");
		$var = "<select name='empresa'  class='COMBO_OBG' {$extra}>";
		$var .= "<option value='-1'></option>";
		while ($row = mysql_fetch_array($result)) {
			if (($id <> NULL) && ($id == $row['id']))
				$var .= "<option value='{$row['id']}' selected>{$row['nome']}</option>";
			else
				$var .= "<option value='{$row['id']}'>{$row['nome']}</option>";
		}
		$var .= "</select>";
		return $var;
	}

//empresas

	public function menu() {
		echo "<a href='?view=paciente_med&act=listar'>Listar</a><br>";
		echo "<a href='?view=paciente_med&act=novo'>Solicita&ccedil;&atilde;o de Avalia&ccedil;&atilde;o</a><br>";
	}

	private function preencheDados($id) {
		$id = anti_injection($id, "numerico");
		$result = mysql_query("SELECT c.*, p.id as plano, cid.codigo as codcid, cid.descricao as descid, c.CIDADE_ID_UND, c.CIDADE_ID_INT, c.CIDADE_ID_DOM,concat(cd.NOME,',',cd.UF) as cidade_und,
    		               (select  concat(x.NOME,',',x.UF) from cidades as x where x.id=c.CIDADE_ID_INT) as cidade_int,
    		               (select  concat(x1.NOME,',',x1.UF) from cidades as x1 where x1.id=c.CIDADE_ID_DOM) as cidade_dom
    					   FROM clientes as c 
    					   LEFT OUTER JOIN planosdesaude as p ON c.convenio = p.id  
    					   LEFT OUTER JOIN cid10 as cid ON c.diagnostico collate utf8_general_ci = cid.codigo collate utf8_general_ci 
    		               LEFT JOIN cidades cd ON c.CIDADE_ID_UND = cd.ID 
    		              
    					   WHERE idClientes = '$id'");
		$dados = new DadosPaciente();

		while ($row = mysql_fetch_array($result)) {
			$dados->codigo = $row['codigo'];
			$dados->dataInternacao = $row['dataInternacao'];
			$dados->nome = $row['nome'];
			$dados->sexo = $row['sexo'];
			$dados->plano = $row['plano'];
			$dados->diagnostico = $row['descid'];
			$dados->cid = $row['codcid'];
			$dados->origem = $row['localInternacao'];
			$dados->leito = $row['LEITO'];
			$dados->solicitante = $row['medicoSolicitante'];
			$dados->especialidade = $row['espSolicitante'];
			$dados->acomp = $row['acompSolicitante'];
			$dados->cnome = $row['cuidador'];
			$dados->csexo = $row['csexo'];
			$dados->relcuid = $row['relcuid'];
			$dados->rnome = $row['responsavel'];
			$dados->rsexo = $row['rsexo'];
			$dados->relresp = $row['relresp'];
			$dados->endereco = $row['endereco'];
			$dados->bairro = $row['bairro'];
			$dados->referencia = $row['referencia'];
			$dados->telPosto = $row['TEL_POSTO'];
			$dados->contato = $row['PES_CONTATO_POSTO'];
			$dados->empresa = $row['empresa'];
			$dados->telResponsavel = $row['TEL_RESPONSAVEL'];
			$dados->telCuidador = $row['TEL_CUIDADOR'];
			$dados->enderecoInternacao = $row['END_INTERNADO'];
			$dados->bairroInternacao = $row['BAI_INTERNADO'];
			$dados->referenciaInternacao = $row['REF_ENDERECO_INTERNADO'];
			$dados->telInternacao = $row['TEL_LOCAL_INTERNADO'];
			$dados->telDomiciliar = $row['TEL_DOMICILIAR'];
			$dados->especialidadeCuidador = $row['CUIDADOR_ESPECIALIDADE'];
			$dados->especialidadeResponsavel = $row['RESPONSAVEL_ESPECIALIDADE'];
			$dados->id_cidade_und = $row['CIDADE_ID_UND'];
			$dados->cidade_und = $row['cidade_und'];
			$dados->id_cidade_internado = $row['CIDADE_ID_INT'];
			$dados->cidade_internado = $row['cidade_int'];
			$dados->id_cidade_domiciliar = $row['CIDADE_ID_DOM'];
			$dados->cidade_domiciliar = $row['cidade_dom'];
			$dados->end_und_origem = $row['END_UND_ORIGEM'];
			$dados->num_matricula_convenio = $row['NUM_MATRICULA_CONVENIO'];

			$dados->nasc = implode("/", array_reverse(explode("-", $row['nascimento'])));
			$dados->cpfCliente =$row['cpf'];

			$dados->cpfRresponsavel =$row['cpf_responsavel'];
			$dados->nascResponsavel = implode("/",array_reverse(explode("-",$row['nascimento_responsavel'])));
			$dados->nascCuidador = implode("/",array_reverse(explode("-",$row['nascimento_cuidador'])));
		}
		return $dados;
	}

	public function form($id = NULL, $visao = '') {
		$dados = new DadosPaciente();
		$modo = "salvar-paciente";
		$titulo = "Solicita��o de Avalia��o";
		if ($id != NULL) {
			$dados = $this->preencheDados($id);
			$modo = "editar-paciente";
			$titulo = "Editar Paciente";
		}
		$visao_sexo = '';
		if ($visao != '') {
			$visao_sexo = 'disabled';
			$titulo = "Detalhes Paciente";
		}
		echo "<input type='hidden' name='paciente' value='{$id}' />";
		echo "<center><h1>" . utf8_encode($titulo) . "</h1></center>";
		echo "<div id='div-novo-paciente'>";
		echo alerta();


		echo "<fieldset style='width:95%;'><legend><b><label style='color:red;'>* </label>Paciente</b></legend><table width='100%'>";
		echo "<tr><tr><td>C&oacute;digo</td><td>Data de Registro</td></tr>";
		echo "<tr><td><input class='OBG' type='text' name='codigo' value='{$dados->codigo}' {$visao} /></td>
    <td><input class='OBG' type='text' name='dataInternacao' value='{$this->PegarData($dados->dataInternacao)}' {$visao} /></td>
    <td><input class='OBG' type='hidden' name='HoraInternacao' value='{$this->PegarHora($dados->dataInternacao)}' {$visao} /></td>
    
  </tr>";
		echo "<tr><td>Nome</td><td>Data de nascimento</td><td>Sexo</td></tr>";
		echo "<tr><td><input class='OBG' type='text' size=50 name='nome' value='{$dados->nome}' {$visao} /></td>";
		echo "<td><input class='data OBG' maxlength='10' size='10' type='text' name='nascimento' value='{$dados->nasc}' {$visao} /></td>";

		echo"<td><select name='sexo'   class='COMBO_OBG' {$visao_sexo}><option value='-1' ></option>";
		echo"<option value='0' " . (($dados->sexo == '0') ? "selected" : "") . " {$visao_sexo} >MASCULINO</option>";
		echo"<option value='1' " . (($dados->sexo == '1') ? "selected" : "") . " {$visao_sexo} >FEMININO</option>";
		echo"</select></td></tr>";

		echo "<tr><td>Conv&ecirc;nio</td><td>N&deg; de Matr&iacute;cula do Cov&ecirc;nio:</td></tr>";
		$r = mysql_query("SELECT p.* FROM planosdesaude as p");
		echo "<tr><td><select name='convenio' class='COMBO_OBG'  {$visao_sexo} >";
		echo "<option value='-1' selected ></option>";
		while ($row = mysql_fetch_array($r)) {
			foreach ($row AS $key => $value) {
				$row[$key] = stripslashes($value);
			}
			if ($row['id'] == $dados->plano)
				echo "<option value='{$row['id']}' selected >{$row['nome']}</option>";
			else
				echo "<option value='{$row['id']}'>{$row['nome']}</option>";
		}
		echo "</select></td><td><input class='OBG'  size='15' type='text' name='num_matricula_convenio' value='{$dados->num_matricula_convenio}' {$visao} /></td></tr>
<tr>
                  <td>
                      <label for='cpf-cliente'>CPF: </label>
                      <input id='cpf-cliente' {$visao} name = 'cpf_cliente' class ='cpf $cpfOBG' value='{$dados->cpfCliente}'>
                      <span id='span-cpf-cliente'  class='text-red'></span>
                  </td>

              </tr></table></fieldset>";
//    echo "<b> Data de cadastro:</b><input class='data' maxlength='10' size='10' type='text' name='internacao' />";
		echo "<fieldset style='width:95%;'><legend><b><label style='color:red;'>* </label>Diagn&oacute;stico Principal</b>.</legend><table width='100%'>";
		///echo "<tr><td><b>Diagn&oacute;stico Principal</b></td></tr>";
		if ($visao == '')
			echo "<tr><td>Buscar Diagn&oacute;stico:</td><td>Diagn&oacute;stico.</td><td>C&oacute;digo.</td></tr><tr><td><input type='text' name='busca-diagnostico' id='busca-diagnostico' {$visao} /></td>";
		echo "<td><input class='OBG' type='text' size=50 name='diagnostico' readonly=readonly value='{$dados->diagnostico}' /></td>";
		echo "<td><input class='OBG' type='text' name='cid-diagnostico' readonly=readonly value='{$dados->cid}' /></td></tr>";
		echo "<tr><td>M&eacute;dico solicitante</td><td>Especialidade</td><td>Acompanhar&aacute; o paciente em domic&iacute;lio (S/N)?</td></tr>";
		echo "<tr><td><input type='text' size=50 name='solicitante' class='OBG' value='{$dados->solicitante}' {$visao} /></td>";
		echo "<td><input type='text' name='sol-especialidade' class='OBG' value='{$dados->especialidade}' {$visao} /></td>";
		echo "<td><input type='text' name='sol-acompanhamento' class='OBG boleano' value='{$dados->acomp}' {$visao} /></td></tr></table></fieldset>";

		echo "<fieldset style='width:95%;'><legend><b><label style='color:red;'>* </label>Unidade de Interna&ccedil;&atilde;o de Origem</b>.</legend><table width='100%'>";
		echo "<tr><td>Nome da Unidade</td><td>Leito</td><td>Telefone do posto de enfermagem</td></tr>";
		echo "<tr><td><input class='OBG' type='text' size=50 name='local' id='local' value='{$dados->origem}' {$visao} /></td><td><input class= type='text' size=10 name='leito' id='leito' value='{$dados->leito}' {$visao} /></td>
    <td><input class='' type='text' size=20 name='telPosto'  maxlength='15' id='telPosto' value='{$dados->telPosto}' {$visao} /></td></tr>";

		echo "<tr><td>Endere&ccedil;o.</td><td>Cidade</td><td>Pessoa para contato.</td></tr>";
		/// if($visao == '') echo "<td></td></tr>";
		echo "<tr><td><input class='OBG' type='text' size=50 name='end_und_origem' id='end_und_origem' value='{$dados->end_und_origem}' {$visao} /></td>
     <td><input type='text' class='OBG' name='cidade_und' size=35 id='cidade_und' value='{$dados->cidade_und}'  {$visao} /></td>
     <td><input class='OBG' type='text' size=35 name='contato' id='contato' value='{$dados->contato}' {$visao} /></td>
   
    <td><input type='hidden' name='id_cidade_und' id='id_cidade_und' value='{$dados->id_cidade_und}' {$visao} /></td>
    </table></fieldset>";

		/* Comentado  echo "<table><tr><td><b>M&eacute;dico solicitante</b></td><td><b>Especialidade</b></td><td><b>Acompanhar&aacute; o paciente em domic&iacute;lio (S/N)?</b></td></tr>";
			echo "<tr><td><input type='text' size=50 name='solicitante' class='OBG' value='{$dados->solicitante}' {$visao} /></td>";
			echo "<td><input type='text' name='sol-especialidade' class='OBG' value='{$dados->especialidade}' {$visao} /></td>";
			echo "<td><input type='text' name='sol-acompanhamento' class='OBG boleano' value='{$dados->acomp}' {$visao} /></td></tr></table>"; */

		echo "<fieldset style='width:95%;'><legend><b><label style='color:red;'>* </label>Respons&aacute;vel Familiar</b></legend><table width='100%'>";
		echo "<tr><td>Nome</td><td>Sexo</td><td>Rela&ccedil;&atilde;o</td><td><span class='espresp'>Especialidade</span></td><tr>";
		echo "<tr><td><input type='text' size=50 name='responsavel' class='OBG' value='{$dados->rnome}' {$visao} /></td>";
		echo"<td><select name='rsexo'   class='COMBO_OBG' {$visao_sexo}><option value='-1' ></option>";
		echo"<option value='0' " . (($dados->rsexo == '0') ? "selected" : "") . " {$visao_sexo} >MASCULINO</option>";
		echo"<option value='1' " . (($dados->rsexo == '1') ? "selected" : "") . " {$visao_sexo} >FEMININO</option>";
		echo"</select></td>";

		$r = mysql_query("SELECT * FROM relacionamento_pessoa");
		echo "<td><select name='relresp'  class='COMBO_OBG'  {$visao_sexo} >";
		echo "<option value='-1' selected ></option>";
		while ($row = mysql_fetch_array($r)) {
			foreach ($row AS $key => $value) {
				$row[$key] = stripslashes($value);
			}
			if ($row['ID'] == $dados->relresp)
				echo "<option value='{$row['ID']}' selected >{$row['RELACIONAMENTO']}</option>";
			else
				echo "<option value='{$row['ID']}'  >{$row['RELACIONAMENTO']}</option>";
		}
		echo "</select></td>";

		if ($dados->relresp != '12') {
			echo "<span flag='1' class='flagx'>";
		} else {
			echo "<span flag='0' class='flagx'>";
		}

		echo"<td><span class='espresp'>{$row['ID']}<input type='text' size=32 name='especialidadeResponsavel'  id='especialidadeResponsavel'  value='{$dados->especialidadeResponsavel}' {$visao} /></span></td></tr>";


		echo"<tr><td>Telefone.</td><td>Data de nascimento</td>
                 <td><label for='cpf-responsavel'>CPF:</label></td></tr>";
		echo "<td><input type='text' name='telResponsavel' maxlength='15' id='telResponsavel' class='' value='{$dados->telResponsavel}' {$visao} /></td>
                   <td><input class='data OBG' maxlength='10' size='10' type='text' name='nascimentoResponsavel' value='{$dados->nascResponsavel}' {$visao_sexo} /></td>
			  <td>
                      <input id='cpf-responsavel' name ='cpf_responsavel'  class ='cpf $cpfOBG' {$visao} value='{$dados->cpfRresponsavel}'>
                      <span id='span-cpf-responsavel' class='text-red'></span>
			  </td></tr></table></fieldset>";
		echo "<fieldset style='width:95%;'><legend><b>Cuidador Familiar</b>.</legend><table width='100%'><tr><td>Se Diferente do Respons&aacute;vel</td></tr>";
		echo "<tr><td>Nome</td><td>Sexo</td><td>Rela&ccedil;&atilde;o</td><td><span class='espcuid'>Especialidade</span></td><tr>";
		echo "<tr><td><input type='text' size=50 name='cuidador' class='' value='{$dados->cnome}' {$visao} /></td>";
		///echo "<td><input type='radio' name='csexo' value='0' ".(($dados->csexo=='0')?"checked":"")." {$visao_sexo} />M";
		///echo "<input type='radio' name='csexo' value='1' ".(($dados->csexo=='1')?"checked":"")." {$visao_sexo} />F</td>";

		echo"<td><select   name='csexo' class='' {$visao_sexo}><option value='-1' ></option>";
		echo"<option value='0' " . (($dados->csexo == '0') ? "selected" : "") . " {$visao_sexo} >MASCULINO</option>";
		echo"<option value='1' " . (($dados->csexo == '1') ? "selected" : "") . " {$visao_sexo} >FEMININO</option>";
		echo"</select></td>";

		$r = mysql_query("SELECT * FROM relacionamento_pessoa");
		echo "<td><select name='relcuid'   {$visao_sexo} >";
		echo "<option value='-1' selected ></option>";
		while ($row = mysql_fetch_array($r)) {
			foreach ($row AS $key => $value) {
				$row[$key] = stripslashes($value);
			}
			if ($row['ID'] == $dados->relcuid)
				echo "<option value='{$row['ID']}' selected >{$row['RELACIONAMENTO']}</option>";
			else
				echo "<option value='{$row['ID']}'>{$row['RELACIONAMENTO']}</option>";
		}

		echo "</select></td>";
		if ($dados->relcuid != '12') {
			echo "<span flag='1' class='flagc'>";
		} else {
			echo "<span flag='0' class='flagc'>";
		}
		echo "<td>
    <span class='espcuid'>{$row['ID']}<input type='text' size=32 name='especialidadeCuidador' id='especialidadeCuidador'   value='{$dados->especialidadeCuidador}' {$visao} /></span>
    </td></tr>";
		/*  Comentado echo "<td><input type='text' name='relcuid' class='' value='{$dados->crel}' {$visao} /></td></tr>";
		 */ echo"<tr><td>Telefone.</td><td>Data de nascimento</td></tr>";



		echo "<td><input type='text' name='telCuidador' maxlength='15' id='telCuidador' class='' value='{$dados->telCuidador}' {$visao} /></td>
 <td><input class='data ' maxlength='10' size='10' type='text' name='nascimentoCuidador' value='{$dados->nascCuidador}' {$visao_sexo} /></td>
                  </tr></table></fieldset>";

		///echo "<td><input type='radio' name='rsexo' value='0' ".(($dados->rsexo=='0')?"checked":"")." {$visao_sexo} />M";
		/// echo "<input type='radio' name='rsexo' value='1' ".(($dados->rsexo=='1')?"checked":"")." {$visao_sexo} />F</td>";

		echo "<fieldset style='width:95%;'><legend><b><label style='color:red;'>* </label>Domiciliar</b></legend><table width='100%'>";
		echo "<tr><td>Endere&ccedil;o.</td><td>Bairro</td><td>Ponto de Referência</td></tr>";
		echo "<tr><td><input type='text' size=50 name='endereco' class='OBG' value='{$dados->endereco}' {$visao} /></td>";
		echo "<td><input type='text' name='bairro' class='OBG' value='{$dados->bairro}' {$visao} /></td>";
		echo "<td><input type='text' name='referencia' class='OBG' value='{$dados->referencia}' {$visao} /></td></tr>";
		echo"<tr><td>Telefone.</td><td>Cidade</td></tr>";
		echo "<tr><td><input type='text' maxlength='15' name='telDomiciliar' id='telDomiciliar' class='' value='{$dados->telDomiciliar}' {$visao} /></td>
    <td><input type='text' class='OBG' size='35' name='cidade_domiciliar' id='cidade_domiciliar' value='{$dados->cidade_domiciliar}'  {$visao} /></td>
    <td><input type='hidden' name='id_cidade_domiciliar' id='id_cidade_domiciliar' value='{$dados->id_cidade_domiciliar}' {$visao} /></td></tr></table></fieldset>";

		echo "<fieldset style='width:95%;'><legend><b>Local de Interna&ccedil;&atilde;o</b></legend><table width='100%'>";
		echo "<tr><td>Se Diferente do domiciliar.</td></tr><tr><td>Endere&ccedil;o.<td>Bairro</td><td>Ponto de Referência</td></tr>";
		echo "<tr><td><input type='text' size=50 name='enderecoInternacao' class='' value='{$dados->enderecoInternacao}' {$visao} /></td>";
		echo "<td><input type='text' name='bairroInternacao' class='' value='{$dados->bairroInternacao}' {$visao} /></td>";
		echo "<td><input type='text' name='referenciaInternacao' class='' value='{$dados->referenciaInternacao}' {$visao} /></td></tr>";
		echo"<tr><td>Telefone.</td><td>Cidade<td></tr>";
		echo "<tr><td><input type='text' maxlength='15' name='telInternacao' id='telInternacao' class='' value='{$dados->telInternacao}' {$visao} /></td>
    <td><input type='text'  size='35' name='cidade_internado' id='cidade_internado' value='{$dados->cidade_internado}'  {$visao} /></td>
    <td><input type='hidden' name='id_cidade_internado' id='id_cidade_internado' value='{$dados->id_cidade_internado}' {$visao} /></td></tr></table></fieldset><table>";
		echo "<tr><td><b><label style='color:red;'>* </label>Unidade Regional</b></td></tr>";
		echo "<tr><td>" . $this->empresas($dados->empresa, $visao_sexo) . "</td></tr></table>";
		if ($visao == '')
			echo "<p><button id='botao-salvar-formulario' modo='{$modo}' >Inserir</button>";
		echo "</div>";
	}

	public function PegarData($data = null) {
		if ($data == null) {
			$data = date('d/m/Y');
		} else {
			$quebrar = explode('-', $data);

			//print_r(explode('-',$data));

			if (strlen($quebrar[0]) == 4) {
				$data = substr($data, 8, 2) . '/' . substr($data, 5, 2) . '/' . substr($data, 0, 4);
			} else {
				$data = substr($data, 0, 2) . '/' . substr($data, 3, 2) . '/' . substr($data, 6, 4);
			}
		}
		return $data;
	}

	public function PegarHora($data = null) {
		if ($data == null) {
			$hora = date('H:i:s');
		} else {
			$hora = substr($data, 11, 2) . ':' . substr($data, 14, 2) . ':' . substr($data, 17, 2);
		}
		return $hora;
	}

	public function listar() {

		require 'templates/paciente_listar.phtml';
	}

//listar

	public function listar_score_paciente($id, $convenio) {

		$id = anti_injection($id, "numerico");

		$sql_scores = "SELECT
		           sa.DATA,
		           sa.ID_AVULSO AS ID_SCORE,
		           '0' AS TIPO,
	                   u.nome,
	                   u.tipo as utipo,
	                   u.conselho_regional
		FROM 
		         score_avulso as sa
	
	                 INNER JOIN usuarios AS u ON sa.ID_USUARIOS = u.idUsuarios AND sa.ID_PACIENTE = '{$id}'
	
		GROUP BY 
		         sa.ID_AVULSO

		ORDER BY 
		        DATA DESC";


		$r = mysql_query($sql_scores);
		echo "<center><h1>Lista de Scores </h1></center>";
		$this->cabecalho($id);
		echo "<br/><br/>";

		echo "<div id='dialog-escolha-score' data-convenio='' title='Escolha o tipo de Score:'></div>";
		if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_score']['medico_paciente_score_novo'])
			|| $_SESSION['adm_user'] == 1) {

			echo "<div><input type='hidden' name='novo_score' value='{$id}'/>
                           <input type='hidden' name='convenio' value='{$convenio}'>";

            echo "<button id='novo_score'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' type='submit' role='button' aria-disabled='false'>
	  		<span class='ui-button-text'>Novo Score</span></button></div>";
        }

		echo "<div id='dialog-remover-score' title='Remover Score do Paciente' cod='{$id}' tipo='{$row["TIPO"]}'></div>";

		$flag = true;

		while ($row = mysql_fetch_array($r)) {
            $compTipo = $row['conselho_regional'];

			if ($flag) {

				$p = ucwords(strtolower($row["fev.PACIENTE_ID"]));
				echo "<br/><table class='mytable' width='100%' >";
				echo "<thead><tr>";
				echo "<th width='55%'>M&eacute;dico 123</th>";
				echo "<th>Data</th>";
				echo "<th width='15%'>Detalhes</th>";
				echo "<th width='15%'>Op&ccedil;&otilde;es</th>";
				echo "</tr></thead>";
				$flag = false;
			}

			$u = ucwords(strtolower($row["nome"])) . $compTipo;

			echo "<tr><td>{$u}</td><td>{$row['DATA']}</td>";
			echo "<td><a href=?view=paciente_med&act=detalhescore&id={$row['ID_SCORE']}&tipo={$row['TIPO']}&paciente_id={$id}><img src='../utils/capmed_16x16.png' title='Detalhes Score' border='0'></a></td>";
			echo"<td>";

			if ($row['TIPO'] == 0) {
                if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_score']['medico_paciente_score_excluir']) || $_SESSION['adm_user'] == 1) {
                    echo "<img class='remover_score' src='../utils/delete_16x16.png' cod='{$row['ID_SCORE']}' tipo='{$row["TIPO"]}' id_paciente='{$id}' width=16 title='Remover Score' border='0'>";
                }
			}
			echo "</td>";
			echo "</tr>";
		}

		if ($flag) {
			echo "<p><center><b>Nenhum score foi preenchido para o paciente</b></center></p>";

			return;
		} else {
			echo "</table>";
		}
	}

//listar_score_paciente

	public function score_paciente($id, $tipo_score) {
		echo "<div id='div-score'  ><br/>";
		echo "<p><center><h1>Novo Score</h1></center></p>";

		$this->cabecalho($id);

		$sql_convenio = "SELECT convenio FROM clientes WHERE idClientes = $id";
		$result = mysql_query($sql_convenio);
		$convenio = mysql_result($result, 0);

		echo alerta();
		echo "<form >";
		echo "<input type='hidden' name='paciente'  value='{$id}' />";
		echo "<input type='hidden' name='usuario'  value='{$_SESSION['id_user']}' />";
		echo "<br/><table class='mytable' width=100% ><tbody>";

        if ($tipo_score == 1) {
            $this->score_petrobras();
        } else {
            $tipo_relatorio = 'capmedica';
            $capmedica = null;

            $sql = <<<SQL
SELECT 
  MAX(ID) AS ultimaAvaEnf,
  COUNT(ID) AS numAvas 
FROM
  avaliacaoenfermagem
WHERE
  PACIENTE_ID = '{$id}'
SQL;
            $rs = mysql_query($sql);
            $rowAvaEnf = mysql_fetch_array($rs);

            if (count($rowAvaEnf) >= '1') {
                $capmedica = $rowAvaEnf['ultimaAvaEnf'];
                $tipo_relatorio = 'avaliacaoenfermagem';
            }
            \App\Controllers\Score::scoreNead2016($id, $capmedica, $tipo_relatorio);
        }
        echo "</tbody></table></form></div>";

		echo "<button id='salvar_score' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
		echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
	}

//score_paciente

	public function nova_capitacao_medica($id) {

		$id = anti_injection($id, "numerico");
		$result = mysql_query("SELECT p.nome FROM clientes as p WHERE p.idClientes = '{$id}'");
		$d = new DadosFicha();
		while ($row = mysql_fetch_array($result)) {
			$d->pacienteid = $id;
			$d->pacientenome = ucwords(strtolower($row["nome"]));
			$d->pacientePlano = $row['convenio'];
		}

		$this->capitacao_medica($d, "");
	}

//nova_capitacao_medica

	public function detalhe_prescricao($id, $idpac, $med) {
        $sql = "SELECT 
                  empresas.nome AS empresa, 
                  planosdesaude.nome AS plano,
                  empresas.id as empresaId
                FROM 
                  prescricoes
                  LEFT JOIN empresas ON prescricoes.empresa = empresas.id
                  LEFT JOIN planosdesaude ON prescricoes.plano = planosdesaude.id
                WHERE 
                  prescricoes.Carater IN (4,7) AND 
                   prescricoes.ID_CAPMEDICA = {$id}
                   GROUP  by ID_CAPMEDICA ";

        $rs = mysql_query($sql);
        $rowPaciente = mysql_fetch_array($rs);
        $empresa = null;
        $convenio = null;
        $empresaId= null;
        if(count($rowPaciente) > 0) {
            $empresa = $rowPaciente['empresa'];
            $convenio = $rowPaciente['plano'];
            $empresaId = $rowPaciente['empresaId'];

        }
        echo "<center><h1>Visualizar Prescri&ccedil;&atilde;o</h1></center>";

		$this->cabecalho($idpac, $empresa, $convenio);


        $responsePaciente = ModelPaciente::getById($idpac);
        $empresaRelatorio = empty($empresaId) ? $responsePaciente['empresa'] : $empresaId;

        $caminho = "/medico/imprimir_prescricao.php?id={$id}&idpac={$idpac}";
        echo "<button caminho='$caminho' empresa-relatorio='{$empresaId}' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  role='button' aria-disabled='false'>
                <span class='ui-button-text'>
                Imprimir
                </span>
              </button>";

//        echo"<form method=post target='_blank' action='imprimir_prescricao.php?id={$id}&idpac={$idpac}'>";
//		echo"<input type=hidden value={$idpac} name='imprimir_det_presc'</input>";
//		echo "<button id='imprimir_det_presc' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' type='submit' role='button' aria-disabled='false'>
//  	<span class='ui-button-text'>Imprimir</span></button>";
//		echo "</form>";

		if ($_SESSION['tipo_user'] === 'administrativo' || $_SESSION['adm_user'] == 1) {
			$this->addFormSugestao('prsc', $id, $idpac, $med);
		}

		echo "<table width=100% class='mytable'><thead><tr>";
		echo "<th><center><b>Históricos de Prescrições</b></center></th>";
		echo "</tr></thead>";
		/* Comentado
		 * $sql2="select id from prescricoes where  ID_CAPMEDICA='{$id}'";
			$x = mysql_query($sql2);
			$row2 = mysql_fetch_array($x);
			$id2 = $row2['id']; */


		$sql = "SELECT
    I.*,U.nome as nomeuser,U.idUsuarios,P.Carater,
    v.via as nvia, f.frequencia as nfrequencia,I.inicio,
  	I.fim, DATE_FORMAT(I.aprazamento,'%H:%i') as apraz, ume.unidade as um,
  	P.STATUS
FROM
   prescricoes AS P INNER JOIN 
   itens_prescricao AS I ON (P.id = I.idPrescricao)  LEFT JOIN
  catalogo AS B ON ( I.CATALOGO_ID = B.ID) INNER JOIN
   usuarios AS U ON (U.idUsuarios=P.criador) LEFT OUTER JOIN frequencia as f ON (I.frequencia = f.id) LEFT OUTER JOIN viaadm as v ON (I.via = v.id)
  	LEFT OUTER JOIN umedidas as ume ON (I.umdose = ume.id)
WHERE
   P.Carater IN (4,7) AND 
    P.ID_CAPMEDICA = {$id}
    
     GROUP BY I.id";

		$result = mysql_query($sql);
		$n = 0;
		$iduser = 0;
		$valCarater = 0;
		while ($row = mysql_fetch_array($result)) {


			if ($row["Carater"] == 4) {
				$carater = " -Prescri&ccedil;&atilde;o Peri&oacute;dica de Avalia&ccedil;&atilde;o";
			}
			if ($row["Carater"] == 7) {
				$carater = " -Prescri&ccedil;&atilde;o Aditiva de Avalia&ccedil;&atilde;o";
			}

			if ($n++ % 2 == 0)
				$cor = '#E9F4F8';
			else
				$cor = '#FFFFFF';

            $corFonte = $row['STATUS'] == 0 ? '#000000'  : '#708090';
            $corCancelada = $row['STATUS'] == 0 ? '#00FF00'  : '#FF0000';

			$tipo = $row['tipo'];
			$i = implode("/", array_reverse(explode("-", $row['inicio'])));
			$f = implode("/", array_reverse(explode("-", $row['fim'])));
			$aprazamento = "";
			$statusPrescricao = $row['STATUS'] == 0 ? 'Ativa' : 'Cancelada';
			if ($row['tipo'] != "0")
				$aprazamento = $row['apraz'];
			$linha = "";
			if ($row['tipo'] != "-1") {
				$aba = $this->abas[$tipo];
				$dose = "";
				if ($row['dose'] != 0)
					$dose = $row['dose'] . " " . $row['um'];
				$linha = "<b>$aba:</b> {$row['nome']} {$row['apresentacao']} {$row['nvia']} {$dose} {$row['nfrequencia']} Per&iacute;odo: de $i até $f OBS: {$row['obs']}";
			} else
				$linha = $row['descricao'];

			if ($iduser != $row['idUsuarios'] || $valCarater != $row["Carater"]) {
				$iduser = $row['idUsuarios'];
				$valCarater = $row["Carater"];
				echo"<tr bgcolor='{$corCancelada}'><td><center><b> <span >({$statusPrescricao})</span> M&eacute;dico: {$row['nomeuser']}{$carater} </center></b></td></tr>";
			}
			echo "<tr bgcolor={$cor} >";
			echo "<td style='color: {$corFonte}'>$linha</td>";
			echo "</tr>";
		}
		echo "</table>";

		echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
	}

//detalhe_prescricao

        public function detalhe_score($id, $tipo, $paciente_id) {

		if ($tipo == 0) {//Avulso
			$sql = "SELECT
                            sa.justificativa,
                            sa.OBSERVACAO, 
                            sa.ID_PACIENTE,
                            si.DESCRICAO , 
                            si.PESO ,
                            si.GRUPO_ID ,
		  					si.ID ,
                                sc.id as tbplano,
                                DATE(sa.DATA) as DATA,
		  		grup.DESCRICAO as grdesc,
		  		sa.versao_score
		FROM score_avulso sa
		  		LEFT OUTER JOIN score_item si ON sa.ID_ITEM_SCORE = si.ID
		  		LEFT OUTER JOIN grupos grup ON si.GRUPO_ID = grup.ID
		  		LEFT OUTER JOIN score sc ON si.SCORE_ID = sc.ID
		WHERE sa.ID_AVULSO = '{$id}' and sa.ID_PACIENTE = '{$paciente_id}' ";


			$result2 = mysql_query($sql);
			$row3 = mysql_fetch_array($result2);
			$result1 = mysql_query($sql);
		} elseif ($tipo == 1) {//Capmedica
			$sql = "SELECT sc_pac.OBSERVACAO,
                sc_pac.ID_PACIENTE, 
                sc_item.DESCRICAO ,
                sc_item.PESO ,sc_item.GRUPO_ID ,
  		sc_item.ID ,sc.id as tbplano,cap.justificativa,DATE(sc_pac.DATA) as DATA,
  		grup.DESCRICAO as grdesc  FROM score_paciente sc_pac
  		LEFT OUTER JOIN score_item sc_item ON sc_pac.ID_ITEM_SCORE = sc_item.ID
  		LEFT OUTER JOIN capmedica cap ON sc_pac.ID_CAPMEDICA = cap.id
  		LEFT OUTER JOIN grupos grup ON sc_item.GRUPO_ID = grup.ID
  		LEFT OUTER JOIN score sc ON sc_item.SCORE_ID = sc.ID
  		WHERE sc_pac.ID_CAPMEDICA = '{$id}' and grup.GRUPO_NEAD_ID IS NULL";
			$result = mysql_query($sql);
			$result1 = mysql_query($sql);
			$result2 = mysql_query($sql);
			$row3 = mysql_fetch_array($result2);
		}

		$sql2 = "SELECT
  	UPPER(c.nome) AS paciente,
  	(CASE c.sexo
  	WHEN '0' THEN 'Masculino'
  	WHEN '1' THEN 'Feminino'
  	END) AS sexo,
  	FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
  	e.nome as empresa,
  	c.`nascimento`,
  	p.nome as Convenio,p.id,
  	NUM_MATRICULA_CONVENIO,
  	cpf,
  	e.id as empresaId
  	FROM
  	clientes AS c LEFT JOIN
  	empresas AS e ON (e.id = c.empresa) INNER JOIN
  	planosdesaude as p ON (p.id = c.convenio)
  	WHERE
  	c.idClientes ={$paciente_id}
  	ORDER BY
  	c.nome DESC LIMIT 1";

		$result3 = mysql_query($sql2);

        $rowPaciente = mysql_fetch_array($result3);
        $empresaId = $rowPaciente['empresaId'];


		echo"<h1><center>Visualizar Score</center></h1>";
		if ($row3['tbplano'] == 1 || $row3['tbplano'] == 2) {
			echo"<table style='width:95%;'>";
			//echo "<a href=imprimir_score.php?id={$id}>imprimir</a></br>";
            $caminho = "/medico/imprimir_score.php?id={$id}&tipo={$tipo}&paciente_id={$paciente_id}tipo_score=0";

            echo "<button  caminho='$caminho' empresa-relatorio='{$empresaId}' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  role='button' aria-disabled='false'><span class='ui-button-text'>Imprimir Score Petrobras/ABMID</span></button>";

//            echo"<form method=post target='_blank' action='imprimir_score.php?id={$id}&tipo={$tipo}&paciente_id={$paciente_id}tipo_score=0'>";
//			echo"<input type=hidden value={$row3['ID_PACIENTE']} name='imprimir_score'</input>";
//			echo "<button id='imprimir_score' tipo='{$tipo}' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' type='submit' role='button' aria-disabled='false'>
//  	<span class='ui-button-text'>Imprimir Score Petrobras/ABMID</span></button>";
//			echo "</form>";
			if ($tipo == 1) {
                $caminho = "imprimir_score.php?id={$id}&tipo={$tipo}&paciente_id={$paciente_id}&tipo_score=1";

                echo "<button  caminho='$caminho' empresa-relatorio='{$empresaId}' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  role='button' aria-disabled='false'><span class='ui-button-text'>Imprimir Score NEAD</span></button>";

//                echo"<form method=post target='_blank' action='imprimir_score.php?id={$id}&tipo={$tipo}&paciente_id={$paciente_id}&tipo_score=1'>";
//				echo"<input type=hidden value={$row3['ID_PACIENTE']} name='imprimir_score'</input>";
//				echo "<button id='imprimir_score' tipo='{$tipo}' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' type='submit' role='button' aria-disabled='false'>
//  	<span class='ui-button-text'>Imprimir Score NEAD</span></button>";
//				echo "</form>";
			}

			if ($row3['tbplano'] == 1) {
				echo "<tr><td><img src='../utils/petro.jpg' title='' border='0' ></td></tr>";
				echo"<tr  bgcolor='grey'><td colspan='3'><center><b>Tabela de Avalia&ccedil;&atilde;o Petrobras.</b>&nbsp;&nbsp;<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> Efetuada em: </b>" . join("/", array_reverse(explode("-", $row3['DATA']))) . "</center></td></tr>";
			} else if ($row3['tbplano'] == 2) {
				echo "<tr><td><img src='../utils/abmid.jpg' title='' border='0' ></td></tr>";
				echo"<tr bgcolor='grey'><td colspan='3'  ><center><b>Tabela de Avalia&ccedil;&atilde;o ABEMID.</b> &nbsp;&nbsp;<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> Efetuada em: </b>" . join("/", array_reverse(explode("-", $row3['DATA']))) . "</center></td></tr>";
			}

			while ($pessoa = mysql_fetch_array($result3)) {
				foreach ($pessoa AS $chave => $valor) {
					$pessoa[$chave] = stripslashes($valor);
				}



				echo "<tr style='background-color:#EEEEEE;'>";
				echo "<td colspan='3'><label><b>PACIENTE: </b>" . $pessoa['paciente'] . "</label></td>";
				echo "</tr></br>";


				echo "<tr><td colspan='3'><b>SEXO: </b>" . $pessoa['sexo'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  	 	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IDADE: </b>" . join("/", array_reverse(explode("-", $pessoa['nascimento']))) . ' (' . $pessoa['idade'] . " anos)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				echo "</tr></br>";
				echo "<tr style='background-color:#EEEEEE;'>";
				echo "<td colspan='3'><label><b>CONV&Ecirc;NIO: </b>" . strtoupper($pessoa['Convenio']) . "</label>&nbsp;
  	 	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATR&Iacute;CULA: </b>" . strtoupper($pessoa['NUM_MATRICULA_CONVENIO']) . "</label></td>";
				echo "</tr><tr><td><b>CPF:</b>{$pessoa['cpf']}</td>
                                  </tr>";
			}


			while ($row = mysql_fetch_array($result)) {
				if ($row['OBSERVACAO'] != null && $row['OBSERVACAO'] != '')
					echo"<tr><td width=50% ><b>" . $row['DESCRICAO'] . "</b></td><td colspan='2'>" . $row['OBSERVACAO'] . "</td></tr>";
			}
			echo"<tr bgcolor='grey'><td><b>Descri&ccedil;&atilde;o</b></td><td><b>Itens da Avalia&ccedil;&atilde;o</b></td><td><b>Peso</b></td></tr>";
			while ($row = mysql_fetch_array($result1)) {
				if ($row['OBSERVACAO'] == null && $row['OBSERVACAO'] == '')
					echo"<tr><td width=40% >" . $row['grdesc'] . "</td><td width=45% >" . $row['DESCRICAO'] . "</td><td colspan='2' >" . $row['PESO'] . "</td></tr>";
				$x += $row['PESO'];
			}
			echo"<tr bgcolor='grey'><td colspan='3' aling='center'><b>TOTAL DO SCORE : {$x}</b></td></tr>";
			echo"<tr bgcolor='grey'><td colspan='3' aling='center'><b>Justificativa para a interna&ccedil;&atilde;o:</b></td></tr>";
			echo"<tr ><td colspan='3' aling='center'>" . $row3['justificativa'] . "</td></tr>";
			echo"</table></br></br>";
		}else {
            $caminho = "imprimir_score.php?id={$id}&tipo={$tipo}&paciente_id={$paciente_id}&tipo_score=1";

            echo "<button  caminho='$caminho' empresa-relatorio='{$empresaId}' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  role='button' aria-disabled='false'><span class='ui-button-text'>Imprimir Score NEAD</span></button>";

//			echo"<form method=post target='_blank' action='imprimir_score.php?id={$id}&tipo={$tipo}&paciente_id={$paciente_id}&tipo_score=1'>";
//			echo"<input type=hidden value={$row3['ID_PACIENTE']} name='imprimir_score'</input>";
//			echo "<button id='imprimir_score' tipo='{$tipo}' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' type='submit' role='button' aria-disabled='false'>
//  	<span class='ui-button-text'>Imprimir Score NEAD</span></button>";
//			echo "</form>";
		}

		if ($tipo == 1 || $tipo == 0 ) {

			$x = $this->visualizarScoreNead($id, $tipo, $paciente_id,$row3['versao_score']);
			echo $x;
		}


		echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
	}

//detalhe_score

	public function detalhes_capitacao_medica($id, $medico) {
		$id = anti_injection($id, "numerico");
		$sql = "SELECT cap.*, pac.nome as pnome,pac.idClientes,usr.nome as unome, DATE_FORMAT(cap.data,'%d/%m/%Y') as fdata, comob.*
  			FROM capmedica as cap 
  			LEFT OUTER JOIN comorbidades as comob ON cap.id = comob.capmed
  			LEFT OUTER JOIN problemasativos as prob ON cap.id = prob.capmed 
  			LEFT OUTER JOIN usuarios as usr ON cap.usuario = usr.idUsuarios 
  			LEFT OUTER JOIN clientes as pac ON cap.paciente = pac.idClientes
  			  
  			WHERE cap.id = '{$id}'";
		$d = new DadosFicha();
		$d->capmedica = $id;
		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {

			$d->pacienteid = $row['idClientes'];
			$d->usuario = ucwords(strtolower($row["unome"]));
			$d->data = $row['fdata'];
			$d->pacientenome = ucwords(strtolower($row["pnome"]));
			$d->motivo = $row['motivo'];
			$d->alergiamed = $row['alergiamed'];
			$d->alergiaalim = $row['alergiaalim'];
			$d->tipoalergiamed = $row['tipoalergiamed'];
			$d->tipoalergiaalim = $row['tipoalergiaalim'];
			$d->prelocohosp = $row['hosplocomocao'];
			$d->prehigihosp = $row['hosphigiene'];
			$d->preconshosp = $row['hospcons'];
			$d->prealimhosp = $row['hospalimentacao'];
			$d->preulcerahosp = $row['hospulcera'];
			$d->prelocodomic = $row['domlocomocao'];
			$d->prehigidomic = $row['domhigiene'];
			$d->preconsdomic = $row['domcons'];
			$d->prealimdomic = $row['domalimentacao'];
			$d->preulceradomic = $row['domulcera'];
			$d->objlocomocao = $row['objlocomocao'];
			$d->objhigiene = $row['objhigiene'];
			$d->objcons = $row['objcons'];
			$d->objalimento = $row['objalimentacao'];
			$d->objulcera = $row['objulcera'];
			$d->shosp = $d->prelocohosp + $d->prehigihosp + $d->preconshosp + $d->prealimhosp + $d->preulcerahosp;
			$d->sdom = $d->prelocodomic + $d->prehigidomic + $d->preconsdomic + $d->prealimdomic + $d->preulceradomic;
			$d->sobj = $d->objlocomocao + $d->objhigiene + $d->objcons + $d->objalimento + $d->objulcera;
			$d->qtdinternacoes = $row['qtdinternacoes'];
			$d->historico = $row['historicointernacao'];
			$d->parecer = $row['parecer'];
			$d->justificativa = $row['justificativa'];
			$d->cancer = $row['cancer'];
			$d->psiquiatrico = $row['psiquiatrico'];
			$d->neuro = $row['neuro'];
			$d->glaucoma = $row['glaucoma'];
			$d->hepatopatia = $row['hepatopatia'];
			$d->obesidade = $row['obesidade'];
			$d->cardiopatia = $row['cardiopatia'];
			$d->dm = $row['dm'];
			$d->reumatologica = $row['reumatologica'];
			$d->has = $row['has'];
			$d->nefropatia = $row['nefropatia'];
			$d->pneumopatia = $row['pneumopatia'];
			$d->modalidade = $row['MODALIDADE'];
			$d->local_av = $row['LOCAL_AV'];
			$d->pre_justificativa = $row['PRE_JUSTIFICATIVA'];
			$d->plano_desmame = $row['PLANO_DESMAME'];
			$d->equipamento_obs = $row['EQUIPAMENTO_OBS'];
			$d->modalidade = $row['MODALIDADE'];
            $d->modeloNead = $row['MODELO_NEAD'];
            $d->classificacao_paciente = $row['CLASSIFICACAO_NEAD'];
			$d->dataAvaliacao = $row['DATA'];
			$d->empresa = $row['empresa'];
		}
		$sql = "SELECT p.DESCRICAO as nome FROM problemasativos as p WHERE p.capmed = '{$id}' ";
		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {

			$d->ativos[] = $row['nome'];
		}

		//$this->alergia_medicamentosa_listagem($d->pacienteid);
		//$this->cabecalho(pacienteid);
		$this->capitacao_medica($d, "readonly", true, $medico);
		echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
	}

	public function cabecalho($dados, $empresa = null, $convenio = null) {
		if (isset($dados->pacienteid)) {
            $id = $dados->pacienteid;
        } else {
            $id = $dados;
        }

		$condp1 = "c.idClientes = '{$id}'";
		$sql2 = "SELECT
  	UPPER(c.nome) AS paciente,
  	(CASE c.sexo
  	WHEN '0' THEN 'Masculino'
  	WHEN '1' THEN 'Feminino'
  	END) AS sexo,
  	FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
  	e.nome as empresa,
  	c.`nascimento`,
  	p.nome as Convenio,p.id,
  	NUM_MATRICULA_CONVENIO,
  	cpf
  	FROM
  	clientes AS c LEFT JOIN
  	empresas AS e ON (e.id = c.empresa) INNER JOIN
  	planosdesaude as p ON (p.id = c.convenio)
  	WHERE
  	{$condp1}
  	ORDER BY
  	c.nome DESC LIMIT 1";

		$result2 = mysql_query($sql2);
		echo "<table width=100% style='border:2px dashed;'>";
		while ($pessoa = mysql_fetch_array($result2)) {
			foreach ($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);
			}
            $pessoa['empresa'] = !empty($empresa) ? $empresa : $pessoa['empresa'];
            $pessoa['Convenio'] = !empty($convenio) ? $convenio : $pessoa['Convenio'];
			echo "<br/><tr>";
			echo "<td colspan='2'><label style='font-size:14px;color:#8B0000'><b><center> INFORMA&Ccedil;&Otilde;ES DO PACIENTE </center></b></label></td>";
			echo "</tr>";

			echo "<tr style='background-color:#EEEEEE;'>";
			echo "<td width='70%'><label><b>PACIENTE </b></label></td>";
			echo "<td><label><b>SEXO </b></label></td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>" . String::removeAcentosViaEReg($pessoa['paciente']) . "</td>";
			echo "<td>{$pessoa['sexo']}</td>";
			echo "</tr>";

			echo "<tr style='background-color:#EEEEEE;'>";
			echo "<td width='70%'><label><b>IDADE </b></label></td>";
			echo "<td><label><b>UNIDADE REGIONAL </b></label></td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>" . join("/", array_reverse(explode("-", $pessoa['nascimento']))) . ' (' . $pessoa['idade'] . " anos)</td>";
			echo "<td>{$pessoa['empresa']}</td>";
			echo "</tr>";

			echo "<tr style='background-color:#EEEEEE;'>";
			echo "<td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>";
			echo "<td><label><b>MATR&Iacute;CULA </b></label></td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>" . strtoupper($pessoa['Convenio']) . "</td>";
			echo "<td>" . strtoupper($pessoa['NUM_MATRICULA_CONVENIO']) . "</td>";
			echo "</tr>";
			echo " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
			$this->plan = $pessoa['id'];
		}
		echo "</table><br/>";
	}

	public function equipamentos($id = null) {

		echo "<tr><thead>";
		echo "<th colspan='3'>Equipamentos</th>";
		echo "</thead></tr>";
		if ($id == null) {
			$sql = "select * from cobrancaplanos where tipo=1";

			$result = mysql_query($sql);

			echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
			while ($row = mysql_fetch_array($result)) {
				echo"<tr><td><input type='checkbox' class= equipamento cod_equipamento='{$row['id']}' />{$row['item']}</td>";
				echo"<td><input style='width:100%' type='texto' class='equipamento' id=obs" . "{$row['id']} readonly name='obs_equpipamento'  {$acesso} /></td>";
				echo"<td><input type='texto' readonly id=qtd" . "{$row['id']}   class='qtd_equipamento'   name='qtd_equipamento'  {$acesso} /></td></tr>";
			}
		} else {

			$sql = "SELECT
	  	C.id,
	  	C.item,
	  	C.unidade,
	  	C.tipo,
	  	eqp.QTD,
	  	eqp.OBSERVACAO
	  	FROM
	  	equipamentos_capmedica as eqp INNER JOIN
	  	cobrancaplanos as C ON (C.id = eqp.COBRANCAPLANOS_ID)
	  
	  	WHERE
	  	eqp.CAPMEDICA_ID = " . $id;


			$result = mysql_query($sql);

			echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td>";
			echo"<td><b>Qtd.</b></td></tr>";
			while ($row = mysql_fetch_array($result)) {
				echo"<tr><td><input type='checkbox' class= equipamento cod_equipamento='{$row['id']}' disabled='disabled' />{$row['item']}</td>";
				echo"<td><input type='texto' style='width:100%' value='{$row['OBSERVACAO']}' class='equipamento' id=obs" . "{$row['id']} readonly='readonly' name='obs_equpipamento'  {$acesso} ></td>";
				echo"<td><input type='texto' readonly='readonly' id=qtd" . "{$row['id']} value='{$row['QTD']}'  class='qtd_equipamento'   name='qtd_equipamento'  {$acesso} ></td></tr>";
			}
		}
	}

	public function equipamentos_editar($id) {

		echo "<thead><tr>";
		echo "<th colspan='3' >Equipamentos</th>";
		echo "</tr></thead>";
		$sql = "SELECT
  	C.id,
  	C.item,
  	C.unidade,
  	C.tipo,
  	eqp.QTD,
  	eqp.OBSERVACAO
  	FROM
  	equipamentos_capmedica as eqp INNER JOIN
  	cobrancaplanos as C ON (C.id = eqp.COBRANCAPLANOS_ID)
  	
  	WHERE
  	eqp.CAPMEDICA_ID = " . $id;


		$result = mysql_query($sql);
		$equipamento = '';
		//echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
		while ($row = mysql_fetch_array($result)) {
			$equipamento[$row[id]] = array("item" => $row['item'], "und" => $row['unidade'], "tipo" => $row['tipo'], "qtd" => $row['QTD'], "obs" => $row['OBSERVACAO']);



			/* Comentado
			 * echo"<tr><td><input type='checkbox' class= equipamento cod_equipamento='{$row['id']}' disabled='disabled' ></input>{$row['item']}</td><td><input type='texto' style='width:100%' value='{$row['OBSERVACAO']}' class='equipamento' id=obs"."{$row['id']} readonly='readonly' name='obs_equpipamento'  {$acesso} ></input></td>
				<td><input type='texto' readonly='readonly' id=qtd"."{$row['id']} value='{$row['QTD']}'  class='qtd_equipamento'   name='qtd_equipamento'  {$acesso} ></input></td></tr>";
			 */
		}



		$sql2 = "select * from cobrancaplanos where tipo=1";

		$result2 = mysql_query($sql2);

		echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
		while ($row = mysql_fetch_array($result2)) {
			if (array_key_exists($row['id'], $equipamento)) {
				$check = "checked='checked'";
				$qtd = $equipamento[$row['id']]['qtd'];
				$obs = $equipamento[$row['id']]['obs'];
				$readonly ='';
			} else {
				$check = '';
				$qtd = '';
				$obs = '';
				$readonly='readonly';
			}
			echo"<tr>"
				. "<td>"
				. " <input type='checkbox' $check class= equipamento cod_equipamento='{$row['id']}' ></input>"
				. "{$row['item']}</td>"
				. "<td><input style='width:100%' type='texto' $readonly class='equipamento' id=obs" . "{$row['id']} value='{$obs}'  name='obs_equpipamento'  {$acesso} ></input>"
				. "</td>"
				. "<td><input type='texto' $readonly id=qtd" . "{$row['id']}   class='qtd_equipamento' value='{$qtd}'  name='qtd_equipamento'  {$acesso} ></input></td></tr>";
		}
	}

	public function score_petrobras($dados = null) {

		echo "<thead><tr>";

		$id = $dados->capmedica;
		echo "<th colspan='3' >Tabela de Avalia&ccedil;&atilde;o de Complexidade Petrobras.</th></tr></thead>";
		echo"<tr bgcolor='grey'><td colspan='3'><b>Informa&ccedil;&otilde;es</b></td></tr>";

		echo "<tr><td colspan='3'> Diagn&Oacute;stico Principal: &nbsp;&nbsp;        <input type='texto' id='diagnostico_principal' name='diagnostico_principal' class='cabec1 diagnostico' cod_item='69'  value='{$dados->diag_principal}' style='width:100%' /> </td></tr>";
		echo "<tr><td colspan='3'> Diagn&oacute;stico Secund&aacute;rio: <input type='texto' name='diagnostico_secundario' id='diagnostico_secundario' class='cabec1 diagnostico'  cod_item='70' style='width:100%'  value='{$dados->diag_secundario}' /> </td></tr>";

		//Pegar pelos problemas ativos

		/* 	echo "<tr><td colspan='3' >Diagn&Oacute;stico Principal:"
			."<input type='text' id='diagnostico-avaliacao-principal' class='busca-diagnostico-avaliacao' {$acesso} size=20 /></td></tr>";
			echo"<tr><td colspan='3' >Diagn&oacute;stico Secund&aacute;rio:"
			."<input type='text' id='diagnostico-avaliacao-secundario' class='busca-diagnostico-avaliacao' {$acesso} size=20 /></td></tr>";
		 */
		if ($id != '') {
			$sql2 = "SELECT
                sc_pac.OBSERVACAO,
                sc_pac.ID_PACIENTE,
                sc_pac.ID_ITEM_SCORE,
                sc_item.DESCRICAO,
                sc_item.PESO,
                sc_item.GRUPO_ID ,
            sc_item.ID ,
            sc.id as tbplano,
            cap.justificativa,
            DATE(sc_pac.DATA) as DATA,
            grup.DESCRICAO as grdesc
                FROM score_paciente sc_pac
            LEFT OUTER JOIN score_item sc_item ON sc_pac.ID_ITEM_SCORE = sc_item.ID
            LEFT OUTER JOIN capmedica cap ON sc_pac.ID_CAPMEDICA = cap.id
            LEFT OUTER JOIN grupos grup ON sc_item.GRUPO_ID = grup.ID
            LEFT OUTER JOIN score sc ON sc_item.SCORE_ID = sc.ID
                WHERE sc_pac.ID_CAPMEDICA = '{$id}'";
			$result2 = mysql_query($sql2);
			$elemento = '';
			//echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
			while ($row = mysql_fetch_array($result2)) {

				$elemento[$row['ID_ITEM_SCORE']] = array("id_elemento" => $row['ID_ITEM_SCORE'], "peso" => $row['PESO']);
			}
		}
		$sql = "SELECT
                    sc_item.*,
                    gru.DESCRICAO as dsc,
                    gru.QTD
                FROM
                    score_item as sc_item LEFT JOIN
                    grupos as gru ON (sc_item.GRUPO_ID = gru.ID)
                WHERE
                    sc_item.SCORE_ID = 1
                ORDER BY
                    sc_item.GRUPO_ID";
		$result = mysql_query($sql);
		$cont = 0;

		$i = 0;
		$cont1 = 1;
		echo "<tr bgcolor='grey'><td><b>" . htmlentities("Descrição") . "</b></td><td colspan='2'><b>Itens da " . htmlentities("Avaliação") . "</b></td></tr>";
		while ($row = mysql_fetch_array($result)) {
			if (is_array($elemento)) {

				if (array_key_exists($row['ID'], $elemento)) {
					$check = "checked='checked'";
					$dados->spetro += $elemento[$row['ID']]['peso'];
				} else {
					$check = '';
				}
			} else {
				$check = '';
			}

			if ($cont1 == 1) {
				$i = $row['QTD'] + 1;
				$cont = $cont1;
			} else {
				$i = 0;
			}
			if ($row['GRUPO_ID'] >= 2 && $row['GRUPO_ID'] <= 11) {
				if ($cont1 == 1) {
					if ($x == "#E9F4F8")
						$x = '';
					else
						$x = "#E9F4F8";
					echo "<tr bgcolor='{$x}'><td ROWSPAN='{$i}'>{$row['dsc']}</td></tr>";
				}
				echo "<tr bgcolor='{$x}'><td colspan='2'>
  	                 <a class='limpar2' title='Desmarcar' ><img src='../utils/desmarcar16x16.png' width='16' title='Desmarcar' border='0'></a>
  	                 <input type='radio'  {$check} class='petro' name=" . $row['dsc'] . " value=" . $row['PESO'] . " cod_item=" . $row['ID'] . "> {$row['DESCRICAO']} - <b>Peso: {$row['PESO']} </b>";
				echo "</td></tr>";
			}

			$cont1++;

			if ($cont1 > $row['QTD'])
				$cont1 = 1;
		}

		$score = "<input type='text' readonly size='2' name='spetro' value='{$dados->spetro}' {$acesso} />";
		echo"<tr bgcolor='grey'><td colspan='3' aling='center'><b>TOTAL DO SCORE SEGUNDO TABELA PETROBRAS: {$score}</b>
    <label id='observacao_score' class='observacao_score'></label></td></tr>";
	}

	public function score_abmid($dados = null) {

		echo "<thead><tr>";

		$cont = "<input type='text' readonly size='2' name='sabmid1' value='{$dados->sabmid1}' {$acesso} />";
		$tex = "<input type='text'  name='sabmid2' value='{$dados->sabmid2}' {$acesso} />";
		$texto = utf8_decode($tex);
		$id = $dados->capmedica;

		echo "<th colspan='3'>Tabela de Avalia&ccedil;&atilde;o de Complexidade ABEMID.</th></tr></thead>";
		echo"<tr bgcolor='grey'><td colspan='3'><b>Informa&ccedil;&otilde;es</b></td></tr>";
		echo "<tr><td colspan='2'> Diagno&Oacute;stico Principal: <input type='texto' name='diagnostico_principal' class='cabec1 OBG' cod_item='71'  value='{$dados->diag_principal}'style='width:100%'> </td></tr>";
		echo "<tr><td colspan='2'> Diagno&oacute;stico Secund&aacute;rio: <input type='texto' class='cabec1' name='diagnostico_secundario' cod_item='72' value='{$dados->diag_secundario}' style='width:100%'> </td></tr>";

		if ($id != '') {
			$sql2 = "SELECT
                sc_pac.OBSERVACAO,
                sc_pac.ID_PACIENTE,
                sc_pac.ID_ITEM_SCORE,
                sc_item.DESCRICAO,
                sc_item.PESO,
                sc_item.GRUPO_ID ,
            sc_item.ID ,
            sc.id as tbplano,
            cap.justificativa,
            DATE(sc_pac.DATA) as DATA,
            grup.DESCRICAO as grdesc
                FROM score_paciente sc_pac
            LEFT OUTER JOIN score_item sc_item ON sc_pac.ID_ITEM_SCORE = sc_item.ID
            LEFT OUTER JOIN capmedica cap ON sc_pac.ID_CAPMEDICA = cap.id
            LEFT OUTER JOIN grupos grup ON sc_item.GRUPO_ID = grup.ID
            LEFT OUTER JOIN score sc ON sc_item.SCORE_ID = sc.ID
                WHERE sc_pac.ID_CAPMEDICA = '{$id}'";
			$result2 = mysql_query($sql2);
			$elemento = '';
			//echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
			while ($row = mysql_fetch_array($result2)) {

				$elemento[$row['ID_ITEM_SCORE']] = array("id_elemento" => $row['ID_ITEM_SCORE'], "peso" => $row['PESO']);
			}
		}
		$sql = "SELECT
                    sc_item.*,
                    gru.DESCRICAO as dsc,
                    gru.QTD
                FROM
                    score_item as sc_item LEFT JOIN
                    grupos as gru ON (sc_item.GRUPO_ID = gru.ID)
                WHERE
                    sc_item.SCORE_ID = 2
                ORDER BY
                    sc_item.GRUPO_ID";

		$result = mysql_query($sql);
		$cont = 0;

		$i = 0;
		$cont1 = 1;
		echo"<tr bgcolor='grey'><td><b>Descri&ccedil;&atilde;o</b></td><td colspan='2'><b>Itens da Avalia&ccedil;&atilde;o</b></td></tr>";
		while ($row = mysql_fetch_array($result)) {
			if (is_array($elemento)) {
				if (array_key_exists($row['ID'], $elemento)) {
					$check = "checked='checked'";
					$dados->sabmid += $elemento[$row['ID']]['peso'];
				} else {
					$check = '';
				}
			} else {
				$check = '';
			}

			if ($cont1 == 1) {
				$i = $row['QTD'] + 1;
				$cont = $cont1;
			} else {
				$i = 0;
			}



			if ($row['GRUPO_ID'] == 1) {
				if ($cont1 == 1) {
					if ($x == "#E9F4F8")
						$x = '';
					else
						$x = "#E9F4F8";

					echo "<tr bgcolor='{$x}'><td ROWSPAN='{$i}'>{$row['dsc']}</td></tr>";
				}
				echo "<tr bgcolor='{$x}'><td colspan='2' class='score_abmid'>
  			<input type='checkbox' class='abmid' name=" . $row['GRUPO_ID'] . " cod_item=" . $row['ID'] . " {$check} value=" . $row['PESO'] . "  > {$row['DESCRICAO']} - <b>Peso: {$row['PESO']} </b>";
				echo "</td></tr>";
			}


			if ($row['GRUPO_ID'] >= 2 && $row['GRUPO_ID'] <= 11) {
				if ($cont1 == 1) {
					if ($x == "#E9F4F8")
						$x = '';
					else
						$x = "#E9F4F8";
					echo "<tr bgcolor='{$x}'><td ROWSPAN='{$i}'>{$row['dsc']}</td></tr>";
				}
				echo "<tr bgcolor='{$x}'><td colspan='2'><a class='limpar' title='Desmarcar' ><img src='../utils/desmarcar16x16.png' width='16' title='Desmarcar' border='0'></a>
  		  			<input type='radio'  {$check} class='abmid' name=" . $row['dsc'] . " value=" . $row['PESO'] . " cod_item=" . $row['ID'] . "> {$row['DESCRICAO']} - Peso: {$row['PESO']}";
				echo "</td></tr>";
			}

			$cont1++;

			if ($cont1 > $row['QTD'])
				$cont1 = 1;
		}



		$score = "<input type='text' readonly size='2' name='sabmid' value='{$dados->sabmid}' {$acesso} />";
		echo"<tr bgcolor='grey'><td colspan='3' aling='center'><b>TOTAL DO SCORE SEGUNDO TABELA ABEMID: {$score}</b><label class='observacao_score'></label></td></tr>";
	}

	////20-08
	public function salv_capmed($id) {

		$sql5 = "select nome FROM clientes where idClientes ={$id}";
		$x = mysql_query($sql5);
		$y = mysql_fetch_array($x);

		echo "<div id='dialog-presc-avaliacao' title='Prescri&ccedil;&atilde;o para Avalia&ccedil;&atilde;o'>";
		echo"<input type='hidden' id='idpacav' value=" . $id . " ></input>";
		echo"<input type='hidden' id='tipoav' value='4' ></input>";
		echo"<input type='hidden' id='capmed_ava' value='' idcap='' ></input>";

		echo "<fieldset><legend><b>Paciente</b></legend><span id='NomePacientePresc' >" . $y['nome'] . "</span></fieldset><br/>";
		$sql = "SELECT
                    *
                FROM
                    prescricoes
                WHERE
                    prescricoes.paciente = {$id}
                    AND prescricoes.Carater = 4";
		$result = mysql_query($sql);
		$existe_presc = mysql_num_rows($result);
		if ($existe_presc > 0) {
			echo "<span id='sp_usar_ultima_prescricao' ><br><input type='checkbox' id='usar_ultima_prescricao'/>Usar a última prescrição de avaliação<br></span>"; //
		}
		$this->periodo();
		echo "<button id='iniciar_pres_avaliacao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>iniciar</span></button></span>";

		echo "</div>";
	}

	public function presc_aditiva($id) {

		$sql5 = "select nome FROM clientes where idClientes ={$id}";
		$x = mysql_query($sql5);
		$y = mysql_fetch_array($x);

		echo "<div id='dialog-presc-avaliacao-aditiva' title='Prescri&ccedil;&atilde;o para Avalia&ccedil;&atilde;o'>";
		echo"<input type='hidden' id='idpacav' value=" . $id . " ></input>";
		echo"<input type='hidden' id='tipoav' value='7' ></input>";
		echo"<input type='hidden' id='capmed_ava' value='' idcap='' ></input>";

		echo "<fieldset><legend><b>Paciente</b></legend><span id='NomePacientePresc' >" . $y['nome'] . "</span></fieldset><br/>";
		$this->periodo();
		echo "<button id='iniciar_pres_avaliacao_aditiva' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>iniciar</span></button></span>";

		echo "</div>";
	}

	public function editar_prescricao($id) {

		$sql5 = "select nome FROM clientes where idClientes ={$id}";
		$x = mysql_query($sql5);
		$y = mysql_fetch_array($x);


		echo "<div id='dialog-editar-presc-avaliacao' title='Editar Prescri&ccedil;&atilde;o para Avalia&ccedil;&atilde;o'>";

		echo"<input type='hidden' id='edt' value='' edt='' ></input>";
		echo"<input type='hidden' id='paciente' value='' paciente='' ></input>";
		echo"<input type='hidden' id='edt_idpresc' value='' antiga='' ></input>";
		echo"<input type='hidden' id='edtidcap' value='' edtidcap='' ></input>";
		echo"<input type='hidden' id='criador' value='' criador='' ></input>";


		echo "<fieldset><legend><b>Paciente</b></legend><span id='Nome_Paciente_Presc' >" . $y['nome'] . "</span></fieldset><br/>";
		$this->periodo();
		echo "<button id='iniciar_editar_pres_avaliacao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>iniciar</span></button></span>";

		echo "</div>";
	}

	public function nova_presc($id) {

		$sql5 = "select nome FROM clientes where idClientes ={$id}";
		$x = mysql_query($sql5);
		$y = mysql_fetch_array($x);


		echo "<div id='dialog-novapresc' title='Prescri&ccedil;&atilde;o para Avalia&ccedil;&atilde;o'>";

		echo"<input type='hidden' id='npaciente' value='' paciente='' ></input>";
		echo"<input type='hidden' id='nedt_idpresc' value='' antiga='' ></input>";
		echo"<input type='hidden' id='ncapmed_ava' value='' idcap='' ></input>";
		echo"<input type='hidden' id='tipoav' value='' ></input>";

		echo "<fieldset><legend><b>Paciente</b></legend><span id='Nome_Paciente_NovaPresc' >" . $y['nome'] . "</span></fieldset><br/>";
		$this->periodo();
		echo "<button id='iniciar_nova_presc_avaliacao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>iniciar</span></button></span>";

		echo "</div>";
	}

	public function cond_pre_ihosp($dados, $enabled) {

		echo "<thead><tr>";
		$score = "SCORE<input type='text' size='2' name='shosp' value='{$dados->shosp}' {$acesso} />";
		echo "<th colspan='3' >" . htmlentities("Condições do paciente pré-Internação") . " hospitalar {$score} </th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='3' ><b>" . htmlentities("locomoção") . ":</b>";
		echo "<input type='radio' class='prelocohosp OBG_RADIO' name='prelocohosp' value=3 " . (($dados->prelocohosp == '3') ? "checked" : "") . " {$enabled} /> Deambulava";
		echo "<input type='radio' class='prelocohosp OBG_RADIO' name='prelocohosp' value=2 " . (($dados->prelocohosp == '2') ? "checked" : "") . " {$enabled} /> Locomovia-se com " . htmlentities("auxílio") . " (Muletas ou Cadeiras)";
		echo "<input type='radio' class='prelocohosp OBG_RADIO' name='prelocohosp' value=1 " . (($dados->prelocohosp == '1') ? "checked" : "") . " {$enabled} /> Restrito ao leito";
		echo "</td></tr>";
		echo "<tr><td colspan='3' ><b>Autonomia para higiene pessoal:</b>";
		echo "<input type='radio' class='prehigihosp OBG_RADIO' name='prehigihosp' value=2 " . (($dados->prehigihosp == '2') ? "checked" : "") . " {$enabled} /> Sim";
		echo "<input type='radio' class='prehigihosp OBG_RADIO' name='prehigihosp' value=1 " . (($dados->prehigihosp == '1') ? "checked" : "") . " {$enabled} /> " . htmlentities("não") . " (Oral, " . htmlentities("dejeções, micção") . " e banho)";
		echo "</td></tr>";
		echo "<tr><td colspan='3' ><b>" . htmlentities("Nível de consciência") . ":</b>";
		echo "<input type='radio' class='preconshosp OBG_RADIO' name='preconshosp' value=3 " . (($dados->preconshosp == '3') ? "checked" : "") . " {$enabled} /> LOTE";
		echo "<input type='radio' class='preconshosp OBG_RADIO' name='preconshosp' value=2 " . (($dados->preconshosp == '2') ? "checked" : "") . " {$enabled} /> Desorientado";
		echo "<input type='radio' class='preconshosp OBG_RADIO' name='preconshosp' value=1 " . (($dados->preconshosp == '1') ? "checked" : "") . " {$enabled} /> Inconsciente";
		echo "</td></tr>";
		echo "<tr><td colspan='3' ><b>" . htmlentities("alimentação") . ":</b>";
		echo "<input type='radio' class='prealimhosp OBG_RADIO' name='prealimhosp' value=4 " . (($dados->prealimhosp == '4') ? "checked" : "") . " {$enabled} /> " . htmlentities("não") . " assistida";
		echo "<input type='radio' class='prealimhosp OBG_RADIO' name='prealimhosp' value=3 " . (($dados->prealimhosp == '3') ? "checked" : "") . " {$enabled} /> Assistida oral";
		echo "<input type='radio' class='prealimhosp OBG_RADIO' name='prealimhosp' value=2 " . (($dados->prealimhosp == '2') ? "checked" : "") . " {$enabled} /> Enteral";
		echo "<input type='radio' class='prealimhosp OBG_RADIO' name='prealimhosp' value=1 " . (($dados->prealimhosp == '1') ? "checked" : "") . " {$enabled} /> Parenteral";
		echo "</td></tr>";
		echo "<tr><td colspan='3' ><b>" . htmlentities("Úlceras de pressão") . ":</b>";
		echo "<input type='radio' class='preulcerahosp OBG_RADIO' name='preulcerahosp' value=2 " . (($dados->preulcerahosp == '2') ? "checked" : "") . " {$enabled} /> " . htmlentities("não") . "";
		echo "<input type='radio' class='preulcerahosp OBG_RADIO' name='preulcerahosp' value=1 " . (($dados->preulcerahosp == '1') ? "checked" : "") . " {$enabled} /> Sim";
		echo "</td></tr>";
	}

	public function cond_pre_id($dados, $enabled) {
		echo "<thead><tr>";
		$score = "SCORE<input type='text' readonly size='2' name='sdom' value='{$dados->sdom}' {$acesso} />";
		echo "<th colspan='3' >" . htmlentities("Condições do paciente pré-Internação") . " domiciliar. {$score}</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='2' ><b>" . htmlentities("locomoção") . ":</b>";
		echo "<input type='radio' class='prelocodomic OBG_RADIO' name='prelocodomic' value=3 " . (($dados->prelocodomic == '3') ? "checked" : "") . " {$enabled} /> Deambulava";
		echo "<input type='radio' class='prelocodomic OBG_RADIO' name='prelocodomic' value=2 " . (($dados->prelocodomic == '2') ? "checked" : "") . " {$enabled} /> Locomovia-se com " . htmlentities("Auxílio") . " (Muletas ou Cadeiras)";
		echo "<input type='radio' class='prelocodomic OBG_RADIO' name='prelocodomic' value=1 " . (($dados->prelocodomic == '1') ? "checked" : "") . " {$enabled} /> Restrito ao leito";
		echo "</td></tr>";
		echo "<tr><td colspan='2' ><b>Autonomia para higiene pessoal:</b>";
		echo "<input type='radio' class='prehigidomic OBG_RADIO' name='prehigidomic' value=2 " . (($dados->prehigidomic == '2') ? "checked" : "") . " {$enabled} /> Sim";
		echo "<input type='radio' class='prehigidomic OBG_RADIO' name='prehigidomic' value=1 " . (($dados->prehigidomic == '1') ? "checked" : "") . " {$enabled} /> " . htmlentities("Não") . " (Oral, " . htmlentities("dejeções, micção") . " e banho)";
		echo "</td></tr>";
		echo "<tr><td colspan='2' ><b>" . htmlentities("Nível de consciência") . ":</b>";
		echo "<input type='radio' class='preconsdomic OBG_RADIO' name='preconsdomic' value=3 " . (($dados->preconsdomic == '3') ? "checked" : "") . " {$enabled} /> LOTE";
		echo "<input type='radio' class='preconsdomic OBG_RADIO' name='preconsdomic' value=2 " . (($dados->preconsdomic == '2') ? "checked" : "") . " {$enabled} /> Desorientado";
		echo "<input type='radio' class='preconsdomic OBG_RADIO' name='preconsdomic' value=1 " . (($dados->preconsdomic == '1') ? "checked" : "") . " {$enabled} /> Inconsciente";
		echo "</td></tr>";
		echo "<tr><td colspan='2' ><b>" . htmlentities("Alimentação") . ":</b>";
		echo "<input type='radio' class='prealimdomic' name='prealimdomic' value=4 " . (($dados->prealimdomic == '4') ? "checked" : "") . " {$enabled} /> " . htmlentities("Não") . " assistida";
		echo "<input type='radio' class='prealimdomic OBG_RADIO' name='prealimdomic' value=3 " . (($dados->prealimdomic == '3') ? "checked" : "") . " {$enabled} /> Assistida oral";
		echo "<input type='radio' class='prealimdomic OBG_RADIO' name='prealimdomic' value=2 " . (($dados->prealimdomic == '2') ? "checked" : "") . " {$enabled} /> Enteral";
		echo "<input type='radio' class='prealimdomic OBG_RADIO' name='prealimdomic' value=1 " . (($dados->prealimdomic == '1') ? "checked" : "") . " {$enabled} /> Parenteral";
		echo "</td></tr>";
		echo "<tr><td colspan='2' ><b>" . htmlentities("Úlceras de pressão") . ":</b>";
		echo "<input type='radio' class='preulceradomic OBG_RADIO' name='preulceradomic' value=2 " . (($dados->preulceradomic == '2') ? "checked" : "") . " {$enabled} /> " . htmlentities("Não") . "";
		echo "<input type='radio' class='preulceradomic OBG_RADIO' name='preulceradomic' value=1 " . (($dados->preulceradomic == '1') ? "checked" : "") . " {$enabled} /> Sim";
		echo "</td></tr>";
	}

	public function cont_inter_domiciliar($dados, $enabled) {

		echo "<thead><tr>";
		$score = "SCORE<input type='text' readonly size='2' name='sobj' value='{$dados->sobj}' {$acesso} />";
		echo "<th colspan='3' >Objetivo da continuidade da " . htmlentities("Internação") . " domiciliar. {$score}</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='2' ><b>" . htmlentities("locomoção") . ":</b>";
		echo "<input type='radio' class='objlocomocao OBG_RADIO' name='objlocomocao' value=3 " . (($dados->objlocomocao == '3') ? "checked" : "") . " {$enabled} /> Deambular";
		echo "<input type='radio' class='objlocomocao OBG_RADIO' name='objlocomocao' value=2 " . (($dados->objlocomocao == '2') ? "checked" : "") . " {$enabled} /> Locomover-se com " . htmlentities("Auxílio") . " (Muletas ou Cadeiras)";
		echo "<input type='radio' class='objlocomocao OBG_RADIO' name='objlocomocao' value=1 " . (($dados->objlocomocao == '1') ? "checked" : "") . " {$enabled} /> Restrito ao leito";
		echo "</td></tr>";
		echo "<tr><td colspan='2' ><b>Autonomia para higiene pessoal:</b>";
		echo "<input type='radio' class='objhigiene OBG_RADIO' name='objhigiene' value=2 " . (($dados->objhigiene == '2') ? "checked" : "") . " {$enabled} /> Sim";
		echo "<input type='radio' class='objhigiene OBG_RADIO' name='objhigiene' value=1 " . (($dados->objhigiene == '1') ? "checked" : "") . " {$enabled} /> " . htmlentities("Não") . " (Oral, " . htmlentities("dejeções, micção") . " e banho)";
		echo "</td></tr>";
		echo "<tr><td colspan='2' ><b>" . htmlentities("Nível de consciência") . ":</b>";
		echo "<input type='radio' class='objcons OBG_RADIO' name='objcons' value=3 " . (($dados->objcons == '3') ? "checked" : "") . " {$enabled} /> LOTE";
		echo "<input type='radio' class='objcons OBG_RADIO' name='objcons' value=2 " . (($dados->objcons == '2') ? "checked" : "") . " {$enabled} /> Desorientado";
		echo "<input type='radio' class='objcons OBG_RADIO' name='objcons' value=1 " . (($dados->objcons == '1') ? "checked" : "") . " {$enabled} /> Inconsciente";
		echo "</td></tr>";
		echo "<tr><td colspan='2' ><b>" . htmlentities("Alimentação") . ":</b>";
		echo "<input type='radio' class='objalimento OBG_RADIO' name='objalimento' value=4 " . (($dados->objalimento == '4') ? "checked" : "") . " {$enabled} /> " . htmlentities("Não") . " assistida";
		echo "<input type='radio' class='objalimento OBG_RADIO' name='objalimento' value=3 " . (($dados->objalimento == '3') ? "checked" : "") . " {$enabled} /> Assistida oral";
		echo "<input type='radio' class='objalimento OBG_RADIO' name='objalimento' value=2 " . (($dados->objalimento == '2') ? "checked" : "") . " {$enabled} /> Enteral";
		echo "<input type='radio' class='objalimento OBG_RADIO' name='objalimento' value=1 " . (($dados->objalimento == '1') ? "checked" : "") . " {$enabled} /> Parenteral";
		echo "</td></tr>";
		echo "<tr><td colspan='2' ><b>" . htmlentities("Úlceras de pressão") . ":</b>";
		echo "<input type='radio' class='objulcera OBG_RADIO' name='objulcera' value=2 " . (($dados->objulcera == '2') ? "checked" : "") . " {$enabled} /> " . htmlentities("Não") . "";
		echo "<input type='radio' class='objulcera OBG_RADIO' name='objulcera' value=1 " . (($dados->objulcera == '1') ? "checked" : "") . " {$enabled} /> Sim";
		echo "</td></tr>";
	}

	public function capitacao_medica($dados, $acesso, $secmed = false) {
		$enabled = "";
        $titulo = 'Nova';
		if ($acesso == "readonly"){
			$enabled = "disabled";
			$secmed = true;
			$titulo = "Visualizar";
		}
		echo "<center><h1>" . htmlentities($titulo." Avaliação Médica") . "</h1></center>";
		//echo "<center><h2>{$dados->pacientenome}</h2></center>";
        $nomeEmpresa='';
        if(!empty($dados->empresa)){
            $responseEmpresa = Filial::getEmpresaById($dados->empresa);
            $nomeEmpresa = $responseEmpresa['nome'];
        }

		$this->cabecalho($dados, $nomeEmpresa, $dados->convenio);

		if ($acesso == "readonly") {

			echo "<center><h2>Respons&aacute;vel: {$dados->usuario} - Data: {$dados->data}</h2></center>";

            $caminho = "/medico/imprimir_ficha.php?id={$dados->capmedica}";

            echo "<button   caminho='$caminho' empresa-relatorio='{$dados->empresa}' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  role='button' aria-disabled='false'><span class='ui-button-text'>Imprimir</span></button>";

//            echo"<form method=post target='_blank' action='imprimir_ficha.php?id={$dados->capmedica}'>";
//			echo"<input type=hidden value={$dados->capmedica} class='imprimir_ficha_mederi'</input>";
//			echo "<button id='imprimir_ficha_mederi' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' type='submit' role='button' aria-disabled='false'>
//  		<span class='ui-button-text'>Imprimir</span></button>";
//			echo "</form>";
			if ($_SESSION['tipo_user'] === 'administrativo' || $_SESSION['adm_user'] == 1) {
				$this->addFormSugestao('fcav', $dados->capmedica, $dados->pacienteid, $dados->usuario);
			}
		}

		$this->plan;


		echo "<div id='div-ficha-medica'><br/>";

		echo alerta();
		echo '<div id="saving"
										class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-default"
										style="position: fixed;
										top: 20px;
										right: 40px;
										width: 120px;
										height: 20px;
										display: none;
										background-color: #fff;
										border: 1px solid #000;
										border-radius: 2px;
										-webkit-box-shadow: -3px 1px 10px 0px rgba(50, 50, 50, 0.71);
										-moz-box-shadow:    -3px 1px 10px 0px rgba(50, 50, 50, 0.71);
										box-shadow:         -3px 1px 10px 0px rgba(50, 50, 50, 0.71);
										">';
		echo '	<span style="vertical-align: middle; color: #8B0000;">Salvando...</span>';
		echo '</div>';
		echo "<form rel='persist'>";
		echo "<input type='hidden' name='paciente'  value='{$dados->pacienteid }' />";
		echo "<br/><table class='mytable' width=100% ><tbody>";
		echo "<thead><tr>";
		echo "<th colspan='3' >Motivo da " . htmlentities("hospitalização") . "</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='2' ><textarea cols=100 rows=5 {$acesso} name='motivo' class='OBG' >{$dados->motivo}</textarea>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Problemas Ativos</th>";
		echo "</tr></thead>";
		if ($acesso != "readonly") {
			echo "<tr><td colspan='3' ><input type='text' name='busca-problemas-ativos' {$acesso} size=20 /></td></tr>";
			echo"<tr><td colspan='3' ><input type='checkbox' id='outros-prob-ativos' name='outros-prob-ativos' {$acesso} />Outros.
							<span class='outro_ativo' ><input type='text' name='outro-prob-ativo' class='outro_ativo' {$acesso} size='20'/>
							<button id='add-prob-ativo' tipo=''  >+ </button></span></td></tr>";
		}
		echo "<tr><td colspan='2' >
  <table id='problemas_ativos'>";
		if (!empty($dados->ativos)) {
			foreach ($dados->ativos as $a) {
				echo "<tr><td>{$a}</td></tr>";
			}
		}

		echo "</table></td></tr>";


		if ($acesso == "readonly") {
			$this->alergia_medicamentosa_listagem($dados->pacienteid);
		} else {
			$this->alergia_medicamentosa($dados->pacienteid);
		}
		echo "</table>";

		$this->alergiaAlimentar($dados, $acesso);

		echo "<table class='mytable' width='100%'>";

		echo "<thead><tr><th colspan='3' >Comorbidades (S/N)</th></tr></thead>";
		echo "<tr><td><input type='text' class='boleano OBG' name='cancer' value='{$dados->cancer}' {$acesso} /><b>Câncer</b></td>";
		echo "<td colspan='2'><input type='text' class='boleano OBG' name='cardiopatia' value='{$dados->cardiopatia}' {$acesso} /><b>Cardiopatia</b></td></tr>";
		echo "<tr><td><input type='text' class='boleano OBG' name='psiquiatrico' value='{$dados->psiquiatrico}' {$acesso} /><b>" . htmlentities("Distúrbio psiquiátrico") . "</b></td>";
		echo "<td colspan='2'><input type='text' class='boleano OBG' name='dm' value='{$dados->dm}' {$acesso} /><b>DM</b></td></tr>";
		echo "<tr><td><input type='text' class='boleano OBG' name='neurologica' value='{$dados->neuro}' {$acesso} /><b>" . htmlentities("doença neurológica") . "</b></td>";
		echo "<td colspan='2'><input type='text' class='boleano OBG' name='reumatologica' value='{$dados->reumatologica}' {$acesso} /><b>" . htmlentities("Doença Reumatológica") . "</b></td></tr>";
		echo "<tr><td><input type='text' class='boleano OBG' name='glaucoma' value='{$dados->glaucoma}' {$acesso} /><b>Glaucoma</b></td>";
		echo "<td colspan='2'><input type='text' class='boleano OBG' name='has' value='{$dados->has}' {$acesso} /><b>HAS</b></td></tr>";
		echo "<tr><td><input type='text' class='boleano OBG' name='hepatopatia' value='{$dados->hepatopatia}' {$acesso} /><b>Hepatopatia</b></td>";
		echo "<td colspan='2'><input type='text' class='boleano OBG' name='nefropatia' value='{$dados->nefropatia}' {$acesso} /><b>Nefropatia</b></td></tr>";
		echo "<tr><td><input type='text' class='boleano OBG' name='obesidade' value='{$dados->obesidade}' {$acesso} /><b>Obesidade</b></td>";
		echo "<td colspan='2'><input type='text' class='boleano OBG' name='pneumopatia' value='{$dados->pneumopatia}' {$acesso} /><b>Pneumopatia</b></td></tr>";

		echo "<thead><tr>";
		echo "<th colspan='3' >" . htmlentities("Internações nos últimos") . " 12 meses</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='3' ><b>Motivo:</b><input type='text' name='historicointernacao' class='internacao_add' size='100' maxlength='250'  {$acesso} /><button id='add-internacao' tipo='' style='cursor:pointer;' >+ </button></td></tr>";
		echo "<tr><td colspan='3'><table id='internacao' style='width:100% '>";
		$sql4 = "SELECT MOTIVO_INTERNACAO AS descricao from internacoescapmedica where ID_CAPMEDICA = {$dados->capmedica} UNION SELECT historicointernacao AS descricao FROM capmedica WHERE id = {$dados->capmedica}";
		$result4 = mysql_query($sql4);
		$contador = 0;
		while ($row4 = mysql_fetch_array($result4)) {
			$nome = $row4['descricao'];
			if ($nome != '') {
				$contador++;
				if ($acesso == "readonly") {
					$classe = '';
				} else {
					$classe = " <img src='../utils/delete_16x16.png' class='del-internacao' title='Excluir' border='0' {$enabled} style='cursor:pointer;'>";
				}
				echo "<tr><td colspan='2' class='internacao' desc='{$nome}' > $classe <b>Motivo Internação $contador:</b>  {$nome} </td> </tr>";
			}
		}
		echo "</table></td></tr>";
        $sql2 = "
                SELECT
                    operadoras_planosdesaude.OPERADORAS_ID
                FROM
                    clientes inner join
                    operadoras_planosdesaude ON (clientes.convenio = operadoras_planosdesaude.PLANOSDESAUDE_ID)				 
                WHERE
                    idClientes = '{$dados->pacienteid}'";

        $result = mysql_fetch_array(mysql_query($sql2));

        if ($acesso != "readonly") {
            if ($this->plan == 7 || $this->plan == 27 || $this->plan == 28 || $this->plan == 29 || $this->plan == 80) {
                $this->score_petrobras();
            } elseif (!in_array($result['OPERADORAS_ID'], [11])) {
                if($dados->dataAvaliacao >= '2017-05-05') {
                    $this->score_abmid();
                }

                if($dados->modeloNead != '2015'){
                    $tipo_relatorio = 'capmedica';
                    echo "</table>";

                    if(empty($dados->capmedica) || $dados->capmedica == null) {
                        $sql = <<<SQL
SELECT 
  MAX(ID) AS ultimaAvaEnf,
  COUNT(ID) AS numAvas 
FROM
  avaliacaoenfermagem
WHERE
  PACIENTE_ID = '{$dados->pacienteid}'
SQL;
                        $rs = mysql_query($sql);
                        $rowAvaEnf = mysql_fetch_array($rs);

                        if (count($rowAvaEnf) >= '1') {
                            $dados->capmedica = $rowAvaEnf['ultimaAvaEnf'];
                            $tipo_relatorio = 'avaliacaoenfermagem';
                        }
                    }

                    \App\Controllers\Score::scoreNead2016($dados->pacienteid, $dados->capmedica, $tipo_relatorio, $dados->classificacao_paciente);
                    echo "<table class='mytable' width='100%'>";
                } else {
                    $this->scoreNead($dados->capmedica);
                }
            }
        }

		echo "<thead ><tr >";
		echo "<th colspan='3'>Ficha de " . htmlentities("Avaliação") . " Mederi.</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='2'><b>Local de " . htmlentities("Avaliação") . " do paciente:</b><input type='radio' class='local_av'  name='local_av' value='1' " . (($dados->local_av == '1') ? "checked" : "") . " {$enabled}> Domicilio/" . htmlentities("intituição") . " de Apoio</input><input type='radio' class='local_av' name='local_av' value='2' " . (($dados->local_av == '2') ? "checked" : "") . " {$enabled}>Hospital</input></td></tr>";

		echo "<thead><tr>";
		echo "<th colspan='3' ><b>Parecer " . htmlentities("Médico") . "</b></th>";
		echo "</tr></thead>";
		echo"<tr><td colspan='3'><textarea cols=100 rows=2 type=text id='prejustparecer' name='pre_justificativa' readonly='readonly' size='100'>{$dados->pre_justificativa}</textarea></td></tr>";
		echo "<tr><td colspan='2' >";
		echo "<input type='radio' id='fav1' name='parecer' value='0' " . (($dados->parecer == '0') ? "checked" : "") . " {$enabled} /> <label style='cursor:pointer;' for='fav1'>" . htmlentities("Favorável") . " </label>";
		echo "<input type='radio' id='fav2' name='parecer' value='1' " . (($dados->parecer == '1') ? "checked" : "") . " {$enabled} /> <label style='cursor:pointer;' for='fav2'>" . htmlentities("Favorável") . " com ressalvas</label>";
		echo "<input type='radio' id='fav3' name='parecer' value='2' " . (($dados->parecer == '2') ? "checked" : "") . " {$enabled} /> <label style='cursor:pointer;' for='fav3'>" . htmlentities("Contrário") . "</label>";
		echo "<br/>Justificativa:<br/><textarea cols=100 rows=5 name='justparecer'  id='justparecer' class='OBG' {$acesso} >{$dados->justificativa}</textarea>";
		echo "</td></tr>";
		echo"<tr><td colspan='3'><b>Plano  de Desmame:</b><br/><textarea cols=100 rows=2 type=text id='desmame' name='plano_desmame' class='OBG' {$acesso} size='100'>{$dados->plano_desmame}</textarea></td></tr>";

		$this->modalidade_homecare($dados->modalidade,$this->plan,$enabled);

		echo "</tbody></table></form></div>";

		if ($acesso != "readonly") {
			$this->salv_capmed($dados->pacienteid);
			echo "<button id='salvar-capmed' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
			echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
		}

		echo $this->createDialogLocalStorageWarning();
	}

	public function detalhes_capitacao_medica_editar($id, $origem = null) {
		$id = anti_injection($id, "numerico");
		$sql = "SELECT cap.*, pac.nome as pnome,pac.idClientes,usr.nome as unome, DATE_FORMAT(cap.data,'%d/%m/%Y') as fdata, comob.*,
e.nome as nomeEmpresa, pl.nome as nomePlano
  	FROM capmedica as cap
  	LEFT OUTER JOIN empresas as e on cap.empresa = e.id
  	LEFT OUTER JOIN planosdesaude as pl on cap.plano = pl.id
  	LEFT OUTER JOIN comorbidades as comob ON cap.id = comob.capmed
  	LEFT OUTER JOIN problemasativos as prob ON cap.id = prob.capmed
  	LEFT OUTER JOIN usuarios as usr ON cap.usuario = usr.idUsuarios
  	LEFT OUTER JOIN clientes as pac ON cap.paciente = pac.idClientes
  
  	WHERE cap.id = '{$id}'";
		
		$d = new DadosFicha();
		$d->capmedica = $id;
		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {

			$d->pacienteid = $row['idClientes'];
			$d->empresa = $row['empresa'];
			$d->convenio = $row['plano'];
            $d->nomeEmpresa = $row['nomeEmpresa'];
            $d->nomePlano = $row['nomePlano'];
			$d->usuario = ucwords(strtolower($row["unome"]));
			$d->data = $row['fdata'];
			$d->pacientenome = ucwords(strtolower($row["pnome"]));
			$d->motivo = $row['motivo'];
			$d->alergiamed = $row['alergiamed'];
			$d->alergiaalim = $row['alergiaalim'];
			$d->tipoalergiamed = $row['tipoalergiamed'];
			$d->tipoalergiaalim = $row['tipoalergiaalim'];
			$d->prelocohosp = $row['hosplocomocao'];
			$d->prehigihosp = $row['hosphigiene'];
			$d->preconshosp = $row['hospcons'];
			$d->prealimhosp = $row['hospalimentacao'];
			$d->preulcerahosp = $row['hospulcera'];
			$d->prelocodomic = $row['domlocomocao'];
			$d->prehigidomic = $row['domhigiene'];
			$d->preconsdomic = $row['domcons'];
			$d->prealimdomic = $row['domalimentacao'];
			$d->preulceradomic = $row['domulcera'];
			$d->objlocomocao = $row['objlocomocao'];
			$d->objhigiene = $row['objhigiene'];
			$d->objcons = $row['objcons'];
			$d->objalimento = $row['objalimentacao'];
			$d->objulcera = $row['objulcera'];
			$d->shosp = $d->prelocohosp + $d->prehigihosp + $d->preconshosp + $d->prealimhosp + $d->preulcerahosp;
			$d->sdom = $d->prelocodomic + $d->prehigidomic + $d->preconsdomic + $d->prealimdomic + $d->preulceradomic;

			$d->sobj = $d->objlocomocao + $d->objhigiene + $d->objcons + $d->objalimento + $d->objulcera;

			$d->qtdinternacoes = $row['qtdinternacoes'];
			$d->historico = $row['historicointernacao'];
			$d->parecer = $row['parecer'];
			$d->justificativa = $row['justificativa'];

			$d->cancer = $row['cancer'];
			$d->psiquiatrico = $row['psiquiatrico'];
			$d->neuro = $row['neuro'];
			$d->glaucoma = $row['glaucoma'];
			$d->hepatopatia = $row['hepatopatia'];
			$d->obesidade = $row['obesidade'];
			$d->cardiopatia = $row['cardiopatia'];
			$d->dm = $row['dm'];
			$d->reumatologica = $row['reumatologica'];
			$d->has = $row['has'];
			$d->nefropatia = $row['nefropatia'];
			$d->pneumopatia = $row['pneumopatia'];
			$d->modalidade = $row['MODALIDADE'];
			$d->local_av = $row['LOCAL_AV'];
			$d->plano_desmame = $row['PLANO_DESMAME'];
			$d->pre_justificativa = $row['PRE_JUSTIFICATIVA'];
			$d->equipamento_obs = $row['EQUIPAMENTO_OBS'];
            $d->modeloNead = $row['MODELO_NEAD'];
            $d->classificacao_paciente = $row['CLASSIFICACAO_NEAD'];
		}
		$sql = "SELECT p.DESCRICAO as nome , p.cid FROM problemasativos as p WHERE p.capmed = '{$id}' ";
		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {

			$elemento[$row['cid']] = array("1" => $row['cid'], "2" => $row['nome']);
		}

		$sql = "SELECT p.DESCRICAO as nome, p.cid as codigo FROM problemasativos as p where p.capmed = '{$id}' ";
		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {
			if (array_key_exists($row['codigo'], $elemento)) {

				$d->ativos[] = $elemento[$row['codigo']];
			}
		}

		$sql2 = "SELECT sc_pac.OBSERVACAO,sc_pac.ID_PACIENTE, sc_item.DESCRICAO ,sc_item.PESO ,sc_item.GRUPO_ID ,
  sc_item.ID ,sc.id as tbplano,cap.justificativa,DATE(sc_pac.DATA) as DATA,
  grup.DESCRICAO as grdesc  FROM score_paciente sc_pac
  LEFT OUTER JOIN score_item sc_item ON sc_pac.ID_ITEM_SCORE = sc_item.ID
  LEFT OUTER JOIN capmedica cap ON sc_pac.ID_CAPMEDICA = cap.id
  LEFT OUTER JOIN grupos grup ON sc_item.GRUPO_ID = grup.ID
  LEFT OUTER JOIN score sc ON sc_item.SCORE_ID = sc.ID
  WHERE sc_pac.ID_CAPMEDICA = '{$id}'";

		$result2 = mysql_query($sql2);

		while ($row2 = mysql_fetch_array($result2)) {
			if ($row2['ID'] == 71) {
				$d->diag_principal = $row2['OBSERVACAO'];
			}
			if ($row2['ID'] == 69) {
				$d->diag_principal = $row2['OBSERVACAO'];
			}

			if ($row2['ID'] == 72) {
				$d->diag_secundario = $row2['OBSERVACAO'];
			}
			if ($row2['ID'] == 70) {
				$d->diag_secundario = $row2['OBSERVACAO'];
			}
			if ($row2['ID'] == 66) {
				$d->atend_24 = $row2['OBSERVACAO'];
			}
			if ($row2['ID'] == 62) {
				$d->atend_24 = $row2['OBSERVACAO'];
			}
			if ($row2['ID'] == 63) {
				$d->atend_24_2 = $row2['OBSERVACAO'];
			}
			if ($row2['ID'] == 67) {
				$d->atend_12 = $row2['OBSERVACAO'];
			}
			if ($row2['ID'] == 64) {
				$d->atend_12 = $row2['OBSERVACAO'];
			}
			if ($row2['ID'] == 65) {
				$d->atend_06 = $row2['OBSERVACAO'];
			}
			if ($row2['ID'] == 68) {
				$d->atend_06 = $row2['OBSERVACAO'];
			}
			if ($row2['ID'] == 73) {
				$d->atend_ad = $row2['OBSERVACAO'];
			}
			if ($row2['ID'] == 74) {
				$d->atend_ad = $row2['OBSERVACAO'];
			}
		}
		//$this->cabecalho(pacienteid);
		$this->editar_capitacao_medica($d, '', ($origem === 'editar' ? false : true),$origem);
		// $this->score_abmid($d);
		// echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
	}

//detalhes_capitacao_medica_editar

	public function editar_capitacao_medica($dados, $acesso, $nova = true,$origem = null) {

		$enabled = "";

		$id_presc = '-1';
		$id_presc_sol = '';

		$sql = "Select Max(id) from capmedica where paciente ='{$dados->pacienteid }'";
		$result = mysql_query($sql);
		$maior_cap = mysql_result($result, 0, 0);

		$sql3 = "select DATE_FORMAT(data,'%Y-%m-%d %H:%i:%s')  from capmedica where id = '{$maior_cap}' ";
		//$sql3="select DATE_FORMAT(data,'%Y-%m-%d')  from capmedica where id = '{$maior_cap}' "; Ricardo 21/05/2013
		$r2 = mysql_query($sql3);
		$data_cap = mysql_result($r2, 0);
		$datalimit = date('Y/m/d', strtotime("+2 days", strtotime($data_cap)));


		$sql2 = "SELECT pre.id, sol.idPrescricao from prescricoes as pre LEFT JOIN solicitacoes as sol ON(pre.id=sol.idPrescricao) WHERE pre.ID_CAPMEDICA = '{$maior_cap}'";
		$result = mysql_query($sql2);
		while ($row2 = mysql_fetch_array($result)) {
			if ($row2['id'] > 0) {
				$id_presc = $row2['id'];
				$id_presc_sol = $row2['idPrescricao'];
			}
		}
		if (($id_presc == $id_presc_sol || $dados->capmedica < $maior_cap && $nova) || empty($origem)) {
			echo "<center><h1>Nova Avaliação Médica</h1></center>";
		} else {
			echo "<center><h1>Editar Avaliação Médica</h1></center>";
		}
		$this->cabecalho($dados, $dados->nomeEmpresa, $dados->nomePlano);
		echo "<center><h2>Respons&aacute;vel: {$dados->usuario} - Data: {$dados->data}</h2></center>";

		if($nova === true)
			self::showVerificacaoSugestoes('fcav', $dados->capmedica, $dados->pacienteid, $dados->usuario, $num_sugestao);

		$this->plan;
		echo "<div id='div-ficha-medica' ><br/>";

		echo alerta();
		echo '<div id="saving"
                class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-default"
                style="position: fixed;
                top: 20px;
                right: 40px;
                width: 120px;
                height: 20px;
                display: none;
                background-color: #fff;
                border: 1px solid #000;
                border-radius: 2px;
                -webkit-box-shadow: -3px 1px 10px 0px rgba(50, 50, 50, 0.71);
                -moz-box-shadow:    -3px 1px 10px 0px rgba(50, 50, 50, 0.71);
                box-shadow:         -3px 1px 10px 0px rgba(50, 50, 50, 0.71);
                ">';
		echo '	<span style="vertical-align: middle; color: #8B0000;">Salvando...</span>';
		echo '</div>';
		echo "<form rel='persist'>";
		echo "<input type='hidden' name='paciente'  value='{$dados->pacienteid }' />";
		echo "<input type='hidden' name='capmedicaid'  value='{$dados->capmedica }' />";
		echo "<input type='hidden' name='num_sugestao'  value='" . (isset($num_sugestao) ? $num_sugestao : 0) . "' />";
		echo "<br/><table class='mytable' style='width:100%'>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Motivo da hospitaliza&ccedil;&atilde;o</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='2' ><textarea cols=100 rows=5 {$acesso} name='motivo' class='OBG' >{$dados->motivo}</textarea>";
		//<input type='text' size=100 value='{$row['motivo']}' name='motivo' class='OBG' /></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3' >Problemas Ativos</th>";
		echo "</tr></thead>";

		if ($acesso != "readonly") {
			echo "<tr><td colspan='2' ><input type='text' name='busca-problemas-ativos' {$acesso} size=100 /></td></tr>";
			echo"<tr><td colspan='2' ><input type='checkbox' id='outros-prob-ativos' name='outros-prob-ativos' {$acesso} />Outros.<span class='outro_ativo' ><input type='text' name='outro-prob-ativo' class='outro_ativo' {$acesso} size=75/><button id='add-prob-ativo' tipo=''  >+ </button></span></td></tr>";
		}
		echo "<tr><td colspan='2' ><table id='problemas_ativos'>";
		if (!empty($dados->ativos)) {
			foreach ($dados->ativos as $a) {
				$x = $a[1];
				$y = $a[2];
				echo "<tr><td  class='prob-ativos' cod={$x} desc='{$y}'><img src='../utils/delete_16x16.png' class='del-prob-ativos' title='Excluir' border='0' >{$y}</td></tr>";
			}
		}

		echo "</table></td></tr>";

		if ($acesso == "readonly") {
			$this->alergia_medicamentosa_listagem($dados->pacienteid);
		} else {
			$this->alergia_medicamentosa($dados->pacienteid);
		}
		echo "</table>";

		$this->alergiaAlimentar($dados, $acesso);

		echo "<table class='mytable' width='100%'>";
		echo "<thead><tr>";
		echo "<th colspan='3' >Comorbidades (S/N)</th>";
		echo "</tr></thead>";
		echo "<tr><td><input type='text' class='boleano OBG' name='cancer' value='{$dados->cancer}' {$acesso} /><b>Câncer</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='cardiopatia' value='{$dados->cardiopatia}' {$acesso} /><b>Cardiopatia</b></td>";
		echo "</tr><tr><td><input type='text' class='boleano OBG' name='psiquiatrico' value='{$dados->psiquiatrico}' {$acesso} /><b>Dist&uacute;rbio Psiqui&aacute;trico</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='dm' value='{$dados->dm}' {$acesso} /><b>DM</b></td>";
		echo "</tr><tr><td><input type='text' class='boleano OBG' name='neurologica' value='{$dados->neuro}' {$acesso} /><b>Doen&ccedil;a Neurol&oacute;gica</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='reumatologica' value='{$dados->reumatologica}' {$acesso} /><b>Doen&ccedil;a Reumatol&oacute;gica</b></td>";
		echo "</tr><tr><td><input type='text' class='boleano OBG' name='glaucoma' value='{$dados->glaucoma}' {$acesso} /><b>Glaucoma</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='has' value='{$dados->has}' {$acesso} /><b>HAS</b></td>";
		echo "</tr><tr><td><input type='text' class='boleano OBG' name='hepatopatia' value='{$dados->hepatopatia}' {$acesso} /><b>Hepatopatia</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='nefropatia' value='{$dados->nefropatia}' {$acesso} /><b>Nefropatia</b></td>";
		echo "</tr><tr><td><input type='text' class='boleano OBG' name='obesidade' value='{$dados->obesidade}' {$acesso} /><b>Obesidade</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='pneumopatia' value='{$dados->pneumopatia}' {$acesso} /><b>Pneumopatia</b></td>";
		echo "</tr><thead><tr>";

		echo "<thead><tr>";
		echo "<th colspan='3' >" . htmlentities("Internações nos últimos") . " 12 meses</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='3' ><b>Motivo:</b><input type='text' name='historicointernacao' class='internacao_add' size='100' maxlength='250'  {$acesso} /><button id='add-internacao' tipo='' style='cursor:pointer;' >+ </button></td></tr>";
		echo "<tr><td colspan='3'><table id='internacao' style='width:100% '>";
		$sql4 = "SELECT MOTIVO_INTERNACAO AS descricao from internacoescapmedica where ID_CAPMEDICA = {$dados->capmedica} UNION SELECT historicointernacao AS descricao FROM capmedica WHERE id = {$dados->capmedica}";
		$result4 = mysql_query($sql4);
		$contador = 0;
		while ($row4 = mysql_fetch_array($result4)) {
			$contador++;
			$nome = $row4['descricao'];
			if ($acesso == "readonly") {
				$classe = '';
			} else {
				$classe = " <img src='../utils/delete_16x16.png' class='del-internacao' title='Excluir' border='0' {$enabled} style='cursor:pointer;'>";
			}
			if ($nome != '')
				echo "<tr><td colspan='2' class='internacao' desc='{$nome}' > $classe <b>Motivo Internação $contador:</b>  {$nome} </td> </tr>";
		}
		echo "</table></td></tr>";

		$sql2 = "
                SELECT
                    operadoras_planosdesaude.OPERADORAS_ID
                FROM
                    clientes inner join
                    operadoras_planosdesaude ON (clientes.convenio = operadoras_planosdesaude.PLANOSDESAUDE_ID)				 
                WHERE
                    idClientes = '{$dados->pacienteid}'";

        $result = mysql_fetch_array(mysql_query($sql2));

		if ($acesso != "readonly") {
			if ($this->plan == 7 || $this->plan == 27 || $this->plan == 28 || $this->plan == 29 || $this->plan == 80) {
				$this->score_petrobras($dados);
				echo "</table>";
			} elseif (!in_array($result['OPERADORAS_ID'], [11])) {
				$idCapmedica = $dados->capmedica;
				$classificacaoPaciente = $dados->classificacao_paciente;
				if($nova == false){
					$idCapmedica = '';
					$classificacaoPaciente = '';
				}

				\App\Controllers\Score::scoreNead2016($dados->pacienteid, $idCapmedica, "capmedica", $classificacaoPaciente);
			}
		}
        echo "<table class='mytable' width='100%'>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Ficha de " . htmlentities("Avaliação") . " Mederi.</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='2'><b>Local de Avalia&ccedil;&atilde;o do paciente:</b><input type='radio' class='local_av'  name='local_av' value='1' " . (($dados->local_av == '1') ? "checked" : "") . " {$enabled}> Domicilio/Institui&ccedil;&atilde;o de Apoio</input><input type='radio' class='local_av' name='local_av' value='2' " . (($dados->local_av == '2') ? "checked" : "") . " {$enabled}>Hospital</input></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3' >Parecer M&eacute;dico</th>";
		echo "</tr></thead>";
		$preJustificativa = $dados->pre_justificativa;
		if($nova == false){
			$preJustificativa = '';
		}
		echo"<tr><td colspan='3'><textarea cols=100 rows=5 readonly type=text name ='pre_justificativa' id='prejustparecer'>{$preJustificativa}</textarea></td></tr>";
		echo "<tr><td colspan='2' >";
		echo "<input type='radio' name='parecer' value='0' " . (($dados->parecer == '0') ? "checked" : "") . " {$enabled} /> Favor&aacute;vel";
		echo "<input type='radio' name='parecer' value='1' " . (($dados->parecer == '1') ? "checked" : "") . " {$enabled} /> Favor&aacute;vel com resalvas";
		echo "<input type='radio' name='parecer' value='2' " . (($dados->parecer == '2') ? "checked" : "") . " {$enabled} /> Contr&aacute;rio";
		echo "<br/><b>Justificativa:</b><br/><textarea cols=100 rows=5 name='justparecer'  id='justparecer' class='OBG' {$acesso} >{$dados->justificativa}</textarea>";
		echo "</td></tr>";
		echo"<tr><td colspan='3'><b>Plano de Desmame:</b><br/><textarea cols=100 rows=2 type=text id='desmame' class='OBG' name='plano_desmame' {$acesso} size='100'>{$dados->plano_desmame}</textarea></td></tr>";

		$this->modalidade_homecare($dados->modalidade,$this->plan,$enabled);

		echo "</tbody></table></form></div>";
		if ($acesso != "readonly") {
			$this->salv_capmed($dados->pacienteid);
			echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
			if ($id_presc == $id_presc_sol || $dados->capmedica < $maior_cap || $datalimit <= date('Y/m/d')) {
				echo "<button id='salvar-capmed' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
			} else {
				echo "<button id='editar_ficha' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
			}
		}
	}

	private function periodo() {

		echo "<br/><fieldset style='width:340px;text-align:center;'><legend><b>Per&iacute;odo</b></legend>";
		echo "DE" .
			"<input type='text' name='inicio' value='' maxlength='10' size='10'  class='OBG data' />" .
			" AT&Eacute; <input type='text' name='fim' value='' maxlength='10' size='10'  class='OBG data' /></fieldset>";
	}

	public function listar_capitacao_medica($id) {
		//$flag = true;
        echo "<div><center><h1>Avaliação Médica</h1></center></div>";
		$id = anti_injection($id, "numerico");
		$sql3 = "select Max(id) as cap from capmedica where paciente = '{$id}' ";
		$r2 = mysql_query($sql3);
		$maior_cap = mysql_result($r2, 0);
		if ($maior_cap != '') {
			$flag = false;
			$maior_cap = mysql_result($r2, 0);

			$sql2 = "SELECT pre.id, sol.idPrescricao,
  	            DATE_FORMAT(pre.data,'%d/%m/%Y - %T') AS sdata,
                    DATE_FORMAT(pre.inicio,'%d/%m/%Y') AS inicio,
                    DATE_FORMAT(pre.fim,'%d/%m/%Y') AS fim, pre.criador,pre.paciente
                from
                    prescricoes as pre LEFT JOIN
                    solicitacoes as sol ON(pre.id=sol.idPrescricao)
                WHERE pre.Carater IN (2,4) AND
                    pre.STATUS=0  AND
                    pre.ID_CAPMEDICA = '{$maior_cap}'";
			$result = mysql_query($sql2);
			$solicitacao = mysql_result($result, 0, 1);
			$maior_presc = mysql_result($result, 0, 0);
			$inicio = mysql_result($result, 0, 3);
			$fim = mysql_result($result, 0, 4);
			$data = mysql_result($result, 0, 2);
			$criador = mysql_result($result, 0, 5);
			$paciente = mysql_result($result, 0, 6);

			date_default_timezone_set('America/Sao_Paulo');

			$sql = "SELECT
                  DATE_FORMAT(c.data,'%d/%m/%Y %H:%i:%s') as fdata,
                  c.data as capdata,
                  c.id,
                  c.paciente as idpaciente,
                  u.nome,
                  u.tipo as utipo,
                  u.conselho_regional,
                  presc.id as prescid,
                  u.idUsuarios,
                  (Select count(*) from prescricoes P1 where P1.ID_CAPMEDICA =c.id) as controle,	  
                  p.nome as paciente,
                  c.parecer
                FROM 
                  capmedica as c LEFT JOIN
                  usuarios as u ON (c.usuario = u.idUsuarios) LEFT JOIN
                  clientes as p ON (c.paciente = p.idClientes) LEFT JOIN
                  prescricoes as presc ON (c.id = presc.ID_CAPMEDICA AND carater = 4 AND presc.STATUS=0) 
	  
                WHERE
                  c.paciente = '{$id}' 	  
                ORDER BY 
                  c.data DESC";
			$r = mysql_query($sql);
			$flag = true;

			$existe_prescricao = 0;
			$penultimo = 0; //valor falso

			while ($row = mysql_fetch_array($r)) {
                $compTipo = $row['conselho_regional'];

				if ($flag) {

					$p = ucwords(strtolower($row["c.paciente"]));
					$this->cabecalho($id);
					//echo "<center><h2>{$p}</h2></center>";

					echo "<br/><br/>";

					echo "<div><input type='hidden' name='nova_ficha' value='{$id}'></input>";
					$this->salv_capmed($row['idpaciente']);
					$this->editar_prescricao($paciente);
					$this->nova_presc($id);
					$this->presc_aditiva($id);

                    if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_avaliacao']['medico_paciente_avaliacao_novo']) || $_SESSION['adm_user'] == 1) {
						echo "<button id='nova_ficha'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' type='submit' role='button' aria-disabled='false'>
  			<span class='ui-button-text'>Nova</span></button>";
					}
					echo"</div>";
					echo "<br/><table class='mytable' style='width:100%' >";
					echo "<thead><tr>";
					echo "<th width='55%'>Usu&aacute;rio</th>";
					echo "<th>Data</th>";
					echo "<th width='15%'>Detalhes</th>";
					echo "<th>Op&ccedil;&otilde;es</th>";
					echo "</tr></thead>";
					$flag = false;
				}
				$id_presc_sol = '';
				$id_presc_sol = -1;
				$teste1 = $row['idUsuarios'];
				$teste2 = $_SESSION['id_user'];

				$data_atual = date('Y/m/d H:i:s');
				$diaSemanaCapMed = date('w', strtotime($row['capdata']));
				$datalimit = date('Y/m/d H:i:s', strtotime(($diaSemanaCapMed != '5' ? "+24 hours" : "+84 hours"), strtotime($row['capdata'])));

				$u = ucwords(strtolower($row["nome"])) . $compTipo;

				echo "<tr><td>" . ($row['parecer'] == 2 ? "<strong style='color: #800;'>{$u} (CONTRÁRIO)</strong>" : $u) . "</td><td>{$row['fdata']}</td>";
				echo "<td><a href=?view=paciente_med&act=capmed&id={$row['id']}><img src='../utils/capmed_16x16.png' title='Visualizar Avaliação' border='0'></a>";

				//Validação dos botões de avaliação e prescrição
				//Detalhes da prescrição

				$existe_prescricao = $row['prescid']; //Igual a zero se não existir
				if ($existe_prescricao > 0) {
					echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=?view=paciente_med&act=detalhesprescricao&id={$row['id']}&idpac={$row['idpaciente']}><img src='../utils/fichas_24x24.png' width='16' title='Visualizar Prescri&ccedil;&atilde;o' border='0'></a>";
				}

				//Score
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=?view=paciente_med&act=detalhescore&id={$row['id']}&tipo=1&paciente_id={$id}><img src='../utils/menu_fin_16x16.png' title='Visualizar Score' border='0'></a>";


				echo "</td><td>";

				$id_atual = $row['id'];
				$solicitado = $solicitacao;


				if ($id_atual == $maior_cap) {//Verifica se a avaliação é a última inserida
					$penultimo = 1;



					if ($data_atual < $datalimit) {//Se a data atual está dentro do limite de edição
						if ($existe_prescricao > 0) {//Tem prescrição de avaliação
							if ($solicitado != 0) {//Menor que o tempo limite e solicitado pela enfermaria
								//Usar para nova avaliação e Prescrição Aditiva
								if ($_SESSION['adm_user'] == 1 || $_SESSION['modmed'] == 1) {
                                    if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_avaliacao']['medico_paciente_avaliacao_usar_para_nova']) || $_SESSION['adm_user'] == 1) {
                                        echo "<a href=?view=paciente_med&act=anterior_capmed&id={$row['id']}><img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para nova avalia&ccedil;&atilde;o.' border='0'>";
                                    }

									echo"<a href=#><img src='../utils/prescricao_aditiva_24x24.png' width='16' title=' Prescri&ccedil;&atilde;o  Aditiva de Avalia&ccedil;&atilde;o.'  id='presc_adt_avaliacao' paciente='{$id}' data='{$data}'   id_cap='{$maior_cap}'  inicio='{$inicio}' fim='{$fim}'  border='0'>";
								}
							} else {//Menor que o tempo limite e não solicitado pela enfermaria
								//Editar avaliação e prescrição

                                        if (($_SESSION['adm_user'] == 1 || $row['idUsuarios'] == $_SESSION['id_user'] || $_SESSION["adm_ur"] == 1) && $datalimit > $data_atual) {
                                            if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_avaliacao']['medico_paciente_avaliacao_editar']) || $_SESSION['adm_user'] == 1) {
                                                echo "<a href=?view=paciente_med&act=editar_capmed&id={$row['id']}><img src='../utils/edit_16x16.png' title='Editar Avaliação' border='0'>";

                                                echo "<a href=#><img src='../utils/editar_prescricao_24x24.png' width='20' title='Editar Prescri&ccedil;&atilde;o' border='0' class='editar_prescricao'  data='{$data}' paciente='{$id}' antiga='{$maior_presc}' data='{$data}' tipo=4 criador='{$criador}' id_cap='{$maior_cap}' inicio='{$inicio}' fim='{$fim}' busca='true'></a>";
                                            }
                                            }else{
                                            if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_avaliacao']['medico_paciente_avaliacao_usar_para_nova']) || $_SESSION['adm_user'] == 1) {
                                                echo "<a href=?view=paciente_med&act=anterior_capmed&id={$row['id']}><img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para nova avalia&ccedil;&atilde;o.' border='0'>";
                                            }
                                        }
							        }
						} else {//Não tem prescrição de avaliação
							//Prescrição de avaliação pendente
							if ($_SESSION['adm_user'] == 1 || $_SESSION['modmed'] == 1) {
								if($row['parecer'] == 2) {
									if (($_SESSION['adm_user'] == 1 || $row['idUsuarios'] == $_SESSION['id_user'] || $_SESSION["adm_ur"] == 1) && $datalimit > $data_atual) {
                                        if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_avaliacao']['medico_paciente_avaliacao_editar']) || $_SESSION['adm_user'] == 1) {
                                            echo "<a href=?view=paciente_med&act=editar_capmed&id={$row['id']}><img src='../utils/edit_16x16.png' title='Editar Ficha' border='0'>";
                                        }
									}
								} else
									echo"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' class='presc_avaliacao' val={$row['id']}><img src='../utils/alerta_prescricao.png' width='20' title='Prescri&ccedil;&atilde;o de Avalia&ccedil&atilde;o Pendente' ></a>";
							}
						}
					} else {//Fora da data limite
						//Usar para nova avaliação
						if ($_SESSION['adm_user'] == 1 || $_SESSION['modmed'] == 1) {
                            if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_avaliacao']['medico_paciente_avaliacao_usar_para_nova']) || $_SESSION['adm_user'] == 1) {
                                echo "<a href=?view=paciente_med&act=anterior_capmed&id={$row['id']}>
  				<img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para nova avalia&ccedil;&atilde;o.' border='0'>";
                            }
						}
					}
				} else {//Na penúltima avaliação e demais avaliações
					//Exibir "usar para nova prescrição" se o último não tiver prescrição
					if ($penultimo == 1) {

						$penultimo = 0;

						if ($existe_prescricao != 0) {
							if ($_SESSION['adm_user'] == 1 || $_SESSION['modmed'] == 1) {
								echo "<a href=#><img src='../utils/prescricao_16x16.png' width='16' title='Usar para nova Prescri&ccedil;&atilde;o' border='0' class='nova_presc_avaliacao'  data='{$data}' paciente='{$id}'  antiga='{$row['prescid']}'  tipo=4  id_cap='{$maior_cap}' inicio='{$inicio}' fim='{$fim}'  busca='true'></a>";
							}
						}
					}

					//Usar para nova avaliação
					if ($_SESSION['adm_user'] == 1 || $_SESSION['modmed'] == 1) {
                        if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_avaliacao']['medico_paciente_avaliacao_usar_para_nova']) || $_SESSION['adm_user'] == 1) {
                            echo "<a href=?view=paciente_med&act=anterior_capmed&id={$row['id']}>
  					<img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para nova avalia&ccedil;&atilde;o.' border='0'>";
                        }
					}
				}


				echo "</td></tr>";
			}
		} else {
			$flag = true;
		}
		if ($flag) {
            if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_avaliacao']['medico_paciente_avaliacao_novo']) || $_SESSION['adm_user'] == 1) {
				$this->nova_capitacao_medica($id);
			} else {
				echo "N&atilde;o possui Ficha de Avalia&ccedil;&atilde;o Cadastrada.";
			}

			return;
		}

		echo "</table>";
	}

	///////////////////////////FICHA EVOLUCAO/////////////////////////////
	public function nova_evolucao_medica($id) {

		$id = anti_injection($id, "numerico");
		$result = mysql_query("SELECT p.nome FROM clientes as p WHERE p.idClientes = '{$id}'");
		$ev = new FichaEvolucao();
		while ($row = mysql_fetch_array($result)) {
			$ev->pacienteid = $id;
			$ev->pacientenome = ucwords(strtolower($row["nome"]));
		}
		$this->evolucao_medica($ev, "");
	}

/////////////////////////////////////

	public function evolucao_medica($ev, $acesso) {

		echo "<div id='dialog-validar-radio' title='Validar Campos'>";
		echo "<center><h3>Algum item n&atilde;o foi selecionado.</h3></center>";
		echo "</div>";

		echo "<center><h1>Nova Evolu&ccedil;&atilde;o M&eacute;dica </h1></center>";

		$this->cabecalho($ev);

		if ($acesso == "readonly") {

			echo "<center><h2>Respons&aacute;vel: {$ev->usuario} - Data: {$ev->data}</h2></center>";
			echo"<form method=post target='_blank' action='imprimir_ficha.php?id={$ev->capmedica}'>";
			echo"<input type=hidden value={$ev->capmedica} class='imprimir_ficha_mederi'</input>";
			echo "<button id='imprimir_ficha_mederi' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' type='submit' role='button' aria-disabled='false'>
					<span class='ui-button-text'>Imprimir</span></button>";
			echo "</form>";
		}

		$this->plan;


		echo "<div id='div-ficha-evolucao-medica'><br/>";

		echo alerta();
		echo '<div id="saving"
										class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-default"
										style="position: fixed;
										top: 20px;
										right: 40px;
										width: 120px;
										height: 20px;
										display: none;
										background-color: #fff;
										border: 1px solid #000;
										border-radius: 2px;
										-webkit-box-shadow: -3px 1px 10px 0px rgba(50, 50, 50, 0.71);
										-moz-box-shadow:    -3px 1px 10px 0px rgba(50, 50, 50, 0.71);
										box-shadow:         -3px 1px 10px 0px rgba(50, 50, 50, 0.71);
										">';
		echo '	<span style="vertical-align: middle; color: #8B0000;">Salvando...</span>';
		echo '</div>';
		echo "<form rel='persist'>";
		echo "<input type='hidden' name='paciente'  value='{$ev->pacienteid}' />";
		echo "<br/>";
		echo "<table class='mytable' style='width:95%;'>";

		$sql = "select  Max(ev.ID) from fichamedicaevolucao as ev where ev.PACIENTE_ID = {$ev->pacienteid}";
		$result = mysql_query($sql);
		$x = mysql_result($result, 0);

		$sql = "select Max(id) from capmedica where paciente = {$ev->pacienteid}";
		$result = $result = mysql_query($sql);
		$y = mysql_result($result, 0);

		echo "<fieldset style='width:550px;' id='field_avulsa'>";
		echo "<legend><b>Data da evolucao:<b></legend>";
		echo "<p><b>Data: </b><input type='text' class='data OBG' id='data-evolucao' name='data-evolucao' />";
		echo "<b>Hora: </b><input type='text'  class='hora OBG '  id='hora-evolucao' name='hora-evolucao'/></p>";
		echo "</fieldset><br><br>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Problemas Ativos</th>";
		echo "</tr></thead>";

		//Pega os problemas ativos mais recentes
		$sql_data_ultima_capmed = "select Max(data), Max(id) from capmedica where paciente = {$ev->pacienteid}";
		$result = mysql_query($sql_data_ultima_capmed);
		$data_ultima_capmed = mysql_result($result, 0);
		$id_ultima_capmed = mysql_result($result, 0, "Max(id)");

		$sql_ultima_evolucao = "select  Max(ev.DATA) as DATA, Max(ev.ID) as ID from fichamedicaevolucao as ev where ev.PACIENTE_ID = {$ev->pacienteid}";
		$result = mysql_query($sql_ultima_evolucao);
		$data_ultima_evolucao = mysql_result($result, 0, "DATA");
		$id_ultima_evolucao = mysql_result($result, 0, "ID");

		if ($x == '') {

			$sql2 = "SELECT p.DESCRICAO as nome , p.cid FROM problemasativos as p WHERE p.capmed = '{$y}'";
			$result2 = mysql_query($sql2);
			$cont = 1;

			while ($row = mysql_fetch_array($result2)) {

				echo "<tr><td cid={$row['cid']} prob_atv='{$row['nome']}' x='{$cont}' class='probativo1' style='width:20%'><b>P{$cont} - {$row['nome']} </b></td>
		  			<td colspan='2'><input type=radio class='probativo' name='P{$cont}' value =1 >Resolvido ";
				echo"<input type=radio name='P{$cont}' class='probativo' value =2 >Melhor
		  			<input type=radio class='probativo' name='P{$cont}' value =3 >Est&aacute;vel
		  			<input class='probativo' type=radio name='P{$cont}' value =4 >Pior</td></tr>";
				echo"<tr><td colspan='3'><span class='obs_probativo' name='P{$cont}'>
		  			<textarea cols=100 rows=2 class='obs{$cont}'    {$acesso} >{$ev->justificativa}</textarea></span></td></tr>";
				$cont++;
			}
		} else {

			if ($data_ultima_capmed <= $data_ultima_evolucao) {

				$sql2 = "select pae.DESCRICAO as nome, pae.CID_ID,pae.STATUS from problemaativoevolucao as pae
  					where pae.EVOLUCAO_ID = {$id_ultima_evolucao} ORDER BY pae.status DESC";
				$result2 = mysql_query($sql2);
				$cont = 1;

				while ($row = mysql_fetch_array($result2)) {
					if ($row['STATUS'] != 1) {
						echo "<tr><td cid='{$row['CID_ID']}' prob_atv='{$row['nome']}' x='{$cont}' class='probativo1' style='width:20%'>
		  					<b>P{$cont} - {$row['nome']}</b> </td><td colspan='2'><input type=radio class='probativo' name='P{$cont}' value =1 >Resolvido ";
						echo"<input type=radio name='P{$cont}' class='probativo' value =2 >
		  					Melhor <input type=radio class='probativo' name='P{$cont}' value =3 >Est&aacute;vel <input class='probativo' type=radio name='P{$cont}' value =4 >Pior</td></tr>";
						echo"<tr><td colspan='3'><span class='obs_probativo' name='P{$cont}'>
		  					<textarea cols=100 rows=2 class='obs{$cont}'    {$acesso} >{$ev->justificativa}</textarea></span></td></tr>";
						$cont++;
					}
				}
			} else {

				$sql2 = "SELECT p.DESCRICAO as nome , p.cid FROM problemasativos as p WHERE p.capmed = '{$id_ultima_capmed}'";
				$result2 = mysql_query($sql2);
				$cont = 1;

				while ($row = mysql_fetch_array($result2)) {

					echo "<tr><td cid={$row['cid']} prob_atv='{$row['nome']}' x='{$cont}' class='probativo1' style='width:20%'><b>P{$cont} - {$row['nome']} </b></td>
  						<td colspan='2'><input type=radio class='probativo' name='P{$cont}' value =1 >Resolvido ";
					echo"<input type=radio name='P{$cont}' class='probativo' value =2 >Melhor
  						<input type=radio class='probativo' name='P{$cont}' value =3 >Est&aacute;vel
  						<input class='probativo' type=radio name='P{$cont}' value =4 >Pior</td></tr>";
					echo"<tr><td colspan='3'><span class='obs_probativo' name='P{$cont}'>
  						<textarea cols=100 rows=2 class='obs{$cont}'    {$acesso} >{$ev->justificativa}</textarea></span></td></tr>";
					$cont++;
				}
			}
		}

		echo "<input type='hidden' name='capmed'  value='{$y}' />";
		if($mod_home_care == '1'){
			$disabledMod 	= "disabled = 'disabled'";
			$classMod			= "OBG";
		}else{
			$disabledMod 	= "";
			$classMod			= "";
		}

		$this->modalidade_homecare_evolucao($mod_home_care);


		echo "<thead><tr>";
		echo "<th colspan='3' >Intercorrências desde a última visita médica</th>";
		echo "</tr></thead>";

		echo "<tr><td>";

		echo"<select name=intercorrencias class='COMBO_OBG'>
								<option value ='-1'>  </option>
								<option value ='n'>N&atilde;o</option>
								<option value ='s'>Sim</option><span name='intercorrencias' id='intercorrencias_span'>
							</select>
  						</td>
						<td>&nbsp;&nbsp;<span name='intercorrencias' id='intercorrencias_span'>
							<b>Descrição:</b>
							<input type='text' name='intercorrencias' class='intercorrencias' size=50/>
							<button id='add-intercorrencias' tipo=''  > + </button></span>
	  					</td>
  					</tr>";

		echo "<thead id='intercorrencias_tabela'></thead>";
		echo "<tr><thead id='intercorrencias_tabela'></thead></tr>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Novos Problemas </th>";
		echo "</tr></thead>";
		if ($acesso != "readonly") {
			echo "<tr><td colspan='3' ><input type='text' name='busca-problemas-ativos-evolucao' {$acesso} size=100 /></td></tr>";
			echo"<tr><td colspan='3' ><input type='checkbox' id='outros-prob-ativos-evolucao' name='outros-prob-ativos-evolucao' {$acesso} />Outros.<span class='outro_ativo_evolucao' ><input type='text' name='outro-prob-ativo-evolucao' class='outro_ativo_evolucao' {$acesso} size=75/><button id='add-prob-ativo-evolucao' tipo=''  >+ </button></span></td></tr>";
		}
		echo "<tr><td colspan='3' ><table id='problemas_ativos' style='width:100%;'>";
		if (!empty($ev->ativos)) {
			foreach ($ev->ativos as $a) {
				echo "<tr><td>{$a}</td></tr>";
			}
		}
		echo "</table>";

		//Alergia Medicamentosa
		$this->alergia_medicamentosa($ev->pacienteid);

		echo "<thead class='sinais_vitais'><tr>";
		echo "<th colspan='3' >Evolu&ccedil;&atilde;o dos sinais vitais (Registro da semana)</th>";
		echo "</tr></thead>";
		echo "<tbody class='sinais_vitais'>";
		echo "<tr><td><b>Pressão Arterial (sist&oacute;lica): </b></td><td>min:
			<input type='texto' class='numerico1 alertas alertas_sinais_vitais' name='pa_sistolica_min' value='{$ev->pa_sistolica_min}' {$acesso} size='4' /> m&aacute;x:
  			<input type='texto' class='numerico1 alertas alertas_sinais_vitais' name='pa_sistolica_max' value='{$ev->pa_sistolica_max}' {$acesso} size='4' /></td>";
		echo "<td><b>ALERTA:</b>  <input type='radio' class='radio alertas_radio' name='pa_sistolica_sinal' value='1' {$acesso} />Amarelo
  			<input type='radio' class='radio alertas_radio' name='pa_sistolica_sinal' value='2' {$acesso} size='4' />VERMELHO</td>";
		echo "</tr><tr><td></td><td></td><td><textarea cols=30 rows=2 name='pa_sistolica_observacao_alerta' class='observacao_alertas' hidden></textarea>
  									</td>";

		echo "<tr><td><b>Pressão Arterial (diast&oacute;lica): </b> </td><td>
  			min:<input type='texto' class='numerico1 alertas alertas_sinais_vitais' name='pa_diastolica_min' value='{$ev->pa_diastolica_min}' {$acesso} size='4'  />
  			m&aacute;x:<input type='texto' class='numerico1 alertas alertas_sinais_vitais' name='pa_diastolica_max' value='{$ev->pa_diastolica_max}' size='4' {$acesso} /></td>";
		echo "<td><b>ALERTA:</b>
  			<input type='radio' class='radio alertas_radio' name='pa_diastolica_sinal' value='1' {$acesso} />Amarelo 
  			<input type='radio' class='radio alertas_radio' name='pa_diastolica_sinal' value='2' {$acesso} size='4' />VERMELHO</td>";
		echo "</tr><tr><td></td><td></td><td><textarea cols=30 rows=2 name='pa_diastolica_observacao_alerta' class='observacao_alertas' hidden></textarea>
  									</td>";

		echo "<tr><td><b>Frequência Cardíaca: </b> </td><td>
  			min:<input type='texto' class='numerico1 alertas alertas_sinais_vitais' name='fc_min' value='{$ev->fc_min}'  size='4' {$acesso} />
  			m&aacute;x:	<input type='texto' class='numerico1 alertas alertas_sinais_vitais' name='fc_max' value='{$ev->fc_max}' {$acesso} size='4' /></td>";
		echo "<td><b>ALERTA: </b> <input type='radio' class='radio alertas_radio' name='fc_sinal' value='1' {$acesso} />Amarelo
  			<input type='radio' class='radio alertas_radio' name='fc_sinal' value='2' {$acesso} size='4' />VERMELHO</td>";
		echo "</tr><tr><td></td><td></td><td><textarea cols=30 rows=2 name='fc_observacao_alerta' class='observacao_alertas' hidden></textarea>
  									</td>";

		echo "<tr><td><b>Frequência Respiratória: </b> </td><td>
  			min:<input type='texto' class='numerico1 alertas alertas_sinais_vitais' name='fr_min' value='{$ev->fr_min}' size='4' {$acesso} />
  			m&aacute;x:<input type='texto' class='numerico1 alertas alertas_sinais_vitais' name='fr_max' value='{$ev->fr_max}' size='4' {$acesso}  /></td>";
		echo "<td><b>ALERTA:</b>  <input type='radio' class='radio alertas_radio' name='fr_sinal' value='1' {$acesso} />Amarelo
  			<input type='radio' class='radio alertas_radio' name='fr_sinal' value='2' {$acesso} size='4' />VERMELHO</td>";
		echo "</tr><tr><td></td><td></td><td><textarea cols=30 rows=2 name='fr_observacao_alerta' class='observacao_alertas' hidden></textarea>
  									</td>";

		echo "<tr><td width=20% ><b>Temperatura: </b></td><td width=25% >
  			min:<input type='texto' class='numerico1 alertas alertas_sinais_vitais' name='temperatura_min' size='4' value='{$ev->temperatura_min}' {$acesso} size='4' />
  			m&aacute;x:<input type='texto' class='numerico1 alertas alertas_sinais_vitais' name='temperatura_max' size='4'value='{$ev->temperatura_max}' {$acesso} size='4' /></td>";
		echo "<td width=25%  ><b>ALERTA: </b>
  			<input type='radio' class='radio alertas_radio' name='temperatura_sinal' value='1' {$acesso} />Amarelo 
  			<input type='radio' class='radio alertas_radio' name='temperatura_sinal' value='2' {$acesso} size='4' />VERMELHO</td>";
		echo "</tr><tr><td></td><td></td><td><textarea cols=30 rows=2 name='temperatura_observacao_alerta' class='observacao_alertas' hidden></textarea>
  									</td>";

		echo "<tr><td width=20% ><b>Glicemia Capilar: </b></td><td width=25% >
  			min:
  			<input type='texto' class='numerico1 glicemia_capilar' name='glicemia_capilar_min' size='4' value='{$ev->glicemia_capilar_min}' {$acesso} size='4' />
  			m&aacute;x:
  			<input type='texto' class='numerico1 glicemia_capilar' name='glicemia_capilar_max' size='4'value='{$ev->glicemia_capilar_max}' {$acesso} size='4' /></td>";
		echo "</tr>";

		//Oximetria de Pulso

		echo "<tr>
  			<td width=20% ><b>Oximetria de Pulso: </b></td>
  			<td >
  			min:
  			<input type='texto' class='numerico1 alertas oximetria_pulso' name='oximetria_pulso_min' size='4' size='4' />
  			<select name='tipo_oximetria_min' class='COMBO_- tipo_oximetria_adicionais oximetria_pulso'>
	  			<option value=''></option>
	  			<option value='1' >Ar Ambiente</option>
	  			<option value='2' >O2 Suplementar</option>
  			
  			</select>
  			</td>
  			
  			<td width=25%  ><b>ALERTA: </b> <input type='radio' class='radio alertas_radio' name='oximetria_sinal' value='1' {$acesso}/>Amarelo
  			<input type='radio' class='radio alertas_radio' name='oximetria_sinal' value='2' {$acesso} size='4'/>VERMELHO</td>
  			</tr>
  			<tr><td></td><td></td><td><textarea cols=30 rows=2 name='oximetria_pulso_observacao_alerta' hidden></textarea></td>
  			</tr>
  			<tr><td></td><td>
  			<span id='suplementar_min_span' name='suplementar' style='display: inline;'>
  			
  			<input type='texto' size='3' class='numerico1' name='litros_min' />l/min
  			<b>Via
  			<select name='via_oximetria_min' class='COMBO_-' >
  			<option value=''></option>
	  			<option value='1' >Cateter Nasal</option>
	  			<option value='2' >Cateter em Traqueostomia</option>
	  			<option value='3' >Máscara de Venturi</option>
	  			<option value='4' >Ventilação Mecânica</option>
  			</select>
  			</span>
  			</td></tr>";
		echo "<tr><td width=20%></td><td >
  			m&aacute;x:
  			<input type='texto' class='numerico1 alertas oximetria_pulso' name='oximetria_pulso_max' size='4' />
  			<select name='tipo_oximetria_max' class='COMBO_- tipo_oximetria_adicionais oximetria_pulso'>
  			<option value=''></option>
  			<option value='1' >Ar Ambiente</option>
  			<option value='2' >O2 Suplementar</option>
  			
  			</select>
  			</td>
  			</tr>
  			<tr><td></td><td>
  			<span id='suplementar_max_span' name='suplementar' style='display: inline;'>
  			<input type='texto' size='3' class='numerico1' name='litros_max' value='{$row['OXIMETRIA_PULSO_LITROS_MAX']}'/>l/min
  			<b>Via
  			<select name='via_oximetria_max' class='COMBO_-' >
  			<option value=''></option>
  			<option value='1' >Cateter Nasal</option>
  			<option value='2' >Cateter em Traqueostomia</option>
  			<option value='3' >Máscara de Venturi</option>
  			<option value='4' >Ventilação Mecânica</option>
  			</select>
  			</span>
  			</td></tr>
  			";
		echo "</tbody>";

		//Exame Físico
		echo "<thead><th colspan='3'>Exame F&iacute;sico</th>";

		echo "<tr><td><b>Estado Geral:</b>  </td><td colspan='2'>
				<input type='radio' name='estd_geral' class='OBG_RADIO' size=50 readonly='readonly' value='1'  />Bom &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type='radio' name='estd_geral' class='OBG_RADIO' size=50 readonly='readonly' value='2'  />Regular &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type='radio' name='estd_geral' class='OBG_RADIO' size=50 readonly='readonly' value='3' />Ruim
				</td></tr>";

		echo "<tr><td><b>Mucosas:</b>  </td><td colspan='2'>
				<input type='radio' name='mucosa' class='mucosa' size=50 readonly='readonly' value='s' />Coradas &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type='radio' name='mucosa' size=50 class='mucosa' readonly='readonly' value='n' " . (($dados->mucosa == 'n') ? "checked" : "") . " {$enabled} />Descoradas
  					<span class='mucosa1'><select name='mucosaobs' class='COMBO_-' >
  					<option value=''></option><option value='+' " . (($ev->mucosa_obs == '+') ? "selected" : "") . " >+</option><option value='++' " . (($ev->mucosa_obs == '++') ? "selected" : "") . " >++</option><option value='+++' " . (($ev->mucosa_obs == '+++') ? "selected" : "") . ">+++</option><option value='++++'" . (($ev->mucosa_obs == '++++') ? "selected" : "") . ">++++</option></select></span></td></tr>";
		echo "<tr><td><b>Escleras:</b>  </td><td colspan='2'><input type='radio' name='escleras' class='escleras' size=50 readonly='readonly' value='s' " . (($ev->escleras == 's') ? "checked" : "") . " {$enabled} />Anict&eacute;ricas &nbsp;&nbsp;&nbsp;&nbsp; <input type='radio' name='escleras' class='escleras' size=50 readonly='readonly' value='n' " . (($dados->escleras == 'n') ? "checked" : "") . " {$enabled} />Ict&eacute;ricas
		<span class='escleras1'><select name='esclerasobs' class='COMBO_-' >
  			<option value=''></option><option value='+' " . (($ev->escleras_obs == '+') ? "selected" : "") . " >+</option><option value='++' " . (($ev->escleras_obs == '++') ? "selected" : "") . " >++</option><option value='+++' " . (($ev->escleras_obs == '+++') ? "selected" : "") . ">+++</option><option value='++++'" . (($ev->escleras_obs == '++++') ? "selected" : "") . ">++++</option></select></span></td></tr>";
		echo "<tr><td><b>Padr&atilde;o Respirat&oacute;rio:</b>  </td><td colspan='2'><input type='radio' name='respiratorio' class='respiratorio' size=50 readonly='readonly' value='s' " . (($ev->respiratorio == 's') ? "checked" : "") . " {$enabled} />Eupn&eacute;ico &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='respiratorio' class='respiratorio' size=50 readonly='readonly' value='n' " . (($dados->respiratorio == 'n') ? "checked" : "") . " {$enabled} />Dispn&eacute;ico
  					<span class='respiratorio1'></span>
  					<input type='radio' name='respiratorio' class='respiratorio' size=50 readonly='readonly' value='1' " . (($ev->respiratorio == '1') ? "checked" : "") . " {$enabled} />Outro<br/><span class='outro_respiratorio'><textarea cols=30 rows=2 name='outro_respiratorio' ></textarea></span></td></tr>";
		echo "<tr id='frequencia_respiratoria' style='display: none;'><td width=20%><b>Frequência Respiratória:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td width=25% colspan='3'><input type='texto' cols=100 rows=2 size='6' class='numerico1' name='fr' value='{$ev->fr}' {$acesso} />irpm</td></tr>";
		//Oximetria de Pulso
		echo "<tr><td width=20% ><b>Oximetria de Pulso: </b></td>
		<td width=25% colspan='3'>
		<input type='texto' class='numerico1' name='oximetria_pulso' size='4' value='{$ev->oximetria_pulso}' {$acesso} size='4' />
		<select name='tipo_oximetria' class='COMBO_-'>
		<option value=''></option>
		<option value='1'>Ar Ambiente</option>
		<option value='2'>O2 Suplementar</option>
		</select>
		<span id='suplementar_span' name='suplementar' style='display: inline;'>
		<input type='texto' size='3' class='numerico1' name='litros'/>l/min
		<b>Via
		<select name='via_oximetria' class='COMBO_-' >
			<option value=''></option>
			<option value='1'>Cateter Nasal</option>
			<option value='2'>Cateter em Traqueostomia</option>
			<option value='3'>Máscara de Venturi</option>
			<option value='4'>Ventilação Mecânica</option>
		</select>
		</span>
		</td></tr>";

		echo "<tr><td width=20%><b>Pressão Arterial(sist&oacute;lica):</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td width=25% colspan='3'><input type='texto' class='numerico1 OBG' size='6' name='pa_sistolica' value='{$ev->pa_sistolica}' {$acesso} />mmhg</td></tr>";
		echo "<tr><td width=20%><b>Pressão Arterial(diast&oacute;lica):</b>&nbsp;&nbsp;&nbsp;</td><td width=25% colspan='3'><input type='texto' class='numerico1 OBG' size='6' name='pa_diastolica' value='{$ev->pa_diastolica}' {$acesso} />mmhg</td></tr>";
		echo "<tr><td width=20%><b>Frequência Cardíaca: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td width=25% colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='fc' value='{$ev->fc}' {$acesso} />bpm</td></tr>";
		echo "<tr><td width=20%><b>Temperatura:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td width=25% colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='temperatura' value='{$ev->temperatura}' {$acesso} /> &deg;C</td></tr>";
		echo "<tr><td width=20%><b>Peso:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td width=25% colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='peso_paciente' value='{$ev->peso_paciente}' {$acesso} /> KG</td></tr>";

		echo"<tr><td colspan='3' ><b>DOR: </b><select name=dor_sn class='COMBO_OBG'><option value ='-1'>  </option>
  					<option value ='n'>N&atilde;o</option>
  					<option value ='s'>Sim</option></select>&nbsp;&nbsp;<span name='dor' id='dor_span'>
  					<b>Local:</b><input type='text' name='dor' class='dor' {$acesso} size=50/><button id='add-dor' tipo=''  > + </button></span></td></tr>";

		echo "<tr><td colspan='3' ><table id='dor' style='width:100%;'>";
		if (!empty($ev->dor)) {
			foreach ($ev->dor as $a) {
				echo "<tr><td>{$a}</td></tr>";
			}
		}
		echo "</table></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3' >CATETERES E SONDAS</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='iot'/>TRAQUEOSTOMIA</td></tr>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='sne'/>SONDA NASOENTERAL</td></tr>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='gt'/>GASTROSTOMIA</td></tr>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='svd'/>SONDA VESICAL DE DEMORA</td></tr>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='sva'/>SONDA VESICAL DE AL&Iacute;VIO</td></tr>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='Acesso Venoso' central'/>ACESSO VENOSO CENTRAL</td></tr>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='Acesso Perif�rico'/>ACESSO VENOSO PERIF&Eacute;RICO</td></tr>";
		echo"<tr><td colspan='3' ><b>OUTRO: </b><input type='text' name='cateter' class='cateter' {$acesso} size=75/><button id='add-cateter' tipo=''  > + </button></td></tr>";

		echo "<tr><td colspan='3' ><table id='cateter' style='width:100%;'>";
		if (!empty($ev->cateter)) {
			foreach ($ev->cateter as $a) {
				echo "<tr><td>{$a}</td></tr>";
			}
		}
		echo "</table></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3' >Exame segmentar</th>";
		echo "</tr></thead>";
		$sql = "select * from examesegmentar";
		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {

			echo "<tr><td idexame={$row['ID']} class='exame1'> <b>{$row['DESCRICAO']}</b></td><td colspan='2'><input type=radio class='exame' name='{$row['ID']}' value ='1' >Normal ";
			echo"<input type=radio class='exame' name='{$row['ID']}'  value ='2' >Alterado <input type=radio class='exame' name='{$row['ID']}' value='3'>N&atilde;o Examinado</td></tr>";
			echo"<tr><td colspan='3'>
  			<span class=obs_exame name='{$row['ID']}'>
  			<textarea cols=100 rows=2 name='obsexame' id='obsexame{$row['ID']}'></textarea></span></td></tr>";
		}
		echo "<thead class='novo-exame'><tr>";
		echo "<th colspan='3' >Exames Complementares Novos</th>";
		echo "</tr></thead>";
		echo "<tr class='novo-exame'><td colspan='3' ><textarea cols=100 rows=5 name='novo_exame' value={$ev->novo_exame} {$acesso} ></textarea></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3' >Impress&atilde;o Geral</th>";
		echo "</tr></thead>";
		echo "<thead><tr>";
		echo "<tr><td colspan='3' ><textarea cols=100 rows=5 name='impressao'   class='OBG' value={$ev->impressao} {$acesso} ></textarea></td></tr>";
		echo "<th colspan='3' >Plano Diagn&oacute;stico</th>";
		echo "</tr></thead>";
		echo "<thead><tr>";
		echo "<tr><td colspan='3' ><textarea cols=100 rows=5 name='plano_diagnostico'   class='OBG' value={$ev->plano_diagnostico} {$acesso} ></textarea></td></tr>";
		echo "<th colspan='3' >Plano Teraup&ecirc;utico</th>";
		echo "</tr></thead>";
		echo "<thead><tr>";
		echo "<tr><td colspan='3' >
  				<b>Conduta:</b>
  				<select name='conduta' class='COMBO_OBG' >
					<option value='-1'></option>
					<option value='1'>Mantida</option>
					<option value='2'>Modificada</option>
				</select>
  				<br><textarea cols=100 rows=5 name='terapeutico' id='terapeutico' hidden value={$ev->plano_terapeutico} {$acesso} ></textarea>
  			</td></tr>";

		echo $this->createDialogLocalStorageWarning();
		echo $this->createDialogSinaisSemanaWarning();
		echo $this->createDialogExamesComplementaresWarning();

		echo "</table></form></div>";

		if ($acesso != "readonly") {
			echo "<button id='salvar-ficha-evolucao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
			echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
		}
	}

	public function modalidade_homecare($mod_home_care,$idPlano,$enabled) {
		echo "<input type='hidden' class='modalidade' name='modalidade' value=0 >";
        if($idPlano == 9){
			echo "
   <thead class='modalidade_homecare'><tr><th colspan='12'>Modalidade de Home Care</th></tr></thead>
   <tbody class='modalidade_homecare'>
   <tr><td colspan='12'><input type='radio' class='modalidade' $enabled name='modalidade' value='1' " . (($mod_home_care == '1') ? "checked" : "") . " {$enabled}>Assistencia Domiciliar</input></td></tr>
   <tr><td colspan='12'><input type='radio' class='modalidade' name='modalidade' value='6' " . (($mod_home_care == '6') ? "checked" : "") . " {$enabled}>DIÁRIA COM BAIXA COMPLEXIDADE</input></td></tr>
   <tr><td colspan='12'><input type='radio' class='modalidade' name='modalidade' value='7' " . (($mod_home_care == '7') ? "checked" : "") . " {$enabled}>DIÁRIA COM MEDIA COMPLEXIDADE</input></td></tr>
   <tr><td colspan='12'><input type='radio' class='modalidade' name='modalidade' value='8' " . (($mod_home_care == '8') ? "checked" : "") . " {$enabled}>DIÁRIA COM ALTA COMPLEXIDADE</input></td></tr>
   <tr><td colspan='12'><input type='radio' class='modalidade' name='modalidade' value='9' " . (($mod_home_care == '9') ? "checked" : "") . " {$enabled}>DIÁRIA COM VENTILAÇÃO MECÂNICA</input></td></tr>
	</tbody>";
		}else{
			echo "
<thead class='modalidade_homecare'><tr><th colspan='12'>Modalidade de Home Care</th></tr></thead>
<tbody class='modalidade_homecare'>
   <tr><td colspan='12'><input type='radio' class='modalidade' name='modalidade' value='1' " . (($mod_home_care == '1') ? "checked" : "") . " {$enabled}>Assistencia Domiciliar / Serviços Pontuais</input></td></tr>
   <tr><td colspan='12'><input type='radio' class='modalidade' name='modalidade' value='2' " . (($mod_home_care == '2') ? "checked" : "") . " {$enabled}>Interna&ccedil;&atilde;o Domiciliar 6h</input></td></tr>
   <tr><td colspan='12'><input type='radio' class='modalidade' name='modalidade' value='3' " . (($mod_home_care == '3') ? "checked" : "") . " {$enabled}>Interna&ccedil;&atilde;o Domiciliar 12h</input></td></tr>
   <tr><td colspan='12'><input type='radio' class='modalidade' name='modalidade' value='4' " . (($mod_home_care == '4') ? "checked" : "") . " {$enabled}>Interna&ccedil;&atilde;o Domiciliar 24h com respirador</input></td></tr>
   <tr><td colspan='12'><input type='radio' class='modalidade' name='modalidade' value='5' " . (($mod_home_care == '5') ? "checked" : "") . " {$enabled}>Interna&ccedil;&atilde;o Domiciliar 24h sem respirador</input></td></tr>
</tbody>";

		}


	}

	public function modalidade_homecare_evolucao($mod_home_care) {


		echo "<thead><tr><th colspan='3'>Modalidade de Home Care</th></tr></thead>
   <tr><td colspan='3'><input type='radio' id='ad' class='modalidade-evolucao OBG' name='modalidade' value='1' " . (($mod_home_care == '1') ? "checked" : "") . " {$enabled}>Assistencia Domiciliar</input></td></tr>
   <tr><td colspan='3'><input type='radio' id='id6h' class='modalidade-evolucao OBG' name='modalidade' value='2' " . (($mod_home_care == '2') ? "checked" : "") . " {$enabled}>Interna&ccedil;&atilde;o Domiciliar 6h</input></td></tr>
   <tr><td colspan='3'><input type='radio' id='id12h' class='modalidade-evolucao OBG' name='modalidade' value='3' " . (($mod_home_care == '3') ? "checked" : "") . " {$enabled}>Interna&ccedil;&atilde;o Domiciliar 12h</input></td></tr>
   <tr><td colspan='3'><input type='radio' id='id24hcr' class='modalidade-evolucao OBG' name='modalidade' value='4' " . (($mod_home_care == '4') ? "checked" : "") . " {$enabled}>Interna&ccedil;&atilde;o Domiciliar 24h com respirador</input></td></tr>
   <tr><td colspan='3'><input type='radio' id='id24hsr' class='modalidade-evolucao OBG' name='modalidade' value='5' " . (($mod_home_care == '5') ? "checked" : "") . " {$enabled}>Interna&ccedil;&atilde;o Domiciliar 24h sem respirador</input></td></tr>
";
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Nova com base na anterior
	//Ana Paula
	public function anterior_evolucao_medica($id, $mode = "nova") {

		echo "<div id='dialog-validar-radio' title='Validar Campos'>";
		echo "<center><h3>Algum item n&atilde;o foi selecionado.</h3></center>";
		echo "</div>";

		echo "<center><h1>Nova Evolu&ccedil;&atilde;o M&eacute;dica </h1></center>";

		//Cabeçalho
		$id = anti_injection($id, "numerico");
		$sql = "SELECT
        DATE_FORMAT(fev.DATA,'%d/%m/%Y') as fdata,
        DATE_FORMAT(fev.DATA,'%H:%i') as fhora,
        fev.PACIENTE_ID as idpaciente,
        u.nome,	p.nome as paciente,
        fev.*,
        e.nome AS empresa,
        pds.nome AS plano
        FROM
        fichamedicaevolucao as fev LEFT JOIN
        usuarios as u ON (fev.USUARIO_ID = u.idUsuarios) INNER JOIN
        clientes as p ON (fev.PACIENTE_ID = p.idClientes)
        LEFT JOIN empresas AS e ON fev.empresa = e.id
        LEFT JOIN planosdesaude AS pds ON fev.plano = pds.id
        WHERE fev.ID = '{$id}'";

		$r = mysql_query($sql);

		while ($row = mysql_fetch_array($r)) {
			$usuario = ucwords(strtolower($row["nome"]));
			$data = $row['fdata'];
			$paciente = $row['idpaciente'];
			$empresa = $row['empresa'];
			$convenio = $row['plano'];
			$mod_home_care = $row['MOD_HOME_CARE'];
			$pa_sistolica_min = $row['PA_SISTOLICA_MIN'];
			$pa_sistolica_max = $row['PA_SISTOLICA_MAX'];
			$pa_sistolica_sinal = $row['PA_SISTOLICA_SINAL'];
			$pa_diastolica_min = $row['PA_DIASTOLICA_MIN'];
			$pa_diastolica_max = $row['PA_DIASTOLICA_MAX'];
			$pa_diastolica_sinal = $row['PA_DIASTOLICA_SINAL'];
			$fc_min = $row['FC_MIN'];
			$fc_max = $row['FC_MAX'];
			$fc_sinal = $row['FC_SINAL'];
			$fr_min = $row['FR_MIN'];
			$fr_max = $row['FR_MAX'];
			$fr_sinal = $row['FR_SINAL'];
			$temperatura_min = $row['TEMPERATURA_MIN'];
			$temperatura_max = $row['TEMPERATURA_MAX'];
			$temperatura_sinal = $row['TEMPERATURA_SINAL'];
			$oximetria_min = $row['OXIMETRIA_PULSO_MIN'];
			$oximetria_tipo_min = $row['OXIMETRIA_PULSO_TIPO_MIN'];
			$oximetria_via_min = $row['OXIMETRIA_PULSO_VIA_MIN'];
			$oximetria_max = $row['OXIMETRIA_PULSO_MAX'];
			$oximetria_tipo_max = $row['OXIMETRIA_PULSO_TIPO_MAX'];
			$oximetria_via_max = $row['OXIMETRIA_PULSO_VIA_MAX'];
			$peso_paciente = $row['PESO_PACIENTE'];
			$glicemia_min = $row['GLICEMI_CAPILAR_MIN'];
			$glicemia_max = $row['GLICEMIA_CAPILAR_MAX'];
			$oximetria_pulso = $row['OXIMETRIA_PULSO'];
			$data = $row['fdata'];
			$hora = $row['fhora'];
		}

		$this->cabecalho($paciente, $empresa, $convenio);

		echo "<center><h2>Respons&aacute;vel: {$usuario} - Data: {$data}</h2></center>";
		//Fim do Cabeçalho

		$this->plan;

		echo "<div id='div-ficha-evolucao-medica'><br/>";

		echo alerta();
		echo '<div id="saving"
										class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-default"
										style="position: fixed;
										top: 20px;
										right: 40px;
										width: 120px;
										height: 20px;
										display: none;
										background-color: #fff;
										border: 1px solid #000;
										border-radius: 2px;
										-webkit-box-shadow: -3px 1px 10px 0px rgba(50, 50, 50, 0.71);
										-moz-box-shadow:    -3px 1px 10px 0px rgba(50, 50, 50, 0.71);
										box-shadow:         -3px 1px 10px 0px rgba(50, 50, 50, 0.71);
										">';
		echo '	<span style="vertical-align: middle; color: #8B0000;">Salvando...</span>';
		echo '</div>';
		echo "<form rel='persist'>";
		echo "<input type='hidden' name='paciente'  value='{$paciente}' />";
		echo "<input type='hidden' name='idevolucao'  value='{$id}' />";
		echo "<br/>";

		echo "<table class='mytable' style='width:95%;'>";

		$sql = "select  Max(ev.ID) from fichamedicaevolucao as ev where ev.PACIENTE_ID = {$paciente}";
		$result = mysql_query($sql);
		$x = mysql_result($result, 0);

		$sql = "select Max(id) from capmedica where paciente = {$paciente}";
		$result = mysql_query($sql);
		$y = mysql_result($result, 0);


		echo "<fieldset style='width:550px;' id='field_avulsa'>";
		echo "<legend><b>Data da evolucao:<b></legend>";
		echo "<p><b>Data: </b><input type='text' class='data OBG' value='".($mode != 'nova' ? $data : '')."' id='data-evolucao' name='data-evolucao' />";
		echo "<b>Hora: </b><input type='text'  class='hora OBG ' value='".($mode != 'nova' ? $hora : '')."' id='hora-evolucao' name='hora-evolucao'/></p>";
		echo "</fieldset><br><br>";

		//Problemas Ativos
		echo "<thead><tr>";
		echo "<th colspan='3' >Problemas Ativos</th>";
		echo "</tr></thead>";

		if($mode == 'nova') {
			if ($x == '') {

				$sql2 = "SELECT p.DESCRICAO as nome , p.cid FROM problemasativos as p WHERE p.capmed = '{$y}' ORDER BY p.status DESC";
				$result2 = mysql_query ($sql2);
				$cont = 1;

				while ($row = mysql_fetch_array ($result2)) {

					echo "<tr><td cid={$row['cid']} prob_atv='{$row['nome']}' x='{$cont}' class='probativo1' style='width:20%'><b>P{$cont} - {$row['nome']} </b></td>
									<td colspan='2'><input type=radio class='probativo' name='P{$cont}' value =1 >Resolvido ";
					echo "<input type=radio name='P{$cont}' class='probativo' value =2 >Melhor
											<input type=radio class='probativo' name='P{$cont}' value =3 >Est&aacute;vel
											<input class='probativo' type=radio name='P{$cont}' value =4 >Pior</td></tr>";
					echo "<tr><td colspan='3'><span class='obs_probativo' name='P{$cont}'>
											<textarea cols=100 rows=2 class='obs{$cont}'    {$acesso} >{$ev->justificativa}</textarea></span></td></tr>";
					$cont++;
				}
			} else {

				if ($ultima_capmed <= $ultima_capmed_evolucao) {

					$sql2 = "select pae.DESCRICAO as nome, pae.CID_ID,pae.STATUS from problemaativoevolucao as pae
											where pae.EVOLUCAO_ID = {$x} ORDER BY pae.status DESC";
					$result2 = mysql_query ($sql2);
					$cont = 1;

					while ($row = mysql_fetch_array ($result2)) {

						if ($row['STATUS'] != 1) {
							echo "<tr><td cid='{$row['CID_ID']}' prob_atv='{$row['nome']}' x='{$cont}' class='probativo1' style='width:20%'>
											<b>P{$cont} - {$row['nome']}</b></td><td colspan='2'>

											<input type=radio class='probativo' name='P{$cont}' value =1 ";
							if ($row['STATUS'] == 1) {
								echo ' checked';
							}
							echo ">	Resolvido ";

							echo "<input type=radio class='probativo' name='P{$cont}' value =2";
							if ($row['STATUS'] == 2) {
								echo ' checked';
							}
							echo ">Melhor";

							echo "<input type=radio class='probativo' name='P{$cont}' value =3";
							if ($row['STATUS'] == 3) {
								echo ' checked';
							}
							echo ">Est&aacute;vel";

							echo "<input type=radio class='probativo' name='P{$cont}' value =4";
							if ($row['STATUS'] == 4) {
								echo ' checked';
							}
							echo ">Pior</td></tr>";

							echo "<tr><td colspan='3'><span class='obs_probativo' name='P{$cont}'>
											<textarea cols=100 rows=2 class='obs{$cont}'>{$row['OBSERVACAO']}</textarea></span></td></tr>";
							$cont++;
						}
					}
				} else {

					$sql2 = "SELECT p.DESCRICAO as nome , p.cid FROM problemasativos as p WHERE p.capmed = '{$ultima_capmed}'
							ORDER BY p.status DESC";
					$result2 = mysql_query ($sql2);
					$cont = 1;

					while ($row = mysql_fetch_array ($result2)) {

						echo "<tr><td cid={$row['cid']} prob_atv='{$row['nome']}' x='{$cont}' class='probativo1' style='width:20%'><b>P{$cont} - {$row['nome']} </b></td>
									<td colspan='2'><input type=radio class='probativo' name='P{$cont}' value =1 >Resolvido ";
						echo "<input type=radio name='P{$cont}' class='probativo' value =2 >Melhor
											<input type=radio class='probativo' name='P{$cont}' value =3 >Est&aacute;vel
											<input class='probativo' type=radio name='P{$cont}' value =4 >Pior</td></tr>";
						echo "<tr><td colspan='3'><span class='obs_probativo' name='P{$cont}'>
											<textarea cols=100 rows=2 class='obs{$cont}'    {$acesso} >{$ev->justificativa}</textarea></span></td></tr>";
						$cont++;
					}
				}
			}
		}

		echo "<input type='hidden' name='capmed'  value='{$y}' />";

		$this->modalidade_homecare_evolucao($mode == "nova" ? "" : $mod_home_care);

		//Intercorrências
		$sql_intercorrencias = "SELECT * FROM `intercorrenciasfichaevolucao` WHERE `FICHAMEDICAEVOLUCAO_ID` = {$id}";
		$resultado_intercorrencias = mysql_query($sql_intercorrencias);
		$cont_intercorrencias = 1;

		$num_linhas_intercorrencias = mysql_num_rows($resultado_intercorrencias);

		echo "<thead><tr>";
		echo "<th colspan='3' >Intercorrências desde a última visita médica</th>";
		echo "</tr></thead>";

		echo "<tr>
        <td>";

		echo"<select name=intercorrencias class='COMBO_OBG'>
            <option value ='-1'>  </option>
            <option value ='n' " . (($num_linhas_intercorrencias <= 0) ? "selected" : "") . ">N&atilde;o</option>
            <option value ='s' " . (($num_linhas_intercorrencias > 0) ? "selected" : "") . ">Sim</option>
        </select></td>
        <td colspan='2'>
         <span name='intercorrencias' id='intercorrencias_span'>";
		echo "<b>Descrição:</b>
            <input type='text' name='intercorrencias' class='intercorrencias' size=50/>
            <button id='add-intercorrencias' tipo=''  > + </button></span></td></tr>";

		while ($row = mysql_fetch_array($resultado_intercorrencias)) {
			echo "<tr><td id='inter' class='interc' name='int{$cont_intercorrencias}'><img src='../utils/delete_16x16.png' class='del-intercorrencias-ativas' title='Excluir' border='0'><b>Descrição: </b>{$row['DESCRICAO']}</td></tr>";

			$cont_intercorrencias++;
		}
		echo "<tr><thead id='intercorrencias_tabela'></thead></tr>";
		echo "</table>";

		echo "<table class='mytable' style='width:95%;'>";
		echo "<thead><tr>";
		echo "<th colspan='3' >Novos Problemas </th>";
		echo "</tr></thead>";

		echo "<tr><td colspan='3' ><b>Busca  </b><input type='text' name='busca-problemas-ativos-evolucao' {$acesso} size=100 /></td></tr>";
		echo"<tr><td colspan='3' ><input type='checkbox' id='outros-prob-ativos-evolucao' name='outros-prob-ativos-evolucao' {$acesso} />
            Outros.<span class='outro_ativo_evolucao' ><input type='text' name='outro-prob-ativo-evolucao' class='outro_ativo_evolucao' {$acesso} size=75/>
            <button id='add-prob-ativo-evolucao' tipo=''  >+ </button></span></td></tr>";
		echo "<tr><td colspan='3' >";


		echo "<table id='problemas_ativos' style='width:95%;'>";
		if($mode != 'nova') {
			$sql2 = "SELECT p.DESCRICAO as nome, CID_ID FROM problemaativoevolucao as p WHERE EVOLUCAO_ID = '{$id}'
			  			 ORDER BY p.status DESC";
			$result2 = mysql_query ($sql2);

			while($arr_probativo = mysql_fetch_array($result2)){
				echo "<tr>
								<td colspan='2' class='prob-ativos-evolucao' cod='{$arr_probativo['CID_ID']}' desc='{$arr_probativo['nome']}'>
									<img src='../utils/delete_16x16.png' class='del-prob-ativos' title='Excluir' border='0' >
									{$arr_probativo['nome']}
								</td>
							</tr>";
			}
		}


		echo "</table>";
		echo "</td></tr>";
		echo "</table>";

		echo "<table width='95%' class='mytable'>";

		$this->alergia_medicamentosa($paciente);

		echo "<table width='95%' class='mytable' id='evolucao_sinais'>";

			echo "<thead class='sinais_vitais'><tr>";

			//Evolução dos Sinais Vitais

			echo "<th colspan='3' >Evolu&ccedil;&atilde;o dos sinais vitais (Registro da semana)</th>";
			echo "</tr></thead>";

			echo "<tbody class='sinais_vitais'>";
			echo "<tr><td><b>Pressão Arterial (sist&oacute;lica): </b></td><td>min:
            <input type='texto' {$disabledMod} class='numerico1 {$classMod} alertas alertas_sinais_vitais' value='".($mode != 'nova' ? $pa_sistolica_min : '')."' name='pa_sistolica_min' {$acesso} size='4' /> m&aacute;x:
            <input type='texto' {$disabledMod} class='numerico1 {$classMod} alertas alertas_sinais_vitais' value='".($mode != 'nova' ? $pa_sistolica_max : '')."' name='pa_sistolica_max'  {$acesso} size='4' /></td>";

			echo "<td><b>ALERTA:</b>  <input type='radio' {$disabledMod} class='radio alertas_radio' ".($pa_sistolica_sinal == '1' && $mode != 'nova' ? "checked" : "")." name='pa_sistolica_sinal' value='1' />Amarelo";
			echo "<input type='radio' {$disabledMod} class='radio alertas_radio' name='pa_sistolica_sinal' ".($pa_sistolica_sinal == '2' && $mode != 'nova' ? "checked" : "")." value='2' />VERMELHO</td>";
			echo "</tr><tr><td></td><td></td><td><textarea {$disabledMod} cols=30 rows=2 name='pa_sistolica_observacao_alerta' class='observacao_alertas' hidden></textarea>
        </td>";

			echo "</tr><tr><td><b>Pressão Arterial (diast&oacute;lica): </b> </td><td> min:<input type='texto' {$disabledMod} value='".($mode != 'nova' ? $pa_diastolica_min : '')."' class='numerico1 {$classMod} alertas alertas_sinais_vitais' name='pa_diastolica_min' {$acesso} size='4'  /> m&aacute;x:
        <input type='texto' {$disabledMod} class='numerico1 {$classMod} alertas alertas_sinais_vitais' value='".($mode != 'nova' ? $pa_diastolica_max : '')."' name='pa_diastolica_max' size='4' {$acesso} /></td>";

			echo "<td><b>ALERTA:</b>  <input type='radio' {$disabledMod} class='radio alertas_radio' ".($pa_diastolica_sinal == '1' && $mode != 'nova' ? "checked" : "")."  name='pa_diastolica_sinal' value='1' {$acesso}/>Amarelo";
			echo "<input type='radio' {$disabledMod} class='radio alertas_radio' name='pa_diastolica_sinal' ".($pa_diastolica_sinal == '2' && $mode != 'nova' ? "checked" : "")." value='2' {$acesso} size='4'/>VERMELHO</td>";
			echo "</tr><tr><td></td><td></td><td><textarea cols=30 rows=2 {$disabledMod} class='observacao_alertas' name='pa_diastolica_observacao_alerta' hidden></textarea>
    </td>";

			echo "</tr><tr><td><b>Frequência Cardíaca: </b> </td><td>min:<input type='texto' {$disabledMod} value='".($mode != 'nova' ? $fc_min : '')."' class='numerico1 {$classMod} alertas alertas_sinais_vitais' name='fc_min'  size='4' {$acesso} /> m&aacute;x:
    <input type='texto' {$disabledMod} class='numerico1 {$classMod} alertas alertas_sinais_vitais' value='".($mode != 'nova' ? $fc_max : '')."' name='fc_max' size='4' /></td>";

			echo "<td><b>ALERTA: </b> <input type='radio' {$disabledMod} class='radio alertas_radio' ".($fc_sinal == '1' ? "checked" : "")."  name='fc_sinal' value='1' {$acesso}/>Amarelo";
			echo "<input type='radio' {$disabledMod} class='radio alertas_radio' name='fc_sinal' ".($fc_sinal == '2' ? "checked" : "")." value='2' {$acesso} size='4'/>VERMELHO</td>";
			echo "</tr><tr><td></td><td></td><td><textarea cols=30 rows=2 {$disabledMod} class='observacao_alertas' name='fc_observacao_alerta' hidden></textarea>
</td>";

			echo "</tr><tr><td><b>Frequência Respiratória: </b> </td><td>min:<input type='texto' value='".($mode != 'nova' ? $fr_min : '')."' {$disabledMod} class='numerico1 {$classMod} alertas alertas_sinais_vitais' name='fr_min' size='4' {$acesso} /> m&aacute;x:
<input type='texto' {$disabledMod} class='numerico1 {$classMod} alertas alertas_sinais_vitais' value='".($mode != 'nova' ? $fr_max : '')."' name='fr_max' size='4' {$acesso}  /></td>";

			echo "<td><b>ALERTA:</b>  <input type='radio' {$disabledMod} class='radio alertas_radio' ".($fr_sinal == '1' && $mode != 'nova' ? "checked" : "")."  name='fr_sinal' value='1' {$acesso}/>Amarelo";
			echo "<input type='radio' {$disabledMod} class='radio alertas_radio' name='fr_sinal' ".($fr_sinal == '2' && $mode != 'nova' ? "checked" : "")." value='2' {$acesso} size='4'/>VERMELHO</td>";
			echo "</tr><tr><td></td><td></td><td><textarea cols=30 rows=2 {$disabledMod} class='observacao_alertas' name='fr_observacao_alerta' hidden></textarea>
</td>";

			echo "</tr><tr><td width=20% ><b>Temperatura: </b></td><td width=25% >min:<input type='texto' value='".($mode != 'nova' ? $temperatura_min : '')."' {$disabledMod} class='numerico1 {$classMod} alertas alertas_sinais_vitais' name='temperatura_min' size='4' size='4' /> m&aacute;x:
<input type='texto' {$disabledMod} class='numerico1 {$classMod} alertas alertas_sinais_vitais' value='".($mode != 'nova' ? $temperatura_max : '')."' name='temperatura_max' size='4' size='4' /></td>";

			echo "<td width=25%  ><b>ALERTA: </b> <input type='radio' {$disabledMod} class='radio alertas_radio' ".($temperatura_sinal == '1' && $mode != 'nova' ? "checked" : "")."  name='temperatura_sinal' value='1' {$acesso}/>Amarelo";
			echo "<input type='radio' {$disabledMod} class='radio alertas_radio' name='temperatura_sinal' ".($temperatura_sinal == '2' && $mode != 'nova' ? "checked" : "")." value='2' {$acesso} size='4'/>VERMELHO</td>";
			echo "</tr><tr><td></td><td></td><td><textarea cols=30 rows=2 {$disabledMod} class='observacao_alertas' name='temperatura_observacao_alerta' hidden></textarea>
</td>";

			echo "</tr>";

			echo "</tr><tr><td width=20% ><b>Glicemia Capilar: </b></td><td width=25% >min:
<input type='texto' {$disabledMod} class='numerico1 glicemia_capilar' name='glicemia_capilar_min' value='".($mode != 'nova' ? $glicemia_min : '')."' size='4' size='4' /> m&aacute;x:
<input type='texto' {$disabledMod} class='numerico1 glicemia_capilar' name='glicemia_capilar_max' value='".($mode != 'nova' ? $glicemia_max : '')."' size='4' size='4' /></td>";
			//Oximetria de Pulso

			echo "<tr><td width=20% ><b>Oximetria de Pulso: </b></td>
<td >
    min:
    <input type='texto' {$disabledMod} class='numerico1 alertas oximetria_pulso' name='oximetria_pulso_min' value='".($mode != 'nova' ? $oximetria_min : '')."' size='4' size='4' />
    <select name='tipo_oximetria_min' {$disabledMod} class='COMBO_- tipo_oximetria_adicionais oximetria_pulso'>
        <option value=''></option>
        <option value='1' ".($oximetria_tipo_min == '1' && $mode != 'nova' ? "selected" : "")." >Ar Ambiente</option>
        <option value='2' ".($oximetria_tipo_min == '2' && $mode != 'nova' ? "selected" : "")." >O2 Suplementar</option>
        
    </select>
</td>
<td width=25%  ><b>ALERTA: </b> <input type='radio' {$disabledMod} class='radio alertas_radio' ".($oximetria_sinal == '1' && $mode != 'nova' ? "checked" : "")." name='oximetria_sinal' value='1' {$acesso}/>Amarelo
 <input type='radio' {$disabledMod} class='radio alertas_radio' ".($oximetria_sinal == '2' && $mode != 'nova' ? "checked" : "")." name='oximetria_sinal' value='2' {$acesso} size='4'/>VERMELHO</td>
</tr><tr><td></td><td></td><td><textarea cols=30 rows=2 {$disabledMod} class='observacao_alertas' name='oximetria_pulso_observacao_alerta' hidden></textarea>
</td>
</tr>
<tr><td></td><td>
  <span id='suplementar_min_span' name='suplementar' style='display: inline;'>
      
    <input type='texto' size='3' class='numerico1' name='litros_min' value='{$row['OXIMETRIA_PULSO_LITROS_MIN']}' />l/min
    <b>Via
        <select name='via_oximetria_min' class='COMBO_-' >
          <option value=''></option>
          <option value='1' ".($oximetria_via_min == '1' && $mode != 'nova' ? "selected" : "")." >Cateter Nasal</option>
          <option value='2' ".($oximetria_via_min == '2' && $mode != 'nova' ? "selected" : "")." >Cateter em Traqueostomia</option>
          <option value='3' ".($oximetria_via_min == '3' && $mode != 'nova' ? "selected" : "")." >Máscara de Venturi</option>
          <option value='4' ".($oximetria_via_min == '4' && $mode != 'nova' ? "selected" : "")." >Ventilação Mecânica</option>
      </select>
  </span>
</td></tr>";
			echo "<tr><td width=20%></td><td >
m&aacute;x:
<input type='texto' {$disabledMod} class='numerico1 alertas oximetria_pulso' value='".($mode != 'nova' ? $oximetria_max : '')."' name='oximetria_pulso_max' size='4' />
<select name='tipo_oximetria_max' {$disabledMod} class='COMBO_- tipo_oximetria_adicionais oximetria_pulso'>
    <option value=''></option>
    <option value='1' ".($oximetria_tipo_max == '1' && $mode != 'nova' ? "selected" : "")." >Ar Ambiente</option>
    <option value='2' ".($oximetria_tipo_max == '2' && $mode != 'nova' ? "selected" : "")." >O2 Suplementar</option>
    
</select>
</td></tr>
<tr><td></td><td>
 <span id='suplementar_max_span' name='suplementar' style='display: inline;'>
    <input type='texto' size='3' class='numerico1' name='litros_max' value='{$row['OXIMETRIA_PULSO_LITROS_MAX']}'/>l/min
    <b>Via
        <select name='via_oximetria_max' class='COMBO_-' >
          <option value=''></option>
          <option value='1' ".($oximetria_via_max == '1' && $mode != 'nova' ? "selected" : "")." >Cateter Nasal</option>
          <option value='2' ".($oximetria_via_max == '2' && $mode != 'nova' ? "selected" : "")." >Cateter em Traqueostomia</option>
          <option value='3' ".($oximetria_via_max == '3' && $mode != 'nova' ? "selected" : "")." >Máscara de Venturi</option>
          <option value='4' ".($oximetria_via_max == '4' && $mode != 'nova' ? "selected" : "")." >Ventilação Mecânica</option>
      </select>
  </span>
</td></tr>";
			echo "</tbody>";

		echo "<thead><tr>";
		echo "<th colspan='3'>Exame F&iacute;sico</th>";

		$sql_evolucao = "SELECT * FROM `fichamedicaevolucao` WHERE `ID` ={$id}";
		$resultado_evolucao = mysql_query($sql_evolucao);
		while ($row = mysql_fetch_array($resultado_evolucao)) {

			echo "<tr><td><b>Estado Geral:</b>  </td><td colspan='2'>
    <input type='radio' name='estd_geral' size=50 value='1'" . (($row['ESTADO_GERAL'] == 1) ? " checked" : "") .
				"/>Bom &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type='radio' name='estd_geral' size=50 value='2'" . (($row['ESTADO_GERAL'] == 2) ? " checked" : "") . "/>Regular &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type='radio' name='estd_geral' size=50 value='3'" . (($row['ESTADO_GERAL'] == 3) ? " checked" : "") . "/>Ruim
</td></tr>";

			echo "<tr><td><b>Mucosas:</b>  </td><td colspan='2'>
<input type='radio' name='mucosa' class='mucosa' size=50 value='s' " . (($row['MUCOSA'] == 's') ? "checked" : "") . " {$enabled} />Coradas &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type='radio' name='mucosa' size=50 value='n' " . (($row['MUCOSA'] == 'n') ? "checked" : "") . " {$enabled} />Descoradas
<span class='mucosa1'><select name='mucosaobs' class='COMBO_-' >
 <option value=''></option><option value='+' " . (($row['MUCOSA_OBS'] == '+') ? "selected" : "") . " >+</option><option value='++' " . (($row['MUCOSA_OBS'] == '++') ? "selected" : "") . " >++</option><option value='+++' " . (($row['MUCOSA_OBS'] == '+++') ? "selected" : "") . ">+++</option><option value='++++'" . (($row['MUCOSA_OBS'] == '++++') ? "selected" : "") . ">++++</option></select></span></td></tr>";

			echo "<tr><td><b>Escleras:</b>  </td><td colspan='2'>
 <input type='radio' name='escleras' class='escleras' size=50 value='s' " . (($row['ESCLERAS'] == 's') ? "checked" : "") . " {$enabled} />Anict&eacute;ricas &nbsp;&nbsp;&nbsp;&nbsp; <input type='radio' name='escleras' class='escleras' size=50 value='n' " . (($row['ESCLERAS'] == 'n') ? "checked" : "") . " {$enabled} />Ict&eacute;ricas
 <span class='escleras1'><select name='esclerasobs' class='COMBO_-' >
  <option value=''></option><option value='+' " . (($row['ESCLERAS_OBS'] == '+') ? "selected" : "") . " >+</option><option value='++' " . (($row['ESCLERAS_OBS'] == '++') ? "selected" : "") . " >++</option><option value='+++' " . (($row['ESCLERAS_OBS'] == '+++') ? "selected" : "") . ">+++</option><option value='++++'" . (($row['ESCLERAS_OBS'] == '++++') ? "selected" : "") . ">++++</option></select></span></td></tr>";

			echo "<tr><td><b>Padr&atilde;o Respirat&oacute;rio:</b>  </td>
  <td colspan='2'><input type='radio' name='respiratorio' class='respiratorio' size=50 value='s' "
				. (($row['PADRAO_RESPIRATORIO'] == 's') ? "checked" : "") . " {$enabled} />
    Eupn&eacute;ico &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type='radio' name='respiratorio' class='respiratorio' size=50 value='n' " . (($row['PADRAO_RESPIRATORIO'] == 'n') ? "checked" : "") . " {$enabled} />
    Dispn&eacute;ico
    <span class='exibir_selecao respiratorio1 '>
        <select name='respiratorioobs' >
            <option value=''></option>
            <option value='+' " . (($row['PADRAO_RESPIRATORIO_OBS'] == '+') ? "selected" : "") . " >+</option>
            <option value='++' " . (($row['PADRAO_RESPIRATORIO_OBS'] == '++') ? "selected" : "") . " >++</option>
            <option value='+++' " . (($row['PADRAO_RESPIRATORIO_OBS'] == '+++') ? "selected" : "") . ">+++</option>
            <option value='++++'" . (($row['PADRAO_RESPIRATORIO_OBS'] == '++++') ? "selected" : "") . ">++++</option>
        </select></span>
        <input type='radio' name='respiratorio' class='respiratorio' size=50 value='1' " . (($row['PADRAO_RESPIRATORIO'] == '1') ? "checked" : "") . " {$enabled} />
        Outro<br/><span class='outro_respiratorio obs_exame'><textarea cols=30 rows=2 name='outro_respiratorio' >{$row['OUTRO_RESP']}</textarea></span></td></tr>";
            if($row['PADRAO_RESPIRATORIO'] == 'n'){
               $obFr='OBG';
            }
			echo "<tr id='frequencia_respiratoria' ". ($row['PADRAO_RESPIRATORIO'] == 'n' ? "style='display:block'" : "style='display:none'") ."><td  width=20%><b>Frequência Respiratória:</b></td><td><input type='texto' cols=100 rows=2 size='6' class='numerico1 {$obFr}' name='fr' value='{$row['FR']}' {$acesso} />irpm</td></tr>";
			//Oximetria de Pulso
			echo "<tr><td width=20% ><b>Oximetria de Pulso: </b></td>
        <td width=25% colspan='3'>
            <input type='texto' class='numerico1' name='oximetria_pulso' size='4' value='{$ev->oximetria_pulso}' {$acesso} size='4' />
            <select name='tipo_oximetria' class='COMBO_-'>
                <option value=''></option>
                <option value='1'>Ar Ambiente</option>
                <option value='2'>O2 Suplementar</option>
            </select>
            <span id='suplementar_span' name='suplementar' style='display: inline;'>
                <input type='texto' size='3' class='numerico1' name='litros'/>l/min
                <b>Via
                    <select name='via_oximetria' class='COMBO_-' >
                        <option value=''></option>
                        <option value='1'>Cateter Nasal</option>
                        <option value='2'>Cateter em Traqueostomia</option>
                        <option value='3'>Máscara de Venturi</option>
                        <option value='4'>Ventilação Mecânica</option>
                    </select>
                </span>
            </td></tr>";

			echo "<tr><td  width=20%><b>Pressão Arterial (sist&oacute;lica):</b></td><td><input type='texto' class='numerico1 OBG' size='6' name='pa_sistolica' value='{$row['PA_SISTOLICA']}' {$acesso} />mmhg</td></tr>";
			echo "<tr><td  width=20%><b>Pressão Arterial (diast&oacute;lica):</b></td><td><input type='texto' class='numerico1 OBG' size='6' name='pa_diastolica' value='{$row['PA_DIASTOLICA']}' {$acesso} />mmhg</td></tr>";
			echo "<tr><td  width=20%><b>Frequência Cardíaca: </b></td><td><input type='texto' size='6' class='numerico1 OBG' name='fc' value='{$row['FC']}' {$acesso} />bpm</td></tr>";
			echo "<tr><td  width=20%><b>Temperatura:</b></td><td><input type='texto' size='6' class='numerico1 OBG' name='temperatura' value='{$row['TEMPERATURA']}' {$acesso} /> &deg;C</td></tr>";
			echo "<tr><td  width=20%><b>Peso:</b></td><td><input type='texto' size='6' class='numerico1 OBG' name='peso_paciente' value='{$row['PESO_PACIENTE']}' {$acesso} /> KG</td></tr>";

			echo"<tr><td colspan='3' ><b>DOR: </b>
            <select name=dor_sn class='COMBO_OBG'>
                <option value ='-1'>  </option>
                <option value ='n' " . (($row['DOR'] == 'n') ? "selected" : "") . ">N&atilde;o</option>
                <option value ='s' " . (($row['DOR'] == 's') ? "selected" : "") . ">Sim</option>
            </select>&nbsp;&nbsp;
            <span name='dor' id='dor_span' class='exibir_dor'>
              <b>Local:</b><input type='text' name='dor' class='dor' size=50/>
              <button id='add-dor' tipo=''  > + </button></span></td></tr>";
		}
		echo "<tr><td colspan='3' ><table id='dor' style='width:95%;'>";

		//Dor

		$sql_dor = "SELECT d.DESCRICAO, d.ESCALA, d.PADRAO, d.PADRAO_ID FROM fichamedicaevolucao f
          INNER JOIN dorfichaevolucao d ON f.ID = d.FICHAMEDICAEVOLUCAO_ID
          WHERE f.ID = {$id}";

		$resultado_dor = mysql_query($sql_dor);
		while ($row = mysql_fetch_array($resultado_dor)) {
			if ($row['PADRAO_ID'] == 5) {
				$display = "style='display:inline'";
				$outro_value = $row['PADRAO'];
			} else {
				$display = "style='display:none'";
				$outro_value = '';
			}
			echo "<tr><td class='dor1' colspan='3' name='{$row['DESCRICAO']}'>
            <img src='../utils/delete_16x16.png' class='del-dor-ativos' title='Excluir' border='0'>
            <b>Local: </b> <input name='local-dor' type='text' value='{$row['DESCRICAO']}' disabled='disabled'></input>
            Escala analogica:
               <select name='dor" . "{$row['DESCRICAO']}" . "' class='COMBO_OBG' >
                   <option value='-1' selected=''></option>
                   <option value='1'" . (($row['ESCALA'] == '1') ? " selected" : "") . ">1</option>
                   <option value='2'" . (($row['ESCALA'] == '2') ? " selected" : "") . ">2</option>
                   <option value='3'" . (($row['ESCALA'] == '3') ? " selected" : "") . ">3</option>
                   <option value='4'" . (($row['ESCALA'] == '4') ? " selected" : "") . ">4</option>
                   <option value='5'" . (($row['ESCALA'] == '5') ? " selected" : "") . ">5</option>
                   <option value='6'" . (($row['ESCALA'] == '6') ? " selected" : "") . ">6</option>
                   <option value='7'" . (($row['ESCALA'] == '7') ? " selected" : "") . ">7</option>
                   <option value='8'" . (($row['ESCALA'] == '8') ? " selected" : "") . ">8</option>
                   <option value='9'" . (($row['ESCALA'] == '9') ? " selected" : "") . ">9</option>
                   <option value='10'" . (($row['ESCALA'] == '10') ? " selected" : "") . ">10</option>
               </select>
                   Padrão da dor:<select name='padrao_dor' class='padrao_dor COMBO_OBG' >
                                <option value='-1' selected></option>
                                <option value='1' " . (($row['PADRAO_ID'] == '1') ? "selected" : "") . ">Pontada</option>
                                <option value='2' " . (($row['PADRAO_ID'] == '2') ? "selected" : "") . ">Queimação</option>
                                <option value='3' " . (($row['PADRAO_ID'] == '3') ? "selected" : "") . ">Latejante</option>
                                <option value='4' " . (($row['PADRAO_ID'] == '4') ? "selected" : "") . ">Peso</option>
                                <option value='5' " . (($row['PADRAO_ID'] == '5') ? "selected" : "") . ">Outro</option>
                            </select></b>
                            <input type='text' {$display} value='{$outro_value}' name='padraodor'></input>               


             </td></tr>";
		}

		//Cateteres e Sondas
		echo "</table></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3' >CATETERES E SONDAS</th>";
		echo "</tr></thead>";

		$sql_cateteres = "SELECT * FROM  `catetersondaevolucao` WHERE `FICHA_EVOLUCAO_ID` = {$id}";
		$resultado_cateteres = mysql_query($sql_cateteres);

		while ($row = mysql_fetch_array($resultado_cateteres)) {

			if ($row['DESCRICAO'] == 'iot') {
				$iot = true;
			} else if ($row['DESCRICAO'] == 'sne') {
				$sne = true;
			} else if ($row['DESCRICAO'] == 'gt') {
				$gt = true;
			} else if ($row['DESCRICAO'] == 'svd') {
				$svd = true;
			} else if ($row['DESCRICAO'] == 'sva') {
				$sva = true;
			} else if ($row['DESCRICAO'] == 'Acesso Venoso') {
				$acesso_venoso = true;
			} else if ($row['DESCRICAO'] == 'Acesso Perif�rico') {
				$acesso_periferico = true;
			} else {
				$outro = true;
			}
		}//while

		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='iot' " . (($iot == true) ? "checked" : "") . "/>TRAQUEOSTOMIA</td></tr>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='sne' " . (($sne == true) ? "checked" : "") . "/>SONDA NASOENTERAL</td></tr>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='gt' " . (($gt == true) ? "checked" : "") . "/>GASTROSTOMIA</td></tr>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='svd' " . (($svd == true) ? "checked" : "") . "/>SONDA VESICAL DE DEMORA</td></tr>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='sva' " . (($sva == true) ? "checked" : "") . "/>SONDA VESICAL DE AL&Iacute;VIO</td></tr>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='Acesso Venoso' central' " . (($acesso_venoso == true) ? "checked" : "") . "/>ACESSO VENOSO CENTRAL</td></tr>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='Acesso Perif�rico' " . (($acesso_periferico == true) ? "checked" : "") . "/>ACESSO VENOSO PERIF&Eacute;RICO</td></tr>";
		echo"<tr><td colspan='3' ><b>OUTRO: </b><input type='text' name='cateter' class='cateter' {$acesso} size=75 /><button id='add-cateter' tipo=''  > + </button></td></tr>";
		echo "<tr><td colspan='3' >";

		echo "<table id='cateter' style='width:95%;'>";

		//Outros cateteres
		$sql_outros_cateteres = "SELECT `DESCRICAO` FROM  `catetersondaevolucao`
        WHERE  `FICHA_EVOLUCAO_ID` ={$id}
        AND  `DESCRICAO` !=  'iot'
        AND  `DESCRICAO` !=  'sne'
        AND  `DESCRICAO` !=  'gt'
        AND  `DESCRICAO` !=  'svd'
        AND  `DESCRICAO` !=  'sva'
        AND  `DESCRICAO` !=  'Acesso Venoso'
        AND  `DESCRICAO` !=  'Acesso Perif�rico'";
		$resultado_outros_cateteres = mysql_query($sql_outros_cateteres);

		$cont_outros_cateteres;
		while ($row = mysql_fetch_array($resultado_outros_cateteres)) {
			echo "<tr><td id='outro_cateter' class='cateter1' name='int{$cont_outros_cateteres}'><img src='../utils/delete_16x16.png' class='del-cateter-ativos' title='Excluir' border='0'><b>Descrição: </b>{$row['DESCRICAO']}</td></tr>";

			$cont_outros_cateteres++;
		}

		//Exame segmentar
		echo "</table></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3'>Exame segmentar</th>";
		echo "</tr></thead>";

		$sql_exame_segmentar = "SELECT es.DESCRICAO, ese.EXAME_SEGMENTAR_ID, ese.ID, ese.AVALIACAO, ese.OBSERVACAO FROM examesegmentarevolucao ese
        INNER JOIN examesegmentar es ON es.ID = ese.EXAME_SEGMENTAR_ID WHERE ese.FICHA_EVOLUCAO_ID = {$id}";
		$resultado_exame_segmentar = mysql_query($sql_exame_segmentar);

		while ($row = mysql_fetch_array($resultado_exame_segmentar)) {

			echo "<tr><td idexame={$row['EXAME_SEGMENTAR_ID']} class='exame1'> <b>{$row['DESCRICAO']}</b></td><td colspan='2'>
            <input type=radio class='exame' name='{$row['EXAME_SEGMENTAR_ID']}' value ='1' " . (($row['AVALIACAO'] == 1) ? " checked" : "") . ">Normal ";
			echo"<input type=radio class='exame' name='{$row['EXAME_SEGMENTAR_ID']}'  value ='2' " . (($row['AVALIACAO'] == 2) ? " checked" : "") . ">Alterado
            <input type=radio class='exame' name='{$row['EXAME_SEGMENTAR_ID']}' value='3'" . (($row['AVALIACAO'] == 3) ? " checked" : "") . ">N&atilde;o Examinado</td></tr>";
			echo"<tr><td colspan='3'>
            <span class=obs_exame name='{$row['EXAME_SEGMENTAR_ID']}'>
              <textarea cols=100 rows=2 id='obsexame{$row['EXAME_SEGMENTAR_ID']}' name='obsexame' >{$row['OBSERVACAO']}</textarea></span></td></tr>";
		}

		//Exames Complementares Novos

		echo "<thead><tr>";
		$sql_evolucao = "SELECT * FROM `fichamedicaevolucao` WHERE `ID` ={$id}";
		$resultado_evolucao = mysql_query($sql_evolucao);

		while ($row = mysql_fetch_array($resultado_evolucao)) {
			echo "<th class='novo-exame' colspan='3' >Exames Complementares Novos</th>";
			echo "</tr></thead>";
			echo "<tr class='novo-exame'><td colspan='3' ><textarea cols=100 rows=5 name='novo_exame' value={$row['NOVOS_EXAMES']} {$acesso} >{$row['NOVOS_EXAMES']} {$acesso}</textarea></td></tr>";
			echo "<thead><tr>";
			echo "<th colspan='3' >Impress&atilde;o</th>";
			echo "</tr></thead>";
			echo "<thead><tr>";
			echo "<tr><td colspan='3' ><textarea cols=100 rows=5 name='impressao'   class='OBG' value={$row['IMPRESSAO']} {$acesso} >{$row['IMPRESSAO']} {$acesso}</textarea></td></tr>";
			echo "<th colspan='3' >Plano Diagn&oacute;stico</th>";
			echo "</tr></thead>";
			echo "<thead><tr>";
			echo "<tr><td colspan='3' ><textarea cols=100 rows=5 name='plano_diagnostico'   class='OBG' value={$row['PLANO_DIAGNOSTICO']} {$acesso} >{$row['PLANO_DIAGNOSTICO']}</textarea></td></tr>";
			echo "<th colspan='3' >Plano Teraup&ecirc;utico</th>";
			echo "</tr></thead>";
			echo "<thead><tr>";
			/* echo "<tr><td colspan='3' >
				<textarea cols=100 rows=5 name='terapeutico'   class='OBG' value={$row['PLANO_TERAPEUTICO']} {$acesso} >{$row['PLANO_TERAPEUTICO']}</textarea>
				</td></tr>"; */

			echo "<tr><td colspan='3' >
          <b>Conduta:</b>
          <select name='conduta' class='COMBO_OBG' >
              <option value='-1'></option>
              <option value='1'>Mantida</option>
              <option value='2'>Modificada</option>
          </select>
          <br><textarea cols=100 rows=5 name='terapeutico' id='terapeutico' hidden value={$row['PLANO_TERAPEUTICO']} {$acesso} >{$row['PLANO_TERAPEUTICO']}</textarea>
      </td></tr>";
		}

		echo $this->createDialogLocalStorageWarning();
		echo $this->createDialogSinaisSemanaWarning();
		echo $this->createDialogExamesComplementaresWarning();

		echo "</table></form></div>";
		if($mode === 'nova')
			echo "<button id='salvar-ficha-evolucao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
		else
			echo "<button id='editar-ficha-evolucao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
		echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
	}


	public function alergiaAlimentar($dados, $acesso) {
		echo "<table id='alergiaAlimentar' class='mytable' style='width:100%;'>";
		echo "<thead><tr><th colspan='3' >Alergia alimentar</th></tr></thead>";
		echo "<tr><td colspan='3' ><b>Alimento:</b><input type='text' name='alimento' class='alimento_add' size='100' maxlength='250'  {$acesso} /><button id='add-alimento' tipo='' style='cursor:pointer;' >+ </button></td></tr>";
		echo "<tr><td colspan='3'><table id='alimento' style='width:100% '>";
		$sql = "SELECT ALIMENTO AS alimento from alergia_alimentar where PACIENTE_ID = {$dados->pacienteid} AND ATIVO = 1 GROUP BY ALIMENTO";
		$result = mysql_query($sql);
		$contador = 0;
		while ($row = mysql_fetch_array($result)) {
			$nome = $row['alimento'];
			if ($nome != '') {
				$contador++;
				if ($acesso == "readonly") {
					$classe = '';
				} else {
					$classe = " <img src='../utils/delete_16x16.png' class='del-alimento' title='Excluir' border='0' {$enabled} style='cursor:pointer;'>";
				}
				echo "<tr><td colspan='2' class='alimento' desc='{$nome}' inativo='false' > $classe <b>Alimento $contador:</b>
{$nome} </td> </tr>";
			}
		}
		echo "</table></td></tr>";
		echo "</table>";
	}

	public function alergia_medicamentosa($paciente) {

		echo "<thead>";
		echo "<tr><th colspan='3' >Alergia a Medicamentos</th></tr>";
		echo "</thead>";

		echo "<tbody>";
		echo "<tr><td colspan='3' ><b>Busca  </b><input type='text' name='busca_medicamento_alergia' id='busca-medicamento-alergia' {$acesso}
size=100 /></td></tr>";
		echo"<tr><td colspan='3' >
                <input type='checkbox' id='outros-medicamento-alergia' name='outros-medicamento-alergia' {$acesso} /> Outros
                <span class='outro_medicamento_alergia' >
                <input type='text' name='outro-medicamento-alergia' class='outro_medicamento_alergia' {$acesso} size=75/>
                <button id='add-medicamento-alergia'> + </button>
        </span></td></tr>";
		echo "<tr><td colspan='3' >";

		echo "<table id='medicamentos_alergia_medicamentosa' style='width:95%;'>";

		$sql_alergia = "SELECT * FROM `alergia_medicamentosa` WHERE `ID_PACIENTE`={$paciente} AND `ATIVO`='s'
        group by `NUMERO_TISS` order by `DESCRICAO`";
		$resultado_alergia = mysql_query($sql_alergia);

		while ($row = mysql_fetch_array($resultado_alergia)) {
			echo "<tr>
                    <td colspan='2' class='medicamentos-alergia' id='{$row['ID']}' inativo='false' catalogo_id='{$row['CATALOGO_ID']}' cod='{$row['NUMERO_TISS']}'
                    desc='{$row['DESCRICAO']}'>
                    <img src='../utils/delete_16x16.png' class='del-medicamento-alergia' title='Excluir' border='0' >{$row['DESCRICAO']}
                   </td>
               </tr>";
		}

		echo "</table>";

		echo "</td></tr>";
		echo "</tbody>";
	}

//alergia_medicamentosa

	public function alergia_medicamentosa_listagem($paciente) {

		echo "<thead>";
		echo "<tr><th colspan='3' >Alergia Medicamentosa</th></tr>";
		echo "</thead>";

		$sql_alergias = "SELECT * FROM `alergia_medicamentosa` WHERE `ID_PACIENTE`={$paciente} AND `NUMERO_TISS` = 0 AND `ATIVO`='s'
  	UNION
  	SELECT * FROM `alergia_medicamentosa` WHERE `ID_PACIENTE`={$paciente} AND `ATIVO`='s' AND `NUMERO_TISS` != 0
  	group by `NUMERO_TISS` order by `DESCRICAO`";
		$resultado_alergias = mysql_query($sql_alergias);

		echo "<tbody>";
		while ($row_alergias = mysql_fetch_array($resultado_alergias)) {
			echo "<tr><td colspan='3'><b>{$row_alergias['DESCRICAO']}</b></td></tr>";
		}
		echo "</tbody>";
	}

	public function detalhes_ficha_evolucao_medica($id) {

		$id = anti_injection($id, "numerico");
		$sql = "SELECT
  	DATE_FORMAT(fev.DATA,'%d/%m/%Y %H:%i:%s') as fdata,
  	fev.ID,
  	fev.PACIENTE_ID as idpaciente,
  	fev.MOD_HOME_CARE,
  	u.nome,
  	p.nome as paciente,
  	e.nome AS empresa_paciente,
  	
  	pds.nome AS plano,
  	if(fev.empresa = 0,p.empresa,fev.empresa) as empresa_rel
  	FROM
  	fichamedicaevolucao as fev LEFT JOIN
  	usuarios as u ON (fev.USUARIO_ID = u.idUsuarios) INNER JOIN
  	clientes as p ON (fev.PACIENTE_ID = p.idClientes)
  	LEFT JOIN empresas AS e ON fev.empresa = e.id
    LEFT JOIN planosdesaude AS pds ON fev.plano = pds.id
  	
  	WHERE
  	fev.ID = '{$id}'";
		$r = mysql_query($sql);

		while ($row = mysql_fetch_array($r)) {
			$usuario = ucwords(strtolower($row["nome"]));
			$data = $row['fdata'];
			$paciente = $row['idpaciente'];
			$empresa = $row['empresa_paciente'];
			$empresa_rel = $row['empresa_rel'];
			$convenio = $row['plano'];
			$mod_home_care = $row['MOD_HOME_CARE'];
			$empresaRel = $row['empresa_rel'];
		}



		echo "<center><h1>Visualizar Evolu&ccedil;&atilde;o M&eacute;dica </h1></center>";

		$this->cabecalho($paciente, $empresa, $convenio);
		echo "<center><h2>Respons&aacute;vel: {$usuario} - Data: {$data}</h2></center>";

//        echo"<form method=post target='_blank' action='imprimir_ficha_evolucao.php?id={$id}'>";
//		echo"<input type=hidden value={$paciente} name='paciente'</input>";
//		echo "<button id='imprimir_ficha_evolucao' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' type='submit' role='button' aria-disabled='false'>
//				<span class='ui-button-text'>Imprimir</span></button>";
//		echo "</form>";

        $responsePaciente = ModelPaciente::getById($paciente);
        $empresaRelatorio = empty($empresa_rel) ? $responsePaciente['empresa'] : $empresa_rel;

        $caminho = "/medico/imprimir_ficha_evolucao.php?id={$id}";
        echo "<button caminho='$caminho' empresa-relatorio='{$empresaRelatorio}' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  role='button' aria-disabled='false'>
                <span class='ui-button-text'>
                Imprimir
                </span>
              </button>";

		echo "<div id='div-ficha-evolucao-medica'><br/>";

		echo alerta();
		echo "<form>";
		echo "<input type='hidden' name='paciente'  value='{$ev->pacienteid}' />";
		echo "<br/><table class='mytable' style='width:100%;'>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Problemas Ativos</th>";
		echo "</tr></thead>";

		$sql2 = "select pae.DESCRICAO as nome, pae.CID_ID,(CASE pae.STATUS
  			WHEN '1' THEN 'RESOLVIDO'
  			WHEN '2' THEN 'MELHOR'
  			WHEN '3' THEN 'EST&Aacute;VEL'
  			WHEN '4' THEN 'PIOR' END) as sta , pae.STATUS,pae.OBSERVACAO from problemaativoevolucao as pae where pae.EVOLUCAO_ID = {$id} 
			ORDER BY pae.status DESC";
		$result2 = mysql_query($sql2);
		$cont = 1;

		while ($row = mysql_fetch_array($result2)) {
			if ($row['STATUS'] != 0) {
				echo "<tr><td cid={$row['cid']} prob_atv='{$row['nome']}' x='{$cont}' class='probativo1' style='width:20%'><b>P{$cont} - {$row['nome']}</b> </td><td colspan='2'><b>Estado:</b> {$row['sta']}</td></tr>";
				if ($row['STATUS'] != 1) {
					echo "<tr><td colspan='3'><b>OBSERVA&Ccedil;&Atilde;O: </b>{$row['OBSERVACAO']}</td></tr>";
				}
				$cont++;
			}
		}

		echo "<input type='hidden' name='capmed'  value='{$y}' />";

		echo "<thead><tr>";
		echo "<th colspan='3' >Modalidade Home Care</th>";
		echo "</tr></thead>";
		switch ($mod_home_care){
			case 1:
				$labelHomeCare = "ASSISTÊNCIA DOMICILIAR";
				break;
			case 2:
				$labelHomeCare = "INTERNAÇÃO DOMICILIAR 6H";
				break;
			case 3:
				$labelHomeCare = "INTERNAÇÃO DOMICILIAR 12H";
				break;
			case 4:
				$labelHomeCare = "INTERNAÇÃO DOMICILIAR 24H COM RESPIRADOR";
				break;
			case 5:
				$labelHomeCare = "INTERNAÇÃO DOMICILIAR 24H SEM RESPIRADOR";
				break;
		}

		echo "<tr><td colspan='3'>{$labelHomeCare}</td></tr>";

		//Novos Problemas
		echo "<thead><tr>";
		echo "<th colspan='3' >Novos Problemas </th>";

		$sql_intercorrencias = "SELECT * FROM intercorrenciasfichaevolucao WHERE fichamedicaevolucao_id = {$id}";
		$resultado_intercorrencias = mysql_query($sql_intercorrencias);

		while ($row = mysql_fetch_array($resultado_intercorrencias)) {
			echo "<tr><td>{$row['DESCRICAO']}</td></tr>";
		}

		echo "</tr></thead>";

		$sql2 = "select pae.DESCRICAO as nome, pae.CID_ID, pae.STATUS, pae.OBSERVACAO from problemaativoevolucao as pae where pae.EVOLUCAO_ID = {$id} ";
		$result2 = mysql_query($sql2);

		while ($row = mysql_fetch_array($result2)) {
			if ($row['STATUS'] == 0) {

				echo"<tr><td colspan=''><b>- {$row['nome']} </b><td colspan='2'> <b>OBS:</b> {$row['OBSERVACAO']}</td></tr>";
			}
		}

		$this->alergia_medicamentosa_listagem($paciente);

		$sql = "select * from fichamedicaevolucao where ID = {$id}";
		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {

			$pa_sistolica_min = $row['PA_SISTOLICA_MIN'];
			$pa_sistolica_max = $row['PA_SISTOLICA_MAX'];
			$pa_sistolica = $row['PA_SISTOLICA'];
			$pa_sistolica_sinal = $row['PA_SISTOLICA_SINAL'];
			$pa_diastolica_min = $row['PA_DIASTOLICA_MIN'];
			$pa_diastolica_max = $row['PA_DIASTOLICA_MAX'];
			$pa_diastolica = $row['PA_DIASTOLICA'];
			$pa_diastolica_sinal = $row['PA_DIASTOLICA_SINAL'];
			$fc = $row["FC"];
			$fc_max = $row["FC_MAX"];
			$fc_min = $row["FC_MIN"];
			$fc_sinal = $row["FC_SINAL"];
			$fr = $row["FR"];
			$fr_max = $row["FR_MAX"];
			$fr_min = $row["FR_MIN"];
			$fr_sinal = $row["FR_SINAL"];
			$temperatura_max = $row["TEMPERATURA_MAX"];
			$temperatura_min = $row["TEMPERATURA_MIN"];
			$temperatura = $row["TEMPERATURA"];
			$temperatura_sinal = $row["TEMPERATURA_SINAL"];
			$estd_geral = $row["ESTADO_GERAL"];
			$mucosa = $row['MUCOSA'];
			$mucosaobs = $row['MUCOSA_OBS'];
			$escleras = $row['ESCLERAS'];
			$esclerasobs = $row['ESCLERAS_OBS'];
			$respiratorio = $row['PADRAO_RESPIRATORIO'];
			$respiratorioobs = $row['PADRAO_RESPIRATORIO_OBS'];
			$novo_exame = $row['NOVOS_EXAMES'];
			$impressao = $row['IMPRESSAO'];
			$diagnostico = $row['PLANO_DIAGNOSTICO'];
			$terapeutico = $row['PLANO_TERAPEUTICO'];
			$dor_sn = $row['DOR'];
			$outro_resp = $row['OUTRO_RESP'];
			$oximetria_pulso_max = $row["OXIMETRIA_PULSO_MAX"];
			$oximetria_pulso_min = $row["OXIMETRIA_PULSO_MIN"];
			$tipo_oximetria_min = $row["OXIMETRIA_PULSO_TIPO_MIN"];
			$litros_min = $row["OXIMETRIA_PULSO_LITROS_MIN"];
			$via_oximetria_min = $row["OXIMETRIA_PULSO_VIA_MIN"];
			$tipo_oximetria_max = $row["OXIMETRIA_PULSO_TIPO_MAX"];
			$litros_max = $row["OXIMETRIA_PULSO_LITROS_MAX"];
			$via_oximetria_max = $row["OXIMETRIA_PULSO_VIA_MAX"];
			$peso_paciente = $row["PESO_PACIENTE"];
			$glicemia_capilar_min = $row["GLICEMIA_CAPILAR_MIN"];
			$glicemia_capilar_max = $row["GLICEMIA_CAPILAR_MAX"];
			$oximetria_sinal = $row["OXIMETRIA_SINAL"];
			$oximetria_pulso = $row["OXIMETRIA_PULSO"];
			$tipo_oximetria = $row["OXIMETRIA_PULSO_TIPO"];
			$litros = $row["OXIMETRIA_PULSO_LITROS"];
			$via_oximetria = $row["OXIMETRIA_PULSO_VIA"];
		}

		echo "<thead><tr>";
		echo "<th colspan='3' >Evolu&ccedil;&atilde;o dos sinais vitais (Registro da semana)</th>";
		echo "</tr></thead>";
		echo "<tr><td><b>Pressão Arterial (sist&oacute;lica): </b></td><td>min:<input type='texto' readonly class='numerico1' name='pa_sistolica_min' value='{$pa_sistolica_min}' readonly size='4' /> m&aacute;x:
  			<input type='texto' class='numerico1' name='pa_sistolica_max' value='{$pa_sistolica_max}' readonly  readonly  size='4' /></td>";
		if ($pa_sistolica_sinal == 1) {
			$x = "AMARELO";
		}

		if ($pa_sistolica_sinal == 2) {
			$x = "VERMELHO";
		}
		echo "<td><b>ALERTA:</b> {$x} </td>";
		echo "</tr>";
		echo "<tr><td><b>Pressão Arterial (diast&oacute;lica): </b> </td><td> min:<input type='texto' class='numerico1' name='pa_diastolica_min' value='{$pa_diastolica_min}' readonly  size='4'  /> m&aacute;x:
  			<input type='texto' class='numerico1' name='pa_diastolica_max' value='{$pa_diastolica_max}' size='4' readonly/></td>";
		$x = '';
		if ($pa_diastolica_sinal == 1) {
			$x = "AMARELO";
		}
		if ($pa_diastolica_sinal == 2) {
			$x = "VERMELHO";
		}
		echo "<td><b>ALERTA:</b> {$x}</td>";
		echo "</tr>";
		echo "<tr><td><b>Frequência Cardíaca: </b> </td><td>min:<input type='texto' class='numerico1' name='fc_min' value='{$fc_min}'  size='4' readonly /> m&aacute;x:
		<input type='texto' class='numerico1' name='fc_max' value='{$fc_max}' readonly size='4' /></td>";
		$x = '';
		if ($fc_sinal == 1) {
			$x = "AMARELO";
		}
		if ($fc_sinal == 2) {
			$x = "VERMELHO";
		}
		echo "<td><b>ALERTA:</b> {$x}</td>";
		echo "</tr>";
		echo "<tr><td><b>Frequência Respiratória: </b> </td><td>min:<input type='texto' class='numerico1' name='fr_min' value='{$fr_min}' size='4' readonly /> m&aacute;x:
  	<input type='texto' class='numerico1' name='fr_max' value='{$fr_max}' size='4'readonly /></td>";
		$x = '';
		if ($fr_sinal == 1) {
			$x = "AMARELO";
		}
		if ($fr_sinal == 2) {
			$x = "VERMELHO";
		}
		echo "<td><b>ALERTA:</b> {$x}</td>";
		echo "</tr>";
		echo "<tr><td width=20% ><b>Temperatura: </b></td><td width=30% >min:<input type='texto' class='numerico1' name='temperatura_min' size='4' value='{$temperatura_min}' readonly size='4' /> m&aacute;x:
  	<input type='texto' class='numerico1' name='temperatura_max' size='4'value='{$temperatura_max}' readonly size='4' /></td>";
		$x = '';
		if ($temperatura_sinal == 1) {
			$x = "AMARELO";
		}
		if ($temperatura_sinal == 2) {
			$x = "VERMELHO";
		}
		echo "<td><b>ALERTA:</b> {$x}</td>";
		echo "</tr>";

		echo "<tr><td width=20% ><b>Glicemia Capilar: </b></td>
  				<td width=30% >min:<input type='texto' class='numerico1' name='glicemia_capilar_min' size='4' value='{$glicemia_capilar_min}' readonly size='4' /> m&aacute;x:
  				<input type='texto' class='numerico1' name='glicemia_capilar_max' size='4'value='{$glicemia_capilar_max}' readonly size='4' /></td>";

		//Oximetria de Pulso
		echo "<tr>
  					<td width=20% ><b>Oximetria de Pulso: </b></td>
  					<td >
	  				min:
	  				<input type='texto' class='numerico1' name='oximetria_pulso_min' size='4' value='{$oximetria_pulso_min}' readonly />";
		$tipo_min = '';
		if ($tipo_oximetria_min == 1) {
			$tipo_min = 'Ar Ambiente';
		}
		if ($tipo_oximetria_min == 2) {
			$tipo_min = 'O2 Suplementar';
		}

		echo "<b> Tipo</b><input type='texto' value='{$tipo_min}' size='18' readonly/>";

		if ($tipo_oximetria_min == 2) {
			$via_min = '';
			if ($via_oximetria_min == 1) {
				$via_min = 'Cateter Nasal';
			}
			if ($via_oximetria_min == 2) {
				$via_min = 'Cateter em Traqueostomia';
			}
			if ($via_oximetria_min == 3) {
				$via_min = 'Máscara de Venturi';
			}
			if ($via_oximetria_min == 4) {
				$via_min = 'Ventilação Mecânica';
			}

			$x = '';
			if ($oximetria_sinal == 1) {
				$x = "AMARELO";
			}
			if ($oximetria_sinal == 2) {
				$x = "VERMELHO";
			}
			echo "</td>
  		<td width=25%  ><b>ALERTA:</b> {$x}";
			echo "</td></tr>";
			echo "<tr><td></td><td>";
			echo "<b> Dosagem</b><input type='texto' size='3' value='{$litros_min}' readonly/>l/min";
			echo "<b> Via</b><input type='texto' value='{$via_min}' readonly/>";
		}
		echo "</td></tr>";

		echo "<tr><td width=20% ></td>
		<td >
  			max:
  			<input type='texto' class='numerico1' name='oximetria_pulso_max' size='4' value='{$oximetria_pulso_max}' readonly size='4' />";
		$tipo_max = '';
		if ($tipo_oximetria_max == 1) {
			$tipo_max = 'Ar Ambiente';
		}
		if ($tipo_oximetria_max == 2) {
			$tipo_max = 'O2 Suplementar';
		}

		echo "<b> Tipo</b><input type='texto' value='{$tipo_max}' size='18' readonly/>";

		if ($tipo_oximetria_max == 2) {
			$via_max = '';
			if ($via_oximetria_max == 1) {
				$via_max = 'Cateter Nasal';
			}
			if ($via_oximetria_max == 2) {
				$via_max = 'Cateter em Traqueostomia';
			}
			if ($via_oximetria_max == 3) {
				$via_max = 'Máscara de Venturi';
			}
			if ($via_oximetria_max == 4) {
				$via_max = 'Ventilação Mecânica';
			}
			echo "</td></tr>";
			echo "<tr><td></td><td><b> Dosagem</b><input type='texto' size='3' value='{$litros_max}' readonly/>l/min";
			echo "<b> Via</b><input type='texto' value='{$via_max}' readonly/>";
		}

		echo "</td></tr>";

		//

		echo "<thead><tr>";
		echo "<th colspan='3'>Exame F&iacute;sico</th>";
		$x = '';
		$y1 = '';
		if ($estd_geral == 1) {
			$x = "BOM";
		}
		if ($estd_geral == 2) {
			$x = "REGULAR";
		}
		if ($estd_geral == 3) {
			$x = "RUIM";
		}

		echo "<tr><td><b>Estado Geral:</b>  </td><td colspan='2'>{$x}</td></tr>";
		$x = '';
		$y1 = '';
		if ($mucosa == 'n') {
			$x = 'DESCORADAS';
			$y1 = $mucosaobs;
		}
		if ($mucosa == 's') {
			$x = 'Coradas';
			$y1 = $mucosaobs;
		}
		echo "<tr><td><b>Mucosas:</b>  </td><td colspan='2'>{$x}  {$y1}</td></tr>";
		$x = '';
		if ($escleras == 'n') {
			$x = 'Ict&eacute;ricas';
			$y1 = $esclerasobs;
		}
		if ($escleras == 's') {
			$x = 'Anict&eacute;ricas';
			$y1 = $esclerasobs;
		}
		echo "<tr><td><b>Escleras:</b>  </td><td colspan='2'>{$x}  {$y1}</td></tr>";
		$x = '';
		$y1 = '';
		if ($respiratorio == 'n') {
			$x = 'Dispn&eacute;ico';
			$y1 = $respiratorioobs;
		}
		if ($respiratorio == 's') {
			$x = 'Eupn&eacute;ico';
			$y1 = $respiratorioobs;
		}
		if ($respiratorio == '1') {
			$x = 'Outro';
			$y1 = $outro_resp;
		}
		echo "<tr><td><b>Padr&atilde;o Respirat&oacute;rio:</b>  </td><td colspan='2'>{$x}  {$y1}</td></tr>";

		echo "<tr><td width=20% ><b>Oximetria de Pulso: </b></td>
  	<td width=25% colspan='3'>
  	<input type='texto' class='numerico1' name='oximetria_pulso' size='4' value='{$oximetria_pulso}' readonly size='4' />";
		$tipo = '';
		if ($tipo_oximetria == 1) {
			$tipo = 'Ar Ambiente';
		}
		if ($tipo_oximetria == 2) {
			$tipo = 'O2 Suplementar';
		}

		echo "<b> Tipo </b><input type='texto' value='{$tipo}' readonly/>";

		if ($tipo_oximetria == 2) {
			$via = '';
			if ($via_oximetria == 1) {
				$via = 'Cateter Nasal';
			}
			if ($via_oximetria == 2) {
				$via = 'Cateter em Traqueostomia';
			}
			if ($via_oximetria == 3) {
				$via = 'Máscara de Venturi';
			}
			if ($via_oximetria == 4) {
				$via = 'Ventilação Mecânica';
			}
			echo "<b> Dosagem</b><input type='texto' size='3' value='{$litros}' readonly/>l/min";
			echo "<b> Via</b><input type='texto' value='{$via}' readonly/>";
		}

		echo "</td></tr>";

		echo "<tr><td width=20%><b>Pressão Arterial (sist&oacute;lica):</b></td><td><input type='texto' class='numerico1 OBG' size='6' name='pa_sistolica' value='{$pa_sistolica}'  readonly {$acesso} />mmhg</td></tr>";
		echo "<tr><td width=20%><b>Pressão Arterial (diast&oacute;lica):</b></td><td><input type='texto' class='numerico1 OBG' size='6' name='pa_diastolica' value='{$pa_diastolica}' readonly {$acesso} />mmhg</td></tr>";
		echo "<tr><td width=20%><b>Frequência Cardíaca: </b></td><td><input type='texto' size='6' class='numerico1 OBG' name='fc' readonly value='{$fc}' {$acesso} />bpm</td></tr>";
		echo "<tr><td width=20%><b>Frequência Respiratória:</b></td><td><input type='texto' size='6' class='numerico1 OBG' readonly name='fr' value='{$fr}' {$acesso} />irpm</td></tr>";
		echo "<tr><td width=20%><b>Temperatura:</b></td><td><input type='texto' size='6' class='numerico1 OBG' name='temperatura' value='{$temperatura}' readonly {$acesso} />&deg;C</td></tr>";
		echo "<tr><td width=20%><b>Peso:</b></td><td><input type='texto' size='6' class='numerico1 OBG' name='peso_paciente' value='{$peso_paciente}' readonly {$acesso} /> KG</td></tr>";

		if ($dor_sn == 's') {
			$dor_sn = "Sim";
		}
		if ($dor_sn == 'n') {
			$dor_sn = "N&atilde;o";
		}
		echo"<tr><td colspan='3' ><b>DOR:</b>{$dor_sn}</td></tr>";

		echo "<tr><td colspan='3' ><table id='dor' style='width:100%;'>";
		$sql = "select * from dorfichaevolucao where FICHAMEDICAEVOLUCAO_ID = {$id} ";
		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {
			echo "<tr><td> <b>Local:</b> {$row['DESCRICAO']}&nbsp;&nbsp; <b>Escala visual:</b> {$row['ESCALA']}&nbsp;&nbsp;<b>Padr&atilde;o:</b>{$row['PADRAO']}&nbsp;&nbsp; </td></tr>";
		}


		echo "</table></td></tr>";

		echo "<thead><tr>";
		echo "<th colspan='3' >CATETERS e SONDAS </th>";
		echo "</tr></thead>";
		$sql = "select DESCRICAO from catetersondaevolucao where FICHA_EVOLUCAO_ID = {$id}";
		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {

			echo "<tr><td colspan='3'><b>- {$row['DESCRICAO']}<b></td></tr>";
		}

		echo "<thead><tr>";
		echo "<th colspan='3' >Exame segmentar</th>";
		echo "</tr></thead>";
		$sql = "select exv.*,(CASE exv.AVALIACAO
  			WHEN '1' THEN 'NORMAL'
  			WHEN '2' THEN 'ALTERADO'
  			WHEN '3' THEN 'N EXAMINADO' END) as av,ex.DESCRICAO from examesegmentarevolucao as exv LEFT JOIN examesegmentar as ex 
			ON (exv.EXAME_SEGMENTAR_ID = ex.ID) where exv.FICHA_EVOLUCAO_ID = {$id}";
		$result = mysql_query($sql);

		$sql_exames = "SELECT * FROM `examesegmentar`";
		$resultado_exames = mysql_query($sql_exames);
		$num = mysql_num_rows();
		//while($row_exames = mysql_fetch_array($resultado_exames)){

		while ($row = mysql_fetch_array($result)) {

			echo "<tr><td idexame={$row_exames['ID']} class='exame1'> <b>{$row['DESCRICAO']}</b></td>";
			echo "<td colspan='2'>{$row['av']}</td></tr>";

			if ($row['AVALIACAO'] == 2 || $row['AVALIACAO'] == 3) {
				echo"<tr><td colspan='3'><b>OBS: </b>{$row['OBSERVACAO']}</td></tr>";
			}
		}

		echo "";
		//}


		echo "<thead><tr>";
		echo "<th colspan='3' >Exames Complementares Novos</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='3' ><b>{$novo_exame} </b></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3' >Impress&atilde;o</th>";
		echo "</tr></thead>";
		echo "<thead><tr>";
		echo "<tr><td colspan='3' ><b>{$impressao} </b></td></tr>";
		echo "<th colspan='3' >Plano Diagn&oacute;stico</th>";
		echo "</tr></thead>";
		echo "<thead><tr>";
		echo "<tr><td colspan='3' ><b>{$diagnostico} </b></td></tr>";
		echo "<th colspan='3' >Plano Teraup&ecirc;utico</th>";
		echo "</tr></thead>";
		echo "<thead><tr>";
		echo "<tr><td colspan='3' ><b>{$terapeutico}</b></td></tr>";

		echo "</table></form></div>";
		echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
	}

	public function listar_evolucao_medica($id) {
		$id = anti_injection($id, "numerico");
		$sql3 = "select Max(id) as ev from fichamedicaevolucao where PACIENTE_ID = '{$id}' ";
		$r2 = mysql_query($sql3);

		$maior_ev = mysql_result($r2, 0);


		$sql = "SELECT
  	DATE_FORMAT(fev.DATA,'%d/%m/%Y %H:%i:%s') as fdata,
  	DATE_FORMAT(fev.DATA,'%Y/%m/%d %H:%i:%s') as fdatacap,
  	fev.ID,
  	fev.PACIENTE_ID as idpaciente,
  	u.nome,
  	u.tipo as utipo,
  	u.conselho_regional,
  	fev.USUARIO_ID,
  	p.nome as paciente
  	FROM
  	fichamedicaevolucao as fev LEFT JOIN
  	usuarios as u ON (fev.USUARIO_ID = u.idUsuarios) INNER JOIN
  	clientes as p ON (fev.PACIENTE_ID = p.idClientes)
  
  	WHERE
  	fev.PACIENTE_ID = '{$id}'
  	ORDER BY
  	fev.DATA DESC";
		$r = mysql_query($sql);
		$flag = true;
		while ($row = mysql_fetch_array($r)) {
            $compTipo = $row['conselho_regional'];

			if ($flag) {

				$p = ucwords(strtolower($row["fev.PACIENTE_ID"]));
				echo "<center><h1>Evolu&ccedil;&atilde;o M&eacute;dica </h1></center>";
				$this->cabecalho($id);
				echo "<br/><br/>";
				if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_evolucao']['medico_paciente_evolucao_novo'])
					|| $_SESSION['adm_user'] == 1) {
					echo "<div><input type='hidden' name='nova_ficha_evolucao' value='{$id}' />";

                    echo "<button id='nova_ficha_evolucao'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' type='submit' role='button' aria-disabled='false'>
  		<span class='ui-button-text'>Nova</span></button></div>";
                }

				echo "<br/><table class='mytable' width='100%' >";
				echo "<thead><tr>";
				echo "<th width='55%'>Usu&aacute;rio</th>";
				echo "<th>Data</th>";
				echo "<th width='15%'>Detalhes</th>";
				echo "<th width='15%'>Op&ccedil;&otilde;es</th>";
				echo "</tr></thead>";
				$flag = false;
			}


			$u = ucwords(strtolower($row["nome"])) . $compTipo;
			$data_cap = $row['fdatacap'];
			date_default_timezone_set('America/Sao_Paulo');
			$datalimit = date('Y/m/d H:i:s', strtotime("+24 hours", strtotime($data_cap)));
			$data_atual = date('Y/m/d H:i:s');

			echo "<tr><td>{$u}</td><td>{$row['fdata']}</td>";
			echo "<td><a href=?view=paciente_med&act=fichaevolucao&id={$row['ID']}><img src='../utils/capmed_16x16.png' title='Visualizar  Evolu&ccedil;&atilde;o' border='0'></a></td>";
            if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_evolucao']['medico_paciente_evolucao_editar']) || $_SESSION['adm_user'] == 1) {
                echo "<td align='center'><a href=?view=paciente_med&act=anterior_evolucao&id={$row['ID']}><img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para nova Evolu&ccedil;&atilde;o' border='0'>";
            }
			if ($row['USUARIO_ID'] == $_SESSION['id_user'] && $datalimit > $data_atual) {
                if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_evolucao']['medico_paciente_evolucao_usar_para_nova']) || $_SESSION['adm_user'] == 1) {
                    echo "<a href=?view=paciente_med&act=editar_evolucao&id={$row['ID']}><img src='../utils/edit_16x16.png' title='Editar Evolução' border='0'>";
                }
			}
			echo "</td></tr>";
		}


		if ($flag) {
            if(isset($_SESSION['permissoes']['medico']['medico_paciente']['medico_paciente_evolucao']['medico_paciente_evolucao_novo']) || $_SESSION['adm_user'] == 1) {
				$this->nova_evolucao_medica($id);
			} else {
				echo "N&atilde;o possui Ficha de Evolu&ccedil;&atilde;o Cadastrada.";
			}

			return;
		}


		echo "</table>";
	}

	public function scoreNead($id)
    {
		echo "<thead>
                   <tr>
                      <th colspan='3'>Tabela de Avaliação para Internação Domiciliar-NEAD<sup>6<sup></th>
                   </tr>
            </thead>";
		if (!empty($id)) {
			$sql2 = "SELECT
                sc_pac.OBSERVACAO,
                sc_pac.ID_PACIENTE,
                sc_pac.ID_ITEM_SCORE,
                sc_item.DESCRICAO,
                sc_item.PESO,
                sc_item.GRUPO_ID ,
                sc_item.ID ,
                sc.id as tbplano,
                cap.justificativa,
                DATE(sc_pac.DATA) as DATA,
                grup.DESCRICAO as grdesc
                FROM score_paciente sc_pac
            LEFT OUTER JOIN score_item sc_item ON sc_pac.ID_ITEM_SCORE = sc_item.ID
            LEFT OUTER JOIN capmedica cap ON sc_pac.ID_CAPMEDICA = cap.id
            LEFT OUTER JOIN grupos grup ON sc_item.GRUPO_ID = grup.ID
            LEFT OUTER JOIN score sc ON sc_item.SCORE_ID = sc.ID
                WHERE sc_pac.ID_CAPMEDICA = '{$id}' and grup.GRUPO_NEAD_ID IS NOT NULL";
			$result2 = mysql_query($sql2);
			$elemento = '';
			while ($row = mysql_fetch_array($result2)) {

				$elemento[$row['ID_ITEM_SCORE']] = array("id_elemento" => $row['ID_ITEM_SCORE'], "peso" => $row['PESO']);
			}
		}

		$sql = "SELECT
                    sc_item.*,
                    gru.DESCRICAO as dsc,
                    gru.QTD,
                    gru_nead.PESO as peso_subgrupo,
                    gru_nead.NOME
                    
                FROM
                    score_item as sc_item INNER JOIN
                    grupos as gru ON (sc_item.GRUPO_ID = gru.ID) INNER JOIN
                    grupo_nead as gru_nead ON (gru.GRUPO_NEAD_ID=gru_nead.ID)
                    
                WHERE
                    sc_item.SCORE_ID = 3
                ORDER BY
                    sc_item.GRUPO_ID,sc_item.ID";

		$result = mysql_query($sql);
		$cont = 0;

		$i = 0;
		$cont1 = 1;
		$sub_grupo = 1;
		$nome_subtotal = '';
		$peso_subgrupo = 0;
		echo"<tr bgcolor='grey'><td><b>Descri&ccedil;&atilde;o</b></td><td colspan='3'><b>Itens da Avalia&ccedil;&atilde;o</b></td></tr>";
		while ($row = mysql_fetch_array($result)) {
			if ($sub_grupo == $row['peso_subgrupo']) {
				$nome_subtotal = $row['NOME'];
				$sub_grupo = $row['peso_subgrupo'];
				$grupo = "grupo" . $sub_grupo;
			} else {
				echo"<tr bgcolor='#F5F5F5'>
                        <td colspan='3'>
                           Subtotal {$nome_subtotal}
                            <input type='text'  value='{$subtotal}' id='{$grupo}' readonly size='2' />
                        </td>
                   </tr>";
				$nome_subtotal = $row['NOME'];
				$sub_grupo = $row['peso_subgrupo'];
				$grupo = "grupo" . $subgupo;
			}



			if (is_array($elemento)) {
				if (array_key_exists($row['ID'], $elemento)) {
					$check = "checked='checked'";
					$total_subgrupo1 = $row['peso_subgrupo'] == 1 ? $total_subgrupo1 + ($row['PESO'] * $row['peso_subgrupo']) : $total_subgrupo1 + 0;
					$total_subgrupo2 = $row['peso_subgrupo'] == 2 ? $total_subgrupo2 + ($row['PESO'] * $row['peso_subgrupo']) : $total_subgrupo2 + 0;
					$total_subgrupo3 = $row['peso_subgrupo'] == 3 ? $total_subgrupo3 + ($row['PESO'] * $row['peso_subgrupo']) : $total_subgrupo3 + 0;
				} else {
					$check = '';
				}
			} else {
				$check = '';
			}

			if ($row['peso_subgrupo'] == 1) {
				$subtotal = $total_subgrupo1;
			} else if ($row['peso_subgrupo'] == 2) {
				$subtotal = $total_subgrupo2;
			} else if ($row['peso_subgrupo'] == 3) {
				$subtotal = $total_subgrupo3;
			}

			if ($cont1 == 1) {
				$i = $row['QTD'] + 1;
				$cont = $cont1;
			} else {
				$i = 0;
			}

			if ($cont1 == 1) {
				if ($x == "#E9F4F8")
					$x = '';
				else
					$x = "#E9F4F8";
				echo "<tr bgcolor='{$x}'><td ROWSPAN='{$i}'>({$row['NOME']}) {$row['dsc']}</td></tr>";
			}
			echo "<tr bgcolor='{$x}' >
                            <td colspan='2'>
                               <span class='limpar_nead' title='Desmarcar'>
                                 <img src='../utils/desmarcar16x16.png' width='16' title='Desmarcar' border='0'>
                               </span>
                                <input type='radio'  {$check} class='nead' grupo='$nome_subtotal' fator='{$sub_grupo}'  name='" . $row['dsc'] . "' value='" . $row['PESO'] . "' cod_item='" . $row['ID'] . "' > 
                                {$row['DESCRICAO']} - Peso: {$row['PESO']}
                            </td>
                        </tr>";


			$cont1++;

			if ($cont1 > $row['QTD'])
				$cont1 = 1;
		}
		echo"<tr bgcolor='#F5F5F5'>
                <td colspan='3'>
                   Subtotal {$nome_subtotal}   
                   <input type='text'  readonly size='2' value='{$subtotal}' id='{$grupo}'/>
                </td>
            </tr>";
		$total_score_nead = $total_subgrupo1 + $total_subgrupo2 + $total_subgrupo3;
		$val_score = '';
		if (!empty($id)) {
			if ($total_score_nead < 8) {
				$justificativa = "Sem indicação para Internação Domiciliar.";
			} else if ($total_score_nead >= 8 && $total_score_nead <= 15) {
				$justificativa = "Internação Domiciliar com visita de enfermagem.";
			} else if ($total_score_nead >= 16 && $total_score_nead <= 20) {
				$justificativa = "Internação Domiciliar com até 6 horas de enfermagem.";
			} else if ($total_score_nead >= 21 && $total_score_nead <= 30) {
				$justificativa = "Internação Domiciliar com até 12 horas de enfermagem.";
			} else if ($total_score_nead > 30) {
				$justificativa = "Internação Domiciliar com até 24 horas de enfermagem.";
			}
			$val_score = "Score do paciente segundo tabela NEAD=" . $total_score_nead . ". " . $justificativa;
		}
		echo"<tr bgcolor='grey'>
                <td colspan='3' aling='center'>
                  <b>Total do Score Segundo a Tabela NEAD</b>   
                  <input type='text' readonly size='2' value='{$total_score_nead}' id='total_score_nead' />
                  <label id='justificativa_nead'>{$val_score}</label>
                </td>
            </tr>";
	}

	public function visualizarScoreNead($id, $tipo, $paciente_id, $versaoScore = null, $viewMode = 'view')
    {
        $html = "";
        $sql = "SELECT MODELO_NEAD, CLASSIFICACAO_NEAD FROM capmedica WHERE id = '{$id}'";
        $rs = mysql_query($sql);
        $row = mysql_fetch_array($rs, MYSQL_ASSOC);


		$sqlAvulso = "select *  FROM score_avulso where ID_AVULSO = {$id} and ID_PACIENTE = $paciente_id";
		$rsAvulso = mysql_query($sqlAvulso);

		$rowAvulso = mysql_fetch_array($rsAvulso, MYSQL_ASSOC);

        if($row['MODELO_NEAD'] != '2015' && $tipo == 1) {
            $html .= \App\Controllers\Score::scoreNead2016Imprimir($paciente_id, $id, "capmedica", $row['CLASSIFICACAO_NEAD'], $viewMode);
        } elseif($tipo == 0 && $rowAvulso['versao_score'] == 'nead2016' ){
			$html .= \App\Controllers\Score::scoreNead2016Imprimir($paciente_id, $rowAvulso['ID_AVULSO'], "scoreavulso", $rowAvulso['CLASSIFICACAO_NEAD'], $viewMode);
		}else {
            if (!empty($id)) {
                if ($tipo == 0) {//Avulso

                    $sql = "SELECT
                     sa.justificativa, 
                     sa.OBSERVACAO, 
                     sa.ID_PACIENTE,
                     si.DESCRICAO , 
                     si.PESO , 
                     si.GRUPO_ID ,
                     si.ID ,
                     sc.id as tbplano,
                     DATE(sa.DATA) as DATA,
                     grup.DESCRICAO as grdesc,
                     pl.nome as plano,
                     cli.nome as paciente,
                     gr_nead.PESO as fatorial
                     FROM score_avulso sa
                                     LEFT OUTER JOIN score_item si ON sa.ID_ITEM_SCORE = si.ID
                                     LEFT OUTER JOIN grupos grup ON si.GRUPO_ID = grup.ID
                                     LEFT OUTER JOIN score sc ON si.SCORE_ID = sc.ID
                                     INNER JOIN clientes as cli ON sa.ID_PACIENTE=cli.idClientes
                                     INNER JOIN planosdesaude as pl ON cli.convenio=pl.id
                                     Inner join grupo_nead as gr_nead on grup.GRUPO_NEAD_ID=gr_nead.ID
                     WHERE sa.ID_AVULSO = '{$id}' and sa.ID_PACIENTE = '{$paciente_id}'";


                    $result = mysql_query($sql);
                } else if ($tipo == 1) {//Capmedica
                    $sql = "SELECT
                                 sc_pac.OBSERVACAO,
                                 sc_pac.ID_PACIENTE,
                                 sc_item.DESCRICAO ,
                                 sc_item.PESO ,
                                 sc_item.GRUPO_ID ,
                                 sc_item.ID ,
                                 sc.id as tbplano,
                                 cap.justificativa,
                                 DATE(sc_pac.DATA) as DATA,
                                 grup.DESCRICAO as grdesc,
                                 pl.nome as plano,
                                 cli.nome as paciente,
                                 grup_nead.PESO as fatorial,
                                 user.nome  as medico,
                                 cpf
                     FROM 
                        score_paciente sc_pac
                                 LEFT OUTER JOIN score_item sc_item ON sc_pac.ID_ITEM_SCORE = sc_item.ID
                                 LEFT OUTER JOIN capmedica cap ON sc_pac.ID_CAPMEDICA = cap.id
                                 LEFT OUTER JOIN grupos grup ON sc_item.GRUPO_ID = grup.ID
                                 LEFT OUTER JOIN score sc ON sc_item.SCORE_ID = sc.ID
                                 INNER JOIN clientes as cli ON sc_pac.ID_PACIENTE=cli.idClientes
                                 INNER JOIN planosdesaude as pl ON cli.convenio=pl.id
                                 INNER JOIN grupo_nead as grup_nead ON grup.GRUPO_NEAD_ID= grup_nead.ID
                                 INNER JOIN usuarios as user ON cap.usuario=user.idUsuarios
                     WHERE 
                                 sc_pac.ID_CAPMEDICA = '{$id}' and 
                                 grup.GRUPO_NEAD_ID IS NOT NULL";

                    $result = mysql_query($sql);
                }

                $resultado_consulta = mysql_num_rows($result);
                if ($resultado_consulta > 0) {
                    $cont == 0;
                    while ($row = mysql_fetch_array($result)) {

                        if ($cont == 0) {
                            $html .= $butao_imprimir;
                            $html .= "<table style='width:95%;'>
                                 <tr  bgcolor='grey'>
                                      <td colspan='3'>
                                        <center>
                                          <b>
                                             TABELA DE AVALIAÇÃO PARA INTERNAÇÃO DOMICILIAR-NEAD<sup>6</sup>.
                                          </b>
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          <b> 
                                             Efetuada em: 
                                          </b>
                                          " . join("/", array_reverse(explode("-", $row['DATA']))) . "
                                         </center>
                                      </td>
                                  </tr>
                                  <tr >
                                      <td colspan='3'>
                                          <b>
                                           PACIENTE:
                                          </b>
                                           {$row['paciente']}
                                      </td>
                                  </tr>
                                  <tr style='background-color:#EEEEEE;'>
                                      <td colspan='3'>
                                          <b>
                                           CONVÊNIO:
                                          </b>
                                          {$row['plano']}
                                          &nbsp;&nbsp;
                                         <b> CPF: </b> {$row['cpf']}
                                        </td>

                                  </tr>

                                   <tr >
                                      <td colspan='3'>
                                          <b>
                                           MÉDICO RESPONSÁVEL:
                                          </b>
                                           {$row['medico']}
                                      </td>
                                  </tr>
                                  <tr bgcolor='grey'>
                                     <td>
                                        <b>Descrição</b>
                                     </td>
                                     <td>
                                       <b>Itens Avaliação</b>
                                     </td>
                                     <td>
                                        <b>Peso</b>
                                     </td>
                                  </tr>
                                  ";
                        }
                        $cont++;


                        $html .= "<tr>
                             <td>{$row['grdesc']}</td>
                             <td>{$row['DESCRICAO']}</td>
                             <td>{$row['PESO']}x{$row['fatorial']}</td>                        
                        ";

                        $total_score_nead = $total_score_nead + ($row['PESO'] * $row['fatorial']);
                        $justificativa = $row['justificativa'];
                    }
                    $html .= "<tr bgcolor='grey'>
                         <td colspan='3'>
                           <b>TOTAL DO SCORE SEGUNDO A TABELA NEAD:</b>
                            $total_score_nead
                         </td>
                       </tr>
                       <tr style='background-color:#EEEEEE;'>
                         <td colspan='3'>
                           <b>Justificatica:</b>
                         </td>
                       </tr>
                       <tr>
                         <td>
                          $justificativa
                         </td>
                       </tr>
           </table>";
                } else {
                    $html .= "<p>
                   <center>
                    <b>
                      Não foi encontrado o score segundo a tabela Nead 7.
                    </b>
                   </center>
                 </p>";
                }
            }
        }
		return $html;
	}

	public function addFormSugestao($local, $capmedica, $paciente, $medico) {
		$sql_sugestao = "   SELECT
                                id 
                            FROM 
                                sugestoes_secmed 
                            WHERE 
                                local_sugestao = '{$local}'
                                AND id_capmedica = {$capmedica}";
		$rs_sugestao = mysql_query($sql_sugestao);
		while ($array_sugestao = mysql_fetch_array($rs_sugestao)) {
			$id_sugestao[] = $array_sugestao['id'];
		}
		$num_sugestoes = mysql_num_rows($rs_sugestao);
		if ($num_sugestoes > 0) {
			if(!is_string($medico)){
				$sql_usuario = "   SELECT
                                        nome 
                                    FROM 
                                        usuarios 
                                    WHERE 
                                        idUsuarios = {$medico}";
				$rs_usuario = mysql_query($sql_usuario);
				$usuario = mysql_fetch_array($rs_usuario, 'MYSQL_ASSOC');
				$medico = $usuario['nome'];
			}
			echo "<button id='correcoes-ficha-avaliacao' local='{$local} 'pac='{$paciente}' cap='{$capmedica}' med='" . htmlentities($medico) . "' style='position: relative; float: right' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false'>
            <span class='ui-button-text'>Verificar Correções Anteriores</span></button>";

			$sql_sugestao = "   SELECT
                                lista.id as id_sugestao_secmed_lista,
                                ss.id as id_sugestao_secmed,
                                lista.status,
                                IF (tipo_sugestao = 'inc', 'Inconformidade', 'Erro de Preenchimento') as tipo,
                                CASE secao_sugestao
                                    WHEN 'fam' THEN 'Ficha de Avaliação Mederi'
                                    WHEN 'mh' THEN 'Motivo da Hospitalização'
                                    WHEN 'pa' THEN 'Problemas Ativos'
                                    WHEN 'am' THEN 'Alergia Medicamentosa'
                                    WHEN 'aa' THEN 'Alergia Alimentar'
                                    WHEN 'com' THEN 'Comorbidades'
                                    WHEN 'ium' THEN 'Internação Últimos 12 Meses'
                                    WHEN 'pm' THEN 'Parecer Médico'
                                    WHEN 'eqp' THEN 'Equipamentos'
                                    WHEN 'rep' THEN 'Repouso'
                                    WHEN 'die' THEN 'Dieta'
                                    WHEN 'ce' THEN 'Cuidados Especiais'
                                    WHEN 'se' THEN 'Soros e Eletrólitos'
                                    WHEN 'ai' THEN 'Antibióticos Injetáveis'
                                    WHEN 'iv' THEN 'Injetáveis IV'
                                    WHEN 'im' THEN 'Injetáveis IM'
                                    WHEN 'sc' THEN 'Injetáveis SC'
                                    WHEN 'di' THEN 'Drogas Inalatórias'
                                    WHEN 'neb' THEN 'Nebulização'
                                    WHEN 'doe' THEN 'Drogas Orais/Enterais'
                                    WHEN 'dt' THEN 'Drogas Tópicas'
                                    WHEN 'susp' THEN 'Suspender'
                                    WHEN 'oxi' THEN 'Oxigenoterapia'
                                    WHEN 'doep' THEN 'Dietas Orais/Enterais/Parenterais'
                                    WHEN 'mat' THEN 'Materiais'
                                    END AS secao,
                                sugestao
                            FROM 
                                sugestoes_secmed ss INNER JOIN sugestoes_secmed_lista lista ON (ss.id = lista.id_sugestao)
                            WHERE 
                                id_sugestao IN (" . implode(',', $id_sugestao) . ")
                                AND local_sugestao = '{$local}' 
                                AND id_capmedica = {$capmedica}";

			//DIALOG
			$html .= '<div class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable ui-resizable" tabindex="-2" role="dialog" aria-labelledby="ui-dialog-title-dialog-erros-ficha-avaliacao" style="display: none; z-index: 1006; outline: 0px; position: absolute; height: auto; width: 500px; top: 300px; left: 1050px; border: 1px solid #333">';
			$html .= '<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">';
			$html .= '<span class="ui-dialog-title" id="ui-dialog-title-dialog-erros-ficha-avaliacao">Incoformidades e Erros de Preenchimento</span>';
			$html .= '<a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" style="display: none;"><span class="ui-icon ui-icon-closethick" style="display: none;">close</span></a>';
			$html .= '</div>';


			//SUGESTOES
			$html .= '<div id="erros-ficha-avaliacao" class="ui-dialog-content ui-widget-content" local="' . $local . '" pac="' . $paciente . '" cap="' . $capmedica . '" med="' . htmlentities($medico) . '" style="width: auto; display: block; z-index: 1006; ">';
			$html .= '<div class="wrap" style="font-size: 9pt;">';
			$html .= '<table width="100%">';
			$i = 1;
			$j = 0;
			$rs_sugestao = mysql_query($sql_sugestao);
			while ($row_sugestao = mysql_fetch_array($rs_sugestao, MYSQL_ASSOC)) {
				$html .= '<tr>';
				$html .= '<td width="80%">';
				$html .= '<b>Sugestão Nº ' . $i . '</b><br>';
				$html .= '<b>Tipo: </b>' . $row_sugestao['tipo'] . '<br>';
				$html .= '<b>Corrigir em </b>' . $row_sugestao['secao'] . '<br>';
				$html .= '<b>O que corrigir: </b>' . $row_sugestao['sugestao'];
				$html .= '</td>';
				$html .= '<td align="right" width="20%">';
				$html .= '<input type="checkbox" ' . ($row_sugestao['status'] === '1' ? "checked" : "") . ' class="corrigidas" name="corrigidas" value="1" />';
				$html .= '<input type="hidden" id="idsugestao' . $j . '" name="idsugestao" value="' . $row_sugestao['id_sugestao_secmed'] . '" />';
				$html .= '<input type="hidden" id="idsugestaolista' . $j . '" name="idsugestaolista" value="' . $row_sugestao['id_sugestao_secmed_lista'] . '" />';
				$html .= '</td>';
				$html .= '<tr>';
				$html .= '<td><hr></td>';
				$html .= '</tr>';
				$i++;
				$j++;
			}
			$html .= '<input type="hidden" id="totalsugestoes" name="idsugestaolista" value="' . $j . '" />';
			$html .= '</table>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">';
			$html .= '<div class="ui-dialog-buttonset">';
			$html .= '<button type="button" id="salvar-sugestao" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"><span class="ui-button-text">Salvar</span></button>';
			$html .= '<button type="button" id="fechar-sugestao" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"><span class="ui-button-text">Fechar</span></button>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</div>';
			echo $html;
		}
		echo "<button id='sugerir-correcoes-ficha-avaliacao' local='{$local}' pac='{$paciente}' cap='{$capmedica}' med='" . htmlentities($medico) . "' style='position: relative; float: right' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false'>
        <span class='ui-button-text'>Sugerir Correções</span></button>";
		echo "<div id='dialog-erros-ficha-avaliacao' title='Sugestão de Incoformidades e Erros de Preenchimento' cod='{$capmedica}' ></div>";
	}

	public static function showVerificacaoSugestoes($local, $capmedica, $paciente, $medico, &$num_sugestao) {
		$sql_sugestao = "   SELECT
                                id 
                            FROM 
                                sugestoes_secmed 
                            WHERE 
                                local_sugestao = '{$local}' AND 
                                status = 0 
                                AND id_capmedica = {$capmedica}";
		$rs_sugestao = mysql_query($sql_sugestao);
		while ($array_sugestao = mysql_fetch_array($rs_sugestao)) {
			$id_sugestao[] = $array_sugestao['id'];
		}
		$num_sugestao = mysql_num_rows($rs_sugestao);
		$html = '';

		if ($num_sugestao > 0) {
			$sql_sugestao = "   SELECT
                                lista.id,
                                IF (tipo_sugestao = 'inc', 'Inconformidade', 'Erro de Preenchimento') as tipo,
                                CASE secao_sugestao 
                                    WHEN 'fam' THEN 'Ficha de Avaliação Mederi'
                                    WHEN 'mh' THEN 'Motivo da Hospitalização'
                                    WHEN 'pa' THEN 'Problemas Ativos'
                                    WHEN 'am' THEN 'Alergia Medicamentosa'
                                    WHEN 'aa' THEN 'Alergia Alimentar'
                                    WHEN 'com' THEN 'Comorbidades'
                                    WHEN 'ium' THEN 'Internação Últimos 12 Meses'
                                    WHEN 'pm' THEN 'Parecer Médico'
                                    WHEN 'eqp' THEN 'Equipamentos'
                                    WHEN 'rep' THEN 'Repouso'
                                    WHEN 'die' THEN 'Dieta'
                                    WHEN 'ce' THEN 'Cuidados Especiais'
                                    WHEN 'se' THEN 'Soros e Eletrólitos'
                                    WHEN 'ai' THEN 'Antibióticos Injetáveis'
                                    WHEN 'iv' THEN 'Injetáveis IV'
                                    WHEN 'im' THEN 'Injetáveis IM'
                                    WHEN 'sc' THEN 'Injetáveis SC'
                                    WHEN 'di' THEN 'Drogas Inalatórias'
                                    WHEN 'neb' THEN 'Nebulização'
                                    WHEN 'doe' THEN 'Drogas Orais/Enterais'
                                    WHEN 'dt' THEN 'Drogas Tópicas'
                                    WHEN 'susp' THEN 'Suspender'
                                    WHEN 'oxi' THEN 'Oxigenoterapia'
                                    WHEN 'doep' THEN 'Dietas Orais/Enterais/Parenterais'
                                    WHEN 'mat' THEN 'Materiais'
                                    END AS secao,
                                sugestao
                            FROM 
                                sugestoes_secmed ss INNER JOIN sugestoes_secmed_lista lista ON (ss.id = lista.id_sugestao)
                            WHERE 
                                id_sugestao IN (" . implode(',', $id_sugestao) . ")
                                AND local_sugestao = '{$local}' 
                                AND lista.status = 0 
                                AND id_capmedica = {$capmedica}";
			//BOTAO ABRIR DIALOG
			$html .= "<button id='correcoes-ficha-avaliacao' style='position: relative; float: right' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false'>";
			$html .= "<span class='ui-button-text'>Visualizar Correções Sugeridas</span></button>";

			//DIALOG
			$html .= '<div class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable ui-resizable" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog-erros-ficha-avaliacao" style="display: none; z-index: 1005; outline: 0px; position: absolute; height: auto; width: 500px; top: 300px; left: 1050px; border: 1px solid #333">';
			$html .= '<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">';
			$html .= '<span class="ui-dialog-title" id="ui-dialog-title-dialog-erros-ficha-avaliacao">Incoformidades e Erros de Preenchimento</span>';
			$html .= '<a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" style="display: none;"><span class="ui-icon ui-icon-closethick" style="display: none;">close</span></a>';
			$html .= '</div>';


			//SUGESTOES
			$html .= '<div id="erros-ficha-avaliacao" class="ui-dialog-content ui-widget-content" pac="' . $pacienteid . '" cap="' . $capmedica . '" med="' . htmlentities($medico) . '" style="width: auto; display: block; z-index: 1006; ">';
			$html .= '<div class="wrap" style="font-size: 9pt;">';
			$i = 1;
			$rs_sugestao = mysql_query($sql_sugestao);
			while ($row_sugestao = mysql_fetch_array($rs_sugestao, MYSQL_ASSOC)) {
				$html .= '<p style="width: 100%; border=1px solid #ccc;" class="blocos">';
				$html .= '<b>Sugestão Nº ' . $i . '</b><br>';
				$html .= '<b>Tipo: </b>' . $row_sugestao['tipo'] . '<br>';
				$html .= '<b>Corrigir em </b>' . $row_sugestao['secao'] . '<br>';
				$html .= '<b>O que corrigir: </b>' . $row_sugestao['sugestao'] . '<br>';
				$html .= '<hr>';
				$html .= '</p>';
				$i++;
			}
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">';
			$html .= '<div class="ui-dialog-buttonset">';
			$html .= '<button type="button" id="fechar-sugestao" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"><span class="ui-button-text">OK</span></button>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</div>';
			echo $html;
		}
	}

}
