<?php

$id = session_id();
if (empty($id))
	session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');
require __DIR__ . '/../vendor/autoload.php';

use \App\Controllers\Administracao as Administracao;
use \App\Models\Administracao\Filial;

class ImprimirRelatorioDeflagrado {

	public function cabecalho($id, $empresa = null, $convenio = null) {

		$condp1 = "c.idClientes = '{$id}'";
		$sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresa,
		c.`nascimento`,
		p.nome as Convenio,p.id,
		NUM_MATRICULA_CONVENIO,
		cpf
		FROM
		clientes AS c LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN
		planosdesaude as p ON (p.id = c.convenio)
		WHERE
		{$condp1}
		ORDER BY
		c.nome DESC LIMIT 1";

		$result = mysql_query($sql);
		$result2 = mysql_query($sql2);
		$html .= "<table width=100% >";
		while ($pessoa = mysql_fetch_array($result2)) {
			foreach ($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);
			}

            $pessoa['empresa'] = !empty($empresa) ? $empresa : $pessoa['empresa'];
            $pessoa['Convenio'] = !empty($convenio) ? $convenio : $pessoa['Convenio'];

			$html .= "<br/><tr>";
			$html .= "<td colspan='2'><label style='font-size:14px;color:#8B0000'><b></b></label></td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>PACIENTE </b></label></td>";
			$html .= "<td><label><b>SEXO </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>{$pessoa['paciente']}</td>";
			$html .= "<td>{$pessoa['sexo']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>IDADE </b></label></td>";
			$html .= "<td><label><b>UNIDADE REGIONAL </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>" . join("/", array_reverse(explode("-", $pessoa['nascimento']))) . ' (' . $pessoa['idade'] . " anos)</td>";
			$html .= "<td>{$pessoa['empresa']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>";
			$html .= "<td><label><b>MATR&Iacute;CULA </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>" . strtoupper($pessoa['Convenio']) . "</td>";
			$html .= "<td>" . strtoupper($pessoa['NUM_MATRICULA_CONVENIO']) . "</td>";
			$html .= "</tr>";
			$html.= " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
		}
		$html .= "</table>";

		return $html;
	}

	public function imprimirDeflagrado($id, $idRelatorioDeflagrado, $empresa) {

		$html .= "<form method='post' target='_blank'>";

        $responseEmpresa = Filial::getEmpresaById($empresa);

		$html .= "<h1><a><img src='../utils/logos/{$responseEmpresa['logo']}' width='100' ></a></h1>";

		$html.= "<h1 style='font-size:18px;text-align:center;'>" . htmlentities("RELATÓRIO") . " DEFLAGRADO MÉDICO</h1>";

        $sql = "SELECT 
                  empresas.nome AS empresa, 
                  planosdesaude.nome AS plano 
                FROM 
                  relatorio_deflagrado_med
                  LEFT JOIN empresas ON relatorio_deflagrado_med.empresa = empresas.id
                  LEFT JOIN planosdesaude ON relatorio_deflagrado_med.plano = planosdesaude.id
                WHERE 
                  relatorio_deflagrado_med.id = '{$idRelatorioDeflagrado}'";
        $rs = mysql_query($sql);
        $rowRel = mysql_fetch_array($rs);

		$html .= $this->cabecalho($id, $responseEmpresa['nome'], $rowRel['plano']);

		$sqlRelDeflagradoMedicoHistClinica="SELECT
                    DATE_FORMAT(relatorio_deflagrado_med.inicio,'%d/%m/%Y') as inicio,
                    DATE_FORMAT(relatorio_deflagrado_med.fim,'%d/%m/%Y') as fim,
                    relatorio_deflagrado_med.historia_clinica,
                    relatorio_deflagrado_med.servicos_prestados,
                    usuarios.nome,
                    usuarios.tipo as utipo, 
                    usuarios.conselho_regional,
                    relatorio_deflagrado_med.usuario_id
                    FROM
                    relatorio_deflagrado_med
                    INNER JOIN usuarios ON relatorio_deflagrado_med.usuario_id = usuarios.idUsuarios
                    WHERE
                    relatorio_deflagrado_med.id= $idRelatorioDeflagrado";
		$result = mysql_query($sqlRelDeflagradoMedicoHistClinica);
		if(!$result){
			echo "erro na consulta";
			return;
		}
		while($row = mysql_fetch_array($result)){
            $compTipo = $row['conselho_regional'];

			$dataInicial = $row['inicio'];
			$dataFinal = $row['fim'];
			$historiaClinica = $row['historia_clinica'];
			$servicosPrestados = $row['servicos_prestados'];
			$nomeUser = $row['nome'] . $compTipo;
			$idUsuario= $row['usuario_id'];
		}

		$html .= "<p><b> " . htmlentities("RELATÓRIO") . " DEFLAGRADO MÉDICO REALIZADO EM {$data}</p>";
		$html .= "<p>PROFISSIONAL " . htmlentities("RESPONSÁVEL") . ": {$nomeUser}</b></p>";

		$sqlrelatorioDeflagardoProblemasAtivos = "
               SELECT
                  relatorio_deflagrado_med_problema_ativo.cid10_id,
                  relatorio_deflagrado_med_problema_ativo.descricao,
                  relatorio_deflagrado_med_problema_ativo.observacao
            FROM
                  relatorio_deflagrado_med_problema_ativo
            WHERE
                  relatorio_deflagrado_med_problema_ativo.relatorio_deflagrado_med_id ='$idRelatorioDeflagrado' ";



		$html .="<table class='mytable' style='width:95%;'>

              <tr>
                 <td colspan='2' >Problemas Ativos</td>
              </tr>

               <tr style='background-color:#ccc;'>
                     <td width='50%'><b>CID/PROBLEMA</b></td>
                     <td><b>DESCRICAO MEDICA</b></td>
               </tr>";

		$resultRelatorioDeflagardoProblemasAtivos = mysql_query($sqlrelatorioDeflagardoProblemasAtivos);
		if(!$resultRelatorioDeflagardoProblemasAtivos){
			echo 'erro';
		}
		while ($row = mysql_fetch_array($resultRelatorioDeflagardoProblemasAtivos)) {
			$html .= "<tr style='border-bottom: 1px solid #ccc;'>
                             <td >
                                         <label>CID - {$row['cid10_id']}  {$row['descricao']}</label>
                             </td>
                             <td>
                                         <label>{$row['observacao']}</label>
                             </td>
                      </tr>";
		}
		$html  .="
         </table>";

		//Mostra Historia Clinica
		$html .= "<table class='mytable' style='width:95%;'>";
		$html .= "<tr><td colspan='3' style='background-color:#EEEEEE;'> <b>" . htmlentities("HISTÓRIA CLÍNICA") . " de $dataInicial à $dataFinal </b></td></tr>";
		//$html .= "<tr><td colspan='3'><b>História Clínica do período de $inicio à $fim </b></td></tr>";
		$html .= "<tr><td colspan='3'>{$historiaClinica}</td></tr>";
		$html .= "</table>";

		$html .= "<table class='mytable' style='width:95%;'>";
		$html .= "<tr><td colspan='3' style='background-color:#EEEEEE;'><b>SERVIÇOS PRESTADOS</b></td></tr>";
		$html .= "<tr><td colspan='3'>{$servicosPrestados}</td></tr>";
		$html .= "</table>";

		$html .= assinaturaProResponsavel($idUsuario);

		$paginas [] = $header . $html.= "</form>";

		$mpdf = new mPDF('pt', 'A4', 8);
		$mpdf->SetHeader('página {PAGENO} de {nbpg}');
		$ano = date("Y");
        $mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']}"." &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));
		$mpdf->WriteHTML("<html><body>");
		$flag = false;
		foreach ($paginas as $pag) {
			if ($flag)
				$mpdf->WriteHTML("<formfeed>");
			$mpdf->WriteHTML($pag);
			$flag = true;
		}
		$mpdf->WriteHTML("</body></html>");
		$mpdf->Output('deflagrado.pdf', 'I');
		exit;
	}
}

$p = new ImprimirRelatorioDeflagrado();
$p->imprimirDeflagrado($_GET['id'], $_GET['idr'], $_GET['empresa']);
