<?php
$id = session_id();
if(empty($id))
  @session_start();

require_once('db/config.php');

function alerta_msgs(){
  echo "<center><b><h1><a href='#'>Quadro de Avisos</a></h1></b></center><div>";
  echo "<img src='../utils/refresh_24x24.png' title='Atualizar' border='0' id='refresh-msg' >";
  echo "<div id='caixa-msg'>";
  $r = mysql_query("SELECT m.id, DATE_FORMAT(data,'%d/%m/%Y - %H:%i:%s') as datasms, m.titulo, m.msg, u.nome FROM msgs as m, usuarios as u 
  					WHERE m.origem = u.idUsuarios AND (m.destino = '{$_SESSION['id_user']}' OR m.destino = '-1') ORDER BY data DESC ");
    echo "<div id='caixa-entrada'><table class='mytable' width=100% >"; 
    echo "<thead><tr>"; 
    echo "<th><b>Entrada</b></th>";  
    echo "</tr></thead>";
    $cor = false;
    while($row = mysql_fetch_array($r)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
      if($cor) echo "<tr>";
      else echo "<tr class='odd' >";
      $cor = !$cor; 
      $ler = "<img cod='{$row['id']}' src='../utils/read_msg_16x16.png' title='Detalhes' border='0' class='ler-msg' t='{$row['titulo']}'>";
      echo "<td valign='top'><b>{$row['datasms']}:</b> - {$row['nome']} : {$row['titulo']} $ler</td>";  
      echo "</tr>";
    }
    echo "</table></div>";
  echo "<br/><br/>";
  $r = mysql_query("SELECT m.id, DATE_FORMAT(data,'%d/%m/%Y - %H:%i:%s') as datasms, m.titulo, m.msg, u.nome FROM msgs as m, usuarios as u 
  					WHERE m.origem = u.idUsuarios AND m.origem = '{$_SESSION['id_user']}' ORDER BY data DESC ");
    echo "<div id='caixa-saida'><table class='mytable' width=100% >"; 
    echo "<thead><tr>"; 
    echo "<th><b>Sa&iacute;da</b></th>";  
    echo "</tr></thead>";
    $cor = false;
    while($row = mysql_fetch_array($r)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
      if($cor) echo "<tr>";
      else echo "<tr class='odd' >";
      $cor = !$cor; 
      $ler = "<img cod='{$row['id']}' src='../utils/read_msg_16x16.png' title='Detalhes' border='0' class='ler-msg' t='{$row['titulo']}'>";
      $del = "<img cod='{$row['id']}' src='../utils/delete_16x16.png' title='Apagar' border='0' class='del-msg' t='{$row['titulo']}'>";
      echo "<td valign='top'><b>{$row['datasms']}:</b> - {$row['nome']} : {$row['titulo']} $ler  $del</td>";  
      echo "</tr>";
    }
    echo "</table></div>";
  echo "</div></div>";
	
}

function alerta_financeiro(){
  $empresa = $_SESSION["empresa_user"];
  echo "<center><b><h1><a href='#'>Avisos do financeiro.</a></h1></b></center><div>";
  $result = mysql_query("SELECT DISTINCT p.*,n.codigo,n.tipo,n.idNotas,f.razaoSocial FROM `parcelas` as p, fornecedores as f, notas as n WHERE p.status = 'Pendente' AND n.idNotas = p.idNota AND f.idFornecedores = n.codFornecedor AND n.empresa = '$empresa' AND p.empresa = '$empresa' AND p.vencimento BETWEEN CURDATE() AND DATE_ADD(CURDATE(),INTERVAL 7 DAY)") or trigger_error(mysql_error());
  echo "<center><b>Notas com vencimento para os pr&oacute;ximos 7 dias.</b></center>";
  if(mysql_affected_rows() == 0) echo "<p>Nenhuma nota para os pr&oacute;ximos 7 dias!";
  else{
    echo "<table class='mytable' width=100% >"; 
    echo "<thead><tr>"; 
    echo "<th><b>C&oacute;digo</b></th>";  
    echo "<th><b>Vencimento</b></th>";
    echo "<th><b>Fornecedor</b></th>"; 
    echo "<th><b>Status</b></th>"; 
    echo "<th><b>Valor</b></th>"; 
    echo "<th colspan='1' ><b>Op&ccedil;&otilde;es</b></th>";
    echo "</tr></thead>";
    $cor = false;
    while($row = mysql_fetch_array($result)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
      if($cor) echo "<tr>";
      else echo "<tr class='odd' >";
      $cor = !$cor; 
      echo "<td valign='top'>" . nl2br( $row['codigo']) . "</td>";  
      echo "<td valign='top'>" . implode("/",array_reverse(explode("-",$row['vencimento']))) . "</td>";  
      echo "<td valign='top'>" . $row['razaoSocial'] . "</td>";  
      echo "<td valign='top'>" . nl2br( $row['status']) . "</td>";  
      $tipo = ((int) $row['tipo'] == 0)?1:-1;
      echo "<td valign='top'>R$ " . ((double) $row['valor']) * $tipo . "</td>";
      echo "<td><a href=financeiro/?op=notas&act=detalhes&idNotas={$row['idNotas']}><img src='../utils/details_16x16.png' title='Detalhes' border='0'></a></td>";
      echo "</tr>";
    }
    echo "</table>";
  }
  echo "<center><b>Notas enviadas pelo setor de Log&iacute;stica.</b></center>";
  if($empresa == "1"){
  $result = mysql_query("SELECT DISTINCT n.status, n.codigo,n.tipo, n.idNotas,f.razaoSocial FROM fornecedores as f, notas as n WHERE n.status = 'Em Aberto' AND f.idFornecedores = n.codFornecedor") or trigger_error(mysql_error());
  if(mysql_affected_rows() == 0) echo "<p>Nenhuma nota da Log&iacute;stica!";
  else{
    echo "<table class='mytable' width=100% >"; 
    echo "<thead><tr>"; 
    echo "<th><b>C&oacute;digo</b></th>";  
    echo "<th><b>Fornecedor</b></th>"; 
    echo "<th><b>Status</b></th>"; 
    echo "<th colspan='1' ><b>Op&ccedil;&otilde;es</b></th>";
    echo "</tr></thead>";
    $cor = false;
    while($row = mysql_fetch_array($result)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
      if($cor) echo "<tr>";
      else echo "<tr class='odd' >";
      $cor = !$cor; 
      echo "<td valign='top'>" . nl2br( $row['codigo']) . "</td>";  
      echo "<td valign='top'>" . $row['razaoSocial'] . "</td>";  
      echo "<td valign='top'>" . nl2br( $row['status']) . "</td>";
      $bDetalhes = "<a href=financeiro/?op=notas&act=editar&idNotas={$row['idNotas']}><img src='../utils/details_16x16.png' title='Detalhes' border='0'></a>";
      $bExcluir = "<img src='../utils/delete_16x16.png' title='Excluir' cod='{$row['idNotas']}' class='excluir-nota-aberto' border='0'>";
      echo "<td>$bDetalhes $bExcluir</td>";
      echo "</tr>";
    }
    echo "</table>";
  }
  } else echo "<p>Nenhuma nota da Log&iacute;stica!";
  echo "</div>";
}

function alerta_logistica(){
  $vazio = true;
  echo "<center><b><h1><a href='#' >Avisos da log&iacute;stica.</a></h1></b></center><div>";
  echo "<center><b>Solicita&ccedil;&otilde;es Pendentes</b></center><br/>";
  $r = mysql_query("SELECT DISTINCT c.nome, s.paciente FROM solicitacoes as s, clientes as c WHERE s.paciente = c.idClientes AND s.qtd > s.enviado AND (s.status = '1' OR s.status = '2') ORDER BY c.nome");
  while($row = mysql_fetch_array($r)){ 
    foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
    $vazio = false;
    echo "<button class='bpaciente' p='{$row['paciente']}' >{$row['nome']}</button>";
  }
  if($vazio) echo "<p>Nenhuma solicita&ccedil;&atilde;o pendente!</div>";
  else echo "</div>";
}

function alerta_adm(){
}

function alerta_enfermagem(){
	$vazio = true;
	echo "<center><b><h1><a href='#' >Avisos da enfermagem.</a></h1></b></center><div>";
	echo "<center><b>Precri&ccedil;&otilde;es Pendentes</b></center>";
	echo "<form method='post' action='/enfermagem/?view=solicitacoes' >";
	$sql = "SELECT presc.*, u.nome, DATE_FORMAT(presc.data,'%d/%m/%Y - %T') as sdata, c.nome as paciente, idClientes  FROM prescricoes as presc, clientes as c, usuarios as u ".
		   "WHERE c.idClientes = presc.paciente AND presc.criador = u.idUsuarios ".
		   "AND (c.empresa = '{$_SESSION['empresa_user']}' OR '{$_SESSION['empresa_user']}' = '1' ) AND presc.lockp < now() ORDER BY data DESC";
	$result = mysql_query($sql);
	$flag = false;
	echo "<input type='hidden' name='label' value='' /><input type='hidden' name='paciente' value='-1' />";
	echo "<table class='mytable' width=100% ><thead><tr><th>Paciente</th><th>Prescri&ccedil;&atilde;o</th></tr></thead><tfoot></tfoot>";
	while($row = mysql_fetch_array($result)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
      $vazio = false;
      $label = "<h1>Paciente: {$row['paciente']}</h1><h3>Profissional: {$row['nome']} - {$row['sdata']}</h3>";
      echo "<tr label='{$label}' paciente='{$row['idClientes']}' ><td><input type='radio' name='antigas' value='{$row['id']}'/>";
      echo "{$row['paciente']}</td><td>{$row['nome']} - {$row['sdata']}</td></tr>";
	}
	echo "</table>";
	echo "<p><button id='iniciar-solicitacao'>Iniciar</button></form>";
	if($vazio) echo "<p>Nenhuma precri&ccedil;&atilde;o pendente!</div>";
  	else echo "</div>";
}

?>