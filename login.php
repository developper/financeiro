<?php
$id = session_id();
//session_set_cookie_params(60);
if(empty($id)){
    session_start();
}

include_once('db/config.php');
include_once('vendor/autoload.php');
use  App\Models\Administracao\Filial;

if(!function_exists('combo_usuarios')){
    function combo_usuarios($name){
        $result = mysql_query("SELECT * FROM usuarios ORDER BY nome");
        $code = "<select name='{$name}' style='background-color:transparent;' >";
        $code .= "<option value='-1'>TODOS</option>";
        while($row = mysql_fetch_array($result)){
            $code .= "<option value='{$row['idUsuarios']}'>{$row['nome']}</option>";
        }
        $code .= "</select>";
        return $code;
    }
}

function formulario_login(){
    $var = '';
    if($_SESSION["logado"] <> "1"){
        $var =  "<form action='?auth=1' method='post'>";
        $var .=  "Login: <input name='login' type='text' /><br>";
        $var .=  "Senha: <input name='senha' type='password' /><br>";
        $var .=  "<input type='submit' value='Acessar' /></form>";
    } else {
        $var .= show_login();
        $var .= relogar();
    }
    return $var;
}


function auth_login($modal= null,$renovar=null){

    $login = addslashes($_POST["login"]);
    $empresaLogin = filter_input(INPUT_POST, 'empresa', FILTER_SANITIZE_NUMBER_INT);
    $senha = $renovar == 1 ? $_POST["senha"] :  md5(addslashes($_POST["senha"]));
    $dataAgora = new \DateTime();

    // $empresaPrincipalComp = "";
    // if($empresaLogin == 1) {
    //     $empresaPrincipalComp = " AND empresa IN (5, 7, 11, 13, 14) ";
    // } elseif($empresaLogin == 2) {
    //     $empresaPrincipalComp = " AND empresa IN (1, 2, 12)";
    // } elseif($empresaLogin == 3) {
    //     $empresaPrincipalComp = " AND empresa IN (4) ";
    // }

    $sql = "SELECT u.*,e.nome as nempresa FROM usuarios as u inner join empresas as e on (u.empresa = e.id) WHERE UPPER(u.login) = UPPER('$login') AND u.senha = '$senha' ";
    $rs = mysql_query($sql);
    $err = "";

    if(mysql_num_rows($rs) == 1) {

        $user = mysql_fetch_array($rs);
        $sqlUsuarioSetores = "Select setor_id from usuarios_setores where usuario_id = {$user['idUsuarios']}";
        $rsUsuarioSetores = mysql_query($sqlUsuarioSetores);
        while($row = mysql_fetch_array($rsUsuarioSetores)){
            $usuariosSetores[] = $row['setor_id'] ;
        }

        //conferindo o login e senha para segurança
        mysql_query("Update usuarios set logado='1' where idUsuarios = {$user['idUsuarios']}");
        /*********************************************************************/
        #carrega as cidades

        $s = '(';
        $cont = 1;

        $empresas ="(";

        if($user['adm']==1){
            $sql2 = "Select {$user['idUsuarios']} as usuarios_id, id as empresas_id from empresas where ATIVO='S'";

        }else{
            $sql2 = "Select usuarios_id, empresas_id from usuarios_empresa where usuarios_id = {$user['idUsuarios']}";
            $empresas .= $user['empresa'];
        }

        $rs1 = mysql_query($sql2);
        $num = mysql_num_rows($rs1);


        while($row_1 = mysql_fetch_array($rs1) ){
            if($cont==1 ){
                if($user['adm']==1)
                {
                    $empresas .= $row_1['empresas_id'].',';
                } else if($user['adm']!=1 && $cont<$num)
                {
                    $empresas .= ','.$row_1['empresas_id'].',';
                }else if($user['adm']!=1 && $cont==$num)
                {
                    $empresas .= ','.$row_1['empresas_id'];
                }

            }elseif($cont<$num){
                $empresas .= $row_1['empresas_id'].',';
            }else{
                $empresas .= $row_1['empresas_id'];
            }
            $cont++;

        }
        $empresas .=")";
        /**********************************************************************/

        $blockAt = \DateTime::createFromFormat('Y-m-d',$user['block_at']);

        if($login == $user['login']){
            if($senha == $user['senha']){
                if($dataAgora <$blockAt){
                    $logado = "1";
                    $id_user = $user['idUsuarios'];
                    $tipo_user = $user['tipo'];
                    $nome_user = $user['nome'];
                    $adm = $user['adm'];
                    $nempresa = $user['nempresa'];
                    $empresa = $user['empresa'];
                    $_SESSION["id_user"] = $id_user;
                    $_SESSION['senha'] = $user['senha'];
                    $_SESSION["logado"] = $logado;
                    $_SESSION["tipo_user"] = $tipo_user;
                    $_SESSION["prestador"] = $user['prestador'];
                    $_SESSION["login_user"] = $login;
                    $_SESSION["nome_user"] = $nome_user;
                    $_SESSION["adm_user"] = $adm;
                    $_SESSION["empresa_user"] = $empresas;
                    $_SESSION["avisos"] = true;
                    $_SESSION["nome_empresa"] = $nempresa;
                    $_SESSION["filial"] = $empresaLogin;
                    $_SESSION["empresa_principal"] = $empresa;
                    $_SESSION["modadm"] = $user['modadm'];
                    $_SESSION["modaud"] = $user['modaud'];
                    $_SESSION["modenf"] = $user['modenf'];
                    $_SESSION["modfin"] = $user['modfin'];
                    $_SESSION["modfat"] = $user['modfat'];
                    $_SESSION["modlog"] = $user['modlog'];
                    $_SESSION["modmed"] = $user['modmed'];
                    $_SESSION["modrem"] = $user['modrem'];
                    $_SESSION["modnutri"] = $user['modnutri'];
                    $_SESSION["adm_ur"] = $user['ADM_UR'];
                    $_SESSION["padronizar_medicamento"] = $user['permissao_padronizar_medicamento'];
                    $_SESSION["user_setores"] = $usuariosSetores;

                    $_SESSION['permissoes'] = \App\Models\Administracao\Usuario::getPermissoes($id_user);

                    # # # # # # # # # # # # # # # # # # # # # # # # # # # #
                    #  CONFIGURANDO O COOKIE COM O TEMPO DE RELOGAR NO SISTEMA
                    #  EVITAR PERDA DE SESSÃO
                    #
                    # # # # # # # # # # # #

                    setcookie('tempo_relogar',date('Y-m-d H:i:s',mktime(date('H'),date('i')+(APP_ENV === 'dev' ? 30 : 15),date('s'),date('m'),date('d'),date('Y'))));

                   // die();

                    if(!is_null($modal)){
                        echo confere_tempo($renovar);
                    }else{
                        if(isset($_SESSION['permissoes']['ocorrencia']) || $_SESSION["adm_user"] == 1){
                            header('Location: /ocorrencia/?action=listar-ocorrencias');
                        }else{
                            header('Location: inicio.php');
                        }
                       
                    }
                }else{
                    $err .= htmlentities("Usuário desativado!");
                }

            } else {
                $err .= "A senha n&atilde;o confere!";
            }
        }else{
            $err .= "O usu&aacute;rio n&atilde;o confere!";
        }
    }else {
        $err .= "Usu&aacute;rio ou senha inv&aacute;lidos. Tente novamente.";
    }

    // $var = formulario_login();
    $var = "<br><h3 align='center' ><font color='red' >".$err."</font></h3>";
    return $var;
}

function atualiza()
{
    setcookie('tempo_relogar',date('Y-m-d H:i:s',mktime(date('H'),date('i')+(APP_ENV === 'dev' ? 35 : 15),date('s'),date('m'),date('d'),date('Y'))));
}

function confere_tempo($renovar = null)
{
    if(!isset($_COOKIE['tempo_relogar']) || $renovar!=null)
    {
        # # # # # # # # # # # # # # # # # # # # # # # # # # # #
        #  CONFIGURANDO O COOKIE COM O TEMPO DE RELOGAR NO SISTEMA
        #  EVITAR PERDA DE SESSÃO
        #  
        # # # # # # # # # # # #

        atualiza();
        $data_inicio = new DateTime(date("Y-m-d H:i:s"));
        $data = $_COOKIE['tempo_relogar'];
        $data_fim = new DateTime($data);
        $data_diff = $data_inicio->diff($data_fim);
        echo ($data_diff->format("%i")*60)+$data_diff->format("%s");
        //return false;
    }
    else
    {
        $data_inicio = new DateTime(date("Y-m-d H:i:s"));
        $data = $_COOKIE['tempo_relogar'];
        $data_fim = new DateTime($data);
        $data_diff = $data_inicio->diff($data_fim);
        echo $data_diff->format("%r%i:%r%s");
    }


    //echo "INICIO:".$data_inicio->format("Y-m-d %r%H:%i:%s").'  FIM:'.$data_fim->format("Y-m-d %r%H:%i:%s").' = DIFF'.$data_diff->format("Y-m-d %r%H:%i:%s");
}



function hora_servidor()
{
    $now = new DateTime(date("Y-m-d H:i:s"));
    echo $now->format("Y-m-d H:i:s");
}

function show_login(){


    $responseEmpresas = Filial::getUnidadesAtivas();
    $var = "
<div id='dialog-escolher-empresa-gerar-documento' caminho='' title='Escolha qual empresa deve sair no documento'>
    <div id='div-escolher-empresa-gerar-documento'>
        <p>
            <label for='select-escolher-empresa-gerar-documento' >Empresas</labelfor>
            <select id='select-escolher-empresa-gerar-documento'>
            <option value='-1' selected>selecione</option>";
    foreach($responseEmpresas as $empresa){
        $var.= "<option  value='{$empresa['id']}' logo='{$empresa['logo']}' nome-empresa='{$empresa['nome']}'>{$empresa['nome']}</option>";
    }
    $var.="
            </select>
        </p>
    </div>
</div>

<div id='dialog-troca-senha' title='Trocar senha.'>
  		 <div id='div-aviso-troca' class='ui-widget'><div class='ui-state-error ui-corner-all' style='padding: 0 .7em;'>
  		 <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;'></span>
  		 <span id='aviso-troca-senha' ></span></p></div></div>	
  		 <br/><b>Senha atual:</b><br/><input type='password' class='OBG' name='senha-atual'/>
  		 <br/><b>Nova senha:</b><br/><input type='password' class='OBG' name='nova-senha'/>
  		 <br/><b>Confirma senha:</b><br/><input type='password' class='OBG' name='conf-senha'/></div>";
    $var .= "<div id='dialog-enviar-msg' title='Enviar mensagem.'>
  		 <div id='div-aviso-msg' class='ui-widget'><div class='ui-state-error ui-corner-all' style='padding: 0 .7em;'>
  		 <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;'></span>
  		 <span id='aviso-msg' ></span></p></div></div>	
  		 <br/><b>Destinatário:</b><br/>".combo_usuarios("usuarios")."
  		 <br/><b>Título:</b><br/><input type='text' name='titulo' />
  		 <br/><b>Mensagem:</b><br/><textarea rows='5' cols='50' name='msg'></textarea></div>";
    $var .= "<table border=0 width=100% cellpadding='1' style='border-spacing:0 0;border-collapse: collapse'' >";
    switch($_SESSION["tipo_user"]){
        case "administrativo":
            $var .= "<tr><td style='vertical-align:top;padding-top:15px;'><img class='img_icon_menu_usu' src='/utils/usr_administrador_48x48.png' title='Setor Administrativo' /></td>";
            break;
        case "enfermagem":
            $var .= "<tr><td style='vertical-align:top;padding-top:15px;'><img class='img_icon_menu_usu' src='/utils/usr_enfermeira_48x48.png' title='Setor Enfermagem' /></td>";
            break;
        case "financeiro":
            $var .= "<tr><td style='vertical-align:top;padding-top:15px;'><img class='img_icon_menu_usu' src='/utils/usr_financeiro_48x48.png' title='Setor Financeiro' /></td>";
            break;
        case "faturamento":
            $var .= "<tr><td style='vertical-align:top;padding-top:15px;'><img class='img_icon_menu_usu' src='/utils/usr_financeiro_48x48.png' title='Setor de Faturamento' /></td>";
            break;
        case "logistica":
            $var .= "<tr><td style='vertical-align:top;padding-top:15px;'><img class='img_icon_menu_usu' src='/utils/usr_logistica_48x48.png' title='Setor Logística' /></td>";
            break;
        case "medico":
            $var .= "<tr><td style='vertical-align:top;padding-top:15px;'><img class='img_icon_menu_usu' src='/utils/usr_medico_48x48.png' title='Setor Médico' /></td>";
            break;
        case "nutricao":
            $var .= "<tr><td style='vertical-align:top;padding-top:15px;'><img class='img_icon_menu_usu' src='/utils/usr_medico_48x48.png' title='Setor de Nutrição' /></td>";
            break;
        default:
            $var .= "<tr><td style='vertical-align:top;padding-top:15px;'><img class='img_icon_menu_usu' src='/utils/usr_administrador_48x48.png' title='Setor Administrativo' /></td>";
            break;
    }

    $var .= "<td id='texto_menu'><p>Ol&aacute; ".ucwords(strtolower($_SESSION["nome_user"]))." </p>";
    $var .= "<p><b>Empresa</b><br/>{$_SESSION['nome_empresa']}<br/><br/><a href='/inicio.php?logoff=1' style='color:blue;font-size:12px;float:right;padding:3px;border:1px solid #ccc;margin-right:5px;background:#eeeeee;'>SAIR</a></p></p></td></tr>";
    $var .= "<tr><td colspan='2'></td></tr>";
    $var .= "<tr><td colspan='2' align='center'><b style='font-size: 16px;'>Horário de Brasília</b></td></tr>";
    $var .= "<tr><td colspan='2' align='center'><b id='horario-brasilia' style='font-size: 14px;'>00:00:00</b></td></tr>";
    $var .= "<tr><td colspan='2'><div class='ui-state-highlight'><span class='ui-icon  ui-icon-key' style='float:left;padding:2px;'></span> Autenticação a cada 15 minutos! </div></tr>";
    $var .= "<tr><td colspan='2'><div id='defaultCountdown' style='padding:5px;display:table;width:100%;'></div></td></tr>";

    $menu = \App\Models\Sistema\Areas::getByIdPai();
    //echo '<pre>'; print_r($_SESSION['permissoes']);
    foreach ($menu as $itemMenu) {
        if($itemMenu['id'] == 74) {
            $var .= "<tr class='linha_menu' id='trocar-senha'><td><img class='img_icon_menu' src='/utils/new-icons/change-pass.png' width='48' title='Trocar de Senha' /></td><td><b>Alterar Senha</b></a></td></tr>";
        } elseif(isset($_SESSION['permissoes'][$itemMenu['nome']]) || $_SESSION['adm_user']) {
            $var .= "<tr class='linha_menu'>
                        <td>
                            <a href='{$itemMenu['link']}'>
                                <img class='img_icon_menu' src='{$itemMenu['caminho_icone']}' width='48' title='{$itemMenu['descricao']}' />
                            </a>
                        </td>
                        <td>
                            <a href='{$itemMenu['link']}' style='a:visited:color:#000;'>
                                <b>{$itemMenu['descricao']}</b>
                            </a>
                        </td>
                    </tr>";
        }
    }

    if(isset($_SESSION['prestador']) && $_SESSION['prestador'] == '1') {
        $var .= "<tr class='linha_menu' id='area-prestador'>
                        <td>
                            <img class='img_icon_menu' src='/utils/new-icons/administration.png' width='48' title='Área do Prestador' />
                        </td>
                        <td>
                            <a href='/prestador' style='a:visited:color:#000;'>
                                <b>Área do Prestador</b>
                            </a>
                        </td>
                    </tr>";
    }

    /*$var .= "<tr class='linha_menu'><td><a href='/inicio.php'><img class='img_icon_menu' src='/utils/new-icons/home.png' width='48' title='Mudanças do Sistema' /></td><td><a href='/inicio.php' style='a:visited:color:#000;}'><b>Mudanças do Sistema</b></a></td></tr>";
    $var .= "<tr class='linha_menu'><td><a href='/ocorrencia/?action=listar-ocorrencias'><img class='img_icon_menu' src='/utils/new-icons/ocorrencia.png' width='48' title='Menu Ocorrências' /></a></td><td><a href='/ocorrencia/?action=listar-ocorrencias'><b>Ocorrências</b></a></td></tr>";
    if(isset($_SESSION['permissoes']['administrativo'])) {
        $var .= "<tr class='linha_menu'><td><a href='/adm'><img class='img_icon_menu' src='/utils/new-icons/administration.png' width='48' title='Menu Administrativo' /></a></td><td><a href='/adm'><b>Administra&ccedil;&atilde;o</b></a></td></tr>";
    }
    if(isset($_SESSION['permissoes']['medico'])) {
        $var .= "<tr class='linha_menu'><td><a href='/medico'><img class='img_icon_menu' src='/utils/new-icons/medic.png' width='48' title='Menu Médico' /></a></td><td><a href='/medico'><b>M&eacute;dicos</b></a></td></tr>";
    }
    if(isset($_SESSION['permissoes']['exames'])) {
        $var .= "<tr class='linha_menu'><td><a href='/exames/?action=menu'><img class='img_icon_menu' src='/utils/new-icons/lab-exams2.png' width='48' title='Menu Exames' /></a></td><td><a href='/exames/?action=menu'><b>Exames</b></a></td></tr>";
    }
    if(isset($_SESSION['permissoes']['enfermagem'])) {
        $var .= "<tr class='linha_menu'><td><a href='/enfermagem'><img class='img_icon_menu' src='/utils/new-icons/nurse.png' width='48' title='Menu Enfermagem' /></a></td><td><a href='/enfermagem'><b>Enfermagem</b></a></td></tr>";
    }
    if(isset($_SESSION['permissoes']['nutricao'])) {
        $var .= "<tr class='linha_menu'><td><a href='/nutricao'><img class='img_icon_menu' src='/utils/new-icons/nutritionist.png' width='48' title='Nutri&ccedil;&atilde;o' /></td><td><a href='/nutricao' style='a:visited:color:#000;}'><b>Nutri&ccedil;&atilde;o</b></a></td></tr>";
    }
    if(isset($_SESSION['permissoes']['auditoria'])) {
        $var .= "<tr class='linha_menu'><td><a href='/auditoria'><img class='img_icon_menu' src='/utils/new-icons/audit.png' width='48' title='Menu Auditoria' /></a></td><td><a href='/auditoria'><b>Auditoria Interna</b></a></td></tr>";
    }
    if(isset($_SESSION['permissoes']['remocao'])) {
        $var .= "<tr class='linha_menu'><td><a href='/remocao/?action=menu'><img class='img_icon_menu' src='/utils/new-icons/ambulance.png' width='48' title='Menu Remoções' /></a></td><td><a href='/remocao/?action=menu'><b>Remo&ccedil;&otilde;es</b></a></td></tr>";
    }
    if(isset($_SESSION['permissoes']['financeiro'])) {
        $var .= "<tr class='linha_menu'><td><a href='/financeiros'><img class='img_icon_menu' src='/utils/new-icons/financial2.png' width='48' title='Menu Financeiro' /></a></td><td><a href='/financeiros'><b>Financeiro</b></a></td></tr>";
    }
    if(isset($_SESSION['permissoes']['faturamento'])) {
        $var .= "<tr class='linha_menu'><td><a href='/faturamento'><img class='img_icon_menu' src='/utils/new-icons/financial.png' width='48' title='Menu Faturamento' /></a></td><td><a href='/faturamento'><b>Faturamento</b></a></td></tr>";
    }
    if(isset($_SESSION['permissoes']['logistica'])) {
        $var .= "<tr class='linha_menu'><td><a href='/logistica'><img class='img_icon_menu' src='/utils/new-icons/logistics.png' width='48' title='Menu Logística' /></a></td><td><a href='/logistica'><b>Log&iacute;stica</b></a></td></tr>";
    }
    if(isset($_SESSION['permissoes']['sac'])) {
        $var .= "<tr class='linha_menu'><td><a href='/sac'><img class='img_icon_menu' src='/utils/sac.png' width='48' title='SAC' /></a></td><td><a href='/sac/?action=menu'><b>SAC</b></a></td></tr>";
    }
    $var .= "<tr class='linha_menu'><td><a href='/tutorial'><img class='img_icon_menu' src='/utils/new-icons/help.jpg' width='48' title='Menu Tutorial' /></a></td><td><a href='/tutorial'><b>Ajuda e Publicações</b></a></td></tr>";
    $var .= "<tr class='linha_menu'><td><img class='img_icon_menu' src='/utils/new-icons/change-pass.png' width='48' title='Trocar de Senha' id='trocar-senha' /></td><td><b>Alterar Senha</b></a></td></tr>";*/
    $var .= "</table>";
    $var .= "</br>";
    return $var;
}


function logoff(){
    mysql_query("Update usuarios set logado=0 where idUsuarios = {$_SESSION["id_user"]}");
    session_destroy();
    echo "<script>window.location.reload()</script>";
}

function relogar(){

    echo"<div id='dialog-relogar' title='Login Automático do Sistema'>";
    echo "<div class='ui-state-highlight'><span class='ui-icon  ui-icon-key' style='float:left;padding:2px;'></span> Autenticação a cada 15 minutos! </div>";
    echo "<p><label for='login'><b>Login: </b></label><span id='login_relogar'> {$_SESSION["login_user"]}</span></p>";
    echo "<p><label for='senha'><b>Senha: </b></label><input type='password' disabled='disabled' name='senha' id='senha_relogar' value='{$_SESSION['senha']}' /></p>";
    echo"</div>";
}

if (isset($_POST['query']))
    switch($_POST['query']) {
        case "relogar":
            auth_login($_POST['modal'],$_POST['renovar']);
            break;
        case "confere_tempo":
            confere_tempo($_POST['renovar']);
            break;
        case "hora_servidor":
            hora_servidor();
            break;
    }

?>
