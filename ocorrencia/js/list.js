$(function($) {
    moment().locale('pt-BR');

    $(".chosen-select").chosen({search_contains: true});
    $('#sugerir-resolvido').bootstrapToggle();

    $(".responder").click(function () {
        var ocorrencia = $(this).attr('ocorrencia');

        $(".responder-" + ocorrencia).slideToggle();
    });

    $(".cancelar-resposta").click(function () {
        var ocorrencia = $(this).attr('ocorrencia');

        $(this).parents('div.panel-body').find('textarea').val('');
        $(this).parents('div.panel-body').find('#sugerir-resolvido').bootstrapToggle('off');

        $(".responder-" + ocorrencia).slideToggle();
    });

    $(".enviar-resposta").click(function () {
        var $panelBody = $(this).parents('div.panel-body');

        var ocorrencia = $(this).attr('ocorrencia');
        var resposta = $panelBody.find('textarea').val();
        var sugerir_resolvido = $panelBody.find('#sugerir-resolvido').prop('checked');

        var data = new FormData();
        $.each($panelBody.find('.arquivos')[0].files, function(i, file) {
            data.append('file-' + i, file);
        });

        data.append('ocorrencia', ocorrencia);
        data.append('resposta', resposta);
        data.append('sugerir_resolvido', sugerir_resolvido ? 1 : 0);
        //console.log(data);

        if(resposta != '') {
            $.ajax({
                url : '/ocorrencia/?action=enviar-resposta',
                type: "POST",
                data : data,
                processData: false,
                contentType: false,
                success: function(response, textStatus, jqXHR){
                    var json = JSON.parse(response);

                    if ($panelBody.has('div.sem-respostas')) {
                        $panelBody.find('div.sem-respostas').remove();
                    }

                    var texto_resolvido = sugerir_resolvido ? 'Sim' : 'Não';

                    $("#respostas-" + ocorrencia).append(
                        '<div class="feed-element" resposta="' + json.id + '">' +
                        '   <div class="media-body">' +
                        '       <small class="pull-right">' + moment().startOf('minute').fromNow() + '</small>' +
                        '       <strong>' + json.usuario + '</strong>' +
                        '       <p style="width: 90%;">' + resposta + '</p>' +
                        '       <p style="width: 90%;"><b>Sugerido como resolvido? </b>' + texto_resolvido + '</p>' +
                        '       <small class="text-muted">' + moment().calendar() + '</small>' +
                        '       <p class="arquivos-resposta-ocorrencia">' +
                                    json.arquivos +
                        '       </p>' +
                        '   </div>' +
                        '</div>' +
                        '<hr>'
                    );
                    $panelBody.find('textarea').val('');
                    $panelBody.find('.arquivos').val('');
                    $(this).parents('div.panel-body').find('textarea').val('');
                    $(this).parents('div.panel-body').find('#sugerir-resolvido').bootstrapToggle('off');
                    $(".responder-" + ocorrencia).slideToggle();
                },
                error: function(jqXHR, textStatus, errorThrown){
                    //if fails
                }
            });
            /*$.post(
                '/ocorrencia/?action=enviar-resposta',
                {
                    ocorrencia: ocorrencia,
                    resposta: resposta,
                    sugerir_resolvido: sugerir_resolvido ? 1 : 0,
                    arquivos: files
                },
                function (response) {
                    var json = JSON.parse(response);

                    if ($panelBody.has('div.sem-respostas')) {
                        $panelBody.find('div.sem-respostas').remove();
                    }

                    var texto_resolvido = sugerir_resolvido ? 'Sim' : 'Não';

                    $("#respostas-" + ocorrencia).append(
                        '<div class="feed-element" resposta="' + json.id + '">' +
                        '   <div class="media-body">' +
                        '       <small class="pull-right">' + moment().startOf('minute').fromNow() + '</small>' +
                        '       <strong>' + json.usuario + '</strong>' +
                        '       <p style="width: 90%;">' + resposta + '</p>' +
                        '       <p style="width: 90%;"><b>Sugerido como resolvido? </b>' + texto_resolvido + '</p>' +
                        '       <small class="text-muted">' + moment().calendar() + '</small>' +
                        '   </div>' +
                        '</div>' +
                        '<hr>'
                    );
                    $panelBody.find('textarea').val('');
                    $(this).parents('div.panel-body').find('textarea').val('');
                    $(this).parents('div.panel-body').find('#sugerir-resolvido').bootstrapToggle('off');
                    $(".responder-" + ocorrencia).slideToggle();
                }
            );*/
        } else {
            alert('A resposta está vazia!');
        }
    });

    $(".resolver").click(function () {
        var ocorrencia_id = $(this).attr('ocorrencia');

        if($('#ocorrencia-' + ocorrencia_id).hasClass('in')) {
            $('#accordion-' + ocorrencia_id).find('.panel-title').trigger('click');
        }

        $('#resolver-' + ocorrencia_id).slideToggle();
    });

    $(".cancelar-resolver").click(function () {
        var ocorrencia = $(this).attr('ocorrencia');

        $(this).parents('div.panel-body').find('textarea').val('');
        $(this).parents('div.panel-body').find('.retorno-hc').bootstrapToggle('off');
        $(this).parents('div.panel-body').find('.qualidade-retorno').bootstrapToggle('off');

        $("#resolver-" + ocorrencia).slideToggle();
    });

    $('.retorno-hc').change(function () {
        var ocorrencia = $(this).attr('ocorrencia');
        $(this).parents('div.panel-body').find('.descricao-qualidade-retorno-combo').hide();
        $(this).parents('div.panel-body').find('.descricao-qualidade-retorno').val('');
        if($(this).is(':checked')) {
            $(this).parents('div.panel-body').find('.descricao-qualidade-retorno-combo').show();
        }
    });

    $(".finalizar").click(function () {
        var $panelBody = $(this).parents('div.panel-body');
        var ocorrencia = $(this).attr('ocorrencia');
        var retorno_hc = $panelBody.find('.retorno-hc').prop('checked');

        var descricao_qualidade_retorno = $panelBody.find('textarea').val();
        if(descricao_qualidade_retorno == '' && retorno_hc == true ){
            alert("Preencha o campo 'Descreva'.");
            return false;
        }

        $.post(
            '/ocorrencia/?action=resolver-ocorrencia',
            {
                ocorrencia: ocorrencia,
                retorno_hc: retorno_hc ? 1 : 0,
                descricao_qualidade_retorno: descricao_qualidade_retorno
            },
            function (response) {
                if(response == '1') {
                    var reabrir = 
                    
                    $panelBody.find('textarea').val('');
                    $panelBody.find('#retorno-hc').bootstrapToggle('off');
                    $("#resolver-" + ocorrencia).slideToggle();
                    $(".status-ocorrencia-" + ocorrencia).
                    removeClass('pull-left').
                    addClass('pull-right').
                    html("<div class='btn-group btn-group-xs pull-left' style='margin-right: 10px; margin-top: -5px;'>"+
                        "<button class='btn btn-xs btn-default dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>"+
                        "Ações"+
                        "<span class='caret'></span>"+
                        "</button>"+
                        "<ul class='dropdown-menu' aria-labelledby='dropdownMenu1'>"+
                        "<li>"+
                        "<a class='reabrir' ocorrencia='"+ocorrencia+"'>"+
                        "<span class='fa fa-undo'></span>"+
                    "Reabrir"+
                    "</a>"+
                    "</li>"+
                    "</ul>"+
                    "</div> Finalizado " + moment().calendar());
                }
            }
        );
    });

    $("#classificacao").change(function () {
        var classificacao = $(this).val();

        $.get(
            '/ocorrencia/?action=json-subclassificacoes',
            {
                classificacao_id: classificacao
            },
            function (response) {
                var json = JSON.parse(response);

                $("#subclassificacao").html(
                    '<option value="">Todos</option>'
                );
                $.each(json, function (index, value) {
                    $("#subclassificacao").append(
                        '<option value="' + value.id + '">' + value.descricao + '</option>'
                    );
                    $(".chosen-select").trigger('chosen:updated')
                });
            }
        );
    });

    $(".desativar").click(function () {
        var ocorrencia = $(this).attr('ocorrencia');

        if(confirm('Deseja realmente desativar essa ocorrência?')) {
            $.post(
                '/ocorrencia/?action=desativar-ocorrencia',
                {
                    ocorrencia: ocorrencia
                },
                function (response) {
                    if (response == '1') {
                        alert('Ocorrência desativada com sucesso!');
                        $("#accordion-" + ocorrencia).remove();
                        $("#div-resposta-" + ocorrencia).remove();
                    }
                }
            );
        }
    });

    $(".reabrir").live('click', function () {
        var ocorrencia = $(this).attr('ocorrencia');

        if(confirm('Deseja realmente reabrir essa ocorrência?')) {
            $.post(
                '/ocorrencia/?action=reabrir-ocorrencia',
                {
                    ocorrencia: ocorrencia
                },
                function (response) {
                    if (response == '1') {
                        alert('Ocorrência reaberta com sucesso!');
                        window.location.reload();
                    }else{
                        alert(response);
                        return false;
                    }
                }
            );
        }
    });
});
