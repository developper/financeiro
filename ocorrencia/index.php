<?php

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../db/config.php';

require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

//ini_set('display_errors', 1);
//
define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
	"GET" => [
		'/ocorrencia/?action=ocorrencia'  => '\App\Controllers\Ocorrencia::form',
		'/ocorrencia/?action=listar-ocorrencias'  => '\App\Controllers\Ocorrencia::listarForm',
		'/ocorrencia/?action=json-subclassificacoes'  => '\App\Controllers\Ocorrencia::jsonOcorrenciaSubclassificacoes',
	],
	"POST" => [
        '/ocorrencia/?action=ocorrencia'  => '\App\Controllers\Ocorrencia::salvarOcorrencia',
        '/ocorrencia/?action=listar-ocorrencias'  => '\App\Controllers\Ocorrencia::listarOcorrencias',
        '/ocorrencia/?action=enviar-resposta'  => '\App\Controllers\Ocorrencia::enviarRespostaOcorrencia',
        '/ocorrencia/?action=resolver-ocorrencia'  => '\App\Controllers\Ocorrencia::resolverOcorrencia',
        '/ocorrencia/?action=desativar-ocorrencia'  => '\App\Controllers\Ocorrencia::desativarOcorrencia',
        '/ocorrencia/?action=reabrir-ocorrencia' => '\App\Controllers\Ocorrencia::reabrirOcorrencia',
	]
];


$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}
