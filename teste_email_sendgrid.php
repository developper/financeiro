<?php
ini_set('display_errors', 'On');
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/db/config.php';

use App\Core\Config;
use App\Services\Gateways\SendGridGateway;
use App\Services\MailAdapter;

$api_data = (object) Config::get('sendgrid');

$gateway = new MailAdapter(
	new SendGridGateway(
		new \SendGrid($api_data->user, $api_data->pass),
		new \SendGrid\Email()
	)
);
$gateway->adapter->setSendData(142, 'auditoria', 14, 11);

var_dump($gateway->adapter->mail, $gateway->adapter->sendMail());
