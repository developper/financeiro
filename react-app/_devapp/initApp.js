import React from 'react'
import Report from "./components/report/Report";
import FinancialBudgetApp from './components/financiaBudget/FinancialBudget'
import ManagerialValues from './components/managerialValues/MagerialValues'

const InitApp = ({id}) => {

    const getRoute = (id) => {
    switch(id)
    {
        case 1:
            return <Report />     
            break;
        case 2:
            return <FinancialBudgetApp/>
            break;
        case 3:
            return <ManagerialValues/>
            break;

    }
    }
   return (
       <>
       {getRoute(id)}
       </>    
   ) 
}

export default InitApp