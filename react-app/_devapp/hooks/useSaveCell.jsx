import React from "react";
import {useDispatch, useSelector} from "react-redux";
import * as _actions from "../store/actions-reducers";
import {Api} from '../services/config/request';
import {MODULES} from "../constants";
import useAmountDataParams from "./useAmountDataParams";
import useToastAlert from "./useToastAlert";
import useAlterCell from "./useAlterCell";


const useSaveCell = () => {
    const dispatch = useDispatch();
    const formData = useSelector(state => state.formData);
    const url = `${MODULES.FINANCEIROS}/orcamento-financeiro/?action=update-item`;
    const {amountDataParams} = useAmountDataParams();
    const {toastAlert} = useToastAlert();
    const { alterCell } = useAlterCell();

    const saveCellOne = (indexOne, index, value, props) => {
        let newFormData = [...formData];

        if (newFormData[indexOne].months[index].value !== value) {
            Api.postRequest(url, amountDataParams(props, value, index, 100)).then(response => {
                newFormData[indexOne].months[index].value = value;
                newFormData[indexOne].months[index].showCell = false;
                dispatch(_actions.saveFormData(alterCell(newFormData, indexOne, index)));

                toastAlert('Valor alterado com sucesso!', 'success');
            });
        } else {
            newFormData[indexOne].months[index].showCell = false;
            dispatch(_actions.saveFormData(newFormData));
        }
    }

    const saveCellPercentageOne = (indexOne, value) => {
        let newFormData = [...formData];
        newFormData[indexOne].percentage = value;
        // newFormData[indexOne].showInputPercentage = false;
        dispatch(_actions.saveFormData(newFormData));
    }

    const saveCellTow = (indexOne, indexTow, index, value, props) => {
        let newFormData = [...formData];
        if (newFormData[indexOne].sub[indexTow].months[index].value !== value) {
            Api.postRequest(url, amountDataParams(props, value, index, 0)).then(response => {
                newFormData[indexOne].sub[indexTow].months[index].value = value
                newFormData[indexOne].sub[indexTow].months[index].showCell = false;
                newFormData[indexOne].sub[indexTow].percentage = 0;
                dispatch(_actions.saveFormData(newFormData));
                toastAlert('Valor alterado com sucesso!', 'success');
            });
        } else {
            newFormData[indexOne].sub[indexTow].months[index].showCell = false;
            dispatch(_actions.saveFormData(newFormData));
        }

    }

    const saveCellPercentageTow = (indexOne, indexTow, value) => {
        let newFormData = [...formData];
        newFormData[indexOne].sub[indexTow].percentage = value;
        // newFormData[indexOne].sub[indexTow].showInputPercentage = false;
        dispatch(_actions.saveFormData(newFormData));
    }

    const saveCellTree = (indexOne, indexTow, indexTree, index, value, props) => { 
        let newFormData = [...formData];
        if (newFormData[indexOne].sub[indexTow].sub[indexTree].months[index].value !== value) {
            Api.postRequest(url, amountDataParams(props, value, index, 0)).then(response => {
                newFormData[indexOne].sub[indexTow].sub[indexTree].months[index].value = value;
                newFormData[indexOne].sub[indexTow].sub[indexTree].months[index].showCell = false;
                newFormData[indexOne].sub[indexTow].sub[indexTree].percentage = 0;
                dispatch(_actions.saveFormData(newFormData));
                toastAlert('Valor alterado com sucesso!', 'success');

            });
        } else {
            newFormData[indexOne].sub[indexTow].sub[indexTree].months[index].showCell = false;
            dispatch(_actions.saveFormData(newFormData));
        }
    }

    const saveCellPercentageTree = (indexOne, indexTow, indexTree, value) => {
        let newFormData = [...formData];
        newFormData[indexOne].sub[indexTow].sub[indexTree].percentage = value;
        // newFormData[indexOne].sub[indexTow].sub[indexTree].showInputPercentage = false;
        dispatch(_actions.saveFormData(newFormData));
    }

    const saveCellFour = (indexOne, indexTow, indexTree, indexFour, index, value, props) => {
        let newFormData = [...formData];
        if(newFormData[indexOne].sub[indexTow].sub[indexTree].sub[indexFour].months[index].value !== value) {
            Api.postRequest(url, amountDataParams(props, value, index, 0)).then(response => {
                newFormData[indexOne].sub[indexTow].sub[indexTree].sub[indexFour].months[index].value = value;
                newFormData[indexOne].sub[indexTow].sub[indexTree].sub[indexFour].months[index].showCell = false;
                newFormData[indexOne].sub[indexTow].sub[indexTree].sub[indexFour].percentage = 0;
                dispatch(_actions.saveFormData(newFormData));
                toastAlert('Valor alterado com sucesso!', 'success');
            })
        } else {
            newFormData[indexOne].sub[indexTow].sub[indexTree].sub[indexFour].months[index].showCell = false;
            dispatch(_actions.saveFormData(newFormData));

        }

    }

    const saveCellPercentageFour = (indexOne, indexTow, indexTree,indexFour, value) => {
        let newFormData = [...formData];
        newFormData[indexOne].sub[indexTow].sub[indexTree].sub[indexFour].percentage = value;
        dispatch(_actions.saveFormData(newFormData));
    }

    return {
        saveCellOne,
        saveCellTow,
        saveCellTree,
        saveCellFour,
        saveCellPercentageOne,
        saveCellPercentageTow,
        saveCellPercentageTree,
        saveCellPercentageFour
    };
}

export default useSaveCell;