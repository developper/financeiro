import React from "react";

const useFirstCharacterIsNumber = () => {

    const firstCharacterIsNumber = (name) => {
        const number = name.substr(0, 1)
        return typeof parseInt(number) === 'number' && isFinite(number);
    }

    return { firstCharacterIsNumber };
}


export default useFirstCharacterIsNumber;