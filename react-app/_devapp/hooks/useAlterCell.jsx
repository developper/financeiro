import React from "react";
import {useDispatch, useSelector} from "react-redux";
import * as _action from "../store/actions-reducers";
import {TYPE_CALC as _type_calc} from "../constants";

const useAlterCell = () => {
    const dispatch = useDispatch();
    let formData = useSelector(state => state.formData);

    const alterCell = (newFormData, oneIndex, indexMonth) => {
        newFormData.map((om, i) => {
            om.sub.map((omSub, onSubIndex) => {
                omSub.months.map((month, mIndex) => {
                    if (mIndex === indexMonth && omSub.type_calc === _type_calc.PERCENTAGE) {
                        month.value = parseFloat(formData[oneIndex].months[indexMonth].value) * (parseFloat(omSub.percentage) / 100);
                    }
                });

                omSub.sub.map((omSubSub, onSubSubIndex) => {
                    omSubSub.months.map((m, mI) => {
                        if (mI === indexMonth && omSub.type_calc === _type_calc.PERCENTAGE) {
                            m.value = parseFloat(formData[oneIndex].months[indexMonth].value) * (parseFloat(omSubSub.percentage) / 100);
                        }
                    })
                })
            })
        }, []);

        return newFormData;

    }

    // dispatch(_action.saveFormData(formData))

    return { alterCell };
}

export default useAlterCell;