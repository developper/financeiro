const useRemoveCaracteres = () => {

    const removeCaracteres = (value) => {
        let commaRemoved = value.replace(/\./g, '');
        commaRemoved = commaRemoved.replace(/\,/g, '.');
        return commaRemoved;
    }

    return { removeCaracteres };
}

export default useRemoveCaracteres