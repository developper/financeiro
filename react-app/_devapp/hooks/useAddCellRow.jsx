import React from "react";
import {useDispatch, useSelector} from "react-redux";
import * as _actions from '../store/actions-reducers';
import { Api } from "../services/config/request";
import {MODULES} from "../constants";
import useToastAlert from "./useToastAlert";

const useAddCellRow = () => {
    const formData = useSelector(state => state.formData);
    const accountPlanRedux = useSelector(state => state.accountPlan);
    const date = useSelector(state => state.date);
    const idBudget = useSelector(state => state.idBudget);
    const dispatch = useDispatch();
    const { toastAlert } = useToastAlert();

    const addCellRow = (level, accountPlan, isRemove) => {
        let newFormData = [...formData];

        const url = `${MODULES.FINANCEIROS}/orcamento-financeiro/?action=gerar-itens-plano-contas`;

        let addRow = {
            id_topico: null,
            data_inicio: date.date_start,
            data_fim: date.date_end,
            orcamento_id: idBudget,
            plano_conta_id: accountPlan.value,

        }
        if(level.length === 3) {
            addRow.id_topico = newFormData[level[0]].sub[level[1]].sub[level[2]].id_topico
        }
        if ( level.length === 2 ) {
            addRow.id_topico = newFormData[level[0]].sub[level[1]].id_topico
        }

        if ( level.length === 1 ) {
            addRow.id_topico = newFormData[level[0]].id_topico
        }

        Api.postRequest(url, addRow).then(response => {
            if(level.length === 3) {
                newFormData[level[0]].sub[level[1]].sub[level[2]].sub.push(response.data[0]);
            }
            if ( level.length === 2 ) {
                newFormData[level[0]].sub[level[1]].sub.push(response.data[0]);
            }
            if ( level.length === 1 ) {
                newFormData[level[0]].sub.push(response.data[0]);
            }
            if (!accountPlanRedux.includes(accountPlan.value)) {
                dispatch(_actions.saveAccountPlan([...accountPlanRedux, accountPlan.value]));
            }

            dispatch(_actions.saveFormData(newFormData));

            const message = 'Plano de conta adicionado com sucesso!'
            toastAlert(message, 'success');
        }).catch(error => {

        });

    }


    return { addCellRow };
}


export default useAddCellRow;