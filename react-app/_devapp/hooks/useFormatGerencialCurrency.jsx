import React from "react";

const useFormatGerencialCurrency = () => {
    const formatCurrency = (value, isRemoveTheDecimalPlace) => {
        const moneyNotSimbol = parseFloat(value).toLocaleString('pt-br', {minimumFractionDigits: 2});
        const arrayCurrency = moneyNotSimbol.split(',');

        if (isRemoveTheDecimalPlace) {
            return arrayCurrency[0];
        } else {
            return moneyNotSimbol;
        }
    };

    return { formatCurrency };
}

export default useFormatGerencialCurrency;