import React from "react";

const useAddKeyInFormData = () => {

    const addKeyInFormData = (formData) => {
        formData.map((levelOne, i) => {
            levelOne.isShowRow = false;

            levelOne.sub.map((levelTow) => {
                levelTow.isShowRow = false;

                levelTow.sub.map((levelTree) => {
                    levelTree.isShowRow = false
                })
            })
        }, [])

        return formData;
    }

    return { addKeyInFormData };
}

export default useAddKeyInFormData