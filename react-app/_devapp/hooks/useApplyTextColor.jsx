const useApplyTextColor = () => {
    const textColor = (value) => {
        return value >= 0 ? 'blue' : 'red'
       }
       return {textColor}
}

export default useApplyTextColor