import React from "react";
import { useSelector, useDispatch } from "react-redux";
import * as _actions from '../store/actions-reducers';

const useHandleLevel = () => {

    const formData = useSelector(state => state.formData);
    const showHiddenLevelLine = useSelector(state => state.showHiddenLevelLine);
    const dispatch = useDispatch();




    const handleAllLevelOne = () => {
        let newFormData = [...formData];

        newFormData.map(itemOne => {
            if (!showHiddenLevelLine.one) {
                itemOne.isShowRow = false;
                //itemOne.isShowRow = true;
                //dispatch(_actions.saveShowHiddenLeval({one: true}));
            } else {
                itemOne.isShowRow = false;
                dispatch(_actions.saveShowHiddenLeval({one: false}));
            }

            itemOne.sub.map(itemTwo => {
                itemTwo.isShowRow = false;


                itemTwo.sub.map(itemThree => {
                    itemThree.isShowRow = false;

                     
                    itemThree.sub.map(itemFour=>{
                        itemFour.isShowRow = false;
                    })
                })
            })
        })
        dispatch(_actions.saveShowHiddenLeval({tow: false, three: false,four:false}))
        dispatch(_actions.saveFormData(newFormData));
    }

    const handleAllLevelTow = () => {
        let newFormData = [...formData];
        newFormData.map(itemOne => {
            //itemOne.isShowRow = true;
            
            itemOne.sub.map(itemTwo => {
                if (!showHiddenLevelLine.tow) {
                    itemOne.isShowRow = true;
                    dispatch(_actions.saveShowHiddenLeval({one: true}));
                } else {
                    //itemOne.isShowRow = false;
                    itemTwo.isShowRow = false;
                    dispatch(_actions.saveShowHiddenLeval({one: false}));
                }

                itemTwo.sub.map(itemThree => {
                    itemThree.isShowRow = false;


                    itemThree.sub.map(itemFour=>{
                        itemFour.isShowRow = false;
                    })

                })
            })
        })
        dispatch(_actions.saveShowHiddenLeval({one:true, three: false,four:false}))
        dispatch(_actions.saveFormData(newFormData));
    }

    const handleAllLevelThree = () => {

        let newFormData = [...formData];
        newFormData.map(itemOne => {
            itemOne.isShowRow = true;

            itemOne.sub.map(itemTwo => {
               

                itemTwo.sub.map(itemThree => {
                   // itemThree.isShowRow = false;
                   if (!showHiddenLevelLine.three) {
                    itemTwo.isShowRow = true;
                    dispatch(_actions.saveShowHiddenLeval({tow: true}));
                } else {
                    itemThree.isShowRow = false;
                  //  dispatch(_actions.saveShowHiddenLeval({tow: false}));
                }

                    itemThree.sub.map(itemFour=>{
                        itemFour.isShowRow = false;
                    })

                })
            })
        })
        dispatch(_actions.saveShowHiddenLeval({one: true, tow: true,four:false}))
        dispatch(_actions.saveFormData(newFormData));
    }

    const handleAllLevelFour = () => {

        let newFormData = [...formData];
        newFormData.map(itemOne => {
            itemOne.isShowRow = true;

            itemOne.sub.map(itemTwo => {
                //itemTwo.isShowRow = false ;

                itemTwo.sub.map(itemThree => {
                    //itemThree.isShowRow = false;
                    if(!showHiddenLevelLine.four) {
                        itemTwo.isShowRow = true ;
                        itemThree.isShowRow = true;
                        dispatch(_actions.saveShowHiddenLeval({three:true}));
                    } else {
                        itemTwo.isShowRow = false;
                        itemThree.isShowRow = false;
                        dispatch(_actions.saveShowHiddenLeval({three:false}));
                    }

                    itemThree.sub.map(itemFour => {
                        itemFour.isShowRow = false;
                    })
                    
                })
            })
        } )
        dispatch(_actions.saveShowHiddenLeval({one: true, tow: true,three:true}))
        dispatch(_actions.saveFormData(newFormData));
    }

  return { handleAllLevelOne, handleAllLevelTow, handleAllLevelThree, handleAllLevelFour };
}

export default useHandleLevel;