import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import * as _actions from "../store/actions-reducers";
import {MODULES, TYPE_CALC as _type_calc} from "../constants";
import {Api} from "../services/config/request";
import useToastAlert from "./useToastAlert";

const useApplyMultipleValues = () => {
    const dispatch = useDispatch();
    const formData = useSelector(state => state.formData)
    let dataApply = [];
    const url = `${MODULES.FINANCEIROS}/orcamento-financeiro/?action=update-itens`;
    const { toastAlert } = useToastAlert();

    const verifySignal = (index, lastNumber, value, valuePercentage) => {
        let _newNumber = 0;
        if (index > 0) {
            if (value[2] === '+') {
                _newNumber = lastNumber + (lastNumber * (valuePercentage / 100));
            } else {
                _newNumber = lastNumber - (lastNumber * (valuePercentage / 100));
            }
            return _newNumber;
        } else {
            return  parseFloat(value[0]);
        }
    }

    const applyMultipleValuesOne = async (indexOne, type, value, valuePercentage) => {
        let newFormData = [...formData];
        let lastNumber = type === _type_calc.VALUE_PERCENTAGE ? parseFloat(value[0]) : parseFloat(value);
        newFormData[indexOne].type_calc = type;
        newFormData[indexOne].percentage = 100;

        newFormData[indexOne].months.map((month, i) => {
            if (type === _type_calc.VALUE) {
                dataApply[i] = {
                    id: month.id,
                    tabela: month.table,
                    valor: value,
                    porcentagem: 100
                };
                return month.value = parseFloat(value);

            }

            if (type === _type_calc.PERCENTAGE) {
                dataApply[i] = {
                    id: month.id,
                    tabela: month.table,
                    valor: parseFloat(newFormData[0].months[i].value) * (valuePercentage / 100),
                    porcentagem: valuePercentage
                };
                newFormData[indexOne].percentage = valuePercentage;
                return month.value = parseFloat(newFormData[0].months[i].value) * (valuePercentage / 100);
            }

            if (type === _type_calc.VALUE_PERCENTAGE) {
                lastNumber = verifySignal(i, lastNumber, value, valuePercentage);
                dataApply[i] = {
                    id: month.id,
                    tabela: month.table,
                    valor: lastNumber,
                    porcentagem: 100
                };
                return month.value = lastNumber
            }
        }, []);

        try {
            await Api.postRequestJson(url, dataApply);
            dispatch(_actions.saveFormData(newFormData));
            toastAlert('Valores alterados com sucesso!', 'success');
        } catch (e) {
            toastAlert('Valores alterados com sucesso!', 'error');
        }

    }

    const applyMultipleValuesTow = async (indexOne, indexTow, type, value, valuePercentage) => {
        let newFormData = [...formData];
        let lastNumber = type === _type_calc.VALUE_PERCENTAGE ? parseFloat(value[0]) : parseFloat(value);
        newFormData[indexOne].sub[indexTow].type_calc = type;
        newFormData[indexOne].sub[indexTow].percentage = type === _type_calc.VALUE_PERCENTAGE ? parseFloat(value[1]) : 0;

        newFormData[indexOne].sub[indexTow].months.map((month, i) => {
            if (type === _type_calc.VALUE) {
                newFormData[indexOne].sub[indexTow].percentage = 0;
                dataApply[i] = {
                    id: month.id,
                    tabela: month.table,
                    valor: value,
                    porcentagem: 0
                };
                return month.value = value;
            }

            if (type === _type_calc.PERCENTAGE) {
                dataApply[i] = {
                    id: month.id,
                    tabela: month.table,
                    valor: parseFloat(newFormData[0].months[i].value) * (valuePercentage / 100),
                    porcentagem: valuePercentage
                };
                newFormData[indexOne].sub[indexTow].percentage = valuePercentage;
                return month.value = parseFloat(newFormData[0].months[i].value) * (valuePercentage / 100);
            }

            if (type === _type_calc.VALUE_PERCENTAGE) {
                lastNumber = verifySignal(i, lastNumber, value, valuePercentage);
                dataApply[i] = {
                    id: month.id,
                    tabela: month.table,
                    valor: lastNumber,
                    porcentagem: 0
                };
                newFormData[indexOne].sub[indexTow].percentage = 0;
                return month.value = lastNumber
            }

        }, []);

        // Api.updateRequest(url, dataApply).then(response => {
        //     dispatch(_actions.saveFormData(newFormData));
        //     toastAlert('Valores alterados com sucesso!', 'success');
        // }).catch(response => {
        //     debugger
        // });
        try {
            await Api.postRequestJson(url, dataApply);
            dispatch(_actions.saveFormData(newFormData));
            toastAlert('Valores alterados com sucesso!', 'success');
        } catch (e) {
            toastAlert('Valores alterados com sucesso!', 'error');
        }
    }

    const applyMultipleValuesTree = async (indexOne, indexTow, indexTree, type, value, valuePercentage) => {
        let newFormData = [...formData];
        let lastNumber = type === _type_calc.VALUE_PERCENTAGE ? parseFloat(value[0]) : parseFloat(value);
        newFormData[indexOne].sub[indexTow].sub[indexTree].type_calc = type;
        newFormData[indexOne].sub[indexTow].sub[indexTree].porcentage = type === _type_calc.VALUE_PERCENTAGE ? parseFloat(value[1]) : 0;

        newFormData[indexOne].sub[indexTow].sub[indexTree].months.map((month, i) => {
            if (type === _type_calc.VALUE) {
                dataApply[i] = {
                    id: month.id,
                    tabela: month.table,
                    valor: value,
                    porcentagem: 0
                };
                newFormData[indexOne].sub[indexTow].sub[indexTree].percentage = 0;
                return month.value = value;
            }

            if (type === _type_calc.PERCENTAGE) {
                dataApply[i] = {
                    id: month.id,
                    tabela: month.table,
                    valor: parseFloat(newFormData[0].months[i].value) * (valuePercentage / 100),
                    porcentagem: valuePercentage
                };
                newFormData[indexOne].sub[indexTow].sub[indexTree].percentage = valuePercentage;
                return month.value = parseFloat(newFormData[0].months[i].value) * (valuePercentage / 100);
            }

            if (type === _type_calc.VALUE_PERCENTAGE) {
                lastNumber = verifySignal(i, lastNumber, value, valuePercentage);
                dataApply[i] = {
                    id: month.id,
                    tabela: month.table,
                    valor: lastNumber,
                    porcentagem: 0
                };
                newFormData[indexOne].sub[indexTow].sub[indexTree].percentage = 0;
                return month.value = lastNumber
            }

        }, []);

        // Api.updateRequest(url, dataApply).then(response => {
        //     debugger
        //     dispatch(_actions.saveFormData(newFormData));
        //     toastAlert('Valores alterados com sucesso!', 'success');
        // }).catch(response => {
        //     debugger
        // });

        try {
            await Api.postRequestJson(url, dataApply);
            dispatch(_actions.saveFormData(newFormData));
            toastAlert('Valores alterados com sucesso!', 'success');
        } catch (e) {
            toastAlert('Valores alterados com sucesso!', 'error');
        }
    }

    const applyMultipleValuesFour = async (indexOne, indexTow, indexTree,indexFour, type, value, valuePercentage) => {
        let newFormData = [...formData];
        let lastNumber = type === _type_calc.VALUE_PERCENTAGE ? parseFloat(value[0]) : parseFloat(value);
        newFormData[indexOne].sub[indexTow].sub[indexTree].sub[indexFour].type_calc = type;
        newFormData[indexOne].sub[indexTow].sub[indexTree].sub[indexFour].porcentage = type === _type_calc.VALUE_PERCENTAGE ? parseFloat(value[1]) : 0;

        newFormData[indexOne].sub[indexTow].sub[indexTree].sub[indexFour].months.map((month, i) => {
            if (type === _type_calc.VALUE) {
                dataApply[i] = {
                    id: month.id,
                    tabela: month.table,
                    valor: value,
                    porcentagem: 0
                };
                newFormData[indexOne].sub[indexTow].sub[indexTree].sub[indexFour].percentage = 0;
                return month.value = value;
            }

            if (type === _type_calc.PERCENTAGE) {
                dataApply[i] = {
                    id: month.id,
                    tabela: month.table,
                    valor: parseFloat(newFormData[0].months[i].value) * (valuePercentage / 100),
                    porcentagem: valuePercentage
                };
                newFormData[indexOne].sub[indexTow].sub[indexTree].sub[indexFour].percentage = valuePercentage;
                return month.value = parseFloat(newFormData[0].months[i].value) * (valuePercentage / 100);
            }

            if (type === _type_calc.VALUE_PERCENTAGE) {
                lastNumber = verifySignal(i, lastNumber, value, valuePercentage);
                dataApply[i] = {
                    id: month.id,
                    tabela: month.table,
                    valor: lastNumber,
                    porcentagem: 0
                };
                newFormData[indexOne].sub[indexTow].sub[indexTree].sub[indexFour].percentage = 0;
                return month.value = lastNumber
            }

        }, []);

        // Api.updateRequest(url, dataApply).then(response => {
        //     debugger
        //     dispatch(_actions.saveFormData(newFormData));
        //     toastAlert('Valores alterados com sucesso!', 'success');
        // }).catch(response => {
        //     debugger
        // });

        try {
            await Api.postRequestJson(url, dataApply);
            dispatch(_actions.saveFormData(newFormData));
            toastAlert('Valores alterados com sucesso!', 'success');
        } catch (e) {
            toastAlert('Valores alterados com sucesso!', 'error');
        }
    }

    return {applyMultipleValuesOne, applyMultipleValuesTow, applyMultipleValuesTree,applyMultipleValuesFour};
}

export default useApplyMultipleValues;