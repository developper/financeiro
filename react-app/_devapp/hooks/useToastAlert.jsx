import React from "react";
import {cssTransition, toast} from "react-toastify";

const useToastAlert = () => {
    const Zoom = cssTransition({
        enter: 'zoomIn',
        exit: 'zoomOut',
        duration: [500, 600],
        appendPosition: false
    });

    const toastAlert = (message, type) => {
        toast(message, {
            position: "top-right",
            autoClose: 3000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            type: type,
            delay: 700,
            transition: Zoom
        });
    }

    return { toastAlert }
}

export default useToastAlert