import React from "react";

const useAmountDataParams = () => {

    const amountDataParams = (props, value, index, percentage) => {
        return {
            id: props.formData.months[index].id,
            porcentagem: percentage,
            tabela: props.formData.months[index].table,
            valor: value
        }
    }

    return { amountDataParams };
}

export default useAmountDataParams;