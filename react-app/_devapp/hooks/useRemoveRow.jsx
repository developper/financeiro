import React from "react";
import {MODULES} from "../constants";
import {useDispatch, useSelector} from "react-redux";
import {Api} from "../services/config/request";
import * as _actions from "../store/actions-reducers";
import useToastAlert from "./useToastAlert";

const useRemoveRow = () => {
    const service = `${MODULES.FINANCEIROS}/orcamento-financeiro/?action=orcamento-financeiro-delete-itens`;
    const formData = useSelector(state => state.formData)
    const accountPlanRedux = useSelector(state => state.accountPlan);
    let newFormData = [...formData];

    const dispatch = useDispatch();
    const {toastAlert} = useToastAlert();

    let dataToRemove = [];

    const removeRow = async (level, idPlanAccount) => {
        if(level.length === 4) {
            newFormData[level[0]].sub[level[1]].sub[level[2]].sub[level[3]].months.map((month, i) => {
                dataToRemove[i] = {
                    id: month.id,
                    tabela: month.table,
                };
            }, []);
            newFormData[level[0]].sub[level[1]].sub[level[2]].sub.splice(level[3], 1); 
        } else if (level.length === 3) {
            newFormData[level[0]].sub[level[1]].sub[level[2]].months.map((month, i) => {
                dataToRemove[i] = {
                    id: month.id,
                    tabela: month.table,
                };
            }, []);
            newFormData[level[0]].sub[level[1]].sub.splice(level[2], 1);

        } else if (level.length === 2) {
            newFormData[level[0]].sub[level[1]].months.map((month, i) => {
                dataToRemove[i] = {
                    id: month.id,
                    tabela: month.table,
                };
            }, []);
            newFormData[level[0]].sub.splice(level[1], 1);
        }

        try {
            await Api.postRequestJson(service, dataToRemove);
            dispatch(_actions.saveFormData(newFormData))
            dispatch(_actions.saveAccountPlan(accountPlanRedux.filter((ac) => ac !== idPlanAccount)))
            toastAlert('Plano de conta removido com sucesso!', 'success');
        } catch (e) {
            toastAlert('Houve um erro ao deletar plano de conta!', 'error');
        }

    }

    return {removeRow}
}

export default useRemoveRow