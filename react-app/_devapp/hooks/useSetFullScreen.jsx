const useSetFull = () => {

    const setFull = () => {
        return window.REACT_APP.isFullScreen = !window.REACT_APP.isFullScreen;
    }

    return { setFull }
}

export default useSetFull;