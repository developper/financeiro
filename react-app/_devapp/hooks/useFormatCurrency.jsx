import React from "react";

const useFormatCurrency = () => {
    const formatCurrency = (value, isRemoveTheDecimalPlace) => {
        
        const tornedToPisitive = value >= 0 ? value : value * -1;
        const moneyNotSimbol = parseFloat(tornedToPisitive).toLocaleString('pt-br', {minimumFractionDigits: 2});
        const arrayCurrency = moneyNotSimbol.split(',');

        if (isRemoveTheDecimalPlace) {
            return arrayCurrency[0];
        } else {
            return moneyNotSimbol;
        }
    };

    return { formatCurrency };
}

export default useFormatCurrency;