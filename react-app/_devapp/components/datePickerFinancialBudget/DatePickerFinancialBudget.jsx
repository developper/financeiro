import React, {useEffect, useState, memo} from "react";
import DatePicker from "react-date-picker";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import './DatePickerFinancialBudget.scss';
import * as moment from "moment";
import 'moment/locale/pt-br';
import {Api} from "../../services/config/request";
import {MODULES} from "../../constants";
import * as _actions from '../../store/actions-reducers';
import {useDispatch,useSelector} from "react-redux";
import useAddKeyInFormData from '../../hooks/useAddKeyInFormData'

moment.locale('pt-br');

const DatePickerFinancialBudget = props => {
     
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(moment(startDate).add(365, 'days').toDate());
    const [minDate, setMinDate] = useState(moment().toDate());
    const [maxDate, setMaxDate] = useState(moment(minDate).add(365, 'days').toDate());
    const URL = `${MODULES.FINANCEIROS}/orcamento-financeiro/?action=gerar`;
    const URL_VIEW_EDIT = `${MODULES.FINANCEIROS}/orcamento-financeiro/?action=orcamento-financeiro-visualizar-editar`;
    const dispatch = useDispatch();
    const [idEditView, setIdEditView] = useState(false);
    const [isGenerate, setIsGenerate] = useState(false);
    const [typeViewEdit, setTypeViewEdit] = useState(false);
    const { addKeyInFormData } = useAddKeyInFormData();
    const monthsRedux = useSelector(state => state.months);

    const _handleChangeMaxDate = (date) => {
        setStartDate(date);
    }

    const _handleGenerate = () => {
        const startMonth = moment(startDate).format('YYYY-MM');
        const endMonth = moment(endDate).format('YYYY-MM');
        dispatch(_actions.changeLoading(true));
        Api.postRequest(URL, {inicio: startMonth, fim: endMonth}).then(response => {
            dispatch(_actions.saveFormData(addKeyInFormData(response.data.items)));
            dispatch(_actions.saveMonths(response.data.months));
            dispatch(_actions.saveIdBudget(response.data.id_budget));
            dispatch(_actions.saveDate({date_start: startMonth, date_end: endMonth}));
            dispatch(_actions.saveAccountPlan(response.data.chart_of_accounts_add));
            dispatch(_actions.changeLoading(false));
            setIsGenerate(true);
        })
    };

    const alterDate = (date) => {
        if (date) {
            setEndDate(moment(startDate).add(365, 'days').toDate())
            setMaxDate(moment(startDate).add(365, 'days').toDate());
        } else {
            setEndDate(date);
            setMaxDate(new Date())
        }
    }

    const verifyUrl = () => {
        const url = window.location.href.split('&')
            console.log(url)
        setIdEditView(url.length === 3 ? url[1].replace('id=', '') : null);
        setTypeViewEdit(url.length === 3 ? url[2].replace('type=', '') : null);
        setIsGenerate(url.length === 3);
    }

    const getBudgetById = (id, type) => {
        dispatch(_actions.changeLoading(true));
        Api.getRequest(`${URL_VIEW_EDIT}&id=${id}&type=${type}`).then(response => {
            console.log(response)
            dispatch(_actions.saveFormData(addKeyInFormData(response.data.items)));
            dispatch(_actions.saveMonths(response.data.months));
            dispatch(_actions.saveIdBudget(response.data.id_budget));
            dispatch(_actions.changeLoading(false));
            dispatch(_actions.saveDate({date_start: response.data.begin_date, date_end: response.data.end_date}));
            dispatch(_actions.saveTypeAction(response.data.type));
            dispatch(_actions.saveAccountPlan(response.data.chart_of_accounts_add));
        })
    }

    useEffect(() => {
        verifyUrl();

        if (idEditView && typeViewEdit) {

            getBudgetById(idEditView, typeViewEdit === 'visualizar' ? 'visualizar': 'editar');
        }

    }, [idEditView, typeViewEdit]);

    useEffect(() => {

        alterDate(startDate);
    }, [startDate]);

 

    return (
        <>
         
            <Form>
            {!idEditView && !isGenerate ?
                <div className="calendar">
                    <div className="calendar__date-start calendar__date">
                        <div className="calendar__date-start-label calendar__date-label">
                            <label>Mês de Início</label>
                        </div>
                        <div className="calendar__date-start-input">
                        <DatePicker
                                className="calendar_input"
                                onChange={(value) => _handleChangeMaxDate(value)}
                                value={startDate}
                                format={'MM/y'}
                                placeholder={'Mês inicial'}
                                monthArialLabel={'Month'}
                                locale={'pt-br'}
                                view={'month'}
                                defaultView={'month'}
                                minDetail="month"

                            />
                        </div>
                    </div>
                    <div className="calendar__date-end calendar__date">
                        <div className="calendar__date-end-label calendar__date-label">
                            <label>Mês final</label>
                        </div>
                        <div className="calendar__date-end-input">
                        <DatePicker
                                className="calendar_input"
                                onChange={(value) => setEndDate(value)}
                                value={endDate}
                                format={'MM/y'}
                                placeholder={'Mês Final'}
                                locale={'pt-br'}
                                view={'month'}
                                defaultView={'month'}
                                maxDate={maxDate}
                            />
                        </div>
                    </div>                    
                        <div className="calendar__button">
                            <Button
                                type={'button'}
                                variant="primary"
                                className="calendar__button-setting"
                                size="lg"
                                onClick={event => _handleGenerate()}
                                disabled={!startDate || !endDate}
                            >
                                Gerar
                            </Button>
                        </div>
                       
                </div>
                 :
                 <div id="show_calendar">
                     <div id="startDate">
                         <h5>Mês de Inicio</h5>
                         {monthsRedux.length > 0 ? <label>{`${monthsRedux[0].month} / ${monthsRedux[0].year}`}</label> : ''}
                     </div>
                     <div id="endDate">
                         <h5>Mês de Fim</h5>
                         {monthsRedux.length > 0 ? <label>{`${monthsRedux[monthsRedux.length -1].month} / ${monthsRedux[monthsRedux.length -1].year}`}</label> : ''}
                     </div>              
                 </div> 
                 }
            </Form>
        </>
    );
}

export default memo(DatePickerFinancialBudget);