import React, { useEffect, useState } from "react";
import Form from "react-bootstrap/Form";
import * as _actions from '../../store/actions-reducers';
import './FormFinancialBudget.scss'
import { useDispatch, useSelector } from "react-redux";
import useApplyMultipleValues from "../../hooks/useApplyMultipleValues";
import useAddCellRow from "../../hooks/useAddCellRow";
import { TYPE_CALC as _type } from "../../constants";
import { MODULES } from "../../constants";
import { Api } from '../../services/config/request';
import 'react-bootstrap-typeahead/css/Typeahead.css'
import Select from 'react-select';
import IntlCurrencyInput from "react-intl-currency-input"

const currencyConfig = {
    locale: "pt-BR",
    formats: {
        number: {
            BRL: {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
            },
        },
    },
};


const FormFinancialBudget = props => {
    const dispatch = useDispatch();
    const level = useSelector(state => state.level);
    const showFieldBudget = useSelector(state => state.showFieldBudget)
    const accountPlanRedux = useSelector(state => state.accountPlan)
    const { applyMultipleValuesOne, applyMultipleValuesTow, applyMultipleValuesTree, applyMultipleValuesFour } = useApplyMultipleValues();
    const { addCellRow } = useAddCellRow()

    const [valueInput, setValueInput] = useState(0);
    const [percentageInput, setPercentageInput] = useState(0);
    const [signal, setSignal] = useState("+");
    const [budgets, setBudgets] = useState([]);
    const [accountPlan, setAccountPlan] = useState({});

    const [typeCalc, setTypeCalc] = useState(_type.VALUE);

    const baseUrl = `${MODULES.FINANCEIROS}/orcamento-financeiro/?action=listar-naturezas`;

    const _handleShowForm = (event) => {
        event.preventDefault();
        dispatch(_actions.showFormFinancial(false))
    };

    const _applyValuesInCell = (event) => {
        event.preventDefault();
        if (showFieldBudget) {
            addCellRow(level, accountPlan);
            dispatch(_actions.showFormFinancial(false));
            dispatch(_actions.saveLevelCell(null));
            dispatch(_actions.saveTableName(null));
            return false;
        }

        const checkInputAndGetValue = checkInputTypeAndGetValue();

        if (level.length === 1)
            applyMultipleValuesOne(level[0], typeCalc, checkInputAndGetValue, parseFloat(percentageInput));
        if (level.length === 2)
            applyMultipleValuesTow(level[0], level[1], typeCalc, checkInputAndGetValue, parseFloat(percentageInput));
        if (level.length === 3)
            applyMultipleValuesTree(level[0], level[1], level[2], typeCalc, checkInputAndGetValue, parseFloat(percentageInput));
        if (level.length === 4)
            applyMultipleValuesFour(level[0], level[1], level[2], level[3], typeCalc, checkInputAndGetValue, parseFloat(percentageInput));
        dispatch(_actions.showFormFinancial(false));
        dispatch(_actions.saveLevelCell(null));
    }

    const checkInputTypeAndGetValue = () => {
        if (typeCalc === _type.VALUE) {
            return valueInput;
        } else if (typeCalc === _type.PERCENTAGE) {
            return percentageInput;
        } else if (typeCalc === _type.VALUE_PERCENTAGE) {
            return [valueInput, percentageInput, signal];
        }
    };

    const _handleCheckRadio = (event) => {
        setTypeCalc(event.target.value);
    }

    const handleFocus = event => event.target.select();


    const _handleChange = (value) => {
        setAccountPlan(value)
    }

    const handleChangeValue = (event, value, maskedValue) => {
        event.preventDefault();
        setValueInput(value);
    };

    useEffect(() => {
        if (showFieldBudget) {
            Api.getRequest(baseUrl).then(response => {
                setBudgets(response.data.filter(budget => !accountPlanRedux.includes(budget.id)).map(i => ({
                    value: i.id,
                    label: i.name
                })));
            });
        }

    }, []);

    return (
        <>

            <div className="modal-financial-table-input-value">

                {showFieldBudget ?
                    <>

                        <div className={'modal-table-input-value__select-title'}>
                            <div className="modal-table-input-value__label modal-form__label">
                                <label htmlFor="modal-value">Orçamento</label>
                            </div>
                            <div className="modal-table-input-value_field modal-form__value">
                                <Select
                                    value={accountPlan}
                                    onChange={(event) => _handleChange(event)}
                                    options={budgets}
                                    placeholder={"Digite o nome do plano de conta"}
                                />
                            </div>

                        </div>
                    </>
                    :
                    ''
                }

                {!showFieldBudget ?
                    <>
                        <div className="modal-table-input-value__label-field modal-form">
                            <div className="modal-table-input-value__label modal-form__label">
                                <div className="span-block-radio-button">
                                    <Form.Check
                                        type="radio"
                                        id="modal-value"
                                        name="modal-value"
                                        value={_type.VALUE}
                                        onChange={event => _handleCheckRadio(event)}
                                        checked={typeCalc === _type.VALUE}
                                    />


                                </div>
                                <div className="span-block-label">
                                    <label htmlFor="modal-value">Valor</label>
                                </div>
                            </div>
                            <div className="modal-table-input-value_field modal-form__value">
                                <span>

                                    <IntlCurrencyInput
                                        currency="BRL"
                                        disabled={typeCalc !== _type.VALUE}
                                        config={currencyConfig}
                                        className={'cell-entry-field__input form-control'}
                                        onFocus={handleFocus}
                                        autoFocus={true}
                                        name="value"
                                        onChange={handleChangeValue}
                                        value={valueInput}
                                        onFocus={handleFocus}
                                    />
                                </span>
                            </div>
                        </div>
                        {level.length !== 1 || level[0] !== 0 ?
                            <div className="modal-table-input-perc_label modal-form">
                                <div className="modal-table-input-value__perc-label modal-form__label">
                                    <div className="span-block-radio-button">
                                        <Form.Check
                                            type="radio"
                                            id="modal-perc"
                                            name="modal-value"
                                            value={_type.PERCENTAGE}
                                            onChange={event => _handleCheckRadio(event)}
                                        />
                                    </div>
                                    <div className="span-block-label">
                                        <label htmlFor="modal-perc">Percentual</label>
                                    </div>
                                </div>
                                <div className="modal-table-input-value__perc-field modal-form__value">
                                    <span>
                                        <Form.Control
                                            type="text"
                                            placeholder="Porcentual"
                                            disabled={typeCalc !== _type.PERCENTAGE}
                                            onChange={event => setPercentageInput(event.target.value)}
                                            value={percentageInput}
                                            onFocus={handleFocus}
                                            name={'percentage'}
                                        />
                                    </span>
                                </div>
                            </div>
                            : ''}

                        <div className="modal-table-input-value-perc modal-form">
                            <div className="modal-table-input-value-perc__label modal-form__label">
                                <div className="span-block-radio-button">

                                    <Form.Check
                                        type="radio"
                                        id="modal-parc-value"
                                        name="modal-value"
                                        value={_type.VALUE_PERCENTAGE}
                                        onChange={event => _handleCheckRadio(event)}
                                    />

                                </div>
                                <div className="span-block-label">
                                    <label htmlFor="modal-parc-value">Valor/Percentual</label>
                                </div>
                            </div>
                            <div className="modal-table-input-value-perc__field modal-form__value">

                                <span className="form__field-value-perc">

                                    <IntlCurrencyInput
                                        currency="BRL"
                                        disabled={typeCalc !== _type.VALUE_PERCENTAGE}
                                        config={currencyConfig}
                                        className={'cell-entry-field__input form-control'}
                                        onFocus={handleFocus}
                                        autoFocus={true}
                                        name="value"
                                        onChange={handleChangeValue}
                                        value={valueInput}
                                        onFocus={handleFocus}
                                    />
                                </span>
                                <span className="form__field-perc">
                                    <Form.Control
                                        as="select"
                                        disabled={typeCalc !== _type.VALUE_PERCENTAGE}
                                        onChange={event => setSignal(event.target.value)}
                                        name={'signal'}
                                    >
                                        <option value={'+'}>+</option>
                                        <option value={'-'}>-</option>
                                    </Form.Control>
                                </span>
                                <span className="form__field-value">
                                    <Form.Control
                                        type="text"
                                        placeholder="%"
                                        disabled={typeCalc !== _type.VALUE_PERCENTAGE}
                                        onChange={event => setPercentageInput(event.target.value)}
                                        value={percentageInput}
                                        onFocus={handleFocus}
                                        name={'value-percentage'}
                                    />
                                </span>

                            </div>
                        </div>
                    </>
                    : ''}


                <div className="modal-table-button">
                    <button
                        className="modal-table-button__btn-cancel square-button square-button-blue"
                        onClick={(event) => _handleShowForm(event)}
                    >
                        Cancelar
                    </button>

                    <button
                        className="modal-table-button__btn square-button square-button-blue"
                        onClick={event => _applyValuesInCell(event)}
                    >
                        Incluir
                    </button>
                </div>
            </div>

        </>
    );
};

export default FormFinancialBudget;