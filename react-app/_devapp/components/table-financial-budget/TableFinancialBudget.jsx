import React, { useEffect, useState, memo } from "react";
import { useDispatch, useSelector } from "react-redux";
import Table from "react-bootstrap/Table";
import SubLevelLine from "./sub-level-line/SubLevelLine";
import './TableFinancialBudget.scss';
import SubLevelLineTow from "./sub-level-line/SubLevelLineTwo";
import SubLevelLineTree from "./sub-level-line/SubLevelLineTree";
import SubLevelLineFour from "./sub-level-line/SubLevelLineFour"
import Header from "../header/Header";
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from 'react-toastify';
import Loading from "../loading/Loading";
import { MONTHS } from '../../constants'
import useSetFull from "../../hooks/useSetFullScreen";
import GroupButtons from "../group-buttons/GroupButtons";

const TableFinancialBudget = props => {

    const formData = useSelector(state => state.formData);
    const monthsRedux = useSelector(state => state.months);
    const showFormFinancial = useSelector(state => state.showFormFinancial);
    const isLoading = useSelector(state => state.isLoading);
    const { setFull } = useSetFull();
    const [fullScreen, setFullScreen] = useState(false);



    const openFullScreen = () => {
        setFullScreen(setFull());
    }
    
    useEffect(()=>{
        document.title=document.querySelector('#right').children[0].childNodes[0].innerHTML;
    },[])
    return (
        <>
            <ToastContainer
            />
            
            <Header
                showFormFinancial={showFormFinancial}
            />

            <div id="outerdiv" className={`table-financial-budget ${fullScreen ? 'full-screen' : 'not-full-screen'}`}>
                
                    {!isLoading && formData.length > 0 ?
                    
                        <Table bordered hover>
                            <thead>
                                <tr id="FinancialTableHead">
                                    
                                    <th id="FinancialTableHeadButtons" className="first-column tr-th-header headcol first-column-level-one" colSpan={2}>
                                        <div><GroupButtons openFullScreen={openFullScreen} /></div>
                                        <div>%</div>
                                    </th>
                                    {monthsRedux.length > 0 ? monthsRedux.map((m, i) =>
                                        <th id="FinancialTableHeadMonth" key={`thead-${i}-${m}`}>{MONTHS[m.month]}/{m.year}</th>
                                    ) : ''}
                                    <th>TOTAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                {formData.map((om, indexObj) =>
                                    <>
                                    
                                        <SubLevelLine
                                            key={`tbody-level-1-${indexObj}`}
                                            name={om.name}
                                            months={om.months}
                                            value={om.value}
                                            levelToClass={'1'}
                                            index={indexObj}
                                            formData={om}
                                            percentage={props.percentage}
                                        />

                                        {om.sub.length > 0 ?
                                            om.sub.map((omSub, omIndex) =>
                                                <>
                                                    <SubLevelLineTow
                                                        key={`tbody-level-2-${indexObj}-${omIndex}`}
                                                        name={omSub.name}
                                                        months={omSub.months}
                                                        value={omSub.value}
                                                        levelToClass={'2'}
                                                        oneIndex={indexObj}
                                                        index={omIndex}
                                                        formData={omSub}
                                                        percentage={props.percentage}
                                                        dataOne={om}
                                                    />
                                                    {omSub.sub.length > 0 ?
                                                        omSub.sub.map((omSubSub, omSubSubIndex) =>
                                                            <>
                                                           
                                                                <SubLevelLineTree
                                                                    key={`tbody-level-3-${indexObj}-${omIndex}-${omSubSubIndex}`}
                                                                    name={omSubSub.name}
                                                                    months={omSubSub.months}
                                                                    value={omSubSub.value}
                                                                    levelToClass={'3'}
                                                                    oneIndex={indexObj}
                                                                    towIndex={omIndex}
                                                                    index={omSubSubIndex}
                                                                    formData={omSubSub}
                                                                    percentage={props.percentage}
                                                                    dataTrow={omSub}
                                                                />
                                                                {omSubSub.sub.length > 0 ?
                                                                
                                                                    omSubSub.sub.map((omSubSubSub, omSubSubSubIndex) =>
                                                                    <>
                                                                        <SubLevelLineFour
                                                                            key={`tbody-level-4-${indexObj}-${omIndex}-${omSubSubIndex}-${omSubSubSubIndex}`}
                                                                            name={omSubSubSub.name}
                                                                            months={omSubSubSub.months}
                                                                            value={omSubSubSub.value}
                                                                            levelToClass={'4'}
                                                                            oneIndex={indexObj}
                                                                            threeIndex={omSubSubIndex}
                                                                            towIndex={omIndex}
                                                                            index={omSubSubSubIndex}
                                                                            formData={omSubSubSub}
                                                                            percentage={props.percentage}
                                                                            dataFour={omSubSub}
                                                                        />
                                                                        </>
                                                                    )
                                                                    : null}
                                                            </>
                                                        )
                                                        :
                                                        null
                                                    }
                                                </>
                                            )
                                            :
                                            null
                                        }
                                    </>
                                )}
                            </tbody>
                        </Table>
                        : null}
                </div>

                {isLoading ? <Loading /> : null}

          
        </>
    );
}

export default memo(TableFinancialBudget);