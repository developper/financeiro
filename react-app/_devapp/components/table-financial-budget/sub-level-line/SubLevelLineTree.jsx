import React, { memo, useState } from 'react';
import './SubLevelLine.scss';
import CellEntryField from "./cell-entry-field/CellEntryField";
import Cell from "./cell-entry-field/cell/Cell";
import { useDispatch, useSelector } from "react-redux";
import * as _actions from '../../../store/actions-reducers';
import useSaveCell from "../../../hooks/useSaveCell";
import useFormatCurrency from "../../../hooks/useFormatCurrency";
import useFirstCharacterIsNumber from "../../../hooks/useFirstCharacterIsNumber";
import useRemoveCaracteres from "../../../hooks/useRemoveCaracteres";

const SubLevelLineTree = props => {

    const formData = useSelector(state => state.formData);
    const dispatch = useDispatch();
    const typeAction = useSelector(state => state.typeAction);
    const { saveCellTree, saveCellPercentageTree } = useSaveCell();
    const { formatCurrency } = useFormatCurrency();
    const { firstCharacterIsNumber } = useFirstCharacterIsNumber();
    const { removeCaracteres } = useRemoveCaracteres();


    const onChangeCell = (index) => {
        let newFormData = [...formData];

        newFormData.map((nfd, i) => {
            nfd.months.map((month) => {
                month.showCell = false;
            })

            nfd.sub.map((omSub, omSubIndex) => {
                omSub.months.map((omS) => {
                    omS.showCell = false;
                })

                omSub.sub.map((omSubSub, omSubSubIndex) => {
                    omSubSub.months.map((omSs, mIndex) => {
                        omSs.showCell =
                            i === props.oneIndex &&
                            omSubIndex === props.towIndex &&
                            omSubSubIndex === props.index &&
                            mIndex === index &&
                            !omSs.showCell;
                    })
                    omSubSub.sub.map((omSubSubSub) => {
                        omSubSubSub.months.map((omSsS) => {
                            omSsS.showCell = false;
                        })    
                    })
                })
            })
        }, [])
        dispatch(_actions.saveTableName(props.months[index].table))
        dispatch(_actions.saveFormData(newFormData))
    }

    const onChangeCellPercentage = () => {
        let newFormData = [...formData];
        alert()
        newFormData.map((nfd, i) => {
            nfd.showInputPercentage = false;

            nfd.sub.map((omSub, omSubIndex) => {
                omSub.showInputPercentage = false;

                omSub.sub.map((omSubSub, omSubSubIndex) => {
                    omSubSub.showInputPercentage =
                        i === props.oneIndex &&
                        omSubIndex === props.towIndex &&
                        omSubSubIndex === props.index &&
                        !omSubSub.showInputPercentage;
                })
            })
        }, [])

        dispatch(_actions.saveFormData(newFormData))
    }

    const sum = (months) => months.reduce((total, month) => total + parseFloat(month.value), 0);

    const _handleKeyDown = (event, index) => {
        if (event.key === 'Enter') {
            saveCellTree(props.oneIndex, props.towIndex, props.index, index, removeCaracteres(event.target.value), props);
            dispatch(_actions.saveTableName(null));
        }
    }

    const _handleOnFocusout = (event, index) => {
        saveCellTree(props.oneIndex, props.towIndex, props.index, index, removeCaracteres(event.target.value), props);
        dispatch(_actions.saveTableName(null));
    }

    return (
        <tr className={"level-" + props.levelToClass + ` ${props.dataTrow.isShowRow ? '' : 'hidden-line'}`}>
            <Cell
                class={'cell-p'} value={props.name}
                showButton={true}
                showButtonPlus={true}
                showPercentage={true}
                levelCell={[props.oneIndex, props.towIndex, props.index]}
                isAddAccountPlan={firstCharacterIsNumber(props.name)}
                months={props.months}
                index={props.index}
                formData={props.formData}
                 colSpan={2} 
                percentage={props.formData.percentage}
                classTd={'headcol cell-one cell-tree'}
            />
            {props.months.length > 0 ? props.months.map((month, index) =>
                <td key={`level-${props.levelToClass}-${props.oneIndex}-${props.towIndex}-${index}`} onClick={(event => onChangeCell(index))}>
                    {
                        props.formData.months[index].showCell && (!typeAction || typeAction === 'edit')
                            ?
                            <CellEntryField
                                key={`cell-${props.levelToClass}-${props.oneIndex}-${props.towIndex}-${index}`}
                                onKeyDown={event => _handleKeyDown(event, index)}
                                value={month.value}
                                onFocusOut={event => _handleOnFocusout(event, index)}
                                indexMonth={index}
                                oneIndex={props.oneIndex}
                                towIndex={props.towIndex}
                                Index={props.index}
                                oneRow={false}
                                tableName={month.table}
                                formData={props.formData}
                            />
                            :
                            formatCurrency(month.value, false)
                    }
                </td>
            ) : ''}
            <Cell showButton={false} value={formatCurrency(sum(props.formData.months), false)} />
        </tr>
    );
}

export default memo(SubLevelLineTree);