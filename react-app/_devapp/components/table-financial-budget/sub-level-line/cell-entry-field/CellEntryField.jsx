import React from 'react';
import './CellEntryField.scss';

import IntlCurrencyInput from "react-intl-currency-input"

const currencyConfig = {
    locale: "pt-BR",
    formats: {
      number: {
        BRL: {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        },
      },
    },
  };

const CellEntryField = props => {

    const handleFocus = event => event.target.select();

    
    
    return (
        <div className="cell-entry-field">


            <IntlCurrencyInput
                currency="BRL" 
                config={currencyConfig}
                onKeyDown={props.onKeyDown}
                defaultValue={props.value}
                onBlur={props.onFocusOut}
                className={'cell-entry-field__input form-control'}
                onFocus={event => handleFocus(event)}
                autoFocus={true}
                name="valueMonth"
            />
        </div>
    );
};

export default CellEntryField;