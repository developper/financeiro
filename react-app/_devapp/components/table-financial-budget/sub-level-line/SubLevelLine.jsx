import React, { memo, useState } from 'react';
import './SubLevelLine.scss';
import CellEntryField from "./cell-entry-field/CellEntryField";
import Cell from "./cell-entry-field/cell/Cell";
import {useDispatch, useSelector} from "react-redux";
import * as _actions from "../../../store/actions-reducers";
import useSaveCell from "../../../hooks/useSaveCell";
import useRemoveCaracteres from "../../../hooks/useRemoveCaracteres";
import useFormatCurrency from "../../../hooks/useFormatCurrency";
import useFirstCharacterIsNumber from "../../../hooks/useFirstCharacterIsNumber";

const SubLevelLine = props => {
    const formData = useSelector(state => state.formData);
    const dispatch = useDispatch();
    const {saveCellOne, saveCellPercentageOne} = useSaveCell();
    const { formatCurrency } = useFormatCurrency();
    const { firstCharacterIsNumber } = useFirstCharacterIsNumber();
    const typeAction = useSelector(state => state.typeAction);
    const { removeCaracteres } = useRemoveCaracteres();

    const onChangeCell = (index) => {
        let newFormData = [...formData];

        newFormData.map((om, i) => {
            om.months.map((month, mIndex) => {
                month.showCell = i === props.index && index === mIndex && !month.showCell;
            })
            om.sub.map((omSub, onSubIndex) => {
                omSub.months.map((month, mIndex) => {
                    month.showCell = false;
                })

                omSub.sub.map((omSubSub, onSubSubIndex) => {
                    omSubSub.months.map((month, mIndex) => {
                        month.showCell = false;
                    })
                    omSubSub.sub.map((omSubSubSub) => {
                        omSubSubSub.months.map((omSsS) => {
                            omSsS.showCell = false;
                        })    
                    })
                })
            })
        }, [])

        dispatch(_actions.saveTableName(props.months[index].table))
        dispatch(_actions.saveFormData(newFormData))
    }

    const sum = (months) => months.reduce((total, month) => total + parseFloat(month.value), 0);


    const _handleKeyDown = (event, index) => {
        if (event.key === 'Enter') {
            saveCellOne(props.index, index, removeCaracteres(event.target.value), props);
            dispatch(_actions.saveTableName(null));
        }
    }

    const _handleOnFocusout = (event, index) => {
        saveCellOne(props.index, index, removeCaracteres(event.target.value), props);
        dispatch(_actions.saveTableName(null));
    }


    return (
        <tr className={`level-${props.levelToClass}_${props.formData.color_background} level-blue_${props.index}`}>
            <Cell
                showButton={true}
                showButtonPlus={true}
                showPercentage={true}
                levelCell={[props.index]}
                class={'cell-p'}
                value={props.name}
                isAddAccountPlan={firstCharacterIsNumber(props.name)}
                months={props.months}
                index={props.index}
                formData={props.formData}
                percentage={props.formData.percentage}
                classTd={`headcol cell-one cell-one_${props.index} cell-one_${props.formData.color_background}`}
            />
            {props.months && props.months.length > 0 ? props.months.map((month, index) =>
                <td key={`level-${props.levelToClass}-${index}`} onClick={(event => onChangeCell(index))}>
                    {
                        props.formData.months[index].showCell && (!typeAction || typeAction === 'edit')
                            ?
                            <CellEntryField
                                key={`cell-${props.levelToClass}-${index}`}
                                onKeyDown={event => _handleKeyDown(event, index)}
                                value={parseFloat(month.value)}
                                onFocusOut={event => _handleOnFocusout(event, index)}
                                oneRow={true}
                                tableName={month.table}
                                id={month.id}
                            />
                            :
                            formatCurrency(month.value, false)
                    }
                </td>
            ) : ''}
            <Cell showButton={false} value={formatCurrency(sum(props.formData.months), false)}/>
        </tr>
    );
}

export default memo(SubLevelLine);