import React, { useState, memo} from 'react';
import useSetFull from "../../hooks/useSetFullScreen";
import useHandleLevel from '../../hooks/useHandleLevel';
import './GroupButtons.scss';

const GroupButtons = memo(({ openFullScreen }) => {
    const {setFull} = useSetFull();
    const { handleAllLevelOne, handleAllLevelTow, handleAllLevelThree,handleAllLevelFour} = useHandleLevel();
    const [fullScreen, setFullScreen] = useState(false);

    return <>
        <button type="button" className={'report__button-full-screen'}
            onClick={() => openFullScreen()}>Tela cheia
                                        </button>
        <div className="btn-group">
            <button type="button" onClick={() => handleAllLevelOne()}
                className={'btn-level'}>1
                                            </button>
            <button type="button" onClick={() => handleAllLevelTow()}
                className={'btn-level'}>2
                                            </button>
            <button type="button" onClick={() => handleAllLevelThree()}
                className={'btn-level'}>3
                                            </button>
            <button type="button" onClick={() => handleAllLevelFour()}
                className={'btn-level'}>4
                                            </button>                           
        </div>
    </>
})

export default GroupButtons;