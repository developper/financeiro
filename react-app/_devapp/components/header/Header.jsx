import React, { memo } from 'react';
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import 'react-calendar/dist/Calendar.css';
import './Header.scss';
import DatePickerFinancialBudget from "../datePickerFinancialBudget/DatePickerFinancialBudget";
import ModalForm from "../modal-form/ModalForm";
import {useDispatch} from "react-redux";
import * as _actions from '../../store/actions-reducers'

export default memo(props => {

    const dispatch = useDispatch();

    const closeModal = () => {
        dispatch(_actions.showFormFinancial(false));
    }

    return (
        <Container fluid>
            <Row>
                <Col>
                    <div className="financial-header">
                        <div className="financial-header__form-date">
                            <DatePickerFinancialBudget/>
                        </div>
                    </div>
                </Col>
                <Col>
                    {<ModalForm show={props.showFormFinancial} onHide={() => closeModal()}/>}
                </Col>
            </Row>
        </Container>
    )
})