import React from "react";
import FormFinancialBudget from "../formFinancialBudget/FormFinancialBudget";
import './ModalForm.scss'

const ModalForm = (props) => {
    return (
        <>
            {
                props.show ?
                    <>
                        <div className="modal-form">
                            <div id="open-modal" className="modal-window">
                                <div>
                                    <a href="#" title="Close" onClick={props.onHide} className="modal-close">Fechar</a>
                                    <FormFinancialBudget/>
                                </div>
                            </div>
                        </div>
                        <div className="modal-form__bg"></div>
                    </>
                    : ''
            }
        </>
    );
}

export default ModalForm;