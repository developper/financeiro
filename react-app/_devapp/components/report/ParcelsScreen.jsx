import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal';
import Table from "react-bootstrap/Table";
import Excelfile from './ExcelFile'
import './ParcelsScreen.scss'


export default function ParcelsScreen(props) {
  const [show, setShow] = useState(false)
  const parcel = Array.from(props.data)
  const reportType = props.type
  const titleDate = reportType === 'relatorio' ? 'Data Pg.' :  'Competência';
  ;

  const formatCurrency = (value) => {
    let formatter = new Intl.NumberFormat([], {
      style: 'currency',
      currency: 'BRL'
    })
    return formatter.format(value);
  }
    return (
     <>
     <button style={{color:'blue',marginLeft:10,padding:0,border:'none',backgroundColor:'transparent',outline:'none',display:`${props.visibility}`}} className="fa fa-info-circle" onClick={(e) => setShow(true)} >
     {/* <FontAwesomeIcon style={{color:'blue',marginLeft:'5px',marginRight:0,visibility:`${props.visibility}`}} icon={faBookmark}></FontAwesomeIcon> */}
     </button>
     
          <Modal
            size="lg"
            show={show}
            onHide={() => setShow(false)} 
            backdropClassName="static"
            centered
            animation={false}
            scrollable
          >
           <Modal.Header closeButton>
             <Modal.Title >
               Parcelas
             </Modal.Title>
           </Modal.Header>
           <Modal.Body>
             <Excelfile data={parcel} type={reportType}/>
            <Table bordered >
             <thead>
               <tr style={{position:'sticky',top:0}}>
                 <th className="head">TR</th>
                 <th className="head">Nº nota</th>
                 <th className="head">Fornecedor</th>
                 <th className="head">{titleDate}</th>
                 <th className="head">Valor</th>
               </tr>
               {parcel.map((item,index) => {
                 return(
                  <tr>
                  <td>{item.tr}</td>
                  <td>{item.numero_nota}</td>
                  <td>{item.fornecedor}</td>
                  <td id="date">{item.data}</td>
                  <td>{formatCurrency(Number(item.valor))}</td>
                </tr>
                 )
                 
               } )}
             </thead>
            </Table>
           </Modal.Body> 
          </Modal>      
     </>
    )
}