import React, {useState, useEffect} from "react";
import Table from "react-bootstrap/Table";
import {MODULES, MONTHS} from "../../constants";
import './Report.scss';
import {useDispatch, useSelector} from "react-redux";
import * as _actions from "../../store/actions-reducers";
import ButtonShowLine from "../table-financial-budget/sub-level-line/button-show-line/ButtonShowLine"
import useAddKeyInFormData from "../../hooks/useAddKeyInFormData";
import {Api} from "../../services/config/request";
import Loading from "../loading/Loading";
import useFormatCurrency from "../../hooks/useFormatCurrency";
import GroupButtons from '../group-buttons/GroupButtons';
import useSetFull from "../../hooks/useSetFullScreen";
import {SelectAll} from '../formFinancialReport/FormFinancialReport'
import ParcelsScreen from './ParcelsScreen';
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";

const Report = props => {

    const [idEditView, setIdEditView] = useState(false);
    const [isGenerate, setIsGenerate] = useState(false);
    const [typeViewEdit, setTypeViewEdit] = useState(false);
    const [selected, setSelected] = useState([]);
    const [availityType,setAvalityType] = useState("");
    const formData = useSelector(state => state.formData);
    const branches = useSelector(state => state.branches);
    const monthsRedux = useSelector(state => state.months);
    const isLoading = useSelector(state => state.isLoading);
    const dispatch = useDispatch();
    const {addKeyInFormData} = useAddKeyInFormData();
    const {formatCurrency} = useFormatCurrency();
    const { setFull } = useSetFull();
    const [fullScreen, setFullScreen] = useState(false);
    const [pageTitle,setPageTitle] = useState("")
 
    
    const URL_REPORT = `${MODULES.FINANCEIROS}/orcamento-financeiro/?action=buscar-relatorio-orcamento-financeiro`;
    const URL_REPORT_GERENCIAL = `${MODULES.FINANCEIROS}/orcamento-financeiro/?action=buscar-relatorio-gerencial-orcamento-financeiro`;
    const typeCol = typeViewEdit == 'relatorio' ? 'Caixa' : 'Gerencial'

    const openFullScreen = () => {
        
        setFullScreen(setFull());
    }
    

    const _handleShowHiddenLineOne = (indexOne) => {
        let newFormData = [...formData];
        newFormData[indexOne].isShowRow = !newFormData[indexOne].isShowRow;
        newFormData[indexOne].sub.map(item => {
            item.sub.map(item2 => {
                if (!newFormData[indexOne].isShowRow)
                item2.isShowRow = false;
                item.isShowRow = false;
            })
           
        }
        );

        dispatch(_actions.saveFormData(newFormData));
    }

    const _handleShowHiddenLineTow = (indexOne, indexTow) => {
        let newFormData = [...formData];
        newFormData[indexOne].sub[indexTow].isShowRow = !newFormData[indexOne].sub[indexTow].isShowRow;
        newFormData[indexOne].sub[indexTow].sub.map(item => {
            item.isShowRow = false;
        })
        dispatch(_actions.saveFormData(newFormData));
    }

    const _handleShowHiddenLineThree = (indexOne,indexTow,indexThree) => {
        let newFormData = [...formData];
        newFormData[indexOne].sub[indexTow].sub[indexThree].isShowRow = !newFormData[indexOne].sub[indexTow].sub[indexThree].isShowRow;
        dispatch(_actions.saveFormData(newFormData));

    }

    const verifyUrl = () => {
        const url = window.location.href.split('&')

        setIdEditView(url.length === 3 ? url[1].replace('id=', '') : null);
        setTypeViewEdit(url.length === 3 ? url[2].replace('type=', '') : null);
        setIsGenerate(url.length === 3);
    }


    const getBudgetByIdAndFilial = (id, type, idFilial) => {
        
        const url = !idFilial ? `${URL_REPORT}&id=${id}&type=${type}` : `${URL_REPORT}&id=${id}&type=${type}&unidade=${idFilial}`
   
        if(type == 'relatorio-gerencial'){
           const  url = `${URL_REPORT_GERENCIAL}&id=${id}&type=${type}` 

        }
        
           
        dispatch(_actions.changeLoading(true));
        Api.getRequest(url).then(response => {
            console.log(response)
            setAvalityType(availityType => response.data.availityType);
            dispatch(_actions.saveFormData(addKeyInFormData(response.data.items)));
            dispatch(_actions.saveMonths(response.data.months));
            dispatch(_actions.saveIdBudget(response.data.id_budget));
            dispatch(_actions.changeLoading(false));
            dispatch(_actions.saveDate({date_start: response.data.begin_date, date_end: response.data.end_date}));
            dispatch(_actions.saveTypeAction(response.data.type));
            dispatch(_actions.saveAccountPlan(response.data.chart_of_accounts_add));
            dispatch(_actions.saveBranches(response.data.units));
        })
    }

    const _handleChangeFilial = (value) => {
       value = selected.map(valor =>{return valor.value});
    
        getBudgetByIdAndFilial(idEditView, typeViewEdit, value);
    }
  
    useEffect(() => {
        document.title=document.querySelector('#right').children[0].childNodes[0].innerHTML;
        setPageTitle(document.querySelector('#right').children[0].childNodes[0].innerHTML);
        verifyUrl();

        if (idEditView && typeViewEdit) {
           
            getBudgetByIdAndFilial(idEditView, typeViewEdit, null);
           
        }

    }, [idEditView, typeViewEdit]);

    const sumOfRealized = (months) => months.reduce((total, month) => total + parseFloat(month.realizado), 0);
    const sumOfProjected = (months) => months.reduce((total, month) => total + parseFloat(month.value), 0);

    const sumOfAH = (designed, realized ,AV) => {
        
        //   if (designed > 0)
        //     return ((realized / designed) * 100 - 100).toFixed(2) + '%';
        //   else
        //    return 0+'%'; 
        return '';
            
    };

    const sumOfAV = (designed, realized ,AV) => {       
            return !!AV ? AV.toFixed(2)+'%' : AV;        
    };

    const applyClassColorTotal = (color, value, AH) => {
        if (AH) {
            return  'black';
        }

        if (color === "") {
            return value >= 0 ? 'blue' : 'red'
        } else {
            return color;
        }
    }

    return <>
          
        {branches.length > 0 && typeViewEdit =='relatorio' ?
            <div className="select__filial">
                <div className="filial__child">
              <Form>
                            <Form.Group>
                                <Form.Label>Selecione a filial que deseja exibir o relatório</Form.Label>
                                  
                                <SelectAll
                                    className='MultiSelect'
                                    onChange={setSelected} 
                                    value={selected}
                                    options={branches.map(branch =>{return {value:branch.id,label:branch.name}})}
                                />
                            </Form.Group>
                        </Form>
                               <button 
                                    type='button'
                                    onClick={_handleChangeFilial}
                                    className='selectButtom'>
                                      Pesquisar
                               </button>
                               </div>
            </div>
            :
            ''
        }


        {isLoading ? <Loading/> :
            <>
                {formData.length > 0 ?
                    <div id="outerdivReport"  className={`report ${fullScreen ? 'full-screen' : 'not-full-screen'}`}>
                        {/* <div id="innerdiv"> */}
                        
                            <Table bordered  className="responsive">
                                <thead>
                                <tr className="report__header-column-1">
                                    <th colSpan={3} className="report__head-th-1 headcol">
                                        <GroupButtons openFullScreen={openFullScreen}  />
                                    </th>
                                    
                                    {monthsRedux.length > 0 ? monthsRedux.map((m, i) =>
                                        <th colSpan={4} key={`thead-${i + 22}-${m.id}`} className="monthHeader">{MONTHS[m.month]}/{m.year}</th>
                                    ) : ''}
                                    <th colSpan={4} className="monthHeader">Total</th>
                                </tr>
                                <tr className="report__header-column-2">
                                    <th colSpan={3} className="report__head-th-2 headcol">
                                      {pageTitle}
                                    </th>
                                    {monthsRedux.length > 0 ? monthsRedux.map((m, i) =>
                                        <>
                                            <th className="actionHeader" key={`thead-${i + 1}-${m.id}`}>Orçado</th>
                                            <th className="actionHeader" key={`thead-${i + 2}-${m.id}`}>{typeCol}</th>
                                            <th className="actionHeader" key={`thead-${i + 3}-${m.id}`}>&Delta;H%</th>
                                            <th className="actionHeader" key={`thead-${i + 4}-${m.id}`}>&Delta;V%</th>
                                        </>
                                    ) : ''}
                                    <th className="actionHeader">Orçado</th>
                                    <th className="actionHeader">{typeCol}</th>
                                    <th className="actionHeader">&Delta;H%</th>
                                    <th className="actionHeader">&Delta;V%</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    formData.map((item, levelOneIndex) =>
                                        <>
                                            <tr key={`${item.name}_${item.id_topico}_${levelOneIndex}`}
                                                className={`report__level-one_${item.color_background} body-tr body-row-${levelOneIndex + 1} line_${levelOneIndex}`}>
                                                <td colSpan={3}
                                                 id="dropDonwTableMenu"
                                                    className={`body-tr-td body-row-${levelOneIndex + 1}-cell-1 headcol headcol__one_${item.color_background} headcol__one_${levelOneIndex}`}>
                                                    <div className={`head__col-box`}>
                                                        <div>
                                                            {item.sub.length > 0 ? <ButtonShowLine
                                                                showRow={item.isShowRow}
                                                                showHiddenTable={() => _handleShowHiddenLineOne(levelOneIndex)}/> : ''}
                                                        </div>
                                                        <div className="head__col-name">{item.name}</div>

                                                    </div>
                                                </td>
                                                {item.months.map((month, i) =>
                                                    <>
                                                        <td
                                                            className={`body-tr-td body-row-${levelOneIndex + i}-cell-${i + 4}`}
                                                            key={`level-one-${levelOneIndex + month.id}`}
                                                        >
                                                    <span
                                                        className={`color__${month.color}`}>{formatCurrency(month.value, true)}</span>
                                                        </td>
                                                        <td
                                                            className={`body-tr-td body-row-${levelOneIndex + i}-cell-${i + 5}`}
                                                        >
                                                            <div style={{display:'flex',justifyContent:'space-between'}}>
                                                    <span
                                                        className={`color__${month.color_realized}`}>{formatCurrency(month.realizado, true)}</span>
                                                        {month.parcelas !== null  ? <ParcelsScreen data={month.parcelas || []}  visibility={(month.parcelas === null ||Number(month.realizado)===0) ? 'none' : 'block'} type={`${typeViewEdit}`} />: ''} 
                                                        </div>
                                                        </td>
                                                        <td
                                                            className={`body-tr-td body-row-${levelOneIndex + i}-cell-${i + 6}`}
                                                        >
                                                            <span
                                                                className={`color__${month.AH_color}`}>{!!month.AH  ? `${month.AH}%` : "" }</span>
                                                        </td>
                                                        <td
                                                            className={`body-tr-td body-row-${levelOneIndex + i}-cell-${i + 7}`}
                                                        >
                                                            <span
                                                                className={`color__${month.AV_color}`}>{!!month.AV  ? `${month.AV}%` : "" }</span>
                                                        </td>
                                                    </>
                                                )}
                                                <td
                                                    className={`body-tr-td body-row-${levelOneIndex}-cell-${levelOneIndex + 8}`}
                                                >
                                            <span
                                                className={`color__${applyClassColorTotal(item.color, sumOfProjected(item.months), false)}`}>{formatCurrency(sumOfProjected(item.months), true)}</span>
                                                </td>
                                                <td
                                                    className={`body-tr-td body-row-${levelOneIndex}-cell-${levelOneIndex + 9}`}
                                                >
                                            <span
                                                className={`color__${applyClassColorTotal(item.color_realized, sumOfRealized(item.months), false)}`}>{formatCurrency(sumOfRealized(item.months), true)}</span>
                                                </td>
                                                <td
                                                    className={`body-tr-td body-row-${levelOneIndex}-cell-${levelOneIndex + 10}`}
                                                >
                                            <span
                                                className={`color__${applyClassColorTotal(
                                                    null,
                                                    sumOfAH(
                                                        sumOfProjected(item.months),
                                                        sumOfRealized(item.months),
                                                        item.total ? item.total.AH :""
                                                    ),
                                                    true
                                                )}`}
                                            >
                                                {sumOfAH(
                                                    sumOfProjected(item.months),
                                                    sumOfRealized(item.months),
                                                    item.total ? item.total.AH : ""
                                                )}
                                            </span>
                                                </td>
                                                <td
                                                    className={`body-tr-td body-row-${levelOneIndex}-cell-${levelOneIndex + 11}`}
                                                >
                                            <span
                                                className={`color__${applyClassColorTotal(
                                                    null,
                                                    sumOfAV(
                                                        sumOfProjected(item.months),
                                                        sumOfRealized(item.months),
                                                        item.total ? item.total.AV :""
                                                    ),
                                                    true
                                                )}`}
                                            >                                                
                                                {sumOfAV(
                                                    sumOfProjected(item.months),
                                                    sumOfRealized(item.months),
                                                    item.total ? item.total.AV :""
                                                )}
                                            </span>
                                                </td>
                                            </tr>
                                            {item.sub.length > 0 ? item.sub.map((levelTow, levelTowIndex) =>
                                                    <>
                                                        <tr key={`${levelTow.name}_${item.id_topico}_${levelTowIndex}`}
                                                            className={`report__level-tow body-tr body-row-${levelOneIndex + levelTowIndex + 1} ${item.isShowRow ? '' : 'hidden-line'}`}>
                                                            <td colSpan={3}
                                                            id="dropDonwTableMenu"
                                                                className={`body-tr-td body-row-${levelOneIndex + 1 + levelTowIndex}-cell-1 headcol headcol__tow`}>
                                                                <div className={`head__col-box`}>
                                                                    <div>
                                                                        {levelTow.sub.length > 0 ? <ButtonShowLine
                                                                            showRow={levelTow.isShowRow}
                                                                            showHiddenTable={() => _handleShowHiddenLineTow(levelOneIndex, levelTowIndex)}/> : ''}
                                                                    </div>
                                                                    <div className="head__col-name">{levelTow.name}</div>
                                                                </div>
                                                            </td>
                                                            {levelTow.months.map(month =>
                                                                    <>
                                                                        <td key={`level-tow-${levelOneIndex + levelTowIndex + month.id}`}>
                                                            <span
                                                                className={`color__${month.color}`}>{formatCurrency(month.value, true)}</span>
                                                                        </td>
                                                                        <td>
                                                                       
                                                                    <div style={{display:'flex',justifyContent:'space-between'}}>
                                                            <span
                                                                className={`color__${month.color_realized}`}>{formatCurrency(month.realizado, true)}</span>
                                                                
                                                            {month.parcelas !== null  ? <ParcelsScreen data={month.parcelas || []}  visibility={(month.parcelas === null ||Number(month.realizado)===0) ? 'none' : 'block'} type={`${typeViewEdit}`} />: ''} </div>
                                                                        </td>
                                                                        <td>
                                                            <span
                                                                className={`color__${month.AH_color}`}>{!!month.AH  ? `${month.AH}%` : "" }</span>
                                                                        </td>
                                                                        <td>
                                                            <span
                                                                className={`color__${month.AV_color}`}>{!!month.AV  ? `${month.AV}%` : "" }</span>
                                                                        </td>
                                                                    </>
                                                            )}
                                                            
                                                            <td>
                                                    <span
                                                        className={`color__${applyClassColorTotal(levelTow.color, sumOfProjected(levelTow.months), false)}`}>{formatCurrency(sumOfProjected(levelTow.months))}</span>
                                                            </td>
                                                            <td>
                                                    <span
                                                        className={`color__${applyClassColorTotal(levelTow.color_realized, sumOfRealized(levelTow.months), false)}`}>{formatCurrency(sumOfRealized(levelTow.months))}</span>
                                                            </td>
                                                            <td>
                                                   <span
                                                       className={`color__${applyClassColorTotal(
                                                           null,
                                                           sumOfAH(
                                                               sumOfProjected(levelTow.months),
                                                               sumOfRealized(levelTow.months),
                                                               levelTow.total ? levelTow.total.AH :""
                                                               ),
                                                           true
                                                       )}`}
                                                   >
                                                {sumOfAH(
                                                    sumOfProjected(levelTow.months),
                                                    sumOfRealized(levelTow.months),
                                                    levelTow.total ? levelTow.total.AH :""
                                                )}
                                            </span>
                                                            </td>
                                                            <td>
                                                   <span
                                                       className={`color__${applyClassColorTotal(
                                                           null,
                                                           sumOfAV(
                                                               sumOfProjected(levelTow.months),
                                                               sumOfRealized(levelTow.months),
                                                               levelTow.total ? levelTow.total.AV :""
                                                               ),
                                                           true
                                                       )}`}
                                                   >
                                                {sumOfAV(
                                                    sumOfProjected(levelTow.months),
                                                    sumOfRealized(levelTow.months),
                                                    levelTow.total ? levelTow.total.AV :""
                                                )}
                                            </span>
                                                            </td>
                                                        </tr>
                                                        {levelTow.sub.length > 0 ? levelTow.sub.map((levelTree, levelTreeIndex) =>
                                                           <>
                                                                <tr key={`${levelTree.name}_${item.id_topico}_${levelTowIndex}_${levelTreeIndex}`}
                                                            className={`report__level-tree body-tr body-row-${levelOneIndex + levelTowIndex + levelTreeIndex + 1} ${levelTow.isShowRow ? '' : 'hidden-line'}`}>
                                                            <td colSpan={3}
                                                            id="dropDonwTableMenu"
                                                                className={`body-tr-td body-row-${levelOneIndex + 1 + levelTowIndex + levelTreeIndex}-cell-1 headcol headcol__tree`}>
                                                                <div className={`head__col-box`}>
                                                                    <div>
                                                                        {levelTree.sub.length > 0 ? <ButtonShowLine
                                                                            showRow={levelTree.isShowRow}
                                                                            showHiddenTable={() => _handleShowHiddenLineThree(levelOneIndex, levelTowIndex,levelTreeIndex)}/> : ''}
                                                                    </div>
                                                                    <div className="head__col-name">{levelTree.name}</div>
                                                                </div>
                                                            </td>
                                                            {levelTree.months.map(month =>
                                                                    <>
                                                                        <td key={`level-tow-${levelOneIndex + levelTowIndex + levelTreeIndex + month.id}`}>
                                                            <span
                                                                className={`color__${month.color}`}>{formatCurrency(month.value, true)}</span>
                                                                        </td>
                                                                        <td >
                                                                            <div style={{display:'flex',justifyContent:'space-between'}}>
                                                            <span
                                                                className={`color__${month.color_realized}`}>{formatCurrency(month.realizado, true)}</span>
                                                               {month.parcelas !== null  ? <ParcelsScreen data={month.parcelas || []}  visibility={(month.parcelas === null ||Number(month.realizado)===0) ? 'none' : 'block'} type={`${typeViewEdit}`} />: ''} </div>
                                                                        </td>
                                                                        <td>
                                                            <span
                                                                className={`color__${month.AH_color}`}>{!!month.AH  ? `${month.AH}%` : "" }</span>
                                                                        </td>
                                                                        <td>
                                                            <span
                                                                className={`color__${month.AV_color}`}>{!!month.AV  ? `${month.AV}%` : "" }</span>
                                                                        </td>
                                                                    </>
                                                            )}
                                                            <td>
                                                    <span
                                                        className={`color__${applyClassColorTotal(levelTree.color, sumOfProjected(levelTree.months), false)}`}>{formatCurrency(sumOfProjected(levelTree.months))}</span>
                                                            </td>
                                                            <td>
                                                    <span
                                                        className={`color__${applyClassColorTotal(levelTree.color_realized, sumOfRealized(levelTree.months), false)}`}>{formatCurrency(sumOfRealized(levelTree.months))}</span>
                                                            </td>
                                                            <td>
                                                   <span
                                                       className={`color__${applyClassColorTotal(
                                                           null,
                                                           sumOfAH(
                                                               sumOfProjected(levelTree.months),
                                                               sumOfRealized(levelTree.months),
                                                               levelTree.total ? levelTree.total.AH :""
                                                               ),                                                              
                                                           true
                                                       )}`}
                                                   >
                                                {sumOfAH(
                                                    sumOfProjected(levelTree.months),
                                                    sumOfRealized(levelTree.months),
                                                    levelTree.total ? levelTree.total.AH :""
                                                )}
                                            </span>
                                                            </td>
                                                            <td>
                                                   <span
                                                       className={`color__${applyClassColorTotal(
                                                           null,
                                                           sumOfAV(
                                                               sumOfProjected(levelTree.months),
                                                               sumOfRealized(levelTree.months),
                                                               levelTree.total ? levelTree.total.AV :""
                                                               ),                                                              
                                                           true
                                                       )}`}
                                                   >
                                                {sumOfAV(
                                                    sumOfProjected(levelTree.months),
                                                    sumOfRealized(levelTree.months),
                                                    levelTree.total ? levelTree.total.AV :""
                                                )}
                                            </span>
                                                            </td>
                                                        </tr>
                                                                {levelTree.sub.length > 0 ? levelTree.sub.map((levelFour, levelFourIndex) =>
                                                                  <tr key={`${levelFour.name}_${item.id_topico}_${levelTowIndex}_${levelTreeIndex}_${levelFourIndex}`}
                                                                  className={`report report__level-four body-tr body-row-${levelOneIndex + levelTowIndex + levelTreeIndex + levelFourIndex + 1} ${levelTree.isShowRow ? '' : 'hidden-line'}`}>

                                                                  <td colSpan={3}
                                                                  id="dropDonwTableMenu"
                                                                      className={`body-tr-td body-row-level-four body-row-${levelOneIndex + 1 + levelTowIndex + levelTowIndex + levelFourIndex}-cell-1 headcol headcol__four`}>
                                                                      <div className={`head__col-box`}>
                                                                          <div></div>
                                                                          <div
                                                                              className="head__col-name">{levelFour.name}</div>
                                                                      </div>
                                                                  </td>
                                                                  {levelFour.months.map(month =>
                                                                          <>
                                                                              <td key={`level-four-${levelOneIndex + levelTowIndex + levelTreeIndex + levelFourIndex + month.id}`}>
                                                              <span
                                                                  className={`color__${month.color}`}>{formatCurrency(month.value, true)}</span>
                                                                              </td>
                                                                              <td>
                                                                                  <div style={{display:'flex',justifyContent:'space-between'}}>
                                                              <span
                                                                  className={`color__${month.color_realized}`}>{formatCurrency(month.realizado, true)}</span>
                                                                   {month.parcelas !== null  ? <ParcelsScreen data={month.parcelas || []}  visibility={(month.parcelas === null ||Number(month.realizado)===0) ? 'none' : 'block'} type={`${typeViewEdit}`} />: ''} 
                                                                  </div>
                                                                               
                                                                              </td>
                                                                              <td>
                                                              <span
                                                                  className={`color__${month.AH_color}`}>{!!month.AH  ? `${month.AH}%` : "" }</span>
                                                                              </td>
                                                                              <td>
                                                              <span
                                                                  className={`color__${month.AV_color}`}>{!!month.AV  ? `${month.AV}%` : "" }</span>
                                                                              </td>

                                                                          </>
                                                                  )}
                                                                  <td>
                                                          <span
                                                              className={`color__${applyClassColorTotal(levelFour.color, sumOfProjected(levelFour.months), false)}`}>{formatCurrency(sumOfProjected(levelFour.months), true)}</span>
                                                                  </td>
                                                                  <td>
                                                          <span
                                                              className={`color__${applyClassColorTotal(levelFour.color_realized, sumOfRealized(levelFour.months), false)}`}>{formatCurrency(sumOfRealized(levelFour.months), true)}</span>
                                                                  </td>
                                                                  <td>
                                                      <span
                                                          className={`color__${applyClassColorTotal(
                                                              null,
                                                              sumOfAH(
                                                                  sumOfProjected(levelFour.months),
                                                                  sumOfRealized(levelFour.months),
                                                                  levelFour.total ? levelFour.total.AH :"",
                                                                  ),                                                               
                                                              true
                                                          )}`}
                                                      >
                                              {sumOfAH(
                                                  sumOfProjected(levelFour.months),
                                                  sumOfRealized(levelFour.months),
                                                  levelFour.total ? levelFour.total.AH :""
                                              )}
                                          </span>
                                                                  </td>
                                                                  <td>
                                                      <span
                                                          className={`color__${applyClassColorTotal(
                                                              null,
                                                              sumOfAV(
                                                                  sumOfProjected(levelFour.months),
                                                                  sumOfRealized(levelFour.months),
                                                                  levelFour.total ? levelFour.total.AV :"",
                                                                  ),                                                               
                                                              true
                                                          )}`}
                                                      >
                                              {sumOfAV(
                                                  sumOfProjected(levelFour.months),
                                                  sumOfRealized(levelFour.months),
                                                  levelFour.total ? levelFour.total.AV :""
                                              )}
                                          </span>
                                                                  </td>
                                                              </tr>                                                           ) : ''}
                                                            </>
                                                        ) : ''}
                                                    </>
                                            ) : ''}
                                        </>
                                    )
                                }
                                </tbody>
                            </Table>
                        {/* </div> */}
                    </div>
                    :
                    ''
                }
            </>
        }
    </>
}


export default Report;