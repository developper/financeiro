import React from 'react'
import './ExcelFile.scss'


export default function Excelfile (props) {

    function generateCSV () {
        const data =props.data;
        const reportType = props.type
        const titleDate = reportType === 'relatorio' ? 'Data Pg.' :  'Competência';
        var csv = `TR,Nº nota,Fornecedor  ,${titleDate},Valor\n`;
        data.forEach(function(obj) {
            csv += Object.values(obj).join(',');
            csv += "\n";
        });
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_blank';
        hiddenElement.download = 'parcelas.csv';
        hiddenElement.click();
    }

    return (
        <button id="export" onClick={() => generateCSV()}>Exportar CSV</button> 
    )
}