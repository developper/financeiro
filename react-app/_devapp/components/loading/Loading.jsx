import React from "react";
import Spinner from "react-bootstrap/Spinner";
import './Loading.scss';

const Loading = () => <div className={"loading"}>
    <Spinner animation="border" role="status">
        <span className="sr-only">Loading...</span>
    </Spinner>
    <br/>
    <h4 className="component-load__text">Carregando as informações...</h4>
</div>

export default Loading