import React, { useEffect, useState, memo } from "react";
import { useDispatch, useSelector } from "react-redux";
import Table from "react-bootstrap/Table";
import SubLevelLine from "./sub-level-line/SubLevelLine";
import './ManagerialValues.scss';
import SubLevelLineTow from "./sub-level-line/SubLevelLineTwo";
import SubLevelLineTree from "./sub-level-line/SubLevelLineTree";
import SubLevelLineFour from "./sub-level-line/SubLevelLineFour"
import * as _actions from "../../store/actions-reducers"
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from 'react-toastify';
import Loading from "../loading/Loading";
import { MONTHS } from '../../constants'
import useSetFull from "../../hooks/useSetFullScreen";
import GroupButtons from "../group-buttons/GroupButtons";
import {Api} from '../../services/config/request'
import {MODULES} from '../../constants'
import useAddKeyInFormData from '../../hooks/useAddKeyInFormData'

const ManagerialValues = props => {

    const formData = useSelector(state => state.formData);
    const monthsRedux = useSelector(state => state.months);
    const isLoading = useSelector(state => state.isLoading);
    const { addKeyInFormData } = useAddKeyInFormData();
    const { setFull } = useSetFull();
    const [fullScreen, setFullScreen] = useState(false);
    const [idEditView, setIdEditView] = useState(false);
    const [typeViewEdit, setTypeViewEdit] = useState(false);
    const dispatch = useDispatch();
    const URL = `${MODULES.FINANCEIROS}/orcamento-financeiro/?action=cadastrar-valores-gerenciais-editar`;

    const openFullScreen = () => {
        setFullScreen(setFull());
    }

    const verifyUrl = () => {
        const url = window.location.href.split('&')
        console.log("URL",url)
        setIdEditView(url.length === 3 ? url[1].replace('id=', '') : null);
        setTypeViewEdit(url.length === 3 ? url[2].replace('type=', '') : null);
       // setIsGenerate(url.length === 3);
    }

    const getBudgetById = (id,type) => {
        dispatch(_actions.changeLoading(true));
        console.log(`URL INTEIRA : ${URL}&id=${id}`)
        Api.getRequest(`${URL}&id=${id}&type=${type}`).then(response => {
            console.log(response)
            dispatch(_actions.saveFormData(addKeyInFormData(response.data.items)));
            dispatch(_actions.saveMonths(response.data.months));
            dispatch(_actions.saveIdBudget(response.data.id_budget));
            dispatch(_actions.changeLoading(false));
            dispatch(_actions.saveDate({date_start: response.data.begin_date, date_end: response.data.end_date}));
            dispatch(_actions.saveTypeAction(response.data.type));
            dispatch(_actions.saveAccountPlan(response.data.chart_of_accounts_add)); 
           
        })
    }

    useEffect(() => {
        document.title=document.querySelector('#right').children[0].childNodes[0].innerHTML;
        verifyUrl();

        if (idEditView && typeViewEdit) {

            getBudgetById(idEditView, typeViewEdit === 'visualizar' ? 'visualizar': 'editar');
        }

    }, [idEditView, typeViewEdit]);

    return(
      <>
        <ToastContainer
            />
        <div id="show_calendar">
                     <div id="startDate">
                         <h5>Mês de Inicio</h5>
                         {monthsRedux.length > 0 ? <label>{`${monthsRedux[0].month} / ${monthsRedux[0].year}`}</label> : ''}
                     </div>
                     <div id="endDate">
                         <h5>Mês de Fim</h5>
                         {monthsRedux.length > 0 ? <label>{`${monthsRedux[monthsRedux.length -1].month} / ${monthsRedux[monthsRedux.length -1].year}`}</label> : ''}
                     </div>              
        </div>
        
        <div id="containerDiv" className={`table-managerial-values ${fullScreen ? 'full-screen' : 'not-full-screen'}`}>
               
                    {!isLoading && formData.length > 0 ?
                    
                        <Table bordered hover className="mvtable">
                            <thead>
                                <tr id="MVTableHead">
                                    {console.log(formData)}
                                    <th id="MvTableHeadButtons" className="mvfirst-column tr-th-mvheader mvheadcol mvfirst-column-level-one mv" colSpan={2}>
                                        <div><GroupButtons openFullScreen={openFullScreen} /></div>
                                        
                                    </th>
                                    {monthsRedux.length > 0 ? monthsRedux.map((m, i) =>
                                        <th id="MvTableHeadMonth" key={`mvthead-${i}-${m}`}>{MONTHS[m.month]}/{m.year}</th>
                                    ) : null}
                                    
                                </tr>
                            </thead>
                            <tbody>
                                {formData.map((om, indexObj) =>
                                    <>
                                        <SubLevelLine
                                            key={`tbody-level-1-${indexObj}`}
                                            name={om.name}
                                            months={om.months}
                                            value={om.value}
                                            levelToClass={'1'}
                                            index={indexObj}
                                            formData={om}
                                            percentage={props.percentage}
                                        />

                                        {om.sub.length > 0 ?
                                            om.sub.map((omSub, omIndex) =>
                                                <>
                                                    <SubLevelLineTow
                                                        key={`tbody-level-2-${indexObj}-${omIndex}`}
                                                        name={omSub.name}
                                                        months={omSub.months}
                                                        value={omSub.value}
                                                        levelToClass={'2'}
                                                        oneIndex={indexObj}
                                                        index={omIndex}
                                                        formData={omSub}
                                                        percentage={props.percentage}
                                                        dataOne={om}
                                                    />
                                                    {omSub.sub.length > 0 ?
                                                        omSub.sub.map((omSubSub, omSubSubIndex) =>
                                                            <>
                                                           
                                                                <SubLevelLineTree
                                                                    key={`tbody-level-3-${indexObj}-${omIndex}-${omSubSubIndex}`}
                                                                    name={omSubSub.name}
                                                                    months={omSubSub.months}
                                                                    value={omSubSub.value}
                                                                    levelToClass={'3'}
                                                                    oneIndex={indexObj}
                                                                    towIndex={omIndex}
                                                                    index={omSubSubIndex}
                                                                    formData={omSubSub}
                                                                    percentage={props.percentage}
                                                                    dataTrow={omSub}
                                                                />
                                                                {omSubSub.sub.length > 0 ?
                                                                
                                                                    omSubSub.sub.map((omSubSubSub, omSubSubSubIndex) =>
                                                                    <>
                                                                        <SubLevelLineFour
                                                                            key={`tbody-level-4-${indexObj}-${omIndex}-${omSubSubIndex}-${omSubSubSubIndex}`}
                                                                            name={omSubSubSub.name}
                                                                            months={omSubSubSub.months}
                                                                            value={omSubSubSub.value}
                                                                            levelToClass={'4'}
                                                                            oneIndex={indexObj}
                                                                            threeIndex={omSubSubIndex}
                                                                            towIndex={omIndex}
                                                                            index={omSubSubSubIndex}
                                                                            formData={omSubSubSub}
                                                                            percentage={props.percentage}
                                                                            dataFour={omSubSub}
                                                                        />
                                                                        </>
                                                                    )
                                                                    : null}
                                                            </>
                                                        )
                                                        :
                                                        null
                                                    }
                                                </>
                                            )
                                            :
                                            null
                                        }
                                    </>
                                )}
                            </tbody>
                        </Table>
                        : null}
               

                {isLoading ? <Loading /> : null}

            </div> 
      </>
    )
 
}

export default memo(ManagerialValues);