import React from 'react';
import './Cell.scss';
import {useDispatch, useSelector} from "react-redux";
import * as _actions from '../../../../../store/actions-reducers';
import useRemoveRow from "../../../../../hooks/useRemoveRow";
import ButtonShowLine from "../../button-show-line/ButtonShowLine";

const Cell = props => {

    const dispatch = useDispatch();
    const formData = useSelector(state => state.formData);
    const typeAction = useSelector(state => state.typeAction);
    const { removeRow } = useRemoveRow();

    const _handleShowForm = () => {
        dispatch(_actions.saveLevelCell(props.levelCell));
        dispatch(_actions.showFormFinancial(true));
        dispatch(_actions.saveTableName(null));
        dispatch(_actions.showFieldBudget(false));

    };

    const _handlePlus = () => {
     
        dispatch(_actions.saveLevelCell(props.levelCell));
        dispatch(_actions.showFormFinancial(true));
        dispatch(_actions.saveTableName(props.months[0].table));
        dispatch(_actions.showFieldBudget(true));
    }

    const _handleRemoveRow = () => {
        removeRow(props.levelCell, props.formData.id_plano_contas)
        dispatch(_actions.showFieldBudget(false));
        dispatch(_actions.showFormFinancial(false));
    }

    const _handleShowHiddenLine = (levelCell) => {
        let newFormData = [...formData];
        if (levelCell.length === 2) {
            newFormData[levelCell[0]].sub[levelCell[1]].isShowRow = !newFormData[levelCell[0]].sub[levelCell[1]].isShowRow;
        }else if(levelCell.length === 3) {
            newFormData[levelCell[0]].sub[levelCell[1]].sub[levelCell[2]].isShowRow = !newFormData[levelCell[0]].sub[levelCell[1]].sub[levelCell[2]].isShowRow;
        } else {
            newFormData[levelCell[0]].isShowRow = !newFormData[levelCell[0]].isShowRow;
            newFormData[levelCell[0]].sub.map(item => {
                if (!newFormData[levelCell[0]].isShowRow)
                    item.isShowRow = false;
            });
        }

        dispatch(_actions.saveFormData(newFormData));
    }

    const _handleShowRow = (levelCell) => {
        if(levelCell.length === 1) {
            return formData[levelCell[0]].isShowRow;
        }else if(levelCell.length === 2) {
            return formData[levelCell[0]].sub[levelCell[1]].isShowRow;
        }else if(levelCell.length === 3) {
            return formData[levelCell[0]].sub[levelCell[1]].sub[levelCell[2]].isShowRow;
        }

    }

    return (
        <td colSpan={props.colSpan ? props.colSpan : 2} className={props.classTd}>
            <div className={'td__btn'}>
               {/*  { console.log("LEVEL CELL:",props.levelCell)} */}
                {(props.levelCell && (props.levelCell.length === 1 || props.levelCell.length === 2 || props.levelCell.length === 3)) ?
                    props.formData.sub.length > 0 ?
                        <ButtonShowLine
                            showRow={() =>_handleShowRow(props.levelCell)}
                            showHiddenTable={() => _handleShowHiddenLine(props.levelCell)}
                        />
                        : ''
                    :
                    ''
                }
            </div>
            <div className={'cell'}>
                <span className={props.class}>{props.value}</span>
                {props.showButton ?
                    <span className={'cell__button'}>
                        {props.showButtonPlus && !props.isAddAccountPlan && (!typeAction || typeAction === 'edit') ?
                            <button type="button" onClick={() => _handlePlus()}
                                    className={'cell-button__btn-plus'}>+</button> : ''}
                        {props.isAddAccountPlan && (!typeAction || typeAction === 'edit') ?
                            <button type="button" onClick={() => _handleRemoveRow()}
                                    className={'cell-button__btn-remove'}>-</button> : ''}
                        {!typeAction || typeAction === 'edit' ?
                            <button onClick={event => _handleShowForm()} type="button"
                                    className={'cell-button__btn'}>$</button>
                            : ''}
                    </span>
                    : ''
                }
            </div>
            <div className={'td__percentage'}>
                {props.showPercentage ?
                    <span className="sub-percentage">{`${parseFloat(props.percentage)}%`}</span>
                    : ''
                }
            </div>

        </td>
    )
}

export default Cell;