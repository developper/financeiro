import React from 'react';
import './CellEntryField.scss';



const currencyConfig = {
    locale: "pt-BR",
    formats: {
      number: {
        BRL: {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        },
      },
    },
  };

const CellEntryField = props => {

    const handleFocus = event => event.target.select();

    
    
    return (
        <div className="cell-entry-field">
          <input 
          type='number'
          name="valueMonth"
          autoFocus={true}
          onFocus={event => handleFocus(event)}
          className={'cell-entry-field__input form-control'}
          onBlur={props.onFocusOut}
          defaultValue={props.value}
          onKeyDown={props.onKeyDown}
          >

          </input>
          
        </div>
    );
};

export default CellEntryField;