import React, { memo, useState } from 'react';
import './SubLevelLine.scss';
import CellEntryField from "./cell-entry-field/CellEntryField";
import Cell from "./cell-entry-field/cell/Cell";
import {useDispatch, useSelector} from "react-redux";
import * as _actions from "../../../store/actions-reducers";
import useSaveCellManagerialValues from "../../../hooks/useSaveCellManagerialValues";
import useFormatGerencialCurrency from "../../../hooks/useFormatGerencialCurrency";
import useFirstCharacterIsNumber from "../../../hooks/useFirstCharacterIsNumber";
import useApplyTextColor from "../../../hooks/useApplyTextColor"

const SubLevelLine = props => {
    const formData = useSelector(state => state.formData);
    const dispatch = useDispatch();
    const {saveCellOne} = useSaveCellManagerialValues();
    const { formatCurrency } = useFormatGerencialCurrency();
    const { firstCharacterIsNumber } = useFirstCharacterIsNumber();
    const { textColor } = useApplyTextColor();
    const typeAction = useSelector(state => state.typeAction);
    

    const onChangeCell = (index) => {
        let newFormData = [...formData];

        newFormData.map((om, i) => {
            om.months.map((month, mIndex) => {
                month.showCell = i === props.index && index === mIndex && !month.showCell;
            })
            om.sub.map((omSub, onSubIndex) => {
                omSub.months.map((month, mIndex) => {
                    month.showCell = false;
                })

                omSub.sub.map((omSubSub, onSubSubIndex) => {
                    omSubSub.months.map((month, mIndex) => {
                        month.showCell = false;
                    })
                    omSubSub.sub.map((omSubSubSub) => {
                        omSubSubSub.months.map((omSsS) => {
                            omSsS.showCell = false;
                        })    
                    })
                })
            })
        }, [])

        dispatch(_actions.saveTableName(props.months[index].table))
        dispatch(_actions.saveFormData(newFormData))
    }

    


    const _handleKeyDown = (event, index) => {
        if (event.key === 'Enter') {
            saveCellOne(props.index, index, event.target.value, props);
            dispatch(_actions.saveTableName(null));
        }
    }

    const _handleOnFocusout = (event, index) => {
        saveCellOne(props.index, index, event.target.value, props);
        dispatch(_actions.saveTableName(null));
    }

    


    return (
        <tr className={`level-${props.levelToClass}_${props.formData.color_background} level-blue_${props.index}`}>
            <Cell
                showButton={true}
                showButtonPlus={true}
                showPercentage={false}
                levelCell={[props.index]}
                class={'cell-p'}
                value={props.name}
                isAddAccountPlan={firstCharacterIsNumber(props.name)}
                months={props.months}
                index={props.index}
                formData={props.formData}
                percentage={props.formData.percentage}
                classTd={`mvheadcol cell-one cell-one_${props.index} cell-one_${props.formData.color_background}`}
            />
            {props.months && props.months.length > 0 ? props.months.map((month, index) =>
                <td style={{color:textColor(month.value)}}  key={`level-${props.levelToClass}-${index}`} onClick={(event => onChangeCell(index))}>
                    {
                        props.formData.months[index].showCell && (!typeAction || typeAction === 'view')
                            ?
                            <CellEntryField
                            
                                key={`cell-${props.levelToClass}-${index}`}
                                onKeyDown={event => _handleKeyDown(event, index)}
                                value={parseFloat(month.value)}
                                onFocusOut={event => _handleOnFocusout(event, index)}
                                oneRow={true}
                                tableName={month.table}
                                id={month.id}
                            />
                            :
                            formatCurrency(month.value, false)
                    }
                </td>
            ) : ''}
           
        </tr>
    );
}

export default memo(SubLevelLine);