import React from "react";
import * as Icon from "react-bootstrap-icons"

const ButtonShowLine = props => {
    return (
        <div className={'button-show-line'}>
            <button onClick={props.showHiddenTable} type={"button"}>
                {props.showRow ? <Icon.ChevronUp /> : <Icon.ChevronDown />}
            </button>
        </div>
    )
}

export default ButtonShowLine;