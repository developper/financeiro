import React, { memo, useState } from 'react';
import './SubLevelLine.scss';
import CellEntryField from "./cell-entry-field/CellEntryField";
import Cell from "./cell-entry-field/cell/Cell";
import { useDispatch, useSelector } from "react-redux";
import * as _actions from '../../../store/actions-reducers';
import useSaveCellManagerialValues from "../../../hooks/useSaveCellManagerialValues";
import useFormatGerencialCurrency from "../../../hooks/useFormatGerencialCurrency";
import useFirstCharacterIsNumber from "../../../hooks/useFirstCharacterIsNumber";
import useApplyTextColor from "../../../hooks/useApplyTextColor"

const SubLevelLineFour = props => {
    const formData = useSelector(state => state.formData);
    const dispatch = useDispatch();
    const typeAction = useSelector(state => state.typeAction);
    const { saveCellFour, saveCellPercentageFour } = useSaveCellManagerialValues();
    const { formatCurrency } = useFormatGerencialCurrency();
    const { firstCharacterIsNumber } = useFirstCharacterIsNumber();
    const { textColor } = useApplyTextColor();
    

    const onChangeCell = index => {
        let newFormData = [...formData];

        newFormData.map((nfd, i) => {
            nfd.months.map((month) => {
                month.showCell = false;
            })

            nfd.sub.map((omSub, omSubIndex) => {
                omSub.months.map((omS) => {
                    omS.showCell = false;
                })

                omSub.sub.map((omSubSub, omSubSubIndex) => {
                    omSubSub.months.map((omSs) => {
                        omSs.showCell = false;
                           
                    })
                    omSubSub.sub.map((omSubSubSub,omSubSubSubIndex) => {
                        omSubSubSub.months.map((omSsS,mSIndex) => {
                            omSsS.showCell = 
                            i === props.oneIndex &&
                            omSubIndex === props.towIndex &&
                            omSubSubIndex === props.threeIndex &&
                            omSubSubSubIndex === props.index &&
                            mSIndex === index &&
                            !omSsS.showCell;
                        })    
                    })
                })
            })
        }, [])
        dispatch(_actions.saveTableName(props.months[index].table))
        dispatch(_actions.saveFormData(newFormData))
    }

    

    const _handleKeyDown = (event, index) => {
        if (event.key === 'Enter') {
            saveCellFour(props.oneIndex, props.towIndex,props.threeIndex, props.index, index, event.target.value, props);
        }
    }

    const _handleOnFocusout = (event, index) => {
            saveCellFour(props.oneIndex, props.towIndex,props.threeIndex, props.index, index, event.target.value, props);
    }

    return (
        <tr className={"level-" + props.levelToClass + ` ${props.dataFour.isShowRow ? '' : 'hidden-line'}`}>
            <Cell
                class={'cell-p'} value={props.name}
                showButton={true}
                showButtonPlus={false}
                showPercentage={false}
                levelCell={[props.oneIndex, props.towIndex,props.threeIndex, props.index]}
                isAddAccountPlan={firstCharacterIsNumber(props.name)}
                months={props.months}
                index={props.index}
                formData={props.formData}
                colSpan={2}
                percentage={props.formData.percentage}
                classTd={'mvheadcol cell-one cell-four'}
            />
            {props.months.length > 0 ? props.months.map((month, index) =>
                <td style={{color:textColor(month.value)}} key={`level-${props.levelToClass}-${props.oneIndex}-${props.towIndex}-${props.threeIndex}-${index}`} onClick={(event => onChangeCell(index))}>
                    {
                        props.formData.months[index].showCell && (!typeAction || typeAction === 'view')
                            ?
                            <CellEntryField
                                key={`cell-${props.levelToClass}-${props.oneIndex}-${props.towIndex}-${props.threeIndex}-${index}`}
                                onKeyDown={event => _handleKeyDown(event, index)}
                                value={month.value}
                                onFocusOut={event => _handleOnFocusout(event, index)}
                                indexMonth={index}
                                oneIndex={props.oneIndex}
                                towIndex={props.towIndex}
                                threeIndex={props.threeIndex}
                                Index={props.index}
                                oneRow={false}
                                tableName={month.table}
                            />
                            :
                            formatCurrency(month.value, false)
                    }
                </td>
            ) : ''}
            
        </tr>
    );

}

export default memo(SubLevelLineFour)