import React, { memo } from 'react';
import './SubLevelLine.scss';
import CellEntryField from "./cell-entry-field/CellEntryField";
import Cell from "./cell-entry-field/cell/Cell";
import {useDispatch, useSelector} from "react-redux";
import * as _actions from '../../../store/actions-reducers';
import useSaveCellManagerialValues from "../../../hooks/useSaveCellManagerialValues";
import useFormatGerencialCurrency from "../../../hooks/useFormatGerencialCurrency";
import useFirstCharacterIsNumber from "../../../hooks/useFirstCharacterIsNumber";
import useApplyTextColor from "../../../hooks/useApplyTextColor"
import useAmountDataParams from "../../../hooks/useAmountDataParams";


const SubLevelLineTow = props => {

    const formData = useSelector(state => state.formData);
    const dispatch = useDispatch();
    const typeAction = useSelector(state => state.typeAction);
    const { saveCellTow, saveCellPercentageTow } = useSaveCellManagerialValues();
    const { formatCurrency } = useFormatGerencialCurrency();
    const { firstCharacterIsNumber } = useFirstCharacterIsNumber()
    const {textColor} = useApplyTextColor();

    const onChangeCell = (index) => {
        let newFormData = [...formData];

        newFormData.map((om, i) => {
            om.months.map((month) => {
                month.showCell = false;
            })

            om.sub.map((omSub, omSubIndex) => {
                omSub.months.map((omS, mIndex) => {
                    omS.showCell =
                        i === props.oneIndex &&
                        omSubIndex === props.index &&
                        mIndex === index &&
                        !omS.showCell;
                })

                omSub.sub.map((omSubSub) => {
                    omSubSub.months.map((omSs) => {
                        omSs.showCell = false;
                    })
                    omSubSub.sub.map((omSubSubSub) => {
                        omSubSubSub.months.map((omSsS) => {
                            omSsS.showCell = false;
                        })    
                    })
                })
            })
        }, [])
        dispatch(_actions.saveTableName(props.months[index].table));
        dispatch(_actions.saveFormData(newFormData))
    }

    

    const _handleKeyDown = (event, index) => {
        if (event.key === 'Enter') {
            saveCellTow(props.oneIndex, props.index, index, event.target.value, props);
            dispatch(_actions.saveTableName(null));
        }
    }

    const _handleOnFocusout = (event, index) => {
        saveCellTow(props.oneIndex, props.index, index, event.target.value, props);
        dispatch(_actions.saveTableName(null));
    }

    return (
        <tr
            className={"level-" + props.levelToClass + ` ${props.dataOne.isShowRow ? '' : 'hidden-line'}`}
        >
            {/*<td>*/}
            {/*    {props.formData.sub.length > 0 ? <ButtonShowLine showRow={formData[props.oneIndex].sub[props.index].isShowRow} showHiddenTable={() => _handleShowHiddenLine(props.oneIndex, props.index)} /> : ''}*/}
            {/*</td>*/}
            <Cell
                showButton={true}
                showButtonPlus={true}
                showPercentage={false}
                levelCell={[props.oneIndex, props.index]}
                class={'cell-p'}
                value={props.name}
                isAddAccountPlan={firstCharacterIsNumber(props.name)}
                months={props.months}
                formData={props.formData}
                index={props.index}
                classTd={'mvheadcol cell-one cell-tow'}
                percentage={props.formData.percentage}
            />
            {props.months.length > 0 ? props.months.map((month, index) =>
                <td style={{color:textColor(month.value)}} key={`level-${props.levelToClass}-${props.oneIndex}-${index}`} onClick={(event => onChangeCell(index))}>
                    {
                        props.formData.months[index].showCell && (!typeAction || typeAction === 'view')
                            ?
                            <CellEntryField
                                key={`cell-${props.levelToClass}-${props.oneIndex}-${index}`}
                                onKeyDown={event => _handleKeyDown(event, index)}
                                value={month.value}
                                onFocusOut={event => _handleOnFocusout(event, index)}
                                indexMonth={index}
                                oneIndex={props.oneIndex}
                                Index={props.index}
                                oneRow={false}
                                tableName={month.table}
                                formData={props.formData}
                            />
                            :
                            formatCurrency(month.value, false)
                    }
                </td>
            ) : ''}
            
        </tr>
    );
}

export default memo(SubLevelLineTow);