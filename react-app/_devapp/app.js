import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import InitApp from './initApp'
//import FinancialBudgetApp from './components/financiaBudget/FinancialBudget'
import {Provider} from "react-redux";
import stare from "./store/reducers";
import 'bootstrap/dist/css/bootstrap.min.css';
import './app.scss';

//import Report from "./components/report/Report";

//const isReport = window.REACT_APP.isReport;
const id = window.REACT_APP.id;

ReactDOM.render(
    <React.StrictMode>
        <Provider store={stare}>         
            <InitApp id={id}/>
        </Provider>
    </React.StrictMode>,
    document.getElementById('financialBudget')
);
