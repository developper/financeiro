import { ACTIONS as __ } from '../constants/';

const saveFormData = data => ({
    type: __.SAVE_FORM_DATA,
    payload: data
});

const showFormFinancial = data => ({
    type: __.SHOW_FORM_FINANCIAL,
    payload: data
});

const saveLevelCell = data => ({
    type: __.SAVE_LEVEL_CELL,
    payload: data
});

const showFieldBudget = data => ({
    type: __.SHOW_FIELD_BUDGET,
    payload: data
});

const saveAccountPlan = data => ({
    type: __.SAVE_ACCOUNT_PAN,
    payload: data
});

const saveMonths = data => ({
    type: __.SAVE_MONTHS,
    payload: data
});

const saveIdBudget = data => ({
    type: __.SAVE_ID_BUDGET,
    payload: data
});

const saveTableName = data => ({
    type: __.SAVE_TABLE_NAME,
    payload: data
});

const saveDate = data => ({
    type: __.SAVE_DATE,
    payload: data
});

const changeLoading = data => ({
    type: __.SAVE_LOADING,
    payload: data
});
const saveTypeAction = data => ({
    type: __.SAVE_TYPE_ACTION,
    payload: data
});

const saveChartOfAccountsAdd = data => ({
    type: __.SAVE_CHART_OF_ACCOUNTS_ADD,
    payload: data
});

const saveBranches = data => ({
    type: __.SAVE_BRANCHES,
    payload: data
});

const saveShowHiddenLeval = data => ({
    type: __.SAVE_SHOW_HIDDEN_LEVEL,
    payload: data
});

export {
    saveFormData,
    showFormFinancial,
    saveLevelCell,
    showFieldBudget,
    saveAccountPlan,
    saveMonths,
    saveTableName,
    saveDate,
    saveIdBudget,
    changeLoading,
    saveTypeAction,
    saveChartOfAccountsAdd,
    saveBranches,
    saveShowHiddenLeval
}