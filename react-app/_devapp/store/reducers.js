import {createStore} from "redux";
import {ACTIONS as __} from "../constants";

const INITIAL_STATE = {
    formData: [],
    showFormFinancial: false,
    level: null,
    showFieldBudget: false,
    accountPlan: [],
    months: [],
    tableName: null,
    date: null,
    idBudget: null,
    isLoading: false,
    typeAction: null,
    chartOfAccountsAdd: [],
    branches: [],
    showHiddenLevelLine: {
        one: false,
        tow: false,
        three: false,
        four:false,
    }
};


function reducers(state, {type, payload}) {
    switch (type) {
        case __.SAVE_FORM_DATA:
            return {...state, formData: payload}
        case __.SHOW_FORM_FINANCIAL:
            return {...state, showFormFinancial: payload}
        case __.SAVE_LEVEL_CELL:
            return {...state, level: payload}
        case __.SHOW_FIELD_BUDGET:
            return {...state, showFieldBudget: payload}
        case __.SAVE_ACCOUNT_PAN:
            return {...state, accountPlan:  payload}
        case __.SAVE_MONTHS:
            return {...state, months: payload}
        case __.SAVE_TABLE_NAME:
            return {...state, tableName: payload}
        case __.SAVE_DATE:
            return {...state, date: payload}
        case __.SAVE_ID_BUDGET:
            return {...state, idBudget: payload}
        case __.SAVE_LOADING:
            return {...state, isLoading: payload}
        case __.SAVE_TYPE_ACTION:
            return {...state, typeAction: payload}
        case __.SAVE_CHART_OF_ACCOUNTS_ADD:
            return {...state, chartOfAccountsAdd: payload}
        case __.SAVE_BRANCHES:
            return {...state, branches: payload}
        case __.SAVE_SHOW_HIDDEN_LEVEL:
            return {...state, showHiddenLevelLine: {...state.showHiddenLevelLine,  ...payload}}
        default:
            return state
    }
}

const stare = createStore(
    reducers,
    INITIAL_STATE,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default stare