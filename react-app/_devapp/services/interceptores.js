import axios from 'axios';
import {API_URL} from "./config/apiUrl";

const config = {
    baseURL: API_URL,
    headers: {
        "Content-Type": "application/x-www-form-urlencoded"
    },
    responseType: 'json'
};

const instance = axios.create(config);

instance.interceptors.request.use(config => {
    return config
}, error => {
    return Promise.reject(error);
});

instance.interceptors.response.use(response => {
    return response;
}, error => {

    return new Promise((resolve, reject) => {
        return Promise.reject(error);
    });

});

export default instance;


