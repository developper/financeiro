import instance from "../interceptores";
import qs from 'querystring';

const handleError = err => {
    // console.error(`Api call error in services -> request.js : `, err);
    debugger
    return err;
};

export const getRequest = (url) => instance.get(url);

// export const getListRequest = async (url) => {
//         const res = await instance.get(url);
//         return res.data.result ? res.data.result : res.data;
// };
//
export const postRequest = (url, data = {}) => {
    return instance.post(url, qs.stringify(data));
}
export const updateRequest = (url, data = {}) => {
    return instance.post(url, data);
}

//

export const postRequestJson = async (url, data = {}) => {
    try {
        const res = await instance({
            url: url,
            method: "POST",
            data
        });
        return res.data.result ? res.data.result : res.data;
    } catch (err) {
        handleError(err);
    }
};
//
// export const patchRequest = async (url, data = {}) => {
//     try {
//         const res = await instance({
//             url: url,
//             method: "PATCH",
//             body: JSON.stringify(data)
//         });
//         return res.data.result ? res.data.result : res.data;
//     } catch (err) {
//         new Promise.reject(err)
//     }
// };
//
// export const deleteRequest = async (url) => {
//     try {
//         const res = await instance({
//             url: url,
//             method: "DELETE",
//         });
//         return res.data.result ? res.data.result : res.data;
//     } catch (err) {
//         handleError(err);
//     }
// };

export const Api = {
    // deleteRequest,
    // getListRequest,
    getRequest,
    postRequest,
    updateRequest,
    postRequestJson,
    // patchRequest
};
