<?php

$id = session_id();
if (empty($id))
	session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');
require __DIR__ . '/../vendor/autoload.php';

use \App\Controllers\Administracao as Administracao;
use \App\Models\Administracao\Filial;

class ImprimirRelatorio {

	public function cabecalho($id, $empresa = null, $convenio = null) {

		$condp1 = "c.idClientes = '{$id}'";
		$sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresa,
		c.`nascimento`,
		p.nome as Convenio,p.id,
		NUM_MATRICULA_CONVENIO,
		cpf
		FROM
		clientes AS c LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN
		planosdesaude as p ON (p.id = c.convenio)
		WHERE
		{$condp1}
		ORDER BY
		c.nome DESC LIMIT 1";

		$result = mysql_query($sql);
		$result2 = mysql_query($sql2);
		$html .= "<table width=100% >";
		while ($pessoa = mysql_fetch_array($result2)) {
			foreach ($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);
			}
            $pessoa['empresa'] = !empty($empresa) ? $empresa : $pessoa['empresa'];
            $pessoa['Convenio'] = !empty($convenio) ? $convenio : $pessoa['Convenio'];
			$html .= "<br/><tr>";
			$html .= "<td colspan='2'><label style='font-size:14px;color:#8B0000'><b></b></label></td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>PACIENTE </b></label></td>";
			$html .= "<td><label><b>SEXO </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>{$pessoa['paciente']}</td>";
			$html .= "<td>{$pessoa['sexo']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>IDADE </b></label></td>";
			$html .= "<td><label><b>UNIDADE REGIONAL </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>" . join("/", array_reverse(explode("-", $pessoa['nascimento']))) . ' (' . $pessoa['idade'] . " anos)</td>";
			$html .= "<td>{$pessoa['empresa']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>";
			$html .= "<td><label><b>MATR&Iacute;CULA </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>" . strtoupper($pessoa['Convenio']) . "</td>";
			$html .= "<td>" . strtoupper($pessoa['NUM_MATRICULA_CONVENIO']) . "</td>";
			$html .= "</tr>
                        <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
		}
		$html .= "</table>";

		return $html;
	}

	public function imprimir_deflagrado($id, $idr, $empresa) {

		$html .= "<form method='post' target='_blank'>";

        $responseEmpresa = Filial::getEmpresaById($empresa);

		$html .= "<h1><a><img src='../utils/logos/{$responseEmpresa['logo']}' width='100' ></a></h1>";

		$html.= "<h1 style='font-size:18px;text-align:center;'>" . htmlentities("RELATÓRIO") . " DEFLAGRADO DE ENFERMAGEM</h1>";

        $sql = "SELECT 
                  empresas.nome AS empresa, 
                  planosdesaude.nome AS plano 
                FROM 
                  relatorio_deflagrado_enf
                  LEFT JOIN empresas ON relatorio_deflagrado_enf.empresa = empresas.id
                  LEFT JOIN planosdesaude ON relatorio_deflagrado_enf.plano = planosdesaude.id
                WHERE 
                  relatorio_deflagrado_enf.id = '{$idr}'";
        $rs = mysql_query($sql);
        $rowRel = mysql_fetch_array($rs);

		$html .= $this->cabecalho($id, $responseEmpresa['nome'], $rowRel['plano']);

		$sql = "SELECT
                    r.*,
                    DATE_FORMAT(r.DATA,'%d/%m/%Y') as data,
                    DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
                    DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
                    u.nome as user,
                    u.tipo as utipo,
                    u.conselho_regional
                FROM
                    relatorio_deflagrado_enf as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios)
                WHERE
                    r.ID = '{$idr}'
                ORDER BY
                    r.DATA DESC";

		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {
			$compTipo = $row['conselho_regional'];

			$profissional = $row['USUARIO_ID'];
			$data = $row['data'];
			$inicio = $row['inicio'];
			$fim = $row['fim'];
			$user = $row['user'] . $compTipo;
			$historia_clinica = $row['HISTORIA_CLINICA'];
			$servicos_prestados = $row['SERVICOS_PRESTADOS'];
		}

		$html .= "<p><b> " . htmlentities("RELATÓRIO") . " DEFLAGRADO DE ENFERMAGEM REALIZADO EM {$data}</p>";
		$html .= "<p>PROFISSIONAL " . htmlentities("RESPONSÁVEL") . ": {$user}</b></p>";

		$sql_problemas = "SELECT
                            p.*
                        FROM
                            problemas_relatorio_prorrogacao_enf as p
                        WHERE
                            p.RELATORIO_DEFLAGRADO_ENF_ID = {$idr}
                        ORDER BY
                        ID
            ";

		$html .= "<table class='mytable' style='width:95%;'>";
		$html .= "<thead><tr>";
		$html .= "<th colspan='3' style='background-color:#EEEEEE;'>PROBLEMAS ATIVOS</th></tr></thead>";
		$html .= "<tr style='background-color:#EEEEEE;'>
            <td style='width:50%;'><b>" . htmlentities("Descrição") . "</b></td>
            <td style='width:50%;'><b>Justificativa</b></td>
        </tr>";
		$result_problemas = mysql_query($sql_problemas);
		while ($row = mysql_fetch_array($result_problemas)) {
			$cid_id = $row['CID_ID'];
			$descricao = $row['DESCRICAO'];
			$observacao = $row['OBSERVACAO'];
			$html .= "<tr><td width='30%'>{$descricao}</td><td colspan='2'>{$observacao}</td></tr>";
		}
		$html .= "</table>";


		//Mostra Historia Clinica
		$html .= "<table class='mytable' style='width:95%;'>";
		$html .= "<thead><tr><th colspan='3' style='background-color:#EEEEEE;'>" . htmlentities("HISTÓRIA CLÍNICA") . " de $inicio à $fim </th></tr></thead>";
		//$html .= "<tr><td colspan='3'><b>História Clínica do período de $inicio à $fim </b></td></tr>";
		$html .= "<tr><td colspan='3'>{$historia_clinica}</td></tr>";
		$html .= "</table>";

		$html .= "<table class='mytable' style='width:95%;'>";
		$html .= "<thead><tr><th colspan='3' style='background-color:#EEEEEE;'>SERVIÇOS PRESTADOS</th></tr></thead>";
		$html .= "<tr><td colspan='3'>{$servicos_prestados}</td></tr>";
		$html .= "</table>";

		$html .= assinaturaProResponsavel($profissional);

		$paginas [] = $header . $html.= "</form>";

		$mpdf = new mPDF('pt', 'A4', 9);
		$mpdf->SetHeader('página {PAGENO} de {nbpg}');
		$ano = date("Y");
        $mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']}"." &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));
		$mpdf->WriteHTML("<html><body>");
		$flag = false;
		foreach ($paginas as $pag) {
			if ($flag)
				$mpdf->WriteHTML("<formfeed>");
			$mpdf->WriteHTML($pag);
			$flag = true;
		}
		$mpdf->WriteHTML("</body></html>");
		$mpdf->Output('ficha_prorrogacao.pdf', 'I');
		exit;
	}


}



$p = new ImprimirRelatorio();
$p->imprimir_deflagrado($_GET['id'], $_GET['idr'], $_GET['empresa']);
?>
