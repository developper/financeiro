<?php
require_once('../db/config.php');

$kitId = $_GET['kit'];
$qtdKits = $_GET['qtd_kits'];
$item = [];
$itens = [];

$sql = <<<SQL
SELECT 
  itens_kit.id,
  concat(catalogo.principio, ' ', catalogo.apresentacao) as nome,
  itens_kit.qtd
FROM
  itens_kit
  INNER JOIN catalogo ON itens_kit.CATALOGO_ID = catalogo.ID
WHERE
  itens_kit.idKit = '{$kitId}' 
ORDER BY 
  nome
SQL;

$rs = mysql_query($sql);

$count = mysql_num_rows($rs);

while($row = mysql_fetch_array($rs)) {
    $item['kit'] = $kitId;
    $item['idItem'] = $row['id'];
    $item['nome'] = $row['nome'];
    $item['qtd'] = $row['qtd'] * $qtdKits;

    $itens[] = $item;
}

if($count > 0) {
    echo json_encode($itens);
} else {
    echo '-1';
}
