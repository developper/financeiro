<?php
/*
 * @autor Ricardo Tássio
 *
 *
 * **
 * */

$id = session_id();

if(empty($id))
  session_start();

include_once('../db/config.php');
include_once('../utils/codigos.php');
require_once('paciente_enf.php');
require_once('relatorios.php');


use App\Helpers\StringHelper as String;
use App\Models\Logistica\Saidas;
use App\Models\Logistica\Solicitacoes;
use App\Models\Administracao\Paciente as ModelPaciente;


class Enfermagem  {


  private function menu(){
      echo "<div>
                <center>
                        <h1>Enfermagem</h1>
                </center>
            </div>";

      if(isset($_SESSION['permissoes']['enfermagem']['enfermagem_paciente']) || $_SESSION['adm_user'] == 1) {
          echo "<p><a href='?view=listar'>Pacientes</a></p>";
      }
    if((validar_tipo("modenf") || isset($_SESSION['permissoes']['enfermagem']) )) {
        if(isset($_SESSION['permissoes']['enfermagem']['enfermagem_solicitacao']) || $_SESSION['adm_user'] == 1) {
            echo "<p><a href='?view=solicitacoes'>Solicita&ccedil;&otilde;es Pendentes</a></p>";
        }
        if(isset($_SESSION['permissoes']['enfermagem']['enfermagem_solicitacao_avulsa']) || $_SESSION['adm_user'] == 1) {
            echo "<p><a href='?view=avulsa'>Solicita&ccedil;&atilde;o Avulsa</a></p>";
        }
        if(isset($_SESSION['permissoes']['enfermagem']['enfermagem_buscar_solicitacao_prescricao']) || $_SESSION['adm_user'] == 1) {
            echo "<p><a href='?view=buscar'>Buscar Solicitações / Prescrições </a></p>";
        }
        if(isset($_SESSION['permissoes']['enfermagem']['enfermagem_prescricao_enfermagem']) || $_SESSION['adm_user'] == 1) {
            echo "<p><a href='/enfermagem/prescricao/index.php?action=prescricao'>" . htmlentities('Prescrições de Enfermagem') . "</a></p>";
        }
        if(isset($_SESSION['permissoes']['enfermagem']['enfermagem_prescricao_curativo']) || $_SESSION['adm_user'] == 1) {
            echo "<p><a href='/enfermagem/prescricao/index.php?action=curativo'>Prescrições de Curativos</a></p>";
        }
        if(isset($_SESSION['permissoes']['enfermagem']['enfermagem_buscar_prescricao_medica']) || $_SESSION['adm_user'] == 1) {
            echo "<p><a href='../medico/?view=buscar'>Buscar Prescri&ccedil;&otilde;es Médicas</a></p>";
        }
        if(isset($_SESSION['permissoes']['enfermagem']['enfermagem_kit']) || $_SESSION['adm_user'] == 1) {
            echo "<p><a href='?view=kits'>Kits</a></p>";
        }
        if(isset($_SESSION['permissoes']['enfermagem']['enfermagem_brasindice']) || $_SESSION['adm_user'] == 1) {
            echo "<p><a href='../logistica/?op=brasindice&act=buscar'>Catálogo</a></p>";
        }
      /*  echo "<p><a href='../logistica/?op=buscar'>Buscar Movimentações (Entradas e Saídas)</a></p>";
      echo "<p><a href='?view=checklist'>Checklist</a></p>";*/
        if(isset($_SESSION['permissoes']['enfermagem']['enfermagem_relatorio']) || $_SESSION['adm_user'] == 1) {
            echo "<p><a href='?view=relatorios'>Relatórios</a></p>";
        }
    }
  }
  private function mountJustificativaAvulsa($avulsa) {
        $html = '';
        if($avulsa == -1){
            $html .= "<p>";
            $html .= "  <b>Justificativa:</b>";
            $html .= "  <select name='justificativa' id='justificativa-item-avulso' class='OBG'>";
            $html .= "      <option value=''>SELECIONE UMA JUSTIFICATIVA</option>";
            $sql   = "SELECT * FROM justificativa_solicitacao_avulsa where visualizar =1";
            $rs    = mysql_query($sql);
            while ($row = mysql_fetch_array($rs, MYSQL_ASSOC)) {
                $html .= "  <option value='{$row['id']}'>{$row['justificativa']}</option>";
            }
            $html .= "  </select>";
            $html .= "  <div id='just-outros'></div>";
            $html .= "  <div id='just-prescricao'></div>";
            $html .= "</p>";

            return $html;
        }  else {
            return "";
        }
  }

    private function formulario($avulsa = null, $temEntregaImediata = false,$tipoMedicamentoPlano = null){
        echo "<div id='formulario' title='Adicionar itens' tipo='' classe='' tipo-medicamento='{$tipoMedicamentoPlano['tipoMedicamento']}'>";
        echo alerta();
        if($tipoMedicamentoPlano['tipoMedicamento'] == 'G'){
            echo "<div style='border: 2px dotted #999'>
                    <span class = 'text-red'>Paciente do plano {$tipoMedicamentoPlano['nomePlano']} deve prescrever itens Genéricos.

                     </span>
                     </div><br>";
        }
        echo "<center><div id='tipos'>";

        if(!in_array($avulsa, array(-1))) {
            echo "<input type='radio' id='bmed' name='tipos' value='med' /><label for='bmed'>Medicamentos</label>";
        }
        echo "<input type='radio' id='bmat' name='tipos' value='mat' /><label for='bmat'>Materiais</label>";
        if(!in_array($avulsa, array(-1))) {
            echo "<input type='radio' id='bdie' name='tipos' value='die' /><label for='bdie'>Dietas</label>";
        }
        echo "<input type='radio' id='bkits' name='tipos' value='kits' /><label for='bkits'>Kits</label>";

        echo "</div></center>";
        echo "<p><b>Busca:</b><br/><input type='text' name='busca' id='busca-item' /></p>";
        echo "<div id='opcoes-kits'></div>";
        echo "<input type='hidden' name='cod' id='cod' value='-1' />";
        echo "<input type='hidden' name='catalogo_id' id='catalogo_id' value='-1' />";
        echo "<input type='hidden' name='categoria' id='categoria' value='' >";
        echo "<input type='hidden' name='valor-venda' id='valor-venda' value='' >";
        echo "<p><b>Nome:</b><input type='text' name='nome' id='nome' class='OBG' /></p>";
        echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";

        if (in_array($avulsa, array(-1, 3, 6)))
          echo '<input id="entrega-imediata" type="checkbox" name="entrega-imediata" class="entrega-imediata">
                <label for="entrega-imediata"><b>Entrega Imediata</b></label>';

        echo $this->mountJustificativaAvulsa($avulsa);

        echo "<p><button id='add-item' tipo='' data-tipoSolicitacao='{$avulsa}' >Adicionar à lista <b>+</b></button></p></div>";
    }

      public function pacientes($post = []) {

        $medico = $_SESSION['id_user'];
//        $result = mysql_query("SELECT c.* FROM clientes as c, equipePaciente as e WHERE e.paciente = c.idClientes AND e.funcionario = {$medico} AND e.fim  IS NULL ORDER BY nome;");
//        $result = mysql_query("SELECT c.* FROM clientes as c WHERE c.status = 4 AND (c.empresa in{$_SESSION['empresa_user']} OR '1' in{$_SESSION['empresa_user']}) ORDER BY nome;");
				$condEmpresa = $_SESSION['empresa_principal'] != 1 ? "WHERE c.empresa IN {$_SESSION['empresa_user']}" : "";
				$sql = "SELECT c.* FROM clientes as c {$condEmpresa} ORDER BY status, nome";
				$result = mysql_query($sql);

        echo "<p><b>Localizar Paciente</b></p>";
        echo "<select name='pacientes' style='background-color:transparent;width:401px' id='selPaciente' data-placeholder='Selecione um Paciente'  class='validacao_periodo_busca chosen-select'>";
        echo "<option value='-1'></option>";
        $aux = 0;
        $paciente_status = array();
        $status =array('1'=>'::     Novo    ::','4'=>'::   Ativo/Autorizado   ::','6'=>'::     Óbito    ::');
        while ($row = mysql_fetch_array($result))
        {
            $paciente = strtoupper($row['nome']);
            if(array_key_exists($row['status'], $paciente_status))
            {
                $pacientes_por_status = array('idCliente'=>$row['idClientes'],'nome'=>$paciente, 'codigo'=>$row['codigo'], 'status'=>$row['status']);
                # Verifica se a chave existe dentro do array $status,
                # caso nÃ£o esteja, ele adiciona ao cÃ³digo 7 para que
                # os outros possam ser ordenados juntos no select
                if(array_key_exists($row['status'], $status))
                {
                    array_push($paciente_status[$row['status']],$pacientes_por_status);
                }
                else
                {
                    if(!array_key_exists(7,$paciente_status))
                    {
                        $paciente_status[7] = array();
                    }
                    array_push($paciente_status[7],$pacientes_por_status);
                }

            }
            else
            {
                $paciente_status[$row['status']] = array();
                $pacientes_por_status = array('idCliente'=>$row['idClientes'],'nome'=>$paciente, 'codigo'=>$row['codigo'], 'status'=>$row['status']);
                # Verifica se a chave existe dentro do array $status,
                # caso nÃ£o esteja, ele adiciona ao cÃ³digo 7 para que
                # os outros possam ser ordenados juntos no select
                if(array_key_exists($row['status'], $status))
                {
                    array_push($paciente_status[$row['status']],$pacientes_por_status);
                }
                else
                {
                    if(!array_key_exists(7,$paciente_status))
                    {
                        $paciente_status[7] = array();
                    }
                    array_push($paciente_status[7],$pacientes_por_status);
                }

            }
        }


       $cont=0;
       asort($paciente_status);
       foreach($paciente_status as $chave=>$dados)
       {
            #Verifica se Ã© a primeira vez que ta rodando
            if(array_key_exists($chave, $status) && $cont==0)
            {
                echo "<optgroup></optgroup>";
                echo "<optgroup label='{$status[$chave]}'>";
            }#Da segunda vez em diante passarÃ¡ por aqui
            elseif(array_key_exists($chave, $status) && $cont>0)
            {
                echo "</optgroup>";
                echo "<optgroup></optgroup>";
                echo "<optgroup label='{$status[$chave]}'>";
            }
            elseif($chave==7)
            {
                echo "</optgroup>";
                echo "<optgroup></optgroup>";
                echo "<optgroup label='::     Outros     ::'>";
            }
            foreach($dados as $chave2=>$dados2)
            {
                echo $chave;

                echo "<option " . ($post['pacientes'] != $dados2['idCliente'] ? '' : 'selected') . " value='{$dados2['idCliente']}' status='{$chave}'> ({$dados2['codigo']}) " . String::removeAcentosViaEReg($dados2['nome']) . "</option>";
            }
            $cont++;
       }
        echo "</optgroup>";
        echo "</select>";
    }

		public function select_carater_prescricao($get = [])
		{
			$sql 	= "SELECT * FROM carater_prescricao ORDER BY id";
			$rs		= mysql_query($sql);
			$carateres = [];
			while($row = mysql_fetch_array($rs)) {
				switch($row['profissional']){
					case 'm':
						$profissional = 'Médico';
						break;
					case 'n':
						$profissional = 'Nutrição';
						break;
					case 'e':
						$profissional = 'Enfermagem';
						break;
				}
				$carateres[$profissional][$row['id']] = $row['nome'];
			}
			$html = <<<HTML
<fieldset style="border: 1px solid #000; width: 500px; height: 62px; position: relative; float: right; margin: 0 200px 0 0;">
	<legend><strong>Caráter da Prescrição</strong></legend>
	<select name="carater" style="background-color:transparent;width:200px" id="sel-carater" data-placeholder="Selecione o Caráter da Prescrição"  class="chosen-select">
		<option value="-1">Todos</option>
HTML;
			foreach($carateres as $prof => $carater) {
				$html .= sprintf("<optgroup label='%s'>", $prof);
				foreach($carater as $id => $nome) {
					$html .= sprintf (
						'<option %s value="%s">%s</option>',
						($get['carater'] == $id ? 'selected' : ''),
						$key,
						$nome
					);
				}
				$html .= "</optgroup>";
			}
			$html .= <<<HTML
	</select>
</fieldset>
HTML;
			return $html;
		}

  private function pacientes_presc($obg = null) {

        $medico = $_SESSION['id_user'];
//        $result = mysql_query("SELECT c.* FROM clientes as c, equipePaciente as e WHERE e.paciente = c.idClientes AND e.funcionario = {$medico} AND e.fim  IS NULL ORDER BY nome;");
//        $result = mysql_query("SELECT c.* FROM clientes as c WHERE c.status = 4 AND (c.empresa in{$_SESSION['empresa_user']} OR '1' in{$_SESSION['empresa_user']}) ORDER BY nome;");
        $result = mysql_query("SELECT c.* FROM clientes as c WHERE (c.empresa in{$_SESSION['empresa_user']} OR '1') ORDER BY status,nome;");

        echo "<p><b>Localizar Paciente</b></p>";
        echo "<select name='pacientes' style='background-color:transparent;width:362px' id='selPaciente' data-placeholder='Selecione um Paciente'  class='validacao_periodo_busca chosen-select {$obg}'>";
        echo "<option value='-1'></option>";
        $aux = 0;
        $paciente_status = array();
        $status =array('1'=>'::     Novo    ::','4'=>'::   Ativo/Autorizado   ::','6'=>'::     Óbito    ::');
        while ($row = mysql_fetch_array($result))
        {
            $paciente = strtoupper($row['nome']);
            if(array_key_exists($row['status'], $paciente_status))
            {
                $pacientes_por_status = array('idCliente'=>$row['idClientes'],'nome'=>$paciente, 'codigo'=>$row['codigo'], 'status'=>$row['status']);
                # Verifica se a chave existe dentro do array $status,
                # caso nÃ£o esteja, ele adiciona ao cÃ³digo 7 para que
                # os outros possam ser ordenados juntos no select
                if(array_key_exists($row['status'], $status))
                {
                    array_push($paciente_status[$row['status']],$pacientes_por_status);
                }
                else
                {
                    if(!array_key_exists(7,$paciente_status))
                    {
                        $paciente_status[7] = array();
                    }
                    array_push($paciente_status[7],$pacientes_por_status);
                }

            }
            else
            {
                $paciente_status[$row['status']] = array();
                $pacientes_por_status = array('idCliente'=>$row['idClientes'],'nome'=>$paciente, 'codigo' => $row['codigo'],'status'=>$row['status']);
                # Verifica se a chave existe dentro do array $status,
                # caso nÃ£o esteja, ele adiciona ao cÃ³digo 7 para que
                # os outros possam ser ordenados juntos no select
                if(array_key_exists($row['status'], $status))
                {
                    array_push($paciente_status[$row['status']],$pacientes_por_status);
                }
                else
                {
                    if(!array_key_exists(7,$paciente_status))
                    {
                        $paciente_status[7] = array();
                    }
                    array_push($paciente_status[7],$pacientes_por_status);
                }

            }
        }


       $cont=0;
       asort($paciente_status);
       foreach($paciente_status as $chave=>$dados)
       {
            #Verifica se Ã© a primeira vez que ta rodando
            if(array_key_exists($chave, $status) && $cont==0)
            {
                echo "<optgroup></optgroup>";
                echo "<optgroup label='{$status[$chave]}'>";
            }#Da segunda vez em diante passarÃ¡ por aqui
            elseif(array_key_exists($chave, $status) && $cont>0)
            {
                echo "</optgroup>";
                echo "<optgroup></optgroup>";
                echo "<optgroup label='{$status[$chave]}'>";
            }
            elseif($chave==7)
            {
                echo "</optgroup>";
                echo "<optgroup></optgroup>";
                echo "<optgroup label='::     Outros     ::'>";
            }
            foreach($dados as $chave2=>$dados2)
            {
                echo $chave;

                echo "<option value='{$dados2['idCliente']}' status='{$chave}'> ({$dados2['codigo']}) " . String::removeAcentosViaEReg($dados2['nome']) . "</option>";
            }
            $cont++;
       }
        echo "</optgroup>";
        echo "</select>";
    }

  private function solicitacoes_pendentes(){
    if(!(validar_tipo("modenf") ) && !isset($_SESSION['permissoes']['enfermagem']['enfermagem_solicitacao']) && $_SESSION['adm_user'] != 1){
      redireciona('/inicio.php');
    }

  	$sql = "SELECT
			       presc.*,
			       u.nome,
			       CONCAT(DATE_FORMAT(presc.inicio,'%d/%m/%Y'),'  até  ',DATE_FORMAT(presc.fim,'%d/%m/%Y')) AS sdata,
			       c.nome AS paciente,
			       idClientes,
			        p.nome as plano,
			        p.id as idPlano
			FROM
			       prescricoes AS presc INNER JOIN
			       clientes AS c ON (presc.paciente = c.idClientes) LEFT JOIN
			       usuarios AS u ON (presc.criador = u.idUsuarios) LEft JOIN
			       planosdesaude as p on (p.id = c.convenio)
			WHERE
			       (c.empresa in {$_SESSION['empresa_user']} OR '1' in{$_SESSION['empresa_user']}) AND
			       presc.lockp < NOW() AND presc.LIBERADO = 'S' AND presc.STATUS = 0
			ORDER BY
			       DATA DESC";
	$result = mysql_query($sql);
	$flag = false;
	//echo "<input type='hidden' name='label' value='' /><input type='hidden' name='paciente' value='-1' />";
	if($_SESSION['adm_user'] == 1){//caso seja adm, a tabela trará a opção de desativar a solicitação
      		echo "<table class='mytable' width=100% ><thead><tr><th>Desativar</th><th>Paciente</th><th>Prescri&ccedil;&atilde;o</th><th>A&ccedil;&atilde;o</th></tr></thead><tfoot></tfoot>";
              }else{
	echo "<table class='mytable' width=100% ><thead><tr><th>Paciente</th><th>Prescri&ccedil;&atilde;o</th><th>A&ccedil;&atilde;o</th></tr></thead><tfoot></tfoot>";
              }
	$i=0;
	while($row = mysql_fetch_array($result)) {

        echo "<input type='hidden' name='label' value='{$row['label']}' />
                    <input type='hidden' name='paciente' value='{$row['paciente']}' />
                    ";
        if ($z++ % 2 == 0) {
            $cor = '#E9F4F8';
        } else {
            $cor = '#FFFFFF';
        }
        foreach ($row AS $key => $value) {
            $row[$key] = stripslashes($value);
        }
        $checked = "";
        //      if(!$flag) $checked = "checked";
        //      $flag = true;
        $profissional = ucwords(strtolower($row['nome']));
        $paciente = ucwords(strtolower(String::removeAcentosViaEReg($row['paciente'])));
        $flagTipoAditiva = empty($row['flag']) ? '':' - '.$row['flag'];


        switch ($row['Carater']) {
            case '1':
                $tipo_prescricao = "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:red'><b><i> ( " . htmlentities("EMERGÊNCIA") . " )</i></b></span>";
                break;
            case '2':
                $tipo_prescricao = "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:green'><b><i> ( " . htmlentities("PERIÓDICA MÉDICA") . " )</i></b></span>";
                break;
            case '3':
                $tipo_prescricao = "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:purple'><b><i> ( ADITIVA  " . htmlentities("MÉDICA").$flagTipoAditiva." )</i></b></span>";
                break;
            case '4':
                $tipo_prescricao = "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:orange'><b><i> ( " . htmlentities("PERIÓDICA DE AVALIAÇÃO") . " )</i></b></span>";
                break;
            case '5':
                $tipo_prescricao = "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:green'><b><i> ( " . htmlentities("PERIÓDICA DE NUTRIÇÃO") . " )</i></b></span>";
                break;
            case '6':
                $tipo_prescricao = "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:purple'><b><i> ( ADITIVA DE " . htmlentities("NUTRIÇÃO").$flagTipoAditiva . " )</i></b></span>";
                break;
            case '7':
                $tipo_prescricao = "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:orange'><b><i> ( ADITIVA DE " . htmlentities("AVALIAÇÃO") . " )</i></b></span>";
                break;
            case '11':
                $tipo_prescricao = "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:orange'><b><i> (  " . htmlentities("SUSPENSÂO") . " )</i></b></span>";
                break;
        }

        $label = "<h1>Paciente: {$paciente}</h1><h3>Profissional: {$profissional} - " . htmlentities($row['sdata']) . "</h3>";
        echo "<tr label='{$label}' paciente='{$row['idClientes']}' antiga='{$row['id']}' class='trPrescricao' bgcolor='$cor' id='{$row['id']}'>";
        if ($_SESSION['adm_user'] == 1 || $_SESSION["adm_ur"] == 1 || isset($_SESSION['permissoes']['enfermagem']['enfermagem_solicitacao']['enfermagem_solicitacao_pendente_desativar'])) {
            echo "<td><span style='float:left;padding-top:2px;'><a href='#' class='desativar-presc' presc='{$row['id']}'><center><img style='padding-left:10px;' src='../utils/excluir.png' width='16' title='Desativar " . htmlentities("Solicitação") . "'></center></a></span></td>";
        }
        echo "&nbsp;&nbsp;<td>" . String::removeAcentosViaEReg($row['paciente']) . "</td>";
        echo "<td>{$row['nome']} - " . ($row['sdata']) . "{$tipo_prescricao}</td>";
        echo "<td>";
        if (isset($_SESSION['permissoes']['enfermagem']['enfermagem_solicitacao']['enfermagem_solicitacao_pendente_iniciar']) || $_SESSION['adm_user'] == 1) {
        echo " <span style='float:left;'>
        <a href='?view=solicitacoes&label={$label}&paciente={$row['idClientes']}&antigas={$row['id']}&carater={$row['Carater']}'>
        <center><img src='../utils/solicitacao_enf.png' width='12' title='" . htmlentities("Iniciar Solicitação") . "'></center></a></span>";
    }
        echo "</td>";
	}
	echo "</table>";
  }

  private function buscar_avulsa($paciente,$inicio,$fim){

  	echo "<fieldset style='margin:0 auto;'>";
  	echo "<div id='div-busca-avulsa' >";

  	echo alerta();

  	echo "<div style='float:right;width:100%;'>";
  	echo "<center><h2 style='background-color:#8B0101;color:#ffffff;height:25px;vertical-align: middle;'><label style='vertical-align: middle;'>BUSCAR SOLICITA&Ccedil;&Otilde;ES AVULSAS</label></h2></center>";
  	echo $this->pacientes_presc();
  	echo $this->periodo();

  	echo "<p><button id='buscar-avulsa' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Buscar</span></button>";


  	echo "</div>";

  	echo "</div>";
  	echo "</fieldset>";
  	echo "<div id='div-resultado-busca-avulsa'></div>";
  	   }
  // Fim Eriton *.*
  private function aprazamento($id,$freq,$cod,$cod1,$apraz1,$tipo){
  	if($tipo != -1){
  	$html = "<br/><b>Aprazamento: </b><div class='aprazamento' item='{$id}'>";
  	if($cod==$cod1)
  		$apraz1 = explode(" ",$apraz1);
  	else
  		$apraz1 ="";

  	///if($cod <= 0 ||  $cod == 15 || $cod == 16 || $cod == 18 || $cod>23){
  		if($tipo !=0 && $tipo !=1 && $tipo !=2 && $tipo !=12 && $tipo != 13){
	  	for($i = 0; $i < $freq; $i++){
	  		$html .= "<input type='text' class='hora OBG' size='4' maxlength='5' />&nbsp;";
	  	}
	  	$html .= "<br>";
	  	for($j = 0; $j < $freq; $j++){
	  		$html .= "<b style='color:red;margin-left:17px;margin-right:17px;'>{$apraz1[$j]}</style></b><b style='padding:10px;'> </b>";
	  	}
  	}else{
  		$html = "";
  	}
  	if($freq == 0) $html = "";
  	else $html .= "</div>";
	}else{
	$html = "";
	}
  	return $html;
  }

 /////////////////////jeferson//////////
 private function medanterior($idpresc,$cod,$cod1,$paciente,$id,$nome,$carater,$catalogo_id,$catalogo_id1){

    if($catalogo_id == $catalogo_id1){

        $sql="
            SELECT
                B.NUMERO_TISS, B.ID, B.principio, B.apresentacao,S.autorizado,S.qtd, S.status, B.tipo as tipo,
                
            FROM
                solicitacoes as S inner join catalogo as B on (B.ID = S.CATALOGO_ID)
            WHERE
                B.ID='{$catalogo_id1}' and S.idPrescricao = '{$idpresc}' and B.tipo = 0 and B.ATIVO ='A'
            ORDER BY
                B.ID LIMIT 1 ";
                /// jeferson 23-07-2013 foi adicionado {and s.tipo in (0,3)}
        /*$result=mysql_query($sql);
        $principio= $result($result,0);

        $sql="select B.principio, B.  from principioativo where cod = $cod";*/
        $result=mysql_query($sql);

            //$sql="select I.nome, S.autorizado from solicitacoes as S inner join itens_prescricao as I on (I.cod=S.cod) where  S.idPrescricao = '$idpresc' and S.cod = '$cod1' and S.tipo= 0 order by nome limit 1";

        $result = mysql_query($sql);
            while($row = mysql_fetch_array($result)){
              $entregaImediata = ($carater == 3 || $carater==6) ? "&nbsp;&nbsp;<input type='checkbox' name='entrega-imediata' value='1' class='entrega-imediata'/><b>Entrega Imediata</b>":'';
              //var_dump($tipo);die();
              /*if($row['tipo'] == 0){
                $tipo = 'med';
              }else{
                $tipo = 'die';
              }*/
                if($row['autorizado'] > 0){
                    $nome=$row['principio']." ".$row['apresentacao'];
                    $html.="<tr class='dados' bgcolor='#ffffff' qtd='{$row['autorizado']}' nome='{$nome}' cod='{$row['NUMERO_TISS']}' catalogo_id='{$row['ID']}' tipo='med' categoria='' nome='{$nome}'>
                    <td>
                         '{$nome}' "
                         . " Quantidade: <input name='{$cod}' class='qtd_item' type='text' value='{$row['autorizado']}'></input>
                             <input type='checkbox' class='qtdok' aceito='1'>Ok</input>
                             {$entregaImediata}
                     <img class='rem-item-solicitacao' border='0' align='right' title='Remover' src='../utils/delete_16x16.png' item='{$id}'>
                    </td>
                    </tr>";
                }
                if($row['status'] == 4){
                    $nome=$row['principio']." ".$row['apresentacao'];
                    $html.="<tr class='dados' bgcolor='#ffffff' qtd='{$row['qtd']}' categoria=''  nome='{$nome}' cod='{$row['NUMERO_TISS']}' catalogo_id='{$row['ID']}' tipo='med' nome='{$nome}'>
                    <td>'{$nome}'  Quantidade: <input name='{$cod}' class='qtd_item' type='text' value='{$row['qtd']}'></input><input type='checkbox' class='qtdok' aceito='1'>Ok</input>
                     <img class='rem-item-solicitacao' border='0' align='right' title='Remover' src='../utils/delete_16x16.png' item='{$id}'>
                    </td>
                    </tr>";
                }
            }
        }
        return $html;
    }

    private function dieanterior($idpresc,$cod,$cod1,$paciente,$id,$nome,$x,$catalogo_id,$catalogo_id1){

    if($catalogo_id == $catalogo_id1){

        $sql="
            SELECT
                B.NUMERO_TISS, B.ID, B.principio, B.apresentacao,S.autorizado,S.qtd, S.status
            FROM
                solicitacoes as S inner join catalogo as B on (B.ID = S.CATALOGO_ID)
            WHERE
                B.ID='{$catalogo_id1}' and S.idPrescricao = '{$idpresc}' and B.tipo = 3 and B.ATIVO ='A'
            ORDER BY
                B.ID LIMIT 1 ";

        $result = mysql_query($sql);
            while($row = mysql_fetch_array($result)){
                  $entregaImediata = ($carater == 3 || $carater==6) ? "&nbsp;&nbsp;<input type='checkbox' name='entrega-imediata' value='1' class='entrega-imediata'/><b>Entrega Imediata</b>":'';

                if($row['autorizado'] > 0){
                    $nome=$row['principio']." ".$row['apresentacao'];
                    $html.="<tr class='dados' bgcolor='#ffffff' qtd='{$row['autorizado']}' nome='{$nome}' categoria=''  cod='{$row['NUMERO_TISS']}' catalogo_id='{$row['ID']}' tipo='die' nome='{$nome}'>
                    <td>
                    '{$nome}' "
                    . " Quantidade:"
                      . " <input name='{$cod}' class='qtd_item' type='text' value='{$row['autorizado']}'></input>
                          <input type='checkbox' class='qtdok' aceito='1'>Ok</input>
                          {$entregaImediata}
                     <img class='rem-item-solicitacao' border='0' align='right' title='Remover' src='../utils/delete_16x16.png' item='{$id}'>
                    </td>
                    </tr>";
                }
                if($row['status'] == 4){
                    $nome=$row['principio']." ".$row['apresentacao'];
                    $html.="<tr class='dados' bgcolor='#ffffff' qtd='{$row['qtd']}' nome='{$nome}' categoria=''  cod='{$row['NUMERO_TISS']}' catalogo_id='{$row['ID']}' tipo='die' nome='{$nome}'>
                    <td>'{$nome}'  Quantidade: <input name='{$cod}' class='qtd_item' type='text' value='{$row['qtd']}'></input><input type='checkbox' class='qtdok' aceito='1'>Ok</input>
                     <img class='rem-item-solicitacao' border='0' align='right' title='Remover' src='../utils/delete_16x16.png' item='{$id}'>
                    </td>
                    </tr>";
                }
            }
        }
        return $html;
    }
  ////////////jefersonfim
 private function load_table($prescricao, $paciente){
  	$prescricao = anti_injection($prescricao,'numerico');
  	$html = "<table class='mytable' width=100% ><thead><tr>";
    $html .= "<th><b>Itens prescritos</b></th>";

   $sql2="SELECT p.`id`, p.`Carater` FROM prescricoes p WHERE  p.id='{$prescricao}' ";
    $result2=mysql_query($sql2);
    $carater_prescricao = mysql_result($result2,0,1);

    if($carater_prescricao == 2 || $carater_prescricao == 5 || $carater_prescricao == 4){
		if($carater_prescricao == 2){
			$condicao="p.`Carater` in (2,4) AND ";
		}else{
			$condicao = "p.`Carater` ={$carater_prescricao} AND ";
		}
    }else{
		$condicao = "";
	}

    $sql = "SELECT
            i.*,
            f.qtd,
            1 AS col
	FROM
            itens_prescricao AS i
            LEFT JOIN frequencia AS f ON f.id = i.frequencia
	WHERE
	idPrescricao = (SELECT
            p.`id`
        FROM prescricoes p
        WHERE
        {$condicao}
        p.id<>'{$prescricao}'
        AND p.`paciente`='{$paciente}'
        and p.DESATIVADA_POR = 0
        and p.STATUS = 0
        ORDER BY p.`id` DESC LIMIT 1 )
    
   	UNION
    SELECT
	i.*,
	f.qtd,
	2 AS col
	FROM
	itens_prescricao AS i
	LEFT JOIN frequencia AS f ON f.id = i.frequencia
	WHERE
	idPrescricao = '{$prescricao}'
	ORDER BY
	tipo,NUMERO_TISS,CATALOGO_ID,col,inicio,aprazamento,nome,apresentacao ASC";
    //echo '<pre>'; die($sql);

   /* if($x == 5)
    	{
    		$sql = "SELECT i.*, f.qtd,1 AS col FROM itens_prescricao AS i, frequencia AS f WHERE idPrescricao = (SELECT p.`id` FROM prescricoes p WHERE p.`Carater`= 5 AND p.id<>'{$prescricao}' AND p.`paciente`='{$paciente}' ORDER BY p.`id` DESC LIMIT 1 ) AND f.id = i.frequencia
    		UNION
    		SELECT i.*, f.qtd,2 AS col FROM itens_prescricao AS i, frequencia AS f WHERE idPrescricao = '{$prescricao}' AND f.id = i.frequencia ORDER BY tipo,cod,col,inicio,aprazamento,nome,apresentacao ASC";
    	}*/
    $result = mysql_query($sql);

//    $cor = false;
	$qtd = 0;
    $lastType = 0;
    $cont = 0;
    $html .= "</tr></thead>";
    //$row1 = array();

    while($row = mysql_fetch_array($result)){

    	//grava os itens da prescri��o anterior na $row1, para ser comparado nas fun��es  aprazamento, medanterior e matanterior(ainda n�o foi criado).
        if($row['col']==1){
            $row1 = $row;
    	}

    	if($row['col']==2){

            $i = implode("/",array_reverse(explode("-",$row['inicio'])));
            $f = implode("/",array_reverse(explode("-",$row['fim'])));

	//$aprazamento = "";
	//if($row['tipo'] <> 0) $aprazamento = $row['apraz'];
	      	$dados = " id='{$row['id']}' ";
    //      	$linha = "{$row['nome']} {$row['apresentacao']} {$row['nvia']} {$row['nfrequencia']} {$aprazamento} Per&iacute;odo: $i até $f OBS: {$row['obs']}";
                $html .= "<tr class='item-prescrito' qtd='0' bgcolor='#ebf3ff' $dados >";
                $botoes = "<img class='add-kit' tipo='kits' item='{$row['id']}' src='../utils/kit_16x16.png' title='Adicionar kit' border='0'>&nbsp;&nbsp;";
                $botoes .= "<img class='add-med' tipo='med' item='{$row['id']}' src='../utils/medicamento_16x16.png' title='Adicionar medicamento' border='0'>&nbsp;&nbsp;";
                $botoes .= "<img class='add-mat' tipo='mat' item='{$row['id']}' src='../utils/material_16x16.png' title='Adicionar material' border='0'>&nbsp;&nbsp;";
                //$botoes .= "<img class='add-serv' tipo='serv' item='{$row['id']}' src='../utils/servicos_16x16.png' title='Servi&ccedil;os' border='0'>&nbsp;&nbsp;";

                $botoes .= "<img class='add-die' tipo='die' item='{$row['id']}' src='../utils/formula_16x16.png' title='Dieta' border='0'>&nbsp;&nbsp;";
                $botoes .= "<img class='look-kit' item='{$row['id']}' src='../utils/look_16x16.png' title='Visualizar' border='0'>&nbsp;&nbsp;";
                $botoes .= "<img class='obs-enf' item='{$row['id']}' src='../utils/exclama.png' width='16' title='Observa&ccedil;&atilde;o' border='0'>&nbsp;&nbsp;";
                $obs =  "<div class='tabela-kits' id='obs-{$row['id']}'><input type='text' size='80' class='obs' cod='{$row['id']}' id='observacao-{$row['id']}'></div>";
            $kits = "<input type='hidden' name='itens_kits_excluidos' id='itens-kits-excluidos' value=''>";
            $kits .= "<table class='mytable tabela-kits' width=100% id='kits-{$row['id']}'><thead><tr><th>KITS</th></tr></thead></table>";
                $med = "<table class='mytable tabela-med' width=100% id='med-{$row['id']}'><thead><tr><th>Medicamentos</th></tr></thead>".$this->medanterior($row1['idPrescricao'],$row['NUMERO_TISS'],$row1['NUMERO_TISS'],$row['paciente'],$row['id'],$row['nome'],$carater_prescricao,$row['CATALOGO_ID'],$row1['CATALOGO_ID'])."</table>";
                $mat = "<table class='mytable tabela-mat' width=100% id='mat-{$row['id']}'><thead><tr><th>Materiais</th></tr></thead></table>";
                //$serv = "<table class='mytable tabela-serv' width=100% id='serv-{$row['id']}'><thead><tr><th>Servi&ccedil;os</th></tr></thead></table>";
                $dieta = "<table class='mytable tabela-die' width=100% id='die-{$row['id']}'><thead><tr><th>Dietas</th></tr></thead>".$this->dieanterior($row1['idPrescricao'],$row['NUMERO_TISS'],$row1['NUMERO_TISS'],$row['paciente'],$row['id'],$row['nome'],$carater_prescricao,$row['CATALOGO_ID'],$row1['CATALOGO_ID'])."</table>";

                $aprazamento = $this->aprazamento($row['id'],$row['qtd'],$row['NUMERO_TISS'],$row1['NUMERO_TISS'],$row1['aprazamento'],$row['tipo'],$row['CATALOGO_ID'],$row1['CATALOGO_ID']);

                /* COMENTADO POR RICARDO PARA FAZER APARECER A BARRA DE KITS, MEDICAMENTOS E MATERIAL PARA F�RMULA*/
                if($row['tipo'] == '12'){

                    $campo_formula = "<br/>Quantidade: <input type='text' size='4' value='' name='qtd-formula' cod='{$row['cod']}' tipo='12' class='numerico' >";
                    //$html .= "<td><span class='status'></span>{$row['descricao']}{$aprazamento}<br/>{$campo_formula}</td>";
                    $html .= "<td><span class='status'></span> $botoes {$obs}{$row['descricao']}{$aprazamento}<br/>{$campo_formula}<br/>$kits<br/>$med<br/>$mat<br/>$serv</td>";
                } else {
                    $html .= "<td><span class='status'></span> $botoes {$obs}{$row['descricao']}{$aprazamento}<br/>$kits<br/>$med<br/>$mat<br/>$serv<br/>$dieta</td>";
                }


	    	$html .= "</tr>";
	    	$cont++;
    	}

    }

    $html .= "</table>";

    if($carater_prescricao == 2 || $carater_prescricao == 4){
    	$inicio = strftime("%Y-%m-%d", strtotime("-30 days"));
    	$fim = date("Y-m-d");
    	$sql = "SELECT
                    CONCAT(b.principio,' ',b.apresentacao) AS item,
                    SUM(s.qtd) AS quantidade,
                    s.DATA_SOLICITACAO AS data,
                    s.NUMERO_TISS AS numero_tiss,
					s.CATALOGO_ID,
					b.categoria_material_id as categoria
                FROM
                    solicitacoes AS s INNER JOIN
                    catalogo AS b ON (s.CATALOGO_ID  = b.ID )
                WHERE
                    s.tipo = 1
                    and DATE_FORMAT(s.DATA_SOLICITACAO,'%Y-%m-%d') BETWEEN '{$inicio}' AND '{$fim}'
                    and s.paciente = $paciente
                    and b.ATIVO ='A'
                GROUP BY
                   s.CATALOGO_ID
                ";
    	$html.="<label style='font-size:14px;color:#8B0000'><b><center> PEDIDOS DE MATERIAIS NOS &Uacute;LTIMOS 30 DIAS </center></b></label>";
    	$html.="<table class='mytable' width = 100% >";
    	$html.="<thead> <tr>";
    	$html.="<th><b>Item</b></th>";

    	$html.="</tr></thead>";

    	$result = mysql_query($sql);

    	    while($row = mysql_fetch_array($result)){
	    	$html.="<tr class='dados' bgcolor='#ffffff' qtd='{$row['quantidade']}' categoria = '{$row['categoria']}' nome='{$row['item']}}' cod='{$row['numero_tiss']}' catalogo_id='{$row['CATALOGO_ID']}' tipo='mat'>
	    	<td>'{$row['item']}'  Quantidade: <input name='{$row['numero_tiss']}' categoria = '{$row['categoria']}' catalogo_id='{$row['CATALOGO_ID']}' class='qtd_item' type='text' value='{$row['quantidade']}'>
	    	</input><input type='checkbox' class='qtdok' aceito='1'>Ok</input><img class='rem-item-solicitacao' border='0' align='right' title='Remover' src='../utils/delete_16x16.png' item='{$row['CATALOGO_ID']}'>
	    	</td></tr>";
    	 	}
    	 $html.="</table>";
    }
    return $html;
  }

  private function load_table1($prescricao,$paciente){
    $prescricao = anti_injection($prescricao,'numerico');
    $html = "<table class='mytable' width=100% ><thead><tr>";
    $html .= "<th><b>Itens prescritos</b></th>";

   $sql2="SELECT p.`id`, p.`Carater` FROM prescricoes p WHERE  p.id='{$prescricao}' ";
   $result2=mysql_query($sql2);
   $carater_prescricao = mysql_result($result2, 0, 1);

    if($carater_prescricao == 2 || $carater_prescricao == 5 || $carater_prescricao == 4){
		if($carater_prescricao == 2){
			$condicao="p.`Carater` in (2,4) AND ";
		}else{
			$condicao = "p.`Carater` ={$carater_prescricao} AND ";
		}
    }else{
		$condicao = "";
	}

    $sql_anterior = "SELECT
            i.*,
            f.qtd,
            1 AS col
	FROM
            itens_prescricao AS i,
            frequencia AS f
	WHERE
            idPrescricao = (SELECT p.`id`
                FROM prescricoes p
                WHERE
                    {$condicao}
                    p.id<>'{$prescricao}'
                    AND p.`paciente`='{$paciente}'
                    and p.DESATIVADA_POR = 0
                    and p.STATUS = 0
                ORDER BY p.`id` DESC LIMIT 1
            )
            AND f.id = i.frequencia
        ORDER BY
            tipo,NUMERO_TISS,CATALOGO_ID,col,inicio,aprazamento,nome,apresentacao ASC";

    $sql_atual = "SELECT
	i.*,
	f.qtd,
	2 AS col
    FROM
	itens_prescricao AS i,
	frequencia AS f
    WHERE
	idPrescricao = '{$prescricao}'
	AND f.id = i.frequencia
    ORDER BY tipo, NUMERO_TISS, CATALOGO_ID, col, inicio, aprazamento, nome, apresentacao ASC";


    $result_anterior = mysql_query($sql_anterior);
    $result_atual = mysql_query($sql_atual);

    $qtd = 0;
    $lastType = 0;
    $cont = 0;
    $html .= "</tr></thead>";

    $row1 = array();

    while ($row_anterior = mysql_fetch_assoc($result_anterior)) {
        $rows_anterior[] = $row_anterior;
    }
    while ($row = mysql_fetch_assoc($result_atual)) {
       $rows_atual[] = $row;
    }
    foreach ($rows_anterior as $row) {
        foreach ($rows_atual as $row_atual) {
            $row1 = array();
            if ($row['NUMERO_TISS'] == $row_atual['NUMERO_TISS']
                && $row['aprazamento'] != ''
                && $row['CATALOGO_ID'] == $row_atual['CATALOGO_ID'])
            {
                $row['paciente'] = $paciente;
                $row1 = $row;
                break;
            }
        }

        $i = implode("/",array_reverse(explode("-",$row['inicio'])));
        $f = implode("/",array_reverse(explode("-",$row['fim'])));

        $dados = " id='{$row['id']}' ";
        $html .= "<tr class='item-prescrito' qtd='0' bgcolor='#ebf3ff' $dados >";
        $botoes = "<img class='add-kit' tipo='kits' item='{$row['id']}' src='../utils/kit_16x16.png' title='Adicionar kit' border='0'>&nbsp;&nbsp;";
        $botoes .= "<img class='add-med' tipo='med' item='{$row['id']}' src='../utils/medicamento_16x16.png' title='Adicionar medicamento' border='0'>&nbsp;&nbsp;";
        $botoes .= "<img class='add-mat' tipo='mat' item='{$row['id']}' src='../utils/material_16x16.png' title='Adicionar material' border='0'>&nbsp;&nbsp;";
        $botoes .= "<img class='add-die' tipo='die' item='{$row['id']}' src='../utils/formula_16x16.png' title='Dieta' border='0'>&nbsp;&nbsp;";
        $botoes .= "<img class='look-kit' item='{$row['id']}' src='../utils/look_16x16.png' title='Visualizar' border='0'>&nbsp;&nbsp;";
        $botoes .= "<img class='obs-enf' item='{$row['id']}' src='../utils/exclama.png' width='16' title='Observa&ccedil;&atilde;o' border='0'>&nbsp;&nbsp;";
        $obs =  "<div class='tabela-kits' id='obs-{$row['id']}'><input type='text' size='80' class='obs' cod='{$row['id']}' id='observacao-{$row['id']}'></div>";
        $kits = "<table class='mytable tabela-kits' width=100% id='kits-{$row['id']}'><thead><tr><th>KITS</th></tr></thead></table>";
        $med = "<table class='mytable tabela-med' width=100% id='med-{$row['id']}'><thead><tr><th>Medicamentos</th></tr></thead>".$this->medanterior($row1['idPrescricao'],$row['NUMERO_TISS'],$row1['NUMERO_TISS'],$row['paciente'],$row['id'],$row['nome'],$carater_prescricao,$row['CATALOGO_ID'],$row1['CATALOGO_ID'])."</table>";
        $mat = "<table class='mytable tabela-mat' width=100% id='mat-{$row['id']}'><thead><tr><th>Materiais</th></tr></thead></table>";
        $dieta = "<table class='mytable tabela-die' width=100% id='die-{$row['id']}'><thead><tr><th>Dietas</th></tr></thead></table>";

        $aprazamento = $this->aprazamento($row['id'],$row['qtd'],$row['NUMERO_TISS'],$row1['NUMERO_TISS'],$row1['aprazamento'],$row['tipo'],$row['CATALOGO_ID'],$row1['CATALOGO_ID']);

        /* COMENTADO POR RICARDO PARA FAZER APARECER A BARRA DE KITS, MEDICAMENTOS E MATERIAL PARA F�RMULA*/
        if($row['tipo'] == '12'){

            $campo_formula = "<br/>Quantidade: <input type='text' size='4' value='' name='qtd-formula' cod='{$row['cod']}' tipo='12' class='numerico' >";
            $html .= "<td><span class='status'></span> $botoes {$obs}{$row['descricao']}{$aprazamento}<br/>{$campo_formula}<br/>$kits<br/>$med<br/>$mat<br/>$serv</td>";
        } else {
            $html .= "<td><span class='status'></span> $botoes {$obs}{$row['descricao']}{$aprazamento}<br/>$kits<br/>$med<br/>$mat<br/>$serv<br/>$dieta</td>";
        }

        $html .= "</tr>";
        $cont++;

    }

    $html .= "</table>";

    if($carater_prescricao == 2 || $carater_prescricao == 4){
    	$inicio = strftime("%Y-%m-%d", strtotime("-30 days"));
    	$fim = date("Y-m-d");
    	$sql = "SELECT
                    CONCAT(b.principio,' ',b.apresentacao) AS item,
                    SUM(s.qtd) AS quantidade,
                    s.DATA_SOLICITACAO AS data,
                    s.NUMERO_TISS AS numero_tiss,
					s.CATALOGO_ID
                FROM
                    solicitacoes AS s INNER JOIN
                    catalogo AS b ON (s.CATALOGO_ID  = b.ID )
                WHERE
                    s.tipo = 1
                    and DATE_FORMAT(s.DATA_SOLICITACAO,'%Y-%m-%d') BETWEEN '{$inicio}' AND '{$fim}'
                    and s.paciente = $paciente
                GROUP BY
                   s.CATALOGO_ID
                ";
    	$html.="<label style='font-size:14px;color:#8B0000'><b><center> PEDIDOS DE MATERIAIS NOS &Uacute;LTIMOS 30 DIAS </center></b></label>";
    	$html.="<table class='mytable' width = 100% >";
    	$html.="<thead> <tr>";
    	$html.="<th><b>Item</b></th>";

    	$html.="</tr></thead>";

    	$result = mysql_query($sql);

    	    while($row = mysql_fetch_array($result)){
	    	$html.="<tr class='dados' bgcolor='#ffffff' qtd='{$row['quantidade']}' cod='{$row['numero_tiss']}' catalogo_id='{$row['CATALOGO_ID']}' tipo='mat'>
	    	<td>'{$row['item']}'  Quantidade: <input name='{$row['numero_tiss']}' catalogo_id='{$row['CATALOGO_ID']}' class='qtd_item' type='text' value='{$row['quantidade']}'>
	    	</input><input type='checkbox' class='qtdok' aceito='1'>Ok</input><img class='rem-item-solicitacao' border='0' align='right' title='Remover' src='../utils/delete_16x16.png' item='{$row['CATALOGO_ID']}'>
	    	</td></tr>";
    	 	}
    	 $html.="</table>";
    }
	//$html.=$sql;
    return $html;
  }

    public function tipoMedicamentoPlano($idPaciente)
    {
        $sqlTpoMedicamento = "SELECT
planosdesaude.nome,
planosdesaude.tipo_medicamento
FROM
clientes
INNER JOIN planosdesaude ON clientes.convenio = planosdesaude.id
WHERE
clientes.idClientes = $idPaciente ";
        $result=  mysql_query($sqlTpoMedicamento);
        $tipoMedicamento = mysql_result($result,0,1);
        $nomePlano = mysql_result($result,0,0);
        $tipoMedicamentoPlano = ["tipoMedicamento" => $tipoMedicamento, "nomePlano" => $nomePlano];
        return $tipoMedicamentoPlano;
    }



  private function prescricoes($label,$data,$antiga,$paciente,$carater){
    if($antiga == NULL){
      echo "<center><h1>" . htmlentities("Solicitações") . " Pendentes</h1></center>";
      echo "<form id='fromPresc' method='post' >";
      $this->solicitacoes_pendentes();
      //echo "<p><button id='iniciar-solicitacao'>Iniciar</button></form>";
      echo "</form>";
    } else {
      $antiga = anti_injection($antiga,'numerico');
      $r = mysql_query("SELECT lockp,empresa FROM prescricoes WHERE id = '$antiga' AND lockp < now() ");
      if(mysql_num_rows($r) == 0){
      	redireciona('index.php?view=solicitacoes');
      } else {
          $rowEmpresa = mysql_fetch_array($r);
          $empresaPrescricao = $rowEmpresa['empresa'];
      	$r = mysql_query("UPDATE prescricoes SET lockp = DATE_ADD(now(),INTERVAL 1 MINUTE) WHERE id = '$antiga' ");
      }

      $tipoMedicamentoPlano = $this->tipoMedicamentoPlano($paciente);
    echo "<input type = 'hidden' id='nomePlano' name='tipoMedicamento' value='{$tipoMedicamentoPlano['tipoMedicamento']}'>";
        echo "<input type = 'hidden' id='empresa-prescricao' name='empresa-prescricao' value='{$empresaPrescricao}'>";

 echo "<div id='dialog-salvo' title='Finalizada'>";
      echo "<center><h3>" . htmlentities("Prescrição") . " salva com sucesso!</h3></center>";
      echo "<form action='../medico/imprimir.php' method='post'>
<input type='hidden' value='-1' name='prescricao'>
<input type='hidden' value='-1' name='empresa'>
<center><button>Imprimir</button></center>
</form>";
      echo "</div>";
      echo "<div id='solicitacao' paciente='$paciente' prescricao='$antiga' carater='$carater' >";
      echo "<center>$label</center>";
      $this->formulario($carater,false,$tipoMedicamentoPlano);
      echo "<div id='dialog-servicos' Title='Adicionar " . htmlentities("Serviços") . "'>";
      echo alerta();
      echo "<b>" . htmlentities("Serviço") . ":</b>".combo_servicos("servicos");
      echo "<br/><b>Quantidade:</b> <input type='text' id='qtd-serv' name='qtd-serv' class='numerico OBG' size='3'/>";
      echo "<br/><button id='add-serv' tipo='' >+</button>";
      echo "</div><div id='tabela-solicitacoes'>";
      echo alerta()."<br/>";
      ///16-10
       ////equipamento
        $responsePrescricao = \App\Models\Medico\Prescricao::getById($antiga);

        if(!empty($responsePrescricao['motivo_ajuste'])){
            echo  "
            <div class='ui-state-highlight' style='padding: 5px;'>
                <span class='ui-icon  ui-icon-alert icon-inline' ></span>
                <b>Motivo do Ajuste da Prescrição Aditiva: </b> 
                {$responsePrescricao['motivo_ajuste']}
            </div>";

        }

    if (($carater == 3) || ($carater == 6)){
        echo "<div id='field_avulsa'>
               <p>
                    <fieldset>
                       <legend>Entrega sem caráter imediato</legend>
                       <div class='ui-state-highlight' style=\"padding: 5px;\">
                        <span class=\"ui-icon  ui-icon-info icon-inline\" ></span>
                        Para os itens \"sem\" caráter de entrega imediata, o prazo mínimo é de 24 horas.
                        </div>
                       <br/>
                       <b>Data </b>
                       <input type='text' class=' ' id='data-avulsa' name='data-avulsa' size='9px' />
                       <b>Hora: </b>
                       <input type='text'  class='hora'  id='hora-avulsa' name='hora-avulsa' size='9px'/>
                    </fieldset>
              </p>
              <p>
                    <fieldset>
                       <legend>Entrega com caráter imediato</legend>
                      <div class='ui-state-highlight' style=\"padding: 5px;\">
                        <span class=\"ui-icon  ui-icon-info icon-inline\" ></span>
                        Não existe um prazo mínimo para os itens \"com\" caráter de entrega imediata.
                      </div>
                      <br/><b>Data</b>
                      <input type='text'  id='dataEntregaImediata'  size='9px' />
                      <b>Hora: </b>
                      <input type='text'   class='hora'  id='horaEntregaImediata'  size='9px'/>
                    </fieldset>
               </p>
           </div>";
        $tdEntregaImediata= "<td><b>Ent. Imediata</b></td>";
    }
      $idCapmed=0;
      if($carater == 4){
        $sql="SELECT
    prescricoes.ID_CAPMEDICA
    FROM
    prescricoes
    WHERE
    prescricoes.id ={$antiga}";
        $result = mysql_query($sql);
        $idCapmed = mysql_result($result, 0,0);
      }
        $sql="SELECT * FROM (
                        SELECT *                     
                        FROM equipamentosativos
                        WHERE equipamentosativos.PACIENTE_ID = {$paciente}
                        AND equipamentosativos.DATA_FIM = '0000-00-00 00:00:00'
                        AND equipamentosativos.DATA_INICIO > '0000-00-00 00:00:00'
                        ORDER BY equipamentosativos.COBRANCA_PLANOS_ID, equipamentosativos.DATA_INICIO DESC
                        ) as equipamentosativos
                        GROUP BY equipamentosativos.COBRANCA_PLANOS_ID order by ID ASC  ";
        $result = mysql_query($sql);
        $idEquipamentoAtivo = mysql_result($result, 0,0);
        if ($idEquipamentoAtivo > 0){
            echo "<p><center><h2>Existe equipamento ativo para o paciente.</h2></center></p>";
        }
      echo"<input type='checkbox' id='equip_ativo' />Equipamentos.";
      echo"<div id='span_equipamentos_ativos' style='display:none;'>";
      echo"<table id='equipamentos_ativos'>";
      $result = mysql_query($sql);
                $equipamento = '';
                while($row=mysql_fetch_array($result)){
                $equipamento[$row['COBRANCA_PLANOS_ID']] = array("item"=>$row['item'],"und"=>$row['unidade'],"tipo"=>$row['tipo'],"qtd"=>$row['QTD'],"obs"=>$row['OBS'],
                   "idativo"=>$row['idativo'],"data"=>$row['DATA']);
                }
                $sql2= "select * from cobrancaplanos where tipo=1 and ATIVO=1 ORDER BY item";

                $result2=mysql_query($sql2);


           echo"<table id='equipamentos_ativos'  >";
                echo"<tr bgcolor='grey'>"
                            . "<td><b>Item</b></td>"
                            . "<td ><b>A&ccedil;&atilde;o</b></td>"
                            . "<td><b>Qtd.</b></td>"
                            . "<td><b>Ultima Substitui&ccedil;&atilde;o</b></td>"
                            ."<td><b>Justificativa</b></td>"
                            . $tdEntregaImediata
                    . "</tr>";
                 $cont_cor=0;

                while($row=mysql_fetch_array($result2)){
                    if( $cont_cor%2 ==0 ){
				$cor = '';
			}else{
				$cor="bgcolor='#EEEEEE'";
			}
                        $cont_cor++;

                    if(array_key_exists($row['id'], $equipamento)){
                        //$check = "checked='checked'";
                        $qtd = $equipamento[$row['id']]['qtd'];
                        $obs = $equipamento[$row['id']]['obs'];
                        $equipamenentoAtivo = 1;
                        $data = $equipamento[$row['id']]['data'];

                        if($carater ==4){
                            $checkedCapmedica = "selected='selected'";
                            $classVerde='verde';
                            $readonly='';
                        }else if($y != '' && $y != null && $avaliacao != 0){
                            $checkedCapmedica = "selected='selected'";
                            $classVerde='verde';
                            $readonly='';
                        }else {
                            $checkedCapmedica ='';
                            $classVerde='';
                            $readonly='readonly';
                        }



                        $options= "<option value='1' class='verde' $checkedCapmedica >Solicitar</option>"
                                . "<option value='3' class='laranja' >Substituir</option>"
                                . "<option value='2' class='vermelho' >Recolher</option>";


                    }else{
                        $classVerde='';
                        $checkedCapmedica ='';
                        $qtd='';
                        $obs = '';
                       $equipamenentoAtivo =0;
                        $data = '';
                        $botao = '';
                        $options= "<option value='1' class='verde' >Solicitar</option>";
                        $readonly='';

                    }
                    $select= "<select class='acao-equipamento $classVerde' data-equipamento-ativo='{$equipamentoAtivo}' data-id-capmedica='{$idCapmed}' data-class-remove='' id='select-acao"."{$row['id']}' data-idequipamento='{$row['id']}' style='max-width:104px;' >
                                 <option value='-1' class='branco'>Selecione</option>
                                 {$options}
                              </select>";
                    //echo"<tr><td><input type='checkbox' $check class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}'></input>";
                   if($carater == 3 || $carater == 6){
                       $tdCheckEntregaImediata= "<td><input type='checkbox' class='entrega-imediata' id='entrega-imediata"."{$row['id']}' disabled='disabled' /></td>";

                   }
                  echo "<tr $cor >"
                            . "<td>{$row['item']}</td>"
                            . "<td>$select</td>"
                            . "<td><input type='texto' $readonly id='qtd"."{$row['id']}"."'  size='3px' class='qtd_equipamento' value=''  name='qtd_equipamento'  {$acesso} ></input></td>"
                            . "<td><input type='texto' $readonly id='data"."{$row['id']}"."' class='data_equipamento' value='{$data}' name='data_equipamento' size='6px' {$acesso}></input></td>"
                            . "<td><textarea $readonly id='observacao"."{$row['id']}"."'></textarea></td>"
                            .$tdCheckEntregaImediata
                         . "</tr>";
                }

      echo"</table></div>";

      if(isset($_SESSION['id_user']) && $_SESSION['id_user'] == 122) {
          $tabela = $this->load_table($antiga,$paciente, $carater);
      } else {
          $tabela = $this->load_table($antiga,$paciente, $carater);
      }

      echo $tabela;
			echo "<br/><button id='salvar-solicitacao' tipo='' style='position: relative; float: left;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></form></div>";
      echo "<button id='sair-sem-salvar' style='position: relative; float: left;' tipo='' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></form></div>";
			if($carater == 2) {
				echo "<span style='position:relative; float:left;'>
							<input style='height: 26px; width: 19px; margin-left: 22px;' type='checkbox' value='1' id='primeira-prescricao' />
							<b style='margin-left: 6px; vertical-align: super; font-size: 14px;'>
								Marque se essa for a primeira prescrição periódica para o paciente
							</b>
						</span>";
			} else {
				echo "<input type='hidden' value='0' id='primeira-prescricao' />";
			}
		}
  }

  private function kits(){
    if(!(validar_tipo("modenf") )){
      redireciona('/inicio.php');
    }
  	echo "<div id='dialog-kit' title='Lista Materiais' kit=''>";
  	echo "<div id='div-tabela-fantasias'></div>";
	echo alerta();
	echo "<center><h3 style='text-transform:uppercase' id='h-nome-kit'></h3></center>";
	echo "<p><b>Busca:</b><br/><input type='text' name='busca' id='busca' />";
    echo "<div id='opcoes-itens'></div>";
    echo "<input type='hidden' name='cod' id='cod' value='-1' />";
    echo "<input type='hidden' name='catalogo_id' id='catalogo_id' value='-1' />";
    echo "<p><b>Nome:</b><input type='text' name='nome' id='nome' class='OBG' size='50' />";

    echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
    echo "<button id='add-item-kit' tipo='' >+</button>";
    echo "<div id='tabela-itens-kit'></div>";
  	echo "</div>";
  	$this->historicoKits();
  	echo "<center><h1>Kits</h1></center>";
  	echo "<input type='text' name='nome-kit' maxlength='50' /> <button id='criar-novo-kit'>Criar Novo</button>";
  	echo "<div id='div-lista-kits'></div>";
  }

    private function historicoKits(){
        echo "<div id='dialog-historico-kit' title='Histórico de Mudanças no Kit' kit=''>";
        echo "<center><h3 style='text-transform:uppercase' id='historico-nome-kit'></h3></center>";
        echo "    <div id='tabela-historico-kit'></div>";
        echo "</div>";
    }

  private function buscas(){
    if(!(validar_tipo("modenf") )){
      redireciona('/inicio.php');
    }
  	echo "<h1><center>Envio de materiais</center></h1>";
    echo "<b>Buscar por:</b><input type='radio' name='tipo-busca-saida' value='med' />Medicamento
    	 <input type='radio' name='tipo-busca-saida' value='mat' />Material
    	 <input type='radio' name='tipo-busca-saida' value='paciente' />Paciente";
   	echo "<div id='combo-busca-saida'></div>";
    echo "<p><b>Resultado:</b><input type='radio' name='resultado' value='a' checked />Analítico<input type='radio' name='resultado' value='c' />Condensado";
    echo "<table><tr><td colspan='4'>";
    $hoje = date("j/n/Y");
    echo "</td></tr><tr><td><b>In&iacute;cio do per&iacute;odo:</b></td><td><input id='inicio' type='text' name='inicio' value='$hoje' maxlength='10' size='10' /></td>";
    echo "<td><b>Fim do per&iacute;odo:</b></td><td><input id='fim' type='text' name='fim' value='$hoje' maxlength='10' size='10' /></td></tr></table>";
    echo "<button id='buscar-saida' >Mostrar</button>";
    echo "<div id='resultado-busca' ></div>";
  }

  private function avulsa($paciente,$nome,$e,$data_avulsa,$hora_avulsa){
    if( !validar_tipo("modenf") &&
        !isset($_SESSION['permissoes']['enfermagem']['enfermagem_solicitacao_avulsa']) &&
        $_SESSION['adm_user'] != 1
    ){
      redireciona('/inicio.php');
    }
  	echo "<h1><center>Solicita&ccedil;&atilde;o Avulsa</center></h1>";
        if($e == 0){
            $tipo_solicitacao = -1;
        }else if($e == 2){
            $tipo_solicitacao = -2;
        }


  	if($paciente == NULL){
            echo "<div id='div-iniciar-avulsa'>";
            echo alerta();
            echo "<form method='post' >";

            echo $this->pacientes(NULL,"pacientes","n");
            echo "<input type='hidden' name='npaciente' value='' />";
        if ($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                ['enfermagem']
                ['enfermagem_solicitacao_avulsa']
                ['enfermagem_solicitacao_avulsa_novo'])) {
            echo "<p><button id='iniciar-avulsa'>Iniciar</button></p>";
        }
           echo "</form></div><span class='text-red'>".  base64_decode($_GET['error'])."</span>";
  	} else {
        $tipoMedicamentoPlano = $this->tipoMedicamentoPlano($paciente);
            $this->formulario(-1,false, $tipoMedicamentoPlano);
            echo "<div id='dialog-servicos' Title='Adicionar Servi&ccedil;os'>";
             echo alerta();
             echo "<b>Servi&ccedil;o:</b>".combo_servicos("servicos");
             echo "<br/><b>Quantidade:</b> <input type='text' id='qtd-serv' name='qtd-serv' class='numerico OBG' size='3'/>";
    	     echo "<br/><button id='add-serv' tipo='' item='avulsa' >+</button>";
             echo "</div>";
        echo "<div id='avulsa' paciente='$paciente' e='$e'>";
        echo "<input type='hidden' id='solicitacao' paciente='{$paciente}' />";
        echo "<h3><center>$nome</center></h3>";
          echo"<br><br><div id='field_avulsa'>
                    <p>
                          <fieldset>
                             <legend>Entrega sem caráter imediato</legend>
                             <div class='ui-state-highlight' style=\"padding: 5px;\">
                              <span class=\"ui-icon  ui-icon-info icon-inline\" ></span>
                              Para os itens \"sem\" caráter de entrega imediata, o prazo mínimo é de 24 horas.
                              </div>
                             <br/>
                             <b>Data </b>
                             <input type='text' class=' ' id='data-avulsa' name='data-avulsa' size='9px' />
                             <b>Hora: </b>
                             <input type='text'  class='hora'  id='hora-avulsa' name='hora-avulsa' size='9px'/>
                          </fieldset>
                    </p>
                    <p>
                          <fieldset>
                             <legend>Entrega com caráter imediato</legend>
                            <div class='ui-state-highlight' style=\"padding: 5px;\">
                              <span class=\"ui-icon  ui-icon-info icon-inline\" ></span>
                              Não existe um prazo mínimo para os itens \"com\" caráter de entrega imediata.
                            </div>
                            <br/><b>Data</b>
                            <input type='text'  id='dataEntregaImediata'  size='9px' />
                            <b>Hora: </b>
                            <input type='text'   class='hora'  id='horaEntregaImediata'  size='9px'/>
                          </fieldset>
                     </p>
                </div>";
        echo"<br>";
        $sql="SELECT * FROM (
                        SELECT *
                        FROM equipamentosativos
                        WHERE equipamentosativos.PACIENTE_ID = {$paciente}
                        AND equipamentosativos.DATA_FIM = '0000-00-00 00:00:00'
                        AND equipamentosativos.DATA_INICIO > '0000-00-00 00:00:00'
                        ORDER BY equipamentosativos.COBRANCA_PLANOS_ID, equipamentosativos.DATA_INICIO DESC
                        ) as equipamentosativos
                        GROUP BY equipamentosativos.COBRANCA_PLANOS_ID order by ID desc  ";
        $result = mysql_query($sql);
        $idEquipamentoAtivo = mysql_result($result, 0,0);
        if ($idEquipamentoAtivo > 0){
            echo "<p><center><h2>Existe equipamento ativo para o paciente.</h2></center></p>";
        }
      $result = mysql_query($sql);
        echo"<input type='checkbox' id='equip_ativo' />Equipamentos.";
        echo"<div id='span_equipamentos_ativos' style='display:none;'>";

                $equipamento = '';
                while($row=mysql_fetch_array($result)){
                $equipamento[$row['COBRANCA_PLANOS_ID']] = array("item"=>$row['item'],"und"=>$row['unidade'],"tipo"=>$row['tipo'],"qtd"=>$row['QTD'],"obs"=>$row['OBS'],"idativo"=>$row['idativo'],"data"=>$row['DATA']);
                }


                $sql2= "select * from cobrancaplanos where tipo=1 and ATIVO=1 ORDER BY item";

                $result2=mysql_query($sql2);

                echo"<table id='equipamentos_ativos'  >";
                echo"<tr bgcolor='grey'>"
                            . "<td><b>Item</b></td>"
                            . "<td ><b>A&ccedil;&atilde;o</b></td>"
                            . "<td><b>Qtd.</b></td>"
                            . "<td><b>Ultima Substitui&ccedil;&atilde;o</b></td>"
                            ."<td><b>Justificativa</b></td>"
                            . "<td><b>Ent. Imediata</b></td>"
                    . "</tr>";
                 $cont_cor=0;
                while($row=mysql_fetch_array($result2)){

                    if( $cont_cor%2 ==0 ){
				$cor = '';
			}else{
				$cor="bgcolor='#EEEEEE'";
			}
                        $cont_cor++;

                    if(array_key_exists($row['id'], $equipamento)){
                        //$check = "checked='checked'";
                        $qtd = $equipamento[$row['id']]['qtd'];
                        $obs = $equipamento[$row['id']]['obs'];
                        $equipamenentoAtivo = 1;
                        $data = $equipamento[$row['id']]['data'];

                        $options= "<option value='1' class='verde' >Solicitar</option>"
                                . "<option value='3' class='laranja' >Substituir</option>"
                                . "<option value='2' class='vermelho' >Recolher</option>";


                    }else{

                        $qtd='';
                        $obs = '';
                       $equipamenentoAtivo =0;
                        $data = '';
                        $botao = '';
                        $options= "<option value='1' class='verde' >Solicitar</option>";


                    }
                    $select= "<select class='acao-equipamento' data-equipamento-ativo='{$equipamentoAtivo}' data-id-capmedica='{$x}' data-class-remove='' id='select-acao"."{$row['id']}' data-idequipamento='{$row['id']}' style='max-width:104px;' >
                                 <option value='-1' class='branco'>Selecione</option>
                                 {$options}
                              </select>";
                    //echo"<tr><td><input type='checkbox' $check class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}'></input>";
                    echo "<tr $cor >"
                            . "<td>{$row['item']}</td>"
                            . "<td>$select</td>"
                            . "<td><input type='texto' readonly id='qtd"."{$row['id']}"."'  size='3px' class='qtd_equipamento' value=''  name='qtd_equipamento'  {$acesso} ></input></td>"
                            . "<td><input type='texto' readonly id='data"."{$row['id']}"."' class='data_equipamento' value='{$data}' name='data_equipamento'  size='6px' {$acesso}></input></td>"
                            . "<td><textarea readonly id='observacao"."{$row['id']}"."'></textarea></td>"
                            . "<td><input type='checkbox' class='entrega-imediata' id='entrega-imediata"."{$row['id']}' disabled='disabled' /></td>"
                         . "</tr>";
                }

        echo"</table></div>";


            $array1 = json_encode($array_equipamentos);

        echo "<input type='hidden' id='array_equipamentos' value='{$array1}' />";
        $botoes = "<img class='add-kit' tipo='kits' item='avulsa' src='../utils/kit_48x48.png' title='Adicionar kit' border='0'>&nbsp;&nbsp;";
        //$botoes .= "<img class='add-med' tipo='med' item='avulsa' src='../utils/medicamento_48x48.png' title='Adicionar medicamento' border='0'>&nbsp;&nbsp;";
        $botoes .= "<img class='add-mat' tipo='mat' item='avulsa' src='../utils/material_48x48.png' title='Adicionar material' border='0'>&nbsp;&nbsp;";
        //$botoes .= "<img class='add-die' tipo='die' item='avulsa' src='../utils/formula_48x48.png' title='Adicionar Dieta' border='0'>&nbsp;&nbsp;";
        echo "<center>$botoes</center>";
        $kits = "<input type='hidden' name='itens_kits_excluidos' id='itens-kits-excluidos' value=''>
                    <table class='mytable' width=100% id='kits-avulsa'><thead><tr><th>KITS</th></tr></thead></table>";
        //$med = "<table class='mytable' width=100% id='med-avulsa'><thead><tr><th>Medicamentos</th></tr></thead></table>";
        $mat = "<table class='mytable' width=100% id='mat-avulsa'><thead><tr><th>Materiais</th></tr></thead></table>";
        //$dieta = "<table class='mytable' width=100% id='die-avulsa'><thead><tr><th>Dietas</th></tr></thead></table>";
        echo "$kits<br/>$med<br/>$mat<br/>$dieta<br/>";
        echo "<button id='salvar-avulsa' data='{$data_avulsa}' hora='{$hora_avulsa}'>Salvar</button></div>";
  	}
  }

  public function periodo($post = []){
  	echo "<br/><fieldset style='width:375px;text-align:center;'><legend><b>Per&iacute;odo</b></legend>";
  	echo "DE"."<input type='text' name='inicio' value='" . ($post['inicio'] == '' ? $this->inicio : $post['inicio']) . "' maxlength='10' size='10' class='OBG inicio-periodo' />".
  	" AT&Eacute; <input type='text' name='fim' value='" . ($post['fim'] == '' ? $this->fim : $post['fim']) . "' maxlength='10' size='10' class='OBG fim-periodo' /></fieldset>";
  }

  private function buscar_solicitacoes($paciente,$inicio,$fim){

  	//echo "<center><h1>Buscar Prescri&ccedil;&otilde;es</h1></center>";
  	echo "<fieldset style='margin:0 auto;'>";
  	echo "<div id='div-busca' >
<div><center><h1>Buscar Solicitações / Prescrições </h1></center></div>";

  	echo alerta();

  	echo "<div style='float:right;width:100%;' id='buscar-solicitacao'>";

  	echo $this->pacientes_presc('OBG_CHOSEN');
  	echo $this->periodo();
    echo "<br><label for ='select-pendentes' ><b>Apenas solicitações pendentes?</b></label>
            <select id='select-pendentes' class = 'OBG' >
            <option value='S'>Sim</option>
            <option selected value='N'>Não</option>

            </select>";


  	echo "<p><button id='buscar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Buscar</span></button>";


  	echo "</div>";

  	echo "</div>";
  	echo "</fieldset>";
  	echo "<div id='div-resultado-busca'></div>";
  }

  private function check(){
      $html = "<table width='100%' border='0'>";
      $html .= "<thead>";
      $html .= "<tr>";
      $html .= "<th>Produto</th>";
      $html .= "<th></th>";
      $html .= "<th></th>";
      $html .= "</tr>";
      $html .= "</thead>";
      $html .= "</table>";
  }
    public function detalhePrescricao($p){
        $sqlPrescricao = "SELECT
usuarios.nome,
DATE_FORMAT(prescricoes.inicio,'%d/%m/%Y') as  BrInicio,
DATE_FORMAT(prescricoes.fim,'%d/%m/%Y') as BrFim,
DATE_FORMAT(prescricoes.`data`,'%d/%m/%Y') as BrData,
itens_prescricao.descricao,
motivo_ajuste

FROM
prescricoes
INNER JOIN itens_prescricao ON prescricoes.id = itens_prescricao.idPrescricao
INNER JOIN usuarios ON prescricoes.criador = usuarios.idUsuarios
WHERE
prescricoes.id = {$p}";
        $resultPrescricao = mysql_query($sqlPrescricao);
        $contPrescricao = 0;
        $htmlPrescricao = "<div>
        <button id='visulaizar-detalhe-prescricao' visualizar=0
        class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'
         role='button' aria-disabled='false' >
         <span class='ui-button-text'>Clique aqui para visualizar a prescrição.</span>
         </button>
         </div>";
        $responsePrescricao = \App\Models\Medico\Prescricao::getById($p);
        if(!empty($responsePrescricao['motivo_ajuste'])) {
            $htmlPrescricao .= "
            <br>
            <div class='ui-state-highlight' style='padding: 5px;'>
                <span class='ui-icon  ui-icon-alert icon-inline' ></span>
                <b>Motivo do Ajuste da Prescrição Aditiva: </b> 
                {$responsePrescricao['motivo_ajuste']}
            </div>
            <br>";
        }

        while ($row = mysql_fetch_array($resultPrescricao)){

            if($contPrescricao == 0){
                $htmlPrescricao   .= " <table width=100% class='mytable' id ='tabela-detalhe-prescricao' >
                 <thead>
                    <tr>
                    <th><center>{$row['nome']} período {$row['BrInicio']} {$row['BrFim']} </center></th>
                    </tr>
                 </thead>";
            }
            if ($contPrescricao % 2 == 0)
                $cor = '#E9F4F8';
            else
                $cor = '#FFFFFF';
            $contPrescricao++;
            $htmlPrescricao .= "<tr bgcolor={$cor}><td> {$row['descricao']}</td></tr>";


        }
        $htmlPrescricao .= "</table>";
        return $htmlPrescricao;

    }

    //! Exibe os detalhes de um prescrição finalizada.
    private function detalhes($p,$tipo,$pendente = null,$carater){
        $css = <<<CSS
.trocados {
    border-left: 5px solid red;
    list-style-type: none;
    padding-left: 5px;
}
.trocados li {
    padding: 5px;
}
CSS;
        echo <<<HTML
<style>
{$css}
</style>
HTML;

        $condicaoIdPrescricao = $tipo == 'med' ? "idPrescricao = {$p}" : "id_prescricao_curativo = {$p} ";

        if(!empty($pendente)){
            $condPendente = $pendente == 'S' ? " and s.PENDENTE = 'S'" : '';

        }

        $sql = "SELECT u.nome as enfermeiro,"
            . "DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA FROM solicitacoes as s INNER JOIN "
            . "usuarios as u on (s.enfermeiro = u.idUsuarios)
            WHERE $condicaoIdPrescricao ORDER BY s.idSolicitacoes ASC limit 1";

        $result_solicitante = mysql_query($sql);
        while ($row = mysql_fetch_array($result_solicitante)){
            $solicitante = $row['enfermeiro'];
            $data = $row['DATA'];
        }
        echo "<center><h1>Detalhes das Solicita&ccedil&otilde;es</h1></center>";
        echo $this->detalhePrescricao($p);

        echo "<p><b>Solicitante:</b> {$solicitante}";
        echo "<p><b>Data da Solicita&ccedil;&atilde;o:</b> {$data}";
        echo "<p><b style='color: red;'>* No caso de itens trocados, o último item da lista corresponde ao item enviado em definitivo.</b>";
        echo "<table width=100% class='mytable'><thead><tr>";
        echo "<th width='60%'><b>Item</b></th>";
        echo "<th width='10%'><b>Quantidade</b></th>";
        echo "<th><b>Status do  Item</b></th>";
        echo "</tr></thead>";


        $sql = "
  SELECT
	UPPER(c.nome),
	s.idSolicitacoes AS sol,
	DATE_FORMAT(s. DATA, '%d/%m/%Y') AS DATA,
	DATE_FORMAT(s.data_auditado, '%d/%m/%Y') AS dataAuditado,
	CONCAT(
		b.principio,
		' - ',
		b.apresentacao
	) AS produto,
	s.*,
        just.JUSTIFICATIVA AS just_negado
FROM
	solicitacoes AS s INNER JOIN
	catalogo AS b ON (s.CATALOGO_ID=b.ID) INNER JOIN
	clientes AS c ON  (s.paciente =c.idclientes )
   LEFT JOIN justificativa_itens_negados AS just ON (s.JUSTIFICATIVA_ITENS_NEGADOS_ID = just.ID)
WHERE
	s.{$condicaoIdPrescricao}

AND s.tipo IN (0, 1, 3)
$condPendente

UNION
	SELECT
		UPPER(c.nome),
		s.idSolicitacoes AS sol,
		DATE_FORMAT(s. DATA, '%d/%m/%Y') AS DATA,
		DATE_FORMAT(s.data_auditado, '%d/%m/%Y') AS dataAuditado,
		cobrancaplanos.item AS produto,
		s.*,
                just.JUSTIFICATIVA AS just_negado
	FROM
		solicitacoes AS s
	INNER JOIN cobrancaplanos ON (
		cobrancaplanos.ID = s.CATALOGO_ID
	)
	INNER JOIN clientes AS c ON (s.paciente = c.idClientes)
	LEFT JOIN justificativa_itens_negados AS just ON (s.JUSTIFICATIVA_ITENS_NEGADOS_ID = just.ID)
	WHERE
		s.{$condicaoIdPrescricao}
	AND s.tipo = 5
	$condPendente
	ORDER BY
		tipo,
		produto,
		autorizado,
		enviado";

        $result = mysql_query($sql);
        $n = 0;
        $data = [];
        while($row = mysql_fetch_array($result)) {
            $data[] = $row;
        }

        $aux = [];
        foreach ($data as $row) {
            $aux[$row['idSolicitacoes']] = $row;
        }
        sort($aux);

        $trocados = [];
        foreach ($aux as $row) {
            if($row['trocado'] == 'S' && !$this->in_array_r($row['idSolicitacoes'], $trocados)) {
                $trocados[$row['idSolicitacoes']] = $this->buscarTrocas($row['idSolicitacoes']);
            }
        }

        foreach($data as $row){
            if($row['inserido_troca'] == 'N') {
                if($n++%2==0)
                    $cor = '#E9F4F8';
                else
                    $cor = '#FFFFFF';

                $i = implode("/",array_reverse(explode("-",$row['inicio'])));
                $f = implode("/",array_reverse(explode("-",$row['fim'])));

                $enviados = $row['enviado'];
                $msg='';
                $motivo='';
                $htmlAjuste ='';
                // ajuste de quantidade liberada
                $ajustes = Solicitacoes::getAjusteBySolicitacaoId($row['idSolicitacoes']);
                $quantidadAjuste = 0;
                if(!empty($ajustes)){
                    $htmlAjuste .= "<ul class='ajusteOrange'> <b>Ajuste feito pela logística:</b>";
                    foreach($ajustes as $a){
                        $labelItem = $a['quantidade'] == 1 ? ' item' :' itens';
                        $quantidadAjuste += $a['quantidade'];
                        $htmlAjuste .= "<li>Abateu {$a['quantidade']} {$labelItem} em {$a['dataBr']} por {$a['usuario']}<br>
                                    <b>Justificativa: </b>{$a['justificativa']}
                              </li>";
                    }
                    $htmlAjuste.= "</ul>";
                }
                $msg = "<ul>";
                if($row['data_auditado'] == '0000-00-00 00:00:00' && $row['PENDENTE'] == 'N' && $row['entrega_imediata'] == 'N' && $row['status']==0){
                    $msg .= "<li>O item ainda não foi auditado</li>";
                }else if($row['entrega_imediata'] == 'S'){
                    $msg .= "<li>Item com caráter de Entrega Imediata. Enviado(s) = {$enviados}.</li> <li>Obs: {$row['obs']}</li>";
                }else if($row['data_auditado'] == '0000-00-00 00:00:00' && $row['PENDENTE'] == 'S' && $row['entrega_imediata'] == 'N' && empty($row['obs'])){
                    $msg .= "<li>Item com pendencia na auditoria: {$row['OBS_PENDENTE']}</li>";
                }else if($row['data_auditado'] <> '0000-00-00 00:00:00' && $row['status'] <> -1
                    &&  $row['entrega_imediata'] == 'N'){
                    $qtdAutorizado = $row['autorizado'] + $quantidadAjuste;
                    $msg .= "<li>Item liberado pela auditoria em {$row['dataAuditado']}.</li> 
                             <li>Qtd autorizada: {$qtdAutorizado} </li> ";
                                if($quantidadAjuste > 0){
                                   $msg .= "<li>Qtd abatida pela logística {$quantidadAjuste}.</li>";
                                }
                    $msg .= "<li>Enviado(s) pela logística: {$enviados}.</li>
                             <li>Obs: {$row['obs']}</li>";
                }else if($row['data_auditado'] <> '0000-00-00 00:00:00' && $row['status'] == -1
                    &&  $row['entrega_imediata'] == 'N'){
                    $motivo = $row['JUSTIFICATIVA_ITENS_NEGADOS_ID'] == 6 ? $row['obs'] : $row['just_negado'];
                    $msg .= "<li>Item negado pela auditoria: {$motivo}</li>";
                }else if(($row['data_auditado'] == '0000-00-00 00:00:00' && $row['entrega_imediata'] == 'N') && !empty($row['obs'])){
                    $labelEnviados = $enviados > 0 ? ". Enviado(s) = {$enviados}." : "";
                    $msg .= "<li>" . $row['obs'] . $labelEnviados . "</li>";
                }
                $msg .= "</ul>";
                echo "<tr bgcolor={$cor} >";
                echo "<td>{$row['idSolicitacoes']} - {$row['produto']}";
                echo $htmlAjuste;
                $saidaItemPrincipal = Saidas::getSaidaByIdSolicitacao($row['idSolicitacoes']);
                if(!empty($saidaItemPrincipal)){
                    echo "<ul class='enviadosGreen'> <b>Itens enviados pela Logística:</b>";
                    foreach($saidaItemPrincipal as $itemPrincipal){
                        echo "<li>{$itemPrincipal['item']}  <b>Qtd:</b> {$itemPrincipal['quantidade']}
                                    <b>Lote:</b> {$itemPrincipal['LOTE']} <b>Data:</b> {$itemPrincipal['data']}
                              </li>";

                    }
                    echo "</ul>";
                }
                if(array_key_exists($row['idSolicitacoes'], $trocados)){
                    sort($trocados[$row['idSolicitacoes']]);
                    echo "<ul class='trocados'> <b>Trocado pelo(s) item(ns):</b>";
                    foreach ($trocados[$row['idSolicitacoes']] as $itemTrocado) {
                        echo "<li>{$itemTrocado['idSolicitacoes']} - {$itemTrocado['produto']} ";
                        $msgItem = '';
                        if($itemTrocado['data_auditado'] == '0000-00-00 00:00:00' ){

                            $msgItem =  $itemTrocado['entrega_imediata'] == 'N' ? 'O item não foi auditado':
                                "Entrega Imediata. Enviado(s): {$itemTrocado['enviado']}";

                        }else{
                            $msgItem = $row['data_auditado'] != '0000-00-00 00:00:00' ? "Autorizado(s) em {$row['dataAuditado']}:" : "Autorizado(s):";
                            $msgItem .= " {$itemTrocado['autorizado']}. Enviado(s):{$itemTrocado['enviado']} ";

                        }
                        echo "<b>{$msgItem} <br>  Obs: {$itemTrocado['obs']}</b></li>";

                        $saidaItemTrocado = Saidas::getSaidaByIdSolicitacao($itemTrocado['idSolicitacoes']);
                        if(!empty($saidaItemTrocado)){
                            echo "<ul class='enviadosGreen'> <b>Itens enviados pela Logística:</b>";
                            foreach($saidaItemTrocado as $item){
                                echo "<li>{$item['item']}  <b>Qtd:</b> {$item['quantidade']}
                                    <b>Lote:</b> {$item['LOTE']} <b>Data:</b> {$item['data']}
                              </li>";

                            }
                            echo "</ul>";
                        }
                    }
                    echo "</ul>
                  </td>";

                } else {
                    echo "</td>";
                }
                echo "<td>{$row['qtd']}</td>";
                echo "<td>{$msg}</td>";
                echo "</tr>";
            }
        }
        if($carater == 4 || $carater == 7){
            $equipamentosAvaliacao = <<<SQL
SELECT
cobrancaplanos.item,
equipamentosativos.QTD
FROM
equipamentosativos
INNER JOIN cobrancaplanos ON equipamentosativos.COBRANCA_PLANOS_ID = cobrancaplanos.id
where
equipamentosativos.PRESCRICAO_ID = $p
AND equipamentosativos.PRESCRICAO_AVALIACAO = 's'
SQL;
            $result = mysql_query($equipamentosAvaliacao);
            $n = 0;
            $data = [];
            while($row = mysql_fetch_array($result)) {

                    if($n++%2==0)
                        $cor = '#E9F4F8';
                    else
                        $cor = '#FFFFFF';
                echo "<tr bgcolor={$cor} ><td>{$row['item']}</td><td>{$row['QTD']}</td><td></td></tr>";
            }

        }
        echo "</table>";

        $responsePaciente = ModelPaciente::getById($id_paciente);

        echo "<div>
                <button caminho='imprimir.php?tipo={$tipo}&p={$p}' empresa-relatorio='{$responsePaciente['empresa']}' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  role='button' aria-disabled='false'>
                    <span class='ui-button-text'>
                        Imprimir
                    </span>
                </button>
                <button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>
              </div>";
    }

    private function menuRelatorios(){
      echo"<div><center><h1>Relatórios</h1></center></div>";
        if(isset($_SESSION['permissoes']['enfermagem']['enfermagem_relatorio']['enfermagem_relatorio_assistencial']) || $_SESSION['adm_user'] == 1) {
            echo "<p><a href='../relatorios/?medico-enfermagem-ur=form'>Relatórios Assistenciais</a></p>";
        }
        if(isset($_SESSION['permissoes']['enfermagem']['enfermagem_relatorio']['enfermagem_relatorio_nao_conformidade']) || $_SESSION['adm_user'] == 1) {
            echo "<p><a href='../auditoria/relatorios/?action=relatorio-nao-conformidades-gerenciar&from=enfermagem'>Relatório de Não Conformidades</a></p>";
        }
        if(isset($_SESSION['permissoes']['enfermagem']['enfermagem_relatorio']['enfermagem_relatorio_plano_tratamento_ferida']) || $_SESSION['adm_user'] == 1) {
            echo "<hr>
            <p>
                <img src='/utils/logo-planserv.jpg' width='100' height='30' />
                <ul style='list-style-type: none;'>
                    <li>
                        <a href='/enfermagem/planserv/?action=feridas'>Plano Terapêutico Tratamento de Feridas</a>
                    </li>
                </ul>
            </p>";
        }
    }

    private function in_array_r($needle, $haystack, $strict = false)
    {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }

        return false;
    }


    private function buscaFilho($solicitacao, &$idSolicitacoes){
        $sql = <<<SQL
SELECT
  historico_troca_item.solicitacao_id_substituta
FROM
  historico_troca_item
WHERE
  historico_troca_item.solicitacao_id = '{$solicitacao}'
SQL;

        $rs = mysql_query($sql);

        while($filhos = mysql_fetch_array($rs)){
            $idSolicitacoes[$filhos['solicitacao_id_substituta']]	= $filhos['solicitacao_id_substituta'];
        }
        return $idSolicitacoes;
    }

    private function buscarTrocas($solicitacao, &$trocados = [])
    {
        $sql = <<<SQL
SELECT
  historico_troca_item.solicitacao_id_substituta
FROM
  historico_troca_item
WHERE
  historico_troca_item.solicitacao_id = '{$solicitacao}'
SQL;

        $rs = mysql_query($sql);

        $idSolicitacoes = [];
// pega od ID das solicitações de todos os filhos da solicitação original.
        while($filhos = mysql_fetch_array($rs)){
            $idSolicitacoes[$filhos['solicitacao_id_substituta']]	= $filhos['solicitacao_id_substituta'];
        }
        $cont = 0;
        $x = count($idSolicitacoes);
// pegar o ID das solicitações dos filhos dos filhos até o fim das gerações da solicitação original.
        while($cont != $x){
            $cont = 0;
            foreach( $idSolicitacoes as $id){
                $this->buscaFilho($id,$idSolicitacoes);
                $cont++;
            }
            $x = count($idSolicitacoes);
        }
//com todos os IDs geradas a partir da solicitação original agora pega os dados  implemento o arrai $trocados, com os itens que devem ser exibidos.
        foreach( $idSolicitacoes as $id){
            $this->getSolicitacoes($id,$trocados);
        }
        return $trocados;
    }

    private function getSolicitacoes($solicitacao, &$trocados = [])
    {

        $sql = <<<SQL
SELECT
  solicitacoes.*,
  CONCAT(
		catalogo.principio,
		' - ',
		catalogo.apresentacao
	) AS produto
FROM
  solicitacoes
  INNER JOIN catalogo ON solicitacoes.CATALOGO_ID = catalogo.ID
WHERE
  idSolicitacoes = '{$solicitacao}'

SQL;
        $rs = mysql_query($sql);
        $row = mysql_fetch_array($rs, MYSQL_ASSOC);
        // faz o filtro das solicitações que devem ser exibidas. Solicitações que foram trocadas e não tiveram nenhum item trocado
        //   não devem aparece.
        if(($row['trocado'] == 'S' && $row['enviado'] > 0) || $row['trocado'] != 'S' ){
            $trocados[$row['idSolicitacoes']] = $row;
        }
        return $trocados;
    }

  public function view($v,$p){
  	switch ($v) {
  		case "menu":
  			$this->menu();
  			break;
        case "relatorios":
            $this->menuRelatorios();
            break;
  		case "solicitacoes":
  			$l = NULL;
  			$antiga = NULL;
  			$data = date("d/m/Y");
  			$pantiga = -1;
  			$paciente = -1;
  			$carater='';
  			if(isset($_REQUEST['label'])) $l = $_REQUEST['label'];
  			if(isset($_REQUEST["data"])) $data = $_REQUEST["data"];
  			if(isset($_REQUEST["antigas"])) $antiga = $_REQUEST["antigas"];
  			if(isset($_REQUEST["paciente"])) $paciente = $_REQUEST["paciente"];
  			if(isset($_REQUEST["carater"])) $carater = $_REQUEST["carater"];
  			$this->prescricoes($l,$data,$antiga,$paciente,$carater);
  			break;
  		case "kits":
  			$this->kits();
  			break;
  		case "buscas":
  			$this->buscas();
  			break;
  		case "detalhes":
  			$this->detalhes($_REQUEST['p'],$_REQUEST['tipo'],$_REQUEST['pendente'],$_REQUEST['carater']);
  			break;
  		case "avulsa":
  			$emergencia = 0;
  			if(isset($_POST['emergencia'])){
                            $emergencia = $_POST['emergencia'];
                        }
  			$paciente = NULL;
  			if(isset($_POST['pacientes'])){
                            $paciente = $_POST['pacientes'];
                        }
  			$npaciente = NULL;
  			//jeferson
  			if(isset($_POST['data-avulsa'])){
                            $data = $_POST['data-avulsa'];
                        }
  			if(isset($_POST['hora-avulsa'])){
                            $hora = $_POST['hora-avulsa'];
                        }
  			//jeferson
  			if(isset($_POST['npaciente'])){
                            $npaciente = ucwords(strtolower($_POST['npaciente']));
                        }
  			$this->avulsa($paciente,$npaciente,$emergencia,$data,$hora);
  			break;
  		case "buscar":
  			$paciente = NULL;
  			if(isset($_POST['pacientes'])) $paciente = $_POST['pacientes'];
  			$inicio = NULL;
  			if(isset($_POST['pacientes'])) $inicio = $_POST['inicio'];
  			$fim = NULL;
  			if(isset($_POST['pacientes'])) $fim = $_POST['fim'];

  			$this->buscar_solicitacoes($paciente,$inicio,$fim);
  			break;
  			// Eriton
  			case "buscar_avulsa":
  				$paciente = NULL;
  				if(isset($_POST['pacientes'])) $paciente = $_POST['pacientes'];
  				$inicio = NULL;
  				if(isset($_POST['pacientes'])) $inicio = $_POST['inicio'];
  				$fim = NULL;
  				if(isset($_POST['pacientes'])) $fim = $_POST['fim'];

  				$this->buscar_avulsa($paciente,$inicio,$fim);
  				break;
  				//Fim Eriton
  		case "checklist":
  			$checklist = new Checklist;
                        $checklist->chamar_checklist();
                        //$this->checklist($paciente,$npaciente,$emergencia);
  			break;

                case "estoque":
                    $estoque = new Checklist;
                    $estoque->mostrar_estoque();
                    break;

  		case "listar":
  			//echo "<button id='tt'>dcgfgh</button>";
  			//Paciente::listar();
  			//$p = new Paciente();
  			$p->listar();
  			break;

  		case "listcapmed":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->listar_capitacao_medica($_GET['id']);
  			}
  			break;

  		case "listfichaevolucao":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->listar_evolucao_medica($_GET['id']);
  			}
  			break;

  		case "novaavaliacaoenf":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->nova_ficha_avaliacao_enfermagem($_GET['id']);
  			}
  			break;

  		case "novaevolucaoenf":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->nova_ficha_evolucao_enfermagem($_GET['id']);
  			}
  			break;


  		case "listavaliacaenfermagem":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->listar_ficha_avaliacao_enfermagem($_GET['id']);
  			}
  			break;

  		case "listevolucaoenfermagem":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->listar_ficha_evolucao_enfermagem($_GET['id']);
  			}
  			break;

  		case "editaravaliacaoenfermagem":
  				if(isset($_GET['id'])){
  					$p = new Paciente();
  					$p->nova_editar_ficha_avaliacao_enfermagem($_GET['id'],$_GET['v']);
  				}
  				break;

  		case "editarevolucaoenfermagem":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->nova_editar_ficha_evolucao_enfermagem($_GET['id'],$_GET['v']);
  			}
  			break;

  		/*case "listfichaevolucao":

  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->listar_evolucao_medica($_GET['id']);
  			}
  			break;*/
  		case "show":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->form($_GET['id'],'readonly=readonly');
  			}
  			break;


  			if(isset($_GET['id'])){

  				$p = new Paciente();
  				$p->detalhes_ficha_evolucao_medica($_GET['id']);
  			}
  			break;
  		case "capmed":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->detalhes_capitacao_medica($_GET['id']);
  				}
  		case "detalhescore":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->detalhe_score($_GET['id']);
  					}

  		case "detalhesprescricao":
  			if(isset($_GET['id']) && isset($_GET['idpac'])){
  				$p = new Paciente();
  				$p->detalhe_prescricao($_GET['id'],$_GET['idpac']);
  						}
  						break;
  		//Eriton 20-03-2013
  		case "frequencia_uso":
                if (isset($_POST['id'])) {
                    $p = new Paciente();
                    $p->frequencia_uso($_POST['id']);
                }
                break;
            case "relatorio":
                if (isset($_GET['opcao'])) {
                    switch ($_GET['opcao']) {
                        case "1":
                            if (isset($_GET['idp']) && !isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_alta($_GET['idp']);
                            } elseif (isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_alta($_GET['id'], $_GET['idr'], $_GET['editar']);
                            } else {
                                $r = new Relatorios();
                                $r->listar_alta_enfermagem($_GET['id']);
                            }
                            break;
                        case "2":
                            if (isset($_GET['idp']) && !isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_aditivo($_GET['idp']);
                            } elseif (isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_aditivo($_GET['id'], $_GET['idr'], $_GET['editar']);
                            } else {
                                $r = new Relatorios();
                                $r->listar_aditiva_enfermagem($_GET['id']);
                            }
                            break;
                        case "3":
                            if (isset($_GET['idp']) && !isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_prorrogacao($_GET['idp']);
                            } elseif (isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_prorrogacao($_GET['id'], $_GET['idr'], $_GET['editar']);
                            } else {
                                $r = new Relatorios();
                                $r->listar_prorrogacao_enfermagem($_GET['id']);
                            }
                            break;
                        case "4":
                            if (isset($_GET['idp']) && !isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_visita($_GET['idp']);
                            } elseif (isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_visita($_GET['id'], $_GET['idr'], $_GET['editar']);
                            } else {
                                $r = new Relatorios();
                                $r->listar_visita_semanal($_GET['id']);
                            }
                            break;
                        case "5":
                            if (isset($_GET['idp']) && !isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_deflagrado($_GET['idp']);
                            } elseif (isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_deflagrado($_GET['id'], $_GET['idr'], $_GET['editar']);
                            } else {
                                $r = new Relatorios();
                                $r->listar_relatorio_deflagrado($_GET['id']);
                            }
                            break;
                    }
                }
                /* $r =new Relatorios();
                  $r->relatorio_alta($_GET['id']); */
                break;
            case "visualizar_rel_prorrogacao":
                $r = new Relatorios();
                $r->visualizar_rel_prorrogacao($_GET['id'], $_GET['idr']);
                break;
            case "visualizar_rel_alta":
                $r = new Relatorios();
                $r->visualizar_rel_alta($_GET['id'], $_GET['idr']);
                break;
            case "visualizar_rel_aditivo":
                $r = new Relatorios();
                $r->visualizar_rel_aditivo($_GET['id'], $_GET['idr'],$_GET['confirmar']);
                break;
            case "visualizar_rel_visita":
                $r = new Relatorios();
                $r->visualizar_visita_semanal($_GET['id'], $_GET['idr']);
                break;
            case "visualizar_rel_deflagrado":
                $r = new Relatorios();
                $r->visualizar_relatorio_deflagrado($_GET['id'], $_GET['idr']);
                break;
		//Fim Eriton 20-03-2013

  	}

  }



}
