<?php
$id = session_id();
if(empty($id))
    session_start();
include_once('../validar.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../db/config.php');
require __DIR__ . '/../vendor/autoload.php';

use \App\Controllers\Administracao as Administracao;
use \App\Models\Administracao\Filial;

$p = $_REQUEST['p'];
$tipo = $_REQUEST['tipo'];
$empresa = $_REQUEST['empresa'];


$condicaoIdPrescricao = $tipo == 'med' ? "idPrescricao = {$p}" : "id_prescricao_curativo = {$p} ";

$sqlData = <<<SQL
select
p.inicio,
p.fim
FROM
prescricoes as p
WHERE id = {$p}
SQL;
if($tipo == 'enf') {
    $sqlData = <<<SQL
select
p.inicio,
p.fim
FROM
prescricao_curativo as p
WHERE id = {$p}
SQL;
}
$result = mysql_query($sqlData);
while ($row = mysql_fetch_array($result)){
    $dt_inicio = $row['inicio'];
    $dt_fim = $row['fim'];
}

$sql = "
  SELECT
	s.*,
        UPPER(c.nome) AS paciente,
	s.idSolicitacoes AS sol,
	DATE_FORMAT(s. DATA, '%d/%m/%Y') AS DATA,
	CONCAT(
		b.principio,
		' - ',
		b.apresentacao
	) AS produto,
	UPPER(u.nome) AS enf,
	c.cuidador,
        just.JUSTIFICATIVA AS just_negado
FROM
	solicitacoes AS s inner join
	catalogo AS b on (b.ID = s.CATALOGO_ID) inner join
	clientes AS c on (c.idclientes = s.paciente) inner join
	usuarios AS u on (s.enfermeiro = u.idUsuarios) LEFT JOIN
        justificativa_itens_negados AS just ON (s.JUSTIFICATIVA_ITENS_NEGADOS_ID = just.ID)
WHERE
	s.$condicaoIdPrescricao
       AND s.tipo IN (0, 1, 3)

UNION
	SELECT
		s.*, 
		UPPER(c.nome) AS paciente,
		s.idSolicitacoes AS sol,
		DATE_FORMAT(s. DATA, '%d/%m/%Y') AS DATA,
		cobrancaplanos.item AS produto,
		UPPER(u.nome) AS enf,
		c.cuidador ,
        just.JUSTIFICATIVA AS just_negado
	FROM
		solicitacoes AS s
	INNER JOIN cobrancaplanos ON (
		cobrancaplanos.ID = s.CATALOGO_ID
	)
	INNER JOIN clientes AS c ON (s.paciente = c.idClientes)
	INNER JOIN usuarios AS u ON (s.enfermeiro = u.idUsuarios)
	 LEFT JOIN
        justificativa_itens_negados AS just ON (s.JUSTIFICATIVA_ITENS_NEGADOS_ID = just.ID)
	WHERE
		s.$condicaoIdPrescricao
	AND s.tipo = 5
	ORDER BY
		tipo,
		produto,
		autorizado,
		enviado";



$result1 = mysql_query($sql);
$flag = true;
$i = 0;
$responseEmpresa = Filial::getEmpresaById($empresa);
while($row1 = mysql_fetch_array($result1)){
    if(!($row['enviado'] == 0 && $row['trocado'] == 'S')) {
        if($i==0){
            $ip = $dt_inicio;
            $fp = $dt_fim;
            $img = "<img src='../utils/logos/{$responseEmpresa['logo']}' width='30%' style='align:center' />";
            $hoje = date("d/m/Y");
            $header = "<table border='1' width='100%' style='border:1px solid #000;border-collapse:collapse;'><thead><tr><th width='40%' rowspan='3' colspan='3' >{$img}<br/>{$responseEmpresa['nome']}</th><th width=20% colspan='5'>Prescrição ". join("/",array_reverse(explode("-",$row1['inicio'])))." a ". join("/",array_reverse(explode("-",$row1['fim'])))." - ".ucwords(strtolower($row1['paciente']))."</th>";
            $header .= "<th rowspan='3' colspan='2' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th></tr></tr>";
            $header .= "<tr><td colspan='5'><b>Cuidador: </b>".ucwords(strtolower($row1['cuidador']))." (".strtolower($row1['relcuid']).")</td></tr>";
            $tipo = "Enfermeiro(a)";

            $header .= "<tr><td colspan='5' ><b>Profissional: </b>".ucwords(strtolower(htmlentities($row1['enf'])))." ({$tipo})</td></tr></table><br/>";

            $tabela = "<table style='border:1px solid #000;border-collapse:collapse;' width='100%' ><thead><tr><th colspan='20' >Solicita&ccedil;&otilde;es </th></tr>";
            $tabela .= "<tr><th style='border:1px solid #000;'>Ord.</th><th colspan='8' style='width:500px;border:1px solid #000;'>Itens</th><th style='border:1px solid #000;'>Qtd.</th><th style='border:1px solid #000;'>Status do Item</th></tr></thead>";

        }
        $enviados = empty($row1['enviado']) ? 0:$row1['enviado'];
        $msg='';
        $motivo='';
        if($row1['data_auditado'] == '0000-00-00 00:00:00' && $row1['PENDENTE'] == 'N' && $row1['entrega_imediata'] == 'N' && $row1['status']==0){
            $msg = "O item ainda não foi auditado";
        }else if($row1['entrega_imediata'] == 'S'){
            $msg = "Item com caráter de Entrega Imediata. Enviado(s) = {$enviados}. {$row1['obs']}";
        }else if($row1['data_auditado'] == '0000-00-00 00:00:00' && $row1['PENDENTE'] == 'S' && $row1['entrega_imediata'] == 'N' && empty($row1['obs'])){
            $msg = "Item com pendencia na auditoria:{$row1['OBS_PENDENTE']}";
        }else if($row1['data_auditado'] <> '0000-00-00 00:00:00' && $row1['status'] <> -1
            &&  $row1['entrega_imediata'] == 'N'){
            $msg = "Item liberado pela auditoria. Autorizado(s): {$row1['autorizado']}. Enviados pela logística:{$enviados}. {$row1['obs']}";
        }else if($row1['data_auditado'] <> '0000-00-00 00:00:00' && $row1['status'] == -1
            &&  $row1['entrega_imediata'] == 'N'){
            $motivo = $row1['JUSTIFICATIVA_ITENS_NEGADOS_ID'] == 6 ? $row1['obs'] : $row1['just_negado'];
            $msg = "Item negado pela auditoria: {$motivo}";
        }else if($row1[$row1['data_auditado'] == '0000-00-00 00:00:00' &&  $row1['entrega_imediata'] == 'N'] && !empty($row1['obs'])){
            $msg = $row1['obs'];
        }

        if($row1['autorizado'] != 0){
            $autorizado = $row1['autorizado'];
        }else{
            $autorizado = "-";
        }

        $tabela .= "<tr><td align='center' style='width:5px;border:1px solid #000;'>{$i}</td><td style='border:1px solid #000;' colspan='8'>{$row1['produto']}</td><td style='border:1px solid #000;' align='center'>{$row1['qtd']}</td>
      <td style='border:1px solid #000;' align='center'>{$msg}</td></tr>";
        $i++;

    }
}
$paginas []= $header.$tabela."</table>";

$mpdf=new mPDF('pt','A4-L',9);
$mpdf->SetHeader('página {PAGENO} de {nbpg}');
$ano = date("Y");
$mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
$mpdf->WriteHTML("<html><body>");
$flag = false;
foreach($paginas as $pag){
    if($flag) $mpdf->WriteHTML("<formfeed>");
    $mpdf->WriteHTML($pag);
    $flag = true;
}
$mpdf->WriteHTML("</body></html>");
$mpdf->Output('prescricao.pdf','I');
exit;

?>
