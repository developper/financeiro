<?php
$id = session_id();
if (empty($id))
    session_start();
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';
require '../../utils/codigos.php';

use \App\Controllers\PrescricaoEnfermagem as Controller;
use	\App\Controllers\Prescricoes as Presc;
use \App\Controllers\Administracao;
use \App\Helpers\Image;

//ini_set('display_errors', 1);

if (isset($_GET['action']) && !empty($_GET['action'])) {

    $action = $_GET['action'];

    switch ($action) {
        case  'prescricao':
            $prescricao = 1;
            require '../enfermagem.php';
            $enfermagem = new Enfermagem();
            $caraterPrescricaoEnfermagem = Controller::caraterPrescricaoEnfermagem();

            require 'templates/gerenciar.phtml';
            break;

        case 'nova-presc':

            if(isset($_GET['paciente']) && isset($_GET['inicio']) && isset($_GET['fim']) && isset($_GET['carater']) ) {
                require '../paciente_enf.php';
                $paciente = new Paciente();
                $responsePaciente = \App\Models\Administracao\Paciente::getById($_GET['paciente']);

                $carater =$_GET['carater'];
                $idPaciente = $_GET['paciente'];
                $inicio = $_GET['inicio'];
                $fim = $_GET['fim'];
                $allCuidados=Controller::cuidadosAtivos();
                $allFrequencia = Controller::allFrequencias();
                $caraterPrescricaoEnfermagem = Controller::caraterPrescricaoEnfermagem();
                require 'templates/prescrever.phtml';
            }else{
                return false;
            }
            break;
        case 'usar-para-nova-prescricao':
            require '../paciente_enf.php';
            $paciente = new Paciente();
            $idPrescEnf = $_GET['idPresc'];
            $itensPrescEnf = Controller::getItensPrescricao($idPrescEnf);
            $carater =$itensPrescEnf[0]['carater'];
            $idPaciente = $itensPrescEnf[0]['cliente_id'];
            $inicio = $_GET['ini'];
            $fim = $_GET['fim'];
            $allCuidados=Controller::cuidadosAtivos();
            $allFrequencia = Controller::allFrequencias();
            $caraterPrescricaoEnfermagem = Controller::caraterPrescricaoEnfermagem();
            $responsePaciente = \App\Models\Administracao\Paciente::getById($idPaciente);
            require 'templates/prescrever.phtml';

            break;
        case 'visualizar':
            $idPrescEnf = $_GET['idPresc'];
            //static $idPaciente;
            if(isset($_GET['idPresc']) && !empty($_GET['idPresc'])){
                require '../paciente_enf.php';
                $paciente = new Paciente();
                $idPrescEnf = $_GET['idPresc'];
                $itensPrescEnf = Controller::getItensPrescricao($idPrescEnf);
                $caminho = "/enfermagem/imprimirPrescEnf.php?prescricao={$idPrescEnf}";
                require 'templates/visualizar.phtml';
            }
            break;
        case 'imprimir':
            $idPrescEnf = $_GET['idPresc'];
            //static $idPaciente;
            if(isset($_GET['idPresc']) && !empty($_GET['idPresc'])){
                require '../paciente_enf.php';
                $paciente = new Paciente();
                $idPrescEnf = $_GET['idPresc'];
                $itensPrescEnf = Controller::getItensPrescricao($idPrescEnf);
                require 'templates/imprimir.phtml';
            }
            break;
        case 'curativo':
            require '../enfermagem.php';
            $enfermagem = new Enfermagem();
            require 'templates/prescricoes-curativos.phtml';
            break;

        case 'nova-presc-curativo':

            if(isset($_GET['paciente']) && isset($_GET['inicio']) && isset($_GET['fim']) ) {
                require '../paciente_enf.php';
                $paciente 			= new Paciente();
                $locais_feridas = Presc::getAllLocaisFeridas();
                $frequencias 		= Presc::getAllFrequencias();
                $carateres			= Presc::getCarateresPrescricao();
                $id_paciente 		= $_GET['paciente'];
                $inicio 			= $_GET['inicio'];
                $fim 				= $_GET['fim'];
                $classMaxEntrega    = 'data-avulsa';
                require 'templates/prescrever-curativos.phtml';
            }else{
                $msg = 'Preencha todos os campos corretamente';
                require 'templates/prescricoes-curativos.phtml';
            }
            break;

        case 'editar-prescricao-curativo':

            if(isset($_GET['prescricao'])) {
                if(Presc::verificaPrescricaoAuditada($_GET['prescricao']) === 0) {
                    require '../paciente_enf.php';
                    $paciente = new Paciente();
                    $locais_feridas = Presc::getAllLocaisFeridas ();
                    $frequencias = Presc::getAllFrequencias ();
                    $carateres = Presc::getCarateresPrescricao ();
                    $prescricao = Presc::preencherPrescricaoCurativo ();

                    require 'templates/editar-curativos.phtml';
                } else {
                    $msg = base64_encode('Essa prescrição já foi auditada! Entre em contato com a Auditoria ou a Logística para cancelar a solicitação.');
                    header ("Location: {$_SERVER['HTTP_REFERER']}&msg={$msg}");
                }
            }else{
                $msg = 'Preencha todos os campos corretamente';
                require 'templates/prescricoes-curativos.phtml';
            }
            break;

        case 'usar-nova-prescricao-curativo':

            if(isset($_GET['prescricao'])) {
                require '../paciente_enf.php';
                $paciente = new Paciente();
                $locais_feridas = Presc::getAllLocaisFeridas();
                $frequencias 		= Presc::getAllFrequencias();
                $prescricao 		= Presc::preencherPrescricaoCurativo();
                $carateres			= Presc::getCarateresPrescricao();
                $usar_nova 			= true;
                require 'templates/editar-curativos.phtml';
            }else{
                $msg = 'Preencha todos os campos corretamente';
                require 'templates/prescricoes-curativos.phtml';
            }
            break;

        case 'visualizar-prescricao-curativo':

            if(isset($_GET['prescricao'])) {
                require '../paciente_enf.php';
                $paciente = new Paciente();
                $response = Presc::visualizarPrescricao ();
                $convenio = Presc::getPacienteById($response['prescricao']['paciente'])['convenio'];
                $caminho = "/enfermagem/imprimirPrescCurativo.php?prescricao={$_GET['prescricao']}";
                require 'templates/visualizar-prescricao-curativo.phtml';
            }
            break;

        case 'impressao-planserv':

            if(isset($_GET['prescricao'])) {
                list($processor, $fileName) = Presc::fichaPlanserv();

                $fileName   = 'temp/' . $fileName;
                $fileSize   = filesize($fileName);

                $processor->saveAs($fileName);
                ob_start();
                header('Content-Type: application/msword');
                header('Content-Description: File Transfer');
                header('Content-Disposition: attachment; filename=' . basename($fileName));
                header('Content-Transfer-Encoding: binary');
                header('Connection: Keep-Alive');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: cache');
                header('Content-Length: ' . $fileSize);
                ob_clean();
                flush();
                readfile($fileName);
                unlink($fileName);
            }
            break;

        case 'cancelar-prescricao-curativo':

            if(isset($_GET['prescricao']) && !empty($_GET['prescricao'])) {
                $msg = base64_encode('Essa prescrição já foi auditada! Entre em contato com a Auditoria ou a Logística para cancelar a solicitação.');
                if(Presc::cancelarPrescricaoCurativo())
                    $msg = base64_encode('Prescrição cancelada com sucesso!');
                header ("Location: {$_SERVER['HTTP_REFERER']}&msg={$msg}");
            }
            break;
    }
}

if (isset($_POST['query']) && !empty($_POST['query'])) {

    $action = $_POST['query'];
    switch ($action) {
        case  'salvar-presc-enfermagem':
            if(isset($_POST['idPaciente']) && !empty($_POST['idPaciente']) && isset($_POST['inicio']) && !empty($_POST['inicio'])
                && isset($_POST['fim']) && !empty($_POST['fim']) && isset($_POST['dados']) && !empty($_POST['dados'])
                && isset($_POST['carater']) && !empty($_POST['carater'])){
                $idPaciente = anti_injection($_POST['idPaciente'],'numerico');
                $inicio = anti_injection($_POST['inicio'],'literal');
                $fim= anti_injection($_POST['fim'],'literal');
                $dados =$_POST['dados'];
                $idUser = $_SESSION['id_user'];
                $carater = $_POST['carater'];
                $response = Controller::salvar($idPaciente,$inicio,$fim,$dados,$idUser,$carater);
            }

            return $response ;
            break;
        case 'buscar-prescricao':
            if(isset($_POST['pacientes']) && isset($_POST['inicio']) && isset($_POST['fim']) && isset($_POST['carater']) ){
                $prescricao = 1;
                $idPaciente = $_POST['pacientes'];
                $fim=$_POST['fim'];
                $inicio = $_POST['inicio'];
                $carater =$_POST['carater'];
                $caraterPrescricaoEnfermagem = Controller::caraterPrescricaoEnfermagem();
                require '../enfermagem.php';
                $enfermagem = new Enfermagem();
                $listaPrescricao = Controller::getPrescrioes($idPaciente,$inicio,$fim,$carater );
                require 'templates/gerenciar.phtml';
            }


            break;

        case 'salvar-prescricao-curativo':
            $prescricoes = new Presc();
            $response = $prescricoes->salvar();
            if(is_numeric($response)){
                header ("Location: /enfermagem/prescricao/?action=visualizar-prescricao-curativo&prescricao=" . $response);
            } else {
                $msg = $response;
                require 'templates/prescricoes-curativos.phtml';
            }
            break;

        case 'editar-prescricao-curativo':
            $response = Presc::atualizar();
            if(is_numeric($response)){
                header ("Location: /enfermagem/prescricao/?action=visualizar-prescricao-curativo&prescricao=" . $response);
                require 'templates/visualizar-prescricao-curativo.phtml';
            } else {
                $msg = $response;
                require 'templates/prescricoes-curativos.phtml';
            }
            break;
    }
}
if (isset($_GET['query']) && !empty($_GET['query'])) {
    $action = $_GET['query'];
    switch ($action) {
        case 'buscar-prescricao-curativo':
            if(isset($_GET['pacientes']) && isset($_GET['inicio']) && isset($_GET['fim']) ){
                $listaPrescricao = Presc::listarPorFiltro();
            } else {
                $msg = "Favor preencher todos os campos!";
            }

            require '../enfermagem.php';
            $enfermagem = new Enfermagem();

            require 'templates/prescricoes-curativos.phtml';

            break;
    }
}
