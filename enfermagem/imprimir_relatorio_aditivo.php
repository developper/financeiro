<?php
$id = session_id();
if (empty($id))
	session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');
require __DIR__ . '/../vendor/autoload.php';

use \App\Controllers\Administracao as Administracao;
use \App\Models\Enfermagem\ImagemFeridaEvolucao;
use \App\Models\DB\ConnMysqli;
use \App\Models\Administracao\Filial;

require 'relatorios.php';
class ImprimirRelatorio {

	public function cabecalho($id, $empresa = null, $convenio = null) {

		$condp1 = "c.idClientes = '{$id}'";
		$sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresa,
		c.`nascimento`,
		p.nome as Convenio,p.id,
		NUM_MATRICULA_CONVENIO,
		cpf
		FROM
		clientes AS c LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN
		planosdesaude as p ON (p.id = c.convenio)
		WHERE
		{$condp1}
		ORDER BY
		c.nome DESC LIMIT 1";

		$result = mysql_query($sql);
		$result2 = mysql_query($sql2);
		$html .= "<table width=100% >";
		while ($pessoa = mysql_fetch_array($result2)) {
			foreach ($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);
			}
            $pessoa['empresa'] = !empty($empresa) ? $empresa : $pessoa['empresa'];
            $pessoa['Convenio'] = !empty($convenio) ? $convenio : $pessoa['Convenio'];
			$html .= "<br/><tr>";
			$html .= "<td colspan='2'><label style='font-size:14px;color:#8B0000'><b></b></label></td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>PACIENTE </b></label></td>";
			$html .= "<td><label><b>SEXO </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>{$pessoa['paciente']}</td>";
			$html .= "<td>{$pessoa['sexo']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>IDADE </b></label></td>";
			$html .= "<td><label><b>UNIDADE REGIONAL </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>" . join("/", array_reverse(explode("-", $pessoa['nascimento']))) . ' (' . $pessoa['idade'] . " anos)</td>";
			$html .= "<td>{$pessoa['empresa']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>";
			$html .= "<td><label><b>MATR&Iacute;CULA </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>" . strtoupper($pessoa['Convenio']) . "</td>";
			$html .= "<td>" . strtoupper($pessoa['NUM_MATRICULA_CONVENIO']) . "</td>";
			$html .= "</tr>";
			$html.= " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
		}
		$html .= "</table>";

		return $html;
	}

	public function imprimir_relatorio_aditivo($id, $idr, $empresa) {

		$html .= "<form method='post' target='_blank'>";

        $responseEmpresa = Filial::getEmpresaById($empresa);

		$html .= "<h1><a><img src='../utils/logos/{$responseEmpresa['logo']}' width='100' ></a></h1>";

		$html.= "<h2 style='font-size:14px;text-align:center;'>RELAT&Oacute;RIO ADITIVO DE ENFERMAGEM</h2>";

        $sql = "SELECT 
                  empresas.nome AS empresa, 
                  planosdesaude.nome AS plano 
                FROM 
                  relatorio_aditivo_enf
                  LEFT JOIN empresas ON relatorio_aditivo_enf.empresa = empresas.id
                  LEFT JOIN planosdesaude ON relatorio_aditivo_enf.plano = planosdesaude.id
                WHERE 
                  relatorio_aditivo_enf.ID = '{$idr}'";
        $rs = mysql_query($sql);
        $rowRel = mysql_fetch_array($rs);

        $html .= $this->cabecalho($id, $responseEmpresa['nome'], $rowRel['plano']);

		$sql = "
		SELECT
		r.*,
		DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
		DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
		DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
		u.nome as user,
		u.conselho_regional,
		u.tipo as tipo_usuario
		FROM
		relatorio_aditivo_enf as r LEFT JOIN
		usuarios as u ON (r.USUARIO_ID = u.idUsuarios)
		WHERE
		r.ID = '{$idr}'
		ORDER BY
		r.DATA DESC";

		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {
			$compTipo = $row['conselho_regional'];

			$profissional = $row['USUARIO_ID'];
			$inicio = $row['inicio'];
			$fim = $row['fim'];
			$user = $row['user'] . $compTipo;
			$solicitacao_aditivo = $row['SOLICITACAO_ADITIVO'];
			$feridas = $row['FERIDAS'];
		}

		$sql_problemas = "
			SELECT
				p.*
			FROM
			problemas_relatorio_prorrogacao_enf as p
			WHERE
			p.RELATORIO_ADITIVO_ENF_ID = {$idr}
			ORDER BY
			ID
			";

		$html .= "<p style='text-align:center'><b>RELAT&Oacute;RIO ADITIVO DE ENFERMAGEM DO PER&Iacute;ODO {$inicio} AT&Eacute; {$fim}</b></p>";
		$html .= "<p style='background-color:#EEEEEE;'><b>PROFISSIONAL RESPONS&Aacute;VEL: " . mb_strtoupper($user) . "</b></p>";

		$html .= "<table class='mytable' style='width:100%;'>";
		$html .= "<tr>";
		$html .= "<td><b>Problemas Ativos</td><td colspan='2'><b>Observa&ccedil;&atilde;o</b></td>";
		$html .= "</tr>";
		$result_problemas = mysql_query($sql_problemas);
		while ($row = mysql_fetch_array($result_problemas)) {
			$cid_id = $row['CID_ID'];
			$descricao = $row['DESCRICAO'];
			$observacao = $row['OBSERVACAO'];

			$html .= "<tr><td width='30%'>{$descricao}</td><td colspan='2'>{$observacao}</td></tr>";
		}

		$html .= "</table>";

		//FERIDAS

    $imagemFeridaEvolucao = new ImagemFeridaEvolucao(ConnMysqli::getConnection());

		$sql_feridas = "
                    SELECT
                        *
                    FROM
                        feridas_relatorio_prorrogacao_enf as f
                    WHERE
                        f.RELATORIO_ADITIVO_ENF_ID = {$idr} AND EDITADA_EM is NULL AND DATA_DESATIVADA is NULL";

        $result_feridas = mysql_query($sql_feridas);
        $numero_ferida = 0;
        while ($row = mysql_fetch_array($result_feridas)) {
            ++$numero_ferida;
            if ($numero_ferida > 1) {
                $feridas_tabela .= "<hr />";
            }

            $feridaImagens = $imagemFeridaEvolucao->getFeridaImagensRelatorioHTML($row['ID']);


            $local = $row['LOCAL'];
            $caracteristica = $row['CARACTERISTICA'];
            $tipo_curativo = $row['TIPO_CURATIVO'];
            $prescricao_prox_periodo = $row['PRESCRICAO_PROX_PERIODO'];
            $feridas_tabela .= "<table class='mytable' style='width:100%'>
                                    <tr>
                                        <td><b>Ferida {$numero_ferida}</b></td>
                                    </tr>
                                    <tr>
                                        <td><b>Local da ferida: &nbsp;</b>{$local}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Caracter&iacute;stica da ferida &nbsp;</b>{$caracteristica}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Tipo do Curativo em uso: &nbsp;</b>{$tipo_curativo}</tr>
                                    <tr>
                                        <td><b>Prescri&ccedil;&atilde;o de cobertura para pr&oacute;ximo per&iacute;odo: </b>{$prescricao_prox_periodo}
                                    </tr>
                                    <tr>
                                      <td><b>Imagem da Ferida:</b></td>
                                    </tr>
                                    <tr>
                                      <td>{$feridaImagens}</td>
                                    </tr>
                                 </table>";
        }

        if ($feridas == '1') {
            $feridas_sn = "SIM";            
        }
        if ($feridas == '2') {
            $feridas_sn = "N&Atilde;O";
            $feridas_tabela = '';
        }
        $html .= "<table class='mytable' style='width:100%'>";
        $html .= "<tr style='background-color:#EEEEEE;'>";
        $html .= "<td colspan='5' ><b>FERIDAS:</b> {$feridas_sn}</td></tr>";
        $html .= "</table>";
        $html .= "{$feridas_tabela}";
        $html .= "<p></p>";

        $html .= "<table class='mytable' style='width:100%;'>";
        $html .= "<tr style='background-color:#EEEEEE;'>";
        $html .= "<td colspan='5' ><b>SOLICITA&Ccedil;&Atilde;O DE ADITIVO</b></td>";
        $html .= "</tr>";
        $html .= "<tr><td>{$solicitacao_aditivo}</td></tr>";
        $html .= "<tr><td colspan='5'>";
        $html .= "</table>";
        
        $rel = new Relatorios;
        
         $html .= "<table class='mytable' style='width:100%;'>";
        $html .= "<tr style='background-color:#EEEEEE;'>";
        $html .= "<td><b>ITENS SOLICITADOS</b></td>";
        $html .= "</tr>";
        $html .= $rel->itensSolicitacaoAditivo($idr,3,$imprimir = 1);
        $html .= "</table>";
        
        $html .= assinaturaProResponsavel($profissional);

        

        $paginas [] = $header . $html.= "</form>";

        $mpdf = new mPDF('pt', 'A4', 8);
        $mpdf->SetHeader('página {PAGENO} de {nbpg}');
        $ano = date("Y");
        $mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']}"." &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));
        $mpdf->WriteHTML("<html><body>");
        $flag = false;
        foreach ($paginas as $pag) {
            if ($flag)
                $mpdf->WriteHTML("<formfeed>");
            $mpdf->WriteHTML($pag);
            $flag = true;
        }
        $mpdf->WriteHTML("</body></html>");
        $mpdf->Output('relatorio_aditivo.pdf', 'I');
        exit;
    }

//alta
}

//imprimir_alta

$p = new ImprimirRelatorio();
$p->imprimir_relatorio_aditivo($_GET['id'], $_GET['idr'], $_GET['empresa']);
?>
