var fileInput = null,
    fileIndex = null,
    selectorDivFerida = null,
    feridaThumbClass = 'image-ferida-thumb',
    spanDataImage = 'span-data-image'
    ;

function excluirImagem(btn) {
  var td = $(btn).closest('td');
  td.find('.' + feridaThumbClass).remove();
  td.find('.' + spanDataImage).remove();
  td.find('.images-ferida').removeAttr('data-images');
  td.find('.images-ferida').attr('data-date','');
  td.find('.progressBar').html('');
  td.find('strong').html('');
  $(fileInput).val('');
}

function _(selector) {
  return document.querySelectorAll(selector)[fileIndex];
}

function uploadFile(input, action, selector) {

  if (input.files.length > 0) {
    var formData = formDataFactory(input);
    var ajax = new XMLHttpRequest();

    fileInput = input;
    selectorDivFerida = selector;
    fileIndex = $(fileInput).closest(selectorDivFerida).index();

    var td = $(fileInput).closest('td');
    $(td.find('.progressBar'))
      .html("Enviando... Por favor, aguarde.")
      .show();

    ajax.addEventListener("progress", progressHandler, false);
    ajax.addEventListener("load", completeHandler, false);
    ajax.addEventListener("error", errorHandler, false);
    ajax.addEventListener("abort", abortHandler, false);
    ajax.open("POST", action, true);
    ajax.send(formData);
  }

}

function progressHandler(event) {
  var td = $(fileInput).closest('td');
  var progressBar = td.find('.progressBar');
  var percent = Math.round((event.loaded / event.total) * 100);
}

function completeHandler(event) {
  var td = $(fileInput).closest('td');
  var progressBar = td.find('.progressBar');
  var status = td.find('.status_image');
  var resp = JSON.parse(this.responseText);

  $(status).html('');
  if (event.target.status != 200) {
    var msg = 'Error: Falha ao enviar o arquivo <br/>' + resp.messages.join('<br/>');
    $(status).html(msg);
  }

  $(progressBar).html('Concluído!');

  createDataSet(resp);
  createImgs(resp);
}

function errorHandler(event) {
  var td = $(fileInput).closest('td');
  var status = td.find('.status_image');
  $(status).html("Falha ao enviar o arquivo");
}

function abortHandler(event) {
  console.log(event);
  var td = $(fileInput).closest('td');
  var status = td.find('.status_image');
  $(status).html("A tentativa de envio foi abortada");
}

function createDataSet(images) {
  var ids = [];
  for(var i = 0, image; image = images[i]; ++i) {
    ids.push(image.id);
  }
  fileInput.dataset.images = ids;
}

function createImgs(images) {
  var td = $(fileInput).closest('td');
  var form = $(fileInput).closest('form');

  td.find('.'+feridaThumbClass).remove(); // clear images
  td.find('.'+spanDataImage).remove(); // remove input data

  for(var i = 0, image; image = images[i]; ++i) {
    var img = document.createElement('img');
    var reset = document.createElement('a');

    img.className = feridaThumbClass;
    img.src = '/' + image.path;

    reset.textContent = 'Excluir';
    reset.className = feridaThumbClass + ' excluir-imagem';

    reset
      .addEventListener('click', function() {
        excluirImagem(this);
      });

    td.append($(img));
    if($('#fromRelatorio').val() == 'prorrogacao_enfermagem' || $('#fromRelatorio').val() == 'aditivo_enfermagem' ){
      td.append("<br><span class='span-data-image'><label style=''>Em </label > <input style='display: inline;text-align: left'type='text'  size='10px' class='data data-ferida-prorrogacao ' value= ''></span>");
    }

    td.append($(reset));

  }
  $(".data").datepicker({
    inline: true
  });
}

function formDataFactory(input) {
  var formData = new FormData();
  var files = input.files;
  for (var i = 0, file; file = files[i]; ++i) {
    formData.append(input.name, file);
  }
  return formData;
}