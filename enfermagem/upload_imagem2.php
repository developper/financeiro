<?php
require __DIR__ . '/../vendor/autoload.php';

use App\Models\DB\ConnMysqli;
use App\Models\FileSystem\FileUpload;

//get the params
session_start();
$userId = $_SESSION['id_user'];
$baseDirectory = 'storage/feridas_enfermagem/';
$files = $_FILES['feridas'];

//save the files in the disk
$fileUpload = new FileUpload($baseDirectory);
$fileUpload->turnOnPrefix();
list($handlerResults, $messages) = $fileUpload->process($files);

if (empty($messages)) {
  $sqls = $fileUpload->getSql($handlerResults, $userId);

//save only the paths in the database
  $data = array();
  $con = ConnMysqli::getConnection();
  foreach ($sqls as $sql) {
    if ($con->query($sql)) {
      $result = $con->query($fileUpload->getSqlById($con->insert_id));
      while ($file = $result->fetch_object()) {
        $data[] = ['id' => $file->id, 'path' => $file->path];
      }
    }
  }
} else {
  foreach ($messages as $errors)
    foreach ($errors as $error)
      $data['messages'][] = sprintf('* %s', (string) $error);
  http_response_code(500);
}

header('Content-Type: application/json');
echo json_encode($data);


