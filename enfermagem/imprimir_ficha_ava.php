<?php
$id = session_id();
if(empty($id))
	session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../vendor/autoload.php');


use \App\Controllers\Administracao as Administracao;
use \App\Models\Administracao\Filial;

class Paciente_imprimir_ficha{


	public function cabecalho($id,$id_a, $nomeEmpresa){
		$condp1= "c.idClientes = '{$id}'";
		$sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		c.END_INTERNADO AS local_i,
		(CASE c.sexo
			WHEN '0' THEN 'Masculino'
			WHEN '1' THEN 'Feminino'
			END) AS nomesexo,
FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
e.nome as empresan,
c.`nascimento`,
p.nome as Convenio,p.id,c.*,(CASE c.acompSolicitante
	WHEN 's' THEN 'Sim'
	WHEN 'n' THEN 'N&atilde;o'
	END) AS acomp,
NUM_MATRICULA_CONVENIO,
cpf
FROM
clientes AS c LEFT JOIN
empresas AS e ON (e.id = c.empresa) INNER JOIN
planosdesaude as p ON (p.id = c.convenio)
WHERE
{$condp1}
ORDER BY
c.nome DESC LIMIT 1";

		$result = mysql_query($sql);
		$result2 = mysql_query($sql2);

		//$sql3 = "Select * from solicitacoes ";

		//$result_enfermeira = mysql_query($sql3);

		//Eriton

		$sql4= "SELECT
u.nome AS nome,
u.conselho_regional,
u.tipo as tipo_usuario,
empresas.nome AS empresa_rel,
planosdesaude.nome AS convenio_rel
FROM
avaliacaoenfermagem AS aval INNER JOIN
usuarios AS u ON (aval.USUARIO_ID = u.idUsuarios)
LEFT JOIN empresas ON aval.empresa = empresas.id
LEFT JOIN planosdesaude ON aval.convenio = planosdesaude.id
WHERE
PACIENTE_ID = {$id} and aval.ID = {$id_a}";
		$result4 = mysql_query($sql4);
		$conselho = mysql_result($result4,0,1);
		$tipo = mysql_result($result4,0,2);
		$empresa = $nomeEmpresa;
		$convenio = mysql_result($result4,0,4);
		$compTipo = " $conselho";

		$nome_enf = mysql_result($result4,0,0) . $compTipo;
		//Fim	

		//Eriton Busca Local Internaï¿½ï¿½o

		$sql5 = "SELECT
c.localinternacao AS local_i
FROM
clientes AS c
WHERE
idClientes = {$id}";
		$result5 = mysql_query($sql5);
		$local_i = mysql_result(result5,0,0);

		//Fim Eriton Busca Local Internaï¿½ï¿½o
		$html.= "<table style='width:100%;' >";
		while($pessoa = mysql_fetch_array($result2)){
			foreach($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);
			}

            $pessoa['empresan'] = !empty($empresa) ? $empresa : $pessoa['empresan'];
            $pessoa['Convenio'] = !empty($convenio) ? $convenio : $pessoa['Convenio'];

			$html.= "<tr bgcolor='#EEEEEE'>";
			$html.= "<td  style='width:100%;'><b>PACIENTE:</b></td>";
			$html.= "<td ><b>SEXO </b></td>";
			$html.= "<td style='width:100%;'><b>IDADE:</b></td>";
			$html.= "</tr>";
			$html.= "<tr>";
			$html.= "<td style='width:100%;'>{$pessoa['paciente']}</td>";
			$html.= "<td>{$pessoa['nomesexo']}</td>";
			$html.= "<td>".join("/",array_reverse(explode("-",$pessoa['nascimento']))).' ('.$pessoa['idade']." anos)</td>";
			$html.= "</tr>";

			$html.= "<tr bgcolor='#EEEEEE'>";

			$html.= "<td ><b>UNIDADE REGIONAL:</b></td>";
			$html.= "<td style='width:100%;'><b>CONV&Ecirc;NIO </b></td>";
			$html.= "<td><b>MATR&Iacute;CULA </b></td>";
			$html.= "</tr>";
			$html.= "<tr>";

			$html.= "<td>{$pessoa['empresan']}</td>";
			$html.= "<td>".strtoupper($pessoa['Convenio'])."</td>";
			$html.= "<td>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
			$html.= "</tr>";

			$html.= "<tr bgcolor='#EEEEEE'>";
			$html.= "<td><b>M&Eacute;DICO ASSISTENTE:</b></td>";
			$html.= "<td><b>ESPECIALIDADE:</b></td>";
			$html.= "<td><b>ACOMPANHARA O PACIENTE (Sim ou N&atilde;o)</b></td>";
			$html.= "</tr>";
			$html.= "<tr>";
			$html.= "<td>{$pessoa['medicoSolicitante']}</td>";
			$html.= "<td>{$pessoa['espSolicitante']}</td>";
			$html.= "<td>{$pessoa['acomp']}</td>";
			$html.= "</tr>";
			$html.= "<tr bgcolor='#EEEEEE'>";
			$html.= "<td><b>UNIDADE DE INTERNA&Ccedil;&Atilde;O/HOSPITAL</b></td>";
			$html.= "<td><b>TELEFONE:</b></td>";
			$html.= "<td><b>TELEFONE RESPONS&Aacute;VEL:</b></td>";
			$html.= "</tr>";
			$html.= "<tr>";
			$html.= "<td>{$pessoa['localInternacao']}</td>";
			$html.= "<td>{$pessoa['TEL_DOMICILIAR']}</td>";
			$html.= "<td>{$pessoa['TEL_RESPONSAVEL']}</td>";
			$html.= "</tr>";
			$html.= "<tr bgcolor='#EEEEEE'>";
			$html.= "<td><b>AVALIADORA</b></td>";
			$html.= "<td><b>LOCAL DA AVALIA&Ccedil;&Atilde;O:</b></td>";
			$html.= "<td><b></b></td>";
			$html.= "</tr>";
			$html.= "<tr>";
			$html.= "<td>{$nome_enf}</td>";
			$html.= "<td>{$pessoa['local_i']}</td>";
			$html.= "<td></td>";
			$html.= "</tr>";
			$html.= " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
		}
		$html.= "</table>";

		return $html;
	}

	public function detalhes_capitacao_medica($id,$id_a,$empresa){
		$sql2= "SELECT * from avaliacaoenfermagem where paciente_id = {$id} and id = {$id_a}";
		$result = mysql_query($sql2);

		while($row = mysql_fetch_array($result)){
			$enfermeiro_id = $row['USUARIO_ID'];
			$paciente = $row['PACIENTE_ID'];
			$id_aval= $row['ID'];
			$clinico = $row['CLINICO'];
			$cirurgico = $row['CIRURGICO'];
			$paliativos = $row['PALIATIVOS'];
			$diagnostico_medico_principal = $row['DIAGNOSTICO_PRINCIPAL'];
			$hist_pregressa = $row['HIST_PREGRESSA'];
			$hist_familiar = $row['HIST_FAMILIAR'];
			$queixas_atuais = $row['QUEIXA_ATUAL'];
			$hist_atual = $row['HIST_ATUAL'];
			$bom = $row['EST_GERAL_BOM'];
			$ruim  = $row['EST_GERAL_RUIM'];
			$nutrido = $row['NUTRIDO'];
			$desnutrido = $row['DESNUTRIDO'];
			$higiene_corpo_s  = $row['HIGIENE_CORPORAL_SATISFATORIA'];
			$higiene_corpo_i  = $row['HIGIENE_CORPORAL_INSATISFATORIA'];
			$higiene_bucal_s  = $row['HIGIENE_BUCAL_SATISFATORIA'];
			$higiene_bucal_i  = $row['HIGIENE_BUCAL_INSATISFATORIA'];
			$estado_emocional  = $row['ESTADO_EMOCIONAL'];
			$consciente = $row['CONCIENTE'];
			$sedado = $row['SEDADO'];
			$obnublado  = $row['OBNUBILADO'];
			$torporoso = $row['TORPOROSO'];
			$comatoso = $row['COMATOSO'];
			$orientado = $row['ORIENTADO'];
			$desorientado = $row['DESORIENTADO'];
			$sonolento = $row['SONOLENTO'];
			$cond_motora = $row['COND_MOTORA'];
			$palergiamed  = $row['ALERGIA_MEDICAMENTO'];
			$palergiaalim = $row['ALERGIA_ALIMENTO'];
			$alergiamed =  $row['ALERGIA_MEDICAMENTO_TIPO'];
			$alergiaalim = $row['ALERGIA_ALIMENTO_TIPO'];
			$integra = $row['PELE_INTEGRA'];
			$lesoes  = $row['PELE_LESAO'];
			$hidratada = $row['PELE_HIDRATADA'];
			$desidratada  = $row['PELE_DESIDRATADA'];
			$icterica = $row['PELE_ICTERICA'];
			$anicterica  = $row['PELE_ANICTERICA'];
			$icterica_escala = $row['PELE_ICTERICA_ESCALA'];
			$palida = $row['PELE_PALIDA'];
			$sudoreica  = $row['PELE_SUDOREICA'];
			$fria = $row['PELE_FRIA'];
			$pele_edema = $row['PELE_EDEMA'];
			$descamacao = $row['PELE_DESCAMACAO'];
			$t_assimetrico = $row['T_ASSIMETRICO'];
			$t_simetrico = $row['T_SIMETRICO'];
			$eupneico= $row['EUPNEICO'];
			$dispneico= $row['DISPNEICO'];
			$taquipneico= $row['TAQUIPNEICO'];
			$mvbd = $row['MVBD'];
			$roncos=$row['RONCOS'];
			$sibilos=$row['SIBILOS'];
			$creptos=$row['CREPTOS'];
			$mv_diminuido=$row['MV_DIMINUIDO'];
			$bcnf= $row['BCNF'];
			$normocardico=$row['NORMOCARDICO'];
			$taquicardico=$row['TAQUICARDICO'];
			$bradicardico=$row['BRADICARDICO'];
			$ventilacao  = $row['VENTILACAO'];
			$vent_metalica  = $row['VENT_TRAQUESTOMIA_METALICA_NUM'];
			$ventilacao_plastica_num  = $row['VENT_TRAQUESTOMIA_PLASTICA_NUM'];
			$bipap = $row['BIPAP'];
			$oxigenio_sn  = $row['OXIGENIO_SN'];
			$oxigenio  = $row['OXIGENIO'];
			$vent_cateter_oxigenio_num = $row['VENT_CATETER_OXIGENIO_NUM'];
			$vent_cateter_oxigenio_quant = $row['VENT_CATETER_OXIGENIO_QUANT'];
			$vent_cateter_oculos_num =  $row['VENT_CATETER_OCULOS_NUM'];
			$vent_respirador = $row['VENT_RESPIRADOR'];
			$vent_respirador_tipo = $row['VENT_RESPIRADOR_TIPO'];
			$vent_mascara_ventury = $row['VENT_MASCARA_VENTURY'];
			$vent_mascara_ventury_tipo  = $row['VENT_MASCARA_VENTURY_NUM'];
			$vent_concentrador   = $row['VENT_CONCENTRADOR'];
			$vent_concentrador_num = $row['VENT_CONCENTRADOR_NUM'];
			$aspiracoes_sn = $row['ALT_ASPIRACOES'];
			$aspiracoes_num   = $row['ASPIRACAO_NUM'];
			$aspiracao_secrecao = $row['ASPIRACAO_SECRECAO'];
			$caracteristicas_sec   = $row['ASPIRACAO_SECRECAO_CARACTERISTICA'];
			$cor_sec = $row['ASPIRACAO_SECRECAO_COR'];
			$plano = $row['PLANO'];
			$flacido = $row['FLACIDO'];
			$distendido = $row['DISTENDIDO'];
			$timpanico = $row['TIMPANICO'];
			$globoso = $row['GLOBOSO'];
			$rha = $row['RHA'];
			$indo_palpacao=$row['INDO_PALPACAO'];
			$doloroso = $row['DOLOROSO'];
			$oral   = $row['VIA_ALIMENTACAO_ORAL'];
			$sne = $row['VIA_ALIMENTACAO_SNE'];
			$sne_num = $row['VIA_ALIMENTACAO_SNE_NUM'];
			$gtm_sonda_gastro = $row['VIA_ALIMENTACAO_GTM'];
			$gtm_num = $row['VIA_ALIMENTACAO_GTM_NUM'];
			$gtm_visivel = $row['VIA_ALIMENTACAO_GTM_VISIVEL'];
			$gtm_profundidade = $row['VIA_ALIMENTACAO_GMT_PROFUNDIDADE'];
			$sonda_gastro   = $row['VIA_ALIMENTACAO_SONDA_GASTRO'];
			$quant_sonda   = $row['VIA_ALIMENTACAO_SONDA_GASTRO_NUM'];
			$parental   = $row['VIA_ALIMENTACAO_PARENTAL'];
			$dieta_sn = $row['DIETA_SN'];
			$sistema_aberto = $row['DIETA_SIST_ABERTO'];
			$quant_sistema_aberto   = $row['DIETA_SIST_ABERTO_DESCRICAO'];
			$uso  = $row['DIETA_SIST_ABERTO_USO'];
			$sistema_fechado  = $row['DIETA_SIST_FECHADO'];
			$nome_sistema_aberto = $row['DIETA_SIST_ABERTO_DESCRICAO'];
			$uso_sistema_aberto = $row['USO_SISTEMA_ABERTO'];
			$quant_sistema_fechado   = $row['DIETA_SIST_FECHADO_DESCRICAO'];
			$vazao   = $row['DIETA_SIST_FECHADO_VAZAO'];
			$dieta_artesanal   = $row['DIETA_SIST_ARTESANAL'];
			$suplemento_sn   = $row['DIETA_SUPLEMENTO_SN'];
			$suplemento   = $row['DIETA_SUPLEMENTO'];
			$suplemento_desc  = $row['DIETA_SUPLEMENTO_DESC'];
			$dieta_npt  = $row['DIETA_NPT'];
			$quant_npt  = $row['DIETA_NPT_DESC'];
			$vazao_npt   = $row['DIETA_NPT_VAZAO'];
			$npp   = $row['DIETA_NPP'];
			$quant_npp   = $row['DIETA_NPP_DESC'];
			$vazao_npp  = $row['DIETA_NPP_VAZAO'];
			$integra_genital = $row['INTEGRA'];
			$secrecao = $row['SECRECAO'];
			$sangramento = $row['SANGRAMENTO'];
			$prurido = $row['PRURIDO'];
			$edema = $row['EDEMA'];
			$topicos_sn = $row['TOPICOS_SN'];
			$cvc   = $row['DISPO_INTRAVENOSO_CVC'];
			$local_cvc  = $row['DISPO_INTRAVENOSO_CVC_LOCAL'];
			$local_uno = $row['DISPO_INTRAVENOSO_UNO_LOCAL'];
			$local_duplo= $row['DISPO_INTRAVENOSO_DUPLO_LOCAL'];
			$local_triplo = $row['DISPO_INTRAVENOSO_TRIPLO_LOCAL'];
			$avp   = $row['DISPO_INTRAVENOSO_AVP'];
			$avp_local  = $row['DISPO_INTRAVENOSO_AVP_LOCAL'];
			$port   = $row['DISPO_INTRAVENOSO_PORTOCATH'];
			$quant_port  = $row['DISPO_INTRAVENOSO_PORTOCATH_NUM'];
			$port_local  = $row['DISPO_INTRAVENOSO_PORTOCATH_LOCAL'];
			$outros_intra  = $row['DISPO_INTRAVENOSO_OUTROS'];
			$eliminacoes = $row['ELIMINACOES'];
			$fralda  = $row['FRALDA'];
			$fralda_dia  = $row['FRALDA_UNID_DIA'];
			$tamanho_fralda  = $row['FRALDA_TAMANHO'];
			$urinario  = $row['DISPO_URINARIO'];
			$quant_urinario  = $row['DISPO_URINARIO_NUM'];
			$sonda  = $row['DISPO_SONDA_FOLEY'];
			$quant_foley  = $row['DISPO_SONDA_FOLEY_NUM'];
			$sonda_alivio  = $row['DISPO_SONDA_ALIVIO'];
			$quant_alivio  = $row['DISPO_SONDA_ALIVIO_NUM'];
			$colostomia  = $row['DISPO_COLOSTOMIA'];
			$quant_colostomia  = $row['DISPO_COLOSTOMIA_NUM'];
			$iliostomia = $row['DISPO_ILIOSTOMIA'];
			$quant_iliostomia = $row['DISPO_ILIOSTOMIA_NUM'];
			$cistostomia  = $row['DISPO_CISTOSTOMIA'];
			$quant_cistostomia  = $row['DISPO_CISTOSTOMIA_NUM'];
			$dreno_sn  = $row['DRENOS'];
			$pa_sistolica  = $row['PA'];
			$pa_diastolica  = $row['PA_DISTOLICA'];
			$fc  = $row['FC'];
			$fr  = $row['FR'];
			$spo2  = $row['SPO2'];
			$temperatura  = $row['TEMPERATURA'];
			$pad  = $row['PAD_ENFERMAGEM'];
			$dor_sn  = $row['DOR'];
			$feridas_sn   = $row['FERIDA'];
			$dispositivos_intravenosos = $row['DISPOSITIVOS_INTRAVENOSOS'];
			$picc= $row['DISPO_INTRAVENOSO_CATETER_PICC'];
			$picc_local= $row['DISPO_INTRAVENOSO_CATETER_PICC_LOCAL'];
			$vetilacaoEspotaneaEquipamento = $row['VENTILACAO_ESP_EQUIP'];
			$arrayMultidisciplinar =[
				'medico'=>$row['medico_multidisciplinar'],
				'enfermeiro'=>$row['enfermagem_multidisciplinar'],
				'fono'=>$row['fono_multidisciplinar'],
				'nutricao'=>$row['nutricao_multidisciplinar'],
				'fissio_motora'=>$row['fissio_motora_multidisciplinar'],
				'fissio_respiratoria'=>$row['fissio_respiratoria_multidisciplinar'],
			];
		}

		$sql5 = "SELECT
	aval_enf.*, f.frequencia as qtd_frequencia
	FROM avaliacao_enf_medicamento AS aval_enf INNER JOIN
	frequencia AS f ON (aval_enf.FREQUENCIA = f.id)
	WHERE
	avaliacao_enf_id = {$id_a}";
		$result5 = mysql_query($sql5);

		$sql4 = "select * from avaliacao_enf_suplemento where avaliacao_enf_id = {$id_a}";
		$result4 = mysql_query($sql4);

		$sql6 = "select * from avaliacao_enf_topicos where avaliacao_enf_id = {$id_a}";
		$result6 = mysql_query($sql6);

		$sql7 = "select * from avaliacao_enf_dreno where avaliacao_enf_id = {$id_a}";
		$result7 = mysql_query($sql7);

		$sql7 = "select * from avaliacao_enf_dor where avaliacao_enf_id = {$id_a}";
		$result2 = mysql_query($sql7);

		$html.= "<form method='post' target='_blank'>";

        $responseEmpresa = Filial::getEmpresaById($empresa);

		$html.= "<a><img src='../utils/logos/{$responseEmpresa['logo']}' width='100' ></a>";

		$html.= "<h1 style='font-size:10px;text-align:center;'>Ficha de " . htmlentities("Avaliação") . " Mederi</h1>";

		$html.=$this->cabecalho($id,$id_a, $responseEmpresa['nome']);
		$html.= "<table class='mytable' style='width:95%;' >";

		$html.= "<tr><td colspan='5'><br></br></td></tr>";
		$html.= "<tr><td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>Anamnese</b></h1></td></tr>";
		if($clinico == 1){
			$x= 'x';
		}else{
			$x= ' ';
		}

		if($cirurgico == 1){
			$c= 'x';
		}else{
			$c= ' ';
		}

		if($paliativos == 1){
			$p= 'x';
		}else{
			$p= ' ';
		}

		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$x})Cl&iacute;nico &nbsp;&nbsp;&nbsp;({$c})Cirurgico  &nbsp;&nbsp;&nbsp;({$p})Cuidados Paliativos</h1></td></tr>";

		//diagnostico medico principal
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>" . htmlentities("Diagnóstico Médico") . " Principal que o levou " . htmlentities("à Hospitalização"). "</b></td>";
		$html.="</tr>";

		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>{$diagnostico_medico_principal}</h1></td></tr>";

		//hist pregressa
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>" . htmlentities("História") . " Pregressa</b></td>";
		$html.="</tr>";

		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>{$hist_pregressa}</h1></td></tr>";

		//Hist Familiar
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>" . htmlentities("História") . " Familiar</b></td>";
		$html.="</tr>";

		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>{$hist_familiar}</h1></td></tr>";



		//Queixas Atuais Atualizaï¿½ï¿½o Eriton 19-03-2013
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>" . htmlentities("Observações") . "</b></td>";
		$html.="</tr>";

		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>{$queixas_atuais}</h1></td></tr>";

		//Medicacao em Uso

		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>" . htmlentities("Medicação") . " em Uso</b></td>";
		$html.="</tr>";

		//Atualizaï¿½ï¿½o Eriton 22-03-2013
		while($row5 = mysql_fetch_array($result5)){
			$medicacao = $row5['MEDICACAO'];
			$apresentacao = $row5['APRESENTACAO'];
			$via = $row5['VIA'];
			$posologia = $row5['POSOLOGIA'];
			$frequencia_med = $row5['qtd_frequencia'];
			$custeado_familia = $row5['CUSTEADO_FAMILIA'];
			if($custeado_familia == 1){
				$f = 'Fam&iacute;lia custeia (x)';
			}else{
				$f= ' ';
			}
			//$html.= "<tr><td> <h1 class='relatorio-texto'>{$medicacao} Apresenta&ccedil;&atilde;o <input type='text' size=5 name='apresentacao' value='{$apresentacao}' /></td><td>Via<input type='text' name='via' value='{$via}' /></td><td> Posologia <input type='text' name='posologia' value='{$posologia}' /></td><td> Fam&iacute;lia custeia ({$f})Fam&iacute;lia Custeia </h1></td> </tr>";
			$html.= "<tr><td> <h1 class='relatorio-texto'><b>" . htmlentities("Medicação") . ":</b> {$medicacao} " . htmlentities("Apresentação") . ": {$apresentacao} Via: {$via} Posologia: {$posologia} Frequ&ecirc;ncia: {$frequencia_med} {$f}</h1></td> </tr>";
			//$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>{$medicacao} | {$apresentacao}</h1></td></tr>";
			//Fim Atualizaï¿½ï¿½o Eriton 22-03-2013
		}

		//Hist Atual
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>" . htmlentities("História") . " Atual</b></td>";
		$html.="</tr>";

		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>{$hist_atual}</h1></td></tr>";

		//Exame Fï¿½sico
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 class='relatorio-texto'><center><b>EXAME " . htmlentities("FÍSICO") . "</b></center></td>";
		$html.="</tr>";

		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>Estado Geral</b></td>";
		$html.="</tr>";

		if($bom == 1){
			$b='x';
		}else{
			$b=' ';
		}
		if($ruim == 1){
			$r='x';
		}else{
			$r=' ';
		}
		if($nutrido == 1){
			$n='x';
		}else{
			$n=' ';
		}
		if($desnutrido == 1){
			$d='x';
		}else{
			$d=' ';
		}
		if($higiene_corpo_s){
			$hc='x';
		}else{
			$hc='';
		}
		if($higiene_corpo_i){
			$hi='x';
		}else{
			$hi='';
		}
		if($higiene_bucal_s){
			$hbs='x';
		}else{
			$hbs='';
		}
		if($higiene_bucal_i){
			$hbi='x';
		}else{
			$hbi='';
		}
		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$b})Bom &nbsp;&nbsp;&nbsp;({$r})Ruim  &nbsp;&nbsp;&nbsp;({$n})Nutrido &nbsp;&nbsp;&nbsp;({$s})Desnutrido</h1></td></tr>";
		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$hc})Higiene Corporal " . htmlentities("Satisfatória") . " &nbsp;&nbsp;&nbsp;({$hi})Higiene Corporal " . htmlentities("Insatisfatória") . "  &nbsp;&nbsp;&nbsp;({$hbs})Higiene Bucal " . htmlentities("Satisfatória") . " &nbsp;&nbsp;&nbsp;({$hbi})Higiene Bucal " . htmlentities("Instatisfatória") . "</h1></td></tr>";

		//Estado Emocional
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>Estado Emocional</b></td>";
		$html.="</tr>";


		if($estado_emocional ==1){
			$calmo='x';
		}
		if($estado_emocional ==2){
			$agitado='x';
		}
		if($estado_emocional ==3){
			$deprimido='x';
		}
		if($estado_emocional ==4){
			$choroso='x';
		}

		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$calmo})Calmo &nbsp;&nbsp;&nbsp;({$agitado})Agitado  &nbsp;&nbsp;&nbsp;({$deprimido})Deprimido &nbsp;&nbsp;&nbsp;({$choroso})Choroso</h1></td></tr>";

		//Nï¿½vel de Consciï¿½ncia
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>" . htmlentities("Nível de Consciência") . "</b></td>";
		$html.="</tr>";

		if($consciente){
			$cons='x';
		}else{
			$cons='';
		}
		if($sedado){
			$sed='x';
		}else{
			$sed='';
		}
		if($obnublado){
			$ob='x';
		}else{
			$ob='';
		}
		if($torporoso){
			$torp='x';
		}else{
			$torp='';
		}
		if($comatoso){
			$coma='x';
		}else{
			$coma='';
		}
		if($orientado){
			$ori='x';
		}else{
			$ori='';
		}
		if($desorientado){
			$deso='x';
		}else{
			$deso='';
		}
		if($sonolento){
			$sono='x';
		}else{
			$sono='';
		}
		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$cons})Consciente &nbsp;&nbsp;&nbsp;({$sed})Sedado  &nbsp;&nbsp;&nbsp;({$ob})Obnubilado &nbsp;&nbsp;&nbsp;({$torp})Torporoso</h1></td></tr>";
		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$coma})Comatoso &nbsp;&nbsp;&nbsp;({$ori})Orientado  &nbsp;&nbsp;&nbsp;({$deso})Desorientado &nbsp;&nbsp;&nbsp;({$sono})Sonolento</h1></td></tr>";


		//Condiï¿½ï¿½o Motora
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>" . htmlentities("Condição") . " Motora</b></td>";
		$html.="</tr>";


		if($cond_motora == 1){
			$deambula='x';
		}
		if($cond_motora ==2){
			$deambula_a='x';
		}
		if($cond_motora ==3){
			$cadeirante='x';
		}
		if($cond_motora ==4){
			$acamado='x';
		}

		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$deambula})Deambula &nbsp;&nbsp;&nbsp;({$deambula_a})Deambula com " . htmlentities("auxílio") . "  &nbsp;&nbsp;&nbsp;({$cadeirante})Cadeirante &nbsp;&nbsp;&nbsp;({$acamado})Acamado</h1></td></tr>";

		//Alergia Medicamentosa
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		switch ($palergiamed){
			case "s":
				$palergiamed = "Sim";
				break;
			case "n":
				$palergiamed = "N&atilde;o";
				break;
			default:
				$palergiamed = "N&atilde;o";
				break;
		}
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>Alergia Medicamentosa - {$palergiamed}</b></td>";
		$html.="</tr>";
		if($palergiamed == 'Sim')
			$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>Medicamento: {$alergiamed}</h1></td></tr>";

		//Alergia Alimentar
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		switch ($palergiaalim){
			case "s":
				$palergiaalim = "Sim";
				break;
			case "n":
				$palergiaalim = "N&atilde;o";
				break;
			default:
				$palergiaalim = "N&atilde;o";
				break;
		}
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>Alergia Alimentar - {$palergiaalim}</b></td>";
		$html.="</tr>";
		if($palergiaalim =='Sim')
			$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>Alimento: {$alergiaalim}</h1></td></tr>";



		//Pele
		//atualizaï¿½ï¿½o Eriton 17-04-2013
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>Pele</b></td>";
		$html.="</tr>";

		if($integra == 1){
			$inte='x';
			$exibir = "style='display:none";
		}else{
			$inte='';
		}
		if($lesoes == 1){
			$leso ='x';
			$exibir = "style='display:none";
		}else{
			$leso ='';
		}
		if($desidratada == '1'){
			$desi='x';
			$exibir = "style='display:none";
		}else{
			$desi='';
		}
		if($hidratada == '1'){
			$hid='x';
			$exibir = "style='display:none";
		}else{
			$hid='';
		}
		if($icterica == '1'){
			$ict='x';
			$exibir = "style='display:block";
		}else{
			$ict='';
		}
		if($anicterica == '1'){
			$ani='x';
			$exibir = "style='display:none";
		}else{
			$ani='';
		}

		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$inte})&Iacute;ntegra  &nbsp;&nbsp;&nbsp;({$ict})" . htmlentities("Ictérica") . " &nbsp;";if($ict){ $html.=" Escala:{$icterica_escala} &nbsp;&nbsp;&nbsp;";}$html.="&nbsp;&nbsp;&nbsp;({$hid})Hidratada  </h1></td></tr>";
		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$leso})Lesionada  &nbsp;&nbsp;&nbsp;({$ani})" . htmlentities("Anictérica") . " &nbsp;&nbsp;&nbsp;({$desi})Desidratada  </h1></td></tr>";
		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>  </h1></td></tr>";

		if($palida == 1){
			$pali='x';
		}
		if($palida == 2){
			$pali2='x';
		}

		if($sudoreica == 1){
			$sudo ='x';
		}
		if($sudoreica == 2){
			$sudo2 ='x';
		}
		if($fria == 1){
			$fri ='x';
		}
		if($fria == 2){
			$fri2 ='x';
		}
		if($edema == 1){
			$ede ='x';
		}
		if($edema == 2){
			$ede2 ='x';
		}
		if($descamacao == 1){
			$desc ='x';
		}
		if($descamacao == 2){
			$desc2 ='x';
		}

		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>" . htmlentities("Pálida") . ": ({$pali})Sim &nbsp;&nbsp;&nbsp;({$pali2})" . htmlentities("Não") . "  </h1></td></tr>";
		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>Sudoreica: ({$sudo})Sim &nbsp;&nbsp;&nbsp;({$sudo2})" . htmlentities("Não") . "  </h1></td></tr>";
		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>Fria: ({$fri})Sim &nbsp;&nbsp;&nbsp;({$fri2})" . htmlentities("Não") . "  </h1></td></tr>";
		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>Edema: ({$ede})Sim &nbsp;&nbsp;&nbsp;({$ede2})" . htmlentities("Não") . "  </h1></td></tr>";
		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>" . htmlentities("Descamação") . ": ({$desc})Sim &nbsp;&nbsp;&nbsp;({$desc2})" . htmlentities("Não") . "  </h1></td></tr>";


		//Avaliaï¿½ï¿½o Cardiorespiratï¿½ria		
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>" . htmlentities("Avaliação cardiorepiratória") . "</b></td>";
		$html.="</tr>";

		if($t_assimetrico){
			$ass='x';
		}else{
			$ass='';
		}
		if($t_simetrico){
			$sime='x';
		}else{
			$sime='';
		}
		if($eupneico){
			$eup='x';
		}else{
			$eup='';
		}
		if($dispneico){
			$disp='x';
		}else{
			$disp='';
		}
		if($taquipneico){
			$taq='x';
		}else{
			$taq='';
		}
		if($mvdb){
			$mvdb='x';
		}else{
			$mvdb='';
		}
		if($roncos){
			$ronc='x';
		}else{
			$ronc='';
		}
		if($sibilos){
			$sib='x';
		}else{
			$sib='';
		}
		if($creptos){
			$cre='x';
		}else{
			$cre='';
		}
		if($mv_diminuido){
			$mv_d='x';
		}else{
			$mv_d='';
		}
		if($bcnf){
			$bcn='x';
		}else{
			$bcn='';
		}
		if($normocardico){
			$normo='x';
		}else{
			$normo='';
		}
		if($taquicardico){
			$taqui='x';
		}else{
			$taqui='';
		}
		if($bradicardico){
			$brad='x';
		}else{
			$brad='';
		}

		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$ass})" . htmlentities("Tórax Assimétrico") . " &nbsp;&nbsp;&nbsp;  ({$sime})" . htmlentities("Tórax Simétrico") . "  &nbsp;&nbsp;&nbsp;({$eup})Eupneico</h1></td></tr>";
		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$disp})Dispneico &nbsp;&nbsp;&nbsp;({$taq})Taquipneico  &nbsp;&nbsp;&nbsp;({$mvdb})MVDB </h1></td></tr>";
		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$ronc})Roncos &nbsp;&nbsp;&nbsp;({$sib})Sibilos  &nbsp;&nbsp;&nbsp;({$cre})Creptos </h1></td></tr>";
		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$mv_d})MV Diminu&iacute;do &nbsp;&nbsp;&nbsp;({$bcn})BCNF  &nbsp;&nbsp;&nbsp;({$normo})Normocardico </h1></td></tr>";
		$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$taqui})Taquicardico &nbsp;&nbsp;&nbsp;({$brad})Bradicardico  </h1></td></tr>";



		//Tipo de Ventilaï¿½ï¿½o
		//atualizaï¿½ï¿½o Eriton 17-04-2013
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>Tipo de " . htmlentities("Ventilação") . "</b></td>";
		$html.="</tr>";

		if($bipap == 1){
			$cont = 'x';
		}if($bipap == 2){
			$cont2 = 'x';
		}

		if($ventilacao == 1){

			$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>" . htmlentities("Expontânea") . "</h1></td></tr>";
		}elseif($ventilacao == 2){

			$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>Traqueostomia " . htmlentities("Metálica") . "  &nbsp;&nbsp;&nbsp; N&ordm; {$vent_metalica}</h1></td></tr>";
			if($vetilacaoEspotaneaEquipamento == 1 ){
				$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>Modo de " . htmlentities("Ventilação") . ": " . htmlentities("Espontânea") . "</h1></td></tr>";
			}else{
				$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>Modo de " . htmlentities("Ventilação") . ": Equipamento</h1></td></tr>";
				$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>Via Bipap:  ({$cont}) " . htmlentities("Contínuo") . " &nbsp;&nbsp;&nbsp; ({$cont2}) Intermitente</h1></td></tr>";
			}
		}elseif($ventilacao == 3){

			$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>Traqueostomia " . htmlentities("Plástica") . " &nbsp;&nbsp;&nbsp; N&ordm; {$ventilacao_plastica_num}</h1></td></tr>";
			if($vetilacaoEspotaneaEquipamento == 1 ){
				$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>Modo de " . htmlentities("Ventilação") . ": " . htmlentities("Espontânea") . "</h1></td></tr>";
			}else{
				$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>Modo de " . htmlentities("Ventilação") . ": Equipamento</h1></td></tr>";
				$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>Via Bipap:  ({$cont}) " . htmlentities("Contínuo") . " &nbsp;&nbsp;&nbsp; ({$cont2}) Intermitente</h1></td></tr>";
			}

		}

		//Oxigenio
		//atualizaï¿½ï¿½o Eriton 22-03-2013	
		if($oxigenio_sn == 's'){
			$oxi = 'x';
			$oxi_teste = 'Sim';
		}if($oxigenio_sn == 'n'){
			$oxi2 = 'x';
			$oxi_teste = htmlentities("Não");
		}
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>" . htmlentities("Oxigênio") . " - </b>{$oxi_teste}</td>";
		$html.="</tr>";

		if($oxigenio == 1){
			$cat_o = 'x';
		}
		if($oxigenio == 2){
			$cat_ocu = 'x';
		}
		if($oxigenio == 3){
			$masc_res = 'x';
		}
		if($oxigenio == 4){
			$masc_vent = 'x';
		}
		if($oxigenio == 5){
			$conce = 'x';
		}
		if ($oxigenio_sn == 's'){
			//$html.= "<tr> <td colspan='5' ><h1 class='relatorio-texto'>Oxig&ecirc;nio: ({$oxi})Sim  ({$oxi2})N&atilde;o </h1></td></tr>";
			$html.="<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$cat_o}) Cateter de " . htmlentities("Oxigênio") . " &nbsp;&nbsp;&nbsp;  " . htmlentities("N°") . ": {$vent_cateter_oxigenio_num} L/MIN: {$vent_cateter_oxigenio_quant}  </h1></td></tr>";
			$html.="<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$cat_ocu}) Cateter tipo " . htmlentities("Óculos") . " &nbsp;&nbsp;&nbsp; {$vent_cateter_oculos_num} L/MIN </h1></td></tr>";
			$html.="<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$masc_res}) " . htmlentities("Máscara Reservatório") . " &nbsp;&nbsp;&nbsp; {$vent_respirador_tipo} L/MIN </h1></td></tr>";
			$html.="<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$masc_vent}) " . htmlentities("Máscara") . " de Ventury &nbsp;&nbsp;&nbsp; {$vent_mascara_ventury} % </h1></td></tr>";
			$html.="<tr> <td colspan='5' ><h1 class='relatorio-texto'>({$conce}) Concentrador &nbsp;&nbsp;&nbsp; {$vent_concentrador_num} % </h1></td></tr>";
		}
		//Fim atualizaï¿½ï¿½o Eriton 22-03-2013

		//Aspiraï¿½ï¿½es
		if($aspiracoes_sn == 's'){
			$aspiracoes = 'Sim';
		}else{
			$aspiracoes = htmlentities("Não");
		}
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>" . htmlentities("Aspirações") . " - $aspiracoes</b></td>";
		$html.="</tr>";

		if($aspiracao_secrecao == 'a'){
			$ause = 'x';
		}
		if($aspiracao_secrecao == 'p'){
			$pres = 'x';
		}
		$html.="<tr> <td colspan='5' ><h1 class='relatorio-texto'>" . htmlentities("Número de Aspirações") . "/Dia:  &nbsp;&nbsp;&nbsp; {$aspiracoes_num}</h1></td></tr>";
		$html.="<tr> <td colspan='5' ><h1 class='relatorio-texto'>" . htmlentities("Secreção") . ":  &nbsp;&nbsp;&nbsp; ({$ause}) Ausente ({$pres}) Presente</h1></td></tr>";

		//atualizaï¿½ï¿½o Eriton 17-04-2013
		if($aspiracao_secrecao == 'p'){

			$html.= "<tr><td colspan = '5'><h1 class='relatorio-texto'><b>" . htmlentities("Características da Secreção") . " </b></h1></td></tr>";
			if($cor_sec == 1){
				$cla = 'x';
			}
			if($caracteristicas_sec == 2){
				$esp = 'x';
			}
			if($cor_sec == 3){
				$ama = 'x';
			}
			if($cor_sec == 4){
				$esv = 'x';
			}
			if($cor_sec == 5){
				$sang = 'x';
			}
			if($caracteristicas_sec == 6){
				$flui = 'x';
			}
			$html.= "<tr><td colspan = '5'><h1 class='relatorio-texto'> ($flui) Fluida  </h1></td></tr>";
			$html.= "<tr><td colspan = '5'><h1 class='relatorio-texto'> ($esp) Espessa </h1></td></tr>";
			$html.= "<tr><td colspan = '5'><h1 class='relatorio-texto'> ($cla) Clara  </h1></td></tr>";
			$html.= "<tr><td colspan = '5'><h1 class='relatorio-texto'> ($ama) Amarelada </h1></td></tr>";
			$html.= "<tr><td colspan = '5'><h1 class='relatorio-texto'> ($esv) Esverdeada </h1></td></tr>";
			$html.= "<tr><td colspan = '5'><h1 class='relatorio-texto'> ($sang) Sanguinolenta </h1></td></tr>";

		}

		//Fim Atualizaï¿½ï¿½o Eriton 17-04-2013
		//Avaliaï¿½ï¿½o Abdominal

		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>" . htmlentities("Avaliação") . " Abdominal</b></td>";
		$html.="</tr>";
		if($plano == 1){
			$plan = 'x';
		}
		if($flacido == 1){
			$fla = 'x';
		}
		if($distendido == 1){
			$dist = 'x';
		}
		if($timpanico == 1){
			$timp = 'x';
		}
		if($indo_palpacao == 1){
			$indo_a = 'x';
		}
		if($doloroso == 1){
			$dolo = 'x';
		}
		if($globoso == 1){
			$glob = 'x';
		}


		if($rha == 'a'){
			$ause_rha = 'x';
		}
		if($rha == 'p'){
			$pres_rha = 'x';
		}

		$html.= "<tr> <td colspan='5'><h1 class='relatorio-texto'> ($plan) Plano </h1></td></tr>";
		$html.= "<tr> <td colspan='5'><h1 class='relatorio-texto'> ($fla)  " . htmlentities("Flácido") . " </h1></td></tr>";
		$html.= "<tr> <td colspan='5'><h1 class='relatorio-texto'> ($dist) Distendido </h1></td></tr>";
		$html.= "<tr> <td colspan='5'> <h1 class='relatorio-texto'>($timp) " . htmlentities("Timpânico") . " </h1></td></tr>";
		$html.= "<tr> <td colspan='5'> <h1 class='relatorio-texto'>($glob) Globoso </h1></td></tr>";
		$html.= "<tr> <td colspan='5'><h1 class='relatorio-texto'> ($indo_a) Indolor " . htmlentities("à Palpação") . " </h1></td></tr>";
		$html.= "<tr> <td colspan='5'><h1 class='relatorio-texto'> ($dolo)   Doloroso </h1></td></tr>";
		$html.= "<tr> <td colspan='5'><h1 class='relatorio-texto'> RHA ($pres_rha) Presente ($ause_rha) Ausente</h1></td></tr>";

		/* VIA ALIMENTAï¿½ï¿½O Atualizaï¿½ï¿½o Eriton 18-04-2013*/

		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>Via " . htmlentities("Alimentação") . "</b></td>";
		$html.="</tr>";

		if($parental == 1 ){
			$paren = 'x';
		}
		if($oral == 1 ){
			$ora = 'x';
		}
		if($sne == 1 ){
			$Sn = 'x';
		}
		if($gtm_sonda_gastro == 1 ){
			$gt = 'x';
		}
		if($gtm_sonda_gastro == 2 ){
			$sond = 'x';
		}
		//Atualizaï¿½ï¿½o Eriton 25-03-2013
		/*if($gtm_visivel == 'v' ){
			$vis = 'Sim';
		}
		if($gtm_visivel == 'i' ){
			$vis = 'N&atilde;o';
		}*/

		if($dieta_sn == 's'){


			$html.= "<tr><td colspan = '5'><h1 class='relatorio-texto'> Dieta Zero (x) Sim  ( )" . htmlentities("Não") . " </h1></td></tr>";
			$html.= "<tr><td colspan = '5'><h1 class='relatorio-texto'> ($paren) Parenteral </h1></td></tr>";
		}elseif($dieta_sn == 'n'){
			$html.= "<tr><td colspan = '5'><h1 class='relatorio-texto'> Dieta Zero ( ) Sim  (x)" . htmlentities("Não") . " </h1></td></tr>";
			$html.="<tr><td colspan = '5'><h1 class='relatorio-texto'> ($ora) Oral </h1></td></tr>";
			$html.="<tr><td colspan = '5'><h1 class='relatorio-texto'> ($sn) SNE    " . htmlentities("Nº") . ": {$sne_num} </h1></tr></td>";
			//$html.="<tr><td colspan = '5'><h1 class='relatorio-texto'>V&Iacute;S&Iacute;VEL: $vis </h1></td></tr>";		
			$html.="<tr><td colspan = '5' bgcolor='#D3D3D3' align='center'><h1 class='relatorio-texto'>GTM: </h1></td></tr>";
			$html.="<tr><td colspan = '5'><h1 class='relatorio-texto'>($gt) Button      " . htmlentities("Nº") . ": {$gtm_num}  Profundidade: {$gtm_profundidade} cm</h1></td></tr>";
			$html.="<tr><td colspan = '5'><h1 class='relatorio-texto'>($sond) Sonda Gastro " . htmlentities("Nº") . ": {$quant_sonda}</h1></td></tr>";

			//Fim atualizaï¿½ï¿½o Eriton 25-03-2013
		}

		///DIETA

		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>Dieta</b></td>";
		$html.="</tr>";

		if($sistema_aberto == 1){
			$siste_a = 'x';
		}
		if($sistema_fechado == 1){
			$siste_f = 'x';
		}
		if($dieta_npt == 1){
			$dieta_np = 'x';
		}
		if($npp == 1){
			$np = 'x';
		}
		if($dieta_artesanal == 1){
			$da = 'x';
		}

		if($dieta_sn == 's'){
			$html.="<tr><td colspan='5'><h1 class='relatorio-texto'>  ($dieta_np) &nbsp;&nbsp;  NPT 	
			&nbsp;&nbsp;Nome: {$quant_npt}   " . htmlentities("Vazão") . ": {$vazao_npt}  ML/H </h1></tr></td>";
			$html.="<tr><td ><h1 class='relatorio-texto'> ($np) &nbsp;&nbsp; NPP &nbsp;&nbsp;  Nome: {$quant_npp}  " . htmlentities("Vazão") . ": {$vazao_npp} ML/H </h1></tr></td>";
		}elseif($dieta_sn == 'n'){
			$html.="<tr><td colspan='5'><h1 class='relatorio-texto'> ($siste_a) Sistema aberto 	&nbsp;&nbsp;&nbsp;&nbsp;	Nome: {$nome_sistema_aberto} &nbsp;&nbsp;&nbsp;&nbsp;   Uso: {$uso_sistema_aberto}</h1> </td></tr>";
			$html.="<tr><td colspan='5'><h1 class='relatorio-texto'> ($siste_f) Sistema fechado 	&nbsp;&nbsp;&nbsp;&nbsp;	Nome: {$quant_sistema_fechado}  &nbsp;&nbsp;&nbsp;&nbsp;  " . htmlentities("Vazão") . ": {$vazao}  ML/H</h1> </td></tr>";
			$html.= "<tr><td><h1 class='relatorio-texto'> ($da) Dieta Artesanal </h1></tr></td>";
		}

		//Suplemento
		if($suplemento_sn == 's'){
			$local_s='x';
		}
		if($suplemento_sn == 'n'){
			$local_n='x';
		}
		if($dieta_sn == 'n'){
			$html.= "<tr><td colspan='5'></td></tr><tr>";
			$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>Suplemento:  ({$local_s}) Sim    ({$local_n}) " . htmlentities("Não") . " </b></td>";
			$html.="</tr>";

			while($row4 = mysql_fetch_array($result4)){
				$local_suplemento = $row4['NOME'];
				$frequencia_suplemento = $row4['FREQUENCIA'];

				$html.= "<tr><td colspan='5'><h1 class='relatorio-texto'> Nome:  {$local_suplemento}  &nbsp;&nbsp;&nbsp;&nbsp;  " . htmlentities("Frequência") . ": {$frequencia_suplemento}  </h1></td></tr>";

			}
		}
		//avaliacao genital
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 class='relatorio-texto'><b>" . htmlentities("Avaliação") . " Genital</b></td>";
		$html.="</tr>";

		if($integra_genital == 1){
			$int_sim ='x';
		}
		if($integra_genital == 2){
			$int_nao ='x';
		}
		if($secrecao == 1){
			$secre_sim ='x';
		}
		if($secrecao == 2){
			$secre_nao ='x';
		}
		if($sangramento == 1){
			$sang_sim ='x';
		}
		if($sangramento == 2){
			$sang_nao ='x';
		}
		if($prurido == 1){
			$pru_sim ='x';
		}
		if($prurido == 2){
			$pru_nao ='x';
		}
		if($edema == 1){
			$ede_sim ='x';
		}
		if($edema == 2){
			$ede_nao ='x';
		}

		$html.= "<tr><td colspan='5'><h1 class='relatorio-texto'> " . htmlentities("Íntegra") . ": ($int_sim)Sim  	($int_nao) " . htmlentities("Não") . " </h1></td></tr>";
		$html.= "<tr><td ><h1 class='relatorio-texto'> " . htmlentities("Secreção") . ": ($secre_sim) Sim ($secre_nao) " . htmlentities("Não") . "  </h1></td></tr>";
		$html.= "<tr><td ><h1 class='relatorio-texto'> Sangramento: ($sang_sim) Sim ($sang_nao) " . htmlentities("Não") . " </h1></td></tr>";
		$html.= "<tr><td ><h1 class='relatorio-texto'> Prurido: ($pru_sim) Sim  ($pru_nao) " . htmlentities("Não") . "  </h1></td></tr>";
		$html.= "<tr><td ><h1 class='relatorio-texto'> Edema: ($ede_sim) Sim ($ede_nao) " . htmlentities("Não") . "  </h1></td></tr>";




		/* Tï¿½PICOS EM USO */
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>Topicos em Uso</b></td>";
		$html.="</tr>";

		if($topicos_sn == 's'){
			$topico_s='x';
		}
		if($topicos_sn == 'n'){
			$topico_n='x';
		}
		$html.= "<tr><td colspan='5'><h1 class='relatorio-texto'> Topicos:  ({$topico_s}) Sim    ({$topico_n}) " . htmlentities("Não") . "  </h1></td></tr>";

		while($row6 = mysql_fetch_array($result6)){
			$local_topico = $row6['LOCAL'];
			$frequencia = $row6['FREQUENCIA'];

			$html.= "<tr><td colspan='5'><h1 class='relatorio-texto'> Nome:  {$local_topico}  &nbsp;&nbsp;&nbsp;&nbsp;  " . htmlentities("Frequência") . ": {$frequencia}  </h1></td></tr>";
		}

		/* DISPOSITIVO INTRAVENOSO Atualizaï¿½ï¿½o Eriton 18-04-2013*/
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		if($dispositivos_intravenosos == 2){
			$dispositivos_sn = 'Sim';
		}else{
			$dispositivos_sn = htmlentities("Não");
		}
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>Dispositivos Intravenosos - $dispositivos_sn</b></td>";
		$html.="</tr>";
		if($dispositivos_intravenosos == 2){
			if($cvc == 1){
				$cvc_teste ='x';
			}
			if($local_uno == 1){
				$uno ='x';
			}
			if($local_duplo == 2){
				$dup ='x';
			}
			if($local_triplo == 3){
				$trip ='x';
			}
			if($avp == 1){
				$avp_s ='x';
			}
			if($port == 1){
				$port_h ='x';
			}

			$picc_x = $picc ==1? 'x':'';

			$html.="<tr><td colspan = '5'><h1 class='relatorio-texto'> <b> ($cvc_teste) CVC</b></td></tr>";
			if($cvc == 1){
				$html.= "<tr><td colspan = '5'><h1 class='relatorio-texto'> ($uno) Uno Lumen Local:   {$local_uno}</h1></td></tr>";
				$html.= "<tr><td colspan = '5'><h1 class='relatorio-texto'> ($dup) Duplo Lumen  Local: {$local_duplo} </h1></td></tr>";
				$html.="<tr><td colspan = '5'><h1 class='relatorio-texto'> ($trip) Triplo Lumen Local:  {$local_triplo} </h1></td></tr>";
			}
			$html.="<tr><td ><h1 class='relatorio-texto'> ($avp_s)AVP  Local: {$avp_local} </h1></td></tr>";
			$html.= "<tr><td><h1 class='relatorio-texto'> ($port_h) Port-O-Cath " . htmlentities("Nº") . ":  {$quant_port}    Local: {$port_local} </h1></td></tr>";
			$html.="<tr><td ><h1 class='relatorio-texto'> ($picc_x)Cateter de PICC  Local: {$picc_local} </h1></td></tr>";
			$html.="<tr><td colspan = '5'><h1 class='relatorio-texto'> Outros: {$outros_intra} </h1></td></tr>";
		}

		/* DISPOSITIVOS PARA ELIMINAï¿½ï¿½ES E/OU EXCREï¿½ï¿½ES */
		if($eliminacoes == 2){
			$eliminacoes_teste_s = 'x';
		}else{
			$eliminacoes_teste_n = 'x';
		}
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>Dispositivos para " . htmlentities("Eliminação e/ou Excreções") . "</b></td>";
		$html.="</tr>";
		$html.= "<tr><td colspan='5'><h1 class='relatorio-texto'> " . htmlentities("Eliminação e/ou Excreções") . "  ({$eliminacoes_teste_s}) Sim    ({$eliminacoes_teste_n}) " . htmlentities("Não") . "  </h1></td></tr>";
		if($fralda == 1){
			$fral='x';
		}
		if($fralda_dia == 1){
			$fralda_p='x';
		}
		if($fralda_dia == 2){
			$fralda_m='x';
		}
		if($fralda_dia == 3){
			$fralda_g='x';
		}
		if($fralda_dia == 4){
			$fralda_xg='x';
		}
		if($urinario == 1){
			$uri ='x';
		}
		if($sonda == 1){
			$sonda_f ='x';
		}
		if($sonda_alivio == 1){
			$sonda_a='x';
		}
		if($colostomia == 1){
			$colos='x';
		}
		if($cistostomia == 1){
			$cisto='x';
		}
		if($iliostomia == 1){
			$ilio='x';
		}
		if ($tamanho_fralda == '1')
			$fralda_p = 'x';
			if ($tamanho_fralda == '2')
				$fralda_m = 'x';
		if ($tamanho_fralda == '3')
			$fralda_g = 'x';
		if ($tamanho_fralda == '4')
			$fralda_xg = 'x';


		if($eliminacoes == 2){
			$html.="<tr><td colspan = '5'><h1 class='relatorio-texto'>  ($fral)Fralda     Unidade/Dia:    {$fralda_dia}    Tamanho: ($fralda_p)P  ($fralda_m)M  ($fralda_g)G ($fralda_xg)XG </h1></td></tr>";
			$html.="<tr><td colspan = '5'><h1 class='relatorio-texto'>  ($uri) Dispositivo " . htmlentities("Urinário    Nº") . ": {$quant_urinario}  </h1></td></tr>";
			$html.="<tr><td> <h1 class='relatorio-texto'> ($sonda_f) Sonda de Foley    N&ordm;: {$quant_foley}</h1></td></tr>";
			$html.="<tr><td><h1 class='relatorio-texto'> ($sonda_a)  Sonda de " . htmlentities("Alívio") . ": {$quant_alivio} </h1> </td></tr>";
			$html.="<tr><td><h1 class='relatorio-texto'>  ($colos) Colostomia Placa/Bolsa 	" . htmlentities("Nº") . ": {$quant_colostomia} </h1>  </td></tr>";
			$html.= "<tr><td><h1 class='relatorio-texto'> ($ilio) Iliostomia Placa/Bolsa		" . htmlentities("Nº") . ": {$quant_iliostomia} </h1></td></tr>";
			$html.= "<tr><td><h1 class='relatorio-texto'> ($cisto) Cistostomia		" . htmlentities("Nº") . ": {$quant_cistostomia} </h1></td></tr>";
		}


		/* DRENOS */
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>Drenos</b></td>";
		$html.="</tr>";

		if($dreno_sn == 's'){
			$dreno_s='x';
		}
		if($dreno_sn == 'n'){
			$dreno_n='x';
		}
		$html.= "<tr><td colspan='5'><h1 class='relatorio-texto'> Dreno:  ({$dreno_s}) Sim    ({$dreno_n}) " . htmlentities("Não") . "  </h1></td></tr>";

		while($row7 = mysql_fetch_array($result7)){
			$tipo_dreno = $row7['DRENO'];
			$local_dreno = $row7['LOCAL'];

			$html.= "<tr><td colspan='5'><h1 class='relatorio-texto'> Tipo: {$tipo_dreno}   Local:  {$local_dreno} </h1></td></tr>";
		}


		//FERIDAS           

		$numero_ferida = $numero_imagem = 0;
		if ($feridas_sn == 's') {

			$x = "Sim";
			$html .= "<tr style='background-color:#D3D3D3;'><td colspan='5'><h1 class='relatorio-texto'><b>Feridas - $x</b></td></tr>";

			$sql = "SELECT
                    quadro_feridas_enfermagem.*, full_path, path, quadro_feridas_enf_local.LOCAL as descricao_local_ferida,
                    quadro_feridas_enfermagem.ID as idQuadroFeridas
			FROM
			quadro_feridas_enfermagem
			  LEFT JOIN files_uploaded ON files_uploaded.id = quadro_feridas_enfermagem.FILE_UPLOADED_ID
			  LEFT JOIN quadro_feridas_enf_local ON quadro_feridas_enf_local.id = quadro_feridas_enfermagem.LOCAL
			WHERE
			AVALIACAO_ENF_ID = '{$id_a}'";
			$result = mysql_query($sql);

			while($row = mysql_fetch_array($result)){
        ++$numero_ferida;

				$local = $row['LOCAL'];
        $descricaoLocalFerida = $row['descricao_local_ferida'];
				$lado = $row['LADO'];
				$tipo = $row['TIPO'];
				$cobertura = $row['COBERTURA'];
				$tamanho = $row['TAMANHO'];
				$profundidade = $row['TAMANHO'];
				$tecido = $row['TECIDO'];
				$grau = $row['GRAU'];
				$humidade = $row['HUMIDADE'];
				$exsudato = $row['EXSUDATO'];
				$contaminacao = $row['CONTAMINACAO'];
				$pele = $row['PELE'];
				$tempo = $row['TEMPO'];
				$odor = $row['ODOR'];
				$periodo = $row['PERIODO'];
        $status_ferida = $row['STATUS_FERIDA'];
				$observacao =  $row['observacao'];

        $imagem_ferida = 'SEM IMAGEM';
        if (isset($row['full_path']) && !empty($row['full_path'])) {
          $feridaImagemSrc = App\Helpers\Storage::getImageUrl($row['path']);
          $imagem_referencia = $descricaoLocalFerida;
          $imagem_ferida = 'EM ANEXO';
          $imagem_feridas .= <<<IMAGEM_FERIDA
<p>
  <p style='font-size:13px'>{$imagem_referencia}</p>
  <p><img style="" src="{$feridaImagemSrc}" width="20%" ></img></p>
</p>
<br/>
IMAGEM_FERIDA;
        }


				$sql_local = "SELECT
				LOCAL
				FROM
				quadro_feridas_enf_local
				WHERE
				ID = $local
				";
				$result_local = mysql_query($sql_local);
				$local_ferida = mysql_result($result_local, 0);

				$lado_ferida = '';
				if($lado == 1){
					$lado_ferida = 'DIREITO';
				}else if($lado == 2){
					$lado_ferida = 'ESQUERDO';
				}



				$sql_tipo = "SELECT
TIPO
FROM
quadro_feridas_enf_tipo
WHERE
ID = $tipo
";
				$result_tipo = mysql_query($sql_tipo);
				$tipo_ferida = mysql_result($result_tipo,0);

				
				$sql_cobertura = "SELECT
                    TIPO
                FROM
                    quadro_feridas_enf_lesao as q inner join
                    quadro_feridas_enfermagem_cobertura as qf  on q.ID = qf.cobertura_id


                WHERE
                   qf.quadro_feridas_enfermagem_id = {$row['idQuadroFeridas']}
                ";



				$cobertura_ferida = '';
				$result_cobertura = mysql_query($sql_cobertura);
				while($rowCobertura = mysql_fetch_array($result_cobertura)) {
					$cobertura_ferida .=" | ".$rowCobertura['TIPO'];
				}

				$sql_tamanho = "SELECT
TAMANHO
FROM
quadro_feridas_enf_tamanho
WHERE
ID = $tamanho
";
				$result_tamanho = mysql_query($sql_tamanho);
				$tamanho_ferida = mysql_result($result_tamanho, 0);

				$sql_profundidade = "SELECT
PROFUNDIDADE
FROM
quadro_feridas_enf_profundidade
WHERE
ID = $profundidade
";
				$result_profundidade = mysql_query($sql_profundidade);
				$profundidade_ferida = mysql_result($result_profundidade, 0);

				$sql_tecido = "SELECT
TECIDO
FROM
quadro_feridas_enf_tecido
WHERE
ID = $tecido
";
				$result_tecido = mysql_query($sql_tecido);
				$tecido_ferida = mysql_result($result_tecido, 0);

				$sql_grau = "SELECT
GRAU
FROM
quadro_feridas_enf_grau
WHERE
ID = $grau
";
				$result_grau = mysql_query($sql_grau);
				$grau_ferida = mysql_result($result_grau, 0);

				$sql_humidade = "SELECT
humidade
FROM
quadro_feridas_enf_humidade
WHERE
ID = $humidade
";
				$result_humidade = mysql_query($sql_humidade);
				$humidade_ferida = mysql_result($result_humidade, 0);

				$sql_exsudato = "SELECT
exsudato
FROM
quadro_feridas_enf_exsudato
WHERE
ID = $exsudato
";
				$result_exsudato = mysql_query($sql_exsudato);
				$exsudato_ferida = mysql_result($result_exsudato, 0);

				$sql_contaminacao = "SELECT
contaminacao
FROM
quadro_feridas_enf_contaminacao
WHERE
ID = $contaminacao
";
				$result_contaminacao = mysql_query($sql_contaminacao);
				$contaminacao_ferida = mysql_result($result_contaminacao, 0);

				$sql_pele = "SELECT
pele
FROM
quadro_feridas_enf_pele
WHERE
ID = $pele
";
				$result_pele = mysql_query($sql_pele);
				$pele_ferida = mysql_result($result_pele, 0);

				$sql_tempo = "SELECT
tempo
FROM
quadro_feridas_enf_tempo
WHERE
ID = $tempo
";
				$result_tempo = mysql_query($sql_tempo);
				$tempo_ferida = mysql_result($result_tempo, 0);

				$sql_periodo = "SELECT
PERIODO_TROCA
FROM
quadro_feridas_enf_periodo
WHERE
ID = $periodo
";
				$result_periodo = mysql_query($sql_periodo);
				$periodo_ferida = mysql_result($result_periodo, 0);

				$sql_status = "SELECT
status_ferida
FROM
quadro_feridas_enf_status
WHERE
ID = $status_ferida
";
$result_status = mysql_query($sql_status);
$status_ferida = mysql_result($result_status, 0);

if ($numero_ferida > 1) {
	$linha_ferida = "<tr><td colspan='5'><hr /></td></tr>";
}
$html .= $linha_ferida;

$odor = $odor == 1 ? "NÃO" : "FETIDO";

if($total_feridas > 1){
	$html .= "<tr><td colspan='5' ><h1 class='relatorio-texto'><b>FERIDA $numero_ferida</b></td></tr>";
}

$colunas = array(
	'LOCAL' => $local_ferida,
	'OBSERVAÇÃO' => $observacao,
	'LADO' => $lado_ferida, 
	'TIPO DE LESÃO' => $tipo_ferida,
	'COBERTURA ATUAL UTILIZADA' => $cobertura_ferida,
	'TAMANHO' => $tamanho_ferida,
	'PROFUNDIDADE' => $profundidade_ferida,
	'TIPO DE TECIDO' => $tecido_ferida,
	'GRAU DA LESÃO' => $grau_ferida,                	
	'GRAU DA UMIDADE' => $humidade_ferida,
	'TIPO DE EXSUDATO' => $exsudato_ferida,
	'GRAU DE CONTAMINAÇÃO' => $contaminacao_ferida,
	'PELE PERI-LESÃO' => $pele_ferida,
	'TEMPO DA LESÃO' => $tempo_ferida,
	'ODOR' => $odor,
	'PERÍODO DE TROCA DA COBERTURA EM USO' => $periodo_ferida,
  'STATUS DA FERIDA' => $status_ferida,
  'IMAGEM DA FERIDA' => $imagem_ferida,

	);
foreach ($colunas as $label => $valor) {
	$html .= "
	<tr>
		<td><h1 class='relatorio-texto'>" . htmlentities($label) . "</h1></td>
		<td><h1 class='relatorio-texto'>{$valor}</h1></td>
	</tr>
	";
				}
			}

		}else{
			$x = "Não";
			$html .= "<tr><td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>Feridas - $x</b></td></tr>";
		}
		/* SINAIS VITAIS */

		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>Sinais vitais</b></td>";
		$html.="</tr>";

		$html.= "<tr><td colspan = '5'><h1 class='relatorio-texto'><b>PA " . htmlentities("Sistólica") . ": {$pa_sistolica}MMHG</b></h1></td></tr>";
		$html.= "<tr><td><h1 class='relatorio-texto'><b>PA " . htmlentities("Sistólica") . ": {$pa_diastolica} MMHG</b></h1></td></tr>";
		$html.= "<tr><td><h1 class='relatorio-texto'><b>FC: {$fc} BPM</b></h1></td></tr>";
		$html.= "<tr><td><h1 class='relatorio-texto'><b>FR: {$fr} INC/MIN</b></h1></td></tr>";
		$html.= "<tr><td><h1 class='relatorio-texto'><b>" . htmlentities("SPO²") . ":  {$spo2} % </b></h1></td></tr>";
		$html.= "<tr><td><h1 class='relatorio-texto'><b>Temperatura: {$temperatura} " . htmlentities("°") . "C</b></h1></td></tr>";
		//Dor
		$html.= "<tr><td colspan='5'></td></tr><tr>";
		$html.= "<td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>" . htmlentities("Avaliação") . " da dor</b></td>";
		$html.="</tr>";

		if($dor_sn == 's'){
			$dor_s='x';
		}
		if($dor_sn == 'n'){
			$dor_n='x';
		}
		$html.= "<tr><td colspan='5'><h1 class='relatorio-texto'> Dor:  ({$dor_s}) Sim    ({$dor_n}) " . htmlentities("Não") . "  </h1></td></tr>";

		while($row2 = mysql_fetch_array($result2)){
			$local_dor = $row2['LOCAL'];
			$dor_inte = $row2['INTENSIDADE'];

			$html.= "<tr><td colspan='5'><h1 class='relatorio-texto'> Local: {$local_dor}   Intensidade: {$dor_inte} </h1></td></tr>";
		}

		// Servicos Multidisciplinar
		$html.= "<tr bgcolor='#D3D3D3' >
                        <td colspan='5' class='relatorio-centralizar'>
                        <h1 class='relatorio-texto'> Serviço Multidiciplinar Hospitalar </h1>
                        </td>
                    </tr>";
		$html .= "<tr>
                <td colspan='5' >
                 <h1 class='relatorio-texto'> Visita Médico (conforme modalidade do score):
                ";
               $html .= $arrayMultidisciplinar['medico'] == -1 ? '' : $arrayMultidisciplinar['medico'] == 1 ? 'Semanal' :
				   $arrayMultidisciplinar['medico'] == 2 ? 'Mensal' :  $arrayMultidisciplinar['medico'] == 3 ?'Quinzenal':
					   $arrayMultidisciplinar['medico'] == 'S' ? 'Sim' : 'Não';
		$html .=   " </h1></td>
                </tr>";
		$html .= "<tr>
                <td colspan='5' >
                 <h1 class='relatorio-texto'> Visita Enfermeiro (conforme modalidade do score):
                ";
		$html .= $arrayMultidisciplinar['enfermeiro'] == -1 ? '' : $arrayMultidisciplinar['enfermeiro'] == 1 ? 'Semanal' :
			$arrayMultidisciplinar['enfermeiro'] == 2 ? 'Mensal' : $arrayMultidisciplinar['enfermeiro'] == 3 ? 'Quinzenal':
				$arrayMultidisciplinar['enfermeiro'] == 4 ? '2x semana': $arrayMultidisciplinar['enfermeiro'] == 'S' ?'Sim':
					'Não';
		$html .=   " </h1></td>
                </tr>";
		$html .= "<tr>
                <td colspan='5' >
                 <h1 class='relatorio-texto'>Atendimento Nutrição:
                ";
		$html .= $arrayMultidisciplinar['nutricao'] == -1 ? '' : $arrayMultidisciplinar['nutricao'] == 'S' ? 'Sim' :'Não';
		$html .=   " </h1></td>
                </tr>";
		$html .= "<tr>
                <td colspan='5' >
                 <h1 class='relatorio-texto'>Atendimento Fissio Motora:
                ";
		$html .= $arrayMultidisciplinar['fissio_motora'] == -1 ? '' : $arrayMultidisciplinar['fissio_motora'] == 'S' ? 'Sim' :'Não';
		$html .=   " </h1></td>
                </tr>";
		$html .= "<tr>
                <td colspan='5' >
                 <h1 class='relatorio-texto'>Atendimento Fissio Respiratória:
                ";
		$html .= $arrayMultidisciplinar['fissio_respiratoria'] == -1 ? '' : $arrayMultidisciplinar['fissio_respiratoria'] == 'S' ? 'Sim' :'Não';
		$html .=   " </h1></td>
                </tr>";
		$html .= "<tr>
                <td colspan='5' >
                 <h1 class='relatorio-texto'>Atendimento Fono:
                ";
		$html .= $arrayMultidisciplinar['fissio_fono'] == -1 ? '' : $arrayMultidisciplinar['fissio_fono'] == 'S' ? 'Sim' :'Não';
		$html .=   " </h1></td>
                </tr>";


		//PAD ENFERMAGEM
		$html.= "<tr>
         <td colspan='5' bgcolor='#D3D3D3' class='relatorio-centralizar'><h1 class='relatorio-texto'><b>Pad Enfermagem</b></h1></td>";
		$html.="</tr>";

		$html.= "<tr><td colspan='5'><h1 class='relatorio-texto'>Pad Enfermagem: ".htmlentities($pad)." </h1></td></tr>";

if (! empty($imagem_feridas))
  $html.= <<<ANEXOS
<tr >
  <td colspan='5'>
   <h1 style='font-size:16px;'>ANEXOS</h1>
    <p style='font-size:14px;'>Feridas</p><br>
    {$imagem_feridas}
  </td>
</tr>
ANEXOS;

$html.= "</table>";
$sql = "SELECT usr.ASSINATURA
FROM usuarios as usr 
WHERE idUsuarios = '{$enfermeiro_id}'";
$result = mysql_query($sql);
$assinatura = mysql_result($result,0,0);
$html .= "<table width='100%'><tr><td><center><img src='{$assinatura}' width='20%' /></center></td></tr></table>";
$paginas []= $html.="</form>";
		//print_r($html);

$mpdf=new mPDF('pt','A4',20);
$stylesheet = file_get_contents('../utils/relatorio.css');
$mpdf->WriteHTML($stylesheet, 1);
$mpdf->SetHeader('Página {PAGENO} de {nbpg}');
$ano = date("Y");
$mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']}"." &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));

$mpdf->WriteHTML("<html><body>");
$mpdf->showImageErrors = false;
$flag = false;
foreach($paginas as $pag){
	if($flag) $mpdf->WriteHTML("<formfeed>");
	$mpdf->WriteHTML($pag);
	$flag = true;
}
$mpdf->WriteHTML("</body></html>");
$mpdf->Output('prescricao.pdf','I');
exit;

}

}

$p = new Paciente_imprimir_ficha();
$p->detalhes_capitacao_medica($_GET['id'],$_GET['id_a'],$_GET['empresa']);


?>
