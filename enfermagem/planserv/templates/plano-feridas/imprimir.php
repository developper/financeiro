<?php
$id = session_id();
if(empty($id))
    session_start();

include_once($_SERVER['DOCUMENT_ROOT'] . '/utils/mpdf/mpdf.php');

use \App\Controllers\Planserv\Feridas;

$html = <<<HTML
<style type='text/css'>
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
    
    .planserv {
        font-family: Times, "Times New Roman", sans-serif;
        font-size: 12px;
        border: 1px solid rgb(84,141,212);
    }

    .planserv td {
        font-family: Times, "Times New Roman", sans-serif;
        padding-left: 2px;
        padding-right: 2px;
        border-bottom: 1px solid rgb(84,141,212);
    }

    .planserv input {
        margin-bottom: 5px;
        margin-top: -10px;
        border: 1.5px solid rgb(84,141,212);
    }

    .planserv textarea {
        margin-left: 2px;
        margin-top: 2px;
        border: 1.5px solid rgb(84,141,212);
    }

    .row-periodo {
        height: 20px;
        background-color: rgb(84,141,212);
    }

    .row-section {
        text-align: center;
        height: 20px;
        background-color: rgb(84,141,212);
        color: #ffffff;
    }

    .row-title {
        height: 80px;
    }

    .row-title td {
        font-family: "Helvetica Neue Light", "HelveticaNeue-Light", "Helvetica Neue", Calibri, Helvetica, Arial;
        font-size: 22px !important;
        color: rgb(84,141,212);
    }

    .options {
        margin-left: 10px;
    }

    .have-border td {
        border: 1px solid rgb(84,141,212);
        border-collapse: collapse;
    }

    #mostrar-menu {
        cursor: pointer;
    }
</style>
<table class="planserv" cellspacing="0" cellpadding="0" align="center" width="100%">
    <tbody>
    <tr class="row-title">
        <td colspan="8" valign="top" align="center">
            <img src="{$_SERVER['DOCUMENT_ROOT']}/utils/logo_ficha_curativo.jpg" width="60" height="60"/>
            <span style="margin: 0 auto;">PLANO TERAPÊUTICO TRATAMENTO DE FERIDAS</span>
        </td>
    </tr>

    <!---------------------------------------------------------------------------------------->
    <tr>
        <td class="row-periodo" colspan="8" valign="top" align="center" >
            <b>1. Identificação do Prestador</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" valign="middle">
            <p><b>Prestador:</b></p> Mederi Saúde Domiciliar
        </td>
        <td colspan="2" valign="top" >
            <p><b>Telefone:</b></p> (75) 3022-9156
        </td>
        <td colspan="3" valign="top" >
            <p><b>Email:</b></p> saad@mederi.com.br
        </td>
    </tr>
    <tr>
        <td class="row-periodo" colspan="8" valign="top" align="center" >
            <b>2. Identificação do Médico Assistente e Enfermeiro Especialista</b>
        </td>
    </tr>
    <tr>
        <td colspan="8" valign="middle">
            <p><b>Nome do Médico:</b></p>
            {$response['profissionais']['med']}
        </td>
    </tr>
    <tr>
        <td colspan="8" valign="top" >
            <p><b>Nome do Enfermeiro:</b></p>
            {$response['profissionais']['enf']}
        </td>
    </tr>

    <tr>
        <td class="row-periodo" colspan="8" valign="top" align="center" >
            <b>3. Dados do Beneficiário</b>
        </td>
    </tr>
    <tr>
        <td colspan="4" valign="middle">
            <p><b>Paciente:</b></p>
            {$response['dados']['paciente_nome']}
        </td>
        <td colspan="4" valign="top" >
            <p><b>Data Nasc:</b></p>
            <span id="data-nasc">{$response['dados']['data_nasc']}</span>
        </td>
    </tr>
    <tr>
        <td colspan="4" valign="top" >
            <p>
                <b>Modalidade de Atendimento:</b>
                <span class="options">( {$hospitalar} ) Hospitalar</span>
                <span class="options">( {$ambulatorial} ) Ambulatorial</span> <br>
                <span class="options">( {$domiciliar} ) Domiciliar</span>
                <br>
                
                <b>Isolamento:</b>
                <span class="options">( {$isolamentoSim} ) Sim</span>
                <span class="options">( {$isolamentoNao} ) Não</span>
                
                <b class="options">Tipo:</b>
                {$response['beneficiario']['tipo_isolamento']}
            </p>
        </td>
        <td colspan="4" valign="top" >
            <p>
                <b>Deambula:</b>
                <span class="options">( {$deambulaSim} ) Sim</span>
                <span class="options">( {$deambulaNao} ) Não</span>
            </p>
        </td>
    </tr>

    <tr>
        <td valign="top" >
            <p><b>Idade:</b></p>
            <span id="idade">{$response['dados']['idade']}</span>
        </td>
        <td colspan="2" valign="top" >
            <p><b>Sexo:</b></p>
            <span id="sexo">{$response['dados']['sexo']}</span>
        </td>
        <td valign="top" >
            <p><b>Peso:</b></p>
            {$response['beneficiario']['peso']} KG
        </td>
        <td colspan="2" valign="top" >
            <p><b>Altura:</b></p>
            {$response['beneficiario']['altura']} m
        </td>
        <td colspan="2" valign="top" >
            <p><b>Data de Admissão:</b></p>
            <span id="data-admissao">{$response['dados']['data_admissao']}</span>
        </td>
    </tr>

    <tr>
        <td class="row-periodo" colspan="8" valign="top" align="center" >
            <b>4. Histórico do Paciente</b>
        </td>
    </tr>
    <tr>
        <td colspan="4" valign="middle">
            <p>
                <b>Diagnóstico de Base:</b>
                {$response['historico']['diagnostico_base']}
            </p>
        </td>
        <td colspan="4" valign="top" >
            <p>
                <b>CID:</b>
                {$response['historico']['cid']}
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="8" valign="top" >
            <p>
                <b>Doenças Sistêmicas Associadas:</b>
                <span class="options">( {$hipertensao} ) Hipertensão</span>
                <span class="options">( {$vasculopatia} ) Vasculopatia</span>
                <span class="options">( {$neoplasia} ) Neoplasias</span>
                <span class="options">( {$cardiopatia} ) Cardiopatia</span>
                <span class="options">( {$diabetes} ) Diabetes</span>
                <span class="options">( {$pneumonia} ) Pneumonia</span>
                <span class="options">( {$alergias} ) Alergias</span>
                <span class="options">( {$cirurgica} ) Cirúrgica</span>
                <span class="options" id="doencas-sistemicas">Quais: </span>
                {$response['historico']['outras_doencas_sistemicas']}
                <br>

                <b>Tempo da Ferida:</b>
                {$response['historico']['tempo_ferida']}
                <br>

                <b>Tratamento Atual:</b>
                <span class="options">( {$camara} ) Câmara Hiperbárica</span>
                <span class="options">( {$coberturas} ) Coberturas Especiais</span>
                <br>

                <b>Tipos de Coberturas:</b>
                {$response['historico']['tipos_coberturas']}
                <br>

                <b>Tempo de Uso:</b>
                {$response['historico']['tempo_uso']}
                <br>

                <b>Frequência de Troca:</b>
                {$response['historico']['frequencia_troca']}
                <br>

                <b>Infecção:</b>
                <span class="options">( {$infeccaoSim} ) Sim</span>
                <span class="options">( {$infeccaoNao} ) Não</span>
                <span class="options">( {$microorganismo} ) Microorganismo</span>
                <br>
                
                <b>Descrição:</b>
                {$response['historico']['microorganismo']}
                <br>

                <b>Incontinência:</b>
                <span class="options">( {$urinaria} ) Urinária</span>
                <span class="options">( {$fecal} ) Fecal</span>
            </p>
        </td>
    </tr>

    <tr>
        <td class="row-periodo" colspan="8" valign="top" align="center" >
            <b>5. Avaliação e Evolução da Ferida</b>
        </td>
    </tr>
    <tr>
        <td colspan="8" valign="top" >
            <p>
                <b>Data de Avaliação:</b>
                <span id="data-avaliacao">{$response['beneficiario']['data_avaliacao']}</span>
            </p>
        </td>
    </tr>

    <tr class="have-border">
        <td valign="middle" colspan="3">
            <p>
                <b>Localização:</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['local']} - {$response['locais'][0]['lado']}
        </td>
        <td>
            {$response['locais'][1]['local']} - {$response['locais'][1]['lado']}
        </td>
        <td>
            {$response['locais'][2]['local']} - {$response['locais'][2]['lado']}
        </td>
        <td>
            {$response['locais'][3]['local']} - {$response['locais'][3]['lado']}
        </td>
        <td>
            {$response['locais'][4]['local']} - {$response['locais'][4]['lado']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Etiologia da Lesão:</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['etiologia']}
        </td>
        <td>
            {$response['locais'][1]['etiologia']}
        </td>
        <td>
            {$response['locais'][2]['etiologia']}
        </td>
        <td>
            {$response['locais'][3]['etiologia']}
        </td>
        <td>
            {$response['locais'][4]['etiologia']}
        </td>
    </tr>

    <tr>
        <td class="collumn-description" colspan="8" valign="top" align="center" >
            <b>Características e Leito de Ferida</b>
        </td>
    </tr>

    <tr class="have-border">
        <td valign="middle" colspan="3">
            <p>
                <b>Comprimento (cm)</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['comprimento']}
        </td>
        <td>
            {$response['locais'][1]['comprimento']}
        </td>
        <td>
            {$response['locais'][2]['comprimento']}
        </td>
        <td>
            {$response['locais'][3]['comprimento']}
        </td>
        <td>
            {$response['locais'][4]['comprimento']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="middle" colspan="3">
            <p>
                <b>Largura (cm)</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['largura']}
        </td>
        <td>
            {$response['locais'][1]['largura']}
        </td>
        <td>
            {$response['locais'][2]['largura']}
        </td>
        <td>
            {$response['locais'][3]['largura']}
        </td>
        <td>
            {$response['locais'][4]['largura']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="middle" colspan="3">
            <p>
                <b>Profundidade (cm)</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['profundidade']}
        </td>
        <td>
            {$response['locais'][1]['profundidade']}
        </td>
        <td>
            {$response['locais'][2]['profundidade']}
        </td>
        <td>
            {$response['locais'][3]['profundidade']}
        </td>
        <td>
            {$response['locais'][4]['profundidade']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Descolamento (S/N)</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['descolamento']}
        </td>
        <td>
            {$response['locais'][1]['descolamento']}
        </td>
        <td>
            {$response['locais'][2]['descolamento']}
        </td>
        <td>
            {$response['locais'][3]['descolamento']}
        </td>
        <td>
            {$response['locais'][4]['descolamento']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Tecido de Granulação (S/N)</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['tecido_granulacao']}
        </td>
        <td>
            {$response['locais'][1]['tecido_granulacao']}
        </td>
        <td>
            {$response['locais'][2]['tecido_granulacao']}
        </td>
        <td>
            {$response['locais'][3]['tecido_granulacao']}
        </td>
        <td>
            {$response['locais'][4]['tecido_granulacao']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Tecido de Esfacelo (S/N)</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['tecido_esfacelo']}
        </td>
        <td>
            {$response['locais'][1]['tecido_esfacelo']}
        </td>
        <td>
            {$response['locais'][2]['tecido_esfacelo']}
        </td>
        <td>
            {$response['locais'][3]['tecido_esfacelo']}
        </td>
        <td>
            {$response['locais'][4]['tecido_esfacelo']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Tecido de Fibrina (S/N)</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['tecido_fibrina']}
        </td>
        <td>
            {$response['locais'][1]['tecido_fibrina']}
        </td>
        <td>
            {$response['locais'][2]['tecido_fibrina']}
        </td>
        <td>
            {$response['locais'][3]['tecido_fibrina']}
        </td>
        <td>
            {$response['locais'][4]['tecido_fibrina']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Tecido Necrótico (S/N)</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['tecido_necrotico']}
        </td>
        <td>
            {$response['locais'][1]['tecido_necrotico']}
        </td>
        <td>
            {$response['locais'][2]['tecido_necrotico']}
        </td>
        <td>
            {$response['locais'][3]['tecido_necrotico']}
        </td>
        <td>
            {$response['locais'][4]['tecido_necrotico']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Exposição Tendão/Vaso/Osso (S/N)</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['exposicao']}
        </td>
        <td>
            {$response['locais'][1]['exposicao']}
        </td>
        <td>
            {$response['locais'][2]['exposicao']}
        </td>
        <td>
            {$response['locais'][3]['exposicao']}
        </td>
        <td>
            {$response['locais'][4]['exposicao']}
        </td>
    </tr>

    <tr>
        <td class="collumn-description" colspan="8" valign="top" align="center" >
            <b>Exsudato/Odor</b>
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Tipo de Exsudato</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['tipo']}
        </td>
        <td>
            {$response['locais'][1]['tipo']}
        </td>
        <td>
            {$response['locais'][2]['tipo']}
        </td>
        <td>
            {$response['locais'][3]['tipo']}
        </td>
        <td>
            {$response['locais'][4]['tipo']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Volume do Exsudato</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['volume']}
        </td>
        <td>
            {$response['locais'][1]['volume']}
        </td>
        <td>
            {$response['locais'][2]['volume']}
        </td>
        <td>
            {$response['locais'][3]['volume']}
        </td>
        <td>
            {$response['locais'][4]['volume']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Odor (S/N)</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['odor']}
        </td>
        <td>
            {$response['locais'][1]['odor']}
        </td>
        <td>
            {$response['locais'][2]['odor']}
        </td>
        <td>
            {$response['locais'][3]['odor']}
        </td>
        <td>
            {$response['locais'][4]['odor']}
        </td>
    </tr>

    <tr>
        <td class="collumn-description" colspan="6" valign="top" align="center" >
            <b>Margens da Ferida/Borda</b>
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Cor</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['cor']}
        </td>
        <td>
            {$response['locais'][1]['cor']}
        </td>
        <td>
            {$response['locais'][2]['cor']}
        </td>
        <td>
            {$response['locais'][3]['cor']}
        </td>
        <td>
            {$response['locais'][4]['cor']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Edema (S/N)</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['edema']}
        </td>
        <td>
            {$response['locais'][1]['edema']}
        </td>
        <td>
            {$response['locais'][2]['edema']}
        </td>
        <td>
            {$response['locais'][3]['edema']}
        </td>
        <td>
            {$response['locais'][4]['edema']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Maceração (S/N)</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['maceracao']}
        </td>
        <td>
            {$response['locais'][1]['maceracao']}
        </td>
        <td>
            {$response['locais'][2]['maceracao']}
        </td>
        <td>
            {$response['locais'][3]['maceracao']}
        </td>
        <td>
            {$response['locais'][4]['maceracao']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Bordas (Regular/Irregular)</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['bordas']}
        </td>
        <td>
            {$response['locais'][1]['bordas']}
        </td>
        <td>
            {$response['locais'][2]['bordas']}
        </td>
        <td>
            {$response['locais'][3]['bordas']}
        </td>
        <td>
            {$response['locais'][4]['bordas']}
        </td>
    </tr>

    <tr>
        <td class="row-periodo" colspan="8" valign="top" align="center" >
            <b>6. Plano Terapêutico para utilização da Atual Cobertura Especial</b>
        </td>
    </tr>
    <tr>
        <td colspan="8" valign="top" >
            <p>
                <b>Previsão de início:</b>
            </p>
            {$response['dados']['inicio']}
        </td>
    </tr>

    <tr class="have-border">
        <td colspan="8" valign="top" >
            <p>
                <b>Previsão de término:</b>
            </p>
            {$response['dados']['fim']}
        </td>
    </tr>

    <tr class="have-border">
        <td colspan="8" valign="top" >
            <p>
                <b>Justificativa para solicitação:</b>
                {$response['dados']['justificativa']}
            </p>
        </td>
    </tr>

    <tr>
        <td class="collumn-description" colspan="8" valign="top" align="center" >
            <b>Cobertura Primária</b>
        </td>
    </tr>

    <tr class="have-border">
        <td valign="middle" colspan="3">
            <p>
                <b>Produto</b>
            </p>
        </td>
        <td>
            {$response['cp'][$response['locais'][0]['id']]['produto']}
        </td>
        <td>
            {$response['cp'][$response['locais'][1]['id']]['produto']}
        </td>
        <td>
            {$response['cp'][$response['locais'][2]['id']]['produto']}
        </td>
        <td>
            {$response['cp'][$response['locais'][3]['id']]['produto']}
        </td>
        <td>
            {$response['cp'][$response['locais'][4]['id']]['produto']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Tamanho</b>
            </p>
        </td>
        <td>
            {$response['cp'][$response['locais'][0]['id']]['tamanho']}
        </td>
        <td>
            {$response['cp'][$response['locais'][1]['id']]['tamanho']}
        </td>
        <td>
            {$response['cp'][$response['locais'][2]['id']]['tamanho']}
        </td>
        <td>
            {$response['cp'][$response['locais'][3]['id']]['tamanho']}
        </td>
        <td>
            {$response['cp'][$response['locais'][4]['id']]['tamanho']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Quantidade</b>
            </p>
        </td>
        <td>
            {$response['cp'][$response['locais'][0]['id']]['quantidade']}
        </td>
        <td>
            {$response['cp'][$response['locais'][1]['id']]['tamanho']}
        </td>
        <td>
            {$response['cp'][$response['locais'][2]['id']]['tamanho']}
        </td>
        <td>
            {$response['cp'][$response['locais'][3]['id']]['tamanho']}
        </td>
        <td>
            {$response['cp'][$response['locais'][4]['id']]['tamanho']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Período de troca</b>
            </p>
        </td>
        <td>
            {$response['cp'][$response['locais'][0]['id']]['frequencia']}
        </td>
        <td>
            {$response['cp'][$response['locais'][1]['id']]['frequencia']}
        </td>
        <td>
            {$response['cp'][$response['locais'][2]['id']]['frequencia']}
        </td>
        <td>
            {$response['cp'][$response['locais'][3]['id']]['frequencia']}
        </td>
        <td>
            {$response['cp'][$response['locais'][4]['id']]['frequencia']}
        </td>
    </tr>

    <tr>
        <td class="collumn-description" colspan="8" valign="top" align="center" >
            <b>Cobertura Secundária</b>
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Produto</b>
            </p>
        </td>
        <td>
            {$response['cs'][$response['locais'][0]['id']]['produto']}
        </td>
        <td>
            {$response['cs'][$response['locais'][1]['id']]['produto']}
        </td>
        <td>
            {$response['cs'][$response['locais'][2]['id']]['produto']}
        </td>
        <td>
            {$response['cs'][$response['locais'][3]['id']]['produto']}
        </td>
        <td>
            {$response['cs'][$response['locais'][4]['id']]['produto']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Tamanho</b>
            </p>
        </td>
        <td>
            {$response['cs'][$response['locais'][0]['id']]['tamanho']}
        </td>
        <td>
            {$response['cs'][$response['locais'][1]['id']]['tamanho']}
        </td>
        <td>
            {$response['cs'][$response['locais'][2]['id']]['tamanho']}
        </td>
        <td>
            {$response['cs'][$response['locais'][3]['id']]['tamanho']}
        </td>
        <td>
            {$response['cs'][$response['locais'][4]['id']]['tamanho']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Quantidade</b>
            </p>
        </td>
        <td>
            {$response['cs'][$response['locais'][0]['id']]['quantidade']}
        </td>
        <td>
            {$response['cs'][$response['locais'][1]['id']]['quantidade']}
        </td>
        <td>
            {$response['cs'][$response['locais'][2]['id']]['quantidade']}
        </td>
        <td>
            {$response['cs'][$response['locais'][3]['id']]['quantidade']}
        </td>
        <td>
            {$response['cs'][$response['locais'][4]['id']]['quantidade']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Período de troca</b>
            </p>
        </td>
        <td>
            {$response['cs'][$response['locais'][0]['id']]['frequencia']}
        </td>
        <td>
            {$response['cs'][$response['locais'][1]['id']]['frequencia']}
        </td>
        <td>
            {$response['cs'][$response['locais'][2]['id']]['frequencia']}
        </td>
        <td>
            {$response['cs'][$response['locais'][3]['id']]['frequencia']}
        </td>
        <td>
            {$response['cs'][$response['locais'][4]['id']]['frequencia']}
        </td>
    </tr>

    <tr>
        <td class="collumn-description" colspan="8" valign="top" align="center" >
            <b>Desbridamento</b>
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top" colspan="3">
            <p>
                <b>Instrumental (S/N)</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['instrumental']}
        </td>
        <td>
            {$response['locais'][1]['instrumental']}
        </td>
        <td>
            {$response['locais'][2]['instrumental']}
        </td>
        <td>
            {$response['locais'][3]['instrumental']}
        </td>
        <td>
            {$response['locais'][4]['instrumental']}
        </td>
    </tr>

    <tr class="have-border">
        <td valign="top"  colspan="3">
            <p>
                <b>Cirúrgico (S/N)</b>
            </p>
        </td>
        <td>
            {$response['locais'][0]['cirurgico']}
        </td>
        <td>
            {$response['locais'][1]['cirurgico']}
        </td>
        <td>
            {$response['locais'][2]['cirurgico']}
        </td>
        <td>
            {$response['locais'][3]['cirurgico']}
        </td>
        <td>
            {$response['locais'][4]['cirurgico']}
        </td> 
    </tr>

    <tr>
        <td class="row-periodo" colspan="8" valign="top" align="center" >
            <b>7. Informações Complementares</b>
        </td>
    </tr>

    <tr>
        <td colspan="8" valign="middle">
            {$response['dados']['informacoes_complementares']}
        </td>
    </tr>

    </tbody>
</table>
<table width="100%" border="0">
<tr>
    <td width="50%" align="center">
        {$response['profissionais']['med']} - CREMEB: {$response['profissionais']['cr_med']} <br>
        <img src='{$_SERVER['DOCUMENT_ROOT']}{$response['profissionais']['assinatura_medico']}' width='20%' />
    </td>
    <td width="50%" align="center">
        {$response['profissionais']['enf']} - COREN: {$response['profissionais']['cr_enf']} <br>
        <img src='{$_SERVER['DOCUMENT_ROOT']}{$response['profissionais']['assinatura_enf']}' width='20%' />
    </td>
</tr>
</table>

<div style="page-break-before: always;"></div>

<center><h2>ANEXO - IMAGEM DAS FERIDAS</h2></center>
HTML;

// <pagebreak />

$imagem_ferida = 'SEM IMAGEM';
$numero_ferida = 0;
$imagem_feridas = '';

if($img_feridas[0]['full_path'] != '') {
    foreach ($img_feridas as $row) {
        ++$numero_ferida;

        $local = $row['LOCAL'];
        $descricaoLocalFerida = $row['descricao_local_ferida'];
        $lado = $row['LADO'];
        $tipo = $row['TIPO'];
        $cobertura = $row['COBERTURA'];
        $tamanho = $row['TAMANHO'];
        $profundidade = $row['TAMANHO'];
        $tecido = $row['TECIDO'];
        $grau = $row['GRAU'];
        $humidade = $row['HUMIDADE'];
        $exsudato = $row['EXSUDATO'];
        $contaminacao = $row['CONTAMINACAO'];
        $pele = $row['PELE'];
        $tempo = $row['TEMPO'];
        $odor = $row['ODOR'];
        $periodo = $row['PERIODO'];
        $status_ferida = $row['STATUS_FERIDA'];
        $observacao = $row['observacao'];

        $imagem_ferida = 'SEM IMAGEM';
        if (isset($row['full_path']) && !empty($row['full_path'])) {
            $feridaImagemSrc = App\Helpers\Storage::getImageUrl($row['path']);
            $imagem_referencia = $descricaoLocalFerida;
            $imagem_feridas .= <<<IMAGEM_FERIDA
<p>
    <p style="display: block; font-size: 14px;">{$imagem_referencia}</p>
    <p><img style="display: block;" src="{$feridaImagemSrc}" height="400" /></p>
</p>
<br/>
IMAGEM_FERIDA;
        }
    }
} else {
    if (!isset($img_feridas_prorrogacao[0]) || $img_feridas_prorrogacao[0]['full_path'] != '') {
        foreach ($img_feridas_prorrogacao as $row) {
            $imagem_ferida = 'SEM IMAGEM';
            if (isset($row['full_path']) && !empty($row['full_path'])) {
                $feridaImagemSrc = App\Helpers\Storage::getImageUrl($row['path']);
                $imagem_referencia = $row['LOCAL'];
                $imagem_feridas .= <<<IMAGEM_FERIDA
<p>
    <p style="display: block; font-size: 14px;">{$imagem_referencia}</p>
    <p><img style="display: block;" src="{$feridaImagemSrc}" height="400" /></p>
</p>
<br/>
IMAGEM_FERIDA;
            }
        }
    }
}
//die();

if($imagem_feridas != '') {
    $html .= <<<HTML
{$imagem_feridas}
HTML;
} else {
    $html .= <<<HTML
{$imagem_ferida}
HTML;

}

//echo '<pre>'; print_r($html); die();

$mpdf=new mPDF('pt','A4',9);
$mpdf->SetHeader('página {PAGENO} de {nbpg}');
$ano = date("Y");
$mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
$mpdf->WriteHTML("<html><body>");
$flag = false;

if($flag) $mpdf->WriteHTML("<formfeed>");
$mpdf->WriteHTML($html);
$flag = true;

$mpdf->WriteHTML("</body></html>");
$mpdf->Output('prescricao.pdf','I');
exit;

?>