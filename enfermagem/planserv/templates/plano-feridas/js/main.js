$(function () {
    $("#left").hide();
    var rightWidth = $("#right").css('width');
    $("#right").css('width', '100%');

    $("#mostrar-menu").live('click', function () {
        $("#right").css('width', rightWidth);
        $("table.planserv").width(765);
        $("#left").show();

    });

    $(".chosen-select").chosen({search_contains: true});

    $("#paciente-dados").change(function(){
        var paciente = $(this).val();

        if(paciente != '') {
            $.ajax({
                type: "GET",
                url: "/enfermagem/planserv/?action=dados-paciente",
                data: {paciente: paciente},
                success: function (response) {
                    response = JSON.parse(response);
                    var nascimento = response.nascimento.split('-');
                    var dataNasc = nascimento.reverse().join('/');
                    $("#data-nasc").html(dataNasc);
                    var nascStamp = new Date(nascimento[2], nascimento[1], nascimento[0]);
                    var hoje = new Date();
                    var idade = Math.floor(Math.ceil(Math.abs(nascStamp.getTime() - hoje.getTime()) / (1000 * 3600 * 24)) / 365.25);
                    $("#idade").html(idade);
                    $("#sexo").html(response.sexo == '0' ? 'Masculino' : 'Feminino');
                    var dataAdmissao = response.dataInternacao.split(' ');
                    $("#data-admissao").html(dataAdmissao[0].split('-').reverse().join('/'));
                }
            });

            $.ajax({
                type: "GET",
                url: "/enfermagem/planserv/?action=ultima-evolucao-medica",
                data: {paciente: paciente},
                success: function (response) {
                    response = JSON.parse(response);
                    if(Object.keys(response).length > 0) {
                        var medico = typeof response[0].idMedico != 'undefined' ? response[0].idMedico : '';
                        $("#medico").val(medico).trigger('chosen:updated');
                        $("#evolucao-med").val(response[0].ID);
                        $("#diagnostico-base").val(response[0].IMPRESSAO);
                        $("#cid").val('');
                        var cid = 'Motivo da Internação: ' + response[0].diagnostico + '.';
                        $.each(response, function (index, value) {

                            if(value.CID_ID != null && value.problemaAtivo != null) {
                                if (typeof response[index + 1] != 'undefined') {
                                    cid += value.CID_ID + " - " + value.problemaAtivo + ", ";
                                } else {
                                    cid += value.CID_ID + " - " + value.problemaAtivo + ".";
                                }
                            }
                        });
                        $("#cid").val(cid);
                    } else {
                        $("#medico").val('');
                        $("#cremeb").val('');
                        $("#diagnostico-base").val('');
                        $("#cid").val('');
                    }
                }
            });

            $.ajax({
                type: "GET",
                url: "/enfermagem/planserv/?action=ultima-evolucao-enfermagem",
                data: {paciente: paciente},
                success: function (response) {
                    response = JSON.parse(response);
                    if(Object.keys(response).length > 0 || response) {
                        $("#enfermeiro").val(response.USUARIO_ID).trigger('chosen:updated');
                        var data_evolucao = response.DATA_EVOLUCAO.split('-').reverse().join('/');
                        $("#data-avaliacao").html(data_evolucao);
                    }
                }
            });

            $.ajax({
                type: "GET",
                url: "/enfermagem/planserv/?action=feridas-ultima-evolucao-enfermagem",
                data: {paciente: paciente},
                success: function (response) {
                    response = JSON.parse(response);
                    console.log(Object.keys(response).length, response);
                    if(Object.keys(response).length > 0) {
                        $.each(response, function (index, value) {
                            $("#locais-" + index).val(value.LOCAL);
                            if(value.LADO == '1' || value.LADO == '2') {
                                $("#lado-" + index).show().val(value.LADO).attr('required', 'required');
                            }
                            $(".feridas-" + index).each(function () {
                                $(this).attr('required', 'required');
                            });
                        });
                    } else {
                        $("#dialog-sem-evo-enf").dialog({
                            resizable: false,
                            height: "auto",
                            width: 400,
                            modal: true,
                            buttons: {
                                "Sim": function() {
                                    $(this).dialog("close");
                                },
                                "Não": function() {
                                    window.location.href = '?action=feridas';
                                }
                            }
                        });
                        $("#dialog-sem-evo-enf").dialog( "open" );
                    }
                }
            });
        }
    });

    $(".locais").change(function () {
        var id = $(this).attr('id');
        id = id.split('-')[1];
        if($(this).val() != '') {
            if($("#locais-" + id + " :selected").attr('lado') == 'S') {
                $("#lado-" + id).show().attr('required', 'required');
            } else {
                $("#lado-" + id).hide().removeAttr('required').val('');
            }
            $(".feridas-" + id).each(function () {
                $(this).attr('required', 'required');
            });
        } else {
            $("#lado-" + id).hide().removeAttr('required').val('');
            $(".feridas-" + id).each(function () {
                $(this).removeAttr('required').val('');
            });
        }
    });

    $("#isolamento").change(function () {
        $("#tipo-isolamento").hide();
        if($(this).val() == '1') {
            $("#tipo-isolamento").show();
        } else {
            $("#tipo-isolamento").find('textarea').val('');
        }
    });

    $("#infeccao").change(function () {
        $("#microorganismo-descricao").hide();
        if($(this).val() == '1' || $(this).val() == '3') {
            $("#microorganismo-descricao").show();
        } else {
            $("#microorganismo-descricao").find('textarea').val('');
        }
    });

    $(".cp-buscar-curativo")
        .autocomplete({
            source: function(request, response) {
                $.getJSON('/enfermagem/busca_brasindice.php', {
                    term: request.term,
                    tipo: 0,
                    tipo_extra: 1
                }, response);
            },
            minLength: 3,
            select: function(event, ui) {
                $(this).val(ui.item.value);
                $(this).parent().find(".catalogo-id").val(ui.item.catalogo_id);

                var catalogoIdElem = $(this).parent().find('.catalogo-id');
                var splitId = catalogoIdElem.attr('id').split('-');
                var id = splitId[3];
                var tipo = splitId[2];
                $("." + tipo + "-" + id).each(function () {
                    $(this).attr('required', 'required').removeAttr('readonly');
                });

                return false;
            }
        })
        .keyup(function () {
            if($(this).val() == '') {
                var catalogoIdElem = $(this).parent().find('.catalogo-id');
                catalogoIdElem.val('');
                var splitId = catalogoIdElem.attr('id').split('-');
                var id = splitId[3];
                var tipo = splitId[2];

                $("." + tipo + "-" + id).each(function () {
                    $(this).removeAttr('required').attr('readonly', 'readonly').val('');
                });
            }
        });

    $(".cs-buscar-curativo")
        .autocomplete({
            source: function(request, response) {
                $.getJSON('../busca_brasindice.php', {
                    term: request.term,
                    tipo: 1
                }, response);
            },
            minLength: 3,
            select: function(event, ui) {
                $(this).val(ui.item.value);
                $(this).parent().find(".catalogo-id").val(ui.item.catalogo_id);

                var catalogoIdElem = $(this).parent().find('.catalogo-id');
                var splitId = catalogoIdElem.attr('id').split('-');
                var id = splitId[3];
                var tipo = splitId[2];
                $("." + tipo + "-" + id).each(function () {
                    $(this).attr('required', 'required').removeAttr('readonly');
                });

                return false;
            }
        })
        .keyup(function () {
            if($(this).val() == '') {
                var catalogoIdElem = $(this).parent().find('.catalogo-id');
                catalogoIdElem.val('');
                var splitId = catalogoIdElem.attr('id').split('-');
                var id = splitId[3];
                var tipo = splitId[2];

                $("." + tipo + "-" + id).each(function () {
                    $(this).removeAttr('required').attr('readonly', 'readonly').val('');
                });
            }
        });

    $("#salvar-plano-feridas").click(function () {
        if($(".locais :selected[value != '']").length == 0) {
            alert('A ficha precisa possuir pelo menos uma ferida!');
            return false;
        }
        return true;
    });
});