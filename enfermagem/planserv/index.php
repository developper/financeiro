<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

//ini_set('display_errors', 1);

use \App\Controllers\Planserv\Feridas;
use \App\Controllers\Administracao;
use \App\Controllers\Medico;
use \App\Controllers\Enfermagem;

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
	"GET" => [
		'/enfermagem/planserv/?action=dados-paciente' => '\App\Controllers\Administracao::dadosPaciente',
		'/enfermagem/planserv/?action=ultima-evolucao-medica' => '\App\Controllers\Medico::ultimaEvolucaoPaciente',
		'/enfermagem/planserv/?action=ultima-evolucao-enfermagem' => '\App\Controllers\Enfermagem::ultimaEvolucaoPaciente',
		'/enfermagem/planserv/?action=feridas-ultima-evolucao-enfermagem' => '\App\Controllers\Enfermagem::feridasUltimaEvolucaoPaciente',
		'/enfermagem/planserv/?action=feridas' => '\App\Controllers\Planserv\Feridas::formListarPlanoFeridas',
        '/enfermagem/planserv/?action=feridas-listar' => '\App\Controllers\Planserv\Feridas::formListarPlanoFeridas',
        '/enfermagem/planserv/?action=feridas-novo' => '\App\Controllers\Planserv\Feridas::formNovoPlanoFeridas',
        '/enfermagem/planserv/?action=feridas-salvar' => '\App\Controllers\Planserv\Feridas::formNovoPlanoFeridas',
        '/enfermagem/planserv/?action=feridas-editar' => '\App\Controllers\Planserv\Feridas::formEditarPlanoFeridas',
        '/enfermagem/planserv/?action=feridas-usar-para-nova' => '\App\Controllers\Planserv\Feridas::formEditarPlanoFeridas',
        '/enfermagem/planserv/?action=imprimir-plano-feridas' => '\App\Controllers\Planserv\Feridas::imprimirPlanoFeridas',
	],
	"POST" => [
		'/enfermagem/planserv/?action=feridas-listar' => '\App\Controllers\Planserv\Feridas::buscarPlanoFeridas',
		'/enfermagem/planserv/?action=feridas-salvar' => '\App\Controllers\Planserv\Feridas::salvarPlanoFeridas',
		'/enfermagem/planserv/?action=feridas-atualizar' => '\App\Controllers\Planserv\Feridas::atualizarPlanoFeridas',
    ]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}