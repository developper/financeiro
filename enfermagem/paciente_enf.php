<?php

$id = session_id();
if (empty($id))
    session_start();


include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');
include_once('../vendor/autoload.php');

use \App\Helpers\StringHelper as String;
use \App\Models\Administracao\Paciente as ModelPaciente;

class DadosPaciente {

    public $codigo = "";
    public $nome = "";
    public $nasc = "";
    public $sexo = "";
    public $plano = NULL;
    public $diagnostico = "";
    public $cid = "";
    public $origem = "";
    public $leito = "";
    public $telPosto = "";
    public $solicitante = "";
    public $especialidade = "";
    public $acomp = "";
    public $cnome = "";
    public $csexo = "";
    public $relcuid = "";
    public $rnome = "";
    public $rsexo = "";
    public $relresp = "";
    public $endereco = "";
    public $bairro = "";
    public $referencia = "";
    public $contato = "";
    public $telResponsavel = "";
    public $telCuidador = "";
    public $enderecoInternacao = "";
    public $bairroInternacao = "";
    public $referenciaInternacao = "";
    public $telDomiciliar = "";
    public $telInternacao = "";
    public $especialidadeCuidador = "";
    public $especialidadeResponsavel = "";
    public $empresa = NULL;
    public $id_cidade_und = "";
    public $cidade_und = "";
    public $id_cidade_internado = "";
    public $cidade_internado = "";
    public $id_cidade_domiciliar = "";
    public $cidade_domiciliar = "";
    public $end_und_origem = "";
    public $num_matricula_convenio = "";

}

class DadosFicha {

    public $usuario = "";
    public $data = "";
    public $pacienteid = "";
    public $pacientenome = "";
    public $motivo = "";
    public $alergiamed = "";
    public $alergiaalim = "";
    public $tipoalergiamed = "";
    public $tipoalergiaalim = "";
    public $prelocohosp = "3";
    public $prehigihosp = "2";
    public $preconshosp = "3";
    public $prealimhosp = "4";
    public $preulcerahosp = "2";
    public $prelocodomic = "3";
    public $prehigidomic = "2";
    public $preconsdomic = "3";
    public $prealimdomic = "4";
    public $preulceradomic = "2";
    public $objlocomocao = "3";
    public $objhigiene = "2";
    public $objcons = "3";
    public $objalimento = "4";
    public $objulcera = "2";
    public $shosp = "14";
    public $sdom = "14";
    public $sobj = "14";
    public $modalidade = "2";
    public $local_av = "2";
    public $qtdinternacoes = "";
    public $historico = "";
    public $parecer = "0";
    public $justificativa = "";
    public $cancer = "";
    public $psiquiatrico = "";
    public $neuro = "";
    public $glaucoma = "";
    public $hepatopatia = "";
    public $obesidade = "";
    public $cardiopatia = "";
    public $dm = "";
    public $reumatologica = "";
    public $has = "";
    public $nefropatia = "";
    public $pneumopatia = "";
    public $ativo;
    public $idcapmedica = "";
    public $diag_principal = '';
    public $diag_secundario = '';
    public $atend_24 = '';
    public $atend_24_2 = '';
    public $atend_12 = '';
    public $atend_06 = '';

}

class FichaEvolucao {

    public $usuario = "";
    public $data = "";
    public $pacienteid = "";
    public $pacientenome = "";
    public $pa_sistolica_min = "";
    public $pa_sistolica_max = "";
    public $pa_diastolica_max = "";
    public $pa_diastolica_min = "";
    public $fc_max = "";
    public $fc_min = "";
    public $fr_max = "";
    public $fr_min = "";
    public $temperatura_max = "";
    public $temperatura_min = "";
    public $estd_geral = "";
    public $mucosa = "";
    public $escleras = "";
    public $respiratorio = "";
    public $pa_sistolica = "";
    public $pa_diastolica = "";
    public $fc = "";
    public $fr = "";
    public $temperatura = "";
    public $novo_exame = "";
    public $impressao = "";
    public $plano_diagnostico = "";
    public $planoterapeutico = "";

}

class FichaAvaliacaoEnfermagem {

    public $usuario = "";
    public $data = "";
    public $pacienteid = "";
    public $pacientenome = "";

}

class FichaEvolucaoEnfermagem {

    public $usuario = "";
    public $data = "";
    public $pacienteid = "";
    public $pacientenome = "";

}

class Paciente {

    private $abas = array('0' => 'Repouso', '1' => 'Dieta', '2' => 'Cuidados Especiais', '3' => 'Soros e Eletr&oacute;litos',
        '4' => 'Antibi&oacute;ticos Injet&aacute;veis', '5' => 'Injet&aacute;veis IV', '6' => 'Injet&aacute;veis IM',
        '7' => 'Injet&aacute;veis SC', '8' => 'Drogas inalat&oacute;rias', '9' => 'Nebuliza&ccedil;&atilde;o',
        '10' => 'Drogas Orais/Enterais', '11' => 'Drogas T&oacute;picas', '12' => 'F&oacute;mulas', '13' => 'Oxigenoterapia');
    public $plan;

    private function equipe($paciente, $tipo) {
        $has = false;
        $result = mysql_query("SELECT e.*, u.nome FROM equipePaciente as e, usuarios as u WHERE e.funcionario = u.idUsuarios AND u.tipo = '$tipo' AND paciente = '$paciente' AND fim IS NULL ORDER BY nome");
        $cor = false;
        while ($row = mysql_fetch_array($result)) {
            $has = true;
            foreach ($row AS $key => $value) {
                $row[$key] = stripslashes($value);
            }
            if ($cor)
                echo "<tr cod='{$row['id']}' >";
            else
                echo "<tr cod='{$row['id']}' class='odd' >";
            $cor = !$cor;
            echo "<td>{$row['nome']}</td>";
            $inicio = implode("/", array_reverse(explode("-", $row['inicio'])));
            echo "<td>{$inicio}</td>";
            echo "<td>-</td>";
            echo "<td><button class='end' >Finalizar</button></td><td><button class='del' >Remover</button></td>";
            echo "</tr>";
        }
        if (!$has)
            echo "<tr><td colspan='5' ><center><b>Nenhuma equipe está acompanhando o paciente.</b></center></td></tr>";
    }

    private function empresas($id, $extra = NULL) {
        $result = mysql_query("SELECT * FROM empresas AND id NOT IN (1, 3, 9, 10) ORDER BY nome");
        $var = "<select name='empresa' style='background-color:transparent;' class='COMBO_OBG' {$extra}>";
        $var .= "<option value='-1'></option>";
        while ($row = mysql_fetch_array($result)) {
            if (($id <> NULL) && ($id == $row['id']))
                $var .= "<option value='{$row['id']}' selected>{$row['nome']}</option>";
            else
                $var .= "<option value='{$row['id']}'>{$row['nome']}</option>";
        }
        $var .= "</select>";
        return $var;
    }

    public function estadoGeral($id,$editar = null) {
        if ($editar == 1) {
            $acesso = "readonly"; //readonly
            $enabled = "disabled";
        }

        $sql = "SELECT
                    evolucaoenfermagem.*,
                    DATE_FORMAT(evolucaoenfermagem.DATA_EVOLUCAO,'%d/%m/%Y') as inicio
                from
                    evolucaoenfermagem
                where id = {$id}";
        //echo $sql;
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {

            $paciente = $row['PACIENTE_ID'];
            $id_aval = $row['ID'];
            $bom_ruim = $row['BOM_RUIM'];
            $data_evolucao = $row['inicio'];
            $nutrido_desnutrido = $row['NUTRIDO_DESNUTRIDO'];
            $higi_corp_satis = $row['HIGI_CORP_SATIS'];
            $higi_bucal_satis = $row['HIGI_BUCAL_SATIS'];
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan =4> <b>Estado Geral</b></th></thead>";
        echo"<tr><td style='width:25%;'><input type='radio' name='bom_ruim' value='1' " . (($bom_ruim == '1') ? "checked" : "") . " {$enabled} class='bom_ruim OBG_RADIO'>Bom</td>
        <td style='width:25%;'><input type='radio' name='nutrido_desnutrido' value='1' " . (($nutrido_desnutrido == '1') ? "checked" : "") . " {$enabled} class='nutrido_desnutrido OBG_RADIO'>Nutrido</td>
        <td style='width:25%;'> <input type='radio' value='1' name='higi_corp_satis' " . (($higi_corp_satis == '1') ? "checked" : "") . " {$enabled} class='higi_corp_satis OBG_RADIO'>Higiene corporal " . htmlentities("satisfatória") . "</td>
        <td style='width:25%;'><input type='radio' value='1' name='higi_bucal_satis' " . (($higi_bucal_satis == '1') ? "checked" : "") . " {$enabled} class='higi_bucal_satis OBG_RADIO'>Higiene bucal " . htmlentities("satisfatória") . "</td>
        </tr>";
        echo " <tr><td style='width:25%;'><input type='radio' name='bom_ruim' value='1' " . (($bom_ruim == '2') ? "checked" : "") . " {$enabled} class='bom_ruim OBG_RADIO'>Ruim</td>
        <td style='width:25%;'><input type='radio' name='nutrido_desnutrido' value='1' " . (($nutrido_desnutrido == '2') ? "checked" : "") . " {$enabled} class='nutrido_desnutrido OBG_RADIO'> Desnutrido</td>
        <td style='width:25%;'><input type='radio' name='higi_corp_satis' value='1' " . (($higi_corp_satis == '2') ? "checked" : "") . " {$enabled} class='higi_corp_satis OBG_RADIO'>Higiene corporal " . htmlentities("insatisfatória") . "</td>
        <td style='width:25%;'><input type='radio' name='higi_bucal_satis' value='1' " . (($higi_bucal_satis == '2') ? "checked" : "") . " {$enabled} class='higi_bucal_satis OBG_RADIO'>Higiene bucal " . htmlentities("insatisfatória") . "</td></tr>";
        echo "</tr>";
        echo "</table>";
    }

    public function estadoEmocional($id,$editar = null){
        if ($editar == 1) {
            $acesso = "readonly"; //readonly
            $enabled = "disabled";
        }

        $sql = "SELECT
                    evolucaoenfermagem.*
                from
                    evolucaoenfermagem
                where id = {$id}";
        //echo $sql;
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $estado_emocional = $row['ESTADO_EMOCIONAL'];
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan =4> <b>Estado Emocional</b></th></tr></thead>";
        echo "<tr><td style='width:25%;'><input type='radio' name='estado_emocional' value='1' " . (($estado_emocional == '1') ? "checked" : "") . " {$enabled} class='estado_emocional OBG_RADIO'>Calmo </td>
                <td style='width:25%;'><input type='radio' name='estado_emocional' value='2' " . (($estado_emocional == '2') ? "checked" : "") . " {$enabled} class='estado_emocional OBG_RADIO'>Agitado</td>
                <td style='width:25%;'><input type='radio' name='estado_emocional' value='3' " . (($estado_emocional == '3') ? "checked" : "") . " {$enabled} class='estado_emocional OBG_RADIO'>Deprimido</td>
                <td style='width:25%;'><input type='radio' name='estado_emocional' value='4'  " . (($estado_emocional == '4') ? "checked" : "") . " {$enabled} class='estado_emocional OBG_RADIO'>Choroso</td></tr>";
        echo "</table>";
    }

    public function nivelConsciencia($id,$editar = null){
        if ($editar == 1) {
            $acesso = "readonly"; //readonly
            $enabled = "disabled";
        }

        $sql = "SELECT
                    evolucaoenfermagem.*
                from
                    evolucaoenfermagem
                where id = {$id}";
        //echo $sql;
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $consciente = $row['CONSCIENTE'];
            $sedado = $row['SEDADO'];
            $obnubilado = $row['OBNUBILADO'];
            $torporoso = $row['TORPOROSO'];
            $comatoso = $row['COMATOSO'];
            $orientado = $row['ORIENTADO'];
            $desorientado = $row['DESORIENTADO'];
            $sonolento = $row['SONOLENTO'];
            $vigil = $row['vigil'];
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan =4> <b>N&iacute;vel de Consci&ecirc;ncia</b></th></thead>";
        echo"<tr><td style='width:25%;'><input type='checkbox' name='consciente' value='1' " . (($consciente) ? "checked" : "") . " {$enabled} class='nivel_consciencia OBG_RADIO'>Consciente</td>
                <td style='width:25%;'><input type='checkbox' name='sedado' value='1' " . (($sedado) ? "checked" : "") . " {$enabled} class='nivel_consciencia OBG_RADIO'>Sedado</td>
                <td style='width:25%;'><input type='checkbox' name='obnubilado' value='1' " . (($obnubilado) ? "checked" : "") . " {$enabled} class='nivel_consciencia OBG_RADIO'>Obnubilado</td>
                <td style='width:25%;'><input type='checkbox' name='torporoso' value='1' " . (($torporoso) ? "checked" : "") . " {$enabled} class='nivel_consciencia OBG_RADIO'>Torporoso</td>
                </tr>";

        echo"<tr><td style='width:25%;'><input type='checkbox' name='comatoso' value='1' " . (($comatoso) ? "checked" : "") . " {$enabled} >Comatoso</td>
                <td style='width:25%;'><input type='checkbox' name='orientado' value='1' " . (($orientado) ? "checked" : "") . " {$enabled} >Orientado</td>
                <td style='width:25%;'><input type='checkbox' name='desorientado' value='1' " . (($desorientado) ? "checked" : "") . " {$enabled} >Desorientado</td>
                <td style='width:25%;'><input type='checkbox' name='sonolento' value='1' " . (($sonolento) ? "checked" : "") . " {$enabled} >Sonolento</td></tr>";
        echo"<tr>
                  <td style='width:25%;' coslpan='3'>
                     <input type='checkbox' name='vigil' value='1' " . (($vigil) ? "checked" : "") . " {$enabled}
                     class='nivel_consciencia OBG_RADIO'>
                      Vigil
                 </td>
             </tr>";
        echo "</table>";
    }

    public function couroCabeludo($id,$editar = null){
        if ($editar == 1) {
            $acesso = "readonly"; //readonly
            $enabled = "disabled";
        }

        $sql = "SELECT
                    evolucaoenfermagem.*
                from
                    evolucaoenfermagem
                where id = {$id}";
        //echo $sql;
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $integro = $row['INTEGRO'];
            $cicatriz = $row['CICATRIZ'];
            $local_cicatriz = $row['LOCAL_CICATRIZ'];
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan ='4'> <b>Couro Cabeludo</b></th></thead>";
        echo "<tr><td style='width:25%;'> " . htmlentities("Íntegro") . "</td><td colspan ='3'> <input type = 'radio' value='1' " . (($integro == '1') ? "checked" : "") . " {$enabled} name= 'integro' class='integro OBG_RADIO'>Sim <input type = 'radio' value='2' " . (($integro == '2') ? "checked" : "") . " {$enabled} name= 'integro' class='integro OBG_RADIO'> " . htmlentities("Não") . " </tr></td>";
        echo "<tr><td > Cicatriz </td><td><input type = 'radio' value='1' " . (($cicatriz == '1') ? "checked" : "") . " {$enabled} name= 'cicatriz' class='cicatriz OBG_RADIO'>Sim <input type = 'radio' value='2' " . (($cicatriz == '2') ? "checked" : "") . " {$enabled} name= 'cicatriz' class='cicatriz OBG_RADIO'> " . htmlentities("Não") . " <td colspan=2 style='display:none'>Local: <input type='text' name='local_cicatriz' size='30' value='{$local_cicatriz}' {$acesso} ></tr></td>";
        echo "</table>";
    }

    public function alergias($id,$editar = null){
        if ($editar == 1) {
            $acesso = "readonly"; //readonly
            $enabled = "disabled";
        }

        $sql = "select
                    evolucaoenfermagem.*,
                    DATE_FORMAT(evolucaoenfermagem.DATA_EVOLUCAO,'%d/%m/%Y') as inicio
                from
                    evolucaoenfermagem
                where id = {$id}";
        //echo $sql;
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $palergiamed = $row['PALERGIAMED'];
            $palergiaalim = $row['PALERGIAALIM'];
            $alergiamed = $row['TIPOALERGIAMED'];
            $alergiaalim = $row['TIPOALERGIAALIM'];
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo"<thead><tr><th colspan='3'>Alergia medicamentosa (S/N) <input type='text' class='boleano alergia OBG' name='palergiamed' value='{$palergiamed}' {$acesso} /></th>";
        echo "<th >Alergia alimentar (S/N) <input size='50' type='text' class='boleano alergia OBG' name='palergiaalim' value='{$palergiaalim}' {$acesso} /></th>";
        echo "</tr></thead>";
        echo "<tr><td colspan='3'><input type='text'  name='alergiamed' {$acesso} disabled size='50' value='{$alergiamed}' /></td>";
        echo "<td><input type='text' name='alergiaalim' size='50' {$acesso} disabled value='{$alergiaalim}'/></td></tr>";
        //}
        echo "</table>";
    }

    public function menu() {
        echo "<a href='?enfermagem=paciente&act=listar'>Listar</a><br>";
        echo "<a href='?enfermagem=paciente&act=novo'>" . htmlentities("Solicitação de Avaliação") . "</a><br>";
    }

    private function preencheDados($id) {
        $id = anti_injection($id, "numerico");
        $result = mysql_query("SELECT c.*, p.id as plano, cid.codigo as codcid, cid.descricao as descid, c.CIDADE_ID_UND, c.CIDADE_ID_INT, c.CIDADE_ID_DOM,concat(cd.NOME,',',cd.UF) as cidade_und,
				(select  concat(x.NOME,',',x.UF) from cidades as x where x.id=c.CIDADE_ID_INT) as cidade_int,
				(select  concat(x1.NOME,',',x1.UF) from cidades as x1 where x1.id=c.CIDADE_ID_DOM) as cidade_dom
				FROM clientes as c
				LEFT OUTER JOIN planosdesaude as p ON c.convenio = p.id
				LEFT OUTER JOIN cid10 as cid ON c.diagnostico collate utf8_general_ci = cid.codigo collate utf8_general_ci
				LEFT JOIN cidades cd ON c.CIDADE_ID_UND = cd.ID

				WHERE idClientes = '$id'");
        $dados = new DadosPaciente();
        //echo $sql . " " . mysql_error();
        while ($row = mysql_fetch_array($result)) {
            $dados->codigo = $row['codigo'];
            $dados->dataInternacao = $row['dataInternacao'];
            $dados->nome = $row['nome'];
            $dados->sexo = $row['sexo'];
            $dados->plano = $row['plano'];
            $dados->diagnostico = $row['descid'];
            $dados->cid = $row['codcid'];
            $dados->origem = $row['localInternacao'];
            $dados->leito = $row['LEITO'];
            $dados->solicitante = $row['medicoSolicitante'];
            $dados->especialidade = $row['espSolicitante'];
            $dados->acomp = $row['acompSolicitante'];
            $dados->cnome = $row['cuidador'];
            $dados->csexo = $row['csexo'];
            $dados->relcuid = $row['relcuid'];
            $dados->rnome = $row['responsavel'];
            $dados->rsexo = $row['rsexo'];
            $dados->relresp = $row['relresp'];
            $dados->endereco = $row['endereco'];
            $dados->bairro = $row['bairro'];
            $dados->referencia = $row['referencia'];
            $dados->telPosto = $row['TEL_POSTO'];
            $dados->contato = $row['PES_CONTATO_POSTO'];
            $dados->empresa = $row['empresa'];
            $dados->telResponsavel = $row['TEL_RESPONSAVEL'];
            $dados->telCuidador = $row['TEL_CUIDADOR'];
            $dados->enderecoInternacao = $row['END_INTERNADO'];
            $dados->bairroInternacao = $row['BAI_INTERNADO'];
            $dados->referenciaInternacao = $row['REF_ENDERECO_INTERNADO'];
            $dados->telInternacao = $row['TEL_LOCAL_INTERNADO'];
            $dados->telDomiciliar = $row['TEL_DOMICILIAR'];
            $dados->especialidadeCuidador = $row['CUIDADOR_ESPECIALIDADE'];
            $dados->especialidadeResponsavel = $row['RESPONSAVEL_ESPECIALIDADE'];
            $dados->id_cidade_und = $row['CIDADE_ID_UND'];
            $dados->cidade_und = $row['cidade_und'];
            $dados->id_cidade_internado = $row['CIDADE_ID_INT'];
            $dados->cidade_internado = $row['cidade_int'];
            $dados->id_cidade_domiciliar = $row['CIDADE_ID_DOM'];
            $dados->cidade_domiciliar = $row['cidade_dom'];
            $dados->end_und_origem = $row['END_UND_ORIGEM'];
            $dados->num_matricula_convenio = $row['NUM_MATRICULA_CONVENIO'];
            $dados->nasc = implode("/", array_reverse(explode("-", $row['nascimento'])));
            $dados->cpfCliente =$row['cpf'];
            $dados->cpfRresponsavel =$row['cpf_responsavel'];
            $dados->nascResponsavel = implode("/",array_reverse(explode("-",$row['nascimento_responsavel'])));
            $dados->nascCuidador = implode("/",array_reverse(explode("-",$row['nascimento_cuidador'])));
        }
        return $dados;
    }


    public function PegarData($data = null) {
        if ($data == null) {
            $data = date('d/m/Y');
        } else {
            $quebrar = explode('-', $data);

            //print_r(explode('-',$data));

            if (strlen($quebrar[0]) == 4) {
                $data = substr($data, 8, 2) . '/' . substr($data, 5, 2) . '/' . substr($data, 0, 4);
            } else {
                $data = substr($data, 0, 2) . '/' . substr($data, 3, 2) . '/' . substr($data, 6, 4);
            }
        }
        return $data;
    }

    public function PegarHora($data = null) {
        if ($data == null) {
            $hora = date('H:i:s');
        } else {
            $hora = substr($data, 11, 2) . ':' . substr($data, 14, 2) . ':' . substr($data, 17, 2);
        }
        return $hora;
    }

    //Fun��o criada para avisar quando o paciente n�o possui avalaia��o feita pelo m�dico no mod de enfermagem
    public function aviso_avaliacao() {
        echo "<div id='dialog-aviso-avaliacao' title='Aviso'>";
        echo "Esse paciente n&atilde;o possui ficha de avalia&ccedil;&atilde;o";
        echo "</div>";
    }

    public function aviso_evolucao() {
        echo "<div id='dialog-aviso-evolucao' title='Aviso'>";
        echo "Esse paciente n&atilde;o possui ficha de evolu&ccedil;&atilde;o";

        echo "</div>";
    }

    public function listar() {
        require 'templates/paciente_listar.phtml';
    }



    public function detalhe_prescricao($id, $idpac) {
        $this->cabecalho($idpac);
        echo "<center><h1>Detalhes Prescri&ccedil;&otilde;es</h1></center>";

        $sql = "SELECT 
                  prescricoes.empresa AS empresa_presc, 
                  clientes.empresa AS empresa_paciente 
                FROM 
                  prescricoes 
                  INNER JOIN clientes ON prescricoes.paciente = clientes.idClientes 
                  WHERE 
                    id = '{$id}' 
                    AND paciente = '{$idpac}'";
        $rs = mysql_query($sql);
        $row = current(mysql_fetch_array($rs));

        echo "<div>
                <button caminho='imprimir_prescricao.php?id={$id}&idpac={$idpac}' empresa-relatorio='" . (!empty($row['empresa_presc']) ? $row['empresa_presc'] : $row['empresa_paciente']) . "' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  role='button' aria-disabled='false'>
                    <span class='ui-button-text'>
                        Imprimir
                    </span>
                </button>
              </div>";

        echo "<table width=100% class='mytable'><thead><tr>";
        echo "<th><b>Itens prescritos</b></th>";
        echo "</tr></thead>";
        $sql2 = "select id from prescricoes where  ID_CAPMEDICA='{$id}'";
        $x = mysql_query($sql2);
        $row2 = mysql_fetch_array($x);
        $id2 = $row2['id'];


        $sql = "SELECT v.via as nvia, f.frequencia as nfrequencia,i.*,  i.inicio,
		i.fim, DATE_FORMAT(i.aprazamento,'%H:%i') as apraz, u.unidade as um
		FROM itens_prescricao as i LEFT OUTER JOIN frequencia as f ON (i.frequencia = f.id)
		LEFT OUTER JOIN viaadm as v ON (i.via = v.id)
		LEFT OUTER JOIN umedidas as u ON (i.umdose = u.id)
		WHERE idPrescricao = '$id2'
		ORDER BY tipo,inicio,aprazamento,nome,apresentacao ASC";
        $result = mysql_query($sql);
        $n = 0;
        while ($row = mysql_fetch_array($result)) {
            if ($n++ % 2 == 0)
                $cor = '#E9F4F8';
            else
                $cor = '#FFFFFF';

            $tipo = $row['tipo'];
            $i = implode("/", array_reverse(explode("-", $row['inicio'])));
            $f = implode("/", array_reverse(explode("-", $row['fim'])));
            $aprazamento = "";
            if ($row['tipo'] != "0")
                $aprazamento = $row['apraz'];
            $linha = "";
            if ($row['tipo'] != "-1") {
                $aba = $this->abas[$tipo];
                $dose = "";
                if ($row['dose'] != 0)
                    $dose = $row['dose'] . " " . $row['um'];
                $linha = "<b>$aba:</b> {$row['nome']} {$row['apresentacao']} {$row['nvia']} {$dose} {$row['nfrequencia']} Per&iacute;odo: de $i até $f OBS: {$row['obs']}";
            } else
                $linha = $row['descricao'];
            echo "<tr bgcolor={$cor} >";
            echo "<td>$linha</td>";
            echo "</tr>";
        }
        echo "</table>";

        echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
    }

 public function cabecalho($dados, $empresa = null, $convenio = null) {
        if (isset($dados->pacienteid))
            $id = $dados->pacienteid;
        else
            $id = $dados;

        $condp1 = "c.idClientes = '{$id}'";
        $sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS nsexo,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as nempresa,
		c.`nascimento`,
		p.nome as Convenio,
		p.id,
		c.*,
		(CASE c.acompSolicitante
		WHEN 's' THEN 'Sim'
		WHEN 'n' THEN 'N&atilde;o'
		END) AS acomp,
		NUM_MATRICULA_CONVENIO,
		cpf
		
		FROM
		clientes AS c LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN
		planosdesaude as p ON (p.id = c.convenio)
		WHERE
		{$condp1}
		ORDER BY
		c.nome DESC LIMIT 1";

        $result = mysql_query($sql);
        $result2 = mysql_query($sql2);
        echo "<table width=100% style='border:2px dashed;'>";

        while ($pessoa = mysql_fetch_array($result2)) {
            foreach ($pessoa AS $chave => $valor) {
                $pessoa[$chave] = stripslashes($valor);
            }

            $pessoa['nempresa'] = !empty($empresa) ? $empresa : $pessoa['nempresa'];
            $pessoa['Convenio'] = !empty($convenio) ? $convenio : $pessoa['Convenio'];

            echo "<br/><tr>";
            echo "<td colspan='2'><label style='font-size:14px;color:#8B0000'><b><center> INFORMA&Ccedil;&Otilde;ES DO PACIENTE </center></b></label></td>";
            echo "</tr>";

            echo "<tr style='background-color:#EEEEEE;'>";
            echo "<td width='70%'><label><b>PACIENTE </b></label></td>";
            echo "<td><label><b>SEXO </b></label></td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>" . String::removeAcentosViaEReg($pessoa['paciente']) . "</td>";
            echo "<td>{$pessoa['nsexo']}</td>";
            echo "</tr>";

            echo "<tr style='background-color:#EEEEEE;'>";
            echo "<td width='70%'><label><b>IDADE </b></label></td>";
            //Atualiza��o Eriton 19-03-2013
            echo "<td><label><b>UNIDADE REGIONAL:</b></label></td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>" . join("/", array_reverse(explode("-", $pessoa['nascimento']))) . ' (' . $pessoa['idade'] . " anos)</td>";
            echo "<td>{$pessoa['nempresa']}</td>";
            echo "</tr>";

            echo "<tr style='background-color:#EEEEEE;'>";
            echo "<td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>";
            echo "<td><label><b>MATR&Iacute;CULA </b></label></td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>" . strtoupper($pessoa['Convenio']) . " <input type='hidden' id='plano-id' name='plano-id' value='{$pessoa['convenio']}'></td>";
            echo "<td>" . strtoupper($pessoa['NUM_MATRICULA_CONVENIO']) . "</td>";
            echo "</tr>";
            echo " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
            $this->plan = $pessoa['id'];
        }
        echo "</table><br/>";
    }


    public function score_petrobras($dados = null,$id_aval,$v = null,$tipo) {
        echo "<input type='hidden' name='tipoScore' value=" . $tipo . " ></input>";

        if ($v == 1) {
                $acesso = "readonly";
                $enabled = "disabled";
                $mostrar = "style='display:none;'";
            }

        echo "<thead><tr>";

        echo "<th colspan='2' >Tabela de Avalia&ccedil;&atilde;o de Complexidade Petrobras.</th><th></th></tr></thead>";

        if($id_aval != ''){
            $sql2 = "SELECT
                sc_pac.OBSERVACAO,
                sc_pac.ID_PACIENTE,
                sc_pac.ID_ITEM_SCORE,
                sc_item.DESCRICAO,
                sc_item.PESO,
                sc_item.GRUPO_ID ,
            sc_item.ID ,
            sc.id as tbplano,
            av_enf.prejustificativa,
            DATE(sc_pac.DATA) as DATA,
            grup.DESCRICAO as grdesc
                FROM score_paciente sc_pac
            LEFT OUTER JOIN score_item sc_item ON sc_pac.ID_ITEM_SCORE = sc_item.ID
            LEFT OUTER JOIN avaliacaoenfermagem as av_enf ON sc_pac.ID_AVALIACAO_ENFERMAGEM = av_enf.ID
            LEFT OUTER JOIN grupos grup ON sc_item.GRUPO_ID = grup.ID
            LEFT OUTER JOIN score sc ON sc_item.SCORE_ID = sc.ID
                WHERE sc_pac.ID_AVALIACAO_ENFERMAGEM = '{$id_aval}'";
            $result2 = mysql_query($sql2);
            $elemento = '';
            //echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
            while ($row = mysql_fetch_array($result2)) {

                $elemento[$row['ID_ITEM_SCORE']] = array("id_elemento" => $row['ID_ITEM_SCORE'], "peso" => $row['PESO']);
            }
        }
        $sql = "SELECT
                    sc_item.*,
                    gru.DESCRICAO as dsc,
                    gru.QTD
                FROM
                    score_item as sc_item LEFT JOIN
                    grupos as gru ON (sc_item.GRUPO_ID = gru.ID)
                WHERE
                    sc_item.SCORE_ID = 1
                ORDER BY
                    sc_item.GRUPO_ID";
        $result = mysql_query($sql);
        $cont = 0;

        $i = 0;
        $cont1 = 1;
        echo "<tr bgcolor='grey'><td><b>" . htmlentities("Descrição") . "</b></td><td colspan='2'><b>Itens da " . htmlentities("Avaliação") . "</b></td></tr>";
        while ($row = mysql_fetch_array($result)) {
            if (is_array($elemento)) {

                if (array_key_exists($row['ID'], $elemento)) {
                    $check = "checked='checked'";
                    $dados->spetro += $elemento[$row['ID']]['peso'];
                } else {
                    $check = '';
                }
            } else {
                $check = '';
            }

            if ($cont1 == 1) {
                $i = $row['QTD'] + 1;
                $cont = $cont1;
            } else {
                $i = 0;
            }
            if ($row['GRUPO_ID'] >= 2 && $row['GRUPO_ID'] <= 11) {
                if ($cont1 == 1) {
                    if ($x == "#E9F4F8")
                        $x = '';
                    else
                        $x = "#E9F4F8";
                    echo "<tr bgcolor='{$x}'><td ROWSPAN='{$i}'>{$row['dsc']}</td></tr>";
                }
                echo "<tr bgcolor='{$x}'><td colspan='2'>
                    <a class='limpar2' title='Desmarcar' $mostrar><img src='../utils/desmarcar16x16.png' width='16' title='Desmarcar' border='0'></a>
                    <input type='radio'  {$check} class='petro' $enabled name=" . $row['dsc'] . " value=" . $row['PESO'] . " cod_item=" . $row['ID'] . "> {$row['DESCRICAO']} - <b>Peso: {$row['PESO']} </b>";
                echo "</td></tr>";
            }

            $cont1++;

            if ($cont1 > $row['QTD'])
                $cont1 = 1;
        }

        $score = "<input type='text' readonly size='2' name='spetro' value='{$dados->spetro}' {$acesso} />";
        echo"<tr bgcolor='grey'><td colspan='3' aling='center'><b>TOTAL DO SCORE SEGUNDO TABELA PETROBRAS: {$score}</b></td></tr>";
    }

    public function score_abmid($dados = null,$id_aval,$v = null,$tipo) {
        echo "<input type='hidden' name='tipoScore' value=" . $tipo . " ></input>";

        if ($v == 1) {
                $acesso = "readonly";
                $enabled = "disabled";
                $mostrar = "style='display:none;'";
            }

        echo "<thead><tr>";

        $cont = "<input type='text' readonly size='2' name='sabmid1' value='{$dados->sabmid1}' {$acesso} />";
        $tex = "<input type='text'  name='sabmid2' value='{$dados->sabmid2}' {$acesso} />";
        $texto = utf8_decode($tex);


        echo "<th colspan='3'>Tabela de Avalia&ccedil;&atilde;o de Complexidade ABEMID.</th></tr></thead>";

        echo "</td></tr>";
        if($id_aval != ''){
            $sql2 = "SELECT
                sc_pac.OBSERVACAO,
                sc_pac.ID_PACIENTE,
                sc_pac.ID_ITEM_SCORE,
                sc_item.DESCRICAO,
                sc_item.PESO,
                sc_item.GRUPO_ID ,
            sc_item.ID ,
            sc.id as tbplano,
            av_enf.prejustificativa,
            DATE(sc_pac.DATA) as DATA,
            grup.DESCRICAO as grdesc
                FROM score_paciente sc_pac
            LEFT OUTER JOIN score_item sc_item ON sc_pac.ID_ITEM_SCORE = sc_item.ID
            LEFT OUTER JOIN avaliacaoenfermagem as av_enf ON sc_pac.ID_AVALIACAO_ENFERMAGEM = av_enf.ID
            LEFT OUTER JOIN grupos grup ON sc_item.GRUPO_ID = grup.ID
            LEFT OUTER JOIN score sc ON sc_item.SCORE_ID = sc.ID
                WHERE sc_pac.ID_AVALIACAO_ENFERMAGEM = '{$id_aval}'";
        $result2 = mysql_query($sql2);
        $elemento = '';
        //echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
        while ($row = mysql_fetch_array($result2)) {

                $elemento[$row['ID_ITEM_SCORE']] = array("id_elemento" => $row['ID_ITEM_SCORE'], "peso" => $row['PESO']);
            }
        }

        $sql = "SELECT
                    sc_item.*,
                    gru.DESCRICAO as dsc,
                    gru.QTD
                FROM
                    score_item as sc_item LEFT JOIN
                    grupos as gru ON (sc_item.GRUPO_ID = gru.ID)
                WHERE
                    sc_item.SCORE_ID = 2
                ORDER BY
                    sc_item.GRUPO_ID";

        $result = mysql_query($sql);
        $cont = 0;

        $i = 0;
        $cont1 = 1;
        echo"<tr bgcolor='grey'><td><b>Descri&ccedil;&atilde;o</b></td><td colspan='2'><b>Itens da Avalia&ccedil;&atilde;o</b></td></tr>";
        while ($row = mysql_fetch_array($result)) {
            if (is_array($elemento)) {
                if (array_key_exists($row['ID'], $elemento)) {
                    $check = "checked='checked'";
                    $dados->sabmid += $elemento[$row['ID']]['peso'];
                } else {
                    $check = '';
                }
            } else {
                $check = '';
            }

            if ($cont1 == 1) {
                $i = $row['QTD'] + 1;
                $cont = $cont1;
            } else {
                $i = 0;
            }



            if ($row['GRUPO_ID'] == 1) {
                if ($cont1 == 1) {
                    if ($x == "#E9F4F8")
                        $x = '';
                    else
                        $x = "#E9F4F8";

                    echo "<tr bgcolor='{$x}'><td ROWSPAN='{$i}'>{$row['dsc']}</td></tr>";
                }
                echo "<tr bgcolor='{$x}'><td colspan='2'><input type='checkbox' class='abmid' {$enabled} name=" . $row['GRUPO_ID'] . " cod_item=" . $row['ID'] . " {$check} value=" . $row['PESO'] . "  > {$row['DESCRICAO']} - <b>Peso: {$row['PESO']} </b>";
                echo "</td></tr>";
            }


            if ($row['GRUPO_ID'] >= 2 && $row['GRUPO_ID'] <= 11) {
                if ($cont1 == 1) {
                    if ($x == "#E9F4F8")
                        $x = '';
                    else
                        $x = "#E9F4F8";
                    echo "<tr bgcolor='{$x}'><td ROWSPAN='{$i}'>{$row['dsc']}</td></tr>";
                }
                echo "<tr bgcolor='{$x}'><td colspan='2'><a class='limpar' title='Desmarcar' $mostrar><img src='../utils/desmarcar16x16.png' width='16' title='Desmarcar' border='0'></a><input type='radio'  {$check} class='abmid' {$enabled} name=" . $row['dsc'] . " value=" . $row['PESO'] . " cod_item=" . $row['ID'] . "> {$row['DESCRICAO']} - <b>Peso: {$row['PESO']} </b>";
                echo "</td></tr>";
            }

            $cont1++;

            if ($cont1 > $row['QTD'])
                $cont1 = 1;
        }



        $score = "<input type='text' readonly size='2' name='sabmid' value='{$dados->sabmid}' {$acesso} />";
        echo"<tr bgcolor='grey'><td colspan='3' aling='center'><b>TOTAL DO SCORE SEGUNDO TABELA ABEMID: {$score}</b></td></tr>";
    }

 private function periodo() {

        echo "<br/><fieldset style='width:340px;text-align:center;'><legend><b>Per&iacute;odo</b></legend>";
        echo "DE" .
        "<input type='text' name='inicio' value='' maxlength='10' size='10'  class='OBG data periodo' />" .
        " AT&Eacute; <input type='text' name='fim' value='' maxlength='10' size='10'  class='OBG data' /></fieldset>";
    }

    public function listar_capitacao_medica($id) {
        $id = anti_injection($id, "numerico");
        $sql3 = "select Max(id) as cap from capmedica where paciente = '{$id}' ";
        $r2 = mysql_query($sql3);
        $maior_cap = mysql_result($r2, 0);


        $sql3 = "select DATE_FORMAT(data,'%d-%m-%Y')  from capmedica where id = '{$maior_cap}' ";
        $r2 = mysql_query($sql3);
        $data_cap = mysql_result($r2, 0);

        $sql2 = "SELECT pre.id, sol.idPrescricao,
		DATE_FORMAT(pre.data,'%d/%m/%Y - %T') AS sdata,
		DATE_FORMAT(pre.inicio,'%d/%m/%Y') AS inicio,
		DATE_FORMAT(pre.fim,'%d/%m/%Y') AS fim, pre.criador,pre.paciente from prescricoes as pre LEFT JOIN solicitacoes as sol ON(pre.id=sol.idPrescricao) WHERE pre.ID_CAPMEDICA = {$maior_cap}";
        $result = mysql_query($sql2);
        $solicitacao = @mysql_result($result, 0, 1);
        $maior_presc = @mysql_result($result, 0, 0);
        $inicio = @mysql_result($result, 0, 3);
        $fim = @mysql_result($result, 0, 4);
        $data = @mysql_result($result, 0, 2);
        $criador = @mysql_result($result, 0, 5);
        $paciente = @mysql_result($result, 0, 6);


        $datalimit = date('d/m/Y', strtotime("+2 days", strtotime($data_cap)));


        $sql = "SELECT
		DATE_FORMAT(c.data,'%d/%m/%Y %H:%i:%s') as fdata,
		c.id,
		c.paciente as idpaciente,

		u.nome,

		u.tipo as utipo, 
		u.conselho_regional,
		presc.id as prescid,
		u.idUsuarios,
		(Select count(*) from prescricoes P1 where P1.ID_CAPMEDICA =c.id) as controle,
		p.nome as paciente
		FROM
		capmedica as c LEFT JOIN
		usuarios as u ON (c.usuario = u.idUsuarios) LEFT JOIN
		clientes as p ON (c.paciente = p.idClientes) LEFT JOIN
		prescricoes as presc ON (c.id = presc.ID_CAPMEDICA AND carater = 4)

		WHERE
		c.paciente = '{$id}'
		ORDER BY
		c.data DESC";
        $r = mysql_query($sql);
        $flag = true;
        while ($row = mysql_fetch_array($r)) {
            $compTipo = $row['conselho_regional'];

            if ($flag) {

                $p = ucwords(strtolower($row["c.paciente"]));
                $this->cabecalho($id);
                //echo "<center><h2>{$p}</h2></center>";

                echo "<br/><br/>";

                echo "<div><input type='hidden' name='nova_ficha' value='{$id}'></input>";

                echo"</div>";
                echo "<br/><table class='mytable' style='width:100%' >";
                echo "<thead><tr>";
                echo "<th width='55%'>Usu&aacute;rio</th>";
                echo "<th>Data</th>";
                echo "<th width='15%' colspan ='2'> Detalhes</th>";

                //echo "<th>Op&ccedil;&otilde;es</th>";
                echo "</tr></thead>";
                $flag = false;
            }
            $id_presc_sol = '';
            $id_presc_sol = -1;
            $teste1 = $row['idUsuarios'];
            $teste2 = $_SESSION['id_user'];

            $u = ucwords(strtolower($row["nome"])) . $compTipo;

            echo "<tr><td>{$u}</td><td>{$row['fdata']}</td>";
            echo "<td><a href='../medico/?view=paciente_med&act=capmed&id=".$row['id']."'><img src='../utils/capmed_16x16.png' title='Detalhes Ficha' border='0'></a>";
            ///mudou ak
            if ($row['id'] == $maior_cap) {

                if ($row['controle'] == 0 && $datalimit > date('d/m/Y')) {
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=?view=detalhescore&id={$row['id']}><img src='../utils/details_16x16.png' title='Detalhes Score' border='0'></a>";

                    echo"</td>";
                } else {
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='../medico/?view=paciente_med&act=detalhescore&id=".$row['id']."&tipo=1&paciente_id=".$row['idpaciente']."'><img src='../utils/details_16x16.png' title='Detalhes Score' border='0'></a>";
                    if ($row['prescid'] > 0) {
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=?view=detalhesprescricao&id={$row['id']}&idpac={$row['idpaciente']}><img src='../utils/fichas_24x24.png' width='16' title='Detalhes Prescri&ccedil;&atilde;o' border='0'></a>";
                    }
                    echo "</td>";
                }

                echo"<td>";


            } else {
                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='../medico/?view=paciente_med&act=detalhescore&id=".$row['id']."&tipo=1&paciente_id=".$row['idpaciente']."'><img src='../utils/details_16x16.png' title='Detalhes Score' border='0'></a>";
                if ($row['prescid'] > 0) {

                    echo"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=?view=detalhesprescricao&id={$row['id']}&idpac={$row['idpaciente']}><img src='../utils/fichas_24x24.png' width='16' title='Detalhes Prescri&ccedil;&atilde;o' border='0'></a>";
                }
                echo"</td>";
                echo"<td>";

                echo"</td>";
            }
            echo "</td></tr>";
        }

        echo "</table>";
    }


    public function listar_evolucao_medica($id) {
        $id = anti_injection($id, "numerico");
        $sql3 = "select Max(id) as ev from fichamedicaevolucao where PACIENTE_ID = '{$id}' ";
        $r2 = mysql_query($sql3);

        $maior_ev = mysql_result($r2, 0);


        $sql = "SELECT
		DATE_FORMAT(fev.DATA,'%d/%m/%Y %H:%i:%s') as fdata,
		DATE_FORMAT(fev.DATA_SISTEMA, '%d/%m/%Y %H:%i:%s') as data_sist,
		fev.ID,
		fev.PACIENTE_ID as idpaciente,
		u.nome,
		u.tipo as utipo,
		u.conselho_regional,
		p.nome as paciente
		FROM
		fichamedicaevolucao as fev LEFT JOIN
		usuarios as u ON (fev.USUARIO_ID = u.idUsuarios) INNER JOIN
		clientes as p ON (fev.PACIENTE_ID = p.idClientes)

		WHERE
		fev.PACIENTE_ID = '{$id}'
		ORDER BY
		fev.DATA DESC";
        $r = mysql_query($sql);
        $flag = true;
        while ($row = mysql_fetch_array($r)) {
            $compTipo = $row['conselho_regional'];

            if ($flag) {

                $p = ucwords(strtolower($row["fev.PACIENTE_ID"]));
                echo "<center><h1>Lista das Fichas de Evolu&ccedil;&atilde;o M&eacute;dica </h1></center>";
                $this->cabecalho($id);
                echo "<br/><br/>";

                echo "<div><input type='hidden' name='nova_ficha_evolucao' value='{$id}' />";

                /* echo "<button id='nova_ficha_evolucao'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
                  <span class='ui-button-text'>Nova ficha</span></button> */
                echo "</div>";

                echo "<br/><table class='mytable' width='100%' >";
                echo "<thead><tr>";
                echo "<th width='55%'>Usu&aacute;rio</th>";
								echo "<th>Data da Evolu&ccedil;&atilde;o</th>";
								echo "<th>Data do Sistema</th>";
                echo "<th width='15%'>Detalhes</th>";

                echo "</tr></thead>";
                $flag = false;
            }


            $u = ucwords(strtolower($row["nome"])) . $compTipo;

            echo "<tr><td>{$u}</td><td>{$row['fdata']}</td><td>{$row['data_sist']}</td>";
            echo "<td><a href=../medico/?view=paciente_med&act=fichaevolucao&id={$row['ID']}><img src='../utils/capmed_16x16.png' title='Detalhes Ficha Evolu&ccedil;&atilde;o' border='0'></a></td>";


            ///mudou ak
            echo "</tr>";
        }

        echo "</table>";
    }

    ///////////////////////////fim ficha medica
    ////////////////////////FICHA AVALI��O ENFERMAGEM/////////////////////
    public function nova_ficha_avaliacao_enfermagem($id) {

        $id = anti_injection($id, "numerico");
        $result = mysql_query("SELECT p.nome FROM clientes as p WHERE p.idClientes = '{$id}'");

        $fae = new FichaAvaliacaoEnfermagem();
        while ($row = mysql_fetch_array($result)) {
            $fae->pacienteid = $id;
            $fae->pacientenome = ucwords(strtolower($row["nome"]));
        }

        $this->ficha_avaliacao_enfermagem($fae, "");
    }

    public function dietas($id,$editar = null,$origem) {
        $tabela = $origem == 1 ? "avaliacaoenfermagem" : "evolucaoenfermagem";
        $condicao = $origem == 1 ? "avaliacaoenfermagem.*" : "evolucaoenfermagem.*, DATE_FORMAT(evolucaoenfermagem.ULTIMA_SUBSTITUICAO,'%d/%m/%Y') as substituicao";

        if ($editar == 1) {
            $acesso = "readonly"; //readonly
            $enabled = "disabled";
        }

        $sql = "SELECT
                    $condicao
                FROM
                    $tabela
                WHERE
                    ID = {$id}";
        $result = mysql_query($sql);
        if ($origem == 1){
            while ($row = mysql_fetch_array($result)) {
                $oral = $row['VIA_ALIMENTACAO_ORAL'];
                $sne = $row['VIA_ALIMENTACAO_SNE'];
                $sne_num = $row['VIA_ALIMENTACAO_SNE_NUM'];
                $gtm_sonda_gastro = $row['VIA_ALIMENTACAO_GTM'];
                $gtm_num = $row['VIA_ALIMENTACAO_GTM_NUM'];
                $gtm_num_profundidade = $row['VIA_ALIMENTACAO_GMT_PROFUNDIDADE'];
                $gtm_visivel = $row['VIA_ALIMENTACAO_GTM_VISIVEL'];
                $sonda_gastro = $row['VIA_ALIMENTACAO_SONDA_GASTRO'];
                $quant_sonda = $origem == 1 ? $row['VIA_ALIMENTACAO_SONDA_GASTRO_NUM'] : $row['QUANT_SONDA'];
                $parental = $row['VIA_ALIMENTACAO_PARENTAL'];
                $dieta_sn = $row['DIETA_SN'];
            }
        }else{
            while ($row = mysql_fetch_array($result)) {
                $dieta_sn = $row['DIETA_SN'];
                $oral = $row['ORAL'];
                $sne = $row['SNE'];
                $sne_num = $row['SNE_NUM'];
                $gtm_sonda_gastro = $row['GTM_SONDA_GASTRO'];
                $gtm_num = $row['GTM_NUM'];
                $gtm_num_profundidade = $row['GTM_NUM_PROFUNDIDADE'];
                $quant_sonda = $row['QUANT_SONDA'];
                $ultima_substituicao = $row['substituicao'];
            }
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Via " . htmlentities("alimentação") . "</th></tr></thead>";
        echo "<tr><td style='width:33%;' colspan='4'> <b>Dieta Zero: </b>
        <select name='dieta_sn' class='COMBO_OBG'  {$enabled} id='dieta_sn'><option value ='-1'>  </option>
        <option value ='n' " . (($dieta_sn == 'n') ? "selected" : "") . "  >" . htmlentities("Não") . "</option>
            <option value ='s' " . (($dieta_sn == 's') ? "selected" : "") . "   >Sim</option>
                    </select>
                    </td></tr>";


        echo "<tr><td colspan='4'><span class='parental_span'><input type='checkbox' value='1' " . (($parental) ? "checked" : "") . " {$enabled} name='parental'>Parenteral</span> </td></tr>";

        if ($origem == 2){
            if($gtm_sonda_gastro == 1){
                $ultima_substituicao_button = $ultima_substituicao;
                $ultima_substituicao_sonda = null;
            }elseif($gtm_sonda_gastro == 2){
                $ultima_substituicao_button = null;
                $ultima_substituicao_sonda = $ultima_substituicao;
            }
            $mostrar_substituicao_button = "<td width='35%'>ultima substituição<input type='text' disabled class='data dieta-extra-gtm' size='10' name='ultima_substituicao_button' value='{$ultima_substituicao_button}' {$enabled}></td>";
            $mostrar_substituicao_sonda = "<td>ultima substituição<input type='text' disabled class='data dieta-extra-gtm' size='10' name='ultima_substituicao_sonda' value='{$ultima_substituicao_sonda}' {$enabled}></td>";
        }else{
            $mostrar_substituicao_button = '';
            $mostrar_substituicao_sonda = '';
        }

        $botao_desmarcar_button = "<button type='button' class='desmarcar-radio-button'>D</button>";
        $botao_desmarcar_sonda = "<button type='button' class='desmarcar-radio-sonda'>D</button>";
        if($acesso == 'readonly') {
            $botao_desmarcar = "";
        }

        echo "<tr><td colspan='4' >";
        echo "<span  name='alimentacao' id='alimentacao_span2' ><table id='via_alimentacao' style='width:100%;'></span>";

        echo "<tr class='alimentacao_span2'><td style='width:33%;'><input type='checkbox' value='1' " . (($oral) ? "checked" : "") . " {$enabled} class='dieta_n_check' name='oral' id='oral'>Oral</td></tr>";
        echo "<tr class='alimentacao_span2'><td><input type='checkbox' value='1' " . (($sne) ? "checked" : "") . " {$enabled} class='dieta_n_check' name='sne' id='sne'>SNE </td><td>" . htmlentities("N°") . "<input type='text' size='3' maxlength='5' name='sne_num' disabled value='{$sne_num}' {$acesso}></tr></td>";
        echo "<tr class='alimentacao_span2' style='background-color:#ccc;'>
                <td colspan='4' align='center'><b>GTM</b></td></tr>";
        echo "<tr class='alimentacao_span2'>
                <td>
                    {$botao_desmarcar_button}
                    <input type='radio' value='1' " . (($gtm_sonda_gastro == 1) ? "checked" : "") . " {$enabled} class='dieta_n_check' name='gtm_sonda_gastro'>Button 
                </td>
                <td>" . htmlentities("N°") . "<input type='text' size='3' maxlength='5' disabled name='gtm_num' class='dieta-extra-button' value='{$gtm_num}' {$acesso}> profundidade 
                    <input type='text' size='3' maxlength='5' class='dieta-extra-button' disabled name='gtm_num_profundidade' value='{$gtm_num_profundidade}' {$acesso}> cm</td>
                    $mostrar_substituicao_button
              </tr>";
        echo "<tr class='alimentacao_span2'>
                <td>
                    {$botao_desmarcar_sonda}
                    <input type='radio' value='2' " . (($gtm_sonda_gastro == 2) ? "checked" : "") . " {$enabled} class='dieta_n_check' name='gtm_sonda_gastro'>Sonda Gastro 
                </td>
                <td>" . htmlentities("N°") . "<input type='text' size='3' class='dieta-extra-sonda' maxlength='5' disabled name='quant_sonda' value='{$quant_sonda}' {$acesso}> fr</td>
                $mostrar_substituicao_sonda
              </tr>";
        echo "</table>";

        echo "</tr></td>";
        echo "</table>";
    }

    public function itensDietas($id, $editar = null,$origem) {
        $tabela = $origem == 1 ? "avaliacaoenfermagem" : "evolucaoenfermagem";
        if ($editar == 1) {
            $acesso = "readonly"; //readonly
            $enabled = "disabled";
        }
        $sql = "SELECT
                    $tabela.*
                FROM
                    $tabela
                WHERE
                    ID = {$id}";
        //echo $sql;
        $result = mysql_query($sql);
        if($origem == 1){
            while($row = mysql_fetch_array($result)) {
                $sistema_aberto = $row['DIETA_SIST_ABERTO'];
                $uso_sistema_aberto = $row['USO_SISTEMA_ABERTO'];
                $quant_sistema_aberto = $row['DIETA_SIST_ABERTO_DESCRICAO'];
                $uso = $row['DIETA_SIST_ABERTO_USO'];
                $sistema_fechado = $row['DIETA_SIST_FECHADO'];
                $quant_sistema_fechado = $row['DIETA_SIST_FECHADO_DESCRICAO'];
                $vazao = $row['DIETA_SIST_FECHADO_VAZAO'];
                $dieta_artesanal = $row['DIETA_SIST_ARTESANAL'];
                $suplemento_sn = $row['DIETA_SUPLEMENTO_SN'];
                $suplemento = $row['DIETA_SUPLEMENTO'];
                $suplemento_desc = $row['DIETA_SUPLEMENTO_DESC'];
                $dieta_npt = $row['DIETA_NPT'];
                $desc_npt = $row['DIETA_NPT_DESC'];
                $vazao_npt = $row['DIETA_NPT_VAZAO'];
                $npp = $row['DIETA_NPP'];
                $desc_npp = $row['DIETA_NPP_DESC'];
                $vazao_npp = $row['DIETA_NPP_VAZAO'];
            }

        }else{
            while ($row = mysql_fetch_array($result)) {
                $sistema_aberto = $row['SISTEMA_ABERTO'];
                $quant_sistema_aberto = $row['QUANT_SISTEMA_ABERTO'];
                $uso_sistema_aberto = $row['USO_SISTEMA_ABERTO'];
                $sistema_fechado = $row['SISTEMA_FECHADO'];
                $quant_sistema_fechado = $row['QUANT_SISTEMA_FECHADO'];
                $vazao = $row['VAZAO'];
                $dieta_npt = $row['DIETA_NPT'];
                $vazao_npt = $row['VAZAO_NPT'];
                $desc_npt = $row['desc_npt'];
                $npp = $row['DIETA_NPP'];
                $vazao_npp = $row['VAZAO_NPP'];
                $desc_npp = $row['desc_npp'];
                $dieta_artesanal = $row['DIETA_ARTESANAL'];
                $suplemento_sn = $row['SUPLEMENTO_SN'];
                $suplemento = $row['SUPLEMENTO'];
            }

        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Dieta</th></tr></thead>";
        echo "<tr class='alimentacao_span2'><td style='width:33%;'><input type ='checkbox' value='1' " . (($sistema_aberto) ? "checked" : "") . " {$enabled} class='sistema_dieta' name='sistema_aberto' id='sistema_aberto'> Sistema aberto </td><td colspan='3'>NOME <input type ='text' size='30' maxlength='50' name='quant_sistema_aberto' disabled value='{$quant_sistema_aberto}' {$acesso}>USO <input type ='text' size='25' maxlength='50' name='uso_sistema_aberto' disabled value='{$uso_sistema_aberto}' {$acesso}></td></tr>";
        echo "<tr class='alimentacao_span2'><td><input type ='checkbox' value='1'  " . (($sistema_fechado) ? "checked" : "") . " {$enabled}  class='sistema_dieta' name='sistema_fechado' id='sistema_fechado'> Sistema fechado</td><td colspan='3'> NOME <input type ='text' size='30' maxlength='50' value='{$quant_sistema_fechado}' {$acesso} name='quant_sistema_fechado' disabled>" . htmlentities("Vazão") . " <input type ='text' size='8' maxlength='8' name='vazao' value='{$vazao}' {$acesso} disabled>ML/H </td></tr>";
        echo "<tr class='parental_span'><td><input type ='checkbox' value = '1'  " . (($dieta_npt) ? "checked" : "") . " {$enabled} class='dieta_s_check' name='dieta_npt' id='dieta_npt'>NPT </td><td colspan='3'>NOME <input type ='text' size='30' maxlength='50' disabled name='desc_npt' value='{$desc_npt}' {$acesso}>" . htmlentities("Vazão") . " <input type ='text' size='8' maxlength='8' disabled name='vazao_npt' value='{$vazao_npt}' {$acesso}>ML/H </td></tr>";
        echo "<tr class='parental_span'><td > <input type ='checkbox' value='1'  " . (($npp) ? "checked" : "") . " {$enabled} class='dieta_s_check' name='npp' id='npp'> NPP </td><td colspan='3'>NOME <input type='text' size='30' maxlength='50' disabled name='desc_npp' value='{$desc_npp}' {$acesso}>" . htmlentities("Vazão") . " <input type ='text' size='8' maxlength='8' disabled name='vazao_npp' value='{$vazao_npp}' {$acesso}>ML/H </tr></td>";
        echo "<tr class='alimentacao_span2'><td colspan='4'><input type ='checkbox' value='1' " . (($dieta_artesanal) ? "checked" : "") . " {$enabled} class='sistema_dieta' name ='dieta_artesanal'> Dieta Artesanal </tr></td>";
        echo"<tr class='alimentacao_span2'><td colspan='4'><b>Suplemento </b><select name='suplemento_sn' id='suplemento_sn2' {$enabled} class='COMBO_OBG'>
        <option value ='n' " . (($suplemento_sn == 'n') ? "selected" : "") . "  >N&atilde;o</option>
                <option value ='s' " . (($suplemento_sn == 's') ? "selected" : "") . "   >Sim</option>
                        </select>";
        if ($editar != 1 && $suplemento_sn == 's') {
            $estilo = '';
        } else {
            $estilo = " style='display:none;' ";
        }
        echo"<span name='suplementos' {$estilo} id='suplemento_span2' ><input type='text' size='110' name='suplemento' class='suplemento_add' size=50/><button id='add-suplemento' tipo=''  >+ </button></span></td></tr>";
        echo "<tr><td colspan='4' >";

        $suplemento = $origem == 1 ? "avaliacao_enf_suplemento" : "evolucao_enf_suplemento";
        $condicao = $origem == 1 ? "AVALIACAO_ENF_ID" : "EVOLUCAO_ENF_ID";

        echo "<table id='suplemento' style='width:100%;'>";
        if ($suplemento_sn == 's') {
            $sql4 = "SELECT * from $suplemento where $condicao = {$id}";
            $result4 = mysql_query($sql4);

            while ($row4 = mysql_fetch_array($result4)) {
                $local_suplemento = $row4['NOME'];
                $frequencia_suplemento = $row4['FREQUENCIA'];
                if ($editar == 1) {
                    $classe = '';
                } else {
                    $classe = "class='del-suplemento'";
                }
                echo "<tr><td colspan='2' class='suplemento' desc='{$local_suplemento}' > <img src='../utils/delete_16x16.png'  {$classe} title='Excluir' border='0' {$enabled} > {$local_suplemento} <b>" . htmlentities("Frequência") . ":</b> {$frequencia_suplemento}</td> </tr>";
            }
        }
        if (!empty($ev->suplemento)) {
            foreach ($ev->suplemento as $a) {
                echo "<tr><td>{$a}</td></tr>";
            }
        }
        echo "</table>";
        echo "</td></tr>";
        echo "</table>";
    }

    public function feridas($id,$v){
        $deletar_ferida = "<img style='float:right;' src='../utils/delete_16x16.png' class='del_ferida_avaliacao' title='Excluir Ferida' border='0' >";
        if ($v == 1) {
            $acesso = "readonly";
            $enabled = "disabled";
            $deletar_ferida = "";
        }

        echo "<div class='div-quadro-feridas'>";
        echo "<table id='tabela_ferida' class='mytable' style='width:95%;'>";
        $sql = "SELECT
                    *, files_uploaded.id as file_uploaded_id, files_uploaded.full_path, files_uploaded.path
                FROM
                    quadro_feridas_enfermagem
                LEFT JOIN files_uploaded ON files_uploaded.id = quadro_feridas_enfermagem.FILE_UPLOADED_ID
                WHERE
                    quadro_feridas_enfermagem.ID = '{$id}'";
        $result = mysql_query($sql);
        $obs_imagem_ferida = '';
        $imagem_ferida_path = false;
        while($row = mysql_fetch_array($result)){
            $local = $row['LOCAL'];
            $lado = $row['LADO'];
            $tipo = $row['TIPO'];
            $cobertura = $row['COBERTURA'];
            $tamanho = $row['TAMANHO'];
            $profundidade = $row['TAMANHO'];
            $tecido = $row['TECIDO'];
            $grau = $row['GRAU'];
            $humidade = $row['HUMIDADE'];
            $exsudato = $row['EXSUDATO'];
            $contaminacao = $row['CONTAMINACAO'];
            $pele = $row['PELE'];
            $tempo = $row['TEMPO'];
            $odor = $row['ODOR'];
            $periodo = $row['PERIODO'];
            $status_ferida = $row['STATUS_FERIDA'];
            $imagem_ferida_path = $row['path'];
            $image_ferida_full_path = $row['full_path'];
            $obs_imagem_ferida = $row['OBSERVACAO_IMAGEM'];
            $file_uploaded_id = $row['file_uploaded_id'];
            $observacao =$row['observacao'];
        }


        echo "<thead><tr><th colspan='3'>Ferida $deletar_ferida</th></tr></thead>";
        $sql_local = "SELECT
                        *
                      FROM
                        quadro_feridas_enf_local
                      ORDER BY
                        LOCAL
                      ASC
                      ";
        $result_local = mysql_query($sql_local);

        echo "<tr>
<td width='20%'>Local</td>
<td width='25%'>
<select id='quadro_local_ferida' name='quadro_local_ferida' class='ferida' $enabled>
            <option value='-1'>SELECIONE</option>";
        while($row = mysql_fetch_array($result_local)){
            echo "<option value='{$row['ID']}' " . (($local == $row['ID']) ? "selected" : "") . " lado='{$row['LADO']}'>{$row['LOCAL']}</option>";
        }
        echo "</select>
<br>

</td>";
        $mostrar_lado = 'display:none';
        if($lado > 0){
            $mostrar_lado = 'display:inline';
        }
        $mostrar_obs = 'display:none';
        if(!empty($observacao)){
            $mostrar_obs = 'display:inline';
        }
        echo "<td >&nbsp;<span style='$mostrar_lado'>Lado:</span>&nbsp;<select name='lado_ferida' $enabled style='$mostrar_lado'>
            <option value='-1'>SELECIONE</option>
            <option value='1' " . (($lado == '1') ? "selected" : "") . ">DIREITO</option>
            <option value='2' " . (($lado == '2') ? "selected" : "") . ">ESQUERDO</option>
            </select>
            <br>
            <span style='$mostrar_obs'> Observação</span> <textarea col = '2' rol='2' $enabled style='$mostrar_obs'>{$observacao}</textarea>
            </td>
            </tr>";
        $sql_tipo = "SELECT
                    *
                  FROM
                    quadro_feridas_enf_tipo
                  ORDER BY
                    ID
                  ASC
                  ";
        $result_tipo = mysql_query($sql_tipo);
        echo "<tr><td width='20%'>TIPO DE LES&Atilde;O</td><td width='25%' colspan='2'><select id='quadro_tipo_ferida' name='quadro_tipo_ferida' class='ferida' $enabled>
            <option value='-1'>SELECIONE</option>";
        while($row = mysql_fetch_array($result_tipo)){
            echo "<option value='{$row['ID']}' " . (($tipo == $row['ID']) ? "selected" : "") . ">{$row['TIPO']}</option>";
        }
            echo "</select></td></tr>";
        $sql_cobertura = "SELECT
                    *
                  FROM
                    quadro_feridas_enf_lesao
                  ORDER BY
                    ID
                  ASC
                  ";
        $result_cobertura = mysql_query($sql_cobertura);
        $sqlQuadroFeridaCobertura = <<<SQL
Select
cobertura_id
FROM
quadro_feridas_enfermagem_cobertura
where
quadro_feridas_enfermagem_id = $id

SQL;

        $coberturas ='';
        $resultQuadroFeridaCobertura = mysql_query($sqlQuadroFeridaCobertura);
        while($row = mysql_fetch_array($resultQuadroFeridaCobertura)){
            $coberturas[] = $row['cobertura_id'];
        }




            echo "<tr><td>COBERTURA ATUAL UTILIZADA</td><td width='25%' colspan='2'>
<select  name='quadro_cobertura_ferida[]' class='chosen-select-ferida ferida' style='width: 800px;' data-placeholder='Selecione os tipos de coberturas...' multiple {$enabled}>";
        while($row = mysql_fetch_array($result_cobertura)){
            
            echo "<option value='{$row['ID']}' " . (in_array($row['ID'],$coberturas) ? "selected" : "") . ">{$row['TIPO']}</option>";
        }
            echo "</select></td></tr>";






        $sql_tamanho = "SELECT
                    *
                  FROM
                    quadro_feridas_enf_tamanho
                  ORDER BY
                    ID
                  ASC
                  ";
        $result_tamanho = mysql_query($sql_tamanho);
            echo "<tr><td>TAMANHO</td><td width='25%' colspan='2'><select id='quadro_tamanho_ferida' name='quadro_tamanho_ferida' class='ferida' {$enabled}>
            <option value='-1'>SELECIONE</option>";
        while($row = mysql_fetch_array($result_tamanho)){
            echo "<option value='{$row['ID']}' " . (($tamanho == $row['ID']) ? "selected" : "") . ">{$row['TAMANHO']}</option>";
        }
            echo "</select></td></tr>";
            $sql_profundidade = "SELECT
                    *
                  FROM
                    quadro_feridas_enf_profundidade
                  ORDER BY
                    ID
                  ASC
                  ";
        $result_profundidade = mysql_query($sql_profundidade);
            echo "<tr><td>PROFUNDIDADE</td><td width='25%' colspan='2'><select id='quadro_profundidade_ferida' name='quadro_profundidade_ferida' class='ferida' {$enabled}>
            <option value='-1'>SELECIONE</option>";
        while($row = mysql_fetch_array($result_profundidade)){
            echo "<option value='{$row['ID']}' " . (($profundidade == $row['ID']) ? "selected" : "") . ">{$row['PROFUNDIDADE']}</option>";
        }
            echo "</select></td></tr>";
            $sql_tecido = "SELECT
                    *
                  FROM
                    quadro_feridas_enf_tecido
                  ORDER BY
                    ID
                  ASC
                  ";
        $result_tecido = mysql_query($sql_tecido);
            echo "<tr><td>TIPO DE TECIDO</td><td width='25%' colspan='2'><select id='quadro_tecido_ferida' name='quadro_tecido_ferida' class='ferida' {$enabled}>
            <option value='-1'>SELECIONE</option>";
        while($row = mysql_fetch_array($result_tecido)){
            echo "<option value='{$row['ID']}' " . (($tecido == $row['ID']) ? "selected" : "") . ">{$row['TECIDO']}</option>";
        }
            echo "</select></td></tr>";
            $sql_grau = "SELECT
                    *
                  FROM
                    quadro_feridas_enf_grau
                  ORDER BY
                    ID
                  ASC
                  ";
        $result_grau = mysql_query($sql_grau);
            echo "<tr><td>GRAU DA LES&Atilde;O</td><td width='25%' colspan='2'><select id='quadro_lesao_ferida' name='quadro_lesao_ferida' class='ferida' {$enabled}>
            <option value='-1'>SELECIONE</option>";
        while($row = mysql_fetch_array($result_grau)){
            echo "<option value='{$row['ID']}' " . (($grau == $row['ID']) ? "selected" : "") . ">{$row['GRAU']}</option>";
        }
            echo "</select></td></tr>";
            $sql_humidade = "SELECT
                    *
                  FROM
                    quadro_feridas_enf_humidade
                  ORDER BY
                    ID
                  ASC
                  ";
        $result_humidade = mysql_query($sql_humidade);
            echo "<tr><td>GRAU DE UMIDADE</td><td width='25%' colspan='2'><select id='quadro_humidade_ferida' name='quadro_humidade_ferida' class='ferida' {$enabled}>
            <option value='-1'>SELECIONE</option>";
        while($row = mysql_fetch_array($result_humidade)){
            echo "<option value='{$row['ID']}' " . (($humidade == $row['ID']) ? "selected" : "") . ">{$row['HUMIDADE']}</option>";
        }
            echo "</select></td></tr>";
            $sql_exsudato = "SELECT
                    *
                  FROM
                    quadro_feridas_enf_exsudato
                  ORDER BY
                    ID
                  ASC
                  ";
        $result_exsudato = mysql_query($sql_exsudato);
            echo "<tr><td>TIPO DE EXSUDATO</td><td width='25%' colspan='2'><select id='quadro_exsudato_ferida' name='quadro_exsudato_ferida' class='ferida' {$enabled}>
            <option value='-1'>SELECIONE</option>";
        while($row = mysql_fetch_array($result_exsudato)){
            echo "<option value='{$row['ID']}' " . (($exsudato == $row['ID']) ? "selected" : "") . ">{$row['EXSUDATO']}</option>";
        }
            echo "</select></td></tr>";
            $sql_contaminacao = "SELECT
                    *
                  FROM
                    quadro_feridas_enf_contaminacao
                  ORDER BY
                    ID
                  ASC
                  ";
        $result_contaminacao = mysql_query($sql_contaminacao);
            echo "<tr><td>GRAU DE CONTAMINA&Ccedil;&Atilde;O</td><td width='25%' colspan='2'><select id='quadro_contaminacao_ferida' name='quadro_contaminacao_ferida' class='ferida' {$enabled}>
            <option value='-1'>SELECIONE</option>";
        while($row = mysql_fetch_array($result_contaminacao)){
            echo "<option value='{$row['ID']}' " . (($contaminacao == $row['ID']) ? "selected" : "") . ">{$row['CONTAMINACAO']}</option>";
        }
            echo "</select></td></tr>";
            $sql_pele = "SELECT
                    *
                  FROM
                    quadro_feridas_enf_pele
                  ORDER BY
                    ID
                  ASC
                  ";
        $result_pele = mysql_query($sql_pele);
            echo "<tr><td>PELE PERI-LES&Atilde;O</td><td width='25%' colspan='2'><select id='quadro_pele_ferida' name='quadro_pele_ferida' class='ferida' {$enabled}>
            <option value='-1'>SELECIONE</option>";
        while($row = mysql_fetch_array($result_pele)){
            echo "<option value='{$row['ID']}' " . (($pele == $row['ID']) ? "selected" : "") . ">{$row['PELE']}</option>";
        }
            echo "</select></td></tr>";
            $sql_tempo = "SELECT
                    *
                  FROM
                    quadro_feridas_enf_tempo
                  ORDER BY
                    ID
                  ASC
                  ";
        $result_tempo = mysql_query($sql_tempo);
            echo "<tr><td>TEMPO DA LES&Atilde;O</td><td width='25%' colspan='2'><select id='quadro_tempo_ferida' name='quadro_tempo_ferida' class='ferida' {$enabled}>
            <option value='-1'>SELECIONE</option>";
        while($row = mysql_fetch_array($result_tempo)){
            echo "<option value='{$row['ID']}' " . (($tempo == $row['ID']) ? "selected" : "") . ">{$row['TEMPO']}</option>";
        }
            echo "</select></td></tr>";
            echo "<tr><td>ODOR</td><td width='25%' colspan='2'><select id='quadro_odor_ferida' name='quadro_odor_ferida' class='ferida' {$enabled}>
                    <option value='-1'>SELECIONE</option>
                    <option value='1' " . (($odor == '1') ? "selected" : "") . ">N&Atilde;O</option>
                    <option value='2' " . (($odor == '2') ? "selected" : "") . ">F&Eacute;TIDO</option>";
            echo "</select></tr></td>";

            $sql_periodo = "SELECT
                    *
                  FROM
                    quadro_feridas_enf_periodo
                  ORDER BY
                    ID
                  ASC
                  ";
        $result_periodo = mysql_query($sql_periodo);
            echo "<tr><td>".htmlentities("PERÍODO DE TROCA")." DA COBERTURA EM USO</td><td width='25%' colspan='2'><select id='quadro_periodo_ferida' name='quadro_periodo_ferida' class='ferida' {$enabled}>
            <option value='-1'>SELECIONE</option>";
        while($row = mysql_fetch_array($result_periodo)){
            echo "<option value='{$row['ID']}' " . (($periodo == $row['ID']) ? "selected" : "") . ">{$row['PERIODO_TROCA']}</option>";
        }
            echo "</select></td></tr>";

        $relatorioFerida = $_GET['view'];
        if (isset($_POST['fromRelatorio']))
          $relatorioFerida = $_POST['fromRelatorio'];

        $sql_status = $this->getStatusFeridaSQL($relatorioFerida);

        $result_status = mysql_query($sql_status);
        echo "<tr><td> STATUS DA FERIDA</td><td width='25%' colspan='2'><select id='quadro_status_ferida' name='quadro_status_ferida{$status_ferida}' class='ferida' {$enabled}>
            <option value='-1'>SELECIONE</option>";
        while ($row = mysql_fetch_array($result_status)) {
            echo "<option value='{$row['id']}' " . (($status_ferida == $row['id']) ? "selected" : "") . ">{$row['status_ferida']}</option>";
        }
        echo "</select></td></tr>";

        $isWriteMode = (isset($_GET['v']) && $_GET['v'] != 1) || false === isset($_GET['v']);

        $form_upload_imagem = '';

        if ($isWriteMode) {
          $form_upload_imagem = <<<FORM_UPLOAD_IMAGEM
<form class='upload_form' enctype='multipart/form-data' method='post'>
  <input type='file' class='images-ferida' data-date='' name='feridas[]' onchange="uploadFile(this, 'upload_imagem2.php', '.div-quadro-feridas')" data-images="{$file_uploaded_id}"/>
  <span class='progressBar'></span>
  <h2 class='status_image'></h2>
</form>
FORM_UPLOAD_IMAGEM;
        }

        if ($isWriteMode && $imagem_ferida_path) {
          $btnExcluirImagem = '<a class="image-ferida-thumb excluir-imagem" onclick="excluirImagem(this)">Excluir</a>';
        }

        $display_ferida_imagem = '<div class="image-ferida-thumb">SEM IMAGEM</div>';
        if ($imagem_ferida_path) {
          $imagem_ferida_url = App\Helpers\Storage::getImageUrl($imagem_ferida_path);
          $imagem_ferida_viewer = App\Helpers\Storage::getImageUrlWithViewer($imagem_ferida_path);
          $display_ferida_imagem = sprintf(
            '<a target="_blank" href="%s"><img src="%s" class="%s" ></img></a>' . $btnExcluirImagem,
            $imagem_ferida_viewer,
            $imagem_ferida_url,
            'image-ferida-thumb'
          );
        }

        echo "<tr>
                  <td>IMAGEM DA FERIDA (2MB)</td>
                  <td colspan='2'>
                    {$form_upload_imagem}
                    {$display_ferida_imagem}
                  </td>
              </tr>
        ";
        echo "</table>";
        echo "</div>";

    }

public function getStatusFeridaSQL($relatorio = null) {
    $isRelatorioAvaliacao = isset($relatorio)
                            && in_array(
                                $relatorio,
                                [
                                  'listavaliacaenfermagem',
                                  'novaavaliacaoenf',
                                  'editaravaliacaoenfermagem',
                                  'avaliacao_enfermagem'
                                ]
                            );

    $whereStatusFerida = 'WHERE relatorio IS NULL';
    if ($isRelatorioAvaliacao)
        $whereStatusFerida = 'WHERE relatorio IN ("avaliação")';            

    return "SELECT * FROM quadro_feridas_enf_status {$whereStatusFerida} ORDER BY id ASC";   
}

public function dispositivosIntravenosos($id = null,$editar = null) {
        if ($editar == 1) {
            $acesso = "readonly";
            $enabled = "disabled";
        }

        $sql = "select * from avaliacaoenfermagem where id = {$id}";
        //echo $sql;
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $dispositivos_intravenosos = $row['DISPOSITIVOS_INTRAVENOSOS'];
            $cvc = $row['CVC'];
            $cateter_venoso_central = $row['DISPO_INTRAVENOSO_CVC'];
            $local_cvc = $row['DISPO_INTRAVENOSO_CVC_LOCAL'];
            $local_uno = $row['DISPO_INTRAVENOSO_UNO_LOCAL'];
            $local_duplo = $row['DISPO_INTRAVENOSO_DUPLO_LOCAL'];
            $local_triplo = $row['DISPO_INTRAVENOSO_TRIPLO_LOCAL'];
            $avp = $row['DISPO_INTRAVENOSO_AVP'];
            $avp_local = $row['DISPO_INTRAVENOSO_AVP_LOCAL'];
            $port = $row['DISPO_INTRAVENOSO_PORTOCATH'];
            $quant_port = $row['DISPO_INTRAVENOSO_PORTOCATH_NUM'];
            $port_local = $row['DISPO_INTRAVENOSO_PORTOCATH_LOCAL'];
            $outros_intra = $row['DISPO_INTRAVENOSO_OUTROS'];
            $picc= $row['DISPO_INTRAVENOSO_CATETER_PICC'];
            $picc_local= $row['DISPO_INTRAVENOSO_CATETER_PICC_LOCAL'];
        }

        $var .=  "<table class='mytable' style='width:95%;'>
                    <thead><th>Dispositivos Intravenosos <select name='dispositivos_intravenosos' $enabled class='dispositivos_intravenosos COMBO_OBG' " . (($dispositivos_intravenosos == '-1') ? "selected" : "") . " {$enabled}><option value='-1'>   </option><option value='1' " . (($dispositivos_intravenosos == '1') ? "selected" : "") . " {$enabled}> N&atilde;o </option><option value='2' " . (($dispositivos_intravenosos == '2') ? "selected" : "") . " {$enabled}>Sim</option></select></th></thead>";

        $show_cvc = "style='display:none'";
        if ($cvc >= '1') {
            $show_cvc = "style='display:block'";
        }
        $show_dispositivos = "style='display:none'";
        if ($dispositivos_intravenosos == '2') {
            $show_dispositivos = "style='display:block'";
        }
        $var .= "<tr class='dispositivos_span' {$show_dispositivos}><td><input type='checkbox' value='1' " . (($cateter_venoso_central == '1') ? "checked" : "") . " {$enabled} name='cateter_venoso_central' class='valida_cvc'>CVC</td></tr>
                    <tr class='cvc_span' {$show_cvc} {$show_dispositivos}><td style='width:150px;'><input type = 'radio' value='1' " . (($cvc == '1') ? "checked" : "") . " {$enabled}   name='cvc' >Uno Lumen </td><td>Local<input type ='text' name='local_uno' size='25' value='{$local_uno}' {$acesso} > </td></tr>
                    <tr class='cvc_span' {$show_cvc}><td style='width:150px;'><input type = 'radio' value='2' " . (($cvc == '2') ? "checked" : "") . " {$enabled}   name='cvc' >Duplo Lumen</td><td>Local<input type = 'text' size='25' value='{$local_duplo}' {$acesso}  name='local_duplo' ></td></tr>
                    <tr class='cvc_span' {$show_cvc}><td style='width:150px;'><input type = 'radio' value='3' " . (($cvc == '3') ? "checked" : "") . " {$enabled}   name='cvc' >Triplo Lumen</td><td>Local<input type = 'text' size='25' value='{$local_triplo}' {$acesso}  name='local_triplo' ></td></tr>
                    <tr class='dispositivos_span' {$show_dispositivos}><td style='width:143px;'> <input type = 'checkbox' value='1' " . (($avp) ? "checked" : "") . " {$enabled}  name='avp' class='valida_cvc'>AVP</td><td>Local<input type = 'text' size='25' value='{$avp_local}' {$acesso} name='avp_local' ></td>
                    <tr class='dispositivos_span' {$show_dispositivos}><td style='width:143px;'><input type = 'checkbox' value='1' " . (($port) ? "checked" : "") . " {$enabled}   name='port' class='valida_cvc'>Port-O-Cath </td><td>N&ordm;  <input type = 'text' size='5' maxlength='5' value='{$port_local}' {$acesso} name='quant_port' >Local<input type = 'text' size='25' value='{$port_local}' {$acesso} name='port_local' ></td></tr>
                    <tr class='dispositivos_span' {$show_dispositivos}><td style='width:143px;'> <input type = 'checkbox' value='1' " . (($picc) ? "checked" : "") . " {$enabled}  name='picc' class='valida_cvc'>Cateter de PICC</td><td>Local<input type = 'text' size='25' value='{$picc_local}' {$acesso} name='picc_local' ></td>
                    <tr class='dispositivos_span' {$show_dispositivos}><td>Outros<textarea cols=66 rows='3' name='outros_intra' value='{$outros_intra}' {$acesso}></textarea></td></tr>
                    </table>";
        echo $var;
    }

     public function dispositivosIntravenososEvolucao($id = null,$editar = null) {
        if ($editar == 1) {
            $acesso = "readonly";
            $enabled = "disabled";
        }

        $sql = "select * from evolucaoenfermagem where id = {$id}";
        //echo $sql;
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $dispositivos_intravenosos = $row['DISPOSITIVOS_INTRAVENOSOS'];
            $cvc = $row['CVC'];
            $cateter_venoso_central = $row['CVC_TIPO'];
            $local_cvc = $row['CVC_LOCAL'];
            $local_uno = $row['UNO_LOCAL'];
            $local_duplo = $row['DUPLO_LOCAL'];
            $local_triplo = $row['TRIPLO_LOCAL'];
            $avp = $row['AVP'];
            $avp_local = $row['AVP_LOCAL'];
            $port = $row['PORTOCATH'];
            $quant_port = $row['PORTOCATH_NUM'];
            $port_local = $row['PORTOCATH_LOCAL'];
            $outros_intra = $row['OUTROS_INTRA'];
            $picc= $row['CATETER_PICC'];
            $picc_local= $row['CATETER_PICC_LOCAL'];
        }

        $var .=  "<table class='mytable' style='width:95%;'>
                    <thead><th>Dispositivos Intravenosos <select name='dispositivos_intravenosos' $enabled class='dispositivos_intravenosos COMBO_OBG' " . (($dispositivos_intravenosos == '-1') ? "selected" : "") . " {$enabled}><option value='-1'>   </option><option value='1' " . (($dispositivos_intravenosos == '1') ? "selected" : "") . " {$enabled}> N&atilde;o </option><option value='2' " . (($dispositivos_intravenosos == '2') ? "selected" : "") . " {$enabled}>Sim</option></select></th></thead>";

        $show_cvc = "style='display:none'";
        if ($cvc >= '1') {
            $show_cvc = "style='display:block'";
        }
        $show_dispositivos = "style='display:none'";
        if ($dispositivos_intravenosos == '2') {
            $show_dispositivos = "style='display:block'";
        }
        $var .= "<tr class='dispositivos_span' {$show_dispositivos}><td><input type='checkbox' value='1' " . (($cateter_venoso_central == '1') ? "checked" : "") . " {$enabled} name='cateter_venoso_central' class='valida_cvc'>CVC</td></tr>
                    <tr class='cvc_span' {$show_cvc} {$show_dispositivos}><td style='width:150px;'><input type = 'radio' value='1' " . (($cvc == '1') ? "checked" : "") . " {$enabled}   name='cvc' >Uno Lumen </td><td>Local<input type ='text' name='local_uno' size='25' value='{$local_uno}' {$acesso} > </td></tr>
                    <tr class='cvc_span' {$show_cvc}><td style='width:150px;'><input type = 'radio' value='2' " . (($cvc == '2') ? "checked" : "") . " {$enabled}   name='cvc' >Duplo Lumen</td><td>Local<input type = 'text' size='25' value='{$local_duplo}' {$acesso}  name='local_duplo' ></td></tr>
                    <tr class='cvc_span' {$show_cvc}><td style='width:150px;'><input type = 'radio' value='3' " . (($cvc == '3') ? "checked" : "") . " {$enabled}   name='cvc' >Triplo Lumen</td><td>Local<input type = 'text' size='25' value='{$local_triplo}' {$acesso}  name='local_triplo' ></td></tr>
                    <tr class='dispositivos_span' {$show_dispositivos}><td style='width:143px;'> <input type = 'checkbox' value='1' " . (($avp) ? "checked" : "") . " {$enabled}  name='avp' class='valida_cvc'>AVP</td><td>Local<input type = 'text' size='25' value='{$avp_local}' {$acesso} name='avp_local' ></td>
                    <tr class='dispositivos_span' {$show_dispositivos}><td style='width:143px;'><input type = 'checkbox' value='1' " . (($port) ? "checked" : "") . " {$enabled}   name='port' class='valida_cvc'>Port-O-Cath </td><td>N&ordm;  <input type = 'text' size='5' maxlength='5' value='{$port_local}' {$acesso} name='quant_port' >Local<input type = 'text' size='25' value='{$port_local}' {$acesso} name='port_local' ></td></tr>
                    <tr class='dispositivos_span' {$show_dispositivos}><td style='width:143px;'> <input type = 'checkbox' value='1' " . (($picc) ? "checked" : "") . " {$enabled}  name='picc' class='valida_cvc'>Cateter de PICC</td><td>Local<input type = 'text' size='25' value='{$picc_local}' {$acesso} name='picc_local' ></td>
                    <tr class='dispositivos_span' {$show_dispositivos}><td>Outros<textarea cols=66 rows='3' name='outros_intra'  {$acesso}>{$outros_intra}</textarea></td></tr>
                    </table>";
        echo $var;
    }

    public function scoreNead($id, $disabled = '') {
        echo "<thead>
                   <tr>
                      <th colspan='3'>Tabela de Avaliação para Internação Domiciliar-NEAD<sup>6<sup></th>
                   </tr>
            </thead>";
        if (!empty($id)) {
            $sql2 = "SELECT
                sc_pac.OBSERVACAO,
                sc_pac.ID_PACIENTE,
                sc_pac.ID_ITEM_SCORE,
                sc_item.DESCRICAO,
                sc_item.PESO,
                sc_item.GRUPO_ID ,
                sc_item.ID ,
                sc.id as tbplano,
                DATE(sc_pac.DATA) as DATA,
                grup.DESCRICAO as grdesc
                FROM score_paciente sc_pac
            LEFT OUTER JOIN score_item sc_item ON sc_pac.ID_ITEM_SCORE = sc_item.ID
            LEFT OUTER JOIN avaliacaoenfermagem ae ON sc_pac.ID_AVALIACAO_ENFERMAGEM = ae.ID
            LEFT OUTER JOIN grupos grup ON sc_item.GRUPO_ID = grup.ID
            LEFT OUTER JOIN score sc ON sc_item.SCORE_ID = sc.ID
                WHERE sc_pac.ID_AVALIACAO_ENFERMAGEM = '{$id}' and grup.GRUPO_NEAD_ID IS NOT NULL";
            $result2 = mysql_query($sql2);
            $elemento = '';
            while ($row = mysql_fetch_array($result2)) {

                $elemento[$row['ID_ITEM_SCORE']] = array("id_elemento" => $row['ID_ITEM_SCORE'], "peso" => $row['PESO']);
            }
        }

        $sql = "SELECT
                    sc_item.*,
                    gru.DESCRICAO as dsc,
                    gru.QTD,
                    gru_nead.PESO as peso_subgrupo,
                    gru_nead.NOME

                FROM
                    score_item as sc_item INNER JOIN
                    grupos as gru ON (sc_item.GRUPO_ID = gru.ID) INNER JOIN
                    grupo_nead as gru_nead ON (gru.GRUPO_NEAD_ID=gru_nead.ID)

                WHERE
                    sc_item.SCORE_ID = 3
                ORDER BY
                    sc_item.GRUPO_ID,sc_item.ID";

        $result = mysql_query($sql);
        $cont = 0;

        $i = 0;
        $cont1 = 1;
        $sub_grupo = 1;
        $nome_subtotal = '';
        $peso_subgrupo = 0;
        echo"<tr bgcolor='grey'><td><b>Descri&ccedil;&atilde;o</b></td><td colspan='3'><b>Itens da Avalia&ccedil;&atilde;o</b></td></tr>";
        while ($row = mysql_fetch_array($result)) {
            if ($sub_grupo == $row['peso_subgrupo']) {
                $nome_subtotal = $row['NOME'];
                $sub_grupo = $row['peso_subgrupo'];
                $grupo = "grupo" . $sub_grupo;
            } else {
                echo"<tr bgcolor='#F5F5F5'>
                        <td colspan='3'>
                           Subtotal {$nome_subtotal}
                            <input type='text'  value='{$subtotal}' id='{$grupo}' readonly size='2' />
                        </td>
                   </tr>";
                $nome_subtotal = $row['NOME'];
                $sub_grupo = $row['peso_subgrupo'];
                $grupo = "grupo" . $subgupo;
            }



            if (is_array($elemento)) {
                if (array_key_exists($row['ID'], $elemento)) {
                    $check = "checked='checked'";
                    $total_subgrupo1 = $row['peso_subgrupo'] == 1 ? $total_subgrupo1 + ($row['PESO'] * $row['peso_subgrupo']) : $total_subgrupo1 + 0;
                    $total_subgrupo2 = $row['peso_subgrupo'] == 2 ? $total_subgrupo2 + ($row['PESO'] * $row['peso_subgrupo']) : $total_subgrupo2 + 0;
                    $total_subgrupo3 = $row['peso_subgrupo'] == 3 ? $total_subgrupo3 + ($row['PESO'] * $row['peso_subgrupo']) : $total_subgrupo3 + 0;
                } else {
                    $check = '';
                }
            } else {
                $check = '';
            }

            if ($row['peso_subgrupo'] == 1) {
                $subtotal = $total_subgrupo1;
            } else if ($row['peso_subgrupo'] == 2) {
                $subtotal = $total_subgrupo2;
            } else if ($row['peso_subgrupo'] == 3) {
                $subtotal = $total_subgrupo3;
            }

            if ($cont1 == 1) {
                $i = $row['QTD'] + 1;
                $cont = $cont1;
            } else {
                $i = 0;
            }

            if ($cont1 == 1) {
                if ($x == "#E9F4F8")
                    $x = '';
                else
                    $x = "#E9F4F8";
                echo "<tr bgcolor='{$x}'><td ROWSPAN='{$i}'>({$row['NOME']}) {$row['dsc']}</td></tr>";
            }
            echo "<tr bgcolor='{$x}' >
                            <td colspan='2'>";
            if($disabled == ''){
                echo "<span class='limpar_nead' title='Desmarcar'>
                     <img src='../utils/desmarcar16x16.png' width='16' title='Desmarcar' border='0'>
                   </span>";
                   }
            echo "<input type='radio'  {$check} class='nead' {$disabled} grupo='$nome_subtotal' fator='{$sub_grupo}'  name='" . $row['dsc'] . "' value='" . $row['PESO'] . "' cod_item='" . $row['ID'] . "' >
                                {$row['DESCRICAO']} - Peso: {$row['PESO']}
                            </td>
                        </tr>";


            $cont1++;

            if ($cont1 > $row['QTD'])
                $cont1 = 1;
        }
        echo"<tr bgcolor='#F5F5F5'>
                <td colspan='3'>
                   Subtotal {$nome_subtotal}
                   <input type='text'  readonly size='2' value='{$subtotal}' id='{$grupo}'/>
                </td>
            </tr>";
        $total_score_nead = $total_subgrupo1 + $total_subgrupo2 + $total_subgrupo3;
        $val_score = '';
        if (!empty($id)) {
            if ($total_score_nead < 8) {
                $justificativa = "Sem indicação para Internação Domiciliar.";
            } else if ($total_score_nead >= 8 && $total_score_nead <= 15) {
                $justificativa = "Internação Domiciliar com visita de enfermagem.";
            } else if ($total_score_nead >= 16 && $total_score_nead <= 20) {
                $justificativa = "Internação Domiciliar com até 6 horas de enfermagem.";
            } else if ($total_score_nead >= 21 && $total_score_nead <= 30) {
                $justificativa = "Internação Domiciliar com até 12 horas de enfermagem.";
            } else if ($total_score_nead > 30) {
                $justificativa = "Internação Domiciliar com até 24 horas de enfermagem.";
            }
            $val_score = "Score do paciente segundo tabela NEAD=" . $total_score_nead . ". " . $justificativa;
        }
        echo"<tr bgcolor='grey'>
                <td colspan='3' aling='center'>
                  <b>Total do Score Segundo a Tabela NEAD</b>
                  <input type='text' readonly size='2' value='{$total_score_nead}' id='total_score_nead' />
                  <label id='justificativa_nead'>{$val_score}</label>
                </td>
            </tr>";
    }

    public function visualizarScoreNead($id, $tipo, $paciente_id) {


        if (!empty($id)) {
            if ($tipo == 0) {//Avulso
                $sql = "SELECT
                     sa.justificativa,
                     sa.OBSERVACAO,
                     sa.ID_PACIENTE,
                     si.DESCRICAO ,
                     si.PESO ,
                     si.GRUPO_ID ,
                     si.ID ,
                     sc.id as tbplano,
                     DATE(sa.DATA) as DATA,
                     grup.DESCRICAO as grdesc,
                     pl.nome as plano,
                     cli.nome as paciente,
                     gr_nead.PESO as fatorial
                     FROM score_avulso sa
                                     LEFT OUTER JOIN score_item si ON sa.ID_ITEM_SCORE = si.ID
                                     LEFT OUTER JOIN grupos grup ON si.GRUPO_ID = grup.ID
                                     LEFT OUTER JOIN score sc ON si.SCORE_ID = sc.ID
                                     INNER JOIN clientes as cli ON sa.ID_PACIENTE=cli.idClientes
                                     INNER JOIN planosdesaude as pl ON cli.convenio=pl.id
                                     Inner join grupo_nead as gr_nead on grup.GRUPO_NEAD_ID=gr_nead.ID
                     WHERE sa.ID_AVULSO = '{$id}' and sa.ID_PACIENTE = '{$paciente_id}'";


                $result = mysql_query($sql);
            } else if ($tipo == 1) {//Capmedica
                $sql = "SELECT
                                 sc_pac.OBSERVACAO,
                                 sc_pac.ID_PACIENTE,
                                 sc_item.DESCRICAO ,
                                 sc_item.PESO ,
                                 sc_item.GRUPO_ID ,
                                 sc_item.ID ,
                                 sc.id as tbplano,
                                 cap.justificativa,
                                 DATE(sc_pac.DATA) as DATA,
                                 grup.DESCRICAO as grdesc,
                                 pl.nome as plano,
                                 cli.nome as paciente,
                                 grup_nead.PESO as fatorial,
                                 user.nome  as medico,
                                 cpf
                     FROM
                        score_paciente sc_pac
                                 LEFT OUTER JOIN score_item sc_item ON sc_pac.ID_ITEM_SCORE = sc_item.ID
                                 LEFT OUTER JOIN capmedica cap ON sc_pac.ID_CAPMEDICA = cap.id
                                 LEFT OUTER JOIN grupos grup ON sc_item.GRUPO_ID = grup.ID
                                 LEFT OUTER JOIN score sc ON sc_item.SCORE_ID = sc.ID
                                 INNER JOIN clientes as cli ON sc_pac.ID_PACIENTE=cli.idClientes
                                 INNER JOIN planosdesaude as pl ON cli.convenio=pl.id
                                 INNER JOIN grupo_nead as grup_nead ON grup.GRUPO_NEAD_ID= grup_nead.ID
                                 INNER JOIN usuarios as user ON cap.usuario=user.idUsuarios
                     WHERE
                                 sc_pac.ID_CAPMEDICA = '{$id}' and
                                 grup.GRUPO_NEAD_ID IS NOT NULL";

                $result = mysql_query($sql);
            }

            $resultado_consulta = mysql_num_rows($result);
            if ($resultado_consulta > 0) {
                $cont == 0;
                while ($row = mysql_fetch_array($result)) {

                    if ($cont == 0) {
                        $html .= "<table style='width:95%;'>
                                 <tr  bgcolor='grey'>
                                      <td colspan='3'>
                                        <center>
                                          <b>
                                             TABELA DE AVALIAÇÃO PARA INTERNAÇÃO DOMICILIAR-NEAD<sup>6</sup>.
                                          </b>
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          <b>
                                             Efetuada em:
                                          </b>
                                          " . join("/", array_reverse(explode("-", $row['DATA']))) . "
                                         </center>
                                      </td>
                                  </tr>
                                  <tr >
                                      <td colspan='3'>
                                          <b>
                                           PACIENTE:
                                          </b>
                                           {$row['paciente']}
                                      </td>
                                  </tr>
                                  <tr style='background-color:#EEEEEE;'>
                                      <td colspan='3'>
                                          <b>
                                           CONVÊNIO:
                                          </b>
                                          {$row['plano']}
                                          &nbsp;&nbsp;
                                         <b> CPF: </b> {$row['cpf']}
                                        </td>

                                  </tr>

                                   <tr >
                                      <td colspan='3'>
                                          <b>
                                           MÉDICO RESPONSÁVEL:
                                          </b>
                                           {$row['medico']}
                                      </td>
                                  </tr>
                                  <tr bgcolor='grey'>
                                     <td>
                                        <b>Descrição</b>
                                     </td>
                                     <td>
                                       <b>Itens Avaliação</b>
                                     </td>
                                     <td>
                                        <b>Peso</b>
                                     </td>
                                  </tr>
                                  ";
                    }
                    $cont++;


                    $html .="<tr>
                             <td>{$row['grdesc']}</td>
                             <td>{$row['DESCRICAO']}</td>
                             <td>{$row['PESO']}x{$row['fatorial']}</td>
                        ";

                    $total_score_nead = $total_score_nead + ($row['PESO'] * $row['fatorial']);
                    $justificativa = $row['justificativa'];
                }
                $html .="<tr bgcolor='grey'>
                         <td colspan='3'>
                           <b>TOTAL DO SCORE SEGUNDO A TABELA NEAD:</b>
                            $total_score_nead
                         </td>
                       </tr>
                       <tr style='background-color:#EEEEEE;'>
                         <td colspan='3'>
                           <b>Justificatica:</b>
                         </td>
                       </tr>
                       <tr>
                         <td>
                          $justificativa
                         </td>
                       </tr>
           </table>";
            } else {
                $html .="<p>
                   <center>
                    <b>
                      Não foi encontrado o score segundo a tabela Nead.
                    </b>
                   </center>
                 </p>";
            }
        }
        return $html;
    }

    public function ficha_avaliacao_enfermagem($fae, $acesso, $dados) {

        echo "<center><h1>Nova Avalia&ccedil;&atilde;o de Enfermagem</h1></center>";

        $this->cabecalho($fae);
        $this->plan;

        echo "<div id='div-ficha-avaliacao-enfermagem'><br/>";

        echo alerta();
        echo "<form>";
        echo "<input type='hidden' name='paciente'  value='{$fae->pacienteid}' />";
        echo "<input type='hidden' id='fromRelatorio'  value='avaliacao_enfermagem' />";

        echo "<br/>";

        /* ANAMNESE */

        echo "<table class='mytable' style='width:95%;'>";

//        echo "<select name='dialog-feridas' class='botao-feridas' title='Feridas' cod='{$fae->pacienteid}'><option value='-1'>SELECIONE</option><option value='1'>Sim</option><option value='2'>N&atilde;o</option></select>";
//        echo "<div id='div_feridas' title='Feridas' cod=''></div>";
        echo "<thead><tr><th colspan='4' >Anamnese</th></tr></thead>";
        echo"<tr><td colspan='4' style='width:33%;'><label><b>Tipo Tratamento:</b></tr></td>";
        echo"<tr><td colspan='2'><input type='checkbox' value='1' name='clinico' class='anamnese OBG_RADIO'><label>Cl&iacute;nico</label></td>
             <td><input type='checkbox' value='1'name='cirurgico' class='anamnese OBG_RADIO'><label>Cir&uacute;rgico</label></td>
             <td><input type='checkbox' value='1' name='paliativos' class='anamnese OBG_RADIO'><label>Cuidados Paliativos</label></td></tr>";
        /* Atualiza��o Eriton 14/03/2013 */
        echo"<tr><td colspan = '4'><label><b>Diagn&oacute;stico M&eacute;dico Principal que o levou &agrave; Hospitaliza&ccedil;&atilde;o:</b></label><br/><textarea cols=90 rows=2 name='diagnostico_principal'   class='OBG' {$acesso} ></textarea></td></tr>";
        echo"<tr><td colspan = '4'><label><b>Hist&oacute;ria Pregressa:</b></label><br/><textarea cols=90 rows=2 name='hist_pregressa'   class='OBG' {$acesso} ></textarea></td></tr>";
        echo"<tr><td colspan = '4'><label><b>Hist&oacute;ria Familiar:</b></label><br/><textarea cols=90 rows=2 name='hist_familiar'   class='OBG' {$acesso} ></textarea></td></tr>";
        echo"<tr><td colspan = '4'><label><b>Observa&ccedil;&otilde;es:</b></label><br/><textarea cols=90 rows=2 name='queixas_atuais' {$acesso} ></textarea></td></tr>";
        /* Atualiza��o Eriton 14/03/2013 */
        echo "</table>";

        echo "<input type='hidden' name='capmed'  value='{$y}' />";

        /* MEDICACOES EM USO */
        echo "<table class='mytable' id='medicamentos_ativos' style='width:95%;'>";
        echo "<thead><tr><th colspan='5' >Medica&ccedil;&otilde;es em uso </th></tr></thead>";
        echo"<tr><td colspan='5'><b>Medica&ccedil;&atilde;o.</b><span class='outro_medicamento_ativo' ><input type='text' size='110' name='medicamento_ativo' class='med_ativo' {$acesso} size=75/><button id='add-medicamento-ativo' tipo=''  >+ </button></span></td></tr>";
         echo "</table>";

        /* HISTORIA ATUAL */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Hist&oacute;ria Atual</th></tr></thead>";
        echo"<tr><td colspan =4><textarea cols=90 rows=4 name='hist_atual' class='OBG' {$acesso} ></textarea></td></tr>";
        echo "</table>";

        echo "<br/>";

        /*         * ************************* EXAME FISICO ************************************ */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<tr style='background-color:#ccc;'><td colspan='4' ><b><center>Exame F&iacute;sico</center></b></td></tr>";
        echo "</table>";

        echo "</br>";

        /* ESTADO GERAL */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan =4> <b>Estado Geral</b></th></thead>";

        echo"<tr><td style='width:25%;'><input type='checkbox' value='1' name='bom' class='estado_geral OBG_RADIO'>Bom</td> <td style='width:25%;'><input type='checkbox' value='1'name='ruim'>Ruim</td><td style='width:25%;'><input type='checkbox' value='1' name='nutrido' class='estado_geral OBG_RADIO'>Nutrido</td><td style='width:25%;'><input type='checkbox' value='1' name='desnutrido' class='estado_geral OBG_RADIO'> Desnutrido</td></tr>";
        echo "<tr><td style='width:25%;'> <input type='checkbox' value='1' name='higiene_corpo_s' class='estado_geral OBG_RADIO'>Higiene corporal satisfat&oacute;ria</td><td style='width:25%;'><input type='checkbox' value='1' name='higiene_corpo_i' class='estado_geral OBG_RADIO'>Higiene corporal insatisfat&oacute;ria</td><td style='width:25%;'><input type='checkbox' value='1'name='higiene_bucal_s' class='estado_geral OBG_RADIO'>Higiene bucal satisfat&oacute;ria</td><td style='width:25%;'><input type='checkbox' value='1' name='higiene_bucal_i' class='estado_geral OBG_RADIO'>Higiene bucal insatisfat&oacute;ria</td></tr>";
        echo "</tr>";
        echo "</table>";

        /* ESTADO EMOCIONAL */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan =4> <b>Estado Emocional</b></th></tr></thead>";
        echo "<tr><td style='width:25%;'><input type='radio' value='1' name='estado_emocional' class='estado_emocional OBG_RADIO'>Calmo </td><td style='width:25%;'><input type='radio' value='2' name='estado_emocional' class='estado_emocional OBG_RADIO'>Agitado</td><td style='width:25%;'><input type='radio' value='3' name='estado_emocional' class='estado_emocional OBG_RADIO'>Deprimido</td><td style='width:25%;'><input type='radio' value='4' name='estado_emocional' class='estado_emocional OBG_RADIO'>Choroso</td></tr>";
        echo "</table>";

        /* NIVEL DE CONSCIENCIA */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan =4> <b>N&iacute;vel de Consci&ecirc;ncia</b></th></thead>";
        echo"<tr><td style='width:25%;'><input type='checkbox' value='1' name='consciente' class='nivel_consciencia OBG_RADIO'>Consciente</td>";
        echo "<td style='width:25%;'><input type='checkbox' value='1' name='sedado' class='nivel_consciencia OBG_RADIO'>Sedado</td>";
        echo "<td style='width:25%;'><input type='checkbox' value='1' name='obnublado' class='nivel_consciencia OBG_RADIO'>Obnubilado</td>";
        echo "<td style='width:25%;'><input type='checkbox' value='1' name='torporoso' class='nivel_consciencia OBG_RADIO'>Torporoso</td></tr>";

        echo"<tr><td style='width:25%;'><input type='checkbox' value='1' name='comatoso' class='nivel_consciencia OBG_RADIO'>Comatoso</td>";
        echo "<td style='width:25%;'><input type='checkbox' value='1' name='orientado' class='nivel_consciencia OBG_RADIO'>Orientado</td>";
        echo "<td style='width:25%;'><input type='checkbox' value='1' name='desorientado' class='nivel_consciencia OBG_RADIO'>Desorientado</td>";
        echo "<td style='width:25%;'><input type='checkbox' value='1' name='sonolento' class='nivel_consciencia OBG_RADIO'>Sonolento</td></tr>";
        echo "</table>";


        /* CONDI��O MOTORA */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan =4> <b>Condi&ccedil;&atilde;o Motora</b></th></thead>";

        echo "<tr><td style='width:25%;'><span ><input type='radio' value='1' class='cond_motora OBG_RADIO' name='cond_motora'>Deambula</span></td><td style='width:25%;'><span ><input type='radio' value='2' class='cond_motora OBG_RADIO' name='cond_motora'>Deambula com Aux&iacute;lio</span></td><td style='width:25%;'><span ><input type='radio' value='3' class='cond_motora OBG_RADIO' name='cond_motora'>Cadeirante</span></td>";
        echo "<td style='width:25%;'><span ><input type='radio' value='4' name='cond_motora' class='cond_motora OBG_RADIO'>Acamado</span></td></tr>";
        echo "</table>";

        /* ALERGIA MEDICAMENTOSA E ALERGIA ALIMENTAR */
        echo "<table class='mytable' style='width:95%;'>";
        echo"<thead><tr><th colspan='3'>Alergia medicamentosa (S/N) <input type='text' class='boleano alergia OBG' name='palergiamed' value='{$dados->alergiamed}' {$acesso} /></th>";
        echo "<th >Alergia alimentar (S/N) <input size='50' type='text' class='boleano alergia OBG' name='palergiaalim' value='{$dados->alergiaalim}' {$acesso} /></th>";
        echo "</tr></thead>";
        //if($alergiamed == 's'){
        echo "<tr><td colspan='3'><input type='text'  name='alergiamed' disabled size='50'  value='{$dados->tipoalergiamed}' /></td>";
        //}
        //if($alergiaalim == 's'){
        echo "<td><input type='text' name='alergiaalim' size='50' disabled value='{$dados->tipoalergiaalim}' /></td></tr>";
        //}
        echo "</table>";

        /* PELE  Atualiza��o Eriton 28-03-2013 */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3' >Pele</th></thead>";
        if ($icterica < '2') {
            $show_icterica = "style='display:inline;'";
        } else {
            $show_icterica = "style='display:none'";
        }
        echo"<tr><td style='width:33%;'><span><input type='radio' value='1' name='integra_lesao' class='pele_integra_lesao OBG_RADIO'>&Iacute;ntegra</span></td><td style='width:33%;'><span><input type='radio' value='1' name='icterica_anicterica' class='pele_icterica_anicterica OBG_RADIO'>Ict&eacute;rica </span><span class='icterica_escala' $show_icterica><b>Escala:</b><input type='text' name='icterica_escala' class='icterica_escala' size='5'></span></td><td style='width:33%;'><span><input type='radio' value='1' name='hidratada_desidratada' class='pele_hidratada_desidratada OBG_RADIO'>Hidratada</span></td></tr>";
        echo"<tr><td style='width:33%;'><span><input type='radio' value='2' name='integra_lesao' class='pele_integra_lesao OBG_RADIO'>Lesionada</span></td><td style='width:33%;'><span><input type='radio' value='2' name='icterica_anicterica' class='pele_icterica_anicterica OBG_RADIO'>Anict&eacute;rica</span></td><td style='width:33%;'><span><input type='radio' value='2' name='hidratada_desidratada' class='pele_hidratada_desidratada OBG_RADIO'>Desidratada</span></td></tr>";
        echo"<tr><td style='width:33%;'><b>P&aacute;lida: </b></td><td colspan='2'><input type='radio' value='1' name='palida' class='palida OBG_RADIO'>Sim <input type='radio' value='2' name='palida' class='palida OBG_RADIO'>N&atilde;o</tr></td>";
        //echo"<tr><td ><b>Les&otilde;es: </b></td><td><span class='valida_pele'><input type='radio' value='1' name='lesoes'>Sim <input type='radio' value='2' name='lesoes'>N&atilde;o</tr></td>";
        echo"<tr><td > <b>Sudoreica: </b></td><td colspan='2'><input type='radio' value='1' name='sudoreica' class='sudoreica OBG_RADIO'>Sim <input type='radio' value='2' name='sudoreica' class='sudoreica OBG_RADIO'>N&atilde;o</tr></td>";
        echo"<tr><td ><b>Fria: </b></td><td colspan='2'><input type='radio' value='1' name='fria' class='fria OBG_RADIO'>Sim <input type='radio' value='2' name='fria' class='fria OBG_RADIO'>N&atilde;o</tr></td>";
        echo"<tr><td ><b>Edema: </b></td><td colspan='2'><input type='radio' value='1' name='pele_edema' class='pele_edema OBG_RADIO'>Sim <input type='radio' value='2' name='pele_edema' class='pele_edema OBG_RADIO'>N&atilde;o</tr></td>";
        echo"<tr><td ><b>Descama&ccedil;&atilde;o: </b></td><td colspan='2'><input type='radio' value='1' name='descamacao' class='descamacao OBG_RADIO'>Sim <input type='radio' value='2' name='descamacao' class='descamacao OBG_RADIO'>N&atilde;o</tr></td>";
        echo "</table>";

        /* AVALIA��O CARDIORESPIRAT�RIA */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3' >Avalia&ccedil;&atilde;o CardioRespirat&oacute;ria</th></thead>";
        echo"<tr><td style='width:33%;'> <input type='checkbox' value='1' name='t_assimetrico' class='cardiorespiratoria OBG_RADIO'>T&oacute;rax Assim&eacute;trico </td><td style='width:33%;'> <input type='checkbox' value='1' name='t_simetrico' class='cardiorespiratoria OBG_RADIO'>T&oacute;rax Sim&eacute;trico </td><td style='width:33%;'> <input type='checkbox' value='1' name='eupneico' class='cardiorespiratoria OBG_RADIO'>Eupneico </td></tr>";
        echo "<tr><td style='width:33%;'><input type='checkbox' value='1' name='dispneico' class='cardiorespiratoria OBG_RADIO'>Dispneico</td>";
        echo "<td style='width:33%;'><input type='checkbox' value='1' name='taquipneico' class='cardiorespiratoria OBG_RADIO'>Taquipneico</td>";
        echo "<td style='width:33%;'><input type='checkbox' value='1' name='mvbd' class='cardiorespiratoria OBG_RADIO'>MVBD </td></tr>";
        echo "<tr> <td style='width:33%;'><input type='checkbox' value='1' name='roncos' class='cardiorespiratoria OBG_RADIO'>Roncos </td>";
        echo "<td style='width:33%;'><input type='checkbox' value='1' name='sibilos' class='cardiorespiratoria OBG_RADIO'>Sibilos </td>";
        echo "<td style='width:33%;'><input type='checkbox' value='1' name='creptos' class='cardiorespiratoria OBG_RADIO'>Creptos </td></tr>";
        echo "<tr><td style='width:33%;'><input type='checkbox' value='1' name='mv_diminuido' class='cardiorespiratoria OBG_RADIO'>MV Diminuido </td><td style='width:33%;'><input type='checkbox' value='1' name='bcnf' class='cardiorespiratoria OBG_RADIO'>BCNF</td>";
        echo "<td style='width:33%;'><input type='checkbox' value='1' name='normocardico' class='cardiorespiratoria OBG_RADIO'>Normocardico </td></tr>";
        echo "<tr><td style='width:33%;'><input type='checkbox' value='1' name='taquicardico' class='cardiorespiratoria OBG_RADIO'>Tarquicardico </td>";
        echo "<td style='width:33%;' colspan='2'> <input type='checkbox' value='1' name='bradicardico' class='cardiorespiratoria OBG_RADIO'>Bradicardico </td></tr>";
        echo "</table>";

        /* TIPO DE VENTILA��O Atualiza��o Eriton 28-03-2013 */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='2' >Tipo de Ventila&ccedil;&atilde;o</th></thead>";
        echo "<tr><td colspan='2'><input type='radio' value='1' name='tipo_ventilacao' class='tipo_ventilacion OBG_RADIO'>Espont&acirc;nea<input type='radio' value='2' name='tipo_ventilacao' class='tipo_ventilacion OBG_RADIO'>Traqueostomia</td></tr>";
        if ($ventilacao >= '2') {
            $show_ventilacion = "style='display:table'";
        } else {
            $show_ventilacion = "style='display:none'";
        }
        echo "<tr><td><span class='ventilacion' $show_ventilacion>Tipo de dispositivo: <select name = 'tipo_ventilacao1' id='tipo_ventilacao1'>
				<option value='-1'>  </option>
				<option value='2'>Metalica</option>
				<option value='3'>Plastica</option></select></span>";
        echo"<td width='70%'><span class='ventilacion' $show_ventilacion><b>N&ordm; </b>   <input type='text' maxlength='5' size='5' name='num_ventilacao' id='num_ventilacao'> </td></span></tr>";
        echo "<tr><td><span class='ventilacion' $show_ventilacion>Modo de Ventila&ccedil;&atilde;o: <select name = 'tipo_ventilacao2' id='tipo_ventilacao2' class='tipo_ventilacao2'>
				<option value='-1'>  </option>
				<option value='1'>Espontanea</option>
				<option value='2'>Equipamento</option></select></span></td>";
        echo "<td><span class='bipap'><b>BIPAP</b> <input type='radio' value='1' name='bipap' class='vm_bipap'>Cont&iacute;nuo <input type='radio' value='2' name='bipap' class='vm_bipap'>Intermitente</span></td></tr>";
        echo "</table>";


        /* OXIGENIO */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Oxig&ecirc;nio</th></tr></thead>";
        echo"<tr><td colspan='4'><b>Oxig&ecirc;nio</b>
				<select name='oxigenio_sn' class='oxigenio_sn COMBO_OBG'><option value ='-1'>  </option>
				<option value ='n'>N&atilde;o</option>
				<option value ='s'>Sim</option>
				</select></tr></td>";
        echo "</table>";
        //ITENS DO OXIGENIO

        echo "<table class='mytable' id='oxigenio_span' style='width:95%;display:none'>";
        echo "<thead><tr><th colspan='4' >Itens do Oxig&ecirc;nio</th></tr></thead>";

        echo "<tr><td colspan='3'><input type='radio' value='1' class='item_oxigenio' name='oxigenio'>Cateter de Oxig&ecirc;nio</td><td>N&ordm; <input type = 'text'  name='vent_cateter_oxigenio_num' size = '5' maxlength='5' >&nbsp;&nbsp;&nbsp;<input type = 'text'  name='vent_cateter_oxigenio_quant' size = '5' maxlength='5' >L/MIN </td></tr>";
        echo "<tr><td colspan='3'><input type='radio' value='2' class='item_oxigenio' name='oxigenio'>Cateter tipo &Oacute;culos </td><td><input type='text' maxlength='5' size='5' name='vent_cateter_oculos_num'>L/MIN</td></tr>";
        echo "<tr><td colspan='3'><input type='radio' value='3' class='item_oxigenio' name='oxigenio'>M&aacute;scara Reservat&oacute;rio</td><td><input type='text'  maxlength='5' size='5' name='vent_respirador_tipo'>L/MIN</tr></td>";
        echo "<tr><td colspan='3'><input type='radio' value='4' class='item_oxigenio' name='oxigenio'>Mascara de Ventury </td><td><input type='text' maxlength='5' size='5' name='vent_mascara_ventury'>% </td></tr>";
        echo "<tr><td colspan='3'><input type='radio' value='5' class='item_oxigenio' name='oxigenio'>Concentrador </td><td><input type='text'  maxlength='5' size='5' name='vent_concentrador_num'>L/MIN</tr></td>";
        echo "</table>";
///20-10-2014

        /* ASPIRA��ES */

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><th colspan='4' >Aspira&ccedil;&otilde;es - <select name='alt_aspiracoes' class='alt_aspiracoes COMBO_OBG'>
                        <option value ='-1'>  </option>
                        <option value ='n'>N&atilde;o</option>
                        <option value ='s'>Sim</option>
                        </select></th></thead>";
        echo"<tr class='aspiracoes_span' style='display:none'><td colspan='4'>N&uacute;mero de Aspira&ccedil;&otilde;es/Dia:<input type='text' maxlength='5' size='5' name='aspiracoes_num' class='aspiracoes_num'></td></tr>";
        echo"<tr class='aspiracoes_span' style='display:none'><td colspan='4'><label><b>Secre&ccedil;&atilde;o</b></label>
	  	<select name= 'aspiracao_secrecao' id='aspiracao_secrecao'><option value ='-1'>  </option>
				<option value ='a'>Ausente</option>
				<option value ='p'>Presente</option>
				</select></tr></td>";

        //CARACTERISTICAS DA SECRE��O Atualiza��o Eriton 17-04-2013

        echo "<tr><td colspan='4' >";
        //echo "<table class='secrecao_span' style='width:100%;'>";

        echo"<tr class='secrecao_span' style='display:none'><td colspan = '3' width='30%'><b>Caracter&iacute;sticas da secre&ccedil;&atilde;o</b></td></tr>";
        echo "<tr class='secrecao_span' style='display:none'><td colspan = '3'><input type='radio' value='6' name='caracteristicas_sec' class='caracteristicas_sec'>Fluida </td>";
        echo "<td colspan = '3'><input type='radio' value='1' name='cor_sec' class='cor_sec'>Clara </td></tr>";
        echo "<tr class='secrecao_span' style='display:none'><td colspan = '3'><input type='radio' value='2' name='caracteristicas_sec' class='caracteristicas_sec'>Espessa </td>";
        echo "<td colspan = '3'><input type='radio' value='3' name='cor_sec' class='cor_sec'>Amarelada </tr></td>";
        echo "<tr class='secrecao_span' style='display:none'><td colspan = '3'> </td>";
        echo "<td colspan = '3'><input type='radio' value='4' name='cor_sec' class='cor_sec'> Esverdeada</td></tr>";
        echo "<tr class='secrecao_span' style='display:none'><td colspan = '3'> </td>";
        echo "<td colspan = '3'><input type='radio' value='5' name='cor_sec' class='cor_sec'> Sanguinolenta</tr></td>";

        //echo "</table>";
        echo "</td></tr>";

        echo "</table>";
        //Fim Atualiza��o Eriton 17-04-2013
        ////AVALIACAO ABDOMINAL////// Atualiza��o Eriton 28-03-2013

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Avalia&ccedil;&atilde;o Abdominal</th></thead>";

        echo "<tr> <td style='width:25%;'> <input type='checkbox' value='1' name='plano' class='avaliacao_abdominal OBG_RADIO'>Plano </td>
				<td style='width:25%;'> <input type='checkbox' value='1' name='flacido' class='avaliacao_abdominal OBG_RADIO'>Flacido </td>
				<td style='width:25%;'> <input type='checkbox' value='1' name='globoso' class='avaliacao_abdominal OBG_RADIO'>Globoso </td>
				<td style='width:25%;'> <input type='checkbox' value='1' name='distendido' class='avaliacao_abdominal OBG_RADIO'>Distendido </td>
				</tr>";

        echo "<tr> <td style='width:25%;'>RHA <select name='rha' id= 'rha' class='COMBO_OBG'><option value='-1'>  </option>
				<option value='p'>Presente</option>
				<option value='a'>Ausente</option></select> </td>
				<td style='width:25%;'> <input type='checkbox' value='1' name='indo_palpacao' class='avaliacao_abdominal OBG_RADIO'>Indolor a Palpa&ccedil&atilde;o </td>
				<td style='width:25%;'> <input type='checkbox' value='1' name='doloroso' class='avaliacao_abdominal OBG_RADIO'>Doloroso </td>
				<td style='width:25%;'> <input type='checkbox' value='1' name='timpanico' class='avaliacao_abdominal OBG_RADIO'>Timp&acirc;nico </td>
				</tr>";

        echo "</table>";

        /* VIA ALIMENTA��O */
        $this->dietas($id,$v,1);
        $this->itensDietas($id,$v,1);

        //avaliacao genital

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Avalia&ccedil;&atilde;o Genital</th></thead>";

        echo "<tr><td style='width:25%;'> &Iacute;ntegra </td> <td   colspan='3' style='width:75%;'><input type = 'radio' value='1' name= 'integra_genital' class='integra OBG_RADIO'>Sim <input type = 'radio' value='2' name= 'integra_genital' class='integra OBG_RADIO'> N&atilde;o </tr></td>";
        echo "<tr><td style='width:25%;'> Secre&ccedil;&atilde;o </td> <td   colspan='3' style='width:75%;'><input type = 'radio' value='1' name= 'secrecao' class='secrecao OBG_RADIO'>Sim <input type = 'radio' value='2' name= 'secrecao' class='secrecao OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "<tr><td style='width:25%;'> Sangramento </td> <td   colspan='3' style='width:75%;'><input type = 'radio' value='1' name= 'sangramento' class='sangramento OBG_RADIO'>Sim <input type = 'radio' value='2' name= 'sangramento' class='sangramento OBG_RADIO'> N&atilde;o </tr></td>";
        echo "<tr><td style='width:25%;'> Prurido </td> <td   colspan='3' style='width:75%;'><input type = 'radio' value='1' name= 'prurido' class='prurido OBG_RADIO'>Sim <input type = 'radio' value='2' name= 'prurido' class='prurido OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "<tr><td style='width:25%;'> Edema </td> <td   colspan='3' style='width:75%;'><input type = 'radio' value='1' name= 'edema' class='edema OBG_RADIO'>Sim <input type = 'radio' value='2' name= 'edema' class='edema OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "</table>";

        /* T�PICOS EM USO */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Topicos em Uso</th></thead>";
        echo"<tr><td colspan='4' >
				<b>Topicos: </b>
				<select name='topicos_sn' class='COMBO_OBG' id='topicos_sn'><option value ='-1'>  </option>
				<option value ='n'>N&atilde;o</option>
				<option value ='s'>Sim</option>
				</select>


	   <span name='topicos' id='topicos_span'><input type='text' size='110' name='topico' class='topico_add'size=75/><button id='add-topicos' tipo=''  >+ </button></span></td></tr>";

        //	  ITENS DE topicos
        echo "<tr><td colspan='4'>";
        echo "<table id='topico' style='width:100%;'>";
        if (!empty($ev->topico)) {
            foreach ($ev->topico as $a) {
                echo "<tr><td colspan='4'>{$a}</td></tr>";
            }
        }
        echo "</table>";
        echo "</td></tr>";

        echo "</table>";

        $this->dispositivosIntravenosos();


        /* DISPOSITIVOS PARA ELIMINA��ES E/OU EXCRECOES Atualizacao Eriton 28-03-2013 */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Dispositivos para elimina&ccedil;&atilde;o e/ou excre&ccedil;&otilde;es <select name='eliminaciones' class='eliminaciones COMBO_OBG' ><option value='-1'>   </option><option value='1'> N&atilde;o </option><option value='2'>Sim</option></select></th></thead>";
        if ($eliminaciones == '2') {
            $show_eliminaciones = '';
        } else {
            $show_eliminaciones = "style='display:none' ";
        }
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td style='width:33%;'> <input type ='checkbox'  class='excrecoes' value='1' " . (($fralda) ? "checked" : "") . " {$enabled} name='fralda' >Fralda Unidade </td><td><input type = 'text' size='5' value='{$fralda_dia}' {$acesso} maxlength='5' name='fralda_dia'> Dia</td><td  colspan='2'>
		Tamanho:<input type ='radio'  value='1' " . (($tamanho_fralda == '1') ? "checked" : "") . " {$enabled}  name='tamanho_fralda' >P <input type = 'radio'  value='2' " . (($tamanho_fralda == '2') ? "checked" : "") . " {$enabled}   name='tamanho_fralda' >M <input type = 'radio'  value='3' " . (($tamanho_fralda == '3') ? "checked" : "") . " {$enabled}  name='tamanho_fralda' >G <input type ='radio'  value='4' " . (($tamanho_fralda == '4') ? "checked" : "") . " {$enabled}   name='tamanho_fralda' >XG</td></tr>";
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td style='width:33%;'> <input type = 'checkbox'  class='excrecoes' value='1' " . (($urinario) ? "checked" : "") . " {$enabled}   name='urinario' >Dispositivo urin&aacute;rio </td><td colspan='3'>N&ordm;<input type = 'text' size='5' value='{$quant_urinario}' {$acesso} maxlength = '5'   name='quant_urinario' >  </td></tr>";
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td>  <input type = 'checkbox'  class='excrecoes' value='1' " . (($sonda) ? "checked" : "") . " {$enabled}   name='sonda' >Sonda de Foley </td><td colspan='3'>N&ordm;<input type = 'text' size='5' value='{$quant_foley}' {$acesso} maxlength='5' name='quant_foley' >  </td></tr>";
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td><input type = 'checkbox'  class='excrecoes' value='1' " . (($sonda_alivio) ? "checked" : "") . " {$enabled}   name='sonda_alivio' >Sonda de Al&iacute;vio </td><td colspan='3'>N&ordm;<input type = 'text' size='5' maxlength='5' value='{$quant_alivio}' {$acesso} name='quant_alivio' > </td></tr>";
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td><input type = 'checkbox'  class='excrecoes' value='1' " . (($colostomia) ? "checked" : "") . " {$enabled}   name='colostomia' >Colostomia Placa/Bolsa</td><td colspan='3'>N&ordm;<input type = 'text' size='5' maxlength='5' value='{$quant_colostomia}' {$acesso} name='quant_colostomia' >  </td></tr>";
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td><input type = 'checkbox'  class='excrecoes' value='1' " . (($iliostomia) ? "checked" : "") . " {$enabled}   name='iliostomia' >Iliostomia Placa/Bolsa</td><td colspan='3'>N&ordm;<input type = 'text' size='5' maxlength='5' value='{$quant_iliostomia}' {$acesso} name='quant_iliostomia' >  </td></tr>";
        echo "<tr class='eliminaciones_span' $show_eliminaciones><td><input type = 'checkbox' class='excrecoes' value='1' " . (($cistostomia) ? "checked" : "") . " {$enabled}  name='cistostomia' >Cistostomia</td><td colspan='3'>N&ordm;<input type = 'text' size='5' name='quant_cistostomia'></td></tr>";
        echo "</table>";

        //Fim Atualiza��o Eriton 28-03-2013

        /* DRENOS */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Drenos</th></thead>";
        echo"<tr><td colspan='4' >
				<b>Drenos: </b>
				<select name='dreno_sn' class='dreno_select COMBO_OBG'><option value ='-1'>  </option>
				<option value ='n'>N&atilde;o</option>
				<option value ='s'>Sim</option>
				</select>
				&nbsp;&nbsp;
				<span name='dreno' class='dreno_span'><b>Tipo:</b><input type='text' name='dreno'  size=50/><button id='add-dreno' tipo=''  > + </button></span></td></tr>";

        /* ITENS DO DRENO */
        echo "<tr><td colspan='4' >";
        echo "<table id='dreno' style='width:100%;'>";
        if (!empty($ev->dreno)) {
            foreach ($ev->dreno as $a) {
                echo "<tr><td>{$a}</td></tr>";
            }
        }
        echo "</table>";
        echo "</td></tr>";

        echo "</table>";



        /* FERIDAS */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Feridas</th></tr></thead>";
        echo"<tr><td colspan='4' ><b>Feridas: </b>
				<select name=feridas_sn class='COMBO_OBG' id='ferida'>
				<option value ='-1'>  </option>
				<option value ='s'>Sim  </option>
				<option value ='n'>N&atilde;o</option>
				</select>

				</td></tr>";

        echo "</table>";
        echo "<span class='quadro_feridas_sn'>";
        echo "<div id='combo-div-quadro-feridas'>";
        $this->feridas();
        echo "</div>";
        echo "<br><button id='nova_ferida' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Nova Ferida</span></button><br><br>";
//        echo "<br><a id='nova_ferida' tipo='' class='incluir' contador='1'>Nova Ferida</a><br>";
        echo "</span>";



        /* SINAIS VITAIS */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><th colspan='4' >Sinais vitais </th></tr></thead>";
        echo "<tr><td style='width:33%;'><b>PA Sist&oacute;lica:</b></td><td colspan='3'><input type='texto' class='numerico1 OBG' size='6' name='pa_sistolica' value='{$ev->pa_sistolica}' {$acesso} />mmhg</td></tr>";
        echo "<tr><td><b>PA Dist&oacute;lica:</b></td><td colspan='3'><input type='texto' class='numerico1 OBG' size='6' name='pa_diastolica' value='{$ev->pa_diastolica}' {$acesso} />MMHG</td></tr>";
        echo "<tr><td><b>FC: </b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='fc' value='{$ev->fc}' {$acesso} />bpm</td></tr>";
        echo "<tr><td><b>FR:</b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='fr' value='{$ev->fr}' {$acesso} />INC/MIN</td></tr>";
        echo "<tr><td><b>SPO&sup2;</b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='spo2' value='{$ev->spo2}' {$acesso} />%</td></tr>";
        echo "<tr><td><b>Temperatura:</b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='temperatura' value='{$ev->temperatura}' {$acesso} />&deg;C</td></tr>";
        echo "</tbale>";


        /* AVALIA��O DA DOR */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4'>Avalia&ccedil;&atilde;o da dor</th></tr></thead>";
        echo "<tr><td colspan='4' ><b>DOR: </b>
            <select name=dor_sn class='COMBO_OBG' id='dor_sn'>
            <option value ='-1'>  </option>
            <option value ='n'>N&atilde;o</option>
            <option value ='s'>Sim</option>
            </select>
            &nbsp;&nbsp;
            <span name='dor_span' id='dor_span'><b>Local:</b><input type='text' name='dor'  size='50' /><button id='add-dor' tipo=''  > + </button></span></td>";
        echo "</tr>";
        echo "<tr><td colspan='4' >";

        /* LISTANDO O QUE FOI SOLICITADO */

        echo "<table id='dor' style='width:100%;'>";

        if ($v == 1) {
            $sql = "select * from avaliacao_enf_dor where `AVALIACAO_ENF_ID` = '{$fae->pacienteid}' ";
            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {
                if ($row['PADRAO_ID'] == 5) {
                    $outrador = "<input type='text' value={$row['PADRAO']}></input>";
                } else {
                    $outrador = "";
                }
                echo "<tr><td class='dor1' name='{$row['LOCAL']}'><img src='../utils/delete_16x16.png' class='del-dor-ativos' title='Excluir' border='0' ><b>Local: </b><input type='text' value={$row['LOCAL']}></input>&nbsp;&nbsp; Escala analogica: <select name='dor" . "{$row['LOCAL']}" . "' class='COMBO_OBG' style='background-color:transparent;'>
                    <option value='-1' selected></option>
                    <option value='1' " . (($row['INTENSIDADE'] == '1') ? "selected" : "") . ">1</option>
                    <option value='2' " . (($row['INTENSIDADE'] == '2') ? "selected" : "") . ">2</option>
                    <option value='3' " . (($row['INTENSIDADE'] == '3') ? "selected" : "") . ">3</option>
                    <option value='4' " . (($row['INTENSIDADE'] == '4') ? "selected" : "") . ">4</option>
                    <option value='5' " . (($row['INTENSIDADE'] == '5') ? "selected" : "") . ">5</option>
                    <option value='6' " . (($row['INTENSIDADE'] == '6') ? "selected" : "") . ">6</option>
                    <option value='7' " . (($row['INTENSIDADE'] == '7') ? "selected" : "") . ">7</option>
                    <option value='8' " . (($row['INTENSIDADE'] == '8') ? "selected" : "") . ">8</option>
                    <option value='9' " . (($row['INTENSIDADE'] == '9') ? "selected" : "") . ">9</option>
                    <option value='10' " . (($row['INTENSIDADE'] == '10') ? "selected" : "") . ">10</option></select> &nbsp;&nbsp;<b>Padr&atilde;o:</b> <select name='padraodor' class='padrao_dor COMBO_OBG' style='background-color:transparent;'>
                    <option value='-1' selected></option>
                    <option value='1' " . (($row['PADRAO_ID'] == '1') ? "selected" : "") . ">Pontada</option>
                    <option value='2' " . (($row['PADRAO_ID'] == '2') ? "selected" : "") . ">Queimação</option>
                    <option value='3' " . (($row['PADRAO_ID'] == '3') ? "selected" : "") . ">Latejante</option>
                    <option value='4' " . (($row['PADRAO_ID'] == '4') ? "selected" : "") . ">Peso</option>
                    <option value='5' " . (($row['PADRAO_ID'] == '5') ? "selected" : "") . ">Outro</option></select></b>{$outrador} </td></tr>";
            }
        } else {
            $sql = "select * from avaliacao_enf_dor where `AVALIACAO_ENF_ID` = '{$fae->pacienteid}' ";
            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {
                if (($row['PADRAO_ID'] == 5) || ($row['PADRAO_ID'] == 0)) {
                    $outrador = "<input type='text' value={$row['PADRAO']}></input>";
                } else {
                    $outrador = "";
                }
                echo "<tr><td class='dor1' name='{$row['LOCAL']}' width='30%'><img src='../utils/delete_16x16.png' class='del-dor-ativos' title='Excluir' border='0' ><b>Local: </b>{$row['LOCAL']}</td><td width='70%' colspan='2'> Escala analogica: <select name='dor" . "{$row['LOCAL']}" . "' class='COMBO_OBG' style='background-color:transparent;'>
                    <option value='-1' selected></option>
                    <option value='1' " . (($row['INTENSIDADE'] == '1') ? "selected" : "") . ">1</option>
                    <option value='2' " . (($row['INTENSIDADE'] == '2') ? "selected" : "") . ">2</option>
                    <option value='3' " . (($row['INTENSIDADE'] == '3') ? "selected" : "") . ">3</option>
                    <option value='4' " . (($row['INTENSIDADE'] == '4') ? "selected" : "") . ">4</option>
                    <option value='5' " . (($row['INTENSIDADE'] == '5') ? "selected" : "") . ">5</option>
                    <option value='6' " . (($row['INTENSIDADE'] == '6') ? "selected" : "") . ">6</option>
                    <option value='7' " . (($row['INTENSIDADE'] == '7') ? "selected" : "") . ">7</option>
                    <option value='8' " . (($row['INTENSIDADE'] == '8') ? "selected" : "") . ">8</option>
                    <option value='9' " . (($row['INTENSIDADE'] == '9') ? "selected" : "") . ">9</option>
                    <option value='10' " . (($row['INTENSIDADE'] == '10') ? "selected" : "") . ">10</option></select> &nbsp;&nbsp;<b>Padr&atilde;o :</b> <select name='padraodor' class='padrao_dor COMBO_OBG' style='background-color:transparent;'>
                    <option value='-1' selected></option>
                    <option value='0' " . (($row['PADRAO_ID'] == '0') ? "selected" : "") . ">Outro</option>
                    <option value='1' " . (($row['PADRAO_ID'] == '1') ? "selected" : "") . ">Pontada</option>
                    <option value='2' " . (($row['PADRAO_ID'] == '2') ? "selected" : "") . ">Queimação</option>
                    <option value='3' " . (($row['PADRAO_ID'] == '3') ? "selected" : "") . ">Latejante</option>
                    <option value='4' " . (($row['PADRAO_ID'] == '4') ? "selected" : "") . ">Peso</option>
                    <option value='5' " . (($row['PADRAO_ID'] == '5') ? "selected" : "") . ">Outro</option></select>{$outrador} </td></tr>";
            }
        }
        echo "</table>";

        echo "</td></tr>";

        /* FECHA LISTANDO */
        echo "</table>";

        /* SCORE PACIENTE */

        echo "<table class='mytable' style='width:95%;'>";
        if ($this->plan == 7 || $this->plan == 27 || $this->plan == 28 || $this->plan == 29 || $this->plan == 80) {
            $this->score_petrobras();
        }else{
            if(empty($dados->capmedica) || $dados->capmedica == null) {
                $tipo_relatorio = 'avaliacaoenfermagem';
                $sql = <<<SQL
SELECT 
  MAX(id) AS ultimaAvaMed 
FROM
  capmedica
WHERE
  paciente = '{$fae->pacienteid}'
SQL;
                $rs = mysql_query($sql);
                $rowAvaMed = mysql_fetch_array($rs);

                if (count($rowAvaMed) > '1') {
                    $dados->capmedica = $rowAvaMed['ultimaAvaMed'];
                    $tipo_relatorio = 'capmedica';
                }
            }

            \App\Controllers\Score::scoreNead2016($fae->pacienteid, $dados->capmedica, $tipo_relatorio, $dados->classificacao_paciente);
        }

        echo "</table>";
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th colspan='3' ><b>Classifica&ccedil;&atilde;o Score</b></th>";
        echo "</tr></thead>";
        echo"<tr><td colspan='3'><textarea cols=100 rows=2 type=text id='prejustparecer' name='pre_justificativa' readonly='readonly' size='100'>{$dados->pre_justificativa}</textarea></td></tr>";
        echo "</table>";
        /* PAD ENFERMAGEM */
        $this->servicoMultidisciplinarHospitalar(null);
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3' >PAD enfermagem</th></tr></thead>";
        echo "<tr><td colspan='3' ><textarea cols=90 rows=5 name='pad'   class='OBG' value={$ev->plano_terapeutico} {$acesso} ></textarea></td></tr>";
        echo "</table>";
        echo alerta();
        echo "</form></div>";

        if ($acesso != "readonly") {
            //echo "<button id='teste' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Teste</span></button></span>";
            echo "<button id='salvar-ficha-avaliacao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
            echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function detalhes_ficha_avaliacao_enfermagem($id) {

        $id = anti_injection($id, "numerico");
        $sql = "SELECT
		DATE_FORMAT(fev.DATA,'%d/%m/%Y %H:%i:%s') as fdata,
		fev.ID,
		fev.PACIENTE_ID as idpaciente,
		u.nome,
		p.nome as paciente,
		e.nome AS empresa_rel,
		pds.nome AS convenio_rel
		FROM
		fichamedicaevolucao as fev LEFT JOIN
		usuarios as u ON (fev.USUARIO_ID = u.idUsuarios) INNER JOIN
		clientes as p ON (fev.PACIENTE_ID = p.idClientes)
        LEFT JOIN empresas AS e ON fev.empresa = empresas.id
        LEFT JOIN planosdesaude AS pds ON fev.plano = pds.id
		WHERE
		fev.ID = '{$id}'";
        $r = mysql_query($sql);

        while ($row = mysql_fetch_array($r)) {
            $usuario = ucwords(strtolower($row["nome"]));
            $data = $row['fdata'];
            $paciente = $row['idpaciente'];
            $empresa = $row['empresa_rel'];
            $convenio = $row['convenio_rel'];
        }

        echo "<center><h1>Detalhes Ficha de Evolu&ccedil;&atilde;o M&eacute;dica </h1></center>";

        $this->cabecalho($paciente, $empresa, $convenio);
        echo "<center><h2>Respons&aacute;vel: {$usuario} - Data: {$data}</h2></center>";

        echo "<div>
                <button caminho='imprimir_ficha_evolucao.php?id={$id}' empresa-relatorio='{$empresa}' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  role='button' aria-disabled='false'>
                    <span class='ui-button-text'>
                        Imprimir
                    </span>
                </button>
              </div>";


        echo "<div id='div-ficha-evolucao-medica'><br/>";

        echo alerta();
        echo "<form>";
        echo "<input type='hidden' name='paciente'  value='{$ev->pacienteid}' />";
        echo "<br/><table class='mytable' style='width:100%;'>";

        echo "<thead><tr>";
        echo "<th colspan='3' >Problemas Ativos.</th>";
        echo "</tr></thead>";

        $sql2 = "select pae.DESCRICAO as nome, pae.CID_ID,(CASE pae.STATUS
		WHEN '1' THEN 'RESOLVIDO'
		WHEN '2' THEN 'MELHOR'
		WHEN '3' THEN 'EST&Aacute;VEL'
		WHEN '4' THEN 'PIOR' END) as sta , pae.STATUS,pae.OBSERVACAO from problemaativoevolucao as pae where pae.EVOLUCAO_ID = {$id} ";
        $result2 = mysql_query($sql2);
        $cont = 1;

        while ($row = mysql_fetch_array($result2)) {
            if ($row['STATUS'] != 0) {
                echo "<tr><td cid={$row['cid']} prob_atv='{$row['nome']}' x='{$cont}' class='probativo1' style='width:20%'><b>P{$cont} - {$row['nome']}</b> </td><td colspan='2'><b>Estado:</b> {$row['sta']}</td></tr>";
                if ($row['STATUS'] != 1) {
                    echo"<tr><td colspan='3'><b>OBSERVA&Ccedil;&Atilde;O: </b>{$row['OBSERVACAO']}</td></tr>";
                }
                $cont++;
            }
        }


        echo "<input type='hidden' name='capmed'  value='{$y}' />";
        echo "<thead><tr>";
        echo "<th colspan='3' >Novos Problemas </th>";
        echo "</tr></thead>";
        $sql2 = "select pae.DESCRICAO as nome, pae.CID_ID, pae.STATUS, pae.OBSERVACAO from problemaativoevolucao as pae where pae.EVOLUCAO_ID = {$id} ";
        $result2 = mysql_query($sql2);

        while ($row = mysql_fetch_array($result2)) {
            if ($row['STATUS'] == 0) {

                echo"<tr><td colspan=''><b>- {$row['nome']} </b><td colspan='2'> <b>OBS:</b> {$row['OBSERVACAO']}</td></tr>";
            }
        }


        $sql = "select * from fichamedicaevolucao where ID = {$id}";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {

            $pa_sistolica_min = $row['PA_SISTOLICA_MIN'];
            $pa_sistolica_max = $row['PA_SISTOLICA_MAX'];
            $pa_sistolica = $row['PA_SISTOLICA'];
            $pa_sistolica_sinal = $row['PA_SISTOLICA_SINAL'];
            $pa_diastolica_min = $row['PA_DIASTOLICA_MIN'];
            $pa_diastolica_max = $row['PA_DIASTOLICA_MAX'];
            $pa_diastolica = $row['PA_DIASTOLICA'];
            $pa_diastolica_sinal = $row['PA_DIASTOLICA_SINAL'];
            $fc = $row["FC"];
            $fc_max = $row["FC_MAX"];
            $fc_min = $row["FC_MIN"];
            $fc_sinal = $row["FC_SINAL"];
            $fr = $row["FR"];
            $fr_max = $row["FR_MAX"];
            $fr_min = $row["FR_MIN"];
            $fr_sinal = $row["FR_SINAL"];
            $temperatura_max = $row["TEMPERATURA_MAX"];
            $temperatura_min = $row["TEMPERATURA_MIN"];
            $temperatura = $row["TEMPERATURA"];
            $temperatura_sinal = $row["TEMPERATURA_SINAL"];
            $estd_geral = $row["ESTADO_GERAL"];
            $mucosa = $row['MUCOSA'];
            $mucosaobs = $row['MUCOSA_OBS'];
            $escleras = $row['ESCLERAS'];
            $esclerasobs = $row['ESCLERAS_OBS'];
            $respiratorio = $row['PADRAO_RESPIRATORIO'];
            $respiratorioobs = $row['PADRAO_RESPIRATORIO_OBS'];
            $novo_exame = $row['NOVOS_EXAMES'];
            $impressao = $row['IMPRESSAO'];
            $diagnostico = $row['PLANO_DIAGNOSTICO'];
            $terapeutico = $row['PLANO_TERAPEUTICO'];
            $dor_sn = $row['DOR'];
        }


        echo "<thead><tr>";
        echo "<th colspan='3' >Evolu&ccedil;&atilde;o dos sinais vitais (Registro da semana)</th>";
        echo "</tr></thead>";
        echo "<tr><td><b>PA (sist&oacute;lica) </b></td><td>min:<input type='texto' readonly class='numerico1 OBG' name='pa_sistolica_min' value='{$pa_sistolica_min}' readonly size='4' /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='pa_sistolica_max' value='{$pa_sistolica_max}' readonly  readonly  size='4' /></td>";
        if ($pa_sistolica_sinal == 1) {
            $x = "AMARELO";
        }

        if ($pa_sistolica_sinal == 2) {
            $x = "VERMELHO";
        }
        echo "<td><b>ALERTA:</b> {$x} </td>";
        echo "</tr>";
        echo "<tr><td><b>PA (diast&oacute;lica) </b> </td><td> min:<input type='texto' class='numerico1 OBG' name='pa_diastolica_min' value='{$pa_diastolica_min}' readonly  size='4'  /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='pa_diastolica_max' value='{$pa_diastolica_max}' size='4' readonly/></td>";
        $x = '';
        if ($pa_diastolica_sinal == 1) {
            $x = "AMARELO";
        }
        if ($pa_diastolica_sinal == 2) {
            $x = "VERMELHO";
        }
        echo "<td><b>ALERTA:</b> {$x}</td>";
        echo "</tr>";
        echo "<tr><td><b>FC </b> </td><td>min:<input type='texto' class='numerico1 OBG' name='fc_min' value='{$fc_min}'  size='4' readonly /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='fc_max' value='{$fc_max}' readonly size='4' /></td>";
        $x = '';
        if ($fc_sinal == 1) {
            $x = "AMARELO";
        }
        if ($fc_sinal == 2) {
            $x = "VERMELHO";
        }
        echo "<td><b>ALERTA:</b> {$x}</td>";
        echo "</tr>";
        echo "<tr><td><b>Fr </b> </td><td>min:<input type='texto' class='numerico1 OBG' name='fr_min' value='{$fr_min}' size='4' readonly /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='fr_max' value='{$fr_max}' size='4'readonly /></td>";
        $x = '';
        if ($fr_sinal == 1) {
            $x = "AMARELO";
        }
        if ($fr_sinal == 2) {
            $x = "VERMELHO";
        }
        echo "<td><b>ALERTA:</b> {$x}</td>";
        echo "</tr>";
        echo "<tr><td width=20% ><b>Temperatura: </b></td><td width=30% >min:<input type='texto' class='numerico1 OBG' name='temperatura_min' size='4' value='{$temperatura_min}' readonly size='4' /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='temperatura_max' size='4'value='{$temperatura_max}' readonly size='4' /></td>";
        $x = '';
        if ($ptemperatura_sinal == 1) {
            $x = "AMARELO";
        }
        if ($temperatura_sinal == 2) {
            $x = "VERMELHO";
        }
        echo "<td><b>ALERTA:</b> {$x}</td>";
        echo "</tr>";
        echo "<thead><tr>";
        echo "<th colspan='3'>Exame F&iacute;sico</th>";
        $x = '';
        $y1 = '';
        if ($estd_geral == 1) {
            $x = "BOM";
        }
        if ($estd_geral == 2) {
            $x = "REGULAR";
        }
        if ($estd_geral == 3) {
            $x = "RUIM";
        }

        echo "<tr><td><b>Estado Geral:</b>  </td><td colspan='2'>{$x}</td></tr>";
        $x = '';
        $y1 = '';
        if ($mucosa == 'n') {
            $x = 'DESCORADAS';
            $y1 = $mucosaobs;
        }
        if ($mucosa == 's') {
            $x = 'Coradas';
            $y1 = $mucosaobs;
        }
        echo "<tr><td><b>Mucosa:</b>  </td><td colspan='2'>{$x}  {$y1}</td></tr>";
        $x = '';
        if ($escleras == 'n') {
            $x = 'Ict&eacute;ricas';
            $y1 = $esclerasobs;
        }
        if ($escleras == 's') {
            $x = 'Anict&eacute;ricas';
            $y1 = $esclerasobs;
        }
        echo "<tr><td><b>Escleras:</b>  </td><td colspan='2'>{$x}  {$y1}</td></tr>";
        $x = '';
        $y1 = '';
        if ($respiratorio == 'n') {
            $x = 'Taqui/Dispn&eacute;ico';
            $y1 = $respiratorioobs;
        }
        if ($respiratorio == 's') {
            $x = 'Eupn&eacute;ico';
            $y1 = $respiratorioobs;
        }
        echo "<tr><td><b>Padr&atilde;o Respirat&oacute;:</b>  </td><td colspan='2'>{$x}  {$y1}</td></tr>";
        echo "<tr><td colspan='3'><b>PA (sist&oacute;lica):</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' class='numerico1 OBG' size='6' name='pa_sistolica' value='{$pa_sistolica}'  readonly {$acesso} />mmhg</td></tr>";
        echo "<tr><td colspan='3'><b>PA (diast&oacute;lica):</b>&nbsp;&nbsp;&nbsp;<input type='texto' class='numerico1 OBG' size='6' name='pa_diastolica' value='{$pa_diastolica}' readonly {$acesso} />mmhg</td></tr>";
        echo "<tr><td colspan='3'><b>FC: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' name='fc' readonly value='{$fc}' {$acesso} />bpm</td></tr>";
        echo "<tr><td colspan='3'><b>FR:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' readonly name='fr' value='{$fr}' {$acesso} />irpm</td></tr>";
        echo "<tr><td colspan='3'><b>Temperatura:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' name='temperatura' value='{$temperatura}' readonly {$acesso} />&deg;C</td></tr>";
        if ($dor_sn == 's') {
            $dor_sn = "Sim";
        }
        if ($dor_sn == 'n') {
            $dor_sn = "N&atilde;o";
        }
        echo"<tr><td colspan='3' ><b>DOR:</b>{$dor_sn}</td></tr>";

        echo "<tr><td colspan='3' ><table id='dor' style='width:100%;'>";
        $sql = "select * from dorfichaevolucao where FICHAMEDICAEVOLUCAO_ID = {$id} ";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            echo "<tr><td> <b>Local:</b> {$row['DESCRICAO']}&nbsp;&nbsp; <b>Escala visual:</b> {$row['ESCALA']}&nbsp;&nbsp;<b>Padr&atilde;o:</b>{$row['PADRAO']}&nbsp;&nbsp; </td></tr>";
        }


        echo "</table></td></tr>";

        echo "<thead><tr>";
        echo "<th colspan='3' >CATETERS e SONDAS </th>";
        echo "</tr></thead>";
        $sql = "select DESCRICAO from catetersondaevolucao where FICHA_EVOLUCAO_ID = {$id}";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {

            echo "<tr><td colspan='3'><b>- {$row['DESCRICAO']}<b></td></tr>";
        }

        echo "<thead><tr>";
        echo "<th colspan='3' >Exame segmentar</th>";
        echo "</tr></thead>";
        $sql = "select exv.*,(CASE exv.AVALIACAO
		WHEN '1' THEN 'NORMAL'
		WHEN '2' THEN 'ALTERADO'
		WHEN '3' THEN 'N EXAMINADO' END) as av,ex.DESCRICAO from examesegmentarevolucao as exv LEFT JOIN examesegmentar as ex ON (exv.EXAME_SEGMENTAR_ID = ex.ID) where exv.FICHA_EVOLUCAO_ID = {$id}";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {

            echo "<tr><td idexame={$row['ID']} class='exame1'> <b>{$row['DESCRICAO']}</b></td><td colspan='2'>{$row['av']}</td></tr>";
            if ($row['AVALIACAO'] == 2 || $row['AVALIACAO'] == 3) {
                echo"<tr><td colspan='3'><b>OBS: </b>{$row['OBSERVACAO']}</td></tr>";
            }
        }
        echo "<thead><tr>";
        echo "<th colspan='3' >Exames Complementares Novos</th>";
        echo "</tr></thead>";
        echo "<tr><td colspan='3' ><b>{$novo_exame} </b></td></tr>";
        echo "<thead><tr>";
        echo "<th colspan='3' >Impress&atilde;o</th>";
        echo "</tr></thead>";
        echo "<thead><tr>";
        echo "<tr><td colspan='3' ><b>{$impressao} </b></td></tr>";
        echo "<th colspan='3' >Plano Diagn&oacute;stico</th>";
        echo "</tr></thead>";
        echo "<thead><tr>";
        echo "<tr><td colspan='3' ><b>{$diagnostico} </b></td></tr>";
        echo "<th colspan='3' >Plano Teraup&ecirc;utico</th>";
        echo "</tr></thead>";
        echo "<thead><tr>";
        echo "<tr><td colspan='3' ><b>{$terapeutico}</b></td></tr>";




        echo "</table></form></div>";
        echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
    }

    ///////////////////////////////////////////////////////

    public function listar_ficha_avaliacao_enfermagem($id) {

        ////jeferson 2012-10-28

        $id = anti_injection($id, "numerico");
        $sql3 = "select Max(ID)  from avaliacaoenfermagem where PACIENTE_ID = '{$id}' ";
        $r2 = mysql_query($sql3);
        $maior_cap = mysql_result($r2, 0);


        $sql3 = "select DATE_FORMAT(DATA,'%d-%m-%Y')  from avaliacaoenfermagem where id = '{$maior_cap}' ";
        $r2 = mysql_query($sql3);
        $data_cap = mysql_result($r2, 0);

        $data_atual = date('Y/m/d');
        $prazoEditar = date('Y/m/d', strtotime("+2 days", strtotime($data_cap)));


        $sql = "SELECT
		DATE_FORMAT(avenf.DATA,'%d/%m/%Y %H:%i:%s') as fdata,
		avenf.ID,
		avenf.PACIENTE_ID as idpaciente,
		u.nome,
		u.tipo as utipo,
		u.conselho_regional,
		p.nome as paciente
		FROM
		avaliacaoenfermagem as avenf LEFT JOIN
		usuarios as u ON (avenf.USUARIO_ID = u.idUsuarios) INNER JOIN
		clientes as p ON (avenf.PACIENTE_ID = p.idClientes)

		WHERE
		avenf.PACIENTE_ID = '{$id}'
		ORDER BY
		avenf.ID DESC";
        $r = mysql_query($sql);
        $r = mysql_query($sql);
        $flag = true;
        while ($row = mysql_fetch_array($r)) {
            $compTipo = $row['conselho_regional'];

            if ($flag) {

                $p = ucwords(strtolower($row["c.paciente"]));
                echo "<center><h1>Avaliação de Enfermagem</h1></center>";
                $this->cabecalho($id);
                //echo "<center><h2>{$p}</h2></center>";

                echo "<br/>";
                echo "<div><input type='hidden' name='nova_avaliacao_id' value='{$id}'></input>";


                if ($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                        ['enfermagem']
                        ['enfermagem_paciente']
                        ['enfermagem_paciente_avaliacao']
                        ['enfermagem_paciente_avaliacao_novo'])) {
                    echo "<button id='nova_avaliacao_enfermagem'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
							<span class='ui-button-text'>Nova Avaliação de Enfermagem</span></button>";
                }

                echo"</div>";
                echo "<br/><table class='mytable' style='width:100%' >";
                echo "<thead><tr>";
                echo "<th width='55%'>Usu&aacute;rio</th>";
                echo "<th>Data</th>";
                echo "<th width='15%' colspan ='2'> Op&ccedil;&otilde;es</th>";

                //echo "<th>Op&ccedil;&otilde;es</th>";
                echo "</tr></thead>";
                $flag = false;
            }

            $u = ucwords(strtolower($row["nome"])) . $compTipo;

            echo "<tr><td>{$u}</td><td>{$row['fdata']}</td>";
            echo "<td><a href=?view=editaravaliacaoenfermagem&id={$row['ID']}&v=1><img src='../utils/capmed_16x16.png' title='Visualizar' border='0'></a></td>";
            echo "<td>";
            //echo "<td><a href=?view=capmed&id={$row['ID']}><img src='../utils/capmed_16x16.png' title='Detalhe Avalia&ccedil;&atilde;o' border='0'></a>";
            ///mudou ak
            if($row['ID'] == $maior_cap && ( $_SESSION['adm_user'] == 1 || $_SESSION['modenf'] == 1
                ||  isset($_SESSION['permissoes']
                        ['enfermagem']
                        ['enfermagem_paciente']
                        ['enfermagem_paciente_avaliacao']
                        ['enfermagem_paciente_avaliacao_usar_para_nova']))) {
                    echo "<a href=?view=editaravaliacaoenfermagem&id={$row['ID']}&v=0><img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para nova avalia&ccedil;&atilde;o.' border='0'></a>";
                }
                if( $data_atual < $prazoEditar ){
              //echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=?view=editaravaliacaoenfermagem&id={$row['id']}&v=2><img src='../utils/edit_16x16.png' title='Editar Avalia&ccedil;&atilde;o Enfermagem' border='0'></a>";

            }
            echo "</td></tr>";
        }

        if ($flag) {
            if ($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                    ['enfermagem']
                    ['enfermagem_paciente']
                    ['enfermagem_paciente_avaliacao']
                    ['enfermagem_paciente_avaliacao_novo'])) {
                $this->nova_ficha_avaliacao_enfermagem($id);
            } else {
                echo '<script type="text/javascript">
                        alert("O paciente não possui ficha de avaliação e você não possui permissão para criar uma nova!");
                        window.location.href = "/enfermagem/?view=listar";
                      </script>';
            }
            return;
        }
        echo "</table>";
    }

    public function nova_editar_ficha_avaliacao_enfermagem($id, $v) {

        $id = anti_injection($id, "numerico");
        $result2 = mysql_query("select PACIENTE_ID from avaliacaoenfermagem where id = '{$id}'");
        while ($row = mysql_fetch_array($result2)) {
            $id_p = $row['PACIENTE_ID'];
        }

        $result = mysql_query("SELECT p.nome FROM clientes as p WHERE p.idClientes = '{$id_p}'");


        $fae = new FichaAvaliacaoEnfermagem();
        while ($row = mysql_fetch_array($result)) {
            $fae->pacienteid = $id_p;
            $fae->pacientenome = ucwords(strtolower($row["nome"]));
        }

        $this->editar_ficha_avaliacao_enfermagem($id, $v, $fae);
    }

    //Fun��o Frequencia uso Eriton
    public function frequencia_uso($id,$editar = null, $controle) {
        $disable = ($editar == 1) ? "disabled='disabled'" : "";
        $result = mysql_query("
					SELECT
						*
					FROM
						frequencia
					ORDER BY frequencia asc;
			");
        $var = "Frequ&ecirc;ncia <select $disable class='frequencia-uso' class='COMBO_OBG' name='frequencia' style='background-color:transparent;font-size:10px' >";
        while ($row = mysql_fetch_array($result)) {
            if (($id <> NULL) && ($id == $row['id']))
                $var .= "<option value='" . $row['id'] . "' selected>" . strtoupper($row['frequencia']) . "</option>";
            else
                $var .= "<option value='" . $row['id'] . "'>" . strtoupper($row['frequencia']) . "</option>";
        }
        $var .= "</select>";

        if ($controle == 1) {
            print_r($var);
        } else {
            return $var;
        }
    }

    //Fim Fun��o Frequencia uso Eriton
    //Fun��o para editar a ficha dce avalia��o do paciente
    /**
     * @param $id
     * @param $v
     * @param $fae
     */
    public function editar_ficha_avaliacao_enfermagem($id, $v, $fae) {
        $titulo = 'Nova';
        if ($v == 1) {
            $acesso = "readonly"; //readonly
            $enabled = "disabled";
            $titulo = 'Visualizar';
        }

        $sql = "select 
                  avaliacaoenfermagem.*,
                  e.nome AS empresa_rel,
                  pds.nome AS convenio_rel
                from 
                  avaliacaoenfermagem
                  LEFT JOIN empresas AS e ON avaliacaoenfermagem.empresa = e.id
                  LEFT JOIN planosdesaude AS pds ON avaliacaoenfermagem.convenio = pds.id 
                where avaliacaoenfermagem.ID = {$id}";
        //echo $sql;
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $empresaId = $row['empresa'];
            $paciente = $row['PACIENTE_ID'];
            $empresa = $row['empresa_rel'];
            $convenio = $row['convenio_rel'];
            $id_aval = $row['ID'];
            $clinico = $row['CLINICO'];
            $cirurgico = $row['CIRURGICO'];
            $paliativos = $row['PALIATIVOS'];
            $diagnostico_medico_principal = $row['DIAGNOSTICO_PRINCIPAL'];
            $hist_pregressa = $row['HIST_PREGRESSA'];
            $hist_familiar = $row['HIST_FAMILIAR'];
            $queixas_atuais = $row['QUEIXA_ATUAL'];
            $hist_atual = $row['HIST_ATUAL'];
            $bom = $row['EST_GERAL_BOM'];
            $ruim = $row['EST_GERAL_RUIM'];
            $nutrido = $row['NUTRIDO'];
            $desnutrido = $row['DESNUTRIDO'];
            $higiene_corpo_s = $row['HIGIENE_CORPORAL_SATISFATORIA'];
            $higiene_corpo_i = $row['HIGIENE_CORPORAL_INSATISFATORIA'];
            $higiene_bucal_s = $row['HIGIENE_BUCAL_SATISFATORIA'];
            $higiene_bucal_i = $row['HIGIENE_BUCAL_INSATISFATORIA'];
            $estado_emocional = $row['ESTADO_EMOCIONAL'];
            $consciente = $row['CONCIENTE'];
            $sedado = $row['SEDADO'];
            $obnublado = $row['OBNUBILADO'];
            $torporoso = $row['TORPOROSO'];
            $comatoso = $row['COMATOSO'];
            $orientado = $row['ORIENTADO'];
            $desorientado = $row['DESORIENTADO'];
            $sonolento = $row['SONOLENTO'];
            $cond_motora = $row['COND_MOTORA'];
            $palergiamed = $row['ALERGIA_MEDICAMENTO'];
            $palergiaalim = $row['ALERGIA_ALIMENTO'];
            $alergiamed = $row['ALERGIA_MEDICAMENTO_TIPO'];
            $alergiaalim = $row['ALERGIA_ALIMENTO_TIPO'];
            $integra = $row['PELE_INTEGRA'];
            $lesoes = $row['PELE_LESAO'];
            $hidratada = $row['PELE_HIDRATADA'];
            $desidratada = $row['PELE_DESIDRATADA'];
            $icterica = $row['PELE_ICTERICA'];
            $anicterica = $row['PELE_ANICTERICA'];
            $icterica_escala = $row['PELE_ICTERICA_ESCALA'];
            $palida = $row['PELE_PALIDA'];
            $sudoreica = $row['PELE_SUDOREICA'];
            $fria = $row['PELE_FRIA'];
            $pele_edema = $row['PELE_EDEMA'];
            $descamacao = $row['PELE_DESCAMACAO'];
            $t_assimetrico = $row['T_ASSIMETRICO'];
            $t_simetrico = $row['T_SIMETRICO'];
            $eupneico = $row['EUPNEICO'];
            $dispneico = $row['DISPNEICO'];
            $taquipneico = $row['TAQUIPNEICO'];
            $mvbd = $row['MVBD'];
            $roncos = $row['RONCOS'];
            $sibilos = $row['SIBILOS'];
            $creptos = $row['CREPTOS'];
            $mv_diminuido = $row['MV_DIMINUIDO'];
            $bcnf = $row['BCNF'];
            $normocardico = $row['NORMOCARDICO'];
            $taquicardico = $row['TAQUICARDICO'];
            $bradicardico = $row['BRADICARDICO'];
            $ventilacao = $row['VENTILACAO'];
            $tipo_ventilacao2 = $row['VENTILACAO_ESP_EQUIP'];
            $vent_metalica = $row['VENT_TRAQUESTOMIA_METALICA_NUM'];
            $ventilacao_plastica_num = $row['VENT_TRAQUESTOMIA_PLASTICA_NUM'];
            $bipap = $row['BIPAP'];
            $oxigenio_sn = $row['OXIGENIO_SN'];
            $oxigenio = $row['OXIGENIO'];
            $vent_cateter_oxigenio_num = $row['VENT_CATETER_OXIGENIO_NUM'];
            $vent_cateter_oxigenio_quant = $row['VENT_CATETER_OXIGENIO_QUANT'];
            $vent_cateter_oculos_num = $row['VENT_CATETER_OCULOS_NUM'];
            $vent_respirador = $row['VENT_RESPIRADOR'];
            $vent_respirador_tipo = $row['VENT_RESPIRADOR_TIPO'];
            $vent_mascara_ventury = $row['VENT_MASCARA_VENTURY'];
            $vent_mascara_ventury_tipo = $row['VENT_MASCARA_VENTURY_NUM'];
            $vent_concentrador = $row['VENT_CONCENTRADOR'];
            $vent_concentrador_num = $row['VENT_CONCENTRADOR_NUM'];
            $alt_aspiracoes = $row['ALT_ASPIRACOES'];
            $aspiracoes_num = $row['ASPIRACAO_NUM'];
            $aspiracao_secrecao = $row['ASPIRACAO_SECRECAO'];
            $caracteristicas_sec = $row['ASPIRACAO_SECRECAO_CARACTERISTICA'];
            $cor_sec = $row['ASPIRACAO_SECRECAO_COR'];
            $plano = $row['PLANO'];
            $flacido = $row['FLACIDO'];
            $distendido = $row['DISTENDIDO'];
            $timpanico = $row['TIMPANICO'];
            $globoso = $row['GLOBOSO'];
            $rha = $row['RHA'];
            $indo_palpacao = $row['INDO_PALPACAO'];
            $doloroso = $row['DOLOROSO'];
            $oral = $row['VIA_ALIMENTACAO_ORAL'];
            $sne = $row['VIA_ALIMENTACAO_SNE'];
            $sne_num = $row['VIA_ALIMENTACAO_SNE_NUM'];
            $gtm_sonda_gastro = $row['VIA_ALIMENTACAO_GTM'];
            $gtm_num = $row['VIA_ALIMENTACAO_GTM_NUM'];
            $gtm_num_profundidade = $row['VIA_ALIMENTACAO_GMT_PROFUNDIDADE'];
            $gtm_visivel = $row['VIA_ALIMENTACAO_GTM_VISIVEL'];
            $sonda_gastro = $row['VIA_ALIMENTACAO_SONDA_GASTRO'];
            $quant_sonda = $row['VIA_ALIMENTACAO_SONDA_GASTRO_NUM'];
            $parental = $row['VIA_ALIMENTACAO_PARENTAL'];
            $dieta_sn = $row['DIETA_SN'];
            $sistema_aberto = $row['DIETA_SIST_ABERTO'];
            $uso_sistema_aberto = $row['USO_SISTEMA_ABERTO'];
            $quant_sistema_aberto = $row['DIETA_SIST_ABERTO_DESCRICAO'];
            $uso = $row['DIETA_SIST_ABERTO_USO'];
            $sistema_fechado = $row['DIETA_SIST_FECHADO'];
            $quant_sistema_fechado = $row['DIETA_SIST_FECHADO_DESCRICAO'];
            $vazao = $row['DIETA_SIST_FECHADO_VAZAO'];
            $dieta_artesanal = $row['DIETA_SIST_ARTESANAL'];
            $suplemento_sn = $row['DIETA_SUPLEMENTO_SN'];
            $suplemento = $row['DIETA_SUPLEMENTO'];
            $suplemento_desc = $row['DIETA_SUPLEMENTO_DESC'];
            $dieta_npt = $row['DIETA_NPT'];
            $quant_npt = $row['DIETA_NPT_DESC'];
            $vazao_npt = $row['DIETA_NPT_VAZAO'];
            $npp = $row['DIETA_NPP'];
            $quant_npp = $row['DIETA_NPP_DESC'];
            $vazao_npp = $row['DIETA_NPP_VAZAO'];
            $integra_genital = $row['INTEGRA'];
            $secrecao = $row['SECRECAO'];
            $sangramento = $row['SANGRAMENTO'];
            $prurido = $row['PRURIDO'];
            $edema = $row['EDEMA'];
            $topicos_sn = $row['TOPICOS_SN'];
            $dispositivos_intravenosos = $row['DISPOSITIVOS_INTRAVENOSOS'];
            $cvc_sn = $row['CVC'];
            $cvc = $row['DISPO_INTRAVENOSO_CVC'];
            $local_cvc = $row['DISPO_INTRAVENOSO_CVC_LOCAL'];
            $local_uno = $row['DISPO_INTRAVENOSO_UNO_LOCAL'];
            $local_duplo = $row['DISPO_INTRAVENOSO_DUPLO_LOCAL'];
            $local_triplo = $row['DISPO_INTRAVENOSO_TRIPLO_LOCAL'];
            $avp = $row['DISPO_INTRAVENOSO_AVP'];
            $avp_local = $row['DISPO_INTRAVENOSO_AVP_LOCAL'];
            $port = $row['DISPO_INTRAVENOSO_PORTOCATH'];
            $quant_port = $row['DISPO_INTRAVENOSO_PORTOCATH_NUM'];
            $port_local = $row['DISPO_INTRAVENOSO_PORTOCATH_LOCAL'];
            $outros_intra = $row['DISPO_INTRAVENOSO_OUTROS'];
            $eliminaciones = $row['ELIMINACOES'];
            $fralda = $row['FRALDA'];
            $fralda_dia = $row['FRALDA_UNID_DIA'];
            $tamanho_fralda = $row['FRALDA_TAMANHO'];
            $urinario = $row['DISPO_URINARIO'];
            $quant_urinario = $row['DISPO_URINARIO_NUM'];
            $sonda = $row['DISPO_SONDA_FOLEY'];
            $quant_foley = $row['DISPO_SONDA_FOLEY_NUM'];
            $sonda_alivio = $row['DISPO_SONDA_ALIVIO'];
            $quant_alivio = $row['DISPO_SONDA_ALIVIO_NUM'];
            $colostomia = $row['DISPO_COLOSTOMIA'];
            $quant_colostomia = $row['DISPO_COLOSTOMIA_NUM'];
            $iliostomia = $row['DISPO_ILIOSTOMIA'];
            $quant_iliostomia = $row['DISPO_ILIOSTOMIA_NUM'];
            $cistostomia = $row['DISPO_CISTOSTOMIA'];
            $quant_cistostomia = $row['DISPO_CISTOSTOMIA_NUM'];
            $dreno_sn = $row['DRENOS'];
            $pa_sistolica = $row['PA'];
            $pa_diastolica = $row['PA_DISTOLICA'];
            $fc = $row['FC'];
            $fr = $row['FR'];
            $spo2 = $row['SPO2'];
            $temperatura = $row['TEMPERATURA'];
            $pad = $row['PAD_ENFERMAGEM'];
            $dor_sn = $row['DOR'];
            $feridas_sn = $row['FERIDA'];
            $pre_justificativa = $row['PREJUSTIFICATIVA'];
            $modeloScoreNead = $row['MODELO_NEAD'];
            $classificacaoNead = $row['CLASSIFICACAO_NEAD'];
            $dataSistema = $row['DATA'];


            $arrayMultidisciplinar =[
                'medico'=>$row['medico_multidisciplinar'],
                'enfermeiro'=>$row['enfermagem_multidisciplinar'],
                'fono'=>$row['fono_multidisciplinar'],
                'nutricao'=>$row['nutricao_multidisciplinar'],
                'fissio_motora'=>$row['fissio_motora_multidisciplinar'],
                'fissio_respiratoria'=>$row['fissio_respiratoria_multidisciplinar'],
            ];
        }

        echo "<center><h1>{$titulo} Avalia&ccedil;&atilde;o de Enfermagem </h1></center>";
        //echo $clinico;

        $this->cabecalho($fae, $empresa, $convenio);
        $this->plan;

        $sqlPaciente = "select * from clientes where clientes.idClientes = {$paciente}";
        $resultPaciente = mysql_query($sqlPaciente);
        $rowPaciente = mysql_fetch_array($resultPaciente);

        if(empty($empresaId)){
            $empresaId =$rowPaciente['empresa'];
        }



        echo "<div id='div-ficha-avaliacao-enfermagem'><br/>";

        $caminho = "/enfermagem/imprimir_ficha_ava.php?&id_a={$id_aval}&id={$paciente}";

        echo "<div><button  caminho='{$caminho}' empresa-relatorio='{$empresaId}' 
class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  
role='button' aria-disabled='false'>
<span class='ui-button-text'>Imprimir</span>
</button></div>";

//        echo"<form method=post target='_blank' action='imprimir_ficha_ava.php?&id_a={$id_aval}&id={$paciente} '>";
//        echo"<input type=hidden value= {$id_aval} class='imprimir-ficha-avaliacao-enf'/>";
//        echo "<button id='imprimir-ficha-avaliacao-enf' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Imprimir</span></button></span>";
//        echo"</form>";

        echo "<form>";
        echo "<input type='hidden' name='paciente'  value='{$paciente}' />";
        echo "<input type='hidden' name='id_aval'  value='{$id_aval}' />";
        echo "<br/>";



        /* ANAMNESE */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Anamnese</th></tr></thead>";
        echo"<tr><td style='width:33%;' colspan='4'><label><b>Tipo Tratamento:</b></tr></td>";
        echo"<tr><td colspan='2'><input type='checkbox'  value=1 class='clinico OBG_RADIO' " . (($clinico) ? "checked" : "") . " {$enabled} name='clinico' ><label>Cl&iacute;nico</label></td><td><input type='checkbox' value=1 " . (($cirurgico) ? "checked" : "") . " {$enabled} name='cirurgico' class='clinico OBG_RADIO'><label>Cir&uacute;rgico</label></td><td><input type='checkbox' value=1 " . (($paliativos) ? "checked" : "") . " {$enabled}  name='paliativos' class='clinico OBG_RADIO'><label>Cuidados Paliativos</label></td></tr>";
        /* Atualiza��o Eriton 14/03/2013 */
        echo"<tr><td colspan = '4'><label><b>Diagn&oacute;stico M&eacute;dico Principal que o levou &agrave; Hospitaliza&ccedil;&atilde;o:</b></label><br/><textarea cols=90 rows=2 name='diagnostico_principal' class='OBG' {$acesso} >{$diagnostico_medico_principal}</textarea></td></tr>";
        echo"<tr><td colspan = '4'><label><b>Hist&oacute;ria Pregressa:</b></label><br/><textarea cols=90 rows=2 name='hist_pregressa'   class='OBG'  {$acesso} >{$hist_pregressa}</textarea></td></tr>";
        echo"<tr><td colspan = '4'><label><b>Hist&oacute;ria Familiar:</b></label><br/><textarea cols=90 rows=2 name='hist_familiar'   class='OBG' {$acesso} >{$hist_familiar} </textarea></td></tr>";
        echo"<tr><td colspan = '4'><label><b>Observa&ccedil;&otilde;es:</b></label><br/><textarea cols=90 rows=2 name='queixas_atuais'   class='OBG'  {$acesso} >{$queixas_atuais}</textarea></td></tr>";
        /* Atualiza��o Eriton 14/03/2013 */
        echo "</table>";


        echo "<input type='hidden' name='capmed'  value='{$y}' />";


        /* MEDICACOES EM USO */
        echo "<table class='mytable' id='medicamentos_ativos' style='width:95%;'>";
        echo "<thead><tr><th colspan='5' >Medica&ccedil;&otilde;es em uso </th></tr></thead>";

        echo"<tr><td colspan='5'><b>Medica&ccedil;&atilde;o:</b><span class='outro_medicamento_ativo' ><input type='text' size='110' name='medicamento_ativo' class='med_ativo' {$acesso} size=75/> <button id='add-medicamento-ativo' tipo='' {$acesso} $enabled >+ </button></span></td></tr>"; //<button id='add-medicamento-ativo' tipo=''  >+ </button>
        if ($v == 1) {
            $classe3 = '';
        } else {
            $classe3 = "class='del-medicamento-ativo'";
        }

        $sql5 = "select * from avaliacao_enf_medicamento where avaliacao_enf_id = {$id}";
        $result5 = mysql_query($sql5);
        $cont_med = 1;
        while ($row5 = mysql_fetch_array($result5)) {
            $medicacao = $row5['MEDICACAO'];
            $apresentacao = $row5['APRESENTACAO'];
            $via = $row5['VIA'];
            /* ATUALIZA��O ERITON 22/03/2013 */
            $posologia = $row5['POSOLOGIA'];
            $frequencia_med = $row5['FREQUENCIA'];
            $custeado_familia = $row5['CUSTEADO_FAMILIA'];


            echo "<tr><td class='medicamento-ativo' desc='{$medicacao}'> <img src='../utils/delete_16x16.png' {$classe3} title='Excluir' border='0' > {$medicacao}
			Apresenta&ccedil;&atilde;o <select name='apresentacao' class='med_ativo' class='COMBO_OBG'  {$enabled}>
			<option value = '-1'>  </option>
			<option value='Ampola' " . (($apresentacao == 'Ampola') ? "selected" : "") . "> Ampola </option>
			<option value='Cápsula' " . (($apresentacao == 'Cápsula') ? "selected" : "") . "> C&aacute;psula </option>
			<option value='CPR' " . (($apresentacao == 'CPR') ? "selected" : "") . "> CPR </option>
			<option value = 'Frasco' " . (($apresentacao == 'Frasco') ? "selected" : "") . "> Frasco </option>
			<option value = 'Solução'" . (($apresentacao == 'Solução') ? "selected" : "") . "> Solu&ccedil;&atilde;o </option>
            <option value='Topico' " . (($apresentacao == 'Tópico') ? "selected" : "") . "> T&oacute;pico </option>
            <option value='Sach&ecirc;' " . (($apresentacao == 'Sachê') ? "selected" : "") . "> Sach&ecirc; </option>
			<option value='Seringa' " . (($apresentacao == 'Seringa' || $apresentacao == 'Seringa;') ? "selected" : "") . "> Seringa </option>
             </select></td>
			<td>Via<select name='via' class='med_ativo' class='COMBO_OBG'  {$enabled}><option value = '-1'>  </option>
			<option value='VO' " . (($via == 'VO') ? "selected" : "") . "> VO </option>
			<option value = 'SNE' " . (($via == 'SNE') ? "selected" : "") . "> SNE </option>
			<option value = 'GTM' " . (($via == 'GTM') ? "selected" : "") . "> GTM </option>
			<option value ='IV' " . (($via == 'IV') ? "selected" : "") . "> IV </option>
            <option value ='IM' " . (($via == 'IM') ? "selected" : "") . "> IM </option>
			<option value ='TOPICA' " . (($via == 'topica') ? "selected" : "") . "> TOPICA </option>
			<option value ='SC' " . (($via == 'SC') ? "selected" : "") . "> SC </option> </select></td>
			<td> Posologia <input type='text' name='posologia' value='{$posologia}' {$acesso} class='med_ativo'/></td>
			<td id = 'frequencia_{$cont_med}' class = 'frequencia-uso'>";
            echo $this->frequencia_uso($frequencia_med,$v);
            echo "</td>
			<td> Fam&iacute;lia custeia <input type='checkbox' name='familia_custeia' value='0' " . (($custeado_familia) ? "checked" : "") . " {$enabled}  class='med-uso' /></td> </tr>";
            $cont_med++;
        }

        if (!empty($ev->ativos)) {
            foreach ($ev->ativos as $a) {
                echo "<tr><td>{$a}</td></tr>";
            }
        }
        echo "</table>";

        /* HISTORIA ATUAL */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Hist&oacute;ria Atual</th></tr></thead>";
        echo"<tr><td colspan =4><textarea cols=90 rows=4 name='hist_atual' class='OBG'  {$acesso} >{$hist_atual}</textarea></td></tr>";
        echo "</table>";

        echo "<br/>";

        /*         * ************************** EXAME FISICO ************************************ */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<tr style='background-color:#ccc;'><td colspan='4' ><b><center>Exame F&iacute;sico</center></b></td></tr>";
        echo "</table>";

        echo "</br>";


        /* ESTADO GERAL */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan =4> <b>Estado Geral</b></th></thead>";

        echo"<tr><td style='width:25%;'><input type='checkbox' value=1 " . (($bom) ? "checked" : "") . " {$enabled} name='bom'>Bom</td> <td style='width:25%;'><input type='checkbox'  value=1 " . (($ruim) ? "checked" : "") . " {$enabled} name='ruim'>Ruim</td>
		<td style='width:25%;'><input type='checkbox' value=1 " . (($nutrido) ? "checked" : "") . " {$enabled} name='nutrido'>Nutrido</td>
		<td style='width:25%;'><input type='checkbox' value=1 " . (($desnutrido) ? "checked" : "") . " {$enabled} name='desnutrido'> Desnutrido</td>
		</tr>
		";
        echo " <tr><td style='width:25%;'> <input type='checkbox' value=1 " . (($higiene_corpo_s) ? "checked" : "") . " {$enabled} name='higiene_corpo_s'>Higiene corporal satisfat&oacute;ria</td>
		<td style='width:25%;'><input type='checkbox' value=1 " . (($higiene_corpo_i) ? "checked" : "") . " {$enabled} name='higiene_corpo_i'>Higiene corporal insatisfat&oacute;ria</td>
		<td style='width:25%;'><input type='checkbox' value=1 " . (($higiene_bucal_s) ? "checked" : "") . " {$enabled} name='higiene_bucal_s'>Higiene bucal satisfat&oacute;ria</td>
		<td style='width:25%;'><input type='checkbox' value=1 " . (($higiene_bucal_i) ? "checked" : "") . " {$enabled}  name='higiene_bucal_i'>Higiene bucal insatisfat&oacute;ria</td>
		</tr>";
        echo "</tr>";
        echo "</tbale>";


        /* ESTADO EMOCIONAL */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan =4> <b>Estado Emocional</b></th></tr></thead>";
        echo "<tr><td style='width:25%;'><input type='radio' value='1' " . (($estado_emocional == '1') ? "checked" : "") . " {$enabled} name='estado_emocional'>Calmo </td>
		<td style='width:25%;'><input type='radio' value='2' " . (($estado_emocional == '2') ? "checked" : "") . " {$enabled} name='estado_emocional'>Agitado</td>
		<td style='width:25%;'><input type='radio'  value='3' " . (($estado_emocional == '3') ? "checked" : "") . " {$enabled} name='estado_emocional'>Deprimido</td>
		<td style='width:25%;'><input type='radio' value='4' " . (($estado_emocional == '4') ? "checked" : "") . " {$enabled} name='estado_emocional' >Choroso</td>
		</tr>";
        echo "</table>";

        /* NIVEL DE CONSCIENCIA */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan =4> <b>N&iacute;vel de Consci&ecirc;ncia</b></th></thead>";
        echo"<tr><td style='width:25%;'><input type='checkbox' value='1' " . (($consciente) ? "checked" : "") . " {$enabled} name='consciente'>Consciente</td>
		<td style='width:25%;'><input type='checkbox' value='1' " . (($sedado) ? "checked" : "") . " {$enabled} name='sedado'>Sedado</td>
		<td style='width:25%;'><input type='checkbox' value='1' " . (($obnublado) ? "checked" : "") . " {$enabled} name='obnublado'>Obnubilado</td>
		<td style='width:25%;'><input type='checkbox' value='1' " . (($torporoso) ? "checked" : "") . " {$enabled} name='torporoso'>Torporoso</td>
		</tr>";

        echo"<tr><td style='width:25%;'><input type='checkbox' value='1' " . (($comatoso) ? "checked" : "") . " {$enabled} name='comatoso'>Comatoso</td>
		<td style='width:25%;'><input type='checkbox' value='1' " . (($orientado) ? "checked" : "") . " {$enabled} name='orientado'>Orientado</td>
		<td style='width:25%;'><input type='checkbox' value='1' " . (($desorientado) ? "checked" : "") . " {$enabled}  name='desorientado'>Desorientado</td>
		<td style='width:25%;'><input type='checkbox' value='1' " . (($sonolento) ? "checked" : "") . " {$enabled} name='sonolento'>Sonolento</td>
		</tr>";
        echo "</table>";


        /* CONDI��O MOTORA */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan =4> <b>Condi&ccedil;&atilde;o Motora</b></th></thead>";

        echo "<tr><td style='width:25%;'><input type='radio' value='1' " . (($cond_motora == '1') ? "checked" : "") . " {$enabled} name='cond_motora'>Deambula</td>
		<td style='width:25%;'><input type='radio' value='2' " . (($cond_motora == '2') ? "checked" : "") . " {$enabled} name='cond_motora'>Deambula com Aux&iacute;lio</td>
		<td style='width:25%;'><input type='radio' value='3' " . (($cond_motora == '3') ? "checked" : "") . " {$enabled} name='cond_motora'>Cadeirante</td>
		<td style='width:25%;'><input type='radio' value='4'  " . (($cond_motora == '4') ? "checked" : "") . " {$enabled} name='cond_motora' >Acamado</td></tr>";
        echo "</table>";

        /* ALERGIA MEDICAMENTOSA E ALERGIA ALIMENTAR */
        echo "<table class='mytable' style='width:95%;'>";
        echo"<thead><tr><th colspan='3'>Alergia medicamentosa (S/N) <input type='text' class='boleano OBG' name='palergiamed' value='{$palergiamed}' {$acesso} /></th>";
        echo "<th >Alergia alimentar (S/N) <input size='50' type='text' class='boleano OBG' name='palergiaalim' value='{$palergiaalim}' {$acesso} /></th>";
        echo "</tr></thead>";
        echo "<tr><td colspan='3'><input type='text' name='alergiamed' size='50'  value='{$alergiamed}' {$acesso} /></td>";
        echo "<td><input type='text' name='alergiaalim' size='50'  value='{$alergiaalim}' {$acesso} /></td></tr>";
        echo "</table>";

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3' >Pele</th></thead>";
        if ($icterica < '2') {
            $show_icterica = "style='display:inline;'";
        } else {
            $show_icterica = "style='display:none'";
        }
        echo"<tr><td style='width:33%;'><input type='radio' value='1' " . (($integra == '1') ? "checked" : "") . " {$enabled} name='integra_lesao' class='pele_integra_lesao OBG_RADIO'>&Iacute;ntegra</td>
				<td style='width:33%;'><input type='radio' value='1' " . (($icterica == '1') ? "checked" : "") . " {$enabled} name='icterica_anicterica' class='pele_icterica_anicterica OBG_RADIO'>Ict&eacute;rica
				<span class='icterica_escala' $show_icterica><b>Escala:</b><input type='text' name='icterica_escala' {$acesso} value='{$icterica_escala}' class='icterica_escala' size='5'></span></td>
				<td style='width:33%;'><input type='radio' value='1' " . (($hidratada == '1') ? "checked" : "") . " {$enabled} name='hidratada_desidratada' class='pele_hidratada_desidratada OBG_RADIO'>Hidratada</td>
				</tr>";
        echo"<tr>
				<td style='width:33%;'><input type='radio' value='2' " . (($integra == '2') ? "checked" : "") . " {$enabled} name='integra_lesao' class='pele_integra_lesao OBG_RADIO'>Lesionada</td>
				<td style='width:33%;'><input type='radio' value='2' " . (($icterica == '2') ? "checked" : "") . " {$enabled} name='icterica_anicterica' class='pele_icterica_anicterica OBG_RADIO'>Anict&eacute;rica</td>
				<td style='width:33%;'><input type='radio' value='2' " . (($hidratada == '2') ? "checked" : "") . " {$enabled} name='hidratada_desidratada' class='pele_hidratada_desidratada OBG_RADIO'>Desidratada</td>
				</tr>";

        echo"<tr><td style='width:33%;'><b>P&aacute;lida: </b></td><td colspan='2'><input type='radio' value='1' " . (($palida == '1') ? "checked" : "") . " {$enabled} name='palida' class='palida OBG_RADIO'>Sim <input type='radio' value='2' " . (($palida == '2') ? "checked" : "") . " {$enabled} name='palida' class='palida OBG_RADIO'>N&atilde;o</tr></td>";

        echo"<tr><td> <b>Sudoreica: </b></td><td colspan='2'><input type='radio' value='1' " . (($sudoreica == '1') ? "checked" : "") . " {$enabled} name='sudoreica' class='sudoreica OBG_RADIO'>Sim <input type='radio' value='2' " . (($sudoreica == '2') ? "checked" : "") . " {$enabled} name='sudoreica' class='sudoreica OBG_RADIO'>N&atilde;o</tr></td>";
        echo"<tr><td ><b>Fria: </b></td><td colspan='2'><input type='radio' value='1' " . (($fria == '1') ? "checked" : "") . " {$enabled} name='fria' class='fria OBG_RADIO'>Sim <input type='radio' value='2' " . (($fria == '2') ? "checked" : "") . " {$enabled} name='fria' class='fria OBG_RADIO'>N&atilde;o</tr></td>";
        echo"<tr><td ><b>Edema: </b></td><td colspan='2'><input type='radio' value='1' " . (($pele_edema == '1') ? "checked" : "") . " {$enabled} name='pele_edema' class='pele_edema OBG_RADIO'>Sim <input type='radio' value='2' " . (($pele_edema == '2') ? "checked" : "") . " {$enabled} name='pele_edema' class='pele_edema OBG_RADIO'>N&atilde;o</tr></td>";
        echo"<tr><td ><b>Descama&ccedil;&atilde;o: </b></td><td colspan='2'><input type='radio' value='1' " . (($descamacao == '1') ? "checked" : "") . " {$enabled} name='descamacao' class='descamacao OBG_RADIO'>Sim <input type='radio' value='2' " . (($descamacao == '2') ? "checked" : "") . " {$enabled} name='descamacao' class='descamacao OBG_RADIO'>N&atilde;o</tr></td>";
        echo "</table>";




        /* AVALIA��O CARDIORESPIRAT�RIA */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3' >Avalia&ccedil;&atilde;o CardioRespirat&oacute;ria</th></thead>";
        echo"<tr><td style='width:33%;'> <input type='checkbox' value='1' " . (($t_assimetrico) ? "checked" : "") . " {$enabled} name='t_assimetrico'>T&oacute;rax Assim&eacute;trico </td><td style='width:33%;'> <input type='checkbox' value='1' " . (($t_simetrico) ? "checked" : "") . " {$enabled} name='t_simetrico'>T&oacute;rax Sim&eacute;trico </td><td style='width:33%;'> <input type='checkbox' value='1' " . (($eupneico) ? "checked" : "") . " {$enabled} name='eupneico'>Eupneico </td></tr>";
        echo "<tr> <td style='width:33%;'><input type='checkbox' value='1' " . (($dispneico) ? "checked" : "") . " {$enabled} name='dispneico'>Dispneico </td>";
        echo "<td style='width:33%;'><input type='checkbox' value='1' " . (($taquipneico) ? "checked" : "") . " {$enabled} name='taquipneico'>Taquipneico </td><td style='width:33%;'><input type='checkbox' value='1' " . (($mvbd) ? "checked" : "") . " {$enabled} name='mvbd'>MVBD </td></tr>";
        echo "<tr><td style='width:33%;'><input type='checkbox' value='1' " . (($roncos) ? "checked" : "") . " {$enabled} name='roncos'>Roncos </td>";
        echo "<td style='width:33%;'><input type='checkbox' value='1' " . (($sibilos) ? "checked" : "") . " {$enabled} name='sibilos'>Sibilos </td>";
        echo "<td style='width:33%;'><input type='checkbox' value='1' " . (($creptos) ? "checked" : "") . " {$enabled} name='creptos'>Creptos </td></tr>";
        echo "<tr><td style='width:33%;'><input type='checkbox' value='1' " . (($mv_diminuido) ? "checked" : "") . " {$enabled} name='mv_diminuido'>MV Diminuido </td>";
        echo "<td style='width:33%;'><input type='checkbox' value='1' " . (($bcnf) ? "checked" : "") . " {$enabled} name='bcnf'>BCNF </td>";
        echo "<td style='width:33%;'><input type='checkbox' value='1' " . (($normocardico) ? "checked" : "") . " {$enabled} name='normocardico'>Normocardico </td></tr>";
        echo "<tr><td style='width:33%;'><input type='checkbox' value='1' " . (($taquicardico) ? "checked" : "") . " {$enabled} name='taquicardico'>Tarquicardico </td>";
        echo "<td style='width:33%;' colspan='2'> <input type='checkbox' value='1' " . (($bradicardico) ? "checked" : "") . " {$enabled} name='bradicardico'>Bradicardico </td></tr>";

        echo "</table>";
        /* TIPO DE VENTILA��O Atualiza��o Eriton 11-04-2013 */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='2' >Tipo de Ventila&ccedil;&atilde;o</th></thead>";
        echo "<tr><td colspan='2'><input type='radio' value='1' " . (($ventilacao == '1') ? "checked" : "") . " {$enabled} name='tipo_ventilacao' class='tipo_ventilacion'>Espont&acirc;nea
		<input type='radio' value='2' " . (($ventilacao >= '2') ? "checked" : "") . " {$enabled} name='tipo_ventilacao' class='tipo_ventilacion'>Traqueostomia</td></tr>";
        $show_ventilacion = "style='display:none'";

        echo "<tr><td><span $show_ventilacion class='ventilacion' >Tipo de Dispositivo: <select {$enabled} name = 'tipo_ventilacao1' >
				<option value='-1'>  </option>
				<option value='2' " . (($ventilacao == '2') ? "selected" : "") . " >Metalica</option>
				<option value='3' " . (($ventilacao == '3') ? "selected" : "") . " >Plastica</option></select></span>";
        if ($vent_metalica == 0) {
            $vent_geral = $ventilacao_plastica_num;
        } else {
            $vent_geral = $vent_metalica;
        }
        echo"<td width='70%'><span class='ventilacion'><b>N&ordm; </b><input type='text' maxlength='5' size='5' name='num_ventilacao' id='num_ventilacao' {$acesso} value='{$vent_geral}'></td></span></tr>";
        echo "<tr><td><span class='ventilacion' $show_ventilacion>Modo de Ventila&ccedil;&atilde;o: <select {$enabled} name = 'tipo_ventilacao2' class='tipo_ventilacao2'>
				<option value='-1'>  </option>
				<option value='1' " . (($tipo_ventilacao2 == '1') ? "selected" : "") . " {$enabled} >Espontanea</option>
				<option value='2' " . (($tipo_ventilacao2 == '2') ? "selected" : "") . ">Equipamento</option></select></span></td>";
        echo "<td class='bipap' style='display:none'><b>BIPAP</b> <input type='radio' value='1' " . (($bipap == '1') ? "checked" : "") . " {$enabled} name='bipap' class='vm_bipap'>Cont&iacute;nuo <input type='radio' value='2' " . (($bipap == '2') ? "checked" : "") . " name='bipap' class='vm_bipap'>Intermitente</td></tr>";
        echo "</table>";


        /* OXIGENIO */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Oxig&ecirc;nio</th></thead>";
        echo"<tr>
                <td colspan='4'><b>Oxig&ecirc;nio</b>
		  <select name='oxigenio_sn' {$enabled} class='oxigenio_sn COMBO_OBG'>
                    <option value ='-1'>  </option>
		    <option value ='n' " . (($oxigenio_sn == 'n') ? "selected" : "") . "  >N&atilde;o</option>
		    <option value ='s' " . (($oxigenio_sn == 's') ? "selected" : "") . "   >Sim</option>
                  </select>
                </td>
            </tr>";

    //20-11-2014
        if ($oxigenio_sn == 's') {
            $estilo = '';
        } else {
            $estilo = " style='display:none;' ";
        }
        //ITENS DO OXIGENIO
        echo "<tr>
                 <td colspan='4' >
                  <table id='oxigenio_span' $estilo style='width:100%;'>";


        $item_oxigenio_classe = "class='item_oxigenio'";

        echo"<tr><td colspan='3'><input type='radio' $item_oxigenio_classe value='1' " . (($oxigenio == '1') ? "checked" : "") . " {$enabled}  name='oxigenio'>Cateter de Oxig&ecirc;nio</td><td>N&ordm; <input type='text' id='vent_cateter_oxigenio_num' name='vent_cateter_oxigenio_num' value='{$vent_cateter_oxigenio_num}' {$acesso} size = '5' maxlength='5' > <input type = 'text'  id='vent_cateter_oxigenio_quant' name='vent_cateter_oxigenio_quant' value='{$vent_cateter_oxigenio_quant}' {$acesso} size = '5' maxlength='5' >L/MIN </td></tr>";
        echo "<tr><td colspan='3'><input type='radio' $item_oxigenio_classe value='2' " . (($oxigenio == '2') ? "checked" : "") . " {$enabled} name='oxigenio'>Cateter tipo &Oacute;culos </td><td><input type='text' maxlength='5' size='5' name='vent_cateter_oculos_num'  value='{$vent_cateter_oculos_num}' {$acesso}>L/MIN</td></tr>";
        echo "<tr><td colspan='3'><input type='radio' $item_oxigenio_classe value='3' " . (($oxigenio == '3') ? "checked" : "") . " {$enabled} name='oxigenio'>M&aacute;scara Reservat&oacute;rio</td><td><input type='text'  maxlength='5' size='5' name='vent_respirador_tipo' value='{$vent_respirador_tipo}' {$acesso}>L/MIN</td></tr>";
        echo"<tr><td colspan='3'><input type='radio' $item_oxigenio_classe value='4' " . (($oxigenio == '4') ? "checked" : "") . " {$enabled} name='oxigenio'>Mascara de Ventury </td><td><input type='text' maxlength='5' size='5' name='vent_mascara_ventury' value='{$vent_mascara_ventury}' {$acesso}>% </td></tr>";
        echo "<tr><td colspan='3'><input type='radio' $item_oxigenio_classe value='5' " . (($oxigenio == '5') ? "checked" : "") . " {$enabled} name='oxigenio'>Concentrador </td><td><input type='text'  maxlength='5' size='5' name='vent_concentrador_num' value='{$vent_concentrador_num}' {$acesso}>L/MIN</td></tr>";

        echo "</table>";
        echo "</td></tr>";


        echo "</table>";

        /* ASPIRA��ES */

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Aspira&ccedil;&otilde;es - <select name='alt_aspiracoes' class='alt_aspiracoes COMBO_OBG' {$enabled}>
                    <option value ='-1'>  </option>
                    <option value ='n' " . (($alt_aspiracoes == 'n') ? "selected" : "") . ">N&atilde;o</option>
                    <option value ='s' " . (($alt_aspiracoes == 's') ? "selected" : "") . ">Sim</option>
                    </select></th></thead>";

        if ($alt_aspiracoes >= 's') {
            $estilo = "style='display:table'";
        } else {
            $estilo = "style='display:none'";
        }

       echo"<tr class='aspiracoes_span' $estilo>"
               . "<td colspan='4'>"
               . "   N&uacute;mero de Aspira&ccedil;&otilde;es/Dia: "
               . "   <input type='text' maxlength='5' size='5' value='{$aspiracoes_num}' {$acesso} name='aspiracoes_num' class='aspiracoes_num'>"
               . "</td>"
          . "</tr>";
        echo"<tr class='aspiracoes_span' {$estilo}>
                <td colspan='4'>
                <label>
                  <b>Secre&ccedil;&atilde;o</b>
                </label>
		<select name= 'aspiracao_secrecao' id='aspiracao_secrecao'  {$enabled} class='COMBO_OBG'>
                    <option value =''>  </option>
                    <option value ='a' " . (($aspiracao_secrecao == 'a') ? "selected" : "") . "  >Ausente</option>
                    <option value ='p' " . (($aspiracao_secrecao == 'p') ? "selected" : "") . "   >Presente</option>
		</select>
                </td>
            </tr>";

        if ($aspiracao_secrecao == 'p') {
            $estilo = '';
        } else {
            $estilo = " style='display:none;' ";
        }

        //CARACTERISTICAS DA SECRE��O Atualiza��o Eriton 01-04-2013
        echo "<tr><td colspan='4' >";


        echo"<tr class='secrecao_span' $estilo><td colspan = '3' width ='30%'><b>Caracter&iacute;sticas da secre&ccedil;&atilde;o</b></td></tr>";
        echo "<tr class='secrecao_span' $estilo>"
             . "<td colspan = '3'>"
                . "<input type='radio' value='6' " . (($caracteristicas_sec == '6') ? "checked" : "") . " {$enabled} name='caracteristicas_sec' class='caracteristicas_sec'>"
                . "Fluida "
          . " </td>";
          echo "<td colspan = '3'>"
                . "<input type='radio' value='1' " . (($cor_sec == '1') ? "checked" : "") . " {$enabled} name='cor_sec' class='cor_sec'>"
                . "  Clara  "
               . "</td>"
         . "</tr>";
        echo "<tr class='secrecao_span' $estilo>"
                . "<td colspan = '3'>"
                . "  <input type='radio' value='2' " . (($caracteristicas_sec == '2') ? "checked" : "") . " {$enabled} name='caracteristicas_sec' class='caracteristicas_sec'>"
                    . "Espessa "
               . "</td>";
            echo "<td colspan = '3'>"
                . "   <input type='radio' value='3' " . (($cor_sec == '3') ? "checked" : "") . " {$enabled} name='cor_sec' class='cor_sec'>"
                . "   Amarelada "
               . "</td>"
          . "</tr>";
        echo "<tr class='secrecao_span' $estilo>"
                . "<td colspan ='3'> </td>";
             echo "<td colspan = '3'>"
                 . "<input type='radio' value='4' " . (($cor_sec == '4') ? "checked" : "") . " {$enabled} name='cor_sec' class='cor_sec'> "
                 . "Esverdeada"
               . "</td>"
         . "</tr>";
        echo "<tr class='secrecao_span' $estilo>"
                 . "<td colspan ='3'> </td>";
              echo "<td colspan = '3'>"
                        . "<input type='radio' value='5' " . (($cor_sec == '5') ? "checked" : "") . " {$enabled} name='cor_sec' class='cor_sec'> "
                        . " Sanguinolenta"
                . "</td>"
          . "</tr>";
        //Fim atualiza��o Eriton 01-04-2013



        echo "</table>";
        ////AVALIACAO ABDOMINAL//////

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Avalia&ccedil;&atilde;o Abdominal</th></thead>";
        //atualiza��o Eriton 01-04-2013
        echo "<tr> <td style='width:25%;'> <input type='checkbox' value='1' " . (($plano) ? "checked" : "") . " {$enabled} name='plano'>Plano </td>
		<td style='width:25%;'> <input type='checkbox' value='1' " . (($flacido) ? "checked" : "") . " {$enabled} name='flacido'>Flacido </td>
		<td style='width:25%;'> <input type='checkbox' value='1' " . (($globoso) ? "checked" : "") . " {$enabled} name='globoso'>Globoso </td>
		<td style='width:25%;'> <input type='checkbox' value='1' " . (($distendido) ? "checked" : "") . " {$enabled} name='distendido'>Distendido </td>
		</tr>";

        //Atualiza��o Eriton 23-03-2013
        echo "<tr> <td style='width:25%;'> RHA<select name='rha' class='COMBO_OBG'  {$enabled}>
		<option value='p' " . (($rha == 'p') ? "selected" : "") . ">Presente</option>
		<option value='a' " . (($rha == 'a') ? "selected" : "") . ">Ausente</option></select> </td>
		<td style='width:25%;'> <input type='checkbox' value='1' " . (($indo_palpacao) ? "checked" : "") . " {$enabled} name='indo_palpacao'>Indolor a Palpa&ccedil&atilde;o </td>
		<td style='width:25%;'> <input type='checkbox' value='1' " . (($doloroso) ? "checked" : "") . " {$enabled} name='doloroso'>Doloroso </td>
		<td style='width:25%;'> <input type='checkbox' value='1' " . (($timpanico) ? "checked" : "") . " {$enabled} name='timpanico'>Timp&acirc;nico </td></tr>";

        echo "</table>";
        // Fim Atualiza��o Eriton 23-03-2013

        /* VIA ALIMENTA��O */


        $this->dietas($id,$v,1);
        $this->itensDietas($id,$v,1);


        //avaliacao genital

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Avalia&ccedil;&atilde;o Genital</th></thead>";

        echo "<tr><td style='width:25%;'> &Iacute;ntegra </td> <td style='width:75%;' colspan='3'><input type = 'radio' value='1'" . (($integra_genital == '1') ? "checked" : "") . " {$enabled} name= 'integra_genital' >Sim <input type = 'radio' value='2' " . (($integra_genital == '2') ? "checked" : "") . " {$enabled} name= 'integra_genital'> N&atilde;o </tr></td>";
        echo "<tr><td style='width:25%;'> Secre&ccedil;&atilde;o </td> <td style='width:75%;' colspan='3'><input type = 'radio' value='1' " . (($secrecao == '1') ? "checked" : "") . " {$enabled} name= 'secrecao' >Sim <input type = 'radio' value='2' " . (($secrecao == '2') ? "checked" : "") . " {$enabled} name= 'secrecao' >N&atilde;o  </tr></td>";
        echo "<tr><td style='width:25%;'> Sangramento </td> <td style='width:75%;' colspan='3'><input type = 'radio' value='1' " . (($sangramento == '1') ? "checked" : "") . " {$enabled} name= 'sangramento' >Sim <input type = 'radio' value='2' " . (($sangramento == '2') ? "checked" : "") . " {$enabled} name= 'sangramento' > N&atilde;o </tr></td>";
        echo "<tr><td style='width:25%;'> Prurido </td> <td style='width:75%;' colspan='3'><input type = 'radio' value='1' " . (($prurido == '1') ? "checked" : "") . " {$enabled} name= 'prurido' >Sim <input type = 'radio' value='2' " . (($prurido == '2') ? "checked" : "") . " {$enabled} name= 'prurido' >N&atilde;o  </tr></td>";
        echo "<tr><td style='width:25%;'> Edema </td> <td style='width:75%;' colspan='3'><input type = 'radio' value='1' " . (($edema == '1') ? "checked" : "") . " {$enabled} name= 'edema' >Sim <input type = 'radio' value='2'" . (($edema == '2') ? "checked" : "") . " {$enabled} name= 'edema' >N&atilde;o  </tr></td>";
        echo "</table>";

        /* T�PICOS EM USO */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Topicos em Uso</th></thead>";
        echo"<tr><td colspan='4' >
		<b>Topicos: </b>
		<select name='topicos_sn' class='COMBO_OBG'  {$enabled} id='topicos_sn2'>
		<option value ='-1' > </option>
		<option value ='n' " . (($topicos_sn == 'n') ? "selected" : "") . "  >N&atilde;o</option>
				<option value ='s' " . (($topicos_sn == 's') ? "selected" : "") . "   >Sim</option>
						</select>";

        if ($v != 1 && $topicos_sn == 's') {
            $estilo = '';
            //$classe = "class'del-dreno'";
        } else {
            $estilo = " style='display:none;' ";
        }
        echo"<span name='topicos' {$estilo} id='topicos_span2'><input type='text' size='110' name='topico' class='topico_add' size=75/><button id='add-topicos' tipo=''  >+ </button></span>";

        //	  ITENS DE topicos


        echo "<tr><td colspan='4' >";
        echo "<table id='topico' style='width:100%;'>";
        if ($topicos_sn == 's') {
            $sql4 = "select * from avaliacao_enf_topicos where avaliacao_enf_id = {$id_aval}";
            $result4 = mysql_query($sql4);

            while ($row4 = mysql_fetch_array($result4)) {
                $local_topico = $row4['LOCAL'];
                $frequencia = $row4['FREQUENCIA'];
                if ($v == 1) {
                    $classe = '';
                } else {
                    $classe = "class='del-suplemento'";
                }

                echo "<tr><td colspan='2' class='topico' desc='{$local_topico}' > <img src='../utils/delete_16x16.png'  {$classe} title='Excluir' border='0' {$enabled} > {$local_topico} Frequ&ecirc;ncia <input type='text' name='local_topico'  value='{$frequencia}' {$acesso} > </td> </tr>";
            }
        }
        if (!empty($ev->topico)) {
            foreach ($ev->topico as $a) {
                echo "<tr><td>{$a}</td></tr>";
            }
        }
        echo "</table>";
        echo "</td></tr>";


        echo "</table>";



        /* DISPOSITIVO INTRAVENOSO */
        $this->dispositivosIntravenosos($id,$v);

        /* DISPOSITIVOS PARA ELIMINA��ES E/OU EXCRE��ES */
        //Atualiza��o Eriton 04-04-2013
        echo "<table class='mytable' style='width:95%;'>";
        if ($eliminaciones == '2') {
            $show_eliminaciones = '';
        } else {
            $show_eliminaciones = "style='display:none' ";
        }
        echo "<thead><tr><th colspan='3' >Dispositivos para elimina&ccedil;&atilde;o e/ou excre&ccedil;&otilde;es <select name='eliminaciones' class='eliminaciones COMBO_OBG' " . (($eliminaciones == '-1') ? "selected" : "") . " {$enabled}><option value='-1'>   </option><option value='1' " . (($eliminaciones == '1') ? "selected" : "") . " {$enabled}> N&atilde;o </option><option value='2' " . (($eliminaciones == '2') ? "selected" : "") . " {$enabled}>Sim</option></select></th></thead>";
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td style='width:33%;'> <input type = 'checkbox' value='1' " . (($fralda) ? "checked" : "") . " {$enabled} name='fralda' >Fralda Unidade </td><td><input type = 'text' size='5' value='{$fralda_dia}' {$acesso} maxlength='5' name='fralda_dia'> Dia</td><td>
		Tamanho:<input type = 'radio'  value='1' " . (($tamanho_fralda == '1') ? "checked" : "") . " {$enabled}  name='tamanho_fralda' >P <input type = 'radio'  value='2' " . (($tamanho_fralda == '2') ? "checked" : "") . " {$enabled} name='tamanho_fralda' >M <input type = 'radio'  value='3' " . (($tamanho_fralda == '3') ? "checked" : "") . " {$enabled}  name='tamanho_fralda' >G <input type = 'radio'  value='4' " . (($tamanho_fralda == '4') ? "checked" : "") . " {$enabled}   name='tamanho_fralda' >XG</td></tr>";
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td style='width:33%;'> <input type = 'checkbox' value='1' " . (($urinario) ? "checked" : "") . " {$enabled}   name='urinario' >Dispositivo urin&aacute;rio </td><td colspan='2'>N&ordm;<input type = 'text' size='5' value='{$quant_urinario}' {$acesso} maxlength = '5'   name='quant_urinario' >  </td></tr>";
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td>  <input type = 'checkbox'  value='1' " . (($sonda) ? "checked" : "") . " {$enabled}   name='sonda' >Sonda de Foley </td><td colspan='2'>N&ordm;<input type = 'text' size='5' value='{$quant_foley}' {$acesso} maxlength='5' name='quant_foley' >  </td></tr>";
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td><input type = 'checkbox'  value='1' " . (($sonda_alivio) ? "checked" : "") . " {$enabled}   name='sonda_alivio' >Sonda de Al&iacute;vio </td><td colspan='2'>N&ordm;<input type = 'text' size='5' maxlength='5' value='{$quant_alivio}' {$acesso} name='quant_alivio' > </td></tr>";
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td><input type = 'checkbox'  value='1' " . (($colostomia) ? "checked" : "") . " {$enabled}   name='colostomia' >Colostomia Placa/Bolsa</td><td colspan='2'>N&ordm;<input type = 'text' size='5' maxlength='5' value='{$quant_colostomia}' {$acesso} name='quant_colostomia' >  </td></tr>";
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td><input type = 'checkbox'  value='1' " . (($iliostomia) ? "checked" : "") . " {$enabled}   name='iliostomia' >Iliostomia Placa/Bolsa</td><td colspan='3'>N&ordm;<input type = 'text' size='5' maxlength='5' value='{$quant_iliostomia}' {$acesso} name='quant_iliostomia' >  </td></tr>";
        echo "<tr class='eliminaciones_span' $show_eliminaciones><td><input type = 'checkbox' value='1' " . (($cistostomia) ? "checked" : "") . " {$enabled}  name='cistostomia' >Cistostomia</td><td colspan='2'>N&ordm;<input type = 'text' size='5' value='{$quant_cistostomia}' {$acesso} name='quant_cistostomia'></td></tr>";
        echo "</table>";
        //Fim Atualiza��o Eriton 04-04-2013
        /* DRENOS */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Drenos</th></thead>";
        echo"<tr><td colspan='4' >
		<b>Drenos: </b>
		<select name='dreno_sn' class='dreno_sn2 COMBO_OBG'  {$enabled}>
		<option value ='-1' > </option>
		<option value ='n' " . (($dreno_sn == 'n') ? "selected" : "") . "  >N&atilde;o</option>
				<option value ='s' " . (($dreno_sn == 's') ? "selected" : "") . "   >Sim</option>
						</select>
						&nbsp;&nbsp;";
        //<span name='dreno' id='dreno_span'><b>Tipo:</b><input type='text' name='dreno'  size=50/><button id='add-dreno' tipo=''  > + </button></span></td></tr>";

        if ($v != 1 && $dreno_sn == 's') {
            $estilo = '';
            //$classe = "class'del-dreno'";
        } else {
            $estilo = " style='display:none;' ";
        }
        echo "<span name='dreno2'  {$estilo} class='dreno_span2' ><b>Tipo:</b><input type='text' name='dreno'  {$acesso} size=50/><button id='add-dreno' tipo='' {$acesso}  > + </button></span></td></tr>";
        // colocar drenos

        /* ITENS DO DRENO */
        echo "<tr><td colspan='4' >";
        echo "<table id='dreno' style='width:100%;'>";
        if ($dreno_sn == 's') {
            $sql4 = "select * from avaliacao_enf_dreno where avaliacao_enf_id = {$id_aval}";
            $result4 = mysql_query($sql4);

            while ($row4 = mysql_fetch_array($result4)) {
                $local_dreno = $row4['LOCAL'];
                if ($v == 1) {
                    $classe = '';
                } else {
                    $classe = "class='del-dreno'";
                }
                echo "<tr><td colspan='2' class='dreno' desc='{$local_dreno}' > <img src='../utils/delete_16x16.png'  {$classe} title='Excluir' border='0' {$enabled} > {$local_dreno} Local: <input type='text' name='local_dreno'  value='{$local_dreno}' {$acesso} > </td> </tr>";
            }
        }
        if (!empty($ev->dreno)) {
            foreach ($ev->dreno as $a) {
                echo "<tr><td>{$a}</td></tr>";
            }
        }
        echo "</table>";
        echo "</td></tr>";

        echo "</table>";



        /* FERIDAS */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Feridas </th></tr></thead>";
        echo"<tr><td colspan='4' ><b>Feridas: </b>
		<select name=feridas_sn class='COMBO_OBG' id='ferida' {$enabled}>
		<option value ='-1' > </option>
		<option value ='n' " . (($feridas_sn == 'n') ? "selected" : "") . "  >N&atilde;o</option>
				<option value ='s' " . (($feridas_sn == 's') ? "selected" : "") . "   >Sim</option>
						</select>

						</td></tr>";

        echo "</table>";

        echo "<span class='quadro_feridas_sn'>";
        echo "<div id='combo-div-quadro-feridas'>";
        $sql_feridas = "SELECT
                    ID
                FROM
                    quadro_feridas_enfermagem
                WHERE
                    AVALIACAO_ENF_ID = '$id_aval'";
        $result_feridas = mysql_query($sql_feridas);
        while($row = mysql_fetch_array($result_feridas)){
            $this->feridas($row['ID'],$v);
        }
        echo "</div>";
        if($v ==0){
        echo "<br><button id='nova_ferida' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Nova Ferida</span></button><br><br>";
        }
//        echo "<br><a id='nova_ferida' tipo='' class='incluir' contador='1'>Nova Ferida</a><br>";
        echo "</span>";


        /* SINAIS VITAIS */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><th colspan='4' >Sinais vitais </th></tr></thead>";
        echo "<tr><td style='width:33%;'><b>PA Sist&oacute;lica:</b></td><td colspan='3'><input type='texto' class='numerico1 OBG' size='6' name='pa_sistolica' value='{$pa_sistolica}' {$acesso} />mmhg</td></tr>";
        echo "<tr><td><b>PA Dist&oacute;lica:</b></td><td colspan='3'><input type='texto' class='numerico1 OBG' size='6' name='pa_diastolica' value='{$pa_diastolica}' {$acesso} />MMHG</td></tr>";
        echo "<tr><td><b>FC: </b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='fc' value='{$fc}' {$acesso} />bpm</td></tr>";
        echo "<tr><td><b>FR:</b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='fr' value='{$fr}' {$acesso} />INC/MIN</td></tr>";
        echo "<tr><td><b>SPO&sup2;</b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='spo2' value='{$spo2}' {$acesso} />%</td></tr>";
        echo "<tr><td><b>Temperatura:</b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='temperatura' value='{$temperatura}' {$acesso} />&deg;C</td></tr>";
        echo "</tbale>";


        /* AVALIA��O DA DOR */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4'>Avalia&ccedil;&atilde;o da dor</th></tr></thead>";
        echo "<tr><td colspan='4' >
		<b>DOR: </b>
		<select name='dor_sn' class='COMBO_OBG' id='dor_sn2'  {$enabled}>
		<option value ='-1' " . (($dor_sn == null) ? "selected" : "") . "  > </option>
				<option value ='n' " . (($dor_sn == 'n') ? "selected" : "") . "  >N&atilde;o</option>
						<option value ='s' " . (($dor_sn == 's') ? "selected" : "") . "   >Sim</option>'
								</select>
								&nbsp;&nbsp;";

        if ($v != 1 && $dor_sn == 's') {
            $estilo = '';
        } else {
            $estilo = " style='display:none;' ";
        }
        echo"<span name='dor_span2' {$estilo} id='dor_span2'><b>Local:</b><input type='text' name='dor'  size='50' /><button id='add-dor' tipo=''  > + </button></span></td>";


        echo "</tr>";
        echo "<tr><td colspan='4' >";
        echo "<table id='dor' style='width:100%;'>";
        echo "<table id='dor' style='width:100%;'>";
        /* if (!empty($ev->dor)) {
          foreach ($ev->dor as $a) {
          echo "<tr><td>{$a}</td></tr>";
          }
          } */
        if ($v == 1) {
            $sql = "select * from avaliacao_enf_dor where `AVALIACAO_ENF_ID` = '{$id_aval}' ";
            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {
                if ($row['PADRAO_ID'] == 5) {
                    $outrador = "<input type='text' value={$row['PADRAO']}></input>";
                } else {
                    $outrador = "";
                }
                echo "<tr><td class='dor1' name='{$row['LOCAL']}'><img src='../utils/delete_16x16.png' class='del-dor-ativos' title='Excluir' border='0' ><b>Local: </b><input type='text' value={$row['LOCAL']}></input>&nbsp;&nbsp; Escala analogica: <select name='dor" . "{$row['LOCAL']}" . "' class='COMBO_OBG' style='background-color:transparent;'>
                    <option value='-1' selected></option>
                    <option value='1' " . (($row['INTENSIDADE'] == '1') ? "selected" : "") . ">1</option>
                    <option value='2' " . (($row['INTENSIDADE'] == '2') ? "selected" : "") . ">2</option>
                    <option value='3' " . (($row['INTENSIDADE'] == '3') ? "selected" : "") . ">3</option>
                    <option value='4' " . (($row['INTENSIDADE'] == '4') ? "selected" : "") . ">4</option>
                    <option value='5' " . (($row['INTENSIDADE'] == '5') ? "selected" : "") . ">5</option>
                    <option value='6' " . (($row['INTENSIDADE'] == '6') ? "selected" : "") . ">6</option>
                    <option value='7' " . (($row['INTENSIDADE'] == '7') ? "selected" : "") . ">7</option>
                    <option value='8' " . (($row['INTENSIDADE'] == '8') ? "selected" : "") . ">8</option>
                    <option value='9' " . (($row['INTENSIDADE'] == '9') ? "selected" : "") . ">9</option>
                    <option value='10' " . (($row['INTENSIDADE'] == '10') ? "selected" : "") . ">10</option></select> &nbsp;&nbsp;<b>Padr&atilde;o:</b> <select name='padraodor' class='padrao_dor COMBO_OBG' style='background-color:transparent;'>
                    <option value='-1' selected></option>
                    <option value='1' " . (($row['PADRAO_ID'] == '1') ? "selected" : "") . ">Pontada</option>
                    <option value='2' " . (($row['PADRAO_ID'] == '2') ? "selected" : "") . ">Queimação</option>
                    <option value='3' " . (($row['PADRAO_ID'] == '3') ? "selected" : "") . ">Latejante</option>
                    <option value='4' " . (($row['PADRAO_ID'] == '4') ? "selected" : "") . ">Peso</option>
                    <option value='5' " . (($row['PADRAO_ID'] == '5') ? "selected" : "") . ">Outro</option></select></b>{$outrador} </td></tr>";
            }
        } else {
            $sql = "select * from avaliacao_enf_dor where `AVALIACAO_ENF_ID` = '{$id_aval}' ";
            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {
                if (($row['PADRAO_ID'] == 5) || ($row['PADRAO_ID'] == 0)) {
                    $outrador = "<input type='text' value={$row['PADRAO']}></input>";
                } else {
                    $outrador = "";
                }
                echo "<tr><td class='dor1' name='{$row['LOCAL']}' width='30%'><img src='../utils/delete_16x16.png' class='del-dor-ativos' title='Excluir' border='0' ><b>Local: </b>{$row['LOCAL']}</td><td width='70%' colspan='2'> Escala analogica: <select name='dor" . "{$row['LOCAL']}" . "' class='COMBO_OBG' style='background-color:transparent;'>
                    <option value='-1' selected></option>
                    <option value='1' " . (($row['INTENSIDADE'] == '1') ? "selected" : "") . ">1</option>
                    <option value='2' " . (($row['INTENSIDADE'] == '2') ? "selected" : "") . ">2</option>
                    <option value='3' " . (($row['INTENSIDADE'] == '3') ? "selected" : "") . ">3</option>
                    <option value='4' " . (($row['INTENSIDADE'] == '4') ? "selected" : "") . ">4</option>
                    <option value='5' " . (($row['INTENSIDADE'] == '5') ? "selected" : "") . ">5</option>
                    <option value='6' " . (($row['INTENSIDADE'] == '6') ? "selected" : "") . ">6</option>
                    <option value='7' " . (($row['INTENSIDADE'] == '7') ? "selected" : "") . ">7</option>
                    <option value='8' " . (($row['INTENSIDADE'] == '8') ? "selected" : "") . ">8</option>
                    <option value='9' " . (($row['INTENSIDADE'] == '9') ? "selected" : "") . ">9</option>
                    <option value='10' " . (($row['INTENSIDADE'] == '10') ? "selected" : "") . ">10</option></select> &nbsp;&nbsp;<b>Padr&atilde;o :</b> <select name='padraodor' class='padrao_dor COMBO_OBG' style='background-color:transparent;'>
                    <option value='-1' selected></option>
                    <option value='0' " . (($row['PADRAO_ID'] == '0') ? "selected" : "") . ">Outro</option>
                    <option value='1' " . (($row['PADRAO_ID'] == '1') ? "selected" : "") . ">Pontada</option>
                    <option value='2' " . (($row['PADRAO_ID'] == '2') ? "selected" : "") . ">Queimação</option>
                    <option value='3' " . (($row['PADRAO_ID'] == '3') ? "selected" : "") . ">Latejante</option>
                    <option value='4' " . (($row['PADRAO_ID'] == '4') ? "selected" : "") . ">Peso</option>
                    <option value='5' " . (($row['PADRAO_ID'] == '5') ? "selected" : "") . ">Outro</option></select>{$outrador} </td></tr>";
            }
        }
        echo "</table>";


        echo "</td></tr></table>";

        // FECHA LISTANDO
        echo "</td></tr></table>";

        /* SCORE PACIENTE */

        echo "<table class='mytable' style='width:95%;'>";
        if ($this->plan == 7 || $this->plan == 27 || $this->plan == 28 || $this->plan == 29 || $this->plan == 80) {

            $this->score_petrobras($dados,$id_aval,$v,1);
        } else{

            if($v == 1 ){
                if($modeloScoreNead == 2016){

                        \App\Controllers\Score::scoreNead2016Imprimir($paciente, $id_aval, "avaliacaoenfermagem", $classificacaoNead, 'view');

                }else {
                    $this->score_abmid($dados, $id_aval, $v, 2);
                    $this->scoreNead($id_aval, ($v == 1 ? 'disabled' : ''));
                }

            }else{
                \App\Controllers\Score::scoreNead2016($paciente, '', "avaliacaoenfermagem", '');


            }

        }


        //echo "<table class='mytable' width='100%'>";
        echo "</table>";
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th colspan='3' ><b>Classifica&ccedil;&atilde;o Score</b></th>";
        echo "</tr></thead>";
        $preJustificativa = $pre_justificativa;
        if($v == 0){
            $preJustificativa = '';
        }

        echo"<tr>
                <td colspan='3'>
                    <textarea cols='100' rows='2' type='text' id='prejustparecer' name='pre_justificativa'
                        readonly size='100'>
                        {$preJustificativa}
                        </textarea>
                </td>
            </tr>";
        echo "</table>";

        $this->servicoMultidisciplinarHospitalar($arrayMultidisciplinar,$v,$dataSistema);
        /* PAD ENFERMAGEM */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3' >PAD enfermagem</th></tr></thead>";
        echo "<tr><td colspan='3' ><textarea cols=90 rows=5 name='pad'   class='OBG'  {$acesso} >{$pad}</textarea></td></tr>";
        echo "</table>";
        echo "</form></div>";





        if ($acesso != "readonly") {
            //echo "<button id='teste' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Teste</span></button></span>";
            echo "<button id='salvar-ficha-avaliacao' paciente='{$paciente}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
            echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
        }
    }

    ///////////////////////////////FIM FICHA AVALIACAO ENFERMAGEM///////////////////
    /////////////////////////////INICIO FICHA EVOLUCAO DE ENFERMAGEM///////////////////

    public function listar_ficha_evolucao_enfermagem($id) {

        ////jeferson 2012-10-28

        $id = anti_injection($id, "numerico");
        $sql3 = "select Max(ID)  from evolucaoenfermagem where PACIENTE_ID = '{$id}' ";
        $r2 = mysql_query($sql3);
        $maior_cap = mysql_result($r2, 0);


        $sql3 = "select DATE_FORMAT(DATA,'%d-%m-%Y')  from evolucaoenfermagem where id = '{$maior_cap}' ";
        $r2 = mysql_query($sql3);
        $data_cap = mysql_result($r2, 0);




        $datalimit = date('d/m/Y', strtotime("+2 days", strtotime($data_cap)));


        $sql = "SELECT
		DATE_FORMAT(evenf.DATA,'%d/%m/%Y %H:%i:%s') as fdata,
                DATE_FORMAT(evenf.DATA_EVOLUCAO,'%d/%m/%Y') as data_evolucao,
		evenf.ID,
		evenf.PACIENTE_ID as idpaciente,
		u.nome,
		u.tipo as utipo,
		u.conselho_regional,
		p.nome as paciente
		FROM
		evolucaoenfermagem as evenf LEFT JOIN
		usuarios as u ON (evenf.USUARIO_ID = u.idUsuarios) INNER JOIN
		clientes as p ON (evenf.PACIENTE_ID = p.idClientes)

		WHERE
		evenf.PACIENTE_ID = '{$id}'
		ORDER BY
		evenf.ID DESC";
        //$r = mysql_query($sql);
        $r = mysql_query($sql);
        $flag = true;
        while ($row = mysql_fetch_array($r)) {
            $compTipo = $row['conselho_regional'];

            if ($flag) {

                $p = ucwords(strtolower($row["c.paciente"]));
                echo "<center><h1>Evolução de Enfermagem</h1></center>";
                $this->cabecalho($id);
              echo "<br/><br/>";
                echo "<div><input type='hidden' name='nova_evolucao_id' value='{$id}'></input>";

                if ($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                        ['enfermagem']
                        ['enfermagem_paciente']
                        ['enfermagem_paciente_evolucao']
                        ['enfermagem_paciente_evolucao_novo'])) {
                    echo "<button id='nova_evolucao_enfermagem'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
                            <span class='ui-button-text'>Nova Evolução de Enfermagem</span>
                          </button>";
                }

                echo"</div>";
                echo "<br/><table class='mytable' style='width:100%' >";
                echo "<thead><tr>";
                echo "<th width='45%'>Usu&aacute;rio</th>";
                echo "<th>Data do sistema</th>";
                echo "<th>Data da evolu&ccedil;&atilde;o</th>";
                echo "<th width='15%' colspan ='3'> Op&ccedil;&otilde;es</th>";

                //echo "<th>Op&ccedil;&otilde;es</th>";
                echo "</tr></thead>";
                $flag = false;
            }

            $u = ucwords(strtolower($row["nome"])) . $compTipo;

            echo "<tr><td>{$u}</td><td>{$row['fdata']}</td><td>{$row['data_evolucao']}</td>";
            echo "<td><a href=?view=editarevolucaoenfermagem&id={$row['ID']}&v=1><img src='../utils/capmed_16x16.png' title='Visualizar' border='0'></a>";
            if ($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                    ['enfermagem']
                    ['enfermagem_paciente']
                    ['enfermagem_paciente_evolucao']
                    ['enfermagem_paciente_evolucao_novo'])) {
                echo "<td><a href=?view=editarevolucaoenfermagem&id={$row['ID']}&v=0><img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para nova evolu&ccedil;&atilde;o.' border='0'></a>";
            }

                echo "</td></tr>";
        }


        if ($flag) {
            if ($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                    ['enfermagem']
                    ['enfermagem_paciente']
                    ['enfermagem_paciente_evolucao']
                    ['enfermagem_paciente_evolucao_novo'])) {
                $this->nova_ficha_evolucao_enfermagem($id);
            }
            return;
        }
        echo "</table>";
    }

    public function nova_ficha_evolucao_enfermagem($id) {

        $id = anti_injection($id, "numerico");
        $result = mysql_query("SELECT p.nome FROM clientes as p WHERE p.idClientes = '{$id}'");

        $fae = new FichaEvolucao();
        while ($row = mysql_fetch_array($result)) {
            $fae->pacienteid = $id;
            $fae->pacientenome = ucwords(strtolower($row["nome"]));
        }

        $this->ficha_evolucao_enfermagem($fae, "");
    }

    public function ficha_evolucao_enfermagem($fae, $acesso) {

        echo "<center><h1>Nova Evolu&ccedil;&atilde;o de Enfermagem</h1></center>";


        $this->cabecalho($fae);
        $this->plan;

        echo "<div id='div-ficha-avaliacao-enfermagem'><br/>";

        echo alerta();
        echo "<form>";
        echo "<input type='hidden' name='paciente'  value='{$fae->pacienteid}' />";



        /*         * ************************* EXAME FISICO ************************************ */

        echo "<br>";
        echo "<table style='width:95%;'>";
        echo "<tr><td>Visita Realizada em: </b><input class='data OBG' type='text' name='inicio' maxlength='10' size='10' /></td></tr>";
        echo "</table>";
        echo "<br>";
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' ><b><center>Exame F&iacute;sico</center></b></td></tr></thead>";
        echo "</table>";




        /* ESTADO GERAL */
        $this->estadoGeral($id,$v);


        /* ESTADO EMOCIONAL */
        $this->estadoEmocional($id,$v);

        /* NIVEL DE CONSCIENCIA Atualiza��o Eriton 04-04-2013 */
        $this->nivelConsciencia($id,$v);

        //COURO CABELUDO Atualiza��o Eriton 04-04-2013
        $this->couroCabeludo($id,$v);

        /* CONDI��O MOTORA */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan =4> <b>Condi&ccedil;&atilde;o Motora</b></th></thead>";

        echo "<tr><td style='width:25%;'><span ><input type='radio' value='1' class='cond_motora OBG_RADIO' name='cond_motora'>Deambula</span></td>
				<td style='width:25%;'><span ><input type='radio' value='2' class='cond_motora OBG_RADIO' name='cond_motora'>Deambula com Aux&iacute;lio</span></td>
				<td style='width:25%;'><span ><input type='radio' value='3' class='cond_motora OBG_RADIO' name='cond_motora'>Cadeirante</span></td>
				<td style='width:25%;'><span ><input type='radio' value='4' name='cond_motora' class='cond_motora OBG_RADIO'>Acamado</span></td></tr>";
        echo "</table>";

        /* ALERGIA MEDICAMENTOSA E ALERGIA ALIMENTAR */
        $this->alergias($id,$v);

        /* PELE  Atualiza��o Eriton 04-04-2013- Evolu��o */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3' >Pele</th></thead>";
        echo"<tr><td style='width:33%;'><span><input type='radio' value='1' name='integra_lesao' class='pele_integra_lesao OBG_RADIO'>&Iacute;ntegra</span></td><td style='width:33%;'><span><input type='radio' value='1' name='icterica_anicterica' class='pele_icterica_anicterica OBG_RADIO'>Ict&eacute;rica </span><span class='icterica_escala'><b>Escala:</b><input type='text' name='icterica_escala' class='icterica_escala' size='5'></span></td><td style='width:33%;'><span><input type='radio' value='1' name='hidratada_desidratada' class='pele_hidratada_desidratada OBG_RADIO'>Hidratada</span></td></tr>";
        echo "<tr><td style='width:33%;'><span><input type='radio' value='2' name='integra_lesao' class='pele_integra_lesao OBG_RADIO'>Lesionada</span></td><td style='width:33%;'><span><input type='radio' value='2' name='icterica_anicterica' class='pele_icterica_anicterica OBG_RADIO'>Anict&eacute;rica</span></td><td style='width:33%;'><span><input type='radio' value='2' name='hidratada_desidratada' class='pele_hidratada_desidratada OBG_RADIO'>Desidratada</span></td></tr>";
        echo"<tr><td style='width:33%;'><b>P&aacute;lida: </b></td><td colspan='2'><input type='radio' value='1' name='palida' class='palida OBG_RADIO'>Sim <input type='radio' value='2' name='palida' class='palida OBG_RADIO'>N&atilde;o</tr></td>";
        echo"<tr><td> <b>Sudoreica: </b></td><td colspan='2'><input type='radio' value='1' name='sudoreica' class='sudoreica OBG_RADIO'>Sim <input type='radio' value='2' name='sudoreica' class='sudoreica OBG_RADIO'>N&atilde;o</tr></td>";
        echo"<tr><td ><b>Fria: </b></td><td colspan='2'><input type='radio' value='1' name='fria' class='fria OBG_RADIO'>Sim <input type='radio' value='2' name='fria' class='fria OBG_RADIO'>N&atilde;o</tr></td>";
        echo"<tr><td ><b>Edema: </b></td><td colspan='2'><input type='radio' value='1' name='pele_edema'class='pele_edema OBG_RADIO'>Sim <input type='radio' value='2' name='pele_edema' class='pele_edema OBG_RADIO'>N&atilde;o</tr></td>";
        echo"<tr><td ><b>Descama&ccedil;&atilde;o: </b></td><td colspan='2'><input type='radio' value='1' name='descamacao' class='descamacao OBG_RADIO'>Sim <input type='radio' value='2' name='descamacao' class='descamacao OBG_RADIO'>N&atilde;o</tr></td>";
        echo "</table>";

        // MUCOSAS Atualiza��o Eriton 04-04-2013


        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan =4> <b>Mucosas</b></th></thead>";
        echo "<tr><td style='width:30%;'>  <input type = 'radio' value='1' name= 'mucosas' class='mucosas OBG_RADIO'>Coradas </td><td> <input type = 'radio' value='2' name= 'mucosas' class='mucosas OBG_RADIO'>  Descoradas  </td><td colspan='2'><input type = 'radio' value='3' name= 'mucosas' class='mucosas OBG_RADIO'>Hipocoradas </tr></td>";

        echo "</table>";

        // PESCO�O Atualiza��o Eriton 04-04-2013
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan =4> <b>Pesco&ccedil;o</b></th></thead>";
        echo"<tr><td colspan='4'><b>Altera&ccedil;&otilde;es</b>
				<select name='alt_pescoco' id='alt_pescoco' class='alt_pescoco COMBO_OBG'><option value ='-1'>  </option>
				<option value ='1'>N&atilde;o</option>
				<option value ='2'>Sim</option>
				</select></tr></td>";

        //ITENS DO PESCO�O
        echo "<tr><td colspan='4'>";
        echo "<table id='pescoco_span' style='width:100%;'>";
        echo "<tr class='pescoco_span'><td style='width:25%;'> N&oacute;dulos </td><td colspan=3><input type = 'radio' value='1' name= 'nodulos' class='nodulos'>Sim <input type = 'radio' value='2' name= 'nodulos' class='nodulos'> N&atilde;o </tr></td>";
        echo "<tr class='pescoco_span'><td > G&acirc;nglios Infartados </td><td colspan=3><input type = 'radio' value='1' name= 'ganglios_inf' class='ganglios_inf'>Sim <input type = 'radio' value='2' name='ganglios_inf' class='ganglios_inf'>N&atilde;o  </tr></td>";
        echo "<tr class='pescoco_span'><td > Rigidez </td><td colspan=3><input type = 'radio' value='1' name= 'rigidez' class='rigidez'>Sim <input type = 'radio' value='2' name= 'rigidez' > N&atilde;o </tr></td>";
        echo "<tr class='pescoco_span'><td > B&oacute;cio </td><td colspan=3><input type = 'radio' value='1' name= 'bocio' class='bocio'>Sim <input type = 'radio' value='2' name= 'bocio' class='bocio'>N&atilde;o  </tr></td>";
        echo "<tr class='pescoco_span'><td > Ferida </td><td colspan=3><input type = 'radio' value='1' name= 'ferida_pescoco' class='ferida_pescoco'>Sim <input type = 'radio' value='2' name= 'ferida_pescoco' class='ferida_pescoco'>N&atilde;o  </tr></td>";
        echo "</table>";
        echo "</td></tr>";
        echo "</table>";


        // T�RAX Atualiza��o Eriton 04-04-2013
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan = '4'> <b>T&oacute;rax</b></th></thead>";
        echo "<tr><td><b>AVALIA&Ccedil;&Atilde;O ANAT&Ocirc;MICA</b></td>";
        echo"<td colspan='3'><input type = 'radio' value='1' name= 'simetrico_assimetrico'  class='simetrico_assimetrico OBG_RADIO'>Sim&eacute;trico <input type = 'radio' value='2' name= 'simetrico_assimetrico' class='simetrico_assimetrico OBG_RADIO'> Assim&eacute;trico </td></tr>";
        echo "<tr><td ><b> Doloroso a Palpa&ccedil;&atilde;o</b> </td><td colspan='3'><input type='radio' value='1' name='palpacao' class='palpacao OBG_RADIO'>Sim <input type='radio' value='2' name='palpacao' class='palpacao OBG_RADIO'>N&atilde;o </td></tr>";
        echo "<tr><td ><b> G&acirc;nglios</b> </td><td colspan='3'><input type='radio' value='1' name='ganglios' class='ganglios OBG_RADIO'>Sim <input type='radio' value='2' name='ganglios'  class='ganglios'>N&atilde;o </td></tr>";
        echo "<tr><td ><b> Cicatriz </b> </td><td> <input type='radio' value='1' name='cicatriz_torax' class='cicatriz_torax OBG_RADIO'>Sim <input type='radio' value='2' name='cicatriz_torax' class='cicatriz_torax OBG_RADIO'>N&atilde;o <span class='local_cicatriz_torax' width='50%'>&nbsp; &nbsp;Local:<input type = 'text' name='local_cicatriz_torax' size='20' ></span></td></tr>";
        echo "<tr><td ><b> Afundamento </b> </td><td colspan='3'> <input type='radio' value='1' name='afundamento' class='afundamento OBG_RADIO'>Sim <input type='radio' value='2' name='afundamento' class='afundamento OBG_RADIO'>N&atilde;o </td></tr>";
        echo "<tr><td ><b> Ausculta alterada</b> </td><td> <input type='radio' value='1' name='ausculta_alterada' class='torax_sel OBG_RADIO'>Sim <input type='radio' value='2' name='ausculta_alterada' class='torax_sel OBG_RADIO'>N&atilde;o";
        echo "<span class='torax_ausculta'>&nbsp;&nbsp;&nbsp;&nbsp; MVDB<input type='radio' name='mv' id ='torax_ausculta' value='1' " . (($ausculta_alterada != '1') ? "checked" : "") . " {$enabled} class='torax_ausculta'></span></td></tr>";
        echo "<tr class='torax' " . (($ausculta_alterada == '1') ? "style='display: table-row;'" : "style='display: none;'") . "><td> <input type='checkbox' value='1' " . (($creptos == '1') ? "checked" : "") . " {$enabled} name='creptos' id='creptos' class='torax '>Creptos </td>";
        echo " <td  colspan='3'> <input type='checkbox' value='1' " . (($estertores == '1') ? "checked" : "") . " {$enabled} name='estertores' id='estertores' class='torax'>Estertores</td></tr>";

        echo "<tr><td width='50%'><span class='creptos_span'" . (($creptos == '1') ? "style='display: table-row;'" : "style='display: none;'") . "><input type='radio' name='creptos_tipo' value='1' " . (($creptos_tipo == '1') ? "checked" : "") . ">Base Esquerda</input><input type='radio' name='creptos_tipo' value='2'  " . (($creptos_tipo == '2') ? "checked" : "") . " >Base Direita</input><input type='radio' name='creptos_tipo' value='3' " . (($creptos_tipo == '3') ? "checked" : "") . " >Ambas bases</input><input type='radio' name='creptos_tipo' value='4' " . (($creptos_tipo == '4') ? "checked" : "") . " >&Aacute;pice Esquerda</input><input type='radio' name='creptos_tipo' value='5' " . (($creptos_tipo == '5') ? "checked" : "") . " >&Aacute;pice Direita</input><br><input type='radio' name='creptos_tipo' value='6' " . (($creptos_tipo == '6') ? "checked" : "") . " >Parede Anterior</input></span></td>";
        echo "<td colspan='3'><span class='estertores_span' " . (($estertores == '1') ? "style='display: table-row;'" : "style='display: none;'") . "><input type='radio' name='estertor_tipo' value='1' " . (($estertor_tipo == '1') ? "checked" : "") . ">Base Esquerda</input><input type='radio' name='estertor_tipo' value='2' " . (($estertor_tipo == '2') ? "checked" : "") . ">Base Direita</input><input type='radio' name='estertor_tipo' value='3' " . (($estertor_tipo == '3') ? "checked" : "") . ">Ambas bases</input><input type='radio' name='estertor_tipo' value='4' " . (($estertor_tipo == '4') ? "checked" : "") . ">&Aacute;pice Esquerda</input><input type='radio' name='estertor_tipo' value='5' " . (($estertor_tipo == '5') ? "checked" : "") . ">&Aacute;pice Direita</input><br><input type='radio' name='estertor_tipo' value='6' " . (($estertor_tipo == '6') ? "checked" : "") . ">Parede Anterior</input></span></td></tr>";
        echo "<tr class='torax' " . (($ausculta_alterada == '1') ? "style='display: table-row;'" : "style='display: none;'") . "><td> <input type='checkbox' value='1' " . (($roncos == '1') ? "checked" : "") . " {$enabled} name='roncos' id='roncos' class='torax'>Roncos </td>";
        echo "<td  colspan='3'> <input type='checkbox' value='1' " . (($mv_dimi == '1') ? "checked" : "") . " {$enabled} name='mv_dimi' id='mv_dimi' class='torax'>MV Diminu&iacute;dos</td></tr>";
        echo "<tr><td><span class='roncos_span' " . (($roncos == '1') ? "style='display: table-row;'" : "style='display: none;'") . "><input type='radio' name='roncos_tipo' value='1' " . (($roncos_tipo == '1') ? "checked" : "") . ">Base Esquerda</input><input type='radio' name='roncos_tipo' value='2'  " . (($roncos_tipo == '2') ? "checked" : "") . ">Base Direita</input><input type='radio' name='roncos_tipo' value='3'  " . (($roncos_tipo == '3') ? "checked" : "") . ">Ambas bases</input><input type='radio' name='roncos_tipo' value='4'  " . (($roncos_tipo == '4') ? "checked" : "") . ">&Aacute;pice Esquerda</input><input type='radio' name='roncos_tipo' value='5'  " . (($roncos_tipo == '5') ? "checked" : "") . ">&Aacute;pice Direita</input><br><input type='radio' name='roncos_tipo' value='6'  " . (($roncos_tipo == '6') ? "checked" : "") . ">Parede Anterior</input></span></td>";
        echo"<td colspan='3'><span class='mv_dimi_span' " . (($mv_dimi == '1') ? "style='display: table-row;'" : "style='display: none;'") . "><input type='radio' name='mv_tipo' value='1' " . (($mv_tipo == '1') ? "checked" : "") . ">Base Esquerda</input><input type='radio' name='mv_tipo' value='2' " . (($mv_tipo == '2') ? "checked" : "") . ">Base Direita</input><input type='radio' name='mv_tipo' value='3' " . (($mv_tipo == '3') ? "checked" : "") . ">Ambas bases</input><input type='radio' name='mv_tipo' value='4' " . (($mv_tipo == '4') ? "checked" : "") . ">&Aacute;pice Esquerda</input><input type='radio' name='mv_tipo' value='5' " . (($mv_tipo == '5') ? "checked" : "") . ">&Aacute;pice Direita</input><br><input type='radio' name='mv_tipo' value='6' " . (($mv_tipo == '6') ? "checked" : "") . ">Parede Anterior</input></span></td></tr>";

        echo "</table>";

        //PADR�O CARDIORESPIRAT�RIO Atualiza��o Eriton 04-04-2013
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Padr&atilde;o Cardiorespirat&oacute;rio </th></thead>";
        echo"<tr><td style='width:33%;'><input type='radio' value='1' name='padrao_cardiorespiratorio2' class='padrao_cardiorespiratorio_sel OBG_RADIO'>Eupneico </td><td colspan='3'><input type='radio' value='1' name='padrao_cardiorespiratorio' class='padrao_cardiorespiratorio_  OBG_RADIO'>Bulhas Card&iacute;acas NormoFon&eacute;tica (BCNF) </td></tr>";
        echo "<tr class='padrao_cardiorespiratorio'><td><input type='radio' value='2' name='padrao_cardiorespiratorio2' class='padrao_cardiorespiratorio OBG_RADIO'>Taquipne&iacute;co </td><td colspan='3'><input type='radio' value='1' name='padrao_cardiorespiratorio1' class='padrao_cardiorespiratorio OBG_RADIO'>Taquicardico </td></tr>";
        echo "<tr class='padrao_cardiorespiratorio'><td> <input type='radio' value='3' name='padrao_cardiorespiratorio2' class='padrao_cardiorespiratorio OBG_RADIO'>Bradipne&iacute;co</td><td colspan='3'><input type='radio' value='2' name='padrao_cardiorespiratorio1' class='padrao_cardiorespiratorio OBG_RADIO'>bradicardico </td></tr>";
        echo "<tr class='padrao_cardiorespiratorio'><td> <input type='radio' value='4' name='padrao_cardiorespiratorio2' class='padrao_cardiorespiratorio OBG_RADIO'>Dispne&iacute;co</td><td colspan='3'><input type='radio' value='3' name='padrao_cardiorespiratorio1' class='padrao_cardiorespiratorio OBG_RADIO'>Normocardico </td></tr>";
        echo "</table>";


        /* TIPO DE VENTILA��O Atualiza��o Eriton 11-04-2013 Evolu��o */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='2' >Tipo de Ventila&ccedil;&atilde;o</th></thead>";
        echo "<tr><td colspan='2'><input type='radio' value='1' name='tipo_ventilacao' class='tipo_ventilacion OBG_RADIO'>Espont&acirc;nea<input type='radio' value='2' name='tipo_ventilacao' class='tipo_ventilacion OBG_RADIO'>Traqueostomia</td></tr>";
        if ($ventilacao >= '2') {
            $show_ventilacion = "style='display:table'";
        } else {
            $show_ventilacion = "style='display:none'";
        }
        echo "<tr><td><span class='ventilacion' $show_ventilacion>Tipo de Dispositivo: <select name = 'tipo_ventilacao1' id='tipo_ventilacao1'>
				<option value='-1'>  </option>
				<option value='2'>Metalica</option>
				<option value='3'>Plastica</option></select></span>";
        echo"<td width='70%'><span class='ventilacion' $show_ventilacion><b>N&ordm; </b>   <input type='text' maxlength='5' size='5' name='num_ventilacao' id='num_ventilacao'> </td></span></tr>";
        echo "<tr><td><span class='ventilacion' $show_ventilacion>Modo de Ventila&ccedil;&atilde;o: <select name = 'tipo_ventilacao2' class='tipo_ventilacao2'>
				<option value='-1'>  </option>
				<option value='1'>Espontanea</option>
				<option value='2'>Equipamento</option></select></span></td>";
        echo "<td><span class='bipap'><b>BIPAP</b> <input type='radio' value='1' name='bipap' class='vm_bipap'>Cont&iacute;nuo <input type='radio' value='2' name='bipap' class='vm_bipap'>Intermitente</span></td></tr>";

        echo "</table>";


        /* OXIGENIO Atualiza��o Eriton 11-04-2013 Evolu��o */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Oxig&ecirc;nio</th></tr></thead>";
        echo "<tr><td colspan='4'><b>Oxig&ecirc;nio</b>
				<select name='oxigenio_sn' class='oxigenio_sn COMBO_OBG'><option value ='-1'>  </option>
				<option value ='n'>N&atilde;o</option>
				<option value ='s'>Sim</option>
				</select></tr></td>";
        echo "</table>";
        //ITENS DO OXIGENIO

        echo "<table class='mytable' id='oxigenio_span' style='width:95%;display:none'>";
        echo "<thead><tr><th colspan='4'>Itens do Oxig&ecirc;nio</th></tr></thead>";

        echo "<tr><td colspan='3'><input type='radio' value='1' class='item_oxigenio' name='oxigenio'>Cateter de Oxig&ecirc;nio</td><td>N&ordm; <input type = 'text'  name='vent_cateter_oxigenio_num' size = '5' maxlength='5' >&nbsp;&nbsp;&nbsp;<input type = 'text'  name='vent_cateter_oxigenio_quant' size = '5' maxlength='5' >L/MIN </td></tr>";
        echo "<tr><td colspan='3'><input type='radio' value='2' class='item_oxigenio' name='oxigenio'>Cateter tipo &Oacute;culos </td><td><input type='text' maxlength='5' size='5' name='vent_cateter_oculos_num'>L/MIN</td></tr>";
        echo "<tr><td colspan='3'><input type='radio' value='3' class='item_oxigenio' name='oxigenio'>M&aacute;scara Reservat&oacute;rio</td><td><input type='text'  maxlength='5' size='5' name='vent_respirador_tipo'>L/MIN</tr></td>";
        echo "<tr><td colspan='3'><input type='radio' value='4' class='item_oxigenio' name='oxigenio'>Mascara de Ventury </td><td><input type='text' maxlength='5' size='5' name='vent_mascara_ventury'>% </td></tr>";
        echo "<tr><td colspan='3'><input type='radio' value='5' class='item_oxigenio' name='oxigenio'>Concentrador </td><td><input type='text'  maxlength='5' size='5' name='vent_concentrador_num'>L/MIN</tr></td>";


        echo "</table>";
        echo "</td></tr>";


        echo "</table>";


        /* ASPIRA��ES Atualiza��o Eriton 11-04-2013 Evolu��o */

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><th colspan='4' >Aspira&ccedil;&otilde;es - <select name='alt_aspiracoes' class='alt_aspiracoes COMBO_OBG'>
                        <option value ='-1'>  </option>
                        <option value ='n'>N&atilde;o</option>
                        <option value ='s'>Sim</option>
                        </select></th></thead>";
        echo"<tr class='aspiracoes_span' style='display:none;'><td colspan='4'>N&uacute;mero de Aspira&ccedil;&otilde;es/Dia:<input type='text' maxlength='5' size='5' name='aspiracoes_num' class='aspiracoes_num'></td></tr>";
        echo"<tr class='aspiracoes_span' style='display:none;'><td colspan='4'><label><b>Secre&ccedil;&atilde;o</b></label>
	  	<select name= 'aspiracao_secrecao' id='aspiracao_secrecao'><option value ='-1'>  </option>
				<option value ='a'>Ausente</option>
				<option value ='p'>Presente</option>
				</select></tr></td>";


        //CARACTERISTICAS DA SECRE��O Atualiza��o Eriton 11-04-2013 Evolu��o

        echo "<tr><td colspan='4' >";

        echo"<tr class='secrecao_span' style='display:none;'><td colspan = '3' width='30%'><b>Caracter&iacute;sticas da secre&ccedil;&atilde;o</b></td></tr>";
        echo "<tr class='secrecao_span' style='display:none;'><td colspan = '3'><input type='radio' value='6' name='caracteristicas_sec' class='caracteristicas_sec'>Fluida </td>";
        echo "<td colspan = '3'><input type='radio' value='1' name='cor_sec' class='cor_sec'>Clara </td></tr>";
        echo "<tr class='secrecao_span' style='display:none;'><td colspan = '3'><input type='radio' value='2' name='caracteristicas_sec' class='caracteristicas_sec'>Espessa </td>";
        echo "<td colspan = '3'><input type='radio' value='3' name='cor_sec' class='cor_sec'>Amarelada </tr></td>";
        echo "<tr class='secrecao_span' style='display:none;'><td colspan = '3'> </td>";
        echo "<td colspan = '3'><input type='radio' value='4' name='cor_sec' class='cor_sec'> Esverdeada</td></tr>";
        echo "<tr class='secrecao_span' style='display:none;'><td colspan = '3'> </td>";
        echo "<td colspan = '3'><input type='radio' value='5' name='cor_sec' class='cor_sec'> Sanguinolenta</tr></td>";

        echo "</td></tr>";

        echo "</table>";

        //Fim Atualiza��o Eriton 28-03-2013
        ////AVALIACAO ABDOMINAL////// Atualiza��o Eriton 11-04-2013 Evolu��o

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Avalia&ccedil;&atilde;o Abdominal</th></thead>";

        echo "<tr> <td style='width:25%;'> <input type='checkbox' value='1' name='plano' class='avaliacao_abdominal OBG_RADIO'>Plano </td>
				<td style='width:25%;'> <input type='checkbox' value='1' name='flacido' class='avaliacao_abdominal OBG_RADIO'>Flacido </td>
				<td style='width:25%;'> <input type='checkbox' value='1' name='globoso' class='avaliacao_abdominal OBG_RADIO'>Globoso </td>
				<td style='width:25%;'> <input type='checkbox' value='1' name='distendido' class='avaliacao_abdominal OBG_RADIO'>Distendido </td>
				</tr>";

        echo "<tr> <td style='width:25%;'>RHA <select name='rha' id= 'rha' class='COMBO_OBG'><option value='-1'>  </option>
				<option value='p'>Presente</option>
				<option value='a'>Ausente</option></select> </td>
				<td style='width:25%;'> <input type='checkbox' value='1' name='indo_palpacao' class='avaliacao_abdominal OBG_RADIO'>Indolor a Palpa&ccedil&atilde;o </td>
				<td style='width:25%;'> <input type='checkbox' value='1' name='doloroso' class='avaliacao_abdominal OBG_RADIO'>Doloroso </td>
				<td style='width:25%;'> <input type='checkbox' value='1' name='timpanico' class='avaliacao_abdominal OBG_RADIO'>Timp&acirc;nico </td>
				</tr>";

        echo "</table>";

        /* VIA ALIMENTA��O Atualiza��o Eriton 11-04-2013 Evolu��o */

        $this->dietas($id,$v,2);
        $this->itensDietas($id,$v,2);

        //avaliacao genital Atualiza��o Eriton 11-04-2013 Evolu��o

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Avalia&ccedil;&atilde;o Genital</th></thead>";

        echo "<tr><td style='width:25%;'> &Iacute;ntegra </td> <td   colspan='3' style='width:75%;'><input type = 'radio' value='1' name= 'integra_genital' class='integra OBG_RADIO'>Sim <input type = 'radio' value='2' name= 'integra_genital' class='integra OBG_RADIO'> N&atilde;o </tr></td>";
        echo "<tr><td style='width:25%;'> Secre&ccedil;&atilde;o </td> <td   colspan='3' style='width:75%;'><input type = 'radio' value='1' name= 'secrecao' class='secrecao OBG_RADIO'>Sim <input type = 'radio' value='2' name= 'secrecao' class='secrecao OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "<tr><td style='width:25%;'> Sangramento </td> <td   colspan='3' style='width:75%;'><input type = 'radio' value='1' name= 'sangramento' class='sangramento OBG_RADIO'>Sim <input type = 'radio' value='2' name= 'sangramento' class='sangramento OBG_RADIO'> N&atilde;o </tr></td>";
        echo "<tr><td style='width:25%;'> Prurido </td> <td   colspan='3' style='width:75%;'><input type = 'radio' value='1' name= 'prurido' class='prurido OBG_RADIO'>Sim <input type = 'radio' value='2' name= 'prurido' class='prurido OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "<tr><td style='width:25%;'> Edema </td> <td   colspan='3' style='width:75%;'><input type = 'radio' value='1' name= 'edema' class='edema OBG_RADIO'>Sim <input type = 'radio' value='2' name= 'edema' class='edema OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "</table>";

        // AVALIA��O DE MEMBROS SUPERIORES Atualiza��o Eriton 04-04-2013
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='2' >Avalia&ccedil;&atilde;o de membros superiores</th></thead>";
        echo "<tr><td style='width:50%;'> Edema </td> <td style='width:50%;'><input type = 'radio' value='1' name= 'edema_mmss' class='edema_mmss OBG_RADIO'>Sim <input type = 'radio' value='2' name= 'edema_mmss' class='edema_mmss OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "<tr><td style='width:50%;'> Deformidades </td> <td style='width:50%;'><input type = 'radio' value='1' name= 'deformidades_mmss' class='deformidades_mmss OBG_RADIO'>Sim <input type = 'radio' value='2' name= 'deformidades_mmss' class='deformidades_mmss OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "<tr><td style='width:50%;'> Les&otilde;es </td> <td style='width:50%;'><input type = 'radio' value='1' name= 'lesoes_mmss' class='lesoes_mmss OBG_RADIO'>Sim <input type = 'radio' value='2' name= 'lesoes_mmss' class='lesoes_mmss OBG_RADIO'>N&atilde;o </tr></td>";

        echo "</table>";

        // AVALIA��O DE MEMBROS INFERIORES Atualiza��o Eriton 04-04-2013
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='2' >Avalia&ccedil;&atilde;o de membros inferiores</th></thead>";
        echo "<tr><td style='width:50%;'> Edema </td> <td style='width:50%;'><input type = 'radio' value='1' name= 'edema_mmii' class='edema_mmii OBG_RADIO'>Sim <input type = 'radio' value='2' name= 'edema_mmii' class='edema_mmii OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "<tr><td style='width:50%;'> Varizes </td> <td style='width:50%;'><input type = 'radio' value='1' name= 'varizes_mmii' class='varizes_mmii OBG_RADIO'>Sim <input type = 'radio' value='2' name= 'varizes_mmii' class='varizes_mmii OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "<tr><td style='width:50%;'> Deformidades </td> <td style='width:50%;'><input type = 'radio' value='1' name= 'deformidades_mmii' class='deformidades_mmii OBG_RADIO'>Sim <input type = 'radio' value='2' name= 'deformidades_mmii' class='deformidades_mmii OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "<tr><td style='width:50%;'> Les&otilde;es </td> <td style='width:50%;'><input type = 'radio' value='1' name= 'lesoes_mmii' class='lesoes_mmii OBG_RADIO'>Sim <input type = 'radio' value='2' name= 'lesoes_mmii' class='lesoes_mmii OBG_RADIO'>N&atilde;o </tr></td>";

        echo "</table>";

        //EXTREMIDADES Atualiza��o Eriton 04-04-2013
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Extremidades</th></thead>";
        echo "<tr><td colspan='4'> <input type='checkbox' value='1' name='aquecidas_ext' class='extremidades OBG_RADIO'>Aquecidas </td></tr>";
        echo "<tr><td colspan='4'> <input type='checkbox' value='1' name='oxigenadas_ext' class='extremidades OBG_RADIO'>Oxigenadas </td></tr>";
        echo "<tr><td colspan='4'> <input type='checkbox' value='1' name='frias_ext' class='extremidades OBG_RADIO'>Frias </td></tr>";
        echo "<tr><td colspan='4'> <input type='checkbox' value='1' name='cianoticas_ext' class='extremidades OBG_RADIO'>Cian&oacute;ticas </td></tr>";
        echo "<tr><td colspan='4'> <input type='checkbox' value='1' name='pulso_presente' class='extremidades OBG_RADIO'>Pulso Presente </td></tr>";


        echo "</table>";
        /* DISPOSITIVO INTRAVENOSO Atualiza��o Eriton 04-04-2013 - Evolu��o */

        $this->dispositivosIntravenososEvolucao();

      /*  echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3' >Dispositivos Intravenosos <select name='dispositivos_intravenosos' class='dispositivos_intravenosos COMBO_OBG' ><option value='-1'>   </option><option value='1'> N&atilde;o </option><option value='2'>Sim</option></select></th></thead>";
        if ($cvc >= '1') {
            $show_cvc = "style='display:table'";
        } else {
            $show_cvc = "style='display:none'";
        }
        $show_dispositivos = "style='display:none'";
        if ($dispositivos_intravenosos == '2') {
            $show_dispositivos = "style='display:block'";
        }
        echo"<tr class='dispositivos_span' ><td style='width:10%;' colspan='3'><input type='checkbox' value='1' name='cateter_venoso_central' class='valida_cvc'>CVC</td></tr>";
        echo "<tr class='cvc_span' {$show_cvc}><td style='width:10%;' colspan='2'><input type = 'radio' class='intravenosos' value='1' " . (($cvc == '1') ? "checked" : "") . " {$enabled} id='uno_lumen' name='cvc' >Uno Lumen </td><td>Local<input type ='text' id='local_uno_lumen' name='local_uno' size='25' > </td></tr>";
        echo "<tr class='cvc_span' {$show_cvc}><td style='width:10%;' colspan='2'><input type = 'radio' class='intravenosos' value='2' " . (($cvc == '2') ? "checked" : "") . " {$enabled} id='duplo_lumen' name='cvc' >Duplo Lumen  </td><td>Local<input type = 'text' size='25' value='{$local_cvc}' {$acesso} id='local_duplo_lumen' name='local_duplo' ></td></tr>";
        echo"<tr class='cvc_span' {$show_cvc}><td style='width:10%;' colspan='2'><input type = 'radio' class='intravenosos' value='3' " . (($cvc == '3') ? "checked" : "") . " {$enabled} id='triplo_lumen' name='cvc' >Triplo Lumen </td><td>Local<input type = 'text' size='25' value='{$local_cvc}' {$acesso} id='local_triplo_lumen' name='local_cvc' ></td></tr>";
        echo"<tr class='dispositivos_span'><td colspan='2'> <input type = 'checkbox' value='1' " . (($avp) ? "checked" : "") . " {$enabled}  name='avp' class='valida_cvc'>AVP  </td><td>Local<input type = 'text' size='25' value='{$avp_local}' {$acesso} name='avp_local' ></td>";
        echo "<tr class='dispositivos_span'><td colspan='2'><input type = 'checkbox' value='1' " . (($port) ? "checked" : "") . " {$enabled}   name='port' class='valida_cvc'>Port-O-Cath </td><td>N&ordm;  <input type = 'text' size='5' maxlength='5' value='{$port_local}' {$acesso} name='quant_port' >Local<input type = 'text' size='25' value='{$port_local}' {$acesso} name='port_local' ></td></tr>";
         echo"<tr class='dispositivos_span'><td colspan='2'> <input type = 'checkbox' value='1' " . (($picc) ? "checked" : "") . " {$enabled}  name='picc' class='valida_cvc'>Cateter de PICC </td><td>Local<input type = 'text' size='25' value='{$picc_local}' {$acesso} name='picc_local' ></td>";
        echo"<tr class='dispositivos_span'><td colspan = '3'>Outros<textarea cols=66 rows='3' name='outros_intra' value='{$outros_intra}' {$acesso}></textarea></td></tr>";
        echo "</table>";
       */


        //FUNCIONAMENTO GASTROINTESTINAL Atualiza��o Eriton 04-04-2013
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Funcionamento Gastrointestinal - <select name='alt_gastro' class='alt_gastro COMBO_OBG'>
            <option value ='-1'>  </option>
            <option value ='n'>N&atilde;o</option>
            <option value ='s'>Sim</option>
            </select></th></thead>";
        //ITENS GASTROINTESTINAL
        if ($alt_gastro == 's') {
            $estilo = "style='display:table'";
        } else {
            $estilo = "style='display:none'";
        }
        echo "<tr><td colspan='4' >";
        echo "<table class='gastro_table' style='width:100%;'>";

        echo"<tr class='gastro_span' {$estilo}><td colspan='4'> <input type='radio' value='1' " . (($diarreia_obstipacao == '1') ? "checked" : "") . " {$enabled} name='diarreia_obstipacao' class='funcionamento_gastrointestinal OBG_RADIO'>" . htmlentities("diarréia") . " <input type='text' size='5' name='diarreia_episodios' value='{$diarreia_episodios}' {$acesso}> " . htmlentities("episódios") . " por dia</td></tr>";
        echo"<tr class='gastro_span' {$estilo}><td colspan='4'> <input type='radio' value='2' " . (($diarreia_obstipacao == '2') ? "checked" : "") . " {$enabled} name='diarreia_obstipacao' class='funcionamento_gastrointestinal OBG_RADIO'>" . htmlentities("obstipação") . " <input type='text' size='5' name='obstipacao_episodios' value='{$obstipacao_episodios}' {$acesso}> Dias </td></tr>";
        echo"<tr class='gastro_span' {$estilo}><td colspan='4'> <input type='checkbox' value='1' " . (($nauseas_gastro == '1') ? "checked" : "") . " {$enabled} name='nauseas_gastro' class='funcionamento_gastrointestinal OBG_RADIO'>" . htmlentities("náuseas") . " </td></tr>";
        echo"<tr class='gastro_span' {$estilo}><td colspan='4'> <input type='checkbox' value='1' " . (($vomitos_gastro == '1') ? "checked" : "") . " {$enabled} name='vomitos_gastro' class='funcionamento_gastrointestinal OBG_RADIO'>" . htmlentities("vômitos") . "<input type='text' size='5' name='vomito_episodios' value='{$vomito_episodios}' {$acesso}> Epis&oacute;dios por dia</td></tr>";
        echo"<tr class='gastro_span' {$estilo}>
                  <td colspan='4'>
                           <input type='checkbox' value='1' " . (($constipado_gastro == '1') ? "checked" : "") . " {$enabled}
                              name='constipacao_gastro' class='funcionamento_gastrointestinal OBG_RADIO'>" . htmlentities("Constipação") . "
                  </td>
            </tr>";

        echo "</table>";
        echo "</td></tr>";
        echo "</table>";

        //FUNCIONAMENTO URINARIO Atualiza��o Eriton 04-04-2013
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Funcionamento Urin&aacute;rio</th></thead>";
        echo "<tr><td width='25%'> <input type='checkbox' value='1' name='normal_urinario' class='funcionamento_urinario OBG_RADIO'>Normal</td>";
        echo "<td width='25%'> <input type='checkbox' value='1' name='anuria_urinario' class='funcionamento_urinario OBG_RADIO'>An&uacute;ria </td>";
        echo "<td width='25%'> <input type='checkbox' value='1' name='incontinencia_urinario' class='funcionamento_urinario OBG_RADIO'>Incontin&ecirc;ncia </td>";
        echo "<td width='25%'> <input type='checkbox' value='1' name='hematuria_urinario' class='funcionamento_urinario OBG_RADIO'>Hemat&uacute;ria </td></tr>";
        echo "<tr><td width='25%'> <input type='checkbox' value='1' name='piuria_urinario' class='funcionamento_urinario OBG_RADIO'>Pi&uacute;ria </td>";
        echo "<td width='25%'> <input type='checkbox' value='1' name='oliguria_urinario' class='funcionamento_urinario OBG_RADIO'>Olig&uacute;ria </td>";
        echo "<td width='25%'> <input type='checkbox' value='1' name='odor_urinario' class='funcionamento_urinario OBG_RADIO'>Odor </td>";
        echo "<td width='25%'>  </td></tr>";

        echo "</table>";


        /* DISPOSITIVOS PARA ELIMINA��ES E/OU EXCRE��ES Atualiza��o Eriton 04-04-2013 - Evolu��o */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Dispositivos para elimina&ccedil;&atilde;o e/ou excre&ccedil;&otilde;es <select name='eliminaciones' class='eliminaciones COMBO_OBG' ><option value='-1'>   </option><option value='1'> N&atilde;o </option><option value='2'>Sim</option></select></th></thead>";
        if ($eliminaciones == '2') {
            $show_eliminaciones = '';
        } else {
            $show_eliminaciones = "style='display:none' ";
        }
        echo "<tr class='eliminaciones_span' $show_eliminaciones><td style='width:33%;'> <input type = 'checkbox' value='1' " . (($fralda) ? "checked" : "") . " {$enabled} name='fralda' class='dispositivos_eliminacoes'>Fralda Unidade </td><td><input type = 'text' size='5' value='{$fralda_dia}' {$acesso} maxlength='5' name='fralda_dia'> /Dia</td><td  colspan='2'>Tamanho:<input type = 'radio'  value='1' " . (($tamanho_fralda == '1') ? "checked" : "") . " {$enabled}  name='tamanho_fralda' >P <input type = 'radio'  value='2' " . (($tamanho_fralda == '2') ? "checked" : "") . " {$enabled}   name='tamanho_fralda' >M <input type = 'radio'  value='3' " . (($tamanho_fralda == '3') ? "checked" : "") . " {$enabled}  name='tamanho_fralda' >G <input type = 'radio'  value='4' " . (($tamanho_fralda == '4') ? "checked" : "") . " {$enabled}   name='tamanho_fralda' >XG</td></tr>";
        echo "<tr class='eliminaciones_span' $show_eliminaciones><td style='width:33%;'> <input type = 'checkbox' value='1' " . (($urinario) ? "checked" : "") . " {$enabled}   name='urinario' class='dispositivos_eliminacoes'>Dispositivo urin&aacute;rio </td><td colspan='3'>N&ordm;<input type = 'text' size='5' value='{$quant_urinario}' {$acesso} maxlength = '5'   name='quant_urinario' >  </td></tr>";
        echo "<tr class='eliminaciones_span' $show_eliminaciones><td>  <input type = 'checkbox'  value='1' " . (($sonda) ? "checked" : "") . " {$enabled}   name='sonda' class='dispositivos_eliminacoes'>Sonda de Foley </td><td colspan='3'>N&ordm;<input type = 'text' size='5' value='{$quant_foley}' {$acesso} maxlength='5' name='quant_foley' >  </td></tr>";
        echo "<tr class='eliminaciones_span' $show_eliminaciones><td><input type = 'checkbox'  value='1' " . (($sonda_alivio) ? "checked" : "") . " {$enabled}   name='sonda_alivio' class='dispositivos_eliminacoes'>Sonda de Al&iacute;vio </td><td colspan='3'>N&ordm;<input type = 'text' size='5' maxlength='5' value='{$quant_alivio}' {$acesso} name='quant_alivio' > </td></tr>";
        echo "<tr class='eliminaciones_span' $show_eliminaciones><td><input type = 'checkbox'  value='1' " . (($colostomia) ? "checked" : "") . " {$enabled}   name='colostomia' class='dispositivos_eliminacoes'>Colostomia Placa/Bolsa</td><td colspan='3'>N&ordm;<input type = 'text' size='5' maxlength='5' value='{$quant_colostomia}' {$acesso} name='quant_colostomia' >  </td></tr>";
        echo "<tr class='eliminaciones_span' $show_eliminaciones><td><input type = 'checkbox'  value='1' " . (($iliostomia) ? "checked" : "") . " {$enabled}   name='iliostomia' class='dispositivos_eliminacoes'>Iliostomia Placa/Bolsa</td><td colspan='3'>N&ordm;<input type = 'text' size='5' maxlength='5' value='{$quant_iliostomia}' {$acesso} name='quant_iliostomia' >  </td></tr>";
        echo "<tr class='eliminaciones_span' $show_eliminaciones><td><input type = 'checkbox' value='1' " . (($cistostomia) ? "checked" : "") . " {$enabled}  name='cistostomia' class='dispositivos_eliminacoes'>Cistostomia</td><td colspan='3'>N&ordm;<input type = 'text' size='5' name='quant_cistosmia'></td></tr>";
        echo "</table>";

        //Fim Atualiza��o Eriton 04-04-2013 - Evolu��o

        /* DRENOS */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Drenos </th></thead>";
        echo"<tr><td colspan='4' >
		<b>Drenos: </b>
		<select name='dreno_sn' class='dreno_select COMBO_OBG'><option value ='-1'>  </option>
		<option value ='n'>N&atilde;o</option>
		<option value ='s'>Sim</option>
		</select>
		&nbsp;&nbsp;
		<span name='dreno' class='dreno_span'><b>Tipo:</b><input type='text' name='dreno' class='dreno_tipo' {$acesso} size=50/><button id='add-dreno' tipo=''  > + </button></span></td></tr>";

        /* ITENS DO DRENO */
        echo "<tr><td colspan='4' >";
        echo "<table id='dreno' style='width:100%;'>";
        if (!empty($ev->dreno)) {
            foreach ($ev->dreno as $a) {
                echo "<tr><td>{$a}</td></tr>";
            }
        }
        echo "</table>";
        echo "</td></tr>";

        echo "</table>";



        /* FERIDAS */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Feridas</th></tr></thead>";
        echo"<tr><td colspan='4' ><b>Feridas: </b>
				<select name='feridas_sn' class='COMBO_OBG' id='ferida'>
				<option value ='-1'>  </option>
				<option value ='s'>Sim  </option>
				<option value ='n'>N&atilde;o</option>
				</select>
				</td></tr>";

        echo "</table>";


        echo "<span class='quadro_feridas_sn'>";
        echo "<div id='combo-div-quadro-feridas'>";
        $this->feridas();
        echo "</div>";
        echo "<br><button id='nova_ferida' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Nova Ferida</span></button><br><br>";
//        echo "<br><a id='nova_ferida' tipo='' class='incluir' contador='1'>Nova Ferida</a><br>";
        echo "</span>";



        /* SINAIS VITAIS */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><th colspan='4' >Sinais vitais </th></tr></thead>";
        echo "<tr><td><b>PA Sistolica:</b></td><td colspan='3'><input type='texto' class='numerico1 OBG' size='6' name='pa_sistolica' value='{$ev->pa_sistolica}' {$acesso} />mmhg</td></tr>";
        echo "<tr><td><b>PA Distolica:</b></td><td colspan='3'><input type='texto' class='numerico1 OBG' size='6' name='pa_diastolica' value='{$ev->pa_diastolica}' {$acesso} />mmhg</td></tr>";
        echo "<tr><td><b>FC: </b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='fc' value='{$ev->fc}' {$acesso} />bpm</td></tr>";
        echo "<tr><td><b>FR:</b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='fr' value='{$ev->fr}' {$acesso} />inc/min</td></tr>";
        echo "<tr><td><b>SPO&sup2;</b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='spo2' value='{$ev->spo2}' {$acesso} />%</td></tr>";
        echo "<tr><td><b>Temperatura:</b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='temperatura' value='{$ev->temperatura}' {$acesso} />&deg;C</td></tr>";
        echo "</tbale>";




          /* QUEIXAS E ANOTA��ES DE ENFERMAGEM */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3' >QUEIXAS E ANOTA&ccedil;&otilde;ES DE ENFERMAGEM</th></tr></thead>";
        echo "<tr><td colspan='3' ><textarea cols=90 rows=5 name='pad'   class='OBG' value={$ev->plano_terapeutico} {$acesso} ></textarea></td></tr>";
        echo "</table>";
        echo alerta();
        echo "</form></div>";

        if ($acesso != "readonly") {
            //echo "<button id='teste' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Teste</span></button></span>";
            echo "<button id='salvar-ficha-evolucao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
            echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
        }
    }

    //////////////////Fim da nova ficha de evolu��o/////////////////////////////////////

    public function nova_editar_ficha_evolucao_enfermagem($id, $v) {

        $id = anti_injection($id, "numerico");
        $result2 = mysql_query("select PACIENTE_ID from evolucaoenfermagem where ID = '{$id}'");
        while ($row = mysql_fetch_array($result2)) {
            $id_p = $row['PACIENTE_ID'];
        }

        $result = mysql_query("SELECT p.nome FROM clientes as p WHERE p.idClientes = '{$id_p}'");


        $feve = new FichaEvolucaoEnfermagem();
        while ($row = mysql_fetch_array($result)) {
            $feve->pacienteid = $id_p;
            $feve->pacientenome = ucwords(strtolower($row["nome"]));
        }

        $this->editar_ficha_evolucao_enfermagem($id, $v, $feve);
    }

    //Fun��o para editar a ficha de Evolu��o do paciente

    public function editar_ficha_evolucao_enfermagem($id, $v, $feve) {

        $titulo = 'Nova';
        if ($v == 1) {
            $acesso = "readonly"; //readonly
            $enabled = "disabled";
            $titulo = 'Visualizar';
        }

        $sql = "select
                    evolucaoenfermagem.*,
                    e.nome AS empresa_rel,
                    e.id AS empresa_id,
                    pds.nome AS convenio_rel,
                    DATE_FORMAT(evolucaoenfermagem.DATA_EVOLUCAO,'%d/%m/%Y') as inicio
                from
                    evolucaoenfermagem
                    LEFT JOIN empresas AS e ON evolucaoenfermagem.empresa = e.id
                    LEFT JOIN planosdesaude AS pds ON evolucaoenfermagem.convenio = pds.id
                where evolucaoenfermagem.ID = '{$id}'";
        //echo $sql;
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {

            $paciente = $row['PACIENTE_ID'];
            $empresa = $row['empresa_rel'];
            $empresa_id = $row['empresa_id'];
            $convenio = $row['convenio_rel'];
            $id_aval = $row['ID'];
            $data_evolucao = $row['inicio'];

            $cond_motora = $row['COND_MOTORA'];

            $integra_lesao = $row['INTEGRA_LESAO'];
            $icterica_anicterica = $row['ICTERICA_ANICTERICA'];
            $hidratada_desidratada = $row['HIDRATADA_DESIDRATADA'];
            $palida = $row['PALIDA'];
            $sudoreica = $row['SUDOREICA'];
            $fria = $row['FRIA'];
            $pele_edema = $row['PELE_EDEMA'];
            $descamacao = $row['DESCAMACAO'];
            $mucosas = $row['MUCOSAS'];
            $alt_pescoco = $row['ALT_PESCOCO'];
            $nodulos = $row['NODULOS'];
            $ganglios_inf = $row['GANGLIOS_INF'];
            $rigidez = $row['RIGIDEZ'];
            $bocio = $row['BOCIO'];
            $ferida_pescoco = $row['FERIDA_PESCOCO'];
            $ausculta_alterada = $row['AUSCULTA_ALTERADA'];
            $creptos = $row['CREPTOS'];
            $estertores = $row['ESTERTORES'];
            $roncos = $row['RONCOS'];
            $mv_dimi = $row['MV_DIMI'];
            $creptos_tipo = $row['TIPO_CREPTOS'];
            $estertor_tipo = $row['TIPO_ESTERTORES'];
            $roncos_tipo = $row['TIPO_RONCOS'];
            $mv_tipo = $row['TIPO_MV'];
            $simetrico_assimetrico = $row['SIMETRICO_ASSIMETRICO'];
            $palpacao = $row['PALPACAO'];
            $ganglios = $row['GANGLIOS'];
            $cicatriz_torax = $row['CICATRIZ_TORAX'];
            $local_cicatriz_torax = $row['LOCAL_CICATRIZ'];
            $afundamento = $row['AFUNDAMENTO'];
            $padrao_cardiorespiratorio = $row['PADRAO_CARDIORESPIRATORIO'];
            $padrao_cardiorespiratorio1 = $row['PADRAO_CARDIORESPIRATORIO1'];
            $padrao_cardiorespiratorio2 = $row['PADRAO_CARDIORESPIRATORIO2'];
            $eupneico = $row['EUPNEICO'];
            $dispineico = $row['DISPINEICO'];
            $bradipneico = $row['BRADIPNEICO'];
            $taquipneico = $row['TAQUIPNEICO'];
            $taquicardico = $row['TAQUICARDICO'];
            $bradicardico = $row['BRADICARDICO'];
            $tipo_ventilacao = $row['TIPO_VENTILACAO'];
            $tipo_ventilacao1 = $row['TIPO_VENTILACAO1'];
            $tipo_ventilacao2 = $row['TIPO_VENTILACAO2'];
            $bipap = $row['BIPAP'];
            $numero = $row['NUM_VENTILACAO'];
            $oxigenio_sn = $row['OXIGENIO_SN'];
            $oxigenio = $row['OXIGENIO'];
            $vent_cateter_oxigenio_num = $row['VENT_CATETER_OXIGENIO_NUM'];
            $vent_cateter_oxigenio_quant = $row['VENT_CATETER_OXIGENIO_QUANT'];
            $vent_cateter_oculos_num = $row['VENT_CATETER_OCULOS_NUM'];
            $vent_respirador = $row['VENT_RESPIRADOR'];
            $vent_respirador_tipo = $row['VENT_RESPIRADOR_TIPO'];
            $vent_mascara_ventury = $row['VENT_MASCARA_VENTURY'];
            $vent_mascara_ventury_tipo = $row['VENT_MASCARA_VENTURY_NUM'];
            $vent_concentrador = $row['VENT_CONCENTRADOR'];
            $vent_concentrador_num = $row['VENT_CONCENTRADOR_NUM'];
            $alt_aspiracoes = $row['ALT_ASPIRACOES'];
            $aspiracoes_num = $row['ASPIRACOES_NUM'];
            $aspiracao_secrecao = $row['ASPIRACAO_SECRECAO'];
            $caracteristicas_sec = $row['CARACTERISTICAS_SEC'];
            $cor_sec = $row['COR_SEC'];
            $plano = $row['PLANO'];
            $flacido = $row['FLACIDO'];
            $globoso = $row['GLOBOSO'];
            $distendido = $row['DISTENDIDO'];
            $rha = $row['RHA'];
            $indo_palpacao = $row['INDO_PALPACAO'];
            $doloroso = $row['DOLOROSO'];
            $timpanico = $row['TIMPANICO'];

            $integra_genital = $row['INTEGRA_GENITAL'];
            $secrecao = $row['SECRECAO'];
            $sangramento = $row['SANGRAMENTO'];
            $prurido = $row['PRURIDO'];
            $edema = $row['EDEMA'];
            $edema_mmss = $row['EDEMA_MMSS'];
            $deformidades_mmss = $row['DEFORMIDADES_MMSS'];
            $lesoes_mmss = $row['LESOES_MMSS'];
            $edema_mmii = $row['EDEMA_MMII'];
            $varizes_mmii = $row['VARIZES_MMII'];
            $deformidades_mmii = $row['DEFORMIDADES_MMII'];
            $lesoes_mmii = $row['LESOES_MMII'];
            $aquecidas_ext = $row['AQUECIDAS_EXT'];
            $oxigenadas_ext = $row['OXIGENADAS_EXT'];
            $frias_ext = $row['FRIAS_EXT'];
            $cianoticas_ext = $row['CIANOTICAS_EXT'];
            $pulso_presente = $row['PULSO_PRESENTE'];
            $dispositivos_intravenosos = $row['DISPOSITIVOS_INTRAVENOSOS'];
            $cvc = $row['CVC'];
            $cvc_tipo = $row['CVC_TIPO'];
            $local_cvc_uno = $row['LOCAL_CVC_UNO'];
            $local_cvc_duplo = $row['LOCAL_CVC_DUPLO'];
            $local_cvc_triplo = $row['LOCAL_CVC_TRIPLO'];
            $avp = $row['AVP'];
            $avp_local = $row['AVP_LOCAL'];
            $port = $row['PORT'];
            $quant_port = $row['QUANT_PORT'];
            $port_local = $row['PORT_LOCAL'];
            $outros_intra = $row['OUTROS_INTRA'];
            $alt_gastro = $row['ALT_GASTRO'];
            $diarreia_obstipacao = $row['DIARREIA_OBSTIPACAO'];
            $nauseas_gastro = $row['NAUSEAS_GASTRO'];
            $vomitos_gastro = $row['VOMITOS_GASTRO'];
            $diarreia_episodios = $row['DIARREIA_EPISODIOS'];
            $obstipacao_episodios = $row['OBSTIPACAO_EPISODIOS'];
            $vomito_episodios = $row['VOMITO_EPISODIOS'];
            $cateterismo_int = $row['CATETERISMO_INT'];
            $normal_urinario = $row['NORMAL_URINARIO'];
            $anuria_urinario = $row['ANURIA_URINARIO'];
            $incontinencia_urinario = $row['INCONTINENCIA_URINARIO'];
            $hematuria_urinario = $row['HEMATURIA_URINARIO'];
            $piuria_urinario = $row['PIURIA_URINARIO'];
            $oliguria_urinario = $row['OLIGURIA_URINARIO'];
            $odor_urinario = $row['ODOR_URINARIO'];
            $eliminaciones = $row['ELIMINACIONES'];
            $fralda = $row['FRALDA'];
            $fralda_dia = $row['FRALDA_DIA'];
            $tamanho_fralda = $row['TAMANHO_FRALDA'];
            $urinario = $row['URINARIO'];
            $quant_urinario = $row['QUANT_URINARIO'];
            $sonda = $row['SONDA'];
            $quant_foley = $row['QUANT_FOLEY'];
            $sonda_alivio = $row['SONDA_ALIVIO'];
            $quant_alivio = $row['QUANT_ALIVIO'];
            $colostomia = $row['COLOSTOMIA'];
            $quant_colostomia = $row['QUANT_COLOSTOMIA'];
            $iliostomia = $row['DISPO_ILIOSTOMIA'];
            $quant_iliostomia = $row['DISPO_ILIOSTOMIA_NUM'];
            $cistostomia = $row['CISTOSTOMIA'];
            $quant_cistosmia = $row['QUANT_CISTOSMIA'];
            $dreno_sn = $row['DRENO_SN'];
            $dreno = $row['DRENO'];
            $feridas_sn = $row['FERIDAS_SN'];
            $pa_sistolica = $row['PA_SISTOLICA'];
            $pa_diastolica = $row['PA_DIASTOLICA'];
            $fc = $row['FC'];
            $fr = $row['FR'];
            $spo2 = $row['SPO2'];
            $temperatura = $row['TEMPERATURA'];
            $pad = $row['PAD'];
            $picc=$row['CATETER_PICC'];
            $picc_local=$row['CATETER_PICC_LOCAL'];
            $constipacao_gastro = $row['constipacao_gastro'];
        }

        echo "<center><h1>{$titulo} Evolu&ccedil;&atilde;o de Enfermagem</h1></center>";

        //echo $clinico;

        $this->cabecalho($feve, $empresa, $convenio);
        $this->plan;


        echo "<div id='div-ficha-avaliacao-enfermagem'><br/>";

        $responsePaciente = ModelPaciente::getById($paciente);
        $empresaRelatorio = empty($empresa_id) ? $responsePaciente['empresa'] : $empresa_id;

        echo "<div>
                <button caminho='imprimir_ficha_evo.php?&id_a={$id_aval}&id={$paciente}' empresa-relatorio='{$empresaRelatorio}' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  role='button' aria-disabled='false'>
                    <span class='ui-button-text'>
                        Imprimir
                    </span>
                </button>
              </div>";

        echo "<form>";
        echo "<table style='width:95%;'>";
        echo "<tr><td>Visita Realizada em: </b><input class='data OBG' type='text' {$enabled} name='inicio' value='{$data_evolucao}' maxlength='10' size='10' /></td></tr>";
        echo "</table>";
        echo "<input type='hidden' name='paciente'  value='{$paciente}' />";
        echo "<input type='hidden' name='id_aval'  value='{$id_aval}' />";
        echo "<br/>";



        /*         * ************************* EXAME FISICO ************************************ */
        echo "<br>";
        echo "<table class='mytable' style='width:95%;'>";
        echo "<tr style='background-color:#ccc;'><td colspan='4' ><b><center>Exame F&iacute;sico</center></b></td></tr>";
        echo "</table>";




        /* ESTADO GERAL */
        $this->estadoGeral($id,$v);


        /* ESTADO EMOCIONAL */
        $this->estadoEmocional($id,$v);

        /* NIVEL DE CONSCIENCIA Atualiza��o Eriton 04-04-2013 */
        $this->nivelConsciencia($id,$v);

        //COURO CABELUDO Atualiza��o Eriton 04-04-2013
        $this->couroCabeludo($id,$v);


        /* CONDI��O MOTORA */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan =4> <b>Condi&ccedil;&atilde;o Motora</b></th></thead>";
        echo "<tr><td style='width:25%;'><span ><input type='radio' value='1' " . (($cond_motora == '1') ? "checked" : "") . " {$enabled} class='cond_motora OBG_RADIO' name='cond_motora'>Deambula</span></td>
				<td style='width:25%;'><span ><input type='radio' value='2' " . (($cond_motora == '2') ? "checked" : "") . " {$enabled} class='cond_motora OBG_RADIO' name='cond_motora'>Deambula com Aux&iacute;lio</span></td>
				<td style='width:25%;'><span ><input type='radio' value='3' " . (($cond_motora == '3') ? "checked" : "") . " {$enabled} class='cond_motora OBG_RADIO' name='cond_motora'>Cadeirante</span></td>
				<td style='width:25%;'><span ><input type='radio' value='4' " . (($cond_motora == '4') ? "checked" : "") . " {$enabled} name='cond_motora' class='cond_motora OBG_RADIO'>Acamado</span></td></tr>";
        echo "</table>";

        /* ALERGIA MEDICAMENTOSA E ALERGIA ALIMENTAR */
        $this->alergias($id,$v);

        /* PELE  Atualiza��o Eriton 04-04-2013- Evolu��o */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3' >Pele</th></thead>";
        if ($icterica < '2') {
            $show_icterica = "style='display:inline;'";
        } else {
            $show_icterica = "style='display:none'";
        }
        echo"<tr><td style='width:33%;'><input type='radio' value='1' " . (($integra_lesao == '1') ? "checked" : "") . " {$enabled} name='integra_lesao' class='pele_integra_lesao OBG_RADIO'>&Iacute;ntegra</td>
				<td style='width:33%;'><input type='radio' value='1' " . (($icterica_anicterica == '1') ? "checked" : "") . " {$enabled} name='icterica_anicterica' class='pele_icterica_anicterica OBG_RADIO'>Ict&eacute;rica
				<span class='icterica_escala' $show_icterica><b>Escala:</b><input type='text' name='icterica_escala' {$acesso} value='{$icterica_escala}' class='icterica_escala' size='5'></span></td>
				<td style='width:33%;'><input type='radio' value='1' " . (($hidratada_desidratada == '1') ? "checked" : "") . " {$enabled} name='hidratada_desidratada' class='pele_hidratada_desidratada OBG_RADIO'>Hidratada</td>
				</tr>";
        echo"<tr>
				<td style='width:33%;'><input type='radio' value='2' " . (($integra_lesao == '2') ? "checked" : "") . " {$enabled} name='integra_lesao' class='pele_integra_lesao OBG_RADIO'>Lesionada</td>
				<td style='width:33%;'><input type='radio' value='2' " . (($icterica_anicterica == '2') ? "checked" : "") . " {$enabled} name='icterica_anicterica' class='pele_icterica_anicterica OBG_RADIO'>Anict&eacute;rica</td>
				<td style='width:33%;'><input type='radio' value='2' " . (($hidratada_desidratada == '2') ? "checked" : "") . " {$enabled} name='hidratada_desidratada' class='pele_hidratada_desidratada OBG_RADIO'>Desidratada</td>
				</tr>";

        echo"<tr><td style='width:33%;'><b>P&aacute;lida: </b></td><td colspan='2'><input type='radio' value='1' " . (($palida == '1') ? "checked" : "") . " {$enabled} name='palida' class='palida OBG_RADIO'>Sim <input type='radio' value='2' " . (($palida == '2') ? "checked" : "") . " {$enabled} name='palida' class='palida OBG_RADIO'>N&atilde;o</tr></td>";
        //echo"<tr><td ><b>Les&otilde;es: </b></td><td><span class='valida_pele'><input type='radio' value='1' name='lesoes'>Sim <input type='radio' value='2' name='lesoes'>N&atilde;o</tr></td>";
        echo"<tr><td> <b>Sudoreica: </b></td><td colspan='2'><input type='radio' value='1' " . (($sudoreica == '1') ? "checked" : "") . " {$enabled} name='sudoreica' class='sudoreica OBG_RADIO'>Sim <input type='radio' value='2' " . (($sudoreica == '2') ? "checked" : "") . " {$enabled} name='sudoreica' class='sudoreica OBG_RADIO'>N&atilde;o</tr></td>";
        echo"<tr><td ><b>Fria: </b></td><td colspan='2'><input type='radio' value='1' " . (($fria == '1') ? "checked" : "") . " {$enabled} name='fria' class='fria OBG_RADIO'>Sim <input type='radio' value='2' " . (($fria == '2') ? "checked" : "") . " {$enabled} name='fria' class='fria OBG_RADIO'>N&atilde;o</tr></td>";
        echo"<tr><td ><b>Edema: </b></td><td colspan='2'><input type='radio' value='1' " . (($pele_edema == '1') ? "checked" : "") . " {$enabled} name='pele_edema' class='pele_edema OBG_RADIO'>Sim <input type='radio' value='2' " . (($pele_edema == '2') ? "checked" : "") . " {$enabled} name='pele_edema' class='pele_edema OBG_RADIO'>N&atilde;o</tr></td>";
        echo"<tr><td ><b>Descama&ccedil;&atilde;o: </b></td><td colspan='2'><input type='radio' value='1' " . (($descamacao == '1') ? "checked" : "") . " {$enabled} name='descamacao' class='descamacao OBG_RADIO'>Sim <input type='radio' value='2' " . (($descamacao == '2') ? "checked" : "") . " {$enabled} name='descamacao' class='descamacao OBG_RADIO'>N&atilde;o</tr></td>";
        echo "</table>";

        // MUCOSAS Atualiza��o Eriton 04-04-2013


        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan =4> <b>Mucosas</b></th></thead>";
        echo "<tr><td style='width:30%;'>  <input type = 'radio' value='1' " . (($mucosas == '1') ? "checked" : "") . " {$enabled} name= 'mucosas' class='mucosas OBG_RADIO'>Coradas </td><td> <input type = 'radio' value='2' " . (($mucosas == '2') ? "checked" : "") . " {$enabled} name= 'mucosas' class='mucosas OBG_RADIO'>  Descoradas  </td><td colspan='2'><input type = 'radio' value='3' " . (($mucosas == '3') ? "checked" : "") . " {$enabled} name= 'mucosas' class='mucosas OBG_RADIO'>Hipocoradas </tr></td>";

        echo "</table>";

        // PESCO�O Atualiza��o Eriton 04-04-2013
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan =4> <b>Pesco&ccedil;o</b></th></thead>";
        echo"<tr><td colspan='4'><b>Altera&ccedil;&otilde;es</b>
				<select name='alt_pescoco' id='alt_pescoco' {$enabled} class='alt_pescoco COMBO_OBG'><option value ='-1'>  </option>
				<option value ='1' " . (($alt_pescoco == '1') ? "selected" : "") . " >N&atilde;o</option>
					<option value ='2' " . (($alt_pescoco == '2') ? "selected" : "") . " >Sim</option>
				</select></tr></td>";

        if ($alt_pescoco == 2) {
            $estilo = '';
        } else {
            $estilo = " style='display:none;' ";
        }

        //ITENS DO PESCO�O
        echo "<tr><td colspan='4'>";
        echo "<table id='pescoco_span' style='width:100%;'>";
        echo "<tr {$estilo} class='pescoco_span'><td style='width:25%;'> N&oacute;dulos </td><td colspan=3><input type = 'radio' value='1' " . (($nodulos == '1') ? "checked" : "") . " {$enabled} name= 'nodulos' class='nodulos OBG_RADIO'>Sim <input type = 'radio' value='2' " . (($nodulos == '2') ? "checked" : "") . " {$enabled} name= 'nodulos' class='nodulos OBG_RADIO'> N&atilde;o </tr></td>";
        echo "<tr {$estilo} class='pescoco_span'><td > G&acirc;nglios Infartados </td><td colspan=3><input type = 'radio' value='1' " . (($ganglios_inf == '1') ? "checked" : "") . " {$enabled} name= 'ganglios_inf' class='ganglios_inf OBG_RADIO'>Sim <input type = 'radio' value='2' " . (($ganglios_inf == '2') ? "checked" : "") . " {$enabled} name= 'ganglios_inf' class='ganglios_inf OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "<tr {$estilo} class='pescoco_span'><td > Rigidez </td><td colspan=3><input type = 'radio' value='1' " . (($rigidez == '1') ? "checked" : "") . " {$enabled} name= 'rigidez' class='rigidez OBG_RADIO'>Sim <input type = 'radio' value='2' " . (($rigidez == '2') ? "checked" : "") . " {$enabled} name= 'rigidez' > N&atilde;o </tr></td>";
        echo "<tr {$estilo} class='pescoco_span'><td > B&oacute;cio </td><td colspan=3><input type = 'radio' value='1' " . (($bocio == '1') ? "checked" : "") . " {$enabled} name= 'bocio' class='bocio OBG_RADIO'>Sim <input type = 'radio' value='2' " . (($bocio == '2') ? "checked" : "") . " {$enabled} name= 'bocio' class='bocio OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "<tr {$estilo} class='pescoco_span'><td > Ferida </td><td colspan=3><input type = 'radio' value='1' " . (($ferida_pescoco == '1') ? "checked" : "") . " {$enabled} name= 'ferida_pescoco' class='ferida_pescoco OBG_RADIO'>Sim <input type = 'radio' value='2' " . (($ferida_pescoco == '2') ? "checked" : "") . " {$enabled} name= 'ferida_pescoco' class='ferida_pescoco OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "</table>";
        echo "</td></tr>";
        echo "</table>";


        // T�RAX Atualiza��o Eriton 04-04-2013
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan = '4'> <b>T&oacute;rax</b></th></thead>";
        echo "<tr><td colspan='4'><b>AVALIA&Ccedil;&Atilde;O ANAT&Ocirc;MICA</b></td></tr>";
        echo"<tr><td style='width:25%;'><input type = 'radio' value='1' " . (($simetrico_assimetrico == '1') ? "checked" : "") . " {$enabled} name= 'simetrico_assimetrico'  class='simetrico_assimetrico OBG_RADIO'>Sim&eacute;trico </td>
			<td colspan='3'><input type = 'radio' value='2' " . (($simetrico_assimetrico == '2') ? "checked" : "") . " {$enabled} name= 'simetrico_assimetrico' class='simetrico_assimetrico OBG_RADIO'> Assim&eacute;trico </td></tr>";
        //echo"<tr><td colspan =3> <input type='checkbox' value='1' name='assimetrico'>Assim&eacute;trico </td></tr>";
        echo "<tr><td ><b> Doloroso a Palpa&ccedil;&atilde;o</b> </td><td colspan='3'><input type='radio' value='1' " . (($palpacao == '1') ? "checked" : "") . " {$enabled} name='palpacao' class='palpacao OBG_RADIO'>Sim <input type='radio' value='2' " . (($palpacao == '2') ? "checked" : "") . " {$enabled} name='palpacao' class='palpacao OBG_RADIO'>N&atilde;o </td></tr>";
        echo "<tr><td ><b> G&acirc;nglios</b> </td><td colspan='3'><input type='radio' value='1' " . (($ganglios == '1') ? "checked" : "") . " {$enabled} name='ganglios' class='ganglios OBG_RADIO'>Sim <input type='radio' value='2' " . (($ganglios == '2') ? "checked" : "") . " {$enabled} name='ganglios'  class='ganglios OBG_RADIO'>N&atilde;o </td></tr>";
        echo "<tr><td ><b> Cicatriz </b> </td><td colspan='3'> <input type='radio' value='1' " . (($cicatriz_torax == '1') ? "checked" : "") . " {$enabled} name='cicatriz_torax' class='cicatriz_torax OBG_RADIO'>Sim <input type='radio' value='2' " . (($cicatriz_torax == '2') ? "checked" : "") . " {$enabled} name='cicatriz_torax' class='cicatriz_torax OBG_RADIO'>N&atilde;o   Local:<input type = 'text' name='local_cicatriz_torax' class='local_cicatriz_torax' size='20' value='{$local_cicatriz_torax}' {$acesso}></td></tr>";
        echo "<tr><td ><b> Afundamento </b> </td><td colspan='3'> <input type='radio' value='1' " . (($afundamento == '1') ? "checked" : "") . " {$enabled} name='afundamento' class='afundamento OBG_RADIO'>Sim <input type='radio' value='2' " . (($afundamento == '2') ? "checked" : "") . " {$enabled} name='afundamento' class='afundamento OBG_RADIO'>N&atilde;o </td></tr>";
        echo "<tr><td ><b> Ausculta alterada</b> </td><td colspan='3'> <input type='radio' value='1' " . (($ausculta_alterada == '1') ? "checked" : "") . " {$enabled} name='ausculta_alterada' class='torax_sel OBG_RADIO'>Sim <input type='radio' value='2' " . (($ausculta_alterada == '2') ? "checked" : "") . " {$enabled} name='ausculta_alterada' class='torax_sel OBG_RADIO'>N&atilde;o </td></tr>";
        //echo "<tr><td colspan='4'><b>AUSCULTA</b></td></tr>";
        echo "<tr class='torax_ausculta'><td colspan='4'> MVDB<input type='radio' name='mv' value='1' " . (($ausculta_alterada != '1') ? "checked" : "") . " {$enabled} class='torax_ausculta'></td></tr>";
        echo "<tr class='torax' " . (($ausculta_alterada == '1') ? "style='display: table-row;'" : "style='display: none;'") . "><td> <input type='checkbox' value='1' " . (($creptos == '1') ? "checked" : "") . " {$enabled} name='creptos' id='creptos' class='torax '>Creptos </td>";
        echo " <td colspan='3'> <input type='checkbox' value='1' " . (($estertores == '1') ? "checked" : "") . " {$enabled} name='estertores' id='estertores' class='torax '>Estertores</td></tr>";

        echo "<tr><td width='50%'><span class='creptos_span' " . (($creptos == '1') ? "style='display: table-row;'" : "style='display: none;'") . "><input type='radio' name='creptos_tipo' value='1' " . (($creptos_tipo == '1') ? "checked" : "") . ">Base Esquerda</input><input type='radio' name='creptos_tipo' value='2'  " . (($creptos_tipo == '2') ? "checked" : "") . " >Base Direita</input><input type='radio' name='creptos_tipo' value='3' " . (($creptos_tipo == '3') ? "checked" : "") . " >Ambas bases</input><input type='radio' name='creptos_tipo' value='4' " . (($creptos_tipo == '4') ? "checked" : "") . " >&Aacute;pice Esquerda</input><input type='radio' name='creptos_tipo' value='5' " . (($creptos_tipo == '5') ? "checked" : "") . " >&Aacute;pice Direita</input><br><input type='radio' name='creptos_tipo' value='6' " . (($creptos_tipo == '6') ? "checked" : "") . " >Parede Anterior</input></span></td>";
        echo "<td colspan='3'><span class='estertores_span' " . (($estertores == '1') ? "style='display: table-row;'" : "style='display: none;'") . "><input type='radio' name='estertor_tipo' value='1' " . (($estertor_tipo == '1') ? "checked" : "") . ">Base Esquerda</input><input type='radio' name='estertor_tipo' value='2' " . (($estertor_tipo == '2') ? "checked" : "") . ">Base Direita</input><input type='radio' name='estertor_tipo' value='3' " . (($estertor_tipo == '3') ? "checked" : "") . ">Ambas bases</input><input type='radio' name='estertor_tipo' value='4' " . (($estertor_tipo == '4') ? "checked" : "") . ">&Aacute;pice Esquerda</input><input type='radio' name='estertor_tipo' value='5' " . (($estertor_tipo == '5') ? "checked" : "") . ">&Aacute;pice Direita</input><br><input type='radio' name='estertor_tipo' value='6' " . (($estertor_tipo == '6') ? "checked" : "") . ">Parede Anterior</input></span></td></tr>";
        echo "<tr class='torax' " . (($ausculta_alterada == '1') ? "style='display: table-row;'" : "style='display: none;'") . "><td> <input type='checkbox' value='1' " . (($roncos == '1') ? "checked" : "") . " {$enabled} name='roncos' id='roncos' class='torax'>Roncos </td>";
        echo "<td colspan='3'> <input type='checkbox' value='1' " . (($mv_dimi == '1') ? "checked" : "") . " {$enabled} name='mv_dimi' id='mv_dimi' class='torax'>MV Diminu&iacute;dos</td></tr>";
        echo "<tr><td><span class='roncos_span' " . (($roncos == '1') ? "style='display: table-row;'" : "style='display: none;'") . "><input type='radio' name='roncos_tipo' value='1' " . (($roncos_tipo == '1') ? "checked" : "") . ">Base Esquerda</input><input type='radio' name='roncos_tipo' value='2'  " . (($roncos_tipo == '2') ? "checked" : "") . ">Base Direita</input><input type='radio' name='roncos_tipo' value='3'  " . (($roncos_tipo == '3') ? "checked" : "") . ">Ambas bases</input><input type='radio' name='roncos_tipo' value='4'  " . (($roncos_tipo == '4') ? "checked" : "") . ">&Aacute;pice Esquerda</input><input type='radio' name='roncos_tipo' value='5'  " . (($roncos_tipo == '5') ? "checked" : "") . ">&Aacute;pice Direita</input><br><input type='radio' name='roncos_tipo' value='6'  " . (($roncos_tipo == '6') ? "checked" : "") . ">Parede Anterior</input></span></td>";
        echo"<td colspan='3'><span class='mv_dimi_span' " . (($mv_dimi == '1') ? "style='display: table-row;'" : "style='display: none;'") . "><input type='radio' name='mv_tipo' value='1' " . (($mv_tipo == '1') ? "checked" : "") . ">Base Esquerda</input><input type='radio' name='mv_tipo' value='2' " . (($mv_tipo == '2') ? "checked" : "") . ">Base Direita</input><input type='radio' name='mv_tipo' value='3' " . (($mv_tipo == '3') ? "checked" : "") . ">Ambas bases</input><input type='radio' name='mv_tipo' value='4' " . (($mv_tipo == '4') ? "checked" : "") . ">&Aacute;pice Esquerda</input><input type='radio' name='mv_tipo' value='5' " . (($mv_tipo == '5') ? "checked" : "") . ">&Aacute;pice Direita</input><br><input type='radio' name='mv_tipo' value='6' " . (($mv_tipo == '6') ? "checked" : "") . ">Parede Anterior</input></span></td></tr>";

        echo "</table>";

        //PADR�O CARDIORESPIRAT�RIO Atualiza��o Eriton 04-04-2013
        if ($eupneico == '1') {
            $estilo = '';
        } else {
            $estilo = " style='display:none;' ";
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >" . htmlentities("Padrão Cardiorepiratório") . " </th></thead>";
        echo"<tr><td style='width:33%;'><input type='radio' value='1' " . (($padrao_cardiorespiratorio2 == '1') ? "checked" : "") . " {$enabled} name='padrao_cardiorespiratorio2' class='padrao_cardiorespiratorio_sel OBG_RADIO'>Eupneico </td>
			<td colspan='3'><input type='radio' value='1' " . (($padrao_cardiorespiratorio == '1') ? "checked" : "") . " {$enabled} name='padrao_cardiorespiratorio' class='padrao_cardiorespiratorio_ '>Bulhas " . htmlentities("Cardíacas Normofonéticas") . " (BCNF) </td></tr>
		<tr class='padrao_cardiorespiratorio'><td><input type='radio' value='2' " . (($padrao_cardiorespiratorio2 == '2') ? "checked" : "") . " {$enabled} name='padrao_cardiorespiratorio2' class='padrao_cardiorespiratorio'>" . htmlentities("taquipnéico") . " </td><td colspan='3'><input type='radio' value='1' " . (($padrao_cardiorespiratorio1 == '1') ? "checked" : "") . " {$enabled} name='padrao_cardiorespiratorio1' class='padrao_cardiorespiratorio '>Taquicardico </td></tr>";
        echo "<tr class='padrao_cardiorespiratorio' ><td> <input type='radio' value='3' " . (($padrao_cardiorespiratorio2 == '3') ? "checked" : "") . " {$enabled} name='padrao_cardiorespiratorio2' class='padrao_cardiorespiratorio'>" . htmlentities("Dispnéico") . "</td><td colspan='3'><input type='radio' value='2' " . (($padrao_cardiorespiratorio1 == '2') ? "checked" : "") . " {$enabled} name='padrao_cardiorespiratorio1' class='padrao_cardiorespiratorio '>bradicardico </td></tr>";
        echo "<tr class='padrao_cardiorespiratorio' ><td> <input type='radio' value='4' " . (($padrao_cardiorespiratorio2 == '4') ? "checked" : "") . " {$enabled} name='padrao_cardiorespiratorio2' class='padrao_cardiorespiratorio'>" . htmlentities("Dispnéico") . "</td><td colspan='3'><input type='radio' value='3' " . (($padrao_cardiorespiratorio1 == '3') ? "checked" : "") . " {$enabled} name='padrao_cardiorespiratorio1' class='padrao_cardiorespiratorio '>Normocardico </td></tr>";
        echo "</table>";


        /* TIPO DE VENTILA��O Atualiza��o Eriton 11-04-2013 Evolu��o */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='2' >Tipo de Ventila&ccedil;&atilde;o</th></thead>";
        echo "<tr><td colspan='2'><input type='radio' value='1' " . (($tipo_ventilacao == '1') ? "checked" : "") . " {$enabled} name='tipo_ventilacao' class='tipo_ventilacion OBG_RADIO'>Espont&acirc;nea<input type='radio' value='2' " . (($tipo_ventilacao == '2') ? "checked" : "") . " {$enabled} name='tipo_ventilacao' class='tipo_ventilacion OBG_RADIO'>Traqueostomia</td></tr>";
        if ($ventilacao >= '2') {
            $show_ventilacion = "style='display:table'";
        } else {
            $show_ventilacion = "style='display:none'";
        }
        echo "<tr><td><span class='ventilacion' $show_ventilacion>Tipo de Dispositivo: <select name = 'tipo_ventilacao1' id='tipo_ventilacao1' {$enabled}>
	<option value='-1'>  </option>
	<option value='2' " . (($tipo_ventilacao1 == '2') ? "selected" : "") . ">Metalica</option>
	<option value='3' " . (($tipo_ventilacao1 == '3') ? "selected" : "") . ">Plastica</option></select></span>";
        echo"<td width='70%'><span class='ventilacion' $show_ventilacion><b>N&ordm; </b>   <input type='text' maxlength='5' size='5' name='num_ventilacao' id='num_ventilacao'>$numero </td></span></tr>";
        echo "<tr><td><span class='ventilacion' $show_ventilacion>Modo de Ventila&ccedil;&atilde;o: <select name = 'tipo_ventilacao2' {$enabled}>
	<option value='-1'>  </option>
	<option value='1' " . (($tipo_ventilacao2 == '1') ? "selected" : "") . " >Espontanea</option>
	<option value='2' " . (($tipo_ventilacao2 == '2') ? "selected" : "") . " >Equipamento</option></select></span></td>";
        echo "<td><span class='bipap'><b>BIPAP</b> <input type='radio' value='1' " . (($bipap == '1') ? "checked" : "") . " {$enabled} name='bipap' class='vm_bipap'>Cont&iacute;nuo <input type='radio' value='2' " . (($bipap == '2') ? "checked" : "") . " name='bipap' class='vm_bipap'>Intermitente</span></td></tr>";
        echo "</table>";




        /* OXIGENIO Atualiza��o Eriton 11-04-2013 Evolu��o */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead>"
             . "<tr>"
                . "<th colspan='4' >"
                . "Oxig&ecirc;nio"
                . "<select name='oxigenio_sn' class='oxigenio_sn COMBO_OBG'{$enabled}><option value ='-1'>  </option>
				<option value ='n' " . (($oxigenio_sn == 'n') ? "selected" : "") . ">N&atilde;o</option>
				<option value ='s' " . (($oxigenio_sn == 's') ? "selected" : "") . ">Sim</option>
			</select>"
                . "</th>"
                . "</tr>"
                . "</thead>";


        //ITENS DO OXIGENIO Atualiza��o Eriton 11-04-2013 Evolu��o
        if ($oxigenio_sn == 's') {
            $estilo = "style='display:table;'";
        } else {
            $estilo = " style='display:none;' ";
        }
        echo "<tr>"
           . " <td colspan='4' >";
                echo "<table id='oxigenio_span'>";
                        echo"<tr ><td colspan='3'><input type='radio'  class='item_oxigenio'value='1' " . (($oxigenio == '1') ? "checked" : "") . " {$enabled}  name='oxigenio'>Cateter de Oxig&ecirc;nio</td><td> N&ordm; <input type = 'text'  name='vent_cateter_oxigenio_num' size = '5' maxlength='5' value='{$vent_cateter_oxigenio_num}' {$acesso}> <input type = 'text'  name='vent_cateter_oxigenio_quant' size = '5' maxlength='5' value='{$vent_cateter_oxigenio_quant}' {$acesso}>L/MIN </td></tr>";
                        echo "<tr ><td colspan='3'><input type='radio' class='item_oxigenio' value='2' " . (($oxigenio == '2') ? "checked" : "") . " {$enabled} name='oxigenio'>Cateter tipo &Oacute;culos </td><td><input type='text' maxlength='5' size='5' name='vent_cateter_oculos_num' value='{$vent_cateter_oculos_num}' {$acesso}>L/MIN</td></tr>";
                        echo "<tr ><td colspan='3'><input type='radio' class='item_oxigenio' value='3' " . (($oxigenio == '3') ? "checked" : "") . " {$enabled} name='oxigenio'>M&aacute;scara Reservat&oacute;rio</td><td><input type='text'  maxlength='5' size='5' name='vent_respirador_tipo' value='{$vent_respirador_tipo}' {$acesso}>L/MIN</tr></td>";
                        echo"<tr ><td colspan='3'><input type='radio'  class='item_oxigenio' value='4' " . (($oxigenio == '4') ? "checked" : "") . " {$enabled} name='oxigenio'>Mascara de Ventury </td><td><input type='text' maxlength='5' size='5' name='vent_mascara_ventury' value='{$vent_mascara_ventury}' {$acesso}>% </td></tr>";
                        echo "<tr ><td colspan='3'><input type='radio' class='item_oxigenio' value='5' " . (($oxigenio == '5') ? "checked" : "") . " {$enabled} name='oxigenio'>Concentrador </td><td><input type='text'  maxlength='5' size='5' name='vent_concentrador_num' value='{$vent_concentrador_num}' {$acesso}>L/MIN</tr></td>";
                echo "</table>";
          echo "</td>"
          . "</tr>";


        echo "</table>";


        /* ASPIRA��ES Atualiza��o Eriton 11-04-2013 Evolu��o */

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead>"
             . "<tr>"
                . " <th colspan='4' >"
                . "Aspira&ccedil;&otilde;es
                <select name='alt_aspiracoes' class='alt_aspiracoes COMBO_OBG' {$enabled}><option value ='-1'>  </option>
				<option value ='n' " . (($alt_aspiracoes == 'n') ? "selected" : "") . ">N&atilde;o</option>
				<option value ='s' " . (($alt_aspiracoes == 's') ? "selected" : "") . ">Sim</option>
				</select>"
                . "</th>"
                . "</tr>"
             . "</thead>";



        if ($alt_aspiracoes >= 's') {
            $estilo = "style='display:table'";
        } else {
            $estilo = "style='display:none'";
        }

        echo"<tr {$estilo} class ='aspiracoes_span'>"
            . "<td colspan='6'>"
                . "N&uacute;mero de Aspira&ccedil;&otilde;es/Dia:"
                . "<input type='text' maxlength='5' size='5' name='aspiracoes_num' class='aspiracoes_num' value='{$aspiracoes_num}' {$acesso} />"
                . "</td>"
            . "</tr>";
        echo"<tr {$estilo} class ='aspiracoes_span'>
                   <td colspan='6'>
                        <label>
                          <b>Secre&ccedil;&atilde;o</b>
                        </label>
                        <select name= 'aspiracao_secrecao' id='aspiracao_secrecao' class='COMBO_OBG' {$enabled}>
                            <option value ='-1'>  </option>
                            <option value ='a' " . (($aspiracao_secrecao == 'a') ? "selected" : "") . ">Ausente</option>
                            <option value ='p' " . (($aspiracao_secrecao == 'p') ? "selected" : "") . ">Presente</option>
                        </select>
                    </td>
             </tr>";

        if ($aspiracao_secrecao == 'p') {
            $estilo = "style='display:table;' ";
        } else {
            $estilo = "style='display:none;' ";
        }

        //CARACTERISTICAS DA SECRE��O Atualiza��o Eriton 11-04-2013 Evolu��o



        echo"<tr class='secrecao_span' $estilo>"
             . "<td colspan = '3' width='30%'>"
                . "<b>Caracter&iacute;sticas da secre&ccedil;&atilde;o</b>"
            . "</td>"
         . "</tr>";
        echo "<tr class='secrecao_span' $estilo>"
                   . "<td colspan = '3'>"
                      . "<input type='radio' value='1' " . (($caracteristicas_sec == '1') ? "checked" : "") . " {$enabled} name='caracteristicas_sec' class='caracteristicas_sec'>"
                      . "Fluida"
                  . " </td>";
                 echo "<td colspan = '3'>"
                      . "<input type='radio' value='1' " . (($cor_sec == '1') ? "checked" : "") . " {$enabled} name='cor_sec' class='cor_sec'>"
                         . "Clara  "
                  . "</td>"
          . "</tr>";
        echo "<tr class='secrecao_span' $estilo>"
                 . "<td colspan = '3'>"
                     . "<input type='radio' value='2' " . (($caracteristicas_sec == '2') ? "checked" : "") . " {$enabled} name='caracteristicas_sec' class='caracteristicas_sec'>"
                     . "Espessa "
                . "</td>";
             echo "<td colspan = '3'>"
                     . "<input type='radio' value='2' " . (($cor_sec == '2') ? "checked" : "") . " {$enabled} name='cor_sec' class='cor_sec'>"
                     . "Amarelada "
                . "</td>"
         . "</tr>";
        echo "<tr class='secrecao_span' $estilo>"
                     . "<td colspan = '3'> </td>";
                  echo "<td colspan = '3'>"
                      . " <input type='radio' value='3' " . (($cor_sec == '3') ? "checked" : "") . " {$enabled} name='cor_sec' class='cor_sec'>"
                      . " Esverdeada"
                      . "</td>"
            . "</tr>";
        echo "<tr class='secrecao_span' $estilo>"
                      . "<td colspan = '3'> </td>";
                 echo "<td colspan = '3'>"
                       . "<input type='radio' value='4' " . (($cor_sec == '4') ? "checked" : "") . " {$enabled} name='cor_sec' class='cor_sec'> "
                       . "Sanguinolenta"
                   . "</td>"
            . "</tr>";



        echo "</table>";
        //Fim Atualiza��o Eriton 28-03-2013
        ////AVALIACAO ABDOMINAL////// Atualiza��o Eriton 11-04-2013 Evolu��o
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Avalia&ccedil;&atilde;o Abdominal</th></thead>";

        echo "<tr> <td style='width:25%;'> <input type='checkbox' value='1' " . (($plano) ? "checked" : "") . " {$enabled} name='plano' class='avaliacao_abdominal OBG_RADIO'>Plano </td>
				<td style='width:25%;'> <input type='checkbox' value='1' " . (($flacido) ? "checked" : "") . " {$enabled} name='flacido' class='avaliacao_abdominal OBG_RADIO'>Flacido </td>
				<td style='width:25%;'> <input type='checkbox' value='1' " . (($globoso) ? "checked" : "") . " {$enabled} name='globoso' class='avaliacao_abdominal OBG_RADIO'>Globoso </td>
				<td style='width:25%;'> <input type='checkbox' value='1' " . (($distendido) ? "checked" : "") . " {$enabled} name='distendido' class='avaliacao_abdominal OBG_RADIO'>Distendido </td>
				</tr>";

        echo "<tr> <td style='width:25%;'>RHA <select name='rha' id= 'rha' class='COMBO_OBG' {$enabled}><option value='-1'>  </option>
				<option value='p' " . (($rha == 'p') ? "selected" : "") . ">Presente</option>
				<option value='a'" . (($rha == 'a') ? "selected" : "") . " >Ausente</option></select> </td>
				<td style='width:25%;'> <input type='checkbox' value='1' " . (($indo_palpacao) ? "checked" : "") . " {$enabled} name='indo_palpacao' class='avaliacao_abdominal OBG_RADIO'>Indolor a Palpa&ccedil&atilde;o </td>
				<td style='width:25%;'> <input type='checkbox' value='1' " . (($doloroso) ? "checked" : "") . " {$enabled} name='doloroso' class='avaliacao_abdominal OBG_RADIO'>Doloroso </td>
				<td style='width:25%;'> <input type='checkbox' value='1' " . (($timpanico) ? "checked" : "") . " {$enabled} name='timpanico' class='avaliacao_abdominal OBG_RADIO'>Timp&acirc;nico </td>
					</tr>";

        echo "</table>";

        /* VIA ALIMENTA��O Atualiza��o Eriton 11-04-2013 Evolu��o */

        $this->dietas($id,$v,2);
        $this->itensDietas($id,$v,2);

//avaliacao genital Atualiza��o Eriton 11-04-2013 Evolu��o

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Avalia&ccedil;&atilde;o Genital</th></thead>";

        echo "<tr><td style='width:25%;'> &Iacute;ntegra </td> <td style='width:75%;' colspan='3'><input type = 'radio' value='1'" . (($integra_genital == '1') ? "checked" : "") . " {$enabled} name= 'integra_genital' >Sim <input type = 'radio' value='2' " . (($integra_genital == '2') ? "checked" : "") . " {$enabled} name= 'integra_genital'> N&atilde;o </tr></td>";
        echo "<tr><td style='width:25%;'> Secre&ccedil;&atilde;o </td> <td style='width:75%;' colspan='3'><input type = 'radio' value='1' " . (($secrecao == '1') ? "checked" : "") . " {$enabled} name= 'secrecao' >Sim <input type = 'radio' value='2' " . (($secrecao == '2') ? "checked" : "") . " {$enabled} name= 'secrecao' >N&atilde;o  </tr></td>";
        echo "<tr><td style='width:25%;'> Sangramento </td> <td style='width:75%;' colspan='3'><input type = 'radio' value='1' " . (($sangramento == '1') ? "checked" : "") . " {$enabled} name= 'sangramento' >Sim <input type = 'radio' value='2' " . (($sangramento == '2') ? "checked" : "") . " {$enabled} name= 'sangramento' > N&atilde;o </tr></td>";
        echo "<tr><td style='width:25%;'> Prurido </td> <td style='width:75%;' colspan='3'><input type = 'radio' value='1' " . (($prurido == '1') ? "checked" : "") . " {$enabled} name= 'prurido' >Sim <input type = 'radio' value='2' " . (($prurido == '2') ? "checked" : "") . " {$enabled} name= 'prurido' >N&atilde;o  </tr></td>";
        echo "<tr><td style='width:25%;'> Edema </td> <td style='width:75%;' colspan='3'><input type = 'radio' value='1' " . (($edema == '1') ? "checked" : "") . " {$enabled} name= 'edema' >Sim <input type = 'radio' value='2'" . (($edema == '2') ? "checked" : "") . " {$enabled} name= 'edema' >N&atilde;o  </tr></td>";
        echo "</table>";

        // AVALIA��O DE MEMBROS SUPERIORES Atualiza��o Eriton 04-04-2013
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='2' >Avalia&ccedil;&atilde;o de membros superiores</th></thead>";
        echo "<tr><td style='width:50%;'> Edema </td> <td style='width:50%;'><input type = 'radio' value='1' " . (($edema_mmss == '1') ? "checked" : "") . " {$enabled} name= 'edema_mmss' class='edema_mmss OBG_RADIO'>Sim <input type = 'radio' value='2' " . (($edema_mmss == '2') ? "checked" : "") . " {$enabled} name= 'edema_mmss' class='edema_mmss OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "<tr><td style='width:50%;'> Deformidades </td> <td style='width:50%;'><input type = 'radio' value='1' " . (($deformidades_mmss == '1') ? "checked" : "") . " {$enabled} name= 'deformidades_mmss' class='deformidades_mmss OBG_RADIO'>Sim <input type = 'radio' value='2' " . (($deformidades_mmss == '2') ? "checked" : "") . " {$enabled} name= 'deformidades_mmss' class='deformidades_mmss OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "<tr><td style='width:50%;'> Les&otilde;es </td> <td style='width:50%;'><input type = 'radio' value='1' " . (($lesoes_mmss == '1') ? "checked" : "") . " {$enabled} name= 'lesoes_mmss' class='lesoes_mmss OBG_RADIO'>Sim <input type = 'radio' value='2' " . (($lesoes_mmss == '2') ? "checked" : "") . " {$enabled} name= 'lesoes_mmss' class='lesoes_mmss OBG_RADIO'>N&atilde;o </tr></td>";
        echo "</table>";

        // AVALIA��O DE MEMBROS INFERIORES Atualiza��o Eriton 04-04-2013
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='2' >Avalia&ccedil;&atilde;o de membros inferiores</th></thead>";
        echo "<tr><td style='width:50%;'> Edema </td> <td style='width:50%;'><input type = 'radio' value='1' " . (($edema_mmii == '1') ? "checked" : "") . " {$enabled} name= 'edema_mmii' class='edema_mmii OBG_RADIO'>Sim <input type = 'radio' value='2' " . (($edema_mmii == '2') ? "checked" : "") . " {$enabled} name= 'edema_mmii' class='edema_mmii OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "<tr><td style='width:50%;'> Varizes </td> <td style='width:50%;'><input type = 'radio' value='1' " . (($varizes_mmii == '1') ? "checked" : "") . " {$enabled} name= 'varizes_mmii' class='varizes_mmii OBG_RADIO'>Sim <input type = 'radio' value='2' " . (($varizes_mmii == '2') ? "checked" : "") . " {$enabled} name= 'varizes_mmii' class='varizes_mmii OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "<tr><td style='width:50%;'> Deformidades </td> <td style='width:50%;'><input type = 'radio' value='1' " . (($deformidades_mmii == '1') ? "checked" : "") . " {$enabled} name= 'deformidades_mmii' class='deformidades_mmii OBG_RADIO'>Sim <input type = 'radio' value='2' " . (($deformidades_mmii == '2') ? "checked" : "") . " {$enabled} name= 'deformidades_mmii' class='deformidades_mmii OBG_RADIO'>N&atilde;o  </tr></td>";
        echo "<tr><td style='width:50%;'> Les&otilde;es </td> <td style='width:50%;'><input type = 'radio' value='1' " . (($lesoes_mmii == '1') ? "checked" : "") . " {$enabled} name= 'lesoes_mmii' class='lesoes_mmii OBG_RADIO'>Sim <input type = 'radio' value='2' " . (($lesoes_mmii == '2') ? "checked" : "") . " {$enabled} name= 'lesoes_mmii' class='lesoes_mmii OBG_RADIO'>N&atilde;o </tr></td>";
        echo "</table>";

        //EXTREMIDADES Atualiza��o Eriton 04-04-2013
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Extremidades</th></thead>";
        echo"<tr><td colspan='4'> <input type='checkbox' value='1' " . (($aquecidas_ext == '1') ? "checked" : "") . " {$enabled} name='aquecidas_ext' class='extremidades OBG_RADIO'>Aquecidas </td></tr>";
        echo"<tr><td colspan='4'> <input type='checkbox' value='1' " . (($oxigenadas_ext == '1') ? "checked" : "") . " {$enabled} name='oxigenadas_ext' class='extremidades OBG_RADIO'>Oxigenadas </td></tr>";
        echo"<tr><td colspan='4'> <input type='checkbox' value='1' " . (($frias_ext == '1') ? "checked" : "") . " {$enabled} name='frias_ext' class='extremidades OBG_RADIO'>Frias </td></tr>";
        echo"<tr><td colspan='4'> <input type='checkbox' value='1' " . (($cianoticas_ext == '1') ? "checked" : "") . " {$enabled} name='cianoticas_ext' class='extremidades OBG_RADIO'>Cian&oacute;ticas </td></tr>";
        echo"<tr><td colspan='4'> <input type='checkbox' value='1' " . (($pulso_presente == '1') ? "checked" : "") . " {$enabled} name='pulso_presente' class='extremidades OBG_RADIO'>Pulso Presente </td></tr>";

        echo "</table>";
        /* DISPOSITIVO INTRAVENOSO Atualiza��o Eriton 04-04-2013 - Evolu��o */
        echo "<table class='mytable' style='width:95%;'>";
       /* echo "<thead><tr><th colspan='3' >Dispositivos Intravenosos <select name='dispositivos_intravenosos' class='dispositivos_intravenosos COMBO_OBG' {$enabled}>
		<option value='-1'>   </option><option value='1' " . (($dispositivos_intravenosos == 1) ? "selected" : "") . "> N&atilde;o </option><option value='2' " . (($dispositivos_intravenosos == '2') ? "selected" : "") . ">Sim</option></select></th></thead>";
        if ($cvc >= 1) {
            $show_cvc = "style='display:table'";
        } else {
            $show_cvc = "style='display:none'";
        }

        if ($dispositivos_intravenosos == 2) {
            $show_dispositivos = "style='display:table'";
        } else {
            $show_dispositivos = "style='display:none'";
        }
        echo"<tr class='dispositivos_span' {$show_dispositivos}><td style='width:10%;' colspan='3'><input type='checkbox' value='1' " . (($cvc >= '1') ? "checked" : "") . " {$enabled} name='cvc' class='cvc valida_cvc'>CVC</td></tr>";
        echo "<tr class='cvc_span' {$show_cvc}><td style='width:10%;' colspan='2'><input type = 'radio' value='1' " . (($cvc == '1') ? "checked" : "") . " {$enabled}   name='cvc' >Uno Lumen </td><td>Local<input type ='text' name='local_uno' size='25' > </td></tr>";
        echo "<tr class='cvc_span' {$show_cvc}><td style='width:10%;' colspan='2'><input type = 'radio' value='2' " . (($cvc == '2') ? "checked" : "") . " {$enabled}   name='cvc' >Duplo Lumen  </td><td>Local<input type = 'text' size='25' value='{$local_cvc}' {$acesso}  name='local_duplo' ></td></tr>";
        echo"<tr class='cvc_span' {$show_cvc}><td style='width:10%;' colspan='2'><input type = 'radio' value='3' " . (($cvc == '3') ? "checked" : "") . " {$enabled}   name='cvc' >Triplo Lumen </td><td>Local<input type = 'text' size='25' value='{$local_cvc}' {$acesso}  name='local_cvc' ></td></tr>";
        echo"<tr {$show_dispositivos} class='dispositivos_span' ><td colspan='2'> <input type = 'checkbox' value='1' " . (($avp) ? "checked" : "") . " {$enabled}  name='avp' class='cvc'>AVP  </td><td>Local<input type = 'text' size='25' value='{$avp_local}' {$acesso} name='avp_local' ></td>";
        echo "<tr {$show_dispositivos} class='dispositivos_span' ><td colspan='2'><input type = 'checkbox' value='1' " . (($port) ? "checked" : "") . " {$enabled}   name='port' class='cvc'>Port-O-Cath </td><td>N&ordm;  <input type = 'text' size='5' maxlength='5' value='{$port_local}' {$acesso} name='quant_port' >Local<input type = 'text' size='25' value='{$port_local}' {$acesso} name='port_local' ></td></tr>";
        echo"<tr {$show_dispositivos} class='dispositivos_span' ><td colspan='2'> <input type = 'checkbox' value='1' " . (($picc) ? "checked" : "") . " {$enabled}  name='picc' class='cvc'>PICC  </td><td>Local<input type = 'text' size='25' value='{$picc_local}' {$acesso} name='picc_local' ></td>";
        echo"<tr {$show_dispositivos} class='dispositivos_span' ><td colspan = '3'>Outros<textarea cols=66 rows='3' name='outros_intra' value='{$outros_intra}' {$acesso}></textarea></td></tr>";
        echo "</table>";*/

        $this->dispositivosIntravenososEvolucao($id,$v);

        // FUNCIONAMENTO GASTROINTESTINAL Atualiza��o Eriton 04-04-2013
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Funcionamento Gastrointestinal</th></thead>";
        echo"<tr><td colspan='4'><b>Altera&ccedil;&otilde;es</b>
			<select name='alt_gastro' class='alt_gastro COMBO_OBG' {$enabled}><option value ='-1'>  </option>
			<option value ='n' " . (($alt_gastro == 'n') ? "selected" : "") . ">N&atilde;o</option>
			<option value ='s' " . (($alt_gastro == 's') ? "selected" : "") . ">Sim</option>
				</select></tr></td>";
        //ITENS GASTROINTESTINAL
        if ($alt_gastro >= 's') {
            $estilo = "style='display:table'";
        } else {
            $estilo = "style='display:none'";
        }
        echo "<tr><td colspan='4' >";
        echo "<table class='gastro_table' style='width:100%;'>";

        echo"<tr class='gastro_span' {$estilo}><td colspan='4'> <input type='radio' value='1' " . (($diarreia_obstipacao == '1') ? "checked" : "") . " {$enabled} name='diarreia_obstipacao' class='funcionamento_gastrointestinal OBG_RADIO'>" . htmlentities("diarréia") . " <input type='text' size='5' name='diarreia_episodios' value='{$diarreia_episodios}' {$acesso}> " . htmlentities("episódios") . " por dia</td></tr>";
        echo"<tr class='gastro_span' {$estilo}><td colspan='4'> <input type='radio' value='2' " . (($diarreia_obstipacao == '2') ? "checked" : "") . " {$enabled} name='diarreia_obstipacao' class='funcionamento_gastrointestinal OBG_RADIO'>" . htmlentities("obstipação") . " <input type='text' size='5' name='obstipacao_episodios' value='{$obstipacao_episodios}' {$acesso}> Dias </td></tr>";
        echo"<tr class='gastro_span' {$estilo}><td colspan='4'> <input type='checkbox' value='1' " . (($nauseas_gastro == '1') ? "checked" : "") . " {$enabled} name='nauseas_gastro' class='funcionamento_gastrointestinal OBG_RADIO'>" . htmlentities("náuseas") . " </td></tr>";
        echo"<tr class='gastro_span' {$estilo}><td colspan='4'> <input type='checkbox' value='1' " . (($vomitos_gastro == '1') ? "checked" : "") . " {$enabled} name='vomitos_gastro' class='funcionamento_gastrointestinal OBG_RADIO'>" . htmlentities("vômitos") . "<input type='text' size='5' name='vomito_episodios' value='{$vomito_episodios}' {$acesso}> Epis&oacute;dios por dia</td></tr>";
        echo"<tr class='gastro_span' {$estilo}>
                  <td colspan='4'>
                           <input type='checkbox' value='1' " . (($constipacao_gastro == '1') ? "checked" : "") . " {$enabled}
                              name='constipacao_gastro' class='funcionamento_gastrointestinal OBG_RADIO'>" . htmlentities("Constipação") . "
                  </td>
            </tr>";
        echo "</table>";
        echo "</td></tr>";
        echo "</table>";

        //FUNCIONAMENTO URINARIO Atualiza��o Eriton 04-04-2013
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Funcionamento Urin&aacute;rio</th></tr></thead>";
        echo"<tr><td width='25%'><input type='checkbox' value='1' " . (($normal_urinario == '1') ? "checked" : "") . " {$enabled} name='normal_urinario' class='funcionamento_urinario OBG_RADIO'>Normal</td>";
        echo"<td width='25%'><input type='checkbox' value='1' " . (($anuria_urinario == '1') ? "checked" : "") . " {$enabled} name='anuria_urinario' class='funcionamento_urinario OBG_RADIO'>An&uacute;ria </td>";
        echo"<td width='25%'> <input type='checkbox' value='1' " . (($incontinencia_urinario == '1') ? "checked" : "") . " {$enabled} name='incontinencia_urinario' class='funcionamento_urinario OBG_RADIO'>Incontin&ecirc;ncia </td>";
        echo"<td width='25%'> <input type='checkbox' value='1' " . (($hematuria_urinario == '1') ? "checked" : "") . " {$enabled} name='hematuria_urinario' class='funcionamento_urinario OBG_RADIO'>Hemat&uacute;ria </td></tr>";
        echo"<tr><td width='25%'> <input type='checkbox' value='1' " . (($piuria_urinario == '1') ? "checked" : "") . " {$enabled} name='piuria_urinario' class='funcionamento_urinario OBG_RADIO'>Pi&uacute;ria </td>";
        echo"<td width='25%'> <input type='checkbox' value='1' " . (($oliguria_urinario == '1') ? "checked" : "") . " {$enabled} name='oliguria_urinario' class='funcionamento_urinario OBG_RADIO'>Olig&uacute;ria </td>";
        echo"<td width='25%' colspan='2'> <input type='checkbox' value='1' " . (($odor_urinario == '1') ? "checked" : "") . " {$enabled} name='odor_urinario' class='funcionamento_urinario OBG_RADIO'>Odor </td></tr>";
        echo "</table>";


        /* DISPOSITIVOS PARA ELIMINA��ES E/OU EXCRE��ES Atualiza��o Eriton 04-04-2013 - Evolu��o */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Dispositivos para elimina&ccedil;&atilde;o e/ou excre&ccedil;&otilde;es <select name='eliminaciones' class='eliminaciones COMBO_OBG' {$enabled}><option value='-1'>   </option><option value='1' " . (($eliminaciones == '1') ? "selected" : "") . "> N&atilde;o </option><option value='2'" . (($eliminaciones == '2') ? "selected" : "") . ">Sim</option></select></th></thead>";
        if ($eliminaciones == '2') {
            $show_eliminaciones = '';
        } else {
            $show_eliminaciones = "style='display:none' ";
        }
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td style='width:33%;'> <input type = 'checkbox' value='1' " . (($fralda) ? "checked" : "") . " {$enabled} name='fralda' >Fralda Unidade </td><td><input type = 'text' size='5' value='{$fralda_dia}' {$acesso} maxlength='5' name='fralda_dia'> /Dia</td><td  colspan='2'>
	Tamanho:<input type = 'radio'  value='1' " . (($tamanho_fralda == '1') ? "checked" : "") . " {$enabled}  name='tamanho_fralda' >P <input type = 'radio'  value='2' " . (($tamanho_fralda == '2') ? "checked" : "") . " {$enabled}   name='tamanho_fralda' >M <input type = 'radio'  value='3' " . (($tamanho_fralda == '3') ? "checked" : "") . " {$enabled}  name='tamanho_fralda' >G <input type = 'radio'  value='4' " . (($tamanho_fralda == '4') ? "checked" : "") . " {$enabled}   name='tamanho_fralda' >XG</td></tr>";
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td style='width:33%;'> <input type = 'checkbox' value='1' " . (($urinario) ? "checked" : "") . " {$enabled}   name='urinario' >Dispositivo urin&aacute;rio </td><td colspan='3'>N&ordm;<input type = 'text' size='5' value='{$quant_urinario}' {$acesso} maxlength = '5'   name='quant_urinario' >  </td></tr>";
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td>  <input type = 'checkbox'  value='1' " . (($sonda) ? "checked" : "") . " {$enabled}   name='sonda' >Sonda de Foley </td><td colspan='3'>N&ordm;<input type = 'text' size='5' value='{$quant_foley}' {$acesso} maxlength='5' name='quant_foley' >  </td></tr>";
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td><input type = 'checkbox'  value='1' " . (($sonda_alivio) ? "checked" : "") . " {$enabled}   name='sonda_alivio' >Sonda de Al&iacute;vio </td><td colspan='3'>N&ordm;<input type = 'text' size='5' maxlength='5' value='{$quant_alivio}' {$acesso} name='quant_alivio' > </td></tr>";
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td><input type = 'checkbox'  value='1' " . (($colostomia) ? "checked" : "") . " {$enabled}   name='colostomia' >Colostomia Placa/Bolsa</td><td colspan='3'>N&ordm;<input type = 'text' size='5' maxlength='5' value='{$quant_colostomia}' {$acesso} name='quant_colostomia' >  </td></tr>";
        echo"<tr class='eliminaciones_span' $show_eliminaciones><td><input type = 'checkbox'  value='1' " . (($iliostomia) ? "checked" : "") . " {$enabled}   name='iliostomia' >Iliostomia Placa/Bolsa</td><td colspan='3'>N&ordm;<input type = 'text' size='5' maxlength='5' value='{$quant_iliostomia}' {$acesso} name='quant_iliostomia' >  </td></tr>";
        echo "<tr class='eliminaciones_span' $show_eliminaciones><td><input type = 'checkbox' value='1' " . (($cistostomia) ? "checked" : "") . " {$enabled}  name='cistostomia' >Cistostomia</td><td colspan='3'>N&ordm;<input type = 'text' size='5' value='{$quant_cistosmia}' name='quant_cistosmia'></td></tr>";
        echo "</table>";

        //Fim Atualiza��o Eriton 04-04-2013 - Evolu��o

        /* DRENOS */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Drenos </th></thead>";
        echo"<tr><td colspan='4' >
	<b>Drenos: </b>
	<select name='dreno_sn' class='dreno_select COMBO_OBG'  {$enabled}>
	<option value ='-1' > </option>
	<option value ='n' " . (($dreno_sn == 'n') ? "selected" : "") . "  >N&atilde;o</option>
			<option value ='s' " . (($dreno_sn == 's') ? "selected" : "") . "   >Sim</option>
					</select>
					&nbsp;&nbsp;";
        //<span name='dreno' id='dreno_span'><b>Tipo:</b><input type='text' name='dreno'  size=50/><button id='add-dreno' tipo=''  > + </button></span></td></tr>";

        if ($dreno_sn == 's') {
            $estilo = '';
            //$classe = "class'del-dreno'";
        } else {
            $estilo = " style='display:none;' ";
        }
        echo "<span name='dreno2' {$estilo}><b>Tipo:</b><input type='text' name='dreno'  {$acesso} size=50/><button id='add-dreno' tipo='' {$acesso}  > + </button></span></td></tr>";
        /* ITENS DO DRENO */
        echo "<tr><td colspan='4' >";
        echo "<table id='dreno' style='width:100%;'>";
        if ($dreno_sn == 's') {
            $sql4 = "select * from evolucao_enf_dreno where evolucao_enf_id = {$id_aval}";
            $result4 = mysql_query($sql4);

            while ($row4 = mysql_fetch_array($result4)) {
                $local_dreno = $row4['LOCAL'];
                if ($v == 1) {
                    $classe = '';
                } else {
                    $classe = "class='del-dreno'";
                }
                echo "<tr><td colspan='2' class='dreno' desc='{$local_dreno}' > <img src='../utils/delete_16x16.png'  {$classe} title='Excluir' border='0' {$enabled} > {$local_dreno} Local: <input type='text' name='local_dreno'  value='{$local_dreno}' {$acesso} > </td> </tr>";
            }
        }
        if (!empty($ev->dreno)) {
            foreach ($ev->dreno as $a) {
                echo "<tr><td>{$a}</td></tr>";
            }
        }
        echo "</table>";
        echo "</td></tr>";

        echo "</table>";



        /* FERIDAS */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='4' >Feridas</th></tr></thead>";
        echo"<tr><td colspan='4' ><b>Feridas: </b>
		<select name=feridas_sn class='COMBO_OBG' id='ferida' {$enabled}>
		<option value ='-1' > </option>
		<option value ='n' " . (($feridas_sn == 'n') ? "selected" : "") . "  >N&atilde;o</option>
				<option value ='s' " . (($feridas_sn == 's') ? "selected" : "") . "   >Sim</option>
						</select>

						</td></tr>";

        echo "</table>";

        echo "<span class='quadro_feridas_sn'>";
        echo "<div id='combo-div-quadro-feridas'>";
        $sql_feridas = "SELECT
                    ID
                FROM
                    quadro_feridas_enfermagem
                WHERE
                    EVOLUCAO_ENF_ID = '$id_aval'";
        $result_feridas = mysql_query($sql_feridas);
        while($row = mysql_fetch_array($result_feridas)){
            $this->feridas($row['ID'],$v);
        }
        echo "</div>";
        if($v ==0){
        echo "<br><button id='nova_ferida' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Nova Ferida</span></button><br><br>";
        }
//        echo "<br><a id='nova_ferida' tipo='' class='incluir' contador='1'>Nova Ferida</a><br>";
        echo "</span>";



        /* SINAIS VITAIS */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><th colspan='4' >Sinais vitais </th></tr></thead>";
        echo "<tr><td style='width:33%;'><b>PA Sist&oacute;lica:</b></td><td colspan='3'><input type='texto' class='numerico1 OBG' size='6' name='pa_sistolica' value='{$pa_sistolica}' {$acesso} />mmhg</td></tr>";
        echo "<tr><td><b>PA Dist&oacute;lica:</b></td><td colspan='3'><input type='texto' class='numerico1 OBG' size='6' name='pa_diastolica' value='{$pa_diastolica}' {$acesso} />MMHG</td></tr>";
        echo "<tr><td><b>FC: </b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='fc' value='{$fc}' {$acesso} />bpm</td></tr>";
        echo "<tr><td><b>FR:</b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='fr' value='{$fr}' {$acesso} />INC/MIN</td></tr>";
        echo "<tr><td><b>SPO&sup2;</b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='spo2' value='{$spo2}' {$acesso} />%</td></tr>";
        echo "<tr><td><b>Temperatura:</b></td><td colspan='3'><input type='texto' size='6' class='numerico1 OBG' name='temperatura' value='{$temperatura}' {$acesso} />&deg;C</td></tr>";
        echo "</table>";

          /* QUEIXAS E ANOTA��ES DE ENFERMAGEM */
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3' >QUEIXAS E ANOTA&ccedil;&otilde;ES DE ENFERMAGEM</th></tr></thead>";
        echo "<tr><td colspan='3' ><textarea cols=90 rows=5 name='pad' class='OBG' {$acesso} >$pad</textarea></td></tr>";
        echo "</table>";
        echo alerta();
        echo "</form></div>";

        if ($acesso != "readonly") {
            //echo "<button id='teste' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Teste</span></button></span>";
            echo "<button id='salvar-ficha-evolucao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
            echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
        }
    }
    public function servicoMultidisciplinarHospitalar($arrayMultdisciplinar,$v,$data = null)
    {
        $valorMedico = -1;
        $valorEnfermeiro = -1;
        $valorNutricao = -1;
        $valorFono = -1;
        $valorFissioMotora = -1;
        $valorFissioRespiratoria =-1;
        $disabled='';
        if(!empty($arrayMultdisciplinar)) {
            $valorMedico = $arrayMultdisciplinar['medico'];
            $valorEnfermeiro = $arrayMultdisciplinar['enfermeiro'];
            $valorNutricao = $arrayMultdisciplinar['nutricao'];
            $valorFono = $arrayMultdisciplinar['fono'];
            $valorFissioMotora = $arrayMultdisciplinar['fissio_motora'];
            $valorFissioRespiratoria = $arrayMultdisciplinar['fissio_respiratoria'];
            $disabled = $v == 1 ? 'disabled':'';

        }
        echo "<table class='mytable' style='width:95%;'>";
            echo "<thead>
                    <tr>
                        <th colspan='3' >
                        Serviço Multidiciplinar Hospitalar Recebido
                        </th>
                    </tr>
                </thead>";
        echo "<tr>
                <td colspan='3' >
                <label for='medico_multidisciplinar'>Visita Médico </label>
                ";
        // em 2017-05-05 esse select vai ser sim ou não para visualizar os anteriores foi feito essa regra
        $dataComparacao = empty($data) ? date('Y-m-d') : $data;
        $selectMedico = $dataComparacao > '2017-05-09' ? 3 : 1;
               $this->selectServicoMultidisciplinarHospitalar($selectMedico, $valorMedico, "medico_multidisciplinar",$disabled);
        echo    "</td>
                </tr>";
        echo "<tr>
                <td colspan='3' >
                <label for='enfermeiro_multidisciplinar'>Enfermeiro </label>
                ";
        // em 2017-05-05 esse select vai ser sim ou não para visualizar os anteriores foi feito essa regra
        $selectEnfermeiro = $dataComparacao > '2017-05-09' ? 3 : 1;
        $this->selectServicoMultidisciplinarHospitalar($selectEnfermeiro, $valorEnfermeiro, "enfermeiro_multidisciplinar",$disabled);
        echo    "</td>
                </tr>";
        echo "<tr>
                <td colspan='3' >
                <label for='nutricao_multidisciplinar'>Atendimento Nutrição </label>
                ";

        $this->selectServicoMultidisciplinarHospitalar(3, $valorNutricao, "nutricao_multidisciplinar",$disabled);
        echo    "</td>
                </tr>";
        echo "<tr>
                <td colspan='3' >
                <label for='fissio_motora_multidisciplinar'>Atendimento Fisio Motora</label>
                ";
        $this->selectServicoMultidisciplinarHospitalar(3, $valorFissioMotora,"fissio_motora_multidisciplinar",$disabled);
        echo    "</td>
                </tr>";
        echo "<tr>
                <td colspan='3' >
                <label for='fissio_respiratoria_multidisciplinar'>Atendimento Fisio Respiratória</label>
                ";
        $this->selectServicoMultidisciplinarHospitalar(3, $valorFissioRespiratoria,"fissio_respiratoria_multidisciplinar",$disabled);
        echo    "</td>
                </tr>";
        echo "<tr>
                <td colspan='3' >
                <label for='fono_multidisciplinar'>Atendimento Fono</label>
                ";
        $this->selectServicoMultidisciplinarHospitalar(3, $valorFono, "fono_multidisciplinar",$disabled);
        echo    "</td>
                </tr>";
        echo "</table>";

    }
   public function selectServicoMultidisciplinarHospitalar($tipo,$valor,$name,$disabled)
    {
        $html = "<select id='{$name}' name='{$name}' $disabled>";

        $selecione = "<option value ='-1' " . (($valor == '-1') ? "selected" : "") . "  >Selecione </option>";
        $html .= $selecione;
        if($tipo == 3) {
            $html .= "<option value ='N' " . (($valor == 'N') ? "selected" : "") . "  >N&atilde;o</option>
				        <option value ='S' " . (($valor == 'S') ? "selected" : "") . "   >Sim</option>";
        } else if ($tipo == 2 || $tipo == 1) {
            $html .= "<option value ='1' " . (($valor == '1') ? "selected" : "") . "  >Semanal</option>
				      <option value ='2' " . (($valor == '2') ? "selected" : "") . "   >Quinzenal</option>
				      <option value ='3' " . (($valor == '3') ? "selected" : "") . "   >Mensal</option>";
            $html .= $tipo == 1 ? '':"<option value ='3' " . (($valor == '4') ? "selected" : "") . "   >2x semana</option>";

        }
        $html .= "</select>";
        echo $html;
    }

}

//////////////////Visualizar ficha de evolu��o/////////////////////////////////////

switch ($_POST['query']) {
    case "frequencia_uso":
        $p = new Paciente();
        $p->frequencia_uso($_POST['id'], $_POST['v'], 1);
        break;
    case "feridas":
        $p = new Paciente();
        $p->feridas($_POST['contador']);
        break;
}
