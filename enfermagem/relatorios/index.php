<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';


use \App\Controllers\Enfermagem;

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
	"GET" => [
		'/enfermagem/relatorios/?action=form-relatorio-evolucoes'   => '\App\Controllers\Enfermagem::formRelatorioEvolucoes',
		'/enfermagem/relatorios/?action=excel-relatorio-evolucoes'  => '\App\Controllers\Enfermagem::excelRelatorioEvolucoes',
        '/enfermagem/relatorios/?action=procedimentos-gtm'          => '\App\Controllers\Enfermagem::listarProcedimentosGtm',
        '/enfermagem/relatorios/?action=procedimentos-gtm-view'     => '\App\Controllers\Enfermagem::viewProcedimentosGtm',
        '/enfermagem/relatorios/?action=procedimentos-gtm-edit'     => '\App\Controllers\Enfermagem::editarProcedimentosGtm',
	],
	"POST" => [
        '/enfermagem/relatorios/?action=form-relatorio-evolucoes' => '\App\Controllers\Enfermagem::relatorioEvolucoes',
        '/enfermagem/relatorios/?action=procedimentos-gtm-edit'   => '\App\Controllers\Enfermagem::atualizarProcedimentosGtm',
	]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}
