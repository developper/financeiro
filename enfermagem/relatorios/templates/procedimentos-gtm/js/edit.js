$(function ($) {
    var tipoAtual = [];
    var furo = [], cm = [], fr = [];

    $(".tipo-gtm").each(function () {
        tipoAtual.push($(this).val());
        if($(this).val() == '1'){
            furo.push($(".furo").val());
            cm.push($(".cm").val());
        } else {
            fr.push($(".fr").val());
        }
    });

    $(".tipo-gtm").live('change', function(){
        var atual = $(this).val();
        var index = $('.tipo-gtm').index($(this));
        var idProc = $(this).parents('.response').find('.id-proc').val();
        console.log(idProc);
        if(tipoAtual[index] != atual) {
            if ($(this).val() == '1') {
                $(this).parent().parent().next("tr").remove();
                $(this).parent().parent().after(
                    '<tr>' +
                    '   <td width="20%">FURO:</td>' +
                    '   <td width="80%"><input type="text" class="furo" required size="4" name="furo[' + idProc + ']" value="' + (furo[index] || '') + '"></td>' +
                    '</tr>' +
                    '<tr>' +
                    '   <td width="20%">CM:</td>' +
                    '   <td width="80%"><input type="text" class="cm" required size="4" name="cm[' + idProc + ']" value="' + (cm[index] || '') + '"></td>' +
                    '</tr>'
                );
            } else if($(this).val() != '1' && tipoAtual[index] == 1) {
                $(this).parent().parent().next("tr").remove();
                $(this).parent().parent().next("tr").remove();
                $(this).parent().parent().after(
                    '<tr>' +
                    '   <td width="20%">FR:</td>' +
                    '   <td width="80%"><input type="text" class="fr" required size="4" name="fr[' + idProc + ']" value="' + (fr[index] || '') + '"></td>' +
                    '</tr>'
                );
            }
            tipoAtual[index] = atual;
        }
    });
});