              <?php
/*
 * @autor Ricardo Tássio
 * 
 * 
 * **
 * */

              
$id = session_id();

if(empty($id))
  session_start();

include_once('../db/config.php');
include_once('../utils/codigos.php');
require_once('paciente_enf.php');
require_once('relatorios.php');

class Enfermagem  {

  
  private function menu(){
    
    echo "<p><a href='?view=listar'>Pacientes</a></p>";
    echo "<p><a href='?view=solicitacoes'>Solicita&ccedil;&otilde;es</a></p>";
    echo "<p><a href='?view=avulsa'>Solicita&ccedil;&atilde;o avulsa</a></p>";
    echo "<p><a href='?view=buscar'>Buscar Solicita&ccedil;&otilde;es </a></p>";
    echo "<p><a href='../medico/?view=buscar'>Buscar Prescri&ccedil;&otilde;es</a></p>";
    echo "<p><a href='?view=kits'>Kits</a></p>";
    echo "<p><a href='?view=buscas'>Envio materiais</a></p>";
    echo "<p><a href='../auditoria/?view=arquivos'>Acessar Prontu&aacute;rios</a></p>";
    echo "<p><a href='../logistica/?op=brasindice&act=buscar'>Brasindice</a></p>";
    echo "<p><a href='?view=checklist'>Checklist</a></p>";
    echo "<p><a href='?view=estoque'>Controle Estoque Domiciliar</a></p>";
  }
  
  private function formulario(){
    echo "<div id='formulario' title='Formul&aacute;rio' tipo='' classe='' >";
    echo alerta();
    echo "<center><div id='tipos'>
		<input type='radio' id='bmed' name='tipos' value='med' /><label for='bmed'>Medicamentos</label>
		<input type='radio' id='bmat' name='tipos' value='mat' /><label for='bmat'>Materiais</label>
                <input type='radio' id='bdie' name='tipos' value='die' /><label for='bdie'>Dietas</label>
		<input type='radio' id='bkits' name='tipos' value='kits' /><label for='bkits'>Kits</label>
    		
	</div></center>";
    echo "<p><b>Busca:</b><br/><input type='text' name='busca' id='busca-item' />";
    echo "<div id='opcoes-kits'></div>";
    echo "<input type='hidden' name='cod' id='cod' value='-1' />";
    echo "<input type='hidden' name='catalogo_id' id='catalogo_id' value='-1' />";
    echo "<p><b>Nome:</b><input type='text' name='nome' id='nome' class='OBG' />";
    echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
    echo "<button id='add-item' tipo='' >+</button></div>";
  }
  
      private function pacientes() {

        $medico = $_SESSION['id_user'];
//        $result = mysql_query("SELECT c.* FROM clientes as c, equipePaciente as e WHERE e.paciente = c.idClientes AND e.funcionario = {$medico} AND e.fim  IS NULL ORDER BY nome;");
//        $result = mysql_query("SELECT c.* FROM clientes as c WHERE c.status = 4 AND (c.empresa in{$_SESSION['empresa_user']} OR '1' in{$_SESSION['empresa_user']}) ORDER BY nome;");
        $result = mysql_query("SELECT c.* FROM clientes as c WHERE (c.empresa in{$_SESSION['empresa_user']} OR '1') ORDER BY status,nome;");

        echo "<p><b>Localizar Paciente</b></p>";
        echo "<select name='pacientes' style='background-color:transparent;width:362px' id='selPaciente' data-placeholder='Selecione um Paciente'  class='validacao_periodo_busca chosen-select'>";
        echo "<option value='-1'></option>";
        $aux = 0;
        $paciente_status = array();
        $status =array('1'=>'::     Novo    ::','4'=>'::   Ativo/Autorizado   ::','6'=>'::     Ã“bito    ::'); 
        while ($row = mysql_fetch_array($result)) 
        {
            $paciente = ucwords(strtolower($row['nome']));
            if(array_key_exists($row['status'], $paciente_status))
            {   
                $pacientes_por_status = array('idCliente'=>$row['idClientes'],'nome'=>$paciente,'status'=>$row['status']);
                # Verifica se a chave existe dentro do array $status, 
                # caso nÃ£o esteja, ele adiciona ao cÃ³digo 7 para que 
                # os outros possam ser ordenados juntos no select 
                if(array_key_exists($row['status'], $status))
                {
                    array_push($paciente_status[$row['status']],$pacientes_por_status);
                }
                else
                {
                    if(!array_key_exists(7,$paciente_status))
                    {
                        $paciente_status[7] = array();
                    }        
                    array_push($paciente_status[7],$pacientes_por_status);
                }    
                
            }
            else 
            {   
                $paciente_status[$row['status']] = array();
                $pacientes_por_status = array('idCliente'=>$row['idClientes'],'nome'=>$paciente,'status'=>$row['status']);
                # Verifica se a chave existe dentro do array $status, 
                # caso nÃ£o esteja, ele adiciona ao cÃ³digo 7 para que 
                # os outros possam ser ordenados juntos no select 
                if(array_key_exists($row['status'], $status))
                {
                    array_push($paciente_status[$row['status']],$pacientes_por_status);
                }
                else
                {
                    if(!array_key_exists(7,$paciente_status))
                    {
                        $paciente_status[7] = array();
                    }          
                    array_push($paciente_status[7],$pacientes_por_status);
                }    
                
            }
        }
      
       
       $cont=0;
       asort($paciente_status);
       foreach($paciente_status as $chave=>$dados)
       {   
            #Verifica se Ã© a primeira vez que ta rodando
            if(array_key_exists($chave, $status) && $cont==0)
            {
                echo "<optgroup></optgroup>";
                echo "<optgroup label='{$status[$chave]}'>";  
            }#Da segunda vez em diante passarÃ¡ por aqui
            elseif(array_key_exists($chave, $status) && $cont>0)
            {
                echo "</optgroup>";
                echo "<optgroup></optgroup>";
                echo "<optgroup label='{$status[$chave]}'>";  
            }
            elseif($chave==7)
            {
                echo "</optgroup>";
                echo "<optgroup></optgroup>";
                echo "<optgroup label='::     Outros     ::'>";  
            }
            foreach($dados as $chave2=>$dados2)
            {
                echo $chave;  
                
                echo "<option value='{$dados2['idCliente']}' status='{$chave}'>{$dados2['nome']}</option>";
            }
            $cont++;
       }    
        echo "</optgroup>";
        echo "</select>";
    }
  
  private function pacientes_presc() {

        $medico = $_SESSION['id_user'];
//        $result = mysql_query("SELECT c.* FROM clientes as c, equipePaciente as e WHERE e.paciente = c.idClientes AND e.funcionario = {$medico} AND e.fim  IS NULL ORDER BY nome;");
//        $result = mysql_query("SELECT c.* FROM clientes as c WHERE c.status = 4 AND (c.empresa in{$_SESSION['empresa_user']} OR '1' in{$_SESSION['empresa_user']}) ORDER BY nome;");
        $result = mysql_query("SELECT c.* FROM clientes as c WHERE (c.empresa in{$_SESSION['empresa_user']} OR '1') ORDER BY status,nome;");

        echo "<p><b>Localizar Paciente</b></p>";
        echo "<select name='pacientes' style='background-color:transparent;width:362px' id='selPaciente' data-placeholder='Selecione um Paciente'  class='validacao_periodo_busca chosen-select'>";
        echo "<option value='-1'></option>";
        $aux = 0;
        $paciente_status = array();
        $status =array('1'=>'::     Novo    ::','4'=>'::   Ativo/Autorizado   ::','6'=>'::     Ã“bito    ::'); 
        while ($row = mysql_fetch_array($result)) 
        {
            $paciente = ucwords(strtolower($row['nome']));
            if(array_key_exists($row['status'], $paciente_status))
            {   
                $pacientes_por_status = array('idCliente'=>$row['idClientes'],'nome'=>$paciente,'status'=>$row['status']);
                # Verifica se a chave existe dentro do array $status, 
                # caso nÃ£o esteja, ele adiciona ao cÃ³digo 7 para que 
                # os outros possam ser ordenados juntos no select 
                if(array_key_exists($row['status'], $status))
                {
                    array_push($paciente_status[$row['status']],$pacientes_por_status);
                }
                else
                {
                    if(!array_key_exists(7,$paciente_status))
                    {
                        $paciente_status[7] = array();
                    }        
                    array_push($paciente_status[7],$pacientes_por_status);
                }    
                
            }
            else 
            {   
                $paciente_status[$row['status']] = array();
                $pacientes_por_status = array('idCliente'=>$row['idClientes'],'nome'=>$paciente,'status'=>$row['status']);
                # Verifica se a chave existe dentro do array $status, 
                # caso nÃ£o esteja, ele adiciona ao cÃ³digo 7 para que 
                # os outros possam ser ordenados juntos no select 
                if(array_key_exists($row['status'], $status))
                {
                    array_push($paciente_status[$row['status']],$pacientes_por_status);
                }
                else
                {
                    if(!array_key_exists(7,$paciente_status))
                    {
                        $paciente_status[7] = array();
                    }          
                    array_push($paciente_status[7],$pacientes_por_status);
                }    
                
            }
        }
      
       
       $cont=0;
       asort($paciente_status);
       foreach($paciente_status as $chave=>$dados)
       {   
            #Verifica se Ã© a primeira vez que ta rodando
            if(array_key_exists($chave, $status) && $cont==0)
            {
                echo "<optgroup></optgroup>";
                echo "<optgroup label='{$status[$chave]}'>";  
            }#Da segunda vez em diante passarÃ¡ por aqui
            elseif(array_key_exists($chave, $status) && $cont>0)
            {
                echo "</optgroup>";
                echo "<optgroup></optgroup>";
                echo "<optgroup label='{$status[$chave]}'>";  
            }
            elseif($chave==7)
            {
                echo "</optgroup>";
                echo "<optgroup></optgroup>";
                echo "<optgroup label='::     Outros     ::'>";  
            }
            foreach($dados as $chave2=>$dados2)
            {
                echo $chave;  
                
                echo "<option value='{$dados2['idCliente']}' status='{$chave}'>{$dados2['nome']}</option>";
            }
            $cont++;
       }    
        echo "</optgroup>";
        echo "</select>";
    }
  
  private function solicitacoes_pendentes(){
  	$sql = "SELECT 
			       presc.*,
			       u.nome,
			       CONCAT(DATE_FORMAT(presc.inicio,'%d/%m/%Y'),'  até  ',DATE_FORMAT(presc.fim,'%d/%m/%Y')) AS sdata,
			       c.nome AS paciente,
			       idClientes 
			FROM 
			       prescricoes AS presc INNER JOIN  
			       clientes AS c ON (presc.paciente = c.idClientes) LEFT JOIN 
			       usuarios AS u ON (presc.criador = u.idUsuarios)
			WHERE        
			       (c.empresa in {$_SESSION['empresa_user']} OR '1' in{$_SESSION['empresa_user']}) AND
			       presc.lockp < NOW() AND presc.LIBERADO = 'S' 
			ORDER BY
			       DATA DESC";
	$result = mysql_query($sql);
	$flag = false;
	//echo "<input type='hidden' name='label' value='' /><input type='hidden' name='paciente' value='-1' />";
	if($_SESSION['adm_user']==1){//caso seja adm, a tabela trará a opção de desativar a solicitação
      		echo "<table class='mytable' width=100% ><thead><tr><th>Desativar</th><th>Paciente</th><th>Prescri&ccedil;&atilde;o</th><th>A&ccedil;&atilde;o</th></tr></thead><tfoot></tfoot>";                	  	
              }else{
	echo "<table class='mytable' width=100% ><thead><tr><th>Paciente</th><th>Prescri&ccedil;&atilde;o</th><th>A&ccedil;&atilde;o</th></tr></thead><tfoot></tfoot>";
              }
	$i=0;
	while($row = mysql_fetch_array($result)){
		
            echo "<input type='hidden' name='label' value='{$row['label']}' /><input type='hidden' name='paciente' value='{$row['paciente']}' />";
            if($z++%2==0){
                $cor = '#E9F4F8';
            }else {
    		$cor = '#FFFFFF';
            }
          foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
          $checked = "";
    //      if(!$flag) $checked = "checked";
    //      $flag = true;
	  $profissional = ucwords(strtolower($row['nome']));
	  $paciente = ucwords(strtolower($row['paciente']));
	  
	  /*if($row['Carater'] == 4){
	      $label = "<h1>Paciente: {$paciente}</h1><h3>Profissional: {$profissional} - ".htmlentities($row['sdata'])."</h3>";
	      echo "<tr label='{$label}' paciente='{$row['idClientes']}' antiga='{$row['id']}' class='trPrescricao' bgcolor='$cor' id='{$row['id']}'>";
	      echo "<td><!--<input type='radio' name='antigas' value='{$row['id']}' $checked />-->{$row['paciente']}</td>";
	      echo "<td>{$row['nome']} - ".utf8_encode($row['sdata'])."&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:orange'><b><i> ( AVALIA&Ccedil;&Atilde;o )</i></b></span></td>";
	      echo "<td><span style='float:left;'><a href='?view=solicitacoes&label={$label}&paciente={$row['idClientes']}&antigas={$row['id']}&carater={$row['Carater']}'><center><img src='../utils/solicitacao_enf.png' width='12' title='Iniciar Solicita��o'></center></a></span>";
	      if($_SESSION['adm_user']==1){
	      	echo "<span style='float:left;padding-top:2px;'><a href='#' class='desativar-presc' presc='{$row['id']}'><center><img style='padding-left:10px;' src='../utils/excluir.png' width='16' title='Desativar Solicita&ccedil;&atilde;o'></center></a></span>";
	      }
	      echo "</td>"; 
	      echo "</tr>";
	  }else{
          }
           */
          //////////////////////////////Atualizado Eriton 19-07-2013 - Tipos das Prescrições e suas cores///////////////////////////

       switch ($row['Carater']){
           case '1':
               $tipo_prescricao =  "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:red'><b><i> ( EMERG&Ecirc;NCIA )</i></b></span>";
               break;
           case '2':
               $tipo_prescricao =  "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:green'><b><i> ( PERI&Oacute;DICA M&Eacute;DICA )</i></b></span>";
               break;
           case '3':
               $tipo_prescricao =  "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:purple'><b><i> ( ADITIVA M&Eacute;DICA )</i></b></span>";
               break;
           case '4':
               $tipo_prescricao =  "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:orange'><b><i> ( PERI&Oacute;DICA DE AVALIA&Ccedil;&Atilde;O )</i></b></span>";
               break;
           case '5':
               $tipo_prescricao =  "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:green'><b><i> ( PERI&Oacute;DICA DE NUTRI&Ccedil;&Atilde;O )</i></b></span>";
               break;
           case '6':
               $tipo_prescricao =  "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:purple'><b><i> ( ADITIVA DE NUTRI&Ccedil;&Atilde;O )</i></b></span>";
               break;
           case '7':
               $tipo_prescricao =  "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:orange'><b><i> ( ADITIVA DE AVALIA&Ccedil;&Atilde;O )</i></b></span>";
               break;
       }
          
	  	$label = "<h1>Paciente: {$paciente}</h1><h3>Profissional: {$profissional} - ".htmlentities($row['sdata'])."</h3>";
	  	echo "<tr label='{$label}' paciente='{$row['idClientes']}' antiga='{$row['id']}' class='trPrescricao' bgcolor='$cor' id='{$row['id']}'>";
                if($_SESSION['adm_user']==1){
      		echo "<td><span style='float:left;padding-top:2px;'><a href='#' class='desativar-presc' presc='{$row['id']}'><center><img style='padding-left:10px;' src='../utils/excluir.png' width='16' title='Desativar Solicita&ccedil;&atilde;o'></center></a></span></td>";                	  	
              }
	  	echo "&nbsp;&nbsp;<!--<input type='radio' name='antigas' value='{$row['id']}' $checked />--><td>{$row['paciente']}</td>";
                echo "<td>{$row['nome']} - ".($row['sdata'])."{$tipo_prescricao}</td>";
	  	echo "<td><span style='float:left;'><a href='?view=solicitacoes&label={$label}&paciente={$row['idClientes']}&antigas={$row['id']}&carater={$row['Carater']}'><center><img src='../utils/solicitacao_enf.png' width='12' title='Iniciar Solicita&ccedil;atilde;o'></center></a></span>";
	  	/*if($_SESSION['adm_user']==1){
      		echo "<span style='float:left;padding-top:2px;'><a href='#' class='desativar-presc' presc='{$row['id']}'><center><img style='padding-left:10px;' src='../utils/excluir.png' width='16' title='Desativar Solicita&ccedil;&atilde;o'></center></a></span>";
      	
                echo "</td>";
	  	echo "</tr>";
	  	
	  }*/
	}
	echo "</table>";
  }
   //////////////////////////////Fim Atualizado Eriton 19-07-2013 - Tipos das Prescrições e suas cores///////////////////////////
  /*
  		Eriton tentando algo
    Mostrar na busca avulsa:
	periodo (data em que foi pedido)
	item
	quantidade
   */
  private function buscar_avulsa($paciente,$inicio,$fim){
  	
  	echo "<fieldset style='margin:0 auto;'>";
  	echo "<div id='div-busca-avulsa' >"; 
  	
  	echo alerta();
  	
  	echo "<div style='float:right;width:100%;'>";
  	echo "<center><h2 style='background-color:#8B0101;color:#ffffff;height:25px;vertical-align: middle;'><label style='vertical-align: middle;'>BUSCAR SOLICITA&Ccedil;&Otilde;ES AVULSAS</label></h2></center>";
  	echo $this->pacientes_presc();
  	echo $this->periodo();
  
  	echo "<p><button id='buscar-avulsa' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Buscar</span></button>";
  
  	
  	echo "</div>";
  
  	echo "</div>";
  	echo "</fieldset>";
  	echo "<div id='div-resultado-busca-avulsa'></div>";
  	   }
  // Fim Eriton *.*
  private function aprazamento($id,$freq,$cod,$cod1,$apraz1,$tipo){
  	if($tipo != -1){
  	$html = "<br/><b>Aprazamento: </b><div class='aprazamento' item='{$id}'>";
  	if($cod==$cod1)
  		$apraz1 = explode(" ",$apraz1);
  	else
  		$apraz1 ="";
  	
  	///if($cod <= 0 ||  $cod == 15 || $cod == 16 || $cod == 18 || $cod>23){
  		if($tipo !=0 && $tipo !=1 && $tipo !=2 && $tipo !=12 && $tipo != 13){
	  	for($i = 0; $i < $freq; $i++){
	  		$html .= "<input type='text' class='hora OBG' size='4' maxlength='5' />&nbsp;";
	  	}
	  	$html .= "<br>";
	  	for($j = 0; $j < $freq; $j++){
	  		$html .= "<b style='color:red;margin-left:17px;margin-right:17px;'>{$apraz1[$j]}</style></b><b style='padding:10px;'> </b>";
	  	}
  	}else{
  		$html = "";
  	}
  	if($freq == 0) $html = "";
  	else $html .= "</div>";
	}else{
	$html = "";
	}
  	return $html;
  }

 /////////////////////jeferson////////// 
 private function medanterior($idpresc,$cod,$cod1,$paciente,$id,$nome,$x,$catalogo_id,$catalogo_id1){
  //	$html.="<tr><td>teste</td></tr>";
///acrescentou $tipo !=0 && $tipo !=1 && $tipo !=2 && $tipo !=12 && $tipo != 13 no if
    if($catalogo_id == $catalogo_id1){

        $sql="
            SELECT
                B.NUMERO_TISS, B.ID, B.principio, B.apresentacao,S.autorizado,S.qtd, S.status
            FROM
                solicitacoes as S inner join catalogo as B on (B.ID = S.CATALOGO_ID) 
            WHERE
                B.ID='{$catalogo_id1}' and S.idPrescricao = '{$idpresc}' and S.tipo in (0,3)
            ORDER BY
                B.ID LIMIT 1 ";
                /// jeferson 23-07-2013 foi adicionado {and s.tipo in (0,3)}
        /*$result=mysql_query($sql);
        $principio= $result($result,0);

        $sql="select B.principio, B.  from principioativo where cod = $cod";*/
        $result=mysql_query($sql);

            //$sql="select I.nome, S.autorizado from solicitacoes as S inner join itens_prescricao as I on (I.cod=S.cod) where  S.idPrescricao = '$idpresc' and S.cod = '$cod1' and S.tipo= 0 order by nome limit 1";

        $result = mysql_query($sql);

            while($row = mysql_fetch_array($result)){
                if($row['autorizado'] > 0){
                    $nome=$row['principio']." ".$row['apresentacao'];
                    $html.="<tr class='dados' bgcolor='#ffffff' qtd='{$row['autorizado']}' cod='{$row['NUMERO_TISS']}' catalogo_id='{$row['ID']}' tipo='med' nome='{$nome}'>
                    <td>'{$nome}'  Quantidade: <input name='{$cod}' class='qtd_item' type='text' value='{$row['autorizado']}'></input><input type='checkbox' class='qtdok' aceito='1'>Ok</input>
                     <img class='rem-item-solicitacao' border='0' align='right' title='Remover' src='../utils/delete_16x16.png' item='{$id}'>
                    </td>
                    </tr>";
                }
                if($row['status'] == 4){
                    $nome=$row['principio']." ".$row['apresentacao'];
                    $html.="<tr class='dados' bgcolor='#ffffff' qtd='{$row['qtd']}' cod='{$row['NUMERO_TISS']}' catalogo_id='{$row['ID']}' tipo='med' nome='{$nome}'>
                    <td>'{$nome}'  Quantidade: <input name='{$cod}' class='qtd_item' type='text' value='{$row['qtd']}'></input><input type='checkbox' class='qtdok' aceito='1'>Ok</input>
                     <img class='rem-item-solicitacao' border='0' align='right' title='Remover' src='../utils/delete_16x16.png' item='{$id}'>
                    </td>
                    </tr>";
                }
            }
        }
        return $html;
    }  
  ////////////jefersonfim
  private function load_table($prescricao,$paciente){
  	$prescricao = anti_injection($prescricao,'numerico');
  	$html = "<table class='mytable' width=100% ><thead><tr>";
    $html .= "<th><b>Itens prescritos</b></th>";
//    $sql = "SELECT v.via as nvia, f.frequencia as nfrequencia,i.*, i.inicio, 
//    		i.fim, DATE_FORMAT(i.aprazamento,'%H:%i') as apraz 
//    		FROM itens_prescricao as i LEFT OUTER JOIN frequencia as f ON (i.frequencia = f.id) LEFT OUTER JOIN viaadm as v ON (i.via = v.id)
//    		WHERE idPrescricao = '$prescricao' 
//    		ORDER BY tipo,inicio,aprazamento,nome,apresentacao ASC";
	
    /* SQL ORIGINAL COMENTADO POR RICARDO  05/07/2012 */
   //$sql = "SELECT i.*, f.qtd FROM itens_prescricao as i, frequencia as f WHERE idPrescricao = '$prescricao' AND f.id = i.frequencia ORDER BY tipo,inicio,aprazamento,nome,apresentacao ASC";
   $sql2="SELECT p.`id`, p.`Carater` FROM prescricoes p WHERE  p.id='{$prescricao}' ";
    $result2=mysql_query($sql2);
    $carater_prescricao = mysql_result($result2,0,1);
   
    if($carater_prescricao == 2 || $carater_prescricao == 5 || $carater_prescricao == 4){
		if($carater_prescricao == 2){
			$condicao="p.`Carater` in (2,4) AND ";
		}else{
			$condicao = "p.`Carater` ={$carater_prescricao} AND ";
		}
    }else{
		$condicao = "";
	}
   
    $sql = "SELECT 
            i.*, 
            f.qtd,
            1 AS col 
	FROM
            itens_prescricao AS i, 
            frequencia AS f
	WHERE 
	idPrescricao = (SELECT 
            p.`id` 
        FROM prescricoes p 
        WHERE 
        {$condicao}
        p.id<>'{$prescricao}' 
        AND p.`paciente`='{$paciente}' 
        and p.DESATIVADA_POR = 0 
        and p.STATUS = 0 
        ORDER BY p.`id` DESC LIMIT 1 ) 
    AND f.id = i.frequencia
   	UNION
    SELECT 
	i.*, 
	f.qtd,
	2 AS col 
	FROM 
	itens_prescricao AS i, 
	frequencia AS f 
	WHERE 
	idPrescricao = '{$prescricao}'
	AND f.id = i.frequencia 
	ORDER BY 
	tipo,NUMERO_TISS,CATALOGO_ID,col,inicio,aprazamento,nome,apresentacao ASC";

   /* if($x == 5)
    	{
    		$sql = "SELECT i.*, f.qtd,1 AS col FROM itens_prescricao AS i, frequencia AS f WHERE idPrescricao = (SELECT p.`id` FROM prescricoes p WHERE p.`Carater`= 5 AND p.id<>'{$prescricao}' AND p.`paciente`='{$paciente}' ORDER BY p.`id` DESC LIMIT 1 ) AND f.id = i.frequencia
    		UNION
    		SELECT i.*, f.qtd,2 AS col FROM itens_prescricao AS i, frequencia AS f WHERE idPrescricao = '{$prescricao}' AND f.id = i.frequencia ORDER BY tipo,cod,col,inicio,aprazamento,nome,apresentacao ASC";
    	}*/
    $result = mysql_query($sql);
   
//    $cor = false;
	$qtd = 0;
    $lastType = 0;
    $cont = 0;
    $html .= "</tr></thead>";
    //$row1 = array();
    
    while($row = mysql_fetch_array($result)){
	
    	//grava os itens da prescri��o anterior na $row1, para ser comparado nas fun��es  aprazamento, medanterior e matanterior(ainda n�o foi criado).  
        if($row['col']==1){
            $row1 = $row;
    	}
    	
    	if($row['col']==2){
    	
            $i = implode("/",array_reverse(explode("-",$row['inicio'])));
            $f = implode("/",array_reverse(explode("-",$row['fim'])));	    	
	    	
	//$aprazamento = "";
	//if($row['tipo'] <> 0) $aprazamento = $row['apraz'];
	      	$dados = " id='{$row['id']}' ";
    //      	$linha = "{$row['nome']} {$row['apresentacao']} {$row['nvia']} {$row['nfrequencia']} {$aprazamento} Per&iacute;odo: $i até $f OBS: {$row['obs']}";
                $html .= "<tr class='item-prescrito' qtd='0' bgcolor='#ebf3ff' $dados >";
                $botoes = "<img class='add-kit' tipo='kits' item='{$row['id']}' src='../utils/kit_16x16.png' title='Adicionar kit' border='0'>&nbsp;&nbsp;";
                $botoes .= "<img class='add-med' tipo='med' item='{$row['id']}' src='../utils/medicamento_16x16.png' title='Adicionar medicamento' border='0'>&nbsp;&nbsp;";
                $botoes .= "<img class='add-mat' tipo='mat' item='{$row['id']}' src='../utils/material_16x16.png' title='Adicionar material' border='0'>&nbsp;&nbsp;";
                //$botoes .= "<img class='add-serv' tipo='serv' item='{$row['id']}' src='../utils/servicos_16x16.png' title='Servi&ccedil;os' border='0'>&nbsp;&nbsp;";
                $botoes .= "<img class='add-die' tipo='die' item='{$row['id']}' src='../utils/formula_16x16.png' title='Dieta' border='0'>&nbsp;&nbsp;";
                $botoes .= "<img class='look-kit' item='{$row['id']}' src='../utils/look_16x16.png' title='Visualizar' border='0'>&nbsp;&nbsp;";
                $botoes .= "<img class='obs-enf' item='{$row['id']}' src='../utils/exclama.png' width='16' title='Observa&ccedil;&atilde;o' border='0'>&nbsp;&nbsp;";
                $obs =  "<div class='tabela-kits' id='obs-{$row['id']}'><input type='text' size='80' class='obs' cod='{$row['id']}' id='observacao-{$row['id']}'></div>";
                $kits = "<table class='mytable tabela-kits' width=100% id='kits-{$row['id']}'><thead><tr><th>KITS</th></tr></thead></table>";
                $med = "<table class='mytable tabela-med' width=100% id='med-{$row['id']}'><thead><tr><th>Medicamentos</th></tr></thead>".$this->medanterior($row1['idPrescricao'],$row['NUMERO_TISS'],$row1['NUMERO_TISS'],$row['paciente'],$row['id'],$row['nome'],$carater_prescricao,$row['CATALOGO_ID'],$row1['CATALOGO_ID'])."</table>";
                $mat = "<table class='mytable tabela-mat' width=100% id='mat-{$row['id']}'><thead><tr><th>Materiais</th></tr></thead></table>";
                //$serv = "<table class='mytable tabela-serv' width=100% id='serv-{$row['id']}'><thead><tr><th>Servi&ccedil;os</th></tr></thead></table>";
                $dieta = "<table class='mytable tabela-die' width=100% id='die-{$row['id']}'><thead><tr><th>Dietas</th></tr></thead></table>";

                $aprazamento = $this->aprazamento($row['id'],$row['qtd'],$row['NUMERO_TISS'],$row1['NUMERO_TISS'],$row1['aprazamento'],$row['tipo'],$row['CATALOGO_ID'],$row1['CATALOGO_ID']);

                /* COMENTADO POR RICARDO PARA FAZER APARECER A BARRA DE KITS, MEDICAMENTOS E MATERIAL PARA F�RMULA*/
                if($row['tipo'] == '12'){

                    $campo_formula = "<br/>Quantidade: <input type='text' size='4' value='' name='qtd-formula' cod='{$row['cod']}' tipo='12' class='numerico' >";
                    //$html .= "<td><span class='status'></span>{$row['descricao']}{$aprazamento}<br/>{$campo_formula}</td>";
                    $html .= "<td><span class='status'></span> $botoes {$obs}{$row['descricao']}{$aprazamento}<br/>{$campo_formula}<br/>$kits<br/>$med<br/>$mat<br/>$serv</td>";
                } else { 
                    $html .= "<td><span class='status'></span> $botoes {$obs}{$row['descricao']}{$aprazamento}<br/>$kits<br/>$med<br/>$mat<br/>$serv<br/>$dieta</td>";
                }
			
			
	    	$html .= "</tr>";
	    	$cont++;
    	}
    
    }
            
    $html .= "</table>";
    
    if($carater_prescricao == 2 || $carater_prescricao == 4){
    	$inicio = strftime("%Y-%m-%d", strtotime("-30 days"));
    	$fim = date("Y-m-d");
    	$sql = "SELECT
                    CONCAT(b.principio,' ',b.apresentacao) AS item,						
                    SUM(s.qtd) AS quantidade,
                    s.DATA_SOLICITACAO AS data,
                    s.NUMERO_TISS AS numero_tiss,
					s.CATALOGO_ID 
                FROM
                    solicitacoes AS s INNER JOIN
                    catalogo AS b ON (s.CATALOGO_ID  = b.ID )
                WHERE
                    s.tipo = 1
                    and DATE_FORMAT(s.DATA_SOLICITACAO,'%Y-%m-%d') BETWEEN '{$inicio}' AND '{$fim}'		               	
                    and s.paciente = $paciente
                GROUP BY
                   s.CATALOGO_ID
                ";
    	$html.="<label style='font-size:14px;color:#8B0000'><b><center> PEDIDOS DE MATERIAIS NOS &Uacute;LTIMOS 30 DIAS </center></b></label>";
    	$html.="<table class='mytable' width = 100% >";
    	$html.="<thead> <tr>";
    	$html.="<th><b>Item</b></th>";

    	$html.="</tr></thead>";
    	
    	$result = mysql_query($sql);
    	
    	    while($row = mysql_fetch_array($result)){
	    	$html.="<tr class='dados' bgcolor='#ffffff' qtd='{$row['quantidade']}' cod='{$row['numero_tiss']}' catalogo_id='{$row['CATALOGO_ID']}' tipo='mat'>
	    	<td>'{$row['item']}'  Quantidade: <input name='{$row['numero_tiss']}' catalogo_id='{$row['CATALOGO_ID']}' class='qtd_item' type='text' value='{$row['quantidade']}'>
	    	</input><input type='checkbox' class='qtdok' aceito='1'>Ok</input><img class='rem-item-solicitacao' border='0' align='right' title='Remover' src='../utils/delete_16x16.png' item='{$row['CATALOGO_ID']}'>
	    	</td></tr>";
    	 	}
    	 $html.="</table>";	
    }
	//$html.=$sql;
    return $html;    
  }
 
  
  private function prescricoes($label,$data,$antiga,$paciente,$carater){
    if($antiga == NULL){
      echo "<center><h1>Solicita&ccedil;&otilde;es Pendentes</h1></center>";
      echo "<form id='fromPresc' method='post' >";
      $this->solicitacoes_pendentes();
      //echo "<p><button id='iniciar-solicitacao'>Iniciar</button></form>";
      echo "</form>";
    } else {
      $antiga = anti_injection($antiga,'numerico');
      $r = mysql_query("SELECT lockp FROM prescricoes WHERE id = '$antiga' AND lockp < now() ");
      if(mysql_num_rows($r) == 0){
      	redireciona('index.php?view=solicitacoes');
      } else {
      	$r = mysql_query("UPDATE prescricoes SET lockp = DATE_ADD(now(),INTERVAL 1 MINUTE) WHERE id = '$antiga' ");
      }
      echo "<div id='dialog-salvo' title='Finalizada'>";
      echo "<center><h3>Prescri&ccedil;&atilde;o salva com sucesso!</h3></center>";
      echo "<form action='../medico/imprimir.php' method='post'><input type='hidden' value='-1' name='prescricao'><center><button>Imprimir</button></center></form>";
      echo "</div>";
      echo "<div id='solicitacao' paciente='$paciente' prescricao='$antiga' carater='$carater' >";
      echo "<center>$label</center>";
      $this->formulario();
      echo "<div id='dialog-servicos' Title='Adicionar Servi&ccedil;os'>";
      echo alerta();
      echo "<b>Servi&ccedil;o:</b>".combo_servicos("servicos");
      echo "<br/><b>Quantidade:</b> <input type='text' id='qtd-serv' name='qtd-serv' class='numerico OBG' size='3'/>";
      echo "<br/><button id='add-serv' tipo='' >+</button>";
      echo "</div><div id='tabela-solicitacoes'>";
      echo alerta()."<br/>";
       ////equipamento
    if (($carater == 3) || ($carater == 6)){
        echo"<fieldset style='width:550px;' id='field_avulsa'>";
        echo "<legend><b>Data de entrega maxima:<b></legend>";
        echo"<p><b>Data: </b><input type='text' class='data OBG' id='data-aditiva' name='data-aditiva' />";
        echo "<b>Hora: </b><input type='text'  class='hora OBG '  id='hora-aditiva' name='hora-aditiva'/></p>";
        echo"</fieldset>";
    }
      
      echo"<input type='checkbox' id='equip_ativo' />Equipamentos.";
      echo"<div id='span_equipamentos_ativos' style='display:none;'>";
      echo"<table id='equipamentos_ativos'>";

      $sql="select ID_CAPMEDICA from prescricoes where paciente={$paciente} and Carater = 4 order by ID_CAPMEDICA desc limit 1";
      $result = mysql_query($sql);
      $x = mysql_result($result, 0,0);

      $sql="select id ,ID_CAPMEDICA from prescricoes where id < {$antiga} and paciente = {$paciente} order by id desc limit 1";
      $result = mysql_query($sql);
      $avaliacao = mysql_result($result, 0,1);

      $sql="select CAPMEDICA_ID from equipamentosativos where CAPMEDICA_ID={$x} order by CAPMEDICA_ID desc limit 1 ";
      $result = mysql_query($sql);
      $y = mysql_result($result, 0,0);

      if($y != '' && $y != null ){
        if($avaliacao != 0){
            $sql = "SELECT
            C.id,
            C.item,
            C.unidade,
            C.tipo,
            eqp.QTD,
            eqp.OBS,
            eqp.ID,
            eqp.PRESCRICAO_AVALIACAO,
            eqp.PRESCRICAO_ID
            FROM
            equipamentosativos as eqp INNER JOIN
            cobrancaplanos as C ON (C.id = eqp.COBRANCA_PLANOS_ID)
            WHERE
            eqp.CAPMEDICA_ID = $avaliacao";


            $result = mysql_query($sql);
            $equipamento = '';
            //echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
            while($row=mysql_fetch_array($result)){
                $equipamento[$row[id]] = array("item"=>$row['item'],"und"=>$row['unidade'],"tipo"=>$row['tipo'],"qtd"=>$row['QTD'],"obs"=>$row['OBS'],"data"=>$row['DATA']);
            }
            $sql2= "select * from cobrancaplanos where tipo=1";

            $result2=mysql_query($sql2);

            echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>A&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td><td><b>Ultima Substitui&ccedil;&atilde;o</b></td></tr>";
            while($row=mysql_fetch_array($result2)){
                if(array_key_exists($row['id'], $equipamento)){
                    $check = "checked='checked'";
                    $qtd = $equipamento[$row['id']]['qtd'];
                    $obs = $equipamento[$row['id']]['obs'];
                    $data = $equipamento[$row['id']]['data'];
                    $botao = "<button style='background-color:#ff0000; width: 80px;' class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='2' acao='1' tipo_solicitacao='{$tipo_solicitacao}' >Recolher</button>&nbsp;&nbsp;<button style='background-color:#FFA824; width: 80px;' class='equipamento_substituir' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='3' acao='1' tipo_solicitacao='{$tipo_solicitacao}'>Substituir</button>";
                }else{
                    $check ='';
                    $qtd='';
                    $obs = '';
                    $data = '';
                    $botao = "<button style='background-color:#00ff00; width: 80px;' class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='1' acao='0' tipo_solicitacao='{$tipo_solicitacao}' >Solicitar</button>";
                }
                //echo"<tr><td><input type='checkbox' $check class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}'></input>";
                echo "<tr><td>{$row['item']}</td><td>$botao</td><td><input type='texto' readonly id=qtd"."{$row['id']}   class='qtd_equipamento' value='{$qtd}'  name='qtd_equipamento'  {$acesso} ></input></td><td><input type='texto' readonly id=data"."{$row['id']} class='data_equipamento' value='{$data}' name='data_equipamento' {$acesso}></input></td></tr>";
            }

        }else{
            $sql = "SELECT
            C.id,
            C.item,
            C.unidade,
            C.tipo,
            eqp.QTD,
            eqp.OBS,
            eqp.ID as idativo,
            eqp.PRESCRICAO_AVALIACAO,
            eqp.PRESCRICAO_ID
            FROM
            equipamentosativos as eqp INNER JOIN
            cobrancaplanos as C ON (C.id = eqp.COBRANCA_PLANOS_ID)

            WHERE
            eqp.CAPMEDICA_ID = {$x} and DATA_FIM = '0000-00-00 00:00:00' ";


            $result = mysql_query($sql);
            $equipamento = '';
            //echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
            while($row=mysql_fetch_array($result)){
                    $equipamento[$row[id]] = array("item"=>$row['item'],"und"=>$row['unidade'],"tipo"=>$row['tipo'],"qtd"=>$row['QTD'],"obs"=>$row['OBS'],"idativo"=>$row['idativo'],"data"=>$row['DATA']);
            }
            $sql2= "select * from cobrancaplanos where tipo=1";

            $result2=mysql_query($sql2);

            echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>A&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td><td><b>Ultima Substitui&ccedil;&atilde;o</b></td></tr>";
            while($row=mysql_fetch_array($result2)){
                if(array_key_exists($row['id'], $equipamento)){
                    $check = "checked='checked'";
                    $qtd = $equipamento[$row['id']]['qtd'];
                    $obs = $equipamento[$row['id']]['obs'];
                    $idativo = $equipamento[$row['id']]['idativo'];
                    $data = $equipamento[$row['id']]['data'];
                    $botao = "<button style='background-color:#ff0000; width: 80px;' class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='2' acao='1' tipo_solicitacao='{$tipo_solicitacao}'>Recolher</button>&nbsp;&nbsp;<button style='background-color:#FFA824; width: 80px;' class='equipamento_substituir' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='3' acao='1' tipo_solicitacao='{$tipo_solicitacao}'>Substituir</button>";
                }else{
                    $check ='';
                    $qtd='';
                    $obs = '';
                    $idativo='';
                    $botao = "<button style='background-color:#00ff00; width: 80px;' class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='1' acao='0' tipo_solicitacao='{$tipo_solicitacao}'>Solicitar</button>";
                }
            //echo"<tr><td><input type='checkbox' $check class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}'></input>";
            echo "<tr><td>{$row['item']}</td><td>$botao</td><td><input type='texto' readonly id=qtd"."{$row['id']}   class='qtd_equipamento' value='{$qtd}'  name='qtd_equipamento'  {$acesso} ></input></td><td><input type='texto' readonly id=data"."{$row['id']} class='data_equipamento' value='{$data}' name='data_equipamento' {$acesso}></input></td></tr>";
            }

        }
      }else{
        $sql = "SELECT
        C.id,
        C.item,
        C.unidade,
        C.tipo,
        eqp.QTD,
        eqp.OBSERVACAO

        FROM
        equipamentos_capmedica as eqp INNER JOIN
        cobrancaplanos as C ON (C.id = eqp.COBRANCAPLANOS_ID)

        WHERE
        eqp.CAPMEDICA_ID = ".$x;


        $result = mysql_query($sql);
        $equipamento = '';

        while($row=mysql_fetch_array($result)){
            $equipamento[$row[id]] = array("item"=>$row['item'],"und"=>$row['unidade'],"tipo"=>$row['tipo'],"qtd"=>$row['QTD'],"obs"=>$row['OBSERVACAO'],"data"=>$row['DATA']);
        }
        $sql2= "select * from cobrancaplanos where tipo=1";

        $result2=mysql_query($sql2);

        echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>A&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td><td><b>Ultima Substitui&ccedil;&atilde;o</b></td></tr>";
        while($row=mysql_fetch_array($result2)){
            if(array_key_exists($row['id'], $equipamento)){
                $check = "checked='checked'";
                $qtd = $equipamento[$row['id']]['qtd'];
                $obs = $equipamento[$row['id']]['obs'];
                $data = $equipamento[$row['id']]['data'];
                $botao = "<button style='background-color:#ff0000; width: 80px;' class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='2' acao='1' tipo_solicitacao='{$tipo_solicitacao}'>Recolher</button>&nbsp;&nbsp;<button style='background-color:#FFA824; width: 80px;' class='equipamento_substituir' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='3' acao='1' tipo_solicitacao='{$tipo_solicitacao}'>Substituir</button>";
            }else{
                $check ='';
                $qtd='';
                $obs = '';
                $data = '';
                $botao = "<button style='background-color:#00ff00; width: 80px;' class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='1' acao='0' tipo_solicitacao='{$tipo_solicitacao}'>Solicitar</button>";
            }
            //echo"<tr><td><input type='checkbox' $check class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}'></input>";
                    echo "<tr><td>{$row['item']}</td><td>$botao</td><td><input type='texto' readonly id=qtd"."{$row['id']}   class='qtd_equipamento' value='{$qtd}'  name='qtd_equipamento'  {$acesso} ></input></td><td><input type='texto' readonly id=data"."{$row['id']} class='data_equipamento' value='{$data}' name='data_equipamento' {$acesso}></input></td></tr>";
        }
      }

      echo"</table></div>";
      $tabela = $this->load_table($antiga,$paciente);
      echo $tabela;
      echo "<br/><button id='salvar-solicitacao' tipo='' style='float:left;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></form></div>";
      echo "<button id='sair-sem-salvar' tipo='' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></form></div>";
      
    }
  }
  
  private function kits(){
  	echo "<div id='dialog-kit' title='Lista Materiais' kit=''>";
  	echo "<div id='div-tabela-fantasias'></div>";
	echo alerta();
	echo "<center><h3 style='text-transform:uppercase' id='h-nome-kit'></h3></center>";
	echo "<p><b>Busca:</b><br/><input type='text' name='busca' id='busca' />";
    echo "<div id='opcoes-itens'></div>";
    echo "<input type='hidden' name='cod' id='cod' value='-1' />";
    echo "<input type='hidden' name='catalogo_id' id='catalogo_id' value='-1' />";
    echo "<p><b>Nome:</b><input type='text' name='nome' id='nome' class='OBG' size='50' />";

    echo "<b>Quantidade:</b> <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>";
    echo "<button id='add-item-kit' tipo='' >+</button>";
    echo "<div id='tabela-itens-kit'></div>";
  	echo "</div>";
  	echo "<center><h1>Kits de material</h1></center>";
  	echo "<input type='text' name='nome-kit' maxlength='50' /> <button id='criar-novo-kit'>Criar Novo</button>";
  	echo "<div id='div-lista-kits'></div>";
  }
  
  private function buscas(){
  	echo "<h1><center>Envio de materiais</center></h1>";
    echo "<b>Buscar por:</b><input type='radio' name='tipo-busca-saida' value='med' />Medicamento 
    	 <input type='radio' name='tipo-busca-saida' value='mat' />Material 
    	 <input type='radio' name='tipo-busca-saida' value='paciente' />Paciente";
   	echo "<div id='combo-busca-saida'></div>";
    echo "<p><b>Resultado:</b><input type='radio' name='resultado' value='a' checked />Analítico<input type='radio' name='resultado' value='c' />Condensado";
    echo "<table><tr><td colspan='4'>";
    $hoje = date("j/n/Y");
    echo "</td></tr><tr><td><b>In&iacute;cio do per&iacute;odo:</b></td><td><input id='inicio' type='text' name='inicio' value='$hoje' maxlength='10' size='10' /></td>";
    echo "<td><b>Fim do per&iacute;odo:</b></td><td><input id='fim' type='text' name='fim' value='$hoje' maxlength='10' size='10' /></td></tr></table>";
    echo "<button id='buscar-saida' >Mostrar</button>";
    echo "<div id='resultado-busca' ></div>";
  }
  
  private function avulsa($paciente,$nome,$e,$data_avulsa,$hora_avulsa){
  	echo "<h1><center>Solicita&ccedil;&atilde;o Avulsa</center></h1>";
        if($e == 0){
            $tipo_solicitacao = -1;
        }else if($e == 2){
            $tipo_solicitacao = -2;
        }
  	if($paciente == NULL){
            echo "<div id='div-iniciar-avulsa'>";
            echo alerta();
            echo "<form method='post' >";
            ///jeferson dta e hora avulsa e emergencia 23-03-2013
            echo"<fieldset style='width:550px;' id='field_avulsa'>";
            echo "<legend><b>Data de entrega maxima:<b></legend>";
            echo"<p><b>Data: </b><input type='text' class='data OBG' id='data-avulsa' name='data-avulsa' />";
            echo "<b>Hora: </b><input type='text'  class='hora OBG'  id='hora-avulsa' name='hora-avulsa'/></p>";
            echo"</fieldset>";
            ///fim data e hora 23-03-2013
            echo $this->pacientes(NULL,"pacientes","n");
            echo "<input type='hidden' name='npaciente' value='' />";
            echo "<input type='checkbox' name='emergencia' value='2' />Emerg&ecirc;ncia";
            echo "<p><button id='iniciar-avulsa'>Iniciar</button></form></div>";
  	} else {
            $this->formulario();
            echo "<div id='dialog-servicos' Title='Adicionar Servi&ccedil;os'>";
            echo alerta();
            echo "<b>Servi&ccedil;o:</b>".combo_servicos("servicos");
            echo "<br/><b>Quantidade:</b> <input type='text' id='qtd-serv' name='qtd-serv' class='numerico OBG' size='3'/>";
    	echo "<br/><button id='add-serv' tipo='' item='avulsa' >+</button>";
        echo "</div>";
        echo "<div id='avulsa' paciente='$paciente' e='$e'>";
        echo "<input type='hidden' id='solicitacao' paciente='{$paciente}' />";
        echo "<h3><center>$nome</center></h3>";
        echo"<br>";
        echo"<input type='checkbox' id='equip_ativo' />Equipamentos.";
        echo"<div id='span_equipamentos_ativos' style='display:none;'>";
        echo"<table id='equipamentos_ativos'>";

        $sql="select ID_CAPMEDICA from prescricoes where paciente={$paciente} and Carater = 4 order by ID_CAPMEDICA desc limit 1";
        $result = mysql_query($sql);
        $x = mysql_result($result, 0,0);

        $sql="select id ,ID_CAPMEDICA from prescricoes where id < {$antiga} and paciente = {$paciente} order by id desc limit 1";
        $result = mysql_query($sql);
        $avaliacao = mysql_result($result, 0,1);

        $sql="select CAPMEDICA_ID from equipamentosativos where CAPMEDICA_ID={$x} order by CAPMEDICA_ID desc limit 1 ";
        $result = mysql_query($sql);
        $y = mysql_result($result, 0,0);

        if($y != '' && $y != null ){
            if($avaliacao != 0){
                $sql = "SELECT
                C.id,
                C.item,
                C.unidade,
                C.tipo,
                eqp.QTD,
                eqp.OBS,
                eqp.ID,
                eqp.PRESCRICAO_AVALIACAO,
                eqp.PRESCRICAO_ID
                FROM
                equipamentosativos as eqp INNER JOIN
                cobrancaplanos as C ON (C.id = eqp.COBRANCA_PLANOS_ID)
                WHERE
                eqp.CAPMEDICA_ID = $avaliacao";

                $result = mysql_query($sql);
                $equipamento = '';
                //echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
                while($row=mysql_fetch_array($result)){
                        $equipamento[$row[id]] = array("item"=>$row['item'],"und"=>$row['unidade'],"tipo"=>$row['tipo'],"qtd"=>$row['QTD'],"obs"=>$row['OBS'],"data"=>$row['DATA']);
                }
                $sql2= "select * from cobrancaplanos where tipo=1";

                $result2=mysql_query($sql2);

                echo "<tr bgcolor='grey'><td><b>Item</b></td><td><b>A&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td><td><b>Ultima Substitui&ccedil;&atilde;o</b></td></tr>";
                while($row=mysql_fetch_array($result2)){
                    if(array_key_exists($row['id'], $equipamento)){
                        $check = "checked='checked'";
                        $qtd = $equipamento[$row['id']]['qtd'];
                        $obs = $equipamento[$row['id']]['obs'];
                        $data = $equipamento[$row['id']]['data'];
                        $botao = "<button style='background-color:#ff0000; width: 80px;' class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='2' acao='1' tipo_solicitacao='{$tipo_solicitacao}' >Recolher</button>&nbsp;&nbsp;<button style='background-color:#FFA824; width: 80px;' class='equipamento_substituir' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='3' acao='1' tipo_solicitacao='{$tipo_solicitacao}'>Substituir</button>";
                    }else{
                        $check ='';
                        $qtd='';
                        $obs = '';
                        $data = '';
                        $botao = "<button style='background-color:#00ff00; width: 80px;' class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='1' acao='0' tipo_solicitacao='{$tipo_solicitacao}' >Solicitar</button>";
                    }
                //echo"<tr><td><input type='checkbox' $check class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}'></input>";
                    echo "<tr><td>{$row['item']}</td><td>$botao</td><td><input type='texto' readonly id=qtd"."{$row['id']}   class='qtd_equipamento' value='{$qtd}'  name='qtd_equipamento'  {$acesso} ></input></td><td><input type='texto' readonly id=data"."{$row['id']} class='data_equipamento' value='{$data}' name='data_equipamento' {$acesso}></input></td></tr>";
                }

            }else{
                $sql = "SELECT
                C.id,
                C.item,
                C.unidade,
                C.tipo,
                eqp.QTD,
                eqp.OBS,
                eqp.ID as idativo,
                eqp.PRESCRICAO_AVALIACAO,
                eqp.PRESCRICAO_ID
                FROM
                equipamentosativos as eqp INNER JOIN
                cobrancaplanos as C ON (C.id = eqp.COBRANCA_PLANOS_ID)

                WHERE
                eqp.CAPMEDICA_ID = {$x} and DATA_FIM = '0000-00-00 00:00:00' ";


                $result = mysql_query($sql);
                $equipamento = '';
                //echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
                while($row=mysql_fetch_array($result)){
                $equipamento[$row[id]] = array("item"=>$row['item'],"und"=>$row['unidade'],"tipo"=>$row['tipo'],"qtd"=>$row['QTD'],"obs"=>$row['OBS'],"idativo"=>$row['idativo'],"data"=>$row['DATA']);
                }
                $sql2= "select * from cobrancaplanos where tipo=1";

                $result2=mysql_query($sql2);

                echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>A&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td><td><b>Ultima Substitui&ccedil;&atilde;o</b></td></tr>";
                while($row=mysql_fetch_array($result2)){
                    if(array_key_exists($row['id'], $equipamento)){
                        //$check = "checked='checked'";
                        $qtd = $equipamento[$row['id']]['qtd'];
                        $obs = $equipamento[$row['id']]['obs'];
                        $idativo = $equipamento[$row['id']]['idativo'];
                        $data = $equipamento[$row['id']]['data'];
                        $botao = "<button style='background-color:#ff0000; width: 80px;' class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='2' acao='1' tipo_solicitacao='{$tipo_solicitacao}'>Recolher</button>&nbsp;&nbsp;<button style='background-color:#FFA824; width: 80px;' class='equipamento_substituir' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='3' acao='1' tipo_solicitacao='{$tipo_solicitacao}'>Substituir</button>";
                    }else{
                        //$check ='';
                        $qtd='';
                        $obs = '';
                        $idativo='';
                        $data = '';
                        $botao = "<button style='background-color:#00ff00; width: 80px;' class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='1' acao='0' tipo_solicitacao='{$tipo_solicitacao}'>Solicitar</button>";
                    }
                    //echo"<tr><td><input type='checkbox' $check class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}'></input>";
                    echo "<tr><td>{$row['item']}</td><td>$botao</td><td><input type='texto' readonly id=qtd"."{$row['id']}   class='qtd_equipamento' value='{$qtd}'  name='qtd_equipamento'  {$acesso} ></input></td><td><input type='texto' readonly id=data"."{$row['id']} class='data_equipamento' value='{$data}' name='data_equipamento' {$acesso}></input></td></tr>";
                }

            }
            }else{
                $sql = "SELECT
                C.id,
                C.item,
                C.unidade,
                C.tipo,
                eqp.QTD,
                eqp.OBSERVACAO

                FROM
                equipamentos_capmedica as eqp INNER JOIN
                cobrancaplanos as C ON (C.id = eqp.COBRANCAPLANOS_ID)

                WHERE
                eqp.CAPMEDICA_ID = ".$x;


                $result = mysql_query($sql);
                $equipamento = '';

                while($row=mysql_fetch_array($result)){
                    $equipamento[$row[id]] = array("item"=>$row['item'],"und"=>$row['unidade'],"tipo"=>$row['tipo'],"qtd"=>$row['QTD'],"obs"=>$row['OBSERVACAO'],"data"=>$row['DATA']);
                }
                $sql2= "select * from cobrancaplanos where tipo=1";

                $result2=mysql_query($sql2);

                echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>A&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td><td><b>Ultima Substitui&ccedil;&atilde;o</b></td></tr>";
                while($row=mysql_fetch_array($result2)){
                    if(array_key_exists($row['id'], $equipamento)){
                        $check = "checked='checked'";
                        $qtd = $equipamento[$row['id']]['qtd'];
                        $obs = $equipamento[$row['id']]['obs'];
                        $data = $equipamento[$row['id']]['data'];
                        $botao = "<button style='background-color:#ff0000; width: 80px;' class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='2' acao='1' tipo_solicitacao='{$tipo_solicitacao}'>Recolher</button>&nbsp;&nbsp;<button style='background-color:#FFA824; width: 80px;' class='equipamento_substituir' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='3' acao='1' tipo_solicitacao='{$tipo_solicitacao}'>Substituir</button>";
                    }else{
                        $check ='';
                        $qtd='';
                        $obs = '';
                        $data = '';
                        $botao = "<button style='background-color:#00ff00; width: 80px;' class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}' value='1' acao='0' tipo_solicitacao='{$tipo_solicitacao}'>Solicitar</button>";
                    }
                    //echo"<tr><td><input type='checkbox' $check class='equipamento' cod_equipamento='{$row['id']}' idativo='{$idativo}' idcap='{$x}'></input>";
                    echo "<tr><td>{$row['item']}</td><td>$botao</td><td><input type='texto' readonly id=qtd"."{$row['id']}   class='qtd_equipamento' value='{$qtd}'  name='qtd_equipamento'  {$acesso} ></input></td><td><input type='texto' readonly id=data"."{$row['id']} class='data_equipamento' value='{$data}' name='data_equipamento' {$acesso}></input></td></tr>";
                    }
            }

        echo"</table></div>";

        $botoes = "<img class='add-kit' tipo='kits' item='avulsa' src='../utils/kit_48x48.png' title='Adicionar kit' border='0'>&nbsp;&nbsp;";
        $botoes .= "<img class='add-med' tipo='med' item='avulsa' src='../utils/medicamento_48x48.png' title='Adicionar medicamento' border='0'>&nbsp;&nbsp;";
        $botoes .= "<img class='add-mat' tipo='mat' item='avulsa' src='../utils/material_48x48.png' title='Adicionar material' border='0'>&nbsp;&nbsp;";
        $botoes .= "<img class='add-die' tipo='die' item='avulsa' src='../utils/formula_48x48.png' title='Adicionar Dieta' border='0'>&nbsp;&nbsp;";
        echo "<center>$botoes</center>";
        $kits = "<table class='mytable' width=100% id='kits-avulsa'><thead><tr><th>KITS</th></tr></thead></table>";
        $med = "<table class='mytable' width=100% id='med-avulsa'><thead><tr><th>Medicamentos</th></tr></thead></table>";
        $mat = "<table class='mytable' width=100% id='mat-avulsa'><thead><tr><th>Materiais</th></tr></thead></table>";
        $dieta = "<table class='mytable' width=100% id='die-avulsa'><thead><tr><th>Dietas</th></tr></thead></table>";
        echo "$kits<br/>$med<br/>$mat<br/>$dieta<br/>";
        echo "<button id='salvar-avulsa' data='{$data_avulsa}' hora='{$hora_avulsa}'>Salvar</button></div>";
  	}
  }
  
  private function periodo(){
  	echo "<br/><fieldset style='width:375px;text-align:center;'><legend><b>Per&iacute;odo</b></legend>";
  	echo "DE"."<input type='text' name='inicio' value='{$this->inicio}' maxlength='10' size='10' class='OBG data periodo' />".
  	" AT&Eacute; <input type='text' name='fim' value='{$this->fim}' maxlength='10' size='10' class='OBG data' /></fieldset>";
  } 
  
  private function buscar_solicitacoes($paciente,$inicio,$fim){
  
  	//echo "<center><h1>Buscar Prescri&ccedil;&otilde;es</h1></center>";
  	echo "<fieldset style='margin:0 auto;'>";
  	echo "<div id='div-busca' >";  
  	
  	echo alerta();
  	
  	echo "<div style='float:right;width:100%;'>";
  	echo "<center><h2 style='background-color:#8B0101;color:#ffffff;height:25px;vertical-align: middle;'><label style='vertical-align: middle;'>BUSCAR SOLICITA&Ccedil;&Otilde;ES</label></h2></center>";
  	echo $this->pacientes_presc();
  	echo $this->periodo();
  
  	echo "<p><button id='buscar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Buscar</span></button>";
  
  	
  	echo "</div>";
  
  	echo "</div>";
  	echo "</fieldset>";
  	echo "<div id='div-resultado-busca'></div>";
  }
  
  private function check(){
      $html = "<table width='100%' border='0'>";
      $html .= "<thead>";
      $html .= "<tr>";
      $html .= "<th>Produto</th>";
      $html .= "<th></th>";
      $html .= "<th></th>";
      $html .= "</tr>";
      $html .= "</thead>";
      $html .= "</table>";
  }  
  
  //! Exibe os detalhes de um prescrição finalizada.
  private function detalhes($p){      
      $result_solicitante = mysql_query("SELECT u.nome as enfermeiro,DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA FROM solicitacoes as s INNER JOIN usuarios as u on (s.enfermeiro = u.idUsuarios) WHERE idPrescricao = {$p}");
      while ($row = mysql_fetch_array($result_solicitante)){
          $solicitante = $row['enfermeiro'];
          $data = $row['DATA'];
      }
  	echo "<center><h1>Detalhes das Solicita&ccedil&otilde;es</h1></center>";
        echo "<p><b>Solicitante:</b> {$solicitante}";
        echo "<p><b>Data da Solicita&ccedil;&atilde;o:</b> {$data}";
  	echo "<table width=100% class='mytable'><thead><tr>";
  	echo "<th><b>Itens prescritos</b></th>";
  	echo "</tr></thead>";
  	/*$sql = "SELECT v.via as nvia, f.frequencia as nfrequencia,i.*,  i.inicio,
  	i.fim, DATE_FORMAT(i.aprazamento,'%H:%i') as apraz, u.unidade as um
  	FROM itens_prescricao as i LEFT OUTER JOIN frequencia as f ON (i.frequencia = f.id)
  	LEFT OUTER JOIN viaadm as v ON (i.via = v.id)
  	LEFT OUTER JOIN umedidas as u ON (i.umdose = u.id)
  	WHERE idPrescricao = '$p'
  	ORDER BY tipo,inicio,aprazamento,nome,apresentacao ASC";*/

$sql = "SELECT 
      s.data_auditado,
      s.enfermeiro,
      s.tipo,
      s.qtd,
      UPPER(c.nome),
      s.autorizado,
      s.enviado,
      s.NUMERO_TISS,
      s.CATALOGO_ID,
      s.idSolicitacoes AS sol,
      s.status,
      DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
      f.formula AS produto 
FROM 
      solicitacoes AS s,
      formulas AS f,
      clientes AS c 
WHERE 
      s.idPrescricao = {$p} AND
      c.idclientes = s.paciente AND
      s.tipo = '12' AND
      
      f.id = s.CATALOGO_ID
UNION 
SELECT 
       s.data_auditado,
       s.enfermeiro,
       s.tipo,
       s.qtd,
       UPPER(c.nome),
       s.autorizado,
       s.enviado,
       s.NUMERO_TISS,
       s.CATALOGO_ID,
       s.idSolicitacoes AS sol,
       s.status,
       DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
       CONCAT(b.principio,' - ',b.apresentacao) AS produto
FROM 
        solicitacoes AS s,
        catalogo AS b,
        clientes AS c 
WHERE 
        s.idPrescricao = {$p} AND
        c.idclientes = s.paciente AND
        s.tipo = '1' AND
        b.ID = s.CATALOGO_ID 
UNION
SELECT
        s.data_auditado,
        s.enfermeiro,
        s.tipo,
        s.qtd,
        UPPER(c.nome),
        s.autorizado,
        s.enviado,
        s.NUMERO_TISS,
        s.CATALOGO_ID,
        s.idSolicitacoes AS sol,
        s.status,
        DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
        CONCAT(b.principio,' - ',b.apresentacao) AS produto
FROM
	solicitacoes AS s inner join
	catalogo AS b ON (b.ID = s.CATALOGO_ID ) inner join
	clientes AS c ON (s.paciente = c.idClientes)
WHERE
	s.idPrescricao = {$p} AND
	s.tipo = '3'
UNION
SELECT 
        s.data_auditado,
	s.enfermeiro,
        s.tipo, 
        s.qtd,
        UPPER(c.nome), 
        s.autorizado, 
        s.enviado, 
        s.NUMERO_TISS,
        s.CATALOGO_ID,		
        s.idSolicitacoes AS sol, 
        s.status, 
        DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA, 
        CONCAT(b.principio,' - ',b.apresentacao) AS produto 
FROM 
	solicitacoes AS s, 
	catalogo AS b, 
	clientes AS c 
WHERE 
	s.idPrescricao = {$p} AND 
	c.idclientes = s.paciente AND 
	s.tipo = '0' AND	
	b.ID = s.CATALOGO_ID 
ORDER BY 
	tipo, 
	produto, 
	autorizado, 
	enviado";
  	
  	
  	
  	$result = mysql_query($sql);
  	$n=0;
  	while($row = mysql_fetch_array($result)){
  	if($n++%2==0)
  		$cor = '#E9F4F8';
  		else
  		$cor = '#FFFFFF';
  
  		$tipo = $row['tipo'];
  		$i = implode("/",array_reverse(explode("-",$row['inicio'])));
  		$f = implode("/",array_reverse(explode("-",$row['fim'])));
  		$aprazamento = "";
  		if($row['tipo'] != "0") $aprazamento = $row['apraz'];
  		$linha = "";
  		if($row['tipo'] != "-1"){
  		$aba = $this->abas[$row['tipo']];
  		$dose = "";
		if($row['data_auditado'] != '0000-00-00 00:00:00'){
		$auditado=" <b>Liberados pela auditoria:</b> {$row['autorizado']}";
		}else{
		$auditado=" <b> N&atilde;o foi auditado</b>";
		}
  		
  		$linha = "<b>Item:</b> {$row['produto']} <b>QTD:</b> {$row['qtd']} {$auditado}";
  	}
  	
  	echo "<tr bgcolor={$cor} >";
  	echo "<td>$linha</td>";
  	echo "</tr>";
  }
  echo "</table>";
  echo "<span style='float:left;'><form action='imprimir.php' target='_blank' method='post'><input type='hidden' value='{$p}' name='prescricao'>
  <button class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Imprimir</span></button>
  </form></span>";
  echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
  
  }  

  public function view($v,$p){
  	switch ($v) {
  		case "menu":
  			$this->menu();
  			break;
  
  		case "solicitacoes":
  			$l = NULL;
  			$antiga = NULL;
  			$data = date("d/m/Y");
  			$pantiga = -1;
  			$paciente = -1;
  			$carater='';
  			if(isset($_REQUEST['label'])) $l = $_REQUEST['label'];
  			if(isset($_REQUEST["data"])) $data = $_REQUEST["data"];
  			if(isset($_REQUEST["antigas"])) $antiga = $_REQUEST["antigas"];
  			if(isset($_REQUEST["paciente"])) $paciente = $_REQUEST["paciente"];
  			if(isset($_REQUEST["carater"])) $carater = $_REQUEST["carater"];
  			$this->prescricoes($l,$data,$antiga,$paciente,$carater);
  			break;
  		case "kits":
  			$this->kits();
  			break;
  		case "buscas":
  			$this->buscas();
  			break;
  		case "detalhes":
  			$this->detalhes($_REQUEST['p']);
  			break;
  		case "avulsa":
  			$emergencia = 0;
  			if(isset($_POST['emergencia'])){
                            $emergencia = $_POST['emergencia'];
                        }
  			$paciente = NULL;
  			if(isset($_POST['pacientes'])){
                            $paciente = $_POST['pacientes'];
                        }
  			$npaciente = NULL;
  			//jeferson
  			if(isset($_POST['data-avulsa'])){
                            $data = $_POST['data-avulsa'];
                        }
  			if(isset($_POST['hora-avulsa'])){
                            $hora = $_POST['hora-avulsa'];
                        }
  			//jeferson
  			if(isset($_POST['npaciente'])){
                            $npaciente = ucwords(strtolower($_POST['npaciente']));
                        }
  			$this->avulsa($paciente,$npaciente,$emergencia,$data,$hora);
  			break;
  		case "buscar":
  			$paciente = NULL;
  			if(isset($_POST['pacientes'])) $paciente = $_POST['pacientes'];
  			$inicio = NULL;
  			if(isset($_POST['pacientes'])) $inicio = $_POST['inicio'];
  			$fim = NULL;
  			if(isset($_POST['pacientes'])) $fim = $_POST['fim'];
  
  			$this->buscar_solicitacoes($paciente,$inicio,$fim);
  			break;  			
  			// Eriton
  			case "buscar_avulsa":
  				$paciente = NULL;
  				if(isset($_POST['pacientes'])) $paciente = $_POST['pacientes'];
  				$inicio = NULL;
  				if(isset($_POST['pacientes'])) $inicio = $_POST['inicio'];
  				$fim = NULL;
  				if(isset($_POST['pacientes'])) $fim = $_POST['fim'];
  			
  				$this->buscar_avulsa($paciente,$inicio,$fim);
  				break;
  				//Fim Eriton
  		case "checklist":                    
  			$checklist = new Checklist;
                        $checklist->chamar_checklist();
                        //$this->checklist($paciente,$npaciente,$emergencia);                    
  			break;
                    
                case "estoque":                    
                    $estoque = new Checklist;
                    $estoque->mostrar_estoque();
                    break;
  
  		case "listar":
  			//echo "<button id='tt'>dcgfgh</button>";
  			//Paciente::listar();
  			//$p = new Paciente();
  			$p->listar();
  			break;
  
  		case "listcapmed":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->listar_capitacao_medica($_GET['id']);
  			}
  			break;
  
  		case "listfichaevolucao":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->listar_evolucao_medica($_GET['id']);
  			}
  			break;
  
  		case "novaavaliacaoenf":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->nova_ficha_avaliacao_enfermagem($_GET['id']);
  			}
  			break;

  		case "novaevolucaoenf":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->nova_ficha_evolucao_enfermagem($_GET['id']);
  			}
  			break;
  				
  			
  		case "listavaliacaenfermagem":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->listar_ficha_avaliacao_enfermagem($_GET['id']);
  			}
  			break;
  				
  		case "listevolucaoenfermagem":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->listar_ficha_evolucao_enfermagem($_GET['id']);
  			}
  			break;
  			
  		case "editaravaliacaoenfermagem":
  				if(isset($_GET['id'])){
  					$p = new Paciente();
  					$p->nova_editar_ficha_avaliacao_enfermagem($_GET['id'],$_GET['v']);
  				}
  				break;
  				
  		case "editarevolucaoenfermagem":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->nova_editar_ficha_evolucao_enfermagem($_GET['id'],$_GET['v']);
  			}
  			break;
  				
  		/*case "listfichaevolucao":
  
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->listar_evolucao_medica($_GET['id']);
  			}
  			break;*/
  		case "show":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->form($_GET['id'],'readonly=readonly');
  			}
  			break;
  
  
  			if(isset($_GET['id'])){
  					
  				$p = new Paciente();
  				$p->detalhes_ficha_evolucao_medica($_GET['id']);
  			}
  			break;
  		case "capmed":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->detalhes_capitacao_medica($_GET['id']);
  				}
  		case "detalhescore":
  			if(isset($_GET['id'])){
  				$p = new Paciente();
  				$p->detalhe_score($_GET['id']);
  					}  				
  					
  		case "detalhesprescricao":
  			if(isset($_GET['id']) && isset($_GET['idpac'])){
  				$p = new Paciente();
  				$p->detalhe_prescricao($_GET['id'],$_GET['idpac']);
  						}
  						break;
  		//Eriton 20-03-2013				
  		case "frequencia_uso":
                if (isset($_POST['id'])) {
                    $p = new Paciente();
                    $p->frequencia_uso($_POST['id']);
                }
                break;
            case "relatorio":
                if (isset($_GET['opcao'])) {
                    switch ($_GET['opcao']) {
                        case "1":
                            if (isset($_GET['idp']) && !isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_alta($_GET['idp']);
                            } elseif (isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_alta($_GET['id'], $_GET['idr'], $_GET['editar']);
                            } else {
                                $r = new Relatorios();
                                $r->listar_alta_enfermagem($_GET['id']);
                            }
                            break;
                        case "2":
                            if (isset($_GET['idp']) && !isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_aditivo($_GET['idp']);
                            } elseif (isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_aditivo($_GET['id'], $_GET['idr'], $_GET['editar']);
                            } else {
                                $r = new Relatorios();
                                $r->listar_aditiva_enfermagem($_GET['id']);
                            }
                            break;
                        case "3":
                            if (isset($_GET['idp']) && !isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_prorrogacao($_GET['idp']);
                            } elseif (isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_prorrogacao($_GET['id'], $_GET['idr'], $_GET['editar']);
                            } else {
                                $r = new Relatorios();
                                $r->listar_prorrogacao_enfermagem($_GET['id']);
                            }
                            break;
                        case "4":
                            if (isset($_GET['idp']) && !isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_visita($_GET['idp']);
                            } elseif (isset($_GET['idr'])) {
                                $r = new Relatorios();
                                $r->relatorio_visita($_GET['id'], $_GET['idr'], $_GET['editar']);
                            } else {
                                $r = new Relatorios();
                                $r->listar_visita_semanal($_GET['id']);
                            }
                            break;
                    }
                }
                /* $r =new Relatorios();
                  $r->relatorio_alta($_GET['id']); */
                break;
            case "visualizar_rel_prorrogacao":
                $r = new Relatorios();
                $r->visualizar_rel_prorrogacao($_GET['id'], $_GET['idr']);
                break;
            case "visualizar_rel_alta":
                $r = new Relatorios();
                $r->visualizar_rel_alta($_GET['id'], $_GET['idr']);
                break;
            case "visualizar_rel_aditivo":
                $r = new Relatorios();
                $r->visualizar_rel_aditivo($_GET['id'], $_GET['idr']);
                break;
            case "visualizar_rel_visita":
                $r = new Relatorios();
                $r->visualizar_visita_semanal($_GET['id'], $_GET['idr']);
                break;
		//Fim Eriton 20-03-2013  
  
  	}
  
  }
  
}
