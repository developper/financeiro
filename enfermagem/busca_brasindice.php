<?php
require_once('../db/config.php');

$like = $_GET['term'];
$plano = $_GET['plano'];
$compPlanoServ = !empty($plano) ? " AND V.idPlano = '{$plano}'" : "";
$condlike = "1";
$flag = true;
$strings = explode(" ",$like);
if(isset($_REQUEST['tipo'])) $_GET['tipo'] = $_REQUEST['tipo'];
if(isset($_REQUEST['tipo_extra'])) $cond_extra = "OR tipo = 1";
$comparar = "concat(principio,' ',apresentacao)";
if(isset($_GET['tipo']) && ($_GET['tipo'] == '2' || $_GET['tipo'] == 'kits')) $comparar = "k.nome";
if(isset($_GET['tipo']) && ($_GET['tipo'] == 'serv')) $comparar = "C.cuidado";
if(isset($_GET['tipo']) && ($_GET['tipo'] == 'eqp' || $_GET['tipo'] == '5')) $comparar = "C.item";
if(isset($_GET['tipo']) && ($_GET['tipo'] == 'gas')) $comparar = "C.item";
foreach($strings as $str){
	if($str !=  "") $condlike .= " AND {$comparar} LIKE '%$str%' ";
}
$tipo = "1";
$t =  "-1";
$cond2="AND ATIVO = 'A'";
if(isset($_GET['tipo'])){
	if($_GET['tipo'] ==  '0' || $_GET['tipo'] == 'med')
		$t = "0";
	else if($_GET['tipo'] ==  '1' || $_GET['tipo'] == 'mat')
			$t = "1";
	else if($_GET['tipo'] ==  '2' || $_GET['tipo'] == 'kits')
			$t = "2";
	else if($_GET['tipo'] ==  'serv' )
		$t = "3";
	else if($_GET['tipo'] ==  'eqp' || $_GET['tipo'] == '5' )
		$t = "5";
	else if($_GET['tipo'] ==  'gas' )
		$t = "6";
	else if($_GET['tipo'] ==  '3' || $_GET['tipo'] == 'die')
		$t = "7";
}
$itens = array();
$result = "";
$sql = "";
if($t == "0" || $t == "1" || $t == "-1" || $t == "7"){
	if($t ==  "7") $t = "3";
		$tipo = " tipo = {$t}";
	
	if($t ==  "-1") $tipo = " 1 ";
	$t = 90;
		$sql = "SELECT concat(principio,' ',apresentacao) as name,apresentacao, NUMERO_TISS as cod,ID as catalogo_id ,valor as custo,tipo, if(tipo = 1, categoria_material_id, NULL) as categoria, COALESCE(VALOR_VENDA,0) as valorVenda  FROM catalogo WHERE ({$tipo} {$cond_extra}) AND {$condlike} {$cond2} ORDER BY principio, apresentacao";
	} else if($t == "2") {
		$sql = "SELECT k.nome as name, 'KIT' as apresentacao, k.idKit as cod, k.COD_REF as cod_ref ,2 as tipo , NULL as categoria FROM kitsmaterial as k WHERE {$condlike} ORDER BY k.nome";
	}else if($t == "3") {
		$sql = "SELECT C.cuidado as name,'SERVI&Ccedil;OS' as apresentacao ,C.id as cod,C.id as catalogo_id, 4 AS tipo, V.CUSTO_COBRANCA_PLANO as custo, NULL as categoria, coalesce(V.valor,'0.00') as valorVenda FROM cuidadosespeciais as C Left join valorescobranca as V ON (V.idCobranca = C.COBRANCAPLANO_ID) WHERE {$condlike} {$compPlanoServ} GROUP BY catalogo_id ORDER BY C.cuidado";
	}
	else if($t == "5") {
		$sql = "SELECT C.item as name,'EQUIPAMENTOS' as apresentacao ,C.id as cod, C.id as catalogo_id, CUSTO as custo, 5 as tipo, NULL as categoria, '' as valorVenda  FROM cobrancaplanos as C WHERE {$condlike} and C.tipo =1 ORDER BY C.item";
	}
	else if($t == "6") {
		$sql = "SELECT C.item as name,'GASOTERAPIA' as apresentacao ,C.id as cod,C.id as catalogo_id, CUSTO as custo, NULL as categoria, 0 as valorVenda FROM cobrancaplanos as C WHERE {$condlike} and C.id in(28,29,30) ORDER BY C.item";
	}
if(isset($_GET['somenteGenerico']) && ($_GET['tipo'] ==  '0' || $_GET['tipo'] == 'med')){
			$somenteGenerico = $_GET['somenteGenerico'];
	$condGenerico = '';
	if($somenteGenerico == 'G'){
		$condGenerico = " AND ( catalogo.GENERICO = 'S' OR catalogo.possui_generico = 'N' ) ";
	}

$sql = "SELECT
concat('(',principio_ativo.principio,') ',catalogo.principio,' ',catalogo.apresentacao) AS `name`,
catalogo.apresentacao as apresentacao,
catalogo.NUMERO_TISS AS cod,
catalogo.valor AS custo,
catalogo.tipo as tipo,
catalogo.GENERICO,
catalogo.ID as catalogo_id,
if(tipo = 1, categoria_material_id, NULL) as categoria, COALESCE(VALOR_VENDA,0) as valorVenda 
FROM
catalogo
LEFT JOIN catalogo_principio_ativo ON catalogo.ID = catalogo_principio_ativo.catalogo_id
LEFT JOIN principio_ativo ON principio_ativo.id = catalogo_principio_ativo.principio_ativo_id
WHERE
catalogo.tipo = 0
AND
concat('(',principio_ativo.principio,') ',catalogo.principio,' ',catalogo.apresentacao) like '%$str%'
AND ATIVO = 'A'
$condGenerico
ORDER BY
catalogo.principio,
catalogo.apresentacao";

}
$result = mysql_query($sql);
//echo $sql;
while ($row = mysql_fetch_array($result)) {
	$textRed = 'N';
	$item['value'] = $row['name'];

	$item['id'] = $row['cod']; 
	$item['cod_ref']=$row['cod_ref'];
	$item['apresentacao']= $row['apresentacao'];
	$item['custo']=$row['custo'];
	$item['tipo']=$row['tipo'];
    $item['categoria']=$row['categoria'];
    $item['catalogo_id'] = $row['catalogo_id'];
    $item['valor'] = $row['valorVenda'];
	if ($somenteGenerico == 'G' && $row['GENERICO'] != 'S')
		$textRed = 'S';
	$item['textRed'] = $textRed;
	
	array_push($itens, $item); 
}
//var_dump($itens);
echo json_encode($itens);
