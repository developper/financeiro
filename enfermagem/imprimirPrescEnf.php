<?php
$id = session_id();
if(empty($id))
  session_start();
include_once('../validar.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../db/config.php');
include_once('../vendor/autoload.php');

use \App\Controllers\Administracao as Administracao,
    \App\Models\Administracao\Filial;

class Itens{
  private $tipo = "";
  private $nome = "";
  private $apresentacao = "";
  private $via = "";
  private $aprazamento = "";
  private $obs = "";
  private $dose = "";
  private $freq = "";

  function Itens($grupo,$obs,$apraz){
    $this->obs =$obs;
    $this->grupo =$grupo;
    $this->aprazamento = $apraz;
  }

  public function getAprazamento($dia){

    if(array_key_exists($dia,$this->aprazamento))
      return $this->aprazamento[$dia];
    return "&nbsp;";
  }

  public function getLabel(){
    return "<b>{$this->grupo}</b> {$this->obs}";
  }
}

function aprazamento($i,$f,$inc,$hora,$ip,$fp){

  $apraz = array();
  $inicio = new DateTime($i);
  $fim = new DateTime($f);
  $atual = new DateTime($ip);
  $fim_periodo = new DateTime($fp);
  $a = $hora;

  while($atual <= $fim_periodo){
    if($atual >= $inicio && $atual <= $fim){
      $apraz[$atual->format('Y-m-d')] = $a;

    }
    else {
      $apraz[$atual->format('Y-m-d')] = "&nbsp;";
    }
    $atual->modify("+1 day");
  }

  return $apraz;
}

$idPrescEnf = $_GET['prescricao'];
$empresa = $_REQUEST['empresa'];
$responseEmpresa = Filial::getEmpresaById($empresa);

$sql = "    SELECT
            usuarios.nome as enf,
            usuarios.conselho_regional,
            usuarios.tipo as tipo_usuario,
            DATE_FORMAT(prescricao_enf.data, '%d/%m/%Y - %T')  as sdata,
            prescricao_enf.inicio,
            prescricao_enf.fim,
            c.nome as paciente,
            c.idClientes,
             c.cuidador,
            c.relcuid as relacao,
            DATE_FORMAT(prescricao_enf.inicio, '%d/%m/%Y') as strinicio,
            DATE_FORMAT(prescricao_enf.fim, '%d/%m/%Y') as strfim,
            usuarios.ASSINATURA

            FROM
            prescricao_enf
            INNER JOIN usuarios ON prescricao_enf.user_id = usuarios.idUsuarios
            INNER JOIN clientes as c on prescricao_enf.cliente_id = c.idClientes
            where
            prescricao_enf.id ={$idPrescEnf}
            ";
$result = mysql_query($sql);


$row = mysql_fetch_array($result);

$ip = $row['inicio'];
$fp = $row['fim'];
$logo = Administracao::buscarLogoPeloIdPaciente($row['idClientes']);

$img = "<img src='../utils/logos/{$responseEmpresa['logo']}' width='20%' style='align:center' />";
$hoje = date("d/m/Y");
$compTipo = $row['conselho_regional'];

$header = "<table border='1' width='100%' >
              <thead>
                <tr>
                      <th width='40%' rowspan='3' colspan='3' >
                              {$img}
                              <br/>
                              {$responseEmpresa['nome']}
                     </th>
                     <th width=20% colspan='5'>
                                 Prescrição {$row['strinicio']} a {$row['strfim']} - ".ucwords(strtolower($row['paciente']))."
                     </th>
                      <th rowspan='3' colspan='2' >
                                                    " . (($row['ASSINATURA'] != '') ? "<img src='{$row['ASSINATURA']}' width='17%' style='align:center' />" : "") . "

                      </th>
                 </tr>
              </tr>
              <tr>
                  <td colspan='5'>
                         <b>Cuidador: </b>
                         ".utf8_encode(ucwords(strtolower($row['cuidador'])))." (".utf8_encode($row['relacao']).")
                  </td>
              </tr>";
$tipo = "Enfermeiro";
$header .= "   <tr>
                    <td colspan='5' >
                         <b>Profissional: </b>"
                        .utf8_encode(ucwords(strtolower($row['enf']))) . $compTipo . "
                         ({$tipo})
                    </td>
                </tr>";

$sql = "    SELECT
            DATE_FORMAT(prescricao_enf.inicio, '%d/%m/%Y') as inicioPt,
            DATE_FORMAT(prescricao_enf.fim, '%d/%m/%Y') as fimPt,
            prescricao_enf.cliente_id,
            prescricao_enf.id,
            usuarios.nome,
            prescricao_enf_itens_prescritos.obs,
            prescricao_enf_itens_prescritos.aprazamento,
            prescricao_enf_grupo.id,
            prescricao_enf_grupo.grupo,
            prescricao_enf_itens_prescritos.fim,
            prescricao_enf_itens_prescritos.inicio
            FROM
            prescricao_enf
            INNER JOIN usuarios ON prescricao_enf.user_id = usuarios.idUsuarios
            INNER JOIN prescricao_enf_itens_prescritos ON prescricao_enf.id = prescricao_enf_itens_prescritos.prescricao_enf_id
            inner join prescricao_enf_cuidado on prescricao_enf_itens_prescritos.presc_enf_cuidados_id= prescricao_enf_cuidado.id
            INNER JOIN prescricao_enf_grupo ON prescricao_enf_cuidado.presc_enf_grupo_id = prescricao_enf_grupo.id
            where
            prescricao_enf.id ={$idPrescEnf}
            ORDER BY prescricao_enf_grupo.id ASC";

$result = mysql_query($sql);
$flag = true;

$datas_periodo_prescrito = array();
$itens_periodo_prescrito = array();
$inicio_periodo_prescrito = new DateTime($ip);
$fim_periodo_prescrito = new DateTime($fp);


  while($inicio_periodo_prescrito <= $fim_periodo_prescrito){
    $tmp = new DateTime();
    $tmp = clone $inicio_periodo_prescrito;
    $datas_periodo_prescrito[] = $tmp;
    $inicio_periodo_prescrito->modify("+1 day");
  }


$suspensoes = array();
while($row = mysql_fetch_array($result)){

    $aprazamento = aprazamento($row['inicio'],$row['fim'],$row['nfrequencia'],$row['aprazamento'],$ip,$fp);

    $itens_periodo_prescrito[] = new Itens($row['grupo'],$row['obs'],$aprazamento);
}
$tabela = "<tr>
                <th colspan='3' >
                     Prescrição Semanal
                </th>
                <th colspan='7'>
                Aprazamento
                </th>
           </tr>";
//print_r($suspensoes);
$mes_atual = substr($ip,5,2);
$paginas = array();

$inicio_periodo_prescrito = new DateTime($ip);
$aviso_suspensoes = true;

while($inicio_periodo_prescrito <= $fim_periodo_prescrito){
  $tabela = "<tr>
                 <th colspan='3' >
                    Prescrição Semanal
                 </th>
                 <th colspan='7'>
                   Aprazamento
                 </th>
            </tr>";
  if($aviso_suspensoes){
    foreach($suspensoes as $susp){
      $tabela .= $susp;
    }
    $aviso_suspensoes = false;
  }
  $date = clone $inicio_periodo_prescrito;
  $data_inicial_tabela = clone $inicio_periodo_prescrito;
  $dias = "";
  $mes_atual = substr($inicio_periodo_prescrito->format('Y-m-d'),5,2);

  for($d = 0; $d < 7; $d++){
      $dia_coluna = "&nbsp;";
      $dia = $date->format("Y-m-d");

      if(($mes_atual == substr($dia,5,2)) && ($date <= $fim_periodo_prescrito)){
        $dia_coluna = $date->format("d/m/Y");
        $inicio_periodo_prescrito->modify("+1 day");
      }
      if($tip != "-1"){
        $dias .= "<th width=10%>{$dia_coluna}</th>";
        $date->modify("+1 day");
      }
  }


    $tabela .= "<tr>
                    <th>N&ordm;</th>
                   <th>Descrição</th>
                   <th>Uso/Observações</th>
                   $dias
              </tr>
              </thead>";
    $i = 1;

    foreach($itens_periodo_prescrito as $item){
      $date = clone $data_inicial_tabela;
      $aprazamento = "&nbsp;";
      for($d = 0; $d < 7; $d++){
        $dia = $date->format("Y-m-d");

        if($mes_atual == $date->format("m")){
          $aprazamento .= "<td align='center' width=10% >".$item->getAprazamento($dia).'</td>';
        } else{
          $aprazamento .= "<td align='center' width=10% >&nbsp;</td>";
        }
        $date->modify("+1 day");
      }
      $cor = "#FFFFFF";
      if($i%2) $cor = "#DCDCDC";
      $label = $item->getLabel();
      $tabela .= "<tr bgcolor='{$cor}' ><td>{$i}</td><td colspan='2' >{$label} </td>";

      $tabela .= $aprazamento;
      $tabela .= "</tr>";
      $i++;
    }

  $paginas[] = $header.$tabela."</table>";
}

$mpdf=new mPDF('pt','A4-L',9);
$mpdf->SetHeader('página {PAGENO} de {nbpg}');
$ano = date("Y");
$mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']}"." &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));
$mpdf->WriteHTML("<html><body>");
$flag = false;
foreach($paginas as $pag){
  if($flag) $mpdf->WriteHTML("<formfeed>");
  $mpdf->WriteHTML($pag);
  $flag = true;
}
$mpdf->WriteHTML("</body></html>");
$mpdf->Output('prescricao.pdf','I');
exit;
