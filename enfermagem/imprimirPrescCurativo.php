<?php
$id = session_id();
if(empty($id))
    session_start();
include_once('../validar.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../db/config.php');
include_once('../vendor/autoload.php');

use \App\Controllers\Administracao,
    \App\Controllers\Prescricoes,
    \App\Models\Administracao\Filial;

class Itens{
    private $tipo = "";
    private $nome = "";
    private $apresentacao = "";
    private $via = "";
    private $aprazamento = "";
    private $obs = "";
    private $dose = "";
    private $freq = "";
    private $qtd = "";

    function Itens($grupo,$obs,$apraz){
        $this->obs =$obs;
        $this->grupo =$grupo;
        $this->aprazamento = $apraz;
    }

    public function getAprazamento($dia){

        if(array_key_exists($dia,$this->aprazamento))
            return $this->aprazamento[$dia];
        return "&nbsp;";
    }

    public function getLabel(){
        return "<b>{$this->grupo}</b> {$this->obs}";
    }
}

function aprazamento($i,$f,$inc,$hora,$ip,$fp){

    $apraz = array();
    $inicio = new DateTime($i);
    $fim = new DateTime($f);
    $atual = new DateTime($ip);
    $fim_periodo = new DateTime($fp);
    $a = $hora;

    while($atual <= $fim_periodo){
        if($atual >= $inicio && $atual <= $fim){
            $apraz[$atual->format('Y-m-d')] = $a;

        }
        else {
            $apraz[$atual->format('Y-m-d')] = "&nbsp;";
        }
        $atual->modify("+1 day");
    }

    return $apraz;
}

$idPresc = $_REQUEST['prescricao'];
$empresa = $_REQUEST['empresa'];
$responseEmpresa = Filial::getEmpresaById($empresa);

$row = Prescricoes::imprimirPrescricao ($idPresc);
$inicio_cabecalho = \DateTime::createFromFormat('Y-m-d', $row['prescricao']['inicio'])->format('d/m/Y');
$fim_cabecalho = \DateTime::createFromFormat('Y-m-d', $row['prescricao']['fim'])->format('d/m/Y');

$ip =  $row['prescricao']['inicio'];
$fp =  $row['prescricao']['fim'];

$logo = Administracao::buscarLogoPeloIdPaciente($row['prescricao']['paciente']);
$img = "<img src='../utils/logos/{$responseEmpresa['logo']}' width='10%' style='align:center' />";
$hoje = date("d/m/Y");
$header = "<table border='1' width='100%' >
                <tr>
                      <th width='40%' rowspan='3' colspan='3' >
                              {$img}
                              <br/>
                              {$responseEmpresa['nome']}
                     </th>
                     <th width=20% colspan='5'>
                                 Prescrição " . ($row['prescricao']['carater'] == 8 ? 'Aditiva' : 'Periódica') . "
                                 {$inicio_cabecalho} a {$fim_cabecalho} - ".ucwords(strtolower($row['prescricao']['paciente_name']))."
                     </th>
                      <th rowspan='3' colspan='2' >
                          " . (($row['prescricao']['assinatura'] != '') ? "<img src='{$row['prescricao']['assinatura']}' width='20%' style='align:center' />" : "") . "
                      </th>
                 </tr>
              </tr>
              <tr>
                  <td colspan='5'>
                         <b>Cuidador: </b>
                         ".ucwords(strtolower($row['prescricao']['cuidador']))." (".$row['prescricao']['relacao'].")
                  </td>
              </tr>";
$tipo = "Enfermeiro";
$header .= "   <tr>
                    <td colspan='5' >
                         <b>Profissional: </b>"
    .ucwords($row['prescricao']['profissional'])."
                         ({$tipo})
                    </td>
                </tr>";

$flag = true;

$datas_periodo_prescrito = array();
$itens_periodo_prescrito = array();
$inicio_periodo_prescrito = new DateTime($ip);
$fim_periodo_prescrito = new DateTime($fp);

$handler = [];
$i = 0;

foreach($row['feridas'] as $ferida){
    foreach($row['cp_itens'][$ferida['id']] as $itens){
        $text = sprintf(
            "<u>Ferida: %s %s </u><br><br>Cobertura Primária: <br> %s  <br> <b>%s</b><br> Obs: %s",
            strip_tags($ferida['local']),
            $ferida['lado'],
            $itens['item_name'],
            $itens['frequencia_descricao'],
            stripslashes($itens['obs'])
        );
        $handler[$text]['aprazamento'] = $itens['aprazamento'];
        $handler[$text]['inicio'] = $itens['inicio'];
        $handler[$text]['fim'] = $itens['fim'];
    }

    foreach($row['cs_itens'][$ferida['id']] as $itens){
        $text = sprintf(
            "<u>Ferida: %s %s </u><br><br>Cobertura Secundária: <br> %s  <br> <b>%s</b><br> Obs: %s",
            strip_tags($ferida['local']),
            $ferida['lado'],
            $itens['item_name'],
            $itens['frequencia_descricao'],
            stripslashes($itens['obs'])
        );
        $handler[$text]['aprazamento'] = $itens['aprazamento'];
        $handler[$text]['inicio'] = $itens['inicio'];
        $handler[$text]['fim'] = $itens['fim'];
    }
    $i++;
}

$suspensoes = array();

foreach($handler as $text => $data) {
    $aprazamento = aprazamento ($data['inicio'], $data['fim'], '', $data['aprazamento'], $ip, $fp);
    $itens_periodo_prescrito[] = new Itens(
        $text,
        '',
        $aprazamento
    );
}
$mes_atual = substr($ip,5,2);
$paginas = array();

$inicio_periodo_prescrito = new DateTime($ip);
$aviso_suspensoes = true;

$tabela = "<tr>
                 <th colspan='10' >
                    Prescrição {$row['prescricao']['carater_nome']} de Curativo
                 </th>
            </tr>";
if($aviso_suspensoes){
    foreach($suspensoes as $susp){
        $tabela .= $susp;
    }
    $aviso_suspensoes = false;
}
$date = clone $inicio_periodo_prescrito;
$data_inicial_tabela = clone $inicio_periodo_prescrito;
$dias = "";
$mes_atual = substr($inicio_periodo_prescrito->format('Y-m-d'),5,2);
$dia_coluna = "&nbsp;";
$dia = $date->format("Y-m-d");

$tabela .= "<tr>
                   <th width='100%' colspan='10'>Descrição</th>
                   $dias
              </tr>
              </thead>";
$i = 1;

foreach($itens_periodo_prescrito as $item){
    $cor = "#FFFFFF";
    if($i%2) $cor = "#DCDCDC";
    $label = $item->getLabel();
    $tabela .= "<tr bgcolor='{$cor}' ><td width='100%' colspan='10' >{$label} </td>";
    $tabela .= "</tr>";
    $i++;
}

$paginas[] = $header.$tabela."</table>";

//print_r($paginas);

$mpdf=new mPDF('pt','A4',9);
$mpdf->SetHeader('página {PAGENO} de {nbpg}');
$ano = date("Y");
$mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']}"." &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));
$mpdf->WriteHTML("<html><body>");
$flag = false;

if($flag) $mpdf->WriteHTML("<formfeed>");
$mpdf->WriteHTML($paginas[0]);
$flag = true;

$mpdf->WriteHTML("</body></html>");
$mpdf->Output('prescricao.pdf','I');
exit;

?>