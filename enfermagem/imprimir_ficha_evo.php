<?php

$id = session_id();
if (empty($id))
    session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../vendor/autoload.php');

// ini_set('display_errors', 1);

use \App\Controllers\Administracao as Administracao;
use \App\Models\Administracao\Filial;

class Paciente_imprimir_ficha {

    public function cabecalho($id,$id_a, $empresa) {
        /*
          if(isset($id))
          $id = $id;
          else
          $id = $id;
         */

        $condp1 = "c.idClientes = '{$id}'";
        $sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		c.END_INTERNADO AS local_i,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS nomesexo,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresan,
		c.`nascimento`,
		p.nome as Convenio,p.id,c.*,(CASE c.acompSolicitante
		WHEN 's' THEN 'Sim'
		WHEN 'n' THEN 'N&atilde;o'
		END) AS acomp,
		NUM_MATRICULA_CONVENIO,
		cpf
		FROM
		clientes AS c LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN
		planosdesaude as p ON (p.id = c.convenio)
		WHERE
		{$condp1}
		ORDER BY
		c.nome DESC LIMIT 1";

        $result = mysql_query($sql);
        $result2 = mysql_query($sql2);

//        $sql3 = "Select * from solicitacoes ";
//
//        $result_enfermeira = mysql_query($sql3);

        //Eriton

        $sql4 = "SELECT
                    u.nome AS nome,
                    u.conselho_regional,
                    u.tipo as tipo_usuario,
                    empresas.nome AS empresa_rel,
                    planosdesaude.nome AS convenio_rel,
                    DATE_FORMAT(evol.DATA_EVOLUCAO,'%d/%m/%Y') AS data
            FROM
                    evolucaoenfermagem AS evol INNER JOIN
                    usuarios AS u ON (evol.USUARIO_ID = u.idUsuarios)
                    LEFT JOIN empresas ON evol.empresa = empresas.id
                    LEFT JOIN planosdesaude ON evol.convenio = planosdesaude.id
            WHERE
                    PACIENTE_ID = {$id} and evol.ID = {$id_a}";

        $result4 = mysql_query($sql4);
        while ($row = mysql_fetch_array($result4)){
            $compTipo = $row['conselho_regional'];
            $nome_enf = $row['nome'] . $compTipo;
            $data_evolucao = $row['data'];
            $empresa = $empresa;
            $convenio = $row['convenio_rel'];
        }
        //Fim	
        //Eriton Busca Local Internaï¿½ï¿½o

        $sql5 = "SELECT
					c.localinternacao AS local_i
				FROM
					clientes AS c
				WHERE
					idClientes = {$id}";
        $result5 = mysql_query($sql5);
        $local_i = mysql_result(result5, 0, 0);

        //Fim Eriton Busca Local Internaï¿½ï¿½o
        $html.= "<table style='width:100%;' >";
        while ($pessoa = mysql_fetch_array($result2)) {
            foreach ($pessoa AS $chave => $valor) {
                $pessoa[$chave] = stripslashes($valor);
            }

            $pessoa['empresan'] = $empresa;
            $pessoa['Convenio'] = !empty($convenio) ? $convenio : $pessoa['Convenio'];

            $html.= "<tr bgcolor='#EEEEEE'>";
            $html.= "<td  style='width:100%;'><b>PACIENTE:</b></td>";
            $html.= "<td ><b>SEXO </b></td>";
            $html.= "<td style='width:100%;'><b>IDADE:</b></td>";
            $html.= "</tr>";
            $html.= "<tr>";
            $html.= "<td style='width:100%;'>{$pessoa['paciente']}</td>";
            $html.= "<td>{$pessoa['nomesexo']}</td>";
            $html.= "<td>" . join("/", array_reverse(explode("-", $pessoa['nascimento']))) . ' (' . $pessoa['idade'] . " anos)</td>";
            $html.= "</tr>";

            $html.= "<tr bgcolor='#EEEEEE'>";

            $html.= "<td ><b>UNIDADE REGIONAL:</b></td>";
            $html.= "<td style='width:100%;'><b>CONV&Ecirc;NIO </b></td>";
            $html.= "<td><b>MATR&Iacute;CULA </b></td>";
            $html.= "</tr>";
            $html.= "<tr>";

            $html.= "<td>{$pessoa['empresan']}</td>";
            $html.= "<td>" . strtoupper($pessoa['Convenio']) . "</td>";
            $html.= "<td>" . strtoupper($pessoa['NUM_MATRICULA_CONVENIO']) . "</td>";
            $html.= "</tr>";

            $html.= "<tr bgcolor='#EEEEEE'>";
            $html.= "<td><b>M&Eacute;DICO ASSISTENTE:</b></td>";
            $html.= "<td><b>ESPECIALIDADE:</b></td>";
            $html.= "<td><b>ACOMPANHARA O PACIENTE (Sim ou N&atilde;o)</b></td>";
            $html.= "</tr>";
            $html.= "<tr>";
            $html.= "<td>{$pessoa['medicoSolicitante']}</td>";
            $html.= "<td>{$pessoa['espSolicitante']}</td>";
            $html.= "<td>{$pessoa['acomp']}</td>";
            $html.= "</tr>";
            $html.= "<tr bgcolor='#EEEEEE'>";
            $html.= "<td><b>UNIDADE DE INTERNA&Ccedil;&Atilde;O/HOSPITAL</b></td>";
            $html.= "<td><b>TELEFONE:</b></td>";
            $html.= "<td><b>TELEFONE RESPONS&Aacute;VEL:</b></td>";
            $html.= "</tr>";
            $html.= "<tr>";
            $html.= "<td>{$pessoa['localInternacao']}</td>";
            $html.= "<td>{$pessoa['TEL_DOMICILIAR']}</td>";
            $html.= "<td>{$pessoa['TEL_RESPONSAVEL']}</td>";
            $html.= "</tr>";
            $html.= "<tr bgcolor='#EEEEEE'>";
            $html.= "<td><b>AVALIADORA</b></td>";
            $html.= "<td><b>LOCAL DA EVOLU&Ccedil;&Atilde;O:</b></td>";
            $html.= "<td><b>DATA DA EVOLU&Ccedil;&Atilde;O:</b></td>";
            $html.= "</tr>";
            $html.= "<tr>";
            $html.= "<td>{$nome_enf}</td>";
            $html.= "<td>{$pessoa['local_i']}</td>";
            $html.= "<td>{$data_evolucao}</td>";
            //Fim atualizaï¿½ï¿½o Eriton 19-03-2013
            $html.= "<td></td>";
            $html.= "</tr>";
            $html.= " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
        }
        $html.= "</table>";

        return $html;
    }

    public function detalhes_evolucao_enfermagem($id, $fichaEvolucaoEnfermagem, $empresa_id) {

        //$this->cabecalho($fae);
        //	$this->plan;
        //$html.= ($id);
        //	$html.= $v;
        if ($v == 1) {
            $acesso = "readonly"; //read
            $enabled = "disabled";
        }

        $sql = "select *,DATE_FORMAT(ULTIMA_SUBSTITUICAO,'%d/%m/%Y') AS ultima_substituicao from evolucaoenfermagem where ID = {$fichaEvolucaoEnfermagem} and PACIENTE_ID = {$id}";
        //$html.= $sql;
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {

            $paciente = $row['PACIENTE_ID'];
            $id_aval = $row['ID'];
            $bom_ruim = $row['BOM_RUIM'];
            $nutrido_desnutrido = $row['NUTRIDO_DESNUTRIDO'];
            $higi_corp_satis = $row['HIGI_CORP_SATIS'];
            $higi_bucal_satis = $row['HIGI_BUCAL_SATIS'];
            $estado_emocional = $row['ESTADO_EMOCIONAL'];
            $consciente = $row['CONSCIENTE'];
            $sedado = $row['SEDADO'];
            $obnubilado = $row['OBNUBILADO'];
            $torporoso = $row['TORPOROSO'];
            $comatoso = $row['COMATOSO'];
            $orientado = $row['ORIENTADO'];
            $desorientado = $row['DESORIENTADO'];
            $sonolento = $row['SONOLENTO'];
            $integro = $row['INTEGRO'];
            $cicatriz = $row['CICATRIZ'];
            $local_cicatriz = $row['LOCAL_CICATRIZ'];
            $cond_motora = $row['COND_MOTORA'];
            $palergiamed = $row['PALERGIAMED'];
            $palergiaalim = $row['PALERGIAALIM'];
            $tipoalergiamed = $row['TIPOALERGIAMED'];
            $tipoalergiaalim = $row['TIPOALERGIAALIM'];
            $integra_lesao = $row['INTEGRA_LESAO'];
            $icterica_anicterica = $row['ICTERICA_ANICTERICA'];
            $hidratada_desidratada = $row['HIDRATADA_DESIDRATADA'];
            $palida = $row['PALIDA'];
            $sudoreica = $row['SUDOREICA'];
            $fria = $row['FRIA'];
            $pele_edema = $row['PELE_EDEMA'];
            $descamacao = $row['DESCAMACAO'];
            $mucosas = $row['MUCOSAS'];
            $alt_pescoco = $row['ALT_PESCOCO'];
            $nodulos = $row['NODULOS'];
            $ganglios_inf = $row['GANGLIOS_INF'];
            $rigidez = $row['RIGIDEZ'];
            $bocio = $row['BOCIO'];
            $ferida_pescoco = $row['FERIDA_PESCOCO'];
            $ausculta_alterada = $row['AUSCULTA_ALTERADA'];
            $creptos = $row['CREPTOS'];
            $tipo_creptos = $row['TIPO_CREPTOS'];
            $estertores = $row['ESTERTORES'];
            $tipo_estertores = $row['TIPO_ESTERTORES'];
            $roncos = $row['RONCOS'];
            $tipo_roncos = $row['TIPO_RONCOS'];
            $mv_dimi = $row['MV_DIMI'];
            $tipo_mv_dimi = $row['TIPO_MV'];
            $simetrico_assimetrico = $row['SIMETRICO_ASSIMETRICO'];
            $palpacao = $row['PALPACAO'];
            $ganglios = $row['GANGLIOS'];
            $cicatriz_torax = $row['CICATRIZ_TORAX'];
            $local_cicatriz_torax = $row['LOCAL_CICATRIZ'];
            $afundamento = $row['AFUNDAMENTO'];
            $padrao_cardiorespiratorio = $row['PADRAO_CARDIORESPIRATORIO'];
            $padrao_cardiorespiratorio1 = $row['PADRAO_CARDIORESPIRATORIO1'];
            $padrao_cardiorespiratorio2 = $row['PADRAO_CARDIORESPIRATORIO2'];
            $eupneico = $row['EUPNEICO'];
            $dispineico = $row['DISPINEICO'];
            $bradipneico = $row['BRADIPNEICO'];
            $taquipneico = $row['TAQUIPNEICO'];
            $taquicardico = $row['TAQUICARDICO'];
            $bradicardico = $row['BRADICARDICO'];
            $tipo_ventilacao = $row['TIPO_VENTILACAO'];
            $tipo_ventilacao1 = $row['TIPO_VENTILACAO1'];
            $num_ventilacao = $row['NUM_VENTILACAO'];
            $tipo_ventilacao2 = $row['TIPO_VENTILACAO2'];
            $bipap = $row['BIPAP'];
            $oxigenio_sn = $row['OXIGENIO_SN'];
            $oxigenio = $row['OXIGENIO'];
            $vent_cateter_oxigenio_num = $row['VENT_CATETER_OXIGENIO_NUM'];
            $vent_cateter_oxigenio_quant = $row['VENT_CATETER_OXIGENIO_QUANT'];
            $vent_cateter_oculos_num = $row['VENT_CATETER_OCULOS_NUM'];
            $vent_respirador = $row['VENT_RESPIRADOR'];
            $vent_respirador_tipo = $row['VENT_RESPIRADOR_TIPO'];
            $vent_mascara_ventury = $row['VENT_MASCARA_VENTURY'];
            $vent_mascara_ventury_tipo = $row['VENT_MASCARA_VENTURY_NUM'];
            $vent_concentrador = $row['VENT_CONCENTRADOR'];
            $vent_concentrador_num = $row['VENT_CONCENTRADOR_NUM'];
            $alt_aspiracoes = $row['ALT_ASPIRACOES'];
            $aspiracoes_num = $row['ASPIRACOES_NUM'];
            $aspiracao_secrecao = $row['ASPIRACAO_SECRECAO'];
            $caracteristicas_sec = $row['CARACTERISTICAS_SEC'];
            $cor_sec = $row['COR_SEC'];
            $plano = $row['PLANO'];
            $flacido = $row['FLACIDO'];
            $globoso = $row['GLOBOSO'];
            $distendido = $row['DISTENDIDO'];
            $rha = $row['RHA'];
            $indo_palpacao = $row['INDO_PALPACAO'];
            $doloroso = $row['DOLOROSO'];
            $timpanico = $row['TIMPANICO'];
            $dieta_sn = $row['DIETA_SN'];
            $oral = $row['ORAL'];
            $sne = $row['SNE'];
            $sne_num = $row['SNE_NUM'];
            $gtm_sonda_gastro = $row['GTM_SONDA_GASTRO'];
            $gtm_num = $row['GTM_NUM'];
            $gtm_num_profundidade = $row['GTM_NUM_PROFUNDIDADE'];
            $quant_sonda = $row['QUANT_SONDA'];
            $ultima_substituicao = $row['ultima_substituicao'];
            $sistema_aberto = $row['SISTEMA_ABERTO'];
            $quant_sistema_aberto = $row['QUANT_SISTEMA_ABERTO'];
            $uso_sistema_aberto = $row['USO_SISTEMA_ABERTO'];
            $sistema_fechado = $row['SISTEMA_FECHADO'];
            $quant_sistema_fechado = $row['QUANT_SISTEMA_FECHADO'];
            $vazao = $row['VAZAO'];
            $dieta_npt = $row['DIETA_NPT'];
            $vazao_npt = $row['VAZAO_NPT'];
            $desc_npt = $row['desc_npt'];
            $dieta_npp = $row['DIETA_NPP'];
            $vazao_npp = $row['VAZAO_NPP'];
            $desc_npp = $row['desc_npp'];
            $dieta_artesanal = $row['DIETA_ARTESANAL'];
            $suplemento_sn = $row['SUPLEMENTO_SN'];
            $suplemento = $row['SUPLEMENTO'];
            $integra_genital = $row['INTEGRA_GENITAL'];
            $secrecao = $row['SECRECAO'];
            $sangramento = $row['SANGRAMENTO'];
            $prurido = $row['PRURIDO'];
            $edema = $row['EDEMA'];
            $edema_mmss = $row['EDEMA_MMSS'];
            $deformidades_mmss = $row['DEFORMIDADES_MMSS'];
            $lesoes_mmss = $row['LESOES_MMSS'];
            $edema_mmii = $row['EDEMA_MMII'];
            $varizes_mmii = $row['VARIZES_MMII'];
            $deformidades_mmii = $row['DEFORMIDADES_MMII'];
            $lesoes_mmii = $row['LESOES_MMII'];
            $aquecidas_ext = $row['AQUECIDAS_EXT'];
            $oxigenadas_ext = $row['OXIGENADAS_EXT'];
            $frias_ext = $row['FRIAS_EXT'];
            $cianoticas_ext = $row['CIANOTICAS_EXT'];
            $pulso_presente = $row['PULSO_PRESENTE'];
            $dispositivos_intravenosos = $row['DISPOSITIVOS_INTRAVENOSOS'];
            $cvc = $row['CVC'];
            $cvc_tipo = $row['CVC_TIPO'];
            $local_cvc_uno = $row['LOCAL_CVC_UNO'];
            $local_cvc_duplo = $row['LOCAL_CVC_DUPLO'];
            $local_cvc_triplo = $row['LOCAL_CVC_TRIPLO'];
            $avp = $row['AVP'];
            $avp_local = $row['AVP_LOCAL'];
            $port = $row['PORT'];
            $quant_port = $row['QUANT_PORT'];
            $port_local = $row['PORT_LOCAL'];
            $outros_intra = $row['OUTROS_INTRA'];
            $alt_gastro = $row['ALT_GASTRO'];
            $diarreia_obstipacao = $row['DIARREIA_OBSTIPACAO'];
            $nauseas_gastro = $row['NAUSEAS_GASTRO'];
            $vomitos_gastro = $row['VOMITOS_GASTRO'];
            $diarreia_episodios = $row['DIARREIA_EPISODIOS'];
            $obstipacao_episodios = $row['OBSTIPACAO_EPISODIOS'];
            $vomito_episodios = $row['VOMITO_EPISODIOS'];
            $cateterismo_int = $row['CATETERISMO_INT'];
            $normal_urinario = $row['NORMAL_URINARIO'];
            $anuria_urinario = $row['ANURIA_URINARIO'];
            $incontinencia_urinario = $row['INCONTINENCIA_URINARIO'];
            $hematuria_urinario = $row['HEMATURIA_URINARIO'];
            $piuria_urinario = $row['PIURIA_URINARIO'];
            $oliguria_urinario = $row['OLIGURIA_URINARIO'];
            $odor_urinario = $row['ODOR_URINARIO'];
            $eliminaciones = $row['ELIMINACIONES'];
            $fralda = $row['FRALDA'];
            $fralda_dia = $row['FRALDA_DIA'];
            $tamanho_fralda = $row['TAMANHO_FRALDA'];
            $urinario = $row['URINARIO'];
            $quant_urinario = $row['QUANT_URINARIO'];
            $sonda = $row['SONDA'];
            $quant_foley = $row['QUANT_FOLEY'];
            $sonda_alivio = $row['SONDA_ALIVIO'];
            $quant_alivio = $row['QUANT_ALIVIO'];
            $colostomia = $row['COLOSTOMIA'];
            $quant_colostomia = $row['QUANT_COLOSTOMIA'];
            $iliostomia = $row['DISPO_ILIOSTOMIA'];
            $quant_iliostomia = $row['DISPO_ILIOSTOMIA_NUM'];
            $cistostomia = $row['CISTOSTOMIA'];
            $quant_cistosmia = $row['QUANT_CISTOSMIA'];
            $dreno_sn = $row['DRENO_SN'];
            $dreno = $row['DRENO'];
            $feridas_sn = $row['FERIDAS_SN'];
            $pa_sistolica = $row['PA_SISTOLICA'];
            $pa_diastolica = $row['PA_DIASTOLICA'];
            $fc = $row['FC'];
            $fr = $row['FR'];
            $spo2 = $row['SPO2'];
            $temperatura = $row['TEMPERATURA'];
            $pad = $row['PAD'];
            $picc=$row['CATETER_PICC'];
            $picc_local=$row['CATETER_PICC_LOCAL'];
           $constipacao_gastro = $row['constipacao_gastro'];
            $vigil =  $row['vigil'];
        }

        $html.= "<form method='post' target='_blank'>";

        $responseEmpresa = Filial::getEmpresaById($empresa_id);

        $html.= "<a><img src='../utils/logos/{$responseEmpresa['logo']}' width='100' ></a>";

        $html.= "<center><h1 style='font-size:20px;'>Ficha de Evolu&ccedil;&atilde;o de Enfermagem.</h1></center>";

        $html.= $this->cabecalho($id,$fichaEvolucaoEnfermagem, $responseEmpresa['nome']);

        $html.= "<table class='mytable' style='width:95%;' >";

        /*         * ************************* EXAME FISICO ************************************ */
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><center><b>Exame F&iacute;sico</b></center></td>";
        $html.= "</tr>";

        /* ESTADO GERAL */


        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Estado Geral</b></td>";
        $html.="</tr>";

        if ($bom_ruim == 1) {
            $bom = 'x';
            $ruim = ' ';
        } else {
            $bom = ' ';
            $ruim = 'x';
        }
        if ($nutrido_desnutrido == 1) {
            $nutrido = 'x';
            $desnutrido = ' ';
        } else {
            $nutrido = ' ';
            $desnutrido = 'x';
        }
        if ($higi_corp_satis) {
            $satisf = 'x';
            $insatis = '';
        } else {
            $satisf = '';
            $insatis = 'x';
        }
        if ($higi_bucal_satis) {
            $bucals = 'x';
            $bucali = '';
        } else {
            $bucals = '';
            $bucali = 'x';
        }

        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$bom})Bom &nbsp;&nbsp;&nbsp;({$ruim})Ruim  &nbsp;&nbsp;&nbsp;({$nutrido})Nutrido &nbsp;&nbsp;&nbsp;({$desnutrido})Desnutrido</h1></td></tr>";
        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$satisf})Higiene Corporal Satisfat&oacute;ria &nbsp;&nbsp;&nbsp;({$insatis})Higiene Corporal Insatisfat&oacute;ria  &nbsp;&nbsp;&nbsp;({$bucals})Higiene Bucal Satisfat&oacute;ria &nbsp;&nbsp;&nbsp;({$bucali})Higiene Bucal Insatisfat&oacute;ria</h1></td></tr>";

        //Estado Emocional
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Estado Emocional</b></td>";
        $html.= "</tr>";

        if ($estado_emocional == 1) {
            $calmo = 'x';
        } elseif ($estado_emocional == 2) {
            $agitado = 'x';
        } elseif ($estado_emocional == 3) {
            $deprimido = 'x';
        } elseif ($estado_emocional == 4) {
            $choroso = 'x';
        } else {
            $calmo = '';
            $agitado = '';
            $choroso = '';
            $deprimido = '';
        }

        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$calmo})Calmo &nbsp;&nbsp;&nbsp;({$agitado})Agitado  &nbsp;&nbsp;&nbsp;({$deprimido})Deprimido &nbsp;&nbsp;&nbsp;({$choroso})Choroso</h1></td></tr>";

        //Nivel de Consci�ncia
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>N&iacute;vel de Consci&ecirc;ncia</b></td>";
        $html.="</tr>";

        if ($consciente == 1) {
            $cons = 'x';
        } else {
            $cons = '';
        }
        if ($sedado == 1) {
            $sed = 'x';
        } else {
            $sed = '';
        }
        if ($obnublado == 1) {
            $ob = 'x';
        } else {
            $ob = '';
        }
        if ($torporoso == 1) {
            $torp = 'x';
        } else {
            $torp = '';
        }
        if ($comatoso == 1) {
            $coma = 'x';
        } else {
            $coma = '';
        }
        if ($orientado == 1) {
            $ori = 'x';
        } else {
            $ori = '';
        }
        if ($desorientado == 1) {
            $deso = 'x';
        } else {
            $deso = '';
        }
        if ($sonolento == 1) {
            $sono = 'x';
        } else {
            $sono = '';
        }
        $marca_vigil  = $vigil == 1 ? 'x':'';
        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$cons})Consciente &nbsp;&nbsp;&nbsp;({$sed})Sedado  &nbsp;&nbsp;&nbsp;({$ob})Obnubilado &nbsp;&nbsp;&nbsp;({$torp})Torporoso</h1></td></tr>";
        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$coma})Comatoso &nbsp;&nbsp;&nbsp;({$ori})Orientado  &nbsp;&nbsp;&nbsp;({$deso})Desorientado &nbsp;&nbsp;&nbsp;({$sono})Sonolento</h1></td></tr>";
        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$marca_vigil})Vígil </h1></td></tr>";
        //Couro cabeludo
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Couro Cabeludo</b></td>";
        $html.="</tr>";

        if ($integro == 1) {
            $integro_ = 'x';
        } else {
            $integro_ = '';
        }
        if ($cicatriz == 1) {
            $cicatriz_ = 'x';
            $local_cicatriz_ = "Local: {$local_cicatriz}";
        } else {
            $cicatriz_ = '';
            $local_cicatriz_ = '';
        }

        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$integro_})&Iacute;ntegro &nbsp;&nbsp;&nbsp;({$cicatriz_})Cicatriz&nbsp;&nbsp;&nbsp; {$local_cicatriz_}</h1></td></tr>";

        //Condição motora
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Condi&ccedil;&atilde;o Motora</b></td>";
        $html.="</tr>";


        if ($cond_motora == 1) {
            $deambula = 'x';
        }
        if ($cond_motora == 2) {
            $deambula_a = 'x';
        }
        if ($cond_motora == 3) {
            $cadeirante = 'x';
        }
        if ($cond_motora == 4) {
            $acamado = 'x';
        }

        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$deambula})Deambula &nbsp;&nbsp;&nbsp;({$deambula_a})Deambula com Aux&iacute;lio  &nbsp;&nbsp;&nbsp;({$cadeirante})Cadeirante &nbsp;&nbsp;&nbsp;({$acamado})Acamado</h1></td></tr>";

        //Alergia Medicamentosa
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        switch ($palergiamed) {
            case "s":
                $palergiamed = "Sim";
                break;
            case "n":
                $palergiamed = "N&atilde;o";
                break;
        }
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Alergia Medicamentosa - {$palergiamed}</b></td>";
        $html.="</tr>";
        if ($palergiamed == 'Sim')
            $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>Medicamento: {$tipoalergiamed}</h1></td></tr>";

        //Alergia Alimentar
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        switch ($palergiaalim) {
            case "s":
                $palergiaalim = "Sim";
                break;
            case "n":
                $palergiaalim = "N&atilde;o";
                break;
            default:
                $palergiaalim = "N&atilde;o";
                break;
        }
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Alergia Alimentar - {$palergiaalim}</b></td>";
        $html.="</tr>";
        if ($palergiaalim == 'Sim')
            $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>Alimento: {$tipoalergiaalim}</h1></td></tr>";

        //Pele
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Pele</b></td>";
        $html.="</tr>";

        if ($integra_lesao == 1) {
            $inte = 'x';
            $leso = '';
            $exibir = "style='display:none";
        } else {
            $leso = 'x';
            $inte = '';
        }
        if ($hidratada_desidratada == '1') {
            $hid = '';
            $desi = 'x';
            $exibir = "style='display:none";
        } else {
            $hid = 'x';
            $desi = '';
        }
        if ($icterica == '1') {
            $ict = 'x';
            $ani = '';
            $exibir = "style='display:block";
        } else {
            $ani = 'x';
            $ict = '';
        }

        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$inte})&Iacute;ntegra  &nbsp;&nbsp;&nbsp;({$ict})Ict&eacute;rica &nbsp;";
        if ($ict) {
            $html.=" Escala:{$icterica_escala} &nbsp;&nbsp;&nbsp;";
        }
        $html.="&nbsp;&nbsp;&nbsp;({$hid})Hidratada  </h1></td></tr>";
        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$leso})Lesionada  &nbsp;&nbsp;&nbsp;({$ani})Anict&eacute;rica &nbsp;&nbsp;&nbsp;({$desi})Desidratada  </h1></td></tr>";
        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>  </h1></td></tr>";
        if ($palida == 1) {
            $pali = 'x';
        }
        if ($palida == 2) {
            $pali2 = 'x';
        }
        if ($sudoreica == 1) {
            $sudo = 'x';
        }
        if ($sudoreica == 2) {
            $sudo2 = 'x';
        }
        if ($fria == 1) {
            $fri = 'x';
        }
        if ($fria == 2) {
            $fri2 = 'x';
        }
        if ($edema == 1) {
            $ede = 'x';
        }
        if ($edema == 2) {
            $ede2 = 'x';
        }
        if ($descamacao == 1) {
            $desc = 'x';
        }
        if ($descamacao == 2) {
            $desc2 = 'x';
        }
        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>P&aacute;lida: ({$pali})Sim &nbsp;&nbsp;&nbsp;({$pali2})N&atilde;o  </h1></td></tr>";
        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>Sudoreica: ({$sudo})Sim &nbsp;&nbsp;&nbsp;({$sudo2})N&atilde;o  </h1></td></tr>";
        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>Fria: ({$fri})Sim &nbsp;&nbsp;&nbsp;({$fri2})N&atilde;o  </h1></td></tr>";
        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>Edema: ({$ede})Sim &nbsp;&nbsp;&nbsp;({$ede2})N&atilde;o  </h1></td></tr>";
        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>Descama&ccedil;&aacute;o: ({$desc})Sim &nbsp;&nbsp;&nbsp;({$desc2})N&atilde;o  </h1></td></tr>";

        //Mucosas
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Mucosas</b></td>";
        $html.="</tr>";

        if ($mucosas == 1) {
            $corada = 'x';
        } else {
            $corada = '';
        }
        if ($mucosas == 2) {
            $descoradas = 'x';
        } else {
            $descoradas = '';
        }
        if ($mucosas == 3) {
            $hipocoradas = 'x';
        } else {
            $hipocoradas = '';
        }

        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$corada})Coradas &nbsp;&nbsp;&nbsp;({$descoradas})Descoradas&nbsp;&nbsp;&nbsp;({$hipocoradas})Hipocoradas</h1></td></tr>";

        //Alterações Pescoço
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        switch ($alt_pescoco) {
            case 1:
                $alt_pescoco = "N&atilde;o";
                break;
            case 2:
                $alt_pescoco = "Sim";
                break;
            default:
                $alt_pescoco = "N&atilde;o";
                break;
        }
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Atera&ccedil;&otilde;es Pesco&ccedil;o - {$alt_pescoco}</b></td>";
        $html.="</tr>";
        if ($alt_pescoco == 'Sim') {
            if ($nodulos == 1) {
                $nodulo = 'x';
            } else {
                $nodulo = '';
            }
            if ($ganglios_inf == 1) {
                $ganglio_inf = 'x';
            } else {
                $ganglio_inf = '';
            }
            if ($rigidez == 1) {
                $rigidez_pescoco = 'x';
            } else {
                $rigidez_pescoco = '';
            }
            if ($bocio == 1) {
                $bocio_pesc = 'x';
            } else {
                $bocio_pesc = '';
            }
            if ($ferida_pescoco == 1) {
                $ferida_pesc = 'x';
            } else {
                $ferida_pesc = '';
            }

            $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$nodulo})n&oacute;dulo &nbsp;&nbsp;&nbsp;({$ganglio_inf})g&acirc;nglios infartados &nbsp;&nbsp;&nbsp;({$rigidez_pescoco})Rigidez &nbsp;&nbsp;&nbsp;({$bocio_pesc})B&oacute;cio &nbsp;&nbsp;&nbsp;({$ferida_pesc})Ferida</h1></td></tr>";
        }

        //Torax
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>T&oacute;rax</b></td>";
        $html.="</tr>";

        if ($simetrico_assimetrico == 1) {
            $simetrico = 'x';
            $assimetrico = '';
        } else {
            $simetrico = '';
            $assimetrico = 'x';
        }
        if ($palpacao == 1) {
            $dor_palpacao_s = 'x';
            $dor_palpacao_n = '';
        } else {
            $dor_palpacao_n = 'x';
            $dor_palpacao = '';
        }
        if ($ganglios == 1) {
            $ganglio_s = 'x';
            $ganglio_n = '';
        } else {
            $ganglio_n = 'x';
            $ganglio = '';
        }
        if ($cicatriz_torax == 1) {
            $cicatriz_tor_s = 'x';
            $cicatriz_tor_n = '';
            $local_cicatriz_torax_ = "Local: {$local_cicatriz_torax}";
        } else {
            $cicatriz_tor_n = 'x';
            $cicatriz_tor = '';
            $local_cicatriz_torax_ = '';
        }
        if ($afundamento == 1) {
            $afundamento_tor_s = 'x';
            $afundamento_tor_n = '';
        } else {
            $afundamento_tor_n = 'x';
            $afundamento_tor = '';
        }
        if ($ausculta_alterada == 1) {
            $ausculta_sim = 'x';
            $ausculta_nao = '';
            if (isset($creptos) && $creptos === '1'){
                $creptos_sim = 'x';
                $creptos_nao = '';
                if (isset($tipo_creptos) && $tipo_creptos !== '0') {
                    switch ($tipo_creptos) {
                        case '1':
                            $creptos_base_esquerda = 'x';
                            $creptos_base_direita = '';
                            $creptos_ambas_bases = '';
                            $creptos_apice_esquerda = '';
                            $creptos_apice_direita = '';
                            $creptos_parede_anterior = '';
                            break;
                        case '2':
                            $creptos_base_esquerda = '';
                            $creptos_base_direita = 'x';
                            $creptos_ambas_bases = '';
                            $creptos_apice_esquerda = '';
                            $creptos_apice_direita = '';
                            $creptos_parede_anterior = '';
                            break;
                        case '3':
                            $creptos_base_esquerda = '';
                            $creptos_base_direita = '';
                            $creptos_ambas_bases = 'x';
                            $creptos_apice_esquerda = '';
                            $creptos_apice_direita = '';
                            $creptos_parede_anterior = '';
                            break;
                        case '4':
                            $creptos_base_esquerda = '';
                            $creptos_base_direita = '';
                            $creptos_ambas_bases = '';
                            $creptos_apice_esquerda = 'x';
                            $creptos_apice_direita = '';
                            $creptos_parede_anterior = '';
                            break;
                        case '5':
                            $creptos_base_esquerda = '';
                            $creptos_base_direita = '';
                            $creptos_ambas_bases = '';
                            $creptos_apice_esquerda = '';
                            $creptos_apice_direita = 'x';
                            $creptos_parede_anterior = '';
                            break;
                        case '6':
                            $creptos_base_esquerda = '';
                            $creptos_base_direita = '';
                            $creptos_ambas_bases = '';
                            $creptos_apice_esquerda = '';
                            $creptos_apice_direita = '';
                            $creptos_parede_anterior = 'x';
                            break;
                    }
                }
            }  else {
                $creptos_sim = '';
                $creptos_nao = 'x';
            }
            if (isset($estertores) && $estertores === '1'){
                $estertores_sim = 'x';
                $estertores_nao = '';
                if (isset($tipo_estertores) && $tipo_estertores !== '0') {
                    switch ($tipo_estertores) {
                        case '1':
                            $estertores_base_esquerda = 'x';
                            $estertores_base_direita = '';
                            $estertores_ambas_bases = '';
                            $estertores_apice_esquerda = '';
                            $estertores_apice_direita = '';
                            $estertores_parede_anterior = '';
                            break;
                        case '2':
                            $estertores_base_esquerda = '';
                            $estertores_base_direita = 'x';
                            $estertores_ambas_bases = '';
                            $estertores_apice_esquerda = '';
                            $estertores_apice_direita = '';
                            $estertores_parede_anterior = '';
                            break;
                        case '3':
                            $estertores_base_esquerda = '';
                            $estertores_base_direita = '';
                            $estertores_ambas_bases = 'x';
                            $estertores_apice_esquerda = '';
                            $estertores_apice_direita = '';
                            $estertores_parede_anterior = '';
                            break;
                        case '4':
                            $estertores_base_esquerda = '';
                            $estertores_base_direita = '';
                            $estertores_ambas_bases = '';
                            $estertores_apice_esquerda = 'x';
                            $estertores_apice_direita = '';
                            $estertores_parede_anterior = '';
                            break;
                        case '5':
                            $estertores_base_esquerda = '';
                            $estertores_base_direita = '';
                            $estertores_ambas_bases = '';
                            $estertores_apice_esquerda = '';
                            $estertores_apice_direita = 'x';
                            $estertores_parede_anterior = '';
                            break;
                        case '6':
                            $estertores_base_esquerda = '';
                            $estertores_base_direita = '';
                            $estertores_ambas_bases = '';
                            $estertores_apice_esquerda = '';
                            $estertores_apice_direita = '';
                            $estertores_parede_anterior = 'x';
                            break;
                    }
                }
            }  else {
                $estertores_sim = '';
                $estertores_nao = 'x';
            }
            if (isset($roncos) && $roncos === '1'){
                $roncos_sim = 'x';
                $roncos_nao = '';
                if (isset($tipo_roncos) && $tipo_roncos !== '0') {
                    switch ($tipo_roncos) {
                        case '1':
                            $roncos_base_esquerda = 'x';
                            $roncos_base_direita = '';
                            $roncos_ambas_bases = '';
                            $roncos_apice_esquerda = '';
                            $roncos_apice_direita = '';
                            $roncos_parede_anterior = '';
                            break;
                        case '2':
                            $roncos_base_esquerda = '';
                            $roncos_base_direita = 'x';
                            $roncos_ambas_bases = '';
                            $roncos_apice_esquerda = '';
                            $roncos_apice_direita = '';
                            $roncos_parede_anterior = '';
                            break;
                        case '3':
                            $roncos_base_esquerda = '';
                            $roncos_base_direita = '';
                            $roncos_ambas_bases = 'x';
                            $roncos_apice_esquerda = '';
                            $roncos_apice_direita = '';
                            $roncos_parede_anterior = '';
                            break;
                        case '4':
                            $roncos_base_esquerda = '';
                            $roncos_base_direita = '';
                            $roncos_ambas_bases = '';
                            $roncos_apice_esquerda = 'x';
                            $roncos_apice_direita = '';
                            $roncos_parede_anterior = '';
                            break;
                        case '5':
                            $roncos_base_esquerda = '';
                            $roncos_base_direita = '';
                            $roncos_ambas_bases = '';
                            $roncos_apice_esquerda = '';
                            $roncos_apice_direita = 'x';
                            $roncos_parede_anterior = '';
                            break;
                        case '6':
                            $roncos_base_esquerda = '';
                            $roncos_base_direita = '';
                            $roncos_ambas_bases = '';
                            $roncos_apice_esquerda = '';
                            $roncos_apice_direita = '';
                            $roncos_parede_anterior = 'x';
                            break;
                    }
                }
            }  else {
                $roncos_sim = '';
                $roncos_nao = 'x';
            }
            if (isset($mv_dimi) && $mv_dimi === '1'){
                $mv_sim = 'x';
                $mv_nao = '';
                if (isset($tipo_mv_dimi) && $tipo_mv_dimi !== '0') {
                    switch ($tipo_mv_dimi) {
                        case '1':
                            $mv_base_esquerda = 'x';
                            $mv_base_direita = '';
                            $mv_ambas_bases = '';
                            $mv_apice_esquerda = '';
                            $mv_apice_direita = '';
                            $mv_parede_anterior = '';
                            break;
                        case '2':
                            $mv_base_esquerda = '';
                            $mv_base_direita = 'x';
                            $mv_ambas_bases = '';
                            $mv_apice_esquerda = '';
                            $mv_apice_direita = '';
                            $mv_parede_anterior = '';
                            break;
                        case '3':
                            $mv_base_esquerda = '';
                            $mv_base_direita = '';
                            $mv_ambas_bases = 'x';
                            $mv_apice_esquerda = '';
                            $mv_apice_direita = '';
                            $mv_parede_anterior = '';
                            break;
                        case '4':
                            $mv_base_esquerda = '';
                            $mv_base_direita = '';
                            $mv_ambas_bases = '';
                            $mv_apice_esquerda = 'x';
                            $mv_apice_direita = '';
                            $mv_parede_anterior = '';
                            break;
                        case '5':
                            $mv_base_esquerda = '';
                            $mv_base_direita = '';
                            $mv_ambas_bases = '';
                            $mv_apice_esquerda = '';
                            $mv_apice_direita = 'x';
                            $mv_parede_anterior = '';
                            break;
                        case '6':
                            $mv_base_esquerda = '';
                            $mv_base_direita = '';
                            $mv_ambas_bases = '';
                            $mv_apice_esquerda = '';
                            $mv_apice_direita = '';
                            $mv_parede_anterior = 'x';
                            break;
                    }
                }
            }  else {
                $mv_sim = '';
                $mv_nao = 'x';
            }
        } else {
            $ausculta_nao = 'x';
            $ausculta_sim = '';
        }
        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$simetrico})Sim&eacute;trico &nbsp;&nbsp;&nbsp;({$assimetrico})Assim&eacute;trico</td></tr>";
        $html.= "<tr><h1 style='font-size:12px;'><td width='25%' colspan='4'>Doloroso a Palpa&ccedil;&atilde;o</h1></td><td><h1 style='font-size:12px;'>({$dor_palpacao_s})Sim &nbsp;&nbsp;&nbsp; ({$dor_palpacao_n})N&atilde;o</h1></td></tr>";
        $html.= "<tr><h1 style='font-size:12px;'><td width='25%' colspan='4'> G&acirc;nglios</h1></td><td><h1 style='font-size:12px;'>({$ganglio_s})Sim &nbsp;&nbsp;&nbsp; ({$ganglio_n})N&atilde;o</h1></td></tr>";
        $html.= "<tr><h1 style='font-size:12px;'><td width='25%' colspan='4'> Cicatriz</h1></td><td><h1 style='font-size:12px;'>({$cicatriz_tor_s})Sim &nbsp;&nbsp;&nbsp; ({$cicatriz_tor_n})N&atilde;o &nbsp;&nbsp; $local_cicatriz_torax_</h1></td></tr>";
        $html.= "<tr><h1 style='font-size:12px;'><td width='25%' colspan='4'> Afundamento</h1></td><td><h1 style='font-size:12px;'>({$afundamento_tor_s})Sim &nbsp;&nbsp;&nbsp; ({$afundamento_tor_n}) N&atilde;o</h1></td></tr>";
        $html.= "<tr><h1 style='font-size:12px;'><td width='25%' colspan='4'> Ausculta Alterada</h1></td><td><h1 style='font-size:12px;'>({$ausculta_sim}) Sim &nbsp;&nbsp;&nbsp; ({$ausculta_nao}) N&atilde;o</h1></td></tr>";
        if($ausculta_sim !== ''){
            //CREPTOS
            $html.= "<tr><td colspan='2'><h1 style='font-size:12px;'> Creptos</h1><h1 style='font-size:12px;'>({$creptos_sim}) Sim &nbsp;&nbsp;&nbsp; ({$creptos_nao}) N&atilde;o</h1>";
            if($creptos_sim !== ''){
                $html.= "<br><h1 style='font-size:12px;'> ({$creptos_base_direita}) Base Direita <br> ({$creptos_base_esquerda}) Base Esquerda <br> ({$creptos_ambas_bases}) Ambas Bases <br> ({$creptos_apice_esquerda}) &Aacute;pice Esquerda <br> ({$creptos_apice_direita}) &Aacute;pice Direita <br> ({$creptos_parede_anterior}) Parede Anterior</h1></td>";
            }
            //ESTERTORES
            $html.= "<td><h1 style='font-size:12px;'> Estertores</h1><h1 style='font-size:12px;'>({$estertores_sim}) Sim &nbsp;&nbsp;&nbsp; ({$estertores_nao}) N&atilde;o</h1>";
            if($estertores_sim !== ''){
                $html.= "<br><h1 style='font-size:12px;'> ({$estertores_base_direita}) Base Direita <br> ({$estertores_base_esquerda}) Base Esquerda <br> ({$estertores_ambas_bases}) Ambas Bases <br> ({$estertores_apice_esquerda}) &Aacute;pice Esquerda <br> ({$estertores_apice_direita}) &Aacute;pice Direita <br> ({$estertores_parede_anterior}) Parede Anterior </h1></td></tr>";
            }
            //RONCOS
            $html.= "<tr><td colspan='2'><h1 style='font-size:12px;'> Roncos</h1><h1 style='font-size:12px;'>({$roncos_sim}) Sim &nbsp;&nbsp;&nbsp; ({$roncos_nao}) N&atilde;o</h1>";
            if($roncos_sim !== ''){
                $html.= "<br><h1 style='font-size:12px;'> ({$roncos_base_direita}) Base Direita <br> ({$roncos_base_esquerda}) Base Esquerda <br> ({$roncos_ambas_bases}) Ambas Bases <br> ({$roncos_apice_esquerda}) &Aacute;pice Esquerda <br> ({$roncos_apice_direita}) &Aacute;pice Direita <br> ({$roncos_parede_anterior}) Parede Anterior </h1></td>";
            }
            //MV DIMINUIDO
            $html.= "<td><h1 style='font-size:12px;'> MV Diminuídos</h1><h1 style='font-size:12px;'>({$mv_sim}) Sim &nbsp;&nbsp;&nbsp; ({$mv_nao}) N&atilde;o</h1>";
            if($mv_sim !== ''){
                $html.= "<br><h1 style='font-size:12px;'> ({$mv_base_direita}) Base Direita <br> ({$mv_base_esquerda}) Base Esquerda <br> ({$mv_ambas_bases}) Ambas Bases <br> ({$mv_apice_esquerda}) &Aacute;pice Esquerda <br> ({$mv_apice_direita}) &Aacute;pice Direita <br> ({$mv_parede_anterior}) Parede Anterior </h1></td></tr>";
            }
        }
        //Padrao CardioRespiratorio
        $html.= "<tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Padr&atilde;o respirat&oacute;rio</b></td>";
        $html.="</tr>";

        if ($padrao_cardiorespiratorio == 1) {
            $padrao_cardio = "BULHAS CARD&Iacute;ACAS NORMOFON&Eacute;TICA (BCNF)";
        } else {
            $padrao_cardio = '';
        }
        switch ($padrao_cardiorespiratorio1) {
            case 1:
                $padrao_cardio1 = "TAQUICARDICO";
                break;
            case 2:
                $padrao_cardio1 = "BRADICARDICO";
                break;
            case 3:
                $padrao_cardio1 = "NORMOCADICO";
                break;
            default:
                $padrao_cardio1 = "";
        }

        switch ($padrao_cardiorespiratorio2) {
            case 1:
                $padrao_cardio2 = "EUPNEICO";
                break;
            case 2:
                $padrao_cardio2 = "TAQUIPNE&Iacute;CO";
                break;
            case 3:
                $padrao_cardio2 = "BRADIPNE&Iacute;CO";
                break;
            case 4:
                $padrao_cardio2 = "DISPNE&Iacute;CO";
                break;
            default:
                $padrao_cardio2 = "";
                break;
        }

        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'> {$padrao_cardio} </h1></td></tr>";
        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'> {$padrao_cardio1} </h1></td></tr>";
        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'> {$padrao_cardio2}</h1></td></tr>";

        //Tipo Ventilacao
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Tipo de Ventila&ccedil;&atilde;o</b></td>";
        $html.="</tr>";

        switch ($tipo_ventilacao) {
            case 1:
                $tipo_ventilacao_ = "ESPONT&Acirc;NEA";
                break;
            case 2:
                $tipo_ventilacao_ = "TRAQUEOSTOMIA";
                $numero = "N&ordm;{$num_ventilacao}";
                break;
            default:
                $tipo_ventilacao_ = '';
                $numero = '';
        }

        switch ($tipo_ventilacao1) {
            case 2:
                $tipo_ventilacao1_ = "METALICA";
                break;
            case 3:
                $tipo_ventilacao1_ = "PLASTICA";
                $tipo_ventilacao2_ = "EQUIPAMENTO";
                break;
            default:
                $tipo_ventilacao1_ = '';
                $tipo_ventilacao2_ = '';
                break;
        }
        if ($bipap == 1) {
            $bipap_ = "cont&iacute;nuo";
        } elseif ($bipap == 2) {
            $bipap_ = "Intermitente";
        } else {
            $bipap_ = '';
        }

        if ($tipo_ventilacao2_ == "EQUIPAMENTO") {
            $bipap_imprimir = "BIPAP: {$bipap_}";
        } else {
            $bipap_imprimir = '';
        }

        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'> {$tipo_ventilacao_} </h1></td></tr>";
        $html.= "<tr> <td colspan='4' ><h1 style='font-size:12px;'> {$tipo_ventilacao1_} </h1></td><td style='font-size:11px;'><b>$numero</b></td></tr>";
        if ($tipo_ventilacao2_ == "EQUIPAMENTO") {
            $html.= "<tr> <td colspan='4' ><h1 style='font-size:12px;'> {$tipo_ventilacao2_}</h1></td><td><h1 style='font-size:12px;'>$bipap_imprimir</h1></td></tr>";
        }

        //Oxigenio
        if ($oxigenio_sn == 's') {
            $oxi = 'x';
            $oxi_teste = 'Sim';
        } elseif ($oxigenio_sn == 'n') {
            $oxi2 = 'x';
            $oxi_teste = 'N&atilde;o';
        }
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Oxig&ecirc;nio - </b>{$oxi_teste}</td>";
        $html.="</tr>";

        if ($oxigenio == 1) {
            $cat_o = 'x';
        }
        if ($oxigenio == 2) {
            $cat_ocu = 'x';
        }
        if ($oxigenio == 3) {
            $masc_res = 'x';
        }
        if ($oxigenio == 4) {
            $masc_vent = 'x';
        }
        if ($oxigenio == 5) {
            $conce = 'x';
        }
        if ($oxigenio_sn == 's') {

            $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$cat_o}) Cateter de Oxig&ecirc;nio &nbsp;&nbsp;&nbsp;  N&ordm;: {$vent_cateter_oxigenio_num} L/MIN: {$vent_cateter_oxigenio_quant}  </h1></td></tr>";
            $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$cat_ocu}) Cateter tipo &Oacute;culos &nbsp;&nbsp;&nbsp; {$vent_cateter_oculos_num} L/MIN </h1></td></tr>";
            $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$masc_res}) M&aacute;scara Reservat&oacute;rio &nbsp;&nbsp;&nbsp; {$vent_respirador_tipo} L/MIN </h1></td></tr>";
            $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$masc_vent}) M&aacute;scara de Ventury &nbsp;&nbsp;&nbsp; {$vent_mascara_ventury} % </h1></td></tr>";
            $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$conce}) Concentrador &nbsp;&nbsp;&nbsp; {$vent_concentrador_num} % </h1></td></tr>";
        }

        //Aspiracoes
        $alt_aspiracoes = $alt_aspiracoes == 's'? "SIM":"N&Atilde;O";
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Aspira&ccedil;&otilde;es - $alt_aspiracoes</b></td>";
        $html.="</tr>";

        if ($aspiracao_secrecao == 'a') {
            $ause = 'x';
        }
        if ($aspiracao_secrecao == 'p') {
            $pres = 'x';
        }
        $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>N&uacute;mero de Aspira&ccedil;&otilde;es/Dia:  &nbsp;&nbsp;&nbsp; {$aspiracoes_num}</h1></td></tr>";
        $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>Secre&ccedil;&atilde;o:  &nbsp;&nbsp;&nbsp; ({$ause}) Ausente ({$pres}) Presente</h1></td></tr>";

        if ($aspiracao_secrecao == 'p') {

            $html.= "<tr><td colspan = '5'><h1 style='font-size:12px;'><b>Caracter&iacute;sticas da secre&ccedil;&atilde;o teste</b></h1></td></tr>";
            if ($cor_sec == 1) {
                $cla = 'x';
            }
            if ($caracteristicas_sec == 2) {
                $esp = 'x';
            }
            if ($cor_sec == 3) {
                $ama = 'x';
            }
            if ($cor_sec == 4) {
                $esv = 'x';
            }
            if ($cor_sec == 5) {
                $sang = 'x';
            }
            if ($caracteristicas_sec == 6) {
                $flui = 'x';
            }
            $html.= "<tr><td colspan = '5'><h1 style='font-size:12px;'> ($flui) Fluida  </h1></td></tr>";
            $html.= "<tr><td colspan = '5'><h1 style='font-size:12px;'> ($esp) Espessa </h1></td></tr>";
            $html.= "<tr><td colspan = '5'><h1 style='font-size:12px;'> ($cla) Clara  </h1></td></tr>";
            $html.= "<tr><td colspan = '5'><h1 style='font-size:12px;'> ($ama) Amarelada </h1></td></tr>";
            $html.= "<tr><td colspan = '5'><h1 style='font-size:12px;'> ($esv) Esverdeada </h1></td></tr>";
            $html.= "<tr><td colspan = '5'><h1 style='font-size:12px;'> ($sang) Sanguinolenta </h1></td></tr>";
        }

        //Avaliacao Abdominal
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Avalia&ccedil;&atilde;o Abdominal</b></td>";
        $html.="</tr>";
        if ($plano == 1) {
            $plan = 'x';
        }
        if ($flacido == 1) {
            $fla = 'x';
        }
        if ($distendido == 1) {
            $dist = 'x';
        }
        if ($timpanico == 1) {
            $timp = 'x';
        }
        if ($indo_palpacao == 1) {
            $indo_a = 'x';
        }
        if ($doloroso == 1) {
            $dolo = 'x';
        }
        if ($globoso == 1) {
            $glob = 'x';
        }

        if ($rha == 'a') {
            $ause_rha = 'x';
        }
        if ($rha == 'p') {
            $pres_rha = 'x';
        }

        $html.= "<tr> <td colspan='5'><h1 style='font-size:12px;'> ($plan) Plano </h1></td></tr>";
        $html.= "<tr> <td colspan='5'><h1 style='font-size:12px;'> ($fla)  Flacido </h1></td></tr>";
        $html.= "<tr> <td colspan='5'><h1 style='font-size:12px;'> ($dist) Distendido </h1></td></tr>";
        $html.= "<tr> <td colspan='5'> <h1 style='font-size:12px;'>($timp) Timp&acirc;nico </h1></td></tr>";
        $html.= "<tr> <td colspan='5'> <h1 style='font-size:12px;'>($glob) Globoso </h1></td></tr>";
        $html.= "<tr> <td colspan='5'><h1 style='font-size:12px;'> ($indo_a) Indolor a Palpa&ccedil;&atilde;o </h1></td></tr>";
        $html.= "<tr> <td colspan='5'><h1 style='font-size:12px;'> ($dolo)   Doloroso </h1></td></tr>";
        $html.= "<tr> <td colspan='5'><h1 style='font-size:12px;'> RHA ($pres_rha) Presente ($ause_rha) Ausente</h1></td></tr>";

        //VIA ALIMENTACAO
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Via alimenta&ccedil;&atilde;o</b></td>";
        $html.="</tr>";

        if ($parental == 1) {
            $paren = 'x';
        }
        if ($oral == 1) {
            $ora = 'x';
        }
        if ($sne == 1) {
            $Sn = 'x';
        }
        if ($gtm_sonda_gastro == 1) {
            $gt = 'x';
            $sond = '';
            $gtm_numero = "N&ordm;: {$gtm_num}  Profundidade: {$gtm_num_profundidade}";
            $quant_sonda_ = '';
            $ultima_substituicao_button = "<td><h1 style='font-size:12px;'>Ultima Substituicao: {$ultima_substituicao}</h1></td>";
            $ultima_substituicao_sonda = null;
        } elseif ($gtm_sonda_gastro == 2) {
            $sond = 'x';
            $quant_sonda_ = "N&ordm;: {$quant_sonda} FR";
            $gt = '';
            $gtm_numero = '';
            $ultima_substituicao_button = null;
            $ultima_substituicao_sonda = "<td><h1 style='font-size:12px;'>Ultima Substituicao: {$ultima_substituicao}</h1></td>";
        }

        if ($dieta_sn == 's') {
            $html.= "<tr><td colspan = '5'><h1 style='font-size:12px;'> Dieta Zero (x) Sim  ( )N&atilde;o </h1></td></tr>";
            $html.= "<tr><td colspan = '5'><h1 style='font-size:12px;'> ($paren) Parenteral </h1></td></tr>";
        } elseif ($dieta_sn == 'n') {
            $html.= "<tr><td colspan = '5'><h1 style='font-size:12px;'> Dieta Zero ( ) Sim  (x)N&atilde;o </h1></td></tr>";
            $html.="<tr><td colspan = '5'><h1 style='font-size:12px;'> ($ora) Oral </h1></td></tr>";
            $html.="<tr><td colspan = '5'><h1 style='font-size:12px;'> ($sn) SNE    N&ordm;: {$sne_num} </h1></td></tr>";
            $html.="<tr><td bgcolor='#D3D3D3' align='center'><h1 style='font-size:12px;'>GTM: </h1></td></tr>";
            $html.="<tr><td colspan = '4'><h1 style='font-size:12px;'>($gt) Button {$gtm_numero}</h1></td>{$ultima_substituicao_button}</tr>";
            $html.="<tr><td colspan = '4'><h1 style='font-size:12px;'>($sond) Sonda Gastro N&ordm;: {$quant_sonda}</h1></td>{$ultima_substituicao_sonda}</tr>";
        }

        ///DIETA
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Dieta</b></h1></td>";
        $html.="</tr>";

        if ($sistema_aberto == 1) {
            $siste_a = 'x';
        }
        if ($sistema_fechado == 1) {
            $siste_f = 'x';
        }
        if ($dieta_npt == 1) {
            $dieta_np = 'x';
        }
        if ($dieta_npp == 1) {
            $np = 'x';
        }
        if ($dieta_artesanal == 1) {
            $da = 'x';
        }
        if ($dieta_sn == 's') {
            $html.="<tr><td colspan='5'><h1 style='font-size:12px;'>  ($dieta_np) &nbsp;&nbsp;  NPT 	
			&nbsp;&nbsp; Nome: {$desc_npt} Vaz&atilde;o: {$vazao_npt} ML/H  </h1></tr></td>";
            $html.="<tr><td ><h1 style='font-size:12px;'> ($np)  &nbsp;&nbsp; NPP &nbsp;&nbsp;  Nome: {$desc_npp}  Vaz&atilde;o: {$vazao_npp} ML/H  </h1></tr></td>";
        } elseif ($dieta_sn == 'n') {
            $html.="<tr><td colspan='5'><h1 style='font-size:12px;'> ($siste_a) Sistema aberto 	&nbsp;&nbsp;&nbsp;&nbsp;	Nome: {$nome_sistema_aberto} &nbsp;&nbsp;&nbsp;&nbsp;   Uso: {$uso_sistema_aberto}</h1> </td></tr>";
            $html.="<tr><td colspan='5'><h1 style='font-size:12px;'> ($siste_f) Sistema fechado 	&nbsp;&nbsp;&nbsp;&nbsp;	Nome: {$quant_sistema_fechado}  &nbsp;&nbsp;&nbsp;&nbsp;  Vaz&atilde;o: {$vazao} ml/L </h1> </td></tr>";
            $html.= "<tr><td><h1 style='font-size:12px;'> ($da) Dieta Artesanal </h1></tr></td>";
        }

        //Suplemento
        if ($suplemento_sn == 's') {
            $local_s = 'x';
        }
        if ($suplemento_sn == 'n') {
            $local_n = 'x';
        }
        if ($dieta_sn == 'n') {
            $html.= "<tr><td colspan='5'></td></tr><tr>";
            $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Suplemento:  ({$local_s}) Sim    ({$local_n}) N&atilde;o </b></td>";
            $html.="</tr>";

            $sql4 = "SELECT * FROM evolucao_enf_suplemento WHERE EVOLUCAO_ENF_ID = '{$id_aval}'";
            $result4 = mysql_query($sql4);
            while ($row4 = mysql_fetch_array($result4)) {
                $local_suplemento = $row4['NOME'];
                $frequencia_suplemento = $row4['FREQUENCIA'];

                $html.= "<tr><td colspan='5'><h1 style='font-size:12px;'> Nome:  {$local_suplemento}  &nbsp;&nbsp;&nbsp;&nbsp;  Frequ&ecirc;ncia: {$frequencia_suplemento}  </h1></td></tr>";
            }
        }
        //avaliacao genital
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Avalia&ccedil;&atilde;o Genital</b></td>";
        $html.="</tr>";

        if ($integra_genital == 1) {
            $int_sim = 'x';
        }
        if ($integra_genital == 2) {
            $int_nao = 'x';
        }
        if ($secrecao == 1) {
            $secre_sim = 'x';
        }
        if ($secrecao == 2) {
            $secre_nao = 'x';
        }
        if ($sangramento == 1) {
            $sang_sim = 'x';
        }
        if ($sangramento == 2) {
            $sang_nao = 'x';
        }
        if ($prurido == 1) {
            $pru_sim = 'x';
        }
        if ($prurido == 2) {
            $pru_nao = 'x';
        }
        if ($edema == 1) {
            $ede_sim = 'x';
        }
        if ($edema == 2) {
            $ede_nao = 'x';
        }

        $html.= "<tr><td colspan='5'><h1 style='font-size:12px;'> &Iacute;ntegra: ($int_sim)Sim  	($int_nao) N&atilde;o </h1></td></tr>";
        $html.= "<tr><td ><h1 style='font-size:12px;'> Secre&ccedil;&atilde;o: ($secre_sim) Sim ($secre_nao) N&atilde;o  </h1></td></tr>";
        $html.= "<tr><td ><h1 style='font-size:12px;'> Sangramento: ($sang_sim) Sim ($sang_nao) N&atilde;o </h1></td></tr>";
        $html.= "<tr><td ><h1 style='font-size:12px;'> Prurido: ($pru_sim) Sim  ($pru_nao) N&atilde;o  </h1></td></tr>";
        $html.= "<tr><td ><h1 style='font-size:12px;'> Edema: ($ede_sim) Sim ($ede_nao) N&atilde;o  </h1></td></tr>";

        //Avaliacao de Membros Superiores
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Avalia&ccedil;&atilde;o de membros Superiores</b></td>";
        $html.="</tr>";

        if ($edema_mmss == 1) {
            $edema_sim = 'x';
            $edema_nao = '';
        }
        if ($edema_mmss == 2) {
            $edema_nao = 'x';
            $edema_sim = '';
        }
        if ($deformidades_mmss == 1) {
            $deformidades_sim = 'x';
            $deformidades_nao = '';
        }
        if ($deformidades_mmss == 2) {
            $deformidades_nao = 'x';
            $deformidades_sim = '';
        }
        if ($lesoes_mmss == 1) {
            $lesoes_sim = 'x';
            $lesoes_nao = '';
        }
        if ($lesoes_mmss == 2) {
            $lesoes_nao = 'x';
            $lesoes_sim = '';
        }

        $html.= "<tr><td colspan='5'><h1 style='font-size:12px;'> Edema: ($edema_sim)Sim  	($edema_nao) N&atilde;o </h1></td></tr>";
        $html.= "<tr><td ><h1 style='font-size:12px;'> Deformidade: ($deformidades_sim) Sim ($deformidades_nao) N&atilde;o  </h1></td></tr>";
        $html.= "<tr><td ><h1 style='font-size:12px;'> Les&otilde;es: ($lesoes_sim) Sim ($lesoes_nao) N&atilde;o </h1></td></tr>";


        //Avaliacao de Membros Inferiores
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Avalia&ccedil;&atilde;o de membros Inferiores</b></td>";
        $html.="</tr>";

        if ($edema_mmii == 1) {
            $edemaii_sim = 'x';
            $edemaii_nao = '';
        }
        if ($edema_mmii == 2) {
            $edemaii_nao = 'x';
            $edemaii_sim = '';
        }
        if ($varizes_mmii == 1) {
            $varizesii_sim = 'x';
            $varizesii_nao = '';
        }
        if ($varizes_mmii == 2) {
            $varizesii_nao = 'x';
            $varizesii_sim = '';
        }
        if ($deformidades_mmii == 1) {
            $deformidadesii_sim = 'x';
            $deformidadesii_nao = '';
        }
        if ($deformidades_mmii == 2) {
            $deformidadesii_nao = 'x';
            $deformidadesii_sim = '';
        }
        if ($lesoes_mmii == 1) {
            $lesoesii_sim = 'x';
            $lesoesii_nao = '';
        }
        if ($lesoes_mmii == 2) {
            $lesoesii_nao = 'x';
            $lesoesii_sim = '';
        }

        $html.= "<tr><td colspan='5'><h1 style='font-size:12px;'> Edema: ($edemaii_sim)Sim  	($edemaii_nao) N&atilde;o </h1></td></tr>";
        $html.= "<tr><td ><h1 style='font-size:12px;'> Varizes: ($varizesii_sim) Sim  ($varizesii_nao) N&atilde;o  </h1></td></tr>";
        $html.= "<tr><td ><h1 style='font-size:12px;'> Deformidade: ($deformidadesii_sim) Sim ($deformidadesii_nao) N&atilde;o  </h1></td></tr>";
        $html.= "<tr><td ><h1 style='font-size:12px;'> Les&otilde;es: ($lesoesii_sim) Sim ($lesoesii_nao) N&atilde;o </h1></td></tr>";


        //Extremidades
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Extremidades</b></td>";
        $html.="</tr>";

        if ($aquecidas_ext == 1) {
            $aquecida = 'x';
        } else {
            $aquecida = '';
        }
        if ($oxigenadas_ext == 1) {
            $oxigenada = 'x';
        } else {
            $oxigenada = '';
        }
        if ($cianoticas_ext == 1) {
            $cianotica = 'x';
        } else {
            $cianotica = '';
        }
        if ($frias_ext == 1) {
            $frias = 'x';
        } else {
            $frias = '';
        }
        if ($pulso_presente == 1) {
            $pulso = 'x';
        } else {
            $pulso = '';
        }

        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$aquecida})Aquecidas &nbsp;&nbsp;&nbsp;({$oxigenada})Oxigenadas  &nbsp;&nbsp;&nbsp;({$frias})Frias &nbsp;&nbsp;&nbsp;({$cianotica})Cian&oacute;ticas &nbsp;&nbsp;&nbsp;({$pulso})Pulso Presente</h1></td></tr>";

        // DISPOSITIVO INTRAVENOSO
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        if ($dispositivos_intravenosos == 2) {
            $dispositivos = "Sim";
        } else {
            $dispositivos = "N&atilde;o";
        }
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Dispositivos Intravenosos - {$dispositivos}</b></td>";
        $html.="</tr>";
        if ($dispositivos_intravenosos == 2) {
            if ($cvc == 1) {
                $cvc_teste = 'x';
            }
            if ($local_uno == 1) {
                $uno = 'x';
            }
            if ($local_duplo == 2) {
                $dup = 'x';
            }
            if ($local_triplo == 3) {
                $trip = 'x';
            }
            if ($avp == 1) {
                $avp_s = 'x';
            }
            if ($port == 1) {
                $port_h = 'x';
            }
            $picc_x = $picc == 1 ? 'x' : '';
            $html.="<tr><td colspan = '5'><h1 style='font-size:12px;'> <b> ($cvc_teste) CVC</b></td></tr>";
            if ($cvc == 1) {
                $html.= "<tr><td colspan = '5'><h1 style='font-size:12px;'> ($uno) Uno Lumen Local:   {$local_uno}</h1></td></tr>";
                $html.= "<tr><td colspan = '5'><h1 style='font-size:12px;'> ($dup) Duplo Lumen  Local: {$local_duplo} </h1></td></tr>";
                $html.="<tr><td colspan = '5'><h1 style='font-size:12px;'> ($trip) Triplo Lumen Local:  {$local_triplo} </h1></td></tr>";
            }
            $html.="<tr><td ><h1 style='font-size:12px;'> ($avp_s)AVP  Local: {$avp_local} </h1></td></tr>";
            $html.= "<tr><td><h1 style='font-size:12px;'> ($port_h) Port-O-Cath N&ordm;:  {$quant_port}    Local: {$port_local} </h1></td></tr>";
            $html.="<tr><td ><h1 style='font-size:12px;'> ($picc_x)Cateter de PICC  Local: {$picc_local} </h1></td></tr>";
            $html.="<tr><td colspan = '5'><h1 style='font-size:12px;'> Outros: {$outros_intra} </h1></td></tr>";
        }

        //Funcionamento GastroIntestinal
        if ($alt_gastro == 's') {
            $alteracoes = 'Sim';
        } elseif ($alt_gastro == 'n') {
            $alteracoes = 'N&atilde;o';
        }
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Altera&ccedil;&otilde;es Gastrointestinais - </b>{$alteracoes}</td>";
        $html.="</tr>";
        if ($alt_gastro == 's') {
            if ($diarreia_obstipacao == 1) {
                $diarreia = 'x';
                $episodios_diarreia = "{$diarreia_episodios} epis&oacute;dios";
                $episodios_obstipacao = '';
                $obstipacao = '';
            }
            if ($diarreia_obstipacao == 2) {
                $obstipacao = 'x';
                $episodios_obstipacao = "{$obstipacao_episodios} epis&oacute;dios";
                $episodios_diarreia = '';
                $diarreia = '';
            }
            if ($nauseas_gastro == 1) {
                $nauseas = 'x';
            } else {
                $nauseas = '';
            }
            if ($vomitos_gastro == 1) {
                $vomitos = 'x';
                $episodios_vomito = "{$vomito_episodios} epis&oacute;dios";
            } else {
                $vomitos = '';
                $episodios_vomito = '';
            }
            $constipacao = $constipacao_gastro == 1 ? 'x':'';
            $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$diarreia}) Diarr&eacute;ia &nbsp;&nbsp;&nbsp;  {$episodios_diarreia}</h1></td></tr>";
            $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$obstipacao}) Obstipa&ccedil;&atilde;o &nbsp;&nbsp;&nbsp; {$episodios_obstipacao} </h1></td></tr>";
            $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$nauseas}) N&aacute;useas &nbsp;&nbsp;&nbsp;</h1></td></tr>";
            $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$vomitos}) V&ocirc;mitos &nbsp;&nbsp;&nbsp; {$episodios_vomito}</h1></td></tr>";
            $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$constipacao}) Constipação &nbsp;&nbsp;&nbsp;</h1></td></tr>";

        }
        //Funcionamento urinario
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Funcionamento Urin&aacute;rio</b></td>";
        $html.="</tr>";

        if ($normal_urinario == 1) {
            $normal = 'x';
        } else {
            $normal = '';
        }
        if ($anuria_urinario == 1) {
            $anuria = 'x';
        } else {
            $anuria = '';
        }
        if ($incontinencia_urinario == 1) {
            $incontinencia = 'x';
        } else {
            $incontinencia = '';
        }
        if ($hematuria_urinario == 1) {
            $hematuria = 'x';
        } else {
            $hematuria = '';
        }
        if ($piuria_urinario == 1) {
            $piuria = 'x';
        } else {
            $piuria = '';
        }
        if ($oliguria_urinario == 1) {
            $oliguria = 'x';
        } else {
            $oliguria = '';
        }
        if ($odor_urinario == 1) {
            $odor = 'x';
        } else {
            $odor = '';
        }
        if ($cateterismo_int == 1) {
            $intermitente = 'x';
        } else {
            $intermitente = '';
        }

        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$normal})Normal &nbsp;&nbsp;&nbsp;({$anuria})An&uacute;ria  &nbsp;&nbsp;&nbsp;({$incontinencia})Incontin&ecirc;ncia &nbsp;&nbsp;&nbsp;({$hematuria})Hemat&uacute;ria &nbsp;&nbsp;&nbsp;</h1></td></tr>";
        $html.= "<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$piuria})Pi&uacute;ria &nbsp;&nbsp;&nbsp;({$oliguria})Olig&uacute;ria  &nbsp;&nbsp;&nbsp;({$odor})Odor &nbsp;&nbsp;&nbsp;({$intermitente})Cateterismo Intermitente</h1></td></tr>";

        //Dispositivo Excrecao
        if ($eliminaciones == '2') {
            $eliminacoes = 'Sim';
        } elseif ($eliminaciones == '1') {
            $eliminacoes = 'N&atilde;o';
        }
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Dispositivos para Elimina&ccedil;&otilde;es - </b>{$eliminacoes}</td>";
        $html.="</tr>";

        if ($fralda == 1) {
            $fralda_uso = 'x';
            $fralda_quant = "{$fralda_dia} DIA";
            switch ($tamanho_fralda) {
                case 1:
                    $f_tamanho = 'P';
                    break;
                case 2:
                    $f_tamanho = 'M';
                    break;
                case 3:
                    $f_tamanho = 'G';
                    break;
                case 4:
                    $f_tamanho = 'XG';
                default:
                    $f_tamanho = '';
                    break;
            }
            $fralda_tamanho = "Tamanho: {$f_tamanho}";
        }
        if ($urinario == 1) {
            $urinario_disp = 'x';
            $urinario_quantidade = "N&ordm; {$quant_urinario}";
        } else {
            $urinario_disp = '';
            $urinario_quantidade = '';
        }
        if ($sonda == 1) {
            $sonda_ = 'x';
            $sonda_quant = "N&ordm; {$quant_foley}";
        } else {
            $sonda_ = '';
            $sonda_quant = '';
        }
        if ($sonda_alivio == 1) {
            $alivio = 'x';
            $alivio_quant = "N&ordm; {$quant_alivio}";
        } else {
            $alivio = '';
            $alivio_quant = '';
        }
        if ($colostomia == 1) {
            $colostomia_placa = 'x';
            $colostomia_quant = "N&ordm; {$quant_colostomia}";
        } else {
            $colostomia_placa = '';
            $colostomia_quant = '';
        }
        if ($iliostomia == 1) {
            $iliostomia_placa = 'x';
            $iliostomia_quant = "N&ordm; {$quant_iliostomia}";
        } else {
            $iliostomia_placa = '';
            $iliostomia_quant = '';
        }
        if ($cistostomia == 1) {
            $cistostomia_ = 'x';
            $cistostomia_quant = "N&ordm; {$quant_cistosmia}";
        } else {
            $cistostomia_ = '';
            $cistostomia_quant = '';
        }
        $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$fralda_uso}) Fralda Unidade &nbsp;&nbsp;&nbsp;  {$fralda_quant} &nbsp;&nbsp;&nbsp;{$fralda_tamanho}</h1></td></tr>";
        $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$urinario_disp}) Dispositivo Urin&aacute;rio &nbsp;&nbsp;&nbsp;  {$urinario_quantidade}</h1></td></tr>";
        $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$sonda_}) Sonda de Foley &nbsp;&nbsp;&nbsp; {$sonda_quant} </h1></td></tr>";
        $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$alivio}) Al&iacute;vio &nbsp;&nbsp;&nbsp; {$alivio_quant}</h1></td></tr>";
        $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$colostomia_placa}) Colostomia Placa/Bolsa &nbsp;&nbsp;&nbsp; {$colostomia_quant}</h1></td></tr>";
        $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$iliostomia_placa}) Iolostomia Placa/Bolsa &nbsp;&nbsp;&nbsp; {$iliostomia_quant}</h1></td></tr>";
        $html.="<tr> <td colspan='5' ><h1 style='font-size:12px;'>({$cistostomia_}) Cistostomia &nbsp;&nbsp;&nbsp; {$cistostomia_quant}</h1></td></tr>";

        /* DRENOS */
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Drenos</b></td>";
        $html.="</tr>";

        if ($dreno_sn == 's') {
            $dreno_s = 'x';
        }
        if ($dreno_sn == 'n') {
            $dreno_n = 'x';
        }
        $html.= "<tr><td colspan='5'><h1 style='font-size:12px;'> Dreno:  ({$dreno_s}) Sim    ({$dreno_n}) N&atilde;o  </h1></td></tr>";

        while ($row7 = mysql_fetch_array($result7)) {
            $tipo_dreno = $row7['DRENO'];
            $local_dreno = $row7['LOCAL'];

            $html.= "<tr><td colspan='5'><h1 style='font-size:12px;'> Tipo: {$tipo_dreno}   Local:  {$local_dreno} </h1></td></tr>";
        }


        //FERIDAS

            $numero_ferida = $numero_imagem = 0;
            if ($feridas_sn == 's') {
                $html .= "</table><table style='width:100%;' >";
                $x = "Sim";
                $html .= "<tr style='background-color:#D3D3D3;'><td colspan='5'><h1 style='font-size:12px;'><b>Feridas - $x</b></td></tr>";

                $sql = "SELECT
                    quadro_feridas_enfermagem.*, full_path, path, quadro_feridas_enf_local.LOCAL as descricao_local_ferida,
                    quadro_feridas_enfermagem.ID as idQuadroFeridas
                FROM
                quadro_feridas_enfermagem
                  LEFT JOIN files_uploaded ON files_uploaded.id = quadro_feridas_enfermagem.FILE_UPLOADED_ID
                  LEFT JOIN quadro_feridas_enf_local ON quadro_feridas_enf_local.id = quadro_feridas_enfermagem.LOCAL
                WHERE
                EVOLUCAO_ENF_ID = '{$fichaEvolucaoEnfermagem}'";
                $result = mysql_query($sql);
                $total_feridas = mysql_num_rows($result);
                $imagem_feridas = '';
                while($row = mysql_fetch_array($result)) {
                    ++$numero_ferida;

                    $local = $row['LOCAL'];
                    $descricaoLocalFerida = $row['descricao_local_ferida'];
                    $lado = $row['LADO'];
                    $tipo = $row['TIPO'];
                    $cobertura = $row['COBERTURA'];
                    $tamanho = $row['TAMANHO'];
                    $profundidade = $row['TAMANHO'];
                    $tecido = $row['TECIDO'];
                    $grau = $row['GRAU'];
                    $humidade = $row['HUMIDADE'];
                    $exsudato = $row['EXSUDATO'];
                    $contaminacao = $row['CONTAMINACAO'];
                    $pele = $row['PELE'];
                    $tempo = $row['TEMPO'];
                    $odor = $row['ODOR'];
                    $periodo = $row['PERIODO'];
                    $status_ferida = $row['STATUS_FERIDA'];
                    $observacao = $row['observacao'];

                    $imagem_ferida = 'SEM IMAGEM';
                    if (isset($row['full_path']) && !empty($row['full_path'])) {
                      $feridaImagemSrc = App\Helpers\Storage::getImageUrl($row['path']);
                      $imagem_referencia = $descricaoLocalFerida;
                      $imagem_feridas .= <<<IMAGEM_FERIDA
<p>
    <p style="display: block; font-size: 14px;">{$imagem_referencia}</p>
    <p><img style="display: block;" src="{$feridaImagemSrc}" height="400"></img></p>
</p>
<br/>
IMAGEM_FERIDA;
                    }


                $sql_local = "SELECT
                    LOCAL
                FROM
                    quadro_feridas_enf_local
                WHERE
                    ID = $local
                ";
                $result_local = mysql_query($sql_local);
                $local_ferida = mysql_result($result_local, 0);

                    $lado_ferida = '';
                    if($lado == 1){
                        $lado_ferida = 'DIREITO';
                    }else if($lado == 2){
                        $lado_ferida = 'ESQUERDO';
                    }
                $sql_tipo = "SELECT
                    TIPO
                FROM
                    quadro_feridas_enf_tipo
                WHERE
                    ID = $tipo
                ";
                $result_tipo = mysql_query($sql_tipo);
                $tipo_ferida = mysql_result($result_tipo,0);

                $sql_cobertura = "SELECT
                    TIPO
                FROM
                    quadro_feridas_enf_lesao as q inner join
                    quadro_feridas_enfermagem_cobertura as qf  on q.ID = qf.cobertura_id


                WHERE
                   qf.quadro_feridas_enfermagem_id = {$row['idQuadroFeridas']}
                ";

                    $cobertura_ferida = '';
                $result_cobertura = mysql_query($sql_cobertura);
                    while($rowCobertura = mysql_fetch_array($result_cobertura)) {
                        $cobertura_ferida .=" | ".$rowCobertura['TIPO'];
                    }



                $sql_tamanho = "SELECT
                    TAMANHO
                FROM
                    quadro_feridas_enf_tamanho
                WHERE
                    ID = $tamanho
                ";
                $result_tamanho = mysql_query($sql_tamanho);
                $tamanho_ferida = mysql_result($result_tamanho, 0);

                $sql_profundidade = "SELECT
                    PROFUNDIDADE
                FROM
                    quadro_feridas_enf_profundidade
                WHERE
                    ID = $profundidade
                ";
                $result_profundidade = mysql_query($sql_profundidade);
                $profundidade_ferida = mysql_result($result_profundidade, 0);

                $sql_tecido = "SELECT
                    TECIDO
                FROM
                    quadro_feridas_enf_tecido
                WHERE
                    ID = $tecido
                ";
                $result_tecido = mysql_query($sql_tecido);
                $tecido_ferida = mysql_result($result_tecido, 0);

                $sql_grau = "SELECT
                    GRAU
                FROM
                    quadro_feridas_enf_grau
                WHERE
                    ID = $grau
                ";
                $result_grau = mysql_query($sql_grau);
                $grau_ferida = mysql_result($result_grau, 0);

                $sql_humidade = "SELECT
                    humidade
                FROM
                    quadro_feridas_enf_humidade
                WHERE
                    ID = $humidade
                ";
                $result_humidade = mysql_query($sql_humidade);
                $humidade_ferida = mysql_result($result_humidade, 0);

                $sql_exsudato = "SELECT
                    exsudato
                FROM
                    quadro_feridas_enf_exsudato
                WHERE
                    ID = $exsudato
                ";
                $result_exsudato = mysql_query($sql_exsudato);
                $exsudato_ferida = mysql_result($result_exsudato, 0);

                $sql_contaminacao = "SELECT
                    contaminacao
                FROM
                    quadro_feridas_enf_contaminacao
                WHERE
                    ID = $contaminacao
                ";
                $result_contaminacao = mysql_query($sql_contaminacao);
                $contaminacao_ferida = mysql_result($result_contaminacao, 0);

                $sql_pele = "SELECT
                    pele
                FROM
                    quadro_feridas_enf_pele
                WHERE
                    ID = $pele
                ";
                $result_pele = mysql_query($sql_pele);
                $pele_ferida = mysql_result($result_pele, 0);

                $sql_tempo = "SELECT
                    tempo
                FROM
                    quadro_feridas_enf_tempo
                WHERE
                    ID = $tempo
                ";
                $result_tempo = mysql_query($sql_tempo);
                $tempo_ferida = mysql_result($result_tempo, 0);

                $sql_periodo = "SELECT
                    PERIODO_TROCA
                FROM
                    quadro_feridas_enf_periodo
                WHERE
                    ID = $periodo
                ";
                $result_periodo = mysql_query($sql_periodo);
                $periodo_ferida = mysql_result($result_periodo, 0);

                $sql_status = " SELECT
                                    status_ferida
                                FROM
                                    quadro_feridas_enf_status
                                WHERE
                                    ID = $status_ferida";
                $result_status = mysql_query($sql_status);
                $status_ferida = mysql_result($result_status, 0);

                $linha_ferida = "";

                if ($numero_ferida > 1) {
                    $linha_ferida = "<tr><td colspan='5'><hr /></td></tr>";
                }
                $html .= "{$linha_ferida}";

                $odor = $odor == 1 ? "NÃO" : "FETIDO";

                if($total_feridas > 1){
                    $html .= "<tr><td colspan='5' ><h1 style='font-size:12px;'><b>FERIDA $numero_ferida</b></td></tr>";
                }

                $html .="
                <tr>
                    <td><h1 style='font-size:12px;'>LOCAL:</h1></td>
                    <td><h1 style='font-size:12px;'>{$local_ferida}</h1></td>
                </tr>
                <tr>
                    <td><h1 style='font-size:12px;'>OBSERVAÇÃO:</h1></td>
                    <td><h1 style='font-size:12px;'>{$observacao}</h1></td>
                </tr>
                <tr>
                    <td><h1 style='font-size:12px;'>LADO:</h1></td>
                    <td><h1 style='font-size:12px;'>{$lado_ferida}</h1></td>
                </tr>
                <tr>
                    <td><h1 style='font-size:12px;'>TIPO DE LES&Atilde;O:</h1></td>
                    <td><h1 style='font-size:12px;'>{$tipo_ferida}</h1></td>
                </tr>
                <tr>
                    <td><h1 style='font-size:12px;'>COBERTURA ATUAL UTILIZADA:</h1></td>
                    <td><h1 style='font-size:12px;'>{$cobertura_ferida}</h1></td>
                </tr>
                <tr>
                    <td><h1 style='font-size:12px;'>TAMANHO:</h1></td>
                    <td><h1 style='font-size:12px;'>{$tamanho_ferida}</h1></td>
                </tr>
                <tr>
                    <td><h1 style='font-size:12px;'>PROFUNDIDADE:</h1></td>
                    <td><h1 style='font-size:12px;'>{$profundidade_ferida}</h1></td>
                </tr>
                <tr>
                    <td><h1 style='font-size:12px;'>TIPO DE TECIDO:</h1></td>
                    <td><h1 style='font-size:12px;'>{$tecido_ferida}</h1></td>
                </tr>
                <tr>
                    <td><h1 style='font-size:12px;'>GRAU DA LES&Atilde;O:</td>
                    <td><h1 style='font-size:12px;'>{$grau_ferida}</td>
                </tr>
                <tr>
                    <td><h1 style='font-size:12px;'>GRAU DE HUMIDADE:</h1></td>
                    <td><h1 style='font-size:12px;'>{$humidade_ferida}</h1></td>
                </tr>
                <tr>
                    <td><h1 style='font-size:12px;'>TIPO DE EXSUDATO:</h1></td>
                    <td><h1 style='font-size:12px;'>{$exsudato_ferida}</h1></td>
                </tr>
                <tr>
                    <td><h1 style='font-size:12px;'>GRAU DE CONTAMINA&Ccedil;&Atilde;O:</h1></td>
                    <td><h1 style='font-size:12px;'>{$contaminacao_ferida}</h1></td>
                </tr>
                <tr>
                    <td><h1 style='font-size:12px;'>PELE PERI-LES&Atilde;O:</h1></td>
                    <td><h1 style='font-size:12px;'>{$pele_ferida}</h1></td>
                </tr>
                <tr>
                    <td><h1 style='font-size:12px;'>TEMPO DA LES&Atilde;O:</h1></td>
                    <td><h1 style='font-size:12px;'>{$tempo_ferida}</h1></td>
                </tr>
                <tr>
                    <td><h1 style='font-size:12px;'>ODOR:</h1></td>
                    <td><h1 style='font-size:12px;'>$odor</h1></td>
                </tr>
                <tr>
                    <td><h1 style='font-size:12px;'>".htmlentities("PERÍODO DE TROCA")." DA COBERTURA EM USO:</h1></td>
                    <td><h1 style='font-size:12px;'>$periodo_ferida</h1></td>
                </tr>
                <tr>
                    <td><h1 style='font-size:12px;'>STATUS DA FERIDA:</td>
                    <td><h1 style='font-size:12px;'>{$status_ferida}</td>
                </tr>
                <tr>
                    <td><h1 style='font-size:12px;'>IMAGEM DA FERIDA:</h1></td>
                    <td><h1 style='font-size:12px;'>EM ANEXO</h1></td>
                </tr>
                ";
            }
            }else{
                $x = "Não";
                $html .= "<tr><td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Feridas - $x</b></h1></td></tr>";
            }
            $html .= "</table>";

        /* SINAIS VITAIS */
        $html .= "<table style='width:100%;' >";
        $html.= "<tr><td colspan='5'></td></tr><tr>";
        $html.= "<td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Sinais vitais</b></h1></td>";
        $html.="</tr>";

        $html.= "<tr><td colspan = '5'><h1 style='font-size:12px;'><b>PA Sist&oacute;lica: {$pa_sistolica}MMHG</b></h1></td></tr>";
        $html.= "<tr><td><h1 style='font-size:12px;'><b>PA Dist&oacute;lica: {$pa_diastolica} MMHG</b></h1></td></tr>";
        $html.= "<tr><td><h1 style='font-size:12px;'><b>FC: {$fc} BPM</b></h1></td></tr>";
        $html.= "<tr><td><h1 style='font-size:12px;'><b>FR: {$fr} INC/MIN</b></h1></td></tr>";
        $html.= "<tr><td><h1 style='font-size:12px;'><b>SPO&sup2;:  {$spo2} % </b></h1></td></tr>";
        $html.= "<tr><td><h1 style='font-size:12px;'><b>Temperatura: {$temperatura} &deg;C</b></h1></td></tr>";

        //PAD ENFERMAGEM
        $html.= "<tr><td colspan='5'></td></tr>";
        $html.= "</table>";
        $html.= "<pagebreak />";
        $html.= "<table style='width:100%;' >";
        $html.= "<tr><td colspan='5' bgcolor='#D3D3D3'><h1 style='font-size:12px;'><b>Pad Enfermagem</b></td>";
        $html.="</tr>";

        $html.= "<tr><td colspan='5'><h1 style='font-size:12px;'> {$pad} </h1></td></tr>";
        $html.= "</table>";

        if (! empty($imagem_feridas))
          $html.= <<<ANEXOS
<pagebreak />
<table style='width:100%;' >
<tr>
  <td>
    <h4>ANEXOS</h4><br/>
    <p>Feridas</p><br/>
    {$imagem_feridas}
  </td>
</tr>
</table>
ANEXOS;
        
        $paginas [] = $html.="</form>";
        $mpdf = new mPDF('pt', 'A4', 20);
        $mpdf->SetHeader('página {PAGENO} de {nbpg}');
        $ano = date("Y");
        $mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']}"." &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));
        $mpdf->WriteHTML("<html><body>");
        $flag = false;
        $mpdf->showImageErrors = false;
        foreach ($paginas as $pag) {
            if ($flag)
                $mpdf->WriteHTML("<formfeed>");
            $mpdf->WriteHTML($pag);
            $flag = true;
        }
        $mpdf->WriteHTML("</body></html>");
        $mpdf->Output('prescricao.pdf', 'I');
        exit;

    }


}

$p = new Paciente_imprimir_ficha();
$p->detalhes_evolucao_enfermagem($_GET['id'], $_GET['id_a'], $_GET['empresa']);
