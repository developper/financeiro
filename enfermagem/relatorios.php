<?php

$id = session_id();
if (empty($id))
    session_start();

include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');
require_once('paciente_enf.php');
require_once('enfermagem.php');
require __DIR__ . '/../vendor/autoload.php';

use \App\Models\Administracao\Paciente as ModelPaciente;

class Relatorios {


    public function listar_alta_enfermagem($id) {

        $sql = "
		SELECT
                    DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
                    DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
                    DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
                    u.nome as user,
                    u.tipo as utipo,
                    u.conselho_regional,
                    u.idUsuarios,
                    r.ID as id_rel,
                    p.nome as paciente
		FROM
                    relatorio_alta_enf as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios) LEFT JOIN
                    clientes as p ON (r.PACIENTE_ID = p.idClientes)	 
		WHERE
                    r.PACIENTE_ID = '{$id}'
		ORDER BY
                    r.DATA DESC";
        $r = mysql_query($sql);
        $flag = true;
        $existe_prescricao = 0;
        $penultimo = 0; //valor falso
        //echo "<div>";

        $sql1 = "
            SELECT
                max(ID)
            FROM
                relatorio_alta_enf
            WHERE
                PACIENTE_ID= {$id}
            ";
        $result1 = mysql_query($sql1);
        $ultima_prorrogacao = mysql_result($result1, 0);

        $sql2 = "select DATE_FORMAT(data,'%Y-%m-%d %H:%i:%s') from relatorio_alta_enf where id = '{$ultima_prorrogacao}' ";

        $result2 = mysql_query($sql2);
        $data_relatorio = mysql_result($result2, 0);
        $data_atual = date('Y/m/d');
        $prazoEditar = date('Y/m/d', strtotime("+2 days", strtotime($data_relatorio)));

        while ($row = mysql_fetch_array($r)) {
            $compTipo = $row['conselho_regional'];

            if ($flag) {
                $p = ucwords(strtolower($row["paciente"]));
                echo "<center><h1>Alta de Enfermagem</h1></center>";
                Paciente::cabecalho($id);

                if ($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                        ['enfermagem']
                        ['enfermagem_paciente']
                        ['enfermagem_paciente_alta']
                        ['enfermagem_paciente_alta_novo'])) {
                    echo "<button id='novo_rel_alta' paciente={$id} class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
				<span class='ui-button-text'>Nova Alta de Enfermagem</span></button>";
                }

                echo "<table class='mytable' style='width:100%' >";
                echo "<thead><tr>";
                echo "<th width='35%'>Usu&aacute;rio</th>";
                echo "<th>Data</th>";
                echo "<th>Per&iacute;odo</th>";
                echo "<th width='10%'>Detalhes</th>";
                echo "<th>Op&ccedil;&otilde;es</th>";
                echo "</tr></thead>";
                $flag = false;
            }

            echo "<tr><td>{$row['user']}{$compTipo}</td>";
            echo "<td>{$row['rdata']}</td>";
            echo "<td>{$row['inicio']} AT&Eacute; {$row['fim']}</td>";
            echo "<td><a href=?view=visualizar_rel_alta&id={$id}&idr=" . $row['id_rel'] . "><img src='../utils/capmed_16x16.png' title='Detalhes prorrogacao'></td>";
            echo "<td>";

            if ($row['id_rel'] == $ultima_prorrogacao) {
                if ($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                        ['enfermagem']
                        ['enfermagem_paciente']
                        ['enfermagem_paciente_alta']
                        ['enfermagem_paciente_alta_editar'])) {
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=relatorio&opcao=1&id={$id}&idr=" . $row['id_rel'] . "&editar=0><img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para nova avalia&ccedil;&atilde;o.' border='0'></a>";
                }
                if($data_atual < $prazoEditar){
                    if ($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                            ['enfermagem']
                            ['enfermagem_paciente']
                            ['enfermagem_paciente_alta']
                            ['enfermagem_paciente_alta_editar'])) {
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=relatorio&opcao=1&id={$id}&idr=" . $row['id_rel'] . "&editar=1><img src='../utils/edit_16x16.png' title='Editar' border='0'></a>";
                    }
                }
            }

            echo "</td></tr>";
        }

        if ($flag) {
            if ($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                    ['enfermagem']
                    ['enfermagem_paciente']
                    ['enfermagem_paciente_alta']
                    ['enfermagem_paciente_alta_novo'])) {
                $this->relatorio_alta($id,null,null);
            } else {
                //echo "N&atilde;o possui Relat&oacute;rio de Prorroga&ccedil;&atilde;o.";
                echo "N&atilde;o possui Relat&oacute;rio de Alta.";
            }
            return;
        }
        echo "</table>";

        echo "</div>";
    }

///////////////////////////////FIM DO LISTAR ALTA//////////////////

    public function listar_aditiva_enfermagem($id) {

        $sql = "
		SELECT
                    DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
                    DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
                    DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
                    u.nome as user,
                    u.tipo as utipo,
                    u.conselho_regional,
                    u.idUsuarios,
                    r.ID as id_rel,
                    p.nome as paciente,
                    r.confirmado
		FROM
                    relatorio_aditivo_enf as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios) LEFT JOIN
                    clientes as p ON (r.PACIENTE_ID = p.idClientes)	 
		WHERE
                    r.PACIENTE_ID = '{$id}'
		ORDER BY
                    r.DATA DESC";
        $r = mysql_query($sql);
        $flag = true;
        $existe_prescricao = 0;
        $penultimo = 0; //valor falso
        //echo "<div>";

        $sql1 = "
            SELECT
                max(ID)
            FROM
                relatorio_aditivo_enf
            WHERE
                PACIENTE_ID= {$id}
            ";
        $result1 = mysql_query($sql1);
        $ultima_prorrogacao = mysql_result($result1, 0);

        $sql2 = "select DATE_FORMAT(data,'%Y-%m-%d %H:%i:%s') from relatorio_aditivo_enf where id = '{$ultima_prorrogacao}' ";

        $result2 = mysql_query($sql2);
        $data_relatorio = mysql_result($result2, 0);
        $data_atual = date('Y/m/d');
        $prazoEditar = date('Y/m/d', strtotime("+2 days", strtotime($data_relatorio)));

        while ($row = mysql_fetch_array($r)) {
            $compTipo = $row['conselho_regional'];

            if ($flag) {
                $p = ucwords(strtolower($row["paciente"]));
                echo "<center><h1>Relatório Aditivo de Enfermagem</h1></center>";
                Paciente::cabecalho($id);

                if (isset($_SESSION['permissoes']['enfermagem']['enfermagem_paciente']
                        ['enfermagem_paciente_aditivo']
                        ['enfermagem_paciente_aditivo_novo']) || $_SESSION['adm_user'] == 1) {
                    echo "<button id='novo_rel_aditivo' paciente={$id} class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
				<span class='ui-button-text'>Novo Relatório Aditivo de Enfermagem</span></button>";
                }

                echo "<table class='mytable' style='width:100%' >";
                echo "<thead><tr>";
                echo "<th width='35%'>Usu&aacute;rio</th>";
                echo "<th>Data</th>";
                echo "<th>Per&iacute;odo</th>";
                echo "<th>Confirmado</th>";
                echo "<th width='10%'>Detalhes</th>";
                echo "<th>Op&ccedil;&otilde;es</th>";
                echo "</tr></thead>";
                $flag = false;
            }
            $confirmado = $row['confirmado'] == 0 ? "Não":"Sim";
            echo "<tr><td>{$row['user']}{$compTipo}</td>";
            echo "<td>{$row['rdata']}</td>";
            echo "<td>{$row['inicio']} AT&Eacute; {$row['fim']}</td>";
            echo "<td>{$confirmado}</td>";
            echo "<td><a href=?view=visualizar_rel_aditivo&id={$id}&idr=" . $row['id_rel'] . "&confirmar=0><img src='../utils/capmed_16x16.png' title='Detalhes Aditivo'></td>";
            echo "<td>";

            if ($row['id_rel'] == $ultima_prorrogacao) {

                if($data_atual < $prazoEditar){
                    if($row['confirmado'] == 0){
                        if($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                                ['enfermagem']
                                ['enfermagem_paciente']
                                ['enfermagem_paciente_aditivo']
                                ['enfermagem_paciente_aditivo_editar'])) {
                            echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=relatorio&opcao=2&id={$id}&idr=" . $row['id_rel'] . "&editar=1><img src='../utils/edit_16x16.png' title='Editar' border='0'></a>";
                        }
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=visualizar_rel_aditivo&id={$id}&idr=" . $row['id_rel'] . "&confirmar=1><img src='../utils/processada_16x16.png' title='Confirmar Aditivo'>";
                    }else{
                        if($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                                ['enfermagem']
                                ['enfermagem_paciente']
                                ['enfermagem_paciente_aditivo']
                                ['enfermagem_paciente_aditivo_usar_para_nova'])) {
                            echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=relatorio&opcao=2&id={$id}&idr=" . $row['id_rel'] . "&editar=0><img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para novo relat&oacute;rio.' border='0'></a>";
                        }
                    }
                }else{
                    if($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                            ['enfermagem']
                            ['enfermagem_paciente']
                            ['enfermagem_paciente_aditivo']
                            ['enfermagem_paciente_aditivo_usar_para_nova'])) {
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=relatorio&opcao=2&id={$id}&idr=" . $row['id_rel'] . "&editar=0><img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para novo relat&oacute;rio.' border='0'></a>";
                    }
                }
            }

            echo "</tr></td>";
        }


        if ($flag) {
            if ($_SESSION['adm_user'] == 1 ||
                isset($_SESSION['permissoes']['enfermagem']['enfermagem_paciente']
                    ['enfermagem_paciente_aditivo']
                    ['enfermagem_paciente_aditivo_novo'])
            ) {
                $this->relatorio_aditivo($id,null,null);
            } else {
                echo "N&atilde;o possui Relat&oacute;rio Aditivo.";
            }
            return;
        }
        echo "</table>";

        echo "</div>";
    }

    ////////////////////////////FIM DO LISTAR ADITIVO//////////////////////////

    public function listar_prorrogacao_enfermagem($id) {

        $sql = "
		SELECT
                    DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
                    DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
                    DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
                    u.nome as user,
                    u.tipo as utipo,
                    u.conselho_regional,
                    u.idUsuarios,
                    r.ID as id_rel,
                    p.nome as paciente
		FROM
                    relatorio_prorrogacao_enf as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios) LEFT JOIN
                    clientes as p ON (r.PACIENTE_ID = p.idClientes)	 
		WHERE
                    r.PACIENTE_ID = '{$id}'
		ORDER BY
                    r.DATA DESC";
        $r = mysql_query($sql);
        $flag = true;
        $existe_prescricao = 0;
        $penultimo = 0; //valor falso
        //echo "<div>";

        $sql1 = "
            SELECT
                max(ID)
            FROM
                relatorio_prorrogacao_enf
            WHERE
                PACIENTE_ID= {$id}
            ";
        $result1 = mysql_query($sql1);
        $ultima_prorrogacao = mysql_result($result1, 0);

        $sql2 = "select DATE_FORMAT(data,'%Y-%m-%d %H:%i:%s') from relatorio_prorrogacao_enf where id = '{$ultima_prorrogacao}' ";

        $result2 = mysql_query($sql2);
        $data_relatorio = mysql_result($result2, 0);
        $data_atual = date('Y/m/d');
        $prazoEditar = date('Y/m/d', strtotime("+2 days", strtotime($data_relatorio)));

        while ($row = mysql_fetch_array($r)) {
            $compTipo = $row['conselho_regional'];

            if ($flag) {
                $p = ucwords(strtolower($row["paciente"]));
                echo "<center><h1>Prorroga&ccedil;&atilde;o de Enfermagem</h1></center>";
                Paciente::cabecalho($id);
                echo "<input id='fromRelatorio' value='prorrogacao_enfermagem' type='hidden' />";

                if ($_SESSION['adm_user'] == 1 ||
                    isset($_SESSION['permissoes']
                        ['enfermagem']
                        ['enfermagem_paciente']
                        ['enfermagem_paciente_prorrogacao']
                        ['enfermagem_paciente_prorrogacao_novo'])) {
                    echo "<button id='novo_rel_prorrogacao' paciente={$id} class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
                            <span class='ui-button-text'>Nova Prorrogação de Enfermagem</span>
                          </button>";
                }

                echo "<table class='mytable' style='width:100%' >";
                echo "<thead><tr>";
                echo "<th width='35%'>Usu&aacute;rio</th>";
                echo "<th>Data</th>";
                echo "<th>Per&iacute;odo</th>";
                echo "<th width='10%'>Detalhes</th>";
                echo "<th>Op&ccedil;&otilde;es</th>";
                echo "</tr></thead>";
                $flag = false;
            }

            echo "<tr><td>{$row['user']}{$compTipo}</td>";
            echo "<td>{$row['rdata']}</td>";
            echo "<td>{$row['inicio']} AT&Eacute; {$row['fim']}</td>";
            echo "<td><a href=?view=visualizar_rel_prorrogacao&id={$id}&idr=" . $row['id_rel'] . "><img src='../utils/capmed_16x16.png' title='Detalhes prorrogacao'></td>";

            if ($row['id_rel'] == $ultima_prorrogacao) {
                if($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                        ['enfermagem']
                        ['enfermagem_paciente']
                        ['enfermagem_paciente_prorrogacao']
                        ['enfermagem_paciente_prorrogacao_usar_para_nova'])) {
                    echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=relatorio&opcao=3&id={$id}&idr=" . $row['id_rel'] . "&editar=0><img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para novo relat&oacute;rio.' border='0'></a>";
                }
                if($data_atual < $prazoEditar){
                    if($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                            ['enfermagem']
                            ['enfermagem_paciente']
                            ['enfermagem_paciente_prorrogacao']
                            ['enfermagem_paciente_prorrogacao_editar'])) {
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=relatorio&opcao=3&id={$id}&idr=" . $row['id_rel'] . "&editar=1><img src='../utils/edit_16x16.png' title='Editar' border='0'></a></td></tr>";
                    }
                }
            }
        }

        echo "<input id='fromRelatorio' value='prorrogacao_enfermagem' type='hidden' />";

        if ($flag) {
            if ($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                    ['enfermagem']['enfermagem_paciente']
                    ['enfermagem_paciente_prorrogacao']
                    ['enfermagem_paciente_prorrogacao_novo'])) {
                $this->relatorio_prorrogacao($id);
            } else {
                echo "N&atilde;o possui Relat&oacute;rio de Prorroga&ccedil;&atilde;o.";
            }
            return;
        }
        echo "</table>";

        echo "</div>";
    }


    //////////////////////////////////////////////FIM DO LISTAR PRORROGACAO//////////////////////////

    public function listar_visita_semanal($id) {

        $sql = "
		SELECT
                    DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
                    DATE_FORMAT(r.DATA_VISITA,'%d/%m/%Y') as data_visita,                    
                    u.nome as user,
                    u.tipo as utipo,
                    u.conselho_regional,
                    u.idUsuarios,
                    r.ID as id_rel,
                    p.nome as paciente
		FROM
                    relatorio_visita_enf as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios) LEFT JOIN
                    clientes as p ON (r.PACIENTE_ID = p.idClientes)	 
		WHERE
                    r.PACIENTE_ID = '{$id}'  and EDITADO_EM is NULL
		ORDER BY
                    r.DATA DESC";
        $r = mysql_query($sql);
        $flag = true;
        $existe_prescricao = 0;
        $penultimo = 0; //valor falso
        //echo "<div>";

        $sql1 = "
            SELECT
                max(ID)
            FROM
                relatorio_visita_enf
            WHERE
                PACIENTE_ID= {$id}
            ";
        $result1 = mysql_query($sql1);
        $ultima_prorrogacao = mysql_result($result1, 0);

        $sql2 = "SELECT
                    DATE_FORMAT(DATA, '%Y-%m-%d %H:%i:%s'),
                    relatorio_visita_enf.*
                FROM
                    relatorio_visita_enf
                WHERE
                    id = '{$ultima_prorrogacao}' ";

        $result2 = mysql_query($sql2);
        $data_relatorio = mysql_result($result2, 0);
        $data_atual = date('Y/m/d');
        $prazoEditar = date('Y/m/d', strtotime("+2 days", strtotime($data_relatorio)));

        while ($row = mysql_fetch_array($r)) {
            $compTipo =$row['conselho_regional'];

            if ($flag) {
                $p = ucwords(strtolower($row["paciente"]));
                echo "<center><h1>Relat&oacute;rio de Visita Semanal de Enfermagem</h1></center>";
                Paciente::cabecalho($id);

                if ($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                        ['enfermagem']
                        ['enfermagem_paciente']
                        ['enfermagem_paciente_visita_semanal']
                        ['enfermagem_paciente_visita_semanal_novo'])) {
                    echo "<button id='novo_rel_visita' paciente={$id} class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
                            <span class='ui-button-text'>Novo Relat&oacute;rio Semanal de Visita</span>
                          </button>";
                }

                echo "<table class='mytable' style='width:100%' >";
                echo "<thead><tr>";
                echo "<th width='35%'>Usu&aacute;rio</th>";
                echo "<th>Data</th>";
                echo "<th>Data da Visita</th>";
                echo "<th width='10%'>Detalhes</th>";
                echo "<th>Op&ccedil;&otilde;es</th>";
                echo "</tr></thead>";
                $flag = false;
            }

            echo "<tr><td>{$row['user']}{$compTipo}</td>";
            echo "<td>{$row['rdata']}</td>";
            echo "<td>{$row['data_visita']}</td>";
            echo "<td><a href=?view=visualizar_rel_visita&id={$id}&idr=" . $row['id_rel'] . "><img src='../utils/capmed_16x16.png' title='Detalhes prorrogacao'></td>";
            echo "<td>";

            if ($row['id_rel'] == $ultima_prorrogacao) {
                if($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                        ['enfermagem']
                        ['enfermagem_paciente']
                        ['enfermagem_paciente_visita_semanal']
                        ['enfermagem_paciente_visita_semanal_usar_para_nova']) || $_SESSION['adm_user'] == 1) {
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=relatorio&opcao=4&id={$id}&idr=" . $row['id_rel'] . "&editar=0><img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para novo relat&oacute;rio.' border='0'></a>";
                }
                if($data_atual < $prazoEditar){
                    if($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                            ['enfermagem']
                            ['enfermagem_paciente']
                            ['enfermagem_paciente_visita_semanal']
                            ['enfermagem_paciente_visita_semanal_novo']) || $_SESSION['adm_user'] == 1) {
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=relatorio&opcao=4&id={$id}&idr=" . $row['id_rel'] . "&editar=1><img src='../utils/edit_16x16.png' title='Editar' border='0'></a>";
                    }
                }
            }

            echo "</td></tr>";
        }

        if ($flag) {
            if ($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                    ['enfermagem']
                    ['enfermagem_paciente']
                    ['enfermagem_paciente_visita_semanal']
                    ['enfermagem_paciente_visita_semanal_novo'])) {
                $this->relatorio_visita($id);
            } else {
                echo "N&atilde;o possui Relat&oacute;rio de Visita Semanal.";
            }
            return;
        }
        echo "</table>";

        echo "</div>";
    }

    public function listar_relatorio_deflagrado($id) {

        $sql = "
        SELECT
                    DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
                    DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
                    DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
                    u.nome as user,
                    u.tipo as utipo,
                    u.conselho_regional,
                    u.idUsuarios,
                    r.ID as id_rel,
                    p.nome as paciente
        FROM
                    relatorio_deflagrado_enf as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios) LEFT JOIN
                    clientes as p ON (r.PACIENTE_ID = p.idClientes)  
        WHERE
                    r.PACIENTE_ID = '{$id}'
        ORDER BY
                    r.DATA DESC";
        $r = mysql_query($sql);
        $flag = true;
        $existe_prescricao = 0;
        $penultimo = 0; //valor falso
        //echo "<div>";

        $sql1 = "
            SELECT
                max(ID)
            FROM
                relatorio_deflagrado_enf
            WHERE
                PACIENTE_ID= {$id}
            ";
        $result1 = mysql_query($sql1);
        $ultimo_relatorio = mysql_result($result1, 0);

        $sql2 = "SELECT
                    DATE_FORMAT(DATA, '%Y-%m-%d %H:%i:%s'),
                    relatorio_deflagrado_enf.*
                FROM
                    relatorio_deflagrado_enf
                WHERE
                    id = '{$ultimo_relatorio}' ";

        $result2 = mysql_query($sql2);
        $data_relatorio = mysql_result($result2, 0);
        $data_atual = date('Y/m/d');
        $prazoEditar = date('Y/m/d', strtotime("+2 days", strtotime($data_relatorio)));

        while ($row = mysql_fetch_array($r)) {
            $compTipo = $row['conselho_regional'];

            if ($flag) {
                $p = ucwords(strtolower($row["paciente"]));
                echo "<center><h1>" . htmlentities("Relatório") . " Deflagrado de Enfermagem</h1></center>";
                Paciente::cabecalho($id);

                if ($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                        ['enfermagem']
                        ['enfermagem_paciente']
                        ['enfermagem_paciente_deflagrado']
                        ['enfermagem_paciente_deflagrado_novo'])) {
                    echo "<button id='novo_rel_deflagrado' paciente={$id} class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
                            <span class='ui-button-text'>Novo " . htmlentities("Relatório") . " Deflagrado de Enfermagem</span>
                          </button>";
                }

                echo "<table class='mytable' style='width:100%' >";
                echo "<thead><tr>";
                echo "<th width='35%'>" . htmlentities("Usuário") . "</th>";
                echo "<th>Data</th>";
                echo "<th>Período</th>";
                echo "<th width='10%'>Detalhes</th>";
                echo "<th>" . htmlentities("Opções") . "</th>";
                echo "</tr></thead>";
                $flag = false;
            }

            echo "<tr><td>{$row['user']}{$compTipo}</td>";
            echo "<td>{$row['rdata']}</td>";
            echo "<td>De {$row['inicio']} até {$row['fim']}</td>";
            echo "<td><a href=?view=visualizar_rel_deflagrado&id={$id}&idr=" . $row['id_rel'] . "><img src='../utils/capmed_16x16.png' title='Detalhes relatorio'></td>";
            echo "<td>";

            if ($row['id_rel'] == $ultimo_relatorio) {
                if ($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                        ['enfermagem']
                        ['enfermagem_paciente']
                        ['enfermagem_paciente_deflagrado']
                        ['enfermagem_paciente_deflagrado_usar_para_nova'])) {
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=relatorio&opcao=5&id={$id}&idr=" . $row['id_rel'] . "&editar=0><img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para novo relat&oacute;rio.' border='0'></a>";
                }
                if($data_atual < $prazoEditar){
                    if ($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                            ['enfermagem']
                            ['enfermagem_paciente']
                            ['enfermagem_paciente_deflagrado']
                            ['enfermagem_paciente_deflagrado_editar'])) {
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=?view=relatorio&opcao=5&id={$id}&idr=" . $row['id_rel'] . "&editar=1><img src='../utils/edit_16x16.png' title='Editar' border='0'></a>";
                    }
                }
            }

            echo "</td></tr>";
        }

        if ($flag) {
            if ($_SESSION['adm_user'] == 1 || isset($_SESSION['permissoes']
                    ['enfermagem']
                    ['enfermagem_paciente']
                    ['enfermagem_paciente_deflagrado']
                    ['enfermagem_paciente_deflagrado_novo'])) {
                $this->relatorio_deflagrado($id);
            } else {
                echo "" . htmlentities("Não possui Relatório") . " Deflagrado de Enfermagem.";
            }
            return;
        }
        echo "</table>";

        echo "</div>";
    }

    public function evolucao_clinica($id_paciente,$id_prorrogacao,$editar) {
        if (isset($id_prorrogacao)) {
            $sql = "
                SELECT
                    r.*,                    
                    r.EVOLUCAO_CLINICA as evolucao
                FROM
                    relatorio_prorrogacao_enf as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios)				 
                WHERE
                    r.ID = '{$id_prorrogacao}'
                ORDER BY
                    r.DATA DESC";

            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {
                $evolucao_clinica = $row['evolucao'];
            }
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>Evolu&ccedil;&atilde;o Cl&iacute;nica</th></tr></thead>";
        echo "<tr><td colspan='3'><textarea cols='120' rows='5' class='OBG' placeholder='EVOLU&Ccedil;&Atilde;O C&Eacute;FALO CAUDAL COMPLETA DESCREVENDO TODOS OS DISPOSITIVOS, EQUIPAMENTOS EM USO.' name='evolucao_clinica'>{$evolucao_clinica}</textarea></td></tr>";
        echo "</table>";
        return;
    }

    public function orientacoes($id_prorrogacao,$tipo_relatorio) {
        $condicao = $tipo_relatorio == 2 ? "relatorio_prorrogacao_enf" : "relatorio_visita_enf";

        if (isset($id_prorrogacao)) {
            $sql = "
                SELECT
                    r.*,                    
                    r.ORIENTACOES as orientacoes
                FROM
                    {$condicao} as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios)				 
                WHERE
                    r.ID = '{$id_prorrogacao}'
                ORDER BY
                    r.DATA DESC";

            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {
                $orientacoes = $row['orientacoes'];
            }
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>ORIENTA&Ccedil;&Otilde;ES DADAS A FAM&Iacute;LIA/ CUIDADOR  E CUIDADOS DE ENFERMAGEM REALIZADOS</th></tr></thead>";
        echo "<tr><td colspan='3'><textarea cols='120' rows='5' class='OBG' name='orientacoes'>{$orientacoes}</textarea></td></tr>";
        echo "</table>";
        return;
    }

    public function feridas($id_prorrogacao,$tipo_relatorio,$editar) {
        if (isset($_POST['fromRelatorio']))
            $fromRelatorio = $_POST['fromRelatorio'];

        $isProrrogacaoEnfermagem = $fromRelatorio == "prorrogacao_enfermagem";
        $isAditivoEnfermagem = $fromRelatorio == "aditivo_enfermagem";
        $dataFeridaEditar = '';
        if($isProrrogacaoEnfermagem || $isAditivoEnfermagem){
            $dataFeridaEditar = 1;
        }

        $maxFormUpload = $isProrrogacaoEnfermagem || $isAditivoEnfermagem ? 2 : 1;

        $hasImagemFerida = true;

        $deletar_ferida = "<img style='float:right;' src='../utils/delete_16x16.png' class='del_ferida_prorrogacao' title='Excluir Ferida' border='0' >";
        $condicao = $tipo_relatorio == 2 ? "RELATORIO_ADITIVO_ENF_ID = {$id_prorrogacao} AND EDITADA_EM is NULL AND DATA_DESATIVADA is NULL" : "RELATORIO_PRORROGACAO_ENF_ID = {$id_prorrogacao} AND EDITADA_EM is NULL AND DATA_DESATIVADA is NULL";

        $exibirUploadImagem = ! isset($_GET['view']) && $_GET['view'] != 'visualizar_rel_prorrogacao';
        $form_upload_imagem = $this->getFormImageTemplate($exibirUploadImagem);

        if (isset($id_prorrogacao) && !empty($id_prorrogacao)) {
            $sql2 = "
                SELECT
                    feridas_relatorio_prorrogacao_enf.ID AS ID,
                    feridas_relatorio_prorrogacao_enf.LOCAL as LOCAL,
                    feridas_relatorio_prorrogacao_enf.CARACTERISTICA as CARACTERISTICA,
                    feridas_relatorio_prorrogacao_enf.TIPO_CURATIVO as curativo,
                    feridas_relatorio_prorrogacao_enf.PRESCRICAO_PROX_PERIODO as prescricao,
                    feridas_relatorio_prorrogacao_enf.DATA
                FROM
                    feridas_relatorio_prorrogacao_enf
                WHERE $condicao";
            $result2 = mysql_query($sql2);
            while ($row2 = mysql_fetch_array($result2)) {
                $this->printFerida($row2, '', $exibirUploadImagem, null, $editar, $maxFormUpload,$dataFeridaEditar);
            }
        }else{
            if ($isProrrogacaoEnfermagem) {
                $imagemFeridaEvolucao = new \App\Models\Enfermagem\ImagemFeridaEvolucao(\App\Models\DB\ConnMysqli::getConnection());
                $locaisFerida = $imagemFeridaEvolucao->getFeridas($_POST['pacienteId']);

            }
            if (isset($locaisFerida) && !empty($locaisFerida)) {
                foreach ($locaisFerida as $localFerida) {
                    $maxFormUploadAux = $maxFormUpload;
                    $display_ferida_imagem = '';
                    $auxiliar = 0;
                    //  die(var_dump($localFerida));

                    foreach ($localFerida as $feridaEvolucao) {

                        if($auxiliar < 2) {
                            $maxFormUploadAux--;
                            if($feridaEvolucao['LADO'] == 1){
                                $lado = 'Direito';
                            }else  if($feridaEvolucao['LADO'] == 2){
                                $lado = 'Esquerdo';
                            }else{
                                $lado ='' ;
                            }
                            $local = utf8_encode($feridaEvolucao['LOCAL']);
                            $feridaEvolucao['LOCAL'] =  $local.' '.$lado;
                            $data_evolucao = \DateTime::createFromFormat('Y-m-d', $feridaEvolucao['DATA_EVOLUCAO'])->format('d/m/Y');
                            $infoImage = "<span class='span-data-image'>Em <input type='text' class='data data-ferida-prorrogacao' value= '{$data_evolucao}'></span>";
                            $display_ferida_imagem .= $this->displayImage(
                                $feridaEvolucao['path'],
                                $exibirUploadImagem,
                                $feridaEvolucao['file_uploaded_id'],
                                $infoImage,
                                $feridaEvolucao['DATA_EVOLUCAO']
                            );
                        }
                        $auxiliar ++;

                    }
                    $this->printFerida(
                        $feridaEvolucao,
                        $display_ferida_imagem,
                        $exibirUploadImagem,
                        "Adicionado automaticamente a partir das evoluções do mês vigente.",
                        null,
                        $maxFormUploadAux,
                        $isProrrogacaoEnfermagem
                    );
                }
            } else {
                $this->printFerida(array(), '', $exibirUploadImagem, null, null, $maxFormUpload,$dataFeridaEditar);
            }
        }
    }

    protected function printFerida(
        $row2 = array(),
        $display_ferida_imagem = '',
        $upload = false,
        $messageInfo = null,
        $editar = null,
        $maxFormUpload = 2,
        $dataFeridaEditar = ''
    ) {
        $local = $row2['LOCAL'];
        $caracteristica = $row2['CARACTERISTICA'];
        $curativo = $row2['curativo'];
        $prescricao = $row2['prescricao'];
        $id_ferida = $row2['ID'];
        $data_ferida = '';

        if (empty($display_ferida_imagem)) {

            $conn = \App\Models\DB\ConnMysqli::getConnection();
            $imagemFeridaEvolucao = new \App\Models\Enfermagem\ImagemFeridaEvolucao($conn);
            $feridasImagens = $imagemFeridaEvolucao->getImagens($row2['ID']);
            if (isset($feridasImagens)) {
                foreach ($feridasImagens as $feridaImagem) {
                    $dataFeridaPtb = '';
                    $descricao = '';
                    $inputData = '';
                    $dataFeridaUS = $row2['DATA'];
                    if(isset($row2['DATA']))
                        $dataFeridaPtb = \DateTime::createFromFormat('Y-m-d H:i:s', $row2['DATA'])->format('d/m/Y');
                    if ($feridaImagem['data_imagem'] != '0000-00-00'){
                        $dataFeridaPtb= \DateTime::createFromFormat('Y-m-d', $feridaImagem['data_imagem'])->format('d/m/Y');
                        $dataFeridaUS =$feridaImagem['data_imagem'];
                    }
                    if($dataFeridaEditar && $editar != 4)
                        $inputData = "<span class='span-data-image'>Em <input type='text' class='data data-ferida-prorrogacao' value= '{$dataFeridaPtb}'></span>";
                    else
                        $inputData = "Em {$dataFeridaPtb}";
                    $descricao=  $infoImage =  $inputData;
                    $display_ferida_imagem .= $this->displayImage($feridaImagem['path'], $upload, $feridaImagem['id'],$descricao, $dataFeridaUS);
                    $maxFormUpload--;
                }
            } else {

                $this->displayImage('', $upload);
            }
        }

        $readonly = $editar == 4 ? "readonly='readonly'" : '';
        $deletar_ferida = $editar == 4 ? '' : "<img style='float:right;' src='../utils/delete_16x16.png' class='del_ferida_prorrogacao' title='Excluir Ferida' border='0' >";//nao aparece o botao para excluir ferida no visualizar relatorio

        $form_upload_imagem = $this->getFormImageTemplate($upload);

        include 'templates/prorrogacao_ferida.phtml';
    }

    /**
     * @deprecated
     */
    protected function getProrrogacaoImagens($feridaId)
    {
        $sql = "SELECT files_uploaded.*
              FROM feridas_relatorio_prorrogacao_enf_imagem
                INNER JOIN files_uploaded ON files_uploaded.id = feridas_relatorio_prorrogacao_enf_imagem.file_uploaded_id
              WHERE ferida_relatorio_prorrogacao_enf_id = {$feridaId}";
        return mysql_query($sql);
    }

    protected function displayImage($imagem_ferida_path, $upload = false, $imageId = '', $descricao = null,$data_ferida )
    {
        $display_ferida_imagem = '';
        $form_upload_imagem = $this->getFormImageTemplate($upload, $imageId, $data_ferida);
        if ($upload && $imagem_ferida_path) {
            $btnExcluirImagem = '<a class="image-ferida-thumb excluir-imagem" onclick="excluirImagem(this)">Excluir</a>';
        }
        if ($imagem_ferida_path) {
            $imagem_ferida_url = App\Helpers\Storage::getImageUrl($imagem_ferida_path);
            $imagem_ferida_viewer = App\Helpers\Storage::getImageUrlWithViewer($imagem_ferida_path);
            $template = '<td align="center">%s<a target="_blank" href="%s"><img src="%s" class="%s" title=""></img></a> <br><span>%s</span>' . $btnExcluirImagem . '</td>';
            $display_ferida_imagem = sprintf($template, $form_upload_imagem, $imagem_ferida_viewer, $imagem_ferida_url, 'image-ferida-thumb', $descricao);
        }
        return $display_ferida_imagem;
    }

    protected function getFormImageTemplate($exibir, $imageId = '',$data_evolucao)
    {
        if ($exibir) {
            return <<<FORM_UPLOAD_IMAGEM
<form class='upload_form' enctype='multipart/form-data' method='post'>
  <input type='file' class='images-ferida' name='feridas[]' onchange="uploadFile(this, 'upload_imagem2.php', '.div-quadro-feridas')" data-images="{$imageId}" data-date="{$data_evolucao}"/>
  <span class='progressBar'></span>
  <h2 class='status_image'></h2>
</form>
FORM_UPLOAD_IMAGEM;
        }

        return '';
    }

    public function pad_enfermagem($id_paciente, $id_prorrogacao, $editar) {
        if (isset($id_prorrogacao)) {
            $sql = "
                SELECT
                    r.*,                    
                    r.PAD_ENFERMAGEM as pad_enfermagem
                FROM
                    relatorio_prorrogacao_enf as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios)				 
                WHERE
                    r.ID = '{$id_prorrogacao}'
                ORDER BY
                    r.DATA DESC";

            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {
                $pad_enfermagem = $row['pad_enfermagem'];
            }
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>PAD ENFERMAGEM PARA PR&Oacute;XIMO PER&Iacute;ODO</th></tr></thead>";
        echo "<tr><td colspan='3'><textarea cols='120' rows='5' class='OBG' name='pad_enfermagem'>{$pad_enfermagem}</textarea></td></tr>";
        echo "</table>";
        return;
    }

    public function solicitacao_aditivo($id_paciente, $id_prorrogacao, $editar) {
        if (isset($id_prorrogacao)) {
            $sql = "
                SELECT
                    r.*,
                    DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
                    DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
                    DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
                    u.nome as user,
                    r.SOLICITACAO_ADITIVO as aditivo
                FROM
                    relatorio_aditivo_enf as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios)				 
                WHERE
                    r.ID = '{$id_prorrogacao}'
                ORDER BY
                    r.DATA DESC";

            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {
                $inicio = $row['inicio'];
                $fim = $row['fim'];
                $user = $row['user'];
                $solicitacao_aditivo = $row['aditivo'];
            }
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>SOLICITA&Ccedil;&Atilde;O DE ADITIVO</th></tr></thead>";
        echo "<tr><td colspan='3'><textarea cols='120' rows='5' name='solicitacao_aditivo' placeholder='Descrever a demanda da solicitaÃ§Ã£o com justificativa clara, fundamentada, objetiva. Se for alteraÃ§Ã£o de curativo enviar fotos atuais'>{$solicitacao_aditivo}</textarea></td></tr>";
        echo "</table>";
        return;
    }

    public function resumo_historia($id_relatorio) {
        $sql = "SELECT
                    *
                FROM
                    relatorio_alta_enf
                WHERE
                    ID = {$id_relatorio}
        ";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $resumo_historia = $row['RESUMO_HISTORIA'];
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>RESUMO DA HIST&Oacute;RIA</th></tr></thead>";
        echo "<tr><td colspan='3'><textarea cols='120' rows='5' placeholder='abordar anamnese, estado geral, exame f&iacute;sico c&eacute;falo-caudal' name='resumo_historia' class='OBG'>{$resumo_historia}</textarea></td></tr>";
        echo "</table>";
        return;
    }

    public function alteracoes($id_prorrogacao) {
        $sql = "SELECT
                    *
                FROM
                    relatorio_visita_enf
                WHERE
                    relatorio_visita_enf.ID = {$id_prorrogacao}";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $alteracoes_sn = $row['ALTERACOES_SN'];
            $descricao_alteracoes = $row['DESCRICAO_ALTERACOES'];
        }

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>ALTERA&Ccedil;&Otilde;ES CL&Iacute;NICAS: ";
        echo "<select name='alteracoes_sn' id='alteracoes_clinicas' class='COMBO_OBG'><option value='-1'></option><option value='1'" . (($alteracoes_sn == '1') ? "selected" : "") . ">Sim</option><option value='2'" . (($alteracoes_sn == '2') ? "selected" : "") . ">N&atilde;o</option></select></th></tr></thead>";
        echo "</table>";
        echo "<table id='tabela_alteracoes' class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>ALTERA&Ccedil;&Otilde;ES</th></tr></thead>";
        echo "<tr><td colspan='3'><textarea cols='120' rows='5' placeholder='Descri&ccedil;&atilde;o das altera&ccedil;&otilde;es' name='desc_alteracoes' id='desc_alteracoes'>$descricao_alteracoes</textarea></td></tr>";
        echo "</table>";
        return;
    }

    public function problemas_material($id_prorrogacao) {
        $sql = "SELECT
                    *
                FROM
                    relatorio_visita_enf
                WHERE
                    relatorio_visita_enf.ID = {$id_prorrogacao}";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $problemas_sn = $row['PROBLEMAS_SN'];
            $problemas_material = $row['PROBLEMAS_MATERIAL'];
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>PROBLEMAS COM MATERIAL OU MEDICA&Ccedil;&Otilde;ES: ";
        echo "<select name='problemas_sn' id='problemas_material' class='COMBO_OBG'><option value='-1'></option><option value='1'" . (($problemas_sn == '1') ? "selected" : "") . ">Sim</option><option value='2'" . (($problemas_material == '2') ? "selected" : "") . ">N&atilde;o</option></select></th></tr></thead>";
        echo "</table>";
        echo "<table id='tabela_problemas' class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>PROBLEMAS</th></tr></thead>";
        echo "<tr><td colspan='3'><textarea cols='120' rows='5' placeholder='Descri&ccedil;&atilde;o das altera&ccedil;&otilde;es' name='prob_material' id='prob_material'>$problemas_material</textarea></td></tr>";
        echo "</table>";
        return;
    }

    public function dificuldade_profissional($id_prorrogacao) {
        $sql = "SELECT
                    *
                FROM
                    relatorio_visita_enf
                WHERE
                    relatorio_visita_enf.ID = {$id_prorrogacao}";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $dificuldades_sn = $row['DIFICULDADE_SN'];
            $dificuldades_profissional = $row['DIFICULDADE_PROFISSIONAL'];
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>DIFICULDADES COM PROFISSIONAL: ";
        echo "<select name='dificuldades_sn' id='dificuldades_profissional' class='COMBO_OBG'><option value='-1'></option><option value='1'" . (($dificuldades_sn == '1') ? "selected" : "") . ">Sim</option><option value='2'" . (($dificuldades_sn == '2') ? "selected" : "") . ">N&atilde;o</option></select></th></tr></thead>";
        echo "</table>";
        echo "<table id='tabela_dificuldade' class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>DETALHES</th></tr></thead>";
        echo "<tr><td colspan='3'><textarea cols='120' rows='5' name='dificuldade' id='dificuldade'>$dificuldades_profissional</textarea></td></tr>";
        echo "</table>";
        return;
    }

    public function dificuldade_familia($id_prorrogacao) {
        $sql = "SELECT
                    *
                FROM
                    relatorio_visita_enf
                WHERE
                    relatorio_visita_enf.ID = {$id_prorrogacao}";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $familia_sn = $row['FAMILIA_SN'];
            $dificuldade_familia = $row['DIFICULDADE_FAMILIA'];
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>DIFICULDADES COM FAM&Iacute;LIA/CUIDADOR: ";
        echo "<select name='familia_sn' id='dificuldades_familia' class='COMBO_OBG'><option value='-1'></option><option value='1'" . (($familia_sn == '1') ? "selected" : "") . ">Sim</option><option value='2'" . (($familia_sn == '2') ? "selected" : "") . ">N&atilde;o</option></select></th></tr></thead>";
        echo "</table>";
        echo "<table id='tabela_familia' class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>DETALHES</th></tr></thead>";
        echo "<tr><td colspan='3'><textarea cols='120' rows='5' name='familia' id='familia'>$dificuldade_familia</textarea></td></tr>";
        echo "</table>";
        return;
    }

    public function cuidado_alta($id_relatorio) {
        $sql = "SELECT
                    orientacoes_relatorio_alta_enf.ORIENTACOES AS ORIENTACOES,
                    orientacoes_relatorio_alta_enf.OUTRAS_ORIENTACOES AS `OUTRAS_ORIENTACOES`,
                    orientacoes_alta_enfermagem.ORIENTACAO AS ORIENTACAO
                FROM
                    orientacoes_alta_enfermagem
                INNER JOIN orientacoes_relatorio_alta_enf ON orientacoes_relatorio_alta_enf.ORIENTACOES = orientacoes_alta_enfermagem.ID
                WHERE
                    orientacoes_relatorio_alta_enf.REL_ALTA_ID = {$id_relatorio}
        ";
        $result = mysql_query($sql);

        $sql_select = "SELECT
                    *
                FROM
                    orientacoes_alta_enfermagem
                    ";
        $result_select = mysql_query($sql_select);

        echo "<table class='mytable' id='tabela_cuidado' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>ORIENTA&Ccedil;&Otilde;ES DE CUIDADOS NO MOMENTO DA ALTA</th></tr></thead>";
        echo "<tr><td colspan='3'><select name='cuidado_alta' id='cuidado_alta' class='COMBO_OBG'><option value='-1'></option>";

        while ($row2 = mysql_fetch_array($result_select)) {
            echo "<option value='{$row2['ID']}'>{$row2['ORIENTACAO']}</option>";
        }
        echo "</select><span class='span_cuidado'></span>&nbsp;<button id='add-cuidado' tipo=''>+ </button></td></tr>";

        while ($row = mysql_fetch_array($result)) {
            if ($row['ORIENTACOES'] != 8) {
                echo "<tr><td colspan='3' class='cuidado' cuidado_id='{$row['ORIENTACOES']}' cuidado_outros='{$row['OUTRAS_ORIENTACOES']}'><img src='../utils/delete_16x16.png' class='del-cuidado' title='Excluir' border='0' >{$row['ORIENTACAO']}</td></tr>";
            }else{
                echo "<tr><td colspan='3' class='cuidado' cuidado_id='{$row['ORIENTACOES']}' cuidado_outros='{$row['OUTRAS_ORIENTACOES']}'><img src='../utils/delete_16x16.png' class='del-cuidado' title='Excluir' border='0' >{$row['OUTRAS_ORIENTACOES']}</td></tr>";
            }
        }
        echo "</table>";
        return;
    }

    public function problemas_ativos($id_paciente, $id_prorrogacao, $editar = null,$origem = null) {
        if($editar == 1 && $origem == 5){
            $sql2 = "
            SELECT
                DESCRICAO as nome,
                CID_ID,
                OBSERVACAO
            FROM
                 problemas_relatorio_prorrogacao_enf
            WHERE
                RELATORIO_DEFLAGRADO_ENF_ID = {$id_prorrogacao}
              ";
        }else{
            $sql = "
            SELECT
                max(ID)
            FROM
                fichamedicaevolucao
            WHERE
                PACIENTE_ID= {$id_paciente}
              ";
            $result = mysql_query($sql);
            $maior_evolucao = mysql_result($result, 0);
            $sql2 = "
            SELECT
                pae.DESCRICAO as nome,
                pae.CID_ID,
                pae.STATUS,
                pae.OBSERVACAO
            FROM
                problemaativoevolucao as pae
            WHERE
                pae.EVOLUCAO_ID = {$maior_evolucao}
            ORDER BY
                pae.status DESC
              ";
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th colspan='3' >Problemas Ativos</th>";
        echo "</tr></thead>";


        echo "<tr><td colspan='3' >";

        echo "<table id='problemas_ativos' style='width:100%;'>";
        echo "<tr style='background-color:#ccc;'><td width='50%'><b>CID/PROBLEMA</b></td><td><b>DESCRICAO MEDICA</b></td></tr>";

        $result2 = mysql_query($sql2);
        $cont = 1;
        while ($row = mysql_fetch_array($result2)) {
            $html .= "<tr style='border-bottom: 1px solid #ccc;'><td class='prob-ativos-evolucao' desc='{$row['nome']}' cod='{$row['CID_ID']}' width='50%'><label>CID - {$row['CID_ID']}  {$row['nome']}</label></td><td><label>{$row['OBSERVACAO']}</label></td></tr>";
        }
        echo $html;
        echo "</table>";

        echo "</td></tr>";
        echo "</table>";
    }

    public function historia_clinica($id_relatorio, $editar = null){
        $sql="SELECT
                DATE_FORMAT(relatorio_deflagrado_enf.inicio,'%d/%m/%Y') as inicio,
                DATE_FORMAT(relatorio_deflagrado_enf.fim,'%d/%m/%Y') as fim,
                HISTORIA_CLINICA
            FROM
                relatorio_deflagrado_enf
            WHERE
                id = $id_relatorio";
        $result = mysql_query($sql);
        while($row = mysql_fetch_array($result)){
            $data_inicial = $row['inicio'];
            $data_final = $row['fim'];
            $historia_clinica = $row['HISTORIA_CLINICA'];
        }
        if($editar == 0){
            $data_inicial = '';
            $data_final = '';
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3' >História Clínica</th></tr></thead>";
        echo "<tr><td colspan='3'><b>" . htmlentities("História Clínica do período") . " </b><input class='data OBG' value='{$data_inicial}' type='text' name='inicio' maxlength='10' size='10' /> a <input class='data OBG' value='{$data_final}' type='text' name='fim' maxlength='10' size='10' /></td></tr>";
        echo "<tr><td colspan='3'><textarea cols='120' rows='5' name='historia_clinica' id='historia_clinica'>$historia_clinica</textarea></td></tr>";
        echo "</table>";
    }

    public function servicos_prestados($id_relatorio, $editar = null){
        $sql="SELECT
                SERVICOS_PRESTADOS
            FROM
                relatorio_deflagrado_enf
            WHERE
                id = $id_relatorio";
        $result = mysql_query($sql);
        while($row = mysql_fetch_array($result)){
            $servicos_prestados = $row['SERVICOS_PRESTADOS'];
        }
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3' >Serviços Prestados</th></tr></thead>";

        echo "<tr><td colspan='3'><textarea cols='120' rows='5' name='servicos_prestados' id='servicos_prestados'>$servicos_prestados</textarea></td></tr>";
        echo "</table>";
    }

    public function relatorio_prorrogacao($id_paciente, $id_prorrogacao, $editar) {

        $sql =  "SELECT
                    relatorio_prorrogacao_enf.*,
                    DATE_FORMAT(relatorio_prorrogacao_enf.inicio,'%d/%m/%Y') as inicio,
                    DATE_FORMAT(relatorio_prorrogacao_enf.fim,'%d/%m/%Y') as fim,
                    relatorio_prorrogacao_enf.FERIDAS as feridas,
                    e.nome AS empresa_rel,
                    pds.nome AS convenio_rel
                FROM
                    relatorio_prorrogacao_enf LEFT JOIN
                    usuarios ON (relatorio_prorrogacao_enf.USUARIO_ID = usuarios.idUsuarios)
                    LEFT JOIN empresas AS e ON relatorio_prorrogacao_enf.empresa = empresas.id
                    LEFT JOIN planosdesaude AS pds ON relatorio_prorrogacao_enf.convenio = pds.id				 
                WHERE
                    relatorio_prorrogacao_enf.ID = '{$id_prorrogacao}'
                ORDER BY
                    relatorio_prorrogacao_enf.DATA DESC";

        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $feridas = $row['feridas'];
            $inicio = $row['inicio'];
            $fim = $row['fim'];
            $empresa = $row['empresa_rel'];
            $convenio = $row['convenio_rel'];
        }
        echo "<div id='relatorio-prorrogacao'>";
        echo alerta();
        echo "<form>";
        echo "<input type='hidden' value='{$id_prorrogacao}' name='id_relatorio' />";
        echo "<input type='hidden' value='{$id_paciente}' name='id_paciente' />";
        echo "<input type='hidden' value='3' name='tipo_relatorio' />";
        echo "<input type='hidden' value='{$editar}' name='editar' />";
        echo "<input id='fromRelatorio' value='prorrogacao_enfermagem' type='hidden' />";
        echo "<center><h1>Nova Prorroga&ccedil;&atilde;o de Enfermagem</h1></center>";
        Paciente::cabecalho($id_paciente, $empresa, $convenio);
        echo "<table style='width:95%;'>";
        echo "<tr><td><b>Prorroga&ccedil;&atilde;o do Per&iacute;odo:</b><input class='data OBG' type='text' name='inicio' maxlength='10' size='10' value='{$inicio}' />";
        echo "&nbsp;<b>at&eacute;</b><input class='data OBG' type='text' name='fim' maxlength='10' size='10' value='$fim' /></tr></td>";
        echo "</table>";

        $this->problemas_ativos($id_paciente, $id_prorrogacao, $editar);


        $this->evolucao_clinica($id_paciente, $id_prorrogacao, $editar);
        $this->orientacoes($id_prorrogacao, 2);
        echo "<table class='mytable' style='width:95%;'>";
        echo "<table id='feridas_prorrogacao' class='mytable' style='width:95%;'>";

        echo "<thead><tr><th colspan='3'>PRESEN&Ccedil;A DE FERIDAS: ";
        echo "<select name='feridas_aditivo' class='feridas_prorrogacao COMBO_OBG' id='feridas_prorrogacao'>
                                <option value='-1'></option>
                                <option value='1' " . (($feridas == '1') ? "selected" : "") . ">Sim</option>
                                <option value='2' " . (($feridas == '2') ? "selected" : "") . ">N&atilde;o</option>
                                </select></th></tr></thead>";
        echo "</table>";
        //$this->feridas();
        echo "<span class='quadro_feridas_aditivo'>";
        echo "<div id='quadro-lesoes'>";
        echo "</div>";
        echo "<br><button id='ferida_prorrogacao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Nova Ferida</span></button><br><br>";
        echo "</span>";
        $this->pad_enfermagem($id_paciente, $id_prorrogacao, $editar);
        echo alerta();
        if ($editar == 1) {
            echo "<br><button id='editar_prorrogacao' paciente='{$id_paciente}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button>";
        } else {
            echo "<br><button id='salvar_prorrogacao' paciente='{$id_paciente}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button>";
        }
        echo "&nbsp;&nbsp;<button id='voltar_' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button>";
        echo "<input type='hidden' value='{$id_paciente}' id='pacienteId' />";
        echo "</form>";
        echo" </div>";
    }

    public function relatorio_alta($id_paciente, $id_prorrogacao, $editar) {
        $sql = "SELECT
                    DATE_FORMAT(INICIO,'%d/%m/%Y') as INICIO,
                    DATE_FORMAT(FIM,'%d/%m/%Y') as FIM,
                    e.nome AS empresa_rel,
                    pds.nome AS convenio_rel
                FROM
                    relatorio_alta_enf 
                    LEFT JOIN empresas AS e ON relatorio_alta_enf.empresa = e.id
                    LEFT JOIN planosdesaude AS pds ON relatorio_alta_enf.plano = pds.id
                WHERE
                   relatorio_alta_enf.id = {$id_prorrogacao}";


        $result = mysql_query($sql);

        while ($row = mysql_fetch_array($result)) {
            $inicio = $row['INICIO'];
            $fim = $row['FIM'];
            $empresa = $row['empresa_rel'];
            $convenio = $row['convenio_rel'];
        }

        echo "<div id='relatorio-alta'>";
        echo "<form>";
        echo "<input type='hidden' value={$id_prorrogacao} name='id_relatorio' />";
        echo "<center><h1>Nova Alta de Enfermagem</h1></center>";
        Paciente::cabecalho($id_paciente, $empresa, $convenio);

        echo "<table style='width:95%;'>";
        echo "<tr><td><b>Alta do Per&iacute;odo:</b><input class='data OBG' type='text' name='inicio' maxlength='10' size='10' value='{$inicio}' />";
        echo "&nbsp;<b>at&eacute;</b><input class='data OBG' type='text' name='fim' maxlength='10' size='10' value='{$fim}' /></tr></td>";
        echo "</table>";

        $this->resumo_historia($id_prorrogacao);
        $this->cuidado_alta($id_prorrogacao);
        echo alerta();
        if ($editar == 1) {
            echo "<br><button id='editar_rel_alta' paciente='{$id_paciente}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
        } else {
            echo "<br><button id='salvar_rel_alta' paciente='{$id_paciente}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
        }
        echo "&nbsp;&nbsp;<button id='voltar_' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button>";
        echo "</form>";
        echo" </div>";
    }

    public function relatorio_aditivo($id_paciente, $id_prorrogacao, $editar) {
        $dataEntrgaImediata = '';
        $horaEntrgaImediata = '';
        $horaEntrga = '';
        $dataEntrga = '';
        if (isset($id_prorrogacao)) {
            $sql = "
                SELECT
                    r.*,
                    DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
                    DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
                    DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
                    u.nome as user,
                    e.nome AS empresa_rel,
                    pds.nome AS convenio_rel,
                    r.SOLICITACAO_ADITIVO as aditivo,
                    r.ALTERACAO_LESOES as alteracoes,
                    r.FERIDAS as feridas
                FROM
                    relatorio_aditivo_enf as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios)	
                    LEFT JOIN empresas AS e ON r.empresa = empresas.id
                    LEFT JOIN planosdesaude AS pds ON r.plano = pds.id			 
                WHERE
                    r.ID = '{$id_prorrogacao}'
                ORDER BY
                    r.DATA DESC";

            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {

                $inicio = $row['inicio'];
                $fim = $row['fim'];
                $user = $row['user'];
                $solicitacao_aditivo = $row['aditivo'];
                $alteracoes = $row['alteracoes'];
                $feridas = $row['feridas'];
                $empresa = $row['empresa_rel'];
                $convenio = $row['convenio_rel'];
            }
            $sqlDatas = "SELECT
                            DATE_FORMAT(solicitacao_rel_aditivo_enf.data_entrega,'%d/%m/%Y') as datap,
                            DATE_FORMAT(solicitacao_rel_aditivo_enf.data_entrega,'%H:%i') as hora,
                            solicitacao_rel_aditivo_enf.entrega_imediata
                            FROM
                            solicitacao_rel_aditivo_enf
                            WHERE
                            solicitacao_rel_aditivo_enf.relatorio_enf_aditivo_id ='{$id_prorrogacao}'
                            GROUP BY
                            solicitacao_rel_aditivo_enf.entrega_imediata";

            $resultDatas = mysql_query($sqlDatas);
            while ($rowDatas = mysql_fetch_array($resultDatas)) {
                if($rowDatas['entrega_imediata'] == 'S'){
                    $dataEntregaImediata= $rowDatas['datap'];
                    $horaEntregaImediata= $rowDatas['hora'];
                }
                if($rowDatas['entrega_imediata'] == 'N'){
                    $dataEntrega= $rowDatas['datap'];
                    $horaEntrega= $rowDatas['hora'];
                }
            }

        }
        echo "<div id='relatorio-prorrogacao'>";
        echo alerta();
        echo "<form>";
        echo "<input type='hidden' value={$id_prorrogacao} name='id_relatorio' />";
        echo "<input type='hidden' id='fromRelatorio'  value='aditivo_enfermagem' />";
        echo "<input type='hidden' value='2' name='tipo_relatorio' />";
        echo "<center><h1>Novo Relat&oacute;rio Aditivo de Enfermagem</h1></center>";
        Paciente::cabecalho($id_paciente, $empresa, $plano);

        echo "<table style='width:95%;'>";
        echo "<tr><td><b>Relat&oacute;rio Aditivo do Per&iacute;odo:</b><input class='data OBG' type='text' name='inicio' maxlength='10' size='10' value='{$inicio}' />";
        echo "&nbsp;<b>at&eacute;</b><input class='data OBG' type='text' name='fim' maxlength='10' size='10' value='$fim' /></tr></td>";
        echo "</table>";
        $this->problemas_ativos($id_paciente, $id_prorrogacao, $editar);
//        echo "<table class='mytable' style='width:95%;'>";
//        echo "<thead><tr><th colspan='3'>ALTERA&Ccedil;&Atilde;O DO QUADRO DE LES&Otilde;ES: <select name='quadro_lesoes'><option value='-1'></option><option value='1' " . (($alteracoes == 1) ? "selected" : "") . ">Sim</option><option value='2' " . (($alteracoes == 2) ? "selected" : "") . ">N&atilde;o</option></select></th></tr></thead>";
//        echo "</table>";
        echo "<table id='feridas_aditivo' class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>ALTERA&Ccedil;&Atilde;O DO QUADRO DE LES&Otilde;ES: ";
        echo "<select name='feridas_aditivo' class='feridas_prorrogacao' id='feridas_prorrogacao'><option value='-1'></option><option value='1' " . (($feridas == 1) ? "selected" : "") . ">Sim</option><option value='2' " . (($feridas == 2) ? "selected" : "") . ">N&atilde;o</option></select></th></tr></thead>";
        echo "</table>";
        echo "<span class='quadro_feridas_aditivo'>";
        echo "<div id='quadro-lesoes'>";
        echo "</div>";
        echo "<br><button id='ferida_prorrogacao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Nova Ferida</span></button><br><br>";
        echo "</span>";

        $this->solicitacao_aditivo($id_paciente, $id_prorrogacao, $editar);
        echo alerta();
        echo "</form>";
        echo " <br><br>
               ".$this->adicionarItemSolicitacaoAditivo()."
               <br>
              <div id='field_avulsa'>
                    <p>
                          <fieldset>
                             <legend>Entrega sem caráter imediato</legend>
                             <div class='ui-state-highlight' style=\"padding: 5px;\">
                              <span class=\"ui-icon  ui-icon-info icon-inline\" ></span>
                              Para os itens \"sem\" caráter de entrega imediata, o prazo mínimo é de 24 horas.
                              </div>
                             <br/>
                             <b>Data </b>
                             <input type='text' value='{$dataEntrega}' class=' ' id='data-avulsa' name='data-avulsa' size='9px' />
                             <b>Hora: </b>
                             <input type='text' value='{$horaEntrega}' class='hora'  id='hora-avulsa' name='hora-avulsa' size='9px'/>
                          </fieldset>
                    </p>
                    <p>
                          <fieldset>
                             <legend>Entrega com caráter imediato</legend>
                            <div class='ui-state-highlight' style=\"padding: 5px;\">
                              <span class=\"ui-icon  ui-icon-info icon-inline\" ></span>
                              Não existe um prazo mínimo para os itens \"com\" caráter de entrega imediata.
                            </div>
                            <br/><b>Data</b>
                            <input type='text'  id='dataEntregaImediata' value='{$dataEntregaImediata}' size='9px' />
                            <b>Hora: </b>
                            <input type='text'   class='hora'  id='horaEntregaImediata'  value='{$horaEntregaImediata}' size='9px'/>
                          </fieldset>
                     </p>
                </div>
                <br>
                <button id='solicitar-item-relatorio-aditivo' 
                    class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' 
                    role='button' aria-disabled='false' >
                       <span class='ui-button-text'>
                                 Solicitar Item
                       </span>
                </button>
                <br><br>

              <table id='solicitar-itens-aditivo' class='mytable' style='width:95%;'>
                  <thead>
                     <tr>
                         <th>Itens Solicitados</th>
                     </tr>
                  </thead>
                  <tr id='mat'></tr>
                  <tr id='eqp'></tr>
                  <tr id='kit'></tr>
                     ".$this->itensSolicitacaoAditivo($id_prorrogacao, $editar)."  
               </table>
        
             " ;
        if ($editar == 1) {
            echo "<br><button id='editar_rel_aditivo' paciente='{$id_paciente}' class='ui-button "
                . "ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' "
                . "role='button' aria-disabled='false' >"
                . "<span class='ui-button-text'>Editar</span>"
                . "</button>";
        } else {
            echo "<br><button id='salvar_rel_aditivo' paciente='{$id_paciente}'  "
                . "class='ui-button ui-widget ui-state-default ui-corner-all "
                . "ui-button-text-only ui-state-hover' role='button' aria-disabled='false' >"
                . "<span class='ui-button-text'>".htmlentities('Salvar')."</span>"
                . "</button>";
        }
        echo "&nbsp;&nbsp;<button id='voltar_' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button>";

        echo" </div>";
    }

    private function adicionarItemSolicitacaoAditivo()
    {
        echo "<div id='div-adicionar-item-aditivo' title='Adicionar itens' tipo='mat' classe='' >";
        echo alerta();
        echo "<center>
                      <div id='tipos-item-solicitacao-aditivo'>                      
                             <input type='radio' id='bmed' name='tipos-solicitacao-aditivo' value='med' />
                            <label for='bmed'>
                                Medicamentos
                            </label>
                            <input type='radio' checked id='bmat' name='tipos-solicitacao-aditivo' value='mat' />
                            <label for='bmat'>
                                Materiais
                            </label> 
                            <input type='radio' id='bdie' name='tipos-solicitacao-aditivo' value='die' />
                            <label for='bdie'>
                                Dietas
                            </label>                    
                            <input type='radio' id='bkits' name='tipos-solicitacao-aditivo' value='kits' />
                            <label for='bkits'>
                                Kits
                            </label>
                            <input type='radio' id='beqp' name='tipos-solicitacao-aditivo' value='eqp' />
                            <label for='beqp'>
                                 Equipamentos
                            </label>
                            <input type='radio' id='bserv' name='tipos-solicitacao-aditivo' value='serv' />
                            <label for='bserv'>
                                Servi&ccedil;os
                            </label>
                      </div>
                 </center>";
        echo "<p>
                      <b>Busca:</b>
                      <br/>
                      <input type='text' name='busca-item-solicitacao-aditivo' id='busca-item-solicitacao-aditivo' />
                 </p>";
        echo "<div id='opcoes-kits'></div>";
        echo "<input type='hidden' name='cod' id='cod' value='-1' />";
        echo "<input type='hidden' name='tipo' id='tipo' value='-1' />";
        echo "<input type='hidden' name='catalogo_id' id='catalogo_id' value='-1' />";
        echo "<input type='hidden' name='categoria' id='categoria' value='' />";
        echo "<input type='hidden' name='valor_venda' id='valor-venda' value='' />";
        echo "<p>
                      <b>Nome:</b>
                      <input type='text' name='nome' id='nome' class='OBG' />
                 </p>";
        echo "<p id='combo-rel-aditivo-qtd'>
                    <b>Quantidade:</b>
                    <input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/>
                  </p>";
        $result = mysql_query("
                    SELECT
                        *
                    FROM
                        frequencia
                    ORDER BY 
                    frequencia asc
                ");
        echo "<p id='combo-rel-aditivo-frequencia'>
                    <b>Frequência:</b>
                    <select name='frequencia_rel_aditivo' id='frequencia-rel-aditivo'>
                        <option value=''>Selecione...</option>";
        while($row = mysql_fetch_array($result)) {
            echo "<option value='{$row['id']}'>{$row['frequencia']}</option>";
        }
        echo "
                    </select>
                  </p>";
        echo "<input type='checkbox' id='entrega-imediata-aditivo'/><b>Entrega Imediata</b>
                   <p>
                   <button id='add-item-solicitacao-aditivo' tipo='' data-tipoSolicitacao='{$avulsa}' >
                    Adicionar à lista <b>+</b>
                   </button>
                </p>
        </div>";
    }
    public function itensSolicitacaoAditivo($id_rel_aditivo, $editar, $imprimir = null)
    {
        if(!isset($id_rel_aditivo)){
            return false;
        }
        $sql=" SELECT
                        solicitacao_rel_aditivo_enf.relatorio_enf_aditivo_id,
                        solicitacao_rel_aditivo_enf.catalogo_id,
                        solicitacao_rel_aditivo_enf.tiss,
                        solicitacao_rel_aditivo_enf.tipo,
                        solicitacao_rel_aditivo_enf.quantidade,
                        '' AS frequencia,
                        solicitacao_rel_aditivo_enf.quantidade_kit,
                        solicitacao_rel_aditivo_enf.kit_id,
                        solicitacao_rel_aditivo_enf.id,
                        CONCAT(catalogo.principio, ' ',catalogo.apresentacao) as item,
                        kitsmaterial.nome as kit,
                        solicitacao_rel_aditivo_enf.id as id_sol_aditivo,
                        solicitacao_rel_aditivo_enf.entrega_imediata,
                        DATE_FORMAT(solicitacao_rel_aditivo_enf.data_entrega, '%d/%m/%Y %H:%i:%s') as dtp
                        FROM
                        solicitacao_rel_aditivo_enf
                        LEFT JOIN kitsmaterial ON solicitacao_rel_aditivo_enf.kit_id = kitsmaterial.idKit
                        INNER JOIN catalogo ON solicitacao_rel_aditivo_enf.catalogo_id = catalogo.ID
                        WHERE
                        solicitacao_rel_aditivo_enf.relatorio_enf_aditivo_id = {$id_rel_aditivo} 
                        AND solicitacao_rel_aditivo_enf.tipo IN (0, 1, 3) 
                        AND solicitacao_rel_aditivo_enf.cancelado_por IS NULL
                        
                        UNION
                        SELECT
                        solicitacao_rel_aditivo_enf.relatorio_enf_aditivo_id,
                        solicitacao_rel_aditivo_enf.catalogo_id,
                        solicitacao_rel_aditivo_enf.tiss,
                        solicitacao_rel_aditivo_enf.tipo,
                        solicitacao_rel_aditivo_enf.quantidade,
                        '' AS frequencia,
                        solicitacao_rel_aditivo_enf.quantidade_kit,
                        solicitacao_rel_aditivo_enf.kit_id,
                        solicitacao_rel_aditivo_enf.id,
                        cobrancaplanos.item AS item,
                        kitsmaterial.nome AS kit,
                        solicitacao_rel_aditivo_enf.id as id_sol_aditivo,
                        solicitacao_rel_aditivo_enf.entrega_imediata,
                        DATE_FORMAT(
                            solicitacao_rel_aditivo_enf.data_entrega,
                            '%d/%m/%Y %H:%i:%s'
                        ) AS dtp
                        FROM
                        solicitacao_rel_aditivo_enf
                        LEFT JOIN kitsmaterial ON solicitacao_rel_aditivo_enf.kit_id = kitsmaterial.idKit
                        INNER JOIN cobrancaplanos ON solicitacao_rel_aditivo_enf.catalogo_id = cobrancaplanos.id 
                        WHERE
                        solicitacao_rel_aditivo_enf.relatorio_enf_aditivo_id = {$id_rel_aditivo} AND
                        solicitacao_rel_aditivo_enf.tipo = 5 AND
                        solicitacao_rel_aditivo_enf.cancelado_por IS NULL
                        
                        UNION
                        SELECT
                        solicitacao_rel_aditivo_enf.relatorio_enf_aditivo_id,
                        solicitacao_rel_aditivo_enf.catalogo_id,
                        solicitacao_rel_aditivo_enf.tiss,
                        solicitacao_rel_aditivo_enf.tipo,
                        solicitacao_rel_aditivo_enf.quantidade,
                        frequencia.frequencia,
                        0 AS quantidade_kit,
                        0 AS kit_id,
                        solicitacao_rel_aditivo_enf.id,
                        cuidadosespeciais.cuidado AS item,
                        '' AS kit,
                        solicitacao_rel_aditivo_enf.id as id_sol_aditivo,
                        solicitacao_rel_aditivo_enf.entrega_imediata,
                        DATE_FORMAT(
                            solicitacao_rel_aditivo_enf.data_entrega,
                            '%d/%m/%Y %H:%i:%s'
                        ) AS dtp
                        FROM
                        solicitacao_rel_aditivo_enf
                        INNER JOIN cuidadosespeciais ON solicitacao_rel_aditivo_enf.catalogo_id = cuidadosespeciais.id
                        INNER JOIN frequencia ON solicitacao_rel_aditivo_enf.frequencia = frequencia.id
                        WHERE
                        solicitacao_rel_aditivo_enf.relatorio_enf_aditivo_id = {$id_rel_aditivo} 
                        AND solicitacao_rel_aditivo_enf.tipo = 4 
                        AND solicitacao_rel_aditivo_enf.cancelado_por IS NULL
                        ORDER BY kit_id,id";
        $result = mysql_query($sql);
        $numRow = mysql_num_rows($result);

        if($numRow == 0){
            return false;
        }
        $idKit=0;
        $excluirItem='';
        $excluirKit='';
        $x ='';
        while ($row = mysql_fetch_array($result)){
            $data ='';
            $checkedImediato = $row['entrega_imediata'] == 'S'?'checked':'';
            $entregaImediata = '';


            if($row['kit_id']==0){
                $x = '';
                $cor = '#ffffff';
                if($editar == 1 || $editar == 0 ){
                    $excluirItem ="<img align='right' class='remover-item-solicitacao' 
                                       src='../utils/delete_16x16.png' title='Remover' border='0' />";
                    $entregaImediata="<input type='checkbox' ".$checkedImediato."
                                              name='entrega-imediata-aditivo'
                                              class='entrega-imediata-aditivo'/>
                                              <b>Entrega Imediata</b>";
                }else{
                    if($row['entrega_imediata'] == 'S' && $imprimir != 1){
                        $entregaImediata = " <b>Entrega Imediata: {$row['dtp']}</b>";
                    }
                }
            }else{
                $cor ='#FAFAFA';
                $excluirItem ='';
                $x =' - ';
                if($editar == 1 || $editar == 0 ) {
                    $entregaImediata = "<input type='checkbox' " . $checkedImediato . "
                                              name='entrega-imediata-aditivo'
                                              class='entrega-imediata-aditivo kit-entrega-imediata-aditivo'/>
                                              <b>Entrega Imediata</b>";
                }else{
                    if($row['entrega_imediata'] == 'S' && $imprimir != 1){
                        $entregaImediata = " <b>Entrega Imediata: {$row['dtp']}</b>";
                    }

                }
            }
            if($idKit != $row['kit_id'] && $row['kit_id']!=0){
                if($editar == 1 || $editar == 0 ) {
                    $excluirKit = "<img align='right' class='remover-item-solicitacao-kit' 
                                             src='../utils/delete_16x16.png' title='Remover' border='0' />";
                }
                $html.= "<tr bgcolor='#ffffff' class='kit' cod='{$row['kit_id']}'>"
                    . "<td> "
                    . "<b>KIT-{$row['kit']}  Quantidade:{$row['quantidade_kit']}</b> {$entregaImediata} {$excluirKit}"
                    . "</td>"
                    . "</tr>";
            }
            $idKit = $row['kit_id'];

            $text_qtd_freq = $row['quantidade'] > 0 ? " <b>Quantidade: {$row['quantidade']}</b>" : "<b>Frequência: {$row['frequencia']}</b>";

            $html .="<tr bgcolor='{$cor}' cod='{$row['kit_id']}' class='dados'  
                                qtd-kit='{$row['quantidade_kit']}' catalogo-id='{$row['catalogo_id']}'
                                 tiss='{$row['tiss']}' qtd='{$row['quantidade']}'
                                 tipo='{$row['tipo']}' id-kit='{$row['kit_id']}'
                                     id-solicitacao-rel-aditivo='{$row['id_sol_aditivo']}'>
                                    <td>
                                     {$x} {$row['item']} {$text_qtd_freq} $entregaImediata {$excluirItem}
                                    </td>
                             </tr>";

        }
        return $html;

    }
    public function relatorio_visita($id_paciente, $id_prorrogacao, $editar) {

        if (isset($id_prorrogacao)) {
            $sql = "
                SELECT
                    r.*,
                    DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
                    DATE_FORMAT(r.DATA_VISITA,'%d/%m/%Y') as data_visita,
                    u.nome as user,
                    e.nome AS empresa_rel,
                    pds.nome AS convenio_rel	
                FROM
                    relatorio_visita_enf as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios)	
                    LEFT JOIN empresas AS e ON r.empresa = e.id
                    LEFT JOIN planosdesaude AS pds ON r.plano = pds.id			 
                WHERE
                    r.ID = '{$id_prorrogacao}'
                ORDER BY
                    r.DATA DESC";

            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {

                $data_visita = $row['data_visita'];
                $user = $row['user'];
                $empresa = $row['empresa_rel'];
                $convenio = $row['convenio_rel'];
            }
        }
        echo "<div id='relatorio-prorrogacao'>";
        echo "<form>";
        echo "<input type='hidden' value={$id_prorrogacao} name='id_relatorio' />";
        echo "<center><h1>Novo Relat&oacute;rio Semanal de Visita de Enfermagem</h1></center>";
        Paciente::cabecalho($id_paciente, $empresa, $convenio);

        echo "<p><b>Relat&oacute;rio de Visita Semanal realizado em:</b><input class='data OBG' value='{$data_visita}' type='text' name='inicio' maxlength='10' size='10' />";

        $this->alteracoes($id_prorrogacao);
        $this->problemas_material($id_prorrogacao);
        $this->dificuldade_profissional($id_prorrogacao);
        $this->dificuldade_familia($id_prorrogacao);
        $this->orientacoes($id_prorrogacao, 3);
        echo alerta();
        if ($editar == 1) {
            echo "<br><button id='editar_rel_visita' paciente='{$id_paciente}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
        } else {
            echo "<br><button id='salvar_rel_visita' paciente='{$id_paciente}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
        }
        echo "&nbsp;&nbsp;<button id='voltar_' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button>";
        echo "</form>";
        echo" </div>";
    }

    public function relatorio_deflagrado($id_paciente, $id_relatorio, $editar){
        if (isset($id_relatorio)) {
            $sql = "
                SELECT
                    r.*,
                    DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
                    DATE_FORMAT(r.DATA,'%d/%m/%Y') as data_visita,
                    u.nome as user,
                    e.nome AS empresa_rel,
                    pds.nome AS convenio_rel
                FROM
                    relatorio_deflagrado_enf as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios)  
                    LEFT JOIN empresas AS e ON r.empresa = e.id
                    LEFT JOIN planosdesaude AS pds ON r.plano = pds.id             
                WHERE
                    r.ID = '{$id_relatorio}'
                ORDER BY
                    r.DATA DESC";


            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {

                $data_visita = $row['data_visita'];
                $user = $row['user'];
                $empresa = $row['empresa_rel'];
                $convenio = $row['convenio_rel'];
            }
        }
        echo "<div id='relatorio-deflagrado'>";
        echo "<form>";
        echo "<input type='hidden' value={$id_relatorio} name='id_relatorio' />";
        echo "<center><h1>Novo " . htmlentities("Relatório") . " Deflagrado de Enfermagem</h1></center>";
        Paciente::cabecalho($id_paciente, $empresa, $convenio);

        $this->problemas_ativos($id_paciente, $id_relatorio, $editar,5);
        $this->historia_clinica($id_relatorio,$editar);
        $this->servicos_prestados($id_relatorio,$editar);
        echo alerta();
        if ($editar == 1) {
            echo "<br><button id='editar_rel_deflagrado' paciente='{$id_paciente}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Editar</span></button></span>";
        } else {
            echo "<br><button id='salvar_rel_deflagrado' paciente='{$id_paciente}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
        }
        echo "&nbsp;&nbsp;<button id='voltar_' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button>";
        echo "</form>";
        echo" </div>";
    }

    public function visualizar_rel_prorrogacao($id_paciente,$idr){
        echo "<div>";

        $sql = "SELECT 
                  pds.nome AS convenio_rel, 
                  e.nome AS empresa_rel 
                FROM 
                  relatorio_prorrogacao_enf
                  LEFT JOIN empresas AS e ON relatorio_prorrogacao_enf.empresa = e.id
                  LEFT JOIN planosdesaude AS pds ON relatorio_prorrogacao_enf.plano = pds.id 
                WHERE 
                  relatorio_prorrogacao_enf.ID = '{$idr}'";
        $rs = mysql_query($sql);
        $rowPaciente = mysql_fetch_array($rs);
        $empresa = null;
        $convenio = null;
        if(count($rowPaciente) > 0) {
            $empresa = $rowPaciente['empresa_rel'];
            $convenio = $rowPaciente['convenio_rel'];
        }

        echo "<center><h1>Visualizar Prorroga&ccedil;&atilde;o de Enfermagem</h1></center>";
        Paciente::cabecalho($id_paciente, $empresa, $convenio);

        $sql = "
		SELECT
		r.*,
		DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
		DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
		DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
		u.nome as user,
		c.empresa as empresaCliente	
		FROM
		relatorio_prorrogacao_enf as r LEFT JOIN
		usuarios as u ON (r.USUARIO_ID = u.idUsuarios)  inner join 
		clientes as c on (r.PACIENTE_ID = c.idClientes)	
		WHERE
		r.ID = '{$idr}'
		ORDER BY
		r.DATA DESC";

        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {

            $inicio = $row['inicio'];
            $fim = $row['fim'];
            $user = $row['user'];
            $evolucao_clinica = $row['EVOLUCAO_CLINICA'];
            $orientacoes = $row['ORIENTACOES'];
            $feridas = $row['FERIDAS'];
            $pad_enfermagem = $row['PAD_ENFERMAGEM'];
           $empresaRelatorio = empty($row['empresa']) ? $row['empresaCliente'] : $row['empresa'];
        }

        echo "<p><b>RELAT&Oacute;RIO DE PRORROGA&Ccedil;&Atilde;O DE ENFERMAGEM DO PER&Iacute;ODO {$inicio} AT&Eacute; {$fim}</p>";
        echo "<p>PROFISSIONAL RESPONS&Aacute;VEL: {$user}</b></p>";

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th colspan='3' >Problemas Ativos</th>";
        echo "</tr></thead>";

//        echo "<tr><td colspan='3' ><b>Busca  </b><input type='text' name='busca-problemas-ativos-evolucao' {$acesso} size=100 /></td></tr>";
//        echo"<tr><td colspan='3' ><input type='checkbox' id='outros-prob-ativos-evolucao' name='outros-prob-ativos-evolucao' {$acesso} />Outros.<span class='outro_ativo_evolucao' ><input type='text' name='outro-prob-ativo-evolucao' class='outro_ativo_evolucao' {$acesso} size=75/><button id='add-prob-ativo-evolucao' tipo=''  >+ </button></span></td></tr>";
        echo "<tr><td colspan='3' >";

        echo "<table id='problemas_ativos' style='width:95%;'>";
        $sql = "
            SELECT
                max(ID)
            FROM
                fichamedicaevolucao
            WHERE
                PACIENTE_ID= {$id_paciente}
              ";
        $result = mysql_query($sql);
        $maior_evolucao = mysql_result($result, 0);
        $sql2 = "
            SELECT
                pae.DESCRICAO as nome,
                pae.CID_ID,
                pae.STATUS,
                pae.OBSERVACAO
            FROM
                problemaativoevolucao as pae
            WHERE
                pae.EVOLUCAO_ID = {$maior_evolucao}
            ORDER BY
                pae.status DESC
              ";
        $result2 = mysql_query($sql2);
        $cont = 1;
        while ($row = mysql_fetch_array($result2)) {
            $html .= "<tr style='border-bottom: 1px solid #ccc;display: block;'><td class='prob-ativos-evolucao' desc='{$row['nome']}' cod='{$row['CID_ID']}' colspan='2' width='50%'>CID - {$row['CID_ID']}  {$row['nome']}</td><td><label>{$row['OBSERVACAO']}</label></td></tr>";
        }
        echo $html;
        echo "</table>";

        echo "</td></tr>";
        echo "</table>";
        //Mostra EvoluÃ§Ã£o clinica
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>Evolu&ccedil;&atilde;o Cl&iacute;nica</th></tr></thead>";
        echo "<tr><td colspan='3'>{$evolucao_clinica}</td></tr>";
        echo "</table>";
        //Mostra OrientaÃ§Ãµes
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>ORIENTA&Ccedil;&Otilde;ES DADAS A FAM&Iacute;LIA/ CUIDADOR  E CUIDADOS DE ENFERMAGEM REALIZADOS</th></tr></thead>";
        echo "<tr><td colspan='3'>{$orientacoes}</td></tr>";
        echo "</table>";
        //Mostra feridas
        if($feridas == '1'){
            $feridas_sn = "Sim";
        }else{
            $feridas_sn = "N&atilde;o";
        }
        echo"<input type=hidden value={$feridas} name='mostrar_ferida'</>";
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>PRESEN&Ccedil;A DE FERIDAS: {$feridas_sn}</th></tr></thead>";
        echo "</table>";
        if($feridas == 1){
            $this->feridas($idr,0,4);
        }

        //Mostra Pad Enfermagem
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>PAD ENFERMAGEM PARA PR&Oacute;XIMO PER&Iacute;ODO</th></tr></thead>";
        echo "<tr><td colspan='3'>{$pad_enfermagem}</td></tr>";
        echo "</table>";



        echo "<button id='imprimir_prorrogacao' empresa-relatorio='{$empresaRelatorio}' caminho='/enfermagem/imprimir_relatorio_prorrogacao.php?id={$id_paciente}&idr={$idr}' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'><span class='ui-button-text'>Imprimir</span></button>";

        echo "</div>";
    }

    public function visualizar_rel_alta($id_paciente,$idr){
        echo "<div>";

        $sql = "SELECT 
                  pds.nome AS convenio_rel, 
                  e.nome AS empresa_rel 
                FROM 
                  relatorio_alta_enf
                  LEFT JOIN empresas AS e ON relatorio_alta_enf.empresa = e.id
                  LEFT JOIN planosdesaude AS pds ON relatorio_alta_enf.plano = pds.id 
                WHERE 
                  relatorio_alta_enf.ID = '{$idr}'";
        $rs = mysql_query($sql);
        $rowPaciente = mysql_fetch_array($rs);
        $empresa = null;
        $convenio = null;
        if(count($rowPaciente) > 0) {
            $empresa = $rowPaciente['empresa_rel'];
            $convenio = $rowPaciente['convenio_rel'];
        }

        echo "<center><h1>Visualizar Alta de Enfermagem</h1></center>";
        Paciente::cabecalho($id_paciente, $empresa, $convenio);

        $sql = "
		SELECT
		r.*,
		DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
		DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
		DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
		u.nome as user
		FROM
		relatorio_alta_enf as r LEFT JOIN
		usuarios as u ON (r.USUARIO_ID = u.idUsuarios)
		WHERE
		r.ID = '{$idr}'
		ORDER BY
		r.DATA DESC";

        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {

            $inicio = $row['inicio'];
            $fim = $row['fim'];
            $user = $row['user'];
            $resumo_historia = $row['RESUMO_HISTORIA'];
        }

        echo "<p><b>ALTA DE ENFERMAGEM - PER&Iacute;ODO: {$inicio} AT&Eacute; {$fim}</p>";
        echo "<p>PROFISSIONAL RESPONS&Aacute;VEL: {$user}</b></p>";

        //Mostra EvoluÃ§Ã£o clinica
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>RESUMO DA HIST&Oacute;RIA</th></tr></thead>";
        echo "<tr><td colspan='3'>{$resumo_historia}</td></tr>";
        echo "</table>";
        //Mostra OrientaÃ§Ãµes        
        $sql_alta = "
                SELECT
                    *
                FROM
                    orientacoes_relatorio_alta_enf as o
                WHERE
                    o.REL_ALTA_ID = {$idr}
                ORDER BY
                    o.DATA desc";

        $result_alta = mysql_query($sql_alta);
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>ORIENTA&Ccedil;&Otilde;ES DE CUIDADOS NO MOMENTO DA ALTA</th></tr></thead>";

        while ($row = mysql_fetch_array($result_alta)){
            switch ($row['ORIENTACOES']){
                case "1":
                    echo "<tr><td colspan='3'>Orientar quanto ao uso de medica&ccedil;&atilde;o</td></tr>";
                    break;
                case "2":
                    echo "<tr><td colspan='3'>Orientar sobre higiene &iacute;ntima, troca de fralda, preven&ccedil;&atilde;o de quedas e de feridas</td></tr>";
                    break;
                case "3":
                    echo "<tr><td colspan='3'>Orientar sobre encaminhamentos ambulatoriais</td></tr>";
                    break;
                case "4":
                    echo "<tr><td colspan='3'>Orientar sobre caso de piora como proceder</td></tr>";
                    break;
                case "5":
                    echo "<tr><td colspan='3'>Orientar sobre manuseio dos dispositivos em uso</td></tr>";
                    break;
                case "6":
                    echo "<tr><td colspan='3'>Orientar sobre realiza&ccedil;&atilde;o de procedimentos</td></tr>";
                    break;
                case "7":
                    echo "<tr><td colspan='3'>Orientado cuidador/familiar sobre curativos simples</td></tr>";
                    break;
                case "8":
                    echo "<tr><td colspan='3'>{$row['OUTRAS_ORIENTACOES']}</td></tr>";
                    break;
            }
        }


        echo "</table>";

        $responsePaciente = ModelPaciente::getById($id_paciente);

        echo "<div>
                <button caminho='imprimir_relatorio_alta.php?id={$id_paciente}&idr={$idr}' empresa-relatorio='" . ($empresa != null ? $empresa : $responsePaciente['empresa']) . "' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  role='button' aria-disabled='false'>
                    <span class='ui-button-text'>
                        Imprimir
                    </span>
                </button>
              </div>
              </div>";
    }

    public function visualizar_rel_aditivo($id_paciente,$idr,$confirmar){
        echo "<div>";

        $sql = "SELECT 
                  pds.nome AS convenio_rel, 
                  e.nome AS empresa_rel,
                  relatorio_aditivo_enf.empresa as empresaId 
                FROM 
                  relatorio_aditivo_enf
                  LEFT JOIN empresas AS e ON relatorio_aditivo_enf.empresa = e.id
                  LEFT JOIN planosdesaude AS pds ON relatorio_aditivo_enf.plano = pds.id 
                WHERE 
                  relatorio_aditivo_enf.ID = '{$idr}'";
        $rs = mysql_query($sql);
        $rowPaciente = mysql_fetch_array($rs);
        $empresa = null;
        $convenio = null;
        $empresaId = null;
        if(count($rowPaciente) > 0) {
            $empresa = $rowPaciente['empresa_rel'];
            $convenio = $rowPaciente['convenio_rel'];
            $empresaId = $rowPaciente['empresaId'];
        }

        echo "<center><h1>Visualizar Relat&oacute;rio Aditivo de Enfermagem</h1></center>";
        Paciente::cabecalho($id_paciente, $empresa, $convenio);

        $sql = "
		SELECT
		r.*,
		DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
		DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
		DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
        r.FERIDAS as feridas,
		u.nome as user
		FROM
		relatorio_aditivo_enf as r LEFT JOIN
		usuarios as u ON (r.USUARIO_ID = u.idUsuarios)
		WHERE
		r.ID = '{$idr}'
		ORDER BY
		r.DATA DESC";

        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {

            $inicio = $row['inicio'];
            $fim = $row['fim'];
            $user = $row['user'];
            $solicitacao_aditivo = $row['SOLICITACAO_ADITIVO'];
            $feridas = $row['feridas'];
        }

        $sql_problemas = "
			SELECT
                            p.*
			FROM
                            problemas_relatorio_prorrogacao_enf as p
			WHERE
                            p.RELATORIO_ADITIVO_ENF_ID = {$idr}
			ORDER BY
			ID
			";
        echo "<p><b>RELAT&Oacute;RIO ADITIVO DE ENFERMAGEM - PER&Iacute;ODO: {$inicio} AT&Eacute; {$fim}</p>";
        echo "<p>PROFISSIONAL RESPONS&Aacute;VEL: {$user}</b></p>";

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th colspan='3' >Problemas Ativos</th>";
        echo "</tr></thead>";
        $result_problemas = mysql_query($sql_problemas);
        while ($row = mysql_fetch_array($result_problemas)) {
            $cid_id = $row['CID_ID'];
            $descricao = $row['DESCRICAO'];
            $observacao = $row['OBSERVACAO'];
            echo "<tr><td width='30%'>{$descricao}</td><td colspan='2'>{$observacao}</td></tr>";
        }
        echo "</table>";
        //Mostra feridas
        if($feridas == '1'){
            $feridas_sn = "Sim";
        }else{
            $feridas_sn = "N&atilde;o";
        }
        echo"<input type=hidden value={$feridas} name='mostrar_ferida'</>";
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>ALTERA&Ccedil;&Atilde;O DO QUADRO DE LES&Otilde;ES: {$feridas_sn}</th></tr></thead>";
        echo "</table>";
        if($feridas == 1){
            $this->feridas($idr,2,4);//id do relatório, parâmetro indicador de relatorio aditivo (2), parametro para retirar botão de excluir ferida(4)
        }
        //Mostra EvoluÃ§Ã£o clinica
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>SOLICITA&Ccedil;&Atilde;O DE ADITIVO</th></tr></thead>";
        echo "<tr><td colspan='3'>{$solicitacao_aditivo}</td></tr>";
        echo "</table>";

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>Itens Solicitados</th></tr></thead>";
        echo $this->itensSolicitacaoAditivo($idr, 3);
        echo "</table>";

        if($confirmar == 0) {

            $responsePaciente = ModelPaciente::getById($id_paciente);
            $empresaRelatorio = empty($empresaId) ? $responsePaciente['empresa'] : $empresaId;


            echo "<button id='imprimir_aditivo' empresa-relatorio='{$empresaRelatorio}' caminho='/enfermagem/imprimir_relatorio_aditivo.php?id={$id_paciente}&idr={$idr}' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'><span class='ui-button-text'>Imprimir</span></button>";

//            echo"<form method=post target='_blank' action='imprimir_relatorio_aditivo.php?id={$id_paciente}&idr={$idr}'>";
//            echo"<input type=hidden value={$paciente} name='paciente'</input>";
//            echo "<button id='imprimir_aditivo' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'><span class='ui-button-text'>Imprimir</span></button>";
//            echo "<button id='voltar_' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'><span class='ui-button-text'>Voltar</span></button>";
//            echo "</form>";
        }


        echo "</div>";
    }

    public function visualizar_visita_semanal($id_paciente,$idr){
        echo "<div>";

        $sql = "SELECT 
                  pds.nome AS convenio_rel, 
                  e.nome AS empresa_rel,
                  e.id AS empresa_id
                FROM 
                  relatorio_visita_enf
                  LEFT JOIN empresas AS e ON relatorio_visita_enf.empresa = e.id
                  LEFT JOIN planosdesaude AS pds ON relatorio_visita_enf.plano = pds.id 
                WHERE 
                  relatorio_visita_enf.ID = '{$idr}'";
        $rs = mysql_query($sql);
        $rowPaciente = mysql_fetch_array($rs);
        $empresa = null;
        $convenio = null;
        if(count($rowPaciente) > 0) {
            $empresa = $rowPaciente['empresa_rel'];
            $empresa_id = $rowPaciente['empresa_id'];
            $convenio = $rowPaciente['convenio_rel'];
        }

        echo "<center><h1>Visualizar Relatório Semanal de Visita de Enfermagem</h1></center>";
        Paciente::cabecalho($id_paciente, $empresa, $convenio);

        $sql = "
		SELECT
		r.*,
		DATE_FORMAT(r.DATA_VISITA,'%d/%m/%Y') as data_visita,
		u.nome as user
		FROM
		relatorio_visita_enf as r LEFT JOIN
		usuarios as u ON (r.USUARIO_ID = u.idUsuarios)
		WHERE
		r.ID = '{$idr}'
		ORDER BY
		r.DATA DESC";

        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {

            $data_visita = $row['data_visita'];
            $user = $row['user'];
            $alteracoes_sn = $row['ALTERACOES_SN'];
            $descricao_alteracoes = $row['DESCRICAO_ALTERACOES'];
            $problemas_sn = $row['PROBLEMAS_SN'];
            $problemas_material = $row['PROBLEMAS_MATERIAL'];
            $dificuldade_sn = $row['DIFICULDADE_SN'];
            $dificuldade_profissional = $row['DIFICULDADE_PROFISSIONAL'];
            $familia_sn = $row['FAMILIA_SN'];
            $dificuldade_familia = $row['DIFICULDADE_FAMILIA'];
            $orientacoes = $row['ORIENTACOES'];
        }

        echo "<p><b>VISITA SEMANAL REALIZADA EM {$data_visita}</p>";
        echo "<p>PROFISSIONAL RESPONS&Aacute;VEL: {$user}</b></p>";

        //Mostra AlteraÃ§Ãµes Clinicas
        echo "<table class='mytable' style='width:95%;'>";
        if ($alteracoes_sn == 1){
            $alteracao = " - Sim";
            $mostrar_alteracao = "<thead><tr><th colspan='3'>ALTERA&Ccedil;&Otilde;ES</th></tr></thead><tr><td colspan='3'>{$descricao_alteracoes}</td></tr>";
        }else{
            $alteracao = "N&atilde;o";
            $mostrar_alteracao = '';
        }
        echo "<thead><tr><th colspan='3'>ALTERA&Ccedil;&Otilde;ES CL&Iacute;NICAS{$alteracao}</th></tr></thead>";
        echo $mostrar_alteracao;
        echo "</table>";
        //Mostra Problemas com Materiais        
        echo "<table class='mytable' style='width:95%;'>";
        if ($problemas_sn == 1){
            $problema = " - Sim";
            $mostrar_problemas = "<thead><tr><th colspan='3'>PROBLEMAS</th></tr></thead><tr><td colspan='3'>{$problemas_material}</td></tr>";
        }else{
            $problema = "N&atilde;o";
            $mostrar_problemas = '';
        }
        echo "<thead><tr><th colspan='3'>PROBLEMAS COM MATERIAL OU MEDICA&Ccedil;&Otilde;ES{$problema}</th></tr></thead>";
        echo $mostrar_problemas;
        echo "</table>";
        //Mostra Dificuldade com Profissional      
        echo "<table class='mytable' style='width:95%;'>";
        if ($dificuldade_sn == 1){
            $dificuldade = " - Sim";
            $mostrar_dificuldade = "<thead><tr><th colspan='3'>DETALHES</th></tr></thead><tr><td colspan='3'>{$dificuldade_profissional}</td></tr>";
        }else{
            $dificuldade = "N&atilde;o";
            $mostrar_dificuldade = '';
        }
        echo "<thead><tr><th colspan='3'>DIFICULDADES COM PROFISSIONAL{$dificuldade}</th></tr></thead>";
        echo $mostrar_dificuldade;
        echo "</table>";
        //Mostra Dificuldade com FamÃ­lia Cuidador      
        echo "<table class='mytable' style='width:95%;'>";
        if ($familia_sn == 1){
            $familia = " - Sim";
            $mostrar_familia = "<thead><tr><th colspan='3'>DETALHES</th></tr></thead><tr><td colspan='3'>{$dificuldade_familia}</td></tr>";
        }else{
            $familia = "N&atilde;o";
            $mostrar_familia = '';
        }
        echo "<thead><tr><th colspan='3'>DIFICULDADES COM FAM&Iacute;LIA/CUIDADOR{$familia}</th></tr></thead>";
        echo $mostrar_familia;
        echo "</table>";
        //Mostra OrientaÃ§Ãµes dadas Ã  familia e cuidados de enfermagem realizados
        echo "<table class='mytable' style='width:95%'>";
        echo "<thead><tr><th>ORIENTA&Ccedil;&Otilde;ES DADAS A FAM&Iacute;LIA/ CUIDADOR  E CUIDADOS DE ENFERMAGEM REALIZADOS</th></tr></thead>";
        echo "<tr><td colspan='3'>{$orientacoes}</td></tr>";
        echo "</table>";

        $responsePaciente = ModelPaciente::getById($paciente);

        echo "<div>
                <button caminho='imprimir_visita_semanal.php?id={$id_paciente}&idr={$idr}' empresa-relatorio='" . ($empresa_id != null ? $empresa_id : $responsePaciente['empresa']) . "' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  role='button' aria-disabled='false'>
                    <span class='ui-button-text'>
                        Imprimir
                    </span>
                </button>
                <button id='voltar_' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'><span class='ui-button-text'>Voltar</span></button>
              </div>";
        echo "</div>";

    }

    public function visualizar_relatorio_deflagrado($id_paciente,$idr){
        echo "<div>";

        $sql = "SELECT 
                  pds.nome AS convenio_rel, 
                  e.nome AS empresa_rel,
                  e.id as relEmpresaId,
                  clientes.empresa as empresaPaciente
                FROM 
                  relatorio_deflagrado_enf
                  inner join clientes on relatorio_deflagrado_enf.PACIENTE_ID = clientes.idClientes
                  LEFT JOIN empresas AS e ON relatorio_deflagrado_enf.empresa = e.id
                  LEFT JOIN planosdesaude AS pds ON relatorio_deflagrado_enf.plano = pds.id 
                WHERE 
                  relatorio_deflagrado_enf.ID = '{$idr}'";
        $rs = mysql_query($sql);
        $rowPaciente = mysql_fetch_array($rs);
        $empresa = null;
        $convenio = null;
        $empresaId = null;
        $empresaPaciente = null;
        if(count($rowPaciente) > 0) {
            $empresa = $rowPaciente['empresa_rel'];
            $convenio = $rowPaciente['convenio_rel'];
            $empresaId = $rowPaciente['relEmpresaId'];
            $empresaPaciente= $rowPaciente['empresaPaciente'];
        }

        echo "<center><h1>Visualizar " . htmlentities("Relatório") . " Deflagrado de Enfermagem</h1></center>";
        Paciente::cabecalho($id_paciente, $empresa, $convenio);

        $sql = "SELECT
                    r.*,
                    DATE_FORMAT(r.DATA,'%d/%m/%Y') as data,
                    DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
                    DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
                    u.nome as user
                FROM
                    relatorio_deflagrado_enf as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios)
                WHERE
                    r.ID = '{$idr}'
                ORDER BY
                    r.DATA DESC";

        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {

            $data = $row['data'];
            $inicio = $row['inicio'];
            $fim = $row['fim'];
            $user = $row['user'];
            $historia_clinica = $row['HISTORIA_CLINICA'];
            $servicos_prestados = $row['SERVICOS_PRESTADOS'];
        }

        echo "<p><b> " . htmlentities("RELATÓRIO") . " DEFLAGRADO DE ENFERMAGEM REALIZADO EM {$data}</p>";
        echo "<p>PROFISSIONAL " . htmlentities("RESPONSÁVEL") . ": {$user}</b></p>";

        $sql_problemas = "SELECT
                            p.*
                        FROM
                            problemas_relatorio_prorrogacao_enf as p
                        WHERE
                            p.RELATORIO_DEFLAGRADO_ENF_ID = {$idr}
                        ORDER BY
                        ID
            ";

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr>";
        echo "<th colspan='3' >Problemas Ativos</th>";
        echo "</tr></thead>";
        $result_problemas = mysql_query($sql_problemas);
        while ($row = mysql_fetch_array($result_problemas)) {
            $cid_id = $row['CID_ID'];
            $descricao = $row['DESCRICAO'];
            $observacao = $row['OBSERVACAO'];
            echo "<tr><td width='30%'>{$descricao}</td><td colspan='2'>{$observacao}</td></tr>";
        }
        echo "</table>";


        //Mostra Historia Clinica
        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>" . htmlentities("História Clínica") . "</th></tr></thead>";
        echo "<tr><td colspan='3'><b>História Clínica do período de $inicio à $fim </b></td></tr>";
        echo "<tr><td colspan='3'>{$historia_clinica}</td></tr>";
        echo "</table>";

        echo "<table class='mytable' style='width:95%;'>";
        echo "<thead><tr><th colspan='3'>Serviços Prestados</th></tr></thead>";
        echo "<tr><td colspan='3'>{$servicos_prestados}</td></tr>";
        echo "</table>";

        $responsePaciente = ModelPaciente::getById($paciente);

        echo "<div>
                <button caminho='imprimir_deflagrado.php?id={$id_paciente}&idr={$idr}' empresa-relatorio='" . ($empresaId != null ? $empresaId : $empresaPaciente) . "' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'  role='button' aria-disabled='false'>
                    <span class='ui-button-text'>
                        Imprimir
                    </span>
                </button>
                <button id='voltar_' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'><span class='ui-button-text'>Voltar</span></button>
              </div>";
        echo "</div>";
    }


}

switch($_POST['query']) {
    case "quadro-lesoes":
        $r = new Relatorios;
        $r->feridas($_POST['id_prorrogacao'], $_POST['tipo_relatorio'], $_POST['editar']);
        break;
}

?>
