<?php

$id = session_id();
if (empty($id))
	session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');
require __DIR__ . '/../vendor/autoload.php';

use \App\Controllers\Administracao as Administracao,
    \App\Models\Administracao\Filial;

class ImprimirRelatorio {

	public function cabecalho($id, $empresa = null, $convenio = null) {

		$condp1 = "c.idClientes = '{$id}'";
		$sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresa,
		c.`nascimento`,
		p.nome as Convenio,p.id,
		NUM_MATRICULA_CONVENIO,
		cpf
		FROM
		clientes AS c LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN
		planosdesaude as p ON (p.id = c.convenio)
		WHERE
		{$condp1}
		ORDER BY
		c.nome DESC LIMIT 1";

		$result = mysql_query($sql);
		$result2 = mysql_query($sql2);
		$html .= "<table width=100% >";
		while ($pessoa = mysql_fetch_array($result2)) {
			foreach ($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);
			}
            $pessoa['empresa'] = !empty($empresa) ? $empresa : $pessoa['empresa'];
            $pessoa['Convenio'] = !empty($convenio) ? $convenio : $pessoa['Convenio'];
			$html .= "<br/><tr>";
			$html .= "<td colspan='2'><label style='font-size:14px;color:#8B0000'><b></b></label></td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>PACIENTE </b></label></td>";
			$html .= "<td><label><b>SEXO </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>{$pessoa['paciente']}</td>";
			$html .= "<td>{$pessoa['sexo']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>IDADE </b></label></td>";
			$html .= "<td><label><b>UNIDADE REGIONAL </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>" . join("/", array_reverse(explode("-", $pessoa['nascimento']))) . ' (' . $pessoa['idade'] . " anos)</td>";
			$html .= "<td>{$pessoa['empresa']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>";
			$html .= "<td><label><b>MATR&Iacute;CULA </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>" . strtoupper($pessoa['Convenio']) . "</td>";
			$html .= "<td>" . strtoupper($pessoa['NUM_MATRICULA_CONVENIO']) . "</td>";
			$html .= "</tr>";
			$html.= " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
		}
		$html .= "</table>";

		return $html;
	}

	public function imprimir_relatorio_alta($id, $idr, $empresa) {

		$html .= "<form method='post' target='_blank'>";

        $responseEmpresa = Filial::getEmpresaById($empresa);

		$html .= "<h1><a><img src='../utils/logos/{$responseEmpresa['logo']}' width='100' ></a></h1>";

		$html.= "<h1 style='font-size:18px;text-align:center;'>RELAT&Oacute;RIO DE ALTA DE ENFERMAGEM</h1>";

        $sql = "SELECT 
                  empresas.nome AS empresa, 
                  planosdesaude.nome AS plano 
                FROM 
                  relatorio_alta_enf
                  LEFT JOIN empresas ON relatorio_alta_enf.empresa = empresas.id
                  LEFT JOIN planosdesaude ON relatorio_alta_enf.plano = planosdesaude.id
                WHERE 
                  relatorio_alta_enf.ID = '{$idr}'";
        $rs = mysql_query($sql);
        $rowRel = mysql_fetch_array($rs);

        $html .= $this->cabecalho($id, $responseEmpresa['nome'], $rowRel['plano']);

		$sql = "
		SELECT
		r.*,
		DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as rdata,
		DATE_FORMAT(r.INICIO,'%d/%m/%Y') as inicio,
		DATE_FORMAT(r.FIM,'%d/%m/%Y') as fim,
		u.nome as user,
		u.conselho_regional,
		u.tipo as tipo_usuario
		FROM
		relatorio_alta_enf as r LEFT JOIN
		usuarios as u ON (r.USUARIO_ID = u.idUsuarios)
		WHERE
		r.ID = '{$idr}'
		ORDER BY
		r.DATA DESC";

		$result = mysql_query($sql);
		while ($row = mysql_fetch_array($result)) {
			$compTipo = $row['conselho_regional'];

			$profissional = $row['USUARIO_ID'];
			$inicio = $row['inicio'];
			$fim = $row['fim'];
			$user = $row['user'] . $compTipo;
			$resumo_historia = $row['RESUMO_HISTORIA'];
		}

		$html .= "<p style='text-align:center'><b>RELAT&Oacute;RIO ADITIVO DE ENFERMAGEM DO PER&Iacute;ODO {$inicio} AT&Eacute; {$fim}</b></p>";
		$html .= "<p style='background-color:#EEEEEE;'><b>PROFISSIONAL RESPONS&Aacute;VEL: " . mb_strtoupper($user) . "</b></p>";
		//Mostra Resumo da História
		$html .= "<table class='mytable' style='width:100%;'>";
		$html .= "<tr style='background-color:#EEEEEE;'>";
		$html .= "<td colspan='5' ><b>RESUMO DA HIST&Oacute;RIA</b></td>";
		$html .= "</tr>";
		$html .= "<tr><td>{$resumo_historia}</td></tr>";
		$html .= "<tr><td colspan='5'>";
		$html .= "</table>";

		//Mostra Resumo Orientações de cuidados
		$sql_alta = "
                SELECT
                    *
                FROM
                    orientacoes_relatorio_alta_enf as o
                WHERE
                    o.REL_ALTA_ID = {$idr}
                ORDER BY
                    o.DATA desc";

		$result_alta = mysql_query($sql_alta);

		$html .= "<table class='mytable' style='width:100%;'>";
		$html .= "<tr style='background-color:#EEEEEE;'>";
		$html .= "<td colspan='5' ><b>ORIENTA&Ccedil;&Otilde;ES DE CUIDADOS NO MOMENTO DA ALTA</b></td>";
		$html .= "</tr>";
		while ($row = mysql_fetch_array($result_alta)){
			$orientacoes = $row['ORIENTACOES'];
			switch ($orientacoes){
				case "1":
					$html .= "<tr><td>Orientar quanto ao uso de medica&ccedil;&atilde;o</td></tr>";
					break;
				case "2":
					$html .= "<tr><td>Orientar sobre higiene &iacute;ntima, troca de fralda, preven&ccedil;&atilde;o de quedas e de feridas</td></tr>";
					break;
				case "3":
					$html .= "<tr><td>Orientar sobre encaminhamentos ambulatoriais</td></tr>";
					break;
				case "4":
					$html .= "<tr><td>Orientar sobre caso de piora como proceder</td></tr>";
					break;
				case "5":
					$html .= "<tr><td>Orientar sobre manuseio dos dispositivos em uso</td></tr>";
					break;
				case "6":
					$html .= "<tr><td>Orientar sobre realiza&ccedil;&atilde;o de procedimentos</td></tr>";
					break;
				case "7":
					$html .= "<tr><td>Orientado cuidador/familiar sobre curativos simples</td></tr>";
					break;
				case "8":
					$html .= "<tr><td>{$row['OUTRAS_ORIENTACOES']}</td></tr>";
					break;
			}
		}
		$html .= "<tr><td colspan='5'>";
		$html .= "</table>";


		$html .= assinaturaProResponsavel($profissional);



		$paginas [] = $header . $html.= "</form>";

		$mpdf = new mPDF('pt', 'A4', 9);
		$mpdf->SetHeader('página {PAGENO} de {nbpg}');
		$ano = date("Y");
        $mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']}"." &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));
		$mpdf->WriteHTML("<html><body>");
		$flag = false;
		foreach ($paginas as $pag) {
			if ($flag)
				$mpdf->WriteHTML("<formfeed>");
			$mpdf->WriteHTML($pag);
			$flag = true;
		}
		$mpdf->WriteHTML("</body></html>");
		$mpdf->Output('ficha_prorrogacao.pdf', 'I');
		exit;
	}

//alta
}

//imprimir_alta

$p = new ImprimirRelatorio();
$p->imprimir_relatorio_alta($_GET['id'], $_GET['idr'], $_GET['empresa']);
?>
