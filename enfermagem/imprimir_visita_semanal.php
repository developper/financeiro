<?php

$id = session_id();
if (empty($id))
    session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');
require __DIR__ . '/../vendor/autoload.php';

use \App\Controllers\Administracao as Administracao;
use \App\Models\Administracao\Filial;

class ImprimirRelatorio {

    public function cabecalho($id, $empresa = null, $convenio = null) {

        $condp1 = "c.idClientes = '{$id}'";
        $sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresa,
		c.`nascimento`,
		p.nome as Convenio,p.id,
		NUM_MATRICULA_CONVENIO,
		cpf
		FROM
		clientes AS c LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN
		planosdesaude as p ON (p.id = c.convenio)
		WHERE
		{$condp1}
		ORDER BY
		c.nome DESC LIMIT 1";

        $result = mysql_query($sql);
        $result2 = mysql_query($sql2);
        $html .= "<table width=100% >";
        while ($pessoa = mysql_fetch_array($result2)) {
            foreach ($pessoa AS $chave => $valor) {
                $pessoa[$chave] = stripslashes($valor);
            }
            $pessoa['empresa'] = !empty($empresa) ? $empresa : $pessoa['empresa'];
            $pessoa['Convenio'] = !empty($convenio) ? $convenio : $pessoa['Convenio'];
            $html .= "<br/><tr>";
            $html .= "<td colspan='2'><label style='font-size:14px;color:#8B0000'><b></b></label></td>";
            $html .= "</tr>";

            $html .= "<tr style='background-color:#EEEEEE;'>";
            $html .= "<td width='70%'><label><b>PACIENTE </b></label></td>";
            $html .= "<td><label><b>SEXO </b></label></td>";
            $html .= "</tr>";
            $html .= "<tr>";
            $html .= "<td>{$pessoa['paciente']}</td>";
            $html .= "<td>{$pessoa['sexo']}</td>";
            $html .= "</tr>";

            $html .= "<tr style='background-color:#EEEEEE;'>";
            $html .= "<td width='70%'><label><b>IDADE </b></label></td>";
            $html .= "<td><label><b>UNIDADE REGIONAL </b></label></td>";
            $html .= "</tr>";
            $html .= "<tr>";
            $html .= "<td>" . join("/", array_reverse(explode("-", $pessoa['nascimento']))) . ' (' . $pessoa['idade'] . " anos)</td>";
            $html .= "<td>{$pessoa['empresa']}</td>";
            $html .= "</tr>";

            $html .= "<tr style='background-color:#EEEEEE;'>";
            $html .= "<td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>";
            $html .= "<td><label><b>MATR&Iacute;CULA </b></label></td>";
            $html .= "</tr>";
            $html .= "<tr>";
            $html .= "<td>" . strtoupper($pessoa['Convenio']) . "</td>";
            $html .= "<td>" . strtoupper($pessoa['NUM_MATRICULA_CONVENIO']) . "</td>";
            $html .= "</tr>";
            $html.= " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
        }
        $html .= "</table>";

        return $html;
    }

    public function imprimir_visita_semanal($id, $idr, $empresa) {
        
        $html .= "<form method='post' target='_blank'>";

        $responseEmpresa = Filial::getEmpresaById($empresa);

        $html .= "<h1><a><img src='../utils/logos/{$responseEmpresa['logo']}' width='100' ></a></h1>";
        
        $html.= "<h1 style='font-size:18px;text-align:center;'>RELAT&Oacute;RIO DE VISITA SEMANAL DE ENFERMAGEM</h1>";

        $sql = "SELECT 
                  empresas.nome AS empresa, 
                  planosdesaude.nome AS plano 
                FROM 
                  relatorio_visita_enf
                  LEFT JOIN empresas ON relatorio_visita_enf.empresa = empresas.id
                  LEFT JOIN planosdesaude ON relatorio_visita_enf.plano = planosdesaude.id
                WHERE 
                  relatorio_visita_enf.ID = '{$idr}'";
        $rs = mysql_query($sql);
        $rowRel = mysql_fetch_array($rs);

        $html .= $this->cabecalho($id, $responseEmpresa['nome'], $rowRel['plano']);

        $sql = "
		SELECT
		r.*,
		DATE_FORMAT(r.DATA_VISITA,'%d/%m/%Y') as data_visita,
		u.nome as user,
		u.conselho_regional,
		u.tipo as tipo_usuario
		FROM
		relatorio_visita_enf as r LEFT JOIN
		usuarios as u ON (r.USUARIO_ID = u.idUsuarios)
		WHERE
		r.ID = '{$idr}'
		ORDER BY
		r.DATA DESC";

        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $compTipo = $row['conselho_regional'];

            $profissional = $row['USUARIO_ID'];
            $data_visita = $row['data_visita'];
            $user = $row['user'] . $compTipo;
            $alteracoes_sn = $row['ALTERACOES_SN'];            
            $descricao_alteracoes = $row['DESCRICAO_ALTERACOES'];
            $problemas_sn = $row['PROBLEMAS_SN'];
            $problemas_material = $row['PROBLEMAS_MATERIAL'];
            $dificuldade_sn = $row['DIFICULDADE_SN'];
            $dificuldade_profissional = $row['DIFICULDADE_PROFISSIONAL'];
            $familia_sn = $row['FAMILIA_SN'];
            $dificuldade_familia = $row['DIFICULDADE_FAMILIA'];
            $orientacoes = $row['ORIENTACOES'];   
        }
        $html .= "<p><b>RELAT&Oacute;RIO DE VISITA SEMANAL REALIZADO EM {$data_visita}</p>";
        $html .= "<p style='background-color:#EEEEEE;'><b>PROFISSIONAL RESPONS&Aacute;VEL: " . mb_strtoupper($user) . "</b></p>";
        //Mostra Alterações Clinicas
        $html .= "<br>";
        $html .= "<table class='mytable' style='width:100%;'>";
        if ($alteracoes_sn == 1){
            $alteracao = " - Sim";
            $mostrar_alteracao = "<tr><td colspan='4'>{$descricao_alteracoes}</td></tr>";
        }else{
            $alteracao = "N&atilde;o";
            $mostrar_alteracao = '';
        }        
        $html .= "<tr style='background-color:#EEEEEE;'>";
        $html .= "<td colspan='5' ><b>ALTERA&Ccedil;&Otilde;ES{$alteracao}</b></td></tr>";
        $html .= "<tr><td>{$mostrar_alteracao}</td></tr>";
        $html .= "</table>";        
        //Mostra Problemas com Materiais
                      
        $html .= "<br>";
        $html .= "<table class='mytable' style='width:100%;'>";
        if ($problemas_sn == 1){
            $problema = " - Sim";
            $mostrar_problemas = "<tr><td colspan='4'>{$problemas_material}</td></tr>";
        }else{
            $problema = "N&atilde;o";
            $mostrar_problemas = '';
        }
        $html .= "<tr style='background-color:#EEEEEE;'>";
        $html .= "<td colspan='5' ><b>PROBLEMAS COM MATERIAL OU MEDICA&Ccedil;&Otilde;ES{$problema}</b></td></tr>";
        $html .= $mostrar_problemas;
        $html .= "</table>";
        
        //Mostra Dificuldade com Profissional
        $html .= "<br>";
        $html .= "<table class='mytable' style='width:100%;'>";
        if ($dificuldade_sn == 1){
            $dificuldade = " - Sim";
            $mostrar_dificuldade = "<tr><td colspan='4'>{$dificuldade_profissional}</td></tr><br>";
        }else{
            $dificuldade = "N&atilde;o";
            $mostrar_dificuldade = '';
        }
        $html .= "<tr style='background-color:#EEEEEE;'>";
        $html .= "<td colspan='5' ><b>DIFICULDADES COM PROFISSIONAL{$dificuldade}</b></td></tr>";
        $html .= "<tr><td>{$mostrar_dificuldade}</td></tr>";
        $html .= "</table>";
        
        //Mostra Dificuldade com Família Cuidador
        $html .= "<br>";
        $html .= "<table class='mytable' style='width:100%;'>";
        if ($familia_sn == 1){
            $familia = " - Sim";
            $mostrar_familia = "<tr><td colspan='4'>{$dificuldade_familia}</td></tr>";
        }else{
            $familia = "N&atilde;o";
            $mostrar_familia = '';
        }
        $html .= "<tr style='background-color:#EEEEEE;'>";
        $html .= "<td colspan='5' ><b>DIFICULDADES COM FAM&Iacute;LIA/CUIDADOR{$familia}</b></td></tr>";  
        $html .= "<tr><td>{$mostrar_familia}</td></tr>";
        $html .= "</table>";
        
        //Mostra Orientações dadas à familia e cuidados de enfermagem realizados
        $html .= "<br>";
        $html .= "<table class='mytable' style='width:100%;'>";        
        $html .= "<tr style='background-color:#EEEEEE;'>";
        $html .= "<td colspan='5' ><b>ORIENTA&Ccedil;&Otilde;ES DADAS A FAM&Iacute;LIA/ CUIDADOR  E CUIDADOS DE ENFERMAGEM REALIZADOS</b></td></tr>";
        $html .= "<tr><td>{$orientacoes}</td></tr>";
        $html .= "</table>";
        
        
        $html .= assinaturaProResponsavel($profissional);



        $paginas [] = $header . $html.= "</form>";

        $mpdf = new mPDF('pt', 'A4', 8);
        $mpdf->SetHeader('página {PAGENO} de {nbpg}');
        $ano = date("Y");
        $mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']}"." &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));
        $mpdf->WriteHTML("<html><body>");
        $flag = false;
        foreach ($paginas as $pag) {
            if ($flag)
                $mpdf->WriteHTML("<formfeed>");
            $mpdf->WriteHTML($pag);
            $flag = true;
        }
        $mpdf->WriteHTML("</body></html>");
        $mpdf->Output('ficha_prorrogacao.pdf', 'I');
        exit;
    }

//alta
}

//imprimir_alta

$p = new ImprimirRelatorio();
$p->imprimir_visita_semanal($_GET['id'], $_GET['idr'], $_GET['empresa']);
?>
