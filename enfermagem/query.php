<?php

$id = session_id();
if (empty($id))
    session_start();
include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/codigos.php');
include_once('../enfermagem/enfermagem.php');

require __DIR__ . '/../vendor/autoload.php';

use App\Core\Config;
use App\Services\Gateways\SendGridGateway;
use App\Services\MailAdapter;
use App\Models\Logistica\Saidas;
use App\Models\Logistica\Solicitacoes;
use App\Services\Autorizacoes\RegraFluxoAutorizacoes;
use \App\Models\Administracao\Filial;

function opcoes($like) {
    echo "<table id='tabela-opcoes' class='mytable' width=100% >";
    echo "<thead><tr>";
    echo "<th><b>Nome</b></th>";
    echo "</tr></thead>";
    $like = anti_injection($like, 'literal');
    $cor = false;
    $total = 0;
    $sql = "SELECT DISTINCT m.nome, m.descricao, m.idMaterial FROM material as m WHERE m.nome LIKE '%$like%' OR m.descricao LIKE '%$like%' ORDER BY m.nome, m.descricao";
//    echo $sql."<br/>";
    $result = mysql_query($sql) or trigger_error(mysql_error());
    while ($row = mysql_fetch_array($result)) {
        foreach ($row AS $key => $value) {
            $row[$key] = stripslashes($value);
        }
        if ($cor)
            echo "<tr cod='{$row['idMaterial']}' >";
        else
            echo "<tr cod='{$row['idMaterial']}' class='odd'>";
        $cor = !$cor;
        echo "<td valign='top'>{$row['nome']} - {$row['descricao']}</td>";
        echo "</tr>";
    }
    echo "</table>";
    echo "<script type='text/javascript' >";

    echo " $('#tabela-opcoes tr').dblclick(function(){
	      $('#nome').val($(this).text());
	      $('#cod').val($(this).attr('cod'));
	      $('#qtd').focus();
	      $('#tabela-opcoes').html('');
	      $('#busca').val('');
	  });";
    echo "</script>";
}

function busca_itens($like, $tipo) {
    $sql = "";
    $label_tipo = "";
    $like = anti_injection($like, 'literal');
    switch ($tipo) {
        case 'kits':
            $label_tipo = "KITS";
            $sql = "SELECT k.nome as nome, k.idKit as cod FROM kitsmaterial as k WHERE k.nome LIKE '%$like%' ORDER BY k.nome";
            break;
        case 'mat':
            $label_tipo = "MATERIAIS";
            $sql = "SELECT concat(mat.nome,' ',mat.descricao) as nome, mat.idMaterial as cod FROM material as mat WHERE mat.nome LIKE '%$like%' OR mat.descricao LIKE '%like%' ORDER BY nome";
            break;
        case 'med':
            $label_tipo = "MEDICAMENTOS";
            $sql = "SELECT concat(med.principioAtivo,' ',com.nome) as nome, com.idNomeComercialMed as cod FROM medicacoes as med, nomecomercialmed as com WHERE (med.principioAtivo LIKE '%$like%' OR com.nome LIKE '%$like%') AND med.idMedicacoes = com.Medicacoes_idMedicacoes ORDER BY nome";
            break;
    }
    $result = mysql_query($sql);
    echo "<table class='mytable' width=100% id='lista-itens' ><thead><tr><th>{$label_tipo}</th></tr></thead>";
    while ($row = mysql_fetch_array($result)) {
        foreach ($row AS $key => $value) {
            $row[$key] = stripslashes($value);
        }
        echo "<tr cod='{$row['cod']}' ><td>{$row['nome']}</td></tr>";
    }
    echo "</table>";
    echo "<script type='text/javascript' >";
    echo " $('#lista-itens tr').dblclick(function(){
	      $('#nome').val($(this).text());
	      $('#cod').val($(this).attr('cod'));
	      $('#lista-itens').html('');
	      $('#busca-item').val('');
	      $(\"#formulario input[name='qtd']\").focus();
	  });";
    echo "</script>";
}

function opcoes_kits($like) {
    $like = anti_injection($like, 'literal');
    echo "<table class='mytable' width=100% id='lista-kits' ><thead><tr><th>KITS</th></tr></thead>";
    $sql = "SELECT * FROM kitsmaterial as k WHERE k.nome LIKE '%$like%' ORDER BY k.nome";
    $result = mysql_query($sql);
    while ($row = mysql_fetch_array($result)) {
        foreach ($row AS $key => $value) {
            $row[$key] = stripslashes($value);
        }
        $add = "<img align='right' class='edit-kit' kit='{$row['idKit']}' name='{$row['nome']}' cod_ref='{$row['COD_REF']}'  catalogo_id='{$row['CATALOGO_ID']}'  src='../utils/add_16x16.png' title='Adicionar material ao kit' border='0'>";
        $rem = "<img align='right' class='rem-kit' kit='{$row['idKit']}' name='{$row['nome']}' src='../utils/delete_16x16.png' title='Excluir kit' border='0'>";
        $hist = "<img align='right' class='historico-kit' kit='{$row['idKit']}' name='{$row['nome']}' src='../utils/details_16x16.png' title='Histórico de Mudanças do Kit' border='0'>";
        echo "<tr><td>{$row['nome']} {$hist} {$rem} {$add}</td></tr>";
    }
    echo "</table>";
    echo "<script type='text/javascript' >";
    echo "$('.edit-kit').click(function(){ 
		var nome = $(this).attr('name'); 
		var kit = $(this).attr('kit'); 
		var cod = $(this).attr('cod_ref');
		var catalogo_id = $(this).attr('catalogo_id');
		$('#h-nome-kit').text(nome); 
		$('#dialog-kit').attr('cod', cod); 
		$('#dialog-kit').attr('kit', kit); 
		$('#dialog-kit').attr('catalogo_id', catalogo_id); 
		$('#dialog-kit').dialog('open'); });";
    echo "$('.historico-kit').click(function(){
        var nome = $(this).attr('name');
        var kit = $(this).attr('kit');
        $('#historico-nome-kit').text(nome);
        $('#dialog-historico-kit').attr('kit', kit);
        $('#dialog-historico-kit').dialog('open'); });";
    echo "$('.rem-kit').click(function(){ 
		var nome = $(this).attr('name'); 
		var kit = $(this).attr('kit'); 
		var obj = this;
		if(confirm('Deseja realmente excluir '+nome+'?')){
			$.post('query.php',{query: 'remover-kit', kit: kit},function(r){if(r=='1') $(obj).parent().parent().remove(); else alert(r);})
		}});";
    echo "</script>";
}

function novo_kit($kit) {
    $kit = anti_injection($kit, 'literal');
    $sql = "INSERT INTO kitsmaterial (nome) VALUES ('{$kit}')";
    $a = mysql_query($sql);
    //echo  mysql_insert_id();
    if ($a) {
        $r = mysql_insert_id();
        echo $r;
        savelog(mysql_escape_string(addslashes($sql)));
    } else
        echo "ERRO: Tente mais tarde!";
}

function remover_kit($kit) {
    $kit = anti_injection($kit, 'numerico');
    $sql = "DELETE FROM kitsmaterial WHERE idKit = '{$kit}' ";
    $a = mysql_query($sql);
    if ($a) {
        savelog(mysql_escape_string(addslashes($sql)));
        echo "1";
    } else
        echo "ERRO: Tente mais tarde!";
}

function novo_item_kit($post) {
    extract($post, EXTR_OVERWRITE);
    $kit = anti_injection($kit, 'numerico');
    $cod = anti_injection($cod, 'numerico');
    $qtd = anti_injection($qtd, 'numerico');
    $catalogo_id = anti_injection($catalogo_id, 'numerico');
    $sql = "INSERT INTO itens_kit (idKit, NUMERO_TISS, qtd,CATALOGO_ID) VALUES ('{$kit}', '{$cod}', '{$qtd}','{$catalogo_id}')";
    $a = mysql_query($sql);

    $sqlHist = <<<SQL
    INSERT INTO historico_kit_enfermagem (id, kit, catalogo_id, usuario, tipo, justificativa, data)
    VALUES (NULL, '{$kit}', '{$catalogo_id}', '{$_SESSION['id_user']}', 'i', '', NOW())
SQL;
    $rs = mysql_query($sqlHist);

    if ($a) {
        savelog(mysql_escape_string(addslashes($sql)));
        lista_itens_kit($post);
    } else
        echo "-1";
}

function atualiza_cod_ref_kit($catalogo_id, $idKit) {

    $sql = "UPDATE `kitsmaterial` SET `COD_REF` = '{$catalogo_id}' WHERE `kitsmaterial`.`idKit` ='{$idKit}'";
    $a = mysql_query($sql);
    if ($a) {
        savelog(mysql_escape_string(addslashes($sql)));
    } else
        echo "-1";
}

function del_item_kit($post) {
    extract($post, EXTR_OVERWRITE);
    $id = anti_injection($id, 'numerico');
    $justificativa = anti_injection($justificativa, 'literal');

    $sqlItemKit = <<<SQL
SELECT
  *
FROM
  itens_kit
WHERE
  id = '{$id}'
SQL;
    $rs = mysql_query($sqlItemKit);
    $item = mysql_fetch_array($rs, MYSQL_ASSOC);

    $sql = "DELETE FROM itens_kit WHERE id = '{$id}' ";
    $a = mysql_query($sql);

    $sqlHist = <<<SQL
    INSERT INTO historico_kit_enfermagem (id, kit, catalogo_id, usuario, tipo, justificativa, data)
    VALUES (NULL, '{$item['idKit']}', '{$item['CATALOGO_ID']}', '{$_SESSION['id_user']}', 'e', '{$justificativa}', NOW())
SQL;
    $rs = mysql_query($sqlHist);

    if ($a) {
        savelog(mysql_escape_string(addslashes($sql)));
        echo "1";
    } else
        echo "ERRO: Tente mais tarde!";
}

function lista_itens_kit($post) {
    extract($post, EXTR_OVERWRITE);
    $sql = "SELECT i.id, i.qtd,i.NUMERO_TISS as cod,concat(b.principio,' (APR) ',b.apresentacao) as nome, i.CATALOGO_ID FROM `itens_kit` as i, `catalogo` as b WHERE i.CATALOGO_ID = b.ID AND i.idKit = {$kit}  ORDER BY `principio`, `apresentacao` ";
    $result = mysql_query($sql);
    echo "<table class='mytable' width=100% >";
    echo "<thead><tr>";
    echo "<th><b>Itens</b></th><th>Principal</th><th>Excluir</th>";
    echo "</tr></thead>";
    $checked = "";

    while ($row = mysql_fetch_array($result)) {
        foreach ($row AS $key => $value) {
            $row[$key] = stripslashes($value);
        }
        if ($cod_ref == $row['CATALOGO_ID'])
            $checked = "checked='checked'";
        else
            $checked = "";

        echo "<tr id='item-{$row['id']}'>
                <td>{$row['nome']}. Quantidade: {$row['qtd']}</td>
                <td><input type='radio' class='cod_ref' name='cod_ref' kit='{$kit}' cod='{$row['cod']}' catalogo_id= '{$row['CATALOGO_ID']}' $checked /></td>
                <td><img align='right' class='del-item-kit' item='{$row['id']}' src='../utils/delete_16x16.png' title='Remover item do kit' border='0'></td>
              </tr>
              <tr class='justificativa-exclusao-kit'>
                <td colspan='3' style='display: none;'></td>
              </tr>";
    }

    echo "</table>";
}

function lista_historico_kit($post) {
    extract($post, EXTR_OVERWRITE);
    $sql = <<<SQL
SELECT
  concat(catalogo.principio,' (APR) ', catalogo.apresentacao) as item,
  usuarios.nome AS usuario,
  historico_kit_enfermagem.tipo,
  historico_kit_enfermagem.data,
  historico_kit_enfermagem.justificativa
FROM
  historico_kit_enfermagem
  INNER JOIN catalogo ON historico_kit_enfermagem.catalogo_id = catalogo.ID
  INNER JOIN usuarios ON historico_kit_enfermagem.usuario = usuarios.idUsuarios
WHERE
  historico_kit_enfermagem.kit = '{$kit}'
SQL;
    $result = mysql_query($sql);
    echo "<table class='mytable' width=100% >";
    echo "<thead><tr>";
    echo "<th><b>Item</b></th>
          <th>Usuário</th>
          <th>Ação</th>
          <th>Justificativa</th>
          <th>Data</th>";
    echo "</tr></thead><tbody>";

    $count = mysql_num_rows($result);

    if($count > 0) {
        while ($row = mysql_fetch_array($result)) {
            foreach ($row AS $key => $value) {
                $row[$key] = stripslashes($value);
            }

            $data = \DateTime::createFromFormat('Y-m-d H:i:s', $row['data'])->format('d/m/Y à\s H:i:s');
            $acao = $row['tipo'] == 'i' ? '<label style="color:#457a1a">Inclusão</label>' : '<label style="color:darkred">Exclusão</label>';

            echo "<tr>
                <td>{$row['item']}</td>
                <td>{$row['usuario']}</td>
                <td>{$acao}</td>
                <td>{$row['justificativa']}</td>
                <td>{$data}</td>
              </tr>";
        }
    } else {
        echo "<tr>
                <td colspan='5' align='center'>Nenhum histórico encontrado!</td>
              </tr>";
    }

    echo "</tbody></table>";
}



function busca_checklist($post) {
    extract($post, EXTR_OVERWRITE);
    $id_paciente = anti_injection($id, 'numerico');
    $sql = "SELECT
                concat(bras.principio,' ', bras.apresentacao) as item,
                bras.ID as cod,
                sol.qtd as qtd,
                sol.tipo as tipo,
                DATE_FORMAT((
                    SELECT 
                            presc.inicio 
                    FROM 
                            prescricoes as presc 
                    WHERE 
                            presc.carater = 2 AND 
                            paciente = {$id_paciente} 
                     ORDER BY
                            inicio desc limit 1,1
                ),'%d/%m/%Y') as inicio,
                DATE_FORMAT((
                    SELECT 
                             max(presc.fim) 
                    FROM 
                             prescricoes as presc 
                    WHERE 
                             presc.carater = 2 AND 
                             paciente = {$id_paciente}
                 ),'%d/%m/%Y') as fim
        FROM
                solicitacoes as sol INNER JOIN
                catalogo as bras ON (sol.CATALOGO_ID = bras.ID) INNER JOIN
                prescricoes as presc
        WHERE
                (sol.enviado > 0) AND 
                (sol.paciente = {$id_paciente}) AND 
                (sol.data between inicio AND fim) AND
                (sol.tipo in (0,1,3))
        GROUP BY
                bras.principio
        UNION
        SELECT
                cb.item,
                cb.id as cod,
                sol.qtd as qtd,
                sol.tipo as tipo,
                DATE_FORMAT((
                    SELECT 
                            presc.inicio 
                    FROM 
                            prescricoes as presc 
                    WHERE 
                            presc.carater = 2 AND 
                            paciente = {$id_paciente} 
                     ORDER BY
                            inicio desc limit 1,1
                ),'%d/%m/%Y') as inicio,
                DATE_FORMAT((
                    SELECT 
                             max(presc.fim) 
                    FROM 
                             prescricoes as presc 
                    WHERE 
                             presc.carater = 2 AND 
                             paciente = {$id_paciente}
                 ),'%d/%m/%Y') as fim
        FROM
                solicitacoes as sol INNER JOIN
                catalogo as bras ON (sol.CATALOGO_ID = bras.ID) INNER JOIN
                cobrancaplanos as cb on (sol.CATALOGO_ID = cb.id) INNER JOIN
                prescricoes as presc
        WHERE
               (sol.enviado > 0) AND 
                (sol.paciente = {$id_paciente}) AND 
                (sol.data between inicio AND fim) AND
                (sol.tipo in (5))
        GROUP BY
                cb.item
        ORDER BY
                item ASC";

    $resultado = mysql_query($sql);

    $html = "<table class='mytable' style='width:95%;'>";
    $html.= "<thead><tr>";
    $html.= "<th colspan='3'>Checklist</th>";
    $html.= "</tr></thead>";
    //$html.= "<tr bgcolor='grey'><td width='48%'>Item</td><td width='7%'>Quantidade</td><td width='18%'>Per&iacute;odo</td><td>Semana 1</td><td>Semana 2</td><td>Semana 3</td><td>Semana 4</td></tr>";
    $html.= "<tr bgcolor='grey'><td width='48%'>Item</td><td width='7%'>Estoque Domiciliar</td></tr>";
    while ($row = mysql_fetch_array($resultado)) {
        //$html.= "<tr><td cod='{$row['cod']}'>{$row['item']}</td><td>{$row['qtd']}</td><td>De {$row['inicio']} at&eacute; {$row['fim']}</td><td><input type='text' class='semana_checklist' size='2'></></td><td><input type='text' size='2'></></td><td><input type='text' size='2'></></td><td><input type='text' size='2'></></td></tr>"; Decidiram tirar isso
        $html.= "<tr><td cod='{$row['cod']}' tipo='{$row['tipo']}'>{$row['item']}</td><td><input type='text' class='semana_checklist' size='10'></></td></tr>";
    }
    $html.= "</table>";
    $html.= "<br>";
    $html.= "<button id='salvar_checklist' id_paciente='{$id_paciente}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button'><span class='ui-button-text'>Salvar</span></button>";
    echo $html;
}

function salva_checklist($post) {
    extract($post, EXTR_OVERWRITE);
    $item = explode("$", $itens);
    foreach ($item as $i) {
        if ($i != "" && $i != null) {
            $e = explode("#", $i);
            $cod = anti_injection($e[0], "numerico");
            $qtd = anti_injection($e[1], "numerico");
            $tipo = anti_injection($e[2], "numerico");

            $sql = "INSERT INTO checklist_enfermagem (PACIENTE_ID,DATA,CRIADOR,CATALOGO_ID,QTD,TIPO) values ('{$id}',now(),'{$_SESSION["id_user"]}','{$cod}','{$qtd}','{$tipo}')";
            //echo $sql;
            $r = mysql_query($sql);

            if (!$r) {
                echo "Erro: tente novamente 1";
                mysql_query("rollback");

                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }
    mysql_query("commit");
    echo "1";
}

function busca_movimentacao($post) {
    extract($post, EXTR_OVERWRITE);
    $id_paciente = anti_injection($id, 'numerico');
    $inicio_estoque = implode("-", array_reverse(explode("/", $inicio)));
    $fim_estoque = implode("-", array_reverse(explode("/", $fim)));
    $sql = "SELECT
                concat(bras.principio,' ', bras.apresentacao) as item,
                bras.ID as cod,
                c.QTD as qtd,
                c.DATA as data
        FROM
                checklist_enfermagem as c INNER JOIN
                catalogo as bras ON (c.CATALOGO_ID = bras.ID)               
        WHERE
                (c.PACIENTE_ID = {$id_paciente}) AND 
                (c.DATA between '{$inicio_estoque}' AND '{$fim_estoque}')
        ORDER BY
                bras.principio ASC";
    $resultado = mysql_query($sql);

    $html = "<table class='mytable' style='width:95%;'>";
    $html.= "<thead><tr>";
    $html.= "<th colspan='3'>Controle de Estoque Domiciliar</th>";
    $html.= "</tr></thead>";
    //$html.= "<tr bgcolor='grey'><td width='48%'>Item</td><td width='7%'>Quantidade</td><td width='18%'>Per&iacute;odo</td><td>Semana 1</td><td>Semana 2</td><td>Semana 3</td><td>Semana 4</td></tr>";
    $html.= "<tr bgcolor='grey'><td width='48%'>Item</td><td>Data da Contagem</td><td width='7%'>Estoque Domiciliar</td></tr>";
    $contar_linha = '1';
    while ($row = mysql_fetch_array($resultado)) {
        if ($contar_linha % 2 == 0) {
            $zebrar_linha = "class='odd'";
        } else {
            $zebrar_linha = "";
        }
        //$html.= "<tr><td cod='{$row['cod']}'>{$row['item']}</td><td>{$row['qtd']}</td><td>De {$row['inicio']} at&eacute; {$row['fim']}</td><td><input type='text' class='semana_checklist' size='2'></></td><td><input type='text' size='2'></></td><td><input type='text' size='2'></></td><td><input type='text' size='2'></></td></tr>"; Decidiram tirar isso
        $html.= "<tr {$zebrar_linha}><td cod='{$row['cod']}'>{$row['item']}</td><td>{$row['data']}</td><td><input type='text' class='semana_checklist' size='10' value='{$row['qtd']}' readonly='readonly'></></td</tr>";
        $contar_linha++;
    }
    $html.= "</table>";
    $html.= "<br>";
    echo $html;
}

function busca_estoque($post) {
    extract($post, EXTR_OVERWRITE);
    $id_paciente = anti_injection($id, 'numerico');
    $sql = "SELECT
                concat(bras.principio,' ', bras.apresentacao) as item,
                bras.ID as cod,
                c.QTD as qtd,
                c.DATA as data_registro
        FROM
                checklist_enfermagem as c INNER JOIN
                catalogo as bras ON (c.CATALOGO_ID = bras.ID)               
        WHERE
                c.PACIENTE_ID = {$id_paciente}
        GROUP BY
                bras.principio ASC";
    $resultado = mysql_query($sql);

    $html = "<table class='mytable' style='width:95%;'>";
    $html.= "<thead><tr>";
    $html.= "<th colspan='3'>Controle de Estoque Domiciliar</th>";
    $html.= "</tr></thead>";
    //$html.= "<tr bgcolor='grey'><td width='48%'>Item</td><td width='7%'>Quantidade</td><td width='18%'>Per&iacute;odo</td><td>Semana 1</td><td>Semana 2</td><td>Semana 3</td><td>Semana 4</td></tr>";
    $html.= "<tr bgcolor='grey'><td width='48%'>Item</td><td width='7%'>Estoque Domiciliar</td><td>Data Registro</td></tr>";
    $contar_linha = '1';
    while ($row = mysql_fetch_array($resultado)) {
        if ($contar_linha % 2 == 0) {
            $zebrar_linha = "class='odd'";
        } else {
            $zebrar_linha = "";
        }
        //$html.= "<tr><td cod='{$row['cod']}'>{$row['item']}</td><td>{$row['qtd']}</td><td>De {$row['inicio']} at&eacute; {$row['fim']}</td><td><input type='text' class='semana_checklist' size='2'></></td><td><input type='text' size='2'></></td><td><input type='text' size='2'></></td><td><input type='text' size='2'></></td></tr>"; Decidiram tirar isso
        $html.= "<tr {$zebrar_linha}><td cod='{$row['cod']}'>{$row['item']}</td><td><input type='text' class='semana_checklist' size='10' value='{$row['qtd']}' readonly='readonly'></></td><td>{$row['data_registro']}</td></tr>";
        $contar_linha++;
    }
    $html.= "</table>";
    $html.= "<br>";
    echo $html;
}

function busca_avulsa($post) {
    extract($post, EXTR_OVERWRITE);
    $inicio = implode("-", array_reverse(explode("/", $inicio)));
    $fim = implode("-", array_reverse(explode("/", $fim)));
    $sql = "SELECT
				CONCAT(b.principio,' ',b.apresentacao) AS item,
				DATE_FORMAT(s.DATA_SOLICITACAO,'%d/%m/%Y') AS sdata,
				u.nome AS nome,
				s.qtd AS qtd
			FROM
				solicitacoes AS s INNER JOIN 
				catalogo AS b ON (s.CATALOGO_ID = b.ID) INNER JOIN
				usuarios AS u ON (s.enfermeiro = u.idUsuarios) 
				
			WHERE
				s.tipo IN (0, 1, 3) and s.idPrescricao = -1
                and s.DATA_SOLICITACAO BETWEEN '2013-01-01' AND '2013-01-31'
                and s.paciente = 1
				
			ORDER BY
				s.DATA_SOLICITACAO desc";

    $result = mysql_query($sql);
}

function salvar_solicitacao($post) {
    extract($post, EXTR_OVERWRITE);
    $primeira_prescricao = anti_injection($primeira_prescricao, 'numerico');
    $presc = anti_injection($presc, 'numerico');
    $paciente = anti_injection($paciente, 'numerico');
    $enfermeiro = $_SESSION["id_user"];
    $enfermeiro = anti_injection($enfermeiro, 'numerico');
    date_default_timezone_set('America/Sao_Paulo');
    $dataLimiteNow = new \DateTime();
    $dataLimit24 = new \DateTime();
    $dataLimit24->add(new \DateInterval('PT24H'));

    $sql = <<<SQL
SELECT
  p.id AS prescricao,
  c.nome AS paciente,
  u.nome AS medico,
  c.empresa AS ur,
  DATE_FORMAT(p.inicio, '%d/%m/%Y') AS inicio,
  DATE_FORMAT(p.fim, '%d/%m/%Y') AS fim,
  p.inicio AS inicio_presc,
  p.fim AS fim_presc,
  c.convenio,
  p.Carater
FROM
  prescricoes AS p
  INNER JOIN clientes AS c ON p.paciente = c.idClientes
  INNER JOIN usuarios AS u ON p.criador = u.idUsuarios
WHERE
  p.id = '{$presc}'
SQL;
    $rs = mysql_query($sql);
    $row = mysql_fetch_array($rs);
    $urPaciente = $row['ur'];

    $planoId = $row['convenio'];
    $caraterPrescricao = $row['Carater'];
    $nomePaciente = $row['paciente'];
    $msgAntencipLogist = "<p>
                            Relação de itens da primeira prescrição (#{$row['prescricao']}) do paciente <b>" . strtoupper($nomePaciente) . "</b>:<br/>
                            Criado pelo(a) médico(a): {$row['medico']} <br/>
                            Solicitado por: {$_SESSION['nome_user']} <br/>
                            Período: {$row['inicio']} à {$row['fim']} <br/>
                          </p>";
    $msgMed = "<p><b>Medicamentos:</b></p>";
    $msgMaterial = "<hr><p><b>Materiais:</b></p>";
    $msgKits = "<hr><p><b>Kits:</b></p>";
    $msgDieta = "<hr><p><b>Dietas:</b></p>";
    $msgEquip = "<hr><p><b>Equipamentos:</b></p>";

    if (isset($carater)) {
        $carater = anti_injection($carater, 'numerico');
    } else {
        echo "Erro na aplicação, problema com o carater da solicitação. Linha: ". __LINE__ . __FILE__;
        return;
    }

    if(isset($contadorEntregaImediata)){
        if($contadorEntregaImediata > 0){
            if(isset($dataEntregaImediata) && isset($horaEntregaImediata)){
                $dataHoraEntregaImediata = \DateTime::createFromFormat('d/m/Y H:i', $dataEntregaImediata . ' ' . $horaEntregaImediata);
                if($carater == -1 || $carater == 3 || $carater == 6 ){
                    if($dataHoraEntregaImediata < $dataLimiteNow){
                        echo "A data e hora da entrega imediata não pode ser menor que a data e hora atual.";
                        return;
                    }
                }
            }else{
                echo "Erro na aplicação, problema com a data e hora de entrega imediata. Linha: " . __LINE__ . __FILE__;
                return;
            }
        }
    }else{
        echo "Erro na aplicação, problema com o contador da entrega Imediata. Linha: ". __LINE__ . __FILE__;
        return;
    }

    if(isset($contadorDataAvulsa)){
        if($contadorDataAvulsa > 0){
            if(isset($dataAvulsa) && isset($horaAvulsa)){
                $dataHoraAvulsa = \DateTime::createFromFormat('d/m/Y H:i', $dataAvulsa . ' ' . $horaAvulsa);
                if($carater == -1 || $carater == 3 || $carater == 6 ){
                    if($dataHoraAvulsa < $dataLimit24){
                        echo "O praza mínimo para entrega avulsa deve ser de 24 horas.";
                        return;
                    }
                }
            }else{
                echo "Erro na aplicação, problema com a data e hora de entrega imediata. Linha: " . __LINE__ . __FILE__;
                return;
            }
        }
    }else{
        echo "Erro na aplicação, problema com o contador da data avulsa. Linha: ". __LINE__ . __FILE__;
        return;
    }



    mysql_query("begin");
    $c = 0;
    $itens = explode("$", $dados);
    $status = 0;


    //Atualiza��o Jeferson 23-03-2013

    if ($presc == -1) {

        $ano = substr(date('Y'), 2, 2);
        $chave_avulsa = "," . date("dm") . $ano . date("is");
        $campo_chave = ",CHAVE_AVULSA";
    } else {

        $chave_avulsa = "";
        $campo_chave = "";
    }

    $itens_excluidos = $itens_excluidos;

    if($itens_excluidos != '') {
        if(strpos($itens_excluidos, '$') !== false) {
            $itensExcluidos = explode('$', $itens_excluidos);
            foreach ($itensExcluidos as $itemExc) {
                $itemExc = explode('#', $itemExc);
                $itemE[$itemExc[0]][] = $itemExc[1];
            }
        } else {
            $itemE = explode('#', $itens_excluidos);
            $itemE[$itemE[0]][] = $itemE[1];
        }
    }

    //fim atualiza��o Jeferson 23-03-2013
    //if para gravar solicita��es baseadas em uma prescri��o de avalia��o	
    if ($carater == "4" || $carater == "7") {
        foreach ($itens as $item) {
            if ($item!= "" && $item != null) {
                $c++;
                $i = explode("#", str_replace("undefined", "", $item));
                $i[0] = anti_injection($i[0], 'literal');
                $i[1] = anti_injection($i[1], 'numerico');
                $i[2] = anti_injection($i[2], 'numerico');
                $i[3] = anti_injection($i[3], 'literal');
                //Quando for kit entra aqui

                if ($i[2] == "2") {
                    $compItemExcluido = "";
                    if($itensExcluidos != '') {
                        if(array_key_exists($i[0], $itemE)) {
                            $compItemExcluido = implode(', ', $itemE[$i[0]]);
                            $compItemExcluido = " AND i.id NOT IN ({$compItemExcluido})";
                        }
                    }
                    $sql = "SELECT bras.NUMERO_TISS as cod,i.CATALOGO_ID,  i.qtd as qtdmat FROM kitsmaterial as kit, catalogo as bras,
                                itens_kit as i	WHERE kit.idKit = '{$i[0]}' AND kit.idKit = i.idKit AND bras.ID = i.CATALOGO_ID {$compItemExcluido}";
                    $result = mysql_query($sql);

                    if (!$result) {
                        echo "Erro na aplicação Linha: ". __LINE__ . __FILE__;
                        mysql_query("rollback");
                        return;
                    }

                    while ($mat = mysql_fetch_array($result)) {
                        $qtd = $i[1] * $mat['qtdmat'];
                        $sql = "INSERT INTO solicitacoes (paciente,enfermeiro,NUMERO_TISS,qtd,tipo,data,status,idPrescricao$campo_chave,DATA_SOLICITACAO,TIPO_SOLICITACAO,CATALOGO_ID,KIT_ID, primeira_prescricao) VALUES ('$paciente','$enfermeiro','{$mat['cod']}','{$qtd}','1',now(),'4','{$presc}'{$chave_avulsa},now(),'{$carater}', '{$mat['CATALOGO_ID']}', '{$i[0]}', '{$primeira_prescricao}')";
                        $r = mysql_query($sql);

                        if($primeira_prescricao == '1'){
                            $msgAntencipLogist .= "";
                        }

                        if (!$r) {
                            echo "Erro na aplicação Linha: ". __LINE__ . __FILE__;
                            mysql_query("rollback");
                            return;
                        }
                        savelog(mysql_escape_string(addslashes($sql)));
                    }
                } else {
                    $sql = "INSERT INTO solicitacoes (paciente,enfermeiro,NUMERO_TISS,qtd,tipo,data,status,idPrescricao$campo_chave,DATA_SOLICITACAO,TIPO_SOLICITACAO,CATALOGO_ID, primeira_prescricao)
                                    VALUES ('$paciente','$enfermeiro','{$i[0]}','{$i[1]}','{$i[2]}',now(),'4','{$presc}'{$chave_avulsa},now(),'{$carater}', '{$i[3]}', '{$primeira_prescricao}')";
                    $r = mysql_query($sql);
                    if (!$r) {
                        echo "Erro na aplicação Linha: ". __LINE__ . __FILE__;
                        mysql_query("rollback");
                        return;
                    }
                    savelog(mysql_escape_string(addslashes($sql)));
                }
            }
        }

        savelog(mysql_escape_string(addslashes($sql)));
        $equipe = explode("$", $equipamentos);

        foreach ($equipe as $equip) {
            if ($equip != "" && $equip != null) {
                $i = explode("#", $equip);
                $cod_equip = anti_injection($i[0], "numerico");
                $obs_equip = anti_injection($i[2], "literal");
                $qtd_equip = anti_injection($i[1], "literal");
                $euipamentoAtivo = anti_injection($i[4], "literal");
                $idCapmedica = anti_injection($i[5], "literal");


                $r = mysql_query("INSERT INTO equipamentosativos (ID,COBRANCA_PLANOS_ID,CAPMEDICA_ID,QTD,OBS,DATA_INICIO,DATA_FIM,USUARIO,PRESCRICAO_AVALIACAO,PRESCRICAO_ID,PACIENTE_ID) VALUES (null,'{$cod_equip}','{$idCapmedica}','{$qtd_equip}','{$obs_equip}','{$row['inicio']}','{$row['fim']}','$enfermeiro','s','{$presc}','$paciente')");
                if (!$r) {
                    // echo "ERRO: " . $err . " " . mysql_error();
                    echo "Erro na aplicação Linha: ". __LINE__ . __FILE__;
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }
        }
        //fim if para gravar solicita��es baseadas em uma prescri��o de avalia��o	
    } else {
       // aqui
        if($presc != -1) {
            $regrasFluxo = new RegraFluxoAutorizacoes();
            if($carater == 3 || $carater == 6 ){
                $tipoSolicitacao = 'aditiva';
            }elseif($carater == 2 || $carater == 5 ){
                $tipoSolicitacao = 'prorrogacao';
            }

            $responseMedicamentos = $regrasFluxo->regrasMedicamentoByPlano($planoId, $tipoSolicitacao);
            $responseMateriais = $regrasFluxo->regrasMaterialByPlano($planoId, $tipoSolicitacao);
            $responseServicos = $regrasFluxo->regrasServicoByPlano($planoId, $tipoSolicitacao);
            $responseEquipamentos = $regrasFluxo->regrasEquipamentoByPlano($planoId, $tipoSolicitacao);
            $responseDietas = $regrasFluxo->regrasDietaByPlano($planoId, $tipoSolicitacao);

            $regrasMedicamentos = [];
            $regrasMateriais = [];
            $regrasServicos = [];
            $regrasEquipamentos = [];
            $regrasDietas = [];


            if(!empty($responseMedicamentos)){
                $regrasMedicamentos = $regrasFluxo->separarGrupoItemRegraFluxoSolicitacao($responseMedicamentos);
            }
            if(!empty($responseMateriais)){
                $regrasMateriais = $regrasFluxo->separarGrupoItemRegraFluxoSolicitacao($responseMateriais);
            }
            if(!empty($responseServicos)){
                $regrasServicos = $regrasFluxo->separarGrupoItemRegraFluxoSolicitacao($responseServicos);
            }
            if(!empty($responseEquipamentos)){
                $regrasEquipamentos = $regrasFluxo->separarGrupoItemRegraFluxoSolicitacao($responseEquipamentos);
            }
            if(!empty($responseDietas)){
                $regrasDietas = $regrasFluxo->separarGrupoItemRegraFluxoSolicitacao($responseDietas);
            }
        }




        $equipe = explode("$", $equipamentos);
        //grava equipamentos solicitados
        foreach ($equipe as $equip) {
            if ($equip != "" && $equip != null) {
                $i = explode("#", $equip);

                $cod_equip = anti_injection($i[0], "numerico");
                $qtd_equip = anti_injection($i[1], "literal");
                $obs_equip = anti_injection($i[2], "literal");
                $entregaImediata = anti_injection($i[3], "literal");
                $equipamentoAtivo = anti_injection($i[4], "literal");
                $idCapmedica = anti_injection($i[5], "literal");
                $acaoEquipamento= anti_injection($i[6], "numerico");
                $nomeEquip = anti_injection($i[7], "literal");


                //Atualiza��o Jeferson 23-03-2013
                //savelog(mysql_escape_string(addslashes($sql)));

                $autorizado = $qtd_equip;
                $dataEntrega = $entregaImediata == "S" ? $dataHoraEntregaImediata->format('Y-m-d H:i:s'):$dataHoraAvulsa->format('Y-m-d H:i:s');

                $status = $entregaImediata == "S" ? 2 : 1;

                // verificar lista de regras de fluxo ver se o item vai ser liberado ou retido na auditoria
                $condColunasAutorizarSolicitacao = ",autorizado,data_auditado,AUDITOR_ID";
                $condValoresAutorizarSolicitacao = ",'{$qtd_equip}', now(),$enfermeiro";




                if(!empty($regrasEquipamentos) && $entregaImediata != "S" && $acaoEquipamento == 1 ){


                    if(!empty($regrasEquipamentos['item']) && array_key_exists($cod_equip,$regrasEquipamentos['item'])){

                        if($regrasEquipamentos['item'][$cod_equip][0]['regra_primaria'] == 'liberar_tudo_exceto'){

                            if($regrasEquipamentos['item'][$cod_equip][0]['tipo_validacao'] == 'binaria' ){
                                $condColunasAutorizarSolicitacao = "";
                                $condValoresAutorizarSolicitacao = "";
                                $status = 0;

                            }else{
                                if($regrasEquipamentos['item'][$cod_equip][0]['campo_comparacao'] == 'quantidade'){

                                if(eval("return {$qtd_equip} {$regrasEquipamentos['item'][$cod_equip][0]['operador']} {$regrasEquipamentos['item'][$cod_equip][0]['valor_referencia']} ")){

                                        $condColunasAutorizarSolicitacao = "";
                                        $condValoresAutorizarSolicitacao = "";
                                        $status = 0;
                                    }else{
                                        $condColunasAutorizarSolicitacao = ",autorizado,data_auditado,AUDITOR_ID";
                                        $condValoresAutorizarSolicitacao = ",'{$qtd_equip}', now(),$enfermeiro";
                                        $status = 1;
                                    }

                                }
                                // não pega valor de equipamento
//                                else if($regrasEquipamentos['item'][$cod_equip][0]['campo_comparacao'] == 'valor'){
//
//                                    if(eval("return {$qtd_equip} {$regrasEquipamentos['item'][$cod_equip][0]['operador']} {$regrasEquipamentos['item'][$cod_equip][0]['valor_referencia']} ")){
//                                        $condColunasAutorizarSolicitacao = "";
//                                        $condValoresAutorizarSolicitacao = "";
//                                    }else{
//                                        $condColunasAutorizarSolicitacao = ",autorizado,data_auditado,AUDITOR_ID";
//                                        $condValoresAutorizarSolicitacao = ",'{$qtd_equip}', now(),$enfermeiro";
//                                        $status = 1;
//                                    }
//
//                                }

                            }
                        }else{


                            if($regrasEquipamentos['item'][$cod_equip][0]['tipo_validacao'] == 'binaria' ){

                                $condColunasAutorizarSolicitacao = ",autorizado,data_auditado,AUDITOR_ID";
                                $condValoresAutorizarSolicitacao = ",'{$qtd_equip}', now(),$enfermeiro";
                                $status = 1;

                            }else{
                                if($regrasEquipamentos['item'][$cod_equip][0]['campo_comparacao'] == 'quantidade'){

                                    if(eval("return {$qtd_equip} {$regrasEquipamentos['item'][$cod_equip][0]['operador']} {$regrasEquipamentos['item'][$cod_equip][0]['valor_referencia']} ")){

                                        $condColunasAutorizarSolicitacao = "";
                                        $condValoresAutorizarSolicitacao = "";
                                        $status = 0;
                                    }else{
                                        $condColunasAutorizarSolicitacao = ",autorizado,data_auditado,AUDITOR_ID";
                                        $condValoresAutorizarSolicitacao = ",'{$qtd_equip}', now(),$enfermeiro";
                                        $status = 1;
                                    }

                                }

                            }

                        }
                    }

                }

                $sql = " INSERT INTO
                        solicitacoes
                        (paciente,
                        enfermeiro,
                        NUMERO_TISS,
                        qtd,                        
                        tipo,
                        data,
                        status,
                        idPrescricao{$campo_chave},
                          DATA_SOLICITACAO,
                          TIPO_SOL_EQUIPAMENTO,
                          DATA_MAX_ENTREGA,
                          TIPO_SOLICITACAO,
                          CATALOGO_ID,
                          JUSTIFICATIVA_AVULSO,
                          entrega_imediata,
                          primeira_prescricao
                          {$condColunasAutorizarSolicitacao}
                          )
			   VALUES 
                           ('$paciente',
                            '$enfermeiro',
                            '{$cod_equip}',
                            '{$qtd_equip}',                            
                            '5',
                            now(),
                            '{$status}',
                            '{$presc}'
                            {$chave_avulsa},
                            now(),
                            '{$acaoEquipamento}',
                            '{$dataEntrega}',
                            '{$carater}',
                            '{$cod_equip}',
                            '{$obs_equip}',
                            '{$entregaImediata}',
                            '{$primeira_prescricao}'
                            {$condValoresAutorizarSolicitacao})";


                $r = mysql_query($sql);

                if($primeira_prescricao == '1') {
                    $msgEquip .= "{$qtd_equip}x {$nomeEquip} <br/>";
                }

                if (!$r) {

                    echo "Erro na aplicação Linha: ". __LINE__ . __FILE__;
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }
            ///jeferson 2012-10-23fim
        }
        //fim grava equipamentos solicitados

        foreach ($itens as $item) {
            if ($item != '' && $item != NULL) {
                $c++;
                $i = explode("#", str_replace("undefined", "", $item));
                $i[0] = anti_injection($i[0], 'literal');
                $i[1] = anti_injection($i[1], 'numerico');
                $i[2] = anti_injection($i[2], 'numerico');
                $i[3] = anti_injection($i[3], 'literal');

                $justicativa_avulso_id = anti_injection($i[4], 'literal');
                $entregaImediata = anti_injection($i[5], 'literal');
                $justificativa_outros = anti_injection($i[6], 'literal');
                $nomeItem = anti_injection($i[7], 'literal');
                if($justicativa_avulso_id == '2') {
                    $periodica = anti_injection($i[8], 'numerico');
                } else {
                    $periodica = '-1';
                }

                $dataEntrega = $entregaImediata == "S" ? $dataHoraEntregaImediata->format('Y-m-d H:i:s'):$dataHoraAvulsa->format('Y-m-d H:i:s');
                $status = $entregaImediata == "S" ? 2 : 1;
                if ($i[2] == "2") {
                    $compItemExcluido = "";
                    if($itens_excluidos != '') {
                        if(array_key_exists($i[0], $itemE)) {
                            $compItemExcluido = implode(', ', $itemE[$i[0]]);
                            $compItemExcluido = " AND i.id NOT IN ({$compItemExcluido})";
                        }
                    }
                    $sql = "SELECT"
                        . " bras.NUMERO_TISS as cod,"
                        . "CONCAT(bras.principio, '', bras.apresentacao) AS nomeItem,"
                        . "i.CATALOGO_ID, "
                        . "bras.tipo,"
                        . "i.qtd as qtdmat, "
                        ."if(bras.tipo = 1, bras.categoria_material_id,null)as categoria,
                        COALESCE(bras.VALOR_VENDA,0) as valorVenda
                        "
                        . "FROM kitsmaterial as kit,"
                        . " catalogo as bras,"
                        . " itens_kit as i	"
                        . "WHERE "
                        . "kit.idKit = '{$i[0]}' "
                        . "AND kit.idKit = i.idKit"
                        . " AND bras.ID = i.CATALOGO_ID {$compItemExcluido}";
                    $result = mysql_query($sql);



                    if (!$result) {
                        echo "Erro na aplicação Linha:". __LINE__ . __FILE__.  $sql;
                        mysql_query("rollback");
                        return;
                    }

                    while ($mat = mysql_fetch_array($result)) {
                        $qtd = $i[1] * $mat['qtdmat'];
                        $liberarColuna = ",autorizado,data_auditado,AUDITOR_ID";
                        $liberarValor = ",'{$qtd}', now(),$enfermeiro";


                        $item = [
                                    'id' => $mat['cod'],
                                    'quantidade' => $qtd,
                                    'grupo' => $mat['categoria'],
                                    'usuario' => $enfermeiro,
                                    'valor' => $mat['valorVenda']
                        ];
                        $condicaoAuditar = '';



                        if($mat['tipo'] == 1 && !empty($regrasMateriais) && $entregaImediata != "S"){
                           $condicaoAuditar = $regrasFluxo->condicionalRegraBloqueio($item,$regrasMateriais);
                        }elseif($mat['tipo'] == 3 && !empty($regrasDietas) && $entregaImediata != "S"){
                            $condicaoAuditar = $regrasFluxo->condicionalRegraBloqueio($item,$regrasDietas);
                        }elseif($mat['tipo'] == 0 && !empty($regrasMedicamentos) && $entregaImediata != "S"){
                            $condicaoAuditar = $regrasFluxo->condicionalRegraBloqueio($item,$regrasMedicamentos);
                        }

                        if(!empty($condicaoAuditar)){
                            $liberarColuna = $condicaoAuditar['coluna'];
                            $liberarValor  = $condicaoAuditar['valores'];
                            $status = $condicaoAuditar['status'];
                        }

                        $sql = "INSERT INTO solicitacoes
                                (
                                    paciente,
                                    enfermeiro,
                                    NUMERO_TISS,
                                    qtd,
                                    tipo,
                                    data,
                                    status,
                                    idPrescricao{$campo_chave},
                                    DATA_SOLICITACAO,
                                    DATA_MAX_ENTREGA,
                                    TIPO_SOLICITACAO,
                                    CATALOGO_ID,
                                    KIT_ID,
                                    entrega_imediata,
                                    JUSTIFICATIVA_AVULSO_ID,
                                    JUSTIFICATIVA_AVULSO,
                                    primeira_prescricao,
                                    id_periodica_complemento
                                    {$liberarColuna})
				     VALUES 
                                     ('$paciente',
									  '$enfermeiro',
									  '{$mat['cod']}',
									  '{$qtd}',
									  '{$mat['tipo']}',
									   now(),
									  '{$status}',
									  '{$presc}'{$chave_avulsa},
									  now(),
									  '{$dataEntrega}',
									  '{$carater}',
									  '{$mat['CATALOGO_ID']}',
									   {$i[0]},
									  '{$entregaImediata}',
									  '{$justicativa_avulso_id}',
									  '{$justificativa_outros}',
									  '{$primeira_prescricao}',
									  '{$periodica}'
									  {$liberarValor})";
                        $r = mysql_query($sql);

                        if($primeira_prescricao == '1') {
                            $msgKits .= "<li>{$qtd} x {$mat['nomeItem']}</li>";
                        }

                        if (!$r) {
                            echo "Erro na aplicação Linha:". __LINE__ . __FILE__;
                            mysql_query("rollback");
                            return;
                        }
                        savelog(mysql_escape_string(addslashes($sql)));
                    }

                    $ultimo_sol_id = mysql_insert_id();
                    $msgKits .= "</ul>";
                } else {
                    $qtd = $i[1];
                    $liberarColuna = ",autorizado,data_auditado,AUDITOR_ID";
                    $liberarValor = ",'{$qtd}', now(),$enfermeiro";

                    $item = [
                        'id' => $i[3],
                        'quantidade' => $i[1],
                        'grupo' => $i[9],
                        'usuario' => $enfermeiro,
                        'valor' => $i[8]
                    ];

                        $tipoItem = $i[2];
                    $condicaoAuditar = '';

                    if($tipoItem == 1 && !empty($regrasMateriais) && $entregaImediata != "S"){

                        $condicaoAuditar = $regrasFluxo->condicionalRegraBloqueio($item,$regrasMateriais);

                    }elseif($tipoItem == 3 && !empty($regrasDietas) && $entregaImediata != "S"){

                        $condicaoAuditar = $regrasFluxo->condicionalRegraBloqueio($item,$regrasDietas);
                    }elseif($tipoItem == 0 && !empty($regrasMedicamentos) && $entregaImediata != "S"){

                        $condicaoAuditar = $regrasFluxo->condicionalRegraBloqueio($item,$regrasMedicamentos);
                    }


                    if(!empty($condicaoAuditar)){
                        $liberarColuna = $condicaoAuditar['coluna'];
                        $liberarValor  = $condicaoAuditar['valores'];
                        $status = $condicaoAuditar['status'];
                    }

                    $catalogo_id = $i[3];

                    $sql = "INSERT INTO solicitacoes
                                (
                                paciente,
                                enfermeiro,
                                NUMERO_TISS,
                                qtd,
                                tipo,
                                data,
                                status,
                                idPrescricao{$campo_chave},
                                DATA_SOLICITACAO,
                                DATA_MAX_ENTREGA,
                                TIPO_SOLICITACAO,
                                CATALOGO_ID,
                                JUSTIFICATIVA_AVULSO,
                                JUSTIFICATIVA_AVULSO_ID,
                                entrega_imediata,
                                primeira_prescricao,
                                id_periodica_complemento
                                {$liberarColuna})
			        VALUES 
                                (
                                	'$paciente',
                                 	'$enfermeiro',
                                    '{$i[0]}',
                                    '{$qtd}',
                                    '{$i[2]}',
                                    now(),
                                    '{$status}',
                                    '{$presc}'{$chave_avulsa},
                                    now(),
                                    '{$dataEntrega}',
                                    '{$carater}',
                                    '{$catalogo_id}',
                                    '{$justificativa_outros}',
                                    '{$justicativa_avulso_id}',
                                    '{$entregaImediata}',
                                    '{$primeira_prescricao}',
                                    '{$periodica}'
                                    {$liberarValor})";
                    //fim Atualiza��o Jeferson 23-03-2013
                    $r = mysql_query($sql);

                    if($primeira_prescricao == '1') {
                        switch($i[2]) {
                            case '0':
                                $msgMed .= "{$qtd}x {$nomeItem} <br/>";
                                break;
                            case '1':
                                $msgMaterial .= "{$qtd}x {$nomeItem} <br/>";
                                break;
                            case '3':
                                $msgDieta .= "{$qtd}x {$nomeItem} <br/>";
                                break;
                        }
                    }

                    if (!$r) {
                        echo "Erro na aplicação Linha:". __LINE__ . __FILE__;
                        mysql_query("rollback");
                        return;
                    }
                    savelog(mysql_escape_string(addslashes($sql)));
                }

                $ultimo_sol_id = mysql_insert_id();
            }
        }
    }

    if ($aprazamentos != "") {
        $array_apraz = explode("$", $aprazamentos);
        foreach ($array_apraz as $apraz) {
            $a = explode("#", str_replace("undefined", "", $apraz));
            $a[0] = anti_injection($a[0], 'literal');
            $a[1] = anti_injection($a[1], 'literal');
            $sql = "UPDATE itens_prescricao SET aprazamento = '{$a['1']}' WHERE id = '{$a['0']}'";
            $r = mysql_query($sql);

            if (!$r) {
                echo "Erro na aplicação Linha:". __LINE__ . __FILE__;
                mysql_query("rollback");
                return;
            }

            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    $array_obs = explode("$", $obs);

    foreach ($array_obs as $ob) {
        if ($ob != "" && $ob != null) {
            $a = explode("#", $ob);
            $idobs = anti_injection($a[0], 'literal');
            $observacao = anti_injection($a[1], 'literal');

            $sql = "UPDATE itens_prescricao SET OBS_ENFERMAGEM= '{$observacao}' WHERE id = '{$idobs}'";
            $r = mysql_query($sql);

            if (!$r) {
                echo "Erro na aplicação Linha:". __LINE__ . __FILE__;
                mysql_query("rollback");
                return;
            }
        }

        savelog(mysql_escape_string(addslashes($sql)));
    }

    if ($presc != "-1") {
        $sql = "UPDATE prescricoes SET lockp = '9999-12-31 23:59:59', solicitado = now() WHERE id = '$presc' ";
        $r = mysql_query($sql);
        if (!$r) {
            echo "Erro na aplicação Linha:". __LINE__ . __FILE__;
            mysql_query("rollback");
            return;
        }

        if ( $contadorEntregaImediata > 0 ) {
            /* PEGANDO O NOME DO PACIENTE PARA SETAR NO PAINEL DA LOGISTICA */
            $consulta = mysql_fetch_array(mysql_query("SELECT C.nome FROM clientes AS C WHERE C.idClientes='{$paciente}'"));
            /*
              $_SESSION['NON'] = $consulta['nome'];
              $_SESSION['status'] = $status;
             */
            $arquivo = 'UR_' . $_SESSION["empresa_principal"];

            # Verifica se existe o arquivo
            if (file_exists("../logistica/chamada_logistica/{$arquivo}.txt")) {

                # Verifica se o arquivo ta vazio, caso esteja acrescenta o nome sem o |, 
                # pois o pipiline será o token para o explode gerar o array no saida.php de logistica
                # Caso contrario ele pega o conteudo do arquivo e concatena com | e o nome do proximo paciente
                if (filesize("../logistica/chamada_logistica/{$arquivo}.txt") == 0) {
                    $conteudo = $consulta['nome'];
                } else {
                    # Verificar a quantidade de pessoas dentro da lista, se for maior que a comparaçao zera a lista e 
                    # reescreve o arquivo da ur com o nome do último paciente solicitado.
                    # Caso contrário segue acumulando os nomes na lista. OBS.: Isso server para evitar listas grandes,
                    # na tela da logistica.
                    if (count(explode(" | ", file_get_contents("../logistica/chamada_logistica/{$arquivo}.txt"))) >= 6) {
                        $conteudo = $consulta['nome'];
                    } else {
                        $conteudo = file_get_contents("../logistica/chamada_logistica/{$arquivo}.txt") . ' | ' . $consulta['nome'];
                    }
                }
            } else {
                $conteudo = $consulta['nome'];
            }

            file_put_contents("../logistica/chamada_logistica/{$arquivo}.txt", $conteudo);

            //ini_put_contents("../logistica/chamada_logistica/teste.ini", $consulta['nome']);
            //array_push($_SESSION['NON'], $consulta['nome']);
            $_SESSION['status'] = $status;
        } else {
            $consulta = mysql_fetch_array(mysql_query("SELECT C.nome FROM clientes AS C WHERE C.idClientes='{$paciente}'"));
            /*
              $_SESSION['NON'] = $consulta['nome'];
              $_SESSION['status'] = $status;
             */
            array_push($_SESSION['NON'], $consulta['nome']);
            $_SESSION['status'] = $status;
        }
        savelog(mysql_escape_string(addslashes($sql)));
    }

    mysql_query("commit");

    if($primeira_prescricao == '1'){
        $para = [];
        $msgAntencipLogist .= $msgMed . $msgMaterial . $msgDieta . $msgKits . $msgEquip;
        $titulo = "Relação de Itens de Primeira Prescrição de Paciente - Sistema de Gerenciamento da Mederi";
        $emails = require $_SERVER['DOCUMENT_ROOT'] . '/app/configs/email.php';
        $para[]= ['email' => $emails['lista']['emails']['logistica'][$urPaciente]['email'], 'nome' => $emails['lista']['emails']['logistica'][$urPaciente]['nome']];
        enviarEmailSimples($titulo, $msgAntencipLogist, $para);
    }

    if($carater == 3) {
        $responseEmpresa = Filial::getEmpresaById($urPaciente);
        $msg = <<<HTML
<p>
O paciente <strong>$nomePaciente</strong> possui <strong>pedido aditivo de enfermagem</strong> disponível para orçamento.
<br>
Empresa: <b>{$responseEmpresa['nome']}</b>
</p>
HTML;

        $titulo = "Pedido Aditivo de Enfermagem para o paciente $nomePaciente";
        $para[]= ['email' => 'saad@mederi.com.br', 'nome' => 'SAAD Mederi'];
        enviarEmailSimples($titulo, $msg, $para);
    }

    if ($presc == -1) {
        echo 1;
    } else {
        echo $presc;
    }
}

//fim fun��o nova

function combo_busca_saida($tipo) {
    $sql = "SELECT idClientes as cod, nome as nome FROM clientes ORDER BY nome";
    $t = -1;
    if ($tipo == "med")
        $t = 0;
    else if ($tipo == "mat")
        $t = 1;
    if ($t != -1)
        $sql = "SELECT b.ID as cod, concat(b.principio,' - ',b.apresentacao) as nome FROM catalogo as b WHERE tipo = '{$t}' ORDER BY principio,apresentacao";
    $result = mysql_query($sql);
    echo "<p>";
    echo "<select name='codigo' style='background-color:transparent;' >";
    if ($tipo == "paciente")
        echo "<option value='todos' selected>Todos</option>";
    while ($row = mysql_fetch_array($result))
        echo "<option value='" . $row['cod'] . "'>" . $row['nome'] . "</option>";
    echo "</select>";
}

function buscar_saida($post) {
    extract($post, EXTR_OVERWRITE);
    $aSelect = "";
    $aGroupBy = "";
    echo "<center><h1>Resultado</h1></center>";
    echo "<table class='mytable' width=100% >";
    echo "<thead><tr>";
    echo "<th><b>Paciente</b></th>";
    if ($resultado == 'a') {
        echo "<th><b>Data</b></th>";
        $aSelect = ", s.data";
        $aGroupBy = " s.data,";
    }
    echo "<th><b>Nome</b></th>";
    echo "<th><b>Segundo Nome</b></th>";
    echo "<th><b>Quantidade</b></th>";
    echo "</tr></thead>";
    $cor = false;
    $total = 0;
    $i = anti_injection(implode("-", array_reverse(explode("/", $inicio))), 'literal');
    $f = anti_injection(implode("-", array_reverse(explode("/", $fim))), 'literal');
    $cond = "s.idCliente = '$codigo'";
    if ($codigo == "todos")
        $cond = "1";
    $sql = "SELECT p.nome as paciente, s.nome, s.segundoNome, sum(s.quantidade) as quantidade $aSelect FROM saida as s, clientes as p WHERE p.idClientes = s.idCliente AND $cond AND s.data BETWEEN '$i' AND '$f' GROUP BY $aGroupBy s.segundoNome, s.nome, p.nome ORDER BY p.nome, s.nome, s.segundoNome ASC";
    if ($tipo == 'med' || $tipo == 'mat') {
        $nomes = explode("#", $codigo);
        $nomes[0] = anti_injection($nomes[0], 'literal');
        $nomes[1] = anti_injection($nomes[1], 'literal');
        $sql = "SELECT p.nome as paciente, s.nome, s.segundoNome, sum(s.quantidade) as quantidade $aSelect FROM saida as s, clientes as p WHERE p.idClientes = idCliente AND s.nome = '{$nomes[0]}' AND s.segundoNome = '{$nomes[1]}' AND s.data BETWEEN '$i' AND '$f' GROUP BY $aGroupBy s.segundoNome, s.nome, p.nome ORDER BY p.nome, s.nome, s.segundoNome ASC";
    }
    $result = mysql_query($sql) or trigger_error(mysql_error());
    $f = true;
    while ($row = mysql_fetch_array($result)) {
        $f = false;
        foreach ($row AS $key => $value) {
            $row[$key] = stripslashes($value);
        }
        if ($cor)
            echo "<tr>";
        else
            echo "<tr class='odd'>";
        $cor = !$cor;
        echo "<td valign='top'>{$row['paciente']}</td>";
        if ($resultado == 'a') {
            $data = implode("/", array_reverse(explode("-", $row['data'])));
            echo "<td valign='top'>$data</td>";
        }
        echo "<td valign='top'>{$row['nome']}</td>";
        echo "<td valign='top'>{$row['segundoNome']}</td>";
        echo "<td valign='top'>{$row['quantidade']}</td>";
        echo "</tr>";
    }
    echo "</table>";
    if ($f)
        echo "<center><b>Nenhum resultado foi encontrado!</b></center>";
}

function verifica_lockp($id){//realiza consulta para verificar se lockp da prescrição é 9999-12-31 23:59:59. Se sim retorna 1 senão retorna 0
    $sql = "SELECT
                    EXISTS(
                        SELECT
                            *
                        FROM
                            prescricoes
                            WHERE
                            id = '$id' and lockp = '9999-12-31 23:59:59')";
    
        $result = mysql_query($sql);        
        $r = mysql_result($result,0);        
        return $r;
}

function refresh_lock($post) {
    extract($post, EXTR_OVERWRITE);
    if (isset($p)) {
        $p = anti_injection($p, 'numerico');
        if(!verifica_lockp($p)){
            mysql_query("UPDATE prescricoes SET lockp = DATE_ADD(now(),INTERVAL 1 MINUTE) WHERE id = '$p' ");
        }
    }
}

function release_lock($post) {
    extract($post, EXTR_OVERWRITE);
    if (isset($p)) {
        $p = anti_injection($p, 'numerico');
        if(!verifica_lockp($p)){
            mysql_query("UPDATE prescricoes SET lockp = '0001-01-01 00:00:00.000' WHERE id = '$p' ");
        }
    }
}

function desativar_prescricao($post) {
    extract($post, EXTR_OVERWRITE);
    if (isset($presc)) {
        $presc = anti_injection($presc, 'numerico');
        if (mysql_query("UPDATE prescricoes SET lockp = '9999-12-31 23:59:59', DATA_DESATIVADA = now(), DESATIVADA_POR ='{$_SESSION['id_user']}',STATUS=1 WHERE id = '$presc' "))
            echo '1';
        else
            echo '0';
    }
}

/*
  function equipamento_ativo_update($post){
  extract($post,EXTR_OVERWRITE);
  $idativo= anti_injection($idativo,'numerico');
  mysql_query("UPDATE equipamentosativos SET DATA_FIM = now() WHERE ID = '$idativo' ");


  }

 * */

//nova fun��o equipamentos



function equipamento_ativo_update($post) {
    extract($post, EXTR_OVERWRITE);
    $idativo = anti_injection($idativo, 'numerico');
    $cod_equip = anti_injection($cod_equip, 'numerico');
    $tipo_sol_equip = anti_injection($tipo, 'numerico');
    $qtd = anti_injection($qtd, 'numerico');
    $enfermeiro = $_SESSION["id_user"];
    $paciente = anti_injection($paciente, 'numerico');
    $status = anti_injection($status, "numerico");
    $presc = $prescricao;
    $data = anti_injection($data, 'literal');
    $hora = anti_injection($hora, 'literal');
    $data1 = implode("-", array_reverse(explode("/", $data)));
    $data_max_entrega = $data1 . " " . $hora . ":00";

    //mysql_query("UPDATE equipamentosativos SET DATA_FIM = now() WHERE ID = '$idativo' ");

    $sql = "INSERT INTO solicitacoes (paciente,enfermeiro,NUMERO_TISS,qtd,tipo,data,status,idPrescricao,DATA_SOLICITACAO,TIPO_SOL_EQUIPAMENTO,CATALOGO_ID,DATA_MAX_ENTREGA,TIPO_SOLICITACAO) VALUES ('$paciente','$enfermeiro','$cod_equip','$qtd','5',now(),'{$status}','{$presc}',now(),'{$tipo_sol_equip}','$cod_equip','{$data_max_entrega}','{$tipo_solicitacao}')";
    $r = mysql_query($sql);
}

//fim nova function

function createDialogPreviewPrescricao()
{
    $css = <<<CSS
.trocados {
    border-left: 5px solid red;
    list-style-type: none;
    padding-left: 5px;
}
.trocados li {
    padding: 5px;
}
CSS;
    echo <<<HTML
<style>
{$css}
</style>
HTML;
    $html = <<<HTML
<div id="dialog-preview-prescricao" title="" style="display:none;">
	<div class="wrap" style="font-size: 10pt;">
	</div>
</div>
HTML;
    return $html;
}

function buscar_prescricoes($post) {
    $css = <<<CSS
.trocados {
    border-left: 5px solid red;
    list-style-type: none;
    padding-left: 5px;
}
.trocados li {
    padding: 5px;
}
CSS;
    echo <<<HTML
<style>
{$css}
</style>
HTML;

    extract($post, EXTR_OVERWRITE);


    if(!empty($pendente)){
        $condPendente = $pendente == 'S' ? " and s.PENDENTE = 'S'" : '';

    }

    if (!isset($op))
        $op = 0;

    if ($i != 'null' && $f != 'null' && $paciente != 'null') {
        $i = implode("-", array_reverse(explode("/", $inicio)));
        $f = implode("-", array_reverse(explode("/", $fim)));

        $_SESSION['i'] = $i;
        $_SESSION['f'] = $f;
        $_SESSION['paciente'] = $paciente;
        $limit = "";
        $conddata = " AND presc.data BETWEEN '{$i}' AND '{$f}'";
        $condDataCurativo = " AND prescricao_curativo.registered_at BETWEEN  '{$i}' AND '{$f}'";
    }
    $condp = "1";
    $condPacieteCurativo = 1;

    if ($paciente != "-1") {
        $condp = "presc.paciente = '{$paciente}'";
        $condp1 = "c.idClientes = '{$paciente}'";
        $condPacieteCurativo = "clientes.idClientes = '{$paciente}'";

    }

    $sql = "SELECT
  presc.*,
  u.nome AS medico,
  DATE_FORMAT(presc.data,'%d/%m/%Y - %T') AS sdata,
  DATE_FORMAT(presc.inicio,'%d/%m/%Y') AS inicio,
  DATE_FORMAT(presc.fim,'%d/%m/%Y') AS fim,
  UPPER(c.nome) AS paciente,
  c.idClientes as CodPaciente,
  (CASE presc.carater
   WHEN '1' THEN '<label style=\'color:red;\'><b>Emergencial</b></label>'
    WHEN '2' THEN '<label style=\'color:green;\'><b>" .htmlentities("Periódica"). "</b></label>' 
				  WHEN '3' THEN concat('<label style=\'color:blue;\'><b>Aditiva ', COALESCE(presc.flag, ''),'</b></label>')
				  WHEN '4' THEN '<label style=\'color:orange;\'><b>" .htmlentities("Avaliação"). "</b></label>' 
				  WHEN '5' THEN '<label style=\'color:#B8860B;\'><b>" .htmlentities("Periódica Nutrição"). "</b></label>'
				  WHEN '6' THEN concat('<label style=\'color:#6959CD ;\'><b>Aditiva " .htmlentities("Nutrição"). " ', COALESCE(presc.flag, ''),'</b></label>')
				   WHEN '7' THEN '<label style=\'color:#9ACD32 ;\'><b>Aditiva " .htmlentities("Avaliação"). "</b></label>'
   
   WHEN '11' THEN '<label ><b>Suspens&atilde;o </b></label>'
   END) AS tipo,
   presc.carater,
FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade
FROM

prescricoes AS presc  INNER JOIN
clientes AS c ON (c.idClientes = presc.paciente) INNER JOIN
usuarios AS u ON (presc.criador = u.idUsuarios) INNER JOIN
solicitacoes as s on (presc.id = s.idPrescricao)
WHERE
{$condp}
{$conddata}
{$condPendente}
and solicitado <> '0000-00-00 00:00:00'
group by presc.id
ORDER BY
presc.data DESC {$limit}
";






    $sql2 = "SELECT
UPPER(c.nome) AS paciente,
(CASE c.sexo
	WHEN '0' THEN 'Masculino'
	WHEN '1' THEN 'Feminino'
	END) AS sexo,
FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
e.nome as empresa,
c.`nascimento`,
p.nome as Convenio,
NUM_MATRICULA_CONVENIO
FROM
clientes AS c LEFT JOIN
empresas AS e ON (e.id = c.empresa) INNER JOIN
planosdesaude as p ON (p.id = c.convenio)
WHERE
{$condp1}
ORDER BY
c.nome DESC LIMIT 1";

    $sqlCurativo= <<<SQL
SELECT
prescricao_curativo.id,
clientes.nome as paciente,
usuarios.nome as enfermeiro,
carater_prescricao.nome as carater_prescricao,
DATE_FORMAT(prescricao_curativo.registered_at,  '%d/%m/%Y - %T') as sdata,
DATE_FORMAT(prescricao_curativo.inicio, '%d/%m/%Y') as dt_inicio,
DATE_FORMAT(prescricao_curativo.fim, '%d/%m/%Y') as dt_fim,
(CASE prescricao_curativo.carater
   WHEN '9' THEN 'color:green;'
   WHEN '8' THEN 'color:blue;'
   WHEN '10' THEN 'color:orange;'
   END) as cor_carater,
   prescricao_curativo.carater,
'Prescrição de Curativo' AS tipo
FROM
solicitacoes as s
INNER JOIN prescricao_curativo ON s.id_prescricao_curativo = prescricao_curativo.id
INNER JOIN clientes ON prescricao_curativo.paciente = clientes.idClientes
INNER JOIN usuarios ON prescricao_curativo.profissional = usuarios.idUsuarios
INNER JOIN carater_prescricao ON prescricao_curativo.carater = carater_prescricao.id

WHERE
 $condPacieteCurativo
 $condDataCurativo
 $condPendente
 AND prescricao_curativo.updated_by = 0
 AND prescricao_curativo.cancelado = 0
 GROUP BY
prescricao_curativo.id
SQL;
    $result = mysql_query($sql);
    $result2 = mysql_query($sql2);
    $resultCurativo =mysql_query($sqlCurativo);
    echo createDialogPreviewPrescricao();
    echo "<table width=100% style='border:2px dashed;'>";
    while ($pessoa = mysql_fetch_array($result2)) {
        foreach ($pessoa AS $chave => $valor) {
            $pessoa[$chave] = stripslashes($valor);
        }
        echo "<br/><tr>";
        echo "<td colspan='2'><label style='font-size:14px;color:#8B0000'><b><center> " . htmlentities("INFORMAÇÕES") . " DO PACIENTE </center></b></label></td>";
        echo "</tr>";

        echo "<tr style='background-color:#EEEEEE;'>";
        echo "<td width='70%'><label><b>PACIENTE </b></label></td>";
        echo "<td><label><b>SEXO </b></label></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>{$pessoa['paciente']}</td>";
        echo "<td>{$pessoa['sexo']}</td>";
        echo "</tr>";

        echo "<tr style='background-color:#EEEEEE;'>";
        echo "<td width='70%'><label><b>IDADE </b></label></td>";
        echo "<td><label><b>UNIDADE REGIONAL </b></label></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>" . join("/", array_reverse(explode("-", $pessoa['nascimento']))) . ' (' . $pessoa['idade'] . " anos)</td>";
        echo "<td>{$pessoa['empresa']}</td>";
        echo "</tr>";

        echo "<tr style='background-color:#EEEEEE;'>";
        echo "<td width='70%'><label><b>" . htmlentities("CONVÊNIO") . " </b></label></td>";
        echo "<td><label><b>" . htmlentities("MATRÍCULA") . " </b></label></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>" . strtoupper($pessoa['Convenio']) . "</td>";
        echo "<td>" . strtoupper($pessoa['NUM_MATRICULA_CONVENIO']) . "</td>";
        echo "</tr>";
    }
    echo "</table><br/>";

    $flag = false;


    echo "<table class='mytable' width=100% >";
    echo "<thead><tr>";
    echo "<th><b>Tipo</b></th>";
    echo "<th><b>Data</b></th>";
    echo "<th><b>Profissional</b></th>";
    echo "<th><b>" . htmlentities("Período") . "</b></th>";
    echo "<th colspan='3'><b>" . htmlentities("Opções") . "</b></th>";
    $n = 0;
    echo "</tr></thead>";

    while ($row = mysql_fetch_array($result)) {
        if ($n++ % 2 == 0)
            $cor = '#E9F4F8';
        else
            $cor = '#FFFFFF';
        foreach ($row AS $key => $value) {
            $row[$key] = stripslashes($value);
        }
        $flag = true;
        $d = "<a href='?view=detalhes&p={$row['id']}&tipo=med&pendente={$pendente}&carater={$row['carater']}'><img src='../utils/details_16x16.png' title='Detalhes' border='0' class='print-presc'></a>";

        echo "<tr bgcolor='{$cor}'>
  <td>" . utf8_decode($row['tipo']) . "</td>
  <td>{$row['sdata']}</td>
  <td>{$row['medico']}</td>
  <td>{$row['inicio']} " . htmlentities("Até") . " {$row['fim']}</td>
  <td>
    <a id='{$row['id']}'
        title='Pré-visualizar esta prescrição'
        module='enfermagem'
        tipo='med'
        tipo-prescricao='" . strip_tags($row['tipo']) . "'
        inicio='{$row['inicio']}'
        fim='{$row['fim']}'
        carater={$row['carater']}
        pendente = '{$pendente}'
        class='preview'>
        <img src='../utils/look_16x16.png' />
    </a>
    {$d}
  </td>
  <td>{$p} </td>
</tr>";
    }
    while ($row = mysql_fetch_array($resultCurativo)) {
        if ($n++ % 2 == 0)
            $cor = '#E9F4F8';
        else
            $cor = '#FFFFFF';
        foreach ($row AS $key => $value) {
            $row[$key] = stripslashes($value);
        }
        $flag = true;
        $d = "<a href='?view=detalhes&p={$row['id']}&tipo=enf'><img src='../utils/details_16x16.png' title='Detalhes' border='0' class='print-presc'></a>";

        echo "<tr bgcolor={$cor}>
                  <td>
                    <label style='".$row['cor_carater']."'>
                        <b>" . htmlentities("Prescrição de Curativo") . " " . $row['carater_prescricao']. "</b>
                    </label>
                  </td>
                  <td>{$row['sdata']}</td>
                  <td>{$row['enfermeiro']}</td>
                  <td>{$row['dt_inicio']} " . htmlentities("Até") . " {$row['dt_fim']}</td>
                  <td>
                    <a id='{$row['id']}'
                        title='Pré-visualizar esta prescrição'
                        module='enfermagem'

                        tipo='enf'
                        tipo-prescricao='" . strip_tags($row['tipo']) . "'
                        inicio='{$row['dt_inicio']}'
                        fim='{$row['dt_fim']}'
                        class='preview'>
                        <img src='../utils/look_16x16.png' />
                    </a>
                    {$d}
                  </td>
                  <td>{$p} </td>
</tr>";
    }
    echo "</table>";


    //Eriton atualiza��o 18-07-2013
    extract($post, EXTR_OVERWRITE);
    $inicio = implode("-", array_reverse(explode("/", $inicio)));
    $fim = implode("-", array_reverse(explode("/", $fim)));

    $cond_paciente = 1;

    if ($paciente != '-1') {
        $cond_paciente = "s.paciente = '{$paciente}'";
    }

    $sql3 = "SELECT
CONCAT(b.principio,' ',b.apresentacao) AS item,
DATE_FORMAT(s.DATA_SOLICITACAO,'%d/%m/%Y - %T') AS sdata,
COALESCE(DATE_FORMAT(s.data_auditado, '%d/%m/%Y'), '') AS dataAuditado,
u.nome AS nome,
s.qtd AS qtd,
s.autorizado AS autorizado,
s.DATA_SOLICITACAO AS data,
s.STATUS,
s.*, 
just.JUSTIFICATIVA AS just_negado,
 just_av.justificativa,
  	DATE_FORMAT(p.inicio,'%d/%m/%Y ') AS inicio,
  	 DATE_FORMAT(p.fim,'%d/%m/%Y ')	 AS fim,
	p.Carater

FROM
solicitacoes AS s INNER JOIN
catalogo AS b ON (s.CATALOGO_ID = b.ID) INNER JOIN
usuarios AS u ON (s.enfermeiro = u.idUsuarios) LEFT JOIN
justificativa_itens_negados AS just ON (s.JUSTIFICATIVA_ITENS_NEGADOS_ID = just.ID) LEFT JOIN
justificativa_solicitacao_avulsa as just_av on (s.JUSTIFICATIVA_AVULSO_ID = just_av.id) LEFT JOIN
prescricoes AS p ON (s.id_periodica_complemento = p.id )

WHERE
  s.tipo IN (0, 1, 3) and s.idPrescricao = -1
 AND s.qtd != 0 and
DATE_FORMAT(s.DATA_SOLICITACAO,'%Y-%m-%d') BETWEEN '{$inicio}' AND '{$fim}' and
$cond_paciente
$condPendente";

    $result3 = mysql_query($sql3);
    if (mysql_num_rows($result3) > 0) {
        echo "<br>";
        echo "<label style='font-size:14px;color:#8B0000'><b><center> " . htmlentities("SOLICITAÇÕES") . " AVULSAS NO PERÍODO</center></b></label>";
        echo "<table class='mytable' width = 100% >";
        echo "<thead> <tr>";
        echo "<th><b>Item</b></th>";
        echo "<th><b>Quantidade</b></th>";
        echo "<th><b>Justificativa</b></th>";
        echo "<th><b>Solicitante</b></th>";
        echo "<th><b>Data</b></th>";
        echo "<th whidth='25%'><b>Status do Item</b></th>";
        echo "</tr></thead>";

        while ($row = mysql_fetch_array($result3)) {
            $data[] = $row;
        }

        $aux = [];
        foreach ($data as $row) {
            $aux[$row['idSolicitacoes']] = $row;
        }
        sort($aux);

        $trocados = [];

        foreach ($aux as $row) {
            if($row['trocado'] == 'S' && !in_array_r($row['idSolicitacoes'], $trocados)) {
                $trocados[$row['idSolicitacoes']] = buscarTrocas($row['idSolicitacoes']);
            }
        }




        foreach($data as $row){
            $compPeriodica = '';
            if($row['JUSTIFICATIVA_AVULSO_ID'] == 2){

                $caraterPeriod = $row['Carater'] == '2' ? 'PERIÓDICA MEDICA' : 'PERIÓDICA DE NUTRIÇÃO';
                $compPeriodica = sprintf(' DE %s DE %s À %s', $caraterPeriod, $row['inicio'], $row['fim']);
            }
          //  var_dump($compPeriodica);
            $htmlAjuste ='';
            // ajuste de quantidade liberada
            $ajustes = Solicitacoes::getAjusteBySolicitacaoId($row['idSolicitacoes']);
            $quantidadAjuste = 0;
            if(!empty($ajustes)){
                $htmlAjuste .= "<ul class='ajusteOrange'> <b>Ajuste feito pela logística:</b>";
                foreach($ajustes as $a){
                    $labelItem = $a['quantidade'] == 1 ? ' item' :' itens';
                    $quantidadAjuste += $a['quantidade'];
                    $htmlAjuste .= "<li>Abateu {$a['quantidade']} {$labelItem} em {$a['dataBr']} por {$a['usuario']}<br>
                                    <b>Justificativa: </b>{$a['justificativa']}
                              </li>";
                }
                $htmlAjuste.= "</ul>";
            }


            if($row['inserido_troca'] == 'N') {
                $enviados = empty($row['enviado']) ? 0 : $row['enviado'];
                $motivo = '';
                $msg = "<ul>";
                if($row['data_auditado'] == '0000-00-00 00:00:00' && $row['PENDENTE'] == 'N' && $row['entrega_imediata'] == 'N' && $row['status'] == 0){
                    $msg .= "<li>O item ainda não foi auditado</li>";
                }else if($row['entrega_imediata'] == 'S'){
                    $msg .= "<li>Item com caráter de Entrega Imediata. Enviado(s) = {$enviados}.</li> <li>Obs: {$row['obs']}</li>";
                }else if($row['data_auditado'] == '0000-00-00 00:00:00' && $row['PENDENTE'] == 'S' && $row['entrega_imediata'] == 'N' && empty($row['obs'])){
                    $msg .= "<li>Item com pendencia na auditoria: {$row['OBS_PENDENTE']}</li>";
                }else if($row['data_auditado'] <> '0000-00-00 00:00:00' && $row['status'] <> -1
                    &&  $row['entrega_imediata'] == 'N'){
                    $qtdAutorizado = $row['autorizado'] + $quantidadAjuste;
                    $msg .= "<li>Item liberado pela auditoria em {$row['dataAuditado']}.</li>
                             <li>Qtd autorizada: {$qtdAutorizado} </li> ";
                    if($quantidadAjuste > 0){
                        $msg .= "<li>Qtd abatida pela logística {$quantidadAjuste}.</li>";
                    }
                    $msg .= "<li>Enviado(s) pela logística: {$enviados}.</li>
                             <li>Obs: {$row['obs']}</li>";
                }else if($row['data_auditado'] <> '0000-00-00 00:00:00' && $row['status'] == -1
                    &&  $row['entrega_imediata'] == 'N'){
                    $motivo = $row['JUSTIFICATIVA_ITENS_NEGADOS_ID'] == 6 ? $row['obs'] : $row['just_negado'];
                    $msg .= "<li>Item negado pela auditoria: {$motivo}</li>";
                }else if(($row['data_auditado'] == '0000-00-00 00:00:00' && $row['entrega_imediata'] == 'N') && !empty($row['obs'])){
                    $labelEnviados = $enviados > 0 ? ". Enviado(s) = {$enviados}." : "";
                    $msg .= "<li>" . $row['obs'] . $labelEnviados . "</li>";
                }
                $msg .= "</ul>";


                if ($n++ % 2 == 0)
                    $cor = '#FFFFFF';
                else
                    $cor = '#E9F4F8';
                echo "<tr bgcolor={$cor}>
                        <td width='45%'>{$row['item']}";
                // Imprimir ajustes
                echo $htmlAjuste;

                // itens que realmente sairam para solicitação
                $saidaItemPrincipal = Saidas::getSaidaByIdSolicitacao($row['idSolicitacoes']);
                if(!empty($saidaItemPrincipal)){
                    echo "<ul class='enviadosGreen'> <b>Itens enviados pela Logística:</b>";
                    foreach($saidaItemPrincipal as $itemPrincipal){
                        echo "<li>{$itemPrincipal['item']}  <b>Qtd:</b> {$itemPrincipal['quantidade']}
                                    <b>Lote:</b> {$itemPrincipal['LOTE']} <b>Data:</b> {$itemPrincipal['data']}
                              </li>";

                    }
                    echo "</ul>";
                }


                if(array_key_exists($row['idSolicitacoes'], $trocados)){
                    sort($trocados[$row['idSolicitacoes']]);


                    echo "<ul class='trocados'> <b>Trocado pelo(s) item(ns):</b>";
                    foreach ($trocados[$row['idSolicitacoes']] as $itemTrocado) {
                        echo "<li>{$itemTrocado['idSolicitacoes']} - {$itemTrocado['produto']} ";
                        $msgItem = '';
                        if($itemTrocado['data_auditado'] == '0000-00-00 00:00:00' ){

                            $msgItem =  $itemTrocado['entrega_imediata'] == 'N' ? 'O item não foi auditado':
                                "Entrega Imediata. Enviado(s): {$itemTrocado['enviado']}";

                        }else{
                            $msgItem = "Autorizado(s) em {$row['dataAuditado']}: {$itemTrocado['autorizado']}. Enviado(s):{$itemTrocado['enviado']} ";

                        }

                                    echo" <b>$msgItem <br> Obs: {$itemTrocado['obs']}</br> </li>";
                        $saidaItemTrocado = Saidas::getSaidaByIdSolicitacao($itemTrocado['idSolicitacoes']);
                        if(!empty($saidaItemTrocado)){
                            echo "<ul class='enviadosGreen'> <b>Itens enviados pela Logística:</b>";
                            foreach($saidaItemTrocado as $item){
                                echo "<li>{$item['item']}  <b>Qtd:</b> {$item['quantidade']}
                                    <b>Lote:</b> {$item['LOTE']} <b>Data:</b> {$item['data']}
                              </li>";

                            }
                            echo "</ul>";
                        }
                    }
                    echo "</ul>
                  </td>";

                } else {
                    echo "</td>";
                }
                echo "<td>{$row['qtd']}</td>
                        <td>{$row['justificativa']} {$row['JUSTIFICATIVA_AVULSO']} {$compPeriodica}</td>
                        <td>{$row['nome']}</td>
                        <td>{$row['sdata']}</td>
                        <td width='25%'>{$msg}</td>
                      </tr>";
            }
        }
        echo "</table>";
    }else {
        echo "<br>";

        echo "<label style='font-size:14px;color:#8B0000'><b><center> " . htmlentities("NÃO EXISTEM SOLICITAÇÕES AVULSAS NO PERÍODO") . "</center></b></label>";
    }


    $sql4 = "SELECT
CONCAT(
  (CASE s.TIPO_SOL_EQUIPAMENTO
    WHEN '1' THEN '<label style=\'color:blue;\'><b>ENVIAR</b></LABEL>'
    WHEN '2' THEN '<label style=\'color:blue;\'><b>RECOLHER</b></LABEL>'
    WHEN '3' THEN '<label style=\'color:red;\'><b>SUBSTITUIR</b></LABEL>'
    END), ' ',cp.item) AS item,
DATE_FORMAT(s.DATA_SOLICITACAO,'%d/%m/%Y - %T') AS sdata,
DATE_FORMAT(s.data_auditado,'%d/%m/%Y') AS dataAuditado,
u.nome AS nome,
s.qtd AS qtd,
s.*, 
just.JUSTIFICATIVA AS just_negado,
 just_av.justificativa
FROM
solicitacoes AS s INNER JOIN 
cobrancaplanos AS cp ON (s.CATALOGO_ID = cp.id) INNER JOIN
usuarios AS u ON (s.enfermeiro = u.idUsuarios) LEFT JOIN
justificativa_itens_negados AS just ON (s.JUSTIFICATIVA_ITENS_NEGADOS_ID = just.ID) LEFT JOIN
justificativa_solicitacao_avulsa as just_av on (s.JUSTIFICATIVA_AVULSO_ID = just_av.id)

WHERE
s.tipo = 5 AND s.qtd != 0 AND
DATE_FORMAT(s.DATA_SOLICITACAO,'%Y-%m-%d') BETWEEN '{$inicio}' AND '{$fim}' and
$cond_paciente
$condPendente

ORDER BY
  sdata DESC";

    $result4 = mysql_query($sql4);
    if (mysql_num_rows($result4) > 0) {
        echo "<br>";
        echo "<label style='font-size:14px;color:#8B0000'><b><center> EQUIPAMENTOS SOLICITADOS</center></b></label>";
        echo "<table class='mytable' width = 100% >";
        echo "<thead> <tr>";
        echo "<th><b>Item</b></th>";
        echo "<th><b>Quantidade</b></th>";
        echo "<th><b>Justificativa</b></th>";
        echo "<th><b>Solicitante</b></th>";
        echo "<th><b>Data</b></th>";
        echo "<th whidth='25%'><b>Status do Item</b></th>";
        echo "</tr></thead>";

        while ($row = mysql_fetch_array($result4)) {

            $enviados = empty($row['enviado']) ? 0:$row['enviado'];
            $motivo='';
            $msg = "<ul>";
            if($row['data_auditado'] == '0000-00-00 00:00:00' && $row['PENDENTE'] == 'N' && $row['entrega_imediata'] == 'N' && $row['status'] == 0){
                $msg .= "<li>O item ainda não foi auditado</li>";
            }else if($row['entrega_imediata'] == 'S'){
                $msg .= "<li>Item com caráter de Entrega Imediata. Enviado(s) = {$enviados}.</li> <li>Obs: {$row['obs']}</li>";
            }else if($row['data_auditado'] == '0000-00-00 00:00:00' && $row['PENDENTE'] == 'S' && $row['entrega_imediata'] == 'N' && empty($row['obs'])){
                $msg .= "<li>Item com pendencia na auditoria: {$row['OBS_PENDENTE']}</li>";
            }else if($row['data_auditado'] <> '0000-00-00 00:00:00' && $row['status'] <> -1
                &&  $row['entrega_imediata'] == 'N'){
                $msg .= "<li>Item liberado pela auditoria em {$row['dataAuditado']}.</li> 
                             <li>Qtd autorizada: {$row['autorizado']}.</li> 
                             <li>Enviado(s) pela logística: {$enviados}.</li> 
                             <li>Obs: {$row['obs']}</li>";
            }else if($row['data_auditado'] <> '0000-00-00 00:00:00' && $row['status'] == -1
                &&  $row['entrega_imediata'] == 'N'){
                $motivo = $row['JUSTIFICATIVA_ITENS_NEGADOS_ID'] == 6 ? $row['obs'] : $row['just_negado'];
                $msg .= "<li>Item negado pela auditoria: {$motivo}</li>";
            }else if(($row['data_auditado'] == '0000-00-00 00:00:00' && $row['entrega_imediata'] == 'N') && !empty($row['obs'])){
                $labelEnviados = $enviados > 0 ? ". Enviado(s) = {$enviados}." : "";
                $msg .= "<li>" . $row['obs'] . $labelEnviados . "</li>";
            }
            $msg .= "</ul>";


            if ($n++ % 2 == 0)
                $cor = '#FFFFFF';
            else
                $cor = '#E9F4F8';
            echo "<tr bgcolor={$cor}>
    <td width='55%'>{$row['item']}</td>
    <td>{$row['qtd']}</td>
    <td>{$row['justificativa']} {$row['JUSTIFICATIVA_AVULSO']}</td>
    <td>{$row['nome']}</td>
    <td>{$row['sdata']}</td>
    <td width='25%'>{$msg}</td>
  </tr>";
        }
    }else {
        echo "<br>";

        echo "<label style='font-size:14px;color:#8B0000'><b><center> " . htmlentities("NÃO HÁ EQUIPAMENTOS ATIVOS") . "</center></b></label>";
    }

}

function previewPrescricao($p, $tipo, $pendente = null, $carater = null){
    $html = "";
    $condicaoIdPrescricao = $tipo == 'med' ? "idPrescricao = {$p}" : "id_prescricao_curativo = {$p} ";

    if(!empty($pendente)){
        $condPendente = $pendente == 'S' ? " and s.PENDENTE = 'S'" : '';

    }
    ini_set('display_errors',1);
    $enfermagem = new Enfermagem();
    $html .= $enfermagem->detalhePrescricao($p);


    $sql = "SELECT u.nome as enfermeiro,"
        . "DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA FROM solicitacoes as s INNER JOIN "
        . "usuarios as u on (s.enfermeiro = u.idUsuarios) WHERE $condicaoIdPrescricao  ORDER BY s.idSolicitacoes ASC limit 1";

    $result_solicitante = mysql_query($sql);
    while ($row = mysql_fetch_array($result_solicitante)){
        $solicitante = $row['enfermeiro'];
        $data = $row['DATA'];
    }
    $html .= "<p><b>Solicitante:</b> {$solicitante}";
    $html .= "<p><b>Data da Solicita&ccedil;&atilde;o:</b> {$data}";
    $html .= "<table width=100% class='mytable'><thead><tr>";
    $html .= "<th width='60%'><b>Item</b></th>";
    $html .= "<th width='10%'><b>Quantidade</b></th>";
    $html .= "<th><b>Status do  Item</b></th>";
    $html .= "</tr></thead>";

    $sql = "
  SELECT
	UPPER(c.nome),
	s.idSolicitacoes AS sol,
	DATE_FORMAT(s. DATA, '%d/%m/%Y') AS DATA,
	DATE_FORMAT(s.data_auditado,'%d/%m/%Y') AS dataAuditado,
	CONCAT(
		b.principio,
		' - ',
		b.apresentacao
	) AS produto,
	s.*,
        just.JUSTIFICATIVA AS just_negado
FROM
	solicitacoes AS s INNER JOIN
	catalogo AS b ON (s.CATALOGO_ID=b.ID) INNER JOIN
	clientes AS c ON  (s.paciente =c.idclientes )
   LEFT JOIN justificativa_itens_negados AS just ON (s.JUSTIFICATIVA_ITENS_NEGADOS_ID = just.ID)
WHERE
	s.{$condicaoIdPrescricao}

AND s.tipo IN (0, 1, 3)
AND s.qtd != 0
$condPendente

UNION
	SELECT
		UPPER(c.nome),
		s.idSolicitacoes AS sol,
		DATE_FORMAT(s. DATA, '%d/%m/%Y') AS DATA,
		DATE_FORMAT(s.data_auditado,'%d/%m/%Y') AS dataAuditado,
		cobrancaplanos.item AS produto,
		s.*,
                just.JUSTIFICATIVA AS just_negado
	FROM
		solicitacoes AS s
	INNER JOIN cobrancaplanos ON (
		cobrancaplanos.ID = s.CATALOGO_ID
	)
	INNER JOIN clientes AS c ON (s.paciente = c.idClientes)
	LEFT JOIN justificativa_itens_negados AS just ON (s.JUSTIFICATIVA_ITENS_NEGADOS_ID = just.ID)
	WHERE
		s.{$condicaoIdPrescricao}
	AND s.tipo = 5
	AND s.qtd != 0
	$condPendente
	ORDER BY
		tipo,
		produto,
		autorizado,
		enviado";

    $result = mysql_query($sql);
    $n=0;
    $data = [];
    while($row = mysql_fetch_array($result)) {
        $data[] = $row;
    }

    $aux = [];
    foreach ($data as $row) {
        $aux[$row['idSolicitacoes']] = $row;
    }
    sort($aux);


    $trocados = [];

    foreach ($aux as $row) {
        if($row['trocado'] == 'S' && !in_array_r($row['idSolicitacoes'], $trocados)) {
            $trocados[$row['idSolicitacoes']] = buscarTrocas($row['idSolicitacoes']);
        }
    }

    foreach($data as $row){

        if($row['inserido_troca'] == 'N') {
            if ($n++ % 2 == 0)
                $cor = '#E9F4F8';
            else
                $cor = '#FFFFFF';

            $i = implode("/", array_reverse(explode("-", $row['inicio'])));
            $f = implode("/", array_reverse(explode("-", $row['fim'])));
            // ajuste de quantidade liberada
            $ajustes = Solicitacoes::getAjusteBySolicitacaoId($row['idSolicitacoes']);
            $quantidadAjuste = 0;
            if(!empty($ajustes)){
                $htmlAjuste .= "<ul class='ajusteOrange'> <b>Ajuste feito pela logística:</b>";
                foreach($ajustes as $a){
                    $labelItem = $a['quantidade'] == 1 ? ' item' :' itens';
                    $quantidadAjuste += $a['quantidade'];
                    $htmlAjuste .= "<li>Abateu {$a['quantidade']} {$labelItem} em {$a['dataBr']} por {$a['usuario']}<br>
                                    <b>Justificativa: </b>{$a['justificativa']}
                              </li>";
                }
                $htmlAjuste.= "</ul>";
            }
            $qtdAutorizado = $row['autorizado'] + $quantidadAjuste;

            $enviados = empty($row['enviado']) ? 0 : $row['enviado'];
            $msg = "<ul>";
            if($row['data_auditado'] == '0000-00-00 00:00:00' && $row['PENDENTE'] == 'N' && $row['entrega_imediata'] == 'N' && $row['status'] == 0){
                $msg .= "<li>O item ainda não foi auditado</li>";
            }else if($row['entrega_imediata'] == 'S'){
                $msg .= "<li>Item com caráter de Entrega Imediata. Enviado(s) = {$enviados}.</li> <li>Obs: {$row['obs']}</li>";
            }else if($row['data_auditado'] == '0000-00-00 00:00:00' && $row['PENDENTE'] == 'S' && $row['entrega_imediata'] == 'N' && empty($row['obs'])){
                $msg .= "<li>Item com pendencia na auditoria: {$row['OBS_PENDENTE']}</li>";
            }else if($row['data_auditado'] <> '0000-00-00 00:00:00' && $row['status'] <> -1
                &&  $row['entrega_imediata'] == 'N'){
                $msg .= "<li>Item liberado pela auditoria em {$row['dataAuditado']}.</li> 
                             <li>Qtd autorizada: {$qtdAutorizado}.</li>
                             <li>Enviado(s) pela logística: {$enviados}.</li> 
                             <li>Obs: {$row['obs']}</li>";
            }else if($row['data_auditado'] <> '0000-00-00 00:00:00' && $row['status'] == -1
                &&  $row['entrega_imediata'] == 'N'){
                $motivo = $row['JUSTIFICATIVA_ITENS_NEGADOS_ID'] == 6 ? $row['obs'] : $row['just_negado'];
                $msg .= "<li>Item negado pela auditoria: {$motivo}</li>";
            }else if(($row['data_auditado'] == '0000-00-00 00:00:00' && $row['entrega_imediata'] == 'N') && !empty($row['obs'])){
                $labelEnviados = $enviados > 0 ? ". Enviado(s) = {$enviados}." : "";
                $msg .= "<li>" . $row['obs'] . $labelEnviados . "</li>";
            }
            $msg .= "</ul>";
            $html .= "<tr bgcolor={$cor} >";
            $html .= "<td>{$row['idSolicitacoes']} - {$row['produto']}";
            $html .= $htmlAjuste;

            $saidaItemPrincipal = Saidas::getSaidaByIdSolicitacao($row['idSolicitacoes']);
            if(!empty($saidaItemPrincipal)){
                $html .= "<ul class='enviadosGreen'> <b>Itens enviados pela Logística:</b>";
                foreach($saidaItemPrincipal as $itemPrincipal){
                    $html .= "<li>{$itemPrincipal['item']}  <b>Qtd:</b> {$itemPrincipal['quantidade']}
                                    <b>Lote:</b> {$itemPrincipal['LOTE']} <b>Data:</b> {$itemPrincipal['data']}
                              </li>";

                }
                $html .= "</ul>";
            }


            if(array_key_exists($row['idSolicitacoes'], $trocados)){
                sort($trocados[$row['idSolicitacoes']]);
                $html .= "<ul class='trocados'> <b>Trocado pelo(s) item(ns):</b>";
                foreach ($trocados[$row['idSolicitacoes']] as $itemTrocado) {
                    $html .= "<li>{$itemTrocado['idSolicitacoes']} - {$itemTrocado['produto']}";
                            $msgItem = '';
                        if($itemTrocado['data_auditado'] == '0000-00-00 00:00:00' ){
                            $msgItem =  $itemTrocado['entrega_imediata'] == 'N' ? 'O item não foi auditado':
                                "Entrega Imediata. Enviado(s): {$itemTrocado['enviado']}";
                        }else if($itemTrocado['data_auditado'] <> '0000-00-00 00:00:00' && $itemTrocado['status'] == -1
                            &&  $row['entrega_imediata'] == 'N'){
                            $motivo = $row['JUSTIFICATIVA_ITENS_NEGADOS_ID'] == 6 ? $row['obs'] : $row['just_negado'];
                            $msgItem .= "<li>Item negado pela auditoria: {$motivo}</li>";
                        }else{
                            $msgItem = "Autorizado(s) em {$itemTrocado['dataAuditado']}: {$itemTrocado['autorizado']}. Enviado(s):{$itemTrocado['enviado']} ";
                        }
                    $html .=" <b>$msgItem. Obs: {$itemTrocado['obs']}</b>";
                    $saidaItemTrocado = Saidas::getSaidaByIdSolicitacao($itemTrocado['idSolicitacoes']);
                    if(!empty($saidaItemTrocado)){
                        $html .= "<ul class='enviadosGreen'> <b>Itens enviados pela Logística:</b>";
                        foreach($saidaItemTrocado as $item){
                            $html .= "<li>{$item['item']}  <b>Qtd:</b> {$item['quantidade']}
                                    <b>Lote:</b> {$item['LOTE']} <b>Data:</b> {$item['data']}
                              </li>";

                        }
                        $html .="</ul>";
                    }
                }
                $html .= "</ul>
                  </td>";

            } else {
                $html .= "</td>";
            }
            $html .= "<td>{$row['qtd']}</td>";
            $html .= "<td>{$msg}</td>";
            $html .= "</tr>";
        }
    }
    if($carater == 4 || $carater == 7){
        $equipamentosAvaliacao = <<<SQL
SELECT
cobrancaplanos.item,
equipamentosativos.QTD
FROM
equipamentosativos
INNER JOIN cobrancaplanos ON equipamentosativos.COBRANCA_PLANOS_ID = cobrancaplanos.id
where
equipamentosativos.PRESCRICAO_ID = $p
AND equipamentosativos.PRESCRICAO_AVALIACAO = 's'
SQL;
        $result = mysql_query($equipamentosAvaliacao);
        $n = 0;
        $data = [];
        while($row = mysql_fetch_array($result)) {

            if($n++%2==0)
                $cor = '#E9F4F8';
            else
                $cor = '#FFFFFF';
            $html .= "<tr bgcolor={$cor} ><td>{$row['item']}</td><td>{$row['QTD']}</td><td></td></tr>";
        }

    }
    $html .= "</table>";
    return $html;
}

function in_array_r($needle, $haystack, $strict = false)
{
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}

function buscarPrescricoesAtivas($paciente)
{
    $sql = <<<SQL
SELECT
  prescricoes.id,
  prescricoes.inicio,
  prescricoes.fim,
  prescricoes.Carater,
  usuarios.nome AS profissional
FROM
  prescricoes
  INNER JOIN usuarios ON prescricoes.criador = usuarios.idUsuarios
WHERE
  prescricoes.paciente = '{$paciente}'
  AND prescricoes.Carater IN (2, 5)
  AND ( 
    prescricoes.inicio >= NOW()
    OR prescricoes.fim >= NOW()
  )
SQL;
    $rs = mysql_query($sql);
    $prescricoes = [];
    $count = mysql_num_rows($rs);

    if($count > 0) {
        while($row = mysql_fetch_array($rs)) {
            $prescricoes[$row['id']] = [
                'inicio'    => \DateTime::createFromFormat('Y-m-d', $row['inicio'])->format('d/m/Y'),
                'fim'       => \DateTime::createFromFormat('Y-m-d', $row['fim'])->format('d/m/Y'),
                'criador'   => $row['profissional'],
                'tipo'      => $row['Carater'] == '2' ? 'Periódica Médica' : 'Periódica de Nutrição',
            ];
        }
        echo json_encode($prescricoes);
    } else {
        echo '0';
    }
}

function buscaFilho($solicitacao, &$idSolicitacoes){
    $sql = <<<SQL
SELECT
  historico_troca_item.solicitacao_id_substituta
FROM
  historico_troca_item
WHERE
  historico_troca_item.solicitacao_id = '{$solicitacao}'
SQL;

    $rs = mysql_query($sql);

    while($filhos = mysql_fetch_array($rs)){
        $idSolicitacoes[$filhos['solicitacao_id_substituta']]	= $filhos['solicitacao_id_substituta'];
    }
    return $idSolicitacoes;
   }

function buscarTrocas($solicitacao, &$trocados = [])
{
    $sql = <<<SQL
SELECT
  historico_troca_item.solicitacao_id_substituta
FROM
  historico_troca_item
WHERE
  historico_troca_item.solicitacao_id = '{$solicitacao}'
SQL;

    $rs = mysql_query($sql);

    $idSolicitacoes = [];
// pega od ID das solicitações de todos os filhos da solicitação original.
    while($filhos = mysql_fetch_array($rs)){
        $idSolicitacoes[$filhos['solicitacao_id_substituta']]	= $filhos['solicitacao_id_substituta'];
    }
    $cont = 0;
    $x = count($idSolicitacoes);
// pegar o ID das solicitações dos filhos dos filhos até o fim das gerações da solicitação original.
    while($cont != $x){
        $cont = 0;
            foreach( $idSolicitacoes as $id){
                buscaFilho($id,$idSolicitacoes);
                $cont++;
            }
        $x = count($idSolicitacoes);
    }
//com todos os IDs geradas a partir da solicitação original agora pega os dados  implemento o array $trocados, com os itens que devem ser exibidos.
    foreach( $idSolicitacoes as $id){
        getSolicitacoes($id,$trocados);
    }
    return $trocados;
}

function getSolicitacoes($solicitacao, &$trocados = [])
{

$sql = <<<SQL
SELECT
  solicitacoes.*,
  DATE_FORMAT(solicitacoes.data_auditado, '%d/%m/%Y') AS dataAuditado,
  CONCAT(
		catalogo.principio,
		' - ',
		catalogo.apresentacao
	) AS produto
FROM 
  solicitacoes
  INNER JOIN catalogo ON solicitacoes.CATALOGO_ID = catalogo.ID
WHERE
  idSolicitacoes = '{$solicitacao}'

SQL;
    $rs = mysql_query($sql);
    $row = mysql_fetch_array($rs, MYSQL_ASSOC);
    // faz o filtro das solicitações que devem ser exibidas. Solicitações que foram trocadas e não tiveram nenhum item trocado
    //   não devem aparece.
   if(($row['trocado'] == 'S' && $row['enviado'] > 0) || $row['trocado'] != 'S' ){
        $trocados[$row['idSolicitacoes']] = $row;
    }
    return $trocados;
}

function buscar_ficha_avaliacao_med($paciente) {

    $id = $paciente;
    $sql3 = "select Max(id) as cap from capmedica where paciente = '{$id}' ";
    $r2 = mysql_query($sql3);
    $maior_cap = mysql_result($r2, 0);

    echo $maior_cap;
}

function buscar_ficha_evolucao_med($paciente2) {
    $id = $paciente2;
    $sql3 = "select Max(id) as ev from fichamedicaevolucao where PACIENTE_ID = '{$id}' ";

    $r2 = mysql_query($sql3);
    /* $id = $paciente;
      $sql3 = "select Max(id) as cap from capmedica where paciente = '{$id}' ";
      $r2=mysql_query($sql3); */
    $maior_cap = mysql_result($r2, 0);

    echo  $maior_cap;
}

function ficha_avaliacao_enfermagem($post) {
    extract($post, EXTR_OVERWRITE);
    mysql_query("begin");
    $hist_pregressa = anti_injection($hist_pregressa, 'literal');
    //$parecer = anti_injection($parecer, "numerico");
    $justparecer = anti_injection($pre_justificativa, "literal");
    //$id = anti_injection($id,'numerico');
    $user = $_SESSION["id_user"];
    $empresa = $_SESSION["empresa_principal"];
    //Atualiza��o Eriton 03-04-2013
    if ($integra_lesao == 1) {
        $integra = 1;
        $lesoes = 2;
    } else {
        $integra = 2;
        $lesoes = 1;
    }
    if ($icterica_anicterica == 1) {
        $icterica = 1;
        $anicterica = 2;
    } else {
        $icterica = 2;
        $anicterica = 1;
    }
    if ($hidratada_desidratada == 1) {
        $hidratada = 1;
        $desidratada = 2;
    } else {
        $hidratada = 2;
        $desidratada = 1;
    }
    if ($tipo_ventilacao == 1) {
        $ventilacao = 1;
    } else {
        $ventilacao = $tipo_ventilacao1;
    }
    if ($tipo_ventilacao1 == 2) {
        $vent_metalica = $num_ventilacao;
        $ventilacao_plastica = 0;
    } else {
        $vent_metalica = 0;
        $ventilacao_plastica = $num_ventilacao;
    }

    $hist_pregressa = anti_injection($hist_pregressa,'literal');

    $diagnostico_principal = anti_injection($diagnostico_principal,'literal');

    $hist_familiar = anti_injection($hist_familiar,'literal');

    $hist_atual = anti_injection($hist_atual,'literal');

    $queixas_atuais = anti_injection($queixas_atuais,'literal');

    $pad = anti_injection($pad,'literal');

    $sql = "SELECT convenio, empresa FROM clientes WHERE idClientes = '{$paciente}'";
    $rs = mysql_query($sql);
    $rowPaciente = mysql_fetch_array($rs);
    $empresa = null;
    $convenio = null;
    if(count($rowPaciente) > 0) {
        $empresa = $rowPaciente['empresa'];
        $convenio = $rowPaciente['convenio'];
    }

    $sql = "INSERT INTO avaliacaoenfermagem (
	USUARIO_ID,
	PACIENTE_ID,
	empresa,
	convenio,
	DATA,
	CLINICO,
	CIRURGICO,
	PALIATIVOS,
	DIAGNOSTICO_PRINCIPAL,
	HIST_PREGRESSA,
	HIST_FAMILIAR,
	QUEIXA_ATUAL,
	HIST_ATUAL,
	EST_GERAL_BOM,
	EST_GERAL_RUIM,
	NUTRIDO,
	DESNUTRIDO,
	HIGIENE_CORPORAL_SATISFATORIA,
	HIGIENE_CORPORAL_INSATISFATORIA,
	HIGIENE_BUCAL_SATISFATORIA,
	HIGIENE_BUCAL_INSATISFATORIA,
	ESTADO_EMOCIONAL,
	CONCIENTE,
	SEDADO,
	OBNUBILADO,
	TORPOROSO,
	COMATOSO,
	ORIENTADO,
	DESORIENTADO,
	SONOLENTO,
	COND_MOTORA,
	ALERGIA_MEDICAMENTO,
	ALERGIA_ALIMENTO,
	ALERGIA_MEDICAMENTO_TIPO,
	ALERGIA_ALIMENTO_TIPO,
	PELE_INTEGRA,
	PELE_LESAO,
	PELE_HIDRATADA,
	PELE_DESIDRATADA,
	PELE_ICTERICA,
	PELE_ICTERICA_ESCALA,
	PELE_ANICTERICA,
	PELE_PALIDA,
	PELE_SUDOREICA,
	PELE_FRIA,
	PELE_EDEMA,
	PELE_DESCAMACAO,
	T_ASSIMETRICO,
	T_SIMETRICO,
	EUPNEICO,
	DISPNEICO,
	TAQUIPNEICO,
	MVBD,
	RONCOS,
	SIBILOS,
	CREPTOS,
	MV_DIMINUIDO,
	BCNF,
	NORMOCARDICO,
	TAQUICARDICO,
	BRADICARDICO,
	VENTILACAO,
	VENT_TRAQUESTOMIA_METALICA_NUM,
	VENT_TRAQUESTOMIA_PLASTICA_NUM,
	BIPAP,
	OXIGENIO_SN,
	OXIGENIO,
	VENT_CATETER_OXIGENIO_NUM,
	VENT_CATETER_OXIGENIO_QUANT,
	VENT_CATETER_OCULOS_NUM,
	VENT_RESPIRADOR,
	VENT_RESPIRADOR_TIPO,
	VENT_MASCARA_VENTURY,
	VENT_MASCARA_VENTURY_NUM,
	VENT_CONCENTRADOR,
	VENT_CONCENTRADOR_NUM,
	ALT_ASPIRACOES,
	ASPIRACAO_NUM,
	ASPIRACAO_SECRECAO,
	ASPIRACAO_SECRECAO_CARACTERISTICA,
	PLANO,
	FLACIDO,
	DISTENDIDO,
	TIMPANICO,
	RHA,
	INDO_PALPACAO,
	DOLOROSO,
	DIETA_SN,
	VIA_ALIMENTACAO_ORAL,
	VIA_ALIMENTACAO_PARENTAL,
	VIA_ALIMENTACAO_VISIVEL,
	VIA_ALIMENTACAO_PARENTAL_NUM,
	VIA_ALIMENTACAO_SNE,
	VIA_ALIMENTACAO_SNE_NUM,
	VIA_ALIMENTACAO_GTM,
	VIA_ALIMENTACAO_GTM_NUM,
	VIA_ALIMENTACAO_GTM_VISIVEL,
	VIA_ALIMENTACAO_SONDA_GASTRO,
	VIA_ALIMENTACAO_SONDA_GASTRO_NUM,
	DIETA_SIST_ABERTO,
	DIETA_SIST_ABERTO_DESCRICAO,
	DIETA_SIST_FECHADO,
	DIETA_SIST_FECHADO_DESCRICAO,
	DIETA_SIST_FECHADO_VAZAO,
	DIETA_SIST_ARTESANAL,
	DIETA_SUPLEMENTO_SN,
	DIETA_NPT,
	DIETA_NPT_DESC,
	DIETA_NPT_VAZAO,
	DIETA_NPP,
	DIETA_NPP_DESC,
	DIETA_NPP_VAZAO,
	INTEGRA,
	SECRECAO,
	SANGRAMENTO,
	PRURIDO,
	EDEMA,
	TOPICOS_SN,
	DISPOSITIVOS_INTRAVENOSOS,
	DISPO_INTRAVENOSO_CVC,
	CVC,
	DISPO_INTRAVENOSO_UNO_LOCAL,
	DISPO_INTRAVENOSO_DUPLO_LOCAL,
	DISPO_INTRAVENOSO_TRIPLO_LOCAL,
	DISPO_INTRAVENOSO_AVP,
	DISPO_INTRAVENOSO_AVP_LOCAL,
	DISPO_INTRAVENOSO_PORTOCATH,
	DISPO_INTRAVENOSO_PORTOCATH_NUM,
	DISPO_INTRAVENOSO_PORTOCATH_LOCAL,
	DISPO_INTRAVENOSO_OUTROS,
	FRALDA,
	FRALDA_UNID_DIA,
	FRALDA_TAMANHO,
	DISPO_URINARIO,
	DISPO_URINARIO_NUM,
	DISPO_SONDA_FOLEY,
	DISPO_SONDA_FOLEY_NUM,
	DISPO_SONDA_ALIVIO,
	DISPO_SONDA_ALIVIO_NUM,
	DISPO_COLOSTOMIA,
	DISPO_COLOSTOMIA_NUM,
	DISPO_ILIOSTOMIA,
	DISPO_ILIOSTOMIA_NUM,
	DISPO_CISTOSTOMIA,
	DISPO_CISTOSTOMIA_NUM,
	DRENOS,
	PA,
	PA_DISTOLICA,
	FC,
	FR,
	TEMPERATURA,
	SPO2,
	PAD_ENFERMAGEM,
	DOR,
	FERIDA,
	ASPIRACAO_SECRECAO_COR,
	GLOBOSO,
	ELIMINACOES,
	USO_SISTEMA_ABERTO,
	VIA_ALIMENTACAO_GMT_PROFUNDIDADE,
	PREJUSTIFICATIVA,
	VENTILACAO_ESP_EQUIP,
    DISPO_INTRAVENOSO_CATETER_PICC,
    DISPO_INTRAVENOSO_CATETER_PICC_LOCAL,
    `medico_multidisciplinar`,
    `enfermagem_multidisciplinar`,
    `fono_multidisciplinar`,
    `fissio_motora_multidisciplinar`  ,
    `fissio_respiratoria_multidisciplinar`,
    nutricao_multidisciplinar,
    CLASSIFICACAO_NEAD

        )
        VALUES
	(
		'{$user}',
		'{$paciente}',
		'{$empresa}',
		'{$convenio}',
		now(),
		'{$clinico}',
		'{$cirurgico}',
		'{$paliativos}',
		'{$diagnostico_principal}',
		'{$hist_pregressa}',
		'{$hist_familiar}',
		'{$queixas_atuais}',
		'{$hist_atual}',
		'{$bom}',
		'{$ruim}',
		'{$nutrido}',
		'{$desnutrido}',
		'{$higiene_corpo_s}',
		'{$higiene_corpo_i}',
		'{$higiene_bucal_s}',
		'{$higiene_bucal_i}',
		'{$estado_emocional}',
		'{$consciente}',
		'{$sedado}',
		'{$obnublado}',
		'{$torporoso}',
		'{$comatoso}',
		'{$orientado}',
		'{$desorientado}',
		'{$sonolento}',
		'{$cond_motora}',
		'{$palergiamed}',
		'{$palergiaalim}',
		'{$alergiamed}',
		'{$alergiaalim}',
		'{$integra}',
		'{$lesoes}',
		'{$hidratada}',
		'{$desidratada}',
		'{$icterica}',
		'{$icterica_escala}',
		'{$anicterica}',
		'{$palida}',
		'{$sudoreica}',
		'{$fria}',
		'{$pele_edema}',
		'{$descamacao}',
		'{$t_assimetrico}',
		'{$t_simetrico}',
		'{$eupneico}',
		'{$dispneico}',
		'{$taquipneico}',
		'{$mvbd}',
		'{$roncos}',
		'{$sibilos}',
		'{$creptos}',
		'{$mv_diminuido}',
		'{$bcnf}',
		'{$normocardico}',
		'{$taquicardico}',
		'{$bradicardico}',
		'{$ventilacao}',
		'{$vent_metalica}',
		'{$ventilacao_plastica}',
		'{$bipap}',
		'{$oxigenio_sn}',
		'{$oxigenio}',
		'{$vent_cateter_oxigenio_num}',
		'{$vent_cateter_oxigenio_quant}',
		'{$vent_cateter_oculos_num}',
		'{$vent_respirador}',
		'{$vent_respirador_tipo}',
		'{$vent_mascara_ventury}',
		'{$vent_mascara_ventury_tipo}',
		'{$vent_concentrador}',
		'{$vent_concentrador_num}',
		'{$alt_aspiracoes}',
		'{$aspiracoes_num}',
		'{$aspiracao_secrecao}',
		'{$caracteristicas_sec}',
		'{$plano}',
		'{$flacido}',
		'{$distendido}',
		'{$timpanico}',
		'{$rha}',
		'{$indo_palpacao}',
		'{$doloroso}',
		'{$dieta_sn}',
		'{$oral}',
		'{$parental}',
		'{$visivel}',
		'{$parental_num}',
		'{$sne}',
		'{$sne_num}',
		'{$gtm_sonda_gastro}',
		'{$gtm_num}',
		'{$gtm_visivel}',
		'{$sonda_gastro}',
		'{$quant_sonda}',
		'{$sistema_aberto}',
		'{$quant_sistema_aberto}',
		'{$sistema_fechado}',
		'{$quant_sistema_fechado}',
		'{$vazao}',
		'{$dieta_artesanal}',
		'{$suplemento_sn}',
		'{$dieta_npt}',
		'{$desc_npt}',
		'{$vazao_npt}',
		'{$npp}',
		'{$desc_npp}',
		'{$vazao_npp}',
		'{$integra_genital}',
		'{$secrecao}',
		'{$sangramento}',
		'{$prurido}',
		'{$edema}',
		'{$topicos_sn}',
		'{$dispositivos_intravenosos}',
		'{$cateter_venoso_central}',
		'{$cvc}',
		'{$local_uno}',
		'{$local_duplo}',
		'{$local_triplo}',
		'{$avp}',
		'{$avp_local}',
		'{$port}',
		'{$quant_port}',
		'{$port_local}',
		'{$outros_intra}',
		'{$fralda}',
		'{$fralda_dia}',
		'{$tamanho_fralda}',
		'{$urinario}',
		'{$quant_urinario}',
		'{$sonda}',
		'{$quant_foley}',
		'{$sonda_alivio}',
		'{$quant_alivio}',
		'{$colostomia}',
		'{$quant_colostomia}',
		'{$iliostomia}',
		'{$quant_iliostomia}',
		'{$cistostomia}',
		'{$quant_cistostomia}',
		'{$dreno_sn}',
		'{$pa_sistolica}',
		'{$pa_diastolica}',
		'{$fc}',
		'{$fr}',
		'{$temperatura}',
		'{$spo2}',
		'{$pad}',
		'{$dor_sn}',
		'{$feridas_sn}',
		'{$cor_sec}',
		'{$globoso}',
		'{$eliminaciones}',
		'{$uso_sistema_aberto}',
		'{$gtm_num_profundidade}',
		'{$justparecer}',
		'{$tipo_ventilacao2}',
                '{$picc}',
                '{$picc_local}',
                '{$medico_multidisciplinar}',
    '{$enfermeiro_multidisciplinar}',
    '{$fono_multidisciplinar}',
    '{$fissio_motora_multidisciplinar}',
    '{$fissio_respiratoria_multidisciplinar}',
    '{$nutricao_multidisciplinar}',
    '{$clasificacao_paciente}'
	) ";

    $a = mysql_query($sql);
    // return $sql;
    if (!$a) {
        echo App\Core\Mensagem::forDatabase(mysql_error());
        mysql_query("rollback");
        return;
    }
    $id_aval = mysql_insert_id();
    savelog(mysql_escape_string(addslashes($sql)));

    // score
    $score_nead = explode("#", $score_nead);
    foreach ($score_nead as $sco_nead) {
        if ($sco_nead != '') {
            $sco_nead = anti_injection($sco_nead, "numerico");
            $sql = "INSERT INTO score_paciente (`ID_PACIENTE`, `ID_USUARIOS`, `DATA`, `ID_ITEM_SCORE`, `ID_AVALIACAO_ENFERMAGEM`, `OBSERVACAO`) VALUES ('{$paciente}','{$user}',now(),'{$sco_nead}','{$id_aval}','') ";
            $r = mysql_query($sql);
            if (!$r) {
                mysql_query("rollback");
                echo "ERRO: Tente mais tarde! Problema no score NEAD";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }
    ////equipamentos
    savelog(mysql_escape_string(addslashes($sql)));

    if(!empty($score_katz) && $score_katz != '') {
        $score_katz = \App\Controllers\Score::salvarScorePaciente($score_katz, $paciente, 'avaliacaoenfermagem', $id_aval);
        if (!$score_katz) {
            mysql_query("rollback");
            echo "ERRO: Tente mais tarde! Problema no score Katz";
            return;
        }
    }

    if(!empty($score_nead2016) && $score_nead2016 != '') {
        $score_nead2016 = \App\Controllers\Score::salvarScorePaciente($score_nead2016, $paciente, 'avaliacaoenfermagem', $id_aval);
        if (!$score_nead2016) {
            mysql_query("rollback");
            echo "ERRO: Tente mais tarde! Problema no score NEAD2016!";
            return;
        }
    }

    ///////Fun��o para inserir os dados de medica��o em uso
    $item = explode("$", $itens);
    foreach ($item as $i) {
        if ($i != "" && $i != null) {
            $e = explode("#", $i);
            $nome = anti_injection($e[0], "literal");
            $apresentacao = anti_injection($e[1], "literal");
            $via = anti_injection($e[2], "literal");
            $posologia = anti_injection($e[3], "literal");
            $familia_custeia = anti_injection($e[4], "literal");
            $frequencia = anti_injection($e[5], "literal");

            //mysql_query("begin");

            $sql2 = "INSERT INTO 
                    avaliacao_enf_medicamento 
                    (AVALIACAO_ENF_ID,
                    MEDICACAO,
                    APRESENTACAO,
                    VIA,
                    POSOLOGIA,
                    CUSTEADO_FAMILIA,
                    FREQUENCIA) values ('{$id_aval}','{$nome}','{$apresentacao}','{$via}','{$posologia}','{$familia_custeia}','{$frequencia}')";
//            echo $sql2;
            $r = mysql_query($sql2);

            if (!$r) {
                echo App\Core\Mensagem::forDatabase(mysql_error());
                mysql_query("rollback");

                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }
    //////////Fim da Fun��o de Medica��o em uso
    /////////Fun��o para inserir os dados de dreno
    $item = explode("$", $dreno);
    foreach ($item as $i) {
        if ($i != "" && $i != null && i != "undefined") {
            $e = explode("#", $i);
            $nome = anti_injection($e[0], "literal");
            $local = anti_injection($e[1], "literal");


            $sql3 = "INSERT INTO avaliacao_enf_dreno (AVALIACAO_ENF_ID,DRENO,LOCAL) values ('{$id_aval}','{$nome}','{$local}')";
            $r = mysql_query($sql3);

            if (!$r) {
                echo App\Core\Mensagem::forDatabase(mysql_error());
                mysql_query("rollback");

                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    /////////Fim da fun��o de dreno
    ////////Fun��o para inserir os dados de dor 

    $item = explode("$", $dor);
    foreach ($item as $i) {
        if ($i != "" && $i != null && i != "undefined") {
            $e = explode("#", $i);
            $nome = anti_injection($e[0], "literal");
            $inte = anti_injection($e[1], "literal");


            $sql4 = "INSERT INTO avaliacao_enf_dor (AVALIACAO_ENF_ID,LOCAL,INTENSIDADE) values ('{$id_aval}','{$nome}','{$inte}')";
            $r = mysql_query($sql4);

            if (!$r) {
                echo App\Core\Mensagem::forDatabase(mysql_error());
                mysql_query("rollback");

                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }



    ///////Fim da fun��o de dor
    ///////Fun��o para inserir os dados de feridas
    $conn = \App\Models\DB\ConnMysqli::getConnection();
    $imagemFeridaQueue = new \App\Models\Enfermagem\ImagemFeridaQueue($conn);

    $item = explode("$", $ferida);
    foreach ($item as $i) {
        if ($i != "" && $i != null) {
            $e = explode("#", $i);
            $local = anti_injection($e[0], "numerico");
            $lado = anti_injection($e[1], "literal");
            $tipo = anti_injection($e[2], "numerico");
            $cobertura = anti_injection($e[3], "literal");
            $tamanho = anti_injection($e[4], "numerico");
            $profundidade = anti_injection($e[5], "numerico");
            $tecido = anti_injection($e[6], "numerico");
            $grau = anti_injection($e[7], "numerico");
            $humidade = anti_injection($e[8], "numerico");
            $exsudato = anti_injection($e[9], "numerico");
            $contaminacao = anti_injection($e[10], "numerico");
            $pele = anti_injection($e[11], "numerico");
            $tempo = anti_injection($e[12], "numerico");
            $odor = anti_injection($e[13], "numerico");
            $periodo = anti_injection($e[14], "numerico");
            $status_ferida = anti_injection($e[15], "numerico");
            $imagemFeridaId = empty($e[16]) ? 'NULL' : anti_injection($e[16], "numerico");
            $observacaoImagem = anti_injection($e[17], "literal");
            $observacao = anti_injection($e[18], "literal");

            if ($observacaoImagem == '-1')
                $observacaoImagem = '';

            $sql5 = "INSERT INTO quadro_feridas_enfermagem (AVALIACAO_ENF_ID,LOCAL,LADO,TIPO,TAMANHO,
                                                            PROFUNDIDADE,TECIDO,GRAU,HUMIDADE,EXSUDATO,CONTAMINACAO,
                                                            PELE,TEMPO,ODOR,PERIODO,STATUS_FERIDA,FILE_UPLOADED_ID,OBSERVACAO_IMAGEM,observacao)
                     VALUES ('{$id_aval}','{$local}','{$lado}','{$tipo}','{$tamanho}','{$profundidade}',
                              '{$tecido}','{$grau}','{$humidade}','{$exsudato}','{$contaminacao}','{$pele}',
                              '{$tempo}','{$odor}','{$periodo}','{$status_ferida}',{$imagemFeridaId},'{$observacaoImagem}','{$observacao}')";

            $r = mysql_query($sql5);

            if (!$r) {
                echo App\Core\Mensagem::forDatabase(mysql_error());
                mysql_query("rollback");
                return;
            }

            $idQuadroFerida = mysql_insert_id();
            savelog(mysql_escape_string(addslashes($sql5)));
            $cobertura = explode(',',$cobertura);

            foreach($cobertura as $c){
                $sql = <<<SQL
Insert into
`quadro_feridas_enfermagem_cobertura`
(
`quadro_feridas_enfermagem_id`  ,
`cobertura_id`
)
values
({$idQuadroFerida},
{$c}
)
SQL;
                $r = mysql_query($sql);

                if (!$r) {
                    echo "Erro: ao insrir cobertura da ferida";
                    mysql_query("rollback");
                    return;
                }

            }
            savelog(mysql_escape_string(addslashes($sql)));

            $imagemFeridaQueue->add($imagemFeridaId);


        }
    }

    /////////Fun��o para inserir os dados de suplemento
    $item = explode("$", $suplemento);
    foreach ($item as $i) {
        if ($i != "" && $i != null && i != "undefined") {
            $e = explode("#", $i);
            $nome = anti_injection($e[0], "literal");
            $frequencia = anti_injection($e[1], "literal");


            $sql6 = "INSERT INTO avaliacao_enf_suplemento (AVALIACAO_ENF_ID,NOME,FREQUENCIA) values ('{$id_aval}','{$nome}','{$frequencia}')";
            $r = mysql_query($sql6);

            if (!$r) {
                echo App\Core\Mensagem::forDatabase(mysql_error());
                mysql_query("rollback");

                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    /////////Fim da fun��o de suplemento
    /////////Fun��o para inserir os dados de topicos
    $item = explode("$", $topico);
    foreach ($item as $i) {
        if ($i != "" && $i != null && i != "undefined") {
            $e = explode("#", $i);
            $nome = anti_injection($e[0], "literal");
            $frequencia = anti_injection($e[1], "literal");


            $sql7 = "INSERT INTO avaliacao_enf_topicos (AVALIACAO_ENF_ID,LOCAL,FREQUENCIA) values ('{$id_aval}','{$nome}','{$frequencia}')";
            $r = mysql_query($sql7);

            if (!$r) {
                echo App\Core\Mensagem::forDatabase(mysql_error());
                mysql_query("rollback");

                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    /////////Fim da fun��o de TOPICOS

    ////--score---//
    savelog(mysql_escape_string(addslashes($sql)));
    $score = explode("#", $score);
    foreach ($score as $sco) {
        if ($sco != '') {
            $sco = anti_injection($sco, "numerico");
            $sql = "INSERT INTO score_paciente (`ID_PACIENTE`, `ID_USUARIOS`, `DATA`, `ID_ITEM_SCORE`, `ID_AVALIACAO_ENFERMAGEM`, `OBSERVACAO`) VALUES ('{$paciente}','{$user}',now(),'{$sco}','{$id_aval}','') ";
            $r = mysql_query($sql);
            if (!$r) {
                mysql_query("rollback");
                echo App\Core\Mensagem::forDatabase(mysql_error());
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }
    mysql_query("commit");

    $imagemFeridaQueue->uploaded(); // Add all images to the queue

    $gateway = enviarEmail($paciente, 1, $empresa);

    $env = Config::get('APP_ENV');
    $mensagem = utf8_decode($gateway->adapter->mail->html);
    $token = Config::get('TOKEN_HIPCHAT_AVALIACOES');
    $hip = new \App\Controllers\HipChatSismederi($token, $env);
    $roomId = 4706984;
    $hip->sendNotificationRoom($roomId, $mensagem);

    echo "1";

    //enviar_email_relatorio($paciente,1,$empresa);
}

///////Fim da fun��o de feridas
/////// Fim da fun��o de avaliacao


//////**********Fun��o de edic�o de avala��o

function editar_ficha_avaliacao_enfermagem($post) {
    extract($post, EXTR_OVERWRITE);
    $hist_pregressa = anti_injection($hist_pregressa, 'literal');
    //$id = anti_injection($id,'numerico');
    $user = $_SESSION["id_user"];

    mysql_query('begin');
    if ($integra_lesao == 1) {
        $integra = 1;
        $lesoes = 2;
    } else {
        $integra = 2;
        $lesoes = 1;
    }
    if ($icterica_anicterica == 1) {
        $icterica = 1;
        $anicterica = 2;
    } else {
        $icterica = 2;
        $anicterica = 1;
    }
    if ($hidratada_desidratada == 1) {
        $hidratada = 1;
        $desidratada = 2;
    } else {
        $hidratada = 2;
        $desidratada = 1;
    }
    if ($tipo_ventilacao == 1) {
        $ventilacao = 1;
    } else {
        $ventilacao = $tipo_ventilacao1;
    }
    if ($tipo_ventilacao1 == 2) {
        $vent_metalica = $num_ventilacao;
        $ventilacao_plastica = 0;
    } else {
        $vent_metalica = 0;
        $ventilacao_plastica = $num_ventilacao;
    }
    
     $hist_pregressa = anti_injection($hist_pregressa,'literal');
    $diagnostico_principal = anti_injection($diagnostico_principal,'literal');
    $hist_familiar = anti_injection($hist_familiar,'literal');
    $hist_atual = anti_injection($hist_atual,'literal');
    $queixas_atuais = anti_injection($queixas_atuais,'literal');
    $pad = anti_injection($pad,'literal');

    $sql = "UPDATE avaliacaoenfermagem SET CLINICO = '{$clinico}', CIRURGICO = '{$cirurgico}', PALIATIVOS = '{$paliativos}' , DIAGNOSTICO_PRINCIPAL = '{$diagnostico_principal}' , HIST_PREGRESSA = '{$hist_pregressa}'  , HIST_FAMILIAR = '{$hist_familiar}' , QUEIXA_ATUAL = '{$queixas_atuais}' , HIST_ATUAL = '{$hist_atual}' ,EST_GERAL_BOM = '{$bom}' , EST_GERAL_RUIM = '{$ruim}' , NUTRIDO = '{$nutrido}' , DESNUTRIDO = '{$desnutrido}' , HIGIENE_CORPORAL_SATISFATORIA = '{$higiene_corpo_s}' , HIGIENE_CORPORAL_INSATISFATORIA = '{$higiene_corpo_i}' , HIGIENE_BUCAL_SATISFATORIA = '{$higiene_bucal_s}' , HIGIENE_BUCAL_INSATISFATORIA = '{$higiene_bucal_i}', ESTADO_EMOCIONAL = '{$estado_emocional}',  CONCIENTE = '{$consciente}' , SEDADO = '{$sedado}' , OBNUBILADO = '{$obnublado}' , TORPOROSO = '{$torporoso}' , COMATOSO = '{$comatoso}' , ORIENTADO = '{$orientado}' , DESORIENTADO = '{$desorientado}' , SONOLENTO = '{$sonolento}', COND_MOTORA = '{$cond_motora}', ALERGIA_MEDICAMENTO = '{$palergiamed}' , ALERGIA_ALIMENTO = '{$palergiaalim}' , ALERGIA_MEDICAMENTO_TIPO = '{$alergiamed}' , ALERGIA_ALIMENTO_TIPO = '{$alergiaalim}', PELE_INTEGRA = '{$integra}' , PELE_LESAO = '{$lesoes}' , PELE_HIDRATADA = '{$hidratada}' , PELE_DESIDRATADA = '{$desidratada}' , PELE_ICTERICA = '{$icterica}' , PELE_ICTERICA_ESCALA = '{$icterica_escala}',PELE_ANICTERICA = '{$anicterica}' , PELE_PALIDA = '{$palida}' , PELE_SUDOREICA = '{$sudoreica}' , PELE_FRIA = '{$fria}' , PELE_EDEMA = '{$pele_edema}', PELE_DESCAMACAO = '{$descamacao}',T_ASSIMETRICO = '{$t_assimetrico}', T_SIMETRICO = '{$t_simetrico}', EUPNEICO = '{$eupneico}', DISPNEICO = '{$dispneico}', TAQUIPNEICO = '{$taquipneico}' , MVBD = '{$mvbd}' ,RONCOS = '{$roncos}', SIBILOS = '{$sibilos}', CREPTOS = '{$creptos}' , MV_DIMINUIDO = '{$mv_diminuido}', BCNF = '{$bcnf}' ,NORMOCARDICO = '{$normocardico}',TAQUICARDICO = '{$taquicardico}' ,BRADICARDICO = '{$bradicardico}',VENTILACAO = '{$ventilacao}' , VENT_TRAQUESTOMIA_METALICA_NUM = '{$vent_metalica}' , VENT_TRAQUESTOMIA_PLASTICA_NUM = '{$ventilacao_plastica_num}', BIPAP = '{$bipap}',OXIGENIO_SN = '{$oxigenio_sn}' ,OXIGENIO = '{$oxigenio}', VENT_CATETER_OXIGENIO_NUM = '{$vent_cateter_oxigenio_num}', VENT_CATETER_OXIGENIO_QUANT = '{$vent_cateter_oxigenio_quant}' ,VENT_CATETER_OCULOS_NUM = '{$vent_cateter_oculos_num}', VENT_RESPIRADOR = '{$vent_respirador}' ,VENT_RESPIRADOR_TIPO = '{$vent_respirador_tipo}', VENT_MASCARA_VENTURY_NUM = '{$vent_mascara_ventury_num}', VENT_CONCENTRADOR_NUM = '{$vent_concentrador_num}', ALT_ASPIRACOES = '{$alt_aspiracoes}', ASPIRACAO_NUM = '{$aspiracoes_num}' , ASPIRACAO_SECRECAO = '{$aspiracao_secrecao}' , ASPIRACAO_SECRECAO_CARACTERISTICA = '{$caracteristicas_sec}', PLANO = '{$plano}' ,FLACIDO = '{$flacido}',DISTENDIDO = '{$distendido}', TIMPANICO = '{$timpanico}', RHA = '{$rha}', INDO_PALPACAO = '{$indo_palpacao}',DOLOROSO = '{$doloroso}',DIETA_SN = '{$dieta_sn}', VIA_ALIMENTACAO_ORAL = '{$oral}',VIA_ALIMENTACAO_PARENTAL = '{$parental}',VIA_ALIMENTACAO_VISIVEL = '{$visivel}',VIA_ALIMENTACAO_PARENTAL_NUM = '{$parental_num}',VIA_ALIMENTACAO_SNE = '{$sne}',VIA_ALIMENTACAO_SNE_NUM = '{$sne_num}',VIA_ALIMENTACAO_GTM = '{$gtm_sonda_gastro}',VIA_ALIMENTACAO_GTM_NUM = '{$gtm_num}', VIA_ALIMENTACAO_GTM_VISIVEL ='{$gtm_visivel}' ,VIA_ALIMENTACAO_SONDA_GASTRO = '{$sonda_gastro}' ,VIA_ALIMENTACAO_SONDA_GASTRO_NUM = '{$quant_sonda}', DIETA_SIST_ABERTO = '{$sistema_aberto}',DIETA_SIST_ABERTO_DESCRICAO = '{$quant_sistema_aberto}',DIETA_SIST_FECHADO = '{$sistema_fechado}', DIETA_SIST_FECHADO_DESCRICAO = '{$quant_sistema_fechado}' ,DIETA_SIST_FECHADO_VAZAO = '{$vazao}',DIETA_SIST_ARTESANAL = '{$dieta_artesanal}',DIETA_SUPLEMENTO_SN = '{$suplemento_sn}',DIETA_NPT = '{$dieta_npt}',DIETA_NPT_DESC = '{$quant_npt}',DIETA_NPT_VAZAO = '{$vazao_npt}',DIETA_NPP = '{$npp}',DIETA_NPP_DESC = '{$quant_npp}',DIETA_NPP_VAZAO = '{$vazao_npp}', INTEGRA = '{$integra_genital}',SECRECAO = '{$secrecao}',SANGRAMENTO = '{$sangramento}',PRURIDO = '{$prurido}',EDEMA = '{$edema}',TOPICOS_SN = '{$topicos_sn}', DISPO_INTRAVENOSO_CVC =  '{$cvc}', DISPO_INTRAVENOSO_UNO_LOCAL = '{$local_uno}',DISPO_INTRAVENOSO_DUPLO_LOCAL = '{$local_duplo}',DISPO_INTRAVENOSO_TRIPLO_LOCAL = '{$local_triplo}' ,DISPO_INTRAVENOSO_AVP = '{$avp}' ,DISPO_INTRAVENOSO_AVP_LOCAL = '{$avp_local}',DISPO_INTRAVENOSO_PORTOCATH = '{$port}',DISPO_INTRAVENOSO_PORTOCATH_NUM = '{$quant_port}',DISPO_INTRAVENOSO_PORTOCATH_LOCAL = '{$port_local}',DISPO_INTRAVENOSO_OUTROS = '{$outros_intra}', FRALDA = '{$fralda}',  FRALDA_UNID_DIA = '{$fralda_dia}', FRALDA_TAMANHO = '{$tamanho_fralda}', DISPO_URINARIO = '{$urinario}', DISPO_URINARIO_NUM = '{$quant_urinario}', DISPO_SONDA_FOLEY = '{$sonda}', DISPO_SONDA_FOLEY_NUM = '{$quant_foley}', DISPO_SONDA_ALIVIO = '{$sonda_alivio}', DISPO_SONDA_ALIVIO_NUM = '{$quant_alivio}', DISPO_COLOSTOMIA = '{$colostomia}', DISPO_COLOSTOMIA_NUM = '{$quant_colostomia}', DISPO_ILIOSTOMIA = '{$iliostomia}', DISPO_ILIOSTOMIA_NUM = '{$quant_iliostomia}', DISPO_CISTOSTOMIA = '{$cistostomia}',DISPO_CISTOSTOMIA_NUM = '{$quant_cistostomia}', DRENOS = '{$dreno_sn}',PA = '{$pa_sistolica}', PA_DISTOLICA = '{$pa_diastolica}',FC = '{$fc}',FR = '{$fr}',TEMPERATURA = '{$temperatura}',SPO2 = '{$spo2}',PAD_ENFERMAGEM = '{$pad}',DOR ='{$dor_sn}',FERIDA = '{$feridas_sn}'
				, GLOBOSO = '{$globoso}', ASPIRACAO_SECRECAO_COR = '{$cor_sec}', ELIMINACOES = '{$eliminaciones}', USO_SISTEMA_ABERTO = '{$uso_sistema_aberto}', DISPOSITIVOS_INTRAVENOSOS = '{$dispositivos_intravenosos}', VIA_ALIMENTACAO_GMT_PROFUNDIDADE = '{$gtm_num_profundidade}', VENTILACAO_ESP_EQUIP = '{$tipo_ventilacao2}' where ID = '{$id_aval}'";


    $a = mysql_query($sql);
    if (!$a) {
        mysql_query("rollback");
        echo "Erro: tente novamente 1" . $sql;
        return;
    }
    //$id_aval=mysql_insert_id();
    savelog(mysql_escape_string(addslashes($sql)));
    ///////Fun��o para inserir os dados de medica��o em uso
    $item = explode("$", $itens);
    $sql4 = "DELETE  FROM avaliacao_enf_medicamento where avaliacao_enf_id = '{$id_aval}' ";
    $b = mysql_query($sql4);

    foreach ($item as $i) {
        if ($i != "" && $i != null) {
            $e = explode("#", $i);
            $nome = anti_injection($e[0], "literal");
            $apresentacao = anti_injection($e[1], "literal");
            $via = anti_injection($e[2], "literal");
            $posologia = anti_injection($e[3], "literal");
            $familia_custeia = anti_injection($e[4], "literal");
            /* ATUALIZA��O ERITON 13/03/2013 */
            $frequencia_med = anti_injection($e[5], "literal");


            $sql2 = "INSERT INTO avaliacao_enf_medicamento (AVALIACAO_ENF_ID,MEDICACAO,APRESENTACAO,VIA,POSOLOGIA,CUSTEADO_FAMILIA,FREQUENCIA) values ('{$id_aval}','{$nome}','{$apresentacao}','{$via}','{$posologia}','${familia_custeia}','{$frequencia_med}')";
            $ab = mysql_query($sql2);
            /* ATUALIZA��O ERITON 13/03/2013 */
            if (!$ab) {
                mysql_query("rollback");
                echo "Erro: tente novamente 1";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }
    //////////Fim da Fun��o de Medica��o em uso
    /////////Fun��o para inserir os dados de dreno
    $item = explode("$", $dreno);


    $sql7 = "DELETE  FROM avaliacao_enf_dreno where avaliacao_enf_id = '{$id_aval}' ";
    $b = mysql_query($sql7);

    foreach ($item as $i) {
        if ($i != "" && $i != null) {
            $e = explode("#", $i);
            $nome = anti_injection($e[0], "literal");
            $local = anti_injection($e[1], "literal");


            $sql10 = "INSERT INTO avaliacao_enf_dreno (AVALIACAO_ENF_ID,DRENO,LOCAL) values ('{$id_aval}','{$nome}','{$local}')";
            $s = mysql_query($sql10);

            //$sql3 = "INSERT INTO avaliacao_enf_dreno (AVALIACAO_ENF_ID,DRENO,LOCAL) values ('{$id_aval}','{$nome}','{$local}')";
            //$r = mysql_query($sql3);

            if (!$s) {
                mysql_query("rollback");
                echo "Erro: tente novamente 2";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    /////////Fim da fun��o de dreno
    ////////Fun��o para inserir os dados de dor

    $item = explode("$", $dor);

    $sql9 = "DELETE  FROM avaliacao_enf_dor where avaliacao_enf_id = '{$id_aval}' ";
    $b = mysql_query($sql9);
    foreach ($item as $i) {
        if ($i != "" && $i != null) {
            $e = explode("#", $i);
            $nome = anti_injection($e[0], "literal");
            $inte = anti_injection($e[1], "literal");


            $sql4 = "INSERT INTO avaliacao_enf_dor (AVALIACAO_ENF_ID,LOCAL,INTENSIDADE) values ('{$id_aval}','{$nome}','{$inte}')";
            $r = mysql_query($sql4);

            if (!$r) {
                mysql_query("rollback");
                echo "Erro: tente  3";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }



    ///////Fim da fun��o de dor
    ///////Fun��o para inserir os dados de feridas

    $item = explode("$", $ferida);


    $sql3 = "DELETE  FROM avaliacao_enf_ferida where avaliacao_enf_id = {$id_aval}";
    $c = mysql_query($sql3);

    foreach ($item as $i) {
        if ($i != "" && $i != null) {
            $e = explode("#", $i);
            $local = anti_injection($e[0], "numerico");
            $lado = anti_injection($e[1], "literal");
            $tipo = anti_injection($e[2], "numerico");
            $cobertura = anti_injection($e[3], "numerico");
            $tamanho = anti_injection($e[4], "numerico");
            $profundidade = anti_injection($e[5], "numerico");
            $tecido = anti_injection($e[6], "numerico");
            $grau = anti_injection($e[7], "numerico");
            $humidade = anti_injection($e[8], "numerico");
            $exsudato = anti_injection($e[9], "numerico");
            $contaminacao = anti_injection($e[10], "numerico");
            $pele = anti_injection($e[11], "numerico");
            $tempo = anti_injection($e[12], "numerico");
            $odor = anti_injection($e[13], "numerico");
            $periodo = anti_injection($e[14], "numerico");

            $sql5 = "INSERT INTO quadro_feridas_enfermagem (AVALIACAO_ENF_ID,LOCAL,LADO,TIPO,COBERTURA,TAMANHO,PROFUNDIDADE,TECIDO,GRAU,HUMIDADE,EXSUDATO,CONTAMINACAO,PELE,TEMPO,ODOR,PERIODO_TROCA) values ('{$id_aval}','{$local}','{$lado}','{$tipo}','{$cobertura}','{$tamanho}','{$profundidade}','{$tecido}','{$grau}','{$humidade}','{$exsudato}','{$contaminacao}','{$pele}','{$tempo}','{$odor}','{$periodo}')";
            //$sql5 = "INSERT INTO avaliacao_enf_ferida (`AVALIACAO_ENF_ID`, `LOCAL`,`LADO`, `TIPO_CURATIVO`, `FREQUENCIA_TROCA_CURATIVO`, `CARACTERISTICA` ) VALUES ('{$id_aval}','{$local}','{$lado}','{$tipo}','{$frequencia}','{$caracteristica}')";
            $j = mysql_query($sql5);

            if (!$j) {
                mysql_query("rollback");
                echo "Erro: tente novamente 4";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    /////////Fun��o para inserir os dados de suplemento

    $item = explode("$", $suplemento);

    $sql8 = "DELETE  FROM avaliacao_enf_suplemento where avaliacao_enf_id = {$id_aval}";
    $d = mysql_query($sql8);
    foreach ($item as $i) {
        if ($i != "" && $i != null && i != "undefined") {
            $e = explode("#", $i);
            $nome = anti_injection($e[0], "literal");
            $frequencia = anti_injection($e[1], "literal");


            $sql6 = "INSERT INTO avaliacao_enf_suplemento (AVALIACAO_ENF_ID,NOME,FREQUENCIA) values ('{$id_aval}','{$nome}','{$frequencia}')";
            $r = mysql_query($sql6);

            if (!$r) {
                mysql_query("rollback");
                echo "Erro: tente novamente 5";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    /////////Fim da fun��o de suplemento
    /////////Fun��o para inserir os dados de topicos

    $item = explode("$", $topico);
    $sql9 = "DELETE  FROM avaliacao_enf_topicos where avaliacao_enf_id = {$id_aval}";
    $f = mysql_query($sql9);

    foreach ($item as $i) {
        if ($i != "" && $i != null && i != "undefined") {
            $e = explode("#", $i);
            $nome = anti_injection($e[0], "literal");
            $frequencia = anti_injection($e[1], "literal");


            $sql7 = "INSERT INTO avaliacao_enf_topicos (AVALIACAO_ENF_ID,LOCAL,FREQUENCIA) values ('{$id_aval}','{$nome}','{$frequencia}')";
            $r = mysql_query($sql7);

            if (!$r) {
                mysql_query("rollback");
                echo "Erro: tente novamente 6";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    $sql = "Delete FROM score_paciente where ID_AVALIACAO_ENFERMAGEM='{$id_aval}' ";
    $r = mysql_query($sql);
    if (!$r) {
        mysql_query("rollback");
        echo "ERRO: Tente mais tarde!5";
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));

    ////--score---//
    $score_nead = explode("#", $score_nead);
    foreach ($score_nead as $sco_nead) {
        if ($sco_nead != '') {
            $sco_nead = anti_injection($sco_nead, "numerico");
            $sql = "INSERT INTO score_paciente (`ID_PACIENTE`, `ID_USUARIOS`, `DATA`, `ID_ITEM_SCORE`, `ID_AVALIACAO_ENFERMAGEM`, `OBSERVACAO`) VALUES ('{$paciente}','{$user}',now(),'{$sco_nead}','{$id_aval}','') ";
            $r = mysql_query($sql);
            if (!$r) {
                mysql_query("rollback");
                echo "ERRO: Tente mais tarde! Problema no score NEAD";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }
    mysql_query('commit');
    echo"1";
    return;
}

///////FICHA DE  EVOLUCAO/////////////////////////////

function ficha_evolucao_enfermagem($post) {
    extract($post, EXTR_OVERWRITE);


    $hist_pregressa = anti_injection($hist_pregressa, 'literal');
    //$id = anti_injection($id,'numerico');
    $user = $_SESSION["id_user"];
    $data_evolucao = implode("-", array_reverse(explode("/", $inicio)));
    $ultima_substituicao = implode("-", array_reverse(explode("/", (($ultima_substituicao_button != null) ? "$ultima_substituicao_button" : "$ultima_substituicao_sonda") )));
    $pad =anti_injection($pad,'literal');

    $sql = "SELECT convenio, empresa FROM clientes WHERE idClientes = '{$paciente}'";
    $rs = mysql_query($sql);
    $rowPaciente = mysql_fetch_array($rs);
    $empresa = null;
    $convenio = null;
    if(count($rowPaciente) > 0) {
        $empresa = $rowPaciente['empresa'];
        $convenio = $rowPaciente['convenio'];
    }

    mysql_query('begin');
    $sql = "INSERT INTO evolucaoenfermagem (
	USUARIO_ID,
	PACIENTE_ID,
	empresa,
	convenio,
	DATA,
	BOM_RUIM,
	NUTRIDO_DESNUTRIDO,
	HIGI_CORP_SATIS,
	HIGI_BUCAL_SATIS,
	ESTADO_EMOCIONAL,
	CONSCIENTE,
	SEDADO,
	OBNUBILADO,
	TORPOROSO,
	COMATOSO,
	ORIENTADO,
	DESORIENTADO,
	SONOLENTO,
	INTEGRO,
	CICATRIZ,
	LOCAL_CICATRIZ,
	COND_MOTORA,
	PALERGIAMED,
	PALERGIAALIM,
	TIPOALERGIAMED,
	TIPOALERGIAALIM,
	INTEGRA_LESAO,
	ICTERICA_ANICTERICA,
	HIDRATADA_DESIDRATADA,
	PALIDA,
	SUDOREICA,
	FRIA,
	PELE_EDEMA,
	DESCAMACAO,
	MUCOSAS,
	ALT_PESCOCO,
	NODULOS,
	GANGLIOS_INF,
	RIGIDEZ,
	BOCIO,
	FERIDA_PESCOCO,
	AUSCULTA_ALTERADA,
	CREPTOS,
	ESTERTORES,
	RONCOS,
	MV_DIMI,
	SIBILOS,
	TIPO_CREPTOS,
	TIPO_ESTERTORES,
	TIPO_RONCOS,
	TIPO_MV,
	SIBILOS_TIPO,
	SIMETRICO_ASSIMETRICO,
	PALPACAO,
	GANGLIOS,
	CICATRIZ_TORAX,
	AFUNDAMENTO,
	PADRAO_CARDIORESPIRATORIO,
	PADRAO_CARDIORESPIRATORIO1,
	PADRAO_CARDIORESPIRATORIO2,
	TIPO_VENTILACAO,
	TIPO_VENTILACAO1,
	NUM_VENTILACAO,
	TIPO_VENTILACAO2,
	BIPAP,
	OXIGENIO_SN,
	OXIGENIO,
	ALT_ASPIRACOES,
	ASPIRACOES_NUM,
	ASPIRACAO_SECRECAO,
	CARACTERISTICAS_SEC,
	COR_SEC,
	PLANO,
	FLACIDO,
	GLOBOSO,
	DISTENDIDO,
	RHA,
	INDO_PALPACAO,
	DOLOROSO,
	TIMPANICO,
	DIETA_SN,
	ORAL,
	SNE,
	SNE_NUM,
	GTM_SONDA_GASTRO,
	GTM_NUM,
	GTM_NUM_PROFUNDIDADE,
	QUANT_SONDA,
	SISTEMA_ABERTO,
	QUANT_SISTEMA_ABERTO,
	USO_SISTEMA_ABERTO,
	SISTEMA_FECHADO,
	QUANT_SISTEMA_FECHADO,
	VAZAO,
	DIETA_NPT,
	VAZAO_NPT,
	desc_npt,
	DIETA_NPP,
	VAZAO_NPP,
	desc_npp,
	DIETA_ARTESANAL,
	SUPLEMENTO_SN,
	SUPLEMENTO,
	INTEGRA_GENITAL,
	SECRECAO,
	SANGRAMENTO,
	PRURIDO,
	EDEMA,
	EDEMA_MMSS,
	DEFORMIDADES_MMSS,
	LESOES_MMSS,
	EDEMA_MMII,
	VARIZES_MMII,
	DEFORMIDADES_MMII,
	LESOES_MMII,
	AQUECIDAS_EXT,
	OXIGENADAS_EXT,
	FRIAS_EXT,
	CIANOTICAS_EXT,
	PULSO_PRESENTE,
	DISPOSITIVOS_INTRAVENOSOS,
	CVC,
	CVC_TIPO,
	LOCAL_CVC_UNO,
	LOCAL_CVC_DUPLO,
	LOCAL_CVC_TRIPLO,
	AVP,
	AVP_LOCAL,
	PORT,
	QUANT_PORT,
	PORT_LOCAL,
	OUTROS_INTRA,
	ALT_GASTRO,
	DIARREIA_OBSTIPACAO,
	NAUSEAS_GASTRO,
	VOMITOS_GASTRO,
	DIARREIA_EPISODIOS,
	CATETERISMO_INT,
	NORMAL_URINARIO,
	ANURIA_URINARIO,
	INCONTINENCIA_URINARIO,
	HEMATURIA_URINARIO,
	PIURIA_URINARIO,
	OLIGURIA_URINARIO,
	ODOR_URINARIO,
	ELIMINACIONES,
	FRALDA,
	FRALDA_DIA,
	TAMANHO_FRALDA,
	URINARIO,
	QUANT_URINARIO,
	SONDA,
	QUANT_FOLEY,
	SONDA_ALIVIO,
	QUANT_ALIVIO,
	COLOSTOMIA,
	QUANT_COLOSTOMIA,
	DISPO_ILIOSTOMIA,
	DISPO_ILIOSTOMIA_NUM,
	CISTOSTOMIA,
	QUANT_CISTOSMIA,
	DRENO_SN,
	DRENO,
	FERIDAS_SN,
	PA_SISTOLICA,
	PA_DIASTOLICA,
	FC,
	FR,
	SPO2,
	TEMPERATURA,
	PAD,
	DATA_EVOLUCAO,
	ULTIMA_SUBSTITUICAO,
	OBSTIPACAO_EPISODIOS,
	VOMITO_EPISODIOS,
        CATETER_PICC,
        CATETER_PICC_LOCAL,
        VENT_CATETER_OXIGENIO_NUM,
	VENT_CATETER_OXIGENIO_QUANT,
	VENT_CATETER_OCULOS_NUM,
	VENT_RESPIRADOR,
	VENT_RESPIRADOR_TIPO,
	VENT_MASCARA_VENTURY,
	VENT_MASCARA_VENTURY_NUM,
	VENT_CONCENTRADOR,
	VENT_CONCENTRADOR_NUM,
	constipacao_gastro,
	vigil
)
VALUES
	(
		'{$user}',
		'{$paciente}',
		'{$empresa}',
		'{$convenio}',
		now(),
		'{$bom_ruim}',
		'{$nutrido_desnutrido}',
		'{$higi_corp_satis}',
		'{$higi_bucal_satis}',
		'{$estado_emocional}',
		'{$consciente}',
		'{$sedado}',
		'{$obnubilado}',
		'{$torporoso}',
		'{$comatoso}',
		'{$orientado}',
		'{$desorientado}',
		'{$sonolento}',
		'{$integro}',
		'{$cicatriz}',
		'{$local_cicatriz}',
		'{$cond_motora}',
		'{$palergiamed}',
		'{$palergiaalim}',
		'{$alergiamed}',
		'{$alergiaalim}',
		'{$integra_lesao}',
		'{$icterica_anicterica}',
		'{$hidratada_desidratada}',
		'{$palida}',
		'{$sudoreica}',
		'{$fria}',
		'{$pele_edema}',
		'{$descamacao}',
		'{$mucosas}',
		'{$alt_pescoco}',
		'{$nodulos}',
		'{$ganglios_inf}',
		'{$rigidez}',
		'{$bocio}',
		'{$ferida_pescoco}',
		'{$ausculta_alterada}',
		'{$creptos}',
		'{$estertores}',
		'{$roncos}',
		'{$mv_dimi}',
		'{$sibilos}',
		'{$creptos_tipo}',
		'{$estertor_tipo}',
		'{$roncos_tipo}',
		'{$mv_tipo}',
		'{$sibilos_tipo}',
		'{$simetrico_assimetrico}',
		'{$palpacao}',
		'{$ganglios}',
		'{$cicatriz_torax}',
		'{$afundamento}',
		'{$padrao_cardiorespiratorio}',
		'{$padrao_cardiorespiratorio1}',
		'{$padrao_cardiorespiratorio2}',
		'{$tipo_ventilacao}',
		'{$tipo_ventilacao1}',
		'{$num_ventilacao}',
		'{$tipo_ventilacao2}',
		'{$bipap}',
		'{$oxigenio_sn}',
		'{$oxigenio}',
		'{$alt_aspiracoes}',
		'{$aspiracoes_num}',
		'{$aspiracao_secrecao}',
		'{$caracteristicas_sec}',
		'{$cor_sec}',
		'{$plano}',
		'{$flacido}',
		'{$globoso}',
		'{$distendido}',
		'{$rha}',
		'{$indo_palpacao}',
		'{$doloroso}',
		'{$timpanico}',
		'{$dieta_sn}',
		'{$oral}',
		'{$sne}',
		'{$sne_num}',
		'{$gtm_sonda_gastro}',
		'{$gtm_num}',
		'{$gtm_num_profundidade}',
		'{$quant_sonda}',
		'{$sistema_aberto}',
		'{$quant_sistema_aberto}',
		'{$uso_sistema_aberto}',
		'{$sistema_fechado}',
		'{$quant_sistema_fechado}',
		'{$vazao}',
		'{$dieta_npt}',
		'{$vazao_npt}',
		'{$desc_npt}',
		'{$npp}',
		'{$vazao_npp}',
		'{$desc_npp}',
		'{$dieta_artesanal}',
		'{$suplemento_sn}',
		'{$suplemento}',
		'{$integra_genital}',
		'{$secrecao}',
		'{$sangramento}',
		'{$prurido}',
		'{$edema}',
		'{$edema_mmss}',
		'{$deformidades_mmss}',
		'{$lesoes_mmss}',
		'{$edema_mmii}',
		'{$varizes_mmii}',
		'{$deformidades_mmii}',
		'{$lesoes_mmii}',
		'{$aquecidas_ext}',
		'{$oxigenadas_ext}',
		'{$frias_ext}',
		'{$cianoticas_ext}',
		'{$pulso_presente}',
		'{$dispositivos_intravenosos}',
		'{$cvc}',
		'{$cateter_venoso_central}',
		'{$local_cvc_uno}',
		'{$local_cvc_duplo}',
		'{$local_cvc_triplo}',
		'{$avp}',
		'{$avp_local}',
		'{$port}',
		'{$quant_port}',
		'{$port_local}',
		'{$outros_intra}',
		'{$alt_gastro}',
		'{$diarreia_obstipacao}',
		'{$nauseas_gastro}',
		'{$vomitos_gastro}',
		'{$diarreia_episodios}',
		'{$cateterismo_int}',
		'{$normal_urinario}',
		'{$anuria_urinario}',
		'{$incontinencia_urinario}',
		'{$hematuria_urinario}',
		'{$piuria_urinario}',
		'{$oliguria_urinario}',
		'{$odor_urinario}',
		'{$eliminaciones}',
		'{$fralda}',
		'{$fralda_dia}',
		'{$tamanho_fralda}',
		'{$urinario}',
		'{$quant_urinario}',
		'{$sonda}',
		'{$quant_foley}',
		'{$sonda_alivio}',
		'{$quant_alivio}',
		'{$colostomia}',
		'{$quant_colostomia}',
		'{$iliostomia}',
		'{$quant_iliostomia}',
		'{$cistostomia}',
		'{$quant_cistosmia}',
		'{$dreno_sn}',
		'{$dreno}',
		'{$feridas_sn}',
		'{$pa_sistolica}',
		'{$pa_diastolica}',
		'{$fc}',
		'{$fr}',
		'{$spo2}',
		'{$temperatura}',
		'{$pad}',
		'{$data_evolucao}',
		'{$ultima_substituicao}',
		'{$obstipacao_episodios}',
		'{$vomito_episodios}',
                '{$picc}',
                '{$picc_local}',
                    '{$vent_cateter_oxigenio_num}',
		'{$vent_cateter_oxigenio_quant}',
		'{$vent_cateter_oculos_num}',
		'{$vent_respirador}',
		'{$vent_respirador_tipo}',
		'{$vent_mascara_ventury}',
		'{$vent_mascara_ventury_tipo}',
		'{$vent_concentrador}',
		'{$vent_concentrador_num}',
		'{$constipacao_gastro}',
		'{$vigil}'
	) ";
    $a = mysql_query($sql);

    if (!$a) {
        echo "Erro: tente novamente 6";
       echo mysql_error();
        mysql_query("rollback");
        return;
    }

    $id_aval = mysql_insert_id();
    
    savelog(mysql_escape_string(addslashes($sql)));

    /////////Fun��o para inserir os dados de suplemento

    $item = explode("$", $suplemento);

    $sql8 = "DELETE  FROM evolucao_enf_suplemento where evolucao_enf_id = {$id_aval}";
    $d = mysql_query($sql8);
    foreach ($item as $i) {
        if ($i != "" && $i != null && i != "undefined") {
            $e = explode("#", $i);
            $nome = anti_injection($e[0], "literal");
            $frequencia = anti_injection($e[1], "literal");


            $sql6 = "INSERT INTO evolucao_enf_suplemento (EVOLUCAO_ENF_ID,NOME,FREQUENCIA) values ('{$id_aval}','{$nome}','{$frequencia}')";
            $r = mysql_query($sql6);

            if (!$r) {
                mysql_query("rollback");
                echo "Erro: tente novamente 5";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql6)));
        }
    }

/////////Fun��o para inserir os dados de dreno
    $item = explode("$", $dreno);
    foreach ($item as $i) {
        if ($i != "" && $i != null && i != "undefined") {
            $e = explode("#", $i);
            $nome = anti_injection($e[0], "literal");
            $local = anti_injection($e[1], "literal");


            $sql3 = "INSERT INTO evolucao_enf_dreno (EVOLUCAO_ENF_ID,DRENO,LOCAL) values ('{$id_aval}','{$nome}','{$local}')";
            $r = mysql_query($sql3);

            if (!$r) {
                //
                echo "Erro: tente novamente 2";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql3)));
        }
    }

/////////Fim da fun��o de dreno
///////Fun��o para inserir os dados de feridas

    $conn = \App\Models\DB\ConnMysqli::getConnection();
    $imagemFeridaQueue = new \App\Models\Enfermagem\ImagemFeridaQueue($conn);

    $item = explode("$", $ferida);
    foreach ($item as $i) {
        if ($i != "" && $i != null) {
            $e = explode("#", $i);
            $local = anti_injection($e[0], "numerico");
            $lado = anti_injection($e[1], "literal");
            $tipo = anti_injection($e[2], "numerico");
            $cobertura = anti_injection($e[3], "literal");

            $tamanho = anti_injection($e[4], "numerico");
            $profundidade = anti_injection($e[5], "numerico");
            $tecido = anti_injection($e[6], "numerico");
            $grau = anti_injection($e[7], "numerico");
            $humidade = anti_injection($e[8], "numerico");
            $exsudato = anti_injection($e[9], "numerico");
            $contaminacao = anti_injection($e[10], "numerico");
            $pele = anti_injection($e[11], "numerico");
            $tempo = anti_injection($e[12], "numerico");
            $odor = anti_injection($e[13], "numerico");
            $periodo = anti_injection($e[14], "numerico");
            $status_ferida = anti_injection($e[15], "numerico");
            $imagemFeridaId = empty($e[16]) ? 'NULL' : anti_injection($e[16], "numerico");
            $observacaoImagem = anti_injection($e[17], "literal");
            $observacao = anti_injection($e[18], "literal");

            if ($observacaoImagem == '-1')
              $observacaoImagem = '';

            if ($imagemFeridaId == '-1')
              $imagemFeridaId = 'NULL';

            $sql5 = "INSERT INTO quadro_feridas_enfermagem (
                        EVOLUCAO_ENF_ID,LOCAL,LADO,TIPO,TAMANHO,PROFUNDIDADE,TECIDO,GRAU,HUMIDADE,
                        EXSUDATO,CONTAMINACAO,PELE,TEMPO,ODOR,PERIODO,STATUS_FERIDA,FILE_UPLOADED_ID, OBSERVACAO_IMAGEM,observacao)
                      VALUES (
                        '{$id_aval}','{$local}','{$lado}','{$tipo}','{$tamanho}',
                        '{$profundidade}','{$tecido}','{$grau}','{$humidade}','{$exsudato}','{$contaminacao}',
                        '{$pele}','{$tempo}','{$odor}','{$periodo}','{$status_ferida}', {$imagemFeridaId}, '{$observacaoImagem}','{$observacao}'
                      );";
            $r = mysql_query($sql5);

            if (!$r) {
                echo "Erro: tente novamente 4";
                mysql_query("rollback");
                return;
            }
            $idQuadroFerida = mysql_insert_id();
            savelog(mysql_escape_string(addslashes($sql5)));
            $cobertura = explode(',',$cobertura);

            foreach($cobertura as $c){
  $sql = <<<SQL
Insert into
`quadro_feridas_enfermagem_cobertura`
(
`quadro_feridas_enfermagem_id`  ,
`cobertura_id`
)
values
({$idQuadroFerida},
{$c}
)
SQL;
                $r = mysql_query($sql);

                if (!$r) {
                    echo "Erro: ao insrir cobertura da ferida";
                    mysql_query("rollback");
                    return;
                }

            }

            $imagemFeridaQueue->add($imagemFeridaId);
        }
    }
    mysql_query('commit');

    $imagemFeridaQueue->uploaded(); // Add all images to the queue

    echo '1';
    return;
}

//////FIM DA FICHA DE EVOLUCAO////////////////////////

function relatorio_alta($post){
    extract($post, EXTR_OVERWRITE);
    $inicio = implode("-",array_reverse(explode("/",$inicio)));
    $fim = implode("-",array_reverse(explode("/",$fim)));
    mysql_query("begin");
    $user = $_SESSION["id_user"];

    $sql = "SELECT convenio, empresa FROM clientes WHERE idClientes = '{$paciente_id}'";
    $rs = mysql_query($sql);
    $rowPaciente = mysql_fetch_array($rs);
    $empresa = null;
    $convenio = null;
    if(count($rowPaciente) > 0) {
        $empresa = $rowPaciente['empresa'];
        $convenio = $rowPaciente['convenio'];
    }

    $resumo_historia = anti_injection($resumo_historia, 'literal');

    $sql = "INSERT INTO relatorio_alta_enf (USUARIO_ID,PACIENTE_ID,empresa,plano,DATA,INICIO,FIM,RESUMO_HISTORIA) VALUES ('{$user}',{$paciente_id},{$empresa},{$convenio},now(),'{$inicio}','{$fim}','{$resumo_historia}')";
    
    $a = mysql_query($sql);
    if (!$a) {
        echo "Erro: tente novamente 1";
        mysql_query("rollback");
        return;
    }
    $id_alta = mysql_insert_id();
    savelog(mysql_escape_string(addslashes($sql)));
    
    $cuidados = explode("$",$cuidado);
    foreach ($cuidados as $i){
        if ($i != "" && $i != null) {
            $e = explode("#", $i);
            $cuidado_id = anti_injection($e[0], "numerico");
            $cuidado_outros = anti_injection($e[1], "literal");
            
            $sql2 = "INSERT INTO orientacoes_relatorio_alta_enf (USUARIO_ID,REL_ALTA_ID,DATA,INICIO,FIM,ORIENTACOES,OUTRAS_ORIENTACOES) VALUES ('{$user}','{$id_alta}',now(),'{$inicio}','{$fim}','{$cuidado_id}','{$cuidado_outros}')";
            
            $r = mysql_query($sql2);
//echo $sql2;
            if (!$r) {
                mysql_query("rollback");
                echo "Erro: tente novamente 2";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }
    mysql_query('commit');

		enviarEmail($paciente_id, 2, $empresa);

    //enviar_email_relatorio($paciente_id, 2, $empresa);

//print_r ("2");
    echo '1';
    return;
}

function editar_relatorio_alta($post){
    extract($post, EXTR_OVERWRITE);
    $inicio = implode("-",array_reverse(explode("/",$inicio)));
    $fim = implode("-",array_reverse(explode("/",$fim)));
    mysql_query("begin");
    $user = $_SESSION["id_user"];
    $empresa = $_SESSION["empresa_principal"];
    $sql = "UPDATE relatorio_alta_enf SET DATA = now(), INICIO = '{$inicio}', FIM = '{$fim}', RESUMO_HISTORIA = '{$resumo_historia}' WHERE ID = {$id_relatorio}";
    
    $a = mysql_query($sql);
    if (!$a) {
        echo "Erro: tente novamente 1";
        mysql_query("rollback");
        return;
    }
    
    savelog(mysql_escape_string(addslashes($sql)));
    
    $cuidados = explode("$",$cuidado);

    $sql2 = "DELETE FROM orientacoes_relatorio_alta_enf where ID = {$id_relatorio}";
    $r = mysql_query($sql2);
    
    foreach ($cuidados as $i){
        if ($i != "" && $i != null) {
            $e = explode("#", $i);
            $cuidado_id = anti_injection($e[0], "numerico");
            $cuidado_outros = anti_injection($e[1], "literal");
            
            $sql2 = "INSERT INTO orientacoes_relatorio_alta_enf (USUARIO_ID,REL_ALTA_ID,DATA,INICIO,FIM,ORIENTACOES,OUTRAS_ORIENTACOES) VALUES ('{$user}','{$id_alta}',now(),'{$inicio}','{$fim}','{$cuidado_id}','{$cuidado_outros}')";
            $r = mysql_query($sql2);
//echo $sql2;
            if (!$r) {
                mysql_query("rollback");
                echo "Erro: tente novamente 2";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }
    mysql_query('commit');
    echo '1';
    return;
}

function relatorio_aditivo($post){
    extract($post, EXTR_OVERWRITE);
    $inicio = implode("-",array_reverse(explode("/",$inicio)));
    $fim = implode("-",array_reverse(explode("/",$fim)));
    date_default_timezone_set('America/Sao_Paulo');
    $dataLimiteNow = new \DateTime();
    $dataLimit24 = new \DateTime();
    $dataLimit24->add(new \DateInterval('PT24H'));
    if(isset($contadorEntregaImediata)){
        if($contadorEntregaImediata > 0){
            if(isset($dataEntregaImediata) && isset($horaEntregaImediata)){
                $dataHoraEntregaImediata = \DateTime::createFromFormat('d/m/Y H:i', $dataEntregaImediata . ' ' . $horaEntregaImediata);

                    if($dataHoraEntregaImediata < $dataLimiteNow){
                        echo "A data e hora da entrega imediata não pode ser menor que a data e hora atual.";
                        return;
                    }

            }else{
                echo "Erro na aplicação, problema com a data e hora de entrega imediata. " ;
                return;
            }
        }
    }else{
        echo "Erro na aplicação, problema com o contador da entrega Imediata.";
        return;
    }
    if(isset($contadorDataAvulsa)){
        if($contadorDataAvulsa > 0){
            if(isset($dataAvulsa) && isset($horaAvulsa)){
                $dataHoraAvulsa = \DateTime::createFromFormat('d/m/Y H:i', $dataAvulsa . ' ' . $horaAvulsa);

                    if($dataHoraAvulsa < $dataLimit24){
                        echo "O praza mínimo para entrega avulsa deve ser de 24 horas.";
                        return;

                }
            }else{
                echo "Erro na aplicação, problema com a data e hora de entrega imediata. Linha: " . __LINE__ . __FILE__;
                return;
            }
        }
    }else{
        echo "Erro na aplicação, problema com o contador da data avulsa. Linha: ". __LINE__ . __FILE__;
        return;
    }

 
    mysql_query("begin");

    $user = $_SESSION["id_user"];

    $sql = "SELECT convenio, empresa FROM clientes WHERE idClientes = '{$paciente_id}'";
    $rs = mysql_query($sql);
    $rowPaciente = mysql_fetch_array($rs);
    $empresa = null;
    $convenio = null;
    if(count($rowPaciente) > 0) {
        $empresa = $rowPaciente['empresa'];
        $convenio = $rowPaciente['convenio'];
    }

    $sql = "INSERT INTO "
            . "relatorio_aditivo_enf "
            . "(USUARIO_ID,
            PACIENTE_ID,
            empresa,
            plano,
            DATA,
            INICIO,
            FIM,
            SOLICITACAO_ADITIVO,
            ALTERACAO_LESOES,
            FERIDAS,
            confirmado,
            confirmado_por,
            confirmado_em )"
            . " VALUES "
            . "('{$user}',
            '{$paciente_id}',
            '{$empresa}',
            '{$convenio}',
            now(),
            '{$inicio}',
            '{$fim}',
            '{$solicitacao_aditivo}',
            '{$quadro_lesoes}',
            '{$feridas_aditivo}',
            1,
            '{$user}',
            now()
            )";
    $a = mysql_query($sql);
    if (!$a) {
        echo "Erro: tente novamente 1";
        mysql_query("rollback");
        return;
    }    
    
    $id_relatorio = mysql_insert_id();
    savelog(mysql_escape_string(addslashes($sql)));
    
    $probativos = explode("$", $probativo);
    foreach ($probativos as $prob) {
        if ($prob != "" && $prob != null) {
            $i = explode("#", $prob);
            $cod_probativo = anti_injection($i[0], "literal");
            $desc_probativo = anti_injection($i[1], "literal");
            $just_procedimento = anti_injection($i[2], "literal");
            
            $just_procedimento = $just_procedimento== 'undefined'? '' :$just_procedimento;

            $sql5 = "INSERT INTO `problemas_relatorio_prorrogacao_enf`(`RELATORIO_ADITIVO_ENF_ID`, `CID_ID`, `DESCRICAO`, `OBSERVACAO`, `DATA`)
			VALUES ('{$id_relatorio}', '{$cod_probativo}', '{$desc_probativo}', '{$just_procedimento}', now())";
            $r = mysql_query($sql5);
            if (!$r) {
                mysql_query("ROLLBACK");
                echo "ERRO 2: Tente novamente mais tarde!";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    $item = explode("$", $feridas);
    foreach ($item as $i) {
        if ($i != "" && $i != null) {
            $e = explode("#", $i);
            $local = anti_injection($e[0], "literal");
            $caracteristica = anti_injection($e[1], "literal");
            $curativo = anti_injection($e[2], "literal");
            $prescricao = anti_injection($e[3], "literal");
            $file_uploaded_ids = !empty($e[4]) ? explode(',', $e[4]) : null;

            $sql3 = "INSERT INTO feridas_relatorio_prorrogacao_enf (USUARIO_ID,RELATORIO_ADITIVO_ENF_ID,DATA,INICIO,FIM,LOCAL,CARACTERISTICA,TIPO_CURATIVO,PRESCRICAO_PROX_PERIODO) VALUES ('{$user}','{$id_relatorio}',now(),'{$inicio}','{$fim}','{$local}','{$caracteristica}','{$curativo}','{$prescricao}')";
            //die($sql3);
            $r = mysql_query($sql3);

            if (!$r) {
                echo "Erro: tente novamente 3";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql3)));

            if (isset($file_uploaded_ids) && !empty($file_uploaded_ids)) {
              salvarFeridasRelatorio('aditivo', $file_uploaded_ids);
            }
        }
    }

    if(!empty($itens_solicitados)){
        $tipoSolicitacao = 'aditiva';
        $planoId = $plano;
        $regrasFluxo = new RegraFluxoAutorizacoes();
        $responseMedicamentos = $regrasFluxo->regrasMedicamentoByPlano($planoId, $tipoSolicitacao);
        $responseMateriais = $regrasFluxo->regrasMaterialByPlano($planoId, $tipoSolicitacao);
        $responseServicos = $regrasFluxo->regrasServicoByPlano($planoId, $tipoSolicitacao);
        $responseEquipamentos = $regrasFluxo->regrasEquipamentoByPlano($planoId, $tipoSolicitacao);
        $responseDietas = $regrasFluxo->regrasDietaByPlano($planoId, $tipoSolicitacao);

        $regrasMedicamentos = [];
        $regrasMateriais = [];
        $regrasServicos = [];
        $regrasEquipamentos = [];
        $regrasDietas = [];

        if(!empty($responseMedicamentos)){
            $regrasMedicamentos = $regrasFluxo->separarGrupoItemRegraFluxoSolicitacao($responseMedicamentos);
        }
        if(!empty($responseMateriais)){
            $regrasMateriais = $regrasFluxo->separarGrupoItemRegraFluxoSolicitacao($responseMateriais);
        }
        if(!empty($responseServicos)){
            $regrasServicos = $regrasFluxo->separarGrupoItemRegraFluxoSolicitacao($responseServicos);
        }
        if(!empty($responseEquipamentos)){
            $regrasEquipamentos = $regrasFluxo->separarGrupoItemRegraFluxoSolicitacao($responseEquipamentos);
        }
        if(!empty($responseDietas)){
            $regrasDietas = $regrasFluxo->separarGrupoItemRegraFluxoSolicitacao($responseDietas);
        }
        if(!empty($responseEquipamentos)){
            $regrasEquipamentos = $regrasFluxo->separarGrupoItemRegraFluxoSolicitacao($responseEquipamentos);
        }
    }

    $itensSolicitados= explode("$", $itens_solicitados);



    foreach ($itensSolicitados as $itens){
        if($itens != "" && $itens != null){
            $i = explode("#", $itens);
            $catalogoId = anti_injection($i[0], "numerico");
            $tiss = anti_injection($i[1], "literal");
            $tipo = anti_injection($i[2], "numerico");
            $qtd = anti_injection($i[3], "literal");
            $qtd = !empty($qtd) ? $qtd : 0;
            $qtdKit = anti_injection($i[4], "numerico");
            $idKit = anti_injection($i[5], "numerico");
            $entregaImediata = anti_injection($i[6], "literal");
            $valorVenda = anti_injection($i[7], "literal");
            $categoria = anti_injection($i[8], "literal");
            $frequencia = anti_injection($i[9], "literal");
            $tipoEquipamento = $tipo == 5 ? 1: 0 ;
            $dataEntrega = $entregaImediata == "S" ? $dataHoraEntregaImediata->format('Y-m-d H:i:s'):$dataHoraAvulsa->format('Y-m-d H:i:s');
            $status = $entregaImediata == "S" ? 2 : 1;

            $liberarColuna = ", autorizado, data_auditado, AUDITOR_ID";
            $liberarValor = ",'{$qtd}', now(), '{$user}'";
            $item = [
                'id' => $catalogoId,
                'quantidade' => $qtd,
                'grupo' => $categoria,
                'usuario' => $user,
                'valor' => $valorVenda
            ];
            $condicaoAuditar = '';

            if($tipo == 1 && !empty($regrasMateriais) && $entregaImediata != "S"){
                $condicaoAuditar = $regrasFluxo->condicionalRegraBloqueio($item,$regrasMateriais);
            }elseif($tipo == 3 && !empty($regrasDietas) && $entregaImediata != "S"){
                $condicaoAuditar = $regrasFluxo->condicionalRegraBloqueio($item,$regrasDietas);
            }elseif($tipo == 0 && !empty($regrasMedicamentos) && $entregaImediata != "S"){
                $condicaoAuditar = $regrasFluxo->condicionalRegraBloqueio($item,$regrasMedicamentos);
            }elseif($tipo == 5 && !empty($regrasEquipamentos) && $entregaImediata != "S") {
                $condicaoAuditar = $regrasFluxo->condicionalRegraBloqueio($item, $regrasEquipamentos);
            }

            if(!empty($condicaoAuditar)){
                $liberarColuna = $condicaoAuditar['coluna'];
                $liberarValor  = $condicaoAuditar['valores'];
                $status = $condicaoAuditar['status'];
            }
            $sql = "INSERT INTO
                      solicitacao_rel_aditivo_enf
                      (id,
                       relatorio_enf_aditivo_id,
                       catalogo_id,
                       tiss,
                       tipo,
                       quantidade,
                       frequencia,
                       quantidade_kit,
                       kit_id,
                       entrega_imediata,
                       data_entrega
                       )
                       VALUES
                       ( null,
                         '{$id_relatorio}',
                         '{$catalogoId}',
                         '{$tiss}',  
                         '{$tipo}',
                         '{$qtd}',
                         '{$frequencia}',
                         '{$qtdKit}',
                         '{$idKit}',
                          '{$entregaImediata}',
                          '{$dataEntrega}'
                        )                       
                       ";                       
            $r = mysql_query($sql);
            if (!$r) {
                echo "Erro: tente novamente 3";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));

            if($tipo != '4') {
                $autorizado = $qtd;
                $sqlInsertSolicitacao = "INSERT INTO `solicitacoes`
                                              (
                                                `paciente`,
                                                `enfermeiro`,
                                                `NUMERO_TISS`,
                                                `CATALOGO_ID`,
                                                `qtd`,                                                
                                                `tipo`,
                                                `data`,
                                                `status`,
                                                `idPrescricao`,
                                                `DATA_SOLICITACAO`,
                                                `KIT_ID`,
                                                `TIPO_SOLICITACAO`,
                                                `DATA_MAX_ENTREGA`,
                                                JUSTIFICATIVA_AVULSO_ID,
                                                TIPO_SOL_EQUIPAMENTO,
                                                entrega_imediata,
                                                relatorio_aditivo_enf_id
                                                $liberarColuna

                                              )
                                                 VALUES
                                              (          '{$paciente_id}',
                                                        '{$user}',
                                                        '{$tiss}',
                                                        '{$catalogoId}',
                                                        '{$qtd}',
                                                        
                                                        '{$tipo}',
                                                         now(),
                                                        '{$status}',
                                                        '-1',
                                                         now(),
                                                        '{$idKit}',
                                                            8,
                                                        '{$dataEntrega}',
                                                        12,
                                                        {$tipoEquipamento},
                                                            '{$entregaImediata}',
                                                        '{$id_relatorio}'
                                                        $liberarValor

                                                )";
                $resultInsertSolicitacao = mysql_query($sqlInsertSolicitacao);
                if (!$resultInsertSolicitacao) {
                    echo "Erro ao Inserir item na solicitação de enfermagem. " . $sqlInsertSolicitacao;
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sqlInsertSolicitacao)));
            }

        }
    }

    mysql_query('commit');

		enviarEmail($paciente_id, 3, $empresa);

    echo '1';
    return;
}

function editar_relatorio_aditivo($post){
    extract($post, EXTR_OVERWRITE);
    $inicio = implode("-",array_reverse(explode("/",$inicio)));
    $fim = implode("-",array_reverse(explode("/",$fim)));
    mysql_query("begin");
    $user = $_SESSION["id_user"];
    $sql = "UPDATE relatorio_aditivo_enf SET USUARIO_ID = '{$user}',PACIENTE_ID = '{$paciente_id}',DATA = now(),INICIO = '{$inicio}',FIM = '{$fim}',SOLICITACAO_ADITIVO = '{$solicitacao_aditivo}',ALTERACAO_LESOES = '{$quadro_lesoes}', FERIDAS = '{$feridas_aditivo}' WHERE ID = {$id_relatorio}";
    $a = mysql_query($sql);
    if (!$a) {
        echo "Erro (1): entre em contato com o TI";
        mysql_query("rollback");
        return;
    }        
    
    savelog(mysql_escape_string(addslashes($sql)));
    
    $probativo = explode("$", $probativos);
    foreach ($probativo as $prob) {
        if ($prob != "" && $prob != null) {
            $i = explode("#", $prob);
            $cod_probativo = anti_injection($i[0], "literal");
            $desc_probativo = anti_injection($i[1], "literal");
            $just_procedimento = anti_injection($i[2], "literal");
        $just_procedimento = $just_procedimento== 'undefined'? '' :$just_procedimento;
            $sql5 = "UPDATE `problemas_relatorio_prorrogacao_enf` SET  CID_ID = '{$cod_probativo}', DESCRICAO = '{$desc_probativo}', OBSERVACAO = '{$just_procedimento}', DATA = now() WHERE RELATORIO_ADITIVO_ENF_ID = {$id_relatorio}";
            $r = mysql_query($sql5);
            if (!$r) {
                mysql_query("ROLLBACK");
                echo "Erro (2): entre em contato com o TI";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
    }

    $item = explode("$", $feridas);
  foreach ($item as $i) {
    if ($i != "" && $i != null && $i != "undefined") {
      $e = explode("#", $i);
      $local = anti_injection($e[0], "literal");
      $caracteristica = anti_injection($e[1], "literal");
      $curativo = anti_injection($e[2], "literal");
      $prescricao = anti_injection($e[3], "literal");
      $id_ferida = anti_injection($e[4], "literal");
      $file_uploaded_ids = !empty($e[5]) ? explode(',', $e[5]) : null;
      
      $sql3 = "UPDATE feridas_relatorio_prorrogacao_enf SET EDITADA_EM = current_timestamp WHERE ID = '{$id_ferida}'";
      $r = mysql_query($sql3);

      if (!$r) {
        echo "Erro (3): entre em contato com o TI";
        mysql_query("rollback");
        return;
      }
      savelog(mysql_escape_string(addslashes($sql3)));
      
      
      $sql4 = "INSERT INTO feridas_relatorio_prorrogacao_enf (USUARIO_ID,RELATORIO_ADITIVO_ENF_ID,DATA,INICIO,FIM,LOCAL,CARACTERISTICA,TIPO_CURATIVO,PRESCRICAO_PROX_PERIODO) VALUES ('{$user}','{$id_relatorio}',now(),'{$inicio}','{$fim}','{$local}','{$caracteristica}','{$curativo}','{$prescricao}')";
      $b = mysql_query($sql4);

      if (!$b) {
        echo "Erro (4): entre em contato com o TI";
        mysql_query("rollback");
        return;
      }
      savelog(mysql_escape_string(addslashes($sql4)));

      if (isset($file_uploaded_ids) && !empty($file_uploaded_ids)) {
        salvarFeridasRelatorio('aditivo', $file_uploaded_ids);
      }
    }
  }

  $item_excluir = explode("$", $feridas_excluir);
  foreach ($item_excluir as $i) {
    if ($i != "" && $i != null && $i != "undefined") {
      $e = explode("#", $i);
      $local_excluir = anti_injection($e[0], "literal");
      $caracteristica_excluir = anti_injection($e[1], "literal");
      $curativo_excluir = anti_injection($e[2], "literal");
      $prescricao_excluir = anti_injection($e[3], "literal");
      $id_ferida_excluir = anti_injection($e[4], "literal");
      
      $sql5 = "UPDATE feridas_relatorio_prorrogacao_enf SET DATA_DESATIVADA = current_timestamp WHERE ID = '{$id_ferida_excluir}'";
      $r = mysql_query($sql5);

      if (!$r) {
        echo "Erro (5): entre em contato com o TI";
        mysql_query("rollback");
        return;
      }
      savelog(mysql_escape_string(addslashes($sql5)));
    }
  }
  $idSolicitacaoRelAditivoManter = anti_injection($id_solicitacao_rel_aditivo_manter, 'literal');
   if(isset($idSolicitacaoRelAditivoManter) ){
       $idSolicitacaoManter = explode('$', $idSolicitacaoRelAditivoManter);       
       $idSolicitacaoManter =  implode(',', array_filter($idSolicitacaoManter));
       if(!empty($idSolicitacaoRelAditivoManter)){
           $cond="id not in ({$idSolicitacaoManter}) and ";
       }else{
           $cond ='';
       }
       $sql = "update 
               solicitacao_rel_aditivo_enf
               set
               cancelado_por={$user},
               cancelado_em = now()
               where
               {$cond}
               relatorio_enf_aditivo_id = $id_relatorio and 
               cancelado_em is null
              ";
        $r = mysql_query($sql);
            if (!$r) {
                echo "Erro (7): entre em contato com o TI";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql))); 
   }
  $itensSolicitados= explode("$", $itens_solicitados);
    foreach ($itensSolicitados as $itens){
        if($itens != "" && $itens != null){
            $i = explode("#", $itens);
            $catalogoId = anti_injection($i[0], "numerico");
            $tiss = anti_injection($i[1], "literal");
            $tipo = anti_injection($i[2], "numerico");
            $qtd = anti_injection($i[3], "numerico");
            $qtdKit = anti_injection($i[4], "numerico");
            $idKit = anti_injection($i[5], "numerico");
            $sql = "INSERT INTO
                      solicitacao_rel_aditivo_enf
                      (id,
                       relatorio_enf_aditivo_id,
                       catalogo_id,
                       tiss,
                       tipo,
                       quantidade,
                       quantidade_kit,
                       kit_id
                       )
                       VALUES
                       ( null,
                         '{$id_relatorio}',
                         '{$catalogoId}',
                         '{$tiss}',  
                         '{$tipo}',
                         '{$qtd}',
                         '{$qtdKit}',
                         '{$idKit}'  
                        )                       
                       ";                       
            $r = mysql_query($sql);
            if (!$r) {
                echo "Erro (6): entre em contato com o TI";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));                  
        }
    }
   
    
    mysql_query('commit');

//print_r ("2");
    echo '1';
    
    return;
}

function relatorio_prorrogacao($post){
    extract($post, EXTR_OVERWRITE);

    $inicio = implode("-",array_reverse(explode("/",$inicio)));
    $fim = implode("-",array_reverse(explode("/",$fim)));
    mysql_query("begin");


    $user = anti_injection($_SESSION["id_user"], 'numerico');
    $paciente_id = anti_injection($paciente_id, 'numerico');
    $inicio = anti_injection($inicio, 'literal');
    $fim = anti_injection($fim, 'literal');
    $evolucao_clinica = anti_injection($evolucao_clinica, 'literal');
    $orientacoes = anti_injection($orientacoes, 'literal');
    $feridas_aditivo = anti_injection($feridas_aditivo, 'literal');
    $pad_enfermagem = anti_injection($pad_enfermagem, 'literal');

    $sql = "SELECT convenio, empresa FROM clientes WHERE idClientes = '{$paciente_id}'";
    $rs = mysql_query($sql);
    $rowPaciente = mysql_fetch_array($rs);
    $empresa = null;
    $convenio = null;
    if(count($rowPaciente) > 0) {
        $empresa = $rowPaciente['empresa'];
        $convenio = $rowPaciente['convenio'];
    }

    $sql = "INSERT INTO relatorio_prorrogacao_enf (USUARIO_ID,PACIENTE_ID,empresa,plano,DATA,INICIO,FIM,EVOLUCAO_CLINICA,ORIENTACOES,FERIDAS,PAD_ENFERMAGEM) VALUES ('{$user}','{$paciente_id}','{$empresa}','{$convenio}',now(),'{$inicio}','{$fim}','{$evolucao_clinica}','{$orientacoes}','{$feridas_aditivo}','{$pad_enfermagem}')";
    $a = mysql_query($sql);
   
    if (!$a) {
        echo "Erro: tente novamente 1";
        mysql_query("rollback");
        return;
    }
    $id_relatorio = mysql_insert_id();
    savelog(mysql_escape_string(addslashes($sql)));    
    
    
    $item = explode("$", $feridas);
    foreach ($item as $i) {
        if ($i != "" && $i != null) {
            $e = explode("#", $i);
            $local = anti_injection($e[0], "literal");
            $caracteristica = anti_injection($e[1], "literal");
            $curativo = anti_injection($e[2], "literal");
            $prescricao = anti_injection($e[3], "literal");
            $file_uploaded_ids = !empty($e[4]) ? explode(',', $e[4]) : null;
            $observacao_imagem = anti_injection($e[5], "literal");
            

            $sql3 = "INSERT INTO feridas_relatorio_prorrogacao_enf (USUARIO_ID,RELATORIO_PRORROGACAO_ENF_ID,DATA,INICIO,FIM,LOCAL,CARACTERISTICA,TIPO_CURATIVO,PRESCRICAO_PROX_PERIODO)
                      VALUES ('{$user}','{$id_relatorio}',now(),'{$inicio}','{$fim}','{$local}','{$caracteristica}','{$curativo}','{$prescricao}');";

            $r = mysql_query($sql3);

            if (!$r) {
                echo "Erro: tente novamente 3";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql3)));

            if (isset($file_uploaded_ids) && !empty($file_uploaded_ids)) {
              salvarFeridasRelatorio('prorrogacao', $file_uploaded_ids);
            }
        }
    }
    
    mysql_query('commit');

		enviarEmail($paciente_id, 4, $empresa);

    //enviar_email_relatorio($paciente_id, 4, $empresa);
    echo '1';
    return;
}

function salvarFeridasRelatorio($relatorio, $file_uploaded_ids) {
  if (isset($file_uploaded_ids) && !empty($file_uploaded_ids)) {
    $relatorio_tabela = array(
      'prorrogacao' => 'feridas_relatorio_prorrogacao_enf_imagem',
      'aditivo'     => 'feridas_relatorio_prorrogacao_enf_imagem',
    );

    if (isset($relatorio_tabela[$relatorio]))
      $tabela = $relatorio_tabela[$relatorio];

    foreach ($file_uploaded_ids as $file_upload_id) {
        $file_upload_id = explode('date',$file_upload_id);
        $fileUpId =$file_upload_id[0];
        $fileData = !empty($file_upload_id[1]) ? $file_upload_id[1]: date('Y-m-d');


      if (! empty($file_upload_id)) {
        $sql = "INSERT INTO {$tabela} (ferida_relatorio_prorrogacao_enf_id, file_uploaded_id, data_imagem)
                VALUES((SELECT id FROM feridas_relatorio_prorrogacao_enf ORDER BY id DESC LIMIT 1),
                  {$fileUpId},'{$fileData}');";
      }

      if (! mysql_query($sql)) {
        echo "Erro: tente novamente";
        mysql_query("rollback");
        return;
      }
      savelog(mysql_escape_string(addslashes($sql)));
    }
  }
}

function editar_relatorio_prorrogacao($post){
  extract($post, EXTR_OVERWRITE);
  $inicio = implode("-",array_reverse(explode("/",$inicio)));
  $fim = implode("-",array_reverse(explode("/",$fim)));

  $user = anti_injection($_SESSION["id_user"], 'numerico');
  $paciente_id = anti_injection($paciente_id, 'numerico');
  $inicio = anti_injection($inicio, 'literal');
  $fim = anti_injection($fim, 'literal');
  $evolucao_clinica = anti_injection($evolucao_clinica, 'literal');
  $orientacoes = anti_injection($orientacoes, 'literal');
  $feridas_aditivo = anti_injection($feridas_aditivo, 'literal');
  $pad_enfermagem = anti_injection($pad_enfermagem, 'literal');

  mysql_query("begin");
  $sql = "UPDATE relatorio_prorrogacao_enf SET USUARIO_ID = '{$user}', DATA = now(), INICIO = '{$inicio}',FIM = '{$fim}',EVOLUCAO_CLINICA = '{$evolucao_clinica}',ORIENTACOES = '{$orientacoes}',FERIDAS = '{$feridas_aditivo}',PAD_ENFERMAGEM = '{$pad_enfermagem}' WHERE ID = '{$id_relatorio}'";
//    $sql = "INSERT INTO relatorio_prorrogacao_enf (USUARIO_ID,PACIENTE_ID,DATA,INICIO,FIM,EVOLUCAO_CLINICA,ORIENTACOES,FERIDAS,PAD_ENFERMAGEM) VALUES ('{$user}','{$paciente_id}',now(),'{$inicio}','{$fim}','{$evolucao_clinica}','{$orientacoes}','{$feridas_sn}','{$pad_enfermagem}')";
//    echo $sql;
  $a = mysql_query($sql);
  if (!$a) {
    echo "Erro: tente novamente 1";
    mysql_query("rollback");
    return;
  }
//    $id_relatorio = mysql_insert_id();
  savelog(mysql_escape_string(addslashes($sql)));    
  
    
  
  $item = explode("$", $feridas);
  foreach ($item as $i) {
    if ($i != "" && $i != null && $i != "undefined") {
      $e = explode("#", $i);
      $local = anti_injection($e[0], "literal");
      $caracteristica = anti_injection($e[1], "literal");
      $curativo = anti_injection($e[2], "literal");
      $prescricao = anti_injection($e[3], "literal");
      $id_ferida = anti_injection($e[4], "literal");
      $file_uploaded_ids = !empty($e[5]) ? explode(',', $e[5]) : null;
      $observacao_imagem = anti_injection($e[6], "literal");

      if ($file_uploaded_id == '-1')
        $file_uploaded_id = 'NULL';
      
      $sql3 = "UPDATE feridas_relatorio_prorrogacao_enf SET EDITADA_EM = current_timestamp WHERE ID = '{$id_ferida}'";
      $r = mysql_query($sql3);

      if (!$r) {
        echo "Erro: tente novamente 3";
        mysql_query("rollback");
        return;
      }
      savelog(mysql_escape_string(addslashes($sql3)));


      $sql4 = "INSERT INTO feridas_relatorio_prorrogacao_enf (USUARIO_ID,RELATORIO_PRORROGACAO_ENF_ID,DATA,INICIO,FIM,LOCAL,CARACTERISTICA,TIPO_CURATIVO,PRESCRICAO_PROX_PERIODO)
                VALUES ('{$user}','{$id_relatorio}',now(),'{$inicio}','{$fim}','{$local}','{$caracteristica}','{$curativo}','{$prescricao}')";
      $b = mysql_query($sql4);

      if (!$b) {
        echo "Erro: tente novamente 4";
        mysql_query("rollback");
        return;
      }
      savelog(mysql_escape_string(addslashes($sql4)));

      if (isset($file_uploaded_ids) && !empty($file_uploaded_ids)) {
        salvarFeridasRelatorio('prorrogacao', $file_uploaded_ids);
      }
    }
  }

  $item_excluir = explode("$", $feridas_excluir);
  foreach ($item_excluir as $i) {
    if ($i != "" && $i != null && $i != "undefined") {
      $e = explode("#", $i);
      $local_excluir = anti_injection($e[0], "literal");
      $caracteristica_excluir = anti_injection($e[1], "literal");
      $curativo_excluir = anti_injection($e[2], "literal");
      $prescricao_excluir = anti_injection($e[3], "literal");
      $id_ferida_excluir = anti_injection($e[4], "literal");
      
      $sql5 = "UPDATE feridas_relatorio_prorrogacao_enf SET DATA_DESATIVADA = current_timestamp WHERE ID = '{$id_ferida_excluir}'";
      $r = mysql_query($sql5);

      if (!$r) {
        echo "Erro: tente novamente 5";
        mysql_query("rollback");
        return;
      }
      savelog(mysql_escape_string(addslashes($sql5)));
    }
  }

  mysql_query('commit');

  echo '1';
  return;
}


function relatorio_visita($post){
    extract($post, EXTR_OVERWRITE);
    $inicio = implode("-",array_reverse(explode("/",$inicio)));
    $fim = implode("-",array_reverse(explode("/",$fim)));
    mysql_query("begin");
    $user = $_SESSION["id_user"];

    $sql = "SELECT convenio, empresa FROM clientes WHERE idClientes = '{$paciente_id}'";
    $rs = mysql_query($sql);
    $rowPaciente = mysql_fetch_array($rs);
    $empresa = null;
    $convenio = null;
    if(count($rowPaciente) > 0) {
        $empresa = $rowPaciente['empresa'];
        $convenio = $rowPaciente['convenio'];
    }

    $sql2 = "INSERT INTO relatorio_visita_enf (USUARIO_ID,PACIENTE_ID,empresa,plano,DATA,DATA_VISITA,ALTERACOES_SN,DESCRICAO_ALTERACOES,PROBLEMAS_SN,PROBLEMAS_MATERIAL,DIFICULDADE_SN,DIFICULDADE_PROFISSIONAL,FAMILIA_SN,DIFICULDADE_FAMILIA,ORIENTACOES) VALUES ('{$user}','{$paciente_id}','{$empresa}','{$convenio}',now(),'{$inicio}','{$alteracoes_sn}','{$desc_alteracoes}','{$problemas_sn}','{$prob_material}','{$dificuldades_sn}','{$dificuldade}','{$familia_sn}','{$familia}','{$orientacoes}')";
    $a = mysql_query($sql2);
    if (!$a) {
        echo "Erro: tente novamente 1";
        mysql_query("rollback");
        return;
    }
    savelog(mysql_escape_string(addslashes($sql2)));
    
    mysql_query('commit');

		enviarEmail($paciente_id, 5, $empresa);

    //enviar_email_relatorio($paciente_id, 5, $empresa);
//print_r ("2");
    echo '1';
    return;
}

function editar_relatorio_visita($post){
    extract($post, EXTR_OVERWRITE);
    $inicio = implode("-",array_reverse(explode("/",$inicio)));
    $fim = implode("-",array_reverse(explode("/",$fim)));
    mysql_query("begin");
    $user = $_SESSION["id_user"];
    
    $sql = "UPDATE relatorio_visita_enf SET EDITADO_EM = current_timestamp WHERE ID = {$id_relatorio}";
      $r = mysql_query($sql);
      if (!$r) {
        echo "Erro: tente novamente 3";
        mysql_query("rollback");
        return;
      }
      savelog(mysql_escape_string(addslashes($sql)));

    $sql2 = "INSERT INTO relatorio_visita_enf (USUARIO_ID,PACIENTE_ID,DATA,DATA_VISITA,ALTERACOES_SN,DESCRICAO_ALTERACOES,PROBLEMAS_SN,PROBLEMAS_MATERIAL,DIFICULDADE_SN,DIFICULDADE_PROFISSIONAL,FAMILIA_SN,DIFICULDADE_FAMILIA,ORIENTACOES) VALUES ('{$user}','{$paciente_id}',now(),'{$inicio}','{$alteracoes_sn}','{$desc_alteracoes}','{$problemas_sn}','{$prob_material}','{$dificuldades_sn}','{$dificuldade}','{$familia_sn}','{$familia}','{$orientacoes}')";
    $a = mysql_query($sql2);
    if (!$a) {
        echo "Erro: tente novamente 1";
        mysql_query("rollback");
        return;
    }
    savelog(mysql_escape_string(addslashes($sql2)));
    
    mysql_query('commit');    
    echo '1';
    return;
}

function relatorio_deflagrado($post){
    extract($post, EXTR_OVERWRITE);
    $inicio = implode("-",array_reverse(explode("/",$inicio)));
    $fim = implode("-",array_reverse(explode("/",$fim)));
    mysql_query("begin");
    $user = $_SESSION["id_user"];

    $sql = "SELECT convenio, empresa FROM clientes WHERE idClientes = '{$paciente_id}'";
    $rs = mysql_query($sql);
    $rowPaciente = mysql_fetch_array($rs);
    $empresa = null;
    $convenio = null;
    if(count($rowPaciente) > 0) {
        $empresa = $rowPaciente['empresa'];
        $convenio = $rowPaciente['convenio'];
    }

    $sql = "INSERT INTO relatorio_deflagrado_enf (USUARIO_ID,PACIENTE_ID,empresa,plano,DATA,INICIO,FIM,HISTORIA_CLINICA,SERVICOS_PRESTADOS) VALUES ('{$user}','{$paciente_id}','{$empresa}','{$convenio}',now(),'{$inicio}','{$fim}','{$historia_clinica}','{$servicos_prestados}')";
    $a = mysql_query($sql);
    if (!$a) {
        echo "Erro: tente novamente 1";
        mysql_query("rollback");
        return;
    }
    $id_relatorio = mysql_insert_id();
    savelog(mysql_escape_string(addslashes($sql)));

    $sql_problemas = "
            SELECT
                max(ID)
            FROM
                fichamedicaevolucao
            WHERE
                PACIENTE_ID= {$paciente_id}
              ";
        $result = mysql_query($sql_problemas);
        $maior_evolucao = mysql_result($result, 0);
        $sql_2 = "
            SELECT
                pae.DESCRICAO as nome,
                pae.CID_ID,
                pae.STATUS,
                pae.OBSERVACAO
            FROM
                problemaativoevolucao as pae
            WHERE
                pae.EVOLUCAO_ID = {$maior_evolucao}
            ORDER BY
                pae.status DESC
              ";
        $result2 = mysql_query($sql_2);
        while ($row = mysql_fetch_array($result2)) {
          $desc_probativo =$row['nome'];
          $cod_probativo = $row['CID_ID'];
          $justificativa = $row['OBSERVACAO'];

          $sql2 = "INSERT INTO `problemas_relatorio_prorrogacao_enf`(`RELATORIO_DEFLAGRADO_ENF_ID`, `CID_ID`, `DESCRICAO`, `OBSERVACAO`, `DATA`)
          VALUES ('{$id_relatorio}', '{$cod_probativo}', '{$desc_probativo}', '{$justificativa}', now())";
          $r = mysql_query($sql2);
          if (!$r) {
            mysql_query("ROLLBACK");
            echo "ERRO 2: Tente novamente mais tarde!";
            return;
          }
          savelog(mysql_escape_string(addslashes($sql2)));
        }
        
    mysql_query('commit');

		$empresa = $_SESSION["empresa_principal"];
		enviarEmail($paciente_id, 13, $empresa);

    //enviar_email_relatorio($paciente_id, 13, $empresa);
    //print_r ("2");
    echo '1';
    return;
}

function editar_relatorio_deflagrado($post){
    extract($post, EXTR_OVERWRITE);
    $inicio = implode("-",array_reverse(explode("/",$inicio)));
    $fim = implode("-",array_reverse(explode("/",$fim)));
    mysql_query("begin");
    $user = $_SESSION["id_user"];
    
    $sql = "UPDATE relatorio_deflagrado_enf SET INICIO = '{$inicio}', FIM = '{$fim}', HISTORIA_CLINICA = '{$historia_clinica}', SERVICOS_PRESTADOS = '{$servicos_prestados}',EDITADO_EM = current_timestamp, EDITADO_POR = $user WHERE ID = {$id_relatorio}";
      $r = mysql_query($sql);
      if (!$r) {
        echo "Erro ao Editar";
        mysql_query("rollback");
        return;
      }
      savelog(mysql_escape_string(addslashes($sql)));

    /*$probativo = explode("$", $probativos);
    foreach ($probativo as $prob) {
        if ($prob != "" && $prob != null) {
            $i = explode("#", $prob);
            $cod_probativo = anti_injection($i[0], "literal");
            $desc_probativo = anti_injection($i[1], "literal");
            $just_procedimento = anti_injection($i[2], "literal");

            $sql3 = "UPDATE `problemas_relatorio_prorrogacao_enf` SET  CID_ID = '{$cod_probativo}', DESCRICAO = '{$desc_probativo}', OBSERVACAO = '{$just_procedimento}', DATA = now() WHERE RELATORIO_DEFLAGRADO_ENF_ID = {$id_relatorio}";
            $r = mysql_query($sql3);
            if (!$r) {
                mysql_query("ROLLBACK");
                echo "ERRO 2: Tente novamente mais tarde!";
                return;
            }
            savelog(mysql_escape_string(addslashes($sql3)));
        }
    }*/
    
    mysql_query('commit');    
    echo '1';
    return;
}

function consultar_itens_kit($post) {
    extract($post, EXTR_OVERWRITE);
    $sql = "SELECT
            concat(b.principio,' - ',b.apresentacao,'  Qtd ',ik.qtd) as itens,
            ik.NUMERO_TISS,
            b.principio,
            ik.qtd,
            b.tipo, 
            ik.CATALOGO_ID
	FROM
            kitsmaterial as k inner join
            itens_kit as ik ON (k.idKit = ik.idKit) inner join
            catalogo as b ON (ik.CATALOGO_ID = b.ID)
	WHERE
            k.idKit= {$cod}
	ORDER BY k.nome";
    
    $result = mysql_query($sql);
    $tipo = 15;    
    echo "<tr bgcolor='#ffffff' cod='{$cod}' class='kit'>"
                . "<td> "
                   . "<b>KIT-{$nome}  Quantidade:{$qtd} {$entregaImediata}</b>"
                       . "<img align='right' class='remover-item-solicitacao-kit' "
                            . "src='../utils/delete_16x16.png' title='Remover' border='0' />"
                . "</td>"
       . "</tr>";
    while ($row = mysql_fetch_array($result)) {
        $a[] = $row['cod'];
        $tipo_bras = $row['tipo'];
        
            $tiss = $row['NUMERO_TISS'];
            $catalogo_id = $row['CATALOGO_ID'];
            $nome = $row['principio'];
            $qtd_item = $row['qtd']*$qtd;
            $linha = $nome . ".  Quantidade: " . $qtd_item ;
            $dados = "qtd-kit='{$qtd}' catalogo-id='{$catalogo_id}' tiss='{$tiss}' qtd='{$qtd_item}'"
                    . " tipo='{$tipo_bras}' id-kit='{$cod}' id-solicitacao-rel-aditivo='0' ";
            echo "<tr bgcolor='#FAFAFA' cod='{$cod}' class= 'dados' {$dados} >"
                       . "<td>"
                            . " {$linha} {$entregaImediata}"
                       . "</td>"
                . "</tr>";
        
    }
}
function confirmarRelatorioAditivo($post)
{
    extract($post, EXTR_OVERWRITE);
    $idPaciente=anti_injection($id_paciente, "numerico");
    $idRelatorioAditivo=anti_injection($id_relatorio_aditivo, "numerico");
    
    mysql_query('begin');
    $sqlMaiorCapmedica = "Select Max(id) from capmedica where paciente ='{$idPaciente}'";
        $resultMaiorCapmedica = mysql_query($sqlMaiorCapmedica);
        $maior_cap = mysql_result($resultMaiorCapmedica, 0, 0);
    $user = $_SESSION["id_user"];
    $sql="update 
            relatorio_aditivo_enf
            set
            confirmado = 1,
            confirmado_por={$user},
            confirmado_em=now()
            where 
            id ={$idRelatorioAditivo}";
    $r = mysql_query($sql);
      if (!$r) {
        echo "Erro ao salvar ";
        mysql_query("rollback");
        return;
      }
    savelog(mysql_escape_string(addslashes($sql)));
    
    $sqlItensSolicitados="SELECT            
                            solicitacao_rel_aditivo_enf.relatorio_enf_aditivo_id,
                            solicitacao_rel_aditivo_enf.catalogo_id,
                            solicitacao_rel_aditivo_enf.tiss,
                            solicitacao_rel_aditivo_enf.tipo,
                            solicitacao_rel_aditivo_enf.quantidade,            
                            solicitacao_rel_aditivo_enf.kit_id,
                            relatorio_aditivo_enf.USUARIO_ID,
                            relatorio_aditivo_enf.PACIENTE_ID
                            FROM
                            solicitacao_rel_aditivo_enf
                            INNER JOIN relatorio_aditivo_enf ON relatorio_aditivo_enf.ID = solicitacao_rel_aditivo_enf.relatorio_enf_aditivo_id
                            WHERE
                            solicitacao_rel_aditivo_enf.relatorio_enf_aditivo_id ={$idRelatorioAditivo}
                            AND solicitacao_rel_aditivo_enf.cancelado_por IS NULL";
    
       $returnItensSolicitados =  mysql_query($sqlItensSolicitados);
         while ($row = mysql_fetch_array($returnItensSolicitados)) {
             
                     $tipoEquipamento = $row['tipo'] == 5 ? 1: 0 ;
                    $sqlInsertSolicitacao= "INSERT INTO `solicitacoes`
                                              (
                                                `paciente`,
                                                `enfermeiro`,
                                                `NUMERO_TISS`,
                                                `CATALOGO_ID`,
                                                `qtd`,
                                                `tipo`,
                                                `data`,
                                                `status`,
                                                `idPrescricao`,
                                                `DATA_SOLICITACAO`,
                                                `KIT_ID`,
                                                `TIPO_SOLICITACAO`,
                                                `DATA_MAX_ENTREGA`,
                                                JUSTIFICATIVA_AVULSO_ID,
                                                TIPO_SOL_EQUIPAMENTO,
                                                entrega_imediata,
                                                relatorio_aditivo_enf_id
                                                
                                              )
                                                 VALUES
                                              (
                                                        '{$row['PACIENTE_ID']}',
                                                        '{$row['USUARIO_ID']}',
                                                        '{$row['tiss']}',
                                                        '{$row['catalogo_id']}',
                                                        '{$row['quantidade']}',
                                                        '{$row['tipo']}',
                                                         now(),
                                                        '0',
                                                        '-1',
                                                         now(),
                                                        '{$row['kit_id']}',
                                                            8,
                                                        now() + INTERVAL 1 DAY,
                                                        12,
                                                        {$tipoEquipamento},
                                                            'N',
                                                        '{$idRelatorioAditivo}'
                                                        
                                                )";
                        $resultInsertSolicitacao = mysql_query($sqlInsertSolicitacao);
                        if (!$resultInsertSolicitacao) {
                          echo "Erro ao Inserir item na solicitação de enfermagem. ".  mysql_error();
                          mysql_query("rollback");
                          return;
                        }
                      savelog(mysql_escape_string(addslashes($sqlInsertSolicitacao)));
           if($row['tipo'] == 5){
               $sqlInsertEqpAtivo="INSERT INTO "
                . "equipamentosativos "
                . "(ID,"
                . "COBRANCA_PLANOS_ID,"
                . "CAPMEDICA_ID,"
                . "QTD,"
                . "OBS,"
                . "USUARIO,"
                . "DATA,"
                . "PACIENTE_ID)"
                . " VALUES "
                . "(null,"
                . " '{$row['catalogo_id']}',"
                . "'{$maior_cap}',"
                . "'{$row['quantidade']}',"
                . "'',"
                . "'{$row['USUARIO_ID']}',"
                . "now(),"
                . "'{$row['PACIENTE_ID']}')";
                $rInsertEqpAtivo = mysql_query($sqlInsertEqpAtivo);

                if (!$rInsertEqpAtivo) {
                   echo "Erro na aplicação Linha: ". __LINE__ . __FILE__;
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sqlInsertEqpAtivo)));
           }
        
    }
    
    mysql_query('commit');
    
		$empresa = $_SESSION["empresa_principal"];
		enviarEmail($idPaciente, 3, $empresa);

		//enviar_email_relatorio($idPaciente, 3, $empresa);
    echo 1;
    
}

function enviarEmail($paciente, $tipo, $empresa)
{
	$api_data = (object) Config::get('sendgrid');

	$gateway = new MailAdapter(
		new SendGridGateway(
			new \SendGrid($api_data->user, $api_data->pass),
			new \SendGrid\Email()
		)
	);
	$gateway->adapter->setSendData($paciente, 'enfermagem', $tipo, $empresa);
	$gateway->adapter->sendMail();
	enviarNotificacaoHipchat($gateway);
	return $gateway;
}

function enviarNotificacaoHipchat($gateway)
{
    $env = Config::get('APP_ENV');
    $token = $env == 'prd' ? Config::get('TOKEN_HIPCHAT') : Config::get('TOKEN_HIPCHAT_DEBUG');
	$hip = new \App\Controllers\HipChatSismederi($token, $env);
	$mensagem = utf8_decode($gateway->adapter->mail->html);
	$gateway->sendNotificationHipChat($hip, $mensagem);
}

function enviarEmailSimples($titulo, $texto, $para)
{
    $api_data = (object) Config::get('sendgrid');

    $gateway = new MailAdapter(
        new SendGridGateway(
            new \SendGrid($api_data->user, $api_data->pass),
            new \SendGrid\Email()
        )
    );

    $gateway->adapter->sendSimpleMail($titulo, $texto, $para);
}

switch ($_POST['query']) {
    case "del-item-kit":
        del_item_kit($_POST);
        break;
    case "novo-item-kit":
        novo_item_kit($_POST);
        break;
    case "lista-itens-kit":
        lista_itens_kit($_POST);
        break;
    case "novo-kit":
        novo_kit($_POST['kit']);
        break;
    case "remover-kit":
        remover_kit($_POST['kit']);
        break;
    case "opcoes-kits":
        opcoes_kits($_POST['like']);
        break;
    case "lista-historico-kit":
        lista_historico_kit($_POST);
        break;
    case "busca-itens":
        busca_itens($_POST['like'], $_POST['tipo']);
        break;
    case "opcoes-itens":
        opcoes($_POST['like']);
        break;
    case "prescricoes-antigas":
        prescricoes_antigas($_POST['p']);
        break;
    case "salvar-solicitacao":
        salvar_solicitacao($_POST);
        break;
    case "combo-busca-saida":
        combo_busca_saida($_POST['tipo']);
        break;
    case "busca-saida":
        buscar_saida($_POST);
        break;
    case "refresh-lock":
        refresh_lock($_POST);
        break;
    case "equipamento-ativo-update":
        equipamento_ativo_update($_POST);
        break;
    case "release-lock":
        release_lock($_POST);
        break;
    case "atualiza-cod-ref-kit":
        atualiza_cod_ref_kit($_POST['catalogo_id'], $_POST['idkit']);
        break;
    case "buscar-prescricoes":
        buscar_prescricoes($_POST);
        break;
    case "desativar-prescricao":
        desativar_prescricao($_POST);
        break;
    case "buscar-ficha-avaliacao-med":
        buscar_ficha_avaliacao_med($_POST['paciente']);
        break;
    case "buscar-ficha-evolucao-med":
        buscar_ficha_evolucao_med($_POST['paciente2']);
        break;
    case "ficha-avaliacao":
        ficha_avaliacao_enfermagem($_POST);
        break;
    case "editar-ficha-avaliacao":
        editar_ficha_avaliacao_enfermagem($_POST);
        break;
    case "ficha-evolucao-enfermagem":
        ficha_evolucao_enfermagem($_POST);
        break;
    case "buscar-avulsa":
        busca_avulsa($_POST);
        break;
    case "buscar_checklist":
        busca_checklist($_POST);
        break;
    case "salvar_checklist":
        salva_checklist($_POST);
        break;
    case "buscar_movimentacao":
        busca_movimentacao($_POST);
        break;
    case "buscar_estoque":
        busca_estoque($_POST);
        break;
    case "relatorio_alta":
        relatorio_alta($_POST);
        break;
    case "editar_relatorio_alta":
        editar_relatorio_alta($_POST);
        break;
    case "relatorio_aditivo":
        relatorio_aditivo($_POST);
        break;
    case "editar_relatorio_aditivo":
        editar_relatorio_aditivo($_POST);
        break;
    case "relatorio_prorrogacao":
        relatorio_prorrogacao($_POST);
        break;
    case "editar_relatorio_prorrogacao":
        editar_relatorio_prorrogacao($_POST);
        break;
    case "relatorio_visita":
        relatorio_visita($_POST);
        break;
    case "editar_relatorio_visita":
        editar_relatorio_visita($_POST);
      break;
    case "relatorio_deflagrado":
        relatorio_deflagrado($_POST);
        break;
    case "editar_relatorio_deflagrado":
        editar_relatorio_deflagrado($_POST);
      break;
    case "consultar-itens-kit":
        consultar_itens_kit($_POST);
        break;
    case "confirmar-relatorio-aditivo":
        confirmarRelatorioAditivo($_POST);
        break;
}

switch ($_GET['query']) {
    case "preview-prescricao":
        echo previewPrescricao($_GET['id'], $_GET['tipo'],$_GET['pendente'], $_GET['carater']);
        break;
    case "buscar-prescricoes-ativas":
        echo buscarPrescricoesAtivas($_GET['paciente']);
        break;
}
?>
