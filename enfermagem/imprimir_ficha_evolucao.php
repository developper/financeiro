<?php
$id = session_id();
if(empty($id))
	session_start();
include_once('../validar.php');

include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
require __DIR__ . '/../vendor/autoload.php';

use \App\Controllers\Administracao as Administracao;
use \App\Models\Administracao\Filial;

class imprimir_ficha_evolucao{

	public function cabecalho($dados, $empresa = null, $convenio = null){

		$id = $dados;
		$condp1= "c.idClientes = '{$id}'";
		$sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresa,
		c.`nascimento`,
		p.nome as Convenio,p.id,
		NUM_MATRICULA_CONVENIO,
		cpf
		FROM
		clientes AS c LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN
		planosdesaude as p ON (p.id = c.convenio)
		WHERE
		{$condp1}
		ORDER BY
		c.nome DESC LIMIT 1";

		$result = mysql_query($sql);
		$result2 = mysql_query($sql2);
		$html .= "<table width=100% >";
		while($pessoa = mysql_fetch_array($result2)){
			foreach($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);
			}
            $pessoa['empresa'] = $empresa;
            $pessoa['Convenio'] = !empty($convenio) ? $convenio : $pessoa['Convenio'];
			$html .= "<br/><tr>";
			$html .= "<td colspan='2'><label style='font-size:14px;color:#8B0000'><b></b></label></td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>PACIENTE </b></label></td>";
			$html .= "<td><label><b>SEXO </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>{$pessoa['paciente']}</td>";
			$html .= "<td>{$pessoa['sexo']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>IDADE </b></label></td>";
			$html .= "<td><label><b>UNIDADE REGIONAL </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>".join("/",array_reverse(explode("-",$pessoa['nascimento']))).' ('.$pessoa['idade']." anos)</td>";
			$html .= "<td>{$pessoa['empresa']}</td>";
			$html .= "</tr>";

			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>";
			$html .= "<td><label><b>MATR&Iacute;CULA </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>".strtoupper($pessoa['Convenio'])."</td>";
			$html .= "<td>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
			$html .= "</tr>";
			$html.= " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";

		}
		$html .= "</table>";

		return $html;
	}

	public function detalhes_ficha_evolucao_medica($dados){

		$id = $dados;


		$sql = "SELECT
  	DATE_FORMAT(fev.DATA,'%d/%m/%Y %H:%i:%s') as fdata,
  	fev.ID,
  	fev.PACIENTE_ID as idpaciente,
  	u.nome,
  	u.conselho_regional,
    u.tipo as tipo_usuario
  	p.nome as paciente,
  	empresas.nome AS empresa_rel
  	planosdesaude.nome AS convenio_rel
  	FROM
  	fichamedicaevolucao as fev LEFT JOIN
  	usuarios as u ON (fev.USUARIO_ID = u.idUsuarios) INNER JOIN
  	clientes as p ON (fev.PACIENTE_ID = p.idClientes)
    LEFT JOIN empresas ON fev.empresa = empresas.id
    LEFT JOIN planosdesaude ON fev.plano = planosdesaude.id
  	WHERE
  	fev.ID = '{$id}'";
		$r = mysql_query($sql);

		while($row = mysql_fetch_array($r)){
            $compTipo = $row['conselho_regional'];

			$usuario = ucwords(mb_strtolower($row["nome"],'UTF-8')) . $compTipo;
			$data = $row['fdata'];
			$paciente=$row['idpaciente'];
		}

        $responseEmpresa = Filial::getEmpresaById($empresa);

		$html .= "<h1><a><img src='../utils/logos/{$responseEmpresa['logo']}' width='200' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> Ficha de Evolu&ccedil;&atilde;o M&eacute;dica  </h1>";


		$html .=$this->cabecalho($paciente, $responseEmpresa['nome']);
		$html .= "<center><h2>Respons&aacute;vel: {$usuario} - Data: {$data}</h2></center>";

		$html .= "<form method='post' target='_blank'>";

		$html .= "<table width=100%>";
		$html .= "<tr  bgcolor='grey'>";
		$html .="<td colspan='3' >Problemas Ativos.</td>";
		$html .="</tr>";

		$sql2="select pae.DESCRICAO as nome, pae.CID_ID,(CASE pae.STATUS
  	WHEN '1' THEN 'RESOLVIDO'
  	WHEN '2' THEN 'MELHOR'
  	WHEN '3' THEN 'EST&Aacute;VEL'
  	WHEN '4' THEN 'PIOR' END) as sta , pae.STATUS,pae.OBSERVACAO from problemaativoevolucao as pae where pae.EVOLUCAO_ID = {$id} ";
		$result2 = mysql_query($sql2);
		$cont=1;

		while($row = mysql_fetch_array($result2)){
			if($cont%2 == 0){
				$cor = '#EEEEEE';
			}else{
				$cor ='#FFFFFF' ;
			}
			if($row['STATUS'] != 0){
				$html .= "<tr bgcolor='{$cor}'>";
				$html .="<td cid={$row['cid']} prob_atv='{$row['nome']}'  class='probativo1' style='width:20%'> <b>P{$cont} - {$row['nome']}</b> </td>";
				$html .="<td colspan='2'> <b>Estado:</b> {$row['sta']}</td>";
				$html .="</tr>";
				if( $row['STATUS'] != 1){
					$html .="<tr bgcolor='{$cor}'><td colspan='3'><b>OBSERVA&Ccedil;&Atilde;O: </b>{$row['OBSERVACAO']}</td></tr>";
				}
				$cont++;
			}
		}
		$html .= "<tr  bgcolor='grey'><td colspan='3' > Novos Problemas </td></tr>";
		$sql2="select pae.DESCRICAO as nome, pae.CID_ID, pae.STATUS,pae.OBSERVACAO from problemaativoevolucao as pae where pae.EVOLUCAO_ID = {$id} ";
		$result2 = mysql_query($sql2);
		$cont=1;
		while($row = mysql_fetch_array($result2)){
			if($cont%2 == 0){
				$cor = '#EEEEEE';
			}else{
				$cor ='#FFFFFF' ;
			}
			if($row['STATUS'] == 0){
				if($cont%2 == 0){
					$cor = '#EEEEEE';
				}else{
					$cor ='#FFFFFF' ;
				}
				$html .="<tr bgcolor='{$cor}'><td colspan=''><b>- {$row['nome']} </b><td colspan='2'> <b>OBS:</b> {$row['OBSERVACAO']}</td></tr>";
				$cont++;
			}
		}

		$sql= "select * from fichamedicaevolucao where ID = {$id}";
		$result=mysql_query($sql);
		while($row = mysql_fetch_array($result)){

			$pa_sistolica_min =$row['PA_SISTOLICA_MIN'];
			$pa_sistolica_max =$row['PA_SISTOLICA_MAX'];
			$pa_sistolica =$row['PA_SISTOLICA'];
			$pa_sistolica_sinal =$row['PA_SISTOLICA_SINAL'];
			$pa_diastolica_min = $row['PA_DIASTOLICA_MIN'];
			$pa_diastolica_max = $row['PA_DIASTOLICA_MAX'];
			$pa_diastolica  = $row['PA_DIASTOLICA'];
			$pa_diastolica_sinal = $row['PA_DIASTOLICA_SINAL'];
			$fc =$row["FC"];
			$fc_max =$row["FC_MAX"];
			$fc_min =$row["FC_MIN"];
			$fc_sinal =$row["FC_SINAL"];
			$fr =$row["FR"];
			$fr_max =$row["FR_MAX"];
			$fr_min =$row["FR_MIN"];
			$fr_sinal =$row["FR_SINAL"];
			$temperatura_max = $row["TEMPERATURA_MAX"];
			$temperatura_min = $row["TEMPERATURA_MIN"];
			$temperatura = $row["TEMPERATURA"];
			$temperatura_sinal = $row["TEMPERATURA_SINAL"];
			$estd_geral = $row["ESTADO_GERAL"];
			$mucosa = $row['MUCOSA'];
			$mucosaobs = $row['MUCOSA_OBS'];
			$escleras = $row['ESCLERAS'];
			$esclerasobs = $row['ESCLERAS_OBS'];
			$respiratorio = $row['PADRAO_RESPIRATORIO'];
			$respiratorioobs = $row['PADRAO_RESPIRATORIO_OBS'];
			$novo_exame = $row['NOVOS_EXAMES'];
			$impressao = $row['IMPRESSAO'];
			$diagnostico= $row['PLANO_DIAGNOSTICO'];
			$terapeutico= $row['PLANO_TERAPEUTICO'];
			$dor_sn =  $row['DOR'];
		}

		$html .= "<tr bgcolor='grey'>";
		$html .="<td  colspan='3' >Evolu&ccedil;&atilde;o dos sinais vitais (Registro da semana)</td>";
		$html .="</tr>";
		$html .= "<tr >";
		$html .="<td><b>PA (sist&oacute;lica) </b></td>";
		$html .="<td width=30%>min:<input type='texto' readonly class='numerico1 OBG' name='pa_sistolica_min' value='{$pa_sistolica_min}' readonly size='4' /> m&aacute;x:
  	<input type='texto' class='numerico1 OBG' name='pa_sistolica_max' value='{$pa_sistolica_max}' readonly  readonly  size='4' />";
		$html .="</td>";
		if($pa_sistolica_sinal == 1){
			$x="AMARELO";
		}

		if($pa_sistolica_sinal == 2){
			$x="VERMELHO";
		}
		$html .= "<td><b>ALERTA:</b> {$x} </td>";
		$html .= "</tr>";
		$html .= "<tr bgcolor='#EEEEEE'>";
		$html .="<td><b>PA (diast&oacute;lica) </b> </td>";
		$html .="<td> min:<input type='texto' class='numerico1 OBG' name='pa_diastolica_min' value='{$pa_diastolica_min}' readonly  size='4'  /> m&aacute;x:
  	<input type='texto' class='numerico1 OBG' name='pa_diastolica_max' value='{$pa_diastolica_max}' size='4' readonly />";
		$html .="</td>";
		$x='';
		if($pa_diastolica_sinal == 1){
			$x="AMARELO";
		}
		if($pa_diastolica_sinal == 2){
			$x="VERMELHO";
		}
		$html .= "<td><b>ALERTA:</b> {$x}</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .="<td><b>FC </b> </td>";
		$html .="<td> min:<input type='texto' class='numerico1 OBG' name='fc_min' value='{$fc_min}'  size='4' readonly /> m&aacute;x:
  	<input type='texto' class='numerico1 OBG' name='fc_max' value='{$fc_max}' readonly size='4' />";
		$html .="</td>";
		$x='';
		if($fc_sinal == 1){
			$x="AMARELO";
		}
		if($fc_sinal == 2){
			$x="VERMELHO";
		}
		$html .= "<td><b>ALERTA:</b> {$x}</td>";
		$html .= "</tr>";
		$html .= "<tr bgcolor='#EEEEEE'>";
		$html .="<td><b>Fr </b> </td>";
		$html .="<td>min:<input type='texto' class='numerico1 OBG' name='fr_min' value='{$fr_min}' size='4' readonly /> m&aacute;x:
  	<input type='texto' class='numerico1 OBG' name='fr_max' value='{$fr_max}' size='4'readonly />";
		$html .="</td>";
		$x='';
		if($fr_sinal == 1){
			$x="AMARELO";
		}
		if($fr_sinal == 2){
			$x="VERMELHO";
		}
		$html .= "<td><b>ALERTA:</b> {$x}</td>";
		$html .= "</tr>";
		$html .= "<tr >";
		$html .="<td width=20% ><b>Temperatura: </b></td>";
		$html .="<td width=30% >min:<input type='texto' class='numerico1 OBG' name='temperatura_min' size='4' value='{$temperatura_min}' readonly size='4' /> m&aacute;x:
  	<input type='texto' class='numerico1 OBG' name='temperatura_max' size='4'value='{$temperatura_max}' readonly size='4' />";
		$html .="</td>";
		$x='';
		if($ptemperatura_sinal == 1){
			$x="AMARELO";
		}
		if($temperatura_sinal == 2){
			$x="VERMELHO";
		}
		$html .= "<td ><b>ALERTA:</b> {$x}</td>";
		$html .= "</tr>";
		$html .= "<tr bgcolor='grey' ><td colspan='3' >Exame F&iacute;sico</td></tr>";
		$x='';
		$y1='';
		if($estd_geral == 1){
			$x="BOM";
		}
		if($estd_geral == 2){
			$x="REGULAR";
		}
		if($estd_geral == 3){
			$x="RUIM";
		}

		$html .= "<tr>";
		$html .="<td><b>Estado Geral:</b></td><td colspan='2'>{$x}</td>";
		$html .="</tr>";
		$x='';
		$y1='';
		if ($mucosa =='n'){
			$x= 'DESCORADAS';
			$y1= $mucosaobs;
		}
		if ($mucosa =='s'){
			$x= 'Coradas';
			$y1= $mucosaobs;
		}
		$html .= "<tr bgcolor='#EEEEEE'>";
		$html .= "<td><b>Mucosa:</b>  </td><td colspan='2'>{$x}  {$y1}</td>";
		$html .= "</tr>";
		$x='';
		if ($escleras =='n'){
			$x= 'Ict&eacute;ricas';
			$y1= $esclerasobs;
		}
		if ($escleras =='s'){
			$x= 'Anict&eacute;ricas';
			$y1= $esclerasobs;
		}
		$html .= "<tr>";
		$html .= "<td><b>Escleras:</b>  </td><td colspan='2'>{$x}  {$y1}</td>";
		$html .= "</tr>";
		$x='';
		$y1='';
		if ($respiratorio =='n'){
			$x= 'Taqui/Dispn&eacute;ico';
			$y1= $respiratorioobs;
		}
		if ($respiratorio =='s'){
			$x= 'Eupn&eacute;ico';
			$y1= $respiratorioobs;
		}
		$html .= "<tr bgcolor='#EEEEEE'><td><b>Padr&atilde;o Respirat&oacute;:</b>  </td><td colspan='2'>{$x}  {$y1}</td></tr>";
		$html .= "<tr>";
		$html .= "<td colspan='3'><b>PA (sist&oacute;lica):</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' class='numerico1 OBG' size='6' name='pa_sistolica' value='{$pa_sistolica}'  readonly {$acesso} />mmhg</td>";
		$html .= "</tr>";
		$html .= "<tr bgcolor='#EEEEEE'>";
		$html .= "<td colspan='3'><b>PA (diast&oacute;lica):</b>&nbsp;&nbsp;&nbsp;<input type='texto' class='numerico1 OBG' size='6' name='pa_diastolica' value='{$pa_diastolica}' readonly {$acesso} />mmhg</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td colspan='3'><b>FC: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' name='fc' readonly value='{$fc}' {$acesso} />bpm</td>";
		$html .= "</tr>";
		$html .= "<tr bgcolor='#EEEEEE'>";
		$html .= "<td colspan='3'><b>FR:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' readonly name='fr' value='{$fr}' {$acesso} />irpm</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td colspan='3'><b>Temperatura:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' name='temperatura' value='{$temperatura}' readonly {$acesso} />&deg;C</td>";
		$html .= "</tr>";
		if($dor_sn == 's'){
			$dor_sn = 'Sim';
		}
		if($dor_sn == 'n'){
			$dor_sn = 'N&atilde;o';
		}

		$html .="<tr bgcolor='#EEEEEE'><td colspan='3' ><b>DOR:</b> {$dor_sn}</td></tr>";

		$html .= "<tr><td colspan='3' ><table id='dor' style='width:100%;'>";
		$sql = "select * from dorfichaevolucao where FICHAMEDICAEVOLUCAO_ID = {$id} ";
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result)){
			$html .= "<tr><td> <b>Local:</b> {$row['DESCRICAO']}&nbsp;&nbsp; <b>Escala visual:</b> {$row['ESCALA']}&nbsp;&nbsp;<b>Padr&atilde;o:</b>{$row['PADRAO']}&nbsp;&nbsp; </td></tr>";
		}

		$html .= "</table></td></tr>";

		$html .= "<tr bgcolor='grey' ><td colspan='3' >CATETERS e SONDAS </td></tr>";
		$sql = "select DESCRICAO from catetersondaevolucao where FICHA_EVOLUCAO_ID = {$id}";
		$result= mysql_query($sql);
		while ($row= mysql_fetch_array($result)){
			$html .= "<tr><td colspan='3' ><b>- {$row['DESCRICAO']}</b></td></tr>";
		}

		$html .= "<tr bgcolor='grey' ><td colspan='3' >Exame segmentar</td></tr>";
		$sql="select exv.*,(CASE exv.AVALIACAO
  	WHEN '1' THEN 'NORMAL'
  	WHEN '2' THEN 'ALTERADO'
  	WHEN '3' THEN 'N EXAMINADO' END) as av,ex.DESCRICAO from examesegmentarevolucao as exv LEFT JOIN examesegmentar as ex ON (exv.EXAME_SEGMENTAR_ID = ex.ID) where exv.FICHA_EVOLUCAO_ID = {$id}";
		$result=mysql_query($sql);
		$cont1=1;
		while($row = mysql_fetch_array($result)){
			if($cont1++%2 == 0){
				$cor ='#EEEEEE';
			}else{
				$cor ='#FFFFFF' ;
			}

			$html .= "<tr bgcolor='{$cor}'>";
			$html .= "<td idexame={$row['ID']} class='exame1'> <b>{$row['DESCRICAO']}</b></td>";
			$html .= "<td colspan='2'>{$row['av']}</td>";
			$html .= "</tr>";
			if($row['AVALIACAO'] == 2 || $row['AVALIACAO'] == 3){
				$html .="<tr bgcolor='{$cor}'><td colspan='3'><b>OBS: </b>{$row['OBSERVACAO']}</td></tr>";
			}
		}
		$html .= "<tr bgcolor='grey'> <td colspan='3' ><b>Exames Complementares Novos</b></td></tr>";
		$html .= "<tr><td colspan='3' >{$novo_exame} </td></tr>";

		$html .= "<tr bgcolor='grey' ><td colspan='3' ><b>Impress&atilde;o</b></td></tr>";
		$html .= "<tr ><td colspan='3' >{$impressao} </td></tr>";

		$html .= "<tr bgcolor='grey' ><td colspan='3' ><b>Plano Diagn&otilde;stico</b></td></tr>";
		$html .= "<tr><td colspan='3' >{$diagnostico} </td></tr>";

		$html .= "<tr bgcolor='grey' ><td colspan='3' ><b>Plano Teraup&ecirc;utico</b></td></tr>";
		$html .= "<tr><td colspan='3' >{$terapeutico}</td></tr>";

		$html .= "</table>";
		$paginas []= $header.$html.= "</form>";

		//print_r($paginas);

		$mpdf=new mPDF('pt','A4',9);
		$mpdf->SetHeader('página {PAGENO} de {nbpg}');
		$ano = date("Y");
        $mpdf->SetFooter(strcode2utf("{$responseEmpresa['razao_social']}"." &#174; CopyRight &#169; {$responseEmpresa['ano_criacao']} - {DATE Y}"));
		$mpdf->WriteHTML("<html><body>");
		$flag = false;
		foreach($paginas as $pag){
			if($flag) $mpdf->WriteHTML("<formfeed>");
			$mpdf->WriteHTML($pag);
			$flag = true;
		}
		$mpdf->WriteHTML("</body></html>");
		$mpdf->Output('ficha_evolucao.pdf','I');
		exit;
	}
}
$p = new imprimir_ficha_evolucao();
$p->detalhes_ficha_evolucao_medica($_GET['id'], $_GET['empresa']);





?>