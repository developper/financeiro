<?php

//http://sistema.mederi.dev/enfermagem/upload_imagem.php?bucket=splendid-window-88321&file=Teste/pomba%20da%20paz.jpg

require __DIR__ . '/../vendor/autoload.php';

$client_email = '381781926511-sqe6cajrfcgtes3p5b4n079miar807qf@developer.gserviceaccount.com';
$private_key = file_get_contents('../../MyFirstProject-492e0a65cd88.p12');
$scopes = array(Google_Service_Storage::DEVSTORAGE_READ_WRITE);

$credentials = new Google_Auth_AssertionCredentials(
  $client_email,
  $scopes,
  $private_key
);

$client = new Google_Client();
$client->setAssertionCredentials($credentials);
if ($client->getAuth()->isAccessTokenExpired())
  $client->getAuth()->refreshTokenWithAssertion();

$accessToken = json_decode($client->getAccessToken(), true)['access_token'];
$params = ['access_token' => $accessToken];

$storage = new Google_Service_Storage($client);

$bucketName = $_GET['bucket'];
$objectName = $_GET['file'];

$file = $storage->objects->get($bucketName, $objectName);

$link = sprintf('%s&%s', $file->getMediaLink(), http_build_query($params));
$name = $file->getName();

?>

<img src="<?= $link ?>" alt="<?= $name ?>"/>
