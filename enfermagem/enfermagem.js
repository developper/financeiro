$(function($) {

    $(".chosen-select-ferida").chosen({search_contains: true});

    function getImagemFeridas(feridaDiv) {
      var feridas = {};
      var imageInputsFerida = $(feridaDiv).find('input.images-ferida');

      feridas.imagesFeridas = [];
      for (var n = 0, imageInputFerida; imageInputFerida = imageInputsFerida[n]; ++n) {
        if (imageInputFerida.dataset.images) {
            var dataImage ='';
            if(imageInputFerida.dataset.date != '')
            dataImage ='date'+imageInputFerida.dataset.date
            feridas.imagesFeridas.push(imageInputFerida.dataset.images+dataImage);

        }
      }
      feridas.observacao_imagem = $(feridaDiv).find('textarea.observacao_imagem').val() || -1;
      feridas.imagesFeridas = feridas.imagesFeridas.join(',');

      return feridas;
    }

    function ExitPageConfirmer(message) {
        this.message = message;
        this.needToConfirm = false;
        var myself = this;
        window.onbeforeunload = function() {
            if (myself.needToConfirm) {
                return myself.message;
            }
        }
    }/*
     * $("#tt").click(function(){ alert('fgdfgdfgdfgfdgdfgdfgdf'); });
     */

    //atualiza��o jeferson 23-03-2013
    $(".hora").mask("99:99");

    $(".desmarcar-radio-button").live('click', function(){
        var $this = $(this);
        $this.parent().find('input[type="radio"]').attr('checked', false);
        $(".dieta-extra-button").attr('disabled', true).val("");
    });

    $(".desmarcar-radio-sonda").live('click', function(){
        var $this = $(this);
        $this.parent().find('input[type="radio"]').attr('checked', false);
        $(".dieta-extra-sonda").attr('disabled', true).val("");
    });
    
    $("#data-avulsa, .data-avulsa").datepicker({
        minDate: 1,
        changeMonth: true,
        numberOfMonths: 1
    });
    $("#dataEntregaImediata,.dataEntregaImediata").datepicker({
        minDate: 0,
        maxDate: "+1d",
        changeMonth: true,
        numberOfMonths: 1
    });

    $("#hora-aditiva").blur(function() {
        var hora = $(this).val();
        var data = $("#data-aditiva").val();

        var hora_atual = new Date;
        var data_atual = new Date;

        var teste_data = data_atual.getDate() + '/' + (data_atual.getMonth() + 1) + '/' + data_atual.getFullYear();
        var hora_agora = hora_atual.getHours() + ':' + hora_atual.getMinutes();

        teste_data = teste_data.split("/").join("");
        data = data.split("/").join("");
        hora_agora = hora_agora.split(":").join("");
        hora = hora.split(":").join("");


        if ((parseInt(teste_data) == parseInt(data)) && (parseInt(hora) < parseInt(hora_agora))) {
            alert("Seu pedido não pode ser para um horário anterior ao atual.");
            $(this).val("");
        } else if ((parseInt(hora) < 700) || (parseInt(hora) > 1900)) {
            alert("A hora selecionada está fora do horário comercial");
            $(this).val("");
        }
        else if ((parseInt(teste_data) == parseInt(data)) && ((1900 - parseInt(hora_agora)) < 100)) {
            alert("Seu pedido não poderá ser entregue hoje. Favor amplie o prazo");
            $(this).val("");
        }

        else if ((parseInt(teste_data) == parseInt(data)) && (1900 - parseInt(hora)) < 100) {
            alert("O prazo mínimo para entregas no mesmo dia é de 1 hora");
            $(this).val("");
        }

    });
   
    //fim atualiza��o jeferson 23-03-2013


    $("#teste").click(function() {
        $.post("query.php", {
            query: "atualiza-cod-ref-kit",
            cod_tiss: cod,
            idkit: kit
        }, function(retorno) {
            alert("Atualizado com Sucesso!");
        });
    });

    var exitPage = new ExitPageConfirmer(
            'Deseja realmente sair dessa página? os dados não salvos serão perdidos!');

    $('.numerico').keyup(function(e) {
        this.value = this.value.replace(/\D/g, '');
    });

    // $( "button, input:submit").button();

    $("input[name='nome']").attr("readonly", "readonly");
    $("input[name='apr']").attr("readonly", "readonly");
    $(".data").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1
    });
    $(".hora").timeEntry({
        show24Hours: true,
        defaultTime: '08:00'
    });
    $(".tabela-kits,.tabela-med,.tabela-mat,.tabela-serv,.tabela-die").hide();
    $("#tipos").buttonset();

    var dates = $("#inicio, #fim")
            .datepicker(
            {
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3,
                onSelect: function(selectedDate) {
                    var option = this.id == "inicio" ? "minDate"
                            : "maxDate", instance = $(this).data(
                            "datepicker");
                    date = $.datepicker
                            .parseDate(
                            instance.settings.dateFormat
                            || $.datepicker._defaults.dateFormat,
                            selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                }
            });

    $("input[name='antigas']").change(
            function() {
                $("input[name='paciente']").val(
                        $(this).parent().parent().attr('paciente'));
                $("input[name='label']").val(
                        $(this).parent().parent().attr('label'));
            });
    //atualizado 21-11-2013
    $("#iniciar-avulsa").hide();
    $("select[name='pacientes']").change(function() {
        var p = $("select[name='pacientes'] option:selected").val();
        var status = $("select[name='pacientes'] option:selected").attr('status');
        if(p != -1 && status == 4){
            $("#iniciar-avulsa").show();
        }else{
            $("#iniciar-avulsa").hide();
        }
    });
        
    $("#iniciar-avulsa").click(function() {
        if (!validar_campos("div-iniciar-avulsa")) {
            return false;
        }

    });
    //fim atualiza��o Eriton 14-05-2013

    //Atualização Eriton 06-08-2013 Score//////////////
    $(".limpar").click(function() {

        $(this).parent().children('input').attr('checked', false);
        var score = 0;
        var cont = 0;
        var texto = '';
        var j = 0;

        $("input[class='abmid']:checked").each(function() {

            if ($(this).val() == 5) {
                cont++;
            }
            score = score + parseInt($(this).val());

            j = score;

            if (j <= 7 && cont == 0) {
                texto = "Score do paciente segundo a tabela ABEMID = " + j + ".";
            } else if (j <= 7 && cont == 1) {
                texto = "Score do paciente segundo a tabela ABEMID = " + j + ".";
            } else if (j >= 8 && j <= 12 && cont == 0) {
                texto = "Score do paciente segundo a tabela ABEMID = " + j + ".";
            } else if (j >= 8 && j <= 12 && cont == 1) {
                texto = "Score do paciente segundo a tabela ABEMID = " + j + ".";
            } else if (j >= 8 && j <= 12 && cont == 2) {
                texto = "Score do paciente segundo a tabela ABEMID = " + j + ".";
            } else if (j >= 13 && j <= 18 && cont < 2) {
                texto = "Score do paciente segundo a tabela ABEMID = " + j + ".";
            } else if (j >= 13 && j <= 18 && cont >= 2) {
                texto = "Score do paciente segundo a tabela ABEMID = " + j + ".";
            } else if (j >= 19) {
                texto = "Score do paciente segundo a tabela ABEMID = " + j + ".";
            }
        });



        $("input[name='sabmid']").val(score.toString());
        $("input[name='sabmid1']").val(cont.toString());
        $("#prejustparecer").val(texto.toString());

    });


    $(".limpar2").click(function() {

        $(this).parent().children('input').attr('checked', false);
        var score = 0;
        var cont = 0;
        var texto = '';
        var j = 0;
        var x = 0;
        $("input[class='petro']:checked").each(function(i) {

            if ($(this).val() == 5) {
                cont++;
            }
            if ($("input[name='supvent']:checked").val() == 5) {
                x = 1;
            }

            score = score + parseInt($(this).val());

            j = score;

            if (j <= 7 && cont == 0 && x != 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ".";
            }
            else if (j <= 7 && cont == 0 && x == 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ".";
            }
            else if (j <= 7 && cont == 1 && x != 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". ";
            }
            else if (j <= 7 && cont == 1 && x == 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". ";
            }
            else if (j >= 8 && j <= 12 && cont == 0 && x != 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ".";
            }
            else if (j >= 8 && j <= 12 && cont == 0 && x == 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". ";
            }
            else if (j >= 8 && j <= 12 && cont == 1 && x != 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ".";
            }
            else if (j >= 8 && j <= 12 && cont == 1 && x == 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". ";
            }
            else if (j >= 8 && j <= 12 && cont == 2 && x != 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ".";
            }
            else if (j >= 8 && j <= 12 && cont == 2 && x == 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ".";
            }
            else if (j >= 13 && j <= 18 && cont == 0 && x != 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ".";
            }
            else if (j >= 13 && j <= 18 && cont == 0 && x == 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ".";
            }
            else if (j >= 13 && j <= 18 && cont >= 2 && x != 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ".";
            }
            else if (j >= 13 && j <= 18 && cont >= 2 && x == 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ".";
            }
            else if (j >= 19 && x != 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ".";
            }

        });
        $("input[name='spetro']").val(score.toString());
        $("input[name='spetro1']").val(cont.toString());

        $("#prejustparecer").val(texto.toString());

    });

    $(".nead").click(function () {

        var score_grupo1 = 0;
        var score_grupo2 = 0;
        var score_grupo3 = 0;
        var total_score_nead = 0;
        var justificativa = '';
        var val_score = '';
        $("input.nead:checked").each(function () {
            if($(this).attr('fator') == 1) {
                score_grupo1 = score_grupo1 + (parseInt($(this).val()) * 1);
            } else if($(this).attr('fator') == 2) {
                score_grupo2 = score_grupo2 + (parseInt($(this).val()) * 2);
            } else if($(this).attr('fator') == 3) {
                score_grupo3 = score_grupo3 + (parseInt($(this).val()) * 3);
            }


        });
        total_score_nead = score_grupo1 + score_grupo2 + score_grupo3;
        if(total_score_nead < 8) {
            justificativa = "Sem indicação para Internação Domiciliar.";
        } else if(total_score_nead >= 8 && total_score_nead <= 15) {
            justificativa = "Internação Domiciliar com visita de enfermagem.";
        } else if(total_score_nead >= 16 && total_score_nead <= 20) {
            justificativa = "Internação Domiciliar com até 6 horas de enfermagem.";
        } else if(total_score_nead >= 21 && total_score_nead <= 30) {
            justificativa = "Internação Domiciliar com até 12 horas de enfermagem.";
        } else if(total_score_nead > 30) {
            justificativa = "Internação Domiciliar com até 24 horas de enfermagem.";
        }
        val_score = "Score do paciente segundo tabela NEAD=" + total_score_nead + ". " + justificativa

        $("#grupo1").val(score_grupo1);
        $("#grupo2").val(score_grupo2);
        $("#grupo3").val(score_grupo3);
        $("#total_score_nead").val(total_score_nead);
        $("#justificativa_nead").text(val_score);
        var observacao_outro_score = $(".observacao_score").text() + " " + val_score;
        $("#prejustparecer").val(observacao_outro_score);

    });

    $('.limpar_nead').click(function () {

        $(this).parent().children('input').attr('checked', false);
        var score_grupo1 = 0;
        var score_grupo2 = 0;
        var score_grupo3 = 0;
        var total_score_nead = 0;
        var justificativa = '';
        var val_score = '';
        var cont = 0;
        $("input.nead:checked").each(function () {
            cont++;
            if($(this).attr('fator') == 1) {
                score_grupo1 = score_grupo1 + (parseInt($(this).val()) * 1);
            } else if($(this).attr('fator') == 2) {
                score_grupo2 = score_grupo2 + (parseInt($(this).val()) * 2);
            } else if($(this).attr('fator') == 3) {
                score_grupo3 = score_grupo3 + (parseInt($(this).val()) * 3);
            }

        });
        total_score_nead = score_grupo1 + score_grupo2 + score_grupo3;
        if(total_score_nead < 8) {
            justificativa = "Sem indicação para Internação Domiciliar.";
        } else if(total_score_nead >= 8 && total_score_nead <= 15) {
            justificativa = "Internação Domiciliar com visita de enfermagem.";
        } else if(total_score_nead >= 16 && total_score_nead <= 20) {
            justificativa = "Internação Domiciliar com até 6 horas de enfermagem.";
        } else if(total_score_nead >= 21 && total_score_nead <= 30) {
            justificativa = "Internação Domiciliar com até 12 horas de enfermagem.";
        } else if(total_score_nead > 30) {
            justificativa = "Internação Domiciliar com até 24 horas de enfermagem.";
        }
        if(cont != 0) {
            val_score = "Score do paciente segundo tabela NEAD=" + total_score_nead + ". " + justificativa
        }
        $("#grupo1").val(score_grupo1);
        $("#grupo2").val(score_grupo2);
        $("#grupo3").val(score_grupo3);
        $("#total_score_nead").val(total_score_nead);
        $("#justificativa_nead").text(val_score);
        var observacao_outro_score = $(".observacao_score").text() + " " + val_score;
        $("#prejustparecer").val(observacao_outro_score);


    });

    function calcularScoreAbmid()
    {
        var score = 0;
        var cont = 0;
        var texto = '';
        var j = 0;

        $("input[class='abmid']:checked").each(function(i) {
            if ($(this).val() == 5) {
                cont++;
            }

            score = score + parseInt($(this).val());

            j = score;

            if (j <= 7 && cont == 0) {
                texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente não elegível para Internação Domiciliar.";
            } else if (j <= 7 && cont == 1) {
                texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de média Complexidade.";
            } else if (j >= 8 && j <= 12 && cont == 0) {
                texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Baixa Complexidade.";
            } else if (j >= 8 && j <= 12 && cont == 1) {
                texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de média Complexidade.";
            } else if (j >= 8 && j <= 12 && cont == 2) {
                texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Alta Complexidade.";
            } else if (j >= 13 && j <= 18 && cont < 2) {
                texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de média Complexidade.";
            } else if (j >= 13 && j <= 18 && cont >= 2) {
                texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Alta Complexidade.";
            } else if (j >= 19) {
                texto = "Score do paciente segundo a tabela ABEMID = " + j + ". Paciente de Alta Complexidade.";
            }
        });

        $(".observacao_score").text("  " + texto);

        $("input[name='sabmid']").val(score.toString());
        $("input[name='sabmid1']").val(cont.toString());
        texto = texto + " " + $("#justificativa_nead").text();
        $("#prejustparecer").val(texto.toString());
    }

    $(".abmid").click(function() {
        calcularScoreAbmid();
    });

    $("#voltar").click(function(event) {
        event.preventDefault();
        history.back(1);
    });
    
    $("#voltar_").click(function(event) {
        event.preventDefault();
        history.back(1);
    });

    function calcularScorePetrobras()
    {
        var score = 0;
        var cont = 0;
        var j = 0;
        var x = 0;
        var texto = "";

        $("input[class='petro']:checked").each(function(i) {

            if ($(this).val() == 5) {
                cont++;
            }

            if ($("input[name='supvent']:checked").val() == 5) {
                x = 1;

            }
            //Ordem de prioridade:
            // VentilaÃ§Ã£o mecÃ¢nica, score e pontuaÃ§Ã£o. (Ana Paula)

            score = score + parseInt($(this).val());

            j = score;

            if (j <= 7 && cont == 0 && x != 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente não elegível para Internação Domiciliar.";
            }
            else if (j <= 7 && cont == 0 && x == 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
            }
            else if (j <= 7 && cont == 1 && x != 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de média Complexidade.";
            }
            else if (j <= 7 && cont == 1 && x == 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
            }
            else if (j >= 8 && j <= 12 && cont == 0 && x != 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Baixa Complexidade.";
            }
            else if (j >= 8 && j <= 12 && cont == 0 && x == 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
            }
            else if (j >= 8 && j <= 12 && cont == 1 && x != 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de média Complexidade.";
            }
            else if (j >= 8 && j <= 12 && cont == 1 && x == 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
            }
            else if (j >= 8 && j <= 12 && cont == 2 && x != 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ".  Paciente de Alta Complexidade.";
            }
            else if (j >= 8 && j <= 12 && cont == 2 && x == 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
            }
            else if (j >= 13 && j <= 18 && cont == 0 && x != 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de média Complexidade.";
            }
            else if (j >= 13 && j <= 18 && cont == 0 && x == 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
            }
            else if (j >= 13 && j <= 18 && cont >= 2 && x != 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade.";
            }
            else if (j >= 13 && j <= 18 && cont >= 2 && x == 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade por possuir ventilação mecânica.";
            }
            else if (j >= 19 && x != 1) {
                texto = "Score do paciente segundo a tabela Petrobras = " + j + ". Paciente de Alta Complexidade.";
            }

        });
        $("input[name='spetro']").val(score.toString());
        $("input[name='spetro1']").val(cont.toString());
        texto = texto + " " + $("#justificativa_nead").text();
        $("#prejustparecer").val(texto.toString());
    };
    
    $(".petro").click(function() {
        calcularScorePetrobras();
    });

    //Fim atualização Eriton 06-08-2013///////////
    
    $("#buscar_checklist").click(function(){
        var id = $(".checklist_paciente").val();
        $.post("query.php",{
            query: "buscar_checklist",
            id: id
        }, function(r) {
            $("#result").html(r);
            $("#result").append();
        });
        return false;
    });
    
    $("#salvar_checklist").live('click',function(){
        var total = $(".semana_checklist").size();
        var itens = "";
        var id = $(this).attr("id_paciente");
        $(".semana_checklist").each(function(i){            
            var tiss = $(this).parent().parent().children("td").eq(0).attr("cod");
            var qtd_semana = $(this).val();
            var tipo = $(this).attr("tipo");
            var linha = tiss + "#" + qtd_semana + "#" + tipo;
            if (i < total - 1){
                linha += "$";
            }
            itens += linha;
        });
        var dados = "query=salvar_checklist" + "&itens=" + itens + "&id=" + id;
       $.ajax({
                type: "POST",
                url: "query.php",
                data: dados,
                success: function(msg) {

                    if (msg == 1) {
                        alert("Checklist atualizado com sucesso.");                        
                        return false;
                    } else {
                        alert(msg);
                        return false;
                    }
                }

            });
    });

$("#buscar_estoque").live('click', function(){
        var id = $(".estoque_paciente").val();
        $.post("query.php",{
            query: "buscar_estoque",
            id: id
        }, function(r) {
            $("#result").html(r);
            $("#result").append();
        });
        return false;
    });
    
    $(".estoque_span").hide();
    $("#buscar_estoque").hide();
    $("#buscar_movimentacao").hide();
    $(".estoque").click(function(){
        if ($(this).val() == 2){
            $(".estoque_span").show();
            $("#buscar_movimentacao").show();
            $("#buscar_estoque").hide();
        }else{
            $(".estoque_span").hide();
            $("#buscar_movimentacao").hide();
            $("#buscar_estoque").show();
        }
    });
    
    $("#buscar_movimentacao").click(function(){
        var id = $(".estoque_paciente").val();
        var inicio = $("input[name='inicio_estoque']").val();
        var fim = $("input[name='fim_estoque']").val();        
        $.post("query.php",{
            query: "buscar_movimentacao",
            id: id,
            inicio: inicio,
            fim: fim
        }, function(r) {
            $("#result").html(r);
            $("#result").append();
        });
        return false;
    });
    
    $(".iniciar-solicitacao").click(function() {
        // alert($(".trPrescricao").attr('antiga'));
        $(".formPresc").submit(function() {

            // $.post("?view=solicitacoes", {antigas:
            // $(".trPrescricao").attr('antiga'), paciente:
            // $(".trPrescricao").attr('paciente'),label:$(".trPrescricao").attr('label')});
        });
    });

    $("select[name='pacientes']").change(
            function() {
                $("input[name='npaciente']").val(
                        $("select[name='pacientes'] option:selected").html());
            });

    function atualiza_qtd(id, op) {
        var item = "#" + id;
        var qtd = $(item).attr("qtd");
        if (op == "+")
            $(item).attr("qtd", parseInt(qtd) + 1);
        else
            $(item).attr("qtd", parseInt(qtd) - 1);
        if ($(item).attr("qtd") > 0) {
            $(item)
                    .find(".status")
                    .eq(0)
                    .html(
                    "<img src='../utils/processada_16x16.png' title='solicitada' />");
        } else {
            $(item).find(".status").eq(0).html("");
        }
    }

    function refreshlock() {
        $.post("query.php", {
            query: "refresh-lock",
            p: $("#solicitacao").attr("prescricao")
        });
    }

    setInterval(function(i) {
        refreshlock();
    }, 120000);

    $("input[name='tipo-busca-saida']").change(function() {
        $("#combo-busca-saida").html("");
        $.post("query.php", {
            query: "combo-busca-saida",
            tipo: $(this).val()
        }, function(retorno) {
            $("#combo-busca-saida").html(retorno);
        });
    });

    $("#tipos input[name='tipos']").change(function() {
        $("#formulario").attr('tipo', $(this).val());
    });

    $("#buscar-saida").click(function() {
        var tipo = $("input[name='tipo-busca-saida']:checked").val();
        var cod = $("select[name='codigo'] option:selected").val();
        var inicio = $("input[name='inicio']").val();
        var fim = $("input[name='fim']").val();
        var resultado = $("input[name='resultado']:checked").val();
        $.post("query.php", {
            query: "busca-saida",
            tipo: tipo,
            codigo: cod,
            inicio: inicio,
            fim: fim,
            resultado: resultado
        }, function(r) {
            $("#resultado-busca").html(r);
        });
        return false;
    });

    // /////////////equipamentos ativos//////////////

    $("#equip_ativo").click(function() {
        if ($(this).is(':checked')) {
            $("#span_equipamentos_ativos").show();
        } else {

            $("#span_equipamentos_ativos").hide();
        }
    });

    
    $(".acao-equipamento").change(function() {        
      var idEquipamento = this.dataset.idequipamento;
     
      if($(this).val() == -1){
          $("#qtd"+idEquipamento).val('');
          $("#qtd"+idEquipamento).attr('style','');
          $("#qtd"+idEquipamento).removeClass('OBG');
          $("#qtd"+idEquipamento).attr('readonly',true);
          $("#entrega-imediata"+idEquipamento).attr('disabled',true);
          $("#entrega-imediata"+idEquipamento).attr('checked',false);
          $("#observacao"+idEquipamento).attr('readonly',true);
          $("#observacao"+idEquipamento).val('');
          $("#observacao"+idEquipamento).removeClass('OBG');
          $("#observacao"+idEquipamento).attr('readonly',true);
      }else{
          $("#qtd"+idEquipamento).addClass('OBG');
          $("#qtd"+idEquipamento).attr('readonly',false); 
          $("#entrega-imediata"+idEquipamento).attr('disabled',false);
          $("#observacao"+idEquipamento).attr('readonly',false);
          $("#observacao"+idEquipamento).addClass('OBG');
          $("#observacao"+idEquipamento).attr('readonly',false);
          
      }    
       var classRemove=this.dataset.classRemove;
       $(this).removeClass(classRemove);
       var claseOption=$(this).children('option:selected').attr('class');
       $(this).addClass(claseOption);
       this.dataset.classRemove = claseOption;
      
     
        
       
    });
    // /jeferson 2012-23-10 fim

    $(".div-aviso").hide();

    function validar_campos(div) {
        $(".error").removeClass("error");
        var ok = true;        
        var elements = [];
        $("#" + div + " .OBG").each(function(i) {
            var tr = $(this).closest("tr");

            if (this.value == "") {
                ok = false;
                console.log($(this));

                tr.addClass("error");
                this.style.border = "3px solid red";
            } else {                
                this.style.border = "";
            }
        });
        $("#" + div + " .COMBO_OBG option:selected").each(function(i) {
            var tr = $(this).closest("tr");
            if (this.value == "-1") {
                ok = false;
                console.log($(this));
                tr.addClass("error");
                $(this).parent().css({
                    "border": "3px solid red"
                });
            } else {
                $(this).parent().css({
                    "border": ""
                });
            }
        });

        $("#"+div+" .OBG_CHOSEN option:selected").each(function (){
            if(this.value == "" || this.value == -1){
                ok = false;
                console.log($(this));
                $(this).parent().parent().children('div').css({"border":"3px solid red"});
            } else {
                var estilo = $(this).parent().parent().parent().children('div').attr('style');

                $(this).parent().parent().parent().children('div').removeAttr('style');
                $(this).parent().parent().parent().children('div').attr('style', estilo.replace("border: 3px solid red",''));
            }
        });

        $("#" + div + " .OBG_RADIO").each(function(i) {
            //if (this.value == "-1") {
            classe = $(this).attr("class").split(" ");
            classe = classe[0];
            var tr = $("." + classe).closest("tr");
            
            classe_aux = "";
            aux = 0;     

            if (classe != classe_aux) {
                classe_aux = classe;
                aux = 0;
                if ($("." + classe).is(':checked')) {
                    aux = 1;
                }

                if (aux == 0) {
                    ok = false;
                    console.log($(this));
                    tr.addClass("error");                   
                    tr.css("border", "3px solid red");
                } else if(classe !== "OBG_RADIO") {            
                    $("." + classe).closest("tr").css("border", "");
                }
            } else {
                if ($("." + classe).is(':checked')) {
                    aux = 1;
                }

                if (aux == 0) {
                    ok = false;
                    tr.addClass("error");  
                    tr.css("border", "3px solid red");

                } else if(classe !== "OBG_RADIO") {   
                    tr.css("border", "");
                }
            }
        });
        
        if (!ok) {           
            var firstElement = $(".error:first");   
            $("html, body").animate({
                scrollTop: (firstElement.offset().top-50)
            }, 1000);
            $("#" + div + " .aviso").text(
                    "Os campos em vermelho são obrigatórios!");
            $("#" + div + " .div-aviso").show();
        } else {
            $("#" + div + " .aviso").text("");
            $("#" + div + " .div-aviso").hide();
        }
        return ok;
    }

    $("input[name='busca']").autocomplete({
        source: function(request, response) {
            $.getJSON('busca_brasindice.php', {
                term: request.term,
                tipo: $("#formulario").attr("tipo"),
                somenteGenerico: $("#formulario").attr("tipo-medicamento")
            }, response);
        },
        minLength: 3,
        select: function(event, ui) {
            $('#buscaitem').val('');
            $('#nome').val(ui.item.value);
            $('#catalogo_id').val(ui.item.catalogo_id);
            $('#cod').val(ui.item.id);
            $('#qtd').focus();
            $('#buscaitem').val('');
            $("#categoria").val(ui.item.categoria);
            $("#valor-venda").val(ui.item.valor);
            return false;
        },
        extraParams: {
            tipo: 2
        }
    });

    $("input[name='nome-kit']").keyup(function() {
        if ($(this).val().length > 2) {
            $.post("query.php", {
                query: "opcoes-kits",
                like: $(this).val()
            }, function(lista) {
                $("#div-lista-kits").html(lista);
            });
        } else {
            $("#div-lista-kits").html("");
        }
        $(".edit-kit").click(function() {
            $("#dialog-kit").dialog("open");
        });
        $(".historico-kit").click(function() {
            $("#dialog-historico-kit").dialog("open");
        });
    });

    $("#criar-novo-kit").click(function() {
        var kit = $("input[name='nome-kit']").val();
        if (kit != "") {
            if (confirm("Deseja realmente criar o kit " + kit + "?")) {
                $.post("query.php", {
                    query: "novo-kit",
                    kit: kit
                }, function(r) {
                    if (isNaN(r))
                        alert(r);
                    else {
                        // alert(r+"122");
                        // alert('4566');
                        $('#h-nome-kit').text(kit);
                        $('#dialog-kit').attr('kit', r);
                        $('#dialog-kit').dialog('open');
                    }
                });
            }
        }
        return false;
    });
    
    $("#justificativa-item-avulso").live('change', function(){
        var paciente = $('#avulsa').attr('paciente');

        $("#just-prescricao").html('');

        if($(this).val() == '11'){  
            $('#just-outros').html("<textarea name='justificativa-outros' placeholder='Justifique aqui o motivo da solicitação desse item' id='justificativa-outros' cols='90' rows='4' class='OBG'></textarea>");
        }else{
            $('#just-outros').html('');
        }

        if($(this).val() == '2') {
            $.get(
                'query.php',
                {
                    query: 'buscar-prescricoes-ativas',
                    paciente: paciente
                },
                function(response) {
                    if(response != '0') {
                        response = JSON.parse(response);
                        var optText;
                        var selectPresc = '<select name="prescricoes_ativas" id="prescricoes-ativas" class="OBG">';
                        selectPresc += '<option value="">Selecione a prescrição a qual o item pertence...</option>';
                        $.each(response, function (index, value) {
                            optText = value.tipo + ' de ' + value.inicio + ' à ' + value.fim + '. Por: ' + value.criador;
                            selectPresc = selectPresc + '<option value="' + index + '">' + optText + '</option>'
                        });
                        selectPresc += '</select>';
                        $("#just-prescricao")
                            .html(
                                '<p>' +
                                ' <b>Prescrição: </b>' +
                                selectPresc +
                                '</p>'
                            );
                    } else {
                        alert('Esse paciente não possui prescrição períodica ativa!');
                        $("#justificativa-item-avulso").val('');
                    }
                }
            );
        }
    });
    
    $(".add-kit,.add-med,.add-mat,.add-die").click(function() {
        var item = $(this).attr("item");
        var tipo = $(this).attr("tipo");
        $("#kits-" + item).show();
        $("#med-" + item).show();
        $("#mat-" + item).show();
        $("#serv-" + item).show();
        $("#die-" + item).show();
        $("#formulario").attr("item", item);
        $("#formulario").attr("tipo", tipo);
        $("#b" + tipo).attr('checked', 'checked');
        $("#b" + tipo).button('refresh');
        $("#formulario").dialog("open");
    });

    $(".look-kit").toggle(function() {
        var item = $(this).attr("item");
        $("#kits-" + item).show();
        $("#med-" + item).show();
        $("#mat-" + item).show();
        $("#serv-" + item).show();
        $("#die-" + item).show();
    }, function() {
        var item = $(this).attr("item");
        $("#kits-" + item).hide();
        $("#med-" + item).hide();
        $("#mat-" + item).hide();
        $("#serv-" + item).hide();
        $("#die-" + item).hide();
    });

    $(".obs-enf").toggle(function(event) {

        var cod = $(this).attr("item");
        $("#obs-" + cod).show(500);
    }, function(event) {

        var cod = $(this).attr('item');

        $("#obs-" + cod).hide(500);
    });

    $("#formulario").dialog({
        autoOpen: false,
        modal: true,
        width: 800,
        position: 'top',
        open: function(event, ui) {
            $("input[name='busca']").val('');
            $('#nome').val('');
            $('#nome').attr('readonly', 'readonly');
            $('#apr').val('');
            $('#apr').attr('readonly', 'readonly');
            $('#cod').val('');
            $('#qtd').val('');
            $("input[name='busca']").focus();
        },
        buttons: {
            "Fechar": function() {
                var item = $("#formulario").attr("item");
                if (item != "avulsa") {
                    $("#kits-" + item).hide();
                    $("#med-" + item).hide();
                    $("#mat-" + item).hide();
                    $("#serv-" + item).hide();
                    $("#die-" + item).hide();
                }
                $(this).dialog("close");
            }
        }
    });

    $(".add-serv").click(function() {
        var item = $(this).attr("item");
        if (item != "avulsa") {
            $("#kits-" + item).show();
            $("#med-" + item).show();
            $("#mat-" + item).show();
            $("#serv-" + item).show();
            $("#die-" + item).show();
        }
        $("#dialog-servicos").attr("item", item);
        $("#dialog-servicos").dialog("open");
        return false;
    });

    $("#dialog-servicos").dialog({
        autoOpen: false,
        modal: true,
        width: 800,
        position: 'top',
        open: function(event, ui) {
            $("#qtd-serv").val("");
            $("#add-serv").attr("item", $(this).attr("item"));
        },
        buttons: {
            "Fechar": function() {
                var item = $("#dialog-servicos").attr("item");
                if (item != "avulsa") {
                    $("#kits-" + item).hide();
                    $("#med-" + item).hide();
                    $("#mat-" + item).hide();
                    $("#serv-" + item).hide();
                    $("#die-" + item).hide();
                }
                $(this).dialog("close");
            }
        }
    });

    $("#add-serv")
            .click(
            function() {
                if (validar_campos("dialog-servicos")) {
                    var cod = $(
                            "select[name='servicos'] option:selected")
                            .val();
                    var nome = $(
                            "select[name='servicos'] option:selected")
                            .text();
                    var qtd = $("#qtd-serv").val();
                    var tipo = "serv";
                    var item = $("#add-serv").attr("item");
                    var dados = "nome='" + nome + "' tipo='" + tipo
                            + "' cod='" + cod + "' qtd='" + qtd + "'";
                    var linha = nome + ". Quantidade: " + qtd;
                    var botoes = "<img align='right' class='rem-item-solicitacao' item='"
                            + item
                            + "' src='../utils/delete_16x16.png' title='Remover' border='0'>";
                    $("#" + tipo + "-" + item).append(
                            "<tr bgcolor='#ffffff' class='dados' "
                            + dados + " ><td>" + linha + botoes
                            + "</td></tr>");
                    atualiza_qtd(item, '+');
                    atualiza_rem_item();
                    $("#qtd-serv").val("");
                    $("select[name='servicos'] option").eq(0).attr(
                            "selected", "selected");
                    exitPage.needToConfirm = true;
                }
            });

    $("#dialog-kit").dialog({
        autoOpen: false,
        modal: true,
        width: 800,
        position: 'top',
        close: function(event, ui) {
            window.location = "?view=kits";
        },
        open: function(event, ui) {
            $("input[name='busca']").val('');
            $('#nome').val('');
            $('#nome').attr('readonly', 'readonly');
            $('#apr').val('');
            $('#apr').attr('readonly', 'readonly');
            $('#cod').val('');

            $.post("query.php", {
                query: 'lista-itens-kit',
                kit: $(this).attr('kit'),
                cod_ref: $(this).attr('cod')
            }, function(r) {
                $("#tabela-itens-kit").html(r);
            });
        },
        buttons: {
            "Fechar": function() {
                var item = $("#formulario").attr("item");
                $("#kits-" + item).hide();
                $(this).dialog("close");

            }
        }
    });

    $("#dialog-historico-kit").dialog({
        autoOpen: false,
        modal: true,
        width: 1000,
        position: 'top',
        open: function(event, ui) {
            $.post("query.php", {
                query: 'lista-historico-kit',
                kit: $(this).attr('kit')
            }, function(r) {
                $("#tabela-historico-kit").html(r);
            });
        },
        buttons: {
            "Fechar": function() {
                var item = $("#formulario").attr("item");
                $("#kits-" + item).hide();
                $(this).dialog("close");

            }
        }
    });

    $('.del-item-kit').live('click', function(){
        var $this = $(this);
        var item = $this.attr('item');
        $this
            .parents('tr')
            .next('.justificativa-exclusao-kit')
            .css('display', 'block')
            .html(
                '<td colspan="3" valign="middle">' +
                '   <b>Justificativa:</b><br>' +
                '   <textarea cols="75" rows="3" id="justificativa-' + item + '"></textarea><br>' +
                '   <button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover remover-item-kit" ' +
                '           item="' + item + '"' +
                '           role="button" ' +
                '           aria-disabled="false">' +
                '       <span class="ui-button-text">Excluir Item</span>' +
                '   </button> ' +
                '   <button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover cancelar-remover-item-kit" ' +
                '           item="' + item + '"' +
                '           role="button" ' +
                '           aria-disabled="false">' +
                '       <span class="ui-button-text">Cancelar</span>' +
                '   </button>' +
                '</td>'
            );
    });

    $('.cancelar-remover-item-kit').live('click', function(){
        var $this = $(this);
        $this
            .parents('tr')
            .css('display', 'none')
            .html('');
    });

    $('.remover-item-kit').live('click', function(){
        var $this = $(this);
        var item = $this.attr('item');
        var $tr = $this.parents('tr');
        var $trItem = $('#item-' + item);
        var justificativa = $tr.find('#justificativa-' + item).val();
        console.log($tr, justificativa);
        $.post(
            'query.php',
            {
                query: 'del-item-kit',
                id: item,
                justificativa: justificativa
            },
            function(r){
                if(r == 1) {
                    $tr.remove();
                    $trItem.remove();
                } else {
                    alert(r);
                }
            }
        );
        return false;
    });

    $("#outro").click(function() {
        $("input[name='nome']").attr("readonly", "");
        $("input[name='apr']").attr("readonly", "");
        $("#cod").val("-1");
        $("input[name='nome']").focus();
    });

    $("#add-item-kit").click(function() {
        if (validar_campos("dialog-kit")) {
            var cod = $("#cod").val();
            var catalogo_id = $("#catalogo_id").val();
            var qtd = $("#dialog-kit input[name='qtd']").val();
            var kit = $("#dialog-kit").attr('kit');
            var cod_ref = $("#dialog-kit").attr('cod');
            $.post("query.php", {
                query: 'novo-item-kit',
                cod: cod,
                qtd: qtd,
                kit: kit,
                cod_ref: cod_ref,
                catalogo_id: catalogo_id
            }, function(r) {
                if (r == '-1')
                    alert('ERRO: Tente mais tarde!');
                else
                    $("#tabela-itens-kit").html(r);
            });
            $("#cod").val("-1");
            $("#dialog-kit input[name='nome']").val("");
            $("#dialog-kit input[name='apr']").val("");
            $("#dialog-kit input[name='qtd']").val("");
            $("#dialog-kit input[name='busca']").focus();
        }
        return false;
    });

    $('input:radio[name=cod_ref]').click(function(){

        var catalogo_id = $(this).attr('catalogo_id');
        var cod = $(this).attr('cod');
        var kit = $(this).attr('kit');

        $.post('query.php', {query: 'atualiza-cod-ref-kit', cod_tiss:cod,idkit:kit,catalogo_id:catalogo_id }, function(retorno){

        });


    });

    $(".rem-item-solicitacao").unbind("click", remove_item).bind("click",
            remove_item);

    function atualiza_rem_item() {
        $(".rem-item-solicitacao").unbind("click", remove_item).bind("click",
                remove_item);
    }

    // //jeferson solicita��o 2012-17-10
    $(".qtdok").click(function() {

        x = $(this).parent().children('input[class=qtd_item]').val();

        if ($(this).is(":checked")) {

            $(this).attr('aceito', 0);
            $(this).parent().parent().attr('qtd', x);
            item = $(this).parent().children('img').attr('item');

            atualiza_qtd(item, '+');
        } else {
            $(this).attr('aceito', 1);
            item = $(this).parent().children('img').attr('item');

            atualiza_qtd(item, '-');
        }

    });

    $(".qtd_item").blur(function(){
        var quantidade = $(this).val();
        //alert(quantidade);
        $(this).closest("tr").attr('qtd',quantidade);
    });

    function remove_item() {
        if (confirm("Deseja realmente remover?")) {
            $(this).parent().parent().remove();
            atualiza_qtd($(this).attr('item'), '-');
        }
        return false;
    }

    $(".rem-item").bind("click", remove_item);

    $("#add-item")
        .click(
            function() {

                if (validar_campos("formulario")){

                    var cod = $("#cod").val();
                    var catalogo_id = $("#formulario input[name='catalogo_id']").val();
                    var nome = $("#formulario input[name='nome']").val();
                    var item = $("#formulario").attr("item");
                    var qtd = $("#formulario input[name='qtd']").val();
                    var mostrar_justificativa = '';
                    var enviar_justificativa = '';
                    var justificativa = $("#formulario select[name='justificativa']").val();
                    var valEntregaImediata = $("#entrega-imediata").is(':checked');
                    var enviar_justificativa_outros = '';
                    var periodica = '';
                    var categoria = $("#categoria").val();
                    var valor = $("#valor-venda").val();



                    if(justificativa !== undefined) {
                        if(justificativa == '11'){
                            mostrar_justificativa = ". Justificativa: " + $("#formulario select[name='justificativa'] option:selected").text()+' - '+$("#justificativa-outros").val();
                            enviar_justificativa_outros = " justificativa_outros='" + $("#justificativa-outros").val() + "'";
                        }else{
                            mostrar_justificativa = ". Justificativa: " + $("#formulario select[name='justificativa'] option:selected").text();
                            enviar_justificativa_outros = '';
                        }

                        if (justificativa == '2') {
                            periodica = $('#prescricoes-ativas').val();
                            mostrar_justificativa += ' DE ' + $('#prescricoes-ativas :selected').text();
                            $('#just-prescricao').html('');
                        }

                        enviar_justificativa = " justificativa='" + justificativa + "'";
                    }
                    var entregaImediata='';
                    if($(this).attr('data-tipoSolicitacao') == -1 || $(this).attr('data-tipoSolicitacao') == 3 || $(this).attr('data-tipoSolicitacao') ==6 ) {
                        var entregaImediataChecked = valEntregaImediata ? 'checked="checked"' : '';
                        entregaImediata="&nbsp;&nbsp;<input type='checkbox' name='entrega-imediata' "+entregaImediataChecked+" value='1' class='entrega-imediata'/><b>Entrega Imediata</b>";
                    }

                    var tipo = $("#formulario").attr("tipo");
                    var dados = "nome='" + nome + "' tipo='" + tipo+ "' categoria='"+categoria+"' valor='"+valor+"'"
                        + " cod='" + cod + "' periodica='" + periodica + "' qtd='" + qtd + "'" + "' catalogo_id='" + catalogo_id + "'" + enviar_justificativa + enviar_justificativa_outros;
                    var linha = nome + ". Quantidade: " + qtd + mostrar_justificativa;

                    if (tipo == 'med' || tipo == 'mat' || tipo=='die') {
                        var aceito = "<input type='hidden' class='qtdok' aceito='0'></input>";
                    } else {
                        var aceito = '';
                    }

                    var botoes = "<img align='right' class='rem-item-solicitacao' item='"
                        + item
                        + "' src='../utils/delete_16x16.png' title='Remover' border='0'>";

                    if (tipo == 'kits') {
                        $.get("busca_itens_kit.php", {
                            kit: cod,
                            qtd_kits: qtd
                        }, function(response) {
                            if (response == '-1') {
                                return false;
                            } else {
                                var itens_kit = '<ul>';
                                response =  JSON.parse(response);
                                var excluir_item = '';
                                for(var i = 0; i <= response.length; i++) {
                                    try{
                                        excluir_item = '<img class="rem-item-kit" kit="' + response[i]['kit'] + '" iditem="' + response[i]['idItem'] + '" src="../utils/delete_16x16.png" title="Remover Item" border="0">';
                                        itens_kit = itens_kit + '<li>' + excluir_item + ' ' + response[i]['qtd'] + ' x ' + response[i]['nome'] + '</li>';
                                    }catch(e){
                                        console.log("Error:", e);
                                    }
                                }
                                itens_kit = itens_kit + '</ul>';
                                $("#" + tipo + "-" + item).append(
                                    "<tr bgcolor='#ffffff' class='dados' "
                                    + dados + " ><td>" + linha + aceito + entregaImediata
                                    + botoes + itens_kit + "</td></tr>");
                            }
                        });
                    } else {
                        $("#" + tipo + "-" + item).append(
                            "<tr bgcolor='#ffffff' class='dados' "
                            + dados + " ><td>" + linha + aceito + entregaImediata
                            + botoes + "</td></tr>");
                    }
                    atualiza_qtd(item, '+');
                    atualiza_rem_item();
                    $("#cod").val("-1");
                    $("#formulario input[name='nome']").val("");
                    $("#formulario input[name='qtd']").val("");
                    $("#formulario input[name='entrega-imediata']").attr('checked',false);
                    $("#formulario select[name='justificativa']").val("");
                    $("#categoria").val('');
                    $("#valor").val('');
                    $('#busca-item').val('');
                    exitPage.needToConfirm = true;
                }

                return false;
            });

    $(".rem-item-kit").live('click', function() {
        if(confirm('Deseja remover esse item?')){
            $(this).parent().remove();
            var $itens_excluidos = $("#itens-kits-excluidos");
            $itens_excluidos.val($itens_excluidos.val() + $(this).attr('kit') + "#" + $(this).attr('iditem') + "$");
        }
    });

    $("#dialog-salvo").dialog({
        autoOpen: false,
        modal: true,
        position: 'top',
        close: function(event, ui) {
            window.location = "?view=solicitacoes";
        },
        buttons: {
            "Fechar": function() {
                $(this).dialog("close");
            }
        }
    });

    $(".qtd_equipamento").keyup(function() {
        var valor = $(this).val().replace(/[^0-9]/g, '');
        $(this).val(valor);
    });

    $(".equipamento").live("click", function() {
        x = $(this).attr('cod_equipamento');

     //   if ($(this).is(":checked")) {
            $("#obs" + x).attr('readonly', false);
            $("#obs" + x).focus();
            $("#qtd" + x).attr('readonly', false);
            $("#qtd" + x).addClass("OBG");

       /* } else {
            $("#obs" + x).attr('readonly', true);
            $("#qtd" + x).attr('readonly', true);
            $("#qtd" + x).removeClass("OBG");
            $("#qtd" + x).removeAttr("style");
            $("#obs" + x).val('');
            $("#qtd" + x).val('');
        }*/

    });

    $("#salvar-solicitacao").click(function() {
         var $salvarSolic = $(this);
         var contadorDataAvulsa = 0;
         var contadorEntregaImediata=0;
         var equipamentos='';
         var itens='';
         var aprovado = 0;
         var primeira_prescricao = $("#primeira-prescricao").is(":checked") ? $("#primeira-prescricao").val() : 0;
         $(".acao-equipamento").each(function(i) {
             var codEquipamento = this.dataset.idequipamento;
             var euipamentoAtivo = this.dataset.euipamentoAtivo;
             var idCapmedica = this.dataset.idCapmedica;
             var acaoEquipamento = $(this).val();
             var nomeEquipamento = $(this).closest('tr').children('td').eq(0).text();
                if ($(this).val() != -1) {
                      entregaImediata='N';
                    if($("#entrega-imediata"+codEquipamento).is(':checked')){
                        entregaImediata='S';
                        contadorEntregaImediata++;
                    }else{
                        contadorDataAvulsa++;
                    }
                    
                        linha2 =  codEquipamento  + "#" + $("#qtd"+codEquipamento).val() 
                                + "#" + $("#observacao"+codEquipamento).val() 
                                + "#" + entregaImediata+ "#" +euipamentoAtivo+ "#" +idCapmedica
                                + "#" + acaoEquipamento + "#" + nomeEquipamento + "$";
                           equipamentos += linha2;
                    
                }
            });
             
            var total=$(".dados").size();
             $(".dados").each(function(i) {
                var nome = $(this).attr("nome");
                var tipo = -1;
                switch ($(this).attr("tipo")) {
                    case 'serv':
                        tipo = 3;
                        break;
                    case 'kits':
                        tipo = 2;
                        break;
                    case 'mat':
                        tipo = 1;
                        break;
                    case 'med':
                        tipo = 0;
                        break;
                    case 'die':
                        tipo = 3;
                        break;
                }
                
                if (tipo == 0 || tipo == 1 || tipo==3) {
                            c = parseInt($(this).children('td').eq(0).children('input[class=qtdok]').attr('aceito'));
                            aprovado += c;
                        }
                        
                var qtd = $(this).attr("qtd");
                var cod = $(this).attr("cod");
                var nomeItem = $(this).attr("nome");
                var catalogo_id = $(this).attr("catalogo_id");
                var justificativa = $(this).attr("justificativa");
                var valor = $(this).attr("valor");
                var categoria = $(this).attr("categoria");
                 entregaImediata='N';
                if($(this).children('td').eq(0).children("input[name='entrega-imediata']").is(':checked')){
                        entregaImediata='S';
                        contadorEntregaImediata++;
                }else{
                    contadorDataAvulsa++;
                }
                
                var linha = cod + "#" + qtd + "#" + tipo + "#" + catalogo_id + "#" + justificativa + "#" +
                    entregaImediata + "#" + '' + "#" + nomeItem + "#" + valor+ "#" + categoria;

                if (i < total - 1)
                    linha += "$";
                itens += linha;
            });
            
              
         
                var dataAvulsa= '00/00/0000';
                var horaAvulsa= '00:00';
                var dataEntregaImediata= '00/00/0000';
                var horaEntregaImediata= '00/00/0000'; 
            
            if (($("#solicitacao").attr("carater") == 3) || ($("#solicitacao").attr("carater") == 6)) { 
                
                dataAvulsa= $('#data-avulsa').val();
                horaAvulsa= $('#hora-avulsa').val();
                dataEntregaImediata=  $("#dataEntregaImediata").val();
                horaEntregaImediata=  $("#horaEntregaImediata").val();
                
                       
                  
                    if(contadorDataAvulsa === 0){
                        $("#data-avulsa").removeClass("OBG");
                        $("#hora-avulsa").removeClass("OBG"); 
                        $("#hora-avulsa").attr('style','');
                        $("#data-avulsa").attr('style','');
                        $("#hora-avulsa").val('');
                        $("#data-avulsa").val('');

                     }else{
                        $("#data-avulsa").addClass("OBG");
                        $("#hora-avulsa").addClass("OBG");

                     }

                    if(contadorEntregaImediata  === 0){
                        $("#dataEntregaImediata").removeClass("OBG");
                        $("#horaEntregaImediata").removeClass("OBG"); 
                        $("#dataEntregaImediata").attr('style','');
                        $("#horaEntregaImediata").attr('style','');
                        $("#dataEntregaImediata").val('');
                        $("#horaEntregaImediata").val('');
                     }else{
                        $("#dataEntregaImediata").addClass("OBG");
                        $("#horaEntregaImediata").addClass("OBG");
                     }
            }
                
                
                if (contadorEntregaImediata == 0 && contadorDataAvulsa == 0 ){             
             alert("A solicitação está vazia.");
             return false;
         }     
                
                
                /////velho
                if (!validar_campos("tabela-solicitacoes"))
                    return false;
                if (confirm('Deseja realmente salvar a prescrição?')) {
                     var total_apraz =  $(".aprazamento").size();
                     var aprazamentos='';
                    $(".aprazamento").each(function(i) {
                        var item = $(this).attr("item");
                        var apraz = " ";
                        $(this).find("input").each(function(i) {
                            apraz += " " + $(this).val();
                        });
                        var linha = item + "#" + apraz;

                        if (i < total_apraz - 1)
                            linha += "$";
                        aprazamentos += linha;

                    });
                    var obs = "";
                    var ob = "";
                    var total_obs = $(".obs").size();
                    $(".obs").each(function(i) {
                        var cod = $(this).attr("cod");

                        ob = $("#observacao-" + cod).val();

                        if (ob != "" && ob != undefined && ob != " ") {
                            var linha2 = cod + "#" + ob;

                            if (i < total_obs - 1)
                                linha2 += "$";
                            obs += linha2;

                        }
                    });

                    if($("#itens-kits-excluidos").val() != '') {
                        var itens_excluidos = $("#itens-kits-excluidos").val();
                        itens_excluidos = itens_excluidos.substr(0, (itens_excluidos.length - 1));
                    }
                    var empresaPrescricao = $("#empresa-prescricao").val();

                    
                    // //jeferson 18-10-2012
                 
                    if (aprovado == 0) {
                        //$salvarSolic.attr('disabled', 'disabled');
                        if(navigator.onLine) {
                            $.post("query.php", {
                                query: "salvar-solicitacao",
                                paciente: $("#solicitacao").attr("paciente"),
                                carater: $("#solicitacao").attr("carater"),
                                dados: itens,
                                presc: $("#solicitacao").attr("prescricao"),
                                aprazamentos: aprazamentos,
                                obs: obs,
                                equipamentos: equipamentos,
                                horaAvulsa: horaAvulsa,
                                dataAvulsa: dataAvulsa,
                                horaEntregaImediata: horaEntregaImediata,
                                dataEntregaImediata: dataEntregaImediata,
                                contadorDataAvulsa: contadorDataAvulsa,
                                contadorEntregaImediata: contadorEntregaImediata,
                                primeira_prescricao: primeira_prescricao,
                                itens_excluidos: itens_excluidos
                            }, function (r) {
                                if (r > 0) {
                                    $("#dialog-salvo input[name='prescricao']").val(r);
                                    $("#dialog-salvo input[name='empresa']").val(empresaPrescricao);

                                    exitPage.needToConfirm = false;
                                    $("#dialog-salvo").dialog("open");
                                } else {
                                    alert(r);
                                    $salvarSolic.removeAttr('disabled');
                                }
                            });
                        } else {
                            setTimeout(function() {
                                $salvarSolic.removeAttr('disabled');
                            }, 5000);
                        }
                    } else {
                           
                        alert("Você não marcou o 'OK' em algum item.");
                    }
                    // jefersonfim

                }
                return false;
            });

    $("#sair-sem-salvar").click(function() {
        exitPage.needToConfirm = true;

        $.post("query.php", {
            query: "release-lock",
            p: $("#solicitacao").attr("prescricao")
        }, function(r) {
            window.location = '?view=solicitacoes';
        });
    });

    $("#salvar-avulsa").click(function() {
        var $salvarAvulsa = $(this);
        var contadorDataAvulsa = 0;
        var contadorEntregaImediata=0;
        var equipamentos='';
        var itens='';
        $(".acao-equipamento").each(function(i) {
            var codEquipamento = this.dataset.idequipamento;
            var euipamentoAtivo = this.dataset.euipamentoAtivo;
            var idCapmedica = this.dataset.idCapmedica
            var acaoEquipamento = $(this).val();
            if ($(this).val() != -1) {
                entregaImediata='N';
                if($("#entrega-imediata"+codEquipamento).is(':checked')){
                    entregaImediata='S';
                    contadorEntregaImediata++;
                }else{
                    contadorDataAvulsa++;
                }

                linha2 =  codEquipamento  + "#" + $("#qtd"+codEquipamento).val()
                    + "#" + $("#observacao"+codEquipamento).val()
                    + "#" + entregaImediata+ "#" +euipamentoAtivo+ "#" +idCapmedica
                    + "#" +acaoEquipamento+ "$";
                equipamentos += linha2;

            }
        });

        var aditivo = false;
        var total=$(".dados").size();
        $(".dados").each(function(i) {
            var nome = $(this).attr("nome");
            var tipo = -1;
            switch ($(this).attr("tipo")) {
                case 'serv':
                    tipo = 3;
                    break;
                case 'kits':
                    tipo = 2;
                    break;
                case 'mat':
                    tipo = 1;
                    break;
                case 'med':
                    tipo = 0;
                    break;
                case 'die':
                    tipo = 3;
                    break;
            }
            var qtd = $(this).attr("qtd");
            var cod = $(this).attr("cod");
            var catalogo_id = $(this).attr("catalogo_id");
            var justificativa = $(this).attr("justificativa");
            var justificativa_outros = $(this).attr("justificativa_outros");
            var periodica = '';
            if(justificativa == '6' || justificativa == '7'){
                aditivo = true;
            }
            if(justificativa == '2') {
                periodica = $(this).attr("periodica");
            }
            entregaImediata='N';
            if($(this).children('td').eq(0).children("input[name='entrega-imediata']").is(':checked')){
                entregaImediata='S';
                contadorEntregaImediata++;
            }else{
                contadorDataAvulsa++;
            }

            var linha = cod + "#" + qtd + "#" + tipo + "#" + catalogo_id + "#" + justificativa+ "#" +entregaImediata+ (justificativa_outros != '' ? "#"+justificativa_outros : "") + '##' + periodica;

            if (i < total - 1)
                linha += "$";
            itens += linha;
        });


        if (contadorEntregaImediata == 0 && contadorDataAvulsa == 0 ){
            alert("A solicitação está vazia.");
            return false;
        }


        if(contadorDataAvulsa === 0){
            $("#data-avulsa").removeClass("OBG");
            $("#hora-avulsa").removeClass("OBG");
            $("#hora-avulsa").attr('style','');
            $("#data-avulsa").attr('style','');
            $("#hora-avulsa").val('');
            $("#data-avulsa").val('');

        }else{
            $("#data-avulsa").addClass("OBG");
            $("#hora-avulsa").addClass("OBG");

        }

        if(contadorEntregaImediata  === 0){
            $("#dataEntregaImediata").removeClass("OBG");
            $("#horaEntregaImediata").removeClass("OBG");
            $("#dataEntregaImediata").attr('style','');
            $("#horaEntregaImediata").attr('style','');
            $("#dataEntregaImediata").val('');
            $("#horaEntregaImediata").val('');
        }else{
            $("#dataEntregaImediata").addClass("OBG");
            $("#horaEntregaImediata").addClass("OBG");
        }

        if($("#itens-kits-excluidos").val() != '') {
            var itens_excluidos = $("#itens-kits-excluidos").val();
            itens_excluidos = itens_excluidos.substr(0, (itens_excluidos.length - 1));
        }

        if (!validar_campos("avulsa"))
            return false;
        if (confirm('Deseja realmente salvar a solicitação?')) {
            $salvarAvulsa.attr('disabled', 'disabled');

            if(navigator.onLine) {
                $.post("query.php", {
                    query: "salvar-solicitacao",
                    paciente: $("#avulsa").attr("paciente"),
                    emergencia: $("#avulsa").attr("e"),
                    carater: -1,
                    primeira_prescricao: 0,
                    dados: itens,
                    equipamentos: equipamentos,
                    presc: "-1",
                    dataAvulsa: $("#data-avulsa").val(),
                    horaAvulsa: $("#hora-avulsa").val(),
                    dataEntregaImediata: $("#dataEntregaImediata").val(),
                    horaEntregaImediata: $("#horaEntregaImediata").val(),
                    contadorEntregaImediata: contadorEntregaImediata,
                    contadorDataAvulsa: contadorDataAvulsa,
                    itens_excluidos: itens_excluidos
                    //fim atualiza��o Jefferson 23-03-2013
                }, function (r) {
                    if (r == "1") {
                        alert("Salvo com sucesso!");
                        exitPage.needToConfirm = false;
                        window.location = "?view=avulsa";

                    } else {
                        alert("ERRO: " + r);
                        $salvarAvulsa.removeAttr('disabled');
                    }
                });
            } else {
                setTimeout(function() {
                    $salvarAvulsa.removeAttr('disabled');
                }, 5000);
            }
            return false;

        }
        return false;
    });

    $("#voltar").click(function() {
        history.go(-1);
    });

    // ////// Modulo de Paciente de enfermagem -
    // avalia��o//////////////////////////

    $(".listcapmed").live('click', function() {

        var idp = $(this).attr("idpaciente");

        $.post("query.php", {
            query: "buscar-ficha-avaliacao-med",
            paciente: idp
        }, function(r) {
            if (r > 0) {
                x = r;
                window.location.href = '../medico/?view=paciente_med&act=listcapmed&id=' + idp;
            } else {
                $("#dialog-aviso-avaliacao").dialog("open");
            }
        });
        return false;
    });

    $("#dialog-aviso-avaliacao").dialog({
        autoOpen: false,
        modal: true,
        width: 400,
        buttons: {
            "Fechar": function() {
                $(this).dialog("close");
            }
        }
    });

    $(".botao_relatorios").live('click', function () {
        $("#dialog-relatorios").attr("cod", $(this).attr("cod"));
        $("#dialog-relatorios").dialog("open");
    });

    // ////// Modulo de Paciente de enfermagem -
    // evolu��o//////////////////////////

    $(".listfichaevolucao").live('click', function() {

        var idp = $(this).attr("idpaciente2");

        // alert(idp);

        $.post("query.php", {
            query: "buscar-ficha-evolucao-med",
            paciente2: idp
        }, function(r) {
            if (r > 0) {
                x = r;
                // alert(x);
                window.location.href = '../medico/?view=paciente_med&act=listfichaevolucao&id=' + idp;
                // $("#div-resultado-busca").html(r);
            } else {
                $("#dialog-aviso-evolucao").dialog("open");
            }
        });
        return false;
    });

    $("#dialog-aviso-evolucao").dialog({
        autoOpen: false,
        modal: true,
        width: 400,
        open: function(event, ui) {

        },
        buttons: {
            "Fechar": function() {
                $(this).dialog("close");
            }
        }
    });

    // //avaliacao

    //Validar campos radio buttom


    //Atualiza��o Eriton 25/03/2013
    function frequencia_uso(cont_med) {
        $.post("paciente_enf.php", {
            query: "frequencia_uso", id: "NULL", editar: "NULL"
        }, function(r) {
            //alert(r);
            $("#frequencia_" + cont_med).html(r);
            //$("td[name='']").html(r);
        });
    }


    $('#add-medicamento-ativo').click(function() {
        var contador = $(".frequencia-uso").size();
        var x = $("input[name='medicamento_ativo']").val();
        var bexcluir = "<img src='../utils/delete_16x16.png' class='del-medicamento-ativo' title='Excluir' border='0' >";
        $('#medicamentos_ativos')
                .append(
                "<tr><td class='medicamento-ativo'  desc='" + x + "'> "
                + bexcluir
                + " "
                + x
                + " Apresenta&ccedil;&atilde;o <select name='apresentacao' class='COMBO_OBG' class='med_ativo'> <option value = '-1'>  </option>"
                + "<option value='Ampola'> Ampola </option>"
                + "<option value='C&aacute;psula'> C&aacute;psula </option>"
                + "<option value='CPR'> CPR </option>"
                + "<option value = 'Frasco' > Frasco </option>"
                + "<option value = 'Solu&ccedil;&atilde;o'> Solu&ccedil;&atilde;o </option>"
                + "<option value = 'Sach&ecirc;'> Sach&ecirc; </option>"
                + "<option value = 'Seringa'> Seringa </option>"
                + "<option value='T&oacute;pico'> T&oacute;pico </option> </select></td>"
                + "<td>Via<select name='via' class='COMBO_OBG' class='med_ativo'>"
                + "<option value = '-1'>  </option> <option value='VO'> VO </option> <option value = 'SNE' > SNE </option> <option value = 'GTM'> GTM </option> <option value ='IV'> IV </option> <option value ='IM'> IM </option> <option value ='oftalmica'> OFT&Aacute;LMICA </option> <option value ='topica'> T&Oacute;PICA </option> <option value ='SC'> SC </option> <option value ='IV'> Inalat&oacute;ria </option></select></td>"
                + "<td> Posologia <input type='text' name='posologia' class='OBG' class='med_ativo'/></td><td class='frequencia-uso' id='frequencia_" + contador + "'> Frequ&ecirc;ncia"
                + "</td><td> Fam&iacute;lia custeia <input type='checkbox' name='familia_custeia' value='0'  class='med-uso' /></td> </tr>");
        frequencia_uso(contador);
        //Fim Atualiza��o Eriton 25/03/2013
        $("input[name='apresentacao-medicacao']").val("");
        $("input[name='medicamento_ativo']").val("");
        // $("#outros-prob-ativos").attr('checked',false);
        $(".medicamentos_ativos").hide();
        // $("input[name='busca-problemas-ativos']").focus();
        atualiza_rem_medicamentos_ativos();
        return false;

    });


    atualiza_rem_medicamentos_ativos();
    function atualiza_rem_medicamentos_ativos() {
        $(".del-medicamento-ativo").unbind("click", remove_medicamentos_ativos)
                .bind("click", remove_medicamentos_ativos);

    }

    function remove_medicamentos_ativos() {
        $(this).parent().parent().remove();

        return false;
    }

    $(".med-uso").live('click', function() {

        if ($(this).is(':checked')) {
            var x = $(this).attr("value", 1);

        } else {
            $(this).attr("value", 0);

        }

    });

    if($("input[name=cicatriz]").is(":checked")){
        if($("input[name=cicatriz]").val() == 1){
            $("input[name='local_cicatriz']").closest("td").attr("style","display:inline");
        }else{
            $("input[name='local_cicatriz']").closest("td").attr("style","display:none");
        }
    }
    $("input[name='cicatriz']").change(function(){
        if($(this).val() == 1){
            $("input[name='local_cicatriz']").closest("td").attr("style","display:inline");
        }else{            
            $("input[name='local_cicatriz").val('');
            $("input[name='local_cicatriz']").closest("td").attr("style","display:none");
        }
    });
    //Atualiza��o Eriton 02-04-2013
    $('.icterica_escala').hide();
    $(".pele_icterica_anicterica").click(function() {
        if ($(this).val() == '1') {
            $('.icterica_escala').addClass("OBG").show();
        } else {
            $('.icterica_escala').removeClass("OBG").hide();
        }
    });
    //Atualiza��o Eriton 03-04-2013

    if ($("input[name='tipo_ventilacao']:checked").val() >= '2') {
        $('.ventilacion').show();
    } else {
        $('.ventilacion').hide();
    }
    //$('.ventilacion').hide();
    $(".tipo_ventilacion").click(function() {
        if ($(this).val() >= '2') {
            $('.ventilacion').addClass("COMBO_OBG").show();
        } else {
            $('.ventilacion').removeClass("COMBO_OBG").hide();
            $("#tipo_ventilacao1").val(-1);
            $("#num_ventilacao").val('');
            $('.bipap').attr("style","display:none");
        }

    });
    $("#tipo_ventilacao1").change(function() {
        if ($(this).val() == '3') {
            $("select[name='tipo_ventilacao2']").val('2').attr(":selected");
            $('.bipap').attr("style","display:inline");
        }
    });
    
    $(".tipo_ventilacao2").click(function() {
        if ($(this).val() == '2') {
            $('.bipap').attr("style","display:inline");
            $('.vm_bipap').addClass("OBG_RADIO");
        } else {
            $('.bipap').attr("style","display:none");
            $('.vm_bipap').removeClass("OBG_RADIO");
            $('.vm_bipap').attr('checked',false);
            
        }

    });
    if($(".vm_bipap").is(":checked")){
        $(".bipap").attr("style","display:inline");
    }else{
        $('.bipap').attr("style","display:none");
    }
    $(".dreno_span").hide();
    $('.dreno_select').change(function() {

        if ($(this).val() == 's') {
            $('.dreno_span').show();
        } else {
            $('.dreno_span').hide();
        }
    });

    
    if($("select[name='oxigenio_sn']").is(":checked")){
        if($(this).val() == 'n'){
            $(".oxigenio_span").hide();            
        }
    }
    $('.oxigenio_sn').change(function() {

        if ($(this).val() == 's') {
            $('#oxigenio_span').show();
            $("input[name='oxigenio']").addClass("OBG_RADIO");
        } else {
            $('#oxigenio_span').hide();
            $("input[name='oxigenio']").removeClass("OBG_RADIO");
            $("input[name='oxigenio']").parent().parent().attr("style","");
            $(".item_oxigenio").attr('checked',false);            
            $("input[name='vent_cateter_oxigenio_num']").val('');
            $("input[name='vent_cateter_oxigenio_quant']").val('');                     
            $("input[name='vent_cateter_oculos_num']").val('');
            $("input[name='vent_respirador_tipo']").val('');
            $("input[name='vent_mascara_ventury']").val('');
            $("input[name='vent_concentrador_num']").val('');            
            
        }
    });

    $(".pescoco_span").hide();
    $(".alt_pescoco").change(function() {

        if ($(this).val() == '2') {
            $(".pescoco_span").show();
        } else {
            $(".pescoco_span").hide();
        }
    });

    $(".sonda_gastro_span1").hide();
    $(".sonda_gastro_span2").hide();

    if($("#sne").is(":checked")){
        $("input[name='sne_num']").removeAttr('disabled');
    }

    $("#sne").click(function(){
        if($(this).is(":checked")){
            $("input[name='sne_num']").removeAttr('disabled');
        }else{
            $("input[name='sne_num']").attr('disabled','disabled');
            $("input[name='sne_num']").val('');
        }
    });

    if ($("input[name='gtm_sonda_gastro']").is(":checked")) {
        var gtmSondaGastro = $("input[name='gtm_sonda_gastro']:checked").val();
        if (gtmSondaGastro == 1) {
            $("input[name='gtm_num']").removeAttr('disabled');
            $("input[name='gtm_num_profundidade']").removeAttr('disabled');
            $("input[name='ultima_substituicao_button']").removeAttr('disabled');
            $("input[name='ultima_substituicao_sonda']").attr('disabled','disabled');
            $("input[name='quant_sonda']").attr('disabled','disabled');
            $("input[name='ultima_substituicao_sonda']").val('');
            $("input[name='quant_sonda']").val('');
        } else if(gtmSondaGastro == 2) {
            $("input[name='quant_sonda']").removeAttr('disabled');
            $("input[name='ultima_substituicao_sonda']").removeAttr('disabled');            
            $("input[name='gtm_num']").attr('disabled','disabled');
            $("input[name='gtm_num']").val('');
            $("input[name='gtm_num_profundidade']").attr('disabled','disabled');
            $("input[name='gtm_num_profundidade']").val('');
            $("input[name='ultima_substituicao_button']").attr('disabled','disabled');
            $("input[name='ultima_substituicao_button']").val('');
        }
    }

    $("input[name='gtm_sonda_gastro']").change(function() {
        if ($(this).val() == 1) {
            $("input[name='gtm_num']").removeAttr('disabled');
            $("input[name='gtm_num_profundidade']").removeAttr('disabled');
            $("input[name='ultima_substituicao_button']").removeAttr('disabled');
            $("input[name='ultima_substituicao_sonda']").attr('disabled','disabled');
            $("input[name='quant_sonda']").attr('disabled','disabled');
            $("input[name='ultima_substituicao_sonda']").val('');
            $("input[name='quant_sonda']").val('');
        } else if($(this).val() == 2) {
            $("input[name='quant_sonda']").removeAttr('disabled');
            $("input[name='ultima_substituicao_sonda']").removeAttr('disabled');            
            $("input[name='gtm_num']").attr('disabled','disabled');
            $("input[name='gtm_num']").val('');
            $("input[name='gtm_num_profundidade']").attr('disabled','disabled');
            $("input[name='gtm_num_profundidade']").val('');
            $("input[name='ultima_substituicao_button']").attr('disabled','disabled');
            $("input[name='ultima_substituicao_button']").val('');
        }
    });

    $(".local_cicatriz_torax").hide();
    $('.cicatriz_torax').change(function() {

        if ($(this).val() == '1') {
            $('.local_cicatriz_torax').show();
            $("input[name='local_cicatriz_torax']").addClass("OBG");
        } else {
            $('.local_cicatriz_torax').hide();
            $("input[name='local_cicatriz_torax']").removeClass("OBG");
        }
    });

  
    $('.alt_aspiracoes').change(function() {

        if ($(this).val() == 's') {
            $('.aspiracoes_span').show();
            
            $('#aspiracao_secrecao').addClass('COMBO_OBG');
        } else {
            $('.aspiracoes_span').hide();
            $('.secrecao_span').hide();
             $('.caracteristicas_sec').attr('checked',false);
              $('.cor_sec').attr('checked',false);
            $('#aspiracao_secrecao').removeClass('COMBO_OBG');
            $('#aspiracao_secrecao').val(-1) 
            $('.aspiracoes_num').val('');
        }
    });

    $(".gastro_span").hide();
    

    $(".urinario_span").hide();
    $('#cateterismo_int').click(function() {

        if ($(this).is(":checked")) {
            $('.urinario_span').show();
        } else {
            $('.urinario_span').hide();
        }
    });


    $("#alimentacao_span").hide();
    $('.alimentacao_span2').show();
    $(".parental_span").hide();

    if ($("#dieta_sn").val() == 'n') {
        $('#alimentacao_span').show();
        $('.alimentacao_span2').show();
        $(".parental_span").hide();

    } else if ($("#dieta_sn").val() == '-1') {
        $("#alimentacao_span").hide();
        $(".alimentacao_span2").hide();
        $(".parental_span").hide();

    } else {
        $("#alimentacao_span").hide();
        $(".alimentacao_span2").hide();
        $(".parental_span").show();
    }
/////////atualiza��o Eriton 07-08-2013////////////
    $('#dieta_sn').change(function() {
        if ($(this).val() == 'n') {
            $('#alimentacao_span').show();
            $('.alimentacao_span2').show();
            $(".parental_span").hide();
            $(".dieta_n_check").addClass("OBG_RADIO");
            $(".sistema_dieta").addClass("OBG_RADIO");
            $(".gtm_sonda_gastro").addClass("OBG_RADIO");
            $(".sne_num").addClass("OBG_RADIO");
            $("#suplemento_select").addClass('COMBO_OBG');
            $(".dieta_s_check").removeClass("OBG_RADIO");

        } else if ($(this).val() == '-1') {
            $("#alimentacao_span").hide();
            $(".alimentacao_span2").hide();
            $(".parental_span").hide();
            $(".dieta_s_check").removeClass("OBG_RADIO");
            $(".sistema_dieta").removeClass("OBG_RADIO");
            $(".dieta_n_check").removeClass("OBG_RADIO");
            $(".gtm_sonda_gastro").removeClass("OBG_RADIO");
            $(".sne_num").removeClass("OBG_RADIO");
            $("#suplemento_select").removeClass('COMBO_OBG');
            $(".dieta_n_check").attr('checked', false);
            $(".dieta-extra-button").val("").attr('disabled', 'disabled');
            $(".dieta-extra-sonda").val("").attr('disabled', 'disabled');

        } else {
            $("#alimentacao_span").hide();
            $(".alimentacao_span2").hide();
            $(".parental_span").show();
            $(".dieta_s_check").addClass("OBG_RADIO");
            $(".sistema_dieta").removeClass("OBG_RADIO");
            $(".dieta_n_check").removeClass("OBG_RADIO");
            $(".gtm_sonda_gastro").removeClass("OBG_RADIO");
            $(".sne_num").removeClass("OBG_RADIO");
            $("#suplemento_select").removeClass('COMBO_OBG');
            $(".dieta_n_check").attr('checked', false);
            $(".dieta-extra-button").val("").attr('disabled', 'disabled');
            $(".dieta-extra-sonda").val("").attr('disabled', 'disabled');
        }
    });

    if($("#dieta_npt").is(":checked")){
        $("input[name='desc_npt']").removeAttr('disabled');
        $("input[name='vazao_npt']").removeAttr('disabled');
    }

    $("#dieta_npt").click(function() {
        if ($(this).is(":checked")) {
            $("input[name='desc_npt']").addClass("OBG");
            $("input[name='desc_npt']").removeAttr('disabled');
            $("input[name='vazao_npt']").addClass("OBG");
            $("input[name='vazao_npt']").removeAttr('disabled');
        } else {
            $("input[name='desc_npt']").removeClass("OBG").removeAttr("style");
            $("input[name='vazao_npt']").removeClass("OBG").removeAttr("style");
            $("input[name='desc_npt']").attr('disabled','disabled');
            $("input[name='vazao_npt']").attr('disabled','disabled');
            $("input[name='desc_npt']").val('');
            $("input[name='vazao_npt']").val('');
        }
    });

    if($("#npp").is(":checked")){
        $("input[name='desc_npp']").removeAttr('disabled');
        $("input[name='vazao_npp']").removeAttr('disabled');
    }

    $("#npp").click(function() {
        if ($(this).is(":checked")) {
            $("input[name='desc_npp']").addClass("OBG");
            $("input[name='desc_npp']").removeAttr('disabled');
            $("input[name='desc_npp']").addClass("OBG");
            $("input[name='vazao_npp']").removeAttr('disabled');
        } else {
            $("input[name='desc_npp']").removeClass("OBG").removeAttr("style");
            $("input[name='desc_npp']").removeClass("OBG").removeAttr("style");
            $("input[name='desc_npp']").attr('disabled','disabled');
            $("input[name='vazao_npp']").attr('disabled','disabled');
            $("input[name='desc_npp']").val('');
            $("input[name='vazao_npp']").val('');
        }
    });

    if ($("#sistema_aberto").is(":checked")) {
            $("input[name='quant_sistema_aberto']").removeAttr('disabled');
            $("input[name='uso_sistema_aberto']").removeAttr('disabled');        
    }

    $("#sistema_aberto").click(function() {
        if ($(this).is(":checked")) {
            $("input[name='quant_sistema_aberto']").addClass("OBG");
            $("input[name='quant_sistema_aberto']").removeAttr('disabled');
            $("input[name='uso_sistema_aberto']").removeAttr('disabled');
        } else {
            $("#quant_sistema_aberto").removeClass("OBG").removeAttr("style");
            $("input[name='quant_sistema_aberto']").attr('disabled','disabled');
            $("input[name='uso_sistema_aberto']").attr('disabled','disabled');
            $("input[name='quant_sistema_aberto']").val('');
            $("input[name='uso_sistema_aberto']").val('');
        }
    });

    if ($("#sistema_fechado").is(":checked")) {
            $("input[name='quant_sistema_fechado']").removeAttr('disabled');
            $("input[name='vazao']").removeAttr('disabled');        
    }

    $("#sistema_fechado").click(function() {
        if ($(this).is(":checked")) {
            $("#quant_sistema_fechado").addClass("OBG");
            $("input[name='quant_sistema_fechado']").removeAttr('disabled');
            $("input[name='vazao']").removeAttr('disabled');
        } else {
            $("#quant_sistema_fechado").removeClass("OBG").removeAttr("style");
            $("input[name='quant_sistema_fechado']").attr('disabled','disabled');
            $("input[name='vazao']").attr('disabled','disabled');
            $("input[name='quant_sistema_fechado']").val('');
            $("input[name='vazao']").val('');
        }
    });

    $(".excrecoes").click(function(i) {
        if ($(this).is(":checked")) {
            $(this).parent().parent().children('td').eq('1').children('input').addClass("OBG");
            $(this).parent().parent().children('td').eq('2').children("input[name='tamanho_fralda']").addClass("OBG_RADIO");
        } else {
            $(this).parent().parent().children('td').eq('1').children('input').removeClass("OBG");
            $(this).parent().parent().children('td').eq('2').children("input[name='tamanho_fralda']").removeClass("OBG_RADIO");
        }
    });

    //Atualização Eriton 12-08-13
    $("#uno_lumen").click(function() {
        if ($(this).is(":checked")) {
            $("#local_uno_lumen").addClass("OBG");
            $("#local_duplo_lumen").removeClass("OBG").removeAttr("style");
            $("#local_triplo_lumen").removeClass("OBG").removeAttr("style");
        }
    });

    $("#duplo_lumen").click(function() {
        if ($(this).is(":checked")) {
            $("#local_duplo_lumen").addClass("OBG");
            $("#local_uno_lumen").removeClass("OBG").removeAttr("style");
            $("#local_triplo_lumen").removeClass("OBG").removeAttr("style");
        }
    });

    $("#triplo_lumen").click(function() {
        if ($(this).is(":checked")) {
            $("#local_triplo_lumen").addClass("OBG");
            $("#local_uno_lumen").removeClass("OBG").removeAttr("style");
            $("#local_duplo_lumen").removeClass("OBG").removeAttr("style");
        }
    });

    //Atualiza��o Eriton 01-04-2013
    $(".cvc_span").hide();
    if($("input[name=cateter_venoso_central]").is(":checked")){
        $(".cvc_span").attr('style','display:block');
    }else{
        $(".cvc_span").attr('style','display:none');           
    }
    $("input[name=cateter_venoso_central]").live('change',function()
    {
        if ($("input[name=cateter_venoso_central]").is(":checked")) {
            $(".cvc_span").attr('style','display:block');
            $("#uno_lumen").addClass("OBG_RADIO");
            $("#duplo_lumen").addClass("OBG_RADIO");
            $("#triplo_lumen").addClass("OBG_RADIO");
        } else {            
            $("#uno_lumen").removeClass("OBG_RADIO");
            $("#uno_lumen").parent().parent().attr("style","");
            $("#duplo_lumen").removeClass("OBG_RADIO");
            $("#duplo_lumen").parent().parent().attr("style","");
            $("#triplo_lumen").removeClass("OBG_RADIO");
            $("#triplo_lumen").parent().parent().attr("style","");
            $(".cvc_span").attr('style','display:none'); 
            $("input[name=cvc]").attr('checked',false);
        }
    });

    //$('.padrao_cardiorespiratorio').hide();
    $('.padrao_cardiorespiratorio_sel').change(function() {
        if ($(this).val() == '1') {
            $('.padrao_cardiorespiratorio').show();
        } else {
            $('.padrao_cardiorespiratorio').hide();
        }
    });

    //('.torax').hide();
    $('.torax_ausculta').hide();
    $('.torax_sel').click(function() {
        if ($(this).val() == '1') {
            $('.torax').show();
            $('.torax_ausculta').hide();
            var creptos_size = $("input[name=creptos_tipo]:checked").size();
            var estertor_size = $("input[name=estertor_tipo]:checked").size();
            var roncos_size = $("input[name=roncos_tipo]:checked").size();
            var mv_size = $("input[name=mv_tipo]:checked").size();
            if(creptos_size > 0)
                $('.creptos_span').show();
            if(estertor_size > 0)
                $('.estertores_span').show();
            if(roncos_size > 0)
                $('.roncos_span').show();
            if(mv_size > 0)
                $('.mv_dimi_span').show();
        } else {
            $('.torax_ausculta').show();
            $('.torax').hide();
            $('.creptos_span').hide();
            $('.estertores_span').hide();
            $('.roncos_span').hide();
            $('.mv_dimi_span').hide();
        }
    });
    
    //$('.creptos_span').hide();
    $('#creptos').click(function(){       
        if ($(this).is(":checked")) { 
            $('.creptos_span').show();
        }else{
            $('.creptos_span').hide();
        }
    });
    
    //$('.estertores_span').hide();
    $('#estertores').click(function(){       
        if ($(this).is(":checked")) { 
            $('.estertores_span').show();
        }else{
            $('.estertores_span').hide();
        }
    });
    
    //$('.roncos_span').hide();
    $('#roncos').click(function(){       
        if ($(this).is(":checked")) { 
            $('.roncos_span').show();
        }else{
            $('.roncos_span').hide();
        }
    });
    
    //$('.mv_dimi_span').hide();
    $('#mv_dimi').click(function(){       
        if ($(this).is(":checked")) { 
            $('.mv_dimi_span').show();
        }else{
            $('.mv_dimi_span').hide();
        }
    });
    
    $('.sibilos_tipo_span').hide();
    $('#sibilos').click(function(){       
        if ($(this).is(":checked")) { 
            $('.sibilos_tipo_span').show();
        }else{
            $('.sibilos_tipo_span').hide();
        }
    });
    
    //$(".eliminaciones_span").hide();
    $('.eliminaciones').change(function() {
        if ($(this).val() == '2') {
            $(".eliminaciones_span").show();
            $("input[name='fralda']").addClass("OBG_RADIO");
        } else {
            $(".eliminaciones_span").hide();
            $(".eliminaciones_span").removeClass("error");
            $("input[name='fralda']").removeClass("OBG_RADIO");
        }
    });

   if($("select[name='dispositivos_intravenosos'] option:selected").val() == 2){
        $(".valida_cvc").addClass("OBG_RADIO");
       $(".dispositivos_span").attr('style','display:block');
   }
    $("select[name='dispositivos_intravenosos']").change(function() {
        if ($(this).val() == '2') {
            $(".dispositivos_span").attr('style','display:block');
            $(".valida_cvc").addClass("OBG_RADIO");
        } else {
            $(".dispositivos_span").attr('style','display:none');
            $(".dispositivos_span").removeClass("error");
            $(".valida_cvc").removeClass("OBG_RADIO");
            $(".valida_cvc").attr('checked', false);
            $("#uno_lumen").removeClass("OBG_RADIO");
            $("#duplo_lumen").removeClass("OBG_RADIO");
            $("#triplo_lumen").removeClass("OBG_RADIO");
            $("#uno_lumen").attr('checked', false);
            $("#duplo_lumen").attr('checked', false);
            $("#triplo_lumen").attr('checked', false);
            $("#local_uno_lumen").removeClass("OBG").removeAttr("style");
            $("#local_duplo_lumen").removeClass("OBG").removeAttr("style");
            $("#local_triplo_lumen").removeClass("OBG").removeAttr("style");
            $(".cvc_span").hide();
        }
    });

    //fim atualiza��o Eriton 28-03-2013
  
    $('#aspiracao_secrecao').change(function() {
        if ($(this).val() == 'p') {
            $('.secrecao_span').show();
            $('.aspiracoes_num').addClass("OBG");
            $(".caracteristicas_sec").addClass("OBG_RADIO");
            $(".cor_sec").addClass("OBG_RADIO");
        } else {
            $(".secrecao_span").hide();
            $('.aspiracoes_num').removeClass("OBG");
            $(".caracteristicas_sec").removeClass("OBG_RADIO");
            $(".cor_sec").removeClass("OBG_RADIO");
            $('.caracteristicas_sec').attr('checked',false);
            $('.cor_sec').attr('checked',false);
        }
    });

    $("#parental2").hide();
    $('#visivel').change(function() {

        if ($(this).val() == '1') {
            $('#parental2').show();
        } else {
            $("#parental2").hide();
        }
    });

    $("#suplemento_span").hide();
    $('#suplemento_select').change(function() {

        if ($(this).val() == 's') {
            $('#suplemento_span').show();
            $('.suplemento_add').addClass("OBG");
        } else {
            $('#suplemento_span').hide();
            $('.suplemento_add').removeClass("OBG");
        }
    });

    $("#topicos_span").hide();
    $('#topicos_sn').change(function() {

        if ($(this).val() == 's') {
            $('#topicos_span').show();
            $('.topico_add').addClass("OBG");
        } else {
            $('#topicos_span').hide();
            $('.topico_add').removeClass("OBG");

        }
    });


    $(document).ready(function() {
        var tipo = $("input[name=tipoScore]").val();
        /*if (tipo == 1) {
            calcularScorePetrobras();
        }else{
            calcularScoreAbmid();
        }*/
        if ($('.item_topico').length) {
            alert('existe');
        }
    });
    //$("#dreno_span").hide();
    $('.dreno_sn2').change(function() {

        if ($(this).val() == 's') {
            $('.dreno_span2').show();
        } else {
            $('.dreno_span2').hide();

            $('.dreno_tipo').each(function() {
                $(this).parent().remove();
            });
        }
    });


    $('#oxigenio_sn2').change(function() {

        if ($(this).val() == 's') {
            $('#oxigenio_span2').show();
        } else {
            $('#oxigenio_span2').hide();

            $("input[name='oxigenio']:checked").attr('checked', '');
            $("input[name='vent_cateter_oxigenio_num']").val('');
            $("input[name='vent_cateter_oxigenio_quant']").val('');
            $("input[name='vent_cateter_oculos_num']").val('');
            $("input[name='vent_respirador_tipo']").val('');
            $("input[name='vent_mascara_ventury']").val('');
            $("input[name='vent_concentrador_num']").val('');
        }
    });

    
    $("#alimentacao_span").hide();
    
    //$("#secrecao_span").hide();
    $('#aspiracao_secrecao2').change(function() {

        if ($(this).val() == 'p') {
            $('#secrecao_span2').show();
        } else {
            $("#secrecao_span2").hide();
            $("input[name='caracteristicas_sec']:checked").attr('checked', '');
        }
    });

    $("#parental22").hide();
    $('#visivel2').change(function() {

        if ($(this).val() == '1') {
            $('#parental22').show();
        } else {
            $("#parental22").hide();
        }
    });

        
    if($("select[name='alt_gastro'] option:selected").val() == 'n'){
        $(".funcionamento_gastrointestinal").removeClass("OBG_RADIO");
    }else{
        $('.gastro_span').show();
        $(".funcionamento_gastrointestinal").addClass("OBG_RADIO");
    }
    
    $("select[name='alt_gastro']").change(function() {
          if($(this).val() == 'n'){
            $('.gastro_span').hide();
            $(".funcionamento_gastrointestinal").removeClass("OBG_RADIO");
            $(".funcionamento_gastrointestinal").removeClass("error");
            $(".gastro_span").removeClass("OBG_RADIO");
            $(".gastro_span").removeClass("error");
          }else{
            $('.gastro_span').show();
              $(".funcionamento_gastrointestinal").addClass("OBG_RADIO");
          }
    });
    
    $("select[name='feridas_sn']").change(function()
    {
       if($(this).val() == 's')
       {
           $(".ferida").addClass("COMBO_OBG");
       }
       else
           {
               $(".ferida").removeClass("COMBO_OBG");
           }
    });

    //$("#suplemento_span").hide();
    $('#suplemento_sn2').change(function() {

        if ($(this).val() == 's') {
            $('#suplemento_span2').show();
        } else {
            $('#suplemento_span2').hide();

            $('.suplemento').each(function() {
                $(this).parent().remove();
            });
        }
    });

    //$("#topicos_span2").hide();
    $('#topicos_sn2').change(function() {

        if ($(this).val() == 's') {
            $('#topicos_span2').show();
        } else {
            $('#topicos_span2').hide();

            $('.topico').each(function() {
                $(this).parent().remove();
            });
        }

    });
    //$("#dor_span2").hide();
    $('#dor_sn2').change(function() {

        if ($(this).val() == 's') {
            $('#dor_span2').show();
        } else {
            $('#dor_span2').hide();

            $('.dor').each(function() {
                $(this).parent().remove();
            });
        }



    });


    //teste marcos

    $('#add-dreno').click(function() {
        var x = $("input[name='dreno']").val();

        var bexcluir = "<img src='../utils/delete_16x16.png' class='del-dreno' title='Excluir' border='0' >";
        if (x != '' && x != undefined) {
            $('#dreno')
                    .append(
                    "<tr><td colspan='4' class='dreno'  desc='"
                    + x
                    + "'> "
                    + bexcluir
                    + "  "
                    + x
                    + "  <b style='margin-left:50px;'>Local</b><input type='text' name='local_dreno' class='med_ativo'/> </td> </tr>");
        }
        $("input[name='dreno']").val("");

        atualiza_rem_dreno();
        return false;

    });
    atualiza_rem_dreno();
    function atualiza_rem_dreno() {
        $(".del-dreno").unbind("click", remove_dreno).bind("click",
                remove_dreno);
    }

    function remove_dreno() {
        $(this).parent().parent().remove();
        return false;
    }

    // //avaliacao da dor
    $("#dor_span").hide();
    $('#dor_sn').change(function() {

        if ($(this).val() == 's') {
            $('#dor_span').show();
        } else {
            $('#dor_span').hide();
        }
    });

    $("#add-dor").click(function() {
        var x = $("input[name='dor']").val();

        var bexcluir = "<img src='../utils/delete_16x16.png' class='del-dor-ativos' title='Excluir' border='0' >";
        $('#dor').append("<tr><td width='40%' class='dor1' name='" + x + "' > " + bexcluir + " <b>Local: </b>" + x + "</td><td width='60%' colspan='2'>Escala analogica:<select name='dor" + x + "' class='COMBO_OBG' style='background-color:transparent;'><option value='-1' selected></option>" +
                "<option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option>" +
                "<option value='6'>6</option><option value='7'>7</option><option value='8'>8</option><option value='9'>9</option><option value='10'>10</option></select>Padr&atilde;o da dor:<select name='padraodor' class='padrao_dor COMBO_OBG' style='background-color:transparent;'><option value='-1' selected></option><option value='1'>Pontada</option><option value='2'>Queimação</option><option value='3'>Latejante</option><option value='4'>Peso</option><option value='5'>Outro</option></select><input type='text' name='padraodor' style='display:none'></td></tr>");
        $("input[name='dor']").val("");

        $("input[name='dor']").focus();
        atualiza_dor_ativos();
        return false;

    });
    atualiza_dor_ativos();
    function atualiza_dor_ativos() {
        $(".del-dor-ativos").unbind("click", remove_dor).bind("click", remove_dor);
    }

    function remove_dor() {
        $(this).parent().parent().remove();
        return false;
    }

    // Suplemento
    $('#add-suplemento')
            .click(
            function() {
                var x = $("input[name='suplemento']").val();

                var bexcluir = "<img src='../utils/delete_16x16.png' class='del-suplemento' title='Excluir' border='0' >";
                if (x != '' && x != undefined) {
                    $('#suplemento').append(
                        "<tr><td colspan='4' class='suplemento item_suplemento'  desc='"
                        + x
                        + "'> "
                        + bexcluir
                        + "  "
                        + x
                        + "  <b style='margin-left:50px;'>Frequ&ecirc;ncia:</b><input type='text' name='local_suplemento' class='med_ativo'/> </td> </tr>");
                }
                $("input[name='suplemento']").val("");

                atualiza_rem_suplemento();
                return false;

            });

    atualiza_rem_suplemento();
    function atualiza_rem_suplemento() {
        $(".del-suplemento").unbind("click", remove_suplemento).bind("click",
                remove_suplemento);
    }

    function remove_suplemento() {
        $(this).parent().parent().remove();
        return false;
    }

    //topicos

    $('#add-topicos')
            .click(
            function() {
                var x = $("input[name='topico']").val();

                var bexcluir = "<img src='../utils/delete_16x16.png' class='del-topico' title='Excluir' border='0' >";
                if (x != '' && x != undefined) {
                    $('#topico')
                            .append(
                            "<tr><td colspan='4' class='topico item_topico'  desc="
                            + x
                            + "'> "
                            + bexcluir
                            + "  "
                            + x
                            + "  <b style='margin-left:50px;'>Frequ&ecirc;ncia:</b><input type='text' name='local_topico' class='med_ativo'/> </td> </tr>");
                }
                $("input[name='topico']").val("");

                atualiza_rem_topico();
                return false;

            });

    atualiza_rem_suplemento();
    function atualiza_rem_topico() {
        $(".del-topico").unbind("click", remove_topico).bind("click",
                remove_topico);
    }

    function remove_topico() {
        $(this).parent().parent().remove();
        return false;
    }



    ///////////////

    $(".paliativos").live('click', function() {

        if ($(this).is(':checked')) {
            var x = $(this).attr("value", 1);

        } else {
            $(this).attr("value", 0);
        }
    });

    if($("input[name='palergiamed']").val() == 's'){        
        $("input[name='alergiamed']").removeAttr('disabled');
    }

    $("input[name='palergiamed']").blur(function(){
        if($(this).val() == 's'){
            $("input[name='alergiamed']").removeAttr('disabled');
        }else{
            $("input[name='alergiamed']").attr('disabled','disabled');
            $("input[name='alergiamed']").val('');
        }
    });

    if($("input[name='palergiaalim']").val() == 's'){        
        $("input[name='alergiaalim']").removeAttr('disabled');
    }

    $("input[name='palergiaalim']").blur(function(){
        if($(this).val() == 's'){
            $("input[name='alergiaalim']").removeAttr('disabled');
        }else{
            $("input[name='alergiaalim']").attr('disabled','disabled');
            $("input[name='alergiaalim']").val('');
        }
    });

    // fun��o validar campos nova ficha atualiza��o Eriton 28-03-2013
    $('#salvar-ficha-avaliacao').click(function() {
        var cont_topico = 0;
        $('.item_topico').each(function() {
            cont_topico = cont_topico + 1;
        });
        if (cont_topico > 0) {
            $('.topico_add').removeClass("OBG");
        }

        var cont_suplemento = 0;
        $('.item_suplemento').each(function() {
            cont_suplemento = cont_suplemento + 1;
        });
        if (cont_suplemento > 0) {
            $('.suplemento_add').removeClass("OBG");
        }

        if (!validar_campos("div-ficha-avaliacao-enfermagem"))
            return false;
        if (confirm('Deseja realmente salvar a Avaliação?')) {
            var dados = $("form").serialize();
            var itens = "";
            var dreno = "";
            var dor = "";
            var ferida = "";
            var topico = "";
            var suplemento = "";
            var aprovado = 0;
            var total = $(".medicamento-ativo").size();
            $(".medicamento-ativo").each(function(i) {
                var nome = $(this).attr("desc");
                var apresentacao = $(this).children('select[name = apresentacao]').val();
                var via = $(this).parent().children('td').eq(1).children('select[name = via]').val();
                var posologia = $(this).parent().children('td').eq(2).children('input[name = posologia]').val();
                var frequencia_med = $(this).parent().children('td').eq(3).children('select[name = frequencia]').val();
                var familia_custeia = $(this).parent().children('td').eq(4).children('input[name = familia_custeia]').val();
                var linha = nome + "#" + apresentacao + "#" + via + "#" + posologia + "#" + familia_custeia + "#" + frequencia_med;
                if (i < total - 1)
                    linha += "$";
                itens += linha;
            });
// dreno
            var total2 = $(".dreno").size();
            $(".dreno").each(function(i) {
                var nome = $(this).attr("desc");
                var local = $(this).children('input[name = local_dreno]').val();
                var linha2 = nome + "#" + local;
                if (i < total2 - 1)
                    linha2 += "$";
                dreno += linha2;
            });

//dor            	
             var total_dor = $(".dor").size();
             var dor123 = "";
             $(".dor1").each(function(i) {
             var nome = $(this).attr("name");
                var padrao_id = $(this).parent().children('td').eq(1).children('select[name=padraodor]').val();
                var padrao = $(this).parent().children('td').eq(1).children('select[name=padraodor]').find('option').filter(':selected').text();
                var escala = $("select[name='dor" + nome + "'] option:selected").val();
                if (padrao_id == 5) {
                    padrao = $(this).parent().children('td').eq(1).children('input').val();
                }
                dor123 += nome + "#" + padrao_id + "#" + padrao + "#" + escala;
                if (i < total_dor - 1) {
                    dor123 += "$";
                }             
             });

// salvar ferida
            var total4 = $(".div-quadro-feridas").size();
            $(".div-quadro-feridas").each(function(i) {
                var local = $(this).children("table").children("tbody").children("tr").eq(0).children("td").eq(1).children("select").val();
                var teste_lado = $(this).children("table").children("tbody").children("tr").eq(0).children("td").eq(1).children("select").find('option').filter(':selected').attr("lado");
                if (teste_lado == 'S'){
                    var lado = $(this).children("table").children("tbody").children("tr").eq(0).children("td").eq(2).children("select").val();
                }else{
                    var lado = '';
                }
                var observacao = '';
                if(local == 51){
                    observacao = $(this).children("table").children("tbody").children("tr").eq(0).children("td").eq(2).children("textarea").val();

                }
                var tipo_lesao = $(this).children("table").children("tbody").children("tr").eq(1).children("td").eq(1).children("select").val();
                var cobertura = $(this).children("table").children("tbody").children("tr").eq(2).children("td").eq(1).children("select").val();
                var tamanho = $(this).children("table").children("tbody").children("tr").eq(3).children("td").eq(1).children("select").val();
                var profundidade = $(this).children("table").children("tbody").children("tr").eq(4).children("td").eq(1).children("select").val();
                var tecido = $(this).children("table").children("tbody").children("tr").eq(5).children("td").eq(1).children("select").val();
                var lesao = $(this).children("table").children("tbody").children("tr").eq(6).children("td").eq(1).children("select").val();
                var humidade = $(this).children("table").children("tbody").children("tr").eq(7).children("td").eq(1).children("select").val();
                var exsudato = $(this).children("table").children("tbody").children("tr").eq(8).children("td").eq(1).children("select").val();
                var contaminacao = $(this).children("table").children("tbody").children("tr").eq(9).children("td").eq(1).children("select").val();
                var pele = $(this).children("table").children("tbody").children("tr").eq(10).children("td").eq(1).children("select").val();
                var tempo = $(this).children("table").children("tbody").children("tr").eq(11).children("td").eq(1).children("select").val();
                var odor = $(this).children("table").children("tbody").children("tr").eq(12).children("td").eq(1).children("select").val();
                var periodo = $(this).children("table").children("tbody").children("tr").eq(13).children("td").eq(1).children("select").val();
                var status_ferida = $(this).children("table").children("tbody").children("tr").eq(14).children("td").eq(1).children("select").val();

                var imagemFeridas = getImagemFeridas(this);

                var linha4 = local + "#" + lado + "#" + tipo_lesao + "#" + cobertura + "#" + tamanho + "#" +
                              profundidade + "#" + tecido + "#" + lesao + "#" + humidade + "#" + exsudato + "#" +
                              contaminacao + "#" + pele + "#" + tempo + "#" + odor + "#" + periodo + "#" +
                              status_ferida + "#" + imagemFeridas.imagesFeridas + "#" +
                              imagemFeridas.observacao_imagem + "#"+ observacao;

                if ((total4 > 1) && (i != total4 - 1))
                  linha4 += "$";

                ferida += linha4;
            });

//topicos
            var total5 = $(".topico").size();
            $(".topico").each(function(i) {
                var nome = $(this).attr("desc");
                var local = $(this).children('input[name = local_topico]').val();
                var linha5 = nome + "#" + local;
                if (i < total5 - 1)
                    linha5 += "$";
                topico += linha5;
            });

//suplemento
            var total6 = $(".suplemento").size();
            $(".suplemento").each(function(i) {
                var nome = $(this).attr("desc");
                var local = $(this).children('input[name = local_suplemento]').val();
                var linha6 = nome + "#" + local;
                if (i < total6 - 1)
                    linha6 += "$";
                suplemento += linha6;
            });

//fim do suplemento
            var score = '';
            $("input[class='abmid']:checked").each(function(i) {
                score = score + $(this).attr('cod_item') + "#";
            });

            $("input[class='petro']:checked").each(function(i) {
                score = score + $(this).attr('cod_item') + "#";
            });

            var score_nead = '';
            $("input.nead:checked").each(function (i) {
                score_nead = score_nead + $(this).attr('cod_item') + "#";

            });
            var score_katz = '';
            $("input.katz:checked").each(function (i) {
                score_katz += 'katz#' + $(this).attr('name') + '#' + $(this).val() + "$";
            });

            var score_nead2016 = '';
            $("input.grupo1:checked").each(function (i) {
                score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).val() + "$";
            });

            $("input.grupo2:checked").each(function (i) {
                score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).val() + "$";
            });

            $("input.grupo3:checked").each(function (i) {
                score_nead2016 += 'grupos#' + $(this).attr('name') + '#' + $(this).attr('peso') + "$";
            });
            score_katz = score_katz.substring(0,(score_katz.length - 1));
            score_nead2016 = score_nead2016.substring(0,(score_nead2016.length - 1));

            var ficha_ava = "query=ficha-avaliacao&" + dados + "&itens=" + itens + "&dreno=" + dreno + "&dor123="
                    + dor123 + "&ferida=" + ferida + "&topico=" + topico + "&suplemento=" + suplemento + "&score=" + score
                    + "&score_nead=" + score_nead+ "&score_katz=" + score_katz +
                "&score_nead2016=" + score_nead2016;;

            $.ajax({
                type: "POST",
                url: "query.php",
                data: ficha_ava,
                success: function(msg) {

                    if (msg == 1) {
                        alert("Avaliacao salva com sucesso.");
                        window.location = '/enfermagem/?view=listavaliacaenfermagem&id='+$("input[name='paciente']").val();
                        return false;
                    } else {
                        alert(msg);
//                        return false;
                    }
                }
            });

        }
        return false;
    });

    $('#salvar-ficha-evolucao').click(function() {
        
        var cont_topico = 0;
        $('.item_topico').each(function() {
            cont_topico = cont_topico + 1;
        });
        if (cont_topico > 0) {
            $('.topico_add').removeClass("OBG");
        }

        var cont_suplemento = 0;
        $('.item_suplemento').each(function() {
            cont_suplemento = cont_suplemento + 1;
        });
        if (cont_suplemento > 0) {
            $('.suplemento_add').removeClass("OBG");
            $('.suplemento_add').removeAttr("style");
        }

        /*if (!validar_campos("div-ficha-avaliacao-enfermagem")){
            
            return false;
        }*/
       
        
        if (confirm('Deseja realmente salvar a Evolução?')) {
            var dados = $("form").serialize();
            var itens = "";
            var topico = "";
            var dreno = "";
            var ferida = "";
            var aprovado = 0;
            var suplemento = "";

            var total2 = $(".dreno").size();
            $(".dreno").each(
                    function(i) {

                        var nome = $(this).attr("desc");

                        var local = $(this).children(
                                'input[name = local_dreno]').val();

                        var linha2 = nome + "#" + local;
                        if (i < total2 - 1)
                            linha2 += "$";
                        dreno += linha2;


                    });

            //dor
            	
             var total3 = $(".dor").size();
             var dor123 = "";
             $(".dor").each(function(i) {
             var nome = $(this).attr("name");
                var padrao_id = $(this).parent().children('td').eq(1).children('select[name=padraodor]').val();
                var padrao = $(this).parent().children('td').eq(1).children('select[name=padraodor]').find('option').filter(':selected').text();
                var escala = $("select[name='dor" + nome + "'] option:selected").val();
                if (padrao_id == 5) {
                    padrao = $(this).parent().children('td').eq(1).children('input').val();
                }
                dor123 += nome + "#" + padrao_id + "#" + padrao + "#" + escala;
                if (i < total_dor - 1) {
                    dor123 += "$";
                }
             
             });
             // fim 28-10
             
            // salvar ferida

            var total4 = $(".div-quadro-feridas").size();
            $(".div-quadro-feridas").each(function(i) {
                var local = $(this).children("table").children("tbody").children("tr").eq(0).children("td").eq(1).children("select").val();
                var teste_lado = $(this).children("table").children("tbody").children("tr").eq(0).children("td").eq(1).children("select").find('option').filter(':selected').attr("lado");
                if (teste_lado == 'S'){
                    var lado = $(this).children("table").children("tbody").children("tr").eq(0).children("td").eq(2).children("select").val();
                }else{
                    var lado = '';
                }
                var observacao = '';
                if(local == 51){
                    observacao = $(this).children("table").children("tbody").children("tr").eq(0).children("td").eq(2).children("textarea").val();

                }
                var tipo_lesao = $(this).children("table").children("tbody").children("tr").eq(1).children("td").eq(1).children("select").val();
                var cobertura = $(this).children("table").children("tbody").children("tr").eq(2).children("td").eq(1).children("select").val();

                var tamanho = $(this).children("table").children("tbody").children("tr").eq(3).children("td").eq(1).children("select").val();
                var profundidade = $(this).children("table").children("tbody").children("tr").eq(4).children("td").eq(1).children("select").val();
                var tecido = $(this).children("table").children("tbody").children("tr").eq(5).children("td").eq(1).children("select").val();
                var lesao = $(this).children("table").children("tbody").children("tr").eq(6).children("td").eq(1).children("select").val();
                var humidade = $(this).children("table").children("tbody").children("tr").eq(7).children("td").eq(1).children("select").val();
                var exsudato = $(this).children("table").children("tbody").children("tr").eq(8).children("td").eq(1).children("select").val();
                var contaminacao = $(this).children("table").children("tbody").children("tr").eq(9).children("td").eq(1).children("select").val();
                var pele = $(this).children("table").children("tbody").children("tr").eq(10).children("td").eq(1).children("select").val();
                var tempo = $(this).children("table").children("tbody").children("tr").eq(11).children("td").eq(1).children("select").val();
                var odor = $(this).children("table").children("tbody").children("tr").eq(12).children("td").eq(1).children("select").val();
                var periodo = $(this).children("table").children("tbody").children("tr").eq(13).children("td").eq(1).children("select").val();
                var status_ferida = $(this).children("table").children("tbody").children("tr").eq(14).children("td").eq(1).children("select").val();

                var imagemFeridas = getImagemFeridas(this);

                var linha4 = local + "#" + lado + "#" + tipo_lesao + "#" + cobertura + "#" + tamanho + "#" + profundidade + "#" + tecido + "#" + lesao + "#" + humidade + "#" + exsudato + "#" + contaminacao + "#" + pele + "#" + tempo + "#" + odor + "#" + periodo+ "#" + status_ferida +
                          "#" + imagemFeridas.imagesFeridas + "#" + imagemFeridas.observacao_imagem + "#" + observacao;

                if ((total4 > 1) && (i != total4 - 1))
                    linha4 += "$";

                ferida += linha4;

                });

            //topicos
            var total5 = $(".topico").size();
            $(".topico").each(
                    function(i) {

                        var nome = $(this).attr("desc");

                        var local = $(this).children(
                                'input[name = local_topico]').val();

                        var linha5 = nome + "#" + local;
                        if (i < total5 - 1)
                            linha5 += "$";
                        topico += linha5;                        

                    });

            //fim topicos

            //suplemento

            var total6 = $(".suplemento").size();
            $(".suplemento").each(function(i) {

                var nome = $(this).attr("desc");
                var local = $(this).children('input[name = local_suplemento]').val();

                var linha6 = nome + "#" + local;
                if (i < total6 - 1)
                    linha6 += "$";
                suplemento += linha6;
                

            });


            //fim do suplemento


            var ficha_evo = "query=ficha-evolucao-enfermagem&" + dados + "&itens=" + itens + "&dreno=" + dreno
                    + "&ferida=" + ferida + "&topico=" + topico + "&suplemento=" + suplemento + "&dor123=" + dor123;

            $.ajax({
                type: "POST",
                url: "query.php",
                data: ficha_evo,
                success: function(msg) {

                    if (msg == 1) {
                        alert("Evolução salva com sucesso.");

                        window.location = '/enfermagem/?view=listevolucaoenfermagem&id='+$("input[name='paciente']").val();
                        return false;
                    } else {
                        alert(msg);
                    }
                }

            });

        }
        return false;
    });

    //FIM DA FICHA DE EVOLU��O 	
    // /////FIM DE Paciente de Enfermagem///////////////////////////////

    $("#buscar").click(function() {
        if(validar_campos('buscar-solicitacao')){
        var p = $("select[name='pacientes'] option:selected").val();
        var i = $("#div-busca input[name='inicio']").val();
        var f = $("#div-busca input[name='fim']").val();
        var pendente = $("#select-pendentes").val();

        $.post("query.php", {
            query: "buscar-prescricoes",
            paciente: p,
            inicio: i,
            fim: f,
            pendente: pendente
        }, function(r) {

            $("#div-resultado-busca").html(r);
        });
    }
        return false;
    });
    //Eriton
    $("#buscar-avulsa").click(function() {
        var paciente = $("select[name='pacientes'] option:selected").val();
        var inicio = $("#div-busca-avulsa input[name='inicio]").val();
        var fim = $("#div-busca-avulsa input[name='fim]").val();

        $.post("query.php", {
            query: "buscar-avulsa",
            paciente: paciente,
            inicio: inicio,
            fim: fim,
        }, function(r) {

            $("#div-resultado-busca-avulsa").html(r);
        });
        return false;
    });
    //Fim Eriton
    $(".desativar-presc").click(function() {
        var presc = $(this).attr('presc');
        $.post("query.php", {
            query: "desativar-prescricao",
            presc: presc
        }, function(r) {
            if (r == 1)
                $("#" + presc).fadeOut("slow");
        });
    });


    atualiza_rem_feridas_ativas();

    function atualiza_rem_feridas_ativas() {
        $(".del-ferida-ativa").unbind("click", remove_feridas_ativas).bind("click", remove_feridas_ativas);
    }

    function remove_feridas_ativas() {
        $(this).parent().parent().remove();
        return false;
    }


    /////////
    $('#add-feridas').click(function() {

        $("input[name='apresentacao-medicacao']").val("");
        $("input[name='medicamento_ativo']").val("");
        // $("#outros-prob-ativos").attr('checked',false);
        $(".medicamentos_ativos").hide();
        // $("input[name='busca-problemas-ativos']").focus();
        atualiza_rem_medicamentos_ativos();
        return false;

    });


    if($("select[name='feridas_sn'] option:selected").val() == 's'){
        $(".quadro_feridas_sn").attr("style","display:inline");
    }else{
        $(".quadro_feridas_sn").attr("style","display:none");
    }
    $('#ferida').change(function() {

        if ($(this).val() == 's') {
            $(".quadro_feridas_sn").show();

        } else {
            $(".quadro_feridas_sn").attr("style","display:none");


        }
    });

    // /jeferson 2012-10-28
    $("#nova_avaliacao_enfermagem").click(function() {

        var id = $("input[name='nova_avaliacao_id']").val();

        window.document.location.href = '?view=novaavaliacaoenf&id=' + id + '';

    });

    ///jeferson 2012-10-28 fim

    $("#nova_evolucao_enfermagem").click(function() {

        var id = $("input[name='nova_evolucao_id']").val();

        window.document.location.href = '?view=novaevolucaoenf&id=' + id + '';

    });

    /*$("#imprimir-ficha-avaliacao-enf").click(function(){
     
     var x =0;
     x =$("input[name='paciente']").val();
     window.location.href='?view&act=editaravaliacaoenfermagem&id='+x;
     
     
     });*/
    $('#novo_rel_alta').click(function() {
        var id = $(this).attr('paciente');
        window.location.href = '?view=relatorio&opcao=1&idp=' + id;
    });
    
    $('#novo_rel_prorrogacao').click(function() {
        var id = $(this).attr('paciente');
        window.location.href = '?view=relatorio&opcao=3&idp=' + id;
    });
    
    $('#novo_rel_aditivo').click(function() {
        var id = $(this).attr('paciente');
        window.location.href = '?view=relatorio&opcao=2&idp=' + id;
    });
    
    $('#novo_rel_visita').click(function() {
        var id = $(this).attr('paciente');
        window.location.href = '?view=relatorio&opcao=4&idp=' + id;
    });

    $('#novo_rel_deflagrado').click(function() {
        var id = $(this).attr('paciente');
        window.location.href = '?view=relatorio&opcao=5&idp=' + id;
    });
    
    $(".botao_relatorios").click(function() {
        $("#dialog-relatorios").attr("cod", $(this).attr("cod"));
        $("#dialog-relatorios").dialog("open");
    });

    $("#dialog-relatorios").dialog({
        autoOpen: false, modal: true, height: 250, width: 400,
        open: function(event, ui) {
            /*$.post("query.php",{query: "show-status-paciente", cod: $("#dialog-relatorios").attr("cod")},function(r){
             $("#dialog-relatorios").html(r);
             });*/

            $("#dialog-relatorios").html(
                "<br><input type='radio' class='botao_relatorio' id='alta' name='opcao_relatorio' value=1 checked/><label for='alta' class='botao_relatorio'>Alta Enfermagem</label><br>" +
                "<input type='radio' class='botao_relatorio' id='aditivo' name='opcao_relatorio' value=2 /><label for='aditivo' class='botao_relatorio'>Aditivo</label><br>" +
                "<input type='radio' class='botao_relatorio' id='prorrogacao' name='opcao_relatorio' value=3 /><label for='prorrogacao' class='botao_relatorio'>Prorrogação</label><br>" +
                "<input type='radio' class='botao_relatorio' id='visita' name='opcao_relatorio' value=4 /><label for='visita' class='botao_relatorio'>Visita Semanal</label><br>" +
                "<input type='radio' class='botao_relatorio' id='deflagrado' name='opcao_relatorio' value=5 /><label for='deflagrado' class='botao_relatorio'>Deflagrado</label><br>");
        },
        buttons: {
            Visualizar: function() {
                var opcao_escolhida = $("#dialog-relatorios input[name='opcao_relatorio']:checked").val();
                var id = $("#dialog-relatorios").attr("cod");
                if (opcao_escolhida == 1 || opcao_escolhida == 2 || opcao_escolhida == 3 || opcao_escolhida == 4 || opcao_escolhida == 5) {                    
                    window.location.href = '?view=relatorio&opcao=' + opcao_escolhida + '&id=' + id;
                }
                
                $(this).dialog("close");
            },
            Cancelar: function() {

                $(this).dialog("close");
            }
        }
    });
    
    $(".tabela_feridas").hide();
    if($("select[name='feridas_sn'] option:selected").val() == 1){
       $(".tabela_feridas").show();
   }
   if($("input[name='mostrar_ferida']").val() == 1){
       $(".tabela_feridas").show();
   }
    $("#feridas_prorrogacao").change(function(){
        if ($(this).val() == 1){
            $(".tabela_feridas").show();
        }else{
            $(".tabela_feridas").hide();
        }
    });
    
    
    if($("select[name='alteracoes_sn'] option:selected").val() == 1){
       $(".tabela_alteracoes").show();
       $("#desc_alteracoes").addClass("OBG");
   }else{
        $("#tabela_alteracoes").hide();
   }
    $("#alteracoes_clinicas").change(function(){
        if ($(this).val() == 1){
            $("#tabela_alteracoes").show();
            $("#desc_alteracoes").addClass("OBG");
        }else{
            $("#tabela_alteracoes").hide();
            $("#desc_alteracoes").removeClass("OBG");
        }
    });
    
    $("#tabela_problemas").hide();
    if($("select[name='problemas_sn'] option:selected").val() == 1){
       $("#tabela_problemas").show();
       $("#prob_material").addClass("OBG");
   }
    $("#problemas_material").change(function(){
        if ($(this).val() == 1){
            $("#tabela_problemas").show();
            $("#prob_material").addClass("OBG");
        }else{
            $("#tabela_problemas").hide();
            $("#prob_material").removeClass("OBG");
        }
    });
    
    $("#tabela_dificuldade").hide();
    if($("select[name='dificuldades_sn'] option:selected").val() == 1){
       $("#tabela_dificuldade").show();
       $("#dificuldade").addClass("OBG");
   }
    $("#dificuldades_profissional").change(function(){
        if ($(this).val() == 1){
            $("#tabela_dificuldade").show();
            $("#dificuldade").addClass("OBG");
        }else{
            $("#tabela_dificuldade").hide();
            $("#dificuldade").removeClass("OBG");
        }
    });
    
    $("#tabela_familia").hide();
    if($("select[name='familia_sn'] option:selected").val() == 1){
       $("#tabela_familia").show();
       $("#familia").addClass("OBG");
   }
    $("#dificuldades_familia").change(function(){
        if ($(this).val() == 1){
            $("#tabela_familia").show();
            $("#familia").addClass("OBG");
        }else{
            $("#tabela_familia").hide();
            $("#familia").removeClass("OBG");
        }
    });
        
    $("#salvar_rel_alta").click(function(){
        var total = $(".cuidado").size();
        if(total > 0){
            $("#cuidado_alta").removeClass("COMBO_OBG");
            $("#cuidado_alta").removeAttr("style");
        }else{
            $("#cuidado_alta").addClass("COMBO_OBG");
        }
        if (validar_campos("relatorio-alta")) {
            var paciente_id = $(this).attr("paciente");
            var dados = $("form").serialize();
            var cuidado = "";
            $(".cuidado").each(function(i) {
                var cuidado_id = $(this).attr("cuidado_id");
                var cuidado_outros = $(this).attr("cuidado_outros");
                cuidado += cuidado_id + "#" + cuidado_outros + "#";
                if (i < total - 1)
                    cuidado += "$";
            });
            var alta = "query=relatorio_alta&" + dados + "&cuidado=" + cuidado + "&paciente_id=" + paciente_id;
            
            $.ajax({
                type: "POST",
                url: "query.php",
                data: alta,
                success: function(msg) {

                    if (msg == 1) {
                        alert("Salvo com sucesso!!");
                        window.location.href = '?view=relatorio&opcao=1&id=' + paciente_id;
                    } else {
                        alert("DADOS: " + msg);
                    }
                }
            });
            return false;
        }
        return false;
    });

$("#editar_rel_alta").click(function(){
        var paciente_id = $(this).attr("paciente");
        var dados = $("form").serialize();
        var total = $(".cuidado").size();
        var cuidado = "";
        $(".cuidado").each(function(i) {
            var cuidado_id = $(this).attr("cuidado_id");
            var cuidado_outros = $(this).attr("cuidado_outros");
            cuidado += cuidado_id + "#" + cuidado_outros + "#";
            if (i < total - 1)
                cuidado += "$";
        });
        var alta = "query=editar_relatorio_alta&" + dados + "&cuidado=" + cuidado + "&paciente_id=" + paciente_id;
        
        $.ajax({
                type: "POST",
                url: "query.php",
                data: alta,
                success: function(msg) {

                    if (msg == 1) {
                        alert("Salvo com sucesso!!");
                      window.location.href = '?view=relatorio&opcao=1&id=' + paciente_id;
                    } else {
                        alert("DADOS: " + msg);
                    }
                }
            });
        return false;
    });
    
    
    $("#salvar_rel_aditivo").click(function(){

            var paciente_id = $(this).attr("paciente");
            var dados = $("form").serialize();
            var total = $(".prob-ativos-evolucao").size();
            var probativos = "";
            $(".prob-ativos-evolucao").each(function(i) {
                var cid = $(this).attr("cod");
                var desc = $(this).attr("desc");
                var just = $(this).children('textarea').val();
                probativos += cid + "#" + desc + "#" + just;
                if (i < total - 1)
                    probativos += "$";
            });

            var feridas = '';
            var total = $(".div-quadro-feridas").size();
            $(".div-quadro-feridas").each(function(i) {
                var local = $(this).find("textarea[name='local']").val();
                var caracteristica = $(this).find("textarea[name='caracteristica']").val();
                var curativo = $(this).find("textarea[name='curativo']").val();
                var prescricao = $(this).find("textarea[name='prescricao']").val();

                var imagemFeridas = getImagemFeridas(this);

                var linha = local + "#" + caracteristica + "#" + curativo + "#" + prescricao + "#" + imagemFeridas.imagesFeridas + "#";

                if (i < total - 1)
                    linha += "$";
                feridas += linha;
            });
            var itens_solicitados ='';
            var itens_solicitados_total = $(".dados").size();
            var contadorEntregaImediata =0;
            var contadorDataAvulsa =0;
             $(".dados").each(function(i){
                 var catlogo_id = $(this).attr('catalogo-id');
                 var tiss = $(this).attr('tiss');
                 var tipo = $(this).attr('tipo');
                 var qtd = $(this).attr('qtd');
                 var frequencia = $(this).attr('frequencia');
                 var id_kit = $(this).attr('id-kit');
                 var qtd_kit = $(this).attr('qtd-kit');
                 var categoria = $(this).attr('categoria');
                 var valorVenda = $(this).attr('valor-venda');
                 var entregaImediata = 'N';
                 if ($(this).children('td').children("input[name='entrega-imediata-aditivo']").is(':checked')){
                     entregaImediata = 'S';
                     contadorEntregaImediata ++;
                 }else{
                     contadorDataAvulsa++;
                 }


                 
                var itens_solicitados_linha= catlogo_id + "#" + tiss  + "#" + tipo + "#" + qtd + "#" +
                                            qtd_kit + "#" + id_kit+ "#" +entregaImediata+ "#" +valorVenda+ "#" +categoria + "#" +frequencia;
                if (i < itens_solicitados_total - 1)
                    itens_solicitados_linha += "$";
                itens_solicitados += itens_solicitados_linha;
                 
             });
        if(contadorEntregaImediata  === 0){
            $("#dataEntregaImediata").removeClass("OBG");
            $("#horaEntregaImediata").removeClass("OBG");
            $("#dataEntregaImediata").attr('style','');
            $("#horaEntregaImediata").attr('style','');
            $("#dataEntregaImediata").val('');
            $("#horaEntregaImediata").val('');
        }else{
            $("#dataEntregaImediata").addClass("OBG");
            $("#horaEntregaImediata").addClass("OBG");
        }
        if(contadorDataAvulsa === 0){
            $("#data-avulsa").removeClass("OBG");
            $("#hora-avulsa").removeClass("OBG");
            $("#hora-avulsa").attr('style','');
            $("#data-avulsa").attr('style','');
            $("#hora-avulsa").val('');
            $("#data-avulsa").val('');

        }else{
            $("#data-avulsa").addClass("OBG");
            $("#hora-avulsa").addClass("OBG");

        }
            var horaEntregaImediata =  $("#horaEntregaImediata").val();
            var dataEntregaImediata =  $("#dataEntregaImediata").val();
            var dataAvulsa = $("#data-avulsa").val();
            var horaAvulsa = $("#hora-avulsa").val();
            var plano = $("#plano-id").val();


            var aditivo = "query=relatorio_aditivo&" + dados +
                          "&probativo=" + probativos +
                          "&feridas=" + feridas +
                         "&paciente_id=" + paciente_id+
                         "&itens_solicitados="+itens_solicitados+
                         "&dataEntregaImediata="+dataEntregaImediata+
                         "&horaEntregaImediata="+horaEntregaImediata +
                         "&contadorEntregaImediata="+contadorEntregaImediata+
                         "&horaAvulsa="+horaAvulsa+
                         "&dataAvulsa="+dataAvulsa+
                         "&contadorDataAvulsa="+contadorDataAvulsa+
                        "&plano="+plano;
        if (validar_campos("relatorio-prorrogacao")) {
            $.ajax({
                type: "POST",
                url: "query.php",
                data: aditivo,
                success: function(msg) {

                    if (msg == 1) {
                        alert("Relatório Aditivo salvo com sucesso! Itens Solicitados já enviados para auditoria.");
                        window.location.href = '?view=relatorio&opcao=2&id=' + paciente_id;
                    } else {
                        alert("DADOS: " + msg);
                    }
                }
            });
            return false;
        }
        return false;
    });

$("#editar_rel_aditivo").click(function(){
        var paciente_id = $(this).attr("paciente");
        var dados = $("form").serialize();
        var total = $(".prob-ativos-evolucao").size();
        var probativos = "";
            $(".prob-ativos-evolucao").each(function(i) {
                var cid = $(this).attr("cod");
                var desc = $(this).attr("desc");
                var just = $(this).children('textarea').val();
                probativos += cid + "#" + desc + "#" + just;
                if (i < total - 1)
                    probativos += "$";
            });

            var feridas = '';
            var total = $(".div-quadro-feridas").size();
            $(".div-quadro-feridas").each(function(i) {
                var local = $(this).children("table").children("tbody").children("tr").eq(0).children("td").eq(1).children("textarea[name='local']").val();
                var caracteristica = $(this).children("table").children("tbody").children("tr").eq(1).children("td").eq(1).children("textarea[name='caracteristica']").val();
                var curativo = $(this).children("table").children("tbody").children("tr").eq(2).children("td").eq(1).children("textarea[name='curativo']").val();
                var prescricao = $(this).children("table").children("tbody").children("tr").eq(3).children("td").eq(1).children("textarea[name='prescricao']").val();
                var id_ferida = $(this).find("input[name='id_ferida']").val();

                var linha = local + "#" + caracteristica + "#" + curativo + "#" + prescricao + "#" + id_ferida + "#";

                if (i < total - 1)
                    linha += "$";
                feridas += linha;
            });

            var feridas_excluir = '';
            var total = $(".ferida_excluida").size();
            $(".ferida_excluida").each(function(i) {
                var local_excluir = $(this).children("table").children("tbody").children("tr").eq(0).children("td").eq(1).children("textarea[name='local']").val();
                var caracteristica_excluir = $(this).children("table").children("tbody").children("tr").eq(1).children("td").eq(1).children("textarea[name='caracteristica']").val();
                var curativo_excluir = $(this).children("table").children("tbody").children("tr").eq(2).children("td").eq(1).children("textarea[name='curativo']").val();
                var prescricao_excluir = $(this).children("table").children("tbody").children("tr").eq(3).children("td").eq(1).children("textarea[name='prescricao']").val();
                var id_ferida_excluir = $(this).find("input[name='id_ferida']").val();

                var linha_excluir = local_excluir + "#" + caracteristica_excluir + "#" + curativo_excluir + "#" + prescricao_excluir + "#" + id_ferida_excluir + "#";

                if (i < total - 1)
                    linha += "$";
                feridas_excluir += linha_excluir;
            });
       //27-01-2015
            var itens_solicitados ='';
            var id_solicitacao_manter = '';
            var itens_solicitados_total = $(".dados").size();
             $(".dados").each(function(i){
                 var catlogo_id = $(this).attr('catalogo-id');
                 var tiss = $(this).attr('tiss');
                 var tipo = $(this).attr('tipo');
                 var qtd = $(this).attr('qtd');
                 var id_kit = $(this).attr('id-kit');
                 var qtd_kit = $(this).attr('qtd-kit');
                 var id_solicitacao_rel_aditivo = $(this).attr('id-solicitacao-rel-aditivo');
                 
                 if(id_solicitacao_rel_aditivo == 0){                 
                        var itens_solicitados_linha= catlogo_id + "#" + tiss  + "#" + tipo 
                                                      + "#" + qtd + "#" + qtd_kit + "#" + id_kit;
                        if (i < itens_solicitados_total - 1)
                            itens_solicitados_linha += "$";
                        itens_solicitados += itens_solicitados_linha;
                    }else{
                       id_solicitacao_manter += id_solicitacao_rel_aditivo + "$";
                    }
                 
             });
       
       
       
        var aditivo = "query=editar_relatorio_aditivo&" + dados + "&probativo=" + probativos 
                     + "&feridas=" + feridas + "&feridas_excluir=" + feridas_excluir
                     + "&paciente_id=" + paciente_id + "&itens_solicitados="+itens_solicitados
                     + "&id_solicitacao_rel_aditivo_manter="+ id_solicitacao_manter;
        $.ajax({
                type: "POST",
                url: "query.php",
                data: aditivo,
                success: function(msg) {

                    if (msg == 1) {
                        alert("Editado com sucesso!!");
                        window.location.href = '?view=relatorio&opcao=2&id=' + paciente_id;
                    } else {
                        alert("DADOS: " + msg);
                    }
                }
            });
        return false;
    });
    
    $("#salvar_rel_visita").click(function(){
        if (validar_campos("relatorio-prorrogacao")) {
            var paciente_id = $(this).attr("paciente");
            var dados = $("form").serialize();
            
            var visita = "query=relatorio_visita&" + dados + "&paciente_id=" + paciente_id;
            
            $.ajax({
                type: "POST",
                url: "query.php",
                data: visita,
                success: function(msg) {

                    if (msg == 1) {
                        alert("Salvo com sucesso!!");
                        window.location.href = '?view=relatorio&opcao=4&id=' + paciente_id;
                    }else{
                        alert("DADOS: " + msg);
                    }
                }
            });
            return false;
        }
        return false;
    });

    $("#editar_rel_visita").click(function(){
        if (validar_campos("relatorio-prorrogacao")) {
            var paciente_id = $(this).attr("paciente");
            var dados = $("form").serialize();
            
            var visita = "query=editar_relatorio_visita&" + dados + "&paciente_id=" + paciente_id;
            
            $.ajax({
                type: "POST",
                url: "query.php",
                data: visita,
                success: function(msg) {
                    
                    if (msg == 1) {
                        alert("Salvo com sucesso!!");
                        window.location.href = '?view=relatorio&opcao=4&id=' + paciente_id;
                    }else{
                        alert("DADOS: " + msg);
                    }
                }
            });
            return false;
        }
        return false;
    });

    $("#salvar_rel_deflagrado").click(function(){
        if (validar_campos("relatorio-deflagrado")) {
            var paciente_id = $(this).attr("paciente");
            var dados = $("form").serialize();
            
            var visita = "query=relatorio_deflagrado&" + dados + "&paciente_id=" + paciente_id;
            
            $.ajax({
                type: "POST",
                url: "query.php",
                data: visita,
                success: function(msg) {

                    if (msg == 1) {
                        alert("Salvo com sucesso!!");
                        window.location.href = '?view=relatorio&opcao=5&id=' + paciente_id;
                    }else{
                        alert("DADOS: " + msg);
                    }
                }
            });
            return false;
        }
        return false;
    });

    $("#editar_rel_deflagrado").click(function(){
        if (validar_campos("relatorio-deflagrado")) {
            var paciente_id = $(this).attr("paciente");
            var dados = $("form").serialize();

            var probativos = "";
            var total = $(".prob-ativos-evolucao").size();
            $(".prob-ativos-evolucao").each(function(i) {
                var cid = $(this).attr("cod");
                var desc = $(this).attr("desc");
                var just = $(this).parent().children("td").eq(1).children("label").html();
                probativos += cid + "#" + desc + "#" + just;
                if (i < total - 1)
                    probativos += "$";
            });
            
            var visita = "query=editar_relatorio_deflagrado&" + dados + "&probativos=" + probativos + "&paciente_id=" + paciente_id;
            
            $.ajax({
                type: "POST",
                url: "query.php",
                data: visita,
                success: function(msg) {
                    
                    if (msg == 1) {
                        alert("Editado com sucesso!!");
                        window.location.href = '?view=relatorio&opcao=5&id=' + paciente_id;
                    }else{
                        alert("DADOS: " + msg);
                    }
                }
            });
            return false;
        }
        return false;
    });
    
    $("#cuidado_alta").live("change",function(){
       var cuidado_id = $("select[name='cuidado_alta'] option:selected").val();
       if(cuidado_id == 8){
           var cuidado = "<input type='text' class='cuidado_outros' name='cuidado_outros' size=50></></td></tr>";
           $(".span_cuidado").append(cuidado);
       }
    });
        
    $("#add-cuidado").click(function(){
        var cuidado_id = $("select[name='cuidado_alta'] option:selected").val();
        var cuidado = $("select[name='cuidado_alta'] option:selected").html();
        var cuidado_outros = '';
        if(cuidado_id == 8){
            cuidado = '';
            cuidado_outros = $("input[name='cuidado_outros']").val();
        }
        var bexcluir = "<img src='../utils/delete_16x16.png' class='del-cuidado' title='Excluir' border='0' >";
        if(cuidado_id != -1 ) {
            if(cuidado_id == 8 && cuidado_outros == ''){
                alert("Favor preencher a caixa de texto destinada a \"Outras Orientações\" quando esta opção for selecionada");
            }else{
                $("#tabela_cuidado").append("<tr><td colspan='3' class='cuidado' cuidado_id='" + cuidado_id + "' cuidado_outros='" + cuidado_outros + "'>" + bexcluir + " " + cuidado + cuidado_outros + "</td></tr>");
            }
        }else{
            alert("Você não selecionou uma orientação válida!");
        }
       $("select[name='cuidado_alta']").val(-1);
       $(".cuidado_outros").remove();
       return false;
    });
    
    $(".del-cuidado").live("click",function(){
            $(this).parent().parent().remove();
        });

    $(".data-ferida-prorrogacao").live('change',function(){

        var dataInput = $(this);
        var dataBR = dataInput.get(0).value;
        var form = dataInput.closest('td').find('form');
        var dataUS = dataBR.split('/').reverse().join('-');
        form.find('.images-ferida').attr('data-date',dataUS);


    });
    
    $("#salvar_prorrogacao").click(function() {
        if (validar_campos("relatorio-prorrogacao")) {
            var paciente_id = $(this).attr("paciente");
            var total = $(".prob-ativos-evolucao").size();
            var dados = $("form").serialize();
            
            var feridas = '';
            var total = $(".div-quadro-feridas").size();
            $(".div-quadro-feridas").each(function(i) {
                var local = $(this).find("textarea[name='local']").val();
                var caracteristica = $(this).find("textarea[name='caracteristica']").val();
                var curativo = $(this).find("textarea[name='curativo']").val();
                var prescricao = $(this).find("textarea[name='prescricao']").val();

                var imagemFeridas = getImagemFeridas(this);

                var linha = local + "#" + caracteristica + "#" + curativo + "#" + prescricao + "#" + imagemFeridas.imagesFeridas + "#";

                if (i < total - 1)
                    linha += "$";
                feridas += linha;
            });

            var prob = "query=relatorio_prorrogacao&" + dados + "&feridas=" + feridas + "&paciente_id=" + paciente_id;

            $.ajax({
                type: "POST",
                url: "query.php",
                data: prob,
                success: function(msg) {

                    if (msg == 1) {
                        alert("Salvo com sucesso!!");
                        window.location.href = '?view=relatorio&opcao=3&id=' + paciente_id;
                    } else {
                        alert("DADOS: " + msg);
                    }
                }
            });
            return false;
        }
        return false;
    });
    
        $("#editar_prorrogacao").click(function() {
        if (validar_campos("relatorio-prorrogacao")) {
            var paciente_id = $(this).attr("paciente");
            var total = $(".prob-ativos-evolucao").size();
            var dados = $("form").serialize();
            
            var feridas = '';
            var total = $(".div-quadro-feridas").size();
            $(".div-quadro-feridas").each(function(i) {
                var local = $(this).find("textarea[name='local']").val();
                var caracteristica = $(this).find("textarea[name='caracteristica']").val();
                var curativo = $(this).find("textarea[name='curativo']").val();
                var prescricao = $(this).find("textarea[name='prescricao']").val();
                var id_ferida = $(this).find("input[name='id_ferida']").val();

                var imagemFeridas = getImagemFeridas(this);

                var linha = local + "#" + caracteristica + "#" + curativo + "#" + prescricao + "#" + id_ferida + "#" + imagemFeridas.imagesFeridas + "#" + imagemFeridas.observacao_imagem + "#";

                if (i < total - 1)
                    linha += "$";
                feridas += linha;
            });

            var feridas_excluir = '';
            var total = $(".ferida_excluida").size();
            $(".ferida_excluida").each(function(i) {
                var local_excluir = $(this).children("table").children("tbody").children("tr").eq(0).children("td").eq(1).children("textarea[name='local']").val();
                var caracteristica_excluir = $(this).children("table").children("tbody").children("tr").eq(1).children("td").eq(1).children("textarea[name='caracteristica']").val();
                var curativo_excluir = $(this).children("table").children("tbody").children("tr").eq(2).children("td").eq(1).children("textarea[name='curativo']").val();
                var prescricao_excluir = $(this).children("table").children("tbody").children("tr").eq(3).children("td").eq(1).children("textarea[name='prescricao']").val();
                var id_ferida_excluir = $(this).find("input[name='id_ferida']").val();

                var linha_excluir = local_excluir + "#" + caracteristica_excluir + "#" + curativo_excluir + "#" + prescricao_excluir + "#" + id_ferida_excluir + "#";

                if (i < total - 1)
                    linha += "$";
                feridas_excluir += linha_excluir;
            });

            var prob = "query=editar_relatorio_prorrogacao&" + dados + "&paciente_id=" + paciente_id + "&feridas=" + feridas + "&feridas_excluir=" + feridas_excluir;

            $.ajax({
                type: "POST",
                url: "query.php",
                data: prob,
                success: function(msg) {

                    if (msg == 1) {
                        alert("Salvo com sucesso!!");
                        window.location.href = '?view=relatorio&opcao=3&id=' + paciente_id;
                    } else {
                        alert("DADOS: " + msg);
                    }
                }
            });
            return false;
        }
        return false;
    });
    

    
    if($("select[name='feridas_aditivo'] option:selected").val() == 1){
            var tipo_relatorio = $("input[name='tipo_relatorio']").val();
            var id_prorrogacao = $("input[name='id_relatorio']").val();
            var editar = $("input[name='editar']").val();
            $(".quadro_feridas_aditivo").show();
            $.post("relatorios.php", {
                query: "quadro-lesoes",
                fromRelatorio: $("#fromRelatorio").val(),
                id_prorrogacao: id_prorrogacao,
                tipo_relatorio: tipo_relatorio,
                editar: editar
            }, function(feridas) {
                $("#quadro-lesoes").append(feridas);
                $(".data").datepicker({
                    inline: true
                });
            });

        }else{
            $(".quadro_feridas_aditivo").hide();
        }
    $("select[name='feridas_aditivo']").change(function(){
        var alteracao = $("select[name='feridas_aditivo'] option:selected").val();
        var id_prorrogacao = $("input[name='id_relatorio']").val();
        var tipo_relatorio = $("input[name='tipo_relatorio']").val();
        var editar = $("input[name='editar']").val();
        if(alteracao == 1){
            $(".quadro_feridas_aditivo").show();
            $.post("relatorios.php", {
            query: "quadro-lesoes",
            fromRelatorio: $("#fromRelatorio").val(),
            pacienteId: $("#pacienteId").val(),
            id_prorrogacao: id_prorrogacao,
            tipo_relatorio: tipo_relatorio,
            editar: editar
        }, function(feridas) {
            $("#quadro-lesoes").append(feridas);
                $(".data").datepicker({
                    inline: true
                });

        });
        }else{
            $(".quadro_feridas_aditivo").hide();
            $("#quadro-lesoes").html('');
            $("#feridas").remove();
            $("#tabela_feridas").remove();
        }
        $('.data').datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1
        });

    });
    
    $("input[name='busca-problemas-ativos-evolucao']").autocomplete({
        source: "/utils/busca_cid.php",
        minLength: 3,
        select: function(event, ui) {

            var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prob-ativos' title='Excluir' border='0' >";
            $('#problemas_ativos').append("<tr><td colspan='2' class='prob-ativos-evolucao' cod='" + ui.item.id + "' desc='" + ui.item.name + "'> " + bexcluir + " " + ui.item.name + " <textarea cols=70 rows=3  class='OBG' ></textarea></td></tr>");
            $("input[name='busca-problemas-ativos-evolucao']").val("");
            $("input[name='busca-problemas-ativos-evolucao']").focus();
            atualiza_rem_problemas_ativos_evolucao();

            return false;
        },
    });

    $("#add-prob-ativo-evolucao").click(function() {
        var x = $("input[name='outro-prob-ativo-evolucao']").val();

        if (x == "") {

            $("input[name='outro-prob-ativo-evolucao']").css({"border": "3px solid red"});

        } else {
            $("input[name='outro-prob-ativo-evolucao']").css({"border": ""});


            var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prob-ativos' title='Excluir' border='0' >";
            $('#problemas_ativos').append("<tr><td colspan='2' class='prob-ativos-evolucao' cod='1111' desc='" + x + "'> " + bexcluir + " " + x + " <textarea cols=70 rows=2  class='OBG' ></textarea></td></tr>");
            $("input[name='busca-problemas-ativos-evolucao']").val("");
            $("input[name='outro-prob-ativo-evolucao']").val("");
            $("#outros-prob-ativos-evolucao").attr('checked', false);
            $(".outro_ativo").hide();
            $("input[name='busca-problemas-ativos-evolucao']").focus();
            atualiza_rem_problemas_ativos_evolucao();

        }
        return false;


    });

    $(".del-prob-ativos").unbind("click", remove_problemas_ativos_evolucao).bind("click", remove_problemas_ativos_evolucao);


    function atualiza_rem_problemas_ativos_evolucao() {
        $(".del-prob-ativos").unbind("click", remove_problemas_ativos_evolucao)
                .bind("click", remove_problemas_ativos_evolucao);
    }

    function remove_problemas_ativos_evolucao() {
        $(this).parent().parent().remove();
        return false;
    }
    $(".lado_ferida").attr("style", "display:none");
    if($("select[name='quadro_local_ferida'] option:selected").attr("lado") == 'S'){
        $(this).parent().parent().children("td").eq(2).children('span').eq(0).attr("style","display:inline");
        $(this).parent().parent().children("td").eq(2).children('select').attr("style","display:inline");
    }else{
        $(this).parent().parent().children("td").eq(2).children('span').eq(0).attr("style","display:none");
        $(this).parent().parent().children("td").eq(2).children('select').attr("style","display:none");
    }
    
    $("select[name='quadro_local_ferida']").live("change",function() {
        if($(this).children("option:selected").attr("lado") == "S"){
            $(this).parent().parent().children("td").eq(2).children('span').eq(0).attr("style","display:inline");
            $(this).parent().parent().children("td").eq(2).children('select').attr("style","display:inline");
        }else{
            $(this).parent().parent().children("td").eq(2).children('span').eq(0).attr("style","display:none");
            $(this).parent().parent().children("td").eq(2).children('select').attr("style","display:none");
        }
        if($(this).val() == 51){
            $(this).parent().parent().children("td").eq(2).children('span').eq(1).attr("style","display:inline");
            $(this).parent().parent().children("td").eq(2).children('textarea').attr("style","display:inline");
        }else{
            $(this).parent().parent().children("td").eq(2).children('span').eq(1).attr("style","display:none");
            $(this).parent().parent().children("td").eq(2).children('textarea').attr("style","display:none");
            $(this).parent().parent().children("td").eq(2).children('textarea').val('');
        }
    });

  // SM-879
  function validar_form(){
    var v = true;
    if(!validar_campos("div-prescrever-curativos"))
      v = false;
    return v;
  }

  // INICIALIZA O PLUGIN DE TOOLTIP NO ICONE DE OBSERVAÇÃO
  $(".img-obs").easyTooltip();

  // SE EXISTIR OS CAMPOS DA CONDIÇÃO, INICIALIZA O PLUGIN DATEPICKER NO FORMATO DE PERIODO PARA PRESCRIÇÃO
  if($("input[name=inicio]").val() !== undefined && $("input[name=fim]").val() !== undefined)
  {
    var inicioG = new Date($("input[name=inicio]").val().split("/").reverse().join(","));
    var fimG = new Date($("input[name=fim]").val().split("/").reverse().join(","));

    $(".cp-inicio").datepicker({
      defaultDate: "+1w",
      minDate: inicioG,
      maxDate: fimG,
      changeMonth: true,
      numberOfMonths: 1
    });
    $(".cp-fim").datepicker({
      minDate: inicioG,
      maxDate: fimG,
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1
    });
    $(".prescricao-prazo-entrega").datepicker({
      minDate: inicioG ,
      maxDate: fimG,
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1
    });
  }

  // REINICIA TODOS OS DATEPICKERS "INICIO" DA PAGINA CASO HAJA ALGUMA ALTERAÇÃO NA DATA DA PRESCRIÇÃO
  $("input[name=inicio]").live('change', function(){
    inicioG = new Date($(this).val().split("/").reverse().join(","));
    $(".cp-inicio").datepicker("destroy");
    $(".cp-inicio").datepicker({
      defaultDate: "+1w",
      minDate: inicioG,
      maxDate: fimG,
      changeMonth: true,
      numberOfMonths: 1
    });
    $(".prescricao-prazo-entrega").datepicker("destroy");
    $(".prescricao-prazo-entrega").datepicker({
      minDate: inicioG,
      maxDate: fimG,
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1
    });

    $(".prescricao-inicio").each(function(){
      $(this).get(0).value = $("input[name=inicio]").val();
    });
  });

  // REINICIA TODOS OS DATEPICKERS "FIM" DA PAGINA CASO HAJA ALGUMA ALTERAÇÃO NA DATA DA PRESCRIÇÃO
  $("input[name=fim]").live('change',function(){
    fimG = new Date($(this).val().split("/").reverse().join(","));
    $(".cp-fim").datepicker("destroy");
    $(".cp-fim").datepicker({
      maxDate: fimG,
      minDate: inicioG,
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1
    });

    $(".cp-inicio").datepicker("destroy");
    $(".cp-inicio").datepicker({
      minDate: inicioG,
      maxDate: fimG,
      changeMonth: true,
    });

    $(".prescricao-prazo-entrega").datepicker("destroy");
    $(".prescricao-prazo-entrega").datepicker({
      minDate: inicioG,
      maxDate: fimG,
      changeMonth: true,
      numberOfMonths: 1
    });

    $(".prescricao-fim").each(function(){
      $(this).get(0).value = $("input[name=fim]").get(0).value;
    });
  });


  // REDIRECIONAMENTOS
  $('.visualizar-presc-curativo').click(function(){
    var idPresc = $(this).attr('data-id-prescricao');
    window.location.href ='?action=visualizar-prescricao-curativo&prescricao='+idPresc;
  });

  $('.editar-presc-curativo').click(function(){
    var idPresc = $(this).attr('data-id-prescricao');
    window.location.href ='?action=editar-prescricao-curativo&prescricao='+idPresc;
  });

  $('.usar-nova-presc-curativo').click(function(){
    var idPresc = $(this).attr('data-id-prescricao');
    window.location.href ='?action=usar-nova-prescricao-curativo&prescricao='+idPresc;
  });

  $('.cancelar-presc-curativo').click(function(){
    var idPresc = $(this).attr('data-id-prescricao');
    if(confirm('Deseja realmente cancelar essa prescrição?'))
      window.location.href ='?action=cancelar-prescricao-curativo&prescricao='+idPresc;
  });

  $('#impressao-planserv').click(function(){
    var idPresc = $(this).attr('data-id-prescricao');
    window.open('?action=impressao-planserv&prescricao='+idPresc, '_blank');
  });



  // REGRA PARA MOSTRAR LADO DA FERIDA
  $("select.local-ferida").live("change",function() {
    if($(this).children("option:selected").attr("lado") == "S"){
      $(this).parent().parent().children('div').eq(1).attr("style","display:inline");
    }else{
      $(this).parent().parent().children('div').eq(1).attr("style","display:none");
    }
  });

  // AUTOCOMPLETE DA COBERTURA PRIMARIA
  $("input.cp-buscar-curativo").autocomplete({
    source: function(request, response) {
      $.getJSON('../busca_brasindice.php', {
        term: request.term,
        tipo: 0,
        tipo_extra: 1
      }, response);
    },
    minLength: 3,
    select: function(event, ui) {
      $(this).attr('data-catalogo-id', ui.item.catalogo_id);
      $(this).attr('data-item-name', ui.item.value);
      $(this).val(ui.item.value);
      $(this).parent().parent().find('input.cp-qtd-item').focus();
      return false;
    }
  });

  // AUTOCOMPLETE DA COBERTURA SECUNDARIA
  $("input.cs-buscar-curativo").autocomplete({
    source: function(request, response) {
      $.getJSON('../busca_brasindice.php', {
        term: request.term,
        tipo: 1
      }, response);
    },
    minLength: 3,
    select: function(event, ui) {
      $(this).attr('data-catalogo-id', ui.item.catalogo_id);
      $(this).attr('data-item-name', ui.item.value);
      $(this).val(ui.item.value);
      $(this).parent().parent().find('input.cs-qtd-item').focus();
      return false;
    }
  });

  var incrementItem = 0;
  if($("#increment-item-curativos").length > 0) {
      incrementItem = $("#increment-item-curativos").val();
  }
  // REGRAS PARA ADICIONAR O ITEM DENTRO DA TABELA DE COBERTURAS PRIMARIAS
  $(".cp-adicionar-cobertura").live('click', function(){
    var buscar_curativo = $(this).parent().parent().find('.cp-buscar-curativo'),
      catalogo_id       = buscar_curativo.attr('data-catalogo-id'),
      item_name         = buscar_curativo.attr('data-item-name'),
      qtd               = $(this).parent().parent().find('.cp-qtd-item').val(),
      frequencia        = $(this).parent().parent().find('.cp-frequencia option:selected').val(),
      frequencia_qtd    = $(this).parent().parent().find('.cp-frequencia option:selected').attr('frequencia-qtd'),
      frequencia_texto  = $(this).parent().parent().find('.cp-frequencia option:selected').text(),
      obs               = $(this).parent().parent().find('.cp-obs').val(),
      inicio            = $(this).parent().parent().find('.cp-inicio').val(),
      fim               = $(this).parent().parent().find('.cp-fim').val(),
      cp_table          = $(this).parent().parent().find('.cp-table-itens'),
      aprazamento       = '',
      data_ord          = $(this).parent().parent().parent().attr('data-ord'),
      i                 = 1;

    if((catalogo_id != '' || item_name != '') && qtd != '' && frequencia != -1) {
      for (i; i <= frequencia_qtd; i++) {
        aprazamento = aprazamento + '<input type="time" name="cp_aprazamento[' + data_ord + '][' + incrementItem + '][' + catalogo_id + '][]" class="cp_aprazamento OBG" value="00:00"> ';
      }

      cp_table.find('tbody').append(
        '<tr>' +
        '<td>' +
        '<img class="rem-item-presc-cobertura" src="../../../utils/delete_16x16.png" title="Remover" border="0"> ' +
        item_name +
        ' - <b>Frequência: ' + frequencia_texto + '</b>' +
        (obs != '' ? '<img src="/utils/info_14x14.png" title="' + obs +'" class="img-obs" style="position: relative; float: right;">' : '') +
        '<input type="hidden" name="cp_frequencia_item[' + data_ord + '][' + incrementItem + '][' + catalogo_id + ']" class="cp_frequencia_item" value="' + frequencia + '">' +
        '<input type="hidden" name="cp_catalogo_id[' + data_ord + '][' + incrementItem + '][]" class="cp_catalogo_id" value="' + catalogo_id + '">' +
        '<input type="hidden" name="cp_obs_item[' + data_ord + '][' + incrementItem + '][' + catalogo_id + ']" class="cp_obs_item" value="' + obs + '">' +
        '</td>' +
        '<td>' +
        '<input type="text" name="cp_inicio_item[' + data_ord + '][' + incrementItem + '][' + catalogo_id + ']" value="' + inicio + '" maxlength="10" size="10" class="cp_inicio_item periodo prescricao-inicio" readonly="">' +
        ' a ' +
        '<input type="text" name="cp_fim_item[' + data_ord + '][' + incrementItem + '][' + catalogo_id + ']" value="' + fim + '" maxlength="10" size="10" class="cp_fim_item periodo prescricao-fim" readonly="">' +
        '</td>' +
        '<td> <input type="text" name="cp_qtd[' + data_ord + '][' + incrementItem + '][' + catalogo_id + ']" class="cp_qtd" size="4" value="' + qtd + '"></td>' +
        '<td>' + aprazamento + '</td>' +
        '</tr>'
      );

      $(".prescricao-inicio").datepicker({
        defaultDate: "+1w",
        minDate: inicioG,
        maxDate: fimG,
        changeMonth: true,
        numberOfMonths: 1
      });
      $(".prescricao-fim").datepicker({
        minDate: inicioG,
        maxDate: fimG,
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1
      });

      buscar_curativo.val('');
      buscar_curativo.attr('data-catalogo-id', '');
      buscar_curativo.attr('data-item-name', '');
      $(this).parent().parent().find('.cp-qtd-item').val('');
      $(this).parent().parent().find('.cp-obs').val('');
      $(this).parent().parent().find('.cp-inicio').val(inicio);
      $(this).parent().parent().find('.cp-fim').val(fim);
      $(this).parent().parent().find('.cp-frequencia').val('-1');
      $(".img-obs").easyTooltip();
      incrementItem++;
    }else{
      alert('Por favor, preencha todos os campos corretamente');
    }
  });

  // REGRAS PARA ADICIONAR O ITEM DENTRO DA TABELA DE COBERTURAS SECUNDARIAS
  $(".cs-adicionar-cobertura").live('click', function(){
    var buscar_curativo = $(this).parent().parent().find('.cs-buscar-curativo'),
      catalogo_id       = buscar_curativo.attr('data-catalogo-id'),
      item_name         = buscar_curativo.attr('data-item-name'),
      qtd               = $(this).parent().parent().find('.cs-qtd-item').val(),
      frequencia        = $(this).parent().parent().find('.cs-frequencia option:selected').val(),
      frequencia_qtd    = $(this).parent().parent().find('.cs-frequencia option:selected').attr('frequencia-qtd'),
      frequencia_texto  = $(this).parent().parent().find('.cs-frequencia option:selected').text(),
      obs               = $(this).parent().parent().find('.cs-obs').val(),
      inicio            = $(this).parent().parent().find('.cs-inicio').val(),
      fim               = $(this).parent().parent().find('.cs-fim').val(),
      cs_table          = $(this).parent().parent().find('.cs-table-itens'),
      aprazamento       = '',
      data_ord          = $(this).parent().parent().parent().attr('data-ord'),
      i                 = 1;

    if((catalogo_id != '' || item_name != '') && qtd != '' && frequencia != -1) {
      for (i; i <= frequencia_qtd; i++) {
        aprazamento = aprazamento + '<input type="time" name="cs_aprazamento[' + data_ord + '][' + incrementItem + '][' + catalogo_id + '][]" class="cs_aprazamento OBG" value="00:00"> ';
      }

      cs_table.find('tbody').append(
        '<tr>' +
        '<td>' +
        '<img class="rem-item-presc-cobertura" src="../../../utils/delete_16x16.png" title="Remover" border="0">' +
        item_name +
        ' - <b>Frequência: ' + frequencia_texto + '</b>' +
        (obs != '' ? '<img src="/utils/info_14x14.png" title="' + obs +'" class="img-obs" style="position: relative; float: right;">' : '') +
        '<input type="hidden" name="cs_frequencia_item[' + data_ord + '][' + incrementItem + '][' + catalogo_id + ']" class="cs_frequencia_item" value="' + frequencia + '">' +
        '<input type="hidden" name="cs_catalogo_id[' + data_ord + '][' + incrementItem + '][]" class="cs_catalogo_id" value="' + catalogo_id + '">' +
        '<input type="hidden" name="cs_obs_item[' + data_ord + '][' + incrementItem + '][' + catalogo_id + ']" class="cs_obs_item" value="' + obs + '">' +
        '</td>' +
        '<td>' +
        '<input type="text" name="cs_inicio_item[' + data_ord + '][' + incrementItem + '][' + catalogo_id + ']" value="' + inicio + '" maxlength="10" size="10" class="cs_inicio_item periodo prescricao-inicio" readonly="">' +
        ' a ' +
        '<input type="text" name="cs_fim_item[' + data_ord + '][' + incrementItem + '][' + catalogo_id + ']" value="' + fim + '" maxlength="10" size="10" class="cs_fim_item periodo prescricao-fim" readonly="">' +
        '</td>' +
        '<td> <input type="text" name="cs_qtd[' + data_ord + '][' + incrementItem + '][' + catalogo_id + ']" class="cs_qtd" size="4" value="' + qtd + '"></td>' +
        '<td>' + aprazamento + '</td>' +
        '</tr>'
      );

      $(".prescricao-inicio").datepicker({
        defaultDate: "+1w",
        minDate: inicioG,
        maxDate: fimG,
        changeMonth: true,
        numberOfMonths: 1
      });
      $(".prescricao-fim").datepicker({
        minDate: inicioG,
        maxDate: fimG,
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1
      });

      buscar_curativo.val('');
      buscar_curativo.attr('data-catalogo-id', '');
      buscar_curativo.attr('data-item-name', '');
      $(this).parent().parent().find('.cs-qtd-item').val('');
      $(this).parent().parent().find('.cs-obs').val('');
      $(this).parent().parent().find('.cp-inicio').val(inicio);
      $(this).parent().parent().find('.cp-fim').val(fim);
      $(this).parent().parent().find('.cs-frequencia').val('-1');
      $(".img-obs").easyTooltip();
      incrementItem++;
    }else{
      alert('Por favor, preencha todos os campos corretamente');
    }
  });

  // REMOVER UM ITEM DAS TABELAS DE COBERTURAS
  $('.rem-item-presc-cobertura').live('click',function(){
    $(this).parent().parent().remove();
  });

  var j = $("#j").val();

  //REMOVE UMA FERIDA
  $(".remover-ferida").live('click', function () {
    if($("div.feridas").length > 1) {
      var d = $(this).parent().parent().attr('data-ord');
      $('hr[data-ord=' + d + ']').remove();
      $(this).parent().parent().parent().remove();
    }
  });

  //ADICIONA UMA FERIDA E RESETA TOD.O O FORMULARIO (AUTOCOMPLETES, DATEPICKERS, ETC)
  $(".adicionar-ferida").live('click', function () {
    j++;
    novoCampo = $("div.feridas:first").clone();
    novoCampo.prepend('<hr data-ord="' + j + '" style="border: 1px dashed #000; margin: 25px 0 25px 0;">');
    novoCampo.attr('data-ord', j);
    novoCampo.find('.local-ferida').val('-1');
    novoCampo.find('.lado-ferida').val('-1');
    novoCampo.find('.justificativa').val('');
    novoCampo.find('.prescricao-inicio, .prescricao-fim').removeAttr('id').removeClass('hasDatepicker');
    novoCampo.find('table.mytable > tbody').html('');
    novoCampo.insertAfter("div.feridas:last");

    $(".prescricao-inicio").datepicker("destroy");
    $(".prescricao-inicio").datepicker({
      defaultDate: "+1w",
      minDate: inicioG,
      maxDate: fimG,
      changeMonth: true,
      numberOfMonths: 1
    });
    $(".prescricao-fim").datepicker("destroy");
    $(".prescricao-fim").datepicker({
      minDate: inicioG,
      maxDate: fimG,
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1
    });

    $("input.cp-buscar-curativo").autocomplete({
      source: function(request, response) {
        $.getJSON('../busca_brasindice.php', {
          term: request.term,
          tipo: 0,
          tipo_extra: 1
        }, response);
      },
      minLength: 3,
      select: function(event, ui) {
        $(this).attr('data-catalogo-id', ui.item.catalogo_id);
        $(this).attr('data-item-name', ui.item.value);
        $(this).val(ui.item.value);
        $(this).parent().parent().find('input.cp-qtd-item').focus();
        return false;
      }
    });

    $("input.cs-buscar-curativo").autocomplete({
      source: function(request, response) {
        $.getJSON('../busca_brasindice.php', {
          term: request.term,
          tipo: 1
        }, response);
      },
      minLength: 3,
      select: function(event, ui) {
        $(this).attr('data-catalogo-id', ui.item.catalogo_id);
        $(this).attr('data-item-name', ui.item.value);
        $(this).val(ui.item.value);
        $(this).parent().parent().find('input.cs-qtd-item').focus();
        return false;
      }
    });
  });

  // FIM SM-879
    
    $("#nova_ferida").click(function() {
        var contador = $("#tabela"+contador).attr("contador") + 1;
        $.post("paciente_enf.php", {
            query: "feridas", contador: contador, fromRelatorio: $("#fromRelatorio").val()
        }, function(r) {

            $("#combo-div-quadro-feridas").append(r);
            $(".chosen-select-ferida").chosen({search_contains: true});
            
        });

        return false;
    });
    
    $("#ferida_prorrogacao").click(function(){
        $.post("relatorios.php", {
            query: "quadro-lesoes",
            fromRelatorio: $("#fromRelatorio").val()
        }, function(feridas) {
            $("#quadro-lesoes").append(feridas);            
        });  
        return false;
    });    
    
    $(".del_ferida_prorrogacao").live("click",function(){
        $(this).closest("div").hide();
        $(this).closest("div").removeClass("div-quadro-feridas");
        $(this).closest("div").addClass("ferida_excluida");
        });
    $(".del_ferida_avaliacao").live("click",function(){
        $(this).closest("div").remove();
    });
    $('#selPaciente').chosen({search_contains: true});
  $('#sel-carater').chosen();
    
  //22-01-2015 solicitacao-aditivo de enfermagem
  $("#solicitar-item-relatorio-aditivo").click(function(){
      $("#div-adicionar-item-aditivo").dialog('open');
      $("#combo-rel-aditivo-frequencia").hide();
      if($("#tipos-item-solicitacao-aditivo input[name='tipos-solicitacao-aditivo']") == 'serv') {
          $("#combo-rel-aditivo-frequencia").show();
      }
  });
  $("#div-adicionar-item-aditivo").dialog({
        autoOpen: false,
        modal: true,
        width: 800,
        position: 'top',
        open: function(event, ui) {
            $("input[name='busca-item-solicitacao-aditivo']").val('');
            $('#nome').val('');
            $('#nome').attr('readonly', 'readonly');
            $('#apr').val('');
            $('#apr').attr('readonly', 'readonly');
            $('#cod').val('');
            $('#qtd').val('');
            $("input[name='busca-item-solicitacao-aditivo']").focus();
        },
        buttons: {
            "Fechar": function() {
               
                $(this).dialog("close");
            }
        }
    });
    $("#tipos-item-solicitacao-aditivo").buttonset();
    $("#tipos-item-solicitacao-aditivo input[name='tipos-solicitacao-aditivo']").change(function() {
        $("#div-adicionar-item-aditivo").attr('tipo', $(this).val());
        $("#combo-rel-aditivo-frequencia").hide();
        $("#combo-rel-aditivo-qtd").show();
        $("#frequencia-rel-aditivo").val('');
        $("#qtd").addClass('OBG');
        $("#frequencia-rel-aditivo").removeClass('OBG');
        if($(this).val() == 'serv') {
            $("#qtd").removeClass('OBG').val('0');
            $("#combo-rel-aditivo-qtd").hide();
            $("#combo-rel-aditivo-frequencia").show();
            $("#frequencia-rel-aditivo").addClass('OBG');
        }
    });
    
    $("input[name='busca-item-solicitacao-aditivo']").autocomplete({
        source: function(request, response) {
            $.getJSON('busca_brasindice.php', {
                term: request.term,
                tipo: $("#div-adicionar-item-aditivo").attr("tipo")
            }, response);
        },
        minLength: 3,
        select: function(event, ui) {
            $('#busca-item').val('');
            $('#nome').val(ui.item.value);
            $('#catalogo_id').val(ui.item.catalogo_id);
            $('#cod').val(ui.item.id);
            $('#qtd').focus();
            $('#tipo').val(ui.item.tipo);
            $('#busca-item').val('');
            $('#valor-venda').val(ui.item.valor);
            $('#categoria').val(ui.item.categoria);
            return false;
        },
        extraParams: {
            tipo: 2
        }
    });
    $("#add-item-solicitacao-aditivo").click(function() {
                if (validar_campos("div-adicionar-item-aditivo")) {
                    var cod = $("#cod").val();
                    var catalogo_id = $("#div-adicionar-item-aditivo input[name='catalogo_id']").val();
                    var nome = $("#div-adicionar-item-aditivo input[name='nome']").val();
                    var item = $("#div-adicionar-item-aditivo").attr("item");
                    var qtd = $("#div-adicionar-item-aditivo input[name='qtd']").val();              
                    var frequencia = $("#div-adicionar-item-aditivo select[name='frequencia_rel_aditivo']") ? $("#div-adicionar-item-aditivo select[name='frequencia_rel_aditivo']").val() : 0;
                    var frequencia_text = $("#div-adicionar-item-aditivo select[name='frequencia_rel_aditivo'] :selected").text();
                    var tipo = $("#tipo").val();
                    var valorVenda = $("#valor-venda").val();
                    var categoria = $("#categoria").val();
                    var dados = "nome='" + nome + "' tipo='" + tipo+ "' valor-venda='" + valorVenda+ "' categoria='" + categoria
                                 + "' tiss='" + cod + "' qtd='" + qtd+ "'"
                        + " catalogo-id='" + catalogo_id +"' qtd-kit='0' id-kit='0' id-solicitacao-rel-aditivo='0'" + "' frequencia='" + frequencia+ "'" ;
                    var comp_qtd_freq = qtd > 0 ? ". Quantidade: " + qtd : ". Frequencia: " + frequencia_text;
                    var linha = nome + comp_qtd_freq;
                    
                    var botoes = "<img align='right' class='remover-item-solicitacao' "
                            + "src='../utils/delete_16x16.png' title='Remover' border='0' />";
                    
                    var cont_kit=0;
                   var  checkedImediato='';

                    if($("#entrega-imediata-aditivo").is(':checked')){
                        checkedImediato='checked';
                    }
                     var entregaImediata="<input type='checkbox' "+checkedImediato+" name='entrega-imediata-aditivo' class='entrega-imediata-aditivo'/><b>Entrega Imediata</b>";
                    
                    if(tipo == 2){
                        var entregaImediata="<input type='checkbox' "+checkedImediato+
                                             " name='entrega-imediata-aditivo'" +
                                             " class='entrega-imediata-aditivo kit-entrega-imediata-aditivo'/><b>Entrega Imediata</b>";
                        $('.kit').each(function(){
                              
                            if ($(this).attr('cod') == cod){
                                cont_kit++;
                            }
                            
                        });
                        var entregaImediata="<input type='checkbox' "+checkedImediato+
                            " name='entrega-imediata-aditivo'" +
                            " class='entrega-imediata-aditivo kit-entrega-imediata-aditivo' cod ='"+cod+"'/><b>Entrega Imediata</b>";
                        if (cont_kit >0){
                             alert(' Este Kit já foi adicionado, favor remover o existente antes de adicioná-lo novamente.')
                            
                        }else{                        
                            $.post("query.php", {query: "consultar-itens-kit", 
                                                 cod: cod,
                                                 qtd:qtd,
                                                 nome:nome,
                                      entregaImediata:entregaImediata
                                              },
                                             function(r) {
                                        $('#solicitar-itens-aditivo').append(r);
                                   });
                            }
                        
                    }else{
                     $("#solicitar-itens-aditivo").append(
                            "<tr bgcolor='#ffffff' class='dados' "
                            + dados + " ><td>" + linha +' '+entregaImediata
                            + botoes + "</td></tr>");
                    }
                    $("#cod").val("-1");
                    $("#tipo").val("-1");
                    $("#div-adicionar-item-aditivo input[name='nome']").val("");
                    $("#div-adicionar-item-aditivo input[name='qtd']").val("");
                    $("#div-adicionar-item-aditivo select[name='frequencia_rel_aditivo']").val("");
                    $('#busca-item-solicitacao-aditivo').val('');
                    $("#valor-venda").val('');
                    $("#categoria").val('');
                    $("#entrega-imediata-aditivo").attr('checked',false);
                    exitPage.needToConfirm = true;
                }
                return false;
    });
            
     $(".remover-item-solicitacao").live('click',function(){
          $(this).parent().parent().remove();
         
     });
     
     $(".remover-item-solicitacao-kit").live('click',function(){
            var cod = $(this).parent().parent().attr('cod');           
          
            if (confirm("Deseja realmente remover?")) {
                  $("#solicitar-itens-aditivo").find("tr[cod=" + cod + "]").each(function() {
                        $(this).remove();

                  });
              }
         
     });
     
     $('#confirmar-rel-aditivo').click(function(){
         if (confirm("Deseja realmente finalizar o Relatório Aditivo?")) {
             var id_paciente= $(this).attr('id-paciente');
             $.post("query.php", {query: "confirmar-relatorio-aditivo", 
                                    id_paciente: $(this).attr('id-paciente'),
                                    id_relatorio_aditivo: $(this).attr('id-relatorio-aditivo')}, 
                                function(r) {
                                        if(r == 1){
                                            alert('O Relatório Aditivo foi confirmado.');
                                            window.location.href = '?view=relatorio&opcao=2&id=' + id_paciente;
                                        }else{
                                            alert(' Erro ao confirmar Relatório Aditivo.');
                                        }
                                   });
         }
     });
    $('.kit-entrega-imediata-aditivo').live ('click',function(){
           var cod = $(this).attr('cod');
           var atributo = false;
           if($(this).is(':checked')){
                atributo = true;
           }
        $('.kit-entrega-imediata-aditivo').each(function(){
            if($(this).attr('cod') == cod ){
                $(this).attr('checked',atributo);
            }
        });

    });

    $(".inicio-periodo").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        onClose: function (selectedDate) {
            $(".fim-periodo").datepicker(
                'option',
                'minDate',
                selectedDate
            )
        }
    });
    $(".fim-periodo").datepicker({
        minDate: $(".inicio-periodo").val(),
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1
    });

    $("#buscar-prescricoes-enf").click(function(){
        if(validar_campos('div-gerenciar-prescricao-enf')){
            var idPaciente=$('#selPaciente').val();
            var inicio  = $('input[name=inicio]').val().split('/').reverse().join('-');
            var fim = $('input[name=fim]').val().split('/').reverse().join('-');
            var carater = $("#carater").val();

            $('#form-gerenciar-presc-enf').submit();
        }
    });

    $("#nova-prescricao-enf").click(function(){
        if(validar_campos('div-gerenciar-prescricao-enf')){
           var paciente = $('#selPaciente').val();
           var inicio = $('.inicio-periodo').val();
           var fim = $('.fim-periodo').val();
            var carater = $("#carater").val();

            if(paciente == -1 || paciente == null ){
                alert('Selecione o paciente.');
                return false;
            }

            if(carater == -1 || carater == null ){
                alert('Selecione o caráter.');
                return false;
            }

            window.location.href ='?action=nova-presc&paciente='+paciente+'&inicio='+inicio+'&fim='+fim+'&carater='+carater;


        }
        return false;
    });

  $("#nova-prescricao-curativo").click(function(){

    if(validar_campos('div-gerenciar-prescricao-enf')){
      var paciente = $('#selPaciente').val();
      var inicio = $('.inicio-periodo').val();
      var fim = $('.fim-periodo').val();

      if(paciente == -1 || paciente == null ){
        alert('Selecione o paciente.');
        return false;
      }
      window.location.href ='?action=nova-presc-curativo&paciente='+paciente+'&inicio='+inicio+'&fim='+fim;
    }
    return false;
  });

    $(".add-cuidado-presc-enf").click(function(){
        var grupo = $(this).attr('data-grupo');
        var cuidado = $(this).parent().parent().find('#sel-grupo0'+grupo+'-cuidado').val();
        var frequencia = $(this).parent().parent().find('#sel-grupo0'+grupo+'-frequencia').val();
        var aprazamento = $(this).parent().parent().find('#sel-grupo0'+grupo+'-frequencia').children('option:selected').attr('data-qtd');
       $(this).parent().parent().find('#sel-grupo0'+grupo+'-cuidado').find('option').each(function(){
            if($(this).val() != cuidado){
                $(this).removeAttr('selected');
            }
        });
        $(this).parent().parent().find('#sel-grupo0'+grupo+'-cuidado').children('option:selected').attr('defaultSelected',true);

        $(this).parent().parent().find('#sel-grupo0'+grupo+'-frequencia').find('option').each(function(){
            if($(this).val() != frequencia){
                $(this).removeAttr('selected');
            }
        });
        $(this).parent().parent().find('#sel-grupo0'+grupo+'-frequencia').children('option:selected').attr('defaultSelected',true);
        var qtdAprazamento =$(this).parent().parent().find('#sel-grupo0'+grupo+'-frequencia option:selected').attr('data-qtd');
        var sqlFrequencia= $(this).parent().parent().find('#sel-grupo0'+grupo+'-frequencia').parent().html();
        var sqlCuidado = $(this).parent().parent().find('#sel-grupo0'+grupo+'-cuidado').parent().html();
       // var dataPresc = $('#data-presc-enf').clone();
        var inicio = $('.inicio-periodo').val();
        var fim = $('.fim-periodo').val();
        var remove = "<img align='lefth' class='rem-item-presc-enf' src='../../../utils/delete_16x16.png' title='Remover' border='0'>";
         sqlFrequencia = sqlFrequencia.replace('id="sel-grupo0'+grupo+'-frequencia"','name="sel-frequencia" class="COMBO_OBG"');
         sqlCuidado = sqlCuidado.replace('id="sel-grupo0'+grupo+'-cuidado"','name="sel-cuidado" class="COMBO_OBG"');
        var html = '';
        if(cuidado == null || cuidado == -1 || frequencia == null || frequencia == -1 ){
            alert("Escolha o cuidado e a frequencia para poder adicionar o item.");
            return false;
        }else {
            if (aprazamento > 0 && aprazamento != null && aprazamento != '') {
                html = "<div class='aprazamento' item=''>" +
                "<span><b>Aprazamento: </b></span></br>" +
                "<span>";
                var freq = 1;
                for (i = 1; i <= aprazamento; i++) {
                    html += "<input type='time' class='horaAprazamento OBG' size='4' maxlength='5' />&nbsp;";
                    if (i % 10 == 0) {
                        html += '</span></br></br><span>';
                    }
            }

            html += "</span></div>";
        }

           $('#div-grupo-'+grupo).append("</br><div class='dados'>" +
           "<span>"+remove+"</span>" +
           "<span>"+sqlCuidado+"</span>" +
            "<span>"+sqlFrequencia+"</span>" +
             "<span><label>De </label><input class='data COMBO_OBG' name='inicio-item' value='"+inicio+"'>" +
                    "<label>Até</label> <input class='data COMBO_OBG' name='fim-item' value='"+fim+"'>" +
           "</span>"+html+"</div>");
             $(this).parent().parent().find('#sel-grupo0'+grupo+'-cuidado').val('');
             $(this).parent().parent().find('#sel-grupo0'+grupo+'-frequencia').val('');

            $('.data').datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1
            });


        }
    });

    $('.rem-item-presc-enf').live('click',function(){
        $(this).parent().parent().remove();
    });
    $('select[name=sel-frequencia]').live('change',function(){
        var aprazamento =$(this).children('option:selected').attr('data-qtd');
        var html ='';
        $(this).parent().parent().find('.aprazamento').remove();
        if (aprazamento > 0 && aprazamento != null && aprazamento != '') {
            html = "<div class='aprazamento' item=''>" +
            "<span><b>Aprazamento: </b></span></br>" +
            "<span>";
            var freq = 1;
            for (i = 1; i <= aprazamento; i++) {
                html += "<input type='time' class='horaAprazamento OBG' size='4' maxlength='5' />&nbsp;";
                if (i % 10 == 0) {
                    html += '</span></br></br><span>';
                }
            }

            html += "</span></div>";
        }
        $(this).parent().parent().append(html);

    });
    $('#salvar-prescricoes-enf').click(function(){
        var contDados = 0;
        var idPaciente = $('#idPaciente').val();
        var inicioPrescricao = $('#div-prescrever-cuidados').find("input[name='inicio']").val();
        inicioPrescricao =inicioPrescricao.split('/').reverse().join('-');
        var fimPrescricao = $('#div-prescrever-cuidados').find("input[name='fim']").val();
        fimPrescricao =fimPrescricao.split('/').reverse().join('-');
        var carater= $('#carater').val();
        var erro=0;
        var erro1=0;
        var totalDados = $('.dados').size();
        var itens='';
        var empresa=$(this).attr('empresa-id');


        $('.dados').each(function(i){
            var idPrescCuidado = $(this).find("select[name='sel-cuidado']").val();
            var textoPrescCuidado =$(this).find("select[name='sel-cuidado'] option:selected").text();
            var idFrequencia = $(this).find("select[name='sel-frequencia']").val();
            var textoFrequencia =$(this).find("select[name='sel-frequencia'] option:selected").text();
            var inicioItem = $(this).find("input[name='inicio-item']").val();
            var fimItem = $(this).find("input[name='fim-item']").val();

            inicioItem = inicioItem.split('/').reverse().join('-');
            fimItem = fimItem.split('/').reverse().join('-');
            var linha ='';
            var aprazamento ='';
            $(this).find('.aprazamento').find('input').each(function(){

                aprazamento += $(this).val()+" ";

            });
           if (inicioItem > fimItem){
               erro++;
           }
            if(inicioItem < inicioPrescricao || inicioItem > fimPrescricao || fimItem > fimPrescricao  ){
                erro1++;

            }
             linha= idPrescCuidado+'#'+textoPrescCuidado+'#'+idFrequencia+'#'+textoFrequencia+'#'+inicioItem+'#'+fimItem+'#'+aprazamento;
            if(i+1 <totalDados){
                linha+='$';
            }
            itens +=linha;
            contDados++;
        });
        if(erro !=0){
            alert('A data inicio de um item não pode ser maior que sua data fim.');
            return false;
        }
        if(erro1 !=0){
            alert('As datas inico e fim devem estar dentro do período da prescrição.');
            return false;
        }
        if(contDados==0){
            alert("A prescrição está vazia. Insira um item.");
            return false;
        }
       
        if(validar_campos('div-prescrever-cuidados')){
            $.post("index.php", {
                query:'salvar-presc-enfermagem',
                dados:itens,
                idPaciente:idPaciente,
                inicio:inicioPrescricao,
                fim:fimPrescricao,
                carater:carater
            },function(r){
                if(r != '0'){
                    $("#dialog-imprimir-prescricao-cuidados").dialog('close');
                    $("#dialog-imprimir-prescricao-cuidados").dialog({
                        autoOpen: false,
                        modal: true,
                        width: 500,
                        position: 'middle',
                        buttons: {
                            "Sim": function() {
                                window.open('/enfermagem/imprimirPrescEnf.php?prescricao=' + r+'&empresa='+empresa, '_blank');
                                $(this).dialog("close");
                                window.location.href ='?action=prescricao';
                            },
                            "Não": function() {
                                window.location.href ='?action=prescricao';
                            }
                        }
                    });
                    $("#dialog-imprimir-prescricao-cuidados").dialog('open');
                }else{
                    alert('Erro ao inserir prescrição.');
                }
            });
            return false;
        }
        return false;
    });

    $('.visualizar-presc-enf').click(function(){
        var idPresc= $(this).attr('data-id-prescricao');
        window.location.href ='?action=visualizar&idPresc='+idPresc;

    });


    $('.usar-para-nova-presc-enf').click(function(){

        var idPresc= $(this).attr('data-id-prescricao');
        $("#dialog-usar-para-nova-prescricao").attr('id-presc',idPresc);
        $("#dialog-usar-para-nova-prescricao").dialog('open');

      //  window.location.href ='?action=usar-para-nova-prescricao&idPresc='+idPresc;

    });

    $('#imprimir-presc-enf').click(function(){
        var idPresc= $(this).attr('data-id-prescricao');
        window.location.href ='?action=imprimir&idPresc='+idPresc;

    });

    //modifica a class do campo prazo maximo de entrga na prescrição curativo
    $('#checkbox-entrega-imediata').click(function(){
        if($(this).is(':checked')){
            $('#dataEntregaImediata').addClass('OBG');
            $('#dataEntregaImediata').removeAttr('style');
            $('#data-avulsa').removeClass('OBG');
            $('#data-avulsa').attr('style','display:none;');
            $('#data-avulsa').val('');
        }else{
            $('#dataEntregaImediata').removeClass('OBG');
            $('#dataEntregaImediata').attr('style','display:none;');
            $('#data-avulsa').addClass('OBG');
            $('#data-avulsa').removeAttr('style');
            $('#dataEntregaImediata').val('');

        }

    });
    $("#dialog-usar-para-nova-prescricao").dialog('close');
    $("#dialog-usar-para-nova-prescricao").dialog({
        autoOpen: false,
        modal: true,
        width: 500,
        position: 'top',

        buttons: {
            "Fechar": function() {
                $("#dialog-usar-para-nova-prescricao").attr('id-presc','');
                $("#fim-div").val('');
                $("#inicio-div").val('');

                $(this).dialog("close");
            },
            "Continuar": function() {
                if(validar_campos('dialog-usar-para-nova-prescricao')){

                    var idPresc= $('#dialog-usar-para-nova-prescricao').attr('id-presc');
                    var inicio =  $("#inicio-div").val();
                    var fim =       $("#fim-div").val();
                    window.location.href ='?action=usar-para-nova-prescricao&idPresc='+idPresc+'&ini='+inicio+'&fim='+fim;

                }
                return false;

            }
        }
    });
    $('#tabela-detalhe-prescricao').hide();
    $('#visulaizar-detalhe-prescricao').live('click',function(){


        if($(this).attr('visualizar') == 0){

            $(this).attr('visualizar',1);
            $('#tabela-detalhe-prescricao').show();


        }else{

            $(this).attr('visualizar',0);
            $('#tabela-detalhe-prescricao').hide();

        }


    });




    
});
