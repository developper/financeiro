<?php

$id = session_id();
if (empty($id))
    session_start();
require __DIR__ . '/../vendor/autoload.php';
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');

use App\Models\Enfermagem\ProcedimentosGtm;
use App\Models\Administracao\Paciente;

class ImprimirProcedimentosGtm
{
    private function assinaturaProResponsavel($medico)
    {
        $sql_assinatura = "
                SELECT
                    usuarios.ASSINATURA
                FROM                    
                    usuarios
                WHERE
                    idUsuarios = {$medico}";
        $result_assinatura = mysql_query($sql_assinatura);
        $assinatura = mysql_result($result_assinatura, 0, 0);
        return "<br/><br/><table width='100%'><tr><td><center><img src='{$assinatura}' width='20%'></center></td></tr></table>";
    }

    private function procedimentos ($response)
    {
        $tipos = [
            '1' => 'BUTTON',
            '2' => 'SONDA DE FOLEY',
            '3' => 'SONDA DE GTM'
        ];
        $motivos = [
            '1' => 'MANUSEIO INDEVIDO',
            '2' => 'ALTERA&Ccedil;&Atilde;O CL&Iacute;NICA DO PACIENTE',
            '3' => 'TRA&Ccedil;&Atilde;O',
            '4' => 'OUTRO'
        ];
        $htm = "";
        foreach ($response as $data) {
            $dataTroca = \DateTime::createFromFormat('Y-m-d', $data['data_troca'])->format('d/m/Y');
            $data['item'] = htmlentities($data['item']);
            $data['marca'] = htmlentities($data['marca']);
            $data['dano'] = htmlentities($data['dano']);
            $data['descricao_origem_dano'] = htmlentities($data['descricao_origem_dano']);

            $htm .= <<<HTML
            <br><br>
    <table width='100%' style='border:2px solid;'>
        <tr style='background-color:#EEEEEE;'>
            <td colspan='2' align='center'><b>INFORMA&Ccedil;&Otilde;ES SOBRE O PROCEDIMENTO</b></td>
        </tr>
        <tr>
            <td width='20%'>ITEM: </td>
            <td width='80%'>{$data['item']}</td>
        </tr>
        <tr style='background-color:#EEEEEE;'>
            <td width='20%'>DATA DA TROCA:</td>
            <td width='80%'>{$dataTroca}</td>
        </tr>
        <tr>
            <td width='20%'>TIPO:</td>
            <td width='80%'>{$tipos[$data['tipo']]}</td>
        </tr>
HTML;
            if ($data['tipo'] == '1') {
                $htm .= <<<HTML
        <tr style='background-color:#EEEEEE;'>
            <td width="20%">FURO:</td>
            <td width="80%">{$data['furo']}</td>
        </tr>
        <tr>
            <td width="20%">CM:</td>
            <td width="80%">{$data['cm']}</td>
        </tr>
HTML;
            } else {
                $htm .= <<<HTML
        <tr style='background-color:#EEEEEE;'>
            <td width = "20%" > FR:</td >
            <td width = "80%" >{$data['fr']}</td >
        </tr >
HTML;
            }

            $htm .= <<<HTML
        <tr>
            <td width="20%">MARCA DO PRODUTO DANIFICADO:</td>
            <td width="80%">{$data['marca']}</td>
        </tr>
        <tr style='background-color:#EEEEEE;'>
            <td width="20%">DESCRI&Ccedil;&Atilde;O DO DANO:</td>
            <td width="80%">{$data['dano']}</td>
        </tr>
        <tr>
            <td width="20%">ORIGEM DO DANO:</td>
            <td width="80%">{$motivos[$data['origem_dano']]}</td>
        </tr>
        <tr style='background-color:#EEEEEE;'>
            <td width="20%">DETALHES DA ORIGEM DO DANO:</td>
            <td width="80%">{$data['descricao_origem_dano']}</td>
        </tr>
    </table>
HTML;
        }
        return $htm;
    }

    public function procedimentos_gtm($id)
    {
        $htm = "";
        $response = ProcedimentosGtm::getByIdPrescricao($id);
        $cabecalho = Paciente::getCabecalho($response[0]['paciente']);
        $htm .= <<<HTML
<h2><a><img src='../utils/logo2.jpg' width='150' ></a> Procedimentos em Gastrostomia</h2>
<table width=100% style='border:2px dashed;'>
    <tr>
        <td colspan='2'><label style='font-size:14px;color:#8B0000'><b><center> INFORMA&Ccedil;&Otilde;ES DO PACIENTE </center></b></label></td>
    </tr>
    <tr style='background-color:#EEEEEE;'>
        <td width='70%'><label><b>PACIENTE </b></label></td>
        <td><label><b>SEXO </b></label></td>
    </tr>
    <tr>
        <td>{$cabecalho['paciente']}</td>
        <td>{$cabecalho['sexo']}</td>
    </tr>
    <tr style='background-color:#EEEEEE;'>
        <td width='70%'><label><b>IDADE </b></label></td>
        <td><label><b>UNIDADE REGIONAL </b></label></td>
    </tr>
    <tr>
        <td>{$cabecalho['nascimento']} {$cabecalho['idade']} Anos</td>
        <td>{$cabecalho['empresa']}</td>
    </tr>

    <tr style='background-color:#EEEEEE;'>
        <td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>
        <td><label><b>MATR&Iacute;CULA </b></label></td>
    </tr>
    <tr>
        <td>{$cabecalho['convenio']}</td>
        <td>{$cabecalho['matricula']}</td>
    </tr>
</table>
HTML;

        $htm .= $this->procedimentos($response);

        $htm .= $this->assinaturaProResponsavel($response[0]['criador']);

        $paginas[] = $htm;
        //die($htm);
        $mpdf = new mPDF('pt', 'A4', 10);
        $mpdf->SetHeader('página {PAGENO} de {nbpg}');
        $mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
        $mpdf->WriteHTML("<html><body>");
        $flag = false;
        foreach ($paginas as $pag) {
            if ($flag)
                $mpdf->WriteHTML("<formfeed>");
            $mpdf->WriteHTML($pag);
            $flag = true;
        }
        $mpdf->WriteHTML("</body></html>");
        $mpdf->Output('relatorio_procedimentos_gtm.pdf', 'I');
        exit;
    }

}

$p = new ImprimirProcedimentosGtm();
$p->procedimentos_gtm($_GET['id']);
