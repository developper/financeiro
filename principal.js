$(function($){

    $(".voltar_").click(function(event) {
        if(confirm('Deseja realmente sair dessa página?')){
            event.preventDefault();
            history.back(1);
        }
        return false;
    });
        if(!Modernizr.inputtypes.date){
             $('input[type=date]').datepicker({altFormat:"yy-mm-dd",dateFormat:"yy-mm-dd"});
        }

        function isNumeric(str) {   
            var er = /^[0-9]+$/;   
            return (er.test(str)); 
        } 
///////////////////////////20130-12-20 relogar
        var timer = null;
        var Day = new Date();
        function stop()
        {
            clearTimeout(timer);
        }
        
        start();
        
       
        function start()
        {
                timer = setInterval(function(i){
                        $('.ui-icon-closethick').css('display','none');
                        $('.ui-dialog-titlebar-close').css('display','none');

                        $.post("/login.php",
                              {
                                   query: "confere_tempo"
                              },function(retorno){
                                   var MinSec = retorno.split(":");
                                   var min = parseInt(MinSec[0]);
                                   var sec = parseInt(MinSec[1]);
                                   if(min<=0 && sec<=0)
                                   {
                                        stop();
                                        $("#dialog-relogar").dialog('open');
                                   }
                                   else
                                   {    
                                        var tempo = (min*60)+sec;
                                       // alert(tempo);
                                        shortly = new Date(); 
                                        // shortly.setMinutes(shortly.getMinutes()+min);
                                        shortly.setSeconds(shortly.getSeconds()+tempo); 
                                        $('#defaultCountdown').countdown('option', {until: shortly,onExpiry:start, format: 'MS'}); 
                                                  
                                        stop();
                                   }
                              }
                        );
                },100);
        }
        
        
        function trim(str) {
            return str.replace(/^\s+|\s+$/g,"");
        }

    $('.preview').live('click', function(){
        var $this   = $(this);
        var id      = $this.attr('id'),
            patient = $this.attr('patient'),
            tipoPrescricao = $this.attr('tipo-prescricao'),
            module  = $this.attr('module'),
            tipo    = $this.attr('tipo'),
            query   = 'preview-prescricao',
            periodo = $this.attr('inicio') + " a " + $this.attr('fim'),
            pendente = $this.attr('pendente');
            carater = $this.attr('carater');



        var $dialog = $("#dialog-preview-prescricao");
        var $dialogWrap = $("#dialog-preview-prescricao .wrap");

        $dialog.dialog({
            autoOpen: false,
            modal: true,
            width: 800,
            title: tipoPrescricao + " de " + periodo,
            buttons: {
                "OK": function () {
                    $(this).dialog("close");
                }
            }
        });

        $dialogWrap.html('');

        var sources = {
            'medico': 'query.php',
            'enfermagem': 'query.php'
        };

        $.ajax({
            type: "GET",
            url: sources[module],
            data: {query: query, id: id, tipo: tipo, pendente: pendente, carater:carater},
            success: function (response) {
                $dialogWrap.html(response);
                $dialog.dialog("open");
                $('#tabela-detalhe-prescricao').hide();
            },
            fail: function(xhr){
                console.log(xhr.responseText);
            }
        });

    });
    $('.escolher-empresa-gerar-documento').click(function () {
        var empresa = $(this).attr('empresa-relatorio');
        $('#select-escolher-empresa-gerar-documento').val(empresa);

        $('#dialog-escolher-empresa-gerar-documento').attr('caminho',$(this).attr('caminho'));
        $('#dialog-escolher-empresa-gerar-documento').dialog('open');
    });

        $('#dialog-escolher-empresa-gerar-documento').dialog({
            closeOnEscape: false ,
            autoOpen: false,
            modal: true,
            width: 400,
            focus:  function(){
                $(this).button;
            },
            open: function(event,ui){

            },
            buttons: {
                "Cancelar": function () {
                    $('#dialog-escolher-empresa-gerar-documento').dialog('close');
                    return false;
                },

                "Imprimir" : function(){
                    var $select = $('#select-escolher-empresa-gerar-documento');
                    if($select.val() == "-1"){

                        $select.css({"border":"3px solid red"});
                        return false;
                    } else{

                        var caminho =  $('#dialog-escolher-empresa-gerar-documento').attr('caminho');
                        caminho = caminho +"&empresa="+$select.val();
                        $select.css({"border":""});
                        window.open(caminho,'_blank');
                        $select.find('option:selected').attr('selected',false);
                        $('#dialog-escolher-empresa-gerar-documento').attr('caminho','');




                    }
                    $('#dialog-escolher-empresa-gerar-documento').dialog('close');

                }
            }
        });


    $("#dialog-relogar").dialog({
                closeOnEscape: false ,
	  	        autoOpen: false,
                modal: true,
                width: 400,
                focus:  function(){
                    $(this).button;
                },
	  	open: function(event,ui){
	  		
	  	},
	  	buttons: {
	  		
	  		"Logar" : function(){
	  			/*if(validar_campos('dialog-relogar')){*/
	  				var login = trim($("#login_relogar").text());
	  				var senha = $("#senha_relogar").val();
					
                                        $.post("/login.php",
                                            {
                                                 query: "relogar",
                                                 modal:1,
                                                 login:login,
                                                 senha:senha,
                                                 renovar:1
                                            },function(resp){
                                                   
                                              
                                                
                                                if(parseInt(resp)){
                                                  $("#dialog-relogar").dialog("close");
                                                  start();
                                                  shortly = new Date(); 
                                                 // shortly.setMinutes(shortly.getMinutes()+min);
                                                  shortly.setSeconds(shortly.getSeconds()+resp); 
                                                  $('#defaultCountdown').countdown('option', {until: shortly,onExpiry:start, format: 'MS'}); 
                                                  
                                                }
                                                else
                                                {
                                                     alert('Usuario ou senha Errado!');
                                                }
                                           
                                            }
                                      );
                                
	  				
	  			//}
	  		}
	  	}
	  });
	
  
  /* Coutdown */
  
  
	Day = new Date(Day.getFullYear(),Day.getMonth(),Day.getDate(),Day.getHours(),Day.getMinutes(),Day.getSeconds()+900);
       
	$('#defaultCountdown').countdown({until: Day,onExpiry:start, format: 'MS'});
	
 /* Fim Coutdown*/
  
  $(".data").datepicker({
	changeMonth: true,
	changeYear: true,
	numberOfMonths: 1
  });
  
  $('.boleano').each(function(i){
  	$(this).attr("size","1");
  	$(this).attr("maxlength","1");
  });
  
  $('.boleano').keyup(function(e){
  	var er = new RegExp("[^(s|n|S|N)]");
    this.value = this.value.replace(er,'');
  });
	
  function avisos(){
    $.post("query.php",{query: "avisos"},function(retorno){
    	if(retorno != "n"){
	 		$("#div-atualiza").html(retorno);
	 		$("#dialog-atualiza").dialog("open");
       	}
    });
  }
  
  $("#dialog-atualiza").dialog({
    modal: true, width: 600, height: 400, autoOpen: false
    });
  $("#alertas").accordion({
  	collapsible: true,
    //active: false,
    disabled:true
  });
  

 // $(".bpaciente").click(function(){
  	//window.location = "/logistica/?op=saida2&p="+$(this).attr("p");
	  
  	//return false;
 // });
  
  $("#trocar-senha").click(function(){
  	$("#dialog-troca-senha").dialog("open");
  	return false;
  });
  
  $("#div-aviso-troca").hide();
  
  function validar_troca(){
  	var ok = true;
    $("#dialog-troca-senha input").each(function(i){
    	if($(this).val() == ""){
    		$("#aviso-troca-senha").html("<b><i>Todos os campos são obrigatórios!</i></b>");
    		$("#div-aviso-troca").show();
    		ok = false;
    	} 
    });
    if($("#dialog-troca-senha input[name='nova-senha']").val() != $("#dialog-troca-senha input[name='conf-senha']").val()){
    	$("#aviso-troca-senha").html("<b><i>Nova senha não confere com a confirmação!</i></b>");
    	$("#div-aviso-troca").show();
    	ok = false;
    }
    if(ok){
    	$("#aviso-troca-senha").html("");
    	$("#div-aviso-troca").hide();
    }
    return ok;
  }
  
  $("#dialog-troca-senha").dialog({
    modal: true, autoOpen: false,
    buttons: {
    	"Salvar" : function(){
    		if(validar_troca()){
    			var atual = $("#dialog-troca-senha input[name='senha-atual']").val();
    			var nova = $("#dialog-troca-senha input[name='nova-senha']").val();
    			$.post("/query.php", {query: "troca-senha", atual:atual, nova: nova}, 
    			function(r){ 
    				if(r=='-1'){
    					$("#aviso-troca-senha").html("<b><i>Senha atual não confere!</i></b>");
    					$("#div-aviso-troca").show();
    				}
    				else {
    					$("#aviso-troca-senha").html("");
    					$("#div-aviso-troca").hide();
    					$("#dialog-troca-senha").dialog("close");
    				}
    			});
    		}
  		},
  		"Cancelar" : function(){
  			$(this).dialog("close");
  		}
  	}
  });
  
  $("#div-aviso-msg").hide();
  
  $("#enviar-msg").click(function(){
  	$("select[name='usuarios'] option").eq(0).attr("selected","selected");
    $("input[name='titulo']").val("");
    $("textarea[name='msg']").val("");
  	$("#dialog-enviar-msg").dialog("open");
  	return false;
  });
  
  $("#dialog-enviar-msg").dialog({
    modal: true, autoOpen: false, width: 400, height: 400,
    buttons: {
    	"Enviar" : function(){
    		var dest = $("select[name='usuarios'] option:selected").val();
    		var titulo = $("input[name='titulo']").val();
    		var msg = $("textarea[name='msg']").val();
    		$.post("/query.php",{query: "enviar-msg", dest: dest, titulo: titulo, msg: msg},function(r){
    			if(r=='-1') alert("ERRO: Tente mais tarde!");
    			else{ 
    				alert("Enviada com sucesso");
    				$("#dialog-enviar-msg").dialog("close");
    			}
    		});
  		},
  		"Cancelar" : function(){
  			$(this).dialog("close");
  		}
  	}
  });
  
  function atualiza_ler_msg(){
	  $(".ler-msg").unbind("click",ler_msg)
	  				.bind("click",ler_msg);
  }
  
  function ler_msg(){
  	var obj = this;
  	$.post("/query.php",{query: "show-msg", cod: $(obj).attr("cod")},function(r){
  		$("#dialog-show-msg span").html($(obj).attr("t"));
  		$("#corpo-msg").html(r);
  		$("#dialog-show-msg").dialog("open");
  	});
  	return false;
  }
  
  function atualiza_apaga_msg(){
	  $(".del-msg").unbind("click",apaga_msg)
	  				.bind("click",apaga_msg);
  }
    
  $("#refresh-msg").click(function(){
  	$.post("/query.php",{query: "get-msg"},function(r){
  			if(r=='-1') alert('ERRO: Tente mais tarde!');
  			else {
				$("#caixa-msg").html(r);
				atualiza_apaga_msg();
			  	atualiza_ler_msg();
  			}
  	 });
  });
  
  function apaga_msg(){
  	var obj = this;
  	if(confirm("Deseja realmente apagar a mensagem?")){
  		$.post("/query.php",{query: "del-msg", cod: $(obj).attr("cod")},function(r){
  			if(r=='-1') alert('ERRO: Tente mais tarde!');
  			else {
  				alert(r);
  				$(obj).parent().parent().remove();
  			}
  		});
  	}
  	return false;
  }
  
  atualiza_apaga_msg();
  atualiza_ler_msg();
  
  $("#dialog-show-msg").dialog({
  	modal: true, autoOpen: false, width: 400, height: 300,
  	buttons: {
  		"Fechar" : function(){
  			$(this).dialog("close");
  		}
  	}
  });
  
  $(".excluir-nota-aberto").click(function(){
  	if(confirm("Deseja realmente excluir essa nota?")){
  		var cod = $(this).attr("cod");
  		var obj = this;
  		$.post("/query.php",{query: "excluir-nota-aberto", cod: cod},function(r){
  			if(r=='-1'){ 
  				alert("ERRO: Tente mais tarde!");
  				return false;
  			} else {
  				$(obj).parent().parent().remove();
  			}	
  		});
  	}
  	return false;
  });

function blockAguarde() {
    var blockNoInternet = document.createElement('div'),
        classBlockNoInternet = document.createAttribute('class'),
        body = document.querySelector('body');

    classBlockNoInternet.value = 'modal-no-internet';
    blockNoInternet.setAttributeNode(classBlockNoInternet);
    blockNoInternet.innerHTML = '<div class="modal-no-internet-content">Sem internet</div>';

    body.appendChild(blockNoInternet);

    var mylog = false;

    function novoLogAlert(message, type, repeat) {
        var d = new Date();
        var dia = d.getDate() > 9 ? d.getDate() : '0' + d.getDate();
        var mes = d.getMonth() > 9 ? (d.getMonth() + 1) : '0' + (d.getMonth() + 1); // 0 is Jan
        var ano = d.getFullYear();
        var horas = d.getHours();
        var minutos = d.getMinutes() > 9 ? d.getMinutes() : '0' + d.getMinutes();

        var now_ptBR = dia + '/' + mes + '/' + ano + ' ' + horas +':'+ minutos;

        var logError = document.querySelector('.alertify-log-show');
        if (logError && repeat == false) return false;
        return alertify.log(now_ptBR + '<hr/>' + message + '<br/><i>(clique aqui para fechar)</i>', type, 0);
    }

    function keepOnPage (message) {
        return function (e) {
            e = e || window.event;
            // For IE and Firefox
            if (e)
                e.returnValue = message;
            // For Safari
            return message;
        };
    }

    window.ononline = function () {
        window.onbeforeunload = null;
        $('.modal-no-internet').hide();
        novoLogAlert('<b>Boa notícia:</b> Você está com internet! :-)', 'success');
    };
    window.onoffline = function () {
        $('.modal-no-internet').show();
        window.onbeforeunload = keepOnPage("Parece que você está sem internet. Você realmente deseja sair desta página?");
        novoLogAlert('<b>Ops</b>... Parece que você está sem conexão com a internet. Você será informado assim que normalizar.', 'error');
    };

    $(document).ajaxStart(function() {
        $.blockUI({ message: '<h1>Aguarde...</h1>' });
    }).ajaxStop($.unblockUI);

    $( document ).ajaxError(function(event, jqXHR, ajaxSettings, thrownError) {
        novoLogAlert('<b>Desculpa!</b> Ocorreu um erro ao tentar enviar os dados para o servidor. Por favor, tente novamente mais tarde.', 'error', false);
    });
}

  blockAguarde();
  avisos();

  

});