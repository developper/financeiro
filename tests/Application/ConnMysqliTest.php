<?php
/**
 * Created by PhpStorm.
 * User: thiago
 * Date: 16/01/2015
 * Time: 10:41
 */

namespace Test\Application;

use App\Models\DB\ConnMysqli,
    Test\AbstractTest;

class ConnMysqliTest extends AbstractTest {

  protected $conn;

  public function setUp()
  {
    $this->conn = new ConnMysqli;
  }

  public function testGetConnection()
  {
    $connection = $this->conn->getConnection();
    $this->assertObjectHasAttribute('info',$connection);
    $this->assertNotEmpty($connection);
  }
}