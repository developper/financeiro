<?php

namespace Test;

use App\Core\Config;
use App\Integration\Obsequium\PacienteAction;
use \App\Integration\Obsequium\PacientesAPI;

class ObsequiumTest extends AbstractTest
{

  protected $config;

  public function setUp()
  {
    $this->config = Config::get('obsequium_api');
    //$this->api = new PacientesAPI((object) $this->config);
  }

  public function testGetPacientesAPICountPacientes($user = 26, $total = 7)
  {
    //$res = $this->api->getAll($user);
    //$this->assertCount($total, $res['pacientes']);
  }

  public function testPostPacientesAPIAndCountPacientes($user = 26)
  {
    $paciente = ['nome' => 'Test Man', 'sexo' => 'M', 'external_ref' => '555'];
    //$res = $this->api->create($user, [$pacientes]);
    //$this->assertEquals(201, $res->getStatusCode());
  }

  public function testPatchPacientesAPIAndCountPacientes($user = 26)
  {
    $pacientes = array(['id' => 1, 'nome' => 'Test Mannn']);
    //$res = $this->api->update($user, $pacientes);
    //$this->assertEquals(204, $res->getStatusCode());
  }

  public function testPushQueuePacienteCreated()
  {
    $action = new PacienteAction();
    //$action->created(33333, 'Fizz Buzz XXX', 0);
  }

}