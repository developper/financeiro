<?php

use Test\AbstractTest,
    App\Models\Faturamento\TabelaTiss;

class TabelaTissTest extends AbstractTest
{

  public function testGetTabelaOutrasDespesas()
  {
    $lista = TabelaTiss::getAll();
    $this->assertNotNull($lista);
    $this->assertNotEmpty($lista);
  }

  public function testGetTabelaById($id = 1, $tiss_codigo = 0)
  {
    $tabela = TabelaTiss::getById($id);
    $this->assertNotNull($tabela);
    $this->assertNotEmpty($tabela);
    $this->assertEquals($tabela['tiss_codigo'], $tiss_codigo);
  }

} 