<?php

use Test\AbstractTest,
    App\Models\Faturamento\TipoFaturamento;

class TipoFaturamentoTest extends AbstractTest
{

  public function testGetAll()
  {
    $lista = TipoFaturamento::getAll();
    $this->assertNotNull($lista);
    $this->assertNotEmpty($lista);
  }

  public function testGetById($id = 2, $tissCodigo = 1)
  {
    $tipo = TipoFaturamento::getById($id);
    $this->assertNotNull($tipo);
    $this->assertNotEmpty($tipo);
    $this->assertEquals($tipo['tiss_codigo'], $tissCodigo);
    $this->assertEquals('Parcial', $tipo['descricao']);
  }

} 