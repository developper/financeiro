<?php

use Test\AbstractTest,
    App\Models\Faturamento\PlanoSaude;

class PlanoSaudeTest extends AbstractTest
{

  public function testGetAll()
  {
    $planos = PlanoSaude::getAll();
    $this->assertNotNull($planos);
    $this->assertNotEmpty($planos);
  }

  public function testGetById($id = 78)
  {
    $plano = PlanoSaude::getById($id);
    $this->assertNotNull($plano);
    $this->assertNotEmpty($plano);
    $this->assertEquals($plano['id'], $id);
  }

} 