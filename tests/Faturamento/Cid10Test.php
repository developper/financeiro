<?php

use Test\AbstractTest,
    App\Models\Faturamento\Cid10;

class Cid10Test extends AbstractTest
{

  public function testGetAll()
  {
    $lista = Cid10::getAll();
    $this->assertNotNull($lista);
    $this->assertNotEmpty($lista);
  }

  public function testGetById($codigo = '1111')
  {
    $cid10 = Cid10::getById($codigo);
    $this->assertNotNull($cid10);
    $this->assertNotEmpty($cid10);
    $this->assertEquals($cid10['codigo'], $codigo);
    $this->assertEquals($cid10['cid10'] . ' ' . $cid10['descricao'], $cid10['nome']);
  }

} 