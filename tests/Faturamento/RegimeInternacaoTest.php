<?php

use Test\AbstractTest,
    App\Models\Faturamento\RegimeInternacao;

class RegimeInternacaoTest extends AbstractTest
{

  public function testGetAll()
  {
    $lista = RegimeInternacao::getAll();
    $this->assertNotNull($lista);
    $this->assertNotEmpty($lista);
  }

  public function testGetById($id = 3, $tissCodigo = 3)
  {
    $regimeInternacao = RegimeInternacao::getById($id);
    $this->assertNotNull($regimeInternacao);
    $this->assertNotEmpty($regimeInternacao);
    $this->assertEquals($regimeInternacao['tiss_codigo'], $tissCodigo);
    $this->assertEquals('Domiciliar', $regimeInternacao['descricao']);
  }

} 