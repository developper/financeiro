<?php

use Test\AbstractTest,
    App\Models\Faturamento\TipoOutrasDespesas;

class TipoOutrasDespesasTest extends AbstractTest
{

  public function testGetLista()
  {
    $lista = TipoOutrasDespesas::getAll();
    $this->assertNotNull($lista);
    $this->assertNotEmpty($lista);
  }

  public function testGetListaComCodigoInterno()
  {
    $lista = TipoOutrasDespesas::getAllComCodigoInterno();
    $this->assertNotNull($lista);
    $this->assertNotEmpty($lista);
  }

} 