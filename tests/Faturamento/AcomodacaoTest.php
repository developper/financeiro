<?php

use Test\AbstractTest,
    App\Models\Faturamento\Acomodacao;

class AcomodacaoTest extends AbstractTest
{

  public function testGetAll()
  {
    $lista = Acomodacao::getAll();
    $this->assertNotNull($lista);
    $this->assertNotEmpty($lista);
  }

  public function testGetById($id = 30, $tissCodigo = 61)
  {
    $acomodacao = Acomodacao::getById($id);
    $this->assertNotNull($acomodacao);
    $this->assertNotEmpty($acomodacao);
    $this->assertEquals($acomodacao['tiss_codigo'], $tissCodigo);
    $this->assertEquals('Outras diárias', $acomodacao['descricao']);
  }

} 