<?php

use Test\AbstractTest,
    App\Models\Faturamento\GuiaSADT;

class GuiaSADTTest extends AbstractTest
{

  public function testGetAll()
  {
    $lista = GuiaSADT::getAll();
    $this->assertNotNull($lista);
  }

  public function testGetByCapaLote($capaLote = '025.140613.124706')
  {
    $guias = GuiaSADT::getByCapaLote($capaLote);
    $this->assertNotNull($guias);
  }

  public function testGetFaturasByGuiaSadt($guiaId = 1)
  {
    $faturas = GuiaSADT::getFaturasByGuiaId($guiaId);
    $this->assertNotNull($faturas);
  }

  public function testGetItensFatura($tissFaturaId = 1)
  {
    $itens = GuiaSADT::getItensFatura($tissFaturaId);
    $this->assertNotNull($itens);
  }

  public function testXMLById($xmlId = 1)
  {
    $xml = GuiaSADT::getXMLById($xmlId);
    $this->assertNotNull($xml);
  }

  public function testGetGuiasByLoteId($loteId = 2)
  {
    $guias = GuiaSADT::getGuiasByLoteId($loteId);
    $this->assertNotNull($guias);
    $this->assertNotEmpty($guias);
  }

} 