<?php

require 'app/Models/Faturamento/Fatura.php';

use Test\AbstractTest,
    App\Models\Faturamento\Fatura;

class FaturaTest extends AbstractTest
{

  public function testGetById($faturaId = 1155, $totalItens = 11, $condicao = "AND VALOR_FATURA != '0.00' ")
  {
    $itensFatura = Fatura::getItensByFaturaId($faturaId, $condicao);
    $this->assertNotNull($itensFatura);
    $this->assertNotEmpty($itensFatura);
    $this->assertCount($totalItens, $itensFatura);
  }

  public function testGetByCapaLote($capaLote = '025.140721.183242')
  {
    $faturas = Fatura::getByCapaLote($capaLote);
    $this->assertNotNull($faturas);
  }

  /**
   * @dataProvider descontoAcrescimoProvider
   */
  public function testCalcularDescontoAcrescimoItemFatura(
    $valorItem,
    $descontoAcrescimo,
    $quantidade,
    $valorDescontado
  ) {
    $data = Fatura::calculaDescontoAcrescimo($valorItem, $descontoAcrescimo, $quantidade);
    $this->assertEquals($data, $valorDescontado);
  }

  public function descontoAcrescimoProvider()
  {
    return array(
      array(45.86, -10, 18, -82.548000000000002),
      array(29.25, -10, 2, -5.8500000000000005),
      array(370, 10.84, 31, 1243.348)
    );
  }

} 