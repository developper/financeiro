<?php

use Test\AbstractTest,
    App\Models\Faturamento\TipoSaida;

class TipoSaidaTest extends AbstractTest
{

  public function testGetAll()
  {
    $lista = TipoSaida::getAll();
    $this->assertNotNull($lista);
    $this->assertNotEmpty($lista);
  }

  public function testGetById($id = 3)
  {
    $tipoSaida = TipoSaida::getById($id);
    $this->assertNotNull($tipoSaida);
    $this->assertNotEmpty($tipoSaida);
    $this->assertEquals($tipoSaida['id'], $id);
  }

} 