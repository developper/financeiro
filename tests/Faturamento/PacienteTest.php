<?php

use Test\AbstractTest,
    App\Models\Faturamento\Paciente;

class PacienteTest extends AbstractTest
{

  public function testGetById($id = 1)
  {
    $paciente = Paciente::getById($id);
    $this->assertNotNull($paciente);
    $this->assertNotEmpty($paciente);
    $this->assertEquals($paciente['idClientes'], $id);
  }

} 