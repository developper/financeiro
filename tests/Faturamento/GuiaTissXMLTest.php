<?php

use Test\AbstractTest,
	  App\Models\Faturamento\XML as Fatu;

class GuiaTissXMLTest extends AbstractTest
{

	protected $guiaXML;
	protected $pathFileXML = './app/Samples/tiss_xml/00000000000000000001_94a3cd599ef7d4935fb2dfe623eec5bf.xml.sample.xml';

	public function setUp()
	{
		$this->guiaXML = new Fatu\GuiaTissXML;
	}

	public function testLoadFromSampleXML()
	{		
		$this->assertInstanceOf('\DOMDocument', $this->guiaXML->loadFromFile($this->pathFileXML));
	}

	public function createDOMDocumentXML()
	{	
		$doc = new \DOMDocument();
		$doc->load($this->pathFileXML);
		return $doc;
	}

	public function testGuiaXMLIsValid()
	{
		$this->assertInstanceOf('\DOMDocument', $this->createDOMDocumentXML());
	}

  public function testImportFromData()
  {
    $data = array ( 'tipoGuia' => 'spsadt', 'tipoTransacao' => 'ENVIO_LOTE_GUIAS', 'sequencialTransacao' => '37', 'identificacaoPrestador' => array ( 'CNPJ' => '07072541000174', ), 'destino' => array ( 'registroANS' => '414310', ), 'registroANS' => '414310', 'numeroLote' => '37', 'guias' => array ( 0 => array ( 'numeroGuiaPrestador' => '112', 'dadosAutorizacao' => array ( 'dataAutorizacao' => '2014-09-08', ), 'contratadoSolicitante' => array ( 'cnpjContratado' => '07072541000174', 'nomeContratado' => 'MEDERI SAUDE DOMICILIAR LTDA', ), 'profissionalSolicitante' => array ( 'profissionalSolicitante' => 'LUCIO COUTO O. JUNIOR', 'conselhoProfissional' => '6', 'numeroConselhoProfissional' => '15847', 'UF' => '29', 'CBOS' => '225225', ), 'dadosBeneficiario' => array ( 'numeroCarteira' => '000876054', 'atendimentoRN' => 'N', 'nomeBeneficiario' => 'ANTÃ”NIO MANOEL DA SILVA', 'identificadorBeneficiario' => 'NTQx', ), 'caraterAtendimento' => '1', 'dadosExecutante' => array ( 'CNES' => '3795160', 'contratadoExecutante' => array ( 'cnpjContratado' => '07072541000174', 'nomeContratado' => 'MEDERI SAUDE DOMICILIAR LTDA', )), 'dadosAtendimento' => array ( 'tipoAtendimento' => '01', 'indicacaoAcidente' => '0', ), 'procedimentos' => array ( 0 => array ( 'dataExecucao' => '2014-06-27', 'codigoTabela' => '00', 'codigoProcedimento' => '89602', 'descricaoProcedimento' => 'DIARIA DE ASSISTENCIA 24h SEM RESPIRADOR', 'quantidadeExecutada' => '010', 'reducaoAcrescimo' => '0.00', 'valorUnitario' => '320.00', 'valorTotal' => '3200.00', ), 1 => array ( 'dataExecucao' => '2014-06-13', 'codigoTabela' => '00', 'codigoProcedimento' => '86153', 'descricaoProcedimento' => 'FISIOTERAPIA MOTORA E RESPIRATORIA POR SESSÃŒO', 'quantidadeExecutada' => '007', 'reducaoAcrescimo' => '0.00', 'valorUnitario' => '60.00', 'valorTotal' => '420.00', ), ), 'outrasDespesas' => array ( 0 => array ( 'codigoDespesa' => '02', 'dataExecucao' => '2014-06-27', 'codigoTabela' => '00', 'codigoProcedimento' => '89601', 'quantidadeExecutada' => '0006', 'unidadeMedida' => '001', 'reducaoAcrescimo' => '0.00', 'valorUnitario' => '136.56', 'valorTotal' => '819.36', 'descricaoProcedimento' => 'EPREX - 4.000 UI/0,4ML Seringa', ), 1 => array ( 'codigoDespesa' => '03', 'dataExecucao' => '2014-06-27', 'codigoTabela' => '00', 'codigoProcedimento' => '89604', 'quantidadeExecutada' => '0010', 'unidadeMedida' => '036', 'reducaoAcrescimo' => '0.00', 'valorUnitario' => '24.00', 'valorTotal' => '240.00', 'descricaoProcedimento' => 'CONCENTRADOR DE OXIGENIO', ), 2 => array ( 'codigoDespesa' => '03', 'dataExecucao' => '2014-06-13', 'codigoTabela' => '00', 'codigoProcedimento' => '86158', 'quantidadeExecutada' => '0001', 'unidadeMedida' => '036', 'reducaoAcrescimo' => '0.00', 'valorUnitario' => '2.62', 'valorTotal' => '2.62', 'descricaoProcedimento' => 'ÃLCOOL 70% QUALITY - ALMOT. 100 ML', ), 3 => array ( 'codigoDespesa' => '03', 'dataExecucao' => '2014-06-13', 'codigoTabela' => '00', 'codigoProcedimento' => '86156', 'quantidadeExecutada' => '0002', 'unidadeMedida' => '036', 'reducaoAcrescimo' => '0.00', 'valorUnitario' => '5.69', 'valorTotal' => '11.38', 'descricaoProcedimento' => 'CATETER NASAL OXIGENIO TP.SONDA NR.10 40CM 2604P', ), 4 => array ( 'codigoDespesa' => '03', 'dataExecucao' => '2014-06-13', 'codigoTabela' => '00', 'codigoProcedimento' => '86157', 'quantidadeExecutada' => '0002', 'unidadeMedida' => '036', 'reducaoAcrescimo' => '0.00', 'valorUnitario' => '18.27', 'valorTotal' => '36.54', 'descricaoProcedimento' => 'TUBO ASPIRAÃ‡ÃƒO / OXIGÃŠNIO - 2MT PVC SILIC', ), 5 => array ( 'codigoDespesa' => '05', 'dataExecucao' => '2014-06-27', 'codigoTabela' => '00', 'codigoProcedimento' => '89607', 'quantidadeExecutada' => '0010', 'unidadeMedida' => '036', 'reducaoAcrescimo' => '0.00', 'valorUnitario' => '3.00', 'valorTotal' => '30.00', 'descricaoProcedimento' => 'TORPEDO DE OXIGENIO DE 10 M3', ), ), 'valorProcedimentos' => '3620.00', 'valorDiarias' => '30.00', 'valorTaxasAlugueis' => '30.00', 'valorMateriais' => '290.54', 'valorMedicamentos' => '819.36', 'valorOPME' => '0.00', 'valorGasesMedicinais' => '0.00', 'valorTotalGeral' => '4759.90', ), ), );
    $this->guiaXML->importFromData($data);
    $this->assertTrue($this->guiaXML->isValid());
  }

}
