<?php

use Test\AbstractTest,
		App\Models\Faturamento\XML as Fatu;

class GuiaTissTest extends AbstractTest
{
	protected $guiaTiss;
	protected $guiaXML;

	protected function setUp()
	{		
		$this->guiaTiss = $this->createGuiaTiss();
		$this->guiaXML = new Fatu\GuiaTissXML($this->guiaTiss);
	}

  public function testCalculoHashComAcentuacaoExterno()
  {
    $file = file_get_contents('./app/Samples/tiss_xml/00000000000000000039_e8fc911ac944e115df2c040936019fa2.xml');
    $xml  = new \SimpleXMLElement($file);
    $hash = $this->guiaXML->getHash($xml);
    $this->assertEquals('53147afae675368c6b920cde309d9758', $hash);
  }

	private function createContratado()
	{
		return new Fatu\Contratado(
			array('CNPJ' => '07072541000174'),
        'MEDERI SAUDE DOMICILIAR LTDA',
        $this->createEnderecoContratado(),
        3795160
      );
	}	

	private function createEnderecoContratado()
	{	return new Fatu\EnderecoContratado('081', 'ARISTON JOSE DE CERQUEIRA', '393', '5454545', 'FEIRA DE SANTANA', 'BA', '44100000');
	}

	private function createProfissional()
	{	return new Fatu\Profissional('LUCIO COUTO O. JUNIOR', Fatu\Profissional::SIGLA_CRM, 15847, Fatu\Profissional::UF_BA, 2231.15, '90855469587');
	}

	public function createCabecalho()
	{
		$c = new Fatu\Cabecalho;		
		$c->setIdentificacaoTransacao($this->createIdentificacaoTransacao());
		$c->setOrigem($this->createOrigem());
		$c->setDestino($this->createDestino());
		$c->setIdentificacaoSoftwareGerador($this->createIdentificacaoSoftwareGerador());
		return $c;
	}
	
	public function createIdentificacaoTransacao()
	{	return new Fatu\IdentificacaoTransacao('ENVIO_LOTE_GUIAS', 1, \DateTime::createFromFormat('d-m-Y H:i:s', '22-05-2014 10:48:45'));				
	}

	public function createGuiaTiss()
	{
		$guia = new Fatu\GuiaTiss;
		$guia->setCabecalho($this->createCabecalho());
		$guia->setPretadorParaOperadora($this->CreatePrestadorParaOperadora());
		$guia->setEpilogo('94a3cd599ef7d4935fb2dfe623eec5bf');
		return $guia;
	}
	
	public function createOrigem()
	{	return new Fatu\Origem('07072541000174');
	}
	
	public function createDestino()
	{	return new Fatu\Destino(324477);
	}
	
	public function createIdentificacaoSoftwareGerador()
	{	return new Fatu\IdentificacaoSoftwareGerador;
	}
	
	public function createPrestadorParaOperadora()
	{
		$po = new Fatu\PrestadorParaOperadora;
		$po->setLoteGuias($this->createLoteGuias());
		return $po;
	}
	
	public function createLoteGuias()
	{
		$lg = new Fatu\LoteGuias;
		$lg->addGuia($this->createGuia(), 1);
		return $lg;
	}	

	public function createGuia()
	{	return new Fatu\Guia($this->createGuiaFaturamento());
	}
	
	public function createGuiaFaturamento()
	{
    $guiaFaturamento = new Fatu\GuiaFaturamento();
    $guiaFaturamento->addGuiaSPSADT($this->createGuiaSPSADT());
    return $guiaFaturamento;
	}
	
	public function createGuiaSPSADT()
	{
		$g = new Fatu\GuiaSPSADT;
		$g->setIdentificacaoGuiaSADTSP($this->createIdentificacaoGuiaSADTSP());
		$g->setNumeroGuiaPrincipal(122);
		$g->setDadosAutorizacao($this->createDadosAutorizacao());
		$g->setDadosBeneficiario($this->createDadosBeneficiario());
		$g->setDadosSolicitante($this->createDadosSolicitante());
		$g->setPrestadorExecutante($this->createPrestadorExecutante());
		$g->setCaraterAtendimento($this->createCaraterAtendimento());
		$g->setDataHoraAtendimento(new \DateTime('2014-03-01T00:00:00'));
		$g->setDiagnosticoAtendimento($this->createDiagnosticoAtendimento());
		$g->setTipoSaida(5);
		$g->setTipoAtendimento(6);		
		$g->setProcedimentosRealizados($this->createProcedimentosRealizados());
    $g->setOutrasDespesas($this->createOutrasDespesas());
		return $g;
	}
	
	public function createIdentificacaoGuiaSADTSP()
	{	return new Fatu\IdentificacaoGuiaSADTSP(array('registroANS' => 324477), \DateTime::createFromFormat('Y-m-d', '2014-04-07'), 122, 122);
	}	
	
	public function createDadosAutorizacao()
	{
		return new Fatu\DadosAutorizacao(
			\DateTime::createFromFormat('Y-m-d', '2014-02-27'), 
			'04345298', 
			\DateTime::createFromFormat('Y-m-d', '2014-04-30')
		);	
	}

	public function createDadosBeneficiario()
	{	return new Fatu\DadosBeneficiario('021546800', 'FRANCISCO NILTON ALENCAR', 'BASICO');
	}
	
	public function createDadosSolicitante()
	{	
		$dadosSolicitante = new Fatu\DadosSolicitante;
		$dadosSolicitante->setContratado($this->createContratado());
		$dadosSolicitante->setProfissional($this->createProfissional());
		return $dadosSolicitante;
	}

	public function createPrestadorExecutante()
	{	return new Fatu\PrestadorExecutante($this->createContratado(), $this->createProfissional());
	}

	public function	createCaraterAtendimento()		
	{	return new Fatu\CaraterAtendimento(Fatu\CaraterAtendimento::ELETIVA);
	}

	public function createDiagnosticoAtendimento()
	{	return new Fatu\DiagnosticoAtendimento($this->createCid(), 2);
	}

	public function createCid()
	{	return new Fatu\Cid('CID-10', 'S82.1', 'Fratura da extremidade proximal da tíbia');
	}

	public function createProcedimentosRealizados()
	{	return new Fatu\ProcedimentosRealizados([$this->createProcedimentos()]);
	}

	public function createProcedimentos()
	{
    return new Fatu\Procedimentos(
      $this->createProcedimento(),
      \DateTime::createFromFormat('Y-m-d', '2014-03-01'),
      \DateTime::createFromFormat('Y-m-d H:i:s', '2014-03-01 00:00:00'),
      \DateTime::createFromFormat('Y-m-d H:i:s', '2014-03-01 00:00:00'),
      12,
      58.00
    );
	}

  public function createOutrasDespesas()
  {
    $despesa1 = new Fatu\Despesa;
    $despesa1->setCodigo(1);
    $despesa1->setTipoTabela('00');
    $despesa1->setDescricao('CAMA HOSPITALAR');
    $despesa1->setTipoDespesa(6);
    $despesa1->setDataRealizacao(\DateTime::createFromFormat('Y-m-d', '2014-05-01'));
    $despesa1->setHoraInicial(\DateTime::createFromFormat('H:i:s', '09:46:36'));
    $despesa1->setHoraFinal(\DateTime::createFromFormat('H:i:s', '09:46:36'));
    $despesa1->setQuantidade(31);
    $despesa1->setValorUnitario(6);

    $despesa2 = new Fatu\Despesa;
    $despesa2->setCodigo(2);
    $despesa2->setTipoTabela(12);
    $despesa2->setDescricao('ALGODAO BOLA 175G');
    $despesa2->setTipoDespesa(3);
    $despesa2->setDataRealizacao(\DateTime::createFromFormat('Y-m-d', '2014-05-01'));
    $despesa2->setHoraInicial(\DateTime::createFromFormat('H:i:s', '09:46:36'));
    $despesa2->setHoraFinal(\DateTime::createFromFormat('H:i:s', '09:46:36'));
    $despesa2->setQuantidade(1);
    $despesa2->setValorUnitario(18.98);

    $outrasDespesas = new Fatu\OutrasDespesas();
    $outrasDespesas->add($despesa1);
    $outrasDespesas->add($despesa2);

    return $outrasDespesas;
  }

	public function createProcedimento()
	{	return new Fatu\Procedimento(72090022, 98, 'SESSÃO FISIOTERAPIA - HOME CARE');
	}

	public function createValorTotal()
	{	return new Fatu\ValorTotal(696.00, 0.0, 0.0, 0.0, 0.0, 0.0);
	}

	public function testGuiaTissClassExists()
	{
		$guia = $this->createGuiaTiss();
		$this->assertNotNull($guia);
		$this->assertClassHasAttribute('cabecalho', get_class($guia));
		$this->assertClassHasAttribute('prestadorParaOperadora', get_class($guia));
		$this->assertClassHasAttribute('epilogo', get_class($guia));
	}

	public function testCabecalhoClassExists()
	{
		$cabecalho = $this->createCabecalho();
		$this->assertNotNull($cabecalho);
		$this->assertClassHasAttribute('identificacaoTransacao', get_class($cabecalho));
		$this->assertClassHasAttribute('origem', get_class($cabecalho));
		$this->assertClassHasAttribute('destino', get_class($cabecalho));
		$this->assertClassHasAttribute('versaoPadrao', get_class($cabecalho));
		$this->assertClassHasAttribute('identificacaoSoftwareGerador', get_class($cabecalho));	
	}	

	public function testIdenficacaoTransacaoClassExists()
	{
		$idTransacao = $this->createIdentificacaoTransacao();
		$this->assertNotNull($idTransacao);
		$this->assertClassHasAttribute('tipoTransacao', get_class($idTransacao));	
		$this->assertClassHasAttribute('sequencialTransacao', get_class($idTransacao));
		$this->assertClassHasAttribute('dataRegistroTransacao', get_class($idTransacao));
		$this->assertClassHasAttribute('horaRegistroTransacao', get_class($idTransacao));	
	}		

	public function testOrigemClassExists()
	{
		$origem = $this->createOrigem();
		$this->assertNotNull($origem);
		$this->assertArrayHasKey('CNPJ', $origem->getCodigoPrestadorNaOperadora());
		$this->assertClassHasAttribute('codigoPrestadorNaOperadora', get_class($origem));
	}			

	public function testDestinoClassExists()
	{
		$destino = $this->createDestino();
		$this->assertNotNull($destino);	
		$this->assertClassHasAttribute('registroANS', get_class($destino));	
	}		

	public function testIdentificacaoSoftwareGeradorClassExists()
	{
		$idSoftGerador = $this->createIdentificacaoSoftwareGerador();
		$this->assertNotNull($idSoftGerador);		
	}	

	public function testCheckIdentificacaoSoftwareGerador()
	{
		$this->assertEquals('SISMEDERI_V1', Fatu\IdentificacaoSoftwareGerador::NOME_APLICATIVO);
		$this->assertEquals('1.x.x', Fatu\IdentificacaoSoftwareGerador::VERSAO_APLICATIVO);
		$this->assertEquals('Mederi', Fatu\IdentificacaoSoftwareGerador::FABRICANTE_SOFTWARE);
	}		

	public function testPrestadorParaOperadoraClassExists()
	{
		$ppo = $this->createPrestadorParaOperadora();
		$this->assertNotNull($ppo);
		$this->assertClassHasAttribute('loteGuias', get_class($ppo));
	}	

	public function testLoteGuiasClassExists()
	{
		$loteGuias = $this->createLoteGuias();
		$this->assertNotNull($loteGuias);
		$this->assertClassHasAttribute('numeroLote', get_class($loteGuias));
		$this->assertClassHasAttribute('guias', get_class($loteGuias));
	}	

	public function testGuiaClassExists()
	{
		$guia = $this->createGuia();
		$this->assertNotNull($guia);
		$this->assertClassHasAttribute('guiaFaturamento', get_class($guia));
	}	

	public function testGuiaFaturamentoClassExists()
	{
		$guiaFatu = $this->createGuiaFaturamento();
		$this->assertNotNull($guiaFatu);				
		$this->assertClassHasAttribute('guiasSPSADT', get_class($guiaFatu));
	}	

	public function testGuiaSPSADTClassExists()
	{
		$g = $this->createGuiaSPSADT();		
		$this->assertEquals('06', $g->getTipoAtendimento());
		$this->assertNotNull($g);		
	}

	public function testCheckAtributosGuiaSPSADT()
	{
		$g = $this->createGuiaSPSADT();
		$this->assertClassHasAttribute('identificacaoGuiaSADTSP', get_class($g));
		$this->assertClassHasAttribute('numeroGuiaPrincipal', get_class($g));
		$this->assertClassHasAttribute('dadosAutorizacao', get_class($g));
		$this->assertClassHasAttribute('dadosBeneficiario', get_class($g));
		$this->assertClassHasAttribute('dadosSolicitante', get_class($g));
		$this->assertClassHasAttribute('prestadorExecutante', get_class($g));
		$this->assertClassHasAttribute('caraterAtendimento', get_class($g));
		$this->assertClassHasAttribute('dataHoraAtendimento', get_class($g));
		$this->assertClassHasAttribute('diagnosticoAtendimento', get_class($g));
		$this->assertClassHasAttribute('tipoSaida', get_class($g));
		$this->assertClassHasAttribute('tipoAtendimento', get_class($g));
		$this->assertClassHasAttribute('procedimentosRealizados', get_class($g));
		$this->assertClassHasAttribute('valorTotal', get_class($g));
	}	

	public function testIdentificacaoGuiaSADTSPClassExists()
	{
		$identificaoSADT = $this->createIdentificacaoGuiaSADTSP();
		$this->assertNotNull($identificaoSADT);
	}	

	public function testDadosAutorizacaoClassExists()
	{
			$dadosAutorizacao = $this->createDadosAutorizacao();
			$this->assertNotNull($dadosAutorizacao);
	}

	public function testCheckAtributosDadosAutoriazacao()
	{
		$dadosAutorizacao = $this->createDadosAutorizacao();
		$this->assertClassHasAttribute('dataAutorizacao', get_class($dadosAutorizacao));
		$this->assertClassHasAttribute('senhaAutorizacao', get_class($dadosAutorizacao));
		$this->assertClassHasAttribute('validadeSenha', get_class($dadosAutorizacao));
	}	

	public function testDadosBeneficiarioClassExists()
	{
		$dadosBene = $this->createDadosBeneficiario();
		$this->assertNotNull($dadosBene);
	}	

	public function testDadosSolicitanteClassExists()
	{
		$dadosSoli = $this->createDadosSolicitante();		
		$this->assertNotNull($dadosSoli);
		$this->assertClassHasAttribute('contratado', get_class($dadosSoli));
		$this->assertClassHasAttribute('profissional', get_class($dadosSoli));		
	}		

	public function testContratadoClassExists()
	{		
		$contratado = $this->createContratado();		
		$this->assertNotNull($contratado);
		$this->assertClassHasAttribute('identificacao', get_class($contratado));
		$this->assertClassHasAttribute('nomeContratado', get_class($contratado));
		$this->assertClassHasAttribute('enderecoContratado', get_class($contratado));
		$this->assertClassHasAttribute('numeroCNES', get_class($contratado));
	}

	public function testEnderecoContratadoClassExists()
	{
		$end = $this->createEnderecoContratado();
		$this->assertNotNull($end);
		$this->assertClassHasAttribute('tipoLogradouro', get_class($end));
		$this->assertClassHasAttribute('logradouro', get_class($end));
		$this->assertClassHasAttribute('numero', get_class($end));
		$this->assertClassHasAttribute('codigoIBGEMunicipio', get_class($end));
		$this->assertClassHasAttribute('municipio', get_class($end));
		$this->assertClassHasAttribute('codigoUF', get_class($end));
		$this->assertClassHasAttribute('cep', get_class($end));		
	}

	public function testProfissionalClassExists()
	{
		$profissional = $this->createProfissional();
		$this->assertNotNull($profissional);
		$this->assertClassHasAttribute('nome', get_class($profissional));		
		$this->assertClassHasAttribute('siglaConselho', get_class($profissional));		
		$this->assertClassHasAttribute('numeroConselho', get_class($profissional));		
		$this->assertClassHasAttribute('ufConselho', get_class($profissional));
		$this->assertClassHasAttribute('cbos', get_class($profissional));	
		$this->assertClassHasAttribute('cpf', get_class($profissional));	
	}	

	public function testPrestadorExecutanteClassExists()
	{
		$prestExecutante = $this->createPrestadorExecutante();
		$this->assertNotNull($prestExecutante);
		$this->assertClassHasAttribute('contratado', get_class($prestExecutante));		
		$this->assertClassHasAttribute('profissional', get_class($prestExecutante));
		$this->assertNotNull($prestExecutante->getContratado());
		$this->assertNotNUll($prestExecutante->getProfissional());
	}	

	public function testCaraterAtendimentoClassExists()
	{
		$caraterAtendimento = $this->createCaraterAtendimento();
		$this->assertNotNull($caraterAtendimento);
		$this->assertEquals($caraterAtendimento->getCarater(), Fatu\CaraterAtendimento::ELETIVA);
	}
	
	public function testDiagnosticoAtendimentoClassExists()
	{	
		$diagAtend = $this->createDiagnosticoAtendimento();
		$this->assertNotNull($diagAtend);			
		$this->assertClassHasAttribute('cid', get_class($diagAtend));
		$this->assertClassHasAttribute('indicadorAcidente', get_class($diagAtend));
	}

	public function testCidClassExists()
	{
		$cid = $this->createCid();
		$this->assertNotNull($cid);		
		$this->assertClassHasAttribute('nomeTabela', get_class($cid));
		$this->assertClassHasAttribute('codigoDiagnostico', get_class($cid));
		$this->assertClassHasAttribute('descricaoDiagnostico', get_class($cid));
	}		

	public function testProcedimentosRealizadosClassExists()
	{
		$pr = $this->createProcedimentosRealizados();
		$this->assertNotNull($pr);
		$this->assertClassHasAttribute('procedimentos', get_class($pr));
	}

  public function testProcedimentosRealizadosTotalGeral()
  {
    $pr = $this->createProcedimentosRealizados();
    $this->assertEquals(696, $pr->getTotalGeral());
  }

  public function testOutrasDespesasClassExists()
  {
    $od = $this->createOutrasDespesas();
    $this->assertNotNull($od);
  }

  public function testDespesasClassExists()
  {
    $despesa = new Fatu\Despesa();
    $this->assertNotNull($despesa);

    $className = get_class($despesa);
    $this->assertClassHasAttribute('codigo', $className);
    $this->assertClassHasAttribute('tipoTabela', $className);
    $this->assertClassHasAttribute('descricao', $className);
    $this->assertClassHasAttribute('tipoDespesa', $className);
    $this->assertClassHasAttribute('dataRealizacao', $className);
    $this->assertClassHasAttribute('horaInicial', $className);
    $this->assertClassHasAttribute('horaFinal', $className);
    $this->assertClassHasAttribute('quantidade', $className);
    $this->assertClassHasAttribute('valorUnitario', $className);
  }

	public function testProcedimentosClassExists()
	{
		$p = $this->createProcedimentos();
		$this->assertNotNull($p);
		$this->assertClassHasAttribute('procedimento', get_class($p));
		$this->assertClassHasAttribute('data', get_class($p));
		$this->assertClassHasAttribute('quantidadeRealizada', get_class($p));
		$this->assertClassHasAttribute('valor', get_class($p));
		$this->assertClassHasAttribute('valorTotal', get_class($p));
	}		

	public function testProcedimentoClassExists()
	{
		$p = $this->createProcedimento();
		$this->assertNotNull($p);
		$this->assertClassHasAttribute('codigo', get_class($p));
		$this->assertClassHasAttribute('tipoTabela', get_class($p));
		$this->assertClassHasAttribute('descricao', get_class($p));
	}

	public function testValorTotalClassExists()
	{
		$vt = $this->createValorTotal();
		$this->assertNotNull($vt);
		$this->assertClassHasAttribute('servicosExecutados', get_class($vt));
		$this->assertClassHasAttribute('diarias', get_class($vt));
		$this->assertClassHasAttribute('taxas', get_class($vt));
		$this->assertClassHasAttribute('materiais', get_class($vt));
		$this->assertClassHasAttribute('medicamentos', get_class($vt));
		$this->assertClassHasAttribute('gases', get_class($vt));
		$this->assertEquals(696, $vt->getTotalGeral());		
	}	

}








