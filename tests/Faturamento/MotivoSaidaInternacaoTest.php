<?php

use Test\AbstractTest,
    App\Models\Faturamento\MotivoSaidaInternacao;

class MotivoSaidaInternacaoTest extends AbstractTest
{

  public function testGetAll()
  {
    $lista = MotivoSaidaInternacao::getAll();
    $this->assertNotNull($lista);
    $this->assertNotEmpty($lista);
  }

  public function testGetById($id = 18, $tissCodigo = 41)
  {
    $motivo = MotivoSaidaInternacao::getById($id);
    $this->assertNotNull($motivo);
    $this->assertNotEmpty($motivo);
    $this->assertEquals($motivo['tiss_codigo'], $tissCodigo);
    $this->assertEquals('Com declaração de óbito fornecida pelo médico assistente', $motivo['descricao']);
  }

} 