<?php

use Test\AbstractTest,
    App\Models\Faturamento\Profissional;

class ProfissionalTest extends AbstractTest
{

  public function testGetAll()
  {
    $profissionais = Profissional::getAll();
    $this->assertNotNull($profissionais);
    $this->assertNotEmpty($profissionais);
  }

  public function testGetById($id = 1)
  {
    $profissional = Profissional::getById($id);
    $this->assertNotNull($profissional);
    $this->assertNotEmpty($profissional);
    $this->assertEquals($profissional['id'], $id);
  }

} 