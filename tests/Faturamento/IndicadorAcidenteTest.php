<?php

use Test\AbstractTest,
    App\Models\Faturamento\IndicadorAcidente;

class IndicadorAcidenteTest extends AbstractTest
{

  public function testGetAll()
  {
    $lista = IndicadorAcidente::getAll();
    $this->assertNotNull($lista);
    $this->assertNotEmpty($lista);
  }

  public function testGetById($id = 1)
  {
    $indicadorAcidente = IndicadorAcidente::getById($id);
    $this->assertNotNull($indicadorAcidente);
    $this->assertNotEmpty($indicadorAcidente);
    $this->assertEquals($indicadorAcidente['id'], $id);
  }

} 