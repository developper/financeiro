<?php

use Test\AbstractTest,
    App\Models\Faturamento\TipoAtendimento;

class TipoAtendimentoTest extends AbstractTest
{

  public function testGetAll()
  {
    $lista = TipoAtendimento::getAll();
    $this->assertNotNull($lista);
    $this->assertNotEmpty($lista);
  }

  public function testGetById($id = 1)
  {
    $tipoAtendimento = TipoAtendimento::getById($id);
    $this->assertNotNull($tipoAtendimento);
    $this->assertNotEmpty($tipoAtendimento);
    $this->assertEquals($tipoAtendimento['id'], $id);
  }

} 