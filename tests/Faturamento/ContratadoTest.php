<?php

use Test\AbstractTest,
    App\Models\Faturamento\Contratado;

class ContratadoTest extends AbstractTest
{

  public function testGetAll()
  {
    $contratados = Contratado::getAll();
    $this->assertNotNull($contratados);
    $this->assertNotEmpty($contratados);
  }

  public function testGetById($id = 1)
  {
    $contratado = Contratado::getById($id);
    $this->assertNotNull($contratado);
    $this->assertNotEmpty($contratado);
    $this->assertEquals($contratado['id'], $id);
  }

} 