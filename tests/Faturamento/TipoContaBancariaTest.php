<?php

use Test\AbstractTest,
    App\Models\Financeiro\TipoContaBancaria;

class TipoContaBancariaTest extends AbstractTest
{

  public function testGetAll()
  {
    $tipos = TipoContaBancaria::getAll();
    $this->assertNotNull($tipos);
    $this->assertNotEmpty($tipos);
  }

} 