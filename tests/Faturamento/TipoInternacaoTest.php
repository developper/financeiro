<?php

use Test\AbstractTest,
    App\Models\Faturamento\TipoInternacao;

class TipoInternacaoTest extends AbstractTest
{

  public function testGetAll()
  {
    $lista = TipoInternacao::getAll();
    $this->assertNotNull($lista);
    $this->assertNotEmpty($lista);
  }

  public function testGetById($id = 3, $tissCodigo = 3)
  {
    $tipoInternacao = TipoInternacao::getById($id);
    $this->assertNotNull($tipoInternacao);
    $this->assertNotEmpty($tipoInternacao);
    $this->assertEquals($tipoInternacao['tiss_codigo'], $tissCodigo);
    $this->assertEquals('Obstétrica', $tipoInternacao['descricao']);
  }

} 