<?php

use Test\AbstractTest,
  App\Models\Financeiro\IPCF;
class IPCFTest extends AbstractTest
{
  public function testGetAll()
  {
    $lista = IPCF::getAll();
    $this->assertNotNull($lista);
    $this->assertNotEmpty($lista);
  }

  public function testGetById($id = 286, $N4 = 'JUROS/MULTAS POR ATRASO')
  {
    $ipcf = IPCF::getById($id);
    $this->assertNotNull($ipcf);
    $this->assertNotEmpty($ipcf);
    $this->assertEquals($ipcf['N4'], $N4);
    $this->assertEquals('2.5.1.02', $ipcf['COD_N4']);
  }
}