<?php

use Test\AbstractTest,
  App\Models\Financeiro\Financeiro;

class CustosTest extends AbstractTest
{
  protected $financeiro;

  public function setUp()
  {
    $this->financeiro = new Financeiro;
  }

  public function testGetCustos()
  {
    $periodo = new \stdClass();
    $periodo->inicio = \DateTime::createFromFormat('Y-m-d','2014-08-01');
    $periodo->fim = \DateTime::createFromFormat('Y-m-d','2014-08-31');
    $periodo->ur = 5;
    $lista = $this->financeiro->getCustos($periodo);
    $this->assertNotNull($lista);
    $this->assertNotEmpty($lista);
  }

}