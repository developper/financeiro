<?php

namespace Test\Core;

use Test\AbstractTest,
    App\Core\Url;

class UrlTest extends AbstractTest
{

  public function testFaturamentoIndexExists()
  {
    $url = Url::get('faturamento-index');
    $this->assertEquals('/faturamento/index.php', $url);
  }

  public function testFaturamentoGerenciarLoteExists()
  {
    $url = Url::get('faturamento-gerenciar-lote');
    $this->assertEquals('/faturamento/?view=gerenciarlote', $url);
  }

  public function testProcessarXML($capalote = '025.140613.124706')
  {
    $url = Url::get('tiss-processar-xml', ['capalote' => $capalote]);
    $this->assertEquals($url, '/faturamento/tissxml/index.php?action=processar-xml&capalote=' . $capalote);
  }

} 