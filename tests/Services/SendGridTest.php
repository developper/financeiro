<?php

namespace Test;

use App\Services\SendGrid;

class SendGridTest extends AbstractTest
{

	public function testGetSendGridAPIData()
	{
		$sendGrid = new SendGrid();
		$sendGridApiMock = (object) array(
			'key_name'	=> 'mederikey',
			'api_key'		=> 'e7I1GNWTTDeoHcjuzQy1Tg',
			'host' 			=> 'https://api.sendgrid.com/',
			'user' 			=> 'mederisender',
			'pass' 			=> 'mederi@2015',
		);
		$this->assertEquals($sendGrid->api_data, $sendGridApiMock);
	}

}