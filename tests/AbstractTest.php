<?php

namespace Test;

include(__DIR__ . '/../db/config.php');

abstract class AbstractTest extends \PHPUnit_Framework_TestCase
{
    public $ROOT_PATH;

    protected function setUp()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $this->ROOT_PATH = dirname(__DIR__);
    }
}