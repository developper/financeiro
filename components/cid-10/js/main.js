$(function($) {
    var iCanUseOn = !!$.fn.on;

    $('#buscar-cid-10').typeahead({
        hint: true,
        highlight: true,
        minLength: 3
}, {
        name: 'itens',
        display: 'value',
        limit: 100,
        source: function(query,teste,response) {
            var x = $.getJSON('/exames/?action=buscar-cid-10', {
                term: $('#buscar-cid-10').val()
            },response);
            return x;
        }
    }).on('typeahead:select', function(event, data) {
        $('#buscar-cid-10')
            .val(data.value)
            .attr('cid-10-id', data.id);
        $('#cid').val(data.id);
    });
});