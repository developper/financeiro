$(function($) {
    var iCanUseOn = !!$.fn.on;


    //function busca(){
    //    var itens ='';
    //    console.log($('#tipo-item').val());
    //  //  itens.initialize();
    //    itens = new Bloodhound({
    //        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    //        queryTokenizer: Bloodhound.tokenizers.whitespace,
    //        remote: {
    //            wildcard: '%QUERY',
    //            url: '/enfermagem/busca_brasindice.php?term=%QUERY&tipo=' + $('#tipo-item').val(),
    //            transform: function(response) {
    //                return $.map(response, function(item) {
    //                    return {
    //                        value: item.value,
    //                        catalogo_id: item.catalogo_id,
    //                        tipo: item.tipo,
    //                        tiss: item.id
    //                    };
    //                });
    //            }
    //        }
    //    });
    //
    //    console.log
    //    return itens;
    //
    //}



   // itens.initialize();
   // itens.initialize();
   //
   //     $('#buscar-item').typeahead(null, {
   //         name: 'itens',
   //         display: 'value',
   //         source: busca()
   //
   //     }).on('typeahead:select', function(event, data) {
   //         $('#buscar-item')
   //             .val(data.value)
   //             .attr('catalogo-id', data.catalogo_id)
   //             .attr('item-tipo', data.tipo)
   //             .attr('item-tiss', data.tiss);
   //         $('#qtd-item').focus();
   //     });
   // $("#buscar-item").autocomplete({
   //     source: function(request, response) {
   //         $.getJSON('../enfermagem/busca_brasindice.php', {
   //             term: request.term,
   //             tipo: 0,
   //             tipo_extra: 1
   //         }, response);
   //     },
   //     minLength: 3,
   //     select: function(event, ui) {
   //         $(this).attr('data-catalogo-id', ui.item.catalogo_id);
   //         $(this).attr('data-item-name', ui.item.value);
   //         $(this).val(ui.item.value);
   //         $(this).parent().parent().find('input.cp-qtd-item').focus();
   //         return false;
   //     }
   // });



    $('#buscar-item').typeahead({
        hint: true,
        highlight: true,
        minLength: 3,

}, {

        name: 'itens',
        display: 'value',
        limit: 100,
        source: function(query,teste,response) {
            var x = $.getJSON('../enfermagem/busca_brasindice.php', {
                term: $('#buscar-item').val(),
                tipo: $('#tipo-item').val()

            },response);

            return x;
        }


    }).on('typeahead:select', function(event, data) {
        $('#buscar-item')
            .val(data.value)
            .attr('catalogo-id', data.catalogo_id)
            .attr('item-tipo', data.tipo)
            .attr('item-tiss', data.id || '');
        $('#qtd-item').focus();
    });
    $("#tipo-item").change(function () {

    });


    //$("#tipo-item").change(function () {
    //    tipoItem = $(this).val();
    //
    //    itens = new Bloodhound({
    //        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    //        queryTokenizer: Bloodhound.tokenizers.whitespace,
    //        remote: {
    //            wildcard: '%QUERY',
    //            url: '/enfermagem/busca_brasindice.php?term=%QUERY&tipo=' + tipoItem,
    //            transform: function(response) {
    //                return $.map(response, function(item) {
    //                    return {
    //                        value: item.value,
    //                        catalogo_id: item.catalogo_id,
    //                        tipo: item.tipo,
    //                        tiss: item.id
    //                    };
    //                });
    //            }
    //        }
    //    });
    //
    //    itens.initialize();
    //
    //    $('#buscar-item').typeahead(null, {
    //        name: 'itens',
    //        display: 'value',
    //        source: itens.ttAdapter()
    //    }).on('typeahead:select', function(event, data) {
    //        $('#buscar-item')
    //            .val(data.value)
    //            .attr('catalogo-id', data.catalogo_id)
    //            .attr('item-tipo', data.tipo)
    //            .attr('item-tiss', data.tiss);
    //        $('#qtd-item').focus();
    //    });
    //});

    if(iCanUseOn) {
        $("#adicionar-item").on('click', function () {
            var nome, catalogo_id, tiss, tipo, qtd, tipoList, $buscar, $qtd;
            $buscar = $("#buscar-item");
            $qtd = $("#qtd-item");
            tipoList = {
                0: 'Medicamento',
                1: 'Material',
                3: 'Dieta',
                5: 'Equipamento'
            };
            if ($qtd.val() > 0 && typeof $buscar.attr('catalogo-id') != 'undefined') {
                nome = $buscar.val();
                catalogo_id = $buscar.attr('catalogo-id');
                tipo = $buscar.attr('item-tipo');
                tiss = $buscar.attr('item-tiss');
                qtd = $qtd.val();
                $(".itens-added").append(
                    '<tr>' +
                    '   <td>' +
                        '<span title="Remover Item" style="font-size: 6px" class="btn btn-danger remover-item-add-item glyphicon glyphicon-remove"></span> '+
                        tipoList[tipo] +
                        '</td>' +
                    '   <td>' +
                    '(' + tiss + ') ' +
                    nome +
                    '<input name="catalogo_id[]" type="hidden" value="' + catalogo_id + '">' +
                    '<input name="tiss[]" type="hidden" value="' + tiss + '">' +
                    '<input name="tipo[]" type="hidden" value="' + tipo + '">' +
                    '<input name="qtd[]" type="hidden" value="' + qtd + '">' +
                    '   </td>' +
                    '   <td>' + qtd + '</td>' +
                    '   </tr>'
                );

                $buscar.removeAttr('catalogo-id item-tipo item-tiss').val('');
                $("#qtd-item").val('');
                return false;
            }
            alert('Escolha um item e coloque uma quantidade maior do que zero!');
        });

    } else {
        $("#adicionar-item").live('click', function () {
            var nome, catalogo_id, tiss, tipo, tipoList, $buscar, $qtd;
            $buscar = $("#buscar-item");
            $qtd = $("#qtd-item");
            tipoList = {
                0: 'Medicamento',
                1: 'Material',
                3: 'Dieta',
                5: 'Equipamento'
            };
            if ($qtd.val() > 0) {
                nome = $buscar.val();
                catalogo_id = $buscar.attr('catalogo-id');
                tipo = $buscar.attr('item-tipo');
                tiss = $buscar.attr('item-tiss');
                $(".itens-added").append(
                    '<tr>' +
                    '   <td>' +
                    '<span title="Remover Item" style="font-size: 6px" class="btn btn-danger remover-item-add-item glyphicon glyphicon-remove"></span> '+
                    tipoList[tipo] + '</td>' +
                    '   <td>' +
                    '(' + tiss + ') ' +
                    nome +
                    '<input name="catalogo_id[]" type="hidden" value="' + catalogo_id + '">' +
                    '<input name="tiss[]" type="hidden" value="' + tiss + '">' +
                    '<input name="tipo[]" type="hidden" value="' + tipo + '">' +
                    '   </td>' +
                    '   <td>' + $qtd.val() + '</td>' +
                    '   </tr>'
                );

                $buscar.removeAttr('catalogo-id item-tipo item-tiss').val('');
                $("#qtd-item").val('');
                return false;
            }
            alert('A quantidade tem que ser maior do que zero!');
        });


    }

    $(".remover-item-add-item").live('click',function(){
        $(this).parent().parent().remove();
    });

});