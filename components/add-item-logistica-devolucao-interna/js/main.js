$(function($) {
    var iCanUseOn = !!$.fn.on;

    $("#adicionar-iten , #buscar-item").click(function(){
        $("#buscar-item").removeAttr('catalogo-id item-tipo item-tiss').val('');
        $("#qtd-item").val('');
        $("#lote-item").val('');
        $("#validade-item").val('');
        $("#observacao-item").val('');
        $('#select-motivo').val(-1);
    });


    $('#buscar-item').typeahead({
        hint: true,
        highlight: true,
        minLength: 3,

}, {

        name: 'itens',
        display: 'value',
        limit: 100,
        source: function(query,teste,response) {


            var x = $.getJSON('../busca_brasindice_pedido.php', {
                term: $('#buscar-item').val(),
                tipo: $('#tipo-item').val()
            },response);
            $('#buscar-item').attr('catalogo-id','');

            return x;
        }


    }).on('typeahead:select', function(event, data) {
        $('#buscar-item')
            .val(data.value)
            .attr('catalogo-id', data.catalogo_id)
            .attr('item-tipo', data.tipo)
            .attr('item-tiss', data.id || '');
        $('#qtd-item').focus();
    });

    $("#tipo-item").change(function () {
        $("#buscar-item").removeAttr('catalogo-id item-tipo item-tiss').val('');


    });




    if(iCanUseOn) {
        $("#adicionar-item").on('click', function () {

            var nome, catalogo_id, lote, validade, tiss, tipo, qtd, tipoList, $buscar, $qtd, motivo, selectMotivo, observacao;
            $buscar = $("#buscar-item");
            $qtd = $("#qtd-item");
            lote = $("#lote-item").val();
            validade = $("#validade-item").val();
            motivo = $("#select-motivo").val();
            observacao = $("#observacao-item").val();
            $('#select-motivo').children('option:selected').attr('selected',true);

            selectMotivo =  document.getElementById("div-select-motivo").innerHTML;
            selectMotivo = selectMotivo.replace('select-motivo','');
            selectMotivo = selectMotivo.replace('nome-motivo-item','motivo[]');

            tipoList = {
                0: 'Medicamento',
                1: 'Material',
                3: 'Dieta',
                5: 'Equipamento'
            };
            if ($qtd.val() > 0 && typeof $buscar.attr('catalogo-id') != 'undefined' && motivo > 0) {
                nome = $buscar.val();
                catalogo_id = $buscar.attr('catalogo-id');
                tipo = $buscar.attr('item-tipo');
                tiss = $buscar.attr('item-tiss');
                qtd = $qtd.val();
                classObgValidade = tipo != 5 ? 'OBG' :'';

                $("#body-"+tipo).append(
                    '<tr>' +
                        '<td>' +

                            '<div  class="col-md-12 form-group">' +
                            '<span title="Remover Item" style="font-size: 6px" class="btn btn-danger remover-item-add-item glyphicon glyphicon-remove"></span> '+
                            ' (' + tiss + ') ' +
                            nome +
                            '</div>' +


                            '<div  class="col-md-2 form-group">'  +

                                 '<h4><span class="label-info label">'+tipoList[tipo]+'</span></h4>'+
                                '<input name="catalogo_id[]" type="hidden" value="' + catalogo_id + '">' +
                                '<input name="tiss[]" type="hidden" value="' + tiss + '">' +
                                '<input name="tipo[]" type="hidden" value="' + tipo + '">' +
                            '</div>' +
                            '<div class="col-md-2 form-group">' +
                                '<label class="control-label" for="tipo-item">Quantidade:</label>' +
                                '<input name="qtd[]" required type="number" class="form-control" min = "1" value="' + qtd + '">' +
                            '</div>' +
                            '<div class="col-md-4 form-group">' +
                                '<label class="control-label" for="tipo-item">Lote:</label>' +
                                '<input name="lote[]" type="text" class="form-control OBG" value="' + lote + '">'
                            +'</div>' +
                            '<div class="col-md-4 form-group">' +
                                '<label class="control-label " for="tipo-item">Validade:</label>' +
                                '<input name="validade[]" type="date" class="form-control col-md-12 '+classObgValidade+'" value="' + validade + '">'+
                            '</div>' +
                             '</div>' +
                            '<div class="col-md-6">'+selectMotivo+'</div>'+
                            '<div class="col-md-6">' +
                             '<label>Observação:</label><br><textarea class="form-control" name="observacao[]">'+observacao+'</textarea>'+
                            '</div>'+


                        '</td>' +
                    '</tr>'
                );

                //$buscar.removeAttr('catalogo-id item-tipo item-tiss').val('');
                $("#qtd-item").val('');
                $("#lote-item").val('');
                $("#validade-item").val('');
                $("#observacao-item").val('');
                $('#select-motivo').val(-1);
                return false;

            }
                alert('Escolha um item no campo buscar, o campo quantidade' +
                    ' deve ser maior que zero e um motivo deve ser selecionado!');



        });

    } else {
        $("#adicionar-item").on('click', function () {

            var nome, catalogo_id, lote, validade, tiss, tipo, qtd, tipoList, $buscar, $qtd, motivo, selectMotivo, observacao;
            $buscar = $("#buscar-item");
            $qtd = $("#qtd-item");
            lote = $("#lote-item").val();
            validade = $("#validade-item").val();
            motivo = $(".select-motivo").val();
            observacao = $("#observacao-item").val();


            $('.select-motivo').children('option:selected').attr('selected',true);
            selectMotivo =  $("#div-select-motivo").innerHTML();
            console.log(selectMotivo);
            return false;
            tipoList = {
                0: 'Medicamento',
                1: 'Material',
                3: 'Dieta',
                5: 'Equipamento'
            };
            if ($qtd.val() > 0 && typeof $buscar.attr('catalogo-id') != 'undefined' && motivo > 0) {
                nome = $buscar.val();
                catalogo_id = $buscar.attr('catalogo-id');
                tipo = $buscar.attr('item-tipo');
                tiss = $buscar.attr('item-tiss');
                qtd = $qtd.val();

                $("#body-"+tipo).append(
                    '<tr>' +
                    '<td>' +

                    '<div  class="col-md-12 form-group">' +
                    '<span title="Remover Item" style="font-size: 6px" class="btn btn-danger remover-item-add-item glyphicon glyphicon-remove"></span> '+
                    ' (' + tiss + ') ' +
                    nome +
                    '</div>' +


                    '<div  class="col-md-2 form-group">'  +
                    '<label class="control-label" for="tipo-item">Tipo:</label><br>' +
                    '<h4><span class="label-warning label">'+tipoList[tipo]+'</span></h4>'+
                    '<input name="catalogo_id[]" type="hidden" value="' + catalogo_id + '">' +
                    '<input name="tiss[]" type="hidden" value="' + tiss + '">' +
                    '<input name="tipo[]" type="hidden" value="' + tipo + '">' +
                    '</div>' +
                    '<div class="col-md-2 form-group">' +
                    '<label class="control-label" for="tipo-item">Quantidade:</label>' +
                    '<input name="qtd[]" required type="number" class="form-control OBG" min = "1" value="' + qtd + '">' +
                    '</div>' +
                    '<div class="col-md-4 form-group">' +
                    '<label class="control-label" for="tipo-item">Lote:</label>' +
                    '<input name="lote[]" type="text" class="form-control " value="' + lote + '">'
                    +'</div>' +
                    '<div class="col-md-4 form-group">' +
                    '<label class="control-label " for="tipo-item">Validade:</label>' +
                    '<input name="validade[]" type="date" class="form-control col-md-12" value="' + validade + '">'+
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-6">'+selectMotivo+'</div>'+
                    '<div class="col-md-6">' +
                    '<label>Observação:</label><br><textarea class="form-control" name="observacao[]">'+observacao+'</textarea>'+
                    '</div>'+


                    '</td>' +
                    '</tr>'
                );

                //$buscar.removeAttr('catalogo-id item-tipo item-tiss').val('');
                $("#qtd-item").val('');
                $("#lote-item").val('');
                $("#validade-item").val('');
                $("#observacao-item").val('');
                $(".modal-body").find('.select-motivo').val(-1);
                return false;

            }
            alert('Escolha um item no campo buscar, o campo quantidade' +
                ' deve ser maior que zero e um motivo deve ser selecionado!');

        });

    }

    $(".remover-item-add-item").live('click',function(){
        $(this).parent().parent().remove();
    });

});