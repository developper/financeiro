$(function($) {
    var mapGrupo2, mapGrupo3;

    $(".katz").change(function () {
        var total_katz = 0, valor;
        $(".katz").each(function () {
            if($(this).is(':checked')){
                valor = parseInt($(this).val());
                total_katz += valor;
            }
        });
        if(total_katz > 0 && total_katz <= 2){
            $("input[name='katzsepontuar2']").attr("checked", true);
        } else {
            $("input[name='katzsepontuar2']").attr("checked", false);
        }
        $("#pontuacao-katz").attr('pontuacao', total_katz).html(total_katz);
    });



    $(".nead2016").change(function () {
        var total_nead = 0, valor;
        $(".nead2016").each(function () {
            if($(this).is(':checked')){
                valor = parseInt($(this).attr('peso'));
                total_nead += valor;
            }
        });
        $("#pontuacao-nead-2016").attr('pontuacao', total_nead).html(total_nead);
    });

    $('.grupo1, .grupo2, .grupo3, .katz, .classificacao-paciente').dblclick(function(){
        $(this).attr('checked',false);
        calculaNEAD2016();
        var total_nead = 0, valor;
        $(".nead2016").each(function () {
            if($(this).is(':checked')){
                valor = parseInt($(this).attr('peso'));
                total_nead += valor;
            }
        });
        $("#pontuacao-nead-2016").attr('pontuacao', total_nead).html(total_nead);

    });

    $(".ventilacao-mecanica, .alimentacao-parenteral, .aspiracao, .medicacao-hipodermoclise,.nead2016").change(function () {

        calculaNEAD2016();
    });

    function calculaNEAD2016() {
        var radiosGroupo2 = $(".ventilacao-mecanica:checked, .alimentacao-parenteral:checked, .aspiracao:checked, .medicacao-hipodermoclise:checked");
        var parecer;
        var radiosGroupo3 = $(".nead2016:checked");

        mapGrupo2 = $.map(radiosGroupo2, function(el) {
            return el.value;
        });
        var valoresGrupo2Ordenado = mapGrupo2.sort();
        // 0 Internação 24 horas, 1 Internação 12 horas, 3 outros pad.
        var tipoGrupo2 =valoresGrupo2Ordenado[0];

        var mapGrupo3 = $.map(radiosGroupo3, function(el) {
            return el.getAttribute('peso');
        });

        var total = '';
        if(mapGrupo3.length > 0) {

            var total = mapGrupo3.reduce(function (tot, val) {
                tot = parseInt(tot);
                val = parseInt(val);
                return tot + val;
            });
        }
        if(tipoGrupo2 == 0){
            parecer =  ' Paciente de Alta complexidade, segundo score NEAD. Indicação de Internação Domiciliar 24h.';
        }else if(total >= 18){
            parecer =  ' Paciente de Alta complexidade, segundo score NEAD. Indicação de Internação Domiciliar 24h.';
        }else if(tipoGrupo2 == 1){
            parecer =  ' Paciente de Média complexidade, segundo score NEAD. Indicação de Internação Domiciliar 12h.';
        }else if( total >= 12 &&  total < 18){
            parecer =  ' Paciente de Média complexidade, segundo score NEAD. Indicação de Internação Domiciliar 12h.';
        }else if(total > 5 && total < 12){
            parecer = ' Considerar Atendimento Domiciliar Multiprofissioanl (Incluino procedimentos pontuais, desde que não exclusivos)';
        }else if(total <= 5){
            parecer =  'Indicação de Assistência Domiciliar/Serviços Pontuais.';
        }else if(tipoGrupo2 == 3){
            parecer =  'Indicação de Assistência Domiciliar/Serviços Pontuais.';
        }

        $('#parecer-nead2016').text(parecer);
        $("#prejustparecer").val(parecer);
    }

    //$(".nead2016").change(function () {
    //    var radios = $(".nead2016:checked"), parecer;
    //
    //    var mapGrupo3 = $.map(radios, function(el) {
    //        return el.getAttribute('peso');
    //    });
    //
    //    var total = mapGrupo3.reduce(function(tot, val) {
    //        tot = parseInt(tot);
    //        val = parseInt(val);
    //        return tot + val;
    //    });
    //
    //    if($.inArray('0', mapGrupo2) == -1) {
    //        $(".modalidade").attr("checked", false);
    //        if(total < 6) {
    //            $(".modalidade").attr("checked", false);
    //            $(".modalidade[value='1']").attr("checked", true);
    //            parecer =  ' Paciente de Baixa complexidade, segundo score NEAD = ' + total + '. Indicação de Assistência Domiciliar/Serviços Pontuais.';
    //        } else if(total > 5 && total < 12){
    //            $(".modalidade").attr("checked", false);
    //            $(".modalidade[value='2']").attr("checked", true);
    //            parecer = ' Paciente de Média complexidade, segundo score NEAD = ' + total + '. Indicação de Internação Domiciliar 6h.';
    //        } else if(total > 11 && total < 18){
    //            $(".modalidade").attr("checked", false);
    //            $(".modalidade[value='3']").attr("checked", true);
    //            parecer =  ' Paciente de Média complexidade, segundo score NEAD = ' + total + '. Indicação de Internação Domiciliar 12h.';
    //        } else if(total >= 18) {
    //            $(".modalidade").attr("checked", false);
    //            $(".modalidade[value='5']").attr("checked", true);
    //            parecer =  ' Paciente de Alta complexidade, segundo score NEAD = ' + total + '. Indicação de Internação Domiciliar 24h sem Respirador.';
    //        }
    //    }
    //
    //
    //    $('#parecer-nead2016').text(parecer);
    //
    //    var observacao_outro_score = $(".observacao_score").text() + " " + parecer;
    //
    //
    //    $("#prejustparecer").val(observacao_outro_score);
    //
    //});
});