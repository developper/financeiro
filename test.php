<?php


class GuiaSPSADT
{
    private $identificacaoGuiaSADTSP;
    private $numeroGuiaPrincipal;
    private $dadosAutorizacao;
    private $dadosBeneficiario;
    private $dadosSolicitante;
    private $prestadorExecutante;
    private $caraterAtendimento;
    private $dataHoraAtendimento;
    private $diagnosticoAtendimento;
    private $tipoSaida;
    private $tipoAtendimento;
    private $procedimentosRealizados;
    private $valorTotal;

    /**
     * @param mixed $caraterAtendimento
     */
    public function setCaraterAtendimento($caraterAtendimento)
    {
        $this->caraterAtendimento = $caraterAtendimento;
    }

    /**
     * @return mixed
     */
    public function getCaraterAtendimento()
    {
        return $this->caraterAtendimento;
    }

    /**
     * @param mixed $dadosAutorizacao
     */
    public function setDadosAutorizacao($dadosAutorizacao)
    {
        $this->dadosAutorizacao = $dadosAutorizacao;
    }

    /**
     * @return mixed
     */
    public function getDadosAutorizacao()
    {
        return $this->dadosAutorizacao;
    }

    /**
     * @param mixed $dadosBeneficiario
     */
    public function setDadosBeneficiario($dadosBeneficiario)
    {
        $this->dadosBeneficiario = $dadosBeneficiario;
    }

    /**
     * @return mixed
     */
    public function getDadosBeneficiario()
    {
        return $this->dadosBeneficiario;
    }

    /**
     * @param mixed $dadosSolicitante
     */
    public function setDadosSolicitante($dadosSolicitante)
    {
        $this->dadosSolicitante = $dadosSolicitante;
    }

    /**
     * @return mixed
     */
    public function getDadosSolicitante()
    {
        return $this->dadosSolicitante;
    }

    /**
     * @param mixed $dataHoraAtendimento
     */
    public function setDataHoraAtendimento($dataHoraAtendimento)
    {
        $this->dataHoraAtendimento = $dataHoraAtendimento;
    }

    /**
     * @return mixed
     */
    public function getDataHoraAtendimento()
    {
        return $this->dataHoraAtendimento;
    }

    /**
     * @param mixed $diagnosticoAtendimento
     */
    public function setDiagnosticoAtendimento($diagnosticoAtendimento)
    {
        $this->diagnosticoAtendimento = $diagnosticoAtendimento;
    }

    /**
     * @return mixed
     */
    public function getDiagnosticoAtendimento()
    {
        return $this->diagnosticoAtendimento;
    }

    /**
     * @param mixed $identificacaoGuiaSADTSP
     */
    public function setIdentificacaoGuiaSADTSP($identificacaoGuiaSADTSP)
    {
        $this->identificacaoGuiaSADTSP = $identificacaoGuiaSADTSP;
    }

    /**
     * @return mixed
     */
    public function getIdentificacaoGuiaSADTSP()
    {
        return $this->identificacaoGuiaSADTSP;
    }

    /**
     * @param mixed $numeroGuiaPrincipal
     */
    public function setNumeroGuiaPrincipal($numeroGuiaPrincipal)
    {
        $this->numeroGuiaPrincipal = $numeroGuiaPrincipal;
    }

    /**
     * @return mixed
     */
    public function getNumeroGuiaPrincipal()
    {
        return $this->numeroGuiaPrincipal;
    }

    /**
     * @param mixed $prestadorExecutante
     */
    public function setPrestadorExecutante($prestadorExecutante)
    {
        $this->prestadorExecutante = $prestadorExecutante;
    }

    /**
     * @return mixed
     */
    public function getPrestadorExecutante()
    {
        return $this->prestadorExecutante;
    }

    /**
     * @param mixed $procedimentosRealizados
     */
    public function setProcedimentosRealizados($procedimentosRealizados)
    {
        $this->procedimentosRealizados = $procedimentosRealizados;
    }

    /**
     * @return mixed
     */
    public function getProcedimentosRealizados()
    {
        return $this->procedimentosRealizados;
    }

    /**
     * @param mixed $tipoAtendimento
     */
    public function setTipoAtendimento($tipoAtendimento)
    {
        $this->tipoAtendimento = $tipoAtendimento;
    }

    /**
     * @return mixed
     */
    public function getTipoAtendimento()
    {
        return $this->tipoAtendimento;
    }

    /**
     * @param mixed $tipoSaida
     */
    public function setTipoSaida($tipoSaida)
    {
        $this->tipoSaida = $tipoSaida;
    }

    /**
     * @return mixed
     */
    public function getTipoSaida()
    {
        return $this->tipoSaida;
    }

    /**
     * @param mixed $valorTotal
     */
    public function setValorTotal($valorTotal)
    {
        $this->valorTotal = $valorTotal;
    }

    /**
     * @return mixed
     */
    public function getValorTotal()
    {
        return $this->valorTotal;
    }

}