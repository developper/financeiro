/**
 * Created by jeferson on 17/10/2017.
 */

function validar_campos(div){
  
    var ok = true;
    $("#"+div+" .OBG").each(function (i){

        if(this.value == ""){
            ok = false;
            this.style.border = "3px solid red";
        } else {
            this.style.border = "";
        }

    });

    $("#"+div+" .COMBO_OBG option:selected").each(function (i){
        if(this.value == "-1"){
            ok = false;
            $(this).parent().css({"border":"3px solid red"});
        } else {
            $(this).parent().css({"border":""});
        }
    });

    $("#"+div+" .OBG_CHOSEN option:selected").each(function (i){

        if($(this).val() == "" || $(this).val() == -1){
            ok = false;
            $(this).parent().parent().children('div').css({"border":"3px solid red"});
        } else {
            var estilo = $(this).parent().parent().children('div').attr('style');
            $(this).parent().parent().children('div').removeAttr('style');
            if(typeof estilo != 'undefined') {
                $(this).parent().parent().children('div').attr('style', estilo.replace("border: 3px solid red", ''));
            } else {
                $(this).parent().parent().children('div').attr('style', "border: 3px solid red");
            }
        }
    });

    $("#"+div+" .OBG_CHOSEN_MULTIPLE").each(function (i) {
        var hasEmptySelect = $(this)
                .find("option:selected")
                .map(function (idx, elem) {
                    return elem.value;
                })
                .filter(function (value) {
                    return value === 0;
                })
                .length > 0;

        if (!hasEmptySelect) {
            ok = false;
            $(this).parent().children('div').css({"border": "3px solid red"});
        } else {
            var estilo = $(this).parent().children('div').attr('style');
            $(this).parent().children('div').removeAttr('style');
            $(this).parent().children('div').attr('style', estilo.replace("border: 3px solid red", ''));
        }
    });

    if(!ok){
        $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
        $("#"+div+" .div-aviso").show();
    } else {
        $("#"+div+" .aviso").text("");
        $("#"+div+" .div-aviso").hide();
    }
    return ok;
}

