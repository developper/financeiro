

    function validar_vencimento_real(vencimento,vencimentoReal){
        
        var arrayFeriadosFixos = [
            '01/01',                                
            '21/04',
            '01/05',            
            '24/06',
            '02/07',
            '07/09',
            '12/10',
            '02/11',
            '15/11',
            '08/12',
            '25/12',
        ];

        var arrayFeriadosMutaveis = [
            '12/02/2024', // carnaval
        '13/02/2024', //carnaval
        '30/05/2024', //Corpus Christi
        '29/03/2024' // paixão de cristo

        ]
        var novoVencimentoReal = vencimentoReal;
        if(vencimentoReal instanceof Date && !isNaN(vencimentoReal.valueOf())){
            novoVencimentoReal = vencimentoReal;
        }else if(vencimento instanceof Date && !isNaN(vencimento.valueOf())){            
            novoVencimentoReal = vencimento;
        }else{
            return '';

        }
        
        
        let isValid = false;
        if(novoVencimentoReal.getDay() == 0 || novoVencimentoReal.getDay() == 6 ||
        arrayFeriadosFixos.indexOf(moment(novoVencimentoReal).format('DD/MM')) != -1 || arrayFeriadosMutaveis.indexOf(moment(novoVencimentoReal).format('DD/MM/YYYY')) != -1){
            while(isValid == false){
                if(novoVencimentoReal.getDay() == 0){           
                        novoVencimentoReal = new Date(novoVencimentoReal.setDate(novoVencimentoReal.getDate() + 1));               
                    }else if(novoVencimentoReal.getDay() == 6) {
                        novoVencimentoReal = new Date(novoVencimentoReal.setDate(novoVencimentoReal.getDate() + 2)); 
                    } 
                    
                    if(arrayFeriadosMutaveis.indexOf(moment(novoVencimentoReal).format('DD/MM/YYYY')) != -1){
                        novoVencimentoReal = new Date(novoVencimentoReal.setDate(novoVencimentoReal.getDate() + 1));
                    }else if(arrayFeriadosFixos.indexOf(moment(novoVencimentoReal).format('DD/MM')) != -1){
                        novoVencimentoReal = new Date(novoVencimentoReal.setDate(novoVencimentoReal.getDate() + 1));                    
                     }else{
                        break;
                    }

            }
        }
        
         
        
        return novoVencimentoReal;


    }


