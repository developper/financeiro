$(function ($) {

  var allowed = [
    'newcapmed',
    'anterior_capmed',
    'listcapmed',

    ];
    /*'newfichaevolucao',
    'listfichaevolucao',
    'anterior_evolucao',
    'editar_evolucao'
  ];*/

  if(allowed.indexOf(queryObj().act) !== -1 || allowed.indexOf(queryObj().op) > -1 ) {
    var keyMatch = 0,
      attrName = '',
      attrClass = '',
      i = 0,
      regex = /[\s_!?/.]/g;

    $('form[rel="persist"] input[type!="hidden"], select, textarea').each(function (e) {
      if(typeof $(this).attr('id') === 'undefined') {
        if (typeof $(this).attr('name') !== 'undefined') {
          if ((attrName !== $(this).attr('name')) && i > 0) {
            i = 0;
            $(this).attr('id', $(this).attr('name').replace(regex,'-') + '-' + i);
          } else {
            i++;
            $(this).attr('id', $(this).attr('name').replace(regex,'-') + '-' + i);
          }
          attrName = $(this).attr('name');
        } else if(typeof $(this).attr('class') === 'undefined') {
          if ((attrClass !== $(this).attr('class').split(' ')[0]) && i > 0) {
            i = 0;
            $(this).attr('id', $(this).attr('class').split(' ')[0].replace(regex,'-') + '-' + i);
          } else {
            i++;
            $(this).attr('id', $(this).attr('class').split(' ')[0].replace(regex,'-') + '-' + i);
          }
          attrClass = $(this).attr('class').split(' ')[0];
        } else {
          $(this).attr('id','NEW' + '-' + i);
        }
      }
    });

    for (var key in localStorage){
      if(key.indexOf(document.location) > 0){
        keyMatch++;
      }
    }
    if(keyMatch > 0){
      $("#dialog-aviso-localstorage").dialog({
        autoOpen: true,
        modal: true,
        width: 500,
        buttons: {
          "Limpar": function () {
            for (var key in localStorage){
              if(key.indexOf(document.location) > 0){
                localStorage.removeItem(key);
              }
            }
            $(this).dialog("close");
            location.reload();
          },
          "Manter": function () {
            $(this).dialog("close");
          }
        }
      });
    }

    $('[rel="persist"]').garlic({
      getPath: function ( $elem ) {
          return "garlic:" + document.location + '>' + $elem.attr('id');
      }
    });

    $('form input, select, textarea').blur(function (e) {
      if($(this).attr('value') !== '')
        $("#saving").fadeIn( 200 ).delay(2000).fadeOut( 200 );
    });
  }
});