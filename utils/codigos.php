<?php
$id = session_id();
if(empty($id))
	session_start();
include_once(__DIR__ . '/../db/config.php');
include_once(__DIR__ . '/class.phpmailer.php');
require __DIR__ . '/../vendor/autoload.php';
use App\Core\Config;





function anti_injection($sql,$validacao) {
	switch($validacao){
		case 'numerico':
			if(!is_numeric($sql)){ echo "$sql nao eh numero"; echo "<script> history.back(); </script>"; exit;}
			break;
		case 'literal':

			$sql = preg_replace("/(from|select|insert|delete|where|drop table|show tables| OR | AND | or | and |#|\*|--|\\\\)/","",$sql);
			$sql = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($sql) : mysql_escape_string($sql);
			$sql = trim($sql);//limpa espaços vazio
			$sql = strip_tags($sql);//tira tags html e php
			//$sql = addslashes($sql);//Adiciona barras invertidas a uma string
			break;
	}
	return $sql;
}

function assinaturaProResponsavel($id){
	$sql_assinatura = "
                SELECT
                    usuarios.ASSINATURA
                FROM                    
                    usuarios
                WHERE
                    idUsuarios = $id";
	$result_assinatura = mysql_query($sql_assinatura);
	$assinatura = mysql_result($result_assinatura, 0, 0);
	$html = "<br/><br/><table width='100%'><tr><td><center><img src='{$assinatura}' width='20%'></center></td></tr></table>";
	return $html;
}

function savelog($msg){
	$usr = $_SESSION['id_user'];
	$sql = "INSERT INTO loguser (`log`, `user`, `data`) VALUES ('{$msg}', '{$usr}', now())";
	mysql_query($sql);
}

function alerta(){
	return '';
	$code = "<div class='div-aviso ui-widget'><div class='ui-state-error ui-corner-all' style='padding: 0 .7em;'><p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;'></span>";
	$code .= "<span class='aviso' ></span></p></div></div>";
	return $code;
}

function info($msg){
	$code = "<div class='ui-widget'><div class='ui-state-highlight ui-corner-all' style='padding: 0 .7em;'><p><span class='ui-icon ui-icon-info' style='float: left; margin-right: .3em;'></span>";
	$code .= "<span>{$msg}</span></p></div></div>";
	return $code;
}

function button($id,$label,$extra = NULL){
	$code = "<button id='{$id}' {$extra} >{$label}</button>";
	return $code;
}

function input($type,$id,$name,$extra = NULL){
	$code = "<input type='{$type}' id='{$id}' name='{$name}' {$extra} />";
	return $code;
}

function combo_servicos($name){
	$result = mysql_query("SELECT * FROM cobrancaplanos ORDER BY item;");
	$code = "<select name='{$name}' class='COMBO_OBG'  >";
	$code .= "<option value='-1'></option>";
	while($row = mysql_fetch_array($result))
	{
		$code .= "<option value='{$row['id']}'>{$row['item']} ({$row['unidade']})</option>";
	}
	$code .= "</select>";
	return $code;
}

function combo_paciente($name){
	$result = mysql_query("SELECT * FROM clientes ORDER BY nome;");
	$code = "<select name='{$name}'  >";
	while($row = mysql_fetch_array($result))
	{
		$code .= "<option value='". $row['idClientes'] ."'>". $row['nome'] ."</option>";
	}
	$code .= "</select>";
	return $code;
}

function combo_fornecedor($name,$selected = NULL){
	$result = mysql_query("SELECT * FROM fornecedores ORDER BY razaoSocial");
	$code = "<select name='{$name}'  >";
	while($row = mysql_fetch_array($result)){
		if($selected != NULL && strcasecmp($selected,$row['razaoSocial']) == 0)
			$code .= "<option cpfcnpj='{$row['cnpj']}' value='{$row['idFornecedores']}' SELECTED>{$row['razaoSocial']}</option>";
		else
			$code .= "<option cpfcnpj='{$row['cnpj']}' value='{$row['idFornecedores']}'>{$row['razaoSocial']}</option>";
	}
	$code .= "</select>";
	return $code;
}

function combo_usuarios($name){
	$result = mysql_query("SELECT * FROM usuarios ORDER BY nome");
	$code = "<select name='{$name}'  >";
	$code .= "<option value='0'>TODOS</option>";
	while($row = mysql_fetch_array($result)){
		$code .= "<option value='{$row['idUsuarios']}'>{$row['nome']}</option>";
	}
	$code .= "</select>";
	return $code;
}

function radio_box($name,$opcoes,$checked){
	$code = "";
	foreach($opcoes as $key => $op){
		if($op == $checked)
			$code .= "<input type='radio' name='{$name}' value='{$op}' checked />{$key}";
		else $code .= "<input type='radio' name='{$name}' value='{$op}' />{$key}";
	}
	return $code;
}

function combo_box($name,$opcoes,$checked,$extra = NULL){
	$code = "<select name='{$name}' $extra >";
	foreach($opcoes as $key => $op){
		if($op == $checked)
			$code .= "<option value='{$op}' SELECTED />{$key}</option>";
		else $code .= "<option value='{$op}' />{$key}</option>";
	}
	$code .= "</select>";
	return $code;
}


function dispararEmails($options)
{
	# Verifica o status do serviço no banco de dados
	$status = checkServiceStatus('email')['status'];


	$configEmail = include __DIR__ .'/../app/configs/email.php';

	$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
	$mail->IsSMTP(); // telling the class to use SMTP
	$mail->SMTPAuth = true;                  // enable SMTP authentication
	$mail->Host = $configEmail['smtp']['host'];      // sets GMAIL as the SMTP server
	$mail->Port = $configEmail['smtp']['port'];                   // set the SMTP port for the GMAIL server
	$mail->Username = $configEmail['smtp']['username'];  // GMAIL username
	$mail->Password = $configEmail['smtp']['password'];            // GMAIL password
//	$mail->SMTPSecure = $configEmail['smtp']['secure'];

	if (isset($options['to'])) {
		foreach($options['to'] as $to) {

			$mail->AddAddress($to['email'], utf8_decode($to['nome']));
		}
	}

	$mail->SetFrom($options['from']['email'], $options['from']['nome']);
	$mail->Subject = $options['subject'];
	$mail->AltBody = $options['altBody'];
	$mail->MsgHTML($options['msgHTML']);



	try {
		$send = $mail->Send();

		if($send){
			if($status === 'off'){
				$sql = "UPDATE services SET `status` = 'on' WHERE name = 'email'";
				$rs  = mysql_query($sql);
			}
		}
		return $send;

	} catch (Exception $ex) {

		$mail->SMTPAuth = true;                  // enable SMTP authentication
		$mail->Host = $configEmail['smtp-gmail']['host'];      // sets GMAIL as the SMTP server
		$mail->Port = $configEmail['smtp-gmail']['port'];                   // set the SMTP port for the GMAIL server
		$mail->Username = $configEmail['smtp-gmail']['username'];  // GMAIL username
		$mail->Password = $configEmail['smtp-gmail']['password'];            // GMAIL password

		try {
			$send = $mail->Send();
			if($send){
				if($status === 'off'){
					$sql = "UPDATE services SET `status` = 'on' WHERE name = 'email'";
					$rs  = mysql_query($sql);
				}
			}
			return $send;

		} catch (Exception $e) {
			if($status === 'on' &&
				($ex->getCode() == 0 && $e->getCode() == 0)
			) {
				if(APP_ENV === 'prd'){
					sendNotificationHipChat (sprintf ('Erro Under: %s | Erro Gmail: %s', $ex->getMessage (), $e->getMessage ()), 'warning');
				}else{
					echo sprintf ('Erro Under: %s | Erro Gmail: %s', $ex->getMessage (), $e->getMessage ());
				}

			}
			$sql = "UPDATE services SET `status` = 'off' WHERE name = 'email'";
			$rs  = mysql_query($sql);
			return false;
		}
	}
}

function sendNotificationHipChat($msg, $carater = null)
{
	$token = Config::get('TOKEN_HIPCHAT');
	$env = Config::get('APP_ENV');
	$hip = new \App\Controllers\HipChatSismederi($token, $env);
	$roomId = $env == 'prd' ? '1179954' : '469564';
	return $hip->sendNotificationRoom($roomId, $msg, array('color'=>'yellow','notify'=>'true'));
}

function checkServiceStatus($service)
{
	$sql = <<<SQL
SELECT
	`status`
FROM
	services
WHERE
	name = '{$service}'
SQL;
	$rs = mysql_query($sql) or die(mysql_error());
	return mysql_fetch_array($rs, MYSQL_ASSOC);

}


function enviar_email_relatorio($id_paciente, $origem, $empresa)
{
	$sql = "SELECT clientes.nome FROM clientes WHERE clientes.idClientes = {$id_paciente}";
	$nome_Paciente = mysql_result(mysql_query($sql), 0);

	$configEmail = include __DIR__ . '/../app/configs/email.php';

	$options = array();

	$lista = $configEmail['lista'];
	$tipo = $origem < 6 ? 'enfermagem' : 'medico';

	if (isset($lista['emails'][$tipo]) && isset($lista['emails'][$tipo][$empresa])) {
		$options['to'][] = $lista['emails'][$tipo][$empresa];
	}


	if (isset($lista['diretoria'][$tipo])) {
		$options['to'][] = $lista['diretoria'][$tipo];
	}


	$profissional = utf8_decode($_SESSION["nome_user"]);
	$msgHTML      = $configEmail['msgHTML'];
	$origem       = utf8_decode($lista['origem'][$origem]);
	$nomePaciente = utf8_decode($nome_Paciente);

	$options['msgHTML']    = sprintf($msgHTML, $origem, $nomePaciente, $profissional);

	if (isset($configEmail['addAddress'])) {
		foreach($configEmail['addAddress'] as $address) {
			$options['to'][] = $address;
		}
	}

	$options['from']['email'] = $configEmail['from']['email'];
	$options['from']['nome'] 	= $configEmail['from']['nome'];
	$options['subject']    = 'Avisos do Sistema';
	$options['altBody']    = $configEmail['altBody'];

	$servicoEmail = dispararEmails($options);
	$servicoHipChat = sendNotificationHipChat($options['msgHTML']);

	if($servicoEmail || $servicoHipChat){
		return true;
	}else{
		echo 'Alguns serviços estão indisponíveis. Seu relatório foi salvo com sucesso, mas nenhum email ou mensagem foi enviado. ',
				 'Avise ao SECMED sobre o relatório e também comunique o TI sobre a indisponibilidade! Agradecemos pela compreensão!';
	}
}


function get_files_dir($dir, $tipos = null){
	if(file_exists($dir)){
		$dh =  opendir($dir);
		while (false !== ($filename = readdir($dh))) {
			if($filename != '.' && $filename != '..'){
				if(is_array($tipos)){
					$extensao = get_extensao_file($filename);
					if(in_array($extensao, $tipos)){
						$files[] = $filename;
					}
				}
				else{
					$files[] = $filename;
				}
			}
		}
		if(isset($files) && is_array($files)){
			sort($files);
			return $files;
		}
	}
	else{
		return false;
	}
}


function get_extensao_file($nome){
	$verifica = explode('.', $nome);
	return $verifica[count($verifica) - 1];
}

function cabecalho_orcamento_avulso($id, $empresa = null){


	$sql2 = "SELECT
                                UPPER(o.PACIENTE) AS paciente,
                                o.CPF,
                                UPPER(o.ENDERECO) AS endereco,
                                UPPER(u.nome) AS usuario,DATE_FORMAT(o.DATA,'%Y-%m-%d') as DATA,
                                o.DATA_INICIO,
                                o.DATA_FIM,
                                p.nome as Convenio,
                                o.PLANO_ID,
                                o.UR,
                                o.OBSERVACAO
                          FROM
                                orcamento_avulso as o inner join
                                planosdesaude as p ON (p.id = o.PLANO_ID) INNER JOIN usuarios as u on (o.USUARIO_ID = u.idUsuarios)
		
                           WHERE
                                o.ID = {$id}
                           ORDER BY
                                o.Paciente, DATA DESC LIMIT 1";

	$result = mysql_query($sql);
	$result2 = mysql_query($sql2);
	$html.= "<table style='width:100%;' >";
	while($pessoa = mysql_fetch_array($result2)){
		foreach($pessoa AS $chave => $valor) {
			$pessoa[$chave] = stripslashes($valor);
		}

		$pessoa['UR'] = !empty($empresa) ? $empresa : $pessoa['UR'];
        $pessoa['endereco'] = $pessoa['endereco'] != '' ? $pessoa['endereco'] : 'ENDEREÇO NÃO INFORMADO.';
        $pessoa['CPF'] = $pessoa['CPF'] != '' ? $pessoa['CPF'] : 'CPF NÃO INFORMADO.';

		$html.= "<tr bgcolor='#EEEEEE'>";
		$html.= "<td  style='width:60%;' colspan='2'><b>PACIENTE:</b></td>";
		$html.= "<td style='width:20%;'><b>CONV&Ecirc;NIO </b></td>";
		$html.= "</tr>";
        $html.= "<tr>";
        $html.= "<td style='width:60%;' colspan='2'>{$pessoa['paciente']}</td>";
        $html.= "<td style='width:20%;'>".strtoupper($pessoa['Convenio'])."</td>";
        $html.= "</tr>";
        $html.= "<tr bgcolor='#EEEEEE'>";
        $html.= "<td  style='width:60%;' colspan='2'><b>ENDEREÇO:</b></td>";
        $html.= "<td style='width:20%;'><b>CPF </b></td>";
        $html.= "</tr>";
        $html.= "<tr>";
        $html.= "<td style='width:60%;' colspan='2'>".strtoupper($pessoa['endereco'])."</td>";
        $html.= "<td style='width:20%;'>{$pessoa['CPF']}</td>";
        $html.= "</tr>";
		$html.= "<tr bgcolor='#EEEEEE'>";
		$html.= "<td style='width:60%;'><b>Faturista:</b></td>";
		$html.= "<td style='width:20%;'><b>Data or&ccedil;ado: </b></td>";
		$html.= "<td style='width:30%;'><b>Per&iacute;odo</b></td>";
		$html.= "</tr>";
		$html.= "<tr>";
		$html.= "<td style='width:60%;'>{$pessoa['usuario']}</td>";
		$html.= "<td style='width:20%;'>".join("/",array_reverse(explode("-",$pessoa['DATA'])))."</td>";
		$html.= "<td style='width:30%;'>".join("/",array_reverse(explode("-",$pessoa['DATA_INICIO'])))." at&eacute; ".join("/",array_reverse(explode("-",$pessoa['DATA_FIM'])))."</td>";
		$html.= "</tr>";
		$inicio = $pessoa['DATA_INICIO'];
		$fim = $pessoa['DATA_FIM'];
		$plano=$pessoa['PLANO_ID'];
		$empresa=$pessoa['UR'];
		$obs=$pessoa['OBSERVACAO'];
	}
	$html.= "</table>";
	$conteudo= array('html'=>$html,'inicio'=>$inicio,'fim'=>$fim,'empresa'=>$empresa,'plano'=>$plano,'observacao'=>$obs);
	return $conteudo;
}
