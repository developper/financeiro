<?php
require_once('../db/config.php');

$like = $_GET['term'];
$plano = $_GET['plano'];
$condlike = "1";
$flag = true;
$strings = explode(" ",$like);
if(isset($_REQUEST['tipo'])) $_GET['tipo'] = $_REQUEST['tipo'];
$comparar = "concat(principio,' ',apresentacao)";
if(isset($_GET['tipo']) && ($_GET['tipo'] == '2' || $_GET['tipo'] == 'kits')) $comparar = "k.nome";
if(isset($_GET['tipo']) && ($_GET['tipo'] == 'serv')) $comparar = "CP.item";
if(isset($_GET['tipo']) && ($_GET['tipo'] == 'eqp')) $comparar = "C.item";
if(isset($_GET['tipo']) && ($_GET['tipo'] == 'gas')) $comparar = "C.item";
if(isset($_GET['tipo']) && ($_GET['tipo'] ==  'med_pedido' || $_GET['tipo'] ==  'die_pedido' || $_GET['tipo'] ==  'mat_pedido')) $comparar = "principio";
//verificar comparação de equipamento
foreach($strings as $str){
	if($str !=  "") $condlike .= " AND {$comparar} LIKE '%$str%' ";
}
$tipo = "1";
$t =  "-1";
$cond2="AND ATIVO = 'A'";
if(isset($_GET['tipo'])){
	if($_GET['tipo'] ==  '0' || $_GET['tipo'] == 'med')
		$t = "0";
	else if($_GET['tipo'] ==  '1' || $_GET['tipo'] == 'mat')
			$t = "1";
	else if($_GET['tipo'] ==  '2' || $_GET['tipo'] == 'kits')
			$t = "2";
	else if($_GET['tipo'] ==  'serv' )
		$t = "3";
	else if($_GET['tipo'] ==  'eqp' )
		$t = "5";
	else if($_GET['tipo'] ==  'gas' )
		$t = "6";
	else if($_GET['tipo'] ==  '3' || $_GET['tipo'] == 'die')
		$t = "7";
	else if($_GET['tipo'] ==  'med_pedido') 
		$t = "8";//ADICIONAR WHERE PARA O TIPO
	else if($_GET['tipo'] ==  'eqp_pedido' )
		$t = "9";
	else if($_GET['tipo'] ==  'die_pedido')
		$t  = "10";
	else if($_GET['tipo'] ==  'mat_pedido')
		$t  = "11";
}

$itens = array();
$result = "";
$sql = "";
if($t == "0" || $t == "1" || $t == "-1" || $t == "7"){
	if($t ==  "7") $t = "3";
	$tipo = " tipo = {$t} ";
	
	if($t ==  "-1") $tipo = " 1 ";
	$t = 90;
		$sql = "SELECT concat(principio,' ',apresentacao) as name,apresentacao, numero_tiss as cod, valor as custo  FROM brasindice WHERE {$tipo} AND {$condlike}  $cond2 ORDER BY principio, apresentacao";
	} else if($t == "2") {
		$sql = "SELECT k.nome as name, 'KIT' as apresentacao, k.idKit as cod, k.COD_REF as cod_ref FROM kitsmaterial as k WHERE {$condlike} ORDER BY k.nome";
	}else if($t == "3") {
		$sql = "SELECT CP.item as name,'SERVI&Ccedil;OS' as apresentacao ,C.id as cod, V.CUSTO_COBRANCA_PLANO as custo FROM cuidadosespeciais as C Left join valorescobranca as V ON (V.idCobranca = C.COBRANCAPLANO_ID) inner join cobrancaplanos as CP on (CP.id = C.COBRANCAPLANO_ID) WHERE {$condlike} and V.idPlano = {$plano} ORDER BY C.cuidado";
	}
	else if($t == "5") {
		$sql = "SELECT C.item as name,'EQUIPAMENTOS' as apresentacao ,C.id as cod, CUSTO as custo FROM cobrancaplanos as C WHERE {$condlike} and C.tipo =1 ORDER BY C.item";
	}
	else if($t == "6") {
		$sql = "SELECT C.item as name,'GASOTERAPIA' as apresentacao ,C.id as cod, CUSTO as custo FROM cobrancaplanos as C WHERE {$condlike} and C.id in(28,29,30) ORDER BY C.item";
	}else if($t == "8"){//Medicamento

		$sql = "SELECT concat(principio,' ',apresentacao) as name, numero_tiss, tipo FROM brasindice 
		WHERE {$condlike} AND tipo = '0' ORDER BY principio, apresentacao";
	}else if($t == "9"){//Equipamentos
		$sql = "SELECT item as name, tipo,  id as numero_tiss FROM cobrancaplanos WHERE {$condlike} AND tipo = '1' ORDER BY item";
		
		//Equipamentos não tem número tiss. Neste caso, é gravado o ID do equipamento em cobrancaplanos.
		
	}else if($t = "10"){
		$sql = "SELECT concat(principio,' ',apresentacao) as name, numero_tiss, tipo FROM brasindice
		WHERE {$condlike} AND tipo = '3' ORDER BY principio, apresentacao";
	}else if($t = "11"){
		$sql = "SELECT concat(principio,' ',apresentacao) as name, numero_tiss, tipo FROM brasindice
		WHERE {$condlike} AND tipo = '1' ORDER BY principio, apresentacao";
	}
	
$result = mysql_query($sql);
//echo $sql." ".mysql_error();
while ($row = mysql_fetch_array($result)) { 
	$item['value'] = $row['name']; 
	$item['id'] = $row['cod']; 
	$item['cod_ref'] = $row['cod_ref'];
	$item['apresentacao'] = $row['apresentacao'];
	$item['custo'] = $row['custo'];
	$item['numero_tiss'] = $row['numero_tiss'];
	$item['tipo'] = $row['tipo'];
	
	array_push($itens, $item); 
}

echo json_encode($itens);
?>