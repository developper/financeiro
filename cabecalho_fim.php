<title>Sistema de Gestão Financeira.</title>
<link type="text/css" rel="stylesheet"  href="/utils/style.css?v=13" media="screen" />
<link type="text/css" rel="stylesheet"  href="/utils/chat_css/chat.css" media="all" />
<link type="text/css" rel="stylesheet"  href="/utils/chat_css/screen.css" media="all"/>
<link type="text/css" rel="stylesheet"  href="/utils/chat_css/screen_ie.css" media="all"/>
<link rel="stylesheet" href="/react-app/assets/css/app.css" type="text/css">

<?php
  date_default_timezone_set('America/Sao_Paulo');
  $d = new DateTime();
?>

<script type="text/javascript">
    $(document).ready(function() {
      var horarioBrasilia = new Date();

      horarioBrasilia.setHours(<?= $d->format('H') ?>);
      horarioBrasilia.setMinutes(<?= $d->format('i') ?>);
      horarioBrasilia.setSeconds(<?= $d->format('s') ?>);

      function startTime(elem) {
        horarioBrasilia.setSeconds(horarioBrasilia.getSeconds() + 1);
        m = horarioBrasilia.getMinutes() > 9 ? horarioBrasilia.getMinutes() : '0' + horarioBrasilia.getMinutes();
        s = horarioBrasilia.getSeconds() > 9 ? horarioBrasilia.getSeconds() : '0' + horarioBrasilia.getSeconds();
        elem.innerHTML = horarioBrasilia.getHours() + ":" + m + ":" + s;
      }
      var elem = document.getElementById('horario-brasilia');
      setInterval(function() { startTime(elem) }, 1000);
    });
</script>

<!--[if lte IE 7]>
<link type="text/css" rel="stylesheet" media="all" href="css/screen_ie.css" />
<![endif]-->
</head>
<body>
<div id="header">
  <div width="100%">
      <img src="/utils/cabec-sismederi3.png" />
  
  <span width="100%" style="margin-left: 30%; text-align: right !important;color:#716969;;font-size:25px;"> Sistema de Gestão Financeira</span>
  </div>


<?php
include($_SERVER['DOCUMENT_ROOT'].'/menu.php');
?>
</div>
