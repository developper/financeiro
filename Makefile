install:
	cp ./app/configs/config.sample.php ../config.php
	php ./bin/composer.phar update
	php ./vendor/phing/phing/bin/phing sql
sql:
	php ./vendor/phing/phing/bin/phing sql
create_db_files:
	bash ./bin/db/create_initial_database.sh
test:
	sudo ./vendor/bin/phpunit --bootstrap=vendor/autoload.php tests/
testFinanceiro:
	sudo ./vendor/bin/phpunit --bootstrap=vendor/autoload.php tests/Financeiro/
testDominio:
	sudo ./vendor/bin/phpunit --bootstrap=vendor/autoload.php tests/Financeiro/DominioTest
testLancamento:
	sudo ./vendor/bin/phpunit --bootstrap=vendor/autoload.php tests/Financeiro/LancamentoTest
testContas:
	sudo ./vendor/bin/phpunit --bootstrap=vendor/autoload.php tests/Financeiro/Controller/ContasTest
testAdministracao:
	sudo ./vendor/bin/phpunit --bootstrap=vendor/autoload.php tests/Administracao/Controller/AdministracaoTest
testSendGrid:
	phpunit --bootstrap=vendor/autoload.php tests/Services/SendGridTest