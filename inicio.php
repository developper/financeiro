<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/validar.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/alertas.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/cabecalho.php');


?>
<script type="text/javascript">
$(document).ready(function($){
  function refresh_msg(){
    $.post("/query.php",{query: "get-msg"},function(r){
      if(r=='-1') alert('ERRO: Tente mais tarde!');
      else {
       /* $("#caixa-msg").html(r);
        atualiza_apaga_msg();
        atualiza_ler_msg();*/
      }
    });
  }
  //setInterval(function(i){refresh_msg();},120000);
  $("input[name='antigas']").change(function(){
    $("input[name='paciente']").val($(this).parent().parent().attr('paciente'));
    $("input[name='label']").val($(this).parent().parent().attr('label'));
  });
});
</script>
<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/cabecalho_fim.php');
?>

<div id="content">
<div id="right">
<?php include 'releases/release_notes.php' ?>
</div>
<div id="left">
<?php include($_SERVER['DOCUMENT_ROOT'].'/painel_login.php'); ?>
</div>
</div>
</body>
</html>
