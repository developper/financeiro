-- SM-1340
ALTER TABLE `remocoes`
ADD COLUMN `fornecedor_cliente` INT(11) NULL DEFAULT NULL AFTER `cod`;

ALTER TABLE `remocoes`
  ADD COLUMN `outros_custos` DOUBLE NULL DEFAULT NULL AFTER `valor_remocao`;

ALTER TABLE `remocoes`
  ADD COLUMN `recebido` ENUM('S', 'N') NOT NULL DEFAULT 'S' AFTER `retorno`,
  ADD COLUMN `forma_pagamento` ENUM('DIN', 'CHQ', 'CRT', 'TRB') NULL DEFAULT NULL AFTER `recebido`,
  ADD COLUMN `observacao` TEXT NULL DEFAULT NULL AFTER `forma_pagamento`;