ALTER TABLE historico_lote_fatura ADD justificativa TEXT NOT NULL;
ALTER TABLE historico_lote_fatura
  MODIFY COLUMN justificativa TEXT NOT NULL AFTER data_lote;