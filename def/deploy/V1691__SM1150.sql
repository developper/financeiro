ALTER TABLE entrada_ur ADD UR INT NULL AFTER ITENSPEDIDOSINTERNOS_ID;

UPDATE entrada_ur SET UR = (SELECT empresa FROM usuarios WHERE idUsuarios = entrada_ur.USUARIO_ID);

ALTER TABLE devolucao ADD UR INT NULL AFTER PACIENTE_ID;

UPDATE devolucao SET UR = (SELECT empresa FROM usuarios WHERE idUsuarios = devolucao.USUARIO_ID);

ALTER TABLE saida ADD UR_ORIGEM INT NULL AFTER UR;

UPDATE saida SET UR_ORIGEM = (SELECT empresa FROM usuarios WHERE idUsuarios = saida.idUsuario);