-- SM-2106

CREATE TABLE tipo_documento_financeiro  (
    `id` int(0) NOT NULL AUTO_INCREMENT,
    `sigla` varchar(255) NULL,
    `descricao` varchar(255) NULL,
    PRIMARY KEY (`id`)
);

INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (1, 'BOL', 'BOLETO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (2, 'CC', 'CARTAO DE CREDITO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (3, 'CFI', 'CARTAO DE FINANCIAMENTO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (4, 'CH', 'CHEQUE');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (5, 'COF', 'COFINS');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (6, 'CR', 'CREDITO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (7, 'CSL', 'CSLL');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (8, 'CSS', 'CSSL');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (9, 'DH', 'DINHEIRO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (10, 'FER', 'FERIAS');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (11, 'FG', 'GFIP');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (12, 'FI', 'FINANCIADO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (13, 'FOL', 'FOLHA DE PAGAMENTO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (14, 'FT', 'FATURA');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (15, 'INS', 'INSS');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (16, 'IRF', 'IRRF');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (17, 'ISS', 'TITULO DE ISS');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (18, 'JP', 'JUROS');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (19, 'NDC', 'NOTA DEBITO CLIENTE');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (20, 'NDF', 'NOTA DEBITO FORNECEDOR');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (21, 'NF', 'NOTA FISCAL');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (22, 'NP', 'PRESTACAO DE CONTAS');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (23, 'PA', 'PAGAMENTO ANTECIPADO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (24, 'PIS', 'PIS');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (25, 'PR', 'TITULO PROVISORIO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (26, 'RA', 'RECEBIMENTO ANTECIPADO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (27, 'RC', 'RECIBO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (28, 'RES', 'RESCISAO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (29, 'RPA', 'RPA');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (30, 'TF', 'DEPOSITO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (31, 'TX', 'TITULO DE TAXAS');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (32, 'PCC', 'PCC');

ALTER TABLE `notas`
    ADD COLUMN `tipo_documento_financeiro_id` int(0) NOT NULL AFTER `nota_fatura`;

-- SM-2107

alter table fornecedores
    add aliquota_iss decimal(10,2) default 0.00 null after validado_receita;

alter table fornecedores
    add aliquota_ir decimal(10,2) default 0.00 null after aliquota_iss;

-- SM-2108
create table classe_valor
(
    id int auto_increment,
    descricao varchar(255) not null,
    constraint classe_valor_pk
        primary key (id)
);

create unique index classe_valor_descricao_uindex
    on classe_valor (descricao);

create table fornecedores_classe_valor
(
    id int auto_increment,
    fornecedor_id int not null,
    classe_valor_id int not null,
    constraint fornecedores_classe_valor_pk
        primary key (id)
);

-- SM-2109
alter table plano_contas
    add POSSUI_RETENCOES int default 2 not null;

alter table plano_contas
    add ISS decimal(10,2) default 0.00 not null;

alter table plano_contas
    add ICMS decimal(10,2) default 0.00 not null;

alter table plano_contas
    add IPI decimal(10,2) default 0.00 not null;

alter table plano_contas
    add PIS decimal(10,2) default 0.00 not null;

alter table plano_contas
    add COFINS decimal(10,2) default 0.00 not null;

alter table plano_contas
    add CSLL decimal(10,2) default 0.00 not null;

alter table plano_contas
    add IR decimal(10,2) default 0.00 not null;

alter table plano_contas
    add PCC decimal(10,2) default 0.00 not null;

alter table plano_contas
    add INSS decimal(10,2) default 0.00 not null;

-- SM-2110
create table plano_contas_configuracao_retencoes
(
    iss int null,
    icms int null,
    ipi int null,
    pis int null,
    cofins int null,
    csll int null,
    ir int null,
    pcc int null,
    inss int null
)
    comment 'Tabela de tupla unica que concentra todos os ID do IPCF que dizem respeito as aliquotas das retencoes';

INSERT INTO `plano_contas_configuracao_retencoes` (`iss`, `icms`, `ipi`, `pis`, `cofins`, `csll`, `ir`, `pcc`, `tipo`) VALUES (null, null, null, null, null, null, null, null, 'desembolso');

ALTER TABLE `plano_contas_configuracao_retencoes`
    ADD COLUMN `inss` int NULL AFTER `pcc`;

-- SM-2111
ALTER TABLE `impostos_retidos`
    ADD COLUMN `tipo_documento_financeiro_id` int(0) NOT NULL AFTER `IPCF4`;

UPDATE `impostos_retidos` SET `tipo_documento_financeiro_id` = 16 WHERE `ID` = 1;
UPDATE `impostos_retidos` SET `tipo_documento_financeiro_id` = 16 WHERE `ID` = 2;
UPDATE `impostos_retidos` SET `tipo_documento_financeiro_id` = 24 WHERE `ID` = 3;
UPDATE `impostos_retidos` SET `tipo_documento_financeiro_id` = 5 WHERE `ID` = 4;
UPDATE `impostos_retidos` SET `tipo_documento_financeiro_id` = 7 WHERE `ID` = 5;
UPDATE `impostos_retidos` SET `tipo_documento_financeiro_id` = 32 WHERE `ID` = 6;
UPDATE `impostos_retidos` SET `tipo_documento_financeiro_id` = 17 WHERE `ID` = 7;

alter table plano_contas_configuracao_retencoes comment 'Tabela que concentra todos os ID do IPCF que dizem respeito as aliquotas das retencoes';

alter table plano_contas_configuracao_retencoes
    add tipo enum('desembolso', 'recebimento') null;

UPDATE `plano_contas_configuracao_retencoes` t SET t.`tipo` = 'desembolso';
INSERT INTO `plano_contas_configuracao_retencoes` (`iss`, `icms`, `ipi`, `pis`, `cofins`, `csll`, `ir`, `pcc`, `tipo`) VALUES (null, null, null, null, null, null, null, null, 'recebimento');

alter table notas
    add natureza_movimentacao int null;

alter table assinaturas
    add CODIGO_DOMINIO int null;

alter table assinaturas
    add CODIGO_GRUPO_DOMINIO int null;

alter table assinaturas
    add DESCRICAO_GRUPO_DOMINIO varchar(255) null;

INSERT INTO assinaturas (ID, DESCRICAO, CODIGO_DOMINIO, CODIGO_GRUPO_DOMINIO, DESCRICAO_GRUPO_DOMINIO) VALUES
(NULL, 'DIRETORIA ADMINISTRATIVA', 100, 10, 'ADMINISTRATIVO'),
(NULL, 'TI', 101, 10, 'ADMINISTRATIVO'),
(NULL, 'FINANCEIRO', 102, 10, 'ADMINISTRATIVO'),
(NULL, 'GESTÃO COM PESSOAS', 103, 10, 'ADMINISTRATIVO'),
(NULL, 'COMPRAS', 104, 10, 'ADMINISTRATIVO'),
(NULL, 'LOGISTICA', 105, 10, 'ADMINISTRATIVO'),
(NULL, 'FARMACIA / ESTOQUE', 106, 10, 'ADMINISTRATIVO'),
(NULL, 'CONTABILIDADE', 107, 10, 'ADMINISTRATIVO'),
(NULL, 'JURIDICO', 108, 10, 'ADMINISTRATIVO'),
(NULL, 'ADMINISTRATIVO', 109, 10, 'ADMINISTRATIVO'),
(NULL, 'INFRAESTRUTURA E PATRIMONIO', 110, 10, 'ADMINISTRATIVO'),
(NULL, 'PRONTU£RIOS', 111, 10, 'ADMINISTRATIVO'),
(NULL, 'DIRETORIA MEDICA', 200, 20, 'OPERACIONAL'),
(NULL, 'BASE OPERACIONAL', 201, 20, 'OPERACIONAL'),
(NULL, 'MEDICOS', 202, 20, 'OPERACIONAL'),
(NULL, 'CAPTACAO', 203, 20, 'OPERACIONAL'),
(NULL, 'CONTAS MEDICAS', 204, 20, 'OPERACIONAL'),
(NULL, 'EQUIPES DE ASSIST. TERC', 205, 20, 'OPERACIONAL'),
(NULL, 'SERVICO SOCIAL', 206, 20, 'OPERACIONAL'),
(NULL, 'DIRETORIA COMERCIAL', 300, 30, 'COMERCIAL'),
(NULL, 'RELACIONAMENTO COM MERCADO', 301, 30, 'COMERCIAL'),
(NULL, 'MARKETING', 302, 30, 'COMERCIAL'),
(NULL, 'PRESIDENCIA', 400, 40, 'PRESIDENCIA'),
(NULL, 'PLANEJAMENTO E QUALIDADE', 401, 40, 'PRESIDENCIA'),
(NULL, 'EXPERIENCIA DO PACIENTE', 402, 40, 'PRESIDENCIA'),
(NULL, 'SAC', 403, 40, 'PRESIDENCIA'),
(NULL, 'CONSELHO ADMINISTRATIVO', 404, 40, 'PRESIDENCIA');

alter table fornecedores
    add celular varchar(30) null after telefone;

alter table fornecedores modify IES varchar(255) null;

alter table retencoes_vencimentos modify retencao
    enum('iss', 'ir', 'ipi', 'icms', 'pis', 'cofins', 'csll', 'pcc', 'inss') not null;
