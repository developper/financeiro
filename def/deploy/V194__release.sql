ALTER TABLE `remocoes`
ADD COLUMN `paciente_telefone`  varchar(20) NULL AFTER `cod`,
ADD COLUMN `telefone_solicitante`  varchar(20) NULL AFTER `contato`,
ADD COLUMN `matricula`  varchar(20) NULL AFTER `telefone_solicitante`,
ADD COLUMN `data_nascimento`  date NULL AFTER `matricula`,
ADD COLUMN `cpf`  varchar(15) NULL AFTER `data_nascimento`,
ADD COLUMN `convenio_id`  int(11) NULL AFTER `cpf`,
ADD COLUMN `paciente_id`  int(11) NULL AFTER `convenio_id`,
ADD COLUMN `endereco_origem`  varchar(200) NULL AFTER `paciente_id`,
ADD COLUMN `endereco_destino`  varchar(200) NULL AFTER `endereco_origem`;

ALTER TABLE `remocoes`
ADD COLUMN `tipo_ambulancia`  enum('S','U') NULL DEFAULT NULL AFTER `endereco_destino`,
ADD COLUMN `tipo_remocao`  enum('INT','TRA','EXA','ADM') NULL DEFAULT NULL AFTER `tipo_ambulancia`,
ADD COLUMN `retorno`  enum('N','S') NULL DEFAULT NULL AFTER `tipo_remocao`;

ALTER TABLE `remocoes`
CHANGE COLUMN `solicitacao` `data_solicitacao`  date NOT NULL AFTER `paciente`,
ADD COLUMN `created_by`  int(11) NULL AFTER `retorno`,
ADD COLUMN `created_at`  datetime NULL AFTER `created_by`;

ALTER TABLE `remocoes`
CHANGE COLUMN `tipo_remocao` `motivo_remocao`  enum('INT','TRA','EXA','ADM') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER `tipo_ambulancia`,
ADD COLUMN `tipo_remocao`  enum('P','C') NULL DEFAULT NULL AFTER `valor`;

ALTER TABLE `cobrancaplanos`
ADD COLUMN `remocao`  enum('N','S') NULL DEFAULT 'N' AFTER `ATIVO`;

update
cobrancaplanos
SET
remocao = 'S'
WHERE
id in (41,
42,
43,
44,
81,
90,
92,
94,
117,
118,
121);

ALTER TABLE `remocoes`
CHANGE COLUMN `vmedico` `valor_medico`  double NOT NULL AFTER `medico`,
CHANGE COLUMN `vtecnico` `valor_tecnico`  double NOT NULL AFTER `tecnico`,
CHANGE COLUMN `vcondutor` `valor_condutor`  double NOT NULL AFTER `condutor`,
ADD COLUMN `data_saida`  date NULL AFTER `tipo_remocao`,
ADD COLUMN `hora_saida`  time NULL AFTER `data_saida`,
ADD COLUMN `data_chegada`  date NULL AFTER `hora_saida`,
ADD COLUMN `hora_chegada`  time NULL AFTER `data_saida`,
ADD COLUMN `tabela_propria`  enum('N','S') NULL DEFAULT NULL AFTER `hora_saida`,
ADD COLUMN `remocao_realizada`  enum('N','S') NULL DEFAULT NULL AFTER `tabela_propria`,
ADD COLUMN `autorizacao`  enum('N','S') NULL DEFAULT NULL AFTER `remocao_realizada`,
ADD COLUMN `motivo`  text NULL AFTER `autorizacao`,
ADD COLUMN `cobrancaplano_id`  int(11) NULL AFTER `motivo`;

ALTER TABLE `remocoes`
ADD COLUMN `finalized_at`  datetime NULL AFTER `cobrancaplano_id`,
ADD COLUMN `finalized_by`  int(11) NULL AFTER `finalized_at`,
ADD COLUMN `flag`  enum('false','true') NOT NULL DEFAULT 'false' AFTER `finalized_by`;

ALTER TABLE `remocoes`
CHANGE COLUMN `valor` `valor_remocao`  double NOT NULL AFTER `km`;

ALTER TABLE `remocoes`
ADD COLUMN `outras_despesas`  double NULL AFTER `tipo_remocao`;