-- SM-1531

ALTER TABLE relatorio_prorrogacao_med ADD indicacao_tecnica TEXT NULL;
ALTER TABLE relatorio_prorrogacao_med ADD regime_atual TEXT NULL;
ALTER TABLE relatorio_prorrogacao_med ADD quadro_clinico TEXT NULL;
ALTER TABLE relatorio_prorrogacao_med
  MODIFY COLUMN CLASSIFICACAO_NEAD INT(11) AFTER indicacao_tecnica,
  MODIFY COLUMN MODELO_NEAD CHAR(4) NOT NULL DEFAULT '2016' AFTER indicacao_tecnica;

ALTER TABLE prorrogacao_planserv_medico ADD indicacao_tecnica TEXT NULL;
ALTER TABLE prorrogacao_planserv_medico ADD regime_atual TEXT NULL;
ALTER TABLE prorrogacao_planserv_medico ADD quadro_clinico TEXT NULL;
ALTER TABLE prorrogacao_planserv_medico
  MODIFY COLUMN crm VARCHAR(30) AFTER indicacao_tecnica,
  MODIFY COLUMN nome VARCHAR(120) AFTER indicacao_tecnica,
  MODIFY COLUMN data_visita DATE NOT NULL DEFAULT '0000-00-00' AFTER indicacao_tecnica;

-- SM-1525

CREATE TABLE `custos_extras` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`nome`  varchar(250) NOT NULL ,
`created_by`  int(11) NOT NULL ,
`created_at`  datetime NOT NULL ,
`status`  enum('D','A') NOT NULL DEFAULT 'A' COMMENT 'A- ativo, D-desativado' ,
`updated_at`  datetime NULL DEFAULT NULL ,
`updated_by`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
COMMENT='Custos extras utilizados no rel. margem de contribuição'
;

CREATE TABLE `fatura_custos_extras` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`id_custo_extra`  int(11) NOT NULL ,
`id_fatura`  int(11) NOT NULL ,
`quantidade`  int(11) NOT NULL ,
`custo`  float NOT NULL ,
`created_at`  datetime NOT NULL ,
`created_by`  int(11) NOT NULL ,
`canceled_at`  datetime NULL ,
`canceled_by`  int(11) NULL ,
PRIMARY KEY (`id`)
)
COMMENT='Custos extras lançados na fatura';
;
