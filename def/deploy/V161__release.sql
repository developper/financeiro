-- SM-1101
CREATE TABLE parecer_entrega_imediata (
  `id`  int(11) NOT NULL AUTO_INCREMENT ,
  `solicitacao_id`  int(11) NOT NULL ,
  `parecer`  TEXT NOT NULL ,
  `created_at` DATETIME NOT NULL ,
  `created_by` int(11) NOT NULL ,
  PRIMARY KEY (`id`)
);