/* SM-988 */
ALTER TABLE `notas`
ADD COLUMN `visualizado`  tinyint(1) NOT NULL DEFAULT 1 AFTER `IMPOSTOS_RETIDOS_ID`;

ALTER TABLE `notas`
ADD COLUMN `visualizado_por`  tinyint(1) NOT NULL DEFAULT 0 AFTER `visualizado`;

UPDATE notas SET visualizado = 1, visualizado_por = 0;