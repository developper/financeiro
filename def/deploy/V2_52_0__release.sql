-- SM-1904
CREATE TABLE profissionais
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(100) NOT NULL
);

INSERT INTO profissionais (descricao) VALUES
  ('Médico'),
  ('Enfermeira'),
  ('Técnico de Enfermagem'),
  ('Fisioterapeuta'),
  ('Fonoaudiólogo(a)'),
  ('Nutricionista'),
  ('Psicólogo(a)'),
  ('Administrativo');

CREATE TABLE relatorio_nao_conformidade_item_profissional
(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  relatorio_nao_conformidade_item_id INT NOT NULL,
  profissional INT NOT NULL
);

-- SM-1907 E SM-1912
CREATE TABLE ocorrencia_arquivos
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  ocorrencia_id INT NOT NULL,
  arquivo_id INT NOT NULL
);

CREATE TABLE ocorrencia_resposta_arquivos
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  ocorrencia_id INT NOT NULL,
  resposta_id INT NOT NULL,
  arquivo_id INT NOT NULL
);