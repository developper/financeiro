ALTER TABLE `devolucao_interna_pedido`
MODIFY COLUMN `status`  enum('FINALIZADO','AUTORIZADO','CANCELADO','PENDENTE') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `motivo_cancelamento`;

ALTER TABLE `devolucao_interna_pedido`
CHANGE COLUMN `aceitado_at` `accepted_at`  datetime NULL DEFAULT NULL AFTER `accepted_by`;

ALTER TABLE `devolucao_interna_itens`
ADD COLUMN `autorizado`  int(11) NULL AFTER `devolucao_interna_motivo_id`;

ALTER TABLE `devolucao_interna_itens`
ADD COLUMN `enviado`  int(11) NULL AFTER `autorizado`;

ALTER TABLE `devolucao_interna_pedido`
ADD COLUMN `sent_by`  int(11) NULL AFTER `accepted_at`,
ADD COLUMN `sent_at`  datetime NULL AFTER `sent_by`;

ALTER TABLE `devolucao_interna_pedido`
MODIFY COLUMN `status`  enum('FINALIZADO','AUTORIZADO','CANCELADO','ENVIADO','PENDENTE') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `motivo_cancelamento`;

ALTER TABLE `devolucao_interna_pedido`
ADD COLUMN `confirmed_by`  int NULL AFTER `sent_at`,
ADD COLUMN `confirmed_at`  datetime NULL AFTER `confirmed_by`;
ALTER TABLE `devolucao_interna_itens`
ADD COLUMN `confirmado`  int(11) NULL AFTER `enviado`;