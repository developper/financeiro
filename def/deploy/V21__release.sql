-- SM-1382
CREATE TABLE remocoes_itens
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  remocao_id INT NOT NULL,
  catalogo_id INT NOT NULL,
  tipo INT NOT NULL,
  tiss VARCHAR(15),
  qtd INT
);
ALTER TABLE remocoes_itens COMMENT = 'Tabela de itens adicionados ao finalizar uma remocao';

-- SM-1386

UPDATE `quadro_feridas_enf_tipo` SET `TIPO`='LESÃO POR PRESSÃO' WHERE (`ID`='2');
INSERT INTO `quadro_feridas_enf_tipo` (`TIPO`) VALUES ('DEISCÊNCIA DE FO');
INSERT INTO `quadro_feridas_enf_local` (`LOCAL`, `LADO`) VALUES ('OUTROS', 'N');
INSERT INTO `quadro_feridas_enf_lesao` (`TIPO`) VALUES ('ESPUMA COM SILICONE');