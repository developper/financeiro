-- modificando label
UPDATE `relatorio_nao_conformidade_justificativa` SET `justificativa`='D�vida em posologia' WHERE (`id`='67');
UPDATE `relatorio_nao_conformidade_topico` SET `item`='REGISTRO DE FISIO / FONO' WHERE (`id`='5');
-- deletando aba reincid�ncia = 17 e suas amara��es.
DELETE FROM relatorio_nao_conformidade_topico_justificativa where relatorio_nao_conformidade_topico_id = 17;
DELETE FROM `relatorio_nao_conformidade_topico` WHERE (`id`='17');

--inserido justificativa, ela deve conter em todas as abas.
INSERT INTO `relatorio_nao_conformidade_justificativa` (`justificativa`,id) VALUES ('Registro correto',100);
insert into relatorio_nao_conformidade_topico_justificativa  (relatorio_nao_conformidade_justificativa_id, relatorio_nao_conformidade_topico_id, obrigatorio) values (100,1, 0);
insert into relatorio_nao_conformidade_topico_justificativa  (relatorio_nao_conformidade_justificativa_id, relatorio_nao_conformidade_topico_id, obrigatorio ) values (100,2, 0);
insert into relatorio_nao_conformidade_topico_justificativa  (relatorio_nao_conformidade_justificativa_id, relatorio_nao_conformidade_topico_id, obrigatorio ) values (100,3, 0);
insert into relatorio_nao_conformidade_topico_justificativa  (relatorio_nao_conformidade_justificativa_id, relatorio_nao_conformidade_topico_id, obrigatorio ) values (100,4, 0);
insert into relatorio_nao_conformidade_topico_justificativa  (relatorio_nao_conformidade_justificativa_id, relatorio_nao_conformidade_topico_id, obrigatorio ) values (100,5, 0);
insert into relatorio_nao_conformidade_topico_justificativa  (relatorio_nao_conformidade_justificativa_id, relatorio_nao_conformidade_topico_id, obrigatorio ) values (100,6, 0);
insert into relatorio_nao_conformidade_topico_justificativa  (relatorio_nao_conformidade_justificativa_id, relatorio_nao_conformidade_topico_id, obrigatorio ) values (100,7, 0);
insert into relatorio_nao_conformidade_topico_justificativa  (relatorio_nao_conformidade_justificativa_id, relatorio_nao_conformidade_topico_id, obrigatorio ) values (100,8, 0);
insert into relatorio_nao_conformidade_topico_justificativa  (relatorio_nao_conformidade_justificativa_id, relatorio_nao_conformidade_topico_id, obrigatorio ) values (100,9, 0);
insert into relatorio_nao_conformidade_topico_justificativa  (relatorio_nao_conformidade_justificativa_id, relatorio_nao_conformidade_topico_id, obrigatorio ) values (100,10, 0);
insert into relatorio_nao_conformidade_topico_justificativa  (relatorio_nao_conformidade_justificativa_id, relatorio_nao_conformidade_topico_id, obrigatorio ) values (100,11, 0);
insert into relatorio_nao_conformidade_topico_justificativa  (relatorio_nao_conformidade_justificativa_id, relatorio_nao_conformidade_topico_id, obrigatorio ) values (100,12, 0);
insert into relatorio_nao_conformidade_topico_justificativa  (relatorio_nao_conformidade_justificativa_id, relatorio_nao_conformidade_topico_id, obrigatorio ) values (100,13, 0);
insert into relatorio_nao_conformidade_topico_justificativa  (relatorio_nao_conformidade_justificativa_id, relatorio_nao_conformidade_topico_id, obrigatorio ) values (100,14, 0);
insert into relatorio_nao_conformidade_topico_justificativa  (relatorio_nao_conformidade_justificativa_id, relatorio_nao_conformidade_topico_id, obrigatorio ) values (100,15, 0);
insert into relatorio_nao_conformidade_topico_justificativa  (relatorio_nao_conformidade_justificativa_id, relatorio_nao_conformidade_topico_id, obrigatorio ) values (100,16, 0);
insert into relatorio_nao_conformidade_topico_justificativa  (relatorio_nao_conformidade_justificativa_id, relatorio_nao_conformidade_topico_id, obrigatorio ) values (100,18, 0);



-- Acrescentando o campo obrigatorio.
ALTER TABLE `relatorio_nao_conformidade_topico_justificativa`
ADD COLUMN `obrigatorio`  set('0','1') NULL DEFAULT '1' AFTER `relatorio_nao_conformidade_justificativa_id`;




-- colocando como n�o OBG os itens
-- �	 Aus�ncia de nome do paciente ou data
-- �	Impresso n�o consta em prontu�rio
--  �	N�o se aplica �	N�o encaminhado

UPDATE
relatorio_nao_conformidade_topico_justificativa
SET
obrigatorio = 0
WHERE
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_justificativa_id in(16,52,64,61);

-- CONTROLE DE OXIGENIO
--  �	Aus�ncia de registro de troca de torpedo
--  �	Descri��o de vaz�o mantida apesar do uso

UPDATE
relatorio_nao_conformidade_topico_justificativa
SET
obrigatorio = 0
WHERE
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_topico_id = 21  and
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_justificativa_id in(11,33)

-- EQUIPAMENTOS
-- �	Equipamentos ativos no sistema desatualizados conforme HC

UPDATE
relatorio_nao_conformidade_topico_justificativa
SET
obrigatorio = 0
WHERE
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_topico_id = 13  and
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_justificativa_id = 45

-- Folha de Les�o
-- �	N�o especifica tamanho ou tipo de curativo
-- �	Registro incompleto de materiais utilizado
UPDATE
relatorio_nao_conformidade_topico_justificativa
SET
obrigatorio = 0
WHERE
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_topico_id = 8  and
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_justificativa_id in (62,86);

-- PRESCRI��O DO ENFERMEIRO T�pico = 2
--  �	Prescri��o sem carimbo do t�cnico e enfermeiro 72
--  �	Servi�os divergentes do relat�rio 92

UPDATE
relatorio_nao_conformidade_topico_justificativa
SET
obrigatorio = 0
WHERE
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_topico_id = 2  and
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_justificativa_id in (72,92);

-- PRESCRI��O M�DICA =18
-- �	Posologia d�vida em posologia - item deve ser alterado texto para: "D�VIDA EM POSOLOGIA 67
-- �	Prescri��o sem carimbo 72 �	Servi�os divergentes do relat�rio m�dico 93

UPDATE
relatorio_nao_conformidade_topico_justificativa
SET
obrigatorio = 0
WHERE
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_topico_id = 18  and
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_justificativa_id in (72, 93, 67);

-- PRESCRI��O NUTRICIONAL 1
-- �	Prescri��o sem carimbo 72
-- �	Servi�os divergentes do relat�rio nutricional 94

UPDATE
relatorio_nao_conformidade_topico_justificativa
SET
obrigatorio = 0
WHERE
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_topico_id = 1  and
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_justificativa_id in (72, 94);

-- REGISTRO DE FOSIO/FONO - alterar t�tulo para REGISTRO FISIO/FONO T�pico = 5 �	Aus�ncia de carimbo e/ou assinatura do profissional 5

UPDATE
relatorio_nao_conformidade_topico_justificativa
SET
obrigatorio = 0
WHERE
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_topico_id = 5  and
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_justificativa_id = 5;

--REGISTRO DE OBSERVA��O DO T�CNICO = 6 �	Aus�ncia de carimbo e/ou assinatura do profissional 5 �	Aus�ncia de registro de oxigenoterapia 19 �	Presen�a de rasuras 74

UPDATE
relatorio_nao_conformidade_topico_justificativa
SET
obrigatorio = 0
WHERE
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_topico_id = 6  and
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_justificativa_id in (5, 19, 74);

-- REGISTRO DE PROFISSIONAIS (IMPRESSO ROSA)= 3 �	Aus�ncia de carimbo e/ou assinatura do profissional 5 �	Aus�ncia de evolu��o do enfermeiro 9
-- �	Aus�ncia de evolu��o do nutricionista 13
-- �	Aus�ncia de evolu��o do psic�logo 14
-- �	Aus�ncia de evolu��o do m�dico 15
UPDATE
relatorio_nao_conformidade_topico_justificativa
SET
obrigatorio = 0
WHERE
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_topico_id = 3  and
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_justificativa_id in (5, 9, 13, 14, 15);

-- REGISTRO DE VISITA M�DICA E DE ENFERMAGEM NO SISTEMA=  4
-- �	Aus�ncia de registro de visita de enfermagem 22
-- �	Aus�ncia de registro de visita m�dica 23
UPDATE
relatorio_nao_conformidade_topico_justificativa
SET
obrigatorio = 0
WHERE
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_topico_id = 4  and
relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_justificativa_id in (22, 23);









