-- SM-2076
ALTER TABLE formulario_condensado ADD atb_nome VARCHAR(255) NULL;
ALTER TABLE formulario_condensado ADD antifungico_nome VARCHAR(255) NULL;
ALTER TABLE formulario_condensado
  MODIFY COLUMN atb_nome int AFTER atb,
  MODIFY COLUMN antifungico_nome datetime AFTER antifungico;