-- SM-2120
-- Alterando valores notas
ALTER TABLE `notas`
    MODIFY COLUMN `valor` decimal(12, 2) NULL DEFAULT NULL AFTER `codCentroCusto`,
    MODIFY COLUMN `ICMS` decimal(12, 2) NOT NULL AFTER `empresa`,
    MODIFY COLUMN `FRETE` decimal(12, 2) NOT NULL AFTER `ICMS`,
    MODIFY COLUMN `SEGURO` decimal(12, 2) NOT NULL AFTER `FRETE`,
    MODIFY COLUMN `VAL_PRODUTOS` decimal(12, 2) NOT NULL AFTER `SEGURO`,
    MODIFY COLUMN `DES_ACESSORIAS` decimal(12, 2) NOT NULL AFTER `VAL_PRODUTOS`,
    MODIFY COLUMN `IPI` decimal(12, 2) NOT NULL AFTER `DES_ACESSORIAS`,
    MODIFY COLUMN `ISSQN` decimal(12, 2) NOT NULL AFTER `IPI`,
    MODIFY COLUMN `DESCONTO_BONIFICACAO` decimal(12, 2) NOT NULL AFTER `ISSQN`,
    MODIFY COLUMN `PIS` decimal(12, 2) NOT NULL AFTER `TIPO_DOCUMENTO`,
    MODIFY COLUMN `COFINS` decimal(12, 2) NOT NULL AFTER `PIS`,
    MODIFY COLUMN `CSLL` decimal(12, 2) NOT NULL AFTER `COFINS`,
    MODIFY COLUMN `IR` decimal(12, 2) NOT NULL AFTER `CSLL`,
    MODIFY COLUMN `PCC` decimal(12, 2) NOT NULL AFTER `DATA`,
    MODIFY COLUMN `INSS` decimal(12, 2) NOT NULL AFTER `PCC`;

-- Alterando parcelas
ALTER TABLE `parcelas`
    MODIFY COLUMN `valor` decimal(12, 2) NOT NULL AFTER `idNota`,
    MODIFY COLUMN `JurosMulta` decimal(12, 2) NOT NULL DEFAULT 0.00 AFTER `empresa`,
    MODIFY COLUMN `desconto` decimal(12, 2) NOT NULL AFTER `JurosMulta`,
    MODIFY COLUMN `encargos` decimal(12, 2) NOT NULL AFTER `desconto`,
    MODIFY COLUMN `VALOR_PAGO` decimal(12, 2) NOT NULL AFTER `TR`,
    MODIFY COLUMN `OUTROS_ACRESCIMOS` decimal(12, 2) NOT NULL DEFAULT 0 AFTER `DATA_PAGAMENTO`,
    MODIFY COLUMN `ISSQN` decimal(12, 2) NOT NULL DEFAULT 0 AFTER `NUM_DOCUMENTO`,
    MODIFY COLUMN `PIS` decimal(12, 2) NOT NULL DEFAULT 0 AFTER `ISSQN`,
    MODIFY COLUMN `COFINS` decimal(12, 2) NOT NULL DEFAULT 0 AFTER `PIS`,
    MODIFY COLUMN `CSLL` decimal(12, 2) NOT NULL DEFAULT 0 AFTER `COFINS`,
    MODIFY COLUMN `IR` decimal(12, 2) NOT NULL DEFAULT 0 AFTER `CSLL`;

-- Alterando rateio
ALTER TABLE `rateio`
    MODIFY COLUMN `VALOR` decimal(12, 2) NOT NULL AFTER `CENTRO_RESULTADO`;

-- Alterando transferencia_bancaria
ALTER TABLE `transferencia_bancaria`
    MODIFY COLUMN `VALOR` decimal(12, 2) NOT NULL AFTER `CONTA_DESTINO`;

-- Alterando contas_bancarias
ALTER TABLE `contas_bancarias`
    MODIFY COLUMN `SALDO` decimal(12, 2) NOT NULL AFTER `UR`,
    MODIFY COLUMN `SALDO_NCONCILIADO` decimal(12, 2) NOT NULL AFTER `DATA_ATUALIZACAO_SALDO`,
    MODIFY COLUMN `SALDO_INICIAL` decimal(12, 2) NOT NULL AFTER `SALDO_NCONCILIADO`;

-- Alterando impostos_tarifas_lancto
ALTER TABLE `impostos_tarifas_lancto`
    MODIFY COLUMN `valor` double(12, 2) NOT NULL AFTER `data`;

-- SM-2118
-- Add area sistema
INSERT INTO `area_sistema` VALUES (96, 'financeiro_conta_pagar_receber', 'Contas a Pagar/Receber', 2, 69, 0, '/financeiros/?op=notas', NULL);
INSERT INTO `area_sistema` VALUES (97, 'financeiro_conta_pagar_receber_nova', 'Nova Conta a Pagar/Receber ', 1, 96, 0, '/financeiros/?op=notas&act=novo', NULL);
INSERT INTO `area_sistema` VALUES (98, 'financeiro_conta_pagar_receber_buscar_conta', 'Buscar Contas a Pagar/Receber', 2, 96, 0, '/financeiros/?op=notas&act=listar', NULL);
INSERT INTO `area_sistema` VALUES (99, 'financeiro_conta_pagar_receber_buscar_parcela', 'Buscar Parcelas a Pagar/Receber', 3, 96, 0, '/financeiros/?op=notas&act=buscar', NULL);
INSERT INTO `area_sistema` VALUES (100, 'financeiro_conta_pagar_receber_fatura_aglutinacao', 'Faturas de Aglutinação', 4, 96, 0, '/financeiros/fatura-imposto/?action=index-fatura-imposto', NULL);
INSERT INTO `area_sistema` VALUES (101, 'financeiro_transferencia_bancaria', 'Transferência Bancária', 6, 69, 0, '/financeiros/?op=transferencia', NULL);
INSERT INTO `area_sistema` VALUES (102, 'financeiro_conciliacao_bancaria', 'Conciliação Bancária', 3, 69, 0, '/financeiros/conciliacao-bancaria/?action=form-conciliacao', NULL);
INSERT INTO `area_sistema` VALUES (103, 'financeiro_conta_bancaria', 'Contas Bancárias', 4, 69, 0, '/financeiros/?op=listarContasBancarias', NULL);
INSERT INTO `area_sistema` VALUES (104, 'financeiro_conta_bancaria_tarifa', 'Tarifas Bancarias', 5, 69, 0, '/financeiros/?op=tarifas-bancarias&id_conta=40&action=listar', NULL);
INSERT INTO `area_sistema`(`id`, `nome`, `descricao`, `ordem`, `id_pai`, `menu`, `link`, `caminho_icone`) VALUES (105, 'financeiro_fornecedor_cliente', 'Fornecedores/Clientes', 1, 69, 0, '/financeiros/?op=fornecedor', NULL);

-- Add area permissao
INSERT INTO `area_permissoes` VALUES (78, 98, 1, 'financeiro_conta_pagar_receber_buscar_conta_editar', 'Editar Nota');
INSERT INTO `area_permissoes` VALUES (79, 99, 1, 'financeiro_conta_pagar_receber_buscar_parcela_processar', 'Processar ');
INSERT INTO `area_permissoes` VALUES (80, 99, 2, 'financeiro_conta_pagar_receber_buscar_parcela_desprocessar', 'Desprocessar');
INSERT INTO `area_permissoes` VALUES (81, 100, 1, 'financeiro_conta_pagar_receber_fatura_aglutinacao_nova', 'Criar Fatura Aglutinação');
INSERT INTO `area_permissoes` VALUES (82, 100, 2, 'financeiro_conta_pagar_receber_fatura_aglutinacao_excluir', 'Excluir Fatura Aglutinação ');
INSERT INTO `area_permissoes` VALUES (86, 101, 1, 'financeiro_transferencia_bancaria_nova', 'Criar Transferência Bancária');
INSERT INTO `area_permissoes` VALUES (87, 101, 2, 'financeiro_transferencia_bancaria_cancelar', 'Cancelar Transferência Bancária');
INSERT INTO `area_permissoes` VALUES (88, 102, 1, 'financeiro_conciliacao_bancaria_conciliar', 'Conciliar');
INSERT INTO `area_permissoes` VALUES (89, 102, 2, 'financeiro_conciliacao_bancaria_desconciliar', 'Desconciliar');
INSERT INTO `area_permissoes` VALUES (90, 102, 3, 'financeiro_conciliacao_bancaria_conciliar_ofx', 'Conciliar com OFX');
INSERT INTO `area_permissoes` VALUES (91, 103, 1, 'financeiro_conta_bancaria_nova', 'Criar Conta Bancária');
INSERT INTO `area_permissoes` VALUES (92, 103, 2, 'financeiro_conta_bancaria_editar', 'Editar Conta Bancária');
INSERT INTO `area_permissoes` VALUES (93, 104, 1, 'financeiro_conta_bancaria_tarifa_nova', 'Criar Tarifas/Impostos');
INSERT INTO `area_permissoes` VALUES (94, 104, 2, 'financeiro_conta_bancaria_tarifa_editar', 'Editar Tarifas/Impostos');
INSERT INTO `area_permissoes` VALUES (95, 104, 3, 'financeiro_conta_bancaria_tarifa_excluir', 'Excluir Tarifas/Impostos');
INSERT INTO `area_permissoes` VALUES (96, 103, 4, 'financeiro_conta_bancaria_tarifa', 'Tarifas Bancárias');
INSERT INTO `area_permissoes`(`id`, `area_id`, `ordem`, `acao`, `descricao`) VALUES (97, 105, 1, 'financeiro_fornecedor_cliente_novo', 'Criar Fornecedor/Cliente');
INSERT INTO `area_permissoes`(`id`, `area_id`, `ordem`, `acao`, `descricao`) VALUES (98, 105, 2, 'financeiro_fornecedor_cliente_editar', 'Editar Fornecedor/Cliente');
INSERT INTO `area_permissoes`(`id`, `area_id`, `ordem`, `acao`, `descricao`) VALUES (99, 105, 3, 'financeiro_fornecedor_cliente_desativar', 'Desativar Fornecedor/Cliente');

-- SM-2117
alter table parcelas modify VALOR_PAGO decimal(12,2) null;
alter table parcelas modify DATA_PAGAMENTO date null;
alter table parcelas modify DATA_PROCESSADO datetime null;

create table historico_notas_desaglutinacao
(
    id int auto_increment,
    nota_id int not null,
    usuario_id int not null,
    data_hora datetime not null,
    constraint historico_notas_desaglutinacao_pk
        primary key (id)
)
    comment 'Historico de notas desaglutinadas';
