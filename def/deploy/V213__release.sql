CREATE TABLE fornecedores_custo_equipamento
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  fornecedor_id INT NOT NULL,
  cobrancaplanos_id INT NOT NULL,
  valor_aluguel DECIMAL(10,2) DEFAULT 0 NOT NULL
);
ALTER TABLE fornecedores_custo_equipamento COMMENT = 'Tabela responsável por associar os itens da tabela cobrancaplanos a um fornecedor.';

CREATE TABLE historico_fornecedores_custo_equipamento
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  fornecedor_id INT NOT NULL,
  cobrancaplanos_id INT NOT NULL,
  valor_aluguel DECIMAL(10,2) NOT NULL,
  data_mudanca DATETIME NOT NULL
);
ALTER TABLE historico_fornecedores_custo_equipamento COMMENT = 'Tabela de histórico de mudanças de valores de equipamentos de fornecedores';

-- SM-1520

CREATE TABLE `evento_aph` (
`id`  int(11) NULL AUTO_INCREMENT ,
`data`  datetime NOT NULL ,
`hora`  time NOT NULL ,
`solicitante`  varchar(255) NOT NULL ,
`telefone`  varchar(255) NOT NULL ,
`tipo_servico`  enum('APH','EV') NOT NULL COMMENT 'EV- Evento, APH' ,
`publico_alvo`  enum('I','A') NOT NULL COMMENT 'A=Adulto I=Infantil' ,
`endereco`  text NOT NULL ,
`cidade_id`  int(11) NULL ,
`complemento`  varchar(225) NULL ,
`observacao`  text NULL ,
`criado_em`  datetime NULL ,
`criado_por`  int(11) NULL ,
`finalizado_em`  datetime NULL ,
`finalizado_por`  int(11) NULL ,
`cancelado_em`  datetime NULL ,
`cancelado_por`  int(11) NULL ,
PRIMARY KEY (`id`)
)
;

ALTER TABLE `evento_aph`
MODIFY COLUMN `data`  date NOT NULL AFTER `id`;

CREATE TABLE `evento_aph_finalizar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
	`evento_aph_id` int(11) DEFAULT NULL,
  `fornecedor_cliente` int(11) DEFAULT NULL,
  `valor` double NOT NULL,
  `outros_custos` double DEFAULT NULL,
  `outras_despesas` double DEFAULT NULL,
  `data_termino` date DEFAULT NULL,
  `hora_termino` time DEFAULT NULL,
  `medico` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `valor_medico` double NOT NULL,
  `tecnico` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `valor_tecnico` double NOT NULL,
  `condutor` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `valor_condutor` double NOT NULL,
	`enfermeiro` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valor_enfermeiro` double DEFAULT NULL,
  `recebido` enum('S','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `forma_pagamento` enum('DIN','CHQ','CRT','TRB') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DIN=Dinheiro, CHQ= Cheque, CRT=Cartão,TRB= Transação Bancária',
  `observacao_final` text COLLATE utf8_unicode_ci,
   `empresa_id` int(11) DEFAULT NULL,
  `sera_cobrada` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'S =Sim, N = Não',
  `justificativa_nao_cobrar` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

CREATE TABLE `evento_aph_itens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `evento_aph_id` int(11) NOT NULL,
  `catalogo_id` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `tiss` varchar(15) DEFAULT NULL,
  `qtd` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci  COMMENT='Tabela de itens adicionados ao finalizar um Evento/APH';

-- Fim SM-1520

-- SM-1522

ALTER TABLE `remocoes`
ADD COLUMN `remocao_sera_cobrada`  varchar(1) NULL COMMENT 'S =Sim, N = Não' AFTER `valor_enfermeiro`,
ADD COLUMN `modo_cobranca`  varchar(2) NULL COMMENT 'F- fatura, NF - nota fiscal' AFTER `remocao_sera_cobrada`,
ADD COLUMN `justificativa_nao_cobrar`  text NULL AFTER `modo_cobranca`;

-- SM-1522

-- Inseriri CRM médico
update usuarios set conselho_regional = '26831' where idUsuarios =468;
update usuarios set conselho_regional = '29727' where idUsuarios =125;
update usuarios set conselho_regional = '30173' where idUsuarios =474;
update usuarios set conselho_regional = '28175' where idUsuarios =358;
update usuarios set conselho_regional = '26873' where idUsuarios =273;
update usuarios set conselho_regional = '24121' where idUsuarios =92;
update usuarios set conselho_regional = '29911' where idUsuarios =417;
update usuarios set conselho_regional = '29932' where idUsuarios =370;
update usuarios set conselho_regional = '25900' where idUsuarios =183;
update usuarios set conselho_regional = '20126' where idUsuarios =333;
update usuarios set conselho_regional = '26845' where idUsuarios =290;
update usuarios set conselho_regional = '14398' where idUsuarios =390;
update usuarios set conselho_regional = '28486' where idUsuarios =180;
update usuarios set conselho_regional = '28350' where idUsuarios =364;
update usuarios set conselho_regional = '13074' where idUsuarios =426;
update usuarios set conselho_regional = '28279' where idUsuarios =393;
update usuarios set conselho_regional = '15847' where idUsuarios =18;
update usuarios set conselho_regional = '26541' where idUsuarios =110;
update usuarios set conselho_regional = '25913' where idUsuarios =286;
update usuarios set conselho_regional = '26419' where idUsuarios =383;
update usuarios set conselho_regional = '22378' where idUsuarios =386;
update usuarios set conselho_regional = '15005' where idUsuarios =49;
update usuarios set conselho_regional = '13684' where idUsuarios =264;
update usuarios set conselho_regional = '25079' where idUsuarios =167;
update usuarios set conselho_regional = '15677' where idUsuarios =405;
update usuarios set conselho_regional = '13375' where idUsuarios =113;
update usuarios set conselho_regional = '26384' where idUsuarios =324;
update usuarios set conselho_regional = '20582' where idUsuarios =382;