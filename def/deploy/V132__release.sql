-- SM-879

CREATE TABLE `carater_prescricao` (
  `id` int(11) NULL ,
  `nome` varchar(100) NOT NULL,
  `profissional` enum('m','e','n') NOT NULL DEFAULT 'm',
  PRIMARY KEY (`id`)
);
ALTER TABLE `carater_prescricao`
MODIFY COLUMN `id`  int(11) NOT NULL AUTO_INCREMENT FIRST;

INSERT INTO `carater_prescricao` (`id`, `nome`, `profissional`)
VALUES
  ('1', 'Emergência', 'm'),
  ('2', 'Periódica', 'm'),
  ('3', 'Aditiva', 'm'),
  ('4', 'Avaliação', 'm'),
  ('5', 'Periódica', 'n'),
  ('6', 'Aditiva', 'n'),
  ('7', 'Aditiva de Avaliação', 'm'),
  ('8', 'Aditiva', 'e'),
  ('9', 'Periódica', 'e'),
  ('10', 'Avaliação', 'e');

CREATE TABLE `prescricao_curativo` (
  `id`  int(11) NULL AUTO_INCREMENT ,
  `paciente`  int(11) NOT NULL ,
  `profissional`  int(11) NOT NULL ,
  `carater`  int(11) NOT NULL,
  `inicio`  date NOT NULL ,
  `fim`  date NOT NULL ,
  `enviado`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 = Não enviado / 1 = Enviado',
  `registered_at`  datetime NOT NULL ,
  `updated_at`  datetime NOT NULL ,
  `updated_by`  int(11) NOT NULL ,
  PRIMARY KEY (`id`)
);

CREATE TABLE `prescricao_curativo_ferida` (
  `id`  int(11) NULL AUTO_INCREMENT ,
  `prescricao`  int(11) NOT NULL,
  `local`  int(11) NOT NULL ,
  `lado`  int NULL COMMENT 'NULL = SEM LADO / 1 = DIREITO / 2 = ESQUERDO' ,
PRIMARY KEY (`id`)
);

CREATE TABLE `prescricao_curativo_ferida_item` (
  `id`  int(11) NULL AUTO_INCREMENT ,
  `prescricao`  int(11) NOT NULL ,
  `ferida`  int(11) NOT NULL ,
  `catalogo_id`  int(11) NOT NULL ,
  `qtd`  int(11) NOT NULL ,
  `inicio`  date NOT NULL,
  `fim`  date NOT NULL,
  `obs`  text NULL,
  `cobertura`  enum('p','s') NOT NULL DEFAULT 'p' COMMENT 'P = Primária / S = Secundária' ,
  `aprazamento`  varchar(200) NOT NULL DEFAULT '00:00',
PRIMARY KEY (`id`)
);