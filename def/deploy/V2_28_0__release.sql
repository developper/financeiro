-- SM-1567
ALTER TABLE `entrada_ur`
ADD COLUMN `catalogo_id`  int(11) NULL AFTER `UR`,
ADD COLUMN `saida_id`  int(11) NULL AFTER `catalogo_id`;

ALTER TABLE `saida`
ADD COLUMN `modelo`  varchar(40) NULL COMMENT 'Vai impactar na rotina de pedidos internos.' AFTER `fornecedor`;

ALTER TABLE `saida`
MODIFY COLUMN `modelo`  varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'Utilizado para funcionalidade de pedidos internos.' AFTER `fornecedor`,
ADD COLUMN `confirmacao_pedidointerno`  int(11) NOT NULL DEFAULT '0' COMMENT 'Qunatidade confirmada pela UR quando for pedido interno.' AFTER `modelo`;

UPDATE
`saida`
SET
modelo = 'v1'
WHERE
saida.modelo is null
AND
saida.idCliente = 0;

-- SM-1514

CREATE TABLE `historico_cancelar_item_pedido_interno_enviado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `saida_id` int(11) DEFAULT NULL,
  `quantidade` int(11) DEFAULT NULL COMMENT 'Quantidade cancelada.',
  `cancelado_por` int(11) DEFAULT NULL,
  `cancelado_em` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `historico_cancelar_item_pedido_interno_enviado`
ADD COLUMN `pedido_interno_id`  int(11) NULL AFTER `cancelado_em`,
ADD COLUMN `item_pedido_interno_id`  int(11) NULL AFTER `pedido_interno_id`;


-- select para montar update e roolback para cancelar tabela itenspedidosinternos
SELECT
CONCAT("UPDATE itenspedidosinterno set CANCELADO = 'S', DATA_CANCELADO = NOW(), CANCELADO_POR = 60, JUSTIFICATIVA = 'CANCELADO POR CAUSA DA MODIFICAOÇÃO DA VERSÃO V2.28.0 DO SISTEMA',CANCELADO_ENTRADA = 'S', DATA_CANCELADO_ENTRADA = NOW(), CANCELADO_ENTRADA_POR = 60, JUSTIFICATIVA_ENTRADA = 'CANCELADO POR CAUSA DA MODIFICAOÇÃO DA VERSÃO V2.28.0 DO SISTEMA' WHERE ID = ", ID,";"  ) AS UP,
CONCAT("UPDATE itenspedidosinterno set CANCELADO = '', DATA_CANCELADO = '0000-00-00 00:00:00', CANCELADO_POR = '', JUSTIFICATIVA = '',CANCELADO_ENTRADA = '', DATA_CANCELADO_ENTRADA ='0000-00-00 00:00:00', CANCELADO_ENTRADA_POR = '', JUSTIFICATIVA_ENTRADA = '' WHERE ID = ", ID,";"  ) AS rl,

FROM
itenspedidosinterno
WHERE
(itenspedidosinterno.ENVIADO > itenspedidosinterno.CONFIRMADO  OR
itenspedidosinterno.ENVIADO=0 ) AND
PEDIDOS_INTERNO_ID < 14523


-- Select para montar update e roolback para cancelar tabela pedidointerno
SELECT
CONCAT("UPDATE pedidosinterno set STATUS = 'F', CANCELADO_POR = 60, DATA_CANCELADO = NOW(), JUSTIFICATIVA = 'CANCELADO POR CAUSA DA MODIFICAOÇÃO DA VERSÃO V2.28.0 DO SISTEMA' WHERE ID = ", ID,";") as UP,
CONCAT("UPDATE pedidosinterno set STATUS = '",STATUS,"', CANCELADO_POR = 0, DATA_CANCELADO = '0000-00-00 00:00:00', JUSTIFICATIVA = '' WHERE ID = ", ID,";") as RL


FROM
pedidosinterno
WHERE
pedidosinterno.ID < 14523
AND
pedidosinterno.`STATUS` <> 'F'
AND
CANCELADO_POR = 0

