-- SM 1205
ALTER TABLE `historico_troca_item`
ADD COLUMN `item_pedido_interno_id`  int(11) NULL AFTER `solicitacao_id`;

ALTER TABLE itenspedidosinterno MODIFY STATUS CHAR(1) NOT NULL COMMENT 'A - aberto, P - pendente, F - finalizado, T - trocado';