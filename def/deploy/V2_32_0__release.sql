-- SM-1673
CREATE TABLE `historico_faturamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `SOLICITACOES_ID` int(11) NOT NULL,
  `NUMERO_TISS` varchar(11) NOT NULL,
  `CODIGO_PROPRIO` varchar(11) NOT NULL DEFAULT '0' COMMENT 'Campo utilizado caso a operadora possua tabela propria para o item',
  `CATALOGO_ID` int(11) NOT NULL,
  `QUANTIDADE` int(11) NOT NULL,
  `TIPO` int(11) NOT NULL COMMENT 'Se tipo for 1 ou 0 buscar na tabela brasindice no caso de 3 buscar na tabela de cobrancaplanos',
  `DATA_FATURADO` datetime NOT NULL,
  `PACIENTE_ID` int(11) NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  `VALOR_FATURA` double NOT NULL,
  `VALOR_CUSTO` double NOT NULL,
  `FATURA_ID` int(11) NOT NULL,
  `VALOR_GLOSA` double NOT NULL DEFAULT '0',
  `MOTIVO_GLOSA` varchar(300) CHARACTER SET utf8 NOT NULL,
  `UNIDADE` varchar(250) NOT NULL,
  `CODIGO_REFERENCIA` int(15) NOT NULL COMMENT 'QUando é editado recebe o id da tabela faturamento .',
  `TABELA_ORIGEM` varchar(50) NOT NULL,
  `DESCONTO_ACRESCIMO` double NOT NULL COMMENT 'se for negativo é desconto.',
  `INCLUSO_PACOTE` int(1) NOT NULL DEFAULT '0' COMMENT 'Se for 1 está incluso no pacote caso seja 0 não esta incluso.',
  `CANCELADO_POR` int(11) DEFAULT NULL,
  `CANCELADO_EM` datetime DEFAULT NULL,
  `TUSS` int(10) DEFAULT NULL,
  `custo_atualizado_em` datetime DEFAULT NULL,
  `custo_atualizado_por` int(11) DEFAULT NULL,
	faturamento_id int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT= 0  DEFAULT CHARSET=latin1;

-- SM-1675
ALTER TABLE `encaminhamento_medico`
ADD COLUMN `contrato_encaminhamento_id`  int(11) NULL AFTER `created_by`,
ADD COLUMN `tipo`  enum('externo','interno') NULL AFTER `contrato_encaminhamento_id`,
ADD COLUMN `nome_paciente`  varchar(255) NULL AFTER `tipo`;

update
encaminhamento_medico
SET
tipo = 'interno'
WHERE
tipo is null

DROP TABLE IF EXISTS `contrato_encaminhamento`;
CREATE TABLE `contrato_encaminhamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of contrato_encaminhamento
-- ----------------------------
INSERT INTO `contrato_encaminhamento` VALUES ('1', 'Sesc Piatã');
