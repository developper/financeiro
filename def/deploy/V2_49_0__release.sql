-- MEDICO
ALTER TABLE capmedica ADD empresa INT NOT NULL AFTER paciente; -- ok
ALTER TABLE capmedica ADD plano INT NOT NULL AFTER empresa;

ALTER TABLE relatorio_prorrogacao_med ADD empresa INT NOT NULL AFTER PACIENTE_ID; -- ok
ALTER TABLE relatorio_prorrogacao_med ADD plano INT NOT NULL AFTER empresa;

ALTER TABLE fichamedicaevolucao ADD empresa INT NOT NULL AFTER PACIENTE_ID; -- ok
ALTER TABLE fichamedicaevolucao ADD plano INT NOT NULL AFTER empresa;

ALTER TABLE relatorio_alta_med ADD empresa INT NOT NULL AFTER PACIENTE_ID; -- ok
ALTER TABLE relatorio_alta_med ADD plano INT NOT NULL AFTER empresa;

ALTER TABLE relatorio_deflagrado_med ADD empresa INT NOT NULL AFTER cliente_id; -- ok
ALTER TABLE relatorio_deflagrado_med ADD plano INT NOT NULL AFTER empresa;

ALTER TABLE intercorrencia_med ADD empresa INT NOT NULL AFTER cliente_id; -- ok
ALTER TABLE intercorrencia_med ADD plano INT NOT NULL AFTER empresa;

ALTER TABLE prescricoes ADD empresa INT NOT NULL AFTER paciente; -- ok
ALTER TABLE prescricoes ADD plano INT NOT NULL AFTER empresa;

SELECT
  MAX(ID),
  CONCAT(
      'UPDATE capmedica SET empresa = ', UR, ', ',
      'plano = ',
      PLANO_ID, ' WHERE id = ', CAPMEDICA_ID, ';'
  ) AS squery
FROM
  fatura
WHERE
  CAPMEDICA_ID IS NOT NULL
  AND CAPMEDICA_ID != 0
GROUP BY CAPMEDICA_ID;

SELECT
  id
FROM
  capmedica
WHERE
  id NOT IN (
    SELECT
      MAX(ID)
    FROM
      fatura
    WHERE
      CAPMEDICA_ID IS NOT NULL
      AND CAPMEDICA_ID != 0
    GROUP BY CAPMEDICA_ID
  );

-- ENFERMAGEM
ALTER TABLE avaliacaoenfermagem ADD empresa INT NOT NULL AFTER PACIENTE_ID;
ALTER TABLE avaliacaoenfermagem ADD convenio INT NOT NULL AFTER empresa;

ALTER TABLE evolucaoenfermagem ADD empresa INT NOT NULL AFTER PACIENTE_ID;
ALTER TABLE evolucaoenfermagem ADD convenio INT NOT NULL AFTER empresa;

ALTER TABLE relatorio_prorrogacao_enf ADD empresa INT NOT NULL AFTER PACIENTE_ID;
ALTER TABLE relatorio_prorrogacao_enf ADD plano INT NOT NULL AFTER empresa;

ALTER TABLE relatorio_alta_enf ADD empresa INT NOT NULL AFTER PACIENTE_ID;
ALTER TABLE relatorio_alta_enf ADD plano INT NOT NULL AFTER empresa;

ALTER TABLE relatorio_aditivo_enf ADD empresa INT NOT NULL AFTER PACIENTE_ID;
ALTER TABLE relatorio_aditivo_enf ADD plano INT NOT NULL AFTER empresa;

ALTER TABLE relatorio_visita_enf ADD empresa INT NOT NULL AFTER PACIENTE_ID;
ALTER TABLE relatorio_visita_enf ADD plano INT NOT NULL AFTER empresa;

ALTER TABLE relatorio_deflagrado_enf ADD empresa INT NOT NULL AFTER PACIENTE_ID;
ALTER TABLE relatorio_deflagrado_enf ADD plano INT NOT NULL AFTER empresa;

ALTER TABLE prescricao_curativo ADD empresa INT NOT NULL AFTER paciente;
ALTER TABLE prescricao_curativo ADD plano INT NOT NULL AFTER empresa;

ALTER TABLE prescricao_enf ADD empresa INT NOT NULL AFTER cliente_id;
ALTER TABLE prescricao_enf ADD plano INT NOT NULL AFTER empresa;

--  SM-1885
 CREATE TABLE saida_remessa  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `alfa` enum('CD','FSA','SSA/RMS','ALG','SUL/BA','SO/BA') NULL,
  `numerica` int(11) NULL,
  `empresa_id` int(11) NULL,
  `remessa` varchar(255) NULL,
  `tipo_saida` enum('PACIENTE','UR') NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `empresas`
ADD COLUMN `sigla_saida` enum('CD','FSA','SSA/RMS','SUL/BA','SO/BA','ALG') NULL COMMENT 'Siglas usadas para gerar remessa na tabela saida_remessa' AFTER `imposto_iss`;

UPDATE `empresas` SET `sigla_saida` = 'FSA' WHERE `id` = 11;
UPDATE `empresas` SET `sigla_saida` = 'CD' WHERE `id` = 1;
UPDATE `empresas` SET `sigla_saida` = 'SSA/RMS' WHERE `id` = 2;
UPDATE `empresas` SET `sigla_saida` = 'SUL/BA' WHERE `id` = 4;
UPDATE `empresas` SET `sigla_saida` = 'ALG' WHERE `id` = 5;
UPDATE `empresas` SET `sigla_saida` = 'SO/BA' WHERE `id` = 7;
UPDATE `empresas` SET `SIGLA_EMPRESA` = 'SO/BA' WHERE `id` = 7;

ALTER TABLE `saida`
ADD COLUMN `remessa` varchar(255) NULL AFTER `confirmacao_pedidointerno`;

--  SM-1884
ALTER TABLE `saida`
ADD COLUMN `saida_remessa_id` int(11) NULL AFTER `remessa`;