-- SM-1603
ALTER TABLE `fatura`
CHANGE COLUMN `remocao_externa` `remocao`  enum('N','S') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL AFTER `protocolo_envio`;


-- SM-1612
CREATE TABLE tipos_queixa
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(100) NOT NULL
);

INSERT INTO tipos_queixa VALUES
  (NULL, 'Febre'),
  (NULL, 'Problemas com GTT'),
  (NULL, 'Alteração de sinais vitais ( PA/G/ FC/FR/SPO2)'),
  (NULL, 'Abdomen Distendido'),
  (NULL, 'Alteração do padrão respiratório'),
  (NULL, 'Sonolência excessiva'),
  (NULL, 'Diarréia constante'),
  (NULL, 'Sangramento'),
  (NULL, 'Queda'),
  (NULL, 'Abertura de ferida'),
  (NULL, 'Edema'),
  (NULL, 'Dor no peito'),
  (NULL, 'Convulsão'),
  (NULL, 'Problemas com TQT'),
  (NULL, 'Vômito/ Náuseas'),
  (NULL, 'Melena (fezes com sangue)'),
  (NULL, 'Alteração de comportamento (agitação/ hipoativo/ tremores)'),
  (NULL, 'Outro');

CREATE TABLE encaminhamento_medico
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  paciente INT NOT NULL,
  queixa_outros TEXT,
  problema_relatado TEXT,
  inicio DATETIME NOT NULL,
  fim DATETIME,
  created_at DATETIME NOT NULL,
  created_by INT NOT NULL
);

CREATE TABLE encaminhamento_medico_conduta
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  encaminhamento_medico INT NOT NULL,
  conduta INT NOT NULL,
  remocao INT NOT NULL,
  conduta_medica TEXT
);

CREATE TABLE encaminhamento_medico_queixas
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  encaminhamento_medico INT NOT NULL,
  queixa INT NOT NULL
);

CREATE TABLE encaminhamento_medico_mudanca_status
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  encaminhamento_medico INT NOT NULL,
  status INT NOT NULL,
  data_hora DATETIME NOT NULL,
  usuario INT NOT NULL
);

ALTER TABLE encaminhamento_medico_conduta ADD medico_plantonista INT NULL;
ALTER TABLE encaminhamento_medico_conduta
  MODIFY COLUMN medico_plantonista INT AFTER encaminhamento_medico;

