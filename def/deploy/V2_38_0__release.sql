CREATE TABLE ocorrencia
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  demandatario INT NOT NULL,
  contato VARCHAR(255) NOT NULL,
  telefone_contato VARCHAR(16) NOT NULL,
  prioridade VARCHAR(10) NOT NULL,
  ocorrencia_paciente INT NOT NULL,
  paciente INT,
  data_ocorrencia DATE NOT NULL,
  hora_ocorrencia TIME NOT NULL,
  tipo_contato INT NOT NULL,
  descricao TEXT NOT NULL,
  classificacao INT NOT NULL,
  subclassificacao INT NOT NULL,
  encaminhamento INT NOT NULL,
  encaminhamento_nome VARCHAR(255) NOT NULL,
  status INT NOT NULL,
  retorno_hc INT NOT NULL,
  qualidade_retorno INT NOT NULL,
  descricao_qualidade_retorno TEXT NOT NULL,
  created_by INT NOT NULL,
  created_at DATETIME  NOT NULL,
  updated_by INT,
  updated_at DATETIME,
  finalizado_por INT NOT NULL,
  finalizado_em DATETIME NOT NULL
);

ALTER TABLE `ocorrencia`
MODIFY COLUMN `status`  varchar(20) NOT NULL AFTER `encaminhamento_nome`;

CREATE TABLE ocorrencia_ur
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  ocorrencia_id INT NOT NULL,
  empresa_id INT NOT NULL
);

CREATE TABLE ocorrencia_setor
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  ocorrencia_id INT NOT NULL,
  setor_id INT NOT NULL
);

CREATE TABLE ocorrencia_classificacao
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  classificacao VARCHAR(255) NOT NULL
);


INSERT INTO ocorrencia_classificacao (classificacao) VALUES ('Mat/Med');
INSERT INTO ocorrencia_classificacao (classificacao) VALUES ('Equipamentos');
INSERT INTO ocorrencia_classificacao (classificacao) VALUES ('Serviços');
INSERT INTO ocorrencia_classificacao (classificacao) VALUES ('Queixas Clínicas');
INSERT INTO ocorrencia_classificacao (classificacao) VALUES ('Marcação de Exames ou consultas');
INSERT INTO ocorrencia_classificacao (classificacao) VALUES ('Orientações');
INSERT INTO ocorrencia_classificacao (classificacao) VALUES ('Orientações Técnicas de Cuidados de Enfermagem');
INSERT INTO ocorrencia_classificacao (classificacao) VALUES ('Elogios');

DROP TABLE IF EXISTS `ocorrencia_subclassificacao`;
CREATE TABLE `ocorrencia_subclassificacao` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) DEFAULT NULL,
  `ocorrencia_classificacao_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Prescrições: Falta de prescrição / Dúvidas sobre administração / Divergência de itens enviados e itens prescritos', '1');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Entrega em horário inconveniente', '1');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Atraso na entrega', '1');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Entrega incompleta', '1');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Entrega errada', '1');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Nova solicitação', '1');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Solicitação de troca', '1');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Insumo com alteração do aspecto/ cor', '1');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Insumo com validade vencida', '1');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Troca por insatisfação da marca pela família/cuidador', '1');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Entrega em horários inconvenientes', '2');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Atraso na entrega', '2');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('solicitação de troca devido defeito', '2');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Entrega errada', '2');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Nova solicitação', '2');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Visita em horário inconveniente', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Atraso na visita', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Falta de visita sem justificativa', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Mudança frequente de profissional s/ aviso', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Postura profissional ( ética)', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Conversas paralelas ( fofocas)', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Recolhimento de material pela equipe', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Divergências das orientações e diagnóstico', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Ausência de retorno a família', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Redução de serviço s/ orientação', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Dificuldade de contato', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Solicitações de orientações da assistência', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Prontuários não recolhidos', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Atraso ou não recolhimento do lixo contaminado', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Dobras de plantões', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Tempo reduzido de atendimento', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Divergência do serviço prestado com o autorizado', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Alimentação dos técnicos', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Falha no uso dos EPIs', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Falha na higiene durante o atendimento', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Ausência de capacitação da equipe', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Puncionar acesso periférico', '3');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Febre', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Problemas com GTT', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Alteração de sinais vitais ( PA/G/ FC/FR/SPO2)', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Abdomen distendido', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Alteração do padrão respiratório', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Sonolência excessiva', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Diarréia constante', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Sangramento', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Queda', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Abertura de ferida', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Edema', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Dor no peito', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Convulsão', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Problemas com TQT', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Vômito/ Náuseas', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Melena (fezes com sangue)', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('outro', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Alteração de comportamento (agitação/ hipoativo/ tremores)', '4');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Solicitação', '5');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Atraso na marcação', '5');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Suspensão', '5');
INSERT INTO ocorrencia_subclassificacao (descricao, ocorrencia_classificacao_id) VALUES ('Local de atendimento', '5');

CREATE TABLE ocorrencia_resposta
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  ocorrencia_id INT NOT NULL,
  sugerido_resolvido INT NOT NULL,
  resposta VARCHAR(255) NOT NULL,
  created_at DATETIME NOT NULL,
  created_by INT NOT NULL
);