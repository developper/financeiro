CREATE TABLE historico_kit_enfermagem
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  kit INT NOT NULL,
  catalogo_id INT NOT NULL,
  usuario INT NOT NULL,
  tipo ENUM('i', 'e') DEFAULT 'i' NOT NULL,
  justificativa TEXT,
  data DATETIME NOT NULL
);
ALTER TABLE historico_kit_enfermagem COMMENT = 'Registra inserções e exclusões de itens nos kits de enfermagem.';