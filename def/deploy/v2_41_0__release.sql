-- SM-1780
ALTER TABLE `operadoras`
ADD COLUMN `data_pagamento`  varchar(255) NULL AFTER `EMAIL`,
ADD COLUMN `dia_maximo_envio_fatura`  int(2) NULL AFTER `data_pagamento`,
ADD COLUMN `created_by`  int(11) NULL AFTER `dia_maximo_envio_fatura`,
ADD COLUMN `created_at`  datetime NULL AFTER `created_by`,
ADD COLUMN `updated_by`  int(11) NULL AFTER `created_at`,
ADD COLUMN `updated_at`  datetime NULL AFTER `updated_by`,
ADD COLUMN `canceled_by`  int(11) NULL AFTER `updated_at`,
ADD COLUMN `canceled_at`  datetime NULL AFTER `canceled_by`;


CREATE TABLE `historico_operadora` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`operadora_id`  int(11) NULL ,
`nome`  varchar(255) NULL ,
`email`  varchar(255) NULL ,
`data_pagamento`  int(2) NULL ,
`dia_maximo_envio_fatura`  int(2) NULL ,
`created_by`  int(11) NULL ,
`created_at`  datetime NULL ,
`tipo`  varchar(255) NULL ,
PRIMARY KEY (`id`)
);
--
-- fim SM-1780

--
-- SM-1784
CREATE TABLE `fatura_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fatura_status
-- ----------------------------
INSERT INTO `fatura_status` VALUES ('-1', 'Rascunho');
INSERT INTO `fatura_status` VALUES ('1', 'Enviado');
INSERT INTO `fatura_status` VALUES ('2', 'Pendente');
INSERT INTO `fatura_status` VALUES ('3', 'Em Análise');
INSERT INTO `fatura_status` VALUES ('4', 'Processado');
INSERT INTO `fatura_status` VALUES ('5', 'Faturado');
INSERT INTO `fatura_status` VALUES ('6', 'Precificar');

-- Modifcar as faturas com status pendentes para precificar.
Update fatura set
STATUS = 6
where
fatura.STATUS = 2 and fatura.ORCAMENTO = 0;
--
-- Fim SM-1784


-- SM-1786
--

CREATE TABLE fatura_itens_originais_cobrados
(
  id                   INT AUTO_INCREMENT
    PRIMARY KEY,
  faturamento_id       INT                     NOT NULL,
  NUMERO_TISS          VARCHAR(11)             NOT NULL,
  CODIGO_PROPRIO       VARCHAR(11) DEFAULT '0' NOT NULL
  COMMENT 'Campo utilizado caso a operadora possua tabela propria para o item',
  CATALOGO_ID          INT                     NOT NULL,
  QUANTIDADE           INT                     NOT NULL,
  TIPO                 INT                     NOT NULL
  COMMENT 'Se tipo for 1 ou 0 buscar na tabela brasindice no caso de 3 buscar na tabela de cobrancaplanos',
  DATA_FATURADO        DATETIME                NOT NULL,
  PACIENTE_ID          INT                     NOT NULL,
  USUARIO_ID           INT                     NOT NULL,
  VALOR_FATURA         DOUBLE                  NOT NULL,
  VALOR_CUSTO          DOUBLE                  NOT NULL,
  FATURA_ID            INT                     NOT NULL,
  UNIDADE              VARCHAR(250)            NOT NULL,
  CODIGO_REFERENCIA    INT(15)                 NOT NULL
  COMMENT 'QUando é editado recebe o id da tabela faturamento .',
  TABELA_ORIGEM        VARCHAR(50)             NOT NULL,
  DESCONTO_ACRESCIMO   DOUBLE                  NOT NULL
  COMMENT 'se for negativo é desconto.',
  INCLUSO_PACOTE       INT(1) DEFAULT '0'      NOT NULL
  COMMENT 'Se for 1 está incluso no pacote caso seja 0 não esta incluso.',
  TUSS                 INT(10)                 NULL

);

CREATE TABLE fatura_consenso
(
  id                   INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  fatura_id            INT          NULL,
  created_by           INT          NULL,
  created_at           DATETIME     NULL,
  auditora_externa     VARCHAR(255) NULL,
  valor_total_faturado DOUBLE       NULL
);

DROP TABLE IF EXISTS `fatura_motivo_glosa`;
CREATE TABLE `fatura_motivo_glosa`  (
  `id` int(11) NOT NULL AUTO_INCREMENT ,
  `motivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of fatura_motivo_glosa
-- ----------------------------
INSERT INTO `fatura_motivo_glosa` VALUES (1, 'Quantidade a maior');
INSERT INTO `fatura_motivo_glosa` VALUES (2, 'Valor a maior');
INSERT INTO `fatura_motivo_glosa` VALUES (3, 'Cobrança não contratada');
INSERT INTO `fatura_motivo_glosa` VALUES (4, 'Falta de autorização');

SET FOREIGN_KEY_CHECKS = 1;
--
-- fim SM-1786