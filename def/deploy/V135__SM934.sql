-- UR FEIRA
SELECT
  c.nome AS PACIENTE,
  cp.item AS EQUIPAMENTO,
  ea.QTD AS QUANTIDADE,
  DATE_FORMAT(DATA_INICIO, '%d/%m/%Y') AS INICIO
FROM
  clientes AS c
  INNER JOIN `equipamentosativos` AS ea ON c.idClientes = ea.PACIENTE_ID
  INNER JOIN cobrancaplanos AS cp ON ea.COBRANCA_PLANOS_ID = cp.id
  LEFT JOIN empresas AS e ON c.empresa = e.id
WHERE
  `DATA_FIM` = '0000-00-00'
  AND `PRESCRICAO_AVALIACAO` <> 's'
  AND `PACIENTE_ID` = c.idClientes
  AND `DATA_INICIO` <> '0000-00-00'
  AND c.empresa = 11
ORDER BY
  c.nome;

-- UR SUL

SELECT
	c.nome AS PACIENTE,
	cp.item AS EQUIPAMENTO,
	ea.QTD AS QUANTIDADE,
	DATE_FORMAT(DATA_INICIO, '%d/%m/%Y') AS INICIO
FROM
	clientes AS c
INNER JOIN `equipamentosativos` AS ea ON c.idClientes = ea.PACIENTE_ID
INNER JOIN cobrancaplanos AS cp ON ea.COBRANCA_PLANOS_ID = cp.id
LEFT JOIN empresas AS e ON c.empresa = e.id
WHERE
	`DATA_FIM` = '0000-00-00'
AND `PRESCRICAO_AVALIACAO` <> 's'
AND `PACIENTE_ID` = c.idClientes
AND `DATA_INICIO` <> '0000-00-00'
AND c.empresa = 4
ORDER BY
	c.nome;