UPDATE planserv_plano_feridas_profissionais SET medico = 0, enfermeiro = 0;
ALTER TABLE planserv_plano_feridas_profissionais MODIFY medico INT;
ALTER TABLE planserv_plano_feridas_profissionais MODIFY enfermeiro INT;
ALTER TABLE planserv_plano_feridas_profissionais DROP cremeb;
ALTER TABLE planserv_plano_feridas_profissionais DROP coren;