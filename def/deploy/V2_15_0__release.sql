ALTER TABLE `clientes`
MODIFY COLUMN `empresa`  int(10) UNSIGNED NULL DEFAULT 1 AFTER `acompSolicitante`;

CREATE TABLE `atendimento_paciente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paciente_id` int(11) DEFAULT NULL,
  `atendimento_paciente_modalidade_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `plano_id` int(11) DEFAULT NULL,
  `matricula` varchar(255) DEFAULT NULL,
  `empresa` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `atendimento_paciente_procedimento` (
`id`  int(11) NULL AUTO_INCREMENT ,
`atendimento_paciente_id`  int(11) NULL ,
`tabela_procedimento`  varchar(255) NULL ,
`tabela_procedimento_id`  int(11) NULL ,
`created_at`  datetime NULL ,
`created_by`  int(11) NULL ,
PRIMARY KEY (`id`)
)
;

CREATE TABLE `atendimento_paciente_modalidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modalidade` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO `atendimento_paciente_modalidade` VALUES ('1', 'Assistência Domiciliar');
INSERT INTO `atendimento_paciente_modalidade` VALUES ('2', 'Internação Domiciliar 6h');
INSERT INTO `atendimento_paciente_modalidade` VALUES ('3', 'Internação Domiciliar 12h');
INSERT INTO `atendimento_paciente_modalidade` VALUES ('4', 'Internação Domiciliar 24h Com Respirador');
INSERT INTO `atendimento_paciente_modalidade` VALUES ('5', 'Internação Domiciliar 24h Sem Respirador');
INSERT INTO `atendimento_paciente_modalidade` VALUES ('6', 'Internação Domiciliar - Cuidados com a Sonda Naso Enteral (SNE)');
INSERT INTO `atendimento_paciente_modalidade` VALUES ('7', 'Internação Domiciliar - Treinamento de Sonda Vesical de Alívio');
INSERT INTO `atendimento_paciente_modalidade` VALUES ('8', 'Internação Domiciliar - Treinamento de Cuidados com a Gastrostomia (GTN)');
INSERT INTO `atendimento_paciente_modalidade` VALUES ('9', 'Internação Domiciliar - Treinamento de Administração de Medicações Subcutânea');
INSERT INTO `atendimento_paciente_modalidade` VALUES ('10', 'Internação Domiciliar - Treinamento de Cuidados Gerais com Pacientes Acamados e/ou Idosos');
INSERT INTO `atendimento_paciente_modalidade` VALUES ('11', 'Internação Domiciliar 24h para Antibióticoterapia (ATB) - 6/6h');
INSERT INTO `atendimento_paciente_modalidade` VALUES ('12', 'Internação Domiciliar 24h para Antibióticoterapia (ATB) - 8/8h');
INSERT INTO `atendimento_paciente_modalidade` VALUES ('13', 'Remoção Externa');

CREATE TABLE `atendimento_paciente_finalizado` (
`id`  int(11) NULL AUTO_INCREMENT ,
`status`  enum('C','F') NULL COMMENT 'F-Finalizado, C-cancelado' ,
`justificativa`  text NULL ,
`atendimento_paciente_id`  int(11) NULL ,
`finalized_by`  int(11) NULL ,
`finalized_at`  datetime NULL ,
PRIMARY KEY (`id`)
)
;