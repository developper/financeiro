ALTER TABLE catalogo_codigo_barras ADD COLUMN fornecedor_id INT(11) NOT NULL AFTER catalogo_id;
ALTER TABLE catalogo_codigo_barras ADD COLUMN produto_fornecedor_id INT(11) NOT NULL AFTER fornecedor_id;

TRUNCATE catalogo_codigo_barras;

