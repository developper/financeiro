-- SM-2067
ALTER TABLE formulario_condensado ADD updated_at datetime NULL;
ALTER TABLE formulario_condensado ADD updated_by int NULL;
ALTER TABLE formulario_condensado
  MODIFY COLUMN updated_by int AFTER created_at,
  MODIFY COLUMN updated_at datetime AFTER updated_by;

ALTER TABLE relatorio_prestador ADD updated_at datetime NULL;
ALTER TABLE relatorio_prestador ADD updated_by int NULL;
ALTER TABLE relatorio_prestador
  MODIFY COLUMN updated_by int AFTER created_at,
  MODIFY COLUMN updated_at datetime AFTER updated_by;