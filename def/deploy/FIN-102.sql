-- gerando o topco Despesas Não Operacionais 
INSERT INTO orcamento_financeiro_topicos (id, descricao) VALUES
	 (105,'Despesas Não Operacionais');

--   
INSERT INTO orcamento_financeiro_estrutura (versao_estrutura,orcamento_finananceiro_topicos_id,ordem,id_pai,aliquota) VALUES
	 (2,105,3,71,0.00);


-- Gerando valor do orçamento para o item despesas não operacionais.
     INSERT INTO orcamento_financeiro_topicos_valores (orcamento_financeiro_topicos,orcamento_financeiro_versao_id,valor_mensal,criado_em,criado_por,editado_em,editado_por,deletado_em,deletado_por,`data`,aliquota) VALUES
	 (105,11,0,NULL,NULL,'2021-09-14 11:57:00',18,NULL,NULL,'2021-01',0.00),
	 (105,11,0,NULL,NULL,'2021-09-14 11:57:00',18,NULL,NULL,'2021-02',0.00),
	 (105,11,0,NULL,NULL,'2021-09-14 11:57:00',18,NULL,NULL,'2021-03',0.00),
	 (105,11,0,NULL,NULL,'2021-09-14 11:57:00',18,NULL,NULL,'2021-04',0.00),
	 (105,11,0,NULL,NULL,'2021-09-14 11:57:00',18,NULL,NULL,'2021-05',0.00),
	 (105,11,0,NULL,NULL,'2021-09-14 11:57:00',18,NULL,NULL,'2021-06',0.00),
	 (105,11,0,NULL,NULL,'2021-09-14 11:57:00',18,NULL,NULL,'2021-07',0.00),
	 (105,11,0,NULL,NULL,'2021-09-14 12:19:33',18,NULL,NULL,'2021-08',0.00),
	 (105,11,0,NULL,NULL,'2021-09-14 12:22:52',18,NULL,NULL,'2021-09',0.00),
	 (105,11,0,NULL,NULL,'2021-09-14 12:26:38',18,NULL,NULL,'2021-10',0.00);
INSERT INTO orcamento_financeiro_topicos_valores (orcamento_financeiro_topicos,orcamento_financeiro_versao_id,valor_mensal,criado_em,criado_por,editado_em,editado_por,deletado_em,deletado_por,`data`,aliquota) VALUES
	 (105,11,0,NULL,NULL,'2021-09-14 12:33:41',18,NULL,NULL,'2021-11',0.00),
	 (105,11,0,NULL,NULL,'2021-09-14 12:39:30',18,NULL,NULL,'2021-12',0.00);