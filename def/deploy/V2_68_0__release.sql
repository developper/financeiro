ALTER TABLE `empresas` 
ADD COLUMN `iw_id` int(11) NULL AFTER `ur_controle_estoque`;

UPDATE `empresas` SET `iw_id` = 1 WHERE `id` = 1;
UPDATE `empresas` SET `iw_id` = 2 WHERE `id` = 4;
UPDATE `empresas` SET `iw_id` = 7 WHERE `id` = 5;
UPDATE `empresas` SET `iw_id` = 5 WHERE `id` = 11;
UPDATE `empresas` SET `iw_id` = 3 WHERE `id` = 15;
UPDATE `empresas` SET `iw_id` = 4 WHERE `id` = 16;

ALTER TABLE `notas`
ADD COLUMN `importada_iw` enum('N','S') NULL DEFAULT 'N' AFTER `nota_aglutinacao`;

ALTER TABLE `parcelas` 
MODIFY COLUMN `vencimento` date NOT NULL COMMENT 'Recebe o vencimento original da parcela.' AFTER `valor`;

ALTER TABLE `parcelas` 
MODIFY COLUMN `codBarras` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `vencimento`;

UPDATE `plano_contas` SET `COD_N3` = '1.0.2' WHERE `ID` = 5;
UPDATE `plano_contas` SET `COD_N3` = '1.0.2' WHERE `ID` = 6;
UPDATE `plano_contas` SET `COD_N3` = '1.0.2' WHERE `ID` = 7;
UPDATE `plano_contas` SET `COD_N3` = '1.0.2' WHERE `ID` = 8;
UPDATE `plano_contas` SET `COD_N3` = '1.0.2' WHERE `ID` = 9;
UPDATE `plano_contas` SET `COD_N3` = '1.0.3' WHERE `ID` = 10;
UPDATE `plano_contas` SET `COD_N3` = '1.0.3', `COD_N3`= '1.0.3.02' WHERE `ID` = 11;
UPDATE `plano_contas` SET `COD_N3` = '1.0.3' WHERE `ID` = 12;
UPDATE `plano_contas` SET `COD_N3` = '1.0.4' WHERE `ID` = 13;
UPDATE `plano_contas` SET `COD_N3` = '1.0.4' WHERE `ID` = 14;
UPDATE `plano_contas` SET `COD_N3` = '1.0.4' WHERE `ID` = 15;
UPDATE `plano_contas` SET `COD_N3` = '1.0.4' WHERE `ID` = 16;
UPDATE `plano_contas` SET `COD_N3` = '1.0.4' WHERE `ID` = 17;

UPDATE `plano_contas` SET N2    = 'RECEITA PREVISTA', COD_N2 = '1.2', COD_N3 = '1.2.1', COD_N4 = '1.2.1.01' WHERE `ID` = 18;
UPDATE `plano_contas` SET N2 = 'RECEITA PREVISTA', COD_N2 = '1.2', COD_N3 = '1.2.1', COD_N4 = '1.2.1.02' WHERE `ID` = 19;
UPDATE `plano_contas` SET N2 = 'RECEITA PREVISTA', COD_N2 = '1.2', COD_N3 = '1.2.1', COD_N4 = '1.2.1.03' WHERE `ID` = 20;

INSERT INTO `assinaturas`(`DESCRICAO`, `CODIGO_DOMINIO`) VALUES ('RECEITA -IMPORTAÇÃO IW', 990);

CREATE TABLE `notas_lote_conta_receber`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT  PRIMARY KEY,
  `nota_id` int(0) NOT NULL,
  `lote` varchar(255) NOT NULL COMMENT 'lote gerado no sistema meio que gera arquivo de importação'
  
);