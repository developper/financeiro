CREATE TABLE `prorrogacao_planserv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paciente_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `inicio` date DEFAULT NULL,
  `fim` date DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `modalidade` varchar(80) DEFAULT NULL,
  `admissao` varchar(50) DEFAULT NULL,
  `criterio` smallint(2) DEFAULT NULL,
  `fonoterapia` smallint(2) DEFAULT NULL,
  `fisioterapia` smallint(2) DEFAULT NULL,
  `o2` char(1) DEFAULT NULL,
  `dieta` enum('art','ind','mis') DEFAULT NULL COMMENT 'art = Artesanal / ind = Industrializada / mis = Mista',
  `via` enum('oral','sne','gtm','jej','bi','grav') DEFAULT NULL COMMENT 'Oral / SNE / GTM / jej = Jejuno / BI / grav = Gravitacional',
  `deambula` enum('ss','sc','n') DEFAULT NULL COMMENT 'ss = Sim Sem Auxilio / sc = Sim Com Auxilio / n = Não',
  `deambula_auxilio` varchar(50) DEFAULT NULL,
  `drenos` char(1) DEFAULT NULL,
  `familia_apta` char(1) DEFAULT NULL,
  `integridade_cultanea` char(1) DEFAULT NULL,
  `lesoes` tinytext,
  `atb` char(1) DEFAULT NULL,
  `atb_descricao` varchar(100) DEFAULT NULL,
  `previsao_termino_atb` date NOT NULL DEFAULT '0000-00-00',
  `antifungico` char(1) DEFAULT NULL,
  `antifungico_descricao` varchar(100) DEFAULT NULL,
  `previsao_termino_antifungico` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `prorrogacao_planserv_enfermagem`
-- ----------------------------
CREATE TABLE `prorrogacao_planserv_enfermagem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prorrogacao_planserv_id` int(11) NOT NULL,
  `prorrogacao_enfermagem` int(11) NOT NULL,
  `evolucao` text,
  `data_visita` date NOT NULL DEFAULT '0000-00-00',
  `nome` varchar(120) DEFAULT NULL,
  `coren` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `prorrogacao_planserv_fisioterapia`
-- ----------------------------
CREATE TABLE `prorrogacao_planserv_fisioterapia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prorrogacao_planserv_id` int(11) NOT NULL,
  `prorrogacao_fisioterapia` int(11) NOT NULL,
  `evolucao` text,
  `data_visita` date NOT NULL DEFAULT '0000-00-00',
  `nome` varchar(120) DEFAULT NULL,
  `crefitto` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of prorrogacao_planserv_fisioterapia
-- ----------------------------

-- ----------------------------
-- Table structure for `prorrogacao_planserv_fonoaudiologia`
-- ----------------------------
CREATE TABLE `prorrogacao_planserv_fonoaudiologia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prorrogacao_planserv_id` int(11) NOT NULL,
  `prorrogacao_fonoaudiologia` int(11) NOT NULL,
  `evolucao` text,
  `data_visita` date NOT NULL DEFAULT '0000-00-00',
  `nome` varchar(120) DEFAULT NULL,
  `crfa` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `prorrogacao_planserv_medico`
-- ----------------------------
CREATE TABLE `prorrogacao_planserv_medico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prorrogacao_planserv_id` int(11) NOT NULL,
  `prorrogacao_medica` int(11) NOT NULL,
  `evolucao` text,
  `data_visita` date NOT NULL DEFAULT '0000-00-00',
  `nome` varchar(120) DEFAULT NULL,
  `crm` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `prorrogacao_planserv_nutricao`
-- ----------------------------
CREATE TABLE `prorrogacao_planserv_nutricao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prorrogacao_planserv_id` int(11) NOT NULL,
  `prorrogacao_nutricao` int(11) NOT NULL,
  `evolucao` text NOT NULL,
  `data_visita` date DEFAULT '0000-00-00',
  `nome` varchar(120) DEFAULT NULL,
  `crn` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `prorrogacao_planserv_solicitacoes`
-- ----------------------------
CREATE TABLE `prorrogacao_planserv_solicitacoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prorrogacao_planserv_id` int(11) NOT NULL,
  `modalidade` tinytext,
  `fisioterapia` tinytext,
  `fonoterapia` tinytext,
  `dietas` tinytext,
  `curativos` tinytext,
  `oxigenoterapia` tinytext,
  `equipamentos` tinytext,
  `atb_med` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;