-- SM-1186
CREATE TABLE historico_mudanca_entrada
(
  id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  usuario INT(11) NOT NULL,
  id_entrada INT(11) NOT NULL,
  data DATETIME(11) NOT NULL,
  catalogo_id_novo INT(11) NOT NULL,
  catalogo_id_anterior INT(11) NOT NULL,
  lote_novo VARCHAR(100),
  lote_anterior VARCHAR(100),
  qtd_novo INT(11) NOT NULL,
  qtd_anterior INT(11) NOT NULL,
  validade_novo DATE,
  validade_anterior DATE
);
ALTER TABLE historico_mudanca_entrada COMMENT = 'Grava o historico de mudança das entradas de produtos';