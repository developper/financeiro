ALTER TABLE `solicitacoes`
ADD COLUMN `visualizado_logistica`  tinyint(1) NULL DEFAULT 0 ;

UPDATE `solicitacoes`
SET visualizado_logistica = 1
WHERE enviado > 0;
