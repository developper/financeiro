-- SM-377

CREATE TABLE `devolucao_interna_pedido` (
`id`  int(11) NULL AUTO_INCREMENT ,
`created_by`  int(11) NULL ,
`created_at`  DATETIME NULL ,
`ur_origem`  int(11) NULL ,
`ur_destino`  int(11) NULL ,
`canceled_by`  int NULL ,
`canceled_at`  DATETIME NULL ,
`motivo_cancelamento`  text NULL ,
PRIMARY KEY (`id`)
)
;

CREATE TABLE `devolucao_interna_itens` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`catalogo_id`  int(11) NOT NULL ,
`tipo`  int(11) NOT NULL ,
`lote`  varchar(30) NULL ,
`vencimento`  datetime NULL ,
`quantidade`  int(11) NOT NULL ,
`observacao`  text NULL ,
PRIMARY KEY (`id`)
)
;

ALTER TABLE `devolucao_interna_itens`
MODIFY COLUMN `vencimento`  date NULL DEFAULT NULL AFTER `lote`,
ADD COLUMN `devolucao_interna_pedido_id`  int(11) NULL AFTER `observacao`;

CREATE TABLE devolucao_interna_motivo (
`id`  int(11) NULL AUTO_INCREMENT ,
`motivo`  varchar(100) NULL ,
PRIMARY KEY (`id`)
)
;

ALTER TABLE `devolucao_interna_itens`
ADD COLUMN `devolucao_interna_motivo_id`  int(11) NULL AFTER `devolucao_interna_pedido_id`;

ALTER TABLE `devolucao_interna_pedido`
ADD COLUMN `status`  enum('FINALIZADO','ACEITO','PENDENTE') NULL AFTER `motivo_cancelamento`,
ADD COLUMN `accepted_by`  int NULL AFTER `status`,
ADD COLUMN `aceitado_at`  datetime NULL AFTER `accepted_by`;
-- Fim SM-377

-- SM-1651
ALTER TABLE `fatura`
ADD COLUMN `prescricao_id`  int(11) NULL AFTER `remocao`;
ALTER TABLE `fatura`
ADD COLUMN `tag_prescricao`  varchar(255) NULL COMMENT 'Indica a origem da prescrição.' AFTER `prescricao_id`;

INSERT INTO `devolucao_interna_motivo` VALUES (1, 'Erro na compra (material solicitado por engano)');
INSERT INTO `devolucao_interna_motivo` VALUES (2, 'Material enviado divergente do solicitado (erro na substituição do item)');
INSERT INTO `devolucao_interna_motivo` VALUES (3, 'Material com validade próxima (menor que três meses)');
INSERT INTO `devolucao_interna_motivo` VALUES (4, 'Material avariado no trajeto');
INSERT INTO `devolucao_interna_motivo` VALUES (5, 'Alta do paciente que utilizaria o item.');
INSERT INTO `devolucao_interna_motivo` VALUES (6, 'Equalização do estoque (devolução por excesso de estoque)');
INSERT INTO `devolucao_interna_motivo` VALUES (7, 'Requisição da CD-Logística (pedido devolução para UR para enviar para outra unidade)');