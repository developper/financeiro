-- SM-1642

ALTER TABLE fornecedores ADD validado_receita INT DEFAULT 0 NULL;
ALTER TABLE fornecedores
  MODIFY COLUMN validado_receita INT DEFAULT 0 AFTER responsavel_equipamento;