ALTER TABLE area_sistema ADD link text NOT NULL;
ALTER TABLE area_sistema ADD menu INT(11) NOT NULL DEFAULT '1' AFTER id_pai;
ALTER TABLE `area_sistema`
ADD COLUMN `caminho_icone`  text NULL AFTER `link`;



-- AREAS DO SISTEMAINSERT INTO `area_sistema` VALUES ('1', 'mundanca_sistema', 'Mudanças do Sistema', '1', '0', '1', '/inicio.php', '/utils/new-icons/home.png');
INSERT INTO `area_sistema` VALUES ('2', 'ocorrencia', 'Ocorrências', '2', '0', '1', '/ocorrencia/?action=listar-ocorrencias', '/utils/new-icons/ocorrencia.png');
INSERT INTO `area_sistema` VALUES ('3', 'administracao', 'Administração', '3', '0', '1', '/adm', '/utils/new-icons/administration.png');
INSERT INTO `area_sistema` VALUES ('4', 'administracao_filial', 'Filiais', '1', '3', '1', '', null);
INSERT INTO `area_sistema` VALUES ('5', 'administracao_paciente', 'Pacientes', '2', '3', '1', '', null);
INSERT INTO `area_sistema` VALUES ('6', 'administracao_paciente_listar', 'Listar', '1', '5', '1', '', null);
INSERT INTO `area_sistema` VALUES ('7', 'administracao_paciente_solicitar_avaliacao', 'Solicitação de Avaliação', '2', '5', '1', '', null);
INSERT INTO `area_sistema` VALUES ('8', 'administracao_paciente_prescricao_curativo_enfermagem', 'Prescrições de Curativo de Enfermagem', '3', '5', '1', '', null);
INSERT INTO `area_sistema` VALUES ('9', 'administracao_operadora', 'Operadoras', '3', '3', '1', '', null);
INSERT INTO `area_sistema` VALUES ('10', 'administracao_plano', 'Planos', '4', '3', '1', '', null);
INSERT INTO `area_sistema` VALUES ('11', 'administracao_usuario', 'Usuários', '5', '3', '1', '', null);
INSERT INTO `area_sistema` VALUES ('12', 'administracao_usuario_listar', 'Listar', '1', '11', '1', '', null);
INSERT INTO `area_sistema` VALUES ('13', 'administracao_usuario_novo', 'Novo Usuário', '2', '11', '1', '', null);
INSERT INTO `area_sistema` VALUES ('14', 'administracao_importar_assinaturas', 'Importar Assinaturas', '6', '3', '1', '', null);
INSERT INTO `area_sistema` VALUES ('15', 'administracao_relatorio', 'Relatórios', '7', '3', '1', '', null);
INSERT INTO `area_sistema` VALUES ('16', 'administracao_relatorio_historico_modalidade', 'Histórico de Mudança de Modalidade Por Paciente', '1', '15', '1', '', null);
INSERT INTO `area_sistema` VALUES ('17', 'administracao_relatorio_epidemiologico', 'Relatório Epidemiológico', '2', '15', '1', '', null);
INSERT INTO `area_sistema` VALUES ('18', 'administracao_relatorio_fatura_liberada', 'Relatório de Faturas Liberadas', '3', '15', '1', '', null);
INSERT INTO `area_sistema` VALUES ('19', 'administracao_relatorio_faturamento_enviado', 'Relatorio de Faturamento Enviado', '4', '15', '1', '', null);
INSERT INTO `area_sistema` VALUES ('20', 'administracao_relatorio_assistencial', 'Relatórios Assistenciais', '8', '3', '1', '', null);
INSERT INTO `area_sistema` VALUES ('21', 'administracao_visualizar_autorizacao', 'Visualizar Autorizações', '9', '3', '1', '', null);
INSERT INTO `area_sistema` VALUES ('22', 'administracao_plano_tratamento_ferida', 'Plano Tratamento de Feridas', '10', '3', '1', '', null);
INSERT INTO `area_sistema` VALUES ('23', 'medico', 'Médicos', '4', '0', '1', '/medico', '/utils/new-icons/medic.png');
INSERT INTO `area_sistema` VALUES ('24', 'medico_paciente', 'Pacientes', '1', '23', '1', '', null);
INSERT INTO `area_sistema` VALUES ('25', 'medico_prescricao', 'Pescrições', '2', '23', '1', '', null);
INSERT INTO `area_sistema` VALUES ('26', 'medico_suspender_item_prescricao_ativa', 'Suspender Itens em Prescrições Ativas', '3', '23', '1', '', null);
INSERT INTO `area_sistema` VALUES ('27', 'medico_relatorio', 'Relatórios', '4', '23', '1', '', null);
INSERT INTO `area_sistema` VALUES ('28', 'medico_relatorio_assistencial', 'Relatórios Assistenciais', '1', '27', '1', '', null);
INSERT INTO `area_sistema` VALUES ('29', 'exame', 'Exames', '5', '0', '1', '/exames/?action=menu', '/utils/new-icons/lab-exams2.png');
INSERT INTO `area_sistema` VALUES ('30', 'exame_consultar', 'Consultar Exames', '1', '29', '1', '', null);
INSERT INTO `area_sistema` VALUES ('31', 'exame_novo', 'Novo Exame', '2', '29', '1', '', null);
INSERT INTO `area_sistema` VALUES ('32', 'enfermagem', 'Enfermagem', '6', '0', '1', '/enfermagem', '/utils/new-icons/nurse.png');
INSERT INTO `area_sistema` VALUES ('33', 'enfermagem_paciente', 'Pacientes', '1', '32', '1', '', null);
INSERT INTO `area_sistema` VALUES ('34', 'enfermagem_solicitacao', 'Solicitações', '2', '32', '1', '', null);
INSERT INTO `area_sistema` VALUES ('35', 'enfermagem_solicitacao_avulsa', 'Solicitacao Avulsa', '3', '32', '1', '', null);
INSERT INTO `area_sistema` VALUES ('36', 'enfermagem_buscar_solicitacao_prescricao', 'Buscar Solicitações / Prescrições', '4', '32', '1', '', null);
INSERT INTO `area_sistema` VALUES ('37', 'enfermagem_prescricao_enfermagem', 'Prescrições de Enfermagem', '5', '32', '1', '', null);
INSERT INTO `area_sistema` VALUES ('38', 'enfermagem_prescricao_curativo', 'Prescrições de Curativos', '6', '32', '1', '', null);
INSERT INTO `area_sistema` VALUES ('39', 'enfermagem_buscar_prescricao_medica', 'Buscar Prescrições Médicas', '7', '32', '1', '', null);
INSERT INTO `area_sistema` VALUES ('40', 'enfermagem_kit', 'Kits', '8', '32', '1', '', null);
INSERT INTO `area_sistema` VALUES ('41', 'enfermagem_brasindice', 'Brasindice', '9', '32', '1', '', null);
INSERT INTO `area_sistema` VALUES ('42', 'enfermagem_relatorio', 'Relatórios', '10', '32', '1', '', null);
INSERT INTO `area_sistema` VALUES ('43', 'enfermagem_relatorio_assistencial', 'Relatórios Assistenciais', '1', '42', '1', '', null);
INSERT INTO `area_sistema` VALUES ('44', 'enfermagem_relatorio_nao_conformidade', 'Relatório de Não Conformidade', '2', '42', '1', '', null);
INSERT INTO `area_sistema` VALUES ('45', 'enfermagem_relatorio_plano_tratamento_ferida', 'Plano Tratamento de Feridas', '3', '42', '1', '', null);
INSERT INTO `area_sistema` VALUES ('46', 'nutricao', 'Nutrição', '7', '0', '1', '/nutricao', '/utils/new-icons/nutritionist.png');
INSERT INTO `area_sistema` VALUES ('47', 'nutricao_paciente', 'Pacientes', '1', '46', '1', '', null);
INSERT INTO `area_sistema` VALUES ('48', 'nutricao_prescricao', 'Prescrições de Nutrição', '2', '46', '1', '', null);
INSERT INTO `area_sistema` VALUES ('49', 'auditoria', 'Auditoria Interna', '8', '0', '1', '/auditoria', '/utils/new-icons/audit.png');
INSERT INTO `area_sistema` VALUES ('50', 'auditoria_auditar_solicitacao', 'Auditar Solicitações', '1', '49', '1', '', null);
INSERT INTO `area_sistema` VALUES ('51', 'auditoria_faturar', 'Faturar', '2', '49', '1', '', null);
INSERT INTO `area_sistema` VALUES ('52', 'auditoria_listar_fatura', 'Listar Faturas', '3', '49', '1', '', null);
INSERT INTO `area_sistema` VALUES ('53', 'auditoria_orcamento_inicial', 'Orçamento Inicial', '4', '49', '1', '', null);
INSERT INTO `area_sistema` VALUES ('54', 'auditoria_orcamento_prorrogacao', 'Orçamento de Prorrogação', '5', '49', '1', '', null);
INSERT INTO `area_sistema` VALUES ('55', 'auditoria_relatorio', 'Relatórios', '6', '49', '1', '', null);
INSERT INTO `area_sistema` VALUES ('56', 'auditoria_visualizar_autorizacao', 'Visualizar Autorizações', '7', '49', '1', '', null);
INSERT INTO `area_sistema` VALUES ('57', 'auditoria_relatorio_fatura_liberada', 'Relatório de Faturas Liberadas', '1', '55', '1', '', null);
INSERT INTO `area_sistema` VALUES ('58', 'auditoria_relatorio_pedido_liberado', 'Relatório de Pedidos Liberados', '2', '55', '1', '', null);
INSERT INTO `area_sistema` VALUES ('59', 'auditoria_relatorio_faturamento_enviado', 'Relatório de Faturamento Enviado', '3', '55', '1', '', null);
INSERT INTO `area_sistema` VALUES ('60', 'auditoria_relatorio_pedido', 'Relatório de Pedido', '4', '55', '1', '', null);
INSERT INTO `area_sistema` VALUES ('61', 'auditoria_relatorio_nao_conformidade', 'Relatório de Não Conformidades', '6', '55', '1', '', null);
INSERT INTO `area_sistema` VALUES ('62', 'auditar_relatorio_nao_conformidade_fazer', 'Fazer', '1', '61', '1', '', null);
INSERT INTO `area_sistema` VALUES ('63', 'auditar_relatorio_nao_conformidade_gerenciar', 'Gerenciar', '2', '61', '1', '', null);
INSERT INTO `area_sistema` VALUES ('64', 'auditar_relatorio_nao_conformidade_busca_condensada', 'Busca Condensada', '3', '61', '1', '', null);
INSERT INTO `area_sistema` VALUES ('65', 'remocao', 'Remoções', '9', '0', '1', '/remocao/?action=menu', '/utils/new-icons/ambulance.png');
INSERT INTO `area_sistema` VALUES ('66', 'remocao_nova_interna', 'Nova Remoção Interna', '1', '65', '1', '', null);
INSERT INTO `area_sistema` VALUES ('67', 'remocao_nova_externa', 'Nova Remoção Externa', '2', '65', '1', '', null);
INSERT INTO `area_sistema` VALUES ('68', 'remocao_pesquisar', 'Pesquisar Remoções', '3', '65', '1', '', null);
INSERT INTO `area_sistema` VALUES ('69', 'financeiro', 'Financeiro', '10', '0', '1', '/financeiros', '/utils/new-icons/financial2.png');
INSERT INTO `area_sistema` VALUES ('70', 'faturamento', 'Faturamento', '11', '0', '1', '/faturamento', '/utils/new-icons/financial.png');
INSERT INTO `area_sistema` VALUES ('71', 'logistica', 'Logística', '12', '0', '1', '/logistica', '/utils/new-icons/logistics.png');
INSERT INTO `area_sistema` VALUES ('72', 'sac', 'SAC', '13', '0', '1', '/sac', '/utils/sac.png');
INSERT INTO `area_sistema` VALUES ('73', 'ajuda_publicacao', 'Ajuda e Publicações', '14', '0', '1', '/tutorial', '/utils/new-icons/help.jpg');
INSERT INTO `area_sistema` VALUES ('74', 'alterar_senha', 'Alterar Senha', '15', '0', '1', '', null);
INSERT INTO `area_sistema` VALUES ('75', 'medico_paciente_avaliacao', 'Avaliação Médica', '1', '24', '0', '', null);
INSERT INTO `area_sistema` VALUES ('76', 'medico_paciente_evolucao', 'Evolução Médica', '2', '24', '0', '', null);
INSERT INTO `area_sistema` VALUES ('77', 'medico_paciente_prorrogacao', 'Prorrogação Médica', '3', '24', '0', '', null);
INSERT INTO `area_sistema` VALUES ('78', 'medico_paciente_intercorrencia', 'Intercorrência Médica', '4', '24', '0', '', null);
INSERT INTO `area_sistema` VALUES ('79', 'medico_paciente_score', 'Score Paciente', '5', '24', '0', '', null);
INSERT INTO `area_sistema` VALUES ('80', 'medico_paciente_alta', 'Alta Médica', '6', '24', '0', '', null);
INSERT INTO `area_sistema` VALUES ('81', 'medico_paciente_deflagrado', 'Relatório Deflagrado Médico', '7', '24', '0', '', null);
INSERT INTO `area_sistema` VALUES ('82', 'medico_paciente_aditivo', 'Aditivo Médico', '8', '24', '0', '', null);
INSERT INTO `area_sistema` VALUES ('83', 'enfermagem_paciente_avaliacao', 'Avaliação de Enfermagem', '1', '33', '0', '', null);
INSERT INTO `area_sistema` VALUES ('84', 'enfermagem_paciente_evolucao', 'Evolução de Enfermagem', '2', '33', '0', '', null);
INSERT INTO `area_sistema` VALUES ('85', 'enfermagem_paciente_prorrogacao', 'Prorrogação de Enfermagem', '3', '33', '0', '', null);
INSERT INTO `area_sistema` VALUES ('86', 'enfermagem_paciente_alta', 'Alta de Enfermagem', '4', '33', '0', '', null);
INSERT INTO `area_sistema` VALUES ('87', 'enfermagem_paciente_deflagrado', 'Deflagrado de Enfermagem', '5', '33', '0', '', null);
INSERT INTO `area_sistema` VALUES ('88', 'enfermagem_paciente_aditivo', 'Aditivo de Enfermagem', '6', '33', '0', '', null);
INSERT INTO `area_sistema` VALUES ('89', 'enfermagem_paciente_procedimentos_gtm', 'Procedimentos em GTM', '7', '33', '0', '', null);
INSERT INTO `area_sistema` VALUES ('90', 'enfermagem_paciente_visita_semanal', 'Visita Semanal', '8', '33', '0', '', null);



-- PERMISSOES
INSERT INTO `area_permissoes` VALUES ('1', '25', '1', 'medico_prescricao_novo', 'Prescrever');
INSERT INTO `area_permissoes` VALUES ('2', '25', '2', 'medico_prescricao_usar_para_nova', 'Usar Prescrição para Nova');
INSERT INTO `area_permissoes` VALUES ('3', '25', '3', 'medico_prescricao_visualizar', 'Visualizar Prescrições');
INSERT INTO `area_permissoes` VALUES ('4', '25', '4', 'medico_prescricao_editar', 'Editar Prescrições');
INSERT INTO `area_permissoes` VALUES ('15', '34', '1', 'enfermagem_solicitacao_pendente_iniciar', 'Iniciar Solicitação de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('16', '34', '2', 'enfermagem_solicitacao_pendente_desativar', 'Desativar Solicitação de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('17', '35', '1', 'enfermagem_solicitacao_avulsa_novo', 'Fazer Solicitação Avulsa');
INSERT INTO `area_permissoes` VALUES ('18', '37', '1', 'enfermagem_prescricao_enfermagem_novo', 'Criar Prescrição de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('19', '37', '2', 'enfermagem_prescricao_enfermagem_editar', 'Editar Prescrição de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('20', '37', '3', 'enfermagem_prescricao_enfermagem_usar_para_nova', 'Usar Para Nova Prescrição de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('21', '38', '1', 'enfermagem_prescricao_curativo_novo', 'Criar Prescrição de Curativos');
INSERT INTO `area_permissoes` VALUES ('22', '38', '2', 'enfermagem_prescricao_curativo_editar', 'Editar Prescrição de Curativos');
INSERT INTO `area_permissoes` VALUES ('23', '38', '3', 'enfermagem_prescricao_curativo_usar_para_nova', 'Usar Para Nova Prescrição de Curativos');
INSERT INTO `area_permissoes` VALUES ('24', '45', '1', 'enfermagem_relatorio_plano_tratamento_ferida_novo', 'Criar Plano de Tratamento de Feridas Planserv');
INSERT INTO `area_permissoes` VALUES ('25', '45', '2', 'enfermagem_relatorio_plano_tratamento_editar', 'Editar Plano de Tratamento de Feridas Planserv');
INSERT INTO `area_permissoes` VALUES ('26', '45', '3', 'enfermagem_relatorio_plano_tratamento_ferida_usar_para_nova', 'Usar Para Nova Plano de Tratamento de Feridas Planserv');
INSERT INTO `area_permissoes` VALUES ('27', '40', '1', 'enfermagem_kit_novo', 'Criar Kit de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('28', '40', '2', 'enfermagem_kit_adicionar_material', 'Adicionar Material em Kit de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('29', '40', '3', 'enfermagem_kit_excluir', 'Excluir Kit de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('30', '38', '4', 'enfermagem_prescricao_curativo_cancelar', 'Cancelar Prescrição de Curativos');
INSERT INTO `area_permissoes` VALUES ('34', '83', '1', 'enfermagem_paciente_avaliacao_novo', 'Criar Avaliação de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('35', '83', '2', 'enfermagem_paciente_avaliacao_editar', 'Editar Avaliação de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('36', '83', '3', 'enfermagem_paciente_avaliacao_usar_para_nova', 'Usar para Nova Avaliação de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('37', '84', '1', 'enfermagem_paciente_evolucao_novo', 'Criar Evolução de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('38', '84', '2', 'enfermagem_paciente_evolucao_editar', 'Editar Evolução de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('39', '84', '3', 'enfermagem_paciente_evolucao_usar_para_nova', 'Usar para Nova Evolução de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('40', '85', '1', 'enfermagem_paciente_prorrogacao_novo', 'Criar Prorrogação de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('41', '85', '2', 'enfermagem_paciente_prorrogacao_editar', 'Editar Prorrogação de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('42', '85', '3', 'enfermagem_paciente_prorrogacao_usar_para_nova', 'Usar para Nova Prorrogação de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('43', '86', '1', 'enfermagem_paciente_alta_novo', 'Criar Alta de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('44', '86', '2', 'enfermagem_paciente_alta_editar', 'Editar Alta de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('45', '86', '3', 'enfermagem_paciente_alta_usar_para_nova', 'Usar para Nova Alta de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('46', '87', '1', 'enfermagem_paciente_deflagrado_novo', 'Criar Relatório Deflagrado de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('47', '87', '2', 'enfermagem_paciente_deflagrado_editar', 'Editar Relatório Deflagrado de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('48', '87', '3', 'enfermagem_paciente_deflagrado_usar_para_nova', 'Usar para Nova Relatório Deflagrado de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('49', '88', '1', 'enfermagem_paciente_aditivo_novo', 'Criar Aditivo de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('50', '88', '2', 'enfermagem_paciente_aditivo_editar', 'Editar Aditivo de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('51', '88', '3', 'enfermagem_paciente_aditivo_usar_para_nova', 'Usar para Nova Aditivo de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('52', '90', '1', 'enfermagem_paciente_visita_semanal_novo', 'Criar Visita Semanal de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('53', '90', '2', 'enfermagem_paciente_visita_semanal_editar', 'Editar Visita Semanal de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('54', '90', '3', 'enfermagem_paciente_visita_semanal_usar_para_nova', 'Usar para Nova Visita Semanal de Enfermagem');
INSERT INTO `area_permissoes` VALUES ('55', '75', '1', 'medico_paciente_avaliacao_novo', 'Criar Avaliação Médica');
INSERT INTO `area_permissoes` VALUES ('56', '75', '2', 'medico_paciente_avaliacao_editar', 'Editar Avaliação Médica');
INSERT INTO `area_permissoes` VALUES ('57', '75', '3', 'medico_paciente_avaliacao_usar_para_nova', 'Usar para Nova Avaliação Médica');
INSERT INTO `area_permissoes` VALUES ('58', '76', '1', 'medico_paciente_evolucao_novo', 'Criar Evolução Médica');
INSERT INTO `area_permissoes` VALUES ('59', '76', '2', 'medico_paciente_evolucao_editar', 'Editar Evolução Médica');
INSERT INTO `area_permissoes` VALUES ('60', '76', '3', 'medico_paciente_evolucao_usar_para_nova', 'Usar para Nova Evolução Médica');
INSERT INTO `area_permissoes` VALUES ('61', '77', '1', 'medico_paciente_prorrogacao_novo', 'Criar Prorrogação Médica');
INSERT INTO `area_permissoes` VALUES ('62', '77', '2', 'medico_paciente_prorrogacao_editar', 'Editar Prorrogação Médica');
INSERT INTO `area_permissoes` VALUES ('63', '77', '3', 'medico_paciente_prorrogacao_usar_para_nova', 'Usar para Nova Prorrogação Médica');
INSERT INTO `area_permissoes` VALUES ('64', '78', '1', 'medico_paciente_intercorrencia_novo', 'Criar Intercorrência Médica');
INSERT INTO `area_permissoes` VALUES ('65', '78', '2', 'medico_paciente_intercorrencia_editar', 'Editar Intercorrência Médica');
INSERT INTO `area_permissoes` VALUES ('66', '78', '3', 'medico_paciente_intercorrencia_usar_para_nova', 'Usar para Nova Intercorrência Médica');
INSERT INTO `area_permissoes` VALUES ('67', '80', '1', 'medico_paciente_alta_novo', 'Criar Alta Médica');
INSERT INTO `area_permissoes` VALUES ('68', '80', '2', 'medico_paciente_alta_editar', 'Editar Alta Médica');
INSERT INTO `area_permissoes` VALUES ('69', '80', '3', 'medico_paciente_alta_usar_para_nova', 'Usar para Nova Alta Médica');
INSERT INTO `area_permissoes` VALUES ('70', '81', '1', 'medico_paciente_deflagrado_novo', 'Criar Deflagrado Médico');
INSERT INTO `area_permissoes` VALUES ('71', '81', '2', 'medico_paciente_deflagrado_editar', 'Editar Deflagrado Médico');
INSERT INTO `area_permissoes` VALUES ('72', '81', '3', 'medico_paciente_deflagrado_usar_para_nova', 'Usar para Nova Deflagrado Médico');
INSERT INTO `area_permissoes` VALUES ('73', '82', '1', 'medico_paciente_aditivo_novo', 'Criar Aditivo Médico');
INSERT INTO `area_permissoes` VALUES ('74', '82', '2', 'medico_paciente_aditivo_editar', 'Editar Aditivo Médico');
INSERT INTO `area_permissoes` VALUES ('75', '82', '3', 'medico_paciente_aditivo_usar_para_nova', 'Usar para Nova Aditivo Médico');
INSERT INTO `area_permissoes` VALUES ('76', '79', '1', 'medico_paciente_score_novo', 'Criar Score Paciente');
INSERT INTO `area_permissoes` VALUES ('77', '79', '2', 'medico_paciente_score_excluir', 'Excluir Score Paciente');



-- USUARIOS AREAS E PERMISSOES
-- menu mudança no  sistema
SELECT
  CONCAT("Insert into usuarios_area (usuario_id, area_id) values (", usuarios.idUsuarios,", 1);") as user_area
FROM
  usuarios
WHERE
  usuarios.block_at = '9999-12-31'
  AND
  usuarios.adm <> 1

-- menu Ocorrências
SELECT
  CONCAT("Insert into usuarios_area (usuario_id, area_id) values (", usuarios.idUsuarios,", 2);") as user_area
FROM
  usuarios
WHERE
  usuarios.block_at = '9999-12-31'
  AND
  usuarios.adm <> 2


-- menu Administrativo
SELECT
  CONCAT("Insert into usuarios_area (usuario_id, area_id) values (", usuarios.idUsuarios,", 3);") as user_area
FROM
  usuarios
WHERE
  usuarios.block_at = '9999-12-31'
  AND
  usuarios.adm <> 1
  AND
  usuarios.modadm = 1;

-- menu nutrição
SELECT
  CONCAT("Insert into usuarios_area (usuario_id, area_id) values (", usuarios.idUsuarios,", 46);") as user_area
FROM
  usuarios
WHERE
  usuarios.block_at = '9999-12-31'
  AND
  usuarios.adm <> 1
  AND
  usuarios.modnutri = 1;

-- menu auditoria
SELECT
  CONCAT("Insert into usuarios_area (usuario_id, area_id) values (", usuarios.idUsuarios,", 49);") as user_area
FROM
  usuarios
WHERE
  usuarios.block_at = '9999-12-31'
  AND
  usuarios.adm <> 1
  AND
  usuarios.modaud = 1;

-- menu remoção
SELECT
  CONCAT("Insert into usuarios_area (usuario_id, area_id) values (", usuarios.idUsuarios,", 65);") as user_area
FROM
  usuarios
WHERE
  usuarios.block_at = '9999-12-31'
  AND
  usuarios.adm <> 1
  AND
  usuarios.modrem = 1;

-- menu financeiro
SELECT
  CONCAT("Insert into usuarios_area (usuario_id, area_id) values (", usuarios.idUsuarios,", 69);") as user_area
FROM
  usuarios
WHERE
  usuarios.block_at = '9999-12-31'
  AND
  usuarios.adm <> 1
  AND
  usuarios.modfin = 1;

-- menu faturamento
SELECT
  CONCAT("Insert into usuarios_area (usuario_id, area_id) values (", usuarios.idUsuarios,", 70);") as user_area
FROM
  usuarios
WHERE
  usuarios.block_at = '9999-12-31'
  AND
  usuarios.adm <> 1
  AND
  usuarios.modfat = 1;

-- menu logística
SELECT
  CONCAT("Insert into usuarios_area (usuario_id, area_id) values (", usuarios.idUsuarios,", 71);") as user_area
FROM
  usuarios
WHERE
  usuarios.block_at = '9999-12-31'
  AND
  usuarios.adm <> 1
  AND
  usuarios.modlog = 1;

-- Usuários tipo médico
SELECT
  CONCAT("Insert into usuarios_area (usuario_id, area_id) values (", usuarios.idUsuarios,", ", tb.id,");") as user_area
FROM
  usuarios LEFT JOIN
  (
    SELECT
      area_sistema.*
    from
      area_sistema
    WHERE
      area_sistema.nome like 'medico%' or area_sistema.id = 31
  ) as tb on 1=1
WHERE
  usuarios.block_at = '9999-12-31'
  and
  usuarios.tipo = 'medico'
  AND
  usuarios.adm <> 1;

-- usuarios modmed = 1 e não são tipo medicos removendo área suspender prescricao
SELECT
  CONCAT("Insert into usuarios_area (usuario_id, area_id) values (", usuarios.idUsuarios,", ", tb.id,");") as user_area
FROM
  usuarios LEFT JOIN
  (
    SELECT
      area_sistema.*
    from
      area_sistema
    WHERE
      area_sistema.nome like 'medico%' and
      area_sistema.id not in (26) and
      area_sistema.id_pai not in (26)
  ) as tb on 1=1
WHERE
  usuarios.block_at = '9999-12-31'
  and
  usuarios.tipo <> 'medico'
  AND
  usuarios.modmed = 1
  AND
  usuarios.adm <> 1;

-- usuarios tipo enfermeiro retira kit que só adm do sistema ver.

SELECT
  CONCAT("Insert into usuarios_area (usuario_id, area_id) values (", usuarios.idUsuarios,", ", tb.id,");") as user_area
FROM
  usuarios LEFT JOIN
  (
    SELECT
      area_sistema.*
    from
      area_sistema
    WHERE
      area_sistema.nome like 'enfermagem%'
      and
      area_sistema.id not in (40)
      AND
      area_sistema.id_pai not in (40)
  ) as tb on 1=1
WHERE
  usuarios.block_at = '9999-12-31'
  and
  usuarios.tipo = 'enfermagem'
  AND
  usuarios.adm <> 1;

-- Usuários com acesso a enfermagem e não são do tipo enfermeiro removendo 40 kit, 34 solicitações e 35 solicitações avuslsas
SELECT
  CONCAT("Insert into usuarios_area (usuario_id, area_id) values (", usuarios.idUsuarios,", ", tb.id,");") as user_area
FROM
  usuarios LEFT JOIN
  (
    SELECT
      area_sistema.*
    from
      area_sistema
    WHERE
      area_sistema.nome like 'enfermagem%'
      and
      area_sistema.id not in (40, 34, 35)
      AND
      area_sistema.id_pai not in (40, 34, 35)
  ) as tb on 1=1
WHERE
  usuarios.block_at = '9999-12-31'
  and
  usuarios.tipo <> 'enfermagem'
  AND
  usuarios.modenf = 1
  AND
  usuarios.adm <> 1;

-- Usuários adm sistema.
SELECT
  CONCAT("Insert into usuarios_area (usuario_id, area_id) values (", usuarios.idUsuarios,", ", tb.id,");") as user_area
FROM
  usuarios LEFT JOIN
  (
    SELECT
      area_sistema.*
    from
      area_sistema
    WHERE
      1
  ) as tb on 1=1
WHERE
  usuarios.block_at = '9999-12-31'
  and
  usuarios.adm = 1;


-- Permissões
-- perimissões enfermeiro não da permissão para kit e desativar solicitação.
SELECT
  CONCAT("Insert into usuarios_permissao (usuario_id, permissao_id) values (", usuarios.idUsuarios,", ", tb.id,");") as user_area
FROM
  usuarios LEFT JOIN
  (
    SELECT
      area_permissoes.*
    from
      area_permissoes
    WHERE
     area_permissoes.acao like 'enfermagem%'

      AND
      area_permissoes.id not in (27,28,29,16)
  ) as tb on 1=1
WHERE
  usuarios.block_at = '9999-12-31'
  and
  usuarios.tipo = 'enfermagem'
  AND
  usuarios.adm <> 1;

-- Permissões médico.
SELECT
  CONCAT("Insert into usuarios_permissao (usuario_id, permissao_id) values (", usuarios.idUsuarios,", ", tb.id,");") as user_area
FROM
  usuarios LEFT JOIN
  (
    SELECT
      area_permissoes.*
    from
      area_permissoes
    WHERE
     area_permissoes.acao like 'medico%'

  ) as tb on 1=1
WHERE
  usuarios.block_at = '9999-12-31'
  and
  usuarios.tipo = 'medico'
  AND
  usuarios.adm <> 1;
