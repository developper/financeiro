ALTER TABLE fichamedicaevolucao ADD PESO_PACIENTE DOUBLE(6,2) NULL;
ALTER TABLE fichamedicaevolucao
  MODIFY COLUMN PESO_PACIENTE DOUBLE(6,2) AFTER TEMPERATURA;

ALTER TABLE intercorrencia_med_quadro_clinico MODIFY descricao TEXT;

ALTER TABLE intercorrencia_med_problema MODIFY descricao TEXT;
ALTER TABLE intercorrencia_med_problema MODIFY observacao TEXT;

ALTER TABLE intercorrencia_med_exame MODIFY descricao TEXT;

ALTER TABLE intercorrencia_med_procedimento MODIFY descricao TEXT;

