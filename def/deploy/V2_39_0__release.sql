CREATE TABLE area_sistema
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  nome VARCHAR(255) NOT NULL,
  descricao VARCHAR(255) NOT NULL,
  ordem INT NOT NULL, 
  id_pai INT NOT NULL
);

INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (1, 'mundanca_sistema', 'Mundanças DO Sistema', 1, 0);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (2, 'ocorrencia', 'Ocorrências', 2, 0);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (3, 'administracao', 'Administração', 3, 0);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (4, 'administracao_filial', 'Filiais', 1, 3);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (5, 'administracao_paciente', 'Pacientes', 2, 3);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (6, 'administracao_paciente_listar', 'Listar', 1, 5);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (7, 'administracao_paciente_solicitar_avaliacao', 'Solicitação de Avaliação', 2, 5);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (8, 'administracao_paciente_prescricao_curativo_enfermagem', 'Prescrições de Curativo de Enfermagem', 3, 5);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (9, 'administracao_operadora', 'Operadoras', 3, 3);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (10, 'administracao_plano', 'Planos', 4, 3);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (11, 'administracao_usuario', 'Usuários', 5, 3);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (12, 'administracao_usuario_listar', 'Listar', 1, 11);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (13, 'administracao_usuario_novo', 'Novo Usuário', 2, 11);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (14, 'administracao_importar_assinaturas', 'Importar Assinaturas', 6, 3);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (15, 'administracao_relatorio', 'Relatórios', 7, 3);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`) VALUES
  (16, 'administracao_relatorio_historico_modalidade', 'Histórico de Mudança de Modalidade Por Paciente', 1, 15);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (17, 'administracao_relatorio_epidemiologico', 'Relatório Epidemiológico', 2, 15);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (18, 'administracao_relatorio_fatura_liberada', 'Relatório de Faturas Liberadas', 3, 15);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (19, 'administracao_relatorio_faturamento_enviado', 'Relatorio de Faturamento Enviado', 4, 15);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (20, 'administracao_relatorio_assistencial', 'Relatórios Assistenciais', 8, 3);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (21, 'administracao_visualizar_autorizacao', 'Visualizar Autorizações', 9, 3);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (22, 'administracao_plano_tratamento_ferida', 'Plano Tratamento de Feridas', 10, 3);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`) VALUES (23, 'medico', 'Médicos', 4, 0);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (24, 'medico_paciente', 'Pacientes', 1, 23);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (25, 'medico_prescricao', 'Pescrições', 2, 23);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (26, 'medico_suspender_item_prescricao_ativa', 'Suspender Itens em Prescrições Ativas', 3, 23);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (27, 'medico_relatorio', 'Relatórios', 4, 23);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (28, 'medico_relatorio_assistencial', 'Relatórios Assistenciais', 1, 27);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`) VALUES (29, 'exame', 'Exames', 5, 0);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (30, 'exame_consultar', 'Consultar Exames', 1, 29);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (31, 'exame_novo', 'Novo Exame', 2, 29);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (32, 'enfermagem', 'Enfermagem', 6, 0);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (33, 'enfermagem_paciente', 'Pacientes', 1, 32);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (34, 'enfermagem_solicitacao', 'Solicitações', 2, 32);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (35, 'enfermagem_solicitacao_avulsa', 'Solicitacao Avulsa', 3, 32);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (36, 'enfermagem_buscar_solicitacao_prescricao', 'Buscar Solicitações / Prescrições', 4, 32);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (37, 'enfermagem_prescricao_enfermagem', 'Prescrições de Enfermagem', 5, 32);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (38, 'enfermagem_prescricao_curativo', 'Prescrições de Curativos', 6, 32);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (39, 'enfermagem_buscar_prescricao_medica', 'Buscar Prescrições Médicas', 7, 32);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (40, 'enfermagem_kit', 'Kits', 8, 32);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (41, 'enfermagem_brasindice', 'Brasindice', 9, 32);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (42, 'enfermagem_relatorio', 'Relatórios', 10, 32);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (43, 'enfermagem_relatorio_assistencial', 'Relatórios Assistenciais', 1, 42);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (44, 'enfermagem_relatorio_nao_conformidade', 'Relatório de Não Conformidade', 2, 42);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (45, 'enfermagem_relatorio_plano_tratamento_ferida', 'Plano Tratamento de Feridas', 3, 42);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`) VALUES (46, 'nutricao', 'Nutrição', 7, 0);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (47, 'nutricao_paciente', 'Pacientes', 1, 46);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (48, 'nutricao_prescricao', 'Prescrições de Nutrição', 2, 46);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (49, 'auditoria', 'Auditoria Interna', 8, 0);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (50, 'auditoria_auditar_solicitacao', 'Auditar Solicitações', 1, 49);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (51, 'auditoria_faturar', 'Faturar', 2, 49);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (52, 'auditoria_listar_fatura', 'Listar Faturas', 3, 49);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (53, 'auditoria_orcamento_inicial', 'Orçamento Inicial', 4, 49);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (54, 'auditoria_orcamento_prorrogacao', 'Orçamento de Prorrogação', 5, 49);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (55, 'auditoria_relatorio', 'Relatórios', 6, 49);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (56, 'auditoria_visualizar_autorizacao', 'Visualizar Autorizações', 7, 49);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (57, 'auditoria_relatorio_fatura_liberada', 'Relatório de Faturas Liberadas', 1, 55);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (58, 'auditoria_relatorio_pedido_liberado', 'Relatório de Pedidos Liberados', 2, 55);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (59, 'auditoria_relatorio_faturamento_enviado', 'Relatório de Faturamento Enviado', 3, 55);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (60, 'auditoria_relatorio_pedido', 'Relatório de Pedido', 4, 55);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (61, 'auditoria_relatorio_nao_conformidade', 'Relatório de Não Conformidades', 6, 55);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (62, 'auditar_relatorio_nao_conformidade_fazer', 'Fazer', 1, 61);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (63, 'auditar_relatorio_nao_conformidade_gerenciar', 'Gerenciar', 2, 61);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (64, 'auditar_relatorio_nao_conformidade_busca_condensada', 'Busca Condensada', 3, 61);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`) VALUES (65, 'remocao', 'Remoções', 9, 0);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (66, 'remocao_nova_interna', 'Nova Remoção Interna', 1, 65);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (67, 'remocao_nova_externa', 'Nova Remoção Externa', 2, 65);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (68, 'remocao_pesquisar', 'Pesquisar Remoções', 3, 65);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (69, 'financeiro', 'Financeiro', 10, 0);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (70, 'faturamento', 'Faturamento', 11, 0);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (71, 'logistica', 'Logística', 12, 0);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`) VALUES (72, 'sac', 'SAC', 13, 0);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (73, 'ajuda_publicacao', 'Ajuda e Publicações', 14, 0);
INSERT INTO `area_sistema` (`id`, `nome`, `descricao`, `ordem`, `id_pai`)
VALUES (74, 'alterar_senha', 'Alterar Senha', 15, 0);

CREATE TABLE area_permissoes
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  area_id INT NOT NULL,
  ordem INT NOT NULL,
  acao VARCHAR(255) NOT NULL,
  descricao VARCHAR(255) NOT NULL
);

CREATE TABLE usuarios_area
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  area_id INT NOT NULL,
  usuario_id INT NOT NULL
);

CREATE TABLE usuarios_permissao
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  permissao_id INT NOT NULL,
  usuario_id INT NOT NULL
);

-- ----------------------------
-- Table structure for `setores`
-- ----------------------------
DROP TABLE IF EXISTS `setores`;
CREATE TABLE `setores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setor` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
-- ----------------------------
-- Records of setores
-- ----------------------------
INSERT INTO `setores` VALUES ('1', 'Dir. Assistência');
INSERT INTO `setores` VALUES ('2', 'Dir. Adm.');
INSERT INTO `setores` VALUES ('3', 'RH');
INSERT INTO `setores` VALUES ('4', 'CID');
INSERT INTO `setores` VALUES ('5', 'TI');
INSERT INTO `setores` VALUES ('6', 'Auditoria');
INSERT INTO `setores` VALUES ('7', 'Financeiro');
INSERT INTO `setores` VALUES ('8', 'Logística');
INSERT INTO `setores` VALUES ('9', 'Farmácia');
INSERT INTO `setores` VALUES ('10', 'Sac');
INSERT INTO `setores` VALUES ('11', 'Serviço Social');
INSERT INTO `setores` VALUES ('12', 'Operações');
INSERT INTO `setores` VALUES ('13', 'Enfermagem');
INSERT INTO `setores` VALUES ('14', 'Médico');

UPDATE `setores` SET `setor`='Patrimônio' WHERE (`id`='12');

UPDATE `empresas` SET `ATIVO`='N' WHERE (`id`='8');
UPDATE `empresas` SET `ATIVO`='N' WHERE (`id`='3');
UPDATE `empresas` SET `ATIVO`='N' WHERE (`id`='6');
UPDATE `empresas` SET `ATIVO`='N' WHERE (`id`='9');
UPDATE `empresas` SET `ATIVO`='N' WHERE (`id`='10');

INSERT INTO `ocorrencia_classificacao` (`classificacao`) VALUES ('Queixas Gerais');

UPDATE `ocorrencia_subclassificacao` SET `descricao`='Atraso do profissional' WHERE (`id`='17');
INSERT INTO `ocorrencia_subclassificacao` (id,`descricao`, `ocorrencia_classificacao_id`) VALUES (60,'Item violado', '1');
INSERT INTO `ocorrencia_classificacao` (id,`classificacao`) VALUES (10,'Autorização');
INSERT INTO `ocorrencia_subclassificacao` (id,`descricao`, `ocorrencia_classificacao_id`) VALUES (61,'Pendência', '10');

ALTER TABLE `ocorrencia`
  DROP COLUMN `qualidade_retorno`,
  MODIFY COLUMN `descricao_qualidade_retorno`  text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL AFTER `retorno_hc`;