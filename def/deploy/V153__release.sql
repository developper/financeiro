-- SM-1067
ALTER TABLE fatura
ADD COLUMN `justificativa_reabertura` text DEFAULT NULL COMMENT 'Justificativa para reabertura da fatura.',
ADD COLUMN `reabertura_por` int(11) DEFAULT NULL COMMENT 'Usuario responsavel pela reabertura da fatura.',
ADD COLUMN `reabertura_em` datetime DEFAULT NULL COMMENT 'Data e hora da reabertura da fatura.';