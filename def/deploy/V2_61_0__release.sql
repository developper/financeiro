-- SM-2036
CREATE TABLE formulario_condensado
(
  id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  paciente_id int NOT NULL,
  ur_id int NOT NULL,
  convenio_id int NOT NULL,
  modalidade_id int NULL,
  tipo_formulario enum('p', 'd') DEFAULT 'p' COMMENT 'p = Prorrogacao / d = Deflagrado',
  inicio date NOT NULL,
  fim date NOT NULL,
  dieta int DEFAULT NULL ,
  dieta_nome varchar(255) DEFAULT NULL ,
  dieta_via int DEFAULT NULL ,
  dieta_adm text DEFAULT NULL,
  oxigenoterapia int DEFAULT NULL COMMENT 'c = Concentrador / t = Torpedo de O2',
  oxigenoterapia_vazao int DEFAULT NULL ,
  deambula int DEFAULT NULL ,
  drenos_cateteres_ostomias int DEFAULT NULL ,
  equipamentos_outros text DEFAULT NULL ,
  integridade_cutanea int DEFAULT NULL ,
  atb int DEFAULT NULL ,
  atb_inicio date DEFAULT NULL ,
  atb_previsao_termino date DEFAULT NULL ,
  antifungico int DEFAULT NULL,
  antifungico_inicio date DEFAULT NULL,
  antifungico_previsao_termino date DEFAULT NULL,
  created_by int NOT NULL,
  created_at datetime NOT NULL,
  deleted_by int DEFAULT NULL ,
  deleted_at datetime DEFAULT NULL,
  emitido int DEFAULT 0 NOT NULL
);
ALTER TABLE formulario_condensado COMMENT = 'Tabela que concentrara as informações de multipla escolha do formulario';

CREATE TABLE formulario_condensado_equipamentos
(
  id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  formulario_condensado_id int NOT NULL,
  equipamento_id          int NOT NULL
);
ALTER TABLE formulario_condensado_equipamentos COMMENT = 'Tabela auxiliar para os equipamentos do formulario condensado. Nao tem relacao com a tabela cobrancaplanos/valorescobranca';

CREATE TABLE formulario_condensado_tipo_lesao
(
  id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  formulario_condensado_id int NOT NULL,
  lesao_id int NOT NULL
);
ALTER TABLE formulario_condensado_tipo_lesao COMMENT = 'Tabela auxiliar para os tipos de lesao do formulario condensado. Nao tem relacao com as tabelas de tipos de feridas de enfermagem';

CREATE TABLE formulario_condensado_evolucao_medica
(
  id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  formulario_condensado_id int NOT NULL,
  relatorio_id int,
  evolucao text NOT NULL,
  data_visita date NOT NULL,
  profissional varchar(150) NOT NULL,
  numero_conselho varchar(50)
);
ALTER TABLE formulario_condensado_evolucao_medica COMMENT = 'Tabela auxiliar para armazenar a evolucao medica do formulario condensado';

CREATE TABLE formulario_condensado_evolucao_enfermagem
(
  id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  formulario_condensado_id int NOT NULL,
  relatorio_id int,
  evolucao text NOT NULL,
  data_visita date NOT NULL,
  profissional varchar(150) NOT NULL,
  numero_conselho varchar(50)
);
ALTER TABLE formulario_condensado_evolucao_enfermagem COMMENT = 'Tabela auxiliar para armazenar a evolucao de enfermagem do formulario condensado';

CREATE TABLE formulario_condensado_evolucao_enfermagem_lesoes
(
  id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  formulario_condensado_id int NOT NULL,
  imagem_id int NOT NULL
);
ALTER TABLE formulario_condensado_evolucao_enfermagem_lesoes COMMENT = 'Tabela complementar para as imagens de lesoes da evolucao';

CREATE TABLE formulario_condensado_evolucao_fisioterapia
(
  id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  formulario_condensado_id int NOT NULL,
  relatorio_id int,
  evolucao text NOT NULL,
  data_visita date NOT NULL,
  profissional varchar(150) NOT NULL,
  numero_conselho varchar(50)
);
ALTER TABLE formulario_condensado_evolucao_fisioterapia COMMENT = 'Tabela auxiliar para armazenar a evolucao de fisioterapia do formulario condensado';

CREATE TABLE formulario_condensado_evolucao_fonoaudiologia
(
  id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  formulario_condensado_id int NOT NULL,
  relatorio_id int,
  evolucao text NOT NULL,
  data_visita date NOT NULL,
  profissional varchar(150) NOT NULL,
  numero_conselho varchar(50)
);
ALTER TABLE formulario_condensado_evolucao_fonoaudiologia COMMENT = 'Tabela auxiliar para armazenar a evolucao de fonoaudiologia do formulario condensado';

CREATE TABLE formulario_condensado_evolucao_nutricao
(
  id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  formulario_condensado_id int NOT NULL,
  relatorio_id int,
  evolucao text NOT NULL,
  data_visita date NOT NULL,
  profissional varchar(150) NOT NULL,
  numero_conselho varchar(50)
);
ALTER TABLE formulario_condensado_evolucao_nutricao COMMENT = 'Tabela auxiliar para armazenar a evolucao de nutricao do formulario condensado';

CREATE TABLE formulario_condensado_evolucao_psicologia
(
  id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  formulario_condensado_id int NOT NULL,
  relatorio_id int,
  evolucao text NOT NULL,
  data_visita date NOT NULL,
  profissional varchar(150) NOT NULL,
  numero_conselho varchar(50)
);
ALTER TABLE formulario_condensado_evolucao_psicologia COMMENT = 'Tabela auxiliar para armazenar a evolucao de psicologia do formulario condensado';

CREATE TABLE formulario_condensado_pad
(
  id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  formulario_condensado_id int NOT NULL,
  modalidade text,
  medico_cuidados_especiais_id INT,
  medico_frequencia_id INT,
  enfermagem_cuidados_especiais_id INT,
  enfermagem_frequencia_id INT,
  tecnico_enfermagem_cuidados_especiais_id INT,
  tecnico_enfermagem_frequencia_id INT,
  fisioterapia_motora_cuidados_especiais_id INT,
  fisioterapia_motora_frequencia_id INT,
  fisioterapia_respiratoria_cuidados_especiais_id INT,
  fisioterapia_respiratoria_frequencia_id INT,
  fonoterapia_cuidados_especiais_id INT,
  fonoterapia_frequencia_id INT,
  psicologo_cuidados_especiais_id INT,
  psicologo_frequencia_id INT,
  nutricionista_cuidados_especiais_id INT,
  nutricionista_frequencia_id INT
);
ALTER TABLE formulario_condensado_pad COMMENT = 'Tabela auxiliar que reune as informações do PAD';

UPDATE `area_sistema` t SET t.`ordem` = 16 WHERE t.`id` = 74;
INSERT INTO `area_sistema` (`nome`, `descricao`, `ordem`, `id_pai`, `menu`, `link`, `caminho_icone`) VALUES ('prestador', 'Área do Prestador', 15, 91, 0, '/prestador', '/utils/new-icons/administration.png');
INSERT INTO `area_sistema` (`nome`, `descricao`, `ordem`, `id_pai`, `menu`, `link`, `caminho_icone`) VALUES ('prestador_listar', 'Listar Prorrogações e Deflagrados', 1, 91, 0, '/prestador/?action=listar', '');
INSERT INTO `area_sistema` (`nome`, `descricao`, `ordem`, `id_pai`, `menu`, `link`, `caminho_icone`) VALUES ('prestador_prorrogar', 'Prorrogar Paciente', 2, 91, 0, '/prestador/?action=prestador-form&tipo=p', '');
INSERT INTO `area_sistema` (`nome`, `descricao`, `ordem`, `id_pai`, `menu`, `link`, `caminho_icone`) VALUES ('prestador_deflagrado', 'Deflagrado Paciente', 3, 91, 0, '/prestador/?action=prestador-form&tipo=d', '');

CREATE TABLE relatorio_prestador
(
  id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  paciente_id int NOT NULL,
  empresa_id int NOT NULL,
  convenio_id int NOT NULL,
  tipo_relatorio char(1) NOT NULL,
  tipo_prestador varchar(20) NOT NULL,
  inicio date NOT NULL,
  fim date NOT NULL,
  evolucao text NOT NULL,
  data_visita date NOT NULL,
  profissional varchar(150) NOT NULL,
  conselho_regional varchar(50) NOT NULL,
  created_at datetime NOT NULL,
  created_by int NOT NULL,
  deleted_at datetime,
  deleted_by int,
  emitido int DEFAULT 0 NOT NULL
);
ALTER TABLE relatorio_prestador COMMENT = 'Tabela responsável por armazenar as prorrogacoes e deflagrados dos prestadores';

UPDATE `cuidadosespeciais` SET GRUPO = 8 WHERE id IN (12, 27);

-- SM-2043
-- Dados bancarios da conta do fornecedor
ALTER TABLE  `contas_fornecedor` 
ADD COLUMN `origem_fundos_id` int(11) NOT NULL AFTER `BANCO_FORNECEDOR_ID`;

ALTER TABLE `contas_fornecedor` 
MODIFY COLUMN `N_CONTA` varchar(11) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL AFTER `FORNECEDOR_ID`,
MODIFY COLUMN `AG` int(6) NULL DEFAULT NULL AFTER `N_CONTA`;

-- atualizando tabela conta_fornecedor para pegar o Id da tabela origem fundos.
update  contas_fornecedor set origem_fundos_id=	6	where BANCO_FORNECEDOR_ID =	1;
update  contas_fornecedor set origem_fundos_id=	7	where BANCO_FORNECEDOR_ID =	2;
update  contas_fornecedor set origem_fundos_id=	49	where BANCO_FORNECEDOR_ID =	3;
update  contas_fornecedor set origem_fundos_id=	154	where BANCO_FORNECEDOR_ID =	4;
update  contas_fornecedor set origem_fundos_id=	141	where BANCO_FORNECEDOR_ID =	5;
update  contas_fornecedor set origem_fundos_id=	169	where BANCO_FORNECEDOR_ID =	6;
update  contas_fornecedor set origem_fundos_id=	136	where BANCO_FORNECEDOR_ID =	7;
update  contas_fornecedor set origem_fundos_id=	11	where BANCO_FORNECEDOR_ID =	8;

-- removendo coluna 
ALTER TABLE `contas_fornecedor` DROP COLUMN `BANCO_FORNECEDOR_ID`;

-- atualizar base de fornecedores.
ALTER TABLE `fornecedores`
MODIFY COLUMN `IES` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL AFTER `CIDADE_ID`,
MODIFY COLUMN `IM` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL AFTER `IES`;

ALTER TABLE
`fornecedores`
MODIFY COLUMN `ATIVO` enum('S','N') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'S' AFTER `IM`;

update fornecedores set ATIVO = 'S';
-- modificar fornecedores que foram associados para o item do IPCC COD_N5 = 1.1.02.01.111 GEAP
-- modificar
update fornecedores set IPCC = 370 where idFornecedores = 127;
update fornecedores set IPCC = 370 where idFornecedores = 602;
update fornecedores set IPCC = 370 where idFornecedores = 936;

-- modificar fornecedores que foram associados para o item do IPCC COD_N5 = 1.1.02.01.192  FUNDAÇÃO E EXTENSÃO NA AREA DE SAUDE
-- modificar
update fornecedores set IPCC = 614 where idFornecedores = 25;
update fornecedores set IPCC = 614 where idFornecedores = 108;
update fornecedores set IPCC = 614 where idFornecedores = 144;
update fornecedores set IPCC = 614 where idFornecedores = 194;
update fornecedores set IPCC = 614 where idFornecedores = 196;
update fornecedores set IPCC = 614 where idFornecedores = 234;
update fornecedores set IPCC = 614 where idFornecedores = 248;
update fornecedores set IPCC = 614 where idFornecedores = 438;
update fornecedores set IPCC = 614 where idFornecedores = 548;
update fornecedores set IPCC = 614 where idFornecedores = 785;
update fornecedores set IPCC = 614 where idFornecedores = 794;
update fornecedores set IPCC = 614 where idFornecedores = 852;
update fornecedores set IPCC = 614 where idFornecedores = 855;
update fornecedores set IPCC = 614 where idFornecedores = 856;
update fornecedores set IPCC = 614 where idFornecedores = 908;
update fornecedores set IPCC = 614 where idFornecedores = 927;
update fornecedores set IPCC = 614 where idFornecedores = 928;
update fornecedores set IPCC = 614 where idFornecedores = 930;
update fornecedores set IPCC = 614 where idFornecedores = 937;
update fornecedores set IPCC = 614 where idFornecedores = 983;
update fornecedores set IPCC = 614 where idFornecedores = 1062;
update fornecedores set IPCC = 614 where idFornecedores = 1065;
update fornecedores set IPCC = 614 where idFornecedores = 1084;
update fornecedores set IPCC = 614 where idFornecedores = 1086;
update fornecedores set IPCC = 614 where idFornecedores = 1087;
update fornecedores set IPCC = 614 where idFornecedores = 1115;
update fornecedores set IPCC = 614 where idFornecedores = 1158;
update fornecedores set IPCC = 614 where idFornecedores = 1159;
update fornecedores set IPCC = 614 where idFornecedores = 1193;
update fornecedores set IPCC = 614 where idFornecedores = 1194;
update fornecedores set IPCC = 614 where idFornecedores = 1212;
update fornecedores set IPCC = 614 where idFornecedores = 1215;
update fornecedores set IPCC = 614 where idFornecedores = 1546;
update fornecedores set IPCC = 614 where idFornecedores = 1558;
update fornecedores set IPCC = 614 where idFornecedores = 1559;
update fornecedores set IPCC = 614 where idFornecedores = 1560;
update fornecedores set IPCC = 614 where idFornecedores = 1570;
update fornecedores set IPCC = 614 where idFornecedores = 1601;
update fornecedores set IPCC = 614 where idFornecedores = 1926;

-- remover contas duplicadas e deixar apenas uma de cada
delete
from
plano_contas_contabil
where
COD_N5 in ('1.1.02.01.192', '1.1.02.01.111')
and ID not in(614, 370);
