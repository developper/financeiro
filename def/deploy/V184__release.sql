CREATE TABLE procedimentos_gtm
(
  id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  prescricao_id INT(11) NOT NULL,
  catalogo_id INT(11) NOT NULL,
  data_troca DATE NOT NULL,
  tipo INT(11) NOT NULL,
  furo VARCHAR(50),
  cm VARCHAR(50),
  fr VARCHAR(50),
  marca VARCHAR(120) NOT NULL,
  dano TEXT NOT NULL,
  origem_dano INT(11) NOT NULL,
  descricao_origem_dano TEXT NOT NULL
);
ALTER TABLE procedimentos_gtm COMMENT = 'Tabela referente as trocas de gastrostomia efetuadas nas prescricoes aditivas medicas';