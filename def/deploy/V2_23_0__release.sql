-- SM-1557

CREATE TABLE autorizacao
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  paciente INT NOT NULL,
  usuario INT NOT NULL,
  origem INT NOT NULL,
  tipo ENUM('orcamento', 'relatorio', 'prescricaoenf') NOT NULL,
  carater ENUM('avaliacao', 'prorrogacao', 'aditivo') NOT NULL,
  status_geral TINYINT(1) NOT NULL COMMENT '1 = Autorizado / 2 = Autorizado Parcial / 3 = Negado',
  created_at DATETIME NOT NULL,
  created_by INT
);

CREATE TABLE autorizacao_itens
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  autorizacao INT NOT NULL,
  origem_item_id INT NOT NULL,
  catalogo_id INT NOT NULL,
  tipo INT NOT NULL,
  tabela_origem VARCHAR(20) NOT NULL,
  qtd_solicitada INT NOT NULL,
  qtd_autorizada INT DEFAULT 0 NOT NULL,
  status_item TINYINT(1) NOT NULL COMMENT '1 = Autorizado / 2 = Autorizado Parcial / 3 = Negado',
  data_autorizacao DATE
);

CREATE TABLE autorizacao_arquivos
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  autorizacao INT NOT NULL,
  files_uploaded_id INT NOT NULL
);
