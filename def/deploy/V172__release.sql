-- SM-1154---

ALTER TABLE `empresas`
ADD COLUMN `imposto_iss`  float NULL AFTER `logo`;

CREATE TABLE `historico_empresa_imposto_iss` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) DEFAULT NULL,
  `imposto_iss` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `canceled_at` datetime DEFAULT NULL,
  `canceled_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
)

ALTER TABLE `fatura`
ADD COLUMN `imposto_iss`  float NULL AFTER `data_envio_plano_at`,
ADD COLUMN `imposto_iss_inserted_by`  int(11) NULL AFTER `imposto_iss`,
ADD COLUMN `imposto_iss_inserted_at`  datetime NULL AFTER `imposto_iss_inserted_by`;

-- colocando faturas finalizadas com imposto ISS igual a 2 caso n�o seja do plano UNIMED SUDOESTE
UPDATE
fatura
SET
imposto_iss = 2,
imposto_iss_inserted_at = NOW(),
imposto_iss_inserted_by = 60
WHERE
fatura.PLANO_ID <> 35 and
enviado_margem_contribuicao_por is not null

-- colocando faturas finalizadas com imposto ISS igual a 3 caso  seja do plano UNIMED SUDOESTE
UPDATE
fatura
SET
imposto_iss = 3,
imposto_iss_inserted_at = NOW(),
imposto_iss_inserted_by = 60
WHERE
fatura.PLANO_ID = 35 and
enviado_margem_contribuicao_por is not null

-- Fim SM-1154---

-- SM-1168
ALTER TABLE `relatorio_nao_conformidade`
ADD COLUMN `finalizado_at`  datetime NULL AFTER `data`,
ADD COLUMN `finalizado_by`  int(11) NULL AFTER `finalizado_at`;
-- fim SM-1168