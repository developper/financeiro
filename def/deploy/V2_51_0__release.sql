-- SM-1896
CREATE TABLE `autorizacao_comentarios`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autorizacao_id` int(11) NULL DEFAULT NULL,
  `fatura_orcamento_id` int(11) NULL DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  `data_hora` datetime NOT NULL,
  `comentario` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `autorizacao_comentarios`
  ADD COLUMN `deleted_at` DATETIME NULL AFTER `comentario`,
  ADD COLUMN `deleted_by` int(11) NULL AFTER `deleted_at`;

-- SM-1908
CREATE TABLE remocoes_historico_acoes  (
  `id` int(0) NOT NULL,
  `remocao_id` int(0) NULL,
  `usuario_id` int(0) NULL,
  `acao` enum('editar','reabrir','finalizar','desativar') NULL,
  `data` datetime NULL,
  PRIMARY KEY (`id`)
);
ALTER TABLE `remocoes_historico_acoes`
MODIFY COLUMN `id` int(11) NOT NULL AUTO_INCREMENT FIRST;

ALTER TABLE `remocoes`
ADD COLUMN `edited_by` int(11) NULL AFTER `justificativa_nao_cobrar`,
ADD COLUMN `edited_at` datetime NULL AFTER `edited_by`;