-- SM-1573

CREATE TABLE planserv_etiologia_ferida
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(150)
);

INSERT INTO planserv_etiologia_ferida VALUES
  (NULL, 'Lesão por Pressão'),
  (NULL, 'Úlcera Venosa'),
  (NULL, 'Úlcera Arterial'),
  (NULL, 'Úlcera Pé Diabético'),
  (NULL, 'Trauma'),
  (NULL, 'Queimadura'),
  (NULL, 'Deiscência'),
  (NULL, 'Outras');

CREATE TABLE planserv_tipo_exsudato_ferida
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(150)
);

INSERT INTO planserv_tipo_exsudato_ferida VALUES
  (NULL, 'Sanguinolento'),
  (NULL, 'Seroso'),
  (NULL, 'Purulento');

CREATE TABLE planserv_volume_exsudato_ferida
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(150)
);

INSERT INTO planserv_volume_exsudato_ferida VALUES
  (NULL, 'Pequena'),
  (NULL, 'Moderada'),
  (NULL, 'Grande');

CREATE TABLE planserv_cor_borda_ferida
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(150)
);

INSERT INTO planserv_cor_borda_ferida VALUES
  (NULL, 'Rosa'),
  (NULL, 'Vermelha Brilhante'),
  (NULL, 'Cinza'),
  (NULL, 'Hiperpigmentada');

CREATE TABLE planserv_plano_feridas
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  paciente INT,
  inicio DATE,
  fim DATE,
  justificativa TEXT,
  informacoes_complementares TEXT,
  created_by INT,
  created_at DATETIME,
  updated_by INT,
  updated_at DATETIME,
  deleted_by INT,
  deleted_at DATETIME
);

CREATE TABLE planserv_plano_feridas_profissionais
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  plano_feridas_id INT NOT NULL,
  medico VARCHAR(150),
  cremeb VARCHAR(30),
  enfermeiro VARCHAR(150),
  coren VARCHAR(30)
);

CREATE TABLE planserv_plano_feridas_beneficiario
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  plano_feridas_id INT NOT NULL,
  modalidade INT,
  isolamento INT,
  tipo_isolamento TEXT,
  deambula INT,
  peso VARCHAR(10),
  altura VARCHAR(10)
);

CREATE TABLE planserv_plano_feridas_historico_paciente
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  plano_feridas_id INT NOT NULL,
  diagnostico_base TEXT,
  cid TEXT,
  doencas_sistemicas INT,
  outras_doencas_sistemicas TEXT,
  tempo_ferida TEXT,
  tratamento_atual INT,
  tipos_coberturas TEXT,
  tempo_uso TEXT,
  frequencia_troca TEXT,
  infeccao INT,
  microorganismo TEXT,
  incontinencia INT
);

CREATE TABLE planserv_plano_feridas_locais
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  plano_feridas_id INT NOT NULL,
  local_ferida INT,
  etiologia INT,
  lado INT,
  comprimento VARCHAR(20),
  largura VARCHAR(20),
  profundidade VARCHAR(20),
  descolamento INT,
  tecido_granulacao INT,
  tecido_esfacelo INT,
  tecido_fibrina INT,
  tecido_necrotico INT,
  exposicao INT,
  tipo_exsudato INT,
  volume_exsudato INT,
  odor INT,
  cor INT,
  edema INT,
  maceracao INT,
  bordas INT,
  instrumental INT,
  cirurgico INT
);

CREATE TABLE planserv_plano_feridas_coberturas
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  plano_feridas_id INT NOT NULL,
  feridas_id INT NOT NULL,
  tipo_cobertura ENUM('cp', 'cs'),
  catalogo_id INT,
  tamanho VARCHAR(120),
  quantidade INT,
  frequencia INT
);

-- SM-1583
 ALTER TABLE `fatura`
ADD COLUMN `remocao_externa`  enum('N','S') NULL DEFAULT NULL AFTER `protocolo_envio`;