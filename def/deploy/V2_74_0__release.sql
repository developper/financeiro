-- SM-2148
INSERT INTO `bordero_tipo` (`id`, `descricao_tipo`, `forma_pagamento`) VALUES (11, 'DOC', 'DOC');

-- SM-2147
CREATE TABLE nota_baixas  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parcela_antecipada_id` int(11) NULL COMMENT 'recebe Id da parcela da nota antecipada',
  `nota_id` int(11) NULL COMMENT 'recebe Id da nota antecipada',
  `valor` decimal(12, 2) NULL ,
  `tipo_movimentacao` enum('0','1') NULL DEFAULT NULL COMMENT '0 - Recebimento e 1- Desembolso',
  `conta_bancaria_id` varchar(255) NULL COMMENT 'Recebe id da conta bancaria ',  
  `descricao` text NULL,
  `numero_documento` varchar(255) NULL,
  `plano_conta_id` int(0) NULL,
  `aplicacao_fundo_id` int(0) NULL COMMENT 'Recebe a forma de pagamento',
  `conciliado` enum('S','N') NULL DEFAULT 'N' COMMENT 'Recebe id da conta onde o dinheiro fez a movimentação',
  `data_conciliado` date NULL,
  `created_by` int(11) NULL,
  `created_at` datetime NULL,
  `canceled_by` int(11) NULL,
  `canceled_at` datetime NULL,
  `conciliado_by` int(11) NULL,
  `conciliado_at` datetime NULL
  PRIMARY KEY (`id`)
);

ALTER TABLE `nota_baixas` 
ADD COLUMN `data_pagamento` date NULL AFTER `nota_id`;

INSERT INTO `area_sistema`(`id`,`nome`, `descricao`, `ordem`, `id_pai`, `menu`) VALUES (108,'financeiro_conta_pagar_receber_baixar', 'Baixar', 0, 96, 0);
INSERT INTO `area_permissoes`(`area_id`, `ordem`, `acao`, `descricao`) VALUES (108, 1, 'financeiro_conta_pagar_receber_baixar_criar', 'Criar Baixa');
INSERT INTO `area_permissoes`(`area_id`, `ordem`, `acao`, `descricao`) VALUES (108, 1, 'financeiro_conta_pagar_receber_baixar_cancelar', 'Cancelar Baixa');

ALTER TABLE `itens_conciliacao_bancaria` 
ADD COLUMN `NOTA_BAIXAS_ID` int(11) NULL COMMENT 'Recebe id da baixa da nota' AFTER `DATA_CONCILIADO`;

-- Criando Plano de Contas Contábil e financeiro de Antecipação de títulos.

INSERT INTO `plano_contas_contabil`(`ID`, `CODIGO_DOMINIO`, `N1`, `COD_N2`, `N2`, `COD_N3`, `N3`, `COD_N4`, `N4`, `COD_N5`, `N5`) VALUES (null, 0, 'A', '1.1', 'ATIVO CIRCULANTE', '1.1.2', 'CLIENTES', '1.1.2.02', 'CONTAS A RECEBER', '1.1.2.02.097', 'ANTECIPAÇÃO DE TÍTULO');

INSERT INTO `plano_contas`(`ID`, `N1`, `COD_N2`, `N2`, `COD_N3`, `N3`, `COD_N4`, `N4`, `COD_IPCC`, `IPCC`, `PROVISIONA`, `POSSUI_RETENCOES`, `ISS`, `ICMS`, `IPI`, `PIS`, `COFINS`, `CSLL`, `IR`, `PCC`, `INSS`, `DOMINIO_ID`) VALUES (NULL, 'E', '1.0', 'RECEITAS', '1.0.4', 'RECEITA OPERACIONAL', '1.0.4.07', ' ANTECIPAÇÃO DE TÍTULO', '1.1.2.02.097', 'ANTECIPAÇÃO DE TÍTULO', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

UPDATE `plano_contas` SET `COD_IPCC` = '1.1.3.05.001' WHERE `ID` = 92;
DELETE FROM `plano_contas_contabil` WHERE `ID` in (137, 138, 139, 140, 141, 142, 143, 144, 146, 147, 148);

UPDATE `plano_contas_contabil` SET `COD_N5` = '1.1.3.05.001' WHERE `ID` = 145;

INSERT INTO `plano_contas`(`ID`, `N1`, `COD_N2`, `N2`, `COD_N3`, `N3`, `COD_N4`, `N4`, `COD_IPCC`, `IPCC`, `PROVISIONA`, `POSSUI_RETENCOES`, `ISS`, `ICMS`, `IPI`, `PIS`, `COFINS`, `CSLL`, `IR`, `PCC`, `INSS`, `DOMINIO_ID`) VALUES (167, 'E', '1.0', 'RECEITAS', '1.0.4', 'RECEITA OPERACIONAL', '1.0.4.08', ' DEVOLUÇÃO DE PAGAMENTO ANTECIPADO', '1.1.3.05.001', 'ADIANTAMENTO A FORNECEDORES', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);