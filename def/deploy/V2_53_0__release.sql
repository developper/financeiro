-- SM-1934
CREATE TABLE ocorrencia_emails_encaminhamento
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  ocorrencia_id INT NOT NULL,
  email VARCHAR(150) NOT NULL
);

