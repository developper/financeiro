-- SM-915 amaarar os cuidados do grupo 7 com os cuidados especiais para poder sair como servi�os nos or�amntos e faturas
ALTER TABLE `prescricao_enf_cuidado`
ADD COLUMN `cuidado_especiais_id`  int(11) NULL AFTER `presc_enf_grupo_id`;


update prescricao_enf_cuidado set cuidado_especiais_id =1 where prescricao_enf_cuidado.id = 114 ;
update prescricao_enf_cuidado set cuidado_especiais_id =2 where prescricao_enf_cuidado.id = 115 ;
update prescricao_enf_cuidado set cuidado_especiais_id =4 where prescricao_enf_cuidado.id = 116 ;
update prescricao_enf_cuidado set cuidado_especiais_id =5 where prescricao_enf_cuidado.id = 117 ;
update prescricao_enf_cuidado set cuidado_especiais_id =6 where prescricao_enf_cuidado.id = 118 ;
update prescricao_enf_cuidado set cuidado_especiais_id =7 where prescricao_enf_cuidado.id = 119 ;
update prescricao_enf_cuidado set cuidado_especiais_id =8 where prescricao_enf_cuidado.id = 120 ;
update prescricao_enf_cuidado set cuidado_especiais_id =9 where prescricao_enf_cuidado.id = 121 ;
update prescricao_enf_cuidado set cuidado_especiais_id =11 where prescricao_enf_cuidado.id = 122 ;
update prescricao_enf_cuidado set cuidado_especiais_id =12 where prescricao_enf_cuidado.id = 123 ;
update prescricao_enf_cuidado set cuidado_especiais_id =13 where prescricao_enf_cuidado.id = 124 ;
update prescricao_enf_cuidado set cuidado_especiais_id =14 where prescricao_enf_cuidado.id = 125 ;
update prescricao_enf_cuidado set cuidado_especiais_id =15 where prescricao_enf_cuidado.id = 126 ;
update prescricao_enf_cuidado set cuidado_especiais_id =16 where prescricao_enf_cuidado.id = 127 ;
update prescricao_enf_cuidado set cuidado_especiais_id =17 where prescricao_enf_cuidado.id = 128 ;
update prescricao_enf_cuidado set cuidado_especiais_id =18 where prescricao_enf_cuidado.id = 129 ;
update prescricao_enf_cuidado set cuidado_especiais_id =19 where prescricao_enf_cuidado.id = 130 ;
update prescricao_enf_cuidado set cuidado_especiais_id =20 where prescricao_enf_cuidado.id = 131 ;
update prescricao_enf_cuidado set cuidado_especiais_id =21 where prescricao_enf_cuidado.id = 132 ;
update prescricao_enf_cuidado set cuidado_especiais_id =22 where prescricao_enf_cuidado.id = 133 ;
update prescricao_enf_cuidado set cuidado_especiais_id =23 where prescricao_enf_cuidado.id = 134 ;
update prescricao_enf_cuidado set cuidado_especiais_id =24 where prescricao_enf_cuidado.id = 135 ;
update prescricao_enf_cuidado set cuidado_especiais_id =25 where prescricao_enf_cuidado.id = 136 ;
update prescricao_enf_cuidado set cuidado_especiais_id =26 where prescricao_enf_cuidado.id = 137 ;
update prescricao_enf_cuidado set cuidado_especiais_id =27 where prescricao_enf_cuidado.id = 138 ;
update prescricao_enf_cuidado set cuidado_especiais_id =28 where prescricao_enf_cuidado.id = 139 ;
update prescricao_enf_cuidado set cuidado_especiais_id =29 where prescricao_enf_cuidado.id = 140 ;
update prescricao_enf_cuidado set cuidado_especiais_id =30 where prescricao_enf_cuidado.id = 141 ;
update prescricao_enf_cuidado set cuidado_especiais_id =31 where prescricao_enf_cuidado.id = 142 ;
update prescricao_enf_cuidado set cuidado_especiais_id =32 where prescricao_enf_cuidado.id = 143 ;
update prescricao_enf_cuidado set cuidado_especiais_id =33 where prescricao_enf_cuidado.id = 144 ;
update prescricao_enf_cuidado set cuidado_especiais_id =34 where prescricao_enf_cuidado.id = 145 ;
update prescricao_enf_cuidado set cuidado_especiais_id =35 where prescricao_enf_cuidado.id = 146 ;
update prescricao_enf_cuidado set cuidado_especiais_id =36 where prescricao_enf_cuidado.id = 147 ;
update prescricao_enf_cuidado set cuidado_especiais_id =37 where prescricao_enf_cuidado.id = 148 ;
update prescricao_enf_cuidado set cuidado_especiais_id =38 where prescricao_enf_cuidado.id = 149 ;
update prescricao_enf_cuidado set cuidado_especiais_id =39 where prescricao_enf_cuidado.id = 150 ;
update prescricao_enf_cuidado set cuidado_especiais_id =40 where prescricao_enf_cuidado.id = 151 ;
update prescricao_enf_cuidado set cuidado_especiais_id =41 where prescricao_enf_cuidado.id = 152 ;


ALTER TABLE `prescricao_curativo`
ADD COLUMN `cancelado`  tinyint(1) NOT NULL DEFAULT 0 AFTER `enviado`,
ADD COLUMN `cancelado_em`  datetime NOT NULL AFTER `cancelado`,
ADD COLUMN `cancelado_por`  int NOT NULL DEFAULT 0 AFTER `cancelado_em`;


-- SM-928
ALTER TABLE `evolucaoenfermagem`
ADD COLUMN `constipacao_gastro`  int(1) NULL AFTER `CATETER_PICC_LOCAL`,
ADD COLUMN `vigil`  int(1) NULL AFTER `constipacao_gastro`;

ALTER TABLE `prescricao_curativo_ferida_item`
ADD COLUMN `frequencia`  int(11) NOT NULL AFTER `qtd`;
