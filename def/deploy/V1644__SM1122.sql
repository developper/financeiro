-- Para itens com mesmo catalogo_id do mesmo paciente
--- roda a primira consulta para pegar identificar quais os itens que devem permanecer dos repetidos

SELECT
custo_item_fatura.id
FROM
custo_item_fatura
INNER JOIN
(SELECT
custo_item_fatura.catalogo_id as cat ,
custo_item_fatura.paciente_id as pac,
custo_item_fatura.tipo
FROM
custo_item_fatura
WHERE
custo_item_fatura.catalogo_id IS NOT NULL
GROUP BY
custo_item_fatura.catalogo_id,
custo_item_fatura.paciente_id,
custo_item_fatura.tipo
HAVING
COUNT(custo_item_fatura.catalogo_id) >1
ORDER BY
custo_item_fatura.paciente_id) as table1 on (custo_item_fatura.catalogo_id =table1.cat and custo_item_fatura.paciente_id = table1.pac and custo_item_fatura.tipo = table1.tipo)
where
catalogo_id IS NOT NULL
GROUP BY
custo_item_fatura.catalogo_id,
custo_item_fatura.paciente_id,
custo_item_fatura.tipo
ORDER BY
paciente_id,
catalogo_id,
data DESC,
custo DESC

-- Os ids v�o ser utilizados no not in dessa fun��o
SELECT
custo_item_fatura.id
FROM
custo_item_fatura
INNER JOIN
(SELECT
custo_item_fatura.catalogo_id as cat ,
custo_item_fatura.paciente_id as pac,
custo_item_fatura.tipo
FROM
custo_item_fatura
WHERE
custo_item_fatura.catalogo_id IS NOT NULL
GROUP BY
custo_item_fatura.catalogo_id,
custo_item_fatura.paciente_id,
custo_item_fatura.tipo
HAVING
COUNT(custo_item_fatura.catalogo_id) >1
ORDER BY
custo_item_fatura.paciente_id) as table1 on (custo_item_fatura.catalogo_id =table1.cat and custo_item_fatura.paciente_id = table1.pac and custo_item_fatura.tipo = table1.tipo)
where
catalogo_id IS NOT NULL
GROUP BY
custo_item_fatura.catalogo_id,
custo_item_fatura.paciente_id,
custo_item_fatura.tipo
ORDER BY
paciente_id,
catalogo_id,
data DESC,
custo DESC

-- Limpando duplicados com mesmo catalogo
delete from custo_item_fatura where id = 10901;
delete from custo_item_fatura where id = 25835;
delete from custo_item_fatura where id = 25836;
delete from custo_item_fatura where id = 10904;
delete from custo_item_fatura where id = 10902;
delete from custo_item_fatura where id = 10900;
delete from custo_item_fatura where id = 10905;
delete from custo_item_fatura where id = 10903;
delete from custo_item_fatura where id = 24941;
delete from custo_item_fatura where id = 24943;
delete from custo_item_fatura where id = 24942;
delete from custo_item_fatura where id = 15884;
delete from custo_item_fatura where id = 25146;
delete from custo_item_fatura where id = 25142;
delete from custo_item_fatura where id = 25143;
delete from custo_item_fatura where id = 25145;
delete from custo_item_fatura where id = 25144;
delete from custo_item_fatura where id = 2065;
delete from custo_item_fatura where id = 17297;
delete from custo_item_fatura where id = 11304;
delete from custo_item_fatura where id = 24558;
delete from custo_item_fatura where id = 24556;
delete from custo_item_fatura where id = 24557;
delete from custo_item_fatura where id = 5311;
delete from custo_item_fatura where id = 5309;
delete from custo_item_fatura where id = 5307;
delete from custo_item_fatura where id = 855;
delete from custo_item_fatura where id = 25531;
delete from custo_item_fatura where id = 25535;
delete from custo_item_fatura where id = 25566;
delete from custo_item_fatura where id = 25529;
delete from custo_item_fatura where id = 25534;
delete from custo_item_fatura where id = 25530;
delete from custo_item_fatura where id = 25533;
delete from custo_item_fatura where id = 25567;
delete from custo_item_fatura where id = 25532;
delete from custo_item_fatura where id = 17803;
delete from custo_item_fatura where id = 4635;
delete from custo_item_fatura where id = 4159;
delete from custo_item_fatura where id = 4151;
delete from custo_item_fatura where id = 4149;
delete from custo_item_fatura where id = 4161;
delete from custo_item_fatura where id = 4152;
delete from custo_item_fatura where id = 4153;
delete from custo_item_fatura where id = 4154;
delete from custo_item_fatura where id = 4155;
delete from custo_item_fatura where id = 4162;
delete from custo_item_fatura where id = 4157;
delete from custo_item_fatura where id = 4150;
delete from custo_item_fatura where id = 4160;
delete from custo_item_fatura where id = 4158;
delete from custo_item_fatura where id = 4156;
delete from custo_item_fatura where id = 21673;
delete from custo_item_fatura where id = 9860;
delete from custo_item_fatura where id = 10335;
delete from custo_item_fatura where id = 2836;
delete from custo_item_fatura where id = 3675;
delete from custo_item_fatura where id = 16027;
delete from custo_item_fatura where id = 16031;
delete from custo_item_fatura where id = 16029;
delete from custo_item_fatura where id = 16033;
delete from custo_item_fatura where id = 16035;
delete from custo_item_fatura where id = 16037;
delete from custo_item_fatura where id = 17176;
delete from custo_item_fatura where id = 17174;
delete from custo_item_fatura where id = 17175;
delete from custo_item_fatura where id = 24479;
delete from custo_item_fatura where id = 24480;
delete from custo_item_fatura where id = 21554;
delete from custo_item_fatura where id = 21555;
delete from custo_item_fatura where id = 21556;
delete from custo_item_fatura where id = 9232;
delete from custo_item_fatura where id = 9231;
delete from custo_item_fatura where id = 9244;
delete from custo_item_fatura where id = 9236;
delete from custo_item_fatura where id = 9238;
delete from custo_item_fatura where id = 9242;
delete from custo_item_fatura where id = 9228;
delete from custo_item_fatura where id = 9233;
delete from custo_item_fatura where id = 9241;
delete from custo_item_fatura where id = 9223;
delete from custo_item_fatura where id = 9243;
delete from custo_item_fatura where id = 9224;
delete from custo_item_fatura where id = 9229;
delete from custo_item_fatura where id = 9230;
delete from custo_item_fatura where id = 9235;
delete from custo_item_fatura where id = 9240;
delete from custo_item_fatura where id = 9226;
delete from custo_item_fatura where id = 9227;
delete from custo_item_fatura where id = 9234;
delete from custo_item_fatura where id = 9239;
delete from custo_item_fatura where id = 9237;
delete from custo_item_fatura where id = 9225;
delete from custo_item_fatura where id = 9654;
delete from custo_item_fatura where id = 9671;
delete from custo_item_fatura where id = 17113;
delete from custo_item_fatura where id = 22961;
delete from custo_item_fatura where id = 22962;
delete from custo_item_fatura where id = 22953;
delete from custo_item_fatura where id = 22950;
delete from custo_item_fatura where id = 22960;
delete from custo_item_fatura where id = 22945;
delete from custo_item_fatura where id = 22952;
delete from custo_item_fatura where id = 22946;
delete from custo_item_fatura where id = 22947;
delete from custo_item_fatura where id = 22948;
delete from custo_item_fatura where id = 22951;
delete from custo_item_fatura where id = 22958;
delete from custo_item_fatura where id = 22956;
delete from custo_item_fatura where id = 22949;
delete from custo_item_fatura where id = 22957;
delete from custo_item_fatura where id = 22954;
delete from custo_item_fatura where id = 22955;
delete from custo_item_fatura where id = 22959;
delete from custo_item_fatura where id = 24082;
delete from custo_item_fatura where id = 24085;
delete from custo_item_fatura where id = 24083;
delete from custo_item_fatura where id = 24088;
delete from custo_item_fatura where id = 24097;
delete from custo_item_fatura where id = 24076;
delete from custo_item_fatura where id = 24095;
delete from custo_item_fatura where id = 24075;
delete from custo_item_fatura where id = 24087;
delete from custo_item_fatura where id = 24084;
delete from custo_item_fatura where id = 24079;
delete from custo_item_fatura where id = 24081;
delete from custo_item_fatura where id = 24092;
delete from custo_item_fatura where id = 24086;
delete from custo_item_fatura where id = 24080;
delete from custo_item_fatura where id = 24078;
delete from custo_item_fatura where id = 24089;
delete from custo_item_fatura where id = 24093;
delete from custo_item_fatura where id = 24077;
delete from custo_item_fatura where id = 24094;
delete from custo_item_fatura where id = 24091;
delete from custo_item_fatura where id = 24074;
delete from custo_item_fatura where id = 24090;
delete from custo_item_fatura where id = 24096;
delete from custo_item_fatura where id = 25720;
delete from custo_item_fatura where id = 21801;
delete from custo_item_fatura where id = 24366;
delete from custo_item_fatura where id = 24385;
delete from custo_item_fatura where id = 24667;
delete from custo_item_fatura where id = 24668;
delete from custo_item_fatura where id = 24659;
delete from custo_item_fatura where id = 25061;
delete from custo_item_fatura where id = 25062;
delete from custo_item_fatura where id = 25057;
delete from custo_item_fatura where id = 25060;
delete from custo_item_fatura where id = 25059;
delete from custo_item_fatura where id = 25058;

-- Rollback

Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (10901,15,88,0,222, '2016-01-18 16:06:59',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25835,15,88,0,222, '2016-01-18 16:06:59',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25836,15,88,0,222, '2016-01-18 16:06:59',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (10904,141,88,1,222, '2015-10-05 15:38:04',2.99);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (10902,5111,88,1,222, '2016-01-18 16:06:59',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (10900,24003,88,0,222, '2016-01-18 16:06:59',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (10905,24756,88,1,222, '2016-02-17 12:15:03',37.32);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (10903,26736,88,1,222, '2016-01-18 16:06:59',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24941,142,96,1,409, '2016-06-14 12:03:34',2.60);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24943,154,96,1,409, '2016-06-14 12:03:34',0.07);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24942,11808,96,1,409, '2016-06-14 12:03:34',0.13);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (15884,26561,142,1,60, '2016-07-05 12:25:23',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25146,124,151,1,409, '2016-06-20 11:11:01',39.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25142,1767,151,0,409, '2016-06-20 11:11:01',0.64);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25143,5574,151,0,409, '2016-06-20 11:11:01',3.75);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25145,24149,151,1,409, '2016-06-20 11:11:01',22.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25144,26491,151,1,409, '2016-06-20 11:11:01',292.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (2065,26564,209,1,263, '2015-07-13 14:48:04',0.55);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (17297,27175,233,1,409, '2016-07-01 11:29:27',0.95);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (11304,112,287,1,222, '2015-10-08 14:49:17',5.50);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24558,124,469,1,409, '2016-06-07 11:57:04',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24556,12989,469,1,409, '2016-06-07 11:57:04',8.50);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24557,24847,469,1,409, '2016-06-07 11:57:04',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (5311,24074,516,1,222, '2016-06-13 09:12:46',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (5309,24075,516,1,222, '2016-06-13 09:12:46',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (5307,24076,516,1,222, '2016-06-13 09:12:46',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (855,6144,661,1,222, '2015-07-09 10:05:12',0.51);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25531,155,711,1,409, '2016-06-29 10:26:13',0.45);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25535,2996,711,1,409, '2016-06-29 10:45:39',2.85);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25566,15341,711,0,409, '2016-06-29 10:45:39',8.80);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25529,22511,711,0,409, '2016-06-29 10:45:39',3.90);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25534,24110,711,1,409, '2016-06-29 10:45:39',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25530,24483,711,1,409, '2016-06-29 10:45:39',0.16);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25533,24854,711,1,409, '2016-06-29 10:45:39',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25567,26705,711,1,409, '2016-06-29 10:45:39',16.50);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25532,27272,711,1,409, '2016-06-29 10:45:39',14.27);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (17803,142,777,1,409, '2016-03-21 13:06:11',2.20);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (4635,26870,845,0,10, '2015-08-06 10:45:36',1.26);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (4159,144,1032,1,10, '2015-08-05 08:55:36',0.88);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (4151,600,1032,0,10, '2015-08-05 08:55:36',3.30);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (4149,766,1032,0,10, '2015-08-05 08:55:36',10.18);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (4161,4291,1032,1,10, '2015-08-05 08:55:37',0.70);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (4152,5298,1032,0,10, '2015-08-05 08:55:36',1.20);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (4153,9220,1032,0,10, '2015-08-05 08:55:36',11.73);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (4154,9460,1032,0,10, '2015-08-05 08:55:36',0.23);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (4155,13272,1032,0,10, '2015-08-05 08:55:36',3.87);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (4162,13622,1032,3,10, '2015-08-05 08:55:37',25.96);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (4157,20453,1032,0,10, '2015-08-05 08:55:36',3.67);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (4150,20515,1032,0,10, '2015-08-05 08:55:36',1.94);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (4160,24857,1032,1,10, '2015-08-05 08:55:36',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (4158,26540,1032,1,10, '2015-08-05 08:55:36',6.31);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (4156,26735,1032,0,10, '2015-11-04 16:25:52',2.39);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (21673,75,1062,1,409, '2016-05-13 14:24:19',0.09);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9860,142,1098,1,222, '2015-09-28 16:28:18',2.99);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (10335,24483,1110,1,222, '2015-09-30 17:16:52',0.15);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (2836,26564,1114,1,263, '2015-11-23 16:08:35',0.55);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (3675,112,1134,1,263, '2015-07-27 16:50:30',3.97);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (16027,57,1142,1,222, '2016-01-18 16:41:43',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (16031,6133,1142,1,222, '2016-01-29 16:26:39',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (16029,6144,1142,1,222, '2016-01-29 16:26:39',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (16033,10339,1142,1,222, '2016-01-18 16:41:43',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (16035,26625,1142,1,222, '2016-01-29 16:26:39',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (16037,26766,1142,1,222, '2016-01-29 16:26:39',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (17176,4818,1145,0,409, '2016-05-18 12:12:22',5.20);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (17174,9435,1145,0,409, '2016-06-30 11:15:18',6.36);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (17175,26730,1145,0,409, '2016-03-08 12:31:45',9.91);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24479,150,1158,1,409, '2016-06-07 09:33:36',35.25);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24480,9551,1158,1,409, '2016-06-07 09:33:36',3.40);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (21554,6143,1184,1,409, '2016-05-13 14:50:01',0.45);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (21555,26621,1184,1,409, '2016-05-13 14:50:01',0.55);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (21556,27175,1184,1,409, '2016-05-13 14:50:01',0.09);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9232,112,1200,1,222, '2015-09-15 10:23:44',5.50);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9231,142,1200,1,222, '2015-09-15 10:23:44',3.99);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9244,154,1200,1,222, '2015-09-15 10:23:44',0.08);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9236,737,1200,1,222, '2015-09-15 10:23:44',0.83);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9238,3738,1200,1,222, '2015-09-15 10:23:44',16.90);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9242,4254,1200,1,222, '2015-09-15 10:23:44',0.34);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9228,5111,1200,1,222, '2015-09-15 10:23:44',1.91);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9233,5414,1200,1,222, '2015-09-15 10:23:44',6.31);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9241,9443,1200,1,222, '2015-09-15 10:23:44',0.44);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9223,17058,1200,0,222, '2015-09-15 10:23:44',6.81);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9243,23359,1200,1,222, '2015-09-15 10:23:44',0.23);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9224,23403,1200,0,222, '2015-09-15 10:23:44',6.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9229,23438,1200,1,222, '2015-09-15 10:23:44',5.13);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9230,23439,1200,1,222, '2015-09-15 10:23:44',5.13);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9235,24100,1200,1,222, '2015-09-15 10:23:44',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9240,24107,1200,1,222, '2015-09-15 10:23:44',0.12);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9226,26562,1200,1,222, '2015-09-15 10:23:44',0.07);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9227,26567,1200,1,222, '2015-09-15 10:23:44',2.10);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9234,26573,1200,1,222, '2015-09-15 10:23:44',3.63);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9239,26704,1200,1,222, '2015-09-15 10:23:44',6.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9237,26705,1200,1,222, '2015-09-15 10:23:44',0.36);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9225,26735,1200,0,222, '2015-09-15 10:23:44',2.39);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9654,26562,1212,1,222, '2015-09-25 14:52:05',0.07);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (9671,26705,1212,1,222, '2015-09-25 14:52:05',0.34);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (17113,26735,1225,0,409, '2016-06-29 09:50:20',2.93);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22961,100,1236,1,409, '2016-05-20 11:34:47',10.61);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22962,101,1236,1,409, '2016-05-20 11:34:47',10.61);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22953,112,1236,1,409, '2016-05-20 11:34:47',4.17);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22950,5272,1236,0,409, '2016-05-20 11:34:47',5.20);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22960,8650,1236,1,409, '2016-05-20 11:34:47',0.95);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22945,9514,1236,0,409, '2016-05-20 11:34:47',13.48);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22952,10338,1236,1,409, '2016-05-20 11:34:47',1.02);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22946,13044,1236,0,409, '2016-05-20 11:34:47',4.33);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22947,17058,1236,0,409, '2016-05-20 11:34:47',8.51);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22948,17061,1236,0,409, '2016-05-20 11:34:47',2.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22951,23440,1236,1,409, '2016-05-20 11:34:47',1.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22958,26564,1236,1,409, '2016-05-20 11:34:47',0.38);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22956,26625,1236,1,409, '2016-05-20 11:34:47',13.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22949,26626,1236,0,409, '2016-05-20 11:34:47',0.19);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22957,26777,1236,1,409, '2016-05-20 11:34:47',1.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22954,27130,1236,1,409, '2016-05-20 11:34:47',0.79);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22955,27133,1236,1,409, '2016-05-20 11:34:47',14.23);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (22959,27147,1236,1,409, '2016-05-20 11:34:47',0.33);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24082,39,1315,1,409, '2016-06-10 10:18:18',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24085,43,1315,1,409, '2016-06-10 10:18:18',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24083,1801,1315,1,409, '2016-06-10 10:18:18',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24088,2139,1315,1,409, '2016-06-10 10:18:18',0.06);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24097,5272,1315,1,409, '2016-06-10 10:18:18',8.50);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24076,6144,1315,1,409, '2016-06-10 10:18:18',0.45);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24095,21812,1315,1,409, '2016-06-10 10:18:18',2.13);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24075,23345,1315,1,409, '2016-06-10 10:18:18',0.45);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24087,24099,1315,1,409, '2016-06-10 10:18:18',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24084,24100,1315,1,409, '2016-06-10 10:18:18',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24079,24102,1315,1,409, '2016-06-10 10:18:18',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24081,24120,1315,1,409, '2016-06-10 10:18:18',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24092,24290,1315,1,409, '2016-06-10 10:18:18',6.49);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24086,24807,1315,1,409, '2016-06-10 10:18:18',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24080,24854,1315,1,409, '2016-06-10 10:18:18',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24078,24855,1315,1,409, '2016-06-10 10:18:18',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24089,24856,1315,1,409, '2016-06-10 10:18:18',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24093,24857,1315,1,409, '2016-06-10 10:18:18',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24077,24860,1315,1,409, '2016-06-10 10:18:18',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24094,24861,1315,1,409, '2016-06-10 10:18:18',0.00);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24091,26705,1315,1,409, '2016-06-10 10:18:18',16.30);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24074,26735,1315,0,409, '2016-06-10 10:18:18',2.93);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24090,26766,1315,1,409, '2016-06-10 10:18:18',1.35);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24096,27129,1315,1,409, '2016-06-10 10:18:18',1.29);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25720,6143,1330,1,409, '2016-07-01 09:15:47',0.45);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (21801,26766,1358,1,10, '2016-05-11 08:26:11',1.79);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24366,26562,1379,1,409, '2016-06-06 13:27:39',0.07);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24385,26735,1379,1,409, '2016-06-06 13:27:39',2.92);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24667,112,1382,1,409, '2016-06-09 13:10:38',5.50);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24668,3871,1382,1,409, '2016-06-09 13:10:38',2.79);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (24659,5257,1382,0,409, '2016-06-09 13:10:38',87.93);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25061,154,1387,1,409, '2016-06-16 15:25:56',0.07);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25062,13622,1387,3,409, '2016-06-16 15:25:56',21.90);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25057,26540,1387,1,409, '2016-06-16 15:25:56',7.25);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25060,26704,1387,1,409, '2016-06-16 15:25:56',0.12);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25059,26705,1387,1,409, '2016-06-16 15:25:56',0.16);
Insert into custo_item_fatura (id, catalogo_id, paciente_id,tipo,usuario_id,data,custo) values (25058,27331,1387,1,409, '2016-06-16 15:25:56',0.85);

--- Para itens de mesmo cobrancaplano_id
--- roda a primira consulta para pegar identificar quais os itens que devem permanecer dos repetidos
SELECT
custo_item_fatura.id
FROM
custo_item_fatura
INNER JOIN
(SELECT
custo_item_fatura.cobrancaplano_id as cat ,
custo_item_fatura.paciente_id as pac,
custo_item_fatura.tipo
FROM
custo_item_fatura
WHERE
custo_item_fatura.cobrancaplano_id IS NOT NULL
GROUP BY
custo_item_fatura.cobrancaplano_id,
custo_item_fatura.paciente_id,
custo_item_fatura.tipo
HAVING
COUNT(custo_item_fatura.cobrancaplano_id) >1
ORDER BY
custo_item_fatura.paciente_id) as table1 on (custo_item_fatura.cobrancaplano_id =table1.cat and custo_item_fatura.paciente_id = table1.pac and custo_item_fatura.tipo = table1.tipo)
where
cobrancaplano_id IS NOT NULL
GROUP BY
custo_item_fatura.cobrancaplano_id,
custo_item_fatura.paciente_id,
custo_item_fatura.tipo
ORDER BY
paciente_id,
cobrancaplano_id,
data DESC,
custo DESC
-- Os ids v�o ser utilizados no not in dessa fun��o
SELECT
CONCAT('delete from custo_item_fatura where id = ', custo_item_fatura.id, ';'),
CONCAT("Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (",COALESCE(custo_item_fatura.id,''),",", COALESCE(custo_item_fatura.cobrancaplano_id,''),",", COALESCE(custo_item_fatura.paciente_id,''),",",COALESCE(custo_item_fatura.tipo,''),",",COALESCE(custo_item_fatura.usuario_id,''),", '",COALESCE(custo_item_fatura.data,''),"',",COALESCE(custo_item_fatura.custo,''),");"),
custo_item_fatura.id,
custo_item_fatura.catalogo_id,
custo_item_fatura.cobrancaplano_id,
custo_item_fatura.paciente_id,
custo_item_fatura.tipo,
custo_item_fatura.usuario_id,
custo_item_fatura.`data`,
custo_item_fatura.custo,
custo_item_fatura.valorescobranca_id
FROM
custo_item_fatura
INNER JOIN
(SELECT
custo_item_fatura.cobrancaplano_id as cat ,
custo_item_fatura.paciente_id as pac,
custo_item_fatura.tipo as tip
FROM
custo_item_fatura
WHERE
custo_item_fatura.cobrancaplano_id IS NOT NULL
GROUP BY
custo_item_fatura.cobrancaplano_id,
custo_item_fatura.paciente_id,
custo_item_fatura.tipo
HAVING
COUNT(custo_item_fatura.cobrancaplano_id) >1
ORDER BY
custo_item_fatura.paciente_id) as table1 on (custo_item_fatura.cobrancaplano_id =table1.cat and custo_item_fatura.paciente_id = table1.pac and custo_item_fatura.tipo = table1.tip)
where
cobrancaplano_id IS NOT NULL and
id not in (11276,
17177,
24935,
2,
9834,
7755,
918,
7255,
7266,
7218,
8365,
16039,
7727,
24533,
7077,
9642,
8973,
9429,
17169,
5753,
5755,
5759,
9193,
9194,
9195,
9196,
20587,
20584,
22915,
11904,
12632,
25165,
14585,
20649,
18147,
18486,
18488
)
ORDER BY
paciente_id,
cobrancaplano_id




-- apagando itens duplicados
delete from custo_item_fatura where id = 11277;
delete from custo_item_fatura where id = 17178;
delete from custo_item_fatura where id = 24940;
delete from custo_item_fatura where id = 3;
delete from custo_item_fatura where id = 9835;
delete from custo_item_fatura where id = 7756;
delete from custo_item_fatura where id = 919;
delete from custo_item_fatura where id = 7256;
delete from custo_item_fatura where id = 7267;
delete from custo_item_fatura where id = 7219;
delete from custo_item_fatura where id = 8367;
delete from custo_item_fatura where id = 16040;
delete from custo_item_fatura where id = 7728;
delete from custo_item_fatura where id = 24551;
delete from custo_item_fatura where id = 7078;
delete from custo_item_fatura where id = 9643;
delete from custo_item_fatura where id = 8974;
delete from custo_item_fatura where id = 9430;
delete from custo_item_fatura where id = 17173;
delete from custo_item_fatura where id = 5754;
delete from custo_item_fatura where id = 5756;
delete from custo_item_fatura where id = 5760;
delete from custo_item_fatura where id = 9219;
delete from custo_item_fatura where id = 9220;
delete from custo_item_fatura where id = 9221;
delete from custo_item_fatura where id = 9222;
delete from custo_item_fatura where id = 20588;
delete from custo_item_fatura where id = 20585;
delete from custo_item_fatura where id = 22943;
delete from custo_item_fatura where id = 11906;
delete from custo_item_fatura where id = 12633;
delete from custo_item_fatura where id = 25166;
delete from custo_item_fatura where id = 14586;
delete from custo_item_fatura where id = 20650;
delete from custo_item_fatura where id = 18148;
delete from custo_item_fatura where id = 18487;
delete from custo_item_fatura where id = 18489;

-- rollback
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (11277,4,24,5,222, '2016-06-07 12:24:56',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (17178,44,96,2,409, '2016-02-16 15:51:06',500.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (24940,101,96,5,409, '2016-06-14 12:03:34',0.07);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (3,21,142,2,60, '2016-01-18 11:37:40',0.22);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (9835,52,144,2,222, '2016-04-01 13:17:35',50.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (7756,17,179,5,263, '2015-12-15 13:26:05',6.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (919,27,188,2,222, '2016-02-24 10:14:10',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (7256,12,205,2,263, '2015-09-04 13:47:07',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (7267,99,205,2,263, '2015-09-04 13:47:07',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (7219,25,228,2,222, '2016-02-16 16:32:06',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (8367,19,233,2,263, '2015-09-11 15:30:08',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (16040,57,233,2,222, '2016-03-15 11:32:17',80.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (7728,55,407,2,263, '2016-03-04 15:47:26',33.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (24551,132,469,5,409, '2016-06-07 11:57:04',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (7078,57,481,2,222, '2016-03-09 13:35:16',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (9643,18,558,2,222, '2015-09-24 10:57:55',180.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (8974,17,901,5,222, '2016-03-17 10:47:52',6.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (9430,27,1142,2,222, '2015-09-25 18:09:54',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (17173,12,1145,2,409, '2016-02-16 15:32:15',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (5754,25,1161,2,222, '2015-08-19 14:32:24',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (5756,27,1161,2,222, '2015-08-19 14:32:24',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (5760,56,1161,2,222, '2015-08-19 14:32:24',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (9219,21,1200,2,222, '2015-09-15 10:23:44',260.73);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (9220,25,1200,2,222, '2015-09-15 10:23:44',30.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (9221,54,1200,2,222, '2015-09-15 10:23:44',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (9222,56,1200,2,222, '2015-09-15 10:23:44',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (20588,99,1205,5,409, '2016-04-20 10:54:51',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (20585,100,1205,5,409, '2016-04-20 10:54:51',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (22943,34,1236,5,409, '2016-05-20 11:34:47',2.09);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (11906,45,1239,2,222, '2015-10-26 19:08:48',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (12633,54,1245,2,222, '2015-11-05 17:54:13',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (25166,44,1249,2,409, '2016-06-21 13:07:40',72.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (14586,57,1284,2,222, '2015-12-23 18:11:09',80.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (20650,128,1300,5,409, '2016-05-19 12:26:10',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (18148,24,1331,2,409, '2016-03-14 08:29:53',0.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (18487,25,1337,2,409, '2016-03-23 17:21:34',30.00);
Insert into custo_item_fatura (id, cobrancaplano_id, paciente_id,tipo,usuario_id,data,custo) values (18489,27,1337,2,409, '2016-03-23 17:21:34',0.00);



--- Para itens de mesmo valorescobranca_id
--- roda a primira consulta para pegar identificar quais os itens que devem permanecer dos repetidos

SELECT
custo_item_fatura.id
FROM
custo_item_fatura
INNER JOIN
(SELECT
custo_item_fatura.valorescobranca_id as cat ,
custo_item_fatura.paciente_id as pac,
custo_item_fatura.tipo
FROM
custo_item_fatura
WHERE
custo_item_fatura.valorescobranca_id IS NOT NULL
GROUP BY
custo_item_fatura.valorescobranca_id,
custo_item_fatura.paciente_id,
custo_item_fatura.tipo
HAVING
COUNT(custo_item_fatura.valorescobranca_id) >1
ORDER BY
custo_item_fatura.paciente_id) as table1 on (custo_item_fatura.valorescobranca_id =table1.cat and custo_item_fatura.paciente_id = table1.pac and custo_item_fatura.tipo = table1.tipo)
where
valorescobranca_id IS NOT NULL
GROUP BY
custo_item_fatura.valorescobranca_id,
custo_item_fatura.paciente_id,
custo_item_fatura.tipo
ORDER BY
paciente_id,
valorescobranca_id,
data DESC,
custo DESC

SELECT
CONCAT('delete from custo_item_fatura where id = ', custo_item_fatura.id, ';'),
CONCAT("Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (",COALESCE(custo_item_fatura.id,''),",", COALESCE(custo_item_fatura.valorescobranca_id,''),",", COALESCE(custo_item_fatura.paciente_id,''),",",COALESCE(custo_item_fatura.tipo,''),",",COALESCE(custo_item_fatura.usuario_id,''),", '",COALESCE(custo_item_fatura.data,''),"',",COALESCE(custo_item_fatura.custo,''),");"),
custo_item_fatura.id,
custo_item_fatura.catalogo_id,
custo_item_fatura.cobrancaplano_id,
custo_item_fatura.paciente_id,
custo_item_fatura.tipo,
custo_item_fatura.usuario_id,
custo_item_fatura.`data`,
custo_item_fatura.custo,
custo_item_fatura.valorescobranca_id
FROM
custo_item_fatura
INNER JOIN
(SELECT
custo_item_fatura.valorescobranca_id as cat ,
custo_item_fatura.paciente_id as pac,
custo_item_fatura.tipo as tip
FROM
custo_item_fatura
WHERE
custo_item_fatura.valorescobranca_id IS NOT NULL
GROUP BY
custo_item_fatura.valorescobranca_id,
custo_item_fatura.paciente_id,
custo_item_fatura.tipo
HAVING
COUNT(custo_item_fatura.valorescobranca_id) >1
ORDER BY
custo_item_fatura.paciente_id) as table1 on (custo_item_fatura.valorescobranca_id =table1.cat and custo_item_fatura.paciente_id = table1.pac and custo_item_fatura.tipo = table1.tip)
where
valorescobranca_id IS NOT NULL and
id not in (24885,
24890,
24887,
25808,
25809,
25815,
25806,
25807,
25810,
25816,
25803,
25811,
25812,
25804,
25813,
25805,
25814,
24934,
19875,
25141,
25424,
21652,
24991,
24529,
24530,
24531,
24532,
24528,
24523,
24534,
24535,
24536,
24527,
24524,
24525,
24526,
24537,
25614,
22273,
25520,
25543,
25544,
25536,
25546,
25547,
25548,
25542,
25537,
25538,
25539,
25540,
25541,
25545,
25549,
22260,
21214,
19503,
21562,
21563,
21564,
21558,
21557,
21559,
21560,
21561,
24471,
24472,
24473,
19133,
22914,
22909,
22916,
22911,
22912,
22910,
22907,
22908,
22913,
24986,
24042,
24043,
24041,
24038,
24039,
24040,
25214,
20021,
20022,
20023,
20024,
22375,
22374,
22373,
22372,
22376,
22371,
25033,
25034,
25035,
25037,
25038,
25036,
25039,
25031,
25032,
25040
)
ORDER BY
paciente_id,
valorescobranca_id

-- retirar itens duplicados com o mesmo valorescobranca_id

delete from custo_item_fatura where id = 24886;
delete from custo_item_fatura where id = 24891;
delete from custo_item_fatura where id = 24888;
delete from custo_item_fatura where id = 25822;
delete from custo_item_fatura where id = 25823;
delete from custo_item_fatura where id = 25829;
delete from custo_item_fatura where id = 25820;
delete from custo_item_fatura where id = 25821;
delete from custo_item_fatura where id = 25824;
delete from custo_item_fatura where id = 25830;
delete from custo_item_fatura where id = 25817;
delete from custo_item_fatura where id = 25825;
delete from custo_item_fatura where id = 25826;
delete from custo_item_fatura where id = 25818;
delete from custo_item_fatura where id = 25827;
delete from custo_item_fatura where id = 25819;
delete from custo_item_fatura where id = 25828;
delete from custo_item_fatura where id = 24939;
delete from custo_item_fatura where id = 19876;
delete from custo_item_fatura where id = 25147;
delete from custo_item_fatura where id = 25425;
delete from custo_item_fatura where id = 21653;
delete from custo_item_fatura where id = 24992;
delete from custo_item_fatura where id = 24547;
delete from custo_item_fatura where id = 24548;
delete from custo_item_fatura where id = 24549;
delete from custo_item_fatura where id = 24550;
delete from custo_item_fatura where id = 24546;
delete from custo_item_fatura where id = 24541;
delete from custo_item_fatura where id = 24552;
delete from custo_item_fatura where id = 24553;
delete from custo_item_fatura where id = 24554;
delete from custo_item_fatura where id = 24545;
delete from custo_item_fatura where id = 24542;
delete from custo_item_fatura where id = 24543;
delete from custo_item_fatura where id = 24544;
delete from custo_item_fatura where id = 24555;
delete from custo_item_fatura where id = 25615;
delete from custo_item_fatura where id = 22274;
delete from custo_item_fatura where id = 25528;
delete from custo_item_fatura where id = 25559;
delete from custo_item_fatura where id = 25560;
delete from custo_item_fatura where id = 25552;
delete from custo_item_fatura where id = 25562;
delete from custo_item_fatura where id = 25563;
delete from custo_item_fatura where id = 25564;
delete from custo_item_fatura where id = 25558;
delete from custo_item_fatura where id = 25553;
delete from custo_item_fatura where id = 25554;
delete from custo_item_fatura where id = 25555;
delete from custo_item_fatura where id = 25556;
delete from custo_item_fatura where id = 25557;
delete from custo_item_fatura where id = 25561;
delete from custo_item_fatura where id = 25565;
delete from custo_item_fatura where id = 22261;
delete from custo_item_fatura where id = 21215;
delete from custo_item_fatura where id = 19504;
delete from custo_item_fatura where id = 21570;
delete from custo_item_fatura where id = 21578;
delete from custo_item_fatura where id = 21571;
delete from custo_item_fatura where id = 21579;
delete from custo_item_fatura where id = 21580;
delete from custo_item_fatura where id = 21572;
delete from custo_item_fatura where id = 21574;
delete from custo_item_fatura where id = 21566;
delete from custo_item_fatura where id = 21573;
delete from custo_item_fatura where id = 21565;
delete from custo_item_fatura where id = 21567;
delete from custo_item_fatura where id = 21575;
delete from custo_item_fatura where id = 21568;
delete from custo_item_fatura where id = 21576;
delete from custo_item_fatura where id = 21569;
delete from custo_item_fatura where id = 21577;
delete from custo_item_fatura where id = 24476;
delete from custo_item_fatura where id = 24477;
delete from custo_item_fatura where id = 24478;
delete from custo_item_fatura where id = 19134;
delete from custo_item_fatura where id = 22942;
delete from custo_item_fatura where id = 22937;
delete from custo_item_fatura where id = 22944;
delete from custo_item_fatura where id = 22939;
delete from custo_item_fatura where id = 22940;
delete from custo_item_fatura where id = 22938;
delete from custo_item_fatura where id = 22935;
delete from custo_item_fatura where id = 22936;
delete from custo_item_fatura where id = 22941;
delete from custo_item_fatura where id = 24987;
delete from custo_item_fatura where id = 24072;
delete from custo_item_fatura where id = 24073;
delete from custo_item_fatura where id = 24071;
delete from custo_item_fatura where id = 24068;
delete from custo_item_fatura where id = 24069;
delete from custo_item_fatura where id = 24070;
delete from custo_item_fatura where id = 25215;
delete from custo_item_fatura where id = 20025;
delete from custo_item_fatura where id = 20026;
delete from custo_item_fatura where id = 20027;
delete from custo_item_fatura where id = 20028;
delete from custo_item_fatura where id = 22381;
delete from custo_item_fatura where id = 22380;
delete from custo_item_fatura where id = 22379;
delete from custo_item_fatura where id = 22378;
delete from custo_item_fatura where id = 22382;
delete from custo_item_fatura where id = 22377;
delete from custo_item_fatura where id = 25049;
delete from custo_item_fatura where id = 25050;
delete from custo_item_fatura where id = 25051;
delete from custo_item_fatura where id = 25053;
delete from custo_item_fatura where id = 25054;
delete from custo_item_fatura where id = 25052;
delete from custo_item_fatura where id = 25055;
delete from custo_item_fatura where id = 25047;
delete from custo_item_fatura where id = 25048;
delete from custo_item_fatura where id = 25056;

-- rollback

Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24886,20497,8,2,409, '2016-06-14 12:13:58',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24891,20506,8,2,409, '2016-06-14 12:13:58',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24888,20507,8,2,409, '2016-06-14 12:13:58',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25822,20575,67,5,60, '2016-07-04 17:50:05',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25823,20576,67,5,60, '2016-07-04 17:50:05',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25829,20577,67,5,60, '2016-07-04 17:50:05',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25820,20583,67,5,60, '2016-07-04 17:50:05',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25821,20584,67,5,60, '2016-07-04 17:50:05',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25824,20586,67,5,60, '2016-07-04 17:50:05',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25830,20587,67,5,60, '2016-07-04 17:50:05',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25817,20590,67,2,60, '2016-07-04 17:50:05',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25825,20591,67,5,60, '2016-07-04 17:50:05',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25826,20592,67,5,60, '2016-07-04 17:50:05',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25818,20593,67,5,60, '2016-07-04 17:50:05',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25827,20594,67,5,60, '2016-07-04 17:50:05',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25819,20611,67,5,60, '2016-07-04 17:50:05',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25828,20619,67,5,60, '2016-07-04 17:50:05',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24939,18752,96,2,409, '2016-06-14 12:03:34',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (19876,20824,142,5,60, '2016-04-11 15:05:42',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25147,18937,151,6,409, '2016-06-20 11:11:01',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25425,21277,168,2,409, '2016-06-27 13:19:02',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21653,18754,234,2,409, '2016-06-28 09:56:42',23.75);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24992,20497,301,2,409, '2016-06-15 11:43:42',32.80);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24547,20822,469,5,409, '2016-06-07 11:57:04',2.50);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24548,20823,469,5,409, '2016-06-07 11:57:04',1.70);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24549,20824,469,5,409, '2016-06-07 11:57:04',4.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24550,20827,469,5,409, '2016-06-07 11:57:04',25.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24546,20828,469,5,409, '2016-06-07 11:57:04',1.33);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24541,20837,469,2,409, '2016-06-07 11:57:04',31.13);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24552,20839,469,5,409, '2016-06-07 11:57:04',159.90);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24553,20840,469,5,409, '2016-06-07 11:57:04',0.65);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24554,20841,469,5,409, '2016-06-07 11:57:04',2.09);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24545,20842,469,5,409, '2016-06-07 11:57:04',5.33);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24542,20851,469,2,409, '2016-06-07 11:57:04',193.41);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24543,20853,469,2,409, '2016-06-07 11:57:04',173.45);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24544,20854,469,2,409, '2016-06-07 11:57:04',30.80);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24555,20861,469,5,409, '2016-06-07 11:57:04',0.65);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25615,21261,626,2,409, '2016-06-29 13:48:10',80.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (22274,19882,711,2,409, '2016-06-01 11:25:19',30.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25528,20855,711,2,409, '2016-06-29 10:26:13',415.52);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25559,23038,711,5,409, '2016-06-29 10:45:39',4.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25560,23046,711,5,409, '2016-06-29 10:45:39',6.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25552,23053,711,2,409, '2016-06-29 10:45:39',30.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25562,23055,711,5,409, '2016-06-29 10:45:39',5.33);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25563,23056,711,5,409, '2016-06-29 10:45:39',0.65);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25564,23057,711,5,409, '2016-06-29 10:45:39',2.09);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25558,23058,711,5,409, '2016-06-29 10:45:39',5.33);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25553,23070,711,2,409, '2016-06-29 10:45:39',227.36);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25554,23072,711,2,409, '2016-06-29 10:45:39',304.83);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25555,23073,711,2,409, '2016-06-29 10:45:39',50.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25556,23075,711,2,409, '2016-06-29 10:45:39',50.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25557,23076,711,2,409, '2016-06-29 10:45:39',50.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25561,23079,711,5,409, '2016-06-29 10:45:39',4.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25565,23081,711,5,409, '2016-06-29 10:45:39',2.24);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (22261,19882,807,2,409, '2016-06-01 11:34:20',40.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21215,20855,1057,2,409, '2016-06-02 09:13:12',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (19504,19882,1089,2,409, '2016-05-16 10:55:24',30.50);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21570,19867,1126,5,409, '2016-05-05 14:37:34',2.50);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21578,19867,1126,5,409, '2016-05-05 14:38:24',2.50);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21571,19868,1126,5,409, '2016-05-05 14:37:34',1.70);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21579,19868,1126,5,409, '2016-05-05 14:38:24',1.70);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21580,19869,1126,5,409, '2016-05-05 14:38:24',4.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21572,19869,1126,5,409, '2016-05-05 14:37:34',4.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21574,19879,1126,2,409, '2016-05-05 14:38:24',109.77);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21566,19879,1126,2,409, '2016-05-05 14:37:34',109.77);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21573,19882,1126,2,409, '2016-05-05 14:38:24',24.90);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21565,19882,1126,2,409, '2016-05-05 14:37:34',24.90);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21567,19899,1126,2,409, '2016-05-05 14:37:34',199.86);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21575,19899,1126,2,409, '2016-05-05 14:38:24',199.86);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21568,19901,1126,2,409, '2016-05-05 14:37:34',195.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21576,19901,1126,2,409, '2016-05-05 14:38:24',195.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21569,19902,1126,2,409, '2016-05-05 14:37:34',156.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (21577,19902,1126,2,409, '2016-05-05 14:38:24',156.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24476,20848,1158,2,409, '2016-06-07 09:33:36',38.34);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24477,20853,1158,2,409, '2016-06-07 09:33:36',173.45);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24478,20856,1158,2,409, '2016-06-07 09:33:36',80.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (19134,19882,1178,2,409, '2016-06-06 09:06:40',31.13);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (22942,19304,1236,5,409, '2016-05-20 11:34:47',1.33);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (22937,19310,1236,2,409, '2016-05-20 11:34:47',33.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (22944,19311,1236,5,409, '2016-05-20 11:34:47',5.33);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (22939,19317,1236,2,409, '2016-05-20 11:34:47',135.09);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (22940,19318,1236,2,409, '2016-05-20 11:34:47',141.07);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (22938,19319,1236,2,409, '2016-05-20 11:34:47',35.60);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (22935,19321,1236,2,409, '2016-05-20 11:34:47',80.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (22936,19327,1236,2,409, '2016-05-20 11:34:47',45.03);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (22941,20227,1236,2,409, '2016-05-20 11:34:47',814.72);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24987,20523,1282,2,409, '2016-06-15 10:17:08',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24072,20822,1315,5,409, '2016-06-10 10:18:18',2.50);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24073,20823,1315,5,409, '2016-06-10 10:18:18',1.70);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24071,20828,1315,5,409, '2016-06-10 10:18:18',1.33);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24068,20835,1315,2,409, '2016-06-10 10:18:18',196.22);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24069,20853,1315,2,409, '2016-06-10 10:18:18',173.45);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (24070,20855,1315,2,409, '2016-06-10 10:18:18',281.73);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25215,22490,1345,2,263, '2016-06-28 09:33:07',196.95);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (20025,20174,1347,2,409, '2016-06-17 13:39:50',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (20026,20191,1347,2,409, '2016-06-17 13:39:50',36.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (20027,20192,1347,2,409, '2016-06-17 13:39:50',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (20028,20193,1347,2,409, '2016-06-17 13:39:50',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (22381,20551,1362,2,409, '2016-05-16 09:51:37',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (22380,20552,1362,2,409, '2016-05-16 09:51:37',30.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (22379,20565,1362,2,409, '2016-05-16 09:51:37',200.15);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (22378,20566,1362,2,409, '2016-05-16 09:51:37',30.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (22382,20567,1362,2,409, '2016-05-16 09:51:37',333.42);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (22377,20568,1362,2,409, '2016-05-16 09:51:37',50.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25049,22316,1387,2,409, '2016-06-16 15:25:56',216.45);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25050,22319,1387,2,409, '2016-06-16 15:25:56',29.38);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25051,22321,1387,2,409, '2016-06-16 15:25:56',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25053,22334,1387,2,409, '2016-06-16 15:25:56',39.26);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25054,22338,1387,2,409, '2016-06-16 15:25:56',219.35);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25052,22340,1387,2,409, '2016-06-16 15:25:56',30.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25055,22342,1387,2,409, '2016-06-16 15:25:56',523.69);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25047,22344,1387,2,409, '2016-06-16 15:25:56',80.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25048,22362,1387,2,409, '2016-06-16 15:25:56',0.00);
Insert into custo_item_fatura (id, valorescobranca_id, paciente_id,tipo,usuario_id,data,custo) values (25056,22364,1387,5,409, '2016-06-16 15:25:56',1.60);




