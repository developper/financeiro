-- SM-1684
ALTER TABLE `devolucao_interna_pedido`
MODIFY COLUMN `status`  enum('FINALIZADO','AUTORIZADO','CANCELADO','ENVIADO','CONFIRMADO PARCIALMENTE','PENDENTE') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `motivo_cancelamento`;

CREATE TABLE `historico_devolucao_interna_item_confirmacao` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`devolucao_interna_item_id`  int(11) NOT NULL ,
`created_at`  datetime NOT NULL ,
`created_by`  int(11) NOT NULL ,
`quantidade`  int NOT NULL ,
PRIMARY KEY (`id`)
)
;


-- SM-1716
CREATE TABLE `motivo_juros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `motivo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;


INSERT INTO `motivo_juros` VALUES ('1', 'Fornecedor');
INSERT INTO `motivo_juros` VALUES ('2', 'Setor Diverso');
INSERT INTO `motivo_juros` VALUES ('3', 'Prorrogação de boleto');
INSERT INTO `motivo_juros` VALUES ('4', 'Setor Financeiro');

ALTER TABLE `parcelas`
ADD COLUMN `motivo_juros_id`  int(11) NULL AFTER `PARCELA_ORIGEM`;