CREATE TABLE planserv_plano_feridas_doencas_sistemicas
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  plano_feridas_id INT NOT NULL,
  doenca_sistemica INT NOT NULL
);

SELECT
  CONCAT('INSERT INTO planserv_plano_feridas_doencas_sistemicas VALUES (NULL, ', plano_feridas_id, ', ', doencas_sistemicas, ')') AS insertT
FROM
  planserv_plano_feridas_historico_paciente
WHERE
  planserv_plano_feridas_historico_paciente.doencas_sistemicas != 0;


ALTER TABLE planserv_plano_feridas_historico_paciente DROP doencas_sistemicas;