-- SM-2068
ALTER TABLE notas ADD nota_fatura int DEFAULT 0 NULL;
ALTER TABLE fatura_controle_recebimento MODIFY status enum('PENDENTE', 'RECEBIDO', 'PROGRAMADO') NOT NULL;
ALTER TABLE fatura_controle_recebimento ADD valor_programado float(20, 2) NULL AFTER nota_id;