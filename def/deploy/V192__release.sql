CREATE TABLE sismederi.exames_laboratoriais
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(150) NOT NULL
);
ALTER TABLE sismederi.exames_laboratoriais COMMENT = 'Tabela com a lista de exames laboratoriais e de imagem';

CREATE TABLE sismederi.exames_laboratoriais_paciente
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  paciente INT NOT NULL,
  inicio DATE NOT NULL,
  fim DATE NOT NULL,
  tipo ENUM('el', 'em') DEFAULT 'el' NOT NULL COMMENT '0 = Eletivo / 1 = Emergencial',
  justificativa_emergencial TEXT,
  observacao TEXT,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created_by INT NOT NULL
);
ALTER TABLE sismederi.exames_laboratoriais_paciente COMMENT = 'Tabela com os dados dos exames lançados para um paciente';

CREATE TABLE sismederi.exames_laboratoriais_tags
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  exame_paciente INT NOT NULL,
  tipo_exame INT NOT NULL
);
ALTER TABLE sismederi.exames_laboratoriais_tags COMMENT = 'Tabela com as tags dos respectivos exames';

CREATE TABLE sismederi.exames_laboratoriais_upload
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  exame_paciente INT NOT NULL,
  upload_id INT(11) NOT NULL
);
ALTER TABLE sismederi.exames_laboratoriais_upload COMMENT = 'Tabela com os caminhos das imagens dos exames';

INSERT INTO exames_laboratoriais VALUES
  (NULL, 'PCR'),
  (NULL, 'GLICEMIA DE JEJUM'),
  (NULL, 'GASOMETRIA ARTERIAL'),
  (NULL, 'SUMÁRIO DE URINA'),
  (NULL, 'UROCULTURA'),
  (NULL, 'SÓDIO'),
  (NULL, 'POTÁSSIO'),
  (NULL, 'CLORO'),
  (NULL, 'CÁLCIO'),
  (NULL, 'FOSFÓRO'),
  (NULL, 'MAGNÉSIO'),
  (NULL, 'URÉIA'),
  (NULL, 'CREATININA'),
  (NULL, 'ÁCIDO ÚRICO'),
  (NULL, 'COAGULOGRAM'),
  (NULL, 'TSH'),
  (NULL, 'T4L'),
  (NULL, 'TROPONINA I'),
  (NULL, 'CK MB'),
  (NULL, 'FERRO'),
  (NULL, 'FERRATINA'),
  (NULL, 'AMILASE'),
  (NULL, 'LIPASE'),
  (NULL, 'TGO'),
  (NULL, 'TGP'),
  (NULL, 'FA'),
  (NULL, 'GAMA GT'),
  (NULL, 'BILIRRUBINA TOTAL'),
  (NULL, 'BILIRRUBINA DIRETA'),
  (NULL, 'BILIRRUBINA INDIRETA'),
  (NULL, 'CULTURA DE ASPIRADO TRAQUEAL'),
  (NULL, 'HEMOCULTURA'),
  (NULL, 'CULTURA SECREÇÃO GASTROSTOMIA');