ALTER TABLE capmedica ADD MODELO_NEAD CHAR(4) DEFAULT '2016' NOT NULL;
ALTER TABLE relatorio_prorrogacao_med ADD MODELO_NEAD CHAR(4) DEFAULT '2016' NOT NULL;

ALTER TABLE capmedica ADD CLASSIFICACAO_NEAD INT NULL;
ALTER TABLE relatorio_prorrogacao_med ADD CLASSIFICACAO_NEAD INT NULL;

UPDATE capmedica
SET
  capmedica.MODELO_NEAD = '2015';

UPDATE relatorio_prorrogacao_med
SET
  relatorio_prorrogacao_med.MODELO_NEAD = '2015';

INSERT INTO score VALUES (NULL, 'NEAD2016');

INSERT INTO grupos VALUES
  (NULL, 'ESTADO NUTRICIONAL', 3, 5),
  (NULL, 'ALIMENTAÇÃO OU MEDICAÇÕES POR VIA ENTERAL', 4, 5),
  (NULL, 'KATZ (SE PONTUAR 2)', 3, 5),
  (NULL, 'INTERNAÇÕES NO ÚLTIMO ANO', 3, 5),
  (NULL, 'ASPIRAÇÕES VIAS AÉREAS SUPERIORES', 3, 5),
  (NULL, 'LESÕES', 3, 5),
  (NULL, 'MEDICAÇÕES', 3, 5),
  (NULL, 'EXERCÍCIOS VENTILATÓRIOS', 2, 5),
  (NULL, 'USO DE OXIGENOTERAPIA', 3, 5),
  (NULL, 'NÍVEL DE CONSCIÊNCIA', 3, 5);

INSERT INTO score_item VALUES
  (NULL, 36, 5, 'Eutrófico', 0),
  (NULL, 36, 5, 'Sobrepeso/Emagrecido', 1),
  (NULL, 36, 5, 'Obeso/Desnutrido', 2),
  (NULL, 37, 5, 'Sem Auxílio', 0),
  (NULL, 37, 5, 'Assistida', 1),
  (NULL, 37, 5, 'Gastrostomia/Jejunostomia', 2),
  (NULL, 37, 5, 'Por SNG/SNE', 3),
  (NULL, 38, 5, 'Independente', 0),
  (NULL, 38, 5, 'Dependente Parcial', 1),
  (NULL, 38, 5, 'Dependente Total', 2),
  (NULL, 39, 5, '0-1 Internação', 0),
  (NULL, 39, 5, '2-3 Internações', 1),
  (NULL, 39, 5, '> 3 Internações', 2),
  (NULL, 40, 5, 'Ausente', 0),
  (NULL, 40, 5, 'Até 5 Vezes ao Dia', 1),
  (NULL, 40, 5, 'Mais de 5 Vezes ao Dia', 2),
  (NULL, 41, 5, 'Nenhuma ou Lesão Única com Curativo Simples', 0),
  (NULL, 41, 5, 'Múltiplas Lesões com Curativos Simples Ou Única Lesão com Curativo Complexo', 1),
  (NULL, 41, 5, 'Múltiplas Lesões com Curativos Complexos', 2),
  (NULL, 42, 5, 'Via Oral', 0),
  (NULL, 42, 5, 'Intravenosa Até 4 Vezes ao Dia', 2),
  (NULL, 42, 5, 'Intramuscular ou Subcultânea', 1),
  (NULL, 43, 5, 'Ausente', 0),
  (NULL, 43, 5, 'Intermitente', 1),
  (NULL, 44, 5, 'Ausente', 0),
  (NULL, 44, 5, 'Intermitente', 1),
  (NULL, 44, 5, 'Contínuo', 2),
  (NULL, 45, 5, 'Alerta', 0),
  (NULL, 45, 5, 'Confuso/Desorientado', 1),
  (NULL, 45, 5, 'Comatoso', 2);

-- SM-1422
ALTER TABLE exames_laboratoriais_paciente ADD coleta ENUM('L', 'E') DEFAULT 'L' NOT NULL;
ALTER TABLE exames_laboratoriais_paciente
  MODIFY COLUMN coleta ENUM('L', 'E') NOT NULL DEFAULT 'L' COMMENT 'L = Laboratorio / E = Enfermeira' AFTER fim;

CREATE TABLE nead2016_paciente
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  paciente INT NOT NULL,
  relatorio INT NOT NULL,
  tipo_relatorio VARCHAR(100) NOT NULL COMMENT 'Ex: capmedica',
  secao VARCHAR(50) NOT NULL,
  opcao VARCHAR(150) NOT NULL,
  valor INT NOT NULL
);

-- SM-1413
CREATE TABLE `historico_custo_servico_plano_ur` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`cobrancaplano_id`  int(11) NOT NULL ,
`empresa_id`  int(11) NOT NULL ,
`inicio`  datetime NOT NULL ,
`custo`  double(8,2) NOT NULL ,
`usuario_id`  int(11) NOT NULL ,
`plano_id`  int(11) NOT NULL DEFAULT 1 COMMENT 'Como não vão ser mais lançados por plano vai ficar fixo como 1, plano particular' ,
`data_sistema`  datetime NOT NULL ,
PRIMARY KEY (`id`)
)
;

ALTER TABLE `historico_custo_servico_plano_ur`
CHANGE COLUMN `data_sistema` `created_at`  datetime NOT NULL AFTER `plano_id`,
ADD COLUMN `created_by`  int(11) NOT NULL AFTER `created_at`;
-- SM-1414
Rename TABLE custo_servicos_plano_ur to custo_servicos_plano_ur_old;

CREATE TABLE `custo_servicos_plano_ur` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PLANO_ID` int(11) NOT NULL,
  `UR` int(11) NOT NULL,
  `COBRANCAPLANO_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  `CUSTO` double(8,2) NOT NULL,
  `STATUS` char(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT= 0 DEFAULT CHARSET=latin1;

-- ur feira
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '1', '2017-03-16 12:53:40', '60', '2.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '2', '2017-03-16 12:53:40', '60', '1.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '3', '2017-03-16 12:53:40', '60', '2.67', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '4', '2017-03-16 12:53:40', '60', '4.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '5', '2017-03-16 12:53:40', '60', '10.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '6', '2017-03-16 12:53:40', '60', '3.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '8', '2017-03-16 12:53:40', '60', '19.66', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '11', '2017-03-16 12:53:40', '60', '21.61', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '12', '2017-03-16 12:53:40', '60', '1.20', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '17', '2017-03-16 12:53:40', '60', '6.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '31', '2017-03-16 12:53:40', '60', '4.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '34', '2017-03-16 12:53:40', '60', '1.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '35', '2017-03-16 12:53:40', '60', '5.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '63', '2017-03-16 12:53:40', '60', '4.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '64', '2017-03-16 12:53:40', '60', '3.40', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '65', '2017-03-16 12:53:40', '60', '4.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '66', '2017-03-16 12:53:40', '60', '4.80', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '67', '2017-03-16 12:53:40', '60', '10.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '74', '2017-03-16 12:53:40', '60', '0.60', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '102', '2017-03-16 12:53:40', '60', '0.60', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '103', '2017-03-16 12:53:40', '60', '63.33', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '108', '2017-03-16 12:53:40', '60', '25.66', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '110', '2017-03-16 12:53:40', '60', '63.33', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '112', '2017-03-16 12:53:40', '60', '1.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '128', '2017-03-16 12:53:40', '60', '0.60', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '129', '2017-03-16 12:53:40', '60', '5.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '132', '2017-03-16 12:53:40', '60', '21.66', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '133', '2017-03-16 12:53:40', '60', '4.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '134', '2017-03-16 12:53:40', '60', '43.33', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '136', '2017-03-16 12:53:40', '60', '1.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '1', '2017-03-16 12:53:40', '60', '2.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '11', '1', '2017-03-16 12:53:40', '60', '2.50', 'A');


-- Alagoinhas

INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '1', '2017-03-16 12:53:40', '60', '2.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '2', '2017-03-16 12:53:40', '60', '1.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '3', '2017-03-16 12:53:40', '60', '2.67', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '4', '2017-03-16 12:53:40', '60', '4.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '5', '2017-03-16 12:53:40', '60', '10.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '6', '2017-03-16 12:53:40', '60', '3.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '8', '2017-03-16 12:53:40', '60', '19.66', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '11', '2017-03-16 12:53:40', '60', '21.61', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '12', '2017-03-16 12:53:40', '60', '1.20', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '17', '2017-03-16 12:53:40', '60', '6.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '31', '2017-03-16 12:53:40', '60', '4.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '34', '2017-03-16 12:53:40', '60', '1.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '35', '2017-03-16 12:53:40', '60', '5.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '63', '2017-03-16 12:53:40', '60', '4.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '64', '2017-03-16 12:53:40', '60', '3.40', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '65', '2017-03-16 12:53:40', '60', '4.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '66', '2017-03-16 12:53:40', '60', '4.80', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '67', '2017-03-16 12:53:40', '60', '10.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '74', '2017-03-16 12:53:40', '60', '0.60', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '102', '2017-03-16 12:53:40', '60', '0.60', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '103', '2017-03-16 12:53:40', '60', '63.33', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '108', '2017-03-16 12:53:40', '60', '25.66', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '110', '2017-03-16 12:53:40', '60', '63.33', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '112', '2017-03-16 12:53:40', '60', '1.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '128', '2017-03-16 12:53:40', '60', '0.60', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '129', '2017-03-16 12:53:40', '60', '5.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '132', '2017-03-16 12:53:40', '60', '21.66', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '133', '2017-03-16 12:53:40', '60', '4.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '134', '2017-03-16 12:53:40', '60', '43.33', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '136', '2017-03-16 12:53:40', '60', '1.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '1', '2017-03-16 12:53:40', '60', '2.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '5', '1', '2017-03-16 12:53:40', '60', '2.50', 'A');

-- salvador
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '1', '2017-03-16 12:53:40', '60', '2.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '2', '2017-03-16 12:53:40', '60', '1.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '3', '2017-03-16 12:53:40', '60', '2.67', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '4', '2017-03-16 12:53:40', '60', '4.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '5', '2017-03-16 12:53:40', '60', '10.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '6', '2017-03-16 12:53:40', '60', '3.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '8', '2017-03-16 12:53:40', '60', '19.66', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '11', '2017-03-16 12:53:40', '60', '21.61', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '12', '2017-03-16 12:53:40', '60', '1.20', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '17', '2017-03-16 12:53:40', '60', '6.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '31', '2017-03-16 12:53:40', '60', '4.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '34', '2017-03-16 12:53:40', '60', '1.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '35', '2017-03-16 12:53:40', '60', '5.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '63', '2017-03-16 12:53:40', '60', '4.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '64', '2017-03-16 12:53:40', '60', '3.40', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '65', '2017-03-16 12:53:40', '60', '4.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '66', '2017-03-16 12:53:40', '60', '4.80', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '67', '2017-03-16 12:53:40', '60', '10.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '74', '2017-03-16 12:53:40', '60', '0.60', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '102', '2017-03-16 12:53:40', '60', '0.60', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '103', '2017-03-16 12:53:40', '60', '63.33', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '108', '2017-03-16 12:53:40', '60', '25.66', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '110', '2017-03-16 12:53:40', '60', '63.33', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '112', '2017-03-16 12:53:40', '60', '1.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '128', '2017-03-16 12:53:40', '60', '0.60', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '129', '2017-03-16 12:53:40', '60', '5.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '132', '2017-03-16 12:53:40', '60', '21.66', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '133', '2017-03-16 12:53:40', '60', '4.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '134', '2017-03-16 12:53:40', '60', '43.33', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '136', '2017-03-16 12:53:40', '60', '1.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '1', '2017-03-16 12:53:40', '60', '2.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '2', '1', '2017-03-16 12:53:40', '60', '2.50', 'A');
-- sulbahia

INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '1', '2017-03-16 12:53:40', '60', '2.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '2', '2017-03-16 12:53:40', '60', '1.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '3', '2017-03-16 12:53:40', '60', '2.67', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '4', '2017-03-16 12:53:40', '60', '4.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '5', '2017-03-16 12:53:40', '60', '10.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '6', '2017-03-16 12:53:40', '60', '3.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '8', '2017-03-16 12:53:40', '60', '19.66', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '11', '2017-03-16 12:53:40', '60', '21.61', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '12', '2017-03-16 12:53:40', '60', '1.20', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '17', '2017-03-16 12:53:40', '60', '6.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '31', '2017-03-16 12:53:40', '60', '4.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '34', '2017-03-16 12:53:40', '60', '1.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '35', '2017-03-16 12:53:40', '60', '5.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '63', '2017-03-16 12:53:40', '60', '4.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '64', '2017-03-16 12:53:40', '60', '3.40', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '65', '2017-03-16 12:53:40', '60', '4.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '66', '2017-03-16 12:53:40', '60', '4.80', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '67', '2017-03-16 12:53:40', '60', '10.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '74', '2017-03-16 12:53:40', '60', '0.60', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '102', '2017-03-16 12:53:40', '60', '0.60', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '103', '2017-03-16 12:53:40', '60', '63.33', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '108', '2017-03-16 12:53:40', '60', '25.66', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '110', '2017-03-16 12:53:40', '60', '63.33', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '112', '2017-03-16 12:53:40', '60', '1.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '128', '2017-03-16 12:53:40', '60', '0.60', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '129', '2017-03-16 12:53:40', '60', '5.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '132', '2017-03-16 12:53:40', '60', '21.66', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '133', '2017-03-16 12:53:40', '60', '4.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '134', '2017-03-16 12:53:40', '60', '43.33', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '136', '2017-03-16 12:53:40', '60', '1.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '1', '2017-03-16 12:53:40', '60', '2.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '4', '1', '2017-03-16 12:53:40', '60', '2.50', 'A');
-- sudoeste
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '1', '2017-03-16 12:53:40', '60', '2.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '2', '2017-03-16 12:53:40', '60', '1.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '3', '2017-03-16 12:53:40', '60', '2.67', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '4', '2017-03-16 12:53:40', '60', '4.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '5', '2017-03-16 12:53:40', '60', '10.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '6', '2017-03-16 12:53:40', '60', '3.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '8', '2017-03-16 12:53:40', '60', '19.66', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '11', '2017-03-16 12:53:40', '60', '21.61', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '12', '2017-03-16 12:53:40', '60', '1.20', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '17', '2017-03-16 12:53:40', '60', '6.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '31', '2017-03-16 12:53:40', '60', '4.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '34', '2017-03-16 12:53:40', '60', '1.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '35', '2017-03-16 12:53:40', '60', '5.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '63', '2017-03-16 12:53:40', '60', '4.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '64', '2017-03-16 12:53:40', '60', '3.40', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '65', '2017-03-16 12:53:40', '60', '4.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '66', '2017-03-16 12:53:40', '60', '4.80', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '67', '2017-03-16 12:53:40', '60', '10.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '74', '2017-03-16 12:53:40', '60', '0.60', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '102', '2017-03-16 12:53:40', '60', '0.60', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '103', '2017-03-16 12:53:40', '60', '63.33', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '108', '2017-03-16 12:53:40', '60', '25.66', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '110', '2017-03-16 12:53:40', '60', '63.33', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '112', '2017-03-16 12:53:40', '60', '1.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '128', '2017-03-16 12:53:40', '60', '0.60', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '129', '2017-03-16 12:53:40', '60', '5.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '132', '2017-03-16 12:53:40', '60', '21.66', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '133', '2017-03-16 12:53:40', '60', '4.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '134', '2017-03-16 12:53:40', '60', '43.33', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '136', '2017-03-16 12:53:40', '60', '1.00', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '1', '2017-03-16 12:53:40', '60', '2.50', 'A');
INSERT INTO `custo_servicos_plano_ur` VALUES (NULL, '1', '7', '1', '2017-03-16 12:53:40', '60', '2.50', 'A');

-- fim SM-1414

-- SM-1419
RENAME TABLE encaminhamento_urgencia_emergencia_med TO intercorrencia_med;

RENAME TABLE enc_urgencia_emergencia_med_exame TO intercorrencia_med_exame;

RENAME TABLE enc_urgencia_emergencia_med_problema TO intercorrencia_med_problema;

RENAME TABLE enc_urgencia_emergencia_med_procedimento TO intercorrencia_med_procedimento;

RENAME TABLE enc_urgencia_emergencia_med_quadro_clinico TO intercorrencia_med_quadro_clinico;

ALTER TABLE `intercorrencia_med_exame`
CHANGE COLUMN `enc_urgencia_emergencia_med_id` `intercorrencia_med_id`  int(11) NULL DEFAULT NULL AFTER `descricao`;

ALTER TABLE `intercorrencia_med_problema`
CHANGE COLUMN `enc_urgencia_emergencia_med_id` `intercorrencia_med_id`  int(11) NULL DEFAULT NULL AFTER `id`;

ALTER TABLE `intercorrencia_med_procedimento`
CHANGE COLUMN `enc_urgencia_emergencia_med_id` `intercorrencia_med_id`  int(11) NULL DEFAULT NULL AFTER `descricao`;

ALTER TABLE `intercorrencia_med_quadro_clinico`
CHANGE COLUMN `enc_urgencia_emergencia_med_id` `intercorrencia_med_id`  int(11) NULL DEFAULT NULL AFTER `descricao`;


ALTER TABLE `intercorrencia_med`
ADD COLUMN `hora`  time NULL AFTER `vaga_uti`,
ADD COLUMN `hospital`  text NULL AFTER `hora`;

-- FIm-1419

-- SM-1428
ALTER TABLE `catalogo`
ADD COLUMN `custo_medio_atualizado_em`  datetime NULL AFTER `brasindice_id`;

-- pegar consulta em sql SM-1428 para atualizar datas.
-- fim SM-1428
