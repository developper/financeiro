ALTER TABLE `historico_mudanca_entrada`
ADD COLUMN `valor_novo`  double NOT NULL AFTER `validade_anterior`,
ADD COLUMN `valor_anterior`  double NOT NULL AFTER `valor_novo`;