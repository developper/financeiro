ALTER TABLE `prorrogacao_planserv_solicitacoes`
ADD COLUMN `visita_medica`  tinytext NULL AFTER `atb_med`,
ADD COLUMN `visita_enfermagem`  tinytext NULL AFTER `visita_medica`,
ADD COLUMN `visita_nutricao`  tinytext NULL AFTER `visita_enfermagem`;