ALTER TABLE `ocorrencia`
MODIFY COLUMN `finalizado_por` int(11) NULL AFTER `updated_at`,
MODIFY COLUMN `finalizado_em` datetime NULL AFTER `finalizado_por`,
ADD COLUMN `reaberto_por` int(11) NULL AFTER `ur_origem`,
ADD COLUMN `reaberto_em` datetime NULL AFTER `reaberto_por`;

CREATE TABLE `historico_ocorrencia_reaberta`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ocorrencia_id` int(11) NULL DEFAULT NULL,
  `finalizado_por` int(11) NULL DEFAULT NULL,
  `finalizado_em` datetime NULL DEFAULT NULL,
  `reaberto_por` int(11) NULL DEFAULT NULL,
  `reaberto_em` datetime NULL DEFAULT NULL,
  `retorno_hc` int(11) NULL DEFAULT NULL,
  `descricao_qualidade_retorno` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`)
) ;

ALTER TABLE tiss_xml MODIFY tiss_versao ENUM('2.02.03', '03.02.00', '03.02.01', '03.02.02', '03.03.00', '03.03.01', '03.03.02', '03.03.03');