CREATE TABLE `conselho_regional` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conselho` varchar(255) DEFAULT NULL,
  `sigla` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of conselho_regional
-- ----------------------------
INSERT INTO `conselho_regional` VALUES ('1', 'CONSELHO REGIONAL DE FONOAUDIOLOGIA', 'CRFa');
INSERT INTO `conselho_regional` VALUES ('2', 'CONSELHO REGIONAL DE SERVIÇO SOCIAL', 'CRESS');
INSERT INTO `conselho_regional` VALUES ('3', 'CONSELHO REGIONAL DE MEDICINA', 'CRM');
INSERT INTO `conselho_regional` VALUES ('4', 'CONSELHO REGIONAL DE ENFERMAGEM', 'COREN');
INSERT INTO `conselho_regional` VALUES ('5', 'CONSELHO REGIONAL DE NUTRICIONISTAS', 'CRN');
INSERT INTO `conselho_regional` VALUES ('6', 'CONSELHO REGIONAL DE PSICOLOGIA', 'CRP');
INSERT INTO `conselho_regional` VALUES ('7', 'CONSELHO REGIONAL DE FISIOTERAPIA', 'CREFITO');


ALTER TABLE `usuarios`
CHANGE COLUMN `conselho_regional` `numero_conselho_regional`  varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL AFTER `nome`,
ADD COLUMN `conselho_regional_id`  int(11) NULL AFTER `prestador`;

ALTER TABLE `usuarios`
ADD COLUMN `conselho_regional`  varchar(40) NULL;

UPDATE
usuarios
SET
conselho_regional = CONCAT("(CRM: ",usuarios.numero_conselho_regional,")"),
usuarios.conselho_regional_id = 3
WHERE
usuarios.tipo = 'medico'
and usuarios.numero_conselho_regional is not null;

UPDATE
usuarios
SET
conselho_regional = CONCAT("(COREN: ",usuarios.numero_conselho_regional,")"),
usuarios.conselho_regional_id = 4
WHERE
usuarios.tipo = 'enfermagem'
and usuarios.numero_conselho_regional is not null;

UPDATE
usuarios
SET
conselho_regional = CONCAT("(CREFITO: ",usuarios.numero_conselho_regional,")"),
usuarios.conselho_regional_id = 7
WHERE
usuarios.tipo = 'fisioterapia'
and usuarios.numero_conselho_regional is not null;

UPDATE
usuarios
SET
conselho_regional = CONCAT("(CRN: ",usuarios.numero_conselho_regional,")"),
usuarios.conselho_regional_id = 5
WHERE
usuarios.tipo = 'nutricao'
and usuarios.numero_conselho_regional is not null;

-- SM-1626
ALTER TABLE `autorizacao`
DROP COLUMN `data_autorizacao`,
MODIFY COLUMN `created_at`  datetime NOT NULL AFTER `status_geral`;

ALTER TABLE `autorizacao_itens`
CHANGE COLUMN `ultima_interacao` `data_autorizacao`  datetime NULL DEFAULT NULL AFTER `status_item`;

ALTER TABLE `autorizacao_itens`
MODIFY COLUMN `data_autorizacao`  date NULL DEFAULT NULL AFTER `status_item`,
ADD COLUMN `autorizado_por`  int NULL AFTER `data_autorizacao`,
ADD COLUMN `autorizado_em`  datetime NULL AFTER `autorizado_por`;