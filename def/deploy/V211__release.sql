-- SM-1469
ALTER TABLE `remocoes`
ADD COLUMN `empresa_id`  int(11) NULL AFTER `valorescobranca_id`,
ADD COLUMN `enfermeiro`  varchar(100) NULL AFTER `empresa_id`,
ADD COLUMN `valor_enfermeiro`  double NULL AFTER `enfermeiro`;