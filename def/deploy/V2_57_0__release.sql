-- SM-1976
ALTER TABLE `evento_aph_finalizar`
ADD COLUMN `conta_recebido` int(11) NULL COMMENT 'recebi id da conta bancaria onde recebeu o valor.' AFTER `justificativa_nao_cobrar`;

ALTER TABLE `evento_aph_finalizar`
ADD COLUMN `recebido_em` date NULL COMMENT 'Data que foi recebido.' AFTER `conta_recebido`;

-- SM-1974
ALTER TABLE `remocoes`
ADD COLUMN `conta_recebido` int(11) NULL COMMENT 'Recebe o id da conta bancaria onde foi recebido a remoção.' AFTER `edited_at`;

ALTER TABLE `remocoes`
ADD COLUMN `recebido_em` date NULL COMMENT 'Inseriri a data que foi recebido' AFTER `conta_recebido`;