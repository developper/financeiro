CREATE TABLE catalogo_codigo_barras
(
  id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  catalogo_id INT(11) NOT NULL,
  codigo_barra VARCHAR(13) NOT NULL
);
ALTER TABLE catalogo_codigo_barras COMMENT = 'Armazena os varios codigos de barras que um item da tabela catalogo pode ter';

SELECT
  CONCAT('INSERT INTO catalogo_codigo_barras VALUES (NULL, ', catalogo.ID, ', \'', catalogo.codigo_barra, '\');') AS result
FROM
  catalogo
WHERE
  catalogo.ATIVO = 'A'
  AND (
    catalogo.brasindice_id IS NOT NULL
    OR catalogo.simpro_id IS NOT NULL
  )
  AND catalogo.codigo_barra IS NOT NULL;

