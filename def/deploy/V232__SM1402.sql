SELECT
  CATALOGO_ID AS ID,
  CONCAT(principio, '', apresentacao) AS item,
  DATE_FORMAT(DATA_SAIDA_INTERNA, '%d/%m/%Y') AS dataSaida,
  DATE_FORMAT(DATA_VENCIMENTO, '%d/%m/%Y') AS VENCIMENTO,
  QUANTIDADE,
  ROUND(catalogo.valorMedio, 2) AS custo
FROM
  saida_interna
  INNER JOIN catalogo ON saida_interna.CATALOGO_ID = catalogo.ID
WHERE
  DATA_SAIDA_INTERNA BETWEEN '2017-02-01' AND '2017-02-28'
  AND VENCIDO = 1
ORDER BY
  DATA_SAIDA_INTERNA;