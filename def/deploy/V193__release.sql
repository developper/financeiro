ALTER TABLE `exames_laboratoriais_upload`
ADD COLUMN `canceled_by`  int(11) NULL AFTER `upload_id`,
ADD COLUMN `canceled_at`  datetime NULL AFTER `canceled_by`;