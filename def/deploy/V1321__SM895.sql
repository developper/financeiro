INSERT INTO `frequencia` (`frequencia`, `HORA`, `SEMANA`)
VALUES
  ('72/72 hrs', '72', '4'),
  ('A cada 4 dias', '0', '4'),
  ('A cada 5 dias', '0', '5');

INSERT INTO `quadro_feridas_enf_local` (`LOCAL`, `LADO`)
VALUES
  ('TORNOZELO', 'S'),
  ('ORELHA', 'S'),
  ('NARIZ', 'N'),
  ('BOCHECHA', 'S'),
  ('BOCA', 'N'),
  ('PUNHO', 'S'),
  ('FOSSA POPLÍTEA', 'S'),
  ('UMBIGO', 'N'),
  ('INTER-GLÚTEA', 'N'),
  ('INGUINAL', 'N'),
  ('COXA ANTERIOR', 'S'),
  ('COXA POSTERIOR', 'S');