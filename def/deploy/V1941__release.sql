ALTER TABLE `remocoes`
ADD COLUMN `canceled_by`  int(11) NULL AFTER `created_at`,
ADD COLUMN `canceled_at`  datetime NULL AFTER `canceled_by`;