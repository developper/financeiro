CREATE TABLE orcamento_simulacao
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  fatura INT NOT NULL,
  inicio DATE NOT NULL,
  fim DATE NOT NULL,
  created_at DATETIME NOT NULL,
  created_by INT NOT NULL
);

CREATE TABLE orcamento_simulacao_item LIKE custo_item_fatura;

ALTER TABLE orcamento_simulacao_item
  MODIFY COLUMN simulacao_id INT NOT NULL AFTER id;

ALTER TABLE orcamento_simulacao_item ADD preco DOUBLE(10,2) NOT NULL;
ALTER TABLE orcamento_simulacao_item ADD acrescimo_desconto DOUBLE(10,2) NOT NULL;
ALTER TABLE orcamento_simulacao_item ADD qtd INT NOT NULL;
ALTER TABLE orcamento_simulacao_item
  MODIFY COLUMN valorescobranca_id INT(11) AFTER preco,
  MODIFY COLUMN custo DOUBLE(10,2) AFTER preco;

ALTER TABLE orcamento_simulacao_item ADD acrescimo_desconto DOUBLE(10,2) NOT NULL;
ALTER TABLE orcamento_simulacao_item
  MODIFY COLUMN qtd INT(11) NOT NULL AFTER data,
  MODIFY COLUMN valorescobranca_id INT(11) AFTER acrescimo_desconto;

ALTER TABLE orcamento_simulacao_item ADD custo_anterior DOUBLE(10,2) NULL;
ALTER TABLE orcamento_simulacao_item
  MODIFY COLUMN custo_anterior DOUBLE(10,2) AFTER custo;