ALTER TABLE `exames_laboratoriais_paciente`
ADD COLUMN `nome`  varchar(200) NOT NULL AFTER `created_by`,
ADD COLUMN `canceled_at`  datetime NULL AFTER `nome`,
ADD COLUMN `canceled_by`  int(11) NULL AFTER `canceled_at`;