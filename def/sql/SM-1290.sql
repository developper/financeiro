CREATE TABLE `relatorio_nao_conformidade_item_comentario` (
`id`  int(11) NULL AUTO_INCREMENT ,
`relatorio_nao_conformidade_item_id`  int(11) NULL ,
`comentario`  text NULL ,
`created_by`  int(11) NULL ,
`created_at`  datetime NULL ,
`canceled_by`  int(11) NULL ,
`canceled_at`  datetime NULL ,
PRIMARY KEY (`id`)
)
;