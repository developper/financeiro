
-- ----------------------------
-- Table structure for justificativa_itens_negados
-- ----------------------------
DROP TABLE IF EXISTS `justificativa_itens_negados`;
CREATE TABLE `justificativa_itens_negados` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `JUSTIFICATIVA` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of justificativa_itens_negados
-- ----------------------------
INSERT INTO `justificativa_itens_negados` VALUES ('1', 'NÃO AUTORIZADO');
INSERT INTO `justificativa_itens_negados` VALUES ('2', 'SEM COBERTURA');
INSERT INTO `justificativa_itens_negados` VALUES ('3', 'SEM JUSTIFICATIVA TÉCNICA');
INSERT INTO `justificativa_itens_negados` VALUES ('4', 'ÍTEM DUPLICADO');
INSERT INTO `justificativa_itens_negados` VALUES ('5', 'SUSPENSO INTERNAÇÃO');
INSERT INTO `justificativa_itens_negados` VALUES ('6', 'OUTROS');


ALTER TABLE `solicitacoes`
ADD COLUMN `JUSTIFICATIVA_ITENS_NEGADOS_ID`  int NULL AFTER `JUSTIFICATIVA_AVULSO`;