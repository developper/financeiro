SELECT
	c.nome AS Paciente,
	DATE_FORMAT(evoenf.`DATA`, '%d/%m/%Y') AS Data_Realizado,
	DATE_FORMAT(
		evoenf.DATA_EVOLUCAO,
		'%d/%m/%Y'
	) AS Data_do_Relatorio,
	'Ficha de Evolução' AS Relatorio,
	u.nome AS Enfermeira_Responsavel
FROM
	evolucaoenfermagem AS evoenf
INNER JOIN clientes AS c ON (
	c.idClientes = evoenf.PACIENTE_ID
)
INNER JOIN usuarios AS u ON (
	u.idUsuarios = evoenf.USUARIO_ID
)
WHERE
	c.empresa = 1
AND evoenf.DATA_EVOLUCAO BETWEEN '2015-01-01'
AND '2015-02-28'
UNION
	SELECT
		c.nome AS Paciente,
		DATE_FORMAT(
			relproenf.`DATA`,
			'%d/%m/%Y'
		) AS Data_Realizado,
		CONCAT(
			'Início: ',
			DATE_FORMAT(
				relproenf.INICIO,
				'%d/%m/%Y'
			),
			' - Fim: ',
			DATE_FORMAT(relproenf.FIM, '%d/%m/%Y')
		) AS Data_do_Relatorio,
		'Ficha de Prorrogação' AS Relatorio,
		u.nome AS Enfermeira_Responsavel
	FROM
		relatorio_prorrogacao_enf AS relproenf
	INNER JOIN clientes AS c ON (
		c.idClientes = relproenf.PACIENTE_ID
	)
	INNER JOIN usuarios AS u ON (
		u.idUsuarios = relproenf.USUARIO_ID
	)
	WHERE
		c.empresa = 1
	AND relproenf.`DATA` BETWEEN '2015-01-01'
	AND '2015-02-28'
	ORDER BY
		Paciente,
		Data_Realizado,
		Relatorio;