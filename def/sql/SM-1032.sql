-- ----------------------------
-- Table structure for `codigo_empresa_operadora`
-- ----------------------------
DROP TABLE IF EXISTS `codigo_empresa_operadora`;
CREATE TABLE `codigo_empresa_operadora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresas_id` int(11) unsigned NOT NULL,
  `operadoras_id` int(11) NOT NULL,
  `codigo` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresas_id_FK` (`empresas_id`),
  KEY `operadoras_id_FK` (`operadoras_id`),
  CONSTRAINT `empresas_id_FK` FOREIGN KEY (`empresas_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `operadoras_id_FK` FOREIGN KEY (`operadoras_id`) REFERENCES `operadoras` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
