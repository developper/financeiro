CREATE TABLE `tiss_unidade_medida` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_codigo` int(11) NOT NULL,
  `abreviacao` varchar(20) NOT NULL DEFAULT '',
  `descricao` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tiss_conselho_profissional` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_codigo` int(11) NOT NULL,
  `sigla` varchar(10) NOT NULL DEFAULT '',
  `descricao` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tiss_carater_atendimento` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_codigo` int(11) NOT NULL,
  `sigla` char(1) NOT NULL DEFAULT '',
  `descricao` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `tiss_xml` ADD `tiss_versao` ENUM('2.02.03', '03.02.00')  NULL  AFTER `id`;
ALTER TABLE `tiss_fatura` ADD `atendimento_rn` ENUM('S', 'N')  NULL  AFTER `numero_carteira`;
ALTER TABLE `tiss_fatura` ADD `numero_cns` VARCHAR(15)  NULL  DEFAULT NULL  AFTER `nome_beneficiario`;
ALTER TABLE `tiss_fatura` ADD `identificador_beneficiario` VARCHAR(100)  NULL  DEFAULT ''  AFTER `numero_cns`;
ALTER TABLE `tiss_fatura` CHANGE `nome_plano` `nome_plano` VARCHAR(100)  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL  DEFAULT '';
ALTER TABLE `tiss_profissional` ADD `tiss_conselho_profissional_id` INT DEFAULT NULL  AFTER `id`;
ALTER TABLE `unidades_fatura` ADD `tiss_codigo` INT  NOT NULL  AFTER `UNIDADE`;
ALTER TABLE `tiss_fatura` ADD `tiss_carater_atendimento_id` INT  NULL  DEFAULT NULL  AFTER `carater_atendimento`;
ALTER TABLE `tiss_profissional` ADD `cbos` VARCHAR(10)  NULL  DEFAULT NULL  AFTER `especialidade_medica_id`;
ALTER TABLE `tiss_fatura_item` ADD `unidades_fatura_id` INT  NOT NULL  AFTER `tiss_outras_despesas_tipo_id`;
ALTER TABLE `tiss_guia_sadt` CHANGE `numero_guia_principal` `numero_guia_principal` INT(11)  NULL;
ALTER TABLE `tiss_guia_sadt` CHANGE `numero_guia` `numero_guia` INT(11)  NULL;
ALTER TABLE `tiss_guia_sadt` CHANGE `numero_guia_prestador` `numero_guia_prestador` INT(11)  NULL;
ALTER TABLE `estados` ADD `tiss_codigo` INT  NOT NULL  AFTER `NOME`;
ALTER TABLE `tiss_profissional` ADD `conselho_estado_id` INT  NOT NULL  AFTER `numero_conselho`;
ALTER TABLE `tiss_tabela` ADD `descricao_completa` VARCHAR(100)  NULL  DEFAULT NULL  AFTER `tiss_codigo`;
ALTER TABLE `tiss_tabela` ADD `is_active` TINYINT  NOT NULL  DEFAULT '0'  AFTER `descricao_completa`;
ALTER TABLE `tiss_outras_despesas_tipo` ADD `tiss_codigo` INT  NOT NULL  AFTER `descricao`;
ALTER TABLE `tiss_outras_despesas_tipo` ADD `is_active` TINYINT  NOT NULL  DEFAULT '1'  AFTER `tiss_codigo`;
ALTER TABLE `tiss_xml` CHANGE `tiss_guia_sadt_id` `tiss_lote_id` INT(11)  NOT NULL;

UPDATE `tiss_outras_despesas_tipo` SET `tiss_codigo` = '1' WHERE `id` = '1';
UPDATE `tiss_outras_despesas_tipo` SET `tiss_codigo` = '2' WHERE `id` = '2';
UPDATE `tiss_outras_despesas_tipo` SET `tiss_codigo` = '3' WHERE `id` = '3';
UPDATE `tiss_outras_despesas_tipo` SET `tiss_codigo` = '4' WHERE `id` = '4';
UPDATE `tiss_outras_despesas_tipo` SET `tiss_codigo` = '5' WHERE `id` = '5';
UPDATE `tiss_outras_despesas_tipo` SET `tiss_codigo` = '6' WHERE `id` = '6';
UPDATE `tiss_outras_despesas_tipo` SET `tiss_codigo` = '7' WHERE `id` = '7';
UPDATE `tiss_outras_despesas_tipo` SET `tiss_codigo` = '8' WHERE `id` = '8';

UPDATE `tiss_outras_despesas_tipo` SET `is_active` = '0' WHERE `id` = '6';
UPDATE `tiss_outras_despesas_tipo` SET `is_active` = '0' WHERE `id` = '4';

UPDATE `tiss_tabela` SET `descricao_completa` = 'Tabela Própria das Operadoras', `is_active` = '1' WHERE `id` = '1';

UPDATE `tiss_profissional` SET `tiss_conselho_profissional_id` = '6' WHERE `numero_conselho` = '15847';
UPDATE `tiss_profissional` SET `cbos` = '225225' WHERE `id` = '1';
UPDATE `tiss_profissional` SET `conselho_estado_id` = '05' WHERE `id` = '1';

UPDATE `especialidade_medica` SET `tiss_cbos` = '225225' WHERE `ID` = '12';

UPDATE `unidades_fatura` SET tiss_codigo = 8 WHERE ID = 1;
UPDATE `unidades_fatura` SET tiss_codigo = 5 WHERE ID = 2;
UPDATE `unidades_fatura` SET tiss_codigo = 13 WHERE ID = 3;
UPDATE `unidades_fatura` SET tiss_codigo = 1 WHERE ID = 4;
UPDATE `unidades_fatura` SET tiss_codigo = 3 WHERE ID = 5;
UPDATE `unidades_fatura` SET tiss_codigo = 52 WHERE ID = 6;
UPDATE `unidades_fatura` SET tiss_codigo = 12 WHERE ID = 7;
UPDATE `unidades_fatura` SET tiss_codigo = 43 WHERE ID = 8;
UPDATE `unidades_fatura` SET tiss_codigo = 0 WHERE ID = 9;
UPDATE `unidades_fatura` SET tiss_codigo = 0 WHERE ID = 10;
UPDATE `unidades_fatura` SET tiss_codigo = 0 WHERE ID = 11;
UPDATE `unidades_fatura` SET tiss_codigo = 36 WHERE ID = 12;
UPDATE `unidades_fatura` SET tiss_codigo = 26 WHERE ID = 13;
UPDATE `unidades_fatura` SET tiss_codigo = 0 WHERE ID = 14;
UPDATE `unidades_fatura` SET tiss_codigo = 0 WHERE ID = 15;

UPDATE `estados` SET `tiss_codigo` = '12' WHERE `ID` = '01';
UPDATE `estados` SET `tiss_codigo` = '27' WHERE `ID` = '02';
UPDATE `estados` SET `tiss_codigo` = '13' WHERE `ID` = '03';
UPDATE `estados` SET `tiss_codigo` = '16' WHERE `ID` = '04';
UPDATE `estados` SET `tiss_codigo` = '29' WHERE `ID` = '05';
UPDATE `estados` SET `tiss_codigo` = '23' WHERE `ID` = '06';
UPDATE `estados` SET `tiss_codigo` = '53' WHERE `ID` = '07';
UPDATE `estados` SET `tiss_codigo` = '32' WHERE `ID` = '08';
UPDATE `estados` SET `tiss_codigo` = '52' WHERE `ID` = '09';
UPDATE `estados` SET `tiss_codigo` = '21' WHERE `ID` = '10';
UPDATE `estados` SET `tiss_codigo` = '31' WHERE `ID` = '11';
UPDATE `estados` SET `tiss_codigo` = '50' WHERE `ID` = '12';
UPDATE `estados` SET `tiss_codigo` = '51' WHERE `ID` = '13';
UPDATE `estados` SET `tiss_codigo` = '15' WHERE `ID` = '14';
UPDATE `estados` SET `tiss_codigo` = '25' WHERE `ID` = '15';
UPDATE `estados` SET `tiss_codigo` = '26' WHERE `ID` = '16';
UPDATE `estados` SET `tiss_codigo` = '22' WHERE `ID` = '17';
UPDATE `estados` SET `tiss_codigo` = '41' WHERE `ID` = '18';
UPDATE `estados` SET `tiss_codigo` = '33' WHERE `ID` = '19';
UPDATE `estados` SET `tiss_codigo` = '24' WHERE `ID` = '20';
UPDATE `estados` SET `tiss_codigo` = '11' WHERE `ID` = '21';
UPDATE `estados` SET `tiss_codigo` = '14' WHERE `ID` = '22';
UPDATE `estados` SET `tiss_codigo` = '43' WHERE `ID` = '23';
UPDATE `estados` SET `tiss_codigo` = '42' WHERE `ID` = '24';
UPDATE `estados` SET `tiss_codigo` = '28' WHERE `ID` = '25';
UPDATE `estados` SET `tiss_codigo` = '35' WHERE `ID` = '26';
UPDATE `estados` SET `tiss_codigo` = '17' WHERE `ID` = '27';


INSERT INTO `tiss_tabela` (`id`, `descricao`, `tiss_codigo`, `descricao_completa`, `is_active`)
VALUES
	(4, 'Taxas hospitalares, diárias e gases medicinais', 18, 'Taxas hospitalares, diárias e gases medicinais', 1),
	(5, 'Materiais', 19, 'Materiais', 1),
	(6, 'Medicamentos', 20, 'Medicamentos', 1),
	(7, 'Procedimentos e eventos em saúde', 22, 'Procedimentos e eventos em saúde (medicina, odonto e demais áreas de saúde)', 1);

INSERT INTO `tiss_carater_atendimento` (`id`, `tiss_codigo`, `sigla`, `descricao`)
VALUES
	(1, 1, 'E', 'Eletiva'),
	(2, 2, 'U', 'Urgência/Emergência');

INSERT INTO `tiss_conselho_profissional` (`id`, `tiss_codigo`, `sigla`, `descricao`)
VALUES
	(1, 1, 'CRAS', 'Conselho Regional de Assistência Social'),
	(2, 2, 'COREN ', 'Conselho Regional de Enfermagem'),
	(3, 3, 'CRF', 'Conselho Regional de Farmácia'),
	(4, 4, 'CRFA ', 'Conselho Regional de Fonoaudiologia'),
	(5, 5, 'CREFITO', 'Conselho Regional de Fisioterapia e Terapia Ocupacional'),
	(6, 6, 'CRM', 'Conselho Regional de Medicina'),
	(7, 7, 'CRN', 'Conselho Regional de Nutrição'),
	(8, 8, 'CRO', 'Conselho Regional de Odontologia'),
	(9, 9, 'CRP', 'Conselho Regional de Psicologia'),
	(10, 10, 'OUT', 'Outros Conselhos');

INSERT INTO `tiss_unidade_medida` (`id`, `tiss_codigo`, `abreviacao`, `descricao`)
VALUES
	(1, 1, 'AMP', 'Ampola'),
	(2, 2, 'BUI', 'Bilhões de Unidades Internacionais'),
	(3, 3, 'BG', 'Bisnaga'),
	(4, 4, 'BOLS', 'Bolsa'),
	(5, 5, 'CX', 'Caixa'),
	(6, 6, 'CAP', 'Cápsula'),
	(7, 7, 'CARP', 'Carpule'),
	(8, 8, 'COM', 'Comprimido'),
	(9, 9, 'DOSE', 'Dose'),
	(10, 10, 'DRG', 'Drágea'),
	(11, 11, 'ENV', 'Envelope'),
	(12, 12, 'FLAC', 'Flaconete'),
	(13, 13, 'FR', 'Frasco'),
	(14, 14, 'FA', 'Frasco Ampola'),
	(15, 15, 'GAL', 'Galão'),
	(16, 16, 'GLOB', 'Glóbulo'),
	(17, 17, 'GTS', 'Gotas'),
	(18, 18, 'G', 'Grama'),
	(19, 19, 'L', 'Litro'),
	(20, 20, 'MCG', 'Microgramas'),
	(21, 21, 'MUI', 'Milhões de Unidades Internacionais'),
	(22, 22, 'MG', 'Miligrama'),
	(23, 23, 'ML', 'Milímetro'),
	(24, 24, 'OVL', 'Óvulo'),
	(25, 25, 'PAS', 'Pastilha'),
	(26, 26, 'LT', 'Lata'),
	(27, 27, 'PER', 'Pérola'),
	(28, 28, 'PIL', 'Pílula'),
	(29, 29, 'PT', 'Pote'),
	(30, 30, 'KG', 'Quilograma'),
	(31, 31, 'SER', 'Seringa'),
	(32, 32, 'SUP', 'Supositório'),
	(33, 33, 'TABLE', 'Tablete '),
	(34, 34, 'TUB', 'Tubete'),
	(35, 35, 'TB', 'Tubo'),
	(36, 36, 'UN', 'Unidade'),
	(37, 37, 'UI', 'Unidade Internacional '),
	(38, 38, 'CM', 'Centímetro'),
	(39, 39, 'CONJ', 'Conjunto'),
	(40, 40, 'KIT', 'Kit'),
	(41, 41, 'MÇ', 'Maço'),
	(42, 42, 'M', 'Metro'),
	(43, 43, 'PC', 'Pacote'),
	(44, 44, 'PÇ', 'Peça'),
	(45, 45, 'RL', 'Rolo'),
	(46, 46, 'GY', 'Gray'),
	(47, 47, 'CGY', 'Centgray'),
	(48, 48, 'PAR', 'Par'),
	(49, 49, 'ADES', 'Adesivo Transdérmico'),
	(50, 50, 'COM EFEV', 'Comprimido Efervecente'),
	(51, 51, 'COM MST', 'Comprimido Mastigável'),
	(52, 52, 'SACHE', 'Sache');