-- Mês de Junho
SET @mes = 06;     
 -- Esse é o ID de Dr. Augusto Motta   
SET @medico = 21;  

-- Essa Busca retorna as Avalaições Iniciais 
-- De um determinado médico em um determinado mês

SELECT 
  UPPER(CLI.nome) as PACIENTE,
  DATE_FORMAT(CAP.data,'%d/%m/%Y') as 'DATA DA AVALIAÇÃO'
FROM
  capmedica AS CAP INNER JOIN
  clientes as CLI ON (CAP.paciente = CLI.idClientes)
WHERE
  CAP.usuario = @medico AND
  MONTH(CAP.data)= @mes 


-- BUSCA RETORNA AS PRESCRIÇÕES DE AVALIAÇÃO
-- NO MÊS SOLICITADO E PARA UM DETERMINADO MÉDICO  

SELECT 
  CLI.nome, 
   DATE_FORMAT(PRESC.data,'%d/%m/%Y') as 'DATA DA AVALIAÇÃO'
FROM
  prescricoes as PRESC INNER JOIN
  clientes as CLI ON (PRESC.paciente = CLI.idClientes)
WHERE
  PRESC.criador = @medico AND
  MONTH(data) = @mes AND
  PRESC.Carater in (7,4)