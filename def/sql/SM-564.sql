SELECT
itens_prescricao.CATALOGO_ID,
itens_prescricao.nome
FROM
prescricoes
INNER JOIN itens_prescricao ON itens_prescricao.idPrescricao = prescricoes.id
INNER JOIN clientes ON prescricoes.paciente = clientes.idClientes
WHERE
clientes.empresa = 1 AND
prescricoes.inicio BETWEEN '2014-07-01' AND '2014-11-11' AND
itens_prescricao.tipo IN (3, 4, 5, 6, 7, 8, 9, 10, 11,13) AND
prescricoes.paciente not in (142,220,69,263,112,101,67)
GROUP BY
itens_prescricao.tipo,
itens_prescricao.CATALOGO_ID

