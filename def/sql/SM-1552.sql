-- saida para pacientes de uma Ur em um determinado período.

set @inicio = '2017-04-01';
set @fim = '2017-07-12';
set @ur = '5';

SELECT
	p.nome AS Paciente,
	(
		CASE s.tipo
		WHEN 0 THEN
			'Medicamento'
		WHEN 1 THEN
			'Material'
		WHEN 3 THEN
			'Dieta'
		END
	)AS Tipo,
	concat(
		cb.principio,
		' - ',
		cb.apresentacao,
		' LAB: ',
		cb.lab_desc
	)AS Item,
	s.LOTE,
	s.quantidade AS Quantidade,
	DATE_FORMAT(s. DATA, '%d/%m/%Y') AS Data
FROM
	saida AS s
INNER JOIN clientes AS p ON(p.idClientes = s.idCliente)
INNER JOIN catalogo AS cb ON(cb.ID = s.CATALOGO_ID)
WHERE
	p.empresa = @ur
AND s.tipo IN(0, 1, 3)
AND s. DATA BETWEEN @inicio
AND @fim
UNION ALL
	SELECT
		p.nome AS Paciente,

		'Equipamento' AS Tipo,
		cb.item AS Item,
		s.LOTE,
		s.quantidade AS Quantidade,
		DATE_FORMAT(s. DATA, '%d/%m/%Y') AS Data
	FROM
		saida AS s
	INNER JOIN clientes AS p ON(p.idClientes = s.idCliente)
	INNER JOIN cobrancaplanos AS cb ON(cb.id = s.CATALOGO_ID)
	WHERE
		p.empresa = @ur
	AND s.tipo = 5
	AND s. DATA BETWEEN @inicio
	AND @fim
	ORDER BY
		`Item` ASC