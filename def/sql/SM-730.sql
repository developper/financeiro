INSERT INTO `modalidade_home_care` (`modalidade`) VALUES ('Internação Domiciliar Treinamento de Cuidador');
INSERT INTO `modalidade_home_care` (`modalidade`) VALUES ('Internação Domiciliar Treinamento de Sonda Vesical');
INSERT INTO `modalidade_home_care` (`modalidade`) VALUES ('Internação Domiciliar Treinamento de Sonda de Alívio');

CREATE TABLE `historico_modalidade_paciente` (
`id`  int(11) NOT NULL ,
`paciente`  int(11) NOT NULL ,
`modalidade`  int(11) NOT NULL ,
`data_mudanca`  int(11) NOT NULL ,
PRIMARY KEY (`id`)
);

ALTER TABLE `historico_modalidade_paciente`
MODIFY COLUMN `id`  int(11) NOT NULL AUTO_INCREMENT FIRST,
MODIFY COLUMN `data_mudanca`  date NOT NULL AFTER `modalidade`,
ADD COLUMN `data_sistema`  datetime NOT NULL DEFAULT NOW() AFTER `data_mudanca`,
ADD COLUMN `usuario`  int(11) NOT NULL AFTER `data_sistema`;