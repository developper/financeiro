ALTER TABLE `avaliacaoenfermagem`
ADD COLUMN `medico_multidisciplinar`  enum('3','2','1','-1') NULL DEFAULT '-1' COMMENT '-1 Selecine; 1- smenal, 2 quizenal, 3 mensal' ,
ADD COLUMN `enfermagem_multidisciplinar`  enum('4','3','2','1','-1') NULL DEFAULT '-1' COMMENT '-1 selecione; 1 semanal; 2 Quinzenal; 3 mensal; 4 2x semana',
ADD COLUMN `fono_multidisciplinar`  enum('S','N','-1') NULL DEFAULT '-1',
ADD COLUMN `fissio_motora_multidisciplinar`  enum('S','N','-1') NULL DEFAULT '-1',
ADD COLUMN `fissio_respiratoria_multidisciplinar`  enum('N','S','-1') NULL DEFAULT '-1',
ADD COLUMN `nutricao_multidisciplinar`  enum('S','N','-1') NULL DEFAULT '-1';