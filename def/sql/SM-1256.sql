SELECT
  clientes.nome AS paciente,
  empresas.nome AS ur,
  SUM(IF(EXTRACT(MONTH FROM saida.data) = 9, saida.quantidade, 0)) AS qtdSetembro,
  SUM(IF(EXTRACT(MONTH FROM saida.data) = 10, saida.quantidade, 0)) AS qtdOutubro,
  SUM(IF(EXTRACT(MONTH FROM saida.data) = 11, saida.quantidade, 0)) AS qtdNovembro
FROM
  saida
  INNER JOIN catalogo ON saida.CATALOGO_ID = catalogo.ID
  INNER JOIN clientes ON saida.idCliente = clientes.idClientes
  INNER JOIN empresas ON clientes.empresa = empresas.id
WHERE
  saida.CATALOGO_ID = 13622
  AND saida.UR = 0
  AND saida.data BETWEEN '2016-09-01' AND '2016-11-01'
GROUP BY idCliente
ORDER BY paciente;