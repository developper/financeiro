SELECT
	f.idFornecedores,
	f.razaoSocial
FROM
	`fornecedores` AS f
WHERE
	EXISTS (
		SELECT
			n.codFornecedor
		FROM
			notas AS n
		WHERE
			n.codFornecedor = f.idFornecedores
		AND
			n.dataEntrada BETWEEN '2014-01-01' AND NOW()
	);