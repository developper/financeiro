SET @DATA_INICIO = '2014-01-01';
SET @DATA_FIM = '2014-07-31'; 

SELECT 
	CONCAT( C.principio, '-', C.apresentacao ) AS ITEM,
	IF( S.tipo =0, 'Medicamento', IF( S.tipo =1, 'Material', IF( S.tipo =3, 'Dieta', '<desconhecido>' ) ) ) AS TIPO, 
	SUM( S.quantidade ) AS Quantidade, 
	TRUNCATE(C.valor,2) AS VALOR_UNITARIO,
	SUM( S.quantidade ) * TRUNCATE(C.valor,2) AS VALOR_TOTAL,
	DATE_FORMAT( S.`DATA_SISTEMA` , '%m' ) AS MES,
	E.nome AS EMPRESA	
FROM 
	saida AS S INNER JOIN 
	empresas AS E ON ( S.`UR` = E.id ) INNER JOIN 
	catalogo AS C ON ( S.CATALOGO_ID = C.ID )
WHERE 
	S.tipo IN ( 0, 1, 3 ) AND 
	S.UR >0 AND 
	S.idUsuario NOT IN( 60, 48, 122, 225, 7 ) AND 
	S.DATA_SISTEMA BETWEEN @DATA_INICIO AND @DATA_FIM
GROUP BY
	MES,C.ID
ORDER BY 
	`VALOR_TOTAL`  DESC