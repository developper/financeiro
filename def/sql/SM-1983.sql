CREATE TABLE fornecedor_equipamento_email  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fornecedor_id` int(11) NULL,
  `email` varchar(255) NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `fornecedor_equipamento_email`
ADD COLUMN `created_at` datetime NULL AFTER `email`,
ADD COLUMN `created_by` int(11) NULL AFTER `created_at`,
ADD COLUMN `deleted_at` datetime NULL AFTER `created_by`,
ADD COLUMN `deleted_by` int(11) NULL AFTER `canceled_at`;