
INSERT INTO `area_sistema`(`nome`, `descricao`, `ordem`, `id_pai`) VALUES ('financeiro_orcamento_gerencial', 'Orçamento Gerencial', 8, 69);
INSERT INTO `area_permissoes`(`area_id`, `ordem`, `acao`, `descricao`) VALUES (111, 1, 'financeiro_orcamento_gerencial_criar', 'Criar');
INSERT INTO `area_permissoes`(`area_id`, `ordem`, `acao`, `descricao`) VALUES (111, 2, 'financeiro_orcamento_gerencial_editar', 'Editar');
INSERT INTO `area_permissoes`(`area_id`, `ordem`, `acao`, `descricao`) VALUES (111, 3, 'financeiro_orcamento_gerencial_cancelar', 'Cancelar');
create table orcamento_gerencial_topicos
(
    id int auto_increment,
    letra char not null,
    descricao varchar(255) not null,
    anotacao text not null,
    ordem int null,
    id_pai int null,
    cor varchar(30) null,
    formula varchar(120) null,
    constraint orcamento_gerencial_topicos_pk
        primary key (id)
);
INSERT INTO `orcamento_gerencial_topicos` VALUES (1, 'A', 'Receita Operacional Bruta', 'Base do Orçamento (Subtraídos os impostos sobre faturamento)', 1, 0, 'blue', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (2, '', 'Receita Operacional Bruta', '', 1, 1, 'blue', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (3, '', 'Entradas Nao Operacionais', '', 2, 1, 'blue', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (4, 'B', 'Impostos sobre Faturamento', '', 2, 0, 'pink', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (5, '', 'Tributos sobre faturamento', '', 1, 4, '', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (6, '', 'Imposto de Renda e CSLL', '', 2, 4, '', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (7, '', 'Retencoes', '', 3, 4, '', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (8, 'C', 'Receita Operacional Liquida', 'Base do Orçamento (Receita Operacional Bruta - Impostos sobre Faturamento)', 3, 0, 'green', 'A - B');
INSERT INTO `orcamento_gerencial_topicos` VALUES (9, 'D', 'Custos dos Serviços', '', 4, 0, 'pink', '%D / C');
INSERT INTO `orcamento_gerencial_topicos` VALUES (10, '', 'Custos Variáveis', 'Orçamento Percentual da Receita Operacional Líquida (C)', 1, 9, 'pink', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (11, '', 'Custo Pessoal (Area Médica)', '', 1, 10, 'red', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (12, '', 'Custo Pessoal (Serviços Prestados)', '', 2, 10, 'red', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (13, '', 'Material Aplicado', '', 3, 10, 'red', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (14, '', 'Custos Fixos', 'Orçamento Fixo em valor', 2, 9, 'pink', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (15, '', 'Custo Pessoal (Area Médica)', '', 1, 14, 'red', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (16, '', 'Custo Pessoal (Serviços Prestados)', '', 2, 14, 'red', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (17, '', 'Outros Custos Variaveis', '', 3, 14, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (18, 'E', 'Despesas Fixas', 'Orçamento Fixo em valor', 5, 0, 'pink', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (19, '', 'Pessoal', '', 1, 18, 'red', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (20, '', 'Adiantamentos', '', 2, 18, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (21, '', 'Adiantamentos, Emprestimos e Mútuos', '', 3, 18, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (22, '', 'Ocupacao', '', 4, 18, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (23, '', 'Viagens e Estadas', '', 5, 18, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (24, '', 'Manutencao', '', 6, 18, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (25, '', 'Locomocao', '', 7, 18, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (26, '', 'Materiais', '', 8, 18, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (27, '', 'Despesas Gerais', '', 9, 18, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (28, '', 'Serviços de Consultoria', '', 10, 18, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (29, '', 'Serviços de Terceiros', '', 11, 18, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (30, '', 'Bancarias', '', 12, 18, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (31, '', 'Impostos, Taxas e Contribuicoes', '', 13, 18, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (32, '', 'Multas de Mora', '', 14, 18, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (33, '', 'Marketing e Propaganda', '', 15, 18, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (34, '', 'Provisões de Perda', '', 16, 18, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (35, '', 'Amortizacao de Obrigacoes', '', 17, 18, 'red', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (36, '', 'Consorcios', '', 1, 35, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (37, '', 'Imobilizacoes', '', 2, 35, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (38, '', 'Projetos (Centro de Resultado)', 'Detalhar somatório dos Centro de Resultado dos Projetos', 17, 19, 'pink', 'D + E');
INSERT INTO `orcamento_gerencial_topicos` VALUES (39, 'G', 'EBITDA', '14% da Receita Operacional Líquida', 6, 0, 'green', 'C - (D + E)');
INSERT INTO `orcamento_gerencial_topicos` VALUES (40, 'H', 'Amortizacao e Depreciacao', 'Orçamento Percentual do Receita Operacional Líquida (C)', 7, 0, 'pink', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (41, 'I', 'Depesas Financeiras (Juros)', 'Orçamento Percentual do Receita Operacional Líquida (C)', 8, 0, 'pink', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (42, '', 'Juros Sobre Empréstimos e Financiamentos.', '', 1, 41, 'red', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (43, '', 'Outros', '', 2, 41, 'bold', '');
INSERT INTO `orcamento_gerencial_topicos` VALUES (44, 'J', 'Lucro Bruto', '', 9, 0, 'blue', 'G - H - I');
INSERT INTO `orcamento_gerencial_topicos` VALUES (45, 'K', 'Imposto de Renda', '(15% sobre Lucro Bruto) + (10% sobre o excedende a partir de 240mil)', 10, 0, 'pink', '(J X 15%) + (( J -24k) X 10%)');
INSERT INTO `orcamento_gerencial_topicos` VALUES (46, 'L', 'Lucro Bruto', '8% da Receita Operacional Líquida', 11, 0, 'green', 'Lucro Liquido');
INSERT INTO `orcamento_gerencial_topicos` VALUES (47, '', 'Lucros', '', 16, 19, 'bold', '');



create table orcamento_gerencial_versao
(
    id int auto_increment,
    competencia_inicio date not null,
    competencia_fim date not null,
    observacao text null,
    criado_por int not null,
    criado_em date not null,
    constraint orcamento_gerencial_versao_pk
        primary key (id)
);

INSERT INTO orcamento_gerencial_versao (id, competencia_inicio, competencia_fim, observacao, criado_por, criado_em) VALUES (1, '2020-01-01', '2020-12-31', null, 1, '2020-01-27');


create table orcamento_gerencial_topicos_valores
(
    id int auto_increment,
    orcamento_gerencial_topicos_id int not null,
    versao_id int not null,
    aliquota decimal(10,5) not null,
    valor_mensal decimal(20,2) not null,
    valor_anual decimal(10,2) not null,
    constraint orcamento_gerencial_topicos_valores_pk
        primary key (id)
);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (1, 1, 1, 100.00000, 6791666.67, 81500000.00);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (2, 2, 1, 100.00000, 6791666.67, 81500000.00);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (3, 3, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (4, 4, 1, 6.00000, 407500.00, 4890000.00);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (5, 5, 1, 2.65000, 179979.17, 2159750.00);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (6, 6, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (7, 7, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (8, 8, 1, 94.00000, 6384166.67, 76610000.00);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (9, 9, 1, 54.85000, 3501975.47, 42023705.62);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (10, 10, 1, 49.19000, 3140439.01, 37685268.17);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (11, 11, 1, 30.16000, 2157219.59, 25886635.03);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (12, 12, 1, 1.89000, 135051.19, 1620614.24);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (13, 13, 1, 17.14000, 1225834.26, 14710011.11);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (14, 14, 1, 5.66000, 361536.45, 4338437.45);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (15, 15, 1, 5.02000, 358706.25, 4304474.95);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (16, 16, 1, 0.23000, 16275.63, 195307.59);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (17, 17, 1, 0.42000, 30032.58, 360391.01);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (18, 18, 1, 25.15000, 1605357.86, 19264294.38);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (19, 19, 1, 21.61000, 1545677.06, 18548124.78);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (20, 20, 1, 0.07000, 4744.01, 56928.13);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (21, 21, 1, 2.27000, 145229.15, 1742749.77);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (22, 47, 1, 1.14000, 73010.29, 876123.47);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (23, 22, 1, 1.44000, 102832.17, 1233986.08);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (24, 23, 1, 0.06000, 4608.68, 55304.11);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (25, 24, 1, 0.90000, 64535.88, 774430.54);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (26, 25, 1, 0.06000, 4038.54, 48462.49);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (27, 26, 1, 0.11000, 7873.47, 94481.67);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (28, 27, 1, 1.37000, 98094.90, 1177138.83);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (29, 28, 1, 2.04000, 145550.65, 1746607.76);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (30, 29, 1, 0.16000, 11408.53, 136902.33);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (31, 30, 1, 0.10000, 7071.74, 84860.93);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (32, 31, 1, 0.54000, 38331.14, 459973.74);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (33, 32, 1, 0.02000, 1690.67, 20288.00);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (34, 33, 1, 0.44000, 28298.22, 339578.59);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (35, 34, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (36, 35, 1, 7.23000, 461723.98, 5540687.75);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (37, 36, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (38, 37, 1, 0.04000, 2698.97, 32387.62);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (39, 38, 1, 0.88000, 44944.53, 539334.40);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (40, 39, 1, 14.00000, 893783.33, 10725400.00);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (41, 40, 1, 0.20000, 12768.33, 153220.00);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (42, 41, 1, 2.00000, 127683.33, 1532200.00);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (43, 42, 1, 0.65000, 41411.82, 496941.82);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (44, 43, 1, 0.96000, 61283.62, 735403.42);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (45, 44, 1, 0.00000, 708387.13, 8500645.60);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (46, 45, 1, 2.74000, 175096.78, 2101161.40);
INSERT INTO `orcamento_gerencial_topicos_valores` VALUES (47, 46, 1, 8.35000, 533290.35, 6399484.20);
DROP TABLE IF EXISTS `orcamento_gerencial_plano_contas_valores`;
CREATE TABLE `orcamento_gerencial_plano_contas_valores`  (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `orcamento_gerencial_topicos_id` int(11) NOT NULL,
 `plano_contas_id` int(11) NOT NULL,
 `versao_id` int(11) NOT NULL,
 `aliquota` decimal(10, 5) NOT NULL,
 `valor_mensal` decimal(20, 2) NOT NULL,
 `valor_anual` decimal(20, 2) NOT NULL,
 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 177 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (1, 2, 1, 1, 29.76200, 2021329.37, 24255952.38);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (2, 2, 2, 1, 27.38100, 1859623.02, 22315476.19);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (3, 2, 3, 1, 16.66700, 1131944.44, 13583333.33);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (4, 2, 4, 1, 26.19100, 1778769.84, 21345238.10);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (5, 2, 19, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (6, 3, 6, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (7, 3, 8, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (8, 3, 9, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (9, 3, 10, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (10, 3, 11, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (11, 3, 12, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (12, 3, 13, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (13, 3, 15, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (14, 3, 16, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (15, 3, 17, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (16, 3, 14, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (17, 5, 0, 1, 2.00000, 135833.33, 1630000.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (18, 5, 149, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (19, 5, 150, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (20, 5, 151, 1, 0.65000, 44145.83, 529750.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (21, 6, 152, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (22, 6, 147, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (23, 7, 143, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (24, 7, 144, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (25, 7, 145, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (26, 7, 146, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (27, 7, 148, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (28, 7, 155, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (29, 7, 156, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (30, 11, 25, 1, 1.58000, 113000.33, 1356003.97);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (31, 11, 26, 1, 0.09000, 6432.24, 77186.92);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (32, 11, 27, 1, 0.11200, 8020.83, 96250.01);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (33, 11, 30, 1, 6.22500, 445178.42, 5342140.99);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (34, 11, 31, 1, 1.20500, 86210.52, 1034526.20);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (35, 11, 32, 1, 0.10200, 7268.67, 87224.01);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (36, 11, 33, 1, 0.07800, 5599.73, 67196.76);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (37, 11, 35, 1, 0.00400, 281.71, 3380.46);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (38, 11, 36, 1, 1.44200, 103145.20, 1237742.41);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (39, 11, 37, 1, 19.32500, 1382081.94, 16584983.28);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (40, 12, 48, 1, 0.05800, 4128.35, 49540.24);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (41, 12, 49, 1, 0.01500, 1056.21, 12674.48);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (42, 12, 50, 1, 0.05400, 3873.75, 46485.04);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (43, 12, 51, 1, 0.05500, 3955.42, 47465.08);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (44, 12, 52, 1, 0.69200, 49517.20, 594206.43);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (45, 12, 54, 1, 0.88700, 63446.21, 761354.52);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (46, 12, 55, 1, 0.12700, 9074.04, 108888.45);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (47, 13, 38, 1, 3.07100, 219629.68, 2635556.18);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (48, 13, 39, 1, 6.32700, 452529.76, 5430357.16);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (49, 13, 40, 1, 1.91900, 137250.22, 1647002.66);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (50, 13, 41, 1, 3.33200, 238295.99, 2859551.83);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (51, 13, 42, 1, 0.95900, 68551.01, 822612.14);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (52, 13, 43, 1, 0.17300, 12350.58, 148207.01);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (53, 13, 44, 1, 0.05900, 4210.66, 50527.88);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (54, 13, 45, 1, 0.03100, 2181.22, 26174.62);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (55, 13, 56, 1, 1.27000, 90835.14, 1090021.63);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (56, 15, 21, 1, 0.40600, 29045.76, 348549.11);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (57, 15, 22, 1, 0.02900, 2070.29, 24843.48);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (58, 15, 23, 1, 2.60400, 186235.00, 2234819.95);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (59, 15, 24, 1, 0.18300, 13083.58, 157003.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (60, 15, 28, 1, 1.61900, 115756.78, 1389081.32);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (61, 15, 29, 1, 0.05300, 3819.75, 45837.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (62, 15, 34, 1, 0.12200, 8695.09, 104341.09);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (63, 16, 46, 1, 0.19200, 13763.80, 165165.60);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (64, 16, 47, 1, 0.03500, 2511.83, 30141.99);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (65, 17, 57, 1, 0.37000, 26478.15, 317737.76);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (66, 17, 53, 1, 0.05000, 3554.44, 42653.25);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (67, 19, 107, 1, 9.20300, 658184.18, 7898210.17);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (68, 19, 108, 1, 3.15700, 225785.51, 2709426.12);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (69, 19, 109, 1, 0.72100, 51588.44, 619061.31);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (70, 19, 110, 1, 0.03900, 2803.64, 33643.64);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (71, 19, 0, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (72, 19, 112, 1, 0.89400, 63933.58, 767202.93);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (73, 19, 81, 1, 0.56600, 40475.11, 485701.32);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (74, 19, 0, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (75, 19, 0, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (76, 19, 114, 1, 2.99700, 214304.85, 2571658.16);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (77, 19, 115, 1, 1.01800, 72805.24, 873662.88);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (78, 19, 116, 1, 0.24400, 17476.50, 209718.02);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (79, 19, 117, 1, 1.25500, 89745.07, 1076940.84);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (80, 19, 118, 1, 0.75700, 54168.44, 650021.28);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (81, 19, 119, 1, 0.34300, 24509.96, 294119.57);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (82, 19, 0, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (83, 19, 120, 1, 0.02500, 1755.64, 21067.65);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (84, 19, 121, 1, 0.27000, 19331.45, 231977.40);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (85, 19, 113, 1, 0.01000, 698.06, 8376.77);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (86, 19, 122, 1, 0.07200, 5171.15, 62053.82);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (87, 19, 123, 1, 0.01600, 1148.04, 13776.47);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (88, 19, 124, 1, 0.02100, 1527.42, 18329.10);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (89, 19, 126, 1, 0.00400, 264.78, 3177.32);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (90, 19, 0, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (91, 19, 0, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (92, 20, 137, 1, 0.00600, 367.98, 4415.76);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (93, 20, 92, 1, 0.02200, 1408.79, 16905.50);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (94, 20, 138, 1, 0.03900, 2457.97, 29495.67);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (95, 21, 0, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (96, 21, 136, 1, 0.06400, 4072.41, 48868.92);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (97, 21, 142, 1, 2.21100, 141156.74, 1693880.85);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (98, 47, 163, 1, 1.14400, 73010.29, 876123.47);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (99, 22, 59, 1, 0.35900, 25678.82, 308145.79);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (100, 22, 60, 1, 0.08100, 5784.76, 69417.18);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (101, 22, 61, 1, 0.41700, 29792.35, 357508.23);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (102, 22, 62, 1, 0.07200, 5175.61, 62107.29);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (103, 22, 105, 1, 0.50900, 36400.63, 436807.60);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (104, 22, 58, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (105, 23, 63, 1, 0.02000, 1434.99, 17219.94);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (106, 23, 64, 1, 0.04400, 3173.68, 38084.18);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (107, 24, 66, 1, 0.25800, 18449.27, 221391.27);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (108, 24, 67, 1, 0.10700, 7614.70, 91376.38);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (109, 24, 68, 1, 0.50400, 36062.99, 432755.91);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (110, 24, 69, 1, 0.03400, 2408.92, 28906.99);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (111, 25, 89, 1, 0.02000, 1399.19, 16790.28);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (112, 25, 125, 1, 0.01600, 1146.98, 13763.79);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (113, 25, 90, 1, 0.02100, 1492.37, 17908.41);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (114, 26, 83, 1, 0.08500, 6089.59, 73075.04);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (115, 26, 0, 1, 0.00000, 5.89, 70.64);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (116, 26, 85, 1, 0.01700, 1183.28, 14199.33);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (117, 26, 86, 1, 0.00800, 594.72, 7136.66);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (118, 27, 70, 1, 0.10400, 7443.47, 89321.70);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (119, 27, 71, 1, 0.17200, 12266.24, 147194.91);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (120, 27, 72, 1, 0.19600, 14014.90, 168178.85);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (121, 27, 73, 1, 0.23700, 16938.87, 203266.42);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (122, 27, 74, 1, 0.07300, 5202.67, 62431.98);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (123, 27, 75, 1, 0.03900, 2811.83, 33741.99);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (124, 27, 76, 1, 0.02200, 1545.45, 18545.39);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (125, 27, 0, 1, 0.00900, 624.70, 7496.40);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (126, 27, 78, 1, 0.04100, 2941.38, 35296.60);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (127, 27, 79, 1, 0.04300, 3064.03, 36768.36);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (128, 27, 0, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (129, 27, 80, 1, 0.02500, 1814.49, 21773.89);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (130, 27, 81, 1, 0.31000, 22132.71, 265592.55);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (131, 27, 82, 1, 0.00500, 370.19, 4442.27);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (132, 27, 106, 1, 0.09700, 6923.96, 83087.51);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (133, 27, 0, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (134, 28, 93, 1, 0.21800, 15558.23, 186698.74);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (135, 28, 94, 1, 0.85800, 61356.75, 736281.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (136, 28, 95, 1, 0.11600, 8273.81, 99285.78);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (137, 28, 96, 1, 0.49500, 35390.92, 424691.05);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (138, 28, 97, 1, 0.01200, 848.73, 10184.73);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (139, 28, 98, 1, 0.18900, 13529.63, 162355.55);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (140, 28, 99, 1, 0.14800, 10592.58, 127110.93);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (141, 29, 100, 1, 0.11400, 8165.57, 97986.88);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (142, 29, 101, 1, 0.04500, 3242.95, 38915.45);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (143, 30, 87, 1, 0.09900, 7070.30, 84843.59);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (144, 30, 88, 1, 0.00000, 1.44, 17.34);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (145, 30, 0, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (146, 31, 127, 1, 0.20200, 14460.76, 173529.15);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (147, 31, 128, 1, 0.02700, 1906.89, 22882.62);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (148, 31, 129, 1, 0.06400, 4597.77, 55173.26);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (149, 31, 130, 1, 0.00700, 508.36, 6100.31);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (150, 31, 131, 1, 0.23600, 16857.37, 202288.40);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (151, 32, 91, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (152, 32, 132, 1, 0.02400, 1690.67, 20288.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (153, 33, 102, 1, 0.43700, 27896.64, 334759.64);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (154, 33, 103, 1, 0.00600, 449.87, 5398.47);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (155, 33, 104, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (156, 34, 168, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (157, 35, 139, 1, 4.32100, 275884.38, 3310612.55);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (158, 35, 0, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (159, 35, 140, 1, 3.19500, 203987.00, 2447844.03);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (160, 36, 162, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (161, 37, 161, 1, 0.01100, 718.51, 8622.07);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (162, 37, 0, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (163, 37, 0, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (164, 37, 0, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (165, 37, 0, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (166, 37, 157, 1, 0.00700, 457.36, 5488.30);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (167, 37, 158, 1, 0.01000, 655.19, 7862.24);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (168, 37, 159, 1, 0.00300, 218.81, 2625.76);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (169, 37, 160, 1, 0.01000, 649.11, 7789.26);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (170, 42, 133, 1, 0.20700, 13229.45, 158753.37);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (171, 42, 135, 1, 0.06400, 4051.48, 48617.80);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (172, 42, 141, 1, 0.37800, 24130.89, 289570.65);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (173, 42, 0, 1, 0.00000, 0.00, 0.00);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (174, 43, 164, 1, 0.02400, 1534.53, 18414.34);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (175, 43, 165, 1, 0.04300, 2713.53, 32562.31);
INSERT INTO `orcamento_gerencial_plano_contas_valores` VALUES (176, 43, 0, 1, 0.97800, 62462.62, 749551.40);
