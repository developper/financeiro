ALTER TABLE `clientes`
ADD COLUMN `cpf`  bigint(11) NULL DEFAULT NULL ,
ADD COLUMN `cpf_responsavel`  bigint(11) NULL DEFAULT NULL,
ADD COLUMN `nascimento_responsavel`  date NULL ,
ADD COLUMN `nascimento_cuidador`  date ;