INSERT INTO planserv_tipo_exsudato_ferida VALUES (NULL, 'Ausente');

CREATE TABLE planserv_plano_feridas_incontinencia
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  plano_feridas_id INT NOT NULL,
  incontinencia_id INT NOT NULL
);

SELECT
  CONCAT('INSERT INTO planserv_plano_feridas_incontinencia VALUES (NULL, ', id, ', ', incontinencia, ');') AS queries
FROM
  planserv_plano_feridas_historico_paciente;

ALTER TABLE planserv_plano_feridas_historico_paciente DROP COLUMN incontinencia;