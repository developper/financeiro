
-- Relat�rio com as feridas das ultimas evolu��es dos pacientes ativos da UR Feira de Santana
SELECT
clientes.nome as Pacientes,
usuarios.nome as Enfermeiro,
evolucaoenfermagem.DATA_EVOLUCAO,
quadro_feridas_enf_local.`LOCAL`,
(CASE quadro_feridas_enfermagem.LADO
WHEN '1' THEN 'DIREITO'
WHEN '2' THEN 'ESQUERDO' END) as LADO,
quadro_feridas_enf_tipo.TIPO,
quadro_feridas_enf_lesao.TIPO as LESAO,
quadro_feridas_enf_tamanho.TAMANHO,
quadro_feridas_enf_profundidade.PROFUNDIDADE,
quadro_feridas_enf_tecido.TECIDO,
quadro_feridas_enf_humidade.HUMIDADE,
quadro_feridas_enf_exsudato.EXSUDATO,
quadro_feridas_enf_grau.GRAU,
quadro_feridas_enf_contaminacao.CONTAMINACAO,
quadro_feridas_enf_pele.PELE,
quadro_feridas_enf_tempo.TEMPO,
quadro_feridas_enf_periodo.PERIODO_TROCA,
quadro_feridas_enf_status.status_ferida,
(CASE quadro_feridas_enfermagem.ODOR
WHEN '1' THEN 'N�o'
WHEN '2' THEN 'Fetido' END) as ODOR
FROM
evolucaoenfermagem
INNER JOIN quadro_feridas_enfermagem ON evolucaoenfermagem.ID = quadro_feridas_enfermagem.EVOLUCAO_ENF_ID
INNER JOIN quadro_feridas_enf_local ON quadro_feridas_enfermagem.`LOCAL` = quadro_feridas_enf_local.ID
INNER JOIN quadro_feridas_enf_tipo ON quadro_feridas_enfermagem.TIPO = quadro_feridas_enf_tipo.ID
INNER JOIN quadro_feridas_enf_lesao ON quadro_feridas_enfermagem.COBERTURA = quadro_feridas_enf_lesao.ID
INNER JOIN quadro_feridas_enf_tamanho ON quadro_feridas_enfermagem.TAMANHO = quadro_feridas_enf_tamanho.ID
INNER JOIN quadro_feridas_enf_profundidade ON quadro_feridas_enfermagem.PROFUNDIDADE = quadro_feridas_enf_profundidade.ID
INNER JOIN quadro_feridas_enf_tecido ON quadro_feridas_enfermagem.TECIDO = quadro_feridas_enf_tecido.ID
INNER JOIN quadro_feridas_enf_humidade ON quadro_feridas_enfermagem.HUMIDADE = quadro_feridas_enf_humidade.ID
INNER JOIN quadro_feridas_enf_exsudato ON quadro_feridas_enfermagem.EXSUDATO = quadro_feridas_enf_exsudato.ID
INNER JOIN quadro_feridas_enf_grau ON quadro_feridas_enfermagem.GRAU = quadro_feridas_enf_grau.ID
INNER JOIN quadro_feridas_enf_contaminacao ON quadro_feridas_enfermagem.CONTAMINACAO = quadro_feridas_enf_contaminacao.ID
INNER JOIN quadro_feridas_enf_pele ON quadro_feridas_enfermagem.PELE = quadro_feridas_enf_pele.ID
INNER JOIN quadro_feridas_enf_tempo ON quadro_feridas_enfermagem.TEMPO = quadro_feridas_enf_tempo.ID
INNER JOIN quadro_feridas_enf_periodo ON quadro_feridas_enfermagem.PERIODO = quadro_feridas_enf_periodo.ID
INNER JOIN quadro_feridas_enf_status ON quadro_feridas_enfermagem.STATUS_FERIDA = quadro_feridas_enf_status.id
INNER JOIN clientes ON evolucaoenfermagem.PACIENTE_ID = clientes.idClientes
INNER JOIN usuarios ON evolucaoenfermagem.USUARIO_ID = usuarios.idUsuarios
WHERE
quadro_feridas_enfermagem.STATUS_FERIDA <> 2 and

evolucaoenfermagem.ID in (SELECT
x.ID
FROM
(
SELECT
clientes.nome,
evolucaoenfermagem.ID,
evolucaoenfermagem.USUARIO_ID,
evolucaoenfermagem.PACIENTE_ID,
evolucaoenfermagem.`DATA`,
evolucaoenfermagem.DATA_EVOLUCAO
FROM
evolucaoenfermagem
INNER JOIN clientes ON evolucaoenfermagem.PACIENTE_ID = clientes.idClientes
WHERE
clientes.empresa = 11 AND
clientes.`status` = 4 AND
clientes.idClientes  NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
ORDER BY evolucaoenfermagem.PACIENTE_ID,DATA_EVOLUCAO DESC
) as x
where 1
GROUP BY PACIENTE_ID)
