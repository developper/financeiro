CREATE TABLE contrato_operadora  (
  `id` int(0) NOT NULL,
  `nome` varchar(255) NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `contrato_operadora`
ADD COLUMN `operadora_id` int(11) NULL AFTER `nome`,
ADD COLUMN `inicio_vigencia` date NULL AFTER `operadora_id`,
ADD COLUMN `fim_vigencia` varchar(255) NULL AFTER `inicio_vigencia`;

CREATE TABLE tabelas_cobrancas  (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE contratos_tabelas_cobrancas (
  `contrato_operadora_id` int(11) NOT NULL,
  `tabela_cobranca_id` int(0) NULL,
  `tipo` enum('medicamento','dieta','material','servico','equipamento') NULL,
  `fator` float(8, 2) NULL COMMENT 'Campo utilizado para desconto ou acrescimo  caso desconto recebe negativo e valor em percentual.',
  PRIMARY KEY (`contrato_operadora_id`)
);

CREATE TABLE tabelas_cobrancas_medicamentos  (
  `tabelas_cobrancas_id` int(11) NULL,
  `catalogo_id` int(0) NULL,
  `item` varchar(255) NULL,
  `codigo_fatura` varchar(255) NULL,
  `valor` double(8, 2) NULL
);

-- CREATE TABLE item_padrao_sistema (
--   `id` int(0) NOT NULL AUTO_INCREMENT,
--   `item` varchar(255) NULL,
--   `generico` enum('S','N') NULL,
--   `tipo` enum('medicamento','dieta','material') NULL,
--   PRIMARY KEY (`id`)
-- );

ALTER TABLE `catalogo`
ADD COLUMN `nome_padrao` varchar(255) NULL AFTER `possui_generico`,
ADD COLUMN `item_escolhido` enum('S','N') NULL AFTER `nome_padrao`;

ALTER TABLE `saida` 
ADD COLUMN `catalogo_id_escolhido` varchar(255) NULL COMMENT 'Recebe o id do item da tabela catalogo escolhido para aparcer na fatura.' AFTER `saida_remessa_id`;

ALTER TABLE `contratos_tabelas_cobrancas` 
MODIFY COLUMN `tabela_cobranca_id` int(11) NOT NULL AFTER `contrato_operadora_id`,
MODIFY COLUMN `tipo` enum('medicamento','dieta','material','servico','equipamento') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL AFTER `tabela_cobranca_id`,
MODIFY COLUMN `fator` float(8, 2) NOT NULL DEFAULT 1 COMMENT 'Campo utilizado para desconto ou acrescimo  caso desconto recebe negativo e valor em percentual.' AFTER `tipo`,
ADD COLUMN `ordem` int(11) NOT NULL COMMENT 'Caso a busca dos valores cobrados forem em mais de uma tabela o sistema deve buscar os itens seguindo a ordem.' AFTER `fator`;

INSERT INTO `tabelas_cobrancas`(`id`, `nome`) VALUES (1, 'BRASINDICE PFB');
INSERT INTO `tabelas_cobrancas`(`id`, `nome`) VALUES (2, 'BRASINDICE PMC');
INSERT INTO `tabelas_cobrancas`(`id`, `nome`) VALUES (3, 'SIMPRO PFB');
INSERT INTO `tabelas_cobrancas`(`id`, `nome`) VALUES (4, 'SIMPRO PMC');

ALTER TABLE `contratos_tabelas_cobrancas` 
DROP PRIMARY KEY;

ALTER TABLE `faturamento` 
ADD COLUMN `TABELA_COBRANCA_ID` varchar(255) NULL AFTER `custo_atualizado_por`;

ALTER TABLE `historico_faturamento` 
ADD COLUMN `TABELA_COBRANCA_ID` int(11) NULL AFTER `custo_atualizado_por`;

ALTER TABLE `faturamento` 
ADD COLUMN `NOME_ITEM` varchar(255) NULL AFTER `TABELA_COBRANCA_ID`;

ALTER TABLE `historico_faturamento` 
ADD COLUMN `NOME_ITEM` varchar(255) NULL AFTER `TABELA_COBRANCA_ID`;

ALTER TABLE `orcamento_avulso_item`
ADD COLUMN `CODIGO_PROPIRO` varchar(255) NULL,
ADD COLUMN `TABELA_COBRANCA_ID` varchar(255) NULL,
ADD COLUMN `NOME_ITEM` varchar(255) NULL;