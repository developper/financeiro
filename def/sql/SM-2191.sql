Delete from plano_contas where ID = 65;

Update plano_contas set COD_N3 = '2.1.20', N3 = 'DESPESAS COM MANUTENCAO', COD_N4 = '2.1.20.01' where ID = 66;
Update plano_contas set COD_N3 = '2.1.20', N3 = 'DESPESAS COM MANUTENCAO', COD_N4 = '2.1.20.02' where ID = 67;
Update plano_contas set COD_N3 = '2.1.20', N3 = 'DESPESAS COM MANUTENCAO', COD_N4 = '2.1.20.03' where ID = 68;
Update plano_contas set COD_N3 = '2.1.20', N3 = 'DESPESAS COM MANUTENCAO', COD_N4 = '2.1.20.04' where ID = 69;

-- rateio ligados ao id 65 que vai ser deletado para o 66

UPDATE rateio set IPCF3 = 'DESPESAS COM MANUTENCAO', IPCG4 = 66 where ID = 2144;
UPDATE rateio set IPCF3 = 'DESPESAS COM MANUTENCAO', IPCG4 = 66 where ID = 6434;
UPDATE rateio set IPCF3 = 'DESPESAS COM MANUTENCAO', IPCG4 = 66 where ID = 12520;


UPDATE plano_contas set COD_N3 = '2.1.01', COD_N4 = '2.1.01.01' where ID =54;
UPDATE plano_contas set COD_N3 = '2.1.01', COD_N4 = '2.1.01.02' where ID =55;
UPDATE plano_contas set COD_N3 = '2.1.01', COD_N4 = '2.1.01.03' where ID =56;
UPDATE plano_contas set COD_N3 = '2.1.01', COD_N4 = '2.1.01.04' where ID =57;
UPDATE plano_contas set COD_N3 = '2.1.01', COD_N4 = '2.1.01.05' where ID =58;
UPDATE plano_contas set COD_N3 = '2.1.01', COD_N4 = '2.1.01.06' where ID =59;
UPDATE plano_contas set COD_N3 = '2.1.01', COD_N4 = '2.1.01.07' where ID =60;
UPDATE plano_contas set COD_N3 = '2.1.01', COD_N4 = '2.1.01.08' where ID =61;
UPDATE plano_contas set COD_N3 = '2.1.01', COD_N4 = '2.1.01.09' where ID =62;
UPDATE plano_contas set COD_N3 = '2.1.02', COD_N4 = '2.1.02.01' where ID =63;
UPDATE plano_contas set COD_N3 = '2.1.02', COD_N4 = '2.1.02.02' where ID =64;
UPDATE plano_contas set COD_N3 = '2.1.03', COD_N4 = '2.1.03.01' where ID =70;
UPDATE plano_contas set COD_N3 = '2.1.03', COD_N4 = '2.1.03.02' where ID =71;
UPDATE plano_contas set COD_N3 = '2.1.03', COD_N4 = '2.1.03.03' where ID =72;
UPDATE plano_contas set COD_N3 = '2.1.03', COD_N4 = '2.1.03.04' where ID =73;
UPDATE plano_contas set COD_N3 = '2.1.03', COD_N4 = '2.1.03.05' where ID =74;
UPDATE plano_contas set COD_N3 = '2.1.03', COD_N4 = '2.1.03.06' where ID =75;
UPDATE plano_contas set COD_N3 = '2.1.03', COD_N4 = '2.1.03.07' where ID =76;
UPDATE plano_contas set COD_N3 = '2.1.03', COD_N4 = '2.1.03.08' where ID =77;
UPDATE plano_contas set COD_N3 = '2.1.03', COD_N4 = '2.1.03.09' where ID =78;
UPDATE plano_contas set COD_N3 = '2.1.03', COD_N4 = '2.1.03.10' where ID =79;
UPDATE plano_contas set COD_N3 = '2.1.03', COD_N4 = '2.1.03.11' where ID =80;
UPDATE plano_contas set COD_N3 = '2.1.03', COD_N4 = '2.1.03.12' where ID =81;
UPDATE plano_contas set COD_N3 = '2.1.03', COD_N4 = '2.1.03.13' where ID =82;
UPDATE plano_contas set COD_N3 = '2.1.04', COD_N4 = '2.1.04.01' where ID =83;
UPDATE plano_contas set COD_N3 = '2.1.04', COD_N4 = '2.1.04.02' where ID =84;
UPDATE plano_contas set COD_N3 = '2.1.04', COD_N4 = '2.1.04.03' where ID =85;
UPDATE plano_contas set COD_N3 = '2.1.04', COD_N4 = '2.1.04.04' where ID =86;
UPDATE plano_contas set COD_N3 = '2.1.04', COD_N4 = '2.1.04.05' where ID =87;
UPDATE plano_contas set COD_N3 = '2.1.04', COD_N4 = '2.1.04.06' where ID =88;
UPDATE plano_contas set COD_N3 = '2.1.04', COD_N4 = '2.1.04.07' where ID =89;
UPDATE plano_contas set COD_N3 = '2.1.04', COD_N4 = '2.1.04.08' where ID =90;
UPDATE plano_contas set COD_N3 = '2.1.04', COD_N4 = '2.1.04.09' where ID =91;
UPDATE plano_contas set COD_N3 = '2.1.04', COD_N4 = '2.1.04.10' where ID =92;
UPDATE plano_contas set COD_N3 = '2.1.05', COD_N4 = '2.1.05.01' where ID =93;
UPDATE plano_contas set COD_N3 = '2.1.05', COD_N4 = '2.1.05.02' where ID =94;
UPDATE plano_contas set COD_N3 = '2.1.05', COD_N4 = '2.1.05.03' where ID =95;
UPDATE plano_contas set COD_N3 = '2.1.05', COD_N4 = '2.1.05.04' where ID =96;
UPDATE plano_contas set COD_N3 = '2.1.05', COD_N4 = '2.1.05.05' where ID =97;
UPDATE plano_contas set COD_N3 = '2.1.05', COD_N4 = '2.1.05.06' where ID =98;
UPDATE plano_contas set COD_N3 = '2.1.05', COD_N4 = '2.1.05.07' where ID =99;
UPDATE plano_contas set COD_N3 = '2.1.06', COD_N4 = '2.1.06.01' where ID =100;
UPDATE plano_contas set COD_N3 = '2.1.06', COD_N4 = '2.1.06.02' where ID =101;
UPDATE plano_contas set COD_N3 = '2.1.07', COD_N4 = '2.1.07.01' where ID =102;
UPDATE plano_contas set COD_N3 = '2.1.07', COD_N4 = '2.1.07.02' where ID =103;
UPDATE plano_contas set COD_N3 = '2.1.07', COD_N4 = '2.1.07.03' where ID =104;
UPDATE plano_contas set COD_N3 = '2.1.08', COD_N4 = '2.1.08.01' where ID =105;
UPDATE plano_contas set COD_N3 = '2.1.08', COD_N4 = '2.1.08.02' where ID =106;
UPDATE plano_contas set COD_N3 = '2.1.09', COD_N4 = '2.1.09.01' where ID =107;
UPDATE plano_contas set COD_N3 = '2.1.09', COD_N4 = '2.1.09.02' where ID =108;
UPDATE plano_contas set COD_N3 = '2.1.09', COD_N4 = '2.1.09.03' where ID =109;
UPDATE plano_contas set COD_N3 = '2.1.09', COD_N4 = '2.1.09.04' where ID =110;
UPDATE plano_contas set COD_N3 = '2.1.09', COD_N4 = '2.1.09.05' where ID =111;
UPDATE plano_contas set COD_N3 = '2.1.09', COD_N4 = '2.1.09.06' where ID =112;
UPDATE plano_contas set COD_N3 = '2.1.09', COD_N4 = '2.1.09.07' where ID =113;

