
--rollback

INSERT INTO `plano_contas`(`ID`, `N1`, `COD_N2`, `N2`, `COD_N3`, `N3`, `COD_N4`, `N4`, `COD_IPCC`, `IPCC`, `PROVISIONA`, `POSSUI_RETENCOES`, `ISS`, `ICMS`, `IPI`, `PIS`, `COFINS`, `CSLL`, `IR`, `PCC`, `INSS`, `DOMINIO_ID`) VALUES (65, 'S', '2.1', 'DESPESAS ADMINISTRATIVAS', '2.1.2', 'DESPESAS COM VIAGENS E ESTADAS', '2.1.2.03', 'DESPESAS COM MANUTENCAO', '3.1.1.02.001', 'SERVICOS PRESTADOS', 's', 2, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0);


Update plano_contas set COD_N3 = '2.1.2', N3 = 'DESPESAS COM VIAGENS E ESTADAS', COD_N4 = '2.1.2.04' where ID = 66;
Update plano_contas set COD_N3 = '2.1.2', N3 = 'DESPESAS COM VIAGENS E ESTADAS', COD_N4 = '2.1.2.05' where ID = 67;
Update plano_contas set COD_N3 = '2.1.2', N3 = 'DESPESAS COM VIAGENS E ESTADAS', COD_N4 = '2.1.2.06' where ID = 68;
Update plano_contas set COD_N3 = '2.1.2', N3 = 'DESPESAS COM VIAGENS E ESTADAS', COD_N4 = '2.1.2.07' where ID = 69;

UPDATE rateio set IPCF3 = 'DESPESAS COM VIAGENS E ESTADAS', IPCG4 = 65 where ID = 2144;
UPDATE rateio set IPCF3 = 'DESPESAS COM VIAGENS E ESTADAS', IPCG4 = 65 where ID = 6434;
UPDATE rateio set IPCF3 = 'DESPESAS COM VIAGENS E ESTADAS', IPCG4 = 65 where ID = 12520;

UPDATE plano_contas set COD_N3 = '2.1.1', COD_N4 = '2.1.1.01' where ID =54;
UPDATE plano_contas set COD_N3 = '2.1.1', COD_N4 = '2.1.1.02' where ID =55;
UPDATE plano_contas set COD_N3 = '2.1.1', COD_N4 = '2.1.1.03' where ID =56;
UPDATE plano_contas set COD_N3 = '2.1.1', COD_N4 = '2.1.1.04' where ID =57;
UPDATE plano_contas set COD_N3 = '2.1.1', COD_N4 = '2.1.1.05' where ID =58;
UPDATE plano_contas set COD_N3 = '2.1.1', COD_N4 = '2.1.1.06' where ID =59;
UPDATE plano_contas set COD_N3 = '2.1.1', COD_N4 = '2.1.1.07' where ID =60;
UPDATE plano_contas set COD_N3 = '2.1.1', COD_N4 = '2.1.1.08' where ID =61;
UPDATE plano_contas set COD_N3 = '2.1.1', COD_N4 = '2.1.1.09' where ID =62;
UPDATE plano_contas set COD_N3 = '2.1.2', COD_N4 = '2.1.2.01' where ID =63;
UPDATE plano_contas set COD_N3 = '2.1.2', COD_N4 = '2.1.2.02' where ID =64;
UPDATE plano_contas set COD_N3 = '2.1.3', COD_N4 = '2.1.3.01' where ID =70;
UPDATE plano_contas set COD_N3 = '2.1.3', COD_N4 = '2.1.3.02' where ID =71;
UPDATE plano_contas set COD_N3 = '2.1.3', COD_N4 = '2.1.3.03' where ID =72;
UPDATE plano_contas set COD_N3 = '2.1.3', COD_N4 = '2.1.3.04' where ID =73;
UPDATE plano_contas set COD_N3 = '2.1.3', COD_N4 = '2.1.3.05' where ID =74;
UPDATE plano_contas set COD_N3 = '2.1.3', COD_N4 = '2.1.3.06' where ID =75;
UPDATE plano_contas set COD_N3 = '2.1.3', COD_N4 = '2.1.3.07' where ID =76;
UPDATE plano_contas set COD_N3 = '2.1.3', COD_N4 = '2.1.3.08' where ID =77;
UPDATE plano_contas set COD_N3 = '2.1.3', COD_N4 = '2.1.3.09' where ID =78;
UPDATE plano_contas set COD_N3 = '2.1.3', COD_N4 = '2.1.3.10' where ID =79;
UPDATE plano_contas set COD_N3 = '2.1.3', COD_N4 = '2.1.3.11' where ID =80;
UPDATE plano_contas set COD_N3 = '2.1.3', COD_N4 = '2.1.3.12' where ID =81;
UPDATE plano_contas set COD_N3 = '2.1.3', COD_N4 = '2.1.3.13' where ID =82;
UPDATE plano_contas set COD_N3 = '2.1.4', COD_N4 = '2.1.4.01' where ID =83;
UPDATE plano_contas set COD_N3 = '2.1.4', COD_N4 = '2.1.4.02' where ID =84;
UPDATE plano_contas set COD_N3 = '2.1.4', COD_N4 = '2.1.4.03' where ID =85;
UPDATE plano_contas set COD_N3 = '2.1.4', COD_N4 = '2.1.4.04' where ID =86;
UPDATE plano_contas set COD_N3 = '2.1.4', COD_N4 = '2.1.4.05' where ID =87;
UPDATE plano_contas set COD_N3 = '2.1.4', COD_N4 = '2.1.4.06' where ID =88;
UPDATE plano_contas set COD_N3 = '2.1.4', COD_N4 = '2.1.4.07' where ID =89;
UPDATE plano_contas set COD_N3 = '2.1.4', COD_N4 = '2.1.4.08' where ID =90;
UPDATE plano_contas set COD_N3 = '2.1.4', COD_N4 = '2.1.4.09' where ID =91;
UPDATE plano_contas set COD_N3 = '2.1.4', COD_N4 = '2.1.4.10' where ID =92;
UPDATE plano_contas set COD_N3 = '2.1.5', COD_N4 = '2.1.5.01' where ID =93;
UPDATE plano_contas set COD_N3 = '2.1.5', COD_N4 = '2.1.5.02' where ID =94;
UPDATE plano_contas set COD_N3 = '2.1.5', COD_N4 = '2.1.5.03' where ID =95;
UPDATE plano_contas set COD_N3 = '2.1.5', COD_N4 = '2.1.5.04' where ID =96;
UPDATE plano_contas set COD_N3 = '2.1.5', COD_N4 = '2.1.5.05' where ID =97;
UPDATE plano_contas set COD_N3 = '2.1.5', COD_N4 = '2.1.5.06' where ID =98;
UPDATE plano_contas set COD_N3 = '2.1.5', COD_N4 = '2.1.5.07' where ID =99;
UPDATE plano_contas set COD_N3 = '2.1.6', COD_N4 = '2.1.6.01' where ID =100;
UPDATE plano_contas set COD_N3 = '2.1.6', COD_N4 = '2.1.6.02' where ID =101;
UPDATE plano_contas set COD_N3 = '2.1.7', COD_N4 = '2.1.7.01' where ID =102;
UPDATE plano_contas set COD_N3 = '2.1.7', COD_N4 = '2.1.7.02' where ID =103;
UPDATE plano_contas set COD_N3 = '2.1.7', COD_N4 = '2.1.7.03' where ID =104;
UPDATE plano_contas set COD_N3 = '2.1.8', COD_N4 = '2.1.8.01' where ID =105;
UPDATE plano_contas set COD_N3 = '2.1.8', COD_N4 = '2.1.8.02' where ID =106;
UPDATE plano_contas set COD_N3 = '2.1.9', COD_N4 = '2.1.9.01' where ID =107;
UPDATE plano_contas set COD_N3 = '2.1.9', COD_N4 = '2.1.9.02' where ID =108;
UPDATE plano_contas set COD_N3 = '2.1.9', COD_N4 = '2.1.9.03' where ID =109;
UPDATE plano_contas set COD_N3 = '2.1.9', COD_N4 = '2.1.9.04' where ID =110;
UPDATE plano_contas set COD_N3 = '2.1.9', COD_N4 = '2.1.9.05' where ID =111;
UPDATE plano_contas set COD_N3 = '2.1.9', COD_N4 = '2.1.9.06' where ID =112;
UPDATE plano_contas set COD_N3 = '2.1.9', COD_N4 = '2.1.9.07' where ID =113;