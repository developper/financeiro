-- consulta para verificar quais itens estão duplicados na tabela cobranca plano desativar e deixar apenas um como ativo.
SELECT
count(cobrancaplanos.item) as x,
cobrancaplanos.item
FROM
cobrancaplanos
WHERE
1
GROUP BY cobrancaplanos.item
ORDER BY x DESC
-- destiva um dos itens duplicados deixando só um ativo.
UPDATE `cobrancaplanos` SET `ATIVO`='0' WHERE (`id`='95');
UPDATE `cobrancaplanos` SET `ATIVO`='0' WHERE (`id`='94');
UPDATE `cobrancaplanos` SET `ATIVO`='0' WHERE (`id`='124');
UPDATE `custo_servicos_plano_ur` SET `STATUS`='D' WHERE COBRANCAPLANO_ID in (95,94,124);


-- Consulta para vrificar itens repetidos e ativso na tabela de custo_servicos_plano_ur
SELECT
COUNT(custo_servicos_plano_ur.UR) as x,
custo_servicos_plano_ur.*
from
custo_servicos_plano_ur
WHERE
`STATUS` = 'A'
GROUP BY UR,COBRANCAPLANO_ID
ORDER BY x desc, UR DESC, COBRANCAPLANO_ID DESC

UPDATE `custo_servicos_plano_ur` SET `STATUS`='D' WHERE ID in (31,32, 63, 64, 95,96,127,128,159,160,);