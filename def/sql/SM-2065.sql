-- MEDICO
SELECT
  CONCAT('UPDATE formulario_condensado_evolucao_medica SET relatorio_id = ', relatorio_prorrogacao_med.ID, ' WHERE id = ', formulario_condensado_evolucao_medica.id, ';') AS squery
FROM
  formulario_condensado
  INNER JOIN formulario_condensado_evolucao_medica ON formulario_condensado.id = formulario_condensado_evolucao_medica.formulario_condensado_id
  LEFT JOIN relatorio_prorrogacao_med ON (
      relatorio_prorrogacao_med.INICIO = formulario_condensado.inicio
      AND relatorio_prorrogacao_med.FIM = formulario_condensado.fim
      AND relatorio_prorrogacao_med.PACIENTE_ID = formulario_condensado.paciente_id
  )
WHERE
  formulario_condensado.tipo_formulario = 'p'
  AND (
      formulario_condensado_evolucao_medica.relatorio_id IS NULL
        OR formulario_condensado_evolucao_medica.relatorio_id = 0
  );
-- FIX
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12095 WHERE id = 87;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12217 WHERE id = 87;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12096 WHERE id = 85;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12089 WHERE id = 84;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12101 WHERE id = 116;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12207 WHERE id = 116;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12060 WHERE id = 88;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12178 WHERE id = 89;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12015 WHERE id = 90;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12065 WHERE id = 91;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12170 WHERE id = 92;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12118 WHERE id = 95;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12177 WHERE id = 96;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12055 WHERE id = 30;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12068 WHERE id = 30;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12205 WHERE id = 30;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12100 WHERE id = 107;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12209 WHERE id = 107;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12140 WHERE id = 97;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12004 WHERE id = 99;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12017 WHERE id = 100;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12097 WHERE id = 101;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12029 WHERE id = 102;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12131 WHERE id = 105;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12171 WHERE id = 106;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12191 WHERE id = 93;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12192 WHERE id = 94;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12146 WHERE id = 59;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12016 WHERE id = 113;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12219 WHERE id = 58;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12243 WHERE id = 112;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12099 WHERE id = 65;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12175 WHERE id = 109;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = 12176 WHERE id = 108;
-- ROLLBACK
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 87;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 87;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 85;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 84;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 116;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 116;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 88;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 89;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 90;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 91;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 92;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 95;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 96;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 30;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 30;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 30;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 107;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 107;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 97;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 99;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 100;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 101;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 102;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 105;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 106;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 93;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 94;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 59;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 113;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 58;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 112;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 65;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 109;
UPDATE formulario_condensado_evolucao_medica SET relatorio_id = NULL WHERE id = 108;
-- ENFERMAGEM
SELECT
  CONCAT('UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = ', relatorio_prorrogacao_enf.ID, ' WHERE id = ', formulario_condensado_evolucao_enfermagem.id, ';') AS squery
FROM
     formulario_condensado
       INNER JOIN formulario_condensado_evolucao_enfermagem ON formulario_condensado.id = formulario_condensado_evolucao_enfermagem.formulario_condensado_id
       LEFT JOIN relatorio_prorrogacao_enf ON (
         relatorio_prorrogacao_enf.INICIO = formulario_condensado.inicio
           AND relatorio_prorrogacao_enf.FIM = formulario_condensado.fim
           AND relatorio_prorrogacao_enf.PACIENTE_ID = formulario_condensado.paciente_id
         )
WHERE
  formulario_condensado.tipo_formulario = 'p'
  AND (
      formulario_condensado_evolucao_enfermagem.relatorio_id IS NULL
      OR formulario_condensado_evolucao_enfermagem.relatorio_id = 0
  );
-- FIX
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8282 WHERE id = 63;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8300 WHERE id = 63;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8281 WHERE id = 60;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8299 WHERE id = 60;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8296 WHERE id = 87;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8405 WHERE id = 40;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8326 WHERE id = 64;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8312 WHERE id = 65;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8375 WHERE id = 66;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8327 WHERE id = 67;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8328 WHERE id = 68;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8292 WHERE id = 17;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8394 WHERE id = 17;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8308 WHERE id = 79;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8329 WHERE id = 69;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8316 WHERE id = 71;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8311 WHERE id = 72;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8285 WHERE id = 73;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8306 WHERE id = 74;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8295 WHERE id = 77;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8305 WHERE id = 78;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8360 WHERE id = 39;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8365 WHERE id = 61;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8283 WHERE id = 81;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = 8290 WHERE id = 80;
-- ROLLBACK
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 63;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 63;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 60;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 60;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 87;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 40;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 64;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 65;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 66;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 67;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 68;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 17;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 17;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 79;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 69;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 71;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 72;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 73;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 74;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 77;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 78;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 39;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 61;
UPDATE formulario_condensado_evolucao_enfermagem SET relatorio_id = NULL WHERE id = 81;

-- Fisioterapia
SELECT
        CONCAT('UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = ', RP.id, ' WHERE id = ', PREST.id, ';') AS squery,
				CONCAT('UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = ', COALESCE(relatorio_id,0), ' WHERE id = ', PREST.id, ';') AS srollback
FROM
        formulario_condensado AS FC INNER JOIN
        formulario_condensado_evolucao_fisioterapia AS PREST ON (FC.id = PREST.formulario_condensado_id) LEFT JOIN
        relatorio_prestador AS RP ON (RP.paciente_id = FC.paciente_id AND RP.inicio = FC.inicio AND  RP.fim = FC.fim)
WHERE
        FC.tipo_formulario = 'p' AND
        RP.tipo_prestador = 'fisioterapia' AND (
				PREST.relatorio_id IS NULL
        OR PREST.relatorio_id = 0
  );
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 1 WHERE id = 94;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 2 WHERE id = 54;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 5 WHERE id = 115;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 6 WHERE id = 92;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 7 WHERE id = 55;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 8 WHERE id = 93;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 13 WHERE id = 109;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 14 WHERE id = 112;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 15 WHERE id = 42;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 19 WHERE id = 116;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 25 WHERE id = 95;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 26 WHERE id = 96;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 27 WHERE id = 97;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 28 WHERE id = 107;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 29 WHERE id = 120;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 30 WHERE id = 101;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 31 WHERE id = 114;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 32 WHERE id = 90;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 39 WHERE id = 125;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 40 WHERE id = 100;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 44 WHERE id = 108;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 47 WHERE id = 106;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 48 WHERE id = 113;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 50 WHERE id = 41;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 69 WHERE id = 103;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 90 WHERE id = 99;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 91 WHERE id = 56;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 93 WHERE id = 88;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 98 WHERE id = 104;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 104 WHERE id = 98;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 106 WHERE id = 119;

-- rolback
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 94;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 54;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 115;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 92;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 55;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 93;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 109;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 112;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 42;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 116;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 95;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 96;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 97;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 107;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 120;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 101;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 114;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 90;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 125;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 100;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 108;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 106;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 113;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 41;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 103;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 99;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 56;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 88;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 104;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 98;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 119;

-- Psicologia
SELECT
        CONCAT('UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = ', RP.id, ' WHERE id = ', PREST.id, ';') AS squery,
				CONCAT('UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = ', COALESCE(relatorio_id,0), ' WHERE id = ', PREST.id, ';') AS srollback
FROM
        formulario_condensado AS FC INNER JOIN
        formulario_condensado_evolucao_psicologia AS PREST ON (FC.id = PREST.formulario_condensado_id) LEFT JOIN
        relatorio_prestador AS RP ON (RP.paciente_id = FC.paciente_id AND RP.inicio = FC.inicio AND  RP.fim = FC.fim)
WHERE
        FC.tipo_formulario = 'p' AND
        RP.tipo_prestador = 'psicologia' AND (
				PREST.relatorio_id IS NULL
        OR PREST.relatorio_id = 0
  );

  UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 114 WHERE id = 7;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 115 WHERE id = 6;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 116 WHERE id = 8;

-- rollback
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 7;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 6;
UPDATE formulario_condensado_evolucao_fisioterapia SET relatorio_id = 0 WHERE id = 8;

-- NUTRI
SELECT
       CONCAT('UPDATE formulario_condensado_evolucao_nutricao SET relatorio_id = ', RP.id, ' WHERE id = ', PREST.id, ';') AS squery,
       CONCAT('UPDATE formulario_condensado_evolucao_nutricao SET relatorio_id = ', COALESCE(relatorio_id,0), ' WHERE id = ', PREST.id, ';') AS srollback
FROM
     formulario_condensado AS FC INNER JOIN
         formulario_condensado_evolucao_nutricao AS PREST ON (FC.id = PREST.formulario_condensado_id) LEFT JOIN
         relatorio_prestador AS RP ON (RP.paciente_id = FC.paciente_id AND RP.inicio = FC.inicio AND  RP.fim = FC.fim)
WHERE
    FC.tipo_formulario = 'p' AND
    RP.tipo_prestador = 'nutricao' AND (
        PREST.relatorio_id IS NULL
          OR PREST.relatorio_id = 0
        );
-- FIX
UPDATE formulario_condensado_evolucao_nutricao SET relatorio_id = 111 WHERE id = 22;
-- ROLLBACK
UPDATE formulario_condensado_evolucao_nutricao SET relatorio_id = 0 WHERE id = 22;
-- FONO
SELECT
       CONCAT('UPDATE formulario_condensado_evolucao_fonoaudiologia SET relatorio_id = ', RP.id, ' WHERE id = ', PREST.id, ';') AS squery,
       CONCAT('UPDATE formulario_condensado_evolucao_fonoaudiologia SET relatorio_id = ', COALESCE(relatorio_id,0), ' WHERE id = ', PREST.id, ';') AS srollback
FROM
     formulario_condensado AS FC INNER JOIN
         formulario_condensado_evolucao_fonoaudiologia AS PREST ON (FC.id = PREST.formulario_condensado_id) LEFT JOIN
         relatorio_prestador AS RP ON (RP.paciente_id = FC.paciente_id AND RP.inicio = FC.inicio AND  RP.fim = FC.fim)
WHERE
    FC.tipo_formulario = 'p' AND
    RP.tipo_prestador = 'fonoaudiologia' AND (
        PREST.relatorio_id IS NULL
          OR PREST.relatorio_id = 0
        );
-- FIX
UPDATE formulario_condensado_evolucao_fonoaudiologia SET relatorio_id = 58 WHERE id = 5;
UPDATE formulario_condensado_evolucao_fonoaudiologia SET relatorio_id = 60 WHERE id = 37;
UPDATE formulario_condensado_evolucao_fonoaudiologia SET relatorio_id = 61 WHERE id = 49;
UPDATE formulario_condensado_evolucao_fonoaudiologia SET relatorio_id = 73 WHERE id = 36;
UPDATE formulario_condensado_evolucao_fonoaudiologia SET relatorio_id = 77 WHERE id = 38;
UPDATE formulario_condensado_evolucao_fonoaudiologia SET relatorio_id = 78 WHERE id = 42;
-- ROLLBACK
UPDATE formulario_condensado_evolucao_fonoaudiologia SET relatorio_id = 0 WHERE id = 5;
UPDATE formulario_condensado_evolucao_fonoaudiologia SET relatorio_id = 0 WHERE id = 37;
UPDATE formulario_condensado_evolucao_fonoaudiologia SET relatorio_id = 0 WHERE id = 49;
UPDATE formulario_condensado_evolucao_fonoaudiologia SET relatorio_id = 0 WHERE id = 36;
UPDATE formulario_condensado_evolucao_fonoaudiologia SET relatorio_id = 0 WHERE id = 38;
UPDATE formulario_condensado_evolucao_fonoaudiologia SET relatorio_id = 0 WHERE id = 42;