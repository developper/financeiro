CREATE TABLE saida_remessa  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `alfa` enum('CD','FSA','SSA/RMS','ALG','SUL/BA','SO/BA') NULL,
  `numerica` int(11) NULL,
  `empresa_id` int(11) NULL,
  `remessa` varchar(255) NULL,
  `tipo_saida` enum('PACIENTE','UR') NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `empresas`
ADD COLUMN `sigla_saida` enum('CD','FSA','SSA/RMS','SUL/BA','SO/BA','ALG') NULL COMMENT 'Siglas usadas para gerar remessa na tabela saida_remessa' AFTER `imposto_iss`;

UPDATE `empresas` SET `sigla_saida` = 'FSA' WHERE `id` = 11;
UPDATE `empresas` SET `sigla_saida` = 'CD' WHERE `id` = 1;
UPDATE `empresas` SET `sigla_saida` = 'SSA/RMS' WHERE `id` = 2;
UPDATE `empresas` SET `sigla_saida` = 'SUL/BA' WHERE `id` = 4;
UPDATE `empresas` SET `sigla_saida` = 'ALG' WHERE `id` = 5;
UPDATE `empresas` SET `sigla_saida` = 'SO/BA' WHERE `id` = 7;
UPDATE `empresas` SET `SIGLA_EMPRESA` = 'SO/BA' WHERE `id` = 7;

ALTER TABLE `saida`
ADD COLUMN `remessa` varchar(255) NULL AFTER `confirmacao_pedidointerno`;