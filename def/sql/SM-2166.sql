INSERT INTO `area_permissoes` VALUES (NULL, 103, 3, 'financeiro_conta_bancaria_consultar_saldos', 'Consultar Saldos Bancários');
INSERT INTO `area_permissoes` VALUES (NULL, 103, 4, 'financeiro_conta_bancaria_salvar_saldos', 'Salvar Saldos Bancários');

create table historico_saldo_contas_bancarias
(
    id int auto_increment,
    conta_id int not null,
    data_saldo date not null,
    saldo_anterior decimal(10,2) not null,
    entrada decimal(10,2) not null,
    saida decimal(10,2) not null,
    saldo_atual decimal(10,2) not null,
    criado_em datetime not null,
    criado_por int not null,
    constraint historico_saldo_contas_bancarias_pk
        primary key (id)
);
