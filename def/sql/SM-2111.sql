ALTER TABLE `impostos_retidos` 
ADD COLUMN `tipo_documento_financeiro_id` int(0) NOT NULL AFTER `IPCF4`;

UPDATE `impostos_retidos` SET `tipo_documento_financeiro_id` = 16 WHERE `ID` = 1;
UPDATE `impostos_retidos` SET `tipo_documento_financeiro_id` = 16 WHERE `ID` = 2;
UPDATE `impostos_retidos` SET `tipo_documento_financeiro_id` = 24 WHERE `ID` = 3;
UPDATE `impostos_retidos` SET `tipo_documento_financeiro_id` = 5 WHERE `ID` = 4;
UPDATE `impostos_retidos` SET `tipo_documento_financeiro_id` = 7 WHERE `ID` = 5;
UPDATE `impostos_retidos` SET `tipo_documento_financeiro_id` = 32 WHERE `ID` = 6;
UPDATE `impostos_retidos` SET `tipo_documento_financeiro_id` = 17 WHERE `ID` = 7;