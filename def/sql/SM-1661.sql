ALTER TABLE `devolucao_interna_itens`
ADD COLUMN `enviado`  int(11) NULL AFTER `autorizado`;

ALTER TABLE `devolucao_interna_pedido`
ADD COLUMN `sent_by`  int(11) NULL AFTER `accepted_at`,
ADD COLUMN `sent_at`  datetime NULL AFTER `sent_by`;

ALTER TABLE `devolucao_interna_pedido`
MODIFY COLUMN `status`  enum('FINALIZADO','AUTORIZADO','CANCELADO','ENVIADO','PENDENTE') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `motivo_cancelamento`;
