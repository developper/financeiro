
-- Primeiro rodei a consulta para veros itens e identifiquei 
-- que algumas notas já estavam com data de emissão e competência corretas.
SELECT 
n.idNotas, 
n.dataEntrada, 
n.data_competencia 
FROM 
notas n 
where
n.dataEntrada >= '2020-11-01'and 
n.idNotas in (29065,
30900,
29060,
29111,
29003,
29004,
29116,
29117,
29067,
29082,
29069,
29118,
29119,
29120,
29121,
29122,
29123,
29124,
29125,
29126,
29072,
29073,
29074,
28987,
29061,
29062,
29063,
29064,
29112,
29113,
29114,
29115,
29080,
29081,
29066,
29070,
29071,
28986,
29944,
29945,
29946,
29947,
29943,
29221,
28985,
29001,
29002,
29079,
29068,
29083,
29559
)
ORDER by n.dataEntrada ;


-- Notas com data de emissão e competencia em novembro 
28985
29121
29122
29123
29124
29125
29126
29221
29559
29943
29944
29945
29946
29947
29120
29119
29118
28986
28987
29001
29002
29003
29004
29111
29112
29113
29114
29115
29116
29117
30900

-- consulta para pegar dados das Trs já em outubro para Marco analisar com Caio se já foram exportadas


SELECT 
n.idNotas as TR, 
n.dataEntrada as EMISSSAO, 
n.data_competencia as COMPETENCIA,
n.codigo as CODIGO_NOTA,
f2.nome as FORNECEDOR
-- sum(n.valor) 
FROM 
notas n 
inner join fornecedores f2 on (n.codFornecedor = f2.idFornecedores )
where
  n.dataEntrada < '2020-11-01'and 
n.idNotas in (29065,
30900,
29060,
29111,
29003,
29004,
29116,
29117,
29067,
29082,
29069,
29118,
29119,
29120,
29121,
29122,
29123,
29124,
29125,
29126,
29072,
29073,
29074,
28987,
29061,
29062,
29063,
29064,
29112,
29113,
29114,
29115,
29080,
29081,
29066,
29070,
29071,
28986,
29944,
29945,
29946,
29947,
29943,
29221,
28985,
29001,
29002,
29079,
29068,
29083,
29559
)
ORDER by n.dataEntrada ;

----------------------------------------------------
-- Sql gerado para atualizar as trs que estavam em novembro e deveriam ser de outubro
-----------------------------------------------
SELECT 
CONCAT('UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = ',idNotas,';') as up,
CONCAT('UPDATE notas set dataEntrada = "', dataEntrada,'", data_competencia = "', data_competencia,'" where idNotas = ',idNotas,';') as roll
FROM 
notas n 
where
  n.dataEntrada >= '2020-11-01'and 
n.idNotas in (29065,
30900,
29060,
29111,
29003,
29004,
29116,
29117,
29067,
29082,
29069,
29118,
29119,
29120,
29121,
29122,
29123,
29124,
29125,
29126,
29072,
29073,
29074,
28987,
29061,
29062,
29063,
29064,
29112,
29113,
29114,
29115,
29080,
29081,
29066,
29070,
29071,
28986,
29944,
29945,
29946,
29947,
29943,
29221,
28985,
29001,
29002,
29079,
29068,
29083,
29559
)
ORDER by n.dataEntrada ;

------------------------------------------
--- Sql de atualização update
--------------------------------------------
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 28985;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29121;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29122;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29123;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29124;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29125;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29126;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29221;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29559;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29943;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29944;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29945;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29946;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29947;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29120;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29119;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29118;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 28986;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 28987;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29001;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29002;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29003;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29004;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29111;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29112;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29113;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29114;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29115;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29116;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 29117;
UPDATE notas set dataEntrada = "2020-10-31", data_competencia = "2020-10" where idNotas = 30900;

-----------------------------------------------
----Sql de retorno roolback
-----------------------------------------------

UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 28985;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29121;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29122;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29123;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29124;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29125;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29126;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29221;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29559;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29943;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29944;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29945;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29946;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29947;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29120;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29119;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29118;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 28986;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 28987;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29001;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29002;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29003;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29004;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29111;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29112;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29113;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29114;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29115;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29116;
UPDATE notas set dataEntrada = "2020-11-01", data_competencia = "2020-11" where idNotas = 29117;
UPDATE notas set dataEntrada = "2020-12-01", data_competencia = "2020-12" where idNotas = 30900;