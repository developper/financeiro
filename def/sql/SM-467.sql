SELECT 
	  ID,
	  lab_desc AS LABORATÓRIO,
	  concat(principio,' - ',apresentacao) AS ITEM,
	  DESC_MATERIAIS_FATURAR AS DESCRICAO_ENFERMEIRA,
	  NUMERO_TISS,
	  TAG  
FROM 
  	  catalogo 
WHERE 
	  ATIVO='A' AND 
	  tipo=1 AND 
	  TAG not IN ('IMPRESSO')