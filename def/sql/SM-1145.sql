-- modificando a prescri��o de avalia��o feita por Dr. Hebert para Huguinho
UPDATE
prescricoes
SET
prescricoes.paciente = 67
WHERE
prescricoes.id = 25249;

-- rollback

UPDATE
prescricoes
SET
prescricoes.paciente = 1345
WHERE
prescricoes.id = 2524;

-- modificando capmed feita por dr. Hebert para o paciente Huginho.
update
capmedica
SET
capmedica.paciente = 67
WHERE
capmedica.id in (2526,
2525);

-- ROLLBACK
update
capmedica
SET
capmedica.paciente = 1345
WHERE
capmedica.id in (2526,
2525);