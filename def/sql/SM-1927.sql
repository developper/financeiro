-- SQl que verifica se existem itens duplicados na tabela simpro com mesmo codigo_simpro_tiss caso tenha ele monta o delet
-- não permitindo deletar o item que esteja já associado na tabela catlogo depois tem que fazer o delet na mão dos itens que ficarem duplicados novamente.
select
COUNT(*),
codigo_simpro_tiss,
descricao,
id,
CONCAT( "DELETE from simpro where codigo_simpro_tiss = '",codigo_simpro_tiss,"' and id <> ",id," and id not in (select simpro_id from catalogo where simpro_id is not null and simpro_id <> '' and simpro_id <> 0);") as del
from
simpro
GROUP BY codigo_simpro_tiss
having COUNT(*) >1