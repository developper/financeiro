-- Consulta relatório de prorrogação de enfermagem.
set @inicio ='2015-03-01';
set @fim    ='2015-04-08';
SELECT
UPPER(clientes.nome) as PACIENTE,
UPPER(usuarios.nome) as ENFERMEIRO,
relatorio_prorrogacao_enf.`DATA` as DATA_SISTEMA,
relatorio_prorrogacao_enf.INICIO,
relatorio_prorrogacao_enf.FIM,
empresas.nome as UR
FROM
relatorio_prorrogacao_enf
INNER JOIN clientes ON relatorio_prorrogacao_enf.PACIENTE_ID = clientes.idClientes
INNER JOIN usuarios ON relatorio_prorrogacao_enf.USUARIO_ID = usuarios.idUsuarios
INNER JOIN empresas ON clientes.empresa = empresas.id
WHERE
relatorio_prorrogacao_enf.`DATA` BETWEEN @inicio AND @fim AND
relatorio_prorrogacao_enf.USUARIO_ID NOT IN (60,48,122) AND
relatorio_prorrogacao_enf.PACIENTE_ID NOT IN (142,67,69,128,434,220,263,112,635,101) AND
empresas.id in (1,2)

--Consulta Relatório Deflagrado de enfermagem
set @inicio ='2015-03-01';
set @fim    ='2015-04-08';
SELECT
UPPER(clientes.nome) AS PACIENTE,
UPPER(usuarios.nome) AS ENFERMEIRO,
DATE_FORMAT(relatorio_deflagrado_enf.`DATA`,'%d/%m/%Y') as DATA_SISTEMA,
DATE_FORMAT(relatorio_deflagrado_enf.INICIO,'%d/%m/%Y')as INICIO,
DATE_FORMAT(relatorio_deflagrado_enf.FIM,'%d/%m/%Y') as FIM,
empresas.nome as UR
FROM
relatorio_deflagrado_enf
INNER JOIN clientes ON relatorio_deflagrado_enf.PACIENTE_ID = clientes.idClientes
INNER JOIN usuarios ON relatorio_deflagrado_enf.USUARIO_ID = usuarios.idUsuarios
INNER JOIN empresas ON clientes.empresa = empresas.id
WHERE
relatorio_deflagrado_enf.`DATA` BETWEEN @inicio AND @fim AND
relatorio_deflagrado_enf.USUARIO_ID NOT IN (60,48,122) AND
relatorio_deflagrado_enf.PACIENTE_ID NOT IN (142,67,69,128,434,220,263,112,635,101) AND
empresas.id in (1,2)

-- Consulta fichas de evolução de enfermagem.
set @inicio ='2015-03-01';
set @fim    ='2015-04-08';
SELECT
UPPER(clientes.nome) AS PACIENTE,
UPPER(usuarios.nome) AS ENFERMEIRO,
DATE_FORMAT(evolucaoenfermagem.`DATA`,'%d/%m/%Y') AS DATA_SISTEMA,
DATE_FORMAT(evolucaoenfermagem.DATA_EVOLUCAO,'%d/%m/%Y') AS DATA_VISITA,
empresas.nome as UR
FROM
evolucaoenfermagem
INNER JOIN clientes ON evolucaoenfermagem.PACIENTE_ID = clientes.idClientes
INNER JOIN usuarios ON evolucaoenfermagem.USUARIO_ID = usuarios.idUsuarios
INNER JOIN empresas ON clientes.empresa = empresas.id
WHERE
evolucaoenfermagem.DATA_EVOLUCAO BETWEEN @inicio AND @fim AND
evolucaoenfermagem.USUARIO_ID NOT IN (60,48,122) AND
evolucaoenfermagem.PACIENTE_ID NOT IN (142,67,69,128,434,220,263,112,635,101) AND
empresas.id in (1,2)