-- ISOSSOURCE
SELECT
	c.apresentacao,
	c.lab_desc,
	c.principio,
	c.PRINCIPIO_ATIVO,
	s.*
FROM
	`solicitacoes` s
INNER JOIN catalogo c ON s.CATALOGO_ID = c.ID
WHERE
	s.paciente = 921
AND s.tipo = 3
ORDER BY
	s.DATA_SOLICITACAO DESC;

-- FRASCO PARA DIETA 300ML
SELECT
	c.apresentacao,
	c.lab_desc,
	c.principio,
	c.PRINCIPIO_ATIVO,
	s.*
FROM
	`solicitacoes` s
INNER JOIN catalogo c ON s.CATALOGO_ID = c.ID
WHERE
	s.paciente = 921
AND s.tipo = 1
AND c.principio LIKE '%FRASCO DESCARTÁVEL P/ DIETA 300ML%'
ORDER BY
	s.DATA_SOLICITACAO DESC;