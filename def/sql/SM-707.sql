-- Update 01
UPDATE
equipamentosativos
SET
DATA_FIM = '2015-02-05 07:00:00' 
WHERE
equipamentosativos.ID in (6208,6209,6210,6218,6213,6214,6217,6219,6212)

--rollback
UPDATE
equipamentosativos
SET
DATA_FIM = '0000-00-00 00:00:00' 
WHERE
equipamentosativos.ID in (6208,6209,6210,6218,6213,6214,6217,6219,6212)

------------------------
-- update 02
UPDATE
equipamentosativos
SET
DATA_INICIO = '2015-01-09 07:00:00' 
WHERE
equipamentosativos.ID=6324
--rollback
UPDATE
equipamentosativos
SET
DATA_INICIO = '0000-00-00 00:00:00' 
WHERE
equipamentosativos.ID=6324