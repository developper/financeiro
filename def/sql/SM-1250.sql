ALTER TABLE `solicitacoes`
  ADD COLUMN `trocado`  enum('N','S') NOT NULL DEFAULT 'N' AFTER `visualizado_logistica`;
-- identificando item inserdido pela logistica na troca

ALTER TABLE `solicitacoes`
ADD COLUMN `inserido_troca`  enum('N','S') NOT NULL DEFAULT 'N' AFTER `trocado`;

-- colocando o id da solicitacao do item susbtituto.
ALTER TABLE `historico_troca_item`
ADD COLUMN `solicitacao_id_substituta`  int(11) NULL AFTER `tipo`;

 ALTER TABLE `solicitacoes`
MODIFY COLUMN `trocado`  enum('N','S') CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `inserido_troca`;

-- Identificando os item anteriores, inseridos na troca pela observacao preenchida.
UPDATE
solicitacoes
SET
inserido_troca = 'S'
WHERE
obs like '%inserido pela Logistica%';

-- indicando que um item trocado.
UPDATE
solicitacoes
SET
trocado = 'S'
WHERE
obs like '%TROCADO PELA%';