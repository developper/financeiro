CREATE TABLE `devolucao_interna_pedido` (
`id`  int(11) NULL AUTO_INCREMENT ,
`created_by`  int(11) NULL ,
`created_at`  int(11) NULL ,
`ur_origem`  int(11) NULL ,
`ur_destino`  int(11) NULL ,
`canceled_by`  int NULL ,
`canceled_at`  int NULL ,
`motivo_cancelamento`  text NULL ,
PRIMARY KEY (`id`)
)
;

CREATE TABLE `devolucao_interna_itens` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`catalogo_id`  int(11) NOT NULL ,
`tipo`  int(11) NOT NULL ,
`lote`  varchar(30) NULL ,
`vencimento`  datetime NULL ,
`quantidade`  int(11) NOT NULL ,
`observacao`  text NULL ,
PRIMARY KEY (`id`)
)
;

ALTER TABLE `devolucao_interna_itens`
MODIFY COLUMN `vencimento`  date NULL DEFAULT NULL AFTER `lote`,
ADD COLUMN `devolucao_interna_pedido_id`  int(11) NULL AFTER `observacao`;

ALTER TABLE `devolucao_interna_pedido`
MODIFY COLUMN `created_at`  date NULL DEFAULT NULL AFTER `created_by`;

CREATE TABLE devolucao_interna_motivo (
`id`  int(11) NULL AUTO_INCREMENT ,
`motivo`  varchar(100) NULL ,
PRIMARY KEY (`id`)
)
;

ALTER TABLE `devolucao_interna_itens`
ADD COLUMN `devolucao_interna_motivo_id`  int(11) NULL AFTER `devolucao_interna_pedido_id`;

ALTER TABLE `devolucao_interna_pedido`
ADD COLUMN `status`  enum('FINALIZADO','ACEITO','PENDENTE') NULL AFTER `motivo_cancelamento`,
ADD COLUMN `accepted_by`  int NULL AFTER `status`,
ADD COLUMN `aceitado_at`  datetime NULL AFTER `accepted_by`;