CREATE TABLE `unidades_fatura` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UNIDADE` varchar(30) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `unidades_fatura` (`ID`, `UNIDADE`)
VALUES
	(1,'COMPRIMIDO'),
	(2,'CAIXA'),
	(3,'FRASCO'),
	(4,'AMPOLA'),
	(5,'BISNAGA'),
	(6,'SACHÊ'),
	(7,'FLACONETE'),
	(8,'PACOTE'),
	(9,'DIÁRIA'),
	(10,'SESSÃO'),
	(11,'VISITA'),
	(12,'UNIDADE'),
	(13,'LATA'),
	(14,'HORA'),
	(15,'AVALIAÇÃO');