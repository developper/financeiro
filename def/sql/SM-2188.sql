ALTER TABLE `notas` 
ADD COLUMN `inserida_caixa_fixo` enum('S','N') NULL DEFAULT 'N' AFTER `compensada`;

INSERT INTO `area_sistema`(id, `nome`, `descricao`, `ordem`, `id_pai`, `menu`, `link`) VALUES (110, 'financeiro_conta_pagar_receber_caixa_fixo', 'Caixa Fixo', 6, 96, 0, '/financeiros/caixa-fixo/?action=index-caixa-fixo');
INSERT INTO `area_permissoes`(id,`area_id`, `ordem`, `acao`, `descricao`) VALUES (114, 110, 1, 'financeiro_conta_pagar_receber_caixa_fixo_nova', 'Criar Conta no Caixa Fixo');
INSERT INTO `area_permissoes`(`id`, `area_id`, `ordem`, `acao`, `descricao`) VALUES (115, 110, 1, 'financeiro_conta_pagar_receber_caixa_fixo_editar', 'Editar Conta no Caixa Fixo');