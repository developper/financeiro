ALTER TABLE `relatorio_nao_conformidade`
ADD COLUMN `reopened_by`  int(11) NULL AFTER `finalizado_by`,
ADD COLUMN `reopened_at`  datetime NULL AFTER `reopened_by`,
ADD COLUMN `justificativa_reopened`  text NULL AFTER `reopened_at`;