CREATE TABLE indexed_demonstrativo_contas_bancarias AS SELECT
	cb.*, CONCAT(
		tcb.abreviacao,
		' - ',
		of.forma,
		': ',
		cb.AGENCIA,
		' - ',
		cb.NUM_CONTA
	) AS conta
FROM
	contas_bancarias AS cb
INNER JOIN origemfundos AS of ON (cb.ORIGEM_FUNDOS_ID = of.id)
INNER JOIN tipo_conta_bancaria AS tcb ON (
	cb.TIPO_CONTA_BANCARIA_ID = tcb.id
);

CREATE TABLE indexed_demonstrativo_desembolsos AS
SELECT
	p2.*
FROM
	parcelas p2
	INNER JOIN notas AS n2 ON (p2.idNota = n2.idNotas)
WHERE
	p2.CONCILIADO = 'S'
	AND p2.`status` = 'Processada'
	AND n2.tipo = 1;

CREATE TABLE indexed_demonstrativo_recebimentos AS
	SELECT
		p1.*
	FROM
		parcelas p1
		INNER JOIN notas AS n1 ON (p1.idNota = n1.idNotas)
	WHERE
		p1.CONCILIADO = 'S'
		AND p1.`status` = 'Processada'
		AND n1.tipo = 0;

