UPDATE
exames_laboratoriais_tags
SET
tipo_exame = 18
WHERE
tipo_exame = 43 ;

UPDATE
exames_laboratoriais_tags
SET
tipo_exame = 36
WHERE
tipo_exame = 28;

UPDATE
exames_laboratoriais_tags
SET
tipo_exame = 3
WHERE
tipo_exame = 38 ;

delete from exames_laboratoriais where id in (28,38,43);

UPDATE `exames_laboratoriais` SET `descricao`='COAGULOGRAMA' WHERE (`id`='15');

ALTER TABLE `exames_laboratoriais`
ADD UNIQUE INDEX `descricao` (`descricao`) ;

INSERT INTO `exames_laboratoriais` (`id`, `descricao`) VALUES ('', 'ANTIBIOGRAMA');
INSERT INTO `exames_laboratoriais` (`id`, `descricao`) VALUES ('', 'ERITROPOETINA');
INSERT INTO `exames_laboratoriais` (`id`, `descricao`) VALUES ('', 'HB1AC');
INSERT INTO `exames_laboratoriais` (`id`, `descricao`) VALUES ('', 'COBALAMINA (VIT B12)');
INSERT INTO `exames_laboratoriais` (`id`, `descricao`) VALUES ('', 'VHS');
