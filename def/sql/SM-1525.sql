CREATE TABLE `custos_extras` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`nome`  varchar(250) NOT NULL ,
`created_by`  int(11) NOT NULL ,
`created_at`  datetime NOT NULL ,
`status`  enum('D','A') NOT NULL DEFAULT 'A' COMMENT 'A- ativo, D-desativado' ,
`updated_at`  datetime NULL DEFAULT NULL ,
`updated_by`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
COMMENT='Custos extras utilizados no rel. margem de contribuição'
;

CREATE TABLE `fatura_custos_extras` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`id_custo_extra`  int(11) NOT NULL ,
`id_fatura`  int(11) NOT NULL ,
`quantidade`  int(11) NOT NULL ,
`custo`  float NOT NULL ,
`created_at`  datetime NOT NULL ,
`created_by`  int(11) NOT NULL ,
`canceled_at`  datetime NULL ,
`canceled_by`  int(11) NULL ,
PRIMARY KEY (`id`)
)
COMMENT='Custos extras lançados na fatura';
;

