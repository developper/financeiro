-- CRIAR OPERADORAS
INSERT INTO `operadoras` (`ID`, `NOME`) VALUES ('30', 'COPASS SAÚDE');
INSERT INTO `operadoras` (`ID`, `NOME`) VALUES ('31', 'AMIL BLUE');
INSERT INTO `operadoras` (`ID`, `NOME`) VALUES ('32', 'CONAB');
INSERT INTO `operadoras` (`ID`, `NOME`) VALUES ('33', 'TRIBUNAL SUPERIOR DO TRABALHO');
INSERT INTO `operadoras` (`ID`, `NOME`) VALUES ('34', 'SENADO');
INSERT INTO `operadoras` (`ID`, `NOME`) VALUES ('35', 'SECRETARIA MUNICIPAL DE SAUDE / SSA');
INSERT INTO `operadoras` (`ID`, `NOME`) VALUES ('36', 'IPASEAL SAÚDE');
INSERT INTO `operadoras` (`ID`, `NOME`) VALUES ('37', 'FIOCRUZ');
INSERT INTO `operadoras` (`ID`, `NOME`) VALUES ('38', 'CASSEB');
INSERT INTO `operadoras` (`ID`, `NOME`) VALUES ('39', 'PLAMED');
INSERT INTO `operadoras` (`ID`, `NOME`) VALUES ('40', 'UNIMED ITABUNA');
INSERT INTO `operadoras` (`ID`, `NOME`) VALUES ('41', 'HAPVIDA');
INSERT INTO `operadoras` (`ID`, `NOME`) VALUES ('42', 'MINISTÉRIO PÚBLICO DA UNIÃO');

-- ROLBACK CRIAR OPERADORAS
DELETE FROM `operadoras` WHERE `ID` IN (30,31,32,33,34,35,36,37,38,39,40,41,42);

-- criar de para operadoras x planos
INSERT INTO `operadoras_planosdesaude` (`OPERADORAS_ID`, `PLANOSDESAUDE_ID`) VALUES ('30', '86');
INSERT INTO `operadoras_planosdesaude` (`OPERADORAS_ID`, `PLANOSDESAUDE_ID`) VALUES ('31', '92');
INSERT INTO `operadoras_planosdesaude` (`OPERADORAS_ID`, `PLANOSDESAUDE_ID`) VALUES ('32', '95');
INSERT INTO `operadoras_planosdesaude` (`OPERADORAS_ID`, `PLANOSDESAUDE_ID`) VALUES ('33', '96');
INSERT INTO `operadoras_planosdesaude` (`OPERADORAS_ID`, `PLANOSDESAUDE_ID`) VALUES ('42', '97');
INSERT INTO `operadoras_planosdesaude` (`OPERADORAS_ID`, `PLANOSDESAUDE_ID`) VALUES ('34', '98');
INSERT INTO `operadoras_planosdesaude` (`OPERADORAS_ID`, `PLANOSDESAUDE_ID`) VALUES ('35', '99');
INSERT INTO `operadoras_planosdesaude` (`OPERADORAS_ID`, `PLANOSDESAUDE_ID`) VALUES ('36', '102');
INSERT INTO `operadoras_planosdesaude` (`OPERADORAS_ID`, `PLANOSDESAUDE_ID`) VALUES ('37', '104');
INSERT INTO `operadoras_planosdesaude` (`OPERADORAS_ID`, `PLANOSDESAUDE_ID`) VALUES ('38', '105');
INSERT INTO `operadoras_planosdesaude` (`OPERADORAS_ID`, `PLANOSDESAUDE_ID`) VALUES ('39', '106');
INSERT INTO `operadoras_planosdesaude` (`OPERADORAS_ID`, `PLANOSDESAUDE_ID`) VALUES ('40', '107');
INSERT INTO `operadoras_planosdesaude` (`OPERADORAS_ID`, `PLANOSDESAUDE_ID`) VALUES ('41', '108');
------ rolback 
DELETE FROM `operadoras_planosdesaude`  where OPERADORAS_ID = 30 and PLANOSDESAUDE_ID = 86;
DELETE FROM `operadoras_planosdesaude`  where OPERADORAS_ID = 31 and PLANOSDESAUDE_ID = 92;
DELETE FROM `operadoras_planosdesaude`  where OPERADORAS_ID = 32 and PLANOSDESAUDE_ID = 95;
DELETE FROM `operadoras_planosdesaude`  where OPERADORAS_ID = 33 and PLANOSDESAUDE_ID = 96;
DELETE FROM `operadoras_planosdesaude`  where OPERADORAS_ID = 42 and PLANOSDESAUDE_ID = 97;
DELETE FROM `operadoras_planosdesaude`  where OPERADORAS_ID = 34 and PLANOSDESAUDE_ID = 98;
DELETE FROM `operadoras_planosdesaude`  where OPERADORAS_ID = 35 and PLANOSDESAUDE_ID = 99;
DELETE FROM `operadoras_planosdesaude`  where OPERADORAS_ID = 36 and PLANOSDESAUDE_ID = 102;
DELETE FROM `operadoras_planosdesaude`  where OPERADORAS_ID = 37 and PLANOSDESAUDE_ID = 104;
DELETE FROM `operadoras_planosdesaude`  where OPERADORAS_ID = 38 and PLANOSDESAUDE_ID = 105;
DELETE FROM `operadoras_planosdesaude`  where OPERADORAS_ID = 39 and PLANOSDESAUDE_ID = 106;
DELETE FROM `operadoras_planosdesaude`  where OPERADORAS_ID = 40 and PLANOSDESAUDE_ID = 107;
DELETE FROM `operadoras_planosdesaude`  where OPERADORAS_ID = 41 and PLANOSDESAUDE_ID = 108;

