ALTER TABLE `remocoes`
ADD COLUMN `remocao_sera_cobrada`  varchar(1) NULL COMMENT 'S =Sim, N = Não' AFTER `valor_enfermeiro`,
ADD COLUMN `modo_cobranca`  varchar(2) NULL COMMENT 'F- fatura, NF - nota fiscal' AFTER `remocao_sera_cobrada`,
ADD COLUMN `justificativa_nao_cobrar`  text NULL AFTER `modo_cobranca`;