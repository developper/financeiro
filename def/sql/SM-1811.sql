CREATE TABLE `historico_autorizacao_itens`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autorizacao_itens_id` int(11) NOT NULL,
  `qtd_autorizada` int(11) NOT NULL DEFAULT 0,
  `status_item` tinyint(1) NOT NULL COMMENT '1 = Autorizado / 2 = Autorizado Parcial / 3 = Negado',
  `data_autorizacao` date NULL DEFAULT NULL,
  `autorizado_por` int(11) NULL DEFAULT NULL,
  `autorizado_em` datetime NULL DEFAULT NULL,
  `cancelado_por` int(11) NULL DEFAULT NULL,
  `cancelado_em` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

ALTER TABLE `sismederi`.`historico_autorizacao_itens`
ADD COLUMN `origem_item_id` int(0) NULL AFTER `cancelado_em`;


-- aditivas
SELECT
solicitacoes.*
from
solicitacoes as S
INNER JOIN
relatorio_aditivo_enf  on (S.`relatorio_aditivo_enf_id`=rel_enf_adt.ID)
where
relatorio_aditivo_enf.INICIO between {$inicio} and {$fim}
and
relatorio_aditivo_enf.FIM between {$inicio} and {$fim}
and
solicitacoes.CATALOGO_ID = {$catalogo}
and
solicitacoes.tipo = {$tipo}
and
solicitacoes.status not in (-1,-2)
and
solicitacoes.autorizado < solicitacoes.qtd

UNION

SELECT
solicitacoes.*
from
solicitacoes
INNER JOIN
prescricoes  on (solicitacoes.`idPrescricao`= prescricoes.id)
where
prescricoes.inicio between {$inicio} and {$fim}
and
prescricoes.fim between {$inicio} and {$fim}
and
solicitacoes.CATALOGO_ID = {$catalogo}
and
solicitacoes.tipo = {$tipo}
and
solicitacoes.status not in (-1,-2)
and
solicitacoes.autorizado < solicitacoes.qtd
AND
prescricoes.Carater = 3
order by asc
solicitacoes.idSolicitacoes

--

-- prorrogacao
SELECT
solicitacoes.*
from
solicitacoes
INNER JOIN
prescricoes  on (solicitacoes.`idPrescricao`= prescricoes.id)
where
prescricoes.inicio between {$inicio} and {$fim}
and
prescricoes.fim between {$inicio} and {$fim}
and
solicitacoes.CATALOGO_ID = {$catalogo}
and
solicitacoes.tipo = {$tipo}
and
solicitacoes.status not in (-1,-2)
and
solicitacoes.autorizado < solicitacoes.qtd
AND
prescricoes.Carater = 2
order by asc
solicitacoes.idSolicitacoes