CREATE TABLE `evento_aph` (
`id`  int(11) NULL AUTO_INCREMENT ,
`data`  datetime NOT NULL ,
`hora`  time NOT NULL ,
`solicitante`  varchar(255) NOT NULL ,
`telefone`  varchar(255) NOT NULL ,
`tipo_servico`  enum('APH','EV') NOT NULL COMMENT 'EV- Evento, APH' ,
`publico_alvo`  enum('I','A') NOT NULL COMMENT 'A=Adulto I=Infantil' ,
`endereco`  text NOT NULL ,
`cidade_id`  int(11) NULL ,
`complemento`  varchar(225) NULL ,
`observacao`  text NULL ,
`criado_em`  datetime NULL ,
`criado_por`  int(11) NULL ,
`finalizado_em`  datetime NULL ,
`finalizado_por`  int(11) NULL ,
`cancelado_em`  datetime NULL ,
`cancelado_por`  int(11) NULL ,
PRIMARY KEY (`id`)
)
;
ALTER TABLE `evento_aph`
MODIFY COLUMN `data`  date NOT NULL AFTER `id`;

CREATE TABLE `evento_aph_finalizar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
	`evento_aph_id` int(11) DEFAULT NULL,
  `fornecedor_cliente` int(11) DEFAULT NULL,
  `valor` double NOT NULL,
  `outros_custos` double DEFAULT NULL,
  `outras_despesas` double DEFAULT NULL,
  `data_termino` date DEFAULT NULL,
  `hora_termino` time DEFAULT NULL,
  `medico` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `valor_medico` double NOT NULL,
  `tecnico` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `valor_tecnico` double NOT NULL,
  `condutor` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `valor_condutor` double NOT NULL,
	`enfermeiro` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valor_enfermeiro` double DEFAULT NULL,
  `recebido` enum('S','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `forma_pagamento` enum('DIN','CHQ','CRT','TRB') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DIN=Dinheiro, CHQ= Cheque, CRT=Cartão,TRB= Transação Bancária',
  `observacao_final` text COLLATE utf8_unicode_ci,
   `empresa_id` int(11) DEFAULT NULL,
  `sera_cobrada` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'S =Sim, N = Não',
  `justificativa_nao_cobrar` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

CREATE TABLE `evento_aph_itens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `evento_aph_id` int(11) NOT NULL,
  `catalogo_id` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `tiss` varchar(15) DEFAULT NULL,
  `qtd` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci  COMMENT='Tabela de itens adicionados ao finalizar um Evento/APH';