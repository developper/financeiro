ALTER TABLE `empresas` 
ADD COLUMN `iw_id` int(11) NULL AFTER `ur_controle_estoque`;

UPDATE `empresas` SET `iw_id` = 1 WHERE `id` = 1;
UPDATE `empresas` SET `iw_id` = 2 WHERE `id` = 4;
UPDATE `empresas` SET `iw_id` = 7 WHERE `id` = 5;
UPDATE `empresas` SET `iw_id` = 5 WHERE `id` = 11;
UPDATE `empresas` SET `iw_id` = 3 WHERE `id` = 15;
UPDATE `empresas` SET `iw_id` = 4 WHERE `id` = 16;

ALTER TABLE `notas` 
DROP COLUMN `importada_iw`,
ADD COLUMN `importada_iw` enum('N','S') NULL DEFAULT 'N' AFTER `nota_aglutinacao`;