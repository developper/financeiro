ALTER TABLE `quadro_feridas_enf_status` ADD `relatorio` ENUM('AVALIAÇÃO', 'ADITIVO', 'EVOLUÇÃO', 'PRORROGAÇÃO')  NULL  AFTER `status_ferida`;

INSERT INTO `quadro_feridas_enf_status` (`status_ferida`, `relatorio`)
VALUES
  ('PRÉ-ADMISSÃO DO PAD', 'AVALIAÇÃO');

