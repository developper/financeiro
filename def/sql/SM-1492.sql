SELECT
equipamentosativos.ID,
clientes.nome as paciente,
empresas.nome as ur,
cobrancaplanos.item as equipamento
FROM
equipamentosativos
INNER JOIN clientes ON equipamentosativos.PACIENTE_ID = clientes.idClientes
INNER JOIN empresas ON clientes.empresa = empresas.id
INNER JOIN cobrancaplanos ON equipamentosativos.COBRANCA_PLANOS_ID = cobrancaplanos.id
WHERE

DATA_FIM = '0000-00-00 00:00:00'
and
DATA_INICIO > '0000-00-00 00:00:00'
AND
equipamentosativos.PRESCRICAO_AVALIACAO <> 's'
AND
PACIENTE_ID
IN
(168,
209,
317,
377,
465,
657,
892,
1132,
1225,
1412,
1417)
ORDER BY PACIENTE_ID, equipamento, ur

-- desativar item.
UPDATE `cobrancaplanos` SET `ATIVO`='0' WHERE (`id`='31')

-- Bateria.
INSERT INTO `cobrancaplanos` (`item`, `unidade`, `tipo`, `CUSTO`) VALUES ('BATERIA', 'UNIDADE', '1', '0');