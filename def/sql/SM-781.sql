SELECT
	mhc.modalidade as modd,
	e.nome AS ur,
	COUNT(maxpacientes)
FROM
	relatorio_prorrogacao_med rpm
INNER JOIN (
	SELECT
		MAX(ID) AS maxpacientes
	FROM
		relatorio_prorrogacao_med rpm1
	INNER JOIN clientes c1 ON rpm1.PACIENTE_ID = c1.idClientes
	WHERE
		c1.`status` = 4
	AND MOD_HOME_CARE != 0
	GROUP BY
		rpm1.PACIENTE_ID
) AS virtual ON rpm.ID = virtual.maxpacientes
INNER JOIN clientes c ON rpm.PACIENTE_ID = c.idClientes
INNER JOIN empresas e ON c.empresa = e.id
INNER JOIN modalidade_home_care mhc ON rpm.MOD_HOME_CARE = mhc.id
GROUP BY
	c.empresa,
	rpm.MOD_HOME_CARE
UNION
	SELECT
		mhc.modalidade  as modd,
		e.nome AS ur,
		COUNT(maxpacientes)
	FROM
		fichamedicaevolucao fme
	INNER JOIN (
		SELECT
			MAX(ID) AS maxpacientes
		FROM
			fichamedicaevolucao fme1
		INNER JOIN clientes c2 ON fme1.PACIENTE_ID = c2.idClientes
		WHERE
			c2.`status` = 4
		AND MOD_HOME_CARE != 0
		GROUP BY
			fme1.PACIENTE_ID
	) AS virtual ON fme.ID = virtual.maxpacientes
	INNER JOIN clientes c ON fme.PACIENTE_ID = c.idClientes
	INNER JOIN empresas e ON c.empresa = e.id
	INNER JOIN modalidade_home_care mhc ON fme.MOD_HOME_CARE = mhc.id
	WHERE
		fme.PACIENTE_ID NOT IN (
			SELECT
				PACIENTE_ID
			FROM
				relatorio_prorrogacao_med rpm1
			INNER JOIN clientes c1 ON rpm1.PACIENTE_ID = c1.idClientes
			WHERE
				c1.`status` = 4
			GROUP BY
				rpm1.PACIENTE_ID
		)
	GROUP BY
		c.empresa,
		fme.MOD_HOME_CARE
	UNION
		SELECT
			mhc.modalidade as modd,
			e.nome AS ur,
			COUNT(maxpacientes)
		FROM
			capmedica cm
		INNER JOIN (
			SELECT
				MAX(ID) AS maxpacientes
			FROM
				capmedica cm1
			INNER JOIN clientes c2 ON cm1.paciente = c2.idClientes
			WHERE
				c2.`status` = 4
			GROUP BY
				cm1.paciente
		) AS virtual ON cm.id = virtual.maxpacientes
		INNER JOIN clientes c ON cm.paciente = c.idClientes
		INNER JOIN empresas e ON c.empresa = e.id
		INNER JOIN modalidade_home_care mhc ON cm.MODALIDADE = mhc.id
		WHERE
			cm.paciente NOT IN (
				SELECT
					PACIENTE_ID
				FROM
					relatorio_prorrogacao_med rpm1
				INNER JOIN clientes c1 ON rpm1.PACIENTE_ID = c1.idClientes
				WHERE
					c1.`status` = 4
				GROUP BY
					rpm1.PACIENTE_ID
			)
		AND cm.paciente NOT IN (
			SELECT
				PACIENTE_ID
			FROM
				fichamedicaevolucao fme1
			INNER JOIN clientes c2 ON fme1.PACIENTE_ID = c2.idClientes
			WHERE
				c2.`status` = 4
			GROUP BY
				fme1.PACIENTE_ID
		)
		GROUP BY
			c.empresa,
			cm.MODALIDADE
		ORDER BY
			ur, modd