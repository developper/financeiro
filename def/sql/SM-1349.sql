-- Consulta que busca itens descartaveis, estoque da matriz, e saidas da Matriz.

SELECT
catalogo.ID,
concat(catalogo.principio,' ',catalogo.apresentacao)as  Item,
estoque.empresas_id_1 as Estoque_Matriz,
COALESCE (Y.Xqtd,0) as Saida_Matriz
FROM
catalogo
LEFT JOIN estoque on catalogo.ID = estoque.CATALOGO_ID
LEFT JOIN (
		SELECT
		catalogo.ID as Xid,
		SUM(saida.quantidade)as Xqtd
		FROM
		catalogo
		LEFT JOIN saida on catalogo.ID = saida.CATALOGO_ID
		WHERE
		catalogo.tipo = 1 AND
		catalogo.ATIVO = 'A' AND
		catalogo.TAG = 'DESCARTAVEL'
		AND
		saida.UR in (2,4,5,7,11)
		AND
		saida.`data` > '2016-02-01'
		GROUP BY catalogo.ID

) as Y on catalogo.ID = Y.Xid
WHERE
catalogo.tipo = 1 AND
catalogo.ATIVO = 'A' AND
catalogo.TAG = 'DESCARTAVEL'