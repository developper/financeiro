--Necessário para filtrar opções de profissionais dentro do relatório de alta
ALTER TABLE `cuidadosespeciais`
ADD COLUMN `ALTA`  int(10) NULL DEFAULT 0 COMMENT 'DEFINE QUAIS ITENS APARECERÃO NA ALTA MÉDICA' AFTER `GRUPO`;

UPDATE `cuidadosespeciais` SET `ALTA`='1' WHERE (`id`='11');
UPDATE `cuidadosespeciais` SET `ALTA`='1' WHERE (`id`='22');
UPDATE `cuidadosespeciais` SET `ALTA`='1' WHERE (`id`='23');
UPDATE `cuidadosespeciais` SET `ALTA`='1' WHERE (`id`='16');
UPDATE `cuidadosespeciais` SET `ALTA`='1' WHERE (`id`='12');
UPDATE `cuidadosespeciais` SET `ALTA`='1' WHERE (`id`='14');