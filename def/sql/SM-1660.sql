ALTER TABLE `devolucao_interna_pedido`
MODIFY COLUMN `status`  enum('FINALIZADO','AUTORIZADO','CANCELADO','PENDENTE') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `motivo_cancelamento`;

ALTER TABLE `devolucao_interna_pedido`
CHANGE COLUMN `aceitado_at` `accepted_at`  datetime NULL DEFAULT NULL AFTER `accepted_by`;

ALTER TABLE `devolucao_interna_itens`
ADD COLUMN `autorizado`  int(11) NULL AFTER `devolucao_interna_motivo_id`;