
--1 passo
INSERT INTO `plano_contas` (`ID`, `N1`, `COD_N2`, `N2`, `COD_N3`, `N3`, `COD_N4`, `N4`) VALUES (346, 'R', '1.9', 'OUTROS RECEBIMENTOS', '1.9.9', 'OUTROS RECEBIMENTOS', '1.9.9.91', 'VALORES REEMBOLSADOS (SÓCIOS)');
INSERT INTO `plano_contas` (`ID`, `N1`, `COD_N2`, `N2`, `COD_N3`, `N3`, `COD_N4`, `N4`) VALUES (347, 'D', '2.1', 'Desembolsos Administrativos', '2.1.1', 'PESSOAL', '2.1.1.98', 'VALORES A REEMBOLSAR');
INSERT INTO `plano_contas` (`ID`, `N1`, `COD_N2`, `N2`, `COD_N3`, `N3`, `COD_N4`, `N4`) VALUES (348, 'D', '2.2', 'Desembolsos Operacionais', '2.2.1', 'PESSOAL', '2.2.1.98', 'VALORES A REEMBOLSAR');

-- Rollback

DELETE from planos_contas where ID in (346,347,348)
