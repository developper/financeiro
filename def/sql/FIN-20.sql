
-- Criando coluna para guarda histórico das  baixa das previsões
CREATE TABLE `parcela_baixa_previsao` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
  `parcela_previsao_id` int(11) NOT NULL,
  `parcela_nota_id` int(11) NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
   PRIMARY KEY (`ID`) 
  
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;
