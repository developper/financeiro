ALTER TABLE `fornecedores` 
ADD COLUMN `precisa_classificar` char(1) NULL DEFAULT NULL COMMENT 'Recebe null, S, ou N' AFTER `aliquota_inss`,
ADD COLUMN `classificacao` char(1) NOT NULL COMMENT 'Recebe null, A, B, C, D' AFTER `precisa_classificar`;