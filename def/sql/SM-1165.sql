SELECT
  CONCAT(catalogo.apresentacao, ' ', catalogo.principio) AS nomeItem,
  entrada.qtd,
  entrada.valor,
  DATE_FORMAT(entrada.data, '%d/%m/%Y') as datahora
FROM
  entrada
  INNER JOIN catalogo ON entrada.CATALOGO_ID = catalogo.ID
WHERE
  catalogo.tipo = 1
  AND entrada.data BETWEEN '2016-08-01' AND '2016-08-31';