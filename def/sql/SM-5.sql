ALTER TABLE `cuidadosespeciais`
ADD COLUMN `JUSTIFICATIVA`  varchar(255) CHARACTER SET UTF8 COLLATE UTF8_general_ci NULL DEFAULT NULL COMMENT 'S-se o serviço possui justificativa cadastrada no sistema.N-se não possui justificativa cadastrada' AFTER `COBRANCAPLANO_ID`;


ALTER TABLE `cuidadosespeciais`
ADD COLUMN `GRUPO` int(10) NULL COMMENT '1-MED 2-ENF 3-TECNICO 4-FISIO MOTOR 5-FISIO RESPIR 6-NUTRI 7-FONO' AFTER `JUSTIFICATIVA`;

UPDATE cuidadosespeciais set JUSTIFICATIVA = 'N' WHERE id not in (2,4,5,6,7,8,9,14,22,23,25,26,28,29,30,31,32,33,34);
UPDATE cuidadosespeciais set JUSTIFICATIVA = 'S' WHERE id in (2,4,5,6,7,8,9,14,22,23,25,26,28,29,30,31,32,33,34);


UPDATE cuidadosespeciais SET GRUPO = 1 WHERE id in (7,8,9);
UPDATE cuidadosespeciais SET GRUPO = 2 WHERE id in (6,32,33,34);
UPDATE cuidadosespeciais SET GRUPO = 3 WHERE id in (2,4,5,28,29,30,31);
UPDATE cuidadosespeciais SET GRUPO = 4 WHERE id = 22;
UPDATE cuidadosespeciais SET GRUPO = 5 WHERE id = 23;
UPDATE cuidadosespeciais SET GRUPO = 6 WHERE id in (14,25);
UPDATE cuidadosespeciais SET GRUPO = 7 WHERE id in (11,26);

ALTER TABLE `profissionais_relatorio_prorrogacao_med`;
ADD COLUMN `JUSTIFICATIVA_ID` int(11) NOT NULL DEFAULT -2 AFTER `RELATORIO_ALTA_MED_ID`;