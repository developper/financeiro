-- Consulta para Material, dieta e medicamento
SELECT
CONCAT(catalogo.principio,' - ',catalogo.apresentacao) as item,
if (solicitacoes.JUSTIFICATIVA_AVULSO_ID is NULL ,solicitacoes.JUSTIFICATIVA_AVULSO,justificativa_solicitacao_avulsa.justificativa) as just
FROM
solicitacoes
INNER JOIN catalogo ON catalogo.ID = solicitacoes.CATALOGO_ID
LEFT JOIN  justificativa_solicitacao_avulsa ON justificativa_solicitacao_avulsa.id = solicitacoes.JUSTIFICATIVA_AVULSO_ID
WHERE
solicitacoes.DATA_SOLICITACAO >= '2014-01-10 00:00:00' AND
solicitacoes.tipo IN (0,1,3) AND
(solicitacoes.idPrescricao = -1 OR
solicitacoes.entrega_imediata = 'S')
ORDER BY
item ASC