CREATE TABLE tipo_documento_financeiro  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `sigla` varchar(255) NULL,
  `descricao` varchar(255) NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (1, 'BOL', 'BOLETO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (2, 'CC', 'CARTAO DE CREDITO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (3, 'CFI', 'CARTAO DE FINANCIAMENTO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (4, 'CH', 'CHEQUE');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (5, 'COF', 'COFINS');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (6, 'CR', 'CREDITO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (7, 'CSL', 'CSLL');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (8, 'CSS', 'CSSL');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (9, 'DH', 'DINHEIRO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (10, 'FER', 'FERIAS');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (11, 'FG', 'GFIP');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (12, 'FI', 'FINANCIADO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (13, 'FOL', 'FOLHA DE PAGAMENTO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (14, 'FT', 'FATURA');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (15, 'INS', 'INSS');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (16, 'IRF', 'IRRF');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (17, 'ISS', 'TITULO DE ISS');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (18, 'JP', 'JUROS');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (19, 'NDC', 'NOTA DEBITO CLIENTE');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (20, 'NDF', 'NOTA DEBITO FORNECEDOR');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (21, 'NF', 'NOTA FISCAL');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (22, 'NP', 'PRESTACAO DE CONTAS');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (23, 'PA', 'PAGAMENTO ANTECIPADO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (24, 'PIS', 'PIS');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (25, 'PR', 'TITULO PROVISORIO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (26, 'RA', 'RECEBIMENTO ANTECIPADO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (27, 'RC', 'RECIBO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (28, 'RES', 'RESCISAO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (29, 'RPA', 'RPA');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (30, 'TF', 'DEPOSITO');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (31, 'TX', 'TITULO DE TAXAS');
INSERT INTO `tipo_documento_financeiro`(`id`, `sigla`, `descricao`) VALUES (32, 'PCC', 'PCC');


ALTER TABLE `notas` 
ADD COLUMN `tipo_documento_financeiro_id` int(0) NOT NULL AFTER `nota_fatura`;