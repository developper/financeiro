SELECT
clientes.nome as Paciente ,
solicitacoes.DATA_SOLICITACAO ,
( case solicitacoes.TIPO_SOLICITACAO
WHEN -2 then 'EMERGENCIA'
WHEN -1 then 'AVULSA'
WHEN 2 then 'PERIODICA MEDICA'
WHEN 3 THEN 'ADITIVA MEDICA'
WHEN 4 THEN 'AVALIA��O'
WHEN 5 THEN ' PERIODICA NUTRICAO'
WHEN 6 THEN 'ADITIVA NUTRICAO'
WHEN 7 THEN 'ADITIVA AVALIACAO'
WHEN 8 THEN 'ADITIVA ENFERMAGEM'
WHEN 9 THEN  'PERIODICA ENFERMAGEM'
WHEN 10 THEN  'AVALIACAO ENFERMAGEM'
WHEN 11 THEN  'SUSPENS�O'
END) AS TIPO_SOL,
COUNT(paciente) as ITENS,
statuspaciente.`status` as STATUS_PACIENTE
FROM
solicitacoes
INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
INNER JOIN statuspaciente on clientes.`status` = statuspaciente.id
WHERE
solicitacoes.DATA_SOLICITACAO > '2016-07-01' AND
clientes.empresa=11
and clientes.idClientes  NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
GROUP BY paciente,DATA_SOLICITACAO
order by nome, DATA_SOLICITACAO