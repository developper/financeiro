ALTER TABLE `impostos_tarifas` 
MODIFY COLUMN `tipo` enum('IMPOSTO','TARIFA','DESCONTO') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `item`;

INSERT INTO `impostos_tarifas`(`item`, `tipo`, `debito_credito`, `ipcf`) VALUES ('BÔNUS', 'DESCONTO', 'CREDITO', '1.0.2.02');