CREATE TABLE `tipo_conta_bancaria` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `abreviacao` varchar(5) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tipo_conta_bancaria` (`id`, `nome`, `abreviacao`)
VALUES
	(1, 'Conta-corrente', 'C/C'),
	(2, 'Poupança', 'C/P'),
	(3, 'Investimento', 'INV.');

ALTER TABLE `contas_bancarias` ADD `TIPO_CONTA_BANCARIA_ID` INT  NULL  DEFAULT NULL  AFTER `ID`;
