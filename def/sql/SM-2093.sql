-- ----------------------------
-- Table structure for esus_condicoes_avaliadas
-- ----------------------------
DROP TABLE IF EXISTS `esus_condicoes_avaliadas`;
CREATE TABLE `esus_condicoes_avaliadas`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `descricao` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of esus_condicoes_avaliadas
-- ----------------------------
INSERT INTO `esus_condicoes_avaliadas` VALUES (1, '1', 'Acamado');
INSERT INTO `esus_condicoes_avaliadas` VALUES (2, '2', 'Domiciliado');
INSERT INTO `esus_condicoes_avaliadas` VALUES (3, '3', 'Úlceras / Feridas (grau III ou IV)');
INSERT INTO `esus_condicoes_avaliadas` VALUES (4, '4', 'Acompanhamento nutricional');
INSERT INTO `esus_condicoes_avaliadas` VALUES (5, '5', 'Uso de sonda naso-gástrica - SNG');
INSERT INTO `esus_condicoes_avaliadas` VALUES (6, '6', 'Uso de sonda naso-enteral - SNE');
INSERT INTO `esus_condicoes_avaliadas` VALUES (7, '7', 'Uso de gastrostomia');
INSERT INTO `esus_condicoes_avaliadas` VALUES (8, '8', 'Uso de colostomia');
INSERT INTO `esus_condicoes_avaliadas` VALUES (9, '9', 'Uso de cistostomia');
INSERT INTO `esus_condicoes_avaliadas` VALUES (10, '10', 'Uso de sonda vesical de demora - SVD');
INSERT INTO `esus_condicoes_avaliadas` VALUES (11, '11', 'Acompanhamento pré-operatório');
INSERT INTO `esus_condicoes_avaliadas` VALUES (12, '12', 'Acompanhamento pós-operatório');
INSERT INTO `esus_condicoes_avaliadas` VALUES (13, '13', 'Adaptação ao uso de órtese / prótese');
INSERT INTO `esus_condicoes_avaliadas` VALUES (14, '14', 'Reabilitação domiciliar');
INSERT INTO `esus_condicoes_avaliadas` VALUES (15, '15', 'Cuidados paliativos oncológico');
INSERT INTO `esus_condicoes_avaliadas` VALUES (16, '16', 'Cuidados paliativos não-oncológico');
INSERT INTO `esus_condicoes_avaliadas` VALUES (17, '17', 'Oxigenoterapia domiciliar');
INSERT INTO `esus_condicoes_avaliadas` VALUES (18, '18', 'Uso de traqueostomia');
INSERT INTO `esus_condicoes_avaliadas` VALUES (19, '19', 'Uso de aspirador de vias aéreas para higiene brônquica');
INSERT INTO `esus_condicoes_avaliadas` VALUES (20, '20', 'Suporte ventilatório não invasivo - CPAP');
INSERT INTO `esus_condicoes_avaliadas` VALUES (21, '21', 'Suporte ventilatório não invasivo - BiPAP');
INSERT INTO `esus_condicoes_avaliadas` VALUES (22, '22', 'Diálise peritonial');
INSERT INTO `esus_condicoes_avaliadas` VALUES (23, '23', 'Paracentese');
INSERT INTO `esus_condicoes_avaliadas` VALUES (24, '24', 'Medicação parenteral');

-- ----------------------------
-- Table structure for esus_conduta_desfecho
-- ----------------------------
DROP TABLE IF EXISTS `esus_conduta_desfecho`;
CREATE TABLE `esus_conduta_desfecho`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `descricao` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `observacoes` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of esus_conduta_desfecho
-- ----------------------------
INSERT INTO `esus_conduta_desfecho` VALUES (1, '7', 'Permanência', NULL);
INSERT INTO `esus_conduta_desfecho` VALUES (2, '3', 'Alta administrativa', NULL);
INSERT INTO `esus_conduta_desfecho` VALUES (3, '1', 'Alta clínica', NULL);
INSERT INTO `esus_conduta_desfecho` VALUES (4, '9', 'Óbito', NULL);
INSERT INTO `esus_conduta_desfecho` VALUES (5, '2', 'Atenção Básica (AD1)', '#ENCAMINHAMENTO');
INSERT INTO `esus_conduta_desfecho` VALUES (6, '4', 'Serviço de urgência e emergência', NULL);
INSERT INTO `esus_conduta_desfecho` VALUES (7, '5', 'Serviço de internação hospitalar', NULL);

-- ----------------------------
-- Table structure for esus_local_de_atendimento
-- ----------------------------
DROP TABLE IF EXISTS `esus_local_de_atendimento`;
CREATE TABLE `esus_local_de_atendimento`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `descricao` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `observacoes` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of esus_local_de_atendimento
-- ----------------------------
INSERT INTO `esus_local_de_atendimento` VALUES (1, '1', 'UBS', NULL);
INSERT INTO `esus_local_de_atendimento` VALUES (2, '2', 'Unidade móvel', NULL);
INSERT INTO `esus_local_de_atendimento` VALUES (3, '3', 'Rua', NULL);
INSERT INTO `esus_local_de_atendimento` VALUES (4, '4', 'Domicílio', NULL);
INSERT INTO `esus_local_de_atendimento` VALUES (5, '5', 'Escola / Creche', NULL);
INSERT INTO `esus_local_de_atendimento` VALUES (6, '6', 'Outros', NULL);
INSERT INTO `esus_local_de_atendimento` VALUES (7, '7', 'Polo (academia da saúde)', NULL);
INSERT INTO `esus_local_de_atendimento` VALUES (8, '8', 'Instituição / Abrigo', NULL);
INSERT INTO `esus_local_de_atendimento` VALUES (9, '9', 'Unidade prisional ou congêneres', NULL);
INSERT INTO `esus_local_de_atendimento` VALUES (10, '10', 'Unidade socioeducativa', NULL);
INSERT INTO `esus_local_de_atendimento` VALUES (11, '11', 'Hospital', 'Utilizado apenas na Ficha de Atendimento Domiciliar');
INSERT INTO `esus_local_de_atendimento` VALUES (12, '12', 'Unidade de pronto atendimento', 'Utilizado apenas na Ficha de Atendimento Domiciliar');
INSERT INTO `esus_local_de_atendimento` VALUES (13, '13', 'CACON / UNACON', 'Utilizado apenas na Ficha de Atendimento Domiciliar');

-- ----------------------------
-- Table structure for esus_modalidade_ad
-- ----------------------------
DROP TABLE IF EXISTS `esus_modalidade_ad`;
CREATE TABLE `esus_modalidade_ad`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `descricao` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `observacoes` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of esus_modalidade_ad
-- ----------------------------
INSERT INTO `esus_modalidade_ad` VALUES (1, '1', 'AD 1', NULL);
INSERT INTO `esus_modalidade_ad` VALUES (2, '2', 'AD 2', NULL);
INSERT INTO `esus_modalidade_ad` VALUES (3, '3', 'AD 3', NULL);
INSERT INTO `esus_modalidade_ad` VALUES (4, '4', 'Inelegível', 'Utilizado apenas na Ficha de Avaliação de Elegibilidade');

-- ----------------------------
-- Table structure for esus_procedimentos_atencao_domiciliar
-- ----------------------------
DROP TABLE IF EXISTS `esus_procedimentos_atencao_domiciliar`;
CREATE TABLE `esus_procedimentos_atencao_domiciliar`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_sigtap` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `descricao` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of esus_procedimentos_atencao_domiciliar
-- ----------------------------
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (1, '0301070024', 'Acompanhamento de paciente em reabilitação em comunicação alternativa');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (2, '0301050082', 'Antibioticoterapia parenteral');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (3, '0301070075', 'Atendimento / Acompanhamento de paciente em reabilitação do desenvolvimento neuropsicomotor');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (4, '0302040021', 'Atendimento fisioterapêutico em paciente com transtorno respiratório sem complicações sistêmicas');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (5, '0301050090', 'Atendimento médico com finalidade de atestar óbito');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (6, '0301070067', 'Atendimento / Acompanhamento em reabilitação nas múltiplas deficiências');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (7, '0301100047', 'Cateterismo vesical de alívio');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (8, '0301100055', 'Cateterismo vesical de demora');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (9, '0201020041', 'Coleta de material para exame laboratorial');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (10, '0301100063', 'Cuidados com estomas');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (11, '0301100071', 'Cuidados com traqueostomia');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (12, '0301100098', 'Enema');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (13, '0301100144', 'Oxigenoterapia');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (14, '0301100152', 'Retirada de pontos de cirurgias básicas (por paciente)');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (15, '0301100179', 'Sondagem gástrica');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (16, '0301100187', 'Terapia de reidratação oral');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (17, '0301050120', 'Terapia de reidratação parenteral');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (18, '0301070113', 'Terapia fonoaudiológica individual');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (19, '0308010019', 'Tratamento de traumatismos de localização especificada / não especificada');
INSERT INTO `esus_procedimentos_atencao_domiciliar` VALUES (20, '0303190019', 'Tratamento em reabilitação');

-- ----------------------------
-- Table structure for esus_sexo
-- ----------------------------
DROP TABLE IF EXISTS `esus_sexo`;
CREATE TABLE `esus_sexo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `descricao` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `observacoes` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of esus_sexo
-- ----------------------------
INSERT INTO `esus_sexo` VALUES (1, '0', 'Masculino', NULL);
INSERT INTO `esus_sexo` VALUES (2, '1', 'Feminino', NULL);
INSERT INTO `esus_sexo` VALUES (3, '4', 'Ignorado', 'Apenas quando não foi informado o sexo do cidadão.');

-- ----------------------------
-- Table structure for esus_tipo_atendimento
-- ----------------------------
DROP TABLE IF EXISTS `esus_tipo_atendimento`;
CREATE TABLE `esus_tipo_atendimento`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `descricao` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `observacoes` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of esus_tipo_atendimento
-- ----------------------------
INSERT INTO `esus_tipo_atendimento` VALUES (1, '1', 'Consulta agendada programada / Cuidado continuado', '');
INSERT INTO `esus_tipo_atendimento` VALUES (2, '2', 'Consulta agendada', '');
INSERT INTO `esus_tipo_atendimento` VALUES (3, '4', 'Escuta inicial / Orientação', '');
INSERT INTO `esus_tipo_atendimento` VALUES (4, '5', 'Consulta no dia', '');
INSERT INTO `esus_tipo_atendimento` VALUES (5, '6', 'Atendimento de urgência', '');
INSERT INTO `esus_tipo_atendimento` VALUES (6, '7', 'Atendimento programado', 'Utilizado apenas nas Fichas de Atendimento Domiciliar');
INSERT INTO `esus_tipo_atendimento` VALUES (7, '8', 'Atendimento não programado', 'Utilizado apenas nas Fichas de Atendimento Domiciliar');
INSERT INTO `esus_tipo_atendimento` VALUES (8, '9', 'Visita domiciliar pós-óbito', 'Utilizado apenas nas Fichas de Atendimento Domiciliar');

-- ----------------------------
-- Table structure for esus_turno
-- ----------------------------
DROP TABLE IF EXISTS `esus_turno`;
CREATE TABLE `esus_turno`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `descricao` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `observacoes` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of esus_turno
-- ----------------------------
INSERT INTO `esus_turno` VALUES (1, '1', 'Manhã', NULL);
INSERT INTO `esus_turno` VALUES (2, '2', 'Tarde', NULL);
INSERT INTO `esus_turno` VALUES (3, '3', 'Noite', NULL);

create table esus_ficha_atendimento_domiciliar
(
  id                int auto_increment
    primary key,
  paciente_id       int         not null,
  paciente_cns      varchar(15) not null,
  data_nascimento   date        not null,
  sexo              char        not null,
  profissional_ator_id int not null,
  profissional_nome varchar(255) not null,
  profissional_cpf  varchar(14) not null,
  profissional_cns  varchar(15) not null,
  profissional_cbo  varchar(50) not null,
  profissional_ine  varchar(100) null,
  profissional_cnes  varchar(100) not null,
  data_visita       date        not null,
  turno             int         not null,
  local_atendimento int         not null,
  modalidade        int         not null,
  tipo_atendimento  int         not null,
  conduta_desfecho  int         not null,
  created_at        datetime    not null,
  created_by        int         not null,
  updated_at        datetime    not null,
  updated_by        int         not null,
  deleted_at        datetime    not null,
  deleted_by        int         not null
);

create table esus_ficha_atendimento_domiciliar_condicoes_avaliadas
(
  id int auto_increment,
  ficha_atendimento_domiciliar_id int not null,
  condicao_avaliada_id int not null,
  constraint esus_ficha_atendimento_domiciliar_condicoes_avaliadas_pk
    primary key (id)
);

create table esus_ficha_atendimento_domiciliar_cid10
(
  id int auto_increment,
  ficha_atendimento_domiciliar_id int not null,
  cid10 varchar(20) not null,
  constraint esus_ficha_atendimento_domiciliar_cid10_pk
    primary key (id)
);

create table esus_ficha_atendimento_domiciliar_ciap2
(
  id int auto_increment,
  ficha_atendimento_domiciliar_id int not null,
  ciap2_id varchar(20) not null,
  constraint esus_ficha_atendimento_domiciliar_ciap2_pk
    primary key (id)
);

create table esus_ficha_atendimento_domiciliar_procedimentos
(
  id int auto_increment,
  ficha_atendimento_domiciliar_id int not null,
  procedimento_id int not null,
  constraint esus_ficha_atendimento_domiciliar_procedimentos_pk
    primary key (id)
);

create table esus_ficha_atendimento_domiciliar_outros_procedimentos
(
  id int auto_increment,
  ficha_atendimento_domiciliar_id int not null,
  sigtap_id int not null,
  constraint esus_ficha_atendimento_domiciliar_outros_procedimentos_pk
    primary key (id)
);

alter table empresas
  add codigo_ibge varchar(50) null;

UPDATE empresas SET empresas.codigo_ibge = '2910800' WHERE id = 11;

DELETE FROM esus_procedimentos_sigtap WHERE codigo_sigtap IN (
  '0301070024'
  '0301050082'
  '0301070075'
  '0302040021'
  '0301050090'
  '0301070067'
  '0301100047'
  '0301100055'
  '0201020041'
  '0301100063'
  '0301100071'
  '0301100098'
  '0301100144'
  '0301100152'
  '0301100179'
  '0301100187'
  '0301050120'
  '0301070113'
  '0308010019'
  '0303190019'
);
