UPDATE `plano_contas` SET `COD_N3` = '1.0.2' WHERE `ID` = 5;
UPDATE `plano_contas` SET `COD_N3` = '1.0.2' WHERE `ID` = 6;
UPDATE `plano_contas` SET `COD_N3` = '1.0.2' WHERE `ID` = 7;
UPDATE `plano_contas` SET `COD_N3` = '1.0.2' WHERE `ID` = 8;
UPDATE `plano_contas` SET `COD_N3` = '1.0.2' WHERE `ID` = 9;
UPDATE `plano_contas` SET `COD_N3` = '1.0.3' WHERE `ID` = 10;
UPDATE `plano_contas` SET `COD_N3` = '1.0.3', `COD_N3`= '1.0.3.02' WHERE `ID` = 11;
UPDATE `plano_contas` SET `COD_N3` = '1.0.3' WHERE `ID` = 12;
UPDATE `plano_contas` SET `COD_N3` = '1.0.4' WHERE `ID` = 13;
UPDATE `plano_contas` SET `COD_N3` = '1.0.4' WHERE `ID` = 14;
UPDATE `plano_contas` SET `COD_N3` = '1.0.4' WHERE `ID` = 15;
UPDATE `plano_contas` SET `COD_N3` = '1.0.4' WHERE `ID` = 16;
UPDATE `plano_contas` SET `COD_N3` = '1.0.4' WHERE `ID` = 17;

UPDATE `plano_contas` SET N2    = 'RECEITA PREVISTA', COD_N2 = '1.2', COD_N3 = '1.2.1', COD_N4 = '1.2.1.01' WHERE `ID` = 18;
UPDATE `plano_contas` SET N2 = 'RECEITA PREVISTA', COD_N2 = '1.2', COD_N3 = '1.2.1', COD_N4 = '1.2.1.02' WHERE `ID` = 19;
UPDATE `plano_contas` SET N2 = 'RECEITA PREVISTA', COD_N2 = '1.2', COD_N3 = '1.2.1', COD_N4 = '1.2.1.03' WHERE `ID` = 20;

INSERT INTO `assinaturas`(`DESCRICAO`, `CODIGO_DOMINIO`) VALUES ('RECEITA -IMPORTAÇÃO IW', 990)

CREATE TABLE `notas_lote_conta_receber`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `nota_id` int(0) NOT NULL,
  `lote` varchar(255) NOT NULL COMMENT 'lote gerado no sistema meio que gera arquivo de importação',
  PRIMARY KEY (`id`)
);