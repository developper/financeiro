ALTER TABLE sismederi.fornecedores ADD observacao text NULL;
ALTER TABLE sismederi.fornecedores CHANGE observacao observacao text NULL AFTER chave_pix;
ALTER TABLE sismederi.fornecedores CHANGE created_by created_by int(11) NULL AFTER created_at;