CREATE TABLE `custo_item_fatura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catalogo_id` int(11) DEFAULT NULL,
  `cobrancaplano_id` int(11) DEFAULT NULL,
  `paciente_id` int(11) DEFAULT NULL,
  `tipo` int(1) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `custo` double(0,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `faturamento`
ADD COLUMN `custo_atualizado_em`  datetime NULL AFTER `TUSS`,
ADD COLUMN `custo_atualizado_por`  int(11) NULL AFTER `custo_atualizado_em`;