SELECT
UCASE(clientes.nome) as PACIENTE,
UCASE(itens_prescricao.nome) as  ITEM,
UCASE(empresas.nome)as EMPRESA,
DATE_FORMAT(itens_prescricao.inicio,'%d/%m/%Y') as INICIO,
DATE_FORMAT(itens_prescricao.fim,'%d/%m/%Y') as FIM,
itens_prescricao.CATALOGO_ID
FROM
prescricoes
INNER JOIN clientes ON prescricoes.paciente = clientes.idClientes
INNER JOIN itens_prescricao ON prescricoes.id = itens_prescricao.idPrescricao
INNER JOIN empresas ON clientes.empresa = empresas.id
WHERE
(
prescricoes.inicio BETWEEN '2017-02-01' AND  '2017-02-28' OR
prescricoes.fim BETWEEN  '2017-02-28' AND '2017-02-28')
AND
prescricoes.Carater in (5,6)
AND
tipo =14
AND
prescricoes.DESATIVADA_POR is not null