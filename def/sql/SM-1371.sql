-- consulta para Matriz
SELECT
catalogo.ID,
catalogo.lab_desc as LABORATORIO,
concat(catalogo.principio,' ',catalogo.apresentacao)as  Item,
COALESCE(E.qtdEntrada,0) as Quantidade_Entrada,
COALESCE(S.qtdSaida,0) as Quantidade_Saida,
if((COALESCE(E.qtdEntrada,0) + COALESCE(S.qtdSaida,0)) > 0, "SIM", "N�O" ) as MOVIMENTACAO,
ES.qtdEstoque as Estoque
FROM
catalogo

LEFT JOIN
(

		SELECT
		catalogo.ID,
		sum(saida.quantidade) as qtdSaida
		FROM
		catalogo
		LEFT JOIN saida on catalogo.ID = saida.CATALOGO_ID
		WHERE
		catalogo.tipo = 0 AND
		catalogo.ATIVO = 'A' AND
		saida.UR in (2,4,5,7,11) AND
		saida.`data` > '2016-07-01'
		GROUP BY catalogo.ID

) as S on  (catalogo.ID = S.ID )
LEFT JOIN
(

		SELECT
		catalogo.ID,
		sum(entrada.qtd) as qtdEntrada
		FROM
		catalogo
		LEFT JOIN entrada on catalogo.ID = entrada.CATALOGO_ID
		WHERE
		catalogo.tipo = 0 AND
		catalogo.ATIVO = 'A' 		AND
		entrada.UR = 1 AND
    entrada.`data` > '2016-07-01'
		GROUP BY catalogo.ID


) as E on  (catalogo.ID = E.ID )

LEFT JOIN
(
SELECT
		catalogo.ID,
		empresas_id_1 as qtdEstoque
		FROM
		catalogo
		LEFT JOIN estoque on catalogo.ID = estoque.CATALOGO_ID
		WHERE
		catalogo.tipo = 0 AND
		catalogo.ATIVO = 'A'
		GROUP BY catalogo.ID


) as ES on (catalogo.ID = ES.ID )
WHERE
catalogo.tipo = 0 AND
catalogo.ATIVO = 'A'
ORDER BY catalogo.ID


-- --------------------------------------- Consulta para urs
--- ----------------------------------------
-- SET @empresa = 'colocar ur da empresa' ;
-- deve trocar @empresaEstoque por empresa_id_"id da ur" para pegar a quantidade de movimenta��o da UR.
SET @empresa = 11;
SELECT
catalogo.ID,
catalogo.lab_desc as LABORATORIO,
concat(catalogo.principio,' ',catalogo.apresentacao)as  Item,
COALESCE(E.qtdEntrada,0) as Quantidade_Entrada,
COALESCE(EI.qtdEntradaInterna,0) as Quantidade_Entrada_Interna,
COALESCE(S.qtdSaida,0) as Quantidade_Saida,
IF((COALESCE(E.qtdEntrada,0) + COALESCE(S.qtdSaida,0) + COALESCE(EI.qtdEntradaInterna,0) ) > 0, "SIM", "N�O" ) as MOVIMENTACAO,
ES.qtdEstoque as Estoque
FROM
catalogo

LEFT JOIN
(

		SELECT
		catalogo.ID,
		sum(saida.quantidade) as qtdSaida
		FROM
		catalogo
		LEFT JOIN saida on catalogo.ID = saida.CATALOGO_ID
		LEFT JOIN clientes on saida.idCliente = clientes.idClientes
		WHERE
		catalogo.tipo = 0 AND
		catalogo.ATIVO = 'A' AND
		clientes.empresa = @empresa AND
		saida.`data` > '2016-07-01'
		GROUP BY catalogo.ID

) as S on  (catalogo.ID = S.ID )
LEFT JOIN
(

		SELECT
		catalogo.ID,
		sum(entrada.qtd) as qtdEntrada
		FROM
		catalogo
		LEFT JOIN entrada on catalogo.ID = entrada.CATALOGO_ID
		WHERE
		catalogo.tipo = 0 AND
		catalogo.ATIVO = 'A' 		AND
		entrada.UR = @empresa AND
    entrada.`data` > '2016-07-01'
		GROUP BY catalogo.ID


) as E on  (catalogo.ID = E.ID )

LEFT JOIN
(
SELECT
		catalogo.ID,
		estoque.empresas_id_11 as qtdEstoque
		FROM
		catalogo
		LEFT JOIN estoque on catalogo.ID = estoque.CATALOGO_ID
		WHERE
		catalogo.tipo = 0 AND
		catalogo.ATIVO = 'A'
		GROUP BY catalogo.ID


) as ES on (catalogo.ID = ES.ID )

LEFT JOIN
(

SELECT
		catalogo.ID,
		sum(entrada_ur.QTD) as qtdEntradaInterna
		FROM
		catalogo
		LEFT JOIN itenspedidosinterno on catalogo.ID = itenspedidosinterno.CATALOGO_ID
		LEFT JOIN entrada_ur on itenspedidosinterno.ID = entrada_ur.ITENSPEDIDOSINTERNOS_ID
		WHERE
		catalogo.tipo = 0 AND
		catalogo.ATIVO = 'A' 		AND
		entrada_ur.UR = @empresa AND
    entrada_ur.`DATA` > '2016-07-01'
		GROUP BY catalogo.ID
) as EI on (catalogo.ID = EI.ID )
WHERE
catalogo.tipo = 0 AND
catalogo.ATIVO = 'A'
ORDER BY catalogo.ID