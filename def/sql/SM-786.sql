CREATE TABLE `services` (
`id`  int(11) NOT NULL ,
`name`  varchar(50) NOT NULL ,
`status`  enum('on','off') NOT NULL DEFAULT 'on' ,
`updated_at`  timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP ,
PRIMARY KEY (`id`)
);

INSERT INTO `services` (`id`, `name`) VALUES ('1', 'email'), ('2', 'hipchat');

