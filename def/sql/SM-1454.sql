-- sql para pegar medicamentos ativos sem principio ativo.
SELECT
catalogo.ID,
catalogo.lab_desc,
catalogo.principio,
catalogo.apresentacao,
catalogo_id
FROM
catalogo
LEFT JOIN catalogo_principio_ativo ON catalogo.ID = catalogo_principio_ativo.catalogo_id
WHERE
catalogo.ATIVO = 'A'
AND
tipo = 0