SELECT c.nome,
				c.responsavel,
				c.endereco,
				c.bairro,
				c.referencia,
        (select  concat(x1.NOME,',',x1.UF) from cidades as x1 where x1.id=c.CIDADE_ID_DOM) as cidade_dom,
        c.TEL_DOMICILIAR,
        c.TEL_RESPONSAVEL,
				statuspaciente.`status` as st
				FROM clientes as c
				LEFT OUTER JOIN planosdesaude as p ON c.convenio = p.id
				LEFT OUTER JOIN cid10 as cid ON c.diagnostico collate utf8_general_ci = cid.codigo collate utf8_general_ci
				LEFT JOIN cidades cd ON c.CIDADE_ID_UND = cd.ID
				INNER JOIN statuspaciente on c.`status` = statuspaciente.id
				WHERE c.empresa =11 and c.idClientes  NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)