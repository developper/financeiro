CREATE TABLE historico_troca_item (
`id`  int(11) NULL AUTO_INCREMENT ,
`catalogo_id_item`  int(11) NOT NULL ,
`catalogo_id_item_substituto`  int(11) NOT NULL ,
`user_id`  int(11) NOT NULL ,
`motivo`  text NOT NULL ,
`data`  datetime NOT NULL ,
`solicitacao_id`  int(11) NOT NULL ,
`tipo`  int(1) NOT NULL ,
PRIMARY KEY (`id`)
)
;