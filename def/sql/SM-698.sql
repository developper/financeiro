-- Número de feridas abertas após implantação do home care. 
-- Levando em consideração a evolução de enfermagem status da ferida igual a nova (1).
SELECT
evolucaoenfermagem.ID,
clientes.nome AS PACIENTE,
usuarios.nome AS USUARIO,
quadro_feridas_enf_status.status_ferida,
quadro_feridas_enf_local.`LOCAL`,
quadro_feridas_enf_tipo.TIPO,
quadro_feridas_enf_lesao.TIPO AS COBERTURA,
quadro_feridas_enf_pele.PELE,
planosdesaude.nome AS CONVENIO,
empresas.nome AS UR,
evolucaoenfermagem.DATA_EVOLUCAO,
DATE_FORMAT(evolucaoenfermagem.DATA_EVOLUCAO,'%d') AS DIA,
DATE_FORMAT(evolucaoenfermagem.DATA_EVOLUCAO,'%m') AS MES,
DATE_FORMAT(evolucaoenfermagem.DATA_EVOLUCAO,'%Y') AS ANO,
evolucaoenfermagem.`DATA` as DATA_SISTEMA
FROM
clientes
INNER JOIN evolucaoenfermagem ON evolucaoenfermagem.PACIENTE_ID = clientes.idClientes
INNER JOIN usuarios ON evolucaoenfermagem.USUARIO_ID = usuarios.idUsuarios
INNER JOIN quadro_feridas_enfermagem ON evolucaoenfermagem.ID = quadro_feridas_enfermagem.EVOLUCAO_ENF_ID AND quadro_feridas_enfermagem.TAMANHO = evolucaoenfermagem.NUTRIDO_DESNUTRIDO
INNER JOIN quadro_feridas_enf_status ON quadro_feridas_enfermagem.STATUS_FERIDA = quadro_feridas_enf_status.id
INNER JOIN quadro_feridas_enf_local ON quadro_feridas_enfermagem.`LOCAL` = quadro_feridas_enf_local.ID
INNER JOIN quadro_feridas_enf_pele ON quadro_feridas_enfermagem.TECIDO = quadro_feridas_enf_pele.ID
INNER JOIN planosdesaude ON clientes.convenio = planosdesaude.id
INNER JOIN empresas ON clientes.empresa = empresas.id
INNER JOIN quadro_feridas_enf_tipo ON quadro_feridas_enfermagem.TIPO = quadro_feridas_enf_tipo.ID 
INNER JOIN quadro_feridas_enf_lesao ON quadro_feridas_enfermagem.COBERTURA = quadro_feridas_enf_lesao.ID
WHERE
evolucaoenfermagem.DATA_EVOLUCAO = 2014 AND
quadro_feridas_enfermagem.STATUS_FERIDA = 1 AND
evolucaoenfermagem.PACIENTE_ID  NOT IN (142,67,69,128,434,220,263,112,635,101)
ORDER BY
clientes.idClientes ASC,
evolucaoenfermagem.DATA_EVOLUCAO ASC

--- Solicitações Avulsas no sistema MAT/MED/DIE/Equp.
SELECT
UPPER(clientes.nome) as PACIENTE,
UPPER(CONCAT(catalogo.principio,' ',catalogo.apresentacao)) AS ITEM,
solicitacoes.qtd as QUANTIDADE,
(CASE solicitacoes.tipo
  when 0 then 'MEDICAMENTO'
  when 1 then 'MATERIAL'
  when 3 then 'DIETA'  
ELSE '' END)
 as TIPO,
UPPER(usuarios.nome) as ENFERMEIRO,
(CASE solicitacoes.entrega_imediata
  when 'S' then 'SIM' 
  when 'N' then 'NÃO'
  else ' ' END
) as ENTREGA_IMEDIATA,
justificativa_solicitacao_avulsa.justificativa,
UPPER(empresas.nome) as UNIDADE_REGIONAL,
solicitacoes.`data` AS DATA_SOLICITACAO,
DATE_FORMAT(solicitacoes.`data`,'%d') as DIA,
DATE_FORMAT(solicitacoes.`data`,'%m') as MES,
DATE_FORMAT(solicitacoes.`data`,'%Y') as ANO
FROM
solicitacoes
INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
INNER JOIN empresas ON clientes.empresa = empresas.id
INNER JOIN usuarios ON solicitacoes.enfermeiro = usuarios.idUsuarios
INNER JOIN catalogo ON solicitacoes.CATALOGO_ID = catalogo.ID
LEFT JOIN  justificativa_solicitacao_avulsa ON justificativa_solicitacao_avulsa.id = solicitacoes.JUSTIFICATIVA_AVULSO_ID
WHERE
solicitacoes.tipo IN (0, 1, 3) AND
YEAR(solicitacoes.`data`) = 2014 AND
solicitacoes.idPrescricao = -1 AND
solicitacoes.enfermeiro NOT IN (60,48,122) AND
solicitacoes.paciente NOT IN (142,67,69,128,434,220,263,112,635,101)
UNION ALL
SELECT
UPPER(clientes.nome) as PACIENTE,
UPPER(cobrancaplanos.item) AS ITEM,
solicitacoes.qtd as QUANTIDADE,
'Equipamento' as TIPO,
UPPER(usuarios.nome) as ENFERMEIRO,
(CASE solicitacoes.entrega_imediata
  when 'S' then 'SIM' 
  when 'N' then 'NÃO'
  else ' ' END
) as ENTREGA_IMEDIATA,
justificativa_solicitacao_avulsa.justificativa,
UPPER(empresas.nome) as UNIDADE_REGIONAL,
solicitacoes.`data` AS DATA_SOLICITACAO,
DATE_FORMAT(solicitacoes.`data`,'%d') as DIA,
DATE_FORMAT(solicitacoes.`data`,'%m') as MES,
DATE_FORMAT(solicitacoes.`data`,'%Y') as ANO
FROM
solicitacoes
INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
INNER JOIN empresas ON clientes.empresa = empresas.id
INNER JOIN usuarios ON solicitacoes.enfermeiro = usuarios.idUsuarios
INNER JOIN cobrancaplanos ON solicitacoes.CATALOGO_ID = cobrancaplanos.id
LEFT JOIN  justificativa_solicitacao_avulsa ON justificativa_solicitacao_avulsa.id = solicitacoes.JUSTIFICATIVA_AVULSO_ID
WHERE
solicitacoes.tipo =5 AND
YEAR(solicitacoes.`data` ) =2014 AND
solicitacoes.idPrescricao = -1 AND
solicitacoes.enfermeiro NOT IN (60,48,122) AND
solicitacoes.paciente NOT IN (142,67,69,128,434,220,263,112,635,101) AND 
solicitacoes.TIPO_SOL_EQUIPAMENTO =1

ORDER BY DATA_SOLICITACAO ASC

-- Número de feridas cicatrizadas após implantação do home care. 
-- Levando em consideração a evolução de enfermagem status da ferida igual a cicatrizada (2).
SELECT
evolucaoenfermagem.ID,
clientes.nome AS PACIENTE,
usuarios.nome AS USUARIO,
quadro_feridas_enf_status.status_ferida,
quadro_feridas_enf_local.`LOCAL`,
quadro_feridas_enf_tipo.TIPO,
quadro_feridas_enf_lesao.TIPO AS COBERTURA,
quadro_feridas_enf_pele.PELE,
planosdesaude.nome AS CONVENIO,
empresas.nome AS UR,
evolucaoenfermagem.DATA_EVOLUCAO,
DATE_FORMAT(evolucaoenfermagem.DATA_EVOLUCAO,'%d') AS DIA,
DATE_FORMAT(evolucaoenfermagem.DATA_EVOLUCAO,'%m') AS MES,
DATE_FORMAT(evolucaoenfermagem.DATA_EVOLUCAO,'%Y') AS ANO,
evolucaoenfermagem.`DATA` as DATA_SISTEMA
FROM
clientes
INNER JOIN evolucaoenfermagem ON evolucaoenfermagem.PACIENTE_ID = clientes.idClientes
INNER JOIN usuarios ON evolucaoenfermagem.USUARIO_ID = usuarios.idUsuarios
INNER JOIN quadro_feridas_enfermagem ON evolucaoenfermagem.ID = quadro_feridas_enfermagem.EVOLUCAO_ENF_ID AND quadro_feridas_enfermagem.TAMANHO = evolucaoenfermagem.NUTRIDO_DESNUTRIDO
INNER JOIN quadro_feridas_enf_status ON quadro_feridas_enfermagem.STATUS_FERIDA = quadro_feridas_enf_status.id
INNER JOIN quadro_feridas_enf_local ON quadro_feridas_enfermagem.`LOCAL` = quadro_feridas_enf_local.ID
INNER JOIN quadro_feridas_enf_pele ON quadro_feridas_enfermagem.TECIDO = quadro_feridas_enf_pele.ID
INNER JOIN planosdesaude ON clientes.convenio = planosdesaude.id
INNER JOIN empresas ON clientes.empresa = empresas.id
INNER JOIN quadro_feridas_enf_tipo ON quadro_feridas_enfermagem.TIPO = quadro_feridas_enf_tipo.ID 
INNER JOIN quadro_feridas_enf_lesao ON quadro_feridas_enfermagem.COBERTURA = quadro_feridas_enf_lesao.ID
WHERE
YEAR(evolucaoenfermagem.DATA_EVOLUCAO) = 2014 AND
quadro_feridas_enfermagem.STATUS_FERIDA = 2 AND
evolucaoenfermagem.PACIENTE_ID  NOT IN (142,67,69,128,434,220,263,112,635,101)
ORDER BY
clientes.idClientes ASC,
evolucaoenfermagem.DATA_EVOLUCAO ASC

-- Cumprimento do prazo de envio de relatórios de prorrogação ( até dia 5 de cada mês)
SELECT
clientes.nome AS PACIENTE,
usuarios.nome AS ENFERMEIRO,
planosdesaude.nome AS CONVENIO,
empresas.nome AS  UR,
relatorio_prorrogacao_enf.`DATA` as DATA_SISTEMA,
DATE_FORMAT(DATE_SUB(relatorio_prorrogacao_enf.INICIO, INTERVAL 1 MONTH),'%Y-%m-05') as DATA_MAX_ENVIO,
relatorio_prorrogacao_enf.INICIO,
relatorio_prorrogacao_enf.FIM,
DATE_FORMAT(relatorio_prorrogacao_enf.`DATA`,'%d') as DIA,
DATE_FORMAT(relatorio_prorrogacao_enf.`DATA`,'%m') as MES,
DATE_FORMAT(relatorio_prorrogacao_enf.`DATA`,'%Y') as ANO,
TIMESTAMPDIFF(HOUR,(DATE_FORMAT(DATE_SUB(relatorio_prorrogacao_enf.INICIO, INTERVAL 1 MONTH),'2014-%m-05 23:59:59')),relatorio_prorrogacao_enf.`DATA`) as DIFERENCA_HORAS

FROM
relatorio_prorrogacao_enf
INNER JOIN clientes ON relatorio_prorrogacao_enf.PACIENTE_ID = clientes.idClientes 
INNER JOIN usuarios ON relatorio_prorrogacao_enf.USUARIO_ID = usuarios.idUsuarios
INNER JOIN empresas ON clientes.empresa = empresas.id
INNER JOIN planosdesaude ON clientes.convenio = planosdesaude.id
WHERE
relatorio_prorrogacao_enf.PACIENTE_ID NOT IN (142,67,69,128,434,220,263,112,635,101) AND
YEAR(relatorio_prorrogacao_enf.`DATA`) = 2014 
and (DATE_FORMAT(DATE_SUB(relatorio_prorrogacao_enf.INICIO, INTERVAL 1 MONTH),'%Y-%m-05 23:59:59') <relatorio_prorrogacao_enf.`DATA`)
ORDER BY DATA ASC

-- Diferença em minutos do tempo que a prescrição é inserida pelo médico e a enfermagem faz a solicitação.
SELECT
clientes.nome AS PACIENTE,
usuarios.nome AS ENFERMEIRO_SOLICITANTE,
empresas.nome AS UR,
planosdesaude.nome AS CONVENIO,
prescricoes.inicio AS INICIO,
prescricoes.fim AS FIM,
prescricoes.`data` AS DATA_PRESCRICAO,
solicitacoes.`DATA_SOLICITACAO` AS DATA_SOLICITACAO,
TIMESTAMPDIFF(MINUTE,prescricoes.`data`,DATA_SOLICITACAO) as DIFERENCA_MINUTOS

FROM
prescricoes
INNER JOIN clientes ON clientes.idClientes = prescricoes.paciente
INNER JOIN empresas ON clientes.empresa = empresas.id
INNER JOIN planosdesaude ON clientes.convenio = planosdesaude.id
INNER JOIN solicitacoes ON solicitacoes.idPrescricao = prescricoes.id
INNER JOIN usuarios ON solicitacoes.enfermeiro = usuarios.idUsuarios
WHERE
prescricoes.paciente NOT IN (142,67,69,128,434,220,263,112,635,101)  AND
prescricoes.Carater IN (2,5) AND
prescricoes.STATUS = 0 AND
YEAR(prescricoes.`data`) = 2014
GROUP BY
prescricoes.id
ORDER BY
DATA_PRESCRICAO ASC

/* Buscar as prescrições inseridas dentro do tempo limite e que os enfermeiros 
não solicitaram dentro desse tempo. Para achar a data de comparação deve pegar 
a data início da prescrição diminuir um mês,  dessa resultado mantemos o mês e ano depois concatenaremos o dia que vai depender da UR: UR SSA- DIA 05, UR FSA – DIA 15,
UR SUDOESTE- DIA 05, UR BRASÍLIA – DIA 15, UR SULBAHIA- DIA 10, 
UR ALAGOINHAS- DIA 10 */
SELECT
clientes.nome AS PACIENTE,
usuarios.nome AS ENFERMEIRO_SOLICITANTE,
empresas.nome AS UR,
planosdesaude.nome AS CONVENIO,
prescricoes.inicio AS INICIO,
prescricoes.fim AS FIM,
prescricoes.`data` AS DATA_PRESCRICAO,
solicitacoes.`DATA_SOLICITACAO` AS DATA_SOLICITACAO,
CONCAT(
DATE_FORMAT(DATE_SUB(prescricoes.inicio, INTERVAL 1 MONTH),'%Y-%m-'),
IF(clientes.empresa = 1,15,
                IF(clientes.empresa = 5,10,
                   IF(clientes.empresa = 7,'05',
                        IF(clientes.empresa = 4,10,
                            IF(clientes.empresa = 2,'05',
                                 IF(clientes.empresa = 9,15,0))))))
) as DATA_LIMITE,
TIMESTAMPDIFF(HOUR,prescricoes.`data`,DATA_SOLICITACAO) as DIFERENCA_HORAS_PRESCR_SOLI
FROM
prescricoes
INNER JOIN clientes ON clientes.idClientes = prescricoes.paciente
INNER JOIN empresas ON clientes.empresa = empresas.id
INNER JOIN planosdesaude ON clientes.convenio = planosdesaude.id
INNER JOIN solicitacoes ON solicitacoes.idPrescricao = prescricoes.id
INNER JOIN usuarios ON solicitacoes.enfermeiro = usuarios.idUsuarios
WHERE
prescricoes.paciente NOT IN (142,67,69,128,434,220,263,112,635,101)  AND
prescricoes.Carater IN (2,5) AND
YEAR(prescricoes.`data`) = 2014 AND
prescricoes.STATUS = 0 AND
prescricoes.`data` < CONCAT(
DATE_FORMAT(DATE_SUB(prescricoes.inicio, INTERVAL 1 MONTH),'%Y-%m-'),
IF(clientes.empresa = 1,15,
                IF(clientes.empresa = 5,10,
                   IF(clientes.empresa = 7,'05',
                        IF(clientes.empresa = 4,10,
                            IF(clientes.empresa = 2,'05',
                                 IF(clientes.empresa = 9,15,0))))))
)

and solicitacoes.`DATA_SOLICITACAO` >CONCAT(
DATE_FORMAT(DATE_SUB(prescricoes.inicio, INTERVAL 1 MONTH),'%Y-%m-'),
IF(clientes.empresa = 1,15,
                IF(clientes.empresa = 5,10,
                   IF(clientes.empresa = 7,'05',
                        IF(clientes.empresa = 4,10,
                            IF(clientes.empresa = 2,'05',
                                 IF(clientes.empresa = 9,15,0))))))
)
GROUP BY
prescricoes.id
ORDER BY
DATA_PRESCRICAO ASC

  
