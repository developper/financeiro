START TRANSACTION;

UPDATE tiss_guia, tiss_guia_sadt
SET tiss_guia.validade_senha = tiss_guia_sadt.validade_senha
WHERE `tiss_guia_sadt`.tiss_guia_id = tiss_guia.id
  AND tiss_guia.validade_senha IS NULL
  AND tiss_guia_sadt.validade_senha IS NOT NULL
;

COMMIT;