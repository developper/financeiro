CREATE TABLE `fatura_controle_recebimento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fatura_id` int(11) NOT NULL,
  `status` enum('PENDENTE','RECEBIDO') NOT NULL,
  `previsao_recebimento` varchar(255) NOT NULL,
  `valor_faturado` float(255, 0) NOT NULL,
  `created_by` int(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NULL,
  `updated_at` datetime NULL,
  `nota_id` int(11) NULL,
  `valor_recebido` float NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `historico_fatura_controle_recebimento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  fatura_controle_recebimento_id int(11) NOT NULL,
  `fatura_id` int(11) NOT NULL,
  `status` enum('PENDENTE','RECEBIDO') NOT NULL,
  `previsao_recebimento` varchar(255) NOT NULL,
  `valor_faturado` float(255, 0) NOT NULL,
  `created_by` int(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NULL,
  `updated_at` datetime NULL,
  `nota_id` int(11) NULL,
  `valor_recebido` float NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `fatura_controle_recebimento`
MODIFY COLUMN `valor_faturado` float(100, 2) NOT NULL AFTER `previsao_recebimento`;

ALTER TABLE `historico_fatura_controle_recebimento`
MODIFY COLUMN `valor_faturado` float(100, 2) NOT NULL AFTER `previsao_recebimento`;

ALTER TABLE `fatura_controle_recebimento`
ADD COLUMN `modalidade_fatura` enum('ID','AD','REMOCAO') NULL AFTER `valor_recebido`;

ALTER TABLE `historico_fatura_controle_recebimento`
ADD COLUMN `modalidade_fatura` enum('ID','AD','REMOCAO') NULL AFTER `valor_recebido`;