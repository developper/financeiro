ALTER TABLE `plano_contas_contabil`
MODIFY COLUMN `N1`  enum('A','P','R','ARE') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `ID`,
MODIFY COLUMN `N2`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `COD_N2`,
MODIFY COLUMN `N3`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `COD_N3`,
MODIFY COLUMN `N4`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `COD_N4`,
MODIFY COLUMN `N5`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `COD_N5`,
ADD COLUMN `CODIGO_DOMINIO`  int(11) NOT NULL AFTER `ID`;

ALTER TABLE `empresas`
ADD COLUMN `centro_custo_dominio`  int(11) NULL DEFAULT 0 AFTER `SIGLA_EMPRESA`

UPDATE `empresas` SET `centro_custo_dominio`='1' WHERE (`id`='1')
UPDATE `empresas` SET `centro_custo_dominio`='7' WHERE (`id`='2')
UPDATE `empresas` SET `centro_custo_dominio`='2' WHERE (`id`='4')
UPDATE `empresas` SET `centro_custo_dominio`='6' WHERE (`id`='5')
UPDATE `empresas` SET `centro_custo_dominio`='4' WHERE (`id`='7')
UPDATE `empresas` SET `centro_custo_dominio`='9' WHERE (`id`='8')
UPDATE `empresas` SET `centro_custo_dominio`='5' WHERE (`id`='9')