create table fechamento_caixa
(
    id int auto_increment,
    data_fechamento date not null,
    fechado_em datetime not null,
    fechado_por int not null,
    constraint fechamento_caixa_pk
        primary key (id)
)
    comment 'Tabela com linha unica contendo os dados do ultimo fechamento de caixa';


create table historico_fechamento_caixa
(
    id int auto_increment,
    data_fechamento date not null,
    justificativa text null,
    created_at datetime not null,
    created_by int not null,
    constraint historico_fechamento_caixa_pk
        primary key (id)
)
    comment 'Tabela que contém o histórico dos fechamentos de caixa';

INSERT INTO `fechamento_caixa` (`data_fechamento`, `fechado_em`, `fechado_por`) VALUES ('2019-01-01', '2019-07-30 15:04:31', 261)

