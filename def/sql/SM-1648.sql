ALTER TABLE `historico_lote_fatura`
ADD COLUMN `status`  int(11) NULL AFTER `cancelado_at`,
ADD COLUMN `data_margem_contribuicao`  date NULL AFTER `status`,
ADD COLUMN `enviado_margem_contribuicao_por`  int(11) NULL AFTER `data_margem_contribuicao`,
ADD COLUMN `enviado_margem_contribuicao_em`  datetime NULL AFTER `enviado_margem_contribuicao_por`,
ADD COLUMN `justificativa_reabertura`  text NULL AFTER `enviado_margem_contribuicao_em`;

