SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `relatorio_nao_conformidade_topico`
-- ----------------------------
DROP TABLE IF EXISTS `relatorio_nao_conformidade_topico`;
CREATE TABLE `relatorio_nao_conformidade_topico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of relatorio_nao_conformidade_topico
-- ----------------------------
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('1', 'PRESCRIÇÃO NUTRICIONAL');
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('2', 'PRESCRIÇÃO DE ENFERMAGEM');
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('3', 'REGISTRO DE PROFISSIONAIS (IMPRESSO ROSA)');
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('4', 'REGISTRO DE VISITA MÉDICA E DE ENFERMAGEM NO SISTEMA');
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('5', 'REGISTRO DE FOSIO / FONO');
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('6', 'REGISTRO DE OBSERVAÇÃO DO TÉCNICO');
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('7', 'SSVV');
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('8', 'FOLHA DE LESÕES');
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('9', 'TROCA DE DISPOSITIVO');
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('10', 'CONTROLE DE FRALDAS');
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('11', 'CONTROLE DE OXIGENIO');
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('12', 'CONTROLE DE ESTOQUE');
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('13', 'EQUIPAMENTOS');
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('14', 'SISTEMA');
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('15', 'RELATÓRIOS');
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('16', 'EVENTOS ADVERSOS');
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('17', 'REINCIDÊNCIA');
INSERT INTO `relatorio_nao_conformidade_topico` VALUES ('18', 'PRESCRIÇÃO MÉDICA');

-- ------------------------------------------------------------------

/*
Navicat MySQL Data Transfer

Source Server         : vagarnt_sismederi
Source Server Version : 50544
Source Host           : 127.0.0.1:3306
Source Database       : sismederi

Target Server Type    : MYSQL
Target Server Version : 50544
File Encoding         : 65001

Date: 2016-08-15 08:34:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `relatorio_nao_conformidade_justificativa`
-- ----------------------------
DROP TABLE IF EXISTS `relatorio_nao_conformidade_justificativa`;
CREATE TABLE `relatorio_nao_conformidade_justificativa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `justificativa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of relatorio_nao_conformidade_justificativa
-- ----------------------------
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('1', 'Abertura de nova lesão');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('2', 'Alteração no uso da dieta porém a prescrição se manteve no sismederi');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('3', 'Alteração no uso da medicação, porém, a prescrição se manteve no sismederi');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('4', 'Aprazamento incorreto ou ausência de aprazamento');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('5', 'Ausência de carimbo e/ou assinatura do profissional');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('6', 'Ausência de descrição da sessão');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('7', 'Ausência de descrição de punção ou troca de dispositivo');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('8', 'Ausência de devolução de itens');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('9', 'Ausência de evolução do enfermeiro');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('10', 'Ausência de evolução do especialista');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('11', 'Ausência de evolução do fisioterapeuta');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('12', 'Ausência de evolução do fonoaudiólogo');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('13', 'Ausência de evolução do nutricionista');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('14', 'Ausência de evolução do psicólogo');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('15', 'Ausência de evolução médica');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('16', 'Ausência de nome do paciente ou data');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('17', 'Ausência de registro de lesão existente');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('18', 'Ausência de registro de NBZ, HGT, aspiração ou oxigênio prescrito');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('19', 'Ausência de registro de oxigenoterapia');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('20', 'Ausência de registro de procedimento realizado');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('21', 'Ausência de registro de troca de torpedo');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('22', 'Ausência de registro de visita de enfermagem');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('23', 'Ausência de registro de visita médica');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('24', 'Ausência de relatório');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('25', 'Comunicação inadequada');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('26', 'Conduta de lesão diverge do protocolo');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('27', 'Condutas realizadas desconformidade com relatórios');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('28', 'Controle de estoque');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('29', 'Controle de fraldas');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('30', 'Controle de oxigênio');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('31', 'Dado não condiz com realidade');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('32', 'Descrição da lesão em não conformidade com a indicação do tratamento');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('33', 'Descrição de vazão mantida apesar do uso');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('34', 'Descrição incoerente');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('35', 'Descrição incorreta');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('36', 'Dieta sem checar ou bolar');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('37', 'Dieta suspensa em sistema e não devolvida');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('38', 'Divergência com enfermagem');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('39', 'Divergência com nutrição');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('40', 'Divergência de outros dados do paciente');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('41', 'Enviado insumo mas não há registro de troca');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('42', 'Envio incompleto');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('43', 'Equipamento usado sem prescrição, indicação ou autorização prévia');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('44', 'Equipamentos');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('45', 'Equipamentos ativos no sistema desatualizados conforme HC');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('46', 'Erro de enfermagem');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('47', 'Eventos adversos');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('48', 'Evolução de procedimento não prescrito');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('49', 'Excesso de itens em pedido');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('50', 'Excesso de material e medicação no HC');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('51', 'Folha de lesões');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('52', 'Impresso não consta em prontuário');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('53', 'Infecção relacionada a assistência');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('54', 'Intercorrência sem conduta');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('55', 'Item liberado sem saída na logística');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('56', 'Item não prescrito no sistema');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('57', 'Itens solicitados e enviados sem autorização da auditoria');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('58', 'Itens solicitados e enviados sem autorização do convênio');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('59', 'Medicação sem checar ou bolar');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('60', 'Medicação suspensa em sistema e não devolvida');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('61', 'Não encaminhado');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('62', 'Não especifica tamanho ou tipo de curativo');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('63', 'Não houve ocorrência');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('64', 'Não se aplica');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('65', 'Novo procedimento por falha técnica');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('66', 'Outro');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('67', 'Posologia dúvida em posologia');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('68', 'Preenchimento incorreto');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('69', 'Prescrição de enfermagem');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('70', 'Prescrição médica');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('71', 'Prescrição nutricional');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('72', 'Prescrição sem carimbo');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('73', 'Prescrição sem carimbo do técnico e enfermeiro');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('74', 'Presença de rasuras');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('75', 'Problemas com equipamentos');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('76', 'Quantidade descrita inferior a quantidade liberada');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('77', 'Queda');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('78', 'Registro de fisioterapia / fono');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('79', 'Registro de item não prescrito pelo enfermeiro');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('80', 'Registro de NBZ, HGT, aspiração, oximetria ou oxigenoterapia sem prescrição');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('81', 'Registro de observação do técnico');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('82', 'Registro de profissionais');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('83', 'Registro de quantidade menor que a enviada e sem devolução');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('84', 'Registro de troca pendente');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('85', 'Registro de visita médica e de enfermagem no sistema');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('86', 'Registro incompleto de materiais utilizados');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('87', 'Registro inferior ou superior ao prescrito');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('88', 'Relatórios');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('89', 'Retirar equipamentos listados');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('90', 'Serviço autorizado porém sem registro');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('91', 'Serviço realizado sem autorização ou acima do autorizado');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('92', 'Serviços divergentes do relatório');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('93', 'Serviços divergentes do relatório médico');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('94', 'Serviços divergentes do relatório nutricional');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('95', 'Sistema');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('96', 'SSVV');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('97', 'Troca de dispositivos');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('98', 'Troca fora da padronização da empresa');
INSERT INTO `relatorio_nao_conformidade_justificativa` VALUES ('99', 'Visitas realizadas a mais ou a menos que o autorizado ou previsto');

-- ------------------------------------------------------------------------------------------------------------------

/*
Navicat MySQL Data Transfer

Source Server         : vagarnt_sismederi
Source Server Version : 50544
Source Host           : 127.0.0.1:3306
Source Database       : sismederi

Target Server Type    : MYSQL
Target Server Version : 50544
File Encoding         : 65001

Date: 2016-08-15 08:34:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `relatorio_nao_conformidade_topico_justificativa`
-- ----------------------------
DROP TABLE IF EXISTS `relatorio_nao_conformidade_topico_justificativa`;
CREATE TABLE `relatorio_nao_conformidade_topico_justificativa` (
  `relatorio_nao_conformidade_topico_id` int(11) DEFAULT NULL,
  `relatorio_nao_conformidade_justificativa_id` int(11) DEFAULT NULL,
  KEY `topico_FK` (`relatorio_nao_conformidade_topico_id`),
  KEY `justificativa_FK` (`relatorio_nao_conformidade_justificativa_id`),
  CONSTRAINT `justificativa_FK` FOREIGN KEY (`relatorio_nao_conformidade_justificativa_id`) REFERENCES `relatorio_nao_conformidade_justificativa` (`id`),
  CONSTRAINT `topico_FK` FOREIGN KEY (`relatorio_nao_conformidade_topico_id`) REFERENCES `relatorio_nao_conformidade_topico` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of relatorio_nao_conformidade_topico_justificativa
-- ----------------------------
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('18', '56');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('18', '67');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('18', '4');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('18', '42');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('18', '72');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('18', '60');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('18', '3');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('18', '59');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('18', '93');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('18', '38');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('18', '39');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('18', '66');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('18', '64');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('1', '42');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('1', '4');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('1', '94');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('1', '66');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('1', '72');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('1', '37');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('1', '2');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('1', '36');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('1', '64');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('2', '64');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('2', '61');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('2', '92');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('2', '66');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('2', '73');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('3', '13');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('3', '9');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('3', '14');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('3', '10');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('3', '5');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('3', '90');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('3', '15');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('3', '16');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('3', '20');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('3', '66');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('3', '64');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('4', '68');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('4', '34');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('4', '40');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('4', '42');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('4', '22');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('4', '99');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('4', '20');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('4', '23');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('4', '66');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('4', '64');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('5', '11');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('5', '6');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('5', '12');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('5', '66');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('5', '5');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('5', '90');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('5', '91');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('5', '16');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('5', '64');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('6', '46');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('6', '7');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('6', '74');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('6', '48');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('6', '5');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('6', '90');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('6', '91');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('6', '16');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('6', '19');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('6', '52');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('6', '66');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('6', '64');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('7', '87');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('7', '80');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('7', '66');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('7', '64');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('7', '52');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('7', '42');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('7', '18');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('7', '16');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('8', '86');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('8', '62');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('8', '83');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('8', '32');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('8', '52');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('8', '42');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('8', '79');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('8', '16');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('8', '17');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('8', '66');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('8', '64');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('9', '98');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('9', '41');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('9', '66');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('9', '64');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('9', '52');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('9', '42');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('9', '84');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('9', '16');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('10', '66');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('10', '76');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('10', '64');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('10', '52');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('10', '42');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('10', '35');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('10', '16');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('11', '64');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('11', '66');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('11', '52');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('11', '33');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('11', '21');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('11', '16');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('12', '66');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('12', '50');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('12', '64');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('12', '52');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('12', '42');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('12', '31');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('12', '16');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('13', '64');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('13', '89');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('13', '43');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('13', '66');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('13', '45');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('14', '66');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('14', '49');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('14', '64');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('14', '55');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('14', '58');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('14', '57');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('14', '8');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('15', '26');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('15', '24');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('15', '66');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('15', '27');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('16', '54');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('16', '65');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('16', '25');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('16', '66');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('16', '77');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('16', '1');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('16', '75');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('16', '53');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('16', '63');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '78');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '85');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '81');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '96');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '71');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '69');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '82');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '70');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '51');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '97');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '29');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '30');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '28');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '44');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '95');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '88');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '47');
INSERT INTO `relatorio_nao_conformidade_topico_justificativa` VALUES ('17', '66');

CREATE TABLE `relatorio_nao_conformidade_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relatorio_nao_conformidade_id` int(11) DEFAULT NULL,
  `relatorio_nao_conformidade_topico` int(11) DEFAULT NULL,
  `relatorio_nao_conformidade_justificativa` int(11) DEFAULT NULL,
  `justificativa` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `canceled_at` datetime DEFAULT NULL,
  `canceled_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `relatorio_nao_conformidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paciente_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `empresa_id` int(11) DEFAULT NULL,
  `inicio` date DEFAULT NULL,
  `fim` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `canceled_at` datetime DEFAULT NULL,
  `canceled_by` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);
