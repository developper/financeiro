ALTER TABLE `plano_contas`
ADD COLUMN `COD_IPCC`  varchar(15) NULL AFTER `N4`,
ADD COLUMN `IPCC`  varchar(200) NULL AFTER `COD_IPCC`,
ADD COLUMN `PROVISIONA`  enum('n','s') NOT NULL AFTER `IPCC`;

CREATE TABLE `error_backtrace` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`date`  datetime NOT NULL ,
`text`  text NOT NULL ,
`module`  varchar(50) NOT NULL ,
`file`  varchar(100) NOT NULL ,
`line`  int(11) NOT NULL ,
`function`  varchar(100) NULL ,
`class`  varchar(100) NULL ,
PRIMARY KEY (`id`)
);