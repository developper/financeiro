create table plano_contas_configuracao_retencoes
(
    iss int null,
    icms int null,
    ipi int null,
    pis int null,
    cofins int null,
    csll int null,
    ir int null,
    pcc int null
)
    comment 'Tabela de tupla unica que concentra todos os ID do IPCF que dizem respeito as aliquotas das retencoes';

INSERT INTO `plano_contas_configuracao_retencoes` (`iss`, `icms`, `ipi`, `pis`, `cofins`, `csll`, `ir`, `pcc`) VALUES (null, null, null, null, null, null, null, null);