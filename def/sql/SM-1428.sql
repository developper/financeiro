ALTER TABLE `catalogo`
ADD COLUMN `custo_medio_atualizado_em`  datetime NULL AFTER `brasindice_id`;


-- consulta para rodar e trazer e fazer update informando o ultima data que os itens foram atualizados.

SELECT
CONCAT("update catalogo set valorMedio = IF(valorMedio > 0, FORMAT(valorMedio,2), ",entrada.valor," ), valor = IF(valor > 0, valor, ",entrada.valor," ), custo_medio_atualizado_em ='",entrada.data,"' where ID = ",entrada.CATALOGO_ID," ;") as up
FROM
entrada INNER JOIN
(SELECT
max(entrada.id) as maxId
FROM
catalogo
INNER JOIN entrada ON catalogo.ID = entrada.CATALOGO_ID
WHERE
entrada.TIPO in (0,1,3)
GROUP BY catalogo.ID
ORDER BY maxid DESC) as table1 on (entrada.id = table1.maxId)
WHERE
entrada.TIPO IN (0,1,3)

-- rollback

SELECT
CONCAT("update catalogo set valorMedio =",table1.valorMedio,", valor = ",table1.valor,", custo_medio_atualizado_em = NULL where ID = ",entrada.CATALOGO_ID," ;") as up

FROM
entrada INNER JOIN
(SELECT
max(entrada.id) as maxId,
catalogo.valorMedio,
catalogo.valor
FROM
catalogo
INNER JOIN entrada ON catalogo.ID = entrada.CATALOGO_ID
WHERE
entrada.TIPO in (0,1,3)
GROUP BY catalogo.ID
ORDER BY maxid DESC) as table1 on (entrada.id = table1.maxId)
WHERE
entrada.TIPO IN (0,1,3)