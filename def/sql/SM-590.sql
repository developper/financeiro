ALTER TABLE `tiss_guia`
ADD COLUMN `validade_senha`  date NULL AFTER `senha`;

ALTER TABLE `tiss_guia`
ADD COLUMN `numero_guia_operadora`  varchar(50) NULL AFTER `nome_plano`;

DELETE FROM `tiss_motivo_saida` WHERE (`id`='7');

INSERT INTO `tiss_motivo_saida` (`descricao`, `tiss_codigo`) VALUES ('Alta mãe/puerpera e do recem nascido', '61');
INSERT INTO `tiss_motivo_saida` (`descricao`, `tiss_codigo`) VALUES ('Alta mãe/puerpera e permanencia do recem-nascido', '62');
INSERT INTO `tiss_motivo_saida` (`descricao`, `tiss_codigo`) VALUES ('Alta mãe/puerpera e obito do recem-nascido', '63');
INSERT INTO `tiss_motivo_saida` (`descricao`, `tiss_codigo`) VALUES ('Alta mãe/puerpera com obito fetal', '64');
INSERT INTO `tiss_motivo_saida` (`descricao`, `tiss_codigo`) VALUES ('Óbito da gestante e do concepto', '65');
INSERT INTO `tiss_motivo_saida` (`descricao`, `tiss_codigo`) VALUES ('Óbito da mãe/puerpera e alta do recem-nascido', '66');
INSERT INTO `tiss_motivo_saida` (`descricao`, `tiss_codigo`) VALUES ('Obito da mae/puerpera e permanencia do recem nascido', '67');


