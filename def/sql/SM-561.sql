
--Itens da tabela particular usada na fatura da Mederi
SELECT
cobrancaplanos.id,
cobrancaplanos.item
FROM
cobrancaplanos

-- relacção dos cuidados especiais solicitados pelos médicos com a tabela utilizada na fatura.
SELECT
cuidadosespeciais.id,
cuidadosespeciais.cuidado,
cobrancaplanos.id,
cobrancaplanos.item
FROM
cuidadosespeciais
LEFT JOIN cobrancaplanos ON cuidadosespeciais.COBRANCAPLANO_ID = cobrancaplanos.id