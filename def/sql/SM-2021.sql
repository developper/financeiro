-- itens agrupados por catalogo id, que estão pendentes na saida de uma determinada Unidade e de um determinado plano (UR:salvador, plano SESAB/SUS)
SELECT
	s.enfermeiro,
	( CASE WHEN COUNT( * ) > 1 THEN SUM( s.enviado ) ELSE s.enviado END ) AS enviados,
	sum( ( CASE s.STATUS WHEN 2 THEN s.qtd ELSE s.autorizado END ) ) AS autorizado,
	s.enviado,
	s.NUMERO_TISS,
	s.CATALOGO_ID,
	s.idSolicitacoes AS sol,
	s.STATUS,
	DATE_FORMAT( s.DATA_SOLICITACAO, '%d/%m/%Y' ) AS DATA,
	concat(b.principio,' ',b.apresentacao,'( LAB: ',b.lab_desc,' PRINCI: ',e.principio,')') as principio,
	b.apresentacao

FROM
	solicitacoes AS s
	INNER JOIN catalogo AS b ON b.ID = s.CATALOGO_ID
	LEFT JOIN catalogo_principio_ativo AS d ON d.catalogo_id = b.ID
	INNER JOIN principio_ativo AS e ON e.id = d.principio_ativo_id
	INNER JOIN clientes AS c ON s.paciente = c.idClientes
WHERE
	c.empresa = '2'
	AND s.tipo = '0'
	AND ( CASE s.STATUS WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END )
	AND ( s.STATUS = '1' OR s.STATUS = '2' )
	AND s.TIPO_SOLICITACAO NOT IN ( 2, 5, 9 )
	and c.convenio = 132
GROUP BY
	CATALOGO_ID

	UNION
	SELECT
	s.enfermeiro,
	( CASE WHEN COUNT( * ) > 1 THEN SUM( s.enviado ) ELSE s.enviado END ) AS enviados,
	sum( ( CASE s.STATUS WHEN 2 THEN s.qtd ELSE s.autorizado END ) ) AS autorizado,
	s.enviado,
	s.NUMERO_TISS,
	s.CATALOGO_ID,
	s.idSolicitacoes AS sol,
	s.STATUS,
	DATE_FORMAT( s.DATA_SOLICITACAO, '%d/%m/%Y' ) AS DATA,
	b.principio,
	b.apresentacao
FROM
	solicitacoes AS s,
	catalogo AS b,
	clientes AS c
WHERE
	c.empresa = '2'
	AND s.paciente = c.idClientes
	AND s.tipo = '3'
	AND ( CASE s.STATUS WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END )
	AND ( s.STATUS = '1' OR s.STATUS = '2' )
	AND b.ID = s.CATALOGO_ID
	AND s.TIPO_SOLICITACAO NOT IN ( 2, 5, 9 )
	and c.convenio = 132
GROUP BY
	CATALOGO_ID

	Union
	SELECT
	s.enfermeiro,
	( CASE WHEN COUNT( * ) > 1 THEN SUM( s.enviado ) ELSE s.enviado END ) AS enviados,
	sum( ( CASE s.STATUS WHEN 2 THEN s.qtd ELSE s.autorizado END ) ) AS autorizado,
	s.enviado,
	s.NUMERO_TISS,
	s.CATALOGO_ID,
	s.idSolicitacoes AS sol,
	s.STATUS,
	DATE_FORMAT( s.DATA_SOLICITACAO, '%d/%m/%Y' ) AS DATA,
	b.principio,
	b.apresentacao
FROM
	solicitacoes AS s,
	catalogo AS b,
	clientes AS c
WHERE
	c.empresa = '2'
	AND s.paciente = c.idClientes
	AND s.tipo = '1'
	AND ( CASE s.STATUS WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END )
	AND ( s.STATUS = '1' OR s.STATUS = '2' )
	AND b.ID = s.CATALOGO_ID
	AND s.TIPO_SOLICITACAO NOT IN ( 2, 5, 9 )
	and c.convenio = 132
GROUP BY
	CATALOGO_ID

	Union
	SELECT
	s.enfermeiro,
	( CASE WHEN COUNT( * ) > 1 THEN SUM( s.enviado ) ELSE s.enviado END ) AS enviados,
	sum( ( CASE s.STATUS WHEN 2 THEN s.qtd ELSE s.autorizado END ) ) AS autorizado,
	s.enviado,
	s.NUMERO_TISS,
	s.CATALOGO_ID,
	s.idSolicitacoes AS sol,
	s.STATUS,
	DATE_FORMAT( s.DATA_SOLICITACAO, '%d/%m/%Y' ) AS DATA,
	cb.item as principio,
	s.TIPO_SOL_EQUIPAMENTO as apresentacao

FROM
	solicitacoes AS s,
	cobrancaplanos AS cb,
	clientes AS c
WHERE
	c.empresa = '2'
	AND s.paciente = c.idClientes
	AND s.tipo = '5'
	AND ( CASE s.STATUS WHEN 2 THEN s.qtd > s.enviado ELSE s.autorizado > s.enviado END )
	AND ( s.STATUS = '1' OR s.STATUS = '2' )
	AND cb.id = s.CATALOGO_ID
	AND s.TIPO_SOLICITACAO NOT IN ( 2, 5, 9 )
	and c.convenio = 132
GROUP BY
	CATALOGO_ID

	ORDER BY
	principio,
	apresentacao,
	enviado