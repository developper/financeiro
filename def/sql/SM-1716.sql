
CREATE TABLE `motivo_juros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `motivo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;


INSERT INTO `motivo_juros` VALUES ('1', 'Fornecedor');
INSERT INTO `motivo_juros` VALUES ('2', 'Setor Diverso');
INSERT INTO `motivo_juros` VALUES ('3', 'Prorrogação de boleto');
INSERT INTO `motivo_juros` VALUES ('4', 'Setor Financeiro');

ALTER TABLE `parcelas`
ADD COLUMN `motivo_juros_id`  int(11) NULL AFTER `PARCELA_ORIGEM`;
