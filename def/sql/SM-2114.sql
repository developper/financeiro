alter table parcelas
    add nota_id_fatura_aglutinacao int null comment 'Coluna responsável por gravar o id da nota que foi gerada a partir de várias parcelas';

alter table notas
    add nota_aglutinacao enum('s', 'n') default 'n' null;