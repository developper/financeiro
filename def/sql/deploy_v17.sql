CREATE TABLE IF NOT EXISTS `servicos_terceirizados` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SERVICO` varchar(100) NOT NULL,
  `TIPO_PROFISSIONAL` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

INSERT INTO `servicos_terceirizados` (`ID`, `SERVICO`, `TIPO_PROFISSIONAL`) VALUES
(1, 'Plantão Técnico 06h', 'Enfermagem'),
(2, 'Plantão Técnico MT', 'Enfermagem'),
(3, 'Plantão Técnico MT FDS', 'Enfermagem'),
(4, 'Plantão Técnico SN', 'Enfermagem'),
(5, 'Plantão Técnico SN FDS', 'Enfermagem'),
(6, 'Plantão Técnico P', 'Enfermagem'),
(7, 'Plantão Técnico P FDS', 'Enfermagem'),
(8, 'Visita de Técnico para procedimento', 'Enfermagem'),
(9, 'Plantão Enfermeiro P', 'Enfermagem'),
(10, 'Sobreaviso Enfermeiro 12h', 'Enfermagem'),
(11, 'Sobreaviso Enfermeiro 24h', 'Enfermagem'),
(12, 'Acionamento em Sobre aviso', 'Enfermagem'),
(13, 'Acionamento fora do sobre aviso', 'Enfermagem'),
(14, 'Assistência de Enfermeiro por paciente  (por dia) AD', 'Enfermagem'),
(15, 'Assistência de Enfermeiro por paciente  (por dia) ID', 'Enfermagem'),
(16, 'Assistência de Enfermeiro por paciente  (por dia) curativo', 'Enfermagem'),
(17, 'Deslocamento', 'Enfermagem'),
(18, 'Avaliação inicial de Enfermagem', 'Enfermagem'),
(19, 'Coordenação de Enfermagem', 'Enfermagem'),
(20, 'Serviços Sac FSA', 'Enfermagem'),
(21, 'Sessão de Fisioterapia Motora', 'Fisioterapia/TO'),
(22, 'Sessão de Fisioterapia Respiratória', 'Fisioterapia/TO'),
(23, 'Sessão de Fisioterapia Motora e Respiratória', 'Fisioterapia/TO'),
(24, 'Avaliação Inicial de Fisioterapia', 'Fisioterapia/TO'),
(25, 'Sessão de Terapia Ocupacional', 'Fisioterapia/TO'),
(26, 'Avaliação Inicial Terapeuta Ocupacional', 'Fisioterapia/TO'),
(27, 'Sessão de Fonoaudiologia', 'Fonoaudiologia'),
(28, 'Avaliação Inicial de Fonoaudiologia', 'Fonoaudiologia'),
(29, 'Visita Nutricionista', 'Nutrição'),
(30, 'Avaliação Inicial Nutricionista', 'Nutrição'),
(31, 'Sessão de Psicologia', 'Psicologia'),
(32, 'Avaliação Inicial Psicologia', 'Psicologia');

CREATE TABLE IF NOT EXISTS `prestadores_servicos` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOGIN` int(14) NOT NULL,
  `SENHA` varchar(100) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `DATA_SENHA` datetime NOT NULL,
  `FORNECEDOR_ID` int(11) NOT NULL,
  `CRIADO_POR` int(11) NOT NULL,
  `DATA_CRIADO` datetime NOT NULL,
  `DATA_DESATIVADO` datetime NOT NULL,
  `DESATIVADO_POR` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `prestador_servico_empresa` (
  `PRESTADOR_ID` int(11) NOT NULL,
  `EMPRESA_ID` int(11) NOT NULL,
  `CRIADO_POR` int(11) NOT NULL,
  `CRIADA_EM` datetime NOT NULL,
  `DESATIVADA_POR` int(11) NOT NULL,
  `DESATIVADA_EM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `servico_terceirizado_prestador` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SERVICO_TERCEIRIZADO_ID` int(11) NOT NULL,
  `PRESTADOR_ID` int(11) NOT NULL,
  `EMPRESA_ID` INT( 11 ) NOT NULL,
  `VALOR` double(8,2) NOT NULL,
  `CRIADO_EM` datetime NOT NULL,
  `CRIADO_POR` int(11) NOT NULL,
  `DESATIVADO_EM` datetime NOT NULL,
  `DESATIVADO_POR` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;