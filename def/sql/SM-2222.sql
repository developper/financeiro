

--gerar relatório financeiro com associação ao planode contas.
select
pc.ID,
pc.COD_N2,
pc.N2,
pc.COD_N3,
pc.N3,
pc.COD_N4,
pc.N4,
pc.COD_IPCC as COD_ITEM_CONTABIL,
pc.IPCC as ITEM_CONTABIL,
pcc.CODIGO_DOMINIO


from
plano_contas as pc
LEFT JOIN plano_contas_contabil as pcc on (pc.COD_IPCC = pcc.COD_N5)