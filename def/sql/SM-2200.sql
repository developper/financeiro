CREATE VIEW power_bi_saldo_contas_bancarias AS (-- RECEBIMENTOS
SELECT
    contas.conta,
    valor_conciliado_entrada,
    valor_conciliado_saida,
    data_conciliado
FROM
    (SELECT
         idcb.ID,
         idcb.conta,
         idcb.SALDO_INICIAL AS saldo_inicial
     FROM
         indexed_demonstrativo_contas_bancarias AS idcb
     GROUP BY
         idcb.ID) AS contas INNER JOIN
    (
        SELECT
            SUM(idr.valor_final_conciliado) AS valor_conciliado_entrada,
            0 AS valor_conciliado_saida,
            idr.DATA_CONCILIADO AS data_conciliado,
            idr.origem
        FROM
            indexed_demonstrativo_recebimentos idr
        WHERE
            idr.DATA_CONCILIADO BETWEEN (
                SELECT
                    cb.DATA_SALDO_INICIAL
                FROM
                    contas_bancarias cb
                WHERE
                        cb.ID = idr.origem
            )
                AND NOW()
          AND CONCILIADO = 'S'
        GROUP BY origem, data_conciliado
    ) AS recebimentos ON contas.ID = recebimentos.origem
UNION

-- DESEMBOLSOS
SELECT
    contas.conta,
    valor_conciliado_entrada,
    valor_conciliado_saida,
    data_conciliado
FROM
    (SELECT
         idcb.ID,
         idcb.conta,
         idcb.SALDO_INICIAL AS saldo_inicial
     FROM
         indexed_demonstrativo_contas_bancarias AS idcb
     GROUP BY
         idcb.ID) AS contas INNER JOIN
    (
        SELECT
            0 AS valor_conciliado_entrada,
            SUM(idd.valor_final_conciliado) AS valor_conciliado_saida,
            idd.DATA_CONCILIADO AS data_conciliado,
            idd.origem
        FROM
            indexed_demonstrativo_desembolsos idd
        WHERE
            idd.DATA_CONCILIADO BETWEEN (
                SELECT
                    cb.DATA_SALDO_INICIAL
                FROM
                    contas_bancarias cb
                WHERE
                        cb.ID = idd.origem
            )
                AND NOW()
          AND CONCILIADO = 'S'
        GROUP BY origem, data_conciliado
    ) AS desembolsos ON contas.ID = desembolsos.origem

UNION

-- ENTRADAS VIA TRANSFERENCIA
SELECT
    contas.conta,
    valor_conciliado_entrada,
    valor_conciliado_saida,
    data_conciliado
FROM
    (SELECT
         idcb.ID,
         idcb.conta,
         idcb.SALDO_INICIAL AS saldo_inicial
     FROM
         indexed_demonstrativo_contas_bancarias AS idcb
     GROUP BY
         idcb.ID) AS contas INNER JOIN
    (
        SELECT
            SUM(tb1.VALOR) AS valor_conciliado_entrada,
            0 AS valor_conciliado_saida,
            tb1.DATA_CONCILIADO_DESTINO AS data_conciliado,
            cb1.ID AS origem
        FROM
            transferencia_bancaria AS tb1
                INNER JOIN contas_bancarias cb1 ON cb1.ID = tb1.CONTA_DESTINO
        WHERE
            tb1.DATA_CONCILIADO_DESTINO BETWEEN (
                SELECT
                    DATA_SALDO_INICIAL
                FROM
                    contas_bancarias cb
                WHERE
                        cb.ID = tb1.CONTA_DESTINO
            )
                AND NOW()
          AND tb1.CONCILIADO_DESTINO = 'S'
          AND tb1.CANCELADA_POR IS NULL
        GROUP BY origem, data_conciliado
    ) AS creditos ON contas.ID = creditos.origem

UNION

-- SAIDAS VIA TRANSFERENCIA
SELECT
    contas.conta,
    valor_conciliado_entrada,
    valor_conciliado_saida,
    data_conciliado
FROM
    (SELECT
         idcb.ID,
         idcb.conta,
         idcb.SALDO_INICIAL AS saldo_inicial
     FROM
         indexed_demonstrativo_contas_bancarias AS idcb
     GROUP BY
         idcb.ID) AS contas INNER JOIN
    (
        SELECT
            0 AS valor_conciliado_entrada,
            SUM(tb1.VALOR) AS valor_conciliado_saida,
            tb1.DATA_CONCILIADO_DESTINO AS data_conciliado,
            cb1.ID AS origem
        FROM
            transferencia_bancaria AS tb1
                INNER JOIN contas_bancarias cb1 ON cb1.ID = tb1.CONTA_ORIGEM
        WHERE
            tb1.DATA_CONCILIADO_DESTINO BETWEEN (
                SELECT
                    DATA_SALDO_INICIAL
                FROM
                    contas_bancarias cb
                WHERE
                        cb.ID = tb1.CONTA_ORIGEM
            )
                AND NOW()
          AND tb1.CONCILIADO_DESTINO = 'S'
          AND tb1.CANCELADA_POR IS NULL
        GROUP BY origem, data_conciliado
    ) AS debitos ON contas.ID = debitos.origem

UNION

-- TARIFAS ENTRADAS
SELECT
    contas.conta,
    valor_conciliado_entrada,
    valor_conciliado_saida,
    data_conciliado
FROM
    (SELECT
         idcb.ID,
         idcb.conta,
         idcb.SALDO_INICIAL AS saldo_inicial
     FROM
         indexed_demonstrativo_contas_bancarias AS idcb
     GROUP BY
         idcb.ID) AS contas INNER JOIN
    (
        SELECT
            SUM(itl.valor) AS valor_conciliado_entrada,
            0 AS valor_conciliado_saida,
            itl.data AS data_conciliado,
            itl.conta AS origem
        FROM
            impostos_tarifas imptar
                INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
                INNER JOIN contas_bancarias cb3 ON cb3.ID = itl.conta
        WHERE
                itl.data <= NOW()
          AND imptar.debito_credito = 'CREDITO'
        GROUP BY origem, data_conciliado
    ) AS tarifas_creditos ON contas.ID = tarifas_creditos.origem

UNION

-- TARIFAS SAIDAS
SELECT
    contas.conta,
    valor_conciliado_entrada,
    valor_conciliado_saida,
    data_conciliado
FROM
    (SELECT
         idcb.ID,
         idcb.conta,
         idcb.SALDO_INICIAL AS saldo_inicial
     FROM
         indexed_demonstrativo_contas_bancarias AS idcb
     GROUP BY
         idcb.ID) AS contas INNER JOIN
    (
        SELECT
            0 AS valor_conciliado_entrada,
            SUM(itl.valor) AS valor_conciliado_saida,
            itl.data AS data_conciliado,
            itl.conta AS origem
        FROM
            impostos_tarifas imptar
                INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
                INNER JOIN contas_bancarias cb3 ON cb3.ID = itl.conta
        WHERE
                itl.data <= NOW()
          AND imptar.debito_credito = 'DEBITO'
        GROUP BY origem, data_conciliado
    ) AS tarifas_creditos ON contas.ID = tarifas_creditos.origem

UNION

-- HISTÓRICO ESTORNOS
SELECT
    contas.conta,
    valor_conciliado_entrada,
    valor_conciliado_saida,
    data_conciliado
FROM
    (SELECT
         idcb.ID,
         idcb.conta,
         idcb.SALDO_INICIAL AS saldo_inicial
     FROM
         indexed_demonstrativo_contas_bancarias AS idcb
     GROUP BY
         idcb.ID) AS contas INNER JOIN
    (
        SELECT
            0 AS valor_conciliado_entrada,
            SUM(hep.valor) AS valor_conciliado_saida,
            hep.data_conciliada AS data_conciliado,
            p.origem
        FROM
            estorno_parcela hep
                INNER JOIN parcelas p ON (p.id = hep.parcela_id)
                INNER JOIN notas n ON (n.idNotas = p.idNota)
        WHERE
                hep.data_conciliada <= NOW()
        GROUP BY origem, data_conciliado
    ) AS historico_estornos ON contas.ID = historico_estornos.origem

UNION

-- DEVOLUÇÃO A FORNECEDORES
SELECT
    contas.conta,
    valor_conciliado_entrada,
    valor_conciliado_saida,
    data_conciliado
FROM
    (SELECT
         idcb.ID,
         idcb.conta,
         idcb.SALDO_INICIAL AS saldo_inicial
     FROM
         indexed_demonstrativo_contas_bancarias AS idcb
     GROUP BY
         idcb.ID) AS contas INNER JOIN
    (
        SELECT
            0 AS valor_conciliado_entrada,
            SUM(nb.valor) AS valor_conciliado_saida,
            nb.data_conciliado AS data_conciliado,
            nb.conta_bancaria_id AS origem
        FROM
            nota_baixas as nb
        WHERE
            nb.canceled_by IS NULL
          AND nb.conciliado = 'S'
          AND nb.data_conciliado <= NOW()
          AND nb.tipo_movimentacao = '1'
        GROUP BY origem, data_conciliado
    ) AS devolucao_fornecedores ON contas.ID = devolucao_fornecedores.origem

UNION

-- DEVOLUÇÃO DE CLIENTES
SELECT
    contas.conta,
    valor_conciliado_entrada,
    valor_conciliado_saida,
    data_conciliado
FROM
    (SELECT
         idcb.ID,
         idcb.conta,
         idcb.SALDO_INICIAL AS saldo_inicial
     FROM
         indexed_demonstrativo_contas_bancarias AS idcb
     GROUP BY
         idcb.ID) AS contas INNER JOIN
    (
        SELECT
            0 AS valor_conciliado_entrada,
            SUM(nb.valor) AS valor_conciliado_saida,
            nb.data_conciliado AS data_conciliado,
            nb.conta_bancaria_id AS origem
        FROM
            nota_baixas as nb
        WHERE
            nb.canceled_by IS NULL
          AND nb.conciliado = 'S'
          AND nb.data_conciliado <= NOW()
          AND nb.tipo_movimentacao = '0'
        GROUP BY origem, data_conciliado
    ) AS devolucao_clientes ON contas.ID = devolucao_clientes.origem

UNION

-- SALDO INICIAL
SELECT
    saldo_inicial.conta,
    valor_conciliado_entrada,
    valor_conciliado_saida,
    data_conciliado
FROM
    (SELECT
         IF(idcb.SALDO_INICIAL >= 0, idcb.SALDO_INICIAL, 0) AS valor_conciliado_entrada,
         IF(idcb.SALDO_INICIAL <= 0, idcb.SALDO_INICIAL*(-1), 0) AS valor_conciliado_saida,
         idcb.DATA_SALDO_INICIAL AS data_conciliado,
         idcb.ID AS origem,
         idcb.conta
     FROM
         indexed_demonstrativo_contas_bancarias AS idcb
    ) AS saldo_inicial

ORDER BY data_conciliado);