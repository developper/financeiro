ALTER TABLE `parcelas` 
ADD COLUMN `tarifa_cartao` decimal(12, 2) NULL COMMENT 'Desconta o valor da tarifa do cartão de credito.' AFTER `perda`;