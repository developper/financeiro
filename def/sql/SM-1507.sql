CREATE TABLE `historico_lote_fatura` (
`id`  int(11) NULL AUTO_INCREMENT ,
`fatura_id`  int(11) NULL ,
`capa_lote`  varchar(255) NULL ,
`data_capa_lote`  datetime NULL ,
`data_envio_plano`  datetime NULL ,
`data_envio_plano_by`  int(11) NULL ,
`data_envio_plano_at`  datetime NULL ,
`protocolo_envio`  varchar(255) NULL ,
`cancelado_by`  int(11) NULL ,
`cancelado_at`  datetime NULL ,
PRIMARY KEY (`id`)
)
;


