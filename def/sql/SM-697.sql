CREATE TABLE alergia_medicamentosa_copia AS SELECT * FROM alergia_medicamentosa;

TRUNCATE alergia_medicamentosa;

ALTER TABLE `alergia_medicamentosa`
MODIFY COLUMN `ID` int(11) NOT NULL AUTO_INCREMENT FIRST ,
ADD PRIMARY KEY (`ID`);

INSERT INTO alergia_medicamentosa (
	ID_PACIENTE,
	NUMERO_TISS,
	CATALOGO_ID,
	DESCRICAO,
	ATIVO,
	DATA,
	USUARIO_ID
)
SELECT
	ID_PACIENTE,
	NUMERO_TISS,
	CATALOGO_ID,
	DESCRICAO,
	ATIVO,
	DATA,
	USUARIO_ID
FROM
	alergia_medicamentosa_copia;

DROP TABLE alergia_medicamentosa_copia;