CREATE TABLE `fatura_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fatura_status
-- ----------------------------
INSERT INTO `fatura_status` VALUES ('-1', 'Rascunho');
INSERT INTO `fatura_status` VALUES ('1', 'Enviado');
INSERT INTO `fatura_status` VALUES ('2', 'Pendente');
INSERT INTO `fatura_status` VALUES ('3', 'Em Análise');
INSERT INTO `fatura_status` VALUES ('4', 'Processado');
INSERT INTO `fatura_status` VALUES ('5', 'Faturado');
INSERT INTO `fatura_status` VALUES ('6', 'Precificar');

-- Modifcar as faturas com status pendentes para precificar.
Update fatura set
STATUS = 6
where
fatura.STATUS = 2 and fatura.ORCAMENTO = 0