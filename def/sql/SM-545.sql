-- ----------------------------
-- Table structure for justificativa_solicitacao_avulsa
-- ----------------------------
CREATE TABLE `justificativa_solicitacao_avulsa` (
  `id` int(11) NOT NULL,
  `justificativa` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of justificativa_solicitacao_avulsa
-- ----------------------------
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('1', 'NÃO LIBERADO PELA AUDITORIA');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('2', 'COMPLEMENTO PEDIDO PER�?ODICO');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('3', 'COMPLEMENTO PEDIDO ADITIVO');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('4', 'COMPLEMENTO PEDIDO ADMISSÃO');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('5', 'ATRASO NA AUTORIZAÇÃO DO PLANO');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('6', 'ALTERAÇÃO DO QUADRO CL�?NICO OU FERIDAS');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('7', 'PROCEDIMENTO PONTUAL');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('8', 'ATRASO NA PRESCRIÇÃO MÉDICA PERIÓDICA');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('9', 'ATRASO NA PRESCRIÇÃO MÉDICA ADITIVA');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('10', 'ATRASO NA PRESCRIÇÃO NUTRICIONAL PERIÓDICA');
