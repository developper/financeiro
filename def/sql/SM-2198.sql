ALTER TABLE `notas` 
ADD COLUMN `data_competencia` date NULL AFTER `valor`;
ALTER TABLE `notas` 
MODIFY COLUMN `data_competencia` char(7) NULL DEFAULT NULL AFTER `valor`;