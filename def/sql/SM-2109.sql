alter table plano_contas
    add POSSUI_RETENCOES int default 2 not null;

alter table plano_contas
    add ISS decimal(10,2) default 0.00 not null;

alter table plano_contas
    add ICMS decimal(10,2) default 0.00 not null;

alter table plano_contas
    add IPI decimal(10,2) default 0.00 not null;

alter table plano_contas
    add PIS decimal(10,2) default 0.00 not null;

alter table plano_contas
    add COFINS decimal(10,2) default 0.00 not null;

alter table plano_contas
    add CSLL decimal(10,2) default 0.00 not null;

alter table plano_contas
    add IR decimal(10,2) default 0.00 not null;

alter table plano_contas
    add PCC decimal(10,2) default 0.00 not null;

alter table plano_contas
    add INSS decimal(10,2) default 0.00 not null;

