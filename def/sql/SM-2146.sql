alter table tipo_documento_financeiro
    add imposto enum('N', 'S') default 'N' not null;

UPDATE tipo_documento_financeiro SET imposto = 'S' WHERE id IN (5, 7, 8, 15, 16, 17, 24, 32);

UPDATE plano_contas SET PROVISIONA = 'n' WHERE PROVISIONA != 's';