ALTER TABLE `tiss_profissional` CHANGE `sigla_conselho` `sigla_conselho` ENUM('CRM')  CHARACTER SET utf8  NULL  DEFAULT NULL;

INSERT INTO `tiss_profissional` (`id`, `tiss_conselho_profissional_id`, `nome_completo`, `cpf`, `sigla_conselho`, `numero_conselho`, `conselho_estado_id`, `uf_conselho`, `especialidade_medica_id`, `cbos`)
VALUES
	(NULL, 5, 'CESAR CLEYTON F. BITTENCURT', '97464129515', '', 59352, 5, 'BA', 0, '223605');

CREATE TABLE `tiss_profissional_fatura` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_profissional_id` int(11) NOT NULL,
  `tiss_fatura_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO tiss_profissional_fatura (tiss_profissional_id, tiss_guia_id)
	SELECT tiss_profissional_id, tiss_fatura.id
  FROM tiss_guia
    INNER JOIN tiss_fatura ON tiss_fatura.tiss_guia_id = tiss_guia.id;

ALTER TABLE `tiss_guia_sadt` DROP `tiss_profissional_id`;
ALTER TABLE `tiss_guia` DROP `tiss_profissional_id`;


-- segunda parte
ALTER TABLE `tiss_profissional_fatura` CHANGE `tiss_fatura_id` `tiss_fatura_item_id` INT(11)  NOT NULL;
RENAME TABLE `tiss_profissional_fatura` TO `tiss_profissional_fatura_item`;

ALTER TABLE `tiss_guia` ADD `tiss_profissional_id` INT  NOT NULL  AFTER `tiss_contratado_id`;
UPDATE tiss_guia SET tiss_profissional_id = 1 WHERE tiss_profissional_id = 0;


