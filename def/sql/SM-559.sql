--Update de itens enviados para Ur sem valor, pega o valor da ultima entradadele no sistema.
SELECT 
  CONCAT("UPDATE catalogo set valor =",(SELECT
                                        entrada.valor
                                        FROM
                                        entrada
                                        WHERE
                                        entrada.CATALOGO_ID=C.ID
                                        ORDER BY
                                        entrada.id DESC
                                        LIMIT 1) ,
    " where catalogo.ID =",C.ID,";") 
FROM 
 saida as S inner join
 catalogo as C on (S.CATALOGO_ID = C.id)
WHERE 
 S.tipo in (0,1,3) and
 UR=5 and 
 S.`DATA_SISTEMA` >= '2014-08-10' AND
S.idCliente not in (142,220,69,263,112,101,67) AND
C.valor=0
GROUP BY
 C.id
ORDER BY
 concat(C.`principio`,' - ',C.`apresentacao`)

-- Update saida da logística da UR Alagoinhas 
SELECT 
CONCAT("UPDATE catalogo set valor =",(SELECT
                                        entrada.valor
                                        FROM
                                        entrada
                                        WHERE
                                        entrada.CATALOGO_ID=C.ID
                                        ORDER BY
                                        entrada.id DESC
                                        LIMIT 1) ,
    " where catalogo.ID =",C.ID,";")
FROM 
 saida as S inner join
 catalogo as C on (S.CATALOGO_ID = C.id) inner JOIN
clientes as Cli on (S.idCliente = Cli.idClientes)
WHERE 
 S.tipo in (0,1,3) and 
 S.`DATA_SISTEMA` >= '2014-08-10' AND
S.idCliente not in (142,220,69,263,112,101,67) AND 
Cli.empresa = 5 AND
C.valor=0
GROUP BY
 C.id
ORDER BY
 concat(C.`principio`,' - ',C.`apresentacao`)


-- Saida de equipamentos para UR Alagoinhas.
SELECT
saida.CATALOGO_ID as CATALOGO,
'EQUIPAMENTO' as TIPO,
cobrancaplanos.item as ITEM,

Sum(saida.quantidade) as QUANTIDADE,
'0' as VALOR
FROM
saida
INNER JOIN cobrancaplanos ON saida.CATALOGO_ID = cobrancaplanos.id
WHERE
saida.tipo = 5 AND
saida.DATA_SISTEMA >= '2014-08-10' AND
saida.UR = 5 AND
saida.idCliente not in (142,220,69,263,112,101,67)
GROUP BY
saida.CATALOGO_ID
ORDER BY
cobrancaplanos.item

---- Saida de Materiais, Medicamentos e Dietas para UR Alagoinhas.
SELECT 
 C.id as CATALOGO,
 (case C.tipo
 when 0 then 'Medicamento'
 when 1 then 'Material'
 when 3 then 'Dietas'
 end) as TIPO, 
 concat(C.`principio`,' - ',C.`apresentacao`) as ITEM,
 SUM(S.quantidade) as QUANTIDADE,
 ROUND(SUM(C.`valor`),2)  as VALOR,
 SUM(C.`valorMedio`) as VALOR_MEDIO
FROM 
 saida as S inner join
 catalogo as C on (S.CATALOGO_ID = C.id)
WHERE 
 S.tipo in (0,1,3) and
 UR=5 and 
 S.`DATA_SISTEMA` >= '2014-08-10' AND
S.idCliente not in (142,220,69,263,112,101,67)
GROUP BY
 C.id
ORDER BY
 concat(C.`principio`,' - ',C.`apresentacao`)

-- Solicitações de enfermagem (material, medicamento e dietas) para logistica da UR de Alagoinhas
SELECT
catalogo.ID CATALOGO,
(case solicitacoes.tipo
when 0 then 'Medicamento'
when 1 then 'Material'
when 3 then 'Dieta'
end) as TIPO,
CONCAT(catalogo.principio,' - ',catalogo.apresentacao) as  ITEM,

SUM(IF(solicitacoes.TIPO_SOLICITACAO = '-1' or solicitacoes.entrega_imediata = 'S' , solicitacoes.autorizado,solicitacoes.qtd)) AS QUANTIDADE,
catalogo.valor as VALOR
FROM
solicitacoes
INNER JOIN catalogo ON solicitacoes.CATALOGO_ID = catalogo.ID
INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
WHERE
solicitacoes.`data` >= '2014-08-10' AND
solicitacoes.tipo IN (0,1,3) AND
 clientes.empresa=5 AND
 solicitacoes.TIPO_SOLICITACAO<>4 AND
 solicitacoes.paciente not in (142,220,69,263,112,101,67)
GROUP BY
solicitacoes.CATALOGO_ID
ORDER BY ITEM



--Solicitação de enfermagem (equipamentos) para logística da UR Alagoinhas.

SELECT
 cobrancaplanos.id as CATALOGO,
"Equipamento" as TIPO,
cobrancaplanos.item  as  ITEM,

SUM(IF(solicitacoes.TIPO_SOL_EQUIPAMENTO <> 2  , solicitacoes.autorizado,0)) AS QUANTIDADE,
'0' as VALOR
FROM
solicitacoes
INNER JOIN cobrancaplanos ON solicitacoes.CATALOGO_ID = cobrancaplanos.id
INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
WHERE
solicitacoes.`data` >= '2014-08-10' AND
solicitacoes.tipo = 5 AND
solicitacoes.TIPO_SOLICITACAO <> 4 AND
clientes.empresa=5 AND
 solicitacoes.paciente not in (142,220,69,263,112,101,67)
GROUP BY
solicitacoes.CATALOGO_ID
ORDER BY ITEM


-- Saida de meterial , medicamento e dieta feita pela Logística da UR Alagoinhas para seus pacientes.
SELECT 
 C.id as CATALOGO,
 (case C.tipo
 when 0 then 'Medicamento'
 when 1 then 'Material'
 when 3 then 'Dietas'
 end) as TIPO, 
 concat(C.`principio`,' - ',C.`apresentacao`) as ITEM,
 SUM(S.quantidade) as QUANTIDADE,
 ROUND(SUM(C.`valor`),2) as VALOR,

FROM 
 saida as S inner join
 catalogo as C on (S.CATALOGO_ID = C.id) inner JOIN
clientes as Cli on (S.idCliente = Cli.idClientes)
WHERE 
 S.tipo in (0,1,3) and 
 S.`DATA_SISTEMA` >= '2014-08-10' AND
S.idCliente not in (142,220,69,263,112,101,67) AND 
Cli.empresa = 5
GROUP BY
 C.id
ORDER BY
 concat(C.`principio`,' - ',C.`apresentacao`)

-- Saida de equipamentos feita pela Logística da UR Alagoinhas para seus pacientes
SELECT
saida.CATALOGO_ID as CATALOGO,
cobrancaplanos.item as ITEM,
'EQUIPAMENTO' as TIPO,
Sum(saida.quantidade) as QUANTIDADE,
'0' as VALOR
FROM
saida
INNER JOIN cobrancaplanos ON saida.CATALOGO_ID = cobrancaplanos.id
INNER JOIN clientes ON saida.idCliente=clientes.idClientes 
WHERE
saida.tipo = 5 AND
saida.DATA_SISTEMA >= '2014-08-10' AND
clientes.empresa =5 AND
saida.idCliente not in (142,220,69,263,112,101,67)
GROUP BY
saida.CATALOGO_ID
ORDER BY
cobrancaplanos.item
