UPDATE `empresas` SET `ATIVO`='N' WHERE (`id`='8');
UPDATE `empresas` SET `ATIVO`='N' WHERE (`id`='3');
UPDATE `empresas` SET `ATIVO`='N' WHERE (`id`='6');
UPDATE `empresas` SET `ATIVO`='N' WHERE (`id`='9');
UPDATE `empresas` SET `ATIVO`='N' WHERE (`id`='10');

INSERT INTO `ocorrencia_classificacao` (`classificacao`) VALUES ('Queixas Gerais');

-- ----------------------------
-- Table structure for `setores`
-- ----------------------------
DROP TABLE IF EXISTS `setores`;
CREATE TABLE `setores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setor` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of setores
-- ----------------------------
INSERT INTO `setores` VALUES ('1', 'Dir. Assistência');
INSERT INTO `setores` VALUES ('2', 'Dir. Adm.');
INSERT INTO `setores` VALUES ('3', 'RH');
INSERT INTO `setores` VALUES ('4', 'CID');
INSERT INTO `setores` VALUES ('5', 'TI');
INSERT INTO `setores` VALUES ('6', 'Auditoria');
INSERT INTO `setores` VALUES ('7', 'Financeiro');
INSERT INTO `setores` VALUES ('8', 'Logística');
INSERT INTO `setores` VALUES ('9', 'Farmácia');
INSERT INTO `setores` VALUES ('10', 'Sac');
INSERT INTO `setores` VALUES ('11', 'Serviço Social');
INSERT INTO `setores` VALUES ('12', 'Operações');
INSERT INTO `setores` VALUES ('13', 'Enfermagem');
INSERT INTO `setores` VALUES ('14', 'Médico');

UPDATE `setores` SET `setor`='Patrimônio' WHERE (`id`='12');

UPDATE `ocorrencia_subclassificacao` SET `descricao`='Atraso do profissional' WHERE (`id`='17');
INSERT INTO `ocorrencia_subclassificacao` (id,`descricao`, `ocorrencia_classificacao_id`) VALUES (60,'Item violado', '1');
INSERT INTO `ocorrencia_classificacao` (id,`classificacao`) VALUES (10,'Autorização');
INSERT INTO `ocorrencia_subclassificacao` (id,`descricao`, `ocorrencia_classificacao_id`) VALUES (61,'Pendência', '10');

ALTER TABLE `ocorrencia`
DROP COLUMN `qualidade_retorno`,
MODIFY COLUMN `descricao_qualidade_retorno`  text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL AFTER `retorno_hc`;
