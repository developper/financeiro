create table bordero
(
    id int auto_increment,
    banco_id int not null,
    conta_id int not null,
    bordero_tipo_id int not null,
    descricao text null,
    ativo enum('s', 'n') not null default 's',
    created_by int not null,
    created_at datetime not null,
    updated_by int null,
    updated_at datetime null,
    constraint bordero_pk
        primary key (id)
)
    comment 'Tabela que contém os dados do bordero';

create table bordero_parcelas
(
    id int auto_increment,
    bordero_id int not null,
    parcela_id int not null,
    vencimento date not null,
    constraint bordero_parcelas_pk
        primary key (id)
)
    comment 'Tabela que recebe os dados da parcela que a compoe';

create table bordero_tipo
(
    id int auto_increment,
    descricao_tipo varchar(255) not null,
    forma_pagamento varchar(20) not null,
    constraint tipo_bordero_pk
        primary key (id)
);

INSERT INTO bordero_tipo (`descricao_tipo`, `forma_pagamento`) VALUES ('TED - MESMO TITULAR', 'TED');
INSERT INTO bordero_tipo (`descricao_tipo`, `forma_pagamento`) VALUES ('TED - OUTRO TITULAR', 'TED');
INSERT INTO bordero_tipo (`descricao_tipo`, `forma_pagamento`) VALUES ('PAGAMENTO DE TITULOS NO SANTANDER', 'BOLETO');
INSERT INTO bordero_tipo (`descricao_tipo`, `forma_pagamento`) VALUES ('PAGAMENTO DE TITULOS NO BRADESCO', 'BOLETO');
INSERT INTO bordero_tipo (`descricao_tipo`, `forma_pagamento`) VALUES ('PAGAMENTO DE TITULOS EM OUTRO BANCO', 'BOLETO');
INSERT INTO bordero_tipo (`descricao_tipo`, `forma_pagamento`) VALUES ('PAGAMENTO DE TRIBUTOS - DARF SIMPLES', 'TRIBUTO');
INSERT INTO bordero_tipo (`descricao_tipo`, `forma_pagamento`) VALUES ('PAGAMENTO DE TRIBUTOS - DARF NORMAL', 'TRIBUTO');
INSERT INTO bordero_tipo (`descricao_tipo`, `forma_pagamento`) VALUES ('PAGAMENTO DE TRIBUTOS - DARJ', 'TRIBUTO');
INSERT INTO bordero_tipo (`descricao_tipo`, `forma_pagamento`) VALUES ('PAGAMENTO DE TRIBUTOS - GPS', 'TRIBUTO');
INSERT INTO bordero_tipo (`descricao_tipo`, `forma_pagamento`) VALUES ('PAGAMENTO A CONCESSIONARIAS', 'CONVENIO');
