ALTER TABLE `fatura_controle_recebimento`
  ADD COLUMN `desconto_imposto` float(20, 2) NOT NULL AFTER `modalidade_fatura`,
  ADD COLUMN `valor_liquido` float(20, 2) NOT NULL AFTER `desconto_imposto`,
  ADD COLUMN `percentual_desconto_imposto` float(20, 2) NOT NULL AFTER `desconto_imposto`;

ALTER TABLE `historico_fatura_controle_recebimento`
  ADD COLUMN `desconto_imposto` float(20, 2) NOT NULL AFTER `modalidade_fatura`,
  ADD COLUMN `valor_liquido` float(20, 2) NOT NULL AFTER `desconto_imposto`,
  ADD COLUMN `percentual_desconto_imposto` float(20, 2) NOT NULL AFTER `desconto_imposto`;