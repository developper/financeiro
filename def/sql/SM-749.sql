SELECT
	clientes.nome AS Paciente,
	empresas.nome AS UR,
	clientes.TEL_DOMICILIAR AS Telefone_Domicilio,
	clientes.TEL_RESPONSAVEL AS Telefone_Responsavel,
	clientes.TEL_CUIDADOR AS Telefone_Cuidador,
	clientes.TEL_LOCAL_INTERNADO AS Telefone_Local_Internacao
FROM
	clientes
INNER JOIN empresas ON empresas.id = clientes.empresa
WHERE
	clientes.`status` = 4
ORDER BY
	UR, clientes.nome