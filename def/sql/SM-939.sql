-- Consulta relat�rio de prorroga��o de enfermagem  POR PER�ODO .
set @inicio ='2015-11-01';
set @fim    ='2015-11-30';
SELECT
UPPER(clientes.nome) as PACIENTE,
UPPER(usuarios.nome) as ENFERMEIRO,
'PRORROGACAO ENF.' AS TIPO,
DATE_FORMAT(relatorio_prorrogacao_enf.`DATA`, '%d/%m/%Y') as DATA_SISTEMA,
DATE_FORMAT(relatorio_prorrogacao_enf.INICIO, '%d/%m/%Y'),
DATE_FORMAT(relatorio_prorrogacao_enf.FIM, '%d/%m/%Y'),
empresas.nome as UR
FROM
relatorio_prorrogacao_enf
INNER JOIN clientes ON relatorio_prorrogacao_enf.PACIENTE_ID = clientes.idClientes
INNER JOIN usuarios ON relatorio_prorrogacao_enf.USUARIO_ID = usuarios.idUsuarios
INNER JOIN empresas ON clientes.empresa = empresas.id
WHERE
(relatorio_prorrogacao_enf.`INICIO` BETWEEN @inicio AND @fim OR
relatorio_prorrogacao_enf.`FIM` BETWEEN @inicio AND @fim ) AND
relatorio_prorrogacao_enf.USUARIO_ID NOT IN (60,48,122) AND
relatorio_prorrogacao_enf.PACIENTE_ID NOT IN (142,67,69,128,434,220,263,112,635,101) AND
empresas.id in (11,2)


--Consulta Relat�rio Deflagrado de enfermagem POR PER�ODO
set @inicio ='2015-09-01';
set @fim    ='2015-09-30';
SELECT
UPPER(clientes.nome) AS PACIENTE,
UPPER(usuarios.nome) AS ENFERMEIRO,
'DEFLAGRADO ENF.' AS TIPO,
DATE_FORMAT(relatorio_deflagrado_enf.`DATA`,'%d/%m/%Y') as DATA_SISTEMA,
DATE_FORMAT(relatorio_deflagrado_enf.INICIO,'%d/%m/%Y')as INICIO,
DATE_FORMAT(relatorio_deflagrado_enf.FIM,'%d/%m/%Y') as FIM,
empresas.nome as UR
FROM
relatorio_deflagrado_enf
INNER JOIN clientes ON relatorio_deflagrado_enf.PACIENTE_ID = clientes.idClientes
INNER JOIN usuarios ON relatorio_deflagrado_enf.USUARIO_ID = usuarios.idUsuarios
INNER JOIN empresas ON clientes.empresa = empresas.id
WHERE
(relatorio_deflagrado_enf.`INICIO` BETWEEN @inicio AND @fim OR
relatorio_deflagrado_enf.`FIM` BETWEEN @inicio AND @fim )AND
relatorio_deflagrado_enf.USUARIO_ID NOT IN (60,48,122) AND
relatorio_deflagrado_enf.PACIENTE_ID NOT IN (142,67,69,128,434,220,263,112,635,101) AND
empresas.id in (11,2)

--Consulta Relat�rio Deflagrado m�dico POR PER�ODO
set @inicio ='2015-09-01';
set @fim    ='2015-09-30';
SELECT
clientes.nome as PACIENTE,
usuarios.nome AS MEDICO,
'DEFLAGRADO MED.' AS TIPO,
DATE_FORMAT(relatorio_deflagrado_med.`data`, '%d/%m/%Y') AS DATA_SISTEMA,
DATE_FORMAT(relatorio_deflagrado_med.inicio, '%d/%m/%Y') AS INICIO,
DATE_FORMAT(relatorio_deflagrado_med.fim, '%d/%m/%Y') AS FIM,
empresas.nome AS EMPRESA
FROM
relatorio_deflagrado_med
INNER JOIN clientes ON relatorio_deflagrado_med.cliente_id = clientes.idClientes
INNER JOIN usuarios ON relatorio_deflagrado_med.usuario_id = usuarios.idUsuarios
INNER JOIN empresas ON clientes.empresa = empresas.id
WHERE
relatorio_deflagrado_med.cancelado_em IS NULL AND
(relatorio_deflagrado_med.inicio BETWEEN @inicio AND @fim OR
relatorio_deflagrado_med.fim BETWEEN @inicio AND @fim) AND
relatorio_deflagrado_med.usuario_id NOT IN (60,48,122)  AND
relatorio_deflagrado_med.cliente_id  NOT IN (142,67,69,128,434,220,263,112,635,101) AND
empresas.id in (11,2)
--Consulta Relat�rio PRORROGACAO m�dico POR PER�ODO
set @inicio ='2015-11-01';
set @fim    ='2015-11-30';
SELECT
clientes.nome AS PACIENTE,
usuarios.nome AS MEDICO,
'PRORROGACAO MED.' AS TIPO,
DATE_FORMAT(relatorio_prorrogacao_med.`DATA`, '%d/%m/%Y') AS DATA_SISTEMA,
DATE_FORMAT(relatorio_prorrogacao_med.INICIO, '%d/%m/%Y') AS INICIO,
DATE_FORMAT(relatorio_prorrogacao_med.FIM, '%d/%m/%Y')AS FIM,
empresas.nome AS EMPRESA
FROM
relatorio_prorrogacao_med
INNER JOIN clientes ON relatorio_prorrogacao_med.PACIENTE_ID = clientes.idClientes
INNER JOIN usuarios ON relatorio_prorrogacao_med.USUARIO_ID = usuarios.idUsuarios
INNER JOIN empresas ON clientes.empresa = empresas.id
WHERE
(relatorio_prorrogacao_med.INICIO BETWEEN @inicio AND @fim OR
relatorio_prorrogacao_med.FIM BETWEEN @inicio AND @fim ) AND
relatorio_prorrogacao_med.PACIENTE_ID NOT IN (142,67,69,128,434,220,263,112,635,101) AND
relatorio_prorrogacao_med.USUARIO_ID  NOT IN (60,48,122) AND
 empresas.id in (11,2)