ALTER TABLE `fornecedores`
ADD COLUMN `created_at`  datetime NULL AFTER `responsavel_equipamento`,
ADD COLUMN `created_by`  int(11) NULL AFTER `created_at`;