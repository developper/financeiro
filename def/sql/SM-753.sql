-- Itens Avulsos (material, dieta, medicamento, equipamento)
set @inicio ='2015-03-01';
set @fim    ='2015-04-08';
SELECT
UPPER(clientes.nome) as PACIENTE,
UPPER(CONCAT(catalogo.principio,' ',catalogo.apresentacao)) AS ITEM,
UPPER(usuarios.nome) as ENFERMEIRO,
(CASE solicitacoes.entrega_imediata
  when 'S' then 'SIM' 
  when 'N' then 'NÃO'
  else ' ' END
) as ENTREGA_IMEDIATA,
justificativa_solicitacao_avulsa.justificativa,
UPPER(empresas.nome) as UNIDADE_REGIONAL,
solicitacoes.`data` AS DATA_SOLICITACAO
FROM
solicitacoes
INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
INNER JOIN empresas ON clientes.empresa = empresas.id
INNER JOIN usuarios ON solicitacoes.enfermeiro = usuarios.idUsuarios
INNER JOIN catalogo ON solicitacoes.CATALOGO_ID = catalogo.ID
LEFT JOIN  justificativa_solicitacao_avulsa ON justificativa_solicitacao_avulsa.id = solicitacoes.JUSTIFICATIVA_AVULSO_ID
WHERE
solicitacoes.tipo IN (0, 1, 3) AND
solicitacoes.`data` between @inicio AND @fim AND
solicitacoes.idPrescricao = -1 AND
solicitacoes.enfermeiro NOT IN (60,48,122) AND
solicitacoes.paciente NOT IN (142,67,69,128,434,220,263,112,635,101) AND 
empresas.id in (1,2)
UNION ALL
SELECT
UPPER(clientes.nome) as PACIENTE,
UPPER(cobrancaplanos.item) AS ITEM,
UPPER(usuarios.nome) as ENFERMEIRO,
(CASE solicitacoes.entrega_imediata
  when 'S' then 'SIM' 
  when 'N' then 'NÃO'
  else ' ' END
) as ENTREGA_IMEDIATA,
justificativa_solicitacao_avulsa.justificativa,
UPPER(empresas.nome) as UNIDADE_REGIONAL,
solicitacoes.`data` AS DATA_SOLICITACAO
FROM
solicitacoes
INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
INNER JOIN empresas ON clientes.empresa = empresas.id
INNER JOIN usuarios ON solicitacoes.enfermeiro = usuarios.idUsuarios
INNER JOIN cobrancaplanos ON solicitacoes.CATALOGO_ID = cobrancaplanos.id
LEFT JOIN  justificativa_solicitacao_avulsa ON justificativa_solicitacao_avulsa.id = solicitacoes.JUSTIFICATIVA_AVULSO_ID
WHERE
solicitacoes.tipo =5 AND
solicitacoes.`data` between @inicio AND @fim AND
solicitacoes.idPrescricao = -1 AND
solicitacoes.enfermeiro NOT IN (60,48,122) AND
solicitacoes.paciente NOT IN (142,67,69,128,434,220,263,112,635,101) AND 
solicitacoes.TIPO_SOL_EQUIPAMENTO =1 AND 
empresas.id in (1,2)
ORDER BY DATA_SOLICITACAO ASC
