CREATE TABLE `historico_modificacao_estoque` (
`id`  int(11) NOT NULL ,
`quantidade_anterior`  int(11) NOT NULL ,
`quantidade_modificada`  int(11) NOT NULL ,
`quantidade_min_anterior`  int(11) NOT NULL ,
`quantidade_min_modificada`  int(11) NOT NULL ,
`usuario_id`  int(11) NOT NULL ,
`catalogo_id`  int(11) NOT NULL ,
`data`  datetime NOT NULL ,
`justificativa`  text NOT NULL
)
;

ALTER TABLE `historico_modificacao_estoque`
ADD COLUMN `ur`  int(11) NULL AFTER `justificativa`;

ALTER TABLE `historico_modificacao_estoque`
MODIFY COLUMN `id`  int(11) NOT NULL AUTO_INCREMENT FIRST ,
MODIFY COLUMN `ur`  int(11) NOT NULL AFTER `justificativa`,
ADD PRIMARY KEY (`id`);

ALTER TABLE `historico_modificacao_estoque`
ADD COLUMN `estoque_id`  int(11) NULL AFTER `ur`;

ALTER TABLE `historico_modificacao_estoque`
MODIFY COLUMN `estoque_id`  int(11) NOT NULL AFTER `ur`,
ADD COLUMN `tipo`  int(1) NOT NULL AFTER `estoque_id`;