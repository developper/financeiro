ALTER TABLE fornecedores ADD tipo_chave_pix_id INT NULL AFTER plano_saude;
ALTER TABLE fornecedores ADD chave_pix varchar(100) NULL  AFTER tipo_chave_pix_id;

CREATE TABLE tipo_chave_pix (
	id INT auto_increment NOT NULL,
	tipo varchar(100) NOT NULL,    
	PRIMARY KEY (`id`)

)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci;

INSERT INTO tipo_chave_pix (id,tipo)
	VALUES (1,'CPF');
INSERT INTO tipo_chave_pix (id,tipo)
	VALUES (2,'CNPJ');
INSERT INTO tipo_chave_pix (id,tipo)
	VALUES (3,'EMAIL');
INSERT INTO tipo_chave_pix (id,tipo)
	VALUES (4,'CELULAR');