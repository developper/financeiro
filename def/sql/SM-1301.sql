-- coluna para identificar se o fornecedor � responsavel por equipamentos.
ALTER TABLE `fornecedores`
ADD COLUMN `responsavel_equipamento`  enum('NAO','SIM') NOT NULL DEFAULT 'NAO' AFTER `IPCC`;

ALTER TABLE `saida`
ADD COLUMN `fornecedor`  int(11) NULL AFTER `ATIVO`;

ALTER TABLE `equipamentosativos`
ADD COLUMN `fornecedor`  int(11) NULL AFTER `finalizado_por`;