INSERT INTO `area_permissoes` VALUES (NULL, 99, 2, 'financeiro_conta_pagar_receber_buscar_parcela_editar_valores', 'Editar Valores da Parcela');

alter table bordero_parcelas
    add valor_parcela decimal(10,2) default 0.00 not null;

alter table bordero_parcelas
    add juros decimal(10,2) default 0.00 not null;

alter table bordero_parcelas
    add desconto decimal(10,2) default 0.00 not null;

alter table bordero_parcelas
    add tarifas_adicionais decimal(10,2) default 0.00 not null;
