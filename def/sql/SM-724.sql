SELECT
clientes.nome as PACIENTE,
concat(catalogo.principio,'-',catalogo.apresentacao) as ITEM,
itens_prescricao.inicio as INICIO,
itens_prescricao.fim as FIM,
itens_prescricao.obs as OBSERVACAO,
empresas.nome as EMPRESA
FROM
prescricoes
INNER JOIN itens_prescricao ON itens_prescricao.idPrescricao = prescricoes.id
INNER JOIN catalogo ON itens_prescricao.CATALOGO_ID = catalogo.ID
INNER JOIN clientes ON prescricoes.paciente = clientes.idClientes
INNER JOIN empresas ON clientes.empresa = empresas.id
WHERE
(year(prescricoes.inicio) = 2015 OR
YEAR(prescricoes.fim = 2015)) AND
clientes.empresa IN (1, 2) AND
clientes.idClientes NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101) AND
(itens_prescricao.tipo = 4 OR catalogo.ANTIBIOTICO = 'S') AND
prescricoes.Carater not IN (4,7)
