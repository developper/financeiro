SELECT
clientes.nome as paciente ,
CONCAT(catalogo.principio,' ',catalogo.apresentacao) as item,
solicitacoes.DATA_SOLICITACAO,
solicitacoes.data_auditado,
TIMEDIFF(solicitacoes.data_auditado,solicitacoes.DATA_SOLICITACAO) DIFERENCA_HORAS,
MONTH(solicitacoes.DATA_SOLICITACAO) as MES_SOLICITACAO,
solicitacoes.OBS_PENDENTE
FROM
solicitacoes
INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
INNER JOIN catalogo ON solicitacoes.CATALOGO_ID = catalogo.ID
WHERE
solicitacoes.paciente IN (855,841,816,843) AND
YEAR(solicitacoes.`data`) and 
solicitacoes.tipo in (1,0,3) and 
solicitacoes.`status` not in (-1,-2) and 
solicitacoes.data_auditado > '0000-00-00 00:00:00'
UNION ALL
SELECT
clientes.nome as paciente,
cobrancaplanos.item ,
solicitacoes.DATA_SOLICITACAO,
solicitacoes.data_auditado,
TIMEDIFF(solicitacoes.data_auditado,solicitacoes.DATA_SOLICITACAO) DIFERENCA_HORAS,
MONTH(solicitacoes.DATA_SOLICITACAO) as MES_SOLICITACAO,
solicitacoes.OBS_PENDENTE
FROM
solicitacoes
INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
INNER JOIN cobrancaplanos ON solicitacoes.CATALOGO_ID = cobrancaplanos.id
WHERE
solicitacoes.paciente IN (855,841,816,843) AND
YEAR(solicitacoes.`data`) and 
solicitacoes.tipo=5 and 
solicitacoes.`status` not in (-1,-2) and 
solicitacoes.data_auditado > '0000-00-00 00:00:00'

ORDER BY paciente ASC
