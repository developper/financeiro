SELECT 

date_format(s.`DATA_SOLICITACAO`,'%d/%m/%Y  %H:%s:%i') as Data,

concat(b.principio,' ',b.apresentacao) as Item ,

c.nome as Paciente,

u.nome as Enfermeiro,

s.`autorizado` as Autorizados

 

FROM

`solicitacoes` as s 

inner join catalogo as b on (b.numero_tiss = s.`NUMERO_TISS`)

inner join clientes as c on (c.idClientes = s. paciente)

inner join empresas as e on (e.id = c.empresa )

inner join usuarios as u on (u.idUsuarios = s.enfermeiro)

WHERE 

s.`idPrescricao`= '-1' and

s.`DATA_SOLICITACAO` between '2014-03-01' and '2014-03-31' 

and s.`idPrescricao` ='-1'

and s.status in (1,2)

and s.enfermeiro not in (60,48,122) and s.paciente not in (142,220,69,263,112,101,67)

and c.empresa = 1
order by s.`DATA_SOLICITACAO`