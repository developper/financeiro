INSERT INTO impostos_tarifas (id, item, tipo, debito_credito, ipcf) VALUES (NULL, 'ESTORNO BANCÁRIO', 'CREDITO', 'CREDITO', '1.0.4.03');

create table historico_estorno_parcela
(
    id int auto_increment,
    parcela_id int not null,
    data_processada datetime not null,
    processada_por int null,
    origem int not null,
    data_conciliada date not null,
    valor decimal(10,2) not null,
    aplicacao int not null,
    criado_em datetime not null,
    constraint historico_estorno_parcela_pk
        primary key (id)
);

