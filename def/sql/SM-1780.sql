ALTER TABLE `operadoras`
ADD COLUMN `data_pagamento`  varchar(255) NULL AFTER `EMAIL`,
ADD COLUMN `dia_maximo_envio_fatura`  int(2) NULL AFTER `data_pagamento`,
ADD COLUMN `created_by`  int(11) NULL AFTER `dia_maximo_envio_fatura`,
ADD COLUMN `created_at`  datetime NULL AFTER `created_by`,
ADD COLUMN `updated_by`  int(11) NULL AFTER `created_at`,
ADD COLUMN `updated_at`  datetime NULL AFTER `update_by`,
ADD COLUMN `canceled_by`  int(11) NULL AFTER ,
ADD COLUMN `canceled_at`  datetime NULL AFTER ;


CREATE TABLE `historico_operadora` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`operadora_id`  int(11) NULL ,
`nome`  varchar(255) NULL ,
`email`  varchar(255) NULL ,
`data_pagamento`  int(2) NULL ,
`dia_maximo_envio_fatura`  int(2) NULL ,
`created_by`  int(11) NULL ,
`created_at`  datetime NULL ,
`tipo`  varchar(255) NULL
PRIMARY KEY (`id`)
)
;


