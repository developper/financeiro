create table classe_valor
(
    id int auto_increment,
    descricao varchar(255) not null,
    constraint classe_valor_pk
        primary key (id)
);

create unique index classe_valor_descricao_uindex
    on classe_valor (descricao);

create table fornecedores_classe_valor
(
    id int auto_increment,
    fornecedor_id int not null,
    classe_valor_id int not null,
    constraint fornecedores_classe_valor_pk
        primary key (id)
);

