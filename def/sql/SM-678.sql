-- PEDIDOS INTERNOS
SELECT
	SUM(ipi.QTD)
FROM
	pedidosinterno AS pi
	INNER JOIN itenspedidosinterno ipi ON (pi.ID = ipi.PEDIDOS_INTERNO_ID)
WHERE
	`DATA` BETWEEN '2014-12-01'
AND '2014-12-31'
AND pi.CONFIRMADO = 'S';

-- PEDIDOS INTERNOS COM CARATER DE EMERGENCIA
SELECT
	SUM(ipi.QTD)
FROM
	pedidosinterno AS pi
	INNER JOIN itenspedidosinterno ipi ON (pi.ID = ipi.PEDIDOS_INTERNO_ID)
WHERE
	`DATA` BETWEEN '2014-12-01'
AND '2014-12-31'
AND pi.CONFIRMADO = 'S'
and ipi.EMERGENCIA = 1;

-- ITENS SOLICITADOS PELAS ENFERMEIRAS
SELECT
	SUM(s.qtd) as solicitados
FROM
solicitacoes AS s
INNER JOIN clientes AS c ON (s.paciente = c.idClientes)
WHERE
	s.`data` BETWEEN '2014-12-01'
AND '2014-12-31'
AND c.empresa = 1
AND s.`status` > -1
AND (entrega_imediata = 'n'
OR entrega_imediata IS NULL);

-- ITENS SOLICITADOS PELAS ENFERMEIRAS COM CARATER IMEDIATO
SELECT
	SUM(s.qtd) as solicitados
FROM
solicitacoes AS s
INNER JOIN clientes AS c ON (s.paciente = c.idClientes)
WHERE
	s.`data` BETWEEN '2014-12-01'
AND '2014-12-31'
AND c.empresa = 1
AND s.`status` > -1
AND entrega_imediata = 's';