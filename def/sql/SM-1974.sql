ALTER TABLE `remocoes`
ADD COLUMN `conta_recebido` int(11) NULL COMMENT 'Recebe o id da conta bancaria onde foi recebido a remoção.' AFTER `edited_at`;

ALTER TABLE `remocoes`
ADD COLUMN `recebido_em` date NULL COMMENT 'Inseriri a data que foi recebido' AFTER `conta_recebido`;