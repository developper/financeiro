ALTER TABLE `remocoes`
MODIFY COLUMN `valor_remocao` double(10, 2) NOT NULL AFTER `perimetro`,
MODIFY COLUMN `outros_custos` double(10, 2) NULL DEFAULT NULL AFTER `valor_remocao`,
MODIFY COLUMN `outras_despesas` double(10, 2) NULL DEFAULT NULL AFTER `tipo_remocao`,
MODIFY COLUMN `valor_medico` double(10, 2) NOT NULL AFTER `medico`,
MODIFY COLUMN `valor_tecnico` double(10, 2) NOT NULL AFTER `tecnico`,
MODIFY COLUMN `valor_condutor` double(10, 2) NOT NULL AFTER `condutor`,
MODIFY COLUMN `valor_enfermeiro` double(10, 2) NULL DEFAULT NULL AFTER `enfermeiro`;