-- Verificando notas com soma dos rateios maior 100 porcento e criando update para fazer ajuste

select
CONCAT("update rateio set PORCENTAGEM = (PORCENTAGEM - (",tabela.soma," - 100)) where ID = ",ID,";"),
tabela.soma,
rateio.*
from
rateio
inner join (
select
*
from
(
SELECT
notas.idNotas,
notas.`status`,
notas.`DATA`,
SUM(rateio.PORCENTAGEM) as soma
FROM
notas
INNER JOIN rateio ON notas.idNotas = rateio.NOTA_ID
WHERE
status <> "Cancelada"
group by notas.idNotas) as principal

where
soma >100 ) as tabela on rateio.NOTA_ID = tabela.idNotas
group by NOTA_ID
order by NOTA_ID

--
-- verificando notas com soma dos rateios menor que 100 porcento e criando update para fazer ajuste
--
select
CONCAT("update rateio set PORCENTAGEM = (PORCENTAGEM + (100 - ",tabela.soma,")) where ID = ",ID,";"),

tabela.soma,
rateio.*
from
rateio
inner join (
select
*
from
(
SELECT
notas.idNotas,
notas.`status`,
notas.`DATA`,
SUM(rateio.PORCENTAGEM) as soma
FROM
notas
INNER JOIN rateio ON notas.idNotas = rateio.NOTA_ID
WHERE
status <> "Cancelada"
group by notas.idNotas) as principal

where
soma < 100 ) as tabela on rateio.NOTA_ID = tabela.idNotas
group by NOTA_ID
order by NOTA_ID

