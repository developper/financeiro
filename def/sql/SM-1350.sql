-- consulta geral
SELECT
catalogo.ID,
concat(catalogo.principio,' ',catalogo.apresentacao)as  Item,
sum(entrada.qtd) as Quantidade_entrada ,
COALESCE(D.qtdDevolucao,0) as Quantidade_devolvida,
COALESCE(S.qtdSaida,0) as Quantidade_saida,
COALESCE(SI.qtdSaidaInterna,0) as Qunatidade_saida_interna,
(entrada.qtd + COALESCE(D.qtdDevolucao,0) + COALESCE(S.qtdSaida,0) + COALESCE(SI.qtdSaidaInterna,0)) as total,
entrada.LOTE,
entrada.VALIDADE
FROM
catalogo
LEFT JOIN entrada on catalogo.ID = entrada.CATALOGO_ID
LEFT JOIN
(

		SELECT
		catalogo.ID,
		concat(catalogo.principio,' ',catalogo.apresentacao)as  Item,
		sum(devolucao.QUANTIDADE) as qtdDevolucao,
		devolucao.LOTE
		FROM
		catalogo
		LEFT JOIN devolucao on catalogo.ID = devolucao.CATALOGO_ID
		WHERE
		catalogo.tipo = 1 AND
		catalogo.ATIVO = 'A' AND
		catalogo.TAG = 'DESCARTAVEL'
		AND
		devolucao.UR = 1
		GROUP BY catalogo.ID,devolucao.LOTE
		ORDER BY catalogo.ID, devolucao.LOTE

) as D on (catalogo.ID = D.ID and entrada.LOTE = D.LOTE)
LEFT JOIN
(

SELECT
catalogo.ID,
concat(catalogo.principio,' ',catalogo.apresentacao)as  Item,
sum(saida.quantidade) as qtdSaida,
saida.LOTE
FROM
catalogo
LEFT JOIN saida on catalogo.ID = saida.CATALOGO_ID
WHERE
catalogo.tipo = 1 AND
catalogo.ATIVO = 'A' AND
catalogo.TAG = 'DESCARTAVEL'
AND
		saida.UR in (2,4,5,7,11)


GROUP BY catalogo.ID,saida.LOTE
ORDER BY catalogo.ID, saida.LOTE

) as S on  (catalogo.ID = S.ID and entrada.LOTE = S.LOTE)
LEFT JOIN
(
	SELECT
	catalogo.ID,
	concat(catalogo.principio,' ',catalogo.apresentacao)as  Item,
	sum(saida_interna.QUANTIDADE) as qtdSaidaInterna,
	saida_interna.LOTE
	FROM
	catalogo
	LEFT JOIN saida_interna on catalogo.ID = saida_interna.CATALOGO_ID
	WHERE
	catalogo.tipo = 1 AND
	catalogo.ATIVO = 'A' AND
	catalogo.TAG = 'DESCARTAVEL'
	AND
	saida_interna.EMPRESA_ID = 1
	GROUP BY catalogo.ID,saida_interna.LOTE
	ORDER BY catalogo.ID, saida_interna.LOTE
) as SI on (catalogo.ID = SI.ID and entrada.LOTE = SI.LOTE)
WHERE
catalogo.tipo = 1 AND
catalogo.ATIVO = 'A' AND
catalogo.TAG = 'DESCARTAVEL'
AND entrada.UR =1
GROUP BY catalogo.ID,entrada.LOTE
ORDER BY catalogo.ID, entrada.LOTE

-- entradas da ur Central materiais descartáveis
SELECT
catalogo.ID,
concat(catalogo.principio,' ',catalogo.apresentacao)as  Item,
sum(entrada.qtd),
entrada.LOTE,
entrada.VALIDADE
FROM
catalogo
LEFT JOIN entrada on catalogo.ID = entrada.CATALOGO_ID
WHERE
catalogo.tipo = 1 AND
catalogo.ATIVO = 'A' AND
catalogo.TAG = 'DESCARTAVEL'
AND entrada.UR =1
GROUP BY catalogo.ID,entrada.LOTE
ORDER BY catalogo.ID, entrada.LOTE

-- saida ur central materiais descartáveis

SELECT
catalogo.ID,
concat(catalogo.principio,' ',catalogo.apresentacao)as  Item,
sum(saida.quantidade),
saida.LOTE
FROM
catalogo
LEFT JOIN saida on catalogo.ID = saida.CATALOGO_ID
WHERE
catalogo.tipo = 1 AND
catalogo.ATIVO = 'A' AND
catalogo.TAG = 'DESCARTAVEL'
AND
		saida.UR in (2,4,5,7,11)
		AND
		saida.`data` > '2014-02-01'

GROUP BY catalogo.ID,saida.LOTE
ORDER BY catalogo.ID, saida.LOTE

-- devoluçãoo ur central materiais descartáveis

SELECT
catalogo.ID,
concat(catalogo.principio,' ',catalogo.apresentacao)as  Item,
sum(devolucao.QUANTIDADE),
devolucao.LOTE
FROM
catalogo
LEFT JOIN devolucao on catalogo.ID = devolucao.CATALOGO_ID
WHERE
catalogo.tipo = 1 AND
catalogo.ATIVO = 'A' AND
catalogo.TAG = 'DESCARTAVEL'
AND
devolucao.UR = 1
GROUP BY catalogo.ID,devolucao.LOTE
ORDER BY catalogo.ID, devolucao.LOTE

-- saida internasda central

SELECT
catalogo.ID,
concat(catalogo.principio,' ',catalogo.apresentacao)as  Item,
sum(saida_interna.QUANTIDADE),
saida_interna.LOTE
FROM
catalogo
LEFT JOIN saida_interna on catalogo.ID = saida_interna.CATALOGO_ID
WHERE
catalogo.tipo = 1 AND
catalogo.ATIVO = 'A' AND
catalogo.TAG = 'DESCARTAVEL'
AND
saida_interna.EMPRESA_ID = 1
GROUP BY catalogo.ID,saida_interna.LOTE
ORDER BY catalogo.ID, saida_interna.LOTE
