-- modificações tabela plano.
ALTER TABLE `planosdesaude`
ADD COLUMN `ano_contrato`  int(4) NULL AFTER `tipo_medicamento`,
ADD COLUMN `data_reajuste`  date NULL AFTER `ano_contrato`,
ADD COLUMN `percentual_reajuste`  float NULL AFTER `data_reajuste`,
ADD COLUMN `created_at`  datetime NULL AFTER `percentual_reajuste`,
ADD COLUMN `created_by`  int(11) NULL AFTER `created_at`;

ALTER TABLE `planosdesaude`
ADD COLUMN `updated_at`  datetime NULL ,
ADD COLUMN `updated_by`  int(11) NULL ;

ALTER TABLE `planosdesaude`
MODIFY COLUMN `percentual_reajuste`  float(8,2) NULL DEFAULT NULL AFTER `data_reajuste`;

CREATE TABLE `historico_planosdesaude` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  planosdesaude_id int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `registro_ans` int(11) DEFAULT NULL COMMENT 'Referência: http://www.ans.gov.br/planos-de-saude-e-operadoras/informacoes-e-avaliacoes-de-operadoras/consultar-dados',
  `ATIVO` char(1) NOT NULL DEFAULT 'S',
  `imposto_iss` float(8,2) DEFAULT NULL,
  `tipo_medicamento` char(1) DEFAULT NULL COMMENT 'T - todos, G - generico',
  `ano_contrato` int(4) DEFAULT NULL,
  `data_reajuste` date DEFAULT NULL,
  `percentual_reajuste` float DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)

);

ALTER TABLE `historico_planosdesaude`
ADD COLUMN `acao`  enum('editar','criar') NULL DEFAULT NULL AFTER `created_by`;
ALTER TABLE `historico_planosdesaude`
MODIFY COLUMN `percentual_reajuste`  float(8,2) NULL DEFAULT NULL AFTER `data_reajuste`;