SELECT
	clientes.nome AS PACIENTE,
	usuarios.nome AS PROFISSIONAL,
	catalogo.principio AS ITEM,
	solicitacoes.qtd AS QTD,
	solicitacoes.JUSTIFICATIVA_AVULSO AS JUSTIFICATIVA,
	DATE_FORMAT(
		solicitacoes. DATA,
		'%d/%m/%Y'
	) AS DATA
FROM
	solicitacoes
INNER JOIN usuarios ON solicitacoes.enfermeiro = usuarios.idUsuarios
INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
INNER JOIN catalogo ON solicitacoes.CATALOGO_ID = catalogo.ID
WHERE
	solicitacoes. DATA BETWEEN '2014-05-01'
AND '2014-05-31'
AND solicitacoes.idPrescricao = - 1
ORDER BY
	solicitacoes.`data` ASC