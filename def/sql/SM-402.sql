CREATE TABLE `tiss_lote` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `numero_lote` varchar(50) NOT NULL DEFAULT '',
  `registration` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `tiss_guia_sadt` ADD `tiss_lote_id` INT(11)  NOT NULL  AFTER `numero_lote`;
ALTER TABLE `tiss_guia_sadt` CHANGE `numero_lote` `numero_lote` VARCHAR(50)  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL  DEFAULT '';
ALTER TABLE `tiss_guia_sadt` DROP INDEX `numero_lote`;
ALTER TABLE `tiss_xml` CHANGE `plan_text` `plan_text` LONGBLOB  NOT NULL;




