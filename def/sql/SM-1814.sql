SELECT
        UPPER(clientes.nome) as NOME,
		  statuspaciente.status as STATUS		
FROM
        equipamentosativos
        INNER JOIN clientes ON equipamentosativos.PACIENTE_ID = clientes.idClientes 
			INNER JOIN statuspaciente ON (clientes.status = statuspaciente.id)
        WHERE           
   equipamentosativos.DATA_FIM = '0000-00-00 00:00:00'
    AND equipamentosativos.DATA_INICIO > '0000-00-00 00:00:00'
        AND equipamentosativos.COBRANCA_PLANOS_ID IN ( 10, 15, 38, 69, 114, 147, 148 ) 
--        and clientes.`status` =4
        group by nome
