set @inicio ='2016-02-01';
set @fim    ='2016-07-31';
SELECT
UPPER(clientes.nome) as PACIENTE,
UPPER(CONCAT(catalogo.principio,' ',catalogo.apresentacao)) AS ITEM,
UPPER(usuarios.nome) as ENFERMEIRO,
(CASE solicitacoes.entrega_imediata
  when 'S' then 'SIM' 
  when 'N' then 'N�O'
  else ' ' END
) as ENTREGA_IMEDIATA,
justificativa_solicitacao_avulsa.justificativa,
solicitacoes.JUSTIFICATIVA_AVULSO as justificativa_outros,
UPPER(empresas.nome) as UNIDADE_REGIONAL,
DATE_FORMAT(solicitacoes.`data`,'%d/%m/%Y') as SOLICITADO_EM,
DATE_FORMAT(solicitacoes.data_auditado, '%d/%m/%Y %H:%i:%s') as AAUDITADO_EM,
solicitacoes.autorizado,
justificativa_itens_negados.JUSTIFICATIVA,
solicitacoes.enviado,
solicitacoes.`data` as dt
FROM
solicitacoes
INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
INNER JOIN empresas ON clientes.empresa = empresas.id
INNER JOIN usuarios ON solicitacoes.enfermeiro = usuarios.idUsuarios
INNER JOIN catalogo ON solicitacoes.CATALOGO_ID = catalogo.ID
LEFT JOIN  justificativa_solicitacao_avulsa ON justificativa_solicitacao_avulsa.id = solicitacoes.JUSTIFICATIVA_AVULSO_ID
LEFT JOIN justificativa_itens_negados on solicitacoes.JUSTIFICATIVA_ITENS_NEGADOS_ID = justificativa_itens_negados.ID
WHERE
solicitacoes.tipo IN (0, 1, 3) AND
solicitacoes.`data` between @inicio AND @fim AND
solicitacoes.idPrescricao = -1 AND
solicitacoes.enfermeiro NOT IN (60,48,122) AND
solicitacoes.paciente NOT IN (142,67,69,128,434,220,263,112,635,101) AND 
empresas.id in (4)
UNION ALL
SELECT
UPPER(clientes.nome) as PACIENTE,
UPPER(cobrancaplanos.item) AS ITEM,
UPPER(usuarios.nome) as ENFERMEIRO,
(CASE solicitacoes.entrega_imediata
  when 'S' then 'SIM' 
  when 'N' then 'N�O'
  else ' ' END
) as ENTREGA_IMEDIATA,
justificativa_solicitacao_avulsa.justificativa,
solicitacoes.JUSTIFICATIVA_AVULSO as justificativa_outros,
UPPER(empresas.nome) as UNIDADE_REGIONAL,
DATE_FORMAT(solicitacoes.`data` , '%d/%m/%Y') as SOLICITADO_EM,
DATE_FORMAT(solicitacoes.data_auditado, '%d/%m/%Y %H:%i:%s') as AAUDITADO_EM,
solicitacoes.autorizado,
justificativa_itens_negados.JUSTIFICATIVA,
solicitacoes.enviado,
solicitacoes.`data` as dt
FROM
solicitacoes
INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
INNER JOIN empresas ON clientes.empresa = empresas.id
INNER JOIN usuarios ON solicitacoes.enfermeiro = usuarios.idUsuarios
INNER JOIN cobrancaplanos ON solicitacoes.CATALOGO_ID = cobrancaplanos.id
LEFT JOIN  justificativa_solicitacao_avulsa ON justificativa_solicitacao_avulsa.id = solicitacoes.JUSTIFICATIVA_AVULSO_ID
LEFT JOIN justificativa_itens_negados on solicitacoes.JUSTIFICATIVA_ITENS_NEGADOS_ID = justificativa_itens_negados.ID
WHERE
solicitacoes.tipo =5 AND
solicitacoes.`data` between @inicio AND @fim AND
solicitacoes.idPrescricao = -1 AND
solicitacoes.enfermeiro NOT IN (60,48,122) AND
solicitacoes.paciente NOT IN (142,67,69,128,434,220,263,112,635,101) AND 
solicitacoes.TIPO_SOL_EQUIPAMENTO =1 AND 
empresas.id in (4)
ORDER BY  dt ASC