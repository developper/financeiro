ALTER TABLE `relatorio_nao_conformidade_item`
ADD COLUMN `status`  enum('RESOLVIDO','MANTIDO','NOVO') NULL DEFAULT 'NOVO' AFTER `canceled_by`,
ADD COLUMN `resolvido_by`  int(11) NULL AFTER `status`,
ADD COLUMN `resolvido_at`  datetime NULL AFTER `resolvido_by`,
ADD COLUMN `item_referencia`  int(11) NULL COMMENT 'recebe o id do item dessa mesma tabela que deu origem a ele' AFTER `resolvido_at`;

ALTER TABLE `relatorio_nao_conformidade_item`
ADD COLUMN `updated_by`  int(11) NULL AFTER `item_referencia`,
ADD COLUMN `updated_at`  datetime NULL AFTER `updated_by`;