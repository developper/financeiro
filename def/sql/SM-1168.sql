ALTER TABLE `relatorio_nao_conformidade`
ADD COLUMN `finalizado_at`  datetime NULL AFTER `data`,
ADD COLUMN `finalizado_by`  int(11) NULL AFTER `finalizado_at`;