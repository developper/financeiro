INSERT INTO 
`area_sistema`
(`nome`, `descricao`, `ordem`, `id_pai`, `menu`, `link`) 
VALUES 
('financeiro_conta_pagar_receber_antecipada', 'Contas Antecipadas', 5, 96, 0, '/financeiros/contas-antecipadas/?action=index');

INSERT INTO `area_permissoes`(`area_id`, `ordem`, descricao, `acao`) VALUES (106, 1, 'Criar Conta Antecipada', 'financeiro_conta_pagar_receber_antecipada_nova');
INSERT INTO `area_permissoes`(`area_id`, `ordem`, `acao`, `descricao`) VALUES (106, 2, 'financeiro_conta_pagar_receber_antecipada_editar', 'Editar Conta Antecipada');

ALTER TABLE `tipo_documento_financeiro` 
ADD COLUMN `adiantamento` varchar(255) NULL AFTER `descricao`,
ADD COLUMN `tipo_lancamento` enum('0','1') NULL DEFAULT NULL COMMENT 'Recebe para  0 recebimento para e 1 para despesas e Null para ambos os tipos ' AFTER `adiantamento`;

ALTER TABLE `tipo_documento_financeiro` 
MODIFY COLUMN `adiantamento` enum('S','N') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT 'Recebe S para os tipos que são adiantamento' AFTER `descricao`;

update
tipo_documento_financeiro
set
adiantamento = 'N';

UPDATE `tipo_documento_financeiro` SET `adiantamento` = 'S', `tipo_lancamento` = '1' WHERE `id` = 23;

UPDATE `tipo_documento_financeiro` SET `adiantamento` = 'S', `tipo_lancamento` = '0' WHERE `id` = 26;

INSERT INTO `area_permissoes`(`area_id`, `ordem`, `acao`, `descricao`) VALUES (106, 3, 'financeiro_conta_pagar_receber_antecipada_cancelar', 'Cancelar Conta Antecipada');

ALTER TABLE `notas` 
ADD COLUMN `nota_adiantamento` enum('S','N') NULL DEFAULT 'N' COMMENT 'Recebe para  S para notas de adiantamento ' AFTER `importada_iw`,
ADD COLUMN `cancelado_por` int(11) NULL AFTER `nota_adiantamento`,
ADD COLUMN `cancelado_em` datetime NULL AFTER `cancelado_por`;
