--necessário inclusão dessas linhas para funcionamento correto das feridas nas fichas de avaliação e evolução de enfermagem
ALTER TABLE `quadro_feridas_enfermagem`
ADD COLUMN `PERIODO`  int(2) NOT NULL AFTER `ODOR`;

CREATE TABLE `quadro_feridas_enf_periodo` (
`ID`  int(11) NOT NULL AUTO_INCREMENT ,
`PERIODO_TROCA`  varchar(250) NULL ,
PRIMARY KEY (`ID`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `quadro_feridas_enf_periodo` (`PERIODO_TROCA`)
VALUES
	('24hs'),
	('48hs'),
	('72hs'),
	('2x/SEMANA'),
	('1X/SEMANA');

	