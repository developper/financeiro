-- Material, dietas, Medicamentos
SELECT
CONCAT(catalogo.principio,' - ',catalogo.apresentacao) AS item,
if (solicitacoes.JUSTIFICATIVA_AVULSO_ID is NULL ,solicitacoes.JUSTIFICATIVA_AVULSO,justificativa_solicitacao_avulsa.justificativa) AS just,
clientes.nome AS nome_paciente,
usuarios.nome AS enfermeiro,
solicitacoes.`data`
FROM
solicitacoes
INNER JOIN catalogo ON catalogo.ID = solicitacoes.CATALOGO_ID
LEFT JOIN justificativa_solicitacao_avulsa ON justificativa_solicitacao_avulsa.id = solicitacoes.JUSTIFICATIVA_AVULSO_ID
INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
INNER JOIN usuarios ON solicitacoes.enfermeiro = usuarios.idUsuarios
WHERE
solicitacoes.DATA_SOLICITACAO BETWEEN '2014-10-01 00:00:00'  AND '2014-11-01 00:00:00' AND
solicitacoes.tipo IN (0,1,3) AND
solicitacoes.idPrescricao = -1 AND clientes.empresa = 1
ORDER BY
item ASC

-- Equipamentos

SELECT
cobrancaplanos.item,
if (solicitacoes.JUSTIFICATIVA_AVULSO_ID is NULL ,solicitacoes.JUSTIFICATIVA_AVULSO,justificativa_solicitacao_avulsa.justificativa) AS just,
clientes.nome AS nome_paciente,
usuarios.nome AS enfermeiro,
solicitacoes.`data`

FROM
solicitacoes
LEFT JOIN justificativa_solicitacao_avulsa ON justificativa_solicitacao_avulsa.id = solicitacoes.JUSTIFICATIVA_AVULSO_ID
INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
INNER JOIN usuarios ON solicitacoes.enfermeiro = usuarios.idUsuarios
INNER JOIN cobrancaplanos ON solicitacoes.CATALOGO_ID = cobrancaplanos.id
WHERE
solicitacoes.DATA_SOLICITACAO BETWEEN '2014-10-01 00:00:00'  AND '2014-11-01 00:00:00' AND
solicitacoes.tipo = 5 AND
solicitacoes.idPrescricao = -1 AND clientes.empresa = 1
ORDER BY
item ASC
