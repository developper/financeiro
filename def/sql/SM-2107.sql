alter table fornecedores
    add aliquota_iss decimal(10,2) default 0.00 null after validado_receita;

alter table fornecedores
    add aliquota_ir decimal(10,2) default 0.00 null after aliquota_iss;