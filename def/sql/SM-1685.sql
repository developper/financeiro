CREATE TABLE `historico_ajuste_autorizado_solicitacao` (
`id`  int(11) NULL AUTO_INCREMENT ,
`solicitacoes_id`  int(11) NULL ,
`justificativa`  text NULL ,
`quantidade`  int(11) NULL ,
`created_at`  datetime NULL ,
`created_by`  int(11) NULL ,
PRIMARY KEY (`id`)
)
;

ALTER TABLE `historico_ajuste_autorizado_solicitacao`
MODIFY COLUMN `quantidade`  int(11) NULL DEFAULT NULL COMMENT 'Quantidade que foi subtraída da coluna autorizado na tabela solicitações.' AFTER `justificativa`;
