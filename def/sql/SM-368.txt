--Relatório  de todas as medicações que sairam da mederi para seus pacientes de 01-07-2013 a 28-07-2014 data em q foi feita a colsulta.

SELECT
 ID as codigo_interno,
 CONCAT(principio, ' - ', apresentacao) as nome,
'Medicamento' as tipo_nome
FROM 
saida as S inner join 
catalogo as C  on (C.ID=S.CATALOGO_ID)
WHERE 
S.tipo =0 and 
S.idUsuario NOT IN( 60, 48, 122, 225, 7 ) and 
S.idCliente not in (142,220,69,263,112,101,67) and 
S.data >='2013-07-01 00:00:00'
GROUP BY C.ID ORDER BY nome ASC;
;