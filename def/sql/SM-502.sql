
-- SM-502

INSERT INTO `quadro_feridas_enf_grau` (`ID`, `GRAU`) VALUES ('5', 'Suspeita de lesão tissular profunda');
INSERT INTO `quadro_feridas_enf_grau` (`ID`, `GRAU`) VALUES ('6', 'UPP sem estadiamento');
INSERT INTO `quadro_feridas_enf_profundidade` (`ID`, `PROFUNDIDADE`) VALUES ('4', 'Cavitária sem exposição de fáscia');
UPDATE `quadro_feridas_enf_profundidade` SET `PROFUNDIDADE` = 'EXPOSIÇÃO DE TENDÃO E OSSO	' WHERE `ID` = '3';
UPDATE `quadro_feridas_enf_profundidade` SET `PROFUNDIDADE` = 'EXPOSIÇÃO DE FÁSCIA MUSCULAR	' WHERE `ID` = '2';
UPDATE `quadro_feridas_enf_profundidade` SET `PROFUNDIDADE` = 'SUPERFICIAL	' WHERE `ID` = '1';
UPDATE `quadro_feridas_enf_profundidade` SET `PROFUNDIDADE` = 'CAVITÁRIA SEM EXPOSIÇÃO DE FÁSCIA' WHERE `ID` = '4';
UPDATE `quadro_feridas_enf_grau` SET `GRAU` = 'SUSPEITA DE LESÃO TISSULAR PROFUNDA' WHERE `ID` = '5';
UPDATE `quadro_feridas_enf_grau` SET `GRAU` = 'UPP SEM ESTADIAMENTO' WHERE `ID` = '6';
