CREATE TABLE remocoes_historico_acoes  (
  `id` int(0) NOT NULL,
  `remocao_id` int(0) NULL,
  `usuario_id` int(0) NULL,
  `acao` enum('editar','reabrir','finalizar','desativar') NULL,
  `data` datetime NULL,
  PRIMARY KEY (`id`)
);
ALTER TABLE `remocoes_historico_acoes`
MODIFY COLUMN `id` int(11) NOT NULL AUTO_INCREMENT FIRST;

ALTER TABLE `remocoes`
ADD COLUMN `edited_by` int(11) NULL AFTER `justificativa_nao_cobrar`,
ADD COLUMN `edited_at` datetime NULL AFTER `edited_by`;