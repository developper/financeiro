
-- Criando novos topicos.

INSERT INTO orcamento_financeiro_topicos (id, descricao) VALUES
	 (99, 'Fluxo Caixa Investimento'),
	 (100, 'Captação Empréstimos'),
	 (101,'Juros Empréstimo'),
	 (102,'Parcela Empréstimo'),
	 (103, 'Parcelamento Tributário');
     INSERT INTO orcamento_financeiro_estrutura (versao_estrutura,orcamento_finananceiro_topicos_id,ordem,id_pai,aliquota) VALUES
	 (2,99,17,0,0.00),
	 (2,100,1,99,0.00),
	 (2,101,2,99,0.00),
	 (2,102,3,99,0.00),
	 (2,103,4,99,0.00);
INSERT INTO orcamento_financeiro_topicos_valores (orcamento_financeiro_topicos,orcamento_financeiro_versao_id,valor_mensal,criado_em,criado_por,editado_em,editado_por,deletado_em,deletado_por,`data`,aliquota) VALUES
	 (99,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-01',NULL),
	 (99,11,0,NULL,NULL,NULL,18,NULL,NULL,'2021-02',NULL),
	 (99,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-03',NULL),
	 (99,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-04',NULL),
	 (99,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-05',NULL),
	 (99,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-06',NULL),
	 (99,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-07',NULL),
	 (99,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-08',NULL),
	 (99,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-09',NULL),
	 (99,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-10',NULL);
INSERT INTO orcamento_financeiro_topicos_valores (orcamento_financeiro_topicos,orcamento_financeiro_versao_id,valor_mensal,criado_em,criado_por,editado_em,editado_por,deletado_em,deletado_por,`data`,aliquota) VALUES
	 (99,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-11',NULL),
	 (99,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-12',NULL);
     INSERT INTO orcamento_financeiro_topicos_valores (orcamento_financeiro_topicos,orcamento_financeiro_versao_id,valor_mensal,criado_em,criado_por,editado_em,editado_por,deletado_em,deletado_por,`data`,aliquota) VALUES
	 (100,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-01',NULL),
	 (100,11,0,NULL,NULL,NULL,18,NULL,NULL,'2021-02',NULL),
	 (100,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-03',NULL),
	 (100,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-04',NULL),
	 (100,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-05',NULL),
	 (100,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-06',NULL),
	 (100,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-07',NULL),
	 (100,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-08',NULL),
	 (100,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-09',NULL),
	 (100,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-10',NULL);
INSERT INTO orcamento_financeiro_topicos_valores (orcamento_financeiro_topicos,orcamento_financeiro_versao_id,valor_mensal,criado_em,criado_por,editado_em,editado_por,deletado_em,deletado_por,`data`,aliquota) VALUES
	 (100,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-11',NULL),
	 (100,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-12',NULL);
     INSERT INTO orcamento_financeiro_topicos_valores (orcamento_financeiro_topicos,orcamento_financeiro_versao_id,valor_mensal,criado_em,criado_por,editado_em,editado_por,deletado_em,deletado_por,`data`,aliquota) VALUES
	 (101,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-01',NULL),
	 (101,11,0,NULL,NULL,NULL,18,NULL,NULL,'2021-02',NULL),
	 (101,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-03',NULL),
	 (101,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-04',NULL),
	 (101,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-05',NULL),
	 (101,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-06',NULL),
	 (101,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-07',NULL),
	 (101,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-08',NULL),
	 (101,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-09',NULL),
	 (101,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-10',NULL);
INSERT INTO orcamento_financeiro_topicos_valores (orcamento_financeiro_topicos,orcamento_financeiro_versao_id,valor_mensal,criado_em,criado_por,editado_em,editado_por,deletado_em,deletado_por,`data`,aliquota) VALUES
	 (101,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-11',NULL),
	 (101,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-12',NULL);
     INSERT INTO orcamento_financeiro_topicos_valores (orcamento_financeiro_topicos,orcamento_financeiro_versao_id,valor_mensal,criado_em,criado_por,editado_em,editado_por,deletado_em,deletado_por,`data`,aliquota) VALUES
	 (102,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-01',NULL),
	 (102,11,0,NULL,NULL,NULL,18,NULL,NULL,'2021-02',NULL),
	 (102,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-03',NULL),
	 (102,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-04',NULL),
	 (102,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-05',NULL),
	 (102,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-06',NULL),
	 (102,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-07',NULL),
	 (102,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-08',NULL),
	 (102,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-09',NULL),
	 (102,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-10',NULL);
INSERT INTO orcamento_financeiro_topicos_valores (orcamento_financeiro_topicos,orcamento_financeiro_versao_id,valor_mensal,criado_em,criado_por,editado_em,editado_por,deletado_em,deletado_por,`data`,aliquota) VALUES
	 (102,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-11',NULL),
	 (102,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-12',NULL);
     INSERT INTO orcamento_financeiro_topicos_valores (orcamento_financeiro_topicos,orcamento_financeiro_versao_id,valor_mensal,criado_em,criado_por,editado_em,editado_por,deletado_em,deletado_por,`data`,aliquota) VALUES
	 (103,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-01',NULL),
	 (103,11,0,NULL,NULL,NULL,18,NULL,NULL,'2021-02',NULL),
	 (103,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-03',NULL),
	 (103,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-04',NULL),
	 (103,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-05',NULL),
	 (103,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-06',NULL),
	 (103,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-07',NULL),
	 (103,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-08',NULL),
	 (103,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-09',NULL),
	 (103,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-10',NULL);
INSERT INTO orcamento_financeiro_topicos_valores (orcamento_financeiro_topicos,orcamento_financeiro_versao_id,valor_mensal,criado_em,criado_por,editado_em,editado_por,deletado_em,deletado_por,`data`,aliquota) VALUES
	 (103,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-11',NULL),
	 (103,11,0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-12',NULL);