-- montar update em usuarios e dar acesso ao menu de faturamento
SELECT
usuarios.*
concat("update usuarios set modfat = 1 where idUsuarios =",  idUsuarios, ";") as updateModFat,
concat("insert into  usuarios_area (area_id, usuario_id) values (70, ",idUsuarios,");") as insertArea
FROM
usuarios
where
usuarios.empresa in (5, 4, 2)
and
usuarios.block_at = '9999-12-31'
and
tipo = 'enfermagem'

-- insert area do faturamento para aparecer no menu principal

insert into  usuarios_area (area_id, usuario_id) values (70, 63);
insert into  usuarios_area (area_id, usuario_id) values (70, 98);
insert into  usuarios_area (area_id, usuario_id) values (70, 99);
insert into  usuarios_area (area_id, usuario_id) values (70, 131);
insert into  usuarios_area (area_id, usuario_id) values (70, 134);
insert into  usuarios_area (area_id, usuario_id) values (70, 143);
insert into  usuarios_area (area_id, usuario_id) values (70, 144);
insert into  usuarios_area (area_id, usuario_id) values (70, 170);
insert into  usuarios_area (area_id, usuario_id) values (70, 246);
insert into  usuarios_area (area_id, usuario_id) values (70, 350);
insert into  usuarios_area (area_id, usuario_id) values (70, 391);
insert into  usuarios_area (area_id, usuario_id) values (70, 439);
insert into  usuarios_area (area_id, usuario_id) values (70, 471);
insert into  usuarios_area (area_id, usuario_id) values (70, 496);
insert into  usuarios_area (area_id, usuario_id) values (70, 534);
insert into  usuarios_area (area_id, usuario_id) values (70, 544);
insert into  usuarios_area (area_id, usuario_id) values (70, 573);
insert into  usuarios_area (area_id, usuario_id) values (70, 584);
insert into  usuarios_area (area_id, usuario_id) values (70, 586);

-- update usuarios dando acesso ao modulo de faturamento
update usuarios set modfat = 1 where idUsuarios =63;
update usuarios set modfat = 1 where idUsuarios =98;
update usuarios set modfat = 1 where idUsuarios =99;
update usuarios set modfat = 1 where idUsuarios =131;
update usuarios set modfat = 1 where idUsuarios =134;
update usuarios set modfat = 1 where idUsuarios =143;
update usuarios set modfat = 1 where idUsuarios =144;
update usuarios set modfat = 1 where idUsuarios =170;
update usuarios set modfat = 1 where idUsuarios =246;
update usuarios set modfat = 1 where idUsuarios =350;
update usuarios set modfat = 1 where idUsuarios =391;
update usuarios set modfat = 1 where idUsuarios =439;
update usuarios set modfat = 1 where idUsuarios =471;
update usuarios set modfat = 1 where idUsuarios =496;
update usuarios set modfat = 1 where idUsuarios =534;
update usuarios set modfat = 1 where idUsuarios =544;
update usuarios set modfat = 1 where idUsuarios =573;
update usuarios set modfat = 1 where idUsuarios =584;
update usuarios set modfat = 1 where idUsuarios =586;