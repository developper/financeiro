alter table fornecedores
    add recolhe_iss int default 0 not null after validado_receita;

alter table fornecedores
    add recolhe_ir int default 0 not null after aliquota_iss;

alter table fornecedores
    add recolhe_ipi int default 0 not null after aliquota_ir;

alter table fornecedores
    add aliquota_ipi decimal(10,2) default 0.00 not null after recolhe_ipi;

alter table fornecedores
    add recolhe_icms int default 0 not null after aliquota_ipi;

alter table fornecedores
    add aliquota_icms decimal(10,2) default 0.00 not null after recolhe_icms;

alter table fornecedores
    add recolhe_pis int default 0 not null after aliquota_icms;

alter table fornecedores
    add aliquota_pis decimal(10,2) default 0.00 not null after recolhe_pis;

alter table fornecedores
    add recolhe_cofins int default 0 not null after aliquota_pis;

alter table fornecedores
    add aliquota_cofins decimal(10,2) default 0.00 not null after recolhe_cofins;

alter table fornecedores
    add recolhe_csll int default 0 not null after aliquota_cofins;

alter table fornecedores
    add aliquota_csll decimal(10,2) default 0.00 not null after recolhe_csll;

alter table fornecedores
    add recolhe_inss int default 0 not null after aliquota_csll;

alter table fornecedores
    add aliquota_inss decimal(10,2) default 0.00 not null after recolhe_inss;

