CREATE TABLE `modalidade_home_care` (
`id`  int(11) NOT NULL ,
`modalidade`  varchar(70) NULL AUTO_INCREMENT FIRST ,
PRIMARY KEY (`id`)
);

INSERT INTO `modalidade_home_care` (`modalidade`) VALUES ('Assistência Domiciliar');
INSERT INTO `modalidade_home_care` (`modalidade`) VALUES ('Internação Domiciliar 6h');
INSERT INTO `modalidade_home_care` (`modalidade`) VALUES ('Internação Domiciliar 12h');
INSERT INTO `modalidade_home_care` (`modalidade`) VALUES ('Internação Domiciliar 24h Com Respirador');
INSERT INTO `modalidade_home_care` (`modalidade`) VALUES ('Internação Domiciliar 24h Sem Respirador');