UPDATE `prescricao_enf_cuidado` SET `item`='Fazer higiene íntima com água e sabão após eliminações veisco-intestinais e usar locação dermoprotetor' WHERE (`id`='16');
UPDATE `prescricao_enf_cuidado` SET `item`='Realizar higiene oral e limpeza dos dentes sempre após as refeições' WHERE (`id`='19');
UPDATE `prescricao_enf_cuidado` SET `item`='Realizar limpeza do óstio da GTT com água e sabão, quando ausẽncia de lesão' WHERE (`id`='24');
UPDATE `prescricao_enf_cuidado` SET `item`='Realizar limpeza da traqueostomia metálica movel 3x/dia e sempre que SN com aǵua e sabão' WHERE (`id`='27');
UPDATE `prescricao_enf_cuidado` SET `item`='Manter sempre a cabeceira elevada a 45º para alimentação' WHERE (`id`='40');
UPDATE `prescricao_enf_cuidado` SET `item`='Comunicar qualquer alteração clínica: taquicardia, dispnéia, febre, sangramentos, distenção abdominal, diarréia, queda de saturação' WHERE (`id`='58');
UPDATE `prescricao_enf_cuidado` SET `item`='Atentar para sinais de hipoglicemia (sudorese intensa, fadiga, tontura, taquicardia, visaõ turva, convulsão)' WHERE (`id`='109');
UPDATE `prescricao_enf_cuidado` SET `item`='Atentar para sinais de hiperglicemia (agitação, diurese, fome e sede aumentada, fadiga)' WHERE (`id`='110');

ALTER TABLE `prescricao_enf_cuidado`
ADD COLUMN `ativo`  int(1) NULL DEFAULT 1 COMMENT 'recebe 1 para ativo e 0 para inativo' AFTER `cuidado_especiais_id`;
