-- Alterando valores notas
ALTER TABLE `notas` 
MODIFY COLUMN `valor` decimal(12, 2) NULL DEFAULT NULL AFTER `codCentroCusto`,
MODIFY COLUMN `ICMS` decimal(12, 2) NOT NULL AFTER `empresa`,
MODIFY COLUMN `FRETE` decimal(12, 2) NOT NULL AFTER `ICMS`,
MODIFY COLUMN `SEGURO` decimal(12, 2) NOT NULL AFTER `FRETE`,
MODIFY COLUMN `VAL_PRODUTOS` decimal(12, 2) NOT NULL AFTER `SEGURO`,
MODIFY COLUMN `DES_ACESSORIAS` decimal(12, 2) NOT NULL AFTER `VAL_PRODUTOS`,
MODIFY COLUMN `IPI` decimal(12, 2) NOT NULL AFTER `DES_ACESSORIAS`,
MODIFY COLUMN `ISSQN` decimal(12, 2) NOT NULL AFTER `IPI`,
MODIFY COLUMN `DESCONTO_BONIFICACAO` decimal(12, 2) NOT NULL AFTER `ISSQN`,
MODIFY COLUMN `PIS` decimal(12, 2) NOT NULL AFTER `TIPO_DOCUMENTO`,
MODIFY COLUMN `COFINS` decimal(12, 2) NOT NULL AFTER `PIS`,
MODIFY COLUMN `CSLL` decimal(12, 2) NOT NULL AFTER `COFINS`,
MODIFY COLUMN `IR` decimal(12, 2) NOT NULL AFTER `CSLL`,
MODIFY COLUMN `PCC` decimal(12, 2) NOT NULL AFTER `DATA`,
MODIFY COLUMN `INSS` decimal(12, 2) NOT NULL AFTER `PCC`;

-- Alterando parcelas
ALTER TABLE `parcelas` 
MODIFY COLUMN `valor` decimal(12, 2) NOT NULL AFTER `idNota`,
MODIFY COLUMN `JurosMulta` decimal(12, 2) NOT NULL DEFAULT 0.00 AFTER `empresa`,
MODIFY COLUMN `desconto` decimal(12, 2) NOT NULL AFTER `JurosMulta`,
MODIFY COLUMN `encargos` decimal(12, 2) NOT NULL AFTER `desconto`,
MODIFY COLUMN `VALOR_PAGO` decimal(12, 2) NOT NULL AFTER `TR`,
MODIFY COLUMN `OUTROS_ACRESCIMOS` decimal(12, 2) NOT NULL DEFAULT 0 AFTER `DATA_PAGAMENTO`,
MODIFY COLUMN `ISSQN` decimal(12, 2) NOT NULL DEFAULT 0 AFTER `NUM_DOCUMENTO`,
MODIFY COLUMN `PIS` decimal(12, 2) NOT NULL DEFAULT 0 AFTER `ISSQN`,
MODIFY COLUMN `COFINS` decimal(12, 2) NOT NULL DEFAULT 0 AFTER `PIS`,
MODIFY COLUMN `CSLL` decimal(12, 2) NOT NULL DEFAULT 0 AFTER `COFINS`,
MODIFY COLUMN `IR` decimal(12, 2) NOT NULL DEFAULT 0 AFTER `CSLL`;

-- Alterando rateio
ALTER TABLE `rateio` 
MODIFY COLUMN `VALOR` decimal(12, 2) NOT NULL AFTER `CENTRO_RESULTADO`;

-- Alterando transferencia_bancaria
ALTER TABLE `transferencia_bancaria` 
MODIFY COLUMN `VALOR` decimal(12, 2) NOT NULL AFTER `CONTA_DESTINO`;

-- Alterando contas_bancarias
ALTER TABLE `contas_bancarias` 
MODIFY COLUMN `SALDO` decimal(12, 2) NOT NULL AFTER `UR`,
MODIFY COLUMN `SALDO_NCONCILIADO` decimal(12, 2) NOT NULL AFTER `DATA_ATUALIZACAO_SALDO`,
MODIFY COLUMN `SALDO_INICIAL` decimal(12, 2) NOT NULL AFTER `SALDO_NCONCILIADO`;

-- Alterando impostos_tarifas_lancto
ALTER TABLE `impostos_tarifas_lancto` 
MODIFY COLUMN `valor` double(12, 2) NOT NULL AFTER `data`;
