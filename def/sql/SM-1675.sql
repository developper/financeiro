ALTER TABLE `encaminhamento_medico`
ADD COLUMN `contrato_encaminhamento_id`  int(11) NULL AFTER `created_by`,
ADD COLUMN `tipo`  enum('externo','interno') NULL AFTER `contarto_encaminhamento_id`,
ADD COLUMN `nome_paciente`  varchar(255) NULL AFTER `tipo`;

update
encaminhamento_medico
SET
tipo = 'interno'
WHERE
tipo is null