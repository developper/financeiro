ALTER TABLE `avaliacaoenfermagem`
ADD COLUMN `MODELO_NEAD`  varchar(255) NOT NULL DEFAULT '2016' AFTER `nutricao_multidisciplinar`,
ADD COLUMN `CLASSIFICACAO_NEAD`  int(11) NULL AFTER `MODELO_NEAD`;

ALTER TABLE `avaliacaoenfermagem`
MODIFY COLUMN `medico_multidisciplinar`  enum('3','2','1','N','S','-1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '-1' COMMENT '-1 Selecine; 1- smenal, 2 quizenal, 3 mensal, S-sim, N-não' AFTER `DISPO_INTRAVENOSO_CATETER_PICC_LOCAL`,
MODIFY COLUMN `enfermagem_multidisciplinar`  enum('4','3','2','1','N','S','-1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '-1' COMMENT '-1 selecione; 1 semanal; 2 Quinzenal; 3 mensal; 4 2x semana, S-sim, N-não' AFTER `medico_multidisciplinar`;
