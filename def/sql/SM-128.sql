-- O problem a foi resolvido realizando update na tabela de itens_prescrição no campo de descrição. 
-- O seguinte scripite SQL foi executado:

/* 16:30:23 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Dieta:</b> Conforme prescrição própria   6 vezes ao dia Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128219';
/* 16:31:24 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Cuidados Especiais:</b> Visita Enfermeiro Assistente   1 vez por semana Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128220';
/* 16:31:48 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Cuidados Especiais:</b> Consulta Médico Assistente   1 vez por semana Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128221';
/* 16:32:02 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Cuidados Especiais:</b> Sessão de Fisioterapia Motora   1 vez ao dia Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128222';
/* 16:32:10 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Cuidados Especiais:</b> Registro de Sinais Vitais   6/6h Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128223';
/* 16:32:16 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Cuidados Especiais:</b> Glicemia Capilar   1 vez por semana Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128224';
/* 16:32:29 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Cuidados Especiais:</b> Sessão de Fonoaudiologia  1 vez ao dia Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128225';
/* 16:32:38 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Cuidados Especiais:</b> Assistência Técnico de Enfermagem 24h sem Respirador   Contínuo Período:01/05/2014 até 31/05/2014' WHERE `id` = '128226';
/* 16:32:46 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Cuidados Especiais:</b> Sessão de Fisioterapia Respiratória   1 vez ao dia Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128227';
/* 16:32:52 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Cuidados Especiais:</b> Consulta Nutricionista   1 vez cada 15 dias Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128228';
/* 16:33:04 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Drogas Orais:</b> LAMITOR 100 mg. cprs.. 1 comprimido 1 vez ao dia VO. Período: 01/05/2014 até 31/05/2014 OBS: A noite' WHERE `id` = '128229';
/* 16:33:15 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Drogas Orais/Enterais:</b> DIPIRONA - GENERICO 500 mg/ml 20 ml  VSNE 40 gotas SN Período:  01/05/2014 até 31/05/2014' WHERE `id` = '128230';
/* 16:33:21 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Drogas Orais/Enterais:</b> NEXIUM 20 mg. cprs. rev. VO 01 comprimido 1 vez ao dia Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128231';
/* 16:33:27 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Drogas Orais/Enterais:</b> Novanlo comp 5 mg VO 01 comprimido 1 vez ao dia Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128232';
/* 16:33:33 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Drogas Orais/Enterais:</b> ESPRAN 10 mg. cprs. VO 1 comprimido 1 vez ao dia Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128233';
/* 16:33:40 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Drogas Orais/Enterais:</b> Xarelto 20 mg  cprs. VO 01 comprimido 1 vez ao dia Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128234';
/* 16:33:49 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Drogas Orais/Enterais:</b> MICARDIS HCT 40 mg + 12,5 mg ct. 2 bl. x 7 cprs. VO 01 comprimido 1 vez ao dia Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128235';
/* 16:33:57 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Drogas Orais/Enterais:</b> TYLEX 30 mg VO 01 comprimido ACM Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128236';
/* 16:34:14 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Drogas Orais/Enterais:</b> SELOZOK 50 mg. cprs. lib. control VO 01 comprimido 2 vezes ao dia Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128237';
/* 16:34:23 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Drogas Orais/Enterais:</b> ATORVASTATINA- GENERICO 80 mg.  cprs. VO 01 comprimido 1 vez ao dia Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128238';
/* 16:34:29 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Drogas Orais/Enterais:</b> LAMICTAL Disp. 200 mg. x 30 cprs. VO 1 comprimido 2 vezes ao dia Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128239';
/* 16:34:35 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Drogas Tópicas:</b> AGE DERM - OILGEL  100 g tb  Topico 01 Aplicação 3 vezes ao dia Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128240';
/* 16:34:43 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Drogas Tópicas:</b> DERMODEX 60 g bisnaga Topico 01 Aplicação 2 vezes ao dia Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128241';
/* 16:34:54 Mederi */ UPDATE `itens_prescricao` SET `descricao` = '<b>Oxigenoterapia:</b> Cateter Nasal Tipo Óculos. 3l/min  3 L/min SN Período: 01/05/2014 até 31/05/2014' WHERE `id` = '128242';
