-- idFornecedores <> 21 fornecedor teste.
-- Fornecedres que deram entrada no estoque de 01/09/2015 at� 01/09/2016.

SELECT
fornecedores.idFornecedores,
fornecedores.nome,
fornecedores.razaoSocial,
fornecedores.cnpj,
fornecedores.telefone,
fornecedores.contato
FROM
entrada
INNER JOIN fornecedores ON entrada.fornecedor = fornecedores.idFornecedores
WHERE
entrada.`data` BETWEEN '2015-09-01' AND '2016-09-01'
and fornecedores.idFornecedores <> 21
and fornecedores.TIPO = 'F'
GROUP BY
entrada.fornecedor

---------- ----

-- Fornecedores que tiveram notas lan�adas como desembolso de 01/09/2015 at� 01/09/2016.

SELECT
fornecedores.idFornecedores,
fornecedores.nome,
fornecedores.razaoSocial,
fornecedores.cnpj,
fornecedores.contato,
fornecedores.telefone
FROM
notas
INNER JOIN fornecedores ON notas.codFornecedor = fornecedores.idFornecedores
WHERE
notas.dataEntrada BETWEEN '2015-09-01' AND '2016-09-01'
AND
fornecedores.TIPO = 'F'
AND
fornecedores.idFornecedores <> 21
AND
notas.tipo = 1
GROUP BY
notas.codFornecedor