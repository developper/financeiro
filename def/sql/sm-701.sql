CREATE TABLE `plano_contas_antigo`(
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `N1` char(1) DEFAULT NULL,
  `COD_N2` varchar(5) NOT NULL,
  `N2` varchar(200) DEFAULT NULL,
  `COD_N3` varchar(5) NOT NULL,
  `N3` varchar(200) DEFAULT NULL,
  `COD_N4` varchar(10) NOT NULL,
  `N4` varchar(200) DEFAULT NULL,
  `COD_IPCC` varchar(15) DEFAULT NULL,
  `IPCC` varchar(200) DEFAULT NULL,
  `PROVISIONA` enum('n','s') NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=352 DEFAULT CHARSET=utf8;

INSERT INTO `plano_contas_antigo` SELECT * FROM `plano_contas`;

DELETE FROM plano_contas WHERE ID < 154;