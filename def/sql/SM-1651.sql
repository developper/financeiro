ALTER TABLE `fatura`
ADD COLUMN `prescricao_id`  int(11) NULL AFTER `remocao`;
ALTER TABLE `fatura`
ADD COLUMN `tag_prescricao`  varchar(255) NULL COMMENT 'Indica a origem da prescrição.' AFTER `prescricao_id`;