INSERT INTO `score` (`ID`, `DESCRICAO`) VALUES ('4', 'NEAD7');

INSERT INTO `grupos` 
(`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES 
('29', 'QUADRO CLÍNICO', '2', '4'), 
('30', 'ASPIRAÇÕES TRAQUEAIS', '4', '4'), 
('31', 'SONDAS/DRENOS/CATÉTERES/ESTOMIAS', '3', '4'), 
('32', 'PROCEDIMENTOS TÉCNICOS INVASIVOS', '6', '4'), 
('33', 'PADRÃO RESPIRATÓRIO', '4', '4'),
('34', 'DEPENDENCIA DE O²', '6', '4'), 
('35', 'CURATIVOS', '4', '4');

INSERT INTO `score_item` 
(`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES 
('132', '29', '4', 'Estável', '0'), 
('133', '29', '4', 'Não Estável', '2'),
('134', '30', '4', 'Ausentes', '0'), 
('135', '30', '4', 'Até três aspirações', '1'), 
('136', '30', '4', 'Três a seis aspirações', '2'),
('137', '30', '4', 'Mais de seis aspirações', '4'),
('138', '31', '4', 'Ausentes', '0'),
('139', '31', '4', 'Presença com família apta', '1'),
('140', '31', '4', 'Presença sem família apta', '2'),
('141', '32', '4', 'Ausentes', '0'),
('142', '32', '4', '1x dia', '1'),
('142', '32', '4', '2x dia', '2'),
('142', '32', '4', '3x dia', '3'),
('142', '32', '4', '4x dia', '4'),
('146', '32', '4', 'Mais de 4x ao dia', '5'),
('147', '33', '4', 'Eupnéico', '0'),
('147', '33', '4', 'Períodos de Dispnéia', '1'),
('149', '33', '4', 'Dispnéia Constante', '2'),
('150', '33', '4', 'Períodos de Apnéia', '3'),
('151', '34', '4', 'Ausentes', '0'),
('152', '34', '4', 'Parcial', '1'),
('153', '34', '4', 'Contínua', '2'),
('154', '34', '4', 'Ventilação não invasiva', '3'),
('155', '34', '4', 'Ventilação invasiva intermitente', '4'),
('156', '34', '4', 'Ventilação invasiva contínua', '5'),
('157', '35', '4', 'Ausentes ou simples', '0'),
('158', '35', '4', 'Pequenos', '1'),
('159', '35', '4', 'Médios', '2'),
('160', '35', '4', 'Grandes/múltiplos', '3');



