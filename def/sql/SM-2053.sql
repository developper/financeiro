ALTER TABLE `ficha_admissao_paciente_secmed` 
ADD COLUMN `PLANO_ID` int(11) NULL AFTER `FREQUENCIA_VISITA_NUTRICIONISTA`,
ADD COLUMN `UR` int(11) NULL AFTER `PLANO_ID`
ADD COLUMN `CANCELED_BY` int(11) NULL AFTER `UR`,
ADD COLUMN `CANCELED_AT` datetime NULL AFTER `CANCELED_BY`;

