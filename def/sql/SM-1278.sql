-- Relatório de Antibioticos prescritos
SELECT
  empresas.nome AS ur,
  clientes.nome AS paciente,
  usuarios.nome AS medico,
  concat(catalogo.principio,' ', catalogo.apresentacao) AS item,
  catalogo.lab_desc AS Lab,
  DATE_FORMAT(prescricoes.inicio, '%d/%m/%Y') AS inicio,
  DATE_FORMAT(prescricoes.fim, '%d/%m/%Y') AS fim
FROM
  itens_prescricao
  INNER JOIN catalogo ON itens_prescricao.CATALOGO_ID = catalogo.ID
  INNER JOIN prescricoes ON prescricoes.id = itens_prescricao.idPrescricao
  INNER JOIN clientes ON prescricoes.paciente = clientes.idClientes
  INNER JOIN empresas ON clientes.empresa = empresas.id
  INNER JOIN usuarios ON prescricoes.criador = usuarios.idUsuarios
WHERE
  prescricoes.inicio BETWEEN '2016-01-01' AND '2016-06-30'
  AND prescricoes.fim BETWEEN '2016-01-01' AND '2016-06-30'
  AND prescricoes.paciente NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
  AND (
    catalogo.ANTIBIOTICO = 'S'
    OR itens_prescricao.tipo = '4'
  )
GROUP BY
  itens_prescricao.id
ORDER BY
  ur, paciente, inicio;

-- Relatório de Feridas que constam com a marcação cicatrizada

SELECT
  empresas.nome AS ur,
  clientes.nome AS paciente,
  usuarios.nome AS enfermeiro,
  DATE_FORMAT(evolucaoenfermagem.DATA_EVOLUCAO, '%d/%m/%Y') AS dataEvolucao,
  quadro_feridas_enf_local.LOCAL,
  COALESCE(
      CASE
      WHEN quadro_feridas_enfermagem.LADO = '1' THEN 'Direito'
      WHEN quadro_feridas_enfermagem.LADO = '2' THEN 'Esquerdo'
      END,
      '-'
  ) AS lado,
  quadro_feridas_enf_tipo.TIPO,
  quadro_feridas_enf_tecido.TECIDO,
  quadro_feridas_enf_lesao.TIPO AS cobertura,
  quadro_feridas_enf_tamanho.TAMANHO,
  quadro_feridas_enf_profundidade.PROFUNDIDADE,
  quadro_feridas_enf_grau.GRAU,
  quadro_feridas_enf_humidade.HUMIDADE,
  quadro_feridas_enf_exsudato.EXSUDATO,
  quadro_feridas_enf_tempo.TEMPO,
  quadro_feridas_enf_periodo.PERIODO_TROCA,
  quadro_feridas_enf_pele.PELE,
  quadro_feridas_enf_contaminacao.CONTAMINACAO,
  IF(quadro_feridas_enfermagem.ODOR = '1', 'SEM ODOR', 'FÉTIDO') AS odor
FROM
  quadro_feridas_enfermagem
  INNER JOIN quadro_feridas_enf_local ON quadro_feridas_enfermagem.LOCAL = quadro_feridas_enf_local.ID
  LEFT JOIN quadro_feridas_enf_tipo ON quadro_feridas_enfermagem.TIPO = quadro_feridas_enf_tipo.ID
  LEFT JOIN quadro_feridas_enf_tecido ON quadro_feridas_enfermagem.TECIDO = quadro_feridas_enf_tecido.ID
  LEFT JOIN quadro_feridas_enf_tamanho ON quadro_feridas_enfermagem.TAMANHO = quadro_feridas_enf_tamanho.ID
  LEFT JOIN quadro_feridas_enf_profundidade ON quadro_feridas_enfermagem.PROFUNDIDADE = quadro_feridas_enf_profundidade.ID
  LEFT JOIN quadro_feridas_enf_grau ON quadro_feridas_enfermagem.GRAU = quadro_feridas_enf_grau.ID
  LEFT JOIN quadro_feridas_enf_humidade ON quadro_feridas_enfermagem.HUMIDADE = quadro_feridas_enf_humidade.ID
  LEFT JOIN quadro_feridas_enf_exsudato ON quadro_feridas_enfermagem.EXSUDATO = quadro_feridas_enf_exsudato.ID
  LEFT JOIN quadro_feridas_enf_tempo ON quadro_feridas_enfermagem.TEMPO = quadro_feridas_enf_tempo.ID
  LEFT JOIN quadro_feridas_enf_periodo ON quadro_feridas_enfermagem.PERIODO = quadro_feridas_enf_periodo.ID
  LEFT JOIN quadro_feridas_enf_status ON quadro_feridas_enfermagem.STATUS_FERIDA = quadro_feridas_enf_status.ID
  LEFT JOIN quadro_feridas_enf_lesao ON quadro_feridas_enfermagem.COBERTURA = quadro_feridas_enf_lesao.ID
  LEFT JOIN quadro_feridas_enf_pele ON quadro_feridas_enfermagem.PELE = quadro_feridas_enf_pele.ID
  LEFT JOIN quadro_feridas_enf_contaminacao ON quadro_feridas_enfermagem.CONTAMINACAO = quadro_feridas_enf_contaminacao.ID
  INNER JOIN evolucaoenfermagem ON quadro_feridas_enfermagem.EVOLUCAO_ENF_ID = evolucaoenfermagem.ID
  INNER JOIN clientes ON evolucaoenfermagem.PACIENTE_ID = clientes.idClientes
  INNER JOIN empresas ON clientes.empresa = empresas.id
  INNER JOIN usuarios ON evolucaoenfermagem.USUARIO_ID = usuarios.idUsuarios
WHERE
  evolucaoenfermagem.DATA_EVOLUCAO BETWEEN '2016-01-01' AND '2016-06-30'
  AND evolucaoenfermagem.PACIENTE_ID NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
  AND quadro_feridas_enfermagem.STATUS_FERIDA = 2
ORDER BY
  ur, paciente, dataEvolucao;

-- Relatório de Feridas que constam a marcação "nova ferida" após paciente ativo

SELECT
  empresas.nome AS ur,
  clientes.nome AS paciente,
  usuarios.nome AS enfermeiro,
  DATE_FORMAT(evolucaoenfermagem.DATA_EVOLUCAO, '%d/%m/%Y') AS dataEvolucao,
  quadro_feridas_enf_local.LOCAL,
  COALESCE(
      CASE
      WHEN quadro_feridas_enfermagem.LADO = '1' THEN 'Direito'
      WHEN quadro_feridas_enfermagem.LADO = '2' THEN 'Esquerdo'
      END,
      '-'
  ) AS lado,
  quadro_feridas_enf_tipo.TIPO,
  quadro_feridas_enf_tecido.TECIDO,
  quadro_feridas_enf_lesao.TIPO AS cobertura,
  quadro_feridas_enf_tamanho.TAMANHO,
  quadro_feridas_enf_profundidade.PROFUNDIDADE,
  quadro_feridas_enf_grau.GRAU,
  quadro_feridas_enf_humidade.HUMIDADE,
  quadro_feridas_enf_exsudato.EXSUDATO,
  quadro_feridas_enf_tempo.TEMPO,
  quadro_feridas_enf_periodo.PERIODO_TROCA,
  quadro_feridas_enf_pele.PELE,
  quadro_feridas_enf_contaminacao.CONTAMINACAO,
  IF(quadro_feridas_enfermagem.ODOR = '1', 'SEM ODOR', 'FÉTIDO') AS odor
FROM
  quadro_feridas_enfermagem
  INNER JOIN quadro_feridas_enf_local ON quadro_feridas_enfermagem.LOCAL = quadro_feridas_enf_local.ID
  LEFT JOIN quadro_feridas_enf_tipo ON quadro_feridas_enfermagem.TIPO = quadro_feridas_enf_tipo.ID
  LEFT JOIN quadro_feridas_enf_tecido ON quadro_feridas_enfermagem.TECIDO = quadro_feridas_enf_tecido.ID
  LEFT JOIN quadro_feridas_enf_tamanho ON quadro_feridas_enfermagem.TAMANHO = quadro_feridas_enf_tamanho.ID
  LEFT JOIN quadro_feridas_enf_profundidade ON quadro_feridas_enfermagem.PROFUNDIDADE = quadro_feridas_enf_profundidade.ID
  LEFT JOIN quadro_feridas_enf_grau ON quadro_feridas_enfermagem.GRAU = quadro_feridas_enf_grau.ID
  LEFT JOIN quadro_feridas_enf_humidade ON quadro_feridas_enfermagem.HUMIDADE = quadro_feridas_enf_humidade.ID
  LEFT JOIN quadro_feridas_enf_exsudato ON quadro_feridas_enfermagem.EXSUDATO = quadro_feridas_enf_exsudato.ID
  LEFT JOIN quadro_feridas_enf_tempo ON quadro_feridas_enfermagem.TEMPO = quadro_feridas_enf_tempo.ID
  LEFT JOIN quadro_feridas_enf_periodo ON quadro_feridas_enfermagem.PERIODO = quadro_feridas_enf_periodo.ID
  LEFT JOIN quadro_feridas_enf_status ON quadro_feridas_enfermagem.STATUS_FERIDA = quadro_feridas_enf_status.ID
  LEFT JOIN quadro_feridas_enf_lesao ON quadro_feridas_enfermagem.COBERTURA = quadro_feridas_enf_lesao.ID
  LEFT JOIN quadro_feridas_enf_pele ON quadro_feridas_enfermagem.PELE = quadro_feridas_enf_pele.ID
  LEFT JOIN quadro_feridas_enf_contaminacao ON quadro_feridas_enfermagem.CONTAMINACAO = quadro_feridas_enf_contaminacao.ID
  INNER JOIN evolucaoenfermagem ON quadro_feridas_enfermagem.EVOLUCAO_ENF_ID = evolucaoenfermagem.ID
  INNER JOIN clientes ON evolucaoenfermagem.PACIENTE_ID = clientes.idClientes
  INNER JOIN empresas ON clientes.empresa = empresas.id
  INNER JOIN usuarios ON evolucaoenfermagem.USUARIO_ID = usuarios.idUsuarios
WHERE
  evolucaoenfermagem.DATA_EVOLUCAO BETWEEN '2016-01-01' AND '2016-06-30'
  AND evolucaoenfermagem.PACIENTE_ID NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
  AND quadro_feridas_enfermagem.STATUS_FERIDA = 1
ORDER BY
  ur, paciente, dataEvolucao;

-- Relatório de Altas e Óbitos

SELECT
  status,
  COUNT(*)
FROM
  (
    SELECT
      c1.idClientes,
      COALESCE(
          st1.status,
          (
            SELECT st2.status
            FROM
              clientes AS c2
              INNER JOIN statuspaciente AS st2 ON c2.status = st2.id
            WHERE
              c2.idClientes = c1.idClientes
          )
      ) AS status
    FROM
      clientes AS c1
      LEFT JOIN historico_status_paciente AS hsp1 ON c1.idClientes = hsp1.PACIENTE_ID
      INNER JOIN statuspaciente AS st1 ON hsp1.STATUS_PACIENTE_ID = st1.id
    WHERE
      hsp1.DATA BETWEEN '2016-01-01 00:00:01' AND '2016-06-30 23:59:59'
      AND hsp1.PACIENTE_ID IN
          (4, 8, 10, 14, 24, 35, 41, 45, 48, 53, 58, 84, 94, 95, 96, 99, 109, 111, 118, 139, 144, 150, 151, 158, 161, 168, 179, 188, 198, 209, 228, 230, 233, 234, 237, 249, 255, 262, 267, 271, 276, 287, 292, 296, 298, 301, 315, 317, 324, 345, 360, 377, 382, 385, 388, 390, 407, 408, 412, 436, 443, 446, 453, 457, 465, 469, 479, 481, 496, 498, 509, 511, 515, 516, 528, 536, 546, 549, 558, 587, 593, 621, 623, 626, 627, 692, 706, 711, 732, 734, 740, 748, 763, 781, 783, 793, 797, 807, 812, 816, 823, 828, 834, 842, 866, 874, 876, 879, 882, 885, 886, 890, 896, 901, 904, 908, 916, 927, 928, 933, 940, 1025, 1027, 1030, 1031, 1038, 1048, 1054, 1056, 1057, 1065, 1077, 1080, 1081, 1082, 1083, 1084, 1085, 1087, 1089, 1090, 1091, 1104, 1107, 1118, 1120, 1124, 1125, 1126, 1132, 1144, 1145, 1147, 1151, 1154, 1157, 1158, 1165, 1168, 1169, 1170, 1172, 1173, 1174, 1175, 1176, 1177, 1178, 1179, 1184, 1185, 1187, 1188, 1191, 1201, 1205, 1208, 1209, 1214, 1215, 1216, 1218, 1219, 1225, 1236, 1239, 1241, 1247, 1248, 1249, 1250, 1253, 1264, 1265, 1268, 1269, 1273, 1274, 1275, 1276, 1277, 1278, 1281, 1282, 1284, 1286, 1288, 1290, 1291, 1295, 1296, 1299, 1300, 1301, 1302, 1303, 1304, 1307, 1309, 1310, 1313, 1315, 1318, 1320, 1325, 1330, 1332, 1335, 1336, 1343, 1345, 1347, 1350, 1351, 1352, 1354, 1355, 1358, 1359, 1361, 1365, 1369, 1373, 1374, 1377, 1379, 1385, 1389, 1392, 378, 383, 648, 688, 817, 1041, 1167, 1204, 1226, 1234, 1260, 1261, 1263, 1266, 1292, 1294, 1297, 1298, 1308, 1312, 1314, 1316, 1317, 1319, 1321, 1323, 1326, 1327, 1329, 1331, 1333, 1334, 1337, 1338, 1339, 1340, 1341, 1342, 1344, 1348, 1353, 1356, 1357, 1360, 1362, 1363, 1364, 1366, 1367, 1368, 1370, 1371, 1372, 1375, 1376, 1380, 1381, 1382, 1383, 1384, 1387, 1388, 1391, 1393)
      AND hsp1.ID = (
        SELECT MAX(ID)
        FROM
          historico_status_paciente AS hsp2
        WHERE
          hsp2.PACIENTE_ID = hsp1.PACIENTE_ID
      )
      AND c1.empresa = 4
      AND hsp1.STATUS_PACIENTE_ID IN (5, 6, 7)
    GROUP BY
      hsp1.PACIENTE_ID

    UNION

    SELECT
      idClientes,
      statuspaciente.status
    FROM
      clientes
      INNER JOIN statuspaciente ON clientes.status = statuspaciente.id
    WHERE
      idClientes IN
      (4, 8, 10, 14, 24, 35, 41, 45, 48, 53, 58, 84, 94, 95, 96, 99, 109, 111, 118, 139, 144, 150, 151, 158, 161, 168, 179, 188, 198, 209, 228, 230, 233, 234, 237, 249, 255, 262, 267, 271, 276, 287, 292, 296, 298, 301, 315, 317, 324, 345, 360, 377, 382, 385, 388, 390, 407, 408, 412, 436, 443, 446, 453, 457, 465, 469, 479, 481, 496, 498, 509, 511, 515, 516, 528, 536, 546, 549, 558, 587, 593, 621, 623, 626, 627, 692, 706, 711, 732, 734, 740, 748, 763, 781, 783, 793, 797, 807, 812, 816, 823, 828, 834, 842, 866, 874, 876, 879, 882, 885, 886, 890, 896, 901, 904, 908, 916, 927, 928, 933, 940, 1025, 1027, 1030, 1031, 1038, 1048, 1054, 1056, 1057, 1065, 1077, 1080, 1081, 1082, 1083, 1084, 1085, 1087, 1089, 1090, 1091, 1104, 1107, 1118, 1120, 1124, 1125, 1126, 1132, 1144, 1145, 1147, 1151, 1154, 1157, 1158, 1165, 1168, 1169, 1170, 1172, 1173, 1174, 1175, 1176, 1177, 1178, 1179, 1184, 1185, 1187, 1188, 1191, 1201, 1205, 1208, 1209, 1214, 1215, 1216, 1218, 1219, 1225, 1236, 1239, 1241, 1247, 1248, 1249, 1250, 1253, 1264, 1265, 1268, 1269, 1273, 1274, 1275, 1276, 1277, 1278, 1281, 1282, 1284, 1286, 1288, 1290, 1291, 1295, 1296, 1299, 1300, 1301, 1302, 1303, 1304, 1307, 1309, 1310, 1313, 1315, 1318, 1320, 1325, 1330, 1332, 1335, 1336, 1343, 1345, 1347, 1350, 1351, 1352, 1354, 1355, 1358, 1359, 1361, 1365, 1369, 1373, 1374, 1377, 1379, 1385, 1389, 1392, 378, 383, 648, 688, 817, 1041, 1167, 1204, 1226, 1234, 1260, 1261, 1263, 1266, 1292, 1294, 1297, 1298, 1308, 1312, 1314, 1316, 1317, 1319, 1321, 1323, 1326, 1327, 1329, 1331, 1333, 1334, 1337, 1338, 1339, 1340, 1341, 1342, 1344, 1348, 1353, 1356, 1357, 1360, 1362, 1363, 1364, 1366, 1367, 1368, 1370, 1371, 1372, 1375, 1376, 1380, 1381, 1382, 1383, 1384, 1387, 1388, 1391, 1393)
      AND clientes.empresa = 4
      AND clientes.status IN (5, 6, 7)
    ORDER BY
      status
  ) AS result
GROUP BY
  status
ORDER BY
  status;

-- Relatório de Modalidades
SELECT
  modalidade,
  COUNT(*)
FROM
  (
    SELECT *
    FROM
      (
        SELECT
          c.idClientes AS paciente,
          mhc.modalidade,
          cm.data      AS data,
          'CAPMED'     AS source
        FROM
          clientes AS c
          INNER JOIN capmedica AS cm ON (cm.paciente = c.idClientes)
          INNER JOIN modalidade_home_care AS mhc ON (mhc.id = cm.MODALIDADE)
        WHERE
          cm.MODALIDADE != 0
          AND cm.paciente IN
              (4, 8, 10, 14, 24, 35, 41, 45, 48, 53, 58, 84, 94, 95, 96, 99, 109, 111, 118, 139, 144, 150, 151, 158, 161, 168, 179, 188, 198, 209, 228, 230, 233, 234, 237, 249, 255, 262, 267, 271, 276, 287, 292, 296, 298, 301, 315, 317, 324, 345, 360, 377, 382, 385, 388, 390, 407, 408, 412, 436, 443, 446, 453, 457, 465, 469, 479, 481, 496, 498, 509, 511, 515, 516, 528, 536, 546, 549, 558, 587, 593, 621, 623, 626, 627, 692, 706, 711, 732, 734, 740, 748, 763, 781, 783, 793, 797, 807, 812, 816, 823, 828, 834, 842, 866, 874, 876, 879, 882, 885, 886, 890, 896, 901, 904, 908, 916, 927, 928, 933, 940, 1025, 1027, 1030, 1031, 1038, 1048, 1054, 1056, 1057, 1065, 1077, 1080, 1081, 1082, 1083, 1084, 1085, 1087, 1089, 1090, 1091, 1104, 1107, 1118, 1120, 1124, 1125, 1126, 1132, 1144, 1145, 1147, 1151, 1154, 1157, 1158, 1165, 1168, 1169, 1170, 1172, 1173, 1174, 1175, 1176, 1177, 1178, 1179, 1184, 1185, 1187, 1188, 1191, 1201, 1205, 1208, 1209, 1214, 1215, 1216, 1218, 1219, 1225, 1236, 1239, 1241, 1247, 1248, 1249, 1250, 1253, 1264, 1265, 1268, 1269, 1273, 1274, 1275, 1276, 1277, 1278, 1281, 1282, 1284, 1286, 1288, 1290, 1291, 1295, 1296, 1299, 1300, 1301, 1302, 1303, 1304, 1307, 1309, 1310, 1313, 1315, 1318, 1320, 1325, 1330, 1332, 1335, 1336, 1343, 1345, 1347, 1350, 1351, 1352, 1354, 1355, 1358, 1359, 1361, 1365, 1369, 1373, 1374, 1377, 1379, 1385, 1389, 1392, 378, 383, 648, 688, 817, 1041, 1167, 1204, 1226, 1234, 1260, 1261, 1263, 1266, 1292, 1294, 1297, 1298, 1308, 1312, 1314, 1316, 1317, 1319, 1321, 1323, 1326, 1327, 1329, 1331, 1333, 1334, 1337, 1338, 1339, 1340, 1341, 1342, 1344, 1348, 1353, 1356, 1357, 1360, 1362, 1363, 1364, 1366, 1367, 1368, 1370, 1371, 1372, 1375, 1376, 1380, 1381, 1382, 1383, 1384, 1387, 1388, 1391, 1393)
          AND cm.data BETWEEN '2016-01-01 00:00:01' AND '2016-06-30 23:59:59'
          AND cm.id = (
            SELECT MAX(id)
            FROM
              capmedica AS cm2
            WHERE
              cm2.paciente = cm.paciente
          )
          AND c.empresa = 4
        GROUP BY
          cm.paciente
        UNION
        SELECT
          c.idClientes  AS paciente,
          mhc.modalidade,
          rpm.INICIO    AS data,
          'PRORROGACAO' AS source
        FROM
          clientes AS c
          INNER JOIN relatorio_prorrogacao_med AS rpm ON (
          rpm.PACIENTE_ID = c.idClientes
          )
          INNER JOIN modalidade_home_care AS mhc ON (mhc.id = rpm.MOD_HOME_CARE)
        WHERE
          MOD_HOME_CARE != 0
          AND rpm.PACIENTE_ID IN
              (4, 8, 10, 14, 24, 35, 41, 45, 48, 53, 58, 84, 94, 95, 96, 99, 109, 111, 118, 139, 144, 150, 151, 158, 161, 168, 179, 188, 198, 209, 228, 230, 233, 234, 237, 249, 255, 262, 267, 271, 276, 287, 292, 296, 298, 301, 315, 317, 324, 345, 360, 377, 382, 385, 388, 390, 407, 408, 412, 436, 443, 446, 453, 457, 465, 469, 479, 481, 496, 498, 509, 511, 515, 516, 528, 536, 546, 549, 558, 587, 593, 621, 623, 626, 627, 692, 706, 711, 732, 734, 740, 748, 763, 781, 783, 793, 797, 807, 812, 816, 823, 828, 834, 842, 866, 874, 876, 879, 882, 885, 886, 890, 896, 901, 904, 908, 916, 927, 928, 933, 940, 1025, 1027, 1030, 1031, 1038, 1048, 1054, 1056, 1057, 1065, 1077, 1080, 1081, 1082, 1083, 1084, 1085, 1087, 1089, 1090, 1091, 1104, 1107, 1118, 1120, 1124, 1125, 1126, 1132, 1144, 1145, 1147, 1151, 1154, 1157, 1158, 1165, 1168, 1169, 1170, 1172, 1173, 1174, 1175, 1176, 1177, 1178, 1179, 1184, 1185, 1187, 1188, 1191, 1201, 1205, 1208, 1209, 1214, 1215, 1216, 1218, 1219, 1225, 1236, 1239, 1241, 1247, 1248, 1249, 1250, 1253, 1264, 1265, 1268, 1269, 1273, 1274, 1275, 1276, 1277, 1278, 1281, 1282, 1284, 1286, 1288, 1290, 1291, 1295, 1296, 1299, 1300, 1301, 1302, 1303, 1304, 1307, 1309, 1310, 1313, 1315, 1318, 1320, 1325, 1330, 1332, 1335, 1336, 1343, 1345, 1347, 1350, 1351, 1352, 1354, 1355, 1358, 1359, 1361, 1365, 1369, 1373, 1374, 1377, 1379, 1385, 1389, 1392, 378, 383, 648, 688, 817, 1041, 1167, 1204, 1226, 1234, 1260, 1261, 1263, 1266, 1292, 1294, 1297, 1298, 1308, 1312, 1314, 1316, 1317, 1319, 1321, 1323, 1326, 1327, 1329, 1331, 1333, 1334, 1337, 1338, 1339, 1340, 1341, 1342, 1344, 1348, 1353, 1356, 1357, 1360, 1362, 1363, 1364, 1366, 1367, 1368, 1370, 1371, 1372, 1375, 1376, 1380, 1381, 1382, 1383, 1384, 1387, 1388, 1391, 1393)
          AND
          (
            rpm.INICIO BETWEEN '2016-01-01' AND '2016-06-30'
            OR rpm.FIM BETWEEN '2016-01-01' AND '2016-06-30'
          )
          AND rpm.ID = (
            SELECT MAX(ID)
            FROM
              relatorio_prorrogacao_med AS rpm2
            WHERE
              rpm2.PACIENTE_ID = rpm.PACIENTE_ID
          )
          AND c.empresa = 4
        GROUP BY
          rpm.PACIENTE_ID
        UNION
        SELECT
          c.idClientes AS paciente,
          mhc.modalidade,
          fme.DATA     AS data,
          'EVOLUCAO'   AS source
        FROM
          clientes AS c
          INNER JOIN fichamedicaevolucao AS fme ON (
          fme.PACIENTE_ID = c.idClientes
          )
          INNER JOIN modalidade_home_care AS mhc ON (mhc.id = fme.MOD_HOME_CARE)
        WHERE
          MOD_HOME_CARE != 0
          AND fme.PACIENTE_ID IN
              (4, 8, 10, 14, 24, 35, 41, 45, 48, 53, 58, 84, 94, 95, 96, 99, 109, 111, 118, 139, 144, 150, 151, 158, 161, 168, 179, 188, 198, 209, 228, 230, 233, 234, 237, 249, 255, 262, 267, 271, 276, 287, 292, 296, 298, 301, 315, 317, 324, 345, 360, 377, 382, 385, 388, 390, 407, 408, 412, 436, 443, 446, 453, 457, 465, 469, 479, 481, 496, 498, 509, 511, 515, 516, 528, 536, 546, 549, 558, 587, 593, 621, 623, 626, 627, 692, 706, 711, 732, 734, 740, 748, 763, 781, 783, 793, 797, 807, 812, 816, 823, 828, 834, 842, 866, 874, 876, 879, 882, 885, 886, 890, 896, 901, 904, 908, 916, 927, 928, 933, 940, 1025, 1027, 1030, 1031, 1038, 1048, 1054, 1056, 1057, 1065, 1077, 1080, 1081, 1082, 1083, 1084, 1085, 1087, 1089, 1090, 1091, 1104, 1107, 1118, 1120, 1124, 1125, 1126, 1132, 1144, 1145, 1147, 1151, 1154, 1157, 1158, 1165, 1168, 1169, 1170, 1172, 1173, 1174, 1175, 1176, 1177, 1178, 1179, 1184, 1185, 1187, 1188, 1191, 1201, 1205, 1208, 1209, 1214, 1215, 1216, 1218, 1219, 1225, 1236, 1239, 1241, 1247, 1248, 1249, 1250, 1253, 1264, 1265, 1268, 1269, 1273, 1274, 1275, 1276, 1277, 1278, 1281, 1282, 1284, 1286, 1288, 1290, 1291, 1295, 1296, 1299, 1300, 1301, 1302, 1303, 1304, 1307, 1309, 1310, 1313, 1315, 1318, 1320, 1325, 1330, 1332, 1335, 1336, 1343, 1345, 1347, 1350, 1351, 1352, 1354, 1355, 1358, 1359, 1361, 1365, 1369, 1373, 1374, 1377, 1379, 1385, 1389, 1392, 378, 383, 648, 688, 817, 1041, 1167, 1204, 1226, 1234, 1260, 1261, 1263, 1266, 1292, 1294, 1297, 1298, 1308, 1312, 1314, 1316, 1317, 1319, 1321, 1323, 1326, 1327, 1329, 1331, 1333, 1334, 1337, 1338, 1339, 1340, 1341, 1342, 1344, 1348, 1353, 1356, 1357, 1360, 1362, 1363, 1364, 1366, 1367, 1368, 1370, 1371, 1372, 1375, 1376, 1380, 1381, 1382, 1383, 1384, 1387, 1388, 1391, 1393)
          AND fme.DATA BETWEEN '2016-01-01 00:00:01' AND '2016-06-30 23:59:59'
          AND fme.ID = (
            SELECT MAX(ID)
            FROM
              fichamedicaevolucao AS fme2
            WHERE
              fme2.PACIENTE_ID = fme.PACIENTE_ID
          )
          AND c.empresa = 4
        GROUP BY
          fme.PACIENTE_ID
        UNION
        SELECT
          c.idClientes     AS paciente,
          mhc.modalidade,
          hmp.data_mudanca AS data,
          'SECMED'         AS source
        FROM
          clientes AS c
          INNER JOIN historico_modalidade_paciente AS hmp ON (hmp.paciente = c.idClientes)
          INNER JOIN modalidade_home_care AS mhc ON (mhc.id = hmp.modalidade)
        WHERE
          hmp.data_mudanca BETWEEN '2016-01-01' AND '2016-06-30'
          AND hmp.paciente IN
              (4, 8, 10, 14, 24, 35, 41, 45, 48, 53, 58, 84, 94, 95, 96, 99, 109, 111, 118, 139, 144, 150, 151, 158, 161, 168, 179, 188, 198, 209, 228, 230, 233, 234, 237, 249, 255, 262, 267, 271, 276, 287, 292, 296, 298, 301, 315, 317, 324, 345, 360, 377, 382, 385, 388, 390, 407, 408, 412, 436, 443, 446, 453, 457, 465, 469, 479, 481, 496, 498, 509, 511, 515, 516, 528, 536, 546, 549, 558, 587, 593, 621, 623, 626, 627, 692, 706, 711, 732, 734, 740, 748, 763, 781, 783, 793, 797, 807, 812, 816, 823, 828, 834, 842, 866, 874, 876, 879, 882, 885, 886, 890, 896, 901, 904, 908, 916, 927, 928, 933, 940, 1025, 1027, 1030, 1031, 1038, 1048, 1054, 1056, 1057, 1065, 1077, 1080, 1081, 1082, 1083, 1084, 1085, 1087, 1089, 1090, 1091, 1104, 1107, 1118, 1120, 1124, 1125, 1126, 1132, 1144, 1145, 1147, 1151, 1154, 1157, 1158, 1165, 1168, 1169, 1170, 1172, 1173, 1174, 1175, 1176, 1177, 1178, 1179, 1184, 1185, 1187, 1188, 1191, 1201, 1205, 1208, 1209, 1214, 1215, 1216, 1218, 1219, 1225, 1236, 1239, 1241, 1247, 1248, 1249, 1250, 1253, 1264, 1265, 1268, 1269, 1273, 1274, 1275, 1276, 1277, 1278, 1281, 1282, 1284, 1286, 1288, 1290, 1291, 1295, 1296, 1299, 1300, 1301, 1302, 1303, 1304, 1307, 1309, 1310, 1313, 1315, 1318, 1320, 1325, 1330, 1332, 1335, 1336, 1343, 1345, 1347, 1350, 1351, 1352, 1354, 1355, 1358, 1359, 1361, 1365, 1369, 1373, 1374, 1377, 1379, 1385, 1389, 1392, 378, 383, 648, 688, 817, 1041, 1167, 1204, 1226, 1234, 1260, 1261, 1263, 1266, 1292, 1294, 1297, 1298, 1308, 1312, 1314, 1316, 1317, 1319, 1321, 1323, 1326, 1327, 1329, 1331, 1333, 1334, 1337, 1338, 1339, 1340, 1341, 1342, 1344, 1348, 1353, 1356, 1357, 1360, 1362, 1363, 1364, 1366, 1367, 1368, 1370, 1371, 1372, 1375, 1376, 1380, 1381, 1382, 1383, 1384, 1387, 1388, 1391, 1393)
          AND hmp.id = (
            SELECT MAX(id)
            FROM
              historico_modalidade_paciente AS hmp2
            WHERE
              hmp2.paciente = hmp.paciente
              AND hmp2.data_mudanca BETWEEN '2016-01-01' AND '2016-06-30'
          )
          AND c.empresa = 4
        GROUP BY
          hmp.paciente
        ORDER BY
          data DESC
      ) AS result
    GROUP BY
      paciente
    ORDER BY
      modalidade ASC
  ) AS result
GROUP BY
  modalidade
ORDER BY
  modalidade;