CREATE TABLE nota_informe_perda  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parcela_id` int(11) NULL,
  `nota_id` int(11) NULL,
  `data` date NULL,
  `tipo_movimentacao` enum('1','0') NULL COMMENT '0 - Recebimento, 1 - Desembolso',
  `valor` decimal(8, 2) NULL,
  `plano_conta_id` int(11) NULL,
  `justificativa` text NULL,
  `created_by` int(11) NULL,
  `created_at` datetime NULL,
  `canceled_by` int(11) NULL,
  `canceled_at` datetime NULL,
  PRIMARY KEY (`id`)
);



ALTER TABLE `parcelas` 
ADD COLUMN `perda` enum('S','N') NULL DEFAULT 'N' COMMENT 'Recebe S caso for processada por Perda.' AFTER `valor_compensada`;

update parcelas set perda = 'N';
UPDATE `plano_contas_contabil` SET `CODIGO_DOMINIO` = 833 WHERE `ID` = 651;

-- codigo passado por Caio de uma conta contabil que não existia na dominio.

UPDATE `plano_contas_contabil` SET `CODIGO_DOMINIO` = 833 WHERE `ID` = 651;

UPDATE `plano_contas_contabil` SET `N5` = '(-) TITULOS DESCONTADOS' WHERE `ID` = 651;
UPDATE `plano_contas` SET `IPCC` = '(-) TITULOS DESCONTADOS' WHERE `ID` = 166;

INSERT INTO plano_contas(ID, N1, COD_N2, N2, COD_N3, N3, COD_N4, N4, COD_IPCC, IPCC, PROVISIONA, POSSUI_RETENCOES, ISS, ICMS, IPI, PIS, COFINS, CSLL, IR, PCC, INSS, DOMINIO_ID) VALUES (168, 'E', '1.0', 'RECEITAS', '1.0.4', 'RECEITA OPERACIONAL', '1.0.4.09', 'PROVISÃO DE PERDAS - CLIENTES', '5.2.1.01.001', 'PERDA - CLIENTES', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
UPDATE `plano_contas_contabil` SET `N5` = 'PERDA - CLIENTES' WHERE `ID` = 634;
