ALTER TABLE `devolucao_interna_pedido`
ADD COLUMN `confirmed_by`  int NULL AFTER `sent_at`,
ADD COLUMN `confirmed_at`  datetime NULL AFTER `confirmed_by`;
ALTER TABLE `devolucao_interna_itens`
ADD COLUMN `confirmado`  int(11) NULL AFTER `enviado`;