CREATE TABLE nota_compensacao  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `parcela_pivo_id` int(0) NULL  ,
  `parcela_id` int(0) NULL ,
  `valor` decimal(12, 2) NULL COMMENT 'Recebe valor compensado.',
  `data_compensado` date NULL,
  `created_by` int(11) NULL,
  `created_at` datetime NULL,
  `deleted_by` int(11) NULL,
  `deleted_at` datetime NULL,
  PRIMARY KEY (`id`)
);


ALTER TABLE `parcelas` 
ADD COLUMN `compensada` enum('PARCIAL','TOTAL','NAO') NULL DEFAULT 'NAO' COMMENT 'Caso tenha sido ' AFTER `vencimento_real`,
ADD COLUMN `valor_compensada` decimal(12, 2) NOT NULL AFTER `compensada`;



INSERT INTO `area_sistema`(`nome`, `descricao`, `id_pai`,`menu`, ordem, link) VALUES ('financeiro_conta_pagar_receber_compensar', 'Compensação', 96, 0, 6, '/financeiros/compensacao/?action=index');
INSERT INTO `area_permissoes`(`area_id`, `ordem`, `acao`, `descricao`) VALUES (107, 1, 'financeiro_conta_pagar_receber_compensar_criar', 'Criar Compensação');
INSERT INTO `area_permissoes`(`area_id`, `ordem`, `acao`, `descricao`) VALUES (107, 2, 'financeiro_conta_pagar_receber_compensar_cancelar', 'Cancelar Compensação');

INSERT INTO `area_permissoes`(`area_id`, `ordem`, `acao`, `descricao`) VALUES (99, 3, 'financeiro_conta_pagar_receber_buscar_parcela_compensar', 'Criar Compensar');

INSERT INTO `area_permissoes`(`area_id`, `ordem`, `acao`, `descricao`) VALUES (99, 4, 'financeiro_conta_pagar_receber_buscar_parcela_descompensar', 'Cancelar Compensar');


CREATE TABLE `historico_parcelas`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parcela` int(11) NOT NULL,
  `idNota` int(10) UNSIGNED NOT NULL,
  `valor` decimal(12, 2) NOT NULL,
  `vencimento` date NOT NULL COMMENT 'Recebe o vencimento do boleto.',
  `codBarras` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `origem` int(11) NOT NULL DEFAULT 1,
  `aplicacao` int(11) NOT NULL,
  `obs` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `comprovante` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `empresa` int(10) UNSIGNED NOT NULL,
  `JurosMulta` decimal(12, 2) NOT NULL DEFAULT 0.00,
  `desconto` decimal(12, 2) NOT NULL,
  `encargos` decimal(12, 2) NOT NULL,
  `TR` int(11) NOT NULL,
  `VALOR_PAGO` decimal(12, 2) NOT NULL,
  `DATA_PAGAMENTO` date NOT NULL,
  `OUTROS_ACRESCIMOS` decimal(12, 2) NOT NULL DEFAULT 0.00,
  `NUM_DOCUMENTO` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ISSQN` decimal(12, 2) NOT NULL DEFAULT 0.00,
  `PIS` decimal(12, 2) NOT NULL DEFAULT 0.00,
  `COFINS` decimal(12, 2) NOT NULL DEFAULT 0.00,
  `CSLL` decimal(12, 2) NOT NULL DEFAULT 0.00,
  `IR` decimal(12, 2) NOT NULL DEFAULT 0.00,
  `CONCILIADO` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DATA_PROCESSADO` datetime NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  `DATA_CONCILIADO` date NOT NULL,
  `DATA_DESPROCESSADA` datetime NOT NULL,
  `DESPROCESSADA_POR` int(11) NOT NULL,
  `JUSTIFICATIVA_DESPROCESSADA` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `PARCELA_ORIGEM` int(11) NOT NULL,
  `motivo_juros_id` int(11) NULL DEFAULT NULL,
  `nota_id_fatura_aglutinacao` int(11) NULL DEFAULT NULL COMMENT 'Coluna responsável por gravar o id da nota que foi gerada a partir de várias parcelas',
  `vencimento_real` date NULL DEFAULT NULL COMMENT 'Recebe o vencimento modificado da nota.',
  `compensada` enum('PARCIAL','TOTAL','NAO') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT 'NAO' COMMENT 'Caso tenha sido ',
  `valor_compensada` decimal(12, 2) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
  
) ;

ALTER TABLE `historico_parcelas` 
ADD COLUMN `deleted_by` int(11) NULL AFTER `valor_compensada`,
ADD COLUMN `deleted_at` datetime NULL AFTER `deleted_by`;