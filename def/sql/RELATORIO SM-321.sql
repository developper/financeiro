## CONSULTA ÚNICA
-- MATERIAIS / MEDICAMENTOS / DIETA / EQUIPAMENTOS

SET @DATA_INICIO = '2014-01-01';
SET @DATA_FIM = '2014-07-31'; 

SELECT 
  (CASE 
      WHEN FTM.`TIPO` in (0,1,3) THEN CONCAT(CAT.`principio`,' - ',CAT.`apresentacao`) 
      ELSE IF(FTM.`TABELA_ORIGEM`='valorescobranca',VLB.`DESC_COBRANCA_PLANO`,CBP.`item`)      
   END) as ITEM,
  EMP.nome as EMPRESA,
  SUM(FTM.`QUANTIDADE`) as QUANTIDADE,
  DATE_FORMAT(FAT.`DATA_INICIO`,'%m') as MES  
FROM
  fatura as FAT INNER JOIN 
  faturamento as FTM ON (FAT.`ID` = FTM.`FATURA_ID`)  LEFT JOIN
  cobrancaplanos as CBP ON (FTM.`CATALOGO_ID` = CBP.`id`) LEFT JOIN
  valorescobranca as VLB ON ( FTM.`CATALOGO_ID` = VLB.`id`) LEFT JOIN
  catalogo as CAT ON (FTM.`CATALOGO_ID` = CAT.`ID` ) INNER JOIN
  clientes as CLI ON (FAT.`PACIENTE_ID` = CLI.`idClientes`) INNER JOIN
  empresas as EMP ON (CLI.empresa = EMP.id)
WHERE
  FAT.`DATA_INICIO` >= @DATA_INICIO AND FAT.`DATA_FIM` <= @DATA_FIM AND
  FAT.`CANCELADO_EM`='0000-00-00 00:00:00' AND
  FTM.`CANCELADO_EM`='0000-00-00 00:00:00' AND
  FAT.`ORCAMENTO`=0 AND
  CLI.idClientes not in (142,220,69,263,112,101,67) 
GROUP BY
  FTM.`CATALOGO_ID`, FTM.TIPO
ORDER BY
  FAT.`DATA_INICIO`



## CONSULTA SEPARADA 

-- MATERIAIS/MEDICAMENTOS/DIETA

SET @DATA_INICIO = '2014-01-01';
SET @DATA_FIM = '2014-07-31'; 


SELECT 
 CONCAT(CAT.`principio`,' - ',CAT.`apresentacao`) as ITEM,
 EMP.nome as EMPRESA,
 SUM(FTM.`QUANTIDADE`) as QUANTIDADE,
 DATE_FORMAT(FAT.`DATA_INICIO`,'%m') as MES  
FROM
  fatura as FAT INNER JOIN 
  faturamento as FTM ON (FAT.`ID` = FTM.`FATURA_ID`)  LEFT JOIN  
  catalogo as CAT ON (FTM.`CATALOGO_ID` = CAT.`ID` ) INNER JOIN
  clientes as CLI ON (FAT.`PACIENTE_ID` = CLI.`idClientes`) INNER JOIN
  empresas as EMP ON (CLI.empresa = EMP.id)
WHERE
  FAT.`DATA_INICIO` >= @DATA_INICIO AND FAT.`DATA_FIM` <= @DATA_FIM AND
  FAT.`CANCELADO_EM`='0000-00-00 00:00:00' AND
  FTM.`CANCELADO_EM`='0000-00-00 00:00:00' AND
  FAT.`ORCAMENTO`=0 AND
  FTM.`TIPO` in (0,1,3) AND
  CLI.idClientes not in (142,220,69,263,112,101,67)
GROUP BY
  FTM.TIPO,FTM.`CATALOGO_ID`
ORDER BY
  FAT.`DATA_INICIO`


-- EQUIPAMENTOS

SET @DATA_INICIO = '2014-01-01';
SET @DATA_FIM = '2014-07-31'; 

SELECT 
  IF(FTM.`TABELA_ORIGEM`='valorescobranca',VLB.`DESC_COBRANCA_PLANO`,CBP.`item`) as ITEM ,
  SUM(FTM.`QUANTIDADE`) as QUANTIDADE,
  EMP.nome as EMPRESA,
  DATE_FORMAT(FAT.`DATA_INICIO`,'%m') as MES  
FROM
  fatura as FAT INNER JOIN 
  faturamento as FTM ON (FAT.`ID` = FTM.`FATURA_ID`)  LEFT JOIN
  cobrancaplanos as CBP ON (FTM.`CATALOGO_ID` = CBP.`id`) LEFT JOIN
  valorescobranca as VLB ON (FTM.`CATALOGO_ID` = VLB.`id`) INNER JOIN
  clientes as CLI ON (FAT.`PACIENTE_ID` = CLI.`idClientes`) INNER JOIN
  empresas as EMP ON (CLI.empresa = EMP.id)
WHERE
  FAT.`DATA_INICIO` >= @DATA_INICIO AND FAT.`DATA_FIM` <= @DATA_FIM AND
  FAT.`CANCELADO_EM`='0000-00-00 00:00:00' AND
  FTM.`CANCELADO_EM`='0000-00-00 00:00:00' AND
  FAT.`ORCAMENTO`=0 AND
  FTM.`TIPO`=5   
GROUP BY
  FTM.`CATALOGO_ID`
ORDER BY
  FAT.`DATA_INICIO`