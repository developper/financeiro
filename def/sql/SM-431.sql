-- ----------------------------
-- Table structure for impostos_tarifas
-- ----------------------------
CREATE TABLE `impostos_tarifas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(255) NOT NULL,
  `tipo` enum('IMPOSTO','TARIFA') NOT NULL,
  `debito_credito` enum('CREDITO','DEBITO') DEFAULT NULL,
  `ipcf` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of impostos_tarifas
-- ----------------------------
INSERT INTO `impostos_tarifas` VALUES ('1', 'IOF', 'IMPOSTO', 'DEBITO', '2.2.7.11');
INSERT INTO `impostos_tarifas` VALUES ('2', 'TED/DOC', 'TARIFA', 'DEBITO', '2.5.1.01');
INSERT INTO `impostos_tarifas` VALUES ('3', 'CUSTODIA', 'TARIFA', 'DEBITO', '2.5.1.01');
INSERT INTO `impostos_tarifas` VALUES ('4', 'CESTA BASICA', 'TARIFA', 'DEBITO', '2.5.1.01');
INSERT INTO `impostos_tarifas` VALUES ('5', 'COBRANÇA', 'TARIFA', 'DEBITO', '2.5.1.01');
INSERT INTO `impostos_tarifas` VALUES ('6', 'DIVERSAS', 'TARIFA', null, '2.5.1.01');

-- ----------------------------
-- Table structure for impostos_tarifas_lancto
-- ----------------------------
CREATE TABLE `impostos_tarifas_lancto` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`conta`  int(11) NOT NULL ,
`item`  int(11) NOT NULL ,
`documento`  varchar(50) NOT NULL ,
`data`  date NOT NULL ,
`valor`  double NOT NULL ,
PRIMARY KEY (`id`)
)
