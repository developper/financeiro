--- Relatório aditivo de enfermagem.---
SELECT
clientes.nome as PACIENTE,
usuarios.nome as USUARIO,
relatorio_aditivo_enf.`DATA`,
relatorio_aditivo_enf.INICIO,
relatorio_aditivo_enf.FIM
FROM
relatorio_aditivo_enf
INNER JOIN clientes ON relatorio_aditivo_enf.PACIENTE_ID = clientes.idClientes
INNER JOIN usuarios ON usuarios.idUsuarios = relatorio_aditivo_enf.USUARIO_ID
WHERE
relatorio_aditivo_enf.`DATA` > '2015-01-19'


--- Relatório de prorrogação de enfermagem.---
SELECT
clientes.nome as PACIENTE,
usuarios.nome as USUARIO,
relatorio_prorrogacao_enf.`DATA`,
relatorio_prorrogacao_enf.INICIO,
relatorio_prorrogacao_enf.FIM
FROM
relatorio_prorrogacao_enf
INNER JOIN usuarios ON relatorio_prorrogacao_enf.USUARIO_ID = usuarios.idUsuarios
INNER JOIN clientes ON relatorio_prorrogacao_enf.PACIENTE_ID = clientes.idClientes
WHERE
relatorio_prorrogacao_enf.`DATA` > '2015-01-19'

--- Relatório de  prorrogação de medica.---
SELECT
clientes.nome as PACIENTE,
usuarios.nome as USUARIO,
relatorio_prorrogacao_med.`DATA`,
relatorio_prorrogacao_med.INICIO,
relatorio_prorrogacao_med.FIM
FROM
relatorio_prorrogacao_med
INNER JOIN usuarios ON relatorio_prorrogacao_med.USUARIO_ID = usuarios.idUsuarios
INNER JOIN clientes ON relatorio_prorrogacao_med.PACIENTE_ID = clientes.idClientes
WHERE
relatorio_prorrogacao_med.`DATA` > '2015-01-19'

--- Relatório aditivo medico.---
SELECT
clientes.nome as PACIENTE,
usuarios.nome as  USUARIO,
prescricoes.`data` as DATA,
prescricoes.inicio as INICIO,
prescricoes.fim as FIM
FROM
prescricoes
INNER JOIN usuarios ON prescricoes.criador = usuarios.idUsuarios
INNER JOIN clientes ON prescricoes.paciente = clientes.idClientes
WHERE
prescricoes.`data` > '2015-01-19' AND
prescricoes.Carater = 3

