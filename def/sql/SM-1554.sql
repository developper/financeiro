UPDATE
itenspedidosinterno
SET
pedidosinterno.CANCELADO_POR = 245,
pedidosinterno.DATA_CANCELADO = NOW(),
pedidosinterno.JUSTIFICATIVA = "Com as mudanças e inventário que fizemos, corremos o risco de ter feito o inventário e dar entrada em pedido já contabilizado pela contagem, cancelamento feito pelo TI"
WHERE
pedidosinterno.ID not in (12634, 13049, 13059, 13103 , 13164) and pedidosinterno.EMPRESA = 2 and pedidosinterno.`STATUS` <> 'F'



UPDATE
itenspedidosinterno
SET
itenspedidosinterno.CANCELADO_ENTRADA = 'S',
itenspedidosinterno.CANCELADO_ENTRADA_POR = 245,
itenspedidosinterno.DATA_CANCELADO_ENTRADA = NOW(),
itenspedidosinterno.JUSTIFICATIVA_ENTRADA = "Com as mudanças e inventário que fizemos, corremos o risco de ter feito o inventário e dar entrada em pedido já contabilizado pela contagem, cancelamento feito pelo TI"
WHERE
(
				itenspedidosinterno.STATUS = 'P'	OR
                                (
					itenspedidosinterno.STATUS = 'F' AND
					itenspedidosinterno.CONFIRMADO < itenspedidosinterno.ENVIADO AND
					itenspedidosinterno.CANCELADO='N' AND
					itenspedidosinterno.CANCELADO_ENTRADA='N'
				)
                OR
				(
					itenspedidosinterno.STATUS = 'F' AND
					itenspedidosinterno.CANCELADO='S' AND
					itenspedidosinterno.CONFIRMADO < itenspedidosinterno.ENVIADO AND
					itenspedidosinterno.CANCELADO_ENTRADA='N'
				)
			)
AND
itenspedidosinterno.PEDIDOS_INTERNO_ID in (167	,
303	,
338	,
385	,
472	,
481	,
543	,
544	,
562	,
563	,
564	,
582	,
587	,
588	,
591	,
592	,
600	,
626	,
629	,
630	,
631	,
637	,
638	,
656	,
659	,
681	,
691	,
698	,
1427	,
1652	,
2389	,
3908	,
10642	,
10643	,
10728	,
10763	,
10780	,
10802	,
10884	,
10922	,
10934	,
10994	,
11375	,
11530	,
11536	,
12022	)
