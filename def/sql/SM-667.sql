
ALTER TABLE `relatorio_aditivo_enf`
ADD COLUMN `confirmado`  tinyint(1) NOT NULL DEFAULT 0 COMMENT 'RECEBE 2-NAO 1-SIM' AFTER `FERIDAS`,
ADD COLUMN `confirmado_por`  int(11) NULL AFTER `confirmado`,
ADD COLUMN `confirmado_em`  datetime NULL AFTER `confirmado_por`;

CREATE TABLE `solicitacao_rel_aditivo_enf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relatorio_enf_aditivo_id` int(11) NOT NULL,
  `catalogo_id` int(11) NOT NULL,
  `tiss` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `quantidade_kit` int(11) NOT NULL,
  `kit_id` int(11) NOT NULL,
  `cancelado_por` int(11) DEFAULT NULL,
  `cancelado_em` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `solicitacoes`
MODIFY COLUMN `TIPO_SOLICITACAO`  int(1) NOT NULL COMMENT 'Recebe -2 EMERGENCIA ENFERMAGEM, -1 - AVULSA, 2 - PERIODICA, 3-ADITIVA,4-AVALIACAO,8-ADITIVO ENFERMAGEM' AFTER `DATA_MAX_ENTREGA`;

ALTER TABLE `solicitacoes`
ADD COLUMN `relatorio_aditivo_enf_id`  int(11) NULL ;


--SM-679
ALTER TABLE `justificativa_solicitacao_avulsa`
ADD COLUMN `visualizar`  tinyint NULL DEFAULT 1 AFTER `justificativa`;

UPDATE `justificativa_solicitacao_avulsa` SET `visualizar`='0' WHERE (`id`='9');
UPDATE `justificativa_solicitacao_avulsa` SET `visualizar`='0' WHERE (`id`='3');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('12', 'SOLICITAÇÃO ADITIVA DE ENFERMAGEM', '0');