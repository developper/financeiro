-- selecet movimentacao medicamentos ativos ultimos 6 meses da matriz

SELECT
catalogo.ID,
catalogo.lab_desc as LABORATORIO,
concat(catalogo.principio,' ',catalogo.apresentacao)as  Item,
COALESCE(E.qtdEntrada,0) as Quantidade_Entrada,
COALESCE(S.qtdSaida,0) as Quantidade_Saida,
if((COALESCE(E.qtdEntrada,0) + COALESCE(S.qtdSaida,0)) > 0, "SIM", "N�O" ) as MOVIMENTACAO
FROM
catalogo

LEFT JOIN
(

		SELECT
		catalogo.ID,
		sum(saida.quantidade) as qtdSaida
		FROM
		catalogo
		LEFT JOIN saida on catalogo.ID = saida.CATALOGO_ID
		WHERE
		catalogo.tipo = 0 AND
		catalogo.ATIVO = 'A' AND
		saida.UR in (2,4,5,7,11)
		AND
		saida.`data` > '2016-07-01'
		GROUP BY catalogo.ID

) as S on  (catalogo.ID = S.ID )
LEFT JOIN
(

		SELECT
		catalogo.ID,
		sum(entrada.qtd) as qtdEntrada
		FROM
		catalogo
		LEFT JOIN entrada on catalogo.ID = entrada.CATALOGO_ID
		WHERE
		catalogo.tipo = 0 AND
		catalogo.ATIVO = 'A' 		AND
		entrada.UR = 1
		AND
    entrada.`data` > '2016-07-01'
		GROUP BY catalogo.ID


) as E on  (catalogo.ID = E.ID )

WHERE
catalogo.tipo = 0 AND
catalogo.ATIVO = 'A'
ORDER BY catalogo.ID