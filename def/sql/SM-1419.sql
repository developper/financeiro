
RENAME TABLE encaminhamento_urgencia_emergencia_med TO intercorrencia_med;

RENAME TABLE enc_urgencia_emergencia_med_exame TO intercorrencia_med_exame;

RENAME TABLE enc_urgencia_emergencia_med_problema TO intercorrencia_med_problema;

RENAME TABLE enc_urgencia_emergencia_med_procedimento TO intercorrencia_med_procedimento;

RENAME TABLE enc_urgencia_emergencia_med_quadro_clinico TO intercorrencia_med_quadro_clinico;

ALTER TABLE `intercorrencia_med_exame`
CHANGE COLUMN `enc_urgencia_emergencia_med_id` `intercorrencia_med_id`  int(11) NULL DEFAULT NULL AFTER `descricao`;

ALTER TABLE `intercorrencia_med_problema`
CHANGE COLUMN `enc_urgencia_emergencia_med_id` `intercorrencia_med_id`  int(11) NULL DEFAULT NULL AFTER `id`;

ALTER TABLE `intercorrencia_med_procedimento`
CHANGE COLUMN `enc_urgencia_emergencia_med_id` `intercorrencia_med_id`  int(11) NULL DEFAULT NULL AFTER `descricao`;

ALTER TABLE `intercorrencia_med_quadro_clinico`
CHANGE COLUMN `enc_urgencia_emergencia_med_id` `intercorrencia_med_id`  int(11) NULL DEFAULT NULL AFTER `descricao`;


ALTER TABLE `intercorrencia_med`
ADD COLUMN `hora`  time NULL AFTER `vaga_uti`,
ADD COLUMN `hospital`  text NULL AFTER `hora`;