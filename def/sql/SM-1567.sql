
ALTER TABLE `entrada_ur`
ADD COLUMN `catalogo_id`  int(11) NULL AFTER `UR`,
ADD COLUMN `saida_id`  int(11) NULL AFTER `catalogo_id`;

ALTER TABLE `saida`
ADD COLUMN `modelo`  varchar(40) NULL COMMENT 'Vai impactar na rotina de pedidos internos.' AFTER `fornecedor`;

ALTER TABLE `saida`
MODIFY COLUMN `modelo`  varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'Utilizado para funcionalidade de pedidos internos.' AFTER `fornecedor`,
ADD COLUMN `confirmacao_pedidointerno`  int(11) NULL COMMENT 'Qunatidade confirmada pela UR quando for pedido interno.' AFTER `modelo`;

ALTER TABLE `saida`
MODIFY COLUMN `confirmacao_pedidointerno`  int(11) NOT NULL DEFAULT 0 COMMENT 'Qunatidade confirmada pela UR quando for pedido interno.' AFTER `modelo`;

UPDATE
`saida`
SET
modelo = 'v1'
WHERE
saida.modelo is null
AND
saida.idCliente = 0