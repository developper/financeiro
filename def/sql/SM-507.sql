/*
Navicat MySQL Data Transfer

Source Server         : vagrant-sismederi
Source Server Version : 50619
Source Host           : localhost:3306
Source Database       : sismederi

Target Server Type    : MYSQL
Target Server Version : 50619
File Encoding         : 65001

Date: 2014-10-10 11:16:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sugestoes_auditoria
-- ----------------------------
DROP TABLE IF EXISTS `sugestoes_secmed`;
CREATE TABLE `sugestoes_secmed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_capmedica` int(11) DEFAULT NULL,
  `id_paciente` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `local_sugestao` enum('fcav','prsc') DEFAULT NULL COMMENT 'fcav = Ficha de Avaliação, prsc = Prescrição',
  `tempo_resposta` datetime DEFAULT NULL COMMENT 'Ultima alteração do médico',
  `status` tinyint(1) DEFAULT NULL COMMENT '0 para em processo de correção, 1 para totalmente corrigido',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for sugestoes_auditoria_lista
-- ----------------------------
DROP TABLE IF EXISTS `sugestoes_secmed_lista`;
CREATE TABLE `sugestoes_secmed_lista` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sugestao` int(11) DEFAULT NULL,
  `tipo_sugestao` enum('erp','inc') DEFAULT NULL COMMENT 'erp = Erro de preenchimento, inc = Inconformidade',
  `secao_sugestao` enum('eqp','pm','ium','com','aa','am','pa','mh','fam') DEFAULT NULL,
  `sugestao` text,
  `status` tinyint(1) DEFAULT NULL COMMENT '0 para não corrigido, 1 para corrigido',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
