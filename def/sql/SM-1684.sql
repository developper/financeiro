ALTER TABLE `devolucao_interna_pedido`
MODIFY COLUMN `status`  enum('FINALIZADO','AUTORIZADO','CANCELADO','ENVIADO','CONFIRMADO PARCIALMENTE','PENDENTE') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `motivo_cancelamento`;

CREATE TABLE `historico_devolucao_interna_item_confirmacao` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`devolucao_interna_item_id`  int(11) NOT NULL ,
`created_at`  datetime NOT NULL ,
`created_by`  int(11) NOT NULL ,
`quantidade`  int NOT NULL ,
PRIMARY KEY (`id`)
)
;
