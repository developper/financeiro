CREATE TABLE `historico_custo_servico_plano_ur` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`cobrancaplano_id`  int(11) NOT NULL ,
`empresa_id`  int(11) NOT NULL ,
`inicio`  datetime NOT NULL ,
`custo`  double(8,2) NOT NULL ,
`usuario_id`  int(11) NOT NULL ,
`plano_id`  int(11) NOT NULL DEFAULT 1 COMMENT 'Como não vão ser mais lançados por plano vai ficar fixo como 1, plano particular' ,
`data_sistema`  datetime NOT NULL ,
PRIMARY KEY (`id`)
)
;

ALTER TABLE `historico_custo_servico_plano_ur`
CHANGE COLUMN `data_sistema` `created_at`  datetime NOT NULL AFTER `plano_id`,
ADD COLUMN `created_by`  int(11) NOT NULL AFTER `created_at`;