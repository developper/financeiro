CREATE TABLE `orcamento_financeiro_plano_contas_valores`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orcamento_gerencial_topicos_id` int(11) NOT NULL,
  `plano_contas_id` int(11) NOT NULL,
  `versao_id` int(11) NOT NULL,
  `aliquota` decimal(10, 5) NOT NULL,
  `valor_mensal` decimal(20, 2) NOT NULL,
  `data` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for orcamento_financeiro_topicos
-- ----------------------------
DROP TABLE IF EXISTS `orcamento_financeiro_topicos`;
CREATE TABLE `orcamento_financeiro_topicos`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of orcamento_financeiro_topicos
-- ----------------------------
INSERT INTO `orcamento_financeiro_topicos` VALUES (1, 'Recebimento');
INSERT INTO `orcamento_financeiro_topicos` VALUES (2, 'Home Care');
INSERT INTO `orcamento_financeiro_topicos` VALUES (3, 'Outros Produtos/Serviços');
INSERT INTO `orcamento_financeiro_topicos` VALUES (4, 'Impostos ');
INSERT INTO `orcamento_financeiro_topicos` VALUES (5, 'Federais');
INSERT INTO `orcamento_financeiro_topicos` VALUES (6, 'Municipais	\r\nMunicipais');
INSERT INTO `orcamento_financeiro_topicos` VALUES (7, 'Fornecedores');
INSERT INTO `orcamento_financeiro_topicos` VALUES (8, 'Material Aplicado');
INSERT INTO `orcamento_financeiro_topicos` VALUES (9, 'Serviços');
INSERT INTO `orcamento_financeiro_topicos` VALUES (10, 'Despesas Operacionais');
INSERT INTO `orcamento_financeiro_topicos` VALUES (11, 'Despesas Gerais e Administrativas');
INSERT INTO `orcamento_financeiro_topicos` VALUES (12, 'Salários Pessoal ');
INSERT INTO `orcamento_financeiro_topicos` VALUES (13, 'Encargos Sociais');
INSERT INTO `orcamento_financeiro_topicos` VALUES (14, 'Pro-Labore');
INSERT INTO `orcamento_financeiro_topicos` VALUES (15, 'Água e Luz');
INSERT INTO `orcamento_financeiro_topicos` VALUES (16, 'Alugueis/Iptu');
INSERT INTO `orcamento_financeiro_topicos` VALUES (17, 'Propaganda');
INSERT INTO `orcamento_financeiro_topicos` VALUES (18, 'Telefone/Internet');
INSERT INTO `orcamento_financeiro_topicos` VALUES (19, 'Consultorias');
INSERT INTO `orcamento_financeiro_topicos` VALUES (20, 'Retenções');
INSERT INTO `orcamento_financeiro_topicos` VALUES (21, 'Despesas em geral');
INSERT INTO `orcamento_financeiro_topicos` VALUES (22, 'Fluxo Caixa Operacional');
INSERT INTO `orcamento_financeiro_topicos` VALUES (23, '(+) Venda de Ativos');
INSERT INTO `orcamento_financeiro_topicos` VALUES (24, '(-) CAPEX');
INSERT INTO `orcamento_financeiro_topicos` VALUES (25, 'Fluxo Caixa Investimento');
INSERT INTO `orcamento_financeiro_topicos` VALUES (26, 'Captação Empréstimos');
INSERT INTO `orcamento_financeiro_topicos` VALUES (27, 'Juros Empréstimo');
INSERT INTO `orcamento_financeiro_topicos` VALUES (28, 'Parcela Empréstimo');
INSERT INTO `orcamento_financeiro_topicos` VALUES (29, 'Parcelamento Tributário');
INSERT INTO `orcamento_financeiro_topicos` VALUES (30, 'Fluxo Caixa Endividamento');
INSERT INTO `orcamento_financeiro_topicos` VALUES (31, 'Aporte Capital Sócios');
INSERT INTO `orcamento_financeiro_topicos` VALUES (32, 'Distribuição Resultados Sócios');
INSERT INTO `orcamento_financeiro_topicos` VALUES (33, 'Classe A');
INSERT INTO `orcamento_financeiro_topicos` VALUES (34, 'Classe B');
INSERT INTO `orcamento_financeiro_topicos` VALUES (35, 'Classe C');
INSERT INTO `orcamento_financeiro_topicos` VALUES (36, 'Classe D');


-- ----------------------------
-- Table structure for orcamento_financeiro_topicos_valores
-- ----------------------------
DROP TABLE IF EXISTS `orcamento_financeiro_topicos_valores`;
CREATE TABLE `orcamento_financeiro_topicos_valores`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orcamento_financeiro_topicos` int(11) NOT NULL,
  `orcamento_financeiro_versao_id` int(11) NOT NULL,
  `valor_mensal` decimal(20, 2) NOT NULL,
  `data` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for orcamento_financeiro_versao
-- ----------------------------
DROP TABLE IF EXISTS `orcamento_financeiro_versao`;
CREATE TABLE `orcamento_financeiro_versao`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inicio` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fim` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `orcamento_financeiro_versao_estrutura` int(255) NULL DEFAULT NULL,
  `observacao` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `criado_por` int(11) NOT NULL,
  `criado_em` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for orcamento_financeiro_estrutura
-- ----------------------------
DROP TABLE IF EXISTS `orcamento_financeiro_estrutura`;
CREATE TABLE `orcamento_financeiro_estrutura`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `versao_estrutura` int(1) NOT NULL,
  `orcamento_finananceiro_topicos_id` int(11) NOT NULL,
  `ordem` int(11) NULL DEFAULT NULL,
  `id_pai` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

INSERT INTO `orcamento_financeiro_estrutura` VALUES (2, 1, 1, 1, 0);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (3, 1, 2, 1, 1);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (4, 1, 3, 2, 1);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (5, 1, 4, 2, 0);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (6, 1, 5, 1, 4);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (7, 1, 6, 1, 4);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (8, 1, 7, 3, 0);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (9, 1, 8, 1, 7);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (10, 1, 9, 2, 7);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (11, 1, 10, 4, 0);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (12, 1, 11, 5, 0);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (13, 1, 12, 1, 11);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (14, 1, 13, 2, 11);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (15, 1, 14, 3, 11);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (16, 1, 15, 4, 11);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (17, 1, 16, 5, 11);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (18, 1, 17, 6, 11);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (19, 1, 18, 7, 11);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (20, 1, 19, 8, 11);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (21, 1, 20, 9, 11);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (22, 1, 21, 10, 11);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (23, 1, 22, 6, 0);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (24, 1, 23, 1, 22);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (25, 1, 24, 1, 22);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (26, 1, 25, 7, 0);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (27, 1, 26, 1, 25);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (28, 1, 27, 2, 25);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (29, 1, 28, 3, 25);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (30, 1, 29, 4, 25);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (31, 1, 30, 8, 0);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (32, 1, 31, 1, 30);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (33, 1, 32, 2, 30);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (34, 1, 33, 1, 2);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (35, 1, 34, 2, 2);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (36, 1, 35, 3, 2);
INSERT INTO `orcamento_financeiro_estrutura` VALUES (37, 1, 36, 4, 2);


ALTER TABLE `orcamento_financeiro_topicos_valores` 
ADD COLUMN `aliquota` decimal(20, 2) NULL AFTER `data`;

ALTER TABLE `orcamento_financeiro_plano_contas_valores` 
CHANGE COLUMN `orcamento_gerencial_topicos_id` `orcamento_financeiro_topicos_id` int(11) NOT NULL AFTER `id`;


INSERT INTO `area_sistema`(`id`, `nome`, `descricao`, `ordem`, `id_pai`, `link`) VALUES (112, 'financeiro_orcamento_financeiro', 'Orçamento Financeiro', 9, 69, '/financeiros/?action=orcamento-financeiro-index');
INSERT INTO `area_permissoes`(`area_id`, `ordem`, `acao`, `descricao`) VALUES (112, 1, 'financeiro_orcamento_financeiro_criar', 'Criar');
INSERT INTO `area_permissoes`( `area_id`, `ordem`, `acao`, `descricao`) VALUES ( 112, 2, 'financeiro_orcamento_financeiro_editar', 'Editar');
INSERT INTO `area_permissoes`( `area_id`, `ordem`, `acao`, `descricao`) VALUES ( 112, 3, 'financeiro_orcamento_financeiro_visualizar', 'Visualizar');

ALTER TABLE `orcamento_financeiro_plano_contas_valores` 
MODIFY COLUMN `aliquota` decimal(10, 2) NOT NULL AFTER `versao_id`;

ALTER TABLE `orcamento_financeiro_plano_contas_valores` 
ADD COLUMN `criado_em` datetime NULL AFTER `valor_mensal`,
ADD COLUMN `criado_por` int(11) NULL AFTER `criado_em`,
ADD COLUMN `editado_em` datetime NULL AFTER `criado_por`,
ADD COLUMN `editado_por` int(11) NULL AFTER `editado_em`,
ADD COLUMN `deletado_em` datetime NULL AFTER `editado_por`,
ADD COLUMN `deletado_por` int(11) NULL AFTER `deletado_em`;

ALTER TABLE `orcamento_financeiro_topicos_valores` 
ADD COLUMN `criado_em` datetime NULL AFTER `valor_mensal`,
ADD COLUMN `criado_por` int(11) NULL AFTER `criado_em`,
ADD COLUMN `editado_em` datetime NULL AFTER `criado_por`,
ADD COLUMN `editado_por` int(11) NULL AFTER `editado_em`,
ADD COLUMN `deletado_em` datetime NULL AFTER `editado_por`,
ADD COLUMN `deletado_por` int(11) NULL AFTER `deletado_em`;


ALTER TABLE `orcamento_financeiro_estrutura` 
ADD COLUMN `aliquota` decimal(10, 2) NULL DEFAULT 0 AFTER `id_pai`;

UPDATE `orcamento_financeiro_estrutura` SET `aliquota` = 100 WHERE `id` = 2;

UPDATE `orcamento_financeiro_estrutura` SET `aliquota` = 0 WHERE `id` <> 2;

INSERT INTO `orcamento_financeiro_topicos` VALUES (37,'Outros Serviços');
INSERT INTO `orcamento_financeiro_estrutura`(`id`, `versao_estrutura`, `orcamento_finananceiro_topicos_id`, `ordem`, `id_pai`) VALUES (38, 1, 37, 1, 10);