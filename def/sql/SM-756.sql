--consulta

set @inicio ='2015-03-01';
set @fim    ='2015-04-08';
SELECT
UPPER(clientes.nome) AS PACIENTE,
UPPER(usuarios.nome) AS ENFERMEIRO_SOLICITANTE,
prescricoes.inicio AS INICIO,
prescricoes.fim AS FIM,
prescricoes.`data` AS DATA_PRESCRICAO,
solicitacoes.`data` AS DATA_SOLICITACAO,
empresas.nome AS UR
FROM
prescricoes
INNER JOIN clientes ON clientes.idClientes = prescricoes.paciente
INNER JOIN empresas ON clientes.empresa = empresas.id
INNER JOIN planosdesaude ON clientes.convenio = planosdesaude.id
INNER JOIN solicitacoes ON solicitacoes.idPrescricao = prescricoes.id
INNER JOIN usuarios ON solicitacoes.enfermeiro = usuarios.idUsuarios
WHERE
prescricoes.paciente NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101) AND
prescricoes.Carater IN (2, 5) AND
solicitacoes.`data` BETWEEN @inicio AND @fim and 
empresas.id in (1,2)
GROUP BY
prescricoes.id
ORDER BY
DATA_PRESCRICAO ASC