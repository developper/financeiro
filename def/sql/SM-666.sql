-- Alice de Souza
UPDATE `equipamentosativos` SET `DATA_INICIO`='2014-12-29 16:24:45' WHERE (`ID`='6138');
UPDATE `equipamentosativos` SET `DATA_INICIO`='2014-12-31 10:36:02' WHERE (`ID`='6198');
UPDATE `equipamentosativos` SET `DATA_FIM`='2014-12-31 10:38:42' WHERE (`ID`='1959');
UPDATE `equipamentosativos` SET `DATA_FIM`='2014-12-31 10:38:42' WHERE (`ID`='1959');
UPDATE `equipamentosativos` SET `DATA_FIM`='2014-12-31 10:39:16' WHERE (`ID`='5500');
UPDATE `equipamentosativos` SET `DATA_INICIO`='2015-01-16 17:40:38' WHERE (`ID`='6463');
UPDATE `equipamentosativos` SET `DATA_INICIO`='2014-11-12 10:42:53' WHERE (`ID`='5521');
UPDATE `equipamentosativos` SET `DATA_INICIO`='2014-11-12 10:45:36' WHERE (`ID`='5518');

-- Roolback Alice de Souza

UPDATE `equipamentosativos` SET `DATA_INICIO`='0000-00-00 00:00:00' WHERE (`ID`='6138');
UPDATE `equipamentosativos` SET `DATA_INICIO`='0000-00-00 00:00:00' WHERE (`ID`='6198');
UPDATE `equipamentosativos` SET `DATA_FIM`='0000-00-00 00:00:00' WHERE (`ID`='1959');
UPDATE `equipamentosativos` SET `DATA_FIM`='0000-00-00 00:00:00' WHERE (`ID`='1959');
UPDATE `equipamentosativos` SET `DATA_FIM`='0000-00-00 00:00:00' WHERE (`ID`='5500');
UPDATE `equipamentosativos` SET `DATA_INICIO`='0000-00-00 00:00:00' WHERE (`ID`='6463');
UPDATE `equipamentosativos` SET `DATA_INICIO`='0000-00-00 00:00:00' WHERE (`ID`='5521');
UPDATE `equipamentosativos` SET `DATA_INICIO`='0000-00-00 00:00:00' WHERE (`ID`='5518');

-- Tereza Maria
UPDATE `equipamentosativos` SET `DATA_FIM`='2014-02-25 11:22:04' WHERE (`ID`='2522');
UPDATE `equipamentosativos` SET `DATA_FIM`='2014-02-25 11:23:09' WHERE (`ID`='2521');
UPDATE `equipamentosativos` SET `DATA_FIM`='2014-12-31 11:24:05' WHERE (`ID`='2520');
UPDATE `equipamentosativos` SET `DATA_FIM`='2014-12-31 11:24:39' WHERE (`ID`='2711');
UPDATE `equipamentosativos` SET `DATA_FIM`='2014-12-31 11:25:14' WHERE (`ID`='2518');
UPDATE `equipamentosativos` SET `DATA_FIM`='2014-12-31 11:25:42' WHERE (`ID`='2709');
UPDATE `equipamentosativos` SET `DATA_FIM`='2014-12-31 11:27:20' WHERE (`ID`='2707');

-- Rollback Tereza Maria 
UPDATE `equipamentosativos` SET `DATA_FIM`='000-00-00 00:00:00' WHERE (`ID`='2522');
UPDATE `equipamentosativos` SET `DATA_FIM`='000-00-00 00:00:00' WHERE (`ID`='2521');
UPDATE `equipamentosativos` SET `DATA_FIM`='000-00-00 00:00:00' WHERE (`ID`='2520');
UPDATE `equipamentosativos` SET `DATA_FIM`='000-00-00 00:00:00' WHERE (`ID`='2711');
UPDATE `equipamentosativos` SET `DATA_FIM`='000-00-00 00:00:00' WHERE (`ID`='2518');
UPDATE `equipamentosativos` SET `DATA_FIM`='000-00-00 00:00:00' WHERE (`ID`='2709');
UPDATE `equipamentosativos` SET `DATA_FIM`='000-00-00 00:00:00' WHERE (`ID`='2707');

-- Silvia Regina
INSERT INTO `equipamentosativos` (`ID`, `COBRANCA_PLANOS_ID`, `CAPMEDICA_ID`, `QTD`, `OBS`, `DATA_INICIO`, `DATA_FIM`, `USUARIO`, `PRESCRICAO_AVALIACAO`, `PRESCRICAO_ID`, `PACIENTE_ID`, `DATA`, `TROCADO_POR`, `JUSTIFICATIVA`, `FATURADO`, `REFATURADO`) 
VALUES 
                                 ('', 35, 89, 1, 'SM-666', '2014-12-31 0000-00-00', '0000-0-0 00:00:00', 60, '', 2000, 8, '2014-12-31 0000-00-00', 0, '', 'N', 0),
								 ('', 33, 89, 1, 'SM-666', '2014-12-31 0000-00-00', '0000-0-0 00:00:00', 60, '', 2000, 8, '2014-12-31 0000-00-00', 0, '', 'N', 0),
								 ('', 13, 89, 1, 'SM-666', '2014-12-31 0000-00-00', '0000-0-0 00:00:00', 60, '', 2000, 8, '2014-12-31 0000-00-00', 0, '', 'N', 0),
								 ('', 12, 89, 1, 'SM-666', '2014-12-31 0000-00-00', '0000-0-0 00:00:00', 60, '', 2000, 8, '2014-12-31 0000-00-00', 0, '', 'N', 0),
								 ('', 2, 89, 1, 'SM-666', '2014-12-31 0000-00-00', '0000-0-0 00:00:00', 60, '', 2000, 8, '2014-12-31 0000-00-00', 0, '', 'N', 0);
								 
-- ROLLBACK Silvia Regina
Delete from equipamentosativos where ID in (6542, 6541, 6540, 6539, 6538);

-- Francisca lourdes
UPDATE `equipamentosativos` SET `DATA_FIM`='2014-12-31 12:13:51' WHERE (`ID`='2529')

--ROLLBACK Francisca lourdes
UPDATE `equipamentosativos` SET `DATA_FIM`='0000-00-00 00:00:00' WHERE (`ID`='2529')

--Joana Batisata
UPDATE `equipamentosativos` SET `DATA_FIM`='2015-01-09 12:18:14' WHERE (`ID`='6185');
UPDATE `equipamentosativos` SET `DATA_FIM`='2015-01-09 12:19:42' WHERE (`ID`='6190');
UPDATE `equipamentosativos` SET `DATA_FIM`='2015-01-09 12:20:38' WHERE (`ID`='6189');
UPDATE `equipamentosativos` SET `DATA_FIM`='2015-01-09 12:21:10' WHERE (`ID`='6188');
UPDATE `equipamentosativos` SET `DATA_FIM`='2015-01-09 12:23:20' WHERE `ID` in (6187,6197,6196,6195,6194,6193,6192,6191,6186)

--Roolback Joana Batista

UPDATE `equipamentosativos` SET `DATA_FIM`='0000-00-00 00:00:00' WHERE (`ID`='6185');
UPDATE `equipamentosativos` SET `DATA_FIM`='0000-00-00 00:00:00' WHERE (`ID`='6190');
UPDATE `equipamentosativos` SET `DATA_FIM`='0000-00-00 00:00:00' WHERE (`ID`='6189');
UPDATE `equipamentosativos` SET `DATA_FIM`='0000-00-00 00:00:00' WHERE (`ID`='6188');
UPDATE `equipamentosativos` SET `DATA_FIM`='0000-00-00 00:00:00' WHERE `ID` in (6187,6197,6196,6195,6194,6193,6192,6191,6186)