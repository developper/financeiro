ALTER TABLE `statuspaciente`
ADD COLUMN `solicitacao_recolhimento_equipamento`  enum('N','S') NULL DEFAULT 'N' AFTER `status`;

update
statuspaciente
set
solicitacao_recolhimento_equipamento = 'S'
WHERE
id in (5,6,7,8)
