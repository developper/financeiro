create table retencoes_vencimentos
(
    id int auto_increment,
    retencao enum('iss', 'ir', 'ipi', 'icms', 'pis', 'cofins', 'csll', 'inss') not null,
    tipo enum('desembolso', 'recebimento') not null,
    vencimento int not null,
    constraint retencoes_vencimentos_pk
        primary key (id)
)
    comment 'Tabela que registro o dia do mês do vencimento das retenções';

INSERT INTO retencoes_vencimentos (id, retencao, tipo, vencimento) VALUES
(NULL, 'iss', 'desembolso', '0'),
(NULL, 'ir', 'desembolso', '0'),
(NULL, 'ipi', 'desembolso', '0'),
(NULL, 'icms', 'desembolso', '0'),
(NULL, 'pis', 'desembolso', '0'),
(NULL, 'cofins', 'desembolso', '0'),
(NULL, 'csll', 'desembolso', '0'),
(NULL, 'inss', 'desembolso', '0'),
(NULL, 'iss', 'recebimento', '0'),
(NULL, 'ir', 'recebimento', '0'),
(NULL, 'ipi', 'recebimento', '0'),
(NULL, 'icms', 'recebimento', '0'),
(NULL, 'pis', 'recebimento', '0'),
(NULL, 'cofins', 'recebimento', '0'),
(NULL, 'csll', 'recebimento', '0'),
(NULL, 'inss', 'recebimento', '0');

