CREATE TABLE `files_uploaded` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `full_path` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `mimetype` varchar(25) NOT NULL DEFAULT '',
  `size_bytes` varchar(50) NOT NULL DEFAULT '',
  `uploaded_by` int(10) NOT NULL,
  `uploaded_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `uploaded_by` (`uploaded_by`),
  CONSTRAINT `files_uploaded_ibfk_1` FOREIGN KEY (`uploaded_by`) REFERENCES `usuarios` (`idUsuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `quadro_feridas_enfermagem` ADD `FILE_UPLOADED_ID` INT(11) UNSIGNED  NULL  DEFAULT NULL;
ALTER TABLE `quadro_feridas_enfermagem` ADD FOREIGN KEY (`FILE_UPLOADED_ID`) REFERENCES `files_uploaded` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `quadro_feridas_enfermagem` ADD `OBSERVACAO_IMAGEM` VARCHAR(255)  NULL  DEFAULT NULL;

CREATE TABLE `feridas_relatorio_prorrogacao_enf_imagem` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ferida_relatorio_prorrogacao_enf_id` int(10) NOT NULL,
  `file_uploaded_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ferida_relatorio_prorrogacao_enf_id` (`ferida_relatorio_prorrogacao_enf_id`),
  KEY `file_uploaded_id` (`file_uploaded_id`),
  CONSTRAINT `feridas_relatorio_prorrogacao_enf_imagem_ibfk_2` FOREIGN KEY (`file_uploaded_id`) REFERENCES `files_uploaded` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `feridas_relatorio_prorrogacao_enf_imagem_ibfk_1` FOREIGN KEY (`ferida_relatorio_prorrogacao_enf_id`) REFERENCES `feridas_relatorio_prorrogacao_enf` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

