SELECT
concat(c.principio,' ',c.apresentacao) as Item,
c.lab_desc as Laboratorio,
(CASE c.tipo
	WHEN '0' THEN 'Medicamento'
	WHEN '1' THEN 'Material'
	WHEN '3' THEN 'Dieta' END) as Tipo,
c.valorMedio as custo
from
catalogo as c
WHERE
c.valorMedio = 0
and ATIVO = 'A'
ORDER BY Tipo, Item ASC