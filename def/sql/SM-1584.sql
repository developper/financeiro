-- faturas que tem uma parte ou seu total cobrados como ID

SELECT
fa.ID as IdFatura,
c.idClientes,
UPPER(c.nome) as PACIENTE,
DATE_FORMAT(fa.DATA_INICIO,'%d/%m/%Y') as INICIO,
DATE_FORMAT(fa.DATA_FIM,'%d/%m/%Y') as FIM,
DATEDIFF(fa.DATA_FIM,fa.DATA_INICIO) + 1 as QUANTIDADE_DIAS_FATURADOS,
SUM(f.QUANTIDADE) as QUANTIDADE_DIAS_ID,
DATEDIFF(fa.DATA_FIM,fa.DATA_INICIO) + 1 - SUM(f.QUANTIDADE) as QUANTIDADE_DIAS_AD,
UPPER(e.nome) as UR
	FROM
		faturamento AS f
	INNER JOIN cobrancaplanos B ON(f.CATALOGO_ID = B.id)
	INNER JOIN fatura AS fa ON(f.FATURA_ID = fa.id)
	INNER JOIN clientes as c on  ( fa.PACIENTE_ID = c.idClientes)
  INNER JOIN empresas as e on (fa.UR = e.id)
	WHERE
	f.TIPO = 2
	AND fa.CANCELADO_POR IS NULL
	AND f.TABELA_ORIGEM = 'cobrancaplano'
	AND f.CANCELADO_POR IS NULL
	AND YEAR(fa.DATA_INICIO)= 2016
	AND YEAR(fa.DATA_FIM)= 2016
AND B.id in (20, 21, 51, 60, 61,77)
AND fa.ORCAMENTO = 0
AND c.idClientes not in (142,67,69,128,434,220,263,112,635,101,110)
GROUP BY fa.ID
UNION
SELECT
fa.ID as IdFatura,
c.idClientes,
UPPER(c.nome) as pACIENTE,
DATE_FORMAT(fa.DATA_INICIO,'%d/%m/%Y') as INICIO,
DATE_FORMAT(fa.DATA_FIM,'%d/%m/%Y') as FIM,
DATEDIFF(fa.DATA_FIM,fa.DATA_INICIO) + 1 as QUANTIDADE_DIAS_FATURADOS,
SUM(f.QUANTIDADE) as QUANTIDADE_DIAS_ID,
DATEDIFF(fa.DATA_FIM,fa.DATA_INICIO) + 1 - SUM(f.QUANTIDADE) as QUANTIDADE_DIAS_AD,
UPPER(e.nome) as UR

FROM
	faturamento AS f
INNER JOIN valorescobranca B ON(f.CATALOGO_ID = B.id)
INNER JOIN fatura AS fa ON(f.FATURA_ID = fa.id)
INNER JOIN clientes as c on  ( fa.PACIENTE_ID = c.idClientes)
INNER JOIN empresas as e on (fa.UR = e.id)
WHERE
f.TIPO = 2
AND fa.CANCELADO_POR IS NULL
AND f.TABELA_ORIGEM = 'valorescobranca'
AND f.CANCELADO_POR IS NULL
AND YEAR(fa.DATA_INICIO)= 2016
AND YEAR(fa.DATA_FIM)= 2016
AND B.idCobranca in (20, 21, 51, 60, 61,77)
AND fa.ORCAMENTO = 0
AND c.idClientes not in (142,67,69,128,434,220,263,112,635,101,110)
GROUP BY fa.ID
ORDER BY PACIENTE, INICIO


-- você deve pegar os ids encontrados nas faturas de Ids e colocar no not in dessa consulta
SELECT
fa.ID as IdFatura,
c.idClientes,
UPPER(c.nome) as PACIENTE,
DATE_FORMAT(fa.DATA_INICIO,'%d/%m/%Y') as INICIO,
DATE_FORMAT(fa.DATA_FIM,'%d/%m/%Y') as FIM,
DATEDIFF(fa.DATA_FIM,fa.DATA_INICIO) + 1 as QUANTIDADE_DIAS_FATURADOS,
0 as QUANTIDADE_DIAS_ID,
DATEDIFF(fa.DATA_FIM,fa.DATA_INICIO) + 1 as QUANTIDADE_DIAS_AD,
UPPER(e.nome) as UR
	FROM

	 fatura AS fa
	INNER JOIN clientes as c on  ( fa.PACIENTE_ID = c.idClientes)
  INNER JOIN empresas as e on (fa.UR = e.id)

	WHEREfa.CANCELADO_POR IS NULL


	AND YEAR(fa.DATA_INICIO)= 2016
	AND YEAR(fa.DATA_FIM)= 2016
AND fa.ID not in (

-- coloca a lista os Ids das faturas encontrado na primeira consulta.

					)
AND fa.ORCAMENTO = 0
AND c.idClientes not in (142,67,69,128,434,220,263,112,635,101,110)
GROUP BY fa.ID