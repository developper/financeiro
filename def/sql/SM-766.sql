ALTER TABLE `relatorio_prorrogacao_med`
ADD COLUMN `PRORROGACAO_ORIGEM`  int(11) NOT NULL DEFAULT 0 AFTER `ALTERACAO_SERVICO`,
ADD COLUMN `EDITADO_POR`  int(11) NOT NULL DEFAULT 0 AFTER `PRORROGACAO_ORIGEM`,
ADD COLUMN `STATUS`  enum('s','n') NOT NULL DEFAULT 's'
COMMENT 'S = Qualquer relatório / N = Inativado por edição (Quando editado, o relatório é inativado, mantendo-se o histórico e sendo criado um novo)'
AFTER `EDITADO_POR`;