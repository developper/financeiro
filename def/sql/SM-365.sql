
SET @DATA_INICIO = '2014-06-01';
SET @DATA_FIM = '2014-07-31'; 


SELECT 
c.nome as Paciente,
concat(date_format(p.inicio,'%d/%m/%Y'),' - ',date_format(p.inicio,'%d/%m/%Y')) as Periodo,
(select nome from usuarios where idUsuarios=p.criador) as Medico,

concat(b.principio,' ',b.apresentacao) as Item ,
u.nome as Enfermeiro,
s.qtd as Quantidade
 

FROM

`solicitacoes` as s 

inner join catalogo as b on (b.numero_tiss = s.`NUMERO_TISS`)

inner join clientes as c on (c.idClientes = s. paciente)

inner join empresas as e on (e.id = c.empresa )

inner join usuarios as u on (u.idUsuarios = s.enfermeiro)
inner join prescricoes as p on (s.idPrescricao=p.id)

WHERE 

p.Carater = 3 and

s.`DATA_SOLICITACAO` between @DATA_INICIO  and @DATA_FIM

and p.DATA_DESATIVADA = '0000-00-00 00:00:00' 


and s.status in (1,2)

and s.enfermeiro not in (60,48,122) and s.paciente not in (142,220,69,263,112,101,67)

and c.empresa in (1,2)
order by s.`DATA_SOLICITACAO`