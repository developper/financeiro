CREATE TABLE `historico_cancelar_item_pedido_interno_enviado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `saida_id` int(11) DEFAULT NULL,
  `quantidade` int(11) DEFAULT NULL COMMENT 'Quantidade cancelada.',
  `cancelado_por` int(11) DEFAULT NULL,
  `cancelado_em` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `historico_cancelar_item_pedido_interno_enviado`
ADD COLUMN `pedido_interno_id`  int(11) NULL AFTER `cancelado_em`,
ADD COLUMN `item_pedido_interno_id`  int(11) NULL AFTER `pedido_interno_id`;