USE mysql;

DROP DATABASE IF EXISTS sismederi;

DROP USER 'sismederi'@'localhost';

CREATE DATABASE sismederi DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE sismederi;

CREATE USER 'sismederi'@'localhost' IDENTIFIED BY 'sismederi';

GRANT all privileges on sismederi.* to 'sismederi'@'localhost';

GRANT SELECT ON `mysql`.`proc` TO 'sismederi'@'localhost'; 

