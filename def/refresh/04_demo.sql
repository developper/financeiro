INSERT INTO `empresas` (`nome`, `percentual`, `ATIVO`, `SIGLA_EMPRESA`, `centro_custo_dominio`, `compra_automatica_central`, `logo`)
VALUES
 ('Mederi Central', 100, 'S', 'CENTRAL', 1, 'N', 'padrao.jpg'),
 ('Mederi Bsb', 50, 'S', 'BSB', 5, 'N', 'padrao.jpg'),
 ('Opção Brasília', 50, 'S', '', 0, 'N', 'opcao.jpg');

INSERT INTO `usuarios` (`login`, `senha`, `nome`, `adm`, `tipo`, `empresa`, `modadm`, `modaud`, `modenf`, `modfin`, `modlog`, `modmed`, `modrem`, `modnutri`, `modfat`, `email`, `contato1`, `contato2`, `LOGADO`, `ADM_UR`, `ASSINATURA`, `block_at`)
VALUES
  ('admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrador', 1, 'administrativo', 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 1, 0, '', '9999-12-31');
 
INSERT INTO `planosdesaude` (`id`, `nome`, `registro_ans`, `ATIVO`)
VALUES
  (1, 'PARTICULAR', NULL, 'S');

INSERT INTO `operadoras` (`ID`, `NOME`, `EMAIL`)
VALUES
  (1, 'PARTICULAR', NULL);

INSERT INTO `operadoras_planosdesaude` (`OPERADORAS_ID`, `PLANOSDESAUDE_ID`)
VALUES
  (1, 1);

INSERT INTO `clientes` (`nome`, `nascimento`, `sexo`, `codigo`, `convenio`, `dataInternacao`, `localInternacao`, `diagnostico`, `medicoSolicitante`, `espSolicitante`, `acompSolicitante`, `empresa`, `responsavel`, `relresp`, `rsexo`, `cuidador`, `relcuid`, `csexo`, `endereco`, `bairro`, `referencia`, `status`, `LEITO`, `TEL_POSTO`, `PES_CONTATO_POSTO`, `TEL_DOMICILIAR`, `TEL_LOCAL_INTERNADO`, `TEL_RESPONSAVEL`, `TEL_CUIDADOR`, `END_INTERNADO`, `BAI_INTERNADO`, `REF_ENDERECO_INTERNADO`, `CUIDADOR_ESPECIALIDADE`, `RESPONSAVEL_ESPECIALIDADE`, `CIDADE_ID_UND`, `CIDADE_ID_INT`, `CIDADE_ID_DOM`, `END_UND_ORIGEM`, `NUM_MATRICULA_CONVENIO`, `NOME_PLANO_CONVENIO`, `LIMINAR`, `DATA_CADASTRO`, `PORCENTAGEM`, `cod_opcao_home_care`)
VALUES
 ('Huguinho', '2012-01-01', 0, 'hcssa-001', 1, '2012-01-30 00:00:00', 'Sta. Isabel', 'I64', 'dr. antonio banderas', 'neurologia', 's', 1, 'margarida', '10', 1, 'pato donald', '10', 0, 'rua espanha, 66', 'santa mônica', 'mederi', 1, '', '', ',', '', '', '555555', '555555', 'rua espanha, 66', 'santa mônica', 'mederi', '', '', 576, 576, 576, ' ,', ',', NULL, 0, '0000-00-00 00:00:00', 100.00, NULL);

INSERT INTO `plano_contas_contabil` (`ID`, `CODIGO_DOMINIO`, `N1`, `COD_N2`, `N2`, `COD_N3`, `N3`, `COD_N4`, `N4`, `COD_N5`, `N5`)
VALUES
  (1, 0, 'A', '1.1', 'ATIVO CIRCULANTE', '1.1.01', 'DISPONÍVEL', '1.1.01.01', 'CAIXA', '1.1.01.01.001', 'CAIXA MATRIZ');

INSERT INTO `fornecedores` (`idFornecedores`, `nome`, `razaoSocial`, `cnpj`, `contato`, `telefone`, `email`, `endereco`, `CIDADE_ID`, `IES`, `IM`, `ATIVO`, `TIPO`, `CPF`, `FISICA_JURIDICA`, `IPCC`)
VALUES
  (1, 'Fornecedor de Teste', 'Fornecedor de Teste Ltda.', '00.000.000/0000-00', 'Teste', '(75)9999-0987', 'fornecedor@teste.com.br', 'Rua dos Testes, 123456 - Sta. Teste', 576, '1234567', '1234567', '', 'F', '', 1, 1);
