USE mysql;

CREATE DATABASE sismederi DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE sismederi;

CREATE USER 'sismederi'@'localhost' IDENTIFIED BY 'mysqlpwd';

GRANT all privileges on sismederi.* to 'sismederi'@'localhost';
