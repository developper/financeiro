/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LOG_BRASINDICE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TISS` varchar(11) DEFAULT NULL,
  `USUARIO` int(11) DEFAULT NULL,
  `DATA` datetime DEFAULT NULL,
  `ACAO` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MATERIAIS_TEMP` (
  `SIMPRO` varchar(13) DEFAULT NULL,
  `ITENS` varchar(99) DEFAULT NULL,
  `EMPRESA` varchar(19) DEFAULT NULL,
  `ID_REFERENTE` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agenda` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MSG_SMS` varchar(160) DEFAULT NULL,
  `ACAO_DISPARO` varchar(50) DEFAULT NULL,
  `DATA_AGENDA` datetime DEFAULT NULL,
  `DATA_AGENDA_ATIVO` tinyint(1) DEFAULT NULL,
  `AGENDA_GRUPO_ID` int(11) NOT NULL,
  `AGENDA_FREQUENCIA_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_agenda_agenda_grupo1` (`AGENDA_GRUPO_ID`),
  KEY `fk_agenda_agenda_frequencia1` (`AGENDA_FREQUENCIA_ID`),
  CONSTRAINT `fk_agenda_agenda_frequencia1` FOREIGN KEY (`AGENDA_FREQUENCIA_ID`) REFERENCES `agenda_frequencia` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_agenda_agenda_grupo1` FOREIGN KEY (`AGENDA_GRUPO_ID`) REFERENCES `agenda_grupo` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agenda_cel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CEL` varchar(45) DEFAULT NULL,
  `NOME` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agenda_frequencia` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FREQUENCIA` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agenda_grupo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GRUPO` varchar(45) DEFAULT NULL,
  `EMPRESA_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agenda_grupo_cel` (
  `AGENDA_GRUPO_ID` int(11) NOT NULL,
  `AGENDA_CEL_ID` int(11) NOT NULL,
  KEY `fk_agenda_grupo_has_cel_agenda_agenda_grupo` (`AGENDA_GRUPO_ID`),
  KEY `fk_agenda_grupo_cel_agenda_cel1` (`AGENDA_CEL_ID`),
  CONSTRAINT `fk_agenda_grupo_cel_agenda_cel1` FOREIGN KEY (`AGENDA_CEL_ID`) REFERENCES `agenda_cel` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_agenda_grupo_has_cel_agenda_agenda_grupo` FOREIGN KEY (`AGENDA_GRUPO_ID`) REFERENCES `agenda_grupo` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alergia_alimentar` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PACIENTE_ID` int(11) DEFAULT NULL,
  `USUARIO_ID` int(11) DEFAULT NULL,
  `ALIMENTO` varchar(100) DEFAULT NULL,
  `DATA` datetime DEFAULT NULL,
  `ATIVO` tinyint(4) DEFAULT '1' COMMENT '1-ATIVO 2-INATIVO',
  `ORIGEM` int(11) DEFAULT NULL COMMENT '1-AVALIAÇÃO MÉDICA 2-EVOLUÇÃO MÉDICA 3-AVALIAÇÃO ENF 4-EVOLUÇÃO ENF',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alergia_medicamentosa` (
  `ID` int(11) NOT NULL,
  `ID_PACIENTE` int(11) NOT NULL,
  `NUMERO_TISS` varchar(11) NOT NULL,
  `CATALOGO_ID` int(11) NOT NULL,
  `DESCRICAO` varchar(255) NOT NULL,
  `ATIVO` char(1) NOT NULL,
  `DATA` datetime NOT NULL,
  `USUARIO_ID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alterar_servicos_cliente` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `COD_SERVICO` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `TIPO` tinyint(2) NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  `ORIGEM` tinyint(2) NOT NULL COMMENT 'GRAVA ORIGEM DA ALTERACAO DO SERVICO. 1-EVOLUCAO, 2-PRORROGACAO',
  `ID_ORIGEM` int(11) NOT NULL,
  `PACIENTE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aplicacaofundos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `forma` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `forma` (`forma`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela referente ao campo "Forma de Pagamento" das notas. ';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assinaturas` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRICAO` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atualizacoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dia` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabela com as atualizações do sistema.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avaliacao_enf_dor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AVALIACAO_ENF_ID` int(11) NOT NULL,
  `LOCAL` varchar(20) NOT NULL,
  `INTENSIDADE` int(11) NOT NULL,
  `PADRAO` tinyint(2) NOT NULL,
  `PADRAO_ID` tinyint(2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avaliacao_enf_dreno` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AVALIACAO_ENF_ID` int(11) NOT NULL,
  `DRENO` varchar(50) CHARACTER SET utf8 NOT NULL,
  `LOCAL` varchar(25) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avaliacao_enf_ferida` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AVALIACAO_ENF_ID` int(11) NOT NULL,
  `LOCAL` varchar(25) CHARACTER SET utf8 NOT NULL,
  `CARACTERISTICA` varchar(50) CHARACTER SET utf8 NOT NULL,
  `TIPO_CURATIVO` varchar(50) CHARACTER SET utf8 NOT NULL,
  `FREQUENCIA_TROCA_CURATIVO` varchar(20) NOT NULL,
  `LADO` varchar(20) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avaliacao_enf_medicamento` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AVALIACAO_ENF_ID` int(11) NOT NULL,
  `MEDICACAO` varchar(50) NOT NULL,
  `APRESENTACAO` varchar(50) NOT NULL,
  `VIA` varchar(50) NOT NULL,
  `POSOLOGIA` varchar(10) NOT NULL,
  `CUSTEADO_FAMILIA` int(1) NOT NULL COMMENT 'RECEBE1-SIM',
  `FREQUENCIA` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`),
  KEY `ID_2` (`ID`),
  KEY `ID_3` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avaliacao_enf_suplemento` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AVALIACAO_ENF_ID` int(11) NOT NULL,
  `NOME` varchar(40) CHARACTER SET utf8 NOT NULL,
  `FREQUENCIA` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avaliacao_enf_topicos` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AVALIACAO_ENF_ID` int(11) NOT NULL,
  `LOCAL` varchar(50) CHARACTER SET utf8 NOT NULL,
  `FREQUENCIA` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avaliacaoenfermagem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USUARIO_ID` int(11) NOT NULL,
  `PACIENTE_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `CLINICO` int(1) NOT NULL,
  `CIRURGICO` int(1) NOT NULL,
  `PALIATIVOS` int(1) NOT NULL,
  `DIAGNOSTICO_PRINCIPAL` text CHARACTER SET utf8 NOT NULL,
  `HIST_PREGRESSA` text CHARACTER SET utf8 NOT NULL,
  `HIST_FAMILIAR` text CHARACTER SET utf8 NOT NULL,
  `QUEIXA_ATUAL` text CHARACTER SET utf8 NOT NULL,
  `HIST_ATUAL` text CHARACTER SET utf8 NOT NULL,
  `EST_GERAL_BOM` int(1) NOT NULL,
  `EST_GERAL_RUIM` int(1) NOT NULL,
  `NUTRIDO` int(1) NOT NULL,
  `DESNUTRIDO` int(1) NOT NULL,
  `HIGIENE_CORPORAL_SATISFATORIA` int(1) NOT NULL,
  `HIGIENE_CORPORAL_INSATISFATORIA` int(1) NOT NULL,
  `HIGIENE_BUCAL_SATISFATORIA` int(1) NOT NULL,
  `HIGIENE_BUCAL_INSATISFATORIA` int(1) NOT NULL,
  `ESTADO_EMOCIONAL` int(1) NOT NULL COMMENT 'RECEBE 1-CALMO, 2-AGITADO, 3-DEPRIMIDO, 4-CHOROSO',
  `CONCIENTE` int(1) NOT NULL,
  `SEDADO` int(1) NOT NULL,
  `OBNUBILADO` int(1) NOT NULL,
  `TORPOROSO` int(1) NOT NULL,
  `COMATOSO` int(1) NOT NULL,
  `ORIENTADO` int(1) NOT NULL,
  `DESORIENTADO` int(1) NOT NULL,
  `SONOLENTO` int(1) NOT NULL,
  `COND_MOTORA` char(1) NOT NULL COMMENT 'Recebe: 1 - deambula, 2 - deambula c/auxilio, 3 - cadeirante e 4  acamado.',
  `ALERGIA_MEDICAMENTO` char(1) NOT NULL COMMENT 'rECEBE S-SIM, N-NÃO',
  `ALERGIA_ALIMENTO` char(1) NOT NULL COMMENT 'RECEBE 1-SIM, 2-NÃO',
  `ALERGIA_MEDICAMENTO_TIPO` text CHARACTER SET utf8 NOT NULL,
  `ALERGIA_ALIMENTO_TIPO` text CHARACTER SET utf8 NOT NULL,
  `PELE_INTEGRA` int(1) NOT NULL,
  `PELE_LESAO` int(1) NOT NULL COMMENT 'Recebe: 1 - SIM, 2 - NÃO',
  `PELE_HIDRATADA` int(1) NOT NULL,
  `PELE_DESIDRATADA` int(11) NOT NULL,
  `PELE_ICTERICA` int(1) NOT NULL,
  `PELE_ICTERICA_ESCALA` int(11) NOT NULL,
  `PELE_ANICTERICA` int(1) NOT NULL,
  `PELE_PALIDA` int(1) NOT NULL COMMENT 'RECEBE 1-SIM, 2- NÃO',
  `PELE_SUDOREICA` int(1) NOT NULL COMMENT 'RECEBE 1-SIM, 2- NÃO',
  `PELE_FRIA` int(1) NOT NULL COMMENT 'RECEBE 1-SIM, 2- NÃO',
  `PELE_EDEMA` int(1) NOT NULL COMMENT 'Recebe: 1 - SIM, 2 - NÃO',
  `PELE_DESCAMACAO` int(1) NOT NULL COMMENT 'RECEBE 1-SIM, 2- NÃO',
  `T_ASSIMETRICO` int(1) NOT NULL,
  `T_SIMETRICO` int(1) NOT NULL,
  `EUPNEICO` int(1) NOT NULL,
  `DISPNEICO` int(1) NOT NULL,
  `TAQUIPNEICO` int(1) NOT NULL,
  `MVBD` int(1) NOT NULL,
  `RONCOS` int(1) NOT NULL,
  `SIBILOS` int(1) NOT NULL,
  `CREPTOS` int(1) NOT NULL,
  `MV_DIMINUIDO` int(1) NOT NULL,
  `BCNF` int(1) NOT NULL,
  `NORMOCARDICO` int(1) NOT NULL,
  `TAQUICARDICO` int(1) NOT NULL,
  `BRADICARDICO` int(1) NOT NULL,
  `VENTILACAO` int(11) NOT NULL COMMENT 'VENTILACAO: 1 para Espontânea, 2 para traqueostomia metálica e 3 para Traqueostomia Plástica.',
  `VENT_TRAQUESTOMIA_METALICA_NUM` int(11) NOT NULL,
  `VENT_TRAQUESTOMIA_PLASTICA_NUM` int(11) NOT NULL,
  `BIPAP` int(1) NOT NULL COMMENT 'Recebe: 1 Continuo e 2 intermitente',
  `OXIGENIO_SN` char(1) CHARACTER SET utf8 NOT NULL COMMENT 'Recebe: 1 - SIM, 2 - NÃO',
  `OXIGENIO` char(1) CHARACTER SET utf8 NOT NULL COMMENT 'Oxigênio: 1 para Catéter de Oxigênio, 2 para Catéter tipo óculos,3 para respirador, 4 para Mascara de Ventury e 5 para Concentrador.',
  `VENT_CATETER_OXIGENIO_NUM` int(11) NOT NULL,
  `VENT_CATETER_OXIGENIO_QUANT` int(11) NOT NULL,
  `VENT_CATETER_OCULOS` int(11) NOT NULL,
  `VENT_CATETER_OCULOS_NUM` int(11) NOT NULL,
  `VENT_RESPIRADOR` int(11) NOT NULL,
  `VENT_RESPIRADOR_TIPO` int(11) NOT NULL,
  `VENT_MASCARA_VENTURY` int(1) NOT NULL,
  `VENT_MASCARA_VENTURY_NUM` int(5) NOT NULL,
  `VENT_CONCENTRADOR` int(1) NOT NULL,
  `VENT_CONCENTRADOR_NUM` int(1) NOT NULL,
  `ALT_ASPIRACOES` varchar(1) NOT NULL,
  `ASPIRACAO_NUM` int(11) NOT NULL,
  `ASPIRACAO_SECRECAO` char(1) NOT NULL COMMENT 'RECEBE 1-AUSENTE, 2 PRESENTE',
  `ASPIRACAO_SECRECAO_CARACTERISTICA` int(1) NOT NULL COMMENT 'RECEBE 1-CLARA, 2- ESPESSA, 3- AMARELADA, 4-ESVERDEADA,  5-SANGUINOLENTA',
  `PLANO` int(1) NOT NULL,
  `FLACIDO` int(1) NOT NULL,
  `DISTENDIDO` int(1) NOT NULL,
  `TIMPANICO` int(1) NOT NULL,
  `RHA` char(1) NOT NULL COMMENT 'Recebe: A de Ausente e P de Presente',
  `INDO_PALPACAO` int(1) NOT NULL,
  `DOLOROSO` int(1) NOT NULL,
  `DIETA_SN` char(1) CHARACTER SET utf8 NOT NULL COMMENT 'Recebe: 1 - SIM, 2 - NÃO',
  `VIA_ALIMENTACAO_ORAL` int(1) NOT NULL,
  `VIA_ALIMENTACAO_PARENTAL` int(1) NOT NULL,
  `VIA_ALIMENTACAO_VISIVEL` int(1) NOT NULL,
  `VIA_ALIMENTACAO_PARENTAL_NUM` int(1) NOT NULL,
  `VIA_ALIMENTACAO_SNE` int(1) NOT NULL,
  `VIA_ALIMENTACAO_SNE_NUM` int(1) NOT NULL,
  `VIA_ALIMENTACAO_GTM` int(11) NOT NULL,
  `VIA_ALIMENTACAO_GTM_NUM` int(11) NOT NULL,
  `VIA_ALIMENTACAO_GMT_PROFUNDIDADE` int(11) NOT NULL,
  `VIA_ALIMENTACAO_GTM_VISIVEL` char(11) CHARACTER SET utf8 NOT NULL,
  `VIA_ALIMENTACAO_SONDA_GASTRO` int(11) NOT NULL,
  `VIA_ALIMENTACAO_SONDA_GASTRO_NUM` int(11) NOT NULL,
  `DIETA_SIST_ABERTO` int(11) NOT NULL,
  `DIETA_SIST_ABERTO_DESCRICAO` varchar(50) NOT NULL,
  `DIETA_SIST_FECHADO` varchar(25) NOT NULL,
  `DIETA_SIST_FECHADO_DESCRICAO` varchar(50) NOT NULL,
  `DIETA_SIST_FECHADO_VAZAO` int(11) NOT NULL,
  `DIETA_SIST_ARTESANAL` int(11) NOT NULL,
  `DIETA_SUPLEMENTO_SN` char(11) NOT NULL COMMENT 'RECEBE S-SIM, N-NÃO',
  `DIETA_NPT` int(11) NOT NULL,
  `DIETA_NPT_DESC` varchar(25) NOT NULL,
  `DIETA_NPT_VAZAO` int(11) NOT NULL,
  `DIETA_NPP` int(11) NOT NULL,
  `DIETA_NPP_DESC` varchar(25) NOT NULL,
  `DIETA_NPP_VAZAO` int(11) NOT NULL,
  `INTEGRA` int(1) NOT NULL COMMENT 'Recebe: 1 - SIM, 2 - NÃO',
  `SECRECAO` int(1) NOT NULL COMMENT 'Recebe: 1 - SIM, 2 - NÃO',
  `SANGRAMENTO` int(1) NOT NULL COMMENT 'Recebe: 1 - SIM, 2 - NÃO',
  `PRURIDO` int(1) NOT NULL COMMENT 'Recebe: 1 - SIM, 2 - NÃO e 3 - S/A',
  `EDEMA` int(1) NOT NULL COMMENT 'Recebe: 1 - SIM, 2 - NÃO',
  `TOPICOS_SN` char(1) CHARACTER SET utf8 NOT NULL COMMENT 'RECEBE S-SIM, N-NÃO',
  `DISPOSITIVOS_INTRAVENOSOS` int(11) NOT NULL,
  `DISPO_INTRAVENOSO_CVC` int(11) NOT NULL COMMENT 'RECEBE 1-UNO LUMEN, 2-DUPLO-LUMEN,3 TRIPLO-LUMEN',
  `DISPO_INTRAVENOSO_UNO_LOCAL` varchar(50) CHARACTER SET utf8 NOT NULL,
  `DISPO_INTRAVENOSO_DUPLO_LOCAL` varchar(50) CHARACTER SET utf8 NOT NULL,
  `DISPO_INTRAVENOSO_TRIPLO_LOCAL` varchar(50) CHARACTER SET utf8 NOT NULL,
  `DISPO_INTRAVENOSO_AVP` int(1) NOT NULL,
  `DISPO_INTRAVENOSO_AVP_LOCAL` varchar(25) NOT NULL,
  `DISPO_INTRAVENOSO_PORTOCATH` int(1) NOT NULL,
  `DISPO_INTRAVENOSO_PORTOCATH_NUM` int(11) NOT NULL,
  `DISPO_INTRAVENOSO_PORTOCATH_LOCAL` varchar(25) CHARACTER SET utf8 NOT NULL,
  `DISPO_INTRAVENOSO_OUTROS` text CHARACTER SET utf8 NOT NULL,
  `FRALDA` int(1) NOT NULL,
  `FRALDA_UNID_DIA` int(1) NOT NULL,
  `FRALDA_TAMANHO` int(1) NOT NULL COMMENT 'RECEB 1-P, 2-M, 3-G,4-XG',
  `DISPO_URINARIO` int(1) NOT NULL,
  `DISPO_URINARIO_NUM` int(5) NOT NULL,
  `DISPO_SONDA_FOLEY` int(1) NOT NULL,
  `DISPO_SONDA_FOLEY_NUM` int(5) NOT NULL,
  `DISPO_SONDA_ALIVIO` int(1) NOT NULL,
  `DISPO_SONDA_ALIVIO_NUM` int(5) NOT NULL,
  `DISPO_COLOSTOMIA` int(1) NOT NULL,
  `DISPO_COLOSTOMIA_NUM` int(5) NOT NULL,
  `DISPO_ILIOSTOMIA` int(11) NOT NULL,
  `DISPO_ILIOSTOMIA_NUM` int(11) NOT NULL,
  `DISPO_CISTOSTOMIA` int(11) NOT NULL,
  `DISPO_CISTOSTOMIA_NUM` int(11) NOT NULL,
  `DRENOS` char(1) CHARACTER SET utf8 NOT NULL COMMENT 'RECEBE 1-SIM, 2- NÃO',
  `DRENOS_LOCAL` varchar(50) NOT NULL,
  `PA` varchar(20) NOT NULL,
  `PA_DISTOLICA` int(11) NOT NULL,
  `FC` int(5) NOT NULL,
  `FR` int(5) NOT NULL,
  `TEMPERATURA` int(5) NOT NULL,
  `SPO2` int(5) NOT NULL,
  `PAD_ENFERMAGEM` text NOT NULL,
  `DOR` char(1) NOT NULL COMMENT 'RECEBE 1-SIM,2-NÃO',
  `FERIDA` char(1) NOT NULL COMMENT 'RECEBE S-SIM,N-NÃO',
  `GLOBOSO` int(1) NOT NULL,
  `ASPIRACAO_SECRECAO_COR` int(1) NOT NULL COMMENT '1-clara, 3 - amarelada, 4 - esverdeada, 5 - samguinolenta',
  `CVC` int(11) NOT NULL COMMENT 'RECEBE 1-SIM, 2-NAO',
  `ELIMINACOES` int(11) NOT NULL COMMENT 'RECEBE 1-SIM, 2-NAO',
  `USO_SISTEMA_ABERTO` int(11) NOT NULL,
  `VENTILACAO_ESP_EQUIP` tinyint(1) NOT NULL,
  `PREJUSTIFICATIVA` text NOT NULL,
  `DISPO_INTRAVENOSO_CATETER_PICC` int(11) DEFAULT NULL,
  `DISPO_INTRAVENOSO_CATETER_PICC_LOCAL` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banco_fornecedor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRICAO` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brasindice` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `COD_LAB` varchar(9) DEFAULT NULL,
  `LABORATORIO` varchar(100) DEFAULT NULL,
  `COD_MED` int(11) DEFAULT NULL,
  `ITEM` varchar(200) DEFAULT NULL,
  `COD_APRE` varchar(11) DEFAULT NULL,
  `APRESENTACAO` varchar(200) DEFAULT NULL,
  `PRECO_MED` double(8,2) DEFAULT NULL,
  `QUANTIDADE` int(11) DEFAULT NULL,
  `FLAG` varchar(11) DEFAULT NULL,
  `PRECO_FRAC` int(11) DEFAULT NULL,
  `EDICAO` int(11) DEFAULT NULL,
  `IPI` double(4,2) DEFAULT NULL,
  `FLAG_PORTARIA_PIS_COFINS` char(1) DEFAULT NULL,
  `EAN_COD_BARRA` int(11) DEFAULT NULL,
  `TISS` int(11) DEFAULT NULL,
  `TUSS` int(11) DEFAULT NULL,
  `TIPO` enum('DIE','MED') CHARACTER SET tis620 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `capmedica` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `paciente` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `motivo` text COLLATE utf8_unicode_ci NOT NULL,
  `alergiamed` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `alergiaalim` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `tipoalergiamed` varchar(255) CHARACTER SET utf8 NOT NULL,
  `tipoalergiaalim` varchar(255) CHARACTER SET utf8 NOT NULL,
  `hosplocomocao` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `hosphigiene` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `hospcons` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `hospalimentacao` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `hospulcera` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `domlocomocao` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `domhigiene` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `domcons` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `domalimentacao` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `domulcera` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `objlocomocao` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `objhigiene` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `objcons` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `objalimentacao` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `objulcera` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `qtdinternacoes` int(10) unsigned NOT NULL,
  `historicointernacao` text COLLATE utf8_unicode_ci NOT NULL,
  `parecer` int(10) unsigned NOT NULL,
  `justificativa` text CHARACTER SET utf8 NOT NULL,
  `usuario` int(10) unsigned NOT NULL,
  `MODALIDADE` int(2) NOT NULL COMMENT 'Modalidade do paciente se vai ser AD recebe 1 caso seja ID recebe 2.',
  `LOCAL_AV` int(2) NOT NULL COMMENT 'Local de avaliação do paciente se Hospital recebe 2 se for em Domicilio ou Instituição de Apoio recebe 1.',
  `PRE_JUSTIFICATIVA` text CHARACTER SET utf8 NOT NULL,
  `PLANO_DESMAME` text CHARACTER SET utf8 NOT NULL,
  `EQUIPAMENTO_OBS` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `paciente` (`paciente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabela contendo as fichas de capitação médica';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `lab_desc` varchar(255) NOT NULL,
  `principio` varchar(255) NOT NULL,
  `apresentacao` varchar(255) NOT NULL,
  `DESC_MATERIAIS_FATURAR` varchar(100) DEFAULT NULL,
  `NUMERO_TISS` varchar(11) NOT NULL,
  `estoque` int(10) NOT NULL,
  `valorMedio` double unsigned NOT NULL,
  `tipo` tinyint(4) NOT NULL COMMENT 'Se 0 medicamento, se 1 material, se 3 Dieta',
  `AddMederi` int(11) NOT NULL DEFAULT '0' COMMENT '0 Se for do Brasindice e 1 se foi adicionado pela mederi',
  `status` int(11) NOT NULL COMMENT 'Nao esta send utilizada nesta tabela',
  `usuario` int(11) NOT NULL COMMENT 'Recebe o id do usuário que cadastrou um produto interno',
  `data` datetime NOT NULL COMMENT 'Data em que foi feito o registro',
  `valor` double(6,2) unsigned zerofill NOT NULL,
  `VALOR_VENDA` double(6,2) NOT NULL,
  `VALOR_FABRICA` double(6,2) NOT NULL,
  `ATIVO` char(1) NOT NULL COMMENT 'A para ativado e D para desativado',
  `TAG` varchar(40) NOT NULL,
  `CONTROLADO` char(1) NOT NULL DEFAULT 'N',
  `ANTIBIOTICO` char(1) NOT NULL DEFAULT 'N',
  `REFERENCIA` char(1) NOT NULL COMMENT 'Bpara Brasindice ou S paraSimpro',
  `PRINCIPIO_ATIVO` varchar(255) DEFAULT NULL,
  `GENERICO` char(1) DEFAULT NULL,
  `ATIVO_TEMP` char(1) NOT NULL DEFAULT 'A',
  `TUSS` int(11) DEFAULT NULL,
  `simpro_id` int(11) DEFAULT NULL,
  `brasindice_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabela contendo os medicamento e materiais';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catetersondaevolucao` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FICHA_EVOLUCAO_ID` int(11) NOT NULL,
  `DESCRICAO` varchar(100) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centrocustos` (
  `idCentroCustos` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idCentroCustos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabela com os centro de custos das notas.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cfop` (
  `ID_CFOP` int(11) NOT NULL DEFAULT '0',
  `DESCRICAO` longtext,
  `APLICACAO` longtext,
  PRIMARY KEY (`ID_CFOP`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checklist_enfermagem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PACIENTE_ID` int(11) DEFAULT NULL,
  `DATA` datetime DEFAULT NULL,
  `CRIADOR` int(11) DEFAULT NULL,
  `CATALOGO_ID` int(11) DEFAULT NULL,
  `QTD` int(11) DEFAULT NULL,
  `TIPO` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cid10` (
  `codigo` varchar(10) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `cid10` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabela contendo a lista CID10';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cidades` (
  `ID` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `ESTADO_ID` int(2) unsigned zerofill NOT NULL DEFAULT '00',
  `UF` varchar(4) NOT NULL DEFAULT '',
  `NOME` varchar(50) NOT NULL DEFAULT '',
  UNIQUE KEY `ID` (`ID`),
  KEY `ID_2` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classmedicamentos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classe` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Sem categoria',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela com a classe de medicamentos. Referente a prescrição';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `idClientes` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `nascimento` date NOT NULL,
  `sexo` tinyint(1) NOT NULL COMMENT 'Recebe 0 masculino, 1 feminino',
  `codigo` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `convenio` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dataInternacao` datetime NOT NULL,
  `localInternacao` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `diagnostico` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `medicoSolicitante` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `espSolicitante` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `acompSolicitante` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `empresa` int(10) unsigned NOT NULL DEFAULT '1',
  `responsavel` varchar(100) CHARACTER SET utf8 NOT NULL,
  `relresp` varchar(100) CHARACTER SET utf8 NOT NULL,
  `rsexo` tinyint(4) NOT NULL,
  `cuidador` varchar(100) CHARACTER SET utf8 NOT NULL,
  `relcuid` varchar(100) CHARACTER SET utf8 NOT NULL,
  `csexo` tinyint(4) NOT NULL,
  `endereco` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `bairro` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `referencia` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `status` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '1 - Novo, 2 - Pendente, 3 - Desfavorável, 4 - Ativo/Autorizado, 5 - Suspenso/Alta, 6 Óbito/Inativo',
  `LEITO` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `TEL_POSTO` varchar(15) CHARACTER SET utf8 NOT NULL,
  `PES_CONTATO_POSTO` varchar(50) CHARACTER SET utf8 NOT NULL,
  `TEL_DOMICILIAR` varchar(15) CHARACTER SET utf8 NOT NULL,
  `TEL_LOCAL_INTERNADO` varchar(15) CHARACTER SET utf8 NOT NULL,
  `TEL_RESPONSAVEL` varchar(15) CHARACTER SET utf8 NOT NULL,
  `TEL_CUIDADOR` varchar(15) CHARACTER SET utf8 NOT NULL,
  `END_INTERNADO` varchar(100) CHARACTER SET utf8 NOT NULL,
  `BAI_INTERNADO` varchar(30) CHARACTER SET utf8 NOT NULL,
  `REF_ENDERECO_INTERNADO` varchar(200) CHARACTER SET utf8 NOT NULL,
  `CUIDADOR_ESPECIALIDADE` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `RESPONSAVEL_ESPECIALIDADE` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `CIDADE_ID_UND` int(4) NOT NULL COMMENT 'Código da Cidade da Unidade',
  `CIDADE_ID_INT` int(4) NOT NULL COMMENT 'Código da Cidade de Internação',
  `CIDADE_ID_DOM` int(4) NOT NULL COMMENT 'Código da Cidade do Domicilio',
  `END_UND_ORIGEM` varchar(50) CHARACTER SET utf8 NOT NULL,
  `NUM_MATRICULA_CONVENIO` varchar(25) CHARACTER SET utf8 NOT NULL,
  `NOME_PLANO_CONVENIO` varchar(100) DEFAULT NULL,
  `LIMINAR` int(11) NOT NULL COMMENT '0-não e 1-sim',
  `DATA_CADASTRO` datetime NOT NULL COMMENT 'recebe data e hora que o paciente foi cadastrado no sistema',
  `PORCENTAGEM` float(8,2) NOT NULL,
  `cod_opcao_home_care` int(11) DEFAULT NULL,
  PRIMARY KEY (`idClientes`),
  KEY `empresa` (`empresa`),
  CONSTRAINT `clientes_ibfk_1` FOREIGN KEY (`empresa`) REFERENCES `empresas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela com o cadastro de pacientes.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cobrancaplanos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(255) NOT NULL,
  `unidade` varchar(100) NOT NULL,
  `tipo` int(2) NOT NULL COMMENT '0 - NAO APARECE O EQUIPAMENTO EM LUGAR ALGUM; 1 - EQUIPAMENTO ATIVO PARA MEDICO E ENFERMAGEM; 2 - APARECE NO FATURAMENTO NA ABA OUTROS',
  `CUSTO` double(6,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabela com os itens cobrados pelo plano de saúde.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comorbidades` (
  `capmed` int(10) unsigned NOT NULL,
  `cancer` char(1) NOT NULL,
  `psiquiatrico` char(1) NOT NULL,
  `neuro` char(1) NOT NULL,
  `glaucoma` char(1) NOT NULL,
  `hepatopatia` char(1) NOT NULL,
  `obesidade` char(1) NOT NULL,
  `cardiopatia` char(1) NOT NULL,
  `dm` char(1) NOT NULL,
  `reumatologica` char(1) NOT NULL,
  `has` char(1) NOT NULL,
  `nefropatia` char(1) NOT NULL,
  `pneumopatia` char(1) NOT NULL,
  KEY `capmed` (`capmed`),
  CONSTRAINT `comorbidades_ibfk_1` FOREIGN KEY (`capmed`) REFERENCES `capmedica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lista de comorbidades, utilizado na capitação médica.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comprovanteparcelas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idParcela` int(11) NOT NULL,
  `nome` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `content` mediumblob NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idParcela` (`idParcela`),
  CONSTRAINT `comprovanteparcelas_ibfk_1` FOREIGN KEY (`idParcela`) REFERENCES `parcelas` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabela com os comprovantes de pagamento.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conciliacao_bancaria` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONTAS_BANCARIAS_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL COMMENT 'o DIA EM QUE FOI FEITO A CONCILIACÃO',
  `SALDO_CONCILIADO` double(8,2) NOT NULL,
  `SALDO_NCONCILIADO` double(8,2) NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  `DATA_INICIO` date NOT NULL,
  `DATA_FIM` date NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contas_bancarias` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIPO_CONTA_BANCARIA_ID` int(11) DEFAULT NULL,
  `ORIGEM_FUNDOS_ID` int(11) NOT NULL,
  `AGENCIA` varchar(11) CHARACTER SET utf8 NOT NULL,
  `NUM_CONTA` varchar(15) CHARACTER SET utf8 NOT NULL,
  `UR` int(11) NOT NULL,
  `SALDO` double(10,2) NOT NULL,
  `DATA_ATUALIZACAO_SALDO` datetime NOT NULL COMMENT 'Recebe a data que o saldo foi atualizado pela primeira vez',
  `SALDO_NCONCILIADO` double(10,2) NOT NULL,
  `SALDO_INICIAL` double(10,2) NOT NULL,
  `DATA_SALDO_INICIAL` date NOT NULL,
  `IPCC` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contas_fornecedor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FORNECEDOR_ID` int(11) DEFAULT NULL,
  `N_CONTA` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `AG` int(4) DEFAULT NULL,
  `OP` int(4) DEFAULT NULL,
  `BANCO_FORNECEDOR_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controlados` (
  `ID` int(11) NOT NULL,
  `LOTE` varchar(20) NOT NULL,
  `NUMERO_TISS` varchar(11) NOT NULL,
  `UR_ID` int(11) NOT NULL,
  `PACIENTE_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `STATUS` varchar(11) NOT NULL,
  `QUANTIDADE` int(11) NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuidadosespeciais` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cuidado` varchar(100) NOT NULL,
  `COBRANCAPLANO_ID` int(11) NOT NULL,
  `JUSTIFICATIVA` varchar(255) DEFAULT NULL COMMENT 'S-se o serviço possui justificativa cadastrada no sistema.N-se não possui justificativa cadastrada',
  `GRUPO` int(10) DEFAULT NULL COMMENT '1-MED 2-ENF 3-TECNICO 4-FISIO MOTOR 5-FISIO RESPIR 6-NUTRI 7-FONO',
  `ALTA` int(10) DEFAULT '0' COMMENT 'DEFINE QUAIS ITENS APARECERÃO NA ALTA MÉDICA',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lista com os cuidados especiais da prescrição médica.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custo_item_fatura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catalogo_id` int(11) DEFAULT NULL,
  `cobrancaplano_id` int(11) DEFAULT NULL,
  `paciente_id` int(11) DEFAULT NULL,
  `tipo` int(1) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `custo` double(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custo_servicos_plano_ur` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PLANO_ID` int(11) NOT NULL,
  `UR` int(11) NOT NULL,
  `COBRANCAPLANO_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  `CUSTO` double(8,2) NOT NULL,
  `STATUS` char(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataparcela` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATA` datetime NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `NOTA_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devolucao` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USUARIO_ID` int(11) NOT NULL,
  `PACIENTE_ID` int(11) NOT NULL,
  `NUMERO_TISS` varchar(20) NOT NULL,
  `CATALOGO_ID` int(11) NOT NULL,
  `TIPO` int(11) NOT NULL,
  `QUANTIDADE` int(11) NOT NULL,
  `LOTE` varchar(50) NOT NULL,
  `DATA_DEVOLUCAO` date NOT NULL COMMENT 'DATA QUE FOI FEITA A DEVOLUÇÃO',
  `DATA` datetime NOT NULL COMMENT 'DATA QUE FOI INSERIDO NO SISTEMA',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dietas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dieta` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Listas das dietas.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dor_relatorio_prorrogacao_med` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRICAO` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ESCALA` int(2) NOT NULL,
  `PADRAO` varchar(100) CHARACTER SET utf8 NOT NULL,
  `RELATORIO_PRORROGACAO_MED_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `PADRAO_ID` tinyint(2) NOT NULL COMMENT '1 - PONTADA 2- QUEIMACAO  3- LATEJANTE 4- PESO 5- OUTRO',
  `RELATORIO_ALTA_MED_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dorfichaevolucao` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRICAO` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ESCALA` int(2) NOT NULL,
  `PADRAO` varchar(100) CHARACTER SET utf8 NOT NULL,
  `FICHAMEDICAEVOLUCAO_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `PADRAO_ID` tinyint(2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(80) CHARACTER SET utf8 NOT NULL,
  `percentual` float NOT NULL DEFAULT '0',
  `ATIVO` char(1) CHARACTER SET utf8 NOT NULL DEFAULT 'S',
  `SIGLA_EMPRESA` char(7) CHARACTER SET utf8 NOT NULL,
  `centro_custo_dominio` int(11) DEFAULT '0',
  `compra_automatica_central` enum('N','S') COLLATE utf8_unicode_ci DEFAULT 'N',
  `logo` varchar(150) COLLATE utf8_unicode_ci DEFAULT 'padrao.jpg',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lista contendo as filiais da Mederi. Manter a matriz c/ id 1';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enc_urgencia_emergencia_med_exame` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exame_medico_id` int(11) DEFAULT NULL,
  `descricao` varchar(200) DEFAULT NULL,
  `enc_urgencia_emergencia_med_id` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enc_urgencia_emergencia_med_problema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enc_urgencia_emergencia_med_id` int(11) DEFAULT NULL,
  `cid_id` int(11) DEFAULT NULL,
  `descricao` varchar(200) DEFAULT NULL,
  `observacao` varchar(200) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enc_urgencia_emergencia_med_procedimento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `procedimentos_id` int(11) NOT NULL,
  `descricao` varchar(200) DEFAULT NULL,
  `enc_urgencia_emergencia_med_id` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enc_urgencia_emergencia_med_quadro_clinico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quadro_clinico_id` int(11) DEFAULT NULL,
  `descricao` varchar(200) DEFAULT NULL,
  `enc_urgencia_emergencia_med_id` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encaminhamento_urgencia_emergencia_med` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `data_encaminhamento` date DEFAULT NULL,
  `tipo_ambulancia` int(11) DEFAULT NULL,
  `vaga_uti` enum('N','S') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrada` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nota` varchar(100) NOT NULL,
  `fornecedor` int(10) unsigned NOT NULL,
  `item` varchar(10) NOT NULL,
  `CATALOGO_ID` int(11) NOT NULL,
  `qtd` int(10) unsigned NOT NULL,
  `valor` double unsigned NOT NULL,
  `data` date NOT NULL,
  `LOTE` varchar(20) NOT NULL,
  `VALIDADE` date NOT NULL,
  `DES_BONIFICACAO_ITEM` double NOT NULL,
  `CFOP` int(6) NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  `CANCELADO` int(11) DEFAULT NULL COMMENT '1 SE ENTRADA DA NOTA FOI CANCELADA',
  `DATA_CANCELADO` timestamp NULL DEFAULT NULL,
  `CANCELADO_POR` int(11) DEFAULT NULL,
  `JUSTIFICATIVA` text,
  `CODIGO_REFERENCIA` varchar(50) DEFAULT NULL,
  `TIPO_AQUISICAO` int(1) DEFAULT NULL COMMENT '1-PARA COMPRA E 2 PARA ALUGUEL',
  `UR` int(11) DEFAULT NULL,
  `TIPO` int(11) DEFAULT NULL COMMENT '0-MEDICAMENTO, 1-MATERIAL, 3-DIETA, 5 EQUIPAMENTO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabela com os itens que entraram em estoque.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrada_ur` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATA` datetime NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  `QTD` int(11) NOT NULL,
  `LOTE` varchar(20) CHARACTER SET utf8 NOT NULL,
  `ITENSPEDIDOSINTERNOS_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipamentos_admissao_paciente_secmed` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `COBRANCAPLANOS_ID` int(11) NOT NULL,
  `FICHA_ADMISSAO_PACIENTE_SECMED_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipamentos_capmedica` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `COBRANCAPLANOS_ID` int(11) NOT NULL,
  `CAPMEDICA_ID` int(11) NOT NULL,
  `QTD` int(5) NOT NULL,
  `OBSERVACAO` varchar(200) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipamentosativos` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `COBRANCA_PLANOS_ID` int(11) NOT NULL,
  `CAPMEDICA_ID` int(11) NOT NULL,
  `QTD` int(11) NOT NULL,
  `OBS` varchar(100) NOT NULL,
  `DATA_INICIO` datetime NOT NULL,
  `DATA_FIM` datetime NOT NULL,
  `USUARIO` int(11) NOT NULL,
  `PRESCRICAO_AVALIACAO` char(1) NOT NULL,
  `PRESCRICAO_ID` int(11) NOT NULL,
  `PACIENTE_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `TROCADO_POR` int(11) NOT NULL COMMENT 'Id do equipamento que ele foi tracado.',
  `JUSTIFICATIVA` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT 'Justiificativa da troca',
  `FATURADO` char(1) NOT NULL DEFAULT 'N' COMMENT 'N- não foi faturado, S- faturado',
  `REFATURADO` int(11) NOT NULL DEFAULT '0' COMMENT 'Quntidade de refaturamento',
  `saida_id` int(11) DEFAULT NULL,
  `lote` varchar(255) DEFAULT NULL,
  `finalizado_em`  datetime NULL ,
  `finalizado_por`  int(11) NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipepaciente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `funcionario` int(11) NOT NULL,
  `paciente` int(11) NOT NULL,
  `inicio` date NOT NULL,
  `fim` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Equipe que acompanha o paciente. NAO UTILIZADO';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_backtrace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `text` text NOT NULL,
  `module` varchar(50) NOT NULL,
  `file` varchar(100) NOT NULL,
  `line` int(11) NOT NULL,
  `function` varchar(100) DEFAULT NULL,
  `class` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `especiais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `principio` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `vencimento` date NOT NULL,
  `lote` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `qtd` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='LIsta de medicamentos especiais da logística.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `especialidade_medica` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ESPECIALIDADE` text NOT NULL,
  `tiss_especialidade` varchar(150) DEFAULT NULL,
  `tiss_cbos` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estados` (
  `ID` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `UF` varchar(10) NOT NULL DEFAULT '',
  `NOME` varchar(20) NOT NULL DEFAULT '',
  `tiss_codigo` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estoque` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `QTD_ATUAL` int(11) DEFAULT NULL,
  `QTD_MINIMO` int(11) DEFAULT NULL,
  `VALOR_MEDIO` double DEFAULT '0',
  `DATA_ULTIMA_ENTRADA` datetime DEFAULT NULL,
  `DATA_ULTIMA_SAIDA` datetime DEFAULT NULL,
  `TIPO` int(11) NOT NULL COMMENT '0 - Medicamento, 1 - Material, 3 - Dieta,5-Equipamentos\n Se for Equipamentos estará relacionada com a tabela de equipamentos\n',
  `NUMERO_TISS` varchar(11) CHARACTER SET utf8 NOT NULL,
  `CATALOGO_ID` int(11) NOT NULL,
  `empresas_id_1` int(11) DEFAULT NULL COMMENT 'Id da Unidade Regional',
  `empresas_id_2` int(11) DEFAULT NULL,
  `empresas_id_4` int(11) DEFAULT NULL,
  `empresas_id_7` int(11) DEFAULT NULL,
  `empresas_id_8` int(11) DEFAULT NULL,
  `empresas_id_9` int(11) DEFAULT NULL,
  `empresas_id_5` int(11) DEFAULT NULL,
  `empresas_id_11` int(11) DEFAULT NULL,
  `QTD_MIN_1` int(11) NOT NULL,
  `QTD_MIN_2` int(11) NOT NULL,
  `QTD_MIN_4` int(11) NOT NULL,
  `QTD_MIN_7` int(11) NOT NULL,
  `QTD_MIN_9` int(11) NOT NULL,
  `QTD_MIN_5` int(11) NOT NULL,
  `QTD_MIN_8` int(11) DEFAULT NULL,
  `QTD_MIN_10` int(11) DEFAULT NULL,
  `QTD_MIN_11` int(11) DEFAULT NULL,
  `empresas_id_10` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evolucao_enf_dreno` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVOLUCAO_ENF_ID` int(11) NOT NULL,
  `DRENO` varchar(50) CHARACTER SET utf8 NOT NULL,
  `LOCAL` varchar(25) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evolucao_enf_ferida` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVOLUCAO_ENF_ID` int(11) NOT NULL,
  `LOCAL` varchar(25) CHARACTER SET utf8 NOT NULL,
  `CARACTERISTICA` varchar(50) CHARACTER SET utf8 NOT NULL,
  `TIPO_CURATIVO` varchar(50) CHARACTER SET utf8 NOT NULL,
  `FREQUENCIA_TROCA_CURATIVO` int(11) NOT NULL COMMENT 'RECEBE 1 - 1XDIA, 2-2X/DIA, 3-A CADA 48H, 4 A CADA 72,5-1XSEMANA',
  `LADO` varchar(20) NOT NULL,
  `OBSERVACAO` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evolucao_enf_suplemento` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVOLUCAO_ENF_ID` int(11) NOT NULL,
  `NOME` varchar(40) DEFAULT NULL,
  `FREQUENCIA` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evolucaoenfermagem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USUARIO_ID` int(11) DEFAULT NULL,
  `PACIENTE_ID` int(11) DEFAULT NULL,
  `DATA` datetime DEFAULT NULL,
  `BOM_RUIM` int(11) DEFAULT NULL,
  `NUTRIDO_DESNUTRIDO` int(11) DEFAULT NULL,
  `HIGI_CORP_SATIS` int(11) DEFAULT NULL,
  `HIGI_BUCAL_SATIS` int(11) DEFAULT NULL,
  `ESTADO_EMOCIONAL` int(11) DEFAULT NULL,
  `CONSCIENTE` int(11) DEFAULT NULL,
  `SEDADO` int(11) DEFAULT NULL,
  `OBNUBILADO` int(11) DEFAULT NULL,
  `TORPOROSO` int(11) DEFAULT NULL,
  `COMATOSO` int(11) DEFAULT NULL,
  `ORIENTADO` int(11) DEFAULT NULL,
  `DESORIENTADO` int(11) DEFAULT NULL,
  `SONOLENTO` int(11) DEFAULT NULL,
  `INTEGRO` int(11) DEFAULT NULL,
  `CICATRIZ` int(11) DEFAULT NULL,
  `LOCAL_CICATRIZ` varchar(50) DEFAULT NULL,
  `COND_MOTORA` int(11) DEFAULT NULL,
  `PALERGIAMED` varchar(1) DEFAULT NULL,
  `PALERGIAALIM` varchar(1) DEFAULT NULL,
  `TIPOALERGIAMED` varchar(100) DEFAULT NULL,
  `TIPOALERGIAALIM` varchar(100) DEFAULT NULL,
  `INTEGRA_LESAO` int(11) DEFAULT NULL,
  `ICTERICA_ANICTERICA` int(11) DEFAULT NULL,
  `HIDRATADA_DESIDRATADA` int(11) DEFAULT NULL,
  `PALIDA` int(11) DEFAULT NULL,
  `SUDOREICA` int(11) DEFAULT NULL,
  `FRIA` int(11) DEFAULT NULL,
  `PELE_EDEMA` int(11) DEFAULT NULL,
  `DESCAMACAO` int(11) DEFAULT NULL,
  `MUCOSAS` int(11) DEFAULT NULL,
  `ALT_PESCOCO` int(11) DEFAULT NULL,
  `NODULOS` int(11) DEFAULT NULL,
  `GANGLIOS_INF` int(11) DEFAULT NULL,
  `RIGIDEZ` int(11) DEFAULT NULL,
  `BOCIO` int(11) DEFAULT NULL,
  `FERIDA_PESCOCO` int(11) DEFAULT NULL,
  `AUSCULTA_ALTERADA` int(11) DEFAULT NULL,
  `CREPTOS` int(11) DEFAULT NULL,
  `ESTERTORES` int(11) DEFAULT NULL,
  `RONCOS` int(11) DEFAULT NULL,
  `MV_DIMI` int(11) DEFAULT NULL,
  `SIMETRICO_ASSIMETRICO` int(11) DEFAULT NULL,
  `PALPACAO` int(11) DEFAULT NULL,
  `GANGLIOS` int(11) DEFAULT NULL,
  `CICATRIZ_TORAX` int(11) DEFAULT NULL,
  `AFUNDAMENTO` int(11) DEFAULT NULL,
  `PADRAO_CARDIORESPIRATORIO` tinyint(1) DEFAULT NULL,
  `PADRAO_CARDIORESPIRATORIO1` tinyint(1) DEFAULT NULL,
  `PADRAO_CARDIORESPIRATORIO2` tinyint(1) DEFAULT NULL,
  `EUPNEICO` int(11) DEFAULT NULL,
  `DISPINEICO` int(11) DEFAULT NULL,
  `BRADIPNEICO` int(11) DEFAULT NULL,
  `TAQUIPNEICO` int(11) DEFAULT NULL,
  `TAQUICARDICO` int(11) DEFAULT NULL,
  `BRADICARDICO` int(11) DEFAULT NULL,
  `TIPO_VENTILACAO` int(11) DEFAULT NULL,
  `TIPO_VENTILACAO1` int(11) DEFAULT NULL,
  `TIPO_VENTILACAO2` int(11) DEFAULT NULL,
  `BIPAP` int(11) DEFAULT NULL,
  `OXIGENIO_SN` varchar(1) DEFAULT NULL,
  `OXIGENIO` int(11) DEFAULT NULL,
  `VENT_CONCENTRADOR_NUM` int(5) DEFAULT NULL,
  `VENT_CONCENTRADOR` int(5) DEFAULT NULL,
  `VENT_MASCARA_VENTURY_NUM` int(5) DEFAULT NULL,
  `VENT_MASCARA_VENTURY` int(5) DEFAULT NULL,
  `VENT_RESPIRADOR_TIPO` int(11) DEFAULT NULL,
  `VENT_RESPIRADOR` int(11) DEFAULT NULL,
  `VENT_CATETER_OCULOS_NUM` int(11) DEFAULT NULL,
  `VENT_CATETER_OCULOS` int(11) DEFAULT NULL,
  `VENT_CATETER_OXIGENIO_QUANT` int(11) DEFAULT NULL,
  `VENT_CATETER_OXIGENIO_NUM` int(11) DEFAULT NULL,
  `ALT_ASPIRACOES` varchar(2) DEFAULT NULL,
  `ASPIRACOES_NUM` varchar(10) DEFAULT NULL,
  `ASPIRACAO_SECRECAO` varchar(1) DEFAULT NULL,
  `CARACTERISTICAS_SEC` int(11) DEFAULT NULL,
  `COR_SEC` int(11) DEFAULT NULL,
  `PLANO` int(11) DEFAULT NULL,
  `FLACIDO` int(11) DEFAULT NULL,
  `GLOBOSO` int(11) DEFAULT NULL,
  `DISTENDIDO` int(11) DEFAULT NULL,
  `RHA` varchar(1) DEFAULT NULL,
  `INDO_PALPACAO` int(11) DEFAULT NULL,
  `DOLOROSO` int(11) DEFAULT NULL,
  `TIMPANICO` int(11) DEFAULT NULL,
  `DIETA_SN` varchar(1) DEFAULT NULL,
  `ORAL` int(11) DEFAULT NULL,
  `SNE` int(11) DEFAULT NULL,
  `SNE_NUM` varchar(10) DEFAULT NULL,
  `GTM_SONDA_GASTRO` int(11) DEFAULT NULL,
  `GTM_NUM` varchar(10) DEFAULT NULL,
  `GTM_NUM_PROFUNDIDADE` varchar(10) DEFAULT NULL,
  `QUANT_SONDA` varchar(10) DEFAULT NULL,
  `SISTEMA_ABERTO` int(11) DEFAULT NULL,
  `QUANT_SISTEMA_ABERTO` varchar(50) DEFAULT NULL,
  `USO_SISTEMA_ABERTO` varchar(10) DEFAULT NULL,
  `SISTEMA_FECHADO` int(11) DEFAULT NULL,
  `QUANT_SISTEMA_FECHADO` varchar(50) DEFAULT NULL,
  `VAZAO` varchar(10) DEFAULT NULL,
  `DIETA_NPT` int(11) DEFAULT NULL,
  `VAZAO_NPT` varchar(10) DEFAULT NULL,
  `QUANT_NPT` varchar(10) DEFAULT NULL,
  `DIETA_NPP` int(11) DEFAULT NULL,
  `VAZAO_NPP` varchar(10) DEFAULT NULL,
  `QUANT_NPP` varchar(10) DEFAULT NULL,
  `DIETA_ARTESANAL` int(11) DEFAULT NULL,
  `SUPLEMENTO_SN` varchar(1) DEFAULT NULL,
  `SUPLEMENTO` varchar(10) DEFAULT NULL,
  `INTEGRA_GENITAL` int(11) DEFAULT NULL,
  `SECRECAO` int(11) DEFAULT NULL,
  `SANGRAMENTO` int(11) DEFAULT NULL,
  `PRURIDO` int(11) DEFAULT NULL,
  `EDEMA` int(11) DEFAULT NULL,
  `EDEMA_MMSS` int(11) DEFAULT NULL,
  `DEFORMIDADES_MMSS` int(11) DEFAULT NULL,
  `LESOES_MMSS` int(11) DEFAULT NULL,
  `EDEMA_MMII` int(11) DEFAULT NULL,
  `VARIZES_MMII` int(11) DEFAULT NULL,
  `DEFORMIDADES_MMII` int(11) DEFAULT NULL,
  `LESOES_MMII` int(11) DEFAULT NULL,
  `AQUECIDAS_EXT` int(11) DEFAULT NULL,
  `OXIGENADAS_EXT` int(11) DEFAULT NULL,
  `FRIAS_EXT` int(11) DEFAULT NULL,
  `CIANOTICAS_EXT` int(11) DEFAULT NULL,
  `PULSO_PRESENTE` int(11) DEFAULT NULL,
  `DISPOSITIVOS_INTRAVENOSOS` int(11) DEFAULT NULL,
  `CVC` int(11) DEFAULT NULL,
  `CVC_TIPO` int(11) DEFAULT NULL,
  `LOCAL_CVC_UNO` varchar(10) DEFAULT NULL,
  `LOCAL_CVC_DUPLO` varchar(10) DEFAULT NULL,
  `LOCAL_CVC_TRIPLO` varchar(10) DEFAULT NULL,
  `AVP` int(11) DEFAULT NULL,
  `AVP_LOCAL` varchar(10) DEFAULT NULL,
  `PORT` int(11) DEFAULT NULL,
  `QUANT_PORT` varchar(10) DEFAULT NULL,
  `PORT_LOCAL` varchar(10) DEFAULT NULL,
  `OUTROS_INTRA` tinytext,
  `ALT_GASTRO` varchar(1) DEFAULT NULL,
  `DIARREIA_OBSTIPACAO` int(11) DEFAULT NULL,
  `NAUSEAS_GASTRO` int(11) DEFAULT NULL,
  `VOMITOS_GASTRO` int(11) DEFAULT NULL,
  `DIARREIA_EPISODIOS` varchar(10) DEFAULT NULL,
  `CATETERISMO_INT` tinyint(1) DEFAULT NULL,
  `NORMAL_URINARIO` tinyint(1) DEFAULT NULL,
  `ANURIA_URINARIO` tinyint(1) DEFAULT NULL,
  `INCONTINENCIA_URINARIO` tinyint(1) DEFAULT NULL,
  `HEMATURIA_URINARIO` tinyint(1) DEFAULT NULL,
  `PIURIA_URINARIO` tinyint(1) DEFAULT NULL,
  `OLIGURIA_URINARIO` tinyint(1) DEFAULT NULL,
  `ODOR_URINARIO` tinyint(1) DEFAULT NULL,
  `OBSTIPACAO_EPISODIOS` int(11) DEFAULT NULL,
  `VOMITO_EPISODIOS` tinyint(3) DEFAULT NULL,
  `ELIMINACIONES` int(11) DEFAULT NULL,
  `FRALDA` int(11) DEFAULT NULL,
  `FRALDA_DIA` varchar(10) DEFAULT NULL,
  `TAMANHO_FRALDA` int(11) DEFAULT NULL,
  `URINARIO` int(11) DEFAULT NULL,
  `QUANT_URINARIO` varchar(10) DEFAULT NULL,
  `SONDA` int(11) DEFAULT NULL,
  `QUANT_FOLEY` varchar(10) DEFAULT NULL,
  `SONDA_ALIVIO` int(11) DEFAULT NULL,
  `QUANT_ALIVIO` varchar(10) DEFAULT NULL,
  `COLOSTOMIA` int(11) DEFAULT NULL,
  `QUANT_COLOSTOMIA` varchar(10) DEFAULT NULL,
  `DISPO_ILIOSTOMIA` int(11) DEFAULT NULL,
  `DISPO_ILIOSTOMIA_NUM` int(11) DEFAULT NULL,
  `CISTOSTOMIA` int(11) DEFAULT NULL,
  `QUANT_CISTOSMIA` varchar(10) DEFAULT NULL,
  `DRENO_SN` varchar(1) DEFAULT NULL,
  `DRENO` varchar(10) DEFAULT NULL,
  `FERIDAS_SN` varchar(1) DEFAULT NULL,
  `PA_SISTOLICA` varchar(10) DEFAULT NULL,
  `PA_DIASTOLICA` varchar(10) DEFAULT NULL,
  `FC` varchar(10) DEFAULT NULL,
  `FR` varchar(10) DEFAULT NULL,
  `SPO2` varchar(10) DEFAULT NULL,
  `TEMPERATURA` varchar(10) DEFAULT NULL,
  `PAD` text,
  `SIBILOS` tinyint(2) DEFAULT NULL,
  `TIPO_CREPTOS` tinyint(2) DEFAULT NULL,
  `TIPO_ESTERTORES` tinyint(2) DEFAULT NULL,
  `TIPO_RONCOS` tinyint(2) DEFAULT NULL,
  `TIPO_MV` tinyint(2) DEFAULT NULL,
  `SIBILOS_TIPO` tinyint(2) DEFAULT NULL,
  `NUM_VENTILACAO` int(11) DEFAULT NULL,
  `DATA_EVOLUCAO` date NOT NULL,
  `ULTIMA_SUBSTITUICAO` date DEFAULT NULL COMMENT 'DATA DA ULTIMA SUBSTITUICAO DA SONDA/BUTTON',
  `CATETER_PICC` int(11) DEFAULT NULL,
  `CATETER_PICC_LOCAL` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exame_medico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) DEFAULT NULL,
  `exame_medico_tipo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exame_medico_tipo_id_FK` (`exame_medico_tipo_id`),
  CONSTRAINT `exame_medico_tipo_id_FK` FOREIGN KEY (`exame_medico_tipo_id`) REFERENCES `exame_medico_tipo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exame_medico_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `examesegmentar` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `DESCRICAO` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `examesegmentarevolucao` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FICHA_EVOLUCAO_ID` int(11) NOT NULL,
  `EXAME_SEGMENTAR_ID` int(11) NOT NULL,
  `AVALIACAO` int(5) NOT NULL COMMENT 'RECEBE 1-NORMAL, 2-ANORMAL, 3 N EXAMINADO.',
  `OBSERVACAO` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatura` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PACIENTE_ID` int(11) NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `DATA_INICIO` date NOT NULL,
  `DATA_FIM` date NOT NULL,
  `CAPA_LOTE` varchar(18) NOT NULL DEFAULT '0',
  `STATUS` int(11) NOT NULL COMMENT 'Recebe 1-enviado, 2- Pendente, 3-finalizado',
  `DATA_LOTE` datetime NOT NULL COMMENT 'Data que o lote foi criado',
  `ORCAMENTO` int(1) NOT NULL DEFAULT '0' COMMENT '0-fatura ,1-Orçamento inicial, 2-orçamento de prorrogação, 3-Orçamento Aditivo',
  `CAPMEDICA_ID` int(11) NOT NULL,
  `OBS` text NOT NULL,
  `GUIA_TISS` varchar(30) NOT NULL DEFAULT '0',
  `PLANO_ID` int(11) NOT NULL,
  `CANCELADO_POR` int(11) DEFAULT NULL,
  `CANCELADO_EM` datetime NOT NULL,
  `DATA_ENVIO_PLANO` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faturamento` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SOLICITACOES_ID` int(11) NOT NULL,
  `NUMERO_TISS` varchar(11) NOT NULL,
  `CATALOGO_ID` int(11) NOT NULL,
  `QUANTIDADE` int(11) NOT NULL,
  `TIPO` int(11) NOT NULL COMMENT 'Se tipo for 1 ou 0 buscar na tabela brasindice no caso de 3 buscar na tabela de cobrancaplanos',
  `DATA_FATURADO` datetime NOT NULL,
  `PACIENTE_ID` int(11) NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  `VALOR_FATURA` double NOT NULL,
  `VALOR_CUSTO` double NOT NULL,
  `FATURA_ID` int(11) NOT NULL,
  `VALOR_GLOSA` double NOT NULL DEFAULT '0',
  `MOTIVO_GLOSA` varchar(300) CHARACTER SET utf8 NOT NULL,
  `UNIDADE` varchar(250) NOT NULL,
  `CODIGO_REFERENCIA` int(15) NOT NULL COMMENT 'QUando é editado recebe o id da tabela faturamento .',
  `TABELA_ORIGEM` varchar(50) NOT NULL,
  `DESCONTO_ACRESCIMO` double NOT NULL COMMENT 'se for negativo é desconto.',
  `INCLUSO_PACOTE` int(1) NOT NULL DEFAULT '0' COMMENT 'Se for 1 está incluso no pacote caso seja 0 não esta incluso.',
  `CANCELADO_POR` int(11) DEFAULT NULL,
  `CANCELADO_EM` datetime NOT NULL,
  `TUSS` int(10) DEFAULT NULL,
  `custo_atualizado_em` datetime DEFAULT NULL,
  `custo_atualizado_por` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feridas_relatorio_prorrogacao_enf` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USUARIO_ID` int(3) NOT NULL,
  `DATA` datetime DEFAULT NULL,
  `INICIO` date DEFAULT NULL,
  `FIM` date DEFAULT NULL,
  `LOCAL` text,
  `RELATORIO_PRORROGACAO_ENF_ID` int(11) DEFAULT NULL,
  `CARACTERISTICA` text CHARACTER SET utf8 NOT NULL,
  `TIPO_CURATIVO` text CHARACTER SET utf8 NOT NULL,
  `PRESCRICAO_PROX_PERIODO` text NOT NULL,
  `EDITADA_EM` timestamp NULL DEFAULT NULL,
  `DATA_DESATIVADA` timestamp NULL DEFAULT NULL,
  `RELATORIO_ADITIVO_ENF_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feridas_relatorio_prorrogacao_enf_imagem` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ferida_relatorio_prorrogacao_enf_id` int(10) NOT NULL,
  `file_uploaded_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ferida_relatorio_prorrogacao_enf_id` (`ferida_relatorio_prorrogacao_enf_id`),
  KEY `file_uploaded_id` (`file_uploaded_id`),
  CONSTRAINT `feridas_relatorio_prorrogacao_enf_imagem_ibfk_2` FOREIGN KEY (`file_uploaded_id`) REFERENCES `files_uploaded` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `feridas_relatorio_prorrogacao_enf_imagem_ibfk_1` FOREIGN KEY (`ferida_relatorio_prorrogacao_enf_id`) REFERENCES `feridas_relatorio_prorrogacao_enf` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ficha_admissao_paciente_secmed` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USUARIO_ID` int(11) NOT NULL,
  `PACIENTE_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `HISTORICO_STATUS_PACIENTE_ID` int(11) NOT NULL,
  `DATA_ADMISSAO` datetime NOT NULL,
  `ATENDIMENTO_DOMICILIAR` int(1) NOT NULL DEFAULT '0',
  `INICIO_ATENDIMENTO` date NOT NULL,
  `FIM_ATENDIMENTO` date NOT NULL,
  `INTERNACAO_DOMICILIAR` int(1) NOT NULL DEFAULT '0',
  `INICIO_INTERNACAO` date NOT NULL,
  `FIM_INTERNACAO` date NOT NULL,
  `TECNICO_ENFERMAGEM` int(1) NOT NULL DEFAULT '0',
  `TECNICO_06` int(1) NOT NULL DEFAULT '0',
  `INICIO_TECNICO_06` date NOT NULL,
  `FIM_TECNICO_06` date NOT NULL,
  `TECNICO_12` int(1) NOT NULL DEFAULT '0',
  `INICIO_TECNICO_12` date NOT NULL,
  `FIM_TECNICO_12` date NOT NULL,
  `TECNICO_24` int(1) NOT NULL DEFAULT '0',
  `INICIO_TECNICO_24` date NOT NULL,
  `FIM_TECNICO_24` date NOT NULL,
  `TECNICO_1XDIA` int(1) NOT NULL DEFAULT '0',
  `INICIO_TECNICO_1XDIA` date NOT NULL,
  `FIM_TECNICO_1XDIA` date NOT NULL,
  `TECNICO_V48H` int(1) NOT NULL DEFAULT '0',
  `INICIO_TECNICO_V48H` date NOT NULL,
  `FIM_TECNICO_V48H` date NOT NULL,
  `TECNICO_V72H` int(1) NOT NULL DEFAULT '0',
  `INICIO_TECNICO_V72H` date NOT NULL,
  `FIM_TECNICO_V72H` date NOT NULL,
  `VISITA_MEDICA` int(1) NOT NULL DEFAULT '0',
  `FREQUENCIA_MEDICA` int(11) NOT NULL,
  `VISITA_ENFERMAGEM` int(1) NOT NULL DEFAULT '0',
  `FREQUENCIA_ENFERMAGEM` int(11) NOT NULL,
  `FISIOTERAPIA` int(1) NOT NULL DEFAULT '0',
  `RESPIRATORIA` int(1) NOT NULL DEFAULT '0',
  `RESPIRATORIA_SESSAO` int(11) NOT NULL,
  `MOTORA` int(1) NOT NULL DEFAULT '0',
  `MOTORA_SESSAO` int(11) NOT NULL,
  `FONOTERAPIA` int(1) NOT NULL DEFAULT '0',
  `FONOTERAPIA_AVALIACAO` int(1) NOT NULL DEFAULT '0',
  `FONOTERAPIA_SESSAO` int(11) NOT NULL,
  `NUTRICIONISTA` int(1) NOT NULL DEFAULT '0',
  `FREQUENCIA_NUTRICIONISTA` int(11) NOT NULL,
  `QTD_DIARIA_DIETA` int(11) NOT NULL,
  `PSICOLOGO` int(1) NOT NULL DEFAULT '0',
  `FREQUENCIA_PSICOLOGO` int(11) NOT NULL,
  `PSICOLOGO_SESSAO` int(11) NOT NULL,
  `MEDICO_ESPECIALISTA` int(1) NOT NULL DEFAULT '0',
  `MEDICO_ESPECIALIDADE` varchar(100) CHARACTER SET utf8 NOT NULL,
  `OBSERVACAO` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fichamedicaevolucao` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USUARIO_ID` int(11) NOT NULL,
  `CAPMEDICA_ID` int(11) NOT NULL,
  `PACIENTE_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `MOD_HOME_CARE` int(1) NOT NULL,
  `PA_SISTOLICA_MIN` double(6,2) NOT NULL,
  `PA_SISTOLICA_MAX` double(6,2) NOT NULL,
  `PA_DIASTOLICA_MIN` double(6,2) NOT NULL,
  `PA_DIASTOLICA_MAX` double(6,2) NOT NULL,
  `FC_MIN` double(6,2) NOT NULL,
  `FC_MAX` double(6,2) NOT NULL,
  `FR_MIN` double(6,2) NOT NULL,
  `FR_MAX` double(6,2) NOT NULL,
  `TEMPERATURA_MIN` double(6,2) NOT NULL,
  `TEMPERATURA_MAX` double(6,2) NOT NULL,
  `PA_SISTOLICA_SINAL` int(2) NOT NULL COMMENT ' RECEBER SINAL DE ALERTA SE 1-AMARELO, 2-VERMELHO',
  `PA_DIASTOLICA_SINAL` int(2) NOT NULL COMMENT ' \r\nRECEBER SINAL DE ALERTA SE 1-AMARELO, 2-VERMELHO',
  `FC_SINAL` int(2) NOT NULL COMMENT ' RECEBER SINAL DE ALERTA SE 1-AMARELO, 2-VERMELHO',
  `FR_SINAL` int(2) NOT NULL COMMENT ' RECEBER SINAL DE ALERTA SE 1-AMARELO, 2-VERMELHO',
  `TEMPERATURA_SINAL` int(2) NOT NULL COMMENT ' RECEBER SINAL DE ALERTA SE 1-AMARELO, 2-VERMELHO',
  `ESTADO_GERAL` int(2) NOT NULL COMMENT 'RECEBE 1-BOM, 2-REGULAR, 3-RUIM',
  `MUCOSA` varchar(2) NOT NULL COMMENT 's-CORADS,N-DESCORADAS',
  `MUCOSA_OBS` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ESCLERAS` varchar(2) NOT NULL COMMENT 'RECEBE S-ANICTERIAS, N-ICTERIAS',
  `ESCLERAS_OBS` varchar(200) CHARACTER SET utf8 NOT NULL,
  `PADRAO_RESPIRATORIO` varchar(2) NOT NULL COMMENT 'S-EUPNEICO, N-TAQUI/DISPNEICO',
  `PADRAO_RESPIRATORIO_OBS` varchar(200) CHARACTER SET utf8 NOT NULL,
  `PA_SISTOLICA` double(6,2) NOT NULL,
  `PA_DIASTOLICA` double(6,2) NOT NULL,
  `FC` double(6,2) NOT NULL,
  `FR` double(6,2) NOT NULL,
  `TEMPERATURA` double(6,2) NOT NULL,
  `NOVOS_EXAMES` text CHARACTER SET utf8 NOT NULL,
  `IMPRESSAO` text CHARACTER SET utf8 NOT NULL,
  `PLANO_DIAGNOSTICO` text CHARACTER SET utf8 NOT NULL,
  `PLANO_TERAPEUTICO` text CHARACTER SET utf8 NOT NULL,
  `DOR` char(1) NOT NULL COMMENT 'Recebe S ou N',
  `OUTRO_RESP` text CHARACTER SET utf8 NOT NULL,
  `OXIMETRIA_PULSO` double(8,2) DEFAULT NULL,
  `OXIMETRIA_PULSO_MAX` double(6,2) NOT NULL,
  `OXIMETRIA_PULSO_MIN` double(6,2) NOT NULL,
  `OXIMETRIA_PULSO_TIPO_MIN` int(11) NOT NULL COMMENT 'Tipo: Ar Ambiente (1) / O2 Suplementar (2)',
  `OXIMETRIA_PULSO_LITROS_MIN` double(6,2) NOT NULL,
  `OXIMETRIA_PULSO_VIA_MIN` int(11) NOT NULL COMMENT 'Via: Cateter Nasal (1) / Cateter em Traqueostomia (2) / Máscara de venturi (3) / Ventilação Mecânica (4)',
  `OXIMETRIA_PULSO_TIPO_MAX` int(11) NOT NULL COMMENT 'Tipo: Ar Ambiente (1) / O2 Suplementar (2)',
  `OXIMETRIA_PULSO_LITROS_MAX` double(6,2) NOT NULL,
  `OXIMETRIA_PULSO_VIA_MAX` int(11) NOT NULL COMMENT 'Via: Cateter Nasal (1) / Cateter em Traqueostomia (2) / Máscara de venturi (3) / Ventilação Mecânica (4)',
  `GLICEMIA_CAPILAR_MIN` varchar(255) NOT NULL,
  `GLICEMIA_CAPILAR_MAX` varchar(255) NOT NULL,
  `OXIMETRIA_PULSO_TIPO` varchar(255) NOT NULL,
  `OXIMETRIA_PULSO_LITROS` varchar(255) NOT NULL,
  `OXIMETRIA_PULSO_VIA` varchar(255) NOT NULL,
  `OXIMETRIA_SINAL` int(2) NOT NULL,
  `DATA_SISTEMA` datetime NOT NULL COMMENT 'DATA EM QUE A EVOLUCAO FOI INSERIDA NO SISTEMA',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fichapaciente` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paciente` int(10) unsigned NOT NULL,
  `tipoficha` int(10) unsigned NOT NULL,
  `data` datetime NOT NULL,
  `nome` varchar(100) NOT NULL,
  `tipo` varchar(30) NOT NULL,
  `size` int(11) NOT NULL,
  `content` mediumblob NOT NULL,
  `lockf` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `paciente` (`paciente`),
  KEY `pasta` (`tipoficha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Antiga ficha de captacao. NAO UTILIZADO';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files_uploaded` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `full_path` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `mimetype` varchar(25) NOT NULL DEFAULT '',
  `size_bytes` varchar(50) NOT NULL DEFAULT '',
  `uploaded_by` int(10) NOT NULL,
  `uploaded_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `uploaded_by` (`uploaded_by`),
  CONSTRAINT `files_uploaded_ibfk_1` FOREIGN KEY (`uploaded_by`) REFERENCES `usuarios` (`idUsuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formulas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `formula` varchar(255) NOT NULL,
  `PACIENTE_ID` int(11) NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  `USUARIO_UR` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lista com as fórmulas solicitadas na prescrição..';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornecedores` (
  `idFornecedores` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) CHARACTER SET utf8 NOT NULL,
  `razaoSocial` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `cnpj` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `contato` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `telefone` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `endereco` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CIDADE_ID` int(4) DEFAULT NULL,
  `IES` varchar(15) NOT NULL,
  `IM` varchar(15) NOT NULL,
  `ATIVO` char(1) NOT NULL,
  `TIPO` char(1) NOT NULL COMMENT 'Recebe C para Cliente e F para fornecedor.',
  `CPF` varchar(14) NOT NULL,
  `FISICA_JURIDICA` int(1) NOT NULL COMMENT '0-Pessoa Fisica e 1-Pessoa Juridica ',
  `IPCC` int(11) DEFAULT NULL,
  PRIMARY KEY (`idFornecedores`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Lista de fornecedores. Inclui funcionários.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frequencia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frequencia` varchar(50) NOT NULL,
  `qtd` int(10) unsigned NOT NULL DEFAULT '0',
  `HORA` int(2) NOT NULL,
  `SEMANA` int(11) NOT NULL COMMENT 'Quantiodade de vezes na semana',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lista com frequência de uso dos medicamentos.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glosa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FATURA_ID` int(11) NOT NULL,
  `CRIADOR_ID` int(11) NOT NULL,
  `DATA_PAGAMENTO` date NOT NULL,
  `DATA` datetime NOT NULL COMMENT 'Data que foi informado a glosa no sistema.',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo_nead` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(255) DEFAULT NULL,
  `PESO` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupos` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `DESCRICAO` varchar(100) CHARACTER SET utf8 NOT NULL,
  `QTD` int(5) NOT NULL,
  `GRUPO_NEAD_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historico_desfazer_conciliacao_bancaria` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USUARIO_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `TRANSFERENCIA_ID` int(11) NOT NULL,
  `PARCELA_ID` int(11) NOT NULL,
  `VALOR` double(10,2) NOT NULL,
  `TIPO_NOTA` int(1) NOT NULL COMMENT '0-recebimento, 1-desembolso',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historico_modalidade_paciente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paciente` int(11) NOT NULL,
  `modalidade` int(11) NOT NULL,
  `data_mudanca` date NOT NULL,
  `data_sistema` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuario` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historico_plano_liminar_paciente` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PACIENTE_ID` int(11) NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  `LIMINAR` int(11) NOT NULL,
  `PLANO_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historico_status_paciente` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USUARIO_ID` int(11) NOT NULL,
  `PACIENTE_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `DATA_ADMISSAO` datetime NOT NULL,
  `DATA_AUTORIZACAO` datetime NOT NULL,
  `TIPO` int(1) NOT NULL,
  `OBSERVACAO` text CHARACTER SET utf8 NOT NULL,
  `STATUS_PACIENTE_ID` int(11) NOT NULL,
  `CAPMEDICA_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `impostos_retidos` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IMPOSTO` varchar(50) CHARACTER SET utf8 NOT NULL,
  `CODIGO` int(11) NOT NULL,
  `FORNECEDOR_ID` int(11) NOT NULL,
  `IPCF2` varchar(250) CHARACTER SET utf8 NOT NULL,
  `IPCF3` varchar(250) CHARACTER SET utf8 NOT NULL,
  `PLANO_CONTA_ID` int(11) NOT NULL,
  `IPCF4` varchar(100) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `impostos_tarifas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(255) NOT NULL,
  `tipo` enum('IMPOSTO','TARIFA') NOT NULL,
  `debito_credito` enum('CREDITO','DEBITO') DEFAULT NULL,
  `ipcf` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `impostos_tarifas_lancto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conta` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `documento` varchar(50) CHARACTER SET utf8 NOT NULL,
  `data` date NOT NULL,
  `valor` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `indexed_demonstrativo_contas_bancarias` (
  `ID` int(11) NOT NULL DEFAULT '0',
  `TIPO_CONTA_BANCARIA_ID` int(11) DEFAULT NULL,
  `ORIGEM_FUNDOS_ID` int(11) NOT NULL,
  `AGENCIA` varchar(11) CHARACTER SET utf8 NOT NULL,
  `NUM_CONTA` varchar(15) CHARACTER SET utf8 NOT NULL,
  `UR` int(11) NOT NULL,
  `SALDO` double(10,2) NOT NULL,
  `DATA_ATUALIZACAO_SALDO` datetime NOT NULL COMMENT 'Recebe a data que o saldo foi atualizado pela primeira vez',
  `SALDO_NCONCILIADO` double(10,2) NOT NULL,
  `SALDO_INICIAL` double(10,2) NOT NULL,
  `DATA_SALDO_INICIAL` date NOT NULL,
  `IPCC` int(11) DEFAULT NULL,
  `conta` varchar(139) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `indexed_demonstrativo_desembolsos` (
  `id` int(11) NOT NULL DEFAULT '0',
  `idNota` int(10) unsigned NOT NULL,
  `valor` double NOT NULL,
  `vencimento` date NOT NULL,
  `codBarras` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `origem` int(11) NOT NULL DEFAULT '1',
  `aplicacao` int(11) NOT NULL,
  `obs` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `comprovante` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `empresa` int(10) unsigned NOT NULL,
  `JurosMulta` double(8,2) NOT NULL DEFAULT '0.00',
  `desconto` double(8,2) NOT NULL,
  `TR` int(11) NOT NULL,
  `VALOR_PAGO` double(8,2) NOT NULL,
  `DATA_PAGAMENTO` date NOT NULL,
  `OUTROS_ACRESCIMOS` double NOT NULL DEFAULT '0',
  `NUM_DOCUMENTO` varchar(20) CHARACTER SET utf8 NOT NULL,
  `ISSQN` double NOT NULL DEFAULT '0',
  `PIS` double NOT NULL DEFAULT '0',
  `COFINS` double NOT NULL DEFAULT '0',
  `CSLL` double NOT NULL DEFAULT '0',
  `IR` double NOT NULL DEFAULT '0',
  `CONCILIADO` char(1) CHARACTER SET utf8 NOT NULL,
  `DATA_PROCESSADO` datetime NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  `DATA_CONCILIADO` date NOT NULL,
  `DATA_DESPROCESSADA` datetime NOT NULL,
  `DESPROCESSADA_POR` int(11) NOT NULL,
  `JUSTIFICATIVA_DESPROCESSADA` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `PARCELA_ORIGEM` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `indexed_demonstrativo_recebimentos` (
  `id` int(11) NOT NULL DEFAULT '0',
  `idNota` int(10) unsigned NOT NULL,
  `valor` double NOT NULL,
  `vencimento` date NOT NULL,
  `codBarras` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `origem` int(11) NOT NULL DEFAULT '1',
  `aplicacao` int(11) NOT NULL,
  `obs` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `comprovante` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `empresa` int(10) unsigned NOT NULL,
  `JurosMulta` double(8,2) NOT NULL DEFAULT '0.00',
  `desconto` double(8,2) NOT NULL,
  `TR` int(11) NOT NULL,
  `VALOR_PAGO` double(8,2) NOT NULL,
  `DATA_PAGAMENTO` date NOT NULL,
  `OUTROS_ACRESCIMOS` double NOT NULL DEFAULT '0',
  `NUM_DOCUMENTO` varchar(20) CHARACTER SET utf8 NOT NULL,
  `ISSQN` double NOT NULL DEFAULT '0',
  `PIS` double NOT NULL DEFAULT '0',
  `COFINS` double NOT NULL DEFAULT '0',
  `CSLL` double NOT NULL DEFAULT '0',
  `IR` double NOT NULL DEFAULT '0',
  `CONCILIADO` char(1) CHARACTER SET utf8 NOT NULL,
  `DATA_PROCESSADO` datetime NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  `DATA_CONCILIADO` date NOT NULL,
  `DATA_DESPROCESSADA` datetime NOT NULL,
  `DESPROCESSADA_POR` int(11) NOT NULL,
  `JUSTIFICATIVA_DESPROCESSADA` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `PARCELA_ORIGEM` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intercorrenciasfichaevolucao` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRICAO` text CHARACTER SET utf8 NOT NULL,
  `FICHAMEDICAEVOLUCAO_ID` int(11) NOT NULL,
  `FICHAMEDICAPRORROGACAO_ID` int(11) NOT NULL,
  `RELATORIOALTA_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `internacoescapmedica` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_CAPMEDICA` int(11) DEFAULT NULL,
  `MOTIVO_INTERNACAO` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itens` (
  `idItens` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codNota` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` int(10) unsigned DEFAULT NULL,
  `NUMERO_TISS` int(11) unsigned NOT NULL,
  `CATALOGO_ID` int(11) NOT NULL,
  `quantidade` int(10) DEFAULT NULL,
  `localizacao` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `lote` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `vencimento` date DEFAULT NULL,
  `valor` double NOT NULL,
  `segundoNome` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `codOrigem` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idItens`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Itens do estoque.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itens_conciliacao_bancaria` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONCILIACAO_BANCARIA_ID` int(11) NOT NULL,
  `DATA_PAGAMENTO` date NOT NULL,
  `TR_PARCELA` varchar(25) NOT NULL,
  `FORNECEDOR_ID` int(11) NOT NULL,
  `NUM_DOCUMENTO` varchar(25) NOT NULL,
  `TIPO_NOTA` int(11) NOT NULL,
  `CONCILIADO` char(1) NOT NULL,
  `VALOR` double(8,2) NOT NULL,
  `TRANSFERENCIA_BANCARIA` char(1) CHARACTER SET utf8 NOT NULL,
  `TRANSFERENCIA_BANCARIA_ID` int(11) NOT NULL,
  `DATA_CONCILIADO` date NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itens_kit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idKit` int(10) unsigned NOT NULL,
  `NUMERO_TISS` varchar(15) NOT NULL,
  `CATALOGO_ID` int(11) NOT NULL,
  `qtd` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idKit` (`idKit`,`NUMERO_TISS`),
  CONSTRAINT `itens_kit_ibfk_1` FOREIGN KEY (`idKit`) REFERENCES `kitsmaterial` (`idKit`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Itens que compõe um kit de material.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itens_prescricao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idPrescricao` int(10) unsigned NOT NULL,
  `tipo` int(11) NOT NULL,
  `NUMERO_TISS` varchar(11) CHARACTER SET utf8 NOT NULL,
  `CATALOGO_ID` int(11) NOT NULL,
  `nome` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `apresentacao` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `via` int(10) unsigned NOT NULL,
  `dose` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `umdose` int(10) unsigned NOT NULL COMMENT 'Unidade de medida da dose',
  `frequencia` int(10) unsigned NOT NULL,
  `aprazamento` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '-',
  `inicio` date NOT NULL,
  `fim` date NOT NULL,
  `obs` text COLLATE utf8_unicode_ci NOT NULL,
  `descricao` varchar(255) CHARACTER SET utf8 NOT NULL,
  `OBS_ENFERMAGEM` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `FATURADO` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `idPrescricao` (`idPrescricao`),
  CONSTRAINT `itens_prescricao_ibfk_1` FOREIGN KEY (`idPrescricao`) REFERENCES `prescricoes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Itens que compõe a prescrição médica.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itensativos` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_PRESCRICAO` int(11) DEFAULT NULL,
  `ID_COBRANCAPLANOS` int(11) DEFAULT NULL,
  `ATIVO` int(11) DEFAULT NULL,
  `PACIENTE` int(11) DEFAULT NULL,
  `QTD_ENVIADO` int(11) DEFAULT NULL,
  `QTD_DEVOLVIDO` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itenspedidosinterno` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `PEDIDOS_INTERNO_ID` int(10) NOT NULL,
  `NUMERO_TISS` varchar(15) CHARACTER SET utf8 NOT NULL,
  `CATALOGO_ID` int(11) NOT NULL,
  `QTD` int(10) NOT NULL,
  `TIPO` int(10) NOT NULL,
  `ENVIADO` int(10) NOT NULL,
  `STATUS` char(1) NOT NULL COMMENT 'A - aberto, P - pendente, F - finalizado',
  `CONFIRMADO` int(11) NOT NULL,
  `CANCELADO` char(1) NOT NULL DEFAULT 'N',
  `DATA_CANCELADO` datetime NOT NULL,
  `CANCELADO_POR` int(11) NOT NULL COMMENT 'Id do usuário do da Central que cancelou',
  `JUSTIFICATIVA` varchar(200) CHARACTER SET utf8 NOT NULL,
  `EMERGENCIA` int(1) NOT NULL DEFAULT '0' COMMENT 'Se 1 é emergencia.',
  `CANCELADO_ENTRADA` char(1) CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `DATA_CANCELADO_ENTRADA` datetime NOT NULL,
  `CANCELADO_ENTRADA_POR` int(11) NOT NULL COMMENT 'Id do usuário da Ur que cancelou',
  `JUSTIFICATIVA_ENTRADA` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `justificativa_itens_negados` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `JUSTIFICATIVA` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `justificativa_profissional_relatorio` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_SERVICO` int(11) NOT NULL,
  `JUSTIFICATIVA` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabela com padronização das justificativas para solicitação de profissional nos relatórios de prorrogação médica.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `justificativa_solicitacao_avulsa` (
  `id` int(11) NOT NULL,
  `justificativa` varchar(100) NOT NULL,
  `visualizar` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kitsmaterial` (
  `idKit` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `COD_REF` varchar(15) NOT NULL,
  PRIMARY KEY (`idKit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabela com os kits de materiais';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locacoes` (
  `idLocacoes` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `valorDiaria` double NOT NULL,
  `idCliente` int(10) unsigned NOT NULL,
  `idFornecedor` int(10) unsigned NOT NULL,
  `inicio` date NOT NULL,
  `fim` date DEFAULT NULL,
  `ativa` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idLocacoes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela com as locações. NAO UTILIZADO';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loguser` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `log` text NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabela com o registros das ações dos usuários.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materiais` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ITEM` varchar(100) NOT NULL,
  `APRESENTACAO` varchar(100) NOT NULL,
  `CODIGOBARRAS` varchar(50) NOT NULL,
  `COD_SIMPRO` int(11) NOT NULL,
  `VALOR_MEDIO` double(8,2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `material` (
  `idMaterial` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigobarras` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0000000000000',
  `estoque` int(11) NOT NULL,
  `valorMedio` double unsigned NOT NULL,
  PRIMARY KEY (`idMaterial`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Lista de materiais. Foi substituida pelo Brasindice.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicacoes` (
  `idMedicacoes` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `principioAtivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `minimo` int(10) unsigned NOT NULL,
  `classe` int(11) NOT NULL DEFAULT '104',
  PRIMARY KEY (`idMedicacoes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lista de medicações, foi substituída pelo Brasindice.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modalidade_home_care` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modalidade` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `msgs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` datetime NOT NULL,
  `origem` int(10) unsigned NOT NULL,
  `destino` int(10) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `msg` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabela com as mensagens enviadas pelos usuários.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mural` (
  `idMural` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dta` date NOT NULL,
  `informacao` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `usuario` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idMural`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Mural de avisos. NAO UTILIZADO.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `naturezanotas` (
  `idNaturezaNotas` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIPO` int(11) NOT NULL,
  PRIMARY KEY (`idNaturezaNotas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela com as naturezas das notas.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nomecomercialmed` (
  `idNomeComercialMed` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Medicacoes_idMedicacoes` int(10) unsigned NOT NULL,
  `nome` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigobarras` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0000000000000',
  `estoque` int(11) NOT NULL,
  `valorMedio` double unsigned NOT NULL,
  `classe` int(10) NOT NULL,
  PRIMARY KEY (`idNomeComercialMed`),
  KEY `NomeComercialMed_FKIndex1` (`Medicacoes_idMedicacoes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela com os nomes comerciais da antiga tab de medicamentos';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nomesfantasia` (
  `idFantasia` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `apr` int(10) unsigned NOT NULL,
  `fantasia` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idFantasia`),
  KEY `apr` (`apr`),
  CONSTRAINT `nomesfantasia_ibfk_1` FOREIGN KEY (`apr`) REFERENCES `nomecomercialmed` (`idNomeComercialMed`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Nome fantasia da antiga tabela de medicamentos.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notas` (
  `USUARIO_ID` int(11) NOT NULL,
  `idNotas` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `codNatureza` int(10) unsigned DEFAULT NULL,
  `codCentroCusto` int(10) unsigned DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `dataEntrada` date DEFAULT NULL COMMENT 'data de emissão',
  `descricao` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `codFornecedor` int(10) unsigned DEFAULT NULL,
  `tipo` tinyint(1) NOT NULL COMMENT ' 0 - RECEBIMENTO, 1 - DESEMBOLSO',
  `status` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `codSubNatureza` int(10) unsigned NOT NULL,
  `empresa` int(10) unsigned NOT NULL,
  `ICMS` double NOT NULL,
  `FRETE` double NOT NULL,
  `SEGURO` double NOT NULL,
  `VAL_PRODUTOS` double NOT NULL,
  `DES_ACESSORIAS` double NOT NULL,
  `IPI` double NOT NULL,
  `ISSQN` double NOT NULL,
  `DESCONTO_BONIFICACAO` double NOT NULL,
  `CHAVE` varchar(255) CHARACTER SET utf8 NOT NULL,
  `TR` int(11) NOT NULL,
  `TIPO_DOCUMENTO` varchar(50) NOT NULL,
  `PIS` double(8,2) NOT NULL,
  `COFINS` double(8,2) NOT NULL,
  `CSLL` double(8,2) NOT NULL,
  `IR` double(8,2) NOT NULL,
  `DATA` datetime NOT NULL COMMENT 'data de entrada no sistema',
  `PCC` float(8,2) NOT NULL,
  `INSS` float(8,2) NOT NULL,
  `NOTA_REFERENCIA` int(11) NOT NULL,
  `IMPOSTOS_RETIDOS_ID` int(11) NOT NULL,
  PRIMARY KEY (`idNotas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela das notas fiscais.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `observacoes_auditoria` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PACIENTE_ID` int(11) DEFAULT NULL,
  `CRIADOR` int(11) DEFAULT NULL,
  `DATA` datetime DEFAULT NULL,
  `OBSERVACAO` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operadoras` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(100) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela contendo as Operadoras dos Plano de saúde';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operadoras_planosdesaude` (
  `OPERADORAS_ID` int(11) NOT NULL,
  `PLANOSDESAUDE_ID` int(10) unsigned NOT NULL,
  KEY `fk_operadoras_has_planosdesaude_planosdesaude1` (`PLANOSDESAUDE_ID`),
  KEY `fk_operadoras_has_planosdesaude_operadoras1` (`OPERADORAS_ID`),
  CONSTRAINT `fk_operadoras_has_planosdesaude_operadoras1` FOREIGN KEY (`OPERADORAS_ID`) REFERENCES `operadoras` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_operadoras_has_planosdesaude_planosdesaude1` FOREIGN KEY (`PLANOSDESAUDE_ID`) REFERENCES `planosdesaude` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orcamento_avulso` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PACIENTE` varchar(250) NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `DATA_INICIO` date NOT NULL,
  `DATA_FIM` date NOT NULL,
  `PLANO_ID` int(11) NOT NULL,
  `UR` int(11) NOT NULL,
  `OBSERVACAO` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orcamento_avulso_item` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORCAMENTO_AVU_ID` int(11) NOT NULL,
  `NUMERO_TISS` int(11) NOT NULL,
  `CATALOGO_ID` int(11) NOT NULL,
  `VAL_PRODUTO` double(8,2) NOT NULL,
  `QTD` int(11) NOT NULL,
  `TIPO` int(11) NOT NULL,
  `CUSTO` double(8,2) NOT NULL,
  `UNIDADE` int(11) NOT NULL,
  `TABELA_ORIGEM` varchar(50) NOT NULL,
  `CANCELADO_POR` int(11) DEFAULT NULL,
  `CANCELADO_EM` datetime DEFAULT NULL,
  `TUSS` int(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orcamento_avulso_item_copy` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORCAMENTO_AVU_ID` int(11) NOT NULL,
  `NUMERO_TISS` int(11) NOT NULL,
  `CATALOGO_ID` int(11) NOT NULL,
  `VAL_PRODUTO` double(8,2) NOT NULL,
  `QTD` int(11) NOT NULL,
  `TIPO` int(11) NOT NULL,
  `CUSTO` double(8,2) NOT NULL,
  `UNIDADE` int(11) NOT NULL,
  `TABELA_ORIGEM` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orientacoes_alta_enfermagem` (
  `ID` int(11) NOT NULL,
  `ORIENTACAO` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orientacoes_relatorio_alta_enf` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USUARIO_ID` int(11) DEFAULT NULL,
  `REL_ALTA_ID` int(11) DEFAULT NULL,
  `DATA` datetime DEFAULT NULL,
  `INICIO` date DEFAULT NULL,
  `FIM` date DEFAULT NULL,
  `ORIENTACOES` tinyint(2) DEFAULT NULL,
  `OUTRAS_ORIENTACOES` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `origemfundos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `forma` varchar(100) CHARACTER SET utf8 NOT NULL,
  `SIGLA_BANCO` char(5) CHARACTER SET utf8 NOT NULL,
  `COD_BANCO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `forma` (`forma`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela referente ao campo "Origem" no process das notas.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parcelas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idNota` int(10) unsigned NOT NULL,
  `valor` double NOT NULL,
  `vencimento` date NOT NULL,
  `codBarras` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `origem` int(11) NOT NULL DEFAULT '1',
  `aplicacao` int(11) NOT NULL,
  `obs` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `comprovante` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `empresa` int(10) unsigned NOT NULL,
  `JurosMulta` double(8,2) NOT NULL DEFAULT '0.00',
  `desconto` double(8,2) NOT NULL,
  `TR` int(11) NOT NULL,
  `VALOR_PAGO` double(8,2) NOT NULL,
  `DATA_PAGAMENTO` date NOT NULL,
  `OUTROS_ACRESCIMOS` double NOT NULL DEFAULT '0',
  `NUM_DOCUMENTO` varchar(20) CHARACTER SET utf8 NOT NULL,
  `ISSQN` double NOT NULL DEFAULT '0',
  `PIS` double NOT NULL DEFAULT '0',
  `COFINS` double NOT NULL DEFAULT '0',
  `CSLL` double NOT NULL DEFAULT '0',
  `IR` double NOT NULL DEFAULT '0',
  `CONCILIADO` char(1) CHARACTER SET utf8 NOT NULL,
  `DATA_PROCESSADO` datetime NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  `DATA_CONCILIADO` date NOT NULL,
  `DATA_DESPROCESSADA` datetime NOT NULL,
  `DESPROCESSADA_POR` int(11) NOT NULL,
  `JUSTIFICATIVA_DESPROCESSADA` text COLLATE utf8_unicode_ci NOT NULL,
  `PARCELA_ORIGEM` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idNota` (`idNota`),
  CONSTRAINT `parcelas_ibfk_1` FOREIGN KEY (`idNota`) REFERENCES `notas` (`idNotas`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Parcelas das notas';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pastasProntuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Pastas do menu de prontuários.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedidosinterno` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `DATA` datetime NOT NULL,
  `USUARIO` int(10) NOT NULL,
  `EMPRESA` int(10) NOT NULL,
  `STATUS` char(1) NOT NULL COMMENT 'A - aberto, P - pendente, F - finalizado',
  `OBSERVACAO` varchar(255) NOT NULL,
  `DATA_MAX_ENTREGA` date NOT NULL,
  `CONFIRMADO` char(1) NOT NULL DEFAULT 'N' COMMENT 'N-não S-SIm',
  `DATA_CONFIRMADO` datetime NOT NULL,
  `CONFIRMADO_POR` int(11) NOT NULL,
  `CANCELADO_POR` int(11) NOT NULL,
  `DATA_CANCELADO` datetime NOT NULL,
  `JUSTIFICATIVA` text NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedidosinterno_copy` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `DATA` datetime NOT NULL,
  `USUARIO` int(10) NOT NULL,
  `EMPRESA` int(10) NOT NULL,
  `STATUS` char(1) NOT NULL COMMENT 'A - aberto, P - pendente, F - finalizado',
  `OBSERVACAO` varchar(255) NOT NULL,
  `DATA_MAX_ENTREGA` date NOT NULL,
  `CONFIRMADO` char(1) NOT NULL DEFAULT 'N' COMMENT 'N-não S-SIm',
  `DATA_CONFIRMADO` datetime NOT NULL,
  `CONFIRMADO_POR` int(11) NOT NULL,
  `CANCELADO_POR` int(11) NOT NULL,
  `DATA_CANCELADO` datetime NOT NULL,
  `JUSTIFICATIVA` text NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plano_contas` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `N1` char(1) DEFAULT NULL,
  `COD_N2` varchar(5) NOT NULL,
  `N2` varchar(200) DEFAULT NULL,
  `COD_N3` varchar(5) NOT NULL,
  `N3` varchar(200) DEFAULT NULL,
  `COD_N4` varchar(10) NOT NULL,
  `N4` varchar(200) DEFAULT NULL,
  `COD_IPCC` varchar(15) DEFAULT NULL,
  `IPCC` varchar(200) DEFAULT NULL,
  `PROVISIONA` enum('n','s') NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plano_contas_antigo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `N1` char(1) DEFAULT NULL,
  `COD_N2` varchar(5) NOT NULL,
  `N2` varchar(200) DEFAULT NULL,
  `COD_N3` varchar(5) NOT NULL,
  `N3` varchar(200) DEFAULT NULL,
  `COD_N4` varchar(10) NOT NULL,
  `N4` varchar(200) DEFAULT NULL,
  `COD_IPCC` varchar(15) DEFAULT NULL,
  `IPCC` varchar(200) DEFAULT NULL,
  `PROVISIONA` enum('n','s') NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plano_contas_contabil` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODIGO_DOMINIO` int(11) NOT NULL,
  `N1` enum('A','P','R','ARE') NOT NULL,
  `COD_N2` varchar(5) NOT NULL,
  `N2` varchar(200) NOT NULL,
  `COD_N3` varchar(6) NOT NULL,
  `N3` varchar(200) NOT NULL,
  `COD_N4` varchar(10) NOT NULL,
  `N4` varchar(200) NOT NULL,
  `COD_N5` varchar(13) NOT NULL,
  `N5` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planocontas_porcento_ur` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `COD_N2` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `N3` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `COD_N3` varchar(15) NOT NULL,
  `N2` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `UR` int(11) NOT NULL,
  `PORCENTO` double(8,2) NOT NULL,
  `DATA` datetime NOT NULL,
  `STATUS` int(1) NOT NULL DEFAULT '1' COMMENT 'Status da porcentagem em relação ao plano de contas 1-ativo, 0-inativo',
  `USUARIO_ID` int(11) NOT NULL,
  `ANTERIOR` int(11) NOT NULL COMMENT 'Se esse item já existia antes e foi alterado ele recebe o ID do item anterior nessa tabela ',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planosdesaude` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `registro_ans` int(11) DEFAULT NULL COMMENT 'Referência: http://www.ans.gov.br/planos-de-saude-e-operadoras/informacoes-e-avaliacoes-de-operadoras/consultar-dados',
  `ATIVO` char(1) NOT NULL DEFAULT 'S',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabela com os cadastros dos planos de saúde.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prescricao_enf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `inicio` date DEFAULT NULL,
  `fim` date DEFAULT NULL,
  `cancelado_em` datetime DEFAULT NULL,
  `cancelado_por` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prescricao_enf_cuidado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(255) DEFAULT NULL,
  `presc_enf_grupo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prescricao_enf_grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grupo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prescricao_enf_itens_prescritos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prescricao_enf_id` int(11) DEFAULT NULL,
  `inicio` date DEFAULT NULL,
  `fim` date DEFAULT NULL,
  `frequencia_id` int(11) DEFAULT NULL,
  `presc_enf_cuidados_id` int(11) DEFAULT NULL,
  `aprazamento` varchar(255) DEFAULT NULL,
  `obs` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prescricoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paciente` int(11) NOT NULL,
  `criador` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `lockp` datetime NOT NULL,
  `inicio` date NOT NULL,
  `fim` date NOT NULL,
  `solicitado` datetime NOT NULL,
  `Carater` int(11) NOT NULL DEFAULT '0' COMMENT 'Este campo recebe 1 para emergência 2 para periódica e 3 para Aditivo, 4 para periódica de avaliação, 5 para periódica nutricional, 6 para adtiva nutricional, 7 aditiva de avaliação',
  `ID_CAPMEDICA` int(11) NOT NULL,
  `DESATIVADA_POR` int(11) NOT NULL,
  `DATA_DESATIVADA` datetime NOT NULL,
  `LIBERADO` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Campo usado para prescrições de carater 4 e 7 , S-liberado para enfremagem e N não liberado',
  `LIBERADO_POR` int(11) NOT NULL,
  `DATA_LIBERADO` datetime NOT NULL,
  `PRESCRICAO_ORIGEM` int(11) NOT NULL,
  `STATUS` int(1) NOT NULL DEFAULT '0' COMMENT 'Recebe 0 para ativo e 1 da desativada',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabela das precrições médicas.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prestador_servico_empresa` (
  `PRESTADOR_ID` int(11) NOT NULL,
  `EMPRESA_ID` int(11) NOT NULL,
  `CRIADO_POR` int(11) NOT NULL,
  `CRIADA_EM` datetime NOT NULL,
  `DESATIVADA_POR` int(11) NOT NULL,
  `DESATIVADA_EM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prestadores_servicos` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOGIN` int(14) NOT NULL,
  `SENHA` varchar(100) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `DATA_SENHA` datetime NOT NULL,
  `FORNECEDOR_ID` int(11) NOT NULL,
  `CRIADO_POR` int(11) NOT NULL,
  `DATA_CRIADO` datetime NOT NULL,
  `DATA_DESATIVADO` datetime NOT NULL,
  `DESATIVADO_POR` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `principioativo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `principio` varchar(255) NOT NULL,
  `DescricaoMedico` varchar(255) NOT NULL,
  `AddMederi` int(11) NOT NULL DEFAULT '0' COMMENT '0 Se for do Brasindice e 1 se foi adicionado pela mederi',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'Para confirma se Dr. Lúcio Confirmou ou não (1 para conf. e 0 para não conf.)',
  `usuario` int(11) NOT NULL COMMENT 'Recebe o id do usuário que cadastrou um produto interno',
  `data` datetime NOT NULL COMMENT 'Data em que foi feito o registro',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Princípios ativos dos medicamentos.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `problemaativoevolucao` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVOLUCAO_ID` int(11) NOT NULL,
  `CID_ID` varchar(11) NOT NULL,
  `DESCRICAO` varchar(200) NOT NULL,
  `STATUS` int(5) NOT NULL COMMENT 'Recebe 1 se resolvido, 2-melhor, 3-estavel, 4-pior',
  `OBSERVACAO` text NOT NULL,
  `DATA` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `problemas_relatorio_prorrogacao_enf` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RELATORIO_PRORROGACAO_ENF_ID` int(11) DEFAULT NULL,
  `CID_ID` varchar(11) NOT NULL,
  `DESCRICAO` varchar(200) CHARACTER SET utf8 NOT NULL,
  `OBSERVACAO` text CHARACTER SET utf8 NOT NULL,
  `DATA` datetime NOT NULL,
  `ORIGEM` int(11) NOT NULL,
  `RELATORIO_ADITIVO_ENF_ID` int(11) DEFAULT NULL,
  `RELATORIO_DEFLAGRADO_ENF_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `problemas_relatorio_prorrogacao_med` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RELATORIO_PRORROGACAO_MED_ID` int(11) DEFAULT NULL,
  `CID_ID` varchar(11) NOT NULL,
  `DESCRICAO` varchar(200) NOT NULL,
  `OBSERVACAO` text NOT NULL,
  `DATA` datetime NOT NULL,
  `RELATORIO_ALTA_MED_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `problemasativos` (
  `capmed` int(10) unsigned NOT NULL,
  `cid` varchar(10) NOT NULL COMMENT 'Recebe 1111 quando for outro tipo de problema q n tenha na tabela cid10',
  `DESCRICAO` varchar(300) NOT NULL,
  KEY `capmed` (`capmed`),
  KEY `cid` (`cid`),
  CONSTRAINT `problemasativos_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `cid10` (`codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `problemasativos_ibfk_3` FOREIGN KEY (`capmed`) REFERENCES `capmedica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Problemas ativos registrados em uma capitação.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procedimentos` (
  `ID` tinyint(2) NOT NULL AUTO_INCREMENT,
  `PROCEDIMENTO` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procedimentos_relatorio_prorrogacao_med` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RELATORIO_PRORROGACAO_MED_ID` int(11) NOT NULL,
  `PROCEDIMENTOS_ID` int(11) NOT NULL,
  `DESCRICAO` varchar(200) CHARACTER SET utf8 NOT NULL,
  `JUSTIFICATIVA` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profissionais_relatorio_prorrogacao_med` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RELATORIO_PRORROGACAO_MED_ID` int(11) NOT NULL,
  `CUIDADOSESPECIAIS_ID` int(11) NOT NULL,
  `DESCRICAO` varchar(200) CHARACTER SET utf8 NOT NULL,
  `JUSTIFICATIVA` text CHARACTER SET utf8 NOT NULL,
  `FREQUENCIA_ID` int(11) NOT NULL,
  `ESPECIALIDADE_MEDICA_ID` int(11) NOT NULL,
  `RELATORIO_ALTA_MED_ID` int(11) DEFAULT NULL,
  `JUSTIFICATIVA_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prontuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paciente` int(10) unsigned NOT NULL,
  `pasta` int(10) unsigned NOT NULL,
  `data` datetime NOT NULL,
  `nome` varchar(100) NOT NULL,
  `tipo` varchar(30) NOT NULL,
  `size` int(11) NOT NULL,
  `content` mediumblob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `paciente` (`paciente`),
  KEY `pasta` (`pasta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Documentos armazenados nas pastas de prontuários.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quadro_clinico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quadro_feridas_enf_contaminacao` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONTAMINACAO` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quadro_feridas_enf_exsudato` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EXSUDATO` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quadro_feridas_enf_grau` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GRAU` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quadro_feridas_enf_humidade` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `HUMIDADE` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quadro_feridas_enf_lesao` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIPO` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quadro_feridas_enf_local` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOCAL` text NOT NULL,
  `LADO` char(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quadro_feridas_enf_pele` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PELE` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quadro_feridas_enf_periodo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PERIODO_TROCA` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quadro_feridas_enf_profundidade` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PROFUNDIDADE` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

CREATE TABLE `quadro_feridas_enf_status` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`status_ferida` varchar(100) NOT NULL,
`relatorio` enum('AVALIAÇÃO','ADITIVO','EVOLUÇÃO','PRORROGAÇÃO') DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quadro_feridas_enf_tamanho` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TAMANHO` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quadro_feridas_enf_tecido` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TECIDO` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quadro_feridas_enf_tempo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TEMPO` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quadro_feridas_enf_tipo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIPO` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quadro_feridas_enfermagem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AVALIACAO_ENF_ID` int(11) DEFAULT NULL,
  `EVOLUCAO_ENF_ID` int(11) DEFAULT NULL,
  `LOCAL` int(2) NOT NULL,
  `LADO` int(2) NOT NULL,
  `TIPO` int(2) NOT NULL,
  `COBERTURA` int(2) NOT NULL,
  `TAMANHO` int(2) NOT NULL,
  `PROFUNDIDADE` int(2) NOT NULL,
  `TECIDO` int(2) NOT NULL,
  `GRAU` int(2) NOT NULL,
  `HUMIDADE` int(2) NOT NULL,
  `EXSUDATO` int(2) NOT NULL,
  `CONTAMINACAO` int(2) NOT NULL,
  `PELE` int(2) NOT NULL,
  `TEMPO` int(2) NOT NULL,
  `ODOR` int(2) NOT NULL,
  `PERIODO` int(2) NOT NULL,
  `STATUS_FERIDA` int(2) NOT NULL,
  `FILE_UPLOADED_ID` int(11) unsigned DEFAULT NULL,
  `OBSERVACAO_IMAGEM` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FILE_UPLOADED_ID` (`FILE_UPLOADED_ID`),
  CONSTRAINT `quadro_feridas_enfermagem_ibfk_1` FOREIGN KEY (`FILE_UPLOADED_ID`) REFERENCES `files_uploaded` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rateio` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOTA_ID` int(11) NOT NULL,
  `NOTA_TR` int(11) NOT NULL,
  `CENTRO_RESULTADO` int(11) NOT NULL,
  `VALOR` double(8,2) NOT NULL,
  `PORCENTAGEM` double(8,5) NOT NULL,
  `IPCG3_ANTIGA` varchar(100) CHARACTER SET utf8 NOT NULL,
  `IPCG4_ANTIGA` int(11) NOT NULL,
  `ASSINATURA` int(11) NOT NULL,
  `IPCF2` varchar(200) CHARACTER SET utf8 NOT NULL,
  `IPCG3` varchar(100) COLLATE utf8_bin NOT NULL,
  `IPCG4` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relacionamento_pessoa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RELACIONAMENTO` varchar(30) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relatorio_aditivo_enf` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USUARIO_ID` int(11) DEFAULT NULL,
  `EVOLUCAO_ID` int(11) DEFAULT NULL,
  `DATA` datetime DEFAULT NULL,
  `INICIO` date DEFAULT NULL,
  `FIM` date DEFAULT NULL,
  `SOLICITACAO_ADITIVO` text,
  `PACIENTE_ID` int(11) DEFAULT NULL,
  `ALTERACAO_LESOES` tinyint(11) DEFAULT NULL COMMENT 'RECEBE 2-NAO 1-SIM',
  `FERIDAS` tinyint(4) DEFAULT NULL COMMENT 'RECEBE 2-NAO 1-SIM',
  `confirmado` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'RECEBE 2-NAO 1-SIM',
  `confirmado_por` int(11) DEFAULT NULL,
  `confirmado_em` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relatorio_alta_enf` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USUARIO_ID` int(11) DEFAULT NULL,
  `DATA` datetime DEFAULT NULL,
  `INICIO` date DEFAULT NULL,
  `FIM` date DEFAULT NULL,
  `RESUMO_HISTORIA` text,
  `PACIENTE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relatorio_alta_med` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USUARIO_ID` int(11) NOT NULL,
  `EVOLUCAO_ID` int(11) NOT NULL,
  `PACIENTE_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `INICIO` date NOT NULL,
  `FIM` date NOT NULL,
  `ESTADO_GERAL` int(2) NOT NULL COMMENT 'RECEBE 1-BOM, 2-REGULAR, 3-RUIM',
  `MUCOSA` varchar(2) NOT NULL COMMENT 's-CORADS,N-DESCORADAS',
  `MUCOSA_OBS` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ESCLERAS` varchar(2) NOT NULL COMMENT 'RECEBE S-ANICTERIAS, N-ICTERIAS',
  `ESCLERAS_OBS` varchar(200) CHARACTER SET utf8 NOT NULL,
  `PADRAO_RESPIRATORIO` varchar(2) NOT NULL COMMENT 'S-EUPNEICO, N-TAQUI/DISPNEICO',
  `PADRAO_RESPIRATORIO_OBS` varchar(200) CHARACTER SET utf8 NOT NULL,
  `PA_SISTOLICA` double(6,2) NOT NULL,
  `PA_DIASTOLICA` double(6,2) NOT NULL,
  `FC` double(6,2) NOT NULL,
  `FR` double(6,2) NOT NULL,
  `TEMPERATURA` double(6,2) NOT NULL,
  `DOR` char(1) NOT NULL COMMENT 'Recebe S ou N',
  `OUTRO_RESP` text CHARACTER SET utf8 NOT NULL,
  `OXIMETRIA_PULSO` double(8,2) DEFAULT NULL,
  `OXIMETRIA_PULSO_TIPO` int(11) NOT NULL COMMENT 'Tipo: Ar Ambiente (1) / O2 Suplementar (2)',
  `OXIMETRIA_PULSO_LITROS` double(6,2) NOT NULL,
  `OXIMETRIA_PULSO_VIA` int(11) NOT NULL COMMENT 'Via: Cateter Nasal (1) / Cateter em Traqueostomia (2) / MÃ¡scara de venturi (3) / VentilaÃ§Ã£o MecÃ¢nica (4)',
  `RESUMO_HISTORIA` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relatorio_alta_med_antibiotico` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RELATORIO_ALTA_ID` int(11) NOT NULL,
  `CATALOGO_ID` int(11) NOT NULL,
  `MEDICAMENTO` varchar(250) NOT NULL,
  `NUMERO_TISS` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relatorio_deflagrado_enf` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USUARIO_ID` int(11) NOT NULL,
  `PACIENTE_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `INICIO` datetime NOT NULL,
  `FIM` datetime NOT NULL,
  `HISTORIA_CLINICA` text,
  `SERVICOS_PRESTADOS` text,
  `EDITADO_EM` datetime DEFAULT NULL,
  `EDITADO_POR` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Relatorio deflagrado de enfermagem.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relatorio_deflagrado_med` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `inicio` datetime NOT NULL,
  `fim` datetime NOT NULL,
  `historia_clinica` text,
  `servicos_prestados` text,
  `editado_em` datetime DEFAULT NULL,
  `editado_por` int(11) DEFAULT NULL,
  `cancelado_em` datetime DEFAULT NULL,
  `cancelado_por` int(11) DEFAULT NULL,
  `relatorio_deflagrado_origem` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Relatorio deflagrado de medico.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relatorio_deflagrado_med_problema_ativo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relatorio_deflagrado_med_id` int(11) NOT NULL,
  `cid10_id` varchar(11) NOT NULL,
  `descricao` varchar(200) NOT NULL,
  `observacao` text NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relatorio_prorrogacao_enf` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USUARIO_ID` int(11) DEFAULT NULL,
  `PACIENTE_ID` int(11) DEFAULT NULL,
  `DATA` datetime DEFAULT NULL,
  `INICIO` date DEFAULT NULL,
  `FIM` date DEFAULT NULL,
  `EVOLUCAO_CLINICA` text,
  `ORIENTACOES` text,
  `FERIDAS` varchar(2) DEFAULT NULL,
  `PAD_ENFERMAGEM` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relatorio_prorrogacao_med` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USUARIO_ID` int(11) NOT NULL,
  `EVOLUCAO_ID` int(11) NOT NULL,
  `PACIENTE_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `INICIO` date NOT NULL,
  `FIM` date NOT NULL,
  `ESTADO_GERAL` int(2) NOT NULL COMMENT 'RECEBE 1-BOM, 2-REGULAR, 3-RUIM',
  `MUCOSA` varchar(2) NOT NULL COMMENT 's-CORADS,N-DESCORADAS',
  `MUCOSA_OBS` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ESCLERAS` varchar(2) NOT NULL COMMENT 'RECEBE S-ANICTERIAS, N-ICTERIAS',
  `ESCLERAS_OBS` varchar(200) CHARACTER SET utf8 NOT NULL,
  `PADRAO_RESPIRATORIO` varchar(2) NOT NULL COMMENT 'S-EUPNEICO, N-TAQUI/DISPNEICO',
  `PADRAO_RESPIRATORIO_OBS` varchar(200) CHARACTER SET utf8 NOT NULL,
  `PA_SISTOLICA` double(6,2) NOT NULL,
  `PA_DIASTOLICA` double(6,2) NOT NULL,
  `FC` double(6,2) NOT NULL,
  `FR` double(6,2) NOT NULL,
  `TEMPERATURA` double(6,2) NOT NULL,
  `DOR` char(1) NOT NULL COMMENT 'Recebe S ou N',
  `OUTRO_RESP` text CHARACTER SET utf8 NOT NULL,
  `OXIMETRIA_PULSO` double(8,2) DEFAULT NULL,
  `OXIMETRIA_PULSO_TIPO` int(11) NOT NULL COMMENT 'Tipo: Ar Ambiente (1) / O2 Suplementar (2)',
  `OXIMETRIA_PULSO_LITROS` double(6,2) NOT NULL,
  `OXIMETRIA_PULSO_VIA` int(11) NOT NULL COMMENT 'Via: Cateter Nasal (1) / Cateter em Traqueostomia (2) / MÃ¡scara de venturi (3) / VentilaÃ§Ã£o MecÃ¢nica (4)',
  `MOD_HOME_CARE` int(1) NOT NULL COMMENT 'Recebe 1-Assistencia Domiciliar, 2-InternaÃ§Ã£o Domiciliar 6h, 3-InternaÃ§Ã£o Domiciliar 12h, 4-InternaÃ§Ã£o Domiciliar 24h',
  `DESLOCAMENTO` char(1) NOT NULL,
  `DESLOCAMENTO_DISTANCIA` int(11) NOT NULL,
  `ALTERACAO_SERVICO` tinyint(2) NOT NULL COMMENT 'REGISTRA SE HOUVE ALTERACAO NO SERVICO PARA O PACIENTE. 1-SE SIM, 2-NAO',
  `PRORROGACAO_ORIGEM` int(11) NOT NULL DEFAULT '0',
  `EDITADO_POR` int(11) NOT NULL DEFAULT '0',
  `STATUS` enum('s','n') NOT NULL DEFAULT 's' COMMENT 'S = Qualquer relatório / N = Inativado por edição (Quando editado, o relatório é inativado, mantendo-se o histórico e sendo criado um novo)',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relatorio_visita_enf` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USUARIO_ID` int(11) DEFAULT NULL,
  `PACIENTE_ID` int(11) DEFAULT NULL,
  `DATA` datetime DEFAULT NULL,
  `DATA_VISITA` date DEFAULT NULL,
  `ALTERACOES_SN` varchar(2) DEFAULT NULL,
  `DESCRICAO_ALTERACOES` text,
  `PROBLEMAS_SN` varchar(2) DEFAULT NULL,
  `PROBLEMAS_MATERIAL` text,
  `DIFICULDADE_SN` varchar(2) DEFAULT NULL,
  `DIFICULDADE_PROFISSIONAL` text,
  `FAMILIA_SN` varchar(2) DEFAULT NULL,
  `DIFICULDADE_FAMILIA` text,
  `ORIENTACOES` text,
  `EDITADO_EM` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remocoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `paciente` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `solicitacao` date NOT NULL,
  `servico` date NOT NULL,
  `origem` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `destino` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `km` double NOT NULL,
  `valor` double NOT NULL,
  `tipo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `medico` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `vmedico` double NOT NULL,
  `tecnico` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `vtecnico` double NOT NULL,
  `condutor` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `vcondutor` double NOT NULL,
  `solicitante` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `idNota` int(10) unsigned NOT NULL,
  `cpfcnpj` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `contato` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cod` (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabela com o registro das remoções.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repousos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `repouso` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lista de reoupsos.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saida` (
  `idSaida` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` int(10) unsigned NOT NULL,
  `NUMERO_TISS` varchar(11) CHARACTER SET utf8 NOT NULL,
  `CATALOGO_ID` int(11) NOT NULL,
  `nome` varchar(255) CHARACTER SET utf8 NOT NULL,
  `segundoNome` varchar(255) CHARACTER SET utf8 NOT NULL,
  `quantidade` int(10) NOT NULL,
  `valor` double NOT NULL,
  `data` date NOT NULL,
  `idCliente` int(10) unsigned NOT NULL,
  `idUsuario` int(10) unsigned NOT NULL,
  `loc` varchar(255) CHARACTER SET utf8 NOT NULL,
  `obs` varchar(255) CHARACTER SET utf8 NOT NULL,
  `idSolicitacao` int(11) NOT NULL,
  `LOTE` varchar(20) DEFAULT NULL,
  `FATURADO` char(1) NOT NULL DEFAULT 'N',
  `ITENSPEDIDOSINTERNOS_ID` int(11) NOT NULL,
  `UR` int(11) NOT NULL,
  `DATA_SAIDA` date NOT NULL,
  `DATA_SISTEMA` datetime NOT NULL,
  PRIMARY KEY (`idSaida`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro das saídas para pacientes.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saida_interna` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIPO` int(11) NOT NULL,
  `QUANTIDADE` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `COLABORADOR` varchar(150) NOT NULL,
  `USUARIO_ID` int(11) NOT NULL,
  `DATA_SAIDA_INTERNA` date NOT NULL,
  `NUMERO_TISS` int(11) NOT NULL,
  `LOTE` varchar(11) NOT NULL,
  `CATALOGO_ID` int(11) NOT NULL,
  `EMPRESA_ID` int(11) NOT NULL,
  `VENCIDO` int(1) NOT NULL COMMENT 'Se for 0 não eh vencido caso seja 1 eh item vencido',
  `DATA_VENCIMENTO` date NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `score` (
  `ID` int(2) NOT NULL AUTO_INCREMENT,
  `DESCRICAO` varchar(30) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `score_avulso` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_PACIENTE` int(11) NOT NULL,
  `ID_USUARIOS` int(5) NOT NULL,
  `DATA` datetime NOT NULL,
  `ID_ITEM_SCORE` int(11) NOT NULL,
  `OBSERVACAO` varchar(200) NOT NULL,
  `ID_AVULSO` int(11) NOT NULL,
  `JUSTIFICATIVA` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `score_item` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GRUPO_ID` int(5) NOT NULL,
  `SCORE_ID` int(2) NOT NULL,
  `DESCRICAO` varchar(100) CHARACTER SET utf8 NOT NULL,
  `PESO` int(2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `score_paciente` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_PACIENTE` int(11) NOT NULL,
  `ID_USUARIOS` int(5) NOT NULL,
  `DATA` datetime NOT NULL,
  `ID_ITEM_SCORE` int(5) NOT NULL,
  `ID_CAPMEDICA` int(5) NOT NULL,
  `OBSERVACAO` varchar(200) CHARACTER SET utf8 NOT NULL COMMENT 'Recebe dados do cabeçario do score ex: diagnostico principal',
  `ID_AVALIACAO_ENFERMAGEM` int(5) NOT NULL,
  `ID_RELATORIO` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` enum('on','off') NOT NULL DEFAULT 'on',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servico_terceirizado_prestador` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SERVICO_TERCEIRIZADO_ID` int(11) NOT NULL,
  `PRESTADOR_ID` int(11) NOT NULL,
  `EMPRESA_ID` int(11) NOT NULL,
  `VALOR` double(8,2) NOT NULL,
  `CRIADO_EM` datetime NOT NULL,
  `CRIADO_POR` int(11) NOT NULL,
  `DESATIVADO_EM` datetime NOT NULL,
  `DESATIVADO_POR` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicos_terceirizados` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SERVICO` varchar(100) NOT NULL,
  `TIPO_PROFISSIONAL` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `simpro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_usuario` varchar(15) DEFAULT NULL,
  `codigo_fracao` varchar(15) DEFAULT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `vigencia` date DEFAULT NULL,
  `identificacao` char(1) DEFAULT NULL COMMENT 'F - Preço Fábrica\r\nV - Preço Venda\r\nL - Fora de Linha\r\nA - Atualiação Suspensa\r\nD - Descontinuado',
  `preco_fabrica_embalagem` int(11) DEFAULT NULL,
  `preco_venda_embalagem` int(11) DEFAULT NULL,
  `preco_usuario_embalagem` int(11) DEFAULT NULL,
  `preco_fabrica_fracao` int(12) DEFAULT NULL,
  `preco_venda_fracao` int(12) DEFAULT NULL,
  `preco_usuario_fracao` int(12) DEFAULT NULL,
  `tipo_embalagem` varchar(3) DEFAULT NULL,
  `tipo_fracao` varchar(4) DEFAULT NULL,
  `quantidade_embalagem` int(8) DEFAULT NULL,
  `quantidade_fracao` int(8) DEFAULT NULL,
  `lucratividade_usuario` int(6) DEFAULT NULL COMMENT '% lucratividade do Usúario',
  `tipo_alteracao` char(1) DEFAULT NULL COMMENT 'I - Inclusão\r\nP - Preço\r\nL - Fora de Linha\r\nA - Alteração Gerais\r\nS - Atualização Simpro\r\nD - Descontinuado',
  `fabricante` varchar(20) DEFAULT NULL,
  `codigo_simpro_tiss` varchar(10) DEFAULT NULL,
  `codigo_mercado` varchar(2) DEFAULT NULL COMMENT '20 - Material\r\n70 - Perfumaria\r\n90 - Reagentes\r\n50 - Medicamentos\r\n30 - Saneamento ',
  `percentual_desconta` int(6) DEFAULT NULL,
  `ipi_produto` int(4) DEFAULT NULL,
  `numero_registri_anvisa` varchar(18) DEFAULT NULL COMMENT 'Número registro Anvisa ou uma das senguintes informações: Isento, Não controlado, notificado e SI/NC',
  `data_validade_registro_anvisa` varchar(13) DEFAULT NULL COMMENT 'Data de validade do registro Anvisa ou uma das seguintes informações: Pendente ou sob protocolo.',
  `codigo_barra` int(13) DEFAULT NULL,
  `tipo_lista` char(1) DEFAULT NULL COMMENT '''+''  Positiva\r\n''-''  Negativa\r\n'' ''  Neutra\r\n''#'' Não aplicável',
  `exclusivamente_hospitalar` char(1) DEFAULT NULL,
  `item_fracionado` char(1) DEFAULT NULL COMMENT 'S- sim\r\nN-não',
  `codigo_tuss` varchar(8) DEFAULT NULL,
  `clasificacao_produto` varchar(2) DEFAULT NULL COMMENT 'Indica a classificação do produto.\r\n( ) Sem classificação.\r\n(MC) Material de consumo\r\n(BD) Bem Duravel\r\n(ME) Material Especial\r\n(OT) Ortese\r\n(PT) Protese\r\n(ST) Sintese\r\n(IT) instrumental',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solicitacao_rel_aditivo_enf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relatorio_enf_aditivo_id` int(11) NOT NULL,
  `catalogo_id` int(11) NOT NULL,
  `tiss` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `quantidade_kit` int(11) NOT NULL,
  `kit_id` int(11) NOT NULL,
  `cancelado_por` int(11) DEFAULT NULL,
  `cancelado_em` datetime DEFAULT NULL,
  `entrega_imediata` enum('N','S') DEFAULT NULL,
  `data_entrega` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solicitacoes` (
  `idSolicitacoes` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paciente` int(10) unsigned NOT NULL,
  `enfermeiro` int(10) unsigned NOT NULL,
  `NUMERO_TISS` varchar(11) NOT NULL,
  `CATALOGO_ID` int(11) NOT NULL,
  `qtd` int(10) unsigned NOT NULL,
  `autorizado` int(11) NOT NULL COMMENT 'Campo alimentado com as quantidades autorizadas pela auditoria',
  `enviado` int(10) unsigned NOT NULL DEFAULT '0',
  `tipo` int(10) unsigned NOT NULL COMMENT '0 - medicamento, 1-  Material,3 - Dieta, 5 - Equipamento ',
  `data` date NOT NULL COMMENT 'Data do Aprazamento',
  `status` int(3) NOT NULL DEFAULT '0' COMMENT '-2 - CANCELADO / -1 - NEGADO',
  `obs` varchar(255) NOT NULL,
  `envioCompleto` datetime NOT NULL COMMENT 'Data em que foi enviado pela logística',
  `VERIFICA` int(11) NOT NULL COMMENT 'Campo criado para saber quais solicitações foram finalizadas no banco por Ricardo',
  `idPrescricao` int(11) NOT NULL COMMENT 'Este campo recebe o id da prescrição que ela está associada',
  `data_auditado` datetime NOT NULL COMMENT 'Data em que fou autorizado pela auditoria',
  `TIPO_SOL_EQUIPAMENTO` int(1) NOT NULL COMMENT 'Se 1-enviar, 2 recolher',
  `CHAVE_AVULSA` int(11) NOT NULL,
  `DATA_SOLICITACAO` datetime NOT NULL,
  `FATURADO` char(1) NOT NULL COMMENT 'Recebe N por padrão e S se a solicitação já foi faturada.',
  `DATA_MAX_ENTREGA` datetime NOT NULL,
  `TIPO_SOLICITACAO` int(1) NOT NULL COMMENT 'Recebe -2 EMERGENCIA ENFERMAGEM, -1 - AVULSA, 2 - PERIODICA, 3-ADITIVA,4-AVALIACAO,8-ADITIVO ENFERMAGEM',
  `KIT_ID` int(3) NOT NULL DEFAULT '0',
  `PENDENTE` char(1) DEFAULT 'N' COMMENT 'N-NAO ESTA PENDENTE; S- ESTA PENDENTE',
  `OBS_PENDENTE` text COMMENT 'OBSERVACOES DE PENDENCIA NA AUDITORIA DE CADA ITEM',
  `AUTOR_PENDENCIA` int(10) DEFAULT NULL,
  `DATA_PENDENCIA` datetime DEFAULT NULL,
  `CANCELADO_POR` int(11) NOT NULL,
  `DATA_CANCELADO` datetime NOT NULL,
  `JUSTIFICATIVA_AVULSO` text,
  `JUSTIFICATIVA_AVULSO_ID` int(11) DEFAULT '0',
  `JUSTIFICATIVA_ITENS_NEGADOS_ID` int(11) DEFAULT NULL,
  `AUDITOR_ID` int(11) DEFAULT NULL,
  `entrega_imediata` enum('S','N') DEFAULT NULL,
  `visualizado` int(11) NOT NULL DEFAULT '0',
  `relatorio_aditivo_enf_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`idSolicitacoes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Solicitacões da enfermagem.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `soros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `soro` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lista de soros.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statuspaciente` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabela com os tipos de status do paciente.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subnatureza` (
  `idSubNatureza` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tipo` tinyint(1) NOT NULL,
  `natureza` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idSubNatureza`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Subnatureza das notas.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sugestoes_alteracao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sugestao` int(11) DEFAULT NULL,
  `alterado_por` int(11) DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sugestoes_secmed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_capmedica` int(11) DEFAULT NULL,
  `id_paciente` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `local_sugestao` enum('fcav','prsc') DEFAULT NULL COMMENT 'fcav = Ficha de AvaliaÃ§Ã£o, prsc = PrescriÃ§Ã£o',
  `data_sugestao` datetime DEFAULT NULL,
  `data_resposta` datetime DEFAULT NULL COMMENT 'Ultima alteraÃ§Ã£o do mÃ©dico',
  `status` tinyint(1) DEFAULT NULL COMMENT '0 para em processo de correÃ§Ã£o, 1 para totalmente corrigido',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sugestoes_secmed_lista` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sugestao` int(11) DEFAULT NULL,
  `tipo_sugestao` enum('erp','inc') DEFAULT NULL COMMENT 'erp = Erro de preenchimento, inc = Inconformidade',
  `secao_sugestao` enum('eqp','pm','ium','com','aa','am','pa','mh','fam','rep','die','ce','se','ai','iv','im','sc','di','neb','doe','dt','susp','oxi','doep','mat') DEFAULT NULL,
  `sugestao` text,
  `status` tinyint(1) DEFAULT NULL COMMENT '0 para não corrigido, 1 para corrigido',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_conta_bancaria` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `abreviacao` varchar(5) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_logradouro` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) NOT NULL,
  `tiss_tp_logradouro` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_acomodacao` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) NOT NULL DEFAULT '',
  `tiss_codigo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_carater_atendimento` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_codigo` int(11) NOT NULL,
  `sigla` char(1) NOT NULL DEFAULT '',
  `descricao` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_conselho_profissional` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_codigo` int(11) NOT NULL,
  `sigla` varchar(10) NOT NULL DEFAULT '',
  `descricao` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_contratado` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) NOT NULL,
  `numero_cnes` varchar(40) NOT NULL DEFAULT '',
  `cnpj` varchar(40) NOT NULL DEFAULT '',
  `razao_social` varchar(150) NOT NULL DEFAULT '',
  `tipo_logradouro_id` int(11) NOT NULL,
  `logradouro` varchar(150) NOT NULL DEFAULT '',
  `numero` varchar(10) NOT NULL DEFAULT '',
  `codigo_ibge_municipio` varchar(40) NOT NULL DEFAULT '',
  `municipio` varchar(100) NOT NULL DEFAULT '',
  `codigo_uf` varchar(2) NOT NULL DEFAULT '',
  `cep` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `empresa_id` (`empresa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_fatura` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_guia_id` int(11) NOT NULL,
  `fatura_id` int(11) NOT NULL,
  `clientes_id` int(11) NOT NULL,
  `numero_carteira` varchar(100) NOT NULL DEFAULT '',
  `nome_beneficiario` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_fatura_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_fatura_id` int(11) NOT NULL,
  `tiss_tabela_id` int(11) NOT NULL,
  `tiss_outras_despesas_tipo_id` int(11) NOT NULL DEFAULT '0',
  `unidades_fatura_id` int(11) NOT NULL,
  `data` date NOT NULL,
  `codigo` varchar(50) NOT NULL DEFAULT '',
  `descricao` varchar(255) NOT NULL DEFAULT '',
  `quantidade` int(11) NOT NULL,
  `valor_unitario` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_fatura_spsadt` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_fatura_id` int(11) NOT NULL,
  `tiss_guia_sadt_id` int(11) NOT NULL,
  `tiss_guia_id` int(11) NOT NULL,
  `clientes_id` int(11) NOT NULL,
  `numero_carteira` varchar(100) NOT NULL DEFAULT '',
  `atendimento_rn` enum('S','N') DEFAULT NULL,
  `nome_beneficiario` varchar(150) NOT NULL DEFAULT '',
  `numero_cns` varchar(15) DEFAULT NULL,
  `identificador_beneficiario` varchar(100) DEFAULT '',
  `nome_plano` varchar(100) DEFAULT '',
  `data_atendimento` date NOT NULL,
  `cid10_codigo` varchar(11) NOT NULL DEFAULT '',
  `carater_atendimento` enum('E','U') NOT NULL COMMENT 'E = ELETIVA / U = URGÊNCIA',
  `tiss_carater_atendimento_id` int(11) DEFAULT NULL,
  `tiss_tipo_atendimento_id` int(11) NOT NULL,
  `tiss_indicador_acidente_id` int(11) NOT NULL,
  `tiss_tipo_saida_id` int(11) NOT NULL,
  `tipo_doenca` enum('A','C') NOT NULL COMMENT 'A = Agudo / C = Crônico',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_guia` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_lote_id` int(11) NOT NULL,
  `tiss_contratado_id` int(11) NOT NULL,
  `tiss_profissional_id` int(11) NOT NULL,
  `clientes_id` int(11) NOT NULL,
  `numero_carteira` varchar(100) NOT NULL DEFAULT '',
  `planosdesaude_id` int(11) NOT NULL,
  `tipo_guia` varchar(100) NOT NULL DEFAULT '',
  `nome_beneficiario` varchar(150) NOT NULL DEFAULT '',
  `numero_cns` varchar(15) DEFAULT NULL,
  `nome_plano` varchar(100) DEFAULT '',
  `numero_guia_operadora` varchar(50) DEFAULT NULL,
  `data_autorizacao` date NOT NULL,
  `senha` varchar(20) NOT NULL DEFAULT '',
  `validade_senha` date DEFAULT NULL,
  `registration` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_guia_resumo_internacao` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_guia_id` int(11) NOT NULL,
  `tiss_tipo_internacao_id` int(11) NOT NULL,
  `tiss_acomodacao_id` int(11) NOT NULL,
  `tiss_regime_internacao_id` int(11) NOT NULL,
  `tiss_motivo_saida_internacao_id` int(11) NOT NULL,
  `tiss_tipo_faturamento_id` int(11) NOT NULL,
  `tiss_indicador_acidente_id` int(11) NOT NULL,
  `carater_atendimento` tinyint(11) NOT NULL,
  `data_hora_saida_internacao` datetime DEFAULT NULL,
  `data_inicio_faturamento` datetime NOT NULL,
  `data_fim_faturamento` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_guia_sadt` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_guia_id` int(11) NOT NULL,
  `numero_lote` varchar(50) DEFAULT '',
  `tiss_lote_id` int(11) NOT NULL,
  `tiss_contratado_id` int(11) NOT NULL,
  `planosdesaude_id` int(11) NOT NULL,
  `numero_guia` int(11) DEFAULT NULL,
  `numero_guia_principal` varchar(20) DEFAULT NULL,
  `data_emissao_guia` date NOT NULL,
  `numero_guia_prestador` int(11) DEFAULT NULL,
  `numero_guia_operadora` varchar(20) NOT NULL,
  `data_autorizacao` date NOT NULL,
  `senha_autorizacao` varchar(100) NOT NULL,
  `validade_senha` date NOT NULL,
  `registration` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_indicador_acidente` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_codigo` int(11) NOT NULL,
  `descricao` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_lote` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `numero_lote` varchar(50) NOT NULL DEFAULT '',
  `registration` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_motivo_saida` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) NOT NULL DEFAULT '',
  `tiss_codigo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_outras_despesas_tipo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) NOT NULL DEFAULT '',
  `tiss_codigo` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_outras_despesas_tipo_interno` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_outras_despesas_tipo_id` int(11) NOT NULL,
  `codigo_interno` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `codigo_interno_unique` (`codigo_interno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_profissional` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_conselho_profissional_id` int(11) DEFAULT NULL,
  `nome_completo` varchar(150) NOT NULL DEFAULT '',
  `cpf` varchar(20) NOT NULL,
  `sigla_conselho` enum('CRM') DEFAULT NULL,
  `numero_conselho` int(11) NOT NULL,
  `conselho_estado_id` int(11) NOT NULL,
  `uf_conselho` enum('BA','SP','GO') NOT NULL,
  `especialidade_medica_id` int(11) NOT NULL,
  `cbos` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_profissional_fatura_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_profissional_id` int(11) NOT NULL,
  `tiss_fatura_item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_regime_internacao` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) NOT NULL DEFAULT '',
  `tiss_codigo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_tabela` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) NOT NULL DEFAULT '',
  `tiss_codigo` int(11) DEFAULT NULL,
  `descricao_completa` varchar(100) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_tipo_atendimento` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_codigo` char(2) NOT NULL DEFAULT '',
  `descricao` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_tipo_faturamento` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) NOT NULL DEFAULT '',
  `tiss_codigo` char(1) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_tipo_internacao` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) NOT NULL DEFAULT '',
  `tiss_codigo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_tipo_saida` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_codigo` int(11) NOT NULL,
  `descricao` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_unidade_medida` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_codigo` int(11) NOT NULL,
  `abreviacao` varchar(20) NOT NULL DEFAULT '',
  `descricao` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiss_xml` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tiss_versao` enum('2.02.03','03.02.00') DEFAULT NULL,
  `tiss_lote_id` int(11) NOT NULL,
  `header` varchar(200) NOT NULL DEFAULT '',
  `plan_text` longblob NOT NULL,
  `epilogo` varchar(200) NOT NULL,
  `registration` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transferencia_bancaria` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USUARIO_ID` int(11) NOT NULL,
  `DATA` datetime NOT NULL,
  `DATA_TRANSFERENCIA` date NOT NULL COMMENT 'data que foi feita a transação',
  `CONTA_ORIGEM` int(11) NOT NULL COMMENT 'Recebe o id do banco da tabela contas_bancarias',
  `CONTA_DESTINO` int(11) NOT NULL COMMENT 'Recebe o id do banco da tabela contas_bancarias',
  `VALOR` double(8,2) NOT NULL,
  `CONCILIADO_ORIGEM` char(1) CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `CONCILIADO_DESTINO` char(1) CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `NUM_DOCUMENTO_BANCARIO` text NOT NULL,
  `DESCRICAO` text NOT NULL,
  `DATA_CONCILIADO_ORIGEM` date NOT NULL,
  `DATA_CONCILIADO_DESTINO` date NOT NULL,
  `CANCELADA_POR` int(11) DEFAULT NULL,
  `CANCELADA_EM` datetime DEFAULT NULL,
  `JUSTIFICATIVA` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umedidas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unidade` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Unidades de medidas.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidades_fatura` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UNIDADE` varchar(30) NOT NULL,
  `tiss_codigo` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_historico_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_modificado` int(11) DEFAULT NULL,
  `usuario_id_modificador` int(11) DEFAULT NULL,
  `data_modificado` date DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `observacao` text,
  `acao` enum('DESATIVAR','ATIVAR') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `idUsuarios` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '12345',
  `nome` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `adm` tinyint(1) NOT NULL DEFAULT '0',
  `tipo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `empresa` int(11) NOT NULL,
  `modadm` tinyint(1) NOT NULL,
  `modaud` tinyint(1) NOT NULL,
  `modenf` tinyint(1) NOT NULL,
  `modfin` tinyint(1) NOT NULL,
  `modlog` tinyint(1) NOT NULL,
  `modmed` tinyint(1) NOT NULL,
  `modrem` tinyint(1) NOT NULL,
  `modnutri` tinyint(1) NOT NULL,
  `modfat` tinyint(1) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `contato1` varchar(14) DEFAULT NULL,
  `contato2` varchar(14) DEFAULT NULL,
  `LOGADO` int(11) NOT NULL,
  `ADM_UR` int(1) NOT NULL DEFAULT '0' COMMENT 'Recebe 0-não e 1-sim',
  `ASSINATURA` varchar(100) NOT NULL,
  `block_at` date DEFAULT '9999-12-31',
  PRIMARY KEY (`idUsuarios`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Usuários do sistema.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios_empresa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `usuarios_id` int(11) NOT NULL,
  `empresas_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_usuarios_empresa_usuarios1` (`usuarios_id`),
  KEY `fk_usuarios_empresa_empresas1` (`empresas_id`),
  CONSTRAINT `fk_usuarios_empresa_empresas1` FOREIGN KEY (`empresas_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuarios_empresa_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`idUsuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valorescobranca` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idPlano` int(10) unsigned NOT NULL,
  `idCobranca` int(10) unsigned NOT NULL,
  `valor` double unsigned NOT NULL,
  `COD_COBRANCA_PLANO` varchar(20) NOT NULL,
  `DESC_COBRANCA_PLANO` varchar(100) NOT NULL,
  `CUSTO_COBRANCA_PLANO` double(6,2) NOT NULL,
  `excluido_por` int(11) DEFAULT NULL,
  `excluido_em` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idPlano` (`idPlano`,`idCobranca`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Valores dos itens de cobrança de cada plano.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `viaadm` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `via` varchar(50) NOT NULL,
  `dieta` tinyint(1) NOT NULL,
  `soro` tinyint(1) NOT NULL,
  `orais` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Via de administração dos medicamentos.';
/*!40101 SET character_set_client = @saved_cs_client */;
