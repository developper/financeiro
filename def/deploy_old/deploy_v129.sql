

----------------------------------------- Sm-817 ---------------------------------------------------------------------
UPDATE `frequencia` SET `qtd`='4' WHERE (`id`='6');

---------
--criar grupo de cuidados
----------
DROP TABLE IF EXISTS `prescricao_enf_grupo`;
CREATE TABLE `prescricao_enf_grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grupo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of prescricao_enf_grupo
-- ----------------------------
INSERT INTO `prescricao_enf_grupo` VALUES ('1', 'FAZER');
INSERT INTO `prescricao_enf_grupo` VALUES ('2', 'MANTER');
INSERT INTO `prescricao_enf_grupo` VALUES ('3', 'OBSERVAR, ANOTAR E COMUNICAR');
INSERT INTO `prescricao_enf_grupo` VALUES ('4', 'TROCAR');
INSERT INTO `prescricao_enf_grupo` VALUES ('5', 'ATENTAR');
INSERT INTO `prescricao_enf_grupo` VALUES ('6', 'CONTROLE DA DOR');
INSERT INTO `prescricao_enf_grupo` VALUES ('7', 'CUIDADOS ESPECIAIS');



-- ----------------------------
-- Table structure for prescricao_enf_cuidado
-- ----------------------------
DROP TABLE IF EXISTS `prescricao_enf_cuidado`;
CREATE TABLE `prescricao_enf_cuidado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(255) DEFAULT NULL,
  `presc_enf_grupo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of prescricao_enf_cuidado
-- ----------------------------
INSERT INTO `prescricao_enf_cuidado` VALUES ('1', 'Fazer administração do medicamento conforme PM, checando em  prontuário', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('2', 'Fazer rodízio do local de aplicação de medicamento subcutâneo ( atentar para hematomas)', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('3', 'Fazer aspiração de V.A.S. e S/N e registrar em prontuário aspecto da secreção', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('4', 'Realiza banho de aspersão em cadeira higiênica (atentar para evitar risco de quedas no banheiro)', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('5', 'Realizar banho no leito ( iniciar higiene face-dorso- genitálias) e registrar em prontuário', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('6', 'Fazer controle de glicemia conforme PM, e registrar em prontuário os valores encontrados', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('7', 'Fazer controle de sinais vitais- PA/FC/FR/T/ e SAT.O2 a cada 2hs (comunicar alterações)', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('8', 'Fazer controle de sinais vitais- PA/FC/FR/T/ e SAT.O2 a cada 4hs (comunicar alterações)', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('9', 'Fazer controle de sinais vitais- PA/FC/FR/T/ e SAT.O2  a cada 6hs (comunicar alterações)', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('10', 'Fazer controle de sinais vitais- PA/FC/FR/T/ e SAT.O2 a cada 8hs (comunicar alterações)', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('11', 'Fazer curativo em traqueo e gastro (usar soro e gase esteril e cobertura quando prescrito), registrar em prontuário', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('12', 'Fazer curativo técnica estéril e registrar em prontuário, conforme prescrição da enfermeira', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('13', 'Instalar dieta em bomba de infusão ml/h conforme PN. Identificar dieta, equipo)', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('14', 'Heparinizar acesso venoso central após medicação', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('15', 'Heparinizar acesso venoso períférico após medicação', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('16', 'Fazer higiene íntima com água e sabão após eliminações vesico-intestinais', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('17', 'Fazer Higiene íntima a cada troca de fralda', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('18', 'Realizar aplicação de loção dermoprotetor / loção hidratante a cada troca de fralda', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('19', 'Realizar higiene oral e limpeza dos dentes', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('20', 'Realizar mudança de decúbito a cada 2hs ( seguir posição do relógio)', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('21', 'Lavar GTT com 20ml de água filtrada a cada administração de medicamento', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('22', 'Realizar limpeza corrente c/ alcool a 70% de ejetores lateriais, torneirinhas e polifix', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('23', 'Realizar limpeza da máscara do nebulizador com água e sabão após inalação e manter protegida', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('24', 'Realizar limpeza do óstio da GTT com gaze não esteril, quando ausência de lesão', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('25', 'Realizar massagem de conforto com hidrante após banho', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('26', 'Realizar massagem de conforto  a cada 3 hs para paciente com pele prejudicada', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('27', 'Realizar limpeza da traqueostomia metálica (usar água e sabão, retirar parte móvel);', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('28', 'Realizar curativo em acesso venoso central , PICC, port-o-cath pela enfermeira usando protocolo da empresa', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('29', 'Instalar colchão caixa de ovo em pacientes com risco de lesão de pele conforme prescrição da enfermeira', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('30', 'Salinizar acesso venoso central após cada medicação', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('31', 'Sinalizar acesso venoso periférico após medicação', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('32', 'Realizar conferência do material diariamente (colocar quantidade do material encontrado no início de cada plantão)', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('33', 'Fazer limpeza dos equipamentos da empresa (manter fios sem dobra, equipamentos elétricos na tomada)', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('34', 'Realizar balanço hídrico a cada 12hs conforme PM', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('35', 'Estimular o autocuidado', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('36', 'Não aspirar GTM, em caso de vômito e distenção abdominal pausar dieta e informar', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('37', 'Lavar umidificador a cada 24hs- usar água e sabão', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('38', 'Pausar dieta em bomba de infusão no momento do banho', '1');
INSERT INTO `prescricao_enf_cuidado` VALUES ('39', 'Manter cateter de O2 coberto quando não estiver em uso', '0');
INSERT INTO `prescricao_enf_cuidado` VALUES ('40', 'Manter sempre a cabeceira elevada a 45º', '2');
INSERT INTO `prescricao_enf_cuidado` VALUES ('41', 'Manter Materiais conferidos e organizados', '2');
INSERT INTO `prescricao_enf_cuidado` VALUES ('42', 'Manter Paciente confortável', '2');
INSERT INTO `prescricao_enf_cuidado` VALUES ('43', 'Manter paciente seco e higienizado', '2');
INSERT INTO `prescricao_enf_cuidado` VALUES ('44', 'Manter quarto do paciente organizado (todo material da empresa deverá estar acessível a equipe)', '2');
INSERT INTO `prescricao_enf_cuidado` VALUES ('45', 'Manter paciente e cuidador orientado dos cuidados prestados', '2');
INSERT INTO `prescricao_enf_cuidado` VALUES ('46', 'Manter repouso restrito ao leito quando prescrito', '2');
INSERT INTO `prescricao_enf_cuidado` VALUES ('47', 'Manter repouso parcial ao leito, com acompanhamento na deambulação', '2');
INSERT INTO `prescricao_enf_cuidado` VALUES ('48', 'Manter grades da cama suspensa para evitar risco de queda', '2');
INSERT INTO `prescricao_enf_cuidado` VALUES ('49', 'Manter oximetria de pulso sempre que aspirar paciente', '2');
INSERT INTO `prescricao_enf_cuidado` VALUES ('50', 'Manter precauções de isolamento de contato ( usar máscara, gorro, capa descartável a cada manipulação)', '2');
INSERT INTO `prescricao_enf_cuidado` VALUES ('51', 'Manter prontuário com as prescrições atualizadas', '2');
INSERT INTO `prescricao_enf_cuidado` VALUES ('52', 'Observar aspecto/ frequencia das eliminações vesico-intestinais, e registrar em prontuário', '3');
INSERT INTO `prescricao_enf_cuidado` VALUES ('53', 'Comunicar deficit no autocuidado', '3');
INSERT INTO `prescricao_enf_cuidado` VALUES ('54', 'Observar frequencia e intensidade da dor (conforme escala)', '3');
INSERT INTO `prescricao_enf_cuidado` VALUES ('55', 'Comunicar se alterações do nível de consciência  (observar sonolência excessiva, não responsividade a estímulos, hipoatividade)', '3');
INSERT INTO `prescricao_enf_cuidado` VALUES ('56', 'Comunicar se dispnéia ou alteração no padrão respiratório', '3');
INSERT INTO `prescricao_enf_cuidado` VALUES ('57', 'Comunicar se dispneia,se saturação de oxigênio menor 90%', '3');
INSERT INTO `prescricao_enf_cuidado` VALUES ('58', 'Comunicar se distensão abdominal', '3');
INSERT INTO `prescricao_enf_cuidado` VALUES ('59', 'Observar se muda aspecto, cor e volume da secreção pulmonar', '3');
INSERT INTO `prescricao_enf_cuidado` VALUES ('60', 'Observar e comunicar se sangramentos, hematomas ou edemas', '3');
INSERT INTO `prescricao_enf_cuidado` VALUES ('61', 'Comunicar se  temperatura acima de 37,7, registrar em prontuário', '3');
INSERT INTO `prescricao_enf_cuidado` VALUES ('62', 'Comunicar se taquicardia e/ou febre', '3');
INSERT INTO `prescricao_enf_cuidado` VALUES ('63', 'Sinais flogísticos em gastrostomia', '3');
INSERT INTO `prescricao_enf_cuidado` VALUES ('64', 'Observar integridade da pele em genitálias a cada troca de fralda', '3');
INSERT INTO `prescricao_enf_cuidado` VALUES ('65', 'Anotar em prontuário toda orientação verbal recebida da equipe médica e enfermeira', '3');
INSERT INTO `prescricao_enf_cuidado` VALUES ('66', 'Em caso de dúvida  quanto caso clínico seguir organograma da empresa', '3');
INSERT INTO `prescricao_enf_cuidado` VALUES ('67', 'Comunicar a base em caso de intercorrências comunicar (alteração clínica dos sinais vitais, febra, convulsão, queda, rebaixamento do sensório)', '3');
INSERT INTO `prescricao_enf_cuidado` VALUES ('68', 'Anotar passagem de plantão de forma objetiva ( registrar em livro de ocorrência)', 3);
INSERT INTO `prescricao_enf_cuidado` VALUES ('69', 'Trocar curativo de acesso venosos períférico a cada 72hs ou S/N', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('70', 'Trocar equipo a cada 72hs ou S/N', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('71', 'Trocar filtro do CPAP a cada 15 dias', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('72', 'Trocar filtro do VM  a cada 15 dias', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('73', 'Trocar filtro barreira/viral a cada 72hs', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('74', 'Trocar filtro HYGOBAC a cada 2 dias', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('75', 'Trocar polifix a cada 72hs ou S/N', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('76', 'Trocar bolsa coletora de diurese sistema aberto mensalmente', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('77', 'Trocar dispositivo urinário a cada 24hs', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('78', 'Trocar coberturas preventivas de proeminência óssea a cada 7 dias', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('79', 'Trocar curativo tegaderme de acesso central, picc e port-o-cath a cada 7 dias', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('80', 'Troca dos equipos para água e dieta a cada 72hs', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('81', 'Trocar água destilada do umidificador a cada 24hs', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('82', 'Trocar cateter de oxigênio a cada 7 dias', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('83', 'Trocar equipos de dieta a cada 72hs', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('84', 'Trocar equipo de soroterapia ou medicação a cada 4 dias', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('85', 'Trocar equipo de ATB cada 24hs', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('86', 'Trocar frasco de dieta diário', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('87', 'Trocar frasco e equipo para água a cada 72hs', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('88', 'Trocar frascos de água a cada 72hs', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('89', 'Trocar seringa de dieta e medicação a cada 24hs', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('90', 'trocar traqueofix semanalmente', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('91', 'Trocar filtro bacteriológico a cada 72hs ( atentar para presença de secreção ou gotículas)', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('92', 'Trocar traquéia do ventilador mensalmente', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('93', 'Trocar latex de aspiração a cada semana', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('94', 'Trocar latex para oxigênio mensal', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('95', 'Trocar sonda de aspiração a cada uso', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('96', 'Trocar curativo conforme prescrição da enfermagem ( vide orientação em impresso próprio)', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('97', 'Trocar colchão caixa de ovo a cada 3 meses', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('98', 'Trocar bolsa de colostomia e placa a cada 7 dias', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('99', 'Trocar traqueofix a cada 15 dias', '4');
INSERT INTO `prescricao_enf_cuidado` VALUES ('100', 'Atentar para circulação períférica prejudicada', '5');
INSERT INTO `prescricao_enf_cuidado` VALUES ('101', 'Atentar descarte do material perfuro cortante em caixa adequada', '5');
INSERT INTO `prescricao_enf_cuidado` VALUES ('102', 'Atentar para risco para broncoaspiração', '5');
INSERT INTO `prescricao_enf_cuidado` VALUES ('103', 'Atentar se apresenta reações adversas durante ou após a medicação e comunicar a central de atendimento', '5');
INSERT INTO `prescricao_enf_cuidado` VALUES ('104', 'Atentar para proeminências ósseas e risco de abertura de lesão ( cotovelos, escápulas, trocanteres, joelhos, calcâneos)', '5');
INSERT INTO `prescricao_enf_cuidado` VALUES ('105', 'Atentar os padrões respiratórios ( dispnéia, cianose, taquipnéia)', '5');
INSERT INTO `prescricao_enf_cuidado` VALUES ('106', 'Atentar para distensão abdominal ( observar quantidade de retorno da dieta)', '5');
INSERT INTO `prescricao_enf_cuidado` VALUES ('107', 'Cuidados com periostomia e gastrostomia', '5');
INSERT INTO `prescricao_enf_cuidado` VALUES ('108', 'Atentar para checagem da prescrição médica, nutrição e de enfermagem', '5');
INSERT INTO `prescricao_enf_cuidado` VALUES ('109', 'Atentar para sinais de hipoglicemia', '5');
INSERT INTO `prescricao_enf_cuidado` VALUES ('110', 'Atentar para sinais de hiperglicemia', '5');
INSERT INTO `prescricao_enf_cuidado` VALUES ('111', 'Atentar para rodízio do oximetro a cada 2hs quando em uso contínuo', '5');
INSERT INTO `prescricao_enf_cuidado` VALUES ('112', 'Atentar para usar luvas de procedimento para troca de fralda, banho, aspiração de V.A.S, retirada de curativo sujo, troca de gase de traqueo e gastro, administração medicação venosa e dieta via sonda gastro ou button', '5');
INSERT INTO `prescricao_enf_cuidado` VALUES ('113', 'Observar indicadores não verbais de desconforto, a apciente impossibilitados de comunicação verbal', '6');
INSERT INTO `prescricao_enf_cuidado` VALUES ('114', 'Sessão de Fisioterapia Motora e Respiratória', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('115', 'Assistência Técnico de Enfermagem 6h', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('116', 'Assistência Técnico de Enfermagem 12h', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('117', 'Assistência Técnico de Enfermagem 24h sem Respirador', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('118', 'Visita Enfermeiro Assistente', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('119', 'Visita Médico Home Care ', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('120', 'Visita Médico Assistente', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('121', 'Visita Médico Especialista', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('122', 'Sessão de Fonoaudiologia', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('123', 'Visita Psicológico', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('124', 'Sessão Terapia Ocupacioinal', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('125', 'Visita Nutricionista', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('126', 'Registro de Sinais Vitais', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('127', 'Curativos', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('128', 'Aspiração de vias aéreas inferiores', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('129', 'Glicemia Capilar', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('130', 'Controle Hídrico', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('131', 'Controle de Ingesta Oral', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('132', 'Oximetria de Pulso', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('133', 'Sessão de Fisioterapia Motora', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('134', 'Sessão de Fisioterapia Respiratória', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('135', 'Ventilação Mecânica Intermitente', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('136', 'Avaliação Nutricionista', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('137', 'Avaliação Fonoaudiologia', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('138', 'Avaliação Psicólogo', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('139', 'Visita de técnico de enfermagem (1 x dia)', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('140', 'Visita de técnico de enfermagem (2 x dia)', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('141', 'Visita de técnico de enfermagem (3 x dia)', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('142', 'Assistência Técnico de Enfermagem 24h com Respirador', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('143', 'Visita de Enfermeiro 1 x / Semana', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('144', 'Visita de Enfermeiro 2 x / Semana', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('145', 'Visita de Enfermeiro 3 x / Semana', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('146', 'Aspiração de vias aéreas superiores', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('147', 'Ventilação Mecânica Contínua', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('148', 'Cateterismo Vesical de Demora', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('149', 'Cateterismo Vesical de Alívio', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('150', 'Visita Assitente Social', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('151', 'Visita de técnico de enfermagem', '7');
INSERT INTO `prescricao_enf_cuidado` VALUES ('152', 'Visita de técnico de enfermagem', '7');

-- ----------------------------
-- Table structure for prescricao_enf_itens_prescritos
-- ----------------------------
DROP TABLE IF EXISTS `prescricao_enf_itens_prescritos`;
CREATE TABLE `prescricao_enf_itens_prescritos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prescricao_enf_id` int(11) DEFAULT NULL,
  `inicio` date DEFAULT NULL,
  `fim` date DEFAULT NULL,
  `frequencia_id` int(11) DEFAULT NULL,
  `presc_enf_cuidados_id` int(11) DEFAULT NULL,
  `aprazamento` varchar(255) DEFAULT NULL,
  `obs` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

ALTER TABLE `solicitacao_rel_aditivo_enf`
ADD COLUMN `entrega_imediata`  enum('N','S') NULL AFTER `cancelado_em`,
ADD COLUMN `data_entrega`  datetime NULL AFTER `entrega_imediata`;

-- ----------------------------
-- Table structure for prescricao_enf
-- ----------------------------
DROP TABLE IF EXISTS `prescricao_enf`;
CREATE TABLE `prescricao_enf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `inicio` date DEFAULT NULL,
  `fim` date DEFAULT NULL,
  `cancelado_em` datetime DEFAULT NULL,
  `cancelado_por` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

------------------------------------ Fim Sm-817 ---------------------------------------------------------------------

------------------------------------ SM-694 ---------------------------------------------------------------------
CREATE TABLE `files_uploaded` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `full_path` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `mimetype` varchar(25) NOT NULL DEFAULT '',
  `size_bytes` varchar(50) NOT NULL DEFAULT '',
  `uploaded_by` int(10) NOT NULL,
  `uploaded_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `uploaded_by` (`uploaded_by`),
  CONSTRAINT `files_uploaded_ibfk_1` FOREIGN KEY (`uploaded_by`) REFERENCES `usuarios` (`idUsuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `quadro_feridas_enfermagem` ADD `FILE_UPLOADED_ID` INT(11) UNSIGNED  NULL  DEFAULT NULL;
ALTER TABLE `quadro_feridas_enfermagem` ADD FOREIGN KEY (`FILE_UPLOADED_ID`) REFERENCES `files_uploaded` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `quadro_feridas_enfermagem` ADD `OBSERVACAO_IMAGEM` VARCHAR(255)  NULL  DEFAULT NULL;

CREATE TABLE `feridas_relatorio_prorrogacao_enf_imagem` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ferida_relatorio_prorrogacao_enf_id` int(10) NOT NULL,
  `file_uploaded_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ferida_relatorio_prorrogacao_enf_id` (`ferida_relatorio_prorrogacao_enf_id`),
  KEY `file_uploaded_id` (`file_uploaded_id`),
  CONSTRAINT `feridas_relatorio_prorrogacao_enf_imagem_ibfk_2` FOREIGN KEY (`file_uploaded_id`) REFERENCES `files_uploaded` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `feridas_relatorio_prorrogacao_enf_imagem_ibfk_1` FOREIGN KEY (`ferida_relatorio_prorrogacao_enf_id`) REFERENCES `feridas_relatorio_prorrogacao_enf` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
------------------------------------ FIM SM-694 ---------------------------------------------------------------------

---------------- SM-830 --------------------------
ALTER TABLE `empresas` ADD `logo` VARCHAR(150)  NULL  DEFAULT 'padrao.jpg'  AFTER `compra_automatica_central`;
UPDATE `empresas` SET `logo`='opcao.jpg' WHERE (`id`='10');
---------------- FIM SM-830 ----------------------