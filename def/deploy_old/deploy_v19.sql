-- Feature/SM-476

UPDATE `unidades_fatura` SET `tiss_codigo` = '36' WHERE `ID` = '9';

INSERT INTO `tiss_outras_despesas_tipo_interno` (`id`, `tiss_outras_despesas_tipo_id`, `codigo_interno`)
VALUES
	(6, 5, 5);

-- FIM feature/SM-476


-- Feature SM-456

-- 1-Passo----------------------------------------------------

CREATE TABLE `grupo_nead` (
`ID`  int(11) NOT NULL AUTO_INCREMENT,
`NOME`  varchar(255) NULL ,
`PESO`  int(11) NULL ,
PRIMARY KEY (`ID`)
)
;

-- 2-PASSO-----------------------------------------------------------------------------------

INSERT INTO `grupo_nead` (`ID`,`NOME`, `PESO`) VALUES (1,'GRUPO 1', '1');
INSERT INTO `grupo_nead` (`ID`,`NOME`, `PESO`) VALUES (2,'GRUPO 1', '2');
INSERT INTO `grupo_nead` (`ID`, `NOME`, `PESO`) VALUES ('3', 'GRUPO 3', '3');

-- 3-Passo------------------------------------------------------------------------------------

ALTER TABLE `grupos`
ADD COLUMN `GRUPO_NEAD_ID`  int(11) NULL AFTER `QTD`;

-- 4-PASSO-------------------------------------------------------------------------------------

INSERT INTO `grupos` (`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES ('13', 'INTERNAÇÃO NOS ÚLTIMO ANO', '3', '1');
INSERT INTO `grupos` (`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES ('14', 'TEMPO DESTA INTERNAÇÃO', '3', '1');
INSERT INTO `grupos` (`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES ('15', 'DEAMBULAÇÃO', '3', '1');
INSERT INTO `grupos` (`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES ('16', 'PLEGIAS', '3', '1');
INSERT INTO `grupos` (`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES ('17', 'ELIMINAÇÕES', '4', '1');
INSERT INTO `grupos` (`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES ('18', 'ESTADO NUTRICIONAL', '3', '1');
INSERT INTO `grupos` (`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES ('19', 'HIGIENE', '3', '1');
INSERT INTO `grupos` (`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES ('20', 'ALIMENTAÇÃO', '4', '2');
INSERT INTO `grupos` (`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES ('21', 'CURATIVOS', '4', '2');
INSERT INTO `grupos` (`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES ('22', 'NÍVEL DE CONCIÊNCIA', '4', '2');
INSERT INTO `grupos` (`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES ('23', 'SECREÇÃO PULMONAR', '3', '3');
INSERT INTO `grupos` (`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES ('24', 'DRENOS/CATET./ESTOMIAS', '3', '3');
INSERT INTO `grupos` (`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES ('25', 'MEDICAÇÕES', '5', '3');
INSERT INTO `grupos` (`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES ('26', 'QUADRO CLÍNICO', '3', '3');
INSERT INTO `grupos` (`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES ('27', 'PADRÃO RESPIRATÓRIO', '4', '3');
INSERT INTO `grupos` (`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES ('28', 'DEPENDÊNCIA DE O2', '5', '3');

-- 5-PASSO--------------------------------------------------------------------------

INSERT INTO `score` (`ID`, `DESCRICAO`) VALUES ('3', 'NEAD');

-- 6-PASSO---------------------------------------------------------------------------
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('75', '13', '3', '0-1 internação', '0');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('76', '13', '3', '2-3 internações', '1');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('77', '13', '3', 'Mais de 3 internações', '2');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('78', '14', '3', 'Menos de 10 dias', '0');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('79', '14', '3', '10-30 dias', '1');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('80', '14', '3', 'Mais de 30 dias', '2');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('81', '15', '3', 'Sem auxílio', '0');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('82', '15', '3', 'Com auxílio', '1');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('83', '15', '3', 'Não deambula', '2');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('84', '16', '3', 'Ausentes', '0');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('85', '16', '3', 'Pres. c/ adaptação', '1');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('86', '16', '3', 'Pres. s/ adaptação', '2');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('87', '17', '3', 'Sem auxílio', '0');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('88', '17', '3', 'Com auxílio ou sonda', '1');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('89', '17', '3', 'Sem controle esfíncteres', '2');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('90', '17', '3', 'Sondagem intermitente', '3');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('91', '18', '3', 'Eutrófico', '0');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('92', '18', '3', 'Emagrecido', '1');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('93', '18', '3', 'Caquético', '2');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('94', '19', '3', 'Sem auxílio', '0');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('95', '19', '3', 'Com auxílio', '1');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('96', '19', '3', 'Dependente', '2');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('97', '20', '3', 'Sem auxílio', '0');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('98', '20', '3', 'Assistida', '1');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('99', '20', '3', 'Por sonda', '2');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('100', '20', '3', 'Por cateter', '3');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('101', '21', '3', 'Ausente ou simples', '0');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('102', '21', '3', 'Médios', '1');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('103', '21', '3', 'Grandes', '2');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('104', '21', '3', 'Complexos', '3');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('105', '22', '3', 'Consciente e calmo', '0');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('106', '22', '3', 'Consciente e agitado', '1');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('107', '22', '3', 'Confuso', '2');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('108', '22', '3', 'Comatoso', '3');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('109', '23', '3', 'Ausente', '0');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('110', '23', '3', 'Peq/mod.quantidade', '1');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('111', '23', '3', 'Abundante', '2');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('112', '24', '3', 'Ausente', '0');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('113', '24', '3', 'Presente c/ família apta', '1');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('114', '24', '3', 'Presente c/família', '2');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('115', '25', '3', 'VO ou SNE', '0');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('116', '25', '3', 'IM ou SC 1 ou 2xdia', '1');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('117', '25', '3', 'IM ou SC mais 2xdia', '2');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('118', '25', '3', 'EV 1 ou 2 x dia', '3');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('119', '25', '3', 'EV mais de 2 x dia', '4');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('120', '26', '3', 'Estável', '0');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('121', '26', '3', 'Instabilidade parcial', '1');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('122', '26', '3', 'Instável', '2');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('123', '27', '3', 'Eupnéico', '0');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('124', '27', '3', 'Períodos de dispnéia', '1');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('125', '27', '3', 'Dispnéia constante', '2');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('126', '27', '3', 'Períodos de apnéia', '3');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('127', '28', '3', 'Ausente', '0');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('128', '28', '3', 'Parcial (resp. esp.)', '1');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('129', '28', '3', 'Contínua (resp. esp.)', '2');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('130', '28', '3', 'Vent. mecânica interm.', '3');
INSERT INTO `score_item` (`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES ('131', '28', '3', 'Vent. mecânica contínua', '4');

------FIm feuture SM-456

-- Feature SM-477
ALTER TABLE `transferencia_bancaria`
ADD COLUMN `CANCELADA_POR`  int(11) NULL AFTER `DATA_CONCILIADO_DESTINO`,
ADD COLUMN `CANCELADA_EM`  datetime NULL AFTER `CANCELADA_POR`,
ADD COLUMN `JUSTIFICATIVA`  text NULL AFTER `CANCELADA_EM`;

-- fim Feature SM-477

