-- SM-843

ALTER TABLE `equipamentosativos`
ADD COLUMN `finalizado_em`  datetime NULL AFTER `lote`,
ADD COLUMN `finalizado_por`  int(11) NULL AFTER `finalizado_em`;

-- SM-425
ALTER TABLE `parcelas`
ADD COLUMN `encargos`  double(10,2) NOT NULL AFTER `desconto`;