-- SM-853
ALTER TABLE `faturamento`
ADD COLUMN `CODIGO_PROPRIO`  varchar(11) NOT NULL DEFAULT '0' COMMENT 'Campo utilizado caso a operadora possua tabela propria para o item' AFTER `NUMERO_TISS`;