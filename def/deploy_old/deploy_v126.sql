-- SM-770

CREATE TABLE `relatorio_deflagrado_med` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `inicio` datetime NOT NULL,
  `fim` datetime NOT NULL,
  `historia_clinica` text,
  `servicos_prestados` text,
  `editado_em` datetime DEFAULT NULL,
  `editado_por` int(11) DEFAULT NULL,
  `cancelado_em` datetime DEFAULT NULL,
  `cancelado_por` int(11) DEFAULT NULL,
  `relatorio_deflagrado_origem`  int(11) NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Relatorio deflagrado de medico.';

CREATE TABLE `relatorio_deflagrado_med_problema_ativo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relatorio_deflagrado_med_id` int(11) NOT NULL,
  `cid10_id` varchar(11) NOT NULL,
  `descricao` varchar(200) NOT NULL,
  `observacao` text NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

-- fim SM-770
---------