//-------FEATURE SM-513----------
ALTER TABLE `score_paciente`
ADD COLUMN `ID_RELATORIO`  int(11) NOT NULL AFTER `ID_AVALIACAO_ENFERMAGEM`;

ALTER TABLE `grupos`
ADD COLUMN `GRUPO_NEAD_ID`  int(11) NOT NULL AFTER `QTD`;

INSERT INTO `score` (`ID`, `DESCRICAO`) VALUES ('4', 'NEAD7');

INSERT INTO `grupos` 
(`ID`, `DESCRICAO`, `QTD`, `GRUPO_NEAD_ID`) VALUES 
('29', 'QUADRO CLÍNICO', '2', '4'), 
('30', 'ASPIRAÇÕES TRAQUEAIS', '4', '4'), 
('31', 'SONDAS/DRENOS/CATÉTERES/ESTOMIAS', '3', '4'), 
('32', 'PROCEDIMENTOS TÉCNICOS INVASIVOS', '6', '4'), 
('33', 'PADRÃO RESPIRATÓRIO', '4', '4'),
('34', 'DEPENDENCIA DE O²', '6', '4'), 
('35', 'CURATIVOS', '4', '4');

INSERT INTO `score_item` 
(`ID`, `GRUPO_ID`, `SCORE_ID`, `DESCRICAO`, `PESO`) VALUES 
('132', '29', '4', 'Estável', '0'), 
('133', '29', '4', 'Não Estável', '2'),
('134', '30', '4', 'Ausentes', '0'), 
('135', '30', '4', 'Até três aspirações', '1'), 
('136', '30', '4', 'Três a seis aspirações', '2'),
('137', '30', '4', 'Mais de seis aspirações', '4'),
('138', '31', '4', 'Ausentes', '0'),
('139', '31', '4', 'Presença com família apta', '1'),
('140', '31', '4', 'Presença sem família apta', '2'),
('141', '32', '4', 'Ausentes', '0'),
('142', '32', '4', '1x dia', '1'),
('143', '32', '4', '2x dia', '2'),
('144', '32', '4', '3x dia', '3'),
('145', '32', '4', '4x dia', '4'),
('146', '32', '4', 'Mais de 4x ao dia', '5'),
('147', '33', '4', 'Eupnéico', '0'),
('148', '33', '4', 'Períodos de Dispnéia', '1'),
('149', '33', '4', 'Dispnéia Constante', '2'),
('150', '33', '4', 'Períodos de Apnéia', '3'),
('151', '34', '4', 'Ausentes', '0'),
('152', '34', '4', 'Parcial', '1'),
('153', '34', '4', 'Contínua', '2'),
('154', '34', '4', 'Ventilação não invasiva', '3'),
('155', '34', '4', 'Ventilação invasiva intermitente', '4'),
('156', '34', '4', 'Ventilação invasiva contínua', '5'),
('157', '35', '4', 'Ausentes ou simples', '0'),
('158', '35', '4', 'Pequenos', '1'),
('159', '35', '4', 'Médios', '2'),
('160', '35', '4', 'Grandes/múltiplos', '3');
//-------FEATURE SM-513----------

//-------FEATURE SM-545----------
-- ----------------------------
-- Table structure for justificativa_solicitacao_avulsa
-- ----------------------------
CREATE TABLE `justificativa_solicitacao_avulsa` (
  `id` int(11) NOT NULL,
  `justificativa` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of justificativa_solicitacao_avulsa
-- ----------------------------
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('1', 'NÃO LIBERADO PELA AUDITORIA');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('2', 'COMPLEMENTO PEDIDO PER�?ODICO');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('3', 'COMPLEMENTO PEDIDO ADITIVO');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('4', 'COMPLEMENTO PEDIDO ADMISSÃO');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('5', 'ATRASO NA AUTORIZAÇÃO DO PLANO');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('6', 'ALTERAÇÃO DO QUADRO CL�?NICO OU FERIDAS');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('7', 'PROCEDIMENTO PONTUAL');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('8', 'ATRASO NA PRESCRIÇÃO MÉDICA PERIÓDICA');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('9', 'ATRASO NA PRESCRIÇÃO MÉDICA ADITIVA');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('10', 'ATRASO NA PRESCRIÇÃO NUTRICIONAL PERIÓDICA');
INSERT INTO `justificativa_solicitacao_avulsa` VALUES ('11', 'OUTROS');

ALTER TABLE `solicitacoes`
ADD COLUMN `JUSTIFICATIVA_AVULSO_ID`  int NULL AFTER `JUSTIFICATIVA_AVULSO`
//-------FEATURE SM-545----------