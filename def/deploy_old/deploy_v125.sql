-- SM-779

-- Verificar a Unidade regional vai fazer compra automatica na Mederi Central
ALTER TABLE `empresas`
ADD COLUMN `compra_automatica_central`  enum('N','S') NULL DEFAULT 'N' AFTER `centro_custo_dominio`;
-- Identificar que Mederi Fsa faz compra automatica.
UPDATE `empresas` SET `compra_automatica_central`='S' WHERE (`id`='11');
-- renomear as colunas do estoque colocando o id da tabela empresas
ALTER TABLE `estoque`
CHANGE COLUMN `UR_FEIRA` `empresas_id_1`  int(11) NULL DEFAULT NULL COMMENT 'Id da Unidade Regional' ,
CHANGE COLUMN `UR_SALVADOR_RMS` `empresas_id_2`  int(11) NULL DEFAULT NULL ,
CHANGE COLUMN `UR_SUL_BAHIA` `empresas_id_4`  int(11) NULL DEFAULT NULL ,
CHANGE COLUMN `UR_SUDOESTE` `empresas_id_7`  int(11) NULL DEFAULT NULL ,
CHANGE COLUMN `UR_CEARA` `empresas_id_8`  int(11) NULL DEFAULT NULL ,
CHANGE COLUMN `UR_BRASILIA` `empresas_id_9`  int(11) NULL DEFAULT NULL ,
CHANGE COLUMN `UR_ALAGOINHAS` `empresas_id_5`  int(11) NULL DEFAULT NULL ,
CHANGE COLUMN `UR_CENTRAL` `empresas_id_11`  int(11) NULL DEFAULT NULL ,
CHANGE COLUMN `QTD_MIN_FEIRA` `QTD_MIN_1`  int(11) NOT NULL ,
CHANGE COLUMN `QTD_MIN_SALVADOR_RMS` `QTD_MIN_2`  int(11) NOT NULL ,
CHANGE COLUMN `QTD_MIN_SUL_BAHIA` `QTD_MIN_4`  int(11) NOT NULL ,
CHANGE COLUMN `QTD_MIN_SUDOESTE` `QTD_MIN_7`  int(11) NOT NULL ,
CHANGE COLUMN `QTD_MIN_BRASILIA` `QTD_MIN_9`  int(11) NOT NULL,
CHANGE COLUMN `QTD_MIN_ALAGOINHAS` `QTD_MIN_5`  int(11) NOT NULL ,
ADD COLUMN `QTD_MIN_8`  int(11) NULL AFTER `QTD_MIN_5`;

ALTER TABLE `itenspedidosinterno`
ENGINE=InnoDB;

-- fim SM-779
--

-- SM-777

-- Verificar qual o id da ur Feira de Santana no banco de produção.
set @empresa =11;
UPDATE
clientes
set
clientes.empresa = @empresa
WHERE
clientes.empresa =1
-- fim SM-777