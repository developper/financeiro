ALTER TABLE `usuarios`
ADD COLUMN `block_at`  date NULL DEFAULT '9999-12-31' AFTER `ASSINATURA`;

CREATE TABLE `usuario_historico_status` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`usuario_id_modificado`  int(11) NULL ,
`usuario_id_modificador`  int(11) NULL ,
`data_modificado`  date NULL ,
`data`  datetime NULL ,
`observacao`  text NULL ,
`acao`  enum('DESATIVAR','ATIVAR') NOT NULL ,
PRIMARY KEY (`id`)
);
