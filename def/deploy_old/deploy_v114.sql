--------------------------------
--------FEATURE SM-592----------
--------------------------------
UPDATE `quadro_feridas_enf_status` SET `status_ferida`='CICATRIZADA' WHERE (`id`='2');
UPDATE `quadro_feridas_enf_status` SET `status_ferida`='EVOLUÇÃO SATISFATÓRIA' WHERE (`id`='3');
INSERT INTO `quadro_feridas_enf_status` (`status_ferida`) VALUES ('EVOLUÇÃO INSATISFATÓRIA ALTERADO CONDUTA');
--------------------------------
--------FEATURE SM-592----------
--------------------------------

--------------------------------
--------FEATURE SM-431----------
--------------------------------
CREATE TABLE `impostos_tarifas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(255) NOT NULL,
  `tipo` enum('IMPOSTO','TARIFA') NOT NULL,
  `debito_credito` enum('CREDITO','DEBITO') DEFAULT NULL,
  `ipcf` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `impostos_tarifas` VALUES ('1', 'IOF', 'IMPOSTO', 'DEBITO', '2.2.7.11');
INSERT INTO `impostos_tarifas` VALUES ('2', 'TED/DOC', 'TARIFA', 'DEBITO', '2.5.1.01');
INSERT INTO `impostos_tarifas` VALUES ('3', 'CUSTODIA', 'TARIFA', 'DEBITO', '2.5.1.01');
INSERT INTO `impostos_tarifas` VALUES ('4', 'CESTA BASICA', 'TARIFA', 'DEBITO', '2.5.1.01');
INSERT INTO `impostos_tarifas` VALUES ('5', 'COBRANÇA', 'TARIFA', 'DEBITO', '2.5.1.01');
INSERT INTO `impostos_tarifas` VALUES ('6', 'DIVERSAS', 'TARIFA', null, '2.5.1.01');

CREATE TABLE `impostos_tarifas_lancto` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`conta`  int(11) NOT NULL ,
`item`  int(11) NOT NULL ,
`documento`  varchar(50) NOT NULL ,
`data`  date NOT NULL ,
`valor`  double NOT NULL ,
PRIMARY KEY (`id`);

ALTER TABLE `impostos_tarifas_lancto`
MODIFY COLUMN `documento`  varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `item`
--------------------------------
--------FEATURE SM-431----------
--------------------------------

--
-- SM-600
--
    ALTER TABLE `catalogo`
    ADD COLUMN `simpro_id`  int(11) NULL AFTER `TUSS`,
    ADD COLUMN `brasindice_id`  int(11) NULL AFTER `simpro_id`;
--
-- fim SM-600
--