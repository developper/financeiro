--------------------------
------FEATURE SM-568------
--------------------------
CREATE TABLE `quadro_feridas_enf_status` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`status_ferida`  varchar(100) NOT NULL ,
PRIMARY KEY (`id`)
);

INSERT INTO `quadro_feridas_enf_status` (`id`, `status_ferida`) VALUES ('1', 'NOVA'),('2', 'PERMANESCENTE'),('3', 'CICATRIZADA');

ALTER TABLE `quadro_feridas_enfermagem`
ADD COLUMN `STATUS_FERIDA`  int(2) NOT NULL AFTER `PERIODO`

--------------------------
------FIM SM-568------
--------------------------

--------------------------
------FEATURE SM-579------
--------------------------

ALTER TABLE `solicitacoes`
ADD COLUMN `visualizado`  int(11) NOT NULL DEFAULT 0 AFTER `entrega_imediata`;

--------------------------
------FIM SM-579------
--------------------------

--------------------------
------Release 1.13  SM-587------
--------------------------

INSERT INTO `quadro_feridas_enf_local` (`LOCAL`, `LADO`) VALUES ('COTO', 'N')
--------------------------
------FIm Release 1.13 SM-587------
--------------------------