-- SM-803
--
-- subtask SM-805
ALTER TABLE `equipamentosativos`
ADD COLUMN `saida_id`  int(11) NULL AFTER `REFATURADO`,
ADD COLUMN `lote`  varchar(255) NULL AFTER `saida_id`;

ALTER TABLE `equipamentosativos`
ENGINE=InnoDB;

--Fim SM-805
--------------------