
-- SM-500
ALTER TABLE `empresas`
ADD COLUMN `centro_custo_dominio`  int(11) NULL DEFAULT 0 AFTER `SIGLA_EMPRESA`;

UPDATE `empresas` SET `centro_custo_dominio`='1' WHERE (`id`='1');
UPDATE `empresas` SET `centro_custo_dominio`='7' WHERE (`id`='2');
UPDATE `empresas` SET `centro_custo_dominio`='2' WHERE (`id`='4');
UPDATE `empresas` SET `centro_custo_dominio`='6' WHERE (`id`='5');
UPDATE `empresas` SET `centro_custo_dominio`='4' WHERE (`id`='7');
UPDATE `empresas` SET `centro_custo_dominio`='9' WHERE (`id`='8');
UPDATE `empresas` SET `centro_custo_dominio`='5' WHERE (`id`='9');


-- SM-688
-- ----------------------------
-- Table structure for quadro_clinico
-- ----------------------------
DROP TABLE IF EXISTS `quadro_clinico`;
CREATE TABLE `quadro_clinico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of quadro_clinico
-- ----------------------------
INSERT INTO `quadro_clinico` VALUES ('1', 'INSUFICIÊNCIA RESPIRATÓRIA AGUDA');
INSERT INTO `quadro_clinico` VALUES ('2', 'INSUFICIÊNCIA RENAL');
INSERT INTO `quadro_clinico` VALUES ('3', 'INSUFICIÊNCIA CARDÍACA CONGESTIVA');
INSERT INTO `quadro_clinico` VALUES ('4', 'HEMORRAGIA DIGESTIVA');
INSERT INTO `quadro_clinico` VALUES ('5', 'SEPSE');
INSERT INTO `quadro_clinico` VALUES ('6', 'CHOQUE SÉPTICO');
INSERT INTO `quadro_clinico` VALUES ('7', 'IAM');
INSERT INTO `quadro_clinico` VALUES ('8', 'REBAIXAMENTO DE NÍVEL DE CONSCIÊNCIA À ESCLARECER');
INSERT INTO `quadro_clinico` VALUES ('10', 'NECESSIDADE DE ACESSO VENOSO CENTRAL');


-- ----------------------------
-- Records of procedimentos
-- ----------------------------

INSERT INTO `procedimentos` VALUES ('8', 'MONITORIZAÇÃO MULTIPARAMÉTRICA');
INSERT INTO `procedimentos` VALUES ('9', 'ACESSO VENOSO PERIFÉRICO');
INSERT INTO `procedimentos` VALUES ('10', 'INTUBAÇÃO OROTRAQUEAL');
INSERT INTO `procedimentos` VALUES ('11', 'ADMINISTRAÇÃO DE OXIGÊNIO VIA CATETER NASAL OU MÁSCARA');
INSERT INTO `procedimentos` VALUES ('12', 'DROGAS DIRECIONADAS PARA A SUSPEITA DIAGNÓSTICA');
INSERT INTO `procedimentos` VALUES ('13', 'DROGA VASOATIVA');
INSERT INTO `procedimentos` VALUES ('14', 'REANIMAÇÃO CARDIOPULMONAR COM SUCESSO');

-- ----------------------------
-- Table structure for exame_medico_tipo
-- ----------------------------
DROP TABLE IF EXISTS `exame_medico_tipo`;
CREATE TABLE `exame_medico_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Records of exame_medico_tipo
-- ----------------------------
INSERT INTO `exame_medico_tipo` VALUES ('1', 'RAIO X');
INSERT INTO `exame_medico_tipo` VALUES ('2', 'TOMOGRAFIA COMPUTADORIZADA');
INSERT INTO `exame_medico_tipo` VALUES ('3', 'LABORATÓRIAL');

-- ----------------------------
-- Table structure for exame_medico
-- ----------------------------
DROP TABLE IF EXISTS `exame_medico`;
CREATE TABLE `exame_medico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) DEFAULT NULL,
  `exame_medico_tipo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exame_medico_tipo_id_FK` (`exame_medico_tipo_id`),
  CONSTRAINT `exame_medico_tipo_id_FK` FOREIGN KEY (`exame_medico_tipo_id`) REFERENCES `exame_medico_tipo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of exame_medico
-- ----------------------------
INSERT INTO `exame_medico` VALUES ('1', 'RAIO X DO TÓRAX', '1');
INSERT INTO `exame_medico` VALUES ('2', 'RAIO X DO  ABDOMEN', '1');
INSERT INTO `exame_medico` VALUES ('3', 'TOMOGRAFIA COMPUTADORIZADA DO ABDOMEN', '2');
INSERT INTO `exame_medico` VALUES ('4', 'TOMOGRAFIA COMPUTADORIZADA DO CRÂNIO', '2');
INSERT INTO `exame_medico` VALUES ('5', 'TOMOGRAFIA COMPUTADORIZADA DO TÓRAX', '2');
INSERT INTO `exame_medico` VALUES ('6', 'LABORATÓRIAL', '3');

-- ----------------------------
-- Table structure for encaminhamento_urgencia_emergencia_med
-- ----------------------------
DROP TABLE IF EXISTS `encaminhamento_urgencia_emergencia_med`;
CREATE TABLE `encaminhamento_urgencia_emergencia_med` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `data_encaminhamento` date DEFAULT NULL,
  `tipo_ambulancia` int(11) DEFAULT NULL,
  `vaga_uti` enum('N','S') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for enc_urgencia_emergencia_med_quadro_clinico
-- ----------------------------
DROP TABLE IF EXISTS `enc_urgencia_emergencia_med_quadro_clinico`;
CREATE TABLE `enc_urgencia_emergencia_med_quadro_clinico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quadro_clinico_id` int(11) DEFAULT NULL,
  `descricao` varchar(200) DEFAULT NULL,
  `enc_urgencia_emergencia_med_id` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for enc_urgencia_emergencia_med_procedimento
-- ----------------------------
DROP TABLE IF EXISTS `enc_urgencia_emergencia_med_procedimento`;
CREATE TABLE `enc_urgencia_emergencia_med_procedimento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `procedimentos_id` int(11) NOT NULL,
  `descricao` varchar(200) DEFAULT NULL,
  `enc_urgencia_emergencia_med_id` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `enc_urgencia_emergencia_med_problema`;
CREATE TABLE `enc_urgencia_emergencia_med_problema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enc_urgencia_emergencia_med_id` int(11) DEFAULT NULL,
  `cid_id` int(11) DEFAULT NULL,
  `descricao` varchar(200) DEFAULT NULL,
  `observacao` varchar(200) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
-- ----------------------------
-- Table structure for enc_urgencia_emergencia_med_exame
-- ----------------------------
DROP TABLE IF EXISTS `enc_urgencia_emergencia_med_exame`;
CREATE TABLE `enc_urgencia_emergencia_med_exame` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exame_medico_id` int(11) DEFAULT NULL,
  `descricao` varchar(200) DEFAULT NULL,
  `enc_urgencia_emergencia_med_id` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

-- FIM SM-688

UPDATE `empresas` SET `centro_custo_dominio`='1' WHERE (`id`='1');
UPDATE `empresas` SET `centro_custo_dominio`='7' WHERE (`id`='2');
UPDATE `empresas` SET `centro_custo_dominio`='2' WHERE (`id`='4');
UPDATE `empresas` SET `centro_custo_dominio`='6' WHERE (`id`='5');
UPDATE `empresas` SET `centro_custo_dominio`='4' WHERE (`id`='7');
UPDATE `empresas` SET `centro_custo_dominio`='9' WHERE (`id`='8');
UPDATE `empresas` SET `centro_custo_dominio`='5' WHERE (`id`='9');

CREATE TABLE `modalidade_home_care` (
`id`  int(11) NOT NULL AUTO_INCREMENT,
`modalidade`  varchar(70) NULL ,
PRIMARY KEY (`id`)
);

INSERT INTO `modalidade_home_care` (`modalidade`) VALUES ('Assistência Domiciliar');
INSERT INTO `modalidade_home_care` (`modalidade`) VALUES ('Internação Domiciliar 6h');
INSERT INTO `modalidade_home_care` (`modalidade`) VALUES ('Internação Domiciliar 12h');
INSERT INTO `modalidade_home_care` (`modalidade`) VALUES ('Internação Domiciliar 24h Com Respirador');
INSERT INTO `modalidade_home_care` (`modalidade`) VALUES ('Internação Domiciliar 24h Sem Respirador');
INSERT INTO `modalidade_home_care` (`modalidade`) VALUES ('Internação Domiciliar Treinamento de Cuidador');
INSERT INTO `modalidade_home_care` (`modalidade`) VALUES ('Internação Domiciliar Treinamento de Sonda Vesical de Alívio');
INSERT INTO `modalidade_home_care` (`modalidade`) VALUES ('Internação Domiciliar Treinamento de Sonda de Gastro');

CREATE TABLE `historico_modalidade_paciente` (
`id`  int(11) NOT NULL ,
`paciente`  int(11) NOT NULL ,
`modalidade`  int(11) NOT NULL ,
`data_mudanca`  int(11) NOT NULL ,
PRIMARY KEY (`id`)
);

ALTER TABLE `historico_modalidade_paciente`
MODIFY COLUMN `id`  int(11) NOT NULL AUTO_INCREMENT FIRST,
MODIFY COLUMN `data_mudanca`  date NOT NULL AFTER `modalidade`,
ADD COLUMN `data_sistema`  datetime NOT NULL DEFAULT NOW() AFTER `data_mudanca`,
ADD COLUMN `usuario`  int(11) NOT NULL AFTER `data_sistema`;