<?php

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../db/config.php';

require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

//ini_set('display_errors', 1);

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
	"GET" => [
		'/prestador/'  => '\App\Controllers\Prestador\Prestador::menu',
        '/prestador/?action=dados-paciente'  => '\App\Controllers\Prestador\Prestador::dadosPaciente',
		'/prestador/?action=prestador-form'  => '\App\Controllers\Prestador\Prestador::form',
		'/prestador/?action=prestador-form-editar'  => '\App\Controllers\Prestador\Prestador::formEditar',
		'/prestador/?action=prestador-visualizar'  => '\App\Controllers\Prestador\Prestador::visualizar',
		'/prestador/?action=listar-form'  => '\App\Controllers\Prestador\Prestador::formListar',
        '/prestador/?action=verificar-relatorio-existente'  => '\App\Controllers\Prestador\Prestador::verificarRelatorioExistente',
	],
	"POST" => [
		'/prestador/?action=salvar-relatorio-prestador'  => '\App\Controllers\Prestador\Prestador::salvarRelatorioPrestador',
		'/prestador/?action=atualizar-relatorio-prestador'  => '\App\Controllers\Prestador\Prestador::atualizarRelatorioPrestador',
		'/prestador/?action=listar-relatorios-prestador'  => '\App\Controllers\Prestador\Prestador::buscarRelatorioPrestador',
	]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}
