$(function($) {
    $(".chosen-select").chosen({search_contains: true});
    $('.summernote').summernote({
        height: 250,
        lang: 'pt-BR',
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline']],
            ['para', ['ul', 'ol']],
            ['table', ['table']]
        ]
    });

    $('#salvar-relatorio-prestador').click(function(){
        if(!validar_campos('div-salvar-relatorio-prestador')){
            return false;
        }
        var paciente = $('#paciente').val();
        var inicio = $("#inicio").val();
        var fim = $('#fim').val();
        var tipo_relatorio = $('#tipo-relatorio').val();
        var ok = false;
        if(paciente != '' && inicio != '' && fim != '' && tipo_relatorio != '') {
            $.ajax({
                type: "GET",
                url: '/prestador/?action=verificar-relatorio-existente',
                data: {paciente: paciente, inicio: inicio, fim: fim, tipo: tipo_relatorio},
                async: false,
                success: function (response) {
                    if (response != '0') {
                        alert('Já existe um relatório para esse paciente no período informado!');
                        ok = false;
                    } else {
                        ok = true;
                    }
                }
            });
        }
        return ok;
    });

    $("#paciente").change(function(){
        var paciente = $(this).val();

        if(paciente != '') {
            $.ajax({
                type: "GET",
                url: "/enfermagem/planserv/?action=dados-paciente",
                data: {paciente: paciente},
                success: function (response) {
                    response = JSON.parse(response);
                    var nascimento = response.nascimento.split('-');
                    var nascStamp = new Date(nascimento[0], nascimento[1], nascimento[2]);
                    var hoje = new Date();
                    var idade = Math.floor(Math.ceil(Math.abs(nascStamp.getTime() - hoje.getTime()) / (1000 * 3600 * 24)) / 365.25);
                    $("#idade").html(idade);
                    $("#genero").html(response.sexo == '0' ? 'Masculino' : 'Feminino');
                    $("#empresa-id").val(response.empresa);
                    $("#convenio-id").val(response.convenio);
                    $("#unidade-regional").html(response.ur);
                    $("#convenio-paciente").html(response.nomeplano);
                    $("#matricula").html(response.NUM_MATRICULA_CONVENIO);
                }
            });
        } else {
            alert('Escolha o paciente para buscar as informações do formulário!');
        }
    });

    $('#buscar-relatorio-prestador').click(function(){
        if(!validar_campos('div-buscar-relatorio-prestador')){
            return false;
        }
        return true;
    });
});

$(document).ready(function() {
    $('#summernote').summernote();
});
