<?php
include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');

require __DIR__ . '/../vendor/autoload.php';

use App\Models\Faturamento\PlanoSaude;
use App\Helpers\StringHelper as String;

class DadosPaciente{
	public $codigo = ""; public $nome = ""; public $nasc = ""; public $sexo = ""; public $plano = NULL;
	public $diagnostico = ""; public $cid = ""; public $origem = ""; public $leito= ""; public $telPosto= "";
	public $solicitante = ""; public $especialidade = ""; public $acomp = "";
	public $cnome = ""; public $csexo = ""; public $relcuid = "";
	public $rnome = ""; public $rsexo = ""; public $relresp = "";
	public $endereco = ""; public $bairro = ""; public $referencia = "";
	public $contato=""; public $telResponsavel= ""; public $telCuidador= "";
	public $enderecoInternacao = "";
	public $bairroInternacao = "";
	public $referenciaInternacao = "";
	public $telDomiciliar= "";
	public $telInternacao= "";
	public $especialidadeCuidador="";
	public $especialidadeResponsavel="";
	public $empresa = NULL;
	public $id_cidade_und= "";
	public $cidade_und ="";
	public $id_cidade_internado= "";
	public $cidade_internado ="";
	public $id_cidade_domiciliar= "";
	public $cidade_domiciliar ="";
	public $end_und_origem="";
	public $num_matricula_convenio="";
    public $liminar ="";
	public $cpfCliente ="";
	public $cpfResponsavel ="";
	public $nascResponsavel = "";
	public $nascCuidador ="";


}

class DadosFicha{

	public $usuario = ""; public $data = ""; public $pacienteid = ""; public $pacientenome = ""; public $motivo = "";
	public $alergiamed = ""; public $alergiaalim = ""; public $tipoalergiamed = ""; public $tipoalergiaalim = "";
	public $prelocohosp = "3"; public $prehigihosp = "2"; public $preconshosp = "3"; public $prealimhosp = "4"; public $preulcerahosp = "2";
	public $prelocodomic = "3"; public $prehigidomic = "2"; public $preconsdomic = "3"; public $prealimdomic = "4"; public $preulceradomic = "2";
	public $objlocomocao = "3"; public $objhigiene = "2"; public $objcons = "3"; public $objalimento = "4"; public $objulcera = "2";
	public $shosp = "14"; public $sdom = "14"; public $sobj = "14";
	public $modalidade="2";
	public $local_av="2";

	public $qtdinternacoes = ""; public $historico = "";
	public $parecer = "0"; public $justificativa = "";

	public $cancer = ""; public $psiquiatrico = ""; public $neuro = ""; public $glaucoma = "";
	public $hepatopatia = ""; public $obesidade = ""; public $cardiopatia = ""; public $dm = "";
	public $reumatologica = ""; public $has = ""; public $nefropatia = ""; public $pneumopatia = "";

	public $ativo;
	public $idcapmedica="";
	public $diag_principal='';
	public $diag_secundario='';
	public $atend_24='';
	public $atend_24_2='';
	public $atend_12='';
	public $atend_06='';
	public $pre_justificativa = ''; public $plano_desmame='';

}

class FichaEvolucao{
	public $usuario = ""; public $data = ""; public $pacienteid = ""; public $pacientenome = "";
	public $pa_sistolica_min = ""; public $pa_sistolica_max = ""; public $pa_diastolica_max = ""; public $pa_diastolica_min = "";
	public $fc_max = ""; public $fc_min = ""; public $fr_max = ""; public $fr_min = "";
	public $temperatura_max = ""; public $temperatura_min = "";
	public $estd_geral=""; public $mucosa=""; public $escleras= ""; public $respiratorio= "";public $pa_sistolica= ""; public $pa_diastolica = "";
	public $fc = ""; public $fr = ""; public $temperatura = ""; public $novo_exame = ""; public $impressao = ""; public $plano_diagnostico = ""; public $planoterapeutico = "";

}

class FichaAvaliacaoEnfermagem{
	public $usuario = ""; public $data = ""; public $pacienteid = ""; public $pacientenome = "";
}

class Paciente{
	private $abas = array('0'=>'Repouso','1'=>'Dieta','2'=>'Cuidados Especiais','3'=>'Soros e Eletr&oacute;litos',
			'4'=>'Antibi&oacute;ticos Injet&aacute;veis','5'=>'Injet&aacute;veis IV','6'=>'Injet&aacute;veis IM',
			'7'=>'Injet&aacute;veis SC','8'=>'Drogas inalat&oacute;rias','9'=>'Nebuliza&ccedil;&atilde;o',
			'10'=>'Drogas Orais/Enterais','11'=>'Drogas T&oacute;picas', '12'=>'F&oacute;mulas','13'=>'Oxigenoterapia', '14' => 'Dieta Enterais', '15' => 'Materiais');
	public $plan;
	private function equipe($paciente,$tipo){
		$has = false;
		$result = mysql_query("SELECT e.*, u.nome FROM equipePaciente as e, usuarios as u WHERE e.funcionario = u.idUsuarios AND u.tipo = '$tipo' AND paciente = '$paciente' AND fim IS NULL ORDER BY nome");
		$cor = false;
		while($row = mysql_fetch_array($result))
		{
			$has = true;
			foreach($row AS $key => $value) {
				$row[$key] = stripslashes($value);
			}
			if($cor) echo "<tr cod='{$row['id']}' >";
			else echo "<tr cod='{$row['id']}' class='odd' >";
			$cor = !$cor;
			echo "<td>{$row['nome']}</td>";
			$inicio = implode("/",array_reverse(explode("-",$row['inicio'])));
			echo "<td>{$inicio}</td>";
			echo "<td>-</td>";
			echo "<td><button class='end' >Finalizar</button></td><td><button class='del' >Remover</button></td>";
			echo "</tr>";
		}
		if(!$has) echo "<tr><td colspan='5' ><center><b>Nenhuma equipe está acompanhando o paciente.</b></center></td></tr>";
	}

	private function empresas($id,$extra = NULL){
		$result = mysql_query("SELECT * FROM empresas where ATIVO='S' AND id NOT IN (1, 3, 9, 10) ORDER BY nome");
		$var = "<select name='empresa' id='empresas' style='background-color:transparent;' class='COMBO_OBG' {$extra}>";
		$var .= "<option value='-1'>Selecione</option>";
		while($row = mysql_fetch_array($result))
		{
			if(($id <> NULL) && ($id == $row['id']))
				$var .= "<option value='{$row['id']}' selected>{$row['nome']}</option>";
			else
				$var .= "<option value='{$row['id']}'>{$row['nome']}</option>";
		}
		$var .= "</select>";
		return $var;
	}



	public function menu(){
		echo "<a href='?adm=paciente&act=listar'>Listar</a><br>";
		if($_SESSION['empresa_principal'] == 1){
		echo "<a href='?adm=paciente&act=novo'>Solicita&ccedil;&atilde;o de Avalia&ccedil;&atilde;o</a><br>";
		}
		echo "<a href='../enfermagem/prescricao/index.php?action=curativo'>Prescrições de Curativos de Enfermagem</a>";
	}

	public function menu_relatorio(){
		echo "<a href='/adm/secmed/?action=relatorio-modalidade-paciente'>Histórico de Mudança de Modalidade Por Paciente</a><br>";
		echo "<a href='/adm/secmed/?action=relatorio-epidemiologico'>Relatório Epidemiológico</a><br>";
		echo "<p><a href='../auditoria/relatorios/?action=form-relatorio-faturas-liberadas'>Relat&oacute;rio de Faturas Liberadas</a></p>";
		echo "<p><a href='../auditoria/relatorios/?action=form-relatorio-faturamento-enviado'>Relat&oacute;rio de Faturamento Enviado</a></p>";
        echo "<p><a href='../auditoria/relatorios/?action=relatorio-nao-conformidades-gerenciar&from=enfermagem'>Relatório de Não Conformidades</a></p>";
   
	}

	private function preencheDados($id){
		$id = anti_injection($id,"numerico");
		$result = mysql_query("
                SELECT c.*, 
                p.id as plano, 
                cid.codigo as codcid, 
                cid.descricao as descid,
                concat(cd.NOME,',',cd.UF) as cidade_und,
				(select  concat(x.NOME,',',x.UF) from cidades as x where x.id=c.CIDADE_ID_INT) as cidade_int,
				(select  concat(x1.NOME,',',x1.UF) from cidades as x1 where x1.id=c.CIDADE_ID_DOM) as cidade_dom
				FROM clientes as c
				LEFT OUTER JOIN planosdesaude as p ON c.convenio = p.id
				LEFT OUTER JOIN cid10 as cid ON c.diagnostico collate utf8_general_ci = cid.codigo collate utf8_general_ci
				LEFT JOIN cidades cd ON c.CIDADE_ID_UND = cd.ID

				WHERE idClientes = '$id'");
		$dados = new DadosPaciente();


		while($row = mysql_fetch_array($result)){
		    //die(var_dump($row));
			$dados->codigo = $row['codigo']; $dados->dataInternacao = $row['dataInternacao'];$dados->nome = String::removeAcentosViaEReg($row['nome']); $dados->sexo = $row['sexo']; $dados->plano = $row['plano'];
			$dados->diagnostico = $row['descid']; $dados->cid = $row['codcid']; $dados->origem = $row['localInternacao'];  $dados->leito = $row['LEITO'];
			$dados->solicitante = $row['medicoSolicitante']; $dados->especialidade = $row['espSolicitante']; $dados->acomp = $row['acompSolicitante'];
			$dados->cnome = $row['cuidador']; $dados->csexo = $row['csexo']; $dados->relcuid = $row['relcuid'];
			$dados->rnome = $row['responsavel']; $dados->rsexo = $row['rsexo']; $dados->relresp = $row['relresp'];
			$dados->endereco = $row['endereco']; $dados->bairro = $row['bairro']; $dados->referencia = $row['referencia'];  $dados->telPosto = $row['TEL_POSTO'];  $dados->contato = $row['PES_CONTATO_POSTO'];
			$dados->empresa = $row['empresa']; $dados->telResponsavel = $row['TEL_RESPONSAVEL']; $dados->telCuidador = $row['TEL_CUIDADOR'];
			$dados->enderecoInternacao = $row['END_INTERNADO']; $dados->bairroInternacao = $row['BAI_INTERNADO']; $dados->referenciaInternacao = $row['REF_ENDERECO_INTERNADO'];
			$dados->telInternacao = $row['TEL_LOCAL_INTERNADO']; $dados->telDomiciliar = $row['TEL_DOMICILIAR'];$dados->especialidadeCuidador = $row['CUIDADOR_ESPECIALIDADE'];
			$dados->especialidadeResponsavel = $row['RESPONSAVEL_ESPECIALIDADE']; $dados->id_cidade_und = $row['CIDADE_ID_UND'];$dados->cidade_und = $row['cidade_und'];
			$dados->id_cidade_internado = $row['CIDADE_ID_INT'];$dados->cidade_internado = $row['cidade_int'];
			$dados->id_cidade_domiciliar = $row['CIDADE_ID_DOM'];$dados->cidade_domiciliar = $row['cidade_dom']; $dados->end_und_origem =$row['END_UND_ORIGEM'];
			$dados->num_matricula_convenio =$row['NUM_MATRICULA_CONVENIO'];
                        ///jeferson 2-10-2013 novo campo
            $dados->liminar =$row['LIMINAR'];
			$dados->cpfCliente =$row['cpf'];

			$dados->cpfRresponsavel =$row['cpf_responsavel'];
			$dados->nascResponsavel = implode("/",array_reverse(explode("-",$row['nascimento_responsavel'])));
				$dados->nascCuidador = implode("/",array_reverse(explode("-",$row['nascimento_cuidador'])));
			 
			$dados->nasc = implode("/",array_reverse(explode("-",$row['nascimento'])));
		}
		return $dados;
	}

	public function form($id = NULL,$visao = ''){
	    $act = $_GET['act'] == 'novo';
		$dados = new DadosPaciente();
		$modo = "salvar-paciente";
		$titulo = "Solicita&ccedil;&atilde;o de Avalia&ccedil;&atilde;o";
        $imprimir= "";

		if($id != NULL){

			$dados = $this->preencheDados($id);
			$modo = "editar-paciente";
			$titulo = "Editar Paciente";
                        $imprimir="<form method=post target='_blank' action='imprimir_cadastro_paciente.php?id={$id}'>".
                            
                            "<button id='imprimir_det_presc' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>"
				."<span class='ui-button-text'>Imprimir</span></button></form>";

		}
		$visao_sexo = '';
		if($visao != ''){
			$visao_sexo = 'disabled';
			$titulo = "Detalhes Paciente";
		}

		$matriculaDisabled = '';
		$matriculaOBG = 'OBG';
		$cpfOBG = '';
		// caso o plano seja particular;
		if($dados->plano ==1 && !$act){
			$matriculaDisabled = 'disabled';
			$matriculaOBG = '';
			$cpfOBG='OBG';
		}
		echo "<input type='hidden' name='paciente' value='{$id}' />";
		echo "<center><h1>".utf8_encode($titulo)."</h1></center>";
                echo $imprimir;
		echo "<div id='div-novo-paciente'>";
		echo alerta();


		echo "<fieldset style='width:95%;'><legend><b><label style='color:red;'>* </label>Paciente</b></legend><table width='100%'>";
		echo "<tr><tr><td>C&oacute;digo</td><td>Data da Solicita&ccedil;&atilde;o</td><td>Hora</td></tr>";
		echo "<tr><td><input class='' type='text' name='codigo' value='{$dados->codigo}' {$visao} disabled='disabled'/></td>
		<td><input class='OBG' type='text' name='dataInternacao' value='{$this->PegarData($dados->dataInternacao)}' {$visao} /></td>
		<td><input class='OBG hora-paciente' type='text' name='HoraInternacao' value='{$this->PegarHora($dados->dataInternacao)}' {$visao} /></td>

		</tr>";
		echo "<tr><td>Nome</td><td>Data de nascimento</td><td>Sexo</td></tr>";
		echo "<tr><td><input class='OBG' type='text' size=50 name='nome' value='" . String::removeAcentosViaEReg($dados->nome) . "' {$visao} /></td>";
		echo "<td><input class='data OBG' maxlength='10' size='10' type='text' name='nascimento' value='{$dados->nasc}' {$visao_sexo} /></td>";

		echo"<td><select name='sexo'  style='background-color:transparent;' class='COMBO_OBG' {$visao_sexo}><option value='-1' ></option>";
		echo"<option value='0' ".(($dados->sexo=='0')?"selected":"")." {$visao_sexo} >MASCULINO</option>";
		echo"<option value='1' ".(($dados->sexo=='1')?"selected":"")." {$visao_sexo} >FEMININO</option>";
		echo"</select></td></tr>";

    $planos = PlanoSaude::getAll(false);

		echo "<tr><td>Convênio</td><td>N° de Matrícula no Convênio:</td><td>Liminar:</td></tr>";
		echo "<tr><td><select name='convenio' class='COMBO_OBG ' id='convenio-ficha-paciente' style='background-color:transparent;' {$visao_sexo} >";
		echo "<option value='-1' selected ></option>";
    foreach ($planos as $plano) {
      $selected = $plano['id'] == $dados->plano ? "selected='selected'" : null;
      echo "<option value='{$plano['id']}' {$selected}  >{$plano['nome']}</option>";
    }
		echo "</select></td><td><input class='$matriculaOBG'  size='15' $matriculaDisabled type='text' name='num_matricula_convenio' value='{$dados->num_matricula_convenio}' {$visao} /></td>";
                echo "<td><select class='COMBO_OBG' name='liminar' style='background-color:transparent;'> ";
               echo "<option value='-1' ></option>";
                    echo"<option value='0' ".(($dados->liminar=='0')?"selected":"")." {$visao_sexo} >N&atilde;o</option>";
		echo"<option value='1' ".(($dados->liminar =='1')?"selected":"")." {$visao_sexo} >Sim</option>";
                    echo"</select></td></tr>
              <tr>
                  <td>
                      <label for='cpf-cliente'>CPF: </label>
                      <input id='cpf-cliente' {$visao} name = 'cpf_cliente' class ='cpf $cpfOBG' value='{$dados->cpfCliente}'>
                      <span id='span-cpf-cliente'  class='text-red'></span>
                  </td>

              </tr>
              </table>
              </fieldset>";

        if(!$act) {
            echo "<fieldset style='width:95%;'><legend><b><label style='color:red;'>* </label>Diagn&oacute;stico Principal</b>.</legend><table width='100%'>";
            ///echo "<tr><td><b>Diagn&oacute;stico Principal</b></td></tr>";
            if ($visao == '') echo "<tr><td>Buscar Diagn&oacute;stico:</td><td>Diagn&oacute;stico.</td><td>C&oacute;digo.</td></tr><tr><td><input type='text' name='busca-diagnostico' id='busca-diagnostico' {$visao} /></td>";
            echo "<td><input class='OBG' type='text' size=50 name='diagnostico' readonly=readonly value='{$dados->diagnostico}' /></td>";
            echo "<td><input class='OBG' type='text' name='cid-diagnostico' readonly=readonly value='{$dados->cid}' /></td></tr>";
            echo "<tr><td>M&eacute;dico solicitante</td><td>Especialidade</td><td>Acompanhar&aacute; o paciente em domic&iacute;lio (S/N)?</td></tr>";
            echo "<tr><td><input type='text' size=50 name='solicitante' class='OBG' value='{$dados->solicitante}' {$visao} /></td>";
            echo "<td><input type='text' name='sol-especialidade' class='OBG' value='{$dados->especialidade}' {$visao} /></td>";
            echo "<td><input type='text' name='sol-acompanhamento' class='OBG boleano' value='{$dados->acomp}' {$visao} /></td></tr>

              </table></fieldset>";
        }

		echo "<fieldset style='width:95%;'><legend><b><label style='color:red;'>* </label>Unidade de Interna&ccedil;&atilde;o de Origem</b>.</legend><table width='100%'>";
		echo "<tr><td>Nome da Unidade</td><td>Observação</td><td>Telefone do posto de enfermagem</td></tr>";
		echo "<tr><td><input class='OBG' type='text' size=50 name='local' id='local' value='{$dados->origem}' {$visao} /></td><td><input class= type='text' size=35 name='leito' id='leito' value='{$dados->leito}' {$visao} /></td>
		<td><input class='' type='text' size=20 name='telPosto'  maxlength='15' id='telPosto' value='{$dados->telPosto}' {$visao} /></td></tr>";

		echo "<tr><td>Endere&ccedil;o.</td><td>Cidade</td><td>Pessoa para contato.</td></tr>";
		/// if($visao == '') echo "<td></td></tr>";
		echo "<tr><td><input class='OBG' type='text' size=50 name='end_und_origem' id='end_und_origem' value='{$dados->end_und_origem}' {$visao} /></td>
		<td><input type='text' class='OBG' name='cidade_und' size=35 id='cidade_und' value='{$dados->cidade_und}'  {$visao} /></td>
		<td><input class='OBG' type='text' size=35 name='contato' id='contato' value='{$dados->contato}' {$visao} /></td>
		 
		<td><input type='hidden' name='id_cidade_und' id='id_cidade_und' value='{$dados->id_cidade_und}' {$visao} /></td>
		</table></fieldset>";

        if(!$act) {
            echo "<fieldset style='width:95%;'><legend><b><label style='color:red;'>* </label>Respons&aacute;vel Familiar</b></legend><table width='100%'>";
            echo "<tr><td>Nome</td><td>Sexo</td><td>Rela&ccedil;&atilde;o</td><td><span class='espresp'>Especialidade</span></td><tr>";
            echo "<tr><td><input type='text' size=50 name='responsavel' class='OBG' value='{$dados->rnome}' {$visao} /></td>";
            echo "<td><select name='rsexo'  style='background-color:transparent;' class='COMBO_OBG' {$visao_sexo}><option value='-1' ></option>";
            echo "<option value='0' " . (($dados->rsexo == '0') ? "selected" : "") . " {$visao_sexo} >MASCULINO</option>";
            echo "<option value='1' " . (($dados->rsexo == '1') ? "selected" : "") . " {$visao_sexo} >FEMININO</option>";
            echo "</select></td>";

            $r = mysql_query("SELECT * FROM relacionamento_pessoa");
            echo "<td><select name='relresp'  class='COMBO_OBG' style='background-color:transparent;' {$visao_sexo} >";
            echo "<option value='-1' selected ></option>";
            while ($row = mysql_fetch_array($r)) {
                foreach ($row AS $key => $value) {
                    $row[$key] = stripslashes($value);
                }
                if ($row['ID'] == $dados->relresp) echo "<option value='{$row['ID']}' selected >{$row['RELACIONAMENTO']}</option>";
                else echo "<option value='{$row['ID']}'  >{$row['RELACIONAMENTO']}</option>";
            }
            echo "</select></td>";

            if ($dados->relresp != '12') {
                echo "<span flag='1' class='flagx'>";
            } else {
                echo "<span flag='0' class='flagx'>";
            }

            echo "<td><span class='espresp'>{$row['ID']}<input type='text' size=32 name='especialidadeResponsavel'  id='especialidadeResponsavel'  value='{$dados->especialidadeResponsavel}' {$visao} /></span></td></tr>";


            echo "<tr><td>Telefone.</td>
                 <td>Data de nascimento</td>
                 <td><label for='cpf-responsavel'>CPF:</label></td>
              </tr>";
            echo "<td><input type='text' name='telResponsavel' maxlength='15' id='telResponsavel'  class='OBG' value='{$dados->telResponsavel}' {$visao} /></td>

			  <td><input class='data OBG' maxlength='10' size='10' type='text' name='nascimentoResponsavel' value='{$dados->nascResponsavel}' {$visao_sexo} /></td>
			  <td>
                      <input id='cpf-responsavel' name ='cpf_responsavel'  class ='cpf $cpfOBG' {$visao} value='{$dados->cpfRresponsavel}'>
                      <span id='span-cpf-responsavel' class='text-red'></span>
			  </td>
			  </tr>
              </table></fieldset>";

            echo "<fieldset style='width:95%;'><legend><b>Cuidador Familiar</b>.</legend><table width='100%'><tr><td>Se Diferente do Respons&aacute;vel</td></tr>";
            echo "<tr><td>Nome</td><td>Sexo</td><td>Rela&ccedil;&atilde;o</td><td><span class='espcuid'>Especialidade</span></td><tr>";
            echo "<tr><td><input type='text' size=50 name='cuidador' class='' value='{$dados->cnome}' {$visao} /></td>";
            ///echo "<td><input type='radio' name='csexo' value='0' ".(($dados->csexo=='0')?"checked":"")." {$visao_sexo} />M";
            ///echo "<input type='radio' name='csexo' value='1' ".(($dados->csexo=='1')?"checked":"")." {$visao_sexo} />F</td>";

            echo "<td><select  style='background-color:transparent;' name='csexo' class='' {$visao_sexo}><option value='-1' ></option>";
            echo "<option value='0' " . (($dados->csexo == '0') ? "selected" : "") . " {$visao_sexo} >MASCULINO</option>";
            echo "<option value='1' " . (($dados->csexo == '1') ? "selected" : "") . " {$visao_sexo} >FEMININO</option>";
            echo "</select></td>";

            $r = mysql_query("SELECT * FROM relacionamento_pessoa");
            echo "<td><select name='relcuid'  style='background-color:transparent;' {$visao_sexo} >";
            echo "<option value='-1' selected ></option>";
            while ($row = mysql_fetch_array($r)) {
                foreach ($row AS $key => $value) {
                    $row[$key] = stripslashes($value);
                }
                if ($row['ID'] == $dados->relcuid) echo "<option value='{$row['ID']}' selected >{$row['RELACIONAMENTO']}</option>";
                else echo "<option value='{$row['ID']}'>{$row['RELACIONAMENTO']}</option>";
            }

            echo "</select></td>";
            if ($dados->relcuid != '12') {
                echo "<span flag='1' class='flagc'>";
            } else {
                echo "<span flag='0' class='flagc'>";
            }
            echo "<td>
		<span class='espcuid'>{$row['ID']}<input type='text' size=32 name='especialidadeCuidador' id='especialidadeCuidador'   value='{$dados->especialidadeCuidador}' {$visao} /></span>
		</td></tr>";
            /*  echo "<td><input type='text' name='relcuid' class='' value='{$dados->crel}' {$visao} /></td></tr>";
             */
            echo "<tr><td>Telefone.</td><td>Data de nascimento</td></tr>";


            echo "<td><input type='text' name='telCuidador' maxlength='15' id='telCuidador' class='' value='{$dados->telCuidador}' {$visao} /></td>
                   <td><input class='data ' maxlength='10' size='10' type='text' name='nascimentoCuidador' value='{$dados->nascCuidador}' {$visao_sexo} /></td>
                   </tr></table></fieldset>";

            ///echo "<td><input type='radio' name='rsexo' value='0' ".(($dados->rsexo=='0')?"checked":"")." {$visao_sexo} />M";
            /// echo "<input type='radio' name='rsexo' value='1' ".(($dados->rsexo=='1')?"checked":"")." {$visao_sexo} />F</td>";

            echo "<fieldset style='width:95%;'><legend><b><label style='color:red;'>* </label>Domicílio do Paciente</b></legend><table width='100%'>";
            echo "<tr><td>Endere&ccedil;o.</td><td>Bairro</td><td>Ponto de Referência</td></tr>";
            echo "<tr><td><input type='text' size=50 name='endereco' class='OBG' value='{$dados->endereco}' {$visao} /></td>";
            echo "<td><input type='text' name='bairro' class='OBG' value='{$dados->bairro}' {$visao} /></td>";
            echo "<td><input type='text' name='referencia' class='' value='{$dados->referencia}' {$visao} /></td></tr>";
            echo "<tr><td>Telefone.</td><td>Cidade</td></tr>";
            echo "<tr><td><input type='text' maxlength='15' name='telDomiciliar' id='telDomiciliar' class='' value='{$dados->telDomiciliar}' {$visao} /></td>
		<td><input type='text' class='OBG' size='35' name='cidade_domiciliar' id='cidade_domiciliar' value='{$dados->cidade_domiciliar}'  {$visao} /></td>
		<td><input type='hidden' name='id_cidade_domiciliar' id='id_cidade_domiciliar' value='{$dados->id_cidade_domiciliar}' {$visao} /></td></tr></table></fieldset>";

            echo "<fieldset style='width:95%;'><legend><b>Local de Interna&ccedil;&atilde;o</b></legend><table width='100%'>";
            echo "<tr><td>Se Diferente do domiciliar.</td></tr><tr><td>Endere&ccedil;o.<td>Bairro</td><td>Ponto de Referência</td></tr>";
            echo "<tr><td><input type='text' size=50 name='enderecoInternacao' class='' value='{$dados->enderecoInternacao}' {$visao} /></td>";
            echo "<td><input type='text' name='bairroInternacao' class='' value='{$dados->bairroInternacao}' {$visao} /></td>";
            echo "<td><input type='text' name='referenciaInternacao' class='' value='{$dados->referenciaInternacao}' {$visao} /></td></tr>";
            echo "<tr><td>Telefone.</td><td>Cidade<td></tr>";
            echo "<tr><td><input type='text' maxlength='15' name='telInternacao' id='telInternacao' class='' value='{$dados->telInternacao}' {$visao} /></td>
		<td><input type='text'  size='35' name='cidade_internado' id='cidade_internado' value='{$dados->cidade_internado}'  {$visao} /></td>
		<td><input type='hidden' name='id_cidade_internado' id='id_cidade_internado' value='{$dados->id_cidade_internado}' {$visao} /></td></tr></table>
		</fieldset>";
        }
		echo "<table>";
        echo "<tr><td><b><label style='color:red;'>* </label>Unidade Regional</b></td</tr>";
		echo "<tr><td>".$this->empresas($dados->empresa,$visao_sexo)."</td></tr></table>";
		if($visao == '') echo "<p><button id='botao-salvar-formulario' modo='{$modo}' >Inserir</button>";
		echo "</div>";
	}

	public function PegarData($data=null){
		if($data==null){
			$data = date('d/m/Y');
		}else{
			$quebrar = explode('-',$data);

			//print_r(explode('-',$data));

			if(strlen($quebrar[0])==4){
				$data = substr($data,8,2).'/'.substr($data,5,2).'/'.substr($data,0,4);
			}else{
				$data = substr($data,0,2).'/'.substr($data,3,2).'/'.substr($data,6,4);
					
			}
		}
		return $data;
	}

	public function PegarHora($data=null){
		if($data==null){
			$hora = '';
		}else{
			$hora = substr($data,11,2).':'.substr($data,14,2).':'.substr($data,17,2);
		}
		return $hora;
	}

	public function listar($empresa,$nome_paciente,$codigo_paciente,$select_status,$select_liminar,$select_convenio = -1){
            require "templates/paciente_listar.phtml";
            /*/////////////jeferson 2013-09-30 busca de pacientes
           
            $cond_nome= '';
            $cond_codigo= '';
            $cond_status= '';
            $cond_liminar= '';
            $cond_empresa = '';
            
            /////////////Quando recebe os parametros para refinar a busca via post 
          if($empresa != -1){
              $cond_empresa = "and c.empresa = {$empresa}";
              
          }
          if($nome_paciente != '' || $nome_paciente != NULL){
              $cond_nome = "and c.nome like '%{$nome_paciente}%' ";
              
          }
          
          if($codigo_paciente != ''|| $codigo_paciente != NULL){
              $cond_codigo = " and c.codigo like '%{$codigo_paciente}%'";
              
          }
          if($select_status != -1){
              $cond_status= " and c.status={$select_status} ";
              
          }
          if($select_liminar != -1){
              $cond_liminar = "and c.LIMINAR = {$select_liminar}";
              
          }
          $cond_convenio = $select_convenio != -1 ? "AND c.convenio = '{$select_convenio}'" : null;

    ///////////
            
            echo "<div>";
            echo "<h1><center>Pacientes</center></h1>";
            echo "<form action='{$_SERVER['PHP_SELF']}' method='POST'>";
            echo "<fieldset style='width:95%;'><legend><b>Busca</b></legend>";
            echo "<input type='hidden' name='query' value='listar'></input>";
            echo "<p><label for='nome_paciente'><b>Paciente</b></label> <input id='nome_paciente' name='nome_paciente' type='text' size='30' ></input></p>";
         
            ///Limita os campos de busca apenas para a empresa central 
            if ($_SESSION['empresa_principal'] == 1 ){
            echo "<p><label for='codigo_paciente'><b>C&oacute;digo Interno</b></label>&nbsp; <input id='codigo_paciente' name='codigo_paciente' type='text' ></input></p>";
            echo "<p><label for='select_status'><b>Status</b></label>&nbsp;<select id='select_status' name='select_status' style='background-color:transparent;'>";
                                    echo "<option value='-1' selected  >Selecione</option>";
                                    echo "<option value='1' >Novo</option>";
                                    echo "<option value='2' >Pendente</option>";
                                    echo "<option value='3' >Desfavor&aacute;vel</option>";
                                    echo "<option value='4' >Ativo/autorizado</option>";
                                    echo "<option value='5' > Suspenso</option>";
                                    echo "<option value='6' >&Oacute;bito</option>";
                                    echo "<option value='7' >Alta</option>";
                                    echo "<option value='8' >Inativo</option>";
            echo "</select></p>";
            
            echo "<p><label for='empresas'><b>Unidade Regional</b></label>&nbsp;".$this->empresas($id,$extra = NULL)."</p>";
           
            echo "<p><label for='select_sliminar'><b>Liminar</b></label>&nbsp;<select id='select_liminar' name='select_liminar' style='background-color:transparent;'>";
                                    echo "<option value='-1' selected  >selecione</option>";
                                    echo "<option value='0'  >N&atilde;o</option>";
                                    echo "<option value='1'  >Sim</option>";

            echo "</select></p>";
            $planos = PlanoSaude::getAll(false);
            $planosOptions = '';
            foreach ($planos as $plano)
              $planosOptions .= sprintf('<option value="%s">%s</option>', $plano['id'], $plano['nome']);

            echo "<p>
                    <label for='convenio'><b>Convênio</b></label>
                    <select name='select_convenio' id='select_convenio' style='background-color:transparent;'>
                    <option value='-1'>SELECIONE</option>
                    {$planosOptions}
                    </select>
                  </p>";
             }
             //////
            echo "</fieldset>";
            echo "<button   type='submit' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Pesquisar</span></button></span>";

             echo "</form>";

            echo "</div>";
               /////////////jeferson 2013-09-30 busca de pacientes fim
            
	      if ($_SESSION['empresa_principal'] == 1 ){
                   
		  $sql="SELECT
                            *,
                            s.STATUS AS nstatus
                        FROM
                            clientes AS c 
                            INNER JOIN statuspaciente AS s ON (c.STATUS = s.id)
                            LEFT JOIN (
                                        SELECT 
                                            u.nome as usuario, 
                                            DATE_FORMAT(DATA, '%d/%m/%Y à\s %H:%i:%s') AS data, 
                                            PACIENTE_ID, 
                                            OBSERVACAO AS ultima_observacao 
                                        FROM 
                                            historico_status_paciente INNER JOIN usuarios AS u ON (historico_status_paciente.USUARIO_ID = u.idUsuarios) 
                                        ORDER BY 
                                            ID DESC
                                      ) AS hsp ON (c.idClientes = hsp.PACIENTE_ID)
                        WHERE
                                1 {$cond_empresa} {$cond_codigo} {$cond_liminar} {$cond_nome} {$cond_status} {$cond_convenio}
                        GROUP BY 
                            c.idClientes
                        ORDER BY
                            nome ASC";

		  }else{
                      
		  $sql="SELECT
                            *,
                            s.STATUS AS nstatus
                        FROM
                            clientes AS c
                            INNER JOIN statuspaciente AS s ON (c.STATUS = s.id)
                            LEFT JOIN (
                                        SELECT
                                            u.nome as usuario,
                                            DATE_FORMAT(DATA, '%d/%m/%Y à\s %H:%i:%s') AS data,
                                            PACIENTE_ID,
                                            OBSERVACAO AS ultima_observacao
                                        FROM
                                            historico_status_paciente INNER JOIN usuarios AS u ON (historico_status_paciente.USUARIO_ID = u.idUsuarios)
                                        ORDER BY 
                                            ID DESC
                                      ) AS hsp ON (c.idClientes = hsp.PACIENTE_ID)
                        WHERE
                            (
                                c. STATUS = 4
                                OR c. STATUS = 1
                            )
						AND c.empresa IN {$_SESSION['empresa_user']}
                         {$cond_nome}
                        GROUP BY 
                            c.idClientes
                        ORDER BY
                            nome ASC";
		  }
			//die($sql);

                //
                $result = mysql_query($sql);
                 
		echo "<div id='dialog-status' title='Status do paciente' cod='' >
                    </div>";
		echo "<div id='dialog-modalidade' title='Modalidade do paciente' cod='' >
                    </div>";
		///////////jeferson 04-10-2013 dialog com as opções medicas.
		echo "<div id='dialog-opcao-enfermagem' title='Opções Enfermagem' cod='' ></div>";
		///////////jeferson 04-10-2013 dialog com as opções medicas.
		echo "<div id='dialog-opcao-medica' title='Opções Médica' cod='' ></div>";
		///////////jeferson 07-11-2013 dialog com as opções secmed.
		echo "<div id='dialog-opcao-secmed' title='Opções Secmed' cod='' ></div>";
		echo "<a href='imprimir.php?nome_paciente={$nome_paciente}&codigo_paciente={$condigo_paciente}&select_status={$select_status}&select_liminar={$select_liminar}&empresa={$empresa}&select_convenio={$select_convenio}' target='_black'>Imprimir lista</a><br><br>";
                $totalPacientes = mysql_num_rows($result);
                echo "Total: {$totalPacientes} paciente(s)";
		echo "<table class='mytable' id='pacientes' width=95% >";
		echo "<thead><tr>";
		echo "<th><b>C&oacute;digo</b></th>";
		echo "<th width=28%><b>Paciente</b></th>";
		echo "<th width=1%><b>Status</b></th>";
		echo "<th  ><b>Op. Medica</b></th>";
		echo "<th ><b>Op. Enfermagem</b></th>";
		echo "<th colspan='5' align='center' ><b>Op&ccedil;&otilde;es</b></th>";
        echo "<th  ><b>% Cadastro</b></th>";
		echo "</tr></thead>";
		$cor = false;
		while($row = mysql_fetch_array($result))
		{
			foreach($row AS $key => $value) {
				$row[$key] = stripslashes($value);
			}
			if($cor) echo "<tr>";
			else echo "<tr class='odd' >";
			$cor = !$cor;
			echo "<td>{$row['codigo']}</td>";
                        echo "<td><label title='Em {$row['data']} por {$row['usuario']}: {$row['ultima_observacao']}'>" . String::removeAcentosViaEReg($row['nome']) . ($row['ultima_observacao'] !== '' ? "<img src='../utils/details.png' width='16' height='16' title='{$row['ultima_observacao']}' />" : "")."</label></td>";
			echo "<td>{$row['nstatus']}</td>";
			echo "<td><img src='../utils/capmed_16x16.png' title='Op&ccedil;&otilde;es m&eacute;dica' border='0' class='botao_relatorios_medicos' cod='{$row['idClientes']}'></td>";
			//echo "<td><a href='?adm=paciente&act=listfichaevolucao&id={$row['idClientes']}'><img src='../utils/fichas_24x24.png' width='16' title='Ficha de evolu&ccedil;&atilde;o m&eacute;dica' border='0'></a></td>";
                         echo "<td><img src='../utils/capmed_16x16.png' title='Op&ccedil;&otilde;es  Enfermagem' class='botao_relatorios_enfermagem' border='0'cod='{$row['idClientes']}'></td>";
			
			 //echo "<td><a href='/enfermagem/?view=listavaliacaenfermagem&id={$row['idClientes']}'><img src='../utils/capmed_16x16.png' title='Ficha de avalia&ccedil;&atilde;o enfermagem' border='0'></a></td>";
			// echo "<td><a href='/enfermagem/?view=listevolucaoenfermagem&id={$row['idClientes']}'><img src='../utils/fichas_24x24.png' width='16' title='Ficha de evolu&ccedil;&atilde;o enfermagem' border='0'></a></td>";
			if($_SESSION['empresa_principal'] == 1 || $_SESSION['empresa_principal'] == 9 || $_SESSION['empresa_principal'] == 10){
				echo "<td><img src='../utils/capmed_16x16.png' title='Op&ccedil;&otilde;es  SECMED' class='botao_relatorios_secmed' border='0'cod='{$row['idClientes']}'></td>";
				echo "<td><img src='../utils/status_16x16.png' class='statusp' cod='{$row['idClientes']}' ur='{$row['empresa']}' title='Mudar status' border='0'></td>";
				echo "<td><img src='../utils/home_care_36x36.png' height='24' class='modhc' cod='{$row['idClientes']}' title='Mudar modalidade do paciente' border='0'></td>";
			echo "<td><a href=?adm=paciente&act=show&id={$row['idClientes']}><img src='../utils/details_16x16.png' title='Detalhes' border='0'></a></td>";
			echo "<td><a href=?adm=paciente&act=editar&id={$row['idClientes']}><img src='../utils/edit_16x16.png' title='Editar' border='0'></a></td>";
			}else{
			echo "<td><a href=?adm=paciente&act=show&id={$row['idClientes']}><img src='../utils/details_16x16.png' title='Detalhes' border='0'></a></td>";
			}
                       echo"<td><div class='barra_de_progresso' x='{$row['PORCENTAGEM']}'></div></td>";
			echo "</tr>";
                        $r=$row['PORCENTAGEM'];
		}
                                           
		echo "</table>";
                echo"";*/
                 

	}

	public function nova_capitacao_medica($id){

		$id = anti_injection($id,"numerico");
		$result = mysql_query("SELECT p.nome FROM clientes as p WHERE p.idClientes = '{$id}'");
		$d = new DadosFicha();
		while($row = mysql_fetch_array($result)){
			$d->pacienteid = $id;
			$d->pacientenome = ucwords(strtolower($row["nome"]));
		}

		$this->capitacao_medica($d,"");

	}

	public function detalhe_prescricao($id,$idpac){
		$this->cabecalho($idpac);
		echo "<center><h1>Detalhes Prescri&ccedil;&otilde;es</h1></center>";
		 
		 
		echo"<form method=post target='_blank' action='imprimir_prescricao.php?id={$id}&idpac={$idpac}'>";
		echo"<input type=hidden value={$idpac} name='imprimir_det_presc'</input>";
		echo "<button id='imprimir_det_presc' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
				<span class='ui-button-text'>Imprimir</span></button>";
		echo "</form>";
		 
		echo "<table width=100% class='mytable'><thead><tr>";
		echo "<th><b>Itens prescritos</b></th>";
		echo "</tr></thead>";
		/*$sql2="select id from prescricoes where  ID_CAPMEDICA='{$id}'";
		 $x = mysql_query($sql2);
		$row2 = mysql_fetch_array($x);
		$id2 = $row2['id'];*/
		 
		 
		$sql = "SELECT
		I.*,
                U.nome as nomeuser,
                U.idUsuarios,P.Carater,
		v.via as nvia,
                f.frequencia as nfrequencia,
                I.inicio,
		I.fim, 
                DATE_FORMAT(I.aprazamento,'%H:%i') as apraz,
                ume.unidade as um
		FROM
		prescricoes AS P INNER JOIN
		itens_prescricao AS I ON (P.id = I.idPrescricao)  LEFT JOIN
		catalogo AS B ON ( I.CATALOGO_ID = B.ID) INNER JOIN
		usuarios AS U ON (U.idUsuarios=P.criador) LEFT OUTER JOIN 
                frequencia as f ON (I.frequencia = f.id) LEFT OUTER JOIN viaadm as v ON (I.via = v.id)
		LEFT OUTER JOIN umedidas as ume ON (I.umdose = ume.id)
		WHERE
		P.Carater IN (4,7) AND
		P.ID_CAPMEDICA = {$id} group by I.id";

		$result = mysql_query($sql);
		$n=0;
		$iduser=0;
		$valCarater=0;
		while($row = mysql_fetch_array($result)){


			if($row["Carater"] == 4){
				$carater= " -Prescri&ccedil;&atilde;o Peri&oacute;dica de Avalia&ccedil;&atilde;o";
			}
			if($row["Carater"] == 7){
				$carater= " -Prescri&ccedil;&atilde;o Aditiva de Avalia&ccedil;&atilde;o";
			}
			if($n++%2==0)
				$cor = '#E9F4F8';
			else
				$cor = '#FFFFFF';
			 
			$tipo = $row['tipo'];
			$i = implode("/",array_reverse(explode("-",$row['inicio'])));
			$f = implode("/",array_reverse(explode("-",$row['fim'])));
			$aprazamento = "";
			if($row['tipo'] != "0") $aprazamento = $row['apraz'];
			$linha = "";
			if($row['tipo'] != "-1"){
				$aba = $this->abas[$tipo];
				$dose = "";
				if($row['dose'] != 0) $dose = $row['dose']." ".$row['um'];
				$linha = "<b>$aba:</b> {$row['nome']} {$row['apresentacao']} {$row['nvia']} {$dose} {$row['nfrequencia']} Per&iacute;odo: de $i até $f OBS: {$row['obs']}";
			}
			else $linha = $row['descricao'];
			 
			if($iduser != $row['idUsuarios'] || $valCarater != $row["Carater"]){
				$iduser= $row['idUsuarios'];
				$valCarater=$row["Carater"];
				echo"<tr bgcolor='grey'><td><center><b>Solicitante {$row['nomeuser']}{$carater}</center></b></td></tr>";
			}
			echo "<tr bgcolor={$cor} >";
			echo "<td>$linha</td>";
			echo "</tr>";
		}
		echo "</table>";

		echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
		 
		 
		 
	}

	public function detalhe_score($id){
		$id = anti_injection($id,"numerico");
		$sql = "SELECT sc_pac.OBSERVACAO,sc_pac.ID_PACIENTE, sc_item.DESCRICAO ,sc_item.PESO ,sc_item.GRUPO_ID ,
		sc_item.ID ,sc.id as tbplano,cap.justificativa,DATE(sc_pac.DATA) as DATA,
		grup.DESCRICAO as grdesc  FROM score_paciente sc_pac
		LEFT OUTER JOIN score_item sc_item ON sc_pac.ID_ITEM_SCORE = sc_item.ID
		LEFT OUTER JOIN capmedica cap ON sc_pac.ID_CAPMEDICA = cap.id
		LEFT OUTER JOIN grupos grup ON sc_item.GRUPO_ID = grup.ID
		LEFT OUTER JOIN score sc ON sc_item.SCORE_ID = sc.ID
		WHERE sc_pac.ID_CAPMEDICA = '{$id}'";
		$result = mysql_query($sql);
		$result1 = mysql_query($sql);
		$result2 = mysql_query($sql);
		$row3 = mysql_fetch_array($result2);
		 
		$sql2="SELECT
		UPPER(c.nome) AS paciente,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresa,
		c.`nascimento`,
		p.nome as Convenio,p.id,
		NUM_MATRICULA_CONVENIO
		FROM
		clientes AS c LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN
		planosdesaude as p ON (p.id = c.convenio)
		WHERE
		c.idClientes ={$row3['ID_PACIENTE']}
		ORDER BY
		c.nome DESC LIMIT 1";
		 
		 
		$result3 = mysql_query($sql2);
		 
		 
		echo"<table style='width:95%;'>";
		//echo "<a href=imprimir_score.php?id={$id}>imprimir</a></br>";
		echo"<form method=post target='_blank' action='imprimir_score.php?id={$id}'>";
		echo"<input type=hidden value={$row3['ID_PACIENTE']} name='imprimir_score'</input>";
		echo "<button id='imprimir_score' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
				<span class='ui-button-text'>Imprimir</span></button>";
		echo "</form>";
		echo"<h1><center>Detalhes Score</center></h1>";
		if ($row3['tbplano'] == 1){
			echo "<tr><td><img src='../utils/petro.jpg' title='' border='0' ></td></tr>";
			echo"<tr  bgcolor='grey'><td colspan='3'><center><b>Tabela de Avalia&ccedil;&atilde;o Petrobras.</b>&nbsp;&nbsp;<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> Efetuada em: </b>".join("/",array_reverse(explode("-",$row3['DATA'])))."</center></td></tr>";
		}else{
			echo "<tr><td><img src='../utils/abmid.jpg' title='' border='0' ></td></tr>";
			echo"<tr bgcolor='grey'><td colspan='3'  ><center><b>Tabela de Avalia&ccedil;&atilde;o ABEMID.</b> &nbsp;&nbsp;<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> Efetuada em: </b>".join("/",array_reverse(explode("-",$row3['DATA'])))."</center></td></tr>";
		}

		while($pessoa = mysql_fetch_array($result3)){
			foreach($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);

			}
				

			echo "<tr style='background-color:#EEEEEE;'>";
			echo "<td colspan='3'><label><b>PACIENTE: </b>".$pessoa['paciente']."</label></td>";
			echo "</tr></br>";
				
				
			echo "<tr><td colspan='3'><b>SEXO: </b>".$pessoa['sexo']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IDADE: </b>".join("/",array_reverse(explode("-",$pessoa['nascimento']))).' ('.$pessoa['idade']." anos)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
			echo "</tr></br>";
			echo "<tr style='background-color:#EEEEEE;'>";
			echo "<td colspan='3'><label><b>CONV&Ecirc;NIO: </b>".strtoupper($pessoa['Convenio'])."</label>&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATR&Iacute;CULA: </b>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</label></td>";
		}


		while($row = mysql_fetch_array($result)){
			if($row['OBSERVACAO'] != null && $row['OBSERVACAO'] != '')
				echo"<tr><td width=50% ><b>".$row['DESCRICAO']."</b></td><td colspan='2'>".$row['OBSERVACAO']."</td></tr>";
		}
		echo"<tr bgcolor='grey'><td><b>Descri&ccedil;&atilde;o</b></td><td><b>Itens da Avalia&ccedil;&atilde;o</b></td><td><b>Peso</b></td></tr>";
		while($row = mysql_fetch_array($result1)){
			if($row['OBSERVACAO'] == null && $row['OBSERVACAO'] == '')

				echo"<tr><td width=40% >".$row['grdesc']."</td><td width=45% >".$row['DESCRICAO']."</td><td colspan='2' >".$row['PESO']."</td></tr>";
			$x += $row['PESO'];
		}
		echo"<tr bgcolor='grey'><td colspan='3' aling='center'><b>TOTAL DO SCORE : {$x}</b></td></tr>";
		echo"<tr bgcolor='grey'><td colspan='3' aling='center'><b>Justificativa para a interna&ccedil;&atilde;o:</b></td></tr>";
		echo"<tr ><td colspan='3' aling='center'>".$row3['justificativa']."</td></tr>";
		echo"</table>";
		echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
		 
	}


	public function detalhes_capitacao_medica($id){
		$id = anti_injection($id,"numerico");
		$sql = "SELECT cap.*, pac.nome as pnome,pac.idClientes,usr.nome as unome, DATE_FORMAT(cap.data,'%d/%m/%Y') as fdata, comob.*
		FROM capmedica as cap
		LEFT OUTER JOIN comorbidades as comob ON cap.id = comob.capmed
		LEFT OUTER JOIN problemasativos as prob ON cap.id = prob.capmed
		LEFT OUTER JOIN usuarios as usr ON cap.usuario = usr.idUsuarios
		LEFT OUTER JOIN clientes as pac ON cap.paciente = pac.idClientes
			
		WHERE cap.id = '{$id}'";
		$d = new DadosFicha();
		$d->capmedica = $id;
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result)){
			 
			$d->pacienteid= $row['idClientes'];
			$d->usuario = ucwords(strtolower($row["unome"])); $d->data = $row['fdata']; $d->pacientenome = ucwords(strtolower($row["pnome"]));
			$d->motivo = $row['motivo'];
			$d->alergiamed = $row['alergiamed']; $d->alergiaalim = $row['alergiaalim'];
			$d->tipoalergiamed = $row['tipoalergiamed']; $d->tipoalergiaalim = $row['tipoalergiaalim'];
			$d->prelocohosp = $row['hosplocomocao']; $d->prehigihosp = $row['hosphigiene'];
			$d->preconshosp = $row['hospcons']; $d->prealimhosp = $row['hospalimentacao']; $d->preulcerahosp = $row['hospulcera'];
			$d->prelocodomic = $row['domlocomocao']; $d->prehigidomic = $row['domhigiene']; $d->preconsdomic = $row['domcons'];
			$d->prealimdomic = $row['domalimentacao']; $d->preulceradomic = $row['domulcera'];
			$d->objlocomocao = $row['objlocomocao']; $d->objhigiene = $row['objhigiene']; $d->objcons = $row['objcons'];
			$d->objalimento = $row['objalimentacao']; $d->objulcera = $row['objulcera'];
			$d->shosp = $d->prelocohosp + $d->prehigihosp +	$d->preconshosp + $d->prealimhosp + $d->preulcerahosp;
			$d->sdom = $d->prelocodomic + $d->prehigidomic +	$d->preconsdomic + $d->prealimdomic + $d->preulceradomic;;
			$d->sobj = $d->objlocomocao + $d->objhigiene + $d->objcons + $d->objalimento + $d->objulcera;

			$d->qtdinternacoes = $row['qtdinternacoes']; $d->historico = $row['historicointernacao'];
			$d->parecer = $row['parecer']; $d->justificativa = $row['justificativa'];

			$d->cancer = $row['cancer']; $d->psiquiatrico = $row['psiquiatrico']; $d->neuro = $row['neuro']; $d->glaucoma = $row['glaucoma'];
			$d->hepatopatia = $row['hepatopatia']; $d->obesidade = $row['obesidade']; $d->cardiopatia = $row['cardiopatia']; $d->dm = $row['dm'];
			$d->reumatologica = $row['reumatologica']; $d->has = $row['has']; $d->nefropatia = $row['nefropatia']; $d->pneumopatia = $row['pneumopatia'];
			$d->modalidade = $row['MODALIDADE']; $d->local_av = $row['LOCAL_AV']; $d->pre_justificativa = $row['PRE_JUSTIFICATIVA']; $d->plano_desmame = $row['PLANO_DESMAME'];
		}
		$sql = "SELECT p.DESCRICAO as nome FROM problemasativos as p WHERE p.capmed = '{$id}' ";
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result)){
			 
			$d->ativos[] = $row['nome'];
		}



		//$this->cabecalho(pacienteid);
		$this->capitacao_medica($d,"readonly");
		echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";



	}

	public function cabecalho($dados){
		if(isset($dados->pacienteid))
			$id = $dados->pacienteid;
		else
			$id = $dados;
		$condp1= "c.idClientes = '{$id}'";
		$sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresa,
		c.`nascimento`,
		p.nome as Convenio,p.id,
		NUM_MATRICULA_CONVENIO
		FROM
		clientes AS c LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN
		planosdesaude as p ON (p.id = c.convenio)
		WHERE
		{$condp1}
		ORDER BY
		c.nome DESC LIMIT 1";
		 
		$result = mysql_query($sql);
		$result2 = mysql_query($sql2);
		$html .= "<table width=100% style='border:2px dashed;'>";
		while($pessoa = mysql_fetch_array($result2)){
			foreach($pessoa AS $chave => $valor) {
				$pessoa[$chave] = stripslashes($valor);
				 
			}
			$html .= "<br/><tr>";
			$html .= "<td colspan='2'><label style='font-size:14px;color:#8B0000'><b><center> INFORMA&Ccedil;&Otilde;ES DO PACIENTE </center></b></label></td>";
			$html .= "</tr>";
			 
			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>PACIENTE </b></label></td>";
			$html .= "<td><label><b>SEXO </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>" . String::removeAcentosViaEReg($pessoa['paciente']) . "</td>";
			$html .= "<td>{$pessoa['sexo']}</td>";
			$html .= "</tr>";
			 
			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>IDADE </b></label></td>";
			$html .= "<td><label><b>UNIDADE REGIONAL </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>".join("/",array_reverse(explode("-",$pessoa['nascimento']))).' ('.$pessoa['idade']." anos)</td>";
			$html .= "<td>{$pessoa['empresa']}</td>";
			$html .= "</tr>";
			 
			$html .= "<tr style='background-color:#EEEEEE;'>";
			$html .= "<td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>";
			$html .= "<td><label><b>MATR&Iacute;CULA </b></label></td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>".strtoupper($pessoa['Convenio'])."</td>";
			$html .= "<td>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
			$html .= "</tr>";
			$this->plan =  $pessoa['id'];
		}
		$html .= "</table><br/>";
		 return $html;
		 
	}
	public function equipamentos($id=null){

		echo "<tr><thead>";
		echo "<th colspan='3'>Equipamentos</th>";
		echo "</thead></tr>";
		if($id==null){
			$sql= "select * from cobrancaplanos where tipo=1";
			 
	  $result=mysql_query($sql);
	   
	  echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
	  while($row=mysql_fetch_array($result)){
	  	echo"<tr><td><input type='checkbox' class= equipamento cod_equipamento='{$row['id']}' />{$row['item']}</td>";
	  	echo"<td><input style='width:100%' type='texto' class='equipamento' id=obs"."{$row['id']} readonly name='obs_equpipamento'  {$acesso} /></td>";
	  	echo"<td><input type='texto' readonly id=qtd"."{$row['id']}   class='qtd_equipamento'   name='qtd_equipamento'  {$acesso} /></td></tr>";
	  }
		}else{
			 
			$sql = "SELECT
					C.id,
					C.item,
					C.unidade,
					C.tipo,
					eqp.QTD,
					eqp.OBSERVACAO
					FROM
					equipamentos_capmedica as eqp INNER JOIN
					cobrancaplanos as C ON (C.id = eqp.COBRANCAPLANOS_ID)
					 
					WHERE
					eqp.CAPMEDICA_ID = ".$id;


			$result = mysql_query($sql);
			 
	  echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td>";
	  echo"<td><b>Qtd.</b></td></tr>";
	  while($row=mysql_fetch_array($result)){
	  	echo"<tr><td><input type='checkbox' class= equipamento cod_equipamento='{$row['id']}' disabled='disabled' />{$row['item']}</td>";
	  	echo"<td><input type='texto' style='width:100%' value='{$row['OBSERVACAO']}' class='equipamento' id=obs"."{$row['id']} readonly='readonly' name='obs_equpipamento'  {$acesso} ></td>";
	  	echo"<td><input type='texto' readonly='readonly' id=qtd"."{$row['id']} value='{$row['QTD']}'  class='qtd_equipamento'   name='qtd_equipamento'  {$acesso} ></td></tr>";
	  }
		}
	}

	public function equipamentos_editar($id){

		echo "<thead><tr>";
		echo "<th colspan='3' >Equipamentos</th>";
		echo "</tr></thead>";
		$sql = "SELECT
				C.id,
				C.item,
				C.unidade,
				C.tipo,
				eqp.QTD,
				eqp.OBSERVACAO
				FROM
				equipamentos_capmedica as eqp INNER JOIN
				cobrancaplanos as C ON (C.id = eqp.COBRANCAPLANOS_ID)
				 
				WHERE
				eqp.CAPMEDICA_ID = ".$id;
		 
		 
		$result = mysql_query($sql);
		$equipamento = '';
		//echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
		while($row=mysql_fetch_array($result)){
			$equipamento[$row[id]] = array("item"=>$row['item'],"und"=>$row['unidade'],"tipo"=>$row['tipo'],"qtd"=>$row['QTD'],"obs"=>$row['OBSERVACAO']);
				


			/*echo"<tr><td><input type='checkbox' class= equipamento cod_equipamento='{$row['id']}' disabled='disabled' ></input>{$row['item']}</td><td><input type='texto' style='width:100%' value='{$row['OBSERVACAO']}' class='equipamento' id=obs"."{$row['id']} readonly='readonly' name='obs_equpipamento'  {$acesso} ></input></td>
			 <td><input type='texto' readonly='readonly' id=qtd"."{$row['id']} value='{$row['QTD']}'  class='qtd_equipamento'   name='qtd_equipamento'  {$acesso} ></input></td></tr>";
			*/	}
			 
			 

			$sql2= "select * from cobrancaplanos where tipo=1";

			$result2=mysql_query($sql2);

			echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
			while($row=mysql_fetch_array($result2)){
				if(array_key_exists($row['id'], $equipamento)){
					$check = "checked='checked'";
					$qtd = $equipamento[$row['id']]['qtd'];
					$obs = $equipamento[$row['id']]['obs'];

				}else{
					$check ='';
					$qtd='';
					$obs = '';
				}
				echo"<tr><td><input type='checkbox' $check class= equipamento cod_equipamento='{$row['id']}' ></input>{$row['item']}</td><td><input style='width:100%' type='texto' class='equipamento' id=obs"."{$row['id']} value='{$obs}' readonly name='obs_equpipamento'  {$acesso} ></input></td>
				<td><input type='texto' readonly id=qtd"."{$row['id']}   class='qtd_equipamento' value='{$qtd}'  name='qtd_equipamento'  {$acesso} ></input></td></tr>";
			}
	}

	public function score_petrobras($dados=null){
		 
		echo "<thead><tr>";
		 
		$id= $dados->capmedica;
		echo "<th colspan='2' >Tabela de Avalia&ccedil;&atilde;o de Complexidade Petrobras.</th><th></th></tr></thead>";
		echo"<tr bgcolor='grey'><td colspan='3'><b>Informa&ccedil;&otilde;es</b></td></tr>";

		echo "<tr><td colspan='2'> Diagno&Oacute;stico Principal: &nbsp;&nbsp;        <input type='texto' name='dias' class='cabec1' cod_item='69'  value='{$dados->diag_principal}' style='width:100%' /> </td></tr>";
		echo "<tr><td colspan='2'> Diagno&oacute;stico Secund&aacute;rio: <input type='texto' name='dias' class='cabec1'  cod_item='70' style='width:100%'  value='{$dados->diag_secundario}' /> </td></tr>";
		 
		 
		$sql2 = "SELECT sc_pac.OBSERVACAO,sc_pac.ID_PACIENTE,sc_pac.ID_ITEM_SCORE, sc_item.DESCRICAO ,sc_item.PESO ,sc_item.GRUPO_ID ,
		sc_item.ID ,sc.id as tbplano,cap.justificativa,DATE(sc_pac.DATA) as DATA,
		grup.DESCRICAO as grdesc  FROM score_paciente sc_pac
		LEFT OUTER JOIN score_item sc_item ON sc_pac.ID_ITEM_SCORE = sc_item.ID
		LEFT OUTER JOIN capmedica cap ON sc_pac.ID_CAPMEDICA = cap.id
		LEFT OUTER JOIN grupos grup ON sc_item.GRUPO_ID = grup.ID
		LEFT OUTER JOIN score sc ON sc_item.SCORE_ID = sc.ID
		WHERE sc_pac.ID_CAPMEDICA = '{$id}'";
		$result2 = mysql_query($sql2);
		$elemento = '';
		//echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
		while($row=mysql_fetch_array($result2)){
			 
			$elemento[$row['ID_ITEM_SCORE']] = array("id_elemento"=>$row['ID_ITEM_SCORE'],"peso"=>$row['PESO']);
		}
		//print_r($elemento);
		$sql= "select sc_item.*, gru.DESCRICAO as dsc, gru.QTD from score_item as sc_item LEFT JOIN grupos as gru ON(sc_item.GRUPO_ID = gru.ID) where sc_item.SCORE_ID = 1 order by sc_item.GRUPO_ID";
		$result = mysql_query($sql);
		$cont=0;
		 
		$i = 0;
		$cont1 = 1;
		echo"<tr bgcolor='grey'><td><b>Descri&ccedil;&atilde;o</b></td><td colspan='2'><b>Itens da Avalia&ccedil;&atilde;o</b></td></tr>";
		while($row = mysql_fetch_array($result)){
			if(is_array($elemento)){

				if(array_key_exists($row['ID'], $elemento)){
					$check = "checked='checked'";
					$dados->spetro +=  $elemento[$row['ID']]['peso'];
						
				}else{
					$check ='';
				}
			}else{
				$check ='';
			}
			 
			if($cont1 == 1){
				$i=$row['QTD']+1;
				$cont = $cont1;
			}else{
				$i = 0;
			}
			if($row['GRUPO_ID'] >=2 && $row['GRUPO_ID']<=11)
			{
				if($cont1 == 1)
				{
					if($x=="#E9F4F8")
						$x='';
					else
						$x="#E9F4F8";
					echo "<tr bgcolor='{$x}'><td ROWSPAN='{$i}'>{$row['dsc']}</td></tr>";
				}
				echo "<tr bgcolor='{$x}'><td colspan='2'><a class='limpar' title='Desmarcar' ><img src='../utils/desmarcar16x16.png' width='16' title='Desmarcar' border='0'></a><input type='radio'  {$check} class='petro' name=".$row['dsc']." value=".$row['PESO']." cod_item=".$row['ID']."> {$row['DESCRICAO']}";
				echo "</td></tr>";
			}
			 
			$cont1++;
			 
			if($cont1 > $row['QTD'])
				$cont1 = 1;
		}
		 
		$score = "<input type='text' readonly size='2' name='spetro' value='{$dados->spetro}' {$acesso} />";
		echo"<tr bgcolor='grey'><td colspan='3' aling='center'><b>TOTAL DO SCORE SEGUNDO TABELA PETROBRAS: {$score}</b></td></tr>";
		echo "<tr><td> Di&aacute;ria Global Alta Complexidade c/ VM 24h durante: <input type='texto' name='dias' class='cabec1 numerico' cod_item='63'  size='5' value='{$dados->atend_24}' /> dias.</td></tr>";
		echo "<tr><td> Di&aacute;ria Global Alta Complexidade s/ VM 24h durante:&nbsp;<input type='texto' name='dias' class='cabec1 numerico' cod_item='62' size='5' value='{$dados->atend_24_2}'/> dias.</td></tr>";
		echo "<tr><td> Di&aacute;ria Global M&eacute;dia Complexidade 12h durante:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' name='dias' class='cabec1 numerico' cod_item='64' size='5' value='{$dados->atend_12}' />  dias.</td></tr>";
		echo "<tr><td> Di&aacute;ria  Global Baixa Complexidade 06h durante:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type='texto' name='dias' class='cabec1 numerico' cod_item='65' size='5' value='{$dados->atend_06}' /> dias.";
		echo "</td></tr>";

	}

	public function score_abmid($dados=null){

		echo "<thead><tr>";
		 
		$cont = "<input type='text' readonly size='2' name='sabmid1' value='{$dados->sabmid1}' {$acesso} />";
		$tex = "<input type='text'  name='sabmid2' value='{$dados->sabmid2}' {$acesso} />";
		$texto = utf8_decode($tex);
		$id=$dados->capmedica;
		 
		echo "<th colspan='3'>Tabela de Avalia&ccedil;&atilde;o de Complexidade ABEMID.</th></tr></thead>";
		echo"<tr bgcolor='grey'><td colspan='3'><b>Informa&ccedil;&otilde;es</b></td></tr>";
		echo "<tr><td colspan='2'> Diagno&Oacute;stico Principal: <input type='texto' name='dias' class='cabec1 OBG' cod_item='71'  value='{$dados->diag_principal}'style='width:100%'> </td></tr>";
		echo "<tr><td colspan='2'> Diagno&oacute;stico Secund&aacute;rio: <input type='texto' class='cabec1' name='dias' cod_item='72' value='{$dados->diag_secundario}' style='width:100%'> </td></tr>";
		 
		$sql2 = "SELECT sc_pac.OBSERVACAO,sc_pac.ID_PACIENTE,sc_pac.ID_ITEM_SCORE, sc_item.DESCRICAO ,sc_item.PESO ,sc_item.GRUPO_ID ,
		sc_item.ID ,sc.id as tbplano,cap.justificativa,DATE(sc_pac.DATA) as DATA,
		grup.DESCRICAO as grdesc  FROM score_paciente sc_pac
		LEFT OUTER JOIN score_item sc_item ON sc_pac.ID_ITEM_SCORE = sc_item.ID
		LEFT OUTER JOIN capmedica cap ON sc_pac.ID_CAPMEDICA = cap.id
		LEFT OUTER JOIN grupos grup ON sc_item.GRUPO_ID = grup.ID
		LEFT OUTER JOIN score sc ON sc_item.SCORE_ID = sc.ID
		WHERE sc_pac.ID_CAPMEDICA = '{$id}'";
		$result2 = mysql_query($sql2);
		$elemento = '';
		//echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
		while($row=mysql_fetch_array($result2)){
			 
			$elemento[$row['ID_ITEM_SCORE']] = array("id_elemento"=>$row['ID_ITEM_SCORE'],"peso"=>$row['PESO']);
		}

		$sql= "select sc_item.*, gru.DESCRICAO as dsc, gru.QTD from score_item as sc_item LEFT JOIN grupos as gru ON(sc_item.GRUPO_ID = gru.ID) where sc_item.SCORE_ID = 2 order by sc_item.GRUPO_ID";
		 
		$result = mysql_query($sql);
		$cont=0;
		 
		$i = 0;
		$cont1 = 1;
		echo"<tr bgcolor='grey'><td><b>Descri&ccedil;&atilde;o</b></td><td colspan='2'><b>Itens da Avalia&ccedil;&atilde;o</b></td></tr>";
		while($row = mysql_fetch_array($result)){
			if(is_array($elemento)){
				if(array_key_exists($row['ID'], $elemento)){
					$check = "checked='checked'";
					$dados->sabmid += $elemento[$row['ID']]['peso'];
				}else{
					$check ='';
				}
			}else{
				$check ='';
			}

			if($cont1 == 1){
				$i=$row['QTD']+1;
				$cont = $cont1;
			}else{
				$i = 0;
			}



			if($row['GRUPO_ID'] == 1){
				if($cont1 == 1){
					if($x=="#E9F4F8")
						$x='';
					else
						$x="#E9F4F8";

					echo "<tr bgcolor='{$x}'><td ROWSPAN='{$i}'>{$row['dsc']}</td></tr>";
				}
				echo "<tr bgcolor='{$x}'><td colspan='2'><input type='checkbox' class='abmid' name=".$row['GRUPO_ID']." cod_item=".$row['ID']." {$check} value=".$row['PESO']."  > {$row['DESCRICAO']}";
				echo "</td></tr>";
			}


			if($row['GRUPO_ID'] >=2 && $row['GRUPO_ID']<=11)
			{
				if($cont1 == 1)
				{
					if($x=="#E9F4F8")
						$x='';
					else
						$x="#E9F4F8";
					echo "<tr bgcolor='{$x}'><td ROWSPAN='{$i}'>{$row['dsc']}</td></tr>";
				}
				echo "<tr bgcolor='{$x}'><td colspan='2'><a class='limpar' title='Desmarcar' ><img src='../utils/desmarcar16x16.png' width='16' title='Desmarcar' border='0'></a><input type='radio'  {$check} class='abmid' name=".$row['dsc']." value=".$row['PESO']." cod_item=".$row['ID']."> {$row['DESCRICAO']}";
				echo "</td></tr>";
			}

			$cont1++;

			if($cont1 > $row['QTD'])
				$cont1 = 1;
		}
		 
		 

		$score = "<input type='text' readonly size='2' name='sabmid' value='{$dados->sabmid}' {$acesso} />";
		echo"<tr bgcolor='grey'><td colspan='3' aling='center'><b>TOTAL DO SCORE SEGUNDO TABELA ABEMID: {$score}</b></td></tr>";
		echo "<tr><td colspan='2'> Programa&ccedil;&atilde;o em dias de atendimento-24 h: <input type='texto' class='cabec1 numerico' name='dias' cod_item='66' value='{$dados->atend_24}' size='5'> dias.</td></tr>";
		echo "<tr><td colspan='2'> Programa&ccedil;&atilde;o em dias de atendimento-12 h: <input type='texto' class='cabec1 numerico' name='dias' cod_item='67'  value='{$dados->atend_12}' size='5'>  dias.</td></tr>";
		echo "<tr><td colspan='2'> Programa&ccedil;&atilde;o em dias de atendimento-06 h: <input type='texto' class='cabec1 numerico' name='dias' cod_item='68'  value='{$dados->atend_06}'  size='5'> dias.";
		echo "</td></tr>";
	}
	////20-08
	public function salv_capmed($id){
		 
		$sql5= "select nome FROM clientes where idClientes ={$id}";
		$x = mysql_query($sql5);
		$y=mysql_fetch_array($x);
		 
		echo "<div id='dialog-presc-avaliacao' title='Prescri&ccedil;&atilde;o para Avalia&ccedil;&atilde;o'>";
		echo"<input type='hidden' id='idpacav' value=".$id." ></input>";
		echo"<input type='hidden' id='tipoav' value='4' ></input>";
		echo"<input type='hidden' id='capmed_ava' value='' idcap='' ></input>";

		echo "<fieldset><legend><b>Paciente</b></legend><span id='NomePacientePresc' >".$y['nome']."</span></fieldset><br/>";
		$this->periodo();
		echo "<button id='iniciar_pres_avaliacao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>iniciar</span></button></span>";

		echo "</div>";
	}

	public function presc_aditiva($id){

		$sql5= "select nome FROM clientes where idClientes ={$id}";
		$x = mysql_query($sql5);
		$y=mysql_fetch_array($x);

		echo "<div id='dialog-presc-avaliacao-aditiva' title='Prescri&ccedil;&atilde;o para Avalia&ccedil;&atilde;o'>";
		echo"<input type='hidden' id='idpacav' value=".$id." ></input>";
		echo"<input type='hidden' id='tipoav' value='7' ></input>";
		echo"<input type='hidden' id='capmed_ava' value='' idcap='' ></input>";

		echo "<fieldset><legend><b>Paciente</b></legend><span id='NomePacientePresc' >".$y['nome']."</span></fieldset><br/>";
		$this->periodo();
		echo "<button id='iniciar_pres_avaliacao_aditiva' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>iniciar</span></button></span>";

		echo "</div>";
	}

	public function editar_prescricao($id){

		$sql5= "select nome FROM clientes where idClientes ={$id}";
		$x = mysql_query($sql5);
		$y=mysql_fetch_array($x);
		 

		echo "<div id='dialog-editar-presc-avaliacao' title='Editar Prescri&ccedil;&atilde;o para Avalia&ccedil;&atilde;o'>";
		 
		echo"<input type='hidden' id='edt' value='' edt='' ></input>";
		echo"<input type='hidden' id='paciente' value='' paciente='' ></input>";
		echo"<input type='hidden' id='edt_idpresc' value='' antiga='' ></input>";
		echo"<input type='hidden' id='edtidcap' value='' edtidcap='' ></input>";
		 

		echo "<fieldset><legend><b>Paciente</b></legend><span id='Nome_Paciente_Presc' >".$y['nome']."</span></fieldset><br/>";
		$this->periodo();
		echo "<button id='iniciar_editar_pres_avaliacao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>iniciar</span></button></span>";

		echo "</div>";
	}

	public function nova_presc($id){

		$sql5= "select nome FROM clientes where idClientes ={$id}";
		$x = mysql_query($sql5);
		$y=mysql_fetch_array($x);


		echo "<div id='dialog-novapresc' title='Prescri&ccedil;&atilde;o para Avalia&ccedil;&atilde;o'>";

		echo"<input type='hidden' id='npaciente' value='' paciente='' ></input>";
		echo"<input type='hidden' id='nedt_idpresc' value='' antiga='' ></input>";
		echo"<input type='hidden' id='ncapmed_ava' value='' idcap='' ></input>";
		echo"<input type='hidden' id='tipoav' value='' ></input>";

		echo "<fieldset><legend><b>Paciente</b></legend><span id='Nome_Paciente_NovaPresc' >".$y['nome']."</span></fieldset><br/>";
		$this->periodo();
		echo "<button id='iniciar_nova_presc_avaliacao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>iniciar</span></button></span>";

		echo "</div>";
	}

	public function cond_pre_ihosp($dados,$enabled){
		 
		echo "<thead><tr>";
		$score = "SCORE<input type='text' size='2' name='shosp' value='{$dados->shosp}' {$acesso} />";
		echo "<th colspan='3' >Condi&ccedil;&otilde;es do paciente pr&eacute;-interna&ccedil;&atilde;o hospitalar {$score} </th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='3' ><b>Locomo&ccedil;&atilde;o:</b>";
		echo "<input type='radio' class='hosp' name='prelocohosp' value=3 ".(($dados->prelocohosp=='3')?"checked":"")." {$enabled} /> Deambulava";
		echo "<input type='radio' class='hosp' name='prelocohosp' value=2 ".(($dados->prelocohosp=='2')?"checked":"")." {$enabled} /> Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)";
		echo "<input type='radio' class='hosp' name='prelocohosp' value=1 ".(($dados->prelocohosp=='1')?"checked":"")." {$enabled} /> Restrito ao leito";
		echo "</td></tr>";
		echo "<tr><td colspan='3' ><b>Autonomia para higiene pessoal:</b>";
		echo "<input type='radio' class='hosp' name='prehigihosp' value=2 ".(($dados->prehigihosp=='2')?"checked":"")." {$enabled} /> Sim";
		echo "<input type='radio' class='hosp' name='prehigihosp' value=1 ".(($dados->prehigihosp=='1')?"checked":"")." {$enabled} /> N&atilde;o (Oral, deje&ccedil;&otilde;es, mic&ccedil&atilde;o e banho)";
		echo "</td></tr>";
		echo "<tr><td colspan='3' ><b>N&iacute;vel de consci&ecirc;ncia:</b>";
		echo "<input type='radio' class='hosp' name='preconshosp' value=3 ".(($dados->preconshosp=='3')?"checked":"")." {$enabled} /> LOTE";
		echo "<input type='radio' class='hosp' name='preconshosp' value=2 ".(($dados->preconshosp=='2')?"checked":"")." {$enabled} /> Desorientado";
		echo "<input type='radio' class='hosp' name='preconshosp' value=1 ".(($dados->preconshosp=='1')?"checked":"")." {$enabled} /> Inconsciente";
		echo "</td></tr>";
		echo "<tr><td colspan='3' ><b>Alimenta&ccedil;&atilde;o:</b>";
		echo "<input type='radio' class='hosp' name='prealimhosp' value=4 ".(($dados->prealimhosp=='4')?"checked":"")." {$enabled} /> N&atilde;o assistida";
		echo "<input type='radio' class='hosp' name='prealimhosp' value=3 ".(($dados->prealimhosp=='3')?"checked":"")." {$enabled} /> Assistida oral";
		echo "<input type='radio' class='hosp' name='prealimhosp' value=2 ".(($dados->prealimhosp=='2')?"checked":"")." {$enabled} /> Enteral";
		echo "<input type='radio' class='hosp' name='prealimhosp' value=1 ".(($dados->prealimhosp=='1')?"checked":"")." {$enabled} /> Parenteral";
		echo "</td></tr>";
		echo "<tr><td colspan='3' ><b>&Uacute;lceras de press&atilde;o:</b>";
		echo "<input type='radio' class='hosp' name='preulcerahosp' value=2 ".(($dados->preulcerahosp=='2')?"checked":"")." {$enabled} /> N&atilde;o";
		echo "<input type='radio' class='hosp' name='preulcerahosp' value=1 ".(($dados->preulcerahosp=='1')?"checked":"")." {$enabled} /> Sim";
		echo "</td></tr>";
		 
		 
	}

	public function cond_pre_id($dados,$enabled){
		echo "<thead><tr>";
		$score = "SCORE<input type='text' readonly size='2' name='sdom' value='{$dados->sdom}' {$acesso} />";
		echo "<th colspan='3' >Condi&ccedil;&otilde;es do paciente pr&eacute;-interna&ccedil;&atilde;o domiciliar. {$score}</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='2' ><b>Locomo&ccedil;&atilde;o:</b>";
		echo "<input type='radio' class='domic' name='prelocodomic' value=3 ".(($dados->prelocodomic=='3')?"checked":"")." {$enabled} /> Deambulava";
		echo "<input type='radio' class='domic' name='prelocodomic' value=2 ".(($dados->prelocodomic=='2')?"checked":"")." {$enabled} /> Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)";
		echo "<input type='radio' class='domic' name='prelocodomic' value=1 ".(($dados->prelocodomic=='1')?"checked":"")." {$enabled} /> Restrito ao leito";
		echo "</td></tr>";
		echo "<tr><td colspan='2' ><b>Autonomia para higiene pessoal:</b>";
		echo "<input type='radio' class='domic' name='prehigidomic' value=2 ".(($dados->prehigidomic=='2')?"checked":"")." {$enabled} /> Sim";
		echo "<input type='radio' class='domic' name='prehigidomic' value=1 ".(($dados->prehigidomic=='1')?"checked":"")." {$enabled} /> N&atilde;o (Oral, deje&ccedil;&otilde;es, mic&ccedil&atilde;o e banho)";
		echo "</td></tr>";
		echo "<tr><td colspan='2' ><b>N&iacute;vel de consci&ecirc;ncia:</b>";
		echo "<input type='radio' class='domic' name='preconsdomic' value=3 ".(($dados->preconsdomic=='3')?"checked":"")." {$enabled} /> LOTE";
		echo "<input type='radio' class='domic' name='preconsdomic' value=2 ".(($dados->preconsdomic=='2')?"checked":"")." {$enabled} /> Desorientado";
		echo "<input type='radio' class='domic' name='preconsdomic' value=1 ".(($dados->preconsdomic=='1')?"checked":"")." {$enabled} /> Inconsciente";
		echo "</td></tr>";
		echo "<tr><td colspan='2' ><b>Alimenta&ccedil;&atilde;o:</b>";
		echo "<input type='radio' class='domic' name='prealimdomic' value=4 ".(($dados->prealimdomic=='4')?"checked":"")." {$enabled} /> N&atilde;o assistida";
		echo "<input type='radio' class='domic' name='prealimdomic' value=3 ".(($dados->prealimdomic=='3')?"checked":"")." {$enabled} /> Assistida oral";
		echo "<input type='radio' class='domic' name='prealimdomic' value=2 ".(($dados->prealimdomic=='2')?"checked":"")." {$enabled} /> Enteral";
		echo "<input type='radio' class='domic' name='prealimdomic' value=1 ".(($dados->prealimdomic=='1')?"checked":"")." {$enabled} /> Parenteral";
		echo "</td></tr>";
		echo "<tr><td colspan='2' ><b>&Uacute;lceras de press&atilde;o:</b>";
		echo "<input type='radio' class='domic' name='preulceradomic' value=2 ".(($dados->preulceradomic=='2')?"checked":"")." {$enabled} /> N&atilde;o";
		echo "<input type='radio' class='domic' name='preulceradomic' value=1 ".(($dados->preulceradomic=='1')?"checked":"")." {$enabled} /> Sim";
		echo "</td></tr>";
		 
	}

	public function cont_inter_domiciliar($dados,$enabled){
		 
		echo "<thead><tr>";
		$score = "SCORE<input type='text' readonly size='2' name='sobj' value='{$dados->sobj}' {$acesso} />";
		echo "<th colspan='3' >Objetivo da continuidade da interna&ccedil;&atilde;o domiciliar. {$score}</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='2' ><b>Locomo&ccedil;&atilde;o:</b>";
		echo "<input type='radio' class='obj' name='objlocomocao' value=3 ".(($dados->objlocomocao=='3')?"checked":"")." {$enabled} /> Deambulava";
		echo "<input type='radio' class='obj' name='objlocomocao' value=2 ".(($dados->objlocomocao=='2')?"checked":"")." {$enabled} /> Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)";
		echo "<input type='radio' class='obj' name='objlocomocao' value=1 ".(($dados->objlocomocao=='1')?"checked":"")." {$enabled} /> Restrito ao leito";
		echo "</td></tr>";
		echo "<tr><td colspan='2' ><b>Autonomia para higiene pessoal:</b>";
		echo "<input type='radio' class='obj' name='objhigiene' value=2 ".(($dados->objhigiene=='2')?"checked":"")." {$enabled} /> Sim";
		echo "<input type='radio' class='obj' name='objhigiene' value=1 ".(($dados->objhigiene=='1')?"checked":"")." {$enabled} /> N&atilde;o (Oral, deje&ccedil;&otilde;es, mic&ccedil&atilde;o e banho)";
		echo "</td></tr>";
		echo "<tr><td colspan='2' ><b>N&iacute;vel de consci&ecirc;ncia:</b>";
		echo "<input type='radio' class='obj' name='objcons' value=3 ".(($dados->objcons=='3')?"checked":"")." {$enabled} /> LOTE";
		echo "<input type='radio' class='obj' name='objcons' value=2 ".(($dados->objcons=='2')?"checked":"")." {$enabled} /> Desorientado";
		echo "<input type='radio' class='obj' name='objcons' value=1 ".(($dados->objcons=='1')?"checked":"")." {$enabled} /> Inconsciente";
		echo "</td></tr>";
		echo "<tr><td colspan='2' ><b>Alimenta&ccedil;&atilde;o:</b>";
		echo "<input type='radio' class='obj' name='objalimento' value=4 ".(($dados->objalimento=='4')?"checked":"")." {$enabled} /> N&atilde;o assistida";
		echo "<input type='radio' class='obj' name='objalimento' value=3 ".(($dados->objalimento=='3')?"checked":"")." {$enabled} /> Assistida oral";
		echo "<input type='radio' class='obj' name='objalimento' value=2 ".(($dados->objalimento=='2')?"checked":"")." {$enabled} /> Enteral";
		echo "<input type='radio' class='obj' name='objalimento' value=1 ".(($dados->objalimento=='1')?"checked":"")." {$enabled} /> Parenteral";
		echo "</td></tr>";
		echo "<tr><td colspan='2' ><b>&Uacute;lceras de press&atilde;o:</b>";
		echo "<input type='radio' class='obj' name='objulcera' value=2 ".(($dados->objulcera=='2')?"checked":"")." {$enabled} /> N&atilde;o";
		echo "<input type='radio' class='obj' name='objulcera' value=1 ".(($dados->objulcera=='1')?"checked":"")." {$enabled} /> Sim";
		echo "</td></tr>";
		 
		 
	}

	public function capitacao_medica($dados,$acesso){
		$enabled = "";

		if($acesso == "readonly") $enabled = "disabled";
		echo "<center><h1>Ficha m&eacute;dica do paciente</h1></center>";
		//echo "<center><h2>{$dados->pacientenome}</h2></center>";
		$this->cabecalho($dados);


		if($acesso == "readonly"){

			echo "<center><h2>Respons&aacute;vel: {$dados->usuario} - Data: {$dados->data}</h2></center>";
			//echo "<a href=imprimir_ficha.php?id={$dados->capmedica}>imprimir</a></br>";
			echo"<form method=post target='_blank' action='imprimir_ficha.php?id={$dados->capmedica}'>";
			echo"<input type=hidden value={$dados->capmedica} class='imprimir_ficha_mederi'</input>";
			echo "<button id='imprimir_ficha_mederi' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
					<span class='ui-button-text'>Imprimir</span></button>";
			echo "</form>";
		}

		$this->plan;


		echo "<div id='div-ficha-medica'  ><br/>";

		echo alerta();
		echo "<form >";
		echo "<input type='hidden' name='paciente'  value='{$dados->pacienteid }' />";
		echo "<br/><table class='mytable' width=100% ><tbody>";

		if( $acesso != "readonly"){
			if($this->plan == 7 || $this->plan == 27 || $this->plan == 28 || $this->plan == 29){

				$this->score_petrobras();
			}
			else $this->score_abmid();


		}

		echo "<thead ><tr >";
		echo "<th colspan='3'>Ficha de avalia&ccedil;&atilde;o Mederi.</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='2'><b>Modalidade de Home Care:</b><input type='radio' class='modalidade' name='modalidade' value='1' ".(($dados->modalidade=='1')?"checked":"")." {$enabled}>Assistencia Domiciliar</input><input type='radio' class='modalidade' name='modalidade' value='2' ".(($dados->modalidade=='2')?"checked":"")." {$enabled}>Interna&ccedil;&atilde;o Domiciliar</input></td></tr>";
		echo "<tr><td colspan='2'><b>Local de Avalia&ccedil;&atilde;o do paciente:</b><input type='radio' class='local_av'  name='local_av' value='1' ".(($dados->local_av=='1')?"checked":"")." {$enabled}> Domicilio/Institui&ccedil;&atilde;o de Apoio</input><input type='radio' class='local_av' name='local_av' value='2' ".(($dados->local_av=='2')?"checked":"")." {$enabled}>Hospital</input></td></tr>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Motivo da hospitaliza&ccedil;&atilde;o</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='2' ><textarea cols=100 rows=5 {$acesso} name='motivo' class='OBG' >{$dados->motivo}</textarea>";
		//<input type='text' size=100 value='{$row['motivo']}' name='motivo' class='OBG' /></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3' >Problemas Ativos</th>";
		echo "</tr></thead>";
		if($acesso != "readonly") {
			echo "<tr><td colspan='3' ><input type='text' name='busca-problemas-ativos' {$acesso} size=20 /></td></tr>";
			echo"<tr><td colspan='3' ><input type='checkbox' id='outros-prob-ativos' name='outros-prob-ativos' {$acesso} />Outros.<span class='outro_ativo' ><input type='text' name='outro-prob-ativo' class='outro_ativo' {$acesso} size='20'/><button id='add-prob-ativo' tipo=''  >+ </button></span></td></tr>";
		}
		echo "<tr><td colspan='2' ><table id='problemas_ativos'>";
		if(!empty($dados->ativos)){
			foreach($dados->ativos as $a){
				echo "<tr><td>{$a}</td></tr>";
			}
		}

		echo "</table></td></tr>";


		echo "<thead><tr>";
		echo "<th colspan='3' >Comorbidades (S/N)</th>";
		echo "</tr></thead>";
		echo "<tr><td><input type='text' class='boleano OBG' name='cancer' value='{$dados->cancer}' {$acesso} /><b>Câncer</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='cardiopatia' value='{$dados->cardiopatia}' {$acesso} /><b>Cardiopatia</b></td>";
		echo "</tr><tr><td><input type='text' class='boleano OBG' name='psiquiatrico' value='{$dados->psiquiatrico}' {$acesso} /><b>Dist&uacute;rbio Psiqui&aacute;trico</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='dm' value='{$dados->dm}' {$acesso} /><b>DM</b></td>";
		echo "</tr><tr><td><input type='text' class='boleano OBG' name='neurologica' value='{$dados->neuro}' {$acesso} /><b>Doen&ccedil;a Neurol&oacute;gica</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='reumatologica' value='{$dados->reumatologica}' {$acesso} /><b>Doen&ccedil;a Reumatol&oacute;gica</b></td>";
		echo "</tr><tr><td><input type='text' class='boleano OBG' name='glaucoma' value='{$dados->glaucoma}' {$acesso} /><b>Glaucoma</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='has' value='{$dados->has}' {$acesso} /><b>HAS</b></td>";
		echo "</tr><tr><td><input type='text' class='boleano OBG' name='hepatopatia' value='{$dados->hepatopatia}' {$acesso} /><b>Hepatopatia</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='nefropatia' value='{$dados->nefropatia}' {$acesso} /><b>Nefropatia</b></td>";
		echo "</tr><tr><td><input type='text' class='boleano OBG' name='obesidade' value='{$dados->obesidade}' {$acesso} /><b>Obesidade</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='pneumopatia' value='{$dados->pneumopatia}' {$acesso} /><b>Pneumopatia</b></td>";
		echo "</tr><thead><tr>";
		echo "<th>Alergia medicamentosa (S/N) <input type='text' class='boleano OBG' name='palergiamed' value='{$dados->alergiamed}' {$acesso} /></th>";
		echo "<th colspan='2' >Alergia alimentar (S/N) <input type='text' class='boleano OBG' name='palergiaalim' value='{$dados->alergiaalim}' {$acesso} /></th>";
		echo "</tr></thead>";
		echo "<tr><td><input type='text' name='alergiamed' size=10 readonly='readonly' value='{$dados->tipoalergiamed}' /></td>";
		echo "<td><input type='text' name='alergiaalim' size=50 readonly='readonly' value='{$dados->tipoalergiaalim}' /></td></tr>";
		//echo"</table>";
		////////hospita///////


		if(isset($_GET['id'])){
			if($dados->modalidade==2 && $dados->local_av==2){
				echo "<tr><td colspan='3'><table id='score-id2' style='width:100% '>";
				$this->cond_pre_ihosp($dados,$enabled);
				echo "</table></td></tr>";
			}

		}else{
			echo "<tr><td colspan='3'><table id='score-id2' style='width:100% '>";
			$this->cond_pre_ihosp($dados,$enabled);
			echo "</table></td></tr>";
		}


		if(isset($_GET['id'])){
			if($dados->modalidade==2 && $dados->local_av==2){
				echo "<tr><td colspan='3'><table id='score-id1' style='width:100% '>";

				$this->cond_pre_id($dados,$enabled);
				$this->cont_inter_domiciliar($dados,$enabled);

				echo "</table></td></tr>";
			}
			if($dados->modalidade==2 && $dados->local_av==1){
				echo "<tr><td colspan='3'><table id='score-id1' style='width:100% '>";

				$this->cond_pre_id($dados,$enabled);
				$this->cont_inter_domiciliar($dados,$enabled);

				echo "</table></td></tr>";
			}
		}else{
			echo "<tr><td colspan='3'><table id='score-id1' style='width:100% '>";

			$this->cond_pre_id($dados,$enabled);
			$this->cont_inter_domiciliar($dados,$enabled);

			echo "</table></td></tr>";

		}

		echo "<thead><tr>";
		echo "<th colspan='3' >Interna&ccedil;&otilde;es dos &uacute;ltimos 12 meses</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='2' ><b>Qtd:</b><input type='text' size='2' class='OBG numerico' name='qtdanteriores' value='{$dados->qtdinternacoes}' {$acesso}/>";
		echo "<b>Motivos:</b><input type='text' name='historicointernacao' size=100 readonly='readonly' value='{$dados->historico}' {$acesso} /></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3' ><b>Parecer M&eacute;dico</b></th>";
		echo "</tr></thead>";
		echo"<tr><td colspan='3'><textarea cols=100 rows=2 type=text id='prejustparecer' name='pre_justificativa' readonly='readonly' size='100'>{$dados->pre_justificativa}</textarea></td></tr>";
		echo "<tr><td colspan='2' >";
		echo "<input type='radio' name='parecer' value='0' ".(($dados->parecer=='0')?"checked":"")." {$enabled} /> Favor&aacute;vel";
		echo "<input type='radio' name='parecer' value='1' ".(($dados->parecer=='1')?"checked":"")." {$enabled} /> Favor&aacute;vel com resalvas";
		echo "<input type='radio' name='parecer' value='2' ".(($dados->parecer=='2')?"checked":"")." {$enabled} /> Contr&aacute;rio";
		echo "<br/>Justificativa:<br/><textarea cols=100 rows=5 name='justparecer'  id='justparecer' class='OBG' {$acesso} >{$dados->justificativa}</textarea>";
		echo "</td></tr>";
		echo"<tr><td colspan='3'><b>Plano de Desmame:</b><br/><textarea cols=100 rows=2 type=text id='desmame' name='plano_desmame' class='OBG' {$acesso} size='100'>{$dados->plano_desmame}</textarea></td></tr>";

		/*if( $acesso != "readonly")
		 $this->equipamentos();
		else
			$this->equipamentos($dados->capmedica);
		*/
		echo"<tr><td colspan='2'><table width=100% >";
		if( $acesso != "readonly"){
			$this->equipamentos();
		}else{
			$this->equipamentos($dados->capmedica);

		}
		echo"</table></td></tr>";
		echo "</tbody></table></form></div>";
		 
		if($acesso != "readonly") {
			$this->salv_capmed($dados->pacienteid);
			//	echo "<button id='teste' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Teste</span></button></span>";
			echo "<button id='salvar-capmed' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
			echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";

		}
		//echo "<br/><button id='salvar-capmed'>Salvar</button>";
	}

	public function detalhes_capitacao_medica_editar($id){
		$id = anti_injection($id,"numerico");
		$sql = "SELECT cap.*, pac.nome as pnome,pac.idClientes,usr.nome as unome, DATE_FORMAT(cap.data,'%d/%m/%Y') as fdata, comob.*
		FROM capmedica as cap
		LEFT OUTER JOIN comorbidades as comob ON cap.id = comob.capmed
		LEFT OUTER JOIN problemasativos as prob ON cap.id = prob.capmed
		LEFT OUTER JOIN usuarios as usr ON cap.usuario = usr.idUsuarios
		LEFT OUTER JOIN clientes as pac ON cap.paciente = pac.idClientes

		WHERE cap.id = '{$id}'";
		$d = new DadosFicha();
		$d->capmedica = $id;
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result)){

			$d->pacienteid= $row['idClientes'];
			$d->usuario = ucwords(strtolower($row["unome"])); $d->data = $row['fdata']; $d->pacientenome = ucwords(strtolower($row["pnome"]));
			$d->motivo = $row['motivo'];
			$d->alergiamed = $row['alergiamed']; $d->alergiaalim = $row['alergiaalim'];
			$d->tipoalergiamed = $row['tipoalergiamed']; $d->tipoalergiaalim = $row['tipoalergiaalim'];
			$d->prelocohosp = $row['hosplocomocao']; $d->prehigihosp = $row['hosphigiene'];
			$d->preconshosp = $row['hospcons']; $d->prealimhosp = $row['hospalimentacao']; $d->preulcerahosp = $row['hospulcera'];
			$d->prelocodomic = $row['domlocomocao']; $d->prehigidomic = $row['domhigiene']; $d->preconsdomic = $row['domcons'];
			$d->prealimdomic = $row['domalimentacao']; $d->preulceradomic = $row['domulcera'];
			$d->objlocomocao = $row['objlocomocao']; $d->objhigiene = $row['objhigiene']; $d->objcons = $row['objcons'];
			$d->objalimento = $row['objalimentacao']; $d->objulcera = $row['objulcera'];
			$d->shosp = $d->prelocohosp + $d->prehigihosp +	$d->preconshosp + $d->prealimhosp + $d->preulcerahosp;
			$d->sdom = $d->prelocodomic + $d->prehigidomic +	$d->preconsdomic + $d->prealimdomic + $d->preulceradomic;;
			$d->sobj = $d->objlocomocao + $d->objhigiene + $d->objcons + $d->objalimento + $d->objulcera;

			$d->qtdinternacoes = $row['qtdinternacoes']; $d->historico = $row['historicointernacao'];
			$d->parecer = $row['parecer']; $d->justificativa = $row['justificativa'];

			$d->cancer = $row['cancer']; $d->psiquiatrico = $row['psiquiatrico']; $d->neuro = $row['neuro']; $d->glaucoma = $row['glaucoma'];
			$d->hepatopatia = $row['hepatopatia']; $d->obesidade = $row['obesidade']; $d->cardiopatia = $row['cardiopatia']; $d->dm = $row['dm'];
			$d->reumatologica = $row['reumatologica']; $d->has = $row['has']; $d->nefropatia = $row['nefropatia']; $d->pneumopatia = $row['pneumopatia'];
			$d->modalidade = $row['MODALIDADE']; $d->local_av = $row['LOCAL_AV']; $d->plano_desmame = $row['PLANO_DESMAME']; $d->pre_justificativa = $row['PRE_JUSTIFICATIVA'];
		}
		$sql = "SELECT p.DESCRICAO as nome , p.cid FROM problemasativos as p WHERE p.capmed = '{$id}' ";
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result)){
			 
			$elemento[$row['cid']]=array("1"=>$row['cid'],"2"=>$row['nome']);
		}

		$sql = "SELECT p.DESCRICAO as nome, p.cid as codigo FROM problemasativos as p where p.capmed = '{$id}' ";
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result)){
			if(array_key_exists($row['codigo'], $elemento)){

				$d->ativos[] = $elemento[$row['codigo']];

			}

		}

		$sql2 = "SELECT sc_pac.OBSERVACAO,sc_pac.ID_PACIENTE, sc_item.DESCRICAO ,sc_item.PESO ,sc_item.GRUPO_ID ,
		sc_item.ID ,sc.id as tbplano,cap.justificativa,DATE(sc_pac.DATA) as DATA,
		grup.DESCRICAO as grdesc  FROM score_paciente sc_pac
		LEFT OUTER JOIN score_item sc_item ON sc_pac.ID_ITEM_SCORE = sc_item.ID
		LEFT OUTER JOIN capmedica cap ON sc_pac.ID_CAPMEDICA = cap.id
		LEFT OUTER JOIN grupos grup ON sc_item.GRUPO_ID = grup.ID
		LEFT OUTER JOIN score sc ON sc_item.SCORE_ID = sc.ID
		WHERE sc_pac.ID_CAPMEDICA = '{$id}'";

		$result2 = mysql_query($sql2);

		while($row2 = mysql_fetch_array($result2)){
			if( $row2['ID'] == 71){
				$d->diag_principal=$row2['OBSERVACAO'];
			}
			if( $row2['ID'] == 69){
				$d->diag_principal=$row2['OBSERVACAO'];
			}

			if( $row2['ID'] == 72){
				$d->diag_secundario=$row2['OBSERVACAO'];
			}
			if( $row2['ID'] == 70){
				$d->diag_secundario=$row2['OBSERVACAO'];
			}
			if( $row2['ID'] == 66){
				$d->atend_24=$row2['OBSERVACAO'];
			}
			if( $row2['ID'] == 62){
				$d->atend_24=$row2['OBSERVACAO'];
			}
			if( $row2['ID'] == 63){
				$d->atend_24_2=$row2['OBSERVACAO'];
			}
			if( $row2['ID'] == 67){
				$d->atend_12=$row2['OBSERVACAO'];
			}
			if( $row2['ID'] == 64){
				$d->atend_12=$row2['OBSERVACAO'];
			}
			if( $row2['ID'] == 65){
				$d->atend_06=$row2['OBSERVACAO'];
			}
			if( $row2['ID'] == 68){
				$d->atend_06=$row2['OBSERVACAO'];
			}
		}
		//$this->cabecalho(pacienteid);
		$this->editar_capitacao_medica($d);
		// $this->score_abmid($d);
		// echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";



	}
	 
	public function editar_capitacao_medica($dados,$acesso){
		$enabled = "";
		 
		$id_presc= '-1';
		$id_presc_sol='';
		 
		$sql="Select Max(id) from capmedica where paciente ='{$dados->pacienteid }'";
		$result=mysql_query($sql);
		$maior_cap = mysql_result($result,0,0);
		 
		$sql3="select DATE_FORMAT(data,'%Y-%m-%d')  from capmedica where id = '{$maior_cap}' ";
		$r2=mysql_query($sql3);
		$data_cap= mysql_result($r2,0);
		//$datalimit = date('Y/m/d', strtotime("+2 days",strtotime($data_cap)));
		$datalimit = date('Y-m-d H:i:s', strtotime("+24 Hours",strtotime($data_cap)));
		 
		 
		$sql2="SELECT pre.id, sol.idPrescricao from prescricoes as pre LEFT JOIN solicitacoes as sol ON(pre.id=sol.idPrescricao) WHERE pre.ID_CAPMEDICA = '{$maior_cap}'";
		$result=mysql_query($sql2);
		while($row2 = mysql_fetch_array($result)){
			if($row2['id'] > 0){
				$id_presc= $row2['id'];
				$id_presc_sol= $row2['idPrescricao'];
			}
		}
		if($id_presc == $id_presc_sol || $dados->capmedica < $maior_cap){
			echo "<center><h1>Ficha m&eacute;dica do paciente</h1></center>";
		}
		else {
			echo "<center><h1>Editar ficha m&eacute;dica do paciente</h1></center>";
		}
		$this->cabecalho($dados);
		echo "<center><h2>Respons&aacute;vel: {$dados->usuario} - Data: {$dados->data}</h2></center>";
		$this->plan;
		echo "<div id='div-ficha-medica' ><br/>";

		echo alerta();
		echo "<form>";
		echo "<input type='hidden' name='paciente'  value='{$dados->pacienteid }' />";
		echo "<input type='hidden' name='capmedicaid'  value='{$dados->capmedica }' />";
		echo "<br/><table class='mytable' style='width:100%'>";

		if( $acesso != "readonly"){
			if($this->plan == 7 || $this->plan == 27 || $this->plan == 28 || $this->plan == 29){
				 
				$this->score_petrobras($dados);
			}
			else $this->score_abmid($dados);
			 
			 
		}

		echo "<thead><tr>";
		echo "<th colspan='3' >Ficha de avalia&ccedil;&atilde;o Mederi.</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='2'><b>Modalidade de Home Care:</b><input type='radio' class='modalidade' name='modalidade' value='1' ".(($dados->modalidade=='1')?"checked":"")." {$enabled}>Assistencia Domiciliar</input><input type='radio' class='modalidade' name='modalidade' value='2' ".(($dados->modalidade=='2')?"checked":"")." {$enabled}>Interna&ccedil;&atilde;o Domiciliar</input></td></tr>";
		echo "<tr><td colspan='2'><b>Local de Avalia&ccedil;&atilde;o do paciente:</b><input type='radio' class='local_av'  name='local_av' value='1' ".(($dados->local_av=='1')?"checked":"")." {$enabled}> Domicilio/Institui&ccedil;&atilde;o de Apoio</input><input type='radio' class='local_av' name='local_av' value='2' ".(($dados->local_av=='2')?"checked":"")." {$enabled}>Hospital</input></td></tr>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Motivo da hospitaliza&ccedil;&atilde;o</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='2' ><textarea cols=100 rows=5 {$acesso} name='motivo' class='OBG' >{$dados->motivo}</textarea>";
		//<input type='text' size=100 value='{$row['motivo']}' name='motivo' class='OBG' /></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3' >Problemas Ativos</th>";
		echo "</tr></thead>";

		if($acesso != "readonly") {
			echo "<tr><td colspan='2' ><input type='text' name='busca-problemas-ativos' {$acesso} size=100 /></td></tr>";
			echo"<tr><td colspan='2' ><input type='checkbox' id='outros-prob-ativos' name='outros-prob-ativos' {$acesso} />Outros.<span class='outro_ativo' ><input type='text' name='outro-prob-ativo' class='outro_ativo' {$acesso} size=75/><button id='add-prob-ativo' tipo=''  >+ </button></span></td></tr>";
		}
		echo "<tr><td colspan='2' ><table id='problemas_ativos'>";
		if(!empty($dados->ativos)){
			foreach($dados->ativos as $a){
				$x=$a[1];
				$y=$a[2];
				echo "<tr><td  class='prob-ativos' cod={$x} desc='{$y}'><img src='../utils/delete_16x16.png' class='del-prob-ativos' title='Excluir' border='0' >{$y}</td></tr>";
			}
		}
		 
		echo "</table></td></tr>";


		echo "<thead><tr>";
		echo "<th colspan='3' >Comorbidades (S/N)</th>";
		echo "</tr></thead>";
		echo "<tr><td><input type='text' class='boleano OBG' name='cancer' value='{$dados->cancer}' {$acesso} /><b>Câncer</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='cardiopatia' value='{$dados->cardiopatia}' {$acesso} /><b>Cardiopatia</b></td>";
		echo "</tr><tr><td><input type='text' class='boleano OBG' name='psiquiatrico' value='{$dados->psiquiatrico}' {$acesso} /><b>Dist&uacute;rbio Psiqui&aacute;trico</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='dm' value='{$dados->dm}' {$acesso} /><b>DM</b></td>";
		echo "</tr><tr><td><input type='text' class='boleano OBG' name='neurologica' value='{$dados->neuro}' {$acesso} /><b>Doen&ccedil;a Neurol&oacute;gica</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='reumatologica' value='{$dados->reumatologica}' {$acesso} /><b>Doen&ccedil;a Reumatol&oacute;gica</b></td>";
		echo "</tr><tr><td><input type='text' class='boleano OBG' name='glaucoma' value='{$dados->glaucoma}' {$acesso} /><b>Glaucoma</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='has' value='{$dados->has}' {$acesso} /><b>HAS</b></td>";
		echo "</tr><tr><td><input type='text' class='boleano OBG' name='hepatopatia' value='{$dados->hepatopatia}' {$acesso} /><b>Hepatopatia</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='nefropatia' value='{$dados->nefropatia}' {$acesso} /><b>Nefropatia</b></td>";
		echo "</tr><tr><td><input type='text' class='boleano OBG' name='obesidade' value='{$dados->obesidade}' {$acesso} /><b>Obesidade</b></td>";
		echo "<td><input type='text' class='boleano OBG' name='pneumopatia' value='{$dados->pneumopatia}' {$acesso} /><b>Pneumopatia</b></td>";
		echo "</tr><thead><tr>";
		echo "<th>Alergia medicamentosa (S/N) <input type='text' class='boleano OBG' name='palergiamed' value='{$dados->alergiamed}' {$acesso} /></th>";
		echo "<th colspan='2' >Alergia alimentar (S/N) <input type='text' class='boleano OBG' name='palergiaalim' value='{$dados->alergiaalim}' {$acesso} /></th>";
		echo "</tr></thead>";
		echo "<tr><td><input type='text' name='alergiamed' size=50 readonly='readonly' value='{$dados->tipoalergiamed}' /></td>";
		echo "<td><input type='text' name='alergiaalim' size=50 readonly='readonly' value='{$dados->tipoalergiaalim}' /></td></tr>";
		//echo"</table>";
		////////hospita///////


		if(isset($_GET['id'])){
			if($dados->modalidade==2 && $dados->local_av==2){
				echo "<tr><td colspan='3'><table id='score-id2' style='width:100% '>";
				$this->cond_pre_ihosp($dados,$enabled);
				echo "</table></td></tr>";
			}
			 
		}else{
			echo "<tr><td colspan='3'><table id='score-id2' style='width:100% '>";
			$this->cond_pre_ihosp($dados,$enabled);
			echo "</table></td></tr>";
		}


		if(isset($_GET['id'])){
			if($dados->modalidade==2 && $dados->local_av==2){
				echo "<tr><td colspan='3'><table id='score-id1' style='width:100% '>";

				$this->cond_pre_id($dados,$enabled);
				$this->cont_inter_domiciliar($dados,$enabled);
				 
				echo "</table></td></tr>";
			}
			if($dados->modalidade==2 && $dados->local_av==1){
				echo "<tr><td colspan='3'><table id='score-id1' style='width:100% '>";

				$this->cond_pre_id($dados,$enabled);
				$this->cont_inter_domiciliar($dados,$enabled);

				echo "</table></td></tr>";
			}
		}else{
			echo "<tr><td colspan='3'><table id='score-id1' style='width:100% '>";

			$this->cond_pre_id($dados,$enabled);
			$this->cont_inter_domiciliar($dados,$enabled);

			echo "</table></td></tr>";

		}
		 
		echo "<thead><tr>";
		echo "<th colspan='3' >Interna&ccedil;&otilde;es dos &uacute;ltimos 12 meses</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='2' ><b>Qtd:</b><input type='text' size='2' class='OBG numerico' name='qtdanteriores' value='{$dados->qtdinternacoes}' {$acesso}/>";
		echo "<b>Motivos:</b><input type='text' name='historicointernacao' size=100 readonly='readonly' value='{$dados->historico}' {$acesso} /></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3' >Parecer M&eacute;dico</th>";
		echo "</tr></thead>";
		echo"<tr><td colspan='3'><textarea cols=100 rows=5 readonly type=text name ='pre_justificativa' id='prejustparecer'>{$dados->pre_justificativa}</textarea></td></tr>";
		echo "<tr><td colspan='2' >";
		echo "<input type='radio' name='parecer' value='0' ".(($dados->parecer=='0')?"checked":"")." {$enabled} /> Favor&aacute;vel";
		echo "<input type='radio' name='parecer' value='1' ".(($dados->parecer=='1')?"checked":"")." {$enabled} /> Favor&aacute;vel com resalvas";
		echo "<input type='radio' name='parecer' value='2' ".(($dados->parecer=='2')?"checked":"")." {$enabled} /> Contr&aacute;rio";
		echo "<br/><b>Justificativa:</b><br/><textarea cols=100 rows=5 name='justparecer'  id='justparecer' class='OBG' {$acesso} >{$dados->justificativa}</textarea>";
		echo "</td></tr>";
		echo"<tr><td colspan='3'><b>Plano de Desmame:</b><br/><textarea cols=100 rows=2 type=text id='desmame' class='OBG' name='plano_desmame' {$acesso} size='100'>{$dados->plano_desmame}</textarea></td></tr>";

		echo"<tr><td colspan='2'><table width=100% >";
		$this->equipamentos_editar($dados->capmedica);
		echo"</table></td></tr>";
		echo "</tbody></table></form></div>";
		if($acesso != "readonly") {
			$this->salv_capmed($dados->pacienteid);
			echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
			if($id_presc == $id_presc_sol || $dados->capmedica < $maior_cap || $datalimit <= date('Y-m-d H:i:s')){
				echo "<button id='salvar-capmed' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
			}else{
				echo "<button id='editar_ficha' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";

			}
			 
			 
		}
		 
	}

	private function periodo(){

		echo "<br/><fieldset style='width:340px;text-align:center;'><legend><b>Per&iacute;odo</b></legend>";
		echo "DE".
				"<input type='text' name='inicio' value='' maxlength='10' size='10'  class='OBG data periodo' />".
				" AT&Eacute; <input type='text' name='fim' value='' maxlength='10' size='10'  class='OBG data' /></fieldset>";
	}


	public function listar_capitacao_medica($id){
		 
		$id = anti_injection($id,"numerico");
		$sql3="select Max(id) as cap from capmedica where paciente = '{$id}' ";
		$r2=mysql_query($sql3);
		$maior_cap= mysql_result($r2,0);


		$sql3="select DATE_FORMAT(data,'%y-%m-%d')  from capmedica where id = '{$maior_cap}' ";
		$r2=mysql_query($sql3);
		$data_cap= mysql_result($r2,0);

		$sql2 = "SELECT pre.id, sol.idPrescricao,
		DATE_FORMAT(pre.data,'%d/%m/%Y - %T') AS sdata,
		DATE_FORMAT(pre.inicio,'%d/%m/%Y') AS inicio,
		DATE_FORMAT(pre.fim,'%d/%m/%Y') AS fim, pre.criador,pre.paciente from prescricoes as pre LEFT JOIN solicitacoes as sol ON(pre.id=sol.idPrescricao) WHERE pre.ID_CAPMEDICA = {$maior_cap}";
		$result = mysql_query($sql2);
		$solicitacao = @mysql_result($result,0,1);
		$maior_presc = @mysql_result($result,0,0);
		$inicio = @mysql_result($result,0,3);
		$fim = @mysql_result($result,0,4);
		$data = @mysql_result($result,0,2);
		$criador = @mysql_result($result,0,5);
		$paciente = @mysql_result($result,0,6);


		//$datalimit = date('Y/m/d', strtotime("+2 days",strtotime($data_cap)));
		$datalimit = date('Y-m-d H:i:s', strtotime("+24 Hours",strtotime($data_cap)));


		$sql = "SELECT
		DATE_FORMAT(c.data,'%d/%m/%Y %H:%i:%s') as fdata,
		c.id,
		c.paciente as idpaciente,
		u.nome,
		presc.id as prescid,
		u.idUsuarios,
		(Select count(*) from prescricoes P1 where P1.ID_CAPMEDICA =c.id) as controle,
		p.nome as paciente
		FROM
		capmedica as c LEFT JOIN
		usuarios as u ON (c.usuario = u.idUsuarios) LEFT JOIN
		clientes as p ON (c.paciente = p.idClientes) LEFT JOIN
		prescricoes as presc ON (c.id = presc.ID_CAPMEDICA AND carater = 4 AND  presc.STATUS=0) 
		 
		WHERE
		c.paciente = '{$id}' 
		ORDER BY
		c.data DESC";
		$r = mysql_query($sql);
		$flag = true;
		while($row = mysql_fetch_array($r)){
			if($flag) {
					
				$p = ucwords(strtolower($row["c.paciente"]));
				$this->cabecalho($id);
				//echo "<center><h2>{$p}</h2></center>";
					
				echo "<br/><br/>";
					
				echo "<div><input type='hidden' name='nova_ficha' value='{$id}'></input>";
				$this->salv_capmed($row['idpaciente']);
				$this->editar_prescricao($paciente);
				$this->nova_presc($id);
				$this->presc_aditiva($id);
					
				if($_SESSION['adm_user'] == 1 || $_SESSION['modmed'] == 1 ){
					echo "<button id='nova_ficha'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
							<span class='ui-button-text'>Nova ficha</span></button>";
				}
				echo"</div>";
				echo "<br/><table class='mytable' style='width:100%' >";
				echo "<thead><tr>";
				echo "<th width='55%'>Usu&aacute;rio</th>";
				echo "<th>Data</th>";
				echo "<th width='15%'>Detalhes</th>";
				echo "<th>Op&ccedil;&otilde;es</th>";
				echo "</tr></thead>";
				$flag = false;
			}
			$id_presc_sol='';
			$id_presc_sol=-1;
			$teste1 =$row['idUsuarios'];
			$teste2 = $_SESSION['id_user'];
			 
			$data_atual = date('Y-m-d H:i:s');
			
			$u = ucwords(strtolower($row["nome"]));

			echo "<tr><td>{$u}</td><td>{$row['fdata']}</td>";
			echo "<td><a href=?adm=paciente&act=capmed&id={$row['id']}><img src='../utils/capmed_16x16.png' title='Detalhes Ficha' border='0'></a>";
			///mudou ak
			if($row['id'] == $maior_cap){
					
				if($row['controle'] == 0 &&  $datalimit > $data_atual){
					echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=?adm=paciente&act=detalhescore&id={$row['id']}><img src='../utils/details_16x16.png' title='Detalhes Score' border='0'></a>";
					/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=?adm=paciente&act=detalhesprescricao&id={$row['id']}&idpac={$row['idpaciente']}><img src='../utils/fichas_24x24.png' width='16' title='Prescri&ccedil;&atilde;o Pendente' ></a></td>";*/
					if($_SESSION['adm_user'] == 1 || $_SESSION['modmed'] == 1 ){
						echo"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' class='presc_avaliacao' val={$row['id']}><img src='../utils/alerta_prescricao.png' width='20' title='Prescri&ccedil;&atilde;o de Avalia&ccedil&atilde;o Pendente' ></a>";
					}
					echo"</td>";
				}
				else{
					echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=?adm=paciente&act=detalhescore&id={$row['id']}><img src='../utils/details_16x16.png' title='Detalhes Score' border='0'></a>";
					if($row['prescid'] > 0){
						echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=?adm=paciente&act=detalhesprescricao&id={$row['id']}&idpac={$row['idpaciente']}><img src='../utils/fichas_24x24.png' width='16' title='Detalhes Prescri&ccedil;&atilde;o' border='0'></a>";
					}
					 
					echo "</td>";
				}
					
				echo"<td>";
					
				if(($solicitacao == $maior_presc && $row['controle'] != 0) || $datalimit < $data_atual){
					if($_SESSION['adm_user'] == 1 || $_SESSION['modmed'] == 1 ){
						echo"<a href=?adm=paciente&act=anterior_capmed&id={$row['id']}><img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para nova avalia&ccedil;&atilde;o.' border='0'>";
						if($datalimit < $data_atual){
							echo"<a href=#><img src='../utils/prescricao_aditiva_24x24.png' width='16' title=' Prescri&ccedil;&atilde;o  Aditiva de Avalia&ccedil;&atilde;o.'  id='presc_adt_avaliacao' paciente='{$id}' data='{$data}'   id_cap='{$maior_cap}'  inicio='{$inicio}' fim='{$fim}'  border='0'>";
						}
					}
				}
				else{
					if(($_SESSION['adm_user'] == 1 || $row['idUsuarios'] == $_SESSION['id_user']) && $datalimit >= $data_atual){
						echo"<a href=?adm=paciente&act=editar_capmed&id={$row['id']}><img src='../utils/edit_16x16.png' title='Editar Ficha' border='0'>";
						if( $row['controle'] != 0 ){
							echo  "<a href=#><img src='../utils/editar_prescricao_24x24.png' width='20' title='Editar Prescri&ccedil;&atilde;o' border='0' class='editar_prescricao'  data='{$data}' paciente='{$id}' antiga='{$maior_presc}' data='{$data}' tipo=4 criador='{$criador}' id_cap='{$maior_cap}' inicio='{$inicio}' fim='{$fim}' busca='true'></a>";
						}
					}
				}
			}else{
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=?adm=paciente&act=detalhescore&id={$row['id']}><img src='../utils/details_16x16.png' title='Detalhes Score' border='0'></a>";
				if($row['prescid'] > 0){
					echo"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=?adm=paciente&act=detalhesprescricao&id={$row['id']}&idpac={$row['idpaciente']}><img src='../utils/fichas_24x24.png' width='16' title='Detalhes Prescri&ccedil;&atilde;o' border='0'></a>";
				}
				echo"</td>";
				echo"<td>";
				if($_SESSION['adm_user'] == 1 || $_SESSION['modmed'] == 1 ){
					if($datalimit < $data_atual || $solicitacao == $maior_presc){
						echo"<a href=?adm=paciente&act=anterior_capmed&id={$row['id']}><img src='../utils/nova_avaliacao_24x24.png' width='16' title='Usar para nova avalia&ccedil;&atilde;o.' border='0'>";
					}
					if(($row['controle'] != 0 || $datalimit < $data_atual ) && $maior_presc == 0  ){
						echo  "<a href=#><img src='../utils/prescricao_16x16.png' width='16' title='Usar para nova Prescri&ccedil;&atilde;o' border='0' class='nova_presc_avaliacao'  data='{$data}' paciente='{$id}'  antiga={$row['prescid']}  tipo=4  id_cap='{$maior_cap}'  inicio='{$inicio}' fim='{$fim}'  busca='true'></a>";
					}



				}
					
					
				echo"</td>";
					
			}



			echo "</td></tr>";
		}

		if($flag){
			if($_SESSION['adm_user'] == 1 || $_SESSION['modmed'] == 1 ){
				$this->nova_capitacao_medica($id);
			}else{
				echo "N&atilde;o possui Ficha de Avalia&ccedil;&atilde;o Cadastrada.";
			}

			return;
		}

		 
		echo "</table>";
	}


	///////////////////////////FICHA EVOLUCAO/////////////////////////////
	public function nova_evolucao_medica($id){

		$id = anti_injection($id,"numerico");
		$result = mysql_query("SELECT p.nome FROM clientes as p WHERE p.idClientes = '{$id}'");
		$ev = new FichaEvolucao();
		while($row = mysql_fetch_array($result)){
			$ev->pacienteid = $id;
			$ev->pacientenome = ucwords(strtolower($row["nome"]));
		}
		$this->evolucao_medica($ev,"");
	}

	/////////////////////////////////////

	public function evolucao_medica($ev,$acesso){

		echo "<div id='dialog-validar-radio' title='Validar Campos'>";
		echo "<center><h3>Algum item n&atilde;o foi selecionado.</h3></center>";
		 
		//      echo "<form action='imprimir.php' method='post'><input type='hidden' value='-1' name='prescricao'><center><button>Imprimir</button></center></form>";
		 
		echo "</div>";

		 
		echo "<center><h1>Ficha de Evolu&ccedil;&atilde;o M&eacute;dica </h1></center>";
		//echo "<center><h2>{$ev->pacientenome}</h2></center>";
		$this->cabecalho($ev);


		if($acesso == "readonly"){

			echo "<center><h2>Respons&aacute;vel: {$ev->usuario} - Data: {$ev->data}</h2></center>";
			//echo "<a href=imprimir_ficha.php?id={$ev->capmedica}>imprimir</a></br>";
			echo"<form method=post target='_blank' action='imprimir_ficha.php?id={$ev->capmedica}'>";
			echo"<input type=hidden value={$ev->capmedica} class='imprimir_ficha_mederi'</input>";
			echo "<button id='imprimir_ficha_mederi' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
					<span class='ui-button-text'>Imprimir</span></button>";
			echo "</form>";
		}

		$this->plan;


		echo "<div id='div-ficha-evolucao-medica'><br/>";

		echo alerta();
		echo "<form>";
		echo "<input type='hidden' name='paciente'  value='{$ev->pacienteid}' />";
		echo "<br/>";
		echo "<table class='mytable' style='width:95%;'>";

		$sql="select  Max(ev.ID) from fichamedicaevolucao as ev where ev.PACIENTE_ID = {$ev->pacienteid}";
		$result=mysql_query($sql);
		$x= mysql_result($result,0);

		$sql="select Max(id) from capmedica where paciente = {$ev->pacienteid}";
		$result=$result=mysql_query($sql);
		$y= mysql_result($result,0);

		echo "<thead><tr>";
		echo "<th colspan='3' >Problemas Ativos</th>";
		echo "</tr></thead>";
		if($x == ''){
			 
			$sql2 = "SELECT p.DESCRICAO as nome , p.cid FROM problemasativos as p WHERE p.capmed = '{$y}' ";
			$result2 = mysql_query($sql2);
			$cont=1;
			 
			while($row = mysql_fetch_array($result2)){
				 
				echo "<tr><td cid={$row['cid']} prob_atv='{$row['nome']}' x='{$cont}' class='probativo1' style='width:20%'><b>P{$cont} - {$row['nome']} </b></td>
				<td colspan='2'><input type=radio class='probativo' name='P{$cont}' value =1 >Resolvido ";
				echo"<input type=radio name='P{$cont}' class='probativo' value =2 >Melhor 
				<input type=radio class='probativo' name='P{$cont}' value =3 >Est&aacute;vel 
				<input class='probativo' type=radio name='P{$cont}' value =4 >Pior</td></tr>";
				echo"<tr><td colspan='3'><span class='obs_probativo' name='P{$cont}'>
				<textarea cols=100 rows=2 class='obs{$cont}'    {$acesso} >{$ev->justificativa}</textarea></span></td></tr>";
				$cont++;
			}
		}else{
			$sql2="select pae.DESCRICAO as nome, pae.CID_ID,pae.STATUS from problemaativoevolucao as pae where pae.EVOLUCAO_ID = {$x} ";
			$result2 = mysql_query($sql2);
			$cont=1;
			 
			while($row = mysql_fetch_array($result2)){
				if($row['STATUS'] != 1){
					echo "<tr><td cid='{$row['CID_ID']}' prob_atv='{$row['nome']}' x='{$cont}' class='probativo1' style='width:20%'><b>P{$cont} - {$row['nome']}</b> </td><td colspan='2'><input type=radio class='probativo' name='P{$cont}' value =1 >Resolvido ";
					echo"<input type=radio name='P{$cont}' class='probativo' value =2 >Melhor <input type=radio class='probativo' name='P{$cont}' value =3 >Est&aacute;vel <input class='probativo' type=radio name='P{$cont}' value =4 >Pior</td></tr>";
					echo"<tr><td colspan='3'><span class='obs_probativo' name='P{$cont}'><textarea cols=100 rows=2 class='obs{$cont}'    {$acesso} >{$ev->justificativa}</textarea></span></td></tr>";
					$cont++;
				}
			}
			 
		}

		echo "<input type='hidden' name='capmed'  value='{$y}' />";
		echo "<thead><tr>";
		echo "<th colspan='3' >Novos Problemas </th>";
		echo "</tr></thead>";
		if($acesso != "readonly") {
			echo "<tr><td colspan='3' ><input type='text' name='busca-problemas-ativos-evolucao' {$acesso} size=100 /></td></tr>";
			echo"<tr><td colspan='3' ><input type='checkbox' id='outros-prob-ativos-evolucao' name='outros-prob-ativos-evolucao' {$acesso} />Outros.<span class='outro_ativo_evolucao' ><input type='text' name='outro-prob-ativo-evolucao' class='outro_ativo_evolucao' {$acesso} size=75/><button id='add-prob-ativo-evolucao' tipo=''  >+ </button></span></td></tr>";
		}
		echo "<tr><td colspan='3' ><table id='problemas_ativos' style='width:100%;'>";
		if(!empty($ev->ativos)){
			foreach($ev->ativos as $a){
				echo "<tr><td>{$a}</td></tr>";
			}
		}
		
		//Intercorrências
		
		echo"<tr><td colspan='3' ><b>Intercorrências: </b>
		<select name=intercorrencias>
			<option value =''>  </option>
			<option value ='n'>N&atilde;o</option>
			<option value ='s'>Sim</option><span name='intercorrencias' id='intercorrencias_span'>
		</select></td>
		<td>
		&nbsp;&nbsp;<span name='intercorrencias' id='intercorrencias_span'>
		<b>Descrição:</b>
		<input type='text' name='intercorrencias' class='intercorrencias' size=50/>
		<button id='add-intercorrencias' tipo=''  > + </button></span></td></tr>";

		echo "</table></td></tr>";
		
		echo "<thead id='intercorrencias_tabela'></thead>";
		
		echo "<thead><tr>";
		echo "<th colspan='3' >Evolu&ccedil;&atilde;o dos sinais vitais (Registro da semana)</th>";
		echo "</tr></thead>";
		echo "<tr><td><b>PA (sist&oacute;lica) </b></td><td>min:<input type='texto' class='numerico1 OBG' name='pa_sistolica_min' value='{$ev->pa_sistolica_min}' {$acesso} size='4' /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='pa_sistolica_max' value='{$ev->pa_sistolica_max}' {$acesso} size='4' /></td>";
		echo "<td><b>ALERTA:</b>  <input type='radio'  name='pa_sistolica_sinal' value='1' {$acesso} />Amarelo <input type='radio' class='radio' name='pa_sistolica_sinal' value='2' {$acesso} size='4' />VERMELHO</td>";
		echo "</tr>";
		echo "<tr><td><b>PA (diast&oacute;lica) </b> </td><td> min:<input type='texto' class='numerico1 OBG' name='pa_diastolica_min' value='{$ev->pa_diastolica_min}' {$acesso} size='4'  /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='pa_diastolica_max' value='{$ev->pa_diastolica_max}' size='4' {$acesso} /></td>";
		echo "<td><b>ALERTA:</b>  <input type='radio'  name='pa_diaistolica_sinal' value='1' {$acesso} />Amarelo <input type='radio' name='pa_diastolica_sinal' value='2' {$acesso} size='4' />VERMELHO</td>";
		echo "</tr>";
		echo "<tr><td><b>FC </b> </td><td>min:<input type='texto' class='numerico1 OBG' name='fc_min' value='{$ev->fc_min}'  size='4' {$acesso} /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='fc_max' value='{$ev->fc_max}' {$acesso} size='4' /></td>";
		echo "<td><b>ALERTA: </b> <input type='radio'  name='fc_sinal' value='1' {$acesso} />Amarelo <input type='radio' name='fc_sinal' value='2' {$acesso} size='4' />VERMELHO</td>";
		echo "</tr>";
		echo "<tr><td><b>Fr </b> </td><td>min:<input type='texto' class='numerico1 OBG' name='fr_min' value='{$ev->fr_min}' size='4' {$acesso} /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='fr_max' value='{$ev->fr_max}' size='4' {$acesso}  /></td>";
		echo "<td><b>ALERTA:</b>  <input type='radio'  name='fr_sinal' value='1' {$acesso} />Amarelo <input type='radio' name='fr_sinal' value='2' {$acesso} size='4' />VERMELHO</td>";
		echo "</tr>";
		echo "<tr><td width=20% ><b>Temperatura: </b></td><td width=25% >
		min:
		<input type='texto' class='numerico1 OBG' name='temperatura_min' size='4' value='{$ev->temperatura_min}' {$acesso} size='4' /> 
		m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='temperatura_max' size='4'value='{$ev->temperatura_max}' {$acesso} size='4' /></td>";
		echo "<td width=25%  ><b>ALERTA: </b> <input type='radio'  name='temperatura_sinal' value='1' {$acesso} />Amarelo <input type='radio' name='temperatura_sinal' value='2' {$acesso} size='4' />VERMELHO</td>";
		echo "</tr>";

		//Oximetria de Pulso
		echo "<tr><td width=20% ><b>Oximetria de Pulso: </b></td>
		<td width=25% colspan='3'>
			min:
			<input type='texto' class='numerico1' name='oximetria_pulso_min' size='4' value='{$ev->oximetria_pulso_min}' {$acesso} size='4' />
			<select name='tipo_oximetria_min' class='COMBO_-'>
				<option value=''></option>
				<option value='1'>Ar Ambiente</option>
				<option value='2'>O2 Suplementar</option>
			</select>
			<span id='suplementar_min_span' name='suplementar' style='display: inline;'>
				<input type='texto' size='3' class='numerico1' name='litros_min'/>l/min
				<b>Via
				<select name='via_oximetria_min' class='COMBO_-' >
					<option value=''></option>
					<option value='1'>Cateter Nasal</option>
					<option value='2'>Cateter em Traqueostomia</option>
					<option value='3'>Máscara de Venturi</option>
					<option value='4'>Ventilação Mecânica</option>
				</select>
			</span>			
		</td></tr>";
		echo "<tr><td width=20%></td>
		<td width=25% colspan='3'>
			m&aacute;x:
			<input type='texto' class='numerico1' name='oximetria_pulso_max' value='{$ev->oximetria_pulso_max}' {$acesso} size='4' />
			<select name='tipo_oximetria_max' class='COMBO_-'>
				<option value=''></option>
				<option value='1'>Ar Ambiente</option>
				<option value='2'>O2 Suplementar</option>
			</select>
			<span id='suplementar_max_span' name='suplementar' style='display: inline;'>
				<input type='texto' size='3' class='numerico1' name='litros_max'/>l/min
				<b>Via
				<select name='via_oximetria_max' class='COMBO_-' >
					<option value=''></option>
					<option value='1'>Cateter Nasal</option>
					<option value='2'>Cateter em Traqueostomia</option>
					<option value='3'>Máscara de Venturi</option>
					<option value='4'>Ventilação Mecânica</option>
				</select>
			</span>			
		</td></tr>";

		echo "<thead><tr>";
		echo "<th colspan='3'>Exame F&iacute;sico</th>";

		echo "<tr><td><b>Estado Geral:</b>  </td><td colspan='2'>
				<input type='radio' name='estd_geral' size=50 readonly='readonly' value='1'  />Bom &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type='radio' name='estd_geral' size=50 readonly='readonly' value='2'  />Regular &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type='radio' name='estd_geral' size=50 readonly='readonly' value='3' />Ruim
				</td></tr>";

		echo "<tr><td><b>Mucosa:</b>  </td><td colspan='2'>
				<input type='radio' name='mucosa' class='mucosa' size=50 readonly='readonly' value='s' />Coradas &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type='radio' name='mucosa' size=50 readonly='readonly' value='n' ".(($dados->mucosa=='n')?"checked":"")." {$enabled} />Descoradas
				<span class='mucosa1'><select name='mucosaobs' class='COMBO_-' style='background-color:transparent;'>
				<option value=''></option><option value='+' ".(($ev->mucosa_obs=='+')?"selected":"")." >+</option><option value='++' ".(($ev->mucosa_obs=='++')?"selected":"")." >++</option><option value='+++' ".(($ev->mucosa_obs=='+++')?"selected":"").">+++</option><option value='++++'".(($ev->mucosa_obs=='++++')?"selected":"").">++++</option></select></span></td></tr>";
		echo "<tr><td><b>Escleras:</b>  </td><td colspan='2'><input type='radio' name='escleras' class='escleras' size=50 readonly='readonly' value='s' ".(($ev->escleras=='s')?"checked":"")." {$enabled} />Anict&eacute;ricas &nbsp;&nbsp;&nbsp;&nbsp; <input type='radio' name='escleras' class='escleras' size=50 readonly='readonly' value='n' ".(($dados->escleras=='n')?"checked":"")." {$enabled} />Ict&eacute;ricas
		<span class='escleras1'><select name='esclerasobs' class='COMBO_-' style='background-color:transparent;'>
		<option value=''></option><option value='+' ".(($ev->escleras_obs=='+')?"selected":"")." >+</option><option value='++' ".(($ev->escleras_obs=='++')?"selected":"")." >++</option><option value='+++' ".(($ev->escleras_obs=='+++')?"selected":"").">+++</option><option value='++++'".(($ev->escleras_obs=='++++')?"selected":"").">++++</option></select></span></td></tr>";
		echo "<tr><td><b>Padr&atilde;o Respirat&oacute;:</b>  </td><td colspan='2'><input type='radio' name='respiratorio' class='respiratorio' size=50 readonly='readonly' value='s' ".(($ev->respiratorio=='s')?"checked":"")." {$enabled} />Eupn&eacute;ico &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='respiratorio' class='respiratorio' size=50 readonly='readonly' value='n' ".(($dados->respiratorio=='n')?"checked":"")." {$enabled} />Taqui/Dispn&eacute;ico
		<span class='respiratorio1'><select name='respiratorioobs' class='COMBO_-' style='background-color:transparent;'>
		<option value=''></option><option value='+' ".(($ev->respiratorio_obs=='+')?"selected":"")." >+</option><option value='++' ".(($ev->respiratorio_obs=='++')?"selected":"")." >++</option><option value='+++' ".(($ev->respiratorio_obs=='+++')?"selected":"").">+++</option><option value='++++'".(($ev->respiratorio_obs=='++++')?"selected":"").">++++</option></select></span><input type='radio' name='respiratorio' class='respiratorio' size=50 readonly='readonly' value='1' ".(($ev->respiratorio=='1')?"checked":"")." {$enabled} />Outro<br/><span class='outro_respiratorio'><textarea cols=30 rows=2 name='outro_respiratorio' ></textarea></span></td></tr>";
		echo "<tr><td colspan='3'><b>PA (sist&oacute;lica):</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' class='numerico1 OBG' size='6' name='pa_sistolica' value='{$ev->pa_sistolica}' {$acesso} />mmhg</td></tr>";
		echo "<tr><td colspan='3'><b>PA (diast&oacute;lica):</b>&nbsp;&nbsp;&nbsp;<input type='texto' class='numerico1 OBG' size='6' name='pa_diastolica' value='{$ev->pa_diastolica}' {$acesso} />mmhg</td></tr>";
		echo "<tr><td colspan='3'><b>FC: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' name='fc' value='{$ev->fc}' {$acesso} />bpm</td></tr>";
		echo "<tr><td colspan='3'><b>FR:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' cols=100 rows=2 size='6' class='numerico1 OBG' name='fr' value='{$ev->fr}' {$acesso} />irpm</td></tr>";
		echo "<tr><td colspan='3'><b>Temperatura:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' name='temperatura' value='{$ev->temperatura}' {$acesso} />&deg;C</td></tr>";

		echo"<tr><td colspan='3' ><b>DOR: </b><select name=dor_sn class='COMBO_OBG'><option value =''>  </option>
		<option value ='n'>N&atilde;o</option>
		<option value ='s'>Sim</option></select>&nbsp;&nbsp;<span name='dor' id='dor_span'>
		<b>Local:</b><input type='text' name='dor' class='dor' {$acesso} size=50/><button id='add-dor' tipo=''  > + </button></span></td></tr>";

		echo "<tr><td colspan='3' ><table id='dor' style='width:100%;'>";
		if(!empty($ev->dor)){
			foreach($ev->dor as $a){
				echo "<tr><td>{$a}</td></tr>";
			}
		}
		echo "</table></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3' >CATETERES E SONDAS</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='iot'/>TRAQUEOSTOMIA</td></tr>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='sne'/>SONDA NASOENTERAL</td></tr>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='gt'/>GASTROSTOMIA</td></tr>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='svd'/>SONDA VESICAL DE DEMORA</td></tr>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='Acesso Venoso' central'/>ACESSO VENOSO CENTRAL</td></tr>";
		echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='Acesso Perif�rico'/>ACESSO VENOSO PERIF&Eacute;RICO</td></tr>";
		echo"<tr><td colspan='3' ><b>OUTRO: </b><input type='text' name='cateter' class='cateter' {$acesso} size=75/><button id='add-cateter' tipo=''  > + </button></td></tr>";

		echo "<tr><td colspan='3' ><table id='cateter' style='width:100%;'>";
		if(!empty($ev->cateter)){
			foreach($ev->cateter as $a){
				echo "<tr><td>{$a}</td></tr>";
			}
		}
		echo "</table></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3' >Exame segmentar</th>";
		echo "</tr></thead>";
		$sql="select * from examesegmentar";
		$result=mysql_query($sql);
		while($row = mysql_fetch_array($result)){

			echo "<tr><td idexame={$row['ID']} class='exame1'> <b>{$row['DESCRICAO']}</b></td><td colspan='2'><input type=radio class='exame' name='{$row['ID']}' value ='1' >Normal ";
			echo"<input type=radio class='exame' name='{$row['ID']}'  value ='2' >Alterado <input type=radio class='exame' name='{$row['ID']}' value='3'>N&atilde;o Examinado</td></tr>";
			echo"<tr><td colspan='3'><span class=obs_exame name='{$row['ID']}'> <textarea cols=100 rows=2 name='obsexame'   class='mostrar_observacao' {$acesso} ></textarea></span></td></tr>";
			 
		}
		echo "<thead><tr>";
		echo "<th colspan='3' >Exames Complementares Novos</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='3' ><textarea cols=100 rows=5 name='novo_exame'  valeu={$ev->novo_exame} {$acesso} ></textarea></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3' >Impress&atilde;o</th>";
		echo "</tr></thead>";
		echo "<thead><tr>";
		echo "<tr><td colspan='3' ><textarea cols=100 rows=5 name='impressao'   class='OBG' valeu={$ev->impressao} {$acesso} ></textarea></td></tr>";
		echo "<th colspan='3' >Plano Diagn&oacute;stico</th>";
		echo "</tr></thead>";
		echo "<thead><tr>";
		echo "<tr><td colspan='3' ><textarea cols=100 rows=5 name='plano_diagnostico'   class='OBG' valeu={$ev->plano_diagnostico} {$acesso} ></textarea></td></tr>";
		echo "<th colspan='3' >Plano Teraup&ecirc;utico</th>";
		echo "</tr></thead>";
		echo "<thead><tr>";
		echo "<tr><td colspan='3' ><textarea cols=100 rows=5 name='terapeutico'   class='OBG' valeu={$ev->plano_terapeutico} {$acesso} ></textarea></td></tr>";




		echo "</table></form></div>";
		 
		if($acesso != "readonly") {
			 
			//	echo "<button id='teste' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Teste</span></button></span>";
			echo "<button id='salvar-ficha-evolucao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
			echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";

		}
		 
	}


	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	//Nova com base na anterior
	//Ana Paula
	public function anterior_evolucao_medica($id){
			
			echo "<div id='dialog-validar-radio' title='Validar Campos'>";
			echo "<center><h3>Algum item n&atilde;o foi selecionado.</h3></center>";
			echo "</div>";
			
			echo "<center><h1>Ficha de Evolu&ccedil;&atilde;o M&eacute;dica </h1></center>";
			
			//Cabeçalho
			$id = anti_injection($id,"numerico");
			$sql = "SELECT
			DATE_FORMAT(fev.DATA,'%d/%m/%Y %H:%i:%s') as fdata,
			fev.ID,	fev.PACIENTE_ID as idpaciente,
			u.nome,	p.nome as paciente
			FROM
			fichamedicaevolucao as fev LEFT JOIN
			usuarios as u ON (fev.USUARIO_ID = u.idUsuarios) INNER JOIN
			clientes as p ON (fev.PACIENTE_ID = p.idClientes)
			WHERE fev.ID = '{$id}'";
			
			$r = mysql_query($sql);
			
			while($row = mysql_fetch_array($r)){
				$usuario =ucwords(strtolower($row["nome"]));
				$data = $row['fdata'];
				$paciente=$row['idpaciente'];
			}
		
			$this->cabecalho($paciente);
			
			echo "<center><h2>Respons&aacute;vel: {$usuario} - Data: {$data}</h2></center>";
			//Fim do Cabeçalho
			
			$this->plan;

			echo "<div id='div-ficha-evolucao-medica'><br/>";
			
			echo alerta();
			echo "<form>";
				echo "<input type='hidden' name='paciente'  value='{$paciente}' />";
				echo "<br/>";
				
				echo "<table class='mytable' style='width:95%;'>";
					
					$sql="select  Max(ev.ID) from fichamedicaevolucao as ev where ev.PACIENTE_ID = {$paciente}";
					$result=mysql_query($sql);
					$x= mysql_result($result,0);
	
					$sql="select Max(id) from capmedica where paciente = {$paciente}";
					$result=mysql_query($sql);
					$y= mysql_result($result,0);
	
					//Problemas Ativos
					echo "<thead><tr>";
					echo "<th colspan='3' >Problemas Ativos</th>";
					echo "</tr></thead>";
					
					// 1 se resolvido, 2-melhor, 3-estavel, 4-pior
					
					if($x == ''){
	
						$sql2 = "SELECT p.DESCRICAO as nome , p.cid_id as cid, p.status, p.OBSERVACAO as status FROM problemaativoevolucao as p WHERE p.evolucao_id = '{$y}'";
						$result2 = mysql_query($sql2);
						$cont=1;
				
						while($row = mysql_fetch_array($result2)){
								
							echo "<tr><td cid={$row['cid']} prob_atv='{$row['nome']}' x='{$cont}' class='probativo1' style='width:20%'>
							<b>P{$cont} - {$row['nome']}</b></td>
							<td colspan='2'><input type=radio class='probativo' name='P{$cont}' value =1 >Resolvido ";
							echo"<input type=radio name='P{$cont}' class='probativo' value =2 >Melhor 
							<input type=radio class='probativo' name='P{$cont}' value =3 >Est&aacute;vel 
							<input class='probativo' type=radio name='P{$cont}' value =4 >Pior</td></tr>";
							echo"<tr><td colspan='3'><span class='obs_exame' name='P{$cont}'>
							<textarea cols=100 rows=2 class='obs{$cont}' >{$row['OBSERVACAO']}</textarea></span></td></tr>";
							$cont++;
						}
					}else{
						//$sql2="select pae.DESCRICAO as nome, pae.CID_ID,pae.STATUS from problemaativoevolucao as pae where pae.EVOLUCAO_ID = {$x} ";
						$sql2 = "SELECT p.DESCRICAO as nome , p.cid_id, p.status, p.OBSERVACAO FROM problemaativoevolucao as p WHERE p.evolucao_id = '{$x}'";
						$result2 = mysql_query($sql2);
						$cont=1;
					
							while($row = mysql_fetch_array($result2)){
								if($row['STATUS'] != 1){
									echo "<tr><td cid='{$row['CID_ID']}' prob_atv='{$row['nome']}' x='{$cont}' class='probativo1' style='width:20%'>
									<b>P{$cont} - {$row['nome']}</b></td><td colspan='2'>
									
									<input type=radio class='probativo' name='P{$cont}' value =1 ";
									if($row['status'] == 1){ echo ' checked'; }
									echo ">	Resolvido ";
									
									echo"<input type=radio class='probativo' name='P{$cont}' value =2"; 
									if($row['status'] == 2){ echo ' checked'; }
									echo ">Melhor"; 
									
									echo "<input type=radio class='probativo' name='P{$cont}' value =3"; 
									if($row['status'] == 3){ echo ' checked'; }
									echo ">Est&aacute;vel";
									 
									echo "<input type=radio class='probativo' name='P{$cont}' value =4"; 
									if($row['status'] == 4){ echo ' checked'; }
									echo ">Pior</td></tr>";
									
									echo"<tr><td colspan='3'><span class='obs_exame' name='P{$cont}'>
									<textarea cols=100 rows=2 class='obs{$cont}'>{$row['OBSERVACAO']}</textarea></span></td></tr>";
									$cont++;
								}
							}
				
					}
					
					echo "<input type='hidden' name='capmed'  value='{$y}' />";
				echo "</table>";
				
				echo "<table class='mytable' style='width:95%;'>";
					echo "<thead><tr>";
						echo "<th colspan='3' >Novos Problemas </th>";
					echo "</tr></thead>";
					
					echo "<tr><td colspan='3' ><b>Busca  </b><input type='text' name='busca-problemas-ativos-evolucao' {$acesso} size=100 /></td></tr>";
					echo"<tr><td colspan='3' ><input type='checkbox' id='outros-prob-ativos-evolucao' name='outros-prob-ativos-evolucao' {$acesso} />
					Outros.<span class='outro_ativo_evolucao' ><input type='text' name='outro-prob-ativo-evolucao' class='outro_ativo_evolucao' {$acesso} size=75/>
					<button id='add-prob-ativo-evolucao' tipo=''  >+ </button></span></td></tr>";	
				echo "<tr><td colspan='3' >";
				
				
					echo "<table id='problemas_ativos' style='width:100%;'>";
					
					if(!empty($ev->ativos)){
						foreach($ev->ativos as $a){
							echo "<tr><td>{$a}</td></tr>";
						}
					}
				
					
					echo "</table>";
				echo "</td></tr>";
				echo "</table>";
				
				echo "<table width='100%' class='mytable'>";
				
				//Intercorrências
				$sql_intercorrencias = "SELECT * FROM `intercorrenciasfichaevolucao` WHERE `FICHAMEDICAEVOLUCAO_ID` = {$id}";
				$resultado_intercorrencias = mysql_query($sql_intercorrencias);
				$cont_intercorrencias = 1;
					
				echo "<thead><tr>";
				echo "<th colspan='3' >Intercorrências </th>";
				echo "</tr></thead>";
				
 				echo "<tr>
 						<td>";//<thead>
				
				echo"<b>Intercorrências: </b>
						<select name=intercorrencias>
							<option value =''>  </option>
							<option value ='n'>N&atilde;o</option>
							<option value ='s'";
				if(!$resultado_intercorrencias){
					echo " selected";
				}
				echo ">Sim</option><span name='intercorrencias' id='intercorrencias_span'>
						</select></td>
					<td colspan='2'><span name='intercorrencias' id='intercorrencias_span'>";
				echo "<b>Descrição:</b>
				<input type='text' name='intercorrencias' class='intercorrencias' size=50/>
				<button id='add-intercorrencias' tipo=''  > + </button></span></td></tr>";
				
				while($row = mysql_fetch_array($resultado_intercorrencias)){
					echo "<tr><td id='inter' class='interc' name='int{$cont_intercorrencias}'><img src='../utils/delete_16x16.png' class='del-intercorrencias-ativas' title='Excluir' border='0'><b>Descrição: </b>{$row['DESCRICAO']}</td></tr>";
				
					$cont_intercorrencias++;
				}
				echo "<tr><thead id='intercorrencias_tabela'></thead></tr>";
				//echo "</thead>";
				echo "</table>";
				
				echo "<table width='100%' class='mytable'>";
				echo "<thead><tr>";
				
				//Evolução dos Sinais Vitais
				
				$sql_evolucao = "SELECT * FROM `fichamedicaevolucao` WHERE `ID` ={$id}";
				$resultado_evolucao = mysql_query($sql_evolucao);
				
				echo "<th colspan='3' >Evolu&ccedil;&atilde;o dos sinais vitais (Registro da semana)</th>";
				echo "</tr></thead>";
				
				while($row = mysql_fetch_array($resultado_evolucao)){
					echo "<tr><td><b>PA (sist&oacute;lica) </b></td><td>min:<input type='texto' class='numerico1 OBG' name='pa_sistolica_min' value='{$row['PA_SISTOLICA_MIN']}' {$acesso} size='4' /> m&aacute;x:
					<input type='texto' class='numerico1 OBG' name='pa_sistolica_max' value='{$row['PA_SISTOLICA_MAX']}' {$acesso} size='4' /></td>";
					
					echo "<td><b>ALERTA:</b>  <input type='radio'  name='pa_sistolica_sinal' value='1' {$acesso}";
					if($row['PA_SISTOLICA_SINAL'] == 1){ echo " checked"; }
					echo "/>Amarelo"; 
					echo "<input type='radio' class='radio' name='pa_sistolica_sinal' value='2' {$acesso} size='4'"; 
					if($row['PA_SISTOLICA_SINAL'] == 2){ echo " checked"; }
					echo "/>VERMELHO</td>";
					
					echo "</tr><tr><td><b>PA (diast&oacute;lica) </b> </td><td> min:<input type='texto' class='numerico1 OBG' name='pa_diastolica_min' value='{$row['PA_DIASTOLICA_MIN']}' {$acesso} size='4'  /> m&aacute;x:
					<input type='texto' class='numerico1 OBG' name='pa_diastolica_max' value='{$row['PA_DIASTOLICA_MAX']}' size='4' {$acesso} /></td>";
					
					echo "<td><b>ALERTA:</b>  <input type='radio'  name='pa_diastolica_sinal' value='1' {$acesso}"; 
					if($row['PA_DIASTOLICA_SINAL'] == 1){ echo " checked"; }
					echo "/>Amarelo"; 
					echo "<input type='radio' name='pa_diastolica_sinal' value='2' {$acesso} size='4'"; 
					if($row['PA_DIASTOLICA_SINAL'] == 2){ echo " checked"; }
					echo "/>VERMELHO</td>";
					
					echo "</tr><tr><td><b>FC </b> </td><td>min:<input type='texto' class='numerico1 OBG' name='fc_min' value='{$row['FR_MIN']}'  size='4' {$acesso} /> m&aacute;x:
					<input type='texto' class='numerico1 OBG' name='fc_max' value='{$row['FC_MAX']}' {$acesso} size='4' /></td>";
					
					echo "<td><b>ALERTA: </b> <input type='radio'  name='fc_sinal' value='1' {$acesso}"; 
					if($row['FC_SINAL'] == 1){ echo " checked"; }
					echo "/>Amarelo"; 
					
					echo "<input type='radio' name='fc_sinal' value='2' {$acesso} size='4'"; 
					if($row['FC_SINAL'] == 2){ echo " checked"; }
					echo "/>VERMELHO</td>";
					
					echo "</tr><tr><td><b>FR </b> </td><td>min:<input type='texto' class='numerico1 OBG' name='fr_min' value='{$row['FR_MIN']}' size='4' {$acesso} /> m&aacute;x:
					<input type='texto' class='numerico1 OBG' name='fr_max' value='{$row['FR_MAX']}' size='4' {$acesso}  /></td>";
					
					echo "<td><b>ALERTA:</b>  <input type='radio'  name='fr_sinal' value='1' {$acesso}"; 
					if($row['FR_SINAL'] == 1){ echo " checked"; }
					echo "/>Amarelo";
					echo "<input type='radio' name='fr_sinal' value='2' {$acesso} size='4'"; 
					if($row['FR_SINAL'] == 2){ echo " checked"; }
					echo "/>VERMELHO</td>";
					
					echo "</tr><tr><td width=20% ><b>Temperatura: </b></td><td width=25% >min:<input type='texto' class='numerico1 OBG' name='temperatura_min' size='4' value='{$row['TEMPERATURA_MIN']}' {$acesso} size='4' /> m&aacute;x:
					<input type='texto' class='numerico1 OBG' name='temperatura_max' size='4'value='{$row['TEMPERATURA_MAX']}' {$acesso} size='4' /></td>";
					
					echo "<td width=25%  ><b>ALERTA: </b> <input type='radio'  name='temperatura_sinal' value='1' {$acesso}";
					if($row['TEMPERATURA_SINAL'] == 1){ echo " checked"; }
					echo "/>Amarelo"; 
					
					echo "<input type='radio' name='temperatura_sinal' value='2' {$acesso} size='4'"; 
					if($row['TEMPERATURA_SINAL'] == 2){ echo " checked"; }
					echo "/>VERMELHO</td>";
					
					echo "</tr>";
					
					//Oximetria de Pulso
					
					echo "<tr><td width=20% ><b>Oximetria de Pulso: </b></td>
					<td width=25% colspan='3'>
					min:
					<input type='texto' class='numerico1' name='oximetria_pulso_min' size='4' value='{$row['OXIMETRIA_PULSO_MIN']}' size='4' />
						<select name='tipo_oximetria_min' class='COMBO_- tipo_oximetria_adicionais'>
							<option value=''></option>
							<option value='1' ".(($row['OXIMETRIA_PULSO_TIPO_MIN']=='1')?"selected":"").">Ar Ambiente</option>
							<option value='2' ".(($row['OXIMETRIA_PULSO_TIPO_MIN']=='2')?"selected":"").">O2 Suplementar</option>
						</select>
					<span id='suplementar_min_span' name='suplementar' style='display: inline;'>
					<input type='texto' size='3' class='numerico1' name='litros_min' value='{$row['OXIMETRIA_PULSO_LITROS_MIN']}'/>l/min
					<b>Via
						<select name='via_oximetria_min' class='COMBO_-' >
							<option value=''></option>
							<option value='1' ".(($row['OXIMETRIA_PULSO_VIA_MIN']=='1')?"selected":"").">Cateter Nasal</option>
							<option value='2' ".(($row['OXIMETRIA_PULSO_VIA_MIN']=='2')?"selected":"").">Cateter em Traqueostomia</option>
							<option value='3' ".(($row['OXIMETRIA_PULSO_VIA_MIN']=='3')?"selected":"").">Máscara de Venturi</option>
							<option value='4' ".(($row['OXIMETRIA_PULSO_VIA_MIN']=='4')?"selected":"").">Ventilação Mecânica</option>
						</select>
					</span>
					</td></tr>";
					echo "<tr><td width=20%></td>
					<td width=25% colspan='3'>
					m&aacute;x:
					<input type='texto' class='numerico1' name='oximetria_pulso_max' value='{$row['OXIMETRIA_PULSO_MAX']}' {$acesso} size='4' />
						<select name='tipo_oximetria_max' class='COMBO_-' tipo_oximetria_adicionais>
							<option value=''></option>
							<option value='1' ".(($row['OXIMETRIA_PULSO_TIPO_MAX']=='1')?"selected":"").">Ar Ambiente</option>
							<option value='2' ".(($row['OXIMETRIA_PULSO_TIPO_MAX']=='2')?"selected":"").">O2 Suplementar</option>
						</select>
					<span id='suplementar_max_span' name='suplementar' style='display: inline;'>
					<input type='texto' size='3' class='numerico1' name='litros_max' value='{$row['OXIMETRIA_PULSO_LITROS_MAX']}'/>l/min
					<b>Via
						<select name='via_oximetria_max' class='COMBO_-' >
							<option value=''></option>
							<option value='1' ".(($row['OXIMETRIA_PULSO_VIA_MAX']=='1')?"selected":"").">Cateter Nasal</option>
							<option value='2' ".(($row['OXIMETRIA_PULSO_VIA_MAX']=='2')?"selected":"").">Cateter em Traqueostomia</option>
							<option value='3' ".(($row['OXIMETRIA_PULSO_VIA_MAX']=='3')?"selected":"").">Máscara de Venturi</option>
							<option value='4' ".(($row['OXIMETRIA_PULSO_VIA_MAX']=='4')?"selected":"").">Ventilação Mecânica</option>
						</select>
					</span>
					</td></tr>";
				}
				
				echo "<thead><tr>";
				echo "<th colspan='3'>Exame F&iacute;sico</th>";
				
				$sql_evolucao = "SELECT * FROM `fichamedicaevolucao` WHERE `ID` ={$id}";
				$resultado_evolucao = mysql_query($sql_evolucao);
				while($row = mysql_fetch_array($resultado_evolucao)){

					echo "<tr><td><b>Estado Geral:</b>  </td><td colspan='2'>
					<input type='radio' name='estd_geral' size=50 value='1'".(($row['ESTADO_GERAL'] == 1)?" checked":"").
					"/>Bom &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type='radio' name='estd_geral' size=50 value='2'".(($row['ESTADO_GERAL'] == 2)?" checked":"")."/>Regular &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type='radio' name='estd_geral' size=50 value='3'".(($row['ESTADO_GERAL'] == 3)?" checked":"")."/>Ruim
					</td></tr>";
					
					echo "<tr><td><b>Mucosa:</b>  </td><td colspan='2'>
						<input type='radio' name='mucosa' class='mucosa' size=50 value='s' />Coradas &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type='radio' name='mucosa' size=50 value='n' ".(($row['MUCOSA']=='n')?"checked":"")." {$enabled} />Descoradas
						<span class='mucosa1'><select name='mucosaobs' class='COMBO_-' style='background-color:transparent;'>
						<option value=''></option><option value='+' ".(($row['MUCOSA_OBS']=='+')?"selected":"")." >+</option><option value='++' ".(($row['MUCOSA_OBS']=='++')?"selected":"")." >++</option><option value='+++' ".(($row['MUCOSA_OBS']=='+++')?"selected":"").">+++</option><option value='++++'".(($row['MUCOSA_OBS']=='++++')?"selected":"").">++++</option></select></span></td></tr>";
					
					echo "<tr><td><b>Escleras:</b>  </td><td colspan='2'><input type='radio' name='escleras' class='escleras' size=50 value='s' ".(($row['ESCLERAS']=='s')?"checked":"")." {$enabled} />Anict&eacute;ricas &nbsp;&nbsp;&nbsp;&nbsp; <input type='radio' name='escleras' class='escleras' size=50 value='n' ".(($row['ESCLERAS']=='n')?"checked":"")." {$enabled} />Ict&eacute;ricas
					<span class='escleras1'><select name='esclerasobs' class='COMBO_-' style='background-color:transparent;'>
					<option value=''></option><option value='+' ".(($row['ESCLERAS_OBS']=='+')?"selected":"")." >+</option><option value='++' ".(($row['ESCLERAS_OBS']=='++')?"selected":"")." >++</option><option value='+++' ".(($row['ESCLERAS_OBS']=='+++')?"selected":"").">+++</option><option value='++++'".(($row['ESCLERAS_OBS']=='++++')?"selected":"").">++++</option></select></span></td></tr>";

					echo "<tr><td><b>Padr&atilde;o Respirat&oacute;:</b>  </td>
							<td colspan='2'><input type='radio' name='respiratorio' class='respiratorio' size=50 value='s' "
							.(($row['PADRAO_RESPIRATORIO']=='s')?"checked":"")." {$enabled} />
							Eupn&eacute;ico &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type='radio' name='respiratorio' class='respiratorio' size=50 value='n' ".(($row['PADRAO_RESPIRATORIO']=='n')?"checked":"")." {$enabled} />
							Taqui/Dispn&eacute;ico 
							<span class='exibir_selecao respiratorio1 '>
							<select name='respiratorioobs' style='background-color:transparent;'>
								<option value=''></option>
								<option value='+' ".(($row['PADRAO_RESPIRATORIO_OBS']=='+')?"selected":"")." >+</option>
								<option value='++' ".(($row['PADRAO_RESPIRATORIO_OBS']=='++')?"selected":"")." >++</option>
								<option value='+++' ".(($row['PADRAO_RESPIRATORIO_OBS']=='+++')?"selected":"").">+++</option>
								<option value='++++'".(($row['PADRAO_RESPIRATORIO_OBS']=='++++')?"selected":"").">++++</option>
							</select></span>
					<input type='radio' name='respiratorio' class='respiratorio' size=50 value='1' ".(($row['PADRAO_RESPIRATORIO']=='1')?"checked":"")." {$enabled} />
					Outro<br/><span class='outro_respiratorio obs_exame'><textarea cols=30 rows=2 name='outro_respiratorio' >{$row['OUTRO_RESP']}</textarea></span></td></tr>";
					
					echo "<tr><td colspan='3'><b>PA (sist&oacute;lica):</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' class='numerico1 OBG' size='6' name='pa_sistolica' value='{$row['PA_SISTOLICA']}' {$acesso} />mmhg</td></tr>";
					echo "<tr><td colspan='3'><b>PA (diast&oacute;lica):</b>&nbsp;&nbsp;&nbsp;<input type='texto' class='numerico1 OBG' size='6' name='pa_diastolica' value='{$row['PA_DIASTOLICA']}' {$acesso} />mmhg</td></tr>";
					echo "<tr><td colspan='3'><b>FC: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' name='fc' value='{$row['FC']}' {$acesso} />bpm</td></tr>";
					echo "<tr><td colspan='3'><b>FR:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' cols=100 rows=2 size='6' class='numerico1 OBG' name='fr' value='{$row['FR']}' {$acesso} />irpm</td></tr>";
					echo "<tr><td colspan='3'><b>Temperatura:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' name='temperatura' value='{$row['TEMPERATURA']}' {$acesso} />&deg;C</td></tr>";
					
					echo"<tr><td colspan='3' ><b>DOR: </b>
							<select name=dor_sn class='COMBO_OBG'>
								<option value =''>  </option>
								<option value ='n' ".(($row['DOR']=='n')?"selected":"").">N&atilde;o</option>
								<option value ='s' ".(($row['DOR']=='s')?"selected":"").">Sim</option>
							</select>&nbsp;&nbsp;
						<span name='dor' id='dor_span' class='exibir_dor'>
						<b>Local:</b><input type='text' name='dor' class='dor' size=50/>
						<button id='add-dor' tipo=''  > + </button></span></td></tr>";
				}
				echo "<tr><td colspan='3' ><table id='dor' style='width:100%;'>";
				
				//Dor
				
				$sql_dor = "SELECT d.DESCRICAO, d.ESCALA, d.PADRAO FROM fichamedicaevolucao f
				INNER JOIN dorfichaevolucao d ON f.ID = d.FICHAMEDICAEVOLUCAO_ID
				WHERE f.ID =72";
				
				$resultado_dor = mysql_query($sql_dor);
				while($row = mysql_fetch_array($resultado_dor)){

					echo "<tr><td class='dor1' name='ftefasdf'> 
							<img src='../utils/delete_16x16.png' class='del-dor-ativos' title='Excluir' border='0'> 
							<b>Local: </b>{$row['DESCRICAO']}</td>
							<td colspan='2'>Escala analogica:
								<select name='dorftefasdf' class='COMBO_OBG' style='background-color:transparent;'>
									<option value='-1' selected=''></option>
									<option value='1'".(($row['ESCALA']=='1')?" selected":"").">1</option>
									<option value='2'".(($row['ESCALA']=='2')?" selected":"").">2</option>
									<option value='3'".(($row['ESCALA']=='3')?" selected":"").">3</option>
									<option value='4'".(($row['ESCALA']=='4')?" selected":"").">4</option>
									<option value='5'".(($row['ESCALA']=='5')?" selected":"").">5</option>
									<option value='6'".(($row['ESCALA']=='6')?" selected":"").">6</option>
									<option value='7'".(($row['ESCALA']=='7')?" selected":"").">7</option>
									<option value='8'".(($row['ESCALA']=='8')?" selected":"").">8</option>
									<option value='9'".(($row['ESCALA']=='9')?" selected":"").">9</option>
									<option value='10'".(($row['ESCALA']=='10')?" selected":"").">10</option>
								</select>
							Padrão da dor:<input type='text' name='padraodor' value='{$row['PADRAO']}'></td></tr>";
				}
				
				//Cateteres e Sondas
				echo "</table></td></tr>";
				echo "<thead><tr>";
				echo "<th colspan='3' >CATETERES E SONDAS</th>";
				echo "</tr></thead>";
				
				$sql_cateteres = "SELECT * FROM  `catetersondaevolucao` WHERE `FICHA_EVOLUCAO_ID` = {$id}";
				$resultado_cateteres = mysql_query($sql_cateteres);

				while($row = mysql_fetch_array($resultado_cateteres)){
					
					if($row['DESCRICAO'] == 'iot'){
						$iot = true;
					}else if($row['DESCRICAO'] == 'sne'){
						$sne = true;
					}else if($row['DESCRICAO'] == 'gt'){
						$gt = true;
					}else if($row['DESCRICAO'] == 'svd'){
						$svd = true;
					}else if($row['DESCRICAO'] == 'Acesso Venoso'){
						$acesso_venoso = true;
					}else if($row['DESCRICAO'] == 'Acesso Perif�rico'){
						$acesso_periferico = true;
					}else{
						$outro = true;
					}
					
				}//while
				
				echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='iot' ".(($iot==true)?"checked":"")."/>TRAQUEOSTOMIA</td></tr>";
				echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='sne' ".(($sne==true)?"checked":"")."/>SONDA NASOENTERAL</td></tr>";
				echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='gt' ".(($gt==true)?"checked":"")."/>GASTROSTOMIA</td></tr>";
				echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='svd' ".(($svd==true)?"checked":"")."/>SONDA VESICAL DE DEMORA</td></tr>";
				echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='Acesso Venoso' central' ".(($acesso_venoso==true)?"checked":"")."/>ACESSO VENOSO CENTRAL</td></tr>";
				echo "<tr><td colspan='3'><input type='checkbox' class='cat_sond' name='Acesso Perif�rico' ".(($acesso_periferico==true)?"checked":"")."/>ACESSO VENOSO PERIF&Eacute;RICO</td></tr>";
				echo"<tr><td colspan='3' ><b>OUTRO: </b><input type='text' name='cateter' class='cateter' {$acesso} size=75 /><button id='add-cateter' tipo=''  > + </button></td></tr>";
				echo "<tr><td colspan='3' >";
				
				echo "<table id='cateter' style='width:100%;'>";
				
				//Outros cateteres
				$sql_outros_cateteres = "SELECT `DESCRICAO` FROM  `catetersondaevolucao` 
										WHERE  `FICHA_EVOLUCAO_ID` ={$id}
										AND  `DESCRICAO` !=  'iot'
										AND  `DESCRICAO` !=  'sne'
										AND  `DESCRICAO` !=  'gt'
										AND  `DESCRICAO` !=  'svd'
										AND  `DESCRICAO` !=  'Acesso Venoso'
										AND  `DESCRICAO` !=  'Acesso Perif�rico'";
				$resultado_outros_cateteres = mysql_query($sql_outros_cateteres);
				
				$cont_outros_cateteres;
				while($row = mysql_fetch_array($resultado_outros_cateteres)){
					echo "<tr><td id='outro_cateter' class='cateter1' name='int{$cont_outros_cateteres}'><img src='../utils/delete_16x16.png' class='del-cateter-ativos' title='Excluir' border='0'><b>Descrição: </b>{$row['DESCRICAO']}</td></tr>";
				
					$cont_outros_cateteres++;
				}
				
				echo "</table></td></tr>";
				echo "<thead><tr>";
				echo "<th colspan='3'>Exame segmentar</th>";
				echo "</tr></thead>";
				
				$sql_exame_segmentar = "SELECT es.DESCRICAO, ese.EXAME_SEGMENTAR_ID, ese.ID, ese.AVALIACAO, ese.OBSERVACAO FROM examesegmentarevolucao ese
				INNER JOIN examesegmentar es ON es.ID = ese.EXAME_SEGMENTAR_ID WHERE ese.FICHA_EVOLUCAO_ID = {$id}";
				$resultado_exame_segmentar = mysql_query($sql_exame_segmentar);
				
					while($row = mysql_fetch_array($resultado_exame_segmentar)){
			
						echo "<tr><td idexame={$row['ID']} class='exame1'> <b>{$row['DESCRICAO']}</b></td><td colspan='2'>
						<input type=radio class='exame' name='{$row['ID']}' value ='1' ".(($row['AVALIACAO']==1)?" checked":"").">Normal ";
						echo"<input type=radio class='exame' name='{$row['ID']}'  value ='2' ".(($row['AVALIACAO']==2)?" checked":"").">Alterado 
						<input type=radio class='exame' name='{$row['ID']}' value='3'".(($row['AVALIACAO']==3)?" checked":"").">N&atilde;o Examinado</td></tr>";
						echo"<tr><td colspan='3'><span class=obs_exame name='{$row['ID']}'> 
						<textarea cols=100 rows=2 name='obsexame'   class='obsexame' {$acesso} >{$row['OBSERVACAO']}</textarea></span></td></tr>";
					}
					
				//Exames Complementares Novos
				
				echo "<thead><tr>";
				$sql_evolucao = "SELECT * FROM `fichamedicaevolucao` WHERE `ID` ={$id}";
				$resultado_evolucao = mysql_query($sql_evolucao);
				
				while($row = mysql_fetch_array($resultado_evolucao)){
					echo "<th colspan='3' >Exames Complementares Novos</th>";
					echo "</tr></thead>";
					echo "<tr><td colspan='3' ><textarea cols=100 rows=5 name='novo_exame'  value={$row['NOVOS_EXAMES']} {$acesso} >{$row['NOVOS_EXAMES']} {$acesso}</textarea></td></tr>";
					echo "<thead><tr>";
					echo "<th colspan='3' >Impress&atilde;o</th>";
					echo "</tr></thead>";
					echo "<thead><tr>";
					echo "<tr><td colspan='3' ><textarea cols=100 rows=5 name='impressao'   class='OBG' value={$row['IMPRESSAO']} {$acesso} >{$row['IMPRESSAO']} {$acesso}</textarea></td></tr>";
										echo "<th colspan='3' >Plano Diagn&oacute;stico</th>";
					echo "</tr></thead>";
					echo "<thead><tr>";
					echo "<tr><td colspan='3' ><textarea cols=100 rows=5 name='plano_diagnostico'   class='OBG' value={$row['PLANO_DIAGNOSTICO']} {$acesso} >{$row['PLANO_DIAGNOSTICO']}</textarea></td></tr>";
										echo "<th colspan='3' >Plano Teraup&ecirc;utico</th>";
					echo "</tr></thead>";
					echo "<thead><tr>";
					echo "<tr><td colspan='3' ><textarea cols=100 rows=5 name='terapeutico'   class='OBG' value={$row['PLANO_TERAPEUTICO']} {$acesso} >{$row['PLANO_TERAPEUTICO']}</textarea></td></tr>";
				}	
				
				echo "</table></form></div>";
				echo "<button id='salvar-ficha-evolucao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
				echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
			
				
			
	}//anterior_evolucao_medica
	
	public function detalhes_ficha_evolucao_medica($id){
		 
		$id = anti_injection($id,"numerico");
		$sql = "SELECT
		DATE_FORMAT(fev.DATA,'%d/%m/%Y %H:%i:%s') as fdata,
		fev.ID,
		fev.PACIENTE_ID as idpaciente,
		u.nome,
		p.nome as paciente
		FROM
		fichamedicaevolucao as fev LEFT JOIN
		usuarios as u ON (fev.USUARIO_ID = u.idUsuarios) INNER JOIN
		clientes as p ON (fev.PACIENTE_ID = p.idClientes)

		WHERE
		fev.ID = '{$id}'";
		$r = mysql_query($sql);

		while($row = mysql_fetch_array($r)){
			$usuario =ucwords(strtolower($row["nome"]));
			$data = $row['fdata'];
			$paciente=$row['idpaciente'];
		}
		 
		echo "<center><h1>Detalhes Ficha de Evolu&ccedil;&atilde;o M&eacute;dica </h1></center>";
		 
		$this->cabecalho($paciente);
		echo "<center><h2>Respons&aacute;vel: {$usuario} - Data: {$data}</h2></center>";
		//echo "<a href=imprimir_ficha.php?id={$ev->capmedica}>imprimir</a></br>";
		echo"<form method=post target='_blank' action='imprimir_ficha_evolucao.php?id={$id}'>";
		echo"<input type=hidden value={$paciente} name='paciente'</input>";
		echo "<button id='imprimir_ficha_evolucao' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
				<span class='ui-button-text'>Imprimir</span></button>";
		echo "</form>";
		 
		 
		echo "<div id='div-ficha-evolucao-medica'><br/>";
		 
		echo alerta();
		echo "<form>";
		echo "<input type='hidden' name='paciente'  value='{$ev->pacienteid}' />";
		echo "<br/><table class='mytable' style='width:100%;'>";
		 
		echo "<thead><tr>";
		echo "<th colspan='3' >Problemas Ativos.</th>";
		echo "</tr></thead>";

		$sql2="select pae.DESCRICAO as nome, pae.CID_ID,(CASE pae.STATUS
		WHEN '1' THEN 'RESOLVIDO'
		WHEN '2' THEN 'MELHOR'
		WHEN '3' THEN 'EST&Aacute;VEL'
		WHEN '4' THEN 'PIOR' END) as sta , pae.STATUS,pae.OBSERVACAO from problemaativoevolucao as pae where pae.EVOLUCAO_ID = {$id} ";
		$result2 = mysql_query($sql2);
		$cont=1;
		 
		while($row = mysql_fetch_array($result2)){
			if($row['STATUS'] != 0){
				echo "<tr><td cid={$row['cid']} prob_atv='{$row['nome']}' x='{$cont}' class='probativo1' style='width:20%'><b>P{$cont} - {$row['nome']}</b> </td><td colspan='2'><b>Estado:</b> {$row['sta']}</td></tr>";
				if( $row['STATUS'] != 1){
					echo"<tr><td colspan='3'><b>OBSERVA&Ccedil;&Atilde;O: </b>{$row['OBSERVACAO']}</td></tr>";
				}
				$cont++;
			}
		}
		 

		echo "<input type='hidden' name='capmed'  value='{$y}' />";
		echo "<thead><tr>";
		echo "<th colspan='3' >Novos Problemas </th>";
		
		$sql_intercorrencias = "SELECT * FROM intercorrenciasfichaevolucao WHERE fichamedicaevolucao_id = {$id}";
		$resultado_intercorrencias = mysql_query($sql_intercorrencias);
		
		while($row = mysql_fetch_array($resultado_intercorrencias)){
			echo "<tr><td>{$row['DESCRICAO']}</td></tr>";
		}
		
		echo "</tr></thead>";
		
		$sql2="select pae.DESCRICAO as nome, pae.CID_ID, pae.STATUS, pae.OBSERVACAO from problemaativoevolucao as pae where pae.EVOLUCAO_ID = {$id} ";
		$result2 = mysql_query($sql2);

		while($row = mysql_fetch_array($result2)){
			if($row['STATUS'] == 0){
					
				echo"<tr><td colspan=''><b>- {$row['nome']} </b><td colspan='2'> <b>OBS:</b> {$row['OBSERVACAO']}</td></tr>";
					
			}
		}
		 
		$sql= "select * from fichamedicaevolucao where ID = {$id}";
		$result=mysql_query($sql);
		while($row = mysql_fetch_array($result)){

			$pa_sistolica_min =$row['PA_SISTOLICA_MIN'];
			$pa_sistolica_max =$row['PA_SISTOLICA_MAX'];
			$pa_sistolica =$row['PA_SISTOLICA'];
			$pa_sistolica_sinal =$row['PA_SISTOLICA_SINAL'];
			$pa_diastolica_min = $row['PA_DIASTOLICA_MIN'];
			$pa_diastolica_max = $row['PA_DIASTOLICA_MAX'];
			$pa_diastolica  = $row['PA_DIASTOLICA'];
			$pa_diastolica_sinal = $row['PA_DIASTOLICA_SINAL'];
			$fc =$row["FC"];
			$fc_max =$row["FC_MAX"];
			$fc_min =$row["FC_MIN"];
			$fc_sinal =$row["FC_SINAL"];
			$fr =$row["FR"];
			$fr_max =$row["FR_MAX"];
			$fr_min =$row["FR_MIN"];
			$fr_sinal =$row["FR_SINAL"];
			$temperatura_max = $row["TEMPERATURA_MAX"];
			$temperatura_min = $row["TEMPERATURA_MIN"];
			$temperatura = $row["TEMPERATURA"];
			$temperatura_sinal = $row["TEMPERATURA_SINAL"];
			$estd_geral = $row["ESTADO_GERAL"];
			$mucosa = $row['MUCOSA'];
			$mucosaobs = $row['MUCOSA_OBS'];
			$escleras = $row['ESCLERAS'];
			$esclerasobs = $row['ESCLERAS_OBS'];
			$respiratorio = $row['PADRAO_RESPIRATORIO'];
			$respiratorioobs = $row['PADRAO_RESPIRATORIO_OBS'];
			$novo_exame = $row['NOVOS_EXAMES'];
			$impressao = $row['IMPRESSAO'];
			$diagnostico= $row['PLANO_DIAGNOSTICO'];
			$terapeutico= $row['PLANO_TERAPEUTICO'];
			$dor_sn= $row['DOR'];
			$outro_resp= $row['OUTRO_RESP'];
			$oximetria_pulso_max = $row["OXIMETRIA_PULSO_MAX"];
			$oximetria_pulso_min = $row["OXIMETRIA_PULSO_MIN"];
			$tipo_oximetria_min = $row["OXIMETRIA_PULSO_TIPO_MIN"];
			$litros_min = $row["OXIMETRIA_PULSO_LITROS_MIN"];
			$via_oximetria_min = $row["OXIMETRIA_PULSO_VIA_MIN"];
			$tipo_oximetria_max = $row["OXIMETRIA_PULSO_TIPO_MAX"];
			$litros_max = $row["OXIMETRIA_PULSO_LITROS_MAX"];
			$via_oximetria_max = $row["OXIMETRIA_PULSO_VIA_MAX"];

			 
		}
		 
		echo "<thead><tr>";
		echo "<th colspan='3' >Evolu&ccedil;&atilde;o dos sinais vitais (Registro da semana)</th>";
		echo "</tr></thead>";
		echo "<tr><td><b>PA (sist&oacute;lica) </b></td><td>min:<input type='texto' readonly class='numerico1 OBG' name='pa_sistolica_min' value='{$pa_sistolica_min}' readonly size='4' /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='pa_sistolica_max' value='{$pa_sistolica_max}' readonly  readonly  size='4' /></td>";
		if($pa_sistolica_sinal == 1){
			$x="AMARELO";
		}
		 
		if($pa_sistolica_sinal == 2){
			$x="VERMELHO";
		}
		echo "<td><b>ALERTA:</b> {$x} </td>";
		echo "</tr>";
		echo "<tr><td><b>PA (diast&oacute;lica) </b> </td><td> min:<input type='texto' class='numerico1 OBG' name='pa_diastolica_min' value='{$pa_diastolica_min}' readonly  size='4'  /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='pa_diastolica_max' value='{$pa_diastolica_max}' size='4' readonly/></td>";
		$x='';
		if($pa_diastolica_sinal == 1){
			$x="AMARELO";
		}
		if($pa_diastolica_sinal == 2){
			$x="VERMELHO";
		}
		echo "<td><b>ALERTA:</b> {$x}</td>";
		echo "</tr>";
		echo "<tr><td><b>FC </b> </td><td>min:<input type='texto' class='numerico1 OBG' name='fc_min' value='{$fc_min}'  size='4' readonly /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='fc_max' value='{$fc_max}' readonly size='4' /></td>";
		$x='';
		if($fc_sinal == 1){
			$x="AMARELO";
		}
		if($fc_sinal == 2){
			$x="VERMELHO";
		}
		echo "<td><b>ALERTA:</b> {$x}</td>";
		echo "</tr>";
		echo "<tr><td><b>Fr </b> </td><td>min:<input type='texto' class='numerico1 OBG' name='fr_min' value='{$fr_min}' size='4' readonly /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='fr_max' value='{$fr_max}' size='4'readonly /></td>";
		$x='';
		if($fr_sinal == 1){
			$x="AMARELO";
		}
		if($fr_sinal == 2){
			$x="VERMELHO";
		}
		echo "<td><b>ALERTA:</b> {$x}</td>";
		echo "</tr>";
		echo "<tr><td width=20% ><b>Temperatura: </b></td><td width=30% >min:<input type='texto' class='numerico1 OBG' name='temperatura_min' size='4' value='{$temperatura_min}' readonly size='4' /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='temperatura_max' size='4'value='{$temperatura_max}' readonly size='4' /></td>";
		$x='';
		if($temperatura_sinal == 1){
			$x="AMARELO";
		}
		if($temperatura_sinal == 2){
			$x="VERMELHO";
		}
		echo "<td><b>ALERTA:</b> {$x}</td>";
		echo "</tr>";
		
		//Oximetria de Pulso
		echo "<tr><td width=20% ><b>Oximetria de Pulso: </b></td>
		<td width=25% colspan='3'>
		min:
		<input type='texto' class='numerico1' name='oximetria_pulso_min' size='4' value='{$oximetria_pulso_min}' readonly size='4' />";
		$tipo_min = '';
		if($tipo_oximetria_min == 1){
			$tipo_min = 'Ar Ambiente';
		}
		if($tipo_oximetria_min == 2){
			$tipo_min = 'O2 Suplementar';
		}
		echo "<b> Tipo</b><input type='texto' value='{$tipo_min}' readonly/>";
		
		if($tipo_oximetria_min == 2){
			$via_min = '';
			if ($via_oximetria_min == 1) {
				$via_min = 'Cateter Nasal';
			}
			if ($via_oximetria_min == 2) {
				$via_min = 'Cateter em Traqueostomia';
			}
			if ($via_oximetria_min == 3) {
				$via_min = 'Máscara de Venturi';
			}
			if ($via_oximetria_min == 4) {
				$via_min = 'Ventilação Mecânica';
			}
			echo "<b> Dosagem</b><input type='texto' size='3' value='{$litros_min}' readonly/>l/min";
			echo "<b> Via</b><input type='texto' value='{$via_min}' readonly/>";
		}
		
		echo "</td></tr>";
		
		echo "<tr><td width=20% ></td>
		<td width=25% colspan='3'>
		max:
		<input type='texto' class='numerico1' name='oximetria_pulso_min' size='4' value='{$oximetria_pulso_max}' readonly size='4' />";
		$tipo_max = '';
		if($tipo_oximetria_max == 1){
			$tipo_max = 'Ar Ambiente';
		}
		if($tipo_oximetria_max == 2){
			$tipo_max = 'O2 Suplementar';
		}
		echo "<b> Tipo</b><input type='texto' value='{$tipo_max}' readonly/>";
		
		if($tipo_oximetria_max == 2){
			$via_max = '';
			if ($via_oximetria_max == 1) {
				$via_max = 'Cateter Nasal';
			}
			if ($via_oximetria_max == 2) {
				$via_max = 'Cateter em Traqueostomia';
			}
			if ($via_oximetria_max == 3) {
				$via_max = 'Máscara de Venturi';
			}
			if ($via_oximetria_max == 4) {
				$via_max = 'Ventilação Mecânica';
			}
			echo "<b> Dosagem</b><input type='texto' size='3' value='{$litros_max}' readonly/>l/min";
			echo "<b> Via</b><input type='texto' value='{$via_max}' readonly/>";
		}
		
		echo "</td></tr>";
		
		echo "<thead><tr>";
		echo "<th colspan='3'>Exame F&iacute;sico</th>";
		$x='';
		$y1='';
		if($estd_geral == 1){
			$x="BOM";
		}
		if($estd_geral == 2){
			$x="REGULAR";
		}
		if($estd_geral == 3){
			$x="RUIM";
		}
		 
		echo "<tr><td><b>Estado Geral:</b>  </td><td colspan='2'>{$x}</td></tr>";
		$x='';
		$y1='';
		if ($mucosa =='n'){
			$x= 'DESCORADAS';
			$y1= $mucosaobs;
		}
		if ($mucosa =='s'){
			$x= 'Coradas';
			$y1= $mucosaobs;
		}
		echo "<tr><td><b>Mucosa:</b>  </td><td colspan='2'>{$x}  {$y1}</td></tr>";
		$x='';
		if ($escleras =='n'){
			$x= 'Ict&eacute;ricas';
			$y1= $esclerasobs;
		}
		if ($escleras =='s'){
			$x= 'Anict&eacute;ricas';
			$y1= $esclerasobs;
		}
		echo "<tr><td><b>Escleras:</b>  </td><td colspan='2'>{$x}  {$y1}</td></tr>";
		$x='';
		$y1='';
		if ($respiratorio =='n'){
			$x= 'Taqui/Dispn&eacute;ico';
			$y1= $respiratorioobs;
		}
		if ($respiratorio =='s'){
			$x= 'Eupn&eacute;ico';
			$y1= $respiratorioobs;
		}
		if ($respiratorio =='1'){
			$x= 'Outro';
			$y1= $outro_resp;
		}
		echo "<tr><td><b>Padr&atilde;o Respirat&oacute;:</b>  </td><td colspan='2'>{$x}  {$y1}</td></tr>";
		echo "<tr><td colspan='3'><b>PA (sist&oacute;lica):</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' class='numerico1 OBG' size='6' name='pa_sistolica' value='{$pa_sistolica}'  readonly {$acesso} />mmhg</td></tr>";
		echo "<tr><td colspan='3'><b>PA (diast&oacute;lica):</b>&nbsp;&nbsp;&nbsp;<input type='texto' class='numerico1 OBG' size='6' name='pa_diastolica' value='{$pa_diastolica}' readonly {$acesso} />mmhg</td></tr>";
		echo "<tr><td colspan='3'><b>FC: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' name='fc' readonly value='{$fc}' {$acesso} />bpm</td></tr>";
		echo "<tr><td colspan='3'><b>FR:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' readonly name='fr' value='{$fr}' {$acesso} />irpm</td></tr>";
		echo "<tr><td colspan='3'><b>Temperatura:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' name='temperatura' value='{$temperatura}' readonly {$acesso} />&deg;C</td></tr>";
		if($dor_sn == 's'){
			$dor_sn = "Sim";
		}
		if($dor_sn == 'n'){
			$dor_sn = "N&atilde;o";
		}
		echo"<tr><td colspan='3' ><b>DOR:</b>{$dor_sn}</td></tr>";
		 
		echo "<tr><td colspan='3' ><table id='dor' style='width:100%;'>";
		$sql = "select * from dorfichaevolucao where FICHAMEDICAEVOLUCAO_ID = {$id} ";
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result)){
			echo "<tr><td> <b>Local:</b> {$row['DESCRICAO']}&nbsp;&nbsp; <b>Escala visual:</b> {$row['ESCALA']}&nbsp;&nbsp;<b>Padr&atilde;o:</b>{$row['PADRAO']}&nbsp;&nbsp; </td></tr>";

		}
		 
		 
		echo "</table></td></tr>";
		 
		echo "<thead><tr>";
		echo "<th colspan='3' >CATETERS e SONDAS </th>";
		echo "</tr></thead>";
		$sql = "select DESCRICAO from catetersondaevolucao where FICHA_EVOLUCAO_ID = {$id}";
		$result= mysql_query($sql);
		while ($row= mysql_fetch_array($result)){
			 
			echo "<tr><td colspan='3'><b>- {$row['DESCRICAO']}<b></td></tr>";
		}
		 
		echo "<thead><tr>";
		echo "<th colspan='3' >Exame segmentar</th>";
		echo "</tr></thead>";
		$sql="select exv.*,(CASE exv.AVALIACAO
		WHEN '1' THEN 'NORMAL'
		WHEN '2' THEN 'ALTERADO'
		WHEN '3' THEN 'N EXAMINADO' END) as av,ex.DESCRICAO from examesegmentarevolucao as exv LEFT JOIN examesegmentar as ex ON (exv.EXAME_SEGMENTAR_ID = ex.ID) where exv.FICHA_EVOLUCAO_ID = {$id}";
		$result=mysql_query($sql);
		while($row = mysql_fetch_array($result)){
			 
			echo "<tr><td idexame={$row['ID']} class='exame1'> <b>{$row['DESCRICAO']}</b></td><td colspan='2'>{$row['av']}</td></tr>";
			if($row['AVALIACAO'] == 2 || $row['AVALIACAO'] == 3){
				echo"<tr><td colspan='3'><b>OBS: </b>{$row['OBSERVACAO']}</td></tr>";
			}
		}
		echo "<thead><tr>";
		echo "<th colspan='3' >Exames Complementares Novos</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='3' ><b>{$novo_exame} </b></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3' >Impress&atilde;o</th>";
		echo "</tr></thead>";
		echo "<thead><tr>";
		echo "<tr><td colspan='3' ><b>{$impressao} </b></td></tr>";
		echo "<th colspan='3' >Plano Diagn&oacute;stico</th>";
		echo "</tr></thead>";
		echo "<thead><tr>";
		echo "<tr><td colspan='3' ><b>{$diagnostico} </b></td></tr>";
		echo "<th colspan='3' >Plano Teraup&ecirc;utico</th>";
		echo "</tr></thead>";
		echo "<thead><tr>";
		echo "<tr><td colspan='3' ><b>{$terapeutico}</b></td></tr>";
		 
		 
		 
		 
		echo "</table></form></div>";
		echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
		 
		 
	}
	////////////////////////////////////////

	public function listar_evolucao_medica($id){
		$id = anti_injection($id,"numerico");
		$sql3="select Max(id) as ev from fichamedicaevolucao where PACIENTE_ID = '{$id}' ";
		$r2=mysql_query($sql3);

		$maior_ev= mysql_result($r2,0);
		 

		$sql = "SELECT
		DATE_FORMAT(fev.DATA,'%d/%m/%Y %H:%i:%s') as fdata,
		fev.ID,
		fev.PACIENTE_ID as idpaciente,
		u.nome,
		p.nome as paciente
		FROM
		fichamedicaevolucao as fev LEFT JOIN
		usuarios as u ON (fev.USUARIO_ID = u.idUsuarios) INNER JOIN
		clientes as p ON (fev.PACIENTE_ID = p.idClientes)

		WHERE
		fev.PACIENTE_ID = '{$id}'
		ORDER BY
		fev.DATA DESC";
		$r = mysql_query($sql);
		$flag = true;
		while($row = mysql_fetch_array($r)){
			if($flag) {

				$p = ucwords(strtolower($row["fev.PACIENTE_ID"]));
				echo "<center><h1>Lista das Fichas de Evolu&ccedil;&atilde;o M&eacute;dica </h1></center>";
				$this->cabecalho($id);
				echo "<br/><br/>";

				echo "<div><input type='hidden' name='nova_ficha_evolucao' value='{$id}'></input>";

				echo "<button id='nova_ficha_evolucao'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
						<span class='ui-button-text'>Nova ficha</span></button></div>";

				echo "<br/><table class='mytable' width='100%' >";
				echo "<thead><tr>";
				echo "<th width='55%'>Usu&aacute;rio</th>";
				echo "<th>Data</th>";
				echo "<th width='15%'>Detalhes</th>";
				echo "<th width='15%'>Op&ccedil;&otilde;es</th>";
				echo "</tr></thead>";
				$flag = false;
			}

			$u = ucwords(strtolower($row["nome"]));

			echo "<tr><td>{$u}</td><td>{$row['fdata']}</td>";
			echo "<td><a href=?adm=paciente&act=fichaevolucao&id={$row['ID']}><img src='../utils/capmed_16x16.png' title='Detalhes Ficha Evolu&ccedil;&atilde;o' border='0'></a></td>";
			echo"<td><a href=?adm=paciente&act=anterior_evolucao&id={$row['ID']}><img src='../utils/nova_avaliacao_24x24.png' width=16 title='Usar para nova avalia&ccedil;&atilde;o' border='0'></td>";
			echo "</tr>";

		}


		if($flag){
			$this->nova_evolucao_medica($id);


			return;
		}


		echo "</table>";
	}


	///////////////////////////fim ficha medica


	////////////////////////FICHA AVALI��O ENFERMAGEM/////////////////////
	public function nova_ficha_avaliacao_enfermagem($id){

		$id = anti_injection($id,"numerico");
		$result = mysql_query("SELECT p.nome FROM clientes as p WHERE p.idClientes = '{$id}'");
		 
		$fae = new FichaAvaliacaoEnfermagem();
		while($row = mysql_fetch_array($result)){
			$fae->pacienteid = $id;
			$fae->pacientenome = ucwords(strtolower($row["nome"]));

		}
		 
		$this->ficha_avaliacao_enfermagem($fae,"");
	}

	/////////////////////////////////////

	public function ficha_avaliacao_enfermagem($fae,$acesso){




		echo "<center><h1>Ficha de Avalia&ccedil;&atilde;o de Enfermagem</h1></center>";
		//echo "<center><h2>{$ev->pacientenome}</h2></center>";
		$this->cabecalho($fae);


		/*	if($acesso == "readonly"){

		echo "<center><h2>Respons&aacute;vel: {$ev->usuario} - Data: {$ev->data}</h2></center>";
		//echo "<a href=imprimir_ficha.php?id={$ev->capmedica}>imprimir</a></br>";
		echo"<form method=post target='_blank' action='imprimir_ficha.php?id={$ev->capmedica}'>";
		echo"<input type=hidden value={$ev->capmedica} class='imprimir_ficha_mederi'</input>";
		echo "<button id='imprimir_ficha_mederi' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
		<span class='ui-button-text'>Imprimir</span></button>";
		echo "</form>";
		}*/

		$this->plan;


		echo "<div id='div-ficha-avaliacao-enfermagem'><br/>";

		echo alerta();
		echo "<form>";
		echo "<input type='hidden' name='paciente'  value='{$ev->pacienteid}' />";
		echo "<br/><table class='mytable' style='width:95%;'>";





		echo "<thead><tr>";
		echo "<th colspan='3' >Anamnese.</th>";
		echo "</tr></thead>";
		echo"<tr><td ><b>Tipo Tratamento:</b></td><td colspan =2><input type='checkbox' value='1' name='clinico'>Cl&iacute;nico<input type='checkbox' value='1'name='cirurgico'>Cir&uacute;rgico<input type='checkbox' value='1' name='paliativos'>Cuidados Paliativos</td></tr>";
		echo"<tr><td ><b>Diagn&oacutestico Principal:</b></td><td colspan =2><textarea cols=100 rows=2 name='diagnostico_principal'   class='OBG' {$acesso} ></textarea></td></tr>";
		echo"<tr><td ><b>Hist&oacute;ria Pregressa:</b></td><td colspan =2><textarea cols=100 rows=2 name='hist_pregressa'   class='OBG' {$acesso} ></textarea></td></tr>";
		echo"<tr><td ><b>Hist&oacute;ria Familiar:</b></td><td colspan =2><textarea cols=100 rows=2 name='hist_familiar'   class='OBG' {$acesso} ></textarea></td></tr>";
		echo"<tr><td ><b>Queixas Atuais:</b></td><td colspan =2><textarea cols=100 rows=2 name='queix_atuais'   class='OBG' {$acesso} ></textarea></td></tr>";



		echo "<input type='hidden' name='capmed'  value='{$y}' />";
		echo "<thead><tr>";
		echo "<th colspan='3' >Medica&ccedil;&otilde;es em uso </th>";
		echo "</tr></thead>";
		if($acesso != "readonly") {
			echo "<tr><td colspan='3' ><input type='text' name='busca-problemas-ativos-evolucao' {$acesso} size=100 /></td></tr>";
			echo"<tr><td colspan='3' ><input type='checkbox' id='outros-prob-ativos-evolucao' name='outros-prob-ativos-evolucao' {$acesso} />Outros.<span class='outro_ativo_evolucao' ><input type='text' name='outro-prob-ativo-evolucao' class='outro_ativo_evolucao' {$acesso} size=75/><button id='add-prob-ativo-evolucao' tipo=''  >+ </button></span></td></tr>";
		}
		echo "<tr><td colspan='3' ><table id='problemas_ativos' style='width:100%;'>";
		if(!empty($ev->ativos)){
			foreach($ev->ativos as $a){
				echo "<tr><td>{$a}</td></tr>";
			}
		}
		echo "</table></td></tr>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Hist&oacute;ria Atual</th>";
		echo "</tr></thead>";
		echo"<tr><td colspan =3><textarea cols=100 rows=2 name='hist_atual'   class='OBG' {$acesso} ></textarea></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3' ><center>Exame F&iacute;sico</center></th>";
		echo "</tr></thead>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Estado Geral</th>";
		echo "</tr></thead>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Estado Emocional</th>";
		echo "</tr></thead>";

		echo "<thead><tr>";
		echo "<th colspan='3' >N&iacute;vel de consci&ecirc;ncia</th>";
		echo "</tr></thead>";

		echo"<thead><tr>";
		echo "<th colspan='2'>Alergia medicamentosa (S/N) <input type='text' class='boleano OBG' name='palergiamed' value='{$dados->alergiamed}' {$acesso} /></th>";
		echo "<th >Alergia alimentar (S/N) <input type='text' class='boleano OBG' name='palergiaalim' value='{$dados->alergiaalim}' {$acesso} /></th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='2'><input type='text' name='alergiamed' size=50 readonly='readonly' value='{$dados->tipoalergiamed}' /></td>";
		echo "<td><input type='text' name='alergiaalim' size=50 readonly='readonly' value='{$dados->tipoalergiaalim}' /></td></tr>";
		//echo"</table>";
		echo "<thead><tr>";
		echo "<th colspan='3' >Pele</th>";
		echo "</tr></thead>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Aspira&ccedil;&otilde;es</th>";
		echo "</tr></thead>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Via alimenta&ccedil;&atilde;o</th>";
		echo "</tr></thead>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Dieta</th>";
		echo "</tr></thead>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Topico em uso</th>";
		echo "</tr></thead>";
		echo"<tr><td colspan =3><textarea cols=100 rows=2 name='topico'   class='OBG' {$acesso} ></textarea></td></tr>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Dispositivo Intravenosos</th>";
		echo "</tr></thead>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Dispositivos para elimina&ccedil;&atilde;o e/ou excre&ccedil;&otilde;es</th>";
		echo "</tr></thead>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Drenos</th>";
		echo "</tr></thead>";
		echo"<tr><td colspan='3' ><b>Drenos: </b><select name=dreno_sn class='COMBO_OBG'><option value =''>  </option>
		<option value ='n'>N&atilde;o</option>
		<option value ='s'>Sim</option></select>&nbsp;&nbsp;<span name='dreno' id='dreno_span'><b>Local:</b><input type='text' name='dreno' class='dreno' {$acesso} size=50/><button id='add-dreno' tipo=''  > + </button></span></td></tr>";

		echo "<tr><td colspan='3' ><table id='dreno' style='width:100%;'>";
		if(!empty($ev->dreno)){
			foreach($ev->dreno as $a){
				echo "<tr><td>{$a}</td></tr>";
			}
		}
		echo "</table></td></tr>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Feridas</th>";
		echo "</tr></thead>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Sinais vitais </th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='3'><b>PA :</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' class='numerico1 OBG' size='6' name='pa_sistolica' value='{$ev->pa_sistolica}' {$acesso} />mmhg</td></tr>";
		echo "<tr><td colspan='3'><b>SPO&sup2;:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' class='numerico1 OBG' size='6' name='pa_diastolica' value='{$ev->pa_diastolica}' {$acesso} /></td></tr>";
		echo "<tr><td colspan='3'><b>FC: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' name='fc' value='{$ev->fc}' {$acesso} />bpm</td></tr>";
		echo "<tr><td colspan='3'><b>FR:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' name='fr' value='{$ev->fr}' {$acesso} />irpm</td></tr>";
		echo "<tr><td colspan='3'><b>Temperatura:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' name='temperatura' value='{$ev->temperatura}' {$acesso} />&deg;C</td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3'>Avalia&ccedil;&atilde;o da dor</th>";
		echo "</thead></tr>";
		echo"<tr><td colspan='3' ><b>DOR: </b><select name=dor_sn class='COMBO_OBG'><option value =''>  </option>
		<option value ='n'>N&atilde;o</option>
		<option value ='s'>Sim</option></select>&nbsp;&nbsp;<span name='dor' id='dor_span'><b>Local:</b><input type='text' name='dor' class='dor' {$acesso} size=50/><button id='add-dor' tipo=''  > + </button></span></td></tr>";

		echo "<tr><td colspan='3' ><table id='dor' style='width:100%;'>";
		if(!empty($ev->dor)){
			foreach($ev->dor as $a){
				echo "<tr><td>{$a}</td></tr>";
			}
		}
		echo "</table></td></tr>";

		 
		echo "<thead><tr>";
		echo "<th colspan='3' >PAD enfermagem</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='3' ><textarea cols=100 rows=5 name='pad'   class='OBG' valeu={$ev->plano_terapeutico} {$acesso} ></textarea></td></tr>";
		echo "</table></form></div>";

		if($acesso != "readonly") {

			//	echo "<button id='teste' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Teste</span></button></span>";
			echo "<button id='salvar-ficha-evolucao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
			echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";

		}

	}


	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function detalhes_ficha_avaliacao_enfermagem($id){

		$id = anti_injection($id,"numerico");
		$sql = "SELECT
		DATE_FORMAT(fev.DATA,'%d/%m/%Y %H:%i:%s') as fdata,
		fev.ID,
		fev.PACIENTE_ID as idpaciente,
		u.nome,
		p.nome as paciente
		FROM
		fichamedicaevolucao as fev LEFT JOIN
		usuarios as u ON (fev.USUARIO_ID = u.idUsuarios) INNER JOIN
		clientes as p ON (fev.PACIENTE_ID = p.idClientes)

		WHERE
		fev.ID = '{$id}'";
		$r = mysql_query($sql);

		while($row = mysql_fetch_array($r)){
			$usuario =ucwords(strtolower($row["nome"]));
			$data = $row['fdata'];
			$paciente=$row['idpaciente'];
		}

		echo "<center><h1>Detalhes Ficha de Evolu&ccedil;&atilde;o M&eacute;dica </h1></center>";

		$this->cabecalho($paciente);
		echo "<center><h2>Respons&aacute;vel: {$usuario} - Data: {$data}</h2></center>";
		//echo "<a href=imprimir_ficha.php?id={$ev->capmedica}>imprimir</a></br>";
		echo"<form method=post target='_blank' action='imprimir_ficha_evolucao.php?id={$id}'>";
		echo"<input type=hidden value={$paciente} name='paciente'</input>";
		echo "<button id='imprimir_ficha_evolucao' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
				<span class='ui-button-text'>Imprimir</span></button>";
		echo "</form>";


		echo "<div id='div-ficha-evolucao-medica'><br/>";

		echo alerta();
		echo "<form>";
		echo "<input type='hidden' name='paciente'  value='{$ev->pacienteid}' />";
		echo "<br/><table class='mytable' style='width:100%;'>";

		echo "<thead><tr>";
		echo "<th colspan='3' >Problemas Ativos.</th>";
		echo "</tr></thead>";

		$sql2="select pae.DESCRICAO as nome, pae.CID_ID,(CASE pae.STATUS
		WHEN '1' THEN 'RESOLVIDO'
		WHEN '2' THEN 'MELHOR'
		WHEN '3' THEN 'EST&Aacute;VEL'
		WHEN '4' THEN 'PIOR' END) as sta , pae.STATUS,pae.OBSERVACAO from problemaativoevolucao as pae where pae.EVOLUCAO_ID = {$id} ";
		$result2 = mysql_query($sql2);
		$cont=1;

		while($row = mysql_fetch_array($result2)){
			if($row['STATUS'] != 0){
				echo "<tr><td cid={$row['cid']} prob_atv='{$row['nome']}' x='{$cont}' class='probativo1' style='width:20%'><b>P{$cont} - {$row['nome']}</b> </td><td colspan='2'><b>Estado:</b> {$row['sta']}</td></tr>";
				if( $row['STATUS'] != 1){
					echo"<tr><td colspan='3'><b>OBSERVA&Ccedil;&Atilde;O: </b>{$row['OBSERVACAO']}</td></tr>";
				}
				$cont++;
			}
		}


		echo "<input type='hidden' name='capmed'  value='{$y}' />";
		echo "<thead><tr>";
		echo "<th colspan='3' >Novos Problemas </th>";
		echo "</tr></thead>";
		$sql2="select pae.DESCRICAO as nome, pae.CID_ID, pae.STATUS, pae.OBSERVACAO from problemaativoevolucao as pae where pae.EVOLUCAO_ID = {$id} ";
		$result2 = mysql_query($sql2);

		while($row = mysql_fetch_array($result2)){
			if($row['STATUS'] == 0){

				echo"<tr><td colspan=''><b>- {$row['nome']} </b><td colspan='2'> <b>OBS:</b> {$row['OBSERVACAO']}</td></tr>";

			}
		}


		$sql= "select * from fichamedicaevolucao where ID = {$id}";
		$result=mysql_query($sql);
		while($row = mysql_fetch_array($result)){

			$pa_sistolica_min =$row['PA_SISTOLICA_MIN'];
			$pa_sistolica_max =$row['PA_SISTOLICA_MAX'];
			$pa_sistolica =$row['PA_SISTOLICA'];
			$pa_sistolica_sinal =$row['PA_SISTOLICA_SINAL'];
			$pa_diastolica_min = $row['PA_DIASTOLICA_MIN'];
			$pa_diastolica_max = $row['PA_DIASTOLICA_MAX'];
			$pa_diastolica  = $row['PA_DIASTOLICA'];
			$pa_diastolica_sinal = $row['PA_DIASTOLICA_SINAL'];
			$fc =$row["FC"];
			$fc_max =$row["FC_MAX"];
			$fc_min =$row["FC_MIN"];
			$fc_sinal =$row["FC_SINAL"];
			$fr =$row["FR"];
			$fr_max =$row["FR_MAX"];
			$fr_min =$row["FR_MIN"];
			$fr_sinal =$row["FR_SINAL"];
			$temperatura_max = $row["TEMPERATURA_MAX"];
			$temperatura_min = $row["TEMPERATURA_MIN"];
			$temperatura = $row["TEMPERATURA"];
			$temperatura_sinal = $row["TEMPERATURA_SINAL"];
			$estd_geral = $row["ESTADO_GERAL"];
			$mucosa = $row['MUCOSA'];
			$mucosaobs = $row['MUCOSA_OBS'];
			$escleras = $row['ESCLERAS'];
			$esclerasobs = $row['ESCLERAS_OBS'];
			$respiratorio = $row['PADRAO_RESPIRATORIO'];
			$respiratorioobs = $row['PADRAO_RESPIRATORIO_OBS'];
			$novo_exame = $row['NOVOS_EXAMES'];
			$impressao = $row['IMPRESSAO'];
			$diagnostico= $row['PLANO_DIAGNOSTICO'];
			$terapeutico= $row['PLANO_TERAPEUTICO'];
			$dor_sn= $row['DOR'];

		}


		echo "<thead><tr>";
		echo "<th colspan='3' >Evolu&ccedil;&atilde;o dos sinais vitais (Registro da semana)</th>";
		echo "</tr></thead>";
		echo "<tr><td><b>PA (sist&oacute;lica) </b></td><td>min:<input type='texto' readonly class='numerico1 OBG' name='pa_sistolica_min' value='{$pa_sistolica_min}' readonly size='4' /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='pa_sistolica_max' value='{$pa_sistolica_max}' readonly  readonly  size='4' /></td>";
		if($pa_sistolica_sinal == 1){
			$x="AMARELO";
		}

		if($pa_sistolica_sinal == 2){
			$x="VERMELHO";
		}
		echo "<td><b>ALERTA:</b> {$x} </td>";
		echo "</tr>";
		echo "<tr><td><b>PA (diast&oacute;lica) </b> </td><td> min:<input type='texto' class='numerico1 OBG' name='pa_diastolica_min' value='{$pa_diastolica_min}' readonly  size='4'  /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='pa_diastolica_max' value='{$pa_diastolica_max}' size='4' readonly/></td>";
		$x='';
		if($pa_diastolica_sinal == 1){
			$x="AMARELO";
		}
		if($pa_diastolica_sinal == 2){
			$x="VERMELHO";
		}
		echo "<td><b>ALERTA:</b> {$x}</td>";
		echo "</tr>";
		echo "<tr><td><b>FC </b> </td><td>min:<input type='texto' class='numerico1 OBG' name='fc_min' value='{$fc_min}'  size='4' readonly /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='fc_max' value='{$fc_max}' readonly size='4' /></td>";
		$x='';
		if($fc_sinal == 1){
			$x="AMARELO";
		}
		if($fc_sinal == 2){
			$x="VERMELHO";
		}
		echo "<td><b>ALERTA:</b> {$x}</td>";
		echo "</tr>";
		echo "<tr><td><b>Fr </b> </td><td>min:<input type='texto' class='numerico1 OBG' name='fr_min' value='{$fr_min}' size='4' readonly /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='fr_max' value='{$fr_max}' size='4'readonly /></td>";
		$x='';
		if($fr_sinal == 1){
			$x="AMARELO";
		}
		if($fr_sinal == 2){
			$x="VERMELHO";
		}
		echo "<td><b>ALERTA:</b> {$x}</td>";
		echo "</tr>";
		echo "<tr><td width=20% ><b>Temperatura: </b></td><td width=30% >min:<input type='texto' class='numerico1 OBG' name='temperatura_min' size='4' value='{$temperatura_min}' readonly size='4' /> m&aacute;x:
		<input type='texto' class='numerico1 OBG' name='temperatura_max' size='4'value='{$temperatura_max}' readonly size='4' /></td>";
		$x='';
		if($ptemperatura_sinal == 1){
			$x="AMARELO";
		}
		if($temperatura_sinal == 2){
			$x="VERMELHO";
		}
		echo "<td><b>ALERTA:</b> {$x}</td>";
		echo "</tr>";
		echo "<thead><tr>";
		echo "<th colspan='3'>Exame F&iacute;sico</th>";
		$x='';
		$y1='';
		if($estd_geral == 1){
			$x="BOM";
		}
		if($estd_geral == 2){
			$x="REGULAR";
		}
		if($estd_geral == 3){
			$x="RUIM";
		}

		echo "<tr><td><b>Estado Geral:</b>  </td><td colspan='2'>{$x}</td></tr>";
		$x='';
		$y1='';
		if ($mucosa =='n'){
			$x= 'DESCORADAS';
			$y1= $mucosaobs;
		}
		if ($mucosa =='s'){
			$x= 'Coradas';
			$y1= $mucosaobs;
		}
		echo "<tr><td><b>Mucosa:</b>  </td><td colspan='2'>{$x}  {$y1}</td></tr>";
		$x='';
		if ($escleras =='n'){
			$x= 'Ict&eacute;ricas';
			$y1= $esclerasobs;
		}
		if ($escleras =='s'){
			$x= 'Anict&eacute;ricas';
			$y1= $esclerasobs;
		}
		echo "<tr><td><b>Escleras:</b>  </td><td colspan='2'>{$x}  {$y1}</td></tr>";
		$x='';
		$y1='';
		if ($respiratorio =='n'){
			$x= 'Taqui/Dispn&eacute;ico';
			$y1= $respiratorioobs;
		}
		if ($respiratorio =='s'){
			$x= 'Eupn&eacute;ico';
			$y1= $respiratorioobs;
		}
		echo "<tr><td><b>Padr&atilde;o Respirat&oacute;:</b>  </td><td colspan='2'>{$x}  {$y1}</td></tr>";
		echo "<tr><td colspan='3'><b>PA (sist&oacute;lica):</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' class='numerico1 OBG' size='6' name='pa_sistolica' value='{$pa_sistolica}'  readonly {$acesso} />mmhg</td></tr>";
		echo "<tr><td colspan='3'><b>PA (diast&oacute;lica):</b>&nbsp;&nbsp;&nbsp;<input type='texto' class='numerico1 OBG' size='6' name='pa_diastolica' value='{$pa_diastolica}' readonly {$acesso} />mmhg</td></tr>";
		echo "<tr><td colspan='3'><b>FC: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' name='fc' readonly value='{$fc}' {$acesso} />bpm</td></tr>";
		echo "<tr><td colspan='3'><b>FR:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' readonly name='fr' value='{$fr}' {$acesso} />irpm</td></tr>";
		echo "<tr><td colspan='3'><b>Temperatura:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='texto' size='6' class='numerico1 OBG' name='temperatura' value='{$temperatura}' readonly {$acesso} />&deg;C</td></tr>";
		if($dor_sn == 's'){
			$dor_sn = "Sim";
		}
		if($dor_sn == 'n'){
			$dor_sn = "N&atilde;o";
		}
		echo"<tr><td colspan='3' ><b>DOR:</b>{$dor_sn}</td></tr>";

		echo "<tr><td colspan='3' ><table id='dor' style='width:100%;'>";
		$sql = "select * from dorfichaevolucao where FICHAMEDICAEVOLUCAO_ID = {$id} ";
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result)){
			echo "<tr><td> <b>Local:</b> {$row['DESCRICAO']}&nbsp;&nbsp; <b>Escala visual:</b> {$row['ESCALA']}&nbsp;&nbsp;<b>Padr&atilde;o:</b>{$row['PADRAO']}&nbsp;&nbsp; </td></tr>";

		}


		echo "</table></td></tr>";

		echo "<thead><tr>";
		echo "<th colspan='3' >CATETERS e SONDAS </th>";
		echo "</tr></thead>";
		$sql = "select DESCRICAO from catetersondaevolucao where FICHA_EVOLUCAO_ID = {$id}";
		$result= mysql_query($sql);
		while ($row= mysql_fetch_array($result)){

			echo "<tr><td colspan='3'><b>- {$row['DESCRICAO']}<b></td></tr>";
		}

		echo "<thead><tr>";
		echo "<th colspan='3' >Exame segmentar</th>";
		echo "</tr></thead>";
		$sql="select exv.*,(CASE exv.AVALIACAO
		WHEN '1' THEN 'NORMAL'
		WHEN '2' THEN 'ALTERADO'
		WHEN '3' THEN 'N EXAMINADO' END) as av,ex.DESCRICAO from examesegmentarevolucao as exv LEFT JOIN examesegmentar as ex ON (exv.EXAME_SEGMENTAR_ID = ex.ID) where exv.FICHA_EVOLUCAO_ID = {$id}";
		$result=mysql_query($sql);
		while($row = mysql_fetch_array($result)){

			echo "<tr><td idexame={$row['ID']} class='exame1'> <b>{$row['DESCRICAO']}</b></td><td colspan='2'>{$row['av']}</td></tr>";
			if($row['AVALIACAO'] == 2 || $row['AVALIACAO'] == 3){
				echo"<tr><td colspan='3'><b>OBS: </b>{$row['OBSERVACAO']}</td></tr>";
			}
		}
		echo "<thead><tr>";
		echo "<th colspan='3' >Exames Complementares Novos</th>";
		echo "</tr></thead>";
		echo "<tr><td colspan='3' ><b>{$novo_exame} </b></td></tr>";
		echo "<thead><tr>";
		echo "<th colspan='3' >Impress&atilde;o</th>";
		echo "</tr></thead>";
		echo "<thead><tr>";
		echo "<tr><td colspan='3' ><b>{$impressao} </b></td></tr>";
		echo "<th colspan='3' >Plano Diagn&oacute;stico</th>";
		echo "</tr></thead>";
		echo "<thead><tr>";
		echo "<tr><td colspan='3' ><b>{$diagnostico} </b></td></tr>";
		echo "<th colspan='3' >Plano Teraup&ecirc;utico</th>";
		echo "</tr></thead>";
		echo "<thead><tr>";
		echo "<tr><td colspan='3' ><b>{$terapeutico}</b></td></tr>";




		echo "</table></form></div>";
		echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";


	}
	////////////////////////////////////////

	public function listar_ficha_avaliacao_enfermagem($id){
		$id = anti_injection($id,"numerico");

		/*$sql3="select Max(id) as ev from fichamedicaevolucao where PACIENTE_ID = '{$id}' ";
		 $r2=mysql_query($sql3);

		$maior_ev= mysql_result($r2,0);


		$sql = "SELECT
		DATE_FORMAT(fev.DATA,'%d/%m/%Y %H:%i:%s') as fdata,
		fev.ID,
		fev.PACIENTE_ID as idpaciente,
		u.nome,
		p.nome as paciente
		FROM
		fichamedicaevolucao as fev LEFT JOIN
		usuarios as u ON (fev.USUARIO_ID = u.idUsuarios) INNER JOIN
		clientes as p ON (fev.PACIENTE_ID = p.idClientes)

		WHERE
		fev.PACIENTE_ID = '{$id}'
		ORDER BY
		fev.DATA DESC";
		$r = mysql_query($sql);
		$flag = true;
		while($row = mysql_fetch_array($r)){
		if($flag) {

		$p = ucwords(strtolower($row["fev.PACIENTE_ID"]));
		echo "<center><h1>Lista das Fichas de Evolu&ccedil;&atilde;o M&eacute;dica </h1></center>";
		$this->cabecalho($id);
		echo "<br/><br/>";

		echo "<div><input type='hidden' name='nova_ficha_evolucao' value='{$id}'></input>";

		echo "<button id='nova_ficha_evolucao'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
		<span class='ui-button-text'>Nova ficha</span></button></div>";

		echo "<br/><table class='mytable' width='100%' >";
		echo "<thead><tr>";
		echo "<th width='55%'>Usu&aacute;rio</th>";
		echo "<th>Data</th>";
		echo "<th width='15%'>Detalhes</th>";

		echo "</tr></thead>";
		$flag = false;
		}


		$u = ucwords(strtolower($row["nome"]));

		echo "<tr><td>{$u}</td><td>{$row['fdata']}</td>";
		echo "<td><a href=?adm=paciente&act=fichaevolucao&id={$row['ID']}><img src='../utils/capmed_16x16.png' title='Detalhes Ficha Evolu&ccedil;&atilde;o' border='0'></a></td>";


		///mudou ak
		echo "</tr>";

		}*/


		//if($flag){
		$this->nova_ficha_avaliacao_enfermagem($id);


		// return;
		// }


		// echo "</table>";
	}



	///////////////////////////////FIM FICHA AVALIACAO EM FERMAGEM///////////////////
        
       
}



?>