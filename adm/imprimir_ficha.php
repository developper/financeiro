<?php



$id = session_id();
if(empty($id))
  session_start();
include_once('../validar.php');

include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');





class DadosFicha{

	public $usuario = ""; public $data = ""; public $pacienteid = ""; public $pacientenome = ""; public $motivo = "";
	public $alergiamed = ""; public $alergiaalim = ""; public $tipoalergiamed = ""; public $tipoalergiaalim = "";
	public $prelocohosp = "3"; public $prehigihosp = "2"; public $preconshosp = "3"; public $prealimhosp = "4"; public $preulcerahosp = "2";
	public $prelocodomic = "3"; public $prehigidomic = "2"; public $preconsdomic = "3"; public $prealimdomic = "4"; public $preulceradomic = "2";
	public $objlocomocao = "3"; public $objhigiene = "2"; public $objcons = "3"; public $objalimento = "4"; public $objulcera = "2";
	public $shosp = "14"; public $sdom = "14"; public $sobj = "14";
	public $modalidade="2";
	public $local_av="2";

	public $qtdinternacoes = ""; public $historico = "";
	public $parecer = "0"; public $justificativa = "";

	public $cancer = ""; public $psiquiatrico = ""; public $neuro = ""; public $glaucoma = "";
	public $hepatopatia = ""; public $obesidade = ""; public $cardiopatia = ""; public $dm = "";
	public $reumatologica = ""; public $has = ""; public $nefropatia = ""; public $pneumopatia = "";

	public $ativo;
	public $idcapmedica="";
	public $pre_justificativa="";
	public $plano_desmame="";
	public $usuarioid="";
}


class Paciente_imprimir_ficha{
public function detalhes_capitacao_medica($id){
	
	$id = $id;
	
	$html.= $id;
	$sql = "SELECT cap.*, pac.nome as pnome,pac.idClientes,usr.nome as unome, DATE_FORMAT(cap.data,'%d/%m/%Y') as fdata, comob.*
	FROM capmedica as cap
	LEFT OUTER JOIN comorbidades as comob ON cap.id = comob.capmed
	LEFT OUTER JOIN problemasativos as prob ON cap.id = prob.capmed
	LEFT OUTER JOIN usuarios as usr ON cap.usuario = usr.idUsuarios
	LEFT OUTER JOIN clientes as pac ON cap.paciente = pac.idClientes

	WHERE cap.id = '{$id}'";
	
	$d = new DadosFicha();
	
	$d->capmedica = $id;
	$result = mysql_query($sql);
	
	
	while($row = mysql_fetch_array($result)){
	$d->pacienteid= $row['idClientes'];
	$d->usuarioid= $row['usuario'];
	
	$d->usuario = ucwords(strtolower($row["unome"])); $d->data = $row['fdata']; $d->pacientenome = ucwords(strtolower($row["pnome"]));
	$d->motivo = $row['motivo'];
	$d->alergiamed = $row['alergiamed']; $d->alergiaalim = $row['alergiaalim'];
	$d->tipoalergiamed = $row['tipoalergiamed']; $d->tipoalergiaalim = $row['tipoalergiaalim'];
	$d->prelocohosp = $row['hosplocomocao']; $d->prehigihosp = $row['hosphigiene'];
	$d->preconshosp = $row['hospcons']; $d->prealimhosp = $row['hospalimentacao']; $d->preulcerahosp = $row['hospulcera'];
	$d->prelocodomic = $row['domlocomocao']; $d->prehigidomic = $row['domhigiene']; $d->preconsdomic = $row['domcons'];
	$d->prealimdomic = $row['domalimentacao']; $d->preulceradomic = $row['domulcera'];
	$d->objlocomocao = $row['objlocomocao']; $d->objhigiene = $row['objhigiene']; $d->objcons = $row['objcons'];
	$d->objalimento = $row['objalimentacao']; $d->objulcera = $row['objulcera'];
	$d->shosp = $d->prelocohosp + $d->prehigihosp +	$d->preconshosp + $d->prealimhosp + $d->preulcerahosp;
	$d->sdom = $d->prelocodomic + $d->prehigidomic +	$d->preconsdomic + $d->prealimdomic + $d->preulceradomic;;
	$d->sobj = $d->objlocomocao + $d->objhigiene + $d->objcons + $d->objalimento + $d->objulcera;

	$d->qtdinternacoes = $row['qtdinternacoes']; $d->historico = $row['historicointernacao'];
	$d->parecer = $row['parecer']; $d->justificativa = $row['justificativa'];

	$d->cancer = $row['cancer']; $d->psiquiatrico = $row['psiquiatrico']; $d->neuro = $row['neuro']; $d->glaucoma = $row['glaucoma'];
	$d->hepatopatia = $row['hepatopatia']; $d->obesidade = $row['obesidade']; $d->cardiopatia = $row['cardiopatia']; $d->dm = $row['dm'];
	$d->reumatologica = $row['reumatologica']; $d->has = $row['has']; $d->nefropatia = $row['nefropatia']; $d->pneumopatia = $row['pneumopatia'];
	$d->modalidade = $row['MODALIDADE']; $d->local_av = $row['LOCAL_AV']; $d->pre_justificativa = $row['PRE_JUSTIFICATIVA']; $d->plano_desmame = $row['PLANO_DESMAME'];
	
}


$sql = "SELECT p.DESCRICAO as nome FROM problemasativos as p  WHERE  p.capmed = '{$id}' ";
$result = mysql_query($sql);
while($row = mysql_fetch_array($result)){
$d->ativos[] = $row['nome'];
}



$this->cabecalho($d);
$this->capitacao_medica($d,"readonly");


}


public function cabecalho($dados){
	if(isset($dados->pacienteid))
		$id = $dados->pacienteid;
	else
		$id = $dados->pacienteid;
	$condp1= "c.idClientes = '{$id}'";
	$sql2 = "SELECT
	UPPER(c.nome) AS paciente,
	(CASE c.sexo
	WHEN '0' THEN 'Masculino'
	WHEN '1' THEN 'Feminino'
	END) AS nomesexo,
	FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
	e.nome as empresan,
	c.`nascimento`,
	p.nome as Convenio,p.id,c.*,(CASE c.acompSolicitante
	WHEN 's' THEN 'Sim'
	WHEN 'n' THEN 'N&atilde;o'
	END) AS acomp,
	NUM_MATRICULA_CONVENIO
	FROM
	clientes AS c LEFT JOIN
	empresas AS e ON (e.id = c.empresa) INNER JOIN
	planosdesaude as p ON (p.id = c.convenio)
	WHERE
	{$condp1}
	ORDER BY
	c.nome DESC LIMIT 1";

	$result = mysql_query($sql);
	$result2 = mysql_query($sql2);
	$html.= "<table style='width:100%;' >";
	while($pessoa = mysql_fetch_array($result2)){
	foreach($pessoa AS $chave => $valor) {
	$pessoa[$chave] = stripslashes($valor);

	}
	

	$html.= "<tr bgcolor='#EEEEEE'>";
	$html.= "<td  style='width:100%;'><b>PACIENTE:</b></td>";
	$html.= "<td ><b>SEXO </b></td>";
	$html.= "<td style='width:100%;'><b>IDADE:</b></td>";
	$html.= "</tr>";
	$html.= "<tr>";
	$html.= "<td style='width:100%;'>{$pessoa['paciente']}</td>";
	$html.= "<td>{$pessoa['nomesexo']}</td>";
	$html.= "<td>".join("/",array_reverse(explode("-",$pessoa['nascimento']))).' ('.$pessoa['idade']." anos)</td>";
	$html.= "</tr>";

	$html.= "<tr bgcolor='#EEEEEE'>";
	
	$html.= "<td ><b>UNIDADE REGIONAL:</b></td>";
	$html.= "<td style='width:100%;'><b>CONV&Ecirc;NIO </b></td>";
	$html.= "<td><b>MATR&Iacute;CULA </b></td>";
	$html.= "</tr>";
	$html.= "<tr>";
	
	$html.= "<td>{$pessoa['empresan']}</td>";
	$html.= "<td>".strtoupper($pessoa['Convenio'])."</td>";
	$html.= "<td>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
	$html.= "</tr>";

	$html.= "<tr bgcolor='#EEEEEE'>";
	$html.= "<td><b>M&Eacute;DICO ASSISTENTE:</b></td>";
	$html.= "<td><b>ESPECIALIDADE:</b></td>";
	$html.= "<td><b>ACOMPANHARA O PACIENTE (Sim ou N&atilde;o)</b></td>";
	$html.= "</tr>";
	$html.= "<tr>";
	$html.= "<td>{$pessoa['medicoSolicitante']}</td>";
	$html.= "<td>{$pessoa['espSolicitante']}</td>";
	$html.= "<td>{$pessoa['acomp']}</td>";
	$html.= "</tr>";
	$html.= "<tr bgcolor='#EEEEEE'>";
	$html.= "<td><b>UNIDADE DE INTERNA&Ccedil;&Atilde;O/HOSPITAL</b></td>";
	$html.= "<td><b>TELEFONE:</b></td>";
	$html.= "<td><b>TELEFONE REPONSAVEL:</b></td>";
	$html.= "</tr>";
	$html.= "<tr>";
	$html.= "<td>{$pessoa['localInternacao']}</td>";
	$html.= "<td>{$pessoa['TEL_DOMICILIAR']}</td>";
	$html.= "<td>{$pessoa['TEL_RESPONSAVEL']}</td>";
	$html.= "</tr>";
	
	
	
			$this->plan =  $pessoa['id'];
}
$html.= "</table>";

return $html;
}

public function equipamentos($id=null){

	$html.= "<tr bgcolor='#D3D3D3'>";
	$html.= "<td colspan='3' ><b>Equipamentos</b></td>";
	$html.= "</tr>";
	/*if($id==null){
		$sql= "select * from cobrancaplanos where tipo=1";

		$result=mysql_query($sql);

		$html.="<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
		while($row=mysql_fetch_array($result)){
			$html.="<tr><td><input type='checkbox' class= equipamento cod_equipamento='{$row['id']}' ></input>{$row['item']}</td><td><input style='width:100%' type='texto' class='equipamento' id=obs"."{$row['id']} readonly name='obs_equpipamento'  {$acesso} ></input></td>
			<td><input type='texto' readonly id=qtd"."{$row['id']}   class='qtd_equipamento'   name='qtd_equipamento'  {$acesso} ></input></td></tr>";
}
}else{*/

$sql = "SELECT
C.id,
C.item,
C.unidade,
C.tipo,
eqp.QTD,
eqp.OBSERVACAO
FROM
equipamentos_capmedica as eqp INNER JOIN
cobrancaplanos as C ON (C.id = eqp.COBRANCAPLANOS_ID)

WHERE
eqp.CAPMEDICA_ID = ".$id;


$result = mysql_query($sql);

$html.="<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
while($row=mysql_fetch_array($result)){
$html.="<tr><td>{$row['item']}</td><td><input type='texto' style='width:100%' value='{$row['OBSERVACAO']}' class='equipamento' id=obs"."{$row['id']} readonly='readonly' name='obs_equpipamento'  {$acesso} ></input></td>
<td><input type='texto' readonly='readonly' id=qtd"."{$row['id']} value='{$row['QTD']}'  class='qtd_equipamento'   name='qtd_equipamento'  {$acesso} ></input></td></tr>";


}
return $html;

}




public function cond_pre_ihosp($d,$enabled){

	$html.= "<tr style='width:100% ' bgcolor='#D3D3D3'>";
	//$score = "SCORE<input type='text' size='2' name='shosp' value='{$dados->shosp}' {$acesso} />";
	$html.= "<td style='width:100% '  colspan='3' ><b>Condi&ccedil;&otilde;es do paciente pr&eacute;-interna&ccedil;&atilde;o hospitalar. Score = {$dados->shosp}. </b></td>";
	$html.= "</tr>";
	$html.= "<tr><td colspan='3' ><b>Locomo&ccedil;&atilde;o:</b>";
	if($d->prelocohosp=='3'){
		$html.= " &nbsp; <b>(x)</b> Deambulava&nbsp;&nbsp;";
		$html.= " <b>( )</b>Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)&nbsp;&nbsp;";
		$html.= " <b>( )</b>Restrito ao leito";
		$html.= "</td></tr>";
	}
	if($d->prelocohosp=='2'){
		$html.= " &nbsp; <b>( )</b> Deambulava&nbsp;&nbsp;";
		$html.= " <b>(x)</b>Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)&nbsp;&nbsp;";
		$html.= " <b>( )</b>Restrito ao leito";
		$html.= "</td></tr>";
	}
	if($d->prelocohosp=='1'){
		
		$html.= " &nbsp; <b>( )</b> Deambulava&nbsp;&nbsp;";
		$html.= " <b>(x)</b>Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)&nbsp;&nbsp;";
		$html.= " <b>( )</b>Restrito ao leito";
		$html.= "</td></tr>";
	}
		$html.= "<tr><td colspan='3' ><b>Autonomia para higiene pessoal:</b>";
	if($d->prehigihosp=='2'){
		$html.= " &nbsp; <b>(x)</b>Sim&nbsp;&nbsp;";
		$html.= "  <b>( )</b>N&atilde;o (Oral, deje&ccedil;&otilde;es, mic&ccedil&atilde;o e banho)";
		$html.= "</td></tr>";
	}
	if($d->prehigihosp=='1'){
		$html.= " &nbsp; <b>( )</b>Sim&nbsp;&nbsp;";
		$html.= "  <b>(x)</b>N&atilde;o (Oral, deje&ccedil;&otilde;es, mic&ccedil&atilde;o e banho)";
		$html.= "</td></tr>";
	}
	
		$html.= "<tr><td colspan='3' ><b>N&iacute;vel de consci&ecirc;ncia:</b>";
	if($dados->preconshosp=='3'){
		$html.= "&nbsp;<b>(x)</b>LOTE&nbsp;&nbsp;";
		$html.= "<b>( )</b>Desorientado&nbsp;&nbsp;";
		$html.= "<b>( )</b>Inconsciente";
		$html.= "</td></tr>";
	}
	if($dados->preconshosp=='2'){
		$html.= "&nbsp;<b>( )</b>LOTE&nbsp;&nbsp;";
		$html.= "<b>(x)</b>Desorientado&nbsp;&nbsp;";
		$html.= "<b>( )</b>Inconsciente";
		$html.= "</td></tr>";
	}
		
		
	if($dados->preconshosp=='1'){
		$html.= "&nbsp;<b>( )</b>LOTE&nbsp;&nbsp;";
		$html.= "<b>( )</b>Desorientado&nbsp;&nbsp;";
		$html.= "<b>(x)</b>Inconsciente";
		$html.= "</td></tr>";
	}
		
	
		$html.= "<tr><td colspan='3' ><b>Alimenta&ccedil;&atilde;o:</b>";
	if($dados->prealimhosp=='4'){
		$html.= " &nbsp;<b>(x)</b>N&atilde;o assistida&nbsp;&nbsp;";
		$html.= " <b>( )</b>Assistida oral&nbsp;&nbsp;";
		$html.= " <b>( )</b>Enteral&nbsp;&nbsp;";
		$html.= " <b>( )</b>Parenteral";
		$html.= "</td></tr>";
	}
    if($dados->prealimhosp=='3'){
		$html.= " &nbsp;<b>( )</b>N&atilde;o assistida&nbsp;&nbsp;";
		$html.= " <b>(x)</b>Assistida oral&nbsp;&nbsp;";
		$html.= " <b>( )</b>Enteral&nbsp;&nbsp;";
		$html.= " <b>( )</b>Parenteral";
		$html.= "</td></tr>";
    }
		

	if($dados->prealimhosp=='2'){
		$html.= " &nbsp;<b>( )</b>N&atilde;o assistida&nbsp;&nbsp;";
		$html.= " <b>( )</b>Assistida oral&nbsp;&nbsp;";
		$html.= " <b>(x)</b>Enteral&nbsp;&nbsp;";
		$html.= " <b>( )</b>Parenteral";
		$html.= "</td></tr>";
          }
	 
	if($dados->prealimhosp=='1'){
		$html.= " &nbsp;<b>( )</b>N&atilde;o assistida&nbsp;&nbsp;";
		$html.= " <b>( )</b>Assistida oral&nbsp;&nbsp;";
		$html.= " <b>( )</b>Enteral&nbsp;&nbsp;";
		$html.= " <b>(x)</b>Parenteral";
		$html.= "</td></tr>";
              }
		$html.= "<tr><td colspan='3' ><b>&Uacute;lceras de press&atilde;o:</b>";
	if($dados->preulcerahosp=='2'){
		$html.= " &nbsp;<b>(x)</b>N&atilde;o&nbsp;&nbsp;";
		$html.= "<b>( )</b>Sim";
		$html.= "</td></tr>";
             }
	if($dados->preulcerahosp=='1'){
		$html.= " &nbsp;<b>( )</b>N&atilde;o&nbsp;&nbsp;";
		$html.= "<b>(x)</b>Sim";
		$html.= "</td></tr>";
		
               }
	return $html;

}

public function cond_pre_id($dados,$enabled){
	
$html.= "<tr  style='width:100% 'bgcolor='#D3D3D3'>";
//$score = "SCORE<input type='text' readonly size='2' name='sdom' value='{$dados->sdom}' {$acesso} />";
$html.= "<td  colspan='3'><b>Condi&ccedil;&otilde;es do paciente pr&eacute;-interna&ccedil;&atilde;o domiciliar. Score = {$dados->sdom}. </b></td>";
$html.= "</tr>";
$html.= "<tr><td colspan='3' ><b>Locomo&ccedil;&atilde;o:</b>";
if($dados->prelocodomic == '3'){
	$html.= " &nbsp; <b>(x)</b>Deambulava&nbsp;&nbsp;";
	$html.= "<b>( )</b>Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)&nbsp;&nbsp;";
	$html.= "<b>( )</b>Restrito ao leito";
	$html.= "</td></tr>";
}
if($dados->prelocodomic == '2'){
	$html.= " &nbsp; <b>( )</b>Deambulava&nbsp;&nbsp;";
	$html.= "<b>(x)</b>Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)&nbsp;&nbsp;";
	$html.= "<b>( )</b>Restrito ao leito";
	$html.= "</td></tr>";
}	
if($dados->prelocodomic == '1')	{
	$html.= " &nbsp; <b>( )</b>Deambulava&nbsp;&nbsp;";
	$html.= "<b>( )</b>Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)&nbsp;&nbsp;";
	$html.= "<b>(x)</b>Restrito ao leito";
	$html.= "</td></tr>";
}

$html.= "<tr><td colspan='3' ><b>Autonomia para higiene pessoal:</b>";
if($dados->prehigidomic=='2'){
	$html.= "  &nbsp; <b>(x)</b>Sim &nbsp;&nbsp;";
	$html.= "  <b>( )</b>N&atilde;o (Oral, deje&ccedil;&otilde;es, mic&ccedil&atilde;o e banho)";
	$html.= "</td></tr>";
}
if($dados->prehigidomic=='1'){
	$html.= "  &nbsp; <b>( )</b>Sim &nbsp;&nbsp;";
	$html.= "  <b>(x)</b>N&atilde;o (Oral, deje&ccedil;&otilde;es, mic&ccedil&atilde;o e banho)";
	$html.= "</td></tr>";
$html.= "<tr><td colspan='3' ><b>N&iacute;vel de consci&ecirc;ncia:</b>";
}
if($dados->preconsdomic=='3'){
	$html.= " &nbsp; <b>(x)</b>LOTE&nbsp;&nbsp;";
	$html.= " <b>( )</b>Desorientado&nbsp;&nbsp;";
	$html.= " <b>( )</b>Inconsciente";
	$html.= "</td></tr>";
}
if($dados->preconsdomic=='2'){
	$html.= " &nbsp; <b>( )</b>LOTE&nbsp;&nbsp;";
	$html.= " <b>(x)</b>Desorientado&nbsp;&nbsp;";
	$html.= " <b>( )</b>Inconsciente";
	$html.= "</td></tr>";
}
	
if($dados->preconsdomic=='1'){
	$html.= " &nbsp; <b>( )</b>LOTE&nbsp;&nbsp;";
	$html.= " <b>( )</b>Desorientado&nbsp;&nbsp;";
	$html.= " <b>(x)</b>Inconsciente";
	$html.= "</td></tr>";
}




$html.= "<tr><td colspan='3' ><b>Alimenta&ccedil;&atilde;o:</b>";
if($dados->prealimdomic=='4'){
	$html.= " &nbsp;<b>(x)</b>N&atilde;o assistida.&nbsp;&nbsp;";
	$html.= " <b>( )</b>Assistida oral.&nbsp;&nbsp;";
	$html.= " <b>( )</b>Enteral.&nbsp;&nbsp;";
	$html.= " <b>( )</b>Parenteral";
	$html.= "</td></tr>";
}
if($dados->prealimdomic=='3'){
	$html.= " &nbsp;<b>( )</b>N&atilde;o assistida.&nbsp;&nbsp;";
	$html.= " <b>(x)</b>Assistida oral.&nbsp;&nbsp;";
	$html.= " <b>( )</b>Enteral.&nbsp;&nbsp;";
	$html.= " <b>( )</b>Parenteral";
	$html.= "</td></tr>";
}
if($dados->prealimdomic=='2'){
	$html.= " &nbsp;<b>( )</b>N&atilde;o assistida.&nbsp;&nbsp;";
	$html.= " <b>( )</b>Assistida oral.&nbsp;&nbsp;";
	$html.= " <b>(x)</b>Enteral.&nbsp;&nbsp;";
	$html.= " <b>( )</b>Parenteral";
	$html.= "</td></tr>";
}
if($dados->prealimdomic=='1'){
	$html.= " &nbsp;<b>( )</b>N&atilde;o assistida.&nbsp;&nbsp;";
	$html.= " <b>( )</b>Assistida oral.&nbsp;&nbsp;";
	$html.= " <b>( )</b>Enteral.&nbsp;&nbsp;";
	$html.= " <b>(x)</b>Parenteral";
	$html.= "</td></tr>";
}
	
$html.= "<tr><td colspan='3' ><b>&Uacute;lceras de press&atilde;o:</b>";
if($dados->preulceradomic=='2'){
$html.= "&nbsp;<b>(x)</b>N&atilde;o&nbsp;&nbsp;";
$html.= " <b>( )</b>Sim";
	$html.= "</td></tr>";
}
if($dados->preulceradomic=='1'){
	$html.= "&nbsp;<b>( )</b>N&atilde;o&nbsp;&nbsp;";
	$html.= " <b>(x)</b>Sim";
	$html.= "</td></tr>";
}
	return $html;
}

public function cont_inter_domiciliar($dados){
	        $html.= "<tr bgcolor='#D3D3D3'>";
	        $html.= "<td colspan='3' ><b>Objetivo da continuidade da interna&ccedil;&atilde;o domiciliar. Score = {$dados->sobj}. </b></td>";
	        $html.= "</tr>";
	       
	        $html.= "<tr><td colspan='3' ><b>Locomo&ccedil;&atilde;o:</b>";
        if($dados->objlocomocao == '3'){
	        $html.= "&nbsp;<b>(x)</b>Deambulava&nbsp;&nbsp;";
	        $html.= "<b>( )</b>Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)&nbsp;&nbsp;";
			$html.= "<b>( )</b>Restrito ao leito";
			$html.= "</td></tr>";
        }
			
		if($dados->objlocomocao == '2'){
			$html.= "&nbsp;<b>( )</b>Deambulava&nbsp;&nbsp;";
			$html.= "<b>(x)</b>Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)&nbsp;&nbsp;";
			$html.= "<b>( )</b>Restrito ao leito";
			$html.= "</td></tr>";
		}
		
		if($dados->objlocomocao == '1'){
			$html.= "&nbsp;<b>( )</b>Deambulava&nbsp;&nbsp;";
			$html.= "<b>( )</b>Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)&nbsp;&nbsp;";
			$html.= "<b>(x)</b>Restrito ao leito";
			$html.= "</td></tr>";
		}
		$html.= "<tr><td colspan='3' ><b>Autonomia para higiene pessoal:</b>";
		if($dados->objhigiene=='2'){
			$html.= " &nbsp;<b>(x)</b>Sim&nbsp;&nbsp;";
			$html.= " <b>( )</b>N&atilde;o (Oral, deje&ccedil;&otilde;es, mic&ccedil&atilde;o e banho)";
			$html.= "</td></tr>";
		}
		if($dados->objhigiene=='1'){
			$html.= " &nbsp;<b>( )</b>Sim&nbsp;&nbsp;";
			$html.= " <b>(x)</b>N&atilde;o (Oral, deje&ccedil;&otilde;es, mic&ccedil&atilde;o e banho)";
			$html.= "</td></tr>";
		}
		
		$html.= "<tr><td colspan='3' ><b>N&iacute;vel de consci&ecirc;ncia:</b>";
		if($dados->objcons=='3'){
			$html.= " &nbsp;<b>(x)</b>LOTE&nbsp;&nbsp;";
			$html.= " <b>( )</b>Desorientado&nbsp;&nbsp;";
			$html.= " <b>( )</b>Inconsciente";
			$html.= "</td></tr>";
		}
			
		
		if($dados->objcons=='2'){
			$html.= " &nbsp;<b>( )</b>LOTE&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Desorientado&nbsp;&nbsp;";
			$html.= " <b>( )</b>Inconsciente";
			$html.= "</td></tr>";
		}
		
		if($dados->objcons=='1'){
			$html.= " &nbsp;<b>( )</b>LOTE&nbsp;&nbsp;";
			$html.= " <b>( )</b>Desorientado&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Inconsciente";
			$html.= "</td></tr>";
		}
	
		$html.= "<tr><td colspan='2' ><b>Alimenta&ccedil;&atilde;o:</b>";
		if($dados->objalimento=='4'){
			$html.= " &nbsp;<b>(x)</b>N&atilde;o assistida&nbsp;&nbsp;";
			$html.= " <b>( )</b>Assistida oral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Enteral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Parenteral";
			$html.= "</td></tr>";
		}
		
		if($dados->objalimento=='3'){
			$html.= " &nbsp;<b>( )</b>N&atilde;o assistida&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Assistida oral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Enteral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Parenteral";
			$html.= "</td></tr>";
		}
		if($dados->objalimento=='2'){
			$html.= " &nbsp;<b>( )</b>N&atilde;o assistida&nbsp;&nbsp;";
			$html.= " <b>( )</b>Assistida oral&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Enteral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Parenteral";
			$html.= "</td></tr>";
		}
		if($dados->objalimento=='1'){
			$html.= " &nbsp;<b>( )</b>N&atilde;o assistida&nbsp;&nbsp;";
			$html.= " <b>( )</b>Assistida oral&nbsp;&nbsp;";
			$html.= " <b>( )</b>Enteral&nbsp;&nbsp;";
			$html.= " <b>(x)</b>Parenteral";
			$html.= "</td></tr>";
		}
	
		$html.= "<tr><td colspan='3' ><b>&Uacute;lceras de press&atilde;o:</b>";
		if($dados->objulcera=='2'){
			$html.= " &nbsp;<b>(x)</b>N&atilde;o&nbsp;&nbsp;";
			$html.= " <b>( )</b>Sim";
			$html.= "</td></tr>";
		}
         if($dados->objulcera=='1'){
         	$html.= " &nbsp;<b>( )</b>N&atilde;o&nbsp;&nbsp;";
         	$html.= " <b>(x)</b>Sim";
         	$html.= "</td></tr>";
         }

return $html;

}

public function capitacao_medica($dados,$acesso){
	$enabled = "";


$this->plan;


//$html.= "<div id='div-ficha-medica' >";

//$html.= alerta();
$html.= "<form method='post' target='_blank'>";
//$html.= "<input type='hidden' name='paciente'  value='{$dados->pacienteid }' />";
$html.= "<table class='mytable' >";

$html.= "<tr><td colspan ='3'><h1><a><img src='../utils/logo2.jpg' width='250' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>Ficha de avalia&ccedil;&atilde;o Mederi.</h1></td></tr>";
//$html.= "<center><h2>{$dados->pacientenome}</h2></center>";
$html.=$this->cabecalho($dados);
$html.= "<tr bgcolor='#D3D3D3'><td colspan='3'><center><b>Respons&aacute;vel pela avalia&ccedil;&atilde;o: ".utf8_decode(htmlentities($dados->usuario))." - Data: {$dados->data} </b></center></td></tr>";


$html.= "<tr><td colspan='3'><b>Modalidade de Home Care:";
if($dados->modalidade =='1')
	$html.= "</b><b>(x)</b>&nbsp; Assistencia Domiciliar <b>( )</b>&nbsp; Interna&ccedil;&atilde;o Domiciliar</td></tr>";
else
	$html.= "</b><b>( )</b>&nbsp; Assistencia Domiciliar <b>(x)</b>&nbsp; Interna&ccedil;&atilde;o Domiciliar</td></tr>";

$html.= "<tr><td colspan='3'><b>Local de Avalia&ccedil;&atilde;o do paciente:</b>";
if($dados->local_av=='1')
	$html.= " <b>(x)</b>&nbsp;Domicilio/Institui&ccedil;&atilde;o de Apoio. <b>( )</b>&nbsp; Hospital</td></tr>";
	else
	$html.= " <b>( )</b>&nbsp; Domicilio/Institui&ccedil;&atilde;o de Apoio. <b>(x)</b>&nbsp; Hospital</td></tr>";
$html.= "<tr><td colspan='3'><br></br></td></tr><tr bgcolor='#D3D3D3'>";
$html.= "<td colspan='3' ><b>Motivo da hospitaliza&ccedil;&atilde;o</b></td>";
$html.= "</tr>";
$html.= "<tr><td colspan='3' >{$dados->motivo}";
//<input type='text' size=100 value='{$row['motivo']}' name='motivo' class='OBG' /></td></tr>";
$html.= "<tr><td colspan='3'><br></br></td></tr><tr bgcolor='#D3D3D3'>";
$html.= "<td colspan='3' ><b>Problemas Ativos</b></td>";
$html.= "</tr>";
if($acesso != "readonly") $html.= "<tr><td colspan='3' ><input type='text' name='busca-problemas-ativos' {$acesso} size=100 /></td></tr>";
$html.= "<tr><td colspan='3' ><table id='problemas_ativos'>";
if(!empty($dados->ativos)){
foreach($dados->ativos as $a){
$html.= "<tr><td>{$a}</td></tr>";
}
}

$html.= "</table></td></tr>";


$html.= " <tr ><td colspan='3'><br></br></td></tr><tr bgcolor='#D3D3D3'>";
$html.= "<td colspan='3' ><b>Comorbidades (S/N)</b></td>";
$html.= "</tr>";
$html.= "<tr><td><input type='text' class='boleano OBG' name='cancer' value='{$dados->cancer}' {$acesso} /><b>Câncer</b></td>";
$html.= "<td><input type='text' class='boleano OBG' name='cardiopatia' value='{$dados->cardiopatia}' {$acesso} /><b>Cardiopatia</b></td>";
$html.= "</tr><tr><td><input type='text' class='boleano OBG' name='psiquiatrico' value='{$dados->psiquiatrico}' {$acesso} /><b>Dist&uacute;rbio Psiqui&aacute;trico</b></td>";
$html.= "<td><input type='text' class='boleano OBG' name='dm' value='{$dados->dm}' {$acesso} /><b>DM</b></td>";
$html.= "</tr><tr><td><input type='text' class='boleano OBG' name='neurologica' value='{$dados->neuro}' {$acesso} /><b>Doen&ccedil;a Neurol&oacute;gica</b></td>";
$html.= "<td><input type='text' class='boleano OBG' name='reumatologica' value='{$dados->reumatologica}' {$acesso} /><b>Doen&ccedil;a Reumatol&oacute;gica</b></td>";
$html.= "</tr><tr><td><input type='text' class='boleano OBG' name='glaucoma' value='{$dados->glaucoma}' {$acesso} /><b>Glaucoma</b></td>";
$html.= "<td><input type='text' class='boleano OBG' name='has' value='{$dados->has}' {$acesso} /><b>HAS</b></td>";
$html.= "</tr><tr><td><input type='text' class='boleano OBG' name='hepatopatia' value='{$dados->hepatopatia}' {$acesso} /><b>Hepatopatia</b></td>";
$html.= "<td><input type='text' class='boleano OBG' name='nefropatia' value='{$dados->nefropatia}' {$acesso} /><b>Nefropatia</b></td>";
$html.= "</tr><tr><td><input type='text' class='boleano OBG' name='obesidade' value='{$dados->obesidade}' {$acesso} /><b>Obesidade</b></td>";
$html.= "<td><input type='text' class='boleano OBG' name='pneumopatia' value='{$dados->pneumopatia}' {$acesso} /><b>Pneumopatia</b></td>";
$html.= "</tr><tr><td colspan='3'><br></br></td></tr><tr bgcolor='#D3D3D3'>";
$html.= "<td style='WIDTH:500px' ><b>Alergia medicamentosa (S/N) </b><input type='text'  class='boleano OBG' name='palergiamed' value='{$dados->alergiamed}' {$acesso} /></td>";
$html.= "<td colspan='2' ><b>Alergia alimentar (S/N) </b><input type='text' class='boleano OBG' name='palergiaalim' value='{$dados->alergiaalim}' {$acesso} /></td>";
$html.= "</tr>";
$html.= "<tr><td><input  size=100 type='text' name='alergiamed'  readonly='readonly' value='{$dados->tipoalergiamed}' /></td>";
$html.= "<td colspan='2'><input type='text' name='alergiaalim' size=100 readonly='readonly' value='{$dados->tipoalergiaalim}' /></td></tr>";
//$html.="</table>";
////////hospita///////



if($dados->modalidade==2 && $dados->local_av==2){
$html.= "<tr><td><br></br></td></tr><tr><td colspan='3'><table id='score-id2' style='width:100% '>";
$html .= $this->cond_pre_ihosp($dados);
$html.= "</table></td></tr>";
}

if($dados->modalidade==2 && $dados->local_av==2){
$html.= "<tr><td><br></br></td></tr><tr><td colspan='3'><table id='score-id1' style='width:100% '>";
$html .= $this->cond_pre_id($dados,$enabled);
$html .= $this->cont_inter_domiciliar($dados);
$html.= "</table></td></tr>";
}
if($dados->modalidade==2 && $dados->local_av==1){
$html.= "<tr><td><br></br></td></tr><tr><td colspan='3'><table id='score-id1' style='width:100% '>";
$html .= $this->cond_pre_id($dados);
$html .= $this->cont_inter_domiciliar($dados);
$html.= "</table></td></tr>";
}


$html.= "<tr><td colspan='3'><br></br></td></tr><tr>";
$html.= "<td colspan='3' bgcolor='#D3D3D3'><b>Interna&ccedil;&otilde;es dos &uacute;ltimos 12 meses</b></td>";
$html.= "</tr>";
$html.= "<tr><td colspan='2' ><b>Qtd:</b><input type='text' size='4'  name='qtdanteriores' value='{$dados->qtdinternacoes}' {$acesso}/>";
$html.= "<b>Motivos:</b><input type='text' name='historicointernacao' size=100 readonly='readonly' value='{$dados->historico}' {$acesso} /></td></tr>";
$html.= "<tr><td colspan='3'><br></br></td></tr><tr bgcolor='#D3D3D3'>";
$html.= "<td colspan='3' ><b>Parecer M&eacute;dico</b></td>";
$html.= "</tr>";
$html.= "<tr><td colspan='2' >";

if($dados->parecer== '0'){
	$html.= "&nbsp;<b>(x)</b>Favor&aacute;vel&nbsp;&nbsp;";
	$html.= "<b>( )</b>Favor&aacute;vel com resalvas&nbsp;&nbsp;";
	$html.= "<b>( )</b>Contr&aacute;rio";}
if($dados->parecer== '1'){
	$html.= "&nbsp;<b>( )</b>Favor&aacute;vel&nbsp;&nbsp;";
	$html.= "<b>(x)</b>Favor&aacute;vel com resalvas&nbsp;&nbsp;";
	$html.= "<b>( )</b>Contr&aacute;rio";
}
if($dados->parecer=='2'){
	$html.= "&nbsp;<b>( )</b>Favor&aacute;vel&nbsp;&nbsp;";
	$html.= "<b>( )</b>Favor&aacute;vel com resalvas&nbsp;&nbsp;";
	$html.= "<b>(x)</b>Contr&aacute;rio";
}



$html.= "<br/><b>Justificativa:</b><br/>{$dados->pre_justificativa}</br>{$dados->justificativa}";
$html.= "</td></tr>";
$html.= "<tr><td colspan='3'><b>Plano Desmame:</b><br/>{$dados->plano_desmame}</td></tr>";
$html.= "<tr><td colspan='3'><br></br></td></tr>";

	
	$html .=  $this->equipamentos($dados->capmedica);

	$html.= "</table>";
	
	////////////////jeferson 6-5-2013 colocar assinatura medica
	$id_usuario = $dados->usuarioid;
	$sql = "SELECT usr.ASSINATURA
	FROM usuarios as usr 
	WHERE idUsuarios = '{$id_usuario}'";
	$result = mysql_query($sql);
	$assinatura = mysql_result($result,0,0);
	$html .= "<br/><br/><table width='100%'><tr><td><center><img src='{$assinatura}' width='20%'></center></td></tr></table>";
	
	$paginas []= $header.$html.= "</form>";
	
	
	//print_r($paginas);
	//print_r($paginas);
	
	$mpdf=new mPDF('pt','A4',13);
	$mpdf->SetHeader('página {PAGENO} de {nbpg}');
	$ano = date("Y");
	$mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
	$mpdf->WriteHTML("<html><body>");
	$flag = false;
	foreach($paginas as $pag){
		if($flag) $mpdf->WriteHTML("<formfeed>");
		$mpdf->WriteHTML($pag);
		$flag = true;
	}
	$mpdf->WriteHTML("</body></html>");
	$mpdf->Output('pacientes.pdf','I');
	exit;
	
	
	/*
	
	$mpdf=new mPDF('pt','A4',13);
    $mpdf->WriteHTML($html);
	$mpdf->Output('pacientes.pdf','I');
	*/
	
	
}

}

$p = new Paciente_imprimir_ficha();
$p->detalhes_capitacao_medica($_GET['id']);


?>