<?
require_once('../validar.php');
require_once('../db/config.php');
include_once('../utils/codigos.php');
require __DIR__ . '/../vendor/autoload.php';

use \App\Models\Administracao\ConselhoRegional;

if(!function_exists('redireciona')){
	function redireciona($link){
		if ($link==-1){
			echo" <script>history.go(-1);</script>";
		}else{
			echo" <script>document.location.href='$link'</script>";
		}
	}
}

function usr_menu(){
	$var = "<a href='?adm=usr&act=listar'>Listar</a><br>";

	$var .=  "<a href=?adm=usr&act=novo>Novo Usu&aacute;rio</a>";

	return $var;
}
function empresas($id,$id_empresa,$flag=0){
	if(isset($id_empresa)){
		$id_empresa= anti_injection($id_empresa,"numerico");
		$result = mysql_query("SELECT * FROM empresas where ATIVO='S' and id<>{$id_empresa} ORDER BY nome");
	}

	$result1 = mysql_query("SELECT * FROM usuarios_empresa WHERE usuarios_id={$id}");

	while($row1 = mysql_fetch_array($result1))
	{
		$arr[$row1['empresas_id']] =  $row1['usuarios_id'];
	}



	while($row = mysql_fetch_array($result))
	{
		if(array_key_exists($row['id'],$arr))
			$var .= "<input type='checkbox' name='empresa[]' value='". $row['id'] ."' checked>". $row['nome']."<br/>";
		else
			$var .= "<input type='checkbox' name='empresa[]' value='". $row['id'] ."' >". $row['nome']."<br/>";
	}
	if($flag==1)
		echo $var;
	else
		return $var;
}

function conselhoReginal($id = null){

	$var = "<select name='conselho_regional_id' id='conselho_regional_id' style='background-color:transparent;'  class=''>";
	$var .= "<option value='-1' selected>Selecione</option>";
	$sql = ConselhoRegional::getSQL();

	$result = mysql_query($sql);
	while($row = mysql_fetch_array($result))
	{
		if(($id <> NULL) && ($id == $row['id']))
			$var .= "<option value='". $row['id'] ."' selected>".$row['conselho']." ( ". $row['sigla'] ." )</option>";
		else
			$var .= "<option value='". $row['id'] ."'>".$row['conselho']." ( ". $row['sigla'] ." )</option>";
	}
	$var .= "</select>";
	return $var;

}
function empresa_principal($id){
	$id= anti_injection($id,"literal");

	$result = mysql_query("SELECT * FROM empresas where ATIVO='S' ORDER BY nome");



	$var = "<select name='empresa_principal' style='background-color:transparent;' id='combo_empresa' class='COMBO_OBG'>";
	$var .= "<option value='-1' selected>- Escolha uma Empresa -</option>";
	while($row = mysql_fetch_array($result))
	{
		if(($id <> NULL) && ($id == $row['id']))
			$var .= "<option value='". $row['id'] ."' selected>". $row['nome'] ."</option>";
		else
			$var .= "<option value='". $row['id'] ."'>". $row['nome'] ."</option>";
	}
	$var .= "</select>";
	return $var;
}

function usr_list(){
	if(!isset($_GET['pg'])) $_GET['pg'] = 1;
	if(!isset($_GET['offset'])) $_GET['offset'] = 999;

	$var = "<div id='dialog-acesso' title='Permiss&otilde;es' cod='' >";
	$var .= "</div>";
	$var .= "<div id='div-destivar-usuario' title='DESATIVAR USUARIO' acao='' idUsuario=''>
            <b>Usuário: <span id='span-div-desativar-usuario'></span></b>
            <p>
                <label for='data-destivar-usuario'>
                    <b>Data:</b>
                </label>
                <input type='date' id='data-destivar-usuario' class='OBG'>
            </p>
            <p>   <label for='obs-destivar-usuario' >
                   <b>Obeservação:</b>
                 </label>
                 <br>
                 <textarea id='obs-destivar-usuario' class='OBG'></textarea>
            </p>
          </div>";
	$var .= "<div id='div-destivar-ativar-usuario' title='ATIVAR OU DESATIVAR USUARIO' acao='' idUsuario=''>
            <b>Usuário: <span id='span-div-desativar-ativar-usuario'></span>.</b>
            <br>
            Para modificar a data que o usuário deve ser desativado basta modificar e clicar em Reprogramar.
            <br> Para remover a programação e ativar o usuário click em Ativar.
            <p>
                <label for='data-destivar-usuario'>
                    <b>Data:</b>
                </label>
                <input type='date' id='data-destivar-usuario' class='OBG'>
            </p>
            <p>   <label for='obs-destivar-usuario' >
                   <b>Obeservação:</b>
                 </label>
                 <br>
                 <textarea id='obs-destivar-usuario' class='OBG'></textarea>
            </p>
          </div>";
	$ant = (int) $_GET['pg'] -1;
	if($_GET['pg'] <> 1) $var .= "<a href=?adm=usr&act=listar&pg=". $ant .">anterior</a> | ";
	else $var .= " anterior | ";
	$start = ($_GET['pg']-1)*$_GET['offset'];
	$result = mysql_query("SELECT
  		u.idUsuarios,
  		u.login,
  		u.senha,
  		u.nome,
  		u.adm,
  		u.tipo,
  		u.empresa,
  		e.nome as nempresa,
  		u.modadm,
  		u.modaud,
  		u.modenf,
  		u.modfin,
  		u.modfat,
  		u.modlog,
  		u.modmed,
  		u.modrem,
  		u.modnutri,
  		u.modfisio,
  		u.modserv,
  		u.modfono,
  		u.modpsico,
  		u.email,
  		u.contato1,
  		u.contato2,
  		u.logado,
                u.block_at                
  	   FROM 
  		    `usuarios` as u inner Join empresas as e on (u.empresa = e.id)
  	where
  		u.tipo!='' AND prestador = 0
  		ORDER BY u.nome") or trigger_error("Erro");

	$result1 = mysql_query("SELECT
		  		ue.usuarios_id,		  		
		  		e.nome AS empresa
		  		  		 		
  	   FROM 
  		    `usuarios_empresa` AS ue LEFT JOIN
  		    `empresas` AS e ON (ue.empresas_id = e.id) 		     
  	   ORDER BY usuarios_id") or trigger_error("Erro");

	$aux='';
	while($row1 = mysql_fetch_array($result1)){
		$usu = $row1['usuarios_id'];
		if($usu!=$aux){
			$emp1[$usu] = $row1['empresa'];
			$aux =$usu;
		}else{
			$emp1[$usu] .= ','.$row1['empresa'];
		}
	}



	$num = mysql_affected_rows();
	if($num <= 15)
		$var .= "pr&oacute;xima";
	else{
		$prox = (int) $_GET['pg'] + 1;
		$var .= "<a href=?adm=usr&act=listar&pg={$prox}>pr&oacute;xima</a>";
	}
	$var .= "<table class='mytable' width=100% >";
	$var .=  "<thead><tr>";
	$var .=  "<th><b>Login</b></th>";
	$var .=  "<th><b>Nome</b></th>";
	$var .=  "<th>Unidade Regional</th>";
	$var .=  "<th><b>Adm</b></th>";
	$var .=  "<th><b>Perfil</b></th>";
	$var .=  "<th colspan='2' ><b>Op&ccedil;&otilde;es</b></th>";
	$var .=  "</tr></thead>";
	$cor = false;
	while($row = mysql_fetch_array($result)){
		foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
		if($cor) $var .= "<tr>";
		else $var .= "<tr class='odd' >";
		$cor = !$cor;
		$access = "<img class='permissoes' cod='{$row['idUsuarios']}' src='../utils/access_16x16.png' title='Permiss&otilde;es' border='0'>";
		$var .=  "<td valign='top'>{$row['login']}</td>";
		$var .=  "<td valign='top'>{$row['nome']} {$access}</td>";
		$usuario = $row['idUsuarios'];
		$var .=  "<td valign='top'>";
		$empresas = explode(",",$emp1[$usuario]);
		$var.= "<b>".$row['nempresa']."</b><br>";
		foreach($empresas as $em){
			$var .= $em."<br>";

		}

		$var .="</td>";

		$adm = "N&Atilde;O";
		$dataAgora = new \DateTime();
		$blockAt = \DateTime::createFromFormat('Y-m-d',$row['block_at']);
		$desativarAtivar ='';
		$blockAtBr = $blockAt->format('d/m/Y');
		$dataLimite = \DateTime::createFromFormat('Y-m-d','9999-12-31');
		if($dataAgora >= $blockAt){
			$desativarAtivar ="<img src='../utils/processada_16x16.png' title='Ativar' id-usuario='{$row['idUsuarios']}'"
				. "                nome-usuario='{$row['nome']}' acao='A' class='ativar-usuario' border='0'> ";
		}else{
			$desativarAtivar = $blockAt == $dataLimite ? "<img src='../utils/negar.png'"
				. " title='Desativar' class='desativar-usuario' acao='D' id-usuario='{$row['idUsuarios']}'"
				. "nome-usuario='{$row['nome']}' border='0'>":"<img src='../utils/relogio_vermelho_24x24.png'"
				. " title='Programado para desativar em $blockAtBr' id-usuario='{$row['idUsuarios']}' nome-usuario='{$row['nome']}'"
				. "class='programado-desativar-usuario' acao='P' border='0'>";
		}

		if ($row['adm'] == "1")
			$adm = "SIM";
		$var .=  "<td valign='top'>{$adm}</td>";
		$var .=  "<td valign='top'>{$row['tipo']}</td>";
		$del = "index.php?adm=usr&act=excluir&id={$row['idUsuarios']}&confirm=ok";
		$var .=  "<td>"
			. "<a href=?adm=usr&act=editar&id={$row['idUsuarios']}>"
			. "<img src='../utils/edit_16x16.png' title='Editar' border='0'>"
			. "</a>"
			. "</td>"
			. "<td>"
			. "<a {$row['idUsuarios']})\">"
			.$desativarAtivar
			. "</a>"

			. "</td> ";
		$var .=  "</tr>";
	}

	$var .=  "</table>";
	return $var;
}

function usr_new(){
	if (isset($_POST['submitted'])){


		$modadm = 0; $modaud = 0; $modenf = 0; $modfin = 0; $modlog = 0; $modmed = 0; $modrem = 0; $modnutri = 0; $modfisio = 0; $modserv = 0; $modfono = 0; $modpsico = 0;
		extract($_POST,EXTR_OVERWRITE);
		if(!empty($conselho_regional_id)){
			$arrayConselhoRegional = ConselhoRegional::getById($conselho_regional_id);
			$labelConselho = "({$arrayConselhoRegional['sigla']}: {$numero_conselho_regional})";
		}


		if($adm_ur == 1){
			$sql = "INSERT INTO `usuarios` (`login` ,  `senha` , `password`,  `nome` ,  `numero_conselho_regional` ,  `adm` ,  `tipo`, `empresa`, `modadm`, `modaud`, `modenf`, `modfin`, `modfat`, `modlog`,
`modmed`, `modrem`,`modnutri`,`modfisio`, `modserv`, `modfono`, `modpsico`,
 ADM_UR,permissao_padronizar_medicamento, prestador,conselho_regional_id,conselho_regional)	VALUES(  '{$login}' ,
 md5('{$senha}') , md5('{$senha}') , '{$nome}' , '{$numero_conselho_regional}' ,  '{$adm}' ,  '{$tipo}', '{$empresa_principal}',
'{$modadm}', '{$modaud}', '{$modenf}', '{$modfin}', '{$modfat}', '{$modlog}', '{$modmed}', '{$modrem}','{$modnutri}','{$modfisio}',
'{$modserv}', '{$modfono}', '{$modpsico}','{$empresa_principal}','{$permissao_padronizar_medicamento}', '{$prestador}',
,'$conselho_regional_id','$labelConselho')";
		}else{
			$sql = "INSERT INTO `usuarios` (`login` ,  `senha` ,  `password`, `nome` , `numero_conselho_regional` ,  `adm` ,
 `tipo`, `empresa`, `modadm`, `modaud`, `modenf`, `modfin`, `modfat`, `modlog`,
`modmed`, `modrem`,`modnutri`,`modfisio`, `modserv`, `modfono`, `modpsico`, ADM_UR,permissao_padronizar_medicamento,
 prestador, conselho_regional_id,conselho_regional )
 VALUES
 (  '{$login}' ,  md5('{$senha}') , md5('{$senha}') , '{$nome}' , '{$numero_conselho_regional}' ,  '{$adm}' ,  '{$tipo}',
  '{$empresa_principal}','{$modadm}', '{$modaud}', '{$modenf}', '{$modfin}', '{$modfat}', '{$modlog}', '{$modmed}',
  '{$modrem}','{$modnutri}','{$modfisio}', '{$modserv}', '{$modfono}', '{$modpsico}','{$adm_ur}',
  '{$permissao_padronizar_medicamento}', '{$prestador}','$conselho_regional_id','$labelConselho')";

		}
		//die(var_dump($sql));
		mysql_query($sql) or die("Erro");
		$usu = mysql_insert_id();
		savelog(mysql_escape_string(addslashes($sql)));

		foreach($empresa as $key=>$empresa){
			$sql2 = "insert into usuarios_empresa (id,usuarios_id,empresas_id) VALUES(null,'{$usu}','{$empresa}')";
			mysql_query($sql2) or die("Erro");
		}

		redireciona('index.php?adm=usr&act=listar');
	}

	$var = "<div id='div-novo-usuario' >";
	$var .= alerta();
	$var .= "<form action='' method='POST'>";
	$var .= "<p><b>Login:</b><br /><input class='OBG' type='text' name='login'/>";
	$var .= "<p><b>Senha:</b><br /><input class='OBG' id='user-passwd' type='password' name='senha'/>";
	$var .= "<p><b>Confirmar Senha:</b><br /><input class='OBG' id='user-confirm-passwd' type='password' name='confirmar_senha'/>";
	$var .= "<p><span style='color: #CC0000' id='warning-incomp-passwd'></span>";
	$var .= "<p><b>Nome:</b><br /><input  class='OBG' type='text' name='nome'/>";
	$var .= "<p><b>Conselho Regional (CRM, COREN, CREFITO, CRN, ETC.):</b><br/>";
	$var .= conselhoReginal();

	$var .= "<p><b>Número do registro:<b><br><input type='text' name='numero_conselho_regional' />";
	$var .= "<p><b>Unidade Regional:</b> <br />".empresa_principal(NULL);
	$var .= "</p><div id='outras_cidades'></div>";
	$var .="<b>Adm UR:</b><br /><input type='radio' name='adm_ur' value=1 />Sim&nbsp;&nbsp;<input type='radio' name='adm_ur' value=0 CHECKED/>N&atilde;o";
	$var .= "<p><b>Adm:</b><br /><input type='radio' name='adm' value=1 />Sim&nbsp;&nbsp;<input type='radio' name='adm' value=0 CHECKED/>N&atilde;o";
	$var .= "<p><b>Perfil:</b><br />";
	$var .= "<input type='radio' name='tipo' value='administrativo' CHECKED/>Administrativo&nbsp;&nbsp;";
	$var .= "<input type='radio' name='tipo' value='enfermagem' />Enfermagem&nbsp;&nbsp;";
	$var .= "<input type='radio' name='tipo' value='financeiro' />Financeiro&nbsp;&nbsp;";
	$var .= "<input type='radio' name='tipo' value='faturamento' />Faturamento&nbsp;&nbsp;";
	$var .= "<input type='radio' name='tipo' value='logistica' />Log&iacute;stica&nbsp;&nbsp;";
	$var .= "<input type='radio' name='tipo' value='medico' />M&eacute;dico";
	$var .= "<input type='radio' name='tipo' value='nutricao' />Nutricionista";
	$var .= "<p/>";
	$var .= "<p><b>Perfis para prestadores terceirizados:</b><br>";
	$var .= "<input type='radio' name='tipo' value='fisioterapia' />Fisioterapia";
	$var .= "<input type='radio' name='tipo' value='servico_social' />Serviço Social";
	$var .= "<input type='radio' name='tipo' value='fonoaudiologia' />Fonoaudiologia";
	$var .= "<input type='radio' name='tipo' value='psicologia' />Psicologia";
    $var .= "</p>";
	$var .= "<p><b>Acesso:</b><br />";
	$var .= "<input type='checkbox' name='modadm' value='1' />Administrativo&nbsp;&nbsp;";
	$var .= "<input type='checkbox' name='modaud' value='1' />Auditoria&nbsp;&nbsp;";
	$var .= "<input type='checkbox' name='modenf' value='1' />Enfermagem&nbsp;&nbsp;";
	$var .= "<input type='checkbox' name='modfin' value='1' />Financeiro&nbsp;&nbsp;";
	$var .= "<input type='checkbox' name='modfat' value='1' />Faturamento&nbsp;&nbsp;";
	$var .= "<input type='checkbox' name='modlog' value='1' />Log&iacute;stica&nbsp;&nbsp;";
	$var .= "<input type='checkbox' name='modmed' value='1' />M&eacute;dico&nbsp;&nbsp;";
	$var .= "<input type='checkbox' name='modrem' value='1' />Remo&ccedil;&otilde;es";
	$var .= "<input type='checkbox' name='modnutri' value='1' />Nutri&ccedil;&atilde;o";
    $var .= "</p>";
    $var .= "<p><b>Permissões para prestadores terceirizados:</b><br>";
	$var .= "<input type='checkbox' name='modfisio' value='1' />Fisioterapia";
	$var .= "<input type='checkbox' name='modserv' value='1' />Serviço Social";
	$var .= "<input type='checkbox' name='modfono' value='1' />Fonoaudiologia";
	$var .= "<input type='checkbox' name='modpsico' value='1' />Psicologia</p>";

	$var .= "<p><label for='permissao_padronizar_medicamento'><b>Permissão para Padronizar Medicamento:</b></label>
				<select id='permissao_padronizar_medicamento' name='permissao_padronizar_medicamento'>
					<option value='N' selected >Não</option>
					<option value='S'  >Sim</option>
				</select>
				</p>";
    $var .= "<p><label for='prestador'><b>Prestador terceirizado?</b></label>
				<select id='prestador' name='prestador'>
					<option value='0' selected >Não</option>
					<option value='1'>Sim</option>
				</select>
				</p>";
	$var .= "<p><input type='submit' id='inserir-usuario' value='Inserir' /><input type='hidden' value='1' name='submitted' />";
	$var .= "</form></div>";

	return $var;
}

function usr_edit($post=null){

	if (isset($_GET['id']) ) {
		$id = (int) $_GET['id'];
		if ($post!=null){

			$modadm = 0; $modaud = 0; $modenf = 0; $modfin = 0; $modfat = 0; $modlog = 0; $modmed = 0; $modrem = 0;$modnutri = 0; $modfisio = 0; $modserv = 0; $modfono = 0; $modpsico = 0;
			extract($post,EXTR_OVERWRITE);

			$modadm = anti_injection($modadm,'numerico');
			$modaud = anti_injection($modaud,'numerico');
			$modenf = anti_injection($modenf,'numerico');
			$modfin = anti_injection($modfin,'numerico');
			$modfat = anti_injection($modfat,'numerico');
			$modlog = anti_injection($modlog,'numerico');
			$modmed = anti_injection($modmed,'numerico');
			$modrem = anti_injection($modrem,'numerico');
			$modnutri = anti_injection($modnutri,'numerico');
			$modfisio = anti_injection($modfisio,'numerico');
			$modserv = anti_injection($modserv,'numerico');
			$modfono = anti_injection($modfono,'numerico');
			$modpsico = anti_injection($modpsico,'numerico');
			$adm_ur = anti_injection($adm_ur,'numerico');
			$prestador = anti_injection($prestador,'numerico');
			if(!empty($conselho_regional_id)){
				$arrayConselhoRegional = ConselhoRegional::getById($conselho_regional_id);
				$labelConselho = "({$arrayConselhoRegional['sigla']} {$regiao}: {$numero_conselho_regional})";
			}

			if($adm_ur==1){
				$sql = "UPDATE `usuarios` SET  `login` =  '{$_POST['login']}' ,  `senha` =  md5('{$_POST['senha']}') ,
 `password` =  md5('{$_POST['senha']}') , `nome` =  '{$_POST['nome']}' , `numero_conselho_regional` =  '{$_POST['numero_conselho_regional']}' ,  `adm` =  '{$_POST['adm']}' ,
`tipo` =  '{$_POST['tipo']}', `empresa` = '{$_POST['empresa_principal']}', `modadm` = '{$modadm}', `modaud` = '{$modaud}', `modenf` = '{$modenf}',
`modfin` = '{$modfin}', `modfat` = '{$modfat}', `modlog` = '{$modlog}', `modmed` = '{$modmed}', `modrem` = '{$modrem}', `modnutri` = '{$modnutri}', `modfisio` = '{$modfisio}', `modserv` = '{$modserv}', `modfono` = '{$modfono}', `modpsico` = '{$modpsico}',
 `ADM_UR` = '{$_POST['empresa_principal']}', permissao_padronizar_medicamento='{$permissao_padronizar_medicamento}', prestador ='{$prestador}',
 conselho_regional_id = '{$_POST['conselho_regional_id']}', conselho_regional = '{$labelConselho}'

   WHERE `idUsuarios` = '$id' ";
			}else{
				$sql = "UPDATE `usuarios` SET  `login` =  '{$_POST['login']}' ,  `senha` =  md5('{$_POST['senha']}') ,  `password` =  md5('{$_POST['senha']}') ,
`nome` =  '{$_POST['nome']}' , `numero_conselho_regional` =  '{$_POST['numero_conselho_regional']}' ,  `adm` =  '{$_POST['adm']}' ,
`tipo` =  '{$_POST['tipo']}', `empresa` = '{$_POST['empresa_principal']}', `modadm` = '{$modadm}', `modaud` = '{$modaud}', `modenf` = '{$modenf}',
`modfin` = '{$modfin}', `modfat` = '{$modfat}', `modlog` = '{$modlog}', `modmed` = '{$modmed}', `modrem` = '{$modrem}', `modnutri` = '{$modnutri}', `modfisio` = '{$modfisio}', `modserv` = '{$modserv}', `modfono` = '{$modfono}', `modpsico` = '{$modpsico}',
 `ADM_UR` = '{$adm_ur}',permissao_padronizar_medicamento ='{$permissao_padronizar_medicamento}', prestador ='{$prestador}',
  conselho_regional_id = '{$_POST['conselho_regional_id']}', conselho_regional = '{$labelConselho}'
 WHERE `idUsuarios` = '$id' ";

			}

			mysql_query($sql) or die("Erro");
			$sql1 = "DELETE FROM usuarios_empresa WHERE `usuarios_id` ='$id' ";
			mysql_query($sql1) or die("Erro");
			foreach($empresa as $key=>$empresa){
				$sql2 = "insert into usuarios_empresa (id,usuarios_id,empresas_id) VALUES(null,'{$id}','{$empresa}')";
				mysql_query($sql2) or die("Erro");
			}

			savelog(mysql_escape_string(addslashes($sql)));
			redireciona('index.php?adm=usr&act=listar');
		}
		$id = anti_injection($id,'numerico');
		$row = mysql_fetch_array ( mysql_query("SELECT * FROM `usuarios` WHERE `idUsuarios` = '$id' "));

		$var = "<div id='div-novo-usuario' >";
		$var .= alerta();
		$var .= "<form action='' method='POST'>";
		$var .= "<p><b>Login:</b><br /><input type='text' class='OBG' name='login' value='".stripslashes($row['login'])."' />";
		$var .= "<p><b>Senha:</b><br /><input type='password' class='OBG' id='user-passwd' name='senha' value='' />";
		$var .= "<p><b>Confirmar Senha:</b><br /><input class='OBG' id='user-confirm-passwd' type='password' name='confirmar_senha'/>";
		$var .= "<p><span style='color: #CC0000' id='warning-incomp-passwd'></span>";
		$var .= "<p><b>Nome:</b><br /><input type='text' class='OBG' name='nome' value='".stripslashes($row['nome'])."' />";
		$var .= "<p><b>Conselho Regional (CRM, COREN, CREFITO, CRN, ETC.):</b><br/>";
		$var .= conselhoReginal($row['conselho_regional_id'],null,'OBG');

		$var .= "<p><b>Número do registro:<b><br><input type='text' name='numero_conselho_regional' value='{$row['numero_conselho_regional']}'/>";

		$var .= "<p><b>Unidade Regional:</b><br />".empresa_principal($row['empresa']);
		$var .= "</p><div id='outras_cidades'>".empresas($_GET['id'],$row['empresa'])."</div>";
		if(stripslashes($row['ADM_UR']) <> "0"){
			$var .="<b>Adm UR:</b><br /><input type='radio' name='adm_ur' value=1 CHECKED/>Sim&nbsp;&nbsp;<input type='radio' name='adm_ur' value=0 />N&atilde;o";
		}else{
			$var .="<b>Adm UR:</b><br /><input type='radio' name='adm_ur' value=1 />Sim&nbsp;&nbsp;<input type='radio' name='adm_ur' value=0 CHECKED/>N&atilde;o";
		}


		if(stripslashes($row['adm']) <> "1")
			$var .= "<p><b>Adm:</b><br /><input type='radio' name='adm' value=1 />Sim&nbsp;&nbsp;<input type='radio' name='adm' value=0 CHECKED/>N&atilde;o";
		else
			$var .= "<p><b>Adm:</b><br /><input type='radio' name='adm' value=1 CHECKED />Sim&nbsp;&nbsp;<input type='radio' name='adm' value=0 />N&atilde;o";
		$var .= "<p><b>Perfil:</b><br />";

		if(stripslashes($row['tipo']) == "administrativo")
			$var .= "<input type='radio' name='tipo' value='administrativo' CHECKED/>Administrativo&nbsp;&nbsp;";
		else
			$var .= "<input type='radio' name='tipo' value='administrativo' />Administrativo&nbsp;&nbsp;";
		if(stripslashes($row['tipo']) == "enfermagem")
			$var .= "<input type='radio' name='tipo' value='enfermagem' CHECKED/>Enfermagem&nbsp;&nbsp;";
		else
			$var .= "<input type='radio' name='tipo' value='enfermagem' />Enfermagem&nbsp;&nbsp;";
		if(stripslashes($row['tipo']) == "financeiro")
			$var .= "<input type='radio' name='tipo' value='financeiro' CHECKED/>Financeiro&nbsp;&nbsp;";
		else
			$var .= "<input type='radio' name='tipo' value='financeiro' />Financeiro&nbsp;&nbsp;";
		if(stripslashes($row['tipo']) == "faturamento")
			$var .= "<input type='radio' name='tipo' value='faturamento' CHECKED/>Faturamento&nbsp;&nbsp;";
		else
			$var .= "<input type='radio' name='tipo' value='faturamento' />Faturamento&nbsp;&nbsp;";
		if(stripslashes($row['tipo']) == "logistica")
			$var .= "<input type='radio' name='tipo' value='logistica' CHECKED/>Log&iacute;stica&nbsp;&nbsp;";
		else
			$var .= "<input type='radio' name='tipo' value='logistica' />Log&iacute;stica&nbsp;&nbsp;";
		if(stripslashes($row['tipo']) == "medico")
			$var .= "<input type='radio' name='tipo' value='medico' CHECKED/>M&eacute;dico&nbsp;&nbsp;";
		else
			$var .= "<input type='radio' name='tipo' value='medico' />M&eacute;dico&nbsp;&nbsp;";
		if(stripslashes($row['tipo']) == "nutricao")
			$var .= "<input type='radio' name='tipo' value='nutricao' CHECKED/>Nutri&ccedil;&atilde;o&nbsp;&nbsp;";
		else
			$var .= "<input type='radio' name='tipo' value='nutricao' />Nutri&ccedil;&atilde;o&nbsp;&nbsp;";
        $var .= "<br>";
        $var .= "<b>Perfis para prestadores terceirizados:</b><br>";
		if(stripslashes($row['tipo']) == "fisioterapia")
			$var .= "<input type='radio' name='tipo' value='fisioterapia' CHECKED/>Fisioterapia";
		else
			$var .= "<input type='radio' name='tipo' value='fisioterapia' />Fisioterapia";
        if(stripslashes($row['tipo']) == "servico_social")
            $var .= "<input type='radio' name='tipo' value='servico_social' CHECKED/>Servi&ccedil;o Social";
        else
            $var .= "<input type='radio' name='tipo' value='servico_social' />Servi&ccedil;o Social";
        if(stripslashes($row['tipo']) == "fonoaudiologia")
            $var .= "<input type='radio' name='tipo' value='fonoaudiologia' CHECKED/>Fonoaudiologia";
        else
            $var .= "<input type='radio' name='tipo' value='fonoaudiologia' />Fonoaudiologia";
        if(stripslashes($row['tipo']) == "psicologia")
            $var .= "<input type='radio' name='tipo' value='psicologia' CHECKED/>Psicologia";
        else
            $var .= "<input type='radio' name='tipo' value='psicologia' />Psicologia";
		$var .= "<p><b>Acesso:</b><br />";
		$var .= "<input type='checkbox' name='modadm' value='1' ".(($row['modadm']=='1')?"checked":"")." />Administrativo&nbsp;&nbsp;";
		$var .= "<input type='checkbox' name='modaud' value='1' ".(($row['modaud']=='1')?"checked":"")." />Auditoria&nbsp;&nbsp;";
		$var .= "<input type='checkbox' name='modenf' value='1' ".(($row['modenf']=='1')?"checked":"")." />Enfermagem&nbsp;&nbsp;";
		$var .= "<input type='checkbox' name='modfin' value='1' ".(($row['modfin']=='1')?"checked":"")." />Financeiro&nbsp;&nbsp;";
		$var .= "<input type='checkbox' name='modfat' value='1' ".(($row['modfat']=='1')?"checked":"")." />Faturamento&nbsp;&nbsp;";
		$var .= "<input type='checkbox' name='modlog' value='1' ".(($row['modlog']=='1')?"checked":"")." />Log&iacute;stica&nbsp;&nbsp;";
		$var .= "<input type='checkbox' name='modmed' value='1' ".(($row['modmed']=='1')?"checked":"")." />M&eacute;dico&nbsp;&nbsp;";
		$var .= "<input type='checkbox' name='modrem' value='1' ".(($row['modrem']=='1')?"checked":"")." />Remo&ccedil;&otilde;es";
		$var .= "<input type='checkbox' name='modnutri' value='1' ".(($row['modnutri']=='1')?"checked":"")." />Nutri&ccedil;&atilde;o";
        $var .= "<br>";
        $var .= "<b>Permissões para prestadores terceirizados:</b>br>";
		$var .= "<input type='checkbox' name='modfisio' value='1' ".(($row['modfisio']=='1')?"checked":"")." />Fisioterapia";
		$var .= "<input type='checkbox' name='modserv' value='1' ".(($row['modserv']=='1')?"checked":"")." />Servi&ccedil;o Social";
		$var .= "<input type='checkbox' name='modfono' value='1' ".(($row['modfono']=='1')?"checked":"")." />Fonoaudilogia";
		$var .= "<input type='checkbox' name='modpsico' value='1' ".(($row['modpsico']=='1')?"checked":"")." />Psicologia";
		$var .= "<p><label for='permissao_padronizar_medicamento'><b>Permissão para Padronizar Medicamento:</b></label>
				<select id='permissao_padronizar_medicamento' name='permissao_padronizar_medicamento'>
					<option value='N' ".(($row['permissao_padronizar_medicamento']=='N')?"selected":"")." >Não</option>
					<option value='S'  ".(($row['permissao_padronizar_medicamento']=='S')?"selected":"").">Sim</option>
				</select>
				</p>";
        $var .= "<p><label for='prestador'><b>Prestador terceirizado?</b></label>
				<select id='prestador' name='prestador'>
					<option value='0' ".(($row['prestador']=='0')?"selected":"")." >Não</option>
					<option value='1'  ".(($row['prestador']=='1')?"selected":"").">Sim</option>
				</select>
				</p>";
		$var .= "<p><input type='submit' id='salvar-usuario' value='Salvar' /><input type='hidden' value='1' name='submitted' />";
		$var .= "</form></div>";
	}
	return $var;
}

function usr_del()
{

	$id = (int) $_GET['id'];
	mysql_query("DELETE FROM `usuarios` WHERE `idUsuarios` = '$id' ") ;
	$var = (mysql_affected_rows()) ? "Row deleted.<br /> " : "Nothing deleted.<br /> ";
	redireciona('index.php?adm=usr&act=listar');

	return $var;
}

switch ($_REQUEST['query']){
	case 'empresa':
		empresas(NULL,$_REQUEST['empresa_id'],1);
		break;

}
?>
