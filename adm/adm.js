$(function($){

  $(".data").datepicker({
    inline: true,
    changeYear: true,
    changeMonth: true,
    yearRange: '1900:2100'
  });
  $(".hora-paciente").mask("99:99:99");
  $(".hora").mask("99:99:00");
    $(".ano").mask("9999");
  $(".valor").priceFormat({
    prefix: '',
    centsSeparator: ',',
    thousandsSeparator: '.'
  });

    $(".editar-item-plano").click(function(){
        console.log($(this));
        $(this).attr('editado',1);
    });
  
/////jeferson 14-5-2013 fim 
$("#add-item-plano").click(function(){
  var cont_linha = 0;
  $(".contador").each(function(){
    cont_linha ++;
  });
  var quantidade_linha = 0;
  quantidade_linha = cont_linha + 1;
    var botoes = "<img align='right' class='rem-item'  src='../utils/delete_16x16.png' title='Remover' border='0'>";

    $("#tabela-item-plano").append("<tr class='odd' editado='F'><td width=1% class='contador' cont='"+quantidade_linha+"'>"+botoes+"</td><td><input name='item' class='OBG form-control' style='font-size:10px;'  size='70%' /></td><td name='"+quantidade_linha+"'></td><td><input class='form-control' type='text' id='cod{$row['id']}' name='cod_plano' style='font-size:10px;'  /></td><td> <input type='text' id-item = '' cod='{$row['id']}' class='valor item form-control' value='0,00' style='font-size:10px;'  /></td></tr>");
    item_cobrancaplano();
    
    $(".valor").priceFormat({
      prefix: '',
      centsSeparator: ',',
      thousandsSeparator: '.'
    });

});

    $('.modal-autorizacao-mensal').live('click', function () {
        var $this = $(this);
        var paciente = $this.attr('paciente-id');
        var convenio = $this.attr('convenio');

        var $modal = $('#modal-autorizacao-servicos');
        $modal.attr('paciente-id', paciente);
        $modal.attr('convenio', convenio);

        $modal.modal("show");
    });

    $('.consultar-autorizacao-periodo').live('click', function () {
        var $modal = $('#modal-autorizacao-servicos');
        var inicio = $modal.find('#inicio-autorizacao').val();
        var fim = $modal.find('#fim-autorizacao').val();
        var tipo_documento = $modal.find('#tipo-documento').val();
        var paciente = $modal.attr('paciente-id');
        var convenio = $modal.attr('convenio');

        window.open('/adm/secmed/?action=autorizacao-ur-excel&inicio=' + inicio + '&fim=' + fim + '&paciente=' + paciente + '&plano=' + convenio + '&tipo_documento=' + tipo_documento, '_blank');

        $modal.find('#inicio-autorizacao').val('');
        $modal.find('#fim-autorizacao').val('');
        $modal.find('#tipo-documento').val('T');
        $modal.attr('paciente-id', '');
        $modal.attr('convenio', '');
        $modal.modal("hide");
    });

    $('#usuario-assinatura').chosen({search_contains: true});
    $("#usuario-assinatura").change(function () {
        console.log($(this).val());
        $.get(
            "query.php",
            {
                query: "buscar-imagem-assinatura",
                usuario: $(this).val()
            },
            function(result){
                console.log(result);
                if(result != '') {
                    $("#imagem-usuario").html("").append(
                        '<img src="' + result + '">'
                    );
                } else {
                    $("#imagem-usuario").html("<b>Esse usu&aacute;rio n&atilde;o possui assinatura!</b>")
                }
            }
        );
    });

function item_cobrancaplano(){
  var cont_linha = 0;
  $(".contador").each(function(){
    cont_linha ++;
  });
  var quantidade_linha = 0;
  quantidade_linha = cont_linha ;
    $.post("planos.php",{query: "item-cobrancaplano",tipo:0},
        function(r){
              
          $("td[name='"+quantidade_linha+"']").html(r);
        });
}

    $(".rem-item").live('click',function(){
        if(confirm('Deseja realmente remover este item?')) {
            var id_item = $(this).attr('id-item');
            var vm = $(this);
            if(id_item) {
                $.post("query.php",{query: "cancelar-item-plano",id:id_item},
                    function(r) {
                        if (r == 1) {
                            alert('Item cancelado com sucesso.');
                            vm.parent().parent().remove();
                        } else {
                            alert(r);
                        }
                        return false;
                    });
            } else {
                vm.parent().parent().remove();
            }
        }
    });
///////
  
  
  
  $('.numerico').keyup(function(e){
    this.value = this.value.replace(/\D/g,'');
  });
  $('.numerico1').keyup(function(e){
      this.value = this.value.replace(/[^0-9.]/g,'');
    });
  
  if($(".flagc").attr('flag')==1){
   $(".espcuid").hide();
   $("#especialidadeCuidador input[name='especialidadeCuidador']").val('');
}else{
   $(".espcuid").show();
   
}
  
  if($(".flagx").attr('flag')==1){
     $(".espresp").hide();
     
  }else{
     $(".espresp").show();
     
  }
 

  $(".div-aviso").hide();
  
  $("#nova_ficha").click(function(){
    var id = $("input[name='nova_ficha']").val();
    
    window.document.location.href = '?adm=paciente&act=newcapmed&id='+id+'';
    
  });
  $("#nova_ficha_evolucao").click(function(){
    var id = $("input[name='nova_ficha_evolucao']").val();
    
    window.document.location.href = '?adm=paciente&act=newfichaevolucao&id='+id+'';
    
  });
//////////////dados iniciais da ficha/////////////
 /// se paciente � id ou ad;
  $(".modalidade").click(function(){ 
    if( $("input[class='modalidade']:checked")){
      var x =$("input[class='modalidade']:checked").val();
   }
    
    if($("input[class='local_av']:checked")){
      var y =$("input[class='local_av']:checked").val();
    }
   
   if(x == 1 ){
     $("#score-id1").hide();
     $("#score-id2").hide();
    
   }
   if(x == 1 && y == 1){
     $("#score-id1").hide();
     $("#score-id2").hide();
     
     
   }
   if(x == 1 && y == 2){
     $("#score-id1").hide();
     $("#score-id2").hide();
     
   }
   if (x == 2 && y == 1){
     $("#score-id2").hide();
     $("#score-id1").show();
     
     
   }
   if(x == 2 && y == 2){
     $("#score-id1").show();
     $("#score-id2").show();
     
   }
   
  
   
});
  
  $("#imprimir_ficha_mederi").click(function(){
  
    var x =0;
    x =$("input[name='paciente']").val();
    window.location.href='?adm=paciente&act=listcapmed&id='+x;
    
    
  });
  $("#imprimir_ficha_evolucao").click(function(){
    
    var x =0;
    x =$("input[name='paciente']").val();
    window.location.href='?adm=paciente&act=listfichaevolucao&id='+x;
    
    
  });
  
  $("#imprimir_score").click(function(){
    
    var x =0;
    x =$("input[name='imprimir_score']").val();
    window.location.href='?adm=paciente&act=listcapmed&id='+x;
    
    
  });
  
  $("#imprimir_det_presc").click(function(){
    
    var x =0;
    x = $("input[name='imprimir_det_presc']").val();
   
    window.location.href='?adm=paciente&act=listcapmed&id='+x;
    
    
  });
  
  $(".local_av").click(function(){ 
    var x;
    var y;
    if( $("input[class='modalidade']:checked")){
       x =$("input[class='modalidade']:checked").val();
   }
    
    if($("input[class='local_av']:checked")){
      y =$("input[class='local_av']:checked").val();
    }
    
   
    
   
  
   if(x == 1 ){
     $("#score-id1").hide();
     $("#score-id2").hide();
     
   }
   if(x == 1 && y == 1){
     $("#score-id1").hide();
     $("#score-id2").hide();
     
   }
   if(x == 1 && y == 2){
     $("#score-id1").hide();
     $("#score-id2").hide();
     
   }
   if (x == 2 && y == 1){
     $("#score-id2").hide();
     $("#score-id1").show();
     
     
   }
   if (x == 2 && y == 2){
     $("#score-id1").show();
     $("#score-id2").show();
     
   }
   
    
  });
  
//////////////////////ficha evolucao///////////////////////////////////
  $(".obs_probativo").hide();
  $(".probativo").click(function(){
  
    if($(this).is(":checked")){
      var val= $(this).val();
        var nome = $(this).attr('name');
        if(val == 1 || val ==3){
          $("span[name='"+nome+"']").hide();
          $("span[name='"+nome+"']").children('textarea').removeClass('OBG');
          $("span[name='"+nome+"']").children('textarea').removeAttr("style");
          
        }else{
        $("span[name='"+nome+"']").show();
        $("span[name='"+nome+"']").children('textarea').addClass('OBG');
        }
  }  
    
  });
  
  
 $(".obs_exame").hide();
 //$(".mostrar_observacao").hide();
  
  $(".obs_exame").each(function(i){//Mostra os campos que tem observação
   var obs = $(this).children('textarea').val();
  
   if(obs != ''){
     $(this).show();
   }

  });
  ///////////jeferson marques status paciente 03-10-2013
    function data_hora() {
        $(".data").datepicker({
            inline: true,
            changeYear: true,
            changeMonth: true,
            yearRange: '1900:2100'
        });
        $(".hora").mask("99:99:00");

        $(".valor").priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
    }
  
   $('.status-paciente').live('click',function(){
       data_hora();
        
       if($(this).attr('s') == 4){
           $('#span-autorizacao').attr('style','');
           $('#span-admissao').attr('style','');
          $('#span-observacao').attr('style','display:none;');
           $('#span-tipo').attr('style','display:none;');
           $('#data-admissao').addClass('OBG');
           $('#hora-admissao').addClass('OBG');
           $('#data-autorizacao').addClass('OBG');
           $('#hora-autorizacao').addClass('OBG');
           $('#status-observacao').removeClass('OBG');
           $('#status-tipo').removeClass('OBG');
           $('#status-tipo').attr('style','');
       }else if($(this).attr('s') ==1){
           $('#data-autorizacao').attr('style','');
           $('#hora-autorizacao').attr('style','');
           $('#data-admissao').attr('style','');
           $('#hora-admissao').attr('style','');
           $('#span-tipo').attr('style','');
           $('#status-tipo').addClass('OBG');
            $('#span-autorizacao').attr('style','display:none;');
           $('#span-admissao').attr('style','display:none;');
            $('#span-observacao').attr('style','');
           
           $('#data-admissao').removeClass('OBG');
           $('#hora-admissao').removeClass('OBG');
           $('#data-autorizacao').removeClass('OBG');
           $('#hora-autorizacao').removeClass('OBG');
           $('#status-observacao').addClass('OBG');
            
            $('#status-observacao').attr('style','');
           
       }else{
           $('#data-autorizacao').attr('style','');
           $('#hora-autorizacao').attr('style','');
           $('#data-admissao').attr('style','');
           $('#hora-admissao').attr('style','');
           $('#span-tipo').attr('style','display:none;');
           $('#span-autorizacao').attr('style','display:none;');
           $('#span-admissao').attr('style','display:none;');
           $('#span-observacao').attr('style','');
           $('#data-admissao').removeClass('OBG');
           $('#hora-admissao').removeClass('OBG');
           $('#data-autorizacao').removeClass('OBG');
           $('#hora-autorizacao').removeClass('OBG');
           $('#status-observacao').addClass('OBG');
           $('#status-tipo').removeClass('OBG');
           $('#status-tipo').attr('style','');
       }    
   });
  
  
  ///fim busca paciente
  
 ///jeferson 04-10-2013 mudar grid do listar paciente 
 ///opções medicas 
 $(".botao_relatorios_medicos").live('click', function() {
        $("#dialog-opcao-medica").attr("cod", $(this).attr("cod"));
        $("#dialog-opcao-medica").dialog("open");
    });
    
    $("#dialog-opcao-medica").dialog({
        autoOpen: false, modal: true, height: 450, width: 400,
        open: function(event, ui) {
           

            $("#dialog-opcao-medica").html("<br> <fieldset style='width:95%;'><legend><b>Fichas</b></legend>"+
                    "<input type='radio' name='opcao_medica' value=6 checked/>Avalia&ccedil;&atilde;o<br>" +
                    "<input type='radio' name='opcao_medica' value=7 />Evolu&ccedil;&atilde;o<br><br></fieldset>"+
                    "<fieldset style='width:95%;'><legend><b>Relat&oacute;rios</b></legend>"+
                    "<input type='radio' name='opcao_medica' value=1 checked/>Alta m&eacute;dica<br>" +
                    "<input type='radio' name='opcao_medica' value=2 />Aditivo<br>" +
                    "<input type='radio' name='opcao_medica' value=3 />Prorroga&ccedil;&atilde;o<br>" +
                    "<input type='radio' name='opcao_medica' value=4 />Intercorr&ecirc;ncia <br>" +
                    "<input type='radio' name='opcao_medica' value=5 />Deflagrado</fieldset>");
                     
        },
        buttons: {
            Visualizar: function() {
                var opcao_escolhida = $("#dialog-opcao-medica input[name='opcao_medica']:checked").val();
                var id = $("#dialog-opcao-medica").attr("cod");

                if (opcao_escolhida == 6){
                   window.location.href ='../medico/?view=paciente_med&act=listcapmed&id='+id;
                    
                } else if (opcao_escolhida == 7){
                   window.location.href ='../medico/?view=paciente_med&act=listfichaevolucao&id='+id;
                    
                }else {
                    window.location.href = ('../medico/?view=relatorio&opcao=' + opcao_escolhida + '&id=' + id);
                }

                
                $(this).dialog("close");
            },
            Cancelar: function() {

                $(this).dialog("close");
            }
        }
    });
    //fim opções medicas.
    ///opções emfermagem
    $(".botao_relatorios_enfermagem").live('click', function() {
        $("#dialog-opcao-enfermagem").attr("cod", $(this).attr("cod"));
        $("#dialog-opcao-enfermagem").dialog("open");
    });
    
    $("#dialog-opcao-enfermagem").dialog({
        autoOpen: false, modal: true, height: 450, width: 400,
        open: function(event, ui) {
            

            $("#dialog-opcao-enfermagem").html("<br> <fieldset style='width:95%;'><legend><b>Fichas</b></legend>"+
                    "<input type='radio' name='opcao_enfermagem' value='5' checked/>Avalia&ccedil;&atilde;o<br>" +
                    "<input type='radio' name='opcao_enfermagem' value='6' />Evolu&ccedil;&atilde;o<br><br></fieldset>"+
                    "<fieldset style='width:95%;'><legend><b>Relat&oacute;rios</b></legend>"+
                    "<input type='radio' name='opcao_enfermagem' value='1' checked/>Alta Enfermagem<br>" +
                    "<input type='radio' name='opcao_enfermagem' value='2' />Aditivo<br>" +
                    "<input type='radio' name='opcao_enfermagem' value='3' />Prorroga&ccedil;&atilde;o<br>" +
                    "<input type='radio' name='opcao_enfermagem' value='4' />Relat&oacute;rio de Visita Semanal<br>" +
                    "<input type='radio' name='opcao_enfermagem' value='7' />Deflagrado<br></fieldset>");
                     
        },
        buttons: {
            Visualizar: function() {
                var opcao_escolhida = $("#dialog-opcao-enfermagem input[name='opcao_enfermagem']:checked").val();
                var id = $("#dialog-opcao-enfermagem").attr("cod");

                if (opcao_escolhida == 5) {
                    
                    window.location.href = '../enfermagem/?view=listavaliacaenfermagem&id=' + opcao_escolhida + '&id=' + id;
                }else if (opcao_escolhida == 6){
                    window.location.href = '../enfermagem/?view=listevolucaoenfermagem&id=' + opcao_escolhida + '&id=' + id;

                }else if (opcao_escolhida == 7){
                    window.location.href = '../enfermagem/?view=relatorio&opcao=' + 5 + '&id=' + id;

                }else{
                    window.location.href = '../enfermagem/?view=relatorio&opcao=' + opcao_escolhida + '&id=' + id;
                }
                $(this).dialog("close");
            },
            Cancelar: function() {

                $(this).dialog("close");
            }
        }
    });
 
 //fim 
 
 ///jeferson 07-11-2013 mudar grid do listar paciente 
 ///opções secmed
 $(".botao_relatorios_secmed").live('click', function() {
        $("#dialog-opcao-secmed").attr("cod", $(this).attr("cod"));
        $("#dialog-opcao-secmed").dialog("open");
    });
    
    $("#dialog-opcao-secmed").dialog({
        autoOpen: false, modal: true, height: 450, width: 400,
        open: function(event, ui) {
           

            $("#dialog-opcao-secmed").html("<br> <fieldset style='width:95%;'><legend><b>Fichas</b></legend>"+
                    "<input type='radio' name='opcao_secmed' value=1 checked/>Admiss&atilde;o<br>\n\
                     <input type='radio' name='opcao_secmed' value=2 >Hist&oacute;rico de Status<br>\n\
                     <input type='radio' name='opcao_secmed' value=3 >Hist&oacute;rico de Plano e Liminar<br>\n\
                     <input type='radio' name='opcao_secmed' value=4 >Hist&oacute;rico de Mudan&ccedil;a de Modalidade\n\
                     <br><input type='radio' name='opcao_secmed' value=5> Prorroga&ccedil;&atilde;o Planserv<br></fieldset>");
                     
        },
        buttons: {
            Visualizar: function() {
                var opcao_escolhida = $("#dialog-opcao-secmed input[name='opcao_secmed']:checked").val();
                var id = $("#dialog-opcao-secmed").attr("cod");

                if (opcao_escolhida == 1) {
                    //window.open('imprimir_alta.php?id='+id+'&opcao='+opcao_escolhida,'_blank');?view=paciente_med&act=listcapmed&id=
                    window.location.href = '?adm=relatorios&opcao=' + opcao_escolhida + '&id=' + id;
                } else if (opcao_escolhida == 2) {
                    //window.open('imprimir_alta.php?id='+id+'&opcao='+opcao_escolhida,'_blank');?view=paciente_med&act=listcapmed&id=
                    window.location.href = '?adm=relatorios&opcao=' + opcao_escolhida + '&id=' + id;
                } else if (opcao_escolhida == 3) {
                    //window.open('imprimir_alta.php?id='+id+'&opcao='+opcao_escolhida,'_blank');?view=paciente_med&act=listcapmed&id=
                    window.location.href = '?adm=relatorios&opcao=' + opcao_escolhida + '&id=' + id;
                } else if (opcao_escolhida == 4) {
                    //window.open('imprimir_alta.php?id='+id+'&opcao='+opcao_escolhida,'_blank');?view=paciente_med&act=listcapmed&id=
                    window.location.href = '/adm/secmed/?type=relatorio-modalidade-paciente';
                }  else if (opcao_escolhida == 5) {
                   window.location.href = '/adm/secmed/?action=prorrogacao-planserv&id=' + id;
                }else{
                    alert("Em fase de implementaÃ§Ã£o!");
                }

                
                $(this).dialog("close");
            },
            Cancelar: function() {

                $(this).dialog("close");
            }
        }
    });
    //fim opções secmed.
 
  $(".exame").click(function(){
  
    if($(this).is(":checked")){
      var val= $(this).val();
        var nome = $(this).attr('name');
       
        if(val == 1){
          $("span[name='"+nome+"']").hide();
          $("span[name='"+nome+"']").children('textarea').removeClass('OBG');
          $("span[name='"+nome+"']").children('textarea').removeAttr("style");
        }else{
        $("span[name='"+nome+"']").show();
        $("span[name='"+nome+"']").children('textarea').addClass('OBG');
        }
  }  
    
  });
  
 $(".mucosa1").hide();
 $(".mucosa").click(function(){
    
    if($(this).is(":checked")){
      var val= $(this).val();
      
        
        if(val == 's'){
          $(".mucosa1").hide();
        }else{
          $(".mucosa1").show();
          $(".mucosa1").focus();
        }
 }  
    
 });
 
 $(".escleras1").hide();
 $(".escleras").click(function(){
    
    if($(this).is(":checked")){
      var val= $(this).val();
      
        
        if(val == 's'){
          $(".escleras1").hide();
        }else{
          $(".escleras1").show();
          $(".escleras1").focus();
        }
 }  
    
 });

 $(".respiratorio1").hide();
 
 $(".exibir_selecao").hide();
 
 $(".exibir_selecao").each(function(i){
   var obs = $(this).children('select').val();
  
   if(obs != ''){
    $(this).show();
   }else{
     $(this).hide();
   }

 });
 
 var val= $("input[name=respiratorio]:checked").val(); 
 if(val != '1' ){
   $(".outro_respiratorio").hide();
 }else{
   $(".outro_respiratorio").show(); 
 }
 
 $(".respiratorio").click(function(){
    
    if($(this).is(":checked")){
      var val= $(this).val();
      
        
        if(val == 's' ){
          $(".respiratorio1").hide();
          $(".outro_respiratorio").hide();
          $(".outro_respiratorio").children('textarea').removeClass('OBG');
          $(".outro_respiratorio").children('textarea').removeAttr("style");
        }
        
        
        if(val == 'n' ){
          $(".respiratorio1").show();
          $(".outro_respiratorio").hide();
          $(".outro_respiratorio").children('textarea').removeClass('OBG');
          $(".outro_respiratorio").children('textarea').removeAttr("style");
         }
        if(val == '1' ){
          $(".respiratorio1").hide();
          $(".outro_respiratorio").show();
          $(".outro_respiratorio").children('textarea').addClass("OBG");
         // $("textarea[name=outro_respiratorio]").addClass('OBG');
          
        }
        
 }  
    
 });
 
 $("#suplementar_min_span").hide();
 
 $("select[name='tipo_oximetria_min']").change(function() {
  
   var valor = $("select[name='tipo_oximetria_min'] option:selected").val();
   
   if(valor == '2'){//O2 suplementar
     $("#suplementar_min_span").show();
     $("input[name='litros_min']").focus();
   }else{
     $("#suplementar_min_span").hide();
     $("input[name='litros_min']").val('');
     $("select[name='via_oximetria_min']").val('');
   }

});
 
 $("#suplementar_max_span").hide();
 
 $("select[name='tipo_oximetria_max']").change(function() {
    
   var valor = $("select[name='tipo_oximetria_max'] option:selected").val();
   
   if(valor == '2'){//O2 suplementar
     $("#suplementar_max_span").show();
     $("input[name='litros_max']").focus();
   }else{
     $("#suplementar_max_span").hide();
     $("input[name='litros_max']").val('');
     $("select[name='via_oximetria_max']").val('');
   }

});
 
 $(".tipo_oximetria_adicionais").each(function(i){//Mostra os campos que tem informações de litro e via (o2 suplementar)
   var tipo_oximetria_min = $("select[name='tipo_oximetria_min'] option:selected").val();
   var tipo_oximetria_max = $("select[name='tipo_oximetria_max'] option:selected").val();
   
   if(tipo_oximetria_min == '2'){
     $("#suplementar_min_span").show();
   }
   if(tipo_oximetria_max == '2'){
     $("#suplementar_max_span").show();
   }

  });
 
 $("#dor_span").hide();
 
 $("select[name='dor_sn']").change(function(){
   var X = $("select[name='dor_sn'] option:selected").val();
   
    if(X == 's'){
      $("#dor_span").show();
      $("input[name='dor']").focus();
    }else{
      $("#dor_span").hide();
      $("input[name='dor']").val('');
    }
 });
 
 $(".exibir_dor").each(function(i){

   var dor_selecionado = $("select[name='dor_sn'] option:selected").val();
   
   if(dor_selecionado == 's'){
      $("#dor_span").show();
      $("input[name='dor']").focus();
    }else{
      $("#dor_span").hide();
      $("input[name='dor']").val('');
    }

 });
 
 $("#intercorrencias_span").hide();
 
 $("select[name='intercorrencias']").change(function(){
   var X = $("select[name='intercorrencias'] option:selected").val();
   
    if(X == 's'){
      $("#intercorrencias_span").show();
      $("input[name='intercorrencias']").focus();
    }else{
      $("#intercorrencias_span").hide();
      $("input[name='intercorrencias']").val('');
    }
 });
 
 
 $("#add-dor").click(function(){
    var x = $("input[name='dor']").val();
    
    var bexcluir = "<img src='../utils/delete_16x16.png' class='del-dor-ativos' title='Excluir' border='0' >";
    $('#dor').append("<tr><td  class='dor1' name='"+x+"' > "+bexcluir+" <b>Local: </b>"+x+
        "</td><td colspan='2'>Escala analogica:<select name='dor"+x+"' class='COMBO_OBG' style='background-color:transparent;'>" +
            "<option value='-1' selected></option>" +
        "<option value='1'>1</option><option value='2'>2</option>" +
        "<option value='3'>3</option><option value='4'>4</option>" +
        "<option value='5'>5</option>" +
        "<option value='6'>6</option><option value='7'>7</option>" +
        "<option value='8'>8</option><option value='9'>9</option><option value='10'>10</option></select>" +
        "Padr&atilde;o da dor:<input type=text name='padraodor'></td></tr>");
    $("input[name='dor']").val("");
    $("input[name='dor']").focus();
    atualiza_dor_ativos();
    return false;
    
  });

 $("#add-intercorrencias").click(function(){
    var x = $("input[name='intercorrencias']").val();
    
    var bexcluir = "<img src='../utils/delete_16x16.png' class='del-intercorrencias-ativas' title='Excluir' border='0' >";
    
    $('#intercorrencias_tabela').append("<tr><td id='inter' class='interc' name='"+x+"' >"+bexcluir+"<b>Descri&ccedil;&atilde;o: </b>"+x+"</td></tr>");
    
    $("input[name='intercorrencias']").val("");
    $("input[name='intercorrencias']").focus();
    atualiza_intercorrencias_ativas();
    return false;
    
    
  });

 $(".del-intercorrencias-ativas").unbind("click",remove_intercorrencias_ativas).bind("click",remove_intercorrencias_ativas);


 function atualiza_intercorrencias_ativas(){
    $(".del-intercorrencias-ativas").unbind("click",remove_intercorrencias_ativas).bind("click",remove_intercorrencias_ativas);
 }
 
 function remove_intercorrencias_ativas(){
  $(this).parent().parent().remove();
  return false;
 }
 
 //dor
 $(".del-dor-ativos").unbind("click",remove_dor_ativos).bind("click",remove_dor_ativos);


 function atualiza_dor_ativos(){
    $(".del-dor-ativos").unbind("click",remove_dor_ativos)
            .bind("click",remove_dor_ativos);
 }
 
 function remove_dor_ativos(){
  $(this).parent().parent().remove();
  return false;
 }
 
 $("#dreno_span").hide();
 
 $("select[name='dreno_sn']").change(function(){
   var X = $("select[name='dreno_sn'] option:selected").val();
   
    if(X == 's'){
      $("#dreno_span").show();
      $("input[name='dreno']").focus();
    }else{
      $("#dreno_span").hide();
      $("input[name='dreno']").val('');
    }
 });
 
 
 $("#add-dreno").click(function(){
    var x = $("input[name='dreno']").val();
    
    var bexcluir = "<img src='../utils/delete_16x16.png' class='del-dreno-ativos' title='Excluir' border='0' >";
    $('#dreno').append("<tr><td  class='dreno1' name='"+x+"' > "+bexcluir+" <b>Local: </b>"+x+"</td></tr>");
    $("input[name='dreno']").val("");
    
     
    $("input[name='dreno']").focus();
    atualiza_dreno_ativos();
    return false;
    
    
  });
 
 

 
 $(".del-dreno-ativos").unbind("click",remove_dreno_ativos).bind("click",remove_dreno_ativos);


 function atualiza_dreno_ativos(){
    $(".del-dreno-ativos").unbind("click",remove_dreno_ativos)
            .bind("click",remove_dreno_ativos);
 }
 
 function remove_dreno_ativos(){
  $(this).parent().parent().remove();
  return false;
 }
 
 
 ////////cateter
 $("#add-cateter").click(function(){
    var x = $("input[name='cateter']").val();
    
    var bexcluir = "<img src='../utils/delete_16x16.png' class='del-cateter-ativos' title='Excluir' border='0' >";
    $('#cateter').append("<tr><td  class='cateter1' name='"+x+"' colspan='3'> <input type=hidden class='cat_sond1' name='"+x+"'/>"+bexcluir+" "+x+"</td></tr>");
    $("input[name='cateter']").val("");
    
     
    $("input[name='cateter']").focus();
    atualiza_cateter_ativos();
    return false;
    
    
  });




$(".del-cateter-ativos").unbind("click",remove_cateter_ativos).bind("click",remove_cateter_ativos);


function atualiza_cateter_ativos(){
    $(".del-cateter-ativos").unbind("click",remove_cateter_ativos)
            .bind("click",remove_cateter_ativos);
}

function remove_cateter_ativos(){
  $(this).parent().parent().remove();
  return false;
}
 
$("#dialog-validar-radio").dialog({
    autoOpen: false, modal: true, position: 'top',
  
    buttons: {
      
      "Fechar":function(){
        
        $(this).dialog("close");
        
      
        
        
      }
    
    }
  });


 
 //////////////
 
 $("#salvar-ficha-evolucao").click(function(){
    if(validar_campos("div-ficha-evolucao-medica")){
        var dados = $("form").serialize();
        var probativos = "";
        var novosprobativos = "";
        var exames= "";
        var cat_sond='';
        var dor123="";
        var intercorrencias="";
        var total = $(".prob-ativos-evolucao").size();
        var total2 = $(".probativo1").size();
        var total3 = $(".exame").size();
        var total4 = $(".dor1").size();
        var score = '';
        var flag=0;
        $(".exame1").each(function(i){
          var val = $(this).parent().children('td').eq(1).children('input[class=exame]:checked').val();
            var id = $(this).attr("idexame");
            //var val = $(this).val();
            var obs  = $('.obsexame'+id+'').val();
          if(val =='' || val == undefined){
            
            flag=1;
          }
            
            exames += id+"#"+val+"#"+obs;
            
          if(i<total3 - 1) exames += "$";
          
          
        });
        
        var idpaciente = $("input[name='paciente']").val();
        
        $(".cat_sond").each(function(i){
          if($(this).is(':checked')){
            var cat = $(this).attr('name');
             cat_sond += cat+"#";
            
          }
          
        });
        $(".cat_sond1").each(function(i){
          
            var cat = $(this).attr('name');
             cat_sond += cat+"#";
          
          
        });
        
                
        if(!$("input[name='escleras']").is(":checked")){            
          flag = 1;
        }
        
        if(!$("input[name='respiratorio']").is(":checked")){            
          flag = 1;
        }
        
        if(!$("input[name='mucosa']").is(":checked")){            
            flag = 1;
        }
          
        if(!$("input[name='estd_geral']").is(":checked")){            
            flag = 1;
        }
          
        $(".dor1").each(function(i){
          var nome = $(this).attr("name");
          ///var escala = $(this).parent().children('td').eq(1).children("select[name='dor1'] option:selected").val();
          var padrao = $(this).parent().children('td').eq(1).children('input[name=padraodor]').val();
          var escala = $("select[name='dor"+nome+"'] option:selected").val();
          dor123 += nome+"#"+escala+"#"+padrao;
        if(i<total4 - 1) dor123 += "$";
        });
        
        $(".interc").each(function(i){
          var descricao = $(this).attr("name");
        
          intercorrencias += descricao+"#";
        });
        
        $(".prob-ativos-evolucao").each(function(i){
          var cid = $(this).attr("cod");
          var desc = $(this).attr("desc");
          var obs= $(this).parent().children('td').children('textarea').val();
          novosprobativos += cid+"#"+desc+"#"+obs;
        if(i<total - 1) novosprobativos += "$";
        });
        
        
        var y1 = 1; 
        
        $(".probativo1").each(function(i){
          
          var tipo = $(this).parent().children('td').eq(1).children('input[class=probativo]:checked').val();
          var cid  = $(this).attr("cid");
          var desc = $(this).attr("prob_atv");
          var x = $(this).attr("x");
          
          if(tipo == '' || tipo == undefined || tipo == "NaN"){
            
            flag=1;
            
          }
          
          var obs  = $('.obs'+x+'').val();
          y1++;
          probativos += cid+"#"+desc+"#"+tipo+"#"+obs;
        if(i<total2 - 1) probativos += "$";
        });
        
        
        
        var capmed = "query=nova-ficha-evolucao&"+dados+"&probativos="+probativos+"&novosprobativos="+novosprobativos+"&exames="+exames+"&dor123="+dor123+"&cat_sond="+cat_sond+"&intercorrencias="+intercorrencias;
        if(flag == 0){
          
          

      $.ajax({
          type: "POST",
          url: "query.php",
          data: capmed,
          success: function(msg){
            
            
            if(msg >= 0){
              window.location.href='?adm=paciente&act=listfichaevolucao&id='+$("input[name='paciente']").val(); 
               
            }else{
              alert("DADOS: "+msg);
            }
            
            //window.location.href='?adm=paciente&act=listcapmed&id='+$("input[name='paciente']").val();            
          }
        
        });
        }else {
          $("#dialog-validar-radio").dialog('open');
        }
        
      }
      return false;
    });
  
  
  
  
  function validar_campos(div){
    var ok = true;
    $("#"+div+" .OBG").each(function (i){
      if(this.value == ""){
    ok = false;
    this.style.border = "3px solid red";
      } else {
    this.style.border = "";
      }
    });
    $("#"+div+" .COMBO_OBG option:selected").each(function (i){
      if(this.value == "-1"){
    ok = false;
    $(this).parent().css({"border":"3px solid red"});
      } else {
    $(this).parent().css({"border":""});
      }
    });
    
    if(!$("#"+div+" input[name='escleras']").is(":checked")){
      $("#"+div+" input[name='escleras']").parent().css({"border":"3px solid red"});
    }else {
      $("#"+div+" input[name='escleras']").parent().css({"border":""});
    }
    
    if(!$("#"+div+" input[name='respiratorio']").is(":checked")){
      $("#"+div+" input[name='respiratorio']").parent().css({"border":"3px solid red"});
    }else {
      $("#"+div+" input[name='respiratorio']").parent().css({"border":""});
    }
    
    if(!$("#"+div+" input[name='mucosa']").is(":checked")){
      $("#"+div+" input[name='mucosa']").parent().css({"border":"3px solid red"});
    }else {
      $("#"+div+" input[name='mucosa']").parent().css({"border":""});
    }
    
    if(!$("#"+div+" input[name='estd_geral']").is(":checked")){
      $("#"+div+" input[name='estd_geral']").parent().css({"border":"3px solid red"});
    }else {
      $("#"+div+" input[name='estd_geral']").parent().css({"border":""});
    }
    
    $("#"+div+" .exame1").each(function(i){
      var val = $(this).parent().children('td').eq(1).children("input").eq(0).attr("name");
        
        if(!$("#"+div+" input[name='"+val+"']").is(":checked")){
            $("#"+div+" input[name='"+val+"']").parent().css({"border":"3px solid red"});
            
          }else {
            $("#"+div+" input[name='"+val+"']").parent().css({"border":""});
            
          }
      
    });

      $("#"+div+" .OBG_CHOSEN option:selected").each(function (i){

          if(this.value == "" || this.value == -1){
              ok = false;
              $(this).parent().parent().children('div').css({"border":"3px solid red"});
          } else {

              var estilo = $(this).parent().parent().children('div').attr('style');
              $(this).parent().parent().children('div').removeAttr('style');
              $(this).parent().parent().children('div').attr('style',estilo.replace("border: 3px solid red",''));
          }
      });

    
    if(!ok){
      $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
      $("#"+div+" .div-aviso").show();
    } else {
      $("#"+div+" .aviso").text("");
      $("#"+div+" .div-aviso").hide();
    }
    return ok;
  }
  
  $("input[name='busca-diagnostico']").autocomplete({
      source: "/utils/busca_cid.php",
      minLength: 3,
      select: function( event, ui ) {
          $("input[name='busca-diagnostico']").val('');
          $("input[name='cid-diagnostico']").val(ui.item.id);
          $("input[name='diagnostico']").val(ui.item.name);
          $('#local').focus();
          $('#busca-diagnostico').val('');
          return false;
      }
  });
  
  /*#######################busca-CIDADE#######################*/
  
  $("input[name='cidade_und']").autocomplete({
    source: "/utils/busca_cidade.php",
    minLength: 3,
    select: function( event, ui ) {
        //$("input[name='busca-cidade']").val('');
        $("input[name='id_cidade_und']").val(ui.item.id);
        $("input[name='cidade_und']").val(ui.item.value);
        $('#local').focus();
        ///$('#busca-cidade').val('');
        return false;
    },
});
  
  $("input[name='cidade_domiciliar']").autocomplete({
    source: "/utils/busca_cidade.php",
    minLength: 3,
    select: function( event, ui ) {
        //$("input[name='busca-cidade']").val('');
        $("input[name='id_cidade_domiciliar']").val(ui.item.id);
        $("input[name='cidade_domiciliar']").val(ui.item.value);
        $('#local').focus();
        ///$('#busca-cidade').val('');
        return false;
    },
});
  
  $("input[name='cidade_internado']").autocomplete({
    source: "/utils/busca_cidade.php",
    minLength: 3,
    select: function( event, ui ) {
        //$("input[name='busca-cidade']").val('');
        $("input[name='id_cidade_internado']").val(ui.item.id);
        $("input[name='cidade_internado']").val(ui.item.value);
        $('#local').focus();
        ///$('#busca-cidade').val('');
        return false;
    },
}); 

  $(".outro_ativo").hide();
  $("#outros-prob-ativos").click(function(){
  if($(this).is(":checked")){

    $(".outro_ativo").show();
  }else{
    $(".outro_ativo").hide();
  }
  
});
  
  $(".outro_ativo_evolucao").hide();
  $("#outros-prob-ativos-evolucao").click(function(){
  if($(this).is(":checked")){

    $(".outro_ativo_evolucao").show();
  }else{
    $(".outro_ativo_evolucao").hide();
  }
  
});
  

  $("#add-prob-ativo").click(function(){
    var x = $("input[name='outro-prob-ativo']").val();
    
    var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prob-ativos' title='Excluir' border='0' >";
    $('#problemas_ativos').append("<tr><td colspan='2' class='prob-ativos' cod='1111' desc='"+x+"'> "+bexcluir+" "+x+"</td></tr>");
    $("input[name='busca-problemas-ativos']").val("");
    $("input[name='outro-prob-ativo']").val("");
     $("#outros-prob-ativos").attr('checked',false);
     $(".outro_ativo").hide();
    $("input[name='busca-problemas-ativos']").focus();
    atualiza_rem_problemas_ativos();
    return false;
    
    
  });
  
  
  $("input[name='busca-problemas-ativos']").autocomplete({
      source: "/utils/busca_cid.php",
      minLength: 3,
      select: function( event, ui ) {
        
        var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prob-ativos' title='Excluir' border='0' >";
        $('#problemas_ativos').append("<tr><td colspan='2' class='prob-ativos' cod='"+ui.item.id+"' desc='"+ui.item.name+"'> "+bexcluir+" "+ui.item.name+"</td></tr>");
        $("input[name='busca-problemas-ativos']").val("");
        $("input[name='busca-problemas-ativos']").focus();
        atualiza_rem_problemas_ativos();
        
        return false;
      },
  });
  
  $("#add-prob-ativo-evolucao").click(function(){
    var x = $("input[name='outro-prob-ativo-evolucao']").val();
    
    var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prob-ativos' title='Excluir' border='0' >";
    $('#problemas_ativos').append("<tr><td colspan='2' class='prob-ativos-evolucao' cod='1111' desc='"+x+"'> "+bexcluir+" "+x+" <textarea cols=70 rows=2  class='OBG' ></textarea></td></tr>");
    $("input[name='busca-problemas-ativos-evolucao']").val("");
    $("input[name='outro-prob-ativo-evolucao']").val("");
     $("#outros-prob-ativos-evolucao").attr('checked',false);
     $(".outro_ativo").hide();
    $("input[name='busca-problemas-ativos-evolucao']").focus();
    atualiza_rem_problemas_ativos_evolucao();
    return false;
    
    
  });


$("input[name='busca-problemas-ativos-evolucao']").autocomplete({
      source: "/utils/busca_cid.php",
      minLength: 3,
      select: function( event, ui ) {
        
        var bexcluir = "<img src='../utils/delete_16x16.png' class='del-prob-ativos' title='Excluir' border='0' >";
        $('#problemas_ativos').append("<tr><td colspan='2' class='prob-ativos-evolucao' cod='"+ui.item.id+"' desc='"+ui.item.name+"'> "+bexcluir+" "+ui.item.name+" <textarea cols=70 rows=3  class='OBG' ></textarea></td></tr>");
        $("input[name='busca-problemas-ativos-evolucao']").val("");
        $("input[name='busca-problemas-ativos-evolucao']").focus();
        atualiza_rem_problemas_ativos_evolucao();
        
        return false;
      },
});
  /*##############  equipamento capmed///////////*/
  
  
  $(".qtd_equipamento").keyup(function() {
     var valor = $(this).val().replace(/[^0-9]/g,'');
     $(this).val(valor);
  });
   
  $(".equipamento").click(function() {
    x = $(this).attr('cod_equipamento');
    
    
    if($(this).is(":checked")){
      $("#obs"+x).attr('readonly',false); 
      $("#obs"+x).focus();
      $("#qtd"+x).attr('readonly',false);
     $("#qtd"+x).addClass("OBG");
      
      
    }else{
      $("#obs"+x).attr('readonly',true);      
      $("#qtd"+x).attr('readonly',true);
      $("#qtd"+x).removeClass("OBG");
      $("#qtd"+x).removeAttr("style");
    }
    
  });
  
  
  /*#######mascara telefones ##############*/
  $("#telPosto").keyup(function() {
     var valor = $(this).val().replace(/[^0-9,(,),-]/g,'');
     $(this).val(valor);
  });
  $("#telContato").keyup(function() {
     var valor = $(this).val().replace(/[^0-9,(,),-]/g,'');
     $(this).val(valor);
  });
  
  $("#telResponsavel").keyup(function() {
     var valor = $(this).val().replace(/[^0-9,(,),-]/g,'');
     $(this).val(valor);
  });
  $("#telCuidador").keyup(function() {
     var valor = $(this).val().replace(/[^0-9,(,),-]/g,'');
     $(this).val(valor);
  });
  $("#telDomiciliar").keyup(function() {
     var valor = $(this).val().replace(/[^0-9,(,),-]/g,'');
     $(this).val(valor);
  });
  $("#telInternacao").keyup(function() {
     var valor = $(this).val().replace(/[^0-9,(,),-]/g,'');
     $(this).val(valor);
  });
/*########################ESPECILIDADE_RELACIONAMENTO###################################*/  
  
 
  
  $("select[name='relresp']").change(function(){
   var X = $("select[name='relresp'] option:selected").val();
   
    if(X==12){
      $(".espresp").show();
      $(".espresp input").focus();
    }else{
      $(".espresp").hide();
      $(".espresp input[name='especialidadeResponsavel']").val(' ');
    }
  });
  
  $("select[name='relcuid']").change(function(){
     var X = $("select[name='relcuid'] option:selected").val();
     
      if(X==12){
        $(".espcuid").show();
        $(".espcuid input").focus();
      }else{
        $(".espcuid input[name='especialidadeCuidador']").val(' ');
        $(".espcuid").hide();
      }
    });
  
/* ######################## OBSERVA��O ASSOCIADA ################################ */
  
  // AutoComplete de Observa��o Associada
  $("input[name='busca']").autocomplete({
    source: "../utils/busca_principio_obs.php",
    minLength: 3,
    select: function( event, ui ) {
        $("input[name='busca']").val('');     
        $('#nome').val(ui.item.princ);
        $('#nome').attr('readonly','readonly');
        $('#cod').val(ui.item.id);
          $('#obs').val(ui.item.obs);       
          $('#busca').val('');
          
          return false;
    }
});
  
// Incluir Observ�ao Associadoa  
$(".incluir").click(function(){
      var p = $("#obs").val();
      var f = $("#cod").val();
      
      $.post("query.php",{query: "salvar-obs", obs: p, cod: f},function(r){
        alert('Registro alterado com Sucesso!');
      });
      return false;
});

/*###################################################################################*/  

/* ######################## EDI��O DE PRODUTOS INTERNOS ################################ */

// AutoComplete de Edi��o de Produto Interno
$("input[name='busca']").autocomplete({
    source: "../utils/busca_principio_produtoInterno.php",
    minLength: 3,
    select: function( event, ui ) {
        $("input[name='busca']").val('');     
        $('#nome').val(ui.item.princ);
        $('#nomeAtual').val(ui.item.princ);
        $('#nomeAtual').attr('readonly','readonly');
        $('#cod').val(ui.item.id);
              
        $('#busca').val('');
        
        return false;
    }
});

//////////editar prescricao/////////////
$("#editar_ficha").click(function(){
  
  if(validar_campos("div-ficha-medica")){
      var dados = $("form").serialize();
      var probativos = "";
      var total = $(".prob-ativos").size();
      
      var score = '';
      $("input[class='abmid']:checked").each(function(i){
        score = score + $(this).attr('cod_item')+"#";
        
      });
      
      $("input[class='petro']:checked").each(function(i){
        score = score + $(this).attr('cod_item')+"#";
        
      });
      $(".prob-ativos").each(function(i){
        var cid = $(this).attr("cod");
        var desc = $(this).attr("desc");
        probativos += cid+"#"+desc;
      if(i<total - 1) probativos += "$";
      });
      var equipamentos='';
      var linha2='';
       var total2 = $(".equipamento").size();
        $("input[class='equipamento']:checked").each(function(i){
          var x = $(this).attr('cod_equipamento');
        
        linha2 = $(this).attr('cod_equipamento')+"#"+$("#obs"+x).val()+"#"+$("#qtd"+x).val();
            
            if(i<total2 - 1) linha2 += "$";
          equipamentos += linha2;
        
        }); 
        
      var totalcab = $(".cabec1").size();
      var linhax ='';
      var cab ='';
      $(".cabec1").each(function(i){
        
        if($(this).val() != null && $(this).val() !=''){
          linhax = $(this).attr('cod_item')+"#"+$(this).val();
          
                
            if(i<totalcab - 1) linhax += "$";
            cab += linhax;
          
        }  
        
        
      });
       
        
      var capmed = "query=editar-ficha-avaliacao&"+dados+"&probativos="+probativos+"&score="+score+"&equipamentos="+equipamentos+"&cab_score="+cab;
      $.ajax({
        type: "POST",
        url: "query.php",
        data: capmed,
        success: function(msg){
          if(msg >=0){
            
            alert("Dados editados com sucesso! ");
            window.location.href='?adm=paciente&act=listcapmed&id='+$("input[name='paciente']").val();
             
          }else{
            alert("DADOS: "+msg);
          }
          
          //window.location.href='?adm=paciente&act=listcapmed&id='+$("input[name='paciente']").val();            
        }
      
      });
    }
    return false;
  
  
});




//Editar Produto Interno  
$(".incluir").click(function(){
      var p = $("#nome").val();
      var f = $("#cod").val();
      
      $.post("query.php",{query: "salvar-produtoInterno", nome: p, cod: f},function(r){
        if(r){
          alert('Registro alterado com Sucesso!');
        }
        
      });
      return false;
});

/*###################################################################################*/  

/*Scores q v�o aparecer*/


$(".del-prob-ativos").unbind("click",remove_problemas_ativos).bind("click",remove_problemas_ativos);


  function atualiza_rem_problemas_ativos(){
    $(".del-prob-ativos").unbind("click",remove_problemas_ativos)
            .bind("click",remove_problemas_ativos);
  }
  
  function remove_problemas_ativos(){
    $(this).parent().parent().remove();
    return false;
  }
  
  
  
  $(".del-prob-ativos").unbind("click",remove_problemas_ativos_evolucao).bind("click",remove_problemas_ativos_evolucao);


  function atualiza_rem_problemas_ativos_evolucao(){
    $(".del-prob-ativos").unbind("click",remove_problemas_ativos_evolucao)
            .bind("click",remove_problemas_ativos_evolucao);
  }
  
  function remove_problemas_ativos_evolucao(){
    $(this).parent().parent().remove();
    return false;
  }
  
  $("input[name='palergiamed']").change(function(){
    if($(this).val() == "S" || $(this).val() == "s"){
      $("input[name='alergiamed']").addClass("OBG");
      $("input[name='alergiamed']").attr("readonly","");
    } else { 
      $("input[name='alergiamed']").removeClass("OBG");
      $("input[name='alergiamed']").attr("readonly","readonly");
      $("input[name='alergiamed']").val("");
    }
  });
  
  $("input[name='palergiaalim']").change(function(){
    if($(this).val() == "S" || $(this).val() == "s"){
      $("input[name='alergiaalim']").addClass("OBG");
      $("input[name='alergiaalim']").attr("readonly","");
    } else{ 
      $("input[name='alergiaalim']").removeClass("OBG");
      $("input[name='alergiaalim']").attr("readonly","readonly");
      $("input[name='alergiaalim']").val("");
    }
  });
  
  $("input[name='qtdanteriores']").change(function(){
    if($(this).val() != "0" && $(this).val() != ""){
      $("input[name='historicointernacao']").addClass("OBG");
      $("input[name='historicointernacao']").attr("readonly","");
    } else{ 
      $("input[name='historicointernacao']").removeClass("OBG");
      $("input[name='historicointernacao']").attr("readonly","readonly");
      $("input[name='historicointernacao']").val("");
    }
  });
  
  $(".hosp").click(function(){
    var score = 0;
    $("input[class='hosp']:checked").each(function(i){
      score = score + parseInt($(this).val());
    });
    $("input[name='shosp']").val(score.toString()); 
  });
  
  $(".domic").click(function(){
    var score = 0;
    $("input[class='domic']:checked").each(function(i){
      score = score + parseInt($(this).val());
    });
    $("input[name='sdom']").val(score.toString());  
  });
  
  
$(".limpar").click(function(){
   
    $(this).parent().children('input').attr('checked',false);
    var score = 0;
      var cont = 0;
      var texto='';
      var j=0;
   
    $("input[class='abmid']:checked").each(function(){
     
        if($(this).val()==5){
          cont++;
        }
        score = score + parseInt($(this).val());
        
        j=score; 
        
        if(j <= 7 && cont == 0){
            texto="Score do paciente segundo a tabela ABEMID = "+j+".";
          } else if(j <= 7 && cont== 1){
            texto= "Score do paciente segundo a tabela ABEMID = "+j+".";
          }else if(j >= 8 && j <= 12 && cont == 0){
            texto= "Score do paciente segundo a tabela ABEMID = "+j+".";
          }else if(j >= 8 && j <= 12 && cont == 1){
            texto= "Score do paciente segundo a tabela ABEMID = "+j+".";
          }else if(j >= 8 && j <= 12 && cont == 2){
            texto= "Score do paciente segundo a tabela ABEMID = "+j+".";
          }else if(j >= 13 && j <= 18 && cont < 2){
            texto= "Score do paciente segundo a tabela ABEMID = "+j+".";
          }else if(j >= 13 && j <= 18 && cont >= 2){
            texto= "Score do paciente segundo a tabela ABEMID = "+j+".";
          }else if(j >= 19 ){
            texto= "Score do paciente segundo a tabela ABEMID = "+j+".";
          }
      });
    
    
      
      $("input[name='sabmid']").val(score.toString());
      $("input[name='sabmid1']").val(cont.toString());
      $("#prejustparecer").val(texto.toString());
    
 });


$(".limpar2").click(function(){
   
    $(this).parent().children('input').attr('checked',false);
    var score = 0;
      var cont = 0;
      var texto='';
      var j=0;
      var x =0;
      $("input[class='petro']:checked").each(function(i){
          
        if($(this).val() == 5){
          cont++;
        }
        
        if($("input[name='supvent']:checked").val() == 5){
          x=1;
          
        }
        
      
        score = score + parseInt($(this).val());
        
        j=score; 
        
        if(j <= 7 && cont == 0 && x != 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          } 
        else if(j <= 7 && cont == 0 && x == 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          } 
        else if(j <= 7 && cont == 1 && x != 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+". ";
          }
        else if(j <= 7 && cont == 1 && x == 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+". ";
          }
        else if(j >= 8 && j <= 12 && cont == 0 && x != 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j >= 8 && j <= 12 && cont == 0 && x == 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+". ";
          }
        else if(j >= 8 && j <= 12 && cont == 1 && x != 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j >= 8 && j <= 12 && cont == 1 && x == 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+". ";
          }
        else if(j >= 8 && j <= 12 && cont == 2  && x != 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j >= 8 && j <= 12 && cont == 2  && x == 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j >= 13 && j <= 18 && cont == 0 && x != 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j >= 13 && j <= 18 && cont == 0 && x == 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j >= 13 && j <= 18 && cont >= 2 && x != 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j >= 13 && j <= 18 && cont >= 2 && x == 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
             else if(j >= 19 && x != 1 ){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        
      });
      $("input[name='spetro']").val(score.toString());
      $("input[name='spetro1']").val(cont.toString());
      /*$("input[name='spetro2']").val(texto.toString());*/
      
    
      $("#prejustparecer").val(texto.toString());
    
});
  
  $(".abmid").click(function(){
      var score = 0;
      var cont = 0;
      var texto='';
      var j=0;
      
      $("input[class='abmid']:checked").each(function(i){
        if($(this).val()==5){
          cont++;
        }
        
        
          
        score = score + parseInt($(this).val());
        
        j=score; 
        
        
        if(j <= 7 && cont == 0){
            texto="Score do paciente segundo a tabela ABEMID = "+j+".";
          } else if(j <= 7 && cont== 1){
            texto= "Score do paciente segundo a tabela ABEMID = "+j+".";
          }else if(j >= 8 && j <= 12 && cont == 0){
            texto= "Score do paciente segundo a tabela ABEMID = "+j+".";
          }else if(j >= 8 && j <= 12 && cont == 1){
            texto= "Score do paciente segundo a tabela ABEMID = "+j+".";
          }else if(j >= 8 && j <= 12 && cont == 2){
            texto= "Score do paciente segundo a tabela ABEMID = "+j+".";
          }else if(j >= 13 && j <= 18 && cont < 2){
            texto= "Score do paciente segundo a tabela ABEMID = "+j+".";
          }else if(j >= 13 && j <= 18 && cont >= 2){
            texto= "Score do paciente segundo a tabela ABEMID = "+j+".";
          }else if(j >= 19 ){
            texto= "Score do paciente segundo a tabela ABEMID = "+j+". ";
          }
      });
    
      
      
      $("input[name='sabmid']").val(score.toString());
      $("input[name='sabmid1']").val(cont.toString());
      $("#prejustparecer").val(texto.toString());
    });
  
  $("#voltar").click(function(){
     history.go(-1); 
    });
  
  
  $(".petro").click(function(){
      var score = 0;
      var cont = 0;
      var j = 0;
      var x = 0;
      
      $("input[class='petro']:checked").each(function(i){
                
        if($(this).val() == 5){
          cont++;
        }
        
        if($("input[name='supvent']:checked").val() == 5){
          x=1;
          
        }
        
      
        score = score + parseInt($(this).val());
        
        j=score; 
        
        if(j <= 7 && cont == 0 && x != 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          } 
        else if(j <= 7 && cont == 0 && x == 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          } 
        else if(j <= 7 && cont == 1 && x != 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j <= 7 && cont == 1 && x == 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j >= 8 && j <= 12 && cont == 0 && x != 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j >= 8 && j <= 12 && cont == 0 && x == 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j >= 8 && j <= 12 && cont == 1 && x != 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j >= 8 && j <= 12 && cont == 1 && x == 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j >= 8 && j <= 12 && cont == 2  && x != 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j >= 8 && j <= 12 && cont == 2  && x == 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j >= 13 && j <= 18 && cont == 0 && x != 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j >= 13 && j <= 18 && cont == 0 && x == 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j >= 13 && j <= 18 && cont >= 2 && x != 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        else if(j >= 13 && j <= 18 && cont >= 2 && x == 1){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
             else if(j >= 19 && x != 1 ){
            texto= "Score do paciente segundo a tabela Petrobras = "+j+".";
          }
        
      });
      $("input[name='spetro']").val(score.toString());
      $("input[name='spetro1']").val(cont.toString());
      /*$("input[name='spetro2']").val(texto.toString());*/
      
    
      $("#prejustparecer").val(texto.toString());
    });
  
  $(".obj").click(function(){
    var score = 0;
    $("input[class='obj']:checked").each(function(i){
      score = score + parseInt($(this).val());
    });
    $("input[name='sobj']").val(score.toString());  
  });
  
 
  
  
 
  
  
  $("#salvar-capmed").click(function(){
    if(validar_campos("div-ficha-medica")){
      var dados = $("form").serialize();
      var probativos = "";
      var total = $(".prob-ativos").size();
      
      var score = '';
      $("input[class='abmid']:checked").each(function(i){
        score = score + $(this).attr('cod_item')+"#";
        
      });
      
      $("input[class='petro']:checked").each(function(i){
        score = score + $(this).attr('cod_item')+"#";
        
      });
      $(".prob-ativos").each(function(i){
        var cid = $(this).attr("cod");
        var desc = $(this).html();
        probativos += cid+"#"+desc;
      if(i<total - 1) probativos += "$";
      });
      var equipamentos='';
      var linha2='';
       var total2 = $(".equipamento").size();
        $("input[class='equipamento']:checked").each(function(i){
          var x = $(this).attr('cod_equipamento');
        
        linha2 = $(this).attr('cod_equipamento')+"#"+$("#obs"+x).val()+"#"+$("#qtd"+x).val();
            
            if(i<total2 - 1) linha2 += "$";
          equipamentos += linha2;
        
        }); 
        
      var totalcab = $(".cabec1").size();
      var linhax ='';
      var cab ='';
      $(".cabec1").each(function(i){
        
        if($(this).val() != null && $(this).val() !=''){
          linhax = $(this).attr('cod_item')+"#"+$(this).val();
          
                
            if(i<totalcab - 1) linhax += "$";
            cab += linhax;
          
        }  
        
        
      });
       
        
      var capmed = "query=nova-capmed&"+dados+"&probativos="+probativos+"&score="+score+"&equipamentos="+equipamentos+"&cab_score="+cab;
      $.ajax({
        type: "POST",
        url: "query.php",
        data: capmed,
        success: function(msg){
          
          
          if(msg >= 0){
            $("#capmed_ava").attr('idcap',msg); 
            $("#dialog-presc-avaliacao").dialog("open");
             
          }else{
            alert("DADOS: "+msg);
          }
          
          //window.location.href='?adm=paciente&act=listcapmed&id='+$("input[name='paciente']").val();            
        }
      
      });
    }
    return false;
  });
  
  $(".presc_avaliacao").click(function(){
    $("#dialog-presc-avaliacao").dialog("open");
    var val= $(this).attr('val');
    $("#capmed_ava").attr('idcap',val);
    $("#tipoav").attr('value',tipo);
   
  });
  
  $("#presc_adt_avaliacao").click(function(){
    $("#dialog-presc-avaliacao-aditiva").dialog("open");
    tipo= $(this).attr('tipo');
    
    var val= $(this).attr('id_cap');
    $("#capmed_ava").attr('idcap',val);
   
   
   
  });
  
  
  
  $(".editar_prescricao").click(function(){
    $("#dialog-editar-presc-avaliacao").dialog("open");
         paciente =$(this).attr('paciente');  
          antigas =$(this).attr('antiga');
          idcap= $(this).attr('id_cap');
         $("#edt").attr('edt',1);
         $("#edtidcap").attr('value',idcap);
         $("#edt_idpresc").attr('value',antigas);
         $("#paciente").attr('value',paciente);
    
  });
  
  $("#dialog-editar-presc-avaliacao").dialog({
      autoOpen: false, modal: true, width: 400, position: 'top',
      close: function(event,ui)
      {
         window.location.href='/adm/?adm=paciente&act=listar';
      },
      open: function(event,ui){
        
      }
  });
      
      $("#dialog-presc-avaliacao-aditiva").dialog({
        autoOpen: false, modal: true, width: 400, position: 'top',
        close: function(event,ui)
        {
           window.location.href='/adm/?adm=paciente&act=listar';
        },
        open: function(event,ui){
          
        }
});
  $("#iniciar_editar_pres_avaliacao").click(function(){
    if(validar_campos("dialog-editar-presc-avaliacao")){
    window.location.href='/medico/?view=prescricao-avaliacao&paciente='+$("#paciente").val()+
    '&inicio='+$("input[name='inicio']").val()+'&fim='+$("input[name='fim']").val()+'&tipo='+$("#tipoav").val()+'&id_cap='+$("#edtidcap").val()+'&antigas='+$("#edt_idpresc").val()+'&edt=1'; ;
    }
    });
  
  $("#iniciar_pres_avaliacao").click(function(){
    if(validar_campos("dialog-presc-avaliacao")){
    window.location.href='/medico/?view=prescricao-avaliacao&paciente='+$("#idpacav").val()+
    '&inicio='+$("#dialog-presc-avaliacao input[name='inicio']").val()+'&fim='+$("#dialog-presc-avaliacao input[name='fim']").val()+'&tipo= 4'+'&antigas=-1'+'&id_cap='+$("#capmed_ava").attr('idcap'); ;
    }
    });
  
  
  $("#iniciar_pres_avaliacao_aditiva").click(function(){
    if(validar_campos("dialog-presc-avaliacao-aditiva")){
    window.location.href='/medico/?view=prescricao-avaliacao&paciente='+$("#idpacav").val()+
    '&inicio='+$("#dialog-presc-avaliacao-aditiva input[name='inicio']").val()+'&fim='+$("#dialog-presc-avaliacao-aditiva input[name='fim']").val()+'&tipo='+$("#tipoav").val()+'&antigas=-1'+'&id_cap='+$("#capmed_ava").attr('idcap'); ;
    }
    });
  
  $("#dialog-presc-avaliacao").dialog({
      autoOpen: false, modal: true, width: 400, position: 'top',
      close: function(event,ui)
      {
         window.location.href='/adm/?adm=paciente&act=listar';
      },
      open: function(event,ui){
        
      }
      
       
      /*buttons: {
        "Iniciar" : function(){
          window.location.href='/medico/?view=prescricao-avaliacao&paciente='+$("#idpacav").val()+
          '&inicio='+$("input[name='inicio']").val()+'&fim='+$("input[name='fim']").val()+'&tipo='+$("#tipoav").val()+'&antigas=-1';
        }
        
        
      }*/
    });

  
  $("input[name='inicio']").click(function() {
  var inicio = $("input[name='inicio']").val();
  var fim = $("input[name='fim']").val();
  
  inicio = inicio.split("/").reverse();
  
  alert(inicio);
  
  });
  
  

  $(".nova_presc_avaliacao").click(function(){
    $("#dialog-novapresc").dialog("open");
         paciente =$(this).attr('paciente');  
          antigas =$(this).attr('antiga');
          id_cap = $(this).attr('id_cap');
          tipo = $(this).attr('tipo');
        
         $("#nedt_idpresc").attr('value',antigas);
         $("#npaciente").attr('value',paciente);
         $("#ncapmed_ava").attr('value',id_cap);
         $("#tipoav").attr('value',tipo);
    
  });
  
  $("#iniciar_nova_presc_avaliacao").click(function(){
    if(validar_campos("dialog-novapresc")){
    window.location.href='/medico/?view=prescricao-avaliacao&paciente='+$("#npaciente").val()+'&inicio='+$("#dialog-novapresc input[name='inicio']").val()+'&fim='+$("#dialog-novapresc input[name='fim']").val()+'&tipo='+$("#tipoav").val()+'&antigas='+$('#nedt_idpresc').val()+'&id_cap='+$("#ncapmed_ava").val(); ;
    }
    });
  
  $("#dialog-novapresc").dialog({
      autoOpen: false, modal: true, width: 400, position: 'top',
      close: function(event,ui)
      {
         window.location.href='/adm/?adm=paciente&act=listar';
      },
      open: function(event,ui){
        
      }
      
       
      
    });

    $(".atualizar-ultima-alteracao").live('click', function(){
        var $this   = $(this);
        var $pElem  = $this.parent();
        var idHist  = $pElem.find('.historico-id').val();
        var dataAut = $pElem.find('.ultima-data-autorizacao').val();
        var horaAut = $pElem.find('.ultima-hora-autorizacao').val();
        var dataAdm = $pElem.find('.ultima-data-admissao').val();
        var horaAdm = $pElem.find('.ultima-hora-admissao').val();
        var data    = {
            id: idHist,
            dataAut: dataAut,
            horaAut: horaAut,
            dataAdm: dataAdm,
            horaAdm: horaAdm
        };

        if(dataAut != '' && horaAut != '' && dataAdm != '' && horaAdm != '') {
            $.post("query.php", {
                query: "salvar-dados-ultima-alteracao",
                data: data
            }, function (r) {
                console.log(r);
                if (r == '1') {
                    alert('Dados atualizados com sucesso!');
                } else {
                    alert('Houve um erro ao atualizar os dados da ultima alteracao de status!');
                }
            });
        } else {
            alert('Preencha os dados corretamente!');
            $pElem.find('.ultima-data-autorizacao').focus();
        }
    });

    $(".statusp").live('click', function(){
        $("#dialog-status").attr("cod",$(this).attr("cod"));
        $("#dialog-status").attr("ur",$(this).attr("ur"));
        $("#dialog-status").dialog("open");
    });

    $("#dialog-status").dialog({
        autoOpen: false, modal: true, height: 450, width: 800,
        open: function(event, ui) {
            $.post("query.php",{query: "show-status-paciente", cod: $("#dialog-status").attr("cod")},function(r){
                $("#dialog-status").html(r);
                data_hora();
            });
        },
        //beforeClose: function(event, ui) {window.document.location.href = '';},
        buttons: {
            Salvar: function() {
                if(validar_campos("dialog-status")){
                    var pstatus = $("#dialog-status input[name='status']:checked").attr("s");
                    var observacao = $("#status-observacao").val();
                    var data_autorizacao= $("#data-autorizacao").val();
                    var data_admissao= $("#data-admissao").val();
                    var hora_autorizacao= $("#hora-autorizacao").val();
                    var hora_admissao= $("#hora-admissao").val();
                    var tipo= $("#status-tipo").val();
                    var id_historico_status=$("#id_historico_status").val();
                    var capmedica_id =$("#capmedica_id").val();


                    $.post("query.php",{query: "salvar-status-paciente", cod: $("#dialog-status").attr("cod"), pstatus: pstatus,
                            observacao: observacao,
                            data_autorizacao:data_autorizacao,
                            data_admissao:data_admissao,
                            hora_autorizacao:hora_autorizacao,
                            hora_admissao:hora_admissao,
                            tipo:tipo,
                            id_historico_status:id_historico_status,
                            capmedica_id:capmedica_id,
                            ur:$("#dialog-status").attr("ur")},
                        function(r){
                            if(r=="1"){
                                window.document.location.href = '';
                            }else if(r > 1){
                                window.document.location.href ='/adm/?adm=formulario_admissao&msg='+r+'&tipo=0';
                            }
                            else{
                                alert(r);
                            }
                        })
                    $( this ).dialog( "close" );
                }else{
                    return false;
                }
            },
            Cancelar: function(){
                $(this).dialog( "close" );
            }
        }
    });

    $(".modhc").live('click',function(){
        $("#dialog-modalidade").attr("cod",$(this).attr("cod"));
        $("#dialog-modalidade").dialog("open");
    });

    $("#dialog-modalidade").dialog({
        autoOpen: false, modal: true, height: 450, width: 600,
        open: function(event, ui) {
            $.post("query.php",{query: "show-modalidade-paciente", cod: $("#dialog-modalidade").attr("cod")},function(r){
                $("#dialog-modalidade").html(r);
            });
        },
        buttons: {
            Salvar: function() {
                if(validar_campos("dialog-modalidade")){

                    var modalidade = $("#dialog-modalidade input[name='modalidade']:checked").attr("m");
                    var data_mudanca = $("#dialog-modalidade input[name='data_mudanca']").val();

                    $.post("query.php",{
                            query: "salvar-modalidade-paciente",
                            paciente: $("#dialog-modalidade").attr("cod"),
                            modalidade: modalidade,
                            data_mudanca: data_mudanca
                        },
                        function(r){
                            if(r=="1") {
                                alert('Modalidade atualizada com sucesso!');
                                window.document.location.href = '';
                            }else
                                alert('Ocorreu um erro inesperado. Entre em contato com o Setor de TI.');
                        });
                    $( this ).dialog( "close" );
                }else{
                    return false;
                }
            },
            Cancelar: function(){
                $(this).dialog( "close" );
            }
        }
    });
  
  $(".permissoes").click(function(){
    $("#dialog-acesso").attr("cod",$(this).attr("cod"));
    $("#dialog-acesso").dialog("open");
  });
  
  $("#dialog-acesso").dialog({
    autoOpen: false, modal: true, height: 300,
    open: function(event, ui) {
      $.post("query.php",{query: "show-permissoes", cod: $("#dialog-acesso").attr("cod")},function(r){
        $("#dialog-acesso").html(r);
      });
    },
    buttons: {
      Salvar: function() {
        var modadm = $("#dialog-acesso input[name='modadm']").attr("checked"); var modaud = $("#dialog-acesso input[name='modaud']").attr("checked");
        var modenf = $("#dialog-acesso input[name='modenf']").attr("checked"); var modfin = $("#dialog-acesso input[name='modfin']").attr("checked");
        var modlog = $("#dialog-acesso input[name='modlog']").attr("checked"); var modmed = $("#dialog-acesso input[name='modmed']").attr("checked");
        var modrem = $("#dialog-acesso input[name='modrem']").attr("checked"); var modfisio = $("#dialog-acesso input[name='modfisio']").attr("checked"); var sysadm = $("#dialog-acesso input[name='sysadm']").attr("checked");
      $.post("query.php",{query: "salvar-permissoes", cod: $("#dialog-acesso").attr("cod"), 
                modadm: modadm, modaud: modaud, modenf: modenf, modfin: modfin, modlog: modlog, modmed: modmed, modrem: modrem, modfisio: modfisio, sysadm: sysadm},
      function(r){alert(r); location.reload;});
      $( this ).dialog( "close" );
      },
      Cancelar: function(){
    $(this).dialog( "close" );
      }
    }
  });


    $("#conselho_regional_id").change(function(){
       
        if($(this).val() == '' || $(this).val() == -1){
            $("input[name='numero_conselho_regional']").removeClass('OBG');
            $("input[name='numero_conselho_regional']").removeAttr('style');

        }else{
            $("input[name='numero_conselho_regional']").addClass('OBG');
        }
    });


    
  $("#inserir-usuario,#salvar-usuario").click(function(){
    var v = true;
    if(!validar_campos("div-novo-usuario"))
      v = false;

    // TESTA A COMPATIBILIDADE DO CAMPO SENHA E DO CAMPO CONFIRMAR SENHA
    if($("#user-passwd").val() !== '' && $("#user-confirm-passwd").val() !== '') {
      if ($("#user-passwd").val() !== $("#user-confirm-passwd").val()) {
        $("#warning-incomp-passwd").html('<b>As senhas informadas n&atilde;o s&atilde;o iguais!</b>');
        v = false;
      }
    }
    return v;
  });
  
  $("#botao-salvar-formulario").click(function(){
  
    if(validar_campos("div-novo-paciente")){
      
      if($("input[name='enderecoInternacao']").val() == ""){
        var x = $("input[name='endereco']").val();
        $("input[name='enderecoInternacao']").val(x);
        
        
        var y = $("input[name='bairro']").val();
        $("input[name='bairroInternacao']").val(y);
        
        var z = $("input[name='telDomiciliar']").val();
        $("input[name='telInternacao']").val(z);
        
        var t = $("input[name='referencia']").val();
        $("input[name='referenciaInternacao']").val(t);
        
        var j = $("input[name='cidade_domiciliar']").val();
        $("input[name='cidade_internado']").val(j);
        
        var j1 = $("input[name='id_cidade_domiciliar']").val();
        $("input[name='id_cidade_internado']").val(j1);
        
        
      }
      
      
      if($("input[name='cuidador']").val()== ""){
        var x = $("input[name='responsavel']").val();
        $("input[name='cuidador']").val(x);
        
      }
      
      if($("select[name='relcuid'] option:selected").val()== "-1"){
        var x = $("select[name='relresp'] option:selected").val();
        $("select[name='relcuid']").val(x);
        
      }
      if($("select[name='csexo'] option:selected").val()== "-1"){
        var x = $("select[name='rsexo'] option:selected").val();
        $("select[name='csexo']").val(x);
        
      }
       
      if($("input[name='especialidadeCuidador']").val()== "" && $("select[name='relcuid'] option:selected").val()=="12"){
        var x = $("input[name='especialidadeResponsavel']").val();
        $("input[name='especialidadeCuidador']").val(x);
        
      }
      
      if($("input[name='telCuidador']").val()== ""){
        var x = $("input[name='telResponsavel']").val();
        $("input[name='telCuidador']").val(x);
        
      }
        if($("input[name='nascimentoCuidador']").val()== ""){
            var x = $("input[name='nascimentoResponsavel']").val();
            $("input[name='nascimentoCuidador']").val(x);

        }
      
      
      
      
      
      
      $.post("query.php",{query: $(this).attr("modo"), nome: $("input[name='nome']").val(), dataInternacao: $("input[name='dataInternacao']").val(), horaInternacao: $("input[name='HoraInternacao']").val(), nascimento: $("input[name='nascimento']").val(), 
            sexo: $("select[name='sexo'] option:selected").val(), codigo: $("input[name='codigo']").val(), 
            convenio: $("select[name='convenio'] option:selected").val(), 
            local: $("input[name='local']").val(), diagnostico: $("input[name='cid-diagnostico']").val(), 
            solicitante: $("input[name='solicitante']").val(), especialidade: $("input[name='sol-especialidade']").val(),
            acomp: $("input[name='sol-acompanhamento']").val(), empresa: $("select[name='empresa'] option:selected").val(), 
            cuid: $("input[name='cuidador']").val(), relcuid: $("select[name='relcuid'] option:selected").val(), csexo: $("select[name='csexo'] option:selected").val(),
            resp: $("input[name='responsavel']").val(), relresp: $("select[name='relresp'] option:selected").val(), rsexo: $("select[name='rsexo'] option:selected").val(),
            endereco: $("input[name='endereco']").val(), bairro: $("input[name='bairro']").val(), referencia: $("input[name='referencia']").val(),
            paciente: $("input[name='paciente']").val(), leito: $("input[name='leito']").val(), telPosto: $("input[name='telPosto']").val(), 
            contato: $("input[name='contato']").val(), telResponsavel: $("input[name='telResponsavel']").val(), telCuidador: $("input[name='telCuidador']").val(), 
            enderecoInternacao: $("input[name='enderecoInternacao']").val(), bairroInternacao: $("input[name='bairroInternacao']").val(), referenciaInternacao: $("input[name='referenciaInternacao']").val(),
              telDomiciliar: $("input[name='telDomiciliar']").val(), telInternacao:$("input[name='telInternacao']").val(),
              especialidadeCuidador:$("input[name='especialidadeCuidador']").val(), 
              especialidadeResponsavel:$("input[name='especialidadeResponsavel']").val(), id_cidade_und:$("input[name='id_cidade_und']").val(),
              id_cidade_internado:$("input[name='id_cidade_internado']").val(), id_cidade_domiciliar:$("input[name='id_cidade_domiciliar']").val(),
              end_und_origem:$("input[name='end_und_origem']").val(),num_matricula_convenio:$("input[name='num_matricula_convenio']").val(),liminar:$("select[name='liminar'] option:selected").val(),
              cpf:$("#cpf-cliente").val(),cpf_responsavel:$("#cpf-responsavel").val(),nascimento_responsavel:$("input[name='nascimentoResponsavel']").val(),
              nascimento_cuidador:$("input[name='nascimentoCuidador']").val()
          },
                            
        function(retorno){ 
                              
          if(retorno == "erro"){
                                alert("Erro ao salvar, tente mais tarde!");
                            }  else{ 
                                alert("Codigo do Paciente:"+retorno);
                               window.location = '?adm=paciente&act=listar';
                               }
        }
      );
    }
    return false;
   });
    
  $("#dialog-add-acompanhamento").dialog({
    autoOpen: false, modal: true, width: 400, height: 230,
    open: function(event, ui) {
      $("#dialog-add-acompanhamento .aviso").css({"background-color":"", "font-weight":""});
      $("#dialog-add-acompanhamento .aviso").text("");
      $(".OBG").each(function (i){this.style.border = "";});
      $.post("query.php",{query: "funcionarios", id: $("#div-detalhes-paciente").attr("cod"), tipo: $(this).attr("tipo")},function(retorno){
  $("#dialog-add-acompanhamento .nomes").html(retorno);
      });
    },
    buttons: {
      Ok: function() {
  if(validar_campos("dialog-add-acompanhamento")){
    $.post("query.php",{query: "adicionar-acompanhamento", paciente: $("#div-detalhes-paciente").attr("cod"), 
     funcionario: $("#dialog-add-acompanhamento .nomes option:selected").val(), inicio: $("input[name='inicio']").val()},
     function(retorno){if(retorno == "erro") alert("ERRO: Não foi possível salvar. Tente mais tarde!")});
    $( this ).dialog( "close" );
  }
      },
      Cancelar: function(){
  $( this ).dialog( "close" );
  window.reload;
      }
    },
    close: function(ev, ui) { window.location.reload() }
  });
  
  $("#dialog-del-acompanhamento").dialog({
    autoOpen: false, modal: true, width: 400, height: 230,
    buttons: {
      Ok: function() {
  $.post("query.php",{query: "remover-acompanhamento", id: $(this).attr("cod")},
     function(retorno){if(retorno == "erro") alert("ERRO: Não foi possível remover. Tente mais tarde!")});
    $( this ).dialog( "close" );
      },
      Cancelar: function(){
  $( this ).dialog( "close" );
      }
    },
    close: function(ev, ui) { window.location.reload() }
  });
  
  $("#dialog-end-acompanhamento").dialog({
    autoOpen: false, modal: true, width: 400, height: 230,
    open: function(event, ui) {
      $("#dialog-end-acompanhamento .aviso").css({"background-color":"", "font-weight":""});
      $("#dialog-end-acompanhamento .aviso").text("");
      $(".OBG").each(function (i){this.style.border = "";});
    },
    buttons: {
      Ok: function() {
  if(validar_campos("dialog-end-acompanhamento")){
    $.post("query.php",{query: "finalizar-acompanhamento", id: $(this).attr("cod"), fim: $("input[name='fim']").val()},
     function(retorno){if(retorno == "erro") alert("ERRO: Não foi possível finalizar. Tente mais tarde!")});
    $( this ).dialog( "close" );
  }
      },
      Cancelar: function(){
  $( this ).dialog( "close" );
      }
    },
    close: function(ev, ui) { window.location.reload() }
  });
  
  $("#dialog-history-acompanhamento").dialog({
    autoOpen: false, modal: true, width: 600, height: 400,
    open: function(event, ui) {
      $("#div-history").html("<img src='../utils/load.gif' /> Aguarde ...");
      $.post("query.php",{query: "history", id: $("#div-detalhes-paciente").attr("cod"), tipo: $(this).attr("tipo")},function(retorno){
  $("#div-history").html(retorno);
      });
    }
  });
  
  $(".add").click(function(){
    $("#dialog-add-acompanhamento").attr("tipo",$(this).attr("tipo"));
    $("#dialog-add-acompanhamento").dialog("open");
    return false;
  });
  
  $(".del").click(function(){
    $("#dialog-del-acompanhamento").attr("cod",$(this).parent().parent().attr("cod"));
    $("#dialog-del-acompanhamento").dialog("open");
    return false;
  });
  
  $(".end").click(function(){
    $("#dialog-end-acompanhamento").attr("cod",$(this).parent().parent().attr("cod"));
    $("#dialog-end-acompanhamento").dialog("open");
    return false;
  });
  
  $(".history").click(function(){
    $("#dialog-history-acompanhamento").attr("tipo",$(this).attr("tipo"));
    $("#dialog-history-acompanhamento").dialog("open");
    return false;
  });
  
 //***************** SEGUROS SAUDE **********************/

    $("#salvar-novo-plano").live('click', function(){
        if(validar_campos("div-plano")){
            var nome = $("input[name='plano']").val();
            var registro_ans = $("input[name='registro_ans']").val();
            var operadora = $("#operadora option:selected").val();
            var iss = $("input[name='iss']").val();
            var tipo_medicamento = $('#tipo_medicamento').val();

            var ano_contrato =  $("input[name='ano_contrato']").val();
            var data_reajuste = '';
            var percentual_reajuste =  '';

            // return false;
            var itens = "";
            var descplano = "";
            var codplano = "";
            var total = $(".item").size();

            $(".item").each(function(i){
                ///cob_plano valor do id do item selecionado na tabela cobranca plano
                var cob_plano = $(this).parent().parent().children('td').eq(2).children('select').children('option:selected').val();
                var cod_item_plano =  $(this).parent().parent().children('td').eq(3).children('input').val();
                var item_plano =  $(this).parent().parent().children('td').eq(1).children('input').val();

                var valor = $(this).val();
                var linha = cob_plano+"#"+valor+"# "+item_plano+" # "+cod_item_plano;
                if(i<total - 1) linha += "$";
                itens += linha;
            });

            // regras fluxo
            var regras = [];
            $('.regra-fluxo-opcao').each(function () {
                var $this = $(this);
                var $tr = $this.parent().parent();
                var opcao = $this.val();
                var regra_primaria = $tr.find('.regra-primaria').val();
                var tipo = $tr.find('.regra-fluxo-tipo').val();
                var item = $tr.find('.regra-fluxo-item').val();
                var carater = $tr.find('.regra-fluxo-carater').val();
                var tipo_validacao = $tr.find('.regra-fluxo-tipo-validacao').val();
                var campo = $tr.find('.regra-fluxo-campo').val();
                var operador = $tr.find('.regra-fluxo-operador').val();
                var valor_referencia = $tr.find('.regra-fluxo-valor-referencia').val();

                regras.push(
                    {
                        regra_primaria: regra_primaria,
                        opcao: opcao,
                        item: item,
                        tipo: tipo,
                        carater: carater,
                        tipo_validacao: tipo_validacao,
                        campo: campo,
                        operador: operador,
                        valor_referencia: valor_referencia
                    }
                );
            });

            $.post("query.php",
                {
                    query: "salvar-novo-plano",
                    plano: nome,
                    registro_ans: registro_ans,
                    operadora: operadora,
                    dados:itens,
                    iss:iss,
                    tipo_medicamento:tipo_medicamento,
                    ano_contrato:ano_contrato,
                    data_reajuste:data_reajuste,
                    percentual_reajuste:percentual_reajuste,
                    regras: regras
                },
                function(r){

                    if(r.indexOf('ERRO:') >= 0) {
                        alert(r);
                    } else {
                        alert('Plano de saúde criado com sucesso!');
                        window.location = "?adm=planos&act=editar&id=" + r;
                    }
                });
        }
        return false;
    });

    $("#editar-plano").click(function(){
        if(validar_campos("div-plano")){
            var id = $("input[name='plano']").attr("cod");
            var nome = $("input[name='plano']").val();
            var registro_ans = $("input[name='registro_ans']").val();
            var operadora = $("#operadora option:selected").val();
            var iss = $("input[name='iss']").val();
            var itens = "";
            var descplano = "";
            var codplano = "";
            var total = $(".item").size();
            var tipo_medicamento = $('#tipo_medicamento').val();
            var ano_contrato =  $("input[name='ano_contrato']").val();
            var data_reajuste =  $("input[name='data_reajuste']").val();
            var percentual_reajuste =  $("input[name='percentual_reajuste']").val();
            var data_reajuste_novo =  $("input[name='data_reajuste_novo']").val();
            var percentual_reajuste_novo =  $("input[name='percentual_reajuste_novo']").val();

            if(percentual_reajuste_novo != ''){
                if(data_reajuste_novo == ''){
                    $("input[name='data_reajuste_novo']").focus();
                    alert('Favor informar a data do reajuste.');
                    return false;
                }
                data_reajuste = data_reajuste_novo;
                percentual_reajuste = percentual_reajuste_novo;

            }

            $(".item").each(function(i){
                var cob_plano = $(this).parent().parent().children('td').eq(2).children('select').children('option:selected').val();
                var cod_item_plano =  $(this).parent().parent().children('td').eq(3).children('input').val();
                var item_plano =  $(this).parent().parent().children('td').eq(1).children('input').val();
                var editado = $(this).parent().parent().attr('editado');
                var id_valorescobranca =  $(this).attr('id-item');

                var valor = $(this).val();
                var linha = cob_plano+"#"+valor+"# "+item_plano+" # "+cod_item_plano +" # "+ editado +" # "+id_valorescobranca;
                if(i<total - 1) linha += "$";
                itens += linha;

            });

            // regras fluxo
            var regras = [];
            $('.regra-fluxo-opcao').each(function () {
                var $this = $(this);
                var $tr = $this.parent().parent();
                var opcao = $this.val();
                var regra_primaria = $tr.find('.regra-primaria-fluxo').val();
                var tipo = $tr.find('.regra-fluxo-tipo').val();
                var item = $tr.find('.regra-fluxo-item').val();
                var carater = $tr.find('.regra-fluxo-carater').val();
                var tipo_validacao = $tr.find('.regra-fluxo-tipo-validacao').val();
                var campo = $tr.find('.regra-fluxo-campo').val();
                var operador = $tr.find('.regra-fluxo-operador').val();
                var valor_referencia = $tr.find('.regra-fluxo-valor-referencia').val();

                regras.push(
                    {
                        regra_primaria: regra_primaria,
                        opcao: opcao,
                        item: item,
                        tipo: tipo,
                        carater: carater,
                        tipo_validacao: tipo_validacao,
                        campo: campo,
                        operador: operador,
                        valor_referencia: valor_referencia
                    }
                );
            });

            $.post("query.php",
                {
                    query: "salvar-edicao-plano",
                    id: id,
                    plano: nome,
                    registro_ans: registro_ans,
                    operadora: operadora,
                    dados: itens,
                    iss: iss,
                    tipo_medicamento: tipo_medicamento,
                    ano_contrato:  ano_contrato,
                    data_reajuste:data_reajuste,
                    percentual_reajuste: percentual_reajuste,
                    regras: regras
                },
                function(r){
                    if(r == 1){
                        alert('Plano de saude atualizado com sucesso!');
                        //window.location = "?adm=planos";
                    } else {
                        alert(r);
                    }
                });
        }
        return false;
    });
  
//*************** FICHAS DE CAPTACAO ******************//
  
  $(".enviar-ficha").click(function(){
    $("#dialog-upload-ficha input[name='tipo']").val($(this).attr("tipo"));
    $("#dialog-upload-ficha").dialog("open");
  });
  
  $("#dialog-upload-ficha").dialog({
    autoOpen: false, modal: true, width: 600, position: 'top',
    buttons: {
      "Fechar" : function(){
        var item = $("#formulario").attr("item");
        $("#kits-"+item).hide();
        $(this).dialog("close");
      }
    }
  });
  
//**************** FILIAIS *******************//

  $("#nova-filial").click(function(){
    $("#dialog-filial").attr("op","novo");
    $("#dialog-filial").attr("cod","-1");
    $("#dialog-filial input[name='nome-filial']").val("");
    $("#dialog-filial input[name='percentual-filial']").val("");
    $("#dialog-filial input[name='iss-filial']").val("");
    $("#dialog-filial").dialog("open");
  });
  
  $(".editar-filial").click(function(){
    $("#dialog-filial").attr("op","editar");
    $("#dialog-filial").attr("cod",$(this).attr("cod"));
    $("#dialog-filial input[name='nome-filial']").val($(this).attr("nome"));
    $("#dialog-filial input[name='percentual-filial']").val($(this).attr("percentual"));
     $("#dialog-filial input[name='iss-filial']").val($(this).attr("iss"));
    $("#dialog-filial").dialog("open");
  });

  $("#dialog-filial").dialog({
    autoOpen: false, modal: true, width: 800, position: 'top',
      buttons: {
          "Salvar" : function(){
              if(validar_campos("dialog-filial")){
                  var nome = $("#dialog-filial input[name='nome-filial']").val();
                  var percentual = $("#dialog-filial input[name='percentual-filial']").val();
                  var iss = $("#dialog-filial input[name='iss-filial']").val();
                  iss = iss.replace(',','.');
                  var cod = $(this).attr("cod");
                  var op = $(this).attr("op");

                  percentual = percentual.replace(',','.');
                  var reg = new RegExp(/^\d*[.]?\d*$/);

                  if(reg.test(percentual) === true){
                      $.post("query.php",{query: "salvar-filial", nome: nome, percentual: percentual, cod: cod, op: op, iss:iss},function(r) {
                          if (r == 1) {
                          alert("Filial criada.");
                          window.location = '?adm=filiais';
                      }else {alert('11111223'+r)};
                      });
                  }else{
                      alert("Por favor, digite um numero no percentual");
                      $("#dialog-filial input[name='percentual-filial']").focus().css('border', '3px red solid');
                  }

              }
              return false;
          },
          "Fechar" : function(){
              $(this).dialog("close");
          }
      }
  });

  $(".send-logo-filial").click(function(){
    var filial = $(this).attr('filial');
    var input_file = $('input#filial-' + filial);
    if(input_file.val() != '') {
      $('#filial-' + filial).html('<img src="/utils/ajax-loader.gif" alt="Enviando..."/>');
      $('#logo-filial-form-' + filial).ajaxForm({
        target:'#filial-' + filial
      }).submit();
      input_file.replaceWith( input_file = input_file.clone( true ) );
    }
    return false;
  });
  

  $(".excluir-filial").click(function(){
    if(confirm("Deseja realmente remover a filial?")){
      $.post("query.php",{query: "excluir-filial", cod: $(this).attr("cod")},function(r){
        if(r==1)
          window.location = '?adm=filiais';
        else alert(r);
      });
    }
    return false;
  });
  
  $(".excluir-plano").click(function(){
    var cod = $(this).attr("cod");
    if(confirm("Deseja realmente remover este plano?")){
    $.post("query.php",{query: "excluir-plano", cod: cod},function(r){
      if(r==1){
        alert('Excluído com sucesso!');
        window.location.reload();
      }else{ alert(r);};
    });
    }
  });
  
  $('#combo_empresa').change(function(){
    $.post("usuario.php",{query: "empresa", empresa_id: $(this).val()},function(r){
      $('#outras_cidades').html(r);
    });
  });
  
  $(".liberar").click(function(){
    var id= $(this).attr('id_prescricao');
    
     $.post("query.php",{query: "liberar", id_prescricao:id},function(r){
        if (r == 1){
          alert("Prescri\u00e7\u00e3o liberada com sucesso para enfermagem");
          window.location.reload();
        }else{
          alert(r);
        }
      });
  });
     
     $(".desativar").click(function(){
        var id= $(this).attr('id_prescricao');
        if(confirm("Deseja realmente desativar a prescri\u00e7\u00e3o?")){
         $.post("query.php",{query:"desativar", id_prescricao:id},function(r){
            if (r == 1){
              alert("Prescri\u00e7\u00e3o desativada com sucesso.");
              window.location.reload();
            }else{
              alert(r);
              
            }
          });
        }
    });
                
//////////////////validações do for_adimissão paciente 2013-11-05
////////só permitir colocar data início e fim se atendimento domiciliar estiver marcado e os campos passam a ser obrigatórios.
$('#atendimento_domiciliar').click(function(){
   
    if($(this).is(':checked')){
        $('#inicio_atendimento').removeAttr('disabled');
        $('#inicio_atendimento').addClass('OBG');
        $('#fim_atendimento').removeAttr('disabled');
        $('#fim_atendimento').addClass('OBG');
      }else{
          $('#inicio_atendimento').attr('disabled','disabled');
          $('#inicio_atendimento').removeClass('OBG');
          $('#fim_atendimento').attr('disabled','disabled');
          $('#fim_atendimento').removeClass('OBG');
          $('#fim_atendimento').removeAttr('style');
          $('#inicio_atendimento').removeAttr('style');
          $('#inicio_atendimento').val('');
          $('#fim_atendimento').val(''); 
      }
    
})
////////Só permitir colocar data início e fim se internação domiciliar estiver marcado e os campos passam a ser obrigatórios.
$('#internacao_domiciliar').click(function(){
   
    if($(this).is(':checked')){
        $('#inicio_internacao').removeAttr('disabled');
        $('#inicio_internacao').addClass('OBG');
        $('#fim_internacao').removeAttr('disabled');
        $('#fim_internacao').addClass('OBG');
      }else{
          $('#inicio_internacao').attr('disabled','disabled');
          $('#inicio_internacao').removeClass('OBG');
          $('#fim_internacao').attr('disabled','disabled');
          $('#fim_internacao').removeClass('OBG');
          $('#fim_internacao').removeAttr('style');
          $('#inicio_internacao').removeAttr('style');
          $('#inicio_internacao').val('');
          $('#fim_internacao').val(''); 
      }
    
})
//////////////////Se técnico de enfermagem for marcado aparece as opções de visita caso contraio some.
$('#tec_enfermagem').click(function(){
   
    if($(this).is(':checked')){
       $('#span_tec_enfermagem').removeAttr('style');
    }else{
        $('#span_tec_enfermagem').attr('style','display:none;');
        ///////v72h  
          $('#inicio_tec_v72h').attr('disabled','disabled');
          $('#inicio_tec_v72h').removeClass('OBG');
          $('#fim_tec_v72h').attr('disabled','disabled');
          $('#fim_tec_v72h').removeClass('OBG');
          $('#inicio_tec_v72h').val('');
          $('#fim_tec_v72h').val(''); 
          $('#tec_v72h').attr('checked',false);
          $('#fim_tec_v72h').removeAttr('style');
          $('#inicio_tec_v72h').removeAttr('style');
          //////v48h
           $('#inicio_tec_v48h').attr('disabled','disabled');
          $('#inicio_tec_v48h').removeClass('OBG');
          $('#fim_tec_v48h').attr('disabled','disabled');
          $('#fim_tec_v48h').removeClass('OBG');
          $('#inicio_tec_v48h').val('');
          $('#fim_tec_v48h').val(''); 
          $('#tec_v48h').attr('checked',false);
          $('#fim_tec_v48h').removeAttr('style');
          $('#inicio_tec_v48h').removeAttr('style');
          /////tec24h
          $('#inicio_tec_24').attr('disabled','disabled');
          $('#inicio_tec_24').removeClass('OBG');
          $('#fim_tec_24').attr('disabled','disabled');
          $('#fim_tec_24').removeClass('OBG');
          $('#inicio_tec_24').val('');
          $('#fim_tec_24').val(''); 
          $('#tec_24').attr('checked',false);
          $('#fim_tec_24').removeAttr('style');
          $('#inicio_tec_24').removeAttr('style');
          ///tec06h
          $('#inicio_tec_06').attr('disabled','disabled');
          $('#inicio_tec_06').removeClass('OBG');
          $('#fim_tec_06').attr('disabled','disabled');
          $('#fim_tec_06').removeClass('OBG');
          $('#fim_tec_06').val('');
          $('#inicio_tec_06').val('');
          $('#tec_06').attr('checked',false);
          $('#fim_tec_06').removeAttr('style');
          $('#inicio_tec_06').removeAttr('style');
          ////tec12h
          $('#inicio_tec_12').attr('disabled','disabled');
          $('#inicio_tec_12').removeClass('OBG');
          $('#fim_tec_12').attr('disabled','disabled');
          $('#fim_tec_12').removeClass('OBG');
          $('#fim_tec_12').val('');
          $('#inicio_tec_12').val('');
          $('#tec_12').attr('checked',false);
          $('#fim_tec_12').removeAttr('style');
          $('#inicio_tec_12').removeAttr('style');
          
         /// tec_01xdia
          $('#inicio_tec_01xdia').attr('disabled','disabled');
          $('#inicio_tec_01xdia').removeClass('OBG');
          $('#fim_tec_01xdia').attr('disabled','disabled');
          $('#fim_tec_01xdia').removeClass('OBG');
          $('#fim_tec_01xdia').val('');
          $('#inicio_tec_01xdia').val('');
          $('#tec_01xdia').attr('checked',false);
          $('#fim_tec_01xdia').removeAttr('style');
          $('#inicio_tec_01xdia').removeAttr('style');

        /// tec_02xdia
        $('#inicio_tec_02xdia').attr('disabled','disabled');
        $('#inicio_tec_02xdia').removeClass('OBG');
        $('#fim_tec_02xdia').attr('disabled','disabled');
        $('#fim_tec_02xdia').removeClass('OBG');
        $('#fim_tec_02xdia').val('');
        $('#inicio_tec_02xdia').val('');
        $('#tec_02xdia').attr('checked',false);
        $('#fim_tec_02xdia').removeAttr('style');
        $('#inicio_tec_02xdia').removeAttr('style');

        /// tec_03xdia
        $('#inicio_tec_03xdia').attr('disabled','disabled');
        $('#inicio_tec_03xdia').removeClass('OBG');
        $('#fim_tec_03xdia').attr('disabled','disabled');
        $('#fim_tec_03xdia').removeClass('OBG');
        $('#fim_tec_03xdia').val('');
        $('#inicio_tec_03xdia').val('');
        $('#tec_03xdia').attr('checked',false);
        $('#fim_tec_03xdia').removeAttr('style');
        $('#inicio_tec_03xdia').removeAttr('style');
          
          
          
          
    }
})

/////Só permitir colocar data início e fim se internação técnico 06h estiver marcado e os campos passam a ser obrigatórios.
$('#tec_06').click(function(){
   
    if($(this).is(':checked')){
        $('#inicio_tec_06').removeAttr('disabled');
        $('#inicio_tec_06').addClass('OBG');
        $('#fim_tec_06').removeAttr('disabled');
        $('#fim_tec_06').addClass('OBG');
      }else{
          $('#inicio_tec_06').attr('disabled','disabled');
          $('#inicio_tec_06').removeClass('OBG');
          $('#fim_tec_06').attr('disabled','disabled');
          $('#fim_tec_06').removeClass('OBG');
          $('#fim_tec_06').removeAttr('style');
          $('#inicio_tec_06').removeAttr('style');
          $('#fim_tec_06').val('');
          $('#inicio_tec_06').val('');
      }
    
})
/////Só permitir colocar data início e fim se internação técnico 12h estiver marcado e os campos passam a ser obrigatórios.
$('#tec_12').click(function(){
   
    if($(this).is(':checked')){
        $('#inicio_tec_12').removeAttr('disabled');
        $('#inicio_tec_12').addClass('OBG');
        $('#fim_tec_12').removeAttr('disabled');
        $('#fim_tec_12').addClass('OBG');
      }else{
          $('#inicio_tec_12').attr('disabled','disabled');
          $('#inicio_tec_12').removeClass('OBG');
          $('#fim_tec_12').attr('disabled','disabled');
          $('#fim_tec_12').removeClass('OBG');
          $('#fim_tec_12').removeAttr('style');
          $('#inicio_tec_12').removeAttr('style');
          $('#fim_tec_12').val('');
          $('#inicio_tec_12').val('');
      }
    
})

/////Só permitir colocar data início e fim se internação técnico 24h estiver marcado e os campos passam a ser obrigatórios.
$('#tec_24').click(function(){
   
    if($(this).is(':checked')){
        $('#inicio_tec_24').removeAttr('disabled');
        $('#inicio_tec_24').addClass('OBG');
        $('#fim_tec_24').removeAttr('disabled');
        $('#fim_tec_24').addClass('OBG');
      }else{
          $('#inicio_tec_24').attr('disabled','disabled');
          $('#inicio_tec_24').removeClass('OBG');
          $('#fim_tec_24').attr('disabled','disabled');
          $('#fim_tec_24').removeClass('OBG');
           $('#fim_tec_24').removeAttr('style');
          $('#inicio_tec_24').removeAttr('style');
           $('#inicio_tec_24').val('');
          $('#fim_tec_24').val('');
          
      }
    
});

/////Só permitir colocar data início e fim se internação técnico 1xdiah estiver marcado e os campos passam a ser obrigatórios.
$('#tec_01xdia').click(function(){
   
    if($(this).is(':checked')){
        $('#inicio_tec_01xdia').removeAttr('disabled');
        $('#inicio_tec_01xdia').addClass('OBG');
        $('#fim_tec_01xdia').removeAttr('disabled');
        $('#fim_tec_01xdia').addClass('OBG');
      }else{
          $('#inicio_tec_01xdia').attr('disabled','disabled');
          $('#inicio_tec_01xdia').removeClass('OBG');
          $('#fim_tec_01xdia').attr('disabled','disabled');
          $('#fim_tec_01xdia').removeClass('OBG');
          $('#fim_tec_01xdia').removeAttr('style');
          $('#inicio_tec_01xdia').removeAttr('style');
          $('#fim_tec_01xdia').val('');
          $('#inicio_tec_01xdia').val('');
      }
    
});

/////Só permitir colocar data início e fim se internação técnico 2xdiah estiver marcado e os campos passam a ser obrigatórios.
    $('#tec_02xdia').click(function(){

        if($(this).is(':checked')){
            $('#inicio_tec_02xdia').removeAttr('disabled');
            $('#inicio_tec_02xdia').addClass('OBG');
            $('#fim_tec_02xdia').removeAttr('disabled');
            $('#fim_tec_02xdia').addClass('OBG');
        }else{
            $('#inicio_tec_02xdia').attr('disabled','disabled');
            $('#inicio_tec_02xdia').removeClass('OBG');
            $('#fim_tec_02xdia').attr('disabled','disabled');
            $('#fim_tec_02xdia').removeClass('OBG');
            $('#fim_tec_02xdia').removeAttr('style');
            $('#inicio_tec_02xdia').removeAttr('style');
            $('#fim_tec_02xdia').val('');
            $('#inicio_tec_02xdia').val('');
        }

    });

    /////Só permitir colocar data início e fim se internação técnico 3xdiah estiver marcado e os campos passam a ser obrigatórios.
    $('#tec_03xdia').click(function(){

        if($(this).is(':checked')){
            $('#inicio_tec_03xdia').removeAttr('disabled');
            $('#inicio_tec_03xdia').addClass('OBG');
            $('#fim_tec_03xdia').removeAttr('disabled');
            $('#fim_tec_03xdia').addClass('OBG');
        }else{
            $('#inicio_tec_03xdia').attr('disabled','disabled');
            $('#inicio_tec_03xdia').removeClass('OBG');
            $('#fim_tec_03xdia').attr('disabled','disabled');
            $('#fim_tec_03xdia').removeClass('OBG');
            $('#fim_tec_03xdia').removeAttr('style');
            $('#inicio_tec_03xdia').removeAttr('style');
            $('#fim_tec_03xdia').val('');
            $('#inicio_tec_03xdia').val('');
        }

    });
  

/////Só permitir colocar data início e fim se internação técnico v48h estiver marcado e os campos passam a ser obrigatórios.
$('#tec_v48h').click(function(){
   
    if($(this).is(':checked')){
        $('#inicio_tec_v48h').removeAttr('disabled');
        $('#inicio_tec_v48h').addClass('OBG');
        $('#fim_tec_v48h').removeAttr('disabled');
        $('#fim_tec_v48h').addClass('OBG');
      }else{
          $('#inicio_tec_v48h').attr('disabled','disabled');
          $('#inicio_tec_v48h').removeClass('OBG');
          $('#fim_tec_v48h').attr('disabled','disabled');
          $('#fim_tec_v48h').removeClass('OBG');
          $('#fim_tec_v48h').removeAttr('style');
          $('#inicio_tec_v48h').removeAttr('style');
           $('#fim_tec_v48h').val('');
          $('#inicio_tec_v48h').val('');
      }
    
})

/////Só permitir colocar data início e fim se internação técnico v72h estiver marcado e os campos passam a ser obrigatórios.
$('#tec_v72h').click(function(){
   
    if($(this).is(':checked')){
        $('#inicio_tec_v72h').removeAttr('disabled');
        $('#inicio_tec_v72h').addClass('OBG');
        $('#fim_tec_v72h').removeAttr('disabled');
        $('#fim_tec_v72h').addClass('OBG');
      }else{
          $('#inicio_tec_v72h').attr('disabled','disabled');
          $('#inicio_tec_v72h').removeClass('OBG');
          $('#fim_tec_v72h').attr('disabled','disabled');
          $('#fim_tec_v72h').removeClass('OBG');
          $('#fim_tec_v72h').removeAttr('style');
          $('#inicio_tec_v72h').removeAttr('style');
          $('#fim_tec_v72h').val('');
          $('#inicio_tec_v72h').val('');
      }
    
})
////////Visita medica
$('#v_medica').click(function(){
   if($(this).is(':checked')){
        $('#fmedica').removeAttr('disabled');
        $('#fmedica').addClass('COMBO_OBG');
    }else{
        $('#fmedica').attr('disabled','disabled');
        $('#fmedica').removeClass('COMBO_OBG');
        $('#fmedica').removeAttr('style');
       $('#fmedica').val('');
    }
});

////////Visita nutricionista
    $('#v_nutricionista').click(function(){
        if($(this).is(':checked')){
            $('#f_visita_nutricionista').removeAttr('disabled');
            $('#f_visita_nutricionista').addClass('COMBO_OBG');
        }else{
            $('#f_visita_nutricionista').attr('disabled','disabled');
            $('#f_visita_nutricionista').removeClass('COMBO_OBG');
            $('#f_visita_nutricionista').removeAttr('style');
            $('#f_visita_nutricionista').val('');
        }
    });

//////////visita enfermagem

$('#v_enfermagem').click(function(){
   if($(this).is(':checked')){
        $('#fenfermagem').removeAttr('disabled');
        $('#fenfermagem').addClass('COMBO_OBG');
    }else{
        $('#fenfermagem').attr('disabled','disabled');
        $('#fenfermagem').removeClass('COMBO_OBG');
        $('#fenfermagem').removeAttr('style');
       $('#fenfermagem').val('');
    }
});
$('#fisioterapia').click(function(){
   if($(this).is(':checked')){
       $('#span_fisioterapia').removeAttr('style');
   }else{
       $('#span_fisioterapia').attr('style','display:none;');
       $('#respiratoria_sessao').attr('disabled','disabled');
       $('#respiratoria_sessao').removeClass('OBG');
        $('#respiratoria').attr('checked',false);
        $('#respiratoria_sessao').val('');
        $('#motora').attr('checked',false);
        $('#motora_sessao').attr('disabled','disabled');
        $('#motora_sessao').removeClass('OBG');
        $('#motora_sessao').val('');
        $('#motora_sessao').removeAttr('style');
        $('#respiratoria_sessao').removeAttr('style');
   }
   
});

$('#respiratoria').click(function(){
   if($(this).is(':checked')){
        $('#respiratoria_sessao').removeAttr('disabled');
        $('#respiratoria_sessao').addClass('OBG');
    }else{
        $('#respiratoria_sessao').attr('disabled','disabled');
        $('#respiratoria_sessao').removeClass('OBG');
        $('#respiratoria_sessao').val('');
        $('#respiratoria_sessao').removeAttr('style');
    }
 });
 
 $('#motora').click(function(){
   if($(this).is(':checked')){
        $('#motora_sessao').removeAttr('disabled');
        $('#motora_sessao').addClass('OBG');
    }else{
        $('#motora_sessao').attr('disabled','disabled');
        $('#motora_sessao').removeClass('OBG');
        $('#motora_sessao').val('');
        $('#motora_sessao').removeAttr('style');
    }
 });
 
 //////////////fonoterapia
 
 $('#fonoterapia').click(function(){
   if($(this).is(':checked')){
       $('#span_fonoterapia').removeAttr('style');
        $('#fonoterapia_sessao').removeAttr('disabled');
       
   }else{
       $('#span_fonoterapia').attr('style','display:none;');
       $('#fonoterapia_av').attr('checked',false);
       $('#fonoterapia_sessao').attr('disabled','disabled');
       $('#fonoterapia_sessao').val('');
       
   }
   
});    

////nutricionista
$('#nutricionista').click(function(){
   if($(this).is(':checked')){
       
        $('#fnutricionista').removeAttr('disabled');
        $('#qtd_diaria_dieta').removeAttr('disabled');
        
       
   }else{
       $('#fnutricionista').attr('disabled','disabled');
       $('#fnutricionista').val('');
       $('#qtd_diaria_dieta').attr('disabled','disabled');
       $('#qtd_diaria_dieta').val('');
       
   }
   
}); 

////npsicologo
$('#psicologo').click(function(){
   if($(this).is(':checked')){
       
        $('#fpsicologo').removeAttr('disabled');
        $('#psicologo_sessao').removeAttr('disabled');
       
   }else{
       $('#fpsicologo').attr('disabled','disabled');
       $('#fpsicologo').val('');
       $('#psicologo_sessao').attr('disabled','disabled');
       $('#psicologo_sessao').val('');
       
   }
   
});

///////////////
$('#medico_especialista').click(function(){
   if($(this).is(':checked')){
       $('#med_especialidade').removeAttr('disabled');
       $('#med_especialidade').addClass('OBG');
   }else{
       $('#med_especialidade').attr('disabled','disabled');
       $('#med_especialidade').removeClass('OBG');
       $('#med_especialidade').removeAttr('style');
   }
   });
   
   ////////////////////salvar ficha de admissão do paciente
    $("#salvar-ficha-admissao").click(function(){
           var paciente_id= $(this).attr('paciente_id');
           var hist_status_id =$(this).attr('hist_status_id');
    if(validar_campos("div-form-admissao")){ 
              var dados = $("form").serialize();
             var itens=''
              $(".equipamento").each(function(i){
        if($(this).is(':checked')){
                            var x = $(this).attr('cod_equipamento');
                            x += "$";
                            itens += x;
                            
                        }
                    
      });
              
              var enviar="query=salvar-ficha-admissao&"+dados+"&equipamentos="+itens+"&paciente_id="+paciente_id+"&hist_status_id="+hist_status_id;
               $.ajax({
          type: "POST",
          url: "query.php",
          data: enviar,
          success: function(msg){
            
            
            if(msg == 1){
                                            alert("A ficha foi salva com sucesso.");
                                       window.location.href='?adm=paciente&act=listar';
                                            
               
            }else{
                      alert(msg);
            }
            
                    
          }
        
        });
     }
          
          return false;
      
    });
    
    
    $('.desativar-usuario').click(function(){ 
        
        var nomeUsuario = $(this).attr('nome-usuario');
        var idUsuario   = $(this).attr('id-usuario');     
       $('#span-div-desativar-usuario').html(nomeUsuario);
       $('#div-destivar-usuario').attr('idUsuario',idUsuario);
       $('#div-destivar-usuario').dialog('open');
    });
    
    $('.programado-desativar-usuario').click(function(){
        var nomeUsuario= $(this).attr('nome-usuario');
        var idUsuario= $(this).attr('id-usuario');  
       $('#span-div-desativar-ativar-usuario').html(nomeUsuario);
       $('#div-destivar-ativar-usuario').attr('idUsuario',idUsuario);
       $('#div-destivar-ativar-usuario').dialog('open');
    });
    
    $('#div-destivar-usuario').hide();
    $('#div-destivar-usuario').dialog('close');
    $("#div-destivar-usuario").dialog({
        autoOpen: false, modal: true, height: 250, width: 300,
       
        buttons: {            
            Salvar: function() {  
                if(!validar_campos('div-destivar-usuario'))
                    return false;
                 var idUsuario = $('#div-destivar-usuario').attr('idUsuario');
                 var data=$('#data-destivar-usuario').val();
                 var observacao=$('#obs-destivar-usuario').val();
                 $.post("query.php",{query: "desativar-usuario", 
                                     idUsuario:idUsuario,
                                     observacao:observacao,
                                     data:data},function(r){
                        
                        if(r == 1){
                          alert('Desativado com sucesso!');
                          window.location.reload();
                        }else{
                            alert(r);                           
                            
                        }
                      });
            },
            Cancelar: function() {
                $(this).dialog("close");
                
            }
        }
    });
    $('.ativar-usuario').click(function(){
         var idUsuario = $(this).attr('id-usuario');                 
            $.post("query.php",{query: "ativar-usuario", 
                                idUsuario:idUsuario},function(r){

                   if(r == 1){
                     alert('Ativado com sucesso!');
                     window.location.reload();
                   }else{
                       alert(r);                           

                   }
                 });
    });
    $('#div-destivar-ativar-usuario').hide();
    $('#div-destivar-ativar-usuario').dialog('close');
    $("#div-destivar-ativar-usuario").dialog({
        autoOpen: false, modal: true, height: 400, width: 300,
       
        buttons: {
            Ativar: function(){
                var idUsuario = $('#div-destivar-ativar-usuario').attr('idUsuario');                
            $.post("query.php",{query: "ativar-usuario", 
                                idUsuario:idUsuario},function(r){

                   if(r == 1){
                     alert('Ativado com sucesso!');
                     window.location.reload();
                   }else{
                       alert(r);                           

                   }
                 });
            },
            Reprogramar : function() {  
                if(!validar_campos('div-destivar-ativar-usuario'))
                    return false;
                 var idUsuario = $('#div-destivar-ativar-usuario').attr('idUsuario');
                 var data=$('#div-destivar-ativar-usuario').val();
                 var observacao=$('#obs-destivar-usuario').val();
                 $.post("query.php",{query: "desativar-usuario", 
                                     idUsuario:idUsuario,
                                     observacao:observacao,
                                     data:data},function(r){
                        
                        if(r == 1){
                          alert('Desativado com sucesso!');
                          window.location.reload();
                        }else{
                            alert(r);                           
                            
                        }
                      });
            },
            Cancelar: function() {
                $(this).dialog("close");
                
            }
        }
    });

    //Cpf
    function verificarCPF(strCPF) {
        var soma = 0;
        var resto;
        if (strCPF == "00000000000")
            return false;
        for (i=1; i<=9; i++)
            soma = soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(strCPF.substring(9, 10)) )
            return false;
        soma = 0;
        for (i = 1; i <= 10; i++)
            soma = soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(strCPF.substring(10, 11) ) )
            return false;
        return true;
    }

    $('.cpf').mask('999.999.999-99');
    $('.cpf').blur(function(){
        var cpf = $(this).val();
        var idSpan= $(this).attr('id');
  //console.log(nome);
        exp = /\.|\-/g
        cpf = cpf.toString().replace( exp, "" );

        var isValido = verificarCPF(cpf);
        $('#span-'+idSpan).text( isValido ? '' : 'CPF Invalido!' );
        if (! isValido)
            $(this).val('');
    });
    // colocar cpf como obg caso plano = particular
    $("#convenio-ficha-paciente").change(function(){
        if($(this).val() ==1 ){
            $("#cpf-cliente").addClass('OBG');
            $("#cpf-responsavel").addClass('OBG');
            $("input[name='num_matricula_convenio']").removeClass('OBG');
            $("input[name='num_matricula_convenio']").val('');
            $("input[name='num_matricula_convenio']").attr('disabled',true);
        }else{
            $("input[name='num_matricula_convenio']").attr('disabled',false);
            $("input[name='num_matricula_convenio']").addClass('OBG');
            $("#cpf-cliente").removeClass('OBG');
            $("#cpf-responsavel").removeClass('OBG');
            $("#cpf-cliente").removeAttr('style');
            $("#cpf-responsavel").removeAttr('style');
        }
    });

    $("#retornar-valor-anterior").click(function(){
        $(".item").each(function(){
            $(this).val($(this).attr('valor-anterior'));
        });
    });

    $("#calcular-novo-valor").click(function(){
        $(".item").each(function(){
            var valor = parseFloat($(this).val().replace('.','').replace(',','.'));
            var percentual = parseFloat($('#percentual_reajuste_novo').val().replace('.','').replace(',','.'));
            var novo_valor = valor;

            if(percentual != 0 && percentual != undefined && isNaN(percentual) != true ){
                novo_valor = valor * (1 + percentual/100);

            }
            $(this).val(float2moeda(novo_valor));

        });
    });
    function float2moeda(num) {

        x = 0;

        if(num<0) {
            num = Math.abs(num);
            x = 1;
        }
        if(isNaN(num)) num = "0";
        cents = Math.floor((num*100+0.5)%100);

        num = Math.floor((num*100+0.5)/100).toString();

        if(cents < 10) cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
            num = num.substring(0,num.length-(4*i+3))+'.'
                +num.substring(num.length-(4*i+3));
        ret = num + ',' + cents;
        if (x == 1) ret = ' - ' + ret;return ret;

    }

    $('.cancelar-admissao').click(function(){
      var cancelarAdmissao = $(this);
      var idAdmissao = cancelarAdmissao.attr('id-admissao');
      $.post("query.php",{
        query: "cancelar-admissao", 
        idAdmissao:idAdmissao
      },function(r){
            if(r == 1){
            alert('Ficha de Admissão cancelada com sucesso!');
            window.location.reload();
            }else{
              alert(r);                          

            }
        });
    });

});
