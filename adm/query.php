<?php
include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/codigos.php');
include_once("../utils/class.phpmailer.php");

use App\Core\Config;
use App\Services\Gateways\SendGridGateway;
use App\Services\MailAdapter;
use App\Controllers\SendGrid;

if(!function_exists('redireciona')){
	function redireciona($link){
		if ($link==-1){
			echo" <script>history.go(-1);</script>";
		}else{
			echo" <script>document.location.href='$link'</script>";
		}
	}
}

function salvar_paciente($post){
    //ini_set('display_errors', 1);
    extract($post,EXTR_OVERWRITE);
    $nascimento = implode("-",array_reverse(explode("/",$nascimento)));
    $dataInternacao = implode("-",array_reverse(explode("/",$dataInternacao)));

    $dataInternacao = $dataInternacao.' '.$horaInternacao;

    $nome = anti_injection($nome,'literal');
    $diagnostico = '';
    $local = anti_injection($local,'literal');
    $solicitante = '';
    $sexo = anti_injection($sexo,'literal');
    $csexo = '';
    $rsexo = '';
    $resp = '';
    $relresp = '';
    $cuid = '';
    $relcuid = '';
    $endereco = anti_injection($endereco,'literal');
    $especialidade = '';
    $acomp = '';
    $bairro = '';
    $referencia = '';
    $leito = anti_injection($leito,'literal');
    $telPosto = anti_injection($telPosto,'literal');
    $contato = anti_injection($contato,'literal');
    $telResponsavel = '';
    $telCuidador = '';
    $enderecoInternacao= anti_injection($enderecoInternacao,'literal');
    $bairroInternacao=  '';
    $telInternacao=  '';
    $telDomiciliar=  '';
    $id_cidade_und = anti_injection( $id_cidade_und,'literal');
    $id_cidade_domiciliar =  '';
    $id_cidade_internado = anti_injection( $id_cidade_internado,'literal');

    $referenciaInternacao=  '';
    $especialidadeResponsavel=  '';
    $especialidadeCuidador=  '';
    $end_und_origem = anti_injection( $end_und_origem,'literal');
    $num_matricula_convenio = anti_injection( $num_matricula_convenio,'literal');
    $liminar =anti_injection( $liminar,'numerico');
    $cpf = anti_injection( $cpf,'literal');
    $cpf = str_replace('-','',str_replace('.','',$cpf));
    $cpfResponsavel =  '';
    $nascimentoResp = '';
    $nascimentoCuid = '';
    $nascimentoCuid = implode("-",array_reverse(explode("/", $nascimentoCuid)));
    $nascimentoResp = implode("-",array_reverse(explode("/", $nascimentoResp)));
    ///////////////jeferson 10/10/2013 verificar os campos obrigatorios que foram cadastrados com . ou - para ver quantos porcentos do cadastro esta faltando informação.
    $cont_porcento =0;
    if($nome == ''){
        $cont_porcento ++;
    }
    if($num_matricula_convenio == ''){
        $cont_porcento ++;
    }
    if($solicitante == ''){
        $cont_porcento ++;
    }
    if($especialidade == ''){
        $cont_porcento ++;
    }
    if($local == ''){
        $cont_porcento ++;
    }
    if($end_und_origem == ''){
        $cont_porcento ++;
    }
    if($id_cidade_und == ''){
        $cont_porcento ++;
    }
    if($contato == ''){
        $cont_porcento ++;
    }
    if($resp == ''){
        $cont_porcento ++;
    }
    if($telResponsavel == ''){
        $cont_porcento ++;
    }
    if($endereco == ''){
        $cont_porcento ++;
    }
    if($bairro == ''){
        $cont_porcento ++;
    }
    if($referencia == ''){
        $cont_porcento ++;
    }
    if($id_cidade_domiciliar == ''){
        $cont_porcento ++;
    }
    $quantidade_obg = 14;

    $porcentagem_ncadastrada = round(($cont_porcento/$quantidade_obg)*100);
    $porcentagem_cadastrada= 100-$porcentagem_ncadastrada;


    /////////////
    mysql_query("begin");

    $sql = "INSERT INTO clientes (`nome`,`nascimento`,`sexo`,`convenio`,`dataInternacao`,`localInternacao`,`diagnostico`,
  								`medicoSolicitante`,`empresa`,`responsavel`,`relresp`,`rsexo`,`cuidador`,`relcuid`,`csexo`,
  								`espSolicitante`,`acompSolicitante`,`endereco`,`bairro`,`referencia`,LEITO,TEL_POSTO,PES_CONTATO_POSTO,TEL_DOMICILIAR,TEL_LOCAL_INTERNADO,TEL_RESPONSAVEL,TEL_CUIDADOR,
  								END_INTERNADO,BAI_INTERNADO,REF_ENDERECO_INTERNADO,CUIDADOR_ESPECIALIDADE, RESPONSAVEL_ESPECIALIDADE,CIDADE_ID_UND,CIDADE_ID_DOM,CIDADE_ID_INT,END_UND_ORIGEM,NUM_MATRICULA_CONVENIO,LIMINAR,DATA_CADASTRO,PORCENTAGEM,
  								cpf,cpf_responsavel,nascimento_responsavel,nascimento_cuidador)
	  VALUES ('$nome','$nascimento','$sexo','$convenio','$dataInternacao','$local','$diagnostico','$solicitante','$empresa',
	  		  '$resp','$relresp','$rsexo','$cuid','$relcuid','$csexo','$especialidade','$acomp','$endereco','$bairro','$referencia','$leito','$telPosto','$contato','$telDomiciliar','$telInternacao','$telResponsavel','$telCuidador','$enderecoInternacao','$bairroInternacao',
  '$referenciaInternacao','$especialidadeCuidador', '$especialidadeResponsavel','$id_cidade_und','$id_cidade_domiciliar','$id_cidade_internado','$end_und_origem','$num_matricula_convenio','$liminar',now(),'$porcentagem_cadastrada',
  '$cpf','$cpfResponsavel','$nascimentoResp','$nascimentoCuid') ";

    if(!mysql_query($sql)){
        mysql_query("rollback");
        echo "erro";
        return;
    }
    $id = mysql_insert_id();

    // This task add to the Queue
    /*$action = new \App\Integration\Obsequium\PacienteAction();
    $action->created($id, $nome, $sexo);*/

    savelog(mysql_escape_string(addslashes($sql)));
    $codigo= "N{$id}";
    $sql="UPDATE clientes SET `codigo`='$codigo'  WHERE `idClientes` = '$id'";
    if(!mysql_query($sql)){
        mysql_query("rollback");
        echo "erro";
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));
    $usuario = $_SESSION["id_user"];
    $sql = "INSERT INTO historico_plano_liminar_paciente (PACIENTE_ID,USUARIO_ID,LIMINAR,PLANO_ID,DATA) values ('$id','$usuario','$liminar','$convenio',now())";
    if(!mysql_query($sql)){
        mysql_query("rollback");
        echo "erro";
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));
    mysql_query("commit");

    echo $codigo;
    return;
}

function editar_paciente($post){
  extract($post,EXTR_OVERWRITE);
  $id = anti_injection($paciente,'numerico');
  $nascimento = implode("-",array_reverse(explode("/",$nascimento)));
  $nome = anti_injection($nome,'literal');
  $codigo = anti_injection($codigo,'literal');
  $diagnostico = anti_injection($diagnostico,'literal');
  $local = anti_injection($local,'literal');
  $solicitante = anti_injection($solicitante,'literal');
  $sexo = anti_injection($sexo,'literal');
  $csexo = anti_injection($csexo,'literal');
  $rsexo = anti_injection($rsexo,'literal');
  $resp = anti_injection($resp,'literal');
  $relresp = anti_injection($relresp,'literal');
  $cuid = anti_injection($cuid,'literal');
  $relcuid = anti_injection($relcuid,'literal');
  $endereco = anti_injection($endereco,'literal');
  $especialidade = anti_injection($especialidade,'literal');
  $acomp = anti_injection($acomp,'literal');
  $bairro = anti_injection($bairro,'literal');
  $referencia = anti_injection($referencia,'literal');
  $leito = anti_injection($leito,'literal');
  $telPosto = anti_injection($telPosto,'literal');
  $telContato = anti_injection($telContato,'literal');
  $contato = anti_injection($contato,'literal');
  $telResponsavel = anti_injection($telResponsavel,'literal');
  $telCuidador = anti_injection($telCuidador,'literal');
  $enderecoInternacao= anti_injection($enderecoInternacao,'literal');
  $bairroInternacao = anti_injection($bairroInternacao,'literal');
  $referenciaInternacao= anti_injection( $referenciaInternacao,'literal');
  $telInternacao= anti_injection( $telInternacao,'literal');
  $telDomiciliar= anti_injection( $telDomiciliar,'literal');
  $especialidadeCuidador= anti_injection( $especialidadeCuidador,'literal');
  $especialidadeResponsavel= anti_injection( $especialidadeResponsavel,'literal');
  $id_cidade_und = anti_injection( $id_cidade_und,'literal');
  $id_cidade_domiciliar = anti_injection( $id_cidade_domiciliar,'literal');
  $id_cidade_internado = anti_injection( $id_cidade_internado,'literal');
  $end_und_origem = anti_injection( $end_und_origem,'literal');
  $num_matricula_convenio = anti_injection( $num_matricula_convenio,'literal');
  $liminar =anti_injection( $liminar,'numerico');
    $cpf = anti_injection( $cpf,'literal');
    $cpf = str_replace('-','',str_replace('.','',$cpf));
    $cpfResponsavel = anti_injection( $cpf_responsavel,'literal');
    $cpfResponsavel = str_replace('-','',str_replace('.','',$cpfResponsavel));
    $nascimentoResp =anti_injection( $nascimento_responsavel,'literal');
    $nascimentoCuid =anti_injection( $nascimento_cuidador,'literal');
    $nascimentoCuid = implode("-",array_reverse(explode("/", $nascimentoCuid)));
    $nascimentoResp = implode("-",array_reverse(explode("/", $nascimentoResp)));

  ///////////////jeferson 10/10/2013 verificar os campos obrigatorios que foram cadastrados com . ou - para ver quantos porcentos do cadastro esta faltando informação.
  $cont_porcento =0;
  if($nome =='-' || $nome =='.'){
      $cont_porcento ++;
  }
   if($num_matricula_convenio =='-' || $num_matricula_convenio =='.'){
      $cont_porcento ++;
  }
   if($num_matricula_convenio =='-' || $num_matricula_convenio =='.'){
      $cont_porcento ++;
  }
   if($solicitante =='-' || $solicitante =='.'){
      $cont_porcento ++;
  }
  if($especialidade =='-' || $especialidade =='.'){
      $cont_porcento ++;
  }
  if($local =='-' || $local =='.'){
      $cont_porcento ++;
  }
  if($end_und_origem =='-' || $end_und_origem =='.'){
      $cont_porcento ++;
  }
   if($id_cidade_und =='-' || $id_cidade_und =='.'){
      $cont_porcento ++;
  }
  if($contato =='-' || $contato =='.'){
      $cont_porcento ++;
  }
  if($resp =='-' || $resp =='.'){
      $cont_porcento ++;
  }
  if($telResponsavel =='-' || $telResponsavel =='.'){
      $cont_porcento ++;
  }
  if($endereco =='-' || $endereco =='.'){
      $cont_porcento ++;
  }
  if($bairro =='-' || $bairro =='.'){
      $cont_porcento ++;
  }
   if($referencia =='-' || $referencia =='.'){
      $cont_porcento ++;
  }
  if($id_cidade_domiciliar=='-' || $id_cidade_domiciliar =='.'){
      $cont_porcento ++;
  }
 $quantidade_obg = 25;

  $porcentagem_ncadastrada = round(($cont_porcento/$quantidade_obg)*100,2);
  $porcentagem_cadastrada= 100-$porcentagem_ncadastrada;


  /////////////

  mysql_query('begin');


  $sql = "UPDATE clientes SET `nome` = '$nome',`nascimento` = '$nascimento',`sexo` = '$sexo',`codigo` = '$codigo',`convenio` = '$convenio',
	  `localInternacao` = '$local',`diagnostico` = '$diagnostico',`medicoSolicitante` = '$solicitante', 
	  `empresa` = '$empresa', `responsavel` = '$resp', `relresp` = '$relresp', `rsexo` = '$rsexo', 
	  `cuidador` = '$cuid', `relcuid` = '$relcuid', `csexo` = '$csexo', `espSolicitante` = '$especialidade',`acompSolicitante` = '$acomp', 
	  `endereco` = '$endereco', `bairro` = '$bairro', `referencia` = '$referencia', `LEITO` = '$leito', `TEL_POSTO`= '$telPosto', `PES_CONTATO_POSTO`= '$contato', `TEL_DOMICILIAR`='$telDomiciliar',`TEL_LOCAL_INTERNADO`='$telInternacao',
`TEL_RESPONSAVEL`= '$telResponsavel', `TEL_CUIDADOR`= '$telCuidador',`END_INTERNADO`='$enderecoInternacao',`BAI_INTERNADO`='$bairroInternacao', `REF_ENDERECO_INTERNADO`='$referenciaInternacao', 
`CUIDADOR_ESPECIALIDADE`= '$especialidadeCuidador',`RESPONSAVEL_ESPECIALIDADE`= '$especialidadeResponsavel', 
 `CIDADE_ID_UND`='$id_cidade_und', `CIDADE_ID_INT`='$id_cidade_internado', `CIDADE_ID_DOM`='$id_cidade_domiciliar', `END_UND_ORIGEM`=' $end_und_origem', `NUM_MATRICULA_CONVENIO`='$num_matricula_convenio',
	`LIMINAR`='$liminar',`PORCENTAGEM`='$porcentagem_cadastrada', cpf = '{$cpf}', cpf_responsavel = '{$cpfResponsavel}',
	  nascimento_responsavel = '{$nascimentoResp}' , nascimento_cuidador= '{$nascimentoCuid}' WHERE `idClientes` = '$id'";
  if(!mysql_query($sql)){
		mysql_query("rollback");
		echo "erro";
		return;
	}
        savelog(mysql_escape_string(addslashes($sql)));



         $usuario = $_SESSION["id_user"];
        $sql = "INSERT INTO historico_plano_liminar_paciente (PACIENTE_ID,USUARIO_ID,LIMINAR,PLANO_ID,DATA) values ('$id','$usuario','$liminar','$convenio',now())";
        if(!mysql_query($sql)){
		mysql_query("rollback");
		echo "erro";
		return;
	}
        savelog(mysql_escape_string(addslashes($sql)));
        mysql_query('commit');

  // This task add to the Queue
  $action = new \App\Integration\Obsequium\PacienteAction();
  $action->updated($id, $nome, $sexo);

  echo $codigo;
}

function excluir_paciente($id){
}

function del_acompanhamento($post){
  extract($post,EXTR_OVERWRITE);
  $id=anti_injection($id,'numerico');
  $sql = "DELETE FROM equipePaciente WHERE id = '{$id}' ";
  if(mysql_query($sql)){
  	savelog(mysql_escape_string(addslashes($sql)));
    echo 1;
  }
  else echo "erro";
}

function add_acompanhamento($post){
  extract($post,EXTR_OVERWRITE);
  $data = implode("-",array_reverse(explode("/",$inicio)));
  $paciente=anti_injection($paciente,'numerico');
  $funcionario=anti_injection($funcionario,'numerico');
  $data=anti_injection($data,'literal');
  $sql = "INSERT INTO equipePaciente (`funcionario`,`paciente`,`inicio`) VALUES ('$funcionario','$paciente','$data') ";
  if(mysql_query($sql)){
  	savelog(mysql_escape_string(addslashes($sql)));
    echo 1;
  }
  else echo "erro";
}

function end_acompanhamento($post){
  extract($post,EXTR_OVERWRITE);
  $data = implode("-",array_reverse(explode("/",$fim)));
  $data=anti_injection($data,'literal');
  $id=anti_injection($id,'numerico');
  $sql = "UPDATE equipePaciente SET `fim` = '$data' WHERE id = '{$id}' ";
  if(mysql_query($sql)){
  	savelog(mysql_escape_string(addslashes($sql)));
    echo 1;
  }
  else echo "erro";
}

####################################### SALVAR OBSERVA��ES ####################################
function salvar_obs($post){
	echo "0";
	extract($post,EXTR_OVERWRITE);
	$cod = anti_injection($cod,'numerico');
	$obs = anti_injection($obs,'literal');
	$sql = "UPDATE principioativo SET DescricaoMedico = '{$obs}' WHERE id = '{$cod}' ";
	$r = mysql_query($sql);
	if(!$r){
		echo "ERRO: tente mais tarde!";
		return;
	}
	echo "1";
}


##################################### EDITAR PRODUTO INTERNO ####################################
function editar_produtoInterno($post){
	echo "0";
	extract($post,EXTR_OVERWRITE);
	$cod = anti_injection($cod,'numerico');
	$obs = anti_injection($nome,'literal');
	$sql = "UPDATE principioativo SET principio = '{$nome}' WHERE id = '{$cod}' and status = 0 and AddMederi = 1 ";
	$r = mysql_query($sql);
	if(!$r){
		echo "ERRO: tente mais tarde!";
		return;
	}
	echo "1";
}

############################################ FIM ##############################################
function historico_acompanhamento($post){
  extract($post,EXTR_OVERWRITE);
  $has = false;
  $tipo = anti_injection($tipo,'literal');
  $id = anti_injection($id,'numerico');
  echo "<table class='mytable' width=100% >";
  echo "<thead><tr>";
  echo "<th width=40%><b>M&eacute;dico</b></th>";
  echo "<th width=15%><b>In&iacute;cio</b></th>";
  echo "<th width=15%><b>Fim</b></th>";
  echo "</tr></thead><tfoot><tr><th colspan='3'></th></tr></tfoot>";
  $result = mysql_query("SELECT e.*, u.nome FROM equipePaciente as e, usuarios as u WHERE e.funcionario = u.idUsuarios AND u.tipo = '$tipo' AND paciente = '$id' AND fim IS NOT NULL ORDER BY nome");
  $cor = false;
  while($row = mysql_fetch_array($result))
  {
    $has = true;
    foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
    if($cor) echo "<tr cod='{$row['id']}' >";
    else echo "<tr cod='{$row['id']}' class='odd' >";
    $cor = !$cor;
    echo "<td>{$row['nome']}</td>";
    $inicio = implode("/",array_reverse(explode("-",$row['inicio'])));
    $fim = implode("/",array_reverse(explode("-",$row['fim'])));
    echo "<td>{$inicio}</td>";
    echo "<td>{$fim}</td>";
    echo "</tr>";
  }
  if(!$has) echo "<tr><td colspan='5' ><center><b>Nenhuma equipe está no hist&oacute;rico do paciente.</b></center></td></tr>";
  echo "</table>";
}

function funcionarios($post){
  extract($post,EXTR_OVERWRITE);
  $tipo = anti_injection($tipo,'literal');
  $id = anti_injection($id,'numerico');
  $sql = "SELECT u.idUsuarios as id, u.nome FROM usuarios as u WHERE u.tipo = '{$tipo}' AND idUsuarios NOT IN (SELECT funcionario FROM equipePaciente WHERE paciente = '{$id}' AND fim IS NULL)";
  $result = mysql_query($sql);
  $t = "Enfermeiro";
  if($tipo == "medico")
    $t = "<p>M&eacute;dico";
  $var = "<b>$t</b><select name='funcionario' style='background-color:transparent;'>";
  while($row = mysql_fetch_array($result))
  {
    $var .= "<option value='". $row['id'] ."'>". $row['nome'] ."</option>";
  }
  $var .= "</select>";
  echo $var;
}

function show_permissoes($post){
	extract($post,EXTR_OVERWRITE);
	$cod = anti_injection($cod,'numerico');
 	$row = mysql_fetch_array ( mysql_query("SELECT * FROM `usuarios` WHERE `idUsuarios` = '{$cod}' "));
 	$var = "<p><b>SysAdmin:</b><br />";
 	$var .= "<input type='checkbox' name='sysadm' value='1' ".(($row['adm']=='1')?"checked":"")." />Sim&nbsp;&nbsp;";
	$var .= "<p><b>Acesso:</b><br />";
  	$var .= "<input type='checkbox' name='modadm'  ".(($row['modadm']=='1')?"checked":"")." />Administrativo<br/>";
 	$var .= "<input type='checkbox' name='modaud'  ".(($row['modaud']=='1')?"checked":"")." />Auditoria<br/>";
 	$var .= "<input type='checkbox' name='modenf'  ".(($row['modenf']=='1')?"checked":"")." />Enfermagem<br/>";
 	$var .= "<input type='checkbox' name='modfin'  ".(($row['modfin']=='1')?"checked":"")." />Financeiro<br/>";
 	$var .= "<input type='checkbox' name='modlog'  ".(($row['modlog']=='1')?"checked":"")." />Log&iacute;stica<br/>";
 	$var .= "<input type='checkbox' name='modmed'  ".(($row['modmed']=='1')?"checked":"")." />M&eacute;dico<br/>";
 	$var .= "<input type='checkbox' name='modrem'  ".(($row['modrem']=='1')?"checked":"")." />Remo&ccedil;&otilde;es<br/>";
 	$var .= "<input type='checkbox' name='modnutri'  ".(($row['modnutri']=='1')?"checked":"")." />Nutri&ccedil;&atilde;o<br/>";
    $var .= "<input type='checkbox' name='modfisio'  ".(($row['modfisio']=='1')?"checked":"")." />Fisioterapia";

    echo $var;
}

function salvar_permissoes($post){
	extract($post,EXTR_OVERWRITE);
	$cod = anti_injection($cod,'numerico');
	$sysadm = (($sysadm=='true')?'1':'0');
	$modadm = (($modadm=='true')?'1':'0');$modaud = (($modaud=='true')?'1':'0');$modenf = (($modenf=='true')?'1':'0');
	$modfin = (($modfin=='true')?'1':'0');$modlog = (($modlog=='true')?'1':'0');$modmed = (($modmed=='true')?'1':'0');
	$modrem = (($modrem=='true')?'1':'0');
    $modfisio = (($modfisio=='true')?'1':'0');

    $sysadm = anti_injection($sysadm,'literal');
        $modadm = anti_injection($modadm,'literal');
         $modaud = anti_injection($modaud,'literal');
          $modenf = anti_injection($modenf,'literal');
           $modfin = anti_injection($modfin,'literal');
    $modfisio = anti_injection($modfisio,'literal');
	$sql = "UPDATE `usuarios` SET `adm` = '{$sysadm}', `modadm` = '{$modadm}', `modaud` = '{$modaud}', `modenf` = '{$modenf}', `modfin` = '{$modfin}', `modlog` = '{$modlog}', `modmed` = '{$modmed}', `modrem` = '{$modrem}', `modnutri` = '{$modnutri}', `modfisio` = '{$modfisio}' WHERE `idUsuarios` = '{$cod}' ";
	$r = mysql_query($sql);
	if(!$r)
		echo"ERRO: Tente mais tarde!";
        // mysql_error();
	else{
		savelog(mysql_escape_string(addslashes($sql)));
		echo "Salvo com sucesso!";
	}
}

function salvar_novo_plano($post){
	extract($post,EXTR_OVERWRITE);
	mysql_query("begin");
    $iss = anti_injection(str_replace(",",".",str_replace(".","",$iss)),"numerico");

	$sql = "INSERT INTO planosdesaude (
nome,
registro_ans,
imposto_iss,
tipo_medicamento,
ano_contrato,
created_at,
created_by)
            VALUES (
'{$plano}',
'{$registro_ans}',
'{$iss}',
'{$tipo_medicamento}',
'{$ano_contrato}',

now(),
{$_SESSION['id_user']}
) ";

	$r = mysql_query($sql);
	if(!$r){
		$err = mysql_errno();
		if($err = 1062) echo "ERRO: Nome do plano já existe, escolha outro.";
		else echo "ERRO: ";
                //$err." ".mysql_error();
		mysql_query("rollback");
		return;
	}
	$id = mysql_insert_id();
	savelog(mysql_escape_string(addslashes($sql)));

    $sql = "INSERT INTO historico_planosdesaude (
planosdesaude_id,
nome,
registro_ans,
imposto_iss,
tipo_medicamento,
ano_contrato,
created_at,
created_by,
acao
) VALUES (
{$id},
'{$plano}',
'{$registro_ans}',
'{$iss}',
'{$tipo_medicamento}',
'{$ano_contrato}',
now(),
{$_SESSION['id_user']},
'criar'
) ";

    $r = mysql_query($sql);
    if(!$r){
        echo "ERRO: Ao salvar histórico do plano de saúde. ".$sql;
        mysql_query("rollback");
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));

	$sql = "INSERT INTO operadoras_planosdesaude VALUES ({$operadora}, {$id})";
	$rs = mysql_query($sql);
	if(!$rs){
		echo "ERRO " . mysql_error();
		mysql_query("rollback");
		return;
	}
	savelog(mysql_escape_string(addslashes($sql)));

	$itens = explode("$",$dados);
	foreach($itens as $item){
		if($item != "" && $item != null){
			$i = explode("#",$item);
			$cob_ranca = anti_injection($i[0],"numerico");
			$item_plano = anti_injection($i[2],"literal");
			$cod_item_plano = anti_injection($i[3],"literal");
			$valor = anti_injection(str_replace(",",".",str_replace(".","",$i[1])),"numerico");

			$r = mysql_query("INSERT INTO valorescobranca (idPlano, idCobranca, valor,DESC_COBRANCA_PLANO,COD_COBRANCA_PLANO) VALUES ('{$id}','{$cob_ranca}','{$valor}','{$item_plano}','{$cod_item_plano}')");
			if(!$r){
				echo "ERRO: ";
                                //$err." ".mysql_error();
				mysql_query("rollback");
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}

	$tabelas_regras = [
	    '0' => 'regra_fluxo_medicamentos',
	    '1' => 'regra_fluxo_materiais',
	    '2' => 'regra_fluxo_servicos',
	    '3' => 'regra_fluxo_dietas',
	    '5' => 'regra_fluxo_equipamentos',
    ];

    $origem_regras = [
        '0' => 'medicamento',
        '1' => 'material',
        '2' => 'servico',
        '3' => 'dieta',
        '5' => 'equipamento',
    ];

    foreach ($regras as $regra) {
        $sql = <<<SQL
INSERT INTO regra_fluxo
  (
    id,
    tabela_origem,
    regra_primaria,
    opcao_regra,
    plano_id
  )
VALUES 
  (
    NULL, 
    '{$origem_regras[$regra['tipo']]}', 
    '{$regra['regra_primaria']}', 
    '{$regra['opcao']}', 
    '{$id}'
  )
SQL;
        $rs = mysql_query($sql);
        if (!$rs) {
            echo "ERRO: " . mysql_error();
            mysql_query("rollback");
            return;
        } else {
            $regra_id = mysql_insert_id();
            $sql = <<<SQL
INSERT INTO {$tabelas_regras[$regra['tipo']]}
  (
    id,
    tipo_solicitacao,
    tipo_validacao,
    regra_fluxo_id,
    item_id,
    created_at,
    created_by,
    deleted_at,
    deleted_by
  )
VALUES 
  (
    NULL, 
    '{$regra['carater']}', 
    '{$regra['tipo_validacao']}',
    '{$regra_id}', 
    '{$regra['item']}',
    NOW(),
    '{$_SESSION['id_user']}',
    NULL,
    NULL
  )
SQL;
            $rs = mysql_query($sql);
            if (!$rs) {
                echo "ERRO: " . mysql_error();
                mysql_query("rollback");
                return;
            } else {
                if ($regra['tipo_validacao'] == '2') {
                    $regra_fluxo_item_id = mysql_insert_id();
                    $sql = <<<SQL
INSERT INTO regra_fluxo_condicionais 
  (
    id,
    tabela_origem,
    tabela_origem_id,
    campo_comparacao,
    operador,
    valor_referencia
  )
VALUES 
  (
    NULL,
    '{$origem_regras[$regra['tipo']]}',
    '{$regra_fluxo_item_id}',
    '{$regra['campo']}',
    '{$regra['operador']}',
    '{$regra['valor_referencia']}'
  )
SQL;
                    $rs = mysql_query($sql);
                    if (!$rs) {
                        echo "ERRO: " . mysql_error();
                        mysql_query("rollback");
                        return;
                    }
                }
            }
        }
    }

    mysql_query("commit");
    echo $id;
    return;
}

function salvar_edicao_plano($post){
	extract($post,EXTR_OVERWRITE);
	mysql_query("begin");

	$plano = anti_injection($plano,"literal");
    $registro_ans = anti_injection($registro_ans,"literal");
	$id = anti_injection($id,"numerico");
    $iss = anti_injection(str_replace(",",".",str_replace(".","",$iss)),"numerico");
    if(isset($percentual_reajuste) && $percentual_reajuste != '') {
        $percentual_reajuste = anti_injection(str_replace(",", ".", str_replace(".", "", $percentual_reajuste)), "numerico");
    }
    if(isset($data_reajuste) && $data_reajuste != '') {
        $data_reajuste = implode('-', array_reverse(explode('/', $data_reajuste)));
    }
	$sql = "UPDATE planosdesaude
SET nome = '{$plano}',
registro_ans = '{$registro_ans}',
imposto_iss ='{$iss}',
tipo_medicamento = '{$tipo_medicamento}',
ano_contrato = '{$ano_contrato}',
data_reajuste = '{$data_reajuste}',
percentual_reajuste = '{$percentual_reajuste}',
updated_by = {$_SESSION['id_user']},
updated_at = now()

                WHERE id = '{$id}' ";

	$r = mysql_query($sql);
	if(!$r){
		$err = mysql_errno();
		if($err = 1062) echo "Nome do plano já existe, escolha outro.";
		else echo "ERRO: ";
                //$err." ".mysql_error();
		mysql_query("rollback");
		return;
	}
	savelog(mysql_escape_string(addslashes($sql)));
    $sql = "INSERT INTO historico_planosdesaude (
planosdesaude_id,
nome,
registro_ans,
imposto_iss,
tipo_medicamento,
ano_contrato,
data_reajuste,
percentual_reajuste,
created_at,
created_by,
acao)
            VALUES (
{$id},
'{$plano}',
'{$registro_ans}',
'{$iss}',
'{$tipo_medicamento}',
'{$ano_contrato}',
'{$data_reajuste}',
'{$percentual_reajuste}',
now(),
{$_SESSION['id_user']},
'editar'
) ";

    $r = mysql_query($sql);
    if(!$r){


        echo "ERRO: Ao salvar histórico do plano de saúde. " . $sql . mysql_error();
        mysql_query("rollback");
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));

	$sql = "DELETE FROM operadoras_planosdesaude WHERE PLANOSDESAUDE_ID = {$id}";
	$rs = mysql_query($sql);
	if(!$rs){
		echo "ERRO";
		mysql_query("rollback");
		return;
	}
    savelog(mysql_escape_string(addslashes($sql)));

	$sql = "INSERT INTO operadoras_planosdesaude VALUES ({$operadora}, {$id})";
	$rs = mysql_query($sql);
	if(!$rs){
		echo "ERRO";
		mysql_query("rollback");
		return;
	}

	savelog(mysql_escape_string(addslashes($sql)));

	$itens = explode("$",$dados);
	foreach($itens as $item){
	if($item != "" && $item != null){
			$i = explode("#",$item);
			$cob_ranca = anti_injection($i[0],"numerico");
			$item_plano = anti_injection($i[2],"literal");
			$cod_item_plano = anti_injection($i[3],"literal");
			$valor = anti_injection(str_replace(",",".",str_replace(".","",$i[1])),"numerico");
            $editado = anti_injection($i[4],"literal");
            $idValorescobranca = anti_injection($i[5],"literal");

        if($editado == 1){
            $sql = "Update  valorescobranca set excluido_em = now() , excluido_por = {$_SESSION['id_user']} where id = {$idValorescobranca}";
            $r = mysql_query($sql);
            if(!$r){
                $err = mysql_errno();
                echo "ERRO: ";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
        }
           // editado recebe 1 quando o item já era cadastrado e alguem modifica a linha, iteme recebe F quando e inserido pelo usuario.
			if($editado == 1 || $editado == 'F' ){
                $r = mysql_query("INSERT INTO valorescobranca (idPlano, idCobranca, valor,DESC_COBRANCA_PLANO,COD_COBRANCA_PLANO) VALUES ('{$id}','{$cob_ranca}','{$valor}','{$item_plano}','{$cod_item_plano}')");
                if(!$r){
                    echo "ERRO: ";
                    //$err." ".mysql_error();
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }
		}
	}

    $tabelas_regras = [
        '0' => 'regra_fluxo_medicamentos',
        '1' => 'regra_fluxo_materiais',
        '2' => 'regra_fluxo_servicos',
        '3' => 'regra_fluxo_dietas',
        '5' => 'regra_fluxo_equipamentos',
    ];

    $origem_regras = [
        '0' => 'medicamento',
        '1' => 'material',
        '2' => 'servico',
        '3' => 'dieta',
        '5' => 'equipamento',
    ];

    \App\Models\Administracao\Planos::deleteCascadeFluxoPlano($id);

    foreach ($regras as $regra) {
        $sql = <<<SQL
INSERT INTO regra_fluxo
  (
    id,
    tabela_origem,
    regra_primaria,
    opcao_regra,
    plano_id
  )
VALUES 
  (
    NULL, 
    '{$origem_regras[$regra['tipo']]}', 
    '{$regra['regra_primaria']}', 
    '{$regra['opcao']}', 
    '{$id}'
  )
SQL;
        $rs = mysql_query($sql);
        if (!$rs) {
            echo "ERRO: " . mysql_error();
            mysql_query("rollback");
            return;
        } else {
            $regra_id = mysql_insert_id();

            $sql = <<<SQL
INSERT INTO {$tabelas_regras[$regra['tipo']]}
  (
    id,
    tipo_solicitacao,
    tipo_validacao,
    regra_fluxo_id,
    item_id,
    created_at,
    created_by,
    deleted_at,
    deleted_by
  )
VALUES 
  (
    NULL, 
    '{$regra['carater']}', 
    '{$regra['tipo_validacao']}',
    '{$regra_id}', 
    '{$regra['item']}',
    NOW(),
    '{$_SESSION['id_user']}',
    NULL,
    NULL
  )
SQL;
            $rs = mysql_query($sql);
            if (!$rs) {
                echo "ERRO: " . mysql_error();
                mysql_query("rollback");
                return;
            } else {
                if ($regra['tipo_validacao'] == 'condicional' || $regra['tipo_validacao'] == '2') {
                    $regra_fluxo_item_id = mysql_insert_id();
                    $sql = <<<SQL
INSERT INTO regra_fluxo_condicionais 
  (
    id,
    tabela_origem,
    tabela_origem_id,
    campo_comparacao,
    operador,
    valor_referencia
  )
VALUES 
  (
    NULL,
    '{$origem_regras[$regra['tipo']]}',
    '{$regra_fluxo_item_id}',
    '{$regra['campo']}',
    '{$regra['operador']}',
    '{$regra['valor_referencia']}'
  )
SQL;
                    $rs = mysql_query($sql);
                    if (!$rs) {
                        echo "ERRO: " . mysql_error();
                        mysql_query("rollback");
                        return;
                    }
                }
            }
        }
    }

	mysql_query("commit");
	echo "1";
	return;
}

/*function salvar_ficha($post,$file){
  extract($post,EXTR_OVERWRITE);
  $paciente = anti_injection($paciente,'numerico');
  $tipoficha = anti_injection($tipo,'numerico');
  $trava = 0;
  if(isset($post["trava"])) $trava = 1;
  $tipoficha = $tipo;
  $arquivo = $file["ficha"]["tmp_name"]; 
  $tamanho = $file["ficha"]["size"];
  $tipo = $file["ficha"]["type"];
  $nome = $file["ficha"]["name"];

  if ( $arquivo != "none" ){
    $fp = fopen($arquivo, "rb");
    $conteudo = fread($fp, $tamanho);
    $conteudo = addslashes($conteudo);
    fclose($fp);
    $sql = "INSERT INTO fichapaciente (`paciente`, `lockf`, `tipoficha`, `data`, `nome`, `tipo`, `size`, `content`) VALUES ('$paciente', '$trava' , '$tipoficha', now(), '$nome', '$tipo', '$tamanho', '$conteudo')";
    if(mysql_num_rows(mysql_query("SELECT 1 FROM fichapaciente WHERE `paciente` = '{$paciente}' AND `tipoficha` = '{$tipoficha}' "))){
    	$sql = "UPDATE fichapaciente SET `lockf` = '{$trava}', `data` = now(), `nome` = '{$nome}', `tipo` = '{$tipo}', `size` = '{$tamanho}', `content` = '{$conteudo}' WHERE `paciente` = '{$paciente}' AND `tipoficha` = '{$tipoficha}' ";
    }
    $r = mysql_query($sql);
    if($r){
		redireciona("-1");
    }
  }
}*/
/*
function show_ficha($p,$t){
  $p = anti_injection($p,'numerico');
  $t = anti_injection($t,'numerico');
  $sql = "SELECT tipo, content, nome FROM fichapaciente WHERE paciente='$p' AND tipoficha='$t' ";
  $result = mysql_query($sql);
  if($row = mysql_fetch_array($result)){
	  header("Content-type: \"{$row['tipo']}\"");
	  header("Content-Disposition: attachment; filename=\"{$row['nome']}\";" ); 
	  print $row['content'];
  } else {
  	echo "<script>alert('sem ficha');</script>";
  	redireciona("?adm=paciente&act=show&id={$p}");
  }
}*/

function salvar_filial($post){
  $op = '';
  $nome = '';
  $percentual = 0;
	extract($post,EXTR_OVERWRITE);
	$op = anti_injection($op,"literal");
	$nome = anti_injection($nome,"literal");
  $percentual = str_replace(',','.',$percentual);
  $percentual = anti_injection($percentual,"numerico");
    $iss = str_replace(',','.',$iss);
    $iss = anti_injection($iss,"numerico");
  //print_r($post); die();
	$sql = "1";
  mysql_query('begin');
  if($op == 'novo'){
    $sql = "INSERT INTO empresas (`nome`, `percentual`,imposto_iss) VALUES ('{$nome}', '{$percentual}', '{$iss}')";

    if(!mysql_query($sql)){
      if($err = 1062) {
        echo "Nome da empresa já existe, escolha outro.";
      }else{
        echo "Erro ao salvar ";
      }
     mysql_query('rollback');
      return;
    }
    $idEmpresas =mysql_insert_id();

    savelog(mysql_escape_string(addslashes($sql)));

    $sqlEstoque =" ALTER TABLE
                    `estoque` ADD COLUMN
                     QTD_MIN_{$idEmpresas}  int(11) NOT NULL DEFAULT 0 ,
                    ADD COLUMN empresas_id_{$idEmpresas}  int(11) NOT NULL DEFAULT 0 ";
    if(!mysql_query($sqlEstoque)){
      echo "Erro ao gerar estoque.";
      mysql_query('rollback');
      return;
    }
    savelog(mysql_escape_string(addslashes($sqlEstoque)));

      $sql = "insert into
historico_empresa_imposto_iss
(created_at,
created_by,
empresa_id,
imposto_iss)
values
(
now(),
{$_SESSION['id_user']},
$idEmpresas,
$iss
)";
      if(!mysql_query($sql)){
          echo "Erro ao gerar gerar historico de ISS.";
          mysql_query('rollback');
          return;
      }
      savelog(mysql_escape_string(addslashes($sql)));


  } elseif ($op =='editar'){
    $sql = "UPDATE empresas SET `nome` = '{$nome}', `percentual` = '{$percentual}', imposto_iss = '{$iss}' WHERE id = '{$cod}' ";
    if(!mysql_query($sql)){
      if($err = 1062) {
        echo "Nome da empresa já existe, escolha outro.";
      }else{
        echo "Erro ao editar ";
      }

      mysql_query('rollback');
      return;
    }
    savelog(mysql_escape_string(addslashes($sql)));

      $sql = "UPDATE
historico_empresa_imposto_iss
set
canceled_by = {$_SESSION['id_user']} ,
canceled_at = now()
 WHERE
 empresa_id = {$cod} and
 canceled_by  is null";
      if(!mysql_query($sql)){

              echo "Erro ao editar o historico de empresa e ISS";

          mysql_query('rollback');
          return;
      }

      $sql = "insert into
historico_empresa_imposto_iss
(created_at,
created_by,
empresa_id,
imposto_iss)
values
(
now(),
{$_SESSION['id_user']},
$cod,
$iss
)";
      if(!mysql_query($sql)){
          echo "Erro ao gerar gerar historico de ISS.";
          mysql_query('rollback');
          return;
      }
      savelog(mysql_escape_string(addslashes($sql)));
  }

  mysql_query('commit');

    echo 1;
  return ;
}

function excluir_filial($post){
	extract($post,EXTR_OVERWRITE);
	$id = anti_injection($cod,"numerico");
	$sql = "DELETE FROM empresas WHERE id = '{$id}' ";
	if(mysql_query($sql)){
		echo "1";
		return;
	}
	echo "Erro:";
}

function excluir_item_plano($post){
	extract($post,EXTR_OVERWRITE);
	$id = anti_injection($cod,"numerico");
	$sql = "DELETE FROM valorescobranca WHERE id = '{$id}' ";
	if(mysql_query($sql)){
		echo "1";
		return;
	}
        echo "Erro:";
	//echo mysql_error();
}

function excluir_plano($post){
	extract($post,EXTR_OVERWRITE);
	$id = anti_injection($cod,"numerico");
	$sql = "Update  planosdesaude set ATIVO = 'N'  WHERE id = '{$id}' ";
	if(mysql_query($sql)){
		echo "1";
		return;
	}
        echo "Erro:";
	//echo mysql_error();
}

function nova_capmed($post){
	extract($post,EXTR_OVERWRITE);
	$paciente = anti_injection($paciente,"numerico");
	$motivo = anti_injection($motivo,"literal");

	$cancer = anti_injection($cancer,"literal");
	$psiquiatrico = anti_injection($psiquiatrico,"literal");
	$neurologica = anti_injection($neurologica,"literal");
	$galucoma = anti_injection($glaucoma,"literal");
	$hepatopatia = anti_injection($hepatopatia,"literal");
	 $obesidade = anti_injection($obesidade,"literal");
	$cardiopatia = anti_injection($cardiopatia,"literal");
	$dm = anti_injection($dm,"literal");
	 $reumatologica = anti_injection($reumatologica,"literal");
	$has = anti_injection($has,"literal");
	$nefropatia = anti_injection($nefropatia,"literal");
	$pneumopatia = anti_injection($pneumopatia,"literal");

	$palergiamed = anti_injection($palergiamed,"literal");
	 $alergiamed = anti_injection($alergiamed,"literal");
	$palergiaalim = anti_injection($palergiaalim,"literal");
	$alergiaalim = anti_injection($alergiaalim,"literal");

	$prelocohosp = anti_injection($prelocohosp,"literal");
	$prehigihosp = anti_injection($prehigihosp,"literal");
	$preconshosp = anti_injection($preconshosp,"literal");
	$prealimhosp = anti_injection($prealimhosp,"literal");
	$preulcerahosp = anti_injection($preulcerahosp,"literal");
	$prelocodomic = anti_injection($prelocodomic,"literal");
	$prehigidomic = anti_injection($prehigidomic,"literal");
	$preconsdomic = anti_injection($preconsdomic,"literal");
	$prealimdomic = anti_injection($prealimdomic,"literal");
	$preulceradomic = anti_injection($preulceradomic,"literal");
	$objlocomocao = anti_injection($objlocomocao,"literal");
	$objhigiene = anti_injection($objhigiene,"literal");
	$objcons = anti_injection($objcons,"literal");
	$objalimento = anti_injection($objalimento,"literal");
	$objulcera = anti_injection($objulcera,"literal");
	$pre_justificativa = anti_injection($pre_justificativa,"literal");
	$plano_desmame = anti_injection($plano_desmame,"literal");


	$historicointernacao = anti_injection($historicointernacao,"literal");
	$qtdanteriores = anti_injection($qtdanteriores,"numerico");
	$parecer = anti_injection($parecer,"numerico");
	$justparecer = anti_injection($justparecer,"literal");
	$usuario = $_SESSION["id_user"];
	$modalidade = anti_injection($modalidade,"numerico");
	$local_av = anti_injection($local_av,"numerico");

	mysql_query("begin");
	$sql = "INSERT INTO capmedica VALUES (NULL,'{$paciente}',now(),'{$motivo}','{$palergiamed}',
	'{$palergiaalim}','{$alergiamed}','{$alergiaalim}','{$prelocohosp}','{$prehigihosp}',
	'{$preconshosp}','{$prealimhosp}','{$preulcerahosp}','{$prelocodomic}','{$prehigidomic}',
	'{$preconsdomic}','{$prealimdomic}','{$preulceradomic}','{$objlocomocao}','{$objhigiene}',
	'{$objcons}','{$objalimento}','{$objulcera}','{$qtdanteriores}','{$historicointernacao}',
	'{$parecer}','{$justparecer}','{$usuario}','{$modalidade}','{$local_av}','{$pre_justificativa}','{$plano_desmame}')";
	$r = mysql_query($sql);
	if(!$r){
		mysql_query("rollback");
		echo "ERRO: Tente mais tarde!";
		return;
	}
	$idcapmedica = mysql_insert_id();
	savelog(mysql_escape_string(addslashes($sql)));
	$sql = "INSERT INTO comorbidades VALUES ('{$idcapmedica}','{$cancer}','{$psiquiatrico}',
			'{$neurologica}','{$glaucoma}','{$hepatopatia}','{$obesidade}','{$cardiopatia}','{$dm}','{$reumatologica}',
			'{$has}','{$nefropatia}','{$pneumopatia}') ";
	$r = mysql_query($sql);
	if(!$r){
		mysql_query("rollback");
		echo "ERRO: Tente mais tarde!";
		return;
	}
	savelog(mysql_escape_string(addslashes($sql)));
	$problemas = explode("$",$probativos);
	foreach($problemas as $prob){
		if($prob != "" && $prob != null){
			$i = explode("#",$prob);
			$cid = anti_injection($i[0],"literal");
			$desc = anti_injection($i[1],"literal");
		$prob = anti_injection($prob,"literal");
		$sql = "INSERT INTO problemasativos VALUES ('{$idcapmedica}','{$cid}','{$desc}') ";
		$r = mysql_query($sql);
		if(!$r){
			mysql_query("rollback");
			echo "ERRO: Tente mais tarde!";
			return;
		}
		savelog(mysql_escape_string(addslashes($sql)));
	}
	}

	////--score---//
	savelog(mysql_escape_string(addslashes($sql)));
	$score = explode("#",$score);
	foreach($score as $sco){
		if($sco!=''){
			$sco = anti_injection($sco,"numerico");
			$sql = "INSERT INTO score_paciente VALUES (NULL,'{$paciente}','{$usuario}',now(),'{$sco}','{$idcapmedica}','') ";
			$r = mysql_query($sql);
			if(!$r){
				mysql_query("rollback");
				echo "ERRO: Tente mais tarde!1";
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
			}
	}
	////equipamentos
	savelog(mysql_escape_string(addslashes($sql)));

	$equipe = explode("$",$equipamentos);

foreach($equipe as $equip){
		if($equip != "" && $equip != null){
			$i = explode("#",$equip);
			$cod_equip = anti_injection($i[0],"numerico");
			$obs_equip = anti_injection($i[1],"literal");
			$qtd_equip = anti_injection($i[2],"literal");

			$r = mysql_query("INSERT INTO equipamentos_capmedica (ID, COBRANCAPLANOS_ID, CAPMEDICA_ID,QTD,OBSERVACAO) VALUES (null,'{$cod_equip}','{$idcapmedica}','{$qtd_equip}','{$obs_equip}')");
			if(!$r){
				echo "ERRO: ";
                                //.$err." ".mysql_error();
				mysql_query("rollback");
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}

	///cabecario score
	savelog(mysql_escape_string(addslashes($sql)));
	$cab_score = explode("$",$cab_score);

	foreach($cab_score as $cab_sc){
		if($cab_sc != "" && $cab_sc != null){
			$i = explode("#",$cab_sc);
			$cod_item = anti_injection($i[0],"numerico");
			$obs_item = anti_injection($i[1],"literal");
			//$qtd_equip = anti_injection($i[2],"literal");


			$r = mysql_query("INSERT INTO score_paciente VALUES (NULL,'{$paciente}','{$usuario}',now(),'{$cod_item}','{$idcapmedica}','{$obs_item}')");
			if(!$r){
				echo "ERRO: ";
                                //.$err." ".mysql_error();
				mysql_query("rollback");
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}

	$status = 0;
	switch($parecer){
		case "0":
			$status = 1;
		break;
		case "1":
			$status = 2;
		break;
		case "2":
			$status = 2;
		break;
	}
	$sql = "UPDATE clientes SET status = '{$status}' WHERE idClientes = '{$paciente}'";
	$r = mysql_query($sql);
	if(!$r){
		mysql_query("rollback");
		echo "ERRO: Tente mais tarde!";
		return;
	}
	savelog(mysql_escape_string(addslashes($sql)));
         ///2013-11-12 Salvar no histórico de status do paciente.
         $sql= "Insert into historico_status_paciente (USUARIO_ID,PACIENTE_ID,DATA,DATA_ADMISSAO,DATA_AUTORIZACAO,TIPO,OBSERVACAO,STATUS_PACIENTE_ID,CAPMEDICA_ID)
             values ('{$_SESSION['id_user']}','$paciente',now(),'','','-1','','$status','$idcapmedica')";
          if(!$r){
		mysql_query("rollback");
		echo "ERRO: Tente mais tarde!10";
		return;
	}
	savelog(mysql_escape_string(addslashes($sql)));
	mysql_query("commit");
	echo $idcapmedica;
}//nova_capmed

///////////ficha evolucao///////////

function nova_ficha_evolucao($post){
	extract($post,EXTR_OVERWRITE);
	$usuario = $_SESSION["id_user"];
	$paciente         = anti_injection($paciente,"literal");
	$sql ="select nome from clientes where idClientes ={$paciente}";
	$result= mysql_query($sql);
	$nomepaciente       = mysql_result($result,0);
	$capmed             = anti_injection($capmed,"literal");
	$temperatura	    = anti_injection($temperatura,"literal");
	$temperatura_sinal	= anti_injection($temperatura_sinal,"literal");
	$temperatura_max    = anti_injection($temperatura_max,"literal");
	$temperatura_min    = anti_injection($temperatura_min,"literal");

	$oximetria_pulso_max = anti_injection($oximetria_pulso_max,"literal");
	$oximetria_pulso_min = anti_injection($oximetria_pulso_min,"literal");
	$tipo_oximetria_min  = anti_injection($tipo_oximetria_min,"literal");
	$litros_min	    	 = anti_injection($litros_min,"literal");
	$via_oximetria_min	 = anti_injection($via_oximetria_min,"literal");
	$tipo_oximetria_max	 = anti_injection($tipo_oximetria_max,"literal");
	$litros_max			 = anti_injection($litros_max,"literal");
	$via_oximetria_max   = anti_injection($via_oximetria_max,"literal");

	$pa_diastolica      = anti_injection($pa_diastolica,"literal");
	$pa_diastolica_sinal= anti_injection($pa_diastolica_sinal,"literal");
	$pa_diastolica_min	= anti_injection($pa_diastolica_min,"literal");
	$pa_diastolica_max	= anti_injection($pa_diastolica_max,"literal");
	$pa_sistolica       = anti_injection($pa_sistolica,"literal");
	$pa_sistolica_sinal	= anti_injection($pa_sistolica_sinal,"literal");
	$pa_sistolica_max	= anti_injection($pa_sistolica_max,"literal");
	$pa_sistolica_min	= anti_injection($pa_sistolica_min,"literal");
	$fc                 = anti_injection($fc,"literal");
	$fc_sinal           = anti_injection($fc_sinal,"literal");

	$fc_max             = anti_injection($fc_max,"literal");
	$fc_min             = anti_injection($fc_min,"literal");
	$fr                 = anti_injection($fr,"literal");
	$fr_sinal           = anti_injection($fr_sinal,"literal");
	$fr_max             = anti_injection($fr_max,"literal");
	$fr_min             = anti_injection($fr_min,"literal");
	$estd_geral         = anti_injection($estd_geral,"literal");
	$outro_respiratorio = anti_injection($outro_respiratorio,"literal");

	if($fc_sinal == 1){
		$fc_msg = 'Alerta Amarelo devido ao FC.';
	}
	if($fc_sinal == 2){
		$fc_msg = 'Alerta Vermelho devido ao FC.';
	}

	if($fr_sinal == 1){
		$fr_msg = 'Alerta Amarelo devido ao FR.';
	}
	if($fc_sinal == 2){
		$fr_msg = 'Alerta Vermelho devido ao FR.';
	}

	if($pa_sistolica_sinal == 1){
		$pa_sistolica_msg = 'Alerta Amarelo devido a Pa Sist&oacute;lica.';
	}
	if($pa_sistolica_sinal == 2){
		$pa_sistolica_msg = 'Alerta Vermelho devido a Pa Sist&oacute;lica..';
	}

    if($pa_diastolica_sinal == 1){
		$pa_diastolica_msg = 'Alerta Amarelo devido a Pa Diast&oacute;lica.';
	}
	if($pa_diastolica_sinal == 2){
		$pa_diastolica_msg = 'Alerta Vermelho devido a Pa diast&oacute;lica.';
	}

	if($temperatura_sinal == 1){
		$temperatura_msg = 'Alerta Amarelo devido a Temperatura.';
	}
	if($temperatura_sinal == 2){
		$temperatura_msg = 'Alerta Vermelho devido a Temperatura.';
	}

	$dor_sn = anti_injection($dor_sn,"literal");
	$mucosa       = anti_injection($mucosa,"literal");
	$impressao    = anti_injection($impressao,"literal");
	$mucosaobs    = anti_injection($mucosaobs,"literal");
	$novo_exame   = anti_injection($novo_exame,"literal");
	$esclerasobs  = anti_injection($esclerasobs,"literal");
	$escleras  = anti_injection($escleras,"literal");
	$plano_diagnostico	= anti_injection($plano_diagnostico,"literal");
	$respiratorioobs = anti_injection($respiratorioobs,"literal");
	$respiratorio = anti_injection($respiratorio,"literal");
	$terapeutico = anti_injection($terapeutico,"literal");

	mysql_query("begin");
	$sql = "INSERT INTO fichamedicaevolucao VALUES (NULL,'{$usuario}','{$capmed}','{$paciente}',now(),'{$pa_sistolica_min}','{$pa_sistolica_max}',
	'{$pa_diastolica_min}','{$pa_diastolica_max}','{$fc_min}','{$fc_max}','{$fr_min}',
	'{$fr_max}','{$temperatura_min}','{$temperatura_max}','{$pa_sistolica_sinal}','{$pa_diastolica_sinal}','{$fc_sinal}','{$fr_sinal}',
	'{$temperatura_sinal}','{$estd_geral}','{$mucosa}','{$mucosaobs}','{$escleras}',
	'{$esclerasobs}','{$respiratorio}','{$respiratorioobs}','{$pa_sistolica}','{$pa_diastolica}','{$fc}','{$fr}','{$temperatura}',
	'{$novo_exame}','{$impressao}','{$plano_diagnostico}','{$terapeutico}','{$dor_sn}','{$outro_respiratorio}','', '{$oximetria_pulso_max}', 
	'{$oximetria_pulso_min}', '{$tipo_oximetria_min}', '{$litros_min}', '{$via_oximetria_min}', '{$tipo_oximetria_max}', '{$litros_max}', '{$via_oximetria_max}')";

	$r = mysql_query($sql);
	if(!$r){
		mysql_query("rollback");
		//echo "ERRO1: Tente mais tarde!";
		echo "ERRO1: ";
		return;
	}
	$idfichaevolucao = mysql_insert_id();
	savelog(mysql_escape_string(addslashes($sql)));
	///problemas ativos
	$problemas = explode("$",$probativos);

	foreach($problemas as $prob){
		if($prob != "" && $prob != null){
			$i      = explode("#",$prob);
			$cid    = anti_injection($i[0],"literal");
			$desc   = anti_injection($i[1],"literal");
			$status = anti_injection($i[2],"numerico");
			$obs    = anti_injection($i[3],"literal");

			$sql = "INSERT INTO problemaativoevolucao VALUES (NULL,'{$idfichaevolucao}','{$cid}','{$desc}','{$status}','{$obs}',now()) ";
			$r = mysql_query($sql);
			if(!$r){
				mysql_query("rollback");
				echo "ERRO2: Tente mais tarde!";
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}
   ///problemas ativos fim
	savelog(mysql_escape_string(addslashes($sql)));
	///problemas novos
	$problemasnovos = explode("$",$novosprobativos);
	foreach($problemasnovos as $prob1){
		if($prob1 != "" && $prob1 != null){
			$i = explode("#",$prob1);
			$cid = anti_injection($i[0],"literal");
			$desc = anti_injection($i[1],"literal");
			$obs = anti_injection($i[2],"literal");

			$sql = "INSERT INTO problemaativoevolucao VALUES (NULL,'{$idfichaevolucao}','{$cid}','{$desc}','0','{$obs}',now()) ";
			$r = mysql_query($sql);
			if(!$r){
				mysql_query("rollback");
				echo "ERRO3: Tente mais tarde!";
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}
	///fim problemas novos

	savelog(mysql_escape_string(addslashes($sql)));
	///exames segmentar evolucao
	$exame = explode("$",$exames);
	foreach($exame as $ex){
		if($ex != "" && $ex != null){
			$i = explode("#",$ex);
			$id = anti_injection($i[0],"literal");
			$val = anti_injection($i[1],"numerico");
			$obs = anti_injection($i[2],"literal");


			$sql = "INSERT INTO examesegmentarevolucao VALUES (NULL,'{$idfichaevolucao}','{$id}','{$val}','{$obs}') ";
			$r = mysql_query($sql);
			if(!$r){
				mysql_query("rollback");
				echo "ERRO4: Tente mais tarde!";
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}
    ////exames segmentar evolucao

	savelog(mysql_escape_string(addslashes($sql)));
	///dor
	$dor = explode("$",$dor123);
	foreach($dor as $d){
		if($d != "" && $d != null){
			$i = explode("#",$d);
			$nome = anti_injection($i[0],"literal");
			$escala = anti_injection($i[1],"numerico");
			$padrao = anti_injection($i[2],"literal");


			$sql = "INSERT INTO dorfichaevolucao VALUES (NULL,'{$nome}','{$escala}','{$padrao}','{$idfichaevolucao}',now()) ";
			$r = mysql_query($sql);
			if(!$r){
				mysql_query("rollback");
				echo "ERRO5: Tente mais tarde!";
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}

	////fim dor

	//Intercorrências

	$intercorrencia = explode("#", $intercorrencias);
	foreach ($intercorrencia as $interc){
		if($interc != "" && $interc !=null){

			$descricao = anti_injection($interc, "literal");

            $sql = "INSERT INTO intercorrenciasfichaevolucao (`fichamedicaevolucao_id`,`descricao`) VALUES ('{$idfichaevolucao}','{$descricao}')";
			$r = mysql_query($sql);
			if(!$r){

				echo "ERRO: ";
				mysql_query("rollback");
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}

	savelog(mysql_escape_string(addslashes($sql)));
	$cat_sond = explode("#",$cat_sond);

	foreach($cat_sond  as $cat){
		if($cat != "" ){
			$nome = anti_injection($cat,"literal");
			$sql = "INSERT INTO catetersondaevolucao VALUES (NULL,'{$idfichaevolucao}','{$nome}') ";
			$r = mysql_query($sql);
			if(!$r){
				mysql_query("rollback");
				echo "ERRO5: Tente mais tarde!";
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}
	if($pa_sistolica_msg != '' || $pa_diastolica_msg != '' || $fc_msg != '' || $fr_msg !='' || $temperatura_msg != ''){
		enviar_email_alerta($pa_sistolica_msg,$pa_diastolica_msg,$fc_msg,$fr_msg,$temperatura_msg,$nomepaciente);

	}
	mysql_query("commit");


}

function enviar_email_alerta($pa_sistolica_msg,$pa_diastolica_msg,$fc_msg,$fr_msg,$temperatura_msg,$nomepaciente){

	$pa_sistolica_msg= $pa_sistolica_msg;
	$paciente= $paciente;
	$pa_diastolica_msg = $pa_diastolica_msg;
	$fc_msg= $fc_msg;
	$fr_msg= $fr_msg;
	$temperatura_msg = $temperatura_msg;


	$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
	$mail->IsSMTP(); // telling the class to use SMTP

	try {
		// $mail->Host       = "mail.yourdomain.com"; // SMTP server
		//	  $mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->SMTPSecure = "TLS";                 // sets the prefix to the servier
		$mail->Host       = "smtp.mederi.com.br";      // sets GMAIL as the SMTP server
		$mail->Port       = 587;                   // set the SMTP port for the GMAIL server
		$mail->Username   = "avisos@mederi.com.br";  // GMAIL username
		$mail->Password   = "2017avisosistema";            // GMAIL password
		//	  $mail->AddReplyTo('name@yourdomain.com', 'First Last');
		$mail->AddAddress('ti@mederi.com.br', 'Avisos Médicos Mederi');
		$mail->SetFrom('avisos@mederi.com.br', 'Avisos de Sistema');
		//	  $mail->AddReplyTo('name@yourdomain.com', 'First Last');
		$mail->Subject = 'AVISO';
		$mail->AltBody = 'Para ver essa mensagem, use um visualizador de email compatível com HTML'; // optional - MsgHTML will create an alternate automatically
		$solicitante = ucwords(strtolower($_SESSION["nome_user"]));
		$mail->MsgHTML("<b>Aviso de alerta de visita</b> <b>Paciente: </b>{$nomepaciente}<br/><b>Solicitante: </b>{$solicitante}.</br> {$pa_sistolica_msg} {$pa_diastolica_msg} {$fc_msg} {$fr_msg} {$temperatura_msg}");
		//	  $mail->AddAttachment('images/phpmailer.gif');      // attachment
		//	  $mail->AddAttachment('images/phpmailer_mini.gif'); // attachment
		$enviado=$mail->Send();

		/*if ($enviado) {
			echo "E-mail enviado com sucesso!";
			window.location.href='?adm=paciente&act=listfichaevolucao&id='{$paciente};
		} else {
			echo "N�o foi poss�vel enviar o e-mail.<br /><br />";
			echo "<b>Informa��es do erro:</b> <br />" . $mail->ErrorInfo;
		}*/
	}
	catch (phpmailerException $e) {
		echo $e->errorMessage(); //Pretty error messages from PHPMailer
	} catch (Exception $e) {
		echo $e->getMessage(); //Boring error messages from anything else!
	}
     header("LOCATION='?adm=paciente&act=listfichaevolucao&id='{$paciente}");
}
//////////////fim ficha evolucao
function editar_ficha_avaliacao($post){

	extract($post,EXTR_OVERWRITE);
	$paciente = anti_injection($paciente,"numerico");
	$motivo = anti_injection($motivo,"literal");
	$capmedicaid = anti_injection($capmedicaid,"numerico");
	$cancer = anti_injection($cancer,"literal");
	$psiquiatrico = anti_injection($psiquiatrico,"literal");
	$neurologica = anti_injection($neurologica,"literal");
	$galucoma = anti_injection($glaucoma,"literal");
	$hepatopatia = anti_injection($hepatopatia,"literal");
	$obesidade = anti_injection($obesidade,"literal");
	$cardiopatia = anti_injection($cardiopatia,"literal");
	$dm = anti_injection($dm,"literal");
	$reumatologica = anti_injection($reumatologica,"literal");
	$has = anti_injection($has,"literal");
	$nefropatia = anti_injection($nefropatia,"literal");
	$pneumopatia = anti_injection($pneumopatia,"literal");

	$palergiamed = anti_injection($palergiamed,"literal");
	$alergiamed = anti_injection($alergiamed,"literal");
	$palergiaalim = anti_injection($palergiaalim,"literal");
	$alergiaalim = anti_injection($alergiaalim,"literal");

	$prelocohosp = anti_injection($prelocohosp,"literal");
	$prehigihosp = anti_injection($prehigihosp,"literal");
	$preconshosp = anti_injection($preconshosp,"literal");
	$prealimhosp = anti_injection($prealimhosp,"literal");
	$preulcerahosp = anti_injection($preulcerahosp,"literal");
	$prelocodomic = anti_injection($prelocodomic,"literal");
	$prehigidomic = anti_injection($prehigidomic,"literal");
	$preconsdomic = anti_injection($preconsdomic,"literal");
	$prealimdomic = anti_injection($prealimdomic,"literal");
	$preulceradomic = anti_injection($preulceradomic,"literal");
	$objlocomocao = anti_injection($objlocomocao,"literal");
	$objhigiene = anti_injection($objhigiene,"literal");
	$objcons = anti_injection($objcons,"literal");
	$objalimento = anti_injection($objalimento,"literal");
	$objulcera = anti_injection($objulcera,"literal");

	$historicointernacao = anti_injection($historicointernacao,"literal");
	$qtdanteriores = anti_injection($qtdanteriores,"numerico");
	$parecer = anti_injection($parecer,"numerico");
	$justparecer = anti_injection($justparecer,"literal");
	$usuario = $_SESSION["id_user"];
	$modalidade = anti_injection($modalidade,"numerico");
	$local_av = anti_injection($local_av,"numerico");
	$pre_justificativa = anti_injection($pre_justificativa,"literal");
	$plano_desmame = anti_injection($plano_desmame,"literal");

	mysql_query("begin");
	$sql = "UPDATE capmedica SET paciente = '{$paciente}', data = now(), motivo = '{$motivo}', alergiamed = '{$palergiamed}',
	alergiaalim = '{$palergiaalim}', tipoalergiamed = '{$alergiamed}', tipoalergiaalim = '{$alergiaalim}', hosplocomocao = '{$prelocohosp}',hosphigiene = '{$prehigihosp}',
	hospcons = '{$preconshosp}', hospalimentacao = '{$prealimhosp}', hospulcera = '{$preulcerahosp}', domlocomocao = '{$prelocodomic}', domhigiene = '{$prehigidomic}',
	domcons = '{$preconsdomic}', domalimentacao = '{$prealimdomic}', domulcera = '{$preulceradomic}',  objlocomocao = '{$objlocomocao}', objhigiene = '{$objhigiene}',
	objcons = '{$objcons}', objalimentacao = '{$objalimento}', objulcera = '{$objulcera}', qtdinternacoes = '{$qtdanteriores}', historicointernacao = '{$historicointernacao}',
	parecer = '{$parecer}', justificativa = '{$justparecer}', usuario = '{$usuario}', MODALIDADE = '{$modalidade}', LOCAL_AV= '{$local_av}', PRE_JUSTIFICATIVA= '{$pre_justificativa}', PLANO_DESMAME= '{$plano_desmame}' WHERE id = '{$capmedicaid}'";
	$r = mysql_query($sql);
	if(!$r){
		mysql_query("rollback");
		echo "ERRO1: Tente mais tarde! '{$capmedicaid}'";
		return;
	}

	savelog(mysql_escape_string(addslashes($sql)));
	$sql = "UPDATE comorbidades SET cancer='{$cancer}',psiquiatrico='{$psiquiatrico}',
	neuro='{$neurologica}',glaucoma='{$glaucoma}',hepatopatia='{$hepatopatia}',obesidade='{$obesidade}',cardiopatia='{$cardiopatia}',dm='{$dm}',reumatologica='{$reumatologica}',
	has='{$has}',nefropatia='{$nefropatia}',pneumopatia='{$pneumopatia}' where capmed='{$capmedicaid}'";
	$r = mysql_query($sql);
	if(!$r){
		mysql_query("rollback");
		echo "ERRO: Tente mais tarde!2";
		return;
	}
	savelog(mysql_escape_string(addslashes($sql)));
	$sql = "Delete FROM problemasativos where capmed='{$capmedicaid}' ";
	$r = mysql_query($sql);
	if(!$r){
		mysql_query("rollback");
		echo "ERRO: Tente mais tarde!3";
		return;
	}
	savelog(mysql_escape_string(addslashes($sql)));
	$problemas = explode("$",$probativos);
foreach($problemas as $prob){
		if($prob != "" && $prob != null){
			$i = explode("#",$prob);
			$cid = anti_injection($i[0],"literal");
			$desc = anti_injection($i[1],"literal");

		$sql = "INSERT INTO problemasativos VALUES ('{$capmedicaid}','{$cid}','{$desc}') ";
		$r = mysql_query($sql);
		if(!$r){
			mysql_query("rollback");
			echo "ERRO: Tente mais tarde!";
			return;
		}
		savelog(mysql_escape_string(addslashes($sql)));
	}
	}
	$sql = "Delete FROM score_paciente where ID_CAPMEDICA='{$capmedicaid}' ";
	$r = mysql_query($sql);
	if(!$r){
		mysql_query("rollback");
		echo "ERRO: Tente mais tarde!5";
		return;
	}
	savelog(mysql_escape_string(addslashes($sql)));

	////--score---//
	savelog(mysql_escape_string(addslashes($sql)));
	$score = explode("#",$score);
	foreach($score as $sco){
		if($sco!=''){
			$sco = anti_injection($sco,"numerico");
			$sql = "INSERT INTO score_paciente VALUES (NULL,'{$paciente}','{$usuario}',now(),'{$sco}','{$capmedicaid}','') ";
			$r = mysql_query($sql);
			if(!$r){
				mysql_query("rollback");
				echo "ERRO: Tente mais tarde!6";
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}

	$sql = "Delete FROM equipamentos_capmedica where CAPMEDICA_ID='{$capmedicaid}' ";
	$r = mysql_query($sql);
	if(!$r){
		mysql_query("rollback");
		echo "ERRO: Tente mais tarde!7";
		return;
	}
	savelog(mysql_escape_string(addslashes($sql)));
	////equipamentos
	savelog(mysql_escape_string(addslashes($sql)));
	$equipe = explode("$",$equipamentos);

	foreach($equipe as $equip){
		if($equip != "" && $equip != null){
			$i = explode("#",$equip);
			$cod_equip = anti_injection($i[0],"numerico");
			$obs_equip = anti_injection($i[1],"literal");
			$qtd_equip = anti_injection($i[2],"literal");

			$r = mysql_query("INSERT INTO equipamentos_capmedica (ID,COBRANCAPLANOS_ID,CAPMEDICA_ID,QTD,OBSERVACAO) VALUES (null,'{$cod_equip}','{$capmedicaid}','{$qtd_equip}','{$obs_equip}')");
			if(!$r){
				echo "ERRO: ";
                                //.$err." ".mysql_error();
				mysql_query("rollback");
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}


	/*$sql = "Delete FROM score_paciente where ID_CAPMEDICA='{$capmedicaid}' ";
	$r = mysql_query($sql);
	if(!$r){
		mysql_query("rollback");
		echo "ERRO: Tente mais tarde!8";
		return;
	}
	savelog(mysql_escape_string(addslashes($sql)));
	///cabecario score
	savelog(mysql_escape_string(addslashes($sql)));*/
	$cab_score = explode("$",$cab_score);

	foreach($cab_score as $cab_sc){
		if($cab_sc != "" && $cab_sc != null){
			$i = explode("#",$cab_sc);
			$cod_item = anti_injection($i[0],"numerico");
			$obs_item = anti_injection($i[1],"literal");
			//$qtd_equip = anti_injection($i[2],"literal");


			$r = mysql_query("INSERT INTO score_paciente VALUES (NULL,'{$paciente}','{$usuario}',now(),'{$cod_item}','{$capmedicaid}','{$obs_item}')");
			if(!$r){
				echo "ERRO: ";
                                //.$err." ".mysql_error();
				mysql_query("rollback");
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}

	$status = 0;
	switch($parecer){
		case "0":
			$status = 1;
			break;
		case "1":
			$status = 2;
			break;
		case "2":
			$status = 2;
			break;
	}
	$sql = "UPDATE clientes SET status = '{$status}' WHERE idClientes = '{$paciente}'";
	$r = mysql_query($sql);
	if(!$r){
		mysql_query("rollback");
		echo "ERRO: Tente mais tarde!10";
		return;
	}
	savelog(mysql_escape_string(addslashes($sql)));
        ///2013-11-12 Salvar no histórico de status do paciente.
         $sql= "Insert into historico_status_paciente (USUARIO_ID,PACIENTE_ID,DATA,DATA_ADMISSAO,DATA_AUTORIZACAO,TIPO,OBSERVACAO,STATUS_PACIENTE_ID,CAPMEDICA_ID)
             values ('{$_SESSION['id_user']}','$paciente',now(),'','','-1','','$status','$capmedicaid')";
          if(!$r){
		mysql_query("rollback");
		echo "ERRO: Tente mais tarde!10";
		return;
	}
	savelog(mysql_escape_string(addslashes($sql)));
	mysql_query("commit");
	echo $capmedicaid;


}


function show_status_paciente($post){
    extract($post,EXTR_OVERWRITE);
    $cod = anti_injection($cod,'numerico');
    //////////Ver qual a maior capmedica do paciente e ver se em histórico existe alguma data de admissão já gravada para a mesma.

    $sql_capmedica= "SELECT max( id ) AS maior_cap FROM capmedica WHERE paciente ='$cod' ";
    $result = mysql_query($sql_capmedica);
    $row_cap = mysql_fetch_array($result);
    $maior_cap=$row_cap['maior_cap'];
    if($maior_cap >0){
        $sql_historico = "Select DATE_FORMAT(DATA_AUTORIZACAO, '%d/%m/%Y') as data_au, DATE_FORMAT( DATA_AUTORIZACAO, '%H:%i:%s' ) AS hora_au,DATE_FORMAT(DATA_ADMISSAO, '%d/%m/%Y') as data_ad, DATE_FORMAT( DATA_ADMISSAO, '%H:%i:%s' ) AS hora_ad, CAPMEDICA_ID,ID from historico_status_paciente where PACIENTE_ID = '$cod' and STATUS_PACIENTE_ID=4 order by ID desc limit 1";
        $row_hist =mysql_fetch_array (mysql_query($sql_historico));
        $id_hist= $row_hist['ID'];
        $cap_id_hist=$row_hist['CAPMEDICA_ID'];
        $data_admissao_hist= $row_hist['data_ad'];
        $hora_admissao_hist= $row_hist['hora_ad'];
        $data_autorizacao_hist= $row_hist['data_au'];
        $hora_autorizacao_hist= $row_hist['hora_au'];
    }
    if ($maior_cap == $cap_id_hist){
        $data_ad=$data_admissao_hist;
        $hora_ad=$hora_admissao_hist;
        $data_au=$data_autorizacao_hist;
        $hora_au=$hora_autorizacao_hist;
        $id_hist=$id_hist;
    }else{
        $data='';
        $hora='';
        $data_au='';
        $hora_au='';
        $id_hist=0;
    }

    $row = mysql_fetch_array ( mysql_query("SELECT nome, status FROM `clientes` WHERE `idClientes` = '{$cod}' "));
    $s = $row['status'];
    $p = ucwords(strtolower($row['nome']));
    $var = "<p><b>Status do paciente {$p}: </b><br />";
    $r = mysql_query("SELECT * FROM statuspaciente");
    while($row = mysql_fetch_array($r)){
        $var .= "<input type='radio' class='status-paciente' name='status' s='{$row['id']}'  ".(($row['id']=="{$s}")?"checked":"")." />{$row['status']}<br/>";
    }
    $var.="<p><span id='span-tipo' style='display:none;'><b>Tipo:</b>
            <select name='tipo' id='status-tipo'>
            <option value='-1' selected>Selecione</option>
            <option value='1'>Admiss&atilde;o</option>
            <option value='2'>Reinterna&ccedil;&atilde;o</option>
            </select></span></p>";
    $var.="<p><span id='span-autorizacao' style='display:none;'><b>Autoriza&ccedil;&atilde;o Data: </b><input type='text' class='data' id='data-autorizacao' name='data-autorizacao' value='{$data_au}' /><b>Hora: </b><input type='text'  class='hora' value='{$hora_au}' id='hora-autorizacao' name='hora-autorizacao'/></span></p>";
    $var.="<p><span id='span-admissao' style='display:none;'><b>Admiss&atilde;o Data: </b><input type='text' class='data' id='data-admissao' name='data-admissao' value='{$data_ad}'/><b>Hora: </b><input type='text'  class='hora'  id='hora-admissao' name='hora-admissao' value='{$hora_ad}'/></span></p>";
    $var.="<p><span id='span-observacao' style='display:none;'><b>Observa&ccedil;&atilde;o:</b><br/><textarea rows='3' cols='60' id='status-observacao'></textarea></span></p>";
    $var.="<p><input type='hidden' value='{$id_hist}'  name='id_historico_status' id='id_historico_status' /></p>";
    $var.="<p><input type='hidden' value='{$maior_cap}'  name='capmedica_id' id='capmedica_id' /></p>";

    $rsH = mysql_query("SELECT ID, DATA_AUTORIZACAO, DATA_ADMISSAO FROM historico_status_paciente WHERE PACIENTE_ID = '{$cod}' ORDER BY ID DESC LIMIT 1");
    $countH = mysql_num_rows($rsH);
    if($countH > 0) { //249
        $rowH = mysql_fetch_array($rsH);
        $ultimaDataAutorizacao = $rowH['DATA_AUTORIZACAO'] != '0000-00-00 00:00:00' ? \DateTime::createFromFormat('Y-m-d H:i:s', $rowH['DATA_AUTORIZACAO']) : new \DateTime();
        $ultimaDataAdmissao = $rowH['DATA_ADMISSAO'] != '0000-00-00 00:00:00' ? \DateTime::createFromFormat('Y-m-d H:i:s', $rowH['DATA_ADMISSAO']) : new \DateTime();

        $var .= "   <hr>
                    <p>
                        <b>Última alteração de status desse paciente:</b>
                        <br>
                        <small><b>* Se ambas as datas estiverem com a data e o horário atual, significa que o paciente não possui histórico!</b></small>
                        <br>
                        <b>Última Autorização: </b><input type='text' name='ultima_data_autorizacao' class='data ultima-data-autorizacao' maxlength='14' size='14' value='" . $ultimaDataAutorizacao->format('d/m/Y') . "'> - <input type='text' name='ultima_hora_autorizacao' class='hora ultima-hora-autorizacao' value='" . $ultimaDataAutorizacao->format('H:i:s') . "'>
                        <br>
                        <b>Última Admissão: </b><input type='text' name='ultima_data_admissao' class='data ultima-data-admissao' maxlength='14' size='14' value='" . $ultimaDataAdmissao->format('d/m/Y') . "'> - <input type='text' name='ultima_hora_admissao' class='hora ultima-hora-admissao' value='" . $ultimaDataAdmissao->format('H:i:s') . "'>
                        <input type='hidden' value='{$rowH['ID']}' name='historico_id' class='historico-id' />
                        <br>
                        <button type='button' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover atualizar-ultima-alteracao'>
                            <span class='ui-button-text'>Atualizar Autorização e Admissão</span>
                        </button>
                    </p>";
    }

    echo $var;
}

function show_modalidade_paciente($post){
	extract($post,EXTR_OVERWRITE);
	$cod = anti_injection($cod,'numerico');
	//////////Ver qual a maior capmedica do paciente e ver se em histórico existe alguma data de admissão já gravada para a mesma.

	$sql_modalidade = "SELECT max( id ) AS maior_mod FROM historico_modalidade_paciente WHERE paciente ='$cod' ";
	$result = mysql_query($sql_modalidade);
	$row_mod = mysql_fetch_array($result);
	$maior_mod = $row_mod['maior_mod'];

	$row = mysql_fetch_array ( mysql_query("SELECT nome FROM `clientes` WHERE `idClientes` = '{$cod}' "));
	$p = ucwords(strtolower($row['nome']));
	$var = "<p><b>Modalidade do paciente " . htmlentities($p) . ": </b><br />";
	$r = mysql_query("SELECT * FROM modalidade_home_care");
	while($row = mysql_fetch_array($r)){
		$var .= "<input type='radio' class='modalidade-paciente OBG_RADIO' name='modalidade' m='{$row['id']}'  ". ($row['id'] == $maior_mod ? "checked" : '' ). " /> {$row['modalidade']} <br/>";
	}
	$var .= '<br/> Data da Mudança: <input type="date" class="OBG" name="data_mudanca" id="data_mudanca" />';

	echo $var;
}
function salvar_status_paciente($post){
	 extract($post,EXTR_OVERWRITE);
	 $cod = anti_injection($cod,'numerico');
	 $pstatus = anti_injection($pstatus,'numerico');
         $tipo = anti_injection($tipo,'numerico');
         $observacao= anti_injection($observacao,'literal');
         $data_autorizacao=anti_injection($data_autorizacao,'literal');
         $data_admissao=anti_injection($data_admissao,'literal');
         $hora_autorizacao=anti_injection($hora_autorizacao,'literal');
         $hora_admissao=anti_injection($hora_admissao,'literal');
         $data_autorizacao=implode("-",array_reverse(explode("/",$data_autorizacao)));
         $data_admissao=implode("-",array_reverse(explode("/",$data_admissao)));
         $data_admissao="{$data_admissao} {$hora_admissao}";
         $data_autorizacao="{$data_autorizacao} {$hora_autorizacao}";
         $capmedica_id = anti_injection($capmedica_id,'literal');
         $id_historico_ativo=0;
    $ur = anti_injection($ur,'numerico');
    $row = mysql_fetch_array ( mysql_query("SELECT nome, status, empresa FROM `clientes` WHERE `idClientes` = '{$cod}' "));
    $statusAnterior = $row['status'];
    $pacienteNome = $row['nome'];
    $empresa = $row['empresa'];




         mysql_query('begin');
	$sql = "UPDATE clientes SET status = '{$pstatus}' WHERE idClientes = '{$cod}' ";
	$r = mysql_query($sql);
	if(!$r){
		echo "ERRO: tente mais tarde1!";
                mysql_query("rollback");
		return;
	}
        savelog(mysql_escape_string(addslashes($sql)));

	  $sql= "Insert into historico_status_paciente (USUARIO_ID,PACIENTE_ID,DATA,DATA_ADMISSAO,DATA_AUTORIZACAO,TIPO,OBSERVACAO,STATUS_PACIENTE_ID,CAPMEDICA_ID)
             values ('{$_SESSION['id_user']}','$cod',now(),'$data_admissao','$data_autorizacao','$tipo','$observacao','$pstatus','$capmedica_id')";
               $r = mysql_query($sql);

	if(!$r){
		echo "ERRO: ao inserir na tabela de historico, tente mais tarde!";
                mysql_query("rollback");
		return;
	}
	$id_historico_ativo=  mysql_insert_id();

	savelog(mysql_escape_string(addslashes($sql)));

    $sql = "select solicitacao_recolhimento_equipamento, status from statuspaciente where id = {$pstatus}";
    $r = mysql_query($sql);
    if(!$r){
        echo "ERRO: Ao tentar verificar status!";
        mysql_query("rollback");
        return;
    }

    $geraSolicitacaoRecolhimento = mysql_result($r,0,0);
    $statusLabel =  mysql_result($r,0,1);


	if($geraSolicitacaoRecolhimento == 'S') {

		$sql_equip_ativo = <<<SQL
	SELECT
		COBRANCA_PLANOS_ID,
		QTD,
		cobrancaplanos.item,
		clientes.nome
	FROM
		equipamentosativos
	INNER JOIN cobrancaplanos ON (equipamentosativos.COBRANCA_PLANOS_ID = cobrancaplanos.id)
	INNER JOIN clientes ON equipamentosativos.PACIENTE_ID = clientes.idClientes
	WHERE
		`DATA_FIM` = '0000-00-00'
		AND `PACIENTE_ID` = '{$cod}'
		AND `DATA_INICIO` <> '0000-00-00'
SQL;

		$rs_equip_ativo = mysql_query ($sql_equip_ativo);
		$num_equip_ativo = mysql_num_rows ($rs_equip_ativo);

		if ($num_equip_ativo > 0) {
			$add_one_day = new \DateInterval('P1D');
			$data_atual = new DateTime();
			$data_max_recolher =  $data_atual->add($add_one_day)->format('Y-m-d H:i:s');
			$sql_solicitar_recol = <<<SQL
	INSERT INTO solicitacoes
	(
		`idSolicitacoes`,
		`paciente`,
		`enfermeiro`,
		`obs`,
		`NUMERO_TISS`,
		`CATALOGO_ID`,
		`qtd`,
		`autorizado`,
		`data_auditado`,
		`tipo`,
		`status`,
		`data`,
		`idPrescricao`,
		`TIPO_SOL_EQUIPAMENTO`,
		`TIPO_SOLICITACAO`,
		`DATA_MAX_ENTREGA`
	)
	VALUES
SQL;
            $status_nome = $statusLabel;

			$itens = "";
			while ($row = mysql_fetch_array ($rs_equip_ativo, MYSQL_ASSOC)) {
				$sql_solicitar_recol .= <<<SQL
	(
		'',
		'{$cod}',
		'{$_SESSION['id_user']}',
		'Solicitação realizada pelo sistema devido a troca de status do paciente!',
		'{$row['COBRANCA_PLANOS_ID']}',
		'{$row['COBRANCA_PLANOS_ID']}',
		'{$row['QTD']}',
		'{$row['QTD']}',
		NOW(),
		'5',
		'1',
		NOW(),
		'-1',
		'2',
		'-2',
		'{$data_max_recolher}'
	),
SQL;
				$itens .= "<li>{$row['item']}</li>";
				$paciente_nome = $row['nome'];

			}
			$msg = sprintf(
				"O paciente <b>%s</b> teve seu status alterado para <b>%s</b>.<br>
				 Os seguintes equipamentos ativos serão recolhidos: <br>
				 <ul>%s</ul>",
				$paciente_nome,
				$status_nome,
				$itens
			);
            enviarEmailCoordenadoria('enfermagem', $ur, $msg);
			$rs_solicitar_recol = mysql_query (substr_replace($sql_solicitar_recol, '', -1));
		}
	}

	mysql_query('commit');
    if($statusAnterior != 4 && $pstatus == 4){
        $agora = date('d/m/Y H:i');

        $configEmail = require $_SERVER['DOCUMENT_ROOT'] . '/app/configs/email.php';
        if(isset($configEmail['lista']['emails']['secretaria'][$empresa])) {
            $para[] = $configEmail['lista']['emails']['secretaria'][$empresa];
        }
        $para[] = $configEmail['lista']['emails']['enfermagem'][$empresa];
        $para[] = $configEmail['lista']['emails']['medico'][$empresa];
        $msg = "O paciente <b>{$pacienteNome}</b> teve seu status alterado para <b>Ativo</b>.<br>
                Em {$agora} por {$_SESSION['nome_user']}.";
        $titulo = "Aviso do Sistema da Mederi. Mudança de status do paciente {$pacienteNome}.";
        SendGrid::enviarEmailSimples($titulo,$msg,$para);
    }
	if($pstatus == 4 && $statusAnterior != 4){
		echo $id_historico_ativo;
	}else {
		echo 1;


	}
}

function salvar_modalidade_paciente($post){
	extract($post,EXTR_OVERWRITE);

	$paciente = anti_injection($paciente, 'numerico');
	$modalidade = anti_injection($modalidade,'numerico');

	$sql= "INSERT INTO historico_modalidade_paciente (paciente, modalidade, data_mudanca, data_sistema, usuario)
	 				VALUES ('$paciente', '$modalidade', '$data_mudanca', NOW(), '{$_SESSION['id_user']}')";
	$r = mysql_query($sql);

	if(!$r){
		echo "ERRO: ao inserir na tabela de historico, tente mais tarde!";
		mysql_query("rollback");
		return;
	}

	savelog(mysql_escape_string(addslashes($sql)));
	if(mysql_query('commit'))
		echo 1;
}

/////liberar prescricao avaliacao

function liberar($id){
	mysql_query("begin");
	$user = $_SESSION["id_user"];
	$sql= "UPDATE prescricoes set LIBERADO ='S', LIBERADO_POR={$user},DATA_LIBERADO = now() where id = '{$id}'";
$r = mysql_query($sql);
	if(!$r){
		echo "ERRO: tente mais tarde!";
		mysql_query("rollback");
		return;
	}
	savelog(mysql_escape_string(addslashes($sql)));
	mysql_query("commit");
	echo "1";

}

function desativar($id){
	mysql_query("begin");
	$user = $_SESSION["id_user"];
	$sql= "UPDATE prescricoes set lockp='9999-12-31 23:59:59', DESATIVADA_POR = '{$user}', DATA_DESATIVADA = now(), STATUS = 1 where id = '{$id}'";
$r = mysql_query($sql);
	if(!$r){
		echo "ERRO: tente mais tarde!";

		return;
		mysql_query("rollback");
	}
	savelog(mysql_escape_string(addslashes($sql)));
	mysql_query("commit");
	echo "1";

}


/////liberar prescricao avaliacao fim

////importar assinatura 27-06-2013
function assinatura($id_user,$file){
	if($file['tmp_name']<>''){

		$foto_temp = $file['tmp_name']; //caminho temporario do arquivo
		$foto_nome = $file['name']; //nome do arquivo
		$foto_tamanho = $file['size']; //tamanho do arquivo
		$foto_erro = $file['error']; //retorna o c?digo de erro no upload
		//formatando nome do arquivo

		$dataatual = date('d')."-".date('m')."-".date('Y')."_".date('H')."".date('i')."".date('s');

		//$extensao = extensao($foto_nome);
		$extensao = strtolower(end(explode('.', $foto_nome)));

		$foto_novonome = "ft".$dataatual.".".$extensao;

		//abaixo a pasta no site onde dever? estar o arquivo
		$arquivo_caminho = "../utils/assinaturas/$foto_novonome";
		//abaixo o local (pasta fisica no servidor) onde dever? ser salvo o arquivo
		//$updir = "../uploads/noticias/$foto_novonome";
		if($extensao == 'jpg' OR $extensao == 'JPG' OR $extensao == 'jpeg' OR $extensao == 'JPEG'){
		//aqui, "ft" ? o nome do campo no formul?rio.
		if (is_uploaded_file($file['tmp_name'])){
		if (!move_uploaded_file($file['tmp_name'],$arquivo_caminho)){
		//testa se o arquivo foi tranferido para $upfile
		$msg= base64_encode("Ocorreu um erro no envio da foto. Tente novamente.");
		 header("Location: /adm/?adm=importar&msg={$msg}");
		}else{
		$sql="update usuarios set ASSINATURA = '{$arquivo_caminho}' where idUsuarios='{$id_user}' ";
	    if(!mysql_query($sql)){
		$msg= base64_encode("Erro no sql");
		header("Location: /adm/?adm=importar&msg={$msg}");
        return $msg;
		}
		$msg=base64_encode("Foto enviada com sucesso!");
		 header("Location: /adm/?adm=importar&msg={$msg}");
	//	header('LOCATION:?adm=importar');
		return $msg;
		}
		}
		//chmod($_SERVER['DOCUMENT_ROOT'].$pasta,0777);
		//salvar_imagem("../noticias/$foto_novonome");
		}else{
		$msg=base64_encode("Extensao nao permitida. Por favor envie arquivos apenas JPG");
		 header("Location: /adm/?adm=importar&msg={$msg}");
		}
	} // fecha if($_FILES['arq']['tmp_name']<>'')




}
////importar assinatura 27-06-2013 fim 
//////////salvar_ficha_admissao 2013-06-11
function salvar_ficha_admissao($post){
    extract($post,EXTR_OVERWRITE);
    $usuario = $_SESSION["id_user"];
    $paciente_id= anti_injection($paciente_id,'numerico');
    $hist_status_id=anti_injection($hist_status_id,'literal');
    $data_ad = anti_injection(implode("-",array_reverse(explode("/",$data_ad))),'literal');
    $atendimento_domiciliar =anti_injection($atendimento_domiciliar,'literal');
    $inicio_atendimento=anti_injection(implode("-",array_reverse(explode("/",$inicio_atendimento))),'literal');
    $fim_atendimento=anti_injection(implode("-",array_reverse(explode("/",$fim_atendimento))),'literal');
    $internacao_domiciliar =anti_injection($internacao_domiciliar,'literal');
    $inicio_internacao=anti_injection(implode("-",array_reverse(explode("/",$inicio_internacao))),'literal');
    $fim_internacao=anti_injection(implode("-",array_reverse(explode("/",$fim_internacao))),'literal');
    $tec_enfermagem =anti_injection($tec_enfermagem,'literal');
    $tec_06 =anti_injection($tec_06,'literal');
    $inicio_tec_06=anti_injection(implode("-",array_reverse(explode("/",$inicio_tec_06))),'literal');
    $fim_tec_06=anti_injection(implode("-",array_reverse(explode("/",$fim_tec_06))),'literal');
    $tec_12 =anti_injection($tec_12,'literal');
    $inicio_tec_12=anti_injection(implode("-",array_reverse(explode("/",$inicio_tec_12))),'literal');
    $fim_tec_12=anti_injection(implode("-",array_reverse(explode("/",$fim_tec_12))),'literal');
    $tec_24 =anti_injection($tec_24,'literal');
    $inicio_tec_24=anti_injection(implode("-",array_reverse(explode("/",$inicio_tec_24))),'literal');
    $fim_tec_24=anti_injection(implode("-",array_reverse(explode("/",$fim_tec_24))),'literal');
    $tec_01xdia =anti_injection($tec_01xdia,'literal');
    $inicio_tec_01xdia=anti_injection(implode("-",array_reverse(explode("/",$inicio_tec_01xdia))),'literal');
    $fim_tec_01xdia=anti_injection(implode("-",array_reverse(explode("/",$fim_tec_01xdia))),'literal');
    $tec_02xdia =anti_injection($tec_02xdia,'literal');
    $inicio_tec_02xdia=anti_injection(implode("-",array_reverse(explode("/",$inicio_tec_02xdia))),'literal');
    $fim_tec_02xdia=anti_injection(implode("-",array_reverse(explode("/",$fim_tec_02xdia))),'literal');
    $tec_03xdia =anti_injection($tec_03xdia,'literal');
    $inicio_tec_03xdia=anti_injection(implode("-",array_reverse(explode("/",$inicio_tec_03xdia))),'literal');
    $fim_tec_03xdia=anti_injection(implode("-",array_reverse(explode("/",$fim_tec_03xdia))),'literal');
    $tec_v48h =anti_injection($tec_v48h,'literal');
    $inicio_tec_v48h=anti_injection(implode("-",array_reverse(explode("/",$inicio_tec_v48h))),'literal');
    $fim_tec_v48h=anti_injection(implode("-",array_reverse(explode("/",$fim_tec_v48h))),'literal');
    $tec_v72h =anti_injection($tec_v72h,'literal');
    $inicio_tec_v72h=anti_injection(implode("-",array_reverse(explode("/",$inicio_tec_v72h))),'literal');
    $fim_tec_v72h=anti_injection(implode("-",array_reverse(explode("/",$fim_tec_v72h))),'literal');
    $v_medica =anti_injection($v_medica,'literal');
    $fmedica =anti_injection($fmedica,'literal');
    $v_enfermagem =anti_injection($v_enfermagem,'literal');
    $fenfermagem =anti_injection($fenfermagem,'literal');
    $fisioterapia =anti_injection($fisioterapia,'literal');
    $respiratoria =anti_injection($respiratoria,'literal');
    $respiratoria_sessao =anti_injection($respiratoria_sessao,'literal');
    $motora =anti_injection($motora,'literal');
    $motora_sessao =anti_injection($motora_sessao,'literal');
    $fonoterapia =anti_injection($fonoterapia,'literal');
    $fonoterapia_av =anti_injection($fonoterapia_av,'literal');
    $fonoterapia_sessao =anti_injection($fonoterapia_sessao,'literal');
    $nutricionista=anti_injection($nutricionista,'literal');
    $fnutricionista=anti_injection($fnutricionista,'literal');
    $qtd_diaria_dieta=anti_injection($qtd_diaria_dieta,'literal');
    $psicologo=anti_injection($psicologo,'literal');
    $fpsicologo=anti_injection($fpsicologo,'literal');
    $psicologo_sessao=anti_injection($psicologo_sessao,'literal');
    $medico_especialista=anti_injection($medico_especialista,'literal');
    $med_especialidade=anti_injection($med_especialidade,'literal');
    $observacao=anti_injection($observacao,'literal');
    $vNutricionista = anti_injection($v_nutricionista,'literal');
    $fVisitaNutricionista = anti_injection($f_visita_nutricionista,'literal');
    $sqlPaciente = "select convenio,empresa from clientes where idClientes = {$paciente_id}";
    $resultPaciente = mysql_query($sqlPaciente);
    $planoId = mysql_result($resultPaciente,0,0);
    $empresaId = mysql_result($resultPaciente,0,1);
   

    mysql_query('begin');
    $sql="Insert into ficha_admissao_paciente_secmed".
        "(USUARIO_ID,PACIENTE_ID,DATA,HISTORICO_STATUS_PACIENTE_ID,DATA_ADMISSAO,ATENDIMENTO_DOMICILIAR,INICIO_ATENDIMENTO,
        FIM_ATENDIMENTO,INTERNACAO_DOMICILIAR,INICIO_INTERNACAO,FIM_INTERNACAO,TECNICO_ENFERMAGEM,TECNICO_06,INICIO_TECNICO_06,
        FIM_TECNICO_06,TECNICO_12,INICIO_TECNICO_12,FIM_TECNICO_12,TECNICO_24,INICIO_TECNICO_24,FIM_TECNICO_24,TECNICO_1XDIA,INICIO_TECNICO_1XDIA,FIM_TECNICO_1XDIA,TECNICO_V48H,
        INICIO_TECNICO_V48H,FIM_TECNICO_V48H,TECNICO_V72H,INICIO_TECNICO_V72H,FIM_TECNICO_V72H,VISITA_MEDICA,FREQUENCIA_MEDICA,
        VISITA_ENFERMAGEM,FREQUENCIA_ENFERMAGEM,FISIOTERAPIA,RESPIRATORIA,RESPIRATORIA_SESSAO,MOTORA,MOTORA_SESSAO,FONOTERAPIA,
        FONOTERAPIA_AVALIACAO,FONOTERAPIA_SESSAO,NUTRICIONISTA,FREQUENCIA_NUTRICIONISTA,
        QTD_DIARIA_DIETA,PSICOLOGO,FREQUENCIA_PSICOLOGO,PSICOLOGO_SESSAO,MEDICO_ESPECIALISTA,
        MEDICO_ESPECIALIDADE,OBSERVACAO,
        TECNICO_2XDIA,INICIO_TECNICO_2XDIA,FIM_TECNICO_2XDIA, TECNICO_3XDIA,INICIO_TECNICO_3XDIA,
        FIM_TECNICO_3XDIA, VISITA_NUTRICIONISTA,FREQUENCIA_VISITA_NUTRICIONISTA,UR, PLANO_ID)".
         " VALUES".
        "('$usuario','$paciente_id',now(),'$hist_status_id','$data_ad','$atendimento_domiciliar','$inicio_atendimento','$fim_atendimento',
        '$internacao_domiciliar','$inicio_internacao','$fim_internacao','$tec_enfermagem','$tec_06','$inicio_tec_06','$fim_tec_06',
        '$tec_12','$inicio_tec_12','$fim_tec_12','$tec_24','$inicio_tec_24','$fim_tec_24',
        '$tec_01xdia','$inicio_tec_01xdia','$fim_tec_01xdia','$tec_v48h','$inicio_tec_v48h','$fim_tec_v48h','$tec_v72h',
        '$inicio_tec_v72h','$fim_tec_v72h','$v_medica','$fmedica','$v_enfermagem','$fenfermagem','$fisioterapia','$respiratoria',
        '$respiratoria_sessao','$motora','$motora_sessao','$fonoterapia','$fonoterapia_av','$fonoterapia_sessao','$nutricionista',
        '$fnutricionista','$qtd_diaria_dieta','$psicologo','$fpsicologo','$psicologo_sessao','$medico_especialista','$med_especialidade',
        '$observacao', '$tec_02xdia','$inicio_tec_02xdia','$fim_tec_02xdia','$tec_03xdia','$inicio_tec_03xdia','$fim_tec_03xdia',
        '$vNutricionista', '$fVisitaNutricionista', '$empresaId', '$planoId')";
    $r=mysql_query($sql);
    if(!$r){
		echo "ERRRO: No salvar ficha. ";
		return;
		mysql_query("rollback");
	}
        $id_ficha_admissao = mysql_insert_id();
	savelog(mysql_escape_string(addslashes($sql)));
        $equipe = explode("$",$equipamentos);

	foreach($equipe as $equip){
		if($equip != "" && $equip != null){
			$i = explode("#",$equip);
			$cod_equip = anti_injection($i[0],"numerico");
			$r = mysql_query("INSERT INTO equipamentos_admissao_paciente_secmed (ID,COBRANCAPLANOS_ID,FICHA_ADMISSAO_PACIENTE_SECMED_ID) VALUES (null,'{$cod_equip}','{$id_ficha_admissao}')");
			if(!$r){
				echo "ERRRO: No salvar equipamentos.";

				mysql_query("rollback");
				return;
			}
			savelog(mysql_escape_string(addslashes($sql)));
		}
	}
	mysql_query("commit");
	echo "1";


}
function desativarUsuario($post){

    extract($post, EXTR_OVERWRITE);
    $idUsuario = anti_injection($idUsuario, "numerico");
    $data = anti_injection($data, "literal");
    $data = implode("-",array_reverse(explode("/",$data)));
    $observacao = anti_injection($observacao, "literal");
    mysql_query('begin');
    $sqlDesativar ="update "
            . " `usuarios` "
            . "set "
            . "block_at = '{$data}'"
            . " WHERE"
            . " `idUsuarios` = '{$idUsuario}'";
            //echo $sqlDesativar;
    $resultDesativar= mysql_query($sqlDesativar);
    if(!$resultDesativar){
				echo "ERRO: Ao desativar Usuário. Entre em contato com o TI. Problema no Update ";
				mysql_query("rollback");
				return;
			}
    savelog(mysql_escape_string(addslashes($sqlDesativar)));
    $sqlInsert="Insert into
        `usuario_historico_status`  (
            `usuario_id_modificado`,
            `usuario_id_modificador`,
            `data_modificado` ,
            `data`,
            `observacao`,
            `acao`) 
            values (
            '{$idUsuario}',
             '{$_SESSION['id_user']}',
             '{$data}',
             now(),
             '{$observacao}',
             'DESATIVAR'
            )";

     $resultInsert =  mysql_query($sqlInsert);
     if(!$resultInsert){
                        echo "ERRO: Ao desativar Usuário. Entre em contato com o TI. Problema no Insert.";
			mysql_query("rollback");
			return;
     }
    savelog(mysql_escape_string(addslashes($sqlInsert)));
    mysql_query('commit');
    echo 1;

}

function ativarUsuario($post){
    extract($post, EXTR_OVERWRITE);
    $idUsuario = anti_injection($idUsuario, "numerico");
    mysql_query('begin');
    $sqlAtivar ="update "
            . " `usuarios` "
            . "set "
            . "block_at = '9999-12-31'"
            . " WHERE"
            . " `idUsuarios` = '{$idUsuario}'";
            //echo $sqlDesativar;
    $resultAtivar= mysql_query($sqlAtivar);
    if(!$resultAtivar){
				echo "ERRO: Ao Ativar Usuário. Entre em contato com o TI. Problema no Update ";
				mysql_query("rollback");
				return;
			}
    savelog(mysql_escape_string(addslashes($sqlAtivar)));
    $sqlInsert="Insert into
        `usuario_historico_status`  (
            `usuario_id_modificado`,
            `usuario_id_modificador`,
            `data_modificado` ,
            `data`,
            `observacao`,
            `acao`) 
            values (
            '{$idUsuario}',
             '{$_SESSION['id_user']}',
             '9999-12-31',
             now(),
             'Ativou o paciente.',
             'ATIVAR'
            )";

     $resultInsert =  mysql_query($sqlInsert);
     if(!$resultInsert){
                        echo "ERRO: Ao Ativar Usuário. Entre em contato com o TI. Problema no Insert.";
			mysql_query("rollback");
			return;
     }
    savelog(mysql_escape_string(addslashes($sqlInsert)));
    mysql_query('commit');
    echo 1;
}

function cancelarItemPlano($post){
    extract($post, EXTR_OVERWRITE);
    $idValorescobranca = anti_injection($id, "numerico");
    mysql_query('begin');

    $sql = "Update  valorescobranca set excluido_em = now() , excluido_por = {$_SESSION['id_user']} where id = {$idValorescobranca}";
    $r = mysql_query($sql);
    if(!$r){
        $err = mysql_errno();
        echo "Erro ao cancelar item.  ";

        mysql_query("rollback");
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));

    mysql_query('commit');
    echo 1;
}

function enviarEmailCoordenadoria($tipo, $empresa, $msg)
{
	$api_data = (object) Config::get('sendgrid');

	$gateway = new MailAdapter(
		new SendGridGateway(
			new \SendGrid($api_data->user, $api_data->pass),
			new \SendGrid\Email()
		)
	);

	$gateway->adapter->sendMailToCoodenadoria($tipo, $empresa, $msg);
}

function salvarDadosUltimaAlteracaoStatus($data) {
    $dataHoraAut = \DateTime::createFromFormat('d/m/Y', $data['dataAut'])->format('Y-m-d ') . $data['horaAut'];
    $dataHoraAdm = \DateTime::createFromFormat('d/m/Y', $data['dataAdm'])->format('Y-m-d ') . $data['horaAdm'];

    $sql = <<<SQL
UPDATE historico_status_paciente SET DATA_AUTORIZACAO = '{$dataHoraAut}', DATA_ADMISSAO = '{$dataHoraAdm}' WHERE ID = '{$data['id']}'
SQL;
    if(mysql_query($sql)){
        echo '1';
    } else {
        echo '0';
    }

}

function buscarImagemAssinatura($usuario) {
    $sql = <<<SQL
SELECT
  ASSINATURA
FROM
  usuarios
WHERE
  idUsuarios = '{$usuario}'
SQL;
    $rs = mysql_query($sql);
    $row = mysql_fetch_array($rs);
    echo $row['ASSINATURA'] != '' ? $row['ASSINATURA'] : '' ;
}

function cancelarAdmissao($idAdmissao){
    $idAdmissao = anti_injection($idAdmissao, "numerico");
    $sql = <<<SQL
    -- Update rows in table 'ficha_admissao_paciente_secmed'
    UPDATE ficha_admissao_paciente_secmed
    SET
    CANCELED_BY = {$_SESSION["id_user"]},
    CANCELED_AT = now()
        -- add more columns and values here
    WHERE 	/* add search conditions here */
        ID = $idAdmissao
SQL;
    $rs = mysql_query($sql);  
    if(!$rs){
        echo 'Erro ao cancelar admissão.';
    }else{
        echo 1;
    }

}

if(isset($_GET['query'])){
    switch($_GET['query']){
        case "show-ficha":
            show_ficha($_GET['p'],$_GET['t']);
            break;
    }
}

if(isset($_GET['query'])){
    switch($_GET['query']) {
        case "buscar-imagem-assinatura":
            buscarImagemAssinatura($_GET['usuario']);
            break;
    }
}

if(isset($_POST['query'])){
    switch($_POST['query']){
        case "salvar-paciente":
            salvar_paciente($_POST);
            break;
        case "editar-paciente":
            editar_paciente($_POST);
            break;
        case "excluir-paciente":
            excluir_paciente($_POST);
            break;
        case "remover-acompanhamento":
            del_acompanhamento($_POST);
            break;
        case "adicionar-acompanhamento":
            add_acompanhamento($_POST);
            break;
        case "finalizar-acompanhamento":
            end_acompanhamento($_POST);
            break;
        case "editar-ficha-avaliacao":
            editar_ficha_avaliacao($_POST);
            break;
        case "history":
            historico_acompanhamento($_POST);
            break;
        case "funcionarios":
            funcionarios($_POST);
            break;
        case "show-permissoes":
            show_permissoes($_POST);
            break;
        case "salvar-permissoes":
            salvar_permissoes($_POST);
            break;
        case "salvar-novo-plano":
            salvar_novo_plano($_POST);
            break;
        case "salvar-edicao-plano":
            salvar_edicao_plano($_POST);
            break;
        case "salvar-ficha":
            salvar_ficha($_POST,$_FILES);
            break;
        case "salvar-filial":
            salvar_filial($_POST);
            break;
        case "excluir-filial":
            excluir_filial($_POST);
            break;
        case "excluir-plano":
            excluir_plano($_POST);
            break;
        case "excluir-item-plano":
            excluir_item_plano($_POST);
            break;
        case "nova-capmed":
            nova_capmed($_POST);
            break;
        case "nova-ficha-evolucao":
            nova_ficha_evolucao($_POST);
            break;
        case "show-status-paciente":
            show_status_paciente($_POST);
            break;
        case "show-modalidade-paciente":
            show_modalidade_paciente($_POST);
            break;
        case "salvar-status-paciente":
            salvar_status_paciente($_POST);
            break;
        case "salvar-modalidade-paciente":
            salvar_modalidade_paciente($_POST);
            break;
        case "salvar-obs":
            salvar_obs($_POST);
            break;
        case "editar-produtoInterno":
            editar_produtoInterno($_POST);
            break;
        case "liberar":
            liberar($_POST['id_prescricao']);
            break;
        case "desativar":
            desativar($_POST['id_prescricao']);
            break;
        case "assinatura":
            assinatura($_POST['usuario'],$_FILES['arquivo']);
            break;
        case "pesquisar-paciente":
            pesquisar_paciente($_POST);
            break;
        case "salvar-ficha-admissao":
            salvar_ficha_admissao($_POST);
            break;
        case "desativar-usuario":
            desativarUsuario($_POST);
            break;
        case "ativar-usuario":
            ativarUsuario($_POST);
            break;
        case "salvar-dados-ultima-alteracao":
        salvarDadosUltimaAlteracaoStatus($_POST['data']);
        break;
        case "cancelar-item-plano":
            cancelarItemPlano($_POST);
        break;
        case "cancelar-admissao":
            cancelarAdmissao($_POST['idAdmissao']);
        break;
    }
}
