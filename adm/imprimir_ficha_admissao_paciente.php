<?php
$id = session_id();
if(empty($id))
  session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('form_admissao_paciente.php');
class imprimir_ficha_admissao_paciente{
 public $cabecalho;
 
 function __construct() {
      $this->cabecalho = new Form_admissao_paciente();
      
 }
 public function detalhes_ficha_admissao_paciente($id){
       $html.= "<form><div width='100%'>"; 
       $html.= "<span><h2><a><img src='../utils/logo2.jpg' width='250' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>Ficha de Admissão Mederi.</h2><span>";

        $a = $this->cabecalho;
        $html.= $a->cabecalho_admissao($id,1);
        
        $sql="Select *,DATE_FORMAT(DATA_ADMISSAO, '%d/%m/%Y %h:%i:%s') data_ad from ficha_admissao_paciente_secmed where ID ={$id}";
             $result=mysql_query($sql);
             while ($row = mysql_fetch_array($result)) {
                 $usuario=$row['USUARIO_ID'];
        $paciente_id=$row['PACIENTE_ID'];
        
        $hist_status_id=$row['HISTORICO_STATUS_PACIENTE_ID'];        
        $atendimento_domiciliar=$row['ATENDIMENTO_DOMICILIAR'];       
        $internacao_domiciliar=$row['INTERNACAO_DOMICILIAR'];        
        $tec_enfermagem=$row['TECNICO_ENFERMAGEM'];
        $tec_06=$row['TECNICO_06'];        
        $tec_12=$row['TECNICO_12'];       
        $tec_24=$row['TECNICO_24'];        
        $tec_01xdia=$row['TECNICO_1XDIA'];
         $tec_02xdia=$row['TECNICO_2XDIA'];
         $tec_03xdia=$row['TECNICO_3XDIA'];
        $tec_v48h=$row['TECNICO_V48H'];        
        $tec_v72h=$row['TECNICO_V72H'];        
        $v_medica=$row['VISITA_MEDICA'];
        $fmedica=$row['FREQUENCIA_MEDICA'];
        $v_enfermagem=$row['VISITA_ENFERMAGEM'];
        $fenfermagem=$row['FREQUENCIA_ENFERMAGEM'];
        $fisioterapia=$row['FISIOTERAPIA'];
        $respiratoria=$row['RESPIRATORIA'];
        $respiratoria_sessao=$row['RESPIRATORIA_SESSAO'];
        $motora=$row['MOTORA'];
        $motora_sessao=$row['MOTORA_SESSAO'];
        $fonoterapia=$row['FONOTERAPIA'];
        $fonoterapia_av=$row['FONOTERAPIA_AVALIACAO'];
        $fonoterapia_sessao=$row['FONOTERAPIA_SESSAO'];
        $nutricionista=$row['NUTRICIONISTA'];
        $fnutricionista=$row['FREQUENCIA_NUTRICIONISTA'];
        $qtd_diaria_dieta=$row['QTD_DIARIA_DIETA'];
        $psicologo=$row['PSICOLOGO'];
        $fpsicologo=$row['FREQUENCIA_PSICOLOGO'];
        $psicologo_sessao=$row['PSICOLOGO_SESSAO'];
        $medico_especialista=$row['MEDICO_ESPECIALISTA'];
        $med_especialidade=$row['MEDICO_ESPECIALIDADE'];
        $observacao=$row['OBSERVACAO'];
              $data_ad= $row['data_ad'];
            $inicio_atendimento=join("/", array_reverse(explode("-", $row['INICIO_ATENDIMENTO'])));
            $fim_atendimento=join("/", array_reverse(explode("-", $row['FIM_ATENDIMENTO'])));
            $inicio_internacao=join("/", array_reverse(explode("-", $row['INICIO_INTERNACAO'])));
            $fim_internacao=join("/", array_reverse(explode("-", $row['FIM_INTERNACAO'])));
            $inicio_tec_06=join("/", array_reverse(explode("-", $row['INICIO_TECNICO_06'])));
            $fim_tec_06=join("/", array_reverse(explode("-", $row['FIM_TECNICO_06'])));
            $inicio_tec_12=join("/", array_reverse(explode("-", $row['INICIO_TECNICO_12'])));
            $fim_tec_12=join("/", array_reverse(explode("-", $row['FIM_TECNICO_12'])));
            $inicio_tec_24=join("/", array_reverse(explode("-", $row['INICIO_TECNICO_24'])));
            $fim_tec_24=join("/", array_reverse(explode("-", $row['FIM_TECNICO_24'])));
            $inicio_tec_01xdia=join("/", array_reverse(explode("-", $row['INICIO_TECNICO_1XDIA'])));
            $fim_tec_01xdia=join("/", array_reverse(explode("-", $row['FIM_TECNICO_1XDIA'])));
             $inicio_tec_02xdia=join("/", array_reverse(explode("-", $row['INICIO_TECNICO_2XDIA'])));
             $fim_tec_02xdia=join("/", array_reverse(explode("-", $row['FIM_TECNICO_2XDIA'])));
             $inicio_tec_03xdia=join("/", array_reverse(explode("-", $row['INICIO_TECNICO_3XDIA'])));
             $fim_tec_03xdia=join("/", array_reverse(explode("-", $row['FIM_TECNICO_3XDIA'])));
            $inicio_tec_v48h=join("/", array_reverse(explode("-", $row['INICIO_TECNICO_V48H'])));
            $fim_tec_v48h=join("/", array_reverse(explode("-", $row['FIM_TECNICO_V48H'])));
            $inicio_tec_v72h=join("/", array_reverse(explode("-", $row['INICIO_TECNICO_V72H'])));
            $fim_tec_v72h=join("/", array_reverse(explode("-", $row['FIM_TECNICO_V72H'])));
            $visitaNutricionista=$row['VISITA_NUTRICIONISTA'];
            $frequenciaVisitaNutricionista=$row['FREQUENCIA_VISITA_NUTRICIONISTA'];
            
      }
      
        $html.=  "<p><b>Previsão de Admiss&atilde;o: </b>{$data_ad}</p>";
        $html .=  "<p width='100%' style='text-align:center;background-color:#EEEEEE;'><b>Modalidade de Assist&ecirc;ncia Autorizada</b></p>";
          if($atendimento_domiciliar=='1'){
               $html .= "<span><b>Atendimento Domiciliar</b> Início: $inicio_atendimento  até Fim: $fim_atendimento</span>";
          }
         if ($internacao_domiciliar=='1'){
         $html .= "<br/><span><b>Internacao Domiciliar</b> Início: $inicio_internacao até Fim: $fim_internacao </span>";
       }
       //tec enfermagem
       if($tec_enfermagem=='1'){
               $html .=" <p width='100%' style='text-align:center;background-color:#EEEEEE;'><b>T&eacute;cnico de Enfermagem</b></p>";
            if($tec_06=='1'){
                $html .="<span><b>Interna&ccedil;&atilde;o 06h</b> Início: $inicio_tec_06 <b>até </b>Fim: $fim_tec_06 </span>";
             }
            
            if($tec_12=='1'){
                $html .="<br/><span><b>Interna&ccedil;&atilde;o 12h</b> Início: $inicio_tec_12 <b>até </b>Fim: $fim_tec_12</span>";
             }
             
             if($tec_24=='1'){
                $html .="<br/><span><b>Interna&ccedil;&atilde;o 24h</b> Início: $inicio_tec_24 <b>até </b> Fim: $fim_tec_24";
             }
             
             if($tec_01xdia=='1'){
                 $html.= "<br/><span><b>01 vez ao dia</b> Início: $inicio_tec_01xdia <b>até </b> Fim: $fim_tec_01xdia</span>";
            }

           if($tec_02xdia=='1'){
               $html.= "<br/><span><b>02 vez ao dia</b> Início: $inicio_tec_02xdia <b>até </b> Fim: $fim_tec_02xdia</span>";
           }

           if($tec_03xdia=='1'){
               $html.= "<br/><span><b>03 vez ao dia</b> Início: $inicio_tec_03xdia <b>até </b> Fim: $fim_tec_03xdia</span>";
           }
            
            if($tec_v48h=='1'){
                $html .="<br/><span><b>Visita a cada 48h</b> Início: $inicio_tec_v48h <b>até</b> Fim: $fim_tec_v48h </span>";
            }
            
            if($tec_v72h=='1'){
               $html .="<br/><span><b>Visita a cada 72h</b>Início: $inicio_tec_v72h  <b>até</b> Fim: $fim_tec_v72h </span>";
            }
            
             
         }
          ///Visita médica   
         if($v_medica=='1'){
        $html .="<p width='100%' style='text-align:center;background-color:#EEEEEE;'><b>Visita Médica</b></p>";
         $nome='fmedica';
         $html .= "<span><b>Frequ&ecirc;ncia:</b> ".$a->frequencia($nome,$fmedica,1)."</span>";
         }
     ///Visita nutricionista
     if($visitaNutricionista =='1'){
         $html .="<p width='100%' style='text-align:center;background-color:#EEEEEE;'><b>Visita Nutricionista</b></p>";
         $nome='f_visita_nutricionista';
         $html .= "<span><b>Frequ&ecirc;ncia:</b> ".$a->frequencia($nome,$frequenciaVisitaNutricionista,1)."</span>";
     }
         
         //visita enfermeiro
         if($v_enfermagem=='1'){
           $html .=  " <p width='100%' style='text-align:center;background-color:#EEEEEE;'><b>Visita Enfermeiro(a)</b></p>";
           $nome='fenfermagem';
           $html .=  "<span><b>Frequ&ecirc;ncia: </b>". $a->frequencia($nome,$fenfermagem,1)."</span>";
         }
         
         ///fisioterapia
         if($fisioterapia=='1'){
        $html .= "<p width='100%' style='text-align:center;background-color:#EEEEEE;'><b>Sess&atilde;o Fisioterapia</b></p>";
         if($respiratoria=='1'){
           $html .= "<span><b>Respiratória: </b> $respiratoria_sessao N° Sess&atilde;o/semana</span>";

         }
         if($motora=='1'){
               $html .=  "<br/><span><b>Motora: </b> $motora_sessao N° Sess&atilde;o/semana</span>";
        
         }
       
         }
     ///fono
     if($fonoterapia=='1'){
         $html .= "<p width='100%' style='text-align:center;background-color:#EEEEEE;'><b>Sess&atilde;o Fonoterapia</b></p>";
         if($fonoterapia_av)
             $avFono = "<span>Avaliação.</span><br>";
         if(!empty($fonoterapia_sessao))
             $sessaoFono= "<span>  N° sessão /semana: {$fonoterapia_sessao}</span>";
         $html .= "<br>".$avFono." ".$sessaoFono;
     }
          ///nutricionista
     if($nutricionista=='1') {
         $html .= "<p width='100%' style='text-align:center;background-color:#EEEEEE;'><b>Sess&atilde;o Nutricionista</b></p>";
         $textDieta = $qtd_diaria_dieta > 0 ? "N° de diária de dieta aut.: {$qtd_diaria_dieta}" : '';

         $html .= "<span><b>Frequ&ecirc;ncia: </b>" . $a->frequencia($nome, $fnutricionista, 1) . " {$textDieta}</span>";
     }


     ///psicologo
         if($psicologo=='1'){
          $html .="<p width='100%' style='text-align:center;background-color:#EEEEEE;'><b>Pscic&oacute;logo</b></p>";
          $nome='fpsicologo';
         $html .=  "<span><b>Frequ&ecirc;ncia: </b>". $a->frequencia($nome,$fpsicologo,1)."</span>";
         $html .=  "<br/><span> <b>N° de sess&otilde;es/semana:</b> $psicologo_sessao</span>";
          
         }
         //médico especialista
        if($medico_especialista=='1'){
           $html .="<p width='100%' style='text-align:center;background-color:#EEEEEE;'> <b>Médico Especialista</b></p>";
           $html .="<span><b>Especialidade: </b>$med_especialidade </span>";
        }  
        
        //equipamentos
        $sql="select cb.item from equipamentos_admissao_paciente_secmed as e inner join cobrancaplanos as cb on (e.COBRANCAPLANOS_ID =cb.id) where e.FICHA_ADMISSAO_PACIENTE_SECMED_ID={$id}";
        $result = mysql_query($sql);  
        $equipamento= mysql_num_rows($result);
        if($equipamento >0){
            
       $html .= "<p width='100%' style='text-align:center;background-color:#EEEEEE;'><b>Equipamentos Especificos Liberados</b></p>";
              while ($row = mysql_fetch_array($result)) {
                $html .= "<span>".$row['item']."</span><br/>";
               
            }
        }
        //observação
        $html .= "<p width='100%' style='text-align:center;background-color:#EEEEEE;'><b>Observa&ccedil;&atilde;o</b></p>";
        $html.="<br/><span> $observacao</span>";
        
        $html.= "</div>";
       $paginas []= $header.$html.= "</form>";
        $mpdf=new mPDF('pt','A4',10);
	$mpdf->SetHeader('página {PAGENO} de {nbpg}');
	$ano = date("Y");
	$mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
	$mpdf->WriteHTML("<html><body>");
	$flag = false;
	foreach($paginas as $pag){
		if($flag) $mpdf->WriteHTML("<formfeed>");
		$mpdf->WriteHTML($pag);
		$flag = true;
	}
	$mpdf->WriteHTML("</body></html>");
	$mpdf->Output('ficha_admissao_paciente.pdf','I');
	exit;
 }
    
}
$p = new imprimir_ficha_admissao_paciente();
$p->detalhes_ficha_admissao_paciente($_GET['id']);
?>
