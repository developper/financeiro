<?php
include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');
function liberar(){
    echo "<div id='liberar-prescricao'>";
    echo " <center><h1>Liberar Prescri&ccedil;&atilde;o de  Avali&ccedil;&atilde;o</h1></center>";

    $sql = "select presc.id, presc.paciente as idpac, presc.ID_CAPMEDICA,DATE_FORMAT(presc.data,'%d/%m/%Y - %T') AS sdata,
				DATE_FORMAT(presc.inicio,'%d/%m/%Y') AS inicio,
				DATE_FORMAT(presc.fim,'%d/%m/%Y') AS fim,
				UPPER(c.nome) AS paciente,
				u.nome AS medico,
                                u.idUsuarios as usuario,
				(CASE presc.carater 
				 WHEN '4' THEN '<label style=\'color:orange;\'><b>Avalia&ccedil;&atilde;o</b></label>' 
				 WHEN '7' THEN '<label style=\'color:#9ACD32 ;\'><b>Aditiva Avalia&ccedil;&atilde;o</b></label>'
				 END) AS tipo
		     from prescricoes as  presc inner join
				 usuarios as u on (presc.criador=idUsuarios) inner join
				 clientes as c on (presc.paciente =c.idclientes)
			where presc.Carater in (4,7) and  presc.DATA_DESATIVADA = '0000-00-00 00:00:00' 
				  and LIBERADO = 'N' and lockp != '9999-12-31 23:59:59' and presc.STATUS <> 1 order by presc.data desc";
    $result = mysql_query($sql);
    $x=mysql_num_rows($result);
    if ($x>0){
        echo"<table class='mytable' width='100%'>";
        echo "<thead><tr><th>TIPO</th><th>PACIENTE</th><th>PROFISSIONAL</th><th>DATA</th><th>PER&Iacute;DO</th><th>OP&Ccedil;&Atilde;O</th></tr></thead>";
        while($row = mysql_fetch_array($result)){
            echo "<tr><td>{$row['tipo']}</td><td>{$row['paciente']}</td><td>{$row['medico']}</td><td>{$row['sdata']}</td>
		      <td>{$row['inicio']} - {$row['fim']}</td>
		      <td><a href='../medico/?view=paciente_med&act=capmed&id={$row['ID_CAPMEDICA']}' target='_blank' class='visualizar' id_prescricao='{$row['id']}' ><img border='0' title='Detalhes Ficha' width='22px' src='../utils/capmed_16x16.png'></a>&nbsp;&nbsp;&nbsp
		      <a href='../medico/?view=paciente_med&act=detalhesprescricao&id={$row['ID_CAPMEDICA']}&idpac={$row['idpac']}&medico={$row['usuario']}' target='_blank' class='visualizar' id_prescricao='{$row['id']}' ><img border='0' title='Detalhes prescri&ccedil;&atilde;o' width='22px' src='../utils/fichas_24x24.png'></a>&nbsp;&nbsp;&nbsp
		      <a href='#' class='liberar' id_prescricao='{$row['id']}'><img src='../utils/aceitar.png' title='Liberar' width='22px' border='0'></a>&nbsp;&nbsp;&nbsp
		      <a href='#' class='desativar' id_prescricao='{$row['id']}'><img src='../utils/delete_22x22.png' title='Desativar' border='0'></a></td></tr>";
            /*echo "<tr><td>{$row['tipo']}</td><td>{$row['paciente']}</td><td>{$row['medico']}</td><td>{$row['sdata']}</td>
          <td>{$row['inicio']} - {$row['fim']}</td>
          <td><a href='imprimir_ficha.php?id={$row['ID_CAPMEDICA']}' target='_blank' class='visualizar' id_prescricao='{$row['id']}' ><img border='0' title='Detalhes Ficha' width='22px' src='../utils/capmed_16x16.png'></a>&nbsp;&nbsp;&nbsp
          <a href='imprimir_prescricao.php?id={$row['ID_CAPMEDICA']}&idpac={$row['idpac']}' target='_blank' class='visualizar' id_prescricao='{$row['id']}' ><img border='0' title='Detalhes prescri&ccedil;&atilde;o' width='22px' src='../utils/fichas_24x24.png'></a>&nbsp;&nbsp;&nbsp
          <a href='#' class='liberar' id_prescricao='{$row['id']}'><img src='../utils/aceitar.png' title='Liberar' width='22px' border='0'></a>&nbsp;&nbsp;&nbsp
          <a href='#' class='desativar' id_prescricao='{$row['id']}'><img src='../utils/delete_22x22.png' title='Desativar' border='0'></a></td></tr>";*/


        }
        echo"</table>";

    }else{
        echo"<p><center><b>N&atildeo existe Prescri&ccedil;&atildeo para ser liberada no momento.</b></center></p>";
    }

    echo "</div>";

}

function  assinatura($msg){
    echo "<div> ";
    echo "<h1><center>Importar Assinaturas</center></h1>";
    echo"<p><b>Usu&aacute;rios:</b> ";
    
    $compUr = "";
    if($_SESSION['empresa_principal'] != '1') {
        $compUr = " AND empresa = '{$_SESSION['empresa_principal']}'";
    }

    $sql ="SELECT idUsuarios, nome 
    FROM 
    usuarios 
    WHERE 
    tipo IN ('medico','enfermagem','nutricao', 
                            'fisioterapia','servico_social','fonoaudiologia','psicologia') {$compUr} ORDER BY nome ASC ";
    $result = mysql_query($sql);
    echo"<br/><form action='query.php' method='POST' name='form' enctype='multipart/form-data'>";
    echo "<select name='usuario' id='usuario-assinatura'>";
    echo "    <option value='' selected disabled>Selecione...</option>";
    while($row = mysql_fetch_array($result)){
        echo "<option value={$row['idUsuarios']}>{$row['nome']}</option>";
    }
    echo "</select></p>";
    echo "<p id='imagem-usuario'></p>";
    echo"
            <p>
                <b>Enviar o arquivo:</b> <input type='file' name='arquivo' size='20'><br>
                <input type='hidden' name='query' value='assinatura'>
                <br><input type='submit' value='Enviar'>
            </p>
		</form>";
    echo"<p><center><b>".base64_decode($msg)."</b></center></p>";

    echo "</div> ";

}

if(isset($_GET['query'])){
    switch($_GET['query']){
        case "msg":
            assinatura($_GET['msg']);
            break;
    }
}

?>