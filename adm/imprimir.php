<?php
echo '<link rel="stylesheet" href="/utils/bootstrap-3.3.6-dist/css/bootstrap.min.css" />';
$id = session_id();
if(empty($id))
    session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');

$cond_nome = '';
$cond_status = '';
$cond_liminar = '';
$cond_empresa = '';
$cond_convenio = '';

if(isset($_GET['busca'])) {
    $cond_nome = $_GET['busca'] != '' ? "and (c.nome like '%{$_GET['busca']}%' OR c.codigo like '%{$_GET['busca']}%')":'';
}
if(isset($_GET['status'])){
    $cond_status= $_GET['status'] != '' ? " and ( c.status='{$_GET['status']}' OR st.status LIKE '%{$_GET['status']}%')":'';
}
if(isset($_GET['liminar'])) {
    $cond_liminar = $_GET['liminar'] != '' ? "and c.LIMINAR = {$_GET['liminar']}":'';
}
if(isset($_GET['empresa']) && !empty($_GET['empresa'])) {
    $cond_empresa = $_GET['empresa'] != '' ?" and c.empresa = {$_GET['empresa']}":'';
} else {
    $cond_empresa = $_SESSION['empresa_principal'] != 1 ? " AND c.empresa IN {$_SESSION['empresa_user']}" : "";
}

if(isset($_GET['convenio'])){
    $cond_convenio = $_GET['convenio']!= '' ? "AND c.convenio = '{$_GET['convenio']}'" : '';
}

$sql = "SELECT         
                 c.nome,
                 DATE_FORMAT(nascimento,'%d/%m/%Y') as nasc, 
                 DATE_FORMAT(dataInternacao,'%d/%m/%Y') as inter,
                 st.status as Status,                 
                 pl.nome as Plano,
                 emp.nome as 'Un. Regional',
                 (Case when  c.LIMINAR =1 then 'Sim' else 'Não' end )  as limi,
                 emp.logo
                 FROM 
                 clientes as c left join 
                 statuspaciente as st on (c.status=st.id) left join
                 planosdesaude as pl on (c.convenio=pl.id) left join 
                 empresas as emp on (c.empresa=emp.id)
                 where
                 1 = 1
                 {$cond_empresa} 
                 {$cond_liminar} 
                 {$cond_nome} 
                 {$cond_status}
                  {$cond_convenio}
                 ORDER BY nome";

$result = mysql_query($sql);
$count = mysql_num_rows($result);

$sql = "SELECT logo, UPPER(nome) AS nome FROM empresas WHERE empresas.id = '{$_SESSION['empresa_principal']}'";
$rs = mysql_query($sql);
$rowLogo = mysql_fetch_array($rs);
echo "<center><h1>Listagem de Pacientes</h1></center>";
echo "<h3><img src='/utils/logos/{$rowLogo['logo']}' width='100'> </h3><br>";
echo "<b>Total de " . $count . " pacientes.</b><br>";
$html = "<table class='table table-bordered table-striped table-condensed' width=100% >";
$html .= "<thead><tr>";
$html .= "<th><b>Paciente</b></th>";
$html .= "<th><b>Conv&ecirc;nio</b></th>";
$html .= "<th><b>Un. Regional</b></th>";
$html .= "<th><b>Status</b></th>";
$html .= "<th><b>Liminar</b></th>";
$html .= "<th><b>Nascimento</b></th>";
$html .= "<th><b>Data Interna&ccedil;&atilde;o</b></th>";
//    $html .= "<th colspan='2' ><b>Op&ccedil;&otilde;es</b></th>";
$html .= "</tr></thead>";
$cor = false;
while($row = mysql_fetch_array($result))
{
    foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
    if($cor) $html .= "<tr>";
    else $html .= "<tr class='odd' >";
    $cor = !$cor;

    $html .= "<td>".utf8_decode(mb_strtoupper($row['nome'],'UTF8'))."</td>";
    $html .= "<td>".utf8_decode(mb_strtoupper($row['Plano'],'UTF8'))."</td>";
    $html .= "<td>".utf8_decode(mb_strtoupper($row['Un. Regional'],'UTF8'))."</td>";
    $html .= "<td>".utf8_decode(mb_strtoupper($row['Status'],'UTF8'))."</td>";
    $html .= "<td>". utf8_decode(mb_strtoupper($row['limi'],'UTF8'))."</td>";
    $html .= "<td>{$row['nasc']}</td>";
    $html .= "<td>{$row['inter']}</td>";

    $html .= "</tr>";
}
$html .= "</table>";
print_r($html);
/*$mpdf=new mPDF('pt','A4',9);
$mpdf->WriteHTML("<formfeed>");
$mpdf->WriteHTML($html);
$mpdf->WriteHTML("</body></html>");
$mpdf->Output('pacientes.pdf','I');*/
?>