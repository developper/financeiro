<?php
include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');

class Form_admissao_paciente {
    public function frequencia($id,$option,$imprimir){
        
        $sql = "SELECT * FROM frequencia ORDER BY frequencia";
        $result = mysql_query($sql);
        $html .="<select name='$id' class='frequencia' id='$id' {$option} disabled='disabled'><option value='-1'>Selecione</option>";
        while ($row = mysql_fetch_array($result)) {
            if($option == $row['id'] ){
                $html .="<option value='{$row['id']}' selected='selected'>{$row['frequencia']}</option>";
             $frequencia=$row['frequencia'];
            }
            else{
            $html .="<option value='{$row['id']}'>{$row['frequencia']}</option>";
            }
        }
        $html .="</select>";
        if($imprimir==1){
            return $frequencia;
        }else{
        return $html;
        }
    }
    public function cabecalho_admissao($fichaId, $tipo, $pacienteId = null){
       ///se tipo = 0 ele faz a busca pelo historico_status_paciente
       
        if($tipo==0 && !empty($fichaId)){
           $cond=" inner join  historico_status_paciente as his on (c.idClientes=his.PACIENTE_ID)";
           $cond2="his.ID = $fichaId";
        }else if ($tipo==1) {
            $cond  = " inner join  ficha_admissao_paciente_secmed as fa on (c.idClientes=fa.PACIENTE_ID)";
            $cond2 = isset($fichaId) ? "fa.ID = $fichaId" : "c.idClientes = {$pacienteId}";
        }else if($tipo == 0 && !empty($pacienteId)){
            $cond  = " ";
            $cond2 =  "c.idClientes = {$pacienteId}";

        }
        $sql2 = "SELECT
	UPPER(c.nome) AS paciente,
	(CASE c.sexo
	WHEN '0' THEN 'Masculino'
	WHEN '1' THEN 'Feminino'
	END) AS nomesexo,
	FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
	e.nome as empresan,
	c.`nascimento`,
        c.responsavel,
        c.TEL_RESPONSAVEL,
        c.TEL_LOCAL_INTERNADO,
	p.nome as Convenio,p.id,c.*,(CASE c.acompSolicitante
	WHEN 's' THEN 'Sim'
	WHEN 'n' THEN 'N&atilde;o'
	END) AS acomp,
	c.NUM_MATRICULA_CONVENIO,
        (select  concat(x.NOME,',',x.UF) from cidades as x where x.id=c.CIDADE_ID_INT) as cidade_int,
        c.END_INTERNADO,
        c.BAI_INTERNADO
       
	FROM
	clientes AS c LEFT JOIN
	empresas AS e ON (e.id = c.empresa) INNER JOIN
	planosdesaude as p ON (p.id = c.convenio) 
        {$cond}
	WHERE
        {$cond2}
	ORDER BY
	c.nome DESC LIMIT 1";

        $result = mysql_query($sql);
        $result2 = mysql_query($sql2);
        $html.= "<table style='width:100%;' >";
        while ($pessoa = mysql_fetch_array($result2)) {
            foreach ($pessoa AS $chave => $valor) {
                $pessoa[$chave] = stripslashes($valor);
            }


            $html.= "<tr bgcolor='#EEEEEE'>";
            $html.= "<td  style='width:50%;'><b>PACIENTE:</b></td>";
            $html.= "<td style='width:25%;'><b>SEXO </b></td>";
            $html.= "<td style='width:30%;'><b>IDADE:</b></td>";
            $html.= "</tr>";
            $html.= "<tr>";
            $html.= "<td >{$pessoa['paciente']}</td>";
            $html.= "<td>{$pessoa['nomesexo']}</td>";
            $html.= "<td >" . join("/", array_reverse(explode("-", $pessoa['nascimento']))) . ' (' . $pessoa['idade'] . " anos)</td>";
            $html.= "</tr>";

            $html.= "<tr bgcolor='#EEEEEE'>";

            $html.= "<td ><b>UNIDADE REGIONAL:</b></td>";
            $html.= "<td ><b>CONV&Ecirc;NIO </b></td>";
            $html.= "<td ><b>MATR&Iacute;CULA </b></td>";
            $html.= "</tr>";
            $html.= "<tr>";
            $html.= "<td>{$pessoa['empresan']}</td>";
            $html.= "<td>" . strtoupper($pessoa['Convenio']) . "</td>";
            $html.= "<td>" . strtoupper($pessoa['NUM_MATRICULA_CONVENIO']) . "</td>";
            $html.= "</tr>";
            $html.= "<tr bgcolor='#EEEEEE'>";
            $html.= "<td><b>Familiar Responsav&eacute;l:</b></td>";
            $html.= "<td colspan='2'><b>TELEFONE:</b></td>";
            $html.= "</tr>";
            $html.= "<tr>";
            $html.= "<td>{$pessoa['responsavel']}</td>";
            $html.= "<td colspan='2'>{$pessoa['TEL_RESPONSAVEL']} -- {$pessoa['TEL_LOCAL_INTERNADO']}</td>";
            $html.= "</tr>";
            $html.= "<tr bgcolor='#EEEEEE'>";
            $html.= "<td><b>Endere&ccedil;o de Implanta&ccedil;&atilde;o</b></td>";
            $html.= "<td><b>Bairro de Implanta&ccedil;&atilde;o:</b></td>";
            $html.= "<td><b>Cidade de Implanta&ccedil;&atilde;o:</b></td>";
            $html.= "</tr>";
            $html.= "<tr>";
            $html.= "<td>{$pessoa['END_INTERNADO']}</td>";
            $html.= "<td>{$pessoa['BAI_INTERNADO']}</td>";
            $html.= "<td>{$pessoa['cidade_int']}</td>";
            $html.= "</tr>";
            
        }
        $html.= "</table>";
        return $html;
    }

    public function form_admissao($id, $tipo, $pacienteId) {
        $id=  anti_injection($id, 'literal');
        
        ////funcao que monta o cabeçalho da ficha.
         echo"<h1><center>Ficha de Admiss&atilde;o do Paciente.</center></h1>";
         echo $this->cabecalho_admissao($id,$tipo,$pacienteId);
         if(!empty($id)){
              ///////////pegar a data da adimsão na tabela de historico_status_paciente
            $sql= "Select  DATE_FORMAT(DATA_ADMISSAO, '%d/%m/%Y') data_ad,PACIENTE_ID from historico_status_paciente where ID={$id}";
            $result = mysql_query($sql);
            $data_ad = mysql_result($result,0,0);
            $paciente_id = mysql_result($result,0,1);
         }else{
            $paciente_id = $pacienteId;

         }
        
        
         ///tipo igual a 1 vai ser visualizar uma ficha de admiss~ãoja feita anteriormente.
         if($tipo==1){
             $sql="Select *,DATE_FORMAT(DATA_ADMISSAO, '%d/%m/%Y') data_ad from ficha_admissao_paciente_secmed where ID ={$id}";
             $result=mysql_query($sql);
             while ($row = mysql_fetch_array($result)) {
                 $usuario=$row['USUARIO_ID'];
        $paciente_id=$row['PACIENTE_ID'];
        
        $hist_status_id=$row['HISTORICO_STATUS_PACIENTE_ID'];        
        $atendimento_domiciliar=$row['ATENDIMENTO_DOMICILIAR'];       
        $internacao_domiciliar=$row['INTERNACAO_DOMICILIAR'];        
        $tec_enfermagem=$row['TECNICO_ENFERMAGEM'];
        $tec_06=$row['TECNICO_06'];        
        $tec_12=$row['TECNICO_12'];       
        $tec_24=$row['TECNICO_24'];        
        $tec_01xdia=$row['TECNICO_1XDIA'];
        $tec_02xdia=$row['TECNICO_2XDIA'];
        $tec_03xdia=$row['TECNICO_3XDIA'];
        $tec_v48h=$row['TECNICO_V48H'];
        $tec_v72h=$row['TECNICO_V72H'];        
        $v_medica=$row['VISITA_MEDICA'];
         $fmedica=$row['FREQUENCIA_MEDICA'];
         $v_nutricionista=$row['VISITA_NUTRICIONISTA'];
         $fVisitaNutricionista=$row['FREQUENCIA_VISITA_NUTRICIONISTA'];
        $v_enfermagem=$row['VISITA_ENFERMAGEM'];
        $fenfermagem=$row['FREQUENCIA_ENFERMAGEM'];
        $fisioterapia=$row['FISIOTERAPIA'];
        $respiratoria=$row['RESPIRATORIA'];
        $respiratoria_sessao=$row['RESPIRATORIA_SESSAO'];
        $motora=$row['MOTORA'];
        $motora_sessao=$row['MOTORA_SESSAO'];
        $fonoterapia=$row['FONOTERAPIA'];
        $fonoterapia_av=$row['FONOTERAPIA_AVALIACAO'];
        $fonoterapia_sessao=$row['FONOTERAPIA_SESSAO'];
        $nutricionista=$row['NUTRICIONISTA'];
        $fnutricionista=$row['FREQUENCIA_NUTRICIONISTA'];
        $qtd_diaria_dieta=$row['QTD_DIARIA_DIETA'];
        $psicologo=$row['PSICOLOGO'];
        $fpsicologo=$row['FREQUENCIA_PSICOLOGO'];
        $psicologo_sessao=$row['PSICOLOGO_SESSAO'];
        $medico_especialista=$row['MEDICO_ESPECIALISTA'];
        $med_especialidade=$row['MEDICO_ESPECIALIDADE'];
        $observacao=$row['OBSERVACAO'];

              $data_ad = $row['data_ad'];
            $inicio_atendimento=join("/", array_reverse(explode("-", $row['INICIO_ATENDIMENTO'])));
            $fim_atendimento=join("/", array_reverse(explode("-", $row['FIM_ATENDIMENTO'])));
            $inicio_internacao=join("/", array_reverse(explode("-", $row['INICIO_INTERNACAO'])));
            $fim_internacao=join("/", array_reverse(explode("-", $row['FIM_INTERNACAO'])));
            $inicio_tec_06=join("/", array_reverse(explode("-", $row['INICIO_TECNICO_06'])));
            $fim_tec_06=join("/", array_reverse(explode("-", $row['FIM_TECNICO_06'])));
            $inicio_tec_12=join("/", array_reverse(explode("-", $row['INICIO_TECNICO_12'])));
            $fim_tec_12=join("/", array_reverse(explode("-", $row['FIM_TECNICO_12'])));
            $inicio_tec_24=join("/", array_reverse(explode("-", $row['INICIO_TECNICO_24'])));
            $fim_tec_24=join("/", array_reverse(explode("-", $row['FIM_TECNICO_24'])));
            $inicio_tec_01xdia=join("/", array_reverse(explode("-", $row['INICIO_TECNICO_1XDIA'])));
            $fim_tec_01xdia=join("/", array_reverse(explode("-", $row['FIM_TECNICO_1XDIA'])));
             $inicio_tec_02xdia=join("/", array_reverse(explode("-", $row['INICIO_TECNICO_2XDIA'])));
             $fim_tec_02xdia=join("/", array_reverse(explode("-", $row['FIM_TECNICO_2XDIA'])));
             $inicio_tec_03xdia=join("/", array_reverse(explode("-", $row['INICIO_TECNICO_3XDIA'])));
             $fim_tec_03xdia=join("/", array_reverse(explode("-", $row['FIM_TECNICO_3XDIA'])));
            $inicio_tec_v48h=join("/", array_reverse(explode("-", $row['INICIO_TECNICO_V48H'])));
            $fim_tec_v48h=join("/", array_reverse(explode("-", $row['FIM_TECNICO_V48H'])));
            $inicio_tec_v72h=join("/", array_reverse(explode("-", $row['INICIO_TECNICO_V72H'])));
            $fim_tec_v72h=join("/", array_reverse(explode("-", $row['FIM_TECNICO_V72H'])));
            

                 
             }
             
             $disabled1 ="disabled='disabled'";
             $style="";
             
         }else{
             $disabled1 ="";
             $style="style='display:none;'";
         }
         
         echo "<br/><div style='width:100%;' id='div-form-admissao'>";
         echo"<form>";
         
         $disabled ="disabled='disabled'";
         echo alerta();
         echo  "<p><label for='data_ad'><b>Previsão de Admiss&atilde;o: </b></label><input type='text' name='data_ad'  $disabled1 id='data_ad' value='$data_ad' maxlength='10' size='10'  class='OBG data ' />";
         echo "<fieldset style='width:50%;'><legend><b>Modalidade de Assist&ecirc;ncia Autorizada</b></legend>";
         echo"<p><input $disabled1 type='checkbox' value='1' ".(($atendimento_domiciliar=='1')?"checked":"")." name='atendimento_domiciliar' id='atendimento_domiciliar' /><label for='atendimento_domiciliar'><b>Atendimento Domiciliar</b></label>";
         echo"&nbsp;&nbsp;&nbsp; <label for='inicio_atendimento'>inicio</label><input type='text' name='inicio_atendimento' id='inicio_atendimento' value='{$inicio_atendimento}' maxlength='10' size='10'  $disabled class='data ' /> ";
         echo " at&eacute;  ";
         echo"<label for='fim_atendimento'>inicio</label><input type='text' name='fim_atendimento' $disabled id='fim_atendimento' value='$fim_atendimento' maxlength='10' size='10'  class='  data' /></p>";
         echo"<p><input type='checkbox' value='1' ".(($internacao_domiciliar=='1')?"checked":"")." $disabled1 name='internacao_domiciliar' id='internacao_domiciliar' /><label for='internacao_domiciliar'><b>Internacao Domiciliar</b></label>";
         echo"&nbsp;&nbsp;&nbsp; <label for='inicio_internacao'>inicio</label><input type='text' $disabled  name='inicio_internacao' id='inicio_internacao' value='$inicio_internacao' maxlength='10' size='10'  class='  data ' /> ";
         echo "  at&eacute;  ";
         echo"<label for='fim_internacao'>inicio</label><input type='text' name='fim_internacao' value='$fim_internacao' $disabled  maxlength='10' size='10'  id='fim_internacao' class='  data' /></p>";
         echo "</fieldset>";
         
         ///técnico de enfermagem
         echo "<fieldset style='width:50%;'><legend><input type='checkbox' $disabled1 value='1'".(($tec_enfermagem=='1')?"checked":"")." id='tec_enfermagem' name='tec_enfermagem'><label for='tec_enfermagem'><b>T&eacute;cnico de Enfermagem</b></label></legend>";
         echo"<span id='span_tec_enfermagem' {$style}><p><input type='checkbox' $disabled1 name='tec_06' value='1'".(($tec_06=='1')?"checked":"")." id='tec_06' /><label for='tec_06'><b>Interna&ccedil;&atilde;o 06h</b></label>";
         echo"&nbsp;&nbsp;&nbsp; <label for='inicio_tec_06'>inicio</label><input type='text' $disabled  name='inicio_tec_06' value='$inicio_tec_06' maxlength='10' size='10' id='inicio_tec_06' class='  data ' /> ";
         echo " at&eacute;  ";
         echo"<label for='fim_tec_06'>fim</label><input type='text' name='fim_tec_06' value='$fim_tec_06' $disabled  maxlength='10' size='10' id='fim_tec_06' class='  data' /></p>";
         
         echo"<p><input type='checkbox' value='1'".(($tec_12=='1')?"checked":"")."  $disabled1 name='tec_12' id='tec_12' /><label for='tec_12'><b>Interna&ccedil;&atilde;o 12h</b></label>";
         echo"&nbsp;&nbsp;&nbsp; <label for='inicio_tec_12'>inicio</label><input type='text' $disabled  name='inicio_tec_12' id='inicio_tec_12' value='$inicio_tec_12' maxlength='10' size='10'  class='  data ' /> ";
         echo "  at&eacute;  ";
         echo"<label for='fim_tec_12'>fim</label><input type='text' name='fim_tec_12' value='$fim_tec_12' $disabled  maxlength='10' size='10'  id='fim_tec_12' class='  data' /></p>";
         
         echo"<p><input type='checkbox' value='1' ".(($tec_24=='1')?"checked":"")." $disabled1 name='tec_24' id='tec_24' /><label for='tec_24'><b>Interna&ccedil;&atilde;o 24h</b></label>";
         echo"&nbsp;&nbsp;&nbsp; <label for='inicio_tec_24'>inicio</label><input type='text' $disabled name='inicio_tec_24' id='inicio_tec_24' value='$inicio_tec_24' maxlength='10' size='10'  class='  data ' /> ";
         echo "  at&eacute;  ";
         echo"<label for='fim_tec_24'>fim</label><input type='text' name='fim_tec_24' value='$fim_tec_24' $disabled  maxlength='10' size='10'  id='fim_tec_24' class='  data' /></p>";
         
         echo"<p><input type='checkbox' value='1' ".(($tec_01xdia=='1')?"checked":"")." $disabled1 name='tec_01xdia' id='tec_01xdia' /><label for='tec_01xdia'><b>01 vez ao dia</b></label>";
         echo"&nbsp;&nbsp;&nbsp; <label for='inicio_tec_01xdia'>inicio</label><input type='text' $disabled name='inicio_tec_01xdia' id='inicio_tec_01xdia' value='$inicio_tec_01xdia' maxlength='10' size='10'  class='  data ' /> ";
         echo "  at&eacute;  ";
         echo"<label for='fim_tec_01xdia'>fim</label><input type='text' name='fim_tec_01xdia' value='$fim_tec_01xdia' $disabled maxlength='10' size='10'  id='fim_tec_01xdia' class='  data' /></p>";

        echo"<p><input type='checkbox' value='1' ".(($tec_02xdia=='1')?"checked":"")." $disabled1 name='tec_02xdia' id='tec_02xdia' /><label for='tec_01xdia'><b>02 vez ao dia</b></label>";
        echo"&nbsp;&nbsp;&nbsp; <label for='inicio_tec_01xdia'>inicio</label><input type='text' $disabled name='inicio_tec_02xdia' id='inicio_tec_02xdia' value='$inicio_tec_02xdia' maxlength='10' size='10'  class='  data ' /> ";
        echo "  at&eacute;  ";
        echo"<label for='fim_tec_02xdia'>fim</label><input type='text' name='fim_tec_02xdia' value='$fim_tec_02xdia' $disabled maxlength='10' size='10'  id='fim_tec_02xdia' class='  data' /></p>";

        echo"<p><input type='checkbox' value='1' ".(($tec_03xdia=='1')?"checked":"")." $disabled1 name='tec_03xdia' id='tec_03xdia' /><label for='tec_03xdia'><b>03 vez ao dia</b></label>";
        echo"&nbsp;&nbsp;&nbsp; <label for='inicio_tec_03xdia'>inicio</label><input type='text' $disabled name='inicio_tec_03xdia' id='inicio_tec_03xdia' value='$inicio_tec_03xdia' maxlength='10' size='10'  class='  data ' /> ";
        echo "  at&eacute;  ";
        echo"<label for='fim_tec_03xdia'>fim</label><input type='text' name='fim_tec_03xdia' value='$fim_tec_03xdia' $disabled maxlength='10' size='10'  id='fim_tec_03xdia' class='  data' /></p>";


        echo"<p><input type='checkbox' value='1'".(($tec_v48h=='1')?"checked":"")." $disabled1 name='tec_v48h' id='tec_v48h' /><label for='tec_v48h'><b>Visita a cada 48h</b></label>";
         echo"&nbsp;&nbsp;&nbsp; <label for='inicio_tec_v48h'>inicio</label><input type='text' $disabled name='inicio_tec_v48h' id='inicio_tec_v48h' value='$inicio_tec_v48h' maxlength='10' size='10'  class='  data ' /> ";
         echo "  at&eacute;  ";
         echo"<label for='fim_tec_v48h'>fim</label><input type='text' name='fim_tec_v48h' value='$fim_tec_v48h' $disabled  maxlength='10' size='10'  id='fim_tec_v48h' class='  data' /></p>";
         
         echo"<p><input type='checkbox' value='1'".(($tec_v72h=='1')?"checked":"")." $disabled1 name='tec_v72h' id='tec_v72h' /><label for='tec_v72h'><b>Visita a cada 72h</b></label>";
         echo"&nbsp;&nbsp;&nbsp; <label for='inicio_tec_v72h'>inicio</label><input type='text' $disabled  name='inicio_tec_v72h' id='inicio_tec_v72h' value='$inicio_tec_v72h' maxlength='10' size='10'  class='  data ' /> ";
         echo "  at&eacute;  ";
         echo"<label for='fim_tec_v72h'>fim</label><input type='text' name='fim_tec_v72h' value='$fim_tec_v72h' $disabled maxlength='10' size='10'  id='fim_tec_v72h' class='  data' /></p>";
         echo "</span></fieldset>";
         ///Visita médica         
         echo "<fieldset style='width:50%;'><legend><input type='checkbox' value='1'".(($v_medica=='1')?"checked":"")." $disabled1 id='v_medica' name='v_medica'><label for='v_medica'><b>Visita Médica</b></label></legend>";
        /* echo "<p><input type='radio' name='frequencia_v_medica' value='1' maxlength='10' size='10'  id='v_med_01xsemana'  /><label for='v_med_01xsemana'>01 x semana</label>";
         echo "&nbsp;&nbsp;&nbsp;<input type='radio' name='frequencia_v_medica' value='2' maxlength='10' size='10'  id='v_med_01xquizenal'  /><label for='v_med_01xquizenal'>01 x quizenal</label>";
         echo "&nbsp;&nbsp;&nbsp;<input type='radio' name='frequencia_v_medica' value='3' maxlength='10' size='10'  id='v_med_01xmensal'  /><label for='v_med_01xmensal'>01 x mensal</label>";
         */
         echo "<p><label for='fmedica' >Frequ&ecirc;ncia: </label>";
         $nome='fmedica';
         echo $this->frequencia($nome,$fmedica);
         echo "</p></fieldset>";

        ///Visita nutricionista
        echo "<fieldset style='width:50%;'>
                <legend><input type='checkbox' value='1'".(($v_nutricionista=='1')?"checked":"")." $disabled1 id='v_nutricionista' name='v_nutricionista'>
                <label for='v_nutricionista'><b>Visita Nutricionista</b></label></legend>";
        echo "<p><label for='f_visita_nutricionista' >Frequ&ecirc;ncia: </label>";
        $nome='f_visita_nutricionista';
        echo $this->frequencia($nome,$fVisitaNutricionista);
        echo "</p></fieldset>";

        //visita enfermeiro
          echo "<fieldset style='width:50%;'><legend><input type='checkbox' value='1'".(($v_enfermagem=='1')?"checked":"")." $disabled1 id='v_enfermagem' name='v_enfermagem'><label for='v_enfermagem'><b>Visita Enfermeiro(a)</b></label></legend>";
        /* echo "<p><input type='radio' name='frequencia_v_enfermagem' value='1' maxlength='10' size='10'  id='v_enf_01xsemana'  /><label for='v_enf_01xsemana'>01 x semana</label>";
         echo "&nbsp;&nbsp;&nbsp;<input type='radio' name='frequencia_v_enfermagem' value='2' maxlength='10' size='10'  id='v_enf_01xquizenal'  /><label for='v_enf_01xquizenal'>01 x quizenal</label>";
         echo "&nbsp;&nbsp;&nbsp;<input type='radio' name='frequencia_v_enfermagem' value='3' maxlength='10' size='10'  id='v_enf_01xmensal'  /><label for='v_enf_01xmensal'>01 x mensal</label>";
         */
          echo "<p><label for='fenfermagem' >Frequ&ecirc;ncia: </label>";
          $nome='fenfermagem';
          echo $this->frequencia($nome,$fenfermagem);
         echo "</p></fieldset>";
        
         ///fisioterapia
         echo "<fieldset style='width:50%;'><legend><input type='checkbox' value='1'".(($fisioterapia=='1')?"checked":"")." $disabled1 id='fisioterapia' name='fisioterapia'><label for='fisioterapia'><b>Sess&atilde;o Fisioterapia</b></label></legend>";
         echo"<span id='span_fisioterapia' $style>";
         echo "<p><input type='checkbox' name='respiratoria' value='1'".(($respiratoria=='1')?"checked":"")." $disabled1 maxlength='10' size='10'  id='respiratoria'  /><label for='respiratoria'>Respirat&oacute;ria</label> <input name='respiratoria_sessao' $disabled id='respiratoria_sessao' type='text' value='$respiratoria_sessao' /><label for='respiratoria_sessao'>N° Sess&atilde;o/semana</label>";
         echo "<p><input type='checkbox' name='motora' value='1'".(($motora=='1')?"checked":"")." $disabled1 maxlength='10' size='10'  id='motora'  /><label for='motora'>Motora</label> <input name='motora_sessao' $disabled id='motora_sessao' type='text' value='$motora_sessao' /><label for='motora_sessao'>N° Sess&atilde;o/semana</label>";
         echo "</p></span></fieldset>";
         ///fonoaudiologo
         echo "<fieldset style='width:50%;'><legend><input type='checkbox' value='1'".(($fonoterapia=='1')?"checked":"")." $disabled1 id='fonoterapia' name='fonoterapia'><label for='fonoterapia'><b>Sess&atilde;o Fonoterapia</b></label></legend>";
         echo"<span id='span_fonoterapia' $style>";
         echo "</p>&nbsp;&nbsp;&nbsp;<input type='checkbox' name='fonoterapia_av' value='1'".(($fonoterapia_av=='1')?"checked":"")." $disabled1 maxlength='10' size='10'  id='fonoterapia_av'  /><label for='fonoterapia_av'>Avalia&ccedil;&atilde;o</label>";
         echo "&nbsp;&nbsp;&nbsp;<input type='text' name='fonoterapia_sessao' value='$fonoterapia_sessao' maxlength='10' size='10'  $disabled  id='fonoterapia_sessao'  /><label for='fonoterapia_sessao'>sess&atilde;o /semana</label>";
         echo "</p></span></fieldset>";
         ///Nutricionista
         echo "<fieldset style='width:50%;'><legend><input type='checkbox' value='1'".(($nutricionista=='1')?"checked":"")." $disabled1  id='nutricionista' name='nutricionista'><label for='nutricionista'><b>Nutricionista</b></label></legend>";
        /* echo "<p><input type='radio' name='nutricionista_frequencia' value='1' maxlength='10' size='10'  id='nutricionista_av'  /><label for='nutricionista_av'>Avalia&ccedil;&atilde;o</label>";
         echo "&nbsp;&nbsp;&nbsp;<input type='radio' name='nutricionista_frequencia' value='2' maxlength='10' size='10'  id='nutricionista_mesal'  /><label for='nutricionista_mesal'>01x Mensal</label>";
         */
         echo "<p><label for='fnutricionista' >Frequ&ecirc;ncia: </label>";
           $nome='fnutricionista';
         echo $this->frequencia($nome,$fnutricionista);
         echo "&nbsp;&nbsp;&nbsp;<input type='text' name='qtd_diaria_dieta' value='$qtd_diaria_dieta' $disabled maxlength='10' size='10'  id='qtd_diaria_dieta'  /><label for='qtd_diaria_dieta'>N° de di&aacute;ria de dieta aut.:</label>";
         echo "</p></fieldset>";
         ///psicologo
          echo "<fieldset style='width:50%;'><legend><input type='checkbox' value='1'".(($psicologo=='1')?"checked":"")." $disabled1  id='psicologo' name='psicologo'><label for='psicologo'><b>Pscic&oacute;logo</b></label></legend>";
         /*echo "<p><input type='radio' name='psicologo_frequencia' value='1' maxlength='10' size='10'  id='psicologo_av'  /><label for='psicologo_av'>Avalia&ccedil;&atilde;o</label>";
         echo "&nbsp;&nbsp;&nbsp;<input type='radio' name='psicologo_frequencia' value='2' maxlength='10' size='10'  id='psicologo_quizenal'  /><label for='psicologo_quizenal'>01x Quizenal</label>";
         echo "&nbsp;&nbsp;&nbsp;<input type='text' name='psicologo_sessao' value='' maxlength='10' size='10'  id='psicologo_sessao'  /><label for='psicologo_sessao'>N° de sess&atilde;o</label>";
         */
          echo "<p><label for='fpsicologo' >Frequ&ecirc;ncia:</label>";
           $nome='fpsicologo';
           echo $this->frequencia($nome,$fpsicologo);
           echo "&nbsp;&nbsp;&nbsp;<input type='text' name='psicologo_sessao' $disabled value='$psicologo_sessao' maxlength='10' size='10'  id='psicologo_sessao'  /><label for='psicologo_sessao'>N° de sess&atilde;o/semana</label>";
           echo "</p></fieldset>";
          
         //médico especialista
        echo "<fieldset style='width:50%;'><legend><input type='checkbox' value='1'".(($medico_especialista=='1')?"checked":"")." $disabled1  id='medico_especialista' name='medico_especialista'><label for='medico_especialista'><b>Médico Especialista</b></label></legend>";
        echo "<p><label for='med_especialidade'>Especialidade: </label><input type='text' name='med_especialidade' size='50%' value='$med_especialidade' $disabled id='med_especialidade'/></p>";
        echo "</fieldset>"; 
         ///equipamentos
        echo "<fieldset style='width:50%;'><legend><b>Equipamentos</b></legend>";
        $equipamento = '';
        if($tipo==1){
        $sql="select COBRANCAPLANOS_ID from equipamentos_admissao_paciente_secmed where FICHA_ADMISSAO_PACIENTE_SECMED_ID={$id}";
        $result = mysql_query($sql);
		
		//echo"<tr bgcolor='grey'><td><b>Item</b></td><td><b>Observa&ccedil;&atilde;o</b></td><td><b>Qtd.</b></td></tr>";
		while($row=mysql_fetch_array($result)){
			$equipamento[$row[COBRANCAPLANOS_ID]]=$row[COBRANCAPLANOS_ID];
		}
        }
        $sql = "select * from cobrancaplanos where tipo=1";

            $result = mysql_query($sql);

            
            while ($row = mysql_fetch_array($result)) {
                if(array_key_exists($row['id'], $equipamento)){
					$check = "checked='checked'";
					

				}else{
					$check ='';
					
				}
                echo"<p><input type='checkbox' class='equipamento' $disabled1 $check cod_equipamento='{$row['id']}' />{$row['item']}</p>";
            }
        echo "</fieldset>"; 
         
        echo "<fieldset style='width:50%;'><legend><label for='observacao'><b>Observa&ccedil;&atilde;o</b></label></legend>";
        echo "<textarea cols='70' rows='4' id='observacao'  $disabled1 name='observacao'>{$observacao}</textarea>";
        echo "</fieldset>"; 
        echo"</form>";
        echo alerta();
        if($tipo==0){
        echo "<button id='salvar-ficha-admissao' paciente_id='{$paciente_id}' hist_status_id='{$id}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
        }else{
            echo"<form method=post target='_blank' action='imprimir_ficha_admissao_paciente.php?id={$id}'>";
			
			echo "<button  target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
					<span class='ui-button-text'>Imprimir</span></button>";
			echo "</form>";
        }
         
         
         echo "</div>";
         
    }
    
    public function visualizar_ficha_admissao($id){

        $sql      = "
        select 
        fa.ID,
        u.nome, 
        DATE_FORMAT(fa.DATA,'%d/%m/%Y %H:%i') as dat 
        from
         ficha_admissao_paciente_secmed as fa inner join 
         usuarios as u on (fa.USUARIO_ID=u.idUsuarios)
          where 
          fa.PACIENTE_ID ={$id} 
          and CANCELED_BY is null
           order by fa.ID desc";
        $result   = mysql_query($sql);
        $num_rows = mysql_num_rows($result);

        if ($num_rows > 0) {
          echo"<h1><center>Ficha de Admiss&atilde;o do Paciente.</center></h1>";
          echo $this->cabecalho_admissao(null, 1, $id);
          echo "<div>";
          if ($_SESSION['empresa_principal'] == 1) {
              echo"<form method='GET'  action='?adm=formulario_admissao&msg=null&tipo=0&pacienteId=$id'>";
              echo "<input type='hidden'  name='msg' value=''/>
                    <input type='hidden' name='tipo' value='0' />
                    <input type='hidden' name='adm' value='formulario_admissao' />
                    <input type='hidden' name='pacienteId' value='$id' />";
            
              echo "<button  target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
                  <span class='ui-button-text'>Novo</span></button>";
              echo "</form>";
          }
          echo "</div>
                <br>";
          echo "<table class='mytable' width=95% >";
          echo "<thead>
                    <tr>";
                    echo "<th><b>Criador</b></th>";
                    echo "<th><b>Criado em</b></th>";                    
                    echo "<th ><b>Visualizar</b></th>";
                    if ($_SESSION['empresa_principal'] == 1) {
                        echo "<th  ><b>Cancelar</b></th>";
                    }
                    
                    
                    
                    echo "</tr>
                </thead>";
          while ($row = mysql_fetch_array($result)) {
            echo"<tr>
                    <td>{$row['nome']}</td>
                    <td>{$row['dat']}</td>
                    <td>
                        <a href=?adm=formulario_admissao&msg={$row['ID']}&tipo=1><img src='../utils/details_16x16.png' title='Visualizar' border='0'></a>
                    </td>";
                    if ($_SESSION['empresa_principal'] == 1) {
                 echo "<td>
                         <img class='cancelar-admissao'  id-admissao='{$row['ID']}' src='../utils/delete_16x16.png' title='Cancelar Admissão' style='cursor:pointer;' border='0'/>
                     </td>";
                    }
           echo "</tr>";
          }
          echo "</table>";
        } else {
            if ($_SESSION['empresa_principal'] == 1) {
                echo $this->form_admissao(null, 0, $id);
            }else{
                echo "<div>
                            <center>
                                <h3>
                                    Paciente não possui Ficha de Admissão no sistema, 
                                    favor entrar em contato com setor responsável.
                                </h3>
                            </center>
                        </div>";
            }
        }
    }

}

?>
