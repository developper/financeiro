<?php
include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');
include_once('paciente.php');

class Historico_paciente {
    
 public $cabecalho;
 function __construct() {
      $this->cabecalho = new Paciente();
      
 }
    public function visualizar_historico_status($id){
         echo"<h1><center>Hist&oacute;rico de Status do Paciente.</center></h1>";
       $a = $this->cabecalho; 
         
       echo  $a->cabecalho($id);
        
       $sql="SELECT 
           u.nome, 
           s.status, 
           DATE_FORMAT( h.DATA, '%d/%m/%Y %h:%i:%s' ) as dat,
           DATE_FORMAT( h.DATA_ADMISSAO, '%d/%m/%Y %h:%i:%s' ) as dat_ad,
           DATE_FORMAT( h.DATA_AUTORIZACAO, '%d/%m/%Y %h:%i:%s' )as dat_int,
           h.OBSERVACAO,
           (CASE h.TIPO 
           when 1 then 'Admissão' 
           when 2 then 'reinternação' 
           when -1 then '' 
            end) as tip
            FROM historico_status_paciente AS h
            INNER JOIN usuarios AS u ON ( h.USUARIO_ID = u.idUsuarios )
            INNER JOIN statuspaciente AS s ON ( h.STATUS_PACIENTE_ID = s.id )
            WHERE
            PACIENTE_ID ={$id}
            order by h.ID desc";
             echo "<table class='mytable' width=95% >";
		echo "<thead><tr>";
		echo "<th><b>USUARIO</b></th>";
		echo "<th ><b>DATA</b></th>";
                echo "<th ><b>D.ADMISSÂO</b></th>";
                echo "<th ><b>D.INTERNAÇÂO</b></th>";
                echo "<th ><b>STATUS</b></th>";
                echo "<th ><b>TIPO</b></th>";
                echo "<th ><b>OBSERVECAO</b></th>";
               echo "</tr></thead>";
               
               $result= mysql_query($sql);
               while ($row = mysql_fetch_array($result)) {
                   echo"<tr><td>{$row['nome']}</td><td>{$row['dat']}</td><td>{$row['dat_ad']}</td><td>{$row['dat_int']}</td><td>{$row['status']}</td><td>{$row['tip']}</td><td>{$row['OBSERVACAO']}</td></tr>";
               }
               
               echo "</table>";
        echo"<form method=post target='_blank' action='imprimir_historico_status.php?id={$id}'>";
	echo "<button  target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'><span class='ui-button-text'>Imprimir</span></button>";
	echo "</form>";
            
        
    }
    public function visualizar_historico_plano_liminar($id){
         echo"<h1><center>Hist&oacute;rico de Plano e Liminar.</center></h1>";
        $a = $this->cabecalho; 
       echo $a->cabecalho($id);
       $sql=" SELECT 
                u.nome as user,
                p.nome as plano,
                (case h.LIMINAR
                when 0 then 'Não'
                when 1 then 'Sim'
                when -1 then ''
                end ) as lim,
                DATE_FORMAT(h.DATA, '%d/%m/%Y %h:%i:%s') as dat

                FROM `historico_plano_liminar_paciente` as h inner join 
                usuarios as u on (h.USUARIO_ID=u.idUsuarios) inner join 
                planosdesaude as p on (h.PLANO_ID=p.id)
                WHERE 
                `PACIENTE_ID`={$id}";
          echo "<table class='mytable' width=95% >";
		echo "<thead><tr>";
		echo "<th><b>USUARIO</b></th>";
		echo "<th ><b>DATA</b></th>";
                echo "<th ><b>PLANO</b></th>";
                echo "<th ><b>LIMINAR</b></th>";
                echo "</tr></thead>";
                $result= mysql_query($sql);
               while ($row = mysql_fetch_array($result)) {
                   echo"<tr><td>{$row['user']}</td><td>{$row['dat']}</td><td>{$row['plano']}</td><td>{$row['lim']}</td></tr>";
               }
               echo "</table>";
               echo"<form method=post target='_blank' action='imprimir_historico_plano_liminar.php?id={$id}'>";
	echo "<button  target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'><span class='ui-button-text'>Imprimir</span></button>";
	echo "</form>";
    }
}
    

?>
