<?php
$id = session_id();
if(empty($id))
  session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('paciente.php');

class imprimir_cadastro_paciente{
    
    
    function detalhes_paciente($id){
        
        $id = anti_injection($id,"numerico");
	$result = mysql_query("SELECT c.*, p.id as plano, cid.codigo as codcid, cid.descricao as descid, c.CIDADE_ID_UND, c.CIDADE_ID_INT, c.CIDADE_ID_DOM,concat(cd.NOME,',',cd.UF) as cidade_und,
				(select  concat(x.NOME,',',x.UF) from cidades as x where x.id=c.CIDADE_ID_INT) as cidade_int,
				(select  concat(x1.NOME,',',x1.UF) from cidades as x1 where x1.id=c.CIDADE_ID_DOM) as cidade_dom,
                                p.nome as plano,
                                ( case c.LIMINAR when 0 then 'N&atilde;o' else 'Sim' end) as limi,
                                DATE_FORMAT(c.dataInternacao, '%d/%m/%Y %h:%i:%s') as data,
                                (select RELACIONAMENTO FROM relacionamento_pessoa as rp where rp.ID=c.relresp) as rel_responsavel,
                                (select RELACIONAMENTO FROM relacionamento_pessoa as rp where rp.ID=c.relcuid) as rel_cuidador
                                
				FROM clientes as c
				LEFT OUTER JOIN planosdesaude as p ON c.convenio = p.id
				LEFT OUTER JOIN cid10 as cid ON c.diagnostico collate utf8_general_ci = cid.codigo collate utf8_general_ci
				LEFT JOIN cidades cd ON c.CIDADE_ID_UND = cd.ID 
                                
                                

				WHERE idClientes = '$id'");
		$dados = new DadosPaciente();
		
		while($row = mysql_fetch_array($result)){
                    
			$dados->codigo = $row['codigo']; 
                        $dados->dataInternacao = $row['data'];
                        $dados->nome = $row['nome']; 
                        $dados->sexo = $row['sexo']; 
                        $dados->plano = $row['plano'];
			$dados->diagnostico = $row['descid']; 
                        $dados->cid = $row['codcid'];
                        $dados->origem = $row['localInternacao'];  
                        $dados->leito = $row['LEITO'];
			$dados->solicitante = $row['medicoSolicitante']; 
                        $dados->especialidade = $row['espSolicitante']; 
                        $dados->acomp = $row['acompSolicitante'];
			$dados->cnome = $row['cuidador'];
                        $dados->csexo = $row['csexo'];
                        $dados->relcuid = $row['rel_cuidador'];
			$dados->rnome = $row['responsavel']; 
                        $dados->rsexo = $row['rsexo']; 
                        $dados->relresp = $row['rel_responsavel'];
			$dados->endereco = $row['endereco']; 
                        $dados->bairro = $row['bairro']; 
                        $dados->referencia = $row['referencia'];  
                        $dados->telPosto = $row['TEL_POSTO'];  
                        $dados->contato = $row['PES_CONTATO_POSTO'];
			$dados->empresa = $row['empresa'];
                        $dados->telResponsavel = $row['TEL_RESPONSAVEL']; 
                        $dados->telCuidador = $row['TEL_CUIDADOR'];
			$dados->enderecoInternacao = $row['END_INTERNADO'];
                        $dados->bairroInternacao = $row['BAI_INTERNADO']; 
                        $dados->referenciaInternacao = $row['REF_ENDERECO_INTERNADO'];
			$dados->telInternacao = $row['TEL_LOCAL_INTERNADO']; 
                        $dados->telDomiciliar = $row['TEL_DOMICILIAR'];
                        $dados->especialidadeCuidador = $row['CUIDADOR_ESPECIALIDADE'];
			$dados->especialidadeResponsavel = $row['RESPONSAVEL_ESPECIALIDADE'];
                        $dados->id_cidade_und = $row['CIDADE_ID_UND'];
                        $dados->cidade_und = $row['cidade_und'];
			$dados->id_cidade_internado = $row['CIDADE_ID_INT'];
                        $dados->cidade_internado = $row['cidade_int'];
			$dados->id_cidade_domiciliar = $row['CIDADE_ID_DOM'];
                        $dados->cidade_domiciliar = $row['cidade_dom']; 
                        $dados->end_und_origem =$row['END_UND_ORIGEM'];
			$dados->num_matricula_convenio =$row['NUM_MATRICULA_CONVENIO'];
                        ///jeferson 2-10-2013 novo campo
                        $dados->liminar =$row['limi'];
                        $dados->plano = $row['plano'];
            $dados->cpfCliente =$row['cpf'];
            $dados->cpfResponsavel =$row['cpf_responsavel'];
            $dados->nascResponsavel = implode("/",array_reverse(explode("-",$row['nascimento_responsavel'])));
            $dados->nascCuidador = implode("/",array_reverse(explode("-",$row['nascimento_cuidador'])));
			$dados->nasc = implode("/",array_reverse(explode("-",$row['nascimento'])));
                       
		}
$html.= "<table class='mytable' >";
$html.= "<tr><td colspan ='3'><h2><a><img src='../utils/logo2.jpg' width='150' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>Ficha de Cadastro do Paciente.</h2></td></tr>";
if($dados->sexo == 1){
    $sexo = "Feminino";
    
}else{
    $sexo = "Masculino";
}

$html .= "<tr><td colspan='3' style='border:solid'><center><b>Paciente</b></center></td></tr>";
$html.= "<tr><td colspan='2'><b>Nome:</b></td><td><b>Sexo:</b></td></tr>";
$html.= "<tr><td colspan='2'> {$dados->nome}</td><td>{$sexo}</td></tr>";
 $html.= "<tr><td colspan='2'><b>CPF:</b></td><td><b>Data Nascimento:</b></td></tr>";
$html.= "<tr><td colspan='2'> {$dados->cpfCliente}</td><td>{$dados->nasc}</td></tr>";
$html .= "<tr><td colspan='2'><b>Conv&ecirc;nio:</b></td><td><b>N&deg; de Matr&iacute;cula do Conv&ecirc;nio:</b></td></tr>";
$html .= "<tr><td colspan='2'>$dados->plano</td><td>$dados->num_matricula_convenio</td></tr>";
$html .= "<tr><td><b>Liminar:</b></td><td><b>C&oacute;digo</b></td><td><b>Data da Solicita&ccedil;&atilde;o</b></tr>";
$html .= "<tr><td>$dados->liminar</td><td>$dados->codigo</td><td>$dados->dataInternacao </td></tr>";
////////////
$html .= "<tr><td colspan='3' style='border:solid'><b><center>Diagn&oacute;stico Principal</center></b></td></tr>";
$html .= "<tr><td><b>M&eacute;dico solicitante:</b></td><td><b>Especialidade</b></td><td><b>Acompanhar&aacute; paciente em casa.</b></td></tr>";
$html .= "<tr><td>{$dados->solicitante}</td><td>{$dados->especialidade}</td><td>{$dados->acomp}</td></tr>";
$html .= "<tr><td colspan='2'><b>Diagn&oacute;stico:</b></td><td><b>C&oacute;digo:</b></td></tr>";
$html .= "<tr><td colspan='2'>{$dados->diagnostico}</td><td>{$dados->cid}</td>></tr>";
/////////////////////
$html .= "<tr><td colspan='3' style='border:solid' ><center><b>Unidade de Interna&ccedil;&atilde;o de Origem</b></center></td></tr>";
$html .= "<tr><td colspan='2'><b>Nome:</b></td><td><b>Leito:</b></td><td><b></b></td></tr>";
$html .= "<tr><td colspan='2'>{$dados->origem}</td><td>{$dados->leito}</td></tr>";
$html .= "<tr><td colspan='2'><b>Endere&ccedil;o:</b></td><td><b>Cidade:</b></td></tr>";
$html .= "<tr><td colspan='2'>{$dados->end_und_origem}</td><td>{$dados->cidade_und}</td></tr>";
$html .= "<tr><td colspan='2'><b>Telefone do posto de enfermagem</b></td><td><b>Pessoa para contato</b></td></tr>";
$html .= "<tr><td colspan='2'>{$dados->telPosto}</td><td>{$dados->contato}</td></tr>";
/////
if($dados->rsexo == 1){
    $rsexo = "Feminino";
    
}else{
    $rsexo = "Masculino";
}
$html .= "<tr><td colspan='3' style='border:solid' ><center><b>Respons&aacute;vel Familiar</b></center></td></tr>";
$html .= "<tr><td colspan='2'><b>Nome:</b></td><td><b>Sexo</b></td></tr>";
$html .= "<tr><td colspan='2'>{$dados->rnome}</td><td>$rsexo</td></tr>";
$html .= "<tr><td colspan='2'><b>Nascimento:</b></td><td><b>CPF</b></td></tr>";
$html .= "<tr><td colspan='2'>{$dados->nascResponsavel}</td><td>{$dados->cpfResponsavel}</td></tr>";
$html .= "<tr><td colspan='2'><b>Rela&ccedil;&atilde;o:</b></td><td><b>Telefone:</b></td></tr>";
$html .= "<tr><td colspan='2'>".$dados->relresp."(".$dados->especialidadeResponsavel.")</td><td>$dados->telResponsavel</td></tr>";
////
if($dados->csexo == 1){
    $csexo = "Feminino";
    
}else{
    $csexo = "Masculino";
}
$html .= "<tr><td colspan='3' style='border:solid' ><center><b>Cuidador Familiar</b></center></td></tr>";
$html .= "<tr><td colspan='2'><b>Nome:</b></td><td><b>Sexo</b></td></tr>";
$html .= "<tr><td colspan='2'>{$dados->cnome}</td><td>$csexo</td></tr>";
$html .= "<tr><td><b>Nascimento</b></td></td><td ><b>Rela&ccedil;&atilde;o:</b></td><td><b>Telefone:</b></td></tr>";
$html .= "<tr><td>{$dados->nascCuidador}</td><td >".$dados->relcuid."(".$dados->especialidadeCuidador.")</td><td>$dados->telCuidador</td></tr>";
/////////////
$html .= "<tr><td colspan='3' style='border:solid' ><center><b>Domicílio do Paciente</b></center></td></tr>";
$html .= "<tr><td colspan='2'><b>Endere&ccedil;o:</b></td><td><b>Bairro:</b></td></tr>";
$html .= "<tr><td colspan='2'>{$dados->endereco}</td><td>{$dados->bairro}'</td></tr>";
$html .= "<tr><td ><b>Telefone:</b></td><td><b>Cidade:</b></td><td><b>Ponto de Referência</b></td></tr>";
$html .= "<tr><td >$dados->telDomiciliar</td><td>$dados->cidade_domiciliar'</td><td>$dados->referencia</td></tr>";
/////////////
$html .= "<tr><td colspan='3' style='border:solid' ><center><b>Local de Interna&ccedil;&atilde;o</b></center></td></tr>";
$html .= "<tr><td colspan='2'><b>Endere&ccedil;o:</b></td><td><b>Bairro:</b></td></tr>";
$html .= "<tr><td colspan='2'>{$dados->enderecoInternacao}</td><td>{$dados->bairroInternacao}'</td></tr>";
$html .= "<tr><td ><b>Telefone:</b></td><td><b>Cidade:</b></td><td><b>Ponto de Referência</b></td></tr>";
$html .= "<tr><td >$dados->telInternacao</td><td>$dados->cidade_internado'</td><td>$dados->referenciaInternacao</td></tr>";
$html.= "</table>";
        
       $paginas []= $header.$html.= "</form>";
	
	
	//print_r($paginas);
	//print_r($paginas);
	
	$mpdf=new mPDF('pt','A4',10);
	$mpdf->SetHeader('página {PAGENO} de {nbpg}');
	$ano = date("Y");
	$mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
	$mpdf->WriteHTML("<html><body>");
	$flag = false;
	foreach($paginas as $pag){
		if($flag) $mpdf->WriteHTML("<formfeed>");
		$mpdf->WriteHTML($pag);
		$flag = true;
	}
	$mpdf->WriteHTML("</body></html>");
	$mpdf->Output('pacientes.pdf','I');
	exit;
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
}





$p = new imprimir_cadastro_paciente();
$p->detalhes_paciente($_GET['id']);
?>
