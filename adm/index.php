<?php
include_once('../validar.php');
require_once('usuario.php');
require_once('paciente.php');
require_once('planos.php');
require_once('filiais.php');
require_once('descricoes.php');
require_once('form_admissao_paciente.php');
require_once('liberar_prescricao_avaliacao.php');
require_once('historico_paciente.php');
require_once('reparar.php');

require_once('importar_assinatura.php');

if(!function_exists('redireciona')){
	function redireciona($link){
		if ($link==-1){
			echo" <script>history.go(-1);</script>";
		}else{
			echo" <script>document.location.href='$link'</script>";
		}
	}
}

include($_SERVER['DOCUMENT_ROOT'].'/cabecalho.php');
?>
<script src="adm.js?v=24" type="text/javascript" charset="ISO-8859-1"></script>
<script type="text/javascript">
function ConfirmChoice(acao,id,cod,cmd)
{
  ok = confirm("Deseja realmente " + acao + " "+ cod +" ?")
  if (ok != 0){
    location = window.location = cmd;
  }
}
</script>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/cabecalho_fim.php');
if(!(validar_tipo("modmed") || validar_tipo("modadm") || validar_tipo("modenf"))){
	redireciona('/inicio.php');
}
?>
</div>
<div id="content">
<div id="right">
<?php
if(isset($_POST['query'])){
	switch($_POST['query']){
		case "listar":
			$p = new Paciente();
			$p->listar(
				$_POST['empresa'],
				$_POST['nome_paciente'],
				$_POST['codigo_paciente'],
				$_POST['select_status'],
				$_POST['select_liminar'],
				$_POST['select_convenio']
			);
			break;

	}
}else{
	if(isset($_GET['adm'])){
		switch($_GET['adm']){
			case "formulario_admissao":
				$f= new Form_admissao_paciente();
				$f-> form_admissao($_GET['msg'],$_GET['tipo'],$_GET['pacienteId']);
				break;
			case "relatorios":
				if(isset($_GET['opcao'])){
					$v = new Historico_paciente();
					$f= new Form_admissao_paciente();
					switch($_GET['opcao']){

						case '1':
							$f->visualizar_ficha_admissao($_GET['id']);
							break;
						case '2':
							$v->visualizar_historico_status($_GET['id']);
							break;
						case '3':
							$v->visualizar_historico_plano_liminar($_GET['id']);
							break;
					}
				}else{
					$p = new Paciente();
					$p->menu_relatorio();
				}
				break;
			case "liberar":
				echo liberar();
				break;
			case "importar":
				echo assinatura($_GET['msg']);
				break;
			case "usr":
				if(!validar_adm() || $_SESSION['empresa_principal'] != 1) redireciona('/inicio.php');
				if(isset($_GET['act'])){
					switch($_GET['act']){
						case "listar":
							echo usr_list();
							break;
						case "novo":
							echo usr_new();
							break;
						case "editar":
							echo usr_edit($_POST);
							break;
						case "excluir":
							if(isset($_GET['confirm']) && ($_GET['confirm'] == "ok"))
								echo usr_del();
							break;
					}
				} else {echo usr_menu();}
				break;
			case "paciente":
				if(isset($_GET['act'])){
					switch($_GET['act']){
						case "listar":
							$p = new Paciente();
							$p->listar(-1,'','',-1,-1);
							break;
						case "novo":
							$p = new Paciente();
							$p->form();
							break;
						case "editar":
							if(isset($_GET['id'])){
								$p = new Paciente();
								$p->form($_GET['id']);
							}
							break;
						case "show":
							if(isset($_GET['id'])){
								$p = new Paciente();
								$p->form($_GET['id'],'readonly=readonly');
							}
							break;
						case "newcapmed":
							if(isset($_GET['id'])){
								$p = new Paciente();
								$p->nova_capitacao_medica($_GET['id']);
							}
							break;
						case "newfichaevolucao":
							if(isset($_GET['id'])){
								$p = new Paciente();
								$p->nova_evolucao_medica($_GET['id']);
							}
							break;
						case "listfichaevolucao":
							if(isset($_GET['id'])){
								$p = new Paciente();
								$p->listar_evolucao_medica($_GET['id']);
							}
							break;
						case "listcapmed":
							if(isset($_GET['id'])){
								$p = new Paciente();
								$p->listar_capitacao_medica($_GET['id']);
							}
							break;
						case "listavaliacaenfermagem":
							if(isset($_GET['id'])){
								$p = new Paciente();
								$p->listar_ficha_avaliacao_enfermagem($_GET['id']);
							}
							break;
						case "capmed":
							if(isset($_GET['id'])){
								$p = new Paciente();
								$p->detalhes_capitacao_medica($_GET['id']);
							}
							break;
						case "fichaevolucao":
							if(isset($_GET['id'])){
								$p = new Paciente();
								$p->detalhes_ficha_evolucao_medica($_GET['id']);
							}
							break;
						case "editarprescricao":
							if(isset($_GET['paciente'])){
								$p = new Paciente();
								$p->editar_prescricao($_GET['paciente']);
							}
							break;
						case "editar_capmed":
							if(isset($_GET['id'])){
								$p = new Paciente();
								$p->detalhes_capitacao_medica_editar($_GET['id']);
							}
							break;
						case "anterior_capmed":
							if(isset($_GET['id'])){
								$p = new Paciente();
								$p->detalhes_capitacao_medica_editar($_GET['id']);
							}
							break;
						case "anterior_evolucao":
							if(isset($_GET['id'])){
								$p = new Paciente();
								$p->anterior_evolucao_medica($_GET['id']);
							}
							break;
						case "detalhescore":
							if(isset($_GET['id'])){
								$p = new Paciente();
								$p->detalhe_score($_GET['id']);
							}
							break;
						case "detalhesprescricao":
							if(isset($_GET['id']) && isset($_GET['idpac'])){
								$p = new Paciente();
								$p->detalhe_prescricao($_GET['id'],$_GET['idpac']);
							}
							break;					}
				} else {
					$p = new Paciente();
					$p->menu();
				}
				break;
			case "planos":
				if(isset($_GET['act'])){
					switch($_GET['act']){
						case "novo":
							$p = new Plano();
							$p->form();
							break;
						case "editar":
							if(isset($_GET['id'])){
								$p = new Plano();
								$p->editar($_GET['id']);
							}
							break;
                        case "usar-novo":
                            if(isset($_GET['id'])){
                                $p = new Plano();
                                $p->editar($_GET['id'], 'usar-novo');
                            }
                            break;
						case "show":
							if(isset($_GET['id'])){
								$p = new Plano();
								$p->show($_GET['id']);
							}
							break;
					}
				} else {
					$p = new Plano();
					$p->listar();
				}
				break;
			case "filiais":
				$f = new Filial();
				$f->listar();
				break;
			# Descri��o p/ alguns Medicamentos feito pelo M�dico.
			case "DescMedico":
				if($_SESSION["adm_user"]){
					$f = new DescMedicamento();
					$f->descrever();
				}else{
					echo" <script>history.go(-1);</script>";
				}
				break;
			case "RepararProdutoI":
				if($_SESSION["adm_user"]){
					$f = new RepararProdutoI();
					$f->RepararProdInt();
				}else{
					echo" <script>history.go(-1);</script>";
				}
				break;

		}
	}else{include('content.php');}
}
?>
</div>
<div id="left">
<?php include($_SERVER['DOCUMENT_ROOT'].'/painel_login.php'); ?>
<div class="box">
<!-- <div id='calculadora'></div> -->
</div>
</div>
</div>
</body>
</html>
