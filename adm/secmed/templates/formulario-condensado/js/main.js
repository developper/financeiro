$(function($) {
    $(".chosen-select").chosen({search_contains: true});
    $('.summernote').summernote({
        height: 250,
        lang: 'pt-BR',
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline']],
            ['para', ['ul', 'ol']],
            ['table', ['table']]
        ]
    });

    if(typeof $('.bt-toggle').bootstrapToggle == 'function') {
        $('.bt-toggle').bootstrapToggle();
    }

    $('.escolher-empresa-gerar-documento-condensado').live('click', function () {
        var empresa = $(this).attr('empresa-relatorio');
        $('.empresa-impressao').val(empresa);
        $('#dialog-escolher-empresa-gerar-documento-condensado').attr('caminho', $(this).attr('caminho'));
        $('#dialog-escolher-empresa-gerar-documento-condensado').dialog('open');
    });

    $('#dialog-escolher-empresa-gerar-documento-condensado').dialog({
        closeOnEscape: false ,
        autoOpen: false,
        modal: true,
        width: 400,
        focus:  function(){
            $(this).button;
        },
        open: function(event,ui){
        },
        buttons: {
            "Cancelar": function () {
                $('#dialog-escolher-empresa-gerar-documento-condensado').dialog('close');
                return false;
            },
            "Imprimir" : function(){
                var $select = $('.empresa-impressao');
                var $titulo = $('#titulo-gerar-documento');
                if($select.val() == "-1"){
                    $select.css({"border":"3px solid red"});
                    return false;
                } else{
                    var caminho =  $('#dialog-escolher-empresa-gerar-documento-condensado').attr('caminho');
                    caminho = caminho + "&empresa=" + $select.val() + "&titulo=" + $titulo.val();
                    $select.css({"border":""});
                    window.open(caminho,'_blank');
                    $select.find('option:selected').attr('selected',false);
                    $('#dialog-escolher-empresa-gerar-documento-condensado').attr('caminho','');
                }
                $('#dialog-escolher-empresa-gerar-documento-condensado').dialog('close');
            }
        }
    });

    $('.cuidados').change(function () {
        if($(this).val() != '') {
            $(this)
                .parent()
                .parent()
                .find('.frequencia')
                .addClass('OBG');
        } else {
            $(this)
                .parent()
                .parent()
                .find('.frequencia')
                .removeClass('OBG')
                .removeClass('form-control')
                .css('border', '')
                .addClass('form-control');
        }
    });

    $('#salvar-formulario-condensado').click(function(){
        if(!validar_campos('div-salvar-formulario-condensado')){
            return false;
        }
        return true;
    });

    $('#buscar-formulario-condensado').click(function(){
        if(!validar_campos('div-buscar-formulario-condensado')){
            return false;
        }
        return true;
    });

    $("#equipamentos-outros").change(function () {
        if($(this).is(':checked')) {
            $("#combo-outros-equipamentos").show();
        } else {
            $("#combo-outros-equipamentos").hide();
            $("#equipamentos-outros-texto").val("");
        }
    });

    $("#integridade-cutanea").change(function () {
        if($(this).val() == '2') {
            $("#combo-tipo-lesao").show();
        } else if($(this).val() == '' || $(this).val() == '1') {
            $("#combo-tipo-lesao").hide();
        }
    });

    $(".atb").change(function () {
        if($(this).is(':checked')) {
            $("#combo-atb").show();
        } else {
            $("#combo-atb").hide();
            $("#atb-nome").val("");
            $("#atb-inicio").val("");
            $("#atb-previsao-termino").val("");
        }
    });

    $(".antifungico").change(function () {
        if($(this).is(':checked')) {
            $("#combo-antifungico").show();
        } else {
            $("#combo-antifungico").hide();
            $("#antifungico-nome").val("");
            $("#antifungico-inicio").val("");
            $("#antifungico-previsao-termino").val("");
        }
    });

    $("#buscar-dados-paciente").click(function(){
        var paciente = $('#paciente').val();
        var inicio = $("#inicio").val();
        var fim = $('#fim').val();
        var tipo_formulario = $('#tipo-formulario').val();

        if(paciente != '' && inicio != '' && fim != '') {
            $.ajax({
                type: "GET",
                url: "/enfermagem/planserv/?action=dados-paciente",
                data: {paciente: paciente},
                success: function (response) {
                    response = JSON.parse(response);
                    var nascimento = response.nascimento.split('-');
                    var nascStamp = new Date(nascimento[0], nascimento[1], nascimento[2]);
                    var hoje = new Date();
                    var idade = Math.floor(Math.ceil(Math.abs(nascStamp.getTime() - hoje.getTime()) / (1000 * 3600 * 24)) / 365.25);
                    $("#idade").html(idade);
                    $("#genero").html(response.sexo == '0' ? 'Masculino' : 'Feminino');
                    $("#empresa-id").val(response.empresa);
                    $("#convenio-id").val(response.convenio);
                    $("#unidade-regional").html(response.ur);
                    $("#convenio-paciente").html(response.nomeplano);
                    $("#matricula").html(response.NUM_MATRICULA_CONVENIO);
                }
            });

            var url = tipo_formulario == 'p' ? '/adm/secmed/?action=json-ultimas-prorrogacoes' : '/adm/secmed/?action=json-ultimos-deflagrados';
            $.ajax({
                type: "GET",
                url: url,
                data: {paciente: paciente, inicio: inicio, fim: fim},
                success: function (response) {
                    response = JSON.parse(response);
                    var modalidades = {
                        '1': 'Assistencia Domiciliar / Serviços Pontuais',
                        '2': 'Internação Domiciliar 6h',
                        '3': 'Internação Domiciliar 12h',
                        '4': 'Internação Domiciliar 24h com respirador',
                        '5': 'Internação Domiciliar 24h sem respirador'
                    };
                    $('.feridas').html("");

                    if(Object.keys(response).length > 0) {

                        $("#evolucao-medica").summernote('code', '');
                        $("#evolucao-enfermagem").summernote('code', '');
                        $("#evolucao-fisioterapia").summernote('code', '');
                        $("#evolucao-fonoaudiologia").summernote('code','');
                        $("#evolucao-psicologia").summernote('code', '');
                        $("#evolucao-nutricao").summernote('code', '');

                        if(response.equipamentos){
                            for(var j = 0; j < response.equipamentos.length; j++) {
                                if(response.equipamentos[j]) {
                                    $(".equipamentos[value='" + response.equipamentos[j] + "']")
                                        .prop('checked', true)
                                        .bootstrapToggle('on');
                                }
                            }
                        } else {
                            $(".equipamentos").each(function () {
                                $(this)
                                    .prop('checked', false)
                                    .bootstrapToggle('off');
                            });
                        }

                        // MEDICO
                        $("#evolucao-medica-profissional").val(response.medico.medico);
                        $("#evolucao-medica-conselho").val(response.medico.conselho_regional);
                        $("#evolucao-medica-id").val(response.medico.id);
                        $("#evolucao-medica").val(response.medico.quadro_clinico);
                        $("#evolucao-medica-data-visita").val(response.medico.data_prorrogacao);
                        $("#modalidade-id").val(response.medico.modalidade);

                        // ENFERMAGEM
                        $("#evolucao-enfermagem-profissional").val(response.enfermagem.enfermeiro);
                        $("#evolucao-enfermagem-conselho").val(response.enfermagem.conselho_regional);
                        $("#evolucao-enfermagem-id").val(response.enfermagem.id);
                        var evolucao_enfermagem = response.enfermagem.quadro_clinico;
                        if(response.enfermagem.FERIDAS == '1' && response.enfermagem.feridas.length > 0 && tipo_formulario == 'p') {
                            for(var i = 0; i < response.enfermagem.feridas.length; i++) {
                                evolucao_enfermagem += '<br><br>' +
                                    '<b>####### LESÃO Nº ' + (i + 1) + ' #######</b><br>' +
                                    '<b>LOCAL:</b> ' + response.enfermagem.feridas[i].LOCAL + '<br>' +
                                    '<b>CARACTERISTICA DA FERIDA:</b> ' + response.enfermagem.feridas[i].CARACTERISTICA + '<br>' +
                                    '<b>TIPO DO CURATIVO EM USO:</b> ' + response.enfermagem.feridas[i].TIPO_CURATIVO + '<br>' +
                                    '<b>PRESCRIÇÃO DE COBERTURA PARA O PRÓXIMO PERÍODO:</b> ' + response.enfermagem.feridas[i].PRESCRICAO_PROX_PERIODO;
                            }
                            for(var aux = 0; aux < response.enfermagem.feridas_imagem.length; aux++) {

                                $('.feridas').append(
                                    '<div class="col-xs-4">' +
                                    '    <div class="thumbnail">' +
                                    '       <input type="hidden" name="imagens_lesoes['+aux+'][imagem_id]" value="' + response.enfermagem.feridas_imagem[aux].imagem_id + '">' +
                                    '       <input type="hidden" name="imagens_lesoes['+aux+'][ferida_id]" value="' + response.enfermagem.feridas_imagem[aux].ferida_id + '">' +
                                    '       <a href="/viewer_image.php?path=' + response.enfermagem.feridas_imagem[aux].path + '" target="_blank">' +
                                    '           <img src="/' + response.enfermagem.feridas_imagem[aux].path + '" alt="' + response.enfermagem.feridas_imagem[aux].LOCAL + '" width="150px" />' +
                                    '       </a>' +
                                    '        <div class="caption text-center">' +
                                    '            <h6>' + response.enfermagem.feridas_imagem[aux].LOCAL + '</h6>' +
                                    '        </div>' +
                                    '    </div>' +
                                    '</div>'
                                );

                            }
                        }
                        $("#evolucao-enfermagem").val(evolucao_enfermagem);
                        $("#evolucao-enfermagem-data-visita").val(response.enfermagem.data_prorrogacao);

                        // FISIOTERAPIA
                        $("#evolucao-fisioterapia-profissional").val(response.fisioterapia.profissional);
                        $("#evolucao-fisioterapia-conselho").val(response.fisioterapia.conselho_regional);
                        $("#evolucao-fisioterapia-id").val(response.fisioterapia.id);
                        $("#evolucao-fisioterapia").val(response.fisioterapia.evolucao);
                        $("#evolucao-fisioterapia-data-visita").val(response.fisioterapia.data_visita);

                        // FONOAUDIOLOGIA
                        $("#evolucao-fonoaudiologia-profissional").val(response.fonoaudiologia.profissional);
                        $("#evolucao-fonoaudiologia-conselho").val(response.fonoaudiologia.conselho_regional);
                        $("#evolucao-fonoaudiologia-id").val(response.fonoaudiologia.id);
                        $("#evolucao-fonoaudiologia").val(response.fonoaudiologia.evolucao);
                        $("#evolucao-fonoaudiologia-data-visita").val(response.fonoaudiologia.data_visita);

                        // PSICOLOGIA
                        $("#evolucao-psicologia-profissional").val(response.psicologia.profissional);
                        $("#evolucao-psicologia-conselho").val(response.psicologia.conselho_regional);
                        $("#evolucao-psicologia-id").val(response.psicologia.id);
                        $("#evolucao-psicologia").val(response.psicologia.evolucao);
                        $("#evolucao-psicologia-data-visita").val(response.psicologia.data_visita);

                        // NUTRIÇÃO
                        $("#evolucao-nutricao-profissional").val(response.nutricao.profissional);
                        $("#evolucao-nutricao-conselho").val(response.nutricao.conselho_regional);
                        $("#evolucao-nutricao-id").val(response.nutricao.id);
                        $("#evolucao-nutricao").val(response.nutricao.evolucao);
                        $("#evolucao-nutricao-data-visita").val(response.nutricao.data_visita);

                        $('.summernote').summernote('destroy');
                        $('.summernote').summernote({
                            height: 250,
                            lang: 'pt-BR',
                            toolbar: [
                                ['style', ['bold', 'italic', 'underline']],
                                ['para', ['ul', 'ol']],
                                ['table', ['table']]
                            ]
                        });
                        $("#evolucao-medica").summernote('code', response.medico.quadro_clinico);
                        $("#evolucao-enfermagem").summernote('code', evolucao_enfermagem);
                        $("#evolucao-fisioterapia").summernote('code', response.fisioterapia.evolucao);
                        $("#evolucao-fonoaudiologia").summernote('code', response.fonoaudiologia.evolucao);
                        $("#evolucao-psicologia").summernote('code', response.psicologia.evolucao);
                        $("#evolucao-nutricao").summernote('code', response.nutricao.evolucao);
                    } else {
                        $("#evolucao-medica-profissional").val('');
                        $("#evolucao-medica-conselho").val('');
                        $("#evolucao-medica-id").val('');
                        $("#evolucao-medica").val('');

                        $("#evolucao-enfermagem-profissional").val('');
                        $("#evolucao-enfermagem-conselho").val('');
                        $("#evolucao-enfermagem-id").val('');
                        $("#evolucao-enfermagem").val('');
                        $("#evolucao-enfermagem-data-visita").val('');
                    }
                }
            });
        } else {
            alert('Escolha o paciente e o período completo para buscar as informações do formulário!');
        }
    });
});
