$(function ($) {
    $("ul.nav-tabs a").click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    // $(".chosen-select").chosen({search_contains: true});
    $(".valor-referencia").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $('.numeric').on('keyup', function () {
        this.value = this.value.replace(/[^0-9]/g, '');
    });

    $(".regra-primaria").on('change', function () {
        var regra_primaria = $(this).val();
        var bgcolor = regra_primaria == '1' ? '#f08080' : '#90ee90';
        var name_container = $(this).attr('container-target');
        var $container = $("#" + name_container);
        $container
            .find('.regras-fluxo tbody tr[class!="nenhuma-regra"]')
            .each(function () {
                $(this).attr('bgcolor', bgcolor);
                $(this).find('.regra-primaria-fluxo').val(regra_primaria);
            });
    });

    $(".tipo-validacao").on('change', function () {
        var name_container = $(this).attr('container-target');
        var $container = $("#" + name_container);
        if($(this).val() == '2') {
            $container
                .find(".campo")
                .removeAttr('disabled')
                .val('');
            $container
                .find(".operador")
                .removeAttr('disabled')
                .val('');
            $container
                .find(".valor-referencia")
                .removeAttr('disabled')
                .val('');
        } else {
            $container
                .find(".campo")
                .removeAttr('disabled')
                .attr('disabled', true)
                .val('');
            $container
                .find(".operador")
                .removeAttr('disabled')
                .attr('disabled', true)
                .val('');
            $container
                .find(".valor-referencia")
                .removeAttr('disabled')
                .attr('disabled', true)
                .val('');
        }
    });

    $(".marcar-todos").on('click', function () {
        var name_container = $(this).attr('container-target');
        var $container = $("#" + name_container);
        $container
            .find('input:checkbox')
            .not(this)
            .prop('checked', this.checked);
    });

    $(".remover-regra-selecionados").on('click', function () {
        var name_container = $(this).attr('container-target');
        var $container = $("#" + name_container);
        var $tbody = $container.find('.regras-fluxo tbody');
        var $trs = $container.find('.regras-fluxo tbody tr');

        $trs.each(function () {
            $(this)
                .find('input:checkbox:checked')
                .parent()
                .parent()
                .remove();
        });

        if($container.find('.regras-fluxo > tbody tr').length == 0) {
            $container.find('.add-regra').prop("disabled", false);
            $tbody
                .html(
                    '<tr align="center" class="nenhuma-regra">' +
                    '   <td colspan="10">' +
                    '       <strong>Nenhuma regra adicionada.</strong>' +
                    '   </td>' +
                    '</tr>'
                );
        }
    });

    $(".campo").on('change', function () {
        var name_container = $(this).attr('container-target');
        var $container = $("#" + name_container);
        if($(this).val() == '1' || $(this).val() == '3') {
            $container
                .find(".valor-referencia")
                .val('')
                .removeClass('numeric')
                .priceFormat({
                    prefix: '',
                    centsSeparator: ',',
                    thousandsSeparator: '.'
                });
        } else {
            $container
                .find(".valor-referencia")
                .unpriceFormat()
                .val('')
                .addClass('numeric');
        }
    });

    $('.add-regra').live('click', function () {
        var $this = $(this);
        var name_container = $(this).attr('container-target');
        var $container = $("#" + name_container);
        var $buscar_item = $container.find(".tt-input");
        var item = $buscar_item.attr('item-id');
        var opcao = $buscar_item.attr('opcao');
        var tipo = $buscar_item.attr('tipo');
        var item_text = $buscar_item.attr('item-name');
        var carater = $container.find(".carater").val();
        var carater_text = $container.find(".carater option:selected").text();
        var tipo_validacao = $container.find(".tipo-validacao").val();
        var campo = $container.find(".campo").val();
        var campo_text = campo != '' ? $container.find(".campo option:selected").text() : '-';
        var operador = $container.find(".operador").val();
        var operador_text = operador != '' ? $container.find(".operador option:selected").text() : '-';
        var valor_referencia = $container.find(".valor-referencia").val();
        valor_referencia = campo == '1' || campo == '3' ? valor_referencia.replace('.', '').replace(',', '.') : valor_referencia;
        var valor_referencia_text = valor_referencia != '' ? $container.find(".valor-referencia").val() : '-';
        valor_referencia_text = campo == '1' || campo == '3' ? 'R$ ' + valor_referencia_text : valor_referencia_text;

        if(opcao != '' && tipo != ''  && item != '' && carater != '' && tipo_validacao != '') {
            if(tipo_validacao == '2' && campo == '' && operador == '' && valor_referencia == '') {
                alert('Preencha todos os campos corretamente!');
            } else {
                var dados = {
                    item: item,
                    opcao: opcao,
                    tipo: tipo,
                    carater: carater,
                    campo: campo,
                    operador: operador,
                    valor_referencia: valor_referencia
                };

                if(validarCamposAddRegra($container, dados)) {
                    var $tbody = $container.find('.regras-fluxo tbody');
                    var regra_primaria = $container.find('.regra-primaria').val();
                    var bgcolor = regra_primaria == '1' ? '#f08080' : '#90ee90';

                    if ($tbody.has('tr.nenhuma-regra').length > 0) {
                        $tbody.html('');
                    }

                    $tbody.append(
                        '<tr class="vacenter" bgcolor="' + bgcolor + '">' +
                        '   <td align="center"><input type="checkbox" class="marcar-remover"></td>' +
                        '   <td>' + item_text +
                        '       <input type="hidden" name="regra_primaria[]" value="' + regra_primaria + '" class="regra-primaria-fluxo">' +
                        '       <input type="hidden" name="regra_fluxo_item[]" value="' + item + '" class="regra-fluxo-item">' +
                        '       <input type="hidden" name="regra_fluxo_opcao[]" value="' + opcao + '" class="regra-fluxo-opcao">' +
                        '       <input type="hidden" name="regra_fluxo_tipo[]" value="' + tipo + '" class="regra-fluxo-tipo">' +
                        '   </td>' +
                        '   <td>' + carater_text + '<input type="hidden" name="regra_fluxo_carater[]" value="' + carater + '" class="regra-fluxo-carater"></td>' +
                        '   <td>' + campo_text +
                        '       <input type="hidden" name="regra_fluxo_tipo_validacao[]" value="' + tipo_validacao + '" class="regra-fluxo-tipo-validacao">' +
                        '       <input type="hidden" name="regra_fluxo_campo[]" value="' + campo + '" class="regra-fluxo-campo">' +
                        '   </td>' +
                        '   <td>' + operador_text + '<input type="hidden" name="regra_fluxo_operador[]" value="' + operador + '" class="regra-fluxo-operador"></td>' +
                        '   <td>' + valor_referencia_text + '<input type="hidden" name="regra_fluxo_valor_referencia[]" value="' + valor_referencia + '" class="regra-fluxo-valor-referencia"></td>' +
                        '   <td align="center">' +
                        '       <button type="button" class="btn btn-danger remover-regra" container-target="' + name_container + '" title="Remover">' +
                        '           <i class="fa fa-times-circle"></i>' +
                        '       </button>' +
                        '   </td>' +
                        '</tr>'
                    );

                    $buscar_item
                        .attr('item-name', '')
                        .attr('item-id', '')
                        .val('')
                        .focus();
                    $container
                        .find(".carater")
                        .val('1');
                    $container
                        .find(".tipo-validacao")
                        .val('');
                    $container
                        .find(".campo")
                        .val('')
                        .attr('disabled', true);
                    $container
                        .find(".operador")
                        .val('')
                        .attr('disabled', true);
                    $container
                        .find(".valor-referencia")
                        .val('')
                        .attr('disabled', true);
                } else {
                    alert('Esse item já foi inserido e/ou possui uma condição ou caráter semelhante!');
                }
            }
        } else {
            alert('Preencha todos os campos corretamente!');
        }
    });

    function validarCamposAddRegra(container, dados) {
        var valid = true;

        container.find('.regras-fluxo tbody tr[class!="nenhuma-regra"]').each(function () {
            var $this = $(this);

            if(dados.opcao == 1) {
                if(dados.item == 0 || dados.item == 'todos') {
                    if (
                        $this.find('.regra-fluxo-item').val() == 0 &&
                        (
                            (
                                $this.find('.regra-fluxo-carater').val() == 1 ||
                                $this.find('.regra-fluxo-carater').val() == 'todos'
                            )
                        )
                    ) {
                        valid = false;
                    }
                } else {
                    if (
                        $this.find('.regra-fluxo-item').val() == dados.item &&
                        $this.find('.regra-fluxo-opcao').val() == dados.opcao &&
                        $this.find('.regra-fluxo-tipo-validacao').val() == dados.tipo_validacao &&
                        $this.find('.regra-fluxo-campo').val() == dados.campo &&
                        $this.find('.regra-fluxo-operador').val() == dados.operador &&
                        $this.find('.regra-fluxo-valor-referencia').val() == dados.valor_referencia
                    ) {
                        valid = false;
                    }
                }
            }

            if(dados.carater == 1) {
                if(
                    $this.find('.regra-fluxo-item').val() == dados.item &&
                    (
                        $this.find('.regra-fluxo-carater').val() == 1 ||
                        $this.find('.regra-fluxo-carater').val() == 2 ||
                        $this.find('.regra-fluxo-carater').val() == 3
                    )
                ) {
                    valid = false;
                } else {
                    if (
                        $this.find('.regra-fluxo-item').val() == dados.item &&
                        $this.find('.regra-fluxo-opcao').val() == dados.opcao &&
                        $this.find('.regra-fluxo-tipo-validacao').val() == dados.tipo_validacao &&
                        $this.find('.regra-fluxo-campo').val() == dados.campo &&
                        $this.find('.regra-fluxo-operador').val() == dados.operador &&
                        $this.find('.regra-fluxo-valor-referencia').val() == dados.valor_referencia
                    ) {
                        valid = false;
                    }
                }
            } else {
                if(
                    $this.find('.regra-fluxo-item').val() == dados.item &&
                    (
                        $this.find('.regra-fluxo-carater').val() == 1 ||
                        $this.find('.regra-fluxo-carater').val() == dados.carater
                    )
                ) {
                    valid = false;
                } else {
                    if (
                        $this.find('.regra-fluxo-item').val() == dados.item &&
                        $this.find('.regra-fluxo-opcao').val() == dados.opcao &&
                        $this.find('.regra-fluxo-carater').val() == dados.carater &&
                        $this.find('.regra-fluxo-tipo-validacao').val() == dados.tipo_validacao &&
                        $this.find('.regra-fluxo-campo').val() == dados.campo &&
                        $this.find('.regra-fluxo-operador').val() == dados.operador &&
                        $this.find('.regra-fluxo-valor-referencia').val() == dados.valor_referencia
                    ) {
                        valid = false;
                    }
                }
            }
        });
        return valid;
    }

    $(".remover-regra").live('click', function () {
        var name_container = $(this).attr('container-target');
        var $container = $("#" + name_container);
        var $tbody = $container.find('.regras-fluxo tbody');
        $(this).parent().parent().remove();
        if($container.find('.regras-fluxo > tbody tr').length == 0) {
            $container.find('.add-regra').prop("disabled", false);
            $tbody.html(
                '<tr align="center" class="nenhuma-regra">' +
                '   <td colspan="10">' +
                '       <strong>Nenhuma regra adicionada.</strong>' +
                '   </td>' +
                '</tr>'
            );
        }
    });

    $('.buscar-item').typeahead({
        hint: true,
        highlight: true,
        minLength: 3,
        fitToElement: true
    }, {
        name: 'itens',
        display: 'value',
        limit: 100,
        source: function(query, teste, response) {
            var $input = $(this.$el[0]).parents('.twitter-typeahead').find("input").first();
            var root = document.location.hostname;
            var x = $.getJSON('http://'+root+'/adm/secmed/?action=json-itens-regra-fluxo', {
                termo: query,
                opcao: $input.attr('opcao'),
                tipo: $input.attr('tipo')
            },response);
            return x;
        }
    }).on('typeahead:select', function(event, data) {
        var self = $(this);
        if(self.attr('opcao') == '1') {
            self
                .val(data.value)
                .attr('item-name', data.value)
                .attr('item-id', data.ID);
        } else {
            self
                .val(data.value)
                .attr('item-name', data.value)
                .attr('item-id', data.ID);;
        }
    });
});
