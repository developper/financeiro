$(function () {
    moment().locale('pt-BR');

    $(".chosen-select").chosen({search_contains: true});

    $(".listar-itens-autorizacao").on('click', function () {
        var $this = $(this);

        var tipo = $this.attr('tipo');
        var tipo_primario = $this.attr('tipo-primario');
        var carater = $this.attr('carater');
        var item = $this.attr('item');
        var periodo = $this.attr('periodo');
        var status_geral = $this.attr('status-geral');
        var autorizacao_id = $this.attr('autorizacao-id');
        var origem_orcamento = $this.attr('origem-orcamento');
        var origem_orcamento_id = $this.attr('origem-orcamento-id');
        var plano_id = $this.attr('plano-id');
        var inicio = $this.attr('inicio');
        var fim = $this.attr('fim');
        var paciente = $("input[name='paciente_id']").val();



        $.get("/adm/secmed/?action=buscar-arquivos-autorizacao",
            {
                autorizacao: autorizacao_id
            },
            function (response) {
                response = JSON.parse(response);
                var $divArquivos = $('#arquivos-upload-autorizacao');

                $divArquivos.html('<ul></ul>');

                $.each(response, function (index, value) {
                    var nome_arquivo = value.full_path.split('/').reverse()[0];
                    $divArquivos
                        .find('ul')
                        .append(
                            '<li>' +
                            '   <a href="/' + value.path + '" target="_blank">' + nome_arquivo + '</a>' +
                            '   <img src="/utils/delete_16x16.png" class="excluir-arquivo" arquivo-id="' + value.autorizacao_arquivo_id + '">' +
                            ('   Autorização: ' + value.data_autorizacao.split('-').reverse().join('/') || '') +
                            '</li>'
                        );
                });
            }
        );

        if(tipo_primario == 'orcamento') {
            $.get("/adm/secmed/?action=pesquisar-itens-orcamento-autorizacao",
                {
                    orcamento: item,
                    tipo: tipo
                },
                function (response) {
                    response = JSON.parse(response);
                    var $modal = $('#modal-autorizar-item');
                    var $tbody_itens = $(".lista-itens-autorizar").find('tbody');

                    $tbody_itens.html('');

                    $modal
                        .find('.modal-title')
                        .html('Autorizar Itens de ' + tipo + ' (' + periodo + ')');

                    $modal
                        .find('#item-id')
                        .val(item);
                    $modal
                        .find('#plano-id')
                        .val(plano_id);

                    $modal
                        .find('#inicio')
                        .val(inicio);

                    $modal
                        .find('#fim')
                        .val(fim);

                    $modal
                        .find('#status-geral')
                        .val(status_geral);

                    $modal
                        .find('#paciente-id')
                        .val(paciente);

                    $modal
                        .find('#carater')
                        .val(carater);

                    $modal
                        .find('#autorizacao-id')
                        .val(autorizacao_id || '');
                    $modal.find('#documento-origem-orcamento').val(origem_orcamento);
                    $modal.find('#id-documento-origem-orcamento').val(origem_orcamento_id);

                    $.each(response, function (index, value) {
                        if(value.status_item != '1' && value.status_item != '3') {
                            var optionNegado = '<option  value="3">Negado</option>';

                            if(value.status_item == 2) {
                                optionNegado = '';
                            }

                            var qtdAutorizada = value.qtd_autorizada == null ? 0 : value.qtd_autorizada;

                            $tbody_itens.append(
                                '<tr class="linha" identificador = "'+ value.fatId + "-" + value.tipo_item + "-" + (value.TABELA_ORIGEM || "catalogo") + "-" + value.CATALOGO_ID + '" quantidade-solicitada="' + value.QUANTIDADE + '">' +
                                '    <td>' +
                                '        <input type="hidden" name="origem" value="' + tipo_primario + '">' +
                                '        <input type="hidden" name="categoria[' + value.fatId + "-" + value.tipo_item + "-" + (value.TABELA_ORIGEM || "catalogo") + "-" + value.CATALOGO_ID + ']"  value="' + value.categoria + '">' +
                                '        <input type="hidden" name="autorizacao_item_id[' + value.fatId + "-" + value.tipo_item + "-" + (value.TABELA_ORIGEM || "catalogo") + "-" + value.CATALOGO_ID + ']"  value="' + value.id + '">' +

                                '        <input type="checkbox" class="autorizacao-lote" name="autorizacao_lote[' + value.CATALOGO_ID + ']">' +
                                '    </td>' +
                                '    <td>' + value.principio + '</td>' +
                                '    <td>' +
                                '       <input type="hidden"  name="quantidade_solicitada[' + value.fatId + "-" + value.tipo_item + "-" + (value.TABELA_ORIGEM || "catalogo") + "-" + value.CATALOGO_ID + ']" ' +
                                ' class="form-control quantidade-solicitada" value="' + value.QUANTIDADE + '" >' +
                                '       ' + value.QUANTIDADE + ' / '+ qtdAutorizada +'</td>' +
                                '    <td>' + value.apresentacao + '</td>' +
                                '    <td><input type="number"  min="0" name="quantidade_autorizada[' + value.fatId + "-" + value.tipo_item + "-" + (value.TABELA_ORIGEM || "catalogo") + "-" + value.CATALOGO_ID + ']" class="form-control quantidade-autorizada" ' +
                                'identificador = "'+ value.fatId + "-" + value.tipo_item + "-" + (value.TABELA_ORIGEM || "catalogo") + "-" + value.CATALOGO_ID + '" value=""></td>' +
                                '    <td width="20%">' +
                                '        <select name="status_item[' + value.fatId + "-" + value.tipo_item + "-" + (value.TABELA_ORIGEM || "catalogo") + "-" + value.CATALOGO_ID + ']"  class="form-control status-item" ' +
                                'identificador = "'+ value.fatId + "-" + value.tipo_item + "-" + (value.TABELA_ORIGEM || "catalogo") + "-" + value.CATALOGO_ID + '">'+
                                '            <option readonly value="">Selecione...</option>' +
                                '            <option  value="1">Autorizado</option>' +
                                '            <option  value="2">Autorizado Parcial</option>' +
                                optionNegado+

                                '        </select>' +
                                '    </td>' +
                                '    <td>' +
                                '       <input name="data_autorizacao[' + value.fatId + "-" + value.tipo_item + "-" + (value.TABELA_ORIGEM || "catalogo") + "-" + value.CATALOGO_ID + ']" class="form-control data-autorizacao" type="date" value="">' +
                                '    </td>' +
                                '</tr>'
                            );
                        }
                    });
                    $('#modal-autorizar-item').modal('show');
                }
            );
        }
    });

    $(".check-all").on('click', function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

    $(".status-item").live('change',function () {

        var identificador = $(this).attr('identificador');
        if($(this).val() == 3){
            var qtd = $("input[name='quantidade_solicitada["+identificador+"]']").val()
            $("input[name='quantidade_autorizada["+identificador+"]']").val(qtd).attr('readonly',true);

        }else{
            $("input[name='quantidade_autorizada["+identificador+"]']").attr('readonly',false);

        }


    });

    $(".acao-lote").on('click', function () {
        var $this = $(this);
        $(".autorizacao-lote:checked").each(function () {
            var $tr = $(this).parents('tr');

            $tr
                .find(".quantidade-autorizada")
                .val($tr.attr('quantidade-solicitada'))
                .attr('readonly',false);
            $tr
                .find(".status-item")
                .val($this.attr('tipo'));

            if($this.attr('tipo') == 3 ){
                $tr
                    .find(".quantidade-autorizada")
                    .attr('readonly',true);

            }

            var data_autorizacao_lote = $("#data-autorizacao-lote").val();

            var data_autorizacao = data_autorizacao_lote || moment().format('YYYY-MM-DD');

            $tr
                .find(".data-autorizacao")
                .val(data_autorizacao);
        })
    });

    $("#salvar-autorizacao").on('click', function () {
        var $this = $(this);
        var $form = $("#form-autorizacao");

        $this.ok = true;
        var contador = 0;

        $('.linha').each(function () {

            var identificador = $(this).attr('identificador');
            var qtd = $("input[name='quantidade_autorizada["+identificador+"]']").val();
            var status = $("select[name='status_item["+identificador+"]']").val();
            var data = $("input[name='data_autorizacao["+identificador+"]']").val();

            if(qtd != ''  || data != '' || status != '' ){
                $("input[name='quantidade_autorizada["+identificador+"]']").attr('required', true);
                $("select[name='status_item["+identificador+"]']").attr('required', true);
                $("input[name='data_autorizacao["+identificador+"]']").attr('required', true);
                contador++;

            }else{
                $("input[name='quantidade_autorizada["+identificador+"]']").attr('required', false);
                $("input[name='quantidade_autorizada["+identificador+"]']").css('border-color', 'rgba(0,0,0,.15)');
                $("select[name='status_item["+identificador+"]']").attr('required', false);
                $("select[name='status_item["+identificador+"]']").css('border-color', 'rgba(0,0,0,.15)');
                $("input[name='data_autorizacao["+identificador+"]']").attr('required', false);
                $("input[name='data_autorizacao["+identificador+"]']").css('border-color', 'rgba(0,0,0,.15)');
            }
        });


        $("#form-autorizacao input[required='required'],  select[required='required'],select[required='true'],input[required='true']").each(function () {
            if($(this).val() == '') {
                $this.ok = false;
                $(this)
                    .css('border-color', '#dc3545');
            } else {
                $(this)
                    .css('border-color', 'rgba(0,0,0,.15)');
            }
        });
        if(contador == 0){
            alert('Informe uma autorização antes de clicar em salvar.');
            return false;
        }

        if(!$this.ok) {
            alert('Preencha todos os campos corretamente!');
            return false;
        } else {

            var formdata = new FormData($form[0]),
                action = 'salvar-autorizacao';

            if($form.find('#autorizacao-id').val() != '') {
                action = 'atualizar-autorizacao';
            }

            $.ajax({
                url: "/adm/secmed/?action=" + action,
                type: 'POST',
                data: formdata,
                async: false,
                success: function (response) {
                    response = JSON.parse(response);

                    var $modal = $('#modal-autorizar-item');
                    var $tbody_itens = $(".lista-itens-autorizar").find('tbody');
                    var item = $form.find('#item-id').val();
                    var status_geral = $form.find('#status-geral').val();
                    var status_geral_txt = $form.find('#status-geral option:selected').text();

                    $tbody_itens.html('');

                    $modal
                        .find('.modal-title')
                        .html('Autorizar Itens');

                    $modal
                        .find('#item-id')
                        .val('');

                    $modal
                        .find('#status-geral')
                        .val('');

                    $modal
                        .find('#arquivos-autorizacao')
                        .val('');

                    $modal
                        .find('#data-autorizacao-arquivo')
                        .val('');

                    $modal
                        .find('#data-autorizacao-lote')
                        .val('');

                    $modal
                        .find('#paciente-id')
                        .val('');

                    $modal
                        .find('#carater')
                        .val('');

                    $('#modal-autorizar-item').modal('hide');

                    if(response.result == '1') {
                        alert('Autorização realizada com sucesso!');
                        $("#" + item).find('.label-status-geral').html(status_geral_txt);
                        $("#" + item).find('.botao-autorizar').html('Não há itens para autorizar!');
                    } else {
                        alert('Autorização realizada com sucesso! Ainda haverão itens a serem autorizados!');
                        $("#" + item).find('.label-status-geral').html('Autorizado Parcial');
                    }

                    $('#button-' + item)
                        .attr('autorizacao-id', response.id)
                        .attr('status-geral', status_geral);
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });

    $(".excluir-arquivo").live('click', function () {
        if(confirm('Deseja realmente excluir esse arquivo?')) {
            var $this = $(this);
            var $parentLi = $this.parent();

            var arquivo_id = $this.attr('arquivo-id');

            $.get("/adm/secmed/?action=excluir-arquivo-autorizacao",
                {
                    arquivo_id: arquivo_id
                },
                function (response) {
                    $parentLi.remove();
                }
            );
        }
    });

    $(".visualizar-itens-autorizacao").on('click', function () {
        var $this = $(this);

        var tipo = $this.attr('tipo');
        var tipo_primario = $this.attr('tipo-primario');
        var item = $this.attr('item');
        var periodo = $this.attr('periodo');
        var autorizacao_id = $this.attr('autorizacao-id');
        var status_geral = $this.attr('status-geral');
        var paciente_form = $this.attr('paciente-form');

        $("#autorizacao-id-arquivos").val(autorizacao_id);

        if(paciente_form
            || $this.attr('ur-form')
            || $this.attr('tipo-form')
            || $this.attr('status-form')
            || $this.attr('inicio-form')
            || $this.attr('fim-form')
        ) {
            $("#paciente-form").val(paciente_form);
            $("#ur-form").val($this.attr('ur-form'));
            $("#tipo-form").val($this.attr('tipo-form'));
            $("#status-form").val($this.attr('status-form'));
            $("#inicio-form").val($this.attr('inicio'));
            $("#fim-form").val($this.attr('fim'));
        }

        $.get("/adm/secmed/?action=buscar-arquivos-autorizacao",
            {
                autorizacao: autorizacao_id
            },
            function (response) {
                response = JSON.parse(response);
                var $divArquivos = $('#arquivos-upload-autorizacao');

                $divArquivos.html('<ul></ul>');

                $.each(response, function (index, value) {
                    var nome_arquivo = value.full_path.split('/').reverse()[0];
                    $divArquivos
                        .find('ul')
                        .append(
                            '<li>' +
                            '   <a href="/' + value.path + '" target="_blank">' + nome_arquivo + '</a>' +
                            ('   Autorização: ' + value.data_autorizacao.split('-').reverse().join('/') || '') +
                            '</li>'
                        );
                });
            }
        );
        if(tipo_primario == 'orcamento') {
            $.get("/adm/secmed/?action=pesquisar-itens-orcamento-autorizacao",
                {
                    orcamento: item,
                    tipo: tipo
                },
                function (response) {
                    response = JSON.parse(response);
                    var $modal = $('#modal-visualizar-autorizacao');
                    var $tbody_itens = $(".lista-itens-autorizar").find('tbody');

                    $tbody_itens.html('');

                    $modal
                        .find('.modal-title')
                        .html('Autorização dos Itens de ' + tipo + ' (' + periodo + ')');

                    var tipo_autorizacao = {
                        1: 'Autorizado',
                        2: 'Autorizado Parcial',
                        3: 'Negado'
                    };

                    $modal
                        .find('#status-geral')
                        .val(status_geral);

                    $.each(response, function (index, value) {

                        var data_autorizacao = '';
                        if( value.data_autorizacao == '0000-00-00' || value.data_autorizacao == '' || value.data_autorizacao == null){
                            data_autorizacao = '';

                        }else{
                            var data_autorizacao = moment(value.data_autorizacao).format('DD/MM/YYYY');
                        }

                        $tbody_itens.append(
                            '<tr>' +
                            '    <td>' + value.principio + '</td>' +
                            '    <td>' + value.QUANTIDADE + '</td>' +
                            '    <td>' + value.apresentacao + '</td>' +
                            '    <td>' + (value.qtd_autorizada || '0') + '</td>' +
                            '    <td width="20%">' + (tipo_autorizacao[value.status_item] || 'Autorização Pendente') + '</td>' +
                            '    <td>' + data_autorizacao + '</td>' +
                            '</tr>'
                        );
                    });
                    $('#modal-visualizar-autorizacao').modal('show');
                }
            );
        }
    });

    $(".listar-comentarios-autorizacao").on('click', function () {
        var $this = $(this);
        var $modal = $('#modal-comentarios');

        var item = $this.attr('item');
        var autorizacao_id = $this.attr('autorizacao-id');
        var usuario_id = $this.attr('usuario-id');
        $modal.find('.item-id').val(item);
        $modal.find('.autorizacao-id').val(autorizacao_id);
        $modal.find('.usuario-id').val(usuario_id);

        $.get("/adm/secmed/?action=json-comentarios-autorizacao",
            {
                fatura_orcamento_id: item,
                autorizacao: autorizacao_id
            },
            function (response) {
                var $divComentarios = $('#comentarios-autorizacao');
                $divComentarios.html('');

                if(response != '0') {
                    response = JSON.parse(response);
                    $.each(response, function (index, value) {
                        var button_remover = '';
                        if(value.usuario_id == usuario_id) {
                            button_remover = '       <button type="button" title="Remover" class="btn btn-sm btn-danger pull-right remover-comentario" comentario-id="' + value.comentario_id + '">' +
                                '           <i class="fa fa-times-circle"></i>' +
                                '       </button> ';
                        }
                        $divComentarios
                            .append(
                                '<div class="feed-element">' +
                                '   <div class="media-body">' +
                                '       <small class="pull-right">' + value.from_now + '</small>' +
                                '       <strong>' + value.usuario + '</strong>' +
                                '       <p style="width: 90%;">' +
                                value.comentario +
                                '       </p>' +
                                '       <small class="text-muted">' + value.data_hora + '</small>' +
                                button_remover +
                                '   </div>' +
                                '</div>' +
                                '<hr>'
                            );
                    });
                } else {
                    $divComentarios.html(
                        '<p class="nenhum-comentario">Nenhum comentário foi feito até o momento!</p>'
                    );
                }
                $modal.modal('show');
            }
        );
    });

    $("#salvar-comentario").live('click', function () {
        var $this = $(this);
        var $modal = $('#modal-comentarios');

        var item = $modal.find('.item-id').val();
        var autorizacao_id = $modal.find('.autorizacao-id').val();
        var comentario = $('#comentario').val();

        if(comentario != '') {
            var $divComentarios = $('#comentarios-autorizacao');
            if($divComentarios.has('.nenhum-comentario').length > 0) {
                $divComentarios.html('');
            }
            $.post("/adm/secmed/?action=salvar-comentarios-autorizacao",
                {
                    fatura_orcamento_id: item,
                    autorizacao: autorizacao_id,
                    comentario: comentario
                },
                function (response) {
                    if(response != '0') {
                        $divComentarios
                            .prepend(
                                '<div class="feed-element">' +
                                '   <div class="media-body">' +
                                '       <small class="pull-right">' + moment().startOf('minute').fromNow() + '</small>' +
                                '       <strong>Você</strong>' +
                                '       <p style="width: 90%;">' +
                                comentario +
                                '       </p>' +
                                '       <small class="text-muted">' + moment().format('DD/MM/YYYY HH:mm:SS') + '</small>' +
                                '       <button type="button" title="Remover" class="btn btn-sm btn-danger pull-right remover-comentario" comentario-id="' + response + '">' +
                                '           <i class="fa fa-times-circle"></i>' +
                                '       </button> ' +
                                '   </div>' +
                                '</div>' +
                                '<hr>'
                            );
                        $('#comentario').val('');
                    } else {
                        alert('Houve um problema ao salvar o comentário autorização!');
                    }
                }
            );
        } else {
            alert('Preencha o comentário corretamente antes de enviar!');
        }
    });

    $(".remover-comentario").live('click', function () {
        var $this = $(this);
        var comentario_id = $this.attr('comentario-id');

        if(confirm('Deseja realmente remover esse comentário?')) {
            $.get("/adm/secmed/?action=remover-comentario-autorizacao",
                {
                    comentario: comentario_id
                },
                function (response) {
                    if(response == '1') {
                        $this.parent().parent().remove();
                    } else {
                        alert('Houve um problema ao remover o comentário!');
                    }
                }
            );
        }
    });

    $(".anexar-arquivos-extras-autorizacao").on('click', function () {
        var $this = $(this);
        var $modal = $('#modal-arquivos-extras');

        var autorizacao_id = $this.attr('autorizacao-id');
        var paciente = $this.attr('paciente');
        var tipo = $this.attr('tipo');
        var inicio = $this.attr('inicio-form');
        var fim = $this.attr('fim-form');
        $modal.find('#autorizacao-id-arquivos').val(autorizacao_id);
        $modal.find('#tipo-form').val(tipo);
        $modal.find('#paciente-form').val(paciente);
        $modal.find('#inicio-form').val(inicio);
        $modal.find('#fim-form').val(fim);

        $.get("/adm/secmed/?action=buscar-arquivos-autorizacao",
            {
                autorizacao: autorizacao_id
            },
            function (response) {
                response = JSON.parse(response);
                var $divArquivos = $('#arquivos-anexados-autorizacao');

                $divArquivos.html('<ul></ul>');

                $.each(response, function (index, value) {
                    var nome_arquivo = value.full_path.split('/').reverse()[0];
                    $divArquivos
                        .find('ul')
                        .append(
                            '<li>' +
                            '   <a href="/' + value.path + '" target="_blank">' + nome_arquivo + '</a>' +
                            '   <img src="/utils/delete_16x16.png" class="excluir-arquivo" arquivo-id="' + value.autorizacao_arquivo_id + '">' +
                            ('   Autorização: ' + value.data_autorizacao.split('-').reverse().join('/') || '') +
                            '</li>'
                        );
                });
            }
        );

        $modal.modal('show');
    });
});
