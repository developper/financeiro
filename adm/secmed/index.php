<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

//ini_set('display_errors', 1);

use \App\Controllers\Administracao;
use \App\Controllers\Autorizacao;

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
	"GET" => [
		'/adm/secmed/?action=relatorio-modalidade-paciente' => '\App\Controllers\Administracao::formBuscarModalidadePorPaciente',
		'/adm/secmed/?action=operadoras' => '\App\Controllers\Administracao::listarOperadoras',
		'/adm/secmed/?action=operadoras-nova' => '\App\Controllers\Administracao::novaOperadora',
		'/adm/secmed/?action=operadoras-editar' => '\App\Controllers\Administracao::fillAll',
		'/adm/secmed/?action=operadoras-excluir' => '\App\Controllers\Administracao::excluirOperadora',
		'/adm/secmed/?action=operadoras-planos-buscar' => '\App\Controllers\Administracao::buscarPlanosOperadora',
		'/adm/secmed/?action=prorrogacao-planserv' => '\App\Controllers\ProrrogacaoPlanserv::listarProrrogacao',
		'/adm/secmed/?action=form-prorrogacao-planserv' => '\App\Controllers\ProrrogacaoPlanserv::formProrrogacao',
		'/adm/secmed/?action=visualizar-prorrogacao-planserv' => '\App\Controllers\ProrrogacaoPlanserv::visualizarProrrogacao',
		'/adm/secmed/?action=gerar-doc-prorrogacao-planserv' => '\App\Controllers\ProrrogacaoPlanserv::gerarDocProrrogacao',
		'/adm/secmed/?action=buscar-prescricao-curativos-prorrogacao-planserv' => '\App\Controllers\Prescricoes::existsPrescricaoCurativo',
		'/adm/secmed/?action=get-pacientes-json' => '\App\Controllers\Administracao::buscarPacientesJSON',
		'/adm/secmed/?action=get-pacientes-adm-json' => '\App\Controllers\Administracao::buscarPacientesAdmJSON',
		'/adm/secmed/?action=get-status-paciente-json' => '\App\Controllers\Administracao::buscarStatusPacienteJSON',
		'/adm/secmed/?action=get-planos-json' => '\App\Controllers\Administracao::buscarPlanosJSON',
		'/adm/secmed/?action=get-empresas-json' => '\App\Controllers\Administracao::buscarEmpresasJSON',
        '/adm/secmed/?action=relatorio-epidemiologico' => '\App\Controllers\Administracao::relatorioEpidemiologico',
        '/adm/secmed/?action=buscar-orcamentos-autorizacao' => '\App\Controllers\Autorizacao::formListarOrcamentosRelatorios',
        '/adm/secmed/?action=pesquisar-orcamento-relatorio-autorizacao' => '\App\Controllers\Autorizacao::formListarOrcamentosRelatorios',
        '/adm/secmed/?action=pesquisar-itens-orcamento-autorizacao' => '\App\Controllers\Autorizacao::pesquisarItensOrcamentoAutorizacao',
        '/adm/secmed/?action=buscar-arquivos-autorizacao' => '\App\Controllers\Autorizacao::buscarArquivosAutorizacao',
        '/adm/secmed/?action=excluir-arquivo-autorizacao' => '\App\Controllers\Autorizacao::excluirArquivoAutorizacao',
        '/adm/secmed/?action=buscar-orcamentos-autorizacao-auditoria' => '\App\Controllers\Autorizacao::formListarAutorizacoesAuditoria',
        '/adm/secmed/?action=pesquisar-orcamento-relatorio-autorizacao-auditoria' => '\App\Controllers\Autorizacao::formListarAutorizacoesAuditoria',
        '/adm/secmed/?action=autorizacao-ur' => '\App\Controllers\Autorizacao::formListarAutorizacoesUr',
        '/adm/secmed/?action=autorizacao-ur-excel' => '\App\Controllers\Autorizacao::excelAutorizacoesUr',
        '/adm/secmed/?action=json-itens-regra-fluxo' => '\App\Controllers\Fluxo\Fluxo::jsonItensRegraFluxo',
        '/adm/secmed/?action=json-comentarios-autorizacao' => '\App\Controllers\Autorizacao::jsonComentariosAutorizacao',
        '/adm/secmed/?action=remover-comentario-autorizacao' => '\App\Controllers\Autorizacao::removerComentarioAutorizacao',
        '/adm/secmed/?action=formulario-condensado' => '\App\Controllers\FormularioCondensado\FormularioCondensado::form',
        '/adm/secmed/?action=formulario-condensado-listar' => '\App\Controllers\FormularioCondensado\FormularioCondensado::formListar',
        '/adm/secmed/?action=json-ultimas-prorrogacoes' => '\App\Controllers\FormularioCondensado\FormularioCondensado::buscarUltimasProrrogacoes',
        '/adm/secmed/?action=json-ultimos-deflagrados' => '\App\Controllers\FormularioCondensado\FormularioCondensado::buscarUltimosDeflagrados',
        '/adm/secmed/?action=formulario-condensado-form-editar' => '\App\Controllers\FormularioCondensado\FormularioCondensado::formEditar',
        '/adm/secmed/?action=formulario-condensado-visualizar' => '\App\Controllers\FormularioCondensado\FormularioCondensado::visualizarFormularioCondensado',
        '/adm/secmed/?action=formulario-condensado-marcar-emitido' => '\App\Controllers\FormularioCondensado\FormularioCondensado::marcarEmitido',
        '/adm/secmed/?action=formulario-condensado-imprimir' => '\App\Controllers\FormularioCondensado\FormularioCondensado::imprimirFormularioCondensado',
	],
	"POST" => [
		'/adm/secmed/?action=relatorio-modalidade-paciente' => '\App\Controllers\Administracao::buscarModalidadePorPaciente',
		'/adm/secmed/?action=operadoras-salvar' => '\App\Controllers\Administracao::salvarOperadora',
		'/adm/secmed/?action=operadoras-atualizar' => '\App\Controllers\Administracao::atualizarOperadora',
		'/adm/secmed/?action=salvar-prorrogacao-planserv' => '\App\Controllers\ProrrogacaoPlanserv::salvarProrrogacao',
		'/adm/secmed/?action=editar-prorrogacao-planserv' => '\App\Controllers\ProrrogacaoPlanserv::editarProrrogacao',
        '/adm/secmed/?action=relatorio-epidemiologico' => '\App\Controllers\Administracao::relatorioEpidemiologico',
        '/adm/secmed/?action=excel-relatorio-epidemiologico' => '\App\Controllers\Administracao::excelRelatorioEpidemiologico',
        '/adm/secmed/?action=pesquisar-orcamento-relatorio-autorizacao' => '\App\Controllers\Autorizacao::pesquisarOrcamentoRelatorio',
        '/adm/secmed/?action=salvar-autorizacao' => '\App\Controllers\Autorizacao::salvarAutorizacao',
		'/adm/secmed/?action=atualizar-autorizacao' => '\App\Controllers\Autorizacao::atualizarAutorizacao',
        '/adm/secmed/?action=pesquisar-orcamento-relatorio-autorizacao-auditoria' => '\App\Controllers\Autorizacao::pesquisarOrcamentoRelatorioAuditoria',
        '/adm/secmed/?action=autorizacao-ur' => '\App\Controllers\Autorizacao::buscarAutorizacoesUr',
        '/adm/secmed/?action=salvar-arquivos-extras-autorizacao' => '\App\Controllers\Autorizacao::salvarArquivosExtrasAutorizacao',
        '/adm/secmed/?action=salvar-comentarios-autorizacao' => '\App\Controllers\Autorizacao::salvarComentariosAutorizacao',
        '/adm/secmed/?action=formulario-condensado' => '\App\Controllers\FormularioCondensado\FormularioCondensado::salvarFormularioCondensador',
        '/adm/secmed/?action=formulario-condensado-listar' => '\App\Controllers\FormularioCondensado\FormularioCondensado::buscarFormularioCondensado',
        '/adm/secmed/?action=formulario-condensado-atualizar' => '\App\Controllers\FormularioCondensado\FormularioCondensado::atualizarFormularioCondensado',
    ]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}