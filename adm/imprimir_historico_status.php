<?php

$id = session_id();
if (empty($id))
    session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('paciente.php');

class Imprimir_historico_status {
    public $cabecalho;
 function __construct() {
      $this->cabecalho = new Paciente();
      
 }

    

    public function historico_status($id) {
        $htm.="<form>";
            $htm.="<h2><a><img src='../utils/logo2.jpg' width='150' ></a> Hist&oacute;rico do Status do Paciente.</h2>";
              $a = $this->cabecalho; 
              $htm.=$a->cabecalho($id);

        $sql = "SELECT 
           u.nome, 
           s.status, 
           DATE_FORMAT( h.DATA, '%d/%m/%Y %h:%i:%s' ) as dat,
           DATE_FORMAT( h.DATA_ADMISSAO, '%d/%m/%Y %h:%i:%s' ) as dat_ad,
           DATE_FORMAT( h.DATA_AUTORIZACAO, '%d/%m/%Y %h:%i:%s' )as dat_int,
           h.OBSERVACAO,
           (CASE h.TIPO 
           when 1 then 'Admissão' 
           when 2 then 'reinternação' 
           when -1 then '' 
            end) as tip
            FROM historico_status_paciente AS h
            INNER JOIN usuarios AS u ON ( h.USUARIO_ID = u.idUsuarios )
            INNER JOIN statuspaciente AS s ON ( h.STATUS_PACIENTE_ID = s.id )
            WHERE
            PACIENTE_ID ={$id}
            order by h.ID desc";
        $htm.= "<table class='mytable' width=95% borde='1px' bordercolor='black'>";
        $htm.= "<tr bgcolor='#EEEEEE'>";
        $htm.= "<th><b>USUARIO</b></th>";
        $htm.= "<th ><b>DATA</b></th>";
        $htm.= "<th ><b>D.ADMISSÂO</b></th>";
        $htm.= "<th ><b>D.INTERNAÇÂO</b></th>";
        $htm.= "<th ><b>STATUS</b></th>";
        $htm.= "<th ><b>TIPO</b></th>";
        $htm.= "<th ><b>OBSERVECAO</b></th>";
        $htm.= "</tr>";
        $i = 0;
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            if ($i++ % 2 == 0)
                $cor = "#E9F4F8";
            else
                $cor = "";
            $htm.="<tr bgcolor={$cor}><td>{$row['nome']}</td><td>{$row['dat']}</td><td>{$row['dat_ad']}</td><td>{$row['dat_int']}</td><td>{$row['status']}</td><td>{$row['tip']}</td><td>{$row['OBSERVACAO']}</td></tr>";
        }

        $htm.= "</table></form>";
        $paginas [] = $header . $htm.= "</form>";
        $mpdf = new mPDF('pt', 'A4', 10);
        $mpdf->SetHeader('página {PAGENO} de {nbpg}');
        $ano = date("Y");
        $mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
        $mpdf->WriteHTML("<html><body>");
        $flag = false;
        foreach ($paginas as $pag) {
            if ($flag)
                $mpdf->WriteHTML("<formfeed>");
            $mpdf->WriteHTML($pag);
            $flag = true;
        }
        $mpdf->WriteHTML("</body></html>");
        $mpdf->Output('ficha_admissao_paciente.pdf', 'I');
        exit;
    }

}

$p = new Imprimir_historico_status();
$p->historico_status($_GET['id']);
?>
