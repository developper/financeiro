<?php

include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/codigos.php');
require __DIR__ . '/../vendor/autoload.php';

use \App\Models\FileSystem\FileUpload,
	\App\Models\DB\ConnMysqli as Conn;

class Filial{

    public function listar(){
			if(!$_SESSION["adm_user"] == "1"){
				redireciona('/inicio.php');
			}
			echo "<div id='dialog-filial' Title='Filial'>";
			echo alerta();
			echo "<p><b>Nome:</b><input type='text' name='nome-filial' size='80' class='OBG'></p>";
			echo "<p><p><b>Percentual:</b><input type='text' name='percentual-filial' size='5' class='OBG'>%</p>";
			echo "<p><p><b>Valor ISS:</b><input type='text' name='iss-filial' size='5' class='OBG '>%</p>";
			echo "</div>";
			echo "<center><h1>Filiais</h1></center>";
			echo "<button id='nova-filial'>Nova</button>";
			echo "<table class='mytable' width=100% >";
	    echo "<thead align='center' ><tr>";
	    echo "<th><b>Filial</b></th>";
			if($_SESSION['adm_user'] == 1 && $_SESSION['empresa_principal'] == 1) {
				echo "<th><b>Percentual</b></th>";
			}
            echo "<th><b>ISS</b></th>";
			echo "<th><b>Logo Atual</b></th>";
			echo "<th><b>Alterar Logo</b></th>";
			echo "<th colspan='2' ><b>Op&ccedil;&otilde;es</b></th>";
    	echo "</tr></thead>";
	    $cor = false;
	    $result = mysql_query("SELECT * FROM empresas ORDER BY nome");
	    while($row = mysql_fetch_array($result))
	    {
	      if($cor) echo "<tr>";
    	  else echo "<tr class='odd' >";
	      $cor = !$cor;
	      echo "<td>{$row['nome']}</td>";

				echo "<td>". number_format($row['percentual'],2,",",".") ."%</td>";
                echo "<td>". number_format($row['imposto_iss'],2,",",".") ."%</td>";
				echo "<td align='center' id='filial-{$row['id']}'><img src='/utils/logos/{$row['logo']}' width='200' /></td>";
				echo "<td>";
				echo "<form id='logo-filial-form-{$row['id']}' method='post' enctype='multipart/form-data' action='filiais.php'>";
				echo "<input type='file' id='filial-{$row['id']}' name='logo-filial' class='logo-filial' />";
				echo "<input type='hidden' name='query' value='upload' />";
				echo "<input type='hidden' name='id' value='{$row['id']}' />";
				echo "<button	class='send-logo-filial ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'
											filial='{$row['id']}'
											type='button'
											role='button'
											aria-disabled='false'>";
				echo "<span class='ui-button-text'>Enviar</span>";
				echo "</button>";
				echo "</form>";
	      $nome = ucwords(strtolower($row['nome']));
	      $excluir = "<img class='excluir-filial' cod='{$row['id']}' src='../utils/delete_16x16.png' title='Remover' border='0'>";
	      $editar = ($_SESSION['adm_user'] == 1 && $_SESSION['empresa_principal'] == 1 ?"<img class='editar-filial' cod='{$row['id']}' percentual='{$row['percentual']}' nome='{$nome}' iss='{$row['imposto_iss']}' src='../utils/edit_16x16.png'
	      title='Editar' border='0'>" : "");
    	  echo "<td width=1% >{$editar}</td><td width=1% >{$excluir}</td>";
		  echo "</tr>";
    	}
    	echo "</table>";
			echo '<script type="text/javascript" src="http://malsup.github.io/min/jquery.form.min.js"></script>';
	}

	public function uploadLogo($filedata, $postdata)
	{
		$config = require __DIR__.'/../../config.php';
		$fileUpload = new FileUpload($config['LOGO_FOLDER']);

		list($handlerResults, $messages) = $fileUpload->process($filedata['logo-filial']);

		//die(var_dump($handlerResults->name, $messages));

		if (empty($messages)) {
			try {
				$this->updateFilialLogo($handlerResults->name, $postdata['id']);
				return "<img src='" . $config['LOGO_FOLDER'] . '/' . $handlerResults->name . "' height='61' width='200' />";
			} catch (\Exception $e) {
				echo $e;
			}
		} else {
			foreach ($messages as $errors)
				foreach ($errors as $error)
					echo sprintf('* %s', (string) $error);
		}
	}

	public function updateFilialLogo($img_name, $filial_id)
	{
		$conn = Conn::getConnection();
		$sql = <<<SQL
UPDATE empresas SET logo = ? WHERE id = ?
SQL;
		$stmt = $conn->prepare($sql);
		$stmt->bind_param('ss', $img_name, $filial_id);
		$rs = $stmt->execute();
		if(!$rs)
			throw new Exception('Erro ao atualizar essa filial: '. $conn->error);
	}
}

if(isset($_POST['query']) && !empty($_POST['query'])){
	switch($_POST['query']){
		case 'upload':
			$filial = new Filial();
			echo $filial->uploadLogo($_FILES, $_POST);
			break;
	}
}