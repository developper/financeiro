<?php

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';

require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

// ini_set('display_errors', 1);

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
	"GET" => [
		'/adm/usuario/?action=usuario'  => '\App\Controllers\Usuario::form',
		'/adm/usuario/?action=usuario-editar'  => '\App\Controllers\Usuario::formEditar',
		'/adm/usuario/?action=listar-usuarios'  => '\App\Controllers\Usuario::listarForm',
	],
	"POST" => [
        '/adm/usuario/?action=usuario'  => '\App\Controllers\Usuario::salvarUsuario',
        '/adm/usuario/?action=usuario-editar'  => '\App\Controllers\Usuario::editarUsuario',
        '/adm/usuario/?action=listar-usuarios'  => '\App\Controllers\Usuario::listarUsuarios',
        '/adm/usuario/?action=bloquear-usuario'  => '\App\Controllers\Usuario::bloquearUsuario',
        '/adm/usuario/?action=ativar-usuario'  => '\App\Controllers\Usuario::ativarUsuario',
	]
];


$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}
