$(function($) {
    $(".chosen-select").chosen({search_contains: true});
    $('.checkbox-toggle').bootstrapToggle();

    $("#empresa-principal").change(function () {
        $("#ur").val('');
        $("#ur option").each(function () {
            $(this).removeAttr('disabled', '');
        });
        $("#ur option[value='" + $(this).val() + "']")
            .attr('disabled', 'disabled')
            .trigger('chosen:updated');
    });

    $("#gerar-senha").click(function () {
        var pass = "", chars = 6;
        var ascii = [[48, 57], [65,90]], j = 0;

        for (var i= 0; i < chars; i++) {
            j = Math.floor(Math.random() * ascii.length);
            pass += String.fromCharCode(
                    Math.floor(Math.random() * (ascii[j][1] - ascii[j][0])) + ascii[j][0]
            );
        }

        $("#senha, #confirmar").val(pass);
        $("#senha-gerador").show().text('A senha gerada foi: ' + pass);
    });

    $(".pai").change(function () {
        var $this = $(this);

        if(!$this.is(':checked')) {

        }
    });

    $(".filho").change(function () {
        var $this = $(this);

        var idpai = $this.attr('idpai');

        if($this.is(':checked')) {
            $("#pai-" + idpai).bootstrapToggle('on');
        }
    });

    $(".subfilho").change(function () {
        var $this = $(this);

        var idpai = $this.attr('idpai');
        var idraiz = $this.attr('idraiz');

        if($this.is(':checked')) {
            $("#filho-" + idpai).bootstrapToggle('on');
            $("#pai-" + idraiz).bootstrapToggle('on');
        }
    });

    $(".permissoes").change(function () {
        var $this = $(this);

        var idpai = $this.attr('idpai');

        if($this.is(':checked')) {
            $("#subfilho-" + idpai).bootstrapToggle('on');
            $("#filho-" + idpai).bootstrapToggle('on');
            $("#pai-" + idpai).bootstrapToggle('on');
        }
    });

    $("#salvar-usuario").click(function(){

        if(validar_campos('right')){
            return true;
        }
        return false;

    });
});
