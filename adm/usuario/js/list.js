$(function($) {
    $(".chosen-select").chosen({search_contains: true});

    $(".bloquear-usuario").live('click', function () {
        var $this = $(this);
        var $modal = $('#modal-bloquear-usuario');
        var usuario_id = $this.attr('usuario-id');
        var nome_usuario = $this.attr('nome-usuario');

        $modal
            .find('input#usuario-id')
            .val(usuario_id);
        $modal
            .find('input#nome-usuario')
            .val(nome_usuario);
        $modal
            .find('h4.modal-title')
            .html('Bloquear o usuário ' + nome_usuario);
        $modal.modal('show');
    });

    $("#salvar-bloqueio").live('click', function () {
        var $modal = $('#modal-bloquear-usuario');
        var usuario_id = $modal.find('input#usuario-id').val();
        var nome_usuario = $modal.find('input#nome-usuario').val();
        var data_bloqueio = $modal.find('input#data-bloqueio').val();
        var motivo_bloqueio = $modal.find('textarea#motivo-bloqueio').val();
        var valid = (usuario_id != '' && data_bloqueio != '' && motivo_bloqueio != '');

        if(valid) {
            $.post(
                '/adm/usuario/?action=bloquear-usuario',
                {
                    usuario_id: usuario_id,
                    data_bloqueio: data_bloqueio,
                    motivo_bloqueio: motivo_bloqueio
                },
                function (r) {
                    if(r == '1') {
                        alert('Usuário bloqueado com sucesso!');
                        $("#ativo-inativo-" + usuario_id).html(
                            '<a href="#" class="ativar-usuario" nome-usuario="' + nome_usuario + '" usuario-id="' + usuario_id + '">Ativar</a>'
                        );
                        $modal.find('input#usuario-id').val('');
                        $modal.find('input#nome-usuario').val('');
                        $modal.find('input#data-bloqueio').val('');
                        $modal.find('textarea#motivo-bloqueio').val('');

                        $("#status-" + usuario_id).html(
                            '<span class="fa fa-user-times"></span> Não'
                        );

                        $modal.modal('hide');
                    }
                }
            );
        } else {
            alert('Por favor, preencha todas as informações corretamente!');
        }
    });

    $(".ativar-usuario").live('click', function () {
        var nome_usuario = $(this).attr('nome-usuario');
        if(confirm('Deseja realmente ativar o usuário ' + nome_usuario + '?')) {
            var usuario_id = $(this).attr('usuario-id');
            $.post(
                '/adm/usuario/?action=ativar-usuario',
                {
                    usuario_id: usuario_id
                },
                function (r) {
                    if(r == '1') {
                        alert('Usuário ativado com sucesso!');
                        $("#ativo-inativo-" + usuario_id).html(
                            '<a href="#" class="bloquear-usuario" nome-usuario="' + nome_usuario + '" usuario-id="' + usuario_id + '">Bloquear</a>'
                        );
                        $("#status-" + usuario_id).html(
                            '<span class="fa fa-user"></span> Sim'
                        );
                    }
                }
            );
        }
    });
});
