$(function($) {
    $(".chosen-select").chosen({search_contains: true});

    if(typeof $('.bt-toggle').bootstrapToggle == 'function') {
        $('.bt-toggle').bootstrapToggle();
    }

    $('#salvar-ficha-atendimento-domiciliar').click(function(){
        var cnsValido = validaCNS($('#paciente-cns').val());
        if(!validar_campos('div-salvar-ficha-atendimento-domiciliar') && !cnsValido){
            return false;
        }
        return true;
    });

    $("#nova-ficha-atendimento-domiciliar").click(function () {
        var paciente = $('#paciente').val();
        if(paciente) {
            window.open('/adm/esus/?action=ficha-atendimento-domiciliar&paciente=' + paciente, '_blank');
            return false;
        } else {
            alert('Selecione um paciente para começar!');
            return false;
        }
    });

    $(".check-all").on('click', function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

    $("#xml-ficha-atendimento-domiciliar").click(function () {
        var paciente = $('#paciente').val();
        var inicio = $('#inicio').val();
        var fim = $('#fim').val();
        var url = '&paciente=' + paciente + '&inicio=' + inicio + '&fim=' + fim;
        var periodo = '';
        var dataVisita = '';
        var createdAt = '';
        periodo = 'De ' + inicio.split('-').reverse().join('/') + ' à ' + fim.split('-').reverse().join('/');

        if(inicio && fim) {
            $.get("/adm/esus/?action=listar-lote-xml",
                {
                    paciente: paciente,
                    inicio: inicio,
                    fim: fim
                },
                function (response) {
                    if (response) {
                        response = JSON.parse(response);
                        var $modal = $('#modal-ficha-atendimento');
                        var $tbody_itens = $(".lista-fichas-atendimento").find('tbody');

                        $tbody_itens.html('');

                        $modal
                            .find('#inicio-modal')
                            .val(inicio);

                        $modal
                            .find('#fim-modal')
                            .val(fim);

                        $modal
                            .find('.modal-title')
                            .html('Fichas de Atendimento (' + periodo + ')');

                        $.each(response, function (index, value) {
                            dataVisita = value.data_visita.split('-').reverse().join('/');
                            createdAt = value.created_at.split(' ');
                            createdAt = createdAt[0];
                            createdAt = createdAt.split('-').reverse().join('/');
                            var emitido = value.emitido_xml == '0' ? 'Não' : 'Sim';
                            var turnos = {
                                '1': 'Manhã',
                                '2': 'Tarde',
                                '3': 'Noite',
                            };
                            $tbody_itens.append(
                                '<tr>' +
                                '   <td>' +
                                '       <input type="checkbox" class="fichas-atendimento" checked name="fichas_atendimento[]" value="' + value.id + '">' +
                                '   </td>' +
                                '   <td>' + value.paciente + ' - ' + value.paciente_cns + '</td>' +
                                '   <td><small>' + value.profissional + '</small></td>' +
                                '   <td>' + dataVisita + '/' + turnos[value.turno] + '</td>' +
                                '   <td>' + createdAt + '</td>' +
                                '   <td>' + value.criado_por + '</td>' +
                                '   <td style="color: ' + (value.emitido_xml == '0' ? '#ff8c00' : '#006400') + '">' +
                                '       <b>' + emitido + '</b>' +
                                '   </td>' +
                                '</tr>'
                            );
                        });
                        $modal.modal('show');
                    } else {
                        alert('Nenhum XML encontrado para o período!');
                    }
                }
            );
        } else {
            alert('Preencha o período corretamente para começar!');
            return false;
        }

        /*if(paciente && inicio && fim) {
            window.open(url, '_blank');
            return false;
        } else {
            alert('Preencha todas as informações corretamente para começar!');
            return false;
        }*/
    });

    $("#gerar-lote-xml").live('click', function () {
        if($(".fichas-atendimento:checked").length > 0) {
            $('#modal-ficha-atendimento').modal('toggle');
            return true;
        }
        alert('Para gerar um lote xml é preciso selecionar pelo menos uma ficha de atendimento!');
        return false;
    });

    var iCanUseOn = !!$.fn.on;
    var timeout;

    $('#buscar-pessoa-fisica').typeahead({
        hint: true,
        highlight: true,
        minLength: 3
    }, {
        name: 'itens',
        display: 'value',
        limit: 100,
        source: function(query, teste, response) {
            if(timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function() {
                var x = $.getJSON('/adm/esus/?action=buscar-pessoa-fisica-json', {
                    term: $('#buscar-pessoa-fisica').val()
                }, response);
                return x;
            }, 1000);
        }
    }).on('typeahead:select', function(event, data) {
        $('#buscar-pessoa-fisica').val(data.value);
        $('#profissional-nome').val(data.no_pessoa_fisica);
        $('#profissional-cpf').val(data.nu_cpf);
        $('#profissional-cns').val(data.nu_cns);
        $('#profissional-cbo').val(data.co_cbo_2002);
        $('#profissional-ator-id').val(data.co_ator);
        $('#profissional-ine').val(data.nu_ine);
        $('#profissional-cnes').val(data.nu_cnes);
    });

    $('#buscar-cid-10').typeahead({
        hint: true,
        highlight: true,
        minLength: 3
    }, {
        name: 'itens',
        display: 'value',
        limit: 100,
        source: function(query, teste, response) {
            if(timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function() {
                var x = $.getJSON('/exames/?action=buscar-cid-10', {
                    term: $('#buscar-cid-10').val()
                }, response);
                return x;
            }, 1000);
        }
    }).on('typeahead:select', function(event, data) {
        $('#buscar-cid-10')
            .val(data.value)
            .attr('cid10', data.id)
            .attr('descricao', data.value);
    });

    $(".add-cid-10").click(function () {
        var cid10 = $('#buscar-cid-10').attr('cid10'),
            descricao = $('#buscar-cid-10').attr('descricao');
        if(cid10 && descricao && $(".cid-10-itens input[value='" + cid10 + "'] ").length == 0) {
            $(".cid-10-itens").append(
                '<tr>' +
                '   <td>' +
                '       <button class="btn btn-sm btn-danger remove-cid-10" type="button" role="button" title="Remover">' +
                '           <i class="fa fa-times-circle"></i>' +
                '       </button>' +
                '   </td>' +
                '   <td>' +
                '       <input name="cid10[]" type="hidden" value="' + cid10 + '">' + cid10 +
                '   </td>' +
                '   <td>' + descricao + '</td>' +
                '</tr>'
            );
            $('#buscar-cid-10').typeahead('val', '');
        }
    });

    $(".remove-cid-10").live('click', function () {
        $(this).parent().parent().remove();
    });

    $('#buscar-sigtap').typeahead({
        hint: true,
        highlight: true,
        minLength: 3
    }, {
        name: 'itens',
        display: 'value',
        limit: 100,
        source: function(query, teste, response) {
            if(timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function() {
                var x = $.getJSON('/adm/esus/?action=buscar-sigtap-json', {
                    term: $('#buscar-sigtap').val()
                }, response);
                return x;
            }, 1000);
        }
    }).on('typeahead:select', function(event, data) {
        $('#buscar-sigtap')
            .val(data.value)
            .attr('sigtap', data.id)
            .attr('codigo', data.codigo_sigtap)
            .attr('descricao', data.descricao);
        $('#cid').val(data.id);
    });

    $(".add-sigtap").click(function () {
        var sigtap = $('#buscar-sigtap').attr('sigtap'),
            codigo_sigtap = $('#buscar-sigtap').attr('codigo'),
            descricao = $('#buscar-sigtap').attr('descricao');
        console.log($(".sigtap-itens input[value='" + sigtap + "'] ").length);
        if(sigtap && codigo_sigtap && descricao && $(".sigtap-itens input[value='" + sigtap + "'] ").length == 0) {
            $(".sigtap-itens").append(
                '<tr>' +
                '   <td>' +
                '       <button class="btn btn-sm btn-danger remove-sigtap" type="button" role="button" title="Remover">' +
                '           <i class="fa fa-times-circle"></i>' +
                '       </button>' +
                '   </td>' +
                '   <td>' +
                '       <input name="sigtap[]" type="hidden" value="' + sigtap + '">' + codigo_sigtap +
                '   </td>' +
                '   <td>' + descricao + '</td>' +
                '</tr>'
            );
            $('#buscar-sigtap').typeahead('val', '');
        }
    });

    $(".add-data-visita").click(function () {
        var lastDiv = $('.data-visita-div');
        lastDiv.append(
            '<div class="col-xs-6">' +
            '   <label>' +
            '       Data da Visita:' +
            '   </label>' +
            '   <div class="input-group">' +
            '       <span class="input-group-btn">' +
            '           <button class="btn btn-danger remove-data-visita" type="button" role="button" title="Remover">' +
            '               <i class="fa fa-times-circle"></i>' +
            '           </button>' +
            '       </span>' +
            '       <input type="date" name="data_visita[]" class="form-control data-visita OBG" value=""> ' +
            '    </div>' +
            '       <label>' +
            '           Turno:' +
            '       </label>' +
            '       <select name="turno[]" class="form-control turno OBG" required>' +
            '           <option value="">Selecione...</option>' +
            '           <option value="1">MANHÃ</option>' +
            '           <option value="2">TARDE</option>' +
            '           <option value="3">NOITE</option>' +
            '       </select>' +
            '</div>'
        );
    });

    $(".remove-data-visita").live('click', function () {
        $(this).parent().parent().parent().remove();
    });

    $(".remove-sigtap").live('click', function () {
        $(this).parent().parent().remove();
    });

    $('#modalidade').change(function(){
        var modalidade = $(this).val();
        if(modalidade == '3') {
            $(".condicoes-avaliadas[value='1']").bootstrapToggle('on');
        }
    });

    $('#paciente-cns').blur(function(){
        var cns = $(this).val();
        return validaCNS(cns);
    });

    $('.excluir-ficha-atendimento').live('click', function () {
        if(confirm('Deseja realmente excluir essa ficha de atendimento?')) {
            var fichaId = $(this).attr('ficha-id');
            var url = '/adm/esus/?action=ficha-atendimento-domiciliar-excluir';
            $.get(
                url,
                {id: fichaId},
                function (response) {
                    if(response == '1') {
                        $('#tr-ficha-' + fichaId).remove();
                    }
                }
            );
        }
    });



    function validaCNS(vlrCNS){
        var soma;
        var resto;
        var dv;
        var pis;
        var resultado;
        var tamCNS = vlrCNS.length;
        if ((tamCNS) != 15) {
            console.log(tamCNS);
            alert("Numero de CNS inválido!");
            return false;
        }
        pis = vlrCNS;
        soma = (
            ((Number(pis.substring(0, 1))) * 15) +
            ((Number(pis.substring(1, 2))) * 14) +
            ((Number(pis.substring(2, 3))) * 13) +
            ((Number(pis.substring(3, 4))) * 12) +
            ((Number(pis.substring(4, 5))) * 11) +
            ((Number(pis.substring(5, 6))) * 10) +
            ((Number(pis.substring(6, 7))) * 9) +
            ((Number(pis.substring(7, 8))) * 8) +
            ((Number(pis.substring(8, 9))) * 7) +
            ((Number(pis.substring(9, 10))) * 6) +
            ((Number(pis.substring(10, 11))) * 5) +
            ((Number(pis.substring(11, 12))) * 4) +
            ((Number(pis.substring(12, 13))) * 3) +
            ((Number(pis.substring(13, 14))) * 2) +
            ((Number(pis.substring(14, 15))) * 1)
        );
        resto = soma % 11;
        if (resto != 0){
            alert("Numero de CNS inválido!");
            $('#paciente-cns').css('border', '1px solid red');
            return false;
        } else{
            $('#paciente-cns').css('border', '1px solid lightgreen');
            return true;
        }
    }

});
