<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

//ini_set('display_errors', 1);

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
	"GET" => [
		'/adm/esus/?action=buscar-ficha-atendimento-domiciliar' => '\App\Controllers\ESUS\FichaAtendimentoDomiciliar::fichaAtendimentoDomiciliarBuscarForm',
		'/adm/esus/?action=ficha-atendimento-domiciliar' => '\App\Controllers\ESUS\FichaAtendimentoDomiciliar::fichaAtendimentoDomiciliarForm',
		'/adm/esus/?action=ficha-atendimento-domiciliar-editar' => '\App\Controllers\ESUS\FichaAtendimentoDomiciliar::fichaAtendimentoDomiciliarFormEditar',
		'/adm/esus/?action=buscar-sigtap-json' => '\App\Controllers\ESUS\SIGTAP::buscarSigtapJson',
        '/adm/esus/?action=buscar-pessoa-fisica-json' => '\App\Controllers\ESUS\PessoaFisica::buscarPessoaFisicaJson',
		'/adm/esus/?action=listar-lote-xml' => '\App\Controllers\ESUS\FichaAtendimentoDomiciliar::listarLoteXML',
        '/adm/esus/?action=ficha-atendimento-domiciliar-excluir' => '\App\Controllers\ESUS\FichaAtendimentoDomiciliar::fichaAtendimentoDomiciliarExcluir',
	],
	"POST" => [
        '/adm/esus/?action=buscar-ficha-atendimento-domiciliar' => '\App\Controllers\ESUS\FichaAtendimentoDomiciliar::fichaAtendimentoDomiciliarBuscar',
        '/adm/esus/?action=ficha-atendimento-domiciliar' => '\App\Controllers\ESUS\FichaAtendimentoDomiciliar::salvarFichaAtendimentoDomiciliar',
        '/adm/esus/?action=ficha-atendimento-domiciliar-editar' => '\App\Controllers\ESUS\FichaAtendimentoDomiciliar::atualizarFichaAtendimentoDomiciliar',
        '/adm/esus/?action=lote-xml' => '\App\Controllers\ESUS\FichaAtendimentoDomiciliar::gerarLoteXML',
    ]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}