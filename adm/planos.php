<?php

include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/codigos.php');
require __DIR__ . '/../vendor/autoload.php';

use \App\Controllers\Administracao as Administracao;

class Plano{
    public function listar(){
        echo "<center><h1>Planos de Sa&uacute;de</h1></center>";
        echo "<button onclick=\"window.location.href='?adm=planos&act=novo'\">Novo</button>";
        echo "<table class='mytable' width=100% >";
        echo "<thead><tr>";
        echo "<th><b>Paciente</b></th>";
        echo "<th colspan='4' ><b>Op&ccedil;&otilde;es</b></th>";
        echo "</tr></thead>";
        $cor = false;
        $result = mysql_query("SELECT * FROM planosdesaude where ATIVO='S' ORDER BY nome");
        while($row = mysql_fetch_array($result))
        {
            foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
            if($cor) echo "<tr>";
            else echo "<tr class='odd' >";
            $cor = !$cor;
            echo "<td>{$row['nome']}</td>";
            $show = "<a href=?adm=planos&act=show&id={$row['id']}><img src='../utils/details_16x16.png' title='Detalhes' border='0'></a>";
            $editar = "<a href=?adm=planos&act=editar&id={$row['id']}><img src='../utils/edit_16x16.png' title='Editar' border='0'></a>";
            $usarNovo = "<a href=?adm=planos&act=usar-novo&id={$row['id']}><img src='../utils/prescricao_aditiva_24x24.png' width='16' title='Usar para Novo Plano' border='0'></a>";
            $excluir = "<img src='../utils/delete_16x16.png' class='excluir-plano' cod='{$row['id']}'>";
            echo "<td width=1% >{$show}</td><td width=1% >{$editar}</td><td width=1% >{$usarNovo}</td><td width=1% >{$excluir}</td>";
            echo "</tr>";
        }
        echo "</table>";
    }


    /////14-05-2013 itens da tabela padrao da mederi.
    public function item_cobrancaplanos($variavel){
        $variavel = anti_injection($variavel,"numerico");
        $sql = "SELECT DISTINCT c.* FROM cobrancaplanos as c";
        $result = mysql_query($sql);
        $var .= "<select id='item_cobrancaplanos' class= 'form-control' name='item_cobrancaplanos' style='background-color:transparent;font-size:9px;'>";
        while($row = mysql_fetch_array($result))
        {
            if(($variavel > 0) && ($variavel == $row['id']))
                $var .= "<option v='{$variavel}' value='{$row['id']}' selected>". $row['item'] ."-". $row['unidade']  ."</option>";
            else
                $var .= "<option v='{$variavel}' value='{$row['id']}'>". $row['item'] ."-". $row['unidade'] ."</option>";
        }
        $var .= "</select>";
        if($variavel > 0){
            return $var;
        }else{
            echo $var;
        }

    }

    public function operadoras($id = null, $disabled = null, $class = null){
        $response = Administracao::buscarOperadoras();
        $html = '<label for="operadora"> Operadora:</label> <select name="operadora" '.$disabled.' id="operadora" class="form-control operadora '.$class.'">';
        $html .= '<option selected value="-1">Selecione</value>';
        foreach($response as $operadora){
            if($operadora['ID'] === $id)
                $selected = "selected";
            else
                $selected = "";
            $html .= <<<HTML
<option {$selected} value="{$operadora['ID']}">{$operadora['NOME']}</value>
HTML;
        }
        $html .= '</select>';
        return $html;
    }

    public function form(){
        $sql = "SELECT DISTINCT c.* 
				FROM cobrancaplanos as c";
        $r = mysql_query($sql);
        $flag = true;
        $cor = false;
        $c = 1;
        echo alerta();
        $cor = !$cor;
        $botoes = "";

        require $_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/planos-de-saude/novo/plano-form.phtml';

    }

    public function show($id){
        echo "<center><h1>Planos de Sa&uacute;de</h1></center>";
        echo "
		<head>
		<link rel='stylesheet' type='text/css' href='/utils/reports.css'>
		<link rel='stylesheet' href='/utils/bootstrap-3.3.6-dist/css/bootstrap.min.css' />
		<link rel='stylesheet' href='/utils/font-awesome-4.7.0/css/font-awesome.css' />
		</head>";
        $id = anti_injection($id,"numerico");
        $sql = "SELECT DISTINCT p.*, c.*, v.valor, v.DESC_COBRANCA_PLANO,v.COD_COBRANCA_PLANO
				FROM planosdesaude as p, cobrancaplanos as c, valorescobranca as v 
				WHERE p.id = v.idPlano AND c.id = v.idCobranca AND p.id = {$id} and v.excluido_em is   NULL";
        $r = mysql_query($sql);
        $flag = true;
        $cor = false;
        $c = 1;

        $sql_op = "SELECT ID,NOME FROM operadoras o INNER JOIN operadoras_planosdesaude op ON op.OPERADORAS_ID = o.ID WHERE op.PLANOSDESAUDE_ID = {$id}";
        $r_op = mysql_query($sql_op);
        $row_op = mysql_fetch_array($r_op);



        while($row = mysql_fetch_array($r)){
            foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
            if($flag){
                $dataReajuste =implode('/',array_reverse(explode('-',$row['data_reajuste'])));
                echo "<div class='panel-body'>
					<div class='row'>
						<div class='col-lg-6'>
							<div class='form-group'>
								<label for='plano'>Nome do plano:</label>
								<input readonly type='text' cod='{$id}' value='{$row['nome']}' name='plano' id='plano' class='OBG form-control' />
							</div>
						</div>
						<div class='col-lg-3'>
							<div class='form-group'>
								<label for='registro_ans'>Registro ANS:</label>
								<input readonly type='text' cod='{$id}' value='{$row['registro_ans']}' name='registro_ans' id='registro_ans' class=' form-control' />
							</div>
						</div>
						<div class='col-lg-3'>
							<div class='form-group'>
								<label for='ano_contrato'>Ano de contrato:</label>
								<input readonly type='text' name='ano_contrato' id='ano_contrato' value='{$row['ano_contrato']}' class='OBG ano form-control' />
							</div>
						</div>
					</div>
					<div class='row'>
						<div class='col-lg-6'>
							<div class='form-group'>";
                $disabled = 'disabled';
                echo $this->operadoras($row_op[0],$disabled);
                echo "		</div>
						</div>
						<div class='col-lg-3'>
							<div class='form-group'>
								<label for='iss'>Imposto ISS:</label>
								<input type='text' class='valor OBG form-control' readonly
									value='".number_format($row['imposto_iss'], 2, ',', '.')."' name='iss' id='iss' size='4'/>
							</div>
						</div>
						<div class='col-lg-3'>
							<div class='form-group'>
								<label for='tipo_medicamento'>Utilizar medicamento: </label>
								<select {$disabled} name='tipo_medicamento' id='tipo_medicamento' class='OBG form-control'>
										<option value='' >Selecione</option>
										<option value='T' " . (($row['tipo_medicamento'] == 'T') ? "selected" : "") . ">Todos</option>
										<option value='G' " . (($row['tipo_medicamento'] == 'G') ? "selected" : "") . ">Genêrico</option>
									</select>
							</div>
						</div>
					</div>
					<div class='row'>
						<div class='col-lg-6'>
							<div class='form-group'>
								<label for='plano'>Data de último reajuste:</label>
								<input type='text' name='data_reajuste' readonly class='form-control' value='{$dataReajuste}' />
							</div>
						</div>
						<div class='col-lg-6'>
							<div class='form-group'>
								<label for='registro_ans'>Percentual de último reajuste:</label>
								<input type='text' readonly name='percentual_ultimo_reajuste'
									value='".number_format($row['percentual_reajuste'], 2, ',', '.')."' class='form-control valor' />
							</div>
						</div>

					</div>

                </div>";

                echo "<table class='table table-bordered table-striped' id='tabela-item-plano' width=100% >";
                echo "<thead><tr>";
                echo "<th></th>";
                echo "<th><b>Item</b></th>";
                echo "<th><b>Item Tabela Mederi</b></th>";
                echo "<th><b>C&oacute;d. Plano</b></th>";
                echo "<th><b>Valor</b></th>";
                echo "</tr></thead>";
                $flag = false;
            }
            if($cor) echo "<tr>";
            else echo "<tr class='odd' >";
            $cor = !$cor;
            $v = number_format($row['valor'], 2, ',', '.');
            echo "<td width=1% >{$c}</td><td>{$row['DESC_COBRANCA_PLANO']}</td><td>{$row['item']} - {$row['unidade']}</td><td>{$row['COD_COBRANCA_PLANO']}</td><td>R$ {$v}</td>";
            echo "</tr>";
            $c++;
        }
        echo "</table>";
    }

    public function editar($id, $modo = 'editar'){
        $id = anti_injection($id,"numerico");
        $target = $modo == 'editar' ? 'editar-plano' : 'salvar-novo-plano';
        //PLANO ESCOLHIDO
        $sql = "SELECT nome, registro_ans, imposto_iss,tipo_medicamento, ano_contrato,
						data_reajuste,
						percentual_reajuste
						 FROM planosdesaude WHERE id = {$id}";
        $rs = mysql_query($sql);
        $row = mysql_fetch_array($rs);
        $sql_op = "SELECT OPERADORAS_ID FROM operadoras_planosdesaude WHERE PLANOSDESAUDE_ID = {$id}";
        $rs_op = mysql_query($sql_op);
        $row_op = mysql_fetch_array($rs_op);
        $dataReajuste = implode('/',array_reverse(explode('-',$row['data_reajuste'])));

        $iss = number_format($row['imposto_iss'], 2, ',', '.');
        $percentual_reajuste = number_format($row['percentual_reajuste'], 2, ',', '.');
        echo alerta();

        //ITENS
        $sql = "SELECT DISTINCT c.id as idCobranca,c.item,c.unidade, v.valor, v.id as cod,v.DESC_COBRANCA_PLANO,v.COD_COBRANCA_PLANO
				FROM planosdesaude as p, cobrancaplanos as c, valorescobranca as v 
				WHERE p.id = v.idPlano AND c.id = v.idCobranca AND p.id = {$id} and v.excluido_em is   NULL";
        $rsValoresCobranca = mysql_query($sql);

        $cor = false;
        $c = 1;

        $regras = \App\Controllers\Fluxo\Fluxo::getFluxoPlano($id);

        $campos = [
            '-' => '-',
            '1' => 'Preço',
            '2' => 'Quantidade',
            '3' => 'Custo'
        ];

        $operadores = [
            '-' => '-',
            '1' => '=',
            '2' => '<',
            '3' => '<=',
            '4' => '>',
            '5' => '>=',
        ];

        //echo '<pre>'; die(print_r($regras));

        require $_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/planos-de-saude/editar/plano-form.phtml';
    }


}
if(isset($_POST['query'])){
    switch($_POST['query']){
        case "item-cobrancaplano":
            $cb = new Plano();
            $cb-> item_cobrancaplanos($_POST['tipo']);
    }
}


?>