<?php

$id = session_id();
if (empty($id))
    session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('paciente.php');

class Imprimir_historico_plano_liminar {
public $cabecalho;
 function __construct() {
      $this->cabecalho = new Paciente();
      
 }
  

    public function historico_plano_liminar($id) {
        $htm.="<form>";
            $htm.="<h2><a><img src='../utils/logo2.jpg' width='150' ></a> Hist&oacute;rico do Plano e Liminar.</h2>";
             $a = $this->cabecalho; 
              $htm.=$a->cabecalho($id);

        $sql=" SELECT 
                u.nome as user,
                p.nome as plano,
                (case h.LIMINAR
                when 0 then 'Não'
                when 1 then 'Sim'
                when -1 then ''
                end ) as lim,
                DATE_FORMAT(h.DATA, '%d/%m/%Y %h:%i:%s') as dat

                FROM `historico_plano_liminar_paciente` as h inner join 
                usuarios as u on (h.USUARIO_ID=u.idUsuarios) inner join 
                planosdesaude as p on (h.PLANO_ID=p.id)
                WHERE 
                `PACIENTE_ID`={$id}";
          $htm .= "<table class='mytable' width=95% >";
		$htm .= "<thead><tr bgcolor='#EEEEEE'>";
		$htm .= "<th><b>USUARIO</b></th>";
		$htm .= "<th ><b>DATA</b></th>";
                $htm .= "<th ><b>PLANO</b></th>";
                $htm .= "<th ><b>LIMINAR</b></th>";
                $htm .= "</tr></thead>";
                $result= mysql_query($sql);
                $i=1;
               while ($row = mysql_fetch_array($result)) {
                    if ($i++ % 2 == 0)
                $cor = "#E9F4F8";
            else
                $cor = "";
                   $htm .="<tr bgcolor={$cor}><td>{$row['user']}</td><td>{$row['dat']}</td><td>{$row['plano']}</td><td>{$row['lim']}</td></tr>";
               }
               

        $htm.= "</table></form>";
        $paginas [] = $header . $htm.= "</form>";
        $mpdf = new mPDF('pt', 'A4', 10);
        $mpdf->SetHeader('página {PAGENO} de {nbpg}');
        $ano = date("Y");
        $mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
        $mpdf->WriteHTML("<html><body>");
        $flag = false;
        foreach ($paginas as $pag) {
            if ($flag)
                $mpdf->WriteHTML("<formfeed>");
            $mpdf->WriteHTML($pag);
            $flag = true;
        }
        $mpdf->WriteHTML("</body></html>");
        $mpdf->Output('ficha_admissao_paciente.pdf', 'I');
        exit;
    }

}

$p = new Imprimir_historico_plano_liminar();
$p->historico_plano_liminar($_GET['id']);

?>
