<?php
include($_SERVER['DOCUMENT_ROOT'].'/login.php');
if(isset($_GET['auth']) && ($_GET['auth'] == 1)){
    echo auth_login();
}

/**
 * @return string
 */
function getAppVersion($tag = './.tag', $revision = './.revision')
{
    $version = 'DEV';
    if (file_exists($tag) && file_exists($revision)) {
        $tag = file_get_contents($tag);
        $revision = file_get_contents($revision);
        $version = sprintf('%s | Revision: #%s', $tag, substr($revision, 0, 7));
    }
    return $version;
}
$version = getAppVersion();

$configPath = __DIR__ . '/../config.php';
if (! file_exists($configPath))
    die('Arquivo "'.$configPath.'" não encontrado!');

$config = include($configPath);
define('APP_ENV', $config['APP_ENV']);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Sistema de Gestão Financeira</title>
    <style type="text/css">
        .login_box {
            height: 220px;
            width: 275px;
            margin-right: auto;
            margin-left: auto;
            margin-top: 60px;
            background-color: #00a189;
            -moz-border-radius: 10px;
            border-radius: 10px;
            -webkit-border-radius: 10px;
            padding: 10px;
            -webkit-box-shadow: 0px 0px 10px #cccccc;
            -moz-box-shadow: 0px 0px 10px #cccccc;
            box-shadow: 0px 0px 10px #cccccc;
        }
        .input_label {
            float: left;
            width: 270px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            clear: both;
            color: #FFF;
            padding-top: 5px;
            padding-bottom: 2px;
        }
        .input {
            float: left;
            clear: both;
            width: 250px;
            padding: 5px;
            border: 1px solid #ACE;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-border-radius: 5px;
        }

        .button{
            background:#fff;
            color:#000;
            border:none;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            position:relative;
            height:30px;
            padding:0 2em;
            cursor:pointer;
            transition:800ms ease all;
            outline:none;
        }
        .button:hover{
            background:#0d3349;
            color:#fff;
        }
        .button:before,button:after{
            content:'';
            position:absolute;
            top:0;
            right:0;
            height:2px;
            width:0;
            background: #fff;
            transition:400ms ease all;
        }
        .button:after{
            right:inherit;
            top:inherit;
            left:0;
            bottom:0;
        }
        .button:hover:before,button:hover:after{
            width:100%;
            transition:800ms ease all;
        }

        .links_wrap {
            clear: both;
            padding-top: 5px;
            padding-right: 5px;
            padding-bottom: 5px;
        }
        body {
            background-color: #AACCEE;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="/utils/style.css?v=11" media="screen" />
</head>
<body>
<h1 align="center" style="color: #0f3c4b">
    <img src="/utils/logos/logo_assiste_vida.png"  /> <br>
    SISTEMA DE GEST&Atilde;O FINANCEIRA <br>
   
</h1>

<?php if ('trn' === APP_ENV): ?>
    <h1 style="text-align: center; font-size: 4em;">Treinamento</h1>
<?php endif; ?>

<div class="login_box">

    <form id="form1" name="form1" method="post" action="?auth=1">
        <table width="270" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="299"></td>
            </tr>
            <tr>
                <td><p><span class="input_label">Usu&aacute;rio</span>
                        <input class="input" type="text" name="login" id="login" />
                    </p></td>
            </tr>
            <tr>
                <td><span class="input_label">Senha</span>
                    <input class="input" type="password" name="senha" id="senha" /></td>
            </tr>
            <!-- <tr>
                <td>
                    <span class="input_label">Empresa</span>
                    <select name="empresa" id="empresa" style="width: 265px;">
                        <option value="2">Assiste Vida SSA</option>
                        <option value="1">Assiste Vida FSA</option>
                        <option value="3">Assiste Vida SUL/BA</option>
                    </select>
                </td>
            </tr> -->
            <tr>
                <td align="center">
                    <br>
                    <input type="submit" name="button" class="button" value="Entrar" />
                </td>
            </tr>
        </table>
    </form>
    <br/>
    <p style="text-align: center; font-size: 0.8em;"><?php echo isset($version) ? $version : ''; ?></p>
</div>
</body>
</html>
