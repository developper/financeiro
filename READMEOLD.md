# SISMederi

## Como instalar

Considerando que você já tenha executado o comando ```vagrant up```, digite os comandos abaixo para instalar o SISMederi na Box:

```
$ vagrant ssh
$ cd /var/www/sismederi && make install
```

O resultado será algo parecido com isso:

```
Loading composer repositories with package information
Updating dependencies (including require-dev)
Nothing to install or update
Generating autoload files
php ./vendor/phing/phing/bin/phing sql
Buildfile: /var/www/sismederi/build.xml

SISMederi > sql:

     [echo] Instalando Base de Dados
[pdosqlexec] Executing file: /var/www/sismederi/def/refresh/FOREIGN_KEY_CHECKS_0.sql
[pdosqlexec] Executing file: /var/www/sismederi/def/refresh/01_dropAndCreateDatabase.sql
[pdosqlexec] Executing file: /var/www/sismederi/def/refresh/02_createTables.sql
[pdosqlexec] Executing file: /var/www/sismederi/def/refresh/03_references.sql
[pdosqlexec] Executing file: /var/www/sismederi/def/refresh/04_demo.sql
[pdosqlexec] Executing file: /var/www/sismederi/def/refresh/FOREIGN_KEY_CHECKS_1.sql
[pdosqlexec] 1060 of 1060 SQL statements executed successfully

BUILD FINISHED

Total time: 1 minutes  29.19 seconds
```

Agora vamos instalar as bibliotecas javascript

De dentro da pasta sismederi digite o comando 
```cd react-app``` para navegar até a pasta de onde temos a arquitetura do React.

Dentro da pasta sismederi execute o ```npm install``` e ele irá instalar as dependências

Agora é simples, dentro do ambiente de desenvolvimento sempre iremos rodar:

```
npm run-script watch
```
O comando acima vai ficar assistindo as alterações que fomos fazendo no react

E quando for para o ambiente de produção vamos rodar:

```
npm run-script build
```
 

Pronto! Agora abra seu browser e acesse esse endereço: [sistema.mederi.dev](http://sistema.mederi.dev/). O usuário padrão é ```admin``` e senha ```admin```.

