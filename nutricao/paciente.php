<?php

include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');
require __DIR__ . '/../vendor/autoload.php';

use App\Models\Faturamento\PlanoSaude;
use App\Helpers\StringHelper as String;

//! Classe para armazenar os dados de um paciente.
/*! utilizado na hora de exibir os detalhes */
class DadosPaciente{
    public $codigo = ""; public $nome = ""; public $nasc = ""; public $sexo = ""; public $plano = NULL;
    public $diagnostico = ""; public $cid = ""; public $origem = ""; public $leito= ""; public $telPosto= "";
    public $solicitante = ""; public $especialidade = ""; public $acomp = "";
    public $cnome = ""; public $csexo = ""; public $relcuid = "";
    public $rnome = ""; public $rsexo = ""; public $relresp = "";
    public $endereco = ""; public $bairro = ""; public $referencia = "";
    public $contato=""; public $telResponsavel= ""; public $telCuidador= "";
    public $enderecoInternacao = "";
    public $bairroInternacao = "";
    public $referenciaInternacao = "";
    public $telDomiciliar= "";
    public $telInternacao= "";
    public $especialidadeCuidador="";
    public $especialidadeResponsavel="";
    public $empresa = NULL;
    public $id_cidade_und= "";
    public $cidade_und ="";
    public $id_cidade_internado= "";
    public $cidade_internado ="";
    public $id_cidade_domiciliar= "";
    public $cidade_domiciliar ="";
    public $end_und_origem="";
    public $num_matricula_convenio="";
    public $liminar ="";
    public $cpfCliente ="";
    public $cpfResponsavel ="";
    public $nascResponsavel = "";
    public $nascCuidador ="";
}
//! Classe para armazenar as informações de uma ficha de captação
class DadosFicha{
	public $usuario = ""; public $data = ""; public $pacienteid = ""; public $pacientenome = ""; public $motivo = ""; 
	public $alergiamed = ""; public $alergiaalim = ""; public $tipoalergiamed = ""; public $tipoalergiaalim = "";
	public $prelocohosp = "3"; public $prehigihosp = "2"; public $preconshosp = "3"; public $prealimhosp = "4"; public $preulcerahosp = "2";
	public $prelocodomic = "3"; public $prehigidomic = "2"; public $preconsdomic = "3"; public $prealimdomic = "4"; public $preulceradomic = "2";
	public $objlocomocao = "3"; public $objhigiene = "2"; public $objcons = "3"; public $objalimento = "4"; public $objulcera = "2";
	public $shosp = "14"; public $sdom = "14"; public $sobj = "14";
	
	public $qtdinternacoes = ""; public $historico = "";
	public $parecer = "0"; public $justificativa = "";
	
	public $cancer = ""; public $psiquiatrico = ""; public $neuro = ""; public $glaucoma = "";
	public $hepatopatia = ""; public $obesidade = ""; public $cardiopatia = ""; public $dm = "";
	public $reumatologica = ""; public $has = ""; public $nefropatia = ""; public $pneumopatia = "";
	
	public $ativo;
}

//! Classe para gerenciar os Pacientes.
class Paciente{
  
  //! Retorna a equipe que acompanha o paciente. Não utilizado.
  /*! Os clientes ainda não definiram se iriam utilizar esse recurso, ficou para depois */
  private function equipe($paciente,$tipo){
    $has = false;
    $result = mysql_query("SELECT e.*, u.nome FROM equipePaciente as e, usuarios as u WHERE e.funcionario = u.idUsuarios AND u.tipo = '$tipo' AND paciente = '$paciente' AND fim IS NULL ORDER BY nome");
    $cor = false;
    while($row = mysql_fetch_array($result))
    {
      $has = true;
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
      if($cor) echo "<tr cod='{$row['id']}' >";
      else echo "<tr cod='{$row['id']}' class='odd' >";
      $cor = !$cor;
      echo "<td>{$row['nome']}</td>";
      $inicio = implode("/",array_reverse(explode("-",$row['inicio'])));
      echo "<td>{$inicio}</td>";
      echo "<td>-</td>";
      echo "<td><button class='end' >Finalizar</button></td><td><button class='del' >Remover</button></td>";
      echo "</tr>";
    }
    if(!$has) echo "<tr><td colspan='5' ><center><b>Nenhuma equipe está acompanhando o paciente.</b></center></td></tr>";
  }
    public function cabecalho($dados, $empresa = null, $convenio = null) {
        if (isset($dados->pacienteid)) {
            $id = $dados->pacienteid;
        } else {
            $id = $dados;
        }

        $condp1 = "c.idClientes = '{$id}'";
        $sql2 = "SELECT
  	UPPER(c.nome) AS paciente,
  	(CASE c.sexo
  	WHEN '0' THEN 'Masculino'
  	WHEN '1' THEN 'Feminino'
  	END) AS sexo,
  	FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
  	e.nome as empresa,
  	c.`nascimento`,
  	p.nome as Convenio,p.id,
  	NUM_MATRICULA_CONVENIO,
  	cpf
  	FROM
  	clientes AS c LEFT JOIN
  	empresas AS e ON (e.id = c.empresa) INNER JOIN
  	planosdesaude as p ON (p.id = c.convenio)
  	WHERE
  	{$condp1}
  	ORDER BY
  	c.nome DESC LIMIT 1";

        $result2 = mysql_query($sql2);
        echo "<table width=100% style='border:2px dashed;'>";
        while ($pessoa = mysql_fetch_array($result2)) {
            foreach ($pessoa AS $chave => $valor) {
                $pessoa[$chave] = stripslashes($valor);
            }
            $pessoa['empresa'] = !empty($empresa) ? $empresa : $pessoa['empresa'];
            $pessoa['Convenio'] = !empty($convenio) ? $convenio : $pessoa['Convenio'];
            echo "<br/><tr>";
            echo "<td colspan='2'><label style='font-size:14px;color:#8B0000'><b><center> INFORMA&Ccedil;&Otilde;ES DO PACIENTE </center></b></label></td>";
            echo "</tr>";

            echo "<tr style='background-color:#EEEEEE;'>";
            echo "<td width='70%'><label><b>PACIENTE </b></label></td>";
            echo "<td><label><b>SEXO </b></label></td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>" . String::removeAcentosViaEReg($pessoa['paciente']) . "</td>";
            echo "<td>{$pessoa['sexo']}</td>";
            echo "</tr>";

            echo "<tr style='background-color:#EEEEEE;'>";
            echo "<td width='70%'><label><b>IDADE </b></label></td>";
            echo "<td><label><b>UNIDADE REGIONAL </b></label></td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>" . join("/", array_reverse(explode("-", $pessoa['nascimento']))) . ' (' . $pessoa['idade'] . " anos)</td>";
            echo "<td>{$pessoa['empresa']}</td>";
            echo "</tr>";

            echo "<tr style='background-color:#EEEEEE;'>";
            echo "<td width='70%'><label><b>CONV&Ecirc;NIO </b></label></td>";
            echo "<td><label><b>MATR&Iacute;CULA </b></label></td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>" . strtoupper($pessoa['Convenio']) . "</td>";
            echo "<td>" . strtoupper($pessoa['NUM_MATRICULA_CONVENIO']) . "</td>";
            echo "</tr>";
            echo " <tr>
                          <td>
                             <b>CPF:</b>{$pessoa['cpf']}
                          </td>
                     </tr>";
            $this->plan = $pessoa['id'];
        }
        echo "</table><br/>";
    }
  
  //! Gera dropbox com as filiais.
  private function empresas($id,$extra = NULL){
  	$result = mysql_query("SELECT * FROM empresas  AND id NOT IN (1, 3, 9, 10) ORDER BY nome");
  	$var = "<select name='empresa' style='background-color:transparent;' class='COMBO_OBG' {$extra}>";
  	$var .= "<option value='-1'></option>";
  	while($row = mysql_fetch_array($result))
  	{
  	  if(($id <> NULL) && ($id == $row['id']))
  	    $var .= "<option value='{$row['id']}' selected>{$row['nome']}</option>";
   	 else 
  	 	$var .= "<option value='{$row['id']}'>{$row['nome']}</option>";
  	}
  	$var .= "</select>";
  	return $var;
  }
  
  public function menu(){
    echo "<a href='?adm=paciente&act=listar'>Listar</a><br>";
    echo "<a href='?adm=paciente&act=novo'>Novo paciente</a><br>";
  }
  
  //! Preenche os dados de um paciente.
  private function preencheDados($id){
  	$id = anti_injection($id,"numerico");
    $result = mysql_query("SELECT c.*,
                p.id as plano,
                cid.codigo as codcid,
                cid.descricao as descid,
                concat(cd.NOME,',',cd.UF) as cidade_und,
				(select  concat(x.NOME,',',x.UF) from cidades as x where x.id=c.CIDADE_ID_INT) as cidade_int,
				(select  concat(x1.NOME,',',x1.UF) from cidades as x1 where x1.id=c.CIDADE_ID_DOM) as cidade_dom
				FROM clientes as c
				LEFT OUTER JOIN planosdesaude as p ON c.convenio = p.id
				LEFT OUTER JOIN cid10 as cid ON c.diagnostico collate utf8_general_ci = cid.codigo collate utf8_general_ci
				LEFT JOIN cidades cd ON c.CIDADE_ID_UND = cd.ID

				WHERE idClientes = '$id'");
    $dados = new DadosPaciente();
    while($row = mysql_fetch_array($result)){
        $dados->codigo = $row['codigo']; $dados->dataInternacao = $row['dataInternacao'];$dados->nome = String::removeAcentosViaEReg($row['nome']); $dados->sexo = $row['sexo']; $dados->plano = $row['plano'];
        $dados->diagnostico = $row['descid']; $dados->cid = $row['codcid']; $dados->origem = $row['localInternacao'];  $dados->leito = $row['LEITO'];
        $dados->solicitante = $row['medicoSolicitante']; $dados->especialidade = $row['espSolicitante']; $dados->acomp = $row['acompSolicitante'];
        $dados->cnome = $row['cuidador']; $dados->csexo = $row['csexo']; $dados->relcuid = $row['relcuid'];
        $dados->rnome = $row['responsavel']; $dados->rsexo = $row['rsexo']; $dados->relresp = $row['relresp'];
        $dados->endereco = $row['endereco']; $dados->bairro = $row['bairro']; $dados->referencia = $row['referencia'];  $dados->telPosto = $row['TEL_POSTO'];  $dados->contato = $row['PES_CONTATO_POSTO'];
        $dados->empresa = $row['empresa']; $dados->telResponsavel = $row['TEL_RESPONSAVEL']; $dados->telCuidador = $row['TEL_CUIDADOR'];
        $dados->enderecoInternacao = $row['END_INTERNADO']; $dados->bairroInternacao = $row['BAI_INTERNADO']; $dados->referenciaInternacao = $row['REF_ENDERECO_INTERNADO'];
        $dados->telInternacao = $row['TEL_LOCAL_INTERNADO']; $dados->telDomiciliar = $row['TEL_DOMICILIAR'];$dados->especialidadeCuidador = $row['CUIDADOR_ESPECIALIDADE'];
        $dados->especialidadeResponsavel = $row['RESPONSAVEL_ESPECIALIDADE']; $dados->id_cidade_und = $row['CIDADE_ID_UND'];$dados->cidade_und = $row['cidade_und'];
        $dados->id_cidade_internado = $row['CIDADE_ID_INT'];$dados->cidade_internado = $row['cidade_int'];
        $dados->id_cidade_domiciliar = $row['CIDADE_ID_DOM'];$dados->cidade_domiciliar = $row['cidade_dom']; $dados->end_und_origem =$row['END_UND_ORIGEM'];
        $dados->num_matricula_convenio =$row['NUM_MATRICULA_CONVENIO'];
        ///jeferson 2-10-2013 novo campo
        $dados->liminar =$row['LIMINAR'];
        $dados->cpfCliente =$row['cpf'];

        $dados->cpfRresponsavel =$row['cpf_responsavel'];
        $dados->nascResponsavel = implode("/",array_reverse(explode("-",$row['nascimento_responsavel'])));
        $dados->nascCuidador = implode("/",array_reverse(explode("-",$row['nascimento_cuidador'])));

        $dados->nasc = implode("/",array_reverse(explode("-",$row['nascimento'])));
    }
    return $dados;
  }
  
  //! Formulário para criação de um novo paciente.


    public function form($id = NULL,$visao = ''){
        $act = $_GET['act'] == 'novo';
        $dados = $this->preencheDados($id);
        $visao_sexo = 'disabled';
        $titulo = "Detalhes Paciente";


        $matriculaDisabled = '';
        $matriculaOBG = 'OBG';
        $cpfOBG = '';
        // caso o plano seja particular;
        if($dados->plano ==1 && !$act){
            $matriculaDisabled = 'disabled';
            $matriculaOBG = '';
            $cpfOBG='OBG';
        }

        echo "<center><h1>".utf8_encode($titulo)."</h1></center>";

        echo "<div id='div-novo-paciente'>";



        echo "<fieldset style='width:95%;'><legend><b><label style='color:red;'>* </label>Paciente</b></legend><table width='100%'>";
        echo "<tr><tr><td>C&oacute;digo</td><td>Data da Solicita&ccedil;&atilde;o</td><td>Hora</td></tr>";
        echo "<tr><td><input class='' type='text' name='codigo' value='{$dados->codigo}' {$visao} disabled='disabled'/></td>
		<td><input class='OBG' type='text' name='dataInternacao' value='{$this->PegarData($dados->dataInternacao)}' {$visao} /></td>
		<td><input class='OBG hora-paciente' type='text' name='HoraInternacao' value='{$this->PegarHora($dados->dataInternacao)}' {$visao} /></td>

		</tr>";
        echo "<tr><td>Nome</td><td>Data de nascimento</td><td>Sexo</td></tr>";
        echo "<tr><td><input class='OBG' type='text' size=50 name='nome' value='" . String::removeAcentosViaEReg($dados->nome) . "' {$visao} /></td>";
        echo "<td><input class='data OBG' maxlength='10' size='10' type='text' name='nascimento' value='{$dados->nasc}' {$visao_sexo} /></td>";

        echo"<td><select name='sexo'  style='background-color:transparent;' class='COMBO_OBG' {$visao_sexo}><option value='-1' ></option>";
        echo"<option value='0' ".(($dados->sexo=='0')?"selected":"")." {$visao_sexo} >MASCULINO</option>";
        echo"<option value='1' ".(($dados->sexo=='1')?"selected":"")." {$visao_sexo} >FEMININO</option>";
        echo"</select></td></tr>";

        $planos = PlanoSaude::getAll(false);

        echo "<tr><td>Convênio</td><td>N° de Matrícula no Convênio:</td><td>Liminar:</td></tr>";
        echo "<tr><td><select name='convenio' class='COMBO_OBG ' id='convenio-ficha-paciente' style='background-color:transparent;' {$visao_sexo} >";
        echo "<option value='-1' selected ></option>";
        foreach ($planos as $plano) {
            $selected = $plano['id'] == $dados->plano ? "selected='selected'" : null;
            echo "<option value='{$plano['id']}' {$selected}  >{$plano['nome']}</option>";
        }
        echo "</select></td><td><input class='$matriculaOBG'  size='15' $matriculaDisabled type='text' name='num_matricula_convenio' value='{$dados->num_matricula_convenio}' {$visao} /></td>";
        echo "<td><select class='COMBO_OBG' name='liminar' style='background-color:transparent;'> ";
        echo "<option value='-1' ></option>";
        echo"<option value='0' ".(($dados->liminar=='0')?"selected":"")." {$visao_sexo} >N&atilde;o</option>";
        echo"<option value='1' ".(($dados->liminar =='1')?"selected":"")." {$visao_sexo} >Sim</option>";
        echo"</select></td></tr>
              <tr>
                  <td>
                      <label for='cpf-cliente'>CPF: </label>
                      <input id='cpf-cliente' {$visao} name = 'cpf_cliente' class ='cpf $cpfOBG' value='{$dados->cpfCliente}'>
                      <span id='span-cpf-cliente'  class='text-red'></span>
                  </td>

              </tr>
              </table>
              </fieldset>";

        if(!$act) {
            echo "<fieldset style='width:95%;'><legend><b><label style='color:red;'>* </label>Diagn&oacute;stico Principal</b>.</legend><table width='100%'>";
            ///echo "<tr><td><b>Diagn&oacute;stico Principal</b></td></tr>";
            if ($visao == '') echo "<tr><td>Buscar Diagn&oacute;stico:</td><td>Diagn&oacute;stico.</td><td>C&oacute;digo.</td></tr><tr><td><input type='text' name='busca-diagnostico' id='busca-diagnostico' {$visao} /></td>";
            echo "<td><input class='OBG' type='text' size=50 name='diagnostico' readonly=readonly value='{$dados->diagnostico}' /></td>";
            echo "<td><input class='OBG' type='text' name='cid-diagnostico' readonly=readonly value='{$dados->cid}' /></td></tr>";
            echo "<tr><td>M&eacute;dico solicitante</td><td>Especialidade</td><td>Acompanhar&aacute; o paciente em domic&iacute;lio (S/N)?</td></tr>";
            echo "<tr><td><input type='text' size=50 name='solicitante' class='OBG' value='{$dados->solicitante}' {$visao} /></td>";
            echo "<td><input type='text' name='sol-especialidade' class='OBG' value='{$dados->especialidade}' {$visao} /></td>";
            echo "<td><input type='text' name='sol-acompanhamento' class='OBG boleano' value='{$dados->acomp}' {$visao} /></td></tr>

              </table></fieldset>";
        }

        echo "<fieldset style='width:95%;'><legend><b><label style='color:red;'>* </label>Unidade de Interna&ccedil;&atilde;o de Origem</b>.</legend><table width='100%'>";
        echo "<tr><td>Nome da Unidade</td><td>Observação</td><td>Telefone do posto de enfermagem</td></tr>";
        echo "<tr><td><input class='OBG' type='text' size=50 name='local' id='local' value='{$dados->origem}' {$visao} /></td><td><input class= type='text' size=35 name='leito' id='leito' value='{$dados->leito}' {$visao} /></td>
		<td><input class='' type='text' size=20 name='telPosto'  maxlength='15' id='telPosto' value='{$dados->telPosto}' {$visao} /></td></tr>";

        echo "<tr><td>Endere&ccedil;o.</td><td>Cidade</td><td>Pessoa para contato.</td></tr>";
        /// if($visao == '') echo "<td></td></tr>";
        echo "<tr><td><input class='OBG' type='text' size=50 name='end_und_origem' id='end_und_origem' value='{$dados->end_und_origem}' {$visao} /></td>
		<td><input type='text' class='OBG' name='cidade_und' size=35 id='cidade_und' value='{$dados->cidade_und}'  {$visao} /></td>
		<td><input class='OBG' type='text' size=35 name='contato' id='contato' value='{$dados->contato}' {$visao} /></td>

		<td><input type='hidden' name='id_cidade_und' id='id_cidade_und' value='{$dados->id_cidade_und}' {$visao} /></td>
		</table></fieldset>";

        if(!$act) {
            echo "<fieldset style='width:95%;'><legend><b><label style='color:red;'>* </label>Respons&aacute;vel Familiar</b></legend><table width='100%'>";
            echo "<tr><td>Nome</td><td>Sexo</td><td>Rela&ccedil;&atilde;o</td><td><span class='espresp'>Especialidade</span></td><tr>";
            echo "<tr><td><input type='text' size=50 name='responsavel' class='OBG' value='{$dados->rnome}' {$visao} /></td>";
            echo "<td><select name='rsexo'  style='background-color:transparent;' class='COMBO_OBG' {$visao_sexo}><option value='-1' ></option>";
            echo "<option value='0' " . (($dados->rsexo == '0') ? "selected" : "") . " {$visao_sexo} >MASCULINO</option>";
            echo "<option value='1' " . (($dados->rsexo == '1') ? "selected" : "") . " {$visao_sexo} >FEMININO</option>";
            echo "</select></td>";

            $r = mysql_query("SELECT * FROM relacionamento_pessoa");
            echo "<td><select name='relresp'  class='COMBO_OBG' style='background-color:transparent;' {$visao_sexo} >";
            echo "<option value='-1' selected ></option>";
            while ($row = mysql_fetch_array($r)) {
                foreach ($row AS $key => $value) {
                    $row[$key] = stripslashes($value);
                }
                if ($row['ID'] == $dados->relresp) echo "<option value='{$row['ID']}' selected >{$row['RELACIONAMENTO']}</option>";
                else echo "<option value='{$row['ID']}'  >{$row['RELACIONAMENTO']}</option>";
            }
            echo "</select></td>";

            if ($dados->relresp != '12') {
                echo "<span flag='1' class='flagx'>";
            } else {
                echo "<span flag='0' class='flagx'>";
            }

            echo "<td><span class='espresp'>{$row['ID']}<input type='text' size=32 name='especialidadeResponsavel'  id='especialidadeResponsavel'  value='{$dados->especialidadeResponsavel}' {$visao} /></span></td></tr>";


            echo "<tr><td>Telefone.</td>
                 <td>Data de nascimento</td>
                 <td><label for='cpf-responsavel'>CPF:</label></td>
              </tr>";
            echo "<td><input type='text' name='telResponsavel' maxlength='15' id='telResponsavel'  class='OBG' value='{$dados->telResponsavel}' {$visao} /></td>

			  <td><input class='data OBG' maxlength='10' size='10' type='text' name='nascimentoResponsavel' value='{$dados->nascResponsavel}' {$visao_sexo} /></td>
			  <td>
                      <input id='cpf-responsavel' name ='cpf_responsavel'  class ='cpf $cpfOBG' {$visao} value='{$dados->cpfRresponsavel}'>
                      <span id='span-cpf-responsavel' class='text-red'></span>
			  </td>
			  </tr>
              </table></fieldset>";

            echo "<fieldset style='width:95%;'><legend><b>Cuidador Familiar</b>.</legend><table width='100%'><tr><td>Se Diferente do Respons&aacute;vel</td></tr>";
            echo "<tr><td>Nome</td><td>Sexo</td><td>Rela&ccedil;&atilde;o</td><td><span class='espcuid'>Especialidade</span></td><tr>";
            echo "<tr><td><input type='text' size=50 name='cuidador' class='' value='{$dados->cnome}' {$visao} /></td>";
            ///echo "<td><input type='radio' name='csexo' value='0' ".(($dados->csexo=='0')?"checked":"")." {$visao_sexo} />M";
            ///echo "<input type='radio' name='csexo' value='1' ".(($dados->csexo=='1')?"checked":"")." {$visao_sexo} />F</td>";

            echo "<td><select  style='background-color:transparent;' name='csexo' class='' {$visao_sexo}><option value='-1' ></option>";
            echo "<option value='0' " . (($dados->csexo == '0') ? "selected" : "") . " {$visao_sexo} >MASCULINO</option>";
            echo "<option value='1' " . (($dados->csexo == '1') ? "selected" : "") . " {$visao_sexo} >FEMININO</option>";
            echo "</select></td>";

            $r = mysql_query("SELECT * FROM relacionamento_pessoa");
            echo "<td><select name='relcuid'  style='background-color:transparent;' {$visao_sexo} >";
            echo "<option value='-1' selected ></option>";
            while ($row = mysql_fetch_array($r)) {
                foreach ($row AS $key => $value) {
                    $row[$key] = stripslashes($value);
                }
                if ($row['ID'] == $dados->relcuid) echo "<option value='{$row['ID']}' selected >{$row['RELACIONAMENTO']}</option>";
                else echo "<option value='{$row['ID']}'>{$row['RELACIONAMENTO']}</option>";
            }

            echo "</select></td>";
            if ($dados->relcuid != '12') {
                echo "<span flag='1' class='flagc'>";
            } else {
                echo "<span flag='0' class='flagc'>";
            }
            echo "<td>
		<span class='espcuid'>{$row['ID']}<input type='text' size=32 name='especialidadeCuidador' id='especialidadeCuidador'   value='{$dados->especialidadeCuidador}' {$visao} /></span>
		</td></tr>";
            /*  echo "<td><input type='text' name='relcuid' class='' value='{$dados->crel}' {$visao} /></td></tr>";
             */
            echo "<tr><td>Telefone.</td><td>Data de nascimento</td></tr>";


            echo "<td><input type='text' name='telCuidador' maxlength='15' id='telCuidador' class='' value='{$dados->telCuidador}' {$visao} /></td>
                   <td><input class='data ' maxlength='10' size='10' type='text' name='nascimentoCuidador' value='{$dados->nascCuidador}' {$visao_sexo} /></td>
                   </tr></table></fieldset>";

            ///echo "<td><input type='radio' name='rsexo' value='0' ".(($dados->rsexo=='0')?"checked":"")." {$visao_sexo} />M";
            /// echo "<input type='radio' name='rsexo' value='1' ".(($dados->rsexo=='1')?"checked":"")." {$visao_sexo} />F</td>";

            echo "<fieldset style='width:95%;'><legend><b><label style='color:red;'>* </label>Domicílio do Paciente</b></legend><table width='100%'>";
            echo "<tr><td>Endere&ccedil;o.</td><td>Bairro</td><td>Ponto de Referência</td></tr>";
            echo "<tr><td><input type='text' size=50 name='endereco' class='OBG' value='{$dados->endereco}' {$visao} /></td>";
            echo "<td><input type='text' name='bairro' class='OBG' value='{$dados->bairro}' {$visao} /></td>";
            echo "<td><input type='text' name='referencia' class='' value='{$dados->referencia}' {$visao} /></td></tr>";
            echo "<tr><td>Telefone.</td><td>Cidade</td></tr>";
            echo "<tr><td><input type='text' maxlength='15' name='telDomiciliar' id='telDomiciliar' class='' value='{$dados->telDomiciliar}' {$visao} /></td>
		<td><input type='text' class='OBG' size='35' name='cidade_domiciliar' id='cidade_domiciliar' value='{$dados->cidade_domiciliar}'  {$visao} /></td>
		<td><input type='hidden' name='id_cidade_domiciliar' id='id_cidade_domiciliar' value='{$dados->id_cidade_domiciliar}' {$visao} /></td></tr></table></fieldset>";

            echo "<fieldset style='width:95%;'><legend><b>Local de Interna&ccedil;&atilde;o</b></legend><table width='100%'>";
            echo "<tr><td>Se Diferente do domiciliar.</td></tr><tr><td>Endere&ccedil;o.<td>Bairro</td><td>Ponto de Referência</td></tr>";
            echo "<tr><td><input type='text' size=50 name='enderecoInternacao' class='' value='{$dados->enderecoInternacao}' {$visao} /></td>";
            echo "<td><input type='text' name='bairroInternacao' class='' value='{$dados->bairroInternacao}' {$visao} /></td>";
            echo "<td><input type='text' name='referenciaInternacao' class='' value='{$dados->referenciaInternacao}' {$visao} /></td></tr>";
            echo "<tr><td>Telefone.</td><td>Cidade<td></tr>";
            echo "<tr><td><input type='text' maxlength='15' name='telInternacao' id='telInternacao' class='' value='{$dados->telInternacao}' {$visao} /></td>
		<td><input type='text'  size='35' name='cidade_internado' id='cidade_internado' value='{$dados->cidade_internado}'  {$visao} /></td>
		<td><input type='hidden' name='id_cidade_internado' id='id_cidade_internado' value='{$dados->id_cidade_internado}' {$visao} /></td></tr></table>
		</fieldset>";
        }
        echo "<table>";
        echo "<tr><td><b><label style='color:red;'>* </label>Unidade Regional</b></td</tr>";
        echo "<tr><td>".$this->empresas($dados->empresa,$visao_sexo)."</td></tr></table>";

        echo "</div>";
    }
  
  //Fun��o criada para avisar quando o paciente n�o possui avalaia��o feita pelo m�dico no mod de enfermagem
  public function aviso_avaliacao(){
  	echo "<div id='dialog-aviso-avaliacao' title='Aviso'>";
  	echo "Esse paciente n&atilde;o possui ficha de avalia&ccedil;&atilde;o";
  	echo "</div>";
  		
  }
  
  public function aviso_evolucao(){
  	echo "<div id='dialog-aviso-evolucao' title='Aviso'>";
  	echo "Esse paciente n&atilde;o possui ficha de evolu&ccedil;&atilde;o";
  
  	echo "</div>";
  
  }
  
  
  //! Lista todos so pacientes.
  
  public function listar(){

      require 'templates/paciente_listar.phtml';

  	/*$usuario_empresa = $_SESSION['empresa_principal'];
  		
  	if ($usuario_empresa == 1) {//UR Central - Lista todos os pacientes
  		$sql = "SELECT c.idClientes, c.nome, s.status FROM clientes as c, statuspaciente as s WHERE c.status = s.id ORDER BY nome";
  	}else{//Lista os novos ou apenas os pacientes daquela UR
  		$sql = "SELECT c.idClientes, c.nome, s.status FROM clientes as c, statuspaciente as s
  		WHERE c.status = s.id AND (c.status = '1' OR c.empresa = '$usuario_empresa')
  		ORDER BY nome";
  	}
  	
  		
  	echo "<div>" ;
  			$this->aviso_avaliacao();
  			echo "</div>";
  			echo "<div>" ;
  			$this->aviso_evolucao();
  			echo "</div>";
  			
  			
  			$result = mysql_query("SELECT *, s.status as nstatus FROM clientes as c, statuspaciente as s WHERE c.status = s.id ORDER BY nome");
  			echo "<div id='dialog-status' title='Status do paciente' cod='' ></div>";

  
  			echo "<table class='mytable' width=95% >";
  			echo "<thead><tr>";
  			echo "<th><b>C&oacute;digo</b></th>";
  			echo "<th width=28%><b>Paciente</b></th>";
  			echo "<th width=1%><b>Status</b></th>";
      if($_SESSION['tipo'] != 'logistica'){
          echo "<th colspan='2' ><b>Op. Medica</b></th>";
          echo "<th colspan='2' ><b>Op. Enfermagem</b></th>";

      }

  			echo "<th colspan='5' ><b>Detalhes</b></th>";
  			echo "</tr></thead>";
  			$cor = false;
  			//echo "<button id='tt'>dcgfgh</button>";
  			while($row = mysql_fetch_array($result))
  			{
  				foreach($row AS $key => $value) {
  					$row[$key] = stripslashes($value);
  				}
  				if($cor) echo "<tr>";
  				else echo "<tr class='odd' >";
  				$cor = !$cor;
  				echo "<td>{$row['codigo']}</td>";
  				echo "<td>{$row['nome']}</td>";
  				echo "<td>{$row['nstatus']}</td>";
                if($_SESSION['tipo'] != 'logistica') {
                    //echo "<td><a href='?view=listcapmed&id={$row['idClientes']}'><img src='../utils/capmed_16x16.png' title='Ficha de avalia&ccedil;&atilde;o m&eacute;dica'  border='0'></a></td>";
                    echo "<td><a href= '../medico/?view=paciente_med&act=listcapmed&id={$row['idClientes']}'><img src='../utils/capmed_16x16.png' title='Ficha de avalia&ccedil;&atilde;o m&eacute;dica' class='listcapmed' idpaciente='{$row['idClientes']}' border='0'></a></td>";
                    //echo "<td><a href='?view=listfichaevolucao&id={$row['idClientes']}'><img src='../utils/fichas_24x24.png' width='16' title='Ficha de evolu&ccedil;&atilde;o m&eacute;dica' border='0'></a></td>";
                    echo "<td><a href='../medico/?view=paciente_med&act=listfichaevolucao&id={$row['idClientes']}'><img src='../utils/fichas_24x24.png' width='16' title='Ficha de evolu&ccedil;&atilde;o m&eacute;dica'  class='listfichaevolucao' idpaciente2='{$row['idClientes']}' border='0'></a></td>";
                    echo "<td><a href='../enfermagem/?view=listavaliacaenfermagem&id={$row['idClientes']}'><img src='../utils/capmed_16x16.png' title='Ficha de avalia&ccedil;&atilde;o enfermagem' class='listfichaavaliacaoenf' idpaciente3='{$row['idClientes']}' border='0'></a></td>";
                    echo "<td><a href='../enfermagem/?view=listevolucaoenfermagem&id={$row['idClientes']}'><img src='../utils/fichas_24x24.png' width='16' title='Ficha de evolu&ccedil;&atilde;o enfermagem' class='listfichaevolucaoenf' idpaciente4='{$row['idClientes']}' border='0'></a></td>";
                    // echo "<td><img src='../utils/status_16x16.png' class='statusp' cod='{$row['idClientes']}' title='Mudar status' border='0'></td>";
                }
                    echo "<td><a href=?view=show&id={$row['idClientes']}><img src='../utils/details_16x16.png' title='Detalhes' border='0'></a></td>";
  				// echo "<td><a href=?adm=paciente&act=editar&id={$row['idClientes']}><img src='../utils/edit_16x16.png' title='Editar' border='0'></a></td>";
  						echo "</tr>";
		}
  		echo "</table>";*/
  
  	}
  
  /*public function listar(){
    $result = mysql_query("SELECT *, s.status as nstatus FROM clientes as c, statuspaciente as s WHERE c.status = s.id ORDER BY nome");
    echo "<div id='dialog-status' title='Status do paciente' cod='' ></div>";
    echo "<a href='imprimir.php'>Imprimir lista</a><br>";
    echo "<table class='mytable' width=100% >"; 
    echo "<thead><tr>";
    echo "<th><b>C&oacute;digo</b></th>";
    echo "<th><b>Paciente</b></th>";
    echo "<th width=1%><b>Status</b></th>"; 
    echo "<th colspan='4' ><b>Op&ccedil;&otilde;es</b></th>";
    echo "</tr></thead>";
    $cor = false;
    while($row = mysql_fetch_array($result))
    {
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
      if($cor) echo "<tr>";
      else echo "<tr class='odd' >";
      $cor = !$cor;
      echo "<td>{$row['codigo']}</td>";
      echo "<td>{$row['nome']}</td>";
      echo "<td>{$row['nstatus']}</td>";
      echo "<td><a href='?adm=paciente&act=listcapmed&id={$row['idClientes']}'><img src='../utils/capmed_16x16.png' title='ficha m&eacute;dica' border='0'></a></td>";
      echo "<td><img src='../utils/status_16x16.png' class='statusp' cod='{$row['idClientes']}' title='Mudar status' border='0'></td>";
      echo "<td><a href=?adm=paciente&act=show&id={$row['idClientes']}><img src='../utils/details_16x16.png' title='Detalhes' border='0'></a></td>";
      echo "<td><a href=?adm=paciente&act=editar&id={$row['idClientes']}><img src='../utils/edit_16x16.png' title='Editar' border='0'></a></td>";
      echo "</tr>";
    }
    echo "</table>";
  }*/
  
  //! Inicializa uma nova captação
  public function nova_capitacao_medica($id){
  	$id = anti_injection($id,"numerico");
  	$result = mysql_query("SELECT p.nome FROM clientes as p WHERE p.idClientes = '{$id}'");
  	$d = new DadosFicha();
    while($row = mysql_fetch_array($result)){
    	$d->pacienteid = $id;
    	$d->pacientenome = ucwords(strtolower($row["nome"]));
    }
    $this->capitacao_medica($d,"");
  }
  
  //! Mostra detalhes de uma captação
  public function detalhes_capitacao_medica($id){
  	$id = anti_injection($id,"numerico");
  	$sql = "SELECT cap.*, pac.nome as pnome, usr.nome as unome, DATE_FORMAT(cap.data,'%d/%m/%Y') as fdata, comob.* 
  			FROM capmedica as cap 
  			LEFT OUTER JOIN comorbidades as comob ON cap.id = comob.capmed
  			LEFT OUTER JOIN problemasativos as prob ON cap.id = prob.capmed 
  			LEFT OUTER JOIN usuarios as usr ON cap.usuario = usr.idUsuarios 
  			LEFT OUTER JOIN clientes as pac ON cap.paciente = pac.idClientes  
  			WHERE cap.id = '{$id}'";
  	$d = new DadosFicha();
  	$result = mysql_query($sql);
    while($row = mysql_fetch_array($result)){
    	$d->usuario = ucwords(strtolower($row["unome"])); $d->data = $row['fdata']; $d->pacientenome = ucwords(strtolower($row["pnome"])); 
    	$d->motivo = $row['motivo']; 
		$d->alergiamed = $row['alergiamed']; $d->alergiaalim = $row['alergiaalim']; 
		$d->tipoalergiamed = $row['tipoalergiamed']; $d->tipoalergiaalim = $row['tipoalergiaalim'];
		$d->prelocohosp = $row['hosplocomocao']; $d->prehigihosp = $row['hosphigiene']; 
		$d->preconshosp = $row['hospcons']; $d->prealimhosp = $row['hospalimentacao']; $d->preulcerahosp = $row['hospulcera'];
		$d->prelocodomic = $row['domlocomocao']; $d->prehigidomic = $row['domhigiene']; $d->preconsdomic = $row['domcons']; 
		$d->prealimdomic = $row['domalimentacao']; $d->preulceradomic = $row['domulcera'];
		$d->objlocomocao = $row['objlocomocao']; $d->objhigiene = $row['objhigiene']; $d->objcons = $row['objcons']; 
		$d->objalimento = $row['objalimentacao']; $d->objulcera = $row['objulcera'];
		$d->shosp = $d->prelocohosp + $d->prehigihosp +	$d->preconshosp + $d->prealimhosp + $d->preulcerahosp; 
		$d->sdom = $d->prelocodomic + $d->prehigidomic +	$d->preconsdomic + $d->prealimdomic + $d->preulceradomic;; 
		$d->sobj = $d->objlocomocao + $d->objhigiene + $d->objcons + $d->objalimento + $d->objulcera;
	
		$d->qtdinternacoes = $row['qtdinternacoes']; $d->historico = $row['historicointernacao'];
		$d->parecer = $row['parecer']; $d->justificativa = $row['justificativa'];
	
		$d->cancer = $row['cancer']; $d->psiquiatrico = $row['psiquiatrico']; $d->neuro = $row['neuro']; $d->glaucoma = $row['glaucoma'];
		$d->hepatopatia = $row['hepatopatia']; $d->obesidade = $row['obesidade']; $d->cardiopatia = $row['cardiopatia']; $d->dm = $row['dm'];
		$d->reumatologica = $row['reumatologica']; $d->has = $row['has']; $d->nefropatia = $row['nefropatia']; $d->pneumopatia = $row['pneumopatia'];
    }
    $sql = "SELECT c.descricao as nome FROM problemasativos as p, cid10 as c WHERE p.cid = c.codigo AND p.capmed = '{$id}' ";
    $result = mysql_query($sql);
    while($row = mysql_fetch_array($result)){
    	$d->ativos[] = $row['nome'];
    }
    $this->capitacao_medica($d,"readonly");
  }
  
  //! Exibe uma captação, seja apenas os detalhes (modo read only) ou para criação.
  public function capitacao_medica($dados,$acesso){
  	  $enabled = "";
  	  if($acesso == "readonly") $enabled = "disabled";
      echo "<center><h1>Ficha m&eacute;dica do paciente</h1></center>";
      echo "<center><h2>{$dados->pacientenome}</h2></center>";
      if($acesso == "readonly"){
      	echo "<center><h2>Respons&aacute;vel: {$dados->usuario} - Data: {$dados->data}</h2></center>";
      }
      echo "<div id='div-ficha-medica' ><br/>";
      echo alerta();
      echo "<form>";
      echo "<input type='hidden' name='paciente' value='{$dados->pacienteid}' />";
      echo "<br/><table class='mytable' width=100% >";
      echo "<thead><tr>"; 
      echo "<th colspan='2' >Motivo da hospitaliza&ccedil;&atilde;o</th>";
      echo "</tr></thead>";
      echo "<tr><td colspan='2' ><textarea cols=100 rows=5 {$acesso} name='motivo' class='OBG' >{$dados->motivo}</textarea>";
      //<input type='text' size=100 value='{$row['motivo']}' name='motivo' class='OBG' /></td></tr>";
      echo "<thead><tr>"; 
      echo "<th colspan='2' >Problemas Ativos</th>";
      echo "</tr></thead>";
      if($acesso != "readonly") echo "<tr><td colspan='2' ><input type='text' name='busca-problemas-ativos' {$acesso} size=100 /></td></tr>";
      echo "<tr><td colspan='2' ><table id='problemas_ativos'>";
      if(!empty($dados->ativos)){
      	foreach($dados->ativos as $a){
      		echo "<tr><td>{$a}</td></tr>";
      	}
      }
      echo "</table></td></tr>";
      echo "<thead><tr>"; 
      echo "<th colspan='2' >Comorbidades (S/N)</th>";
      echo "</tr></thead>";
      echo "<tr><td><input type='text' class='boleano OBG' name='cancer' value='{$dados->cancer}' {$acesso} /><b>Câncer</b></td>";
      echo "<td><input type='text' class='boleano OBG' name='cardiopatia' value='{$dados->cardiopatia}' {$acesso} /><b>Cardiopatia</b></td>";
      echo "</tr><tr><td><input type='text' class='boleano OBG' name='psiquiatrico' value='{$dados->psiquiatrico}' {$acesso} /><b>Dist&uacute;rbio Psiqui&aacute;trico</b></td>";
      echo "<td><input type='text' class='boleano OBG' name='dm' value='{$dados->dm}' {$acesso} /><b>DM</b></td>";
      echo "</tr><tr><td><input type='text' class='boleano OBG' name='neurologica' value='{$dados->neuro}' {$acesso} /><b>Doen&ccedil;a Neurol&oacute;gica</b></td>";
      echo "<td><input type='text' class='boleano OBG' name='reumatologica' value='{$dados->reumatologica}' {$acesso} /><b>Doen&ccedil;a Reumatol&oacute;gica</b></td>";
      echo "</tr><tr><td><input type='text' class='boleano OBG' name='glaucoma' value='{$dados->glaucoma}' {$acesso} /><b>Glaucoma</b></td>";
      echo "<td><input type='text' class='boleano OBG' name='has' value='{$dados->has}' {$acesso} /><b>HAS</b></td>";
      echo "</tr><tr><td><input type='text' class='boleano OBG' name='hepatopatia' value='{$dados->hepatopatia}' {$acesso} /><b>Hepatopatia</b></td>";
      echo "<td><input type='text' class='boleano OBG' name='nefropatia' value='{$dados->nefropatia}' {$acesso} /><b>Nefropatia</b></td>";
      echo "</tr><tr><td><input type='text' class='boleano OBG' name='obesidade' value='{$dados->obesidade}' {$acesso} /><b>Obesidade</b></td>";
      echo "<td><input type='text' class='boleano OBG' name='pneumopatia' value='{$dados->pneumopatia}' {$acesso} /><b>Pneumopatia</b></td>";
      echo "</tr><thead><tr>"; 
      echo "<th>Alergia medicamentosa (S/N) <input type='text' class='boleano OBG' name='palergiamed' value='{$dados->alergiamed}' {$acesso} /></th>";
      echo "<th>Alergia alimentar (S/N) <input type='text' class='boleano OBG' name='palergiaalim' value='{$dados->alergiaalim}' {$acesso} /></th>";
      echo "</tr></thead>";
      echo "<tr><td><input type='text' name='alergiamed' size=50 readonly='readonly' value='{$dados->tipoalergiamed}' /></td>";
      echo "<td><input type='text' name='alergiaalim' size=50 readonly='readonly' value='{$dados->tipoalergiaalim}' /></td></tr>";
      echo "<thead><tr>"; 
      $score = "SCORE<input type='text' size='2' name='shosp' value='{$dados->shosp}' {$acesso} />";
      echo "<th colspan='2' >Condi&ccedil;&otilde;es do paciente pr&eacute;-interna&ccedil;&atilde;o hospitalar {$score} </th>";
      echo "</tr></thead>";
      echo "<tr><td colspan='2' ><b>Locomo&ccedil;&atilde;o:</b>";
      echo "<input type='radio' class='hosp' name='prelocohosp' value=3 ".(($dados->prelocohosp=='3')?"checked":"")." {$enabled} /> Deambulava";
      echo "<input type='radio' class='hosp' name='prelocohosp' value=2 ".(($dados->prelocohosp=='2')?"checked":"")." {$enabled} /> Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)";
      echo "<input type='radio' class='hosp' name='prelocohosp' value=1 ".(($dados->prelocohosp=='1')?"checked":"")." {$enabled} /> Restrito ao leito";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>Autonomia para higiene pessoal:</b>";
      echo "<input type='radio' class='hosp' name='prehigihosp' value=2 ".(($dados->prehigihosp=='2')?"checked":"")." {$enabled} /> Sim";
      echo "<input type='radio' class='hosp' name='prehigihosp' value=1 ".(($dados->prehigihosp=='1')?"checked":"")." {$enabled} /> N&atilde;o (Oral, deje&ccedil;&otilde;es, mic&ccedil&atilde;o e banho)";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>N&iacute;vel de consci&ecirc;ncia:</b>";
      echo "<input type='radio' class='hosp' name='preconshosp' value=3 ".(($dados->preconshosp=='3')?"checked":"")." {$enabled} /> LOTE";
      echo "<input type='radio' class='hosp' name='preconshosp' value=2 ".(($dados->preconshosp=='2')?"checked":"")." {$enabled} /> Desorientado";
      echo "<input type='radio' class='hosp' name='preconshosp' value=1 ".(($dados->preconshosp=='1')?"checked":"")." {$enabled} /> Inconsciente";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>Alimenta&ccedil;&atilde;o:</b>";
      echo "<input type='radio' class='hosp' name='prealimhosp' value=4 ".(($dados->prealimhosp=='4')?"checked":"")." {$enabled} /> N&atilde;o assistida";
      echo "<input type='radio' class='hosp' name='prealimhosp' value=3 ".(($dados->prealimhosp=='3')?"checked":"")." {$enabled} /> Assistida oral";
      echo "<input type='radio' class='hosp' name='prealimhosp' value=2 ".(($dados->prealimhosp=='2')?"checked":"")." {$enabled} /> Enteral";
      echo "<input type='radio' class='hosp' name='prealimhosp' value=1 ".(($dados->prealimhosp=='1')?"checked":"")." {$enabled} /> Parenteral";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>&Uacute;lceras de press&atilde;o:</b>";
      echo "<input type='radio' class='hosp' name='preulcerahosp' value=2 ".(($dados->preulcerahosp=='2')?"checked":"")." {$enabled} /> N&atilde;o";
      echo "<input type='radio' class='hosp' name='preulcerahosp' value=1 ".(($dados->preulcerahosp=='1')?"checked":"")." {$enabled} /> Sim";
      echo "</td></tr>";
      echo "<thead><tr>";
      $score = "SCORE<input type='text' readonly size='2' name='sdom' value='{$dados->sdom}' {$acesso} />";
      echo "<th colspan='2' >Condi&ccedil;&otilde;es do paciente pr&eacute;-interna&ccedil;&atilde;o domiciliar. {$score}</th>";
      echo "</tr></thead>";
      echo "<tr><td colspan='2' ><b>Locomo&ccedil;&atilde;o:</b>";
      echo "<input type='radio' class='domic' name='prelocodomic' value=3 ".(($dados->prelocodomic=='3')?"checked":"")." {$enabled} /> Deambulava";
      echo "<input type='radio' class='domic' name='prelocodomic' value=2 ".(($dados->prelocodomic=='2')?"checked":"")." {$enabled} /> Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)";
      echo "<input type='radio' class='domic' name='prelocodomic' value=1 ".(($dados->prelocodomic=='1')?"checked":"")." {$enabled} /> Restrito ao leito";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>Autonomia para higiene pessoal:</b>";
      echo "<input type='radio' class='domic' name='prehigidomic' value=2 ".(($dados->prehigidomic=='2')?"checked":"")." {$enabled} /> Sim";
      echo "<input type='radio' class='domic' name='prehigidomic' value=1 ".(($dados->prehigidomic=='1')?"checked":"")." {$enabled} /> N&atilde;o (Oral, deje&ccedil;&otilde;es, mic&ccedil&atilde;o e banho)";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>N&iacute;vel de consci&ecirc;ncia:</b>";
      echo "<input type='radio' class='domic' name='preconsdomic' value=3 ".(($dados->preconsdomic=='3')?"checked":"")." {$enabled} /> LOTE";
      echo "<input type='radio' class='domic' name='preconsdomic' value=2 ".(($dados->preconsdomic=='2')?"checked":"")." {$enabled} /> Desorientado";
      echo "<input type='radio' class='domic' name='preconsdomic' value=1 ".(($dados->preconsdomic=='1')?"checked":"")." {$enabled} /> Inconsciente";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>Alimenta&ccedil;&atilde;o:</b>";
      echo "<input type='radio' class='domic' name='prealimdomic' value=4 ".(($dados->prealimdomic=='4')?"checked":"")." {$enabled} /> N&atilde;o assistida";
      echo "<input type='radio' class='domic' name='prealimdomic' value=3 ".(($dados->prealimdomic=='3')?"checked":"")." {$enabled} /> Assistida oral";
      echo "<input type='radio' class='domic' name='prealimdomic' value=2 ".(($dados->prealimdomic=='2')?"checked":"")." {$enabled} /> Enteral";
      echo "<input type='radio' class='domic' name='prealimdomic' value=1 ".(($dados->prealimdomic=='1')?"checked":"")." {$enabled} /> Parenteral";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>&Uacute;lceras de press&atilde;o:</b>";
      echo "<input type='radio' class='domic' name='preulceradomic' value=2 ".(($dados->preulceradomic=='2')?"checked":"")." {$enabled} /> N&atilde;o";
      echo "<input type='radio' class='domic' name='preulceradomic' value=1 ".(($dados->preulceradomic=='1')?"checked":"")." {$enabled} /> Sim";
      echo "</td></tr>";
      echo "<thead><tr>"; 
      $score = "SCORE<input type='text' readonly size='2' name='sobj' value='{$dados->sobj}' {$acesso} />";
      echo "<th colspan='2' >Objetivo da continuidade da interna&ccedil;&atilde;o domiciliar. {$score}</th>";
      echo "</tr></thead>";
      echo "<tr><td colspan='2' ><b>Locomo&ccedil;&atilde;o:</b>";
      echo "<input type='radio' class='obj' name='objlocomocao' value=3 ".(($dados->objlocomocao=='3')?"checked":"")." {$enabled} /> Deambulava";
      echo "<input type='radio' class='obj' name='objlocomocao' value=2 ".(($dados->objlocomocao=='2')?"checked":"")." {$enabled} /> Locomovia-se com Aux&iacute;lio (Muletas ou Cadeiras)";
      echo "<input type='radio' class='obj' name='objlocomocao' value=1 ".(($dados->objlocomocao=='1')?"checked":"")." {$enabled} /> Restrito ao leito";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>Autonomia para higiene pessoal:</b>";
      echo "<input type='radio' class='obj' name='objhigiene' value=2 ".(($dados->objhigiene=='2')?"checked":"")." {$enabled} /> Sim";
      echo "<input type='radio' class='obj' name='objhigiene' value=1 ".(($dados->objhigiene=='1')?"checked":"")." {$enabled} /> N&atilde;o (Oral, deje&ccedil;&otilde;es, mic&ccedil&atilde;o e banho)";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>N&iacute;vel de consci&ecirc;ncia:</b>";
      echo "<input type='radio' class='obj' name='objcons' value=3 ".(($dados->objcons=='3')?"checked":"")." {$enabled} /> LOTE";
      echo "<input type='radio' class='obj' name='objcons' value=2 ".(($dados->objcons=='2')?"checked":"")." {$enabled} /> Desorientado";
      echo "<input type='radio' class='obj' name='objcons' value=1 ".(($dados->objcons=='1')?"checked":"")." {$enabled} /> Inconsciente";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>Alimenta&ccedil;&atilde;o:</b>";
      echo "<input type='radio' class='obj' name='objalimento' value=4 ".(($dados->objalimento=='4')?"checked":"")." {$enabled} /> N&atilde;o assistida";
      echo "<input type='radio' class='obj' name='objalimento' value=3 ".(($dados->objalimento=='3')?"checked":"")." {$enabled} /> Assistida oral";
      echo "<input type='radio' class='obj' name='objalimento' value=2 ".(($dados->objalimento=='2')?"checked":"")." {$enabled} /> Enteral";
      echo "<input type='radio' class='obj' name='objalimento' value=1 ".(($dados->objalimento=='1')?"checked":"")." {$enabled} /> Parenteral";
      echo "</td></tr>";
      echo "<tr><td colspan='2' ><b>&Uacute;lceras de press&atilde;o:</b>";
      echo "<input type='radio' class='obj' name='objulcera' value=2 ".(($dados->objulcera=='2')?"checked":"")." {$enabled} /> N&atilde;o";
      echo "<input type='radio' class='obj' name='objulcera' value=1 ".(($dados->objulcera=='1')?"checked":"")." {$enabled} /> Sim";
      echo "</td></tr>";
      echo "<thead><tr>"; 
      echo "<th colspan='2' >Interna&ccedil;&otilde;es dos &uacute;ltimos 12 meses</th>";
      echo "</tr></thead>";
      echo "<tr><td colspan='2' ><b>Qtd:</b><input type='text' size='2' class='OBG numerico' name='qtdanteriores' value='{$dados->qtdinternacoes}' {$acesso}/>";
      echo "<b>Motivos:</b><input type='text' name='historicointernacao' size=100 readonly='readonly' value='{$dados->historico}' {$acesso} /></td></tr>";
      echo "<thead><tr>"; 
      echo "<th colspan='2' >Parecer M&eacute;dico</th>";
      echo "</tr></thead>";
      echo "<tr><td colspan='2' >";
      echo "<input type='radio' name='parecer' value='0' ".(($dados->parecer=='0')?"checked":"")." {$enabled} /> Favor&aacute;vel";
      echo "<input type='radio' name='parecer' value='1' ".(($dados->parecer=='1')?"checked":"")." {$enabled} /> Favor&aacute;vel com resalvas";
      echo "<input type='radio' name='parecer' value='2' ".(($dados->parecer=='2')?"checked":"")." {$enabled} /> Contr&aacute;rio";
      echo "<br/>Justificativa:<br/><textarea cols=100 rows=5 name='justparecer' class='OBG' {$acesso} >{$dados->justificativa}</textarea>";
      echo "</td></tr>";
      echo "</table></div>";
      echo "</form>";
      if($acesso != "readonly") echo "<br/><button id='salvar-capmed'>Salvar</button>";
  }
    public function PegarData($data=null){
        if($data==null){
            $data = date('d/m/Y');
        }else{
            $quebrar = explode('-',$data);

            //print_r(explode('-',$data));

            if(strlen($quebrar[0])==4){
                $data = substr($data,8,2).'/'.substr($data,5,2).'/'.substr($data,0,4);
            }else{
                $data = substr($data,0,2).'/'.substr($data,3,2).'/'.substr($data,6,4);

            }
        }
        return $data;
    }

    public function PegarHora($data=null){
        if($data==null){
            $hora = '';
        }else{
            $hora = substr($data,11,2).':'.substr($data,14,2).':'.substr($data,17,2);
        }
        return $hora;
    }
  
  //! Lista as captações de um paciente com ID
  public function listar_capitacao_medica($id){
  	$id = anti_injection($id,"numerico");
  	$sql = "SELECT DATE_FORMAT(c.data,'%d/%m/%Y') as fdata, c.id, u.nome, p.nome as paciente FROM capmedica as c, usuarios as u, clientes as p 
  			WHERE paciente = '{$id}' AND c.usuario = u.idUsuarios AND p.idClientes = c.paciente ORDER BY data ";
  	$r = mysql_query($sql);
  	$flag = true;
  	while($row = mysql_fetch_array($r)){
  		if($flag) {
  			$p = ucwords(strtolower($row["paciente"]));
  			echo "<center><h2>{$p}</h2></center>";
  			echo "<br/><br/>";
  			echo "<a href='?adm=paciente&act=newcapmed&id={$id}'>Nova ficha</a>";
	  		echo "<br/><table class='mytable' width=100% >";
    	  	echo "<thead><tr>"; 
	    	echo "<th>Usu&aacute;rio</th>";
		    echo "<th>Data</th>";
		    echo "<th>Op&ccedil;&otilde;es</th>";
      		echo "</tr></thead>";
      		$flag = false;
  		}
  		$u = ucwords(strtolower($row["nome"]));
  		echo "<tr><td>{$u}</td><td>{$row['fdata']}</td>";
  		echo "<td><a href=?adm=paciente&act=capmed&id={$row['id']}><img src='../utils/details_16x16.png' title='Detalhes' border='0'></a></td></tr>";
  	}
  	if($flag){
  		$this->nova_capitacao_medica($id);
  		return;
  	}
  	echo "</table>";
  }
}

?>