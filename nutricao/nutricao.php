<?php
$id = session_id();
if(empty($id))
    session_start();

include_once('../db/config.php');
include_once('../utils/codigos.php');

require __DIR__ . '/../vendor/autoload.php';

use \App\Models\Administracao\Paciente as ModelPaciente;

/*!
 * Classe Nutricionistas, responsável por exibir as opções relacionadas com o módulo "médicos" do sistema.
 *
 */

class Nutricionistas{

    /*! Quantidade de itens prescritos, serve de apoio no momento de carregar uma prescrição antiga.
     * Primeiro é preenchido o array com as quantidades, posteriormente é gerado o código html com as respectivas quantidades.
     * No códgio html, essas quantidades são armazenadas como atributos do div correspondente a sua aba.
     */
    private $qtds = array('0'=>'0','1'=>'0','2'=>'0','3'=>'0','12'=>'0','14'=>'0','s'=>'0');
    //! Abas e seus respectivos códigos
    private $abas = array('0'=>'Repouso','1'=>'Dieta','2'=>'Cuidados Especiais','3'=>'Soros e Eletr&oacute;litos',
        '4'=>'Antibi&oacute;ticos Injet&aacute;veis','5'=>'Injet&aacute;veis IV','6'=>'Injet&aacute;veis IM',
        '7'=>'Injet&aacute;veis SC','8'=>'Drogas inalat&oacute;rias','9'=>'Nebuliza&ccedil;&atilde;o',
        '10'=>'Drogas Orais/Enterais','11'=>'Drogas T&oacute;picas', '12'=>'F&oacute;mulas','13'=>'Oxigenoterapia','14'=>'Dieta Enterais', '15' => 'Materiais');
    // private $abas = array('0'=>'Repouso','1'=>'Dieta','2'=>'Cuidados Especiais','12'=>'F&oacute;mulas','14'=>'Dieta Enterais');
    private $frequencia = array(); private $vadm = array();private $frequenciaoxi = array(); private $frequencia_emergencial = array();
    private $equipe = array(); private $soros = array();
    private $dietas = array(); private $repousos = array();
    private $inicio = ""; private $fim = "";private $tipo_presc = "";

    //!contrutor da classe, inicializa os dropbox.
    function Nutricionistas(){
        $this->inicio = date("d/m/Y");
        $this->fim = date("d/m/Y",strtotime("+1 Week ".date("Y-m-d") ));
        $sql = "SELECT * FROM frequencia ORDER BY frequencia";
        $result = mysql_query($sql);
        $this->frequencia[''] = -1;
        while($row = mysql_fetch_array($result)){
            $this->frequencia[$row['frequencia']] = $row['id'];
        }
        $sql = "SELECT * FROM frequencia WHERE id='21' OR id='20' OR id='26' ORDER BY frequencia;";
        $result = mysql_query($sql);
        $this->frequenciaoxi[''] = -1;
        while($row = mysql_fetch_array($result)){
            $this->frequenciaoxi[$row['frequencia']] = $row['id'];
        }
        $sql = "SELECT * FROM frequencia WHERE  id in (2,3,4,5,6,7,8,9,10,11,12,13) ORDER BY id;";
        $result = mysql_query($sql);
        $this->frequencia_emergencial[''] = -1;
        while($row = mysql_fetch_array($result)){
            $this->frequencia_emergencial[$row['frequencia']] = $row['id'];
        }
        $sql = "SELECT * FROM viaadm";
        $result = mysql_query($sql);
        while($row = mysql_fetch_array($result)){
            $this->vadm[$row['via']] = $row['id'];
        }
        $sql = "SELECT * FROM cuidadosespeciais WHERE id in (14, 19, 20) ORDER BY cuidado";
        $result = mysql_query($sql);
        $this->equipe[''] = -1;
        while($row = mysql_fetch_array($result)){

            $this->equipe[$row['cuidado']] = $row['id'];
        }
        $sql = "SELECT * FROM soros ORDER BY soro";
        $result = mysql_query($sql);
        $this->soros[''] = -1;
        while($row = mysql_fetch_array($result)){
            $this->soros[$row['soro']] = $row['id'];
        }
        $sql = "SELECT * FROM dietas ORDER BY dieta";
        $result = mysql_query($sql);
        $this->dietas[''] = -1;
        while($row = mysql_fetch_array($result)){
            $this->dietas[$row['dieta']] = $row['id'];
        }
        $sql = "SELECT * FROM repousos ORDER BY repouso";
        $result = mysql_query($sql);
        $this->repousos[''] = -1;
        while($row = mysql_fetch_array($result)){
            $this->repousos[$row['repouso']] = $row['id'];
        }
    }
    //! Gera dropbox com as unidades de medida
    private function combo_um($extra = NULL){
        $html = "<select name='um' style='background-color:transparent;' $extra >";
        $html .= "<option value='-1'></option>";
        $sql = "SELECT * FROM umedidas ORDER BY unidade";
        $result = mysql_query($sql);
        while($row = mysql_fetch_array($result)){
            $html .= "<option value='{$row['id']}' >{$row['unidade']}</optioin>";
        }
        $html .= "</select>";
        return $html;
    }
    //! Gera os botões de navegação das abas de prescrição
    private function botoes_navegacao($a,$p){
        $b = "";
        if($p <> -1) $b .= "<button class='next-tab' rel='$p' style='float:right' >Pr&oacute;ximo >></button>";
        if($a <> -1) $b .= "<button class='prev-tab' rel='$a' style='float:right' ><< Anterior</button>";
        return $b;
    }
    //! Código do dialog para prescrição de medicamentos.
    private function formulario(){
        echo "<div id='formulario'  tipo='' classe='' >";
        echo alerta();
        echo "<p><b>Busca:</b><br/><input type='text' name='busca' id='busca' /><button id='outro' >outro</button>";
        echo "<div id='opcoes-itens'></div>";
        echo "<input type='hidden' name='brasindice_id' id='brasindice_id' value='-1' />";
        echo "<input type='hidden' name='cod' id='cod' value='-1' />";
        echo "<p><b>Princ&iacute;pio:</b><input type='text' name='nome' id='nome' class='OBG' size='80' />";
        //echo "<b>Apresenta&ccedil;&atilde;o:</b><input type='text' name='apr' id='apr' class='OBG' />";
        echo "<br/><b>Dosagem:</b> <input size='5' type='text' name='dose' class='OBG ' /> ".$this->combo_um("class='COMBO_OBG'");
        echo " <b>Frequ&ecirc;ncia: </b>".combo_box('frequencia',$this->frequencia,'-1',"class='COMBO_OBG'");
        echo " <b>Via ADM:</b> ".$this->viaadm('vadm-formulario','dieta','-1',"class='COMBO_OBG'");
//    echo $this->aprazamento();
        echo $this->periodo();
        echo "<br/><b>Observa&ccedil;&atilde;o:</b> <input type='text' name='obs' id='obs' size='60' />";
        echo "<br/><button id='add-item' tipo='' class='' >+ incluir</button></div>";
    }

    //! Tela de dialog para pre_prescri��o de medicamentos.
    private function pre_prescricao(){

        echo "<div id='pre-presc'   classe='' presc=''   CodPaciente='' title='Nova Prescri&ccedil;&atilde;o de Nutrição'>";
        echo alerta();
        echo "<form method='post' action='?view=prescricoes'>";
        echo "<input type='hidden' name='paciente' id='paciente' value=''>";
        echo "<input type='hidden' name='antigas' id='antiga' value=''>";

        echo "<fieldset><legend><b>Paciente</b></legend><span id='NomePacientePresc' ></span></fieldset><br/>";

        echo $this->periodo(true);
        echo $this->tipo_prescricao();

        echo "<br/><button id='iniciarprescricao' tipo='' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Iniciar</span></button></form></div>";

    }


    //! Gera as opções de via de administração
    private function viaadm($name,$tipo,$checked,$extra = NULL){
        $sql = "SELECT v.id, UPPER(v.via) as via FROM viaadm as v WHERE $tipo = '1' ORDER BY via";
//  	echo $sql;
        $html = "<select name='{$name}' style='background-color:transparent;' $extra >";
        $html .= "<option value='-1'></option>";
        $result = mysql_query($sql);
        while($row = mysql_fetch_array($result)){
            if($row['via'] == $checked)
                $html .= "<option value='{$row['id']}' SELECTED />{$row['via']}</option>";
            else $html .= "<option value='{$row['id']}' />{$row['via']}</option>";
        }
        $html .= "</select>";
        return $html;
    }

    //! Gera o compo de aprazamento, hora início.
    /*! NÃO UTILIZADO, a responsabilidade pelo aprazamento passou para as enfermeiras. */
    private function aprazamento(){
        echo "<b>Hora In&iacute;cio:</b>";
        echo "<input type='text' name='aprazamento' class='hora OBG' maxlength='5' size='5' />";
    }

    // Gera dois compos inputs para inserir um período de data
    private function periodo($dialog = false, $interno = false){
        //SE A ROTINA QUE ESTÁ CHAMANDO É O DIALOG DE INICIAR UMA PRESCRIÇÃO
        $inicioDialog = $dialog ? "id='inicio' dialog='true'" : '';
        $fimDialog = $dialog ? " id='fim' dialog='true'" : '';

        //SE A ROTINA QUE ESTÁ CHAMANDO É A PÁGINA INTERNA DE PRESCRIÇÕES
        $inicioInterno = $interno ? "prescricao-inicio' readonly" : "inicio-periodo'";
        $fimInterno = $interno ? "prescricao-fim' readonly" : "fim-periodo'";

        echo "<fieldset style='width:480px;text-align:center;'><legend><b>Per&iacute;odo</b></legend>";
        echo "DE".
            "<input type='text' name='inicio' value='{$this->inicio}' i='{$this->inicio}' maxlength='10' size='10' class='OBG periodo {$inicioInterno} {$inicioDialog} />".
            " AT&Eacute; <input type='text' name='fim' value='{$this->fim}' f='{$this->fim}' maxlength='10' size='10' class='OBG periodo {$fimInterno} {$fimDialog} /></fieldset>";
    }

    private function tipo_prescricao(){
        echo "<p></p>";
        echo "<fieldset style='width:340px;'>";
        echo "<legend><b>Tipo da Prescri&ccedil;&atilde;o: </b></legend>";
        echo "<input type='radio'  name='tipo' value='5' class='OBG tipo_presc' checked />Peri&oacute;dica</br>";
        echo "<input type='radio'  name='tipo' value='6' class='OBG tipo_presc' />Aditiva</br>";


        echo "</fieldset>";

    }


    private function posologia(){
        echo "<div class='posologia' >";
        echo "<br/><b>Posologia:</b>";
        echo "<br/><b>Via ADM:</b> ".radio_box('vadm',$this->vadm,'iv');
        echo "<br/><b>Frequ&ecirc;ncia:</b> ".combo_box('frequencia',$this->frequencia,'1h');
        echo "</div>";
    }

    //! Gera o dropbox com a lista de pacientes que pode ser prescrito pelo médico.
    private function pacientes(){

        $medico = $_SESSION['id_user'];
        $result = mysql_query("SELECT c.* FROM clientes as c, equipePaciente as e WHERE e.paciente = c.idClientes AND e.funcionario = {$medico} AND e.fim  IS NULL ORDER BY nome;");
        $result = mysql_query("SELECT c.* FROM clientes as c WHERE c.status = 4 AND (c.empresa in{$_SESSION['empresa_user']} OR '1' in{$_SESSION['empresa_user']} ) ORDER BY nome;");

        echo "<p><b>Localizar Paciente</b></p>";
        echo "<select data-placeholder='Selecione um paciente...' name='paciente' style='background-color:transparent;width:362px' id='selPaciente'>";
        echo "<option value='-1'></option>";
        while($row = mysql_fetch_array($result))
        {
            $paciente = strtoupper($row['nome']);
            echo "<option value='{$row['idClientes']}'>({$row['codigo']}) {$paciente}</option>";
        }
        echo "</select>";
    }

    //! Retorna o nome de um paciente com determinado ID
    private function pacienteNome($id){
        $result = mysql_query("SELECT nome FROM clientes WHERE idClientes='$id' ORDER BY nome;");
        while($row = mysql_fetch_array($result))
        {
            return ucwords(strtolower($row['nome']));
        }
    }

    //! Gera o menu da parte central.
    //Atualiza��o Eriton 20-05-2013
    private function menu(){
        echo "<div><center><h1>Nutrição</h1></center></div>";
        echo "<p><a href='?view=listar'>Pacientes</a></p>";
        echo "<p><a href='?view=buscar'>Prescri&ccedil;&otilde;es</a>";

    }

    //! Gera a aba repouso
    private function repouso(){
        echo "<div id='div-repouso' class='tab-presc' qtd='{$this->qtds[0]}' title='Repouso' tipo='0' >";
        echo "<center><h2>Repouso</h2></center>";
        echo alerta();
        echo "<br/><b>Repouso:</b>".combo_box('repouso',$this->repousos,'-1',"class='COMBO_OBG' ");
        echo "<br/><b>Frequ&ecirc;ncia:</b> ".combo_box('frequencia',$this->frequencia,'-1',"class='COMBO_OBG'");
        echo $this->periodo(false, true);
        echo "<br/><b>Observa&ccedil;&atilde;o:</b>".input('text','obs','obs',"size='50'");
        echo "<br/>".button('add-repouso','+ incluir',"tipo='0' label='Repouso' class='incluir' ");
        echo $this->botoes_navegacao(-1,1);
        echo "</div>";
    }

    //! Gera a aba dieta
    private function dieta(){
        echo "<div id='div-dieta' class='tab-presc' qtd='{$this->qtds[1]}' title='Dieta' tipo='1' >";
        echo "<center><h2>Dieta Oral</h2></center>";
        echo alerta();
        echo "<br/><b>Dieta:</b>".combo_box('dieta',$this->dietas,'-1',"class='COMBO_OBG' ");
        echo "<br/><b>Via ADM:</b> ".$this->viaadm('vadm-dieta','dieta','-1');
        echo "<br/><b>Frequ&ecirc;ncia:</b> ".combo_box('frequencia',$this->frequencia,'-1',"class='COMBO_OBG'");
//  	echo $this->aprazamento();
        echo $this->periodo(false, true);
        echo "<br/><b>Observa&ccedil;&atilde;o:</b>".input('text','obs','obs',"size='50'");
        echo "<br/>".button('add-dieta','+ incluir',"tipo='1' label='Dieta' ");
        echo $this->botoes_navegacao(0,2);
        echo "</div>";
    }

    //! Gera a aba equipe multidiscilinar
    private function equipe_multidisciplinar(){
        echo "<div id='div-cuidados' class='tab-presc' qtd='{$this->qtds[2]}' title='Cuidados Especiais' tipo='2' >";
        echo "<center><h2>Cuidados Especiais</h2></center>";
        echo alerta();
        echo "<p><b>Cuidados Especiais:</b>".combo_box('cuidados',$this->equipe,'-1',"class='COMBO_OBG' ");
        echo "<br/><b>Frequ&ecirc;ncia: </b>".combo_box('frequencia',$this->frequencia,'-1',"class='COMBO_OBG'");
        echo $this->periodo(false, true);
        echo "<br/><b>Observa&ccedil;&atilde;o:</b>".input('text','obs','obs',"size='50'");
        echo "<p>".button('add-cuidados','+ incluir',"tipo='2' label='Cuidados' class='incluir'");
        echo $this->botoes_navegacao(1,3);
        echo "</div>";
    }

    /* //! Gera a aba de soros e eltrólitos
     private function soros_eletrolitos(){
         echo "<div id='div-soros' class='tab-presc' qtd='{$this->qtds[3]}' title='Soros e Eletr&oacute;litos' tipo='3' >";
         echo "<center><h2>Soros e Eletr&oacute;litos</h2></center>";
         echo alerta();
       echo "<p><b>Soros e Eletr&oacute;litos:</b>".combo_box('soros',$this->soros,'-1',"class='COMBO_OBG' ");
       echo "<br/><b>Via ADM:</b> ".$this->viaadm('vadm-soro','soro','-1');
         echo "<br/><b>Frequ&ecirc;ncia:</b> ".combo_box('frequencia',$this->frequencia,'-1',"class='COMBO_OBG'");
       echo "<b>Fluxo:</b><input size='5' type='text' class=' OBG' name='fluxo' />".$this->combo_um("class='COMBO_OBG'")."";
       echo $this->periodo();
       echo "<br/><b>Observa&ccedil;&atilde;o:</b>".input('text','obs','obs',"size='50'");
       echo "<p>".button('add-soro','+ incluir',"tipo='3' label='Soros' class='incluir' ");
         echo $this->botoes_navegacao(2,4);
         echo "</div>";
     }

     //! Gera a aba de antibióticos injetáveis
     private function antibioticos_injetaveis(){

         echo "<div id='div-antibio-inj' class='tab-presc' qtd='{$this->qtds[4]}' title='Antibi&oacute;ticos Injet&aacute;veis' tipo='4' >";
         echo "<center><h2>Antibi&oacute;ticos Injet&aacute;veis</h2></center>";
         echo "<button class='show-form' tipo='4' classe='1' label='Antibi&oacute;ticos Injet&aacute;veis' via='{$this->vadm['IV']}' >Novo</button>";
         echo $this->botoes_navegacao(3,5);
         echo "</div>";
     }

     //! Gera a aba de injetáveis EV
     private function injetaveis_ev(){
         echo "<div id='div-inj-ev' class='tab-presc' qtd='{$this->qtds[5]}' title='Injet&aacute;veis IV' tipo='5' >";
         echo "<center><h2>Injet&aacute;veis IV</h2></center>";
         echo "<button class='show-form' tipo='5' classe='2' label='Injet&aacute;veis IV' via='{$this->vadm['IV']}' >Novo</button>";
         echo $this->botoes_navegacao(4,6);
         echo "</div>";
     }

     //! Gera a aba de injetáveis IM
     private function injetaveis_im(){
         echo "<div id='div-inj-im' class='tab-presc' qtd='{$this->qtds[6]}' title='Injet&aacute;veis IM' tipo='6' >";
         echo "<center><h2>Injet&aacute;veis IM</h2></center>";
         echo "<button class='show-form' tipo='6' classe='3' label='Injet&aacute;veis IM' via='{$this->vadm['IM']}' >Novo</button>";
         echo $this->botoes_navegacao(5,7);
         echo "</div>";
     }

     //! Gera a aba de injetáveis SC
     private function injetaveis_sc(){
         echo "<div id='div-inj-sc' class='tab-presc' qtd='{$this->qtds[7]}' title='Injet&aacute;veis SC' tipo='7' >";
         echo "<center><h2>Injet&aacute;veis SC</h2></center>";
         echo "<button class='show-form' tipo='7' classe='4' label='Injet&aacute;veis SC' via='{$this->vadm['SC']}' >Novo</button>";
         echo $this->botoes_navegacao(6,8);
         echo "</div>";
     }

     //! Gera a aba de drogas inalatórias
     private function drogas_inalatorias(){
         echo "<div id='div-drogas-inalatorias' class='tab-presc' qtd='{$this->qtds[8]}' title='Drogas Inalat&oacute;rias' tipo='8' >";
         echo "<center><h2>Drogas Inalat&oacute;rias </h2></center>";
         echo "<button class='show-form' tipo='8' classe='5' label='Drogas Inalat&oacute;rias' via='-1' via='{$this->vadm['SC']}'>Novo</button>";
         echo $this->botoes_navegacao(7,9);
         echo "</div>";
     }

     //! Gera a aba de nebulização
     private function nebulizacao(){
         ob_start();
         echo "<div id='div-nebulizacao' class='tab-presc' qtd='{$this->qtds[9]}' title='Nebuliza&ccedil;&atilde;o' tipo='9' >";
         echo "<center><h2>Nebuliza&ccedil;&atilde;o</h2></center>";
         echo alerta();
         echo "<br/><b>Composi&ccedil;&atilde;o</b>";
         echo "<table>";
         echo "<tr class='comp-nebulizacao' id='comp-nebulizacao'><td><input type='checkbox' class='comp-nebulizacao' id='Atrovent' name='Atrovent'></td>
              <td>Atrovent</td><td><input size='5' type='text' class='' disabled='disabled' name='neb'/></td><td>".$this->combo_um()."</td></tr>";
         echo "<tr class='comp-nebulizacao' id='comp-nebulizacao2'><td><input type='checkbox' class='comp-nebulizacao'  id='Berotec' name='Berotec'></td>
              <td>Berotec</td><td><input size='5' type='text' class='' disabled='disabled'  name='neb'/></td><td>".$this->combo_um("id='comp-nebulizacao2select'")."</td></tr>";

         echo "</table>";
         echo "<div id='nebulizacao-outros' ><table><tr><td><input type='checkbox' id='check-outro-neb' class='comp-nebulizacao-outro'></td><td>Outros</td></tr>";
         echo "<tr><td></td><td>Busca:<input type='text' name='busca-outro-neb' id='busca-outro-neb' disabled='disabled' /></td></tr>";
         echo "<tr><td></td><td><input type='text' name='nome-outro-neb' id='nome-outro-neb' size='80' disabled='disabled' /></td>";
         echo "<td></td><td><input size='5' type='text' name='dose-outro-neb' id='dose-outro-neb' class='' disabled='disabled' /></td><td>".$this->combo_um("id='um-outro-neb' class='nebul-outro'")."</td></tr></table></div>";
         echo "<b>Dilui&ccedil;&atilde;o</b><select class='OBG'  name='diluicao-nebulizacao' id='diluicao-nebulizacao'>
         <option></option>
         <option dil='SF 0,9%'>SF 0,9% </option>
         <option dil='AD'> AD </option></selecte>";*/
    /*echo "<input name='diluicao-nebulizacao' type='radio' value='sf' dil='SF 0,9%' checked />SF 0,9%
          <input name='diluicao-nebulizacao' value='ad' dil='AD' type='radio' />AD";*/
    /*echo "<input class='OBG' name='qtd-diluicao' id='qtd-diluicao' size='5' type='text' />".$this->combo_um("id='um-diluicao-neb' class='COMBO_OBG' ");
    echo "<br/><b>Frequ&ecirc;ncia:</b> ".combo_box('frequencia',$this->frequencia,'-1',"id='frequencia' name='frequencia' class='COMBO_OBG'" );
    echo $this->periodo();
    echo "<br/><b>Observa&ccedil;&atilde;o:</b>".input('text','obs','obs',"size='50'");
    echo "<br/>".button('add-nebulizacao','+ incluir',"tipo='9' label='Nebuliza&ccedil;&atilde;o' class='incluir'");
    echo $this->botoes_navegacao(8,10);
    echo "<div id='dialog-imprimir' title='Imprimir'>";
    echo "<center><h3>Visualizar Prescri&ccedil;&atilde;o?</h3></center>";
    echo "<form action='imprimir.php' method='post' target='_blank'><input type='hidden' value='-1' name='prescricao'><center></center></form>";
    echo "</div>";
    echo "<div id='dialog-incluir-diluente' title='Incluir Nebuliza&ccedil;&atilde;o'>";
    echo "<center><h3>Deseja prescrever uma nebuliza&ccedil;&atilde;o apenas com diluente?</h3></center>";

    //      echo "<form action='imprimir.php' method='post'><input type='hidden' value='-1' name='prescricao'><center><button>Imprimir</button></center></form>";

    echo "</div>";
    echo "</div>";

    ob_end_flush();
}*/

    ///Fim aba nebuliza��o



    //! Gera a aba de drogas orais
    private function drogas_orais(){
        echo "<div id='div-drogas-orais' class='tab-presc' qtd='{$this->qtds[10]}' title='Dietas Enterais' tipo='14' >";
        echo "<center><h2>Dieta Enterais</h2></center>";
        echo "<button class='show-form' tipo='14' classe='6' label='Dietas Enterais' via='{$this->vadm['VO']}' >Novo</button>";
        echo $this->botoes_navegacao(9,11);
        echo "</div>";
    }

    ///Gerar aba Oxigenoterapia
    /* private function oxigenoterapia(){
         ob_start();
         echo "<div id='div-oxigenoterapia' class='tab-presc' qtd='{$this->qtds[13]}' title='Oxigenoterapia' tipo='13' >";
         echo "<center><h2>Oxigenoterapia</h2></center>";
         echo alerta();
         echo "<br/><b>Equipamentos:</b>";
         echo "<table >";
         echo "<tr class='comp-oxigenoterapia' ><td colspan= '3'><input type='radio' id = '1'class='oxi' name='02'  tipo='2' amb='DBVMR (AMBU)' >
         <b>DBVMR (AMBU)</b></td></tr>";
         echo "<tr class='comp-oxigenoterapia' >
         <td ><input type='radio' class='oxi' name='02' tipo='1'  /><b>M&aacute;scara de Venturi.</b></td><td><select  class = 'oxi2' id='s' disabled='disabled' name='dose'>
       <option></option>
         <option value='1'  med='10'  n='50% (10 L/min)' >50% (10 L/min)</option>
         <option value='2'  med='8'   n='40% (8 L/min)' >40% (8 L/min)</option>
         <option value='3'  med='8'    n='35% (8 L/min)'>35% (8 L/min)</option>
         <option value='4'  med='6'    n='31% (6 L/min)'>31% (6 L/min)</option>
         <option value='5'  med='4'    n='28% (4L/min)'>28% (4 L/min)</option>
         <option value='6'  med='4'    n='24% (4 L/min)'>24% (4 L/min)</option>
         </select> </td>

         </tr>";

         echo "<tr class='comp-oxigenoterapia' ><td><input type='radio' name='02'class='oxi' tipo='3' >
         <b>M&aacute;scara Facial com Reservat&oacute;rio.</b></td><td><select  class = 'oxi2' id='s1' disabled='disabled' name='dose'>
       <option></option>
         <option value='1'  med='7'    n='7 L/min' > 7  L/min</option>
         <option value='2'  med='8'    n='8 L/min' > 8  L/min</option>
         <option value='3'  med='9'    n='9 L/min' > 9  L/min</option>
         <option value='4'  med='10'   n='10 L/min'> 10 L/min</option>
         <option value='5'  med='11'   n='11 L/min'> 11 L/min</option>
         <option value='5'  med='12'   n='12 L/min'> 12 L/min</option>
         </td></tr>";


         echo "<tr class='comp-oxigenoterapia' colspan= ''><td><input type='radio'name='02' class='oxi'  tipo='0'>
         <b>Cateter Nasal Tipo Simples.</b></td><td><input name='dose' size='3' toxi='1' type='text'  disabled='disabled' class='oxi2' id='u1' maxlength='1' /><b>1 a 5 l/min</b></td><td width='30%'></td></tr>";
         echo "<tr class='comp-oxigenoterapia' colspan= ''><td><input type='radio'name='02' class='oxi' tipo='0' >
         <b>M&aacute;scara Facial Simples.</b></td><td><input name='dose' size='3' toxi='4' type='text'  disabled='disabled' class='oxi2' id='u4' maxlength='1'  /><b>5 a 8 l/min</b></td><td width='30%'></td></tr>";
         echo "<tr class='comp-oxigenoterapia' colspan= ''><td><input type='radio'name='02' class='oxi' tipo='0' >
         <b>Cateter Nasal Tipo &Oacute;culos.</b></td><td><input name='dose' size='3' toxi='3' type='text'  disabled='disabled' class='oxi2' id='u' maxlength='1'  /><b>1 a 5 l/min</b></td><td width='30%'></td></tr>";
         echo "<tr class='comp-oxigenoterapia' colspan= ''><td><input type='radio'name='02' class='oxi' tipo='0'>
         <b>Concentrador de Oxig&eacute;nio.</b></td><td><input name='dose' size='3'  type='text'  disabled='disabled' class='oxi2' id='u2' maxlength='1' /><b>1 a 6 l/min</b></td><td width='30%'></td></tr>";
         echo "</table>";
         echo "<br/><b>Frequ&ecirc;ncia:</b> ".combo_box('frequencia',$this->frequenciaoxi,'-1',"name='frequenciaoxi' id='frequenciaoxi'");
         echo $this->periodo();
         echo "<br/><b>Observa&ccedil;&atilde;o:</b>".input('text','obs','obs',"size='50'");
         echo "<br/>".button('add-oxigenoterapia','+ incluir',"tipo='13' label='Oxigenoterapia' class='incluir'");
         echo $this->botoes_navegacao(13,-1);
         echo "</div>";
     ob_end_flush();
     }*/

    //! Gera a aba de drogas tópicas
    /* private function drogas_topicas(){


         echo "<div id='div-drogas-topicas' class='tab-presc' qtd='{$this->qtds[11]}' title='Drogas T&oacute;picas' tipo='11' >";
         echo "<center><h2>Drogas T&oacute;picas</h2></center>";
         echo "<button class='show-form' tipo='11' classe='8' label='Drogas T&oacute;picas' via='{$this->vadm['Topico']}'>Novo</button>";
         echo $this->botoes_navegacao(10,12);
         echo "</div>";
     }*/

    //! Gera a aba de fórmulas
    private function formulas(){
        echo "<div id='div-formulas' class='tab-presc' qtd='{$this->qtds[12]}' title='F&oacute;rmulas' tipo='12' >";
        echo "<center><h2>F&oacute;rmulas</h2></center>";
        echo alerta();
        echo "<p><b>Descri&ccedil;&atilde;o</b>".input('text','formula','formula',"class='OBG' size='50'");
        echo "<br/><b>Frequ&ecirc;ncia:</b> ".combo_box('frequencia',$this->frequencia,'-1',"class='COMBO_OBG'");
        echo " <b>Via ADM:</b> ".$this->viaadm('vadm-formula','1','-1');
//  	echo $this->aprazamento();
        echo $this->periodo();
        echo "<br/><b>Observa&ccedil;&atilde;o:</b>".input('text','obs','obs',"size='50'");
        echo "<p>".button('add-formula','+ incluir',"tipo='12' label='F&oacute;rmula' class='incluir'");
        echo $this->botoes_navegacao(11,13);
        echo "</div>";
    }

    //! Gera a aba com as drogas ativas, passíveis de suspensão.
    /*! Essa aba é responsável por exibir os medicamentos em uso pelo paciente.*/
    private function drogas_ativas($id){
        echo "<div id='div-drogas-ativas' class='tab-presc' qtd='{$this->qtds['s']}' title='Suspender' tipo='s' >";
        echo "<center><h2>Suspender itens</h2></center>";
        $html = "<table width=100% id='itens-ativo' class='mytable'><thead><tr>";
        $html .= "<th><b>Itens Ativos</b></th>";
        $html .= "<th width=1% ><input type='checkbox' id='checktodos-susp' name='select' /></th>";
        $sql = "SELECT v.via as nvia, f.frequencia as nfrequencia,
    		i.*, i.id as id, i.inicio, i.fim, DATE_FORMAT(aprazamento,'%H:%i') as apraz, u.unidade as um
    		FROM prescricoes as p, itens_prescricao as i  
    		LEFT OUTER JOIN frequencia as f ON (i.frequencia = f.id) LEFT OUTER JOIN viaadm as v ON (i.via = v.id) LEFT OUTER JOIN umedidas as u ON (i.umdose = u.id) 
    		WHERE i.tipo <> '-1' AND p.paciente = '$id' AND ADDDATE(curdate(), INTERVAL 1 DAY) BETWEEN i.inicio AND i.fim AND p.id = i.idPrescricao 
    		ORDER BY tipo,inicio,aprazamento,nome,apresentacao ASC";
        $result = mysql_query($sql);
        $qtd = 0;
        $lastType = 0;
        $n = 0;
        while($row = mysql_fetch_array($result)){
            if($n++%2==0)
                $cor = '#E9F4F8';
            else
                $cor = '#FFFFFF';

            $html .= "<tr></tr>";
            $i = implode("/",array_reverse(explode("-",$row['inicio'])));
            $f = implode("/",array_reverse(explode("-",$row['fim'])));
            $aprazamento = "";
            ///jeferson 3-8-2012 id_item
            $id_item= $row['id'];
            $dose=$row['dose']." ".$row['um'];
            $dose1=$row['dose'];


            $aba = $this->abas[$row['tipo']];
            if($row['tipo'] <> 0) $aprazamento = $row['apraz'];

            $dados = " tipo='s' cod='{$row['NUMERO_TISS']}'  brasindice_id='{$row['CATALOGO_ID']}' nome='{$row['nome']}' dose='{$dose1}'  um='{$row['umdose']}' frequencia='{$row['frequencia']}' inicio='{$i}' fim='{$f}' obs='{$row['obs']}' id_item='{$id_item}'";
            $linha = "<b>$aba:</b> {$row['nome']} {$row['nvia']} Dose:  {$row['nfrequencia']} {$aprazamento}. Per&iacute;odo: $i até $f OBS: {$row['obs']}";
            $html .= "<tr bgcolor=$cor $dados  class='suspender' >";
            $html .= "<td >$linha </td><td><input type='checkbox' name='select' class='select-item-susp' /></td>";
            $html .= "</tr>";
            $qtd++;
        }
        if($qtd == 0){
            $html .= "<tr><td colspan='2' align='center'><b>N&atilde;o existe drogras ativas.</b></td></tr>";
        }
        $html .= "</table>";
        echo $html;
        echo "<br/><button id='susp-item' >Suspender</button>";
        echo $this->botoes_navegacao(12,14);
        echo "</div>";
    }

    //! Inicializa uma prescrição nova com base em uma anterior.
    private function load_table($prescricao){
        $html = ""; $flag = true;
        $baseinicio = implode("-",array_reverse(explode("/",$this->inicio)));
        $basefim = implode("-",array_reverse(explode("/",$this->fim)));
        //ADDDATE('$baseinicio', INTERVAL DATEDIFF(i.fim,i.inicio) DAY)
        $sql = "SELECT v.via as nvia, f.frequencia as nfrequencia,i.*,  '$baseinicio' as inicio, 
    		'$basefim' as fim, DATE_FORMAT(i.aprazamento,'%H:%i') as apraz, u.unidade as um
    		FROM itens_prescricao as i 
    		LEFT OUTER JOIN frequencia as f ON (i.frequencia = f.id) 
    		LEFT OUTER JOIN viaadm as v ON (i.via = v.id) 
    		LEFT OUTER JOIN umedidas as u ON (i.umdose = u.id) 
    		WHERE idPrescricao = '$prescricao'   
    		ORDER BY tipo,inicio,aprazamento,nome,apresentacao ASC";
        $result = mysql_query($sql);
//    $cor = false;
        $qtd = 0;
        $lastType = 0;
        $z = 0;
        while($row = mysql_fetch_array($result)){
            if($z++%2==0)
                $cor1 = '#E9F4F8';
            else
                $cor1 = '#FFFFFF';
            if($flag){
//		    $this->inicio = implode("/",array_reverse(explode("-",$row['inicio'])));
//		    $this->fim = implode("/",array_reverse(explode("-",$row['fim'])));
                $html .= "<table width=100% id='tabela-itens' inicio='{$this->inicio}' fim='{$this->fim}' class='mytable'><thead><tr>";
                $html .= "<th><b>Itens prescritos</b></th>";
                $html .= "<th width=1% ><input type='checkbox' id='checktodos' name='select' /></th>";
                $flag = false;
            }
            $tipo = $row['tipo'];
            for($t = $lastType; $t <= $tipo; $t++){
                $aba = $this->abas[$t];
                $html .= "<tr  id='itens-$t' ></tr>";
            }
            $this->qtds[$tipo]++;
            $lastType = $row['tipo'] + 1;
            $i = implode("/",array_reverse(explode("-",$row['inicio'])));
            $f = implode("/",array_reverse(explode("-",$row['fim'])));
            $aprazamento = "";
            $aba = $this->abas[$row['tipo']];

            if($row['tipo'] <> 0) $aprazamento = $row['apraz'];
            $dose = ""; $dados_dose = "";
            if($row['dose'] != 0){
                $dose = $row['dose']." ".$row['um'];

                $dose1 = $row['dose'];

                $dados_dose = " dose='{$dose1}' um='{$row['umdose']}' ";
            }
            $dados = " tipo='{$row['tipo']}' cod='{$row['NUMERO_TISS']}' brasindice_id='{$row['CATALOGO_ID']}'   nome='{$row['nome']}' via='{$row['via']}' frequencia='{$row['frequencia']}' inicio='{$i}' fim='{$f}' obs='{$row['obs']}' {$dados_dose}";
            if($row['tipo']== -1){
                $linha= $row['descricao'];
            }else
                $linha = "<b>$aba:</b> {$row['nome']} {$row['nvia']} {$dose} {$row['nfrequencia']} Per&iacute;odo: $i até $f OBS: {$row['obs']} ";
//      	if($cor) $html .= "<tr $dados >";
//      	else $html .= "<tr class='odd dados' $dados >";
            $html .= "<tr bgcolor=$cor1 class='dados' $dados id='id-{$row['id']}' >";
//      	$cor = !$cor;
            $html .= "<td>$linha</td><td><input type='checkbox' name='select' class='select-item' /></td>";
            $html .= "</tr>";
        }
        if($prescricao == "-1"){
            foreach($this->abas as $op => $aba){
                $html .= "<tr id='itens-$op' ></tr>";
            }
        } else {
            for($t = $lastType; $t < count($this->abas); $t++){
                $aba = $this->abas[$t];
                $html .= "<tr id='itens-$t' ></tr>";
            }
        }
        $html .= "</tr></thead>";
        $html .= "</table>";
        return $html;
    }

    //! Exibe a lista de prescrições salvas, mas não finalizadas.
    private function prescricoes_salvas(){
        $dir = "salvas/".$_SESSION["id_user"];
        if(!is_dir($dir)) return;
        $ext = array('htm');
        $arquivos = get_files_dir($dir,$ext);
        if(is_array($arquivos)){
            $tam =  count($arquivos);
            $cont=1;
            echo "<br/>";
            echo "<table width='100%' class='mytable'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th colspan='2'><center>PACIENTES</center></th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            echo "<tr>";
            foreach ($arquivos as $arquivo){

                $nome = explode(".",$arquivo);

                $paciente = $this->pacienteNome($nome[0]);
                if($cont==3){
                    echo "</tr>";
                    echo "<tr>";
                }
                echo "<td><a href='?view=prescricoes&salva={$nome[0]}'>".strtoupper($paciente)."</a></td>";

                //echo "<p style='display:table;float:left;padding-right:20px;'></p>";
                $cont++;
            }
            echo "</tr>";
            echo "</tbody>";
            echo "</table>";
        }
    }

    //! Carrega uma prescrição salva.
    private function load_salva($arq){
        $dir = "salvas/".$_SESSION["id_user"];
        $filename = $dir."/".$arq.".htm";
        $str = stripslashes(file_get_contents($filename));
        echo "<div id='corpo-prescricao'>";
        echo "<div id='dialog-salvo' title='Finalizada'>";
        echo "<center><h3>Prescri&ccedil;&atilde;o salva com sucesso!</h3></center>";
//      echo "<form action='imprimir.php' method='post'><input type='hidden' value='-1' name='prescricao'><center><button>Imprimir</button></center></form>";
        echo "<br/><center>Enviar email de aviso?</center>";
        echo "</div>";
        echo $this->formulario();
        echo "<div id='aviso' title='Avisos' ></div>";
        echo "<center><h1>Prescri&ccedil;&otilde;es</h1></center>";
        echo $str;
        echo "</div>";
        echo "<button id='salvar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
        echo "<button id='finalizar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Finalizar</span></button></span>";
        echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
        //echo "<button id='salvar'>Salvar</button>&nbsp;&nbsp;&nbsp;<button id='finalizar' >Finalizar</button>";
    }

    private function incluir_diluente(){
        echo "<div id='dialog-incluir-diluente' title='Incluir Nebuliza��o'>";
        echo "<center><h3>nebuliza��o apenas com diluente? !</h3></center>";
        //      echo "<form action='imprimir.php' method='post'><input type='hidden' value='-1' name='prescricao'><center><button>Imprimir</button></center></form>";
        echo "<br/><center>Enviar email de aviso?</center>";
        echo "</div>";
    }

    //! Método de controle, determina se a prescrição será nova, nova baseada em antiga ou um prescrição salva.
    private function prescricoes($id,$data,$antiga,$inicio,$fim,$salva,$tipo,$id_cap){
        if($salva != NULL){
            $this->load_salva($salva);
            return;
        }
        if($tipo==1){
            $this->presc_emergencia($id,$data,$inicio,$fim,$tipo);
        }else{
            if($id == NULL || $id == "-1"){
                echo "<center><h1>Nova Prescri&ccedil;&atilde;o</h1></center>";
                echo "<div id='div-iniciar-prescricao' >";
                echo alerta();
                echo "<center><h3>Prescri&ccedil;&otilde;es Salvas</h3></center>";
                $this->prescricoes_salvas();
                echo "<center><h3>Prescri&ccedil;&atilde;o Nova</h3></center>";


                echo "<form method='post' >";
                echo $this->tipo_prescricao();
                echo "<div id='container-presc'>";
                $this->pacientes();
                echo $this->periodo(false, true);
//      echo "<b>Data:</b><input type='text' name='data' value='$data' maxlength='10' size='10' class='OBG data' />";
                echo "<p><b>&Uacute;ltimas prescri&ccedil;&otilde;es:</b>";
                echo "<div id='antigas' ></div>";

                echo "</div>"; # Fim do div container-presc !

                echo "<button id='iniciar-prescricao' >Iniciar</button></form>";

                echo "</div>";
            } else {
                //$this->tipo_presc =  $tipo_presc;
                $this->inicio = $inicio;
                $this->fim = $fim;
                echo "<div id='corpo-prescricao'>";
                echo "<div id='dialog-salvo' title='Finalizada'>";
                echo "<center><h3>Prescri&ccedil;&atilde;o salva com sucesso!</h3></center>";
//      echo "<form action='imprimir.php' method='post'><input type='hidden' value='-1' name='prescricao'><center><button>Imprimir</button></center></form>";
                echo "<br/><center>Enviar email de aviso?</center>";
                echo "</div>";
                echo "<div id='dialog-imprimir' title='Imprimir'>";
                echo "<center><h3>Visualizar Prescri&ccedil;&atilde;o?</h3></center>";
                echo "<form action='imprimir.php' method='post' target='_blank'>
<input type='hidden' value='-1' name='prescricao'>
<input type='hidden' value='-1' name='empresa'>
<center></center></form>";
                echo "</div>";

                ///jeferson

                echo $this->formulario();
                echo "<div id='aviso' title='Avisos' ></div>";
                echo "<center><h1>Nova Prescri&ccedil;&atilde;o de Nutrição</h1></center>";
                $pnome = $this->pacienteNome($id);
                $sqlTpoMedicamento = "SELECT
planosdesaude.nome,
planosdesaude.tipo_medicamento,
clientes.empresa
FROM
clientes
INNER JOIN planosdesaude ON clientes.convenio = planosdesaude.id
WHERE
clientes.idClientes = $id ";
                $result=  mysql_query($sqlTpoMedicamento);
                $tipoMedicamento = mysql_result($result,0,1);
                $nomePlano = mysql_result($result,0,0);
                $empresaId = mysql_result($result,0,2);
                echo "<center><h2>Paciente: {$pnome}</h2></center>";

                if($tipo == 6 ){
                    echo"
<div id='div-tipo-aditivo'>
                    <p>
                         <label><b>Selecione o tipo de prescrição Aditiva:</b></label>
                         <br>
                         <select name='tipo-aditiva' id='tipo-aditiva' class='OBG'>           
                                 <option value=''>Selecione</option>
                                     <option value='NOVO ITEM'>Novo Item</option>
                                     <option value='AJUSTE'>Ajuste</option>
                                     
                   </select>
                    </p>";

                    echo "
                            <p class='container-motivo-ajuste'>
                                    <label><b>Motivo do ajuste:</b></label>
                                    <br>
                                    <textarea id='motivo-ajuste-aditivo' name='motivo-ajuste-aditivo' cols='60'></textarea>
                                </p>";


                    echo '</div>';

                }

                echo "<div id='prescricao' paciente='{$id}' pnome='{$pnome}' data='{$data}' empresa-id={$empresaId} inicio='{$inicio}' fim='{$fim}' tipo_presc='{$tipo}' id_cap='{$id_cap}'>";
                echo "<ul>";
                echo "<li><a href='#div-repouso' ><span>Repouso</span></a></li>";
                echo "<li><a href='#div-dieta' ><span>Dieta</span></a></li>";
                echo "<li><a href='#div-cuidados' ><span>Cuidados Especiais</span></a></li>";
                /*echo "<li><a href='#div-soros' ><span>Soros e Eletr&oacute;litos</span></a></li>";
                echo "<li><a href='#div-antibio-inj' ><span>Antibi&oacute;ticos Injet&aacute;veis</span></a></li>";
                echo "<li><a href='#div-inj-ev' ><span>Injet&aacute;veis IV</span></a></li>";
                echo "<li><a href='#div-inj-im' ><span>Injet&aacute;veis IM</span></a></li>";
                echo "<li><a href='#div-inj-sc' ><span>Injet&aacute;veis SC</span></a></li>";
                echo "<li><a href='#div-drogas-inalatorias' ><span>Drogas Inalat&oacute;rias</span></a></li>";
                echo "<li><a href='#div-nebulizacao' ><span>Nebuliza&ccedil;&atilde;o</span></a></li>";*/
                echo "<li><a href='#div-drogas-orais' ><span>Dietas Enterais</span></a></li>";
                /* echo "<li><a href='#div-drogas-topicas' ><span>Drogas T&oacute;picas</span></a></li>";*/
                //echo "<li><a href='#div-formulas' ><span>F&oacute;rmulas</span></a></li>";
                echo "<li><a href='#div-drogas-ativas' ><span>Suspender</span></a></li>";
                // echo "<li><a href='#div-oxigenoterapia' ><span>Oxigenoterapia</span></a></li>";
                echo "</ul>";
                $tabela = $this->load_table($antiga);
                $this->repouso();
                $this->dieta();
                $this->equipe_multidisciplinar();
                /* $this->soros_eletrolitos();
                 $this->antibioticos_injetaveis();
                 $this->injetaveis_ev();
                 $this->injetaveis_im();
                 $this->injetaveis_sc();
                 $this->drogas_inalatorias();
                 $this->nebulizacao();*/
                $this->drogas_orais();
                /// $this->drogas_topicas();
                //$this->formulas();
                //$this->oxigenoterapia();
                $this->drogas_ativas($id);
                echo "</div>";
                echo "<br/><button id='rem-item' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Remover</span></button></span>";
                //echo "<button id='rem-item' >remover</button><br/>";
                if($antiga == -1){
                    echo "<table width=100% id='tabela-itens' inicio='{$this->inicio}' fim='{$this->fim}' class='mytable'><thead><tr>";
                    echo "<th><b>Itens prescritos</b></th>";
                    echo "<th width=1% ><input type='checkbox' id='checktodos' name='select' /></th>";
                }
                echo $tabela;
                echo "</div>";
                echo "<button id='salvar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Salvar</span></button></span>";
                echo "<button id='finalizar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Finalizar</span></button></span>";
                echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";
                //echo "<button id='salvar'>Salvar</button>&nbsp;&nbsp;&nbsp;<button id='finalizar' >Finalizar</button>";
            }
        }//fim do else teste tipo ==1
    }

    //! Busca prescrições finalizadas.
    private function buscar(){
        if(isset($_GET['msg']) and $_GET['msg'] !== ''){
            echo "<center><h4 style='color:#fff !important; background-color:#8b0101; border: 1px solid #8b0101;'>".base64_decode($_GET['msg'])."</h4></center>";
        }
        echo "<center><h1>Prescri&ccedil;&otilde;es</h1></center>";
        echo "<fieldset style='margin:0 auto;'>";
        echo "<div id='div-busca' >";

        echo $this->pre_prescricao();
        echo alerta();
        echo "<div style='float:left;width:400px;'>";
        echo "<center><h2 style='background-color:#8B0101;color:#ffffff;height:25px;'><label style='vertical-align: middle;'>PRESCRI&Ccedil;&Otilde;ES SALVAS</label></h2></center>";
        $this->prescricoes_salvas();

        echo "</div>";
        echo "<div style='float:right;width:500px;'>";
        echo "<center><h2 style='background-color:#8B0101;color:#ffffff;height:25px;vertical-align: middle;'><label style='vertical-align: middle;'>PRESCRI&Ccedil;&Otilde;ES</label></h2></center>";
        echo $this->pacientes();
        echo $this->periodo();

        echo "<p><button id='buscar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Buscar</span></button>";

        if($_SESSION['modnutri'] == 1 || $_SESSION['adm_user']==1){
            echo "<button name='antigas' id='novoBtn' paciente_btn_nova='' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' >
                <span class='ui-button-text'>Nova</span>
              </button>";
        }
        echo "</div>";

        echo "</div>";
        echo "</fieldset>";
        echo "<div id='div-resultado-busca'></div>";
    }

    //! Exibe os detalhes de um prescrição finalizada.
    private function detalhes($p, $idPaciente){

        $r = new Paciente();
        echo "<center><h1>Visualizar Prescrição de Nutrição</h1></center>";
        $r->cabecalho($idPaciente);

        $responsePrescricao = \App\Models\Medico\Prescricao::getById($p);

        if(!empty($responsePrescricao['motivo_ajuste'])){
            echo  "
<div class='ui-state-highlight' style='padding: 5px;'>
                <span class='ui-icon  ui-icon-alert icon-inline' ></span>
                <b>Motivo do Ajuste da Prescrição Aditiva: </b> 
                {$responsePrescricao['motivo_ajuste']}
            </div>
            <br>
";
        }
        echo "<table width=100% class='mytable'><thead><tr>";
        echo "<th><b>Itens prescritos</b></th>";
        echo "</tr></thead>";
        $sql = "SELECT v.via as nvia, f.frequencia as nfrequencia,i.*,  i.inicio,
    		i.fim, DATE_FORMAT(i.aprazamento,'%H:%i') as apraz, u.unidade as um,
    		p.empresa, p.paciente
    		FROM itens_prescricao as i LEFT OUTER JOIN frequencia as f ON (i.frequencia = f.id) 
    		inner join prescricoes as p on (i.idPrescricao = p.id)
    		LEFT OUTER JOIN viaadm as v ON (i.via = v.id) 
    		LEFT OUTER JOIN umedidas as u ON (i.umdose = u.id) 
    		WHERE idPrescricao = '$p'  
    		ORDER BY tipo,inicio,aprazamento,nome,apresentacao ASC";
        $result = mysql_query($sql);
        $n=0;
        while($row = mysql_fetch_array($result)){
            if($n++%2==0)
                $cor = '#E9F4F8';
            else
                $cor = '#FFFFFF';

            $tipo = $row['tipo'];
            $i = implode("/",array_reverse(explode("-",$row['inicio'])));
            $f = implode("/",array_reverse(explode("-",$row['fim'])));
            $aprazamento = "";
            if($row['tipo'] != "0") $aprazamento = $row['apraz'];
            $linha = "";
            if($row['tipo'] != "-1"){
                $aba = $this->abas[$row['tipo']];
                $dose = "";
                if($row['dose'] != 0) $dose = $row['dose']." ".$row['um'];
                $linha = "<b>$aba:</b> {$row['nome']} {$row['apresentacao']} {$row['nvia']} {$dose} {$row['nfrequencia']} Per&iacute;odo: de $i até $f OBS: {$row['obs']}";
            }
            else $linha = $row['descricao'];
            echo "<tr bgcolor={$cor} >";
            echo "<td>$linha</td>";
            echo "</tr>";
            $idPaciente = $row['paciente'];
            $empresaId = $row['empresa'];
        }
        echo "</table>";
        $responsePaciente = ModelPaciente::getById($idPaciente);
        $empresaRelatorio = empty($empresaId) ? $responsePaciente['empresa'] : $empresaId;
        echo "
        <button id='' empresa-relatorio='{$empresaRelatorio}' caminho='/nutricao/imprimir.php?prescricao={$p}' class='escolher-empresa-gerar-documento ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'><span class='ui-button-text'>Imprimir</span></button>";

        echo "<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";

    }


    /////////prescricao avaliaca


    //! Controla qual página será exibida ao usuário.
    public function view($v,$p){
        switch ($v) {
            case "menu":
                //unset($_SESSION['paciente']);
                $this->menu();
                break;
            case "prescricoes":

                $id = NULL;
                $tipo = null;
                $antiga = NULL;
                $data = date("d/m/Y");
                $pantiga = -1;
                $inicio = NULL;
                $fim = NULL;
                $salva = NULL;
                $id_cap = NULL;
                if(isset($_POST['paciente'])) $id = $_POST['paciente'];
                if(isset($_POST['tipo'])) $tipo = $_POST['tipo'];
                if(isset($_GET["data"])) $data = $_GET["data"];
                if(isset($_POST["antigas"])) $antiga = $_POST["antigas"];
                if(isset($_POST["inicio"])) $inicio = $_POST["inicio"];
                if(isset($_POST["fim"])) $fim = $_POST["fim"];
                if(isset($_GET["salva"])) $salva = $_GET["salva"];
                if(isset($_POST['id_cap'])) $id_cap = $_POST['id_cap'];

                $data_inicio = \DateTime::createFromFormat('d/m/Y', $inicio);
                $data_final = \DateTime::createFromFormat('d/m/Y', $fim);

                if($data_inicio <= $data_final)
                    $this->prescricoes($id,$data,$antiga,$inicio,$fim,$salva,$tipo,$id_cap);
                else
                    header("Location: /nutricao/?view=buscar&msg=".base64_encode('A data inicial é maior que a final. Corrija a data para prosseguir.'));

                break;
            case "prescricao-avaliacao":
                $id = NULL;
                $tipo = null;
                $antiga = NULL;
                $data = date("d/m/Y");
                $pantiga = -1;
                $inicio = NULL;
                $fim = NULL;
                $salva = NULL;
                if(isset($_REQUEST['paciente'])) $id = $_REQUEST['paciente'];
                if(isset($_REQUEST['tipo'])) $tipo = $_REQUEST['tipo'];
                if(isset($_REQUEST["data"])) $data = $_REQUEST["data"];
                if(isset($_REQUEST["antigas"])) $antiga = $_REQUEST["antigas"];
                if(isset($_REQUEST["inicio"])) $inicio = $_REQUEST["inicio"];
                if(isset($_REQUEST["fim"])) $fim = $_REQUEST["fim"];
                if(isset($_REQUEST["salva"])) $salva = $_REQUEST["salva"];
                if(isset($_REQUEST["id_cap"])) $id_cap = $_REQUEST["id_cap"];
                $this->prescricoes($id,$data,$antiga,$inicio,$fim,$salva,$tipo,$id_cap);
                break;

            case "listar":
                //echo "<button id='tt'>dcgfgh</button>";
                //Paciente::listar();
                $p = new Paciente();
                $p->listar();
                break;

            case "listavaliacaenfermagem":
                if(isset($_GET['ID'])){
                    $p = new Paciente();
                    $p->listar_ficha_avaliacao_enfermagem($_GET['ID']);
                }
                break;

            case "listevolucaoenfermagem":
                if(isset($_GET['ID'])){
                    $p = new Paciente();
                    $p->listar_ficha_evolucao_enfermagem($_GET['ID']);
                }
                break;

            case "buscar":

                $this->buscar();
                break;
            case "detalhes":
                if(isset($_GET['p']))
                    $this->detalhes($_GET['p'],$_GET['idPac']);
                else $this->menu();
                break;
            case "show":
                if(isset($_GET['id']))
                    $p->form($_GET['id'],'readonly');
                else $this->menu();

        }


    }

}

?>