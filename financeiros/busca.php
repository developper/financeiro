
<?php
$id = session_id();
if(empty($id))
  session_start();
include('../db/config.php');

class Busca {

	public function status(){
		$var = "<select id='status' name='status' style='background-color:transparent;'>";
		$var .= "<option value='' selected>Selecione</option>";
		$var .= "<option value='cancelada'>Cancelada</option>";
//      $var .= "<option value='em aberto'>Em Aberto</option>";
		$var .= "<option value='inconsistente'>Inconsistente</option>";
		$var .= "<option value='pendente'  >Pendente</option>";
		$var .= "<option value='Processada' >Processada</option>";
		$var .= "</select>";
		return $var;
	}

	public function statusBootstrap(){
		$var = "<select id='status' name='status' class='form-control'>";
		$var .= "<option value='' selected>Selecione</option>";
		$var .= "<option value='cancelada'>Cancelada</option>";
//      $var .= "<option value='em aberto'>Em Aberto</option>";
		$var .= "<option value='inconsistente'>Inconsistente</option>";
		$var .= "<option value='pendente'  >Pendente</option>";
		$var .= "<option value='Processada' >Processada</option>";
		$var .= "</select>";
		return $var;
	}

	public function naturezas($id){
		$result = mysql_query("SELECT * FROM naturezanotas ORDER BY nome;");
		$var = "<select id='natureza' name='codNatureza' style='background-color:transparent;'>";
		while($row = mysql_fetch_array($result))
		{
			if(($id <> NULL) && ($id == $row['idNaturezaNotas']))
				$var .= "<option value='". $row['idNaturezaNotas'] ."' selected>". strtoupper($row['nome']) ."</option>";
			else
				$var .= "<option value='". $row['idNaturezaNotas'] ."'>". strtoupper($row['nome']) ."</option>";
		}
		$var .= "</select>";
		return $var;
	}
    
    public function centrocusto($id){
      $result = mysql_query("SELECT * FROM centrocustos ORDER BY nome;");
      $var = "<select id='centrocusto' name='codCentroCusto' style='background-color:transparent;'>";
      while($row = mysql_fetch_array($result))
      {
	if(($id <> NULL) && ($id == $row['idCentroCustos']))
	  $var .= "<option value='". $row['idCentroCustos'] ."' selected>". $row['nome'] ."</option>";
	else 
	  $var .= "<option value='". $row['idCentroCustos'] ."'>". $row['nome'] ."</option>";
      }
      $var .= "</select>";
      return $var;
    }
    
    
   /* public function ipcg3($id){
    	$var ="<span id ='sp_ipcg3'>";
    	$result = mysql_query("SELECT DISTINCT N3 FROM plano_contas where ORDER BY ID;");
    	$var .= "<select id='ipcg3' name='ipcg3' style='background-color:transparent;'>";
    	while($row = mysql_fetch_array($result))
    	{
    		if(($id <> NULL) && ($id == $row['N3']))
    			$var .= "<option value='". $row['N3'] ."' selected>". $row['N3'] ."</option>";
    		else
    			$var .= "<option value='". $row['N3'] ."'>". $row['N3'] ."</option>";
    	}
    	$var .= "</select>";
    	$var .= "</span>";
    	return $var;
    }*/
    
    public function ipcf2_b($id){
    	$cont=1;
    	$id = $id;
    	$cond="1";
    
    
    	if ($id == 1){
    		$cond=" N1 like 'S'";
    	}
    	if ($id == 0){
    		$cond=" N1 like 'E'";
    	}
    	if ($id == 't'){
    		$cond=" 1";
    	}
    
    	$result = mysql_query("SELECT DISTINCT N2 FROM plano_contas where {$cond} ORDER BY ID");
    	$var .= "<select class='ipcf2_busca COMBO_OBG' name='ipcf2_busca' style='background-color:transparent;'>";
    	while($row = mysql_fetch_array($result))
    	{
    		if($cont == 1 ){
    			$var .= "<option value=''>selecione</option>";
    		}
    
    		$var .= "<option value='". $row['N2'] ."'>". $row['N2'] ."</option>";
    		$cont++;
    	}
    	$var .= "</select>";
    
    	//print_r($var);
    
    	if ($id == NULL){
    		return $var;
    	}else print_r($var);
    }
   public function ipcg3_b($id,$ipcf2){
    	$cont=1;
    	$id = $id;
    	$cond="1 ";
    
    
    	if ($id == 1){
    		$cond=" N1 like 'S' and N2 like '%{$ipcf2}%'";
    	}
    	if ($id == 0){
    		$cond=" N1 like 'E' and N2 like '%{$ipcf2}%'";
    	}
    	if ($id == 't'){
    		$cond=" 1";
    	}
    
    	$result = mysql_query("SELECT DISTINCT N3 FROM plano_contas where {$cond} ORDER BY ID");
    	$var .= "<select class='ipcg3_busca COMBO_OBG' name='ipcg3_busca' style='background-color:transparent;'>";
    	while($row = mysql_fetch_array($result))
    	{
    		if($cont == 1 ){
    			$var .= "<option value=''>selecione</option>";
    		}
    
    		$var .= "<option value='". $row['N3'] ."'>". $row['N3'] ."</option>";
    		$cont++;
    	}
    	$var .= "</select>";
    
    	//print_r($var);
    
    	if ($id == NULL){
    		return $var;
    	}else print_r($var);
    }

	public function tipoDocumentoFinanceiroBootstrap(){
		$result = mysql_query("SELECT * FROM tipo_documento_financeiro where adiantamento != 'S' ORDER BY sigla;");
		$var = "";
		 $var = "<select id='select_tipo_documento' name='select_tipo_doc_financeiro' class='select_small $ chosen-select form-control'  >";
		
			$var .="<option value='' selected>Selecione</option>";
		
		while($row = mysql_fetch_array($result))
		{
		
			
				$var .= "<option value='". $row['id'] ."' sigla_tipo_documento='". $row['sigla'] ."' >". $row['sigla'] ." - ". $row['descricao'] ."</option>";
		}
		$var .= "</select>";
		return $var;
	}


	public function ipcg4_b($tipo_nota, $ipcf2, $ipcg3){

		$cond = " N1 = 'S'";
		if ($tipo_nota == '0'){
			$cond = " N1 like 'E'";
		}
		$result = mysql_query("SELECT * FROM plano_contas WHERE {$cond} {$id} ORDER BY ID");
		$var = "<select class='ipcg4_nota chosen-select COMBO_OBG' name='ipcg4_nota' style='width: 300px' >";
		$var .= "<option value='' selected>Selecione...</option>";
		while($row = mysql_fetch_array($result))
		{
			$var .= "<option value='". $row['ID'] ."'>{$row['COD_N4']} - {$row['N4']}</option>";
		}
		$var .= "</select>";

		echo $var;
	}

	public function ipcg4_busca_bootstrap()
	{
		$result = mysql_query("SELECT * FROM plano_contas ORDER BY COD_N4");
		$var = "<select class='ipcg4_nota chosen-select form-control' name='ipcg4_nota'>";
		$var .= "<option value='' selected>Selecione...</option>";
		while($row = mysql_fetch_array($result))
		{
			$codigoNatureza = str_replace('.', '', $row['COD_N4']);
			$var .= "<option value='". $row['ID'] ."'>{$codigoNatureza} - {$row['N4']}</option>";
		}
		$var .= "</select>";

		return $var;
	}
    
    
  /*  public function ipcg4($id){
    	$var ="<span id='sp_ipcg4'>";
    	$result = mysql_query("SELECT DISTINCT  N4 FROM plano_contas  ORDER BY ID;");
    	$var .= "<select id='ipcg4' name='ipcg4' style='background-color:transparent;'>";
    	while($row = mysql_fetch_array($result))
    	{
    		if(($id <> NULL) && ($id == $row['N4']))
    			$var .= "<option value='". $row['N4'] ."' selected>". $row['N4'] ."</option>";
    		else
    			$var .= "<option value='". $row['N4'] ."'>". $row['N4'] ."</option>";
    	}
    	$var .= "</select>";
    	$var .= "</span>";
    	return $var;
    }*/

	public function empresa($id, $empresas = null){
		$cont=1;
		$id = $id;
		$condUR = empty($empresas) ? "ATIVO = 'S'" : "id in {$empresas}";
		$result = mysql_query("SELECT * FROM empresas where {$condUR} ORDER BY nome ");
		$var .= "<select class='empresa COMBO_OBG' name='empresa' style='background-color:transparent;'>";
		while($row = mysql_fetch_array($result))
		{
			if($cont == 1 ){
				$var .= "<option value=''>Selecione</option>";
			}

			$var .= "<option value='". $row['id'] ."'>". $row['nome'] ."</option>";
			$cont++;
		}
		$var .= "</select>";

		//print_r($var);

		if ($id == NULL){
			return $var;
		}else print_r($var);
	}

	public function empresa_busca_bootstrap($empresas = null)
	{
		$cont = 1;
		$condUR = empty($empresas) ? " WHERE ATIVO = 'S' " : " WHERE id in {$empresas} ";
		$result = mysql_query("SELECT * FROM empresas {$condUR} ORDER BY nome ");
		$var = "<select class='empresa form-control' name='empresa'>";
		while($row = mysql_fetch_array($result))
		{
			if($cont == 1 ){
				$var .= "<option value=''>Selecione</option>";
			}
			$var .= "<option value='". $row['id'] ."'>". $row['nome'] ."</option>";
			$cont++;
		}
		$var .= "</select>";

		return $var;
	}

	public function assinatura($id){
		$cont=1;
		$id = $id;
		$result = mysql_query("SELECT * FROM assinaturas where 1 ORDER BY DESCRICAO ");
		$var .= "<select class='assinatura COMBO_OBG' name='assinatura' style='background-color:transparent;'>";
		while($row = mysql_fetch_array($result))
		{
			if($cont == 1 ){
				$var .= "<option value=''>Selecione</option>";
			}

			$var .= "<option value='". $row['ID'] ."'>". $row['DESCRICAO'] ."</option>";
			$cont++;
		}
		$var .= "</select>";

		//print_r($var);

		if ($id == NULL){
			return $var;
		}else print_r($var);
	}

	public function assinatura_busca_bootstrap(){
		$cont=1;
		$result = mysql_query("SELECT * FROM assinaturas ORDER BY DESCRICAO ");
		$var = "<select class='assinatura form-control' name='assinatura'>";
		while($row = mysql_fetch_array($result))
		{
			if($cont == 1 ){
				$var .= "<option value=''>Selecione</option>";
			}

			$var .= "<option value='". $row['ID'] ."'>". $row['DESCRICAO'] ."</option>";
			$cont++;
		}
		$var .= "</select>";

		return $var;
	}


	public function fornecedores($id){
		$result = mysql_query("SELECT *, COALESCE(razaoSocial,nome) as fornecedor FROM fornecedores ORDER BY razaoSocial;");
		$var = "<select name='codFornecedor' id='fornecedores' class='chosen-select' style='background-color:transparent;' >";
		$var .="<option value='' selected>Selecione</option>";
		while($row = mysql_fetch_array($result))
		{
			
			if(($id <> NULL) && ($id == $row['idFornecedores']))
				$var .= "<option value='". $row['idFornecedores'] ."' selected>". $row['fornecedor'] ."</option>";
			else
				$var .= "<option value='". $row['idFornecedores'] ."'>". $row['fornecedor'] ."</option>";
		}
		$var .= "</select>";
		return $var;
	}

	public function fornecedores_busca_bootstrap(){
		$result = mysql_query("SELECT *, COALESCE(razaoSocial,nome) as fornecedor FROM fornecedores ORDER BY razaoSocial;");
		$var = "<select name='codFornecedor' id='fornecedores' class='chosen-select form-control'>";
		$var .="<option value='' selected>Selecione</option>";
		while($row = mysql_fetch_array($result))
		{	$inativo = $row['ATIVO'] == "N" ? '(INATIVO) ': '';
			$styleInativo = $row['ATIVO'] == "N" ? 'red': '';
 			$cpfCnpjFornecedor = $row['FISICA_JURIDICA'] == 1 ? $row['cnpj'] : $row['CPF'];
			$var .= "<option style = 'color:{$styleInativo}' value='". $row['idFornecedores'] ."'>".$inativo.$cpfCnpjFornecedor." - ". $row['fornecedor'] ."</option>";
		}
		$var .= "</select>";
		return $var;
	}

    public function pacientes($id){
      $result = mysql_query("SELECT * FROM clientes ORDER BY nome;");
      $var = "<select name='idClientes' style='background-color:transparent;' >";
      while($row = mysql_fetch_array($result))
      {
      if(($id <> NULL) && ($id == $row['idClientes']))
	  $var .= "<option value='". $row['idClientes'] ."' selected>". $row['nome'] ."</option>";
      else
	  $var .= "<option value='". $row['idClientes'] ."'>". $row['nome'] ."</option>";
      }
      $var .= "</select>";
      return $var;
	}
	
	public  function aplicacaoFundosOption($list, $id = null){
		$result = mysql_query("SELECT * FROM aplicacaofundos where id in ($list) ORDER BY forma;");
		
	   
		$var .= "	<option  value=''>Selecione...</option>";
		while($row = mysql_fetch_array($result))
		{
			if(($id <> NULL) && ($id == $row['id']))
				$var .= "<option value='{$row['id']}' selected>{$row['forma']}</option>";
			else
				$var .= "<option value='{$row['id']}'>{$row['forma']}</option>";
		}
	   
		return $var;
	}
//       echo "<h2>Escolha o m&eacute;todo de busca, por:</h2>";
//       echo "<p><a href='?op=notas&act=buscar&campo=codigo'>C&oacute;digo</a>";
//       echo "<p><a href='?op=notas&act=buscar&campo=fornecedor'>Fornecedor</a>";
//       echo "<p><a href='?op=notas&act=buscar&campo=periodo'>Per&iacute;odo</a>";
//       echo "<p><a href='?op=notas&act=buscar&campo=paciente'>Paciente</a>";

	public function show(){
		$html = <<<HTML
<link rel="stylesheet" href="/utils/bootstrap-3.3.6-dist/css/bootstrap.min.css" />
<script src="/utils/bootstrap-3.3.6-dist/js/bootstrap.min.js" ></script>
<link rel='stylesheet' href='/utils/font-awesome-4.7.0/css/font-awesome.css' />
<div id="div-busca">
<form>
	<h2 class="text-center">
		Buscar Parcelas a Pagar/Receber
	</h2>
	<br>
	<div class="row" style="margin-top: 10px;">
		<div class="col-xs-12">
			<label><input type='radio' name='btipo' class='btipo' value='t' CHECKED /> Todos </label> &nbsp;&nbsp;
			<label><input type='radio' name='btipo' class='btipo' value='1'/> Contas a Pagar </label> &nbsp;&nbsp;
			<label><input type='radio' name='btipo' class='btipo' value='0'/> Contas a Receber </label>
		</div>
	</div>
	<br>
	<div class="row">
	    <div class="col-xs-2">
			<label>TR</label>
			<input type='text' name='codigo' class="form-control" />
		</div>
	    <div class="col-xs-3">
			<label for="">Valor</label>
			<input type='text' class='valor form-control' name='valor' maxlength="15" style='text-align:right' />
		</div>
		<div class="col-xs-2">
			<label>Número da Nota</label>
			<input type='text' name='numero_nota' class="form-control" />
		</div>
		<div class="col-xs-2">
			<label>Descrição da Nota</label>
			<input type='text' size='60' name='descricao_nota' class="form-control" />
		</div>
		<div class="col-xs-3">
			<label>Status</label>
			<span id='sp_status_busca' >
				{$this->statusBootstrap()}
			</span>
		</div>
	</div>
	<div class="row">
	    <div class="col-xs-4">
			<label for="">Natureza</label>
			{$this->ipcg4_busca_bootstrap()}
		</div>
		<div class="col-xs-4">
			<label for="">Unidade Regional</label>
			<span id='sp_empresa_busca' >
				{$this->empresa_busca_bootstrap($_SESSION['empresa_user'])}
			</span>
		</div>
	    <div class="col-xs-4">
			<label for="">Centro de Custo</label>
			<span id='sp_ass'>
				{$this->assinatura_busca_bootstrap()}
			</span>
		</div>
	</div>
	<div class="row">
	    <div class="col-xs-6">
			<label>Fornecedor</label>
			<span id='sp_fornecedor_busca' >
				{$this->fornecedores_busca_bootstrap()}
			</span>
		</div>
		<div class="col-xs-3">
			<label for="">Vencimento Real de</label>
			<input id='inicio' type='text' name='inicio' class="form-control" maxlength='10' size='10' />
		</div>
		<div class="col-xs-3">
			<label for="">Vencimento Real até</label>
			<input id='fim' type='text' name='fim' class="form-control" maxlength='10' size='10' />
		</div>
	</div><div class="row">
	    <div class="col-xs-6">
			<label>Tipo Documento</label>
			<span id='sp_fornecedor_busca' >
				{$this->tipoDocumentoFinanceiroBootstrap()}
			</span>
		</div>
		<div class="col-xs-3">
			<label for="">Emissão de</label>
			<input id='inicioE' type='text' name='inicioE' class="form-control" maxlength='10' size='10' />
		</div>
		<div class="col-xs-3">
			<label for="">Emissão até</label>
			<input id='fimE' type='text' name='fimE' class="form-control" maxlength='10' size='10' />
		</div>
	</div>
	<div class="row">
		
		<div class="col-xs-3">
			<label for="">Pagamento de</label>
			<input id='inicioPagamento' type='text' name='inicioPagamento' class="form-control" maxlength='10' size='10' />
		</div>
		<div class="col-xs-3">
			<label for="">Pagamento até</label>
			<input id='fimPagamento' type='text' name='fimPagamento' class="form-control" maxlength='10' size='10' />
		</div>	
    	<div class="col-xs-6">
            <label>Histórico contém:</label>
            <input type='text' name='descricao_nota' id='descricao_nota' class="form-control"  />
        </div>
   
	</div>
	<div class="row">
        
        <div class="col-xs-3">
            <label>Competência de</label>
            <input type='text' name='competenciaInicio' id='competenciaInicio' class="form-control mes-ano" maxlength='10' size='10' />
        </div>
        <div class="col-xs-3">
            <label>Competência até</label>
            <input type='text' name='competenciaFim' id='competenciaFim' class="form-control mes-ano" maxlength='10' size='10' />
        </div>
		<div class="col-xs-6">
			<label>Forma Pagamento</label>
                <select id='select_forma_pagamento' name='select_forma_pagamento' class='select_small  $ chosen-select form-control'>    
                    <!-- <option value = '' selected>Selecione</option>
					
                    <option value = 'Boleto'>Boleto</option>
                    <option value = 'Cartão de Crédito'>Cartão de Crédito</option>
                    <option value = 'Transferência'>Transferência</option>      -->
					{$this->aplicacaoFundosOption(implode(',', [1, 7, 5,9,6, 10]))}
                </select>
        </div>
    </div>
	<br>
	<div class="row">
		<div class="col-xs-12 text-center">
			<button type="button" role="button" id='buscar' class="btn btn-primary">
				<i class="fa fa-search"></i> Pesquisar
			</button>
			<a href="/financeiros/?op=notas" style="color: white;" class="btn btn-warning">
				<i class="fa fa-arrow-circle-left"></i> Voltar
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12" id='resultado-busca'>
		
		</div>
	</div>
</form>
</div>
HTML;

		require_once ("estornar-parcela/templates/modal-estornar-parcela.phtml");
		echo $html;
	}

    public function show2(){
      echo "<div id='div-busca'><form>";
      echo"<h1><center>Buscar Parcelas a Pagar/Receber</center></h1>";
      echo "<p><b>Tipo:</b><input type='radio' name='btipo' class='btipo' value='t' CHECKED/>Todos&nbsp;&nbsp;";
      echo "</b><input type='radio' name='btipo' class='btipo' value='1'/>Contas a Pagar&nbsp;&nbsp;";
      echo "<input type='radio' name='btipo' class='btipo' value='0'/>Contas a Receber</p>";
      echo "<p><b>TR:</b><input type='text' name='codigo' />&nbsp;&nbsp;&nbsp;&nbsp;</p>";
      echo "<p><b>Numero da Nota:</b><input type='text' name='numero_nota' />&nbsp;&nbsp;&nbsp;&nbsp;</p>";
      echo "<p><b>Descri&ccedil;&atilde;o da Nota:</b><input type='text' size='60' name='descricao_nota' />&nbsp;&nbsp;&nbsp;&nbsp;</p>";
      echo "<p><b>Status:</b><span id='sp_status_busca' >" . $this->status()." </span></p>";
      echo "<p><b>Fornecedor:</b><span id='sp_fornecedor_busca' >" . $this->fornecedores(NULL) ." </span></p>";       
			echo "<p><b>Unidade Regional:</b><span id='sp_empresa_busca' >" . $this->empresa(NULL,$_SESSION['empresa_user'])."</span></p>";    
      echo "<p><b>Centro de Custo:</b><span id='sp_ass'>" . $this->assinatura(NULL)."</span></p>";         
			echo "<p><label><b>IPCF4:</b></label></p>";
			echo $this->ipcg4_b(NULL);
	
     // echo "<p><span id='sp_ipcg3_busca'><b>IPCG3:</b><span id='sp_ipcg3_b' >" . $this->ipcg3_b(NULL)."</span><input type='checkbox' name='tipcg3' id='tipcg3' />Todos</span></p>";
     //echo "<div id='divsubnatureza'></div>";
      echo "<p><b>Valor (R$):</b><input type='text' class='valor' name='valor' size='10' style='text-align:right' /><button id='limparvalor'>limpar</button></p>";
      echo "<p>"
      . "       <b>Vencimento de:</b>"
              . "<input id='inicio' type='text' name='inicio' maxlength='10' size='10' />";
            echo "&nbsp;<b>at&eacute;</b>"
            . "<input id='fim' type='text' name='fim' maxlength='10' size='10' />"
         . "</p>";
      echo "<p>"
            .    "<b>Emissão de:</b>"
              . "<input id='inicioE' type='text' name='inicioE' maxlength='10' size='10' />";
            echo "&nbsp;<b>at&eacute;</b>"
                . "<input id='fimE' type='text' name='fimE' maxlength='10' size='10' />"
         . "</p>";
      echo "<p>"
            . " <b>Data de Pagamento:</b>"
              . "<input id='inicioPagamento' type='text' name='inicioPagamento' maxlength='10' size='10' />";
      echo "&nbsp;<b>at&eacute;</b>"
      . "       <input id='fimPagamento' type='text' name='fimPagamento' maxlength='10' size='10' />";      
      echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
      . "   </p>";
      echo "<p><button id='buscar' type='button' style='float:left;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Pesquisar</span></button>
&nbsp;&nbsp;&nbsp;&nbsp;<button id='voltar_' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button>";
      echo "</form>";
      echo "</br></br><div id='resultado-busca'></div>";
      echo "</div>";
    }

    public function show_busca_codigo(){
      echo "<form name=\"form\" action='' method='POST'>";
      echo "<p><b>C&oacute;digo:</b><input type='text' name='codigo'/>";
      echo "<input type='submit' value='Buscar' /><input type='hidden' value='1' name='submitted' />";
      echo "</form>";
    }

    public function show_busca_fornecedor(){
      echo "<form name=\"form\" action='' method='POST'>";
      echo "<p><b>Fornecedor:</b>" . $this->fornecedores(NULL);
      echo "<input type='submit' value='Buscar' /><input type='hidden' value='1' name='submitted' />";
      echo "</form>";
    }

    public function show_busca_periodo(){
      echo "<form name=\"form\" action='' method='POST'>";
      echo "<p><b></b><input type='radio' name='campo' value='dataEntrada' CHECKED/>Entrada&nbsp;&nbsp;";
      echo "<input type='radio' name='campo' value='vencimento' />Vencimento";
      echo "<p><b>In&iacute;cio do per&iacute;odo:</b><input id='entrada' type='text' name='inicio' maxlength='10' size='10' />";
      echo "&nbsp;<b>Fim do per&iacute;odo:</b><input id='vencimento' type='text' name='fim' maxlength='10' size='10' />";
      echo "<br/><input type='submit' value='Buscar' /><input type='hidden' value='1' name='submitted' />";
      echo "</form>";
    }

    public function show_busca_saida(){
      echo "<form name=\"form\" action='' method='POST'>";
      echo "<p><b>Paciente:</b>" . $this->pacientes(NULL);
      echo "<p><b>In&iacute;cio do per&iacute;odo:</b><input id='entrada' type='text' name='inicio' maxlength='10' size='10' />";
      echo "&nbsp;<b>Fim do per&iacute;odo:</b><input id='vencimento' type='text' name='fim' maxlength='10' size='10' />";
      echo "<br/><input type='submit' value='Buscar' /><input type='hidden' value='1' name='submitted' />";
      echo "</form>";
    }
    public function show_result($result){
      echo "<table border=0 width=100% >"; 
      echo "<tr bgcolor=\"#9bffff\" >"; 
      echo "<th><b>C&oacute;digo</b></th>"; 
      echo "<th><b>Parcelas</b></th>"; 
      echo "<th><b>Data de Entrada</b></th>"; 
      echo "<th><b>Vencimento</b></th>"; 
      echo "<th><b>Fornecedor</b></th>"; 
      echo "<th><b>Status</b></th>"; 
      echo "<th><b>Valor</b></th>"; 
      echo "<th colspan=\"4\" ><b>Op&ccedil;&otilde;es</b></th>";
      echo "</tr>";
      $cor = false;
      while($row = mysql_fetch_array($result)){ 
	foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
	if($cor) echo "<tr bgcolor=\"#f0f0f0\">";
	else echo "<tr bgcolor=\"#999999\">"; 
	$cor = !$cor;
	echo "<td valign='top'>" . nl2br( $row['codigo']) . "</td>";  
	echo "<td valign='top'>" . nl2br( $row['parcelas']) . "</td>";  
	echo "<td valign='top'>" . implode("/",array_reverse(explode("-",$row['dataEntrada']))) . "</td>";  
	echo "<td valign='top'>" . implode("/",array_reverse(explode("-",$row['vencimento']))) . "</td>";  
	echo "<td valign='top'>" . fornecedorNome($row['codFornecedor']) . "</td>";  
	echo "<td valign='top'>" . nl2br( $row['status']) . "</td>";  
	$tipo = ((int) $row['tipo'] == 0)?1:-1;
	echo "<td valign='top'>" . ((double) $row['valor']) * $tipo . "</td>";  
	echo "<td valign='top'><a href=?op=notas&act=detalhes&idNotas={$row['idNotas']}>Detalhes</a></td><td valign='top'><a href=?op=notas&act=process&idNotas={$row['idNotas']}>Processar</a></td><td valign='top'><a href=?op=notas&act=editar&idNotas={$row['idNotas']}>Editar</a></td><td><a href=?op=notas&act=excluir&idNotas={$row['idNotas']}>Excluir</a></td> ";
	echo "</tr>";
      }
      echo "</table>";
    }

    public function show_result_saida($result){
      echo "<table border=0 width=100% >"; 
      echo "<tr bgcolor=\"#9bffff\" >"; 
      echo "<th><b>Nome principal</b></th>"; 
      echo "<th><b>Nome Comercial</b></th>"; 
      echo "<th><b>Quantidade</b></th>"; 
      echo "<th><b>Valor Unit&aacute;rio</b></th>";
      echo "<th><b>Valor Total</b></th>";
      echo "<th><b>Data</b></th>"; 
      echo "</tr>";
      $cor = false;
      $total = 0;
      while($row = mysql_fetch_array($result)){ 
	foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
	if($cor) echo "<tr bgcolor=\"#f0f0f0\">";
	else echo "<tr bgcolor=\"#999999\">"; 
	$cor = !$cor;
	echo "<td valign='top'>" . nl2br( $row['nome']) . "</td>";  
	echo "<td valign='top'>" . nl2br( $row['segundoNome']) . "</td>";  
	echo "<td valign='top'>" . $row['quantidade'] . "</td>";  
	echo "<td valign='top'>" . $row['valor'] . "</td>";
	$subtotal = $row['valor'] *  $row['quantidade'];
	$total += $subtotal;
	echo "<td valign='top'>" . $subtotal . "</td>";
	echo "<td valign='top'>" . implode("/",array_reverse(explode("-",$row['data']))) . "</td>";  
	echo "</tr>";
      }
      echo "<tr bgcolor=\"#54FF9F\" ><th colspan=\"5\"><b>TOTAL</b></th><th align='right' width=15%>{$total}</th></tr>";
      echo "</table>";
    }    

    public function result_busca_codigo($cod){
      $result = mysql_query("SELECT * FROM `notas` WHERE codigo = '$cod'") or trigger_error(mysql_error());
      if(mysql_affected_rows() == 0) echo "Nenhum resultado encontrado!";
      else $this->show_result($result);
    }

    public function result_busca_fornecedor($fornecedor){
      $result = mysql_query("SELECT * FROM `notas` WHERE codFornecedor = '$fornecedor'") or trigger_error(mysql_error());
      if(mysql_affected_rows() == 0) echo "Nenhum resultado encontrado!";
      else $this->show_result($result);
    }

    public function result_busca_periodo($inicio,$fim,$campo){
      $result = mysql_query("SELECT * FROM `notas` WHERE {$campo} BETWEEN '$inicio' AND '$fim'") or trigger_error(mysql_error());
      if(mysql_affected_rows() == 0) echo "Nenhum resultado encontrado!";
      else $this->show_result($result);
    }

    public function result_busca_saida($idClientes,$inicio,$fim){
      if($inicio == "" && $fim == "")
	$result = mysql_query("SELECT * FROM `saida` WHERE idCliente = {$idClientes}") or trigger_error(mysql_error());
      else if($inicio == "")
	$result = mysql_query("SELECT * FROM `saida` WHERE idCliente = {$idClientes} AND data >= '$inicio'") or trigger_error(mysql_error());
      else if($fim == "")
	$result = mysql_query("SELECT * FROM `saida` WHERE idCliente = {$idClientes} AND data <= '$fim'") or trigger_error(mysql_error());
      else
	$result = mysql_query("SELECT * FROM `saida` WHERE idCliente = {$idClientes} AND data BETWEEN '$inicio' AND '$fim'") or trigger_error(mysql_error());
      if(mysql_affected_rows() == 0) echo "Nenhum resultado encontrado!";
      else $this->show_result_saida($result);
    }
    
    public function balancete(){
    	echo "<h1><center>Lan&ccedil;amentos</center></h1>";
    	echo "<div id='dialog-edit-conta-banco' title='Conta' tipo='d' cod='-1' >";
    	echo "<input type='radio' name='conta' value='1' checked />Transfer&ecirc;ncia BB<br/>";
    	echo "<input type='radio' name='conta' value='2' />Transfer&ecirc;ncia BRD<br/>";
    	echo "<input type='radio' name='conta' value='3' />Cheque BB<br/>";
    	echo "<input type='radio' name='conta' value='4' />Cheque BRD<br/>";
    	echo "<input type='radio' name='conta' value='5' />Dinheiro<br/>";
    	echo "</div>";
    	echo "<p><b>Per&iacute;odo:</b>"; 
    	echo "De <input id='inicio' type='text' name='inicio' maxlength='10' size='10' />";
    	echo "  At&eacute; <input id='fim' type='text' name='fim' maxlength='10' size='10' />";
    	echo "  <input type='checkbox' name='pendente'>Pendente 
    			<input type='checkbox' name='processada'>Processada 
    			<input type='checkbox' name='cancelada'>Cancelada";
    	echo "<p><button id='mostrar-balancete' >Mostrar</button>";
    	echo "<div id='balancete' ></div>";
//    	echo "<p><b>Usar extrato:</b><form enctype='multipart/form-data' name='form' method='POST'>";
//		echo "<input type='file' name='extrato'/>";
//		echo "<input type='submit' value='Enviar'/>";
//		echo "</form>";
    }
    
    public function showBalancete($extrato){
    	$arquivo = $extrato["tmp_name"]; 
  		$tamanho = $extrato["size"];
		$tipo = $extrato["type"];
		$nome = $extrato["name"];
		$flag = true;
		$first = true; $currentdate = ""; $lastdate = "";
		if ( $arquivo != "none" ){
    		$fp = fopen($arquivo, "rb");
    		echo "<table class='mytable' >";
    		while (($data = fgetcsv($fp,0,",")) !== FALSE) {
    			$num = count ($data);
    			$currentdate = $data[0];
    			if($flag){ 
    				echo "<thead><tr>";
    				for ($c=0; $c < $num; $c++){
	    				echo "<th>".utf8_encode($data[$c])."</th>";}
	    			echo "</tr></thead>";
    			} else {
    				if(($currentdate != $lastdate) && !$first){ 
    					$arraydata = array_reverse(explode("/",$lastdate));
    					$d = $arraydata[1]; $arraydata[1] = $arraydata[2]; $arraydata[2] = $d; 
    					$dataFatura = implode("-",$arraydata);
    					$sql = "SELECT p.id, p.valor, p.status, p.codBarras, DATE_FORMAT(p.vencimento,'%d/%m/%Y') as vencimento, n.codigo, n.idNotas,  n.tipo, f.razaoSocial, f.cnpj, f.contato ".
    					   		"FROM parcelas as p, notas as n, fornecedores as f ".
    					   		"WHERE p.vencimento = '{$dataFatura}' AND n.idNotas = p.idNota AND f.idFornecedores = n.codFornecedor";
	    				$result = mysql_query($sql);
    					if(mysql_num_rows($result) > 0 ) echo "<tr bgcolor='#54FF9F' ><td colspan='$num' >";
    					while($row = mysql_fetch_array($result)){ 
							foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
							$link = "<a href=?op=notas&act=detalhes&idNotas={$row['idNotas']}><img align='right' src='../utils/details_16x16.png' title='Detalhes' border='0'></a>";
							$dadosparcela = "nota: {$row['codigo']}, {$row['razaoSocial']} CPF/CNPJ: {$row['cpf']}, valor R$ {$row['valor']} venc: {$row['vencimento']} CB:{$row['codBarras']}  ({$row['status']}) $link";
							echo "<p><input type='checkbox' name='parcela-{$row['id']}' class='select-parcela' /> $dadosparcela";
   		 				}
    					if(mysql_num_rows($result) > 0 ) echo "</td></tr>";
    					echo "<tr bgcolor='#f0f0f0' ><td colspan='$num'></td></tr>";
    					echo "<tr>";
			    		for ($c=0; $c < $num; $c++){
  			    	  		echo "<td>".utf8_encode($data[$c])."</td>";}
    					echo "</tr>";
					}
					else {
						echo "<tr>";
			    		for ($c=0; $c < $num; $c++){
  			    	  		echo "<td>".utf8_encode($data[$c])."</td>";}
    					echo "</tr>";
					}
					$lastdate = $currentdate; $first = false;
    			}
				$flag = false;
    		}
			echo "</table>";
		    fclose($fp);
		}	
    }
}

if(isset($_POST['query'])){
	switch($_POST['query']){
		case "ipcg4":
			$p = new Busca();
	                $p->ipcg4_b($_POST['tipo'],$_POST['ipcf2'],$_POST['ipcg3']);
			break;
			case "ipcg3":
				$p = new Busca();
			$p->ipcg3_b($_POST['tipo'],$_POST['ipcf2']);
			break;
                    case "ipcf2":
				$p = new Busca();
			$p->ipcf2_b($_POST['tipo']);
			break;
			

	}

}
?>
