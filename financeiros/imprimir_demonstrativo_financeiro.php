<?php
$id = session_id();
if(empty($id))
  session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
class imprimir_demonstrativo_financeiro{
    
public function demonstrativo_financeiro($btipo,$status,$fim,$inicio,$todos_ur,$empresa,$comparar_porcentagem){
//extract($post,EXTR_OVERWRITE);
$fim1 = implode("/",array_reverse(explode("-",$fim)));
$inicio1 = implode("/",array_reverse(explode("-",$inicio)));

$html .= "<form><div>";
  $html .= "<h1>
               <a>
                 <img src='../utils/logo2.jpg' width='200' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              </a> 
              Demonstrativo Financeiro 
          </h1>";
  $html .= "<table width='100%'>
              <tr bgcolor='#EEEEEE' >
                  <td>
                       <center>
                             Per&iacute;do: {$inicio1} at&eacute; {$fim1} &nbsp;&nbsp; Status: {$status}
                      </center>
                 </td>
              </tr>
          </table>";
  $cont_status = 0;

//////////////verifica se é para comparar o percentual que foi atingido com o percentual cadastrado dos IPCGs para a UR.
if($comparar_porcentagem == 1 && $todos_ur != 1){
   $sql="SELECT 
             `COD_N2`,
             `COD_N3`,
             `PORCENTO`,
             (select
                 sum(`PORCENTO`)
             FROM 
                 `planocontas_porcento_ur` as p1
             WHERE 
                  UR={$empresa} and 
                  STATUS=1 and 
                  p1.`COD_N2` =p.`COD_N2` ) as percent2
            
          FROM 
             `planocontas_porcento_ur` as p 
          WHERE 
            UR={$empresa} and 
            STATUS=1";
            
            /////////Monta um array com os percentuais do IPCG# E IPCG2 e seus codigos para serem comparados com o resultado da busca do demonstrativo.
            $result = mysql_query($sql);
    $itens = '';

    while ($row = mysql_fetch_array($result)) {
        $itens[$row['COD_N3']] = array("porcento_ipcg2" =>number_format($row['percent2'],2,',','.'), "porcento_ipcg3" => number_format($row['PORCENTO'],2,',','.'),"ipcg2"=>$row['COD_N2']);
    }
   
   $comparar_porcentagem = 1; 
   
}

///////////////// Verificar por qual UR deve ser feito a consulta
if($todos_ur == 1){
    $condicao_ur = "";
    $todos_ur=1;
}else{
    $condicao_ur1 = " and R1.CENTRO_RESULTADO = {$empresa}";
     $condicao_ur2 = " and R2.CENTRO_RESULTADO = {$empresa}";
    $condicao_ur=" and R.CENTRO_RESULTADO = {$empresa}";
    $todos_ur=0;
}
/////Bloco que monta a condi��o do status para a consulta sql.
 if($status == 'Processada'){
		$condicao_status = " and P.status = '{$status}'";
	    $condicao_status1 = " and P1.status = '{$status}'";
	    $condicao_status2 = " and P2.status = '{$status}'";
	    $tipo_valor  = "sum(P.VALOR_PAGO* ( R.PORCENTAGEM /100 )) as VALOR,";
	    $tipo_valor1 =" sum(P1.VALOR_PAGO* ( R1.PORCENTAGEM /100 ))";
	    $tipo_valor2 =" sum(P2.VALOR_PAGO* ( R2.PORCENTAGEM /100 ))";
	}else {
		$condicao_status = " and P.status = '{$status}'";
		$condicao_status1 = " and P1.status = '{$status}'";
		$condicao_status2 = " and P2.status = '{$status}'";
		$tipo_valor= "sum(P.valor* ( R.PORCENTAGEM /100 )) as VALOR,";
		$tipo_valor1="sum(P1.valor* ( R1.PORCENTAGEM /100 ))";
		$tipo_valor2="sum(P2.valor* ( R2.PORCENTAGEM /100 ))";
 	}





////btipo recebe tipo da nota:1 desembolso ou 0 recebimento.
       if ($btipo == 1){
    		$cond_tipo=" and PL.N1 like 'D'";
    	}
    	if ($btipo == 0){
    		$cond_tipo =" and PL.N1 like 'R'";
    	}
    	if ($btipo == 't'){
    		$cond_tipo = " ";
    	}
 if($status == 'Processada'){
		$sql="SELECT 
		      P.idNota,
		      R.IPCG3,
		      PL.N4 as IPCG4,  
		      PL.N1 as tipo,	 
			  substr( COD_N3, 1, 3 ) as cod_n3,
			  COD_N4,
			  COD_N2,
			  COD_N3,
		      P.status as status_parcela,	 
		      {$tipo_valor}
			  (SELECT 
				{$tipo_valor1}
			FROM 
				parcelas as P1 inner join 
				notas as N1 ON (P1.idNota = N1.idNotas) inner join 
				rateio as R1 ON (N1.idNotas = R1.NOTA_ID) inner join 
				plano_contas as PL1 ON (PL1.ID = R1.IPCG4) 
			WHERE 
				P1.DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}'
				{$condicao_status1} 
				{$cond_tipo} and 
				PL1.COD_N2 = PL.COD_N2 and
                                PL1.ID >153
                                {$condicao_ur1}
			GROUP BY 
				PL1.COD_N2
			
			) as total,
			(SELECT 
					{$tipo_valor2}
				FROM 
					parcelas as P2 inner join 
					notas as N2 ON (P2.idNota = N2.idNotas) inner join 
					rateio as R2 ON (N2.idNotas = R2.NOTA_ID) inner join 
					plano_contas as PL2 ON (PL2.ID = R2.IPCG4) 
				WHERE 
					P2.DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}'
					{$condicao_status2} 
					{$cond_tipo} and 
					PL2.COD_N3 = PL.COD_N3 and
                                PL2.ID >153
                                        {$condicao_ur2}
				GROUP BY 
					PL.COD_N3
				) as total_n3,
			
			PL.N2,
                        (SELECT 
				{$tipo_valor1}
			FROM 
				parcelas as P1 inner join 
				notas as N1 ON (P1.idNota = N1.idNotas) inner join 
				rateio as R1 ON (N1.idNotas = R1.NOTA_ID) inner join 
				plano_contas as PL1 ON (PL1.ID = R1.IPCG4) 
			WHERE 
				P1.DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}'
				{$condicao_status1} 
				{$cond_tipo} and 
				PL1.N1 = 'D'and
                                PL1.ID >153
                                {$condicao_ur1}
			
			
			) as total_desembolso,
                        (SELECT 
				{$tipo_valor2}
			FROM 
				parcelas as P2 inner join 
				notas as N2 ON (P2.idNota = N2.idNotas) inner join 
				rateio as R2 ON (N2.idNotas = R2.NOTA_ID) inner join 
				plano_contas as PL2 ON (PL2.ID = R2.IPCG4) 
			WHERE 
				P2.DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}'
				{$condicao_status2} 
				{$cond_tipo} and 
				PL2.N1 = 'R'and
                                PL2.ID >153
                                {$condicao_ur2}
			
			
			) as total_recebimento
			
		FROM 
		      parcelas as P inner join       
		      notas as N ON (P.idNota = N.idNotas) inner join
		      rateio as R ON (N.idNotas = R.NOTA_ID) inner join 
		      plano_contas as PL ON (PL.ID = R.IPCG4)
		WHERE 
                      P.DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}' {$condicao_status} {$cond_tipo} {$condicao_ur}
                          and  PL.ID >153
		GROUP BY 
		      PL.COD_N4
		UNION
                    SELECT 
			0 AS idNota,
			PL.N3 as IPCG3,
			PL.N4 as IPCG4,
			PL.N1 AS tipo,
			PL.COD_N2 AS cod_n3,
			PL.COD_N4 AS COD_N4,
			PL.COD_N2,
			PL.COD_N3,
			'' AS status_parcela,
			SUM(itl.valor) AS VALOR,
			0 AS total,
			0 AS total_n3,
			PL.N2 AS N2,
			0 AS total_desembolso,
			0 AS total_recebimento
                    FROM
			impostos_tarifas imptar
			INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
			LEFT JOIN plano_contas PL ON (imptar.ipcf = PL.COD_N4)
                    WHERE
			itl.`data` BETWEEN '{$inicio}' AND '{$fim}'                
                    GROUP BY
                        PL.COD_N4
                    ORDER BY
                        tipo DESC,
                        COD_N4,
                        N2 ASC,
                        IPCG3 ASC";
		 }else{
    	
					    $sql="SELECT 
					      P.idNota,
					      R.IPCG3,
					      PL.N4 as IPCG4,  
					      PL.N1 as tipo,	 
						  substr( COD_N3, 1, 3 ) as cod_n3,
						  COD_N4,
						  COD_N2,
						  COD_N3,
					      P.status as status_parcela,	 
					      {$tipo_valor}
						  (SELECT 
							{$tipo_valor1}
						FROM 
							parcelas as P1 inner join 
							notas as N1 ON (P1.idNota = N1.idNotas) inner join 
							rateio as R1 ON (N1.idNotas = R1.NOTA_ID) inner join 
							plano_contas as PL1 ON (PL1.ID = R1.IPCG4) 
						WHERE 
							P1.vencimento BETWEEN '{$inicio}' and '{$fim}'
							{$condicao_status1} 
							{$cond_tipo} and 
							PL1.COD_N2 = PL.COD_N2 
                                                        {$condicao_ur1} and
                                                         PL1.ID >153
						GROUP BY 
							PL1.COD_N2
						
						) as total,
						(SELECT 
								{$tipo_valor2}
							FROM 
								parcelas as P2 inner join 
								notas as N2 ON (P2.idNota = N2.idNotas) inner join 
								rateio as R2 ON (N2.idNotas = R2.NOTA_ID) inner join 
								plano_contas as PL2 ON (PL2.ID = R2.IPCG4) 
							WHERE 
								P2.vencimento BETWEEN '{$inicio}' and '{$fim}'
								{$condicao_status2} 
								{$cond_tipo} and 
								PL2.COD_N3 = PL.COD_N3 and
                                                                PL2.ID >153
                                                                {$condicao_ur2}
							GROUP BY 
								PL.COD_N3
							) as total_n3,
						
						PL.N2,
                                                (SELECT 
							{$tipo_valor1}
						FROM 
							parcelas as P1 inner join 
							notas as N1 ON (P1.idNota = N1.idNotas) inner join 
							rateio as R1 ON (N1.idNotas = R1.NOTA_ID) inner join 
							plano_contas as PL1 ON (PL1.ID = R1.IPCG4) 
						WHERE 
							P1.vencimento BETWEEN '{$inicio}' and '{$fim}'
							{$condicao_status1} 
							{$cond_tipo} and 
							PL1.N1 = 'D'and
                                                        PL1.ID >153 
                                                        {$condicao_ur1}
						
						
						) as total_desembolso,
                                                (SELECT 
							{$tipo_valor1}
						FROM 
							parcelas as P1 inner join 
							notas as N1 ON (P1.idNota = N1.idNotas) inner join 
							rateio as R1 ON (N1.idNotas = R1.NOTA_ID) inner join 
							plano_contas as PL1 ON (PL1.ID = R1.IPCG4) 
						WHERE 
							P1.vencimento BETWEEN '{$inicio}' and '{$fim}'
							{$condicao_status1} 
							{$cond_tipo} and 
							PL1.N1 = 'R'  and
                                                         PL1.ID >153
                                                        {$condicao_ur1}
						
						
						) as total_recebimento
						
					FROM 
					      parcelas as P inner join       
					      notas as N ON (P.idNota = N.idNotas) inner join
					      rateio as R ON (N.idNotas = R.NOTA_ID) inner join 
					      plano_contas as PL ON (PL.ID = R.IPCG4)
					WHERE 
                                              P.vencimento BETWEEN '{$inicio}' and '{$fim}' {$condicao_status} {$cond_tipo} {$condicao_ur}
                                                  and  PL.ID >153
					GROUP BY 
					      PL.COD_N4
					ORDER BY 
						PL.N1 DESC,
						PL.COD_N4,
						PL.N2 ASC,
						R.IPCG3 ASC ";
    	} 
        
	  $result = mysql_query($sql);
          $sql_tarifas = "SELECT
                            imptar.ipcf AS COD_N4,
                            sum(itl.valor) AS val
                        FROM
                            impostos_tarifas imptar
                            INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
                            INNER JOIN contas_bancarias cb ON (cb.id = itl.conta)
                        WHERE
                            itl.data BETWEEN '{$inicio}' AND '{$fim}'
                        GROUP BY
                            COD_N4
                        ORDER BY
                            COD_N4 ASC";
        $resultset = mysql_query($sql_tarifas);
        $sql_total_n2 = "SELECT
                            sum(
                                P2.VALOR_PAGO * (R2.PORCENTAGEM / 100)
                            ) AS total
                        FROM
                                parcelas AS P2
                        INNER JOIN notas AS N2 ON (P2.idNota = N2.idNotas)
                        INNER JOIN rateio AS R2 ON (N2.idNotas = R2.NOTA_ID)
                        INNER JOIN plano_contas AS PL2 ON (PL2.ID = R2.IPCG4)
                        WHERE
                                P2.DATA_PAGAMENTO BETWEEN '{$inicio}' AND '{$fim}'
                        AND P2. STATUS = 'Processada'
                        AND PL2.COD_N2 = '2.5'
                        AND PL2.ID > 153
                        AND R2.CENTRO_RESULTADO = 1
                        GROUP BY
                                PL2.COD_N3
                                
                        UNION
                        
                        SELECT
                            sum(
                                    P1.VALOR_PAGO * (R1.PORCENTAGEM / 100)
                            ) as total_desembolso
                        FROM
                            parcelas AS P1
                            INNER JOIN notas AS N1 ON (P1.idNota = N1.idNotas)
                            INNER JOIN rateio AS R1 ON (N1.idNotas = R1.NOTA_ID)
                            INNER JOIN plano_contas AS PL1 ON (PL1.ID = R1.IPCG4)
                        WHERE
                            P1.DATA_PAGAMENTO BETWEEN '{$inicio}' AND '{$fim}'
                            AND P1. STATUS = 'Processada'
                            AND PL1.N1 = 'D'
                            AND PL1.ID > 153
                            AND R1.CENTRO_RESULTADO = 1";
                                
        $resultset_n2 = mysql_query($sql_total_n2); 
        $totais = [];
        while($row_total = mysql_fetch_array($resultset_n2, MYSQL_ASSOC)){
            $totais[] = $row_total['total'];
        }
        $tarifa = [];
        while ($tarifas = mysql_fetch_array($resultset)){
            $COD_N2 = substr($tarifas['COD_N4'],0,3);
            $COD_N3 = substr($tarifas['COD_N4'],0,5);
            $tarifa['total_desembolso'] = $totais[1];
            $tarifa[$COD_N2] = $tarifas['val'] + $totais[0];
            $tarifa[$COD_N3] = $tarifas['val'];
        }
	  $aux = '';	 
	  $total_geral   = 0;
	  $total_geral_r =0;
	  $total_geral_d =0;
	  $total_por_tipo = 0;
	  $cont =1;
	   $aux_n2 = '';
	   $aux_tipo = '';
	 
	  $num_rows= mysql_num_rows($result);
	   $cont2 =$num_rows;
	 $cor = "bgcolor='#eee'";
	
	
while($row = mysql_fetch_array($result)){
    if($comparar_porcentagem == 1 && $todos_ur == 0){
        
    if (array_key_exists($row['COD_N3'], $itens)) {
                    $porcento_ipcg2 = "<th>" . $itens[$row['COD_N3']]['porcento_ipcg2'] . "%</th>";
                    $porcento_ipcg3 = "<td>" . $itens[$row['COD_N3']]['porcento_ipcg3'] . "%</td>";
                    $p_ipcg2 = $itens[$row['COD_N3']]['porcento_ipcg2'];
                    $p_ipcg3 = $itens[$row['COD_N3']]['porcento_ipcg3'];




                    $porcento_n3 = ($row['total_n3'] / $row['total_desembolso']) * 100;
                    $sub = $porcento_n3 - number_format($p_ipcg3, 2, '.', ',');
                    if ($row['tipo'] == 'D') {
                        if ($sub > 2 || $sub < -2) {
                            $cor_ipcg3 = 'red';
                        } else {
                            $cor_ipcg3 = 'blue';
                        }
                    } else {
                        if ( $sub < -2) {
                            $cor_ipcg3 = 'red';
                        } else {
                            $cor_ipcg3 = 'blue';
                        }
                    }
                } else {
                    $cor_ipcg3 = 'black';
                    $porcento_ipcg2 = "<td><center>0,00%</center></td>";
                    $porcento_ipcg3 = "<td><center>0,00%</center></td>";
                }
}
   $cod_n3 = implode("",(explode(".",$row['cod_n3'])));
   if($cont2--%2 == 0){	
		$class = "odd";
	}else{
		$class = "";
	}
	
	if($row['tipo'] == 'R'){
		 $total_geral_r += $row['VALOR'];
		
		}else{
		$total_geral_d += $row['VALOR'];
		}
		
	     $total_geral = $total_geral_r - $total_geral_d ;
	     $total_por_tipo += $row['VALOR']; 

	if($row['tipo'] !=  $aux_tipo){
	 $aux_tipo=$row['tipo'] ;
	 if($aux_tipo == 'R'){
	      $html .= "<table width='100%' >
                                            <tr>
                                                <td colspan='4'>
                                                       <h2>
                                                          <center>RECEBIMENTO</center>
                                                       </h2>
                                               </td>
                                            </tr>    
                                        ";
              }else{
		    if($cont>1){
				$html .= "<tr> 
                                              <td width='50%'>
                                                 <b>TOTAL RECEBIMENTO:</b>
                                              </td>
                                              <td>
                                                <b> R$ ".number_format($total_geral_r,2,',','.')."</b>
                                              </td>
                                           </tr>
                      </table>
                      <br>
                                        <table width='100%' >
                                            <tr>
                                                <td colspan='4'>
                                                   <h2>
                                                        <center>DESEMBOLSO</center>
                                                   </h2>
                                               </td>
                                           </tr>    
                                        ";
                    }else{
				$html .= "<br>
                                        <table width='100%'>
                                            <tr>
                                                <td colspan='4'>
                                                       <h2>
                                                         <center>DESEMBOLSO</center>
                                                       </h2>
                                                 </td>
                                           </tr>    
                                        ";
                    }
		 }
		 
	
	
	}
	
	if($row['tipo'] =='D'){
                if($row['N2'] !=  $aux_n2){
                    if (array_key_exists($row['COD_N2'], $tarifa)) {                
                        $row['total'] += $tarifa[$row['COD_N2']];
                        $total_geral_d += $tarifa[$row['COD_N2']];
                        if($row['COD_N2'] == '2.5'){
                            $total_porcentagem = ($row['total']/$tarifa['total_desembolso'])*100;
                        }
                    }  else {
                        $total_porcentagem = 0;
                    }
                    $aux_n2=$row['N2'] ;
                    $porcento_n2 = (($row['total']/$row['total_desembolso'])*100) + $total_porcentagem;
                    $html .= "
                                    <tr>
                                        <td widtd='50%' align='left'>
                                            <b><span >{$row['COD_N2']}</span> {$row['N2']} </b>
                                        </td>
                                        <td>
                                            <b>Total: R$ ".number_format($row['total'],2,',','.')."</b>
                                        </td>
                                        <td>
                                           <center> ".number_format($porcento_n2,2,',','.')." %</center>
                                        </td>
                                        {$porcento_ipcg2}
                                    </tr>
                             ";

                }
            if($row['IPCG3'] !=  $aux){
                if (array_key_exists($row['COD_N3'], $tarifa)) {                
                    $row['total_n3'] += $tarifa[$row['COD_N3']]; 
                    $total_porcentagem = ($row['total_n3']/$tarifa['total_desembolso'])*100;
                }
                  $classe_item = "G".$cont;
                        $aux = $row['IPCG3'];
                        $porcento_n3 = (($row['total_n3']/$row['total_desembolso'])*100) + $total_porcentagem;

                        $html .= "<tr >
                                        <td width='50%'>
                                                <b>
                                                    <span > {$row['COD_N3']} </span>
                                                    {$row['IPCG3']}
                                               </b>
                                         </td>
                                         <td>
                                                <b>Total: R$ ".number_format($row['total_n3'],2,',','.')."</b>
                                         </td>
                                          <td>
                                                 <font color='{$cor_ipcg3}'>
                                                   <center>".number_format($porcento_n3,2,',','.')." %</center>
                                                 </font>
                                           </td>
                                           {$porcento_ipcg3}
                                    </tr>";
             }


                        $html .= "<tr>
                                            <td style='padding-left:20px;' width='50%'>
                                                  {$row[COD_N4]} - {$row['IPCG4']}
                                            </td>
                                            <td> 
                                                   R$ ".number_format($row['VALOR'],2,',','.')."
                                            </td>
                                  </tr>";
}else {
    if($row['N2'] !=  $aux_n2){
	 $aux_n2=$row['N2'] ;
         $porcento_n2 = ($row['total']/$row['total_recebimento'])*100;
	$html .= "
                        <tr>
                            <td widtd='50%' >
                                <b><span >{$row['COD_N2']}</span> {$row['N2']} </b>
                            </td>
                            <td>
                                <b>Total: R$ ".number_format($row['total'],2,',','.')."</b>
                            </td>
                            <td>
                               <center> ".number_format($porcento_n2,2,',','.')." %</center>
                            </td>
                            {$porcento_ipcg2}
                        </tr> ";
	
	}
    if($row['IPCG3'] !=  $aux){
                $classe_item = "G".$cont;
		$aux = $row['IPCG3'];
		$porcento_n3 = ($row['total_n3']/$row['total_recebimento'])*100;
	    
		$html .= "<tr>
                                    <td width='50%'>
                                       <b><span > {$row['COD_N3']} </span>{$row['IPCG3']}</b>
                                    </td>
                                    <td>
                                        <b>Total: R$ ".number_format($row['total_n3'],2,',','.')."</b>
                                    </td>
                                    <td>
                                        <font color='{$cor_ipcg3}'>
                                            <center>".number_format($porcento_n3,2,',','.')." %</center>
                                        </font>
                                    </td>
                                    {$porcento_ipcg3}
                                    </tr>";
	 }
	 
	 
		$html .= "<tr>
                                    <td style='padding-left:20px;' width='50%'>  
                                        {$row[COD_N4]} - {$row['IPCG4']}
                                    </td>
                                    <td> 
                                        R$ ".number_format($row['VALOR'],2,',','.')."
                                    </td>
                         </tr>";
		

}	
		 
	 $cont++;
	 
	 
	 
	 
	
}
   

 ////exibir o total se o btipo = t
 if($btipo == 't' && $total_geral_d > 0){
 
 
  $html .= "<tr>
              <td width='50%'>
                <b>TOTAL DESEMBOLSO:</b>
              </td>
              <td>
                <b> R$ ".number_format($total_geral_d,2,',','.')."</b>
              </td>
          </tr>";
 
 
 }
	
	$html .= "</table>
            </br>
            </br>";
	$html .= "<table class='mytable' width='100%'>";
                $html .= "<tr>
                             <td width='50%'>
                               <b>TOTAL PERÍODO:</b>
                             </td>
                             <td>
                                <b> R$ ".number_format($total_geral,2,',','.')."</b>
                            </td>
                        </tr>";
	$html .= "</table></br>";

      $paginas []= $header.$html.= "</div></form>";
	
	
	//print_r($paginas);
	//print_r($paginas);
	
	$mpdf=new mPDF('pt','A4',9);
	$mpdf->SetHeader('pÃ¡gina {PAGENO} de {nbpg}');
	$ano = date("Y");
	$mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
	$mpdf->WriteHTML("<html><body>");
	$flag = false;
	foreach($paginas as $pag){
		if($flag) $mpdf->WriteHTML("<formfeed>");
		$mpdf->WriteHTML($pag);
		$flag = true;
	}
        
	$mpdf->WriteHTML("</body></html>");
	$mpdf->Output('demonstrativo_financeiro.pdf','I');
	exit;

       // print_r($html);


}

}
$p = new imprimir_demonstrativo_financeiro();
$p->demonstrativo_financeiro($_GET['btipo'],$_GET['status'],$_GET['fim'],$_GET['inicio'],$_GET['todos_ur'],$_GET['empresa'],$_GET['comparar_porcentagem']);


?>