$(function($) {
    $("#atualizar-fechamento").live('click', function () {
        var data_fechamento = moment($("#data-fechamento").val());
        var data_fechamento_anterior = moment($("#data-fechamento-anterior").val());
        console.log(data_fechamento, data_fechamento_anterior);
        if(data_fechamento.isBefore(data_fechamento_anterior)) {
            $("#modal-justificativa-fechamento").modal("show");
        } else {
            $("#form-fechamento-caixa").submit();
        }
    });
});