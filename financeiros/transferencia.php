<?php
$id = session_id();
if(empty($id))
  session_start();

include_once('../db/config.php');
include_once('../utils/codigos.php');

Class Transferencia{
     public function pesquisarTransferenciaBancaria(){
      
             echo "<center><h1>Transfer&ecirc;ncia Bancaria</h1></center>";
             if(isset($_SESSION['permissoes']['financeiro']['financeiro_transferencia_bancaria']['financeiro_transferencia_bancaria_nova']) || $_SESSION['adm_user'] == 1){
             echo "<div style='text-align:right'>                
                    <a href='?op=transferencia&action=criar'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button'  type='button'>
                        <span class='ui-button-text'>+ Nova</span>
                    </a>             
                
             </div>";
             }
             echo "
                 <fieldset>
                      <legend><b>Buscar</b></legend>";                            
                echo "<div id='dialog-cancelar-transferencia' data-id-transferencia=''
                        title='Cancelar Transfer&ecirc;ncia'>                
                        </div>";
                echo "<div id='div-transferencia'>";  
                        echo "<p> <b>Conta Origem:</b> <span id='transferencia_origem'>" .siglaOrigemFundos('origem','transferencia-origem')."</span> </p>";
                        echo "<p> <b>Conta Destino:</b><span id='transferencia_destino'> " .siglaOrigemFundos(NULL,'transferencia-destino')."</span></p>";
                        echo "<p>  
                                    <span id='span-periodo'>
                                        <label for='inicio'>
                                            <b>Inicio: </b>
                                        </label>
                                        <input id='inicio'  type='text' style='text-align:right'  class='data OBG'/>
                                        <label for='fim'>
                                            <b>Fim: </b>
                                        </label>
                                        <input id='fim'  type='text' style='text-align:right'  class='data OBG'/>
                                    </span>
                                </p>"; 
                echo "</div>";            
                echo"<div id='span-pesquisar-transferencia' style='text-align:center'>
                        <button id='pesquisar-transferencia' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button'  type='button'>
                             <span class='ui-button-text'>Pesquisar</span>
                        </button>
                    
                    &nbsp;&nbsp;
                    <button id='voltar_' type = 'button' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'
                    role='button' aria-disabled='false' >
                    <span class='ui-button-text'>Voltar</span>
                    </button>
                    </div>
                </fieldset>";
         echo "<div id='div-pesquisa-transferencia'></div>";
         
     }

     public function criarTransferenciaBancaria(){
        
        echo "<center><h1>Transfer&ecirc;ncia Bancaria</h1></center>
        <div style='text-align: right;'>
            <a href='?op=transferencia' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button' type='button'>
                <span class='ui-button-text'>Listar</span>
            </a>
        </div>
        <fieldset>
                <legend><b>Criar Nova</b></legend>";     
        echo "<div id='div-transferencia'>";
         $fechamento_caixa = \App\Controllers\FechamentoCaixa::getFechamentoCaixa();
         echo "<input id='data-fechamento' type='hidden' value='{$fechamento_caixa['data_fechamento']}'>";
            echo "<p> 
                    <b>Conta Origem:</b> 
                    <span id='transferencia_origem'>" .siglaOrigemFundos('origem', 'transferencia-origem', 'N')."</span>
                </p>";
            echo "<p>
                     <b>Conta Destino:</b>
                     <span id='transferencia_destino'> " .siglaOrigemFundos(NULL, 'transferencia-destino','N')."</span>
                </p>";
            echo "<p>
                    <span id='span-data'>   
                        <label for='data'><b>Data: </b></label>
                        <input id='data' type='text' style='text-align:right' class='verifica-fechamento OBG '/>
                    </span>
                </p>"; 
            echo "<p>
                    <span id='span_doc_bancario'>
                        <label for='doc_bancario'><b>Num. Doc. Bancário </b></label>
                        <input id='doc_bancario'  type='text' maxlength='20' size='20' class='OBG' />
                    <span>
                </p>"; 
            echo "<p>
                        <span id='span_valor_transferencia'>
                        <label for='valor_transferencia'><b>Valor: </b></label>
                        <input id='valor_transferencia'  type='text' maxlength='15' style='text-align:right'   value='0.00' class='valor OBG'/>
                        <span>
                    </p>"; 
            echo "<p><span id='span_descricao'><label for='descricao'><b>Descrição/Motivo : </b></label><br/><textarea id='descricao' cols='60' rowls='2'></textarea><span></p>"; 
            echo "<p><span id='span-periodo' style='display:none;'><label for='inicio'><b>Inicio: </b></label><input id='inicio'  type='text' style='text-align:right'  class='data'/>
                <label for='fim'><b>Fim: </b></label><input id='fim'  type='text' style='text-align:right'  class='data '/></span></p>"; 
        echo "</div>";
        echo"<div style='text-align:center'>
                <button id='salvar-transferencia' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' 
                aria-disabled='false' role='button'  type='button'>
                    <span class='ui-button-text'>Salvar</span>
                </button>
               &nbsp;&nbsp;
                <button id='voltar_' type = 'button' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'
                role='button' aria-disabled='false' >
                    <span class='ui-button-text'>Voltar</span>
                </button>
            </div>        
        </fieldset>";
    }
}
