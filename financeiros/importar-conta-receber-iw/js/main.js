$(function($) {
    $('.chosen-select').chosen();

    $('#buscar-faturas-enviadas').click(function () {
        if(!validar_campos('div-pesquisar-faturas-enviadas')){
            return false;
        }
    });

    $(".valor").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $(".marcar-todos").live('click', function () {
        $("#tbody-parcelas-result")
            .find('input:checkbox')
            .not(this)
            .prop('checked', this.checked);
    });

    $('#pesquisar-impostos-retidos-pendentes').on('click', function(){
        var impostos_adicionados = [];
        var tipo_nota = $("#tipo-nota").val();
        var $parcelasFaturaElem = $(".parcelas-fatura");
        if($parcelasFaturaElem.length > 0) {
            $parcelasFaturaElem.each(function () {
                impostos_adicionados.push($(this).val());
            })
        }

        if(validar_campos('div-pesquisar-impostos-retidos-pendentes')){
            var data =   $("#form-impostos-retidos-pendentes").serialize();
            data += "&tipo_nota=" + tipo_nota;

            $.ajax({
                url: "/financeiros/fatura-imposto/?action=pesquisar-nf-impostos-retidos-pendentes",
                type: 'GET',
                data: data,
                success: function (response) {
                    if(response != '0') {
                        response = JSON.parse(response);
                        if (Object.keys(response).length > 0) {
                            var htmlTr = '';
                            for (var i = 0; i < response.length; i++) {
                                var dataVencimento = response[i]['data_vencimento'].split('-').reverse().join('/');
                                var nomeFornecedor = response[i]['nome_fornecedor'];
                                var ur = response[i]['ur'];
                                var num_parcela = response[i]['num_parcela'];
                                var valorPagar = float2moeda(response[i]['valor_pagar']);
                                var sigla = response[i]['sigla'];
                                var idParcela = response[i]['id_parcela'];
                                var idNota = response[i]['id_nota'];
                                var codigo = response[i]['codigo'];
                                var strikethrough = '';
                                var disabled = '';
                                var checked = '';

                                if($.inArray(idParcela, impostos_adicionados) != '-1') {
                                    strikethrough = " style='text-decoration: line-through;' ";
                                    disabled = ' disabled ';
                                    checked = ' checked ';
                                }

                                htmlTr += "<tr class='tr-parcelas-buscar' " + strikethrough + ">" +
                                    "<td class='text-center'>" +
                                    "<input type='checkbox' " +
                                    disabled +
                                    checked +
                                    "class='check-adicionar-parcela' " +
                                    "data-vencimento='" + dataVencimento + "'" +
                                    "nome-fornecedor='" + nomeFornecedor + "'" +
                                    "valor-pagar='" + valorPagar + "'" +
                                    "sigla='" + sigla + "'" +
                                    "id-nota='" + idNota + "'" +
                                    "codigo='" + codigo + "'" +
                                    "id-parcela='" + idParcela + "'" +
                                    "/>" +
                                    "</td>" +
                                    "<td class='text-center'>" + codigo + "</td>" +
                                    "<td class='text-center'>" + num_parcela + "</td>" +
                                    "<td>" + ur.toUpperCase() + "</td>" +
                                    "<td>" + nomeFornecedor + "</td>" +
                                    "<td class='text-center'>" + dataVencimento + "</td>" +
                                    "<td>" + valorPagar + "</td>" +
                                    "<td class='text-center'>" + sigla + "</td>" +
                                    "</tr>";
                            }

                            $("#tbody-parcelas-result").html(htmlTr);
                            $('#modal-add-parcelas').modal();
                        } else {
                            alert("Nenhuma parcela encontrada para os filtros escolhidos");
                        }
                    } else {
                        alert("Nenhuma parcela encontrada para os filtros escolhidos!");
                    }
                }
            });
        }else{
            alert("Algum campo obrigatório não foi informado.");
        }
    });

    $('#adicionar-parcela').live('click', function () {
        var aux = 0;
        var faturaLista = 0;
        parcelasId = [];
        var valor_total = parseFloat($("#valor-total").val());

        $('.id_fatura').each(function () {
            parcelasId.push($(this).attr('fatura'));
        });

        $("#info-fatura-nota").remove();

        $('.check-adicionar-parcela:not(:disabled)').each(function () {
            if($(this).is(':checked')){
                var parcelasID = $(this).attr('idParcela');
                aux++;

                if(!parcelasId.includes(parcelasID)) {
                    var valor_pagar = parseFloat(
                        $(this)
                            .attr('valor-pagar')
                            .replace('.', '')
                            .replace(',', '.')
                    );
                    valor_total += valor_pagar;
                    $("#tbody-fatura-nota").append(
                        "<tr class='tr-parcela-fatura'>" +
                        "   <td>" +
                        "       <button title='Remover Parcela' type='button' role='button'" +
                        "               class='btn btn-danger remover-parcela'>" +
                        "           <i class='fa fa-times'></i>" +
                        "       </button>" +
                        "       <input type='hidden' valor-parcela='" + valor_pagar + "' name='parcelas[" +  $(this).attr('id-nota') + "][]' " +
                        "              class='parcelas-fatura' value='"+ $(this).attr('id-parcela')+"' />"+
                        "   </td>"+
                        "   <td>" + $(this).attr('codigo') + "</td>" +
                        "   <td>" + $(this).attr('nome-fornecedor') + "</td>" +
                        "   <td>" + $(this).attr('data-vencimento') + "</td>" +
                        "   <td>" + $(this).attr('sigla') + "</td>" +
                        "   <td>R$ " + $(this).attr('valor-pagar')+ "</td>" +
                        "</tr>"
                    );
                }else{
                    faturaLista++;
                }
            }
        });

        $("#valor-total-text").text("R$ " + float2moeda(valor_total));
        $("#valor-total").val(valor_total);

        $(".valor").priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        if(aux == 0){
            alert('Selecione uma Parcela antes de tentar adicionar.');
            return false;
        }
        $("#tbody-fatura-result").html('');
        $('#modal-add-fatura').modal('hide');
    });

    $(".remover-parcela").live('click', function () {
        $(this).closest('tr').remove();
        var valor_total = 0;

        $(".tr-parcela-fatura input").each(function () {
            var valor_pagar = parseFloat($(this).attr('valor-parcela'));
            valor_total += valor_pagar;
        });

        $("#valor-total-text").text("R$ " + float2moeda(valor_total));
        $("#valor-total").val(valor_total);
    });

    $(".tr-parcelas-buscar td").live('click', function (event) {
        if (event.target.type !== 'checkbox') {
            $(this)
                .parent()
                .find('input:checkbox')
                .trigger('click');
        }
    });

    $('#dados-nota-aglutinacao').live('click', function () {
        var $inputParcelas = $(".parcelas-fatura");

        if(typeof $inputParcelas != 'undefined' && $inputParcelas.length >= 1) {
            $("#modal-dados-nota").modal('show');
        } else {
            alert('Para finalizar a nota de aglutinação, é necessário adicionar uma ou mais parcelas!');
        }
    });

    $('#criar-nota-aglutinacao').live('click', function () {
        var $inputParcelas = $(".parcelas-fatura");

        if(validar_campos('div-dados-nota-aglutinacao')){
            var tipo_nota = $("#tipo-nota").val();
            var tipo_documento_nota = $("#tipo-documento-nota").val();
            var numero_nota = $("#numero-nota").val();
            var natureza_nota = $("#natureza-nota").val();
            var fornecedor_nota = $("#fornecedor-nota").val();
            var ur_nota = $("#ur-nota").val();
            var data_emissao = $("#data-emissao").val();
            var data_vencimento = $("#data-vencimento").val();
            var descricao = $("#descricao").val();
            var parcelasId = [];
            var parcelasValor = [];
            if($inputParcelas.length > 0) {
                $inputParcelas.each(function () {
                    parcelasId.push($(this).val());
                    parcelasValor.push($(this).attr('valor-parcela'));
                });
            }
            var data = {
                'parcelas_id': parcelasId,
                'parcelas_valor': parcelasValor,
                'tipo_nota': tipo_nota,
                'tipo_documento_nota': tipo_documento_nota,
                'numero_nota': numero_nota,
                'natureza_nota': natureza_nota,
                'fornecedor_nota': fornecedor_nota,
                'ur_nota': ur_nota,
                'data_emissao': data_emissao,
                'data_vencimento': data_vencimento,
                'descricao': descricao
            };
            $.ajax({
                url: "/financeiros/fatura-imposto/?action=criar-nota-aglutinacao",
                type: 'POST',
                data: data,
                success: function (response) {
                    if(response != '0') {
                        alert('Nota criada com sucesso! TR ' + response);
                        window.location.href = '/financeiros/fatura-imposto/?action=criar-fatura-imposto&tipo=' + tipo_nota;
                    } else {
                        alert('Houve um problema ao criar a nota de aglutinação! Entre em contato com o TI!');
                    }
                }
            });
        }
    });

    $("#visualizar-parcelas-aglutinadas").live('click', function () {
        var $this = $(this);
        var nota_id = $this.attr('id-nota-aglutinacao');
        var $tbodyParcelas = $("#tbody-parcelas-result");

        $.ajax({
            url: "/financeiros/fatura-imposto/?action=visualizar-parcelas-aglutinadas",
            type: 'GET',
            data: {nota_id: nota_id},
            success: function (response) {
                if(response != '0') {
                    response = JSON.parse(response);
                    if (Object.keys(response).length > 0) {
                        $tbodyParcelas.html("");
                        $.each(response, function (index, value) {
                            var dataVencimento = value.data_vencimento.split('-').reverse().join('/');
                            var valorPagar = float2moeda(value.valor_pagar);
                            $tbodyParcelas.append(
                                "<tr>" +
                                "   <td class='text-center'>" + value.codigo + "</td>" +
                                "   <td class='text-center'>" + value.num_parcela + "</td>" +
                                "   <td>" + value.ur.toUpperCase() + "</td>" +
                                "   <td>" + value.nome_fornecedor + "</td>" +
                                "   <td class='text-center'>" + dataVencimento + "</td>" +
                                "   <td>" + valorPagar + "</td>" +
                                "   <td class='text-center'>" + value.sigla + "</td>" +
                                "</tr>"
                            );
                        });
                        $('#modal-visualizar-parcelas').modal("show");
                    }
                } else {
                    alert('Houve um problema ao trazer as parcelas aglutinadas! Entre em contato com o TI!');
                }
            }
        });
    });

    $("#desfazer-nota-aglutinacao").live('click', function () {
        var $this = $(this);
        var nota_id = $this.attr('id-nota-aglutinacao');

        $.ajax({
            url: "/financeiros/fatura-imposto/?action=desfazer-nota-aglutinacao",
            type: 'GET',
            data: {nota_id: nota_id},
            success: function (response) {
                if(response == '1') {
                    alert('Nota desfeita com sucesso!');
                    $this.parents('tr').remove();
                } else if(response == 'processada') {
                    alert('Essa nota já foi processada/conciliada! Desfaça o processamento e/ou conciliação primeiro para depois desfazê-la!');
                } else if(response == '0') {
                    alert('Houve um problema ao desfazer a nota de aglutinação! Entre em contato com o TI!');
                }
            }
        });
    });

    function float2moeda(num) {
        x = 0;
        if(num<0) {
            num = Math.abs(num);
            x = 1;
        }
        if(isNaN(num)) num = "0";
        cents = Math.floor((num*100+0.5)%100);
        num = Math.floor((num*100+0.5)/100).toString();
        if(cents < 10) cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
            num = num.substring(0,num.length-(4*i+3))+'.'
                +num.substring(num.length-(4*i+3));
        ret = num + ',' + cents;
        if (x == 1) ret = ' - ' + ret;return ret;
    }
});