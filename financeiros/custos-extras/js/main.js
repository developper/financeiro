$(function($) {


    $("#salvar-custo-extra").click(function(){

       if(validar_campos('div-adicionar-custo-extra')){
           var custo = $("#custo-extra").val();

           $.post( '/financeiros/custos-extras/?action=salvar-custos-extras',
               {custo:custo},
               function(r){
                    if(r == 1){

                        alert('Custo Extra salvo com sucesso!');
                        window.location.reload();

                    }else{
                        alert(r+" Entre em contato com o TI");
                        return false;
                    }
               });
       }
        return false;
    });

    $(".desativar-custo-extra").click(function(){

        if(confirm('Deseja realmente desativar o item?')){
            var idCusto = $(this).attr('item');

            $.post( '/financeiros/custos-extras/?action=desativar-custo-extra',
                {idCusto:idCusto},
                function(r){
                    if(r == 1){

                        alert('Custo extra desativado com sucesso!');
                        window.location.reload();

                    }else{
                        alert(r+" Entre em contato com o TI");
                        return false;
                    }
                });
        }
        return false;
    });
    $(".ativar-custo-extra").click(function(){

        if(confirm('Deseja realmente ativar o item?')){
            var idCusto = $(this).attr('item');

            $.post( '/financeiros/custos-extras/?action=ativar-custo-extra',
                {idCusto:idCusto},
                function(r){
                    if(r == 1){

                        alert('Custo extra ativado sucesso!');
                        window.location.reload();

                    }else{
                        alert(r+" Entre em contato com o TI");
                        return false;
                    }
                });
        }
        return false;
    });










    function validar_campos(div){
        var ok = true;
        $("#"+div+" .OBG").each(function (i){

            if(this.value == ""){
                ok = false;
                this.style.border = "3px solid red";
            } else {
                this.style.border = "";
            }

        });
        $("#"+div+" .COMBO_OBG option:selected").each(function (i){
            if(this.value == "-1"){
                ok = false;
                $(this).parent().css({"border":"3px solid red"});
            } else {
                $(this).parent().css({"border":""});
            }
        });

        $("#"+div+" .OBG_CHOSEN option:selected").each(function (i){

            if(this.value == "" || this.value == -1){

                $(this).parent().parent().children('div').css({"border":"3px solid red"});
            } else {

               var estilo = $(this).parent().parent().children('div').attr('style');
                $(this).parent().parent().children('div').removeAttr('style');
                $(this).parent().parent().children('div').attr('style',estilo.replace("border: 3px solid red",''));
            }
        });

        if(!ok){
            $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
            $("#"+div+" .div-aviso").show();
        } else {
            $("#"+div+" .aviso").text("");
            $("#"+div+" .div-aviso").hide();
        }
        return ok;
    }

    $(".cancelar").live('click',function(){
        var eventoAPH = $(this);
        var id = eventoAPH.attr('id-evento-aph');
        $.post( '/eventos_aph/?action=cancelar-evento-aph',
            {id:id},
            function(r){
                if(r == 1){
                    eventoAPH.parent().parent().parent().remove();
                    alert('Evento/APH cancelado com sucesso!');
                }else{
                    alert('Erro ao cancelar Evento/APH. Entre em contato com a equipe do TI');
                }
            }
        );
    });





});
