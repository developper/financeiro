<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';
//ini_set('display_errors', 1);

use \App\Controllers\CustosExtras;

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
	"GET" => [
		'/financeiros/custos-extras/?action=custos-extras-form' => '\App\Controllers\CustosExtras::form',
		],
	"POST" => [
		'/financeiros/custos-extras/?action=salvar-custos-extras' => '\App\Controllers\CustosExtras::salvar',
		'/financeiros/custos-extras/?action=desativar-custo-extra' => '\App\Controllers\CustosExtras::desativar',
		'/financeiros/custos-extras/?action=ativar-custo-extra' => '\App\Controllers\CustosExtras::ativar',
        ]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}
