<?php
$id = session_id();
if(empty($id))
    session_start();
include_once('../validar.php');
include('../db/config.php');

$extension = '.xls';

if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
    $extension = '.xlsx';
}

//ini_set('display_errors', 1);

header("Content-type: application/excel");
header("Content-type: application/force-download;");
header("Content-Disposition: attachment; filename=extrato".$extension);
header("Pragma: no-cache");

Class exportar_excel_extrato {

    function detalhe_extrato($id_conta,$conciliado,$inicio,$fim){

        $sql = "SELECT
				c.ID as id_conta,
				o.forma as banco,
				c.AGENCIA as agencia,
				c.NUM_CONTA as conta,
				e.nome as ur,
				tipo_conta_bancaria.nome as tipoConta,
                c.SALDO_INICIAL as saldo_inicial,
				c.DATA_SALDO_INICIAL as data_saldo_inical
			FROM
				contas_bancarias as c
				INNER JOIN origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id)
				INNER JOIN empresas as e ON (c.UR = e.id)
				LEFT JOIN tipo_conta_bancaria ON tipo_conta_bancaria.id = c.TIPO_CONTA_BANCARIA_ID
			WHERE
				c.ID = {$id_conta}
			ORDER BY
			banco
			";
        $result = mysql_query($sql);
        $tipoConta = mysql_result($result, 0, 5);
        $conta = mysql_result($result,0,1);
        $agencia = mysql_result($result,0,2);
        $num_conta = mysql_result($result,0,3);
        $ur = mysql_result($result,0,4);
        $saldo_inicial = mysql_result($result,0,6);
        $data_saldo_inicial = \DateTime::createFromFormat('Y-m-d', mysql_result($result,0,7))->format('d/m/Y');
        $cor_saldo = "";
        if($saldo_inicial < 0) {
            $cor_saldo = "style='font-size:12px; color:red;'";
        }

        $var  ="<table>
            <tr>
            <td><b>Tipo:&nbsp;{$tipoConta}</b></td>
            <td><b>Banco:</b>&nbsp;{$conta}</td>";
        $var .="<td><b>Agencia:</b>&nbsp;{$agencia}</td>";
        $var .="<td><b>Conta:</b>&nbsp;{$num_conta}</td>";
        $var .="<td><b>UR:</b>&nbsp;{$ur}</b></td>";
        $var .="<td><b>SALDO INICIAL:</b><span {$cor_saldo}>R$ " . number_format($saldo_inicial,2,',','.') . "</span>&nbsp;(Em: {$data_saldo_inicial})</b></td>
                        <tr>";


        $fim1 = implode("-",array_reverse(explode("/",$fim)));
        $inicio1 = implode("-",array_reverse(explode("/",$inicio)));
        $sql_sald_inicial="SELECT 
            `SALDO_INICIAL` , 
            DATE_FORMAT( `DATA_SALDO_INICIAL` , '%d/%m/%Y' ),
            `DATA_SALDO_INICIAL` 
            FROM 
            `contas_bancarias` 
            WHERE 
            `ID` ={$id_conta}";
        $result_sald_inicial = mysql_query($sql_sald_inicial);
        $saldo_inicial = mysql_result($result_sald_inicial,0,0);
        $data_saldo_inicial_pt = mysql_result($result_sald_inicial,0,1);
        $data_saldo_inicial_us =mysql_result($result_sald_inicial,0,2);
        $total_conciliado = $saldo_inicial;
        $total_nconciliado = $saldo_inicial;
        $total = $saldo_inicial;
        $auxMovimentacaoAnterior = 0;

        $cond_conciliado = $conciliado == 'N' ? "p.CONCILIADO='N'" : "p.CONCILIADO='S'";
        $cond_data_parcela = $conciliado == 'N' ? "p.DATA_PAGAMENTO" : "DATA_CONCILIADO";
        $cond_conciliado_baixa = $conciliado == 'N' ? "nb.conciliado='N'" : "nb.conciliado='S'";
        $cond_data_baixa = $conciliado == 'N' ? "nb.data_pagamento" : "nb.data_conciliado";
        $unionTarifas = "";
        $unionEstorno = "";
        if($conciliado == 'S') {
            $unionTarifas = <<<SQL
UNION
  SELECT
    itl.id,
    'S' AS CONCILIADO,
    itl.valor,
    0 AS idNota,
    0 AS TR,
    itl.documento,
    DATE_FORMAT(itl.data, '%d/%m/%Y') AS data_pg,
    IF(debito_credito = 'DEBITO', 1, 0) AS tipo_nota,
    imptar.item,
    0 AS fornecedor_id,
    itl.data AS data_pg_us
  FROM
    impostos_tarifas imptar
    INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
  WHERE
    itl.conta = {$id_conta}
SQL;
            $unionEstorno = <<<SQL
UNION
  SELECT
    hep.id,
    'S' AS CONCILIADO,
    hep.valor,
    hep.credito_id AS idNota,
    p.TR AS TR,
    CONCAT('ESTORNO BANCÁRIO PARA ', IF(f.razaoSocial != '', f.razaoSocial, f.nome)) AS codigo,
    DATE_FORMAT(hep.data_conciliada, '%d/%m/%Y') AS data_pg,
    '1' AS tipo_nota,
    f.razaoSocial as fornecedor,
    n.codFornecedor AS fornecedor_id,
    hep.data_conciliada AS data_pg_us
  FROM
    estorno_parcela hep
    INNER JOIN parcelas p ON (p.id = hep.parcela_id)
    INNER JOIN notas n ON (n.idNotas = p.idNota)
    LEFT JOIN fornecedores f ON (n.codFornecedor = f.idFornecedores)
  WHERE
    hep.origem = '{$id_conta}'
SQL;
        }

        $sql = "SELECT
                    p.id,
                    p.CONCILIADO,
                    p.VALOR_PAGO,
                    p.idNota,
                    p.TR,
                    p.NUM_DOCUMENTO,
                    DATE_FORMAT({$cond_data_parcela},'%d/%m/%Y') as data_pg,
                    n.tipo as tipo_nota,
                    f.razaoSocial as fornecedor,
                    n.codFornecedor as fornecedor_id,
                    DATE_FORMAT({$cond_data_parcela},'%Y-%m-%d') as data_pg_us
	       FROM
                    parcelas as p INNER JOIN
                    contas_bancarias AS o ON (p.origem = o.id) INNER JOIN
                    notas as n on (p.idNota = n.idNotas) INNER JOIN
                    fornecedores as f on (n.codFornecedor = f.idFornecedores)
	       WHERE
			        p.status = 'Processada' AND
			        p.origem = {$id_conta} AND
                    (p.nota_id_fatura_aglutinacao is null or p.nota_id_fatura_aglutinacao = 0) AND
			        {$cond_data_parcela} <= '{$fim1}' AND
			        {$cond_data_parcela} >= '{$data_saldo_inicial_us}' AND
			        {$cond_conciliado}
UNION
SELECT
		ID,
		(case when `CONTA_ORIGEM`={$id_conta}  then CONCILIADO_ORIGEM when `CONTA_DESTINO`={$id_conta}  then  CONCILIADO_DESTINO end) as CONCILIADO,
		( case when `CONTA_ORIGEM`={$id_conta}  then `VALOR` when `CONTA_DESTINO`={$id_conta} then  VALOR end) as VALOR_PAGO,
		0 as idNota,
		'0' as TR,
		'Transferência' as NUM_DOCUMENTO,
		(CASE
                        WHEN
                        `CONTA_ORIGEM`={$id_conta}   AND  CONCILIADO_ORIGEM ='S'
                        THEN
                        DATE_FORMAT(`DATA_CONCILIADO_ORIGEM`,'%d/%m/%Y')
                        WHEN
                        `CONTA_DESTINO`={$id_conta}   AND  CONCILIADO_DESTINO ='S'
                        THEN  DATE_FORMAT(`DATA_CONCILIADO_DESTINO`,'%d/%m/%Y')
                        ELSE
                        DATE_FORMAT(`DATA_TRANSFERENCIA`,'%d/%m/%Y')

                        end) as data_pg,

		( case when `CONTA_ORIGEM`={$id_conta} then '1' when `CONTA_DESTINO`={$id_conta}  then '0' end) as tipo_nota,
		( case when `CONTA_ORIGEM`={$id_conta}  THEN (
												SELECT
														concat(o.SIGLA_BANCO,'-',c.AGENCIA,'-',c.NUM_CONTA,'-',e.SIGLA_EMPRESA)
												FROM
														contas_bancarias as c INNER JOIN
														origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id) INNER JOIN
														empresas as e ON (c.UR = e.id)
												WHERE
														c.ID= CONTA_DESTINO
											)

			  WHEN `CONTA_DESTINO`={$id_conta}  THEN (
											 SELECT
													concat(o.SIGLA_BANCO,'-',c.AGENCIA,'-',c.NUM_CONTA,'-',e.SIGLA_EMPRESA)
											 FROM
													contas_bancarias as c INNER JOIN
													origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id) INNER JOIN
													empresas as e ON (c.UR = e.id)
											 WHERE
													c.ID = CONTA_ORIGEM
											)
											 END
		) as fornecedor,
		( case when `CONTA_ORIGEM`={$id_conta} then CONTA_DESTINO when `CONTA_DESTINO`={$id_conta}  then  CONTA_ORIGEM end) as fornecedor_id,
                    (CASE
                        WHEN
                        `CONTA_ORIGEM`={$id_conta}   AND  CONCILIADO_ORIGEM ='S'
                        THEN
                        DATE_FORMAT(`DATA_CONCILIADO_ORIGEM`,'%Y-%m-%d')
                        WHEN
                        `CONTA_DESTINO`={$id_conta}   AND  CONCILIADO_DESTINO ='S'
                        THEN  DATE_FORMAT(`DATA_CONCILIADO_DESTINO`,'%Y-%m-%d')
                        ELSE
                        DATE_FORMAT(`DATA_TRANSFERENCIA`,'%Y-%m-%d')
                        end) as data_pg_us



FROM
		`transferencia_bancaria`
WHERE
		(`CONTA_DESTINO`={$id_conta} or `CONTA_ORIGEM`={$id_conta}) and
		            IF  (  `CONTA_ORIGEM`={$id_conta}   AND  CONCILIADO_ORIGEM ='S',
                                    DATE_FORMAT(`DATA_CONCILIADO_ORIGEM`,'%Y-%m-%d') ,
                                     IF (
                                        `CONTA_DESTINO`={$id_conta}   AND  CONCILIADO_DESTINO ='S',
                                         DATE_FORMAT(`DATA_CONCILIADO_DESTINO`,'%Y-%m-%d'),
                                         DATE_FORMAT(`DATA_TRANSFERENCIA`,'%Y-%m-%d')
                                    ))
                            <= '{$fim1}' AND

                              IF  (  `CONTA_ORIGEM`={$id_conta}   AND  CONCILIADO_ORIGEM ='S',
                                    DATE_FORMAT(`DATA_CONCILIADO_ORIGEM`,'%Y-%m-%d') ,
                                     IF (
                                        `CONTA_DESTINO`={$id_conta}   AND  CONCILIADO_DESTINO ='S',
                                         DATE_FORMAT(`DATA_CONCILIADO_DESTINO`,'%Y-%m-%d'),
                                         DATE_FORMAT(`DATA_TRANSFERENCIA`,'%Y-%m-%d')
                                    ))>= '{$data_saldo_inicial_us}' AND

                     CANCELADA_POR is NULL AND
                     (CASE
                        WHEN `CONTA_ORIGEM`={$id_conta} THEN CONCILIADO_ORIGEM = '{$conciliado}'
                        WHEN `CONTA_DESTINO`={$id_conta}  THEN  CONCILIADO_DESTINO = '{$conciliado}'
                      END)
            {$unionTarifas}
            {$unionEstorno}
                    UNION
		SELECT
			nb.id,
			 nb.conciliado as CONCILIADO,
			nb.valor ,
			0 AS idNota,
			0 AS TR,
			nb.numero_documento as NUM_DOCUMENTO,
			DATE_FORMAT(nb.data_pagamento, '%d/%m/%Y') AS data_pg,
			nb.tipo_movimentacao AS tipo_nota,
			'Devolução Adiantamento' as fornecedor,
			0 AS fornecedor_id,			
			{$cond_data_baixa}  AS  data_pg_us
			
FROM
	nota_baixas as nb
				
WHERE
			nb.conta_bancaria_id = {$id_conta}
			AND
			nb.canceled_by is null
			AND
			$cond_conciliado_baixa AND 
            {$cond_data_baixa} <= '{$fim1}' AND
            {$cond_data_baixa} >= '{$data_saldo_inicial_us}' 

		ORDER BY
			data_pg_us";


        $result = mysql_query($sql);
        $numero_linhas = mysql_num_rows($result);
        /*if($numero_linhas == 0){
            $var .= "<br><br><br>";
            $var .= "</br><p><center><b>Nenhum item encontrado</b></center></p>";
        }else{*/
        $var .= "<br><br><br>";
        $label_data = $conciliado == 'N' ? "Data do Processamento" : "Data Compensado";

        $var .= "<tr>
                        <td widtd='7%'>".utf8_decode($label_data)."</td>
                        <td>TR</td>
                        <td widtd='50%'>Fornecedor</td>
                        <td>N&deg; Documento</td>
                        <td>Entrada </td>
                        <td>Saida </td>
                        <td>Saldo</td>
                    </tr>";
        if($data_saldo_inicial_us >= $inicio1){

            $var .= "<tr>
                           <td>{$data_saldo_inicial_pt}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Saldo Inicial</td><td></td>
                            <td $cor>".number_format($saldo_inicial,2,',','.')."</td>			        
                            </tr>";
        }
        //}
        $cont_saldo_dia_anterior=0;
        $numeroLinhas = mysql_num_rows($result);
        $aux = 0;
        $entradas = 0.00;
        $saidas = 0.00;
        $corEnt   = "style='font-size:12px; color:blue;'";
        $corSaida   = "style='font-size:12px; color:red;'";
        while($row = mysql_fetch_array($result)){
            ////tipo da parcela 1-saida ou 0-entrada
            $cor = "";
            $cor_saldo = "";
            $entrada = 0.00;
            $saida = 0.00;
            if($row['tipo_nota'] == 0){
                $total += $row['VALOR_PAGO'];
                $entradas += $row['VALOR_PAGO'];
            }else{
                $total -= $row['VALOR_PAGO'];
                $saidas -= $row['VALOR_PAGO'];
            }
            $aux++;

            if($row['data_pg_us'] >= $inicio1 && $row['data_pg_us'] <= $fim1){
                if($cont_saldo_dia_anterior == 0){
                    $saldo_dia_anterior = $auxMovimentacaoAnterior == 0 ? $saldo_inicial : $saldo_dia_anterior;
                    $var .= "<tr>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td><b>SALDO DO DIA ANTERIOR</b></td>
                                 <td>".number_format($saldo_dia_anterior,2,',','.')."</td>
                                 </tr>";

                }

                if($row['tipo_nota'] == 0){
                    $entrada = $row['VALOR_PAGO'];
                }else{
                    $saida = $row['VALOR_PAGO'];
                }

                $cor_saldo = "style='font-size:12px; color:blue;'";
                if($total < 0) {
                    $cor_saldo = "style='font-size:12px; color:red;'";
                }
                $var .= "<tr>
                                 <td>{$row['data_pg']}</td>
                                 <td>{$row['idNota']}-{$row['TR']}</td>
                                 <td>".utf8_decode($row['fornecedor'])."</td>
                                 <td>{$row['NUM_DOCUMENTO']}</td>
                                 <td $corEnt>R$ ".number_format($entrada,2,',','.')."</td>
                                 <td $corSaida>R$ ".number_format($saida,2,',','.')."</td>
                                 <td>".number_format($total,2,',','.')."</td>
                                 </tr>";

                $cont_saldo_dia_anterior ++;
            }else{
                $saldo_dia_anterior = $total;
                $auxMovimentacaoAnterior ++;
                if( $numeroLinhas == $aux){
                    $saldo_dia_anterior = $auxMovimentacaoAnterior == 0 ? $saldo_inicial :$saldo_dia_anterior;
                    echo "<tr>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td><b>SALDO DO DIA ANTERIOR</b></td>
                                       <td {$cor_saldo}>R$ ".number_format($saldo_dia_anterior,2,',','.')."</td>
                                       </tr>";

                }

            }
        }
        $cor_saldo = "style='font-size:12px; color:blue;'";
        if($total < 0) {
            $cor_saldo = "style='font-size:12px; color:red;'";
        }
        $var .= "<tr>
                     <td colspan='7' align='right'><b>SALDO FINAL</b></td>
                  </tr>
                  <tr class='dados' valor_pago='{$row['VALOR_PAGO']}' id_parcela={$row['id']} conciliado='".$row['CONCILIADO']."' tipo='".$row['tipo_nota']."' tr='".$row['idNota']."-".$row['TR']."'  data='".$row['data_pg']."' fornecedor = '".$row['fornecedor_id']."'  >
                     <td colspan='5' {$corEnt} align='right'>R$ ".number_format($entradas,2,',','.')."</td>
                     <td {$corSaida} align='right'>R$ ".number_format($saidas,2,',','.')."</td>
                     <td {$cor_saldo}>R$ ".number_format($total,2,',','.')."</td>
          </tr>";
        $var .= "</table>";

        echo $var;
    }


}
$p = new exportar_excel_extrato();
$p->detalhe_extrato($_GET['conta'],$_GET['conciliado'],$_GET['inicio'],$_GET['fim']);
