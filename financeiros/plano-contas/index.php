<?php

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

use \App\Controllers\PlanoContas as Controller,
  \App\Core\Url;



if (isset($_GET['action']) && !empty($_GET['action'])) {

  $action = $_GET['action'];

  switch ($action) {
		#----------------------------- IPCF -----------------------------
    case "ipcf" :
      $contas = Controller::listarArvore();
      require 'templates/ipcf-listar.phtml';
      break;
    case "ipcf-editar" :
      $data = (isset($_GET['level']) && isset($_GET['match']) ? Controller::preencherCampos($_GET) : null);
      require 'templates/ipcf-form.phtml';
      break;
    case "ipcf-form" :
      require 'templates/ipcf-form.phtml';
      break;
    case "ipcf-salvar" :
      if(isset($_POST) && !empty($_POST))
        $response = Controller::novoItem($_POST['ipcf'], $_POST['retencoes']);
      require 'templates/ipcf-form.phtml';
      break;
    case "ipcf-atualizar" :
      $response = Controller::atualizarItem($_POST);
      header('Location: ' . Url::get('ipcf') .
        (isset($response['msg']) && array_key_exists('success', $response['msg']) ?
          "&msg=" . base64_encode($response['msg']['success']) . "&type=s" :
          "&msg=" . base64_encode($response['msg']['failed'])  . "&type=f"
        )
      );
      break;
    case "ipcf-excluir" :
      $response = Controller::excluirItem($_GET);
      header('Location: ' . Url::get('ipcf').
        (isset($response['msg']) && array_key_exists('success', $response['msg']) ?
          "&msg=" . base64_encode($response['msg']['success']) . "&type=s" :
          "&msg=" . base64_encode($response['msg']['failed'])  . "&type=f"
        )
      );
      break;
    case "ipcf-getjson" :
      $response = Controller::autoComplete($_GET);
      echo $response;
      break;

		#----------------------------- IPCC -----------------------------
		case "ipcc" :
			$contas = Controller::listarArvore('Contábil');
			require 'templates/ipcc-listar.phtml';
			break;
		case "ipcc-form" :
			require 'templates/ipcc-form.phtml';
			break;
		case "ipcc-salvar" :
			if(isset($_POST) && !empty($_POST))
				$response = Controller::novoItem($_POST['ipcc'], [], 'Contábil');
			require 'templates/ipcc-form.phtml';
			break;
		case "ipcc-editar" :
			$data = (isset($_GET['level']) && isset($_GET['match']) ? Controller::preencherCampos($_GET, 'Contábil') : null);
			require 'templates/ipcc-form.phtml';
			break;
		case "ipcc-atualizar" :
			$response = Controller::atualizarItem($_POST, 'Contábil');
			header('Location: ' . Url::get('ipcc') .
				(isset($response['msg']) && array_key_exists('success', $response['msg']) ?
					"&msg=" . base64_encode($response['msg']['success']) . "&type=s" :
					"&msg=" . base64_encode($response['msg']['failed'])  . "&type=f"
				)
			);
			break;
		case "ipcc-excluir" :
			$response = Controller::excluirItem($_GET, 'Contábil');
			header('Location: ' . Url::get('ipcc').
				(isset($response['msg']) && array_key_exists('success', $response['msg']) ?
					"&msg=" . base64_encode($response['msg']['success']) . "&type=s" :
					"&msg=" . base64_encode($response['msg']['failed'])  . "&type=f"
				)
			);
			break;
		case "ipcc-getjson" :
			$response = Controller::autoComplete($_GET, 'Contábil');
			echo $response;
			break;
    case "ipcc-importar" :
      var_dump(Controller::importarIPCC());
      break;
  }
}


