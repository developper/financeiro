<?php
$id = session_id();
if(empty($id))
  session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
    header("Content-type: application/x-msexce");
    header("Content-type: application/force-download;");
    header("Content-Disposition: attachment; filename=demonstrativo.xls");
    header("Pragma: no-cache");
//- See more at: http://www.botecodigital.info/php/exportando-dados-para-excel-com-php/#sthash.MARLYRSE.dpuf
class imprimir_demonstrativo_financeiro{
public function demonstrativo_financeiro($btipo,$status,$fim,$inicio,$todos_ur,$empresa,$comparar_porcentagem){
//extract($post,EXTR_OVERWRITE);
    $fim1 = implode("/",array_reverse(explode("-",$fim)));
    $inicio1 = implode("/",array_reverse(explode("-",$inicio)));

//
//]echo "<form>";
  echo "<h1><a><img src='../utils/logo2.jpg' width='200' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> Demonstrativo Financeiro  </h1>";
  echo "<table width='100%'><tr bgcolor='#EEEEEE' ><td><center>Per&iacute;do: {$inicio1} at&eacute; {$fim1} &nbsp;&nbsp; Status: {$status}</center></td></tr></table>";
  $cont_status = 0;
  extract($post,EXTR_OVERWRITE);
//////////////verifica se é para comparar o percentual que foi atingido com o percentual cadastrado dos IPCGs para a UR.
if($comparar_porcentagem == 1 && $todos_ur != 1){
   $sql="SELECT 
             `COD_N2`,
             `COD_N3`,
             `PORCENTO`,
             (select
                 sum(`PORCENTO`)
             FROM 
                 `planocontas_porcento_ur` as p1
             WHERE 
                  UR={$empresa} and 
                  STATUS=1 and 
                  p1.`COD_N2` =p.`COD_N2` ) as percent2
            
          FROM 
             `planocontas_porcento_ur` as p 
          WHERE 
            UR={$empresa} and 
            STATUS=1";
            
            /////////Monta um array com os percentuais do IPCG# E IPCG2 e seus codigos para serem comparados com o resultado da busca do demonstrativo.
            $result = mysql_query($sql);
    $itens = '';

    while ($row = mysql_fetch_array($result)) {
        $itens[$row['COD_N3']] = array("porcento_ipcg2" =>number_format($row['percent2'],2,',','.'), "porcento_ipcg3" => number_format($row['PORCENTO'],2,',','.'),"ipcg2"=>$row['COD_N2']);
    }
   
   $comparar_porcentagem = 1; 
   
}

///////////////// Verificar por qual UR deve ser feito a consulta
if($todos_ur == 1){
    $condicao_ur = "";
    $todos_ur=1;
}else{
    $condicao_ur1 = " and R1.CENTRO_RESULTADO = {$empresa}";
     $condicao_ur2 = " and R2.CENTRO_RESULTADO = {$empresa}";
    $condicao_ur=" and R.CENTRO_RESULTADO = {$empresa}";
    $todos_ur=0;
}
/////Bloco que monta a condi��o do status para a consulta sql.
 if($status == 'Processada'){
		$condicao_status = " and P.status = '{$status}'";
	    $condicao_status1 = " and P1.status = '{$status}'";
	    $condicao_status2 = " and P2.status = '{$status}'";
	    $tipo_valor  = "sum(P.VALOR_PAGO* ( R.PORCENTAGEM /100 )) as VALOR,";
	    $tipo_valor1 =" sum(P1.VALOR_PAGO* ( R1.PORCENTAGEM /100 ))";
	    $tipo_valor2 =" sum(P2.VALOR_PAGO* ( R2.PORCENTAGEM /100 ))";
	}else {
		$condicao_status = " and P.status = '{$status}'";
		$condicao_status1 = " and P1.status = '{$status}'";
		$condicao_status2 = " and P2.status = '{$status}'";
		$tipo_valor= "sum(P.valor* ( R.PORCENTAGEM /100 )) as VALOR,";
		$tipo_valor1="sum(P1.valor* ( R1.PORCENTAGEM /100 ))";
		$tipo_valor2="sum(P2.valor* ( R2.PORCENTAGEM /100 ))";
 	}





////btipo recebe tipo da nota:1 desembolso ou 0 recebimento.
       if ($btipo == 1){
    		$cond_tipo=" and PL.N1 like 'D'";
    	}
    	if ($btipo == 0){
    		$cond_tipo =" and PL.N1 like 'R'";
    	}
    	if ($btipo == 't'){
    		$cond_tipo = " ";
    	}
 if($status == 'Processada'){
		$sql="  
                    SET NAMES 'iso-8859-1';
                    SELECT 
		      P.idNota,
		      R.IPCG3,
		      PL.N4 as IPCG4,  
		      PL.N1 as tipo,	 
			  substr( COD_N3, 1, 3 ) as cod_n3,
			  COD_N4,
			  COD_N2,
			  COD_N3,
		      P.status as status_parcela,	 
		      {$tipo_valor}
			  (SELECT 
				{$tipo_valor1}
			FROM 
				parcelas as P1 inner join 
				notas as N1 ON (P1.idNota = N1.idNotas) inner join 
				rateio as R1 ON (N1.idNotas = R1.NOTA_ID) inner join 
				plano_contas as PL1 ON (PL1.ID = R1.IPCG4) 
			WHERE 
				P1.DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}'
				{$condicao_status1} 
				{$cond_tipo} and 
				PL1.COD_N2 = PL.COD_N2 
                                {$condicao_ur1}
			GROUP BY 
				PL1.COD_N2
			
			) as total,
			(SELECT 
					{$tipo_valor2}
				FROM 
					parcelas as P2 inner join 
					notas as N2 ON (P2.idNota = N2.idNotas) inner join 
					rateio as R2 ON (N2.idNotas = R2.NOTA_ID) inner join 
					plano_contas as PL2 ON (PL2.ID = R2.IPCG4) 
				WHERE 
					P2.DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}'
					{$condicao_status2} 
					{$cond_tipo} and 
					PL2.COD_N3 = PL.COD_N3 
                                        {$condicao_ur2}
				GROUP BY 
					PL.COD_N3
				) as total_n3,
			
			PL.N2,
                        (SELECT 
				{$tipo_valor1}
			FROM 
				parcelas as P1 inner join 
				notas as N1 ON (P1.idNota = N1.idNotas) inner join 
				rateio as R1 ON (N1.idNotas = R1.NOTA_ID) inner join 
				plano_contas as PL1 ON (PL1.ID = R1.IPCG4) 
			WHERE 
				P1.DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}'
				{$condicao_status1} 
				{$cond_tipo} and 
				PL1.N1 = 'D'
                                {$condicao_ur1}
			
			
			) as total_desembolso,
                        (SELECT 
				{$tipo_valor2}
			FROM 
				parcelas as P2 inner join 
				notas as N2 ON (P2.idNota = N2.idNotas) inner join 
				rateio as R2 ON (N2.idNotas = R2.NOTA_ID) inner join 
				plano_contas as PL2 ON (PL2.ID = R2.IPCG4) 
			WHERE 
				P2.DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}'
				{$condicao_status2} 
				{$cond_tipo} and 
				PL2.N1 = 'R'
                                {$condicao_ur2}
			
			
			) as total_recebimento
			
		FROM 
		      parcelas as P inner join       
		      notas as N ON (P.idNota = N.idNotas) inner join
		      rateio as R ON (N.idNotas = R.NOTA_ID) inner join 
		      plano_contas as PL ON (PL.ID = R.IPCG4)
		WHERE 
                      P.DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}' {$condicao_status} {$cond_tipo} {$condicao_ur}
		GROUP BY 
		      PL.COD_N4
		ORDER BY 
			PL.N1 DESC,
			PL.COD_N4,
			PL.N2 ASC,
			R.IPCG3 ASC";
		 }else{
    	
					    	$sql=" 
                                                   
                                            SELECT 
					      P.idNota,
					      R.IPCG3,
					      PL.N4 as IPCG4,  
					      PL.N1 as tipo,	 
						  substr( COD_N3, 1, 3 ) as cod_n3,
						  COD_N4,
						  COD_N2,
						  COD_N3,
					      P.status as status_parcela,	 
					      {$tipo_valor}
						  (SELECT 
							{$tipo_valor1}
						FROM 
							parcelas as P1 inner join 
							notas as N1 ON (P1.idNota = N1.idNotas) inner join 
							rateio as R1 ON (N1.idNotas = R1.NOTA_ID) inner join 
							plano_contas as PL1 ON (PL1.ID = R1.IPCG4) 
						WHERE 
							P1.vencimento BETWEEN '{$inicio}' and '{$fim}'
							{$condicao_status1} 
							{$cond_tipo} and 
							PL1.COD_N2 = PL.COD_N2 
                                                        {$condicao_ur1}
						GROUP BY 
							PL1.COD_N2
						
						) as total,
						(SELECT 
								{$tipo_valor2}
							FROM 
								parcelas as P2 inner join 
								notas as N2 ON (P2.idNota = N2.idNotas) inner join 
								rateio as R2 ON (N2.idNotas = R2.NOTA_ID) inner join 
								plano_contas as PL2 ON (PL2.ID = R2.IPCG4) 
							WHERE 
								P2.vencimento BETWEEN '{$inicio}' and '{$fim}'
								{$condicao_status2} 
								{$cond_tipo} and 
								PL2.COD_N3 = PL.COD_N3 
                                                                {$condicao_ur2}
							GROUP BY 
								PL.COD_N3
							) as total_n3,
						
						PL.N2,
                                                (SELECT 
							{$tipo_valor1}
						FROM 
							parcelas as P1 inner join 
							notas as N1 ON (P1.idNota = N1.idNotas) inner join 
							rateio as R1 ON (N1.idNotas = R1.NOTA_ID) inner join 
							plano_contas as PL1 ON (PL1.ID = R1.IPCG4) 
						WHERE 
							P1.vencimento BETWEEN '{$inicio}' and '{$fim}'
							{$condicao_status1} 
							{$cond_tipo} and 
							PL1.N1 = 'D'
                                                        {$condicao_ur1}
						
						
						) as total_desembolso,
                                                (SELECT 
							{$tipo_valor1}
						FROM 
							parcelas as P1 inner join 
							notas as N1 ON (P1.idNota = N1.idNotas) inner join 
							rateio as R1 ON (N1.idNotas = R1.NOTA_ID) inner join 
							plano_contas as PL1 ON (PL1.ID = R1.IPCG4) 
						WHERE 
							P1.vencimento BETWEEN '{$inicio}' and '{$fim}'
							{$condicao_status1} 
							{$cond_tipo} and 
							PL1.N1 = 'R'
                                                        {$condicao_ur1}
						
						
						) as total_recebimento
						
					FROM 
					      parcelas as P inner join       
					      notas as N ON (P.idNota = N.idNotas) inner join
					      rateio as R ON (N.idNotas = R.NOTA_ID) inner join 
					      plano_contas as PL ON (PL.ID = R.IPCG4)
					WHERE 
                                              P.vencimento BETWEEN '{$inicio}' and '{$fim}' {$condicao_status} {$cond_tipo} {$condicao_ur}
					GROUP BY 
					      PL.COD_N4
					ORDER BY 
						PL.N1 DESC,
						PL.COD_N4,
						PL.N2 ASC,
						R.IPCG3 ASC";
    	} 
	
	  $result = mysql_query($sql);
	  $aux = '';	 
	  $total_geral   = 0;
	  $total_geral_r =0;
	  $total_geral_d =0;
	  $total_por_tipo = 0;
	  $cont =1;
	   $aux_n2 = '';
	   $aux_tipo = '';
	 
	  $num_rows= mysql_num_rows($result);
	   $cont2 =$num_rows;
	 $cor = "bgcolor='#eee'";
	
	
while($row = mysql_fetch_array($result)){
    if($comparar_porcentagem == 1 && $todos_ur == 0){
        
    if (array_key_exists($row['COD_N3'], $itens)) {
               $porcento_ipcg2   = "<th>".$itens[$row['COD_N3']]['porcento_ipcg2']."%</th>";
                $porcento_ipcg3  = "<td>".$itens[$row['COD_N3']]['porcento_ipcg3']."%</td>";
                
            }else{
                $porcento_ipcg2 = "<th>0,00%</th>";
                $porcento_ipcg3 ="<td>0,00%</td>";
            }
}
   $cod_n3 = implode("",(explode(".",$row['cod_n3'])));
   if($cont2--%2 == 0){	
		$class = "odd";
	}else{
		$class = "";
	}
	
	if($row['tipo'] == 'R'){
		 $total_geral_r += $row['VALOR'];
		
		}else{
		$total_geral_d += $row['VALOR'];
		}
		
	     $total_geral = $total_geral_r - $total_geral_d ;
	     $total_por_tipo += $row['VALOR']; 

	if($row['tipo'] !=  $aux_tipo){
	 $aux_tipo=$row['tipo'] ;
	 if($aux_tipo == 'R'){
			echo "<h1><center>RECEBIMENTO</center></h1><br><table width='100%' class='mytable'>";
		}else{
		    if($cont>1)
				echo "<tr><td width='50%'><b>TOTAL RECEBIMENTO:</b></td><td><b> R$ ".number_format($total_geral_r,2,',','.')."</b></td></tr>
                      </table><h1><center>DESEMBOLSO</center></h1><br><table width='100%' class='mytable'>";
			else
				echo "<h1><center>DESEMBOLSO</center></h1><br><table width='100%' class='mytable'>";
		 }
		 
	//echo "<thead  aux='0' ><tr><th  colspan='2'><span style:'text-align:center;'><b>{$tipo}</b></span></th></tr></thead>";
	
	}
	
	if($row['tipo'] =='D'){
	if($row['N2'] !=  $aux_n2){
	 $aux_n2=$row['N2'] ;
         $porcento_n2 = ($row['total']/$row['total_desembolso'])*100;
	echo "<thead  n3='{$classe_item}' aux='0' ><tr><th width='50%' ><b><span >{$row['COD_N2']}</span> ".utf8_decode($row['N2'])." </b></th><th><b>Total: R$ ".number_format($row['total'],2,',','.')."</b></th><th>".number_format($porcento_n2,2,',','.')." %</th>{$porcento_ipcg2}</tr></thead>";
	
	}
    if($row['IPCG3'] !=  $aux){
	  $classe_item = "G".$cont;
		$aux = $row['IPCG3'];
		$porcento_n3 = ($row['total_n3']/$row['total_desembolso'])*100;
	    
		echo "<tr class='icon' {$cor} n3='{$classe_item}' aux='0' ><td width='50%'><b><span >{$row['COD_N3']} </span>".utf8_decode($row['IPCG3'])."</b></td><td><b>Total: R$ ".number_format($row['total_n3'],2,',','.')."</b></td><td>".number_format($porcento_n3,2,',','.')." %</td>{$porcento_ipcg3}</tr>";
	 }
	 
         
		echo "<tr  class='$classe_item $class' aux1='0' ><td style='padding-left:20px;' width='50%'>{$row[COD_N4]} - ".utf8_decode($row['IPCG4'])."</td><td> R$ ".number_format($row['VALOR'],2,',','.')."</td></tr>";
		
}else {
    if($row['N2'] !=  $aux_n2){
	 $aux_n2=$row['N2'] ;
         $porcento_n2 = ($row['total']/$row['total_recebimento'])*100;
	echo "<thead  n3='{$classe_item}' aux='0' ><tr><th width='50%' ><b><span >{$row['COD_N2']}</span>".utf8_decode($row['N2'])." </b></th><th><b>Total: R$ ".number_format($row['total'],2,',','.')."</b></th><th>".number_format($porcento_n2,2,',','.')." %</th>{$porcento_ipcg2}</tr></thead>";
	
	}
    if($row['IPCG3'] !=  $aux){
	  $classe_item = "G".$cont;
		$aux = $row['IPCG3'];
		$porcento_n3 = ($row['total_n3']/$row['total_recebimento'])*100;
	    
                echo "<tr class='icon' {$cor} n3='{$classe_item}' aux='0' ><td width='50%'><b><span >{$row['COD_N3']} </span>".utf8_decode($row['IPCG3'])."</b></td><td><b>Total: R$ ".number_format($row['total_n3'],2,',','.')."</b></td><td>".number_format($porcento_n3,2,',','.')." %</td>{$porcento_ipcg3}</tr>";
	 }
	 
	 
                echo "<tr  class='$classe_item $class' aux1='0'  ><td style='padding-left:20px;' width='50%'>{$row['COD_N4']} - ".utf8_decode($row['IPCG4'])."</td><td> R$ ".number_format($row['VALOR'],2,',','.')."</td></tr>";
		

}	
		 
	 $cont++;
	 
	 
	 
	 
	
}
   

 ////exibir o total se o btipo = t
 if($btipo == 't' && $total_geral_d > 0){
 
 
  echo "<tr><td width='50%'><b>TOTAL DESEMBOLSO:</b></td><td><b> R$ ".number_format($total_geral_d,2,',','.')."</b></td></tr>";
 
 
 }
	
	echo "</table></br></br>";
	echo "<table class='mytable' width='100%'>";
          echo "<tr><td width='50%'><b>TOTAL GERAL:</b></td><td><b> R$ ".number_format($total_geral,2,',','.')."</b></td></tr>";
	echo "</table></br>";
        

/*$paginas []= $header.$html.= "</form>";
	
	
	//print_r($paginas);
	//print_r($paginas);
	
	$mpdf=new mPDF('pt','A4',9);
	$mpdf->SetHeader('pÃ¡gina {PAGENO} de {nbpg}');
	$ano = date("Y");
	$mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
	$mpdf->WriteHTML("<html><body>");
	$flag = false;
	foreach($paginas as $pag){
		if($flag) $mpdf->WriteHTML("<formfeed>");
		$mpdf->WriteHTML($pag);
		$flag = true;
	}
	$mpdf->WriteHTML("</body></html>");
	$mpdf->Output('demonstrativo_financeiro.pdf','I');
	exit;*/



}

}
$p = new imprimir_demonstrativo_financeiro();
$p->demonstrativo_financeiro($_GET['btipo'],$_GET['status'],$_GET['fim'],$_GET['inicio'],$_GET['todos_ur'],$_GET['empresa'],$_GET['comparar_porcentagem']);


?>