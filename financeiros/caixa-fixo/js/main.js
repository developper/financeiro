$(function($) {
    $('.chosen-select').chosen();

    $(".mes-ano").mask('99/9999');

    $(".mes-ano").blur(function () {
        var inputValue = $(this).val();
        if(inputValue != '') {
            var momentInput = moment(inputValue, 'MM/YYYY');
            if(!momentInput.isValid()) {
                alert('Informe uma competência válida!');
                $(this).val('').focus()
                $(this).focus()
            }
            return true;
        }
    });

    $('#pesquisar-caixa-fixo').click(function () {
        if(
            $("#tr").val() == '' &&
            ($("#competencia-inicio").val() == '' && $("#competencia-fim").val() == '') &&
            ($("#inicioPagamento").val() == '' && $("#fimPagamento").val() == '') &&
            ($("#inicio").val() == '' && $("#fim").val() == '')
        ){
            alert('Preencha pelo menos a TR, o período da competência, o período do pagamento ou o período da emissão!');
            return false;
        }
        return true;
    });

    $(".valor").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $("#data-vencimento, #data-vencimento-real, #data-emissao").live('change', function () {
        var dataEmissao = new Date($("#data-emissao").val().split("/").reverse().join("-")+' 01:00:00');
        var vencimento = new Date($("#data-vencimento").val().split("/").reverse().join("-")+' 01:00:00');
        var vencimentoReal = new Date($("#data-vencimento-real").val().split("/").reverse().join("-")+' 01:00:00');
        if(vencimento < dataEmissao || vencimentoReal < dataEmissao) {

            vencimento < dataEmissao ? $("#data-vencimento").val("") : '';
            vencimentoReal < dataEmissao ? $("#data-vencimento-real").va(''): '';

            alert("Datas de vencimentos não podem ser menores que da emissão");
            return false;
        }
        vencimentoReal = validar_vencimento_real(vencimento, vencimentoReal);
        vencimentoReal = moment(vencimentoReal).format('YYYY-MM-DD');
        $("#data-vencimento-real").val(vencimentoReal);

    });



    $('#salvar-conta').on('click', function(){

        if(validar_campos('div-form-conta')){
            var data =   $("#form-conta").serialize();

            $.ajax({
                url: "/financeiros/caixa-fixo/?action=salvar-conta",
                type: 'POST',
                data: data,
                success: function (response) {

                    response = JSON.parse(response);

                    if(response['tipo'] != 'erro') {
                        alert("TR "+response['msg']+" criada com sucesso.");
                        window.location.reload();
                    } else {
                        alert(response['msg']);
                        return false;
                    }
                }
            });
        }else{
            alert("Algum campo obrigatório não foi informado.");
        }
    });

    $('#editar-conta').on('click', function(){

        if(validar_campos('div-form-conta')){
            var data =   $("#form-conta").serialize();

            $.ajax({
                url: "/financeiros/caixa-fixo/?action=caixa-fixo-update-conta",
                type: 'POST',
                data: data,
                success: function (response) {

                    response = JSON.parse(response);
                  

                    if(response['tipo'] != 'erro') {
                        alert("TR "+response['msg']+" editada com sucesso.");
                        window.location.reload();
                    } else {
                        alert(response['msg']);
                        return false;
                    }
                }
            });
        }else{
            alert("Algum campo obrigatório não foi informado.");
        }
    });

    $(".caixa-fixo-cancelar-nota").on('click', function () {
        var $this = $(this);
        var nota_id = $this.attr('id-nota');
        $('#id-nota-cancelar').val(nota_id);
        $('#modal-cancelar-nota').modal("show");

    });

    $('#salvar-cancelamento-nota').on('click', function(){

        if(validar_campos('div-modal-cancelar-nota')){
            var data = $("#form-cancelar-nota").serialize();

            $.ajax({
                url: "/financeiros/caixa-fixo/?action=caixa-fixo-cancelar-conta",
                type: 'POST',
                data: data,
                success: function (response) {
                    response = JSON.parse(response);
                    if(response['tipo'] != 'erro') {
                        alert("Nota TR "+response['msg']+"cancelada com sucesso.");
                        window.location.reload();
                    } else {
                        alert(response['msg']);
                        return false;
                    }
                }
            });
        }else{
            alert("Algum campo obrigatório não foi informado.");
        }
    });

    $(".cancelar-compensacao").live('click', function () {
        
        var $this = $(this);
        var compensacaoId = $this.attr('id-compensacao');
        if(confirm('Deseja realmente cancelar compensação?')) {
            $.ajax({
                url: "/financeiros/compensacao/?action=cancelar-compensacao",
                type: 'POST',
                data: {id: compensacaoId},
                success: function (response) {
                    response = JSON.parse(response);                   
                    if(response['tipo'] != 'erro') {
                        alert(response['msg']);
                        window.location.reload();
                    } else {
                        alert(response['msg']);
                        
                    }
                    return false;
                }
            });
        }
    });

    $(".baixar-nota-antecipacao").on('click', function () {
        $('#modal-baixar-nota').find("input[name='valor']").val('');
        $('#modal-baixar-nota').find("textarea[name='descricao']").val('');
        $('#modal-baixar-nota').find("input[name='data_pagamento']").val('');
        $('#modal-baixar-nota').find("input[name='numero_documento']").val('');
        $('#modal-baixar-nota').find("select[name='origem']").val('').trigger("chosen:updated");;
        $('#modal-baixar-nota').find("select[name='forma_pagamento']").val('').trigger("chosen:updated");;

        var $this = $(this);
        var nota_id = $this.attr('id-nota-antecipacao');
        var parcela_id = $this.attr('id-parcela-antecipacao');
        var valor_nota = $this.attr('valor-nf');
        var valor_compensado = $this.attr('valor-compensado');
        var max_valor_baixar = parseFloat(replaceValorMonetarioFloat(valor_nota)) - parseFloat(replaceValorMonetarioFloat(valor_compensado));
        $('#nota-id-baixa').val(nota_id);
        $('#parcela-id-baixa').val(parcela_id);
        $('#valor-maximo-baixar').val(max_valor_baixar);
        $('#valor-conta').html(valor_nota);
        $('#valor-compensado').html(valor_compensado);
        $('#modal-baixar-nota').modal("show");
       
        

    });

    $('#salvar-baixa-conta').on('click', function(){

        if(validar_campos('div-dados-baixa')){
            var data =   $("#form-dados-baixa").serialize();
            var valorBaixar =  $('#modal-baixar-nota').find("input[name='valor']").val();
            var valorBaixar = parseFloat(replaceValorMonetarioFloat(valorBaixar));
            var valorMaxBaixar =  $('#modal-baixar-nota').find("input[name='valor_maximo_baixar']").val();
            var valorMoeda = float2moeda(valorMaxBaixar);
         
            if(Number(valorBaixar).toFixed(2) > Number(valorMaxBaixar).toFixed(2)){
                alert("Valor não pode ser maior que R$ "+valorMoeda+".");
                return false;
            }
                

            $.ajax({
                url: "/financeiros/contass/?action=salvar-baixa-conta",
                type: 'POST',
                data: data,
                success: function (response) {
                    response = JSON.parse(response);

                    if(response['tipo'] != 'erro') {
                        alert(response['msg']);
                        window.location.reload();
                    } else {
                        alert(response['msg']);
                        return false;
                    }
                }
            });
        }else{
            alert("Algum campo obrigatório não foi informado.");
        }
    });

    $(".cancelar-baixa").live('click', function () {
        
        var $this = $(this);
        var baixaId = $this.attr('id-baixa');
        if(confirm('Deseja realmente cancelar baixa?')) {
            $.ajax({
                url: "/financeiros/contass/?action=cancelar-baixa",
                type: 'POST',
                data: {id: baixaId},
                success: function (response) {
                    response = JSON.parse(response);                   
                    if(response['tipo'] != 'erro') {
                        alert(response['msg']);
                        window.location.reload();
                    } else {
                        alert(response['msg']);
                        
                    }
                    return false;
                }
            });
        }
    });

    $("#editar-competencia").live('click', function () {
        
        var $this = $(this);
        var notaId = $this.attr('id-nota');
        var competencia = $('#competencia').val();
        if(confirm('Deseja realmente mudar a competência?')) {
            $.ajax({
                url: "/financeiros/caixa-fixo/?action=editar-competencia",
                type: 'POST',
                data: {
                    notaId: notaId,
                    competencia: competencia
                },
                success: function (response) {
                    
                    response = JSON.parse(response);                   
                    if(response['tipo'] != 'erro') {
                        alert(response['msg']);
                    } else {
                        alert(response['msg']);
                        
                    }
                    return false;
                }
            });
        }
    });

    function float2moeda(num) {
        x = 0;
        if(num<0) {
            num = Math.abs(num);
            x = 1;
        }
        if(isNaN(num)) num = "0";
        cents = Math.floor((num*100+0.5)%100);
        num = Math.floor((num*100+0.5)/100).toString();
        if(cents < 10) cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
            num = num.substring(0,num.length-(4*i+3))+'.'
                +num.substring(num.length-(4*i+3));
        ret = num + ',' + cents;
        if (x == 1) ret = ' - ' + ret;return ret;
    }

    function replaceValorMonetarioFloat(valor){
    return valor.replace(/\./g,'').replace(/,/g,'.')
    }
});