<?php

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

 //ini_set('display_errors',1);

$routes = [
    'GET'  => [
		"/financeiros/caixa-fixo/?action=index-caixa-fixo" => '\App\Controllers\CaixaFixo::listar' ,  
        "/financeiros/caixa-fixo/?action=caixa-fixo-criar-conta" => '\App\Controllers\CaixaFixo::criarConta',
        "/financeiros/caixa-fixo/?action=caixa-fixo-visualizar-conta" => '\App\Controllers\CaixaFixo::visualizar',
        "/financeiros/caixa-fixo/?action=caixa-fixo-editar-conta" => '\App\Controllers\CaixaFixo::editar',    
    ],
    'POST' => [      
        '/financeiros/caixa-fixo/?action=salvar-conta' => '\App\Controllers\CaixaFixo::salvar',
        '/financeiros/caixa-fixo/?action=caixa-fixo-update-conta' => '\App\Controllers\CaixaFixo::update',
        '/financeiros/caixa-fixo/?action=pesquisar-caixa-fixo' => '\App\Controllers\CaixaFixo::pesquisarNotas',
        '/financeiros/caixa-fixo/?action=caixa-fixo-cancelar-conta' => '\App\Controllers\CaixaFixo::cancelarNota',
        "/financeiros/caixa-fixo/?action=salvar-baixa-caixa-fixo" => '\App\Controllers\CaixaFixo::salvarBaixaNota',
        "/financeiros/caixa-fixo/?action=cancelar-baixa" => '\App\Controllers\CaixaFixo::cancelarBaixa',
        "/financeiros/caixa-fixo/?action=editar-competencia" => '\App\Controllers\Contas::editarCompetencia'
    ]
];
$args = isset($_GET) && !empty($_GET) ? $_GET : null;
try {
    $app = new App\Application;
    $app->setRoutes($routes);
    $app->dispatch(METHOD, URI,$args);
} catch(App\BaseException $e) {
    echo $e->getMessage();
} catch(App\NotFoundException $e) {
    http_response_code(404);
    echo $e->getMessage();
}


