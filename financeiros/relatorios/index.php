<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';
//ini_set('display_errors', 1);

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
	"GET" => [
		'/financeiros/relatorios/?action=form-relatorio-retencoes' => '\App\Controllers\Financeiro::formRelatorioRetencoes',
		'/financeiros/relatorios/?action=excel-relatorio-retencoes' => '\App\Controllers\Financeiro::excelRelatorioRetencoes',
        '/financeiros/relatorios/?action=relatorio-simulacoes-realizadas' => '\App\Controllers\Financeiro::formRelatorioSimulacoes',
        '/financeiros/relatorios/?action=excel-relatorio-simulacoes-realizadas' => '\App\Controllers\Financeiro::excelRelatorioSimulacoes',
        '/financeiros/relatorios/?action=saldos-contas-bancarias' => '\App\Controllers\ContasBancarias::indexSaldosContasBancarias',
        '/financeiros/relatorios/?action=posicao-fornecedores' => '\App\Controllers\Fornecedores::indexRelatorioPosicaoFornecedores',
        '/financeiros/relatorios/?action=excel-posicao-fornecedores' => '\App\Controllers\Fornecedores::excelRelatorioPosicaoFornecedores',
		'/financeiros/relatorios/?action=notas-emitidas' => '\App\Controllers\RelatorioNotas::formNotasEmitidas',
		'/financeiros/relatorios/?action=pesquisar-notas-emitidas' => '\App\Controllers\RelatorioNotas::pesquisarNotasEmitidas',
		'/financeiros/relatorios/?action=excel-relatorio-notas-emitidas' => '\App\Controllers\RelatorioNotas::excelNotasEmitidas',
		'/financeiros/relatorios/?action=notas-com-processamento' => '\App\Controllers\RelatorioNotas::formNotasComProcessamento',
		'/financeiros/relatorios/?action=pesquisar-notas-com-processamento' => '\App\Controllers\RelatorioNotas::pesquisarNotasComProcessamento',
		'/financeiros/relatorios/?action=excel-relatorio-notas-com-processamento' => '\App\Controllers\RelatorioNotas::excelNotasProcessada',
		'/financeiros/relatorios/?action=relatorio-juros-desconto' => '\App\Controllers\RelatorioJurosDesconto::index',
		'/financeiros/relatorios/?action=excel-relatorio-juros-desconto' => '\App\Controllers\RelatorioJurosDesconto::excel',
		'/financeiros/relatorios/?action=relatorio-fluxo-caixa' => '\App\Controllers\RelatorioFluxoCaixa::index',
		'/financeiros/relatorios/?action=excel-relatorio-fluxo-caixa' => '\App\Controllers\RelatorioFluxoCaixa::excel',
		'/financeiros/relatorios/?action=relatorio-fluxo-caixa-completo' => '\App\Controllers\RelatorioFluxoCaixa::indexCompleto',
		'/financeiros/relatorios/?action=excel-relatorio-fluxo-caixa-completo' => '\App\Controllers\RelatorioFluxoCaixa::excelCompleto',

	],
	"POST" => [
        '/financeiros/relatorios/?action=form-relatorio-retencoes' => '\App\Controllers\Financeiro::relatorioRetencoes',
        '/financeiros/relatorios/?action=relatorio-simulacoes-realizadas' => '\App\Controllers\Financeiro::relatorioSimulacoes',
        '/financeiros/relatorios/?action=saldos-contas-bancarias' => '\App\Controllers\ContasBancarias::saldosContasBancarias',
        '/financeiros/relatorios/?action=salvar-saldos-contas-bancarias' => '\App\Controllers\ContasBancarias::salvarSaldosContasBancarias',		
		'/financeiros/relatorios/?action=posicao-fornecedores' => '\App\Controllers\Fornecedores::relatorioPosicaoFornecedores',
		'/financeiros/relatorios/?action=pesquisar-juros-desconto' => '\App\Controllers\RelatorioJurosDesconto::pesquisar',
		'/financeiros/relatorios/?action=pesquisar-relatorio-fluxo-caixa' => '\App\Controllers\RelatorioFluxoCaixa::pesquisar',
		'/financeiros/relatorios/?action=pesquisar-relatorio-fluxo-caixa-completo' => '\App\Controllers\RelatorioFluxoCaixa::pesquisarCompleto',

	]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}
