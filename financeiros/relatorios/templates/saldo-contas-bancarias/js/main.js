$(function($) {
    $('.chosen-select').chosen();

    $('#salvar-saldos-contas-bancarias').click(function () {
        var data = $('#form-saldo-contas-bancarias').serializeArray();
        if(confirm('Deseja realmente salvar os saldos atuais?')) {
            $.post(
                '/financeiros/relatorios/?action=salvar-saldos-contas-bancarias',
                data,
                function (response) {
                    alert('Saldos salvos com sucesso!');
                }
            );
        }
        return false;
    });
});