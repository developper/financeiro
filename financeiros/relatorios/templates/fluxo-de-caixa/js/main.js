$(function($) {
    $('.chosen-select').chosen({search_contains: true});

    $('.esconder').click(function () {
        var $this = $(this);
        var key = $this.attr('data-key');
        $(".fluxo-" + key).css('display', 'none');
        $('.esconder-' + key).css('display', 'none');
    })

    $('.mostrar-dados-entrada').click(function () {
        var $this = $(this);
        var key = $this.attr('data-key');
        $('.esconder-' + key).css('display', 'block');

        $('.fluxo-' + key).css('display', 'table-row');
        $(".entrada-" + key).removeClass('hidden');
        $(".entrada-" + key).addClass('show');
        $(".saida-" + key).removeClass('show');
        $(".saida-" + key).addClass('hidden');
    });

    $('.mostrar-dados-saida').on('click', function () {
        var $this = $(this);
        var key = $this.attr('data-key');
        $('.esconder-' + key).css('display', 'block');

        $('.fluxo-' + key).css('display', 'table-row');
        $(".saida-" + key).removeClass('hidden');
        $(".saida-" + key).addClass('show');
        $(".entrada-" + key).removeClass('show');
        $(".entrada-" + key).addClass('hidden');
    });
    //
});