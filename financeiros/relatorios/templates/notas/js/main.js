$(function($) {

    $(".mes-ano").mask('99/9999');

    $(".mes-ano").blur(function () {
        var inputValue = $(this).val();
        if(inputValue != '') {
            var momentInput = moment(inputValue, 'MM/YYYY');
            if(!momentInput.isValid()) {
                alert('Informe uma competência válida!');
                $(this).val('')
                $(this).focus()
            }
            return true;
        }
    });
    $('.chosen-select').chosen();
    $('#pesquisar-notas-emitidas').click(function () {
        if(validar_campos('form-pesquisar-notas-emitidas')){
            let inicio = $('#inicio').val();
            let fim = $('#fim').val();
            if(fim < inicio){
                alert ("A data incio não pode ser menor que data fim");
                return false;
            }
        }        
       

    });

    
    
});