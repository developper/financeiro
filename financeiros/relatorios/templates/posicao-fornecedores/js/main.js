$(function($) {
    $('.chosen-select').chosen({search_contains: true});

    $('.show-notas').live('click', function () {
        var $this = $(this);
        var fornecedor = $this.attr('fornecedor');
        if($this.hasClass('fa-caret-square-o-down')) {
            $('.notas-' + fornecedor).toggle('slow');
            $this.removeClass('fa-caret-square-o-down').addClass('fa-caret-square-o-up');
        } else if($this.hasClass('fa-caret-square-o-up')) {
            $('.notas-' + fornecedor).toggle('slow');
            $this.removeClass('fa-caret-square-o-up').addClass('fa-caret-square-o-down');
        }
    });

    var json_fornecedor = '';
    var json_cliente = '';

    $("#tipo-busca").change(function () {
        var $buscarRetFornElem = $("#fornecedores-clientes");
        var tipo_forn = $(this).val();
        if(
            (json_fornecedor == '' && tipo_forn == 'F') ||
            (json_cliente == '' && tipo_forn == 'C')
        ) {
            $.get('/financeiros/query.php', {
                    query: 'buscar-fornecedor-options',
                    tipo: tipo_forn
                },
                function (response) {
                    if (response != '0') {
                        if(tipo_forn == 'F') {
                            json_fornecedor = response;
                        } else if(tipo_forn == 'C') {
                            json_cliente = response;
                        }
                        response = JSON.parse(response);
                        $buscarRetFornElem.html('');
                        $.each(response, function (index, value) {
                            $buscarRetFornElem.append(
                                '<option value="' + value.idFornecedores + '">' + value.fornecedor + '</option>'
                            );
                        });
                        $buscarRetFornElem.trigger("chosen:updated");
                    }
                }
            );
        } else {
            var response = tipo_forn == 'F' ? json_fornecedor : json_cliente;
            response = JSON.parse(response);
            $buscarRetFornElem.html('');
            $.each(response, function (index, value) {
                $buscarRetFornElem.append(
                    '<option value="' + value.idFornecedores + '">' + (value.TIPO == "F" ? value.razaoSocial : value.nome) + '</option>'
                );
            });
            $buscarRetFornElem.trigger("chosen:updated");
        }
    });
});