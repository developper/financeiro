
<?php
$id = session_id();
if (empty($id))
    session_start();
include_once('../db/config.php');
include_once('../utils/codigos.php');
include_once 'SelectPaciente.php';
//ini_set('display_errors',1);
//ini_set('display_startup_erros',1);
//error_reporting(E_ALL);


use \App\Models\Financeiro\CustosExtras;


class CustoFatura
{
    public function pesquisarFaturas()
    {
        echo "<div id='div-pesquisar-fatura-custo'>  
                 <center>
                     <h1> Fatura/Custo </h1>
                 </center>";


        echo"<fieldset style='width:550px;'>
                    <legend>
                       <b>Pesquisar Faturas</b>
                    </legend>
                    <p><lable for='paciente-custo-fatura'>Paciente</label>";
        echo $this->listarPacientes("paciente-custo-fatura", "COMBO_OBG1");
        echo "</p>
                    <p>
                        <b>Inicio:</b>
                         <input type='text' name='inicio_fatura' value='' maxlength='10' size='15' class='OBG inicio-periodo' id='inicio_fatura'/>
                        <b>Fim:</b> 
                        <input type='text' name='fim_fatura' value='' maxlength='10' size='15' class='OBG fim-periodo' id='fim_fatura'/>
                    </p>                            
                 <button id='pesquisar-fatura-custo' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'  aria-disabled='false' >
                     <span class='ui-button-text'>Pesquisar Faturas</span>
                 </button>
                 &nbsp;&nbsp;
                 <button id='' type='button' class='voltar_ ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'
                    role='button' aria-disabled='false' >
                <span class='ui-button-text'>Voltar</span>
            </button>

       
            </div>
            <div id='resultado-pesquisa-fatura-custo'></div>";



    }

    public function listarPacientes($idSelect,$OBG)
    {
        $paciente = new SelectPaciente;
        return $paciente->pacientes($idSelect,$OBG);

    }

    public function cadastrarCustoFatura($id)
    {
        if(empty($id)){
            echo "<p>"
                . "<b>ERRO: Na fatura, entre em contato com o TI .</b>"
                . "</p>";
            return;
        }
        $idFatura = anti_injection($id,'numerico');
        echo "<div>
                 <center>
                    <h1> Cadastrar custo para a (Fatura ou Orçamento) #{$idFatura}</h1>
                 </center>
            </div> 
            <div id='margem-fatura-custo' title='Resultado da Fatura ou Orçamento.'></div>";
        echo $this->cabecalhoFatura($idFatura);
        echo "<br>";
        if($_SESSION['empresa_principal'] != '1') {
            echo info("<b>Aten&ccedil;&atilde;o:</b> Os custos modificados por você não serão salvos no sistema.
                        Caso o documento não esteja finalizado, os valores dos custos em tela podem ser diferentes do
                        documento em excel.");
        }
        echo $this->detalheFatura($idFatura);


    }
    public function cabecalhoFatura($id)
    {
        $sql2 = "SELECT
		UPPER(c.nome) AS paciente,
		UPPER(u.nome) AS usuario,DATE_FORMAT(f.DATA,'%Y-%m-%d') as DATA,f.DATA_INICIO,f.DATA_FIM,
		(CASE c.sexo
		WHEN '0' THEN 'Masculino'
		WHEN '1' THEN 'Feminino'
		END) AS sexo1,
		FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
		e.nome as empresan,
		c.`nascimento`,
		c.diagnostico,
		c.codigo,
		p.nome as Convenio,
		p.id,
		c.*,
		NUM_MATRICULA_CONVENIO,
		f.imposto_iss
		FROM
		fatura as f inner join
		clientes AS c on (f.PACIENTE_ID = c.idClientes) LEFT JOIN
		empresas AS e ON (e.id = c.empresa) INNER JOIN 
		planosdesaude as p ON (p.id = c.convenio) INNER JOIN usuarios as u on (f.USUARIO_ID = u.idUsuarios)
		
		WHERE
		f.ID = {$id}
		ORDER BY
		c.nome DESC LIMIT 1";

        $result = mysql_query($sql);
        $result2 = mysql_query($sql2);
        $html.= "<table style='width:100%;' >";
        while($pessoa = mysql_fetch_array($result2)){
            foreach($pessoa AS $chave => $valor) {
                $pessoa[$chave] = stripslashes($valor);

            }
            $idade = join("/",array_reverse(explode("-",$pessoa['nascimento'])))."(".$pessoa['idade']." anos)";

            if($idade == "00/00/0000( anos)"){
                $idade='';
            }
            $html.= "<tr bgcolor='#EEEEEE'>";
            $html.= "<td  style='width:60%;'><b>PACIENTE:</b></td>";
            $html.= "<td style='width:20%;' ><b>SEXO </b></td>";
            $html.= "<td style='width:30%;'><b>IDADE:</b></td>";
            $html.= "</tr>";
            $html.= "<tr>";
            $html.= "<td style='width:60%;'>".utf8_decode($pessoa['paciente'])."</td>";
            $html.= "<td style='width:20%;'>{$pessoa['sexo1']}</td>";
            $html.= "<td style='width:30%;'>{$idade}</td>";
            $html.= "</tr>";

            $html.= "<tr bgcolor='#EEEEEE'>";

            $html.= "<td style='width:60%;'><b>UNIDADE REGIONAL:</b></td>";
            $html.= "<td style='width:20%;'><b>CONV&Ecirc;NIO </b></td>";
            $html.= "<td style='width:30%;'><b>MATR&Iacute;CULA </b></td>";
            $html.= "</tr>";
            $html.= "<tr>";

            $html.= "<td style='width:60%;'>{$pessoa['empresan']}</td>";
            $html.= "<td style='width:20%;'>".strtoupper($pessoa['Convenio'])."</td>";
            $html.= "<td style='width:30%;'>".strtoupper($pessoa['NUM_MATRICULA_CONVENIO'])."</td>";
            $html.= "</tr>";

            $html.= "<tr bgcolor='#EEEEEE'>";

            $html.= "<td style='width:60%;'><b>Faturista:</b></td>";
            $html.= "<td style='width:20%;'><b>Data faturado: </b></td>";
            $html.= "<td style='width:30%;'><b>Per&iacute;odo</b></td>";
            $html.= "</tr>";
            $html.= "<tr>";

            $html.= "<td style='width:60%;'>".utf8_decode($pessoa['usuario'])."</td>";
            $html.= "<td style='width:20%;'>".join("/",array_reverse(explode("-",$pessoa['DATA'])))."</td>";
            $html.= "<td style='width:30%;'>".join("/",array_reverse(explode("-",$pessoa['DATA_INICIO'])))." at&eacute; ".join("/",array_reverse(explode("-",$pessoa['DATA_FIM'])))."</td>";
            $html.= "</tr>";

            $html.= "<tr bgcolor='#EEEEEE'>";

            $html.= "<td style='width:60%;'><b>CID:</b></td>";
            $html.= "<td style='width:20%;'><b>CONTA MEDERI: </b></td>";
            $html.= "<td style='width:30%;'></td>";
            $html.= "</tr>";
            $html.= "<tr>";

            $html.= "<td style='width:60%;'>{$pessoa['diagnostico']}</td>";
            $html.= "<td style='width:20%;'>{$pessoa['codigo']}</td>";
            $html.= "<td style='width:30%;'></td>";
            $html.= "</tr>";

            $this->plan =  $pessoa['id'];
            $impostoIss = $pessoa['imposto_iss'];
        }
        $html.= "</table>";

        $html.= "<p>
                    <label for='iss'><b>Favor informar o imposto ISS:</b></label>
                    <input type='number' id='iss' class='OBG' size='5'  value='$impostoIss'> %
                </p>";
        $html.= "<p>

                    <input type='checkbox' id='adicionar-custo-anterior' class='OBG'>
                    <label for='iss'><b>Adicionar custo anteriores aos custos zerados:</b></label>
                </p>";
        return $html;

    }

    public function detalheFatura($idFatura)
    {

        $sql = $this->sqlDetalheFatura($idFatura);
        $result = mysql_query($sql);
        $num_row= mysql_num_rows($result);

        $valorTotal = 0;
        $data = [];

        while($line = mysql_fetch_array($result)){
            $data[] = $line;
            $tipoFatura = $line['ORCAMENTO'];
            $quantidade = $line['QUANTIDADE'];
            $desconto_acrescimo =  ($quantidade*$line['VALOR_FATURA'])*($line['DESCONTO_ACRESCIMO'] /100);
            $valorTotal +=($quantidade*$line['VALOR_FATURA'])+$desconto_acrescimo;
        }

        ///custos  extras
        $sqlExtras = "select
        fatura_custos_extras.*,
        custos_extras.nome

        from
        fatura_custos_extras
        inner join custos_extras on fatura_custos_extras.id_custo_extra = custos_extras.id
        where
        fatura_custos_extras.id_fatura = {$idFatura}
        and canceled_by is null
        ";
        $totalCustosExtras =0;
        $i=0;
        $resultExtras = mysql_query($sqlExtras);
        while($array = mysql_fetch_array($resultExtras)){
            $arrayExtra[$i] = $array;
            $totalCustosExtras += $array['custo'] * $array['quantidade'];
            $i++;
        }

        if(!$result){
            echo "<p> "
                . "<b>ERRO: Problema na pesquisa dos itens da fatura. Entre em contato com o TI</b>"
                . "</p>";
            return;
        }
        if($num_row == 0 ){
            echo "<p>"
                . "<b>Nenhuma fatura foi encontrada.</b>"
                . "</p>";
            return;
        }


        echo "<br>";
        echo "
              <div id='saving'
                    class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-default'
                    style='position: fixed;
                    top: 20px;
                    right: 40px;
                    width: 120px;
                    height: 20px;
                    display: none;
                    background-color: #fff;
                    border: 1px solid #000;
                    border-radius: 2px;
                    -webkit-box-shadow: -3px 1px 10px 0px rgba(50, 50, 50, 0.71);
                    -moz-box-shadow:    -3px 1px 10px 0px rgba(50, 50, 50, 0.71);
                    box-shadow:         -3px 1px 10px 0px rgba(50, 50, 50, 0.71);'>
                 <span style='vertical-align: middle; color: #8B0000;'>Salvando...</span>
              </div>    
              
            <div id='div-finalizar-custo' title='Enviar para Relatório de Margem de Contribuição'>
               <br>
              <span>
                Indique em que mês e ano esse documento deve aparecer no Relatório de Margem de Contribuição.
              </span>
              <br>
              <label for='data-finalizar-custo'><b>Data: </b><label>
              <input id='data-finalizar-custo' class='mes-ano OBG'>
              <input type='hidden' value='$idFatura' id='idFaturaFinalizarCusto'>
              <input type='hidden' value='$tipoFatura' id='tipoFatura'>
          </div>

              <table  width=100% class='mytable'>
		              <thead>
                    <tr>		     	
                       <th style='font-size:12px;'>Item</th>                            
                       <th style='font-size:12px'>Qtd</th>
                       <th style='font-size:12px;'>Unidade</th>
                       <th style='font-size:12px;'>Pre&ccedil;o Unt.</th>
                       <th style='font-size:12px;'>Custo Anterior</th>
                       <th style='font-size:12px;'>Custo Unit.</th>
                    </tr>                    
               </thead>
                    <tr>
                        <th colspan='6' align='center' bgcolor='#90ee90'><b>Valor Total Faturado:</b> R$ ".number_format($valorTotal,2,',','.')."</th>
                    </tr>";

        $valorTotal = 0;
        $custoTotal = 0;
        foreach($data as $row){
            $dataDocumentoAtual = $row['DATA_INICIO'];
            $tipoDocumentoAtual = $row['ORCAMENTO'];
            $pacienteID = $row['PACIENTE_ID'];
            $cor='';
            $obs = $row['OBS'];
            $quantidade = $row['QUANTIDADE'];
            $custoItem = $row['VALOR_CUSTO'];
            $custoItemCalculo = $row['VALOR_CUSTO'];
            $desconto_acrescimo =  ($quantidade*$row['VALOR_FATURA'])*($row['DESCONTO_ACRESCIMO'] /100);
            $valorTotal +=($quantidade*$row['VALOR_FATURA'])+$desconto_acrescimo;
            $custoTotal += $quantidade*$custoItemCalculo;

            echo  "<tr {$cor}>
                        <td width='50%'>{$row['NUMERO_TISS']} - {$row['principio']} </td>
                        <td>{$row['QUANTIDADE']}</td>
                        <td>{$row['unidade_medida']}</td>
                        <td>R$ ".number_format($row['VALOR_FATURA'],2,',','.')."</td>
                        <td>R$ ".number_format($row['custo_item'],2,',','.')."
                        </td>
                        <td>
                             R$
                             <input type='text' id='{$row['ID']}' name='valor_item' class='valor calcula-custo valor-item' value='".number_format($custoItem,2,',','.')."'"
                . "data-id-faturamento='{$row['ID']}' "
                . "data-codigo-item='{$row['cod_item']}'"
                . "data-tipo='{$row['TIPO']}'"
                . "data-acrescimo-desconto='{$row['DESCONTO_ACRESCIMO']}'"
                . "data-paciente='{$row['PACIENTE_ID']}'"
                . "data-custo-iten-fatura-id='{$row['custo_fatura_id']}'"
                . "data-quantidade='{$row['QUANTIDADE']}'"
                . "data-valor-fatura='{$row['VALOR_FATURA']}'"
                . "data-tabela-origem='{$row['TABELA_ORIGEM']}'"
                . "data-custo-item-valor='".number_format($row['custo_item'],2,',','.')."' ></td>";
            $tipoDocumento = $row['ORCAMENTO'];
            $idPlano = $row['PLANO_ID'];
        }

        $pissCofins = 4.95;
        $ir = 3.5;

        $sqlIssFatura =  "select imposto_iss from fatura where id = {$idFatura}";
        $resultIssFatura = mysql_query($sqlIssFatura);
        $issFatura = mysql_result($resultIssFatura,0);

        $custoTotal = ($valorTotal * ($pissCofins + $ir + $issFatura )/100) + $custoTotal +$totalCustosExtras;
        $lucroReais = $valorTotal - $custoTotal;
        $lucroPorcento = ($valorTotal - $custoTotal)/$valorTotal;

        $custosExtras = CustosExtras::getAtivos();

        // custos extras
        echo "</table>
                <br>
                <fieldset width='90%'>
                    <legend><b> Adicionar custos extras:</b></legend>";
        if(!empty($custosExtras)){
                    echo "<div id='div-adicionar-item'>
                        <label><b>Itens:</b></label>
                       <select style='background-color:transparent;' id='sel-custo-extra' class='OBG'>
                            <option value='' selected>Selecione</option>";
                        foreach($custosExtras as $cExtra){
                                echo "<option value='{$cExtra['id']}' >{$cExtra['nome']}</option>";
                        };

             echo " </select>
                     <button id='adicionar-custo-extra'   title='Adicionar custo extra.' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'  aria-disabled='false' >
                     <span class='ui-button-text'>+</span>
                 </button>
                    </div>";
                    }else{

            echo " <div> Nenhum item de custo extra adicionado. Favor solicitar a pessoa responsável para realizar o cadastro caso queira inserir algum. </div> ";

                 }
                   echo "<br>
                    <div id ='div-table-custo-extra'>
                        <table class='mytable' width=100% id='table-custo-extra' >
                            <thead>
                                <tr>
                                    <th  width='50%'>ITEM</th>

                                    <th>QUANTIDADE</th>

                                    <th>CUSTO UNIT.</th>
                                </tr>
                            </thead>";
        // Verifica se a fatura já teve custo cadastrado caso ainda não tenha, o sistema vai trazer os custos extras cadastrados
        // na fatura do mês anterior caso exista.

        if($this->getFaturaTeveCustoLancado($idFatura) == 0){
            $arrayCustoExtraMesAnterior = CustosExtras::getCustoExtraMesAnterior($pacienteID,$dataDocumentoAtual,$tipoDocumentoAtual);
            if(!empty($arrayCustoExtraMesAnterior)){
                $contArrayCustoExtraMesAnterior = 1;
                foreach($arrayCustoExtraMesAnterior as $extra ){

                    echo  "<tr class='tr-item-custo-extra'
                            id-fatura-custo-extra= '0'
                            quantidade-anterior = '0'
                            custo-anterior = '0'
                            cancelar='N'>
                            <td id-custo-extra='{$extra['id_custo_extra']}'>
                                <img align='left' class='remover-custo-extra' style ='padding-right:4px;'
                                    src='../utils/delete_16x16.png' title='Remover' border='0'/>
                                    {$extra['nome']}
                            </td>
                            <td><input type='number' class='OBG qtd-custo-extra' value='' id='tr-{$contArrayCustoExtraMesAnterior}'' /></td>
                            <td>R$ <input type='text' value='".number_format($extra['custo'],2,',','.')."' class='valor calcula-custo-extra'  /></td></tr>";
                    $contArrayCustoExtraMesAnterior++;

                }


            }

        }
        if(!empty($arrayExtra)){
                $contExtra = 1;
            foreach($arrayExtra as $extra ){

                echo  "<tr class='tr-item-custo-extra'
                            id-fatura-custo-extra= '{$extra['id']}'
                            quantidade-anterior = '{$extra['quantidade']}'
                            custo-anterior = '".number_format($extra['custo'],2,',','.')."'
                            cancelar='N'>
                            <td id-custo-extra='{$extra['id_custo_extra']}'>
                                <img align='left' class='cancelar-custo-extra' style ='padding-right:4px;'
                                    src='../utils/delete_16x16.png' title='Remover' border='0'/>
                                    {$extra['nome']}
                            </td>
                            <td><input type='number' class='OBG qtd-custo-extra' value='{$extra['quantidade']}' id='tr-{$contExtra}'' /></td>
                            <td>R$ <input type='text' value='".number_format($extra['custo'],2,',','.')."' class='valor calcula-custo-extra'  /></td></tr>";
                $contExtra++;

            }


        }
                 echo "</table>
                    </div>

                </fieldset>";



              echo "  <br>
                <table class='mytable' width=100% >
                <thead>
                <tr><th colspan='2'>Valores dos encargos adiconados ao custo total: PISCOFINS 4,73 %, IR 2% mais o ISS informado acima.
                    <b>OBS:</b> O valor total da fatura leva em consideração os descontos ou acréscimos</th></tr>
</thead>

                         <tr>
                            <td>
                                <b> Valor Total Faturado:</b>
                            </td>
                            <td>
                                R$ <span id='valor-total'>".number_format($valorTotal,2,',','.')."</span>
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <b>Custo Total + Encargos:</b>
                            </td>
                            <td>
                                 R$ <span id='custo-encargo'> ".number_format($custoTotal,2,',','.')."</span>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <b>Margem:</b>
                            </td>
                            <td>
                                 R$ <span id='margem'>".number_format($lucroReais,2,',','.')."</span>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <b>Margem Percentual &cong;</b>
                            </td>
                            <td>
                                 <span id='margem-percentual'>".number_format(($lucroPorcento)*100,2,',','.')."</span>%
                            </td>
                        </tr>
                        " . ($obs != '' ? "<tfoot><tr><td colspan='2'><b>OBSERVAÇÃO:</b> {$obs}</td></tr></tfoot>" : "") . "
                </table>
                <br>
                 <input type='hidden' value='$tipoDocumento' name ='tipo-documento' id ='tipo-documento'>
                 <input type='hidden' value='3.5' name ='ir' id ='ir'>
                 <input type='hidden' value='4.95' name ='piscofins' id ='piscofins'>
                 <input type='hidden' value='$idFatura' name ='id-fatura' id ='id-fatura'>";
        if($_SESSION['empresa_principal'] == '1') {
            echo "<button id='salvar-custo-fatura' title='Os custos lançados vão ser gravados neste orçamento/fatura' tipo='s'  id-fatura = '{$idFatura}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'  aria-disabled='false' >
                     <span class='ui-button-text'>Salvar Custo</span>
                 </button>
                <button id='finalizar-custo-fatura' tipo='f' tipo-documento = '{$tipoDocumento}' id-fatura = '{$idFatura}' title='Ao clicar nesse botão o orçamento/fatura  sera enviado para o Relatório de Margem de Contribuição. E o mesmo não poderá mais ser modificado.' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'  aria-disabled='false' >
                     <span class='ui-button-text'>Finalizar</span>
                 </button>";
        }

    }

    public function sqlDetalheFatura($id)
    {
        $sql="
                    SELECT
                            f.*,
                            fa.OBS,
                            IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),B.DESC_MATERIAIS_FATURAR) AS principio,
                            (CASE f.TIPO
                            WHEN '0' THEN 'Medicamento'
                            WHEN '1' THEN 'Material'
                             WHEN '3' THEN 'Dieta' END) as apresentacao,
                             (CASE f.TIPO
                            WHEN '0' THEN '3'
                            WHEN '1' THEN '4'
                             WHEN '3' THEN '5' END) as ordem,
                            1 as cod,
                            COALESCE(custo_item.custo,0.00) as custo_item,
                            f.CATALOGO_ID as cod_item,
                            COALESCE(custo_item.id,0) as custo_fatura_id,
                            fa.PLANO_ID,
                            unidades_fatura.UNIDADE AS unidade_medida,
                            fa.DATA_INICIO,
                            fa.ORCAMENTO
		    FROM
                                faturamento as f  INNER JOIN
                                `catalogo` B ON (f.CATALOGO_ID = B.ID)
                                LEFT JOIN unidades_fatura ON f.UNIDADE = unidades_fatura.ID
                                inner join fatura fa on (f.FATURA_ID=fa.ID) LEFT JOIN
                                custo_item_fatura as custo_item on (fa.PACIENTE_ID =custo_item.paciente_id 
                                                                    and custo_item.catalogo_id =f.CATALOGO_ID
                                                                    and custo_item.tipo = f.TIPO)
		    WHERE
			f.FATURA_ID= {$id} and
			f.TIPO IN (0,1,3) 
                        and f.CANCELADO_POR is NULL
							
		UNION		
			SELECT 
                            f.*,
                            fa.OBS,
			    vc.DESC_COBRANCA_PLANO AS principio,                           
		           ('Servi&ccedil;os') AS apresentacao,	
                            1 as ordem,						  
			    vc.COD_COBRANCA_PLANO as cod,
                            COALESCE(custo_item.custo,0.00) as custo_item,
                            vc.id as cod_item,
                            COALESCE(custo_item.id,0) as custo_fatura_id,
                            fa.PLANO_ID,
                            unidades_fatura.UNIDADE AS unidade_medida,
                            fa.DATA_INICIO,
                            fa.ORCAMENTO
		      FROM 
                            faturamento as f INNER JOIN				      
                             valorescobranca as vc on (f.CATALOGO_ID =vc.id) 
                             LEFT JOIN unidades_fatura ON f.UNIDADE = unidades_fatura.ID
                             inner join fatura fa on (f.FATURA_ID=fa.ID) LEFT JOIN
                            custo_item_fatura as custo_item on (fa.PACIENTE_ID = custo_item.paciente_id 
                                                            and custo_item.valorescobranca_id =vc.id
                                                            and custo_item.tipo = f.TIPO)
                      WHERE 
                            f.FATURA_ID= {$id} and 
                            f.TIPO =2 and 
                            f.TABELA_ORIGEM='valorescobranca'	
                              and f.CANCELADO_POR is NULL
                     UNION           
                      SELECT 
                            f.*,
                            fa.OBS,
			                 vc.item AS principio,
		                  ('Servi&ccedil;os') AS apresentacao,
                            1 as ordem,						  
			               '' as cod,
                            COALESCE(custo_item.custo,0.00) as custo_item,
                            vc.id as cod_item,
                            COALESCE(custo_item.id,0) as custo_fatura_id,
                            fa.PLANO_ID,
                            unidades_fatura.UNIDADE AS unidade_medida,
                            fa.DATA_INICIO,
                            fa.ORCAMENTO
		      FROM 
                            faturamento as f INNER JOIN
                             cobrancaplanos as vc on (f.CATALOGO_ID =vc.id) 
                             LEFT JOIN unidades_fatura ON f.UNIDADE = unidades_fatura.ID
                             inner join fatura fa on (f.FATURA_ID=fa.ID)  LEFT JOIN
                            custo_item_fatura as custo_item on (fa.PACIENTE_ID = custo_item.paciente_id 
                                                            and custo_item.cobrancaplano_id =vc.id
                                                            and custo_item.tipo = f.TIPO)
                      WHERE 
                            f.FATURA_ID= {$id} and 
                            f.TIPO =2 and 
                            f.TABELA_ORIGEM='cobrancaplano' 
                             and f.CANCELADO_POR is NULL
			
			UNION
			SELECT DISTINCT
			                 f.*,
                                         fa.OBS,
                                        CO.item AS principio,
                                        
                                        ('Equipamentos') AS apresentacao,
                                            2 as ordem,	
                                         '' as cod, 
                            COALESCE(custo_item.custo,0.00) as custo_item,
                            CO.id as cod_item,
                            COALESCE(custo_item.id,0) as custo_fatura_id,
                            fa.PLANO_ID,
                            unidades_fatura.UNIDADE AS unidade_medida,
                            					      	fa.DATA_INICIO,
                            fa.ORCAMENTO
			FROM 
                                    faturamento as f INNER JOIN
                                    cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`) 
                                    LEFT JOIN unidades_fatura ON f.UNIDADE = unidades_fatura.ID
                                    inner join fatura fa on (f.FATURA_ID=fa.ID) LEFT JOIN
                                   custo_item_fatura as custo_item on (fa.PACIENTE_ID = custo_item.paciente_id
                                    and custo_item.cobrancaplano_id =CO.id
                                    and custo_item.tipo = f.TIPO)
						
		      WHERE 
                                    f.FATURA_ID= {$id} and
                                    f.TIPO =5  and  
                                    f.TABELA_ORIGEM='cobrancaplano' 
                                     and f.CANCELADO_POR is NULL
                                
                        UNION
			SELECT DISTINCT
			                             f.*,
                                         fa.OBS,
                                        CO.DESC_COBRANCA_PLANO AS principio,
                                                                          
                                        ('Equipamentos') AS apresentacao,
                                            2 as ordem,	
                                         CO.COD_COBRANCA_PLANO as cod, 
                            COALESCE(custo_item.custo,0.00) as custo_item,
                            CO.id as cod_item,
                            COALESCE(custo_item.id,0) as custo_fatura_id,
                            fa.PLANO_ID,
                            unidades_fatura.UNIDADE AS unidade_medida,
                            					      	fa.DATA_INICIO,
                            fa.ORCAMENTO
			FROM 
                                    faturamento as f INNER JOIN
                                    valorescobranca CO ON (f.CATALOGO_ID = CO.`id`)  
                                    LEFT JOIN unidades_fatura ON f.UNIDADE = unidades_fatura.ID
                                    inner join fatura fa on (f.FATURA_ID=fa.ID) LEFT JOIN
                                   custo_item_fatura as custo_item on (fa.PACIENTE_ID = custo_item.paciente_id
                                                            and custo_item.valorescobranca_id =CO.id
                                                            and custo_item.tipo = f.TIPO)
						
		      WHERE 
                                    f.FATURA_ID= {$id} and
                                    f.TIPO =5  and  
                                    f.TABELA_ORIGEM='valorescobranca' 
                                     and f.CANCELADO_POR is NULL

					
		UNION
			SELECT DISTINCT
			                 f.*,fa.OBS,
					 CO.item AS principio,				       
					   
					  ('Gasoterapia') AS apresentacao,
					 6 as ordem,	
					'' as cod, 
                            COALESCE(custo_item.custo,0.00) as custo_item,
                            CO.id as cod_item,
                            COALESCE(custo_item.id,0) as custo_fatura_id,
                            fa.PLANO_ID,
                            unidades_fatura.UNIDADE AS unidade_medida,
                            					      	fa.DATA_INICIO,
                            fa.ORCAMENTO
				FROM 
                                        faturamento as f INNER JOIN
                                        cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`) 
                                        LEFT JOIN unidades_fatura ON f.UNIDADE = unidades_fatura.ID
                                        inner join fatura fa on (f.FATURA_ID=fa.ID) LEFT JOIN
                            custo_item_fatura as custo_item on (fa.PACIENTE_ID = custo_item.paciente_id 
                                                            and custo_item.cobrancaplano_id =CO.id
                                                            and custo_item.tipo = f.TIPO)
				WHERE 
					     f.FATURA_ID= {$id} and
					      f.TIPO =6  and f.TABELA_ORIGEM='cobrancaplano' 
                                               and f.CANCELADO_POR is NULL
              UNION
			SELECT DISTINCT
			                 f.*,fa.OBS,
					 vc.DESC_COBRANCA_PLANO AS principio,				       
					  
					  ('Gasoterapia') AS apresentacao,
					 6 as ordem,	
					 vc.COD_COBRANCA_PLANO as cod,
                            COALESCE(custo_item.custo,0.00) as custo_item,
                            vc.id as cod_item,
                            COALESCE(custo_item.id,0) as custo_fatura_id,
                            fa.PLANO_ID,
                            unidades_fatura.UNIDADE AS unidade_medida,
                            fa.DATA_INICIO,
                            fa.ORCAMENTO
					      	
				FROM 
                                        faturamento as f  inner join 
                                        valorescobranca as vc on (vc.id = f.CATALOGO_ID) 
                                        LEFT JOIN unidades_fatura ON f.UNIDADE = unidades_fatura.ID inner join 
                                        fatura fa on (f.FATURA_ID=fa.ID) LEFT JOIN
                            custo_item_fatura as custo_item on (fa.PACIENTE_ID = custo_item.paciente_id 
                                                            and custo_item.valorescobranca_id =vc.id
                                                            and custo_item.tipo = f.TIPO)
				WHERE 
					     f.FATURA_ID= {$id} and
					      f.TIPO =6  and 
                                              f.TABELA_ORIGEM='valorescobranca' 	
                                               and f.CANCELADO_POR is NULL
					      			
	ORDER BY 
        ordem,
        tipo,
        principio";

        return $sql;
    }

    public function getTableIssEmpresa()
    {
        $sql = "select nome, COALESCE(imposto_iss,0.00) as iss from empresas where ATIVO = 'S'";
        $result = mysql_query($sql);
        $html .= "<table class = 'mytable'>
                <thead>
                <tr>
                <th>Unidade Regional</th>
                 <th>Imposto ISS</th>
                </tr>
                </thead>";

        while($row = mysql_fetch_array($result)){
            $html .= "<tr>
            <td>".strtoupper($row['nome'])."</td>
            <td>".$row['iss']." %</td>
            </tr>";
        }
        echo $html .= "</table>";
        return;
    }

    public function getFaturaTeveCustoLancado($faturaId)
    {
        $sql = "Select
        f.*
        from
        faturamento as f
        where
        f.FATURA_ID = {$faturaId}
        and
        f.custo_atualizado_em is not null
        ";

        $result =  mysql_query($sql);
        $foiLancadoCusto = mysql_num_rows($result);
        return $foiLancadoCusto;

    }



}


