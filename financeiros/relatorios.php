<?php
$id = session_id();
if(empty($id))
    session_start();
include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/codigos.php');

class RelatoriosFinanceiro {

    function menuRelatoriosFinanceiro()
    {
        echo "
        <p>
             <a href='/financeiros/relatorios/?action=relatorio-fluxo-caixa'>
                Relatório de Fluxo de Caixa Projetado.
             </a>
            </p>
            <p>
             <a href='/financeiros/relatorios/?action=relatorio-fluxo-caixa-completo'>
                Relatório de Fluxo de Caixa Completo.
             </a>
            </p>
                      
            <p>
            <b>Relatório Contas Emitidas</b>
            <ul>
            
              <li>
                <a href='/financeiros/relatorios/?action=notas-emitidas&tipo=pagar'>
                    Contas a Pagar
                </a>
              </li>
              <li>
                <a href='/financeiros/relatorios/?action=notas-emitidas&tipo=receber'>
                    Contas a Receber
                </a>
              </li>
            </ul>
             
            </p>

            <p>
            <b>Relatório Contas Processadas</b>
            <ul>
            
              <li>
                <a href='/financeiros/relatorios/?action=notas-com-processamento&tipo=pagar'>
                    Contas a Pagar
                </a>
              </li>
              <li>
                <a href='/financeiros/relatorios/?action=notas-com-processamento&tipo=receber'>
                    Contas a Receber
                </a>
              </li>
            </ul>
             
            </p>

            <!--<p>
             <a href='/financeiros/relatorios/?action=notas-com-processamento&tipo=receber'>
                Relatório Contas Emitidas a Receber
             </a>
            </p>
            <p>
             <a href='/financeiros/relatorios/?action=notas-com-processamento&tipo=receber'>
                Relatório Contas a Receber com Processamento
             </a>
            </p>
            <p>
             <a href='/financeiros/relatorios/?action=notas-com-processamento&tipo=pagar'>
                Relatório Contas a Pagar com Processamento
             </a>
            </p> -->
            <!-- <p>
             <a href='?op=relatorio-margem-contribuicao'>
                Relatório de Margem de Contribuição.
             </a>
            </p> -->
            <p>
             <a href='/financeiros/relatorios/?action=form-relatorio-retencoes'>
                Relatório de Retenções
             </a>
            </p><p>
             <a href='/financeiros/relatorios/?action=posicao-fornecedores'>
                Posição de Fornecedores/Clientes
             </a>
            </p>
            
            <!--<p>
                <a href='../auditoria/relatorios/?action=form-relatorio-faturamento-enviado'>
                 Relat&oacute;rio de Faturamento Enviado
                </a>
            </p>
            <p>
                <a href='../remocao/?action=relatorio-remocoes-realizadas&from=financeiro'>
                 Relat&oacute;rio de Remoções Realizadas
                </a>
            </p>
            <p>
                <a href=../logistica/relatorios/?action=relatorio-notas-entrada>
                Relat&oacute;rio de Entrada de Notas da Logística
                </a>
            </p>
            <p>
                <a href=../eventos_aph/?action=relatorio-eventos-aph-realizados>
                Relat&oacute;rio de Eventos/APH Finalizados.
                </a>
            </p> -->
            <p>
             <a href='/financeiros/relatorios/?action=relatorio-juros-desconto'>
                Relatório de Juros e Desconto.
             </a>
            </p>
            

            ";
        // if($_SESSION['adm_user'] == 1 && $_SESSION['empresa_principal'] == 1) {
        //     echo "<p>
        //      <a href='?op=relatorio-custos'>
        //         Custo de Material por Empresa
        //      </a>
        //      <p>
        //         <a href='../financeiros/relatorios/?action=relatorio-simulacoes-realizadas'>
        //          Relat&oacute;rio de Simulações Realizadas
        //         </a>
        //     </p>
        //     </p>";
        // }

    }

    function relatorioFluxoCaixaRealizado()
    {

        echo <<<HTML
<form action="demonstrativo-financeiro.php">
  <div>
      <center><h1>Relatório de Fluxo de Caixa Realizado</h1></center>
      <p>
        <strong>OBS: Devido ao grande volume de informações que este relatório consulta, o mesmo pode levar alguns minutos para ser gerado.</strong>
      </p>
HTML;
        $this->status();
        $this->contas_multiple();
        $this->periodo();
        echo <<<HTML
        <p>
            <label><b>Tipo: </b></label>
            <label><input type="radio" name="tipo" value="M" checked /> Mensal</label>
            <label><input type="radio" name="tipo" value="D" /> Diário</label>
        </p>
        <button id='pesquisar-relatorio-fluxo-caixa-realizado'
            class='ui-button ui-widget ui-state-default ui-corner-all
            ui-button-text-only ui-state-hover' aria-disabled='false'
            role='button' style='float:left;' type='submit'>
          <span class='ui-button-text'>
            Exportar para Excel
          </span>
        </button>
        &nbsp;&nbsp;
         <button id='' type='button' class='voltar_ ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'
                    role='button' aria-disabled='false' >
                <span class='ui-button-text'>Voltar</span>
            </button>
  </div>
</form>
HTML;

    }

    function relatorioAnaliticoFluxoCaixa()
    {
        echo "<div id='relatorio-analitico-fluxo-caixa'>
                
                  <center>
                     <h1>
                     Relatório Analítico de Fluxo de Caixa
                     </h1>
                  </center>
                ";
        $this->status();
        $this->periodo();
        $this->nivel4IPCF();
        $empresa= $_SESSION['empresa_principal']==1?$this->empresasRelatorios():
            "<input type='hidden' id='empresa-relatorio' value='{$_SESSION['empresa_principal']}'>";
        echo $empresa;
        echo "   </br>
                </br>
                <button id='pesquisar-relatorio-analitico-fluxo-caixa' 
                    class='ui-button ui-widget ui-state-default ui-corner-all 
                    ui-button-text-only ui-state-hover' aria-disabled='false'
                    role='button' style='float:left;' type='button'>
                  <span class='ui-button-text'>
                    Pesquisar
                  </span>
                </button>
                &nbsp;&nbsp;
                 <button id='' type='button' class='voltar_ ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'
                    role='button' aria-disabled='false' >
                <span class='ui-button-text'>Voltar</span>
            </button>
                <div style='float:right;' id='opcoes-relatorio-analitico-fluxo-caixa'>
                </div>
                <br/>
                <div id='div-resultado-relatorio-analitico-fluxo-caixa'>
                </div>
            </div>";
    }

    function periodo(){
        echo "<p>
                <span>
                   <b>
                    Período
                   </b>
                   <label for='inicio'>
                      <b>Início: </b>
                  </label>
                  <input id='inicio'  type='text' style='text-align:right'  class='data COMBO_OBG' name='inicio' />
                  <label for='fim'>
                   <b>Fim: </b>
                  </label>
                  <input id='fim'  type='text' style='text-align:right'  class='data COMBO_OBG' name='fim'/>
                </span>
             </p>";
    }
    function periodoMesAno(){
        echo "<p>
                <span>
                   <b>
                    Período
                   </b>
                   <label for='inicio'>
                      <b>Início: </b>
                  </label>
                  <input id='inicio'  type='text' style='text-align:right'  class='apenas-mes-ano COMBO_OBG' name='inicio' />
                  <label for='fim'>
                   <b>Fim: </b>
                  </label>
                  <input id='fim'  type='text' style='text-align:right'  class='apenas-mes-ano COMBO_OBG' name='fim'/>
                </span>
             </p>";
    }

    function contas()
    {
        $sql = <<<SQL
      SELECT
        idcb.ID,
        idcb.conta
      FROM
        indexed_demonstrativo_contas_bancarias AS idcb
			ORDER BY
			 	ID
SQL;
        $rs = mysql_query($sql);
        echo "<p id='contas'>
              <strong>Conta: </strong>
              <select name='origem' id='origem' class='OBG'>
                <option value=''>TODOS</option>";
        while($row = mysql_fetch_array($rs, MYSQL_ASSOC)){
            echo "  <option value='{$row['ID']}'>{$row['conta']}</option>";
        }
        echo "  </select>
            </p>";
    }

    function contas_multiple()
    {
        $sql = <<<SQL
      SELECT
        idcb.ID,
        idcb.conta
      FROM
        indexed_demonstrativo_contas_bancarias AS idcb
			ORDER BY
			 	ID
SQL;
        $rs = mysql_query($sql);
        echo "<p id='contas'>
              <strong>Conta(s): </strong>
              <select name='origem[]' id='origem' data-placeholder='Selecione as contas para compor o relatório...' multiple class='chosen-select OBG'>";
        while($row = mysql_fetch_array($rs, MYSQL_ASSOC)){
            echo "  <option value='{$row['ID']}'>{$row['conta']}</option>";
        }
        echo "  </select>
                <input type='checkbox' name='origem' id='origem-todas' value='T'> Todas
            </p>";
    }

    function status()
    {
        echo <<<HTML
          <p>
            <label><b>Formato: </b></label>
            <label><input type="radio" name="status" class='status_relatorio' value="Processada" checked /> Processado</label>
            <label><input type="radio" name="status" class='status_relatorio' value="Pendente" /> Projetado</label>
          </p>
HTML;
    }

    function filiais(){
        $sql = "SELECT id AS cod, nome FROM empresas WHERE ATIVO='S'";
        $rs = mysql_query($sql);
        echo "<p>
                <strong>UR: </strong><select name='ur' id='ur' class='OBG'>";
        while($row = mysql_fetch_array($rs, MYSQL_ASSOC)){
            echo "    <option value='{$row['cod']}'>{$row['nome']}</option>";
        }
        echo "    </select>
            </p>";
    }

    function nivel4IPCF(){
        $sql=' SELECT
                plano_contas.ID,
                plano_contas.COD_N4,
                plano_contas.N4
                FROM
                plano_contas
                WHERE
                plano_contas.ID > 153
                ORDER BY
                plano_contas.COD_N4 ASC';

        $result=mysql_query($sql);
        $cont=0;
        echo "<p>
                 <label for='ipcf_nivel4'><b>IPCF Nível 4</b></label>
                 <select id='ipcf_nivel4' style='background-color:transparent;'>
              ";
        while($row = mysql_fetch_array($result)){
            $optionTodos= $cont ==0? "<option value='T' selected='selected'>Todos</option>":'';
            echo $optionTodos;
            echo "<option value='{$row['ID']}'>{$row['COD_N4']} - {$row['N4']}</option>";
            $cont++;


        }
        echo"</select>
             </p>";

    }

    function empresasRelatorios(){
        $cont =0;
        $result = mysql_query("SELECT * FROM empresas where ATIVO = 'S' ORDER BY nome ");
        $var .= "<p>
                <label for='empresa-relatorio'><b>Empresa:</b></label>
                <select class='COMBO_OBG' name='empresaRelatorios' id='empresa-relatorio' style='background-color:transparent;'>";
        while($row = mysql_fetch_array($result))
        {
            $var .= $cont == 0? "<option value='T'>Todos</option>":'';

            $var .= "<option value='". $row['id'] ."'>". $row['nome'] ."</option>";
            $cont++;
        }
        $var .= "</select>
                </p>";
        return $var;

    }

    public function relatorioCustos()
    {
        $msg = filter_input(INPUT_GET, 'msg', FILTER_SANITIZE_STRING);
        $msg = (isset($msg) && !empty($msg)) ? base64_decode($msg) : '';
        echo <<<HTML
    <form action="custos.php" method="post">
      <div id="custos-container">
        <center><h1>Relatório de Custos Por UR</h1></center>
HTML;
        $this->filiais();
        $this->periodo();
        echo <<<HTML
        <p style="color: red;">
            <strong>{$msg}</strong>
        </p>
        <button id='gerar-relatorio-custos'
            class='ui-button ui-widget ui-state-default ui-corner-all
            ui-button-text-only ui-state-hover' aria-disabled='false'
            role='button' style='float:left;' type='button'>
          <span class='ui-button-text'>
            Buscar
          </span>
        </button>
        &nbsp;
        <button id='pesquisar-relatorio-custos'
            class='ui-button ui-widget ui-state-default ui-corner-all
            ui-button-text-only ui-state-hover' aria-disabled='false'
            role='button' style='float:left;' type='submit'>
          <span class='ui-button-text'>
            Exportar para excel
          </span>
        </button>
        &nbsp;
        <button id='' type='button' class='voltar_ ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'
                role='button' aria-disabled='false' >
            <span class='ui-button-text'>Voltar</span>
        </button>
        <div id="resultado"></div>
    </div>
  </form>
HTML;

    }

}

