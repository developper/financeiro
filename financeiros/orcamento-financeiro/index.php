<?php

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);


$routes = [
    'GET'  => [
        '/financeiros/orcamento-financeiro/?action=orcamento-financeiro-index' => '\App\Controllers\OrcamentoFinanceiro::index',
        '/financeiros/orcamento-financeiro/?action=orcamento-financeiro' => '\App\Controllers\OrcamentoFinanceiro::novo',
        '/financeiros/orcamento-financeiro/?action=gerar' => '\App\Controllers\OrcamentoFinanceiro::gerar',
        '/financeiros/orcamento-financeiro/?action=listar-naturezas' => '\App\Controllers\OrcamentoFinanceiro::listarNaturezas',       
        '/financeiros/orcamento-financeiro/?action=orcamento-financeiro-visualizar-editar' => '\App\Controllers\OrcamentoFinanceiro::orcamentoById',
        '/financeiros/orcamento-financeiro/?action=gerar-itens-plano-contas' => '\App\Controllers\OrcamentoFinanceiro::gerarItensPlanoContas',
        '/financeiros/orcamento-financeiro/?action=relatorio-orcamento-financeiro' => '\App\Controllers\OrcamentoFinanceiro::relatorioOrcamentoFinanceiro',
        '/financeiros/orcamento-financeiro/?action=relatorio-gerencial-orcamento-financeiro' => '\App\Controllers\OrcamentoFinanceiro::relatorioOrcamentoFinanceiro',
        '/financeiros/orcamento-financeiro/?action=buscar-relatorio-orcamento-financeiro' => '\App\Controllers\OrcamentoFinanceiro::relatorioByModeloAndOrcamentoById',
        '/financeiros/orcamento-financeiro/?action=cadastrar-valores-gerenciais' => '\App\Controllers\OrcamentoFinanceiro::cadastrarValoresGerenciaisByModeloOrcamentoById',
        '/financeiros/orcamento-financeiro/?action=cadastrar-valores-gerenciais-editar' => '\App\Controllers\OrcamentoFinanceiro::valoresGerenciaisByOrcamentoById',
        '/financeiros/orcamento-financeiro/?action=cron-atualizar-valores-financeiros' => '\App\Controllers\CronJobs\Financeiro\FinanceiroIW::cronFinanceiroIW',
        

    ],

    'POST' => [
        '/financeiros/orcamento-financeiro/?action=gerar' => '\App\Controllers\OrcamentoFinanceiro::gerar',
        '/financeiros/orcamento-financeiro/?action=update-item' => '\App\Controllers\OrcamentoFinanceiro::updateItem',
        '/financeiros/orcamento-financeiro/?action=update-itens' => '\App\Controllers\OrcamentoFinanceiro::updateItens',
        '/financeiros/orcamento-financeiro/?action=gerar-itens-plano-contas' => '\App\Controllers\OrcamentoFinanceiro::gerarItensPlanoContas',
        '/financeiros/orcamento-financeiro/?action=orcamento-financeiro-delete-itens' => '\App\Controllers\OrcamentoFinanceiro::deleteItens',
        '/financeiros/orcamento-financeiro/?action=update-valor-gerencial' => '\App\Controllers\OrcamentoFinanceiro::updateItemValorGerencial',

       ]

    ];
$args = isset($_GET) && !empty($_GET) ? $_GET : null;
try {
    $app = new App\Application;
    $app->setRoutes($routes);
    $app->dispatch(METHOD, URI,$args);
} catch(App\BaseException $e) {
    echo $e->getMessage();
} catch(App\NotFoundException $e) {
    http_response_code(404);
    echo $e->getMessage();
}
