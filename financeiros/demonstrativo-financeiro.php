<?php
set_time_limit(300);
ini_set("display_errors", "On");
ini_set("error_reporting", "E_ALL");
$id = session_id();
if(empty($id))
  session_start();

include('../db/config.php');
include('../vendor/autoload.php');

use App\Models\Financeiro\Financeiro;
use App\Core\DateTime;

$inicio = isset($_GET['inicio']) && !empty($_GET['inicio']) ? \DateTime::createFromFormat('d/m/Y', $_GET['inicio']) : false;
$fim    = isset($_GET['fim']) && !empty($_GET['fim']) ? \DateTime::createFromFormat('d/m/Y', $_GET['fim']) : false;
$tipo   = isset($_GET['tipo']) && !empty($_GET['tipo']) ? $_GET['tipo'] : 'M';
$status = isset($_GET['status']) && !empty($_GET['status']) ? $_GET['status'] : 'Processada';
$conta  = isset($_GET['origem']) && !empty($_GET['origem']) ? $_GET['origem'] : false;



$periodos = DateTime::getPeriodos($inicio, $fim, $tipo);

$financeiro = new Financeiro;
$data = $financeiro->getDemonstrativos($periodos, $financeiro->getPlanoContas(), $status, $conta);

ksort($data); // Ordenar por descrição



$table['rows'] = '';
foreach ($data as $descricao => $totais) {
  $totaisHtml = '';
  if (isset($totais) && !empty($totais)) {
    foreach ($periodos as $periodo) {
      if(!property_exists($periodo, 'dia')){
        $datekey = $periodo->inicio->format('m/Y');
      }  else {
        $datekey = $periodo->dia->format('d/m/Y');
      }
      $valor = isset($totais[$datekey]) ? $totais[$datekey] : 0.00;
      $totaisHtml .= sprintf('<td>%s</td>', $valor);
    }
  }

  $table['rows'] .= sprintf('<tr><td>%s</td>%s</tr>', htmlentities($descricao), $totaisHtml);
}

$table['col']['dates'] = array_reduce($periodos, function($buffer, $periodo) {
  return $buffer .= sprintf('<td>%s</td>', (!property_exists($periodo, 'dia') ? $periodo->inicio->format('m/Y') : $periodo->dia->format('d/m/Y')));
});

$qtdColunas = count($periodos) + 1; // Inclui coluna Desc.

$filename = sprintf("relatorio_fluxo_caixa_realizado_%s_%s", $inicio->format('d-m-Y'), $fim->format('d-m-Y'));

header("Content-type: application/x-msexce; charset=ISO-8859-1");
header("Content-Disposition: attachment; filename={$filename}.xls");
header("Pragma: no-cache");

?>

<table>
  <tr>
    <td colspan="<?= $qtdColunas ?>"><h1><?= htmlentities('Relatório Fluxo de Caixa Realizado') ?></h1></td>
  </tr>
  <tr><td colspan="<?= $qtdColunas ?>" style="font-weight: bold; background-color: #ddd;"><?= htmlentities('Período:') ?></td></tr>
  <tr>
    <td colspan="<?= $qtdColunas ?>" style="border-bottom: 1px solid #ccc">
      <?= sprintf('%s - %s', $inicio->format('d/m/Y'), $fim->format('d/m/Y')) ?>
    </td>
  </tr>
  <tr style="font-weight: bold;">
    <td><?= htmlentities('Descrição') ?></td>
    <?= $table['col']['dates'] ?>
  </tr>
  <?= $table['rows'] ?>
</table>
