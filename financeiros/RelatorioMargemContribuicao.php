<?php
use App\Models\Faturamento\PlanoSaude;
class RelatorioMargemContribuicao
{
    public function pesquisarRelatorioMargemCusto()
    {


        echo "<style type='text/css'>
.ui-datepicker-calendar {
    display: none;
}
</style>
<div id='div-relatorio-margem-contribuicao'>
                      <center><h1>Pesquisar Relatório de Margem de Contribuição</h1></center>
                      <p>
                      <div class='ui-state-highlight' style='margin-top: 5px;'>
		                	<center><b>Escolha os filtros, para gerar o relatório.
                              Caso selecione um paciente, este será o filtro principal.<br>
                              </center>
			            </div>

                      </p>
                      <p>";
                      $this->selectPaciente();
        echo          "</p>
                       <p>
                        ";
                         $r =  new RelatoriosFinanceiro;

                         $empresa= $_SESSION['empresa_principal'] == 1 ? $r->empresasRelatorios():
                         "<input type='hidden' id='empresa-relatorio' value='{$_SESSION['empresa_principal']}'>";
        echo $empresa;
        echo "           </p>
                         <p>";
                         $this->selectPlano();
                         echo "</p>
                         <p>";
                         $this->tipoDocumento();
                        echo "</p>
                         <p><div id='div-data'>";
                        echo $r->periodoMesAno();
            echo "</div></p>
            <button id='pesquisar-relatorio-margem-contribuicao' class='ui-button
                    ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'
                      role='button' aria-disabled='false' >
                      <span class='ui-button-text'>
                         Pesquisar
                      </span>
            </button>
            &nbsp;&nbsp;
         <button id='' type='button' class='voltar_ ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'
                    role='button' aria-disabled='false' >
                <span class='ui-button-text'>Voltar</span>
            </button>



                </div>
                <div id='resultado-pesquisa-rel-margem'></div>";

    }
    function selectPlano(){
       echo "<label for='convenio'><b>Planos:</b></label>
             <select name='convenio' id='convenio' class='COMBO_OBG' style='background-color:transparent;'>";
		echo "<option value='-1' selected >Todos</option>";
        $planos = PlanoSaude::getAll(false);

    foreach ($planos as $plano) {
      echo "<option value='{$plano['id']}'  >{$plano['nome']}</option>";
    }
		echo "</select>";

    }

     function selectPaciente()
    {

        $condEmpresa = $_SESSION['empresa_principal'] != 1 ? "WHERE c.empresa IN {$_SESSION['empresa_user']}" : "";
        $sql = "SELECT c.* FROM clientes as c {$condEmpresa} ORDER BY status, nome";
        $result = mysql_query($sql);

        echo "<p><b>Pacientes:</b></p>";
        echo "<select name='paciente' style='background-color:transparent;width:362px' id='selPaciente'>";
        echo "<option value='-1'></option>";
        $aux = 0;
        $paciente_status = array();
        $status =array('1'=>'::     Novo    ::','4'=>'::   Ativo/Autorizado   ::','6'=>'::     �bito    ::');
        while ($row = mysql_fetch_array($result))
        {
            $paciente = strtoupper($row['nome']);
            if(array_key_exists($row['status'], $paciente_status))
            {
                $pacientes_por_status = array('idCliente'=>$row['idClientes'],'nome'=>$paciente,'status'=>$row['status']);
                # Verifica se a chave existe dentro do array $status,
                # caso não esteja, ele adiciona ao código 7 para que
                # os outros possam ser ordenados juntos no select
                if(array_key_exists($row['status'], $status))
                {
                    array_push($paciente_status[$row['status']],$pacientes_por_status);
                }
                else
                {
                    if(!array_key_exists(7,$paciente_status))
                    {
                        $paciente_status[7] = array();
                    }
                    array_push($paciente_status[7],$pacientes_por_status);
                }

            }
            else
            {
                $paciente_status[$row['status']] = array();
                $pacientes_por_status = array('idCliente'=>$row['idClientes'],'nome'=>$paciente,'status'=>$row['status']);
                # Verifica se a chave existe dentro do array $status,
                # caso não esteja, ele adiciona ao código 7 para que
                # os outros possam ser ordenados juntos no select
                if(array_key_exists($row['status'], $status))
                {
                    array_push($paciente_status[$row['status']],$pacientes_por_status);
                }
                else
                {
                    if(!array_key_exists(7,$paciente_status))
                    {
                        $paciente_status[7] = array();
                    }
                    array_push($paciente_status[7],$pacientes_por_status);
                }

            }
        }


        $cont=0;
        asort($paciente_status);
        foreach($paciente_status as $chave=>$dados)
        {
            #Verifica se é a primeira vez que ta rodando
            if(array_key_exists($chave, $status) && $cont==0)
            {
                echo "<optgroup></optgroup>";
                echo "<optgroup label='{$status[$chave]}'>";
            }#Da segunda vez em diante passará por aqui
            elseif(array_key_exists($chave, $status) && $cont>0)
            {
                echo "</optgroup>";
                echo "<optgroup></optgroup>";
                echo "<optgroup label='{$status[$chave]}'>";
            }
            elseif($chave==7)
            {
                echo "</optgroup>";
                echo "<optgroup></optgroup>";
                echo "<optgroup label='::     Outros     ::'>";
            }
            foreach($dados as $chave2=>$dados2)
            {
                echo $chave;

                echo "<option value='{$dados2['idCliente']}' status='{$chave}'>{$dados2['nome']}</option>";
            }
            $cont++;
        }
        echo "</optgroup>";
        echo "</select>";

    }

    function tipoDocumento()
    {
        echo "<label for='tipo_documento' name='tipo_documento'><b>Tipo Documento:</b></label>
                  <select style='background-color:transparent;'  id='tipo_documento'>
                     <option value='-1' selected>Todos</option>
                     <option value='0' >Fatura</option>
                     <option value='1' >Orçamento Inicial</option>
                     <option value='2' >Orçamento de Prorrogação</option>
                     <option value='3' >Orçamento Aditivo</option>
                   </select>";
    }

}