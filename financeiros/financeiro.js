$(function($){
    
    $("#tipo-chave-pix").change(function(){
        
        var tipoChavePix = $(this).val();
        $("#chave-pix").removeClass('cpf').removeClass('cnpj')
                        .removeClass('celular').removeClass('email').removeClass('OBG').val('');
        
        if(tipoChavePix == 1){
            $("#chave-pix").removeAttr('disabled').addClass('cpf').addClass('OBG');
            $('.cpf').mask('999.999.999-99');
        }else if(tipoChavePix == 2){
            $("#chave-pix").removeAttr('disabled').addClass('cnpj').addClass('OBG');
            $('.cnpj').mask('99.999.999/9999-99');
        }else if(tipoChavePix == 3){
            $("#chave-pix").removeAttr('disabled').removeAttr('mask').addClass('email').addClass('OBG');
            $('.email').unmask();
        }else if(tipoChavePix == 4){
            $("#chave-pix").removeAttr('disabled').addClass('celular').addClass('OBG');
            $('.celular').mask('(99) 99999-9999');            
        }else{
            $("#chave-pix").attr('disabled',true);
        }

    });

    $('.cpf').live('blur', function(){
       
        var cpf = $(this).val();
        exp = /\.|\-/g
        cpf = cpf.toString().replace( exp, "" );

        var isValido = verificarCPF(cpf);
       
        if (! isValido){
            alert('CPF Invalido!');
            $(this).val('');
        }
            
    });

    $('.cnpj').live('blur',function(){
        
        var cnpj = $(this).val();
        var valida = new Array(6,5,4,3,2,9,8,7,6,5,4,3,2);
        var dig1= new Number;
        var dig2= new Number;

        exp = /\.|\-|\//g
        cnpj = cnpj.toString().replace( exp, "" );
        var digito = new Number(eval(cnpj.charAt(12)+cnpj.charAt(13)));

        for(i = 0; i<valida.length; i++){
            dig1 += (i>0? (cnpj.charAt(i-1)*valida[i]):0);
            dig2 += cnpj.charAt(i)*valida[i];
        }
        dig1 = (((dig1%11)<2)? 0:(11-(dig1%11)));
        dig2 = (((dig2%11)<2)? 0:(11-(dig2%11)));

        if(((dig1*10)+dig2) != digito){
            alert('CNPJ Invalido!');
            $(this).val('');
        }

    });

    $('.chosen-select').chosen({search_contains: true});

    $('.telefone').mask('(99) 9999-9999');

    $('.celular').mask('(99) 99999-9999');

    $(".mesAno").mask('99/9999');
    $(".mesAno").blur(function(){

    });

    $('form').bind("keypress", function(e) {
        if (e.keyCode == 13) {               
          e.preventDefault();
          return false;
        }
      });

    $(".valor-negativo").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.',
        allowNegative: true,
    });

    $('#origem-todas').click(function () {
        if($(this).is(":checked")) {
            $('#origem option:selected').removeAttr("selected");
            $('#origem').attr('disabled', true).trigger("chosen:updated");
        } else {
            $('#origem').removeAttr('disabled').trigger("chosen:updated");
        }
    });

    $('.mes-ano').mask('99/9999');

    $('.mes-ano').live('blur', function(){
        var datevalue = $(this).val();
        var done = false;

        if(datevalue == '__/____'){
            $(this).val('');
            return false;
        }

        if(datevalue != null && datevalue != ''){
            var tmp = datevalue.split('/');
            var month = tmp[0];
            var year = tmp[1];

            if (month >= 1 && month <= 12) {
                if (year >= 1900 && year <= 2999) {
                    done = true;
                } else {
                    alert('Não é um ano válido!');
                    $(this).val("").focus();
                }
            } else {
                alert('O mês deve estar entre 01 e 12!');
                $(this).val("").focus();
            }
        }
    });

    $('.mask-data').mask('99/99/9999');

    $('.mask-data').live('blur', function(){
        var datevalue = $(this).val();
        var done = false;

        if(datevalue == '__/__/____'){
            $(this).val('');
            return false;
        }

        if(datevalue != null && datevalue != ''){
            var tmp = datevalue.split('/');
            var day = tmp[0];
            var month = tmp[1];
            var year = tmp[2];
            var bisexto = year % 4 == 0;
            var fev_bisexto = month == 2 && bisexto;
            var days_month = {
                '01': 31,
                '02': !fev_bisexto ? 28 : 29,
                '03': 31,
                '04': 30,
                '05': 31,
                '06': 30,
                '07': 31,
                '08': 31,
                '09': 30,
                '10': 31,
                '11': 30,
                '12': 31
            };

            if(day >= 1 && day <= days_month[month]) {
                if (month >= 1 && month <= 12) {
                    if (year >= 1900 && year <= 2999) {
                        done = true;
                    } else {
                        alert('Não é um ano válido!');
                        $(this).val("").focus();
                    }
                } else {
                    alert('O mês deve estar entre 01 e 12!');
                    $(this).val("").focus();
                }
            } else {
                alert('Não é um dia válido!');
                $(this).val("").focus();
            }
        }
    });

    $("#voltar_").click(function(event) {
        if(confirm('Deseja realmente sair dessa página?')){
            event.preventDefault();
            history.back(1);
        }
        return false;

    });

    

    $("#fornecedor-precisa-classificar").change(function(){
        $("#div-fornecedor-classificacao").attr('hidden',true);
        $("#fornecedor-classificacao").removeClass('OBG');
        $("#fornecedor-classificacao").val('');
       console.log($(this).val());
        if($(this).val() == 'S'){
            $("#div-fornecedor-classificacao").removeAttr('hidden');
            $("#fornecedor-classificacao").addClass('OBG');
            
        }

    });

    $(".apenas-mes-ano").datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        showButtonCurrent: false,
        dateFormat: 'mm/yy',
        closeText:'Aplicar',
        onClose: function (dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
        },
        beforeShow: function( input ) {
            setTimeout(function () {
                $(input).datepicker("widget").find(".ui-datepicker-current").hide();
                var clearButton = $(input ).datepicker( "widget" ).find( ".ui-datepicker-close" );
                clearButton.unbind("click").bind("click",function(){$.datepicker._clearDate( input );});
            }, 1 );
        }
    });


    $("#adicionar-custo-extra").click(function(){

        if(validar_campos("div-adicionar-item")){
            var idItem = $("#sel-custo-extra").val();
            var item = $("#sel-custo-extra option:selected").html();

            var x = $(".qtd-custo-extra:last").attr('id') || 0;
            var cont = 0;

            if(x == 0){
                cont++;
            }else{
                var array = x.split('-');
            }

            var tr = "<tr class='tr-item-custo-extra' id-fatura-custo-extra=0 cancelar='N' custo-anterior='0' quantidade-anterior='0' >" +
                "<td id-custo-extra='"+idItem+"'><img align='left' class='remover-custo-extra'  style ='padding-right:4px;' src='../utils/delete_16x16.png' title='Remover' border='0'/>"+item+"</td><td><input type='number'  class='OBG qtd-custo-extra' id='tr-"+cont+"' /></td><td>R$ <input type='text' value='0' class='valor calcula-custo-extra'  /></td></tr>"

            $("#table-custo-extra").append(tr);
            $('#tr-'+cont).focus();
            $(".valor").priceFormat({
                prefix: '',
                centsSeparator: ',',
                thousandsSeparator: '.'
            });
            $("#sel-custo-extra").val('');
        }
        return false;

    });

    $('.remover-custo-extra').live('click',function(){
        $(this).parent().parent().remove();
        calculaCusto();
    });
    $('.cancelar-custo-extra').live('click',function(){
        $(this).parent().parent().attr('style','display:none');
        $(this).parent().parent().attr('cancelar','S');
        calculaCusto();
    });


    $("#mostrar-balancete").click(function(){
        var i = $("#inicio").val(); var f = $("#fim").val();
        var pendente = $("input[name='pendente']").attr("checked");
        var processada = $("input[name='processada']").attr("checked");
        var cancelada = $("input[name='cancelada']").attr("checked");
        $.post("query.php",{query: "balancete", inicio: i, fim: f, pendente: pendente, processada: processada, cancelada: cancelada },function(r){
            $("#balancete").html(r);
        });
    });

    $(".editparcela").click(function(){
        $("#dialog-editar-parcela").attr("parcela",$(this).attr("cod"));
        $("#dialog-editar-parcela input[name='valor']").val('');
        $("#dialog-editar-parcela input[name='vencimento']").val('');
        $("#dialog-editar-parcela").dialog("open");
        return;
    });

    // function validar_vencimento_real(vencimento,vencimentoReal){
    //     var arrayFeriadosNacionais = [
    //         '01/01',
    //         '19/04',
    //         '21/04',
    //         '01/05',
    //         '07/09',
    //         '12/10',
    //         '02/11',
    //         '15/11',
    //         '25/12'
    //     ];
    //     var novoVencimentoReal = vencimentoReal;
    //     if(vencimentoReal instanceof Date && !isNaN(vencimentoReal.valueOf())){
    //         novoVencimentoReal = vencimentoReal;
    //     }else{
    //         novoVencimentoReal = vencimento;
    //     }
        
        
    //     let isValid = false;
    //     if(novoVencimentoReal.getDay() == 0 || novoVencimentoReal.getDay() == 6 || arrayFeriadosNacionais.indexOf(moment(novoVencimentoReal).format('DD/MM')) != -1){
    //         while(isValid == false){
    //             if(novoVencimentoReal.getDay() == 0){           
    //                     novoVencimentoReal = new Date(novoVencimentoReal.setDate(novoVencimentoReal.getDate() + 1));               
    //                 }else if(novoVencimentoReal.getDay() == 6) {
    //                     novoVencimentoReal = new Date(novoVencimentoReal.setDate(novoVencimentoReal.getDate() + 2)); 
    //                 }  
    //                 if(arrayFeriadosNacionais.indexOf(moment(novoVencimentoReal).format('DD/MM')) != -1){
    //                     novoVencimentoReal = new Date(novoVencimentoReal.setDate(novoVencimentoReal.getDate() + 1));                    
    //                 }else{                    
    //                     break;
    //                 }

    //         }
    //     }
    //    /* if(vencimentoReal instanceof Date && !isNaN(vencimentoReal.valueOf())){
    //         if(vencimentoReal.getDay() == 0){           
    //             novoVencimentoReal = new Date(vencimentoReal.setDate(vencimentoReal.getDate() + 1));               
    //         }else if(novoVencimentoReal.getDay() == 6) {
    //             novoVencimentoReal = new Date(vencimentoReal.setDate(vencimentoReal.getDate() + 2)); 
    //         }            
            
    //     }else{
    //         novoVencimentoReal = vencimento;
    //         if(vencimento.getDay() == 0 ){        
    //             novoVencimentoReal = new Date(vencimento.setDate(vencimento.getDate() + 1));               
    //         } else if( vencimento.getDay() == 6){        
    //             novoVencimentoReal = new Date(vencimento.setDate(vencimento.getDate() + 2));               
    //         }   */         
    //     //}
        
    //     return novoVencimentoReal;


    // }
    $(".vencimento-parcela-editar").live('blur', function () {
        var $this = $(this);
        var data = $this.val();
        if(data == '__/__/____' || data == ''){
            return false;
        }
        
        var data_fechamento = new Date($("#data-fechamento").val() +' 01:00:00');
        var emissao = new Date($(".data-emissao-nota").val().split("/").reverse().join("-") +' 01:00:00');
        $(this).parent().parent().find('.vencimento-real-parcela-editar').val(data);
        var vencimento = new Date($this.val().split("/").reverse().join("-")+' 01:00:00');
        var vencimentoReal = new Date($this.closest('tr').find('.vencimento-real-parcela-editar').val().split("/").reverse().join("-")+' 01:00:00');
        vencimentoReal = validar_vencimento_real(vencimento, vencimentoReal);
        if(vencimento > vencimentoReal){
            $this.val('');
            alert("Vencimento não pode ser maior que Vencimento Real");
            return false;
        }
        if(vencimento < emissao || vencimentoReal < emissao) {
           
            vencimento < emissao ? $this.val("") : '';                
            vencimentoReal < emissao ? $this.closest('tr').find('.vencimento-real-parcela-editar').val(''):'';
            alert('Um ou mais vencimentos de parcelas são menores que a data de emissão da nota fiscal! Verifique também se a data de emissão da nota está correta!');

            return false;
        }
        if(vencimentoReal < data_fechamento) {
            
            vencimentoReal < data_fechamento ? $this.closest('tr').find('.vencimento-real-parcela-editar').val('') : '';
            vencimento < data_fechamento ? $this.val('') : '';
            alert('Um ou mais vencimentos reais das de parcelas são menores que a data de fechamento de Caixa! Verifique também se a data de emissão da nota está correta!');

            return false;
        }

        $this.closest('tr').find('.vencimento-real-parcela-editar').val(vencimentoReal.toLocaleDateString('pt-BR'));
    });

    $(".vencimento-parcela").live('blur', function () {
        var $this = $(this);
        var data = $this.val();
        if(data == '__/__/____' || data == ''){
            return false;
        }
        
        var data_fechamento = new Date($("#data-fechamento").val() +' 01:00:00');
        var emissao = new Date($(".data-emissao-nota").val().split("/").reverse().join("-") +' 01:00:00');
        $(this).parent().parent().find('.vencimento-real-parcela').val(data);
        var vencimento = new Date($this.val().split("/").reverse().join("-")+' 01:00:00');
        var vencimentoReal = new Date($this.closest('tr').find('.vencimento-real-parcela').val().split("/").reverse().join("-")+' 01:00:00');
        vencimentoReal = validar_vencimento_real(vencimento, vencimentoReal);
        if(vencimento > vencimentoReal){
            $this.val('');
            alert("Vencimento não pode ser maior que Vencimento Real");
            return false;
        }
        if(vencimento < emissao || vencimentoReal < emissao) {
           
            vencimento < emissao ? $this.val("") : '';                
            vencimentoReal < emissao ? $this.closest('tr').find('.vencimento-real-parcela').val(''):'';
            alert('Um ou mais vencimentos de parcelas são menores que a data de emissão da nota fiscal! Verifique também se a data de emissão da nota está correta!');

            return false;
        }
        if(vencimentoReal < data_fechamento) {
            
            vencimentoReal < data_fechamento ? $this.closest('tr').find('.vencimento-real-parcela').val('') : '';
            vencimento < data_fechamento ? $this.val('') : '';
            alert('Um ou mais vencimentos reais das de parcelas são menores que a data de fechamento de Caixa! Verifique também se a data de emissão da nota está correta!');

            return false;
        }

        $this.closest('tr').find('.vencimento-real-parcela').val(vencimentoReal.toLocaleDateString('pt-BR'));
    });


    $(".data-emissao-nota, .vencimento-real-parcela").live('change', function () {
       
        
        
        // foi adicionado naa data a hora 01:00:00 porque quando passa apenas a dat a hora fica 00:00:00 quando usa getDay() pega o dia anterior;
        var data_fechamento = new Date($("#data-fechamento").val() +' 01:00:00');
        var emissao = new Date($(".data-emissao-nota").val().split("/").reverse().join("-") +' 01:00:00');
        var isValidEmissao = true;
        var isValidFechamento = true;

        $(".vencimento-parcela").each(function () {


            var $this = $(this);
            if($this.val() == '__/__/____' || $this.val() == ''){
                return false;
            };
            var vencimento = new Date($this.val().split("/").reverse().join("-")+' 01:00:00');
            var vencimentoReal = new Date($this.closest('tr').find('.vencimento-real-parcela').val().split("/").reverse().join("-")+' 01:00:00');
            vencimentoReal = validar_vencimento_real(vencimento, vencimentoReal);
            if(vencimento == '__/__/____'){
            
                return false;
            };

            if(vencimento > vencimentoReal){
                $this.closest('tr').find('.vencimento-real-parcela').val('');
                alert("Vencimento não pode ser maior que Vencimento Real");
                return false;
            }
                          
            
                if(vencimentoReal < data_fechamento) {
                    isValidFechamento = !isValidFechamento;
                    vencimentoReal < data_fechamento ? $this.closest('tr').find('.vencimento-real-parcela').val('') : '';
                    vencimento < data_fechamento ? $this.val('') : '';
                    return false;
                }
            

            if(vencimento < emissao || vencimentoReal < emissao) {
                isValidEmissao = !isValidEmissao;
                vencimento < emissao ? $this.val("") : '';                
                vencimentoReal < emissao ? $this.closest('tr').find('.vencimento-real-parcela').val(''):'';
                return false;
            }
            $this.closest('tr').find('.vencimento-real-parcela').val(vencimentoReal.toLocaleDateString('pt-BR'));
            
        });
        

        if(!isValidEmissao){
            alert('Um ou mais vencimentos de parcelas são menores que a data de emissão da nota fiscal! Verifique também se a data de emissão da nota está correta!');
        }

        if(!isValidFechamento){
            alert('Um ou mais vencimentos de parcelas são menores que a data de fechamento do período! Verifique também se a data de emissão da nota está correta!');
        }
    

    });

    $(".vencimento-real-parcela-editar").live('change', function () {
       
        
        
        // foi adicionado naa data a hora 01:00:00 porque quando passa apenas a dat a hora fica 00:00:00 quando usa getDay() pega o dia anterior;
        var data_fechamento = new Date($("#data-fechamento").val() +' 01:00:00');
        var emissao = new Date($(".data-emissao-nota").val().split("/").reverse().join("-") +' 01:00:00');
        var isValidEmissao = true;
        var isValidFechamento = true;

       

            
            var $this = $(this);
            if($this.val() == '__/__/____' || $this.val() == ''){
                return false;
            };
            var vencimento = new Date($this.closest('tr').find('.vencimento-parcela').val().split("/").reverse().join("-")+' 01:00:00');
            var vencimentoReal = new Date($this.val().split("/").reverse().join("-")+' 01:00:00');
            vencimentoReal = validar_vencimento_real(vencimento, vencimentoReal);
            if(vencimento == '__/__/____'){
            
                return false;
            };

            if(vencimento > vencimentoReal){
                $this.val('');
                alert("Vencimento não pode ser maior que Vencimento Real");
                return false;
            }
                          
            
                if(vencimentoReal < data_fechamento) {
                    isValidFechamento = !isValidFechamento;
                    vencimentoReal < data_fechamento ? $this.val('') : '';
                    vencimento < data_fechamento ? $this.closest('tr').find('.vencimento-parcela').val('') : '';
                    return false;
                }
            

            if(vencimento < emissao || vencimentoReal < emissao) {
                isValidEmissao = !isValidEmissao;
                vencimento < emissao ? $this.closest('tr').find('.vencimento-parcela').val("") : '';                
                vencimentoReal < emissao ? $this.val(''):'';
                return false;
            }

            $this.val(vencimentoReal.toLocaleDateString('pt-BR'));          
       
        

        if(!isValidEmissao){
            alert('Um ou mais vencimentos de parcelas são menores que a data de emissão da nota fiscal! Verifique também se a data de emissão da nota está correta!');
        }

        if(!isValidFechamento){
            alert('Um ou mais vencimentos de parcelas são menores que a data de fechamento do período! Verifique também se a data de emissão da nota está correta!');
        }
    

    });


    $(".tipo-cadastro").change( function () {
        var $this = $(this);
        var $ipcc = $("#ipcc");

        if($this.val() == 'F') {
            $ipcc.val('896');
        }

        if($this.val() == 'C') {
            $ipcc.val('613');
        }

        $ipcc.trigger("chosen:updated");
    });

    $("#dialog-editar-parcela").dialog({
        autoOpen: false,modal: true,
        buttons: {
            Ok: function() {
                if(validar_campos("dialog-editar-parcela")){
                    $.post("query.php",{query: "editar_parcela", parcela: $( "#dialog-editar-parcela" ).attr("parcela"),
                            valor: $("#dialog-editar-parcela input[name='valor']").val(),
                            vencimento: $("#dialog-editar-parcela input[name='vencimento']").val()},
                        function(retorno){
                            alert(retorno);
                        });
                    $( this ).dialog( "close" );
                    location.reload();
                }
            },
            Cancelar: function(){
                $( this ).dialog( "close" );
            }
        }
    });

    $(".div-aviso").hide();

    function validar_campos_selectize(div){
        var ok = true;
        $("#"+div+" .OBG-SELECTIZE").each(function (i){
            if($(this).parent().has('div.selectize-input.invalid')){
                ok = false;
                $(this).parent().find('.selectize-input').css('border', '3px solid red');
            } else if($(this).parent().has('div.selectize-input.full')){
                $(this).parent().find('.selectize-input').removeAttr('style');
            }
        });
        return ok;
    }

    function validar_campos(div){
        var ok = true;
        $("#"+div+" .OBG").each(function (i){
            if(this.value == ""){
                ok = false;
                console.log(this);
                this.style.border = "3px solid red";
            } else {
                this.style.border = "";
            }
        });
        $("#"+div+" .COMBO_OBG").each(function (i){
            if(this.value == "" || this.value == "-1" ){
                ok = false;
                this.style.border = "3px solid red";
                console.log(this);
            } else {
                this.style.border = "";
            }
        });
        $("#"+div+" .COMBO_OBG1 option:selected").each(function (i){

            if(this.value == "-1"){
                console.log(this);
                ok = false;
                $(this).parent().parent().children('div').css({"border":"3px solid red"});
            } else {
                $(this).parent().parent().parent().children('div').css({"border":""});

            }
        });
        $("#"+div+" .valor.OBG").each(function (i){
            if(this.value == "0,00"){
                console.log(this);
                ok = false;
                this.style.border = "3px solid red";
            } else {
                this.style.border = "";
            }
        });

        $("#" + div + " .OBG_RADIO").each(function(i) {
            //if (this.value == "-1") {
            classe = $(this).attr("name");


            classe_aux = "";
            aux = 0;

            if(classe != classe_aux){
                classe_aux = classe;
                aux = 0;
                if ($("input[name='"+classe+"']").is(':checked'))    {
                    aux = 1;
                }

                if(aux == 0){
                    $("input[name='"+classe+"']").parent().css("border","3px solid red");
                    ok = false;
                }else{
                    $("input[name='"+classe+"']").parent().css("border","");
                }
            }else{
                if ($("input[name='"+classe+"']").is(':checked'))    {
                    aux = 1;
                }

                if(aux == 0){
                    $("input[name='"+classe+"']").parent().css("border","3px solid red");
                    ok = false;
                }else{
                    $("input[name='"+classe+"']").parent().css("border","");
                }
            }
        });

        $("#"+div+" .OBG_CHOSEN option:selected").each(function (){
            if(this.value == "" || this.value == -1){
                console.log($(this));
                ok = false;
                $(this).parent().parent().children('div').css({"border":"3px solid red"});
            } else {
                var estilo = $(this).parent().parent().children('div').attr('style');
                $(this).parent().parent().children('div').removeAttr('style');
                $(this).parent().parent().children('div').attr('style', estilo.replace("border: 3px solid red",''));
            }
        });
        if(!ok){
            $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
            $("#"+div+" .div-aviso").show();
        } else {
            $("#"+div+" .aviso").text("");
            $("#"+div+" .div-aviso").hide();
        }





        return ok;
    }

    function inserir_parcelas(nota){
        $("#parcelas tr").each(function (i){
            if(i > 1){ //0-tr do thead e 1:tr do tfoot
                var valor = $(this).children("td").eq(0).children("input").val();
                var vencimento = $(this).children("td").eq(1).children("input").val();
                var barras = $(this).children("td").eq(2).children("input").val();
                $.post("query.php",{query: "salvar_parcela", nota: nota, valor: valor, vencimento: vencimento, barras: barras},
                    function(retorno){
//		 		alert(retorno);
                        if(retorno == "-1") alert("Algumas parcelas podem não foram inseridas completamente. Tente inserir separadamente.");
                    });
            }
        });
    }

    $("#dialog-add-parcela").dialog({
        minWidth: 500,minHeight: 300,autoOpen: false,modal: true,
        buttons: {
            Ok: function() {
                if(validar_campos("dialog-add-parcela")){
                    $.post("query.php",{query: "salvar_parcela", nota: $( "#dialog-add-parcela" ).attr("nota"), valor: $("#valor").val(),
                            vencimento: $("#vencimento").val(), barras: $("#codBarras").val()},
                        function(retorno){
                            alert(retorno);
                        });
                    $( this ).dialog( "close" );
                    location.reload();
                }
            },
            Cancelar: function(){
                $( this ).dialog( "close" );
            }
        }
    });

    $("#dialog-add-parcela").dialog({
        minWidth: 500,minHeight: 300,autoOpen: false,modal: true,
        buttons: {
            Ok: function() {
                if(validar_campos("dialog-add-parcela")){
                    $.post("query.php",{query: "salvar_parcela", nota: $( "#dialog-add-parcela" ).attr("nota"), valor: $("#valor").val(),
                            vencimento: $("#vencimento").val(), barras: $("#codBarras").val()},
                        function(retorno){
                            alert(retorno);
                        });
                    $( this ).dialog( "close" );
                    location.reload();
                }
            },
            Cancelar: function(){
                $( this ).dialog( "close" );
            }
        }
    });

    $("#add-parcela").live('click', function(){
        $("#dialog-add-parcela").dialog("open");
        return false;
    });
    $("#dialog-message").dialog('close');

    $("#dialog-listar-naturezas").dialog({
        minWidth: 700, minHeight: 450, autoOpen: false, modal: true,
        buttons: {
            Cancelar: function(){
                $("#filtrar-natureza").val('');
                $( this ).dialog( "close" );
            }
        }
    });

    $("#buscar-naturezas-dialog").click(function(){
        var tipo_nota = $('.tipo_nota:checked').val();
        $(".escolher-natureza").show();
        if(tipo_nota == '1') {
            $(".escolher-natureza[tipo='E']").each(function () {
                $(this).hide();
            });
        } else if(tipo_nota == '0') {
            $(".escolher-natureza[tipo='S']").each(function () {
                $(this).hide();
            });
        }
        $("#dialog-listar-naturezas").dialog("open");
        $("#dialog-listar-naturezas").attr('tipo','principal');
        return false;
    });

    $(".buscar-naturezas-dialog-rateio").live('click', function(){
        var tipo_nota = $('.tipo_nota:checked').val();
        var $this =  $(this);
       
        $(".escolher-natureza").show();
        if(tipo_nota == '1') {
            $(".escolher-natureza[tipo='E']").each(function () {
                $(this).hide();
            });
        } else if(tipo_nota == '0') {
            $(".escolher-natureza[tipo='S']").each(function () {
                $(this).hide();
            });
        }
        $("#dialog-listar-naturezas").attr('tipo','');
        $("#dialog-listar-naturezas").data('tipo', $this).dialog("open");
        
        
        return false;
    });

    $('#filtrar-natureza').live('keyup', function(event) {
        var text = $(this).val().toUpperCase();
        var tipo_nota = $('.tipo_nota:checked').val() == '1' ? 'S' : 'E';
        $('.escolher-natureza[tipo="' + tipo_nota + '"]').hide();
        if(text != '') {
            $('.escolher-natureza[tipo="' + tipo_nota + '"] .desc-natureza:contains(' + text + ')').parent().show();
        } else {
            $('.escolher-natureza[tipo="' + tipo_nota + '"]').show();
        }
    });

    $(".escolher-natureza").click(function () {
        var $this = $(this);
        var natureza = JSON.parse($this.attr('json-natureza'));
       
      

        if($("#dialog-listar-naturezas").attr('tipo') == 'principal'){
            var $naturezaMovElem = $("#natureza-movimentacao");
            var $codigoContaElem = $("#codigo-conta");           
            $codigoContaElem.val(natureza.COD_N4.split('.').join(''));
            $naturezaMovElem.val(natureza.ID);
            $naturezaMovElem.attr('json-natureza', $this.attr('json-natureza'));
            $(".nome-natureza").text(natureza.COD_N4 + ' - ' + natureza.N4);
            if($('#rateio').find('.id-natureza-rateio').length == 1){
                $('#rateio').find(".nome-natureza-rateio").val(natureza.COD_N4 + ' - ' + natureza.N4);
                $('#rateio').find(".id-natureza-rateio").val(natureza.ID);

            }

        if($("#issqn").attr('aliquota') == '') {
            if (typeof natureza.ISS != 'undefined') {
                $("#issqn").attr('aliquota', natureza.ISS);
                $("#iss-texto-aliquota").text('(' + natureza.ISS.toString().replace('.', ',') + '%)');
            }
        }
        if($("#ir").attr('aliquota') == '') {
            if (typeof natureza.IR != 'undefined') {
                $("#ir").attr('aliquota', natureza.IR);
                $("#ir-texto-aliquota").text('(' + natureza.IR.toString().replace('.', ',') + '%)');
            }
        }
        if($("#ipi").attr('aliquota') == '') {
            if (typeof natureza.IPI != 'undefined') {
                $("#ipi").attr('aliquota', natureza.IPI);
                $("#ipi-texto-aliquota").text('(' + natureza.IPI.toString().replace('.', ',') + '%)');
            }
        }
        if($("#icms").attr('aliquota') == '') {
            if (typeof natureza.ICMS != 'undefined') {
                $("#icms").attr('aliquota', natureza.ICMS);
                $("#icms-texto-aliquota").text('(' + natureza.ICMS.toString().replace('.', ',') + '%)');
            }
        }
        if($("#pis").attr('aliquota') == '') {
            if (typeof natureza.PIS != 'undefined') {
                $("#pis").attr('aliquota', natureza.PIS);
                $("#pis-texto-aliquota").text('(' + natureza.PIS.toString().replace('.', ',') + '%)');
            }
        }
        if($("#cofins").attr('aliquota') == '') {
            if (typeof natureza.COFINS != 'undefined') {
                $("#cofins").attr('aliquota', natureza.COFINS);
                $("#cofins-texto-aliquota").text('(' + natureza.COFINS.toString().replace('.', ',') + '%)');
            }
        }
        if($("#csll").attr('aliquota') == '') {
            if (typeof natureza.CSLL != 'undefined') {
                $("#csll").attr('aliquota', natureza.CSLL);
                $("#csll-texto-aliquota").text('(' + natureza.CSLL.toString().replace('.', ',') + '%)');
            }
        }
        if($("#inss").attr('aliquota') == '') {
            if (typeof natureza.INSS != 'undefined') {
                $("#inss").attr('aliquota', natureza.INSS);
                $("#inss-texto-aliquota").text('(' + natureza.INSS.toString().replace('.', ',') + '%)');
            }
        }
        

    }else{
                 
      
        var element =   $("#dialog-listar-naturezas").data('tipo');
               
        element.parent('td').find(".nome-natureza-rateio").val(natureza.COD_N4 + ' - ' + natureza.N4);
        element.parent('td').find(".id-natureza-rateio").val(natureza.ID);
    }
    $("#dialog-listar-naturezas").attr('tipo','');
        $("#dialog-listar-naturezas").dialog( "close" );
        $("#filtrar-natureza").val('');
    });

    $('#codigo-conta').blur(function () {
        var conta_id = $(this).val();
        var regOnlyNum = /^(\d+){6}$/;

        if(conta_id != 0 || conta_id != '') {
            if(regOnlyNum.test(conta_id)) {
                $.ajax({
                    type: "GET",
                    url: "query.php",
                    data: {
                        query: 'buscar-conta-financeira-id',
                        conta_id: conta_id
                    },
                    success: function (response) {
                        if (response != 'false') {
                            $("#natureza-movimentacao").attr('json-natureza', response);
                            response = JSON.parse(response);
                            $(".nome-natureza").text(response.COD_N4 + ' - ' + response.N4);
                            $("#natureza-movimentacao").val(response.ID);
                            $("#natureza-movimentacao").attr(response.ID);
                            if($("#issqn").attr('aliquota') == '') {
                                if (typeof response.ISS != 'undefined') {
                                    $("#issqn").attr('aliquota', response.ISS);
                                    $("#iss-texto-aliquota").text('(' + response.ISS.toString().replace('.', ',') + '%)');
                                }
                            }
                            if($("#ir").attr('aliquota') == '') {
                                if (typeof response.IR != 'undefined') {
                                    $("#ir").attr('aliquota', response.IR);
                                    $("#ir-texto-aliquota").text('(' + response.IR.toString().replace('.', ',') + '%)');
                                }
                            }
                            if($("#ipi").attr('aliquota') == '') {
                                if (typeof response.IPI != 'undefined') {
                                    $("#ipi").attr('aliquota', response.IPI);
                                    $("#ipi-texto-aliquota").text('(' + response.IPI.toString().replace('.', ',') + '%)');
                                }
                            }
                            if($("#icms").attr('aliquota') == '') {
                                if (typeof response.ICMS != 'undefined') {
                                    $("#icms").attr('aliquota', response.ICMS);
                                    $("#icms-texto-aliquota").text('(' + response.ICMS.toString().replace('.', ',') + '%)');
                                }
                            }
                            if($("#pis").attr('aliquota') == '') {
                                if (typeof response.PIS != 'undefined') {
                                    $("#pis").attr('aliquota', response.PIS);
                                    $("#pis-texto-aliquota").text('(' + response.PIS.toString().replace('.', ',') + '%)');
                                }
                            }
                            if($("#cofins").attr('aliquota') == '') {
                                if (typeof response.COFINS != 'undefined') {
                                    $("#cofins").attr('aliquota', response.COFINS);
                                    $("#cofins-texto-aliquota").text('(' + response.COFINS.toString().replace('.', ',') + '%)');
                                }
                            }
                            if($("#csll").attr('aliquota') == '') {
                                if (typeof response.CSLL != 'undefined') {
                                    $("#csll").attr('aliquota', response.CSLL);
                                    $("#csll-texto-aliquota").text('(' + response.CSLL.toString().replace('.', ',') + '%)');
                                }
                            }
                            if($("#inss").attr('aliquota') == '') {
                                if (typeof response.INSS != 'undefined') {
                                    $("#inss").attr('aliquota', response.INSS);
                                    $("#inss-texto-aliquota").text('(' + response.INSS.toString().replace('.', ',') + '%)');
                                }
                            }

                        } else {
                            alert('Este ID não corresponde a nenhuma conta! Tente novamente.');
                            $('#natureza-movimentacao').val('');
                            $('#codigo-conta').val('');
                        }
                    }
                });
            } else {
                alert('O código da conta deve ter 5 digitos!');
                $('#natureza-movimentacao').val('');
                $('#codigo-conta').val('');
            }
        }
    });

    $(".codigo-barra").blur(function(){
        var $this = $(this);
        var codigoBarras = $this.val();
        if(codigoBarras != '') {
            if(codigoBarras.length != 44 && codigoBarras.length != 47 && codigoBarras.length != 48) {
                alert('O tamanho do código de barras está inválido! Tamanho atual: ' + codigoBarras.length);
                $this.val('');
                $this.focus();
                return false;
            }
        }
    });

    $("#inserir_nota").click(function(){
        
       

        var data_fechamento = moment($("#data-fechamento").val());
        var tipo = $("input[name='tipo']:checked").val();
        var val_rateio=0;
        var val_parcela=0;
        var val_porcento=0;
        var rateio = '';
        var parcela='';
        var nota_tipo_documento_sigla = $("select[name='notas_tipo_documento'] option:selected").attr('sigla_tipo_documento');

        var compromisso_iss = true;
        var vencimento_retencao_iss = true;
        var vencimento_formated = '';
        if($("#issqn").val() != '0,00') {
            if($("input[name='valor_issqn']").val() == '0,00' && $("input[name='vencimento_issqn']").val() == '') {
                compromisso_iss = false;
            }
            vencimento_formated = $("input[name='vencimento_issqn']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_issqn']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_iss = false;
            }
        }

        var compromisso_ir = true;
        var vencimento_retencao_ir = true;
        if($("#ir").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_ir']").val() == '0,00' && $("input[name='vencimento_ir']").val() == '') {
                compromisso_ir = false;
            }
            vencimento_formated = $("input[name='vencimento_ir']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_ir']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_ir = false;
            }
        }

        var compromisso_pis = true;
        var vencimento_retencao_pis = true;
        if($("#pis").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_pis']").val() == '0,00' && $("input[name='vencimento_pis']").val() == '') {
                compromisso_pis = false;
            }
            vencimento_formated = $("input[name='vencimento_pis']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_pis']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_pis = false;
            }
        }

        var compromisso_cofins = true;
        var vencimento_retencao_cofins = true;
        if($("#cofins").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_cofins']").val() == '0,00' && $("input[name='vencimento_cofins']").val() == '') {
                compromisso_cofins = false;
            }
            vencimento_formated = $("input[name='vencimento_cofins']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_cofins']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_cofins = false;
            }
        }

        var compromisso_csll = true;
        var vencimento_retencao_csll = true;
        if($("#csll").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_csll']").val() == '0,00' && $("input[name='vencimento_csll']").val() == '') {
                compromisso_csll = false;
            }
            vencimento_formated = $("input[name='vencimento_csll']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_csll']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_csll = false;
            }
        }

        var compromisso_pcc = true;
        var vencimento_retencao_pcc = true;
        if($("#pcc").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_pcc']").val() == '0,00' && $("input[name='vencimento_pcc']").val() == '') {
                compromisso_pcc = false;
            }
            vencimento_formated = $("input[name='vencimento_pcc']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_pcc']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_pcc = false;
            }
        }

        var compromisso_inss = true;
        var vencimento_retencao_inss = true;
        if($("#inss").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_inss']").val() == '0,00' && $("input[name='vencimento_inss']").val() == '') {
                compromisso_inss = false;
            }
            vencimento_formated = $("input[name='vencimento_inss']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_inss']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_inss = false;
            }
        }
        
        if(!validar_campos("div-nova-nota")){
            return false;
        }

        if(
            (
                !compromisso_iss ||
                !compromisso_ir ||
                !compromisso_pis ||
                !compromisso_cofins ||
                !compromisso_csll ||
                !compromisso_pcc ||
                !compromisso_inss
            ) &&
            tipo == '1'
        ) {
            alert('Alguns compromissos de retenções não foram devidamente preenchidos! ' +
                  'Clique no botão "Calcular Vencimentos" para que o sistema os calcule corretamente!');
            $("#calcular-vencimentos").focus();
            return false;
        }

        var data_emissao_nota = moment($('.data-emissao-nota').val().split('/').reverse().join('-'));
        var vencimento_emissao_nota = data_emissao_nota.isSameOrBefore(data_fechamento);

        if( !vencimento_retencao_iss ||
            !vencimento_retencao_ir ||
            !vencimento_retencao_pis ||
            !vencimento_retencao_cofins ||
            !vencimento_retencao_csll ||
            !vencimento_retencao_pcc ||
            !compromisso_inss
        ) {
            alert('Alguns compromissos de retenções tem o vencimento menor que a data de fechamento do período (' + data_fechamento.format('DD/MM/YYYY') + ')! ' +
                'Verifique-os para que sejam posterior!');
            $("#calcular-vencimentos").focus();
            return false;
        }

        if(vencimento_emissao_nota) {
            alert('A data da emissão da nota é menor que a data  de fechamento do período (' + data_fechamento.format('DD/MM/YYYY') + ')!');
            $(".data-emissao-nota").focus();
            return false;
        }

        var dados = $("form").serialize();
        tot = parseFloat(replaceValorMonetarioFloat($("#valor_nota").val()));
        var valor_bruto =parseFloat(replaceValorMonetarioFloat($("#val_produtos").val()));

        //if(tipo==1){
        $(".valor_rateio").each(function(){
            val_rateio += parseFloat(replaceValorMonetarioFloat($(this).val()));
        });

        $(".porcento").each(function(){
            val_porcento += parseFloat($(this).attr('valor_real'));
        });
        //}

        $(".valor_parcela").each(function(){
            val_parcela += parseFloat(replaceValorMonetarioFloat($(this).val()));
        });

        if(tot == '0.00' || tot == '0' ){
            alert("Valor da nota n&aatilde;o pode ser 0,00");
            return false;
        }
        if( val_parcela.toFixed(2) != tot){
            alert("Valor da soma da(s) parcela(s) é diferente  do valor da nota");
            return false;
        }
        

        var tipo_calculo_rateio = $(".tipo-calculo-rateio:checked").val();
        var valor_calculo = tipo_calculo_rateio == '0' ? tot : valor_bruto;
       
        if( val_rateio.toFixed(2) != valor_calculo){
            alert("Valor da soma do rateio não pode ser diferente que o valor da nota");
            return false;
        }

        if( val_porcento.toFixed(2) != parseFloat(100)){
            alert("A soma da porcentagem do rateio deve ser 100%");
            return false;
        }

        //}
        var x= $(".rateio").size();
        var x2= $(".parcela").size();
        var isValidVencimento = true;
        var isValidCodBarras = true;

        $(".parcela").each(function(i){           

            var trp= $(this).children('td').eq(1).children('input').val();
            var data= $(this).children('td').eq(2).children('input').val();
            var vencimento_real = $(this).children('td').eq(3).children('input').val();
           
            var valor= $(this).children('td').eq(4).children('input').val();
            var cod_barra= $(this).children('td').eq(5).children('input').val();

            if(cod_barra != '') {
                if(cod_barra.length != 44 && cod_barra.length != 47 && cod_barra.length != 48) {
                    isValidCodBarras = false;
                    return;
                }
            }
            
            parcela += trp+'#'+data+'#'+cod_barra+'#'+valor+'#'+vencimento_real;

            if(i< x2 - 1){
                parcela += "$";
            }
            var vencimento = new Date(data.split("/").reverse().join("-"));
            var emissao = new Date($('.data-emissao-nota').val().split("/").reverse().join("-"));
                
            if(vencimento < emissao) {
                isValidVencimento = !isValidVencimento;
                $(this).children('td').eq(2).children('input').val('');
                return false;
            }

        });

        if(isValidCodBarras == false){
            alert('O tamanho do código de barras não é válido!');
            return false;
        }

        if(isValidVencimento == false){
            alert('A data de vencimento não pode ser menor que a data de emissão.');
            return false;
        }

        //if(tipo==1){
        $(".rateio").each(function(i){
            var ur = $(this).children('td').eq(0).children('select').val();
            var valor = $(this).children('td').eq(1).children('input').val();
            var porcento = $(this).children('td').eq(2).children('input').attr('valor_real');
            var ipcf2 = '';
            var ipcg3 = '';
            var ipcg4 = $(this).children('td').eq(3).find('.id-natureza-rateio').val();
            var assinatura = $(this).children('td').eq(4).children('select').val();

            rateio += ur+'#'+valor+'#'+porcento+'#'+ipcg3+'#'+ipcg4+'#'+assinatura+'#'+ipcf2;

            if(i< x - 1){
                rateio += "$";
            }

        });

        var imposto_retido='';
        var cont_imposto=$(".imposto_retido").size();
        $(".imposto_retido").each(function(i){
            var valor = $(this).children('td').eq(4).children('input').val();
            valor= valor.replace('.','').replace(',','.');
            if(valor >0){
                
                var tipo= $(this).children('td').eq(1).children('select').children('option:selected').attr('sigla_tipo_documento');
                var tipo_doc_fin_id = $(this).children('td').eq(1).children('select').val();
               
                var cod = $(this).children('td').eq(2).children('input').val();
                var vencimento = $(this).children('td').eq(3).children('input').val();
                var fornecedor = $(this).children('td').eq(5).children('select').val();
                var ipcf4 = $(this).children('td').eq(4).children('input').attr('ipcf4');
                var id_imposto_retido = $(this).attr('id_imposto_retido');

                imposto_retido += tipo+'#'+cod+'#'+vencimento+'#'+valor+'#'+fornecedor+'#'+ipcf4+'#'+id_imposto_retido+'#'+tipo_doc_fin_id;

                if(i< cont_imposto- 1){
                    imposto_retido += "$";
                }
            }

        });

        var nota = "query=inserir_nota&" + dados +
            "&parcela=" + parcela +
            "&rateio=" + rateio+
            "&imposto_retido="+imposto_retido+
            "&nota_tipo_documento_sigla="+nota_tipo_documento_sigla+
            "&tipo_calculo="+tipo_calculo_rateio;

        $.ajax({
            type: "POST",
            url: "query.php",
            data: nota,
            success: function(msg){

                if(msg > 0){
                    $("#trGerada").html('<p>Conta salva com Sucesso!</p><p>TR ' + msg + '</p>');
                    $("#dialog-message").dialog('open');

                }else{
                    alert(msg);
                }
            }
        });


        return false ;
    });

    $("#inserir_nota_edt").click(function(){
        var data_fechamento = moment($("#data-fechamento").val());
        var val_rateio=0;
        var val_parcela=0;
        var val_porcento=0;
        var rateio = '';
        var parcela='';
        var tipo = $("input[name='tipo']:checked").val();
        var nota_tipo_documento_sigla = $("select[name='notas_tipo_documento'] option:selected").attr('sigla_tipo_documento');

        if(!validar_campos("div-nova-nota")){
            return false;
        }

        var compromisso_iss = true;
        var vencimento_retencao_iss = true;
        var vencimento_formated = '';
        if($("#issqn").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_issqn']").val() == '0,00' && $("input[name='vencimento_issqn']").val() == '') {
                compromisso_iss = false;
            }
            vencimento_formated = $("input[name='vencimento_issqn']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_issqn']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_iss = false;
            }
        }

        var compromisso_ir = true;
        var vencimento_retencao_ir = true;
        if($("#ir").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_ir']").val() == '0,00' && $("input[name='vencimento_ir']").val() == '') {
                compromisso_ir = false;
            }
            vencimento_formated = $("input[name='vencimento_ir']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_ir']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_ir = false;
            }
        }

        var compromisso_pis = true;
        var vencimento_retencao_pis = true;
        if($("#pis").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_pis']").val() == '0,00' && $("input[name='vencimento_pis']").val() == '') {
                compromisso_pis = false;
            }
            vencimento_formated = $("input[name='vencimento_pis']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_pis']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_pis = false;
            }
        }

        var compromisso_cofins = true;
        var vencimento_retencao_cofins = true;
        if($("#cofins").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_cofins']").val() == '0,00' && $("input[name='vencimento_cofins']").val() == '') {
                compromisso_cofins = false;
            }
            vencimento_formated = $("input[name='vencimento_cofins']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_cofins']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_cofins = false;
            }
        }

        var compromisso_csll = true;
        var vencimento_retencao_csll = true;
        if($("#csll").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_csll']").val() == '0,00' && $("input[name='vencimento_csll']").val() == '') {
                compromisso_csll = false;
            }
            vencimento_formated = $("input[name='vencimento_csll']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_csll']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_csll = false;
            }
        }

        var compromisso_pcc = true;
        var vencimento_retencao_pcc = true;
        if($("#pcc").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_pcc']").val() == '0,00' && $("input[name='vencimento_pcc']").val() == '') {
                compromisso_pcc = false;
            }
            vencimento_formated = $("input[name='vencimento_pcc']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_pcc']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_pcc = false;
            }
        }

        var compromisso_inss = true;
        var vencimento_retencao_inss = true;
        if($("#inss").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_inss']").val() == '0,00' && $("input[name='vencimento_inss']").val() == '') {
                compromisso_inss = false;
            }
            vencimento_formated = $("input[name='vencimento_inss']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_inss']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_inss = false;
            }
        }

        if(!validar_campos("div-nova-nota")){
            return false;
        }

        if(
            (
                !compromisso_iss ||
                !compromisso_ir ||
                !compromisso_pis ||
                !compromisso_cofins ||
                !compromisso_csll ||
                !compromisso_pcc ||
                !compromisso_inss
            ) && tipo == 1
        ) {
            alert('Alguns compromissos de retenções não foram devidamente preenchidos! ' +
                'Clique no botão "Calcular Vencimentos" para que o sistema os calcule corretamente!');
            $("#calcular-vencimentos").focus();
            return false;
        }

        var data_emissao_nota = moment($('.data-emissao-nota').val().split('/').reverse().join('-'));
        var data_emissao_nota_original = moment($('.data-emissao-nota-original').val().split('/').reverse().join('-'));
        var vencimento_emissao_nota = data_emissao_nota.isSameOrBefore(data_fechamento);

        if( !vencimento_retencao_iss ||
            !vencimento_retencao_ir ||
            !vencimento_retencao_pis ||
            !vencimento_retencao_cofins ||
            !vencimento_retencao_csll ||
            !vencimento_retencao_pcc ||
            !compromisso_inss
         ) {
            alert('Alguns compromissos de retenções tem o vencimento menor que a data de fechamento do período (' + data_fechamento.format('DD/MM/YYYY') + ')! ' +
                'Verifique-os para que sejam posterior!');
            $("#calcular-vencimentos").focus();
            return false;
        }

        if(data_emissao_nota_original.format('dmY') != data_emissao_nota.format('dmY') && vencimento_emissao_nota) {
            alert('A data da emissão da nota é menor que a data de fechamento do período (' + data_fechamento.format('DD/MM/YYYY') + ')!');
            $(".data-emissao-nota").focus();
            return false;
        }
       
        var dados = $("form").serialize();
        var tot =parseFloat(replaceValorMonetarioFloat($("#valor_nota").val()));
        var valor_bruto = parseFloat(replaceValorMonetarioFloat($("#val_produtos").val()));
        //if(tipo == 1){
        $(".valor_rateio").each(function(){
            val_rateio += parseFloat(replaceValorMonetarioFloat($(this).val()));
        });
        //}
        $(".valor_parcela").each(function(){
            val_parcela += parseFloat(replaceValorMonetarioFloat($(this).val()));
        });
        $(".porcento").each(function(){
            val_porcento += parseFloat($(this).attr('valor_real'));
        });

        if(tot == '0.00' || tot == '0' ){
            alert("Valor do lan&ccedil;amento n&aatilde;o pode ser 0,00");
            return false;
        }


        if( val_parcela.toFixed(2) != tot){
            alert("Valor da soma da(s) parcela(s) menor que o valor total.");
            return false;
        }
        //if(tipo==1){
        var tipo_calculo_rateio = $(".tipo-calculo-rateio:checked").val();
        var valor_calculo = tipo_calculo_rateio == '0' ? tot : valor_bruto;
        if( val_rateio.toFixed(2) != valor_calculo){
            alert("Valor da soma do rateio não pode ser diferente que o valor da nota");
            return false;
        }

        if( val_porcento.toFixed(2) != 100){
            alert("A soma da porcentagem do rateio deve ser 100%");
            return false;
        }
        //}
        var x= $(".rateio").size();
        var x2= $(".parcela").size();
        var isValidVencimento = true;


        $(".parcela").each(function(i){
            var st = 'Pendente';

            var trp= $(this).children('td').eq(1).children('input').val();
            var data= $(this).children('td').eq(2).children('input').val();
            var cod_barra= $(this).children('td').eq(5).children('input').val();;
            var valor= $(this).children('td').eq(4).children('input').val();
            var vencimento_real = $(this).children('td').eq(3).children('input').val();
            var id_parcela = $(this).attr('data-id-parcela');
            var data_editar_parcela = $(this).attr('data-editar-parcela');
            var status_parcela = $(this).attr('status');
            parcela += trp+'#'+data+'#'+cod_barra+'#'+valor+'#'+st+'#'+id_parcela+'#'+data_editar_parcela+'#'+vencimento_real;

            if(i< x2 - 1){
                parcela += "$";
            }
            var vencimento = new Date(data.split("/").reverse().join("-"));
            var emissao = new Date($('.data-emissao-nota').val().split("/").reverse().join("-"));
              var vencimento_real  = new Date(vencimento_real.split("/").reverse().join("-"));
            if(vencimento < emissao || vencimento_real< emissao) {
                isValidVencimento = !isValidVencimento;
                $(this).children('td').eq(2).children('input').val('');
                return false;
            }
        });

        if(isValidVencimento == false){
            alert('Data do vencimento ou vencimento real não podem ser menor que da emissão');
            return false;
        }

        $(".rateio").each(function(i){
            var ur= $(this).children('td').eq(0).children('select').val();
            var valor= $(this).children('td').eq(1).children('input').val();
            var porcento = $(this).children('td').eq(2).children('input').attr('valor_real');
            var ipcf2= '';
            var ipcg3= '';
            var ipcg4 = $(this).children('td').eq(3).find('.id-natureza-rateio').val();
            var assinatura= $(this).children('td').eq(4).children('select').val();

            rateio += ur+'#'+valor+'#'+porcento+'#'+ipcg3+'#'+ipcg4+'#'+assinatura+'#'+ipcf2;

            if(i< x - 1){
                rateio += "$";
            }
        });

        var imposto_retido='';
        var cont_imposto=$(".imposto_retido").size();
        $(".imposto_retido").each(function(i){

            var valor = $(this).children('td').eq(4).children('input').val();
            valor= valor.replace('.','').replace(',','.');
            var  id_subnota = $(this).attr('id_nota');
            var tipo= $(this).children('td').eq(1).children('select').children('option:selected').attr('sigla_tipo_documento');
            var tipo_doc_fin_id = $(this).children('td').eq(1).children('select').val();
                
            var cod = $(this).children('td').eq(2).children('input').val();
            var vencimento = $(this).children('td').eq(3).children('input').val();
            var fornecedor = $(this).children('td').eq(5).children('select').val();
            var ipcf4 = $(this).children('td').eq(4).children('input').attr('ipcf4');
            var id_imposto_retido = $(this).attr('id_imposto_retido');
            if(id_subnota > 0 || valor > 0){
                imposto_retido += tipo+'#'+cod+'#'+vencimento+'#'+valor+'#'+fornecedor+'#'+ipcf4+'#'+id_imposto_retido+'#'+id_subnota+'#'+tipo_doc_fin_id;

                if(i< cont_imposto- 1){
                    imposto_retido += "$";
                }
            }
        });

        var nota = "query=editar-nota&"+dados+"&parcela="+parcela+"&rateio="+rateio+"&tipo="+tipo+"&imposto_retido="+imposto_retido+"&nota_tipo_documento_sigla="+nota_tipo_documento_sigla+"&tipo_calculo_rateio="+tipo_calculo_rateio;

        $.ajax({
            type: "POST",
            url: "query.php",
            data: nota,
            success: function(msg){
                if(msg == 1){
                    $("#dialog-editar").dialog('open');
                } else {
                    alert(msg);
                }
            }
        });

        return false ;
    });

    $("#inserir_nota_usar_nova").click(function(){
        var data_fechamento = moment($("#data-fechamento").val());
        var tipo = $("input[name='tipo']:checked").val();
        var val_rateio=0;
        var val_parcela=0;
        var val_porcento=0;
        var rateio = '';
        var parcela='';
        var nota_tipo_documento_sigla = $("select[name='notas_tipo_documento'] option:selected").attr('sigla_tipo_documento');

        var compromisso_iss = true;
        var vencimento_retencao_iss = true;
        var vencimento_formated = '';
        if($("#issqn").val() != '0,00') {
            if($("input[name='valor_issqn']").val() == '0,00' && $("input[name='vencimento_issqn']").val() == '') {
                compromisso_iss = false;
            }
            vencimento_formated = $("input[name='vencimento_issqn']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_issqn']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_iss = false;
            }
        }

        var compromisso_ir = true;
        var vencimento_retencao_ir = true;
        if($("#ir").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_ir']").val() == '0,00' && $("input[name='vencimento_ir']").val() == '') {
                compromisso_ir = false;
            }
            vencimento_formated = $("input[name='vencimento_ir']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_ir']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_ir = false;
            }
        }

        var compromisso_pis = true;
        var vencimento_retencao_pis = true;
        if($("#pis").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_pis']").val() == '0,00' && $("input[name='vencimento_pis']").val() == '') {
                compromisso_pis = false;
            }
            vencimento_formated = $("input[name='vencimento_pis']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_pis']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_pis = false;
            }
        }

        var compromisso_cofins = true;
        var vencimento_retencao_cofins = true;
        if($("#cofins").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_cofins']").val() == '0,00' && $("input[name='vencimento_cofins']").val() == '') {
                compromisso_cofins = false;
            }
            vencimento_formated = $("input[name='vencimento_cofins']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_cofins']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_cofins = false;
            }
        }

        var compromisso_csll = true;
        var vencimento_retencao_csll = true;
        if($("#csll").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_csll']").val() == '0,00' && $("input[name='vencimento_csll']").val() == '') {
                compromisso_csll = false;
            }
            vencimento_formated = $("input[name='vencimento_csll']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_csll']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_csll = false;
            }
        }

        var compromisso_pcc = true;
        var vencimento_retencao_pcc = true;
        if($("#pcc").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_pcc']").val() == '0,00' && $("input[name='vencimento_pcc']").val() == '') {
                compromisso_pcc = false;
            }
            vencimento_formated = $("input[name='vencimento_pcc']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_pcc']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_pcc = false;
            }
        }

        var compromisso_inss = true;
        var vencimento_retencao_inss = true;
        if($("#inss").val() != '0,00' && tipo == 1) {
            if($("input[name='valor_inss']").val() == '0,00' && $("input[name='vencimento_inss']").val() == '') {
                compromisso_inss = false;
            }
            vencimento_formated = $("input[name='vencimento_inss']").val().split('/').reverse().join('-');
            if($("input[name='vencimento_inss']").val() != '' && moment(vencimento_formated).isSameOrBefore(data_fechamento)) {
                vencimento_retencao_inss = false;
            }
        }

        if(!validar_campos("div-nova-nota")){
            return false;
        }

        if(
            (
                !compromisso_iss ||
                !compromisso_ir ||
                !compromisso_pis ||
                !compromisso_cofins ||
                !compromisso_csll ||
                !compromisso_pcc ||
                !compromisso_inss
            ) &&
            tipo == '1'
        ) {
            alert('Alguns compromissos de retenções não foram devidamente preenchidos! ' +
                'Clique no botão "Calcular Vencimentos" para que o sistema os calcule corretamente!');
            $("#calcular-vencimentos").focus();
            return false;
        }

        var data_emissao_nota = moment($('.data-emissao-nota').val().split('/').reverse().join('-'));
        var vencimento_emissao_nota = data_emissao_nota.isSameOrBefore(data_fechamento);

        if( !vencimento_retencao_iss ||
            !vencimento_retencao_ir ||
            !vencimento_retencao_pis ||
            !vencimento_retencao_cofins ||
            !vencimento_retencao_csll ||
            !vencimento_retencao_pcc ||
            !compromisso_inss
        ) {
            alert('Alguns compromissos de retenções tem o vencimento menor que a data de fechamento do período (' + data_fechamento.format('DD/MM/YYYY') + ')! ' +
                'Verifique-os para que sejam posterior!');
            $("#calcular-vencimentos").focus();
            return false;
        }

        if(vencimento_emissao_nota) {
            alert('A data da emissão da nota é menor que a data  de fechamento do período (' + data_fechamento.format('DD/MM/YYYY') + ')!');
            $(".data-emissao-nota").focus();
            return false;
        }

        var dados = $("form").serialize();
        tot =parseFloat(replaceValorMonetarioFloat($("#valor_nota").val()));

        //if(tipo==1){
        $(".valor_rateio").each(function(){
            val_rateio += parseFloat(replaceValorMonetarioFloat($(this).val()));
        });
        $(".porcento").each(function(){
            val_porcento += parseFloat($(this).attr('valor_real'));
        });
        //}

        $(".valor_parcela").each(function(){
            val_parcela += parseFloat(replaceValorMonetarioFloat($(this).val()));
        });

        if(tot == '0.00' || tot == '0' ){
            alert("Valor da nota n&aatilde;o pode ser 0,00");
            return false;
        }
        if( val_parcela.toFixed(2) < tot){
            alert("Valor da soma da(s) parcela(s) menor que o valor da nota");
            return false;
        }

        //if(tipo==1){
        if( val_rateio.toFixed(2) < tot){
            alert("Valor da soma do rateio menor que o valor da nota");
            return false;
        }

        if( val_porcento.toFixed(2) != parseFloat(100)){
            alert("A soma da porcentagem do rateio deve ser 100%");
            return false;
        }
        //}
        var x= $(".rateio").size();
        var x2= $(".parcela").size();
        var isValidVencimento = true;
        var isValidCodBarras = true;

        $(".parcela").each(function(i){

            var trp= $(this).children('td').eq(1).children('input').val();
            var data= $(this).children('td').eq(2).children('input').val();
            var vencimento_real = $(this).children('td').eq(3).children('input').val();

            var valor= $(this).children('td').eq(4).children('input').val();
            var cod_barra= $(this).children('td').eq(5).children('input').val();

            if(cod_barra != '') {
                if(cod_barra.length != 44 && cod_barra.length != 47 && cod_barra.length != 48) {
                    isValidCodBarras = false;
                    return;
                }
            }

            parcela += trp+'#'+data+'#'+cod_barra+'#'+valor+'#'+vencimento_real;

            if(i< x2 - 1){
                parcela += "$";
            }
            var vencimento = new Date(data.split("/").reverse().join("-"));
            var emissao = new Date($('.data-emissao-nota').val().split("/").reverse().join("-"));

            if(vencimento < emissao) {
                isValidVencimento = !isValidVencimento;
                $(this).children('td').eq(2).children('input').val('');
                return false;
            }

        });

        if(isValidCodBarras == false){
            alert('O tamanho do código de barras não é válido!');
            return false;
        }

        if(isValidVencimento == false){
            alert('A data de vencimento não pode ser menor que a data de emissão.');
            return false;
        }

        //if(tipo==1){
        $(".rateio").each(function(i){
            var ur = $(this).children('td').eq(0).children('select').val();
            var valor = $(this).children('td').eq(1).children('input').val();
            var porcento = $(this).children('td').eq(2).children('input').attr('valor_real');
            var ipcf2 = '';
            var ipcg3 = '';
            var ipcg4 = $(this).children('td').eq(3).find('.id-natureza-rateio').val();
            var assinatura = $(this).children('td').eq(4).children('select').val();

            rateio += ur+'#'+valor+'#'+porcento+'#'+ipcg3+'#'+ipcg4+'#'+assinatura+'#'+ipcf2;

            if(i< x - 1){
                rateio += "$";
            }

        });

        var imposto_retido='';
        var cont_imposto=$(".imposto_retido").size();
        $(".imposto_retido").each(function(i){
            var valor = $(this).children('td').eq(4).children('input').val();
            valor= valor.replace('.','').replace(',','.');
            if(valor >0){

                var tipo= $(this).children('td').eq(1).children('select').children('option:selected').attr('sigla_tipo_documento');
                var tipo_doc_fin_id = $(this).children('td').eq(1).children('select').val();

                var cod = $(this).children('td').eq(2).children('input').val();
                var vencimento = $(this).children('td').eq(3).children('input').val();
                var fornecedor = $(this).children('td').eq(5).children('select').val();
                var ipcf4 = $(this).children('td').eq(4).children('input').attr('ipcf4');
                var id_imposto_retido = $(this).attr('id_imposto_retido');

                imposto_retido += tipo+'#'+cod+'#'+vencimento+'#'+valor+'#'+fornecedor+'#'+ipcf4+'#'+id_imposto_retido+'#'+tipo_doc_fin_id;

                if(i< cont_imposto- 1){
                    imposto_retido += "$";
                }
            }

        });

        var nota = "query=usar-nota-para-nova&" + dados + "&parcela=" + parcela + "&rateio=" + rateio+"&imposto_retido="+imposto_retido+"&nota_tipo_documento_sigla="+nota_tipo_documento_sigla;

        $.ajax({
            type: "POST",
            url: "query.php",
            data: nota,
            success: function(msg){

                if(msg > 0){
                    $("#trGerada").html('<p>Conta salva com Sucesso!</p><p>TR ' + msg + '</p>');
                    $("#dialog-message").dialog('open');

                }else{
                    alert(msg);
                }
            }
        });


        return false ;
    });

    $('.selecionar-todos').live('click',function(){
        if($(this).is(':checked')){
            $(this).attr('defaultChecked',true);
            $('.selecionar-parcela-exclusao').each(function(){
                $(this).attr('checked','checked');
                $(this).attr('defaultChecked',true);

            });
        }else{
            $('.selecionar-parcela-exclusao').each(function(){
                $(this).attr('checked',false);
                $(this).attr('defaultChecked',false);
            });
            $(this).attr('defaultChecked',false);
        }
    });

    $('#excluir-parcelas-selecionadas').live('click',function(){
        if(confirm('Deseja realmente excluir os itens selecionados?')){
            $('.selecionar-parcela-exclusao').each(function(){
                if($(this).is(':checked')){
                    $(this).parent().parent().remove();
                }
            });
        }
    });

    $('#empresa-nota').change(function(){
       var val =  $(this).val();
       if($('#rateio').find('.id-natureza-rateio').length == 1){
        $('#rateio').find(".empresa").val(val);

      }
    });

    $(".gerar-varias-parcelas").click(function(){
        var qtd = parseInt($('.qtd-parcelas-gerar').val());
        var vencimento = $('.vencimento-parcelas-gerar').val();
        var valor = $('.valor-parcelas-gerar').val();
        var from = $(this).attr('from');
       
        if($('.data-emissao-nota').val() == ''){
            alert("Preencha a data de Emissão.");
            return false;
        }
        var emissao = new Date($('.data-emissao-nota').val().split("/").reverse().join("-"));
        var data_vencimento = new Date(vencimento.split('/').reverse().join('-'));
        if(data_vencimento < emissao){
            alert('Vencimento de parcela não pode ser menor que a data de emissão da conta!');
            return false;
        }

        if(qtd > 0) {
            if(from == 'new-notas') {
                $(".parcela").remove();
            }
            var data_inicial = moment(vencimento.split('/').reverse().join('-'));
            var vencimento_atual = '', tr = 1;
            if(from == 'edit-notas') {
                tr = parseInt($("#parcelas tr:last").children('td').eq(1).children('input').val());
            }
            for (i = 1; i <= qtd; i++) {
                if(from == 'edit-notas' && tr) {
                    tr = tr + 1;
                } else {
                    tr = i;
                }
                vencimento_atual = moment(data_inicial);
                vencimento_atual = moment(vencimento_atual).format('DD/MM/YYYY');
                data_format_bd = moment(data_inicial).format('YYYY-MM-DD');
                date = new Date(data_format_bd+' 01:00:00');                
                vencimento_real =  validar_vencimento_real(date, false);
                

                $("#parcelas").append(
                    "<tr class='parcela' status='Pendente' data-editar-parcela='0' data-id-parcela='0'>" +
                    "   <td><input type='checkbox' class='selecionar-parcela-exclusao'></td>" +
                    "   <td>" +
                    "       <img align='left' class='del_parcela' src='../utils/delete_16x16.png' title='Remover' border='0'/>" +
                    "       <input type='text' name='tr_parcela' size='5' value='"+tr+"' />" +
                    "       <input type='hidden' maxlength='128' size='20' name='codBarras' />" +
                    "   </td>" +
                    "   <td><input type='text' maxlength='10' size='10' class='mask-data OBG vencimento-parcela' value='" + vencimento_atual + "'/></td>"+
                    "   <td><input type='text' maxlength='10' size='10' class='mask-data OBG vencimento-real-parcela' value='" +moment(vencimento_real).format('DD/MM/YYYY')+ "'/></td>"+
                    "   <td><input class='OBG valor valor_parcela' value='" + valor + "' type='text'  maxlength='15' style='text-align:right' /></td>" +
                    "   <td><input  value='' size='70' class='codigo-barra' type='text'/></td>"+
                    "</tr>"
                );
                vencimento_atual = moment(data_inicial).add(1, 'M');
                data_inicial = vencimento_atual;
                $("#parcelas tr:odd").css("background-color","#ebf3ff");

                $('.qtd-parcelas-gerar').val('1');
                $('.valor-parcelas-gerar').val('0,00');

                $('.mask-data').mask('99/99/9999');

                $(".valor").priceFormat({
                    prefix: '',
                    centsSeparator: ',',
                    thousandsSeparator: '.'
                });
            }
        }
    });

    $("#add_parcela").live('click', function(){
        var valor = $("#parcelas tr:last").children('td').eq(1).children('input').val();
        par= parseInt(valor)+1;

        $("#parcelas").append(
            "<tr class='parcela' status='Pendente' data-editar-parcela='0' data-id-parcela='0'>" +
            "   <td><input type='checkbox' class='selecionar-parcela-exclusao'></td>" +
            "   <td><img align='left' class='del_parcela' src='../utils/delete_16x16.png' title='Remover' border='0'/><input type='text' name='tr_parcela' size='5' value='"+par+"' /></td>" +
            "   <td><input type='text' maxlength='10' size='10' class='mask-data vencimento-parcela OBG'/></td>"+
            "   <td><input type='text' maxlength='10' size='10' class='mask-data vencimento-real-parcela OBG'/></td>"+
            "   <td><input class='OBG valor valor_parcela' value='0,00' type='text'  maxlength='15' style='text-align:right' /></td>" +
            "   <td><input class='codigo-barra' value='' type='text'  size='70' /></td>" +

            "</tr>"
        );
        $("#parcelas tr:odd").css("background-color","#ebf3ff");
        $(".data").datepicker({
            inline: true
        });

        $('.mask-data').mask('99/99/9999');

        $(".valor").priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        return false;
    });

    $(".del_parcela").live('click',function(){
        if($("#parcelas tr").length > 3)
            $(this).parent().parent().remove();
        $("#parcelas tr:odd").css("background-color","#ebf3ff");
        return false;
    });

    $(".del_rateio").live('click',function(){
        if($("#rateio tr").length > 3)
            $(this).parent().parent().remove();
        $("#rateio tr:odd").css("background-color","#ebf3ff");
        return false;
    });


    function ipcf2_sel(){
        var val = 0;
        var val1 = 0;
        $(".rateio").each(function(){

            val =$(this).children('td').eq(3).attr("name");

        });
        val= parseInt(val);
        val1 = val ;

        var tipo = $("input[name='tipo']:checked").val();
        $.post("financeiro.php",{query: "ipcf2", tipo:tipo},
            function(r){

                $("td[name='"+val1+"']").html(r);
            });
    }

    function empresa_sel(id = null){
        var val = 0;
        var val1 = 0;
        $(".rateio").each(function(){

            val =$(this).children('td').eq(1).attr("name");

        });
        console.log(val);
        val= parseInt(val);
        val1 = val ;
        
       
        $.post("financeiro.php",{query: "empresa", tipo:id},
            function(r){

                $("td[name='empresa_"+val1+"']").html(r);
            });
    }
    function assinatura_sel(){
        var val = 0;
        var val1 = 0;
        $(".rateio").each(function(){

            val =$(this).children('td').eq(1).attr("name");

        });
        val= parseInt(val);
        val1 = val ;
       
        $.post("financeiro.php",{query: "assinatura", tipo: 0},
            function(r){

                $("td[name='assinatura_"+val1+"']").html(r);
            });
    }

    $("#add_rateio").click(function(){
        var val=0;
        $(".rateio").each(function(){

            val =$(this).children('td').eq(1).attr("name");


        });
        val= parseInt(val);
        val1 = val +1;
        naturezaPrincipal = $("#natureza-movimentacao").val();
        labelNaturezaPrincipal = $(".nome-natureza").html();

        $("#rateio").append(
            "<tr class='rateio'>"+
            "<td name='empresa_"+val1+"' style='font-size:2px;'><img align='left' class='del_rateio'  src='../utils/delete_16x16.png' title='Remover' border='0'/></td>"+
            "<td name='" + val1 + "' style='font-size:2px;'><input class='OBG valor valor_rateio' value='0,00' type='text'  maxlength='15' style='text-align:right;font-size:10px;'  /></td>"+
            "<td ><input type='text'  maxlength='15' value='0,00' valor_real='0,00' disabled='disabled'  class='valor OBG porcento '  style='text-align:right;font-size:10px;' />%</td>"+
            "<td class='td_4_"+val1+"' name='ipcg4_"+val1+"' >"+
            "<img src='/utils/look_16x16.png' class='buscar-naturezas-dialog-rateio' style='cursor: pointer;' /> &nbsp;"+
            "<input type = 'hidden' class='id-natureza-rateio' value ='' />"+
            "<input type = 'text' class='OBG nome-natureza-rateio' style='font-size:9px;' disabled readonly size='50' value='' />"+
            "<span class='negrito' tipo='rateio' style='color: red'></span></td>"+
                                
            "</td>"+
            "<td class='td_5_"+val1+"' name='assinatura_"+val1+"' ></td>"+
            "</tr>");
       // ipcg4_sel_busca();
        empresa_sel();
        assinatura_sel();
        $("#rateio tr:odd").css("background-color","#ebf3ff");

        $(".valor").priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        return false;
    });




    $("#cancelar_nota").click(function(){
        $( "#dialog-cancelar-nota" ).dialog("open");
        return false;
    });

    $(".cancelar_parcela").click(function(){
        $( "#dialog-cancelar-parcela" ).attr("idparcela",$(this).attr("idparcela"));
        $( "#dialog-cancelar-parcela" ).dialog("open");
        return false;
    });



    $(".informe-perda").live('click',function(){
       
        var $this = $(this);
        var parcelaId = $this.attr('parcela-id');
        var notaId = $this.attr('nota-id');
        var valor = $this.attr('valor');
       
        $("#dialog-informe-perda" ).attr("parcela-id",$(this).attr("parcela-id"));
        $("#dialog-informe-perda" ).attr("nota-id",$(this).attr("nota-id"));
        $("#dialog-informe-perda" ).attr("valor",$(this).attr("valor"));
        
      
        $("#dialog-informe-perda" ).dialog('open')
        
       
        
    
    
    
        // var query = $this
        // if(confirm('Deseja realmente Processar como Perda?')) {
        //     $.ajax({
        //         url: "/financeiros/informe-perda/?action=salvar-informe-perda",
        //         type: 'POST',
        //         data: {parcelaId: parcelaId, 
        //                 notaId: notaId, 
        //                 valor: valor},
        //         success: function (response) {
        //             response = JSON.parse(response);                   
        //             if(response['tipo'] != 'erro') {
        //                 alert(response['msg']);
        //                 window.location.reload();
        //             } else {
        //                 alert(response['msg']);
                        
        //             }
        //             return false;
        //         }
        //     });
        // }
    });
    
    $("#dialog-informe-perda").dialog({
        autoOpen: false,
        modal: true,
        minWidth: 700, 
        minHeight: 300,
        buttons: {
            Salvar: function() {
                if(validar_campos("dialog-informe-perda")){
                  //  moment.locale('pt-BR');        
                    var data_fechamento = moment($("#data-fechamento").val());
                    var data_pagamento = moment($("#data-informe-perda").val().split('/').reverse().join('-'));
                    if(data_pagamento.isSameOrBefore(data_fechamento)) {
                        alert('A data de pagamento é menor que a data de fechamento do período (' + data_fechamento.format('DD/MM/YYYY') + ')!');
                        return false;
                    }
    
                    var parcelaId = $("#dialog-informe-perda" ).attr('parcela-id');
                    var notaId = $("#dialog-informe-perda" ).attr('nota-id');
                    var valor = $("#dialog-informe-perda" ).attr('valor');                
                    var data = $("#data-informe-perda").val()
                    var justificativa = $("#justificativa-infome-perda").val();
                    $.ajax({
                        url: "/financeiros/informe-perda/?action=salvar-informe-perda",
                        type: 'POST',
                        data: { parcelaId: parcelaId, 
                                notaId: notaId, 
                                valor: valor,
                                data: data,
                                justificativa: justificativa},
                        success: function (response) {
                            response = JSON.parse(response);                   
                            if(response['tipo'] != 'erro') {
                                alert(response['msg']);
                                window.location.reload();
                            } else {
                                alert(response['msg']);
                                
                            }
                            return false;
                        }
                    });              
                }
            },
            Cancelar: function(){
                $('#lembrete-parcela-origem').html('');
                $( this ).dialog( "close" );
            }
        }
    });

    $( "#dialog-editar-valores-parcela" ).dialog({
        minWidth: 700, minHeight: 300, autoOpen: false,modal: true,
        open: function(event, ui) {
            $(".OBG").each(function (i){
                this.style.border = "";
            });
        },
        buttons: {
            Salvar: function() {
                var juros= $("#dialog-editar-valores-parcela input[name='juros_parcela_valores']").val();
                var desconto= $("#dialog-editar-valores-parcela input[name='desconto_parcela_valores']").val();
                var tarifas_adicionais= $("#dialog-editar-valores-parcela input[name='tarifas_adicionais_parcela_valores']").val();
                var tarifaCartao= $("#dialog-editar-valores-parcela input[name='tarifa_cartao_parcela_valores']").val();

                var vencimento_real= $("#dialog-editar-valores-parcela input[name='vencimento_real_parcela_valores']").val();
                var vencimento= $("#dialog-editar-valores-parcela input[name='vencimento_parcela_valores']").val();

                if(tarifaCartao =! '0,00' || juros != '0,00' || desconto != '0,00' || tarifas_adicionais != '0,00' || ( vencimento_real != '' &&  vencimento != ''))  
                {
                    $.post("query.php",
                        {
                            query: "editar-valores-parcela",
                            id: $( "#dialog-editar-valores-parcela" ).attr("parcela-id"),
                            juros: $("#dialog-editar-valores-parcela input[name='juros_parcela_valores']").val(),
                            desconto: $("#dialog-editar-valores-parcela input[name='desconto_parcela_valores']").val(),
                            tarifas_adicionais: $("#dialog-editar-valores-parcela input[name='tarifas_adicionais_parcela_valores']").val(),
                            vencimento_real : vencimento_real,
                            vencimento : vencimento,
                            tarifaCartao: $("#dialog-editar-valores-parcela input[name='tarifa_cartao_parcela_valores']").val()
                        },
                        function(retorno){
                            if(retorno == 1){
                                alert('Parcela editada com sucesso!');
                                window.location.reload();
                            }else{
                                alert(retorno);}
                        });
                    $( this ).dialog( "close" );
                }else{
                    validar_campos("dialog-editar-valores-parcela");
                }
            },
            Cancelar: function(){
                $('#lembrete-parcela-origem').html('');
                $( this ).dialog( "close" );
            }
        }
    });

    $(".editar_valores_parcela").click(function(){
        var $this = $(this);
        var $dialog = $("#dialog-editar-valores-parcela" );
        $dialog.attr('parcela-id', $this.attr('idparcela'));
        $dialog.find(".juros-parcela-valores").val(float2moeda($this.attr('juros')));
        $dialog.find(".desconto-parcela-valores").val(float2moeda($this.attr('desconto')));
        $dialog.find(".tarifas-adicionais-parcela-valores").val(float2moeda($this.attr('tarifas-adicionais')));
        $dialog.find(".vencimento-real-parcela-valores").val($this.attr('vencimento_real'));
        $dialog.find(".vencimento-parcela-valores").val($this.attr('vencimento'));
        $dialog.find(".tarifa-cartao-parcela-valores").val(float2moeda($this.attr('tarifa_cartao')));

        $dialog.dialog("open");
    });

    function somaValorProcessarParcela(){
        var valor=  parseFloat(replaceValorMonetarioFloat($("#dialog-processar-parcela input[name='valor']").val()));
        var desconto = parseFloat(replaceValorMonetarioFloat($("#dialog-processar-parcela input[name='desconto']").val()));
        var juros = parseFloat(replaceValorMonetarioFloat($("#dialog-processar-parcela input[name='juros']").val()));
        var outros = parseFloat(replaceValorMonetarioFloat($("#dialog-processar-parcela input[name='outros']").val()));
        var tarifaCartao = parseFloat(replaceValorMonetarioFloat($("#dialog-processar-parcela input[name='tarifa_cartao']").val()));

        var encargos = parseFloat(replaceValorMonetarioFloat($("#dialog-processar-parcela input[name='encargos']").val()));
        var tipo_nota =  $("#dialog-processar-parcela" ).attr("tipo_nota");
        var impostos_receber = 0;
        var valor_pago = (valor + juros + outros + encargos) - (desconto + tarifaCartao);
        console.log(valor_pago);
        return valor_pago;
    }

    $(".processar_parcela").click(function(){
        $("#dialog-processar-parcela" ).attr("idparcela",$(this).attr("idparcela"));
        $("#dialog-processar-parcela input[name='valor']").val($(this).attr("valor"));
        $("#dialog-processar-parcela input[name='juros']").val(float2moeda($(this).attr("juros")));
        $("#dialog-processar-parcela input[name='desconto']").val(float2moeda($(this).attr("desconto")));
        $("#dialog-processar-parcela input[name='outros']").val(float2moeda($(this).attr("tarifas-adicionais")));
        $("#dialog-processar-parcela input[name='tarifa_cartao']").val(float2moeda($(this).attr("tarifa_cartao")));
        if($(this).attr("vencimento") != '00/00/0000') {
            $("#dialog-processar-parcela input[name='vencimento']").val($(this).attr("vencimento"));
           
        }
        if($(this).attr("vencimento_real") != '00/00/0000') {
            $("#dialog-processar-parcela input[name='vencimento_real']").val($(this).attr("vencimento_real"));
            $("#dialog-processar-parcela input#data").val($(this).attr("vencimento_real"));
        }
        $("#dialog-processar-parcela .valor_pago").html(float2moeda($(this).attr("valor-pagar")));
        var parcela_origem =$(this).attr('parcela-origem');
        if(parcela_origem >0){
            $.post("query.php",{query:"buscar-parcela-origem",parcela_origem :parcela_origem },function(r){

                $('#lembrete-parcela-origem').html(r);
                $("#dialog-processar-parcela" ).dialog("open");

            });

        }else{
            var valor_pago = somaValorProcessarParcela();
        valor_pago =valor_pago.toFixed(2);
        valor_pago =float2moeda(valor_pago);


        $("#dialog-processar-parcela .valor_pago").html(valor_pago);
            $("#dialog-processar-parcela" ).dialog("open");
        }
        return false;
    });
    ////////somatorio do valor pago jeferson 03-05-2013
   
    $(".soma_valor").blur(function(){
        
        var valor_pago = somaValorProcessarParcela();
        valor_pago =valor_pago.toFixed(2);
        valor_pago =float2moeda(valor_pago);


        $("#dialog-processar-parcela .valor_pago").html(valor_pago);
    });

    $(".soma_valor").keyup(function(){
       var  valor_pago = somaValorProcessarParcela();
        valor_pago =valor_pago.toFixed(2);
        valor_pago =float2moeda(valor_pago);


        $("#dialog-processar-parcela .valor_pago").html(valor_pago);
    });

    //////////////////////////////////fim
    $(".comprovante").click(function(){
        $(this).parent().next("td").children("div").show();
    });

    $(".form-comprovante,.salvar-comprovante").hide();

    $(".arquivo-comprovante").change(function(){
        $(this).next("input").show();
    });

    $(".show-comprovante").click(function(){
//     $.post("query.php",{query: "show_comprovante", id: $(this).attr("id")}, function(retorno){window.load(retorno);});
        window.location = "query.php?query=show_comprovante&id="+$(this).attr("id");
        return false;
    });

    if(jQuery().selectize) {

        $(".classe-valor-selectize").selectize({
            create: true,
            sortField: 'text'
        });
    }

    $(".buscar-retencoes-fornecedor").change(function () {
        var fornecedor = $(this).val();
        $.get("query.php",
            {
                query: "buscar-retencoes-fornecedor",
                fornecedor: fornecedor
            },
            function(response){
                response = JSON.parse(response);
                if(Object.keys(response).length > 0){
                    var iss = 0.00;
                    var ir = 0.00;
                    var ipi = 0.00;
                    var icms = 0.00;
                    var pis = 0.00;
                    var cofins = 0.00;
                    var csll = 0.00;
                    var inss = 0.00;
                    var $issqnElem = $("#issqn");
                    var $irElem = $("#ir");
                    var $ipiElem = $("#ipi");
                    var $icmsElem = $("#icms");
                    var $pisElem = $("#pis");
                    var $cofinsElem = $("#cofins");
                    var $csllElem = $("#csll");
                    var $inssElem = $("#inss");
                    if(response.recolhe_iss == '1') {
                        $issqnElem.attr(
                            'aliquota',
                            response.aliquota_iss > 0.00 ?
                            iss = response.aliquota_iss :
                            iss = $issqnElem.attr('aliquota')
                        );
                    } else if(response.recolhe_iss == '0') {
                        $issqnElem.attr('aliquota', 0.00);
                    }

                    if(response.recolhe_ir == '1') {
                        $irElem.attr(
                            'aliquota',
                            response.aliquota_ir > 0.00 ?
                            ir = response.aliquota_ir :
                            ir = $irElem.attr('aliquota')
                        );
                    } else if(response.recolhe_ir == '0') {
                        $irElem.attr('aliquota', 0.00);
                    }

                    if(response.recolhe_ipi == '1') {
                        $ipiElem.attr(
                            'aliquota',
                            response.aliquota_ipi > 0.00 ?
                            ipi = response.aliquota_ipi :
                            ipi = $ipiElem.attr('aliquota')
                        );
                    } else if(response.recolhe_ipi == '0') {
                        $ipiElem.attr('aliquota', 0.00);
                    }

                    if(response.recolhe_icms == '1') {
                        $icmsElem.attr(
                            'aliquota',
                            response.aliquota_icms > 0.00 ?
                            icms = response.aliquota_icms :
                            icms = $icmsElem.attr('aliquota')
                        );
                    } else if(response.recolhe_icms == '0') {
                        $icmsElem.attr('aliquota', 0.00);
                    }

                    if(response.recolhe_pis == '1') {
                        $pisElem.attr(
                            'aliquota',
                            response.aliquota_pis > 0.00 ?
                            pis = response.aliquota_pis :
                            pis = $pisElem.attr('aliquota')
                        );
                    } else if(response.recolhe_pis == '0') {
                        $pisElem.attr('aliquota', 0.00);
                    }

                    if(response.recolhe_cofins == '1') {
                        $cofinsElem.attr(
                            'aliquota',
                            response.aliquota_cofins > 0.00 ?
                            cofins = response.aliquota_cofins :
                            cofins = $cofinsElem.attr('aliquota')
                        );
                    } else if(response.recolhe_cofins == '0') {
                        $cofinsElem.attr('aliquota', 0.00);
                    }

                    if(response.recolhe_csll == '1') {
                        $csllElem.attr(
                            'aliquota',
                            response.aliquota_csll > 0.00 ?
                            csll = response.aliquota_csll :
                            csll = $csllElem.attr('aliquota')
                        );
                    } else if(response.recolhe_csll == '0') {
                        $csllElem.attr('aliquota', 0.00);
                    }

                    if(response.recolhe_inss == '1') {
                        $inssElem.attr(
                            'aliquota',
                            response.aliquota_inss > 0.00 ?
                            inss = response.aliquota_inss :
                            inss = $inssElem.attr('aliquota')
                        );
                    } else if(response.recolhe_inss == '0') {
                        $inssElem.attr('aliquota', 0.00);
                    }
                    $("#iss-texto-aliquota").text('(' + iss.toString().replace('.', ',') + '%)');
                    $("#ir-texto-aliquota").text('(' + ir.toString().replace('.', ',') + '%)');
                    $("#ipi-texto-aliquota").text('(' + ipi.toString().replace('.', ',') + '%)');
                    $("#icms-texto-aliquota").text('(' + icms.toString().replace('.', ',') + '%)');
                    $("#pis-texto-aliquota").text('(' + pis.toString().replace('.', ',') + '%)');
                    $("#cofins-texto-aliquota").text('(' + cofins.toString().replace('.', ',') + '%)');
                    $("#csll-texto-aliquota").text('(' + csll.toString().replace('.', ',') + '%)');
                    $("#inss-texto-aliquota").text('(' + inss.toString().replace('.', ',') + '%)');
                }else{
                    $("#issqn").attr('aliquota', 0.00);
                    $("#ir").attr('aliquota', 0.00);
                    $("#ipi").attr('aliquota', 0.00);
                    $("#icms").attr('aliquota', 0.00);
                    $("#pis").attr('aliquota', 0.00);
                    $("#cofins").attr('aliquota', 0.00);
                    $("#csll").attr('aliquota', 0.00);
                    $("#inss").attr('aliquota', 0.00);
                    $("#iss-texto-aliquota").text('0,00');
                    $("#ir-texto-aliquota").text('0,00');
                    $("#ipi-texto-aliquota").text('0,00');
                    $("#icms-texto-aliquota").text('0,00');
                    $("#pis-texto-aliquota").text('0,00');
                    $("#cofins-texto-aliquota").text('0,00');
                    $("#csll-texto-aliquota").text('0,00');
                    $("#inss-texto-aliquota").text('0,00');
                    return false;
                }
            }
        );
    });

    $(".recolhe-retencao").change(function () {
        var retencao = $(this).attr('id').split('-');
        if($(this).is(':checked')) {
            $("#" + retencao[1]).removeAttr('disabled');
        } else {
            $("#" + retencao[1]).attr('disabled', true);
            $("#" + retencao[1]).val('0,00');
        }
    });

    $("#classe-valor-selectized").keyup(function() {
        $(this).val($(this).val().toUpperCase());
    });

    $("#salvar-fornecedor").live('click', function(){
        var ipcc = $('#ipcc').val();
        if(ipcc == ''){
            alert('Selecione a Conta contabil.');
            return false;
        }
        
        var validCampoSelectize = validar_campos_selectize("div-salvar-fornecedor");
       
        
        if(validar_campos("div-salvar-fornecedor")){
            var nome = $('input[name=nome]').val();
            var razaosocial = $('input[name=razaoSocial]').val();
            var ies = $('input[name=ies]').val();
            var tipo = $('select[name=tipo]').val();
            var im = $('input[name=im]').val();
            var contato = $('input[name=contato]').val();
            var telefone = $('input[name=telefone]').val();
            var celular = $('input[name=celular]').val();
            var endereco = $('input[name=endereco]').val();
            var bairro = $('input[name=bairro]').val();
            var id_cidade_und = $('input[name=id_cidade_und]').val();
            if(!id_cidade_und >0){
                alert('Selecione corretamente a Cidade.');
                return false;

            }
            var email = $('input[name=email]').val();
            var ag = $('input[name=ag]').val();
            var conta = $('input[name=conta]').val();
            var op = $('input[name=op]').val();
            var banco = $('input[name=banco]').val();
            var linha = '';
            var itens = '';
            var fisica_juridica =$("input[name='cpf_cnpj']:checked").val();
            var cnpj = $('input[name=cnpj]').val();
            var cpf = $('input[name=cpf]').val();

            // RETEÇÕES
            var recolhe_iss = $('#recolhe-iss').is(':checked') ? 1 : 0;
            var iss = $('input[name=iss]').val();
            var recolhe_ir = $('#recolhe-ir').is(':checked') ? 1 : 0;
            var ir = $('input[name=ir]').val();
            var recolhe_ipi = $('#recolhe-ipi').is(':checked') ? 1 : 0;
            var ipi = $('input[name=ipi]').val();
            var recolhe_icms = $('#recolhe-icms').is(':checked') ? 1 : 0;
            var icms = $('input[name=icms]').val();
            var recolhe_pis = $('#recolhe-pis').is(':checked') ? 1 : 0;
            var pis = $('input[name=pis]').val();
            var recolhe_cofins = $('#recolhe-cofins').is(':checked') ? 1 : 0;
            var cofins = $('input[name=cofins]').val();
            var recolhe_csll = $('#recolhe-csll').is(':checked') ? 1 : 0;
            var csll = $('input[name=csll]').val();
            var recolhe_inss = $('#recolhe-inss').is(':checked') ? 1 : 0;
            var inss = $('input[name=inss]').val();
            var classe_valor = $('select[name=classe_valor]').val();
            var classe_valor_text = $('select[name=classe_valor] option:selected').text();
            var precisa_classificar = $('#fornecedor-precisa-classificar').val();
            var classificacao = $('#fornecedor-classificacao').val();
            var plano_saude = $('#fornecedor-plano-saude').val();
            var tipo_chave_pix = $('#tipo-chave-pix').val();
            var chave_pix = $('#chave-pix').val();
            var observacao = $('#observacao').val();

            var total= $('.dados_contas').size();
            $('.dados_contas').each(function(i){
                var banco = $(this).children('td').eq(0).attr('cod');
                var ag = $(this).children('td').eq(2).children('input').val();
                //var op = $(this).children('td').eq(3).children('input').val();
                var tipo = $(this).children('td').eq(3).children('select').val();
                var conta = $(this).children('td').eq(4).children('input').val();
                var digitoConta = $(this).children('td').eq(5).children('input').val();

                var linha = banco+"#"+ag+"#"+tipo+"#"+conta+"-"+digitoConta;

                if(i<total - 1) linha += "$";
                itens += linha;

            });
            var ipcc = $('#ipcc').val();

            $.post("query.php",
                {
                    query: "salvar-fornecedor",
                    nome: nome,
                    razaosocial: razaosocial,
                    cnpj: cnpj,
                    ies: ies,
                    im: im,
                    contato: contato,
                    telefone: telefone,
                    celular: celular,
                    endereco: endereco,
                    id_cidade_und: id_cidade_und,
                    email: email,
                    dados: itens,
                    tipo: tipo,
                    cpf: cpf,
                    fisica_juridica: fisica_juridica,
                    ipcc: ipcc,
                    recolhe_iss: recolhe_iss,
                    iss: iss,
                    recolhe_ir: recolhe_ir,
                    ir: ir,
                    recolhe_ipi: recolhe_ipi,
                    ipi: ipi,
                    recolhe_icms: recolhe_icms,
                    icms: icms,
                    recolhe_pis: recolhe_pis,
                    pis: pis,
                    recolhe_cofins: recolhe_cofins,
                    cofins: cofins,
                    recolhe_csll: recolhe_csll,
                    csll: csll,
                    recolhe_inss: recolhe_inss,
                    inss: inss,
                    classe_valor: classe_valor,
                    classe_valor_text: classe_valor_text,
                    bairro: bairro,
                    precisa_classificar: precisa_classificar,
                    classificacao: classificacao,
                    plano_saude: plano_saude,
                    tipo_chave_pix: tipo_chave_pix,
                    chave_pix: chave_pix,
                    observacao: observacao
                },
                function(r){
                    if(r == 1){
                        alert('Fornecedor/Cliente criado com sucesso!');
                        window.location.href='?op=fornecedor&act=novo';
                    }else{
                        alert(r);
                    }
                }
            );
        }else{
            return false;
        }
    });

    $("#editar-fornecedor").live('click', function(){
        var ipcc = $('#ipcc').val();
        if(ipcc == ''){
            alert('Selecione a Conta contabil.');
            return false;
        }
        if(validar_campos("div-salvar-fornecedor")){
            var nome = $('input[name=nome]').val();
            var razaosocial = $('input[name=razaoSocial]').val();
            var cnpj = $('input[name=cnpj]').val();
            var ies = $('input[name=ies]').val();
            var im = $('input[name=im]').val();
            var tipo = $('select[name=tipo]').val();
            var contato = $('input[name=contato]').val();
            var telefone = $('input[name=telefone]').val();
            var celular = $('input[name=celular]').val();
            var endereco = $('input[name=endereco]').val();
            var bairro = $('input[name=bairro]').val();
            var id_cidade_und = $('input[name=id_cidade_und]').val();
            var fisica_juridica =$("input[name='cpf_cnpj']:checked").val();
            var cnpj = $('input[name=cnpj]').val();
            var cpf = $('input[name=cpf]').val();

            // RETEÇÕES
            var recolhe_iss = $('#recolhe-iss').is(':checked') ? 1 : 0;
            var iss = $('input[name=iss]').val();
            var recolhe_ir = $('#recolhe-ir').is(':checked') ? 1 : 0;
            var ir = $('input[name=ir]').val();
            var recolhe_ipi = $('#recolhe-ipi').is(':checked') ? 1 : 0;
            var ipi = $('input[name=ipi]').val();
            var recolhe_icms = $('#recolhe-icms').is(':checked') ? 1 : 0;
            var icms = $('input[name=icms]').val();
            var recolhe_pis = $('#recolhe-pis').is(':checked') ? 1 : 0;
            var pis = $('input[name=pis]').val();
            var recolhe_cofins = $('#recolhe-cofins').is(':checked') ? 1 : 0;
            var cofins = $('input[name=cofins]').val();
            var recolhe_csll = $('#recolhe-csll').is(':checked') ? 1 : 0;
            var csll = $('input[name=csll]').val();
            var recolhe_inss = $('#recolhe-inss').is(':checked') ? 1 : 0;
            var inss = $('input[name=inss]').val();
            var classe_valor = $('select[name=classe_valor]').val();            
            var classe_valor_text = $('select[name=classe_valor] option:selected').text();
            if(!id_cidade_und > 0){
                alert('Selecione corretamente a Cidade.');
                return false;
            }
            var email = $('input[name=email]').val();
            var ag = $('input[name=ag]').val();
            var conta = $('input[name=conta]').val();
            var op = $('input[name=op]').val();
            var banco = $('input[name=banco]').val();
            var linha = '';
            var itens = '';
            var total= $('.dados_contas').size();
            var precisa_classificar = $('#fornecedor-precisa-classificar').val();
            var classificacao = $('#fornecedor-classificacao').val();
            var plano_saude = $('#fornecedor-plano-saude').val();
            var tipo_chave_pix = $('#tipo-chave-pix').val();
            var chave_pix = $('#chave-pix').val();
            var observacao = $('#observacao').val();
            $('.dados_contas').each(function(i){
                var banco = $(this).children('td').eq(0).attr('cod');
                var ag = $(this).children('td').eq(2).children('input').val();
                // var op = $(this).children('td').eq(3).children('input').val();
                var tipo = $(this).children('td').eq(3).children('select').val();
                var conta = $(this).children('td').eq(4).children('input').val();
                var digitoConta = $(this).children('td').eq(5).children('input').val();

                var linha = banco+"#"+ag+"#"+tipo+"#"+conta+"-"+digitoConta;

                if(i<total - 1) linha += "$";
                itens += linha;

            });

            var id= $("#idfor").attr('idfornecedor');
            $.post(
                "query.php",
                {
                    query: "editar-fornecedor",
                    id: id,
                    nome: nome,
                    razaosocial: razaosocial,
                    cnpj: cnpj,
                    ies: ies,
                    im: im,
                    contato: contato,
                    telefone: telefone,
                    celular: celular,
                    endereco: endereco,
                    id_cidade_und: id_cidade_und,
                    email: email,
                    dados: itens,
                    tipo: tipo,
                    cpf: cpf,
                    fisica_juridica: fisica_juridica,
                    ipcc: ipcc,
                    recolhe_iss: recolhe_iss,
                    iss: iss,
                    recolhe_ir: recolhe_ir,
                    ir: ir,
                    recolhe_ipi: recolhe_ipi,
                    ipi: ipi,
                    recolhe_icms: recolhe_icms,
                    icms: icms,
                    recolhe_pis: recolhe_pis,
                    pis: pis,
                    recolhe_cofins: recolhe_cofins,
                    cofins: cofins,
                    recolhe_csll: recolhe_csll,
                    csll: csll,
                    recolhe_inss: recolhe_inss,
                    inss: inss,
                    classe_valor: classe_valor,
                    classe_valor_text: classe_valor_text,
                    bairro:bairro,
                    precisa_classificar: precisa_classificar,
                    classificacao: classificacao,
                    plano_saude:plano_saude,
                    tipo_chave_pix: tipo_chave_pix,
                    chave_pix: chave_pix,
                    observacao: observacao
                },
                function(r){
                    if(r == 1){
                        alert('Fornecedor/Cliente atualizado com sucesso!');
                        window.location.href='?op=fornecedor&act=listar';
                    }else{
                        alert(r);
                    }
                }
            );
        }else{
            return false;
        }

    });

    $("#add-conta").click(function(){
        var x = $('select[name=banco] option:selected').val();

        var bexcluir = "<img src='../utils/delete_16x16.png' class='del-conta-fornecedor' title='Excluir' border='0' >";

        var banco = $('select[name=banco] option:selected').text();
        var conta = "<input type='text' name='conta' class=' form-control OBG' maxlength='11' size='38'/>";
        // var op = "<input type='text' name='op' class='form-control numero' size='8'/>";
        var op = "<select name='tipo_conta' class='form-control OBG' ><option value=''></option><option value='Corrente'>Corrente</option><option value='Poupança'>Poupança</option></select>"
        var ag = "<input type='text' name='ag' class='form-control OBG' maxlength='5' size='8'/>";
        var digitoConta = "<input type='text' name='digito_conta' class=' form-control OBG' maxlength='1' size='2'/>";

        $("#tabela_banco").append("<tr class='dados_contas'><td  class='conta' cod='"+x+"' > "+bexcluir+"</td><td>"+banco+"</td><td>"+ag+"</td><td>"+op+"</td><td>"+conta+"</td><td>"+digitoConta+"</td></tr>");

        atualiza_conta_fornecedor();
        return false;


    });

    $(".del-conta-fornecedor").unbind("click",remove_conta_fornecedor)
        .bind("click",remove_conta_fornecedor);

    function atualiza_conta_fornecedor(){
        $(".del-conta-fornecedor").unbind("click",remove_conta_fornecedor)
            .bind("click",remove_conta_fornecedor);
    }

    function remove_conta_fornecedor(){
        $(this).parent().parent().remove();
        return false;
    }


    $( "#dialog-message" ).dialog({
        minWidth: 300,minHeight: 200,autoOpen: false,modal: true,
        buttons: {
            Ok: function() {
                $( this ).dialog( "close" );
                window.location.href='?op=notas&act=novo';
            }

        }
    });

    $( "#dialog-editar" ).hide();
    $( "#dialog-editar" ).dialog({
        minWidth: 300,minHeight: 200,autoOpen: false,modal: true,
        buttons: {
            Ok: function() {
                $( this ).dialog( "close" );
                window.location.href='?op=notas&act=listar';
            }

        }
    });
    $( "#dialog-parcela-ok" ).hide();
    $( "#dialog-parcela-ok" ).dialog({
        minWidth: 300,minHeight: 200,autoOpen: false,modal: true,
        buttons: {
            Ok: function() {
                var id =$("#idnota").attr("idnota");
                $( this ).dialog( "close" );
                window.location.href='?op=notas&act=detalhes&idNotas='+id;
            }

        }
    });

    $( "#dialog-processar-parcela" ).dialog({
        minWidth: 700, minHeight: 300, autoOpen: false,modal: true,
        open: function(event, ui) {
            $("#dialog-processar-parcela .aviso-parcela").css({"background-color":"", "font-weight":"bolder"});
            $("#dialog-processar-parcela .aviso-parcela").text("");
            $(".OBG").each(function (i){
                this.style.border = "";
            });
        },
        buttons: {
            Ok: function() {
                if(validar_campos("dialog-processar-parcela")){
                    var data_fechamento = moment($("#data-fechamento").val());
                    var data_pagamento = moment($("#data").val().split('/').reverse().join('-'));
                    if(data_pagamento.isSameOrBefore(data_fechamento)) {
                        alert('A data de pagamento é menor que a data de fechamento do período (' + data_fechamento.format('DD/MM/YYYY') + ')!');
                        return false;
                    }
                    pis = 0;
                    cofins = 0;
                    issqn = 0;
                    ir = 0;
                    csll = 0;
                    var juros = parseFloat($("#dialog-processar-parcela input[name='juros']").val().replace('.','').replace(',','.'));

                    if(juros != 0){
                        if($("#motivo-juros").val() == ''){
                            $("#motivo-juros").focus();
                            alert('Selecione o motivo do juros.');
                            return false;
                        }
                    }else{
                        $("#motivo-juros").val('');
                    }

                    $.post("query.php",
                        {
                            query: "processar_parcela", id: $( "#dialog-processar-parcela" ).attr("idparcela"),
                            origem: $("#dialog-processar-parcela select[name='origemFundos'] option:selected ").val(),
                            aplicacao: $("#dialog-processar-parcela select[name='aplicacaoFundos'] option:selected ").val(),
                            vencimento: $("#dialog-processar-parcela input[name='vencimento']").val(),
                            valor: $("#dialog-processar-parcela input[name='valor']").val(),
                            obs: $("#dialog-processar-parcela input[name='obs']").val(),
                            juros: $("#dialog-processar-parcela input[name='juros']").val(),
                            desconto: $("#dialog-processar-parcela input[name='desconto']").val(),
                            outros: $("#dialog-processar-parcela input[name='outros']").val(),
                            valor_pago: $("#dialog-processar-parcela .valor_pago").text(),
                            num_documento: $("#dialog-processar-parcela input[name='num_documento']").val(),
                            pis :pis,
                            cofins :cofins,
                            issqn :issqn,
                            ir : ir,
                            csll : csll,
                            tipo_nota: $("#dialog-processar-parcela").attr("tipo_nota"),
                            data: $("#data").val(),
                            usuario: $("#usuario").val(),
                            encargos: $("#dialog-processar-parcela input[name='encargos']").val(),
                            motivo_juros:$("#motivo-juros").val(),
                            vencimento_real: $("#dialog-processar-parcela input[name='vencimento_real']").val(),
                            tarifaCartao: $("#dialog-processar-parcela input[name='tarifa_cartao']").val(),
                        },
                        function(retorno){
                            if(retorno>1){
                                $( "#dialog-parcela-ok" ).dialog('open');
                            }else{
                                alert(retorno);}
                        });
                    $( this ).dialog( "close" );
                    $('#lembrete-parcela-origem').html('');

                }
            },
            Cancelar: function(){
                $('#lembrete-parcela-origem').html('');
                $( this ).dialog( "close" );
            }
        }
    });

    $( "#dialog-cancelar-parcela" ).dialog({
        minWidth: 200,minHeight: 100,autoOpen: false,modal: true,
        open: function(event, ui) {
            $("#dialog-cancelar-parcela .aviso-parcela").css({"background-color":"", "font-weight":"bolder"});
            $("#dialog-cancelar-parcela .aviso-parcela").text("");
            $(".OBG").each(function (i){
                this.style.border = "";
            });
        },
        buttons: {
            Ok: function() {
                if(validar_campos("dialog-cancelar-parcela")){
                    $.post("query.php",{query: "cancelar_parcela", id: $( "#dialog-cancelar-parcela" ).attr("idparcela"),
                        motivo: $("#dialog-cancelar-parcela input[name='obs']").val()},function(retorno){
                    });
                    $( this ).dialog( "close" );
                    location.reload();
                }
            },
            Cancelar: function(){
                $( this ).dialog( "close" );
            }
        }
    });

    /*$("#tel").keyup(function() {
        var valor = $(this).val().replace(/[^0-9,(0-9,) ,-]/g,'');
        $(this).val(valor);
    });*/
    $(".conta").live('keyup',function() {
        var valor = $(this).val().replace(/[^0-9,.,-]/g,'');
        $(this).val(valor);
    });

    $(".numero").live('keyup',function() {
        var valor = $(this).val().replace(/[^0-9]/g,'');
        $(this).val(valor);
    });

    $( "#dialog-cancelar-nota" ).dialog({
        minWidth: 200,minHeight: 100,autoOpen: false,modal: true,
        open: function(event, ui) {
            $("#dialog-cancelar-nota .aviso-nota").css({"background-color":"", "font-weight":"bolder"});
            $("#dialog-cancelar-nota .aviso-nota").text("");
            $(".OBG").each(function (i){
                this.style.border = "";
            });
        },
        buttons: {
            Ok: function() {
                if(validar_campos("dialog-cancelar-nota")){
                    $.post("query.php",{query: "cancelar_nota", id: $( "#dialog-cancelar-nota" ).attr("idnota"),
                        motivo: $("#dialog-cancelar-nota input[name='obs']").val()},function(retorno){
                           
                        if(retorno == '1') {
                            alert('Nota cancelada com sucesso!');
                            $('#nota-' + $("#dialog-cancelar-nota").attr("idnota")).remove();
                            window.location="?op=notas&act=buscar";
                        } else if(retorno == '2') {
                            alert('Essa nota possui algum impostos em fatura de aglutinação ou processado! Para poder excluir os impostos não podem ter sido processadosou aglutinados!');
                        }
                        else if(retorno == '-1') {
                            alert('Erro ao cancelar nota');
                        }
                    });
                }
            },
            Cancelar: function(){
                $( this ).dialog( "close" );
            }
        }
    });


    $("#inicio").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        onClose: function(selectedDate){
            $("#fim").datepicker(
                'option',
                'minDate',
                selectedDate
            )
        }
    });
    $("#fim").datepicker({
        minDate: $("#inicio").val(),
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1
    });
    $("#inicioE").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        onClose: function(selectedDate){
            $("#fimE").datepicker(
                'option',
                'minDate',
                selectedDate
            )
        }
    });
    $("#fimE").datepicker({
        minDate: $("#inicioE").val(),
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1
    });
    /*$("#competenciaInicio").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        onClose: function(selectedDate){
            $("#competenciaFim").datepicker(
                'option',
                'minDate',
                selectedDate
            )
        }
    });
    $("#competenciaFim").datepicker({
        minDate: $("#competenciaInicio").val(),
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1
    });*/
    $("#inicioVencimentoReal").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        onClose: function(selectedDate){
            $("#fimVencimentoReal").datepicker(
                'option',
                'minDate',
                selectedDate
            )
        }
    });
    $("#fimVencimentoReal").datepicker({
        minDate: $("#inicioVencimentoReal").val(),
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1
    });
    $("#inicioPagamento").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        onClose: function(selectedDate){
            $("#fimPagamento").datepicker(
                'option',
                'minDate',
                selectedDate
            )
        }
    });
    $("#fimPagamento").datepicker({
        minDate: $("#inicioPagamento").val(),
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1
    });

//  $( "button, input:submit").button();
    var loadPicker = function() {
        $(".data").datepicker({
            inline: true,
        });
        var data_fechamento = $("#data-fechamento").val();
        if(data_fechamento) {
            data_fechamento = data_fechamento.split('-');
            $("#entrada, #vencimento, .imposto-retido, .verifica-fechamento").datepicker({
                inline: true,
                minDate: new Date(parseInt(data_fechamento[0]), parseInt(data_fechamento[1])-1, parseInt(data_fechamento[2]) + 1),
            });
        } else {
            $("#entrada, #vencimento, .imposto-retido, .verifica-fechamento").datepicker({
                inline: true,
            });
        }
    };


    loadPicker();

    $("#div-xml-nota").hide();


    $("#div-xml-nota").dialog({
        autoOpen: false, modal: true, width: 600, position: 'top',
        open: function(event,ui){

        },
        buttons: {
            "Fechar" : function(){
                $(this).dialog("close");
            }
        }
    });

    $("#xml_nota").click(function(){
        $("#div-xml-nota").dialog("open");

    });


    ///////////////////////////////////////jeferson auto soma nota e impostos


    $(".valor_parcela").live('blur',function(event){
        $('#mensagem-erro-parcelas').html();
        val = parseFloat(replaceValorMonetarioFloat($(this).val()));
        var val2 = 0.00;
        $(".valor_parcela").each(function(){
            val2 += parseFloat(replaceValorMonetarioFloat($(this).val()));
        });

        tot = parseFloat(replaceValorMonetarioFloat($("input[name='valor_nota']").val()));
        if(tot == 0 || tot == NaN){
            $('#mensagem-erro-parcelas').append(
                '<p style="color: red"><b>Valor do Produto/Serviço ainda não foi prenchido.</b></p>'
            );
            $(this).attr('value', '0,00');
            return;

        }

        val2 = val2 - val;
        val2 = val2 + val;
        if(val2.toFixed(2) > tot){
            $('#mensagem-erro-parcelas').append(
                '<p style="color: red"><b>A soma da(s) parcela(s) maior que o valor total.</b></p>'
            );
            //alert("A soma da(s) parcela(s) maior que o valor total.");
            $(this).attr('value','0,00');
        }
    });

    $(".tipo-calculo-rateio").click(function(){
        $(".valor_rateio").each(function(){
            $(this).val('0,00');
            $(this).parents('tr').find('.porcento').val(0);
            $(this).parents('tr').find('.porcento').attr('valor_real', 0)
        });
    });

    

    

    $(".valor_rateio").live('blur',function(){
        var val_nota = $('#valor_nota').val();
        var val_bruto = $('#val_produtos').val();
        var val1 = parseFloat(replaceValorMonetarioFloat($(this).val()));
        var val2 = 0;
        val2 = parseFloat(val2);

        $(".valor_rateio").each(function(){
            valor_parcela = replaceValorMonetarioFloat($(this).val());
            val2 += parseFloat(valor_parcela);
        });

        var valor_calculo = $(".tipo-calculo-rateio:checked").val() == '0' ? val_nota : val_bruto;
        tot = parseFloat(replaceValorMonetarioFloat(valor_calculo));

        if(tot == 0 || tot == NaN){
            alert('Valor da nota ainda não foi prenchido.');
            $(this).attr('value','0,00');
            return;
        }

        val2 = val2 - val1;
        val2 = val2 + val1;

        if(val2.toFixed(2) > tot){
            alert("Valores Rateio Maior que o valor da nota");
            $(this).attr('value','0,00');
        }

        porc = (val1 *100)/tot ;

        $(this).parent().parent().children('td').eq(2).children('input').attr('valor_real',porc.toFixed(5));
        porc= float2moeda(porc);
        $(this).parent().parent().children('td').eq(2).children('input').attr('value',porc);
    });

    /* $(".porcento").live('blur',function(){
         val = parseFloat($(this).attr('valor_real').replace('.','').replace(',','.'));
         val_nota= parseFloat($('#valor_nota').val().replace('.','').replace(',','.'));
         val12= val/100;
         x = val12*val_nota;
         x= float2moeda(x);

         var val2 = 0.00;
         if(val_nota == 0 || val_nota == NaN){
             alert('Valor da nota ainda não foi preenchido.');
             $(this).attr('value','0,00');
             return;

         }
         $(".porcento").each(function(){
             val2 += parseFloat($(this).val().replace('.','').replace(',','.'));
         });


         val2 = val2 - val;
         val2 = val2 + val;
         if(val2 > 100.00){
             alert("Percentual maior que 100%");
             $(this).attr('value','0,00');
         }else{
             $(this).parent().parent().children("td").eq(1).children("input").attr("value",x);
         }
     });*/



    $(".calcular_val_nota").blur(function(){
        calcular_total_nota();

    });

    $("#calcular-retencoes").click(function(){
        var produtos = verificaNaN($("input[name='val_produtos']").val());
        produtos = parseFloat(replaceValorMonetarioFloat(produtos));
        calculaAliquotas(produtos);
        calcular_total_nota();
        $("#calcular-vencimentos").trigger('click');
    });

    function calcular_total_nota(){
        var pcc='';
        if($('#check_pcc').is(':checked')){
            pcc = verificaNaN($("input[name='pcc']").val());
            pcc = parseFloat(replaceValorMonetarioFloat(pcc));

        }else{
            var pis   = verificaNaN($("input[name='pis']").val());
            pis = parseFloat(replaceValorMonetarioFloat(pis));
            var cofins  = verificaNaN($("input[name='cofins']").val());
            cofins = parseFloat(replaceValorMonetarioFloat(cofins));
            var csll = verificaNaN($("input[name='csll']").val());
            csll = parseFloat(replaceValorMonetarioFloat(csll));
            pcc=pis+cofins+csll;

        }

        var produtos = verificaNaN($("input[name='val_produtos']").val());
                
        produtos = parseFloat(replaceValorMonetarioFloat(produtos));
        var des_acessorias = verificaNaN($("input[name='des_acessorias']").val());
        des_acessorias = parseFloat(replaceValorMonetarioFloat(des_acessorias));

        var ir = verificaNaN($("input[name='ir']").val());
        ir = parseFloat(replaceValorMonetarioFloat(ir));

        var issqn = verificaNaN($("input[name='issqn']").val());
        issqn = parseFloat(replaceValorMonetarioFloat(issqn));

        var des_bon = verificaNaN($("input[name='des_bon']").val());
        des_bon = parseFloat(replaceValorMonetarioFloat(des_bon));

        var inss = verificaNaN($("input[name='inss']").val());
        inss = parseFloat(replaceValorMonetarioFloat(inss));

        var icms= verificaNaN($("input[name='icms']").val());
        icms = parseFloat(replaceValorMonetarioFloat(icms));

        var frete = verificaNaN($("input[name='frete']").val());
        frete = parseFloat(replaceValorMonetarioFloat(frete));

        var ipi = verificaNaN($("input[name='ipi']").val());
        ipi = parseFloat(replaceValorMonetarioFloat(ipi));

        var seguro= verificaNaN($("input[name='seguro']").val());
        seguro = parseFloat(replaceValorMonetarioFloat(seguro));

        // ver calculo com celso
        if($('.tipo_nota').val()==1){
            var tot = produtos+des_acessorias+icms+ipi+seguro+frete+ (-1*(ir+issqn+des_bon+inss+pcc));
        }else{
            var tot = produtos+des_acessorias+icms+ipi+seguro+frete+ (-1*(ir+issqn+des_bon+inss+pcc));
        }

        if(tot >=0){
            $("#novaentrada").show();
            $("#novaentrada1").show();
        }else{
            $("#novaentrada").hide();
            $("#novaentrada1").hide();
        }

        tot = float2moeda(tot);

        $("input[name='valor_nota']").val(tot);

        if($(".valor_parcela").length == 1) {
            $(".valor_parcela:first").val(tot);
        }
        if($(".valor_rateio").length == 1) {
            $(".valor_rateio:first").val(tot);
            $(".porcento:first")
                .val('100,00')
                .attr('valor_real', '100,00');
        }

    }

    function calculaAliquotas (valor_produtos) {
        $("input[name='issqn']").val('0,00');
        $("input[name='ir']").val('0,00');
        $("input[name='ipi']").val('0,00');
        $("input[name='icms']").val('0,00');
        $("input[name='pis']").val('0,00');
        $("input[name='cofins']").val('0,00');
        $("input[name='csll']").val('0,00');
        $("input[name='inss']").val('0,00');
        if($("input[name='issqn']").attr('aliquota') > 0) {
            var aliquota_iss = parseFloat($("input[name='issqn']").attr('aliquota'));
            var total_iss = (valor_produtos * aliquota_iss) / 100;
            total_iss = float2moeda(total_iss);
            $("input[name='issqn']").val(total_iss);
        }

        if($("input[name='ir']").attr('aliquota') > 0) {
            var aliquota_ir = parseFloat($("input[name='ir']").attr('aliquota'));
            var total_ir = (valor_produtos * aliquota_ir) / 100;
            total_ir = float2moeda(total_ir);
            $("input[name='ir']").val(total_ir);
        }

        if($("input[name='ipi']").attr('aliquota') > 0 && $(".tipo_nota:checked").val() == '1') {
            var aliquota_ipi = parseFloat($("input[name='ipi']").attr('aliquota'));
            var total_ipi = (valor_produtos * aliquota_ipi) / 100;
            total_ipi = float2moeda(total_ipi);
            $("input[name='ipi']").val(total_ipi);
        }

        if($("input[name='icms']").attr('aliquota') > 0  && $(".tipo_nota:checked").val() == '1') {
            var aliquota_icms = parseFloat($("input[name='icms']").attr('aliquota'));
            var total_icms = (valor_produtos * aliquota_icms) / 100;
            total_icms = float2moeda(total_icms);
            $("input[name='icms']").val(total_icms);
        }

        if(!$('#check_pcc').is(':checked')) {
            if ($("input[name='pis']").attr('aliquota') > 0) {
                var aliquota_pis = parseFloat($("input[name='pis']").attr('aliquota'));
                var total_pis = (valor_produtos * aliquota_pis) / 100;
                total_pis = float2moeda(total_pis);
                $("input[name='pis']").val(total_pis);
            }

            if ($("input[name='cofins']").attr('aliquota') > 0) {
                var aliquota_cofins = parseFloat($("input[name='cofins']").attr('aliquota'));
                var total_cofins = (valor_produtos * aliquota_cofins) / 100;
                total_cofins = float2moeda(total_cofins);
                $("input[name='cofins']").val(total_cofins);
            }

            if ($("input[name='csll']").attr('aliquota') > 0) {
                var aliquota_csll = parseFloat($("input[name='csll']").attr('aliquota'));
                var total_csll = (valor_produtos * aliquota_csll) / 100;
                total_csll = float2moeda(total_csll);
                $("input[name='csll']").val(total_csll);
            }
        }

        if($("input[name='inss']").attr('aliquota') > 0) {
            var aliquota_inss = parseFloat($("input[name='inss']").attr('aliquota'));
            var total_inss = (valor_produtos * aliquota_inss) / 100;
            total_inss = float2moeda(total_inss);
            $("input[name='inss']").val(total_inss);
        }
    }

    function verificaNaN (param){
        if(param=='NaN' ||  param==null || param==''){
            param = '0,00';
            return param;
        }else {

            return param;}


    }

    function float2moeda(num) {

        x = 0;

        if(num<0) {
            num = Math.abs(num);
            x = 1;
        }
        if(isNaN(num)) num = "0";
        cents = Math.floor((num*100+0.5)%100);

        num = Math.floor((num*100+0.5)/100).toString();

        if(cents < 10) cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
            num = num.substring(0,num.length-(4*i+3))+'.'
                +num.substring(num.length-(4*i+3));
        ret = num + ',' + cents;
        if (x == 1) ret = ' - ' + ret;return ret;

    }

    ///////////////////////////fim  auto soma nota e impostos
    /////Demontrativo por ur 23-09-2013 jeferson
    $("#todos_ur").click(function(){
        if($(this).is(':checked')){

            $(this).parent().children('select').removeClass("COMBO_OBG");
            $(this).parent().children('select').val('-1');
        }else{
            $(this).parent().children('select').addClass("COMBO_OBG");

        }

    });
    ////////////////pesquisar demonstrativo
    $("#pesquisar-demonstrativo").click(function(){
        if(validar_campos("div-demonstrativo")){
            var dados = $("form").serialize();
            // alert(dados);
            var nota = "query=pesquisar-demonstrativo&"+dados;

            $.ajax({
                type: "POST",
                url: "query.php",
                data: nota,
                success: function(msg){
                    $("#divsubnatureza").html(msg);

                }
            });
            return false;
        }

    });

    //////fim pesquisar demonstrativo

    $("#gerar-relatorio-custos").click(function(){
        if(validar_campos("custos-container")){
            var inicio = $("#inicio").val();
            var fim = $("#fim").val();
            var ur = $("#ur").val();
            var tipo = "view";

            $.ajax({
                type: "POST",
                url: "custos.php",
                data: {inicio: inicio, fim: fim, ur: ur, tipo: tipo},
                success: function(data){
                    $("#resultado").html(data);
                }
            });
            return false;
        }

    });

    //Eriton Pesquisar extrato bancario 06-05-2013
    $("#pesquisar-extrato").click(function(){
        if(validar_campos("div-extrato-bancario")){
            var dados = $("form").serialize();

            var extrato = "query=pesquisar-extrato&"+dados;

            $.ajax({
                type: "POST",
                url: "query.php",
                data: extrato,
                success: function(msg){
                    $("#div-extrato-bancario-resultado").html(msg);

                }
            });
            return false;
        }

    });
    //Eriton Fim pesquisar extrato bancario 06-05-2013

    ///////////Jeferso pesquisar concilia��o 21-05-2013
    $("#pesquisar-conciliacao").click(function(){
        if(validar_campos("div-conciliacao")){
            var dados = $("form").serialize();
            // alert(dados);
            var nota = "query=pesquisar-conciliacao&"+dados;

            $.ajax({
                type: "POST",
                url: "query.php",
                data: nota,
                success: function(msg){
                    $("#div-conciliacao-resultado").html(msg);
                    loadPicker();

                }
            });
            return false;
        }

    });
    /////////// FIM Jeferson pesquisar concilia��o 21-05-2013
    ////////////////conciliacao bancaria jeferson 2013-05-22

    $(".conciliado").live('click',function(){//Converte em ponto flutuante e atribui o valor do saldo aos totais para realiza��o de opera��es
        // var saldo_conciliado=parseFloat($("#saldo_conciliado").html().replace('.','').replace(',','.'));
        var saldo_conciliado=parseFloat($("#saldo_conciliado").attr('saldo_conciliado'));

        if(isNaN(saldo_conciliado) == true){
            saldo_conciliado = 0;
        }

        var id_parcela = $(this).attr('id_parcela');
        var inputData = $("input[id_parcela='"+id_parcela+"'][name='data_conciliado']");


        if($(this).val() == 'S' && $(this).attr('alterado') == 'N') {
            inputData.show();
            valor = parseFloat($(this).attr('valor'));
            ////Atributo tipo � o tipo da nota.  0 � recebimento soma o valor com o saldo conciliado caso  1 � saida  subtrai o valor do saldo conciliado
            if($(this).attr('tipo') == 0){

                conciliado = saldo_conciliado + valor  ;
            }else{
                conciliado = saldo_conciliado  - valor;
            }
            $(this).attr('alterado','S');
            $(this).parent().parent().children('td').eq('6').children('input').eq(1).attr('alterado','S');
            $(this).parent().parent().attr('alterado','S');
            //// verificar se conciliado � maior ou menor que zero
            if(conciliado < 0){
                conciliado= conciliado.toFixed(2)
                $("#saldo_conciliado").attr('saldo_conciliado',conciliado);
                conciliado = float2moeda(conciliado);
                $("#saldo_conciliado").html(conciliado);
                $("#saldo_conciliado").addClass('textRed');

            }else{
                ////conciliado no padr�o 0.00
                conciliado= conciliado.toFixed(2)
                $("#saldo_conciliado").attr('saldo_conciliado',conciliado);
                conciliado = float2moeda(conciliado);
                $("#saldo_conciliado").html(conciliado);
                $("#saldo_conciliado").removeClass('textRed');
            }

        }

        if($(this).val() == 'N' && $(this).attr('alterado') == 'S'){
            inputData.hide();

            valor = parseFloat($(this).attr('valor'));

            ////Atributo tipo � o tipo da nota.  0 � recebimento subtrai o valor com o saldo conciliado caso  1 � saida  soma o valor do saldo conciliado
            if($(this).attr('tipo') == 0){

                conciliado = saldo_conciliado - valor;
            }else{

                conciliado = saldo_conciliado  + valor;
            }

            $(this).attr('alterado','N');
            $(this).parent().parent().children('td').eq('6').children('input').eq(0).attr('alterado','N');
            $(this).parent().parent().attr('alterado','N');
            if(conciliado < 0){
                conciliado= conciliado.toFixed(2)
                $("#saldo_conciliado").attr('saldo_conciliado',conciliado);
                conciliado = float2moeda(conciliado);
                $("#saldo_conciliado").html(conciliado);
                $("#saldo_conciliado").addClass('textRed');

            }else{
                conciliado= conciliado.toFixed(2)
                $("#saldo_conciliado").attr('saldo_conciliado',conciliado);
                conciliado = float2moeda(conciliado);
                $("#saldo_conciliado").html(conciliado);
                $("#saldo_conciliado").removeClass('textRed');
            }
        }

        $(this).parent().parent().attr('conciliado',$(this).val());

    });

    

    //Eriton Salvar Conta Bancaria 04-06-2013
    $("#salvar-conta").click(function(){
        if (validar_campos("div-bancos")) {
            var tipo = $('#tipo_conta').val();
            var banco = $('#id_banco').val();
            if (banco == -1) {
                alert('Banco Inválido! Selecione outro banco.');
                return false;
            }
            var agencia = $('#agencia').val();
            var conta = $('#conta').val();
            var ur = $('.empresa').val();
            var saldo_inicial = $('#saldo_inicial').val();
            var data = $('#entrada').val();
            var status = $('#status').val();
            var ipcc = $('#ipcc').val();

            $.post( "query.php",{
                    id_tipo:tipo,
                    id_banco:banco,
                    agencia:agencia,
                    conta:conta,
                    ur:ur,
                    saldo_inicial:saldo_inicial,
                    data:data,
                    status:status,
                    ipcc:ipcc,
                    query: "salvar-conta"
                },
                function(msg){

                    //$("#trGerada").html('<p>Conta salva com Sucesso!</p><p>TR '+msg+'</p>');
                    if(msg == 1){
                        alert('Conta salva com Sucesso!');
                        location.reload();
                    }else{
                        alert('A conta nao foi salva.');
                    }
                    //$("#dialog-message").dialog('open');

                }
            );
        }
        return false;

    });

    $("#editar-conta").click(function(){
        if(validar_campos("div-bancos")){
            var tipo = $('#tipo_conta').val();
            var banco = $('#id_banco').val();
            if (banco == -1) {
                alert('Banco Inválido! Selecione outro banco.');
                return false;
            }
            var agencia = $('#agencia').val();
            var conta = $('#conta').val();
            var id_conta = $('#id_conta').val();
            var ipcc = $('#ipcc').val();


            $.post( "query.php",{
                    id_tipo:tipo,
                    id_conta:id_conta,
                    id_banco:banco,
                    agencia:agencia,
                    conta:conta,
                    ipcc:ipcc,
                    query: "editar-conta"
                },
                function(msg){

                    //$("#trGerada").html('<p>Conta salva com Sucesso!</p><p>TR '+msg+'</p>');
                    if(msg == 1){
                        alert('Conta Editada com Sucesso!');
                        window.location.href='?op=listarContasBancarias';
                    }else{
                        console.log(msg);
                        alert('A conta nao foi Editada.');
                    }
                    //$("#dialog-message").dialog('open');

                }
            );
        }
        return false;

    });

    //fim Eriton Salvar Conta Bancaria 04-06-2013
    $("#salvar-conciliacao").live('click',function(){

        if(validar_campos("div-conciliacao-resultado")){
            var  inicio= $(this).attr('inicio');
            var  fim = $(this).attr('fim');
            var  banco_id = $(this).attr('id_banco');
            var  saldo_n_conciliado = $("#saldo_nconciliado").attr('saldo_nconciliado');
            var  saldo_conciliado = $("#saldo_conciliado").attr('saldo_conciliado');
            qtd_linha = $(".dados").size();
            var linha ='';
            $(".dados").each(function(i){
                var  fornecedor  = $(this).attr('fornecedor');
                var tipo_nota =$(this).attr('tipo');
                var data_pagamento = $(this).attr('data');
                var tr = $(this).attr('tr');
                var conciliado = $(this).attr('conciliado');
                var num_documento = $(this).children('td').eq(3).html();
                var valor = $(this).attr('valor_pago');

                var id_parcela = $(this).attr('id_parcela');
                var transferencia = $(this).attr('transferencia');
                var id_parcela = $(this).attr('id_parcela');
                var inputData = $("input[id_parcela='"+id_parcela+"'][name='data_conciliado']").val();
                var data_conciliado =inputData;
                var alterado = $(this).attr('alterado');

                /* if(){

                 }*/
                linha += tr+"#"+tipo_nota+"#"+fornecedor+"#"+data_pagamento+"#"+conciliado+"#"+valor+"#"+num_documento+"#"+id_parcela+"#"+transferencia+"#"+data_conciliado+"#"+alterado;

                if(i< qtd_linha - 1){
                    linha += "$";
                }
                //alert(linha);
            });



            $.post( "query.php",{
                    query: "salvar-conciliacao",
                    inicio:inicio,
                    fim:fim,
                    banco_id:banco_id,

                    linha:linha},
                function(msg){

                    if(msg == 1){
                        alert("Concilia\u00e7\u00e3o Salva.");
                        //$("#dialog-message").dialog('open');
                        location.reload();

                    }else{
                        alert(msg);
                        return false;
                    }
                }
            );


        }else{
            return false;
        }


    });


///////////////conciliacao bancaria jeferson 2013-05-22 fim


    $(".valor").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $("#limparvalor").click(function(){
        $("#div-busca input[name='valor']").val("");
        return false;
    });

    $("#buscar").click(function(){

        var dados = $("form").serialize();
        var nota = "query=buscar&"+dados;

        $.ajax({
            type: "POST",
            url: "query.php",
            data: nota,
            success: function(msg){
                $("#resultado-busca").html(msg);

            }
        });
        return false;

    });

    $(".paginacao-parcela").live('click',function(){     

        var dados = $(this).attr('dados');     
        $.ajax({
            type: "POST",
            url: "query.php",
            data: dados,
            success: function(msg){
                $("#resultado-busca").html(msg);

            }
        });
        return false;

    });

    $('.estornar-parcela').live('click', function () {
        var $this = $(this);
        var parcela = $this.attr('parcela-id');
        var valor_estorno = $this.attr('valor-estorno');
        var valor_original = $this.attr('valor-original-parcela');
        $('#valor-estorno').val(valor_estorno);
        $('#valor-original-parcela').val(valor_original);
        $('#parcela-id-estornar').val(parcela);
        $('#modal-estornar-parcela').modal("show");
    });

    $('#salvar-estornar-parcela').live('click', function () {
        var $this = $(this);
        var parcela = $('#parcela-id-estornar').val();
        var valor_original = $('#valor-original-parcela').val();
        var valor_estorno = $('#valor-estorno').val();
        var conta = $('#conta').val();
        var data_agendamento = $('#data-agendamento').val();
        var data_estorno = $('#data-estorno').val();
        var data = {
            parcela: parcela,
            valor_estorno: valor_estorno,
            conta: conta,
            data_agendamento: data_agendamento,
            data_estorno: data_estorno
        };
        if(conta && valor_estorno != '0,00' && data_agendamento && data_estorno) {
            $.post(
                '/financeiros/estornar-parcela/?action=estornar-parcela',
                data,
                function (response) {
                    alert('Parcela estornada com sucesso!');
                    var $parcelaRow = $('#parcela-' + parcela);
                    $parcelaRow
                        .find('.data-pagto')
                        .text('');
                    $parcelaRow
                        .find('.status-parcela')
                        .text('Pendente');
                    $parcelaRow
                        .find('.tem-bordero-parcela')
                        .text('NÂO');
                    $parcelaRow
                        .find('.valor-pagto')
                        .text('R$ ' + float2moeda(valor_original));
                    $('#parcela-id-estornar').val('');
                    $('#data-agendamento').val('');
                    $('#data-estorno').val('');
                    $('#conta').val('');
                    $('.chosen-select').trigger('chosen:updated');
                    $('#modal-estornar-parcela').modal("hide");
                }
            );
        } else {
            alert('Prencha os campos corretamente!');
        }
    });

    $('.desfazer-estorno-parcela').live('click', function () {
        var $this = $(this);
        var estorno_id = $this.attr('estorno-id');
        var credito_id = $this.attr('credito-id');
        var data = {
            estorno_id: estorno_id,
            credito_id: credito_id
        };
        if(confirm('Tem certeza que deseja desfazer esse estorno?')) {
            $.post(
                '/financeiros/estornar-parcela/?action=desfazer-estorno-parcela',
                data,
                function (response) {
                    alert('Estorno desfeito com sucesso!');
                    $('.estorno-' + estorno_id).remove();
                    $('.tarifa-' + credito_id).remove();
                }
            );
        }
    });

    $(".imprimir_parcelas").live('click',function(){

        var dados = $("form").serialize();
        var nota = dados;
        window.open('imprimir_parcelas.php?'+dados,'_blank');

        return false;

    });
    $("#buscar_nota").click(function(){

        var dados = $("form").serialize();
        var nota = "query=buscar_nota&"+dados;
        $.ajax({
            type: "POST",
            url: "query.php",
            data: nota,
            success: function(msg){
                $("#resultado-busca-nota").html(msg);
                $('[data-toggle="tooltip"]').tooltip();
            }
        });
        return false;

    });
    $(".paginacao-contas").live('click',function(){     

        var dados = $(this).attr('dados');     
        $.ajax({
            type: "POST",
            url: "query.php",
            data: dados,
            success: function(msg){
                $("#resultado-busca-nota").html(msg);

            }
        });
        return false;

    });

    $("#voltar").click(function(){
        history.go(-1);

    });

    $("#div-busca input[name='tnaturezas']").click(function(){
        if(!$("#div-busca input[name='tnaturezas']").attr("checked"))
            $.get("get.php",{q: $("#natureza").val(), b: "true"},function(retorno){
                $("#divsubnatureza").html(retorno);
            });
        else $("#divsubnatureza").html("");
    });

    $("#natureza").change(function(){
        $.get("get.php",{q: $("#natureza").val(), b: "true"},function(retorno){
            $("#divsubnatureza").html(retorno);
        });
    });

    $("#natureza-nota").change(function(){
        $.get("get.php",{q: $("#natureza-nota").val(), b: "false"},function(retorno){
            $("#divsubnatureza").html(retorno);
        });
    });

    /////////////////////////////jefersosn 04/09/2013 transferência bancaria
    $("#select-transferencia").change(function(){
        if($(this).val()==1){
            $("#span-salvar-transferencia").attr('style','');
            $("#span_valor_transferencia").attr('style','');
            $("#span-pesquisar-transferencia").attr('style','display:none');
            $("#valor_transferencia").addClass('OBG');
            $("select[name='origemFundos']").addClass('COMBO_OBG');
            $("#data").addClass('OBG');
            $("#span-data").attr('style','');
            $("#span_doc_bancario").attr('style','');
            // $("#doc_bancario").addClass('OBG');
            $("#span_descricao").attr('style','');
            //$("#descricao").removeClass('OBG');
            $("#span-periodo").attr('style','display:none');
        }else{
            $("#span-pesquisar-transferencia").attr('style','');
            $("#span-salvar-transferencia").attr('style','display:none');
            $("#span_valor_transferencia").attr('style','display:none');
            $("select[name='origemFundos']").removeClass('COMBO_OBG');
            $("#valor_transferencia").attr('style','display:none');
            $("#valor_transferencia").attr('style','');
            $("#valor_transferencia").removeClass('OBG');
            $("#valor_transferencia").val('');
            $("#span_doc_bancario").attr('style','display:none');
            // $("#doc_bancario").removeClass('OBG');
            $("#doc_bancario").val('');
            $("#span_descricao").attr('style','display:none');
            // $("#descricao").removeClass('OBG');
            $("#descricao").val('');
            $("#data").removeClass('OBG');
            $("#span-data").attr('style','display:none');
            $("#span-periodo").attr('style','');
        }

    });

    $("#salvar-transferencia").click(function(){
        if(validar_campos("div-transferencia")){
            ///alert("ok");
            var data = $("#data").val();
            var conta_origem =$("#transferencia_origem").children('select').val();
            var conta_destino =$("#transferencia_destino").children('select').val();
            var valor_transferido = parseFloat(replaceValorMonetarioFloat($("#valor_transferencia").val()));
            var doc_bancario=$("#doc_bancario").val();
            var descricao =$("#descricao").val();
            $.post( "query.php",{
                    query: "salvar-transferencia",
                    data:data,
                    conta_origem:conta_origem,
                    conta_destino:conta_destino,
                    valor_transferido:valor_transferido,
                    doc_bancario:doc_bancario,
                    descricao:descricao
                },
                function(msg){

                    if(msg == 1){
                        alert("Transferência salva com sucesso.");
                        location.reload();
                    }else{
                        alert(msg);
                        return false;
                    }
                }
            );

        }
    });

    $("#pesquisar-transferencia").click(function(){
        if(validar_campos("div-transferencia")){
            ///alert("ok");
            var inicio = $("#inicio").val();
            var fim = $("#fim").val();
            var conta_origem =$("#transferencia_origem").children('select').val();
            var conta_destino =$("#transferencia_destino").children('select').val();

            $.post( "query.php",{
                    query: "pesquisar-transferencia",
                    inicio:inicio,
                    fim:fim,
                    conta_origem:conta_origem,
                    conta_destino:conta_destino
                },
                function(msg1){
                    $("#div-pesquisa-transferencia").html(msg1);
                }
            );

        }

    });

    ///////////////////////////fim transferencia
    ///////////Jeferson 16-09-2013 Cadastrar porcentagem por ipcg3 para conferir com o demosntrativo.
    $("#salvar-porcentagem-ipcg").live('click',function() {
        var cont = 0;
        ////////Verifica se algum item está checado.
        $(".alterar_porcento").each(function() {
            if ($(this).is(':checked')) {
                cont++;
            }
        });
        ///caso algum item estejá checado séra salvo no banco.
        if (cont > 0) {
            var qtd = $(".alterar_porcento:checked").size();
            var linha ='';
            //monta a linha com os valores a serem gravados para cada item no banco separando as linhas por $
            $(".alterar_porcento:checked").each(function(i) {

                var porcento = parseFloat($(this).parent().parent().children('td').eq(4).children('input').val().replace('.', '').replace(',', '.'));
                var n3 = $(this).parent().parent().children('td').eq(4).children('input').attr('n3');
                var n2 = $(this).parent().parent().children('td').eq(4).children('input').attr('n2');
                var cod_n3 = $(this).parent().parent().children('td').eq(4).children('input').attr('cod_n3');
                var cod_n2 = $(this).parent().parent().children('td').eq(4).children('input').attr('cod_n2');
                var ur = $(this).parent().parent().children('td').eq(4).children('input').attr('ur');
                var anterior = $(this).parent().parent().children('td').eq(4).children('input').attr('anterior');
                linha += n3 + "#" + cod_n3 + "#" + cod_n2 + "#" + porcento+ "#" + ur+ "#" + anterior+"#"+n2;
                ////verifica se não é a ultima linha, porque essa n deve receber o $ no fim.
                if (i < qtd - 1) {
                    linha += "$";
                }


            });
            //////////envia para a query.php os dados a serem gravados
            $.post("query.php", {
                    query: "salvar-porcentagem-ipcg",
                    linha: linha
                },
                function(msg) {

                    alert(msg);
                    window.location.reload();
                }
            );


        }else{/////nenhum item foi checado não houve alteração
            alert("N\u00e3o houve altera\u00e7\u00e3o");
        }

    });
    ////////// Local:cadastrar percentual para o plano de contas.
    //Botão pesquisar. Envia a ID da UR para pesquisar se já tem algum item do plano de contas com o percentual cadastrado.
    $("#pesquisar-porcentagem-ipcg").click(function(){
        /////Verifica se a empresa foi escolhida.
        if(validar_campos("div-cadastro-porcento-ipcg")){
            var empresa=$('.empresa').val();
            var nome_empresa = $('.empresa').find('option').filter(':selected').text();
            $.post( "query.php",{
                    query: "pesquisar-porcentagem-ipcg",
                    empresa:empresa,
                    nome_empresa:nome_empresa

                },
                function(msg){
                    ///Retorna o ipcg3 do plano de contas para que associe o percentual.
                    $("#pesquisar-percentual").html(msg);
                }
            );

        }

    });


    $(".status_relatorio").change(function(){
        if($(this).val() == 'Pendente'){
            $("#contas").hide();
        }else{
            $("#contas").show();
        }
    });

    ////////  Local:cadastrar percentual para o plano de contas.
    /// Chekbox que veio do retorno da pesquisa, quando é checked habilita o campo porcento para alteração.
    $(".alterar_porcento").live('click',function(){
        $(".valor").priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });

        if($(this).is(':checked')){   ///////Habilita o input para receber valor
            $(this).parent().parent().children('td').eq(4).children('input').removeAttr('disabled');

        }else{
            var  porcento_anterior =$(this).attr('porcento');
            var porcento_atual=$(this).parent().parent().children('td').eq(4).children('input').val();
            ///verifica se houve alteração no valor da porcentagem
            if(porcento_anterior == porcento_atual){//Porcentagem igual a anterior permit desabilitar o input e desmarcar o checkbox
                $(this).parent().parent().children('td').eq(4).children('input').attr('disabled','disabled');
            }else{/////Porcentagem direferente não permite desabilitar o input e nem desmarcar o checked
                $(this).attr('checked','checked');
                alert("Voc\u00ea alterou o percentual ele precisa ficar checado.");
            }
        }
    });

    /////////////Verificar se o somatório da porcentagem dos itens de recebimento é menor ou igual a 100.
    $(".porcento_recebimento").live('blur',function(){

        var val = parseFloat($(this).val().replace('.','').replace(',','.'));
        var val2 = 0.00;
        $(".porcento_recebimento").each(function(){
            val2 += parseFloat($(this).val().replace('.','').replace(',','.'));
        });
        if(val2.toFixed(2) > 100){
            alert("A soma da(s) parcela(s) maior que o valor total.");
            $(this).attr('value','0,00');
        }
    });
    /////////////Verificar se o somatório da porcentagem dos itens de desembolso é menor ou igual a 100.
    $(".porcento_desembolso").live('blur',function(){

        var val = parseFloat($(this).val().replace('.','').replace(',','.'));
        var val2 = 0.00;
        $(".porcento_desembolso").each(function(){
            val2 += parseFloat($(this).val().replace('.','').replace(',','.'));
        });
        if(val2.toFixed(2) > 100){
            alert("A soma da(s) parcela(s) maior que o valor total.");
            $(this).attr('value','0,00');
        }
    });



    ////////fim Cadastrar porcentagem por ipcg3
    $("#dialog-edit-conta-banco").dialog({
        minWidth: 500,minHeight: 200,autoOpen: false, modal: true,
        buttons: {
            Ok: function() {
                $.post("query.php",{query: "edit-conta-banco", cod: $(this).attr('cod'), v: $("input[name='conta']:checked").val()},
                    function(r){
                        alert(r);
                    });
                $( this ).dialog( "close" );
            },
            Cancelar: function(){
                $( this ).dialog( "close" );
            }
        }
    });

    $("input[name='cidade_und']").autocomplete({
        source: "/utils/busca_cidade.php",
        minLength: 3,
        select: function( event, ui ) {
            //$("input[name='busca-cidade']").val('');
            $("input[name='id_cidade_und']").val(ui.item.id);
            $("input[name='cidade_und']").val(ui.item.value);
            $('#local').focus();
            ///$('#busca-cidade').val('');
            return false;
        }
    });

    var json_fornecedor = '';
    var json_cliente = '';

    $(".tipo_nota").click(function(){
        var x= $(this).val();
        $("#codigo-conta").val('');
        $("#natureza-movimentacao").val('');
        $(".nome-natureza").text('');

        $("#ir").val('0,00');
        $("input[name='valor_ir']").val('0,00');
        $("input[name='vencimento_ir']").attr('disabled',true);
        $("input[name='vencimento_ir']").val('');
        $("input[name='vencimento_ir']").removeClass('OBG');
        $("input[name='vencimento_ir']").attr('style','');
        $("#pis").val('0,00');
        $("#pis").attr('disabled',false);
        $("input[name='valor_pis']").val('0,00');
        $("input[name='vencimento_pis']").attr('disabled',true);
        $("input[name='vencimento_pis']").val('');
        $("input[name='vencimento_pis']").removeClass('OBG');
        $("input[name='vencimento_pis']").attr('style','');
        $("#cofins").val('0,00');
        $("#cofins").attr('disabled',false);
        $("input[name='valor_cofins']").val('0,00');
        $("input[name='vencimento_cofins']").attr('disabled',true);
        $("input[name='vencimento_cofins']").val('');
        $("input[name='vencimento_cofins']").removeClass('OBG');
        $("input[name='vencimento_cofins']").attr('style','');
        $("#csll").val('0,00');
        $("#csll").attr('disabled',false);
        $("input[name='valor_csll']").val('0,00');
        $("input[name='vencimento_csll']").attr('disabled',true);
        $("input[name='vencimento_csll']").val('');
        $("input[name='vencimento_csll']").removeClass('OBG');
        $("input[name='vencimento_csll']").attr('style','');
        $("#pcc").val('0,00');
        $("#check_pcc").attr('checked',false);
        $("#pcc").attr('disabled',true);
        $("input[name='valor_pcc']").val('0,00');
        $("input[name='vencimento_pcc']").attr('disabled',true);
        $("input[name='vencimento_pcc']").val('');
        $("input[name='vencimento_pcc']").removeClass('OBG');
        $("input[name='vencimento_pcc']").attr('style','');
        $("#issqn").val('0,00');
        $("input[name='valor_issqn']").val('0,00');
        $("input[name='vencimento_issqn']").attr('disabled',true);
        $("input[name='vencimento_issqn']").val('');
        $("input[name='vencimento_issqn']").removeClass('OBG');
        $("input[name='vencimento_issqn']").attr('style','');

        if(x == '1') {
            var tipo_forn = 'F';
        } else if(x == '0') {
            var tipo_forn = 'C';
        }

        var $buscarRetFornElem = $(".buscar-retencoes-fornecedor");
        if((json_fornecedor == '' && x == 1) || (json_cliente == '' && x == 0)) {
            $.get('query.php', {query: 'buscar-fornecedor-options', tipo: tipo_forn},
                function (response) {
                    if (response != '0') {
                        if(x == '1') {
                            json_fornecedor = response;
                        } else if(x == '0') {
                            json_cliente = response;
                        }
                        response = JSON.parse(response);
                        $buscarRetFornElem.html('<option disabled selected value="">Selecione...</option>');
                        $.each(response, function (index, value) {
                            var cpfCnpj = value.FISICA_JURIDICA == 1 ? value.cnpj : value.CPF;
                            $(".buscar-retencoes-fornecedor").append(
                                '<option class="fornecedor-option" value="' + value.idFornecedores + '" tipo="' + value.TIPO + '" fisica_juridica="' + value.FISICA_JURIDICA + '">' +cpfCnpj+' '+ value.fornecedor + '</option>'
                            );
                        });
                        $buscarRetFornElem.trigger("chosen:updated");
                    }
                }
            );
        } else {
            var response = x == 1 ? json_fornecedor : json_cliente;
            response = JSON.parse(response);
            $buscarRetFornElem.html('<option disabled selected value="">Selecione...</option>');
            $.each(response, function (index, value) {
                $(".buscar-retencoes-fornecedor").append(
                    '<option class="fornecedor-option" value="' + value.idFornecedores + '" tipo="' + value.TIPO + '" fisica_juridica="' + value.FISICA_JURIDICA + '">' + (value.TIPO == "F" ? value.razaoSocial : value.nome) + '</option>'
                );
            });
            $buscarRetFornElem.trigger("chosen:updated");
        }

        if(x == 0){
            //zerar valor dos impostos retidos
            $(".rateio").each(function(){
                $(".rateio").remove();
            });
            //$("#div_rateio").hide();

            /* TROCANDO OS LABELS */
            $("#icms").attr('disabled','disabled');
            $("#frete").attr('disabled','disabled');
            $("#seguro").attr('disabled','disabled');
            $("#ipi").attr('disabled','disabled');
            $("#icms").val('0,00');
            $("#frete").val('0,00');
            $("#seguro").val('0,00');
            $("#ipi").val('0,00');


            /****************************************************************/


            $("#rateio").append(
                "<tr class='rateio' >" +
                "<td name='empresa_1' style='font-size:2px;'><img align='left' class='del_rateio'  src='../utils/delete_16x16.png' title='Remover' border='0'/></td>"+
            "<td name='1' style='font-size:2px;'><input class='OBG valor valor_rateio' value='0,00' type='text'  maxlength='15' style='text-align:right;font-size:10px;'  /></td>"+
            "<td ><input type='text'  maxlength='15' value='0,00' valor_real='0,00' disabled='disabled'  class='valor OBG porcento '  style='text-align:right;font-size:10px;' />%</td>"+
            "<td class='td_4_1' name='ipcg4_1' >"+
            "<img src='/utils/look_16x16.png' class='buscar-naturezas-dialog-rateio' style='cursor: pointer;' /> &nbsp;"+
            "<input type = 'hidden' class='id-natureza-rateio' />"+
            "<input type = 'text' class='OBG nome-natureza-rateio' style='font-size:9px;' disabled readonly size='50' />"+
           
                                
            
                 "</td>"+
                "<td class='td_5_1' name='assinatura_1'></td>"+
                "</tr>"
            );
           // ipcg4_sel_busca();
           var idEmpresaPrincipal =  $('#empresa-nota').val();
            empresa_sel(idEmpresaPrincipal);
            assinatura_sel();
            $("#rateio tr:odd").css("background-color","#ebf3ff");

            $(".valor").priceFormat({
                prefix: '',
                centsSeparator: ',',
                thousandsSeparator: '.'
            });
            console.log('a');

        }
        if(x == 1){
            /* TROCANDO OS LABELS */
            $("#icms").attr('disabled',false);
            $("#frete").attr('disabled',false);
            $("#seguro").attr('disabled',false);
            $("#ipi").attr('disabled',false);

            console.log('b');

            /****************************************************************/
            /*$("#div_rateio").show();*/
            $(".rateio").each(function(){
                $(".rateio").remove();
            });
            $("#rateio").append(
                "<tr class='rateio' >" +
                "<td name='empresa_1' style='font-size:2px;'><img align='left' class='del_rateio'  src='../utils/delete_16x16.png' title='Remover' border='0'/></td>"+
            "<td name='1' style='font-size:2px;'><input class='OBG valor valor_rateio' value='0,00' type='text'  maxlength='15' style='text-align:right;font-size:10px;'  /></td>"+
            "<td ><input type='text'  maxlength='15' value='0,00' valor_real='0,00' disabled='disabled'  class='valor OBG porcento '  style='text-align:right;font-size:10px;' />%</td>"+
            "<td class='td_4_1' name='ipcg4_1' >"+
            "<img src='/utils/look_16x16.png' class='buscar-naturezas-dialog-rateio' style='cursor: pointer;' /> &nbsp;"+
            "<input type = 'hidden' class='id-natureza-rateio' />"+
            "<input type = 'text' class='OBG nome-natureza-rateio' style='font-size:9px;' disabled readonly size='50' />"+
           
                                
            
                 "</td>"+
                "<td class='td_5_1' name='assinatura_1'></td>"+
                "</tr>");

            //ipcf2_sel();
            // ipcg4_sel_busca();
           var idEmpresaPrincipal =  $('#empresa-nota').val();
           empresa_sel(idEmpresaPrincipal);
            assinatura_sel();
            $("#rateio tr:odd").css("background-color","#ebf3ff");

            $(".valor").priceFormat({
                prefix: '',
                centsSeparator: ',',
                thousandsSeparator: '.'
            });

        }

        calcular_total_nota();

    });

    // $("#editar-vencimento-parcela").click(function(){
    //     var vencimento = $(".vencimento");
    //     vencimento.each(function () {
    //         if ($(this).parent().parent('tr').attr('status') == 'Pendente')
    //             $(this).removeAttr('disabled');
    //     });
        
    //     $('#parcelas').after('<button id="salvar-edicao-vencimento" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" role="button" type="button" aria-disabled="false"><span class="ui-button-text">Atualizar Vencimentos</span></button>');
    // });
    $("#editar-parcela").click(function(){
       var  obj  =  $(this).closest("#div_parcelas");
        if(obj.find('#add_parcela').length == 0){       
            var vencimento = $(".vencimento");
            vencimento.each(function () {
                if ($(this).parent().parent('tr').attr('status') == 'Pendente'){
                    if($(this).parent().parent('tr').attr('emBordero') == 0){
                        $(this).closest('tr').find('.valor-parcela').removeAttr('readonly');                       
                        $(this).removeAttr('disabled');
                        $(this).closest('tr').find('.form_parcelas').removeAttr('readonly');
                    }
                    
                    $(this).closest('tr').find('.codigo-barra').removeAttr('disabled');
                    
                    $(this).closest('tr').find('.selecionar-parcela-exclusao').removeAttr('disabled');
                }
                    
            });
            $('#add_parcela').removeAttr('disabled');
            $('#excluir-parcelas-selecionadas').removeAttr('disabled');
            $('#parcelas').after("<button id='add_parcela' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' type='button' aria-disabled='false'  ><span class='ui-button-text'>Adicionar Parcela + </span</button> "+
            "<button class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' type='button' aria-disabled='false'   id='excluir-parcelas-selecionadas' ><span class='ui-button-text'>Excluir parcelas selecionadas</span</button>"+
            "<button id='salvar-edicao-parcela' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' type='button' aria-disabled='false'><span class='ui-button-text'>Atualizar Parcelas</span></button>");
    }
    return false;
    });

    $('#salvar-edicao-vencimento').live('click', function(){
        var dados 	= [];
        var idNota	= $("input[name='idnota']").val();
        var isValidVencimento = true;
        $(".vencimento").each(function() {
            if ($(this).parent().parent('tr').attr('status') == 'Pendente') {
                var parcela = $(this).attr('data-id-parcela'),
                    novo_vencimento = $(this).val();
                novo_vencimento = novo_vencimento.split('/').reverse().join('-');

                dados.push({'parcela': parcela, 'novo_vencimento': novo_vencimento});
                var emissao = new Date($('.data-emissao-nota').val().split("/").reverse().join("-"));
                novo_vencimento = new Date(novo_vencimento);
                
            if(novo_vencimento < emissao) {
                isValidVencimento = !isValidVencimento;
                $(this).val('');
                return false;
            }
            }
            
        });
        
        if(isValidVencimento == false){
            alert('Data de vencimento não pode ser menor que data de emissão');
            return false;
        }

        $.post('query.php', {query: 'salvar-edicao-vencimento', dados: dados, idNota: idNota},
            function (r) {
                if (r == '1') {
                    alert('Parcelas atualizadas com sucesso!');
                    $(".vencimento").each(function () {
                        $(this).attr('disabled', 'disabled');
                    });
                    $("#salvar-edicao-vencimento").remove();
                } else {
                    alert('Houve um problema ao atualizar a parcela. Favor entrar em contato com o setor de TI.');
                }
            });
    });

    $('#salvar-edicao-parcela').live('click', function(){
        if(validar_campos("div_parcelas")){
            var dados 	= [];
            var arrayParcelaId = [];

        var idNota	= $("input[name='idnota']").val();
        var parcelasPendentes = [];

        var isValidVencimento = true;
        var data_fechamento = new Date($("#data-fechamento").val() +' 01:00:00');
        var emissao = new Date($(".data-emissao-nota").val().split("/").reverse().join("-") +' 01:00:00');
        var val_parcela = 0;
        var tot = parseFloat(replaceValorMonetarioFloat($("#valor_nota").val()));       

        $(".valor_parcela").each(function(){
            val_parcela += parseFloat(replaceValorMonetarioFloat($(this).val()));
        });

        if(tot == '0.00' || tot == '0' ){
            alert("Valor da nota n&aatilde;o pode ser 0,00");
            return false;
        }

        if( val_parcela.toFixed(2) < tot){
            alert("Valor da soma da(s) parcela(s) menor que o valor da nota");
            return false;
        }

        var returnParcela = false;
        
        $(".parcela").each(function() {
           
            if ($(this).attr('status') == 'Pendente') {
               
                var obj = $(this)
                var trp= obj.children('td').eq(1).children('input').val();
                var data= obj.children('td').eq(2).children('input').val();
                var vencimento_real = obj.children('td').eq(3).children('input').val();
                var valor= obj.children('td').eq(4).children('input').val();
                var cod_barra= obj.children('td').eq(5).children('input').val();
                var idParcela = obj.attr('data-id-parcela');
                arrayParcelaId.push(idParcela);

                dados.push({'trp': trp,
                    'vencimento': data,
                    'vencimento_real' : vencimento_real,
                    'valor' : valor,
                    'cod_barra' : cod_barra,
                    'idParcela' : idParcela
                });



                var vencimento = new Date(data.split("/").reverse().join("-"));
                var emissao = new Date($('.data-emissao-nota').val().split("/").reverse().join("-"));

                if(vencimento < emissao) {

                    obj.children('td').eq(2).children('input').val('');
                    returnParcela = true;
                    alert("Vencimento menor que data de emissão.");
                    return false;
                }

                if(vencimento_real < emissao) {
                    returnParcela = true;
                    obj.children('td').eq(3).children('input').val('');
                    alert("Vencimento Real menor que data de emissão.");
                    return false;
                }

                
                
                if(vencimento_real < data_fechamento) {
                    returnParcela = true;
                    obj.children('td').eq(3).children('input').val('');
                    alert("Vencimento real menor que data de fechamento.");
                    return false;
                }
            }
        });

        if(returnParcela){
            return false;
        }    

       
        $(".parcelas-pendentes-editar").each(function() { 
            parcelasPendentes.push({'idParcela' : $(this).val()});
        });

        $.post('query.php', {query: 'salvar-edicao-parcela', 
        dados: dados,
         idNota: idNota, 
         parcelasPendentes : parcelasPendentes,
         arrayParcelaId: arrayParcelaId
        },
            function (r) {
                if (r == '1') {
                    alert('Parcelas atualizadas com sucesso!');
                   window.location.reload();
                } else {
                    alert('Houve um problema ao atualizar a parcela. Favor entrar em contato com o setor de TI.');
                }
            });
        }else{
            alert('Algum campo obrigatório não foi informado.');

            return false;
        }
    });
    

    $("#editar-rateio").click(function(){
        var rateio = $("#rateio");
        var editar = rateio.attr('editar');
        if(editar != 1){
            rateio.attr('editar','1');
            rateio.find('input,select').each(function () {
                $(this).not('.porcento').removeAttr('disabled');
            });
            $("#add_rateio").removeAttr('disabled');
            $('select.empresa').before('<img align="left" class="del_rateio" src="../utils/delete_16x16.png" title="Remover" border="0">');
            $('#add_rateio').after('<button id="salvar-edicao-rateio" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" role="button" type="button" aria-disabled="false"><span class="ui-button-text">Atualizar Rateio</span></button>');
        }
    });

    $('#salvar-edicao-rateio').live('click', function(){
        var dados 	= [];
        var idNota	= $("#rateio").attr('idNota');
        var val_porcento = 0;
        $(".rateio").each(function(){
            var ur			= $(this).children('td').eq(0).children('select').val();
            var valor		= $(this).children('td').eq(1).children('input').val();
            var porcento 	= $(this).children('td').eq(2).children('input').attr('valor_real');
            var ipcf2		= $(this).children('td').eq(3).children('select').val();
            var ipcg3		= $(this).children('td').eq(4).children('select').val();
            var ipcg4		= $(this).children('td').eq(3).find('.id-natureza-rateio').val();
            var assinatura	= $(this).children('td').eq(6).children('select').val();
            val_porcento += parseFloat(porcento);

            dados.push({'ur': ur, 'valor': valor, 'porcento': porcento, 'ipcf2': ipcf2, 'ipcg3': ipcg3, 'ipcg4': ipcg4, 'assinatura': assinatura});
        });
        if(val_porcento.toFixed(2) == 100) {
            $.post('query.php', {query: 'salvar-edicao-rateio', dados: dados, idNota: idNota},
                function (r) {
                    if (r == '1') {
                        alert('Rateio atualizado com sucesso');
                        $("#rateio").find('input,select').each(function () {
                            $(this).not('.porcento').attr('disabled', 'disabled');
                        });
                        $('.del_rateio').remove();
                        $("#add_rateio").attr('disabled', 'disabled');
                        $("#salvar-edicao-rateio").remove();
                    } else {
                        alert('Houve um problema ao atualizar o rateio. Favor entrar em contato com o setor de TI.');
                    }
                });
        }else{
            alert('O(s) valore(s) do(s) rateio(s) não corresponde(m) ao valor total!');
        }
    });

    $("select[name='ipcg3']").change(function(){
        var X = $("select[name='ipcg3'] option:selected").val();

        if(X == "Servi�os"){
        }
    });

    $("select[name='ipcg3_notas']").live('change',function(){
        var X = $(this).val();
        var ipcf2 = $(this).parent().parent().children('td').eq(3).children("select").val();
        var linha = $(this).parent().attr("name");
        var separar= linha.split('_');
        linha =separar[1];
        var tipo_nota = $("input[name='tipo']:checked").val();

        $.post("financeiro.php",{query: "ipcg4", cod:X,tipo_nota:tipo_nota,ipcf2:ipcf2},
            function(r){

                $("td[name='ipcg4_"+linha+"']").html(r);
            });

    });

    $("select[name='ipcf2_notas']").live('change',function(){
        var X = $(this).val();
        var linha = $(this).parent().attr("name");
        var tipo_nota = $("input[name='tipo']:checked").val();

        $.post("financeiro.php",{query: "ipcg3", cod:X,tipo_nota:tipo_nota},
            function(r){

                $("td[name='ipcg3_"+linha+"']").html(r);
                $("td[name='ipcg4_"+linha+"']").children('select').remove();
            });

    });
    ////todos marcado
    $("#tstatus").click(function(){

        if ($(this).is(':checked')){
            $("#status").attr('disabled','disabled');
        }else{
            $("#status").attr('disabled',false);
        }

    });
    $("#tfornecedores").click(function(){

        if ($(this).is(':checked')){
            $("#fornecedor").attr('disabled','disabled');
            $("#fornecedores").attr('disabled','disabled');
        }else{
            $("#fornecedor").attr('disabled',false);
            $("#fornecedores").attr('disabled',false);

        }

    });
    $("#tcentrocusto").click(function(){

        if ($(this).is(':checked')){
            $(".empresa").attr('disabled','disabled');
        }else{
            $(".empresa").attr('disabled',false);
        }

    });
    $("#tipcg3").click(function(){

        if ($(this).is(':checked')){
            $("#sp_ipcg3_b").hide();
            $("#sp_ipcg4_busca").hide();
            $(".ipcg3_busca").val('');
        }else{
            $("#sp_ipcg3_b").show();
        }

    });

    $("#tipcg4").click(function(){

        if ($(this).is(':checked')){
            $("#sp_ipcg4_b").hide();
        }else{
            $("#sp_ipcg4_b").show();
        }

    });

    $("#tassinatura").click(function(){

        if ($(this).is(':checked')){
            $(".assinatura").attr('disabled','disabled');
        }else{
            $(".assinatura").attr('disabled',false);
        }

    });

    /////////////////////////////jeferson marques 2013-12-17 auto complet de bancos usado em contas bancarias
    $('#busca-banco').autocomplete({
        source: function(request, response) {
            $.getJSON('busca_banco.php', {
                term: request.term

            }, response);
        },
        minLength: 3,
        select: function( event, ui ) {
            $('#busca-banco').val(ui.item.banco);
            $('#id_banco').val(ui.item.id);
            $('#status').val(ui.item.status);

        }

    });

    /////////////////////////////////////fim

    
    $("#sp_ipcg3_busca").hide();
    $(".ipcg4_busca").hide();
    $("#sp_ipcf2_busca").hide();

    $(".btipo").click(function(){

        val = $(this).val();

        if(val == 't'){
            $("#sp_ipcg4_busca").hide();
            $("#sp_ipcg3_busca").hide();
            $("#sp_ipcf2_busca").hide();

        }else{
            ipcf2_sel_busca();
            $("#sp_ipcf2_busca").show();
            $("#sp_ipcg3_busca").hide();
            $("#sp_ipcg4_busca").hide();
        }


    });
    ////////////////////////////////////jeferson 24-4-13 demonstrativo

    $(".icon").live('click',function(){
        var n3 = $(this).attr('n3');


        var aux = $(this).attr('aux');
        //alert(aux);
        if (aux == 0){


            $("."+n3).show();
            //$("."+n3).removeAttr('style');
            $(this).attr('aux',1);
        }else{
            $("."+n3).hide();
            $(this).attr('aux',0);

        }

    });
    ////////////////////////////////////jeferson 24-4-13fim 
    $("#check_pcc").click(function(){
        var vencimentosJson = JSON.parse($("#vencimentos-json").val());

        if($(this).is(':checked')){
            $('#pcc').attr("disabled",false);

            var pis = parseFloat($('#pis').val().replace('.', '').replace(',', '.'));
            var cofins = parseFloat($('#cofins').val().replace('.', '').replace(',', '.'));
            var csll = parseFloat($('#csll').val().replace('.', '').replace(',', '.'));
            var pcc_total = pis + cofins + csll;
           // caso desembolso
            if($("input[name='tipo']:checked").val()==1){

                $("input[name='vencimento_pcc']").attr('disabled',false);
                if($('.vencimento-parcela:first').val()){
                    var vencimentoParcela = $('.vencimento-parcela:first').val();
                }else{
                    var vencimentoParcela = $('.vencimento-parcela-editar:first').val();
                }
    
                if(vencimentoParcela != '' ) {
                    vencimentoParcela = moment(vencimentoParcela, 'DD/MM/YYYY').add(1, 'month');
                } else {
                    vencimentoParcela = moment().add(1, 'month');
                }
                var dataPcc = vencimentoParcela.clone();
    
                vencimento_pcc = vencimentosJson.desembolso.pcc != '0' ?
                    dataPcc.set('date', vencimentosJson.desembolso.pcc) :
                    null;
                   
                $("input[name='vencimento_pcc']").val(vencimento_pcc.format('DD/MM/YYYY'));
                $("input[name='cod_pcc']").attr('disabled',false);
                $("input[name='cod_pcc']").addClass('OBG');
                $("input[name='valor_pcc']").val(float2moeda(pcc_total));

            }       
           
            
           
            
            $('#pcc').val(float2moeda(pcc_total));
            $('#pis').attr("disabled",true);
            $('#pis').val('0,00');
            $('#cofins').attr("disabled",true);
            $('#cofins').val('0,00');
            $('#csll').attr("disabled",true);
            $('#csll').val('0,00');

            $("input[name='valor_pis']").val('0,00');
            $("input[name='vencimento_pis']").attr('disabled',true);
            $("input[name='vencimento_pis']").removeClass('OBG');
            $("input[name='cod_pis']").attr('disabled',true);
            $("input[name='cod_pis']").removeClass('OBG');
            $("input[name='vencimento_pis']").val('');

            $("input[name='valor_cofins']").val('0,00');
            $("input[name='vencimento_cofins']").attr('disabled',true);
            $("input[name='vencimento_cofins']").removeClass('OBG');
            $("input[name='cod_cofins']").attr('disabled',true);
            $("input[name='cod_cofins']").removeClass('OBG');
            $("input[name='vencimento_cofins']").val('');

            $("input[name='valor_csll']").val('0,00');
            $("input[name='vencimento_csll']").attr('disabled',true);
            $("input[name='vencimento_csll']").removeClass('OBG');
            $("input[name='cod_csll']").attr('disabled',true);
            $("input[name='cod_csll']").removeClass('OBG');
            $("input[name='vencimento_csll']").val('');
           
        }else{
            $('#pcc').attr("disabled",true);
            $("input[name='cod_pcc']").attr('disabled',true);
            $("input[name='cod_pcc']").removeClass('OBG');
            $('#pcc').val('0,00');
            $('#pis').attr("disabled",false);
            $('#cofins').attr("disabled",false);
            $('#csll').attr("disabled",false);
            $("input[name='valor_pcc']").val('0,00');
            $("input[name='vencimento_pcc']").attr('disabled',true);
            $("input[name='vencimento_pcc']").removeClass('OBG');
            $("input[name='vencimento_pcc']").val('');
            $("input[name='valor_pcc']").val('0,00');
        }
        calcular_total_nota();
    });
    //Cpf
    function verificarCPF(strCPF) {
        var soma = 0;
        var resto;
        if (strCPF == "00000000000")
            return false;
        for (i=1; i<=9; i++)
            soma = soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(strCPF.substring(9, 10)) )
            return false;
        soma = 0;
        for (i = 1; i <= 10; i++)
            soma = soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(strCPF.substring(10, 11) ) )
            return false;
        return true;
    }

    $('#cpf').mask('999.999.999-99');
    $('#cpf').blur(function(){
        var cpf = $(this).val();
        exp = /\.|\-/g
        cpf = cpf.toString().replace( exp, "" );

        var isValido = verificarCPF(cpf);
        $('#cpf_invalido').text( isValido ? '' : 'CPF Invalido!' );
        if (! isValido)
            $(this).val('');
    });

    $('#cnpj').mask('99.999.999/9999-99');
    
    $('#cnpj').blur(function(){
        var cnpj = $(this).val();
        var valida = new Array(6,5,4,3,2,9,8,7,6,5,4,3,2);
        var dig1= new Number;
        var dig2= new Number;

        exp = /\.|\-|\//g
        cnpj = cnpj.toString().replace( exp, "" );
        var digito = new Number(eval(cnpj.charAt(12)+cnpj.charAt(13)));

        for(i = 0; i<valida.length; i++){
            dig1 += (i>0? (cnpj.charAt(i-1)*valida[i]):0);
            dig2 += cnpj.charAt(i)*valida[i];
        }
        dig1 = (((dig1%11)<2)? 0:(11-(dig1%11)));
        dig2 = (((dig2%11)<2)? 0:(11-(dig2%11)));

        if(((dig1*10)+dig2) != digito){
            $('#cnpj_invalido').text('CNPJ Invalido!');
            $(this).val('');
        }else{
            $('#cnpj_invalido').text('');
        }

    });

    $('.cpf_cnpj-editar').click(function(){
        if($("input[name='cpf_cnpj']:checked").val() == 1){
            $('#cnpj').attr('disabled',false);
            $('#cpf').attr('disabled',true);
            $('#cpf').val('');
            $('#cpf').removeClass('OBG');
            $('#cpf').removeAttr('style');
            $('#cpf_invalido').text('');
        }else{
            $('#cnpj_invalido').text('');
            $('#cpf').attr('disabled',false);
            $('#cnpj').attr('disabled',true);
            $('#cnpj').val('');
            $('#cnpj').removeClass('OBG');
            $('#cnpj').removeAttr('style');
        }
    });


    $('.cpf_cnpj').click(function(){
        if($("input[name='cpf_cnpj']:checked").val() == 1){
            $('#cnpj').attr('disabled',false);
            $('#cpf').attr('disabled',true);
            $('#cpf').val('');
            $('#cpf').removeClass('OBG');
            $('#cpf').removeAttr('style');
            $('#cnpj').addClass('OBG');
            $('#cpf_invalido').text('');

        }else{
            $('#cnpj_invalido').text('');
            $('#cpf').attr('disabled',false);
            $('#cnpj').attr('disabled',true);
            $('#cnpj').val('');
            $('#cnpj').removeClass('OBG');
            $('#cnpj').removeAttr('style');
            $('#cpf').addClass('OBG');
        }
    });
///05/06/2014
    $('#val_produtos, #ir , #issqn, #pcc, #pis, #cofins, #csll, #inss').live('blur', function(){

        var vencimentosJson = JSON.parse($("#vencimentos-json").val());
        if($("input[name='tipo']:checked").val()==1){
            var valor= parseFloat($(this).val().replace('.','').replace(',','.'));
            var nome = $(this).attr('name');
            if(nome == 'ir'){
                if($('#fornecedor option:selected').attr('fisica_juridica')==1){
                   // $('#cod_ir').html('1708');
                    $('#cod_ir').parent().attr('id_imposto_retido',1);
                    $("input[name='cod_"+nome+"']").val('1708');
                }else{
                    //$('#cod_ir').html('0588');
                    $('#cod_ir').parent().attr('id_imposto_retido',2);
                    $("input[name='cod_"+nome+"']").val('0588');
                }
            }

            var dataEmissao = $('.data-emissao-nota').val();
            if(dataEmissao != '') {
                dataEmissao = moment(dataEmissao, 'DD/MM/YYYY').add(1, 'month');
            } else {
                dataEmissao = moment().add(1, 'month');
            }
            if($('.vencimento-parcela:first').val()){
                var vencimentoParcela = $('.vencimento-parcela:first').val();
            }else{
                var vencimentoParcela = $('.vencimento-parcela-editar:first').val();
            }
            
            if(vencimentoParcela != '') {
                vencimentoParcela = moment(vencimentoParcela, 'DD/MM/YYYY').add(1, 'month');
            } else {
                vencimentoParcela = moment().add(1, 'month');
            }
            console.log(vencimentoParcela)
            var vencimentos = [];
            //if($("input[name='tipo']:checked").val() == 1) {
                var dataIss = dataEmissao.clone();
                var dataIr = dataEmissao.clone();
                var dataIcms = dataEmissao.clone();
                var dataIpi = dataEmissao.clone();
                var dataInss = dataEmissao.clone();
                var dataPis = vencimentoParcela.clone();
                var dataCofins = vencimentoParcela.clone();
                var dataCsll = vencimentoParcela.clone();
                var dataInss = dataEmissao.clone();
                vencimentos['issqn'] = vencimentosJson.desembolso.iss != '0' ?
                    dataIss.set('date', vencimentosJson.desembolso.iss) :
                    null;
                vencimentos['ir'] = vencimentosJson.desembolso.ir != '0' ?
                    dataIr.set('date', vencimentosJson.desembolso.ir) :
                    null;
                vencimentos['ipi'] = vencimentosJson.desembolso.recolhe_ipi != '0' ?
                    dataIcms.set('date', vencimentosJson.desembolso.ipi) :
                    null;
                vencimentos['icms'] = vencimentosJson.desembolso.icms != '0' ?
                    dataIpi.set('date', vencimentosJson.desembolso.icms) :
                    null;
                vencimentos['pis'] = vencimentosJson.desembolso.pis != '0' ?
                    dataPis.set('date', vencimentosJson.desembolso.pis) :
                    null;
                vencimentos['cofins'] = vencimentosJson.desembolso.cofins != '0' ?
                    dataCofins.set('date', vencimentosJson.desembolso.cofins) :
                    null;
                vencimentos['csll'] = vencimentosJson.desembolso.csll != '0' ?
                    dataCsll.set('date', vencimentosJson.desembolso.csll) :
                    null;
                vencimentos['inss'] = vencimentosJson.desembolso.inss != '0' ?
                    dataInss.set('date', vencimentosJson.desembolso.inss) :
                    null;

            /* } else {
                vencimentos['issqn'] = vencimentosJson.recebimento.iss != '0' ?
                    dataIss.set('date', vencimentosJson.recebimento.iss) :
                    null;
                vencimentos['ir'] = vencimentosJson.recebimento.ir != '0' ?
                    dataIr.set('date', vencimentosJson.recebimento.ir) :
                    null;
                vencimentos['ipi'] = vencimentosJson.recebimento.recolhe_ipi != '0' ?
                    dataIcms.set('date', vencimentosJson.recebimento.ipi) :
                    null;
                vencimentos['icms'] = vencimentosJson.recebimento.icms != '0' ?
                    dataIpi.set('date', vencimentosJson.recebimento.icms) :
                    null;
                vencimentos['pis'] = vencimentosJson.recebimento.pis != '0' ?
                    vencimentoParcela.set('date', vencimentosJson.recebimento.pis) :
                    null;
                vencimentos['cofins'] = vencimentosJson.recebimento.cofins != '0' ?
                    vencimentoParcela.set('date', vencimentosJson.recebimento.cofins) :
                    null;
                vencimentos['csll'] = vencimentosJson.recebimento.csll != '0' ?
                    vencimentoParcela.set('date', vencimentosJson.recebimento.csll) :
                    null;
                vencimentos['inss'] = vencimentosJson.recebimento.inss != '0' ?
                    dataInss.set('date', vencimentosJson.recebimento.inss) :
                    null;
            }*/
            console.log(vencimentos);

            if(valor >0){

                valor =float2moeda(valor);
                $("input[name='valor_"+nome+"']").val(valor);
                $("input[name='vencimento_"+nome+"']").attr('disabled',false);
                $("input[name='cod_"+nome+"']").attr('disabled',false);
                if(vencimentos[nome] != null) {
                    $("input[name='vencimento_" + nome + "']").val(vencimentos[nome].format('DD/MM/YYYY'));
                }
                $("input[name='vencimento_"+nome+"']").addClass('OBG');
                $("input[name='cod_"+nome+"']").addClass('OBG');
                if(nome == 'issqn'){
                    $('#fornecedor_issqn').addClass('OBG_CHOSEN').attr('disabled',false);
                }

            }else{
                valor =float2moeda(valor);
                $("input[name='valor_"+nome+"']").val(valor);
                $("input[name='vencimento_"+nome+"']").attr('disabled',true);
                $("input[name='vencimento_"+nome+"']").val('');
                $("input[name='vencimento_"+nome+"']").removeClass('OBG');
                $("input[name='vencimento_"+nome+"']").attr('style','');
                $("input[name='cod_"+nome+"']").attr('disabled',true);
                $("input[name='cod_"+nome+"']").removeClass('OBG');
                $("input[name='cod_"+nome+"']").attr('style','');
                if(nome == 'ir'){
                    $("input[name='cod_"+nome+"']").val('');
                }
                if(nome == 'issqn'){   
                    estilo = $('#fornecedor_issqn_chosen').attr('style');               
                    
                    $('#fornecedor_issqn_chosen').attr('style',estilo.replace("border: 3px solid red",''));
                    $('#fornecedor_issqn').removeClass('OBG_CHOSEN').attr('disabled',true);
                }
            }

            $('#fornecedor_issqn').trigger("chosen:updated");

        }
    });

    $(".compensar").live('click',function(){
        var url = $(this).attr('url');
        window.open(url, '_blank');
    });

    $(".cancelar-compensacao").live('click', function () {
        
        var $this = $(this);
        var compensacaoId = $this.attr('id-compensacao');
        if(confirm('Deseja realmente cancelar compensação?')) {
            $.ajax({
                url: "/financeiros/compensacao/?action=cancelar-compensacao",
                type: 'POST',
                data: {id: compensacaoId},
                success: function (response) {
                    response = JSON.parse(response);
                   
                    if(response['tipo'] != 'erro') {
                        alert(response['msg']);
                        window.location.reload();
                    } else {
                        alert(response['msg']);
                        
                    }
                    return false;
                }
            });
        }
    });

    $('#calcular-vencimentos').click(function(){

        var vencimentosJson = JSON.parse($("#vencimentos-json").val());
       
        var dataEmissao = $('.data-emissao-nota').val();
        if(dataEmissao != '') {
            dataEmissao = moment(dataEmissao, 'DD/MM/YYYY').add(1, 'month');
        } else {
            dataEmissao = moment().add(1, 'month');
        }
        var vencimentoParcela = $('.vencimento-parcela:first').val();
        if(vencimentoParcela != '') {
            vencimentoParcela = moment(vencimentoParcela, 'DD/MM/YYYY').add(1, 'month');
        } else {
            vencimentoParcela = moment().add(1, 'month');
        }
        var vencimentoRealParcela = $('.vencimento-real-parcela:first').val();
        if(vencimentoRealParcela != '') {
            vencimentoRealParcela = moment(vencimentoRealParcela, 'DD/MM/YYYY').add(1, 'month');
        } else {
            vencimentoRealParcela = moment().add(1, 'month');
        }

        var vencimentos = [];
        if($("input[name='tipo']:checked").val() == 1) {
            var dataIss = dataEmissao.clone();
            var dataIr = dataEmissao.clone();
            var dataIcms = dataEmissao.clone();
            var dataIpi = dataEmissao.clone();
            var dataInss = dataEmissao.clone();
            var dataPis = vencimentoParcela.clone();
            var dataCofins = vencimentoParcela.clone();
            var dataCsll = vencimentoParcela.clone();
            var dataPcc = vencimentoParcela.clone();
            vencimentos['issqn'] = vencimentosJson.desembolso.iss != '0' ?
                dataIss.set('date', vencimentosJson.desembolso.iss) :
                null;
            vencimentos['ir'] = vencimentosJson.desembolso.ir != '0' ?
                dataIr.set('date', vencimentosJson.desembolso.ir) :
                null;
            vencimentos['ipi'] = vencimentosJson.desembolso.recolhe_ipi != '0' ?
                dataIcms.set('date', vencimentosJson.desembolso.ipi) :
                null;
            vencimentos['icms'] = vencimentosJson.desembolso.icms != '0' ?
                dataIpi.set('date', vencimentosJson.desembolso.icms) :
                null;
            if(!$('#check_pcc').is(':checked')) {
                vencimentos['pis'] = vencimentosJson.desembolso.pis != '0' ?
                    dataPis.set('date', vencimentosJson.desembolso.pis) :
                    null;
                vencimentos['cofins'] = vencimentosJson.desembolso.cofins != '0' ?
                    dataCofins.set('date', vencimentosJson.desembolso.cofins) :
                    null;
                vencimentos['csll'] = vencimentosJson.desembolso.csll != '0' ?
                    dataCsll.set('date', vencimentosJson.desembolso.csll) :
                    null;
            } else {
                vencimentos['pcc'] = vencimentosJson.desembolso.pcc != '0' ?
                    dataPcc.set('date', vencimentosJson.desembolso.pcc) :
                    null;
            }
            vencimentos['inss'] = vencimentosJson.desembolso.inss != '0' ?
                dataInss.set('date', vencimentosJson.desembolso.inss) :
                null;
            /*} else if($("input[name='tipo']:checked").val() == 0) {
                vencimentos['issqn'] = vencimentosJson.recebimento.iss != '0' ?
                    dataEmissao.set('date', vencimentosJson.recebimento.iss) :
                    null;
                vencimentos['ir'] = vencimentosJson.recebimento.ir != '0' ?
                    dataEmissao.set('date', vencimentosJson.recebimento.ir) :
                    null;
                vencimentos['ipi'] = vencimentosJson.recebimento.recolhe_ipi != '0' ?
                    dataEmissao.set('date', vencimentosJson.recebimento.ipi) :
                    null;
                vencimentos['icms'] = vencimentosJson.recebimento.icms != '0' ?
                    dataEmissao.set('date', vencimentosJson.recebimento.icms) :
                    null;
                vencimentos['pis'] = vencimentosJson.recebimento.pis != '0' ?
                    vencimentoParcela.set('date', vencimentosJson.recebimento.pis) :
                    null;
                vencimentos['cofins'] = vencimentosJson.recebimento.cofins != '0' ?
                    vencimentoParcela.set('date', vencimentosJson.recebimento.cofins) :
                    null;
                vencimentos['csll'] = vencimentosJson.recebimento.csll != '0' ?
                    vencimentoParcela.set('date', vencimentosJson.recebimento.csll) :
                    null;
                vencimentos['inss'] = vencimentosJson.recebimento.inss != '0' ?
                    dataEmissao.set('date', vencimentosJson.recebimento.inss) :
                    null;
            }*/
            $('#ir , #issqn, #pcc, #pis, #cofins, #csll').each(function () {

                var valor = parseFloat(replaceValorMonetarioFloat($(this).val()));
                var nome = $(this).attr('name');

                if (nome == 'ir') {
                    if ($('#fornecedor option:selected').attr('fisica_juridica') == 1) {
                      //  $('#cod_ir').html('1708');
                        $("input[name='cod_" + nome + "']").val('1708');
                        $('#cod_ir').parent().attr('id_imposto_retido', 1);
                    } else {
                       // $('#cod_ir').html('0588');
                       $("input[name='cod_" + nome + "']").val('0588');
                        $('#cod_ir').parent().attr('id_imposto_retido', 2);
                    }
                }

                if (valor > 0) {

                    valor = float2moeda(valor);
                    $("input[name='valor_" + nome + "']").val(valor);
                    $("input[name='vencimento_" + nome + "']").attr('disabled', false);
                    $("input[name='cod_" + nome + "']").attr('disabled', false);
                    $("input[name='cod_" + nome + "']").addClass('OBG');
                    if (vencimentos[nome] != null) {
                        $("input[name='vencimento_" + nome + "']").val(vencimentos[nome].format('DD/MM/YYYY'));
                    }
                    $("input[name='vencimento_" + nome + "']").addClass('OBG');
                    if (nome == 'issqn') {
                        $('#fornecedor_issqn').addClass('OBG_CHOSEN').attr('disabled', false);
                    }

                } else {
                    valor = float2moeda(valor);
                    $("input[name='valor_" + nome + "']").val(valor);
                    $("input[name='vencimento_" + nome + "']").attr('disabled', true);
                    $("input[name='vencimento_" + nome + "']").val('');
                    $("input[name='vencimento_" + nome + "']").removeClass('OBG');
                    $("input[name='vencimento_" + nome + "']").attr('style', '');

                    $("input[name='cod_" + nome + "']").removeClass('OBG');
                    $("input[name='cod_" + nome + "']").attr('style', '');
                    $("input[name='cod_" + nome + "']").attr('disabled', true);
                    
                    if(nome == 'ir'){
                        $("input[name='cod_" + nome + "']").val('');

                    }

                    if (nome == 'issqn') {
                        estilo = $('#fornecedor_issqn_chosen').attr('style');

                        $('#fornecedor_issqn_chosen').attr('style', estilo.replace("border: 3px solid red", ''));
                        $('#fornecedor_issqn').removeClass('OBG_CHOSEN').attr('disabled', true);
                    }
                }
            });

            $('#fornecedor_issqn').trigger("chosen:updated");
        }
    });

    $('#fornecedor').change(function(){
        if($('#fornecedor option:selected').attr('fisica_juridica')==1){
           // $('#cod_ir').html('1708');
            $('#cod_ir').parent().attr('id_imposto_retido',1);
            $("input[name='cod_ir']").val('1708');
        }else{
           // $('#cod_ir').html('0588');
            
            $("input[name='cod_ir']").val('0588');
            $('#cod_ir').parent().attr('id_imposto_retido',2);
        }
    });
///05/06/2014 fim
    $(".editar_parcela .form_parcelas").keypress(function(){

        $(".editar_parcela").attr('data-editar-parcela','1');


    });

    $(".editar_parcela .form_parcelas").click(function(){

        $(".editar_parcela").attr('data-editar-parcela','1');


    });


    //07/07/2014
    $(".desfazer-conciliacao").live('click',function(){

        var id = $(this).attr('id');
        var id_banco= $(this).attr('id_banco');
        var entrada = $(this).attr('entrada');
        var saida = $(this).attr('saida');
        var transferencia =$(this).attr('transferencia');
        var atual = $(this);
        var tipo= $(this).attr('tipo');
        var num_linha = $(this).attr('num_linha');
        var valor=$(this).attr('valor');

        var data_processado =atual.parent().parent().attr('data');


        $.post("query.php",{query: "desfazer-conciliacao",
                id:id,
                id_banco:id_banco,
                entrada:entrada,
                saida:saida,
                transferencia:transferencia

            },
            function(r){
                if(r==1){
                    atual.parent().parent().children('td').eq(6).children('center').remove();
                    atual.parent().parent().children('td').eq(7).children('input').val(data_processado);
                    atual.parent().parent().children('td').eq(7).children('input').attr('disabled',false);
                    atual.parent().parent().children('td').eq(7).children('input').attr('style','display:none;');
                    atual.parent().parent().attr('conciliado','N');
                    atual.parent().parent().children('td').eq(6).html(
                        "<input type='radio' alterado='N' name='conciliado_"+id+"' value='S' tipo='"+tipo+"' num_linha='"+num_linha+"' valor='"+valor+"' id_parcela='"+id+"' class='conciliado OBG_RADIO'  >Sim </input>"+
                        "<input type='radio' alterado='N'  class='OBG_RADIO conciliado'  transferencia='"+transferencia+"' name='conciliado_"+id+"' value='N' tipo='"+tipo+"' num_linha='"+num_linha+"' valor='"+valor+"' id_parcela='"+id+"' checked='checked' style='font-size:9px;'>N&atilde;o</input>"
                    );
                    atual.remove();

                    alert('Operação realizada com sucesso!');
                }else{
                    alert(r);
                }

            });

    });
    // fim
    //15/07/2014
    $('.desfazer-processamento-parcela').click(function(){
        var idparcela=$(this).attr('data-idparcela');
        $("#dialog-desfazer-processamento-parcela").attr('data-idparcela',idparcela);
        $("#dialog-desfazer-processamento-parcela").dialog('open');
    });

    $("#dialog-desfazer-processamento-parcela").dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            Ok: function() {
                if(validar_campos("dialog-desfazer-processamento-parcela")){
                    $.post("query.php",{query: "desfazer-processamento-parcela",
                            id_parcela: $(this).attr("data-idparcela"),
                            justificativa:$('#justificativa-desfazer-processamento-parcela').val()},
                        function(retorno){
                            if(retorno ==1){

                                alert("O processamento da parcela foi desfeito.");
                                location.reload();
                            }else{
                                alert(retorno);
                                return false;
                            }

                        });


                }
                $('#justificativa-desfazer-processamento-parcela').val('');
                $( this ).dialog( "close" );
            },
            Cancelar: function(){
                $('#justificativa-desfazer-processamento-parcela').val('');
                $( this ).dialog( "close" );
            }
        }
    });
//fim
    //17/07/2007
    $('.desfazer-processamento-nota').live('click', function(){

        var idnota=$(this).attr('data-idnota');
        $("#dialog-desfazer-processamento-nota").attr('data-idnota',idnota);
        $("#dialog-desfazer-processamento-nota").dialog('open');
    });

    $("#dialog-desfazer-processamento-nota").dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            Ok: function() {
                if(validar_campos("dialog-desfazer-processamento-nota")){
                    $.post("query.php",{
                            query: "desfazer-processamento-nota",
                            id_nota: $(this).attr("data-idnota"),
                            justificativa:$('#justificativa-desfazer-processamento-nota').val()},
                        function(retorno){
                            if(retorno == 1){
                                alert("O processamento da nota foi desfeito.");
                                location.reload();
                                //$('#nota-' + $(this).attr("data-idnota")).find('.status-nota-lista').text('Em Aberto');
                            } else if(retorno == 2){
                                alert("Essa nota possui parcelas aglutinadas ou em algum bordero ou compensada.");
                            }else{
                                alert(retorno);
                                return false;
                            }
                        });
                }
                $('#justificativa-desfazer-processamento-nota').val('');
                $( this ).dialog( "close" );
            },
            Cancelar: function(){
                $('#justificativa-desfazer-processamento-nota').val('');
                $( this ).dialog( "close" );
            }
        }
    });

    ///28/07/214
    $('#fornecedor-cadastro-servico').change(function(){
        var idfornecedor=$(this).val();
        /*$.post("cadastro_servico_prestador.php",{
            query:"op",
            id_fornecedor:idfornecedor});*/
        window.location="?op=cadastro_servico_prestador&idFornecedor="+idfornecedor;

    });

    //29/07/2014
    $('.servico_terceirizado').click(function(){
        var idservico=$(this).val();
        if($(this).is(':checked')){
            $("."+idservico).each(function(){
                $(this).attr('disabled',false);
            });
        }else{
            $("."+idservico).each(function(){
                $(this).attr('disabled',true);
                $(this).val('0.00');
            });
        }
    });

    $('#fornecedor-cadastro-servico').chosen();
    $('#ipcc').chosen();
    $('#banco-chosen').chosen({search_contains: true});

    $('#salvar-servico-prestador').click(function(){
        if(validar_campos('cadastro-servico-prestador')){
            var cont_empresa=0;
            var cont_servico=0;
            var cont=0;
            var empresas = '';
            var linha_servico='';
            var id_servico='';
            $(".empresa_servico_prestador").each(function(){

                if($(this).is(":checked")){
                    cont_empresa++;
                    empresas +=$(this).val()+"#";
                }
                // cont++;
            });
            $(".servico_terceirizado").each(function(){

                if($(this).is(":checked")){
                    cont_servico++;
                    id_servico= $(this).val();
                    $("."+id_servico).each(function(){
                        linha_servico+= id_servico+"#"+parseFloat( $(this).val().replace('.','').replace(',','.'))+"#"+$(this).attr('data-empresa')+"$";
                    });
                }

            });


            if(cont_empresa==0){
                alert('O Prestador precisa ser associado a alguma Unidade Regional.');
                return false;
            }else if(cont_servico==0){
                alert('O Prestador precisa ser associado a algum Serviço.');
            }else{
                var id_fornecedor   = $("#fornecedor-cadastro-servico").val();
                var login_prestador = $("#ativar_cpf_cnpj").val();


                /*   console.log(id_fornecedor);
                   console.log(login_prestador);
                   console.log(empresas);
                   console.log(linha_servico);*/
                $.post("query.php",{
                    query:"salvar-cadastro-servico-prestador",
                    id_fornecedor:id_fornecedor,
                    login_prestador:login_prestador,
                    linha_servico:linha_servico,
                    empresas:empresas
                },function(r){
                    if(r==1){
                        alert('Cadastro Salvo.');
                        window.location="?op=cadastro_servico_prestador";
                    }else{
                        alert(r);
                    }

                });
            }


        }else{
            return false;
        }
    });


    $('#editar-servico-prestador').click(function(){
        var id_prestador= $(this).attr('data-idprestador');
        if(validar_campos('cadastro-servico-prestador')){
            var cont_empresa=0;
            var cont_servico=0;
            var cont=0;
            var empresas = '';
            var linha_servico='';
            var id_servico='';
            var desativar_empresa='';
            var linha_desativar_servico='';
            var linha_desativar_servico_empresa='';
            $(".empresa_servico_prestador").each(function(){

                if($(this).is(":checked")){
                    cont_empresa++;
                    if($(this).attr('data-empresa-anterior')==0){
                        empresas +=$(this).val()+"#";
                    }
                }else{
                    if($(this).attr('data-empresa-anterior')==1){
                        desativar_empresa +=$(this).val()+"#";
                    }
                }
                // cont++;
            });
            $(".servico_terceirizado").each(function(){
                id_servico= $(this).val();
                if($(this).is(":checked")){

                    cont_servico++;

                    if($(this).attr('data-check-anterior')==0){

                        $("."+id_servico).each(function(){

                            linha_servico+= id_servico+"#"+parseFloat( $(this).val().replace('.','').replace(',','.'))+"#"+$(this).attr('data-empresa')+"$";
                        });
                    }else{

                        $("."+id_servico).each(function(){
                            if(parseFloat( $(this).val().replace('.','').replace(',','.'))!=$(this).attr('data-anterior')){
                                linha_servico+= id_servico+"#"+parseFloat( $(this).val().replace('.','').replace(',','.'))+"#"+$(this).attr('data-empresa')+"$";
                                linha_desativar_servico_empresa +=id_servico+"#"+$(this).attr('data-empresa')+"$";
                            }
                        });

                    }
                }else{


                    if($(this).attr('data-check-anterior')==1){
                        linha_desativar_servico +=id_servico+"#";
                    }


                }

            });


            if(cont_empresa==0){
                alert('O Prestador precisa ser associado a alguma Unidade Regional.');
                return false;
            }else if(cont_servico==0){
                alert('O Prestador precisa ser associado a algum Serviço.');
            }else{
                var id_fornecedor   = $("#fornecedor-cadastro-servico").val();
                var login_prestador = $("#ativar_cpf_cnpj").val();


                /* console.log('fornecedor:'+id_fornecedor);

                 console.log('empresa_add:'+empresas);
                 console.log('empresa_exc:'+desativar_empresa);
                 console.log('servico_add:'+linha_servico);
                 console.log('servico_del_empresa'+linha_desativar_servico_empresa);
                 console.log('servico_del'+linha_desativar_servico);*/

                $.post("query.php",{
                    query:"editar-cadastro-servico-prestador",
                    id_prestador:id_prestador,
                    empresas:empresas,
                    desativar_empresa:desativar_empresa,
                    linha_servico:linha_servico,
                    linha_desativar_servico_empresa:linha_desativar_servico_empresa,
                    linha_desativar_servico:linha_desativar_servico

                },function(r){
                    if(r==1){
                        alert('Cadastro Editado.');
                        window.location="?op=cadastro_servico_prestador";
                    }else{
                        alert(r);
                    }

                });
            }


        }else{
            return false;
        }
    });



    $('.desfazer-processamento-nota').click(function(){
        var idnota=$(this).attr('data-idnota');
        $("#dialog-desfazer-processamento-nota").attr('data-idnota',idnota);
        $("#dialog-desfazer-processamento-nota").dialog('open');


    });

    $('#cancelar-transferencia').live('click',function(){

        $('#dialog-cancelar-transferencia').dialog('open');
        $("#dialog-cancelar-transferencia").html("<span id='span-cancelar-transferencia'><p><label>Justificativa</label></p><p><textarea id='justificativa-cancelar-transferencia' class='OBG'></textarea></p></span>");

    });

    $(".marcar-transferencias").live('click', function () {
        $('input.item-transferencia').not(this).attr('checked', this.checked);
    });

    $("#dialog-cancelar-transferencia").dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            Ok: function() {
                if(validar_campos("dialog-cancelar-transferencia")){
                    var transferencias = [];

                    $(".item-transferencia:checked").each(function () {
                        transferencias.push(
                            $(this).attr('idTransferencia')
                        );
                    });

                    $.post("query.php",{query: "cancelar-transferencia",
                            transferencias: transferencias,
                            justificativa:$('#justificativa-cancelar-transferencia').val()},
                        function(retorno){
                            if(retorno == 1){
                                alert("O cancelamento da(s) transferência(s) foi realizado com sucesso!");
                                $(".item-transferencia:checked").each(function () {
									$(this).parent().parent().remove();
                                });
                                $(".marcar-transferencias").attr('checked', false);
                            }else{

                                alert(retorno);
                                return false;
                            }


                        });



                }else{
                    return false
                }
                $('#justificativa-cancelar-transferencia').val('');
                $( this ).dialog( "close" );

            },
            Cancelar: function(){
                $('#justificativa-desfazer-processamento-nota').val('');
                $( this ).dialog( "close" );
            }
        }
    });


    $("#pesquisar-relatorio-analitico-fluxo-caixa").click(function(){

        if(validar_campos('relatorio-analitico-fluxo-caixa')){
            var inicio = $("#inicio").val();
            var inicio1 = inicio.split('/').reverse().join('');
            var fim = $("#fim").val();
            var fim1 = fim.split('/').reverse().join('');
            if(fim1<inicio1){
                alert("Data Fim está menor que a data Inicio.");
                return false;
            }else{
                $.post("query.php",{
                    query:"pesquisar-relatorio-analitico-fluxo-caixa",
                    inicio:inicio,
                    fim:fim,
                    empresa:$("#empresa-relatorio").val(),
                    id_ipcf_n4:$("#ipcf_nivel4").val(),
                    status: $(".status_relatorio:checked").val()
                },function(i){
                    $("#div-resultado-relatorio-analitico-fluxo-caixa").html(i);
                    if(i.search('Nenhum') == -1)
                        $("#opcoes-relatorio-analitico-fluxo-caixa").html(
                            "<button id='exportar-relatorio-analitico-fluxo-caixa' " +
                            "class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' " +
                            "inicio='"+inicio+"' " +
                            "fim='"+fim+"' " +
                            "status='" + $(".status_relatorio:checked").val() + "' " +
                            "empresa='"+$("#empresa-relatorio").val()+"' " +
                            "empresa-texto='"+$("#empresa-relatorio :selected").text()+"' " +
                            "id_ipcf_n4='"+$("#ipcf_nivel4").val()+"' " +
                            "id_ipcf_n4-texto='"+$("#ipcf_nivel4 :selected").text()+"' " +
                            "aria-disabled='false' " +
                            "role='button' " +
                            "type='button' " +
                            "style='float:right;'>" +
                            "<span class='ui-button-text'>Exportar</span>" +
                            "</button>" +
                            "<button " +
                            "id='imprimir-relatorio-analitico-fluxo-caixa' " +
                            "class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' " +
                            "inicio='"+inicio+"' " +
                            "fim='"+fim+"' " +
                            "status='" + $(".status_relatorio:checked").val() + "' " +
                            "empresa='"+$("#empresa-relatorio").val()+"' " +
                            "empresa-texto='"+$("#empresa-relatorio :selected").text()+"' " +
                            "id_ipcf_n4='"+$("#ipcf_nivel4").val()+"' " +
                            "id_ipcf_n4-texto='"+$("#ipcf_nivel4 :selected").text()+"' " +
                            "aria-disabled='false' " +
                            "role='button' " +
                            "type='button' " +
                            "style='float:right;'>" +
                            "<span class='ui-button-text'>Imprimir</span>" +
                            "</button></br>");

                    return false
                });
            }
        }else{
            return false;
        }
    });


    //INICIO EXPORTAR RELATORIO ANALITICO DE FLUXO DE CAIXA
    $('#exportar-relatorio-analitico-fluxo-caixa').live('click',function(){
        var inicio = $(this).attr('inicio');
        var fim = $(this).attr('fim');
        var status = $(this).attr('status');
        var empresa = $(this).attr('empresa');
        var empresa_texto = $(this).attr('empresa-texto');
        var id_ipcf_n4 = $(this).attr('id_ipcf_n4');
        var id_ipcf_n4_texto = $(this).attr('id_ipcf_n4-texto');
        window.open('exportar_rel_analito_fluxo_caixa_excel.php?inicio='+Base64.encode(inicio)+'&fim='+Base64.encode(fim)+'&status='+Base64.encode(status)+'&empresa='+Base64.encode(empresa)+'&empresa_texto='+empresa_texto+'&id_ipcf_n4='+Base64.encode(id_ipcf_n4)+'&id_ipcf_n4_texto='+id_ipcf_n4_texto);
    });
    //fim exportar

    //INICIO IMPRIMIR RELATORIO ANALITICO DE FLUXO DE CAIXA
    $('#imprimir-relatorio-analitico-fluxo-caixa').live('click',function(){
        var inicio = $(this).attr('inicio');
        var fim = $(this).attr('fim');
        var status = $(this).attr('status');
        var empresa = $(this).attr('empresa');
        var empresa_texto = $(this).attr('empresa-texto');
        var id_ipcf_n4 = $(this).attr('id_ipcf_n4');
        var id_ipcf_n4_texto = $(this).attr('id_ipcf_n4-texto');
        window.open('imprimir_rel_analito_fluxo_caixa.php?inicio='+Base64.encode(inicio)+'&fim='+Base64.encode(fim)+'&status='+Base64.encode(status)+'&empresa='+Base64.encode(empresa)+'&empresa_texto='+empresa_texto+'&id_ipcf_n4='+Base64.encode(id_ipcf_n4)+'&id_ipcf_n4_texto='+id_ipcf_n4_texto);
    });
    //fim exportar


    ///inicio inicio-periodo
    $(".inicio-periodo").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        onClose: function(selectedDate){
            $(".fim-periodo").datepicker(
                'option',
                'minDate',
                selectedDate
            )
        }
    });
    ///fim inicio-periodo

    ///inicio fim-periodo
    $(".fim-periodo").datepicker({
        minDate: $(".inicio-periodo").val(),
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1
    });
    ///fim fim-periodo

    //fim document.ready

    $("#salvar-tarifa").click(function () {
        if(validar_campos('nova-tarifa')){
            var item = $("#item-tarifa").val();
            var documento = $("#doc-tarifa").val();
            var data = $("#data-tarifa").val();
            var data_fechamento = moment($("#data-fechamento").val());
            var data_tarifa = moment(data);
            var valor = $("#valor-tarifa").val();
            var conta = $(this).attr('id-conta');
            if(data_tarifa.isSameOrBefore(data_fechamento)) {
                alert('A data da tarifa é menor que a data de fechamento do período (' + data_fechamento.format('DD/MM/YYYY') + ')!')
                return false;
            }
            $.post("query.php", {query: "salvar-tarifa", item: item, documento: documento, data: data, valor: valor, conta: conta},
                function (response) {
                    if(response == 1){
                        alert('Tarifa/Imposto cadastrado com sucesso!');
                        location.href = '?op=tarifas-bancarias&id_conta='+conta+'&action=listar';
                    }else{
                        alert('Houve um erro ao cadastrar a tarifa! Entre em contato com o setor de TI! '+response);
                       // location.href = '?op=tarifas-bancarias&id_conta='+conta;
                    }
                });
        }
    });

    $("#editar-tarifa").click(function () {
        if(validar_campos('nova-tarifa')){
            var item = $("#item-tarifa").val();
            var documento = $("#doc-tarifa").val();
            var data = $("#data-tarifa").val();
            
            var data_fechamento = moment($("#data-fechamento").val());
            var data_tarifa = moment(data);
            var valor = $("#valor-tarifa").val();
            var conta = $(this).attr('id-conta');
            var id = $(this).attr('id-tarifa');
            if(data_tarifa.isSameOrBefore(data_fechamento)) {
                alert('A data da tarifa é menor que a data de fechamento do período (' + data_fechamento.format('DD/MM/YYYY') + ')!')
                return false;
            }
            $.post("query.php", {query: "editar-tarifa", item: item, documento: documento, data: data, valor: valor, id: id},
                function (response) {
                    if(response == 1){
                        alert('Tarifa/Imposto atualizado com sucesso!');
                        location.href = '?op=tarifas-bancarias&id_conta='+conta+'&action=listar';
                    }else{
                        alert('Houve um erro ao atualizar a tarifa! Entre em contato com o setor de TI! '+response);
                        
                    }
                });
        }
    });

    $(".excluir-tarifa").live('click', function () {
        if(confirm('Deseja realmente excluir esse lançamento?')){
            var inicio = $(this).attr('inicio-tarifa');
            var fim = $(this).attr('fim-tarifa');
            var conta = $(this).attr('id-conta');
            var id = $(this).attr('id-tarifa');
            $.post("query.php", {query: "excluir-tarifa", id: id},
                function (response) {
                    if(response == 1){
                        alert('Tarifa/Imposto excluído com sucesso!');
                        $.post("query.php", {query: "buscar-tarifa", inicio: inicio, fim: fim, conta: conta},
                            function (response) {
                                $("#resultado-busca").html(response);
                            });
                    }else{
                        alert('Houve um erro ao cadastrar a tarifa! Entre em contato com o setor de TI!');
                        location.href = '?op=tarifas-bancarias&id_conta='+conta;
                    }
                });
        }
    });

    $("#buscar-tarifas").click( function () {
        if(validar_campos('buscar-impostos-tarifas')){
            var inicio = $("#inicio-tarifa").val();
            var fim = $("#fim-tarifa").val();
            var conta = $(this).attr('id-conta');
            $.post("query.php", {query: "buscar-tarifa", inicio: inicio, fim: fim, conta: conta},
                function (response) {
                    $("#resultado-busca").html(response);
                });
        }
    });
    $('#editar-num-nota').click(function(){
        var numeroNota = $("#num").val();
        var idNota =$("input[name='idnota']").val();
        var codFornecedor = $("#fornecedor").val();

        if(numeroNota != '' && numeroNota != null && numeroNota != undefined ){
            $.post("query.php", {query: "editar-numero-nota",
                    numeroNota: numeroNota,
                    idNota: idNota,
                    codFornecedor: codFornecedor},
                function (r) {
                    if(r == 1){
                        alert('O numero da nota foi editado.');
                        return false;
                    }else{
                        alert(r);
                        return false;
                    }
                    return false;
                });
        }else{
            alert('O numero da nota não pode ser vazio');
        }

    });

    $('#editar-competencia').click(function(){
        var competencia = $("#data-competencia").val();
        var idNota =$("input[name='idnota']").val();
        

        if(competencia != '' && competencia != null && competencia != undefined ){
            $.post("query.php", {query: "editar-competencia",
            competencia: competencia,
                    idNota: idNota},
                function (r) {
                    if(r == 1){
                        alert('O campo competencia foi editado.');
                        return false;
                    }else{
                        alert(r);
                        return false;
                    }
                    return false;
                });
        }else{
            alert('O campo competencia não pode ser vazio');
        }

    });


    $('#paciente-custo-fatura').chosen({search_contains: true});
    $('#pesquisar-fatura-custo').click(function(){
        if(validar_campos('div-pesquisar-fatura-custo')){
            $.post("query.php", {query: 'pesquisar-faturas-custo',
                id_paciente: $('#paciente-custo-fatura').val(),
                fim:$('#fim_fatura').val(),
                inicio:$('#inicio_fatura').val()}, function(r) {
                $("#resultado-pesquisa-fatura-custo").html(r);


            });
        }
        return false;
    });

    $("#margem-fatura-custo").dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            Excel: function() {
                var idFatura = $('#id-fatura').val();
                var tipoDocumento = $('#id-fatura').val();
                window.open('exportar_fatura_custo.php?id='+idFatura+'&av='+tipoDocumento+'&zerados=1');
            },
            Confirmar: function() {
                location.href ='?op=pesquisarCustoFatura';
            },
            Voltar: function(){
                $( this ).dialog( "close" );
            }
        }
    });

    $("#margem-fatura-custo").dialog('close');
    $(".qtd-custo-extra").live('change',function(){
        calculaCusto();
    });
    $(".calcula-custo, #iss,.calcula-custo-extra").live('keyup',function(){
        calculaCusto();
    });

    function calculaCusto(){
        var desconto_acrescimo = 0;
        var valorTotal = 0;
        var custoTotal = 0;
        var custoParcial = 0;
        var issPlano = $('#iss').val();
        var custoExtraTotal = 0;
        if(issPlano == ''){
            alert('Favor adicionar o ISS para que o calculo seja feito.');
            return false;
        }
        var ir = $('#ir').val();
        var piscofins = $('#piscofins').val();

        $('.valor-item').each(function(i){
            var acrescimoDescontoItem = parseFloat($(this).attr('data-acrescimo-desconto'));
            var custoItem = parseFloat($(this).val().replace('.','').replace(',','.'));
            var quantidadeItem = parseInt($(this).attr('data-quantidade'));
            var valorFatura =parseFloat($(this).attr('data-valor-fatura'));

            desconto_acrescimo +=  parseFloat((quantidadeItem*valorFatura)*(acrescimoDescontoItem /100));
            valorTotal += parseFloat((quantidadeItem*valorFatura)+acrescimoDescontoItem);
            custoParcial += parseFloat(quantidadeItem*custoItem);
        });
        $('.calcula-custo-extra').each(function(i){

            if($(this).parent().parent().attr('cancelar') == 'N'){
                custoExtraTotal = parseFloat(custoExtraTotal);
                custoExtraTotal += parseFloat($(this).val().replace('.','').replace(',','.')) * parseFloat($(this).parent().parent().children('td').eq(1).children('input').val() ) ;

            }
        });

        custoTotal = parseFloat((valorTotal * (parseFloat(piscofins) + parseFloat(ir) + parseFloat(issPlano))/100) + custoParcial) + custoExtraTotal;
        var lucroReais = parseFloat(valorTotal - custoTotal);

        var lucroPorcento = valorTotal == 0 ? -100  : parseFloat(((valorTotal - custoTotal)/valorTotal)*100);

        $('#valor-total').html(float2moeda(valorTotal));
        $('#margem').html(float2moeda(lucroReais));
        $('#margem-percentual').html(lucroPorcento.toFixed(2));
        $('#custo-encargo').html(float2moeda(custoTotal));
    }
    $('#salvar-custo-fatura').click(function(){
        if($('#iss').val() == ''){
            alert('Favor adicionar o imposto ISS.');
            return false;
        }
        var total = $('.valor-item').size();
        var linha = '';
        var iss = $('#iss').val();
        var itemArray = [] ;
        var tabelaArray = [];
        var idFatura = $(this).attr('id-fatura');

        $('.valor-item').each(function(i){
            var idFaturamento = $(this).attr('data-id-faturamento');
            var codigoItem = $(this).attr('data-codigo-item');
            var tipoItem = $(this).attr('data-tipo');
            var acrescimoDescontoItem = $(this).attr('data-acrescimo-desconto');
            var custoItem = $(this).val();
            var paciente = $(this).attr('data-paciente');
            var custoItenFaturaId = $(this).attr('data-custo-iten-fatura-id');
            var quantidadeItem = $(this).attr('data-quantidade');
            var valorFatura =$(this).attr('data-valor-fatura');
            var tabelaOrigem = $(this).attr('data-tabela-origem');

            linha += idFaturamento+"#"+codigoItem+"#"+tipoItem+"#"+acrescimoDescontoItem
                +"#"+custoItem+"#"+paciente+"#"+custoItenFaturaId
                +"#"+quantidadeItem+"#"+valorFatura+"#"+tabelaOrigem ;
            if(i< total - 1){
                linha+= "$";
            }

        });

        var linhaCustoExtra = '';
        var totalCustoExtra = $(".tr-item-custo-extra").size();

        if(validar_campos('div-table-custo-extra')){
            $(".tr-item-custo-extra").each(function(i){
                var idCustoExtra =  $(this).children('td').eq(0).attr('id-custo-extra');
                var quantidade = $(this).children('td').eq(1).children('input').val();
                var custo = $(this).children('td').eq(2).children('input').val();
                var idFaturaCustoExtra = $(this).attr('id-fatura-custo-extra');
                var quantidadeAnterior = $(this).attr('quantidade-anterior');
                var custoAnterior = $(this).attr('custo-anterior');
                var cancelar = $(this).attr('cancelar');

                linhaCustoExtra += idCustoExtra+"#"+quantidade+"#"+custo+"#"+idFaturaCustoExtra
                    +"#"+quantidadeAnterior+"#"+custoAnterior+"#"+cancelar;
                if(i< totalCustoExtra - 1){
                    linhaCustoExtra+= "$";
                }

            });
        }else{
            return false;
        }
        $.post("query.php", {query: 'salvar-custo-fatura',
            linha: linha,
            tipo:'salvar',
            iss: iss,
            idFaturaFinalizarCusto : idFatura,
            linhaCustoExtra: linhaCustoExtra
        }, function(r) {

            if(r == 1){
                alert("Erro ao atualizar o ISS.");
                return false;
            }else if(r == 2){
                alert("Erro ao atualizar preço na fatura. Entre em contato com o TI.  Para não perder os dados click em voltar");
                return false;
            }else if(r == 3){
                alert("Erro ao inserir um custo.Entre em contato com o TI. Para não perder os dados click em voltar");
                return false;

            }else if(r == 4){
                alert("Erro ao finalizar um orçamento/fatura. Entre em contato com o TI. Para não perder os dados click em voltar");
                return false;

            }else if(r == 5){
                alert("Erro ao salvar itens. Entre em contato com o TI. Para não perder os dados click em voltar");
                return false;
            }
            $("#margem-fatura-custo").html(r);
            $("#margem-fatura-custo").dialog('open');
        });
    });

    $('.v_exportar').live('click',function(){
        var id = $(this).attr('idfatura');
        var av = $(this).attr('av');
        window.open('exportar_fatura_custo.php?id='+id+'&av='+av+'&zerados=1');
        // window.open('exportar_fatura_custo.php?id='+id+'&av='+av+'&zerados=1','_blank');
    });

    $('#div-finalizar-custo').dialog({
        autoOpen:false,
        buttons:{
            Confirmar: function() {
                if (validar_campos('div-finalizar-custo')){
                    var  dataMargem = $('.mes-ano').val();

                    var total = $('.valor-item').size();
                    var linhaCustoExtra = '';
                    var totalCustoExtra = $(".tr-item-custo-extra").size();
                    var linha = '';
                    var iss = $('#iss').val();

                    $('.valor-item').each(function(i){
                        var idFaturamento = $(this).attr('data-id-faturamento');
                        var codigoItem = $(this).attr('data-codigo-item');
                        var tipoItem = $(this).attr('data-tipo');
                        var acrescimoDescontoItem = $(this).attr('data-acrescimo-desconto');
                        var custoItem = $(this).val();
                        var paciente = $(this).attr('data-paciente');
                        var custoItenFaturaId = $(this).attr('data-custo-iten-fatura-id');
                        var quantidadeItem = $(this).attr('data-quantidade');
                        var valorFatura =$(this).attr('data-valor-fatura');
                        var tabelaOrigem = $(this).attr('data-tabela-origem');

                        linha += idFaturamento+"#"+codigoItem+"#"+tipoItem+"#"+acrescimoDescontoItem
                            +"#"+custoItem+"#"+paciente+"#"+custoItenFaturaId
                            +"#"+quantidadeItem+"#"+valorFatura+"#"+tabelaOrigem ;
                        if(i< total - 1){
                            linha+= "$";
                        }
                    });

                    var linhaCustoExtra = '';
                    var totalCustoExtra = $(".tr-item-custo-extra").size();

                    if(validar_campos('div-table-custo-extra')){
                        $(".tr-item-custo-extra").each(function(i){
                            var idCustoExtra =  $(this).children('td').eq(0).attr('id-custo-extra');
                            var quantidade = $(this).children('td').eq(1).children('input').val();
                            var custo = $(this).children('td').eq(2).children('input').val();
                            var idFaturaCustoExtra = $(this).attr('id-fatura-custo-extra');
                            var quantidadeAnterior = $(this).attr('quantidade-anterior');
                            var custoAnterior = $(this).attr('custo-anterior');
                            var cancelar = $(this).attr('cancelar');

                            linhaCustoExtra += idCustoExtra+"#"+quantidade+"#"+custo+"#"+idFaturaCustoExtra
                                +"#"+quantidadeAnterior+"#"+custoAnterior+"#"+cancelar;
                            if(i< totalCustoExtra - 1){
                                linhaCustoExtra+= "$";
                            }
                        });
                    }else{
                        return false;
                    }
                    $.post("query.php", {query: 'salvar-custo-fatura',
                        linha: linha,tipo:'finalizar',idFaturaFinalizarCusto: $('#idFaturaFinalizarCusto').val(),
                        dataMargem:dataMargem,
                        iss:iss,
                        linhaCustoExtra:linhaCustoExtra
                    }, function(r) {
                        if(r == 1){
                            alert("Erro ao atualizar o ISS.");
                            $('#div-finalizar-custo').dialog( "close" );
                            return false;
                        }else if(r == 2){
                            alert("Erro ao atualizar preço na fatura. Entre em contato com o TI.  Para não perder os dados click em voltar");
                            $('#div-finalizar-custo').dialog( "close" );
                            return false;
                        }else if(r == 3){
                            alert("Erro ao inserir um custo.Entre em contato com o TI. Para não perder os dados click em voltar");
                            $('#div-finalizar-custo').dialog( "close" );
                            return false;
                        }else if(r == 4){
                            alert("Erro ao finalizar um orçamento/fatura. Entre em contato com o TI. Para não perder os dados click em voltar");
                            $('#div-finalizar-custo').dialog( "close" );
                            return false;
                        }
                        $("#margem-fatura-custo").html(r);
                        $("#margem-fatura-custo").dialog('open');
                    });
                }
                return false;
            },
            Cancelar: function(){
                $( this ).dialog( "close" );
            }
        }
    });

    $('#finalizar-custo-fatura').click(function(){

        if($('#iss').val() == ''){
            alert('Favor adicionar o imposto ISS.');
            return false;
        }
        $('#div-finalizar-custo').dialog('open');


    });

    /*$(".mes-ano").datepicker({
        inline: true,
        changeMonth: true,
        changeYear: true
    });*/
    $('#selPaciente').chosen({search_contains: true});
    $('#pesquisar-relatorio-margem-contribuicao').click(function(){
        if(validar_campos('div-data')) {
            var paciente = $('#selPaciente').val();
            var plano = $('select[name=convenio]').val();
            var tipoDocumento = $('#tipo_documento').val();
            var inicio = $('#inicio').val();
            var fim = $('#fim').val();
            var empresa = $('#empresa-relatorio').val();
            var nomePaciente = paciente == -1 ? 'Todos' : $('#selPaciente').find('option:selected').text();
            var nomePlano = plano == 'T'  ? 'Todos' : $('select[name=convenio]').find('option:selected').text();
            var nomeTipoDocumento = tipoDocumento == -1 ? 'Todos' : $('#tipo_documento').find('option:selected').text();

            $.post("query.php", {
                query: 'pesquisar-rel-margem-contribuicao',
                fim: fim, inicio: inicio, tipoDocumento: tipoDocumento, plano: plano, paciente: paciente,
                empresa: empresa, nomePaciente: nomePaciente, nomePlano: nomePlano, nomeTipoDocumento: nomeTipoDocumento
            }, function (r) {
                $('#resultado-pesquisa-rel-margem').html(r);
                //console.log(r);
            });
        }else{
            return false;
        }
    });
    $('#exportar-excel-relatorio-margem-contribuicao').live('click',function(){
        var paciente = $(this).attr('data-paciente');
        var plano = $(this).attr('data-plano');
        var tipoDocumento = $(this).attr('data-tipodocumento');
        var inicio = $(this).attr('data-inicio');
        var fim = $(this).attr('data-fim');
        var empresa = $(this).attr('data-empresa');
        var nomePaciente = paciente == -1 ? 'Todos' : $(this).attr('data-nomepaciente');
        var nomePlano = plano == 'T'  ? 'Todos' : $(this).attr('data-nomeplano');
        var nomeTipoDocumento = tipoDocumento == -1 ? 'Todos' : $(this).attr('data-nometipodocumento');

        window.open('exportarExcelRelMargemContribuicao.php?paciente='+paciente+'&plano='+plano+
            '&tipoDocumento='+tipoDocumento+'&inicio='+inicio+'&fim='+fim+'&empresa='+empresa+
            '&nomePaciente='+nomePaciente+'&nomePlano='+nomePlano+'&nomeTipoDocumento='+nomeTipoDocumento);
        // window.open('exportar_fatura_custo.php?id='+id+'&av='+av+'&zerados=1','_blank');
    });


    $("#adicionar-custo-anterior").click(function(){
        if($('#iss').val() == ''){
            alert('Favor adicionar o ISS para que o calculo seja feito.');
            return false;
        }
        if($(this).is(':checked')){
            $('.valor').each(function(){

                var valor_atual = parseFloat($(this).val().replace('.','').replace(',','.'));

                if(valor_atual == 0){
                    $(this).val($(this).attr('data-custo-item-valor'));
                    calculaCusto();
                }

            });
        }

    });

    $('#transferencia-destino, #transferencia-origem').change(function(){
      
        var origem = $('#transferencia-origem').val();
        var destino = $('#transferencia-destino').val();
        if(origem == destino && (origem != 't' && destino !='t')){
            alert('Conta Origem não pode ser igual a conta Destino');
            $(this).val('');
        }

    });

    
});

$(".ipcg3_busca").live('change',function(){
    $(this).val();

    ipcg4_sel_busca();
    $("#sp_ipcg4_busca").show();

});

$(".ipcf2_busca").live('change',function(){
    $(this).val();
    ipcg3_sel_busca();
    $("#sp_ipcg3_busca").show();
    $("#sp_ipcg4_busca").hide();

});









$(".cancelar-informe-perda").live('click',function(){
    console.log('teste');
    var $this = $(this);
    var parcelaId = $this.attr('parcela-id');
    var notaId = $this.attr('nota-id');
    var informePerdaId = $this.attr('informe-perda-id');
    var query = $this
    if(confirm('Deseja realmente CANCELAR o Informe de Perda?')) {
        $.ajax({
            url: "/financeiros/informe-perda/?action=cancelar-informe-perda",
            type: 'POST',
            data: {parcelaId: parcelaId, 
                    notaId: notaId, 
                    informePerdaId: informePerdaId},
            success: function (response) {
                response = JSON.parse(response);                   
                if(response['tipo'] != 'erro') {
                    alert(response['msg']);
                    window.location.reload();
                } else {
                    alert(response['msg']);
                    
                }
                return false;
            }
        });
    }
});





function ipcf2_sel_busca(){

    var tipo = $("input[name='btipo']:checked").val();

    $.post("busca.php",{query: "ipcf2", tipo:tipo},
        function(r){

            $("#sp_ipcf2_b").html(r);
        });
}

function ipcg3_sel_busca(){

    var tipo = $("input[name='btipo']:checked").val();
    var ipcf2 = $(".ipcf2_busca").val();

    $.post("busca.php",{query: "ipcg3", tipo:tipo,ipcf2:ipcf2},
        function(r){

            $("#sp_ipcg3_b").html(r);
        });
}

function ipcg4_sel_busca(){

    var ipcg3 = $(".ipcg3_busca").val();
    var ipcf2 = $(".ipcf2_busca").val();
    var tipo = $(".tipo_nota:checked").val();
    var val = 1;
    $(".rateio").each(function(){

        val =$(this).children('td').eq(1).attr("name");

    });
    $.post("busca.php", {
        query: "ipcg4", tipo:tipo, ipcf2:ipcf2, ipcg3:ipcg3},
        function(r){

            $("td[name='ipcg4_" + val + "']").html(r);
            $('.chosen-select').chosen({search_contains: true});
        });
}


function validar(){
    d = document.all;
    if(d.codigo.value = ""){
        alert("Codigo não pode ficar em branco.");
        d.codigo.focus();
    }
}

function ConfirmChoice(acao,id,cod,cmd)
{
    ok = confirm("Deseja realmente " + acao + " "+ cod +" ?")
    if (ok != 0){
        location = window.location = cmd;
    }
}

function replaceValorMonetarioFloat(valor)
{
    return valor.replace(/\./g,'').replace(/,/g,'.')
}

//BASE64_ENCODE PARA JAVASCRIPT
var Base64 = {

    // private property
    _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

    // public method for encoding
    encode : function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
                this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
                this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },

    // public method for decoding
    decode : function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = Base64._utf8_decode(output);

        return output;

    },

    // private method for UTF-8 encoding
    _utf8_encode : function (string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

   

    // private method for UTF-8 decoding
    _utf8_decode : function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while ( i < utftext.length ) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }
}
//FIM BASE64_ENCODE PARA JAVASCRIPT

