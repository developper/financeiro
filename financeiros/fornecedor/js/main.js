$(function($) {

    $(".chosen-select").chosen({search_contains: true});

    $(".voltar_").click(function(event) {
        if(confirm('Deseja realmente sair dessa página?')){
            event.preventDefault();
            history.back(1);
        }
        return false;
    });

    $(".fornecedor-busca").on('change', function(){
        var fornecedor = $(this).val();

        $.get(
            '/financeiros/fornecedor/?action=equipamentos-fornecedor',
            {
                fornecedor: fornecedor
            },
            function(r){
                var obj = jQuery.parseJSON(r);
                obj = alphabeticalSortObjects(obj, 'item');

                $("#equipamentos-fornecedor").find('tbody').html("");
                $.each(obj, function(index, value) {
                    $("#equipamentos-fornecedor").find('tbody').append(
                        '<tr>' +
                        '<td>' + value.item + '</td>' +
                        '<td>' + value.unidade + '</td>' +
                        '<td><input type="text" style="text-align: right" class="valor valor_aluguel" cobrancaplanosid="' + value.id + '" value="' + value.valor_aluguel.replace('.', ',') + '"></td>' +
                        '</tr>'
                    );
                });

                $(".valor").priceFormat({
                    prefix: '',
                    centsSeparator: ',',
                    thousandsSeparator: '.'
                });
            });
    });

    $(".fornecedor-busca-email").on('change', function(){
        var fornecedor = $(this).val();

        $.get(
            '/financeiros/fornecedor/?action=get-email-fornecedor-equipamento',
            {
                fornecedor: fornecedor
            },
            function(r){
                var obj = jQuery.parseJSON(r);

                var element = '';

                element += '<div class="panel panel-default" id="panel-adicionar-email">'+
                    '<div class="panel-heading clearfix" >'+
                        '<div class="col-md-12">'+
                            '<h3 class="panel-title pull-left" style="padding-top: 7.5px;">'+
                            '<i class="fa fa-at" aria-hidden="true"></i> E-mails já Cadastrados:'+
                             '</h3>'+
                       '</div>'+
                   '</div>'+
                   '<div class="panel-body">';
                $("#panel-adicionar-email").find('.panel-body').html("");

                $("#div-emails-cadastrados").html("");
                if(obj.length > 0){
                    obj = alphabeticalSortObjects(obj, 'email');
                    $.each(obj, function(index, value) {

                        element += "<div class='col-md-12 form-group'>" +
                        "<div class='col-md-1'>" +
                        "<div id-email='"+value.id+"' class='btn-sm btn-danger  deletar-email  text-center'>" +
                        "<i class='fa fa-minus' aria-hidden='true'> </i>" +
                        "</div>" +
                        "</div>"+
                        "<div class='col-md-5'>" +
                        value.email+
                        "</div>" +
                        "</div>"
                    });

                }else{
                    element += '<div class="col-md-12 text-center"><b>Nenhum e-mail cadastrado.</b></div>'
                }
                element += '</div>'+
                        '</div>';
                $("#div-emails-cadastrados").append(
                    element
                );

            });
    });

    $('#adicionar-novo-email-fornecedor').click(function () {

        if($('.fornecedor-busca-email').val() == ''){
            alert('Escolha um fornecedor antes de adicinar e-mail.');
            return;
        }

        $("#panel-adicionar-email").find('.panel-body').append(
                "<div class='col-md-12 form-group'>" +
                    "<div class='col-md-1'>" +
                        "<div class='btn-sm btn-danger  remover-email text-center '>" +
                        "<i class='fa fa-minus' aria-hidden='true'></i>" +
                        "</div>" +
                    "</div>"+
                    "<div class='col-md-5'>" +
                        "<input type='text' name='novo-email[]' class='form-control OBG' required>" +
                    "</div>" +
                "</div>");
    });

    $('.remover-email').live('click', function(){

        $(this).closest('.form-group').remove();
    });
    $('.deletar-email').live('click', function(){
        var element = $(this);
        if(confirm('Deseja realmente deletar e-mail?')){
            $.post(
                '/financeiros/fornecedor/?action=deletar-email-fornecedor-equipamento',
                {
                    id:element.attr('id-email')
                },
                function(r){
                    if(r == 1){
                        alert("E-mail deletado com sucesso");
                        element.closest('.form-group').remove();

                    }else{
                        alert(r);
                    }



                });
        }


    });

    $("#salvar-novo-email").on('click', function(){
        if(validar_campos('right')){
            var fornecedor = $(this).val();
            var dados = $('form').serialize();
           if($("#right").find("input[name='novo-email[]']").length == 0){
               alert("Adicione ao menos um novo e-mail para poder salvar.")
           }
            $.post(
                '/financeiros/fornecedor/?action=salvar-email-fornecedor-equipamento',
                dados,
                function(r){

                    if(r.indexOf('Erro:') > -1){
                        alert(r);
                        return false;
                    }else{
                        alert('E-mail Salvo com sucesso.');

                        var obj = jQuery.parseJSON(r);
                        $("#panel-adicionar-email").find('.panel-body').html("");
                        var element = '';

                        element += '<div class="panel panel-default" id="panel-adicionar-email">'+
                            '<div class="panel-heading clearfix" >'+
                            '<div class="col-md-12">'+
                            '<h3 class="panel-title pull-left" style="padding-top: 7.5px;">'+
                            '<i class="fa fa-at" aria-hidden="true"></i> E-mails já Cadastrados:'+
                            '</h3>'+
                            '</div>'+
                            '</div>'+
                            '<div class="panel-body">';
                        $("#panel-adicionar-email").find('.panel-body').html("");

                        $("#div-emails-cadastrados").html("");
                        if(obj.length > 0){
                            obj = alphabeticalSortObjects(obj, 'email');
                            $.each(obj, function(index, value) {

                                element += "<div class='col-md-12 form-group'>" +
                                    "<div class='col-md-1'>" +
                                    "<div id-email='"+value.id+"' class='btn-sm btn-danger  deletar-email  text-center'>" +
                                    "<i class='fa fa-minus' aria-hidden='true'> </i>" +
                                    "</div>" +
                                    "</div>"+
                                    "<div class='col-md-5'>" +
                                    value.email+
                                    "</div>" +
                                    "</div>"
                            });

                        }else{
                            element += '<div class="col-md-12 text-center"><b>Nenhum e-mail cadastrado.</b></div>'
                        }
                        element += '</div>'+
                            '</div>';
                        $("#div-emails-cadastrados").append(
                            element
                        );
                    }


                });
        }

    });

    function alphabeticalSortObjects(data, attr) {
        var arr = [];
        for (var prop in data) {
            if (data.hasOwnProperty(prop)) {
                var obj = {};
                obj[prop] = data[prop];
                obj.tempSortName = data[prop][attr].toLowerCase();
                arr.push(obj);
            }
        }

        arr.sort(function(a, b) {
            var at = a.tempSortName,
                bt = b.tempSortName;
            return at > bt ? 1 : ( at < bt ? -1 : 0 );
        });

        var result = [];
        for (var i=0, l=arr.length; i<l; i++) {
            var obj = arr[i];
            delete obj.tempSortName;
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    var id = prop;
                }
            }
            var item = obj[id];
            result.push(item);
        }
        return result;
    }

    $("#salvar-equipamentos-fornecedor").click(function(){
        var fornecedor = $(".fornecedor-busca").val();
        var dados = {};
        var valor_aluguel;
        var cobrancaplanos_id;
        var i = 0;

        $(".valor_aluguel").each(function () {
            cobrancaplanos_id = $(this).attr('cobrancaplanosid');
            valor_aluguel = $(this).val();
            valor_aluguel = parseFloat(valor_aluguel.replace('.', '').replace(',', '.'));
            dados[cobrancaplanos_id] = valor_aluguel;
        });

        $.post(
            '/financeiros/fornecedor/?action=salvar-equipamentos-fornecedor',
            {
                fornecedor: fornecedor,
                dados: dados
            },
            function(r){
                if(r == 1){
                    alert('Custos atualizados com sucesso!');
                    $("#equipamentos-fornecedor").find('tbody').html(
                        '<tr>' +
                        '<td colspan="3" align="center"><b>Escolha um fornecedor para começar...</b></td>' +
                        '</tr>'
                    );
                    $(".fornecedor-busca").val("");
                    $(".chosen-select").trigger("chosen:updated");
                }else{
                    alert("Erro ao atualizar os custos dos equipamentos. Entre em contato com o TI");
                    return false;
                }
            });
    });

});
