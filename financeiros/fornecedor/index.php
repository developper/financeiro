<?php

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';



session_start();

use \App\Controllers\Financeiro;

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);



$routes = [
	"GET" => [
        '/financeiros/fornecedor/?action=equipamentos-fornecedor' => '\App\Controllers\Financeiro::equipamentosFornecedorJSON',
		'/financeiros/fornecedor/?action=equipamentos-fornecedor-form'  => '\App\Controllers\Financeiro::equipamentosFornecedorForm',
        '/financeiros/fornecedor/?action=gerenciar-email-fornecedor-equipamento-form'  => '\App\Controllers\Financeiro::gerenciarEmailFornecedorEquipamentoForm',
        '/financeiros/fornecedor/?action=fornecedor-equipamento-menu'  => '\App\Controllers\Financeiro::fornecedorEquipamentoMenu',
        '/financeiros/fornecedor/?action=get-email-fornecedor-equipamento' => '\App\Controllers\Financeiro::getEmailfornecedorEquipamentoJSON',
    ],
	"POST" => [
		'/financeiros/fornecedor/?action=salvar-equipamentos-fornecedor'  => '\App\Controllers\Financeiro::salvarEquipamentosFornecedor',
        '/financeiros/fornecedor/?action=salvar-email-fornecedor-equipamento' => '\App\Controllers\Financeiro::salvarEmailFornecedorEquipamento',
        '/financeiros/fornecedor/?action=deletar-email-fornecedor-equipamento' => '\App\Controllers\Financeiro::deletarEmailFornecedorEquipamento',
	]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}
