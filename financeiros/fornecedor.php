<?php
include_once('../db/config.php');
include_once('../utils/codigos.php');

use \App\Models\Financeiro\Fornecedor;
use \App\Models\Financeiro\ClasseValor;

function menu_fornecedor(){
    $var  = "<div><center><h1>Fornecedores/Clientes</h1></center></div>";

    $var .= "<a href='?op=fornecedor&act=listar'>Listar Fornecedores/Clientes</a><br>";
    if(isset($_SESSION['permissoes']['financeiro']['financeiro_fornecedor_cliente']['financeiro_fornecedor_cliente_novo']) || $_SESSION['adm_user'] == 1) { 
        $var .=  "<a href=?op=fornecedor&act=novo>Cadastrar Fornecedor/Cliente</a><br>";
    }

    return $var;
}

function list_fornecedor($like) {
    $tipo = [
        'F' => 'Fornecedor',
        'C' => 'Cliente',
    ];
    $pessoa = [
        'F' => 'Física',
        'J' => 'Jurídica',
    ];
    $pessoaSQL = [
        'F' => 0,
        'J' => 1,
    ];

    $compTipo = '';
    $compStatus = " ";
    $compPessoa = '';
    $compLike = '';

    if(isset($_GET['brf']) && !empty($_GET['brf'])){
        $compLike =" AND (razaoSocial LIKE '%{$like}%' OR cnpj LIKE '%{$like}%' OR CPF LIKE '%{$like}%' OR nome LIKE '%{$like}%')";
    }

    if(isset($_GET['tipo']) && $_GET['tipo'] != ''){
        $compTipo = "AND TIPO = '{$_GET['tipo']}'";
    }

    if(isset($_GET['ativo']) && $_GET['ativo'] != 'T' && $_GET['ativo'] != ''){
        $compStatus = " AND ATIVO = '{$_GET['ativo']}'";
    } elseif(isset($_GET['ativo']) && $_GET['ativo'] == 'T') {
        $compStatus = "";
    }

    if(isset($_GET['pessoa']) && $_GET['pessoa'] != ''){
        $compPessoa = "AND FISICA_JURIDICA = '{$pessoaSQL[$_GET['pessoa']]}'";
    }

    if(!isset($_GET['pg'])) $_GET['pg'] = 1;
    if(!isset($_GET['offset'])) $_GET['offset'] = 50;

    $ant = (int) $_GET['pg'] -1;
    $prox = (int) $_GET['pg'] + 1;
    $start = ($_GET['pg']-1)*$_GET['offset'];

    // todos os registros
    $sqlGeral = "SELECT * FROM fornecedores where 1=1 $compTipo $compPessoa $compStatus $compLike";
    $resultGeral = mysql_query($sqlGeral);
    $quantidadeTotalRegistros = mysql_num_rows($resultGeral);
    $totalPages = ceil($quantidadeTotalRegistros/50);

    if($totalPages < 6) {
        $pages = range(1, $totalPages);
    } else {
        $limit = 2;
        $currentPageNumber = $_GET['pg'];
        $firstPageNumber = ($currentPageNumber - $limit) > 1 ? $currentPageNumber - $limit : 1;
        $lastPageNumber = ($currentPageNumber + $limit) < $totalPages ? $currentPageNumber + $limit : $totalPages;
        $pages = range($firstPageNumber, $lastPageNumber);
    }

    // Rgistro Filtrados
    $sqlFiltrado = "SELECT * FROM fornecedores where 1=1 $compTipo $compPessoa $compStatus $compLike LIMIT {$start},{$_GET['offset']}";
    //$sqlFiltrado = "SELECT * FROM fornecedores where 1=1 $compTipo $compPessoa $compStatus $compLike";

    $resultFiltrado = mysql_query($sqlFiltrado);
    $quantidadeRegistrosNaPagina = mysql_num_rows($resultFiltrado);

    if(!isset($_GET['brf'])) $totalPages = $_GET['last'];

    echo " <link rel='stylesheet' type='text/css' href='/utils/reports.css' />
    <link rel='stylesheet' href='/utils/bootstrap-3.3.6-dist/css/bootstrap.min.css' />
    <link rel='stylesheet' href='/utils/font-awesome-4.7.0/css/font-awesome.css' />
    <link rel='stylesheet' href='/utils/jquery/css/toggle/toggle.css' />
    <link rel='stylesheet' href='/utils/bootstrap-3.3.6-dist/css/chosen.bootstrap.min.css' />
    <script src='/utils/bootstrap-3.3.6-dist/js/bootstrap.min.js' ></script>
    <div class='form-group'>
        <div class='text-center col-xs-12'>
        <h1 style='font: bold 2.1em Arial, Arial, Sans-Serif;
        color: #8B0000;'>Listar Fornecedores/Clientes<h1>
        </div>
    </div>";
    if(isset($_SESSION['permissoes']['financeiro']['financeiro_fornecedor_cliente']['financeiro_fornecedor_cliente_novo']) || $_SESSION['adm_user'] == 1) { 
        echo "<div class='form-group text-right'>
        <a style='color: white;' class='btn btn-primary' href='?op=fornecedor&act=novo' role='button'>            
            + Novo
        </a>
        </div>
        ";
    }
    
   
    echo" <form name='input' action='?op=fornecedor&act=listar' method='GET' >
    
              <div class='form-group'>
                <div class='row'>
                    <div class='col-xs-6 '>
                        <label for='brf'>
                            <b>Razão Social / Nome / CNPJ / CPF: </b>
                        </label>				
                        <input type='text' name='brf' id='brf' class='form-control' value='{$_GET['brf']}' /> 
                    </div>
                    <div class='col-xs-6 '>
                        <label for='tipo'>
                            <b>Tipo:</b> 
                        </label>            
                        <select  name='tipo' id='tipo' class='form-control'>
                        <option value=''>Todos</option>
                        ";
    foreach ($tipo as $key => $t) {
        echo "			<option " . ($_GET['tipo'] == $key ? 'selected' : '') . " value='{$key}'>{$t}</option>";
    }
    echo "</select>            
                    </div>
                </div>
                <div class='row'>
                    <div class='col-xs-6' >
                        <label for='pessoa'>
                            <b>Pessoa:</b> 
                        </label>           
                        <select name='pessoa' id='pessoa' class='form-control'>
                            <option value=''>Todos</option>";
    foreach ($pessoa as $key => $p) {
        echo "<option " . ($_GET['pessoa'] == $key ? 'selected' : '') . " value='{$key}'>{$p}</option>";
    }
    echo "</select>
                
                    </div>
                    <div class='col-xs-6 '>
                        <label for='status' >
                            <b>Status:</b> 
                        </label>
                        <select name='ativo' id='status' class='form-control'>
                        <option " . (isset($_GET['ativo']) && $_GET['ativo'] == 'T' ? 'selected' : '') . " value='T' selected>Todos</option>
                            <option " . (isset($_GET['ativo']) && $_GET['ativo'] == 'S' ? 'selected' : '') . "  value='S'>Somente Ativos</option>
                            <option " . (isset($_GET['ativo']) && $_GET['ativo'] == 'N' ? 'selected' : '') . " value='N'>Somente Inativos</option>
                           
                        </select>         
                    </div>
                </div>            
             </div>
             <div class='form-group'>
                <div class='col-xs-12 text-center row'>
                    <input type='submit' value='Buscar' class='btn btn-primary' />                                     
                    
                </div>
            </div>
             
    </form>";
    echo "<div class='form-group col-xs-12'>";
    if($quantidadeRegistrosNaPagina > 0){
        echo "<div class='row'>
        <div class='col-xs-12 text-right'>
        <span class=' text-right'> 
         Mostrando {$quantidadeRegistrosNaPagina} de {$quantidadeTotalRegistros} registro(s) encontrado(s).
    </span>
        </div>
        
               
             </div>";
    }else{
        echo "<div class='row'>
                    <span> Nenhum registro encontrado.</span>    
                </div>";
    }
    echo "</div>";
    if ($quantidadeRegistrosNaPagina > 0) {
        echo "<ul class='pagination'>";
        if ($_GET['pg'] <> 1) {
            echo "<li><a href='?op=fornecedor&act=listar&pg={$ant}&last={$totalPages}&brf={$_GET['brf']}&tipo={$_GET['tipo']}&pessoa={$_GET['pessoa']}&ativo={$_GET['ativo']}'>Anterior</a></li>";
        } else {
            echo "<li><a href='#'>Anterior</a></li>";
        }

        foreach ($pages as $page) {
            if($_GET['pg'] != $page) {
                echo "<li><a href='?op=fornecedor&act=listar&pg={$page}&last={$totalPages}&brf={$_GET['brf']}&tipo={$_GET['tipo']}&pessoa={$_GET['pessoa']}&ativo={$_GET['ativo']}'>{$page}</a></li>";
            } else {
                echo "<li class='active'><a href='#'>{$page}</a></li>";
            }
        }

        if ($_GET['pg'] == $totalPages) {
            echo "<li><a href='#'>Próxima</a></li>";
        } else {
            echo "<li><a href='?op=fornecedor&act=listar&pg={$prox}&last={$totalPages}&brf={$_GET['brf']}&tipo={$_GET['tipo']}&pessoa={$_GET['pessoa']}&ativo={$_GET['ativo']}'>Próxima</a></li>";
        }
        echo "</ul>";
        echo "<div class='col-xs-12 text-right' style='text-align: right;'>
            <span>
                <button target='_blank' class='btn btn-default imprimir_parcelas' type='button' role='button' aria-disabled='false'>
                    <i class='fa fa-file-excel-o' aria-hidden='true'></i> 
                    Exportar</span>
                </button>
            </span> 
        </div>";
    }
    echo "<div class='col-xs-12' style='overflow: auto;'>";
    echo "<table class='table table-bordered table-responsive table-striped' width=100% ><thead >";
    echo "<tr>";
    echo "<th><b>RAZ&Atilde;O SOCIAL/NOME</b></th>";
    echo "<th><b>NOME FANTASIA/APELIDO</b></th>";
    echo "<th><b>CNPJ/CPF</b></th>";
    if(!isset($_GET['tipo']) || $_GET['tipo'] == '') {
        echo "<th><b>TIPO</b></th>";
    }
    if(!isset($_GET['pessoa']) || $_GET['pessoa'] == '') {
        echo "<th><b>PESSOA</b></th>";
    }
    echo "<th><b>TELEFONE</b></th>";
    echo "<th><b>ATIVO</b></th>";
    echo "<th colspan='3' ><b>OP&Ccedil;&Otilde;ES</b></th>";
    echo "</tr></thead><tfoot><tr><th colspan='10'></th></tr></tfoot><tbody>";
    $cor = false;
    while($row = mysql_fetch_array($resultFiltrado)){
        foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
        if($cor) echo "<tr >";
        else echo "<tr class='odd'>";
        $cor = !$cor;
        $nome = $row['nome'];
        if($nome == "")
            $nome = substr($row['razaoSocial'],0,15);

        $cnpjcpf = $row['cnpj'] != '' ? $row['cnpj'] : $row['CPF'];
        echo "<td valign='top'>{$row['razaoSocial']}</td>";
        echo "<td valign='top'>$nome</td>";
        echo "<td valign='top'>{$cnpjcpf}</td>";
        if(!isset($_GET['tipo']) || $_GET['tipo'] == '') {
            echo "<td valign='top'>" . $tipo[$row['TIPO']] . "</td>";
        }
        if(!isset($_GET['pessoa']) || $_GET['pessoa'] == '') {
            echo "<td valign='top'>" . ($row['FISICA_JURIDICA'] == '0' ? 'Física' : 'Jurídica') . "</td>";
        }
        echo "<td valign='top'>" . $row['telefone'] . "</td>";
        echo "<td valign='top'>" . ($row['ATIVO'] == 'S' ? 'SIM' : 'NÃO') . "</td>";
        $del = "index.php?op=fornecedor&act=excluir&idFornecedores={$row['idFornecedores']}";
        echo "<td style='padding: -2px; color: white;'>
                    <a style='color: white;' class='btn btn-sm btn-primary' title='Visualizar' href=?op=fornecedor&act=editar&idFornecedores={$row['idFornecedores']}&visualizar=1>
                        <i class='fa fa-eye'></i>
                    </a>
              </td>";
              
              if(isset($_SESSION['permissoes']['financeiro']['financeiro_fornecedor_cliente']['financeiro_fornecedor_cliente_editar']) || $_SESSION['adm_user'] == 1) { 
                    echo"<td><a style='color: white;' class='btn btn-sm btn-warning' title='Editar' href=?op=fornecedor&act=editar&idFornecedores={$row['idFornecedores']}&visualizar=0>
                        <i class='fa fa-edit'></i>
                    </a>
                    </td>";
              }
        if($row['ATIVO'] == 'S'){
            
            if(isset($_SESSION['permissoes']['financeiro']['financeiro_fornecedor_cliente']['financeiro_fornecedor_cliente_desativar']) || $_SESSION['adm_user'] == 1) { 
                    echo "<td>
                            <a style='color: white;' class='btn btn-sm btn-danger' title='Desativar' href=\"javascript:ConfirmChoice('desativar','{$row['idFornecedores']}','{$row['razaoSocial']}','{$del}')\">
                                <i class='fa fa-times-circle-o'></i>
                            </a>
                            </td>";
            }
        }else{
            echo "<td></td>";

        }
        echo "</tr>";
    }
    echo "</tbody></table>";
    if ($quantidadeRegistrosNaPagina > 0) {
        echo "<ul class='pagination'>";
        if ($_GET['pg'] <> 1) {
            echo "<li><a href='?op=fornecedor&act=listar&pg={$ant}&last={$totalPages}&brf={$_GET['brf']}&tipo={$_GET['tipo']}&pessoa={$_GET['pessoa']}&ativo={$_GET['ativo']}'>Anterior</a></li>";
        } else {
            echo "<li><a href='#'>Anterior</a></li>";
        }

        foreach ($pages as $page) {
            if($_GET['pg'] != $page) {
                echo "<li><a href='?op=fornecedor&act=listar&pg={$page}&last={$totalPages}&brf={$_GET['brf']}&tipo={$_GET['tipo']}&pessoa={$_GET['pessoa']}&ativo={$_GET['ativo']}'>{$page}</a></li>";
            } else {
                echo "<li class='active'><a href='#'>{$page}</a></li>";
            }
        }

        if ($_GET['pg'] == $totalPages) {
            echo "<li><a href='#'>Próxima</a></li>";
        } else {
            echo "<li><a href='?op=fornecedor&act=listar&pg={$prox}&last={$totalPages}&brf={$_GET['brf']}&tipo={$_GET['tipo']}&pessoa={$_GET['pessoa']}&ativo={$_GET['ativo']}'>Próxima</a></li>";
        }
        echo "</ul>";
    }
    echo "</div>";
}

function new_fornecedor() {
    echo "<link rel='stylesheet' type='text/css' href='/utils/reports.css' />
    <link rel='stylesheet' href='/utils/bootstrap-3.3.6-dist/css/bootstrap.min.css' />
    <link rel='stylesheet' href='/utils/font-awesome-4.7.0/css/font-awesome.css' />
    <link rel='stylesheet' href='/utils/jquery/css/toggle/toggle.css' />
    <link rel='stylesheet' href='/utils/bootstrap-3.3.6-dist/css/chosen.bootstrap.min.css' />
    <link rel='stylesheet' href='/utils/jquery/css/selectize/selectize.bootstrap3.css' />
    <script src='/utils/bootstrap-3.3.6-dist/js/bootstrap.min.js' ></script>
    <script type='text/javascript' src='/utils/jquery/js/bootstrap-toggle.min.js'></script>
    <script src='/utils/jquery/js/selectize/selectize.js' ></script>";
    echo "<div id='div-salvar-fornecedor' class='form-group'>";
    echo alerta();
    echo " <div class='row text-center'>
                <h1 style='font: bold 2.1em Arial, Arial, Sans-Serif;color: #8B0000;'>
                    Cadastrar Fornecedor/Cliente
                </h1>
            </div>";
    echo "<fieldset>";

//echo "<form action='' method='POST'>";
    echo "
            <div class='form-group row'>
                <div class='col-xs-6'>
                    <label for='tipo-cadastro'>
                    <b>Tipo</b>
                    </label>
                    <select name='tipo' id='tipo-cadastro' class='tipo-cadastro OBG form-control'>
                        <option value = ''></option>
                        <option value='F'>Fornecedor</option>
                        <option value='C'>Cliente</option>
                    </select>
                </div>     
                <div class='col-xs-6'>
                    <label for='ipcc'>
                        Conta Contábil
                    </label>
                    <select class='form-control ' data-placeholder='Selecione uma conta...' name='ipcc' id='ipcc'>
                        <option value='' selected>NENHUMA</option>";

    $dados = listar_ipcc();
    foreach($dados as $nivel1 => $nivel2){
        echo "	<optgroup label='{$nivel1}'>";
        foreach($nivel2 as $n2 => $nivel3){
            echo "	<optgroup label='{$n2}'>";
            foreach($nivel3 as $n3 => $nivel4){
                echo "	<optgroup label='{$n3}'>";
                foreach($nivel4 as $n4 => $nivel5){
                    echo "	<optgroup label='{$n4}'>";
                    foreach($nivel5 as $id => $n5){
                        echo "	<option value='{$id}'>{$n5}</option>";
                    }
                    echo "  </optgroup>";
                }
                echo "	</optgroup>";
            }
            echo "  </optgroup>";
        }
        echo "  </optgroup>";
    }
    echo "        </select>
                </div>           
                <div class='col-xs-12'>
                    <label for='nome-fantasia'>
                        <b>Nome Fantasia/Apelido</b>
                    </label>
                    <input class='OBG form-control' id='nome-fantasia' type='text' name='nome' />
                    
                </div>
                <div class='col-xs-12' for='razaoSocial'>
                    <label>
                        <b>Raz&atilde;o Social/Nome</b>
                    </label>
                    <input class='OBG form-control' id='razaoSocial' type='text' name='razaoSocial' />
                    
                </div>
                <div class='col-xs-6'>                    
                    <input class='cpf_cnpj' id='radio-cnpj' checked type='radio' name='cpf_cnpj' value='1' />
                    <label for='radio-cnpj'>
                    <b>CNPJ </b>
                    </label>
                    <input class='OBG form-control' type='text' name='cnpj'  id='cnpj' size='30' />
                </div>
                <div class='col-xs-6'>                    
                    <input class='cpf_cnpj' id='radio-cpf' type='radio' name='cpf_cnpj' value='0' />
                    <label for='radio-cpf'>
                    <b>CPF </b>&nbsp;
                    </label>
                    <input class='form-control' type='text' disabled name='cpf' id='cpf' size='30' />
                    <span id='cpf_invalido' class='text-red'></span>
                </div>  
                <div class='col-xs-6'> 
                    <label for='ies'>
                        <b>Inscrição Estadual/RG</b>
                    </label> 
                        <input class='OBG form-control' type='text' id='ies' name='ies' size='30'/>                    
                </div>  
                <div class='col-xs-6'>
                    <label for='im'>
                        <b>Inscrição Municipal</b>
                    </label>
                        <input  type='text' name='im' id='im' size='20' class ='form-control' />
                    
                </div> 
                <div class='col-xs-6'>
                    <label for='contato'>
                    <b>Contato</b>
                    </label>
                    <input class='OBG form-control' id='contato' type='text' name='contato' size='60'/>                    
                </div>                 
                <div class='col-xs-3'>
                    <label><b>Telefone Fixo / FAX</b></label>
                        <input id='tel' class=' form-control telefone' maxlength='14' type='text' name='telefone' size='26'/>
                </div>
                <div class='col-xs-3'>
                    <label><b>Telefone Celular</b></label>
                        <input id='' class='form-control celular' maxlength='15' type='text' name='celular' size='26'/>
                </div>
                
                <div class='col-xs-6'>
                    <label for='email'>
                    <b>Email</b>
                    </label>
                    <input type='text' class='form-control' name='email' id='email' size='50'/>
                </div>                  
                <div class='col-xs-6'>
                    <label for='cidade-und'>
                        <b>Cidade</b>
                    </label>
                        <input type='text'  class='OBG form-control' id='cidade-und' name='cidade_und' size=35 id='cidade_und'/>
                        <input type='hidden' name='id_cidade_und' id='id_cidade_und' value='' />                    
                </div> 
                <div class='col-xs-6'>
                    <label for='bairro'>
                        <b>Bairro</b>
                    </label>
                    <input class='OBG form-control' type='text' size='92' id='bairro' name='bairro'/>                    
                </div> 
                <div class='col-xs-6'>
                    <label for='endereco'>
                        <b>Endere&ccedil;o</b>
                    </label>
                    <input class='OBG form-control' type='text' size='92' id='endereco' name='endereco'/>                    
                </div>     
                <div class='col-xs-12'>
                    <label for='classe-valor'>
                        <b>Grupo de Fornecedores</b>
                    </label>
                    <select 
                        id='classe-valor' 
                        name='classe_valor'
                        required 
                        class='form-control classe-valor-selectize' 
                        placeholder='Selecione uma classe de valor...' 
                        tabindex='-1' 
                        style='display: none; text-transform: uppercase'>
                        <option value='' selected='selected'></option>";
    $classesValor = ClasseValor::getAll();
    foreach ($classesValor as $classeValor) {
        echo "<option value='{$classeValor['id']}'>{$classeValor['descricao']}</option>";
    }
    echo "          </select>                   
                </div>  
                <div class='col-xs-6'>
                <label for='tipo-cadastro'>
                <b>Precisa classificar?</b>
                </label>
                <select name='precisa_classificar' id='fornecedor-precisa-classificar' class=' OBG form-control'>
                    <option value = ''></option>
                    <option value='N'>Não</option>
                    <option value='S'>Sim</option>
                </select>
            </div>   
            <div class='col-xs-6' id='div-fornecedor-classificacao' hidden >
            <label for='classificacao'>
            <b>Classificação</b>
            </label>
            <select name='classificacao' id='fornecedor-classificacao' class=' form-control'>
                <option value = ''></option>
                <option value='A'>A</option>
                <option value='B'>B</option>
                <option value='C'>C</option>
                <option value='D'>D</option>
                
            </select>
        </div>  
        <div class='col-xs-6'>
                <label for='tipo-cadastro'>
                <b>Plano de saude?</b>
                </label>
                <select name='plano_saude' id='fornecedor-plano-saude' class=' OBG form-control'>
                    <option value = ''></option>
                    <option value='N'>Não</option>
                    <option value='S'>Sim</option>
                </select>
            </div>  
            <div class='col-xs-12'>
                <label for='observacao'>
                <b>Observação</b>
                </label>
                <input name='observacao' id='observacao' class='form-control'></input>
            </div>            
            </div>";

    echo "</fieldset>";

    echo "<fieldset>
            <legend>
                Alíquotas <br>
                <small style='font-size: small'>
                    <b>
                    * Preencher somente se o fornecedor/cliente possuir alíquotas diferentes das já discriminadas  
                    nas naturezas do plano de contas financeiro.
                    </b>
                </small>
            </legend>";
    echo "<div class='row'>
            <div class='col-xs-2'>
                <label for='recolhe-iss'><b>Recolhe ISS?</b></label>
                <input name='recolhe-iss' id='recolhe-iss' type='checkbox'
                       data-toggle='toggle' data-on='Sim' data-off='Não' 
                       data-onstyle='success' data-offstyle='danger' 
                       data-size='small' class='bt-toggle recolhe-retencao' 
                       value='1'>
            </div>
            <div class='col-xs-4'>
                <label for='iss'><b>ISS:</b></label>
                <div class='input-group'>
                  <input type='text' class='form-control valor' disabled name='iss' id='iss' value='0,00' />
                  <span class='input-group-addon'>%</span>
                </div>
            </div>
            <div class='col-xs-2'>
                <label for='recolhe-ir'><b>Recolhe IR?</b></label>
                <input name='recolhe-ir' id='recolhe-ir' type='checkbox'
                       data-toggle='toggle' data-on='Sim' data-off='Não' 
                       data-onstyle='success' data-offstyle='danger' 
                       data-size='small' class='bt-toggle recolhe-retencao' 
                       value='1'>
            </div>
            <div class='col-xs-4'>
                <label for='ir'><b>IR:</b></label>
                <div class='input-group'>
                  <input type='text' class='form-control valor' disabled name='ir' id='ir' value='0,00' />
                  <span class='input-group-addon'>%</span>
                </div>
            </div>
          </div>
          <div class='row'>
            <div class='col-xs-2'>
                <label for='recolhe-ipi'><b>Recolhe IPI?</b></label>
                <input name='recolhe-ipi' id='recolhe-ipi' type='checkbox'
                       data-toggle='toggle' data-on='Sim' data-off='Não' 
                       data-onstyle='success' data-offstyle='danger' 
                       data-size='small' class='bt-toggle recolhe-retencao' 
                       value='1'>
            </div>
            <div class='col-xs-4'>
                <label for='ipi'><b>IPI:</b></label>
                <div class='input-group'>
                  <input type='text' class='form-control valor' disabled name='ipi' id='ipi' value='0,00' />
                  <span class='input-group-addon'>%</span>
                </div>
            </div>
            <div class='col-xs-2'>
                <label for='recolhe-icms'><b>Recolhe ICMS?</b></label>
                <input name='recolhe-icms' id='recolhe-icms' type='checkbox'
                       data-toggle='toggle' data-on='Sim' data-off='Não' 
                       data-onstyle='success' data-offstyle='danger' 
                       data-size='small' class='bt-toggle recolhe-retencao' 
                       value='1'>
            </div>
            <div class='col-xs-4'>
                <label for='icms'><b>ICMS:</b></label>
                <div class='input-group'>
                  <input type='text' class='form-control valor' disabled name='icms' id='icms' value='0,00' />
                  <span class='input-group-addon'>%</span>
                </div>
            </div>
          </div>
          <div class='row'>
            <div class='col-xs-2'>
                <label for='recolhe-pis'><b>Recolhe PIS?</b></label>
                <input name='recolhe-pis' id='recolhe-pis' type='checkbox'
                       data-toggle='toggle' data-on='Sim' data-off='Não' 
                       data-onstyle='success' data-offstyle='danger' 
                       data-size='small' class='bt-toggle recolhe-retencao' 
                       value='1'>
            </div>
            <div class='col-xs-4'>
                <label for='pis'><b>PIS:</b></label>
                <div class='input-group'>
                  <input type='text' class='form-control valor' disabled name='pis' id='pis' value='0,00' />
                  <span class='input-group-addon'>%</span>
                </div>
            </div>
            <div class='col-xs-2'>
                <label for='recolhe-cofins'><b>Recolhe COFINS?</b></label>
                <input name='recolhe-cofins' id='recolhe-cofins' type='checkbox'
                       data-toggle='toggle' data-on='Sim' data-off='Não' 
                       data-onstyle='success' data-offstyle='danger' 
                       data-size='small' class='bt-toggle recolhe-retencao' 
                       value='1'>
            </div>
            <div class='col-xs-4'>
                <label for='cofins'><b>COFINS:</b></label>
                <div class='input-group'>
                  <input type='text' class='form-control valor' disabled name='cofins' id='cofins' value='0,00' />
                  <span class='input-group-addon'>%</span>
                </div>
            </div>
          </div>
          <div class='row'>
            <div class='col-xs-2'>
                <label for='recolhe-csll'><b>Recolhe CSLL?</b></label>
                <input name='recolhe-csll' id='recolhe-csll' type='checkbox'
                       data-toggle='toggle' data-on='Sim' data-off='Não' 
                       data-onstyle='success' data-offstyle='danger' 
                       data-size='small' class='bt-toggle recolhe-retencao' 
                       value='1'>
            </div>
            <div class='col-xs-4'>
                <label for='csll'><b>CSLL:</b></label>
                <div class='input-group'>
                  <input type='text' class='form-control valor' disabled name='csll' id='csll' value='0,00' />
                  <span class='input-group-addon'>%</span>
                </div>
            </div>
            <div class='col-xs-2'>
                <label for='recolhe-inss'><b>Recolhe INSS?</b></label>
                <input name='recolhe-inss' id='recolhe-inss' type='checkbox'
                       data-toggle='toggle' data-on='Sim' data-off='Não' 
                       data-onstyle='success' data-offstyle='danger' 
                       data-size='small' class='bt-toggle recolhe-retencao' 
                       value='1'>
            </div>
            <div class='col-xs-4'>
                <label for='inss'><b>INSS:</b></label>
                <div class='input-group'>
                  <input type='text' class='form-control valor' disabled name='inss' id='inss' value='0,00' />
                  <span class='input-group-addon'>%</span>
                </div>
            </div>
          </div>";
    echo "</fieldset><br>";

    echo "<fieldset> <legend>Dados Banc&aacute;rios</legend>";
    echo "
    <div class='row'>
        <div class='col-xs-12'>
         <label for='banco'><b>Banco:</b></label>
         </div>
         <div class='col-xs-4'>
         
         ".listar_bancos_chosen();
    echo "
        </div>
        <div class='col-xs-8'>
            <button id='add-conta' class='btn btn-primary'>+</button>
        </div>
      </div>";

    echo "
    <br>
    <div class='row'>
        <div class='col-xs-12'>    
            <table id='tabela_banco' width='100%' class='table'>
                <thead style='background-color:#ccc;'>
                    <tr>
                        <th></th>
                        <th>Banco</th>
                        <th>Agência</th>
                        <th>Tipo</th>
                        <th>Conta</th>
                        <th>Dígito Conta</th>
                    </tr>    
                </thead>
            </table>
        </div>
    </div>
</fieldset><br>";
$tipoChavePix = Fornecedor::getTipoChavePix();
echo "<fieldset> <legend>Chave Pix</legend>";
    echo "
        <div class='row'>
            <div class='col-xs-6'>        
                <label for='tipo-chave-pix'>
                    <b>Tipo da chave:</b>
                </label>
                    <select name='tipo_chave_pix' id='tipo-chave-pix' class=' form-control'>
                    <option value = ''></option>";
                    foreach($tipoChavePix as $tipo){
                        echo "<option value='{$tipo['id']}'>{$tipo['tipo']}</option>";                 }
                       
                    echo "</select>
            </div>
            <div class='col-xs-6'> 
                <label for='csll'><b>Chave Pix:</b></label>
                <input type='text' class='form-control chave-pix' disabled name='chave_pix' id='chave-pix' value='' />
                    
            </div>       
         
        </div>
        </fieldset><br>";
    echo "<div class='row text-center'>
            <div id='salvar-fornecedor' class='btn btn-success'>
                Salvar
            </div>
            <div id='voltar_' class='btn btn-default'>
                 Voltar
             </div>
        </div>";
    echo "</div>";

}

function listar_ipcc()
{
    $row = [];
    $sql = "SELECT * FROM plano_contas_contabil";
    $result = mysql_query($sql);
    while($fetch = mysql_fetch_array($result)){
        switch($fetch['N1']){
            case 'A':
                $nivel1 = "1 - ATIVO";
                break;
            case 'P':
                $nivel1 = "2 - PASSIVO";
                break;
            case 'R':
                $nivel1 = "3 - RESULTADOS";
                break;
            case 'D':
                $nivel1 = "9 - APURAÇÃO DO RESULTADO DO EXERCICIO";
                break;
        }
        $id 		=	$fetch['ID'];
        $nivel2 = strtoupper(sprintf('%s - %s', $fetch['COD_N2'], $fetch['N2']));
        $nivel3 = strtoupper(sprintf('%s - %s', $fetch['COD_N3'], $fetch['N3']));
        $nivel4 = strtoupper(sprintf('%s - %s', $fetch['COD_N4'], $fetch['N4']));
        $nivel5 = strtoupper(sprintf('%s - %s', $fetch['COD_N5'], $fetch['N5']));

        $row[$nivel1][$nivel2][$nivel3][$nivel4][$id] = $nivel5;
    }
    return $row;
}

function listar_bancos($id){
    $result = mysql_query("SELECT * FROM contas ORDER BY NOME");
    $var = "<select name='banco' style='background-color:transparent;'>";
    while($row = mysql_fetch_array($result))
    {
        if(($id <> NULL) && ($id == $row['ID']))
            $var .= "<option value='". $row['ID'] ."' selected>". $row['NOME'] ."</option>";
        else
            $var .= "<option value='". $row['ID'] ."'>". $row['NOME'] ."</option>";
    }
    $var .= "</select>";
    return $var;
}
function listar_bancos_chosen($id){
    $result = mysql_query("SELECT * FROM origemfundos ORDER BY forma");
    $var = "<select name='banco' class='form-control ' data-placeholder='Selecione um banco...' id='banco-chosen' style='background-color:transparent;'>";
    while($row = mysql_fetch_array($result))
    {
        if(($id <> NULL) && ($id == $row['id']))
            $var .= "<option value='". $row['id'] ."' selected>". mb_strtoupper($row['COD_BANCO']." - ".$row['forma'],'UTF8') ."</option>";
        else
            $var .= "<option value='". $row['id'] ."'>".mb_strtoupper($row['COD_BANCO']." - ".$row['forma'],'UTF8') ."</option>";
    }
    $var .= "</select>";
    return $var;
}

function bancos_fornecedores($id = null){
    $result = mysql_query("SELECT * FROM banco_fornecedor ORDER BY DESCRICAO");
    $var = "<select name='banco' class='form-control' id='banco'>";
    while($row = mysql_fetch_array($result))
    {
        if(($id <> NULL) && ($id == $row['ID']))
            $var .= "<option value='". $row['ID'] ."' selected>". $row['DESCRICAO'] ."</option>";
        else
            $var .= "<option value='". $row['ID'] ."'>". $row['DESCRICAO'] ."</option>";
    }
    $var .= "</select>";
    return $var;
}

function edit_fornecedor() {
    if (isset($_GET['idFornecedores']) && isset($_GET['visualizar']) ) {
        $idFornecedores = (int) $_GET['idFornecedores'];
        $v = (int) $_GET['visualizar'];
        if (isset($_POST['submitted'])) {
            foreach($_POST AS $key => $value) { $_POST[$key] = mysql_real_escape_string($value); }
            $sql = "UPDATE `fornecedores` SET `nome` =  '{$_POST['nome']}', `razaoSocial` =  '{$_POST['razaoSocial']}' ,  `cnpj` =  '{$_POST['cnpj']}' ,  `contato` =
'{$_POST['contato']}' ,  `telefone` =  '{$_POST['telefone']}' ,  `email` =  '{$_POST['email']}' ,  `endereco` =  '{$_POST['endereco']}' `ipcc`
=  {$_POST['ipcc']},   WHERE
`idFornecedores` = '$idFornecedores' ";
            mysql_query($sql) or die(mysql_error());
            //echo (mysql_affected_rows()) ? "Edited row.<br />" : "Nothing changed. <br />";
            //echo "<a href='list_fornecedores.php'></a>";
            header('Location:?op=fornecedor&act=listar');
        }
        $row = mysql_fetch_array ( mysql_query("SELECT f.*, concat(cd.NOME,',',cd.UF) as cidade, cd.ID as id_cidade FROM `fornecedores` as f left join cidades as cd on (f.CIDADE_ID = cd.ID ) WHERE `idFornecedores` = '$idFornecedores' "));

        $nome = mb_strtoupper($row['nome'],'UTF8');

        echo "<link rel='stylesheet' type='text/css' href='/utils/reports.css' />
    <link rel='stylesheet' href='/utils/bootstrap-3.3.6-dist/css/bootstrap.min.css' />
    <link rel='stylesheet' href='/utils/font-awesome-4.7.0/css/font-awesome.css' />
    <link rel='stylesheet' href='/utils/jquery/css/toggle/toggle.css' />
    <link rel='stylesheet' href='/utils/bootstrap-3.3.6-dist/css/chosen.bootstrap.min.css' />
    <link rel='stylesheet' href='/utils/jquery/css/selectize/selectize.bootstrap3.css' />
    <script src='/utils/bootstrap-3.3.6-dist/js/bootstrap.min.js' ></script>
    <script type='text/javascript' src='/utils/jquery/js/bootstrap-toggle.min.js'></script>
    <script src='/utils/jquery/js/selectize/selectize.js' ></script>";
    

        if($nome == "")
            $nome = substr(mb_strtoupper($row['razaoSocial'], 'UTF8'),0,15);
        if($v == 0){

            $editar ='';
            $enab ="";
            $label = 'Editar';
        }
        if($v == 1){

            $editar = "readonly='readonly'";
            $enab ="disabled";
            $label = 'Visualizar';
        }
        //echo "<form action='' method='POST'>";

        echo alerta();
        echo " <div class='row text-center'>
                    <h1 style='font: bold 2.1em Arial, Arial, Sans-Serif;color: #8B0000;'>
                        {$label} Fornecedor/Cliente
                    </h1>
                </div>";

        $class_cnpj="";
        $class_cpf="";
        $disabled_cpf="";
        $disabled_cnpj='';
        $ipcc = $row['IPCC'];
        $tipoChavePixFornecedor = $row['tipo_chave_pix_id'];
        $chavePixFornecedor = $row['chave_pix'];

        echo "<div id='div-salvar-fornecedor' class='form-group'>";
        echo "<fieldset>";
        echo "<legend><b>{$label}</b> </legend>";
        echo "
            <div class='form-group row'>
                <div class='col-xs-6'>
                    <label for='tipo-cadastro'>
                    <b>Tipo</b>
                    </label>
                    <select name='tipo' {$enab} id='tipo-cadastro' class='tipo-cadastro OBG form-control'>
                        <option value = ''></option>
                        <option value='F' ".(($row['TIPO']=='F')?"selected":"").">Fornecedor</option>
                        <option value='C' ".(($row['TIPO']=='C')?"selected":"").">Cliente</option>
                    </select>
                </div>              
                <div class='col-xs-6'>
                    <label for='ipcc'>
                        Conta Contábil
                    </label>
                    <select class='form-control ' $enab data-placeholder='Selecione uma conta contábil...' required name='ipcc' id='ipcc'>
                        <option value='' selected>NENHUMA</option>";
        $dados = listar_ipcc();
        foreach($dados as $nivel1 => $nivel2){
            echo "	<optgroup label='{$nivel1}'>";
            foreach($nivel2 as $n2 => $nivel3){
                echo "	<optgroup label='{$n2}'>";
                foreach($nivel3 as $n3 => $nivel4){
                    echo "	<optgroup label='{$n3}'>";
                    foreach($nivel4 as $n4 => $nivel5){
                        echo "	<optgroup label='{$n4}'>";
                        foreach($nivel5 as $id => $n5){
                            echo "	<option ".($id == $ipcc ? "selected" : "")."  value='{$id}'>{$n5}</option>";
                        }
                        echo "	</optgroup>";
                    }
                    echo "	</optgroup>";
                }
                echo "	</optgroup>";
            }
            echo "	</optgroup>";
        }
        echo "</select>
            </div>  
                <div class='col-xs-12'>
                    <label for='nome-fantasia'>
                        <b>Nome Fantasia/Apelido</b>
                    </label>
                    <input class='OBG form-control'  $editar id='nome-fantasia' type='text' name='nome' value='$nome' />
                    
                </div>
                <div class='col-xs-12' for='razaoSocial'>
                    <label>
                        <b>Raz&atilde;o Social/Nome</b> 
                    </label>
                    <input class='OBG form-control' $editar id='razaoSocial' value='". mb_strtoupper($row['razaoSocial'],'UTF8') ."' type='text' name='razaoSocial' />
                    
                </div>";

        if($row['FISICA_JURIDICA']=='1'){
            $class_cnpj="OBG";
            $disabled_cpf="disabled='disabled'";

        }else{
            $class_cpf="OBG";
            $disabled_cnpj="disabled='disabled'";

        }

        $id_cpf= $v == 0?"id='cpf'":"";
        $id_cnpj= $v == 0?"id='cnpj'":"";

        $classeValorFornecedor = Fornecedor::getClasseValor($row['idFornecedores']);
        echo"  <div class='col-xs-6'>                    
                    <input class='cpf_cnpj' id='radio-cnpj' $enab type='radio' name='cpf_cnpj' 
                    value='1' ".(($row['FISICA_JURIDICA']=='1')?"checked":"")." />
                    <label for='radio-cnpj'>
                    <b>CNPJ </b>
                    </label>
                    <input class='$class_cnpj form-control' type='text' $editar $disabled_cnpj name='cnpj'
                    value='". $row['cnpj']."'  id='cnpj' size='30' />
                </div>
                <div class='col-xs-6'>                    
                    <input class='cpf_cnpj' id='radio-cpf' type='radio' $enab name='cpf_cnpj' 
                    value='0' ".(($row['FISICA_JURIDICA']=='0')?"checked":"")."  />
                    <label for='radio-cpf'>
                    <b>CPF </b>&nbsp;
                    </label>
                    <input class='form-control $class_cpf' $editar type='text' value='". $row['CPF'] ."'
                    name='cpf'  $disabled_cpf id='cpf' size='30' />
                    <span id='cpf_invalido' class='text-red'></span>
                </div>  
                <div class='col-xs-6'> 
                    <label for='ies'>
                        <b>Inscrição Estadual/RG</b>
                    </label> 
                        <input class='OBG form-control' $editar type='text' id='ies'
                         value='". mb_strtoupper($row['IES'],'UTF8') ."' name='ies' size='30'/>                    
                </div>  
                <div class='col-xs-6'>
                    <label for='im'>
                        <b>Inscrição Municipal</b>
                    </label>
                        <input  type='text' name='im'  $editar id='im' size='20' 
                        value='". mb_strtoupper($row['IM'],'UTF8') ."' class ='form-control' />
                    
                </div> 
                <div class='col-xs-6'>
                    <label for='contato'>
                    <b>Contato</b>
                    </label>
                    <input class='OBG form-control' $editar id='contato' type='text'
                     value='". mb_strtoupper($row['contato'],'UTF8') ."' name='contato' size='60'/>                    
                </div>                 
                <div class='col-xs-3'>
                    <label><b>Telefone Fixo / FAX</b></label>
                        <input id='tel' class=' form-control telfone' maxlength='14' $editar type='text' name='telefone'
                        value='". mb_strtoupper($row['telefone'],'UTF8') ."' size='26'/>
                </div>
                <div class='col-xs-3'>
                    <label><b>Celular</b></label>
                        <input id='cel' class='form-control celular' maxlength='15' $editar type='text' name='celular'
                        value='". mb_strtoupper($row['celular'],'UTF8') ."' size='26'/>
                </div>  
                <div class='col-xs-6'>
                    <label for='email'>
                    <b>Email</b>
                    </label>
                    <input type='text' class='form-control' name='email' $editar
                    value='". mb_strtoupper($row['email'],'UTF8') ."' id='email' size='50'/>
                 </div>                                    
                <div class='col-xs-6'>
                    <label for='cidade-und'>
                        <b>Cidade</b>
                    </label>
                        <input type='text' class='OBG form-control' $editar id='cidade-und'
                        value='". mb_strtoupper($row['cidade'],'UTF8') ."' name='cidade_und' size=35 id='cidade_und'/>
                        <input type='hidden' name='id_cidade_und' id='id_cidade_und' value='". $row['id_cidade']."' />
                    
                </div>  
                <div class='col-xs-6'>
                <label for='bairro'>
                    <b>Bairro</b>
                </label>
                <input class='OBG form-control' $editar type='text' size='92' id='bairro'
                value='". mb_strtoupper($row['bairro'],'UTF8') ."' name='bairro'/>                    
            </div>
                <div class='col-xs-6'>
                <label for='endereco'>
                    <b>Endere&ccedil;o</b>
                </label>
                <input class='OBG form-control' $editar type='text' size='92' id='endereco'
                value='". mb_strtoupper($row['endereco'],'UTF8') ."' name='endereco'/>                    
            </div> <div class='col-xs-12'>
                <label for='classe-valor'>
                    <b>Grupo de Fornecedores</b>
                </label>
                <select 
                    id='classe-valor' 
                    name='classe_valor'
                    required 
                    class='form-control classe-valor-selectize' 
                    {$enab}                    
                    placeholder='Selecione uma classe de valor...' 
                    tabindex='-1' 
                    style='display: none; text-transform: uppercase'>";
        $classesValor = ClasseValor::getAll();
        echo "<option  value=''></option>";  
        foreach ($classesValor as $classeValor) {
           if(empty($classeValorFornecedor)){
                echo "<option selected value=''></option>";
           }
        echo "<option ". ($classeValorFornecedor == $classeValor['id'] ? 'selected' : '') ."  
                    value='{$classeValor['id']}'>{$classeValor['descricao']}</option>";
        }
        echo "      </select>                   
                </div>  
                <div class='col-xs-6'>
                <label for='tipo-cadastro'>
                <b>Precisa classificar?</b>
                </label>
                <select {$enab}  name='precisa_classificar' id='fornecedor-precisa-classificar' class=' OBG form-control'>
                    <option ".($row['precisa_classificar'] == '' ? 'selected' : '')." value = ''></option>
                    <option ".( $row['precisa_classificar'] == 'N' ? 'selected' : '')." value='N'>Não</option>
                    <option ".( $row['precisa_classificar'] == 'S' ? 'selected' : '')." value='S'>Sim</option>
                </select>
            </div>   
            <div class='col-xs-6' id='div-fornecedor-classificacao' ".( $row['precisa_classificar'] == 'S' ? '' : 'hidden')." >
            <label for='classificacao'>
            <b>Classificação</b>
            </label>
            <select {$enab}  name='classificacao' id='fornecedor-classificacao' class=' form-control'>
                <option ".( $row['classificacao'] == '' ? 'selected' : '')." value = ''></option>
                <option".( $row['classificacao'] == 'A' ? 'selected' : '')."  value='A'>A</option>
                <option ".( $row['classificacao'] == 'B' ? 'selected' : '')." value='B'>B</option>
                <option ".( $row['classificacao'] == 'C' ? 'selected' : '')." value='C'>C</option>
                <option ".( $row['classificacao'] == 'D' ? 'selected' : '')." value='D'>D</option>
                
            </select>
        </div>   
        <div class='col-xs-6'>
                <label for='tipo-cadastro'>
                <b>Plano de saude?</b>
                </label>
                <select {$enab}  name='plano_saude' id='fornecedor-plano-saude' class=' OBG form-control'>
                <option ".($row['plano_saude'] == '' ? 'selected' : '')." value = ''></option>
                <option ".( $row['plano_saude'] == 'N' ? 'selected' : '')." value='N'>Não</option>
                <option ".( $row['plano_saude'] == 'S' ? 'selected' : '')." value='S'>Sim</option>
                </select>
            </div> 
            <div class='col-xs-12'>
                <label for='observacao'>
                <b>Observação</b>
                </label>
                <input name='observacao'  id='observacao' $editar class='form-control' value='{$row['observacao']}'>                                   
                </input>
            </div>                 
              </div>";

        if($row['FISICA_JURIDICA']=='1'){
            $class_cnpj="class='OBG'";
            $disabled_cpf="disabled='disabled'";

        }else{
            $class_cpf="class='OBG'";
            $disabled_cnpj="disabled='disabled'";
        }

        $id_cpf= $v == 0?"id='cpf'":"";
        $id_cnpj= $v == 0?"id='cnpj'":"";

        $recolhe_iss = $row['recolhe_iss'] == 1 ? 'checked' : '';
        $iss = number_format($row['aliquota_iss'], 2, ',', '.');
        $recolhe_ir = $row['recolhe_ir'] == 1 ? 'checked' : '';
        $ir = number_format($row['aliquota_ir'], 2, ',', '.');
        $recolhe_ipi = $row['recolhe_ipi'] == 1 ? 'checked' : '';
        $ipi = number_format($row['aliquota_ipi'], 2, ',', '.');
        $recolhe_icms = $row['recolhe_icms'] == 1 ? 'checked' : '';
        $icms = number_format($row['aliquota_icms'], 2, ',', '.');
        $recolhe_pis = $row['recolhe_pis'] == 1 ? 'checked' : '';
        $pis = number_format($row['aliquota_pis'], 2, ',', '.');
        $recolhe_cofins = $row['recolhe_cofins'] == 1 ? 'checked' : '';
        $cofins = number_format($row['aliquota_cofins'], 2, ',', '.');
        $recolhe_csll = $row['recolhe_csll'] == 1 ? 'checked' : '';
        $csll = number_format($row['aliquota_csll'], 2, ',', '.');
        $recolhe_inss = $row['recolhe_inss'] == 1 ? 'checked' : '';
        $inss = number_format($row['aliquota_inss'], 2, ',', '.');

        echo "<fieldset>
            <legend>
                Alíquotas <br>
                <small style='font-size: small'>
                    <b>
                    * Preencher somente se o fornecedor/cliente possuir alíquotas diferentes das já discriminadas  
                    nas naturezas do plano de contas financeiro.
                    </b>
                </small>
            </legend>";
        echo "<div class='row'>
            <div class='col-xs-2'>
                <label for='recolhe-iss'><b>Recolhe ISS?</b></label>
                <input name='recolhe-iss' id='recolhe-iss' type='checkbox'
                       data-toggle='toggle' data-on='Sim' data-off='Não' 
                       data-onstyle='success' data-offstyle='danger' 
                       data-size='small' {$enab} {$recolhe_iss} class='bt-toggle recolhe-retencao' 
                       value='1'>
            </div>
            <div class='col-xs-4'>
                <label for='iss'><b>ISS:</b></label>
                <div class='input-group'>
                  <input type='text' class='form-control valor' " . ($row['recolhe_iss'] == '0' ? 'disabled' : '') . "  name='iss' id='iss' value='{$iss}' />
                  <span class='input-group-addon'>%</span>
                </div>
            </div>
            <div class='col-xs-2'>
                <label for='recolhe-ir'><b>Recolhe IR?</b></label>
                <input name='recolhe-ir' id='recolhe-ir' type='checkbox'
                       data-toggle='toggle' data-on='Sim' data-off='Não' 
                       data-onstyle='success' data-offstyle='danger' 
                       data-size='small' {$enab} {$recolhe_ir} class='bt-toggle recolhe-retencao' 
                       value='1'>
            </div>
            <div class='col-xs-4'>
                <label for='ir'><b>IR:</b></label>
                <div class='input-group'>
                  <input type='text' class='form-control valor' " . ($row['recolhe_ir'] == '0' ? 'disabled' : '') . " name='ir' id='ir' value='{$ir}' />
                  <span class='input-group-addon'>%</span>
                </div>
            </div>
          </div>
          <div class='row'>
            <div class='col-xs-2'>
                <label for='recolhe-ipi'><b>Recolhe IPI?</b></label>
                <input name='recolhe-ipi' id='recolhe-ipi' type='checkbox'
                       data-toggle='toggle' data-on='Sim' data-off='Não' 
                       data-onstyle='success' data-offstyle='danger' 
                       data-size='small' {$enab} {$recolhe_ipi} class='bt-toggle recolhe-retencao' 
                       value='1'>
            </div>
            <div class='col-xs-4'>
                <label for='ipi'><b>IPI:</b></label>
                <div class='input-group'>
                  <input type='text' class='form-control valor' " . ($row['recolhe_ipi'] == '0' ? 'disabled' : '') . " name='ipi' id='ipi' value='{$ipi}' />
                  <span class='input-group-addon'>%</span>
                </div>
            </div>
            <div class='col-xs-2'>
                <label for='recolhe-icms'><b>Recolhe ICMS?</b></label>
                <input name='recolhe-icms' id='recolhe-icms' type='checkbox'
                       data-toggle='toggle' data-on='Sim' data-off='Não' 
                       data-onstyle='success' data-offstyle='danger' 
                       data-size='small' {$enab} {$recolhe_icms} class='bt-toggle recolhe-retencao' 
                       value='1'>
            </div>
            <div class='col-xs-4'>
                <label for='icms'><b>ICMS:</b></label>
                <div class='input-group'>
                  <input type='text' class='form-control valor' " . ($row['recolhe_icms'] == '0' ? 'disabled' : '') . " name='icms' id='icms' value='{$icms}' />
                  <span class='input-group-addon'>%</span>
                </div>
            </div>
          </div>
          <div class='row'>
            <div class='col-xs-2'>
                <label for='recolhe-pis'><b>Recolhe PIS?</b></label>
                <input name='recolhe-pis' id='recolhe-pis' type='checkbox'
                       data-toggle='toggle' data-on='Sim' data-off='Não' 
                       data-onstyle='success' data-offstyle='danger' 
                       data-size='small' {$enab} {$recolhe_pis} class='bt-toggle recolhe-retencao' 
                       value='1'>
            </div>
            <div class='col-xs-4'>
                <label for='pis'><b>PIS:</b></label>
                <div class='input-group'>
                  <input type='text' class='form-control valor' " . ($row['recolhe_pis'] == '0' ? 'disabled' : '') . " name='pis' id='pis' value='{$pis}' />
                  <span class='input-group-addon'>%</span>
                </div>
            </div>
            <div class='col-xs-2'>
                <label for='recolhe-cofins'><b>Recolhe COFINS?</b></label>
                <input name='recolhe-cofins' id='recolhe-cofins' type='checkbox'
                       data-toggle='toggle' data-on='Sim' data-off='Não' 
                       data-onstyle='success' data-offstyle='danger' 
                       data-size='small' {$enab} {$recolhe_cofins} class='bt-toggle recolhe-retencao' 
                       value='1'>
            </div>
            <div class='col-xs-4'>
                <label for='cofins'><b>COFINS:</b></label>
                <div class='input-group'>
                  <input type='text' class='form-control valor' " . ($row['recolhe_cofins'] == '0' ? 'disabled' : '') . " name='cofins' id='cofins' value='{$cofins}' />
                  <span class='input-group-addon'>%</span>
                </div>
            </div>
          </div>
          <div class='row'>
            <div class='col-xs-2'>
                <label for='recolhe-csll'><b>Recolhe CSLL?</b></label>
                <input name='recolhe-csll' id='recolhe-csll' type='checkbox'
                       data-toggle='toggle' data-on='Sim' data-off='Não' 
                       data-onstyle='success' data-offstyle='danger' 
                       data-size='small' {$enab} {$recolhe_csll} class='bt-toggle recolhe-retencao' 
                       value='1'>
            </div>
            <div class='col-xs-4'>
                <label for='csll'><b>CSLL:</b></label>
                <div class='input-group'>
                  <input type='text' class='form-control valor' " . ($row['recolhe_csll'] == '0' ? 'disabled' : '') . " name='csll' id='csll' value='{$csll}' />
                  <span class='input-group-addon'>%</span>
                </div>
            </div>
            <div class='col-xs-2'>
                <label for='recolhe-inss'><b>Recolhe INSS?</b></label>
                <input name='recolhe-inss' id='recolhe-inss' type='checkbox'
                       data-toggle='toggle' data-on='Sim' data-off='Não' 
                       data-onstyle='success' data-offstyle='danger' 
                       data-size='small' {$enab} {$recolhe_inss} class='bt-toggle recolhe-retencao' 
                       value='1'>
            </div>
            <div class='col-xs-4'>
                <label for='inss'><b>INSS:</b></label>
                <div class='input-group'>
                  <input type='text' class='form-control valor' " . ($row['recolhe_inss'] == '0' ? 'disabled' : '') . " name='inss' id='inss' value='{$inss}' />
                  <span class='input-group-addon'>%</span>
                </div>
            </div>
          </div>";
        echo "</fieldset><br>";

        echo "<fieldset> <legend>Dados Banc&aacute;rios</legend>";
        if ($v == 0) {
            echo "
            <div class='row'>
             <div class='col-xs-12'>
                 <label for='banco'><b>Banco:</b></label>
             </div>
             <div class='col-xs-4'>
             
             ".listar_bancos_chosen();
            echo "
            </div>
            <div class='col-xs-8'>
                <button id='add-conta' class='btn btn-primary'>+</button>
            </div>
          </div>";
        }

        echo "
        <br>
        <div class='col-xs-12'>    
            <table id='tabela_banco' width='100%' class='table'>
                <thead style='background-color:#ccc;'>
                    <tr>
                        <th></th>
                        <th>Banco</th>
                        <th>Agência</th>
                        <th>Tipo</th>
                        <th>Conta</th>
                        <th>Dígito Conta</th>
                    </tr>    
                </thead>";
        $sql= "SELECT cf . * ,
                 concat(COD_BANCO,' - ',forma) as DESCRICAO
					FROM contas_fornecedor AS cf
					INNER JOIN origemfundos AS c ON ( cf.origem_fundos_id = c.id )
					WHERE cf.FORNECEDOR_ID = '$idFornecedores' ";
        $result =mysql_query($sql);
        while($row = mysql_fetch_array($result)){
            if($v == 0){
                $x= "<img src='../utils/delete_16x16.png' class='del-conta-fornecedor' title='Excluir' border='0' >";
            }

            $dadosConta = explode('-', $row['N_CONTA']);
        

            echo "<tr class='dados_contas'>
            <td  class='' $editar cod='{$row['origem_fundos_id']}' > {$x}</td>
            <td>".mb_strtoupper($row['DESCRICAO'],'UTF8')."</td>
            <td><input type='text' name='ag' $editar class='OBG form-control' maxlength='5' value='". strtoupper($row['AG']) ."' size='8'/></td>
            <td><select name='tipo_conta' $editar {$enab} class='form-control OBG' >
                <option value=''></option>
                <option ".( $row['TIPO'] == 'Corrente' ? 'selected' : '')." value='Corrente'>Corrente</option>
                <option ".( $row['TIPO'] == 'Poupança' ? 'selected' : '')."  value='Poupança'>Poupança</option>
                </select>
            </td>
            <td><input type='text' name='conta' class='OBG form-control' maxlength='12' size='38' $editar  value='". strtoupper($dadosConta[0]) ."' /></td>
            <td><input type='text' name='digito_conta' class=' form-control OBG' maxlength='1' $editar value='".$dadosConta[1] ."'size='2'/></td></tr>";
        }
        echo "    </table>
        </div>
    
    </fieldset><br>";
    $tipoChavePix = Fornecedor::getTipoChavePix();
    if(empty($tipoChavePixFornecedor)){       
        $disabledPix = 'disabled';
    }else{
        $classChavePix = Fornecedor::getTipoChavePix($tipoChavePixFornecedor);   
        $classChavePix = $classChavePix[0]['tipo'];   

    }
    
echo "<fieldset> <legend>Chave Pix</legend>";
    echo "
        <div class='row'>
            <div class='col-xs-6'>        
                <label for='tipo-chave-pix'>
                    <b>Tipo da chave:</b>
                </label>
                    <select name='tipo_chave_pix' id='tipo-chave-pix' $enab class=' form-control'>
                    <option value = ''></option>";
                    foreach($tipoChavePix as $tipo){
                       
                        echo "<option ".( $tipo['id'] == $tipoChavePixFornecedor ? "selected" : "") ." value='{$tipo['id']}'>{$tipo['tipo']}</option>";                 }
                       
                    echo "</select>
            </div>
            <div class='col-xs-6'> 
                <label for='csll'><b>Chave Pix:</b></label>
                <input type='text' $editar class='form-control chave-pix ".strtolower($classChavePix)."' {$disabledPix} name='chave_pix' id='chave-pix' value='{$chavePixFornecedor}' />
                    
            </div>       
         
        </div>
        </fieldset><br>";
        echo "<div class='row text-center'>";
        echo"<input idfornecedor='$idFornecedores' id='idfor' type='hidden'>";
        if($v == 0){
            echo "<div id='editar-fornecedor' class='btn btn-success'>
                    Atualizar
                </div> &nbsp;";
        }
        echo "<div id='voltar_' class='btn btn-default'>
                     Voltar
                 </div>
            </div>";


        echo "</div>";
        //echo "</form>";

    }

}

function del_fornecedor() {
    $idFornecedores = (int) $_GET['idFornecedores'];
    mysql_query("UPDATE `fornecedores` SET ATIVO = 'N' WHERE `idFornecedores` = '$idFornecedores' ") ;
    echo "Fornecedor desativado com sucesso. <a href='?op=fornecedor&act=listar'>(voltar)<br /> ";
}


?>
