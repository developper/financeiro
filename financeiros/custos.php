<?php

$id = session_id();
if(empty($id))
  session_start();

include('../db/config.php');
include('../vendor/autoload.php');

use App\Models\Financeiro\Financeiro;

$periodo = new \stdClass;

$inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
$fim    = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
$ur     = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_NUMBER_INT);
$tipo   = filter_input(INPUT_POST, 'tipo', FILTER_SANITIZE_STRING);

$periodo->inicio  = isset($inicio) && !empty($inicio) ? \DateTime::createFromFormat('d/m/Y', $inicio) : false;
$periodo->fim     = isset($fim) && !empty($fim) ? \DateTime::createFromFormat('d/m/Y', $fim) : false;
$periodo->ur      = isset($ur) && !empty($ur) ? $ur : false;

if($periodo->inicio === false || $periodo->fim === false || $periodo->ur === false){
  header("Location: ".$_SERVER['HTTP_REFERER']."&msg=".base64_encode("Favor preencher todos os campos corretamente."));
}

$financeiro = new Financeiro;
$data = $financeiro->getCustos($periodo);

if(!isset($tipo) && empty($tipo)){
  $filename = sprintf("relatorio_custos_%s_%s", $periodo->inicio->format('d-m-Y'), $periodo->fim->format('d-m-Y'));

  header("Content-type: application/x-msexce; charset=ISO-8859-1");
  header("Content-Disposition: attachment; filename={$filename}.xls");
  header("Pragma: no-cache");
}
?>

<table class='mytable font10' width='100%'>
  <thead>
    <tr>
      <td colspan="7" align="center" style="font-size: 18pt;"><strong>Relat&oacute;rio de Custos por UR</strong></td>
    </tr>
    <tr>
      <td colspan="7" align="center" style="font-size: 10pt;"><?= sprintf('Per&iacute;odo: De %s a %s', $periodo->inicio->format('d/m/Y'),
          $periodo->fim->format('d/m/Y')) ?></td>
    </tr>
    <tr>
      <th align="left">UR</th>
      <th align="center">DATA</th>
      <th align="center">TISS</th>
      <th align="center">ITEM</th>
      <th align="center">QUANTIDADE</th>
      <th align="center">VALOR </th>
      <th align="center">SUBTOTAL </th>
    </tr>
  </thead>
  <?php
    if(count($data) > 0):
      $cont = 1;
      $cor= $cont%2 ==0? "bgcolor='#DCDCDC'":'';
      $total = 0;
      $totaisUr = [];
      foreach($data as $row):
        $subtotal = 0;
        $subtotal = $row['qtd']*round($row['valor'],2);
        $total += $subtotal;
        $totaisUr[$row['ur']]['participacao'] = $row['percentual'];
        $totaisUr[$row['ur']]['totalPorUr'] +=($subtotal*$row['percentual'])/100 ;

  ?>
  <tr $cor valign="center">
    <td><?= htmlentities($row['ur']) ?></td>
    <td align="center"><?= $row['dataSaida'] ?></td>
    <td align="center"><?= $row['tiss'] ?></td>
    <td><?= htmlentities($row['item']) ?></td>
    <td align="center"><?= $row['qtd'] ?></td>
    <td align="center"><?= 'R$ '.$financeiro->money($row['valor']) ?></td>
    <td align="center"><?= 'R$ '.$financeiro->money($subtotal) ?></td>
  </tr>
  <?php
        $cont++;
      endforeach;
  ?>
  <tr>
    <td align="right" colspan="6"><strong>TOTAL: </strong></td>
    <td align="center"><strong><?= 'R$ '.$financeiro->money($total) ?></strong></td>
  </tr>
  <?php
    endif;
    if(count($data) === 0):
  ?>
  <tr>
    <td colspan="7">Nenhum resultado foi encontrado!</td>
  </tr>
  <?php endif; ?>
  <tr>
    <td colspan="7"> </td>
  </tr>
</table>

<?php
  if(count($totaisUr) > 0):
    $totalCustos = 0;
?>
  <table class='mytable font10' width='100%'>
    <thead>
      <tr>
        <td align="center"><strong>UR</strong></td>
        <td align="center"><strong>PARTICIPA&Ccedil;&Atilde;O</strong></td>
        <td align="center"><strong>VALOR A SER COBRADO</strong></td>
      </tr>
    </thead>
    <tbody>
    <?php
      foreach($totaisUr as $ur => $row):
        $totalCustos += round($row['totalPorUr'],2);
    ?>
      <tr>
        <td align="center"><?= htmlentities($ur) ?></td>
        <td align="center"><?= $row['participacao'].'%' ?></td>
        <td align="center"><?= 'R$ '.$financeiro->money($row['totalPorUr']) ?></td>
      </tr>
    <?php
      endforeach;
    ?>
      <tr>
        <td colspan="2" align="right"><strong>Total: </strong></td>
        <td align="center"><strong><?= 'R$ '.$financeiro->money($totalCustos) ?></strong></td>
      </tr>
    </tbody>
  </table>
<?php endif; ?>