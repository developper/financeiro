<?php
$id = session_id();
if(empty($id))
	session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');

$extension = '.xls';

if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
    $extension = '.xlsx';
}

header("Content-type: application/x-msexce");
header("Content-type: application/force-download;");
header("Content-Disposition: attachment; filename=relatorio".$extension);
header("Pragma: no-cache");

class imprimir_rel_analito_fluxo_caixa{

	public function detalhe_rel_analito_fluxo_caixa($inicioperiodo, $fimperiodo, $status, $empresa, $empresa_texto, $id_ipcf_n4, $id_ipcf_n4_texto){
		$inicio = implode('-',array_reverse(explode('/',$inicioperiodo)));
		$fim = implode('-',array_reverse(explode('/',$fimperiodo)));
		$textoEmpresa = $empresa == 'T' ? 'TODAS ' : $empresa_texto;
		$textoIPCFN = $id_ipcf_n4 == 'T' ? 'TODOS ' : $id_ipcf_n4_texto;
		$condEmpresa = $empresa == 'T' ? '' : " AND rateio.CENTRO_RESULTADO = {$empresa}";
		$condIPCFN4 = $id_ipcf_n4 == 'T' ? '' : " AND rateio.IPCG4={$id_ipcf_n4}";
		$condTarifas = $id_ipcf_n4 == 'T' ? '' : " AND pc.ID = {$id_ipcf_n4}";

		$html.= "<table style='width:1257px;' >";
		$html .= "<tr colspan='5'><h1> Relat&oacute;rio Anal&iacute;tico de Fluxo de Caixa  </h1></tr>";

		$html.= "<table style='width:1257px;' >";

		$html.= "<tr bgcolor='#EEEEEE'>";
		$html.= "<td  style='width:256px;'><b>PER&iacute;ODO:</b></td>";
		$html.= "<td style='width:256px;' ><b>IPCF N&iacute;vel 4 </b></td>";
		$html.= "<td style='width:152px;'><b>EMPRESA:</b></td>";
		$html.= "<td style='width:485px;'> </td>";
		$html.= "</tr>";
		$html.= "<tr>";
		$html.= "<td style='width:256px;'>De {$inicioperiodo} a {$fimperiodo}</td>";
		$html.= "<td style='width:152px;'>{$textoIPCFN}</td>";
		$html.= "<td style='width:152px;'>{$textoEmpresa}</td>";
		$html.= "<td style='width:485px;'> </td>";
		$html.= "</tr>";

		$html.= "</table>";
		$html .="<br></br>";

        /*UNION ALL
              SELECT
                'MEDERI CENTRAL' AS empresa,
                IF(modalidade_fatura != 'REMOCAO', 'ASSIST. DOMICILIAR', 'REMOÇOES E EVENTOS') AS N4,
                IF(modalidade_fatura != 'REMOCAO', '1.1.1.01', '1.1.2.01') AS COD_N4,
                DATE_FORMAT(previsao_recebimento, '%d/%m/%Y') AS dat_conciliado,
                0 AS VALOR_PAGO,
                0 AS PORCENTAGEM,
                valor_liquido AS val,
                'PREVISAO DE RECEBIMENTO DE FATURA' AS tipo_nota,
                fatura.protocolo_envio AS documento,
                planosdesaude.nome AS razaoSocial,
                '' AS descricao
              FROM
                fatura
                INNER JOIN planosdesaude ON fatura.PLANO_ID = planosdesaude.id
                INNER JOIN fatura_controle_recebimento on fatura_controle_recebimento.fatura_id = fatura.ID
              WHERE
                fatura.CANCELADO_POR IS NULL
        AND fatura.ORCAMENTO = 0
        AND fatura_controle_recebimento.previsao_recebimento BETWEEN '{$inicio}' AND '{$fim}'
        AND fatura_controle_recebimento.status = 'PENDENTE'*/

		if($status == "Processada") {
			$sql = "SELECT
            empresas.nome as empresa,
            plano_contas.N4,
            plano_contas.COD_N4,
            DATE_FORMAT(parcelas.DATA_CONCILIADO,'%d/%m/%Y') as dat_conciliado,
            parcelas.VALOR_PAGO,
            rateio.PORCENTAGEM,
            ROUND((parcelas.VALOR_PAGO *(rateio.PORCENTAGEM/100)),2) as val,
            (CASE notas.tipo WHEN 0 THEN 'RECEB.' ELSE 'PG.' END) as tipo_nota,
            notas.codigo,
            fornecedores.razaoSocial,
            notas.descricao,
            indexed_demonstrativo_contas_bancarias.conta
        FROM
            rateio
            INNER JOIN parcelas ON parcelas.idNota = rateio.NOTA_ID
            INNER JOIN notas ON parcelas.idNota = notas.idNotas
            INNER JOIN empresas ON empresas.id = rateio.CENTRO_RESULTADO
            INNER JOIN plano_contas ON rateio.IPCG4 = plano_contas.ID
            INNER JOIN fornecedores ON notas.codFornecedor = fornecedores.idFornecedores
            INNER JOIN indexed_demonstrativo_contas_bancarias ON indexed_demonstrativo_contas_bancarias.ID = parcelas.origem
        WHERE
            parcelas.`status` <> 'Cancelada' AND
            parcelas.CONCILIADO = 'S' AND
            notas.`status` <> 'Cancelada' AND
            parcelas.DATA_CONCILIADO BETWEEN '{$inicio}' AND '{$fim}'
            $condEmpresa
            $condIPCFN4
        UNION ALL
            SELECT
		emp.nome AS empresa,
		pc.N4,
		imptar.ipcf AS COD_N4,
		DATE_FORMAT(itl. DATA, '%d/%m/%Y') AS dat_conciliado,
		0 AS VALOR_PAGO,
                0 AS PORCENTAGEM,
                itl.valor AS val,
		'PG.' AS tipo_nota,
		itl.documento,
		imptar.item AS razaoSocial,
		'' AS descricao,
		indexed_demonstrativo_contas_bancarias.conta
            FROM
		impostos_tarifas imptar
                INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
                INNER JOIN contas_bancarias cb ON (cb.id = itl.conta)
                INNER JOIN plano_contas pc ON (imptar.ipcf = pc.COD_N4)
                INNER JOIN empresas emp ON (emp.id = cb.UR)
                INNER JOIN indexed_demonstrativo_contas_bancarias ON indexed_demonstrativo_contas_bancarias.ID = itl.conta
                WHERE
                itl.data BETWEEN '{$inicio}' AND '{$fim}'
                {$condTarifas}
            ORDER BY
		COD_N4,
		dat_conciliado ASC";
		} else {
			$sql = "SELECT
            empresas.nome as empresa,
            plano_contas.N4,
            plano_contas.COD_N4,
            DATE_FORMAT(parcelas.vencimento,'%d/%m/%Y') as dat_conciliado,
            parcelas.valor,
            rateio.PORCENTAGEM,
            ROUND((parcelas.valor * (rateio.PORCENTAGEM/100)),2) as val,
            (CASE notas.tipo WHEN 0 THEN 'RECEB.' ELSE 'PG.' END) as tipo_nota,
            notas.codigo,
            fornecedores.razaoSocial,
            notas.descricao
        FROM
            rateio
            INNER JOIN parcelas ON parcelas.idNota = rateio.NOTA_ID
            INNER JOIN notas ON parcelas.idNota = notas.idNotas
            INNER JOIN empresas ON empresas.id = rateio.CENTRO_RESULTADO
            INNER JOIN plano_contas ON rateio.IPCG4 = plano_contas.ID
            INNER JOIN fornecedores ON notas.codFornecedor = fornecedores.idFornecedores
        WHERE
            parcelas.`status` = 'Pendente'
            AND parcelas.vencimento BETWEEN '{$inicio}' AND '{$fim}'
            $condEmpresa
            $condIPCFN4
              ORDER BY
                COD_N4,
                dat_conciliado ASC";
		}

		$html.= "<table style='width:1257px;'>";
		$html.= "<tr bgcolor='#cccccc'>
                    <td align='left' style='font-size:12px;width:256px;'>IPCF</td>
                    <td align='left' style='font-size:12px;width:82px;'>| DATA</td>
                    <td align='left' style='font-size:12px;width:152px;'>| C. RESULT.</td>
                    <th align='left' style='font-size:12px;width:685px;'>| HIST&Oacute;RICO </th>
                    <th align='left' style='font-size:12px;width:152px;'>| CONTA </th>
                    <th align='left' style='font-size:12px;width:82px;'>| VALOR</th>
                 </tr>";
		$result = mysql_query($sql);
		$cont_cor=0;
		while($row = mysql_fetch_array($result)){

			if( $cont_cor%2 ==0 ){
				$cor = '';
			}else{
				$cor="bgcolor='#EEEEEE'";
			}

			$html.= "<tr $cor>
            <td align='left'>{$row['COD_N4']} - ".htmlentities($row['N4'])."</td>
            <td align='left'>{$row['dat_conciliado']}</td>
            <td align='left'>{$row['empresa']}</td>
            <td align='left'>{$row['tipo_nota']} DOC.{$row['codigo']} - ".htmlentities($row['razaoSocial'])." -".htmlentities($row['descricao'])."</td>
            <td align='left'>{$row['conta']}</td>
            <td align='left'>".number_format($row['val'],2,',','.')."</td>";
			$cont_cor++;

		}

		$html.= "</table>";

		echo $html;
	}
}

$p = new imprimir_rel_analito_fluxo_caixa();
$p->detalhe_rel_analito_fluxo_caixa(base64_decode($_GET['inicio']), base64_decode($_GET['fim']), base64_decode($_GET['status']), base64_decode($_GET['empresa']), $_GET['empresa_texto'], base64_decode($_GET['id_ipcf_n4']), $_GET['id_ipcf_n4_texto']);

   