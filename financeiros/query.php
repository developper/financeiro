<?php
$id = session_id();
if(empty($id))
  session_start();
  include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/codigos.php');
include_once("../utils/class.phpmailer.php");
include_once("tarifas_bancarias.php");

use App\Helpers\StringHelper;
use App\Models\Financeiro\Fornecedor;
use App\Models\Financeiro\Financeiro;
use App\Models\Financeiro\ClasseValor;
use App\Helpers\DateHelper;
use App\Models\Financeiro\Notas;
use App\Models\Financeiro\Bordero;
use App\Models\Financeiro\Parcela;

function salvar_nota($id,$codigo,$codNatureza,$codCentroCusto,$dataEntrada,$descricao,$codFornecedor,$tipo,$codSubNatureza){
  $id = anti_injection($id,'numerico');$codigo = anti_injection($codigo,'literal');$codNatureza = anti_injection($codNatureza,'numerico');
  $codCentroCusto = anti_injection($codCentroCusto,'numerico');$dataEntrada = anti_injection($dataEntrada,'literal');
  $descricao = anti_injection($descricao,'literal');
  $codFornecedor = anti_injection($codFornecedor,'numerico');$tipo = anti_injection($tipo,'numerico');
  $codSubNatureza = anti_injection($codSubNatureza,'numerico');
  $status = "Pendente";
  $empresa = $_SESSION["empresa_user"];
  $sql = "UPDATE `notas` SET `codigo` = '$codigo' ,  `codNatureza` = '$codNatureza' ,  `codCentroCusto` = '$codCentroCusto',  `dataEntrada` = '$dataEntrada',
	 `descricao` = '$descricao',  `codFornecedor` = '$codFornecedor',  `tipo` = '$tipo',  `status` = '$status',  `codSubNatureza` = '$codSubNatureza', `empresa` = '$empresa' 
	  WHERE idNotas = '$id'"; 
  if(mysql_query($sql)){ 
  	savelog(mysql_escape_string(addslashes($sql)));
  	echo $id;
  }
  else echo mysql_error();
}

function inserir_nota($post){
    extract($post,EXTR_OVERWRITE);
    
    $notaJaCadastrada = Notas::verificarNFJaExiste($codFornecedor, $num);
    if(!empty($notaJaCadastrada)){
        echo "Conta já possui cadastro no sistema TR {$notaJaCadastrada['idNotas']}.";
        return;
    }
        mysql_query('begin');
    $fechamento_caixa = new \DateTime(\App\Controllers\FechamentoCaixa::getFechamentoCaixa()['data_fechamento']);
    $ent = implode("-",array_reverse(explode("/",$dataEntrada)));
    $dataCompetencia = implode("-",array_reverse(explode("/",$dataCompetencia)));
   
	$data_entrada_obj = new \DateTime($ent);
	//echo $ent;
	if($data_entrada_obj <= $fechamento_caixa) {
        echo "-1";
        mysql_query("rollback");
        return;
    }
	$codigo = anti_injection($num,'literal');
	$natureza = 1;
  	$centrocusto = 1;
  	$valor_nota = str_replace('.','',$valor_nota);
  	$valor_nota = str_replace(',','.',$valor_nota);
  	
  	$val_produtos =str_replace('.','',$val_produtos);
  	$val_produtos =str_replace(',','.',$val_produtos);

  	$pis =str_replace('.','',$pis);
  	$pis =str_replace(',','.',$pis);

        $cofins =str_replace('.','',$cofins);
  	$cofins =str_replace(',','.',$cofins);
        
        $csll =str_replace('.','',$csll);
  	$csll =str_replace(',','.',$csll);
        
        $ir =str_replace('.','',$ir);
  	$ir =str_replace(',','.',$ir);

  	$issqn =str_replace('.','',$issqn);
  	$issqn =str_replace(',','.',$issqn);
        
  	$des_bon =str_replace('.','',$des_bon);
  	$des_bon =str_replace(',','.',$des_bon);
        
        $des_acessorias =str_replace('.','',$des_acessorias);
  	$des_acessorias =str_replace(',','.',$des_acessorias);
        
        $ipi=str_replace('.','',$ipi);
  	$ipi =str_replace(',','.',$ipi);
        
        $icms =str_replace('.','',$icms);
  	$icms =str_replace(',','.',$icms);
        
        $frete =str_replace('.','',$frete);
  	$frete =str_replace(',','.',$frete);
        
        $seguro =str_replace('.','',$seguro);
  	$seguro =str_replace(',','.',$seguro);
        
        $pcc =str_replace('.','',$pcc);
  	$pcc =str_replace(',','.',$pcc);
        
        $inss =str_replace('.','',$inss);
  	$inss =str_replace(',','.',$inss);

  	$descricao = mb_strtoupper(trim(anti_injection($descricao,'literal')), 'UTF-8');
  	$ur_nota = anti_injection($ur,'literal');
  	$natuezaMov = anti_injection($natureza_movimentacao,'literal');
  	$fornecedor = anti_injection($codFornecedor,'literal');
  	$tipo = anti_injection($tipo,'literal');
 		$subnatureza = 1;
 		$tr = 1;
 		$tipo_doc=anti_injection($tipo_doc,'literal');
		$status = "Pendente";
	
      $e = $_SESSION["empresa_principal"];
      $notaTipoDocumentoSigla = anti_injection($nota_tipo_documento_sigla,'literal'); 	

      $formaPagameto = anti_injection($forma_pagamento,'literal'); 
  	
  	
  	$sql = "INSERT INTO 
            `notas`   
            (USUARIO_ID,
             `codigo`,
             `codNatureza`,
             `codCentroCusto`,
             `valor`,
             `dataEntrada`,
              data_competencia,
             `descricao`,
             `codFornecedor`,
             `tipo`,
             `status`,
             `codSubNatureza`,
             `empresa`,
             `ICMS`,
             `FRETE`,
             `SEGURO`,
             `VAL_PRODUTOS`,
             `DES_ACESSORIAS`,
             `IPI`,
             `ISSQN`,
             `DESCONTO_BONIFICACAO`,
             `TR`,
             `TIPO_DOCUMENTO`,
             DATA,
             PIS,
             COFINS,
             CSLL,
             IR,
             INSS,
             PCC,
             tipo_documento_financeiro_id,
             natureza_movimentacao,
             forma_pagamento) 
             VALUES
             ('{$_SESSION['id_user']}',
                 '{$codigo}',
                 '{$natureza}',
                 '{$centrocusto}',
                 '{$valor_nota}',
                 '{$ent}',
                 '{$dataCompetencia}',
                 '{$descricao}',
                 '{$fornecedor}',
                 '{$tipo}',
                 'Em aberto',
                 '{$subnatureza}',
                 '{$ur_nota}',
                 '{$icms}',
                 '{$frete}',
                 '{$seguro}',
                 '{$val_produtos}',
                 '{$des_acessorias}',
                 '{$ipi}',
                 '{$issqn}',
                 '{$des_bon}',
                 '{$tr}',
                 '{$notaTipoDocumentoSigla}',
                 now(),
                '{$pis}',
                '{$cofins}',
                '{$csll}',
                '{$ir}',
                '{$inss}',
                '{$pcc}',
                '{$notas_tipo_documento}',
                '{$natuezaMov}',
                '{$formaPagameto}')";


  	$r = mysql_query($sql);
  	if(!$r){ 
		echo $sql;
		mysql_query("rollback");
		return;
	}
         $nota = mysql_insert_id(); 
	
         savelog(mysql_escape_string(addslashes($sql)));

	$tr= str_pad($nota,6,"0",STR_PAD_LEFT);
	$sql="UPDATE notas set TR= '{$tr}' where idNotas= '{$nota}'";
	$r = mysql_query($sql);
	if(!$r){
		echo "-122";
		mysql_query("rollback");
		return;
	}
	savelog(mysql_escape_string(addslashes($sql)));
  	$parcelas_nota = explode("$",$parcela);
  	$num = 1;
  	foreach($parcelas_nota as $parcela){
  		$p = explode("#",str_replace("undefined","",$parcela));
  		$ntr=anti_injection($p[0],'literal');
  		$venc = implode("-",array_reverse(explode("/",$p[1])));
  		$codBarras = anti_injection($p[2],'literal');
        $v = str_replace(",",".",str_replace(".","",$p[3]));
        $venc_real = implode("-",array_reverse(explode("/",$p[4])));  		
  		$venc = anti_injection($venc,'literal');
  		
  		$num++;
  		$sql = "INSERT INTO parcelas (`idNota`, `valor`, `vencimento`, `codBarras`, `status`, `empresa`,`TR`,vencimento_real ) 
  				VALUES ( '{$nota}', '{$v}', '{$venc}', '{$codBarras}', 'Pendente', '{$e}','{$ntr}','{$venc_real}')";
  		$r = mysql_query($sql);
  		if(!$r){ 
			echo "-12".$sql;
			mysql_query("rollback");
			return;
		}
		savelog(mysql_escape_string(addslashes($sql)));
  	}
  //	if($tipo==1){
        
        $cont=0;
  	$rateio_nota = explode("$", $rateio);
  	$somaRateio = 0;
  	$valorUltimoRateio = 0;
    $tipo_calculo = anti_injection($tipo_calculo_rateio,'numerico');
  	foreach($rateio_nota as $rateio){
  		$p = explode("#",str_replace("undefined","",$rateio));
        $ur=anti_injection($p[0],'numerico');
  		$v = str_replace(",",".",str_replace(".","",$p[1]));
        $ipcg4 = anti_injection($p[4],'numerico');
        $sqlIPCF4 = "SELECT N2, N3 FROM plano_contas WHERE ID = '{$ipcg4}'";
        $rsIPCF4 = mysql_query($sqlIPCF4);
        $rowIPCF4 = mysql_fetch_array($rsIPCF4, MYSQL_ASSOC);
  		$perc = str_replace(",",".",str_replace(".",",",$p[2]));
  		$ipcg3 = $rowIPCF4['N3'];
  		$ass = anti_injection($p[5],'literal');
        $ipcf2 = $rowIPCF4['N2'];
        if($tipo_calculo == '1') {
            $v = ($valor_nota * $perc) / 100;
        }
        
        $sql = "INSERT INTO rateio (`NOTA_ID`, `NOTA_TR`, `CENTRO_RESULTADO`, `VALOR`, `PORCENTAGEM`,IPCG3,IPCG4,ASSINATURA,IPCF2)
  		VALUES ( '{$nota}', '{$tr}', '{$ur}', '{$v}', '{$perc}','{$ipcg3}','{$ipcg4}','{$ass}','{$ipcf2}')";  		
  		$r = mysql_query($sql);
  		if(!$r){
            echo "-13";
            mysql_query("rollback");
            return;
        }
        $somaRateio += $perc;
        $valorUltimoRateio = $perc;
        $somaValorRateio += $v;
        $valorUltimoRateio = $v;
  		$idTabelaRateio = mysql_insert_id();


        if($ur != $_SESSION['empresa_principal']){
              $ur_rateio[]=$ur; 
              $cont++;
            }
        
  	savelog(mysql_escape_string(addslashes($sql)));

  	}

    if($somaRateio > 100){
        $dif = 100 - $somaRateio;
        $difValor = $valor_nota - $somaValorRateio;
        $sql = "update rateio set PORCENTAGEM = PORCENTAGEM + {$dif}, VALOR = VALOR +{$difValor} where ID = $idTabelaRateio ";
        $r = mysql_query($sql);
        if(!$r){
            echo "-13.1";
            mysql_query("rollback");
            return;
        }
    }else if($somaRateio < 100){
        $dif = 100 - $somaRateio;
        $difValor = $valor_nota - $somaValorRateio;
        $sql = "update rateio set PORCENTAGEM = PORCENTAGEM + {$dif}, VALOR = VALOR +{$difValor}  where ID = $idTabelaRateio ";
        $r = mysql_query($sql);
        if(!$r){
            echo "-13.2";
            mysql_query("rollback");
            return;
        }

    }
        
        ///subnotas
        $subnotas = explode('$', $imposto_retido);
     foreach($subnotas as $subnota){
         if($subnota != NULL && $subnota!=''){
            $s = explode("#",str_replace("undefined","",$subnota));
            $imposto = anti_injection($s[0],'literal');
            $cod_darf_dan = anti_injection($s[1],'literal');
            $vencimento_subnota = implode("-",array_reverse(explode("/",$s[2])));
            $vencimento_real_subnota = implode("-",array_reverse(explode("/",DateHelper::diaUtilVencimentoReal($s[2]))));
            $valor_subnota =$s[3];
            $fornecedor_subnota =anti_injection($s[4],'numerico');
            $ipcg4_subnota=anti_injection($s[5],'numerico');
            $impostos_retidos = anti_injection($s[6],'numerico');
            $tipo_subnota= $imposto;
            $descricao_subnota= "Imposto {$imposto} referente a Nota N°-{$codigo} de TR-{$nota}. ";
            $tipoDocFinId = anti_injection($s[7],'numerico');
            
           $sql=" INSERT INTO 
            `notas`   
            (USUARIO_ID,
             `codigo`,
             `codNatureza`,
             `codCentroCusto`,
             `valor`,
             `dataEntrada`,
             data_competencia,
             `descricao`,
             `codFornecedor`,
             `tipo`,
             `status`,
             `codSubNatureza`,
             `empresa`,             
             `VAL_PRODUTOS`,             
             `TR`,
             `TIPO_DOCUMENTO`,
             DATA, 
             NOTA_REFERENCIA,
             IMPOSTOS_RETIDOS_ID,
             tipo_documento_financeiro_id,
             natureza_movimentacao)             
             VALUES
             ('{$_SESSION['id_user']}',
                 '{$cod_darf_dan}',
                 '{$natureza}',
                 '{$centrocusto}',
                 '{$valor_subnota}',
                 '{$ent}',
                 '{$dataCompetencia}',
                 '{$descricao_subnota}',
                 '{$fornecedor_subnota}',
                 '1',
                 'Em aberto',
                 '{$subnatureza}',
                 '{$ur_nota}',                 
                 '{$valor_subnota}',
                 '1',
                 '{$tipo_subnota}',
                 now(),
                 $nota,
                 $impostos_retidos,
                 $tipoDocFinId,
                 '{$ipcg4_subnota}'
                )";
               $r = mysql_query($sql);
                $id_subnota = mysql_insert_id();
                if(!$r){
                        echo "-122";
                        mysql_query("rollback");
                        return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
                
                $tr_subnota= str_pad($id_subnota,6,"0",STR_PAD_LEFT);
                $sql="UPDATE notas set TR= '{$tr_subnota}' where idNotas= '{$id_subnota}'";
                $r = mysql_query($sql);
                if(!$r){
                        echo "-122";
                        mysql_query("rollback");
                        return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
               
  		if(!$r){
                    echo "Erro ao inserir subnota";
                    mysql_query("rollback");
                    return;               
  	          }      
                  savelog(mysql_escape_string(addslashes($sql)));

                  
                
                 
               //inserir parcela   
                $sql = "INSERT INTO parcelas (`idNota`, `valor`, `vencimento`, `codBarras`, `status`, `empresa`,`TR`, vencimento_real ) 
  			VALUES ( '{$id_subnota}', '{$valor_subnota}', '{$vencimento_subnota}', '', 'Pendente', '{$e}','1','{$vencimento_real_subnota}')";
  		$r = mysql_query($sql);
  		if(!$r){ 
			echo "-12".$sql;
			mysql_query("rollback");
			return;
		}
		savelog(mysql_escape_string(addslashes($sql)));
                ////inserir rateio
              $somaRateio =0;
                    foreach($rateio_nota as $rateio){
                    $p = explode("#",str_replace("undefined","",$rateio));
                    $ur=anti_injection($p[0],'numerico');

                    $perc = str_replace(",",".",str_replace(".",",",$p[2]));


                    $ass = anti_injection($p[5],'literal');
                    $valor_subnota_rateio= $valor_subnota*($perc/100);

        
                   $sql = "INSERT INTO rateio (`NOTA_ID`, `NOTA_TR`, `CENTRO_RESULTADO`, `VALOR`, `PORCENTAGEM`,IPCG3,IPCG4,ASSINATURA,IPCF2)
                    VALUES ( '{$id_subnota}', '{$id_subnota}', '{$ur}', '{$valor_subnota_rateio}', '{$perc}','IMPOSTOS E TAXAS','{$ipcg4_subnota}','{$ass}','DESEMBOLSOS ADMINISTRATIVOS')";  		
                    $r = mysql_query($sql);
                    if(!$r){ 
                        echo "-13";
                        mysql_query("rollback");
                        return;

                   }
                        $somaRateio += $perc;
                        $valorUltimoRateio = $perc;
                        $idTabelaRateio = mysql_insert_id();


                 savelog(mysql_escape_string(addslashes($sql)));

  	     }
             if($somaRateio > 100){
                 $dif = 100 - $somaRateio;
                 $sql = "update rateio set PORCENTAGEM = PORCENTAGEM + {$dif} where ID = $idTabelaRateio ";
                 $r = mysql_query($sql);
                 if(!$r){
                     echo "-13.1";
                     mysql_query("rollback");
                     return;
                 }
             }else if($somaRateio < 100){
                 $dif = 100 - $somaRateio;
                 $sql = "update rateio set PORCENTAGEM = PORCENTAGEM + {$dif} where ID = $idTabelaRateio ";
                 $r = mysql_query($sql);
                 if(!$r){
                     echo "-13.2";
                     mysql_query("rollback");
                     return;
                 }

             }
                
        }
   } 
   
  	mysql_query("commit"); 
        print_r($sql_1);
       if($cont>0){
	   enviar_email_rateio($ur_rateio,$nota);
        }else{
            echo $nota;
        }
}

function inserir_nota_usar_nova($post){
    extract($post,EXTR_OVERWRITE);

    mysql_query('begin');
    $fechamento_caixa = new \DateTime(\App\Controllers\FechamentoCaixa::getFechamentoCaixa()['data_fechamento']);
    $ent = implode("-",array_reverse(explode("/",$dataEntrada)));
    $data_entrada_obj = new \DateTime($ent);
    //echo $ent;
    if($data_entrada_obj <= $fechamento_caixa) {
        echo "-1";
        mysql_query("rollback");
        return;
    }
    $codigo = anti_injection($num,'literal');
    $natureza = 1;
    $centrocusto = 1;
    $valor_nota = str_replace('.','',$valor_nota);
    $valor_nota = str_replace(',','.',$valor_nota);

    $val_produtos =str_replace('.','',$val_produtos);
    $val_produtos =str_replace(',','.',$val_produtos);

    $pis =str_replace('.','',$pis);
    $pis =str_replace(',','.',$pis);

    $cofins =str_replace('.','',$cofins);
    $cofins =str_replace(',','.',$cofins);

    $csll =str_replace('.','',$csll);
    $csll =str_replace(',','.',$csll);

    $ir =str_replace('.','',$ir);
    $ir =str_replace(',','.',$ir);

    $issqn =str_replace('.','',$issqn);
    $issqn =str_replace(',','.',$issqn);

    $des_bon =str_replace('.','',$des_bon);
    $des_bon =str_replace(',','.',$des_bon);

    $des_acessorias =str_replace('.','',$des_acessorias);
    $des_acessorias =str_replace(',','.',$des_acessorias);

    $ipi=str_replace('.','',$ipi);
    $ipi =str_replace(',','.',$ipi);

    $icms =str_replace('.','',$icms);
    $icms =str_replace(',','.',$icms);

    $frete =str_replace('.','',$frete);
    $frete =str_replace(',','.',$frete);

    $seguro =str_replace('.','',$seguro);
    $seguro =str_replace(',','.',$seguro);

    $pcc =str_replace('.','',$pcc);
    $pcc =str_replace(',','.',$pcc);

    $inss =str_replace('.','',$inss);
    $inss =str_replace(',','.',$inss);

    $descricao = anti_injection($descricao,'literal');
    $ur_nota = anti_injection($ur,'literal');
    $natuezaMov = anti_injection($natureza_movimentacao,'literal');
    $fornecedor = anti_injection($codFornecedor,'literal');
    $tipo = anti_injection($tipo,'literal');
    $subnatureza = 1;
    $tr = 1;
    $tipo_doc=anti_injection($tipo_doc,'literal');
    $status = "Pendente";

    $e = $_SESSION["empresa_principal"];
    $notaTipoDocumentoSigla = anti_injection($nota_tipo_documento_sigla,'literal');

    $sql = "INSERT INTO 
            `notas`   
            (USUARIO_ID,
             `codigo`,
             `codNatureza`,
             `codCentroCusto`,
             `valor`,
             `dataEntrada`,
             `descricao`,
             `codFornecedor`,
             `tipo`,
             `status`,
             `codSubNatureza`,
             `empresa`,
             `ICMS`,
             `FRETE`,
             `SEGURO`,
             `VAL_PRODUTOS`,
             `DES_ACESSORIAS`,
             `IPI`,
             `ISSQN`,
             `DESCONTO_BONIFICACAO`,
             `TR`,
             `TIPO_DOCUMENTO`,
             DATA,
             PIS,
             COFINS,
             CSLL,
             IR,
             INSS,
             PCC,
             tipo_documento_financeiro_id,
             natureza_movimentacao) 
             VALUES
             ('{$_SESSION['id_user']}',
                 '{$codigo}',
                 '{$natureza}',
                 '{$centrocusto}',
                 '{$valor_nota}',
                 '{$ent}',
                 '{$descricao}',
                 '{$fornecedor}',
                 '{$tipo}',
                 'Em aberto',
                 '{$subnatureza}',
                 '{$ur_nota}',
                 '{$icms}',
                 '{$frete}',
                 '{$seguro}',
                 '{$val_produtos}',
                 '{$des_acessorias}',
                 '{$ipi}',
                 '{$issqn}',
                 '{$des_bon}',
                 '{$tr}',
                 '{$notaTipoDocumentoSigla}',
                 now(),
                '{$pis}',
                '{$cofins}',
                '{$csll}',
                '{$ir}',
                '{$inss}',
                '{$pcc}',
                '{$notas_tipo_documento}',
                '{$natuezaMov}')";

    $r = mysql_query($sql);
    if(!$r){
        echo "-1";
        mysql_query("rollback");
        return;
    }
    $nota = mysql_insert_id();

    savelog(mysql_escape_string(addslashes($sql)));

    $descricaoPR = "PR#{$nota}";
    $tr= str_pad($nota,6,"0",STR_PAD_LEFT);
    $sql="UPDATE notas set TR= '{$tr}', codigo = '{$descricaoPR}' where idNotas= '{$nota}'";
    $r = mysql_query($sql);
    if(!$r){
        echo "-122";
        mysql_query("rollback");
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));
    $parcelas_nota = explode("$",$parcela);
    $num = 1;
    foreach($parcelas_nota as $parcela){
        $p = explode("#",str_replace("undefined","",$parcela));
        $ntr=anti_injection($p[0],'literal');
        $venc = implode("-",array_reverse(explode("/",$p[1])));
        $codBarras = anti_injection($p[2],'literal');
        $v = str_replace(",",".",str_replace(".","",$p[3]));
        $venc_real = implode("-",array_reverse(explode("/",$p[4])));
        $venc = anti_injection($venc,'literal');

        $num++;
        $sql = "INSERT INTO parcelas (`idNota`, `valor`, `vencimento`, `codBarras`, `status`, `empresa`,`TR`,vencimento_real ) 
  				VALUES ( '{$nota}', '{$v}', '{$venc}', '{$codBarras}', 'Pendente', '{$e}','{$ntr}','{$venc_real}')";
        $r = mysql_query($sql);
        if(!$r){
            echo "-12".$sql;
            mysql_query("rollback");
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));
    }
    //	if($tipo==1){

    $cont=0;
    $rateio_nota = explode("$",$rateio);
    $somaRateio = 0;
    $valorUltimoRateio = 0;
    foreach($rateio_nota as $rateio){
        $p = explode("#",str_replace("undefined","",$rateio));
        $ur=anti_injection($p[0],'numerico');
        $v = str_replace(",",".",str_replace(".","",$p[1]));
        $ipcg4 = anti_injection($p[4],'numerico');
        $sqlIPCF4 = "SELECT N2, N3 FROM plano_contas WHERE ID = '{$ipcg4}'";
        $rsIPCF4 = mysql_query($sqlIPCF4);
        $rowIPCF4 = mysql_fetch_array($rsIPCF4, MYSQL_ASSOC);
        $perc = str_replace(",",".",str_replace(".",",",$p[2]));
        $ipcg3 = $rowIPCF4['N3'];
        $ass = anti_injection($p[5],'literal');
        $ipcf2 = $rowIPCF4['N2'];

        $sql = "INSERT INTO rateio (`NOTA_ID`, `NOTA_TR`, `CENTRO_RESULTADO`, `VALOR`, `PORCENTAGEM`,IPCG3,IPCG4,ASSINATURA,IPCF2)
  		VALUES ( '{$nota}', '{$tr}', '{$ur}', '{$v}', '{$perc}','{$ipcg3}','{$ipcg4}','{$ass}','{$ipcf2}')";
        $r = mysql_query($sql);
        if(!$r){
            echo "-13";
            mysql_query("rollback");
            return;
        }

        $somaRateio += $perc;
        $valorUltimoRateio = $perc;
        $idTabelaRateio = mysql_insert_id();


        if($ur != $_SESSION['empresa_principal']){
            $ur_rateio[]=$ur;
            $cont++;
        }

        savelog(mysql_escape_string(addslashes($sql)));

    }

    if($somaRateio > 100){
        $dif = 100 - $somaRateio;
        $sql = "update rateio set PORCENTAGEM = PORCENTAGEM + {$dif} where ID = $idTabelaRateio ";
        $r = mysql_query($sql);
        if(!$r){
            echo "-13.1";
            mysql_query("rollback");
            return;
        }
    }else if($somaRateio < 100){
        $dif = 100 - $somaRateio;
        $sql = "update rateio set PORCENTAGEM = PORCENTAGEM + {$dif} where ID = $idTabelaRateio ";
        $r = mysql_query($sql);
        if(!$r){
            echo "-13.2";
            mysql_query("rollback");
            return;
        }

    }

    ///subnotas
    $subnotas = explode('$', $imposto_retido);
    foreach($subnotas as $subnota){
        if($subnota != NULL && $subnota!=''){
            $s = explode("#",str_replace("undefined","",$subnota));
            $imposto = anti_injection($s[0],'literal');
            $cod_darf_dan = anti_injection($s[1],'literal');
            $vencimento_subnota = implode("-",array_reverse(explode("/",$s[2])));
            $vencimento_real_subnota = implode("-",array_reverse(explode("/",DateHelper::diaUtilVencimentoReal($s[2]))));
            $valor_subnota =$s[3];
            $fornecedor_subnota =anti_injection($s[4],'numerico');
            $ipcg4_subnota=anti_injection($s[5],'numerico');
            $impostos_retidos = anti_injection($s[6],'numerico');
            $tipo_subnota= $imposto;
            $descricao_subnota= "Imposto {$imposto} referente a Nota N°-{$codigo} de TR-{$nota}. ";
            $tipoDocFinId = anti_injection($s[7],'numerico');

            $sql=" INSERT INTO 
            `notas`   
            (USUARIO_ID,
             `codigo`,
             `codNatureza`,
             `codCentroCusto`,
             `valor`,
             `dataEntrada`,
             `descricao`,
             `codFornecedor`,
             `tipo`,
             `status`,
             `codSubNatureza`,
             `empresa`,             
             `VAL_PRODUTOS`,             
             `TR`,
             `TIPO_DOCUMENTO`,
             DATA, 
             NOTA_REFERENCIA,
             IMPOSTOS_RETIDOS_ID,
             tipo_documento_financeiro_id,
             natureza_movimentacao)             
             VALUES
             ('{$_SESSION['id_user']}',
                 '{$cod_darf_dan}',
                 '{$natureza}',
                 '{$centrocusto}',
                 '{$valor_subnota}',
                 '{$ent}',
                 '{$descricao_subnota}',
                 '{$fornecedor_subnota}',
                 '1',
                 'Em aberto',
                 '{$subnatureza}',
                 '{$ur_nota}',                 
                 '{$valor_subnota}',
                 '1',
                 '{$tipo_subnota}',
                 now(),
                 $nota,
                 $impostos_retidos,
                 $tipoDocFinId,
                 '{$ipcg4_subnota}'
                )";
            $r = mysql_query($sql);
            $id_subnota = mysql_insert_id();
            if(!$r){
                echo "-122";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));

            $tr_subnota= str_pad($id_subnota,6,"0",STR_PAD_LEFT);
            $sql="UPDATE notas set TR= '{$tr_subnota}' where idNotas= '{$id_subnota}'";
            $r = mysql_query($sql);
            if(!$r){
                echo "-122";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));

            if(!$r){
                echo "Erro ao inserir subnota";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));




            //inserir parcela
            $sql = "INSERT INTO parcelas (`idNota`, `valor`, `vencimento`, `codBarras`, `status`, `empresa`,`TR`, vencimento_real ) 
  			VALUES ( '{$id_subnota}', '{$valor_subnota}', '{$vencimento_subnota}', '', 'Pendente', '{$e}','1','{$vencimento_real_subnota}')";
            $r = mysql_query($sql);
            if(!$r){
                echo "-12".$sql;
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));
            ////inserir rateio
            $somaRateio =0;
            foreach($rateio_nota as $rateio){
                $p = explode("#",str_replace("undefined","",$rateio));
                $ur=anti_injection($p[0],'numerico');

                $perc = str_replace(",",".",str_replace(".",",",$p[2]));


                $ass = anti_injection($p[5],'literal');
                $valor_subnota_rateio= $valor_subnota*($perc/100);


                $sql = "INSERT INTO rateio (`NOTA_ID`, `NOTA_TR`, `CENTRO_RESULTADO`, `VALOR`, `PORCENTAGEM`,IPCG3,IPCG4,ASSINATURA,IPCF2)
                    VALUES ( '{$id_subnota}', '{$id_subnota}', '{$ur}', '{$valor_subnota_rateio}', '{$perc}','IMPOSTOS E TAXAS','{$ipcg4_subnota}','{$ass}','DESEMBOLSOS ADMINISTRATIVOS')";
                $r = mysql_query($sql);
                if(!$r){
                    echo "-13";
                    mysql_query("rollback");
                    return;

                }
                $somaRateio += $perc;
                $valorUltimoRateio = $perc;
                $idTabelaRateio = mysql_insert_id();


                savelog(mysql_escape_string(addslashes($sql)));

            }
            if($somaRateio > 100){
                $dif = 100 - $somaRateio;
                $sql = "update rateio set PORCENTAGEM = PORCENTAGEM + {$dif} where ID = $idTabelaRateio ";
                $r = mysql_query($sql);
                if(!$r){
                    echo "-13.1";
                    mysql_query("rollback");
                    return;
                }
            }else if($somaRateio < 100){
                $dif = 100 - $somaRateio;
                $sql = "update rateio set PORCENTAGEM = PORCENTAGEM + {$dif} where ID = $idTabelaRateio ";
                $r = mysql_query($sql);
                if(!$r){
                    echo "-13.2";
                    mysql_query("rollback");
                    return;
                }

            }

        }
    }

    mysql_query("commit");
    print_r($sql_1);
    if($cont>0){
        enviar_email_rateio($ur_rateio,$nota);
    }else{
        echo $nota;
    }
}


function editar_nota($post){
    
    extract($post,EXTR_OVERWRITE);
    $notaJaCadastrada = Notas::verificarNFJaExiste($codFornecedor, $num);
    if(!empty($notaJaCadastrada) && $notaJaCadastrada['idNotas'] != $idnota){
        echo "Conta já possui cadastro no sistema TR {$notaJaCadastrada['idNotas']}.";
        return;
    }
    mysql_query('begin');
    $ent = implode("-",array_reverse(explode("/",$dataEntrada)));
    $dataCompetencia = implode("-",array_reverse(explode("/",$dataCompetencia)));
    $fechamento_caixa = new \DateTime(\App\Controllers\FechamentoCaixa::getFechamentoCaixa()['data_fechamento']);
    $data_entrada_obj = new \DateTime($ent);

    // VERIFICA SE A DATA QUE VEIO DO FRONT É IGUAL A DATA ORIGINAL DA NOTA
    $sqlDataEntrada = "SELECT dataEntrada FROM notas WHERE idNotas = '{$idnota}'";
    $rsDataEntrada = mysql_query($sqlDataEntrada);
    $rowDataEntrada = mysql_fetch_array($rsDataEntrada);
    $dataEntradaOriginal = new \DateTime($rowDataEntrada['dataEntrada']);
    if($dataEntradaOriginal != $data_entrada_obj && $data_entrada_obj <= $fechamento_caixa) {
        echo "-1";
        mysql_query("rollback");
        return;
    }
    $codigo = anti_injection($num,'literal');
    $ur_nota = anti_injection($ur,'literal');
    $natureza = 1;
    $centrocusto = 1;
    $valor_nota = str_replace('.','',$valor_nota);
    $valor_nota = str_replace(',','.',$valor_nota);

    $val_produtos =str_replace('.','',$val_produtos);
    $val_produtos =str_replace(',','.',$val_produtos);

    $pis =str_replace('.','',$pis);
    $pis =str_replace(',','.',$pis);

    $cofins =str_replace('.','',$cofins);
    $cofins =str_replace(',','.',$cofins);

    $csll =str_replace('.','',$csll);
    $csll =str_replace(',','.',$csll);

    $ir =str_replace('.','',$ir);
    $ir =str_replace(',','.',$ir);

    $issqn =str_replace('.','',$issqn);
    $issqn =str_replace(',','.',$issqn);

    $des_bon =str_replace('.','',$des_bon);
    $des_bon =str_replace(',','.',$des_bon);

    $des_acessorias =str_replace('.','',$des_acessorias);
    $des_acessorias =str_replace(',','.',$des_acessorias);

    $ipi=str_replace('.','',$ipi);
    $ipi =str_replace(',','.',$ipi);

    $icms =str_replace('.','',$icms);
    $icms =str_replace(',','.',$icms);

    $frete =str_replace('.','',$frete);
    $frete =str_replace(',','.',$frete);

    $seguro =str_replace('.','',$seguro);
    $seguro =str_replace(',','.',$seguro);

    $pcc =str_replace('.','',$pcc);
    $pcc =str_replace(',','.',$pcc);

    $inss =str_replace('.','',$inss);
    $inss =str_replace(',','.',$inss);
    $idnota =anti_injection($idnota,'literal');
    $descricao = mb_strtoupper(trim(anti_injection($descricao,'literal')), 'UTF-8');
    $fornecedor = anti_injection($codFornecedor,'literal');
    $naturezaMov = anti_injection($natureza_movimentacao,'literal');
    $tipo = anti_injection($tipo,'literal');
    $subnatureza = 1;
    $tr = 1;
    $tipo_doc=anti_injection($tipo_doc,'literal');
    $status = "Em Aberto";
    $notaTipoDocumentoSigla = anti_injection($nota_tipo_documento_sigla,'literal');
    $tipo_calculo_rateio = anti_injection($tipo_calculo_rateio,'numerico');
    $formaPagamento = anti_injection($forma_pagamento,'literal'); 
    

    $e = $_SESSION["empresa_principal"];
        
    
   

    $sql= "UPDATE 
            notas set 
            `codigo` =	'{$num}' , 
	`codNatureza` =	 '{$natureza}' , 
	 `codCentroCusto` ='{$centrocusto}',
	`valor` = '{$valor_nota}' ,
	`dataEntrada`  ='{$ent}' , 
	 `descricao`   = '{$descricao}', 
	`codFornecedor`  = '{$fornecedor}' , 
	 `tipo` = '{$tipo}' ,
          `codSubNatureza`= '{$subnatureza}', 
	 `empresa`=	'{$ur_nota}',
     data_competencia = '{$dataCompetencia}',
	ICMS=	'{$icms}', 
	FRETE=	'{$frete}',
	SEGURO=	'{$seguro}',
	VAL_PRODUTOS=	'{$val_produtos}',
	DES_ACESSORIAS=	'{$des_acessorias}',
	 IPI=	'{$ipi}',
	ISSQN=	'{$issqn}',
	DESCONTO_BONIFICACAO=	'{$des_bon}',
	TR=	'{$idnota}',
	TIPO_DOCUMENTO=	'{$notaTipoDocumentoSigla}',
        PIS='{$pis}',
        COFINS ='{$cofins}',
        CSLL = '{$csll}',
        IR = '{$ir}',
        PCC = '{$pcc}',
        INSS = '{$inss}',
        tipo_documento_financeiro_id = '{$notas_tipo_documento}',
        natureza_movimentacao = '{$naturezaMov}',
        forma_pagamento = '{$formaPagamento}'
	where idNotas ='{$idnota}'";


    $r = mysql_query($sql);
    if(!$r){
        echo "-1";
        mysql_query("rollback");
        return;
    }

    savelog(mysql_escape_string(addslashes($sql)));


    $parcelas_nota = explode("$",$parcela);

    foreach($parcelas_nota as $parcela){
        $p = explode("#",str_replace("undefined","",$parcela));
        $ntr=anti_injection($p[0],'literal');
        $venc = implode("-",array_reverse(explode("/",$p[1])));
        $codBarra=anti_injection($p[2],'literal');
        $valor = str_replace(",",".",str_replace(".","",$p[3]));
        $st=anti_injection($p[4],'literal');
        $id_parcela = anti_injection($p[5],'numerico');
        $data_editar_parcela =anti_injection($p[6],'numerico');
        $venc = anti_injection($venc,'literal');
        $venc_real = implode("-",array_reverse(explode("/",$p[7])));
        $status_parcela = anti_injection($p[8],'literal');

        if($st == 'Pendente'){
            $sql= " UPDATE 
                            parcelas
                            set
                            valor='{$valor}',
                            vencimento='{$venc}',
                            codBarras='{$codBarra}',
                            status='{$st}',
                            empresa='{$e}',
                            TR='{$ntr}',
                            vencimento_real = '{$venc_real}'
                            where
                            id='{$id_parcela}'                                
                            ";
            $r = mysql_query($sql);

            if(!$r){
                echo "-2".$sql;
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));

            $array_id_parcela[]=$id_parcela;

        }

        if($id_parcela == 0){
            $sql = "INSERT INTO parcelas (`idNota`, `valor`, `vencimento`, `codBarras`, `status`, `empresa`,`TR`, vencimento_real )
			VALUES ( '{$idnota}', '{$valor}', '{$venc}', '{$codBarra}', '{$st}', '{$e}','{$ntr}','{$venc_real}')";
            $r = mysql_query($sql);
            if(!$r){
                echo "-3".$sql;
                mysql_query("rollback");
                return;
            }
            $array_id_parcela[]= mysql_insert_id();
            savelog(mysql_escape_string(addslashes($sql)));

        }
        $array_id_parcela[]=$id_parcela;





    }


    $sql= " UPDATE 
            parcelas
            set                           
            status='Cancelada'                         
            where
            idNota='{$idnota}' and 
            id not in (".implode(',',$array_id_parcela).")";
    $r = mysql_query($sql);

    if(!$r){
        echo "-4";
        mysql_query("rollback");
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));

    $sql= "DELETE from rateio where NOTA_ID= {$idnota}";
    $r = mysql_query($sql);
    if(!$r){
        echo "-5";
        mysql_query("rollback");
        return;
    }
    savelog(mysql_escape_string(addslashes($sql)));


    $rateio_nota = explode("$",$rateio);
    $somaRateio =0;
    foreach($rateio_nota as $rateio){
        $p = explode("#",str_replace("undefined","",$rateio));

        $ur=anti_injection($p[0],'literal');
        $v = str_replace(",",".",str_replace(".","",$p[1]));
        $ipcg4 = anti_injection($p[4],'numerico');
        $sqlIPCF4 = "SELECT N2, N3 FROM plano_contas WHERE ID = '{$ipcg4}'";
        $rsIPCF4 = mysql_query($sqlIPCF4);
        $rowIPCF4 = mysql_fetch_array($rsIPCF4, MYSQL_ASSOC);
        $perc = str_replace(",",".",str_replace(".",",",$p[2]));
        $ipcg3 = $rowIPCF4['N3'];
        $ass = anti_injection($p[5],'literal');
        $ipcf2 = $rowIPCF4['N2'];
        if($tipo_calculo_rateio == '1') {
            $v = round(($valor_nota * $perc) / 100,2);
        }

        $sql = "INSERT INTO rateio (`NOTA_ID`, `NOTA_TR`, `CENTRO_RESULTADO`, `VALOR`, `PORCENTAGEM`,IPCG3,IPCG4,ASSINATURA,IPCF2 )
		VALUES ( '{$idnota}', '{$tr}', '{$ur}', '{$v}', '{$perc}','{$ipcg3}','{$ipcg4}','{$ass}','{$ipcf2}')";
        $r = mysql_query($sql);
        if(!$r){
            echo "-6";
            mysql_query("rollback");
            return;
        }
        $somaRateio += $perc;
        $valorUltimoRateio = $perc;
        $somaValorRateio += $v;
        $valorUltimoRateio = $v;
        $idTabelaRateio = mysql_insert_id();

        if($ur != $_SESSION['empresa_principal']){
            $ur_rateio[]=$ur;
            $cont++;
        }
        savelog(mysql_escape_string(addslashes($sql)));
    }
    if($somaRateio > 100){
        $dif = 100 - $somaRateio;
        $difValor = $valor_nota - $somaValorRateio;
        $sql = "update rateio set PORCENTAGEM = PORCENTAGEM + {$dif}, VALOR = VALOR +{$difValor} where ID = $idTabelaRateio ";
        $r = mysql_query($sql);
        if(!$r){
            echo "-13.1";
            mysql_query("rollback");
            return;
        }
    }else if($somaRateio < 100){
        $dif = 100 - $somaRateio;
        $difValor = $valor_nota - $somaValorRateio;
        $sql = "update rateio set PORCENTAGEM = PORCENTAGEM + {$dif}, VALOR = VALOR +{$difValor}  where ID = $idTabelaRateio ";
        $r = mysql_query($sql);
        if(!$r){
            echo "-13.2";
            mysql_query("rollback");
            return;
        }

    }
    //}
   
    ////////////////////////editar subnotas
    $subnotas = explode('$', $imposto_retido);
   
    foreach($subnotas as $subnota){
       
        
        if($subnota != NULL && $subnota!=''){
            $s = explode("#",str_replace("undefined","",$subnota));
            
            $imposto = anti_injection($s[0],'literal');
            $cod_darf_dan = anti_injection($s[1],'literal');
            
            
            if(!empty($s[2])){
                $vencimento_subnota = implode("-",array_reverse(explode("/",$s[2])));
                $vencimento_real_subnota = implode("-",array_reverse(explode("/",DateHelper::diaUtilVencimentoReal($s[2]))));
                $condVencimentoSubnota = ", vencimento = '{$vencimento_subnota}'";
                $condVencimentoRealSubnota = ", vencimento_real = '{$vencimento_real_subnota}' ";
            }else{
                $vencimento_subnota = '';
                $vencimento_real_subnota ='';
                $condVencimentoSubnota='';
                $condVencimentoRealSubnota='';
            }
            $valor_subnota =$s[3];
            
            $fornecedor_subnota =anti_injection($s[4],'numerico');
            $ipcg4_subnota=anti_injection($s[5],'numerico');
            $impostos_retidos = anti_injection($s[6],'numerico');
            $tipo_subnota= $imposto;
            $descricao_subnota= "Imposto {$imposto} referente a Nota N°-{$codigo} de TR-{$idnota}. ";
            $id_subnota = anti_injection($s[7],'numerico');
            $tipoDocFinId = anti_injection($s[8],'numerico');
            

            
           
            if($id_subnota>0){
                
                $status_subnotas = $valor_subnota > 0? 'Em aberto':'Cancelada';
                $sql=" UPDATE                             
                              `notas`   
                                set
                              USUARIO_ID = '{$_SESSION['id_user']}',
                             `codigo` = '{$cod_darf_dan}',
                             `codNatureza` = '{$natureza}',
                             `natureza_movimentacao` = '{$ipcg4_subnota}',
                             `codCentroCusto`='{$centrocusto}',
                             `valor`='{$valor_subnota}',
                             `dataEntrada`='{$ent}',
                             data_competencia = '{$dataCompetencia}',
                             `descricao`='{$descricao_subnota}',
                             `codFornecedor`='{$fornecedor_subnota}',
                             `tipo`='1',
                             `status`='{$status_subnotas}',
                             `codSubNatureza`='{$subnatureza}',
                             `empresa`='{$ur_nota}',             
                             `VAL_PRODUTOS`='{$valor_subnota}',             
                             `TR`='{$id_subnota}',
                             `TIPO_DOCUMENTO`='{$tipo_subnota}',
                             DATA=now(), 
                             NOTA_REFERENCIA='{$idnota}',
                             IMPOSTOS_RETIDOS_ID='{$impostos_retidos}',
                             tipo_documento_financeiro_id = $tipoDocFinId           
                             where
                             idNotas={$id_subnota}";
                             

                $r = mysql_query($sql);
                if(!$r){
                    echo "-7";
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
                
                ///parcelas sub_nota update       
                $status_subnotas_parcelas = $valor_subnota== 0? 'Cancelada':'Pendente';
                $sql = "Update 
                               parcelas set  
                              `valor`='{$valor_subnota}
                              '$condVencimentoSubnota,
                              `status`='{$status_subnotas_parcelas}', 
                              `empresa`='{$ur_nota}',
                              `TR` ='1'
                              $condVencimentoRealSubnota 
                              where
                              idNota = {$id_subnota}  and
                              `status` <> 'Cancelada'    
                              ";
                             

                $r = mysql_query($sql);
                if(!$r){
                    echo "-8";
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));

                ////rateio subnota update
                $sql= "DELETE from rateio where NOTA_ID= {$id_subnota}";
                $r = mysql_query($sql);
                if(!$r){
                    echo "-9";
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
                $somaRateio = 0;
                foreach($rateio_nota as $rateio){
                    $p    = explode("#",str_replace("undefined","",$rateio));
                    $ur   = anti_injection($p[0],'numerico');
                    $perc = str_replace(",",".",str_replace(".",",",$p[2]));
                    $ass  = anti_injection($p[5],'literal');
                    $valor_subnota_rateio= $valor_subnota*($perc/100);
                    $sql = "
                                INSERT INTO 
                                rateio (`NOTA_ID`, 
                                `NOTA_TR`,
                                `CENTRO_RESULTADO`,
                                `VALOR`, 
                                `PORCENTAGEM`,
                                IPCG3,IPCG4,
                                ASSINATURA,
                                IPCF2)
                               VALUES (
                               '{$id_subnota}', 
                               '{$id_subnota}', 
                               '{$ur}', 
                               '{$valor_subnota_rateio}',
                               '{$perc}',
                               'IMPOSTOS E TAXAS',
                               '{$ipcg4_subnota}',
                               '{$ass}',
                               'DESEMBOLSOS ADMINISTRATIVOS')";
                    $r = mysql_query($sql);
                    if(!$r){
                        echo "-10";
                        mysql_query("rollback");
                        return;

                    }
                    $somaRateio += $perc;
                    $valorUltimoRateio = $perc;
                    $idTabelaRateio = mysql_insert_id();
                    savelog(mysql_escape_string(addslashes($sql)));
                }
                if($somaRateio > 100){
                    $dif = 100 - $somaRateio;
                    $sql = "update rateio set PORCENTAGEM = PORCENTAGEM + {$dif} where ID = $idTabelaRateio ";
                    $r = mysql_query($sql);
                    if(!$r){
                        echo "-13.1";
                        mysql_query("rollback");
                        return;
                    }
                }else if($somaRateio < 100){
                    $dif = 100 - $somaRateio;
                    $sql = "update rateio set PORCENTAGEM = PORCENTAGEM + {$dif} where ID = $idTabelaRateio ";
                    $r = mysql_query($sql);
                    if(!$r){
                        echo "-13.2";
                        mysql_query("rollback");
                        return;
                    }
                }
            }else{
                ///incluir novas subnotas
                $sql=" INSERT INTO 
            `notas`   
            (USUARIO_ID,
             `codigo`,
             `codNatureza`,
             `codCentroCusto`,
             `valor`,
             `dataEntrada`,
             data_competencia,
             `descricao`,
             `codFornecedor`,
             `tipo`,
             `status`,
             `codSubNatureza`,
             `empresa`,             
             `VAL_PRODUTOS`,             
             `TR`,
             `TIPO_DOCUMENTO`,
             DATA, 
             NOTA_REFERENCIA,
             `natureza_movimentacao`,
             IMPOSTOS_RETIDOS_ID,
             tipo_documento_financeiro_id )             
             VALUES
             ('{$_SESSION['id_user']}',
                 '{$cod_darf_dan}',
                 '{$natureza}',
                 '{$centrocusto}',
                 '{$valor_subnota}',
                 '{$ent}',
                '{$dataCompetencia}',
                 '{$descricao_subnota}',
                 '{$fornecedor_subnota}',
                 '1',
                 'Em aberto',
                 '{$subnatureza}',
                 '{$ur_nota}',                 
                 '{$valor_subnota}',
                 '1',
                 '{$tipo_subnota}',
                 now(),
                 $idnota,
                 $ipcg4_subnota,
                 $impostos_retidos,                 
                 $tipoDocFinId     
                )";
                $r = mysql_query($sql);
                $id_subnota = mysql_insert_id();
                if(!$r){
                    echo "-11";
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
                $tr_subnota= str_pad($id_subnota,6,"0",STR_PAD_LEFT);
                $sql="UPDATE notas set TR= '{$tr_subnota}' where idNotas= '{$id_subnota}'";
                $r = mysql_query($sql);
                if(!$r){
                    echo "-12";
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));

                if(!$r){
                    echo "Erro ao inserir subnota";
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));

                //inserir parcela
                $sql = "INSERT INTO parcelas (`idNota`, `valor`, `vencimento`, `codBarras`, `status`, `empresa`,`TR`,vencimento_real ) 
  			VALUES ( '{$id_subnota}', '{$valor_subnota}', '{$vencimento_subnota}', '{$codb}', 'Pendente', '{$ur_nota}','1', '{$vencimento_real_subnota}')";
                $r = mysql_query($sql);
                if(!$r){
                    echo "-13".$sql;
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
                ////inserir rateio
                $somaRateio = 0;
                foreach($rateio_nota as $rateio){
                    $p = explode("#",str_replace("undefined","",$rateio));
                    $ur = anti_injection($p[0],'numerico');
                    $perc = str_replace(",",".",str_replace(".",",",$p[2]));
                    $ass = anti_injection($p[5],'literal');
                    $valor_subnota_rateio= $valor_subnota*($perc/100);

                    $sql = "INSERT INTO rateio (`NOTA_ID`, `NOTA_TR`, `CENTRO_RESULTADO`, `VALOR`, `PORCENTAGEM`,IPCG3,IPCG4,ASSINATURA,IPCF2)
                    VALUES ( '{$id_subnota}', '{$id_subnota}', '{$ur}', '{$valor_subnota_rateio}', '{$perc}','IMPOSTOS E TAXAS','{$ipcg4_subnota}','{$ass}','DESEMBOLSOS ADMINISTRATIVOS')";
                    $r = mysql_query($sql);
                    if(!$r){
                        echo "-14";
                        mysql_query("rollback");
                        return;
                    }
                    $somaRateio += $perc;
                    $valorUltimoRateio = $perc;
                    $idTabelaRateio = mysql_insert_id();
                    savelog(mysql_escape_string(addslashes($sql)));
                }
                if($somaRateio > 100){
                    $dif = 100 - $somaRateio;
                    $sql = "update rateio set PORCENTAGEM = PORCENTAGEM + {$dif} where ID = $idTabelaRateio ";
                    $r = mysql_query($sql);
                    if(!$r){
                        echo "-13.1";
                        mysql_query("rollback");
                        return;
                    }
                }else if($somaRateio < 100){
                    $dif = 100 - $somaRateio;
                    $sql = "update rateio set PORCENTAGEM = PORCENTAGEM + {$dif} where ID = $idTabelaRateio ";
                    $r = mysql_query($sql);
                    if(!$r){
                        echo "-13.2";
                        mysql_query("rollback");
                        return;
                    }
                }
            }
        }
    }
    
    //////////////////////////////fim editar subbnotas
    mysql_query("commit");
    if($cont>0){
        enviar_email_rateio($ur_rateio,$nota,1);
    }else{
        echo 1;
    }
}

function cancelar_nota($id,$motivo){
  $id = anti_injection($id,'numerico');
  $motivo = anti_injection($motivo,'literal');

  $sql = <<<SQL
SELECT
  COUNT(parcelas.id) AS qtd_aglutinadas
FROM
  notas
  INNER JOIN parcelas ON parcelas.idNota = notas.idNotas
WHERE
  notas.NOTA_REFERENCIA = '{$id}'
  AND (parcelas.nota_id_fatura_aglutinacao IS NOT NULL
  OR parcelas.status = "Processada")
SQL;
  $rs = mysql_query($sql);
  $rowAglutinadas = mysql_fetch_array($rs);
  $qtdAglutinadas = $rowAglutinadas['qtd_aglutinadas'];
   
    if($qtdAglutinadas == 0) {
        mysql_query('begin');
        $sql = "UPDATE 
  notas 
  SET 
  status = 'Cancelada',
  descricao = 'Cancelada: $motivo' ,  
  cancelado_por = '{$_SESSION['id_user']}',
  cancelado_em = now() 
   WHERE 
   idNotas = '$id' OR NOTA_REFERENCIA = '$id'";
        savelog(mysql_escape_string(addslashes($sql)));
        if (!mysql_query($sql)) {
            mysql_query('rollback');
            echo -1;
            return;
        }
        $sql = "UPDATE parcelas SET status = 'Cancelada', obs = 'Cancelada: Cancelamento da nota.' WHERE idNota = '$id'";
        if (!mysql_query($sql)) {
            mysql_query('rollback');
            echo -1;
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));

     
                $sql = "UPDATE 
                parcelas,
                notas
                 SET parcelas.status = 'Cancelada',
                 parcelas.obs = 'Cancelada: Cancelamento da nota.' 
                  WHERE 
                  notas.NOTA_REFERENCIA = '{$id}' and 
                  notas.idNotas = parcelas.idNota ";
                $rs = mysql_query($sql);
                if (!$rs) {
                    echo $sql;
                    mysql_query('rollback');
                    echo -1;
                    return;
                }
          
        savelog(mysql_escape_string(addslashes($sql)));
        mysql_query('commit');
        echo '1';
        return;
    } else {
        echo '2';
        return;
    }

}

function salvar_parcela($nota,$valor,$vencimento,$barras){
  $empresa = $_SESSION["empresa_user"];
  $nota = anti_injection($nota,'numerico'); $valor = anti_injection($valor,'numerico');
  $vencimento = anti_injection($vencimento,'literal'); $barras = anti_injection($barras,'literal');
  $sql = "INSERT INTO parcelas (`idNota`, `valor`, `vencimento`, `codBarras`, `status`, `empresa`) VALUES('{$nota}','{$valor}','{$vencimento}','{$barras}','Pendente', '{$empresa}')";
  if(mysql_query($sql)){
  	savelog(mysql_escape_string(addslashes($sql))); 
  	echo mysql_insert_id();
  }
  else echo -1;
}

function cancelar_parcela($id,$motivo){
  $id = anti_injection($id,'numerico');
  $motivo = anti_injection($motivo,'literal');
  $sql = "UPDATE parcelas SET status = 'Cancelada', obs = 'Cancelada: $motivo' WHERE id = '$id'";
  if(mysql_query($sql)){
  	savelog(mysql_escape_string(addslashes($sql)));
    $sql = "SELECT 
        count(*) as c,
        n.idNotas 
        FROM `notas` as n,
        parcelas as p 
        WHERE 
        n.idNotas = p.idNota AND
        p.status = 'Pendente'
        AND n.idNotas = (SELECT idNota FROM parcelas WHERE id = '$id')";
    $result = mysql_query($sql);
    while($row = mysql_fetch_array($result)){
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
	      if($row['c'] == 0){
			$sql = "UPDATE notas SET status = 'Processada' WHERE idNotas = '{$row['idNotas']}'";
			mysql_query($sql);
	      }
    }
    echo 1;
  }
  else echo -1;
}

function processar_parcela($id,$origem,$aplicacao,$vencimento,$valor,$obs,
                           $juros,$desconto,$data,$outros,$valor_pago,$num_documento,
                           $issqn,$ir,$pis,$cofins,$csll,$tipo_nota,$usuario,
                           $encargos, $motivoJuros,$vencimento_real, $tarifaCartao){
	mysql_query('begin');
	$id = anti_injection($id,'numerico');
	$origem = anti_injection($origem,'numerico');
	$aplicacao = anti_injection($aplicacao,'numerico');
    $novo_vencimento = anti_injection($vencimento,'literal');
    $novo_vencimento = \DateTime::createFromFormat('d/m/Y', $novo_vencimento)->format('Y-m-d');
    $vencimentoReal = anti_injection($vencimento_real,'literal');
    $vencimentoReal =  \DateTime::createFromFormat('d/m/Y', $vencimentoReal)->format('Y-m-d');
	$valor = anti_injection($valor,'numerico');
	$obs = anti_injection($obs,'literal');
	$data = anti_injection($data,'literal');
    $fechamento_caixa = new \DateTime(\App\Controllers\FechamentoCaixa::getFechamentoCaixa()['data_fechamento']);
    $data_pag = implode("-",array_reverse(explode("/",$data)));
    $data_pag_obj = new \DateTime($data_pag);
    if($data_pag_obj <= $fechamento_caixa) {
        echo "erro fechamento";
        mysql_query("rollback");
        return;
    }
	$juros = anti_injection($juros,'numerico');
	$desconto = anti_injection($desconto,'numerico');
	$encargos = anti_injection($encargos,'numerico');
    $motivoJuros = anti_injection($motivoJuros,'literal');
    $tarifaCartao =  anti_injection($tarifaCartao,'numerico');

	$sql = "SELECT *,valor, idNota FROM parcelas WHERE id = '$id'";
	$result = mysql_query($sql);
	$valor_orginal = 0;
	$idNota = 0;
	$usuario = $_SESSION["id_user"];
	while($row = mysql_fetch_array($result))
	{
		$valor_orginal = $row['valor'];
		$idNota = $row['idNota'];
		$vencimento= $row['vencimento'];
		$codbarra = $row['codBarras'];
		$empresa = $row['empresa'];

	}
	$valor1= $valor_orginal-$valor;

	$status = "Processada";
	///Jeferson se a nota for a receber
	if($tipo_nota == 0){
		///se o valor recebido for menor do que o valor da parcela
		if($valor_orginal > $valor){
			$status = "Processada";
			$obs="Pagou parcialmente.";
			$sql_up = "UPDATE
                parcelas
                SET CONCILIADO= 'N',
                status = '$status',
                obs = '$obs',
                valor='$valor',
                vencimento='{$novo_vencimento}',
                VALOR_PAGO='$valor_pago',
                origem = '$origem',
                aplicacao = '$aplicacao',
                JurosMulta = '$juros',
                desconto = '$desconto',
                encargos = '$encargos',
                DATA_PAGAMENTO='$data',
                OUTROS_ACRESCIMOS='$outros',
                NUM_DOCUMENTO='$num_documento',
                ISSQN='{$issqn}',
                PIS ='{$pis}',
                COFINS='{$cofins}',
                CSLL='{$csll}',
                IR='{$ir}',
                USUARIO_ID = '{$usuario}',
                DATA_PROCESSADO = now() ,
                motivo_juros_id = '{$motivoJuros}',
                vencimento_real = '{$vencimentoReal}',
                tarifa_cartao = '{$tarifaCartao}'
                WHERE id = '$id'";
            $rs_up = mysql_query($sql_up);
            if(!$rs_up){
                echo "erro1";
                mysql_error("rollback");
            }
            savelog(mysql_escape_string(addslashes($sql)));
			$sql="SELECT MAX( TR ) as tr fROM parcelas WHERE idNota ={$idNota} and status <>'Cancelada'";
			//echo $sql;
			$result= mysql_query($sql);
			$row = mysql_fetch_array($result);
			$tr= $row['tr'] +1;
			if(!mysql_query($sql)){
				echo "erro2";
				mysql_error("rollback");
			}
			savelog(mysql_escape_string(addslashes($sql)));
			$sql = "INSERT INTO parcelas (`idNota`, `valor`, `vencimento`, `codBarras`, `status`, `empresa`,`TR`,vencimento_real) VALUES('{$idNota}','{$valor1}','{$novo_vencimento}','{$codbarra}','Pendente', '{$empresa}','{$tr}', '{$vencimentoReal}')";
			if(!mysql_query($sql)){
				echo "erro3";
				mysql_error("rollback");
			}
			savelog(mysql_escape_string(addslashes($sql)));


		}else{
			$sql_up = "UPDATE parcelas
            SET CONCILIADO= 'N',
            status = '$status',
            obs = '$obs',
            valor='$valor',
            vencimento='{$novo_vencimento}',
            VALOR_PAGO='$valor_pago',
            origem = '$origem',
            aplicacao = '$aplicacao',JurosMulta = '$juros',
            desconto = '$desconto', encargos = '$encargos',
            DATA_PAGAMENTO='$data',OUTROS_ACRESCIMOS='$outros',
            NUM_DOCUMENTO='$num_documento', ISSQN='{$issqn}', PIS ='{$pis}',
            COFINS='{$cofins}', CSLL='{$csll}', IR='{$ir}', USUARIO_ID = '{$usuario}',
            DATA_PROCESSADO = now(),
            motivo_juros_id = '{$motivoJuros}',
            vencimento_real = '{$vencimentoReal}',
            tarifa_cartao = '{$tarifaCartao}'
            WHERE id = '$id'";
            $rs_up = mysql_query($sql_up);
		}
	}else{


		if($valor_pago < $valor_orginal && $desconto == '0.00' && $juros =='0.00'){

			$status = "Processada";
			$obs="Pagou parcialmente.";
			$sql_up = "UPDATE 
            parcelas
             SET 
             CONCILIADO= 'N', 
             status = '$status', 
             obs = '$obs',
             valor='$valor',
             vencimento='{$novo_vencimento}',
              VALOR_PAGO='$valor_pago', 
             origem = '$origem',
              aplicacao = '$aplicacao',
              JurosMulta = '$juros',
               desconto = '$desconto',
               encargos = '$encargos',
               DATA_PAGAMENTO='$data',
              OUTROS_ACRESCIMOS='$outros', 
              NUM_DOCUMENTO='$num_documento', 
              USUARIO_ID = '{$usuario}',
               DATA_PROCESSADO = now(),
               motivo_juros_id = '{$motivoJuros}',
               vencimento_real='{$vencimentoReal}',
               tarifa_cartao = '{$tarifaCartao}'
               WHERE
                id = '$id'";
            $rs_up = mysql_query($sql_up);
            if(!$rs_up){
				echo "erro4 " . mysql_error() . $sql_up;
				mysql_error("rollback");

			}
			savelog(mysql_escape_string(addslashes($sql)));
			$sql="SELECT MAX( TR ) as tr fROM parcelas WHERE idNota ={$idNota} and status <>'Cancelada'";
			//echo $sql;
			$result= mysql_query($sql);
			$row = mysql_fetch_array($result);
			$tr= $row['tr'] +1;
			if(!mysql_query($sql)){
				echo "erro5";
				mysql_error("rollback");
			}
			savelog(mysql_escape_string(addslashes($sql)));


			$sql = "INSERT INTO parcelas (`idNota`, `valor`, `vencimento`, `codBarras`, `status`, `empresa`,`TR`,vencimento_real) 
            VALUES
            ('{$idNota}','{$valor1}','{$novo_vencimento}','{$codbarra}','Pendente', '{$empresa}','{$tr}','{$vencimentoReal}')";
			if(!mysql_query($sql)){
				echo "erro6";
				mysql_error("rollback");
			}
			savelog(mysql_escape_string(addslashes($sql)));


		}else{

			$sql_up = "UPDATE parcelas
             SET 
             CONCILIADO= 'N', 
             status = '$status',
              obs = '$obs', 
              origem = '$origem',
               aplicacao = '$aplicacao',
               vencimento='{$novo_vencimento}',
               JurosMulta = '$juros', 
               VALOR_PAGO='$valor_pago',
               desconto = '$desconto',
                encargos = '$encargos',
                 DATA_PAGAMENTO='$data',
                 OUTROS_ACRESCIMOS='$outros', 
                 NUM_DOCUMENTO='$num_documento', 
                 USUARIO_ID = '{$usuario}', 
                 DATA_PROCESSADO = now(),
                 motivo_juros_id = '{$motivoJuros}',
                 vencimento_real = '{$vencimentoReal}',
                 tarifa_cartao = '{$tarifaCartao}'
                  WHERE id = '$id'";
            $rs_up = mysql_query($sql_up);
		}
    }
    
	if($rs_up){
		savelog(mysql_escape_string(addslashes($sql)));
		$sql = "SELECT
        count(*) as c,
        n.idNotas 
        FROM 
        `notas` as n,
        parcelas as p 
        WHERE 
        n.idNotas = p.idNota AND
        p.status = 'Pendente' 
        AND n.idNotas = (SELECT idNota FROM parcelas WHERE id = '$id')";
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result)){
			foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
			if($row['c'] == 0){
				$sql1 = "UPDATE notas SET status = 'Processada' WHERE idNotas = '{$row['idNotas']}'";
				//mysql_query($sql1);
				if(!mysql_query($sql1)){
					echo "erro7";
					mysql_error("rollback");
				}
				savelog(mysql_escape_string(addslashes($sql)));
			}else{
				$sql2 = "UPDATE notas SET status = 'Pendente' WHERE idNotas = '{$row['idNotas']}'";
				if(!mysql_query($sql2)){
					echo "erro8";
					mysql_error("rollback");
				}
				savelog(mysql_escape_string(addslashes($sql)));}
		}
		$sql3 = "UPDATE remocoes SET tipo = 'Processada' WHERE idNota = '$idNota'";
		if(!mysql_query($sql3)){
			echo "erro9";
			mysql_error("rollback");
		}
		savelog(mysql_escape_string(addslashes($sql)));


	}else {
		echo "erro10";
        mysql_error("rollback");
        return;
	}
	mysql_query('commit');
	echo 2;
}

function salvar_comprovante($id,$file,$nota){
	$arquivo = $file["tmp_name"];
	$tamanho = $file["size"];
	$tipo = $file["type"];
	$nome = $file["name"];

	if ( $arquivo != "none" ){
		$fp = fopen($arquivo, "rb");
		$conteudo = fread($fp, $tamanho);
		$conteudo = addslashes($conteudo);
		fclose($fp);

		$sql = "INSERT INTO comprovanteparcelas (`idParcela`, `nome`, `tipo`, `size`, `content`) VALUES ('$id', '$nome', '$tipo', '$tamanho', '$conteudo')";
		mysql_query($sql);
		savelog(mysql_escape_string(addslashes("INSERT INTO comprovanteparcelas (`idParcela`, `nome`, `tipo`, `size`, `content`) VALUES ('$id', '$nome', '$tipo', '$tamanho', 'conteudo')")));
		header("Location: /financeiro/?op=notas&act=detalhes&idNotas=$nota");
//       echo "Comprovante salvo com sucesso. <a href=javascript:history.go(-1)>voltar</a>";
//     else
//       echo "Erro ao salvar comprovante, tente mais tarde. <a href=javascript:history.go(-1)>voltar</a>";
	}
	/* else
			echo "Não foi possível carregar o arquivo para o servidor, tente mais tarde. <a href=javascript:history.go(-1)>voltar</a>";*/
}

function show_comprovante($id){
  $result = mysql_query("SELECT tipo, content, nome FROM comprovanteparcelas WHERE id='$id' ");
  $row = mysql_fetch_array($result);
//  echo mysql_error();
  header("Content-type: {$row['tipo']}");
  header("Content-Disposition: attachment; filename={$row['nome']};" ); 
  print $row['content'];
}

function buscar($post) {
    ini_set('memory_limit', '512M');
    //ini_set('display_errors', 1);

    $dados = $post;
    extract($post, EXTR_OVERWRITE);
    $condTipoDoc = '';

    $btipo = anti_injection($btipo, 'literal');
    if ($codigo == '' && $numero_nota == '') {
        if($select_tipo_doc_financeiro != ''){
            $condTipoDoc = "  AND n.tipo_documento_financeiro_id = {$select_tipo_doc_financeiro} ";
        }
        if ($ipcg4_nota != '') {
            $rinner = " INNER JOIN rateio r ON r.NOTA_ID = n.idNotas";
            $condipcg4 = " AND (r.IPCG4 ='{$ipcg4_nota}' OR n.natureza_movimentacao = {$ipcg4_nota})";
        }
        if ($btipo != "t") {

            $condtip = " AND n.tipo = '$btipo' ";
        } else {
            $condtip = " AND n.tipo in(0,1) ";
        }
        if ($status != "" && !empty($status)) {
            $condst = " AND UPPER(p.status) = UPPER('{$status}') ";
        }
        if (!empty($codFornecedor)) {
            $condfor = " AND codFornecedor = '$codFornecedor' ";
        }
        if ($assinatura != '') {
            $rinner = " INNER JOIN rateio r ON r.NOTA_ID = n.idNotas";
            $condass = " AND r.ASSINATURA = '$assinatura' ";
        }

        $condFormaPagamento = '';        
        if($select_forma_pagamento != ''){
            $condFormaPagamento = " AND n.forma_pagamento = '{$select_forma_pagamento}'";
        }

        //buscando pela descricao da nota --Adicionado em 06/10/2014

        if ($descricao_nota != '') {
            $conddesc = " AND descricao LIKE '%{$descricao_nota}%'";
        }

        $valor = str_replace('.', '', $valor);
        $valor = str_replace(',', '.', $valor);
        if ($valor != '') {
            $condvalor = " AND p.valor = '{$valor}'";
        }

        if ($competenciaInicio != '') {
            $i = implode("-", array_reverse(explode("/", $competenciaInicio)));
            $condCompetenciaInicio = "AND n.data_competencia >= '{$i}'";
        }
        if ($competenciaFim != '') {
            $f = implode("-", array_reverse(explode("/", $competenciaFim)));
            $condCompetenciaFim = "AND n.data_competencia <= '{$f}'";
        }

        if ($inicio != '') {
            $i = implode("-", array_reverse(explode("/", $inicio)));
            $condini = " AND p.vencimento_real >= '{$i}'";
        }
        if ($inicioE != '') {
            $i = implode("-", array_reverse(explode("/", $inicioE)));
            $condiniE = " AND n.dataEntrada >= '{$i}'";
        }
        if ($inicioPagamento != '' && !empty($inicioPagamento)) {
            $i = implode("-", array_reverse(explode("/", $inicioPagamento)));
            $condiniPagamento = " AND p.DATA_PAGAMENTO >= '{$i}'";
        }
        if ($fim != '') {
            $f = implode("-", array_reverse(explode("/", $fim)));
            $condfim = " AND p.vencimento_real <= '{$f}'";
        }
        if ($fimE != '') {
            $f = implode("-", array_reverse(explode("/", $fimE)));

            $condfimE = " AND n.dataEntrada <= '{$f}'";
        }
        if ($fimPagamento != '') {
            $f = implode("-", array_reverse(explode("/", $fimPagamento)));
            $condFimPagamento = " AND p.DATA_PAGAMENTO <= '{$f}'";
        }
        $condemp ='';
        if(!empty($empresa)){
            $condemp  = " AND n.empresa = $empresa";
        }
        $condcod = '';
    } else if ($codigo != '') {
        $condcod = " AND n.idNotas = '{$codigo}'";
    } else if ($numero_nota != '' && $codigo == '') {
        $condnumero_nota = " AND n.codigo = '{$numero_nota}'";
    }

    $condur = " AND n.empresa in {$_SESSION['empresa_user']}";

// paginação de parcelas
    if(!isset($pg)) $pg = 1;
    if(!isset($offset)) $offset = 100;

    $ant = (int) $pg -1;
    $prox = (int) $pg + 1;
    $start = ($pg -1)*$offset;
    $sqlGeral = "select 
                n.*,
                {$r}
                p.*,
                p.DATA_PAGAMENTO as data_pagamento,
                f.razaoSocial,
                e.nome as ur,
                p.status as pst,
                p.vencimento as pven, 
                p.valor as pval,
                p.vencimento_real as parcela_vencimento_real,
                ap.forma
            from 
                notas as n inner join 
                fornecedores as f on (f.idFornecedores = n.codFornecedor) inner join
                empresas as e on (n.empresa = e.id) inner join 
                parcelas as p on (n.idNotas = p.idNota)
                left join aplicacaofundos as ap on (n.forma_pagamento = ap.id)
                {$rinner} 
                
            where
            p.status <> 'CANCELADA' 
            and n.inserida_caixa_fixo <> 'S'
                $condcod 
                $condnumero_nota 
                $condtip 
                $condass 
                $condemp
                $condfim
                $condfimE 
                $condfor
                $condini
                $condiniE
                $condipcg2
                $condipcg3
                $condipcg4 
                $condst
                $condvalor
                $condiniPagamento
                $condFimPagamento 
                $condur
                $conddesc
                $condTipoDoc
                $condCompetenciaInicio
                $condCompetenciaFim
                $condFormaPagamento
            Group by 
                idNOTAS,p.id,p.tr
            order by 
                pven, razaoSocial ASC";

    $resultGeral = mysql_query($sqlGeral);
    $quantidadeTotalRegistros = mysql_num_rows($resultGeral);
    $totalPages = ceil($quantidadeTotalRegistros/100);
    $valorGeral = 0;

        while ($row = mysql_fetch_array($resultGeral)) {

        if ($row['tipo'] != '1') {
            ///se parcela j� for processada pega valor pago caso contrario pega valor
            if ($row['status'] == 'Processada') {
                $valorGeral += $row['VALOR_PAGO'];
            } else {
                if ($row['status'] != 'Cancelada') {
                    $valorGeral += $row['pval'];
                }
            }
        } else {
            if ($row['status'] == 'Processada') {
                $valorGeral -= $row['VALOR_PAGO'];
            } else {
                if ($row['status'] != 'Cancelada') {
                    $valorGeral -= $row['pval'];
                }
            }
        }
    }
    if($totalPages < 6) {
        $pages = range(1, $totalPages);
    } else {
        $limit = 2;
        $currentPageNumber = $pg;
        $firstPageNumber = ($currentPageNumber - $limit) > 1 ? $currentPageNumber - $limit : 1;
        $lastPageNumber = ($currentPageNumber + $limit) < $totalPages ? $currentPageNumber + $limit : $totalPages;
        $pages = range($firstPageNumber, $lastPageNumber);
    }

    $sqlFiltrado = "select 
                n.*,
                {$r}
                p.*,
                p.DATA_PAGAMENTO as data_pagamento,
                f.razaoSocial,
                e.nome as ur,
                p.status as pst,
                p.vencimento as pven, 
                p.valor as pval,
                p.vencimento_real as parcela_vencimento_real,
                ap.forma
            from 
                notas as n inner join 
                fornecedores as f on (f.idFornecedores = n.codFornecedor) inner join
                empresas as e on (n.empresa = e.id) inner join 
                parcelas as p on (n.idNotas = p.idNota)
                left join aplicacaofundos as ap on (n.forma_pagamento = ap.id)
                {$rinner} 
                $condur
            where
                p.status <> 'CANCELADA' 
                and n.inserida_caixa_fixo <> 'S'
                $condcod 
                $condnumero_nota 
                $condtip 
                $condass 
                $condemp
                $condfim
                $condfimE 
                $condfor
                $condini
                $condiniE
                $condipcg2
                $condipcg3
                $condipcg4 
                $condst
                $condvalor
                $condiniPagamento
                $condFimPagamento 
                $condur2
                $conddesc
                $condTipoDoc
                $condCompetenciaInicio
                $condCompetenciaFim
                $condFormaPagamento
            Group by 
                idNOTAS,p.id,p.tr
            order by 
                pven, razaoSocial ASC LIMIT {$start},{$offset}";
    $resultFiltrado = mysql_query($sqlFiltrado) or die (mysql_error());
    $quantidadeRegistrosNaPagina = mysql_num_rows($resultFiltrado);

    $i = 0;
    $idn = 0;
    $idnp = 0;
    if($quantidadeRegistrosNaPagina == 0){
        $t .= "<div class='row'>
        <span><b> Nenhum registro encontrado.</b></span>    
    </div>";

    }else{
        $t .= "<div class='row'>
                <div class='col-xs-12 text-right' style='text-align: right;'>
                    <span> 
                        <b>Mostrando {$quantidadeRegistrosNaPagina} de {$quantidadeTotalRegistros} registro(s) encontrado(s).</b>
                    </span>
                    <span>
                        <button target='_blank' class='btn btn-default imprimir_parcelas' type='button' role='button' aria-disabled='false'>
                            <i class='fa fa-file-excel-o' aria-hidden='true'></i> 
                            Exportar</span>
                        </button>
                    </span>                   
                </div>      
            </div>";
        if ($totalPages > 1) {
            $t .= "<div class='row'>";

            if ($pg <> 1) {
                $t .= "<span  class='paginacao-parcela' style='cursor: pointer;'
             dados='query=buscar&assinatura={$assinatura}&btipo={$btipo}&codFornecedor={$codFornecedor}&codigo={$codigo}&descricao_nota={$descricao_nota}";
                $t .= "&empresa={$empresa}&fim={$fim}&fimE={$fimE}&fimPagamento={$fimPagamento}&inicio={$inicio}&inicioE={$inicioE}&inicioPagamento={$inicioPagamento}";
                $t .= "&ipcf2_busca={$ipcf2_busca}&ipcg3_busca={$ipcg3_busca}&ipcg4_busca={$ipcg4_busca}&numero_nota={$numero_nota}&status={$status}";
                $t .= "&valor={$valor}&pg={$ant}&last={$totalPages}&tipo_documento_financeiro_id={$condTipoDoc}&competenciaFim={$competenciaFim}&competenciaInicio={$competenciaInicio}&select_forma_pagamento={$condFormaPagamento}' ><a>Anterior</a> | </span>";
            }

            foreach ($pages as $page) {
                if ($pg != $page) {
                    $t .= "<span class='paginacao-parcela' style='cursor: pointer;'";
                    $t .= "dados='query=buscar&assinatura={$assinatura}&btipo={$btipo}&codFornecedor={$codFornecedor}&codigo={$codigo}&descricao_nota={$descricao_nota}";
                    $t .= "&empresa={$empresa}&fim={$fim}&fimE={$fimE}&fimPagamento={$fimPagamento}&inicio={$inicio}&inicioE={$inicioE}&inicioPagamento={$inicioPagamento}";
                    $t .= "&ipcf2_busca={$ipcf2_busca}&ipcg3_busca={$ipcg3_busca}&ipcg4_busca={$ipcg4_busca}&numero_nota={$numero_nota}&status={$status}";
                    $t .= "&valor={$valor}&pg={$page}&last={$totalPages}&tipo_documento_financeiro_id={$condTipoDoc}&competenciaFim={$competenciaFim}&competenciaInicio={$competenciaInicio}&select_forma_pagamento={$condFormaPagamento}' 
                ><a>{$page}</a> | </span>";
                } else {
                    $t .= "<span class='active'>{$page} | </span>";
                }
            }

            if ($pg != $totalPages) {
                $t .= "<span><a class='paginacao-parcela' style='cursor: pointer;'
            dados='query=buscar&assinatura={$assinatura}&btipo={$btipo}&codFornecedor={$codFornecedor}&codigo={$codigo}&descricao_nota={$descricao_nota}";
                $t .= "&empresa={$empresa}&fim={$fim}&fimE={$fimE}&fimPagamento={$fimPagamento}&inicio={$inicio}&inicioE={$inicioE}&inicioPagamento={$inicioPagamento}";
                $t .= "&ipcf2_busca={$ipcf2_busca}&ipcg3_busca={$ipcg3_busca}&ipcg4_busca={$ipcg4_busca}&numero_nota={$numero_nota}&status={$status}";
                $t .= "&valor={$valor}&pg={$prox}&last={$totalPages}&tipo_documento_financeiro_id={$condTipoDoc}&competenciaFim={$competenciaFim}&competenciaInicio={$competenciaInicio}&select_forma_pagamento={$condFormaPagamento}' 
            >Próxima</a></span>";
            }

            $t .= "</div>";
        }

        $t .= "</br>
        <table id='tb_list' style='font-size:12px' class='table table-bordered table-condensed table-hover' width=100% >
        <thead>
        <tr>
        <th></th>
        <th>COMPETÊNCIA</th>
		<th>DATA PGTO</th>
        <th>VENC.</th>
        <th>VENC. REAL</th>
        <th>FORNECEDOR</th>
        <th>TIPO DOC</th>
		<th>UR</th>
        <th>STATUS</th>
        <th>COMP.</th>
        <th>BORDERO</th>
        <th>TR</th>
        <th>COD NOTA</th>
		<th>VALOR</th>
		<th></th>
		</tr></thead>";
        $total = 0;

        while ($row = mysql_fetch_array($resultFiltrado)) {
            $parcelaBordero = 'Não';
            $parcelaResponseBordero =  Bordero::getAllParcelasBordero($row['id']);

            if(!empty($parcelaResponseBordero)){
                foreach ($parcelaResponseBordero as $parcela_bordero) {
                    $resposneBordero = current(Bordero::getBorderoById($parcela_bordero['bordero_id']));
                    if ($resposneBordero['ativo'] == 's') {
                        $parcelaBordero = $parcela_bordero['bordero_id'];
                    }
                }
            }
         
            $condcancelada = '';
            if($row['status'] != 'Cancelada'){
                $condcancelada = $row['nota_adiantamento']  <> 'S' ? "?op=notas&act=detalhes&idNotas={$row['idNotas']}" :
                    "/financeiros/contas-antecipadas/?action=visualizar-conta-antecipada&id={$row['idNotas']}";

            }
            if ($row['tipo'] != '1') {
                ///se parcela j� for processada pega valor pago caso contrario pega valor
                if ($row['status'] == 'Processada') {
                    $condval = "<td>{$row['idNotas']}-{$row['TR']}</td>
                    <td>{$row['codigo']}</td>
                    <td>R$ " . number_format($row['VALOR_PAGO'], 2, ',', '.') . "</td>";
                    $total += $row['VALOR_PAGO'];
                } else {
                    $condval = "<td>{$row['idNotas']}-{$row['TR']}</td>
                    <td>{$row['codigo']}</td>
                    <td>R$ " . number_format($row['pval'], 2, ',', '.') . "</td>";
                    if ($row['status'] != 'Cancelada') {
                        $total += $row['pval'];
                    }
                }
            } else {
                if ($row['status'] == 'Processada') {
                    $condval = "<td>{$row['idNotas']}-{$row['TR']}</td>
                    <td>{$row['codigo']}</td>
                    <td class='valor-pagto' style='font-size:12px; color:red;'>R$ - " . number_format($row['VALOR_PAGO'], 2, ',', '.') . "</td>";
                    $total -= $row['VALOR_PAGO'];
                } else {
                    $condval = "<td>{$row['idNotas']}-{$row['TR']}</td>
                    <td>{$row['codigo']}</td>
                    <td class='valor-pagto' style='font-size:12px; color:red;'>R$ - " . number_format(($row['pval'] + $row['JurosMulta'] + $row['OUTROS_ACRESCIMOS']) - $row['desconto'], 2, ',', '.') . "</td>";
                    if ($row['status'] != 'Cancelada') {
                        $total -= $row['pval'];
                    }
                }
            }

            if ($i++ % 2 == 0) {
                $cor = "#E9F4F8";
            } else {
                $cor = "";
            }
            $data_pgto = implode("/", array_reverse(explode("-", $row['DATA_PAGAMENTO'])));
            if ($data_pgto == '00/00/0000') {
                $data_pgto = "";
            }
            $compensada = 'Não';
            if($row['compensada'] <> 'NAO'){
                $compensada = $row['compensada'];
                if($row['VALOR_PAGO'] == 0 && $row['status'] == 'Processada' ){
                    $condval = "<td>{$row['idNotas']}-{$row['TR']}</td>
                                <td>{$row['codigo']}</td>
                                <td style='font-size:12px; color:red;'>R$ - " . number_format($row['valor_compensada'], 2, ',', '.') . "</td>";
                }
            }
            $data_vencimento_real = '';
            if(!empty($row['parcela_vencimento_real'])) {
                $data_vencimento_real = \DateTime::createFromFormat('Y-m-d', $row['parcela_vencimento_real'])->format('d/m/Y');
            }
            $estornarOuDesfazer = "";
            if(($row['CONCILIADA'] == 'N' || $row['CONCILIADA'] == '') && $row['nota_id_fatura_aglutinacao'] == '') {
                $valorEstorno = $row['pst'] == 'Pendente' ? ($row['pval'] + $row['JurosMulta'] + $row['OUTROS_ACRESCIMOS']) - $row['desconto'] : $row['VALOR_PAGO'];
                $valorEstorno = number_format($valorEstorno, 2, ',', '.');
                $estornarOuDesfazer = <<<HTML
<li>
    <a href='#' class='estornar-parcela' valor-original-parcela='{$row['pval']}' valor-estorno='{$valorEstorno}' parcela-id='{$row['id']}'>
        <i class='fa fa-retweet'></i>
        Gerar Hist. de Estorno
    </a>
</li>
HTML;
            }
            $badgeStatus = "info";
            if($row['pst'] == 'Processada') {
                $badgeStatus = "primary";
            } elseif($row['pst'] == 'Pendente') {
                $badgeStatus = "warning";
            }

            $faIcon = '';
                if($row['forma_pagamento'] == 1){
                    $faIcon = 'fa-barcode';
                }elseif($row['forma_pagamento'] == 7){
                    $faIcon = 'fa-credit-card';
                }elseif($row['forma_pagamento'] == 5){
                    $faIcon = 'fa-exchange';
                }elseif($row['forma_pagamento'] == 4){
                    $faIcon = 'fa-money';
                }elseif($row['forma_pagamento'] == 6){
                    $faIcon = 'fa-check-circle-o';
                }elseif($row['forma_pagamento'] == 9){
                    $faIcon = 'fa-list';
                }elseif($row['forma_pagamento'] == 10){
                    $faIcon = 'pix';
                }

            $data_competencia = implode("/", array_reverse(explode("-", $row['data_competencia'])));
            $t .= "<tr style='font-size:11px;'  id='parcela-{$row['id']}' bgcolor='{$cor}'>
            <td>
                <i style='font-size:20px !important' class='fa {$faIcon}' title='Forma de pagamento: {$row['forma']}'> </i> 
                
            </td>
            <td>{$data_competencia}</td>
                             <td class='data-pagto'>{$data_pgto}</td>
                             <td>" . implode("/", array_reverse(explode("-", $row['pven']))) . "</td>
                             <td>{$data_vencimento_real}</td>
                             <td>{$row['razaoSocial']}</td>
                             <td>{$row['TIPO_DOCUMENTO']}</td>
                             <td>{$row['ur']}</td>
                             <td class='status-parcela'><span class='label label-{$badgeStatus}'>{$row['pst']}</span></td>
                             <td>{$compensada}</td>
                             <td class='tem-bordero-parcela'>{$parcelaBordero}</td>
                             {$condval}
                             <td class='text-center' style='font-size: 16px;'>
                                <div class='dropdown'>
                                    <button class='btn btn-default dropdown-toggle' type='button' data-toggle='dropdown'>
                                        Opções
                                        <span class='caret'></span>
                                    </button>
                                    <ul class='dropdown-menu dropdown-menu-right'>
                                        <li>
                                            <a target='_blank' href='{$condcancelada}'>
                                                <i class='fa fa-list'></i>
                                                Visualizar/Processar
                                            </a>
                                        </li>
                                        {$estornarOuDesfazer}
                                    </ul>
                                </div>
                             </td>
                          </tr>";
        }
        /*$t .= "<tr>
                   <td colspan = '5' valign='top' style='font-size:12px;text-align: right;'>
                      <b> TOTAL DA PÁGINA R$ " . number_format($total, 2, ',', '.') . " </b>
                   </td>
                   <td valign='top' style='font-size:12px;' colspan='3'>
                      <b> TOTAL GERAL R$ " . number_format($valorGeral, 2, ',', '.') . "</b>
                  </td>
             </tr>";*/

        $t .= "</table>";
        if ($totalPages > 1) {
            $t .= "<br><div class='row'>";
            if ($pg <> 1) {
                $t .= "<span  class='paginacao-parcela' style='cursor: pointer;'
                 dados='query=buscar&assinatura={$assinatura}&btipo={$btipo}&codFornecedor={$codFornecedor}&codigo={$codigo}&descricao_nota={$descricao_nota}";
                $t .= "&empresa={$empresa}&fim={$fim}&fimE={$fimE}&fimPagamento={$fimPagamento}&inicio={$inicio}&inicioE={$inicioE}&inicioPagamento={$inicioPagamento}";
                $t .= "&ipcf2_busca={$ipcf2_busca}&ipcg3_busca={$ipcg3_busca}&ipcg4_busca={$ipcg4_busca}&numero_nota={$numero_nota}&status={$status}";
                $t .= "&valor={$valor}&pg={$ant}&last={$totalPages}'&select_tipo_doc_financeiro={$condTipoDoc}} ><a>Anterior</a> | </span>";
            }

            foreach ($pages as $page) {
                if ($pg != $page) {
                    $t .= "<span class='paginacao-parcela' style='cursor: pointer;'";
                    $t .= "dados='query=buscar&assinatura={$assinatura}&btipo={$btipo}&codFornecedor={$codFornecedor}&codigo={$codigo}&descricao_nota={$descricao_nota}";
                    $t .= "&empresa={$empresa}&fim={$fim}&fimE={$fimE}&fimPagamento={$fimPagamento}&inicio={$inicio}&inicioE={$inicioE}&inicioPagamento={$inicioPagamento}";
                    $t .= "&ipcf2_busca={$ipcf2_busca}&ipcg3_busca={$ipcg3_busca}&ipcg4_busca={$ipcg4_busca}&numero_nota={$numero_nota}&status={$status}";
                    $t .= "&valor={$valor}&pg={$page}&last={$totalPages}&tipo_documento_financeiro_id={$condTipoDoc}' 
                    ><a>{$page}</a> | </span>";
                } else {
                    $t .= "<span class='active'>{$page} | </span>";
                }
            }

            if ($pg != $totalPages) {
                $t .= "<span><a class='paginacao-parcela' style='cursor: pointer;'
                dados='query=buscar&assinatura={$assinatura}&btipo={$btipo}&codFornecedor={$codFornecedor}&codigo={$codigo}&descricao_nota={$descricao_nota}";
                $t .= "&empresa={$empresa}&fim={$fim}&fimE={$fimE}&fimPagamento={$fimPagamento}&inicio={$inicio}&inicioE={$inicioE}&inicioPagamento={$inicioPagamento}";
                $t .= "&ipcf2_busca={$ipcf2_busca}&ipcg3_busca={$ipcg3_busca}&ipcg4_busca={$ipcg4_busca}&numero_nota={$numero_nota}&status={$status}";
                $t .= "&valor={$valor}&pg={$prox}&last={$totalPages}&tipo_documento_financeiro_id={$condTipoDoc}' 
                >Próxima</a></span>";
            }

            $t .= "</div><br>";
        }


        $t.= "<div class='col-xs-12 text-right' style='text-align: right;'>
                    <button target='_blank' class='btn btn-default imprimir_parcelas' type='button' role='button' aria-disabled='false'>
                        <span class=''>
                            <i class='fa fa-file-excel-o' aria-hidden='true'></i> Exportar
                        </span>
                    </button>
                </div><br><br>";
    }

    print_r($t);
}

function buscar_nota($post) {

    ini_set('memory_limit', '512M');
    extract($post, EXTR_OVERWRITE);
    $valor = str_replace('.', '', $valor);
    $valor = str_replace(',', '.', $valor);    

    $btipo = anti_injection($btipo, 'literal');

    if ($codigo == '' && $numero_nota == '') {

        if ($btipo != "t") {

            $condtip = " AND n.tipo = '$btipo' ";
            if ($tipcg3 != "true" && $ipcg3_busca != '') {
                $condipcg3 = "AND r.IPCG3 like'$ipcg3_busca' ";
                if ($tipcg4 != "true" && $ipcg4_busca != '') {
                    $condipcg4 = "AND r.IPCG4 ='$ipcg4_busca' ";
                }
            }
        } else {
            $condtip = " AND n.tipo in(0,1) ";
        }

        //buscando pela descricao da nota --Adicionado em 06/10/2014
        if ($descricao_nota !== '') {
            $conddesc = "AND n.descricao LIKE '%{$descricao_nota}%'";
        }

        $condFormaPagamento = '';        
        if($select_forma_pagamento != ''){
            $condFormaPagamento = " AND n.forma_pagamento = '{$select_forma_pagamento}'";
        }

        if($select_tipo_doc_financeiro != ''){
            $condTipoDoc = "  AND n.tipo_documento_financeiro_id = {$select_tipo_doc_financeiro} ";
        }

        if (!empty($codFornecedor)) {
            $condfor = "AND codFornecedor = '$codFornecedor' ";
        }

        if ($valor != '') {
            $condvalor = "AND n.valor = '{$valor}'";
        }

        if ($inicioVencimentoReal != '') {
            $i = implode("-", array_reverse(explode("/", $inicioVencimentoReal)));
            $condInicioVencReal = "AND p.vencimento_real >= '{$i}'";
        }

        if ($competenciaInicio != '') {
            $i = implode("-", array_reverse(explode("/", $competenciaInicio)));
            $condCompetenciaInicio = "AND n.data_competencia >= '{$i}'";
        }
        if ($competenciaFim != '') {
            $f = implode("-", array_reverse(explode("/", $competenciaFim)));
            $condCompetenciaFim = "AND n.data_competencia <= '{$f}'";
        }

        if ($inicioE != '') {
            $i = implode("-", array_reverse(explode("/", $inicioE)));
            $condiniE = "AND n.dataEntrada >= '{$i}'";
        }

        if ($fimVencimentoReal != '') {
            $f = implode("-", array_reverse(explode("/", $fimVencimentoReal)));
            $condFimVencReal = "AND p.vencimento_real <= '{$f}'";
        }

        if ($fimE != '') {
            $f = implode("-", array_reverse(explode("/", $fimE)));
            $condfimE = "AND n.dataEntrada <= '{$f}'";
        }

        $condcod = '';
    } else if ($codigo != '') {
        $condcod = " AND n.idNotas = '{$codigo}'";
    } else if ($numero_nota != '') {
        $condnumero_nota = " AND n.codigo = '{$numero_nota}'";
    }
    if ($_SESSION['empresa_principal'] != 1) {
        $condur = " inner join rateio as r on (n.idNotas = r.NOTA_ID)";
        $condur2 = " AND (r.CENTRO_RESULTADO in {$_SESSION['empresa_user']} or n.empresa in {$_SESSION['empresa_user']})";
    } else {
        $condur = "";
        $condur2 = "";
    }

    if ($ipcg4_nota != '') {
        $condNatureza = "AND n.natureza_movimentacao = '{$ipcg4_nota}'";
    }

    // paginação de parcelas
    if(!isset($pg)) $pg = 1;
    if(!isset($offset)) $offset = 100;

    $ant = (int) $pg -1;
    $prox = (int) $pg + 1;
    $start = ($pg -1)*$offset;

    $sqlGeral = "select 
                n.*,
                DATE_FORMAT(dataEntrada, '%d/%m/%Y') as data_entrada,
                
                f.razaoSocial,
                e.nome as ur ,
                ap.forma,
                u.nome as usuario
            from 
                notas as n inner join 
                empresas as e on (n.empresa = e.id) inner join
                fornecedores as f on (f.idFornecedores = n.codFornecedor) 
                left join tipo_documento_financeiro as tdf on (n.tipo_documento_financeiro_id = tdf.id)
                inner join parcelas as p on (n.idNotas = p.idNota and p.status <> 'Cancelada')
                left join aplicacaofundos as ap on (n.forma_pagamento = ap.id)
                inner join usuarios as u on (n.USUARIO_ID = u.idUsuarios)
                $condur       
            where
            n.status <> 'CANCELADA' 
            and n.inserida_caixa_fixo <> 'S'
                $condcod $condnumero_nota $condtip $condfor $condvalor $condiniE $condfimE $condur2 $conddesc 
                $condInicioVencReal $condFimVencReal $condNatureza $condTipoDoc $condCompetenciaInicio
                $condCompetenciaFim $condFormaPagamento
            group by 
                n.idNotas                  
            order by 
                dataEntrada, razaoSocial ASC";

              

               
               
                
        $resultGeral = mysql_query($sqlGeral);
        $quantidadeTotalRegistros = mysql_num_rows($resultGeral);
        $totalPages = ceil($quantidadeTotalRegistros/100);
        $valorGeral = 0;

        while ($row = mysql_fetch_array($resultGeral)) {
                if ($row['tipo'] == 0) {               
                    $valorGeral += $row['valor'];
                } else {               
                    $valorGeral -= $row['valor'];
                }

            }

        if($totalPages < 6) {
            $pages = range(1, $totalPages);
        } else {
            $limit = 2;
            $currentPageNumber = $pg;
            $firstPageNumber = ($currentPageNumber - $limit) > 1 ? $currentPageNumber - $limit : 1;
            $lastPageNumber = ($currentPageNumber + $limit) < $totalPages ? $currentPageNumber + $limit : $totalPages;
            $pages = range($firstPageNumber, $lastPageNumber);
        }

        $sqlFiltrado = "select 
                n.*,
                DATE_FORMAT(dataEntrada, '%d/%m/%Y') as data_entrada,
                f.razaoSocial,
                e.nome as ur,
                tdf.sigla as tipo_doc_financeiro,
                ap.forma,
                u.nome as usuario 
            from 
                notas as n inner join 
                empresas as e on (n.empresa = e.id) inner join
                fornecedores as f on (f.idFornecedores = n.codFornecedor) 
                inner join parcelas as p on (n.idNotas = p.idNota and p.status <> 'Cancelada')
                left join tipo_documento_financeiro as tdf on (n.tipo_documento_financeiro_id = tdf.id)
                left join aplicacaofundos as ap on (n.forma_pagamento = ap.id)
                inner join usuarios as u on (n.USUARIO_ID = u.idUsuarios)
                $condur       
            where
            n.status <> 'CANCELADA' 
            and n.inserida_caixa_fixo <> 'S'
                $condcod $condnumero_nota $condtip $condfor $condvalor $condiniE $condfimE $condur2 $conddesc 
                $condInicioVencReal $condFimVencReal $condNatureza $condTipoDoc $condCompetenciaInicio
                $condCompetenciaFim $condFormaPagamento
            group by 
                n.idNotas                  
            order by 
                dataEntrada, razaoSocial ASC LIMIT {$start},{$offset}";

        $resultFiltrado = mysql_query($sqlFiltrado);
        $quantidadeRegistrosNaPagina = mysql_num_rows($resultFiltrado);

     $i = 0;
    $idn = 0;
    $idnp = 0;   
    if($quantidadeRegistrosNaPagina > 0){
        $t .= "<div class='row'>
                <div class='col-xs-12 text-right' style='text-align: right;'>
                    <span > 
                        <b>Mostrando {$quantidadeRegistrosNaPagina} de {$quantidadeTotalRegistros} registro(s) encontrado(s).</b>
                    </span>
                </div>      
            </div>";
            
    // inicio paginação
    if ($totalPages > 1) {
        $t .= "<div class='row'>";
        if ($pg <> 1) {
            $t .= "<span  class='paginacao-contas'  style='cursor: pointer;'";
            $t .= "dados='query=buscar_nota&btipo={$btipo}&codFornecedor={$codFornecedor}&codigo={$codigo}&descricao_nota={$descricao_nota}";
            $t .= "&fimE={$fimE}&inicioE={$inicioE}&numero_nota={$numero_nota}&status={$status}&valor={$valor}&pg={$ant}&last={$totalPages}
            &tipo_documento_financeiro_id={$condTipoDoc}&competenciaFim={$competenciaFim}&competenciaInicio={$competenciaInicio}&select_forma_pagamento={$condFormaPagamento}'  ><a>Anterior</a> | </span>";
        }

        foreach ($pages as $page) {
            if ($pg != $page) {
                $t .= "<span class='paginacao-contas' style='cursor: pointer;'";
                $t .= "dados='query=buscar_nota&btipo={$btipo}&codFornecedor={$codFornecedor}&codigo={$codigo}&descricao_nota={$descricao_nota}";
                $t .= "&fimE={$fimE}&inicioE={$inicioE}&numero_nota={$numero_nota}&status={$status}&valor={$valor}&pg={$page}&last={$totalPages}
                &tipo_documento_financeiro_id={$condTipoDoc}&competenciaFim={$competenciaFim}&competenciaInicio={$competenciaInicio}&select_forma_pagamento={$condFormaPagamento}'
                ><a>{$page}</a> | </span>";
            } else {
                $t .= "<span class='active'>{$page} | </span>";
            }
        }

        if ($pg != $totalPages) {
            $t .= "<span><a class='paginacao-contas'  style='cursor: pointer;'";
            $t .= "dados='query=buscar_nota&btipo={$btipo}&codFornecedor={$codFornecedor}&codigo={$codigo}&descricao_nota={$descricao_nota}";
            $t .= "&fimE={$fimE}&inicioE={$inicioE}&numero_nota={$numero_nota}&status={$status}&valor={$valor}&pg={$prox}&last={$totalPages}
            &tipo_documento_financeiro_id={$condTipoDoc}&competenciaFim={$competenciaFim}&competenciaInicio={$competenciaInicio}&select_forma_pagamento={$condFormaPagamento}'
            >Próxima</a></span>";
        }
        $t .= "</div>";
    }
// fim paginacao
                $t .= "</br>
                <div>
                <table id='tb_list' class='table table-bordered table-hover table-striped'>
                    <thead>
                    <tr>
                        <th></th>
                        <th>DATA ENTRADA</th>
                        <th>COMPETÊNCIA</th>
                        <th>TR</th>
                        <th>FORNECEDOR</th>
                        <th>UR</th>
                        <th>CODIGO</th>
                        <th>VALOR PRODUTOS</th>
                        <th>VALOR LÍQUIDO</th>
                        <th>TIPO</th>
                        <th>TIPO DOC FIN</th>                        
                        <th>STATUS</th>
                        <th>OPÇÕES</th>
                    </tr>
                    </thead>";
                    $total = 0;

        while ($row = mysql_fetch_array($resultFiltrado)) {

            if ($row['tipo'] == 0) {
                $condval = "<td>R$ " . number_format($row['VAL_PRODUTOS'], 2, ',', '.') . "</td>
                            <td>R$ " . number_format($row['valor'], 2, ',', '.') . "</td>
                            <td><span class='label label-success'>A Receber</span></td>";
                $total += $row['valor'];
            } else {
                $condval = "<td style='font-size:11px; color:red;'>R$ - " . number_format($row['VAL_PRODUTOS'], 2, ',', '.') . "</td>
                            <td style='font-size:11px; color:red;'>R$ - " . number_format($row['valor'], 2, ',', '.') . "</td>
                            <td><span class='label label-danger'>A Pagar</span></td>";
                $total -= $row['valor'];
            }

            if ($i++ % 2 == 0)
                $cor = "#E9F4F8";
            else
                $cor = "";

            $editar = "?op=notas&act=editar&idNotas={$row['idNotas']}&notaref={$row['NOTA_REFERENCIA']}";
            if($row['nota_adiantamento'] == 'S') {
                $editar = "/financeiros/contas-antecipadas/?action=visualizar-conta-antecipada&id={$row['idNotas']}";
            }

            $resultNotas = mysql_query("SELECT * FROM `notas`as n  where  `idNotas` = '{$row['idNotas']}' ") or trigger_error(mysql_error());
            $rowNotas = mysql_fetch_array($resultNotas);
            $notaAntecipada = $rowNotas['nota_adiantamento'];
            $filtrosCompensada = [
                'idNota' => $row['idNotas'],
                'compensada' => "'TOTAL', 'PARCIAL'"
            ];
            $filtrosAglutinada = [
                'idNota' => $row['idNotas'],
                'aglutinada' => 'S'
            ];
            $responseCompensada = Parcela::getParcelaAtivaByFiltros($filtrosCompensada);
            $responseAglutinada = Parcela::getParcelaAtivaByFiltros($filtrosAglutinada);

            $cancelarNota = false;
            if(count($responseCompensada) > 0){
                $cancelarNota = true;
            }

            if(count($responseAglutinada) > 0){
                $cancelarNota = true;
            }
            $desfazerDesprocessamento = "";
            if(
                (
                    isset($_SESSION['permissoes']
                        ['financeiro']
                        ['financeiro_conta_pagar_receber']
                        ['financeiro_conta_pagar_receber_buscar_parcela']
                        ['financeiro_conta_pagar_receber_buscar_parcela_desprocessar']) ||
                    $_SESSION['adm_user'] == 1
                )
            ) {
                if ($row['status'] == 'Processada' && $row['nota_aglutinacao'] == 'n') {
                    $desfazerDesprocessamento = <<<HTML
<li>
    <a href='#' class='desfazer-processamento-nota' data-idnota='{$row['idNotas']}'>
        <i class='fa fa-times'></i>
        Desfazer Desprocessamento
    </a>
</li>
HTML;
                }
            }

            $sqlNumParcelas = <<<SQL
SELECT COUNT(id) AS qtd_parcelas
FROM parcelas
WHERE idNota = '{$row['idNotas']}'
AND status != 'Cancelada'
GROUP BY idNota
SQL;
            $rsNumParcelas = mysql_query($sqlNumParcelas);
            $rowNumParcelas = mysql_fetch_array($rsNumParcelas);

            $usarParaNova = "";
            $linkUsarNova = "?op=notas&act=usar_nova&idNotas={$row['idNotas']}";
            if(
                $rowNumParcelas['qtd_parcelas'] == 1 &&
                $row['status'] != 'Cancelada' &&
                (
                    isset($_SESSION['permissoes']
                        ['financeiro']
                        ['financeiro_conta_pagar_receber']
                        ['financeiro_conta_pagar_receber_buscar_conta']
                        ['financeiro_conta_pagar_receber_buscar_parcela_usar_para_nova']) ||
                    $_SESSION['adm_user'] == 1
                )
            ) {
                if ($row['nota_aglutinacao'] == 'n' && ($row['NOTA_REFERENCIA'] == '0' || $row['NOTA_REFERENCIA'] == '')) {
                    $usarParaNova = <<<HTML
<li>
    <a target='_blank' href='{$linkUsarNova}'>
        <i class='fa fa-copy'></i> 
        Prorrogar Nota
    </a>
</li>
HTML;
                }
            }

            $cancelarNotaOpt = "";
            if(
                !$cancelarNota &&
                $row['status'] != 'Cancelada' &&
                (
                    isset($_SESSION['permissoes']
                        ['financeiro']
                        ['financeiro_conta_pagar_receber']
                        ['financeiro_conta_pagar_receber_buscar_parcela']
                        ['financeiro_conta_pagar_receber_buscar_parcela_cancelar']) ||
                    $_SESSION['adm_user'] == 1
                )
            ) {
                if ($row['nota_aglutinacao'] == 'n') {
                    $cancelarNotaOpt = <<<HTML
<li>
    <a href='#' id='cancelar-nota' data-idnota='{$row['idNotas']}'>
        <i class='fa fa-times'></i>
        Cancelar Nota
    </a>
</li>
HTML;
                }
            }
            $badgeStatus = "info";
            if($row['status'] == 'Processada') {
                $badgeStatus = "primary";
            } elseif($row['status'] == 'Pendente') {
                $badgeStatus = "warning";
            }

            $dataSistema = \DateTime::createFromFormat('Y-m-d H:i:s', $row['DATA'])
                ->format('d/m/Y à\s H:i:s');

                $data_competencia = implode("/", array_reverse(explode("-", $row['data_competencia'])));
                $faIcon = '';
                if($row['forma_pagamento'] == 1){
                    $faIcon = 'fa-barcode';
                }elseif($row['forma_pagamento'] == 7){
                    $faIcon = 'fa-credit-card';
                }
                elseif($row['forma_pagamento'] == 5){
                    $faIcon = 'fa-exchange';
                }
                elseif($row['forma_pagamento'] == 4){
                    $faIcon = 'fa-money';
                }elseif($row['forma_pagamento'] == 6){
                    $faIcon = 'fa-check-circle-o';
                }elseif($row['forma_pagamento'] == 9){
                    $faIcon = 'fa-list';
                }elseif($row['forma_pagamento'] == 10){
                    $faIcon = 'pix';
                }

                if( isset($_SESSION['permissoes']
                            ['financeiro']
                            ['financeiro_conta_pagar_receber']
                            ['financeiro_conta_pagar_receber_buscar_conta']
                            ['financeiro_conta_pagar_receber_buscar_conta_visualizar_usuario']) ||
                        $_SESSION['adm_user'] == 1 ) {

                            $userName = " Usuário: {$row['usuario']}";
                } 
                
                
                
                $importadaIW = $row['importada_iw'] == 'S' ? 'Importada IW' : 'Inserida Manualmente';
                

            $t .= "<tr style='font-size:11px;' data-toggle='tooltip' data-placement='left' title='Inserido em: {$dataSistema}. Forma pagamento {$row['forma']}. {$importadaIW}. {$userName}' id='nota-{$row['idNotas']}'  bgcolor='{$cor}'>
            <td>
                <i style='font-size:20px !important' class='fa {$faIcon}'> </i> 
                
            </td>
            <td>{$row['data_entrada']}</td>
            <td>{$data_competencia}</td>            
            <td> {$row['TR']}</td>
            <td>{$row['razaoSocial']}</td>
            <td>{$row['ur']}</td>
            <td>{$row['codigo']}</td>
            {$condval}
            <td class='text-center'>{$row['tipo_doc_financeiro']}</td>
            <td class='status-nota-lista'><span class='label label-{$badgeStatus}'>{$row['status']}</span></td>
            <td class='text-center' style='font-size: 16px;'>
                <div class='dropdown'>
                    <button class='btn btn-default dropdown-toggle' type='button' data-toggle='dropdown'>
                        Opções
                        <span class='caret'></span>
                    </button>
                    <ul class='dropdown-menu dropdown-menu-right'>
                        <li>
                            <a target='_blank' href='{$editar}'>
                                <i class='fa fa-list'></i>
                                Visualizar/Processar
                            </a>
                        </li>
                        
                    </ul>
                </div>
             </td>
        </tr>";
            /*{$desfazerDesprocessamento}
            {$cancelarNotaOpt}*/
        }
        /*$t .= "<tr><td colspan = '6' valign='top' style='font-size:11px;text-align: right;'>
            <b> TOTAL DA PÁGINA  R$ " . number_format($total, 2, ',', '.') . "</b>
            </td>
            <td valign='top' style='font-size:11px;' colspan='3'>
            <b>TOTAL GERAL R$ " . number_format($valorGeral, 2, ',', '.') . "</b>
            </td></tr>";*/

        $t .= "</table>
        
        </div>";
    // inicio paginação
    if ($totalPages > 1) {
            $t .= "<div class='row'>";
            if ($pg <> 1) {
                $t .= "<span  class='paginacao-contas' style='cursor: pointer;'";
                $t .= "dados='query=buscar_nota&btipo={$btipo}&codFornecedor={$codFornecedor}&codigo={$codigo}&descricao_nota={$descricao_nota}";
                $t .= "&fimE={$fimE}&inicioE={$inicioE}&numero_nota={$numero_nota}&status={$status}&valor={$valor}&pg={$ant}&last={$totalPages}&tipo_documento_financeiro_id={$condTipoDoc}'  ><a>Anterior</a> | </span>";
            }

            foreach ($pages as $page) {
                if ($pg != $page) {
                    $t .= "<span class='paginacao-contas' style='cursor: pointer;'";
                    $t .= "dados='query=buscar_nota&btipo={$btipo}&codFornecedor={$codFornecedor}&codigo={$codigo}&descricao_nota={$descricao_nota}";
                    $t .= "&fimE={$fimE}&inicioE={$inicioE}&numero_nota={$numero_nota}&status={$status}&valor={$valor}&pg={$page}&last={$totalPages}&tipo_documento_financeiro_id={$condTipoDoc}' 
                ><a>{$page}</a> | </span>";
                } else {
                    $t .= "<span class='active'>{$page} | </span>";
                }
            }

            if ($pg != $totalPages) {
                $t .= "<span><a class='paginacao-contas' style='cursor: pointer;'";
                $t .= "dados='query=buscar_nota&btipo={$btipo}&codFornecedor={$codFornecedor}&codigo={$codigo}&descricao_nota={$descricao_nota}";
                $t .= "&fimE={$fimE}&inicioE={$inicioE}&numero_nota={$numero_nota}&status={$status}&valor={$valor}&pg={$prox}&last={$totalPages}' 
            >Próxima</a></span>";
            }
            $t .= "</div>";
        }
// fim paginacao
    
            
    }else{
        $t .= "<div class='row'>
                    <span><b> Nenhum registro encontrado.</b></span>    
                </div>";
    } 
  

        

    print_r($t);
}

function balancete($post){
	extract($post,EXTR_OVERWRITE);
	$i = implode("-",array_reverse(explode("/",$inicio)));
	$f = implode("-",array_reverse(explode("/",$fim)));
	$condpendente = "0"; $condprocessada = "0"; $condcancelada = "0"; 
	if($pendente == "true") $condpendente = "p.status = 'Pendente'";
	if($processada  == "true") $condprocessada = "p.status = 'Processada' OR p.status = 'Inconsistente'";
	if($cancelada  == "true") $condcancelada = "p.status = 'Cancelada'";
	$condtipo = "( $condpendente OR $condprocessada OR $condcancelada )";
//	$sql = "SELECT DATE_FORMAT(p.vencimento,'%d/%m/%Y') as vencimento, sum(IF(n.tipo = '0',p.valor,0)) as receitas, sum(IF(n.tipo = '1',p.valor,0)) as despesas FROM parcelas as p, notas as n WHERE p.idNota = n.idNotas AND n.status = 'Pendente' AND p.status = 'Pendente' AND p.vencimento BETWEEN '$i' AND '$f' GROUP BY p.vencimento ORDER BY p.vencimento DESC";
	$sql = "SELECT
              DATE_FORMAT(p.vencimento,'%d/%m/%Y') as vencimento,
              f.razaoSocial, 
              n.codigo, 
              IF(n.tipo = '0',p.valor,0) as receitas,
              IF(n.tipo = '1',p.valor,0) as despesas, 
              o.forma as conta, 
              p.status,
              n.idNotas,
              p.id 
              FROM 
              origemfundos as o,
              parcelas as p,
              notas as n,
              fornecedores as f 
              WHERE
              n.codFornecedor = f.idFornecedores AND 
              p.idNota = n.idNotas AND 
             $condtipo AND 
              p.valor <> '0' AND 
              o.id = p.origem AND
              p.vencimento BETWEEN '$i' AND '$f'
              ORDER BY 
              p.vencimento,f.razaoSocial,p.valor DESC";
//	echo $sql;
	$result = mysql_query($sql);
	$var = "<table class='mytable' width=100% >";
	$var .= "<thead><tr>"; 
	$var .= "<th><b>Vencimento</b></th>";
	$var .= "<th><b>Fornecedor</b></th>";
	$var .= "<th><b>C&oacute;digo</b></th>";
	$var .= "<th><b>Receita</b></th>"; 
	$var .= "<th><b>Despesa</b></th>";
	$var .= "<th><b>Conta</b></th>";
  	$var .= "</tr></thead>";
  	$cor = false;
  	$d = 0;
  	$r = 0;
	while($row = mysql_fetch_array($result)){
    	foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
    	if($cor) $var .= "<tr>";
    	else $var .= "<tr class='odd' >";
    	$cor = !$cor;
    	$d = $d + $row['despesas'];
    	$r = $r + $row['receitas'];
    	$receita = number_format($row['receitas'],2,',','.');
		$despesa = number_format($row['despesas'],2,',','.');
		$editconta = "<img src='../utils/edit_16x16.png' class='editconta' title='Editar' border='0' cod='{$row['id']}' >";
		$conta = "DINHEIRO {$editconta}";
		if(substr_count($row['conta'],"BB") != 0) $conta = "BB {$editconta}";
		if(substr_count($row['conta'],"BRD") != 0) $conta = "BRD {$editconta}";
    	$var .= "<td valign='top'>{$row['vencimento']}</td>";
    	$var .= "<td valign='top'>{$row['razaoSocial']}</td>";
    	$icon = "<img src='../utils/pendente_16x16.png' title='Pendente' border='0'>";
    	if($row['status'] == "Pendente" || $row['status'] == "Cancelada") $conta = "<a href=?op=notas&act=detalhes&idNotas={$row['idNotas']}><img src='../utils/details_16x16.png' title='Detalhes' border='0'></a>";
    	if($row['status'] == "Processada")
    		$icon = "<img src='../utils/processada_16x16.png' title='Processada' border='0'>";
    	else if($row['status'] == "Inconsistente")
    		$icon = "<img src='../utils/inconsistente_16x16.png' title='Processada com inconsist&ecirc;ncia' border='0'>";
    	else if($row['status'] == "Cancelada")
    		$icon = "<img src='../utils/cancelada_16x16.png' title='Cancelada' border='0'>";
    	$var .= "<td valign='top'>{$icon} {$row['codigo']}</td>";
    	$var .= "<td valign='top'>R$ {$receita}</td>";  
    	$var .= "<td valign='top'>R$ -{$despesa}</td>";
    	$var .= "<td valign='top'>$conta</td>";
    	$var .= "</tr>"; 
  	}
  	$var .= "<tr><th colspan=3 >Total</th><th>R$ ".number_format($r,2,',','.')."</th><th>R$ ".number_format($d,2,',','.')."</th></tr>";
  	$var .= "</table>";
  	$var .= "<script>$('.editconta').click(function(){\$('#dialog-edit-conta-banco').attr('cod',\$(this).attr('cod'));\$('#dialog-edit-conta-banco').dialog('open');return false;});</script>";
  	echo $var;
  	
}

function editar_conta_banco_parcela($post){
	extract($post,EXTR_OVERWRITE);
	$sql = "UPDATE parcelas SET origem = '$v' WHERE id = '$cod' ";
	if(mysql_query($sql)){
		echo "Salvo com sucesso!";
		savelog(mysql_escape_string(addslashes($sql)));
	}else
		echo "ERRO: tente mais tarde!";
}

function editar_parcela($post){
	extract($post,EXTR_OVERWRITE);
	$v = str_replace(".","",$valor);
	$venc = implode("-",array_reverse(explode("/",$vencimento)));
	$sql = "UPDATE parcelas SET vencimento = '$venc', valor = '$v' WHERE id = '$parcela' ";
	if(mysql_query($sql)){
		echo "Editado com sucesso!";
		savelog(mysql_escape_string(addslashes($sql)));
	}else
		echo "ERRO: tente mais tarde! ".mysql_error();
}

function salvar_fornecedor($post){
  extract($post,EXTR_OVERWRITE);
  mysql_query('begin');
  $nome = anti_injection($nome,'literal');
  $razaosocial = anti_injection($razaosocial,'literal');
    $fisica_juridica = anti_injection($fisica_juridica,'literal');
  $cnpj = anti_injection($cnpj,'literal');
    $tipo = anti_injection($tipo,'literal');
    $cpf=anti_injection($cpf,'literal');
    //$cnpjTratado = str_replace('-','',str_replace('/','',str_replace('.','',$cnpj)));
    /*if($fisica_juridica == 1 ){
        $responseCnpj = Fornecedor::checkIfExistsFornecedorByCNPJ($cnpj,$tipo);
        if(!empty($responseCnpj)){
            $labelTipo = $tipo == 'F'? 'Fornecedor' : "Cliente" ;
            echo "Já existe {$labelTipo} cadastrado com esse CNPJ {$cnpj}, no Sitema.";
            return;

        }

    }else{
        $responseCpf = Fornecedor::checkIfExistsFornecedorByCPF($cpf,$tipo);
        if(!empty($responseCpf)){
            $labelTipo = $tipo == 'F'? 'Fornecedor' : "Cliente" ;
            echo "Já existe {$labelTipo} cadastrado com esse CPF {$cpf}, no Sitema.";
            return;
        }
    }*/

    $classe_valor = anti_injection($classe_valor,'literal');
    $classe_valor_text = anti_injection($classe_valor_text,'literal');
    $classe_valor = strtoupper(\App\Helpers\StringHelper::removeAcentosViaEReg($classe_valor));
    $classe_valor_text = strtoupper(\App\Helpers\StringHelper::removeAcentosViaEReg($classe_valor_text));
    if(($existsClasseValor = ClasseValor::checkIfExistsClasseValor($classe_valor, ($classe_valor == $classe_valor_text))) != false) {
        $classe_valor = $existsClasseValor;
    } else {
        if(!empty($classe_valor)) {
            $classe_valor = ClasseValor::save($classe_valor);
        }
    }

  $ies = anti_injection($ies,'literal');
  $im = anti_injection($im,'literal');
  $contato = anti_injection($contato,'literal');

  $telefone = anti_injection($telefone,'literal');
  $celular = anti_injection($celular,'literal');
  $endereco = anti_injection($endereco,'literal');
  $bairro = anti_injection($bairro,'literal');
  $id_cidade_und = anti_injection($id_cidade_und,'numerico');
  $email = anti_injection($email,'literal');
  $cpf=anti_injection($cpf,'literal');

  $ipcc = anti_injection($ipcc,'literal');

  // RETENÇÕES
  $recolhe_iss = anti_injection($recolhe_iss,'literal');
  $iss = anti_injection($iss,'literal');
  $iss = \App\Helpers\StringHelper::moneyStringToFloat($iss);
  $recolhe_ir = anti_injection($recolhe_ir,'literal');
  $ir = anti_injection($ir,'literal');
  $ir = \App\Helpers\StringHelper::moneyStringToFloat($ir);
  $recolhe_ipi = anti_injection($recolhe_ipi,'literal');
  $ipi = anti_injection($ipi,'literal');
  $ipi = \App\Helpers\StringHelper::moneyStringToFloat($ipi);
  $recolhe_icms = anti_injection($recolhe_icms,'literal');
  $icms = anti_injection($icms,'literal');
  $icms = \App\Helpers\StringHelper::moneyStringToFloat($icms);
  $recolhe_pis = anti_injection($recolhe_pis,'literal');
  $pis = anti_injection($pis,'literal');
  $pis = \App\Helpers\StringHelper::moneyStringToFloat($pis);
  $recolhe_cofins = anti_injection($recolhe_cofins,'literal');
  $cofins = anti_injection($cofins,'literal');
  $cofins = \App\Helpers\StringHelper::moneyStringToFloat($cofins);
  $recolhe_csll = anti_injection($recolhe_csll,'literal');
  $csll = anti_injection($csll,'literal');
  $csll = \App\Helpers\StringHelper::moneyStringToFloat($csll);
  $recolhe_inss = anti_injection($recolhe_inss,'literal');
  $inss = anti_injection($inss,'literal');
  $inss = \App\Helpers\StringHelper::moneyStringToFloat($inss);
  //die(print_r($post));
  $precisa_classificar= anti_injection($precisa_classificar,'literal');
  $classificacao= anti_injection($classificacao,'literal');
  if($precisa_classificar == 'N'){
    $classificacao = NULL;
  }

  $planoSaude = anti_injection($plano_saude,'literal');
  $tipoChavePix = anti_injection($tipo_chave_pix,'literal');
  $chavePix = anti_injection($chave_pix,'literal');
  $observacao = anti_injection($observacao,'literal');
 

	$sql = "INSERT INTO `fornecedores` 
            ( `idFornecedores`,`nome`, `razaoSocial` ,  `cnpj` ,  `contato` ,  `telefone` , `celular` , 
             `email` ,  `endereco`, bairro,`CIDADE_ID`,`IES`,`IM`,TIPO,CPF,
            FISICA_JURIDICA, IPCC, recolhe_iss, aliquota_iss, recolhe_ir, aliquota_ir, 
            recolhe_ipi, aliquota_ipi, recolhe_icms, aliquota_icms, recolhe_pis, aliquota_pis,
            recolhe_cofins, aliquota_cofins, recolhe_csll, aliquota_csll, recolhe_inss, aliquota_inss, 
            precisa_classificar, classificacao, plano_saude, tipo_chave_pix_id, chave_pix,observacao,
             created_at,created_by)
	     VALUES
             (null, '{$nome}', '{$razaosocial}' ,  '{$cnpj}' ,  '{$contato}' ,  '{$telefone}' ,  '{$celular}' ,
              '{$email}' ,  '{$endereco}','{$bairro}','{$id_cidade_und}', '{$ies}','{$im}','{$tipo}', '{$cpf}',
              '{$fisica_juridica}', '{$ipcc}', '{$recolhe_iss}', {$iss}, '{$recolhe_ir}', {$ir}, 
              '{$recolhe_ipi}', {$ipi},  '{$recolhe_icms}', {$icms},  '{$recolhe_pis}', {$pis}, 
              '{$recolhe_cofins}', {$cofins}, '{$recolhe_csll}', {$csll}, '{$recolhe_inss}', {$inss},
              '{$precisa_classificar}','{$classificacao}','{$planoSaude}','{$tipoChavePix}','{$chavePix}', '{$observacao}',
              now(),'{$_SESSION['id_user']}')";
	$r=mysql_query($sql);
	if(!$r){
        echo $sql;
		echo mysql_error();
		mysql_query("rollback");
		return;
	}

	/* DADOS BANC�RIOS */
	$id_fornecedor =mysql_insert_id();
if(!empty($classe_valor)){
    $sql = <<<SQL
INSERT INTO fornecedores_classe_valor (fornecedor_id, classe_valor_id) 
VALUES ({$id_fornecedor}, {$classe_valor})
SQL;

    $rs = mysql_query($sql);
    if(!$rs){
        echo 'Não foi possível associar a classe de valor! ' . mysql_error();
        mysql_query("rollback");
        return;
    }
}

	$itens = explode("$",$dados);

	foreach($itens as $item){
		$con++;
		$i = explode("#",str_replace("undefined","",$item));
		$banco = anti_injection($i[0],'literal');
		$ag = anti_injection($i[1],'literal');
		$conta = anti_injection($i[3],'literal');
        $tipo = anti_injection($i[2],'literal');
        if(empty($op)){
            $op = NULL;
        }

		if(isset($banco) && !empty($banco)){
            $sql = "INSERT INTO `contas_fornecedor` (  `FORNECEDOR_ID` ,  `N_CONTA` ,  `AG` ,  `TIPO` ,  `origem_fundos_id` ) VALUES( '{$id_fornecedor}', '{$conta}' ,  '{$ag}' ,  '{$tipo}' ,  '{$banco}'  )";
		    $r= mysql_query($sql);
		if(!$r){
			echo mysql_error();
			mysql_query("rollback");
			return;
		}

      }
		
	}
	
	
	mysql_query("commit");
	echo '1';
	return;
	
}

function editar_fornecedor($post){
	extract($post,EXTR_OVERWRITE);
	
	$nome = anti_injection($nome,'literal');
	$razaosocial = anti_injection($razaosocial,'literal');
	$cnpj = anti_injection($cnpj,'literal');
	$ies = anti_injection($ies,'literal');
	$im = anti_injection($im,'literal');
	$contato = anti_injection($contato,'literal');
	$id = anti_injection($id,'numerico');
	$tipo = anti_injection($tipo,'literal');

	$telefone = anti_injection($telefone,'literal');
	$celular = anti_injection($celular,'literal');
    $endereco = anti_injection($endereco,'literal');
    $bairro = anti_injection($bairro,'literal');
	$id_cidade_und = anti_injection($id_cidade_und,'numerico');
	$email = anti_injection($email,'literal');
   $cpf=anti_injection($cpf,'literal');
   $fisica_juridica = anti_injection($fisica_juridica,'literal');
   $ipcc = anti_injection($ipcc,'literal');

    // RETENÇÕES
    $recolhe_iss = anti_injection($recolhe_iss,'literal');
    $iss = anti_injection($iss,'literal');
    $iss = \App\Helpers\StringHelper::moneyStringToFloat($iss);
    $recolhe_ir = anti_injection($recolhe_ir,'literal');
    $ir = anti_injection($ir,'literal');
    $ir = \App\Helpers\StringHelper::moneyStringToFloat($ir);
    $recolhe_ipi = anti_injection($recolhe_ipi,'literal');
    $ipi = anti_injection($ipi,'literal');
    $ipi = \App\Helpers\StringHelper::moneyStringToFloat($ipi);
    $recolhe_icms = anti_injection($recolhe_icms,'literal');
    $icms = anti_injection($icms,'literal');
    $icms = \App\Helpers\StringHelper::moneyStringToFloat($icms);
    $recolhe_pis = anti_injection($recolhe_pis,'literal');
    $pis = anti_injection($pis,'literal');
    $pis = \App\Helpers\StringHelper::moneyStringToFloat($pis);
    $recolhe_cofins = anti_injection($recolhe_cofins,'literal');
    $cofins = anti_injection($cofins,'literal');
    $cofins = \App\Helpers\StringHelper::moneyStringToFloat($cofins);
    $recolhe_csll = anti_injection($recolhe_csll,'literal');
    $csll = anti_injection($csll,'literal');
    $csll = \App\Helpers\StringHelper::moneyStringToFloat($csll);
    $recolhe_inss = anti_injection($recolhe_inss,'literal');
    $inss = anti_injection($inss,'literal');
    $inss = \App\Helpers\StringHelper::moneyStringToFloat($inss);
    $precisa_classificar= anti_injection($precisa_classificar,'literal');
  $classificacao= anti_injection($classificacao,'literal');
  if($precisa_classificar == 'N'){
    $classificacao = NULL;
  }

    $classe_valor = anti_injection($classe_valor,'literal');
    $classe_valor_text = anti_injection($classe_valor_text,'literal');
    $classe_valor = strtoupper(\App\Helpers\StringHelper::removeAcentosViaEReg($classe_valor));
    $classe_valor_text = strtoupper(\App\Helpers\StringHelper::removeAcentosViaEReg($classe_valor_text));
    if(($existsClasseValor = ClasseValor::checkIfExistsClasseValor($classe_valor, ($classe_valor == $classe_valor_text))) != false) {
        $classe_valor = $existsClasseValor;
    } else {
        if(!empty($classe_valor)){
            $classe_valor = ClasseValor::save($classe_valor);
        }       
    }
    $planoSaude = anti_injection($plano_saude,'literal');
    $tipoChavePix = anti_injection($tipo_chave_pix,'literal');
  $chavePix = anti_injection($chave_pix,'literal');
  $observacao = anti_injection($observacao,'literal');

	mysql_query("BEGIN");
	$sql ="UPDATE fornecedores set `nome`='{$nome}', `razaoSocial`='{$razaosocial}' ,  
           `cnpj`='{$cnpj}' ,  `contato`='{$contato}' ,  `telefone`='{$telefone}' ,  `celular`='{$celular}' ,  
           `email`='{$email}' , `endereco`='{$endereco}',bairro='{$bairro}',
           `CIDADE_ID`='{$id_cidade_und}',
           `IES`='{$ies}',`IM`='{$im}', TIPO = '{$tipo}',CPF='{$cpf}', 
           FISICA_JURIDICA='{$fisica_juridica}', IPCC = '{$ipcc}', 
           recolhe_iss = '{$recolhe_iss}', aliquota_iss = '{$iss}', 
           recolhe_ir = '{$recolhe_ir}', aliquota_ir = '{$ir}', 
           recolhe_ipi = '{$recolhe_ipi}', aliquota_ipi = '{$ipi}', 
           recolhe_icms = '{$recolhe_icms}', aliquota_icms = '{$icms}', 
           recolhe_pis = '{$recolhe_pis}', aliquota_pis = '{$pis}', 
           recolhe_cofins = '{$recolhe_cofins}', aliquota_cofins = '{$cofins}', 
           recolhe_csll = '{$recolhe_csll}', aliquota_csll = '{$csll}', 
           recolhe_inss = '{$recolhe_inss}', aliquota_inss = '{$inss}',
           precisa_classificar='{$precisa_classificar}',
           classificacao= '{$classificacao}',
           plano_saude = '{$planoSaude}',
           tipo_chave_pix_id = '{$tipoChavePix}',
           chave_pix= '{$chavePix}',
           observacao = '{$observacao}'
  
           where `idFornecedores` = '{$id}' ";
	$r=mysql_query($sql);
	if(!$r){
	echo mysql_error();
	mysql_query("ROLLBACK");
	return;
}

    $sql = "DELETE FROM fornecedores_classe_valor WHERE fornecedor_id = '{$id}' ";
    $r= mysql_query($sql);
    if(!$r){
        echo mysql_error();
        mysql_query("ROLLBACK");
        return;
    }
    if(!empty($classe_valor)){     
    
    $sql = <<<SQL
INSERT INTO fornecedores_classe_valor (fornecedor_id, classe_valor_id) 
VALUES ({$id}, {$classe_valor})
SQL;

    $rs = mysql_query($sql);
    if(!$rs){
        echo 'Não foi possível associar a classe de valor! ' . mysql_error();
        mysql_query("rollback");
        return;
    }
}

/* DADOS BANC�RIOS */
$sql = "DELETE FROM contas_fornecedor WHERE FORNECEDOR_ID = '{$id}' ";
$r= mysql_query($sql);
if(!$r){
	echo mysql_error();
	mysql_query("ROLLBACK");
	return;
}
$itens = explode("$",$dados);
foreach($itens as $item){
$con++;
$i = explode("#",str_replace("undefined","",$item));
$banco = anti_injection($i[0],'literal');
$ag = anti_injection($i[1],'literal');
$conta = anti_injection($i[3],'literal');
$tipo = anti_injection($i[2],'literal');
    if(!empty($banco)){
    $sql = "INSERT INTO `contas_fornecedor` (  `FORNECEDOR_ID` ,  `N_CONTA` ,  `AG` ,  `TIPO` ,  `origem_fundos_id` ) VALUES( '{$id}', '{$conta}' ,  '{$ag}' ,  '{$tipo}' ,  '{$banco}'  )";
    $r= mysql_query($sql);
        if(!$r){
        echo mysql_error();
        mysql_query("ROLLBACK");
        return;
        }
    }
}


mysql_query("COMMIT");
echo '1';
return;

}
//////pesquisar-demonstrativo
function pesquisar_demonstrativo($post){
$cont_status = 0;
extract($post,EXTR_OVERWRITE);
//////////////verifica se é para comparar o percentual que foi atingido com o percentual cadastrado dos IPCGs para a UR.
if($comparar_porcentagem == 'on' && $todos_ur != 'on'){
   $sql="SELECT 
             `COD_N2`,
             `COD_N3`,
             `PORCENTO`,
             (select
                 sum(`PORCENTO`)
             FROM 
                 `planocontas_porcento_ur` as p1
             WHERE 
                  UR={$empresa2} and 
                  STATUS=1 and 
                  p1.`COD_N2` =p.`COD_N2` ) as percent2
            
          FROM 
             `planocontas_porcento_ur` as p 
          WHERE 
            UR={$empresa2} and 
            STATUS=1";
            
            /////////Monta um array com os percentuais do IPCG# E IPCG2 e seus codigos para serem comparados com o resultado da busca do demonstrativo.
            $result = mysql_query($sql);
    $itens = '';

    while ($row = mysql_fetch_array($result)) {
        $itens[$row['COD_N3']] = array("porcento_ipcg2" =>number_format($row['percent2'],2,',','.'), "porcento_ipcg3" => number_format($row['PORCENTO'],2,',','.'),"ipcg2"=>$row['COD_N2']);
    }
   
   $comparar_porcentagem = 1; 
   
}

///////////////// Verificar por qual UR deve ser feito a consulta
if($todos_ur == 'on'){
    $condicao_ur = "";
    $todos_ur=1;
}else{
    $condicao_ur1 = " and R1.CENTRO_RESULTADO = {$empresa2}";
     $condicao_ur2 = " and R2.CENTRO_RESULTADO = {$empresa2}";
    $condicao_ur=" and R.CENTRO_RESULTADO = {$empresa2}";
    $todos_ur=0;
}
/////Bloco que monta a condi��o do status para a consulta sql.
 if($status == 'Processada'){
		$condicao_status = " and P.status = '{$status}'";
	    $condicao_status1 = " and P1.status = '{$status}'";
	    $condicao_status2 = " and P2.status = '{$status}'";
	    $tipo_valor  = "sum(P.VALOR_PAGO* ( R.PORCENTAGEM /100 )) as VALOR,";
	    $tipo_valor1 =" sum(P1.VALOR_PAGO* ( R1.PORCENTAGEM /100 ))";
	    $tipo_valor2 =" sum(P2.VALOR_PAGO* ( R2.PORCENTAGEM /100 ))";
	}else {
		$condicao_status = " and P.status = '{$status}'";
		$condicao_status1 = " and P1.status = '{$status}'";
		$condicao_status2 = " and P2.status = '{$status}'";
		$tipo_valor= "sum(P.valor* ( R.PORCENTAGEM /100 )) as VALOR,";
		$tipo_valor1="sum(P1.valor* ( R1.PORCENTAGEM /100 ))";
		$tipo_valor2="sum(P2.valor* ( R2.PORCENTAGEM /100 ))";
 	}


$fim = implode("-",array_reverse(explode("/",$fim)));
$inicio = implode("-",array_reverse(explode("/",$inicio)));


////btipo recebe tipo da nota:1 desembolso ou 0 recebimento.
       if ($btipo == 1){
    		$cond_tipo=" and PL.N1 like 'D'";
    	}
    	if ($btipo == 0){
    		$cond_tipo =" and PL.N1 like 'R'";
    	}
    	if ($btipo == 't'){
    		$cond_tipo = " ";
    	}
 if($status == 'Processada'){
		$sql="SELECT 
		      P.idNota,
		      R.IPCG3,
		      PL.N4 as IPCG4,  
		      PL.N1 as tipo,	 
			  substr( COD_N3, 1, 3 ) as cod_n3,
			  COD_N4,
			  COD_N2,
			  COD_N3,
		      P.status as status_parcela,	 
		      {$tipo_valor}
			  (SELECT 
				{$tipo_valor1}
			FROM 
				parcelas as P1 inner join 
				notas as N1 ON (P1.idNota = N1.idNotas) inner join 
				rateio as R1 ON (N1.idNotas = R1.NOTA_ID) inner join 
				plano_contas as PL1 ON (PL1.ID = R1.IPCG4) 
			WHERE 
				P1.DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}'
				{$condicao_status1} 
				{$cond_tipo} and 
				PL1.COD_N2 = PL.COD_N2 and
                                PL1.ID >153
                                {$condicao_ur1}
			GROUP BY 
				PL1.COD_N2
			
			) as total,
			(SELECT 
					{$tipo_valor2}
				FROM 
					parcelas as P2 inner join 
					notas as N2 ON (P2.idNota = N2.idNotas) inner join 
					rateio as R2 ON (N2.idNotas = R2.NOTA_ID) inner join 
					plano_contas as PL2 ON (PL2.ID = R2.IPCG4) 
				WHERE 
					P2.DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}'
					{$condicao_status2} 
					{$cond_tipo} and 
					PL2.COD_N3 = PL.COD_N3 and
                                       PL2.ID >153
                                        {$condicao_ur2}
				GROUP BY 
					PL.COD_N3
				) as total_n3,
			
			PL.N2,
                        (SELECT 
				{$tipo_valor1}
			FROM 
				parcelas as P1 inner join 
				notas as N1 ON (P1.idNota = N1.idNotas) inner join 
				rateio as R1 ON (N1.idNotas = R1.NOTA_ID) inner join 
				plano_contas as PL1 ON (PL1.ID = R1.IPCG4) 
			WHERE 
				P1.DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}'
				{$condicao_status1} 
				{$cond_tipo} and 
				PL1.N1 = 'D'and
                                PL1.ID >153
                                {$condicao_ur1}
			
			
			) as total_desembolso,
                        (SELECT 
				{$tipo_valor2}
			FROM 
				parcelas as P2 inner join 
				notas as N2 ON (P2.idNota = N2.idNotas) inner join 
				rateio as R2 ON (N2.idNotas = R2.NOTA_ID) inner join 
				plano_contas as PL2 ON (PL2.ID = R2.IPCG4) 
			WHERE 
				P2.DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}'
				{$condicao_status2} 
				{$cond_tipo} and 
				PL2.N1 = 'R'and
                                PL2.ID >153
                                {$condicao_ur2}
			
			
			) as total_recebimento
			
		FROM 
		      parcelas as P inner join       
		      notas as N ON (P.idNota = N.idNotas) inner join
		      rateio as R ON (N.idNotas = R.NOTA_ID) inner join 
		      plano_contas as PL ON (PL.ID = R.IPCG4)
		WHERE 
                      P.DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}' {$condicao_status} {$cond_tipo} {$condicao_ur} and
                                PL.ID >153
                GROUP BY
                    PL.COD_N4
                UNION
                    SELECT 
			0 AS idNota,
			PL.N3 as IPCG3,
			PL.N4 as IPCG4,
			PL.N1 AS tipo,
			PL.COD_N2 AS cod_n3,
			PL.COD_N4 AS COD_N4,
			PL.COD_N2,
			PL.COD_N3,
			'' AS status_parcela,
			SUM(itl.valor) AS VALOR,
			0 AS total,
			0 AS total_n3,
			PL.N2 AS N2,
			0 AS total_desembolso,
			0 AS total_recebimento
                    FROM
			impostos_tarifas imptar
			INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
			LEFT JOIN plano_contas PL ON (imptar.ipcf = PL.COD_N4)
                    WHERE
			itl.`data` BETWEEN '{$inicio}' AND '{$fim}'                
                    GROUP BY
                        PL.COD_N4
                    ORDER BY
                        tipo DESC,
                        COD_N4,
                        N2 ASC,
                        IPCG3 ASC";
		 }else{
    	
                    $sql="SELECT 
                            P.idNota,
                            R.IPCG3,
                            PL.N4 as IPCG4,  
                            PL.N1 as tipo,	 
                                substr( COD_N3, 1, 3 ) as cod_n3,
                                COD_N4,
                                COD_N2,
                                COD_N3,
                            P.status as status_parcela,	 
                            {$tipo_valor}
                                (SELECT 
                                      {$tipo_valor1}
                              FROM 
                                      parcelas as P1 inner join 
                                      notas as N1 ON (P1.idNota = N1.idNotas) inner join 
                                      rateio as R1 ON (N1.idNotas = R1.NOTA_ID) inner join 
                                      plano_contas as PL1 ON (PL1.ID = R1.IPCG4) 
                              WHERE 
                                      P1.vencimento BETWEEN '{$inicio}' and '{$fim}'
                                      {$condicao_status1} 
                                      {$cond_tipo} and 
                                      PL1.COD_N2 = PL.COD_N2 
                                      {$condicao_ur1} and
                                       PL1.ID >153
                              GROUP BY 
                                      PL1.COD_N2

                              ) as total,
                              (SELECT 
                                              {$tipo_valor2}
                                      FROM 
                                              parcelas as P2 inner join 
                                              notas as N2 ON (P2.idNota = N2.idNotas) inner join 
                                              rateio as R2 ON (N2.idNotas = R2.NOTA_ID) inner join 
                                              plano_contas as PL2 ON (PL2.ID = R2.IPCG4) 
                                      WHERE 
                                              P2.vencimento BETWEEN '{$inicio}' and '{$fim}'
                                              {$condicao_status2} 
                                              {$cond_tipo} and 
                                              PL2.COD_N3 = PL.COD_N3 and
                                              PL2.ID >153
                                              {$condicao_ur2}
                                      GROUP BY 
                                              PL.COD_N3
                                      ) as total_n3,

                              PL.N2,
                              (SELECT 
                                      {$tipo_valor1}
                              FROM 
                                      parcelas as P1 inner join 
                                      notas as N1 ON (P1.idNota = N1.idNotas) inner join 
                                      rateio as R1 ON (N1.idNotas = R1.NOTA_ID) inner join 
                                      plano_contas as PL1 ON (PL1.ID = R1.IPCG4) 
                              WHERE 
                                      P1.vencimento BETWEEN '{$inicio}' and '{$fim}'
                                      {$condicao_status1} 
                                      {$cond_tipo} and 
                                      PL1.N1 = 'D'and
                                      PL1.ID >153 
                                      {$condicao_ur1}


                              ) as total_desembolso,
                              (SELECT 
                                      {$tipo_valor1}
                              FROM 
                                      parcelas as P1 inner join 
                                      notas as N1 ON (P1.idNota = N1.idNotas) inner join 
                                      rateio as R1 ON (N1.idNotas = R1.NOTA_ID) inner join 
                                      plano_contas as PL1 ON (PL1.ID = R1.IPCG4) 
                              WHERE 
                                      P1.vencimento BETWEEN '{$inicio}' and '{$fim}'
                                      {$condicao_status1} 
                                      {$cond_tipo} and 
                                      PL1.N1 = 'R'  and
                                       PL1.ID >153
                                      {$condicao_ur1}


                              ) as total_recebimento

                      FROM 
                            parcelas as P inner join       
                            notas as N ON (P.idNota = N.idNotas) inner join
                            rateio as R ON (N.idNotas = R.NOTA_ID) inner join 
                            plano_contas as PL ON (PL.ID = R.IPCG4)
                      WHERE 
                            P.vencimento BETWEEN '{$inicio}' and '{$fim}' {$condicao_status} {$cond_tipo} {$condicao_ur}
                                and  PL.ID >153
                      GROUP BY
                            PL.COD_N4
                        ORDER BY
                            PL.N1 DESC,
                            PL.COD_N4,
                            PL.N2 ASC,
                            R.IPCG3 ASC";
    	} 
	
        $result = mysql_query($sql);
        $sql_tarifas = "SELECT
                            imptar.ipcf AS COD_N4,
                            sum(itl.valor) AS val
                        FROM
                            impostos_tarifas imptar
                            INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
                            INNER JOIN contas_bancarias cb ON (cb.id = itl.conta)
                        WHERE
                            itl.data BETWEEN '{$inicio}' AND '{$fim}'
                        GROUP BY
                            COD_N4
                        ORDER BY
                            COD_N4 ASC";
        $resultset = mysql_query($sql_tarifas);
        $sql_total_n2 = "SELECT
                            sum(
                                P2.VALOR_PAGO * (R2.PORCENTAGEM / 100)
                            ) AS total
                        FROM
                                parcelas AS P2
                        INNER JOIN notas AS N2 ON (P2.idNota = N2.idNotas)
                        INNER JOIN rateio AS R2 ON (N2.idNotas = R2.NOTA_ID)
                        INNER JOIN plano_contas AS PL2 ON (PL2.ID = R2.IPCG4)
                        WHERE
                                P2.DATA_PAGAMENTO BETWEEN '{$inicio}' AND '{$fim}'
                        AND P2. STATUS = 'Processada'
                        AND PL2.COD_N2 = '2.5'
                        AND PL2.ID > 153
                        AND R2.CENTRO_RESULTADO = 1
                        GROUP BY
                                PL2.COD_N3
                                
                        UNION
                        
                        SELECT
                                sum(
                                        P1.VALOR_PAGO * (R1.PORCENTAGEM / 100)
                                ) as total_desembolso
                        FROM
                                parcelas AS P1
                        INNER JOIN notas AS N1 ON (P1.idNota = N1.idNotas)
                        INNER JOIN rateio AS R1 ON (N1.idNotas = R1.NOTA_ID)
                        INNER JOIN plano_contas AS PL1 ON (PL1.ID = R1.IPCG4)
                        WHERE
                                P1.DATA_PAGAMENTO BETWEEN '2014-10-01'
                        AND '2014-11-24'
                        AND P1. STATUS = 'Processada'
                        AND PL1.N1 = 'D'
                        AND PL1.ID > 153
                        AND R1.CENTRO_RESULTADO = 1";
                                
        $resultset_n2 = mysql_query($sql_total_n2); 
        $totais = [];
        while($row_total = mysql_fetch_array($resultset_n2, MYSQL_ASSOC)){
            $totais[] = $row_total['total'];
        }
        $tarifa = [];
        while ($tarifas = mysql_fetch_array($resultset)){
            $COD_N2 = substr($tarifas['COD_N4'],0,3);
            $COD_N3 = substr($tarifas['COD_N4'],0,5);
            $tarifa['total_desembolso'] = $totais[1];
            $tarifa[$COD_N2] = $tarifas['val'] + $totais[0];
            $tarifa[$COD_N3] = $tarifas['val'];
        }
	  $aux = '';	 
	  $total_geral   = 0;
	  $total_geral_r =0;
	  $total_geral_d =0;
	  $total_por_tipo = 0;
	  $cont =1;
	   $aux_n2 = '';
	   $aux_tipo = '';
	 
	  $num_rows= mysql_num_rows($result);
	   $cont2 =$num_rows;
	 $cor = "bgcolor='#eee'";
	
if($num_rows >0){	
while($row = mysql_fetch_array($result)){
    if($comparar_porcentagem == 1 && $todos_ur == 0){
    if (array_key_exists($row['COD_N3'], $itens)) {
                    $porcento_ipcg2 = "<th>" . $itens[$row['COD_N3']]['porcento_ipcg2'] . "%</th>";
                    $porcento_ipcg3 = "<td>" . $itens[$row['COD_N3']]['porcento_ipcg3'] . "%</td>";
                    $p_ipcg2 = $itens[$row['COD_N3']]['porcento_ipcg2'];
                    $p_ipcg3 = $itens[$row['COD_N3']]['porcento_ipcg3'];
                    
                    $porcento_n3 = ($row['total_n3'] / $row['total_desembolso']) * 100;
                    $sub = $porcento_n3 - number_format($p_ipcg3, 2, '.', ',');
                    if ($row['tipo'] == 'D') {
                        if ($sub > 2 || $sub < -2) {
                            $cor_ipcg3 = 'red';
                        } else {
                            $cor_ipcg3 = 'blue';
                        }
                    } else {
                        if ( $sub < -2) {
                            $cor_ipcg3 = 'red';
                        } else {
                            $cor_ipcg3 = 'blue';
                        }
                    }
                } else {
                    $cor_ipcg3 = 'black';
                    $porcento_ipcg2 = "<th>0,00%</th>";
                    $porcento_ipcg3 = "<td>0,00%</td>";
                }
}
    
   $cod_n3 = implode("",(explode(".",$row['cod_n3'])));
   if($cont2--%2 == 0){	
		$class = "odd";
	}else{
		$class = "";
	}
	
	if($row['tipo'] == 'R'){
		 $total_geral_r += $row['VALOR'];
		
		}else{
		$total_geral_d += $row['VALOR'];
		}
		
	     $total_geral = $total_geral_r - $total_geral_d ;
	     $total_por_tipo += $row['VALOR']; 

	if($row['tipo'] !=  $aux_tipo){
	 $aux_tipo=$row['tipo'] ;
	 if($aux_tipo == 'R'){
			echo "<h1><center>RECEBIMENTO</center></h1><br><table width='100%' class='mytable'>";
		}else{
		    if($cont>1)
				echo "<tr><td width='50%'><b>TOTAL RECEBIMENTO:</b></td><td><b> R$ ".number_format($total_geral_r,2,',','.')."</b></td></tr>
                      </table><h1><center>DESEMBOLSO</center></h1><br><table width='100%' class='mytable'>";
			else
				echo "<h1><center>DESEMBOLSO</center></h1><br><table width='100%' class='mytable'>";
		 }
	}
	
	if($row['tipo'] =='D'){
            if($row['N2'] !=  $aux_n2){
                if (array_key_exists($row['COD_N2'], $tarifa)) {                
                    $row['total'] += $tarifa[$row['COD_N2']];
                    $total_geral_d += $tarifa[$row['COD_N2']];
                    if($row['COD_N2'] == '2.5'){
                        $total_porcentagem = ($row['total']/$tarifa['total_desembolso'])*100;
                    }
                }  else {
                    $total_porcentagem = 0;
                }
             $aux_n2=$row['N2'] ;
             $porcento_n2 = (($row['total']/$row['total_desembolso'])*100) + $total_porcentagem;


            echo "<thead  n3='{$classe_item}' aux='0' ><tr><th width='50%' ><b><span >{$row['COD_N2']}</span> {$row['N2']} </b></th><th><b>Total: R$ ".number_format($row['total'],2,',','.')."</b></th><th><span style='text-color:{$cor_ipcg2};'>".number_format($porcento_n2,2,',','.')." %</span></th>{$porcento_ipcg2}</tr></thead>";
	
	}
    if($row['IPCG3'] !=  $aux){
            if (array_key_exists($row['COD_N3'], $tarifa)) {                
                $row['total_n3'] += $tarifa[$row['COD_N3']]; 
                $total_porcentagem = ($row['total_n3']/$tarifa['total_desembolso'])*100;
            }
	  $classe_item = "G".$cont;
		$aux = $row['IPCG3'];
		$porcento_n3 = (($row['total_n3']/$row['total_desembolso'])*100) + $total_porcentagem;
	    
		echo "<tr class='icon' {$cor} n3='{$classe_item}' aux='0' ><td width='50%'><b><span > {$row['COD_N3']} </span>{$row['IPCG3']}</b></td><td><b>Total: R$ ".number_format($row['total_n3'],2,',','.')."</b></td><td><font color='{$cor_ipcg3}'>".number_format($porcento_n3,2,',','.')." %</font></td>{$porcento_ipcg3}</tr>";
	 }
	 
         
		echo "<tr  class='$classe_item $class' aux1='0'  style='display:none;'><td style='padding-left:20px;' width='50%'>  {$row[COD_N4]} - {$row['IPCG4']}</td><td> R$ ".number_format($row['VALOR'],2,',','.')."</td></tr>";
		
}else {
    if($row['N2'] !=  $aux_n2){
	 $aux_n2=$row['N2'] ;
         $porcento_n2 = ($row['total']/$row['total_recebimento'])*100;
	echo "<thead  n3='{$classe_item}' aux='0' ><tr><th width='50%' ><b><span >{$row['COD_N2']}</span> {$row['N2']} </b></th><th><b>Total: R$ ".number_format($row['total'],2,',','.')."</b></th><th>".number_format($porcento_n2,2,',','.')." %</th>{$porcento_ipcg2}</tr></thead>";
	
	}
    if($row['IPCG3'] !=  $aux){
	  $classe_item = "G".$cont;
		$aux = $row['IPCG3'];
		$porcento_n3 = ($row['total_n3']/$row['total_recebimento'])*100;
	    
		echo "<tr class='icon' {$cor} n3='{$classe_item}' aux='0' ><td width='50%'><b><span > {$row['COD_N3']} </span>{$row['IPCG3']}</b></td><td><b>Total: R$ ".number_format($row['total_n3'],2,',','.')."</b></td><td><font color='{$cor_ipcg3}'>".number_format($porcento_n3,2,',','.')." %</font></td>{$porcento_ipcg3}</tr>";
	 }
	 
	 
		echo "<tr  class='$classe_item $class' aux1='0'  style='display:none;'><td style='padding-left:20px;' width='50%'>  {$row[COD_N4]} - {$row['IPCG4']}</td><td> R$ ".number_format($row['VALOR'],2,',','.')."</td></tr>";
		

}	
		 
	 $cont++;
	 
	 
	 ########################
	 # Este bloco serve para exibir o �ltimo total_por_tipo do demonstrativo com cores alternadas;  
	 ###################
	 
	
}
  /////condi��o para passar o status das parcelas para o imprimir demonstrativo financeiro
  if($pendente == "on"){
     $pendente1 = 1;
   }else{
    $pendente1 = 0;
   } 
   
   if($processada == "on"){
     $processada1 = 1;
   }else{
     $processada1 = 0;
   } 
   
   if($cancelada == "on"){
     $cancelada1 = 1;
   }else{
     $cancelada1 = 0;
   }    

 ////exibir o total se o btipo = t
 if($btipo == 't' && $total_geral_d > 0){
 
 
  echo "<tr><td width='50%'><b>TOTAL DESEMBOLSO:</b></td><td><b> R$ ".number_format($total_geral_d,2,',','.')."</b></td></tr>";
 
 
 }
	
	echo "</table></br></br>";
	echo "<table class='mytable' width='100%'>";
          echo "<tr><td width='50%'><b>TOTAL GERAL:</b></td><td><b> R$ ".number_format(abs($total_geral),2,',','.')."</b></td></tr>";
	echo "</table></br>";
	/////2013-08-31
	//echo $sql;
	echo"<form method=post target='_blank' action='imprimir_demonstrativo_financeiro.php?status={$status}&btipo={$btipo}&fim={$fim}&inicio={$inicio}&todos_ur={$todos_ur}&empresa={$empresa2}&comparar_porcentagem={$comparar_porcentagem}'>";
	echo "<button id='imprimir_score' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
				<span class='ui-button-text'>Imprimir</span></button>";
		echo "</form>";
}else{
    echo "<br/><br/><p><b>Nenhum resultado encontrado.</b></p>";
}




}
//////fim pesquisar-demonstrativo
////// 2013-05-21 pesquisar-conciliacao
function pesquisar_conciliacao($post){
extract($post,EXTR_OVERWRITE);

$fim1 = implode("-",array_reverse(explode("/",$fim)));
$inicio1 = implode("-",array_reverse(explode("/",$inicio)));
   
  ############## Tratamento do Filtro de conciliação para Transferencia
  $condicao_conciliacao_transf ="";
 // echo "----{$conciliado}-------";
    $selectTarifas = '';

    if($conciliado_radio != 'N'){
        $selectTarifas = "UNION
                SELECT
                    itl.id,
                    'S' AS CONCILIADO,
                    itl.valor as VALOR_PAGO,
                    0 AS idNota,
                    0 AS TR,
                    itl.documento as NUM_DOCUMENTO,
                    itl.data as dt,
                    DATE_FORMAT(itl.data, '%d/%m/%Y') AS data_pg,
                    1 AS tipo_nota,
                    imptar.item as fornecedor,
                    0 AS fornecedor_id,
                    'N' as transferencia,
                    DATE_FORMAT(itl.data, '%d/%m/%Y')  AS data_conciliado,
                   
                    'impostos_tarifas' as tabela_origem 
		FROM
			impostos_tarifas imptar
                        INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
		WHERE
                    itl.conta = $origemFundos
                    AND 
                    itl.data BETWEEN '{$inicio1}' AND '{$fim1}'";
    }
	if($conciliado_radio=='S')
	{
		$condicao_conciliacao_parcela="p.DATA_PAGAMENTO BETWEEN '{$inicio1}' AND '{$fim1}' AND CONCILIADO='{$conciliado_radio}'";
		$condicao_conciliacao_transf = "(`DATA_TRANSFERENCIA`  BETWEEN '{$inicio1}' and '{$fim1}' AND IF(`CONTA_DESTINO`='{$origemFundos}',CONCILIADO_DESTINO='".$conciliado_radio."',CONCILIADO_ORIGEM='".$conciliado_radio."'))";
	}
	elseif($conciliado_radio=='N')
	{   $condicao_conciliacao_parcela="( p.DATA_PAGAMENTO BETWEEN '{$inicio1}' AND '{$fim1}' AND conciliado<> 'S')";
		$condicao_conciliacao_transf = "(`DATA_TRANSFERENCIA`  BETWEEN '{$inicio1}' and '{$fim1}' AND (IF(`CONTA_DESTINO`={$origemFundos},CONCILIADO_DESTINO <>'S' ,CONCILIADO_ORIGEM <> 'S')) )";
	}
	else
	{
		$condicao_conciliacao_parcela="( p.DATA_PAGAMENTO BETWEEN '{$inicio1}' AND '{$fim1}'  )";
		$condicao_conciliacao_transf = "(`DATA_TRANSFERENCIA`  BETWEEN '{$inicio1}' and '{$fim1}' )";
	}
   //////////sql parcelas e transferências
   $sql ="Select 
         p.id, 
         p.CONCILIADO,
         p.VALOR_PAGO,
         p.idNota,
	     p.TR,
         p.NUM_DOCUMENTO,
	     p.DATA_PAGAMENTO as dt,
         DATE_FORMAT(p.DATA_PAGAMENTO,'%d/%m/%Y') as data_pg,
	     n.tipo as tipo_nota,
	     f.razaoSocial as fornecedor,
         n.codFornecedor as fornecedor_id,
         'N' as transferencia,
       DATE_FORMAT(p.DATA_CONCILIADO,'%d/%m/%Y') as data_conciliado,
       'parcelas' as tabela_origem 
       
from 
		parcelas as p inner join 
		contas_bancarias as o on (p.origem = o.id) inner join 
		notas as n on (p.idNota = n.idNotas) inner join 
		fornecedores as f on (n.codFornecedor=f.idFornecedores)
	where 
        p.status ='Processada' AND
        p.origem = {$origemFundos} AND {$condicao_conciliacao_parcela} 
	
UNION       
SELECT 
		ID,
		(case when `CONTA_ORIGEM`={$origemFundos}  then CONCILIADO_ORIGEM when `CONTA_DESTINO`={$origemFundos}  then  CONCILIADO_DESTINO end) as CONCILIADO,
		( case when `CONTA_ORIGEM`={$origemFundos}  then `VALOR` when `CONTA_DESTINO`={$origemFundos}  then  VALOR end) as VALOR_PAGO,
		0 as idNota,
		'0' as TR,
		'Transferência' as NUM_DOCUMENTO,
		`DATA_TRANSFERENCIA` as dt,
		DATE_FORMAT(`DATA_TRANSFERENCIA`,'%d/%m/%Y') as data_pg,
		( case when `CONTA_ORIGEM`={$origemFundos}  then '1' when `CONTA_DESTINO`={$origemFundos}  then '0' end) as tipo_nota,
		( case when `CONTA_ORIGEM`={$origemFundos}  THEN (
												SELECT  
														concat(o.SIGLA_BANCO,'-',c.AGENCIA,'-',c.NUM_CONTA,'-',e.SIGLA_EMPRESA)
												FROM
														contas_bancarias as c INNER JOIN
														origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id) INNER JOIN 
														empresas as e ON (c.UR = e.id) 
												WHERE
														c.ID= CONTA_DESTINO
											)
		
			  WHEN `CONTA_DESTINO`={$origemFundos}  THEN (
											 SELECT  
													concat(o.SIGLA_BANCO,'-',c.AGENCIA,'-',c.NUM_CONTA,'-',e.SIGLA_EMPRESA)
											 FROM
													contas_bancarias as c INNER JOIN
													origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id) INNER JOIN 
													empresas as e ON (c.UR = e.id) 
											 WHERE 
													c.ID = CONTA_ORIGEM
											)							
											 END
		) as fornecedor,
		( CASE 
				WHEN `CONTA_ORIGEM`={$origemFundos} THEN 
					CONTA_DESTINO 
				WHEN `CONTA_DESTINO`={$origemFundos}  THEN  
					CONTA_ORIGEM 
		END) as fornecedor_id,
        'S' as trasferencia,
        (case when `CONTA_ORIGEM`={$origemFundos}  then DATE_FORMAT(DATA_CONCILIADO_ORIGEM,'%d/%m/%Y') when `CONTA_DESTINO`={$origemFundos}  then  DATE_FORMAT(DATA_CONCILIADO_DESTINO,'%d/%m/%Y') end) as data_conciliado,
        'transferencia_bancaria' as tabela_origem 
FROM
		`transferencia_bancaria` 
WHERE 
		(`CONTA_DESTINO`={$origemFundos} or `CONTA_ORIGEM`={$origemFundos}) and
		 
		
		{$condicao_conciliacao_transf}  and CANCELADA_POR is NULL
		
		{$selectTarifas}
order by dt";

	$result = mysql_query($sql);
	  $numero_linhas = mysql_num_rows($result);
	if($numero_linhas == 0){
		echo "</br><p><center><b>Nenhum item encontrado</b></center></p>";
	}else{

   $nconciliado=$saldo_nconciliado+$nconciliado+$transferencia_nconciliado;
   $conciliado=$saldo+$transferencia_conciliado; 
	
    echo "</br></br>";

    echo "<table class='mytable' width='100%' >
        <thead>
             <tr>
                 <th width='7%' style='font-size:9px;'>DATA DE PROCESSAMENTO</th>
                 <th>TR</th>
                 <th>Fornecedor</th>
                 <th>N&deg; Documento</th>
                 <th>Entrada</th>
                 <th>Sa&iacute;da</th>
                 <th  width='10%'>CONCILIA&Ccedil;&Atilde;O</th>
                 <th style='font-size:9px;'>Data Conciliado</th>
                 <th>DESFAZER</th>
             </tr>
       </thead>";
   /////contador para numerar linhas
   $cont =1;
   $id_saldo=$origemFundos;
   while($row = mysql_fetch_array($result)){
        ////tipo da parcela 1-saida ou 0-entrada	 
      if($row['tipo_nota'] == 0){
      $entrada =$row['VALOR_PAGO'];
       $saida ='0.00';
      }else{
      $entrada ='0.00';
       $saida =$row['VALOR_PAGO'];
      }
      $desfazer='';
   /////se a parcela j� foi conlciliada ou n 
  		  if($row['CONCILIADO'] == 'S'){
              if($row['tabela_origem'] == 'impostos_tarifas'){
                  $desfazer= '';
              }else {
                  $desfazer = "<button class='desfazer-conciliacao' id_banco ='{$id_saldo}' transferencia='{$row['transferencia']}' id='{$row['id']}' entrada='{$entrada}' saida='{$saida}' tipo='{$row['tipo_nota']}' num_linha='{$cont}' valor='{$row['VALOR_PAGO']}' id_parcela='{$row['id']}' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
				<span class='ui-button-text'>Desfazer</span></button>";
              }
                         $data_conciliado = "<input  disabled='disabled' class='data' id_parcela='{$row['id']}' type='text' name='data_conciliado' maxlength='10' size='10' value='{$row['data_conciliado']}' />";

   	   			 $enabled = "disabled='disabled'";
      			 $radio = "<center> Sim</center><span style='display:none;'><input type='hidden' alterado='N' name='conciliado_{$row['id']}' value='S' tipo='{$row['tipo_nota']}' num_linha='{$cont}' valor='{$row['VALOR_PAGO']}' id_parcela='{$row['id']}' class='conciliado' {$enabled} checked='checked'>Sim </input>
                                     
                                          </span>";
      	 }else if($row['CONCILIADO'] == 'N'){
                 $data_conciliado = "<input  style='display:none;' class='data' id_parcela='{$row['id']}' type='text' name='data_conciliado' maxlength='10' size='10' value='{$row['data_pg']}' />";
      		 $enabled ="";
       		 $radio = "<input type='radio' alterado='N' name='conciliado_{$row['id']}' value='S'   tipo='{$row['tipo_nota']}' num_linha='{$cont}' valor='{$row['VALOR_PAGO']}' id_parcela='{$row['id']}' class='conciliado OBG_RADIO' style='font-size:9px;'>Sim</input>
                           <input type='radio' name='conciliado_{$row['id']}' value='N'   transferencia='{$row['transferencia']}' alterado='N' class='OBG_RADIO conciliado ' checked='checked' tipo='{$row['tipo_nota']}' num_linha='{$cont}' valor='{$row['VALOR_PAGO']}' id_parcela='{$row['id']}' style='font-size:9px;'>N&atilde;o</input>";
     	 }else{
     	   $enabled ="";
      	  $radio = " <input type='radio' name='conciliado_{$row['id']}' alterado='N' value='S'  tipo='{$row['tipo_nota']}' num_linha='{$cont}' valor='{$row['VALOR_PAGO']}' transferencia='{$row['transferencia']}' id_parcela='{$row['id']}' class='conciliado OBG_RADIO' style='font-size:9px;'> Sim</input>
                     <input type='radio' transferencia='{$row['transferencia']}' name='conciliado_{$row['id']}'  value='N' alterado='N' class='OBG_RADIO conciliado'  tipo='{$row['tipo_nota']}' valor='{$row['VALOR_PAGO']}' num_linha='{$cont}' id_parcela='{$row['id']}' style='font-size:9px;'>N&atilde;o</input>";
      	 }
     
      
     
   
   echo "<tr class='dados' valor_pago='{$row['VALOR_PAGO']}' transferencia='{$row['transferencia']}' id_parcela='{$row['id']}' conciliado='".$row['CONCILIADO']."' tipo='".$row['tipo_nota']."' tr='".$row['idNota']."-".$row['TR']."'  data='".$row['data_pg']."' fornecedor = '".$row['fornecedor_id']."' alterado='N'  >
           <td>{$row['data_pg']}</td>
           <td>{$row['idNota']}-{$row['TR']}</td>
           <td style='font-size:9px;'>{$row['fornecedor']}</td>
           <td>{$row['NUM_DOCUMENTO']}</td>
           <td>".number_format($entrada,2,',','.')."</td>
           <td>".number_format($saida,2,',','.')."</td>
           <td style='font-size:9px;'>$radio</td>
           <td>$data_conciliado</td>
           <td>$desfazer</td>
          </tr>";
   }
   
   
   
   echo "</table>";
   
   
   echo "<button id='salvar-conciliacao' id_banco ='{$id_saldo}'  inicio='{$inicio}' fim='{$fim}' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
				<span class='ui-button-text'>Salvar</span></button>";

}


}
//Eriton pesquisar extrato 05-06-2013
function pesquisar_extrato($post){
    extract($post,EXTR_OVERWRITE);
    $fim1 = implode("-",array_reverse(explode("/",$fim)));
    $inicio1 = implode("-",array_reverse(explode("/",$inicio)));
    $sql_sald_inicial="SELECT 
                            SALDO_INICIAL,
                            DATE_FORMAT(DATA_SALDO_INICIAL , '%d/%m/%Y' ),
                            DATA_SALDO_INICIAL
                        FROM
                            contas_bancarias
                        WHERE
                            ID ={$id_conta}";
    $result_sald_inicial = mysql_query($sql_sald_inicial);
    $saldo_inicial = mysql_result($result_sald_inicial,0,0);
    $data_saldo_inicial_pt = mysql_result($result_sald_inicial,0,1);
    $data_saldo_inicial_us =mysql_result($result_sald_inicial,0,2);
    $total_conciliado = $saldo_inicial;
    $total_nconciliado = $saldo_inicial;
    $total = $saldo_inicial;
    $auxMovimentacaoAnterior = 0;

    $cond_conciliado = $conciliado == 'N' ? "p.CONCILIADO='N'" : "p.CONCILIADO='S'";
    $cond_data_parcela = $conciliado == 'N' ? "p.DATA_PAGAMENTO" : "DATA_CONCILIADO";

    $cond_conciliado_baixa = $conciliado == 'N' ? "nb.conciliado='N'" : "nb.conciliado='S'";
    $cond_data_baixa = $conciliado == 'N' ? "nb.data_pagamento" : "nb.data_conciliado";

    $unionTarifas = "";
    $unionEstorno = "";
    if($conciliado == 'S') {
        $unionTarifas = <<<SQL
UNION
  SELECT
    itl.id,
    'S' AS CONCILIADO,
    itl.valor,
    0 AS idNota,
    0 AS TR,
    itl.documento,
    DATE_FORMAT(itl.data, '%d/%m/%Y') AS data_pg,
    IF(debito_credito = 'DEBITO', 1, 0) AS tipo_nota,
    imptar.item,
    0 AS fornecedor_id,
    itl.data AS data_pg_us,
    'tarifa' AS tipo_item
  FROM
    impostos_tarifas imptar
    INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
  WHERE
    itl.conta = {$id_conta}
    AND itl.data <= '{$fim1}'
SQL;
        $unionEstorno = <<<SQL
UNION
  SELECT
    hep.id,
    'S' AS CONCILIADO,
    hep.valor,
    hep.credito_id AS idNota,
    p.TR AS TR,
    CONCAT('ESTORNO BANCÁRIO PARA ', IF(f.razaoSocial != '', f.razaoSocial, f.nome)) AS codigo,
    DATE_FORMAT(hep.data_conciliada, '%d/%m/%Y') AS data_pg,
    '1' AS tipo_nota,
    f.razaoSocial as fornecedor,
    n.codFornecedor AS fornecedor_id,
    hep.data_conciliada AS data_pg_us,
    'estorno' as tipo_item
  FROM
    estorno_parcela hep
    INNER JOIN parcelas p ON (p.id = hep.parcela_id)
    INNER JOIN notas n ON (n.idNotas = p.idNota)
    LEFT JOIN fornecedores f ON (n.codFornecedor = f.idFornecedores)
  WHERE
    hep.origem = '{$id_conta}'
    AND hep.data_conciliada <= '{$fim1}'
SQL;
    }

    $sql = "SELECT
                    p.id, 
                    p.CONCILIADO,
                    p.VALOR_PAGO,
                    p.idNota,
                    p.TR,
                    p.NUM_DOCUMENTO,
                    DATE_FORMAT({$cond_data_parcela},'%d/%m/%Y') as data_pg,
                    n.tipo as tipo_nota,
                    f.razaoSocial as fornecedor,
                    n.codFornecedor as fornecedor_id,
                    DATE_FORMAT({$cond_data_parcela},'%Y-%m-%d') as data_pg_us,
                    'parcela' AS tipo_item
	       FROM
                    parcelas as p INNER JOIN
                    contas_bancarias AS o ON (p.origem = o.id) INNER JOIN
                    notas as n on (p.idNota = n.idNotas) INNER JOIN
                    fornecedores as f on (n.codFornecedor = f.idFornecedores)
	       WHERE
			        p.status = 'Processada' AND
	                n.status != 'Cancelada' AND
			        p.origem = {$id_conta} AND
                    (p.nota_id_fatura_aglutinacao is null or p.nota_id_fatura_aglutinacao = 0) AND
                    
			        {$cond_data_parcela} <= '{$fim1}' AND
			        {$cond_data_parcela} >= '{$data_saldo_inicial_us}' AND
			        {$cond_conciliado}

UNION       
SELECT 
		ID,
		(case when `CONTA_ORIGEM`={$id_conta}  then CONCILIADO_ORIGEM when `CONTA_DESTINO`={$id_conta}  then  CONCILIADO_DESTINO end) as CONCILIADO,
		( case when `CONTA_ORIGEM`={$id_conta}  then `VALOR` when `CONTA_DESTINO`={$id_conta} then  VALOR end) as VALOR_PAGO,
		0 as idNota,
		'0' as TR,
		'Transferência' as NUM_DOCUMENTO,
		(CASE 
                        WHEN
                        `CONTA_ORIGEM`={$id_conta}   AND  CONCILIADO_ORIGEM ='S'
                        THEN
                        DATE_FORMAT(`DATA_CONCILIADO_ORIGEM`,'%d/%m/%Y') 
                        WHEN
                        `CONTA_DESTINO`={$id_conta}   AND  CONCILIADO_DESTINO ='S'
                        THEN  DATE_FORMAT(`DATA_CONCILIADO_DESTINO`,'%d/%m/%Y')  
                        ELSE
                        DATE_FORMAT(`DATA_TRANSFERENCIA`,'%d/%m/%Y')

                        end) as data_pg,

		( case when `CONTA_ORIGEM`={$id_conta} then '1' when `CONTA_DESTINO`={$id_conta}  then '0' end) as tipo_nota,
		( case when `CONTA_ORIGEM`={$id_conta}  THEN (
												SELECT  
														concat(o.SIGLA_BANCO,'-',c.AGENCIA,'-',c.NUM_CONTA,'-',e.SIGLA_EMPRESA)
												FROM
														contas_bancarias as c INNER JOIN
														origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id) INNER JOIN 
														empresas as e ON (c.UR = e.id) 
												WHERE
														c.ID= CONTA_DESTINO
											)
		
			  WHEN `CONTA_DESTINO`={$id_conta}  THEN (
											 SELECT  
													concat(o.SIGLA_BANCO,'-',c.AGENCIA,'-',c.NUM_CONTA,'-',e.SIGLA_EMPRESA)
											 FROM
													contas_bancarias as c INNER JOIN
													origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id) INNER JOIN 
													empresas as e ON (c.UR = e.id) 
											 WHERE 
													c.ID = CONTA_ORIGEM
											)							
											 END
		) as fornecedor,
		( case when `CONTA_ORIGEM`={$id_conta} then CONTA_DESTINO when `CONTA_DESTINO`={$id_conta}  then  CONTA_ORIGEM end) as fornecedor_id,
                    (CASE 
                        WHEN
                        `CONTA_ORIGEM`={$id_conta}   AND  CONCILIADO_ORIGEM ='S'
                        THEN
                        DATE_FORMAT(`DATA_CONCILIADO_ORIGEM`,'%Y-%m-%d') 
                        WHEN
                        `CONTA_DESTINO`={$id_conta}   AND  CONCILIADO_DESTINO ='S'
                        THEN  DATE_FORMAT(`DATA_CONCILIADO_DESTINO`,'%Y-%m-%d')  
                        ELSE
                        DATE_FORMAT(`DATA_TRANSFERENCIA`,'%Y-%m-%d')
                        end) as data_pg_us,
       'transferencia' AS tipo_item
              
                   
                    
FROM
		`transferencia_bancaria` 
WHERE 
		(`CONTA_DESTINO`={$id_conta} or `CONTA_ORIGEM`={$id_conta}) and
		            IF  (  `CONTA_ORIGEM`={$id_conta}   AND  CONCILIADO_ORIGEM ='S',
                                    DATE_FORMAT(`DATA_CONCILIADO_ORIGEM`,'%Y-%m-%d') ,
                                     IF (                    
                                        `CONTA_DESTINO`={$id_conta}   AND  CONCILIADO_DESTINO ='S',
                                         DATE_FORMAT(`DATA_CONCILIADO_DESTINO`,'%Y-%m-%d'),              
                                         DATE_FORMAT(`DATA_TRANSFERENCIA`,'%Y-%m-%d')
                                    ))
                            <= '{$fim1}' AND
                                
                              IF  (  `CONTA_ORIGEM`={$id_conta}   AND  CONCILIADO_ORIGEM ='S',
                                    DATE_FORMAT(`DATA_CONCILIADO_ORIGEM`,'%Y-%m-%d') ,
                                     IF (                    
                                        `CONTA_DESTINO`={$id_conta}   AND  CONCILIADO_DESTINO ='S',
                                         DATE_FORMAT(`DATA_CONCILIADO_DESTINO`,'%Y-%m-%d'),              
                                         DATE_FORMAT(`DATA_TRANSFERENCIA`,'%Y-%m-%d')
                                    ))>= '{$data_saldo_inicial_us}' AND

                     CANCELADA_POR is NULL AND
                     (CASE
                        WHEN `CONTA_ORIGEM`={$id_conta} THEN CONCILIADO_ORIGEM = '{$conciliado}'
                        WHEN `CONTA_DESTINO`={$id_conta}  THEN  CONCILIADO_DESTINO = '{$conciliado}'
                      END)
                      UNION
		SELECT
			nb.id,
			nb.conciliado as CONCILIADO,
			nb.valor ,
			0 AS idNota,
			0 AS TR,
			nb.numero_documento as NUM_DOCUMENTO,
			DATE_FORMAT({$cond_data_baixa}, '%d/%m/%Y') AS data_pg,
			nb.tipo_movimentacao AS tipo_nota,
			'Devolução Adiantamento' as fornecedor,
			0 AS fornecedor_id,			
			{$cond_data_baixa} AS data_pg_us,
		    'devolucao' AS tipo_item	
        FROM
            nota_baixas as nb             
        WHERE
            nb.conta_bancaria_id = {$id_conta}
            AND
            nb.canceled_by is null
            AND
            $cond_conciliado_baixa AND 
            {$cond_data_baixa} <= '{$fim1}' AND
            {$cond_data_baixa} >= '{$data_saldo_inicial_us}' 
        {$unionTarifas}
        {$unionEstorno}
        ORDER BY
            data_pg_us";

            

    $result = mysql_query($sql);
    $numero_linhas = mysql_num_rows($result);
    
    /*if($numero_linhas == 0){
        echo "<br><br><br>";
        echo "</br><p><center><b>Nenhum item encontrado</b></center></p>";
        return;
    }else{*/
    echo "<br><br><br>";
    $label_data = $conciliado == 'N' ? "DATA DO PROCESSAMENTO" : "DATA COMPENSADO";

    echo "<table class='mytable' width='100%' >
                    <thead>
                        <tr>
                            <th></th>
                            <th width='7%'>$label_data</th>
                            <th>TR</th>
                            <th>Fornecedor</th>
                            <th>N&deg; Documento</th>
                            <th>ENTRADA </th>
                            <th>SAÍDA </th>
                            <th>SALDO</th>
                        </tr>
                    </thead>";
    if($data_saldo_inicial_us >= $inicio1){

        echo "<tr class='dados' valor_pago='{$saldo_inicial}' id_parcela={$row['id']} conciliado='".$row['CONCILIADO']."' tipo='".$row['tipo_nota']."' tr='".$row['idNota']."-".$row['TR']."'  data='".$row['data_pg']."' fornecedor = '".$row['fornecedor_id']."'  >
                           <td></td>
                           <td></td>
                           <td>{$data_saldo_inicial_pt}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Saldo Inicial</td><td></td>
                            <td $cor>R$ ".number_format($saldo_inicial,2,',','.')."</td>			        
                            </tr>";

    }
    //}
    $cont_saldo_dia_anterior=0;
    $numeroLinhas = mysql_num_rows($result);
    
    $aux = 0;
    $entradas = 0.00;
    $saidas = 0.00;
    $corEnt   = "style='font-size:12px; color:blue;'";
    $corSaida   = "style='font-size:12px; color:red;'";
    while($row = mysql_fetch_array($result)){
        ////tipo da parcela 1-saida ou 0-entrada
        $cor = "";
        $cor_saldo = "";
        $entrada = 0.00;
        $saida = 0.00;
        if($row['tipo_nota'] == 0){
            $total += $row['VALOR_PAGO'];
           // $entradas += $row['VALOR_PAGO'];
        }else{
            $total -= $row['VALOR_PAGO'];
           // $saidas -= $row['VALOR_PAGO'];
        }

        $aux++;

        if($row['data_pg_us'] >= $inicio1 && $row['data_pg_us'] <= $fim1){
            if($cont_saldo_dia_anterior == 0 ){
                $saldo_dia_anterior = $auxMovimentacaoAnterior == 0 ? $saldo_inicial : $saldo_dia_anterior;
                echo "<tr class='dados' valor_pago='{$row['VALOR_PAGO']}' id_parcela={$row['id']} conciliado='".$row['CONCILIADO']."' tipo='".$row['tipo_nota']."' tr='".$row['idNota']."-".$row['TR']."'  data='".$row['data_pg']."' fornecedor = '".$row['fornecedor_id']."'  >
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td><b>SALDO DO DIA ANTERIOR</b></td>
                                 <td>R$ ".number_format($saldo_dia_anterior,2,',','.')."</td>
                                 </tr>";

            }
            if($row['tipo_nota'] == 0){
                $entrada = $row['VALOR_PAGO'];
            }else{
                $saida = $row['VALOR_PAGO'];
            }

            $desfazerEstorno = "";
            if($row['tipo_item'] == 'estorno') {
                $desfazerEstorno = <<<HTML
<a href='#'
    style="a:visited: black" 
    title='Desfazer Estorno'
    class='desfazer-estorno-parcela' 
    credito-id='{$row['idNota']}' 
    estorno-id='{$row['id']}'>
    <img src="/utils/cancelada_16x16.jpg">
</a>
HTML;
            }
            $cor_saldo = "style='font-size:12px; color:blue;'";
            if($total < 0) {
                $cor_saldo = "style='font-size:12px; color:red;'";
            }
            if($row['tipo_nota'] == 0){
              
                $entradas += $row['VALOR_PAGO'];
            }else{
                
                $saidas -= $row['VALOR_PAGO'];
            }
            echo "<tr class='dados {$row['tipo_item']}-{$row['id']}' valor_pago='{$row['VALOR_PAGO']}' id_parcela={$row['id']} conciliado='".$row['CONCILIADO']."' tipo='".$row['tipo_nota']."' tr='".$row['idNota']."-".$row['TR']."'  data='".$row['data_pg']."' fornecedor = '".$row['fornecedor_id']."'  >
                     <td>{$desfazerEstorno}</td>
                     <td>{$row['data_pg']}</td>
                     <td>{$row['idNota']}-{$row['TR']} </td>
                     <td>{$row['fornecedor']}</td>
                     <td>{$row['NUM_DOCUMENTO']}</td>
                     <td $corEnt>R$ ".number_format($entrada,2,',','.')."</td>
                     <td $corSaida>R$ ".number_format($saida,2,',','.')."</td>
                     <td {$cor_saldo}>R$ ".number_format($total,2,',','.')."</td>
                  </tr>";

            $cont_saldo_dia_anterior ++;
        }else{
            $saldo_dia_anterior = $total;
            $auxMovimentacaoAnterior ++;
            if( $numeroLinhas == $aux){
                $saldo_dia_anterior = $auxMovimentacaoAnterior == 0 ? $saldo_inicial :$saldo_dia_anterior;
                echo "<tr class='dados' valor_pago='{$row['VALOR_PAGO']}' id_parcela={$row['id']} conciliado='".$row['CONCILIADO']."' tipo='".$row['tipo_nota']."' tr='".$row['idNota']."-".$row['TR']."'  data='".$row['data_pg']."' fornecedor = '".$row['fornecedor_id']."'  >
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td><b>SALDO DO DIA ANTERIOR</b></td>
                                 <td>R$ ".number_format($saldo_dia_anterior,2,',','.')."</td>
                                 </tr>";

            }
        }
    }
    $cor_saldo = "style='font-size:12px; color:blue;'";
    if($total < 0) {
        $cor_saldo = "style='font-size:12px; color:red;'";
    }
    echo "<tr>
            <td colspan='8' align='right'><b>SALDO FINAL</b></td>
          </tr>
          <tr class='dados' valor_pago='{$row['VALOR_PAGO']}' id_parcela={$row['id']} conciliado='".$row['CONCILIADO']."' tipo='".$row['tipo_nota']."' tr='".$row['idNota']."-".$row['TR']."'  data='".$row['data_pg']."' fornecedor = '".$row['fornecedor_id']."'  >
             <td colspan='6' {$corEnt} align='right'>R$ ".number_format($entradas,2,',','.')."</td>
             <td {$corSaida} align='right'>R$ ".number_format($saidas,2,',','.')."</td>
             <td {$cor_saldo}>R$ ".number_format($total,2,',','.')."</td>
          </tr>";
    
    echo "</table>";
    echo" <form action='exportar_excel_extrato.php?conta={$id_conta}&conciliado={$conciliado}&inicio={$inicio1}&fim={$fim1}' target='_blank' method='post'>
                 <button   class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover ' type='submit' role='button' aria-disabled='false'>
                          <span class='ui-button-text' >Exportar Excel</span>
                 </button>
           </form>";
}

//Eriton Salvar contas 04-06-2013
function salvar_contas($post){
	
	extract($post,EXTR_OVERWRITE);
	
	mysql_query('begin');

	 $iduser = $_SESSION['id_user'];
   $tipo_conta_bancaria = anti_injection($id_tipo, 'numerico');
   $saldo_inicial =str_replace('.','',$saldo_inicial);
	 $saldo_inicial =str_replace(',','.',$saldo_inicial);
	 $data = implode("-",array_reverse(explode("/",$data)));
   $id_banco= anti_injection($id_banco,'numerico');
   $agencia= anti_injection($agencia,'literal');
   $conta= anti_injection($conta,'literal');
   $ur= anti_injection($ur,'numerico');
   $status= anti_injection($status,'literal');
   $ipcc = anti_injection($ipcc,'numerico');
         
         
	$sql = "INSERT INTO contas_bancarias (TIPO_CONTA_BANCARIA_ID, ORIGEM_FUNDOS_ID,AGENCIA,NUM_CONTA,UR,SALDO_INICIAL,DATA_SALDO_INICIAL, IPCC) VALUES
({$tipo_conta_bancaria},'{$id_banco}','{$agencia}','{$conta}','{$ur}','{$saldo_inicial}','{$data}', {$ipcc})";
	
	if(!mysql_query($sql)){
		echo"Erro: Conta não salva";
		mysql_query('rollback');
	}

savelog(mysql_escape_string(addslashes($sql)));
if($status == 'D'){
    $sql = "Update origemfundos set STATUS='A' where id='{$id_banco}' ";
    if(!mysql_query($sql)){
		echo"Erro: Ao ativar banco";
		mysql_query('rollback');
	}
        savelog(mysql_escape_string(addslashes($sql)));
}

mysql_query('commit');

echo "1";
	
}

function editar_contas($post){
	
	extract($post,EXTR_OVERWRITE);
	
	mysql_query('begin');
  $iduser = $_SESSION['id_user'];
  $id_tipoConta = anti_injection($id_tipo,'numerico');
  $id_conta= anti_injection($id_conta,'numerico');
  $id_banco= anti_injection($id_banco,'numerico');
  $agencia= anti_injection($agencia,'literal');
  $conta= anti_injection($conta,'literal');
  $id_conta= anti_injection($id_conta,'numerico');
  $ipcc = anti_injection($ipcc,'numerico');
         
         
	$sql = "update contas_bancarias set TIPO_CONTA_BANCARIA_ID = {$id_tipoConta}, ORIGEM_FUNDOS_ID=$id_banco,AGENCIA='{$agencia}',NUM_CONTA='{$conta}', IPCC
= {$ipcc}
where ID ={$id_conta}";
	
	if(!mysql_query($sql)){
		echo"Erro: Conta não salva";
		mysql_query('rollback');
	}

    savelog(mysql_escape_string(addslashes($sql)));
    mysql_query('commit');
    echo "1";
	
}



//Fim Eriton Salvar contas 04-06-2013
function salvar_conciliacao($post){

	extract($post,EXTR_OVERWRITE);
 
 	mysql_query("BEGIN");
 	//Insere o cabe�alho da concilia��o
  	$fim = implode("-",array_reverse(explode("/",$fim)));
  	$inicio= implode("-",array_reverse(explode("/",$inicio))); 
  	$iduser=$_SESSION['id_user'];
 	$sql ="INSERT 
              INTO conciliacao_bancaria (CONTAS_BANCARIAS_ID,DATA,SALDO_CONCILIADO,SALDO_NCONCILIADO,USUARIO_ID,DATA_INICIO,DATA_FIM) VALUES ('{$banco_id}',now(),'{$saldo_conciliado}','{$saldo_n_conciliado}','{$iduser}','{$inicio}','{$fim}')";
  	
 	if(!mysql_query($sql)){
   		echo"Erro: No insert em conciliacao_bancaria ";
  		mysql_query("ROLLBACK");
  	}
  
  	///pega o id da conciliacao bancaria
  	$id_conciliacao = mysql_insert_id();
  	savelog(mysql_escape_string(addslashes($sql)));
  
  	//Separa os itens da concilia��o
  	//Salva os itens da concilia��o na tabela correspondente	
	
    $saldo_conciliado=0;
    $itens = explode("$",$linha);
	foreach($itens as $item){
		$con++;
		$i = explode("#",str_replace("undefined","",$item));
		
		$tr = anti_injection($i[0],'literal');
		$tipo_nota = anti_injection($i[1],'literal');
		$fornecedor = anti_injection($i[2],'literal');
		$data_pagamento = anti_injection($i[3],'literal');
		$data_pagamento = implode("-",array_reverse(explode("/", $data_pagamento)));
		$conciliado = anti_injection($i[4],'literal');
		$valor = anti_injection($i[5],'numerico');
                //  $valor=parseInt($valor);
		$num_documento = anti_injection($i[6],'literal');
		$id_parcela = $i[7];
                $transferencia = anti_injection($i[8],'literal');
                $data_conciliado= implode("-",array_reverse(explode("/",$i[9]))); 
                $alterado =anti_injection($i[10],'literal');
                //$tipo_nota = 0 entrada, $tipo_nota = 1 saída*/
                if($alterado=='S'){
                $saldo_conciliado = $tipo_nota == 1 ? $saldo_conciliado-$valor : $saldo_conciliado+$valor;
               
                }
               if($conciliado == 'N'){
                    $data_conciliado = '0000-00-00';
                }else{
                $c .= $data_conciliado;
                }
                if($transferencia == 'N'){
                            $sql = "INSERT INTO `itens_conciliacao_bancaria`
                        (`CONCILIACAO_BANCARIA_ID`, `DATA_PAGAMENTO`, `TR_PARCELA`, `FORNECEDOR_ID`, `NUM_DOCUMENTO`, `TIPO_NOTA`, `CONCILIADO`, `VALOR`,`TRANSFERENCIA_BANCARIA`,DATA_CONCILIADO) 
                        VALUES 
                        ('{$id_conciliacao}','{$data_pagamento}','{$tr}','{$fornecedor}','{$num_documento}','{$tipo_nota}','{$conciliado}','{$valor}','{$transferencia}','{$data_conciliado}')";

                    $r = mysql_query($sql);
                    if (!$r) {
                        echo mysql_error();
                        mysql_query("ROLLBACK");
                        return;
                      }
                      savelog(mysql_escape_string(addslashes($sql)));


                    $sql = "Update parcelas set CONCILIADO = '{$conciliado}' , DATA_CONCILIADO='{$data_conciliado}' where id = {$id_parcela} ";
                    $r = mysql_query($sql);
                    if (!$r) {
                        echo mysql_error();
                        mysql_query("ROLLBACK");
                        return;
                      }
                      savelog(mysql_escape_string(addslashes($sql)));
               }else{
                    $sql = "INSERT INTO `itens_conciliacao_bancaria`
                        (`CONCILIACAO_BANCARIA_ID`, `DATA_PAGAMENTO`, `TRANSFERENCIA_BANCARIA_ID`, `FORNECEDOR_ID`, `NUM_DOCUMENTO`, `TIPO_NOTA`, `CONCILIADO`, `VALOR`,`TRANSFERENCIA_BANCARIA`,DATA_CONCILIADO) 
                        VALUES 
                        ('{$id_conciliacao}','{$data_pagamento}','{$id_parcela}','{$fornecedor}','{$num_documento}','{$tipo_nota}','{$conciliado}','{$valor}','{$transferencia}','{$data_conciliado}')";

                    $r = mysql_query($sql);
                    if (!$r) {
                        echo mysql_error();
                        mysql_query("ROLLBACK");
                        return;
                      }
                      savelog(mysql_escape_string(addslashes($sql)));
                      if($tipo_nota==0){
                           $sql = "Update transferencia_bancaria set CONCILIADO_DESTINO = '{$conciliado}', DATA_CONCILIADO_DESTINO='{$data_conciliado}' where id = {$id_parcela} ";
                    $r = mysql_query($sql);
                    if (!$r) {
                        echo mysql_error();
                        mysql_query("ROLLBACK");
                        return;
                      }
                      savelog(mysql_escape_string(addslashes($sql)));
                      }else{
                           $sql = "Update transferencia_bancaria set CONCILIADO_ORIGEM = '{$conciliado}', DATA_CONCILIADO_ORIGEM='{$data_conciliado}' where id = {$id_parcela} ";
                    $r = mysql_query($sql);
                    if (!$r) {
                        echo mysql_error();
                        mysql_query("ROLLBACK");
                        return;
                      }
                      savelog(mysql_escape_string(addslashes($sql)));
                          
                      }
                   
               }
		
	}
        

  	//Atualiza o saldo
  	
  	$sql_saldo_banco = "UPDATE `contas_bancarias` 
  	SET 
  	`SALDO`=`SALDO` + {$saldo_conciliado},`DATA_ATUALIZACAO_SALDO`=NOW(),`SALDO_NCONCILIADO`=`SALDO_NCONCILIADO`- {$saldo_conciliado} 
  	WHERE ID = '{$banco_id}'";
  	
  	$result_saldo_banco = mysql_query($sql_saldo_banco);
  	if(!$result_saldo_banco){
  		echo mysql_error();
		mysql_query("ROLLBACK");
		return;
  	}
        savelog(mysql_escape_string(addslashes($sql)));
        $sql="Update
                conciliacao_bancaria,
                contas_bancarias
            set 
                conciliacao_bancaria.SALDO_CONCILIADO=contas_bancarias.SALDO,
                conciliacao_bancaria.SALDO_NCONCILIADO=contas_bancarias.SALDO_NCONCILIADO
            where            
                conciliacao_bancaria.ID ='{$id_conciliacao}' and
                contas_bancarias.ID='{$banco_id}'
                ";
                $result = mysql_query($sql);
  	if(!$result){
  		echo mysql_error();
		mysql_query("ROLLBACK");
		return;
  	}
  
  	mysql_query("COMMIT");
  	echo "1";

}//salvar_conciliacao


//////fim  2013-05-21 pesquisar-conciliacao
///////////////////////04/09/2013 transferencia bancaria salvar

function salvar_transferencia($post){
    extract($post,EXTR_OVERWRITE);
    mysql_query("BEGIN");   
    $fechamento_caixa = new \DateTime(\App\Controllers\FechamentoCaixa::getFechamentoCaixa()['data_fechamento']);
	$ent = $data;
	$data_entrada_obj = \DateTime::createFromFormat('d/m/Y', $ent);
	if($data_entrada_obj <= $fechamento_caixa) {
        echo "Data da transferência (".$data_entrada_obj->format('d/m/Y').") menor qua a data de fechamento de caixa (".$fechamento_caixa->format('d/m/Y').")";
        mysql_query("rollback");
        return;
    }
    $sql = "Insert into transferencia_bancaria (USUARIO_ID,DATA,DATA_TRANSFERENCIA,CONTA_ORIGEM,CONTA_DESTINO,VALOR,NUM_DOCUMENTO_BANCARIO,DESCRICAO) values ('{$_SESSION['id_user']}',now(),'{$data_entrada_obj->format('Y-m-d')}','{$conta_origem}','{$conta_destino}','{$valor_transferido}','{$doc_bancario}','{$descricao}')";
    $result= mysql_query($sql);
    if(!$result){
  		echo mysql_error();	
                mysql_query("ROLLBACK");
		return;
  	}
        savelog(mysql_escape_string(addslashes($sql)));
  
  	mysql_query("COMMIT");
        echo '1';
}

function pesquisar_transferencia($post){
    extract($post,EXTR_OVERWRITE);
    $inicio= implode("-",array_reverse(explode("/",$inicio))); 
    $fim= implode("-",array_reverse(explode("/",$fim)));
    $data_fechamento_obj = new \DateTime(\App\Controllers\FechamentoCaixa::getFechamentoCaixa()['data_fechamento']);

    $cont_data=0;
    $cont_destino = 0;
    $cont_origem = 0;
    
    if($inicio != ''){
        $cond_data= " AND `DATA_TRANSFERENCIA` between '{$inicio}' and '{$fim}'";
        $cont_data++;
    }

    $cond_origem = '';

    if($conta_origem != 't'){

        $cond_origem = "`CONTA_ORIGEM` = '{$conta_origem}'";
        if($cont_data > 0){

          $cond_origem = " AND  {$cond_origem}";  
        }
        $cont_origem++;
    }else{
        // caso não seja MAtriz só traz caixa fixo
        
        $tipoConta = $_SESSION['empresa_principal'] == 1 ? NULL : 4;
        
        $contasBancarias = Financeiro::getContasBancariasByURs($_SESSION['empresa_user'],$tipoConta);
        
        foreach($contasBancarias as $contas) {
            $arrayContas[] = $contas['id'];
        }
        $listContas = implode(',',$arrayContas);
        $cond_origem = "AND `CONTA_ORIGEM` in ($listContas) ";
    }

    $cond_destino = '';
    if($conta_destino != 't'){
        $cond_destino ="`CONTA_DESTINO`='{$conta_destino}'";
        if($cont_data >0 || $cont_origem>0){
          $cond_destino = " AND  {$cond_destino}";  
        }
       $cond_destino++;
    }
    
    $sql="
SELECT 
ID,
DATA_TRANSFERENCIA AS data_transf,
DATE_FORMAT(t.DATA_TRANSFERENCIA,'%d/%m/%Y') as data,
(select  
  concat(o.SIGLA_BANCO,'-',c.AGENCIA,'-',c.NUM_CONTA,'-',e.SIGLA_EMPRESA)
  FROM
  contas_bancarias as c INNER JOIN
  origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id) 
  INNER JOIN empresas as e ON (c.UR = e.id) 
  where 
  c.ID= t.CONTA_ORIGEM) as origem,
(select  
   concat(o.SIGLA_BANCO,'-',c.AGENCIA,'-',c.NUM_CONTA,'-',e.SIGLA_EMPRESA)
   FROM
   contas_bancarias as c INNER JOIN
   origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id) 
   INNER JOIN empresas as e ON (c.UR = e.id) 
   where 
    c.ID= t.CONTA_DESTINO) as destino,
t.VALOR,
NUM_DOCUMENTO_BANCARIO as num_doc,
DESCRICAO,
CONCILIADO_ORIGEM,
CONCILIADO_DESTINO
FROM 
`transferencia_bancaria` as t
WHERE 
 CANCELADA_POR is NULL
 {$cond_data}  
 {$cond_origem}  
 {$cond_destino} 
 order by data";

        
        $result = mysql_query($sql);
	$numero_linhas = mysql_num_rows($result);
	if($numero_linhas == 0){
		echo "<br><br>";
                echo "</br><p><center><b>Nenhum transfer&ecirc;ncia  encontrado</b></center></p>";
	}else{
		echo "<br><br><br>";
                
		echo "<table class='mytable' width='100%' >
                <thead>
                    <tr>
                    <th><input type='checkbox' class='marcar-transferencias' title='Selecionar todas'></th>
                    <th> DATA</th>
                    <th>CONTA ORIGEM</th>
                    <th>CONTA DESTINO</th>
                    <th  width='10%'>VALOR</th>
                    <th>NUM. DOC. BANC&Aacute;RIO</th>
                    <th>DESCRI&Ccedil;&Atilde;O</th>
                    </tr>
                </thead>";
        
                while($row = mysql_fetch_array($result)){
                    $cancelar='';
                    if(($row['CONCILIADO_DESTINO'] == 'N' && $row['CONCILIADO_ORIGEM']=='N')) {
                        $data_transferencia_obj = new \DateTime($row['data_transf']);
                        if ($data_fechamento_obj <= $data_transferencia_obj) {
                            $cancelar = "<input type='checkbox' class='item-transferencia' idTransferencia='{$row['ID']}' title='Cancelar' >";
                        }
                    }
               		echo "<tr  >
                            <td>$cancelar</td>
                            <td>{$row['ID']}   {$row['data']}</td>
                            <td>{$row['origem']}</td>
                            <td>{$row['destino']}</td>
                            <td >R$ ".number_format($row['VALOR'],2,',','.')."</td>
                            <td>{$row['num_doc']}</td>
                            <td>{$row['DESCRICAO']}</td>
                            </tr>";

                }
                echo"</table>";
                echo"<form method=post target='_blank' action='imprimir_transferencia.php?i={$inicio}&f={$fim}&origem={$conta_origem}&destino={$conta_destino}'>";
                if(isset($_SESSION['permissoes']['financeiro']['financeiro_transferencia_bancaria']['financeiro_transferencia_bancaria_cancelar']) || $_SESSION['adm_user'] == 1){
                    echo "<button id='cancelar-transferencia' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='button' role='button' aria-disabled='false'>
				<span class='ui-button-text'>Cancelar Transferências</span>
			  </button>";
                }

		echo "
              <button id='imprimir_score' target='_blank' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
				<span class='ui-button-text'>Imprimir</span>
			  </button>";
		echo "</form>";
                
                
                
        }	
     
}
//////////////////////////fim transferencia

///////// jeferson salvar-porcentagem-ipcg 16-09-2013
function salvar_porcentagem_ipcg($post){
     extract($post,EXTR_OVERWRITE);
     mysql_query('begin');
     
     ///////separa os itens concatenados em umalinha para serem salvos no banco.
   $itens = explode("$",$linha);
	foreach($itens as $item){
		$con++;
		$i = explode("#",str_replace("undefined","",$item));
                $n3 = anti_injection($i[0],'literal');;
                $cod_n3=anti_injection($i[1],'literal');;
                $cod_n2=anti_injection($i[2],'literal');;
                $porcento=anti_injection($i[3],'numerico');;
                $ur=anti_injection($i[4],'literal');;
                $anterior=anti_injection($i[5],'numerico');;
                $n2=anti_injection($i[6],'literal');
                 ///////////Verificar se  $anterior é diferente de 0, o que siginifica que esse item ipcg já tem porcentagem cadastrada e seu status deve ser mudado para inativo. 
                if($anterior != 0){
                    $sql ="update  planocontas_porcento_ur set STATUS=0 where ID='{$anterior}'";
                    $result = mysql_query($sql);
                    if(!$result){
                         echo mysql_error();
                        mysql_query("roolback");
                       
                        return;
                    }
                    savelog(mysql_escape_string(addslashes($sql)));
                    
                }
                ///////////Grava o percentual do item ipcg na tabela planocontas_porcento_ur com o istatus de ativo(1).
                    $sql ="insert into planocontas_porcento_ur ( `COD_N2`, `N3` , `COD_N3` , `N2` ,`UR` ,`DATA` , `STATUS`,PORCENTO , `USUARIO_ID`,`ANTERIOR`) values
                                                               ( '{$cod_n2}', '{$n3}', '{$cod_n3}', '{$n2}','{$ur}',now(),'1','{$porcento}','{$_SESSION['id_user']}','{$anterior}')";
                                                                  
  

                    $result = mysql_query($sql);
                    if(!$result){
                        echo mysql_error();
                        mysql_query("roolback");
                        return;
                    }
                    savelog(mysql_escape_string(addslashes($sql)));
                    
                }
                        
	
       
        mysql_query('commit');
        echo "Salvo com sucesso";
        
}
function pesquisar_porcentagem_ipcg($post) {
    extract($post, EXTR_OVERWRITE);

    
    $sql = "Select COD_N3,ID,N2,PORCENTO from planocontas_porcento_ur where UR={$empresa} and STATUS=1";
    
    $result = mysql_query($sql);
    $itens = '';

    while ($row = mysql_fetch_array($result)) {
        $itens[$row['COD_N3']] = array("anterior" => $row['ID'], "porcento" => number_format($row['PORCENTO'],2,',','.'));
    }
    //////////////////
    /*  echo "<div><p><b>RECEBIMENTO IPCG2</b></p>";
      $sql_ipcg2_recebimento ="SELECT `COD_N2`,`N2` FROM `plano_contas` WHERE `N1`='R' group by `COD_N2`,`N2`";
      $result_ipcg2_recebimento=mysql_query($sql_ipcg2_recebimento);
      $resposta_ipcg2_recebimento = mysql_num_rows($result_ipcg2_recebimento);
      if($resposta_ipcg2_recebimento == 0){
      echo "<p>Nenhum resultado encontrado!</p>";
      }else{
      echo "<table class='mytable' width=50% ><thead>
      <tr>
      <th>COD. ITEM</th>
      <th>ITEM</th>
      <th>PORCENTAGEM</th>
      </tr>
      </thead>";
      while($row = mysql_fetch_array($result_ipcg2_recebimento)){

      ///$data =join("/",array_reverse(explode("-",$row['DATA'])));


      echo "<tr><td>{$row['COD_N2']}</td><td>{$row['N2']}</td><td><input type='text' ></input></td></tr>";

      }
      echo "</table>";
      }
      echo"</div>";
      echo "<div><p><b>DESEMBOLSO IPCG2</b></p>";
      $sql_ipcg2_desembolso="SELECT `COD_N2`,`N2` FROM `plano_contas` WHERE `N1`='D' group by `COD_N2`,`N2`";
      $result_ipcg2_desembolso=mysql_query($sql_ipcg2_desembolso);
      $resposta_ipcg2_desembolso= mysql_num_rows($result_ipcg2_desembolso);
      if($resposta_ipcg2_desembolso == 0){
      echo "<p>Nenhum resultado encontrado!</p>";
      }else{
      echo "<table class='mytable' width=50% ><thead>
      <tr>
      <th>COD. ITEM</th>
      <th>ITEM</th>
      <th>PORCENTAGEM</th>
      </tr>
      </thead>";
      while($row = mysql_fetch_array($result_ipcg2_desembolso)){

      ///$data =join("/",array_reverse(explode("-",$row['DATA'])));


      echo "<tr><td>{$row['COD_N2']}</td><td>{$row['N2']}</td><td><input type='text' ></input></td></tr>";

      }
      echo "</table>";
      }
      echo " </div>";
     */
    echo "<div>";
        echo "<center><h1>{$nome_empresa}</h1></center>";
        echo"<p><b>RECEBIMENTO IPCF3</b></p>";
    $sql = "SELECT
             ID,
             N2,
             COD_N2,
             `COD_N3`,
             `N3` 
             FROM 
             `plano_contas` 
             WHERE 
             `N1`='R' and
              ID >153
             group by 
             `N3`,
             `COD_N2` order BY COD_N3";
    $result = mysql_query($sql);
    $resposta_ipcg3_recebimento = mysql_num_rows($result);
    if ($resposta_ipcg3_recebimento == 0) {
        echo "<p>Nenhum resultado encontrado!</p>";
    } else {
        
        echo "<table class='mytable' width=100% ><thead>
			<tr>
                        <th></th>
                        <th>IPCF2</th>
                        <th>COD. IPCF3</th>
			<th>IPCF3</th>
                        <th>PERCENTUAL IPCF3</th>
                        </tr>
			</thead>";
        while ($row = mysql_fetch_array($result)) {
           

            if (array_key_exists($row['COD_N3'], $itens)) {
               $porcento = $itens[$row['COD_N3']]['porcento'];
               $anterior = $itens[$row['COD_N3']]['anterior'];
                
            }else{
                $porcento = '0,00';
                $anterior = '0';
            }
            ///$data =join("/",array_reverse(explode("-",$row['DATA'])));


            echo "<tr><td><input type='checkbox' class='alterar_porcento' porcento='{$porcento}' ></td><td>{$row['N2']}</td><td>{$row['COD_N3']}</td><td>{$row['N3']}</td><td><input type='text' class='valor porcento_recebimento' n2='{$row['N2']}' n3='{$row['N3']}' cod_n3='{$row['COD_N3']}' cod_n2='{$row['COD_N2']}' value='{$porcento}' disabled='disabled' anterior='{$anterior}' ur='{$empresa}'></input></td></tr>";
        }
        echo "</table>";
    }



    echo"</div>";
    echo "<div><p><b>DESEMBOLSO IPCF3</b></p>";
    $sql = "SELECT 
        N2,
        COD_N2,
        `COD_N3`
        ,`N3` 
        FROM 
        `plano_contas` 
        WHERE 
        `N1`='D' and
              ID >153 
              group by 
              `N3`,
              `COD_N2` 
              order BY 
              COD_N3";
    $result = mysql_query($sql);
    $resposta_ipcg3_desembolso = mysql_num_rows($result);
    if ($resposta_ipcg3_desembolso == 0) {
        echo "<p>Nenhum resultado encontrado!</p>";
    } else {
        echo "<table class='mytable' width=100% ><thead>
			<tr><th></th>
                        <th>IPCF2</th>
                        <th>COD. IPCF3</th>
			<th>IPCF3</th>
                        <th>PERCENTUAL IPCF3</th>
                        </tr>
			</thead>";
        while ($row = mysql_fetch_array($result)) {

            if (array_key_exists($row['COD_N3'], $itens)) {
               $porcento = $itens[$row['COD_N3']]['porcento'];
                $anterior = $itens[$row['COD_N3']]['anterior'];
                
            }else{
                $porcento = '0,00';
                $anterior = '0';
            }


            echo "<tr><td><input type='checkbox' class='alterar_porcento' porcento='{$porcento}' ></td><td>{$row['N2']}</td><td>{$row['COD_N3']}</td><td>{$row['N3']}</td><td><input type='text' class='valor porcento_desembolso' n3='{$row['N3']}' n2='{$row['N2']}' cod_n3='{$row['COD_N3']}' cod_n2='{$row['COD_N2']}' value='{$porcento}' disabled='disabled' anterior='{$anterior}' ur='{$empresa}'></input></td></tr>";
        }
        echo "</table>";
    }


    echo"</div>";
    echo"<span id='span-salvar-porcentagem-ipcg'><button id='salvar-porcentagem-ipcg' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button' style='float:left;' type='button'>
              <span class='ui-button-text'>Salvar</span>
              </button></span>";
    ////////////////////
    return;
}
///////// fim  salvar-porcentagem-ipcg 16-09-2013


/////////// Jeferson 2013-12-12 enviar e-mail quando no rateio de uma nota tiver uma UR diferente da do usuário que cadastrou a mesma no sistema.

function enviar_email_rateio($ur_rateio,$nota,$edt){

    $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
    $mail->IsSMTP(); // telling the class to use SMTP

    try {
        $mail->SMTPAuth = true;                  // enable SMTP authentication
        //$mail->SMTPSecure = "TLS";                 // sets the prefix to the servier
        $mail->Host = "smtp.mederi.com.br";      // sets GMAIL as the SMTP server
        $mail->Port = 587;                   // set the SMTP port for the GMAIL server
        $mail->Username = "avisos@mederi.com.br";  // GMAIL username
        $mail->Password = "2017avisosistema";            // GMAIL password
        foreach($ur_rateio as $ur){
           if($ur == '1' ){//email por unidade regional
           $mail->AddAddress('financeiro@mederi.com.br', ' - Feira de Santana');
          } else if($ur == '2'){
              $mail->AddAddress('adm.salvador@mederi.com.br', '- ssa');
          } else if($ur == '4'){
                $mail->AddAddress('adm.sulbahia@mederi.com.br', '- sulbahia');
          } else if($ur == '5'){
               $mail->AddAddress('adm.alagoinhas@mederi.com.br', '- alagoinhas');
          } else if($ur == '7'){      
              $mail->AddAddress('adm.sudoeste@mederi.com.br', '-- sudoeste');
          } else if($ur == '9'){  
              $mail->AddAddress('adm.brasilia@mederi.com.br', '-- bsb');
          }else{
               $mail->AddAddress('financeiro@mederi.com.br', ' - Feira de Santana');
          }
  		
        }
          
         
        $mail->SetFrom('avisos@mederi.com.br', 'Avisos de Sistema');
        $mail->Subject = 'TESTE - Rateio';
        $mail->AltBody = 'Para ver essa mensagem, use um visualizador de email compat&iacute;vel com HTML'; // optional - MsgHTML will create an alternate automatically
      $mail->MsgHTML("<p>Inclusão no rateio da nota com TR={$nota} feita por ".utf8_decode($_SESSION["nome_user"])." da UR</p>".utf8_decode($_SESSION["nome_empresa"]));
        $enviado = $mail->Send();

        
    } catch (phpmailerException $e) {
      // adicionar erro numa tabela de log
    } catch (Exception $e) {
      // adicionar erro numa tabela de log
    }
    if($edt == 1){
         echo $edt;
    }else{
        echo $nota; 
    }
}

function desfazer_conciliacao($post){
    extract($post, EXTR_OVERWRITE);
    mysql_query('begin');
    
    $tipo_nota= $saida ==0.00 ? 0:1;
    $valor = $saida== 0.00 ? $entrada:$saida;
    if($transferencia == 'S' ){
        $transferencia_id = $id;
        $transferencia_parcela=0;
        if($saida== '0.00'){
            $sql= "Update transferencia_bancaria set CONCILIADO_DESTINO='N',DATA_CONCILIADO_DESTINO='0000-00-00 00:00:00' where ID={$id}";
                       
           }else{
            $sql= "Update transferencia_bancaria set CONCILIADO_ORIGEM='N',DATA_CONCILIADO_ORIGEM='0000-00-00 00:00:00' where ID={$id}";
            
         }
        
      }else{
          $transferencia_id = 0;
          $transferencia_parcela=$id;
          $sql ="Update parcelas set CONCILIADO='N', DATA_CONCILIADO ='0000-00-00 00:00:00' where id= {$id}";
        
    }
    
       $result = mysql_query($sql);
        if(!$result){
            echo mysql_error().'1';
            mysql_query("roolback");
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));
        
        $sql ="Update contas_bancarias set SALDO=(SALDO - {$entrada} + {$saida}), SALDO_NCONCILIADO=(SALDO_NCONCILIADO + {$entrada} - {$saida}) where ID= $id_banco";
        $result = mysql_query($sql);
        if(!$result){
            echo mysql_error().$sql;
            mysql_query("roolback");
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));
        
        $sql="INSERT INTO
            historico_desfazer_conciliacao_bancaria
               (`USUARIO_ID`,
                `DATA` ,
                `TRANSFERENCIA_ID`,
                `PARCELA_ID` ,
                `VALOR`,
                `TIPO_NOTA`)
            VALUES
            ({$_SESSION['id_user']},
              now(),
              $transferencia_id,
              $transferencia_parcela,
              $valor,
              $tipo_nota
              
                )
            ";
            $result = mysql_query($sql);
        if(!$result){
            echo mysql_error();
            mysql_query("roolback");
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));
           
        mysql_query('commit');
        echo 1;
    
    
    
}

function  desfazer_processamento_parcela($post){
    extract($post, EXTR_OVERWRITE);
    mysql_query('begin');
    
    $id_parcela=  anti_injection($id_parcela,'numerico');
    $justificativa= anti_injection($justificativa,'literal');
    
    $sql = "UPDATE
            parcelas 
        SET
            status='Pendente',
            DESPROCESSADA_POR='{$_SESSION['id_user']}',
            DATA_DESPROCESSADA=now(),
            JUSTIFICATIVA_DESPROCESSADA='{$justificativa}',
            obs = '$obs',                
            VALOR_PAGO = 0,
            origem = Null,
            aplicacao = Null,
            JurosMulta = 0,
            desconto = 0,
            encargos = 0,
            tarifa_cartao = 0,
            DATA_PAGAMENTO = Null,
            OUTROS_ACRESCIMOS = 0,
            NUM_DOCUMENTO = Null,                
            DATA_PROCESSADO = Null 
         WHERE
            id = {$id_parcela}";
      
      $result = mysql_query($sql);
        if(!$result){
            echo mysql_error();
            mysql_query("roolback");
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));
        
      $sql="Select
             *
             from
             parcelas
             where
             id={$id_parcela}
            ";
          $result = mysql_query($sql);

        while ($row = mysql_fetch_array($result)) {         
            
            $id_nota=$row['idNota'];
        }
        
        
        
          $sql_p="Select 
                    count(idNota)
                    from
                    parcelas
                    where 
                    status ='Processada' 
                    and
                    idNota = {$id_nota}
                    ";
   $result_p=mysql_query($sql_p);
     if(!$result_p){
            echo mysql_error();
            mysql_query("roolback");
            return;
        }
   $status_nota =mysql_result($result_p,0);
   
   $status_n= $status_nota== 0 ?'Em aberto':'Pendente';
        
          $sql="UPDATE
            notas
        SET
            status='{$status_n}'
            
         WHERE
            idNotas = $id_nota ";
            
            $result=  mysql_query($sql);
             if(!$result){
                    echo mysql_error();
                    mysql_query("roolback");
                    return;
        }
        savelog(mysql_escape_string(addslashes($sql_nova_parcela)));
        
        mysql_query("commit");
        echo 1;
  }
  
  function desfazer_processamento_nota($post){
       extract($post, EXTR_OVERWRITE);
      mysql_query('begin');    
    $id_nota=  anti_injection($id_nota,'numerico');
      $sql = <<<SQL
SELECT COUNT(*) AS qtd_aglutinadas 
FROM parcelas 
WHERE idNota = '{$id_nota}' 
AND nota_id_fatura_aglutinacao != 0
GROUP BY
idNota
SQL;
      $rs = mysql_query($sql);
      $qtdAglutinadas = current(mysql_fetch_array($rs))['qtd_aglutinadas'];

      $sql = <<<SQL
SELECT COUNT(*) AS qtd_parcelas_bordero 
FROM bordero_parcelas 
INNER JOIN parcelas ON parcela_id = parcelas.id 
inner join bordero on (bordero_parcelas.bordero_id = bordero.id)
WHERE parcelas.idNota = '{$id_nota}'
and bordero.ativo ='s'
GROUP BY idNota
SQL;
      $rs = mysql_query($sql);
      $qtdBordero = current(mysql_fetch_array($rs))['qtd_parcelas_bordero'];

      $sql = <<<SQL
SELECT COUNT(*) AS qtd_parcelas_compensada 
FROM parcelas 
WHERE parcelas.idNota = '{$id_nota}' and compensada <>'NAO'
GROUP BY idNota
SQL;
      $rs = mysql_query($sql);
      $qtdCompensada = current(mysql_fetch_array($rs))['qtd_parcelas_compensada'];


      if($qtdAglutinadas != 0 || $qtdBordero != 0 || $qtdCompensada != 0) {
          echo '2';
          return;
      }
   
   $justificativa= anti_injection($justificativa,'literal');  
        
        $sql="UPDATE
            parcelas 
        SET
            status='Pendente',
            DESPROCESSADA_POR='{$_SESSION['id_user']}',
            DATA_DESPROCESSADA=now(),
            JUSTIFICATIVA_DESPROCESSADA='{$justificativa}',
            status='Pendente',
            DESPROCESSADA_POR='{$_SESSION['id_user']}',
            DATA_DESPROCESSADA=now(),
            JUSTIFICATIVA_DESPROCESSADA='{$justificativa}',
            obs = '$obs',                
            VALOR_PAGO = 0,
            origem = Null,
            aplicacao = Null,
            JurosMulta = 0,
            desconto = 0,
            encargos = 0,
            tarifa_cartao = 0,
            DATA_PAGAMENTO = Null,
            OUTROS_ACRESCIMOS = 0,
            NUM_DOCUMENTO = Null,                
            DATA_PROCESSADO = Null,
            CONCILIADO = 'N',
            DATA_CONCILIADO = NULL
         WHERE
            idNota = $id_nota and 
            status='Processada'";
            
            $result=  mysql_query($sql);
             if(!$result){
                    echo mysql_error().$sql;
                    mysql_query("roolback");
                    return;
        }
        savelog(mysql_escape_string(addslashes($sql_nova_parcela)));
        $sql="UPDATE
            notas
        SET
            status='Em aberto'
            
         WHERE
            idNotas = $id_nota ";
            
            $result=  mysql_query($sql);
             if(!$result){
                    echo mysql_error();
                    mysql_query("roolback");
                    return;
        }
        savelog(mysql_escape_string(addslashes($sql_nova_parcela)));
        
   mysql_query('commit');
   echo 1;  
  }
function buscar_parcela_origem($parcela_origem){
    $sql = "SELECT 
                    p . * ,                                     
                    c.id AS idComp,
                    o.SIGLA_BANCO, 
                    a.forma AS af, 
                    cb.* ,
                    e.SIGLA_EMPRESA,
                    DATE_FORMAT(p.DATA_PAGAMENTO,'%d/%m/%Y' ) as data_pg,
                    tcb.nome as tip_conta,
                    motivo_juros.motivo
                    
	FROM parcelas AS p
	LEFT OUTER JOIN comprovanteparcelas AS c ON c.idParcela = p.id
	LEFT OUTER JOIN contas_bancarias AS cb ON p.origem = cb.ID
	LEFT OUTER JOIN origemfundos AS o ON cb.ORIGEM_FUNDOS_ID = o.id
	LEFT OUTER JOIN empresas AS e ON cb.UR = e.id
	LEFT OUTER JOIN aplicacaofundos AS a ON p.aplicacao = a.id
    LEFT OUTER JOIN tipo_conta_bancaria AS tcb on cb.TIPO_CONTA_BANCARIA_ID =tcb.id
    LEFT JOIN motivo_juros on p.motivo_juros_id = motivo_juros.id
       where         
       p.id = '$parcela_origem'";
    $result=  mysql_query($sql);
   while($row = mysql_fetch_array($result)){
         $valor = number_format($row['valor'],2,',','.');
         $of= $row['tip_conta']." - ".$row['SIGLA_BANCO']." - ".$row['AGENCIA']." - ".$row['NUM_CONTA']." - ".$row['SIGLA_EMPRESA'];
          $parcela = $row['TR'];
          $valor_processado= $row['valor']-($row['PIS']+$row['COFINS']+$row['ISSQN']+$row['IR']+$row['CSLL']+$row['desconto'])+$row['JurosMulta']+$row['OUTROS_ACRESCIMOS'];
          $af = $row['af'];
          $var .= "    <tr>
                       <td colspan='7'><b>Lembrete de como ela era processada.</b></td>
                       </tr>
              
                       <tr>                        
                        <td><b>Valor:</b></td> 
                        <td>R$ $valor</td>
                        <td><b>Val. Processado:</b></td>
                        <td>R$ ".number_format($valor_processado,2,',','.')."</td>
                        <td><b>Desconto:</b></td>
                        <td colspan='2'>R$ ".number_format($row['desconto'],2,',','.')."</td>

                        
                        
                       </tr>
                       <tr>
                       <td><b>Juros:</b></td>
                       <td>R$ ".number_format($row['JurosMulta'],2,',','.')."</td>
                       <td><b>Motivo Juros:</b></td>
                       <td colspan='4'>{$row['motivo']}</td>
                       </tr>";
                   
	  $var .= "<tr class='odd'>

                   <td><b>Origem/Destino:</b></td>
                   <td colspan='6'>$of</td>
                    </tr>";
          
	  $var .= "<tr>
                      <td><b>Forma Pgto</b></td>                                        
                      <td>$af</td>
                      <td><b>Tarifas Adicionais:</b> </td>
                      <td>R$ ".number_format($row['OUTROS_ACRESCIMOS'],2,',','.')." </td>
                     <td><b>N&deg; Documento: </b></td>
                      <td>{$row['NUM_DOCUMENTO']}</td>
                   </tr>
                   <tr>
                        <td ><b>Observa&ccedil;&otilde;es:</b> </td>
                        <td colspan='5'>{$row['obs']}</td>
                   </tr>";
            $var .= "<tr><td ><b>Data de Pagamento:</b></td><td colspan='5'>{$row['data_pg']} </td></tr>";
	         
    }
    
    echo $var;
}

function salvar_cadastro_servico_prestador($post){
    extract($post,EXTR_OVERWRITE);
    
    $id_fornecedor  = anti_injection($id_fornecedor,"numerico");
    $login_prestador= anti_injection($login_prestador,"numerico");
    $id_user=$_SESSION['id_user'];
    mysql_query('begin');
    $sql="INSERT INTO
           `prestadores_servicos`
            (`ID`, 
            `LOGIN`, 
            `FORNECEDOR_ID`, 
            `CRIADO_POR`,
            `DATA_CRIADO`) 
            VALUES 
            (NULL,
             '{$login_prestador}',
             '{$id_fornecedor}',
              '{$id_user}',
             now())";
              
             $result=  mysql_query($sql);
             if(!$result){
                    echo mysql_error();
                    mysql_query("roolback");
                    return;
        }
        $id_prestador= mysql_insert_id(); 
        savelog(mysql_escape_string(addslashes($sql)));
       
       
      $empresa = explode("#",$empresas);
       foreach($empresa as $emp){
           if($emp != NULL && $emp!=''){ 
                        $id_empresa= anti_injection($emp, "numerico");

                             $sql="INSERT INTO 
                                 `prestador_servico_empresa`
                                 (`PRESTADOR_ID`, 
                                 `EMPRESA_ID`, 
                                 `CRIADO_POR`, 
                                 `CRIADA_EM`
                                 ) VALUES (
                                     {$id_prestador},
                                     {$emp},
                                     {$id_user},
                                     now()    
                                     )";
                            $result=  mysql_query($sql);
                          if(!$result){
                                 echo mysql_error();
                                 mysql_query("roolback");
                                 return;
                     }
                     savelog(mysql_escape_string(addslashes($sql)));
           }
       }
       
       $servico = explode("$", $linha_servico);
    
       foreach ($servico as $ser){
                if($ser != NULL && $ser!=''){ 
                 
                 $s = explode("#",str_replace("undefined","",$ser));
                 $id_servico = anti_injection($s[0], 'numerico');
                 $valor      = anti_injection($s[1], 'numerico');
                 $id_empresa = anti_injection($s[2], 'numerico');
                 $sql="INSERT INTO
                     `servico_terceirizado_prestador`
                     (`ID`, 
                     `SERVICO_TERCEIRIZADO_ID`, 
                     `PRESTADOR_ID`, 
                     EMPRESA_ID,
                     `VALOR`, 
                     `CRIADO_EM`, 
                     `CRIADO_POR`)
                     VALUES 
                     (NULL,
                     '{$id_servico}',
                     '{$id_prestador}',
                     '{$id_empresa}',
                     '{$valor}',
                     '{$id_user}',
                       now()
                       )";
                      $result=  mysql_query($sql);
                                if(!$result){
                                       echo mysql_error();
                                       mysql_query("roolback");
                                       return;
                           }
                           savelog(mysql_escape_string(addslashes($sql)));

             }
       }
    mysql_query('commit');
    echo 1;
    
}

function editar_cadastro_servico_prestador($post){
    extract($post,EXTR_OVERWRITE);
    
    $id_prestador  = anti_injection($id_prestador,"numerico");
    $id_user=$_SESSION['id_user'];
    mysql_query('begin');
    
    $desativar_empresa = explode("#",$desativar_empresa);
       foreach($desativar_empresa as $des_emp){
           if($des_emp != NULL && $des_emp!=''){ 
                        $id_empresa= anti_injection($des_emp, "numerico");

                             $sql="UPDATE 
                                 `prestador_servico_empresa`
                                 SET 
                                 `DESATIVADA_POR`={$id_user},
                                 `DESATIVADA_EM`=now()
                                  WHERE 
                                  `PRESTADOR_ID`={$id_prestador} and
                                  `EMPRESA_ID`= $id_empresa and 
                                  `DESATIVADA_POR`=0";
                            $result=  mysql_query($sql);
                          if(!$result){
                                 echo mysql_error();
                                 mysql_query("roolback");
                                 return;
                     }
                     savelog(mysql_escape_string(addslashes($sql)));
           }
       }
       
      $empresa = explode("#",$empresas);
       foreach($empresa as $emp){
           if($emp != NULL && $emp!=''){ 
                        $id_empresa= anti_injection($emp, "numerico");

                             $sql="INSERT INTO 
                                 `prestador_servico_empresa`
                                 (`PRESTADOR_ID`, 
                                 `EMPRESA_ID`, 
                                 `CRIADO_POR`, 
                                 `CRIADA_EM`
                                 ) VALUES (
                                     {$id_prestador},
                                     {$emp},
                                     {$id_user},
                                     now()    
                                     )";
                            $result=  mysql_query($sql);
                          if(!$result){
                                 echo mysql_error();
                                 mysql_query("roolback");
                                 return;
                     }
                     savelog(mysql_escape_string(addslashes($sql)));
           }
       }
       
       $linha_desativar_servico= explode("#", $linha_desativar_servico);
       foreach( $linha_desativar_servico as $des_servico){
           if($des_servico != NULL && $des_servico!=''){ 
                        $id_servico= anti_injection($des_servico, "numerico");

                             $sql="UPDATE 
                                 `servico_terceirizado_prestador`
                                 SET 
                                 `DESATIVADO_POR`={$id_user},
                                 `DESATIVADO_EM`=now()
                                  WHERE 
                                  `PRESTADOR_ID`={$id_prestador} and 
                                  `SERVICO_TERCEIRIZADO_ID`= {$id_servico} and
                                  `DESATIVADO_POR`=0";
                            $result=  mysql_query($sql);
                          if(!$result){
                                 echo mysql_error();
                                 mysql_query("roolback");
                                 return;
                     }
                     savelog(mysql_escape_string(addslashes($sql)));
           }
       }
       
        $linha_desativar_servico_empresa= explode("$", $linha_desativar_servico_empresa);
       foreach( $linha_desativar_servico_empresa as $des_servico_emp){
           if($des_servico_emp != NULL && $des_servico_emp!=''){ 
                        $s = explode("#",str_replace("undefined","",$des_servico_emp));
                        $id_servico = anti_injection($s[0], 'numerico');
                        $id_empresa = anti_injection($s[1], 'numerico');

                             $sql="UPDATE 
                                 `servico_terceirizado_prestador`
                                 SET 
                                 `DESATIVADO_POR`={$id_user},
                                 `DESATIVADO_EM`=now()
                                  WHERE 
                                  `PRESTADOR_ID`={$id_prestador} and 
                                  `SERVICO_TERCEIRIZADO_ID`= {$id_servico} and
                                   EMPRESA_ID={$id_empresa} and
                                  `DESATIVADO_POR`=0";
                            $result=  mysql_query($sql);
                          if(!$result){
                                 echo mysql_error();
                                 mysql_query("roolback");
                                 return;
                     }
                     savelog(mysql_escape_string(addslashes($sql)));
           }
       }
       
       
       $servico = explode("$", $linha_servico);
    
       foreach ($servico as $ser){
                if($ser != NULL && $ser!=''){ 
                 
                 $s = explode("#",str_replace("undefined","",$ser));
                 $id_servico = anti_injection($s[0], 'numerico');
                 $valor      = anti_injection($s[1], 'numerico');
                 $id_empresa = anti_injection($s[2], 'numerico');
                 $sql="INSERT INTO
                     `servico_terceirizado_prestador`
                     (`ID`, 
                     `SERVICO_TERCEIRIZADO_ID`, 
                     `PRESTADOR_ID`, 
                     EMPRESA_ID,
                     `VALOR`, 
                     `CRIADO_EM`, 
                     `CRIADO_POR`)
                     VALUES 
                     (NULL,
                     '{$id_servico}',
                     '{$id_prestador}',
                     '{$id_empresa}',
                     '{$valor}',
                     '{$id_user}',
                       now()
                       )";
                      $result=  mysql_query($sql);
                                if(!$result){
                                       echo mysql_error();
                                       mysql_query("roolback");
                                       return;
                           }
                           savelog(mysql_escape_string(addslashes($sql)));

             }
       }
    mysql_query('commit');
    echo 1;
    
}

function cancelar_transferencia($transferencias, $justificativa){
    mysql_query('begin');
    //$id_transferencia =  anti_injection($id_transferencia, 'numerico');
    $justificativa =  anti_injection($justificativa,'literal');

    foreach($transferencias as $transferencia) {
        $sql = <<<SQL
UPDATE 
  transferencia_bancaria 
SET
  CANCELADA_POR = '{$_SESSION['id_user']}',
  CANCELADA_EM = NOW(),
  JUSTIFICATIVA = '{$justificativa}'
WHERE 
  ID = '{$transferencia}'
SQL;

        $result=  mysql_query($sql);
        if(!$result){
            echo mysql_error();
            mysql_query("roolback");
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));
    }

    mysql_query('commit');
    echo 1;
}

function salvarEdicaoVencimento($post){
    $idNota = 0;
    $dados = [];
    extract($post,EXTR_OVERWRITE);
    mysql_query("BEGIN");
    foreach($dados as $parcela){
        $sql = "UPDATE parcelas SET
                    vencimento = '{$parcela['novo_vencimento']}'
                WHERE
                    id = '{$parcela['parcela']}'
                    AND idNota = '{$idNota}'";
        $rs = mysql_query($sql);
    }
    if(!$rs){
        echo mysql_error();
        mysql_query("ROOLBACK");
        echo 0;
    }else{
        savelog(mysql_real_escape_string(addslashes($sql)));
        mysql_query("COMMIT");
        echo 1;
    }
}

function salvarEdicaoParcela($post){
    
    $idNota = 0;
    $dados = [];
    
    extract($post,EXTR_OVERWRITE);
    
   
    
    mysql_query("BEGIN");

    $sql = "Select 
                * 
                from
                parcelas
                    where  
                    idNota = {$idNota} and 
                status = 'Pendente' ";
                    $result = mysql_query($sql);
        while($row = mysql_fetch_array($result)){
            $empresa = $row['empresa'];
            
           
            $sql = "INSERT INTO historico_parcelas 
            (
                `id_parcela`,
                `idNota`,
                `valor`, 
                `vencimento`, 
                `codBarras`, 
                `status`,
                `empresa`,
               
                `TR`,
                vencimento_real,
                obs,
                deleted_by,
                deleted_at
                ) 
        VALUES
                (    
                    '{$row['id']}', 
                    '{$row['idNota']}', 
                    '{$row['valor']}', 
                    '{$row['vencimento']}',
                    '{$row['codBarras']}',
                    'Cancelada',
                    '{$row['empresa']}',
                    
                    '{$row['TR']}',
                    '{$row['vencimento_real']}',
                    'Cancelada por motivo de edição',
                    '{$_SESSION['id_user']}',
                    now()
                )";
  		$r = mysql_query($sql);
  		if(!$r){ 
			echo "Erro ao salvar histórico.";
			mysql_query("rollback");
			return;
        }
        if(!in_array($row['id'],$arrayParcelaId)){
            $sql = "update parcelas set status = 'Cancelada' where id = {$row['id']}";
            $r = mysql_query($sql);
            if(!$r){ 
                echo "Erro ao cancelar parcela.";
                mysql_query("rollback");
                return;
            }
                
        }

        }
   
    foreach($dados as $p){

       
  		$ntr=anti_injection($p['trp'],'literal');
  		$venc = implode("-",array_reverse(explode("/",$p['vencimento'])));
  		$codBarras = anti_injection($p['cod_barra'],'literal');
        $v = str_replace(",",".",str_replace(".","",$p['valor']));
        $venc_real = implode("-",array_reverse(explode("/",$p['vencimento_real'])));  		
          $venc = anti_injection($venc,'literal');
          $idParcela = anti_injection($p['idParcela'],'literal');
          if($idParcela > 0){
            $sql = "Update 
            parcelas
            set  
            `valor` = '{$v}', 
            `vencimento` = '{$venc}', 
            `codBarras` = '{$codBarras}',           
             `TR` = '{$ntr}',
             vencimento_real = '{$venc_real}' 
             where 
             id = $idParcela ";
            $r = mysql_query($sql);
            if(!$r){ 
              echo "Erro ao atualizar parcela";
              mysql_query("rollback");
              return;
          }

          }else{
            $sql = "INSERT INTO parcelas (`idNota`, `valor`, `vencimento`, `codBarras`, `status`, `empresa`,`TR`,vencimento_real ) 
            VALUES ( '{$idNota}', '{$v}', '{$venc}', '{$codBarras}', 'Pendente', '{$empresa}','{$ntr}','{$venc_real}')";
    $r = mysql_query($sql);
    if(!$r){ 
      echo "Erro ao inserir parcela";
      mysql_query("rollback");
      return;
  }

          }
  		
  		
  		
        
    }
    
        savelog(mysql_real_escape_string(addslashes($sql)));
        mysql_query("COMMIT");
        echo 1;
    
}

function salvarEdicaoRateio($post){
    $idNota = 0;
    $dados = [];
    extract($post,EXTR_OVERWRITE);
    $sql  = "DELETE FROM rateio WHERE NOTA_ID = {$idNota}";
    $rs = mysql_query($sql);
    if($rs){
        mysql_query("BEGIN");
        $sql = "INSERT INTO rateio (`NOTA_ID`, `NOTA_TR`, `CENTRO_RESULTADO`, `VALOR`, `PORCENTAGEM`,IPCG3,IPCG4,ASSINATURA,IPCF2 ) VALUES ";
        foreach($dados as $rateio){
            $sqlIPCF4 = "SELECT N2, N3 FROM plano_contas WHERE ID = '{$rateio['ipcg4']}'";
        $rsIPCF4 = mysql_query($sqlIPCF4);
        $rowIPCF4 = mysql_fetch_array($rsIPCF4, MYSQL_ASSOC);
  		
  		$ipcg3 = $rowIPCF4['N3'];
  		
        $ipcg2 = $rowIPCF4['N2'];
            $sql .= " ('{$idNota}', '1', '{$rateio['ur']}', '".str_replace(".","",$rateio['valor'])."', '".round($rateio['porcento'],2)."', '{$ipcg3}',	'{$rateio['ipcg4']}',	'{$rateio['assinatura']}', '{$ipcg2}'),";
        }
        $rs = mysql_query(substr_replace($sql,";",-1));
        if(!$rs){
            echo substr_replace($sql,";",-1);
            echo mysql_error();
            mysql_query("ROOLBACK");
            echo 0;
        }else{
            savelog(mysql_real_escape_string(addslashes($sql)));
            mysql_query("COMMIT");
            echo 1;
        }
    }

}

function pesquisarRelatorioAnaliticoFluxoCaixa($post){
    extract($post,EXTR_OVERWRITE);
    $status = anti_injection($status, 'literal');
    $inicio = anti_injection($inicio, 'literal');
    $fim = anti_injection($fim, 'literal');
    $empresa = anti_injection($empresa, 'literal');
    $idIPCFN4 = anti_injection($id_ipcf_n4 , 'literal');
    $inicio = implode('-',array_reverse(explode('/',$inicio)));
    $fim = implode('-',array_reverse(explode('/',$fim)));
    $condEmpresa = $empresa == 'T' ? '' : " AND rateio.CENTRO_RESULTADO = {$empresa}";
    $condIPCFN4 = $idIPCFN4 == 'T' ? '' : " AND rateio.IPCG4={$idIPCFN4}";
    $condTarifas = $idIPCFN4 == 'T' ? '' : " AND pc.ID = {$idIPCFN4}";

    if($status == "Processada") {
        $sql = "SELECT
            empresas.nome as empresa,
            plano_contas.N4,
            plano_contas.COD_N4,
            DATE_FORMAT(parcelas.DATA_CONCILIADO,'%d/%m/%Y') as dat_conciliado,
            parcelas.VALOR_PAGO,
            rateio.PORCENTAGEM,
            ROUND((parcelas.VALOR_PAGO *(rateio.PORCENTAGEM/100)),2) as val,
            (CASE notas.tipo WHEN 0 THEN 'RECEB.' ELSE 'PG.' END) as tipo_nota,
            notas.codigo,
            fornecedores.razaoSocial,
            notas.descricao,
            indexed_demonstrativo_contas_bancarias.conta
        FROM
            rateio
            INNER JOIN parcelas ON parcelas.idNota = rateio.NOTA_ID
            INNER JOIN notas ON parcelas.idNota = notas.idNotas
            INNER JOIN empresas ON empresas.id = rateio.CENTRO_RESULTADO
            INNER JOIN plano_contas ON rateio.IPCG4 = plano_contas.ID
            INNER JOIN fornecedores ON notas.codFornecedor = fornecedores.idFornecedores
            INNER JOIN indexed_demonstrativo_contas_bancarias ON indexed_demonstrativo_contas_bancarias.ID = parcelas.origem
        WHERE
            parcelas.`status` <> 'Cancelada' AND
            parcelas.CONCILIADO = 'S' AND
            notas.`status` <> 'Cancelada' AND
            parcelas.DATA_CONCILIADO BETWEEN '{$inicio}' AND '{$fim}'
            $condEmpresa
            $condIPCFN4
        UNION ALL
            SELECT
                emp.nome AS empresa,
                pc.N4,
                imptar.ipcf AS COD_N4,
                DATE_FORMAT(itl. DATA, '%d/%m/%Y') AS dat_conciliado,
                0 AS VALOR_PAGO,
                0 AS PORCENTAGEM,
                itl.valor AS val,
                'PG.' AS tipo_nota,
                itl.documento,
                imptar.item AS razaoSocial,
                '' AS descricao,
                indexed_demonstrativo_contas_bancarias.conta
            FROM
		      impostos_tarifas imptar
                INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
                INNER JOIN contas_bancarias cb ON (cb.id = itl.conta)
                INNER JOIN plano_contas pc ON (imptar.ipcf = pc.COD_N4)
                INNER JOIN empresas emp ON (emp.id = cb.UR)
                INNER JOIN indexed_demonstrativo_contas_bancarias ON indexed_demonstrativo_contas_bancarias.ID = itl.conta
              WHERE
                itl.data BETWEEN '{$inicio}' AND '{$fim}'
                {$condTarifas}
                
            ORDER BY
		COD_N4,
		dat_conciliado ASC";
    } else {

        /*UNION ALL
                  SELECT
                    'MEDERI CENTRAL' AS empresa,
                    IF(modalidade_fatura != 'REMOCAO', 'ASSIST. DOMICILIAR', 'REMOÇOES E EVENTOS') AS N4,
                    IF(modalidade_fatura != 'REMOCAO', '1.1.1.01', '1.1.2.01') AS COD_N4,
                    DATE_FORMAT(previsao_recebimento, '%d/%m/%Y') AS dat_conciliado,
                    0 AS VALOR_PAGO,
                    0 AS PORCENTAGEM,
                    valor_liquido AS val,
                    'PREVISAO DE RECEBIMENTO DE FATURA' AS tipo_nota,
                    fatura.protocolo_envio AS documento,
                    planosdesaude.nome AS razaoSocial,
                    '' AS descricao
                FROM
                    fatura
                    INNER JOIN planosdesaude ON fatura.PLANO_ID = planosdesaude.id
                    INNER JOIN fatura_controle_recebimento on fatura_controle_recebimento.fatura_id = fatura.ID
                WHERE
                    fatura.CANCELADO_POR IS NULL
        AND fatura.ORCAMENTO = 0
        AND fatura_controle_recebimento.previsao_recebimento BETWEEN '{$inicio}' AND '{$fim}'
        AND fatura_controle_recebimento.status = 'PENDENTE'*/

        $sql = "SELECT
            empresas.nome as empresa,
            plano_contas.N4,
            plano_contas.COD_N4,
            DATE_FORMAT(parcelas.vencimento,'%d/%m/%Y') as dat_conciliado,
            parcelas.valor,
            rateio.PORCENTAGEM,
            ROUND((parcelas.valor * (rateio.PORCENTAGEM/100)),2) as val,
            (CASE notas.tipo WHEN 0 THEN 'RECEB.' ELSE 'PG.' END) as tipo_nota,
            notas.codigo,
            fornecedores.razaoSocial,
            notas.descricao
        FROM
            rateio
            INNER JOIN parcelas ON parcelas.idNota = rateio.NOTA_ID
            INNER JOIN notas ON parcelas.idNota = notas.idNotas
            INNER JOIN empresas ON empresas.id = rateio.CENTRO_RESULTADO
            INNER JOIN plano_contas ON rateio.IPCG4 = plano_contas.ID
            INNER JOIN fornecedores ON notas.codFornecedor = fornecedores.idFornecedores
        WHERE
            parcelas.`status` = 'Pendente'
            AND parcelas.vencimento BETWEEN '{$inicio}' AND '{$fim}'
            {$condEmpresa}
            {$condIPCFN4}            
            ORDER BY
		COD_N4,
		dat_conciliado ASC";
    }
    //echo '<pre>'; die(print_r($sql));
    $result = mysql_query($sql);

    $numeroLinhas=mysql_num_rows($result);
    if($numeroLinhas>0){
        $var.= "<br></br>
                     <table class='mytable font8' width='100%'>
                          
                          <thead>
                             <tr>
                               <th>IPCF</th>                            
                               <th>DATA</th>
                               <th>C. RESULT.</th>
                               <th>HISTÓRICO</th>
                               <th>CONTA</th>
                               <th>VALOR </th>
                             </tr>
                             </thead>
                           
                             
                          ";
        $cont=1;
       
        while($row = mysql_fetch_array($result)){

            $cor= $cont%2 ==0? "bgcolor='#DCDCDC'":'';

            $var.= "<tr $cor >
                           <td>{$row['COD_N4']} - {$row['N4']}</td>
                           <td>{$row['dat_conciliado']}</td>
                           <td>{$row['empresa']}</td>
                           <td>{$row['tipo_nota']} DOC.{$row['codigo']} - {$row['razaoSocial']} -{$row['descricao']}</td>
                           <td>{$row['conta']}</td>
                           <td> R$ ".number_format($row['val'],2,',','.')."</td>
                          </tr>";

            $cont++;

        }

        $var.= "</table>";



    }else{
        $var.= "<br/><br/><p style='text-align:center;'><b>Nenhum resultado foi encontrado</b></p>";
    }

    echo $var;
}

//// fim 
function editarNumeroNota($post){
    extract($post,EXTR_OVERWRITE);
    $numeroNota = anti_injection($numeroNota, 'literal');
    $idNota     = anti_injection($idNota , 'numerico');
    $codFornecedor  = anti_injection($codFornecedor , 'numerico');
    $notaJaCadastrada = Notas::verificarNFJaExiste($codFornecedor, $numeroNota);

    if(!empty($notaJaCadastrada) && $notaJaCadastrada['idNotas'] != $idNota){
        echo "Conta já possui cadastro no sistema TR {$notaJaCadastrada['idNotas']}.";
        return;
    }
   
    if(empty($numeroNota)){
       return "Erro (1): entre em contato com o TI";
    }
    $sql =" Update notas set codigo = '{$numeroNota}' where idNotas ={$idNota}";
    
    $result=  mysql_query($sql);
    if (!$result){
       return "Erro ao fazer Update no numero da nota";
         
    }
   // savelog(mysql_escape_string(addslashes($sql)));

    echo 1;

}

function editarCompetencia($post){
    extract($post,EXTR_OVERWRITE);
    $competencia = anti_injection($competencia, 'literal');
    $idNota     = anti_injection($idNota , 'numerico');

    
    if(empty($competencia)){
       return "Erro (1): entre em contato com o TI";
    }
    $competencia = implode("-", array_reverse(explode("/", $competencia)));
    $sql =" Update notas set data_competencia = '{$competencia}' where idNotas ={$idNota}";
    
    $result=  mysql_query($sql);
    if (!$result){
       return "Erro ao fazer Update na data de competência";
         
    }
   // savelog(mysql_escape_string(addslashes($sql)));

    echo 1;

}
function pasquisarFaturasCusto($post){
       extract($post,EXTR_OVERWRITE);
       if(empty($id_paciente) || $id_paciente == -1 ){
        echo "<p>"
                 . "<b>ERRO: Selecione o paciente.</b>"
          . "</p>";
        return;
    }
    if(empty($inicio)){
        echo "<p>"
             . "<b>ERRO: Indique a data início.</b>"
          . "</p>";
        return;
    }
    if(empty($fim)){
        echo "<p>"
               . "<b>ERRO: Indique a data fim.</b>"
           . "</p>";
        return;
    }       
   
    $inicio = anti_injection(implode("-",array_reverse(explode("/",$inicio))),'literal');
    $fim = anti_injection(implode("-",array_reverse(explode("/",$fim))),'literal');    
    if($inicio > $fim ){
        echo "<p>"
                 . "<b>ERRO: A data de início deve ser menor ou igual a data final</b>"
            . "</p>";
        return;
    }
     $idPaciente = anti_injection($id_paciente,'numerico');
     
     $sql="SELECT
            usuarios.nome as usuario,
            clientes.nome as paciente ,
            DATE_FORMAT(fatura.`DATA`,'%d/%m/%Y %H:%i:%s') as data_faturado,
            DATE_FORMAT(fatura.DATA_INICIO,'%d/%m/%Y' ) as ini,
            DATE_FORMAT(fatura.DATA_FIM,'%d/%m/%Y' ) as f,
            fatura.ID,
            (case fatura.STATUS
             when 1 then 'Enviado'
             when 2 then 'Pendente'
             when 3 then 'Finalizado'
             when 5 then 'Faturado'
             when 6 then 'Precificar'
             end) as st,
             (case fatura.ORCAMENTO
             when 0 then 'Fatura'
             when 1 then 'Or. Inical'
             when 2 then 'Or. Prorrogação'
             when 3 then 'Or. Aditivo'
             end) as tip,
             DATE_FORMAT(fatura.data_margem_contribuicao,'%m/%Y' ) as dataMargem,
             fatura.remocao
           
            FROM
            fatura
            INNER JOIN clientes ON fatura.PACIENTE_ID = clientes.idClientes
            INNER JOIN usuarios ON fatura.USUARIO_ID = usuarios.idUsuarios
            WHERE
            fatura.CANCELADO_POR IS NULL AND
            fatura.PACIENTE_ID ={$id_paciente} AND
            (fatura.DATA_INICIO BETWEEN '{$inicio}' AND  '{$fim}' OR
            fatura.DATA_FIM BETWEEN '{$inicio}' AND '{$fim}')
            ORDER BY fatura.`DATA` asc";
            $result = mysql_query($sql);
   $num_row= mysql_num_rows($result);

   if(!$result){
       echo "<p> "
             . "<b>ERRO: Problema na pesquisa que lista as faturas. Entre em contato com o TI</b>"
          . "</p>";
       return;
   } 
   
  
   
    if($num_row == 0 ){
        echo "<p>"
              . "<b>Nenhuma fatura foi encontrada.</b>"
          . "</p>";
        return;
    }
    $contadorRel=0;
     echo "<table class='mytable' width='100%' >";
   while( $row = mysql_fetch_array($result)){ 
       
         $carater= $row['carater'] == 8 ? 'enf':'med';
            if($contador ==0){
                echo "<thead>
                       <tr>
                          <th>ID Fatura</th>
                          <th>Paciente</th>
                          <th>Profissional</th>
                          <th>Tipo</th>
                          <th>Status</th>
                          <th>Data</th>
                          <th>".htmlentities('Período')."</th>
                          <th>".htmlentities('Ação')."</th>                          
                       </tr>
                     </thead>"; 
                }  
            echo "<tr>
                    <td>#{$row['ID']}</td>
                    <td>{$row['paciente']}</td>
                    <td>{$row['usuario']}</td>
                    <td>{$row['tip']} ".($row['remocao'] == 'S' ? 'Remoção ' : '')." </td>
                    <td>{$row['st']}</td>
                    <td>{$row['data_faturado']}</td>
                    <td>{$row['ini']} ".htmlentities('até')." {$row['f']}</td>
                    <td> ";

                      echo " <a href='?op=cadastrarCustoFatura&fatura={$row['ID']}'>Cadastrar custo</a>
                        &nbsp; &nbsp; &nbsp;";



                     echo " <a href='#'>
                                  <img src='../utils/excel_24x24.png' width='16' 
                                    width='20' title='Exportar para Excel.' class='v_exportar'
                                    idfatura ='{$row['ID']}' av='{$row{'ORCAMENTO'}}' >
                        </a>";
                    if($row['dataMargem'] != NULL) {
                        echo " &nbsp; &nbsp; &nbsp
                        <label >
                                    <img src='../utils/details.png' width='16' height='16' title='Ja está no Relatório de Margem de Contribuiçao em {$row['dataMargem']}'>
                             </label>";
                    }
                   echo " </td>
                    
                  </tr>";
                    $contador++;
    
   }   
   echo "</table>"; 
     
 }
 function salvarCustoFatura($post)
 {

     extract($post, EXTR_OVERWRITE);
     $dados = explode('$', $linha);
     $dadosCustoExtra = explode('$', $linhaCustoExtra);
     $desconto_acrescimo = 0;
     $valorTotal = 0;
     $custoTotal = 0;
     $tipoAcao = anti_injection($tipo, 'literal');
     $idFatura = anti_injection($idFaturaFinalizarCusto, 'literal');
     $iss = anti_injection($iss, 'numerico');
     $pissCofins = 4.95;
     $ir = 3.5;
     mysql_query('begin');

     $sqlISS = "Update
     fatura
    set
     imposto_iss = {$iss},
     imposto_iss_inserted_at = now(),
     imposto_iss_inserted_by = {$_SESSION['id_user']}
     where
     ID = $idFatura";
     $resultSqlISS = mysql_query($sqlISS);
     if (!$resultSqlISS) {
         echo 1;
         mysql_query("ROOLBACK");
         return;
     }
     savelog(mysql_escape_string(addslashes($sqlISS)));

     foreach ($dados as $d) {
         $i = explode('#', $d);
         $idFaturamento = anti_injection($i[0], 'numerico');
         $codigoItem = anti_injection($i[1], 'numerico');
         $tipoItem = anti_injection($i[2], 'numerico');
         $acrescimoDescontoItem = anti_injection($i[3], 'numerico');
         $custoItem = anti_injection($i[4], 'literal');
         $custoItem = str_replace('.', '', $custoItem);
         $custoItem = str_replace(',', '.', $custoItem);
         $paciente = anti_injection($i[5], 'numerico');
         $custoItenFaturaId = anti_injection($i[6], 'numerico');
         $quantidade = anti_injection($i[7], 'numerico');
         $valorFatura = anti_injection($i[8], 'literal');
         $tabelaOrigem = anti_injection($i[9], 'literal');


         $desconto_acrescimo = ($quantidade * $valorFatura) * ($acrescimoDescontoItem / 100);
         $valorTotal += ($quantidade * $valorFatura) + $desconto_acrescimo;
         $custoTotal += $quantidade * $custoItem;

         $sqlUpdateFaturamento = "
            Update 
            faturamento 
            set       
            VALOR_CUSTO ='{$custoItem}',
            `custo_atualizado_em` = now() ,
            `custo_atualizado_por` = '{$_SESSION['id_user']}' 
            where
            ID={$idFaturamento}      
         
         ";
         $resultUpdateFaturamento = mysql_query($sqlUpdateFaturamento);
         if (!$resultUpdateFaturamento) {
             echo 2;
             mysql_query("ROOLBACK");
             return;
         }
         savelog(mysql_escape_string(addslashes($sqlUpdateFaturamento)));

         if ($tipoItem == 1 || $tipoItem == 0 || $tipoItem == 3) {
             $condicao = "catalogo_id";
         } else {
             $condicao = $tabelaOrigem . "_id";
         }
         $sqlExisteItemCusto = "Select
id
 from
custo_item_fatura
  WHERE
paciente_id = '{$paciente}' and
tipo =  '{$tipoItem}' and
{$condicao} = '{$codigoItem}' ";

         $resultExisteItemCusto = mysql_query($sqlExisteItemCusto);
         $existe = mysql_num_rows($resultExisteItemCusto);

         if ($existe == 0) {
             $sqlInsertCustoItem = "INSERT INTO
                      custo_item_fatura
                      (id,
                       {$condicao},
                       paciente_id,
                       tipo,
                       usuario_id,
                       data,
                       custo)
                       VALUES 
                       (null,
                        '{$codigoItem}',
                        '{$paciente}',
                        '{$tipoItem}',
                        '{$_SESSION['id_user']}',
                        now(),
                        '{$custoItem}'
                       )                       
                     ";
             $resultInsertCustoItem = mysql_query($sqlInsertCustoItem);
             if (!$resultInsertCustoItem) {
                 echo 3;
                 mysql_query("ROOLBACK");
                 return;
             }
             savelog(mysql_escape_string(addslashes($sqlInsertCustoItem)));
         } else {

             $sqlUpdateCustoItem =
                 "Update
                      custo_item_fatura
                      set
                      custo = '{$custoItem}',
                      data = now()
                      where
                      id ={$custoItenFaturaId}";
             $resultUpdateCustoItem = mysql_query($sqlUpdateCustoItem);
             if (!$resultUpdateCustoItem) {
                 echo 3;
                 mysql_query("ROOLBACK");
                 return;
             }
             savelog(mysql_escape_string(addslashes($sqlUpdateCustoItem)));
         }
     }
     $dataMargem = anti_injection(implode("-", array_reverse(explode("/", $dataMargem))), 'literal');

     $totalCustoExtras = 0;


     if (!empty($dadosCustoExtra[0])) {

     foreach ($dadosCustoExtra as $d) {
         $i = explode('#', $d);

         $idCustoExtra = $i[0];
         $idFatura = $idFaturaFinalizarCusto;
         $quantidade = $i[1];
         $custoExtras = str_replace('.', '', $i[2]);
         $custoExtras = str_replace(',', '.', $custoExtras);
         $idFaturaCustoExtra = $i[3];
         $quantidadeAnterior = $i[4];
         $custoAnterior = str_replace('.', '', $i[5]);
         $custoAnterior = str_replace(',', '.', $custoExtras);
         $cancelar = $i[6];

         if($cancelar == 'N'){
             $totalCustoExtras += $custoExtras * $quantidade ;
             if( $idFaturaCustoExtra == 0 ||
                 ($idFaturaCustoExtra != 0 &&
                     ($custoExtras != $custoAnterior || $quantidade != $quantidadeAnterior)
                 )
             ){

                 $sql = "INSERT INTO
 `fatura_custos_extras`
 ( id_custo_extra,
 id_fatura,
 quantidade,
 custo,
 created_at,
 created_by
 )
 values(
  $idCustoExtra,
 $idFatura,
 $quantidade,
 '$custoExtras',
 now(),
 '{$_SESSION['id_user']}'

 )";
                 $resultCustoExtras = mysql_query($sql);
                 if (!$resultCustoExtras) {
                     echo $sql;
                     mysql_query("ROOLBACK");
                     return;
                 }
                 savelog(mysql_escape_string(addslashes($sql)));

             }

             if($idFaturaCustoExtra != 0 &&
                 ($custoExtras != $custoAnterior || $quantidade != $quantidadeAnterior)
             ){
                 $sql = "update
 `fatura_custos_extras`
 set
 canceled_by ='{$_SESSION['id_user']}' ,
 canceled_at = now()
 WHERE
 id =$idFaturaCustoExtra

 ";
                 $resultCustoExtras = mysql_query($sql);
                 if (!$resultCustoExtras) {
                     echo 5;
                     mysql_query("ROOLBACK");
                     return;
                 }
                 savelog(mysql_escape_string(addslashes($sql)));
             }

         }else{

             $sql = "update
 `fatura_custos_extras`
 set
 canceled_by ='{$_SESSION['id_user']}' ,
 canceled_at = now()
 WHERE
 id =$idFaturaCustoExtra

 ";
             $resultCustoExtras = mysql_query($sql);
             if (!$resultCustoExtras) {
                 echo 5;
                 mysql_query("ROOLBACK");
                 return;
             }
             savelog(mysql_escape_string(addslashes($sql)));

         }





     }
 }
     if($tipoAcao == 'finalizar'){
        $sql = "update
                  fatura set
                  data_margem_contribuicao= '$dataMargem',
                  enviado_margem_contribuicao_por = '{$_SESSION['id_user']}',
                  enviado_margem_contribuicao_em = now(),
                  STATUS = 3
                  WHERE
                  ID ='$idFatura' ";
         $resultUpdateFatura= mysql_query($sql);
         if(!$resultUpdateFatura){
             echo 4;
             mysql_query("ROOLBACK");
             return;
         }
         savelog(mysql_escape_string(addslashes($sql)));
     }



     mysql_query('commit');
     $custoTotal = ($valorTotal * ($pissCofins + $ir + $iss)/100) + $custoTotal + $totalCustoExtras;
     $lucroReais = $valorTotal - $custoTotal;
     $lucroPorcento = ($valorTotal - $custoTotal)/$valorTotal;
     $lucroPorcento *= 100;

     if($valorTotal >= 4000) {
         $alerta = "Orçamento aprovado com ressalva(s)";
         $corAlerta = "orange";
         if ($lucroPorcento < 25 && $lucroReais < 1000) {
             $alerta = "Orçamento reprovado";
             $corAlerta = "red";
         } elseif ($lucroPorcento >= 25 && $lucroReais >= 1000) {
             $alerta = "Orçamento aprovado";
             $corAlerta = "green";
         }
     } elseif($valorTotal > 1000 && $valorTotal < 4000) {
         if ($lucroPorcento < 25) {
             $alerta = "Orçamento reprovado";
             $corAlerta = "red";
         } elseif ($lucroPorcento >= 25) {
             $alerta = "Orçamento aprovado";
             $corAlerta = "green";
         }
     } elseif($valorTotal <= 1000) {
         if ($lucroPorcento >= 15) {
             $alerta = "Orçamento aprovado";
             $corAlerta = "green";
         } elseif ($lucroPorcento < 15) {
             $alerta = "Orçamento reprovado";
             $corAlerta = "red";
         }
     }

     echo "<p> "
             ."<b>OBS:</b> O valor total da fatura leva em consideração os descontos ou acréscimos" 
             . "<br><br>" 
             . "<b>Valor Total Faturado:</b> "
             . "R$ ".number_format($valorTotal,2,',','.').""
             . "<br>"
             . "<b>Custo Total + Encargos:</b> "
             . "R$ ".number_format($custoTotal,2,',','.').""
             . "<br>"             
             . "<b>Margem:</b> "
             . "R$ ".number_format($lucroReais,2,',','.').""
             . "<br>"
             . "<b>Margem Percentual &cong;</b> "
             .number_format($lucroPorcento,2,',','.')."%"
             . "<br>"
             . "<center style='background-color: {$corAlerta}'><b>{$alerta}</b></center> "
        ."</p>";
          
                 
                 
 }
function gerarRelatarioMargemContribuicao($post){
    extract($post,EXTR_OVERWRITE);

    // para cabeçalho:
    $nomePaciente = anti_injection($nomePaciente, 'literal');
    $nomePlano = anti_injection($nomePlano, 'literal');
    $nomeTipoDocumento = anti_injection($nomeTipoDocumento, 'literal');
    $inicioObj = \DateTime::createFromFormat('m/Y',$inicio);
    $inicioBr = $inicioObj->format('m/Y');
    $fimObj = \DateTime::createFromFormat('m/Y',$fim);
    $fimBr = $fimObj->format('m/Y');


    $fim= anti_injection(implode("-",array_reverse(explode("/",$fim))),'literal');

    $inicio = anti_injection(implode("-",array_reverse(explode("/",$inicio))),'literal');
    $tipoDocumento =anti_injection ($tipoDocumento, 'numerico');
    $plano =anti_injection ($plano, 'numerico');
    $paciente =anti_injection ($paciente, 'numerico');
	$empresa =anti_injection ($empresa, 'literal');

    $condEmpresa = $empresa == 'T' ? "" :"AND empresas.id = {$empresa} ";
    $condPaciente = $paciente == -1 ? "" :"AND fatura.PACIENTE_ID = {$paciente} ";
    $condPlano = $plano == -1 ? "" :"AND fatura.PLANO_ID = {$plano} ";
    $condTipoDocumento = $tipoDocumento == -1 ? "" :"AND fatura.ORCAMENTO = {$tipoDocumento} ";
     /* impostos a serem acresentados no valor dos custos.
        IR 2%
        PissCofins 4,73 %
        ISS buscar na tabela do plano.
        Pegar a soma dos impostos  aplicar no valor faturado ( somaImposto * (valor + desc_custo) ) e acrescentar no custo.
      */
    $pissCofins = 4.95;
    $ir = 3.5;
    $iss =0;

    $sql= "
select
SUM(faturamento.QUANTIDADE*faturamento.VALOR_FATURA) as valor,
SUM(faturamento.QUANTIDADE*faturamento.VALOR_CUSTO) as custo,
SUM((faturamento.QUANTIDADE*faturamento.VALOR_FATURA)*faturamento.DESCONTO_ACRESCIMO/100) as desc_custo,
tbl.*
FROM
faturamento inner join
(
SELECT
        clientes.nome as paciente,
        planosdesaude.nome as convenio,
        empresas.nome as ur,
        fatura.`DATA`,
        DATE_FORMAT(fatura.DATA_INICIO,'%d/%m/%Y') as inicioBr,
        DATE_FORMAT(fatura.DATA_FIM ,'%d/%m/%Y') as fimBr,
        fatura.data_margem_contribuicao,
        fatura.ID,
        fatura.imposto_iss,
        fatura.ORCAMENTO,
        fatura.remocao
        FROM
        fatura
        LEFT JOIN clientes ON fatura.PACIENTE_ID = clientes.idClientes
        LEFT JOIN planosdesaude ON fatura.PLANO_ID = planosdesaude.id
        LEFT JOIN empresas ON fatura.UR = empresas.id
        WHERE

        fatura.CANCELADO_POR is NULL AND
        DATE_FORMAT(fatura.data_margem_contribuicao, '%Y-%m') between '$inicio'
         and '$fim'
         {$condEmpresa}
         {$condPaciente}
         {$condPlano}
         {$condTipoDocumento}

         group by fatura.ID
 ) as tbl on  faturamento.FATURA_ID = tbl.ID
         WHERE
         faturamento.CANCELADO_POR is NULL
         group by tbl.ID";





    $nomeUr = 'Todos';
    if($empresa != 'T'){
        $result = mysql_query("SELECT nome FROM empresas where id = $empresa");
        $nomeUr = mysql_result($result,0);
    }

    $html = "<div>
                 <center><h1> Relatório de Margem de Contribuição</h1></center>
                 <p><b>Paciente:</b>{$nomePaciente}  <b>Empresa:</b> {$nomeUr}</p>
                 <p><b>Plano:</b> {$nomePlano} <b>Tipo documento:</b> $nomeTipoDocumento</p>
                 <p><b>Retirado por ".$_SESSION["nome_user"]." em  ".date('d/m/Y h:i')."</b></p>
                 <p><b>Inicio:</b> $inicioBr  <b>Fim:</b> $fimBr </p>
             </div>";
    $result = mysql_query($sql);

        if(mysql_num_rows($result) == 0){
            $html .= "<span> Nenhum lançamento foi encontrado.</span>";
            echo $html;
            return;
        }
    // custos extras
    $sqlExtras = "select
sum(fatura_custos_extras.custo * fatura_custos_extras.quantidade) as custoExtra,
tbl.ID
        from
        fatura_custos_extras
        inner join
        (
        SELECT
        fatura.ID
        FROM
        fatura
        WHERE
        fatura.CANCELADO_POR is NULL AND
        DATE_FORMAT(fatura.data_margem_contribuicao, '%Y-%m') between '$inicio'
         and '$fim'
         {$condEmpresa}
         {$condPaciente}
         {$condPlano}
         {$condTipoDocumento}
         group by fatura.ID
        ) as tbl on fatura_custos_extras.id_fatura = tbl.ID
        where
         canceled_by is null
         group by tbl.ID
        ";

    $resultExtras = mysql_query($sqlExtras);
    while($array = mysql_fetch_array($resultExtras)){

        $arrayCustosExtras[$array['ID']] = $array['custoExtra'] ;


    }



    $html .= "<table class='mytable'>";
    $html .= "<thead><tr>
                  <th>ID da Fatura</th>
                  <th>Mês Base</th>
                  <th>Paciente</th>
                  <th>Período</th>                  
                  <th>UR</th>
                  <th>Tipo</th>
                  <th>Convênio</th>
                  <th>Faturado</th>
                  <th>Custo</th>
                  <th>Margem R$</th>
                  <th>Margem % </th>
             </tr></thead>";
     $totalFaturado          = 0;
     $totalCusto             = 0;
     $totalMargem            = 0;
     $totalMargemPorcento    = 0;

     while( $row = mysql_fetch_array($result)){

         $custosExtras = empty($arrayCustosExtras[$row['ID']]) ? 0 : $arrayCustosExtras[$row['ID']];
            $faturado = $row['valor'] + $row['desc_custo'];
          $custo = ($row['valor'] * ($pissCofins + $ir + $row['imposto_iss'] )/100) + $row['custo'] + $custosExtras;
          $margem = $faturado - $custo;
          $margemPorcento = $faturado == 0 ? -1 : $margem/$faturado;
         $totalFaturado += $faturado;
         $totalCusto += $custo;
         $totalMargem += $margem;



         $sqlTipo = "SELECT
                        vc.idCobranca AS cod_item
                    FROM
                        faturamento AS f
                    LEFT JOIN valorescobranca AS vc ON (f.CATALOGO_ID = vc.id)
                    WHERE
                        f.FATURA_ID = {$row['ID']}
                    AND f.TIPO = 2
                    AND f.TABELA_ORIGEM = 'valorescobranca'
                    AND f.CANCELADO_POR IS NULL
                    AND vc.idCobranca IN (20, 21, 51, 60, 61, 77)
                    UNION
                        SELECT
                            vc.id AS cod_item
                        FROM
                            faturamento AS f
                        LEFT JOIN cobrancaplanos AS vc ON (f.CATALOGO_ID = vc.id)
                        WHERE
                            f.FATURA_ID = {$row['ID']}
                        AND f.TIPO = 2
                        AND f.TABELA_ORIGEM = 'cobrancaplano'
                        AND f.CANCELADO_POR IS NULL
                        AND f.CATALOGO_ID IN (20, 21, 51, 60, 61, 77)";
         $resultTipo = mysql_query($sqlTipo);

         $tipoInternacao = "ID";
         if(mysql_num_rows($resultTipo) == 0){
             $tipoInternacao = "AD";
         }
         $html .= "<tr>
                  <td>{$row['ID']}</td>
                  <td>" . \DateTime::createFromFormat('Y-m-d', $row['data_margem_contribuicao'])->format('m/Y') . "  </td>
                  <td>{$row['paciente']}</td>
                  <td>{$row['inicioBr']} - {$row['fimBr']}  </td>
                  <td>{$row['ur']}</td>
                  <td>".($row['remocao'] == 'S' ? htmlentities('Remoção ') : $tipoInternacao)."</td>
                  <td>{$row['convenio']}</td>
                  <td>R$ ".number_format($faturado,2,',','.')."</td>
                  <td>R$ ".number_format($custo,2,',','.')."</td>
                  <td>R$ ".number_format($margem,2,',','.')."</td>
                  <td>".number_format($margemPorcento *100,2,',','.')."%</td>

             </tr>";

     }
    $totalMargemPorcento +=$totalFaturado == 0 ? -1:  $totalMargem/$totalFaturado;
    $html .= "<tr style='background-color:#E6E6E6;'>
                  <td colspan='7' align='right'><b>TOTAIS:</b></td>
                  <td>R$ ".number_format($totalFaturado,2,',','.')."</td>
                  <td>R$ ".number_format($totalCusto,2,',','.')."</td>
                  <td>R$ ".number_format($totalMargem,2,',','.')."</td>
                  <td>".number_format($totalMargemPorcento *100,2,',','.')." %</td>

             </tr>
                </table>
                <button id='exportar-excel-relatorio-margem-contribuicao' class='ui-button
                    ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover'
                      role='button' aria-disabled='false'
                       data-inicio='$inicio' data-fim='$fim' data-nomepaciente='$nomePaciente'
                       data-nomeplano='$nomePlano' data-nometipodocumento ='$nomeTipoDocumento'
                        data-tipodocumento='$tipoDocumento' data-plano='$plano' data-paciente='$paciente'
                         data-empresa='$empresa'>
                      <span class='ui-button-text'>
                         Exportar Excel
                      </span>
            </button>";
    echo $html;
}

function verificaItemDuplicadoCustoFatura($post){
    extract($post,EXTR_OVERWRITE);
    $itensArray = [] ;
    $dados = explode('$', $linha);

    foreach ($dados as $d) {
        $i = explode('#', $d);

        $idFaturamento = anti_injection($i[0], 'numerico');
        $codigoItem = anti_injection($i[1], 'numerico');
        $tipoItem = anti_injection($i[2], 'numerico');
        $acrescimoDescontoItem = anti_injection($i[3], 'numerico');
        $custoItem = anti_injection($i[4], 'literal');
        $custoItem = str_replace('.','',$custoItem);
        $custoItem = str_replace(',','.',$custoItem);
        $paciente = anti_injection($i[5], 'numerico');
        $custoItenFaturaId= anti_injection($i[6], 'numerico');
        $quantidade = anti_injection($i[7], 'numerico');
        $valorFatura = anti_injection($i[8], 'literal');
        $tabelaOrigem = anti_injection($i[9], 'literal');
        $itensArray [$tabelaOrigem][] = $codigoItem;

    }
    foreach($itensArray as $itens){
        $qtdItens = count($itens);
        $uniqueItens = array_unique($itens);
        $qtdUniqueItens = count($uniqueItens);
        if($qtdItens != $qtdUniqueItens){
            echo "duplicado";
            return ;
        }
    }
    echo "ok";
    return ;


}

function buscarRetencoesFornecedor($fornecedor){
    $sql = <<<SQL
SELECT 
  recolhe_iss,
  aliquota_iss,
  recolhe_ir,
  aliquota_ir,
  recolhe_ipi,
  aliquota_ipi,
  recolhe_icms,
  aliquota_icms,
  recolhe_pis,
  aliquota_pis,
  recolhe_cofins,
  aliquota_cofins,
  recolhe_csll,
  aliquota_csll,
  recolhe_inss,
  aliquota_inss
FROM
  fornecedores
WHERE
  idFornecedores = '{$fornecedor}'
SQL;
    $rs = mysql_query($sql);
    $response = [];
    while($row = mysql_fetch_array($rs, MYSQL_ASSOC)) {
        $response = $row;
    }
    echo json_encode($response);
}

function buscarContaFinanceiraPorId($contaId){
    // $contaCodigo = vsprintf(
    //     "%s.%s.%s.%s%s",
    //     str_split($contaId)
    // );
    $contaCodigo = $contaId;

    $sql = <<<SQL
SELECT 
  *
FROM
  plano_contas
WHERE
  replace(COD_N4,'.','') = '{$contaCodigo}'
SQL;
    $rs = mysql_query($sql);
    $response = mysql_fetch_array($rs, MYSQL_ASSOC);
    if(count($response) > 0) {
        echo json_encode($response);
    } else {
        echo '0';
    }
}

function buscarFornecedorJsonPorTipo($tipo){
    $sql = <<<SQL
SELECT 
  *,
  COALESCE(razaoSocial, nome) as fornecedor
FROM
  fornecedores
WHERE
  TIPO = '{$tipo}'
  AND ATIVO = 'S'
ORDER BY nome
SQL;
    $rs = mysql_query($sql);
    $response = [];
    while($row = mysql_fetch_array($rs, MYSQL_ASSOC)){
        $response[] = $row;
    }
    if(count($response) > 0) {
        echo json_encode($response);
    } else {
        echo '0';
    }
}

function editarValoresParcela($data)
{
    $parcela = $data['id'];
    $juros = StringHelper::moneyStringToFloat($data['juros']);
    $desconto = StringHelper::moneyStringToFloat($data['desconto']);
    $tarifas_adicionais = StringHelper::moneyStringToFloat($data['tarifas_adicionais']);
    $vencimento_real = \DateTime::createFromFormat('d/m/Y', $data['vencimento_real'])->format('Y-m-d');
    $vencimento = \DateTime::createFromFormat('d/m/Y', $data['vencimento'])->format('Y-m-d');
    $tarifaCartao = StringHelper::moneyStringToFloat($data['tarifaCartao']);
    

    $sql = <<<SQL
UPDATE parcelas
SET JurosMulta = '{$juros}',
 desconto = '{$desconto}', 
 OUTROS_ACRESCIMOS = '{$tarifas_adicionais}',
 vencimento_real = '{$vencimento_real}',
 vencimento = '{$vencimento}',
 tarifa_cartao = '{$tarifaCartao}'
WHERE id = '{$parcela}'
SQL;
    echo mysql_query($sql);
}


if (isset($_POST['query'])) {
    switch ($_POST['query']) {
        case "edit-conta-banco":
            editar_conta_banco_parcela($_POST);
            break;
        case "balancete":
            balancete($_POST);
            break;
        case "inserir_nota":
            inserir_nota($_POST);
            break;
        case "usar-nota-para-nova":
            inserir_nota_usar_nova($_POST);
            break;
        case "salvar_nota":
            salvar_nota($_POST['id'], $_POST['codigo'], $_POST['natureza'], $_POST['centrocusto'], implode("-", array_reverse(explode("/", $_POST['entrada']))), $_POST['descricao'], $_POST['fornecedor'], $_POST['tipo'], $_POST['subnatureza']);
            break;
        case "cancelar_nota":
            cancelar_nota($_POST['id'], $_POST['motivo']);
            break;
        case "salvar_parcela":
            salvar_parcela($_POST['nota'], str_replace(",", ".", str_replace(".", "", $_POST['valor'])), implode("-", array_reverse(explode("/", $_POST['vencimento']))), $_POST['barras']);
            break;
        case "cancelar_parcela":
            cancelar_parcela($_POST['id'], $_POST['motivo']);
            break;
        case "editar_parcela":
            editar_parcela($_POST);
            break;
        case "processar_parcela":
            processar_parcela($_POST['id'], $_POST['origem'], $_POST['aplicacao'],  $_POST['vencimento'],
                str_replace(",", ".", str_replace(".", "", $_POST['valor'])), $_POST['obs'], str_replace(",", ".", str_replace(".", "", $_POST['juros'])),
                str_replace(",", ".", str_replace(".", "", $_POST['desconto'])), implode("-", array_reverse(explode("/", $_POST['data']))),
                str_replace(",", ".", str_replace(".", "", $_POST['outros'])), str_replace(",", ".", str_replace(".", "", $_POST['valor_pago'])),
                $_POST['num_documento'], $_POST['issqn'], $_POST['ir'], $_POST['pis'], $_POST['cofins'], $_POST['csll'], $_POST['tipo_nota'],
                $_SESSION['id_user'], str_replace(",", ".", str_replace(".", "", $_POST['encargos'])), $_POST['motivo_juros'], $_POST['vencimento_real'],
                str_replace(",", ".", str_replace(".", "", $_POST['tarifaCartao'])));
            break;
        case "salvar_comprovante":
            salvar_comprovante($_POST['id'], $_FILES['comprovante'], $_POST['nota']);
            break;
        case "salvar-conta":
            salvar_contas($_POST);
            break;
        case "editar-conta":
            editar_contas($_POST);
            break;
        case "show_comprovante":
            show_comprovante($_POST['id']);
            break;
        case "buscar":
            buscar($_POST);
            break;
        case "buscar_nota":
            buscar_nota($_POST);
            break;
        case "salvar-fornecedor":
            salvar_fornecedor($_POST);
            break;
        case "editar-fornecedor":
            editar_fornecedor($_POST);
            break;
        case "editar-nota":
            editar_nota($_POST);
            break;
        case "salvar-edicao-vencimento":
            salvarEdicaoVencimento($_POST);
            case "salvar-edicao-parcela":
            salvarEdicaoParcela($_POST);
            break;
        case "salvar-edicao-rateio":
            salvarEdicaoRateio($_POST);
            break;
        case "pesquisar-extrato":
            pesquisar_extrato($_POST);
            break;
        case "pesquisar-demonstrativo":
            pesquisar_demonstrativo($_POST);
            break;
        case "pesquisar-conciliacao":
            pesquisar_conciliacao($_POST);
            break;
        case "salvar-conciliacao":
            salvar_conciliacao($_POST);
            break;
        case "salvar-transferencia":
            salvar_transferencia($_POST);
            break;
        case "pesquisar-transferencia":
            pesquisar_transferencia($_POST);
            break;
        case "salvar-porcentagem-ipcg";
            salvar_porcentagem_ipcg($_POST);
            break;
        case "pesquisar-porcentagem-ipcg";
            pesquisar_porcentagem_ipcg($_POST);
            break;
        case "desfazer-conciliacao";
            desfazer_conciliacao($_POST);
            break;
        case "desfazer-processamento-parcela";
            desfazer_processamento_parcela($_POST);
            break;
        case "desfazer-processamento-nota";
            desfazer_processamento_nota($_POST);
            break;
        case "buscar-parcela-origem":
            buscar_parcela_origem($_POST["parcela_origem"]);
            break;
        case "salvar-cadastro-servico-prestador":
            salvar_cadastro_servico_prestador($_POST);
            break;
        case "editar-cadastro-servico-prestador":
            editar_cadastro_servico_prestador($_POST);
            break;
        case "cancelar-transferencia":
            cancelar_transferencia($_POST['transferencias'], $_POST['justificativa']);
            break;
        case "pesquisar-relatorio-analitico-fluxo-caixa":
            pesquisarRelatorioAnaliticoFluxoCaixa($_POST);
            break;
        case "salvar-tarifa":
            echo Tarifas::salvarTarifa($_POST);
            break;
        case "editar-tarifa":
            echo Tarifas::editarTarifa($_POST);
            break;
        case "excluir-tarifa":
            echo Tarifas::excluirTarifa($_POST['id']);
            break;
        case "buscar-tarifa":
            Tarifas::buscarTarifa($_POST);
            break;
        case "editar-numero-nota":
            editarNumeroNota($_POST);
            break;
        case "pesquisar-faturas-custo":
              pasquisarFaturasCusto($_POST);
           break;
        case "salvar-custo-fatura":
        salvarCustoFatura($_POST);
          break;
        case "pesquisar-rel-margem-contribuicao":
        gerarRelatarioMargemContribuicao($_POST);
          break;
        case "verifica-item-duplicado-custo-fatura":
            verificaItemDuplicadoCustoFatura($_POST);
            break;
        case "editar-valores-parcela":
            editarValoresParcela($_POST);
            break;
            case "editar-competencia":
                editarCompetencia($_POST);
                break;

    }
}

if (isset($_GET['query'])) {
    switch ($_GET['query']) {
        case "buscar-retencoes-fornecedor":
            buscarRetencoesFornecedor($_GET['fornecedor']);
            break;
        case "buscar-conta-financeira-id":
            buscarContaFinanceiraPorId($_GET['conta_id']);
            break;
        case "buscar-fornecedor-options":
            buscarFornecedorJsonPorTipo($_GET['tipo']);
            break;
    }
}
