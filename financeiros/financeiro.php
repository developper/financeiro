<?php
$id = session_id();
if(empty($id))
    session_start();

include_once('../db/config.php');
include_once('../utils/codigos.php');
require __DIR__ . '/../vendor/autoload.php';


use App\Models\Financeiro\Compensacao;
use App\Models\Financeiro\TipoContaBancaria;
use App\Models\Financeiro\Financeiro;
use App\Models\Financeiro\Parcela;
use App\Models\Financeiro\Retencoes;

function mask($val, $mask)
{
    $maskared = '';
    $k = 0;
    $val = preg_replace("/[^0-9]/", "", $val);
    for($i = 0; $i <= strlen($mask)-1; $i++) {
        if($mask[$i] == '#') {
            if(isset($val[$k]))
                $maskared .= $val[$k++];
        } else {
            if(isset($mask[$i])) {
                $maskared .= $mask[$i];
            }
        }
    }
    return $maskared;
}



function fornecedorNome($id)
{
    $var = "";
    $result = mysql_query("SELECT * FROM fornecedores WHERE idFornecedores = '$id'");
    while($row = mysql_fetch_array($result))
    {
        $var = $row['nome'];
        if($var == "")
            $var = substr(($row['razaoSocial']),0,15);
    }
    return $var;
}

function naturezaNome($id){
    $var = "";

    $result = mysql_query("SELECT * FROM naturezanotas WHERE idNaturezaNotas = '$id'");
    while($row = mysql_fetch_array($result))
    {
        $var = $row['nome'];
    }

    return $var;
}


function subnaturezaNome($id){
    $var = "";

    $result = mysql_query("SELECT * FROM subnatureza WHERE idSubNatureza = '$id'");
    while($row = mysql_fetch_array($result))
    {
        $var = $row['nome'];
    }

    return $var;
}

function centroCustoNome($id){
    $var = "";

    $result = mysql_query("SELECT * FROM centrocustos WHERE idCentroCustos = '$id'");
    while($row = mysql_fetch_array($result))
    {
        $var = $row['nome'];
    }

    return $var;
}

function centrocusto($id){
    $result = mysql_query("SELECT * FROM centrocustos ORDER BY nome;");
    $var = "<select id='centrocusto' name='codCentroCusto' style='background-color:transparent;'>";
    while($row = mysql_fetch_array($result))
    {
        if(($id <> NULL) && ($id == $row['idCentroCustos']))
            $var .= "<option value='". $row['idCentroCustos'] ."' selected>". $row['nome'] ."</option>";
        else
            $var .= "<option value='". $row['idCentroCustos'] ."'>". $row['nome'] ."</option>";
    }
    $var .= "</select>";
    return $var;
}

function fornecedores_busca(){
    $result = mysql_query("SELECT *, COALESCE(razaoSocial,nome) as fornecedor FROM fornecedores ORDER BY razaoSocial;");
    $var = "<select id='fornecedor' name='codFornecedor' class='chosen-select form-control' data-placeholder='Selecione um fornecedor...' >";
    $var .="<option value='' selected>Selecione</option>";
    while($row = mysql_fetch_array($result))
    {
        $inativo = $row['ATIVO'] == "N" ? '(INATIVO) ':'';
        $styleInativo = $row['ATIVO'] == "N" ? 'red': '';
        $cpfCnpjFornecedor = $row['FISICA_JURIDICA'] == 1 ? $row['cnpj'] : $row['CPF'];
        $var .= "<option class='fornecedor-option' style = 'color:{$styleInativo}' value='". $row['idFornecedores'] ."' tipo='{$row['TIPO']}' fisica_juridica='". $row['FISICA_JURIDICA'] ."' >".$inativo.$cpfCnpjFornecedor." - ". $row['fornecedor'] ."</option>";
    }
    $var .= "</select>";
    return $var;
}

function fornecedores($id, $id_select, $readonly, $disabled, $obrigatorio = null, $tipo = 'F'){
    $condInativo = $id != null ? " OR (idFornecedores = '{$id}' AND ATIVO = 'N') " : "";
    $result = mysql_query("SELECT *, COALESCE(razaoSocial,nome) as fornecedor FROM fornecedores WHERE tipo = '{$tipo}' AND ATIVO = 'S' {$condInativo} ORDER BY razaoSocial;");
    $var = "";
    if(!$readonly) $var .= "<select id='$id_select' name='codFornecedor' $disabled class='chosen-select buscar-retencoes-fornecedor $obrigatorio' data-placeholder='Selecione um fornecedor...' style='background-color:transparent; width: 600px;' >";
    else $var = "<select id='$id_select' name='' class='select_small $obrigatorio chosen-select ' $disabled style='background-color:transparent;' >";
    
        $var .="<option value='' selected>Selecione</option>";
    
    while($row = mysql_fetch_array($result))
    {
        $cpfCnpj = '';
        if(trim($row['cnpj']) != '' || trim($row['CPF'])) {
            $cpfCnpj = $row['FISICA_JURIDICA'] == 1 ?
                mask($row['cnpj'], '##.###.###/####-##') . ' - ' :
                mask($row['CPF'], '###.###.###-##') . ' - ';
        }
        if(($id <> NULL) && ($id == $row['idFornecedores']))
            $var .= "<option class='fornecedor-option' value='". $row['idFornecedores'] ."' tipo='{$row['TIPO']}' fisica_juridica='". $row['FISICA_JURIDICA'] ."' selected>{$cpfCnpj}{$row['fornecedor']}</option>";
        else
            $var .= "<option class='fornecedor-option' value='". $row['idFornecedores'] ."' tipo='{$row['TIPO']}' fisica_juridica='". $row['FISICA_JURIDICA'] ."' >{$cpfCnpj}{$row['fornecedor']}</option>";
    }
    $var .= "</select>";
    return $var;
}

function notasTipoDocumento($id, $id_select, $readonly, $disabled, $obrigatorio = null, $size = '400px'){
    $result = mysql_query("SELECT * FROM tipo_documento_financeiro where adiantamento != 'S' ORDER BY sigla;");
    $var = "";
    if(!$readonly) $var .= "<select id='$id_select' name='notas_tipo_documento' $disabled class='chosen-select $obrigatorio' data-placeholder='Selecione...' style='background-color:transparent; width: {$size};' >";
    else $var = "<select id='$id_select' name='' class='select_small $obrigatorio chosen-select ' $disabled style='background-color:transparent;' >";
    
        $var .="<option value='' selected>Selecione</option>";
    
    while($row = mysql_fetch_array($result))
    {
    
        if(($id <> NULL) && ($id == $row['id']))
            $var .= "<option value='". $row['id'] ."' sigla_tipo_documento='". $row['sigla'] ."' selected>". $row['sigla'] ." - ". $row['descricao'] ."</option>";
        else
            $var .= "<option value='". $row['id'] ."' sigla_tipo_documento='". $row['sigla'] ."' >". $row['sigla'] ." - ". $row['descricao'] ."</option>";
    }
    $var .= "</select>";
    return $var;
}

function naturezas($id){
    $result = mysql_query("SELECT * FROM naturezanotas ORDER BY nome;");
    $var = "<select id='natureza-nota' name='codNatureza' style='background-color:transparent;' >";
    while($row = mysql_fetch_array($result))
    {
        if(($id <> NULL) && ($id == $row['idNaturezaNotas']))
            $var .= "<option value='". $row['idNaturezaNotas'] ."' selected>". strtoupper($row['nome']) ."</option>";
        else
            $var .= "<option value='". $row['idNaturezaNotas'] ."'>". strtoupper($row['nome']) ."</option>";
    }
    $var .= "</select>";
    return $var;
}

function origemFundos($id,$outros){
    $cont = 1;
    $result = mysql_query("SELECT * FROM origemfundos where STATUS='A' ORDER BY forma;");
    $var = "<select name='origemFundos' class='OBG' style='background-color:transparent;'>";
    $var .= "	<option selected disabled value=''>Selecione...</option>";
    while($row = mysql_fetch_array($result))
    {
        if(($id <> NULL) && ($id == $row['forma']))
            $var .= "<option value='{$row['id']}' selected>".$row['COD_BANCO']." - ".$row['forma']." </option>";
        else
            if($cont == 1 ){
                $var .= "<option value=''>selecione</option>";
            }

        $var .= "<option value='{$row['id']}'>{$row['forma']}</option>";
        $cont++;

    }
    /////quando a função for chamadada para criar uma conta bancaria.

    $var .= "</select>";
    return $var;
}

function aplicacaoFundos($id){
    $result = mysql_query("SELECT * FROM aplicacaofundos where id in ( 4,5,6,7,8) ORDER BY forma;");
    $var = "<select name='aplicacaoFundos' class='OBG' style='background-color:transparent;'>";
    $var .= "	<option selected disabled value=''>Selecione...</option>";
    while($row = mysql_fetch_array($result))
    {
        if(($id <> NULL) && ($id == $row['forma']))
            $var .= "<option value='{$row['id']}' selected>{$row['forma']}</option>";
        else
            $var .= "<option value='{$row['id']}'>{$row['forma']}</option>";
    }
    $var .= "</select>";
    return $var;
}

function menuControleFaturamento(){
    $var  = "<p><a href='/financeiros/faturamento-controle-recebimento/?action=adicionarPrevisaoRecebimento'>Adicionar Previsão de Recebimento</a>";
    $var  .= "<p><a href='/financeiros/faturamento-controle-recebimento/?action=adicionarInformeRecebimento'>Adicionar Programação de Recebimento</a>";
    $var  .= "<p><a href='/financeiros/faturamento-controle-recebimento/?action=associar-fatura-nf-recebimento'>Associar Fatura a NF de Recebimento</a>";
    $var  .= "<p><a href='/financeiros/faturamento-controle-recebimento/?action=relatorioPrevisaoRecebimento'>Relatório Controle de Recebimento de Faturas</a>";

    return $var;
}

function menu_notas(){
    $var = "<div><center><h1>Contas a Pagar/Receber</h1></center></div>";
    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_pagar_receber']['financeiro_conta_pagar_receber_nova']) || $_SESSION['adm_user'] == 1) {
        $var .= "<p><a href=?op=notas&act=novo>Nova Conta a Pagar/Receber </a></p>";
    }
    
    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_pagar_receber']['financeiro_conta_pagar_receber_buscar_conta']) || $_SESSION['adm_user'] == 1) {
        $var .= "<p><a href='?op=notas&act=listar'>Buscar Contas a Pagar/Receber</a></p>";
    }
    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_pagar_receber']['financeiro_conta_pagar_receber_buscar_parcela']) || $_SESSION['adm_user'] == 1) {
        $var .= "<p><a href=?op=notas&act=buscar>Buscar Parcelas a Pagar/Receber</a></p>";
    }
    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_pagar_receber']['financeiro_conta_pagar_receber_fatura_aglutinacao']) || $_SESSION['adm_user'] == 1) {
        $var .= "<p><a href='/financeiros/fatura-imposto/?action=index-fatura-imposto'>Faturas de Aglutinação</a></p>";
    }
    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_pagar_receber']['financeiro_conta_pagar_receber_antecipada']) || $_SESSION['adm_user'] == 1) {
        $var .= "<p><a href='/financeiros/contas-antecipadas/?action=index-conta-antecipada'>Contas Antecipadas</a>";
    }
    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_pagar_receber']['financeiro_conta_pagar_receber_fatura_aglutinacao']) || $_SESSION['adm_user'] == 1) {
        $var .= "<p><a href='/financeiros/cnab/?action=index-bordero'>Borderos</a></p>";
    }
    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_pagar_receber']['financeiro_conta_pagar_receber_caixa_fixo']) || $_SESSION['adm_user'] == 1) {
        $var .= "<p><a href='/financeiros/caixa-fixo/?action=index-caixa-fixo'>Caixa Fixo</a></p>";
    }
    $var .= "<p><a href='/financeiros/previsao/?action=index-previsao'>Baixar Previsão</a></p>";
    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_pagar_receber']['financeiro_conta_pagar_receber_nova']) || $_SESSION['adm_user'] == 1) {
        $var .= "<p><a href='/financeiros/multiplas-contas/?action=criar-multiplas-notas&tipo=S'>Multi  Conta a Pagar </a></p>";
    }
    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_pagar_receber']['financeiro_conta_pagar_receber_nova']) || $_SESSION['adm_user'] == 1) {
        $var .= "<p><a href='/financeiros/multiplas-contas/?action=criar-multiplas-notas&tipo=E'>Multi Conta a Receber </a></p>";
    }
    /*if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_pagar_receber']['financeiro_conta_pagar_receber_fatura_aglutinacao']) || $_SESSION['adm_user'] == 1) {
        $var .= "<p><a href='/financeiros/cnab/?action=importar-arquivo-retorno'>Importar Retorno Cnab</a></p>";
    }*/
    // $var .= "<p> ______________________________________________";
    // $var .= "<p><a href='/financeiros/contas/?type=buscar-contas-ur'>Buscar Contas a Pagar/Receber da UR ". strtoupper($_SESSION['nome_empresa']). "</a>";
    return $var;
}

function ipcg4_busca_bootstrap()
{
    $result = mysql_query("SELECT * FROM plano_contas ORDER BY COD_N4");
    $var = "<select class='ipcg4_nota chosen-select form-control' name='ipcg4_nota'>";
    $var .= "<option value='' selected>Selecione...</option>";
    while($row = mysql_fetch_array($result))
    {
        $codigoNatureza = str_replace('.', '', $row['COD_N4']);
        $var .= "<option value='". $row['ID'] ."'>{$codigoNatureza} - {$row['N4']}</option>";
    }
    $var .= "</select>";

    return $var;
}

function aplicacaoFundosOption($list, $id = null){
    $result = mysql_query("SELECT * FROM aplicacaofundos where id in ($list) ORDER BY forma;");
    
   
    $var .= "	<option selected  value='' >Selecione...</option>";
    while($row = mysql_fetch_array($result))
    {
        if(($id <> NULL) && ($id == $row['id']))
            $var .= "<option value='{$row['id']}' selected>{$row['forma']}</option>";
        else
            $var .= "<option value='{$row['id']}'>{$row['forma']}</option>";
    }
   
    return $var;
}

function tipoDocumentoFinanceiroBootstrap(){
    $result = mysql_query("SELECT * FROM tipo_documento_financeiro where adiantamento != 'S' ORDER BY sigla;");
    $var = "";
     $var = "<select id='select_tipo_documento' name='select_tipo_doc_financeiro' class='select_small $ chosen-select form-control'  >";
    
        $var .="<option value='' selected>Selecione</option>";
    
    while($row = mysql_fetch_array($result))
    {
    
        
            $var .= "<option value='". $row['id'] ."' sigla_tipo_documento='". $row['sigla'] ."' >". $row['sigla'] ." - ". $row['descricao'] ."</option>";
    }
    $var .= "</select>";
    return $var;
}

function list_notas() {
    $fornecedorSelect = fornecedores_busca();
    $naturezaSelect = ipcg4_busca_bootstrap();
    $tipoDocumentoFinanceiroBootstrap = tipoDocumentoFinanceiroBootstrap();
    $aplicacaoFundosOption = aplicacaoFundosOption(implode(',', [1, 7, 5, 6, 9, 10]));
    $html = <<<HTML
<link rel="stylesheet" href="/utils/bootstrap-3.3.6-dist/css/bootstrap.min.css" />
<script src="/utils/bootstrap-3.3.6-dist/js/bootstrap.min.js" ></script>
<link rel='stylesheet' href='/utils/font-awesome-4.7.0/css/font-awesome.css' />
<div id='div-busca'>
<div id='dialog-desfazer-processamento-nota' data-idparcela=''>
 <p><span>Deseja realmente desfazer o processamento da Nota?</span></p>
 <span><b>Justificativa:</b><textarea class='OBG' id='justificativa-desfazer-processamento-nota'></textarea></span>
</div>
<form>
    <h1 class="text-center">Buscar Contas a Pagar/Receber</h1>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <input type='radio' name='btipo' class='btipo' value='t' CHECKED/> Todos &nbsp;&nbsp;
            <input type='radio' name='btipo' class='btipo' value='1'/> Contas a Pagar &nbsp;&nbsp;
            <input type='radio' name='btipo' class='btipo' value='0'/> Contas a Receber
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-4">
            <label>TR</label>
            <input type='text' name='codigo' class="form-control" />    
        </div>
        <div class="col-xs-4">
            <label>Numero da Nota</label>
            <input type='text' name='numero_nota' class="form-control" />    
        </div>
        <div class="col-xs-4">
            <label>Valor</label>
            <input type='text' class='valor form-control' name='valor' maxlength="15" style='text-align:right' />
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-6">
            <label>Fornecedor</label>
            {$fornecedorSelect}
        </div>
        <div class="col-xs-3">
            <label>Emissão de</label>
            <input type='text' name='inicioE' id='inicioE' class="form-control" maxlength='10' size='10' />
        </div>
        <div class="col-xs-3">
            <label>Emissão até</label>
            <input type='text' name='fimE' id='fimE' class="form-control" maxlength='10' size='10' />
        </div>
    </div>
    <br>
    <div class="row">
        
        <div class="col-xs-3">
            <label>Vencimento Real de</label>
            <input type='text' name='inicioVencimentoReal' id='inicioVencimentoReal' class="form-control" maxlength='10' size='10' />
        </div>
        <div class="col-xs-3">
            <label>Vencimento Real até</label>
            <input type='text' name='fimVencimentoReal' id='fimVencimentoReal' class="form-control" maxlength='10' size='10' />
        </div>
	    <div class="col-xs-6">
			<label for="">Natureza</label>
			{$naturezaSelect}
		</div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <label>Histórico contém:</label>
            <input type='text' name='descricao_nota' id='descricao_nota' class="form-control"  />
        </div>
        <div class="col-xs-6">
			<label>Tipo Documento</label>			
				{$tipoDocumentoFinanceiroBootstrap}			
		</div>
    </div>
    <div class="row">
        
        <div class="col-xs-3">
            <label>Competência de</label>
            <input type='text' name='competenciaInicio' id='competenciaInicio' class="form-control mes-ano" maxlength='10' size='10' />
        </div>
        <div class="col-xs-3">
            <label>Competência até</label>
            <input type='text' name='competenciaFim' id='competenciaFim' class="form-control mes-ano" maxlength='10' size='10' />
        </div>
        <div class="col-xs-6">
			<label>Forma Pagamento</label>
                <select id='select_forma_pagamento' name='select_forma_pagamento' class='select_small  $ chosen-select form-control'>    
                {$aplicacaoFundosOption}    
                </select>
        </div>
    </div>
    
    <br>
	<div class="row">
		<div class="col-xs-12 text-center">
			<button type="button" role="button" id='buscar_nota' class="btn btn-primary">
				<i class="fa fa-search"></i> Pesquisar
			</button>
			<a href="/financeiros/?op=notas" style="color: white;" class="btn btn-warning">
				<i class="fa fa-arrow-circle-left"></i> Voltar
			</a>
		</div>
	</div>
	<div class="row">
	    <div class="col-xs-12" id="resultado-busca-nota">
	    
        </div>
    </div>
</form>
</div>
HTML;
    echo $html;
}

function list_notas2($like) {

    
    echo "<div id='div-busca'><form>";
    echo"<h1><center>Buscar Contas a Pagar/Receber</center></h1>";
    echo "<p><b>Tipo:</b><input type='radio' name='btipo' class='btipo' value='t' CHECKED/>Todos&nbsp;&nbsp;";
    echo "</b><input type='radio' name='btipo' class='btipo' value='1'/>Contas a Pagar&nbsp;&nbsp;";
    echo "<input type='radio' name='btipo' class='btipo' value='0'/>Contas a Receber</p>";
    echo "<p><b>TR:</b><input type='text' name='codigo' />&nbsp;&nbsp;&nbsp;&nbsp;</p>";
    echo "<p><b>Numero da Nota:</b><input type='text' name='numero_nota' />&nbsp;&nbsp;&nbsp;&nbsp;</p>";
    echo "<p><b>Descri&ccedil;&atilde;o da Nota:</b><input type='text' size='60' name='descricao_nota' />&nbsp;&nbsp;&nbsp;&nbsp;</p>";
    echo "<p><b>Valor (R$):</b><input type='text' class='valor' name='valor' size='10' style='text-align:right' /><button id='limparvalor'>limpar</button></p>";
    echo "<p><b>Fornecedor:</b><span id='sp_fornecedor_busca' >" . fornecedores(NULL,'fornecedor') ." </span></p>";
    echo "<p><b>Emissão de:</b><input  type='text' name='inicioE' id='inicioE' maxlength='10' size='10' />";
    echo "&nbsp;<b>at&eacute;</b><input  type='text' name='fimE' id='fimE' maxlength='10' size='10' /></p>";

    echo "<p><button id='buscar_nota' type='button' style='float:left;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Pesquisar</span></button>
&nbsp;&nbsp;&nbsp;&nbsp;<button id='voltar_' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button>";

    echo "</form>
    
	</br><div id='resultado-busca-nota'></div>
	</div>";


}
///////////////////////jeferson

function xml_nota(){
    echo "<div id='div-xml-nota' title='Importar Nota'>";
    echo "<form name='xml' action='?op=notas&act=importar' method='post' enctype='multipart/form-data'>";
    echo "<div style='width:53px;float:left;display:table;padding-right:10px;'><label><b>Arquivo:</b></label></div>";
    echo"<div style='display:table;width:200px;padding-right:100px;' ><input name='xml' type='file'/></div>";
    echo"<br/><button id='enviar_xml_nota' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
	<span class='ui-button-text' id='enviar_xml_nota'>Enviar</span></button></form></div>";

}

/************************************************
 * ROTINA DE IMPORTA��O DE NOTAS ATRAV�S DO XML *
 ************************************************/

function importar($file){

    // Pasta onde o arquivo vai ser salvo
    $nota['pasta'] = 'xml/';

    // Tamanho m�ximo do arquivo (em Bytes)
    $nota['tamanho'] = 1024 * 1024 *1024 * 1024 *1024 * 1024 * 2;

    // Array com as extens�es permitidas
    $nota['extensoes'] = array('xml');

    // Renomeia o arquivo? (Se true, o arquivo ser� salvo como .xml e um nome �nico)
    $nota['renomeia'] = false;

    // Array com os tipos de erros de upload do PHP
    $nota['erros'][0] = 'Importada com Sucesso!';
    $nota['erros'][1] = 'O arquivo no upload � maior do que o permitido';
    $nota['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado';
    $nota['erros'][3] = 'O upload do arquivo foi feito parcialmente';
    $nota['erros'][4] = 'N�o foi feito o upload do arquivo!';

    // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
    if ($file['xml']['error'] != 0) {
        die("N�o foi poss�vel fazer o upload, erro:<br />" . $nota['erros'][$file['xml']['error']]);
        exit; // Para a execu��o do script
    }

    // Caso script chegue a esse ponto, n�o houve erro com o upload e o PHP pode continuar

    // Faz a verifica��o da extens�o do arquivo
    $extensao = strtolower(end(explode('.', $file['xml']['name'])));

    if (array_search($extensao, $nota['extensoes']) === false) {
        echo "Por favor, envie arquivos no formato XML";
    }

    // Faz a verifica��o do tamanho do arquivo
    else if ($nota['tamanho'] < $file['xml']['size']) {
        echo "O arquivo enviado � muito grande, envie arquivos de at� 2Mb.";
    }

    // O arquivo passou em todas as verifica��es, hora de tentar mov�-lo para a pasta
    else {
        // Primeiro verifica se deve trocar o nome do arquivo
        if ($nota['renomeia'] == true) {
            // Cria um nome baseado no UNIX TIMESTAMP atual e com extens�o .jpg
            $nome_final = time().'.jpg';
        } else {
            // Mant�m o nome original do arquivo
            $nome_final = $file['xml']['name'];
        }

        // Depois verifica se � poss�vel mover o arquivo para a pasta escolhida
        if (@move_uploaded_file($file['xml']['tmp_name'], $nota['pasta'] . $nome_final)) {
            // Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
            //echo "Upload efetuado com sucesso!";
            //echo '<br /><a href="' . $nota['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
            $arq_xml = $nota['pasta'] . $nome_final;


            $dados = $processar_importacao($arq_xml);

            $show_itens($dados[numero_nota],$dados[valor_nota],$dados[fornecedor],'Nota Importada '.date("d/m/Y"),$dados[val_produtos],$dados[icms],$dados[frete],$dados[seguro],$dados[ipi],$dados[issqn],$dados[outros],$dados[des_bon],$dados[produtos],$dados[chave]);
        } else {
            // N�o foi poss�vel fazer o upload, provavelmente a pasta est� incorreta
            echo "N�o foi poss�vel enviar o arquivo, tente novamente!";
        }

    }
    return $dados;
}

function processar_importacao($arq_xml){
    $xml1 = @simplexml_load_file($arq_xml);

    //ID
    $numero_nota = $xml1->NFe->infNFe->ide->nNF;
    //chave
    foreach($xml1->NFe->infNFe[0]->attributes() as $id=>$val){
        if($id=="Id")
            $chave = $val;
    }



    //DADOS DO FORNECEDOR
    $fornecedor = $xml1->NFe->infNFe->emit->xNome;
    $fantasia = $xml1->NFe->infNFe->emit->xFant;
    $cnpj =  $xml1->NFe->infNFe->emit->CNPJ;
    $cidade =  $xml1->NFe->infNFe->emit->enderEmit->xMun;
    $logradouro =  $xml1->NFe->infNFe->emit->enderEmit->xLgr;
    $bairro =  $xml1->NFe->infNFe->emit->enderEmit->xBairro;
    $telefone =  $xml1->NFe->infNFe->emit->enderEmit->fone;
    $email =  $xml1->NFe->infNFe->emit->enderEmit->xBairro;

    $des_bon =  (double) $xml1->NFe->infNFe->total->ICMSTot->vDesc;
    $valor_nota = (double) $xml1->NFe->infNFe->total->ICMSTot->vNF;
    $outros = (double) $xml1->NFe->infNFe->total->ICMSTot->vOutro;

    //IMPOSTOS
    $icms = (double) $xml1->NFe->infNFe->total->ICMSTot->vST;
    $frete = (double) $xml1->NFe->infNFe->total->ICMSTot->vFrete;
    $seguro = (double) $xml1->NFe->infNFe->total->ICMSTot->vSeg;
    $ipi = (double) $xml1->NFe->infNFe->total->ICMSTot->vIPI;
    $val_produtos =  (double) $xml1->NFe->infNFe->total->ICMSTot->vProd;
    if($xml1->NFe->infNFe->total->ICMSTot->vISSQN)
        $issqn =  (double) $xml1->NFe->infNFe->total->ICMSTot->vISSQN;
    else
        $issqn = 0.00;




    foreach($xml1->NFe->infNFe->det as $xmlProd){

        $produtos[]= $xmlProd->prod->xProd."#".$xmlProd->prod->vProd."#".$xmlProd->prod->qCom."#".$xmlProd->prod->CFOP;

    }


    $forn = $verificar_fornecedor($cnpj);


    if($forn==0){
        $fornecedor = $cadastrar_fornecedor($fornecedor,$fantasia,$cnpj,$telefone,$logradouro,$bairro,$cidade);
    }else{
        $fornecedor = $buscar_fornecedor($cnpj);
    }

    $dados = array(
        "numero_nota"=>$numero_nota,
        "fornecedor"=>(int) $fornecedor,
        "cnpj"=>$cnpj,
        "des_bon"=>number_format($des_bon,2,",","."),
        "valor_nota"=>number_format($valor_nota,2,",","."),
        "icms"=>number_format($icms,2,",","."),
        "frete"=>number_format($frete,2,",","."),
        "seguro"=>number_format($seguro,2,",","."),
        "ipi"=>number_format($ipi,2,",","."),
        "val_produtos"=>number_format($val_produtos,2,",","."),
        "issqn"=>number_format($issqn,2,",","."),
        "outros"=>number_format($outros,2,",","."),
        "chave"=>$chave,
        "produtos"=>$produtos
    );


    return $dados;
}

function verificar_fornecedor($cnpj){
    $sql = "Select count(*) as total from fornecedores where cnpj =$cnpj LIMIT 1";
    $res = mysql_query($sql);
    while($row = mysql_fetch_array($res))
    {
        return $row['total'];
    }
}

function verificar_cidade($cidade){
    $sql = "Select ID from CIDADES where NOME = '{$cidade}' LIMIT 1";
    $res = mysql_query($sql);
    if($res){
        while($row = mysql_fetch_array($res))
        {
            return $row['ID'];
        }
    }
}

function cadastrar_fornecedor($fornecedor,$fantasia,$cnpj,$telefone,$logradouro,$bairro,$cidade){
    $cid_cod = $verificar_cidade($cidade);
    $sql = "INSERT INTO fornecedores (nome,razaoSocial,cnpj,telefone,endereco,bairro,cidade_id) VALUES('{$fornecedor}','{$fantasia}','{$cnpj}','{$telefone}','{$logradouro}','{$bairro}','{$cid_cod}')";

    $res = mysql_query($sql);


    return mysql_insert_id();
}

function buscar_fornecedor($cnpj){
    $sql = "Select idFornecedores from fornecedores where cnpj = '{$cnpj}' LIMIT 1";
    $res = mysql_query($sql);
    if($res){
        while($row = mysql_fetch_array($res))
        {
            return $row['idFornecedores'];
        }
    }
}
function ipcf2($id, $sel,$disabled){
    $cont=1;
    $id = $id;
    $cond=" N1 like 'S' ";


    if ($id == 1){
        $cond=" N1 like 'S'";
    }
    if ($id == '0'){
        $cond=" N1 like 'E'";
    }

    $result = mysql_query("SELECT DISTINCT N2 FROM plano_contas where {$cond} ORDER BY ID");
    $var .= "<select class='ipcf2_notas COMBO_OBG select_small' name='ipcf2_notas' $disabled >";
    while($row = mysql_fetch_array($result))
    {
        if(($id != NULL) && ($sel == $row['N2'])){
            $var .= "<option value='". $row['N2']."' selected>". $row['N2'] ."</option>";
        }
        else{
            if($cont == 1 ){
                $var .= "<option value=''>selecione</option>";
            }

            $var .= "<option value='". $row['N2'] ."'>". $row['N2'] ."</option>";
            $cont++;
        }
    }
    $var .= "</select>";

    //print_r($var);

    if ($id == NULL || $sel <> ''){
        return $var;
    }else print_r($var);
}

function ipcg3($cod, $sel, $tipo_nota, $disabled = '') {
    $cont=1;
    if ($tipo_nota == 1){
        $cond=" N1 like 'S' and N2 like '%{$cod}%'";
    }
    if ($tipo_nota == '0'){
        $cond=" N1 like 'E'  and N2 like '%{$cod}%'";
    }


    $sql ="SELECT DISTINCT N3 FROM plano_contas where {$cond} ORDER BY ID";
    $result = mysql_query($sql);
    $var .= "<select class='ipcg3_notas COMBO_OBG select_small' name='ipcg3_notas' $disabled>";
    while($row = mysql_fetch_array($result))
    {
        if(($cod != NULL) && ($sel == $row['N3'])){
            $var .= "<option value='". $row['N3']."' selected>". $row['N3'] ."</option>";
        }
        else{
            if($cont == 1 ){
                $var .= "<option value=''>selecione</option>";
            }

            $var .= "<option value='". $row['N3'] ."'>". $row['N3'] ."</option>";
            $cont++;
        }
    }
    $var .= "</select> ";



    if ($sel <> NULL ){
        return $var;
    }else print_r($var);
}

function empresa($id = null, $cod = null, $onclick = 0, $disabled = ""){
    $cont=1;
    $var = "";
    $result = mysql_query("SELECT * FROM empresas where ATIVO = 'S' ORDER BY nome ");
    if ($onclick==0){
        $var .= "<img align='left' class='del_rateio'  src='../utils/delete_16x16.png'  title='Remover' $onclick  border='0'/> &nbsp;&nbsp;";
    }
    $var .=   "<select $disabled class='empresa COMBO_OBG' name='empresa' style='background-color:transparent;font-size:10px;'>";
    $var .= "<option value=''>selecione</option>";
    while($row = mysql_fetch_array($result))
    {
        $var .= "<option " . (($id != NULL) && ($id == $row['id']) ? 'selected' : '') . " value='". $row['id']."'>". $row['nome'] ."</option>";
    }
    $var .= "</select>";

    //print_r($var);

    if ($id == NULL || $cod==1){
        return $var;
    }else print_r($var);
}

function ur($id,$cod){
    $cont=1;
    $id = $id;
    $result = mysql_query("SELECT * FROM empresas where ATIVO = 'S' and empresas.id in {$_SESSION['empresa_user']} ORDER BY nome ");

    //$var.= "<img align='left' class='del_rateio' title='Remover' border='0'/>";
    /* if ($id == 1){
         $disabled = '';
     }else{
         $disabled = "disabled='disabled'";
     }*/

    $var .= "<select $disabled class='empresa COMBO_OBG' name='empresa2' style='background-color:transparent;font-size:10px;'>";
    while($row = mysql_fetch_array($result))
    {
        if(($id != NULL) && ($id == $row['id'])){
            $var .= "<option value='". $row['id']."' selected>". $row['nome'] ."</option>";
            $cont++;
        }else{
            if($cont == 1 ){
                $var .= "<option value=''>selecione</option>";
            }

            $var .= "<option value='". $row['id'] ."'>". $row['nome'] ."</option>";
            $cont++;
        }
    }
    $var .= "</select>";

    //print_r($var);


    return $var;

}

function assinatura($id,$c,$disabled){
    $cont=1;
    $result = mysql_query("SELECT * FROM assinaturas where 1 ORDER BY DESCRICAO ");
    $var = "<select class='assinatura select_small OBG' name='assinatura' $disabled >";
    $var .= "	<option value=''>selecione</option>";
    while($row = mysql_fetch_array($result))
    {
        if($id <> NULL && $id == $row['ID']){
            $var .= "<option value='". $row['ID'] ."' selected>". $row['DESCRICAO'] ."</option>";
        }else{
            $var .= "<option value='". $row['ID'] ."'>". $row['DESCRICAO'] ."</option>";
        }
        $cont++;

    }
    $var .= "</select>";

    if($c == 1) {
        return $var;
    } else {
        echo $var;
	}
}

function ipcg4($id, $c = 1, $ipcg4 = null ,$tipo_nota = 1, $ipcf2 = null , $disabled = ''){


    //if($id != '' && $id != null && $id != "undefined"){
    $cond = " N1 = 'S'";
    if ($tipo_nota == '0'){
        $cond = " N1 like 'E'";
    }
    $result = mysql_query("SELECT * FROM plano_contas WHERE {$cond} ORDER BY ID");
    $var = "<select class='ipcg4_nota chosen-select OBG_CHOSEN' $disabled name='ipcg4_nota' style='width: 300px' >";
    $var .= "<option value='' selected>Selecione...</option>";
    while($row = mysql_fetch_array($result))
    {
        if($id == $row['ID'])
            $var .= "<option value='". $row['ID'] ."' selected>{$row['COD_N4']} - {$row['N4']}</option>";
        else
            $var .= "<option value='". $row['ID'] ."'>{$row['COD_N4']} - {$row['N4']}</option>";
    }
    $var .= "</select>";

    if($c == 1){
        return $var;
    }else{
        echo $var;
    }
    /*}else{
        if($c == 1){
            return $var;
        }else{
            print_r($var);
        }
    }*/
}

function input_data($nome,$obg,$vencimento){
    $var= "<input value='{$vencimento}' name='$nome' type='text' maxlength='10' size='10' disabled class='data $obg'/>";
    return $var;
}

function input_data_imposto_retido($nome,$obg,$vencimento){
    $var= "<input value='{$vencimento}' name='$nome' type='text' maxlength='10' size='10' disabled class='imposto-retido $obg'/>";
    return $var;
}



function impostos_retidos($idNotas,$poder_editar,$notaref){
    $configRetencoes = Retencoes::getConfiguracaoRetencoes();

    //para não repetir o o imposto IR
    $comparar=1;


    if(isset($idNotas) && $idNotas>0){
        $sql= "select
            n.idNotas,
            n.tipo,
            DATE_FORMAT(p.vencimento,'%d/%m/%Y') as venc,
            n.codigo,
            n.valor,
            n.codFornecedor,
            n.IMPOSTOS_RETIDOS_ID,
            n.status

            from
            notas as n
            inner join parcelas as p on(n.idNotas = p.idNota)
            where
            n.NOTA_REFERENCIA={$idNotas}
            and n.status <> 'Cancelada' ";
        $result= mysql_query($sql);

        while($row = mysql_fetch_array($result))
        {
            $impostos[$row[IMPOSTOS_RETIDOS_ID]] =array('id_nota'=>$row['idNotas'],'vencimento'=>$row['venc'],
                'valor'=>$row['valor'],'fornecedor'=>$row['codFornecedor'],
                'codigo'=>$row['codigo'],'status'=>$row['status']);

            if($row['IMPOSTOS_RETIDOS_ID']==1){
                $comparar=2;
            }
        }


    }

    $sql=" select
            *
            from
            impostos_retidos
            where
            1
        ";
    $result= mysql_query($sql);

    if($idNotas==0){
        $colspan=7;
        $var= "<span><b>COMPROMISSO DE IMPOSTOS RETIDOS</b></span>
                  <table class='mytable'  width='70%'>
                   <thead>
                    
                    <tr>
                        <th>RETENÇÃO</th>
                        <th>TIPO DOC.</th>
                        <th>CÓD. DARF/DAM</th>
                        <th>VENCIMENTO</th>
                        <th>VALOR</th>
                        <th>FORNECEDOR</th>
                        <th>IPCF4</th>
                    </tr>
                  </thead>";
        while($row = mysql_fetch_array($result))
        {
            $id=$row['ID'];

            if($id!=1){
                //se for issqn

                $disabled = "disabled='disabled'";
                $codigo = $row['IMPOSTO']=='IR'?'':$row['CODIGO'];
                $impostos_retidos_id = $row['IMPOSTO']=='IR'?'':$row['ID'];

                $nome=strtolower($row['IMPOSTO']);
                $conta = $configRetencoes['desc_' . $nome];
                $contaId = $configRetencoes[$nome];
                if($nome == 'issqn') {
                    $conta = $configRetencoes['desc_iss'];
                    $contaId = $configRetencoes['iss'];
                }
                $var .=  "<tr class='imposto_retido' id_imposto_retido='{$impostos_retidos_id}'>
                                     <td><b>{$row['IMPOSTO']}</b></td>
                                     <td>".notasTipoDocumento($row['tipo_documento_financeiro_id'],'tipo_documento_'.$nome,false,'disabled', 'OBG_CHOSEN', '200px')."</td>
                                     <td id='cod_{$nome}'><input name = 'cod_{$nome}' value='{$codigo}' $disabled/></td>
                                     <td>".input_data_imposto_retido("vencimento_".$nome,$obg)."</td>
                                     <td><input  name='valor_{$nome}' ipcf4='{$contaId}' class='valor' style='text-align:right' disabled value='0,00' /></td>
                                     <td>" . fornecedores($row['FORNECEDOR_ID'],"fornecedor_{$nome}",1,$disabled) ."</td>
                                     <td>{$conta}</td>

                                  </tr>";
            }

        }
    }else{
        $colspan=9;
        $var= "
            <span><b>COMPROMISSO DE IMPOSTOS RETIDOS</b></span>
                  <table class='mytable' width='70%'>
                   <thead>
                    <tr>
                         <th>RETENÇÃO</th>
                        <th>TIPO DOC.</th>
                        <th>CÓD. DARF/DAM</th>
                        <th>VENCIMENTO</th>
                        <th>VALOR</th>
                        <th>FORNECEDOR</th>
                        <th>IPCF4</th>
                        <th>TR</th>
                        <th>STATUS</th>
                    </tr>
                  </thead>";
        while($row = mysql_fetch_array($result)){

            if (array_key_exists($row['ID'], $impostos)) {
                $vencimento = $impostos[$row['ID']]['vencimento'];
                $fornecedor = $impostos[$row['ID']]['fornecedor'];
                $codigo     = $impostos[$row['ID']]['codigo'];
                $id_nota    = $impostos[$row['ID']]['id_nota'];
                $valor      = $impostos[$row['ID']]['valor'];
                $status     = $impostos[$row['ID']]['status'];
                $impostos_retidos_id= $row['ID'];
            }else{
                $codigo     = $row['IMPOSTO']=='IR'?'':$row['CODIGO'];
                $vencimento ='';
                $fornecedor =$row['FORNECEDOR_ID'];
                $id_nota    = 0;
                $valor      = 0.00;
                $status    ='';
                $impostos_retidos_id = $row['IMPOSTO']=='IR'?'':$row['ID'];
            }

            if($row['ID']!=$comparar){
                $nome=strtolower($row['IMPOSTO']);
                $obrigatorio='';
                //se for issqn
                if ($poder_editar == 0 && $notaref ==0) {
                    $disabled = "disabled='disabled'";
                   
                } else {
                    $disabled="disabled='disabled'";
                }

 $var .=  "<tr class='imposto_retido' id_imposto_retido='{$impostos_retidos_id}'  id_nota='{$id_nota}'>
                <td><b>{$row['IMPOSTO']}</b></td>
                <td>".notasTipoDocumento($row['tipo_documento_financeiro_id'],'tipo_documento_'.$nome,false,'disabled', 'OBG_CHOSEN', '200px')."</td>
                <td id='cod_{$nome}'><input name = 'cod_{$nome}' value='{$codigo}' disabled/></td>
                <td>".input_data("vencimento_".$nome,$obg,$vencimento)."</td>
                <td><input  name='valor_{$nome}' ipcf4='{$row['PLANO_CONTA_ID']}' class='valor' style='text-align:right' disabled value='".number_format($valor,2,',','.')."' /></td>
                <td>" . fornecedores($fornecedor,"fornecedor_{$nome}",1,$disabled,$obrigatorio ) ."</td>
                <td>".$row['IPCF4']."</td>
                <td>$id_nota</td>
                <td>$status</td>
             </tr>";
            }
        }
    }



    $var .=" <tfoot>
                                  <tr>
                                      <th colspan='{$colspan}'></th>
                                  </tr>
                          </tfoot>

              </table>
              <br/>";
    return $var;
}
//////////////////////jeferson
function new_notas() {
    //ini_set('display_errors', 1);
    $sql = <<<SQL
SELECT * FROM retencoes_vencimentos
SQL;
    $rs = mysql_query($sql);
    $vencimentos = [];
    while($row = mysql_fetch_array($rs)) {
        $vencimentos[$row['tipo']][$row['retencao']] = $row['vencimento'];
    }
    $vencimentosJson = json_encode($vencimentos);
    $var = "<link rel='stylesheet' type='text/css' href='/utils/reports.css'>";
    $var .="<div id='dialog-message' title='Nota salva'>
                <p>
                    <span id='trGerada'></span>".
                " <div id='text-dialog'></div>
                </p>
            </div>";
    $var .= "<div id='div-nova-nota'>";
        $var .="<h1>
                    <center>Nova Conta a Pagar/Receber</center>
                </h1>";
    $var .= alerta();
    $fechamento_caixa = \App\Controllers\FechamentoCaixa::getFechamentoCaixa();
        $var .= "<form>
            <div class='half'>
                <label class='label negrito'>COMPETÊNCIA:</label>
                <input class='OBG mes-ano' type='text' maxlength='10' size='10' name='dataCompetencia' id='data-competencia'/> &nbsp;&nbsp;
            </div>";
        $var .= "<input id='data-fechamento' type='hidden' value='{$fechamento_caixa['data_fechamento']}'>";
            $var .= "<div class='full'>";
                $var .= "<label class='label negrito'>TIPO:</label>";
                $var .= "<label><input class='tipo_nota' type='radio' name='tipo' value='0' />RECEBIMENTOS</label>&nbsp;&nbsp;";
                $var .= "<label><input class='tipo_nota' type='radio' name='tipo' value='1' CHECKED='checked'/>DESEMBOLSOS</label>";
            $var .= "</div>";
            $var .= "<div class='half'>
                        <label class='label negrito'>TIPO DOCUMENTO:</label>
                        ".notasTipoDocumento(NULL,'tipo_documento',false,'', 'OBG_CHOSEN')."&nbsp;
                    </div>
                    <div class='half'>
                        <label class='label negrito'>DATA DE EMISS&Atilde;O:</label>
                        <input class='OBG data-emissao-nota' type='text' maxlength='10' size='10' name='dataEntrada' id='entrada'/> &nbsp;&nbsp;
                        <input type='hidden' id='vencimentos-json' value='{$vencimentosJson}' />
                        <label class='negrito'> N&deg;:</label>
                        <input class='OBG' type='text' name='num' id=num'/>
                    </div>
                   
                    ";
            $resultUr = mysql_query("SELECT id, nome FROM empresas where ATIVO = 'S' and id in {$_SESSION['empresa_user']} ORDER BY nome ");
            $var .= "<div class='full'>
                        <label class='label negrito'>UNIDADE REGIONAL:</label>
                        <select id='empresa-nota' name='ur' class='chosen-select OBG_CHOSEN' data-placeholder='Selecione a unidade responsável...' style='background-color:transparent; width: 305px;' >
                            <option selected disabled value=''>Selecione...</option>";
                         while ($rowUr = mysql_fetch_array($resultUr)) {
                            $var .= "<option " . ($rowUr['id'] != $_SESSION['empresa_principal'] ? '' : 'selected') . " value='{$rowUr['id']}'>{$rowUr['nome']}</option>";
                            }
                $var .= "</select>
                    </div>";
                    $var .= "<div class='full'>
                    <label class='label negrito'>FORMA PAGAMENTO:</label>
                    <select id='forma-pagamento' name='forma_pagamento' class='chosen-select OBG_CHOSEN' data-placeholder='Selecione a forma de pagamenteo...' style='background-color:transparent; width: 305px;' >";
                       
                        $var .= aplicacaoFundosOption(implode(',', [1, 7, 5, 6, 9, 10]));
                     
                        // $var .= "<option value='Boleto'>Boleto</option>";
                        // $var .= "<option value='Cartão de Crédito'>Cartão de Crédito</option>";
                        // $var .= "<option value='Transferência'>Transferência</option>";
                        
            $var .= "</select>
                </div>";
            $var .= "<div class='full' style='word-wrap: break-word'>
                        <label class='label negrito'>NATUREZA:</label>
                        <input type='number' name='codigo_conta' id='codigo-conta' class='OBG' min='1' step='1' max='999999' /> &nbsp;
                        <input type='hidden' name='natureza_movimentacao' id='natureza-movimentacao' class='OBG' /> &nbsp;
                        <img src='/utils/look_16x16.png' id='buscar-naturezas-dialog' style='cursor: pointer;' /> &nbsp;
                        <span class='nome-natureza negrito' style='color: red'></span>
                    </div>";
    $sql = <<<SQL
SELECT 
  ID,
  N1,
  COD_N4, 
  N4, 
  POSSUI_RETENCOES,
  ISS,
  IR,
  PIS,
  COFINS,
  CSLL,
  INSS
FROM 
  plano_contas
ORDER BY 
  CONCAT(COD_N4, ' - ', N4) ASC
SQL;
    $rs = mysql_query($sql);
    $naturezas = [];
    while ($linha = mysql_fetch_array($rs, MYSQL_ASSOC)) {
        $naturezas[] = $linha;
    }

    $var .= "<div class='full'>
                <div id='dialog-listar-naturezas' title='Lista de Naturezas' tipo = '' style='display: none; min-height: 400px; overflow: hidden;'>
                    <div>
                        <input type='text' id='filtrar-natureza' style='text-transform: uppercase;' size='70' maxlength='50'>
                    </div>
                    <br>
                    <div style='height: 300px; overflow: auto;'>
                        <table class='mytable' id='table-lista-naturezas' width='100%' style='cursor:pointer;'>
                          <tbody>";
                                foreach ($naturezas as $natureza) {
                                    $json_natureza = json_encode($natureza);
                                    $contaCodigo = str_replace('.', '', $natureza['COD_N4']);
                                    $var .= "<tr tipo='{$natureza['N1']}' class='escolher-natureza' json-natureza='{$json_natureza}'>
                                                <td class='desc-natureza'>{$contaCodigo} - ({$natureza['COD_N4']} - {$natureza['N4']})</td>
                                            </tr>";
                                }
                $var .= "</tbody>
                        </table>
                    </div>
                </div>
             </div>";
    $var .= "<div class='full' style='word-wrap: break-word'>
                <label class='label negrito'>FORNECEDOR / CLIENTE:</label>
                ".fornecedores(NULL,'fornecedor',false,'', 'OBG_CHOSEN')."
             </div>";
    $var .= "<div class='full'>
                <label class='label negrito'>DESCRI&Ccedil;&Atilde;O:</label>
                <textarea style='text-transform: uppercase;' name='descricao' id='descricao' cols='80' rows='3'></textarea>
	         </div>";
    $var .="<div class='full'>    
                <table class='mytable'
                    width='100%' 
                    style='position: relative; 
                            float: left; 
                            margin: 0 auto;'>
                    <thead>
                        <tr>
                        <th colspan='5' align='center'>RETENÇÕES</th>
                        </tr>
                    </thead>";

            $var .="<tr id='impostos'>
                        <td><b>VAL. PRODUTO/SERV.</b></td>
                        <td><b>ICMS</b> <b id='icms-texto-aliquota'></b></td>
                        <td><b>FRETE</b></td>
                        <td><b>SEGURO</b></td>
                    </tr>";
            $var .="<tr id='impostos_valores'>
                        <td>R$ <input id='val_produtos' maxlength='15' name='val_produtos' class='valor OBG calcular_val_nota' style='text-align:right' value='0,00' /></td>
                            <td>R$ <input id='icms' aliquota='' name='icms' maxlength='15' class='valor calcular_val_nota' style='text-align:right' value='0,00' /></td>
                        <td>R$ <input id='frete'  name='frete' style='text-align:right'  maxlength='15' class='valor calcular_val_nota' value='0,00'/></td>
                        <td>R$ <input  id='seguro' name='seguro' style='text-align:right' maxlength='15' class='valor calcular_val_nota' value='0,00'/></td>
                    </tr>";
            $var .="<tr id='impostos1'>
                        <td><b>IPI</b> <b id='ipi-texto-aliquota'></b></td>
                        <td><b>ISSQN</b> <b id='iss-texto-aliquota'></b></td>
                        <td><b>DESPESAS ACESS.</b></td>
                        <td><b>DESCONTO/BONIF.</b></td>
                    </tr>";
            $var .="<tr id='impostos_valores1'>
                        <td>R$ <input id='ipi' aliquota='' maxlength='15' class='valor calcular_val_nota' name='ipi' style='text-align:right' value='0,00' /></td>
                        <td>R$ <input id='issqn' aliquota='' name='issqn' maxlength='15'  style='text-align:right' class='valor calcular_val_nota' value='0,00' /></td>
                        <td>R$ <input id='des_acessorias'   maxlength='15' class='valor calcular_val_nota' name='des_acessorias' style='text-align:right' value='0,00' /></td>
                        <td>R$ <input id='des_bon' name='des_bon' style='text-align:right'  maxlength='15' class='valor calcular_val_nota' value='0,00' /></td>
                    </tr>";
            $var .="<tr id=''>
                        <td><b>PIS</b> <b id='pis-texto-aliquota'></b></td>
                        <td><b>COFINS</b> <b id='cofins-texto-aliquota'></b></td>
                        <td><b>CSLL</b> <b id='csll-texto-aliquota'></b></td>
                        <td><b>IR</b> <b id='ir-texto-aliquota'></b></td>
                    </tr>";
            $var .="<tr id=''>
                            <td>R$ <input id='pis' aliquota='' maxlength='15' class='valor calcular_val_nota' name='pis' style='text-align:right' value='0,00' /></td>
                            <td>R$ <input id='cofins' aliquota='' name='cofins'  style='text-align:right' maxlength='15' class='valor calcular_val_nota' value='0,00' /></td>
                            <td>R$ <input id='csll' aliquota='' maxlength='15' class='valor calcular_val_nota' name='csll' style='text-align:right' value='0,00' /></td>
                            <td>R$ <input id='ir' aliquota='' name='ir' style='text-align:right' maxlength='15' class='valor calcular_val_nota' value='0,00' /></td>
                    </tr>";
            $var .="<tr id=''>
                        <td collspan='2'><input type='checkbox' name='check_pcc' id='check_pcc'/>
                            <label for='check_pcc'><b>PCC</b></label>
                        </td>
                        <td><b>INSS</b> <b id='inss-texto-aliquota'></b></td>
                        <td colspan='2'><b>VALOR TOTAL</b></td>
                    </tr>";
            $var .="<tr id=''>
                            <td collspan='2'>R$ <input id='pcc' aliquota='' name='pcc' disabled='disabled' style='text-align:right' maxlength='15' class='valor calcular_val_nota' value='0,00' /></td>
                            <td>R$ <input id='inss' aliquota='' maxlength='15' class='valor calcular_val_nota' name='inss' style='text-align:right'  value='0,00' /></td>
                            <td>R$ <input type='text' maxlength='15' class='valor' value='0' id='valor_nota' name='valor_nota' readonly style='text-align: right'/></td>
                            <td align='center'>
                                <button type='button' id='calcular-retencoes' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'>
                                    <span class='ui-button-text'>Calcular Retenções</span> 
                                </button>
                            </td>
                    </tr>";
        $var .= "<tfoot>
                    <tr>
                        <th colspan='4'></th>
                    </tr>
                </tfoot>";
        $var .="</table>
            </div>
            <div class='full'></div>";
    $var .= "<div class='full' id='div_parcelas'>";
        $var .= '<script type="text/javascript" src="/utils/bower_components/moment/moment.js"></script>';
        $var .= '<script type="text/javascript" src="/utils/jquery/js/validar_vencimento_real.js?v=1.2"></script>';
        $var .= "<b>PARCELAS:</b>";
        $var .= "<p>
                    Gerar várias parcelas: <br>
                    Qtd.: <input type='number' name='qtd_parcelas_gerar' class='qtd-parcelas-gerar' size='3' maxlength='3'  value='1'>
                    Vencimento: <input type='text' name='vencimento_parcelas_gerar' class='vencimento-parcelas-gerar mask-data' size='10' maxlength='10' value='" . date('d/m/Y') . "'>
                    Valor: <input type='text' name='valor_parcelas_gerar' class='valor-parcelas-gerar valor'  maxlength='15' value='0,00'>
                    <button type='button' class='gerar-varias-parcelas' from='new-notas'>+</button>
                    <br>
                    <small>*O vencimento será gerado automaticamente baseada na primeira data.</small>
                </p>";
    $var .="<div  style='overflow: auto;'>";
    $var .= '<div id="mensagem-erro-parcelas"></div>';
        $var .= "<table class='mytable' id='parcelas' width='50%'>";
            $var .= "<thead>
                        <tr>
                            <th><input type='checkbox' class='selecionar-todos'></th>
                            <th width='25%'>TR PARCELA</th>
                            <th>VENCIMENTO</th>
                            <th>VENCIMENTO REAL</th>
                            <th>VALOR</th>
                            <th>COD. BARRA</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th colspan='6'></th>
                        </tr>
                    </tfoot>
                    <tbody>";
                $var .= "<tr class='parcela' >
                            <td width='5%'><input type='checkbox' class='selecionar-parcela-exclusao'></td>
                            <td width='25%'>
                                <img align='left' class='del_parcela'  src='../utils/delete_16x16.png' title='Remover' border='0'/> &nbsp;
                                <input type='text' name='tr_parcela' value='1'  size='5' />
                            </td>
                            <td width='20%'>
                                <input value='". date("d/m/Y") ."' type='text' maxlength='10' size='10' class='vencimento-parcela mask-data OBG '/>
                                <input type='hidden' maxlength='128' size='20' name='codBarras' />
                            </td>
                            <td width='20%'>
                                <input value='". date("d/m/Y") ."' type='text' maxlength='10' size='10' class='vencimento-real-parcela mask-data OBG '/>
                              
                            </td>
                            <td>
                                <input class='valor OBG valor_parcela' value='0,00' type='text' maxlength='15' style='text-align:right' />
                            </td>
                            <td >
                                <input value='' type='text'  size='70' class='codigo-barra'/>                              
                            </td>
                        </tr>";
            $var .= "</tbody>
                </table>
            </div>";
        $var .= "<button id='add_parcela' >Adicionar Parcela +</button> 
                <button type='button' id='excluir-parcelas-selecionadas' >Excluir parcelas selecionadas</button>";
    $var .= "</div>";

    $var .= "<div class='full'>";
    $var .= "</div>";

    $var .= "<div class='full' style='text-align: center'>";
        $var .= "<button type='button' id='calcular-vencimentos' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'>
                    <span class='ui-button-text'>Calcular Vencimentos</span> 
                </button>";
    $var .= "</div>";
    $var .= "<div class='full' style='overflow: auto;'>";   
    $var .= impostos_retidos();
    $var .= "</div>";

    $var .= "<br><br>";
    $var .= "<div class='full' id='div_rateio' style='overflow:auto'>";
        $var .= "<b>RATEIO:</b>";
        $var .= "<hr>
                <div>
                    <b>CÁLCULO DE RATEIO:</b> <br>
                    <label><input class='tipo-calculo-rateio' type='radio' name='tipo_calculo_rateio' checked value='0' />VALOR LÍQUIDO</label>&nbsp;&nbsp;
                    <label><input class='tipo-calculo-rateio' type='radio' name='tipo_calculo_rateio' value='1'/>VALOR BRUTO</label>    
                </div>";
        $var .= "<button style='position: relative; float: right;' id='add_rateio' > Adicionar Rateio +</button>";
        $var .= "<table class='mytable' id='rateio' width='100%'>";
        /*
        * <th>IPCF2</th>
        <th>IPCF3</th>
        <td class='td_2' name='1' ></td>
        <td class='td_3' name='ipcg3_1' ></td>
        */
          $var .= "<thead>
                        <tr>
                            <th>Unidade Regional</th>
                            <th>Valor</th>
                            <th>Percentual</th>
                            <th>IPCF4</th>
                            <th>Centro de Custo</th>
                        </tr>
                    </thead>
                       <tfoot>
                              <tr>
                                  <th colspan='5'></th>
                              </tr>
                      </tfoot>
                      <tbody>";
                $var .= "<tr class='rateio' >
                                <td name='empresa_1' style='font-size:2px;'>";
                                $var .= empresa($_SESSION['empresa_principal'], 1);
                $var .= "       </td>
                                <td name='1' style='font-size:2px;'><input class='valor OBG valor_rateio' value='0,00' type='text' maxlength='15' style='text-align:right;font-size:10px;' /></td>
                                <td><input type='text' maxlength='15' value='0,00' valor_real='0,00'   class='valor OBG porcento' disabled='disabled' style='text-align:right;font-size:10px;'/>%</td>
                                <td class='td_4_1' name='ipcg4_1'>
                                <img src='/utils/look_16x16.png' class='buscar-naturezas-dialog-rateio' style='cursor: pointer;' /> &nbsp;
                                <input type = 'hidden' class='id-natureza-rateio' />
                               
                                <input type = 'text' class='OBG nome-natureza-rateio' style='font-size:8px;' readonly disabled size='50' /></td>
                                
                                <td class='td_5_1' name='assinatura_1'>".assinatura(0, 1)."</td>
                         </tr>";
            $var .= "</tbody>
                </table>
                ";
$var .= "</div>";


    $var .= "<div class='full' style='text-align: center'>";
        $var .= "<button id='inserir_nota' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'  aria-disabled='false' >
                    <span class='ui-button-text'>Salvar</span>
                </button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <button id='voltar_' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' >
                    <span class='ui-button-text'>Voltar</span>
                </button>
            </div>";

    $var .= "</form></div>";


    return $var;
}



function processar_notas(){
    if (isset($_GET['idNotas']) ) {
        $idNotas = (int) $_GET['idNotas'];
        $sql = "UPDATE `notas` SET  `status` =  'Processada' WHERE `idNotas` = '$idNotas' ";
        mysql_query($sql) or die(mysql_error());
        $var = (mysql_affected_rows()) ? "Edited row.<br />" : "Nothing changed. <br />";
        header('Location:?op=notas&act=listar');
    }
}

function edit_notas() {
    $sql = <<<SQL
SELECT * FROM retencoes_vencimentos
SQL;
    $rs = mysql_query($sql);
    $vencimentos = [];
    while($row = mysql_fetch_array($rs)) {
        $vencimentos[$row['tipo']][$row['retencao']] = $row['vencimento'];
    }
    $vencimentosJson = json_encode($vencimentos);
    $idNotas = (int) $_GET['idNotas'];
    $sql = "UPDATE notas SET visualizado = 1, visualizado_por = '{$_SESSION['id_user']}' WHERE idNotas = '{$idNotas}'";
    $rs = mysql_query($sql);
    $notaref = (int) $_GET['notaref'];
    $empresa = $_SESSION["empresa_user"];
    $sql_editar="Select
                    count(idNotas)
                    from
                    notas
                    where
                    status not in('Em aberto','Cancelada') and
                    (idNotas = $idNotas
                    or
                    NOTA_REFERENCIA =$idNotas) ";
    $result=mysql_query($sql_editar);
    $poder_editar =mysql_result($result,0);

    $sqlBordero = "
    SELECT COUNT(*) AS qtd_parcelas_bordero 
    FROM bordero_parcelas 
    INNER JOIN parcelas ON parcela_id = parcelas.id 
    inner join bordero on (bordero_parcelas.bordero_id = bordero.id)
    WHERE parcelas.idNota = '{$idNotas}'
    and bordero.ativo = 's'
    GROUP BY idNota
    ";
    $qtdBorderoNota = 0;
          $rs = mysql_query($sqlBordero);
          $qtdBorderoNota = current(mysql_fetch_array($rs))['qtd_parcelas_bordero'];


    $sql = "SELECT
           notas.*,
           plano_contas.ID AS conta_id,
           plano_contas.N4 AS conta_nome,
           plano_contas.COD_N4 conta_codigo 
          FROM
          `notas`
          LEFT JOIN plano_contas ON notas.natureza_movimentacao = plano_contas.ID
          WHERE
          
         `idNotas` = '$idNotas' ";
         
    $row = mysql_fetch_array (mysql_query($sql));
    $statusNota = $row['status'];

    $sqlAliquotas = <<<SQL
SELECT 
  recolhe_iss,
  aliquota_iss,
  recolhe_ir,
  aliquota_ir,
  recolhe_ipi,
  aliquota_ipi,
  recolhe_icms,
  aliquota_icms,
  recolhe_pis,
  aliquota_pis,
  recolhe_cofins,
  aliquota_cofins,
  recolhe_csll,
  aliquota_csll,
  recolhe_inss,
  aliquota_inss
FROM 
  fornecedores 
WHERE
  idFornecedores = '{$row['codFornecedor']}'
SQL;
    $rs = mysql_query($sqlAliquotas);
    $rowAliquotas = current(mysql_fetch_array($rs));

    $fechamento_caixa = \App\Controllers\FechamentoCaixa::getFechamentoCaixa();

    $var = "<link rel='stylesheet' type='text/css' href='/utils/reports.css'>";
    $var .="<div id='dialog-editar' title='Salva'>
  <p><span >Conta editada com sucesso!</span>".
        "<div id='text-dialog'></div>
  </p></div>";
    $var .= "<div id='div-nova-nota'> ";
    $var .= alerta();
    $disabledExcluirInput = '';

    if(($poder_editar!=0 && $notaref==0) || $notaref!=0 || $qtdBorderoNota != 0){
        $disabledExcluirInput ="disabled='disabled'";
        $read='readonly';
        $disabled = "disabled='disabled'";
        $onclick = "1";
        $var .="<h1><center>Visualizar Conta</center></h1>";
        if($row['PCC'] >0){
            $checked_pcc = "checked='checked'";
            $disabled_pcc = "disabled='disabled";
        }
        $editarNumeroNota="<a href='#' id='editar-num-nota'class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' 
 aria-disabled='false' ><span class='ui-button-text'>Editar</span></a>";

 $editarCompetencia="<a href='#' id='editar-competencia'class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' 
 aria-disabled='false' ><span class='ui-button-text'>Editar Competência</span></a>";
        $disabled_impostos_pcc = "disabled='disabled'";
    }else{
        $onclick ='0';
        $var .="<h1><center>Editar Conta</center></h1>";
        $editarNumeroNota='';
        $editarCompetencia='';

        $disabled_tipo_nota = $row['tipo'] == '0' ?"disabled='disabled'":"";
        if($row['PCC'] >0){
            $checked_pcc = "checked='checked'";
            $disabled_impostos_pcc = "disabled='disabled'";
            $disabled_pcc = "";
        }else{
            $disabled_pcc = "disabled='disabled";
            $disabled_impostos_pcc='';
        }
    }
 $empresaUsuario =  explode(',', str_replace(')', '', str_replace('(', '', $_SESSION['empresa_user'])));
 $empresaNota = $row['empresa'];

    $var .= "<form>
    <div class='half'>
    <label class='label negrito'>COMPETÊNCIA:</label>
    <input class='OBG mes-ano' type='text' maxlength='10' size='10' name='dataCompetencia' id='data-competencia' value='" .implode("/",array_reverse(explode("-",$row['data_competencia'])))."' /> &nbsp;&nbsp; "; 
    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_pagar_receber']['financeiro_conta_pagar_receber_buscar_conta']['financeiro_conta_pagar_receber_buscar_conta_editar']) || $_SESSION['adm_user'] == 1){
        if(in_array($empresaNota, $empresaUsuario) || $_SESSION['adm_user'] == 1){
            $var .= $editarCompetencia;
        }
     }                       
    $var .= "</div>";
    $var .= "<input id='data-fechamento' type='hidden' value='{$fechamento_caixa['data_fechamento']}'>";
    $var .= "<div class='full'>";
    $var .= "<label class='label negrito'>TIPO:</label>";
    $tipo=$row['tipo'];
    if($row['tipo'] == '0'){
        $var .= "<label><input type='radio' $disabled class='tipo_nota' name='tipo' value='0' CHECKED/>RECEBIMENTOS</label>&nbsp;&nbsp;";
        $var .= "<label><input type='radio'  $disabled  class='tipo_nota' name='tipo' value='1' />DESEMBOLSOS</label>	";

    } else if($row['tipo'] == '1'){
        $var .= "<label><input type='radio' $disabled class='tipo_nota' name='tipo' value='0' />RECEBIMENTOS</label>&nbsp;&nbsp;";
        $var .= "<label><input type='radio' $disabled class='tipo_nota' name='tipo' value='1' CHECKED/>DESEMBOLSOS</label>	";
    }
    $var .= "</div>";

    $data = implode("/",array_reverse(explode("-",$row['dataEntrada'])));
    $var .= "<div class='half'>
                <label class='label negrito'>TIPO DOCUMENTO:</label>
                ".notasTipoDocumento($row['tipo_documento_financeiro_id'],'tipo_documento',false,$disabled, 'OBG_CHOSEN')."&nbsp;&nbsp;&nbsp;
	         </div>
	         <div class='half'>
                 <label class='label negrito'>DATA DE EMISS&Atilde;O:</label>
                 <input class='OBG data-emissao-nota' type='text' maxlength='10' size='10'  $disabled  name='dataEntrada' id='entrada' value='" .implode("/",array_reverse(explode("-",$row['dataEntrada']))). "'/>
                 <input class='data-emissao-nota-original' type='hidden' name='dataEntradaOriginal' id='entrada-original' value='" .implode("/",array_reverse(explode("-",$row['dataEntrada']))). "'/>
                 <input type='hidden' id='vencimentos-json' value='{$vencimentosJson}'>
                 <label class='negrito'> N&deg;:</label>
                 <input class='OBG' type='text' name='num' id='num'  value='{$row['codigo']}'/>";
                 if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_pagar_receber']['financeiro_conta_pagar_receber_buscar_conta']['financeiro_conta_pagar_receber_buscar_conta_editar']) || $_SESSION['adm_user'] == 1){
                    if(in_array($empresaNota, $empresaUsuario) || $_SESSION['adm_user'] == 1){
                        $var .= $editarNumeroNota;
                    }
                 } 
                 $var .="<input type='hidden' name='idnota' $read value='{$idNotas}'/><b>TR</b> {$row['TR']}
              </div>
               ";

              $codUr = $onclick == 1 ? "id = {$row['empresa']}" : "(ATIVO = 'S' AND id IN {$_SESSION['empresa_user']}) or id = {$row['empresa']}";

    $resultUr = mysql_query("SELECT id, nome FROM empresas where $codUr ORDER BY nome ");
    $var .= "<div class='full'>
                <label class='label negrito'>UNIDADE REGIONAL:</label>
                <select id='empresa-nota' name='ur' class='chosen-select OBG_CHOSEN' data-placeholder='Selecione a unidade responsável...' style='background-color:transparent; width: 305px;' >
                <option disabled value=''>Selecione...</option>";
    while ($rowUr = mysql_fetch_array($resultUr)) {
        $var .= "<option {$disabled} " . ($rowUr['id'] == $row['empresa'] ? 'selected' : '') ." value='{$rowUr['id']}'>{$rowUr['nome']}</option>";
    }
    $var .= "</select></div>";

    $var .= "<div class='full'>
                <label class='label negrito'>FORMA PAGAMENTO:</label>
            <select id='forma-pagamento' name='forma_pagamento' class='chosen-select OBG_CHOSEN' data-placeholder='Selecione a forma de pagamenteo...' style='background-color:transparent; width: 305px;' >";
            $var .= aplicacaoFundosOption(implode(',', [1, 7, 5, 6, 9, 10]), $row['forma_pagamento']);    
            // <option selected disabled value=''>Selecione...</option>";     
            // $var .= "<option  ".   ($row['forma_pagamento'] == 'Boleto' ? 'selected' : '') . "  value='Boleto'>Boleto</option>";
            // $var .= "<option  ".  ($row['forma_pagamento'] == 'Cartão de Crédito' ? 'selected' : '') ."  value='Cartão de Crédito'>Cartão de Crédito</option>";
            // $var .= "<option  ".  ($row['forma_pagamento'] == 'Transferência' ? 'selected' : '') ." value='Transferência'>Transferência</option>";
        
    $var .= "</select>
            </div>";

    $contaCodigo = str_replace('.', '', $row['conta_codigo']);
    $var .= "<div class='full' style='word-wrap: break-word'>
                <label class='label negrito'>NATUREZA:</label>
                <input type='number' $disabled name='codigo_conta' id='codigo-conta' value='{$contaCodigo}' class='OBG' min='1' step='1' max='99999'>
                <input type='hidden' name='natureza_movimentacao' id='natureza-movimentacao' value='{$row['conta_id']}' class='OBG'> &nbsp;";
    if(($poder_editar == 0 && $notaref != 0) || $notaref == 0) {
        $var .= "   <img src='/utils/look_16x16.png' id='buscar-naturezas-dialog' style='cursor: pointer;' /> &nbsp;";
    }
    $var .= "   <span class='nome-natureza negrito' style='color: red'>{$row['conta_codigo']} - {$row['conta_nome']}</span>
             </div>";

    $sql = <<<SQL
SELECT 
  ID,
  N1,
  COD_N4, 
  N4, 
  POSSUI_RETENCOES,
  ISS,
  IR,
  PIS,
  COFINS,
  CSLL,
  INSS
FROM 
  plano_contas
ORDER BY 
  CONCAT(COD_N4, ' - ', N4) ASC
SQL;
    $rs = mysql_query($sql);
    $naturezas = [];
    while ($linha = mysql_fetch_array($rs, MYSQL_ASSOC)) {
        $naturezas[] = $linha;
        $naturezaId[$linha['ID']] = $linha['COD_N4'].' - '.$linha['N4'];
    }

    $var .= "<div class='full'>
                <div id='dialog-listar-naturezas' title='Lista de Naturezas' style='display: none; min-height: 400px; overflow: hidden;'>
                    <div>
                        <input type='text' id='filtrar-natureza' style='text-transform: uppercase;' size='70' maxlength='50'>
                    </div>
                    <br>
                    <div style='height: 300px; overflow: auto;'>
                        <table class='mytable' id='table-lista-naturezas' width='100%' style='cursor:pointer;'>
                          <tbody>";
    foreach ($naturezas as $natureza) {
        $json_natureza = json_encode($natureza);
        $contaCodigo = str_replace('.', '', $natureza['COD_N4']);
        $var .= "<tr tipo='{$natureza['N1']}' class='escolher-natureza' json-natureza='{$json_natureza}'>
                    
                    <td class='desc-natureza'>{$contaCodigo} - ({$natureza['COD_N4']} - {$natureza['N4']})</td>
                 </tr>";
    }
    $var .= "            </tbody>
                        </table>
                    </div>
                </div
             </div>";
    $var .= "<div class='full' style='word-wrap: break-word'>
              <label class='label negrito'>FORNECEDOR / CLIENTE: </label>
              " . fornecedores($row['codFornecedor'],'fornecedor',false, $disabled, 'OBG_CHOSEN', ($row['tipo'] == 1 ? 'F' : 'C'))."
             </div>";
    $var .= "<div class='full'>
                <label class='label negrito'>DESCRI&Ccedil;&Atilde;O:</label>
                <textarea name='descricao' $disabled id='descricao' style='text-transform: uppercase;' cols='80' rows='3'>{$row['descricao']}</textarea>
	         </div>";

    $formatNumber = function ($num) {
        return number_format($num, 2, ',', '.');
    };

    $aliquotaIss = ($row['ISSQN'] / $row['VAL_PRODUTOS']) * 100;
    $aliquotaIr = ($row['IR'] / $row['VAL_PRODUTOS']) * 100;
    $aliquotaIpi = ($row['IPI'] / $row['VAL_PRODUTOS']) * 100;
    $aliquotaIcms = ($row['ICMS'] / $row['VAL_PRODUTOS']) * 100;
    $aliquotaPis = ($row['PIS'] / $row['VAL_PRODUTOS']) * 100;
    $aliquotaCofins = ($row['COFINS'] / $row['VAL_PRODUTOS']) * 100;
    $aliquotaCsll = ($row['CSLL'] / $row['VAL_PRODUTOS']) * 100;
    $aliquotaInss = ($row['INSS'] / $row['VAL_PRODUTOS']) * 100;
    $aliquotaPcc = ($row['PCC'] / $row['VAL_PRODUTOS']) * 100;

    $var .="<div class='full'>
            <table class='mytable'
                   width='100%' 
                   style='position: relative; 
                          float: left; 
                          margin: 0 auto;'>
                <thead>
                    <tr>
                    <th colspan='5' align='center'>RETENÇÕES</th>
                    </tr>
                </thead>";
    ////////////novo
    $var .="<tr id='impostos'>
                    <td><b>VAL. PRODUTO/SERV.</b></td>
                    <td><b>ICMS</b> <b id='icms-texto-aliquota'>({$formatNumber($aliquotaIcms)}%)</b></td>
                    <td><b>FRETE</b></td>
                    <td><b>SEGURO</b></td>
               </tr>";
    $var .="<tr id='impostos_valores'>
                    <td>R$ <input id='val_produtos' $read name='val_produtos' maxlength='15' class='valor OBG calcular_val_nota' style='text-align:right' value='".number_format($row['VAL_PRODUTOS'],2,',','.')."' /></td>
                    <td>R$ <input id='icms' aliquota='{$aliquotaIcms}' $read name='icms' $disabled_tipo_nota maxlength='15' class='valor calcular_val_nota' style='text-align:right' value='".number_format($row['ICMS'],2,',','.')."' /></td>
                   <td>R$ <input id='frete'  $read name='frete' $disabled_tipo_nota  style='text-align:right' maxlength='15' class='valor calcular_val_nota' value='".number_format($row['FRETE'],2,',','.')."'/></td>
                   <td>R$ <input  id='seguro' $read name='seguro' $disabled_tipo_nota  style='text-align:right' maxlength='15' class='valor calcular_val_nota' value='".number_format($row['SEGURO'],2,',','.')."'/></td>
              </tr>";
    $var .="<tr id='impostos1'>
                    <td><b>IPI</b> <b id='ipi-texto-aliquota'>({$formatNumber($aliquotaIpi)}%)</b></td>
                    <td><b>ISSQN</b> <b id='iss-texto-aliquota'>({$formatNumber($aliquotaIss)}%)</b></td>
                    <td><b>DESPESAS ACESS.</b></td>
                    <td><b>DESCONTO/BONIF.</b></td>
                </tr>";
    $var .="<tr id='impostos_valores1'>
                        <td>R$ <input id='ipi' aliquota='{$aliquotaIpi}' $read $disabled_tipo_nota maxlength='15' class='valor calcular_val_nota' name='ipi' style='text-align:right' value='".number_format($row['IPI'],2,',','.')."' /></td>
                        <td>R$ <input id='issqn' aliquota='{$aliquotaIss}' name='issqn' $read  style='text-align:right' maxlength='15' class='valor calcular_val_nota' value='".number_format($row['ISSQN'],2,',','.')."' /></td>
                        <td>R$ <input id='des_acessorias' $read maxlength='15' class='valor calcular_val_nota' name='des_acessorias' style='text-align:right' value='".number_format($row['DES_ACESSORIAS'],2,',','.')."' /></td>
                        <td>R$ <input id='des_bon' name='des_bon' $read style='text-align:right'  maxlength='15' class='valor calcular_val_nota' value='".number_format($row['DESCONTO_BONIFICACAO'],2,',','.')."' /></td>
                </tr>";


    ///nova

    $var .="<tr id=''>
                     <td><b>PIS</b> <b id='pis-texto-aliquota'>({$formatNumber($aliquotaPis)}%)</b></td>
                     <td><b>COFINS</b> <b id='cofins-texto-aliquota'>({$formatNumber($aliquotaCofins)}%)</b></td>
                     <td><b>CSLL</b> <b id='csll-texto-aliquota'>({$formatNumber($aliquotaCsll)}%)</b></td>
                     <td><b>IR</b> <b id='ir-texto-aliquota'>({$formatNumber($aliquotaIr)}%)</b></td>
                </tr>";
    $var .="<tr id=''>
                        <td>R$ <input id='pis' aliquota='{$aliquotaPis}' $read  $disabled_impostos_pcc maxlength='15' class='valor calcular_val_nota' name='pis' style='text-align:right' value='".number_format($row['PIS'],2,',','.')."' /></td>
                        <td>R$ <input id='cofins' aliquota='{$aliquotaCofins}' $read $disabled_impostos_pcc name='cofins'  style='text-align:right' maxlength='15'class='valor calcular_val_nota' value='".number_format($row['COFINS'],2,',','.')."' /></td>
                        <td>R$ <input id='csll' aliquota='{$aliquotaCsll}' $read  $disabled_impostos_pcc maxlength='15' class='valor calcular_val_nota' name='csll' style='text-align:right' value='".number_format($row['CSLL'],2,',','.')."' /></td>
                        <td>R$ <input id='ir' aliquota='{$aliquotaIr}' name='ir' $read style='text-align:right'  maxlength='15' class='valor calcular_val_nota' value='".number_format($row['IR'],2,',','.')."' /></td>
                </tr>";
    $var .="<tr id=''>
                     <td collspan='2'><input type='checkbox' name='check_pcc' $disabled $checked_pcc id='check_pcc'/>
                         <label for='check_pcc'><b>PCC</b></label>
                     </td>
                     <td><b>INSS</b> <b id='inss-texto-aliquota'>({$formatNumber($aliquotaInss)}%)</b></td>
                     <td colspan='2'><b>VALOR TOTAL</b></td>
                </tr>";
    $var .="<tr id=''>
                        <td>R$ <input id='pcc' $read name='pcc' $disabled_pcc style='text-align:right' maxlength='15'class='valor calcular_val_nota' value='".number_format($row['PCC'],2,',','.')."' /></td>
                        <td>R$ <input id='inss' $read maxlength='15'class='valor calcular_val_nota' name='inss' style='text-align:right'  value='".number_format($row['INSS'],2,',','.')."' /></td>
                        <td>R$ <input type='text' class='valor' maxlength='15' id='valor_nota' name='valor_nota'  value='".number_format($row['valor'],2,',','.')."' readonly style='text-align:right'/></td>
                        <td align='center'>
                            <button type='button' $disabled id='calcular-retencoes' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'>
                                <span class='ui-button-text'>Calcular Retenções</span> 
                            </button>
                        </td>
                  </tr>";

    $var .= "<tfoot>
                <tr>
                    <th colspan='4'></th>
                </tr>
             </tfoot>";

    $var .="</table></div><div class='full'></div>";

    $var .= "<div class='full' id='div_parcelas'>";
    $var .= "<b>PARCELAS:</b>";
    $var .= '<script type="text/javascript" src="/utils/bower_components/moment/moment.js"></script>';
    $var .= '<script type="text/javascript" src="/utils/jquery/js/validar_vencimento_real.js?v=1.4"></script>';
    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_pagar_receber']['financeiro_conta_pagar_receber_buscar_conta']['financeiro_conta_pagar_receber_buscar_conta_editar']) || $_SESSION['adm_user'] == 1){
        if(in_array($empresaNota, $empresaUsuario) || $_SESSION['adm_user'] == 1){
            if(isset($disabled) && !empty($disabled) && ($statusNota == 'Pendente' || $statusNota == 'Em aberto' )) {
                // $var .= " <button style='margin-left: 375px;' id='editar-vencimento-parcela' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' type='button' aria-disabled='false' >
                //                 <span class='ui-button-text'>Editar vencimentos</span>
                //             </button>";
                    $var .= " <button style='margin-left: 375px;' id='editar-parcela' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' type='button' aria-disabled='false' >
                        <span class='ui-button-text'>Editar Parcela</span>
                    </button>";
            }
        }
     }
    
    if (empty($disabled)) {
        $var .= "<p>
				Gerar várias parcelas: <br>
				Qtd.: <input type='number' name='qtd_parcelas_gerar' class='qtd-parcelas-gerar' size='3' maxlength='3'  value='1'>
				Vencimento: <input type='text' name='vencimento_parcelas_gerar' class='vencimento-parcelas-gerar mask-data' size='10' maxlength='10' value='" . date('d/m/Y') . "'>
				Valor: <input type='text' name='valor_parcelas_gerar' maxlength='15' class='valor-parcelas-gerar valor'  value='0,00'>
				<button type='button' class='gerar-varias-parcelas' from='edit-notas'>+</button>
				<br>
				<small>*O vencimento será gerado automaticamente baseada na primeira data.</small>
             </p>";
    }
$var .= "<div style='overflow:auto;'>
            <div id='mensagem-erro-parcelas'></div>
            <table class='mytable' id='parcelas'>";
    $var .= "<thead>
            <tr>
                <th><input type='checkbox' $disabled class='selecionar-todos'></th>
                <th>TR PARCELA</th>
                <th>VENCIMENTO</th>
                <th>VENCIMENTO REAL</th>
                <th>VALOR</th>
                <th>CÓDIGO BARRA</th>
             </tr>
          </thead>
          <tfoot><tr><th colspan='6'></th></tr></tfoot><tbody>";
    $sql = "SELECT * FROM `parcelas` WHERE  `idNota` = '$idNotas' and status <> 'Cancelada'";
    $result=mysql_query($sql);
    $varInputParcelasPendentes = '';
    WHILE( $row = mysql_fetch_array ($result)){
        $sql = "
    SELECT COUNT(*) AS qtd_parcelas_bordero 
    FROM bordero_parcelas 
    INNER JOIN parcelas ON parcela_id = parcelas.id 
    inner join bordero on (bordero_parcelas.bordero_id = bordero.id)
    WHERE parcelas.id = '{$row['id']}'
    and bordero.ativo = 's'
    GROUP BY idNota
    ";
    $qtdBordero = 0;
          $rs = mysql_query($sql);
          $qtdBordero = current(mysql_fetch_array($rs))['qtd_parcelas_bordero'];
        
        if($row['status'] == 'Pendente' && $qtdBordero == 0){
            $varInputParcelasPendentes .= "<input type='hidden' class ='parcelas-pendentes-editar' parcela-id = '{$row['id']}'  name='parcelaPendente[{$row['id']}]' value='{$row['id']}' />";

        }
        if($qtdBordero >0){
            $disabledBordero = "disabled";
            $readBordero = "readonly";
            $del='';
        }

        if($onclick == 0 && $qtdBordero == 0 ){
            $del= "<img align='left' class='del_parcela'   src='../utils/delete_16x16.png' title='Remover' border='0'/>";

        }
        $var .= "<tr class='parcela editar_parcela' status='{$row['status']}' emBordero = '{$qtdBordero}' data-editar-parcela='0' data-id-parcela='{$row['id']}' >
                 <td>";
                 if($qtdBordero == 0){
                    

                    $var .= "<input type='checkbox' $disabled class='selecionar-parcela-exclusao'>";
                 }
            $var .= "</td>
            <td>{$del}<input type='text' class='form_parcelas' name='tr_parcela' size='5' $read value='{$row['TR']}'/></td>
            <td>
            	<input data-id-parcela='{$row['id']}' value='".implode("/",array_reverse(explode("-",$row['vencimento']))). "' type='text' maxlength='10' size='10' $disabled $cond $disabledBordero class='mask-data OBG form_parcelas vencimento-parcela-editar vencimento'/>
            	<input type='hidden' class='form_parcelas' maxlength='128' size='20' name='codBarras' value='{$row['codBarras']}' $cond $read />
            </td>
            <td>
            	<input status='{$row['status']}' data-id-parcela='{$row['id']}' $disabledBordero value='".implode("/",array_reverse(explode("-",$row['vencimento_real']))). "' type='text' maxlength='10' size='10' $disabled $cond class='mask-data OBG form_parcelas vencimento-real-parcela-editar vencimento'/>
            	
            </td>
            <td><input class='valor OBG valor_parcela form_parcelas' maxlength='15' $disabledBordero value='".number_format($row['valor'],2,',','.')."'  $cond $read type='text'  style='text-align:right' /></td>
            <td>
            	<input data-id-parcela='{$row['id']}' value='".$row['codBarras']. "' type='text' size='70' $disabled $cond class='codigo-barra form_parcelas'/>
            	
            </td>
            </tr>";
        }
    $var .= "</tbody>
        </table>
        <div><form id='form-parcela-pendentes'>".$varInputParcelasPendentes."</form></div>
    </div>";
    if (empty($disabled)) {
        $var .= "<button id='add_parcela'  >Adicionar Parcela +</button> 
                 <button type='button'  id='excluir-parcelas-selecionadas' >Excluir parcelas selecionadas</button>";
    }
    $var .= "</div>";

    $var .= "<div class='full' style='text-align: center;'>";
    $var .= "<button type='button' id='calcular-vencimentos' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'>
                <span class='ui-button-text'>Calcular Vencimentos</span> 
            </button>";
    $var .= "</div>";
    $var .= "<div class='full' style='overflow: auto;'>";
    $var .= impostos_retidos($idNotas,$poder_editar,$notaref);
    $var .= "</div>";
    $var .= "<br><br>";

    //if($tipo == '1') {
    
    $var .= "<div class='full' id='div_rateio'>";
    $var .= "<div><b>RATEIO:</b>";
    if(empty($disabled)){
    $var .= "<hr>
    <div>
        <b>CÁLCULO DE RATEIO:</b> <br>
        <label><input class='tipo-calculo-rateio' type='radio' name='tipo_calculo_rateio' checked value='0' />VALOR LÍQUIDO</label>&nbsp;&nbsp;
        <label><input class='tipo-calculo-rateio' type='radio' name='tipo_calculo_rateio' value='1'/>VALOR BRUTO</label>    
    </div>";
}
    $var .= "<button $disabled style='position: relative; float: right;' id='add_rateio' > Adicionar Rateio +</button></div>";
    
    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_pagar_receber']['financeiro_conta_pagar_receber_buscar_conta']['financeiro_conta_pagar_receber_buscar_conta_editar']) || $_SESSION['adm_user'] == 1){
        if(in_array($empresaNota, $empresaUsuario) || $_SESSION['adm_user'] == 1){
            if(isset($disabled) && !empty($disabled)){
                // $var .= "<div style='position: relative; float:right;'>
                //             <button id='editar-rateio'
                //                 class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only
                //                 ui-state-hover' role='button' type='button' aria-disabled='false' >
                //                 <span class='ui-button-text'>Editar rateio</span>
                //             </button>
                //          </div>";
            }            
        }
     }   
    
    $var .= "<table class='mytable full' id='rateio' width='100%' editar='0' idNota='$idNotas' >";
    $var .= "<thead>
                  <tr>
                      <th>Unidade Regional</th>
                      <th>Valor</th>
                      <th>Percentual</th>
                      <th>IPCF4</th>
                      <th>Centro de Custo</th>
                  </tr>
           </thead>
           <tfoot>
                <tr>
                    <th colspan='7'></th>
                 </tr>
           </tfoot>
           <tbody>";
    $sql = "SELECT r.*, n.tipo 
            FROM `rateio`as r 
            inner join  notas as n 
            on (r.NOTA_ID = n.idNotas) 
            WHERE  `NOTA_ID` = '$idNotas' ";
    $result=mysql_query($sql);
    $cont=1;
    WHILE( $row = mysql_fetch_array ($result)){

        $var .= "<tr class='rateio' >
  <td name='empresa_1'>".empresa($row['CENTRO_RESULTADO'],1,$onclick,$disabled)."</td>
  <td name='1'><input maxlength='15' class='valor OBG valor_rateio' $disabled value='".number_format($row['VALOR'],2,',','.')."' type='text'  style='text-align:right;font-size:10px;' /></td>
  <td><input type='text' maxlength='10' $disabled valor_real='{$row['PORCENTAGEM']}' value='".number_format($row['PORCENTAGEM'],2,',','.')."' size='5' disabled='disabled' class='valor OBG porcento' style='text-align:right;font-size:10px;'/>%</td>
  <td class='td_4_1' name='ipcg4_$cont'>";
  if(($poder_editar == 0 && $notaref != 0) || $notaref == 0) {
    $var .= "  <img src='/utils/look_16x16.png' class='buscar-naturezas-dialog-rateio' style='cursor: pointer;' /> &nbsp;";
}
  
$var .= "  <input type = 'hidden' class='id-natureza-rateio' value='".$row['IPCG4']."' />
            <input type = 'text' class='OBG nome-natureza-rateio' style='font-size:8px;' readonly disabled size='50' value= '". $naturezaId[$row['IPCG4']]."' />
            </td>
                                
  
  <td class='td_5_1' name='assinatura_$cont'>".assinatura($row['ASSINATURA'],1,$disabled)."</td>
  </tr>";
        $cont++;
    }
    $var .= "</tbody></table>";
    $var .= "</div>";
    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_pagar_receber']['financeiro_conta_pagar_receber_buscar_conta']['financeiro_conta_pagar_receber_buscar_conta_editar']) || $_SESSION['adm_user'] == 1){
        if(in_array($empresaNota, $empresaUsuario) || $_SESSION['adm_user'] == 1){
            if($notaref==0 && $poder_editar==0 &&  $qtdBorderoNota == 0){
                $var .= "<div class='full' style='text-align: center;'>
                            <button id='inserir_nota_edt' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'  aria-disabled='false' >
                            <span class='ui-button-text'>Atualizar</span>
                            </button>
                         </div>";
            }                      
        }
     }  
   
    $var .= "</form></div></div>";


    return $var;
}

function usar_nova_notas() {
    $sql = <<<SQL
SELECT * FROM retencoes_vencimentos
SQL;
    $rs = mysql_query($sql);
    $vencimentos = [];
    while($row = mysql_fetch_array($rs)) {
        $vencimentos[$row['tipo']][$row['retencao']] = $row['vencimento'];
    }
    $vencimentosJson = json_encode($vencimentos);
    $idNotas = (int) $_GET['idNotas'];
    $sql = "UPDATE notas SET visualizado = 1, visualizado_por = '{$_SESSION['id_user']}' WHERE idNotas = '{$idNotas}'";
    $rs = mysql_query($sql);
    $notaref = (int) $_GET['notaref'];
    $empresa = $_SESSION["empresa_user"];
    $sql_editar="Select
                    count(idNotas)
                    from
                    notas
                    where
                    status not in('Em aberto','Cancelada') and
                    (idNotas = $idNotas
                    or
                    NOTA_REFERENCIA =$idNotas) ";
    $result=mysql_query($sql_editar);
    $poder_editar =mysql_result($result,0);


    $sql = "SELECT
           notas.*,
           plano_contas.ID AS conta_id,
           plano_contas.N4 AS conta_nome,
           plano_contas.COD_N4 conta_codigo 
          FROM
          `notas`
          LEFT JOIN plano_contas ON notas.natureza_movimentacao = plano_contas.ID
          WHERE
          
         `idNotas` = '$idNotas' ";

    $row = mysql_fetch_array (mysql_query($sql));
    $statusNota = $row['status'];

    $sqlAliquotas = <<<SQL
SELECT 
  recolhe_iss,
  aliquota_iss,
  recolhe_ir,
  aliquota_ir,
  recolhe_ipi,
  aliquota_ipi,
  recolhe_icms,
  aliquota_icms,
  recolhe_pis,
  aliquota_pis,
  recolhe_cofins,
  aliquota_cofins,
  recolhe_csll,
  aliquota_csll,
  recolhe_inss,
  aliquota_inss
FROM 
  fornecedores 
WHERE
  idFornecedores = '{$row['codFornecedor']}'
SQL;
    $rs = mysql_query($sqlAliquotas);
    $rowAliquotas = current(mysql_fetch_array($rs));

    $fechamento_caixa = \App\Controllers\FechamentoCaixa::getFechamentoCaixa();

    $var = "<link rel='stylesheet' type='text/css' href='/utils/reports.css'>";
    $var = "<script type='text/javascript' src='auto-save.js?v=1'></script>";
    $var .="<div id='dialog-message' title='Nota salva'>
                <p>
                    <span id='trGerada'></span>".
        " <div id='text-dialog'></div>
                </p>
            </div>";
    $var .= "<div id='div-nova-nota'> ";
    $var .= alerta();
    $disabledExcluirInput = '';
    $var .="<h1><center>Prorrogar Nota</center></h1>";

    if(($poder_editar!=0 && $notaref==0) || $notaref!=0){
        $disabledExcluirInput ="disabled='disabled'";
        $read='readonly';
        $disabled = "disabled='disabled'";
        $onclick = "1";
        if($row['PCC'] >0){
            $checked_pcc = "checked='checked'";
            $disabled_pcc = "disabled='disabled";
        }

        $disabled_impostos_pcc = "disabled='disabled'";
    }else{
        $onclick ='0';
        $editarNumeroNota='';
        $disabled_tipo_nota = $row['tipo'] == '0' ?"disabled='disabled'":"";
        if($row['PCC'] >0){
            $checked_pcc = "checked='checked'";
            $disabled_impostos_pcc = "disabled='disabled'";
            $disabled_pcc = "";
        }else{
            $disabled_pcc = "disabled='disabled";
            $disabled_impostos_pcc='';
        }
    }
    $empresaUsuario =  explode(',', str_replace(')', '', str_replace('(', '', $_SESSION['empresa_user'])));
    $empresaNota = $row['empresa'];

    $var .= "<form>";
    $var .= "<input id='data-fechamento' type='hidden' value='{$fechamento_caixa['data_fechamento']}'>";
    $var .= "<div class='full'>";
    $var .= "<label class='label negrito'>TIPO:</label>";
    $tipo=$row['tipo'];
    if($row['tipo'] == '0'){
        $var .= "<label><input type='radio' class='tipo_nota' name='tipo' value='0' CHECKED/>RECEBIMENTOS</label>&nbsp;&nbsp;";
        $var .= "<label><input type='radio' class='tipo_nota' name='tipo' value='1' />DESEMBOLSOS</label>	";

    } else if($row['tipo'] == '1'){
        $var .= "<label><input type='radio' class='tipo_nota' name='tipo' value='0' />RECEBIMENTOS</label>&nbsp;&nbsp;";
        $var .= "<label><input type='radio' class='tipo_nota' name='tipo' value='1' CHECKED/>DESEMBOLSOS</label>	";
    }
    $var .= "</div>";

    $data = implode("/",array_reverse(explode("-",$row['dataEntrada'])));
    $hoje = (new \DateTime())->format('d/m/Y');
    $var .= "<div class='half'>
                <label class='label negrito'>TIPO DOCUMENTO:</label>
                ".notasTipoDocumento(25,'tipo_documento',false, $disabled, 'OBG_CHOSEN')."&nbsp;&nbsp;&nbsp;
	         </div>
	         <div class='half'>
                 <label class='label negrito'>DATA DE EMISS&Atilde;O:</label>
                 <input class='OBG data-emissao-nota' 
                        type='text' maxlength='10' size='10' name='dataEntrada' id='entrada' value='{$hoje}'/>
                 <input type='hidden' id='vencimentos-json' value='{$vencimentosJson}'>
                 <label class='negrito'> N&deg;:</label>
                 <input class='' type='text' name='num' id='num'  value=''/>";
        $var .="<input type='hidden' name='idnota' $read value='{$idNotas}'/>
              </div>";

    $codUr = $onclick == 1 ? "id = {$row['empresa']}" : "(ATIVO = 'S' AND id IN {$_SESSION['empresa_user']}) or id = {$row['empresa']}";

    $resultUr = mysql_query("SELECT id, nome FROM empresas where $codUr ORDER BY nome ");
    $var .= "<div class='full'>
                <label class='label negrito'>UNIDADE REGIONAL:</label>
                <select id='empresa-nota' name='ur' class='chosen-select OBG_CHOSEN' data-placeholder='Selecione a unidade responsável...' style='background-color:transparent; width: 305px;' >
                <option disabled value=''>Selecione...</option>";
    while ($rowUr = mysql_fetch_array($resultUr)) {
        $var .= "<option {$disabled} " . ($rowUr['id'] == $row['empresa'] ? 'selected' : '') ." value='{$rowUr['id']}'>{$rowUr['nome']}</option>";
    }
    $var .= "</select></div>";

    $contaCodigo = str_replace('.', '', $row['conta_codigo']);
    $var .= "<div class='full' style='word-wrap: break-word'>
                <label class='label negrito'>NATUREZA:</label>
                <input type='number' name='codigo_conta' id='codigo-conta' value='{$contaCodigo}' class='OBG' min='1' step='1' max='99999'>
                <input type='hidden' name='natureza_movimentacao' id='natureza-movimentacao' value='{$row['conta_id']}' class='OBG'> &nbsp;";
    if(($poder_editar == 0 && $notaref != 0) || $notaref == 0) {
        $var .= "   <img src='/utils/look_16x16.png' id='buscar-naturezas-dialog' style='cursor: pointer;' /> &nbsp;";
    }
    $var .= "   <span class='nome-natureza negrito' style='color: red'>{$row['conta_codigo']} - {$row['conta_nome']}</span>
             </div>";

    $sql = <<<SQL
SELECT 
  ID,
  N1,
  COD_N4, 
  N4, 
  POSSUI_RETENCOES,
  ISS,
  IR,
  PIS,
  COFINS,
  CSLL,
  INSS
FROM 
  plano_contas
ORDER BY 
  CONCAT(COD_N4, ' - ', N4) ASC
SQL;
    $rs = mysql_query($sql);
    $naturezas = [];
    while ($linha = mysql_fetch_array($rs, MYSQL_ASSOC)) {
        $naturezas[] = $linha;
        $naturezaId[$linha['ID']] = $linha['COD_N4'].' - '.$linha['N4'];
    }

    $var .= "<div class='full'>
                <div id='dialog-listar-naturezas' title='Lista de Naturezas' style='display: none; min-height: 400px; overflow: hidden;'>
                    <div>
                        <input type='text' id='filtrar-natureza' style='text-transform: uppercase;' size='70' maxlength='50'>
                    </div>
                    <br>
                    <div style='height: 300px; overflow: auto;'>
                        <table class='mytable' id='table-lista-naturezas' width='100%' style='cursor:pointer;'>
                          <tbody>";
    foreach ($naturezas as $natureza) {
        $json_natureza = json_encode($natureza);
        $contaCodigo = str_replace('.', '', $natureza['COD_N4']);
        $var .= "<tr tipo='{$natureza['N1']}' class='escolher-natureza' json-natureza='{$json_natureza}'>
                    
                    <td class='desc-natureza'>{$contaCodigo} - ({$natureza['COD_N4']} - {$natureza['N4']})</td>
                 </tr>";
    }
    $var .= "            </tbody>
                        </table>
                    </div>
                </div
             </div>";
    $var .= "<div class='full' style='word-wrap: break-word'>
              <label class='label negrito'>FORNECEDOR / CLIENTE: </label>
              " . fornecedores($row['codFornecedor'],'fornecedor',false, null, 'OBG_CHOSEN', ($row['tipo'] == 1 ? 'F' : 'C'))."
             </div>";
    $var .= "<div class='full'>
                <label class='label negrito'>DESCRI&Ccedil;&Atilde;O:</label>
                <textarea name='descricao' id='descricao' cols='80' rows='3'>NOTA DO TIPO PREVISÃO</textarea>
	         </div>";

    $formatNumber = function ($num) {
        return number_format($num, 2, ',', '.');
    };

    $aliquotaIss = ($row['ISSQN'] / $row['VAL_PRODUTOS']) * 100;
    $aliquotaIr = ($row['IR'] / $row['VAL_PRODUTOS']) * 100;
    $aliquotaIpi = ($row['IPI'] / $row['VAL_PRODUTOS']) * 100;
    $aliquotaIcms = ($row['ICMS'] / $row['VAL_PRODUTOS']) * 100;
    $aliquotaPis = ($row['PIS'] / $row['VAL_PRODUTOS']) * 100;
    $aliquotaCofins = ($row['COFINS'] / $row['VAL_PRODUTOS']) * 100;
    $aliquotaCsll = ($row['CSLL'] / $row['VAL_PRODUTOS']) * 100;
    $aliquotaInss = ($row['INSS'] / $row['VAL_PRODUTOS']) * 100;
    $aliquotaPcc = ($row['PCC'] / $row['VAL_PRODUTOS']) * 100;

    $var .="<div class='full'>
            <table class='mytable'
                   width='100%' 
                   style='position: relative; 
                          float: left; 
                          margin: 0 auto;'>
                <thead>
                    <tr>
                    <th colspan='5' align='center'>RETENÇÕES</th>
                    </tr>
                </thead>";
    ////////////novo
    $var .="<tr id='impostos'>
                    <td><b>VAL. PRODUTO/SERV.</b></td>
                    <td><b>ICMS</b> <b id='icms-texto-aliquota'>({$formatNumber($aliquotaIcms)}%)</b></td>
                    <td><b>FRETE</b></td>
                    <td><b>SEGURO</b></td>
               </tr>";
    $var .="<tr id='impostos_valores'>
                    <td>R$ <input id='val_produtos' $read name='val_produtos' maxlength='15' class='valor OBG calcular_val_nota' style='text-align:right' value='".number_format($row['VAL_PRODUTOS'],2,',','.')."' /></td>
                    <td>R$ <input id='icms' aliquota='{$aliquotaIcms}' $read name='icms' $disabled_tipo_nota maxlength='15' class='valor calcular_val_nota' style='text-align:right' value='".number_format($row['ICMS'],2,',','.')."' /></td>
                   <td>R$ <input id='frete'  $read name='frete' $disabled_tipo_nota  style='text-align:right' maxlength='15' class='valor calcular_val_nota' value='".number_format($row['FRETE'],2,',','.')."'/></td>
                   <td>R$ <input  id='seguro' $read name='seguro' $disabled_tipo_nota  style='text-align:right' maxlength='15' class='valor calcular_val_nota' value='".number_format($row['SEGURO'],2,',','.')."'/></td>
              </tr>";
    $var .="<tr id='impostos1'>
                    <td><b>IPI</b> <b id='ipi-texto-aliquota'>({$formatNumber($aliquotaIpi)}%)</b></td>
                    <td><b>ISSQN</b> <b id='iss-texto-aliquota'>({$formatNumber($aliquotaIss)}%)</b></td>
                    <td><b>DESPESAS ACESS.</b></td>
                    <td><b>DESCONTO/BONIF.</b></td>
                </tr>";
    $var .="<tr id='impostos_valores1'>
                        <td>R$ <input id='ipi' aliquota='{$aliquotaIpi}' $read $disabled_tipo_nota maxlength='15' class='valor calcular_val_nota' name='ipi' style='text-align:right' value='".number_format($row['IPI'],2,',','.')."' /></td>
                        <td>R$ <input id='issqn' aliquota='{$aliquotaIss}' name='issqn' $read  style='text-align:right' maxlength='15' class='valor calcular_val_nota' value='".number_format($row['ISSQN'],2,',','.')."' /></td>
                        <td>R$ <input id='des_acessorias' $read maxlength='15' class='valor calcular_val_nota' name='des_acessorias' style='text-align:right' value='".number_format($row['DES_ACESSORIAS'],2,',','.')."' /></td>
                        <td>R$ <input id='des_bon' name='des_bon' $read style='text-align:right'  maxlength='15' class='valor calcular_val_nota' value='".number_format($row['DESCONTO_BONIFICACAO'],2,',','.')."' /></td>
                </tr>";


    ///nova

    $var .="<tr id=''>
                     <td><b>PIS</b> <b id='pis-texto-aliquota'>({$formatNumber($aliquotaPis)}%)</b></td>
                     <td><b>COFINS</b> <b id='cofins-texto-aliquota'>({$formatNumber($aliquotaCofins)}%)</b></td>
                     <td><b>CSLL</b> <b id='csll-texto-aliquota'>({$formatNumber($aliquotaCsll)}%)</b></td>
                     <td><b>IR</b> <b id='ir-texto-aliquota'>({$formatNumber($aliquotaIr)}%)</b></td>
                </tr>";
    $var .="<tr id=''>
                        <td>R$ <input id='pis' aliquota='{$aliquotaPis}' $read  $disabled_impostos_pcc maxlength='15' class='valor calcular_val_nota' name='pis' style='text-align:right' value='".number_format($row['PIS'],2,',','.')."' /></td>
                        <td>R$ <input id='cofins' aliquota='{$aliquotaCofins}' $read $disabled_impostos_pcc name='cofins'  style='text-align:right' maxlength='15'class='valor calcular_val_nota' value='".number_format($row['COFINS'],2,',','.')."' /></td>
                        <td>R$ <input id='csll' aliquota='{$aliquotaCsll}' $read  $disabled_impostos_pcc maxlength='15' class='valor calcular_val_nota' name='csll' style='text-align:right' value='".number_format($row['CSLL'],2,',','.')."' /></td>
                        <td>R$ <input id='ir' aliquota='{$aliquotaIr}' name='ir' $read style='text-align:right'  maxlength='15' class='valor calcular_val_nota' value='".number_format($row['IR'],2,',','.')."' /></td>
                </tr>";
    $var .="<tr id=''>
                     <td collspan='2'><input type='checkbox' name='check_pcc' $disabled $checked_pcc id='check_pcc'/>
                         <label for='check_pcc'><b>PCC</b></label>
                     </td>
                     <td><b>INSS</b> <b id='inss-texto-aliquota'>({$formatNumber($aliquotaInss)}%)</b></td>
                     <td colspan='2'><b>VALOR TOTAL</b></td>
                </tr>";
    $var .="<tr id=''>
                        <td>R$ <input id='pcc' $read name='pcc' $disabled_pcc style='text-align:right' maxlength='15'class='valor calcular_val_nota' value='".number_format($row['PCC'],2,',','.')."' /></td>
                        <td>R$ <input id='inss' $read maxlength='15'class='valor calcular_val_nota' name='inss' style='text-align:right'  value='".number_format($row['INSS'],2,',','.')."' /></td>
                        <td>R$ <input type='text' class='valor' maxlength='15' id='valor_nota' name='valor_nota'  value='".number_format($row['valor'],2,',','.')."' readonly style='text-align:right'/></td>
                        <td align='center'>
                            <button type='button' $disabled id='calcular-retencoes' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'>
                                <span class='ui-button-text'>Calcular Retenções</span> 
                            </button>
                        </td>
                  </tr>";

    $var .= "<tfoot>
                <tr>
                    <th colspan='4'></th>
                </tr>
             </tfoot>";

    $var .="</table></div><div class='full'></div>";

    $var .= "<div class='full' id='div_parcelas'>";
    $var .= "<b>PARCELAS:</b>";
    $var .= '<script type="text/javascript" src="/utils/bower_components/moment/moment.js"></script>';
    $var .= '<script type="text/javascript" src="/utils/jquery/js/validar_vencimento_real.js?v=1.4"></script>';

    $var .= "<p>
            Gerar várias parcelas: <br>
            Qtd.: <input type='number' name='qtd_parcelas_gerar' class='qtd-parcelas-gerar' size='3' maxlength='3'  value='1'>
            Vencimento: <input type='text' name='vencimento_parcelas_gerar' class='vencimento-parcelas-gerar mask-data' size='10' maxlength='10' value='" . date('d/m/Y') . "'>
            Valor: <input type='text' name='valor_parcelas_gerar' maxlength='15' class='valor-parcelas-gerar valor'  value='0,00'>
            <button type='button' class='gerar-varias-parcelas' from='edit-notas'>+</button>
            <br>
            <small>*O vencimento será gerado automaticamente baseada na primeira data.</small>
         </p>";
    $var .= "<div style='overflow:auto;'>
            <table class='mytable' id='parcelas'>";
    $var .= "<thead>
            <tr>
                <th><input type='checkbox' $disabled class='selecionar-todos'></th>
                <th>TR PARCELA</th>
                <th>VENCIMENTO</th>
                <th>VENCIMENTO REAL</th>
                <th>VALOR</th>
                <th>CÓDIGO BARRA</th>
             </tr>
          </thead>
          <tfoot><tr><th colspan='6'></th></tr></tfoot><tbody>";
    $sql = "SELECT * FROM `parcelas` WHERE  `idNota` = '$idNotas' and status <> 'Cancelada'";
    $result=mysql_query($sql);
    $varInputParcelasPendentes = '';
    $trintaDias = new DateInterval('P30D');
    while( $row = mysql_fetch_array ($result, MYSQL_ASSOC)){
        $vencimentoParcela = \DateTime::createFromFormat('Y-m-d', $row['vencimento'])->add($trintaDias)->format('d/m/Y');
        $vencimentoRealParcela = \DateTime::createFromFormat('Y-m-d', $row['vencimento'])->add($trintaDias)->format('d/m/Y');
        if($row['vencimento_real'] != '') {
            $vencimentoRealParcela = \DateTime::createFromFormat('Y-m-d', $row['vencimento_real'])->add($trintaDias)->format('d/m/Y');
        }
        $del= "<img align='left' class='del_parcela' src='../utils/delete_16x16.png' title='Remover' border='0'/>";

        $var .= "<tr class='parcela editar_parcela' status='{$row['status']}' data-editar-parcela='0' data-id-parcela='{$row['id']}' >
            <td><input type='checkbox' class='selecionar-parcela-exclusao'></td>
            <td>{$del}<input type='text' class='form_parcelas' name='tr_parcela' size='5' $read value='{$row['TR']}'/></td>
            <td>
            	<input data-id-parcela='{$row['id']}' value='{$vencimentoParcela}' type='text' maxlength='10' size='10' $disabled $cond class='mask-data OBG form_parcelas vencimento-parcela vencimento'/>
            	<input type='hidden' class='form_parcelas' maxlength='128' size='20' name='codBarras' value='{$row['codBarras']}' $cond $read />
            </td>
            <td>
            	<input data-id-parcela='{$row['id']}' value='{$vencimentoRealParcela}' type='text' maxlength='10' size='10' $disabled $cond class='mask-data OBG form_parcelas vencimento-parcela vencimento-real-parcela vencimento'/>
            </td>
            <td><input class='valor OBG valor_parcela form_parcelas' maxlength='15' value='".number_format($row['valor'],2,',','.')."'  $cond $read type='text'  style='text-align:right' /></td>
            <td>
            	<input data-id-parcela='{$row['id']}' value='".$row['codBarras']. "' type='text' size='70' $disabled $cond class='codigo-barra form_parcelas'/>
            </td>
            </tr>";
    }
    $var .= "</tbody>
        </table>
    </div>";
    $var .= "<button id='add_parcela'>Adicionar Parcela +</button> 
             <button type='button' id='excluir-parcelas-selecionadas' >Excluir parcelas selecionadas</button>";

    $var .= "</div>";

    $var .= "<div class='full' style='text-align: center;'>";
    $var .= "<button type='button' id='calcular-vencimentos' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'>
                <span class='ui-button-text'>Calcular Vencimentos</span> 
            </button>";
    $var .= "</div>";
    $var .= "<div class='full' style='overflow: auto;'>";
    $var .= impostos_retidos($idNotas,$poder_editar,$notaref);
    $var .= "</div>";
    $var .= "<br><br>";

    //if($tipo == '1') {

    $var .= "<div class='full' id='div_rateio'>";
    $var .= "<div><b>RATEIO:</b>";
    $var .= "<button style='position: relative; float: right;' id='add_rateio' > Adicionar Rateio +</button></div>";
    $var .= "<table class='mytable' id='rateio' width='100%' editar='0' idNota='$idNotas' >";
    $var .= "<thead>
                  <tr>
                      <th>Unidade Regional</th>
                      <th>Valor</th>
                      <th>Percentual</th>
                      <th>IPCF4</th>
                      <th>Setor</th>
                  </tr>
           </thead>
           <tfoot>
                <tr>
                    <th colspan='7'></th>
                 </tr>
           </tfoot>
           <tbody>";
    $sql = "SELECT r.*, n.tipo 
            FROM `rateio`as r 
            inner join  notas as n 
            on (r.NOTA_ID = n.idNotas) 
            WHERE  `NOTA_ID` = '$idNotas' ";
    $result=mysql_query($sql);
    $cont=1;
    WHILE( $row = mysql_fetch_array ($result)){

        $var .= "<tr class='rateio' >
  <td name='empresa_1'>".empresa($row['CENTRO_RESULTADO'],1)."</td>
  <td name='1'><input maxlength='15' class='valor OBG valor_rateio' value='".number_format($row['VALOR'],2,',','.')."' type='text'  style='text-align:right;font-size:10px;' /></td>
  <td><input type='text' maxlength='10' valor_real='{$row['PORCENTAGEM']}' value='".number_format($row['PORCENTAGEM'],2,',','.')."' size='5' disabled='disabled' class='valor OBG porcento' style='text-align:right;font-size:10px;'/>%</td>
  <td class='td_4_1' name='ipcg4_$cont'>";
$var .= "  <img src='/utils/look_16x16.png' class='buscar-naturezas-dialog-rateio' style='cursor: pointer;' /> &nbsp;";

$var .= "  <input type = 'hidden' class='id-natureza-rateio' value='".$row['IPCG4']."' />
            <input type = 'text' class='OBG nome-natureza-rateio' style='font-size:8px;' readonly disabled size='50' value= '". $naturezaId[$row['IPCG4']]."' />
            </td>
                                
  
  <td class='td_5_1' name='assinatura_$cont'>".assinatura($row['ASSINATURA'],1)."</td>
  </tr>";
        $cont++;
    }
    $var .= "</tbody></table>";
    $var .= "</div>";
    $var .= "<div class='full' style='text-align: center'>";
    $var .= "<button id='inserir_nota_usar_nova' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button'  aria-disabled='false' >
                    <span class='ui-button-text'>Salvar</span>
                </button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <button id='voltar_' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' >
                    <span class='ui-button-text'>Voltar</span>
                </button>
            </div>";

    $var .= "</form></div></div>";

    return $var;
}


function show_notas(){
    
    $idNotas = (int) $_GET['idNotas'];
    $empresa = $_SESSION["empresa_user"];
    $usuario = $_SESSION["id_user"];
    $fechamento_caixa = \App\Controllers\FechamentoCaixa::getFechamentoCaixa();

    $result = mysql_query("SELECT * FROM `notas`as n  where  `idNotas` = '$idNotas' ") or trigger_error(mysql_error());
    $row = mysql_fetch_array($result);
    $notaAntecipada = $row['nota_adiantamento'];
    $notaAglutinacao = $row['nota_aglutinacao'];
    $filtrosCompensada = ['idNota' => $idNotas,
                     'compensada' => "'TOTAL', 'PARCIAL'"
                ];
    $filtrosAglutinada = ['idNota' => $idNotas,
                     'aglutinada' => 'S'
                      ];
    $responseCompensada = Parcela::getParcelaAtivaByFiltros($filtrosCompensada);
    $responseAglutinada = Parcela::getParcelaAtivaByFiltros($filtrosAglutinada);  

    $sql = "
    SELECT COUNT(*) AS qtd_parcelas_bordero 
    FROM bordero_parcelas 
    INNER JOIN parcelas ON parcela_id = parcelas.id 
    inner join bordero on(bordero_parcelas.bordero_id = bordero.id)
    WHERE parcelas.idNota = {$idNotas}
    and bordero.ativo = 's'
    GROUP BY idNota
    ";
          $rs = mysql_query($sql);
          $qtdBordero = current(mysql_fetch_array($rs))['qtd_parcelas_bordero'];

      
    $cancelarNota = false;
    $desfazerNota = false;
    if($qtdBordero >0){
        $cancelarNota = true;
        $desfazerNota = true;
    }

    if(count($responseCompensada) > 0){
        $cancelarNota = true;
        $desfazerNota = true;
    }
    
    if(count($responseAglutinada) > 0){
        $cancelarNota = true;
        $desfazerNota = true;
    }

    if($notaAglutinacao == 's' || $notaAglutinacao == "S" ){
        $cancelarNota = true;
        $desfazerNota = false;
    }


    $var = "<div id='dialog-editar-valores-parcela' parcela-id='-1' title='Editar Valores da Parcela'>";
    $var .= "<form name='form-editar-parcela' action='' method='POST'>";
    $var .= "<p><b>Juros:</b> <input class='valor OBG juros-parcela-valores' type='text' name='juros_parcela_valores' value='0,00' style='text-align:right' /></p>";
    $var .= "<p><b>Desconto:</b> <input class='valor OBG desconto-parcela-valores' type='text' name='desconto_parcela_valores' value='0,00' style='text-align:right' /></p>";
    $var .= "<p><b>Tarifas Adicionais:</b> <input class='valor OBG tarifas-adicionais-parcela-valores' type='text' name='tarifas_adicionais_parcela_valores' value='0,00' style='text-align:right' /></p>";
    
    $var .= "<p><b>Tarifa Cartão de Crédito:</b> <input class='valor OBG tarifa-cartao-parcela-valores' type='text' name='tarifa_cartao_parcela_valores' value='0,00' style='text-align:right' /></p>";
    $var .= "<p><b>Vencimento:</b> <input class='data OBG vencimento-parcela-valores' type='text' name='vencimento_parcela_valores' value='' style='text-align:right' /></p>";
    $var .= "<p><b>Vencimento Real:</b> <input class='data OBG vencimento-real-parcela-valores' type='text' name='vencimento_real_parcela_valores' value='' style='text-align:right' /></p>";

    $var .= "</form></div>";
   
    $tipo_nota = $row['tipo'];// 0 recebimento e 1 despesas
    $var .= "<div id='dialog-processar-parcela' title='Processar parcela' idparcela='-1' tipo_nota='{$tipo_nota}'>";
    $var .= alerta();
    $var .= "<form name='form' action='' method='POST'>";
    $var .= '<script type="text/javascript" src="/utils/bower_components/moment/moment.js"></script>';
    $var .= "<input id='data-fechamento' type='hidden' value='{$fechamento_caixa['data_fechamento']}'>";
    $var .= "<p><b>Origem:</b>".siglaOrigemFundos('processar');
    $var .= "<p><b>Tipo:</b>".aplicacaoFundos(NULL)."&nbsp;&nbsp;&nbsp;
				<b>Data de Compensação:</b><input id='data'  type='text' maxlength='10' size='10' class='verifica-fechamento OBG '/>";
    $var .= "<p><b>Vencimento:</b><input type='text' name='vencimento' class='data OBG '/>";
    $var .= "<p><b>Vencimento Real:</b><input type='text' name='vencimento_real' class='verifica-fechamento OBG '/>";
    $var .= "<p><b>N&deg; Documento:</b><input type='text' name='num_documento'/>";
    $var .= "<p><b>Valor:</b><input class='valor OBG soma_valor' type='text' name='valor'  style='text-align:right' />";
    $var .= "<p><b>Observa&ccedil;&atilde;o:</b><input type='text' name='obs'/>";

    $var .= "<input type='text' id='usuario' value='{$usuario}' hidden />";

    // justificativa para juros
    $motivoJuros = Financeiro::getMotivoJuros();
    $optionMotivo = "<option value= ''> Selecione</option>";
    foreach($motivoJuros as $m){
        $optionMotivo .= "<option value= {$m['id']}> {$m['motivo']}</option>";
    }

    $selsectJustificativaJuros ="<b>Motivo para juros: </b>
								<select name='motivo-juros' id='motivo-juros'>
												{$optionMotivo}
 								</select>";

    //Fim impostos do a receber 13/05/2013
    $var .= "<p><b>Juros/Multa:</b><input type='text' class='valor soma_valor' size='10px' value='0,00' style='text-align:right' name='juros'/></p>
			<p> {$selsectJustificativaJuros}</p>
		<p><b>Desconto:</b><input type='text' style='text-align:right' class='valor soma_valor' size='10px' value='0,00' name='desconto'/></p>";
    $var .= "<p><b>Tarifas Adicionais:</b><input class='valor soma_valor' type='text' name='outros'  value='0,00' style='text-align:right' />";
    $var .= "<p><b>Tarifas Cartão de Crédito:</b><input class='valor soma_valor' type='text' name='tarifa_cartao'  value='0,00' style='text-align:right' />";

    $var .= "<p><b>Encargos Financeiros: <small style='color: #FF0000;'>(Apenas utilizar quando for empréstimo!)</small></b><br><input class='valor soma_valor' type='text' name='encargos'  value='0,00' style='text-align:right' />";

    if($tipo_nota == 0){
        $var .= "<p><b>Valor Pago:</b><span  class='valor_pago' ></span>";
    }else{
        $var .= "<p><b>Valor Pago:</b><span  class='valor_pago' ></span>";
    }
    $var .= "</form>
               <table id='lembrete-parcela-origem' class='font10'></table>
               </div>";
    $var .="<div id='dialog-parcela-ok' title='Processada'>
	<form name='form' action='' method='POST'>
	<p><span >Parcela processada com sucesso!</span></p></form></div>";
    $var .= "<div id='dialog-cancelar-parcela' title='Cancelar parcela' idparcela='-1'>";
    $var .= alerta();
    $var .= "<form name='form' action='' method='POST'>";
    $var .= "<p><b>Motivo:</b><input class='OBG' type='text' maxlength='15' size='15' name='obs'/>";
    $var .= "</form></div>";

    $var .= "<div id='dialog-cancelar-nota' title='Cancelar nota' idnota='".$row['idNotas']."'>";
    $var .= alerta();
    $var .= "<span><font style='background-color:red'><b>CUIDADO!</b></font> Cancelar uma nota irá cancelar todas as suas parcelas.</span>";
    $var .= "<form name='form' action='' method='POST'>";
    $var .= "<p><b>Motivo:</b><input class='OBG' type='text' maxlength='15' size='15' name='obs'/>";
    $var .= "</form></div>";

    $data = date("d/m/Y");
    $var .="<div id='dialog-informe-perda' parcela-id='' nota-id='' valor='' title='Informe de perda'>
                
                 <p>
                        <b>Data:</b>
                        <br>
                        <input id='data-informe-perda'  type='text'  class='verifica-fechamento OBG ' />
                </p>                
                    <p><b>Justificativa:</b>
                    <br>
                    <textarea class='OBG' id='justificativa-infome-perda' cols='50' rols='5'></textarea>
                    </p>
            </div>";

    $var .= "<div id='dialog-add-parcela' nota='$idNotas'>";
    $var .= alerta();
    $var .= "<b>Parcelas:</b>";
    $var .= "<table class='mytable' id='parcelas'>";
    $var .= "<thead><tr><th>Valor</th><th>Vencimento</th><th>C&oacute;digo Barras</th></tr></thead><tfoot><tr><th colspan='3'></th></tr></tfoot><tbody>";
    $var .= "<tr><td><input id='valor' class='valor OBG' value='0,00' type='text' name='valor' size='10' style='text-align:right' /></td>
		  <td><input id='vencimento' value='$data' type='text' maxlength='10' size='10' class='data OBG '/></td>
		  <td><input id='codBarras' type='text' maxlength='128' size='20' name='codBarras' /></td></tr>";
    $var .= "</tbody></table>";
    $var .= "</div>";

    $var .= "<div id='dialog-editar-parcela' title='Editar parcela' parcela='-1'>";
    $var .= alerta();
    $var .= "<b>Valor:</b><input class='valor OBG' type='text' name='valor' style='text-align:right' />";
    $var .= "<br/><b>Vencimento:</b><input name='vencimento' value='' type='text' maxlength='10' size='10' class='data OBG '/></div>";

    $var .= "<center><h1>Processar Parcela(s)</h1></center>";
    $var .= "<table class='mytable' border=0 width=100% >";
    $var .= "<thead><tr>";
    $var .= "<th><b>TR</b></th>";
    $var .= "<th><b>C&Oacute;DIGO</b></th>";
    $var .= "<th><b>FORNECEDOR</b></th>";
    $var .= "<th><b>VALOR</b></th>";
    $var .= "<th><b>TIPO</b></th>";
    $var .= "</tr></thead>";
    $var .= "<tr class='odd'>";
    $var .= "<td valign='top' id='idnota' idnota={$row['idNotas']}>" . $row['TR'] . "</td>";
    $var .= "<td valign='top'>" . $row['codigo'] . "</td>";
    $var .= "<td valign='top'>" . fornecedorNome( $row['codFornecedor']) . "</td>";
    $var.="<td valign='top'>R$ ".number_format($row['valor'],2,',','.')."</td>";
    $tipo = ((int) $row['tipo'] == 0) ? "Entrada" : "Saída";
    $var .= "<td valign='top'>" . $tipo . "</td>";
    $var .= "</tr></table>";
    $var .= "<table class='mytable' border=0 width=100% ><thead><tr>";
    $var .= "<th width=10%><b>Data de Entrada</b></th>";
    $var .= "<th width=10% ><b>Status</b></th>";
    $var .= "<th><b>Descri&ccedil;&atilde;o</b></th>";
    $var .= "</tr></thead>";
    $var .= "<tr class='odd'>";
    $var .= "<td valign='top'>" . implode("/",array_reverse(explode("-",$row['dataEntrada']))) . "</td>";
    $var .= "<td valign='top'>" . nl2br($row['status']) . "</td>";
    $var .= "<td valign='top'>" . $row['descricao'] . "</td>";
    $var .= "</tr></table>";
    $sql2="select cf.*,c.NOME from contas_fornecedor as cf inner join contas as c on (c.ID = cf.BANCO_FORNECEDOR_ID) where FORNECEDOR_ID = {$row['codFornecedor']} ";
    $result2 = mysql_query($sql2);
    $var .= "<table class='mytable' border=0 width=100% ><thead><tr>";
    $var .= "<th width=50%><b>Banco</b></th>";
    $var .= "<th width=20% ><b>Conta</b></th>";
    $var .= "<th><b>Ag&ecirc;ncia</b></th>";
    $var .= "</tr></thead>";
    $i=1;
    while($row = mysql_fetch_array($result2)){
        if($i++%2==0)
            $cor = "";
        else $cor = "#E9F4F8";
        $var .= "<tr  bgcolor='{$cor}'>";
        $var .= "<td valign='top'>".$row['NOME']."</td>";
        $var .= "<td valign='top'>".$row['N_CONTA']."</td>";
        $var .= "<td valign='top'>".$row['AG']."</td></tr>";
    }
    $var .="</table>";

    $cancel = "";
    if(!$cancelarNota){  
    if($row['status']!='Cancelada') $cancel = "<button id='cancelar_nota' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' type='submit' role='button' aria-disabled='false'>
		<span class='ui-button-text' >Cancelar</span></button>";
        if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_pagar_receber']['financeiro_conta_pagar_receber_buscar_parcela']['financeiro_conta_pagar_receber_buscar_parcela_cancelar']) || $_SESSION['adm_user'] == 1) {
            $var .= "$cancel";
        }
    }
    

    $var .="<div id='dialog-desfazer-processamento-nota' data-idparcela=''>
                 <p><span>Deseja realmente desfazer o processamento da Nota?</span></p>
                 <span><b>Justificativa:</b><textarea class='OBG' id='justificativa-desfazer-processamento-nota'></textarea></span>
                </div>";
                if(!$cancelarNota){              
                
                    if(isset($_SESSION['permissoes']
                                        ['financeiro']
                                        ['financeiro_conta_pagar_receber']
                                        ['financeiro_conta_pagar_receber_buscar_parcela']
                                        ['financeiro_conta_pagar_receber_buscar_parcela_desprocessar']) ||
                        $_SESSION['adm_user'] == 1) {
                        
                                $var .=" <tr>
                                                <td>
                                                    <button  data-idnota='{$idNotas}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover desfazer-processamento-nota' type='submit' role='button' aria-disabled='false'>
                                                    <span class='ui-button-text' >Desfazer Processamento Nota</span>
                                                    </button>
                                                </td>
                                            </tr>";
                            }
                        
                }


    $var.="<br/><br/><center><b>PARCELAS</b></center><br>";
    $var .="<div id='dialog-desfazer-processamento-parcela' data-idparcela=''>
                 <p><span>Deseja realmente desfazer o processamento da Parcela?</span></p>
                 <span><b>Justificativa:</b><textarea class='OBG' id='justificativa-desfazer-processamento-parcela'></textarea></span>
                </div>";

     // informe perda           
   
    //$var .= "<table class='mytable' border=0 width=100% style='position: relative;float: left; margin: 0 auto;' >";

    $sql = "
            SELECT
                p . * ,
                (CASE
                WHEN p.status='Processada' THEN IF(p.CONCILIADO ='N'  ,'Não Conciliada','Conciliada')
                ELSE '' END )  as conciliacao,
                c.id AS idComp,
                o.SIGLA_BANCO,
                a.forma AS af,
                cb.* ,
                e.SIGLA_EMPRESA,
                tipo_conta_bancaria.nome as tipoConta,
                motivo_juros.motivo,
                nip.id as informe_perda_id
            FROM parcelas AS p
            LEFT OUTER JOIN comprovanteparcelas AS c ON c.idParcela = p.id
            LEFT OUTER JOIN contas_bancarias AS cb ON p.origem = cb.ID
            LEFT OUTER JOIN origemfundos AS o ON cb.ORIGEM_FUNDOS_ID = o.id
            LEFT OUTER JOIN empresas AS e ON cb.UR = e.id
            LEFT OUTER JOIN aplicacaofundos AS a ON p.aplicacao = a.id
            LEFT JOIN tipo_conta_bancaria ON tipo_conta_bancaria.id = cb.TIPO_CONTA_BANCARIA_ID
            LEFT JOIN motivo_juros on p.motivo_juros_id = motivo_juros.id 
            LEFT JOIN nota_informe_perda as nip on (p.id = nip.parcela_id and nip.canceled_by is null)
           
            WHERE
            
            idNota = '$idNotas' and p.status <>'Cancelada' order by p.TR, p.vencimento ASC ";
            


    $sql_max_tr="SELECT
                        max( `TR` ) as max_tr
                      FROM
                       `parcelas`
                      WHERE
                      idNota =$idNotas and
                      status='Processada'";
    $result_max_tr = mysql_query($sql_max_tr);
    $hdl_max_tr = mysql_fetch_array($result_max_tr);
    $max_tr = $hdl_max_tr['max_tr'];

    $result = mysql_query($sql);

    while($row = mysql_fetch_array($result)){
        foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
        $valor = number_format($row['valor'],2,',','.');
        $status = $row['status'];
        if($row['idComp'] != NULL){
            $status .= " <a href='' id='{$row['idComp']} ' class='show-comprovante'><img src='../utils/icon-anexo.png' border='0'></a>";
        }
        $of = $row['of']; $af = $row['af']; $editparcela = "";
        if($row['status'] == "Pendente"){$of = ""; $af = "";
            //$editparcela = "<img src='../utils/edit_16x16.png' class='editparcela' title='Editar' border='0' cod='{$row['id']}' >";
        }else{
            $of= $row['tipoConta'] . ' - ' . $row['SIGLA_BANCO']." - ".$row['AGENCIA']." - ".$row['NUM_CONTA']." - ".$row['SIGLA_EMPRESA'];
        }
        $parcela = $row['TR'];
        $valor_processado= $row['VALOR_PAGO'];
        $vencimento = $row['vencimento'] != '' && $row['vencimento'] != '0000-00-00' ? implode("/",array_reverse(explode("-",$row['vencimento']))) : '';
        $vencimento_real = $row['vencimento_real'] != '' && $row['vencimento_real'] != '0000-00-00' ? implode("/",array_reverse(explode("-",$row['vencimento_real']))) : '';

        $var .= "<div  style='overflow: auto;'>
        <table class='mytable'>
        <thead>
					<tr>
						<th>N</th>
						<th>Valor</th>
                        <th>Vencimento</th>
                        <th>Venc. Real</th>
						<th>Origem/Destino</th>
						<th>Forma Pgto</th>
						<th>Status</th>
						<th>Val. Processado</th>
						<th>Juros</th>
						<th>Desconto</th>
                        <th>Encargos</th>
                        <th>Tarifa Cartão</th>
					</tr>
				 </thead>";
        $var .= "<tr class='odd'>
					<td>$parcela </td>
					<td>R$ $valor</td>
                    <td>{$vencimento}</td>
                    <td>{$vencimento_real}</td>
					<td>$of</td>
					<td>$af</td>
					<td>$status</td>
					<td>R$ ".number_format($valor_processado,2,',','.')."</td>
					<td>R$ ".number_format($row['JurosMulta'],2,',','.')."</td>
					<td>R$ ".number_format($row['desconto'],2,',','.')."</td>
                    <td>R$ ".number_format($row['encargos'],2,',','.')."</td>
                    <td>R$ ".number_format($row['tarifa_cartao'],2,',','.')."</td>
				 </tr>";
        $var .= "<tr><td colspan='8' ><b>Observa&ccedil;&otilde;es:</b> {$row['obs']}</td><td><b>Tarifas Adicionais:</b> R$ ".number_format($row['OUTROS_ACRESCIMOS'],2,',','.')." </td><td colspan='3'><b>N&deg; Documento: </b>{$row['NUM_DOCUMENTO']}</td></tr>";
        $data_pagamento = '';
        if($row['DATA_PAGAMENTO'] != '' && $row['DATA_PAGAMENTO'] != '0000-00-00') {
            $data_pagamento = \DateTime::createFromFormat('Y-m-d', $row['DATA_PAGAMENTO'])->format('d/m/Y');
        }
        $var .= "<tr class='odd' >
                    <td colspan='8' >
                        <b>C&oacute;digo de barras:</b> {$row['codBarras']}
                    </td>
                    <td>
                        <b>{$row['conciliacao']}</b>
                    </td>
                    <td colspan='3'>
                        <b>Data Pagamento:</b> $data_pagamento
                    </td>
                 </tr>";
        $var .= "<tr >
                    <td colspan='8' >
                        <b>Compensada:</b> {$row['compensada']}
                    </td>
                    <td colspa='4'>
                        <b>Valor Compensado:</b> ".number_format($row['valor_compensada'],2,',','.')."
                    </td>
                    
                 </tr>";
                
                 if($row['compensada'] != 'NAO' && $row['perda'] <> 'S'){
                    $var .= "<tr>
                                <td colspan='13'>";
                     $responseParcela = Compensacao::getCompensacaoByIdParcela($row['id']);

                     $var .= "
                     <table class='mytable' width='100%'>
                     <thead> <tr>
                            <th></th>                 
                            <th>N° Documento</th>
                            <th>Fornecedor</th>
                            <th>Data Emissão</th>
                            <th>Tipo</th>
                            <th>Valor</th>                                    
                            <th>Vencimento Real</th>
                        </tr>
                        </thead>";
                     foreach($responseParcela as $p){
                         
                        $var .= "<tr> 
                                <td>";
                                if(isset($_SESSION['permissoes']
                                    ['financeiro']
                                    ['financeiro_conta_pagar_receber']
                                    ['financeiro_conta_pagar_receber_buscar_parcela']
                                    ['financeiro_conta_pagar_receber_buscar_parcela_compensar']) || $_SESSION['adm_user'] == 1) {
                  
                                        $var .=" <a class='cancelar-compensacao'  id-compensacao = '{$p['id']}'>
                                                     <img src='../utils/delete_16x16.png' title='Cancelar Compensação' border='0'>
                                            </a> ";  
                                    }                           
                                    
                        $var .="</button>
                                </td>
                                <td>{$p['codNota']}</td>
                                <td>{$p['nome_fornecedor']}</td>
                                <td>{$p['dataEntrada']}</td>
                                <td>{$p['sigla']}</td>
                                <td>".number_format($p['valor'],2,',','.')."</td>                                            
                                <td>{$p['vencimento_real']}</td>
                            </tr>";

                     }
                     $var .= "</table></td>
                     </tr>";

                 }
         


        switch($row['status']){
            case "Pendente":
                $vencimento = '';
                if($row['vencimento'] != '' && $row['vencimento'] != '0000-00-00') {
                    $vencimento = \DateTime::createFromFormat('Y-m-d', $row['vencimento'])->format('d/m/Y');
                }
                $vencimentoReal ='';
                if($row['vencimento_real'] != '' && $row['vencimento_real'] != '0000-00-00') {
                    $vencimentoReal = \DateTime::createFromFormat('Y-m-d', $row['vencimento_real'])->format('d/m/Y');
                }
                $var .= "<tr><td colspan='3' >";
                if(isset($_SESSION['permissoes']
                                    ['financeiro']
                                    ['financeiro_conta_pagar_receber']
                                    ['financeiro_conta_pagar_receber_buscar_parcela']
                                    ['financeiro_conta_pagar_receber_buscar_parcela_processar']) || $_SESSION['adm_user'] == 1) {
                    $valorPagar = ($row['valor'] + $row['JurosMulta'] + $row['OUTROS_ACRESCIMOS']) - $row['desconto'];
                    $var .= "<button idparcela='{$row['id']}' 
                                     vencimento='{$vencimento}' 
                                     vencimento_real='{$vencimentoReal}' 
                                     valor='$valor' 
                                     juros='{$row['JurosMulta']}' 
                                     desconto='{$row['desconto']}' 
                                     tarifas-adicionais='{$row['OUTROS_ACRESCIMOS']}'
                                     valor-pagar='{$valorPagar}'
                                     parcela-origem='{$row['PARCELA_ORIGEM']}' 
                                     tipo_nota= '{$tipo_nota}' 
                                     tarifa_cartao = '{$row['tarifa_cartao']}'
                                     class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover processar_parcela' type='submit' role='button' aria-disabled='false'>
                        <span class='ui-button-text' >Processar</span>
                        </button>";
                }
                $sqlBordero = <<<SQL
SELECT parcela_id
FROM bordero_parcelas
INNER JOIN bordero ON bordero.id = bordero_parcelas.bordero_id
WHERE parcela_id = '{$row['id']}'
AND bordero.ativo = 's'
SQL;
                $rs = mysql_query($sqlBordero);
                $rowBordero = mysql_fetch_array($rs, MYSQL_ASSOC);
                $inBordero = isset($rowBordero['parcela_id']);
                if((isset($_SESSION['permissoes']
                        ['financeiro']
                        ['financeiro_conta_pagar_receber']
                        ['financeiro_conta_pagar_receber_buscar_parcela']
                        ['financeiro_conta_pagar_receber_buscar_parcela_editar_valores']) || $_SESSION['adm_user'] == 1) && !$inBordero) {
                    $var .= "<button vencimento_real ='{$vencimentoReal}' tarifa_cartao = '{$row['tarifa_cartao']}' vencimento ='{$vencimento}' idparcela='{$row['id']}' juros='{$row['JurosMulta']}' desconto='{$row['desconto']}' tarifas-adicionais='{$row['OUTROS_ACRESCIMOS']}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover editar_valores_parcela' type='button' role='button' aria-disabled='false'>
                                <span class='ui-button-text' >Editar Valores</span>
                            </button>";
                }
                    $var .= "</td>
                    <td colspan='8'>";
                         if( ($notaAntecipada == 'N' && $row['status'] != 'Processada') &&  $row['compensada'] != 'TOTAL'  ){
                            if(isset($_SESSION['permissoes']
                                    ['financeiro']
                                    ['financeiro_conta_pagar_receber']
                                    ['financeiro_conta_pagar_receber_buscar_parcela']
                                    ['financeiro_conta_pagar_receber_buscar_parcela_descompensar']) || $_SESSION['adm_user'] == 1) {
                  
                                        // $var .="
                                        //      <span class='ui-button-text' >
                                        //             <a  href='/financeiros/compensacao/?action=compensar-parcela&id={$row['id']}&antecipada={$notaAntecipada}' target='_blank' >
                                        //                     Compensar
                                        //             </a> 
                                        //         </span>
                                        //     ";  

                                            $var .= "<button style='background-color:green' url='/financeiros/compensacao/?action=compensar-parcela&id={$row['id']}&antecipada={$notaAntecipada}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover compensar' type='submit' role='button' aria-disabled='false'>
                                                        <span class='ui-button-text' >Compensar</span>
                                                    </button>";
                                    }  
                            }
                            if($tipo_nota == 0){
                                $var .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button hoje='{$data}' parcela-id='{$row['id']}' nota-id='{$row['idNota']}' valor='{$row['valor']}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover informe-perda'  role='button' aria-disabled='false'>
                                         <span class='ui-button-text' >Informe Perda</span>
                                        </button>";
                            }
                    
                $var .= "</td>
                    </tr>";
                break;
            case "Processada":
            case "Inconsistente":
                if($row['idComp'] == NULL){
                    $desprocessar = $max_tr== $row['TR'] ?'trtrtrtrt':'';
                    $var .= "<tr>
                                  <td colspan='4'>
                                    <button   class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover comprovante' type='submit' role='button' aria-disabled='false'>
                                        <span class='ui-button-text' >Enviar comprovante</span>
                                    </button>
                                  </td>";
                    $var .= "<td colspan='2'>
                                    <div class='form-comprovante' >";
                    $var .= "<form enctype='multipart/form-data' name='form' action='query.php' method='POST'>";
                    $var .= "<input type='hidden' value='salvar_comprovante' name='query' />
                                           <input type='hidden' value='{$row['id']}' name='id' />";
                    $var .= "<input type='hidden' value='{$row['idNota']}' name='nota' />";
                    $var .= "<input class='arquivo-comprovante' type='file' name='comprovante'/>";
                    $var .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    $var .= "<input class='salvar-comprovante' type='submit' value='Salvar'/>";
                    $var .= "</form>
                                   </div>
                                </td>
                                <td colspan='5'><b>Motivo Juros:</b> {$row['motivo']}</td>
                              </tr>";
                }
                //15/07/2014
            
                if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_pagar_receber']['financeiro_conta_pagar_receber_buscar_parcela']['financeiro_conta_pagar_receber_buscar_parcela_desprocessar']) || $_SESSION['adm_user'] == 1) {
                    $data_pag_obj = new \DateTime($row['DATA_PAGAMENTO']);
                    $data_fechamento_obj = new \DateTime($fechamento_caixa['data_fechamento']);
                   
                    
                    if(/*$max_tr == $row['TR'] and*/ $row['CONCILIADO']=='N' && $data_pag_obj > $data_fechamento_obj){
                       
                        if($row['compensada'] == 'NAO' && ($row['nota_id_fatura_aglutinacao'] == 0 || $row['nota_id_fatura_aglutinacao'] == NULL) &&  $row['perda'] <> 'S'){
                            
                            $var .= " <tr>
                            <td>
                                <button  data-idparcela='{$row['id']}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover desfazer-processamento-parcela' type='submit' role='button' aria-disabled='false'>
                                            <span class='ui-button-text' >Desfazer Processamento</span>
                                </button>
                            </td>
                            </tr>";
                        }
                       
                        
                    }
                    if( $row['perda'] == 'S'&& $data_pag_obj > $data_fechamento_obj){
                            
                        $var .= " <tr>
                        <td colspan='4'>
                            <button nota-id = '{$row['idNota']}' parcela-id='{$row['id']}' informe-perda-id = '{$row['informe_perda_id']}' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover cancelar-informe-perda' type='submit' role='button' aria-disabled='false'>
                                        <span class='ui-button-text' >Cancelar Informe de  Perda</span>
                            </button>
                        </td>
                        </tr>";
                    }
                }
                
                break;
        }
        $parcela++;
        $var .= "</table></div><br>";
    }
   // $var .= "</table>";
    $var .="<button id='voltar' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button></span>";

    return $var;
}

function del_notas(){

    $idNotas = (int) $_GET['idNotas'];
    mysql_query("DELETE FROM `notas` WHERE `empresa` in{$empresa} AND `idNotas` = '$idNotas' ") ;
    return "Nota exclu&iacute;da.<a href='?op=notas&act=listar'>(voltar)</a><br />";
}

function demonstrativo_financeiro() {
    $ur= $_SESSION['empresa_principal'];
    if($ur==1){
        $todas= "<p> <b>Unidade Regional: </b>" . ur($ur) . "<input type='checkbox' name='todos_ur' id='todos_ur'>Todas Unidades Regionais</input></p>";
    }else{
        $todas="<input name='empresa2' type=hidden value={$ur}> </input>";
    }
    echo "<div id='div-demonstrativo'>	";
    echo "<h1><center>Demonstrativo Financeiro</center></h1>";
    echo "<form>";
    echo $todas;
    echo "<p><b>Tipo:</b><input type='radio' name='btipo' class='btipo' value='t' CHECKED/>Todos&nbsp;&nbsp;";
    echo "</b><input type='radio' name='btipo' class='btipo' value='1'/>Contas a Pagar&nbsp;&nbsp;";
    echo "<input type='radio' name='btipo' class='btipo' value='0'/>Contas a Receber</p>";
    echo "<p><b> Per&iacute;odo:</b>  In&iacute;cio <input id='entrada' type='text' name='inicio' maxlength='10' class='OBG' size='10' />";
    echo "&nbsp;Fim<input id='vencimento' class='OBG' type='text' name='fim' maxlength='10' size='10' /></p>";
    echo "  <p><b>Status:</b>
	            <input type='radio' name='status' checked value='Pendente'/>Pendente
    			<input type='radio' name='status' value='Processada'/>Processada
    			<input type='radio' name='status' value='Cancelada'/>Cancelada</p>";
    echo "<p><input type='checkbox' id='comparar_porcentagem' name='comparar_porcentagem'></input><label for='comparar_porcentagem'>Comparar porcentagem.</label></p>";
    //echo "<p><b>Natureza:</b>" . $this->naturezas(NULL)." <input type='checkbox' name='tnaturezas' CHECKED/>Todos";
    echo"<button id='pesquisar-demonstrativo' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button' style='float:left;' type='button'>
<span class='ui-button-text'>Pesquisar</span>
</button>
&nbsp;&nbsp;<button id='voltar_' type = 'button' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button>
";
    echo "</form>";
    echo"</div>";
    echo "<div id='divsubnatureza'></div>";
    // echo "<br/><input type='submit' value='Exibir' /><input type='hidden' value='1' name='submitted' />";
}

function conciliacao(){
    echo "<div id='div-conciliacao'>	";
    echo "<h1><center>Concilia&ccedil;&atilde;o Banc&aacute;ria</center></h1>";
    echo "<form>";
    echo "<p>
			<div class='ui-state-highlight'>
				<span class='ui-icon  ui-icon-info' style='float:left;padding:2px;'>		
				</span> 
				Observação: Nessa funcionalidade atualmente não é permitido desfazer a conciliação das Tarifas Bancáris e Impostos.
			</div>
		</p>";
    echo "<p><b>Origem:</b>".siglaOrigemFundos('processar');
    echo "<p><b> Per&iacute;odo:</b>  In&iacute;cio <input id='entrada' type='text' name='inicio' maxlength='10' class='OBG' size='10' />";
    echo "&nbsp;Fim<input id='vencimento' class='OBG' type='text' name='fim' maxlength='10' size='10' /></p>";
    echo "<p><input type='radio' value='T' name='conciliado_radio'  checked='checked'/><b>Todos</b></p>";
    echo "<p><input type='radio' value='S' name='conciliado_radio'/><b>Conciliado</b></p>";
    echo "<p><input type='radio' value='N' name='conciliado_radio'/><b>N&atilde;o Conciliado</b></p>";
    echo"<button id='pesquisar-conciliacao' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button' style='float:left;' type='button'>
	<span class='ui-button-text'>Pesquisar</span>
	</button>
	&nbsp;&nbsp;
	<button id='voltar_' type = 'button' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button>
";
    echo "</form>";
    echo"</div>";
    echo "<div id='div-conciliacao-resultado'>";
    echo alerta();
    echo "</div>";


}

//Eriton 04-06-2013 Fun��o para trazer select de "resumo" de contas
function siglaOrigemFundos($id, $idSelect = null, $mostrarTodos = null){
    $cont = 1;
  
    if(($id =='processar' || $id =='origem') && $_SESSION['empresa_principal'] != 1) {          
        $ur = "where c.UR in {$_SESSION['empresa_user']}";
    }else{
        $ur="";
    }
    $sql =  "SELECT
    c.ID as id,
    o.forma,
    o.SIGLA_BANCO as banco,
    c.AGENCIA as agencia,
    c.NUM_CONTA as conta,
    e.SIGLA_EMPRESA as ur,
    tipo_conta_bancaria.abreviacao
FROM contas_bancarias as c
    INNER JOIN origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id)
    INNER JOIN empresas as e ON (c.UR = e.id)
    LEFT JOIN tipo_conta_bancaria ON tipo_conta_bancaria.id = c.TIPO_CONTA_BANCARIA_ID
       {$ur}
ORDER BY
forma,agencia,conta,abreviacao,ur";

        
    
    //$result = mysql_query("SELECT * FROM origemfundos where id in (6,7,8) ORDER BY forma;");
    $result = mysql_query($sql);
            
           
    $var = "<select name='origemFundos' id='{$idSelect}' style='background-color:transparent;' class='OBG'>";
    $var .= "	<option selected disabled value=''>Selecione...</option>";

    while($row = mysql_fetch_array($result))
    {
        $abreviacao = $row['abreviacao'];
        if(($id <> NULL) && ($id == $row['forma']))
            $var .= "<option value='{$row['id']}' selected>{$row['forma']} - {$abreviacao} - {$row['agencia']} - {$row['conta']} - {$row['ur']}</option>";
        else
            if($cont == 1  && $id != 'processar' && $mostrarTodos != 'N'){
                $var .= "<option value='t'>TODAS</option>";
            }

        $var .= "<option value='{$row['id']}'>{$row['forma']}  - {$abreviacao} - {$row['agencia']} - {$row['conta']} - {$row['ur']}</option>";
        $cont++;

    }
    $var .= "</select>";
    return $var;
    //return $ur;
}

//Eriton 05-06-2013 Fun��o para buscar extrato bancario
function extrato_bancario($id){
    echo "<div id='div-extrato-bancario'>";
    echo "<h1><center>Extrato Banc&aacute;rio</center></h1>";

    echo "<br>";

    $sql = "SELECT
				c.ID as id_conta,
				o.forma as banco,
				c.AGENCIA as agencia,
				c.NUM_CONTA as conta,
				e.nome as ur,
				tipo_conta_bancaria.nome as tipoConta,
				CONCAT(pcc.COD_N4, ' - ', pcc.N4) as conta_contabil,
                c.SALDO_INICIAL as saldo_inicial,
				c.DATA_SALDO_INICIAL as data_saldo_inical
			FROM
				contas_bancarias as c
				INNER JOIN origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id)
				INNER JOIN empresas as e ON (c.UR = e.id)
				LEFT JOIN tipo_conta_bancaria ON tipo_conta_bancaria.id = c.TIPO_CONTA_BANCARIA_ID
				LEFT JOIN plano_contas_contabil AS pcc ON (pcc.ID = c.IPCC)
			WHERE
				c.ID = {$id}
			ORDER BY
			banco
			";
    $result = mysql_query($sql);
    $tipoConta = mysql_result($result,0,5);
    $conta = mysql_result($result,0,1);
    $agencia = mysql_result($result,0,2);
    $num_conta = mysql_result($result,0,3);
    $ur = mysql_result($result,0,4);
    $ipcc = mysql_result($result,0,6);
    $saldo_inicial = mysql_result($result,0,7);
    $data_saldo_inicial = \DateTime::createFromFormat('Y-m-d', mysql_result($result,0,8))->format('d/m/Y');
    $cor_saldo = "";
    if($saldo_inicial < 0) {
        $cor_saldo = "style='font-size:12px; color:red;'";
    }
    echo "<p><b>Tipo:</b>&nbsp;{$tipoConta}<br>";
    echo "<b>Banco:</b>&nbsp;{$conta}<br>";
    echo "<b>Agencia:</b>&nbsp;{$agencia}<br>";
    echo "<b>Conta:</b>&nbsp;{$num_conta}<br>";
    echo "<b>Saldo Inicial:</b>&nbsp;<span {$cor_saldo}>R$ " . number_format($saldo_inicial,2,',','.') . "</span>&nbsp;(Em: {$data_saldo_inicial})<br>";
    echo "<b>UR:</b>&nbsp;{$ur}</b><br>";
    echo "<b>Conta Contábil:</b>&nbsp;";
    echo isset($ipcc) && !empty($ipcc) ? $ipcc : "Nenhuma conta associada.";
    echo "</b></p>";

    echo "<form>";
    echo "<p><input type='radio' id='conciliado' checked='checked' name='conciliado' value='S'><label for='conciliado'>Conciliada</label> <input type='radio'name='conciliado' value='N'><label for='nconciliado'>Não Conciliada</label></p>";
    echo "<p><b> Per&iacute;odo:</b>  In&iacute;cio <input id='entrada' type='text' name='inicio' maxlength='10' class='OBG' size='10' />";
    echo "&nbsp;Fim<input id='vencimento' class='OBG' type='text' name='fim' maxlength='10' size='10' /></p>";
    echo "<input type='hidden' value='{$id}' name='id_conta'/>";
    echo " <br><div style='text-align:center'>
    <button id='pesquisar-extrato' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button' ' type='button'>
    <span class='ui-button-text'>Pesquisar</span>
    </button>
                        &nbsp;&nbsp;
                            <button id='voltar_' type = 'button' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' >
                            <span class='ui-button-text'>Voltar</span>
                            </button>        
                        </div>";
                
    echo "</form>";

    echo "</div>";

    echo "<div id='div-extrato-bancario-resultado'>";
    echo alerta();
    echo "</div>";
}
//Eriton fun��o para cadastrar contas bancarias 04-06-2013
function listarContasBancarias(){
    echo "<h1><center>Contas Bancárias</center></h1>";
    echo "<div style='text-align:right'>";
    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_bancaria']['financeiro_conta_bancaria_consultar_saldos']) || $_SESSION['adm_user'] == 1){
        echo "<a target='_blank' href='/financeiros/relatorios/?action=saldos-contas-bancarias' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button' type='button'>
                <span class='ui-button-text'>Consultar Saldos</span>
             </a>";
    }

    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_bancaria']['financeiro_conta_bancaria_nova']) || $_SESSION['adm_user'] == 1){
        echo "<a href='?op=criarContaBancaria'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button'  type='button'>
                <span class='ui-button-text'>+ Nova</span>
            </a>";
    }

    echo "</div><br><br><br>";


    $sql = "SELECT
				c.ID as id_conta,
				o.forma as banco,
        o.COD_BANCO,
				c.AGENCIA as agencia,
				c.NUM_CONTA as conta,
				e.nome as ur,
				tipo_conta_bancaria.nome as tipo
			FROM
				contas_bancarias as c
			INNER JOIN origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id)
			INNER JOIN empresas as e ON (c.UR = e.id)
			LEFT JOIN tipo_conta_bancaria ON tipo_conta_bancaria.id = c.TIPO_CONTA_BANCARIA_ID
			where
            e.id in {$_SESSION['empresa_user']}
			ORDER BY
			banco
			";
    $result = mysql_query($sql);

    echo "<div id='resultado_contas'>";
    echo "<table id='busca_contas' class='mytable' width='100%'>";
    echo "<thead><tr>";
    echo "<th style='solid #000;'>Tipo</th>";
    echo "<th style='solid #000;'>Banco</th>";
    echo "<th style='solid #000;'>Agencia</th>";
    echo "<th style='solid #000;'>Conta</th>";
    echo "<th style='solid #000;'>UR</th>";
    echo "<th style='solid #000;'>OP&Ccedil&Otilde;ES</th>";
    echo "</tr></thead>";
    
    while($row = mysql_fetch_array($result)){
        $tipo = isset($row['tipo']) && !empty($row['tipo']) ? $row['tipo'] : htmlentities('<não cadastrado>');
        echo "<tr>
                <td>{$tipo}</td>
                <td>{$row['banco']}</td>
                <td>{$row['agencia']}</td>
                <td>{$row['conta']}</td>
                <td>{$row['ur']}</td>
                <td>
                
                    <a href=?op=extrato_bancario&id_conta={$row['id_conta']}><img src='../utils/details_16x16.png' title='Extrato' border='0'></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_bancaria']['financeiro_conta_bancaria_editar']) || $_SESSION['adm_user'] == 1){
                        echo "<a href=?op=editar_conta&id_conta={$row['id_conta']}><img src='../utils/edit_16x16.png' title='Editar Conta' border='0'></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    }
                    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_bancaria']['financeiro_conta_bancaria_tarifa']) || $_SESSION['adm_user'] == 1){
                    echo "<a href=?op=tarifas-bancarias&id_conta={$row['id_conta']}&action=listar><b><img src='../utils/tax_16x16_novo.png' title='Tarifas' border='0'></b></a>";
                    }
                echo "</td>
            </tr>";
    }
    echo "</table>
    <br>
    <div style='text-align:center'>
    <button id='voltar_' type = 'button' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' ><span class='ui-button-text'>Voltar</span></button>

    </div>";

    echo "</div>";

}

function criarContaBancaria(){
    
    echo "<h1><center>Criar Conta Bancária</center></h1>";
    echo "<div id='div-bancos'>";
    echo "<form>";
    $outros = 1;

    $tiposConta = TipoContaBancaria::getAll();
    $optionsTipo = '<option value="-1">SELECIONE</option>';
    foreach ($tiposConta as $tipoConta)
        $optionsTipo .= sprintf('<option value="%s">%s</option>', $tipoConta['id'], $tipoConta['nome']);
    echo "<p>
            <b>Tipo:&nbsp;&nbsp;&nbsp;&nbsp;</b>
            <select class='COMBO_OBG' name='tipo_conta' id='tipo_conta' style='background-color:transparent;font-size:10px;'>{$optionsTipo}</select>
          </p>";

    echo "<p><b>Banco:&nbsp;&nbsp;&nbsp;&nbsp;</b><input type='text' name='busca-banco' class='OBG' id='busca-banco' size='80' />";
    echo "<input type='hidden' id='id_banco' value='-1' />";
    echo "<input type='hidden' id='status' value='-1' />";
    echo "<p><b>Agencia:&nbsp;</b><input id='agencia' type='text' name='agencia' class='OBG'/>";
    echo "<p><b>Conta:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b><input id='conta' type='text' name='conta' class='OBG'/>";
    $ur= $_SESSION['empresa_principal'];
    echo "<p><b>UR:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>".ur($_SESSION["empresa_principal"]);
    $cond_sql="";
    
    /*if($ur==1){
        echo "<p><b>UR:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>".ur($_SESSION["empresa_principal"]);
        $cond_sql="";
    }else{
        echo"<input name='empresa2' class='empresa' type=hidden value={$ur}> </input>";
        $cond_sql ="  where c.UR ={$ur}";
    }*/

    echo "<p><b>Saldo Inicial:&nbsp;</b><input id='saldo_inicial' type='text' name='saldo_inicial' maxlength='15' class='OBG valor-negativo' />";
    echo "<p><b>Data de Abertura</b><input id='entrada' type='text' name='inicio' maxlength='10' class='OBG' size='10' />";
    echo "<p>
					<b>Conta Contábil</b>
					<div style='position: relative; float: left; width: 100%; display: block; font-size: 9px;'>
						<span style='position: relative; float: left;'>Selecione uma conta: </span>
					</div>
						<select data-placeholder='Selecione uma conta...' name='ipcc' id='ipcc'>";
    $dados = listar_ipcc();
    foreach($dados as $nivel1 => $nivel2){
        echo "	<optgroup label='{$nivel1}'>";
        foreach($nivel2 as $n2 => $nivel3){
            echo "	<optgroup label='{$n2}'>";
            foreach($nivel3 as $n3 => $nivel4){
                echo "	<optgroup label='{$n3}'>";
                foreach($nivel4 as $n4 => $nivel5){
                    echo "	<optgroup label='{$n4}'>";
                    foreach($nivel5 as $id => $n5){
                        echo "	<option value='{$id}'>{$n5}</option>";
                    }
                    echo  	 "	</optgroup>";
                }
                echo  	 "	</optgroup>";
            }
            echo  	 "	</optgroup>";
        }
        echo  	 "	</optgroup>";
    }
    echo "		</select>
                    </p>";
                    
    echo "<br>
                <div style='text-align:center'>
                        <button id='salvar-conta' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button' type='button'>
                         <span class='ui-button-text'>Salvar</span>
                         </button>
                         &nbsp;&nbsp;
                         <button id='voltar_' type = 'button' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' >
                         <span class='ui-button-text'>Voltar</span>
                         </button>
                </div>
";
    echo "</form>";
    echo "</div>";

}

function editar_conta($id_conta){
    echo "<div id='div-bancos'>
            <h1><center>Editar Conta Bancária</center></h1>";

    $sql = "SELECT
				c.ID as id_conta,
				o.forma as banco,
                                o.COD_BANCO,
				c.AGENCIA as agencia,
				c.NUM_CONTA as conta,
                                c.ORIGEM_FUNDOS_ID,
				e.nome as ur,
				tipo_conta_bancaria.id as tipoContaId,
				tipo_conta_bancaria.nome as tipoContaNome,
				c.IPCC
			FROM
				contas_bancarias as c
				INNER JOIN origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id)
				INNER JOIN empresas as e ON (c.UR = e.id)
				LEFT JOIN tipo_conta_bancaria ON tipo_conta_bancaria.id = c.TIPO_CONTA_BANCARIA_ID
                          where
                              c.ID=$id_conta

			";

    $result = mysql_query($sql);
    while($row = mysql_fetch_array($result)){
        $banco=$row['COD_BANCO']." - ".$row['banco'];

        $tiposConta = TipoContaBancaria::getAll();
        $optionsTipo = '<option value="-1">SELECIONE</option>';
        foreach ($tiposConta as $tipoConta) {
            $selected = $row['tipoContaId'] == $tipoConta['id'] ? 'selected="selected"' : null;
            $optionsTipo .= sprintf('<option value="%s" '.$selected.'>%s</option>', $tipoConta['id'], $tipoConta['nome']);
        }
        echo "<p>
            <b>Tipo:&nbsp;&nbsp;&nbsp;&nbsp;</b>
            <select class='COMBO_OBG' name='tipo_conta' id='tipo_conta' style='background-color:transparent;font-size:10px;'>{$optionsTipo}</select>
          </p>";

        echo "<p><b>Banco:&nbsp;&nbsp;&nbsp;&nbsp;</b><input type='text' name='busca-banco' value='{$banco}' class='OBG' id='busca-banco' size='80' />";
        echo "<input type='hidden' id='id_banco' value='{$row['ORIGEM_FUNDOS_ID']}' />";
        echo "<input type='hidden' id='id_conta' value='$id_conta' />";

        echo "<p><b>Agencia:&nbsp;</b><input id='agencia' type='text' name='agencia' value='{$row['agencia']}' class='OBG'/>";
        echo "<p><b>Conta:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b><input id='conta' type='text' name='conta' value='{$row['conta']}' class='OBG'/>";
        echo "<p>
					<b>Conta Contábil</b>
					<div style='position: relative; float: left; width: 100%; display: block; font-size: 9px;'>
						<span style='position: relative; float: left;'>Selecione uma conta: </span>
					</div>
						<select name='ipcc' id='ipcc'>";
        $dados = listar_ipcc();
        foreach($dados as $nivel1 => $nivel2){
            echo "	<optgroup label='{$nivel1}'>";
            foreach($nivel2 as $n2 => $nivel3){
                echo "	<optgroup label='{$n2}'>";
                foreach($nivel3 as $n3 => $nivel4){
                    echo "	<optgroup label='{$n3}'>";
                    foreach($nivel4 as $n4 => $nivel5){
                        echo "	<optgroup label='{$n4}'>";
                        foreach($nivel5 as $id => $n5){
                            echo "	<option " . ($row['IPCC'] == $id ? 'selected' : '') . " value='{$id}'>{$n5}</option>";
                        }
                        echo  	 "	</optgroup>";
                    }
                    echo  	 "	</optgroup>";
                }
                echo  	 "	</optgroup>";
            }
            echo  	 "	</optgroup>";
        }
        echo "		</select>
									</p>";
        echo "<div style='text-align:center'>
                    <button id='editar-conta' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button'  type='button'>
                    <span class='ui-button-text'>Salvar</span></button>
                    &nbsp;&nbsp;
                    <button id='voltar_' type = 'button' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' >
                    <span class='ui-button-text'>Voltar</span>
                    </button>
            </div>";

    }
    echo '</div>';

}

function menu_plano_contas(){
    $var  = "<p><a href='plano-contas/index.php?action=ipcf'>Financeiro</a>";
    $var .= "<p><a href='plano-contas/index.php?action=ipcc'>Contábil</a>";
    $var .= "<p><a href='/financeiros/retencoes/?action=configuracao-retencoes'>Configuração de Naturezas de Retenções </a>";
    $var .= "<p><a href='dominio-sistemas/'>Gerar Arquivo Domínio Sistemas</a>";
    $var .= "<p><a href='/financeiros/totvs/'>Gerar Arquivo TOTVS</a>";
    return $var;
}

function menu_conciliacao(){
    $var  = "<p><a href='?op=conciliacao'>Manual</a>";
    $var .= "<p><a href='/financeiros/conciliacao-bancaria/?action=form-conciliacao'>Automatizada</a>";
    return $var;
}

if(isset($_POST['query'])){
    switch($_POST['query']){
        case "ipcg4":
            ipcg4($_POST['cod'],NULL,NULL,$_POST['tipo_nota'],$_POST['ipcf2'],null);
            break;
        case "ipcg3":
            ipcg3($_POST['cod'],NULL,$_POST['tipo_nota']);
            break;
        case "empresa":
            empresa($_POST['tipo']);
            break;
        case "assinatura":
            assinatura($_POST['tipo']);
            break;
        case "ipcf2":
            ipcf2($_POST['tipo']);

    }

}
