<?php

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);


$routes = [
    'GET'  => [
        '/financeiros/compensacao/?action=compensar-parcela' => '\App\Controllers\CompensacaoController::criarCompensacao',
        "/financeiros/compensacao/?action=pesquisar-parcelas-compensar" => '\App\Controllers\CompensacaoController::pesquisarParcelaParaCompensar',
    ],

    'POST' => [      
        "/financeiros/compensacao/?action=salvar-compensacao" => '\App\Controllers\CompensacaoController::salvarCompensacao',
        "/financeiros/compensacao/?action=cancelar-compensacao" => '\App\Controllers\CompensacaoController::cancelarCompensacao',
    ]

    ];
$args = isset($_GET) && !empty($_GET) ? $_GET : null;
try {
    $app = new App\Application;
    $app->setRoutes($routes);
    $app->dispatch(METHOD, URI,$args);
} catch(App\BaseException $e) {
    echo $e->getMessage();
} catch(App\NotFoundException $e) {
    http_response_code(404);
    echo $e->getMessage();
}