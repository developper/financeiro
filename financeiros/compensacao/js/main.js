$(function($) {
    $('.chosen-select').chosen();

    $('#buscar-faturas-enviadas').click(function () {
        if(!validar_campos('div-pesquisar-faturas-enviadas')){
            return false;
        }
    });

    $(".valor").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $("#data-vencimento, #data-vencimento-real, #data-emissao").live('blur', function () {
        var dataEmissao = new Date($("#data-emissao").val().split("/").reverse().join("-")+' 01:00:00');
        var vencimento = new Date($("#data-vencimento").val().split("/").reverse().join("-")+' 01:00:00');
        var vencimentoReal = new Date($("#data-vencimento-real").val().split("/").reverse().join("-")+' 01:00:00');
        if(vencimento < dataEmissao || vencimentoReal < dataEmissao) {
            
            vencimento < dataEmissao ? $("#data-vencimento").val("") : '';                
            vencimentoReal < dataEmissao ? $("#data-vencimento-real").va(''): '';
            
            alert("Datas de vencimentos não podem ser menores que da emissão");
            return false;
        }
        vencimentoReal = validar_vencimento_real(vencimento, vencimentoReal);        
        vencimentoReal = moment(vencimentoReal).format('YYYY-MM-DD');        
        $("#data-vencimento-real").val(vencimentoReal);       
        
    });

    $(".marcar-todos").live('click', function () {
        $("#tbody-parcelas-result")
            .find('input:checkbox')
            .not(this)
            .prop('checked', this.checked);
    });
    

    $('#pesquisar-parcelas').on('click', function(){
        var impostos_adicionados = [];
        var tipo_nota = $("#tipo-nota").val();
        var $parcelasFaturaElem = $(".valor-parcela-compensar");
        if($parcelasFaturaElem.length > 0) {
            $parcelasFaturaElem.each(function () {
                impostos_adicionados.push($(this).attr('parcela-id'));
            })
        }

        var tr_parcela = $('#tr-parcela').val();
        var inicio = $('#inicio').val();
        var fim = $('#fim').val();
        if((tr_parcela != '' && tr_parcela != '0') || (inicio != '' && fim != '')){
            var data = $("#form-pesquisar-parcelas").serialize();
            data += "&tipo_nota=" + tipo_nota;
            data += "&tr=" + tr_parcela;

            $.ajax({
                url: "/financeiros/compensacao/?action=pesquisar-parcelas-compensar",
                type: 'GET',
                data: data,
                success: function (response) {
                    if(response) {
                        response = JSON.parse(response);
                        if (Object.keys(response).length > 0) {
                            var htmlTr = '';
                            for (var i = 0; i < response.length; i++) {
                                var dataVencimento = response[i]['data_vencimento'].split('-').reverse().join('/');
                                var dataVencimentoReal = response[i]['data_vencimento_real'].split('-').reverse().join('/');

                                var dataEmissao = response[i]['dataEntrada'].split('-').reverse().join('/');
                                var nomeFornecedor = response[i]['nome_fornecedor'];
                                var ur = response[i]['ur'];
                                var num_parcela = response[i]['num_parcela'];
                                var valorPagar = float2moeda(response[i]['valor_pagar']);
                                var valorCompensada = float2moeda(response[i]['valor_compensada']);
                               
                                var valorACompensar= parseFloat(response[i]['valor_pagar']) - parseFloat(response[i]['valor_compensada']);
                                 valorACompensar= float2moeda(valorACompensar);
                                var sigla = response[i]['sigla'];
                                var idParcela = response[i]['id_parcela'];
                                var idNota = response[i]['id_nota'];
                                var codigo = response[i]['codigo'];
                                var strikethrough = '';
                                var disabled = '';
                                var checked = '';

                                if($.inArray(idParcela, impostos_adicionados) != '-1') {
                                    strikethrough = " style='text-decoration: line-through;' ";
                                    disabled = ' disabled ';
                                    checked = ' checked ';
                                }

                                htmlTr += "<tr class='tr-parcelas-buscar' " + strikethrough + ">" +
                                    "<td class='text-center'>" +
                                    "<input type='checkbox' " +
                                    disabled +
                                    checked +
                                    "class='check-adicionar-parcela' " +
                                    "data-vencimento='" + dataVencimento + "'" +
                                    "data-vencimento-real='" + dataVencimentoReal + "'" +
                                    "data-emissao='" + dataEmissao + "'" +
                                    "nome-fornecedor='" + nomeFornecedor + "'" +
                                    "valor-pagar='" + valorPagar + "'" +
                                    "sigla='" + sigla + "'" +
                                    "id-nota='" + idNota + "'" +
                                    "codigo='" + codigo + "'" +
                                    "tr-parcela='" + num_parcela + "'" +
                                    "id-parcela='" + idParcela + "'" +
                                    "valor-compensado='" + valorCompensada + "'" +
                                    "valor-a-compensar='" + valorACompensar + "'" +
                                    "/>" +
                                    "</td>" +
                                    "<td class='text-center'>" + codigo + "</td>" +
                                    "<td class='text-center'>" + num_parcela + "</td>" +
                                    "<td>" + ur.toUpperCase() + "</td>" +
                                    "<td>" + nomeFornecedor + "</td>" +
                                    "<td class='text-center'>" + dataEmissao + "</td>" +
                                    "<td class='text-center'>" + dataVencimento + "</td>" +
                                    "<td class='text-center'>" + dataVencimentoReal + "</td>" +
                                    "<td>" + valorPagar + "</td>" +
                                    "<td>" + valorCompensada + "</td>" +
                                    "<td>" + valorACompensar + "</td>" +
                                    "<td class='text-center'>" + sigla + "</td>" +
                                    "</tr>";
                            }

                            $("#tbody-parcelas-result").html(htmlTr);
                            $('#modal-add-parcelas').modal();
                        } else {
                            alert("Nenhuma parcela encontrada para os filtros escolhidos");
                        }
                    } else {
                        alert("Nenhuma parcela encontrada para os filtros escolhidos!");
                    }
                }
            });
        }else{
            alert("Algum campo obrigatório não foi informado.");
        }
    });

    $('#adicionar-parcela').live('click', function () {
        var aux = 0;
        var faturaLista = 0;
        parcelasId = [];
        var valor_total = parseFloat($("#valor-total").val());
       

        $('.id_fatura').each(function () {
            parcelasId.push($(this).attr('fatura'));
        });

        $("#info-fatura-nota").remove();
        $('.check-adicionar-parcela:not(:disabled)').each(function () {
            if($(this).is(':checked')){
                var parcelasID = $(this).attr('idParcela');
                aux++;
                if(!parcelasId.includes(parcelasID)) {
                    var valor_a_compensar = parseFloat(
                        replaceValorMonetarioFloat($(this)
                            .attr('valor-a-compensar'))                            
                    );
                    valor_total = valor_a_compensar + valor_total;
                    
                    $("#tbody-compensar-parcela").append(
                        "<tr class='tr-parcela-compensar'>" +
                        "   <td>" +
                        "       <button title='Remover Parcela' type='button' role='button'" +
                        "               class='btn btn-danger remover-parcela'>" +
                        "           <i class='fa fa-times'></i>" +
                        "       </button>" +
                        "       <input type='hidden' valor-a-compensar='" + valor_a_compensar + "' name='parcelas[" +   $(this).attr('id-parcela') + "][valor]' " +
                        "             parcela-id ='"+ $(this).attr('id-parcela') +"' class='valor-parcela-compensar' value='"+ $(this).attr('valor-a-compensar')+"' />"+
                        "       <input type='hidden'  name='parcelas[" +   $(this).attr('id-parcela') + "][nota]' " +
                        "             value='"+ $(this).attr('id-nota')+"' />"+
                        "       <input type='hidden'  name='parcelas[" +   $(this).attr('id-parcela') + "][idParcela]' " +
                        "             value='"+ $(this).attr('id-parcela')+"' />"+                        
                        "   </td>"+
                        "   <td>" + $(this).attr('codigo') + " - "+ $(this).attr('tr-parcela') +  "</td>" +
                        "   <td>" + $(this).attr('nome-fornecedor') + "</td>" +
                        "   <td>" + $(this).attr('data-vencimento') + "</td>" +
                        "   <td>" + $(this).attr('data-vencimento-real') + "</td>" +
                        "   <td>" + $(this).attr('sigla') + "</td>" +
                        "   <td>R$ " + $(this).attr('valor-a-compensar')+ "</td>" +
                        "</tr>"
                    );
                }else{
                    faturaLista++;
                }
            }
        });
        $("#valor-total-text").text("R$ " + float2moeda(valor_total));
        $("#valor-total").val((valor_total).toFixed(2));
        $(".valor").priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        if(aux == 0){
            alert('Selecione uma Parcela antes de tentar adicionar.');
            return false;
        }
        $("#tbody-fatura-result").html('');
        $('#modal-add-fatura').modal('hide');
    });

    $(".remover-parcela").live('click', function () {
        $(this).closest('tr').remove();
        var valor_total = 0;
        $(".valor-parcela-compensar").each(function () {           
            var valor_pagar = parseFloat($(this).attr('valor-a-compensar'));
            valor_total += valor_pagar;
        });
        $("#valor-total-text").text("R$ " + float2moeda(valor_total));
        $("#valor-total").val(valor_total.toFixed(2));
    });

    $(".tr-parcelas-buscar td").live('click', function (event) {
        if (event.target.type !== 'checkbox') {
            $(this)
                .parent()
                .find('input:checkbox')
                .trigger('click');
        }
    });
    

    $('#finalizar-compensar-parcela').live('click', function () {
        var $inputParcelas = $(".valor-parcela-compensar");
        if(typeof $inputParcelas != 'undefined' && $inputParcelas.length >= 1) {
            var valorTotal = parseFloat($("#valor-total").val());
            var valorACompensarParcela = parseFloat($("#parcela-compensar-valor").val());
            var notaAdiantamento = $("input[name='parcelaCompensar[adiantamento]'").val();
                      
            if(notaAdiantamento == 'N' && valorTotal > valorACompensarParcela ){
                alert('Valor total das Parcelas Antecipadas não pode ser maior que o Valor da Parcela a Compensar!'+
                ' De acordo com a regra você deve dividir a parcela para ser compensada completamente.');
            return false;
            }

            if(notaAdiantamento == 'N' && valorTotal < valorACompensarParcela ){
                alert('Valor total das Parcelas Antecipadas não pode ser menor que o Valor da Parcela a Compensar!'+
                ' De acordo com a regra você deve dividir a parcela para ser compensada completamente.');
            return false;
            }
            
           if(valorTotal > valorACompensarParcela){
            alert('Valor total das parcelas não pode ser maior que Valor a Compensar da Parcela!');
            return false;

           }
           var data = $('#form-finalizar-compensar-parcelas').serialize();
           
           
            $.ajax({
                url: "/financeiros/compensacao/?action=salvar-compensacao",
                type: 'POST',
                data: data,
                success: function (response) {
                    response = JSON.parse(response);

                    if(response['tipo'] != 'erro') {
                        alert("Parcelas compensadas com sucesso.");
                        window.location.reload();
                    } else {
                        alert(response['msg']);
                        return false;
                    }
                }
            });
        
    } else {
        alert('Para compensar, é necessário adicionar uma ou mais parcelas!');
    }
    });

    

    $("#desfazer-nota-aglutinacao").live('click', function () {
        var $this = $(this);
        var nota_id = $this.attr('id-nota-aglutinacao');
        if(confirm('Deseja realmente desfazer essa nota de aglutinação?')) {
            $.ajax({
                url: "/financeiros/fatura-imposto/?action=desfazer-nota-aglutinacao",
                type: 'GET',
                data: {nota_id: nota_id},
                success: function (response) {
                    if (response == '1') {
                        alert('Nota desfeita com sucesso!');
                        $this.parents('tr').remove();
                    } else if (response == 'processada') {
                        alert('Essa nota já foi processada/conciliada! Desfaça o processamento e/ou conciliação primeiro para depois desfazê-la!');
                    } else if (response == '0' || response == 'false') {
                        alert('Houve um problema ao desfazer a nota de aglutinação! Entre em contato com o TI!');
                    }
                }
            });
        }
    });

    function float2moeda(num) {
        x = 0;
        if(num<0) {
            num = Math.abs(num);
            x = 1;
        }
        if(isNaN(num)) num = "0";
        cents = Math.floor((num*100+0.5)%100);
        num = Math.floor((num*100+0.5)/100).toString();
        if(cents < 10) cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
            num = num.substring(0,num.length-(4*i+3))+'.'
                +num.substring(num.length-(4*i+3));
        ret = num + ',' + cents;
        if (x == 1) ret = ' - ' + ret;return ret;
    }

    function replaceValorMonetarioFloat(valor)
{
    return valor.replace(/\./g,'').replace(/,/g,'.')
}
});