<?php

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

// ini_set('display_erros',1);

$routes = [
    'GET'  => [
		"/financeiros/contas-antecipadas/?action=index-conta-antecipada" => '\App\Controllers\ContasAntecipadas::listar' ,  
        "/financeiros/contas-antecipadas/?action=criar-conta-antecipada" => '\App\Controllers\ContasAntecipadas::criar',
        "/financeiros/contas-antecipadas/?action=visualizar-conta-antecipada" => '\App\Controllers\ContasAntecipadas::visualizar',
        "/financeiros/contas-antecipadas/?action=editar-conta-antecipada" => '\App\Controllers\ContasAntecipadas::editar',    
    ],
    'POST' => [      
        '/financeiros/contas-antecipadas/?action=salvar-conta-antecipada' => '\App\Controllers\ContasAntecipadas::salvar',
        '/financeiros/contas-antecipadas/?action=update-conta-antecipada' => '\App\Controllers\ContasAntecipadas::update',
        '/financeiros/contas-antecipadas/?action=pesquisar-contas-antecipadas' => '\App\Controllers\ContasAntecipadas::pesquisarNotasAntecipadas',
        '/financeiros/contas-antecipadas/?action=cancelar-conta-antecipada' => '\App\Controllers\ContasAntecipadas::cancelarNota',
        "/financeiros/contas-antecipadas/?action=salvar-baixa-conta-antecipada" => '\App\Controllers\ContasAntecipadas::salvarBaixaNota',
        "/financeiros/contas-antecipadas/?action=cancelar-baixa" => '\App\Controllers\ContasAntecipadas::cancelarBaixa',
        "/financeiros/contas-antecipadas/?action=editar-competencia" => '\App\Controllers\Contas::editarCompetencia'
    ]
];
$args = isset($_GET) && !empty($_GET) ? $_GET : null;
try {
    $app = new App\Application;
    $app->setRoutes($routes);
    $app->dispatch(METHOD, URI,$args);
} catch(App\BaseException $e) {
    echo $e->getMessage();
} catch(App\NotFoundException $e) {
    http_response_code(404);
    echo $e->getMessage();
}


