<?php
$id = session_id();
if(empty($id))
    session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/mpdf/mpdf.php');
include_once('../utils/codigos.php');


use App\Models\Financeiro\Bordero;
$extension = '.xls';

if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
    $extension = '.xlsx';
}

header("Content-type: application/x-msexce");
header("Content-type: application/force-download;");
header("Content-Disposition: attachment; filename=parcelas".$extension);
header("Pragma: no-cache");
class imprimir_parcelas{

	public function buscar(
$assinatura,
$btipo,
$codFornecedor,
$codigo,
$descricao_nota,
$empresa,
$fim,
$fimE,
$fimPagamento,
$inicio,
$inicioE,	
$inicioPagamento,
$ipcg4_nota,
$numero_nota,
$select_tipo_doc_financeiro,	
$status,
$valor,
$competenciaInicio,
$competenciaFim,
$select_forma_pagamento	
	) {
		ini_set('memory_limit', '512M');

		$header = "	<style>
table {
  border-collapse: collapse;
}

table, td, th {
  border: 1px solid black;
}
</style>";

        $header .= "<table width='100%' style='border:1px solid #000;border-collapse:collapse;'>
			<thead>
				<tr>
					<th rowspan='2' style='float:left;padding:10px;width:20%;'>
					
					</th>
					<th style='float:left;padding:10px;' colspan='9'>
						<span style='font-size:16px;'>RELAT&Oacute;RIO DE PARCELAS</span>
					</th>
				</tr>
			</thead>
		</table>";
		
		$condTipoDoc = '';

		$btipo = anti_injection($btipo, 'literal');
		if ($codigo == '' && $numero_nota == '') {
            
            if($select_tipo_doc_financeiro != ''){
				$condTipoDoc = "  AND n.tipo_documento_financeiro_id = {$select_tipo_doc_financeiro} ";
			}
            
            if ($ipcg4_nota != '') {
				$rinner = " INNER JOIN rateio r ON r.NOTA_ID = n.idNotas";
				$condipcg4 = "AND (r.IPCG4 ='{$ipcg4_nota}' OR n.natureza_movimentacao = {$ipcg4_nota})";
			}
            
            if ($btipo != "t") {
	
				$condtip = " n.tipo = '$btipo' ";
			} else {
				$condtip = " n.tipo in(0,1) ";
			}
            
            if ($status != "" && !empty($status)) {
				$condst = "AND UPPER(p.status) = UPPER('{$status}') ";
			}
            
            if (!empty($codFornecedor)) {
				$condfor = "AND codFornecedor = '$codFornecedor' ";
			}
            
            if ($assinatura != '') {
				$rinner = " INNER JOIN rateio r ON r.NOTA_ID = n.idNotas";
				$condass = "AND r.ASSINATURA = '$assinatura' ";
			}
	
			//buscando pela descricao da nota --Adicionado em 06/10/2014
			
			if ($descricao_nota != '') {
				$conddesc = "AND descricao LIKE '%{$descricao_nota}%'";
			}
	
			$valor = str_replace('.', '', $valor);
			$valor = str_replace(',', '.', $valor);
			if ($valor != '') {
				$condvalor = "AND n.valor = '{$valor}'";
			}

			if ($competenciaInicio != '') {
				$i = implode("-", array_reverse(explode("/", $competenciaInicio)));
				$condCompetenciaInicio = "AND n.data_competencia >= '{$i}'";
			}
            
            if ($competenciaFim != '') {
				$f = implode("-", array_reverse(explode("/", $competenciaFim)));
				$condCompetenciaFim = "AND n.data_competencia <= '{$f}'";
			}

			if ($inicio != '') {
				$i = implode("-", array_reverse(explode("/", $inicio)));
	
				$condini = "AND p.vencimento_real >= '{$i}'";
            }
            
			if ($inicioE != '') {
				$i = implode("-", array_reverse(explode("/", $inicioE)));
	
				$condiniE = "AND n.dataEntrada >= '{$i}'";
            }
            
			if ($inicioPagamento != '' && !empty($inicioPagamento)) {
				$i = implode("-", array_reverse(explode("/", $inicioPagamento)));
	
				$condiniPagamento = "AND p.DATA_PAGAMENTO >= '{$i}'";
            }
            
			if ($fim != '') {
	
				$f = implode("-", array_reverse(explode("/", $fim)));
				$condfim = "AND p.vencimento_real <= '{$f}'";
            }
            
			if ($fimE != '') {
				$f = implode("-", array_reverse(explode("/", $fimE)));
	
				$condfimE = "AND n.dataEntrada <= '{$f}'";
            }
            
			if ($fimPagamento != '') {
				$f = implode("-", array_reverse(explode("/", $fimPagamento)));
	
				$condFimPagamento = "AND p.DATA_PAGAMENTO <= '{$f}'";
			}
			if ($select_forma_pagamento != '') {	
				$condFormaPagamento = "AND n.forma_pagamento = '{$select_forma_pagamento}'";
			}
            $condemp ='';
            
        if(!empty($empresa)){
            $condemp  = "AND n.empresa = $empresa";
        }

        $condur = " AND n.empresa in {$_SESSION['empresa_user']}";

        $sql= "select 
		n.*,
		{$r}
		p.*,
		p.DATA_PAGAMENTO as data_pagamento,
		f.razaoSocial,
		e.nome as ur,
		p.status as pst,
		p.vencimento as pven, 
		p.valor as pval,
		p.vencimento_real as parcela_vencimento_real,
		pl.COD_N4 as cod_natureza,
		pl.N4 as natureza,
		ap.forma
	from 
		notas as n inner join 
		fornecedores as f on (f.idFornecedores = n.codFornecedor) inner join
		empresas as e on (n.empresa = e.id) inner join 
		parcelas as p on (n.idNotas = p.idNota) inner join
		plano_contas as pl on (n.natureza_movimentacao = pl.ID) 
		left join aplicacaofundos as ap on (n.forma_pagamento = ap.id) 
		{$rinner} 
		
	where
	p.status <> 'CANCELADA' and 
	n.inserida_caixa_fixo <> 'S' AND
	
		$condcod 
		$condnumero_nota 
		$condtip 
		$condass 
		$condemp
		$condfim
		$condfimE 
		$condfor
		$condini
		$condiniE		
		$condipcg4 
		$condst
		$condvalor
		$condiniPagamento
		$condFimPagamento 
		$condur
		$conddesc
		$condTipoDoc
		$condCompetenciaInicio
		$condCompetenciaFim
		$condFormaPagamento
	Group by 
		idNOTAS,p.id,p.tr
	order by 
		pven, razaoSocial ASC";
        $i=0;
        $idn=0;
        $idnp=0;        

        $result = mysql_query($sql);
        $resultado = mysql_num_rows($result);


        if($resultado != 0){
            $t = "<table>
		<tr>
			<td colspan='11'>
				<p style='text-align: right;'>{$resultado} reguistro(s) encontrado(s)
			</td>
		</tr>";

            $t .= "<table  width=100% >
		<tr bgcolor='#b3b3b3'>
		<td>FOMRA PAGAMENTO</td>
			<td>EMISSÃO</td>
			<td>COMPETÊNCIA</td>
			<td>DATA PAGAMENTO</td>
			<td>VENCIMENTO</td>
			<td>VENC. REAL</td>
			<td>FORNECEDOR</td>
			<td>TIPO DOC</td>
			<td>UR</td>
			<td>COD NATUREZA</td>
			<td>NATUREZA PRINCIPAL</td>
			<td>STATUS</td>
			<td>COMPENSADA</td>
			<td>BORDERO</td>
			<td>TR</td>
			<td>COD NOTA</td>
			<td>VALOR</td>
			<td>VALOR PARA CALCULO</td>
			<td>".utf8_decode('OBSERVÇÃO')."</td>		
		</tr>";
            $total = 0;

            while ($row = mysql_fetch_array($result)) {
                $parcelaBordero = 'Não';
                $parcelaResponseBordero =  Bordero::getAllParcelasBordero($row['id']);

                if(!empty($parcelaResponseBordero)){
                    foreach ($parcelaResponseBordero as $parcela_bordero) {
                        $resposneBordero = current(Bordero::getBorderoById($parcela_bordero['bordero_id']));
                        if ($resposneBordero['ativo'] == 's') {
                            $parcelaBordero = $parcela_bordero['bordero_id'];
                        }
                    }
                }

                if ($row['tipo'] != '1') {
                    ///se parcela j� for processada pega valor pago caso contrario pega valor
                    if ($row['status'] == 'Processada') {
                        $condval = "
                    <td>R$ " . number_format($row['VALOR_PAGO'], 2, ',', '.') . "</td>";
                        $valor = $row['VALOR_PAGO'];
                    } else {
                        $condval = "
                    <td>R$ " . number_format($row['pval'], 2, ',', '.') . "</td>";
                        if ($row['status'] != 'Cancelada') {
                            $valor = $row['pval'];
                        }
                    }
                } else {
                    if ($row['status'] == 'Processada') {
                        $condval = "
                    <td style=' color:red;'>R$ - " . number_format($row['VALOR_PAGO'], 2, ',', '.') . "</td>";
                        $valor = -1 * $row['VALOR_PAGO'];
                    } else {
                        $condval = "
                    <td style=' color:red;'>R$ - " . number_format(($row['pval'] + $row['JurosMulta'] + $row['OUTROS_ACRESCIMOS']) - $row['desconto'], 2, ',', '.') . "</td>";
                        if ($row['status'] != 'Cancelada') {
                            $valor = -1 * (($row['pval'] + $row['JurosMulta'] + $row['OUTROS_ACRESCIMOS']) - $row['desconto']);
                        }
                    }
                }

                if ($i++ % 2 == 0) {
                    $cor = "#e6e6e6";
                } else {
                    $cor = "";
                }
                $data_pgto = implode("/", array_reverse(explode("-", $row['DATA_PAGAMENTO'])));
                if ($data_pgto == '00/00/0000') {
                    $data_pgto = "";
                }
                $compensada = 'NÃO';
                if($row['compensada'] <> 'NAO'){
                    $compensada = $row['compensada'];
                    if($row['VALOR_PAGO'] == 0 && $row['status'] == 'Processada' ){
                        $condval = "
                                <td style='font-size:12px; color:red;'>R$ - " . number_format($row['valor_compensada'], 2, ',', '.') . "</td>";
                    }
                }
			
			$msg ='';
			
			if(
					$row['status'] == 'Processada' && 
					($row['nota_id_fatura_aglutinacao'] != '' || $row['nota_id_fatura_aglutinacao'] != 0)
				){
					$msg = "Baixada na TR:".$row['nota_id_fatura_aglutinacao'];
					$valor = 0;
				}elseif($row['status'] == 'Processada' && $row['perda'] == 'S' ){
					$msg = "Baixada por infome de Perda";
					$valor = 0;
				}elseif($row['status'] == 'Processada' && $row['VALOR_PAGO'] == 0){
					$msg = "Baixada por Compensação";
					$valor = 0;
				};

				$corFonte = $valor >=0 ? '' : "style='color:red;'";
				$total += $valor;
				$data_competencia = implode("/", array_reverse(explode("-", $row['data_competencia'])));
				$emissao = implode("/", array_reverse(explode("-", $row['dataEntrada'])));

			$t .= "<tr bgcolor='{$cor}'>
			<td>".utf8_decode($row['forma'])."</td>
						<td>{$emissao}</td>
							<td>{$data_competencia} </td>
                             <td>{$data_pgto} </td>
                             <td>" . implode("/", array_reverse(explode("-", $row['pven']))) . "</td>
                             <td>" . implode("/", array_reverse(explode("-", $row['parcela_vencimento_real']))) . "</td>
                             <td>".utf8_decode($row['razaoSocial'])."</td>
                             <td>{$row['TIPO_DOCUMENTO']}</td>
							 <td>{$row['ur']}</td>
							 <td>{$row['cod_natureza']}</td>
							 <td>{$row['natureza']}</td>
                             <td>{$row['pst']}</td>
							 <td>".utf8_decode($compensada)."</td>
							 <td>".utf8_decode($parcelaBordero)."</td>
							 <td>{$row['idNotas']}-{$row['TR']}</td>
								<td>{$row['codigo']}</td>
							 {$condval}
							 <td {$corFonte} >R$ " . number_format($valor, 2, ',', '.') . "</td>
							 <td>".utf8_decode($msg)."</td>
                             </tr>";
            }
       
            $corFonteTotal = $total >=0 ? '' : "style='color:red;'";

            $t .="<tr  bgcolor='#b3b3b3'>
				<td colspan='14' style='text-align: right;'><b>Total</b></td>
				<td colspan='2' style='text-align: left;' {$corFonteTotal} >R$ " . number_format($total, 2, ',', '.') . "</td>
			</tr>";

            $t .= "</table>";
        }


            echo $header.$t.= "</form>";


            /*
            print_r($t);
            die();

            $mpdf=new mPDF('pt','A4',9);
            $mpdf->SetHeader('página {PAGENO} de {nbpg}');
            $ano = date("Y");
            $mpdf->SetFooter(strcode2utf('Mederi Sa&#250;de Domiciliar Ltda &#174; CopyRight &#169; 2011 - {DATE Y}'));
            $mpdf->WriteHTML("<html><body>");
            $flag = false;
            foreach($paginas as $pag){
            if($flag) $mpdf->WriteHTML("<formfeed>");
            $mpdf->WriteHTML($pag);
            $flag = true;
            }
            $mpdf->WriteHTML("</body></html>");
            $mpdf->Output('prescricao_avaliacao.pdf','I');
            exit;
            */



        }

        //Fim atualiza��o Eriton 31-05-2013

    }
}

$p = new imprimir_parcelas();

$p->buscar(	
	$_GET['assinatura'],
	$_GET['btipo'],
	$_GET['codFornecedor'],
	$_GET['codigo'],
	$_GET['descricao_nota'],
	$_GET['empresa'],
	$_GET['fim'],
	$_GET['fimE'],
	$_GET['fimPagamento'],
	$_GET['inicio'],
	$_GET['inicioE'],	
	$_GET['inicioPagamento'],
	$_GET['ipcg4_nota'],
	$_GET['numero_nota'],
	$_GET['select_tipo_doc_financeiro'],	
	$_GET['status'],
	$_GET['valor'],
	$_GET['competenciaInicio'],	
	$_GET['competenciaFim'],
	$_GET['select_forma_pagamento']
);
