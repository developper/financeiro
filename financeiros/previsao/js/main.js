$(function($) {
    $('.chosen-select').chosen();

   
    $(".valor").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $(".marcar-todos").live('click', function () {
        $("#tbody-bordero")
            .find('input:checkbox')
            .not(this)
            .prop('checked', this.checked);
    });

    $("#pesquisar-parcelas-previsao").click(function () {
                      
        if(!validar_campos('div-pesquisar-parcelas-previsao')){
            alert('Defina o período da busca');
            return false;
        }

        console.log($("#pesquisar-form-parcela-previsao").submit());
       
        
    });

    $('.pesquisar-parcelas-notas').on('click', function(){
        console.log('ok');
       
        var parcelaId = $(this).attr('id-parcela');        
        var data = "parcelaId=" + parcelaId;

            $.ajax({
                url: "/financeiros/previsao/?action=pesquisar-parcelas-notas",
                type: 'GET',
                data: data,
                success: function (response) {
                   
                    if(response != '0' && response != 'null') {
                        
                        obj = JSON.parse(response);                         
                        response =   obj.parcelas;
                        parcelaPrevisao = obj.parcela_previsao;
                        var valorParcelaPrevisao = float2moeda(parcelaPrevisao['valor']); 
                         
                              
                 
                        if (Object.keys(obj.parcelas).length > 0) {
                            var htmlTr = '';
                            for (var i = 0; i < response.length; i++) {
                                var dataVencimento = response[i]['vencimento_PtBr'];
                                var idNota = response[i]['tr'];
                                var codigo = response[i]['codigo'];
                                var natureza = response[i]['natureza'];
                                var idParcela = response[i]['id_parcela'];
                                var numNota = response[i]['num_nota'];
                                var valorRateio = float2moeda(response[i]['valor_rateio']);
                                var valorBaixado = float2moeda(response[i]['valor_baixa']);
                                var maxValorBaixa = response[i]['max_valor_baixa'];
                                var ur = response[i]['ur'];
                                var strikethrough = '';
                                var disabled = '';
                                var checked = '';

                                // if($.inArray(idParcela, impostos_adicionados) != '-1') {
                                //     strikethrough = " style='text-decoration: line-through;' ";
                                //     disabled = ' disabled ';
                                //     checked = ' checked ';
                                // }

                                htmlTr += "<tr>" +
                                    "<td class='text-center'>" +
                                    "<input type='checkbox' " +
                                    disabled +
                                    checked +
                                    "class='check-adicionar-parcela' " +                                    
                                    "id-parcela='" + idParcela + "'" +
                                    "/>" +
                                    "</td>" +
                                    "<td class='text-center'>" + idNota + "</td>" +
                                    "<td class='text-center'>" + codigo + "</td>" +
                                    "<td class='text-center'>" + ur + "</td>" +
                                    "<td>" + natureza+ "</td>" +                                  
                                    "<td class='text-center'>" + dataVencimento + "</td>" +
                                    "<td>" + valorRateio + "</td>" +
                                    "<td>" + valorBaixado + "</td>" +
                                    "<td class='text-center'>" + 
                                    "<input type='text' name = 'parcelas["+idParcela+"]' parla-id = '"+idParcela+"' disabled value = '0' class='valor form-control' max-valor='"+ maxValorBaixa +"' > "+
                                    "</td>" +
                                    "</tr>";
                            }
                            htmlTr += "<tr>"+
                            "<td colspan='7' class='text-right' id='parcela-previsao' total ='"+valorParcelaPrevisao+"' >TOTAL PARCELA PREVISÃO R$ "+ valorParcelaPrevisao +"</td>"+
                            "<td colspan='2' class='text-right'>TOTAL R$ <span id= 'total-baixa'>0,00</span></td>"+
                            "</tr>"
                            
                            $("#tbody-parcelas-result").html(htmlTr);
                            $('#modal-add-parcelas').modal();
                            $(".valor").priceFormat({
                                prefix: '',
                                centsSeparator: ',',
                                thousandsSeparator: '.'
                            });
                            $("#baixar-parcela").attr('parcela-previsao', parcelaPrevisao['id_parcela']);
                            $("#baixar-parcela").attr('nota-previsao', parcelaPrevisao['id_nota']);
                        } else {
                            alert("Nenhuma parcela encontrada para os filtros escolhidos");
                        }
                    } else {
                        alert("Nenhuma parcela encontrada para os filtros escolhidos!");
                    }
                }
            });
        
    });

    $(".marcar-todas-parcelas").live('click', function () {
        var checkedTodo = $(this).is(':checked');
        var valorParcelaPrevisao = $("#parcela-previsao").attr('total');
        var valorParcelaPrevisaoFloat = parseFloat(replaceValorMonetarioFloat(valorParcelaPrevisao));
        var total = 0;
        
        

        $('.check-adicionar-parcela').each(function(){
            
            if(checkedTodo){
                var maxValor =   $(this).closest('tr').find('.valor').attr('max-valor');
                var floatMaxValor = parseFloat(maxValor);
                
                
                if((total + floatMaxValor)  <= valorParcelaPrevisaoFloat){
                    total += floatMaxValor;
                    $(this).closest('tr').find('.valor').attr('disabled',false);
                    $(this).closest('tr').find('.valor').val(float2moeda(maxValor));
                    $(this).prop('checked', true);
                }else{
                    $(this).closest('tr').find('.valor').attr('disabled',true);
                    $(this).closest('tr').find('.valor').val('0,00');
                    $(this).prop('checked', false);
                }
                
                    
            }else{
                $(this).closest('tr').find('.valor').attr('disabled',true);
                 $(this).closest('tr').find('.valor').val('0,00');
                 $(this).prop('checked', false);
            }
            
            
        })
        
            $('#total-baixa').html(float2moeda(total));
        // $("#tbody-tbody-parcelas-result")
        //     .find('input:checkbox')
        //     .not(this)
        //     .prop('checked', this.checked);
    });

    $('.check-adicionar-parcela').live('click', function () {
       
        if($(this).is(':checked')){
            
            var maxValor =   $(this).closest('tr').find('.valor').attr('max-valor');
            $(this).closest('tr').find('.valor').attr('disabled',false);
            $(this).closest('tr').find('.valor').val(float2moeda(maxValor));
         
        }else{
               
            $(this).closest('tr').find('.valor').attr('disabled',true);
            $(this).closest('tr').find('.valor').val('0,00');
        }
        var total = 0
        $('.valor:not(:disabled)').each(function () {
            total += parseFloat(replaceValorMonetarioFloat($(this).val()));
        });
        let valorParcelaPrevisao = $("#parcela-previsao").attr('total');
        let valorParcelaPrevisaoFloat = parseFloat(replaceValorMonetarioFloat(valorParcelaPrevisao));
        if(total > valorParcelaPrevisaoFloat){
            alert("A soma dos valores não pode ser maior que R$ "+valorParcelaPrevisao+".");
            $(this).closest('tr').find('.valor').val('0,00');
            return false;
        }
        
        $('#total-baixa').html(float2moeda(total));
    });


   
    $('.valor').live('blur', function(){
        var total = 0;
        let valor = parseFloat(replaceValorMonetarioFloat($(this).val()));
        let maxValor = parseFloat($(this).attr('max-valor'));
        let maxValorMoeda = float2moeda(maxValor);
        let valorParcelaPrevisao = $("#parcela-previsao").attr('total');
        let valorParcelaPrevisaoFloat = parseFloat(replaceValorMonetarioFloat(valorParcelaPrevisao));
        
          
        if(maxValor < valor){
            alert("Valor não pode ser maior que R$ "+maxValorMoeda+".");
            $(this).val('0.00');
            return false;

        }
        $('.valor:not(:disabled)').each(function () {
           
            total +=  parseFloat(replaceValorMonetarioFloat($(this).val()));
        });
        if(total > valorParcelaPrevisaoFloat){
            alert("A soma dos valores não pode ser maior que R$ "+valorParcelaPrevisao+".");
            $(this).val('0.00');
            return false;

        }

        $('#total-baixa').html(float2moeda(total));
       
    });

    $("#baixar-parcela").live('click', function(){
        var total = 0;
        var idParcelaPrevisao = $(this).attr('parcela-previsao');
        var idNotaPrevisao = $(this).attr('nota-previsao');
        var dados = "parcela-previsao="+idParcelaPrevisao+"&nota-previsao="+idNotaPrevisao; 
        $('.valor:not(:disabled)').each(function () {
            
            let valor = parseFloat(replaceValorMonetarioFloat($(this).val()));
            total += valor;
            if(valor >0){
                dados += "&"+$(this).attr('name')+"="+$(this).val();
            }
        });
       
        if(total > 0){
            
            $.ajax({
                url: "/financeiros/previsao/?action=baixar-previsao",
                type: 'POST',
                data: dados,
                success: function (response) {
                    obj = JSON.parse(response); 
                    console.log(obj);
                    if(obj.tipo == 'erro'){
                        alert(obj.msg);
                        return false;
                    }
                    alert(obj.msg);
                    window.location.reload();
                }
            });
            return false;
        }
        
        alert("Você precisa informar algum valor para ser baixado.");
        return false;

        

       

    });

    $('#cancelar-parcelas-previsao').live('click', function () {
        console.log('ok');
        if(confirm('Deseja realmente cancelar as parcelas?')){
            var aux = 0;
            var dados = '';
            $('.cancelar-parcela').each(function () {
                if($(this).is(':checked')){
                    aux++;
                    dados += "&"+$(this).attr('name')+"="+$(this).val();
                   
    
                }
            });
            if(aux > 0){
                
            
                    $.ajax({
                        url: "/financeiros/previsao/?action=cancelar-parcelas-previsao",
                        type: 'POST',
                        data: dados,
                        success: function (response) {
                            obj = JSON.parse(response); 
                            console.log(obj);
                            if(obj.tipo == 'erro'){
                                alert(obj.msg);
                                return false;
                            }
                            alert(obj.msg);
                            window.location.reload();
                        }
                    });
                    return false;
              
                
                
            }
            alert("Você precisa selecinar uma parcela para poder cancelar.");

        }
        return false;
        
        });

 

    function replaceValorMonetarioFloat(valor)
{
    return valor.replace(/\./g,'').replace(/,/g,'.')
}

    function float2moeda(num) {
        x = 0;
        if(num<0) {
            num = Math.abs(num);
            x = 1;
        }
        if(isNaN(num)) num = "0";
        cents = Math.floor((num*100+0.5)%100);
        num = Math.floor((num*100+0.5)/100).toString();
        if(cents < 10) cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
            num = num.substring(0,num.length-(4*i+3))+'.'
                +num.substring(num.length-(4*i+3));
        ret = num + ',' + cents;
        if (x == 1) ret = ' - ' + ret;return ret;
    }

    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
});
