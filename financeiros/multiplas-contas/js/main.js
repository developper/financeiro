$(function($) {
    console.log('ok');
    $('.chosen-select').chosen();
    $("#table-rateio .chosen-results").css('font-size','10px');
    $("#table-rateio .chosen-single").css('font-size','10px');

    $(".mes-ano").mask('99/9999');

    $(".mes-ano").blur(function () {
        var inputValue = $(this).val();
        if(inputValue != '') {
            var momentInput = moment(inputValue, 'MM/YYYY');
            if(!momentInput.isValid()) {
                alert('Informe uma competência válida!');
                $(this).val('').focus()
                $(this).focus()
            }
            return true;
        }
    });

    

    $(".valor").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
    

    $("#adicionar-rateio").click(function(e){
        // e.preventDefault();
        $('.tr-rateio:first').clone().appendTo($('#table-rateio')); // clona o primeiro elemento e adiciona ao final do form
        $('.tr-rateio').last().find('input[type="text"]').val(0);
        $('.tr-rateio').last().find('.unidade_rateio').val('');

        // var option = $('.tr-rateio:last td').parent('td');
        // console.log(option);
        $('.chosen-select').chosen();
        $("#table-rateio .chosen-results").css('font-size','10px');
        $("#table-rateio .chosen-single").css('font-size','10px');
        $(".valor").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
        });

        // $('.tr-rateio:last .unidade_rateio').parent('td').children('div').eq(1).remove();
        $('.tr-rateio:last .natureza-rateio').parent('td').children('div').eq(1).remove();
        // $('.tr-rateio:last .assinatura-rateio').parent('td').children('div').eq(1).remove();
    });

    $(".remover-rateio").live('click', function(){
        if ($('.tr-rateio').length > 1) {
            $(this).closest("tr").remove();
          }else{
              alert('Deve existir ao menos um rateio');
          }

    });

    $("#data-vencimento, #data-emissao, #competencia").live('blur', function () {

        var dataEmissao = new Date($("#data-emissao").val().split("/").reverse().join("-")+' 01:00:00');
        var vencimento = new Date($("#data-vencimento").val().split("/").reverse().join("-")+' 01:00:00'); 
              
        var competencia = $("#competencia").val() == '' ? 'Invalid Date' : new Date($("#competencia").val().split("/").reverse().join("-")+'-01 01:00:00');
        var mesAnoEmissao = moment(dataEmissao).format('YYYY-MM');
        var mesAnoCompetencia = moment(competencia).format('YYYY-MM');
        var mesAnoVencimento = moment(vencimento).format('YYYY-MM');
     
        // if(competencia.toString() !== 'Invalid Date' && vencimento.toString() !== 'Invalid Date' && dataEmissao.toString() !== 'Invalid Date'){
        //     if(mesAnoEmissao != mesAnoCompetencia || mesAnoCompetencia != mesAnoVencimento){
        //         $("#data-emissao").val('');
        //             $("#data-vencimento").val('');
        //             $("#competencia").val('');
        //         alert("Datas de Competência, Emissão e vencimento devem estar no mesmo mês/ano.");
        //         return false;
        //     }  
        // }
        
        if(vencimento < dataEmissao) {
            vencimento < dataEmissao ? $("#data-vencimento").val("") : '';
            alert("Data de vencimento não pode ser menore que da emissão");
            return false;
        }
        vencimentoReal = validar_vencimento_real(vencimento, vencimento);
        vencimentoReal = moment(vencimentoReal).format('YYYY-MM-DD');
        $("#data-vencimento").val(vencimentoReal);

    });

    $('#valor-nota').blur(function(){
        var valor = $('#valor-nota').val();
        if($(".valor_rateio").length == 1) {
            $(".valor_rateio:first").val(valor);
            $(".percentual_rateio:first").val('100,00');
            $(".percentual_real_rateio:first").val('100,00');
                
        }
    });

    $(".valor_rateio").live('blur',function(){
        var _this = $(this);
        var val_nota = $('#valor-nota').val(); 
        if(val_nota == 0 || val_nota == NaN || val_nota == undefined){
            alert('Valor da nota ainda não foi prenchido.');
            $(this).attr('value','0,00');
            return;
        }           
        var val_inserido = parseFloat(replaceValorMonetarioFloat($(this).val()));       
        var val_auxiliar = 0;        

        $(".valor_rateio").each(function(){         
            valor_rateio = replaceValorMonetarioFloat($(this).val());
            val_auxiliar += parseFloat(valor_rateio);
        });        
        
        
       var total = parseFloat(replaceValorMonetarioFloat(val_nota));
       val_auxiliar = val_auxiliar - val_inserido;
       val_auxiliar = val_auxiliar + val_inserido;

        if(val_auxiliar.toFixed(2) > total){
            alert("Valores Rateio Maior que o valor da nota");
            $(this).attr('value','0,00');
            _this.closest('tr').find('.percentual_rateio').val(0,00);
            _this.closest('tr').find('.percentual_real_rateio').val(0);
            return;
        }

        porcentagem = (val_inserido *100)/total ;
        
        _this.closest('tr').find('.percentual_real_rateio').val(porcentagem.toFixed(5));
        porcentagemMoeda= float2moeda(porcentagem);
        _this.closest('tr').find('.percentual_rateio').val(porcentagemMoeda);

        console.log($('.percentual_real_rateio').val());
        console.log($('.percentual_rateio').val());

    });



    $('#salvar-conta').on('click', function(){

        if(validar_campos('div-form-conta')){

        var dataEmissao = new Date($("#data-emissao").val().split("/").reverse().join("-")+' 01:00:00');
        var vencimento = new Date($("#data-vencimento").val().split("/").reverse().join("-")+' 01:00:00'); 
              
        var competencia = $("#competencia").val() == '' ? 'Invalid Date' : new Date($("#competencia").val().split("/").reverse().join("-")+'-01 01:00:00');
        var mesAnoEmissao = moment(dataEmissao).format('YYYY-MM');
        var mesAnoCompetencia = moment(competencia).format('YYYY-MM');
        var mesAnoVencimento = moment(vencimento).format('YYYY-MM');
     
        // if(competencia.toString() !== 'Invalid Date' && vencimento.toString() !== 'Invalid Date' && dataEmissao.toString() !== 'Invalid Date'){
        //     if(mesAnoEmissao != mesAnoCompetencia || mesAnoCompetencia != mesAnoVencimento){
        //         $("#data-emissao").val('');
        //             $("#data-vencimento").val('');
        //             $("#competencia").val('');
        //         alert("Datas de Competência, Emissão e vencimento devem estar no mesmo mês/ano.");
        //         return false;
        //     }  
        // }

            var total_rateio = 0;
            $(".valor_rateio").each(function(){         
                valor_rateio = replaceValorMonetarioFloat($(this).val());
                console.log(valor_rateio);
                if(valor_rateio == '0.00'){
                    alert("Não deve existir rateio zerado.");
                    return false;
                }
                total_rateio += parseFloat(valor_rateio);
                console.log(total_rateio);
            });

            var val_nota = parseFloat(replaceValorMonetarioFloat($('#valor-nota').val()));
            console.log(total_rateio.toFixed(2), val_nota)
            if(val_nota != total_rateio.toFixed(2)){
                alert("Soma do rateio diferente do valor da Nota.");
                return false;
            }

            var data =   $("#form-conta").serialize();

            $.ajax({
                url: "/financeiros/multiplas-contas/?action=salvar-conta",
                type: 'POST',
                data: data,
                success: function (response) {

                    response = JSON.parse(response);

                    if(response['tipo'] != 'erro') {
                        alert("TR "+response['msg']+" criada com sucesso.");
                        window.location.reload();
                        return false;
                    } else {
                        alert(response['msg']);
                        return false;
                    }
                }
            });
        }else{
            alert("Algum campo obrigatório não foi informado.");
        }
    });
    

    function float2moeda(num) {
        x = 0;
        if(num<0) {
            num = Math.abs(num);
            x = 1;
        }
        if(isNaN(num)) num = "0";
        cents = Math.floor((num*100+0.5)%100);
        num = Math.floor((num*100+0.5)/100).toString();
        if(cents < 10) cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
            num = num.substring(0,num.length-(4*i+3))+'.'
                +num.substring(num.length-(4*i+3));
        ret = num + ',' + cents;
        if (x == 1) ret = ' - ' + ret;return ret;
    }

    function replaceValorMonetarioFloat(valor){
    return valor.replace(/\./g,'').replace(/,/g,'.')
    }
});