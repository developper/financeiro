<?php
$id = session_id();
if (empty($id))
    session_start();
include_once('../db/config.php');
Class SelectPaciente
{
    public function pacientes($id_select,$comboObg1 = NULL) {
        if ($_SESSION["empresa_principal"] != 1) {
            $result = mysql_query("SELECT c.*,s.status as st FROM clientes as c inner join statuspaciente as s on (c.status=s.id) WHERE   c.empresa ='{$_SESSION["empresa_principal"]}' ORDER BY nome");
        } else {
            $result = mysql_query("SELECT c.*,s.status as st FROM clientes as c inner join statuspaciente as s on (c.status=s.id) WHERE 1  ORDER BY nome");
        }
        if (!isset($name)){
            $name='';
        };
        $var = "<p><select name='{$name}'  data-placeholder='Selecione um Paciente' style='background-color:transparent;' class='$comboObg1 chosen-select' id='{$id_select}'>";
        if (!isset($todos)){
            $todos='';
            
        };
        if ($todos == "t")
            $var .= "<option value='todos' selected>Todos</option>";
        $var .= "<option value='-1' selected></option>";

        $aux = 0;
        $paciente_status = array();
        $status = array('1' => '::     Novo    ::', '4' => '::   Ativo/Autorizado   ::', '6' => '::     Óbito    ::');
        while ($row = mysql_fetch_array($result)) {
            $paciente = ucwords(strtolower($row['nome']));
            if (array_key_exists($row['status'], $paciente_status)) {
                $pacientes_por_status = array('idCliente' => $row['idClientes'], 'nome' => $paciente, 'codigo' => $row['codigo'], 'status' => $row['status'], 'nstatus' => $row['st']);
                # Verifica se a chave existe dentro do array $status,
                # caso não esteja, ele adiciona ao código 7 para que
                # os outros possam ser ordenados juntos no select
                if (array_key_exists($row['status'], $status)) {
                    array_push($paciente_status[$row['status']], $pacientes_por_status);
                } else {
                    if (!array_key_exists(7, $paciente_status)) {
                        $paciente_status[7] = array();
                    }
                    array_push($paciente_status[7], $pacientes_por_status);
                }
            } else {
                $paciente_status[$row['status']] = array();
                $pacientes_por_status = array('idCliente' => $row['idClientes'], 'nome' => $paciente, 'codigo' => $row['codigo'], 'status' => $row['status'], 'nstatus' => $row['st']);
                # Verifica se a chave existe dentro do array $status,
                # caso não esteja, ele adiciona ao código 7 para que
                # os outros possam ser ordenados juntos no select
                if (array_key_exists($row['status'], $status)) {
                    array_push($paciente_status[$row['status']], $pacientes_por_status);
                } else {
                    if (!array_key_exists(7, $paciente_status)) {
                        $paciente_status[7] = array();
                    }
                    array_push($paciente_status[7], $pacientes_por_status);
                }
            }
        }


        $cont = 0;
        ksort($paciente_status);
        foreach ($paciente_status as $chave => $dados) {
            #Verifica se é a primeira vez que ta rodando
            if (array_key_exists($chave, $status) && $cont == 0) {
                $var .= "<optgroup></optgroup>";
                $var .= "<optgroup label='{$status[$chave]}'>";
            }#Da segunda vez em diante passará por aqui
            elseif (array_key_exists($chave, $status) && $cont > 0) {
                $var .= "</optgroup>";
                $var .= "<optgroup></optgroup>";
                $var .= "<optgroup label='{$status[$chave]}'>";
            } elseif ($chave == 7) {
                $var .= "</optgroup>";
                $var .= "<optgroup></optgroup>";
                $var .= "<optgroup label='::     Outros     ::'>";
            }
            foreach ($dados as $chave2 => $dados2) {
                $var .= $chave;

                $var .= "<option value='{$dados2['idCliente']}' status='{$chave}' nome_status='{$dados2['nstatus']}'>({$dados2['codigo']}) {$dados2['nome']}</option>";
            }
            $cont++;
        }
        $var .= "</optgroup>";
        $var .= "</select></p>";


      echo $var;
    }
}

