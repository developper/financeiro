<?php

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);


 // ini_set('display_errors',1);
$routes = [
    'GET'  => [
        '/financeiros/informe-perda/?action=index-conta-receber-iw' => '\App\Controllers\ImportarContaReceberIWController::index',
        ],

    'POST' => [      
        '/financeiros/informe-perda/?action=salvar-informe-perda' => '\App\Controllers\InformePerdaController::salvar',
        '/financeiros/informe-perda/?action=cancelar-informe-perda' => '\App\Controllers\InformePerdaController::cancelar',
         
    ]

];
$args = isset($_GET) && !empty($_GET) ? $_GET : null;
try {
    $app = new App\Application;
    $app->setRoutes($routes);
    $app->dispatch(METHOD, URI,$args);
} catch(App\BaseException $e) {
    echo $e->getMessage();
} catch(App\NotFoundException $e) {
    http_response_code(404);
    echo $e->getMessage();
}