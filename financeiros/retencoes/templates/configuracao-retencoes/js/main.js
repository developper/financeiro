$(function($) {
    var timeout;

    $(".buscar-ipcf-retencao").typeahead({
        hint: true,
        highlight: true,
        minLength: 3
    }, {
        name: 'itens',
        display: 'value',
        limit: 100,
        source: function(query, teste, response) {
            if(timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function() {
                var x = $.getJSON('/financeiros/retencoes/?action=buscar-contas-retencoes', {
                    term: $(".buscar-ipcf-retencao:focus").val()
                }, response);
                return x;
            }, 1000);
        }
    }).on('typeahead:select', function(event, data) {
        var $ipcfRetencaoElem = $(".buscar-ipcf-retencao:focus");
        $ipcfRetencaoElem.val(data.value);
        var tipo_retencao = $ipcfRetencaoElem.attr('tipo');
        var tipo_mov = $ipcfRetencaoElem.attr('tipo-mov');
       
        $('#' + tipo_retencao + "-id-conta-" + tipo_mov).val(data.ID);
    });

    $(".buscar-ipcf-retencao").on('typeahead:change',function(){
        
        var $ipcfRetencaoElem = $(this);        
        if($ipcfRetencaoElem.val() == ''){           
            var tipo_retencao = $ipcfRetencaoElem.attr('tipo');
            var tipo_mov = $ipcfRetencaoElem.attr('tipo-mov');           
            $('#' + tipo_retencao + "-id-conta-" + tipo_mov).val('');
        }
        

    });

    $(".validar-dia-vencimento").keyup(function () {
        if(parseInt($(this).val()) > 31) {
            alert('O dia precisa ser inferior a 31!');
            $(this).val('');
        }
       
    });
    $(".validar-dia-vencimento").on('change',function(){
        if($(this).val() == ''){
            $(this).attr('value', null);
        }
    });

});
