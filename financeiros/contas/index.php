<?php

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

use \App\Controllers\Contas;

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
	"buscar-contas-ur" => [
		'GET'  => ['/financeiros/contas/?type=buscar-contas-ur' => '\App\Controllers\Contas::formBuscarParcelasPorUR'],
		'POST' => ['/financeiros/contas/?type=buscar-contas-ur' => "\App\Controllers\Contas::buscarParcelasPorUR"]
	]
];

try {
	$app = new App\Application;
	$app->setRoutes($routes[$_GET['type']]);
	$app->dispatch(METHOD, URI);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}