<?php

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

ini_set('disable_erros',1);


$routes = [
    'GET'  => [
        '/financeiros/fatura-imposto/?action=index-fatura-imposto' => '\App\Controllers\FaturaImposto::indexFaturaImposto',
        '/financeiros/fatura-imposto/?action=criar-fatura-imposto' => '\App\Controllers\FaturaImposto::criarFaturaImposto',
        "/financeiros/fatura-imposto/?action=pesquisar-nf-impostos-retidos-pendentes" => '\App\Controllers\FaturaImposto::pesquisarNFImpostosRetidosPendentes',
        "/financeiros/fatura-imposto/?action=visualizar-parcelas-aglutinadas" => '\App\Controllers\FaturaImposto::visualizarParcelasAglutinadas',
        "/financeiros/fatura-imposto/?action=desfazer-nota-aglutinacao" => '\App\Controllers\FaturaImposto::desfazerNotaAglutinacao',
    ],

    'POST' => [      
        '/financeiros/fatura-imposto/?action=pesquisar-notas-aglutinacao' => '\App\Controllers\FaturaImposto::pesquisarNotasDeAglutinacao',
         "/financeiros/fatura-imposto/?action=criar-nota-aglutinacao" => '\App\Controllers\FaturaImposto::criarNotaAglutinacao',
    ]

];
$args = isset($_GET) && !empty($_GET) ? $_GET : null;
try {
    $app = new App\Application;
    $app->setRoutes($routes);
    $app->dispatch(METHOD, URI,$args);
} catch(App\BaseException $e) {
    echo $e->getMessage();
} catch(App\NotFoundException $e) {
    http_response_code(404);
    echo $e->getMessage();
}