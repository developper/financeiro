<?php
include_once('../validar.php');
require('financeiro.php');
require('fornecedor.php');
require('busca.php');
require('transferencia.php');
require('porcento_ipcg.php');
require('cadastro_servico_prestador.php');
require('relatorios.php');
require('tarifas_bancarias.php');
require('CustoFatura.php');
require('RelatorioMargemContribuicao.php');


if(!function_exists('redireciona')){
	function redireciona($link){
		if ($link==-1){
			echo" <script>history.go(-1);</script>";
		}else{
			echo" <script>document.location.href='$link'</script>";
		}
	}
}

include($_SERVER['DOCUMENT_ROOT'].'/cabecalho.php');

/*destroi a sessao da busca de medico*/
unset($_SESSION['paciente']);
?>
<script src="financeiro.js?v=57" type="text/javascript"></script>
<script src="/utils/jquery/js/voltar.js" type="text/javascript"></script>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/cabecalho_fim.php');
if(!validar_tipo("modfin")){
	if(!validar_tipo("modlog") && $_GET['op'] === 'fornecedor'){
        redireciona('/inicio.php');
    }
}
?>
</div>
<div id="content">
<div id="right">
<?php
if(isset($_GET['op'])){
	switch($_GET['op']){
        case "menu-controle-faturamento":
            echo menuControleFaturamento();
            break;
		case "plano-contas":
			echo menu_plano_contas();
			break;
		case "tarifas-bancarias":
			echo Tarifas::gerenciarTarifas($_GET['id_conta'],$_GET['action']);
			break;
		case "atualizar-tarifas-bancarias":
			echo Tarifas::atualizarTarifas($_GET['id_tarifa'], $_GET['id_conta']);
			break;
		case "editar_conta":
			echo editar_conta($_GET['id_conta']);
			break;
		case "porcento_ipcg":
			$p = new Porcento_ipcg();
			$p->cadastrar_porcentagem_ipcg();
			break;
		case "transferencia":
		 if(isset($_GET['action'])){
			$t = new Transferencia();
			$t->criarTransferenciaBancaria();
		 }else{
			$t = new Transferencia();
			$t->pesquisarTransferenciaBancaria();
		 }
			
			break;
        case "conciliacao-menu":
            echo menu_conciliacao();
            break;
		case "conciliacao":
			echo conciliacao();
			break;
		case "listarContasBancarias":
			echo listarContasBancarias();
			break;
		case "criarContaBancaria":		       
			echo criarContaBancaria();
		break;
		case "extrato_bancario":
			echo extrato_bancario($_GET['id_conta']);
			break;
		case "notas":
			if(isset($_GET['act'])){

				switch($_GET['act']){
					case "listar":
						echo list_notas(NULL);
						break;

					case "processar":
						if(isset($_GET['confirm']) && ($_GET['confirm'] == "ok"))
							echo processar_notas();
						break;

					case "importar":
						importar($_FILES);
						break;

					case "novo":
						echo new_notas();
						break;

					case "editar":
						echo edit_notas();
						break;

                    case "usar_nova":
                        echo usar_nova_notas();
                        break;

					case "detalhes":
						echo show_notas();
						break;

					case "excluir":
						if(isset($_GET['confirm']) && ($_GET['confirm'] == "ok"))
							echo del_notas();
						break;

					case "buscar":
						//echo"erro";

						if(!isset($_POST['csv'])){
							$b = new Busca;
							$b->show();
						}
						break;

					case "lancamentos":
						$b = new Busca;
						if(isset($_FILES['extrato']))
							$b->showBalancete($_FILES['extrato']);
						else
							$b->balancete();
						break;

					case "dfc":

						echo demonstrativo_financeiro();
						break;
				}
			} else{
				echo menu_notas();
			}
			break;
		case "fornecedor":
			if(isset($_GET['act'])){

				switch($_GET['act']){
					case "listar":
						ob_start();
						list_fornecedor(NULL);
						ob_end_flush();
						break;
					case "novo":
						ob_start();
						new_fornecedor();
						ob_end_flush();
						break;
					case "editar":
						ob_start();
						edit_fornecedor();
						ob_end_flush();
						break;
					case "excluir":
						ob_start();
						del_fornecedor();
						ob_end_flush();
						break;
				}
			} else {
				echo menu_fornecedor();
			}
			break;

		case "cadastro_servico_prestador";
			$c = new CadastroServicoPrestador;
			$c->formCadastroServico($_GET['idFornecedor']);
			break;
		case "relatorios";
			$r = new RelatoriosFinanceiro;
			$r-> menuRelatoriosFinanceiro();
			break;
		case "relatorio-analito-fluxo-caixa";
			$r = new RelatoriosFinanceiro;
			$r->relatorioAnaliticoFluxoCaixa();
			break;
		case "relatorio-fluxo-caixa-realizado";
			$r = new RelatoriosFinanceiro;
			$r->relatorioFluxoCaixaRealizado();
			break;
		case "relatorio-custos";
			$r = new RelatoriosFinanceiro;
			$r->relatorioCustos();
			break;
                case "pesquisarCustoFatura":
                        $fc = new CustoFatura ;
                        $fc->pesquisarFaturas();
                     break; 
                 case "cadastrarCustoFatura":
                        $fc = new CustoFatura ;
                        $fc->cadastrarCustoFatura($_GET['fatura']);
                     break;
		case "relatorio-margem-contribuicao";
			  $r = new RelatorioMargemContribuicao;
			$r->pesquisarRelatorioMargemCusto();
	}
} elseif(isset($_GET['brf'])){
	ob_start();
	if($_GET['brf'] <> "")
		list_fornecedor($_GET['brf']);
	else
		list_fornecedor(NULL);
	ob_end_flush();
}elseif(isset($_GET['brn'])){
	if($_GET['brn'] <> "")
		echo list_notas($_GET['brn']);
	else
		echo list_notas(NULL);
}else {
	include('content.php');
}
?>
</div>
<div id="left">
<?php include($_SERVER['DOCUMENT_ROOT'].'/painel_login.php'); ?>
<!-- <div class="box">
  <ul>
   <li><a href='?op=fornecedor'>Fornecedores/Clientes</a></li>
   <ul>
    <li><a href='?op=fornecedor&act=listar'>Listar</a></li>
    <li><a href=?op=fornecedor&act=novo>Novo Fornecedore/Cliente</a></li>
   </ul>
   <li><a href='?op=notas'>Contas a Pagar/Receber</a></li>
   <ul>
     <li><a href='?op=notas&act=listar'>Buscar Contas a Pagar/Receber</a></li>
     <li><a href=?op=notas&act=buscar>Buscar Parcelas a Pagar/Receber</a></li>
     <li><a href=?op=notas&act=novo>Nova conta a Pagar/Receber</a></li>
		 <li><a href='/financeiros/contas/?type=buscar-contas-ur'>Buscar Contas a Pagar/Receber da UR <?= strtoupper($_SESSION['nome_empresa']) ?></a></li>
  </ul>

    
    <li><a href='?op=conciliacao'> Concilia&ccedil;&atilde;o Banc&aacute;ria</a></li>
    <li><a href='?op=bancos'>Bancos</a></li>
    <li><a href='?op=transferencia'> Transf&ecirc;rencia Bancaria</a></li>
    <li><a href='?op=relatorios'>Relatórios</a></li>
    <ul>
        <li><a href='?op=relatorio-analito-fluxo-caixa'>Relatório Analítico de Fluxo de Caixa</a></li>
        <li><a href='?op=relatorio-fluxo-caixa-realizado'>Relatório Fluxo de Caixa Realizado</a></li>
    </ul>
    <?php if($_SESSION['empresa_principal'] == '1'){?>
			<li><a href='?op=plano-contas'>Planos de Contas</a></li>
			<ul>
				<li><a href='plano-contas/index.php?action=ipcf'>Financeiro</a></li>
				<li><a href='plano-contas/index.php?action=ipcc'>Contábil</a></li>
				<li><a href='dominio-sistemas/'>Gerar Arquivo Domínio Sistemas</a></li>
			</ul>
    <li><a href='?op=porcento_ipcg'>Taxa IPCG</a></li>
    <li><a href='?op=cadastro_servico_prestador'>Cadastrar Serviços dos Prestadores.</a></li>
    <?php }?>

	  <li><a href='?op=pesquisarCustoFatura'>Custo Orcamento/Fatura</a></li>

  </ul>
</div>
<div class="box">
<div id='calculadora'></div>
</div> -->
</div>
</div>
</body>
</html>

