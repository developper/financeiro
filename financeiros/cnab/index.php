<?php

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

//ini_set('display_errors',1);

$routes = [
    'GET'  => [
        '/financeiros/cnab/?action=index-bordero' => '\App\Controllers\Bordero::indexBordero',
        '/financeiros/cnab/?action=criar-bordero' => '\App\Controllers\Bordero::criarBordero',
        '/financeiros/cnab/?action=editar-bordero' => '\App\Controllers\Bordero::editarBordero',
        "/financeiros/cnab/?action=pesquisar-parcelas" => '\App\Controllers\Bordero::pesquisarParcelas',
        "/financeiros/cnab/?action=visualizar-parcelas" => '\App\Controllers\Bordero::visualizarParcelasBordero',
        "/financeiros/cnab/?action=desfazer-bordero" => '\App\Controllers\Bordero::desfazerBordero',
        "/financeiros/cnab/?action=gerar-cnab" => '\App\Controllers\Bordero::gerarCnab',
        "/financeiros/cnab/?action=importar-retorno-cnab" => '\App\Controllers\Bordero::importarRetornoCnab',
        "/financeiros/cnab/?action=imprimir-bordero" => '\App\Controllers\Bordero::imprimirParcelasBordero',
        "/financeiros/cnab/?action=importar-varredura-cnab" => '\App\Controllers\Bordero::importarVarreduraCnab',
    ],
    'POST' => [      
        '/financeiros/cnab/?action=index-bordero' => '\App\Controllers\Bordero::pesquisarBorderos',
        '/financeiros/cnab/?action=criar-bordero' => '\App\Controllers\Bordero::salvarBordero',
        '/financeiros/cnab/?action=editar-bordero' => '\App\Controllers\Bordero::atualizarBordero',
        '/financeiros/cnab/?action=upload-retorno-cnab' => '\App\Controllers\Bordero::processarArquivoRetornoCnab',
        '/financeiros/cnab/?action=baixar-parcelas-retorno' => '\App\Controllers\Bordero::baixarParcelasRetorno',
        '/financeiros/cnab/?action=estornar-parcela' => '\App\Controllers\Bordero::estornarParcela',
        '/financeiros/cnab/?action=upload-varredura-cnab' => '\App\Controllers\Bordero::processarArquivoVarreduraCnab',
    ]

];
$args = isset($_GET) && !empty($_GET) ? $_GET : null;
try {
    $app = new App\Application;
    $app->setRoutes($routes);
    $app->dispatch(METHOD, URI,$args);
} catch(App\BaseException $e) {
    echo $e->getMessage();
} catch(App\NotFoundException $e) {
    http_response_code(404);
    echo $e->getMessage();
}