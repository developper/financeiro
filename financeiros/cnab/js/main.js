$(function ($) {
    $('.chosen-select').chosen();

    $('#buscar-faturas-enviadas').click(function () {
        if (!validar_campos('div-pesquisar-faturas-enviadas')) {
            return false;
        }
    });

    $(".valor").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $(".marcar-todos").live('click', function () {
        $("#tbody-parcelas-result")
            .find('input:checkbox')
            .not(this)
            .prop('checked', this.checked);
    });

    $("#pesquisar-bordero").on('click', function () {
        var codigo_bordero = $('#codigo-bordero').val();
        var inicio = $('#inicio').val();
        var fim = $('#fim').val();
        if (codigo_bordero || (inicio && fim)) {
            $('#form-pesquisa-bordero').submit();
        } else {
            alert('Preencha ao menos o período ou o código do bordero!');
        }
    });

    $("#tipo-bordero").change(function(){
        var banco = $("#cod-banco").val();
        if( banco == 341 ){
            $("#tbody-bordero").html('');
        $("#valor-total-text").html('<b>R$ 0,00</b>');
        }        

    });

    $('#pesquisar-parcelas').on('click', function(){
        var tipo_bordero = $("#tipo-bordero").val();
        var banco = $("#cod-banco").val();
        if(tipo_bordero == '' && banco == 341 ){
            alert('Você deve selecionar o tipo de Bordero antes de fazer a pesquisa.');
            return false;
        }
        var banco = $("#cod-banco").val();
        var impostos_adicionados = [];
        var tipo_nota = $("#tipo-nota").val();
        var $parcelasFaturaElem = $(".parcelas-bordero");
        if ($parcelasFaturaElem.length > 0) {
            $parcelasFaturaElem.each(function () {
                impostos_adicionados.push($(this).val());
            })
        }

        var tr_parcela = $('#tr-parcela').val();
        var inicio = $('#inicio').val();
        var fim = $('#fim').val();
        if ((tr_parcela != '' && tr_parcela != '0') || (inicio != '' && fim != '')) {
            var data = $("#form-parcelas").serialize();
            data += "&tipo_nota=" + tipo_nota;
            data += "&tr=" + tr_parcela;
            data += "&banco=" + banco;
            data += "&tipo_bordero=" + tipo_bordero;

            $.ajax({
                url: "/financeiros/cnab/?action=pesquisar-parcelas",
                type: 'GET',
                data: data,
                success: function (response) {
                    if (response != '0' && response != 'null') {
                        response = JSON.parse(response);
                        if (Object.keys(response).length > 0) {
                            var htmlTr = '';
                            for (var i = 0; i < response.length; i++) {
                                var dataVencimento = response[i]['data_vencimento'].split('-').reverse().join('/');
                                var dataEmissao = response[i]['dataEntrada'].split('-').reverse().join('/');
                                var nomeFornecedor = response[i]['nome_fornecedor'];
                                var ur = response[i]['ur'];
                                var num_parcela = response[i]['num_parcela'];
                                var valorPagar = float2moeda(response[i]['valor_pagar']);
                                var sigla = response[i]['sigla'];
                                var codigo_barras = response[i]['codBarras'];
                                var idParcela = response[i]['id_parcela'];
                                var idNota = response[i]['id_nota'];
                                var codigo = response[i]['codigo'];
                                var strikethrough = '';
                                var disabled = '';
                                var checked = '';
                                var link = "../?op=notas&act=editar&idNotas="+response[i]['id_nota']+"&notaref="+response[i]['NOTA_REFERENCIA'];
                                var forma_pagamento = response[i]['forma_pagamento'];
                                var faIcon = '';
                                var forma = response[i]['forma'];
                                var chave_pix = response[i]['chave_pix'];

                                if(forma_pagamento == 1){
                                    faIcon = 'fa-barcode';
                                }else if(forma_pagamento == 7){
                                    faIcon = 'fa-credit-card';
                                }
                                else if(forma_pagamento == 5){
                                    faIcon = 'fa-exchange';
                                }
                                else if(forma_pagamento == 4){
                                    faIcon = 'fa-money';
                                }else if(forma_pagamento == 6){
                                    faIcon = 'fa-check-circle-o';
                                }else if(forma_pagamento == 10){
                                    faIcon = 'pix';
                                }else if(forma_pagamento == 9){
                                    faIcon = 'fa-list';
                                }
                                
                                if($.inArray(idParcela, impostos_adicionados) != '-1') {
                                    strikethrough = " style='text-decoration: line-through;' ";
                                    disabled = ' disabled ';
                                    checked = ' checked ';
                                }

                                htmlTr += "<tr class='tr-parcelas-buscar' " + strikethrough + ">" +
                                    "<td class='text-center'>" +
                                    "<input type='checkbox' " +
                                    disabled +
                                    checked +
                                    "class='check-adicionar-parcela' " +
                                    "data-vencimento='" + dataVencimento + "'" +
                                    "data-emissao='" + dataEmissao + "'" +
                                    "nome-fornecedor='" + nomeFornecedor + "'" +
                                    "valor-pagar='" + valorPagar + "'" +
                                    "sigla='" + sigla + "'" +
                                    "codigo-barras='" + codigo_barras + "'" +
                                    "id-nota='" + idNota + "'" +
                                    "codigo='" + codigo + "'" +
                                    "id-parcela='" + idParcela + "'" +
                                    "chave-pix='" + chave_pix + "'" +
                                    "/>" +
                                    "</td>" +
                                    "<td class='text-center'>" +
                                    "<a target='_blank' href='" + link + "'>" +
                                    idNota + "</a></td>" +
                                    "<td class='text-center'>" + codigo + "</td>" +
                                    "<td class='text-center'>" + num_parcela + "</td>" +
                                    "<td>" + ur.toUpperCase() + "</td>" +
                                    "<td>" + nomeFornecedor + "</td>" +
                                    "<td class='text-center'>" + dataEmissao + "</td>" +
                                    "<td class='text-center'>" + dataVencimento + "</td>" +
                                    "<td>" + valorPagar + "</td>" +
                                    "<td class='text-center'>" + sigla + "</td>" +
                                    "<td class='text-center'> <i style='font-size:20px !important' class='fa "+faIcon+"' title='"+forma+"'> </i> </td>" +
                                    "</tr>";
                            }

                            $("#tbody-parcelas-result").html(htmlTr);
                            $('#modal-add-parcelas').modal();
                        } else {
                            alert("Nenhuma parcela encontrada para os filtros escolhidos");
                        }
                    } else {
                        alert("Nenhuma parcela encontrada para os filtros escolhidos!");
                    }
                }
            });
        } else {
            alert("É necessário preencher pelo menos o campo TR ou o período para buscar as parcelas!");
        }
    });

    $('#adicionar-parcela').live('click', function () {
        var aux = 0;
        var borderoLista = 0;
        parcelasId = [];
        var valor_total = parseFloat($("#valor-total").val());
        var tipo_bordero = $("#tipo-bordero").val();
        console.log(tipo_bordero);
        

        $('.id_bordero').each(function () {
            parcelasId.push($(this).attr('bordero'));
        });

        $("#info-bordero").remove();

        $('.check-adicionar-parcela:not(:disabled)').each(function () {
            if ($(this).is(':checked')) {
                var parcelasID = $(this).attr('idParcela');
                aux++;

                if (!parcelasId.includes(parcelasID)) {
                    var valor_pagar = parseFloat(
                        $(this)
                            .attr('valor-pagar')
                            .replace('.', '')
                            .replace(',', '.')
                    );
                    valor_total += valor_pagar;
                    var vencimento = $(this).attr('data-vencimento').split('/').reverse().join('-');
                    var chave_pix = '';
                    if(tipo_bordero == 13){
                         chave_pix = "<br> <b>PIX: "+$(this).attr('chave-pix')+"</b>";
                    }
                  
                    $("#tbody-bordero").append(
                        "<tr class='tr-parcela-bordero'>" +
                        "   <td valign='middle' rowspan='2'>" +
                        "       <button title='Remover Parcela' type='button' role='button'" +
                        "               class='btn btn-danger remover-parcela'>" +
                        "           <i class='fa fa-times'></i>" +
                        "       </button>" +
                        "       <input type='hidden' valor-parcela='" + valor_pagar + "' name='parcelas[" + $(this).attr('id-nota') + "][]' " +
                        "              class='parcelas-bordero' value='" + $(this).attr('id-parcela') + "' />" +
                        "   </td>" +
                        "   <td style='vertical-align: middle'>" + $(this).attr('codigo') + "</td>" +
                        "   <td style='vertical-align: middle'>" + $(this).attr('nome-fornecedor') + "</td>" +
                        "   <td><input type='date' class='form-control vencimentos' name='vencimentos[" + $(this).attr('id-parcela') + "]' value='" + vencimento + "'></td>" +
                        "   <td style='vertical-align: middle'>" + $(this).attr('sigla') + "</td>" +
                        "   <td style='vertical-align: middle' colspan='1' class='valor_real'>" +
                        "       <input type='hidden' name='valores_parcelas[" + $(this).attr('id-parcela') + "]' class='valores_parcelas' value='" + $(this).attr('valor-pagar') + "'>" +
                        "       R$ " + $(this).attr('valor-pagar') +
                        "   </td>" +
                        "<td class='total_individual' total-individual = '"+valor_pagar+"' style='vertical-align: middle'> " + "R$" + $(this).attr('valor-pagar') + " </td>" +
                        "</tr>" +
                        "<tr class='tr-parcela-codigo-barra'>" +
                        "   <td style='vertical-align: middle' colspan='3'><b>Código de Barras: " + ($(this).attr('codigo-barras') || 'Nenhum.') + "</b>"+chave_pix+"</td>" +
                        "   <td><b>Juros (R$):</b> <input type='text' class='form-control valor juros' name='juros[" + $(this).attr('id-parcela') + "]' value='0,00'></td>" +
                        "   <td><b>Desconto (R$):</b> <input type='text' class='form-control valor descontos' name='descontos[" + $(this).attr('id-parcela') + "]' value='0,00'></td>" +
                        "   <td><b>Adicionais (R$):</b> <input type='text' class='form-control valor adicionais' name='tarifas_adicionais[" + $(this).attr('id-parcela') + "]' value='0,00'></td>" +
                        "</tr>"
                    );
                } else {
                    borderoLista++;
                }
            }
        });

        $("#valor-total-text").text("R$ " + float2moeda(valor_total));
        $("#valor-total").val(valor_total);


        $(".valor").priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });

        if (aux == 0) {
            alert('Selecione uma parcela antes de tentar adicionar.');
            return false;
        }
        $("#tbody-bordero-result").html('');
        $('#modal-add-parcelas').modal('hide');
    });

    $(".remover-parcela").live('click', function () {
        var firstTr = $(this).closest('tr');
        var secondTr = firstTr.next('tr');
        var valor_total_parcela = firstTr.find('.total_individual').attr('total-individual');
        var valor_total = parseFloat($("#valor-total").val());
        valor_total -= valor_total_parcela;

        firstTr.remove();
        secondTr.remove();        
        
        $("#valor-total-text").text("R$ " + float2moeda(valor_total));
        $("#valor-total").val(valor_total);

    });

    $(".tr-parcelas-buscar td").live('click', function (event) {
        if (event.target.type !== 'checkbox') {
            $(this)
                .parent()
                .find('input:checkbox')
                .trigger('click');
        }
    });

    $('#dados-bordero').live('click', function () {
        var $inputParcelas = $(".parcelas-bordero");
        var $tipoBordero = $("#tipo-bordero");

        if ($tipoBordero.val() == '') {
            alert('Para finalizar o bordero, é necessário escolher um tipo!');
            return false;
        }

        if (typeof $inputParcelas != 'undefined' && $inputParcelas.length >= 1) {
            $("#modal-dados-bordero").modal('show');
        } else {
            alert('Para finalizar o bordero, é necessário adicionar uma ou mais parcelas!');
        }
    });

    $('#criar-bordero').live('click', function () {
        var $inputParcelas = $(".parcelas-bordero");
        var $inputVencimentos = $(".vencimentos");
        var $inputValorParcela = $(".valores_parcelas");
        var $inputJuros = $(".juros");
        var $inputDescontos = $(".descontos");
        var $inputAdicionais = $(".adicionais");
        var banco = $("#cod-banco").val();

        if (validar_campos('div-dados-bordero')) {
            var tipo_bordero = $("#tipo-bordero").val();
            var conta = $("#conta").val();
            var descricao = $("#descricao").val();
            var vencimentos = [];
            var valores_parcelas = [];
            var juros = [];
            var descontos = [];
            var tarifas_adicionais = [];
            var parcelasId = [];
            if ($inputParcelas.length > 0) {
                $inputParcelas.each(function () {
                    parcelasId.push($(this).val());
                });

                $inputVencimentos.each(function () {
                    vencimentos.push($(this).val());
                });

                $inputValorParcela.each(function () {
                    valores_parcelas.push($(this).val());
                });

                $inputJuros.each(function () {
                    juros.push($(this).val());
                });

                $inputDescontos.each(function () {
                    descontos.push($(this).val());
                });

                $inputAdicionais.each(function () {
                    tarifas_adicionais.push($(this).val());
                });
            }
            var data = {
                'vencimentos': vencimentos,
                'valores_parcelas': valores_parcelas,
                'juros': juros,
                'descontos': descontos,
                'tarifas_adicionais': tarifas_adicionais,
                'parcelas_id': parcelasId,
                'tipo_bordero': tipo_bordero,
                'conta': conta,
                'descricao': descricao
            };
            $.ajax({
                url: "/financeiros/cnab/?action=criar-bordero",
                type: 'POST',
                data: data,
                success: function (response) {
                    response = JSON.parse(response);
                    
                    if (response.erro == false) {
                        alert(response.mensagem);
                        window.location.href = '/financeiros/cnab/?action=criar-bordero&banco='+banco;
                    } else {
                       alert(response.mensagem+' Entre em contato com o TI!');
                   }
                }
            });
        }
    });

    $("#visualizar-parcelas-bordero").live('click', function () {
        var $this = $(this);
        var bordero_id = $this.attr('id-bordero');
        var $tbodyParcelas = $("#tbody-parcelas-result");
        var $botaoImprimir = $("#imprimir-bordero");
        var urlImprimir = $botaoImprimir.attr('url');
        $botaoImprimir.attr('href', urlImprimir + bordero_id);

        $.ajax({
            url: "/financeiros/cnab/?action=visualizar-parcelas",
            type: 'GET',
            data: { bordero_id: bordero_id },
            success: function (response) {
                if (response != '0') {
                    response = JSON.parse(response);
                    if (Object.keys(response).length > 0) {
                        $tbodyParcelas.html("");
                        $.each(response, function (index, value) {
                            var dataVencimento = value.data_vencimento.split('-').reverse().join('/');
                            var dataEmissao = value.dataEntrada.split('-').reverse().join('/');
                            var valorPagar = float2moeda(value.valor_pagar);
                            var forma_pagamento = value.forma_pagamento;
                            var conta_fornecedor = value.conta_fornecedor;
                            var codBarras = value.codBarras;
                            var agencia_fornecedor = value.agencia_fornecedor;
                            var cc_fornecedor = value.cc_fornecedor;
                            var bgTr = '';
                            var conta_codigo_barras = '';
                            var btnEstornar = '';
                            var erro = '';
                            if (forma_pagamento == 'TED') {
                                if (!conta_fornecedor || cc_fornecedor.length > 12 || agencia_fornecedor.length > 5) {
                                    bgTr = 'class="danger"';
                                }
                                if (value.status == 'Processada' && value.CONCILIADO == 'S') {
                                    btnEstornar = '<button type="button" parcela="' + value.id + '" class="btn btn-warning estornar-parcela">Estornar</button>';
                                }
                                conta_codigo_barras = conta_fornecedor || 'Fornecedor sem conta cadastrada';
                                if (conta_fornecedor && agencia_fornecedor.length > 5) {
                                    erro += '<br><br>Erro: O número da agência deve ter no máximo 5 dígitos!';
                                }
                                if (conta_fornecedor && cc_fornecedor.length > 12) {
                                    erro += '<br><br>Erro: O número da conta deve ter no máximo 12 dígitos!';
                                }
                                $('#conta-codigo-barras').text('Conta do Fornecedor');
                            } else if (forma_pagamento == 'BOLETO') {
                                if (codBarras == '' ||
                                    (
                                        codBarras.length != '44' && codBarras.length != '47'
                                    )
                                ) {
                                    bgTr = 'class="danger"';
                                    erro = '<br><br>Erro: O código de barras está vazio ou não tem o tamanho adequado para este tipo de lançamento! Tamanho atual: ' + (codBarras ? codBarras.length : 0);
                                }
                                conta_codigo_barras = codBarras || '';
                                $('#conta-codigo-barras').text('Código de Barras');
                            } else if (forma_pagamento == 'CONVENIO') {
                                if (codBarras == '' ||
                                    (
                                        codBarras.length != '44' && codBarras.length != '48'
                                    )
                                ) {
                                    bgTr = 'class="danger"';
                                    erro = '<br><br>Erro: O código de barras está vazio ou não tem o tamanho adequado para este tipo de lançamento! Tamanho atual: ' + (codBarras ? codBarras.length : 0);
                                }
                                conta_codigo_barras = codBarras || '';
                                $('#conta-codigo-barras').text('Código de Barras');
                            }
                            $tbodyParcelas.append(
                                "<tr " + bgTr + ">" +
                                "   <td class='text-center'>" + value.codigo + "</td>" +
                                "   <td class='text-center'>" + value.num_parcela + "</td>" +
                                "   <td>" + value.ur.toUpperCase() + "</td>" +
                                "   <td>" + value.nome_fornecedor + "</td>" +
                                "   <td class='text-center'>" + dataEmissao + "</td>" +
                                "   <td class='text-center'>" + dataVencimento + "</td>" +
                                "   <td>" + valorPagar + "</td>" +
                                "   <td>" + conta_codigo_barras + " " + erro + "</td>" +
                                "   <td class='text-center'>" + value.sigla + "</td>" +
                                "   <td class='text-center'>" + btnEstornar + "</td>" +
                                "</tr>"
                            );
                        });
                        $('#modal-visualizar-parcelas').modal("show");
                    }
                } else {
                    alert('Houve um problema ao trazer as parcelas desse bordero! Entre em contato com o TI!');
                }
            }
        });
    });

    $("#desfazer-bordero").live('click', function () {
        var $this = $(this);
        var bordero_id = $this.attr('id-bordero');
        if (confirm('Deseja realmente desfazer esse bordero?')) {
            $.ajax({
                url: "/financeiros/cnab/?action=desfazer-bordero",
                type: 'GET',
                data: { bordero_id: bordero_id },
                success: function (response) {
                    if (response == '1') {
                        alert('Bordero desfeito com sucesso!');
                        $this.parents('tr').remove();
                    } else if (isJson(response)) {
                        response = JSON.parse(response);
                        if (Object.keys(response).length > 0) {
                            var trs = [];
                            $.each(response, function (index, value) {
                                trs.push(index);
                            });
                            var trtexto = trs.join(', ');
                            alert('Esse bordero possui parcelas que já foram processadas/conciliadas na(s) TR(s): ' + trtexto + '.' +
                                'Desfaça o processamento e/ou conciliação primeiro para depois desfazê-lo!');
                        }
                    } else if (response == '0') {
                        alert('Houve um problema ao desfazer o bordero! Entre em contato com o TI!');
                    }
                }
            });
        }
    });

    $('#atualizar-bordero').live('click', function () {
        var bordero_id = $("#bordero-id").val();
        var $inputParcelas = $(".parcelas-bordero");
        var $inputVencimentos = $(".vencimentos");
        var $inputValorParcela = $(".valores_parcelas");
        var $inputJuros = $(".juros");
        var $inputDescontos = $(".descontos");
        var $inputAdicionais = $(".adicionais");

        if (validar_campos('div-dados-bordero')) {
            var conta = $("#conta").val();
            var descricao = $("#descricao").val();
            var tipo_bordero = $("#tipo-bordero").val();
            var vencimentos = [];
            var valores_parcelas = [];
            var juros = [];
            var descontos = [];
            var tarifas_adicionais = [];
            var parcelasId = [];
            if ($inputParcelas.length > 0) {
                $inputParcelas.each(function () {
                    parcelasId.push($(this).val());
                });

                $inputVencimentos.each(function () {
                    vencimentos.push($(this).val());
                });

                $inputValorParcela.each(function () {
                    valores_parcelas.push($(this).val());
                });

                $inputJuros.each(function () {
                    juros.push($(this).val());
                });

                $inputDescontos.each(function () {
                    descontos.push($(this).val());
                });

                $inputAdicionais.each(function () {
                    tarifas_adicionais.push($(this).val());
                });
            }
            var data = {
                'bordero_id': bordero_id,
                'tipo_bordero': tipo_bordero,
                'vencimentos': vencimentos,
                'valores_parcelas': valores_parcelas,
                'juros': juros,
                'descontos': descontos,
                'tarifas_adicionais': tarifas_adicionais,
                'parcelas_id': parcelasId,
                'conta': conta,
                'descricao': descricao
            };
            $.ajax({
                url: "/financeiros/cnab/?action=editar-bordero",
                type: 'POST',
                data: data,
                success: function (response) {
                    if (response != '0') {
                        alert('Bordero atualizado com sucesso!');
                        window.location.href = '/financeiros/cnab/?action=index-bordero';
                    } else {
                        alert('Houve um problema ao atualizar o bordero! Entre em contato com o TI!');
                    }
                }
            });
        }
    });

    $(".tr-parcelas-retorno td").live('click', function (event) {
        if (event.target.type !== 'checkbox') {
            $(this)
                .parent()
                .find('input:checkbox')
                .trigger('click');
        }
    });

    $('.estornar-parcela').live('click', function () {
        var $this = $(this);
        var parcela = $this.attr('parcela');

        if (confirm('Deseja realmente estornar esse TED? ' +
            'Um crédito será gerado para a conta que ela foi baixada. ' +
            'A parcela será liberada para que seja incluída em outro bordero.')) {

            $.ajax({
                url: "/financeiros/cnab/?action=estornar-parcela",
                type: 'POST',
                data: { parcela: parcela },
                success: function (response) {
                    if (response == '2') {
                        alert('Parcela estornada com sucesso!');
                        location.href = '/financeiros/cnab/?action=index-bordero';
                        return false;
                    }
                    if (response != '0') {
                        alert('Parcela estornada com sucesso!');
                        $this.parents('tr').remove();
                        return false;
                    } else if (response == '0') {
                        alert('Houve um problema ao estornar essa parcela! Entre em contato com o TI!');
                        return false;
                    }
                }
            });
        }
    });

    $('.juros, .descontos, .adicionais').live('blur', function () {
        console.log($(this).parent().parent()[0].previousElementSibling.lastElementChild)
        var $this = $(this),
            total_real = 0,
            juros_total = 0,
            desconto_total = 0,
            adicional_total = 0,
            valor_total = parseFloat($("#valor-total").val()),
            campo_total_individual = $this.parent().parent()[0].previousElementSibling.lastElementChild,
            juros = parseFloat($this.parent().parent().find('input.juros').val()
                .replace('.', '')
                .replace(',', '.')),
            desconto = parseFloat($this.parent().parent().find('input.descontos').val()
                .replace('.', '')
                .replace(',', '.')),
            adicionais = parseFloat($this.parent().parent().find('input.adicionais').val()
                .replace('.', '')
                .replace(',', '.')),
            valor_total_individual = parseFloat(campo_total_individual.innerText
                .replace("R$", "")
                .replace('.', '')
                .replace(',', '.')),
            valor_campo_total = parseFloat(($this.parent().parent()[0].previousElementSibling.cells[5].textContent)
                .replace("\t", "")
                .replace("R$", "")
                .replace('.', '')
                .replace(',', '.'));

        $('.tr-parcela-bordero').find('.valor_real').each(function () {
            total_real += parseFloat($(this)[0].textContent.replace("\t", "")
                .replace("R$", "")
                .replace('.', '')
                .replace(',', '.'))
        })

        $('.tr-parcela-codigo-barra').find('input.juros').each(function () {
            juros_total += parseFloat($(this).context.value
                .replace('.', '')
                .replace(',', '.'))
        })

        $('.tr-parcela-codigo-barra').find('input.descontos').each(function () {
            desconto_total += parseFloat($(this).context.value
                .replace('.', '')
                .replace(',', '.'))
        })

        $('.tr-parcela-codigo-barra').find('input.adicionais').each(function () {
            adicional_total += parseFloat($(this).context.value
                .replace('.', '')
                .replace(',', '.'))
        })

        valor_total = total_real + juros_total + adicional_total - desconto_total;
        valor_total_individual = valor_campo_total + juros + adicionais - desconto;

        campo_total_individual.innerText = "R$" + float2moeda(valor_total_individual);
        $("#valor-total-text").text("R$ " + float2moeda(valor_total));
        $("#valor-total").val(valor_total);
    });

    function float2moeda(num) {
        x = 0;
        if (num < 0) {
            num = Math.abs(num);
            x = 1;
        }
        if (isNaN(num)) num = "0";
        cents = Math.floor((num * 100 + 0.5) % 100);
        num = Math.floor((num * 100 + 0.5) / 100).toString();
        if (cents < 10) cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + '.'
                + num.substring(num.length - (4 * i + 3));
        ret = num + ',' + cents;
        if (x == 1) ret = ' - ' + ret; return ret;
    }

    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
});
