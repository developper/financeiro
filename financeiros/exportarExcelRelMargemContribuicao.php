<?php
$id = session_id();
if(empty($id))
    session_start();
include_once('../validar.php');
include('../db/config.php');
include_once('../utils/codigos.php');

header("Content-type: application/x-msexce");
header("Content-type: application/force-download;");
header("Content-Disposition: attachment; filename=margem de contribuicao.xls");
header("Pragma: no-cache");

Class exportarExcelRelatorioMargemContribuicao {
public function relatorioMargemContribuicao($nomePaciente, $nomePlano, $nomeTipoDocumento,
$inicio, $fim, $tipoDocumento,$plano, $paciente, $empresa)
{

$nomePaciente = anti_injection($nomePaciente, 'literal');
$nomePlano = anti_injection($nomePlano, 'literal');
$nomeTipoDocumento = anti_injection($nomeTipoDocumento, 'literal');
$inicioObj = \DateTime::createFromFormat('Y-m', $inicio);
$inicioBr = $inicioObj->format('m/Y');
$fimObj = \DateTime::createFromFormat('Y-m', $fim);
$fimBr = $fimObj->format('m/Y');


$fim = anti_injection( $fim, 'literal');

$inicio = anti_injection($inicio, 'literal');
$tipoDocumento = anti_injection ($tipoDocumento, 'numerico');
$plano = anti_injection ($plano, 'numerico');
$paciente = anti_injection ($paciente, 'numerico');
$empresa = anti_injection ($empresa, 'literal');

$condEmpresa = $empresa == 'T' ? "" :"AND empresas.id =  {$empresa}";
$condPaciente = $paciente == -1 ? "" : "AND fatura.PACIENTE_ID = {$paciente} ";
$condPlano = $plano == -1 ? "" : "AND fatura.PLANO_ID = {$plano} ";
$condTipoDocumento = $tipoDocumento == -1 ? "" : "AND fatura.ORCAMENTO = {$tipoDocumento} ";
/* impostos a serem acresentados no valor dos custos.
   IR 2%
   PissCofins 4,95 %
   ISS buscar na tabela do plano.
   Pegar a soma dos impostos  aplicar no valor faturado ( somaImposto * (valor + desc_custo) ) e acrescentar no custo.
 */
$pissCofins = 4.95;
$ir = 3.5;
$iss = 0;

    $sql= "
select
SUM(faturamento.QUANTIDADE*faturamento.VALOR_FATURA) as valor,
SUM(faturamento.QUANTIDADE*faturamento.VALOR_CUSTO) as custo,
SUM((faturamento.QUANTIDADE*faturamento.VALOR_FATURA)*faturamento.DESCONTO_ACRESCIMO/100) as desc_custo,
tbl.*
FROM
faturamento inner join
(
SELECT
        clientes.nome as paciente,
        planosdesaude.nome as convenio,
        empresas.nome as ur,
        fatura.`DATA`,
        DATE_FORMAT(fatura.DATA_INICIO,'%d/%m/%Y') as inicioBr,
        DATE_FORMAT(fatura.DATA_FIM ,'%d/%m/%Y') as fimBr,
        fatura.data_margem_contribuicao,
        fatura.ID,
        fatura.imposto_iss,
        fatura.ORCAMENTO,
        fatura.remocao
        FROM
        fatura
        LEFT JOIN clientes ON fatura.PACIENTE_ID = clientes.idClientes
        LEFT JOIN planosdesaude ON fatura.PLANO_ID = planosdesaude.id
        LEFT JOIN empresas ON fatura.UR = empresas.id
        WHERE

        fatura.CANCELADO_POR is NULL AND
        DATE_FORMAT(fatura.data_margem_contribuicao, '%Y-%m') between '$inicio'
         and '$fim'
         {$condEmpresa}
         {$condPaciente}
         {$condPlano}
         {$condTipoDocumento}

         group by fatura.ID
 ) as tbl on  faturamento.FATURA_ID = tbl.ID
         WHERE
         faturamento.CANCELADO_POR is NULL
         group by tbl.ID";
$nomeUr = 'Todos';
if ($empresa != 'T') {
    $result = mysql_query("SELECT nome FROM empresas where id = $empresa");
    $nomeUr = mysql_result($result, 0);
}

$html = "<table >
                 <tr>
                    <td colspan='9'><center><h1> Relat&oacute;rio de Margem de Contribui&ccedil;&atilde;o</h1></center></td>
                 </tr>
                 <tr>
                      <td><b>Paciente:</b>{$nomePaciente}</td>
                      <td colspan='9'><b>Empresa:</b> {$nomeUr}</td>
                 </tr>
                 <tr>
                     <td><b>Plano:</b> {$nomePlano}</td>
                     <td colspan='9'><b>Tipo documento:</b> $nomeTipoDocumento</td>
                 </tr>

                 <tr>
                     <td><b>Inicio:</b> $inicioBr</td>
                     <td colspan='9'><b>Fim:</b>$fimBr</td>
                 </tr>
                 <tr>
                   <td colspan='9'><b>Retirado por " . $_SESSION["nome_user"] . " em  " . date('d/m/Y h:i') . "</b></td>
                 </tr>
                 <tr>
                   <td colspan='9'><b>Obs.: A coluna <i>M&ecirc;s Base</i> serve de par&acirc;metro de filtro da busca.</b></td>
                 </tr>
             </table><br><br>";
$result = mysql_query($sql);

if (mysql_num_rows($result) == 0) {
    $html .= "<span> Nenhum lan&ccedil;amento foi encontrado.</span>";
    echo $html;
    return;
}
    // custos extras
    $sqlExtras = "select
sum(fatura_custos_extras.custo * fatura_custos_extras.quantidade) as custoExtra,
tbl.ID


        from
        fatura_custos_extras
        inner join
        (
        SELECT
        fatura.ID
        FROM
        fatura
        WHERE
        fatura.CANCELADO_POR is NULL AND
        DATE_FORMAT(fatura.data_margem_contribuicao, '%Y-%m') between '$inicio'
         and '$fim'
         {$condEmpresa}
         {$condPaciente}
         {$condPlano}
         {$condTipoDocumento}
         group by fatura.ID
        ) as tbl on fatura_custos_extras.id_fatura = tbl.ID
        where
         canceled_by is null
         group by tbl.ID
        ";

    $resultExtras = mysql_query($sqlExtras);
    while($array = mysql_fetch_array($resultExtras)){

        $arrayCustosExtras[$array['ID']] = $array['custoExtra'] ;


    }
$html .= "<table border='1px' bordercolor='#0000'>";
$html .= "<thead><tr style='background-color:#E6E6E6;'>
                  <td><b>ID da Fatura</b></td>
                  <td><b>M&ecirc;s Base</b></td>
                  <td><b>Paciente</b></td>
                  <td><b>Per&iacute;odo</b></td>
                  <td><b>UR</b></td>
                  <td><b>Tipo</b></td>
                  <td><b>Conv&ecirc;nio</b></td>
                  <td><b>Faturado</b></td>
                  <td><b>Custo</b></td>
                  <td><b>Margem R$</b></td>
                  <td><b>Margem % </b></td>
             </tr></thead>";
    $totalFaturado          = 0;
    $totalCusto             = 0;
    $totalMargem            = 0;
    $totalMargemPorcento    = 0;

while ($row = mysql_fetch_array($result)) {
    $custosExtras = empty($arrayCustosExtras[$row['ID']]) ? 0 : $arrayCustosExtras[$row['ID']];

    $faturado = $row['valor'] + $row['desc_custo'];
    $custo = ($row['valor'] * ($pissCofins + $ir + $row['imposto_iss']) / 100) + $row['custo'] + $custosExtras;
    $margem = $faturado - $custo;
    $margemPorcento = $faturado == 0 ? -1 : $margem / $faturado;
    $totalFaturado += $faturado;
    $totalCusto += $custo;
    $totalMargem += $margem;
    $sqlTipo = "SELECT
                        vc.idCobranca AS cod_item
                    FROM
                        faturamento AS f
                    LEFT JOIN valorescobranca AS vc ON (f.CATALOGO_ID = vc.id)
                    WHERE
                        f.FATURA_ID = {$row['ID']}
                    AND f.TIPO = 2
                    AND f.TABELA_ORIGEM = 'valorescobranca'
                    AND f.CANCELADO_POR IS NULL
                    AND vc.idCobranca IN (20, 21, 51, 60, 61, 77)
                    UNION
                        SELECT
                            vc.id AS cod_item
                        FROM
                            faturamento AS f
                        LEFT JOIN cobrancaplanos AS vc ON (f.CATALOGO_ID = vc.id)
                        WHERE
                            f.FATURA_ID = {$row['ID']}
                        AND f.TIPO = 2
                        AND f.TABELA_ORIGEM = 'cobrancaplano'
                        AND f.CANCELADO_POR IS NULL
                        AND f.CATALOGO_ID IN (20, 21, 51, 60, 61, 77)";
    $resultTipo = mysql_query($sqlTipo);

    $tipoInternacao = "ID";
    if(mysql_num_rows($resultTipo) == 0){
        $tipoInternacao = "AD";
    }

    $html .= "<tr>
                  <td>{$row['ID']}</td>
                  <td>" . \DateTime::createFromFormat('Y-m-d', $row['data_margem_contribuicao'])->format('m/Y') . "  </td>
                  <td>".htmlentities($row['paciente'])."</td>
                  <td>{$row['inicioBr']} - {$row['fimBr']}  </td>                  
                  <td>".htmlentities($row['ur'])."</td>
                  <td>".($row['remocao'] == 'S' ? htmlentities('Remoção') : $tipoInternacao)."</td>
                  <td>".htmlentities($row['convenio'])."</td>
                  <td>" . number_format($faturado, 2, ',', '.') . "</td>
                  <td>" . number_format($custo, 2, ',', '.') . "</td>
                  <td>" . number_format($margem, 2, ',', '.') . "</td>
                  <td>" . number_format($margemPorcento * 100, 2, ',', '.') . "%</td>

             </tr>";

}
$totalMargemPorcento += $totalFaturado == 0 ? -1 : $totalMargem / $totalFaturado;
$html .= "<tr style='background-color:#E6E6E6;'>
                  <td colspan='7' align='right'><b>TOTAIS:</b></td>
                  <td>" . number_format($totalFaturado, 2, ',', '.') . "</td>
                  <td>" . number_format($totalCusto, 2, ',', '.') . "</td>
                  <td>" . number_format($totalMargem, 2, ',', '.') . "</td>
                  <td>" . number_format($totalMargemPorcento * 100, 2, ',', '.') . " %</td>

             </tr>
                </table>";

echo $html;
}

}

$p =new exportarExcelRelatorioMargemContribuicao;
$p->relatorioMargemContribuicao($_GET['nomePaciente'], $_GET['nomePlano'], $_GET['nomeTipoDocumento'],
    $_GET['inicio'], $_GET['fim'], $_GET['tipoDocumento'],
    $_GET['plano'], $_GET['paciente'], $_GET['empresa']);