
$(function($) {


        $('#paciente, #ur, #cliente, #plano, #fornecedor').chosen();

        $('#buscar-faturas-enviadas').click(function () {
            if(!validar_campos('div-pesquisar-faturas-enviadas')){
                return false;
            }
        });
    $("#regras-pagamento").hide();

    $("#visualizar-regras-pagamento").click(function(){

        if($(this).attr('visualizar') == 0){
            $('#icone').removeClass('fa-angle-down');
            $('#icone').addClass('fa-angle-up');
            $(this).attr('visualizar',1);
            $("#regras-pagamento").show();


        }else{
            $('#icone').removeClass('fa-angle-up');
            $('#icone').addClass('fa-angle-down');
            $(this).attr('visualizar',0);
            $("#regras-pagamento").hide();

        }


    });

    $(".valor").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

        $('#adicionar-data').on('click',function () {

            var quantidadeSelecionada =  $('.checkbox-add-data:checked').length;
            var data = $("#data-add").val();
            var desconto = $('#add-desconto').val();

            if(quantidadeSelecionada == 0){
                alert('Selecione uma Fatura antes de tentar adicionar uma data.');
                return false;
            }

            if(!validar_campos('div-add-data')){
                return false;
            }

            $('.checkbox-add-data:checked').each(function () {
                $(this).closest('tr').find('.data-previsao').val(data);
                $(this).closest('tr').find('.percentual-desconto').val(desconto);

            });

            $("#data-add").val('');
            $('#add-desconto').val('');
            $('.checkbox-add-data:checked').attr('checked',false);
            $("#checkbox-add-data-todos").attr('checked',false);

        });

        $("#checkbox-add-data-todos").on('click',function () {
            if($(this).is(':checked')){
                $(this).closest('table').find('.checkbox-add-data').attr('checked',true);
            }else{
                $(this).closest('table').find('.checkbox-add-data').attr('checked',false);
            }

        });

        $('#salvar-previsao-recebimento').on('click', function(){

            var aux = 0;
            $('.data-previsao').each(function () {

                var date = new Date($(this).val());
                if( (date instanceof Date && !isNaN(date.valueOf())) != false){
                    aux++;
                    $(this).closest('tr').find('.percentual-desconto').addClass('OBG');
                }else{
                    $(this).closest('tr').find('.percentual-desconto').removeClass('OBG');
                    $(this).closest('tr').find('.percentual-desconto').removeAttr('style');
                }

            });


            if(aux > 0){
                if(validar_campos('response')){
                    var data =   $("#form-previsao-recebimento").serialize();
                    $.ajax({
                        url: "/financeiros/faturamento-controle-recebimento/?action=salvar-previsao-recebimento",
                        type: 'POST',
                        data: data,
                        success: function (response) {
                            if(response == 1){
                                alert("Previsão salva com sucesso.");
                                window.location.href = "/financeiros/faturamento-controle-recebimento/?action=adicionarPrevisaoRecebimento";
                            }else{
                                alert(response);
                            }

                        }
                    });
                }else {
                    alert('Algum campo obrigatório não foi preenchido.');
                }

            }else{
                alert("Insira uma data valida antes de tentar salvar.");
            }


        });

    $('#pesquisar-previsao-recebimento-pendentes').on('click', function(){
        if(validar_campos('div-pesquisar-faturas-previsao-cadastrada')){
            var data =   $("#form-pesquisa-previsoes-pendentes").serialize();
            $.ajax({
                url: "/financeiros/faturamento-controle-recebimento/?action=pesquisarFaturasParaInformeRecebimento",
                type: 'POST',
                data: data,
                success: function (response) {
                     response = JSON.parse(response);
                    if(response.length > 0){
                        var htmlTr = '';
                        for(i = 0 ; i< response.length; i++){
                            var dataPrevisao = response[i]['previsao_recebimento'].split('-').reverse().join('/');
                            var dataEnvio = response[i]['dataEnvio'].split(' ');
                            dataEnvio = dataEnvio[0].split('-').reverse().join('/');
                            var valorFaturado = float2moeda(response[i]['valor_faturado']);
                            var percentualEncargos = float2moeda(response[i]['percentual_desconto_imposto']);
                            var valorLiquido = float2moeda(response[i]['valor_liquido']);

                            htmlTr += "<tr>"+
                                            "<td>" +
                                                "<input type='checkbox' class='check-adicionar-fatura' " +
                                                    "paciente='" + response[i]['paciente'] +"'" +
                                                    "convenio='" + response[i]['convenio'] +"'" +
                                                    "ur='" + response[i]['ur'] +"'" +
                                                    "valor_faturado='" + valorFaturado +"'" +
                                                    "encargos='" + percentualEncargos +"'" +
                                                    "valor_liquido='" + valorLiquido +"'" +
                                                    "inicio='" + response[i]['inicio'] +"'" +
                                                    "fim='" + response[i]['fim'] +"'" +
                                                    "modalidade='" + response[i]['modalidade_fatura'] +"'" +
                                                    "fatura_id='"+response[i]['ID']+"'"+
                                                "/>" +
                                            "</td>"+
                                            "<td>" + response[i]['paciente'] +"</td>"+
                                            "<td>" + response[i]['convenio'] +"</td>"+
                                            "<td>" + response[i]['ur'] +"</td>"+
                                            "<td>" + response[i]['inicio'] + " À " + response[i]['fim'] + "</td>"+
                                            "<td>R$ " + valorFaturado + " </td>"+
                                            "<td>% " + percentualEncargos + " </td>"+
                                            "<td>R$ " + valorLiquido + " </td>"+
                                            "<td>" + dataPrevisao +"</td>"+
                                            "<td>" + dataEnvio+"</td>"+
                                            "<td>" + response[i]['protocolo_envio']+"</td>"+
                                            "<td>" + response[i]['modalidade_fatura'] +"</td>"
                                        "</tr>";
                        }

                        $("#tbody-fatura-result").html(htmlTr);
                        $('#modal-add-fatura').modal();
                    }else{
                        alert("Nenhuma fatura encontrada para os filtros inseridos");
                    }
                }
            });
        }else{
            alert("Algum campo obrigatório não foi informado.");
        }
    });

    $('#adicionar-fatura').click(function () {
        var aux = 0;
        var faturaLista = 0;
        faturaId = [];
        $('.id_fatura').each(function () {
            faturaId.push($(this).attr('fatura'));
        });

        $('.check-adicionar-fatura').each(function () {
           if($(this).is(':checked')){
               aux++;
               var faturaID = $(this).attr('fatura_id');
               if(!faturaId.includes(faturaID)) {
                   $("#tbody-fatura-nota").append(
                       "<tr>" +
                       "<td>" +
                       "<div >"+
                       "<span title='Remover Fatura' style='font-size: 6px' class='btn btn-danger remover-fatura glyphicon glyphicon-remove'></span>"+
                       "</div>"+
                       "</td>"+
                       "<td>"+ $(this).attr('paciente')+"</td>" +
                       "<td>"+$(this).attr('convenio') +"</td>" +
                       "<td>"+ $(this).attr('ur')+"</td>" +
                       "<td>"+ $(this).attr('inicio')+" A "+$(this).attr('fim')+"</td>" +
                       "<td>R$ "+$(this).attr('valor_faturado') +"</td>" +
                       "<td>% "+$(this).attr('encargos') +"</td>" +
                       "<td>R$ "+ $(this).attr('valor_liquido')+"</td>" +
                       "<td style='width:250px; overflow: auto; !important;'>" +
                       "R$ <input class='form-control valor id_fatura OBG' style='width:150px; overflow: auto; !important;' fatura='"+$(this).attr('fatura_id')+"' name='valor_recebido["+$(this).attr('fatura_id')+"]' type='text' /> " +
                       "<input type='hidden' name='modalidade["+$(this).attr('fatura_id')+"]' value='"+$(this).attr('modalidade')+"' >"+
                       "</td>" +
                       "<td>"+$(this).attr('modalidade')+"</td>"+
                       "</tr>"
                   );
               }else{
                   faturaLista++;
               }
           }
        });

        $(".valor").priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        if(aux == 0){
           alert('Selecione uma fatura antes de tentar adicionar.');
           return false;
        }
        $("#tbody-fatura-result").html('');
        $('#modal-add-fatura').modal('hide');
        alert('Fatura adicionada com sucesso para compor a NF');
    });

    $(".remover-fatura").live('click', function () {

        $(this).closest('tr').remove();

    });

    $('#salvar-informe-recebimento').click(function () {

        /*if($('.id_fatura').find().prevObject.length == 0){
            alert("Adicione ao menos uma fatura antes de tentar salvar.");
            return false;
        }*/
        var ok = false;
        if($('.id_fatura').find().prevObject.length == 0){
            ok = confirm('Nenhuma fatura foi associada! Deseja continuar?');
        } else {
            ok = true;
        }
        var porcent_assist = parseFloat($('#porcentagem-assistencia').val().replace('.', '').replace(',', '.')) || 0.00;
        var porcent_remocao = parseFloat($('#porcentagem-remocao').val().replace('.', '').replace(',', '.')) || 0.00;

        var ok_porcent = false;
        console.log(porcent_assist,
        porcent_remocao);
        if(porcent_assist != 0.00 || porcent_remocao != 0.00) {
            ok_porcent = porcent_assist + porcent_remocao == 100.00;
            if(!ok_porcent) {
                alert('A(s) porcentagem(ns) deve(m) somar 100%!');
            }
        } else {
            alert('Informe a(s) porcentagem(ns) da NF!');
            ok_porcent = false;
        }

        if(validar_campos('pre-nota') && ok && ok_porcent){
            var data = $('#form-salvar-informe-recebimento').serialize();
            $.ajax({
                url: "/financeiros/faturamento-controle-recebimento/?action=salvarInformeRecebimento",
                type: 'POST',
                data: data,
                success: function (response) {
                    response = JSON.parse(response);
                    if(response[0] > 1){
                        var alertFaturaPendente = response[1] == '0' ? 'Será necessário associar uma fatura posteriormente!' : '';
                        alert("Informe de recebimento criado com sucesso. Termine de cadastrar a NF! " + alertFaturaPendente);
                        window.location.href = "../../financeiros/?op=notas&act=editar&idNotas="+response+"&notaref=0";
                    }else{
                        alert('Erro ao tentar salvar uma nota.');
                    }

                }
            });

        }

    });

    $('#pesquisar-previsao-recebimento-pendentes-associar').on('click', function(){
        if(validar_campos('div-pesquisar-faturas-previsao-cadastrada')){
            var data =   $("#form-pesquisa-previsoes-pendentes").serialize();
            $.ajax({
                url: "/financeiros/faturamento-controle-recebimento/?action=pesquisarFaturasParaInformeRecebimento",
                type: 'POST',
                data: data,
                success: function (response) {
                    response = JSON.parse(response);
                    if(response.length > 0){
                        var htmlTr = '';
                        for(i = 0 ; i< response.length; i++){
                            var dataPrevisao = response[i]['previsao_recebimento'].split('-').reverse().join('/');
                            var dataEnvio = response[i]['dataEnvio'].split(' ');
                            dataEnvio = dataEnvio[0].split('-').reverse().join('/');
                            var valorFaturado = float2moeda(response[i]['valor_faturado']);
                            var percentualEncargos = float2moeda(response[i]['percentual_desconto_imposto']);
                            var valorLiquido = float2moeda(response[i]['valor_liquido']);

                            htmlTr += "<tr>" +
                                "<td>" +
                                "<input type='checkbox' class='check-adicionar-fatura' " +
                                "paciente='" + response[i]['paciente'] + "'" +
                                "convenio='" + response[i]['convenio'] + "'" +
                                "ur='" + response[i]['ur'] + "'" +
                                "valor_faturado='" + valorFaturado + "'" +
                                "encargos='" + percentualEncargos + "'" +
                                "valor_liquido='" + valorLiquido + "'" +
                                "inicio='" + response[i]['inicio'] + "'" +
                                "fim='" + response[i]['fim'] + "'" +
                                "modalidade='" + response[i]['modalidade_fatura'] + "'" +
                                "fatura_id='" + response[i]['ID'] + "'/>" +
                                "</td>" +
                                "<td>" + response[i]['paciente'] + "</td>" +
                                "<td>" + response[i]['convenio'] + "</td>" +
                                "<td>" + response[i]['ur'] + "</td>" +
                                "<td>" + response[i]['inicio'] + " À " + response[i]['fim'] + "</td>" +
                                "<td>R$ " + valorFaturado + " </td>" +
                                "<td>% " + percentualEncargos + " </td>" +
                                "<td>R$ " + valorLiquido + " </td>" +
                                "<td><input type='text' class='valor-recebido-fatura valor OBG' size='10'></td>" +
                                "<td>" + dataPrevisao + "</td>" +
                                "<td>" + dataEnvio + "</td>" +
                                "<td>" + response[i]['protocolo_envio']+"</td>" +
                                "<td>" + response[i]['modalidade_fatura'] +"</td>" +
                            "</tr>";
                        }

                        $("#tbody-fatura-result").html(htmlTr);
                        $('#modal-add-fatura').modal();
                        $(".valor").priceFormat({
                            prefix: '',
                            centsSeparator: ',',
                            thousandsSeparator: '.'
                        });
                    }else{
                        alert("Nenhuma fatura encontrada para os filtros inseridos");
                    }
                }
            });
        }else{
            alert("Algum campo obrigatório não foi informado.");
        }
    });

    $(".associar-fatura").on('click', function () {
        var notaId = $(this).attr('nota-id');
        var previsaoRecebimento = $(this).attr('previsao-recebimento');
        $('#modal-add-fatura-nf').modal('show');
        $('#modal-add-fatura-nf').find('span#id-nota').text(notaId);
        $('#modal-add-fatura-nf').find('input#id-nota-recebimento').val(notaId);
        $('#modal-add-fatura-nf').find('input#previsao-recebimento').val(previsaoRecebimento);
    });

    $('#finalizar-associar-fatura').on('click', function () {
        var checkAdicionarFatura = $('.check-adicionar-fatura:checked');
        var data = [];
        if(checkAdicionarFatura.length > 0 ) {
            checkAdicionarFatura.each(function () {
                data.push({
                    paciente: $(this).attr('paciente'),
                    convenio: $(this).attr('convenio'),
                    ur: $(this).attr('ur'),
                    valor_faturado: $(this).attr('valor_faturado'),
                    encargos: $(this).attr('encargos'),
                    valor_liquido: $(this).attr('valor_liquido'),
                    inicio: $(this).attr('inicio'),
                    fim: $(this).attr('fim'),
                    modalidade: $(this).attr('modalidade'),
                    fatura_id: $(this).attr('fatura_id'),
                    previsao_recebimento: $('#previsao-recebimento').val(),
                    valor_recebido: $(this)
                        .parent()
                        .parent()
                        .find('.valor-recebido-fatura')
                        .val(),
                    nota_id: $('#id-nota-recebimento').val()
                });
            });
            $.post(
                '/financeiros/faturamento-controle-recebimento/?action=finalizar-associar-fatura-nota-recebimento',
                {data: data},
                function (response) {
                    if(response == 1) {
                        alert('Fatura(s) associada(s) com sucesso!');
                        $('#tbody-fatura-result').html("");
                        $('#modal-add-fatura-nf').modal('hide');
                    }
                }
            );
        } else {
            alert('É necessário associar pelo menos UMA fatura à nota!');
        }
    });

    $(".informar-valor-recebido").on('click', function () {
        var faturaId = $(this).attr('fatura-id');
        var nomePaciente = $(this).attr('nome-paciente');
        $('#modal-informar-recebimento-fatura').modal('show');
        $('#modal-informar-recebimento-fatura').find('span#id-fatura-header').text('#' + faturaId + ' de ' + nomePaciente);
        $('#modal-informar-recebimento-fatura').find('input#fatura-id').val(faturaId);
    });

    $('#salvar-recebimento-fatura').on('click', function () {
        var valor_recebido = $('#valor-recebido').val();
        var faturaId = $('#fatura-id').val();
        if(valor_recebido != '' && valor_recebido != '0,00') {
            var data = {'fatura_id': faturaId, 'valor_recebido': valor_recebido};
            $.post(
                '/financeiros/faturamento-controle-recebimento/?action=salvar-recebimento-fatura',
                {data: data},
                function (response) {
                    if(response == 1) {
                        alert('Atualização feita com sucesso!');
                        $('#valor-recebido').val('0,00');
                        $('#modal-informar-recebimento-fatura').modal('hide');
                        $('td#status-' + faturaId).html('RECEBIDO');
                        $('td#valor-recebido-' + faturaId).html('R$ ' + valor_recebido);
                        $('td#informar-' + faturaId).html('');
                    } else {
                        alert('Houve um problema ao atualizar o recebimento!');
                    }
                }
            );
        } else {
            alert('É necessário informar um valor!');
        }
    });

    function float2moeda(num) {

        x = 0;

        if(num<0) {
            num = Math.abs(num);
            x = 1;
        }
        if(isNaN(num)) num = "0";
        cents = Math.floor((num*100+0.5)%100);

        num = Math.floor((num*100+0.5)/100).toString();

        if(cents < 10) cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
            num = num.substring(0,num.length-(4*i+3))+'.'
                +num.substring(num.length-(4*i+3));
        ret = num + ',' + cents;
        if (x == 1) ret = ' - ' + ret;return ret;

    }
});