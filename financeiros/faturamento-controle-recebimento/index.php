<?php

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

ini_set('disable_erros',1);


$routes = [

		'GET'  => [
		   '/financeiros/faturamento-controle-recebimento/?action=adicionarPrevisaoRecebimento' => '\App\Controllers\FaturamentoControleRecebimento::formAdicionarPrevisaoRecebimentoFaturamentoEnviado',
            '/financeiros/faturamento-controle-recebimento/?action=relatorioPrevisaoRecebimento' => '\App\Controllers\FaturamentoControleRecebimento::formRelatorioPrevisaoRecebimentoFaturamentoEnviado',
            '/financeiros/faturamento-controle-recebimento/?action=adicionarInformeRecebimento' => '\App\Controllers\FaturamentoControleRecebimento::formAdicionarInformeRecebimento',
            '/financeiros/faturamento-controle-recebimento/?action=associar-fatura-nf-recebimento' => '\App\Controllers\FaturamentoControleRecebimento::formAssociarFaturaNFRecebimento',
            '/financeiros/faturamento-controle-recebimento/?action=pesquisar-fatura-nf-recebimento' => '\App\Controllers\FaturamentoControleRecebimento::formAssociarFaturaNFRecebimento',
        ],
		'POST' => [
            '/financeiros/faturamento-controle-recebimento/?action=pesquisarFaturamentoEnviado' => '\App\Controllers\FaturamentoControleRecebimento::pesquisarFaturamentoEnviado',
            "/financeiros/faturamento-controle-recebimento/?action=salvar-previsao-recebimento" => '\App\Controllers\FaturamentoControleRecebimento::salvarPrevisaoRecebimento',
            '/financeiros/faturamento-controle-recebimento/?action=pesquisarRelatorioPrevisaoRecebimento' => '\App\Controllers\FaturamentoControleRecebimento::pesquisarRelatorioPrevisaoRecebimento',
            '/financeiros/faturamento-controle-recebimento/?action=pesquisarFaturasParaInformeRecebimento' => '\App\Controllers\FaturamentoControleRecebimento::pesquisarFaturasParaInformeRecebimento',
            '/financeiros/faturamento-controle-recebimento/?action=pesquisar-fatura-nf-recebimento' => '\App\Controllers\FaturamentoControleRecebimento::pesquisarNFRecebimentoSemFaturaAssociada',
            "/financeiros/faturamento-controle-recebimento/?action=salvarInformeRecebimento" => '\App\Controllers\FaturamentoControleRecebimento::salvarInformeRecebimento',
            "/financeiros/faturamento-controle-recebimento/?action=finalizar-associar-fatura-nota-recebimento" => '\App\Controllers\FaturamentoControleRecebimento::finalizarAssociarFaturaNotaRecebimento',
            "/financeiros/faturamento-controle-recebimento/?action=salvar-recebimento-fatura" => '\App\Controllers\FaturamentoControleRecebimento::informarValorRecebidoFatura',

				]

];
$args = isset($_GET) && !empty($_GET) ? $_GET : null;
try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI,$args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}