<?php
set_time_limit(600);

include('../db/config.php');
include('../vendor/autoload.php');

use App\Models\Financeiro\Financeiro;
use App\Models\FileSystem\Directory;
use App\Core\Config;
use App\Services\MailAdapter;
use App\Services\Gateways\SendGridGateway;

$apiData = (object) Config::get('sendgrid');

$gateway = new MailAdapter(
    new SendGridGateway(
        new \SendGrid($apiData->user, $apiData->pass),
        new \SendGrid\Email()
    )
);

// Cria as tabelas do relatório de Fluxo de Caixa Realizado
$indexTables = Financeiro::indexTablesDemonstrativo();
$indexMsg = $indexTables ?
    'Tabelas criadas com sucesso!' :
    'Houve um erro ao criar as tabelas!';

// Limpa a pasta 'financeiros/ofx'
$path = $_SERVER['DOCUMENT_ROOT'] . '/financeiros/ofx/';
$directory = new Directory($path);
$afetados = $directory->removeFilesIntoDirectory(['ofx', 'OFX']);
$afetadosMsg = "{$afetados} arquivo(s) excluído(s)";

// Envia um email para o TI com os resultados da Cron
$titulo = "Resultados da CronTab diária";
$msg = <<<HTML
<p>
    <b>Relatório de Fluxo de Caixa Realizado: </b> {$indexMsg}
</p>
<p>
    <b>Arquivos OFX: </b> {$afetadosMsg}
</p>
HTML;
$emails = require $_SERVER['DOCUMENT_ROOT'] . '/app/configs/email.php';
$para[]= ['email' => 'ti03@mederi.com.br', 'nome' => 'TI - Mederi'];

$gateway->adapter->sendSimpleMail($titulo, $msg, $para);