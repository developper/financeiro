
$(function($) {

    ////////////////conciliacao bancaria jeferson 2013-05-22

    $(".conciliado").live('click',function(){
    //Converte em ponto flutuante e atribui o valor do saldo aos totais para realiza��o de opera��es
        // var saldo_conciliado=parseFloat($("#saldo_conciliado").html().replace('.','').replace(',','.'));
        var saldo_conciliado=parseFloat($("#saldo_conciliado").attr('saldo_conciliado'));

        if(isNaN(saldo_conciliado) == true){
            saldo_conciliado = 0;
        }

        var id_parcela = $(this).attr('id_parcela');
        var inputData = $("input[id_parcela='"+id_parcela+"'][name='data_conciliado']");


        if($(this).val() == 'S' && $(this).attr('alterado') == 'N') {
            inputData.show();
            inputData.addClass('OBG');
            valor = parseFloat($(this).attr('valor'));
            ////Atributo tipo � o tipo da nota.  0 � recebimento soma o valor com o saldo conciliado caso  1 � saida  subtrai o valor do saldo conciliado
            if($(this).attr('tipo') == 0){

                conciliado = saldo_conciliado + valor  ;
            }else{
                conciliado = saldo_conciliado  - valor;
            }
            $(this).attr('alterado','S');
            $(this).parent().parent().children('td').eq('6').children('input').eq(1).attr('alterado','S');
            $(this).parent().parent().attr('alterado','S');
            //// verificar se conciliado � maior ou menor que zero
            if(conciliado < 0){
                conciliado= conciliado.toFixed(2)
                $("#saldo_conciliado").attr('saldo_conciliado',conciliado);
                conciliado = float2moeda(conciliado);
                $("#saldo_conciliado").html(conciliado);
                $("#saldo_conciliado").addClass('textRed');

            }else{
                ////conciliado no padr�o 0.00
                conciliado= conciliado.toFixed(2)
                $("#saldo_conciliado").attr('saldo_conciliado',conciliado);
                conciliado = float2moeda(conciliado);
                $("#saldo_conciliado").html(conciliado);
                $("#saldo_conciliado").removeClass('textRed');
            }

        }

        if($(this).val() == 'N' && $(this).attr('alterado') == 'S'){
            inputData.hide();
            inputData.removeClass('OBG');

            valor = parseFloat($(this).attr('valor'));

            ////Atributo tipo � o tipo da nota.  0 � recebimento subtrai o valor com o saldo conciliado caso  1 � saida  soma o valor do saldo conciliado
            if($(this).attr('tipo') == 0){

                conciliado = saldo_conciliado - valor;
            }else{

                conciliado = saldo_conciliado  + valor;
            }

            $(this).attr('alterado','N');
            $(this).parent().parent().children('td').eq('6').children('input').eq(0).attr('alterado','N');
            $(this).parent().parent().attr('alterado','N');
            if(conciliado < 0){
                conciliado= conciliado.toFixed(2)
                $("#saldo_conciliado").attr('saldo_conciliado',conciliado);
                conciliado = float2moeda(conciliado);
                $("#saldo_conciliado").html(conciliado);
                $("#saldo_conciliado").addClass('textRed');

            }else{
                conciliado= conciliado.toFixed(2)
                $("#saldo_conciliado").attr('saldo_conciliado',conciliado);
                conciliado = float2moeda(conciliado);
                $("#saldo_conciliado").html(conciliado);
                $("#saldo_conciliado").removeClass('textRed');
            }
        }

        $(this).parent().parent().attr('conciliado',$(this).val());

    });
    $("#pesquisar-conciliacao-bancaria").click(function(){

        if(!validar_campos('div-pesquisar-conciliacao-bancaria')){
            return false;
        }

    });

    $(".desfazer-conciliacao").live('click', function () {
        if(confirm('Deseja realmente cancelar a conciliação?')) {
            var id = $(this).attr('id');
            var id_banco = $(this).attr('id_banco');
            var entrada = $(this).attr('entrada');
            var saida = $(this).attr('saida');
            var transferencia = $(this).attr('transferencia');
            var atual = $(this);
            var tipo = $(this).attr('tipo');
            var num_linha = $(this).attr('num_linha');
            var valor = $(this).attr('valor');
            var data_processado = atual.parent().parent().attr('dataDb');
            var tabela_origem = $(this).attr('tabela_origem');
            $.ajax({
                url: "/financeiros/conciliacao-bancaria/?action=desfazer-consiliacao",
                type: 'POST',
                data: {
                    id: id,
                    id_banco: id_banco,
                    entrada: entrada,
                    saida: saida,
                    transferencia: transferencia,
                    tabela_origem: tabela_origem
                },
                success: function (response) {
                    if (response == 1) {
                        atual.parent().parent().children('td').eq(6).children('center').remove();
                        atual.parent().parent().children('td').eq(7).children('input').val(data_processado);
                        atual.parent().parent().children('td').eq(7).children('input').attr('disabled', false)
                        atual.parent().parent().children('td').eq(7).children('input').attr('style', 'display:none;')
                        atual.parent().parent().attr('conciliado','N');
                        atual.parent().parent().children('td').eq(6).html(
                            "<input type='radio' alterado='N' name='conciliado_" + id + "' value='S' tipo='" + tipo + "'" +
                            " num_linha='" + num_linha + "' valor='" + valor + "' id_parcela='" + id + "' class='conciliado OBG_RADIO'  >Sim </input>" +
                            "<input type='radio' alterado='N'  class='OBG_RADIO conciliado' " +
                            " transferencia='" + transferencia + "' name='conciliado_" + id + "' value='N'" +
                            " tipo='" + tipo + "' num_linha='" + num_linha + "' valor='" + valor +
                            "' id_parcela='" + id + "' checked='checked' style='font-size:9px;'>N&atilde;o</input>"
                        );
                        atual.remove();

                        alert('Operação realizada com sucesso!');
                    } else {
                        alert(response);
                    }
                }
            });
        }

    });
    $("#table-ofx-nao-encontrado").hide();
    $("#ofx-nao-encontrado").click(function(){

        if($(this).attr('visualizar') == 0){
            $('#icone').removeClass('fa-angle-down');
            $('#icone').addClass('fa-angle-up');
            $(this).attr('visualizar',1);
            $("#table-ofx-nao-encontrado").show();


        }else{
            $('#icone').removeClass('fa-angle-up');
            $('#icone').addClass('fa-angle-down');
            $(this).attr('visualizar',0);
            $("#table-ofx-nao-encontrado").hide();

        }


    });

    $("#salvar-conciliacao").on('click',function() {
        if (validar_campos("div-conciliacao-resultado")) {
            var inicio = $(this).attr('inicio');
            var fim = $(this).attr('fim');
            var banco_id = $(this).attr('id_banco');
            var saldo_n_conciliado = $("#saldo_nconciliado").attr('saldo_nconciliado');
            var saldo_conciliado = $("#saldo_conciliado").attr('saldo_conciliado');
            qtd_linha = $(".dados").size();
            var linha = '';
            var isValidConciliacao = true;
            var data_fechamento = moment($("#data-fechamento").val());
            $(".dados").each(function (i) {
                var fornecedor = $(this).attr('fornecedor');
                var tipo_nota = $(this).attr('tipo');
                var data_pagamento = $(this).attr('data');
                var tr = $(this).attr('tr');
                var conciliado = $(this).attr('conciliado');
                var num_documento = $(this).children('td').eq(3).html();
                var valor = $(this).attr('valor_pago');
                var tabela_origem = $(this).attr('tabela_origem');               

                var id_parcela = $(this).attr('id_parcela');
                var transferencia = $(this).attr('transferencia');
                var id_parcela = $(this).attr('id_parcela');
                var inputDataConciliacao = $("input[id_parcela='" + id_parcela + "'][name='data_conciliado']");
                var inputData = inputDataConciliacao.val();
                var inputDataMoment = moment(inputDataConciliacao.val());
                if(inputDataMoment.isBefore(data_fechamento)) {
                    isValidConciliacao = !isValidConciliacao;
                    return false;
                }
                var data_conciliado = inputData;
                var alterado = $(this).attr('alterado');

                linha += tr + "$#$" + tipo_nota + "$#$" + fornecedor + "#$#$" + data_pagamento + "$#$" + conciliado + "$#$" + valor + "$#$" + num_documento + "$#$" + id_parcela + "$#$" + transferencia + "$#$" + data_conciliado + "$#$" + alterado + "$#$" +  tabela_origem;

                if (i < qtd_linha - 1) {
                    linha += "*$*";
                }
            });

            if(!isValidConciliacao) {
                alert('Uma ou mais datas de conciliação são menores que a data de fechamento do período ' +
                    '(' + data_fechamento.format('DD/MM/YYYY') + ').');
                return false;
            }

            $.ajax({
                url: "/financeiros/conciliacao-bancaria/?action=salvar-consiliacao-bancaria",
                type: 'POST',
                data: {
                    inicio: inicio,
                    fim: fim,
                    banco_id: banco_id,
                    linha: linha
                },
                success: function (response) {
                    if (response == 1) {
                        alert("Concilia\u00e7\u00e3o Salva.");
                        //$("#dialog-message").dialog('open');
                        window.location.href = "/financeiros/conciliacao-bancaria/?action=importar-ofx";

                    } else {
                        alert(response);
                        return false;
                    }
                }
            });
        }

    });




    function float2moeda(num) {

        x = 0;

        if(num<0) {
            num = Math.abs(num);
            x = 1;
        }
        if(isNaN(num)) num = "0";
        cents = Math.floor((num*100+0.5)%100);

        num = Math.floor((num*100+0.5)/100).toString();

        if(cents < 10) cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
            num = num.substring(0,num.length-(4*i+3))+'.'
                +num.substring(num.length-(4*i+3));
        ret = num + ',' + cents;
        if (x == 1) ret = ' - ' + ret;return ret;

    }
});