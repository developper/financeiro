<?php

use \App\Controllers\FechamentoCaixa;

$id = session_id();
if(empty($id))
  session_start();
include_once('../db/config.php');

class Tarifas {
    
    public static function gerenciarTarifas($id, $action){
        
        $html = "<script type='text/javascript' src='../utils/bower_components/moment/moment.js'></script>";
        if(!isset($id) || empty($id)){           
            $html .= "Erro na aplicação. Linha: ". __LINE__ . __FILE__;
            return $html;
        }else{
            $html .= "<div id='gerenciar-tarifas'>";
            $html .= "  <h1><center>Gerenciar Tarifas / Impostos</center></h1>";
            if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_bancaria_tarifa']['financeiro_conta_bancaria_tarifa_nova']) || $_SESSION['adm_user'] == 1) { 
                if ($action == 'listar') {
                    $html .= "<div style='text-align:right'>                
                            <a href='?op=tarifas-bancarias&id_conta={$id}&action=lancar'  class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button'  type='button'>
                                <span class='ui-button-text'>+ Nova</span>
                            </a>
                        </div>";
                }        
             }
            $conta = self::getContaBancariaById($id);
            $html .= "  <fieldset>";
            $html .= "      <legend>" . htmlentities('Conta Bancária') . ":</legend>";
            $html .= "      <b>Tipo:</b>&nbsp;{$conta['tipoNome']}&nbsp;&nbsp;&nbsp;";
            $html .= "      <b>Banco:</b>&nbsp;{$conta['banco']}&nbsp;&nbsp;&nbsp;";
            $html .= "      <b>Agencia:</b>&nbsp;{$conta['agencia']}&nbsp;&nbsp;&nbsp;";
            $html .= "      <b>Conta:</b>&nbsp;{$conta['conta']}&nbsp;&nbsp;&nbsp;";
            $html .= "      <b>UR:</b>&nbsp;{$conta['ur']}&nbsp;&nbsp;&nbsp;";
            $html .= "      <b>Conta Contábil:</b>&nbsp;";
            $html .= isset($conta['conta_contabil']) && !empty($conta['conta_contabil']) ? $conta['conta_contabil'] : 'Nenhuma conta associada.';
            $html .= "  </fieldset>";
            $html .= "  </div>";
            if ($action == 'lancar') {
                $fechamento_caixa = FechamentoCaixa::getFechamentoCaixa();
            $html .= "  <div id='nova-tarifa'>";
            $html .= "  <input type='hidden' id='data-fechamento' value='{$fechamento_caixa['data_fechamento']}' />";
            $html .= "      <fieldset>";
            $html .= "      <legend>Lançar tarifa:</legend>";
            $html .= "      <p>";
            $html .= "          <b>Tipo: </b>";
            $html .=            "<select id='item-tarifa' class='OBG'>";
            $html .= "              <option value=''>SELECIONE UMA TARIFA/IMPOSTO...</option>";
            foreach (self::getTiposTarifaImposto() as $tipo => $tarifas) {
                $html .= sprintf("<optgroup label='%s'>", $tipo);
                foreach ($tarifas as $tarifa) {
                    $html .= sprintf("<option value='%s'>%s</option>", $tarifa['id'], $tarifa['item']);
                }
                $html .= "</optgroup>";
            }
            $html .= "          </select>";
            $html .= "      </p>";
            $html .= "      <p>";
            $html .= "          <label for='documento'><b>No. Documento:</b></label>";
            $html .= "          <input type='text' id='doc-tarifa' placeholder='Ex.: 1A2B3C4D'/>";
            $html .= "      </p>";
            $html .= "      <p>";
            $html .= "          <label for='data-lancamento'><b>Data:</b></label>";
            $html .= "          <input type='date' min='{$fechamento_caixa['data_fechamento']}' id='data-tarifa' class='OBG' />";
            $html .= "      </p>";
            $html .= "      <p>";
            $html .= "          <label for='data-lancamento'><b>Valor:</b></label>";
            $html .= "          <input type='text' id='valor-tarifa' style='text-align:right' class='valor OBG' maxlength='15' value='0,00'/>";
            $html .= "      </p>";
            $html .= "      </fieldset>";
            $html .= "      </div>";
            $html .= "<br><div style='text-align:center'>
                            <button id-conta='{$id}' id='salvar-tarifa' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button'  type='button'>
                                <span class='ui-button-text'>Salvar</span>
                            </button>
                            &nbsp;&nbsp;
                            <button id='voltar_' type = 'button' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' >
                            <span class='ui-button-text'>Voltar</span>
                            </button>        
                        </div>";
                  
            $html .= "      <br/>";
            $html .= "  </div><br/><br/>";
        }else{
            $html .= "  <div id='buscar-impostos-tarifas'>";
            $html .= "      <fieldset>";
            $html .= "      <legend>Buscar Lançamentos:</legend>";
            $html .= "          <p>";
            $html .= "              <b>Período <label for='inicio'>Inicio</label></b>";
            $html .= "              <input type='date' class='OBG' id='inicio-tarifa' />";
            $html .= "              <b><label for='inicio'>Fim</label></b>";
            $html .= "              <input type='date' class='OBG' id='fim-tarifa'/>";
            $html .= "          </p>";
            $html .= "      </fieldset>";                  
            $html .= " <br><div style='text-align:center'>
                        <button id-conta='{$id}' id='buscar-tarifas' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button'  type='button'>
                            <span class='ui-button-text'>Pesquisar</span>
                        </button>
                        &nbsp;&nbsp;
                            <button id='voltar_' type = 'button' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' >
                            <span class='ui-button-text'>Voltar</span>
                            </button>        
                        </div>";
            $html .= "  </div>";
            $html .= "  <div id='resultado-busca'></div>";
            $html .= "</div>";
        }

            return $html;
        }
    }
    
    public static function atualizarTarifas($id, $conta){
        $html = "";
        $html = "<script type='text/javascript' src='../utils/bower_components/moment/moment.js'></script>";

        if(!isset($id) || empty($id)){           
            $html .= "Erro na aplicação. Linha: ". __LINE__ . __FILE__;
            return $html;
        }else{
            $contaBancaria = $conta;
            $html .= "<div id='atualizar-tarifas'>";
            $html .= "  <h1><center>Atualizar Tarifas / Impostos</center></h1>";
            $conta = self::getContaBancariaById($conta);
            $html .= "  <fieldset>";
            $html .= "      <legend>" . htmlentities('Conta Bancária') . ":</legend>";
            $html .= "      <b>Tipo:</b>&nbsp;{$conta['tipoNome']}&nbsp;&nbsp;&nbsp;";
            $html .= "      <b>Banco:</b>&nbsp;{$conta['banco']}&nbsp;&nbsp;&nbsp;";
            $html .= "      <b>Agencia:</b>&nbsp;{$conta['agencia']}&nbsp;&nbsp;&nbsp;";
            $html .= "      <b>Conta:</b>&nbsp;{$conta['conta']}&nbsp;&nbsp;&nbsp;";
            $html .= "      <b>UR:</b>&nbsp;{$conta['ur']}";
            $html .= "      <b>Conta Contábil:</b>&nbsp;{$conta['conta_contabil']}";
            $html .= "  </fieldset>";
            $html .= "  <div>";
            $tarifabanco = self::getTarifasById($id);
            $fechamento_caixa = FechamentoCaixa::getFechamentoCaixa();
            $html .= "  <div id='atualizar-tarifa'>";
            $html .= "      <fieldset>";
            $html .= "      <legend>Atualizar tarifa: </legend>";
            $html .= "      <p>";
            $html .= "          <b>Tipo: </b>";
            $html .=            "<select id='item-tarifa' class='OBG'>";
            $html .= "              <option value=''>SELECIONE UMA TARIFA/IMPOSTO...</option>";
            foreach(self::getTiposTarifaImposto() as $tipo => $tarifas){
                $html .= sprintf("<optgroup label='%s'>", $tipo);
                foreach ($tarifas as $tarifa) {
                    if($tarifa['item'] === $tarifabanco['item']){
                      $selected = "selected";
                    }  else {
                      $selected = "";
                    }
                    $html .= sprintf("<option value='%s' %s>%s</option>", $tarifa['id'], $selected, $tarifa['item']);
                }
                $html .= "</optgroup>";
            }
            $html .= "          </select>";
            $html .= "      </p>";
            $html .= "      <p>";
            $html .= "          <label for='documento'><b>No. Documento:</b></label>";
            $html .= "          <input type='text' id='doc-tarifa' value='{$tarifabanco['documento']}'/>";
            $html .= "      </p>";
            $html .= "      <p>";
            $html .= "  <input type='hidden' id='data-fechamento' value='{$fechamento_caixa['data_fechamento']}' />";
            $html .= "          <label for='data-lancamento'><b>Data:</b></label>";
            $html .= "          <input type='date' min='{$fechamento_caixa['data_fechamento']}' id='data-tarifa' class='OBG' value='{$tarifabanco['data']}' />";
            $html .= "      </p>";
            $html .= "      <p>";
            $html .= "          <label for='data-lancamento'><b>Valor:</b></label>";
            $html .= "          <input type='text' id='valor-tarifa' style='text-align:right' maxlength='15' class='valor OBG' value='" . Utils::money($tarifabanco['valor'], 'view') . "' />";
            $html .= "      </p>";
            $html .= "      </fieldset>";
            $html .= "      </div>";
            $html .= " <br><div style='text-align:center'>
            <button id-conta='{$contaBancaria}' id-tarifa='{$id}' id='editar-tarifa' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' aria-disabled='false' role='button'  type='button'>
                <span class='ui-button-text'>Atualizar</span>
            </button>
            &nbsp;&nbsp;
                <button id='voltar_' type = 'button' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover' role='button' aria-disabled='false' >
                <span class='ui-button-text'>Voltar</span>
                </button>        
            </div>";
            
            $html .= "      <br/>";
            $html .= "  </div>";
            $html .= "</div>";

            return $html;
        }
    }
    
    public static function buscarTarifa ($post){
        extract($post);
        $html = '';
        $data_fechamento_obj = new \DateTime(FechamentoCaixa::getFechamentoCaixa()['data_fechamento']);
        $response = self::getTarifasByPeriodo($conta, $inicio, $fim);
        if(count($response) > 0){
            foreach($response as $tipo => $data){
                $cont = 1;
                $html .= "<br></br>";
                $html .= sprintf("<center><h3 style='color:#990000;' align='center'>%sS</h3></center>",$tipo);
                $html .= "<table style='width:100%;' class='mytable font12'>";
                $html .= "  <thead>";
                $html .= "      <tr>";                            
                $html .= sprintf("<th>%s</th>",$tipo);
                $html .= "          <th>DOCUMENTO</th>";
                $html .= "          <th>DATA</th>";
                $html .= "          <th>VALOR</th>";
                $html .= "          <th>OP&Ccedil;&Otilde;ES</th>";
                $html .= "      </tr>";
                $html .= "  </thead>";
                $html .= "  <tbody>";

                foreach ($data as $tarifa) {
                    $cor = $cont % 2 == 0 ? "bgcolor='#DCDCDC'" : '';
                    $dataTarifa = \DateTime::createFromFormat('Y-m-d', $tarifa['data']);
                    $html .= "      <tr $cor >";
                    $html .= sprintf("<td>%s</td>",$tarifa['item']);
                    $html .= sprintf("<td>%s</td>",$tarifa['documento']);
                    $html .= sprintf("<td>%s</td>",$dataTarifa->format('d/m/Y'));
                    $html .= sprintf("<td>R$ %s</td>",  number_format($tarifa['valor'],2,',','.'));
                    $html .= "        <td>";
                    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_bancaria_tarifa']['financeiro_conta_bancaria_tarifa_editar']) || $_SESSION['adm_user'] == 1) {
                        if($dataTarifa > $data_fechamento_obj) {
                            $html .= "          <a href=?op=atualizar-tarifas-bancarias&id_tarifa={$tarifa['id']}&id_conta={$conta}><img src='../utils/details_16x16.png' title='Editar Tarifa' border='0'></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        }
                    }
                    if(isset($_SESSION['permissoes']['financeiro']['financeiro_conta_bancaria_tarifa']['financeiro_conta_bancaria_tarifa_excluir']) || $_SESSION['adm_user'] == 1) {
                        if($dataTarifa > $data_fechamento_obj) {
                            $html .= "          <a class='excluir-tarifa' id-tarifa='{$tarifa['id']}' id-conta='{$conta}' inicio-tarifa='{$inicio}' fim-tarifa='{$fim}'><img src='../utils/delete_16x16.png' title='Excluir Tarifa' border='0'></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        }
                    }
                    $html .= "        </td>";
                    $html .= "      </tr>";
                    $cont++;
                }
                $html .= "  </tbody>";
                $html .= "</table>";  
            }
        } else {
            $html .= "<br><br><center><h4 align='center'>Nenhum resultado foi encontrado!</h4></center>";
        }
        echo $html;
    }

    public static function salvarTarifa($post){
        extract($post);
        $fechamento_caixa = new \DateTime(\App\Controllers\FechamentoCaixa::getFechamentoCaixa()['data_fechamento']);
	$ent = $data;
	$data_entrada_obj = new \DateTime($ent);
	if($data_entrada_obj <= $fechamento_caixa) {
        echo "Data da transação (".$data_entrada_obj->format('d/m/Y').") menor qua a data de fechamento de caixa (".$fechamento_caixa->format('d/m/Y').")";
        mysql_query("rollback");
        return;
    }
        $valor  = Utils::money($valor, 'db');
        $sql    = "INSERT INTO impostos_tarifas_lancto (conta, item, documento, data, valor)
                   VALUES({$conta}, {$item}, '{$documento}', '{$data}', {$valor})";    
        $result = mysql_query($sql);
        savelog(mysql_escape_string(addslashes($sql)));
        return $result;
    }
    
    public static function editarTarifa($post){
      extract($post);
      $fechamento_caixa = new \DateTime(\App\Controllers\FechamentoCaixa::getFechamentoCaixa()['data_fechamento']);
      $ent = $data;
      $data_entrada_obj = new \DateTime($ent);
      if($data_entrada_obj <= $fechamento_caixa) {
          echo "Data da transação (".$data_entrada_obj->format('d/m/Y').") menor qua a data de fechamento de caixa (".$fechamento_caixa->format('d/m/Y').")";
          mysql_query("rollback");
          return;
      }
      $valor  = Utils::money($valor, 'db');
      $sql    = "UPDATE impostos_tarifas_lancto SET item = {$item}, documento = '{$documento}', data = '{$data}', valor = {$valor} WHERE id = {$id}";
      $result = mysql_query($sql);
      savelog(mysql_escape_string(addslashes($sql)));
      return $result;
    }
    
    public static function excluirTarifa($id){
      $sql    = "DELETE FROM impostos_tarifas_lancto WHERE id = {$id}";
      $result = mysql_query($sql);
      savelog(mysql_escape_string(addslashes($sql)));
      return $result;
    }

    private static function getContaBancariaById($idConta) {
        $sql = "SELECT
                    c.ID as id_conta,
                    o.forma as banco,
                    c.AGENCIA as agencia,
                    c.NUM_CONTA as conta,
                    e.nome as ur,
                    tipo_conta_bancaria.nome as tipoNome,
				            CONCAT(pcc.COD_N4, ' - ', pcc.N4) as conta_contabil
                FROM
                    contas_bancarias as c INNER JOIN
                    origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id) INNER JOIN empresas as e ON (c.UR = e.id)
                    LEFT JOIN tipo_conta_bancaria ON tipo_conta_bancaria.id = c.TIPO_CONTA_BANCARIA_ID
                    LEFT JOIN plano_contas_contabil AS pcc ON (pcc.ID = c.IPCC)
                WHERE
                    c.ID = {$idConta}
                ORDER BY 
                    banco";
        $result = mysql_query($sql);
        $conta = mysql_fetch_array($result, MYSQL_ASSOC);

        return $conta;
    }
    
    private static function getTiposTarifaImposto() {
        $tarifas = [];
        $sql    = "SELECT * FROM impostos_tarifas ORDER BY tipo";
        $result = mysql_query($sql);
        while($tarifa = mysql_fetch_array($result, MYSQL_ASSOC)){
            $tarifas[$tarifa['tipo']][] = $tarifa;
        }
        return $tarifas;
    }
    
    private static function getTarifasById($id){
        $sql    = " SELECT
                        imptar.item,
                        imptar.tipo,
                        itl.documento,
                        itl.data,
                        itl.valor
                    FROM 
                        impostos_tarifas imptar
                        INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
                    WHERE
                        itl.id = {$id}
                    ORDER BY 
                        imptar.tipo, itl.data";
        $result = mysql_query($sql);
        $tarifa = mysql_fetch_array($result, MYSQL_ASSOC);
        return $tarifa;
    }
    
    private static function getTarifasByPeriodo($conta, $inicio, $fim){
        $data = [];
        $sql    = " SELECT
                        itl.id,
                        imptar.item,
                        imptar.tipo,
                        itl.documento,
                        itl.data,
                        itl.valor
                    FROM 
                        impostos_tarifas imptar
                        INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
                    WHERE
                        itl.conta = {$conta}
                        AND itl.data BETWEEN '{$inicio}' AND '{$fim}'
                    ORDER BY 
                        imptar.tipo, itl.data";
        $result = mysql_query($sql);
        while($tarifa = mysql_fetch_array($result, MYSQL_ASSOC)){
            $data[$tarifa['tipo']][] = $tarifa;
        }
        return $data;
    }
}

class Utils{
  
  public static function money($number, $type) {
    if ($type === 'view') {
      return number_format($number, 2, ',', '.');
    } else if ($type === 'db') {
      return \str_replace(',', '.', str_replace('.', '', $number));
    }
  }
}

