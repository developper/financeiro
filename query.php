<?php

$id = session_id();
if(empty($id))
  session_start();
  
include_once ("db/config.php");
  
function avisos() {
    if(isset($_SESSION["avisos"]) && $_SESSION["avisos"] == true){
      $sql = "SELECT dia, info FROM atualizacoes WHERE NOW() < ADDDATE(dia,7)";
      $rs = mysql_query($sql);
      if(mysql_num_rows($rs) > 0){
      	echo "<ul>";
      	while($row = mysql_fetch_array($rs)){
      		foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
      		echo "<li>".implode("/",array_reverse(explode("-",$row['dia'])))." - {$row['info']}</li>";	
      	}
      	echo "</ul>";
      } else {
      	echo "n";
      }
      $_SESSION["avisos"] = false;
    } else {
      echo "n";
    }
}

function atualiza_lista_contatos(){
	$sql = "Select login from usuarios where logado='1' and idUsuarios<>{$_SESSION["id_user"]}";
	$r = mysql_query($sql);
	if(mysql_num_rows($r)>0){
		while($n = mysql_fetch_array($r)){
		
			echo "<p><img src='/utils/usr_financeiro_48x48.png' style='width:16px;' /><a style='color:green;padding-left:5px;' href=\"javascript:void(0)\" onclick=\"javascript:chatWith('".ucfirst($n['login'])."')\"><b>".ucfirst($n['login'])."</b></a></p>";
		}
	}else{
		echo '<center>Nenhum Usu&aacute;rio Conectado!</center>';
	}
		
	
}

function troca_senha($post){
	extract($post,EXTR_OVERWRITE);
	$sql = "UPDATE usuarios SET `senha` =  md5('{$nova}') WHERE `idUsuarios` = '{$_SESSION['id_user']}' AND `senha` = md5('{$atual}')";
	$r = mysql_query($sql);
	$n = mysql_affected_rows();
	if($n == 0){
		echo "-1";
		return;
	}
	echo "1";
}

function enviar_msg($post){
	extract($post,EXTR_OVERWRITE);
	$sql = "INSERT INTO msgs (`origem`, `destino`, `titulo`, `msg`, `data`) VALUES ('{$_SESSION['id_user']}', '$dest', '$titulo', '$msg', now()) ";
	$r = mysql_query($sql);
	if(!$r){
		echo "-1";
		return;
	}
	echo "1";
}

function show_msg($post){
	extract($post,EXTR_OVERWRITE);
	$sql = "SELECT m.msg FROM msgs as m WHERE id = '$cod'";
	$r = mysql_query($sql);
	if(!$r){
		echo "ERRO: Tente mais tarde!";
		return;
	}
	$row = mysql_fetch_array($r);
	echo $row['msg'];
}

function del_msg($post){
	extract($post,EXTR_OVERWRITE);
	$sql = "DELETE FROM msgs WHERE id = '$cod'";
	$r = mysql_query($sql);
	if(!$r){
		echo "-1";
		return;
	}
	echo "Mensagem apagada com sucesso";
}

function get_msg(){
	$r = mysql_query("SELECT m.id, DATE_FORMAT(data,'%d/%m/%Y - %H:%i:%s') as data, m.titulo, m.msg, u.nome FROM msgs as m, usuarios as u 
  					WHERE m.origem = u.idUsuarios AND (m.destino = '{$_SESSION['id_user']}' OR m.destino = '-1') ORDER BY data DESC ");
    echo "<div id='caixa-entrada'><table class='mytable' width=100% >"; 
    echo "<thead><tr>"; 
    echo "<th><b>Entrada</b></th>";  
    echo "</tr></thead>";
    $cor = false;
    while($row = mysql_fetch_array($r)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
      if($cor) echo "<tr>";
      else echo "<tr class='odd' >";
      $cor = !$cor; 
      $ler = "<img cod='{$row['id']}' src='../utils/read_msg_16x16.png' title='Detalhes' border='0' class='ler-msg' t='{$row['titulo']}'>";
      echo "<td valign='top'><b>{$row['data']}:</b> - {$row['nome']} : {$row['titulo']} $ler</td>";  
      echo "</tr>";
    }
  echo "</table></div>";
  echo "<br/><br/>";
  $r = mysql_query("SELECT m.id, DATE_FORMAT(data,'%d/%m/%Y - %H:%i:%s') as data, m.titulo, m.msg, u.nome FROM msgs as m, usuarios as u 
  					WHERE m.origem = u.idUsuarios AND m.origem = '{$_SESSION['id_user']}' ORDER BY data DESC ");
    echo "<div id='caixa-saida'><table class='mytable' width=100% >"; 
    echo "<thead><tr>"; 
    echo "<th><b>Sa&iacute;da</b></th>";  
    echo "</tr></thead>";
    $cor = false;
    while($row = mysql_fetch_array($r)){ 
      foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
      if($cor) echo "<tr>";
      else echo "<tr class='odd' >";
      $cor = !$cor; 
      $ler = "<img cod='{$row['id']}' src='../utils/read_msg_16x16.png' title='Detalhes' border='0' class='ler-msg' t='{$row['titulo']}'>";
      $del = "<img cod='{$row['id']}' src='../utils/delete_16x16.png' title='Apagar' border='0' class='del-msg' t='{$row['titulo']}'>";
      echo "<td valign='top'><b>{$row['data']}:</b> - {$row['nome']} : {$row['titulo']} $ler  $del</td>";  
      echo "</tr>";
    }
    echo "</table></div>";
}

function excluir_nota($post){
	extract($post,EXTR_OVERWRITE);
	$sql = "DELETE FROM notas WHERE idNotas = '$cod'";
	$r = mysql_query($sql);
	if(!$r){
		echo "-1";
		return;
	}
	echo "1";
}

switch ($_POST["query"]) {
  case "avisos":
      avisos();
      break;
  case "troca-senha":
      troca_senha($_POST);
      break;
  case "enviar-msg":
      enviar_msg($_POST);
      break;
  case "get-msg":
      get_msg();
      break;
  case "show-msg":
      show_msg($_POST);
      break;
  case "del-msg":
      del_msg($_POST);
      break;
  case "excluir-nota-aberto":
      excluir_nota($_POST);
      break;
  case "atualiza-contato":
      atualiza_lista_contatos();
      break;
  default:
      echo "n";
      break;
  
}


?>