<?php
/**
 * Front-controller
 */

if (preg_match('/\.(?:png|jpg|jpeg|gif|js|css)$/', $_SERVER["REQUEST_URI"]))
    return false;    // serve the requested resource as-is.

require_once __DIR__.'/../app/bootstrap.php';

use App\Core\Kernel;
use \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

$kernel = new Kernel('dev');

$app = new Silex\Application();

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => array(
        ROOT.'/app/Resources/views',
        ROOT.'/app/Modules/Application/Resources/views'
    ),
));

$app['debug'] = true;

$app->get('/hello/{nome}', function($nome) use ($app) {
   return $app['twig']->render('login.twig', array('nome'=> $nome));
});

$app->error(function (NotFoundHttpException $e, $code) use ($app) {
    // Chama o script requisitado (Versão Antiga)
    require_once $_SERVER['SCRIPT_FILENAME'];
    exit;
});

$app->run();
