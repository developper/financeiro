<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang='pt-br' xml:lang='pt-br' ng-app="financeiro">
<head>
    <!-- Hotjar Tracking Code for http://sistema.mederi.com.br -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:713258,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109307914-1"></script>
    <script src="https://unpkg.com/regenerator-runtime@0.13.1/runtime.js"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-109307914-1');
    </script>
    <script type="text/javascript">
        var STATIC_URL = "<?php echo "http://" . $_SERVER['SERVER_NAME']. "/"; ?>";
    </script>

    <link rel="stylesheet" type="text/css" href="/utils/jquery/css/coutdown/jquery.countdown.css">

    <link rel="shortcut icon" href="/favicon.ico"/>
    <?php
    /**
     * @var $allowed Lista com as seções do sistema onde é permitido usar o localstorage
     */
    $allowed = [
        'newcapmed',
        'anterior_capmed',
        'listcapmed',
        'buscar',
        'novo-exame',
        'exames-novo',
        'editar-exame',
        'pesquisar-form',
        'pesquisar-remocao',
        'convenio-form',
        'finalizar-remocao-convenio-form',
        'particular-form',
        'finalizar-remocao-particular-form',
        'relatorio-aluguel-equipamentos',
        'equipamentos-fornecedor-form',
        'form-eventos-aph',
        'pesquisar-evento-aph',
        'finalizar-evento-aph',
        'custos-extras-form',
        'salvar-custos-extras',
        'feridas',
        'feridas-listar',
        'planos',
        'buscar-orcamentos-autorizacao',
        'pesquisar-orcamento-relatorio-autorizacao',
        'encaminhamento-listar',
        'encaminhamento-novo',
        'buscar-lista-encaminhamentos',
        'atualizar-encaminhamento',
        'buscar-orcamentos-autorizacao-auditoria',
        'pesquisar-orcamento-relatorio-autorizacao-auditoria',
        'formSolicitarDevolucaoInterna',
        'buscarPedidosEnviadosENaoConfirmados',
        'formCancelarItensNaoConfirmados',
        'cancelarPedidosEnviadosENaoConfirmados',
        'pesquisarSolicitacaoDevolucaoInterna',
        'visualizarSolicitacaoDevolucaoInterna',
        'enviarItensSolicitacaoDevolucaoInterna',
        'confirmarEntregaItensSolicitacaoDevolucaoInterna',
        'relatorioAjusteEstoque',
        'form-conciliacao',
        'importar-ofx',
        'pesquisar-conciliacao-bancaria',
        'ocorrencia',
        'listar-ocorrencias',
        'usuario',
        'usuario-editar',
        'listar-usuarios',
        'consensoPesquisarFatura',
        'formGerarConsenso',
        'salvarConsensoFatura',
        'listar-categorias-materiais',
        'adicionarPrevisaoRecebimento',
        'pesquisarFaturamentoEnviado',
        'relatorioPrevisaoRecebimento',
        'pesquisarRelatorioPrevisaoRecebimento',
        'associar-fatura-nf-recebimento',
        'pesquisar-fatura-nf-recebimento',
        'autorizacao-ur',
        'adicionarInformeRecebimento',
        'relatorio-saida-remessa',
        'editar-remocao-convenio',
        'editar-remocao-convenio-finalizada',
        'editar-remocao-convenio-nao-finalizada',
        'editar-remocao-particular',
        'editar-remocao-particular-finalizada',
        'editar-remocao-particular-nao-finalizada',
        'relatorio-remocoes-realizadas',
        'relatorio-eventos-aph-realizados',
        'gerenciar-email-fornecedor-equipamento-form',
        'fornecedor-equipamento-menu',
        'gerenciar-email-fornecedor-equipamento-form',
        'form-relatorio-pacientes-faturar',
        'formulario-condensado-listar',
        'formulario-condensado-form-editar',
        'formulario-condensado',
        'listar-relatorios-prestador',
        'listar-form',
        'prestador-form',
        'prestador-form-editar',
        'buscar-ficha-atendimento-domiciliar',
        'ficha-atendimento-domiciliar',
        'ficha-atendimento-domiciliar-editar',
        'configuracao-retencoes',
        'criar-fatura-imposto',
        'index-fatura-imposto',
        'pesquisar-notas-aglutinacao',
        'index-bordero',
        'criar-bordero',
        'editar-bordero',
        'importar-retorno-cnab',
        'upload-retorno-cnab',
        'fechamento-caixa',
        'index-conta-pagar-iw',
        'index-conta-antecipada',
        'criar-conta-antecipada',
        'salvar-conta-antecipada',
        'pesquisar-contas-antecipadas',
        'visualizar-conta-antecipada',
        'editar-conta-antecipada',
        'visualizar-conta-antecipada',
        'compensar-parcela',
        'saldos-contas-bancarias',
        'posicao-fornecedores',
        'notas-emitidas',
        'pesquisar-notas-emitidas',
        'notas-com-processamento',
        'pesquisar-notas-com-processamento',
        'index-caixa-fixo',
        'caixa-fixo-criar-conta',
        'pesquisar-caixa-fixo',
        'caixa-fixo-editar-conta',
        'orcamento-gerencial-visualizar',
        'form-integracao',
        'pesquisar-relatorio-fluxo-caixa',
        'relatorio-fluxo-caixa',
        'orcamento-financeiro',
        'relatorio-orcamento-financeiro',
        'orcamento-financeiro-index',
        'index-previsao',
        'pesquisar-parcela-previsao',
        'baixar-previsao',
        'cancelar-parcelas-previsao',
        'relatorio-gerencial-orcamento-financeiro',
        'cadastro-valores-gerenciais',
        'caixa-fixo-visualizar-conta',
        'criar-multiplas-notas',
        'pesquisar-relatorio-fluxo-caixa-completo',
        'upload-varredura-cnab'
        
    ];
    $url = parse_url($_SERVER['REQUEST_URI']);
    @parse_str($url['query'], $query);
    if( array_search(@$query['act'], $allowed) !== false ||
        array_search(@$query['view'], $allowed) !== false ||
        array_search(@$query['action'], $allowed) !== false ||
        array_search(@$query['adm'], $allowed) !== false ||
        (
            @$query['adm'] == 'paciente' &&
            @$query['act'] == 'listar' &&
            $url['path'] == '/adm/'
        ) ||
        (
            @$query['view'] == 'paciente_med' &&
            @$query['act'] == 'listar' &&
            $url['path'] == '/medico/'
        ) ||
        (
            $url['path'] == '/enfermagem/' &&
            @$query['view'] == 'listar'
        ) ||
        (
            $url['path'] == '/nutricao/' &&
            @$query['view'] == 'listar'
        ) ||
        (
            $url['path'] == '/financeiros/' &&
            @$query['op'] == 'fornecedor'&&            
           ( @$query['act'] == 'listar' || @$query['act'] == 'novo' || 'editar')
        ) ||
        (
            $url['path'] == '/financeiros/' &&
            @$query['op'] == 'notas'&&
            ( @$query['act'] == 'listar')
        )
    ):
        ?>
    <link rel="stylesheet" type="text/css" href="/utils/bootstrap-3.3.6-dist/css/chosen.bootstrap.min.css">
    <link type="text/css" href="/utils/jquery/css/mederi/jquery-ui-1.11.4.min.css" rel="stylesheet" />
        <script type="text/javascript" src="/utils/jquery/js/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="/utils/jquery/js/jquery-ui-1.11.4.min.js"></script>
        <script src="/utils/jquery/js/jquery-migrate-1.2.1.min.js"></script>
        <script src="/utils/helpers/helper.js?v=2" type="text/javascript"></script>
        <script src="/utils/helpers/garlic.min.js?v=2" type="text/javascript"></script>
        <script src="/utils/helpers/localstorage.js?v=3" type="text/javascript"></script>
    <?php else: ?>
    <link rel="stylesheet" type="text/css" href="/utils/jquery/css/chosen/chosen.css">
    <link type="text/css" href="/utils/jquery/css/mederi/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
        <script type="text/javascript" src="/utils/jquery/js/jquery-1.4.4.min.js"></script>
        <script type="text/javascript" src="/utils/jquery/js/jquery-ui-1.8.9.custom.min.js"></script>
    <?php endif; ?>
    <script type="text/javascript" src="/utils/jquery/js/jquery.countdown.min.js"></script>

    <style type="text/css">@import "/utils/jquery/js/jquery.calculator.alt.css";</style>
    <script type="text/javascript" src="/utils/jquery/js/jquery.calculator.js"></script>
    <script type="text/javascript" src="/utils/jquery/js/jquery.calculator-pt-BR.js"></script>

    <script type="text/javascript" src="/utils/jquery/js/jquery.ui.datepicker-pt-BR.js"></script>

    <!--<script type="text/javascript" src="/utils/jquery/js/jquery.price_format.1.3.js"></script>-->
    <script type="text/javascript" src="/utils/jquery/js/jquery.price_format.1.8.js"></script>

    <script type="text/javascript" src="/utils/jquery/js/jquery.blockUI.js?v=2"></script>

    <style type="text/css">@import "/utils/jquery/jquery.timeentry/jquery.timeentry.css";</style>
    <script type="text/javascript" src="/utils/jquery/jquery.timeentry/jquery.timeentry.js"></script>

    <script type="text/javascript" src="/utils/jquery/js/easyTooltip.js"></script>

    <script>$(function($){ $('#calculadora').calculator(); });</script>

    <script type="text/javascript" src="/principal.js?v=7.5"></script>
    <script type="text/javascript" src="/utils/jquery/js/jquery.cookie.js"></script>

    <script type="text/javascript" src="/utils/jquery/js/chosen.jquery.min.js"></script>

    <script type="text/javascript" src="/utils/jquery/js/jquery.maskedinput-1.3.js"></script>

    <script type="text/javascript" src="/utils/alertify/alertify.min.js"></script>
    <link rel="stylesheet" href="/utils/alertify/alertify.core.css" />
    <link rel="stylesheet" href="/utils/alertify/alertify.bootstrap.css" />
    <link rel="stylesheet" href="/utils/ribbon.css" />

    <script type="text/javascript" src="/utils/Modernizr/modernizr.custom.09263.js"></script>
    <script type="text/javascript" src="/enfermagem/upload.js?v=4"></script>
    <script type="text/javascript" src="/utils/jquery/js/jquery.maskMoney-3.1.1.min.js"></script>
