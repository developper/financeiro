<?php

$id = session_id();
if(empty($id))
  session_start();

include_once('../db/config.php');
include_once('../utils/codigos.php');
include_once("../utils/class.phpmailer.php");

use App\Core\Config;
use App\Services\Gateways\SendGridGateway;
use App\Services\MailAdapter;

if(!function_exists('redireciona')){
	function redireciona($link){
		if ($link==-1){
			echo" <script>history.go(-1);</script>";
		}else{
			echo" <script>document.location.href='$link'</script>";
		}
	}
}

function salvar_auditoria($post){
    extract($post,EXTR_OVERWRITE);
    //echo '<pre>'; die(var_dump($post));
	mysql_query("begin");
	$enfermeiro = $_SESSION["id_user"];
	$c = 0;
	$itens = explode("$",$dados);
	$enfermeiros = array();
	$enfermeiros_emergencia = array();
	$id = -1;
	$auditora = $_SESSION["nome_user"];
	$negado=0;
    $countPareceres = 0;
    $emailParecerHTML = "<p>Pareceres do(a) auditor(a) <b>{$auditora}</b> sobre itens de entrega imediata do(a) paciente <b>{$np}</b>: </p>";

	foreach($pareceres as $key => $parecer) {
        $countPareceres++;
        $parecerText = addslashes($parecer['parecer']);
        $sql = "INSERT INTO parecer_entrega_imediata
                VALUES (NULL, '{$parecer['solicitacao']}', '{$parecerText}', NOW(), '{$enfermeiro}')";
        $rs = mysql_query($sql) or die(mysql_error());
        if(!$rs){
            mysql_query("rollback");
            echo "Erro ao salvar um parecer! ";
        }
        $emailParecerHTML .= "<p>Parecer sobre {$parecer['apresentacao']}:<br>{$parecer['parecer']} <br/> <hr> </p>";
    }

    $pendente = [];

	foreach($itens as $item){
		$c++;
		$i = explode("#",str_replace("undefined","",$item));
		$id = anti_injection($i[0],'numerico');
		$s = anti_injection($i[1],'numerico');
		$enf = anti_injection($i[2],'numerico');
		$nome = anti_injection($i[3],'literal');
		$e = anti_injection($i[4],'numerico');
		$camp = anti_injection($i[5],'numerico');
                $just = anti_injection($i[6], 'literal');
                $idJustItemNegado= anti_injection($i[7], 'numerico');
                $textJustItemNegado= anti_injection($i[8], 'literal');
		$textAdicional = anti_injection($i[9], 'literal');
		$isPendente = anti_injection($i[10], 'literal') == 'S';
		$qtdSol = anti_injection($i[11], 'literal');
		if($isPendente && $e != '2' && $s == '1') {
            $pendente[$id]['item'] = $nome;
            $pendente[$id]['autorizado'] = $camp;
            $pendente[$id]['qtd'] = $qtdSol;
            $pendente[$id]['justNegado'] = $textJustItemNegado;
        }

		if($e == 2){
            $sql = "UPDATE solicitacoes SET autorizado = '{$camp}',data_auditado=now(), AUDITOR_ID='{$enfermeiro}' WHERE idSolicitacoes = '{$id}' ";
        }else{

            $sql = "UPDATE solicitacoes SET status = '{$s}',autorizado = '{$camp}', "
                . "data_auditado=now(),obs='{$i[6]}',JUSTIFICATIVA_ITENS_NEGADOS_ID={$i[7]},AUDITOR_ID='{$enfermeiro}' WHERE idSolicitacoes = '{$id}' ";

        }
		$r = mysql_query($sql);
		if(!$r){
			//echo mysql_error();
			mysql_query("rollback");
                        echo "Ao fazer um update na tabela solicitações";
			return;
		}
		savelog(mysql_escape_string(addslashes($sql)));
		if($s == '-1'){
                    $negado++;

                    $corpo_email .= "<b>Item:</b> {$nome}<br/> <b>Identificação:</b> {$textAdicional} <br/> <b>Justificativa:</b> {$textJustItemNegado} : {$just} <br/> ______________________<br/>";

			if($e == 0){
				if(!array_key_exists($enf,$enfermeiros))
					$enfermeiros[$enf] = array();
				array_push($enfermeiros[$enf],$nome);
			}else if($e == 2){
				if(!array_key_exists($enf,$enfermeiros_emergencia))
				$enfermeiros_emergencia[$enf] = array();
				array_push($enfermeiros_emergencia[$enf],$nome);
			}
                }else{

            /* PEGANDO O NOME DO PACIENTE PARA SETAR NO PAINEL DA LOGISTICA */
           // $consulta = mysql_fetch_array(mysql_query("SELECT C.nome FROM clientes AS C WHERE C.idClientes='{$paciente}'"));
            /*
              $_SESSION['NON'] = $consulta['nome'];
              $_SESSION['status'] = $status;
             */
            $arquivo = 'UR_' .$empresa;

            # Verifica se existe o arquivo
            if (file_exists("../logistica/chamada_logistica/{$arquivo}.txt")) {

                # Verifica se o arquivo ta vazio, caso esteja acrescenta o nome sem o |,
                # pois o pipiline será o token para o explode gerar o array no saida.php de logistica
                # Caso contrario ele pega o conteudo do arquivo e concatena com | e o nome do proximo paciente
                if (filesize("../logistica/chamada_logistica/{$arquivo}.txt") == 0) {
                    $conteudo = $consulta['nome'];
                } else {
                    # Verificar a quantidade de pessoas dentro da lista, se for maior que a comparaçao zera a lista e
                    # reescreve o arquivo da ur com o nome do último paciente solicitado.
                    # Caso contrário segue acumulando os nomes na lista. OBS.: Isso server para evitar listas grandes,
                    # na tela da logistica.
                    if (count(explode(" | ", file_get_contents("../logistica/chamada_logistica/{$arquivo}.txt"))) >= 6) {
                        $conteudo = $np;
                    } else {
                        $conteudo = file_get_contents("../logistica/chamada_logistica/{$arquivo}.txt") . ' | ' . $np;
                    }
                }
            } else {
                $conteudo = $np;
            }

            file_put_contents("../logistica/chamada_logistica/{$arquivo}.txt", $conteudo);
                $_SESSION['status']==1;

                }
	}
	foreach($enfermeiros as $enf => $itens){
		$titulo = "({$np}) Itens negados pela auditoria";
		$msg = "<p>A solicita&ccedil;&atilde;o do paciente {$np} teve os seguintes itens negados:</p><br/><br/><ul>";
		foreach($itens as $item){
			$msg .= "<li>".$item."</li>";
		}
		$msg .= "</ul>";
		$sql = "INSERT INTO msgs (`origem`, `destino`, `titulo`, `msg`, `data`) VALUES ('{$_SESSION['id_user']}', '{$enf}', '$titulo', '$msg', now()) ";
		$r = mysql_query($sql);
		if(!$r){
			//echo mysql_error();
			mysql_query("rollback");
                         echo "Ao inserir na tabela msgs itens normais.";
			return;
		}
	}
	foreach($enfermeiros_emergencia as $enf => $itens){
		$titulo = "({$np}) Itens emergenciais negados pela auditoria";
		$msg = "<p>A solicita&ccedil;&atilde;o do paciente {$np} precisar&aacute; de relat&oacute;rio dos itens:</p><br/><br/><ul>";
		foreach($itens as $item){
			$msg .= "<li>".$item."</li>";
		}
		$msg .= "</ul>";
		$sql = "INSERT INTO msgs (`origem`, `destino`, `titulo`, `msg`, `data`) VALUES ('{$_SESSION['id_user']}', '{$enf}', '$titulo', '$msg', now()) ";
		$r = mysql_query($sql);
		if(!$r){
			//echo mysql_error();
			mysql_query("rollback");
                         echo "Ao inserir na tabela msgs itens de emergencia";
			return;
		}
	}
	mysql_query("commit");
    ////////Se algum item foi negado deve ser enviado um e-mail informando o item e sua justificativa.
    $para = [];
    if($negado > 0){
		date_default_timezone_set('America/Bahia');
		$dataAuditado = date('d/m/Y');
		$hora =  date('H:i');
        $titulo = "({$np}) Itens Negados Auditoria - Sistema de Gerenciamento da Mederi";
        $msgHTML = "<p>A solicitação do paciente <b>".$np."</b>, tiveram os seguintes itens negados:</p>".$corpo_email." pelo(a) auditor(a) $auditora. <br/> Auditada em {$dataAuditado} às {$hora}  (Horário da Bahia).";
		$temp = require $_SERVER['DOCUMENT_ROOT'] . '/app/configs/email.php';
		$para[] = ['email' => $temp['lista']['emails']['enfermagem'][$empresa]['email'], 'nome'=> $temp['lista']['emails']['enfermagem'][$empresa]['nome']];
		if($empresa == 11) {
			$para[] = ['email' => 'enfermagem.urfsa@mederi.com.br', 'nome' => 'Enfermagem - FSA'];
		}
		if($empresa == 7) {
			$para[] = ['email' => 'coordenacaoenf@cuidarevita.com.br', 'nome' => ' '];
			$para[] = ['email' => 'enfplantonista.sudoeste@mederi.com.br', 'nome' => 'Enfermagem - Sudoeste'];
		}
        enviarEmailSimples($titulo, $msgHTML, $para);
        //enviarEmail($np, 14, $empresa, $msgHTML);
    }

    $para = [];
    if($countPareceres > 0) {
        $temp = require $_SERVER['DOCUMENT_ROOT'] . '/app/configs/email.php';
		$titulo = "({$np}) Parecer da Auditoria Sobre Itens de Entrega Imediata - Sistema de Gerenciamento da Mederi";
		$para[] = ['email' => 'coord.auditoria@mederi.com.br','nome' => 'Coordenação de Auditoria'];

        $para[] = ['email' => $temp['lista']['emails']['enfermagem'][$empresa]['email'], 'nome'=> $temp['lista']['emails']['enfermagem'][$empresa]['nome']];
		if($empresa == 11) {
			$para[] = ['email' => 'enfermagem.urfsa@mederi.com.br', 'nome' => 'Enfermagem - FSA'];
		}
		if($empresa == 7) {
			$para[] = ['email' => 'coordenacaoenf@cuidarevita.com.br', 'nome' => ' '];
			$para[] = ['email' => 'enfplantonista.sudoeste@mederi.com.br', 'nome' => 'Enfermagem - Sudoeste'];
		}
        enviarEmailSimples($titulo, $emailParecerHTML, $para);
    }

    $para = [];
    if(count($pendente) > 0) {
        $temp = require $_SERVER['DOCUMENT_ROOT'] . '/app/configs/email.php';
        $msg = "<p><b>Alguns itens pendentes do paciente {$np} foram atualizados: </p><ul>";
        foreach ($pendente as $p) {
            $decisao = "<li>{$p['autorizado']} de {$p['qtd']} Autorizado(s)</li> <li>Liberado por {$_SESSION['nome_user']}</li>";
            $msg .= "<li>{$p['item']}<ul>{$decisao}</ul></li>";
        }
        $msg .= "</ul>";
        $titulo = "({$np}) Itens pendentes liberados - Sistema de Gerenciamento da Mederi";
        $para[] = ['email' => $temp['lista']['emails']['enfermagem'][$empresa]['email'], 'nome'=> $temp['lista']['emails']['enfermagem'][$empresa]['nome']];
		$para[] = ['email' => 'saad@mederi.com.br', 'nome'=> 'SAAD Mederi'];
        enviarEmailSimples($titulo, $msg, $para);
    }
    echo "1";
}

function salvar_prontuario($post,$file){
  extract($post,EXTR_OVERWRITE);
  $paciente = anti_injection($paciente,'numerico');
  $pasta = anti_injection($pasta,'numerico');
  $arquivo = $file["tmp_name"];
  $tamanho = $file["size"];
  $tipo = $file["type"];
  $nome = $file["name"];

  if ( $arquivo != "none" ){
    $fp = fopen($arquivo, "rb");
    $conteudo = fread($fp, $tamanho);
    $conteudo = addslashes($conteudo);
    fclose($fp);

    $sql = "INSERT INTO prontuarios (`paciente`, `pasta`, `data`, `nome`, `tipo`, `size`, `content`) VALUES ('$paciente', '$pasta', now(), '$nome', '$tipo', '$tamanho', '$conteudo')";
    mysql_query($sql);
    savelog(mysql_escape_string(addslashes($sql)));
	redireciona("-1");
  }
}

function salvar_observacao($post){
    extract($post,EXTR_OVERWRITE);
    $observacao = anti_injection($observacao, "literal");
    $sql = "INSERT INTO observacoes_auditoria (PACIENTE_ID,DATA,CRIADOR,OBSERVACAO) values ('{$id}',now(),'{$_SESSION["id_user"]}','{$observacao}')";
//echo $sql;
            $r = mysql_query($sql);

            if (!$r) {
                echo "Erro: tente novamente 1";
                mysql_query("rollback");

                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));

    $sql = "SELECT
                    o.OBSERVACAO as obs,
                    DATE_FORMAT(o.DATA,'%d/%m/%Y %h:%m:%s') as data,
                    u.nome as criador
                FROM
                    observacoes_auditoria AS o INNER JOIN
                    usuarios as u on (o.CRIADOR = u.idUsuarios)
                WHERE
                    o.PACIENTE_ID = '{$id}'
                ORDER BY
                    o.ID desc limit 1";
        $result = mysql_query($sql);
        while($row = mysql_fetch_array($result)){
            echo "<tr><td>{$row['obs']}</td><td>{$row['data']}</td><td>{$row['criador']}</td></td></tr>";
        }
        mysql_query("commit");
}

############################################ TROCAR ITENS ###########################################################

function cancelar_item_solicitacao($post){
	extract($post,EXTR_OVERWRITE);
	$sol = anti_injection($sol,'numerico');
	$just = anti_injection($just,'literal');
	$just = "CANCELADO PELA AUDITORIA: ".$just;
        $sql="UPDATE solicitacoes SET status = '-2', obs = '{$just}', CANCELADO_POR='{$_SESSION['id_user']}',DATA_CANCELADO=now() WHERE idSolicitacoes = '{$sol}' ";
	$r = mysql_query($sql);
	if(!$r){
		echo "ERRO: Tente mais tarde!";
		return;
	}
        savelog(mysql_escape_string(addslashes($sql)));
	echo "1";
	return 1;
}

function busca_itens($like,$tipo){
	$sql = "";
	$label_tipo = "";
	$like = anti_injection($like,'literal');
	switch($tipo){
		case 'kits':
			$label_tipo = "KITS";
			$sql = "SELECT k.nome as nome, k.idKit as cod FROM kitsmaterial as k WHERE k.nome LIKE '%$like%' ORDER BY k.nome";
			break;
		case 'mat':
			$label_tipo = "MATERIAIS";
			$sql = "SELECT concat(mat.nome,' ',mat.descricao) as nome, mat.idMaterial as cod FROM material as mat WHERE mat.nome LIKE '%$like%' OR mat.descricao LIKE '%like%' ORDER BY nome";
			break;
		case 'med':
			$label_tipo = "MEDICAMENTOS";
			$sql = "SELECT concat(med.principioAtivo,' ',com.nome) as nome, com.idNomeComercialMed as cod FROM medicacoes as med, nomecomercialmed as com WHERE (med.principioAtivo LIKE '%$like%' OR com.nome LIKE '%$like%') AND med.idMedicacoes = com.Medicacoes_idMedicacoes ORDER BY nome";
			break;
	}
	$result = mysql_query($sql);
	echo "<table class='mytable' width=100% id='lista-itens' ><thead><tr><th>{$label_tipo}</th></tr></thead>";
	while($row = mysql_fetch_array($result)){
		foreach($row AS $key => $value) {
		$row[$key] = stripslashes($value);
		}
		echo "<tr cod='{$row['cod']}' ><td>{$row['nome']}</td></tr>";
	}
	echo "</table>";
	echo "<script type='text/javascript' >";
	echo " $('#lista-itens tr').dblclick(function(){
	$('#nome').val($(this).text());
	$('#cod').val($(this).attr('cod'));
	$('#lista-itens').html('');
	$('#busca-item-troca').val('');
	$(\"#formulario-troca input[name='qtd']\").focus();
});";
echo "</script>";
}

function trocar_item_solicitacao($post){

		extract($post,EXTR_OVERWRITE);

		$sol = anti_injection($sol,'numerico');
		//$presc = anti_injection($presc,'numerico');
		$just = anti_injection($just,'literal');
		$p = anti_injection($p,'numerico');
		$item = anti_injection($item,'literal');
		$qtd = anti_injection($qtd,'numerico');
		$n = anti_injection($n,'literal');
		$itemOriginal = anti_injection($itemOriginal,'literal');

                $catalogo_id = anti_injection($catalogo_id,'numerico');
                $entrega_imediata = anti_injection($entrega_imediata,'literal');
				$catalogoAntigo = anti_injection($catalogoAntigo,'numerico');

	$r = mysql_query("Select
CATALOGO_ID,
idPrescricao,
TIPO_SOLICITACAO,
concat(catalogo.principio, '- ',catalogo.apresentacao) as item,
DATA_MAX_ENTREGA,
entrega_imediata,
solicitacoes.status,
solicitacoes.qtd,
solicitacoes.enviado,
(Case  entrega_imediata when   'S' THEN solicitacoes.qtd else solicitacoes.autorizado  END ),
solicitacoes.DATA_SOLICITACAO,
solicitacoes.TIPO_SOL_EQUIPAMENTO,
solicitacoes.CHAVE_AVULSA,
solicitacoes.JUSTIFICATIVA_AVULSO,
solicitacoes.JUSTIFICATIVA_AVULSO_ID,
solicitacoes.id_prescricao_curativo,
solicitacoes.primeira_prescricao,
solicitacoes.relatorio_aditivo_enf_id,
solicitacoes.tipo,
solicitacoes.id_periodica_complemento
from
solicitacoes inner JOIN
catalogo on (catalogo.ID = solicitacoes.CATALOGO_ID)
WHERE
idSolicitacoes = '{$sol}'
and paciente= '{$p}'");
	if(!$r){
		echo "ERRO: Tente mais tarde! COD: 00";
		mysql_query("rollback");
		return;
	}

	$tipoSolicitacao = mysql_result($r, 0, 2);
	$dataMaxEntrega = mysql_result($r, 0, 4);
	$entregaImediata = mysql_result($r, 0, 5);
	$status = mysql_result($r, 0, 6);
	$dataSolicitacao =mysql_result($r, 0, 10);
	$tipoSolEquipamento = mysql_result($r, 0, 11);
	$chaveAvulsa = mysql_result($r, 0, 12);
	$justificativaAvulsa = mysql_result($r, 0, 13);
	$justificativaAvulsaId = mysql_result($r, 0, 14);
	$idPrescricaoCurativo =  mysql_result($r, 0, 15);
	$primeiraPrescricao = mysql_result($r, 0, 16);
	$relatorioAditivoEnfId = mysql_result($r, 0, 17);
	$tipoItem  = mysql_result($r, 0, 18);
	$idPeriodicaComplemento  = mysql_result($r, 0, 19);


		$just = "TROCADO PELA AUDITORIA POR: ".$n." MOTIVO: ".$just;

		//a pres foi alterade de A para B por causa de C. mandar por email para msgsistema.
		mysql_query("begin");
                $sql="UPDATE solicitacoes SET  status = '-2',
obs = '{$just}',
trocado = 'S'
 WHERE idSolicitacoes = '{$sol}' ";
		$r = mysql_query($sql);
		if(!$r){
		echo "ERRO: Tente mais tarde1!";
		mysql_query("rollback");
		return;
		}
                savelog(mysql_escape_string(addslashes($sql)));

		///jeferson 2012-10-24
                $sql="Select e.ID, s.NUMERO_TISS, e.CAPMEDICA_ID,s.CATALOGO_ID from solicitacoes as s inner join equipamentosativos as e on (e.COBRANCA_PLANOS_ID = s.CATALOGO_ID) WHERE idSolicitacoes = '{$sol}'and e.PACIENTE_ID= '{$p}' and  e.DATA_INICIO = '0000-00-00 00:00:00' and  e.DATA_FIM = '0000-00-00 00:00:00'";
		$r = mysql_query($sql);
		$ideqpatvo=mysql_result($r,0,0);
		$cod=mysql_result($r,0,1);
		$idcap=mysql_result($r,0,2);
		$catalogo_ideqp=mysql_result($r,0,3);
		if(!$r){
			echo "ERRO: Tente mais tarde!--2";
			mysql_query("rollback");
			return;
		}
                savelog(mysql_escape_string(addslashes($sql)));
	$justificativaNovoItem = "Item inserido pela Auditoria no lugar de:{$itemOriginal} da solicitaçao {$sol}";
		$t = 0;
		if($tipo == 'mat') $t = 1;
	if($tipo == 'die') $t = 3;
		$solicitante = $_SESSION["id_user"];





		if($tipo == 'eqp'){
			$t = 5;

			$sql = "INSERT INTO
solicitacoes
(paciente,
enfermeiro,
NUMERO_TISS,
qtd,
tipo,
data,
status,
TIPO_SOL_EQUIPAMENTO,
CATALOGO_ID,
idPrescricao,
DATA_MAX_ENTREGA,
TIPO_SOLICITACAO,
entrega_imediata,
obs,
inserido_troca,
CHAVE_AVULSA,
JUSTIFICATIVA_AVULSO,
JUSTIFICATIVA_AVULSO_ID,
id_prescricao_curativo,
primeira_prescricao,
relatorio_aditivo_enf_id,
DATA_SOLICITACAO,
id_periodica_complemento)
 VALUES
  ('$p','$solicitante','{$item}',
  '{$qtd}','{$t}',now(),$status,'{$tipoSolEquipamento}',
  '{$catalogo_id}',
  '{$presc}','{$dataMaxEntrega}','{$tipo_solicitacao}','{$entregaImediata}','{$justificativaNovoItem}','S',
  '{$chaveAvulsa}', '{$justificativaAvulsa}', '{$justificativaAvulsaId}','{$idPrescricaoCurativo}', '{$primeiraPrescricao}',
   '{$relatorioAditivoEnfId}', '{$dataSolicitacao}', '{$idPeriodicaComplemento}')";
		}else{
			$sql = "INSERT INTO solicitacoes (paciente,enfermeiro,NUMERO_TISS,qtd,tipo,data,
status,TIPO_SOL_EQUIPAMENTO,CATALOGO_ID,idPrescricao,DATA_MAX_ENTREGA,TIPO_SOLICITACAO,entrega_imediata,obs,inserido_troca,
CHAVE_AVULSA,
JUSTIFICATIVA_AVULSO,
JUSTIFICATIVA_AVULSO_ID,
id_prescricao_curativo,
primeira_prescricao,
relatorio_aditivo_enf_id,
DATA_SOLICITACAO,
id_periodica_complemento)
 VALUES
 ('$p','$solicitante','{$item}','{$qtd}','{$t}',now(),$status,'0','{$catalogo_id}','{$presc}',
 '{$dataMaxEntrega}','{$tipo_solicitacao}','{$entrega_imediata}','{$justificativaNovoItem}','S',
   '{$chaveAvulsa}', '{$justificativaAvulsa}', '{$justificativaAvulsaId}','{$idPrescricaoCurativo}',
   '{$primeiraPrescricao}', '{$relatorioAditivoEnfId}', '{$dataSolicitacao}', '{$idPeriodicaComplemento}')";

		}

		$r = mysql_query($sql);
		if(!$r){
			echo "ERRO: Tente mais tarde!--3";
			mysql_query("rollback");
			return;
		}
	$idNovaSolicitacao = mysql_insert_id();
                savelog(mysql_escape_string(addslashes($sql)));
		$sql="INSERT INTO equipamentosativos (COBRANCA_PLANOS_ID,CAPMEDICA_ID,QTD,DATA_INICIO,DATA_FIM,USUARIO,PACIENTE_ID,DATA) VALUES ('{$item}','{$idcap}','{$qtd}','0000-00-00 00:00:00','0000-00-00 00:00:00','{$solicitante}','{$p}',now())";
		$r = mysql_query($sql);
		if(!$r){
			echo "ERRO: Tente mais tarde!--5";
			mysql_query("rollback");
			return;
		}
		$id=mysql_insert_id();
                savelog(mysql_escape_string(addslashes($sql)));
                $sql="UPDATE equipamentosativos  SET  DATA_INICIO = '9999-12-31 23:59:59', DATA_FIM = '9999-12-31 23:59:59', TROCADO_POR= '{$id}',JUSTIFICATIVA = '{$just}' WHERE ID = '{$ideqpatvo}' ";
		$r = mysql_query($sql);
		if(!$r){
			echo "ERRO: Tente mais tarde!--6";
			mysql_query("rollback");
			return;
		}
	savelog(mysql_escape_string(addslashes($sql)));



	$sql = "INSERT INTO historico_troca_item
(catalogo_id_item,
catalogo_id_item_substituto,
user_id,
motivo,
data,
solicitacao_id,
tipo,
solicitacao_id_substituta)
VALUES
($catalogoAntigo,
$catalogo_id,
$solicitante,
'$just',
now(),
$sol,
$tipoItem,
$idNovaSolicitacao
)";
	$r = mysql_query($sql);

	if (!$r) {
		echo "ERRO: Tente mais tarde!--7";
		mysql_query("rollback");
		return;
	}
		 savelog(mysql_escape_string(addslashes($sql)));
		mysql_query("commit");
		echo "1";
		return;
}

##################################################################### CARREGA AUDITORIA NA BUSCA ###################################################################
function auditar($post){
	extract($post,EXTR_OVERWRITE);
	$inicio = implode("-",array_reverse(explode("/",$inicio)));
	$fim = implode("-",array_reverse(explode("/",$fim)));
	if($inicio!=null and $fim!=null){

		$between = " and s.data BETWEEN '{$inicio}' and '{$fim}'";
	}else{

		$between = "";
	}
	$p = anti_injection($p,'numerico');
	$row = mysql_fetch_array(mysql_query("SELECT c.nome FROM clientes as c WHERE c.idClientes = '$p'"));
	//echo "<center><h1>Sa&iacute;da para o paciente:<h1>";
	$paciente = ucwords($row['nome']);
	//echo "<h3>$paciente</h3></center>";
	echo "<div id='div-envio-paciente' p='$p' np='$paciente'>";
	//echo alerta();

	echo "<table class='mytable' width=100% id='lista-enviados' ><thead><tr><th>Itens Solicitados</th><th align='center' width='1%'>Enviado/Pedido</th><th width='22%' align='center'>Enviar</th></tr></thead>";
  	$p = anti_injection($p,'numerico');


  	$sqlmed = "SELECT s.enfermeiro, s.PENDENTE,
  				( SELECT coalesce(SUM(X0.autorizado),0)
				  FROM solicitacoes as X0, CATALOGO as X1
				  WHERE X0.paciente = '$p' AND X0.tipo = '0' AND (X0.status = '1' OR X0.status = '2')
				  AND X1.numero_tiss = X0.cod and X1.numero_tiss = s.cod
				  AND X0.data BETWEEN DATE_FORMAT(NOW(),'%Y-%m-01') and DATE_FORMAT(NOW(),'%Y-%m-31')) as autorizado,
				  s.qtd, s.cod, s.idSolicitacoes as sol, s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data, b.principio, b.apresentacao
  			   FROM solicitacoes as s, CATALOGO as b
  			   WHERE s.paciente = '$p' AND s.tipo = '0' AND (s.status = '0' OR s.status = '2')".$between."
  			   AND b.numero_tiss = s.cod ORDER BY principio, apresentacao, qtd, enviado";

//  	$sqlmed = "SELECT med.principioAtivo, com.nome, s.enfermeiro, s.qtd, s.enviado, s.cod, s.idSolicitacoes as sol, s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data
//  			   FROM solicitacoes as s, medicacoes as med, nomecomercialmed as com
//  			   WHERE s.paciente = '$p' AND s.tipo = '0' AND (s.status = '0' OR s.status = '2')
//  			   AND com.idNomeComercialMed = s.cod AND com.Medicacoes_idMedicacoes = med.idMedicacoes
//  			   ORDER BY principioAtivo, nome, qtd, enviado";

  	$rmed = mysql_query($sqlmed);
  	while($row = mysql_fetch_array($rmed)){
  		foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
  		$emergencia = 0; $emergencia_label = "";
  		if($row['status'] == '2'){
  			$emergencia = '2'; $emergencia_label = "<b><i>emerg&ecirc;ncia</i></b>";
  		}
  		$nome = "{$row['principio']} {$row['apresentacao']}";
  		$dados = "sol='{$row['sol']}' enf='{$row['enfermeiro']}' nome='{$nome}' e='{$emergencia}'";
  		$status = "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' pendente='{$row['PENDENTE']}' qtd='{$row['qtd']}' d='1' /><button class='aceitePB' alt='Autorizar' title='Autorizar' id='A{$row['sol']}'></button>";
  		//$status .= "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' d='0' checked />Decidir Depois";
  		$status .= "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' pendente='{$row['PENDENTE']}' qtd='{$row['qtd']}' d='-1' /><button class='negarPB' alt='Negar' title='Negar' id='N{$row['sol']}'></button>";
  		$campo = "<input type='text' name='campo-{$row['sol']}' class='qtdAut OBG' id='qtdAut{$row['sol']}' size='2'>";
  		echo "<tr><td><b>MEDICAMENTO: </b>{$nome} {$emergencia_label}</td>";
  		echo "<td align='center'>".$row['autorizado']."/".$row['qtd']."</td><td width='22%'>$status $campo</td></tr>";
  	}
  	$sqlmat = "SELECT s.enfermeiro,
				  ( SELECT coalesce(SUM(X0.autorizado),0)
						  FROM solicitacoes as X0, CATALOGO as X1
						  WHERE X0.paciente = '$p' AND X0.tipo = '1' AND (X0.status = '1' OR X0.status = '2')
						  AND X1.numero_tiss = X0.cod and X1.numero_tiss = s.cod
						  AND X0.data BETWEEN DATE_FORMAT(NOW(),'%Y-%m-01') and DATE_FORMAT(NOW(),'%Y-%m-31')) as autorizado,
		  				 s.qtd, s.cod, s.idSolicitacoes as sol, s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data, b.principio, b.apresentacao
  			   FROM solicitacoes as s, CATALOGO as b
  			   WHERE s.paciente = '$p' AND s.tipo = '1' AND (s.status = '0' OR s.status = '2')".$between."
  			   AND b.numero_tiss = s.cod ORDER BY principio, apresentacao, qtd, enviado";

//  	$sqlmat = "SELECT mat.nome, mat.descricao, s.enfermeiro, s.qtd, s.enviado, s.cod, s.idSolicitacoes as sol, s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data
//  			   FROM solicitacoes as s, material as mat
//  			   WHERE s.paciente = '$p' AND s.tipo = '1' AND (s.status = '0' OR s.status = '2')
//  			   AND mat.idMaterial = s.cod
//  			   ORDER BY nome, descricao, qtd, enviado";
  	$rmat = mysql_query($sqlmat);
  	while($row = mysql_fetch_array($rmat)){
  		foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
  		$emergencia = 0; $emergencia_label = "";
  		if($row['status'] == '2'){
  			$emergencia = '2'; $emergencia_label = "<b><i>emerg&ecirc;ncia</i></b>";
  		}
  		$nome = "{$row['principio']} {$row['apresentacao']}";
  		$dados = "sol='{$row['sol']}' enf='{$row['enfermeiro']}' nome='{$nome}' e='{$emergencia}' ";
  		$status = "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' pendente='{$row['PENDENTE']}' qtd='{$row['qtd']}' d='1' /><button class='aceitePB' alt='Autorizar' title='Autorizar' id='A{$row['sol']}'></button>";
  		//$status .= "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' d='0' checked />Decidir Depois";
  		$status .= "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' pendente='{$row['PENDENTE']}' qtd='{$row['qtd']}' d='-1' /><button class='negarPB' alt='Negar' title='Negar' id='N{$row['sol']}'></button>";
  		$campo = "<input type='text' name='campo-{$row['sol']}' class='OBG' id='qtdAut{$row['sol']}' size='2'>";
  		echo "<tr><td><b>MATERIAL: </b>{$nome} {$emergencia_label}</td>";
  		echo "<td align='center'>".$row['autorizado']."/".$row['qtd']."</td><td>$status $campo</td></tr>";
  	}
  	$sqlformula = "SELECT s.enfermeiro,
				( SELECT coalesce(SUM(X0.autorizado),0)
				  FROM solicitacoes as X0, CATALOGO as X1
				  WHERE X0.paciente = '$p' AND X0.tipo = '12' AND (X0.status = '1' OR X0.status = '2')
				  AND X1.numero_tiss = X0.cod and X1.numero_tiss = s.cod
				  AND X0.data BETWEEN DATE_FORMAT(NOW(),'%Y-%m-01') and DATE_FORMAT(NOW(),'%Y-%m-31')) as autorizado,
  	 		   s.qtd, s.cod, s.idSolicitacoes as sol, s.status, DATE_FORMAT(s.data,'%d/%m/%Y') as data, f.formula
  			   FROM solicitacoes as s, formulas as f
  			   WHERE s.paciente = '$p' AND s.tipo = '12' AND (s.status = '0' OR s.status = '2')".$between."
  			   AND f.id = s.cod ORDER BY formula, qtd, enviado";
  	$rformula = mysql_query($sqlformula);
  	while($row = mysql_fetch_array($rformula)){
  		foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
  		$emergencia = 0; $emergencia_label = "";
  		if($row['status'] == '2'){
  			$emergencia = '2'; $emergencia_label = "<b><i>emerg&ecirc;ncia</i></b>";
  		}
  		$nome = "{$row['formula']}";
  		$dados = "sol='{$row['sol']}' enf='{$row['enfermeiro']}' nome='{$nome}' e='{$emergencia}' ";
  		$status = "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' pendente='{$row['PENDENTE']}' qtd='{$row['qtd']}' d='1' /><button class='aceitePB' alt='Autorizar' title='Autorizar' id='A{$row['sol']}'></button>";
  		//$status .= "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' d='0' checked />Decidir Depois";
  		$status .= "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' pendente='{$row['PENDENTE']}' qtd='{$row['qtd']}' d='-1' /><button class='negarPB' alt='Negar' title='Negar' id='N{$row['sol']}'></button>";
  		$campo = "<input type='text' name='campo-{$row['sol']}' class='OBG' id='qtdAut{$row['sol']}' size='2'>";
  		echo "<tr><td>{$nome} {$emergencia_label}</td>";
  		echo "<td align='center'>".$row['autorizado']."/".$row['qtd']."</td><td>$status $campo</td></tr>";
  	}
  	echo "</table>";

  	echo "</div>";

}
####################################

function show_prontuario($id){
  $id = anti_injection($id,'numerico');
  $result = mysql_query("SELECT tipo, content, nome FROM prontuarios WHERE id='$id' ");
  $row = mysql_fetch_array($result);
  header("Content-type: {$row['tipo']}");
  header("Content-Disposition: attachment; filename={$row['nome']};" );
  print $row['content'];
}

function pendencia_item_solicitacao($post){
    extract($post,EXTR_OVERWRITE);
    mysql_query('begin');
    $autor = $_SESSION["nome_user"];
    $empresa = anti_injection($empresa, "numerico");
    $paciente =anti_injection($paciente, "literal");
	$idUser = $_SESSION['id_user'];
    $item = explode("$", $dados);
    $cont=0;
    $corpo_email = "";
    $inicioPresc = [];
    $fimPresc    = [];
    foreach ($item as $i) {
        //if ($i != "" && $i != null) {
            $e = explode("#", $i);
            $solicitacao = anti_injection($e[0], "numerico");
            $just_pendente = anti_injection($e[1], "literal");
            $pendente = anti_injection($e[2], "literal");
            $item = anti_injection($e[3], "literal");

            if($pendente == 'S'){
                $sqlData = <<<SQL
SELECT 
  inicio, 
  fim,
  IF(idPrescricao = '-1', DATA_SOLICITACAO, '') AS dataSol,
  idPrescricao
FROM 
  solicitacoes 
  LEFT JOIN prescricoes ON prescricoes.id = solicitacoes.idPrescricao
WHERE
  idSolicitacoes = '{$solicitacao}'
SQL;
                $rsData = mysql_query($sqlData);
                $rowData = mysql_fetch_array($rsData);
                if($rowData['idPrescricao'] != '-1'){
                    $inicioPresc = \DateTime::createFromFormat('Y-m-d', $rowData['inicio'])->format('d/m/Y');
                    $fimPresc = \DateTime::createFromFormat('Y-m-d', $rowData['fim'])->format('d/m/Y');
                    $compData = "<br><b>Prescri&ccedil;&atilde;o de</b> {$inicioPresc} a {$fimPresc}";
                } else {
                    $dataSol = \DateTime::createFromFormat('Y-m-d H:i:s', $rowData['dataSol'])->format('d/m/Y à\s H:i:s');
                    $compData = "<br><b>Solicitado em</b> {$dataSol}.";
                }
                $cont++;
                $sql="update solicitacoes set OBS_PENDENTE='{$just_pendente}',PENDENTE='{$pendente}',AUTOR_PENDENCIA='{$idUser}',DATA_PENDENCIA=now() where idSolicitacoes='{$solicitacao}'";
                 $corpo_email .= "<b>Item:</b> {$item} <br/> <b>Justificativa:</b> {$just_pendente} {$compData} <br/> ______________________<br/>";
            }else{
                $sql="update solicitacoes set OBS_PENDENTE='{$just_pendente}',PENDENTE='{$pendente}' where idSolicitacoes='{$solicitacao}'";
            }
    $result= mysql_query($sql);
    //echo $sql;
    if(!$result){
        echo "Erro ao salvar pendencia";
        mysql_query('rollback');
        return;
    }
     savelog(mysql_escape_string(addslashes($sql)));
   }
    mysql_query('commit');
    
    if($cont>0){
        $msgHTML = "<p>Pendência(s) para a solicitação do paciente <b>".$paciente."</b> </p>".$corpo_email." pelo(a) auditor(a) " . $autor;
        enviarEmail($paciente, 15, $empresa, $msgHTML);
        //enviar_email_pendencia($corpo_email, $paciente, $empresa);
    }
    
    echo 1;
    

}


function atualizar_visualizacao($post)
{
    extract($post,EXTR_OVERWRITE);
    $result = mysql_query("UPDATE solicitacoes SET visualizado = 1 WHERE paciente = {$paciente} ");

    return false !== $result;

 }

//// aviso de e-mail de itens negados pela auditoria 2013-11-22 Jeferson
function enviar_email_negado($corpo_email,$np,$empresa){

    $configs = require __DIR__ . '/../../config.php';

    $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
    $mail->IsSMTP(); // telling the class to use SMTP

    try {
        $mail->SMTPAuth = true;                    // enable SMTP authentication
        $mail->Host     = "smtp.mederi.com.br";    // sets GMAIL as the SMTP server
        $mail->Port     = 587;                     // set the SMTP port for the GMAIL server
        $mail->Username = "avisos@mederi.com.br";  // GMAIL username
        $mail->Password = "2017avisosistema";            // GMAIL password

        if (isset($configs['APP_ENV']) && $configs['APP_ENV'] === 'prd') {
          $mail->AddAddress('direnf@mederi.com.br', 'Diretora de Enfermagem ');
          if($empresa == '11' || $empresa == '1'){ //email por unidade regional
            $mail->AddAddress('urfsa@mederi.com.br', 'Unidade - Feira de Santana');
          } else if($empresa == '2'){
            $mail->AddAddress('coordenf.salvador@mederi.com.br', 'Coordenação de Enfermagem - Salvador');
          } else if($empresa == '4'){
            $mail->AddAddress('coordenf.sulbahia@mederi.com.br', 'Coordenação de Enfermagem - Sul/Bahia');
          } else if($empresa == '5'){
            $mail->AddAddress('coordenf.alagoinhas@mederi.com.br', 'Coordenação de Enfermagem - Alagoinhas');
            $mail->AddAddress('karlaenf.alagoinhas@mederi.com.br', 'Enfermeira Karla - Alagoinhas');
          } else if($empresa == '7'){
            $mail->AddAddress('coordenf.sudoeste@mederi.com.br', 'Coordenação de Enfermagem - Sudoeste');
          } else if($empresa == '9'){
            $mail->AddAddress('coordenf.brasilia@mederi.com.br', 'Coordenação de Enfermagem - BSB');
          }
        } else {
          $mail->AddAddress('ti@mederi.com.br', 'Coordenação de Enfermagem - Feira de Santana');
        }

        $mail->SetFrom('avisos@mederi.com.br', 'Avisos de Sistema');
        $mail->Subject = 'Itens- negados auditoria Sistema de Gerenciamento da Mederi';
        $mail->AltBody = 'Para ver essa mensagem, use um visualizador de email compat&iacute;vel com HTML'; // optional - MsgHTML will create an alternate automatically
        $auditora= $_SESSION["nome_user"];
        $mail->MsgHTML("<p>A solicita&ccedil;&atilde;o do paciente <b>".utf8_decode($np)."</b> tiveram os seguintes itens negados:</p>".utf8_decode($corpo_email)." pelo(a) auditor(a) ".utf8_decode($auditora)."");
        $enviado = $mail->Send();

    } catch (Exception $ex) {
        $mail->SMTPAuth = true;                  // enable SMTP authentication
				$configEmail = require __DIR__."/../app/configs/email.php";
        $mail->Host = $configEmail['smtp-gmail']['host'];      // sets GMAIL as the SMTP server
        $mail->Port = $configEmail['smtp-gmail']['port'];      // set the SMTP port for the GMAIL server
        $mail->Username = $configEmail['smtp-gmail']['username'];  // GMAIL username
        $mail->Password = $configEmail['smtp-gmail']['password'];  // GMAIL password
        
        try {
            return $mail->Send();
        } catch (Exception $e) {
            echo sprintf('Erro: %s / Causa Under: %s | Causa Gmail: %s', $mail->ErrorInfo, $ex->getMessage(), $e->getMessage());
        }
    }
   echo "1";
   //header("LOCATION:?view=solicitacoes");
}

function enviar_email_pendencia($corpo_email,$paciente,$empresa){

    $configs = require __DIR__ . '/../../config.php';

    $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
    $mail->IsSMTP(); // telling the class to use SMTP

    try {
        $mail->SMTPAuth = true;                    // enable SMTP authentication
        $mail->Host     = "smtp.mederi.com.br";    // sets GMAIL as the SMTP server
        $mail->Port     = 587;                     // set the SMTP port for the GMAIL server
        $mail->Username = "avisos@mederi.com.br";  // GMAIL username
        $mail->Password = "2017avisosistema";            // GMAIL password

        if (isset($configs['APP_ENV']) && $configs['APP_ENV'] === 'prd') {
          $mail->AddAddress('direnf@mederi.com.br', 'Diretora de Enfermagem ');
          if($empresa == '1'){ //email por unidade regional
            $mail->AddAddress('urfsa@mederi.com.br', 'Unidade - Feira de Santana');
          } else if($empresa == '2'){
            $mail->AddAddress('coordenf.salvador@mederi.com.br', 'Coordenação de Enfermagem - Salvador');
          } else if($empresa == '4'){
            $mail->AddAddress('coordenf.sulbahia@mederi.com.br', 'Coordenação de Enfermagem - Sul/Bahia');
          } else if($empresa == '5'){
            $mail->AddAddress('coordenf.alagoinhas@mederi.com.br', 'Coordenação de Enfermagem - Alagoinhas');
          } else if($empresa == '7'){
            $mail->AddAddress('coordenf.sudoeste@mederi.com.br', 'Coordenação de Enfermagem - Sudoeste');
          } else if($empresa == '9'){
            $mail->AddAddress('coordenf.brasilia@mederi.com.br', 'Coordenação de Enfermagem - BSB');
          }
        } else {
          $mail->AddAddress('ti@mederi.com.br', 'Coordenação de Enfermagem - Feira de Santana');
        }
        
        $mail->SetFrom('avisos@mederi.com.br', 'Avisos de Sistema');
        $mail->Subject = utf8_decode('Pendência').' na Auditoria do  Sistema de Gerenciamento da Mederi';
        $mail->AltBody = 'Para ver essa mensagem, use um visualizador de email compat&iacute;vel com HTML'; // optional - MsgHTML will create an alternate automatically
        $auditora= $_SESSION["nome_user"];
        $mail->MsgHTML("<p>".htmlentities ('Pendência(s)')." para a ".htmlentities ('solicitação')." do paciente <b>".utf8_decode($paciente)."</b> </p>".utf8_decode($corpo_email)." pelo(a) auditor(a) ".utf8_decode($auditora)."");
        $enviado = $mail->Send();

    } catch (Exception $ex) {
        $mail->SMTPAuth = true;                  // enable SMTP authentication
        $mail->Host = $configEmail['smtp-gmail']['host'];      // sets GMAIL as the SMTP server
        $mail->Port = $configEmail['smtp-gmail']['port'];                   // set the SMTP port for the GMAIL server
        $mail->Username = $configEmail['smtp-gmail']['username'];  // GMAIL username
        $mail->Password = $configEmail['smtp-gmail']['password'];            // GMAIL password
        
        try {
            return $mail->Send();
        } catch (Exception $e) {
            return sprintf('Erro: %s / Causa Under: %s | Causa Gmail: %s', $mail->ErrorInfo, $ex->getMessage(), $e->getMessage());
        }
    }
   echo "1";
   //header("LOCATION:?view=solicitacoes");
}

function enviarEmail($paciente, $tipo, $empresa, $msgHTML = '')
{
	$api_data = (object) Config::get('sendgrid');

	$gateway = new MailAdapter(
		new SendGridGateway(
			new \SendGrid($api_data->user, $api_data->pass),
			new \SendGrid\Email()
		)
	);

	$gateway->adapter->setSendData($paciente, 'enfermagem', $tipo, $empresa, $msgHTML);
	$gateway->adapter->sendMail();
}

function enviarEmailSimples($titulo, $texto, $para)
{
    $api_data = (object) Config::get('sendgrid');

    $gateway = new MailAdapter(
        new SendGridGateway(
            new \SendGrid($api_data->user, $api_data->pass),
            new \SendGrid\Email()
        )
    );

    $gateway->adapter->sendSimpleMail($titulo, $texto, $para);
}

//// fim

if(isset($_POST['query'])){
switch($_POST['query']){
  case "salvar-auditoria":
  	salvar_auditoria($_POST);
  break;
  case "salvar-prontuario":
  	salvar_prontuario($_POST,$_FILES['prontuario']);
  break;
  case "cancelar-item-solicitacao":
  	cancelar_item_solicitacao($_POST);
  	break;
  case "busca-itens-troca":
  	busca_itens($_POST['like'],$_POST['tipo']);
  	break;
  case "trocar-item-solicitacao":
  	trocar_item_solicitacao($_POST);
  	break;
  case "salvar_observacao":
        salvar_observacao($_POST);
        break;
  case "pendencia-item-solicitacao":
        pendencia_item_solicitacao($_POST);
        break;
 case "atualizar_visualizacao":
        echo atualizar_visualizacao($_POST);
        break;
}
}

if(isset($_GET['query'])){
  switch($_GET['query']){
    case "show-prontuario":
      if(isset($_GET['id']))show_prontuario($_GET['id']);
    break;
  }
}

switch($_POST['query']){
	case "buscar":
		auditar($_POST);
		break;
}
?>