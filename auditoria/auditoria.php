<?php
//ini_set('display_errors', 'On');
$id = session_id();
if(empty($id))
    session_start();

include_once('../db/config.php');
include_once('../utils/codigos.php');

use \App\Services\CacheAdapter;
use \App\Services\Cache\SimpleCache;
use \App\Models\FileSystem\FileUpload;
use \App\Helpers\StringHelper as String;

class Auditoria{

    private function menu(){
        echo "<dev><center><h1>Auditoria Interna</h1></center></dev>";
        echo "<p><a href='?view=solicitacoes'>Auditar Solicita&ccedil;&otilde;es</a>";
       
        echo "<p><a href='../faturamento/?view=listar'>Listar Faturas</a></p>";
        echo "<p><a href='../faturamento/?view=faturaravaliacao'>Or&ccedil;amento Inicial</a></p>";
        echo "<p><a href='../faturamento/?view=orcamento-prorrogacao'>Or&ccedil;amento de Prorrogação</a></p>";
        echo "<p><a href='/adm/secmed/?action=buscar-orcamentos-autorizacao-auditoria'>Visualizar Autorizações</a></p>";
        if($_SESSION['empresa_principal'] == '11' || $_SESSION['empresa_principal'] == '1') {
            echo "<p><a href='?view=relatorios'>Relat&oacute;rios</a></p>
                  <p>
                    Não Conformidades:
                    <ul>
                        <li><a href='relatorios/?action=form-relatorio-nao-conformidades'>Relatar</a></li>
                        <li><a href='relatorios/?action=relatorio-nao-conformidades-gerenciar'>Gerenciar</a></li>
                    </ul>
                  </p>";
        }
    }

    private function menuRelatorios ()
    {
        if ($_SESSION['id_user'] == '44' || $_SESSION['adm_user'] == '1') {

            //echo "<p><a href='relatorios/?action=form-relatorio-faturas-liberadas'>Relat&oacute;rio de Faturas Liberadas</a></p>";
            echo "<p><a href='relatorios/?action=form-relatorio-pedidos-liberados'>Relat&oacute;rio de Pedidos Liberados</a></p>";
            echo "<p><a href='relatorios/?action=form-relatorio-faturamento-enviado'>Relat&oacute;rio de Faturamento Enviado</a></p>";

        }
        echo "<p><a href='relatorios/?action=form-relatorio-saidas'>Relat&oacute;rio de Pedidos</a></p>";
        echo "<p><a href='relatorios/?action=relatorio-nao-conformidades-busca-condensada'>Relatório de não Conformidades</a></p>";
    }

    private function createDialogParecerEntregaImediata($solicitacao)
    {
        $html = <<<HTML
<div id="dialog-parecer-entrega-imediata-{$solicitacao}" title="Parecer Entrega Imediata" style="display:none;">
	<div class="wrap" style="font-size: 12pt;">
	    <p>
	        <span id="label-item-parecer"></span>
        </p>
		<p>
			<textarea cols="50" rows="5" apresentacao="" name="parecer_entrega_imediata" solicitacao="{$solicitacao}" class="parecer-textarea"></textarea>
		</p>
	</div>
</div>
HTML;
        return $html;
    }

    private function formulario(){
        echo "<div id='formulario-troca' title='Formul&aacute;rio de troca' catalogoAntigo='' nomeItem='' sol='' tipo='' presc='' data_entrega='' fim='' inicio='' status='' kit='' entrega-imediata='' tipo_solicitacao>";
        echo alerta();
        echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar a troca de item, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
        echo "<p><b>Busca:</b><br/><input type='text' name='busca' id='busca-item-troca' />";
        echo "<div id='opcoes-troca'></div>";
        echo "<input type='hidden' name='catalogo_id' id='catalogo_id' value='-1' />";
        echo "<input type='hidden' name='cod' id='cod' value='-1' />";
        echo "<p><b>Nome:</b><br/><input type='text' readonly=readonly name='nome' id='nome' class='OBG' size='50' /></p>";
        echo "<p><b>Quantidade:</b><br><input type='text' id='qtd' name='qtd' class='numerico OBG' size='3'/></p>";
        echo "<p><b>Justificativa:</b><br><input type='text' name='justificativa' class='OBG' size='100' /></p></div>";
    }

    private function pacientes($id,$name,$todos){
        $result = mysql_query("SELECT * FROM clientes ORDER BY nome;");
        $var = "<select name='{$name}' style='background-color:transparent;' class='COMBO_OBG' >";
        if($todos == "t") $var .= "<option value='todos' selected>Todos</option>";
        if($todos == "n") $var .= "<option value='-1' selected></option>";
        while($row = mysql_fetch_array($result))
        {
            if(($id <> NULL) && ($id == $row['idClientes']))
                $var .= "<option value='{$row['idClientes']}' selected>({$row['codigo']})" . strtoupper(String::removeAcentosViaEReg($row['nome'])) . "</option>";
            else
                $var .= "<option value='{$row['idClientes']}'>({$row['codigo']}) " . strtoupper(String::removeAcentosViaEReg($row['nome'])) . "</option>";
        }
        $var .= "</select>";
        return $var;
    }

    private function pastas(){
        $result = mysql_query("SELECT * FROM pastasProntuarios ORDER BY nome;");
        $var = "";
        while($row = mysql_fetch_array($result))
        {
            $var .= "<button class='bpasta' p='{$row['id']}' >{$row['nome']}</button>";
        }
        return $var;
    }

    private function auditar($p,$inicio=null,$fim=null,$idP,$e){

        if($inicio!=null and $fim!=null){
            $inicio = implode("-",array_reverse(explode("/",$inicio)));
            $fim = implode("-",array_reverse(explode("/",$fim)));
            $between = "AND
                        '{$inicio}'<=(CASE s.idPrescricao WHEN -1 THEN
                        s.data
                        ELSE
                        p.inicio
                        END) AND
                        '{$fim}' >=(CASE s.idPrescricao WHEN -1 THEN
                         s.data
                        ELSE
                        p.fim
                        END)";
            $between2 = " AND
                        '{$inicio}'<=(CASE X0.idPrescricao WHEN -1 THEN
                        X0.data
                        ELSE
                        X2.inicio
                        END) AND
                        '{$fim}' >=(CASE X0.idPrescricao WHEN -1 THEN
                         X0.data
                        ELSE
                        X2.fim
                        END)";
        }else{
            $between = "";
            $between2 = "";
        }

        $p = anti_injection($p,'numerico');

        echo "<div id='cancelar-item' title='Cancelar item' sol='' >";
        echo alerta();
        echo info("<b>Aten&ccedil;&atilde;o:</b> Ap&oacute;s salvar o cancelamento, a p&aacute;gina será atualizada e dados preenchidos no campo 'enviar' ser&atilde;o apagados.");
        echo "<p><b>Justificativa:</b><input type='text' name='justificativa' class='OBG' size='100' /></div>";
        $this->formulario();
        $row = mysql_fetch_array(mysql_query("SELECT c.nome FROM clientes as c WHERE c.idClientes = '$p'"));
        $paciente = strtoupper(String::removeAcentosViaEReg($row['nome']));
        echo "<div id='div-envio-paciente' p='$p' np='$paciente' >";
        echo alerta();

        echo "<table class='mytable' width=100% id='lista-enviados' >
  					<thead>
  						<tr>
  							<th style='width: 100px;' id='thpendencias'>
  								<button class='PTodos' id='PTodos' alt='Pendencia' title='Marcar Todos como Pendentes' ></button> Pendente
								</th>
								<th colspan='2'>
									Itens Solicitados
								</th>
								<th align='center' width='1%'>
									Enviado/Pedido
								</th>
								<th>
									domic&iacute;lio
								</th>
								<th width='25%' >
									<input type='radio' name='todos' class='todos'  value='1' />
									<button class='aceitePB' id='Atodos' alt='Autorizar' title='Autorizar Todos' ></button>
									<input type='radio' name='todos' class='todos'  value='-1'/>
									<button class='negarPB' alt='Negar' id='Ntodos' title='Negar Todos' ></button>
									Enviar
								</th>
							</tr>
						</thead>";
        $p = anti_injection($p,'numerico');

        $sqlmed = "SELECT
                    s.`idPrescricao`,
                    s.PENDENTE,
                    s.OBS_PENDENTE,
                    s.enfermeiro,
                    u.nome as autor,
                    DATE_FORMAT(s.DATA_PENDENCIA,'%d/%m/%Y %H:%i:%s') as data_pendencia,
                    (   SELECT
                            checkl.QTD
                        FROM
                            checklist_enfermagem as checkl
                        WHERE
                            checkl.PACIENTE_ID = '{$p}' AND
                            checkl.CATALOGO_ID = s.CATALOGO_ID AND
                            checkl.DATA = ( SELECT
                                                max(check2.DATA)
                                            FROM
                                                checklist_enfermagem as check2
                                            WHERE
                                                check2.PACIENTE_ID = '{$p}' AND
                                                check2.CATALOGO_ID = s.CATALOGO_ID
                                          )
                    ) as estoque,
                    s.TIPO_SOLICITACAO,
                    DATE_FORMAT(s.DATA_MAX_ENTREGA,'%d/%m/%Y %H:%i:%s') as data_entrega,
                    (CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia ,
                    (CASE  when s.status= 0 and s.idPrescricao=-1 THEN '1' ELSE '0' END) as avulsa,
                    (   SELECT
                            COALESCE(SUM(X0.autorizado),0)
                        FROM
                            solicitacoes AS X0 INNER JOIN
                            catalogo AS X1 ON (X0.CATALOGO_ID = X1.ID) LEFT JOIN
                            prescricoes AS X2 ON (X0.idPrescricao = X2.id)
                        WHERE
                            X0.paciente = '{$p}' AND
                            X0.tipo = '0' AND
                            (X0.status = '1' OR X0.status = '2') ".$between2." AND
                            X0.qtd <> 0 AND
                            X1.ID = s.CATALOGO_ID ) AS autorizado,
                    s.qtd,
                    s.NUMERO_TISS,
                    s.CATALOGO_ID,
                    s.idSolicitacoes AS sol,
                    s.status,
                    DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
                    (   CASE
                            s.idPrescricao WHEN -1 THEN s.data
                        ELSE
                            DATE_FORMAT(p.inicio,'%d/%m/%Y ') END) AS inicio,
                    (   CASE
                            s.idPrescricao WHEN -1 THEN s.data
                        ELSE
                            DATE_FORMAT(p.fim,'%d/%m/%Y ') END) AS fim,
                    b.principio,
                    b.apresentacao,
                    k.nome as kit,
                    s.KIT_ID,
                    s.DATA_MAX_ENTREGA,
                    IF (s.JUSTIFICATIVA_AVULSO_ID = 11, CONCAT(jsa.justificativa,': ' ,s.JUSTIFICATIVA_AVULSO), jsa.justificativa) as Just,
                    IF (s.JUSTIFICATIVA_AVULSO_ID = null OR s.JUSTIFICATIVA_AVULSO_ID = 0, s.JUSTIFICATIVA_AVULSO, '') as Antigas,
                    s.JUSTIFICATIVA_AVULSO_ID,
                    s.id_periodica_complemento,
                    s.entrega_imediata,
                    s.enviado as enviado,
                   COALESCE(p.flag, '') as flag
                   FROM
                    solicitacoes AS s INNER JOIN
                    catalogo AS b ON (b.ID = s.CATALOGO_ID) LEFT JOIN
                    prescricoes AS p ON (s.idPrescricao = p.id ) LEFT JOIN
                    kitsmaterial as k on (s.KIT_ID = idKit)
                    LEFT JOIN usuarios as u ON (s.AUTOR_PENDENCIA = u.idUsuarios)
                    LEFT JOIN justificativa_solicitacao_avulsa AS jsa ON (s.JUSTIFICATIVA_AVULSO_ID = jsa.id)
                   WHERE
                    s.paciente = '{$p}' AND
                    s.tipo = '0' AND
                    s.data_auditado ='0000-00-00 00:00:00' AND
                    (s.status = '0' OR s.status = '2') ".$between."
                    AND s.id_prescricao_curativo = 0
                    AND s.qtd <> 0

                   UNION
									/*
									 UNION PARA PEGAR OS DADOS NA TABELA PRESCRICAO_CURATIVO
									 */
                    SELECT
                    s.`idPrescricao`,
                    s.PENDENTE,
                    s.OBS_PENDENTE,
                    s.enfermeiro,
                    u.nome as autor,
                    DATE_FORMAT(s.DATA_PENDENCIA,'%d/%m/%Y %H:%i:%s') as data_pendencia,
                    (   SELECT
                            checkl.QTD
                        FROM
                            checklist_enfermagem as checkl
                        WHERE
                            checkl.PACIENTE_ID = '{$p}' AND
                            checkl.CATALOGO_ID = s.CATALOGO_ID AND
                            checkl.DATA = ( SELECT
                                                max(check2.DATA)
                                            FROM
                                                checklist_enfermagem as check2
                                            WHERE
                                                check2.PACIENTE_ID = '{$p}' AND
                                                check2.CATALOGO_ID = s.CATALOGO_ID
                                          )
                    ) as estoque,
                    s.TIPO_SOLICITACAO,
                    DATE_FORMAT(p.data_max_entrega,'%d/%m/%Y') as data_entrega,
                    (CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia ,
                    (CASE  when s.status= 0 and s.idPrescricao=-1 THEN '1' ELSE '0' END) as avulsa,
                    (   SELECT
                            COALESCE(SUM(X0.autorizado),0)
                        FROM
                            solicitacoes AS X0 INNER JOIN
                            catalogo AS X1 ON (X0.CATALOGO_ID = X1.ID) LEFT JOIN
                            prescricoes AS X2 ON (X0.idPrescricao = X2.id)
                        WHERE
                            X0.paciente = '{$p}' AND
                            X0.tipo = '0' AND
                            (X0.status = '1' OR X0.status = '2') ".$between2." AND
                            X0.qtd <> 0 AND
                            X1.ID = s.CATALOGO_ID ) AS autorizado,
                    s.qtd,
                    s.NUMERO_TISS,
                    s.CATALOGO_ID,
                    s.idSolicitacoes AS sol,
                    s.status,
                    DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
                    DATE_FORMAT(p.inicio,'%d/%m/%Y') AS inicio,
                    DATE_FORMAT(p.fim,'%d/%m/%Y') AS fim,
                    b.principio,
                    b.apresentacao,
                    k.nome as kit,
                    s.KIT_ID,
                    s.DATA_MAX_ENTREGA,
                    IF (s.JUSTIFICATIVA_AVULSO_ID = 11, CONCAT(jsa.justificativa,': ' ,s.JUSTIFICATIVA_AVULSO), jsa.justificativa) as Just,
                    IF (s.JUSTIFICATIVA_AVULSO_ID = null OR s.JUSTIFICATIVA_AVULSO_ID = 0, s.JUSTIFICATIVA_AVULSO, '') as Antigas,
                    s.JUSTIFICATIVA_AVULSO_ID,
                    s.id_periodica_complemento,
                    p.entrega_imediata,
                    s.enviado as enviado,
                    '' as flag
                   FROM
                    solicitacoes AS s INNER JOIN
                    catalogo AS b ON (b.ID = s.CATALOGO_ID) LEFT JOIN
                    prescricao_curativo AS p ON (s.id_prescricao_curativo = p.id ) LEFT JOIN
                    kitsmaterial as k on (s.KIT_ID = idKit)
                    LEFT JOIN usuarios as u ON (s.AUTOR_PENDENCIA = u.idUsuarios)
                    LEFT JOIN justificativa_solicitacao_avulsa AS jsa ON (s.JUSTIFICATIVA_AVULSO_ID = jsa.id)
                   WHERE
                    s.paciente = '{$p}' AND
                    s.tipo = '0' AND
                    s.data_auditado ='0000-00-00 00:00:00' AND
                    (s.status = '0' OR s.status = '2') ".$between."
                    AND s.TIPO_SOLICITACAO != 10
										AND s.idPrescricao = 0
										AND s.qtd <> 0

                   ORDER BY
                    principio,
                    apresentacao,
                    qtd,
                    enviado";

        $rmed = mysql_query($sqlmed);

        while($row = mysql_fetch_array($rmed)){
            foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
            $emergencia = 0;
            $emergencia_label = "";
            $data_entrega ='';
            $cor='';
            $parecerButton = '';
            $textoTipoSolicitacao ='';
            if($row['status'] == '2') {
                $emergencia = '2';
                $emergencia_label = "<b><i>Entrega Imediata  {$row['data_entrega']}</i></b>";
                echo $this->createDialogParecerEntregaImediata($row['sol']);
                $parecerButton = "<button class='ui-button
                                            ui-widget
                                            ui-corner-all
                                            ui-button-text-only
                                            ui-state-hover
                                            parecer-entrega-imediata'
                                            solicitacao='{$row['sol']}'
                                            apresentacao='Medicamento: {$row['principio']} {$row['apresentacao']}'>
                                            <span class='ui-button-text'>Parecer</span>
                                  </button>";
            }
            if ($row["emergencia"] == 1) {
                $cor = "style='color:red'";
                $data_entrega = "";
            } else if ($row["avulsa"] == 1) {
                $cor = "style='color:blue'";
                if ($row['data_entrega'] != '00/00/0000 00:00:00') {
                    $data_entrega = "&nbsp;<span style='color:#000;'><b> Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";
                } else {
                    $data_entrega = "";
                }

            } else if ($row["TIPO_SOLICITACAO"] == 2 || $row["TIPO_SOLICITACAO"] == 5 || $row["TIPO_SOLICITACAO"] == 9) {
                $cor = "style='color:green'";
                $data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b>";

            } else if ($row["TIPO_SOLICITACAO"] == 3 || $row["TIPO_SOLICITACAO"] == 6 || $row["TIPO_SOLICITACAO"] == 8) {
                if($row["emergencia"] != 1){
                    $cor = "style='color:purple'";
                }
                $data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b>&nbsp;<span style='color:#000;'><b> Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";

            }

            // verificando o tipo de solicitação e data
            $textoTipoEntrega = '';
            if ($row["emergencia"] == 1) {
                $textoTipoEntrega =" Entrega imediata";
            }
            $dataSolicitado = "Solicitado em {$row['DATA']}";
            if($row["TIPO_SOLICITACAO"] == 2 ){
                $textoTipoSolicitacao = "Prescrição Medica periódica  {$row['inicio']} até {$row['fim']}.";

            }else if($row["TIPO_SOLICITACAO"] == 5){
                $textoTipoSolicitacao = "Prescrição Nutrição  periódica {$row['inicio']} até {$row['fim']}.";
            }else if($row["TIPO_SOLICITACAO"] == 9){
                $textoTipoSolicitacao = "Prescrição Enfermagem  periódica {$row['inicio']} até {$row['fim']}.";

            }else if($row["TIPO_SOLICITACAO"] == 3){
                $textoTipoSolicitacao = "Prescrição Médica Aditiva {$row['inicio']} até {$row['fim']}.";

            }else if($row["TIPO_SOLICITACAO"] == 6){
                $textoTipoSolicitacao = "Prescrição Nutrição Aditiva {$row['inicio']} até {$row['fim']}.";

            }else if($row["TIPO_SOLICITACAO"] == 8){
                $textoTipoSolicitacao = "Prescrição Enfermagem Aditiva {$row['inicio']} até {$row['fim']}.";
            }else if($row["TIPO_SOLICITACAO"] == -1){
                $textoTipoSolicitacao = "Solicitação Avulsa.";

            }

            ////////verificar se é de kit
            if(empty($row['kit'])){
                $kit = " ";

            }else{
                $kit = "<b>(kit-{$row['kit']})</b> ";
            }
            ////////
            ////////
            ////jeferson 21-11-2013 ver se o item esát pendente e trazer o checked marcado.
            if($row['PENDENTE']=='S'){
                $pendente= "checked='checked'";
            }else{
                $pendente='';
            }
            ///
            $informacaoTipoSolicitado = "{$textoTipoSolicitacao} {$dataSolicitado}. {$textoTipoEntrega}";

            $nome = " {$row['principio']} {$row['apresentacao']}";
            $dados = "sol='{$row['sol']}' enf='{$row['enfermeiro']}' nome='{$nome}' e='{$emergencia}'  info-adicional = '{$informacaoTipoSolicitado}'";
            $cancela = "<img align='right' sol='{$row['sol']}' class='cancela-item' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
            $trocar = "<img align='right' nomeItem='{$nome}' catalogoAntigo = '{$row['CATALOGO_ID']}' entrega-imediata='{$row['entrega_imediata']}' sol='{$row['sol']}' presc='{$row['idPrescricao']}' tipo='med' tipo_solicitacao='{$row["TIPO_SOLICITACAO"]}' kit='{$row['KIT_ID']}' inicio='{$row['inicio']}' fim='{$row['fim']}'  data_entrega='{$row['DATA_MAX_ENTREGA']}' status='{$row['status']}' class='troca-item' src='../utils/exchange_16x16.png' title='Trocar' border='0'>";
            $status = "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' pendente='{$row['PENDENTE']}' qtd='{$row['qtd']}' d='1' /><button class='aceitePB' alt='Autorizar' title='Autorizar' id='A{$row['sol']}'></button>";
            $status .= "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' pendente='{$row['PENDENTE']}' qtd='{$row['qtd']}' d='-1'/><button class='negarPB' alt='Negar' title='Negar' id='N{$row['sol']}'></button>";
            $campo = "<input type='text' name='campo-{$row['sol']}' class='qtdAut ' id='qtdAut{$row['sol']}' size='2'>";
            $but = "<button class='limpar' title='Desmarcar' val='{$row['sol']}'>D</button>";
            $labelFlag = '';
            if(!empty($row['flag'])) {
              $labelFlag = "<div class='ui-state-highlight' style='margin-top: 5px;'>
                    <center><b>{$row['flag']}</center>
            </div>";
          }
            echo "<tr {$cor}>
                <td align='center'><input type='checkbox' $pendente solicitacao_id='{$row['sol']}' class='pendentes' /></td>
                <td class='item-auditoria'><b>MEDICAMENTO: </b><span style='color:#000;'>{$kit}</span>{$nome} {$emergencia_label} {$data_entrega} {$labelFlag}  </td>
                <td width='5%'>{$cancela} {$trocar}</td>";
            echo "<td align='center'>".$row['autorizado']."/".$row['qtd']."</td>
                <td align='center'>$parecerButton {$row['estoque']}</td><td width='22%'>$status $campo $but</td>
              </tr>";
            if($row['Just'] != "" || $row['Antigas'] != ""){
                $compPeriodica = '';
                if($row['JUSTIFICATIVA_AVULSO_ID'] == 2){
                    $sqlCompPeriod = "SELECT inicio, fim, Carater FROM prescricoes WHERE id = '{$row['id_periodica_complemento']}'";
                    $rsComPeriod = mysql_query($sqlCompPeriod);
                    $rowCompPeriod = mysql_fetch_array($rsComPeriod);
                    $caraterPeriod = $rowCompPeriod['Carater'] == '2' ? 'PERIÓDICA MEDICA' : 'PERIÓDICA DE NUTRIÇÃO';
                    $inicioPeriod = \DateTime::createFromFormat('Y-m-d', $rowCompPeriod['inicio'])->format('d/m/Y');
                    $fimPeriod = \DateTime::createFromFormat('Y-m-d', $rowCompPeriod['fim'])->format('d/m/Y');
                    $compPeriodica = sprintf(' DE %s DE %s À %s', $caraterPeriod, $inicioPeriod, $fimPeriod);
                }
                echo "<tr><td colspan='6'><p><b>Justificativa do pedido:</b></p><textarea id='Just-{$row['sol']}' cols='80' readonly='readonly'>{$row['Just']}{$compPeriodica}{$row['Antigas']}</textarea></td></tr>";
            }
            if($row['OBS_PENDENTE'] != ""){
                echo "<tr><td colspan='3'  ><textarea id='pendencia-{$row['sol']}' class='textarea-pendencias OBG' placeholder='Observa&ccedil;&atilde;o do item pendente' cols='80'>{$row['OBS_PENDENTE']}</textarea></td><td colspan='3'>Comentado por {$row['autor']} em {$row['data_pendencia']}</td><tr>";
            }else{
                echo "<tr><td colspan='6'  ><textarea id='pendencia-{$row['sol']}' class='textarea-pendencias ' style='display:none;' placeholder='Observa&ccedil;&atilde;o do item pendente' cols='80'>{$row['OBS_PENDENTE']}</textarea></td></tr>";
            }
            $id="selectJusItemNegado-".$row['sol'];
            $disabled="disabled='disabled'";
            $campo =$this->justificativaItensNegados($id,$disabled);
            echo "<tr> "
                . "<td colspan='2'>".$campo."</td>"
                . "<td colspan='4'>"
                . " <textarea class='texto-justificativa' id='T-{$row['sol']}' style='display:none;' placeholder='Observa&ccedil;&atilde;o do item negado'  cols='80'></textarea>"
                . "</td>"
                . "</tr>";
            echo "<tr>";
            echo "<td colspan='6'><hr style='border: 2px dashed #000'></td>";
            echo "</tr>";
        }
        $sqlmat = "SELECT
                  s.`idPrescricao`,
                  s.PENDENTE,
                  s.OBS_PENDENTE,
            	  s.enfermeiro,
                  u.nome as autor,
                  DATE_FORMAT(s.DATA_PENDENCIA,'%d/%m/%Y %H:%i:%s') as data_pendencia,
                  (SELECT
                        checkl.QTD
                   FROM
                        checklist_enfermagem as checkl
                   WHERE
                        checkl.PACIENTE_ID = '{$p}' AND
                        checkl.CATALOGO_ID = s.CATALOGO_ID AND
                        checkl.DATA = (SELECT
                                        max(check2.DATA)
                                       FROM
                                        checklist_enfermagem as check2
                                       WHERE
                                        check2.PACIENTE_ID = '{$p}' AND
                                        check2.CATALOGO_ID = s.CATALOGO_ID)
                   ) as estoque,
                  s.TIPO_SOLICITACAO,
                  DATE_FORMAT(s.DATA_MAX_ENTREGA,'%d/%m/%Y %H:%i:%s') as data_entrega,
		  (CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia ,
                  (CASE  when s.status= 0 and s.idPrescricao=-1 THEN '1' ELSE '0' END) as avulsa,
				 (SELECT COALESCE(SUM(X0.autorizado),0)
					  FROM
						  solicitacoes AS X0 INNER JOIN
						  catalogo AS X1 ON (X0.CATALOGO_ID = X1.ID) LEFT JOIN
						  prescricoes AS X2 ON (X0.idPrescricao = X2.id )
					  WHERE
						  X0.paciente = '{$p}' AND
						  X0.tipo = '1' AND
						  (X0.status = '1' OR X0.status = '2') ".$between2." AND
						  X0.qtd <> 0 AND
						  X1.ID = s.CATALOGO_ID ) AS autorizado,
				  s.qtd,
				  s.NUMERO_TISS,
          s.CATALOGO_ID,
				  s.idSolicitacoes AS sol,
				  s.status,
				  DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
				  (CASE s.idPrescricao WHEN -1 THEN
				  s.data
				  ELSE
				 DATE_FORMAT(p.inicio,'%d/%m/%Y ') END) AS inicio,
				  (CASE s.idPrescricao WHEN -1 THEN
				  s.data
				  ELSE
				  DATE_FORMAT(p.fim,'%d/%m/%Y ')
				  END) AS fim,
				  b.principio,
				  b.apresentacao,
					k.nome as kit,
					IF (s.JUSTIFICATIVA_AVULSO_ID = 11, CONCAT(jsa.justificativa,': ' ,s.JUSTIFICATIVA_AVULSO), jsa.justificativa) as Just,
					IF (s.JUSTIFICATIVA_AVULSO_ID = null OR s.JUSTIFICATIVA_AVULSO_ID = 0, s.JUSTIFICATIVA_AVULSO, '') as Antigas,
					s.JUSTIFICATIVA_AVULSO_ID,
					s.id_periodica_complemento,
					s.entrega_imediata,
					s.enviado as enviado
  			   FROM
			       solicitacoes AS s
						 INNER JOIN catalogo AS b ON (b.ID = s.CATALOGO_ID)
						 LEFT JOIN prescricoes AS p ON (s.idPrescricao = p.id )
						 LEFT JOIN kitsmaterial as k ON (s.KIT_ID = idKit)
						 LEFT JOIN usuarios as u ON (s.AUTOR_PENDENCIA = u.idUsuarios)
						 LEFT JOIN justificativa_solicitacao_avulsa AS jsa ON (s.JUSTIFICATIVA_AVULSO_ID = jsa.id)
  			   WHERE
			       s.paciente = '{$p}' AND
				   s.tipo = '1' and s.data_auditado ='0000-00-00 00:00:00'   AND
				   (s.status = '0' OR s.status = '2') ".$between."
				   AND s.id_prescricao_curativo = 0
				   AND s.qtd <> 0

				   UNION
				   /*
				   UNION PARA PEGAR OS DADOS NA TABELA PRESCRICAO_CURATIVO
				   */

				   SELECT
                  s.id_prescricao_curativo AS idPrescricao,
                  s.PENDENTE,
                  s.OBS_PENDENTE,
            	  s.enfermeiro,
                  u.nome as autor,
                  DATE_FORMAT(s.DATA_PENDENCIA,'%d/%m/%Y %H:%i:%s') as data_pendencia,
                  (SELECT
                        checkl.QTD
                   FROM
                        checklist_enfermagem as checkl
                   WHERE
                        checkl.PACIENTE_ID = '{$p}' AND
                        checkl.CATALOGO_ID = s.CATALOGO_ID AND
                        checkl.DATA = (SELECT
                                        max(check2.DATA)
                                       FROM
                                        checklist_enfermagem as check2
                                       WHERE
                                        check2.PACIENTE_ID = '{$p}' AND
                                        check2.CATALOGO_ID = s.CATALOGO_ID)
                   ) as estoque,
                  s.TIPO_SOLICITACAO,
                  DATE_FORMAT(p.data_max_entrega,'%d/%m/%Y') as data_entrega,
		  (CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia ,
                  (CASE  when s.status= 0 and s.idPrescricao=-1 THEN '1' ELSE '0' END) as avulsa,
				 (SELECT COALESCE(SUM(X0.autorizado),0)
					  FROM
						  solicitacoes AS X0 INNER JOIN
						  catalogo AS X1 ON (X0.CATALOGO_ID = X1.ID) LEFT JOIN
						  prescricoes AS X2 ON (X0.idPrescricao = X2.id )
					  WHERE
						  X0.paciente = '{$p}' AND
						  X0.tipo = '1' AND
						  (X0.status = '1' OR X0.status = '2') ".$between2." AND
						  X0.qtd <> 0 AND
						  X1.ID = s.CATALOGO_ID ) AS autorizado,
				  s.qtd,
				  s.NUMERO_TISS,
          s.CATALOGO_ID,
				  s.idSolicitacoes AS sol,
				  s.status,
				  DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
				  DATE_FORMAT(p.inicio,'%d/%m/%Y') AS inicio,
				  DATE_FORMAT(p.fim,'%d/%m/%Y') AS fim,
				  b.principio,
				  b.apresentacao,
					k.nome as kit,
					IF (s.JUSTIFICATIVA_AVULSO_ID = 11, CONCAT(jsa.justificativa,': ' ,s.JUSTIFICATIVA_AVULSO), jsa.justificativa) as Just,
					IF (s.JUSTIFICATIVA_AVULSO_ID = null OR s.JUSTIFICATIVA_AVULSO_ID = 0, s.JUSTIFICATIVA_AVULSO, '') as Antigas,
					s.JUSTIFICATIVA_AVULSO_ID,
					s.id_periodica_complemento,
					p.entrega_imediata,
					s.enviado as enviado
  			   FROM
			       solicitacoes AS s
						 INNER JOIN catalogo AS b ON (b.ID = s.CATALOGO_ID)
						 LEFT JOIN prescricao_curativo AS p ON (s.id_prescricao_curativo = p.id )
						 LEFT JOIN kitsmaterial as k ON (s.KIT_ID = idKit)
						 LEFT JOIN usuarios as u ON (s.AUTOR_PENDENCIA = u.idUsuarios)
						 LEFT JOIN justificativa_solicitacao_avulsa AS jsa ON (s.JUSTIFICATIVA_AVULSO_ID = jsa.id)
  			   WHERE
			       s.paciente = '{$p}' AND
				   s.tipo = '1' and s.data_auditado ='0000-00-00 00:00:00'   AND
				   (s.status = '0' OR s.status = '2') ".$between."
				   AND s.idPrescricao = 0
				   AND s.qtd <> 0
				   AND s.TIPO_SOLICITACAO != 10
  			    ORDER BY
				   principio,
				   apresentacao,
				   qtd,
				   enviado";

        $rmat = mysql_query($sqlmat) or die(mysql_error());
        while($row = mysql_fetch_array($rmat)){
            foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
            $emergencia = 0; $emergencia_label = "";
            $parecerButton = '';
            if($row['status'] == '2'){
                $emergencia = '2'; $emergencia_label = "<b><i>Entrega Imediata {$row['data_entrega']}</i></b>";
                echo $this->createDialogParecerEntregaImediata($row['sol']);
                $parecerButton = "<button class='ui-button
                                            ui-widget
                                            ui-corner-all
                                            ui-button-text-only
                                            ui-state-hover
                                            parecer-entrega-imediata'
                                            solicitacao='{$row['sol']}'
                                            apresentacao='Material: {$row['principio']} {$row['apresentacao']}'>
                                            <span class='ui-button-text'>Parecer</span>
                                  </button>";
            }
            if ($row["emergencia"] == 1) {
                $cor = "style='color:red'";
                $data_entrega = "";
            } else if ($row["avulsa"] == 1) {
                $cor = "style='color:blue'";
                if ($row['data_entrega'] != '00/00/0000 00:00:00') {
                    $data_entrega = "&nbsp;<span style='color:#000;'><b> Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";
                } else {
                    $data_entrega = "";
                }
            } else if ($row["TIPO_SOLICITACAO"] == 2 || $row["TIPO_SOLICITACAO"] == 5 || $row["TIPO_SOLICITACAO"] == 9) {
                $cor = "style='color:green'";
                $data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b>";
            } else if ($row["TIPO_SOLICITACAO"] == 3 || $row["TIPO_SOLICITACAO"] == 6 || $row["TIPO_SOLICITACAO"] == 8) {
                if($row["emergencia"] != 1){
                    $cor = "style='color:purple'";
                }
                $data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b>&nbsp;<span style='color:#000;'><b> Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";

            }

            ////////verificar se é de kit
            if(empty($row['kit'])){
                $kit = " ";

            }else{
                $kit = " <b>(kit-{$row['kit']})</b>";
            }
            ////////
            ////////
            ////jeferson 21-11-2013 ver se o item esát pendente e trazer o checked marcado.
            if($row['PENDENTE']=='S'){
                $pendente= "checked='checked'";
            }else{
                $pendente='';
            }
            ///


            // verificando o tipo de solicitação e data
            $textoTipoEntrega = '';
            if ($row["emergencia"] == 1) {
                $textoTipoEntrega =" Entrega imediata";
            }
            $dataSolicitado = "Solicitado em {$row['DATA']}";
            if($row["TIPO_SOLICITACAO"] == 2 ){
                $textoTipoSolicitacao = "Prescrição Medica periódica  {$row['inicio']} até {$row['fim']}.";

            }else if($row["TIPO_SOLICITACAO"] == 5){
                $textoTipoSolicitacao = "Prescrição Nutrição  periódica {$row['inicio']} até {$row['fim']}.";
            }else if($row["TIPO_SOLICITACAO"] == 9){
                $textoTipoSolicitacao = "Prescrição Enfermagem  periódica {$row['inicio']} até {$row['fim']}.";

            }else if($row["TIPO_SOLICITACAO"] == 3){
                $textoTipoSolicitacao = "Prescrição Médica Aditiva {$row['inicio']} até {$row['fim']}.";

            }else if($row["TIPO_SOLICITACAO"] == 6){
                $textoTipoSolicitacao = "Prescrição Nutrição Aditiva {$row['inicio']} até {$row['fim']}.";

            }else if($row["TIPO_SOLICITACAO"] == 8){
                $textoTipoSolicitacao = "Prescrição Enfermagem Aditiva {$row['inicio']} até {$row['fim']}.";
            }else if($row["TIPO_SOLICITACAO"] == -1){
                $textoTipoSolicitacao = "Solicitação Avulsa.";

            }
            $informacaoTipoSolicitado = "{$textoTipoSolicitacao} {$dataSolicitado}. {$textoTipoEntrega}";
            //
            $nome = "{$row['principio']} {$row['apresentacao']}";
            $dados = "sol='{$row['sol']}' info-adicional = '{$informacaoTipoSolicitado}'enf='{$row['enfermeiro']}' nome='{$nome}' e='{$emergencia}'  data_entrega='{$row['data_entrega']}' status='{$row['status']}' tipo_solicitacao='{$row["TIPO_SOLICITACAO"]}' kit='{$row['kit']}' inicio='{$row['inicio']}' fim='{$row['fim']}'  ";
            $cancela = "<img align='right' sol='{$row['sol']}' class='cancela-item' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
            $trocar = "<img align='right' nomeItem='{$nome}' catalogoAntigo = '{$row['CATALOGO_ID']}' entrega-imediata='{$row['entrega_imediata']}' sol='{$row['sol']}' presc='{$row['idPrescricao']}' tipo='mat' tipo_solicitacao='{$row["TIPO_SOLICITACAO"]}' kit='{$row['kit']}' inicio='{$row['inicio']}' fim='{$row['fim']}'   data_entrega='{$row['DATA_MAX_ENTREGA']}' status='{$row['status']}' class='troca-item' src='../utils/exchange_16x16.png' title='Trocar' border='0'>";
            $status = "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' pendente='{$row['PENDENTE']}' qtd='{$row['qtd']}' d='1' /><button class='aceitePB' alt='Autorizar' title='Autorizar' id='A{$row['sol']}'></button>";
            $status .= "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' pendente='{$row['PENDENTE']}' qtd='{$row['qtd']}' d='-1' /><button class='negarPB' alt='Negar' title='Negar' id='N{$row['sol']}'></button>";
            $campo = "<input type='text' name='campo-{$row['sol']}' class='' id='qtdAut{$row['sol']}' size='2'>";
            $but = "<button class='limpar' title='Desmarcar' val='{$row['sol']}'>D</button>";
            echo "<tr {$cor}><td align='center'><input type='checkbox' $pendente solicitacao_id='{$row['sol']}' class='pendentes' /></td><td><b>MATERIAL: </b><span style='color:#000;'>{$kit}</span>{$nome} {$emergencia_label} {$data_entrega} </td><td>{$cancela} {$trocar}</td>";
            echo "<td align='center'>".$row['autorizado']."/".$row['qtd']."</td><td width='63' align='center'>{$parecerButton} {$row['estoque']}</td><td>$status $campo $but</td></tr>";
            if($row['Just'] != "" || $row['Antigas'] != ""){
                $compPeriodica = '';
                if($row['JUSTIFICATIVA_AVULSO_ID'] == 2){
                    $sqlCompPeriod = "SELECT inicio, fim, Carater FROM prescricoes WHERE id = '{$row['id_periodica_complemento']}'";
                    $rsComPeriod = mysql_query($sqlCompPeriod);
                    $rowCompPeriod = mysql_fetch_array($rsComPeriod);
                    $caraterPeriod = $rowCompPeriod['Carater'] == '2' ? 'PERIÓDICA MEDICA' : 'PERIÓDICA DE NUTRIÇÃO';
                    $inicioPeriod = \DateTime::createFromFormat('Y-m-d', $rowCompPeriod['inicio'])->format('d/m/Y');
                    $fimPeriod = \DateTime::createFromFormat('Y-m-d', $rowCompPeriod['fim'])->format('d/m/Y');
                    $compPeriodica = sprintf(' DE %s DE %s À %s', $caraterPeriod, $inicioPeriod, $fimPeriod);
                }
                echo "<tr><td colspan='6'><p><b>Justificativa do pedido:</b></p><textarea id='Just-{$row['sol']}' cols='80' readonly='readonly'>{$row['Just']}{$compPeriodica}{$row['Antigas']}</textarea></td></tr>";
            }
            if($row['OBS_PENDENTE'] != ""){
                echo "<tr><td colspan='3'  ><textarea id='pendencia-{$row['sol']}' class='textarea-pendencias OBG' placeholder='Observa&ccedil;&atilde;o do item pendente' cols='80'>{$row['OBS_PENDENTE']}</textarea></td><td colspan='3'>Comentado por {$row['autor']} em {$row['data_pendencia']}</td></tr>";
            }else{
                echo "<tr><td colspan='6'  ><textarea id='pendencia-{$row['sol']}' class='textarea-pendencias ' style='display:none;' placeholder='Observa&ccedil;&atilde;o do item pendente' cols='80'>{$row['OBS_PENDENTE']}</textarea> </td></tr>";
            }
            $id="selectJusItemNegado-".$row['sol'];
            $disabled="disabled='disabled'";
            $campo =$this->justificativaItensNegados($id,$disabled);
            echo "<tr> "
                . "<td colspan='2'>".$campo."</td>"
                . "<td colspan='4'>"
                . "<textarea class='texto-justificativa' id='T-{$row['sol']}' style='display:none;' placeholder='Observa&ccedil;&atilde;o do item negado' cols='80' ></textarea>"
                . "</td>"
                . "</tr>";
            echo "<tr>";
            echo "<td colspan='6'><hr style='border: 1px dashed #000'></td>";
            echo "</tr>";
        }

        //Dieta
        $sqldieta = "SELECT
                  s.`idPrescricao`,
                  s.PENDENTE,
                  s.OBS_PENDENTE,
            	  s.enfermeiro,
                  u.nome as autor,
                  DATE_FORMAT(s.DATA_PENDENCIA,'%d/%m/%Y %H:%i:%s') as data_pendencia,
                  (SELECT
                        checkl.QTD
                   FROM
                        checklist_enfermagem as checkl
                   WHERE
                        checkl.PACIENTE_ID = '{$p}' AND
                        checkl.CATALOGO_ID = s.CATALOGO_ID AND
                        checkl.DATA = (SELECT
                                        max(check2.DATA)
                                       FROM
                                        checklist_enfermagem as check2
                                       WHERE
                                        check2.PACIENTE_ID = '{$p}' AND
                                        check2.CATALOGO_ID = s.CATALOGO_ID)
                   ) as estoque,
                   s.TIPO_SOLICITACAO,
                   DATE_FORMAT(s.DATA_MAX_ENTREGA,'%d/%m/%Y %H:%i:%s') as data_entrega,
		  (CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia ,
                  (CASE  when s.status= 0 and s.idPrescricao=-1 THEN '1' ELSE '0' END) as avulsa,
				 (SELECT COALESCE(SUM(X0.autorizado),0)
					  FROM
						  solicitacoes AS X0 INNER JOIN
						  catalogo AS X1 ON (X0.CATALOGO_ID = X1.ID) LEFT JOIN
						  prescricoes AS X2 ON (X0.idPrescricao = X2.id )
					  WHERE
						  X0.paciente = '{$p}' AND
						  X0.tipo = '3' AND
						  (X0.status = '1' OR X0.status = '2') ".$between2." AND
						  X0.qtd <> 0 AND
						  X1.ID = s.CATALOGO_ID ) AS autorizado,
				  s.qtd,
				  s.NUMERO_TISS,
                                  s.CATALOGO_ID,
				  s.idSolicitacoes AS sol,
				  s.status,
				  DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
				  (CASE s.idPrescricao WHEN -1 THEN
				  s.data
				  ELSE
				  DATE_FORMAT(p.inicio,'%d/%m/%Y ') END) AS inicio,
				  (CASE s.idPrescricao WHEN -1 THEN
				  s.data
				  ELSE
				   DATE_FORMAT(p.fim,'%d/%m/%Y ')
				  END) AS fim,
				  b.principio,
				  b.apresentacao,
                                  k.nome as kit,
                                  IF (s.JUSTIFICATIVA_AVULSO_ID = 11, CONCAT(jsa.justificativa,': ' ,s.JUSTIFICATIVA_AVULSO), jsa.justificativa) as Just,
                                  IF (s.JUSTIFICATIVA_AVULSO_ID = null OR s.JUSTIFICATIVA_AVULSO_ID = 0, s.JUSTIFICATIVA_AVULSO, '') as Antigas,
                                  s.JUSTIFICATIVA_AVULSO_ID,
                                  s.id_periodica_complemento,
                                  s.entrega_imediata
  			   FROM
			       solicitacoes AS s
                               INNER JOIN catalogo AS b ON (b.ID = s.CATALOGO_ID)
                               LEFT JOIN prescricoes AS p ON (s.idPrescricao = p.id )
                               LEFT JOIN kitsmaterial as k ON (s.KIT_ID = idKit)
                               LEFT JOIN usuarios as u ON (s.AUTOR_PENDENCIA = u.idUsuarios)
                               LEFT JOIN justificativa_solicitacao_avulsa AS jsa ON (s.JUSTIFICATIVA_AVULSO_ID = jsa.id)
  			   WHERE
			       s.paciente = '{$p}' AND
				   s.tipo = '3'  and s.data_auditado ='0000-00-00 00:00:00'  AND
				   (s.status = '0' OR s.status = '2') ".$between."
				   AND s.qtd <> 0
  			    ORDER BY
				   principio,
				   apresentacao,
				   qtd,
				   enviado";
        $rdie = mysql_query($sqldieta);
        while($row = mysql_fetch_array($rdie)){
            foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
            $emergencia = 0; $emergencia_label = "";
            $parecerButton = '';
            if($row['status'] == '2'){
                $emergencia = '2'; $emergencia_label = "<b><i>Entrega Imediata {$row['data_entrega']}</i></b>";
                echo $this->createDialogParecerEntregaImediata($row['sol']);
                $parecerButton = "<button class='ui-button
                                            ui-widget
                                            ui-corner-all
                                            ui-button-text-only
                                            ui-state-hover
                                            parecer-entrega-imediata'
                                            solicitacao='{$row['sol']}'
                                            apresentacao='Dieta: {$row['principio']} {$row['apresentacao']}'>
                                            <span class='ui-button-text'>Parecer</span>
                                  </button>";
            }
            if ($row["emergencia"] == 1) {
                $cor = "style='color:red'";
                $data_entrega = "";
            } else if ($row["avulsa"] == 1) {
                $cor = "style='color:blue'";
                if ($row['data_entrega'] != '00/00/0000 00:00:00') {
                    $data_entrega = "&nbsp;<span style='color:#000;'><b>Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";
                } else {
                    $data_entrega = "";
                }
            } else if ($row["TIPO_SOLICITACAO"] == 2 || $row["TIPO_SOLICITACAO"] == 5) {
                $cor = "style='color:green'";
                $data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b>";
            } else if ($row["TIPO_SOLICITACAO"] == 3 || $row["TIPO_SOLICITACAO"] == 6) {
                if($row["emergencia"] != 1){
                    $cor = "style='color:purple'";
                }
                $data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b>&nbsp;<span style='color:#000;'><b>Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";

            }

            ////////verificar se é de kit
            if(empty($row['kit'])){
                $kit = " ";

            }else{
                $kit = "<b>(kit-{$row['kit']})</b> ";
            }
            ////////
            ////jeferson 21-11-2013 ver se o item esát pendente e trazer o checked marcado.
            if($row['PENDENTE']=='S'){
                $pendente= "checked='checked'";
            }else{
                $pendente='';
            }
            ///
            // verificando o tipo de solicitação e data
            $textoTipoEntrega = '';
            if ($row["emergencia"] == 1) {
                $textoTipoEntrega =" Entrega imediata";
            }
            $dataSolicitado = "Solicitado em {$row['DATA']}";
            if($row["TIPO_SOLICITACAO"] == 2 ){
                $textoTipoSolicitacao = "Prescrição Medica periódica  {$row['inicio']} até {$row['fim']}.";

            }else if($row["TIPO_SOLICITACAO"] == 5){
                $textoTipoSolicitacao = "Prescrição Nutrição  periódica {$row['inicio']} até {$row['fim']}.";
            }else if($row["TIPO_SOLICITACAO"] == 9){
                $textoTipoSolicitacao = "Prescrição Enfermagem  periódica {$row['inicio']} até {$row['fim']}.";

            }else if($row["TIPO_SOLICITACAO"] == 3){
                $textoTipoSolicitacao = "Prescrição Médica Aditiva {$row['inicio']} até {$row['fim']}.";

            }else if($row["TIPO_SOLICITACAO"] == 6){
                $textoTipoSolicitacao = "Prescrição Nutrição Aditiva {$row['inicio']} até {$row['fim']}.";

            }else if($row["TIPO_SOLICITACAO"] == 8){
                $textoTipoSolicitacao = "Prescrição Enfermagem Aditiva {$row['inicio']} até {$row['fim']}.";
            }else if($row["TIPO_SOLICITACAO"] == -1){
                $textoTipoSolicitacao = "Solicitação Avulsa.";

            }
            $informacaoTipoSolicitado = "{$textoTipoSolicitacao} {$dataSolicitado}. {$textoTipoEntrega}";
            //
            $nome = "{$kit} {$row['principio']} {$row['apresentacao']}";
            $dados = "sol='{$row['sol']}' info-adicional='{$informacaoTipoSolicitado}' enf='{$row['enfermeiro']}' nome='{$nome}' e='{$emergencia}'  data_entrega='{$row['data_entrega']}' status='{$row['status']}' tipo_solicitacao='{$row["TIPO_SOLICITACAO"]}' kit='{$row['kit']}' inicio='{$row['inicio']}' fim='{$row['fim']}' ";
            $cancela = "<img align='right' sol='{$row['sol']}' class='cancela-item' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
            $trocar = "<img align='right' nomeItem='{$nome}' catalogoAntigo = '{$row['CATALOGO_ID']}' entrega-imediata='{$row['entrega_imediata']}' sol='{$row['sol']}' presc='{$row['idPrescricao']}' tipo_solicitacao='{$row["TIPO_SOLICITACAO"]}' kit='{$row['kit']}' inicio='{$row['inicio']}' fim='{$row['fim']}'   data_entrega='{$row['DATA_MAX_ENTREGA']}' status='{$row['status']}' tipo='die' class='troca-item' src='../utils/exchange_16x16.png' title='Trocar' border='0'>";
            $status = "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' pendente='{$row['PENDENTE']}' qtd='{$row['qtd']}' d='1' /><button class='aceitePB' alt='Autorizar' title='Autorizar' id='A{$row['sol']}'></button>";
            $status .= "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' pendente='{$row['PENDENTE']}' qtd='{$row['qtd']}' d='-1' /><button class='negarPB' alt='Negar' title='Negar' id='N{$row['sol']}'></button>";
            $campo = "<input type='text' name='campo-{$row['sol']}' class='' id='qtdAut{$row['sol']}' size='2'>";
            $but = "<button class='limpar' title='Desmarcar' val='{$row['sol']}'>D</button>";
            echo "<tr {$cor}><td align='center'><input type='checkbox' $pendente solicitacao_id='{$row['sol']}'  class='pendentes' /></td><td><b>DIETA: </b><span style='color:#000;'>{$kit}</span>{$nome} {$emergencia_label} {$data_entrega}</td><td> {$cancela} {$trocar}</td>";
            echo "<td align='center'>".$row['autorizado']."/".$row['qtd']."</td><td align='center'>$parecerButton {$row['estoque']}</td><td>$status $campo $but</td></tr>";
            if($row['Just'] != "" || $row['Antigas'] != ""){
                $compPeriodica = '';
                if($row['JUSTIFICATIVA_AVULSO_ID'] == 2){
                    $sqlCompPeriod = "SELECT inicio, fim, Carater FROM prescricoes WHERE id = '{$row['id_periodica_complemento']}'";
                    $rsComPeriod = mysql_query($sqlCompPeriod);
                    $rowCompPeriod = mysql_fetch_array($rsComPeriod);
                    $caraterPeriod = $rowCompPeriod['Carater'] == '2' ? 'PERIÓDICA MEDICA' : 'PERIÓDICA DE NUTRIÇÃO';
                    $inicioPeriod = \DateTime::createFromFormat('Y-m-d', $rowCompPeriod['inicio'])->format('d/m/Y');
                    $fimPeriod = \DateTime::createFromFormat('Y-m-d', $rowCompPeriod['fim'])->format('d/m/Y');
                    $compPeriodica = sprintf(' DE %s DE %s À %s', $caraterPeriod, $inicioPeriod, $fimPeriod);
                }
                echo "<tr><td colspan='6'><p><b>Justificativa do pedido:</b></p><textarea id='Just-{$row['sol']}' cols='80' readonly='readonly'>{$row['Just']}{$compPeriodica}{$row['Antigas']}</textarea></td></tr>";
            }
            if($row['OBS_PENDENTE'] != ""){
                echo "<tr><td colspan='3'  ><textarea id='pendencia-{$row['sol']}' class='textarea-pendencias OBG' placeholder='Observa&ccedil;&atilde;o do item pendente' cols='80'>{$row['OBS_PENDENTE']}</textarea></td><td colspan='3'>Comentado por {$row['autor']} em {$row['data_pendencia']}</td></tr>";
            }else{
                echo "<tr><td colspan='6'  ><textarea id='pendencia-{$row['sol']}' class='textarea-pendencias ' style='display:none;' placeholder='Observa&ccedil;&atilde;o do item pendente' cols='80'>{$row['OBS_PENDENTE']}</textarea> </td></tr>";
            }
            $id="selectJusItemNegado-".$row['sol'];
            $disabled="disabled='disabled'";
            $campo =$this->justificativaItensNegados($id,$disabled);
            echo "<tr> "
                . "<td colspan='2'>".$campo."</td>"
                . "<td colspan='4'>"
                . "<textarea class='texto-justificativa' id='T-{$row['sol']}' style='display:none;' placeholder='Observa&ccedil;&atilde;o do item negado' cols='80' ></textarea>"
                . "</td>"
                . "</tr>";
            echo "<tr>";
            echo "<td colspan='6'><hr style='line-height: 2px dashed #000;'></td>";
            echo "</tr>";
        }

        $sqlformula = "SELECT
                  s.`idPrescricao`,
                  s.PENDENTE,
                  s.OBS_PENDENTE,
            	  s.enfermeiro,
                  u.nome as autor,
                  DATE_FORMAT(s.DATA_PENDENCIA,'%d/%m/%Y %H:%i:%s') as data_pendencia,
                  (SELECT
                        checkl.QTD
                   FROM
                        checklist_enfermagem as checkl
                   WHERE
                        checkl.PACIENTE_ID = '{$p}' AND
                        checkl.CATALOGO_ID = s.CATALOGO_ID AND
                        checkl.DATA = (SELECT
                                        max(check2.DATA)
                                       FROM
                                        checklist_enfermagem as check2
                                       WHERE
                                        check2.PACIENTE_ID = '{$p}' AND
                                        check2.CATALOGO_ID = s.CATALOGO_ID)
                   ) as estoque,
                   s.TIPO_SOLICITACAO,
                   DATE_FORMAT(s.DATA_MAX_ENTREGA,'%d/%m/%Y %H:%i:%s') as data_entrega,
		  (CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia ,
                  (CASE  when s.status= 0 and s.idPrescricao=-1 THEN '1' ELSE '0' END) as avulsa,
				 (SELECT COALESCE(SUM(X0.autorizado),0)
					  FROM
						  solicitacoes AS X0 INNER JOIN
						  formulas AS X1 ON (X0.CATALOGO_ID = X1.ID) LEFT JOIN
						  prescricoes AS X2 ON (X0.idPrescricao = X2.id )
					  WHERE
						  X0.paciente = '{$p}' AND
						  X0.tipo = '12' AND
						  (X0.status = '1' OR X0.status = '2') ".$between2." AND
						  X1.id = s.CATALOGO_ID ) AS autorizado,
				  s.qtd,
				  s.NUMERO_TISS,
                  s.CATALOGO_ID,
				  s.idSolicitacoes AS sol,
				  s.status,
				  DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
				  (CASE s.idPrescricao WHEN -1 THEN
				  s.data
				  ELSE
				  DATE_FORMAT(p.inicio,'%d/%m/%Y ') END) AS inicio,
				  (CASE s.idPrescricao WHEN -1 THEN
				  s.data
				  ELSE
				   DATE_FORMAT(p.fim,'%d/%m/%Y ')
				  END) AS fim,
				  f.formula,
				  b.principio,
				  b.apresentacao,
				  s.JUSTIFICATIVA_AVULSO_ID,
				  s.id_periodica_complemento,
          s.DATA_MAX_ENTREGA,
          s.JUSTIFICATIVA_AVULSO

  			   FROM
			       solicitacoes AS s INNER JOIN
				   formulas AS f INNER JOIN
				   catalogo AS b ON (b.ID = s.CATALOGO_ID) LEFT JOIN
				   prescricoes AS p ON (s.idPrescricao = p.id ) LEFT JOIN
                                 kitsmaterial as k on (s.KIT_ID = idKit)
                                 LEFT JOIN usuarios as u ON (s.AUTOR_PENDENCIA = u.idUsuarios)
  			   WHERE
			       s.paciente = '{$p}' AND
				   s.tipo = '12'  and s.data_auditado ='0000-00-00 00:00:00'   AND
				   (s.status = '0' OR s.status = '2') ".$between." and s.CATALOGO_ID = f.id
  			    ORDER BY
				   formula,
				   qtd,
				   enviado";

        $rformula = mysql_query($sqlformula);
        while($row = mysql_fetch_array($rformula)){
            foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
            $emergencia = 0; $emergencia_label = "";
            if($row['status'] == '2'){
                $emergencia = '2'; $emergencia_label = "<b><i>Entrega Imediata {$row['data_entrega']}</i></b>";
            }
            if ($row["emergencia"] == 1) {
                $cor = "style='color:red'";
                $data_entrega = "";
            } else if ($row["avulsa"] == 1) {
                $cor = "style='color:blue'";
                if ($row['data_entrega'] != '00/00/0000 00:00:00') {
                    $data_entrega = "&nbsp;<span style='color:#000;'><b>Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";
                } else {
                    $data_entrega = "";
                }
            } else if ($row["TIPO_SOLICITACAO"] == 2 || $row["TIPO_SOLICITACAO"] == 5) {
                $cor = "style='color:green'";
                $data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b>";
            } else if ($row["TIPO_SOLICITACAO"] == 3 || $row["TIPO_SOLICITACAO"] == 6) {
                $cor = "style='color:purple'";
                $data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b>&nbsp;<span style='color:#000;'><b>Prazo m&aacute;ximo&nbsp;" . $row['data_entrega'] . "</b></span>";

            }

            ////////verificar se é de kit
            if(empty($row['kit'])){
                $kit = " ";

            }else{
                $kit = "<b>(kit-{$row['kit']})</b> ";
            }
            ////////
            $nome = " {$kit} {$row['formula']}";
            $dados = "sol='{$row['sol']}' enf='{$row['enfermeiro']}' nome='{$nome}' e='{$emergencia}'   data_entrega='{$row['DATA_MAX_ENTREGA']}' status='{$row['status']}' tipo_solicitacao='{$row["TIPO_SOLICITACAO"]}' kit='{$row['kit']}' inicio='{$row['inicio']}' fim='{$row['fim']}' ";


            $status = "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' pendente='{$row['PENDENTE']}' qtd='{$row['qtd']}' d='1' /><button class='aceitePB' alt='Autorizar' title='Autorizar' id='A{$row['sol']}'></button>";
            $status .= "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' pendente='{$row['PENDENTE']}' qtd='{$row['qtd']}' d='-1' /><button class='negarPB' alt='Negar' title='Negar' id='N{$row['sol']}'></button></button>";
            $campo = "<input type='text' name='campo-{$row['sol']}' class='' id='qtdAut{$row['sol']}' size='2'>";
            $but = "<button class='limpar' title='Desmarcar' val='{$row['sol']}'>D</button>";
            echo "<tr {$cor}><td align='center'><input type='checkbox' $pendente solicitacao_id='{$row['sol']}' class='pendentes' /></td><td>{$nome} {$emergencia_label} {$data_entrega}</td>";
            echo "<td align='center'>".$row['autorizado']."/".$row['qtd']."</td><td align='center'>{$row['estoque']}</td><td>$status $campo $but</td></tr>";
            if($row['JUSTIFICATIVA_AVULSO'] != ""){
                echo "<tr><td colspan='6'><p><b>Justificativa do pedido:</b></p><textarea id='Just-{$row['sol']}' cols='80' readonly='readonly'> {$row['JUSTIFICATIVA_AVULSO']}</textarea></td></tr>";
            }
            if($row['OBS_PENDENTE'] != ""){
                echo "<tr><td colspan='3'  ><textarea id='pendencia-{$row['sol']}' class='textarea-pendencias OBG' placeholder='Observa&ccedil;&atilde;o do item pendente' cols='80'>{$row['OBS_PENDENTE']}</textarea></td><td colspan='3'>Comentado por {$row['autor']} em {$row['data_pendencia']}</td></tr>";
            }else{
                echo "<tr><td colspan='6'  ><textarea id='pendencia-{$row['sol']}' class='textarea-pendencias OBG' style='display:none;' placeholder='Observa&ccedil;&atilde;o do item pendente' cols='80'>{$row['OBS_PENDENTE']}</textarea> </td></tr>";
            }
            echo "<tr>";
            echo "<td colspan='6'><hr style='line-height: 2px dashed #000;'></td>";
            echo "</tr>";
        }


        $sqlequipamento = "SELECT
  	s.`idPrescricao`,
          s.PENDENTE,
          s.OBS_PENDENTE,
  	s.enfermeiro,
        u.nome as autor,
      DATE_FORMAT(s.DATA_PENDENCIA,'%d/%m/%Y %H:%i:%s') as data_pendencia,
        (SELECT
                        checkl.QTD
                   FROM
                        checklist_enfermagem as checkl
                   WHERE
                        checkl.PACIENTE_ID = '{$p}' AND
                        checkl.CATALOGO_ID = s.CATALOGO_ID AND
                        checkl.TIPO = 5 AND
                        checkl.DATA = (SELECT
                                        max(check2.DATA)
                                       FROM
                                        checklist_enfermagem as check2
                                       WHERE
                                        check2.PACIENTE_ID = '{$p}' AND
                                        check2.TIPO = 5 AND
                                        check2.CATALOGO_ID = s.CATALOGO_ID)
                   ) as estoque,
         s.TIPO_SOLICITACAO,
         DATE_FORMAT(s.DATA_MAX_ENTREGA,'%d/%m/%Y %H:%i:%s') as data_entrega,
	(CASE s.status when 2 THEN '1' ELSE '0' END) as emergencia ,
         (CASE  when s.status= 0 and s.idPrescricao=-1 THEN '1' ELSE '0' END) as avulsa,
  	(SELECT COALESCE(SUM(X0.autorizado),0)
  	FROM
  	solicitacoes AS X0 INNER JOIN
  	cobrancaplanos AS X1 ON (X0.CATALOGO_ID = X1.id) LEFT JOIN
  	prescricoes AS X2 ON (X0.idPrescricao = X2.id )
  	WHERE
  	X0.paciente = '{$p}' AND
  	X0.tipo = '5' AND
  	X0.qtd <> 0 AND
  	(X0.status = '1' OR X0.status = '2') ".$between2." AND
  	X1.id = s.CATALOGO_ID ) AS autorizado,
  	s.qtd,
  	s.NUMERO_TISS,
        s.CATALOGO_ID,
  	s.idSolicitacoes AS sol,
  	s.status,
  	DATE_FORMAT(s.data,'%d/%m/%Y') AS DATA,
  	(CASE s.idPrescricao WHEN -1 THEN
  	s.data
  	ELSE
  	DATE_FORMAT(p.inicio,'%d/%m/%Y ') END) AS inicio,
  	(CASE s.idPrescricao WHEN -1 THEN
  	s.data
  	ELSE
  	 DATE_FORMAT(p.fim,'%d/%m/%Y ')
	END) AS fim,
  	cb.item,
  	s.TIPO_SOL_EQUIPAMENTO,
        k.nome as kit,
        s.JUSTIFICATIVA_AVULSO,
        s.JUSTIFICATIVA_AVULSO_ID,
        s.id_periodica_complemento,
        s.entrega_imediata,
        IF (s.JUSTIFICATIVA_AVULSO_ID = 11, CONCAT(jsa.justificativa,': ' ,s.JUSTIFICATIVA_AVULSO), jsa.justificativa) as Just,
      IF (s.JUSTIFICATIVA_AVULSO_ID = null OR s.JUSTIFICATIVA_AVULSO_ID = 0, s.JUSTIFICATIVA_AVULSO, '') as Antigas,
      s.JUSTIFICATIVA_AVULSO_ID
  	FROM
            solicitacoes AS s INNER JOIN
            cobrancaplanos AS cb ON (cb.id = s.CATALOGO_ID) LEFT JOIN
            prescricoes AS p ON (s.idPrescricao = p.id ) LEFT JOIN
            kitsmaterial as k on (s.KIT_ID = idKit)
            left JOIN usuarios as u ON (s.AUTOR_PENDENCIA = u.idUsuarios)
             LEFT JOIN justificativa_solicitacao_avulsa AS jsa ON (s.JUSTIFICATIVA_AVULSO_ID = jsa.id)
  	WHERE
  	s.paciente = '{$p}' AND
  	s.tipo = '5' and s.data_auditado ='0000-00-00 00:00:00'   AND
  	(s.status = '0' OR s.status = '2') ".$between."
  	AND s.qtd <> 0
  	ORDER BY
  	cb.item,
  	qtd,
  	enviado";

        $reqp = mysql_query($sqlequipamento);
        while($row = mysql_fetch_array($reqp)){
            foreach($row AS $key => $value) {
                $row[$key] = stripslashes($value);
            }
            $emergencia = 0; $emergencia_label = "";
            $parecerButton = '';
            if($row['status'] == '2'){
                $emergencia = '2'; $emergencia_label = "<b><i>Entrega Imediata {$row['data_entrega']}</i></b>";
                echo $this->createDialogParecerEntregaImediata($row['sol']);
                $parecerButton = "<button class='ui-button
                                            ui-widget
                                            ui-corner-all
                                            ui-button-text-only
                                            ui-state-hover
                                            parecer-entrega-imediata'
                                            solicitacao='{$row['sol']}'
                                            apresentacao='Equipamento: {$row['item']}'>
                                            <span class='ui-button-text'>Parecer</span>
                                  </button>";
            }
            if($row['TIPO_SOL_EQUIPAMENTO'] == 1){
                $tipo_sol="Enviar";
            }
            if($row['TIPO_SOL_EQUIPAMENTO'] == 2){
                $tipo_sol="Recolher";
            }
            if($row['TIPO_SOL_EQUIPAMENTO'] == 3){
                $tipo_sol="Substituir";
            }
            if ($row["emergencia"] == 1) {
                $cor = "style='color:red'";
                $data_entrega = "";
            } else if ($row["avulsa"] == 1) {
                $cor = "style='color:blue'";
                if ($row['data_entrega'] != '00/00/0000 00:00:00') {
                    $data_entrega = "&nbsp;<span style='color:#000;'><b>Prazo " . htmlentities("máximo") . "&nbsp;" . $row['data_entrega'] . "</b></span>";
                } else {
                    $data_entrega = "";
                }
            } else if ($row["TIPO_SOLICITACAO"] == 2 || $row["TIPO_SOLICITACAO"] == 5) {
                $cor = "style='color:#006400'";
                $data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b>";
            } else if ($row["TIPO_SOLICITACAO"] == 3 || $row["TIPO_SOLICITACAO"] == 6) {
                if($row["emergencia"] != 1){
                    $cor = "style='color:purple'";
                }
                $data_entrega = "<b>{$row['inicio']} at&eacute; {$row['fim']}</b>&nbsp;<span style='color:#000;'><b>Prazo " . htmlentities("máximo") . "&nbsp;" . $row['data_entrega'] . "</b></span>";

            }

            ////////verificar se é de kit
            if(empty($row['kit'])){
                $kit = " ";

            }else{
                $kit = "<b>(kit-{$row['kit']})</b> ";
            }
            ////////
            ////jeferson 21-11-2013 ver se o item esát pendente e trazer o checked marcado.
            if($row['PENDENTE']=='S'){
                $pendente= "checked='checked'";
            }else{
                $pendente='';
            }
            ///
            // verificando o tipo de solicitação e data
            $textoTipoEntrega = '';
            if ($row["emergencia"] == 1) {
                $textoTipoEntrega =" Entrega imediata";
            }
            $dataSolicitado = "Solicitado em {$row['DATA']}";
            if($row["TIPO_SOLICITACAO"] == 2 ){
                $textoTipoSolicitacao = "Prescrição Medica periódica  {$row['inicio']} até {$row['fim']}.";

            }else if($row["TIPO_SOLICITACAO"] == 5){
                $textoTipoSolicitacao = "Prescrição Nutrição  periódica {$row['inicio']} até {$row['fim']}.";
            }else if($row["TIPO_SOLICITACAO"] == 9){
                $textoTipoSolicitacao = "Prescrição Enfermagem  periódica {$row['inicio']} até {$row['fim']}.";

            }else if($row["TIPO_SOLICITACAO"] == 3){
                $textoTipoSolicitacao = "Prescrição Médica Aditiva {$row['inicio']} até {$row['fim']}.";

            }else if($row["TIPO_SOLICITACAO"] == 6){
                $textoTipoSolicitacao = "Prescrição Nutrição Aditiva {$row['inicio']} até {$row['fim']}.";

            }else if($row["TIPO_SOLICITACAO"] == 8){
                $textoTipoSolicitacao = "Prescrição Enfermagem Aditiva {$row['inicio']} até {$row['fim']}.";
            }else if($row["TIPO_SOLICITACAO"] == -1){
                $textoTipoSolicitacao = "Solicitação Avulsa.";

            }
            $informacaoTipoSolicitado = "{$textoTipoSolicitacao} {$dataSolicitado}. {$textoTipoEntrega}";
            //
            $nome = " {$row['item']} ";
            $dados = "sol='{$row['sol']}' info-adicional='{$informacaoTipoSolicitado}' enf='{$row['enfermeiro']}' nome='{$nome}' e='{$emergencia}' tipo='5' tipo_sol_equipamento='{$row['TIPO_SOL_EQUIPAMENTO']}'   data_entrega='{$row['DATA_MAX_ENTREGA']}' status='{$row['status']}' tipo_solicitacao='{$row["TIPO_SOLICITACAO"]}' kit='{$row['kit']}' inicio='{$row['inicio']}' fim='{$row['fim']}' ";
            $cancela = "<img align='right' sol='{$row['sol']}' class='cancela-item' src='../utils/delete_16x16.png' title='Cancelar' border='0'>";
            $trocar = "<img align='right' nomeItem='{$nome}' catalogoAntigo = '{$row['CATALOGO_ID']}' entrega-imediata='{$row['entrega_imediata']}' sol='{$row['sol']}' presc='{$row['idPrescricao']}' tipo_solicitacao='{$row["TIPO_SOLICITACAO"]}' kit='{$row['kit']}' inicio='{$row['inicio']}' fim='{$row['fim']}'  data_entrega='{$row['data_entrega']}' status='{$row['status']}' tipo='eqp' class='troca-item' src='../utils/exchange_16x16.png' title='Trocar' border='0'>";
            $status = "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' d='1' /><button class='aceitePB' alt='Autorizar' title='Autorizar' id='A{$row['sol']}'></button>";
            $status .= "<input type='radio' name='item-{$row['sol']}' {$dados} class='liberacao' d='-1' /><button class='negarPB' alt='Negar' title='Negar' id='N{$row['sol']}'></button>";
            $campo = "<input type='text' name='campo-{$row['sol']}' class='' id='qtdAut{$row['sol']}' size='2'>";
            $but = "<button class='limpar' title='Desmarcar' val='{$row['sol']}'>D</button>";
            echo "<tr {$cor}><td align='center'><input type='checkbox' $pendente solicitacao_id='{$row['sol']}' class='pendentes' /></td><td><b>Equipamento: </b>{$tipo_sol} {$nome} {$emergencia_label} {$data_entrega} </td><td>{$cancela} {$trocar}</td>";
            echo "<td align='center'>".$row['autorizado']."/".$row['qtd']."</td><td align='center'>$parecerButton {$row['estoque']}</td><td>$status $campo $but</td></tr>";
            if($row['Just'] != "" || $row['Antigas'] != ""){
                $compPeriodica = '';
                if($row['JUSTIFICATIVA_AVULSO_ID'] == 2){
                    $sqlCompPeriod = "SELECT inicio, fim, Carater FROM prescricoes WHERE id = '{$row['id_periodica_complemento']}'";
                    $rsComPeriod = mysql_query($sqlCompPeriod);
                    $rowCompPeriod = mysql_fetch_array($rsComPeriod);
                    $caraterPeriod = $rowCompPeriod['Carater'] == '2' ? 'PERIÓDICA MEDICA' : 'PERIÓDICA DE NUTRIÇÃO';
                    $inicioPeriod = \DateTime::createFromFormat('Y-m-d', $rowCompPeriod['inicio'])->format('d/m/Y');
                    $fimPeriod = \DateTime::createFromFormat('Y-m-d', $rowCompPeriod['fim'])->format('d/m/Y');
                    $compPeriodica = sprintf(' DE %s DE %s À %s', $caraterPeriod, $inicioPeriod, $fimPeriod);
                }
                echo "<tr><td colspan='6'><p><b>Justificativa do pedido:</b></p><textarea id='Just-{$row['sol']}' cols='80' readonly='readonly'>{$row['Just']}{$compPeriodica}{$row['Antigas']}</textarea></td></tr>";
            }
            if($row['OBS_PENDENTE'] != ""){
                echo "<tr><td colspan='3'  ><textarea id='pendencia-{$row['sol']}' class='textarea-pendencias OBG' placeholder='Observa&ccedil;&atilde;o do item pendente' cols='80'>{$row['OBS_PENDENTE']}</textarea></td><td colspan='3'>Comentado por {$row['autor']} em {$row['data_pendencia']}</td></tr>";
            }else{
                echo "<tr><td colspan='6'  ><textarea id='pendencia-{$row['sol']}' class='textarea-pendencias ' style='display:none;' placeholder='Observa&ccedil;&atilde;o do item pendente' cols='80'>{$row['OBS_PENDENTE']}</textarea> </td></tr>";
            }

            $id="selectJusItemNegado-".$row['sol'];
            $disabled="disabled='disabled'";
            $campo =$this->justificativaItensNegados($id,$disabled);

            echo "<tr> "
                . "<td colspan='2'>".$campo."</td>"
                . "<td colspan='4'>"
                . "<textarea class='texto-justificativa' id='T-{$row['sol']}' placeholder='Observa&ccedil;&atilde;o do item negado' cols='80' style='display:none;' ></textarea>"
                . "</td>"
                . "</tr>";
            echo "<tr>";
            echo "<td colspan='6'><hr style='line-height: 2px dashed #000;'></td>";
            echo "</tr>";
        }
        echo "<tr style='display:none;' class='textarea-pendencias-todos'>
							<td colspan='6'>
								<b>Observação Geral dos Itens Pendentes:</b><br/>
								<small style='letter-spacing: 1px; font-size: 9px;'>*Se houver alguma observação pertinente a um único item, faça-o após escrever a observação geral.</small><br/>
								<textarea id='pendencia-todos' placeholder='Observa&ccedil;&atilde;o Geral dos Itens Pendentes' cols='80'></textarea>
							</td>
						</tr>";
        echo "</table>";

        echo "</div>";

    }

    private function auditar_periodo($p,$inicio=null,$fim=null,$idP,$e){
        $dir = __DIR__ . '/cache';
        $cache = new CacheAdapter(new SimpleCache($dir));
        $cache->adapter->cleanCache();

        $row = mysql_fetch_array(mysql_query("SELECT c.nome FROM clientes as c WHERE c.idClientes = '$p'"));

        echo "<center><h1>Sa&iacute;da para o paciente:<h1>";
        $paciente = strtoupper($row['nome']);
        echo "<input type='hidden' name='p'  id='p'>";

        echo "<h3>" . String::removeAcentosViaEReg($paciente) . "</h3></center>";
        echo "<fieldset style='background-color:#eee;'>";
        echo "<center>";
        echo "<b>" . htmlentities("PERÍODO DE AUTORIZAÇÃO") . ":</b> "."<input type='text' class='data OBG' id='periodo-autorizado1' name='periodo-autorizado1' value='{$_GET['i']}' >"." at&eacute; "."<input type='text' class='data OBG' id='periodo-autorizado2' name='periodo-autorizado2' value='{$_GET['f']}' ><button id='buscar-auditoria' p='{$p}'>Buscar</button><br>";
        echo "</center>";
        echo "</fieldset>";
        echo "<br>";
        echo "<div id='auditoria_atual'>";
        $this->auditar($p,$inicio,$fim,$idP,$e);
        echo "</div>";
        echo "<label id='texto-justificativa-geral' for='selJustItemNegadoGeral'>";
        echo "</label>";
        echo "<div id='justificativa-geral'>";
        echo $this->justificativaItensNegados('','','geral');
        echo "<br/>";
        echo "<td colspan='5'><textarea id='TGeral' style='display:none;' placeholder='Observa&ccedil;&atilde;o do item negado'  cols='80'></textarea></td>";
        echo "</div>";
        echo "<br/><button id='salvar-auditoria' empresa='$e'>Finalizar</button>";
        echo "<button id='sair-sem-salvar'>Voltar</button>";
        echo "<button id='salvar-pendencia' empresa='$e'>Salvar Pend&ecirc;ncia</button>";
    }

    private function solicitacoes_pendentes(){

        echo "<div><center><h1>Solicita&ccedil;&otilde;es Pendentes</h1></center>";
        echo <<<HTML
<div>
	<button
		id="reload"
		class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
		role="button"
		aria-disabled="false">
			<span class="ui-button-text">
				↺ Atualizar
			</span>
	</button>
</div>
<br>
HTML;

        $vazio                  = true;
        $solicitacoesAdAvEm     = [];
        $solicitacoesPeriodicas = [];
        $pendentes              = [];
        $naoVisualizados        = [];
        $empresas               = [];

        $cache = new CacheAdapter(new SimpleCache(__DIR__ . '/cache'));
        $cachedAvAdEm = $cache->adapter->read('solicitacoes-pendentes-ad-av-em');
        $cachedPeriodicas = $cache->adapter->read('solicitacoes-pendentes-periodicas');

        if (!$cachedAvAdEm || !$cachedPeriodicas) {
            // Armazenando as solicitacoes pendentes
            $sqlPendentes = <<<SQL
SELECT
	count(PENDENTE) as pend,
	paciente
FROM
	solicitacoes
WHERE
	(
		STATUS = '0'
		OR STATUS = '2'
	)
AND data_auditado = '0000-00-00 00:00:00'
AND PENDENTE = 'S'
AND qtd <> 0
GROUP BY
	paciente
SQL;
            $rs = mysql_query($sqlPendentes);
            while($row = mysql_fetch_array($rs, MYSQL_ASSOC))
                $pendentes[$row['paciente']] = $row['pend'];

            // Armazenando as solicitacoes nao visualizadas
            $sqlNaoVisualizados = <<<SQL
SELECT
	count(visualizado) AS nao_visualizado,
	paciente
FROM
	solicitacoes
WHERE
	(STATUS = '0' OR STATUS = '2')
AND data_auditado = '0000-00-00 00:00:00'
AND visualizado = 0
AND qtd <> 0
GROUP BY
	paciente
SQL;
            $rs = mysql_query($sqlNaoVisualizados);
            while($row = mysql_fetch_array($rs, MYSQL_ASSOC))
                $naoVisualizados[$row['paciente']] = $row['nao_visualizado'];

            // Trazendo todos os pacientes a serem auditados do tipo Avulsa, Aditiva e Emergencia
            $sql = <<<SQL
SELECT
	c.nome,
	s.paciente,
	c.empresa,
	s.TIPO_SOLICITACAO,
	DATE_FORMAT(
		s.DATA_SOLICITACAO,
		'%d/%m/%Y %H:%i:%s'
	)AS data,
	s.idPrescricao AS idP,
	p.nome AS convenio,
	emp.nome AS unidade_regional,
	MAX(primeira_prescricao)AS primeira_prescricao
FROM
	solicitacoes AS s
INNER JOIN clientes AS c ON(s.paciente = c.idClientes)
INNER JOIN planosdesaude AS p ON(c.convenio = p.id)
INNER JOIN empresas AS emp ON(c.empresa = emp.id)
WHERE
	(s. STATUS = '0' OR s. STATUS = '2')
AND s.data_auditado = '0000-00-00 00:00:00'
AND emp.id = {$_SESSION['empresa_principal']}
AND s.TIPO_SOLICITACAO IN ('-2', '-1', '1', '3', '6', '7', '8')
AND s.qtd <> 0
GROUP BY
	s.paciente
ORDER BY
	primeira_prescricao DESC,
	MIN(s. DATA)ASC
SQL;
            $r = mysql_query($sql);
            while($row = mysql_fetch_array($r, MYSQL_ASSOC)) {
                $row['pend'] = 0;
                $row['nao_visualizado'] = 0;
                if(array_key_exists($row['paciente'], $pendentes))
                    $row['pend'] = $pendentes[$row['paciente']];
                if(array_key_exists($row['paciente'], $naoVisualizados))
                    $row['nao_visualizado'] = $naoVisualizados[$row['paciente']];

                $solicitacoesAdAvEm[] = $row;
            }
            $cache->adapter->save('solicitacoes-pendentes-ad-av-em', $solicitacoesAdAvEm);

            // Trazendo todos os pacientes a serem auditados do tipo Periódicas
            $sql = <<<SQL
SELECT
	c.nome,
	s.paciente,
	c.empresa,
	s.TIPO_SOLICITACAO,
	DATE_FORMAT(
		s.DATA_SOLICITACAO,
		'%d/%m/%Y %H:%i:%s'
	)AS data,
	s.idPrescricao AS idP,
	p.nome AS convenio,
	emp.nome AS unidade_regional,
	MAX(primeira_prescricao)AS primeira_prescricao
FROM
	solicitacoes AS s
INNER JOIN clientes AS c ON(s.paciente = c.idClientes)
INNER JOIN planosdesaude AS p ON(c.convenio = p.id)
INNER JOIN empresas AS emp ON(c.empresa = emp.id)
WHERE
	(s. STATUS = '0' OR s. STATUS = '2')
AND s.data_auditado = '0000-00-00 00:00:00'
AND emp.id = {$_SESSION['empresa_principal']}
AND s.TIPO_SOLICITACAO NOT IN ('-2', '-1', '1', '3', '6', '7', '8')
AND s.qtd <> 0
GROUP BY
	s.paciente
ORDER BY
	primeira_prescricao DESC,
	MIN(s. DATA)ASC
SQL;
            $r = mysql_query($sql);
            while($row = mysql_fetch_array($r, MYSQL_ASSOC)) {
                $row['pend'] = 0;
                $row['nao_visualizado'] = 0;
                if(array_key_exists($row['paciente'], $pendentes))
                    $row['pend'] = $pendentes[$row['paciente']];
                if(array_key_exists($row['paciente'], $naoVisualizados))
                    $row['nao_visualizado'] = $naoVisualizados[$row['paciente']];

                $solicitacoesPeriodicas[] = $row;
            }
            $cache->adapter->save('solicitacoes-pendentes-periodicas', $solicitacoesPeriodicas);
        } else {
            $solicitacoesAdAvEm = $cachedAvAdEm;
            $solicitacoesPeriodicas = $cachedPeriodicas;
        }

        $sql = <<<SQL
SELECT
	empresas.id,
	empresas.nome
FROM
	solicitacoes
INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
INNER JOIN empresas ON clientes.empresa = empresas.id
WHERE
	solicitacoes.autorizado < solicitacoes.qtd
AND(
	solicitacoes.`status` = 0
	OR solicitacoes.`status` = 2
)
AND solicitacoes.data_auditado = '0000-00-00 00:00:00'
AND empresas.id != {$_SESSION['empresa_principal']}
AND solicitacoes.qtd <> 0
GROUP BY
	empresas.id
SQL;
        $r = mysql_query($sql);
        $empresaPrincipalTab = $_SESSION['empresa_principal'] - 1; // Coloca #tab-0, para não conflitar com o ID das outras Tabs
        while($row = mysql_fetch_array($r, MYSQL_ASSOC))
            $empresas[] = $row;
        echo '<div id="solicitacoes-pendentes-tabs">';
        echo '<ul>';
        if($_SESSION['empresa_principal'] != '1') {
            echo "<li><a href='#tabs-{$empresaPrincipalTab}'>UR " . str_replace('Mederi', '', $_SESSION["nome_empresa"]) . "</a></li>";
        }
        foreach($empresas as $empresa) {
            echo "<li><a class='tabs-empresas' href='ajax_auditoria.php?action=solicitacoes&empresa={$empresa['id']}'>UR " . str_replace('Mederi', '', ucwords(strtolower($empresa['nome']))) . "</a></li>";
        }
        echo '</ul>';
        if($_SESSION['empresa_principal'] != '1') {
            echo "<div id='tabs-{$empresaPrincipalTab}'>";

            echo '<div style="display: table;" class="solicitacoes-pendentes-tipos-tabs">';
            echo '<ul>';
            echo "<li><a href='#tabs-aditivas-avulsas-emergencias-{$empresaPrincipalTab}'>Aditivas/Avulsas/Emergências</a></li>";
            echo "<li><a href='#tabs-periodicas-{$empresaPrincipalTab}'>Periódicas</a></li>";
            echo '</ul>';
            echo "<div id='tabs-aditivas-avulsas-emergencias-{$empresaPrincipalTab}'>";
            foreach ($solicitacoesAdAvEm as $row) {
                $vazio = false;
                if ($row['pend'] > 0) {
                    $pendente = "Pendencia";
                } else {
                    $pendente = "";
                }
                echo "<div style='border:1px solid #ccc;height:200px;width:240px;display:table;float:left;margin:10px; padding:15px; font-size:12px;' class='wrapper'>";
                if ($row['nao_visualizado'] > 0) {
                    $qtd_nao_visualizado = str_pad($row['nao_visualizado'], 2, '0', STR_PAD_LEFT);
                    echo "<div class='ribbon-wrapper-green'><div class='ribbon-green'> {$qtd_nao_visualizado} Novo(s)</div></div>";
                }

                $class_button_paciente = $row['primeira_prescricao'] == 0 ? "bpaciente" : "bpaciente_prioridade";

                echo "<table width='100%'>";
                echo "<tr>";
                echo "<td color='red'>";
                echo "<center><span style='color:red'><b>$pendente</b></span></center>";
                echo "</td>";
                echo "</tr>";
                echo "<tr>";
                echo "<td>";
                echo "<center><button class='{$class_button_paciente}' p='{$row['paciente']}' idP='{$row['idP']}' empresa={$row['empresa']}></button></center>";
                echo "</td>";
                echo "</tr>";
                echo "<tr>";
                echo "<td>";
                echo "<center><b>" . strtoUpper(String::removeAcentosViaEReg($row['nome'])) . "</b></center>";
                echo "</td>";
                echo "</tr>";
                echo "<tr>";
                echo "<td>";
                echo "<center><span style='color:blue'><b>" . strtoUpper($row['convenio']) . "</b></span></center>";
                echo "</td>";
                echo "</tr>";
                echo "<tr>";
                echo "<tr>";
                echo "<td>";
                echo "<center><span style='color:blue'><b>" . strtoUpper($row['unidade_regional']) . "</b></span></center>";
                echo "</td>";
                echo "</tr>";
                echo "<tr>";
                echo "<td>";
                echo "<center>" . implode('/', array_reverse(explode("-", $row['data']))) . "</center>";
                echo "</td>";
                echo "</tr>";
                echo "</tr>";
                echo "</table>";
                echo "</div>";
            }
            echo "</div>";

            echo "<div id='tabs-periodicas-{$empresaPrincipalTab}'>";
            foreach ($solicitacoesPeriodicas as $row) {
                $vazio = false;
                if ($row['pend'] > 0) {
                    $pendente = "Pendencia";
                } else {
                    $pendente = "";
                }
                echo "<div style='border:1px solid #ccc;height:200px;width:240px;display:table;float:left;margin:10px; padding:15px; font-size:12px;' class='wrapper'>";
                if ($row['nao_visualizado'] > 0) {
                    $qtd_nao_visualizado = str_pad($row['nao_visualizado'], 2, '0', STR_PAD_LEFT);
                    echo "<div class='ribbon-wrapper-green'><div class='ribbon-green'> {$qtd_nao_visualizado} Novo(s)</div></div>";
                }

                $class_button_paciente = $row['primeira_prescricao'] == 0 ? "bpaciente" : "bpaciente_prioridade";

                echo "<table width='100%'>";
                echo "<tr>";
                echo "<td color='red'>";
                echo "<center><span style='color:red'><b>$pendente</b></span></center>";
                echo "</td>";
                echo "</tr>";
                echo "<tr>";
                echo "<td>";
                echo "<center><button class='{$class_button_paciente}' p='{$row['paciente']}' idP='{$row['idP']}' empresa={$row['empresa']}></button></center>";
                echo "</td>";
                echo "</tr>";
                echo "<tr>";
                echo "<td>";
                echo "<center><b>" . strtoUpper(String::removeAcentosViaEReg($row['nome'])) . "</b></center>";
                echo "</td>";
                echo "</tr>";
                echo "<tr>";
                echo "<td>";
                echo "<center><span style='color:blue'><b>" . strtoUpper($row['convenio']) . "</b></span></center>";
                echo "</td>";
                echo "</tr>";
                echo "<tr>";
                echo "<tr>";
                echo "<td>";
                echo "<center><span style='color:blue'><b>" . strtoUpper($row['unidade_regional']) . "</b></span></center>";
                echo "</td>";
                echo "</tr>";
                echo "<tr>";
                echo "<td>";
                echo "<center>" . implode('/', array_reverse(explode("-", $row['data']))) . "</center>";
                echo "</td>";
                echo "</tr>";
                echo "</tr>";
                echo "</table>";
                echo "</div>";
            }
            echo "</div>";
            echo "</div>";

            if ($vazio)
                echo "<p id='vazio' style='text-align:center;color:red;'><b>Nenhuma solicita&ccedil;&atilde;o pendente!</b></p>";
            echo "</div>";
        }
        $pacientesAuditaosHoje = new PacientesAuditadosHoje();
        $pacientesAuditaosHoje ->listarPacientesAuditadosHoje();

        echo "</div>";
        echo "</div>";
    }

    private function arquivos($d,$p){
        echo "<center><h1>Prontu&aacute;rio Eletr&ocirc;nico</h1></center>";
        if($p == NULL){
            echo "<div id='prontuarios'>";
            echo alerta();
            echo "<p><b>Paciente:</b>".$this->pacientes('paciente','paciente','n');
            echo "<p>".$this->pastas();
            echo "</div>";
        } else {
            echo "<form enctype='multipart/form-data' name='form' action='query.php' method='POST'>";
            echo  "<input type='hidden' value='salvar-prontuario' name='query' />";
            echo "<input type='hidden' value='{$p}' name='paciente' />";
            echo "<input type='hidden' value='{$d}' name='pasta' />";
            echo "<input type='file' name='prontuario'/>";
            echo "<input type='submit' value='Enviar'/><i>(Limite max de 16mb)</i>";
            echo "</form>";
            $p = anti_injection($p,'numerico');
            $d = anti_injection($d,'numerico');
            $sql = "SELECT a.id, d.nome as pasta, p.nome as paciente, a.nome, DATE_FORMAT(a.data,'%d/%m/%Y - %T') as data
  				FROM pastasProntuarios as d, clientes as p, prontuarios as a
  				WHERE d.id = '$d' AND p.idClientes = '$p' AND d.id = a.pasta AND p.idClientes = a.paciente ";
            $r = mysql_query($sql);
            echo "<div id='arquivos'>";
            echo "<table width=100% border=1 id='lista-arquivos' ><thead><tr><th width=50% >Pasta</th><th>Arquivo</th><th>Data</th></tr></thead>";
            while($row = mysql_fetch_array($r)){
                $pasta = strtoupper($row['pasta']);
                $paciente = strtoupper($row['paciente']);
                $icon = "icon-anexo.png";
                $ext = strrchr($row['nome'],".");
                switch($ext){
                    case ".doc":
                    case ".docx":
                        $icon = "word_24x24.png";
                        break;
                    case ".xls":
                    case ".xlsx":
                    case ".xlsm":
                    case ".csv":
                        $icon = "excel_24x24.png";
                        break;
                    case ".pdf":
                        $icon = "pdf_24x24.png";
                        break;
                    case ".jpg":
                    case ".jpeg":
                    case ".png":
                    case ".bmp":
                    case ".eps":
                    case ".tiff":
                    case ".gif":
                        $icon = "img_24x24.png";
                        break;
                    case ".ppt":
                    case ".pptx":
                        $icon = "ppt_24x24.png";
                        break;
                }
                $b = "<a href='query.php?query=show-prontuario&id={$row['id']}'><img src='../utils/{$icon}' border='0'></a>";
                echo "<tr><td>{$paciente} > {$pasta}</td><td>{$row['nome']} {$b}</td><td>{$row['data']}</td></tr>";
            }
            echo "</table>";
            echo "</div>";
        }
    }

    public function justificativaItensNegados($id,$disabled,$tipo = null){
        if($tipo == null){
            $var .= "<label for='$id'>Justificativa Item Negado:</label>";
            $var .= "<select class='selJustItemNegado' id='{$id}' name='{$id}' $disabled  >";
        }else
            $var .= "<select class='selJustItemNegadoGeral' id='selJustItemNegadoGeral' name='selJustItemNegadoGeral' style='display:none;'  >";
        $sql= "SELECT
                   justificativa_itens_negados.ID,
                   justificativa_itens_negados.JUSTIFICATIVA
                   FROM
                   justificativa_itens_negados
                   WHERE
                    1";
        $result=  mysql_query($sql);
        $cont=1;
        while($row=  mysql_fetch_array($result)){
            if ($cont==1){
                $var .= "<option value='-1'>Selecione</option>";
                $cont++;
            }
            $var .= "<option value='".$row['ID']."'>".$row['JUSTIFICATIVA']."</option>";

        }
        $var .="</select>";
        return $var;
    }

    public function view($v){
        switch ($v) {
            case "menu":
                $this->menu();
                break;
            case "relatorios":
                $this->menuRelatorios();
                break;
            case "solicitacoes":
                $this->solicitacoes_pendentes();
                break;
            case "auditar":
                if(isset($_GET["p"])){
                    if(isset($_GET["i"]))$inicio = $_GET["i"]; else $inicio = null;
                    if(isset($_GET["f"]))	$fim = $_GET["f"]; else $fim = null;
                    $this->auditar($_GET["p"],$inicio,$fim,$_GET["idP"]);
                }else{
                    $this->solicitacoes_pendentes();
                }
                break;
            case "auditar_periodo":
                if(isset($_GET["paciente"])){
                    if(isset($_GET["i"]))$inicio = $_GET["i"]; else $inicio = null;
                    if(isset($_GET["f"]))	$fim = $_GET["f"]; else $fim = null;
                    $this->auditar_periodo($_GET["paciente"],$inicio,$fim,$_GET["idP"],$_GET["e"]);
                }else{
                    $this->solicitacoes_pendentes();
                }
                break;
            case "arquivos":
                if(isset($_GET["pasta"]) && isset($_GET["paciente"]))
                    $this->arquivos($_GET["pasta"],$_GET["paciente"]);
                else $this->arquivos(NULL,NULL);
                break;
        }

    }
}

?>