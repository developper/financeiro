$(function($){

  $('.numerico').keyup(function(e){
    this.value = this.value.replace(/\D/g,'');
  });

  $(".hora").timeEntry({show24Hours: true, defaultTime: '08:00'});

  var dates = $( "#inicio, #fim" ).datepicker({
	defaultDate: "+1w",
	changeMonth: true,
	numberOfMonths: 3,
	onSelect: function( selectedDate ) {
		var option = this.id == "inicio" ? "minDate" : "maxDate",
		instance = $( this ).data( "datepicker" );
		date = $.datepicker.parseDate(
		instance.settings.dateFormat ||
		$.datepicker._defaults.dateFormat,
		selectedDate, instance.settings );
		dates.not( this ).datepicker( "option", option, date );
	}
  });

    $(".parecer-entrega-imediata").click(function() {
        var $this = $(this);
        var solicitacao_parecer = $this.attr('solicitacao');
        var apresentacao = $this.attr('apresentacao');
        var $dialog = $("#dialog-parecer-entrega-imediata-" + solicitacao_parecer);
        $dialog.find("span#label-item-parecer").html('Parecer sobre:<br/><b>' + apresentacao + '</b>');
        $dialog.find('.parecer-textarea').attr('apresentacao', apresentacao);

        $("#dialog-parecer-entrega-imediata-" + solicitacao_parecer).dialog({
            autoOpen: false,
            modal: true,
            width: 500,
            buttons: {
                "Salvar": function () {
                    $(this).dialog("close");
                },
                "Cancelar": function () {
                    $(this).find('.parecer-textarea').val('');
                    $(this).dialog("close");
                }
            }
        });
        $dialog.dialog("open");
    });

   $(".div-aviso").hide();

   function validar_campos(div){
    var ok = true;
    $("#"+div+" .OBG").each(function (i){
        
      if(this.value == ""){
		ok = false;
		this.style.border = "3px solid red";
      } else {
		this.style.border = "";
      }

    });
    $("#"+div+" .COMBO_OBG option:selected").each(function (i){
      if(this.value == "-1"){
		ok = false;
		$(this).parent().css({"border":"3px solid red"});
      } else {
		$(this).parent().css({"border":""});
      }
    });
    if(!ok){
      $("#"+div+" .aviso").text("Os campos em vermelho são obrigatórios!");
      $("#"+div+" .div-aviso").show();
    } else {
      $("#"+div+" .aviso").text("");
      $("#"+div+" .div-aviso").hide();
    }
    return ok;
  }

  $(".bpaciente, .bpaciente_prioridade").live('click', function(){
  	var p = $(this).attr('p');
  	var idP = $(this).attr('idP');
      var emp = $(this).attr('empresa');
      $.post('query.php',{
               query:'atualizar_visualizacao',
               paciente: p,
               idPrescricao: idP
      },function(r){
              if(r){
                window.location = '?view=auditar_periodo&paciente='+p+'&idP='+idP+'&e='+emp;
              }else{
                  alert('Erro ao tentar enviar sua requisição tente novamente!');
              }

      });

  });

  // SM-941

  $("#solicitacoes-pendentes-tabs").tabs({
    beforeLoad: function( event, ui ) {
      ui.jqXHR.fail(function() {
        ui.panel.html(
          "Não foi possível carregar as solicitações dessa UR!"
        );
      });
    },
    spinner: "Carregando...",
    load: function( event, ui ) {
      $(".solicitacoes-pendentes-tipos-tabs").tabs( 'load', 0 );
    }
  });

  $(".solicitacoes-pendentes-tipos-tabs").tabs();

  $("#reload").click(function(){
    $.get(
      'ajax_auditoria.php',
      { action:'reload' },
      function(r){
        window.location = '?view=solicitacoes';
    });
  });

  $("#buscar-auditoria").click(function(){
	  var inicio = $('#periodo-autorizado1').val();
	  var fim = $('#periodo-autorizado2').val();
	  var p = $(this).attr('p');

		window.location.href='/auditoria/?view=auditar_periodo&paciente='+p+'&i='+inicio+'&f='+fim;
  });


  $(".limpar").click(function(){


	  var valor = $(this).attr('val');

	  //if($('.liberacao').is(':checked')){
		  $('input:radio[name=item-'+valor+']').attr('checked',false);
		  $("#A"+valor).removeClass('aceite');
		  $("#A"+valor).addClass('aceitePB');
		  $('#qtdAut'+valor).removeClass('OBG');
                  $('#T-'+valor).attr('style','display:none;');
                  $('#T-'+valor).removeClass('OBG');
		  $("#N"+valor).removeClass('negar');
		  $("#N"+valor).addClass('negarPB');
                  $('#selectJusItemNegado-'+valor).attr('disabled',true);
                 $('#selectJusItemNegado-'+valor).removeClass('COMBO_OBG');
                 $('#selectJusItemNegado-'+valor).val('-1');
	 // }

  });

  $("#sair-sem-salvar").click(function(){
	 window.location.href = '?view=solicitacoes';

  });

  $(".bpasta").click(function(){
  	if(validar_campos("prontuarios")){
  		var pasta = $(this).attr('p');
  		var paciente = $("select[name='paciente'] option:selected").val();
  		window.location = '?view=arquivos&paciente='+paciente+'&pasta='+pasta;
  	}
  	return false;
  });

  $(".liberacao").click(function(){
	  var name = $(this).attr('name');
	  var id = name.split('-');
	   if($(this).attr('d')==1){
		 $('#A'+id[1]).removeClass('aceitePB');
		 $('#A'+id[1]).addClass('aceite');
		 $('#N'+id[1]).removeClass('negar');
		 $('#N'+id[1]).addClass('negarPB');
         $('#T-'+id[1]).attr('style','display:none;');
         $('#T-'+id[1]).removeClass('OBG');
         $('#qtdAut'+id[1]).addClass('OBG');
         $('#selectJusItemNegado-'+id[1]).attr('disabled',true);
         $('#selectJusItemNegado-'+id[1]).removeClass('COMBO_OBG');
         $('#selectJusItemNegado-'+id[1]).val('-1');
         $('#selectJusItemNegado-'+id[1]).attr('style','');


	  }else if($(this).attr('d')==-1){
		 $('#N'+id[1]).removeClass('negarPB');
		 $('#N'+id[1]).addClass('negar');
		 $('#A'+id[1]).removeClass('aceite');
		 $('#A'+id[1]).addClass('aceitePB');
                 //$('#T-'+id[1]).attr('style','');
                 //$('#T-'+id[1]).addClass('OBG');
                 $('#qtdAut'+id[1]).removeClass('OBG');
                 $('#selectJusItemNegado-'+id[1]).attr('disabled',false);
                 $('#selectJusItemNegado-'+id[1]).addClass('COMBO_OBG');

	  }

  });
  /////22-11-2013 jeferson aceitar e negar todos
    $(".todos").click(function() {
        if ($(this).val() == '1') {
            //colocar cor no botão aceitar todos e fazer o negar todos ficar transparente
                    $('#Atodos').removeClass('aceitePB');
                    $('#Atodos').addClass('aceite');
                    $('#Ntodos').removeClass('negar');
                    $('#Ntodos').addClass('negarPB');
            $(".liberacao").each(function() {
                ///marcar todos os aceitar e fazer com que estes fiquem com cor e colocando o negar  para ficar transparente
                var name = $(this).attr('name');
	        var id = name.split('-');
                if ($(this).attr('d') == 1) {
                    $(this).attr('checked','checked');
                    $('#A' + id[1]).removeClass('aceitePB');
                    $('#A' + id[1]).addClass('aceite');
                    $('#N' + id[1]).removeClass('negar');
                    $('#N' + id[1]).addClass('negarPB');
                    $('#T-'+id[1]).attr('style','display:none;');
                    $('#T-'+id[1]).removeClass('OBG');
                    $('#qtdAut'+id[1]).addClass('OBG');
                    $('#selectJusItemNegado-'+id[1]).removeClass('COMBO_OBG');
                    $('#selectJusItemNegado-'+id[1]).val('-1');
                    $('#selectJusItemNegado-'+id[1]).attr('disabled',true);
                    $('#selectJusItemNegado-'+id[1]).attr('style','');


                }
            });

        } else {
            //colocar cor no botão negar todos e fazer o aceitar todos ficar transparente
             $('#Ntodos').removeClass('negarPB');
             $('#Ntodos').addClass('negar');
             $('#Atodos').removeClass('aceite');
             $('#Atodos').addClass('aceitePB');
             var cont = $(".selJustItemNegado").size();
             if(cont > 1){
                $('#texto-justificativa-geral').html('<b>Justificativa Geral:</b><br/>');
                $('#selJustItemNegadoGeral').show().focus().change(function(){
                    var itemNegadoGeral = $(this);
                    $(".selJustItemNegado").each(function(index, liberacao) {
                           $(liberacao).attr('value', itemNegadoGeral.val()).change();
                           $('#TGeral').hide();
                    });
                    if(itemNegadoGeral.val() == '6'){
                        $('#TGeral').show().focus().keyup(function(){
                            $(".texto-justificativa").each(function() {
                                $(this).attr('value',$('#TGeral').val());
                            });
                        });
                    }else
                        $('#TGeral').hide();
                });
             }
             $(".liberacao").each(function() {
                ///marcar todos os negar e fazer com que estes fiquem com cor e colocando o aceitar para ficar transparente
                var name = $(this).attr('name');
	        var id = name.split('-');
                if ($(this).attr('d') == -1) {
                    $(this).attr('checked','checked');
                    $('#N' + id[1]).removeClass('negarPB');
                    $('#N' + id[1]).addClass('negar');
                    $('#A' + id[1]).removeClass('aceite');
                    $('#A' + id[1]).addClass('aceitePB');
                    $('#qtdAut'+id[1]).removeClass('OBG');
                    $('#selectJusItemNegado-'+id[1]).attr('disabled',false);
                    $('#selectJusItemNegado-'+id[1]).addClass('COMBO_OBG');

                }
            });
        }
    });

    $('.selJustItemNegado').change(function(){
        var name = $(this).attr('name');
        var id = name.split('-');
       if($(this).val()==6){
            $('#T-'+id[1]).attr('style','');
            $('#T-'+id[1]).addClass('OBG');
        }else{
            $('#T-'+id[1]).attr('style','display:none;');
            $('#T-'+id[1]).removeClass('OBG');
            $('#T-'+id[1]).attr('style','display:none;');
            $('#T-'+id[1]).val('');
        }

    });

    $('.PTodos').live('click', function(){
        $('#thpendencias').html("<button class='RPTodos' id='RPTodos' alt='Pendencia' title='Marcar Todos como não Pendentes' ></button> Pendente");
        $('.textarea-pendencias, .textarea-pendencias-todos').show();
        $('.textarea-pendencias').each(function(){
            $(this).addClass('OBG');
        });
        $('#pendencia-todos').focus();
        $('.pendentes').each(function(){
            $(this).attr('checked', 'checked');
        });
    });

    $('.RPTodos').live('click', function(){
        $('#thpendencias').html("<button class='PTodos' id='PTodos' alt='Pendencia' title='Marcar Todos como Pendentes' ></button> Pendente");
        $('.textarea-pendencias, .textarea-pendencias-todos').hide().attr('value', '');
        $('.textarea-pendencias').each(function(){
            $(this).removeClass('OBG');
        });
        $('.pendentes').each(function(){
            $(this).attr('checked', '');
        });
    });

    $('#pendencia-todos').focus().keyup(function(){
        $(".textarea-pendencias").each(function() {
            $(this).attr('value',$('#pendencia-todos').val());
        });
    });

  $("#add-observacao").click(function(){
      var observacao = $("#obs_audt").val();
      var id = $("#div-envio-paciente").attr("p");
      $.post("query.php",{
            query: "salvar_observacao",
            observacao: observacao,
            id: id
        }, function(r) {
            $("#observacoes").append(r);
        });
        return false;
  });

  /**************************************** TROCA DE ITENS / CANCELAR **********************************************************/
  $("#cancelar-item").dialog({
	  	autoOpen: false, modal: true, width: 800, position: 'top',
	  	open: function(event,ui){
	  		$("#cancelar-item .aviso").text("");
	     	$("#cancelar-item .div-aviso").hide();
	  		$("#cancelar-item input[name='justificativa']").val('');
	  		$("#cancelar-item input[name='justificativa']").css({"border":""});
	  	    $("#cancelar-item input[name='justificativa']").focus();
	  	},
	  	buttons: {
	  		"Cancelar" : function(){
	  			$(this).dialog("close");
	  		},
	  		"Salvar" : function(){
	  			if(validar_campos('cancelar-item')){
	  				var just = $("#cancelar-item input[name='justificativa']").val();
	  				var sol = $("#cancelar-item").attr("sol");
	  				$.post("query.php",{query: "cancelar-item-solicitacao", sol: sol, just: just},function(r){
	  					if(r == '1'){
	  						alert('Cancelamento salvo com sucesso!');
	  						window.location.reload();
	  					}
	  				});
	  				$(this).dialog("close");
	  			}
	  		}
	  	}
	  });

	  $(".cancela-item").click(function(){
	  	$("#cancelar-item").attr("sol",$(this).attr("sol"));
	  	$("#cancelar-item").dialog("open");
	  });

  $("#busca-item-troca").autocomplete({
		//source: "/utils/busca_brasindice.php",
	  source: function(request, response) {
	        $.getJSON('busca_brasindice.php', {
	            term: request.term,
	            tipo: $("#formulario-troca").attr("tipo")
	        }, response);},
		minLength: 3,
		select: function( event, ui ) {
				$('#nome').val(ui.item.value);
    			$('#cod').val(ui.item.id);
                $('#catalogo_id').val(ui.item.catalogo_id);
			    $('#busca-item-troca').val('');
			    $("input[name='qtd']").focus();
		}
});

$("#formulario-troca").dialog({
autoOpen: false, modal: true, width: 800, position: 'top',
open: function(event,ui){
	$("#formulario-troca input[name='justificativa']").val('');
	$("input[name='busca']").val('');
	$('#nome').val('');
  $('#nome').attr('readonly','readonly');
  $('#apr').val('');
  $('#apr').attr('readonly','readonly');
  $('#catalogo_id').val('');
  $('#cod').val('');
    $('#qtd').val('');
  $("input[name='busca']").focus();
},
buttons: {
	"Cancelar" : function(){
		$(this).dialog("close");
	},
	"Trocar" : function(){
		if(validar_campos('formulario-troca')){
			var just = $("#formulario-troca input[name='justificativa']").val();
			var sol = $("#formulario-troca").attr("sol");
			var tipo = $("#formulario-troca").attr("tipo");
			var presc = $("#formulario-troca").attr("presc");
			var item = $("#formulario-troca input[name='cod']").val();
                        var catalogo_id = $("#formulario-troca input[name='catalogo_id']").val();
			var qtd = $("#formulario-troca input[name='qtd']").val();
			var n = $("#formulario-troca input[name='nome']").val();
            var itemOriginal =  $("#formulario-troca").attr("nomeItem");
            var catalogoAntigo = $("#formulario-troca").attr("catalogoAntigo");



			var p = $("#div-envio-paciente").attr("p");
                        var tipo_solicitacao = $("#formulario-troca").attr("tipo_solicitacao");
                        var data_entrega= $("#formulario-troca").attr("data_entrega");
                        var entrega_imediata= $("#formulario-troca").attr("entrega-imediata");

			$.post("query.php",{query: "trocar-item-solicitacao", sol: sol, just: just, tipo: tipo, item: item, qtd: qtd, p: p, n: n,presc:presc,catalogo_id:catalogo_id,data_entrega:data_entrega,
                    tipo_solicitacao:tipo_solicitacao,data_entrega:data_entrega,
                    entrega_imediata:entrega_imediata,itemOriginal:itemOriginal, catalogoAntigo:catalogoAntigo},
				function(r){
					if(r == '1'){
						alert('Troca efetuada com sucesso!');
						window.location.reload();
					}else{
                        alert('Erro ao fazer a troca:'+ r);
                        return false;
                    }
			});
			$(this).dialog("close");
		}
	}
}
});

  $(".troca-item").click(function(){
	  	var tipo = $(this).attr("tipo");
	  	var sol = $(this).attr("sol");
	  	var presc = $(this).attr("presc");
                var inicio = $(this).attr("inicio");
                var fim = $(this).attr("fim");
                var data_entrega = $(this).attr("data_entrega");
                var status = $(this).attr("status");
                var kit = $(this).attr("kit");
                var tipo_solicitacao=$(this).attr("tipo_solicitacao");
                var entrega_imediata = $(this).attr("entrega-imediata");
                var catalogoAntigo = $(this).attr('catalogoAntigo');
                $("#formulario-troca").attr("nomeItem",$(this).attr("nomeItem"));
	  	        $("#formulario-troca").attr("sol",sol);
	        	$("#formulario-troca").attr("tipo",tipo);
	  	        $("#formulario-troca").attr("presc",presc);
                $("#formulario-troca").attr("inicio",inicio);
                $("#formulario-troca").attr("catalogoAntigo",catalogoAntigo);
                $("#formulario-troca").attr("fim",fim);
                $("#formulario-troca").attr("data_entrega",data_entrega);
                $("#formulario-troca").attr("status",status);
                $("#formulario-troca").attr("kit",kit);
                $("#formulario-troca").attr("tipo_solicitacao",tipo_solicitacao);
                $("#formulario-troca").attr("entrega-imediata",entrega_imediata);
	  	$("#formulario-troca").dialog("open");
  });

  /*********************************************************************************************************/
  $(".qtdAut").keyup(function() {
	   var valor = $(this).val().replace(/[^0-9]/g,'');
	   $(this).val(valor);
	});

    $("#salvar-auditoria").click(function() {

        if (confirm('Deseja realmente salvar a auditoria?')) {
            if (validar_campos('div-envio-paciente')) {
                var contador = 0;
                var itens = "";
                var empresa = $(this).attr('empresa');

                var total = $(".liberacao:checked").size();
                var flag = 0;
                var pareceres = [];

                $(".parecer-textarea").each(function() {
                    var $this = $(this);
                    var solicitacao = $this.attr('solicitacao');
                    var apresentacao = $this.attr('apresentacao');

                    if($this.val() != '') {
                        pareceres.push({'solicitacao': solicitacao, 'apresentacao': apresentacao, 'parecer': $this.val()});
                    }
                });
              //  console.log(pareceres);

                $(".liberacao:checked").each(function(i) {
                    var sol = $(this).attr("sol");
                    var d = $(this).attr("d");
                    var camp = $('#qtdAut' + sol).val();
                    contador++;


                    if (camp == "")
                        var campo = 0;
                    else
                        var campo = $('#qtdAut' + sol).val();

                    if (camp == "" && d == 1) {
                        //$('#qtdAut'+sol).css('border','3px solid red');
                        $('#qtdAut' + sol).focus();
                        flag = 1;
                        total = 0;
                    } else {

                        var pendente = $(this).attr('pendente');
                        var qtdSol = $(this).attr('qtd');
                        var decisao = $(this).attr('d');
                        var enf = $(this).attr('enf');
                        var nome = $(this).attr('nome');
                        var emerg = $(this).attr('e');
                        var just = $('#T-' + sol).val();
                        var infoAdiconal = $(this).attr('info-adicional');
                        var idJustificativaNegado= $('#selectJusItemNegado-'+ sol).val();
                        var textJustificativaNegado= $("select[name='selectJusItemNegado-"+ sol+"'] option:selected").text();

                        var linha = sol + "#" + decisao + "#" + enf + "#" + nome + "#" + emerg + "#" + campo + "#" + just + "#" + idJustificativaNegado + "#" + textJustificativaNegado + "#" + infoAdiconal + "#" + pendente + "#" + qtdSol;
                        total = $(".liberacao:checked").size();
                        if (i < total - 1)
                            linha += "$";
                        itens += linha;
                    }
                });
                if(contador > 0){
                    $.post("query.php", {query: "salvar-auditoria", dados: itens, pareceres: pareceres, np: $("#div-envio-paciente").attr('np'), f: 1, empresa: empresa}, function(r) {
                    if (r == "1") {
                        alert("Salvo com sucesso!");
                        window.location = "?view=solicitacoes";
                    }
                    else {
                        alert("ERRO: " + r);
                    }
                });
            } else {//acusa erro caso nada tenha sido marcado
                alert("ERRO: A auditoria está vazia.");
            }
            } else {//acusa erro caso algum campo obg tenha sido ativado porém continue sem ser preenchido
                alert("ERRO: A auditoria está vazia. Há campos obrigatórios a serem preenchidos. ");
            }
        }

        return false;
    });
  ////////////////////////// 21-11-2013
  

  $(".pendentes").click(function(){
      var sol = $(this).attr("solicitacao_id");
      if($(this).is(":checked")){
          $('#pendencia-'+sol).attr('style','display:inline');
          $('#pendencia-'+sol).addClass('OBG');
      }else{
          $('#pendencia-'+sol).attr('style','display:none');
          $('#pendencia-'+sol).removeClass('OBG');
      }
  });

  $("#salvar-pendencia").click(function(){
      var itens = "";
      var total = $(".pendentes").size();
      var flag = 0;
      if (validar_campos('div-envio-paciente')){
      $(".pendentes").each(function(i){
          if($(this).is(":checked")){
              var pendente = "S";
          }else{
              var pendente = "N";
          }
          var solicitacao = $(this).attr('solicitacao_id');
          
         var item= $("input[name='item-"+solicitacao+"']").attr('nome');
         var inicio= $("input[name='item-"+solicitacao+"']").attr('inicio');
         var fim = $("input[name='item-"+solicitacao+"']").attr('fim');
        
          if(pendente == "N"){
              var just_pendente = "";
          }else{
              var just_pendente = $('#pendencia-'+solicitacao).val();
          }
          var linha = solicitacao+"#"+just_pendente+"#"+pendente+"#"+item;
          if(i<total - 1){
              linha += "$";
          }
              itens += linha;
      });
          var paciente = $("#div-envio-paciente").attr('np');
          var empresa = $(this).attr('empresa');
         

          $.post("query.php",{query: "pendencia-item-solicitacao",
              dados:itens,
              empresa:empresa,
              paciente:paciente
              
          },function(r){
              if(r == '1'){
                  $(".pendentes").each(function(i){
                      if($(this).is(":checked")){
                          var pendente = "S";
                      }else{
                          var pendente = "N";
                      }
                      $(this).parents('tr').find('.liberacao').attr('pendente', pendente);
                  });
                  alert("Pendencia salva com sucesso!!");
              }
          });
      }else{
          return false;
      }
  });
  ///////////////////////

});