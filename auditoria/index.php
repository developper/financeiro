<?php
include_once('../validar.php');
include_once('auditoria.php');
include_once('pacientes_auditados_hoje.php');

if(!function_exists('redireciona')){
	function redireciona($link){
		if ($link==-1){
			echo" <script>history.go(-1);</script>";
		}else{
			echo" <script>document.location.href='$link'</script>";
		}
	}
}

include($_SERVER['DOCUMENT_ROOT'].'/cabecalho.php');

/*destroi a sess�o da busca de medico*/
unset($_SESSION['paciente']);
?>
<script src="auditoria.js?v=6" type="text/javascript"></script>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/cabecalho_fim.php');
?>
</div>
<div id="content">
<div id="right">
<?php
   $view = "menu";
   if(isset($_GET["view"])) $view = $_GET["view"];
   if(!validar_tipo("modaud") && !(validar_tipo("modenf") && ($view == "arquivos"))){
	 redireciona('/inicio.php');
   }
   $aud = new Auditoria;
   $aud->view($view);
?>
</div>
<div id="left">
<?php include($_SERVER['DOCUMENT_ROOT'].'/painel_login.php'); ?>
<!-- <div class="box">
  <ul>
   <li><a href='?view=solicitacoes'>Auditar Solicita&ccedil;&otilde;es</a></li>
   <li><a href='?view=arquivos'>Acessar Prontu&aacute;rios</a></li>
  </ul>
</div>
<div class="box">
<div id='calculadora'></div>
</div> -->
</div>
</div>
</body>
</html>
