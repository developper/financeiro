<?php
ini_set('display_errors', 'On');
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

use \App\Controllers\BuscarSolicitacoes;
use \App\Controllers\Auditoria;
use \App\Services\CacheAdapter;
use \App\Services\Cache\SimpleCache;

if (isset($_GET['action']) && !empty($_GET['action'])) {

	$action = $_GET['action'];

	switch ($action) {
		case "solicitacoes":
			if(isset($_GET["empresa"]) && !empty($_GET["empresa"]))
				echo BuscarSolicitacoes::buscarSolicitacoesNaoAuditadasPorEmpresa($_GET['empresa']);
			break;
		case "reload":
			$cache = new CacheAdapter(new SimpleCache(__DIR__ . '/cache'));
			$cache->adapter->cleanCache();
			break;
	}
}

if (isset($_POST['action']) && !empty($_POST['action'])) {

	$action = $_POST['action'];

	switch ($action) {
		case "enviar-parecer":
			if(isset($_POST["solicitacao"]) && !empty($_POST["solicitacao"]))
				echo Auditoria::enviarParecerEntregaImediata($_POST);
			break;
	}
}