<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';
//ini_set('display_errors', 1);

use \App\Controllers\Auditoria;

define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);



$routes = [
	"GET" => [
		'/auditoria/relatorios/?action=form-relatorio-faturas-liberadas'                => '\App\Controllers\Auditoria::formRelatorioFaturasLiberadas',
		'/auditoria/relatorios/?action=form-relatorio-pedidos-liberados'    		    => '\App\Controllers\Auditoria::formRelatorioPedidosLiberados',
		'/auditoria/relatorios/?action=form-relatorio-faturamento-enviado' 			    => '\App\Controllers\Auditoria::formRelatorioFaturamentoEnviado',
		'/auditoria/relatorios/?action=form-relatorio-nao-conformidades'			    => '\App\Controllers\Auditoria::formRelatorioNaoConformidades',
		'/auditoria/relatorios/?action=form-relatorio-saidas'			                => '\App\Controllers\Auditoria::formRelatorioSaidas',
        '/auditoria/relatorios/?action=excel-relatorio-saidas' 	                        => '\App\Controllers\Auditoria::excelRelatorioSaidas',
		'/auditoria/relatorios/?action=combo-busca-itens'			                    => '\App\Controllers\Auditoria::comboBuscaItens',
		'/auditoria/relatorios/?action=relatorio-nao-conformidades-busca-condensada'	=> '\App\Controllers\Auditoria::buscaCondensadaRelatorioNaoConformidades',
		'/auditoria/relatorios/?action=relatorio-nao-conformidades-gerenciar'	        => '\App\Controllers\Auditoria::gerenciarRelatorioNaoConformidades',
		'/auditoria/relatorios/?action=relatorio-nao-conformidades-visualizar' 	        =>'\App\Controllers\Auditoria::visualizarRelatorioNaoConformidades',
		'/auditoria/relatorios/?action=relatorio-nao-conformidades-editar'		        => '\App\Controllers\Auditoria::formRelatorioNaoConformidadesEditar',
		'/auditoria/relatorios/?action=excel-relatorio-faturas-liberadas'  	 	        => '\App\Controllers\Auditoria::excelRelatorioFaturasLiberadas',
		'/auditoria/relatorios/?action=excel-relatorio-pedidos-liberados'  	 	        => '\App\Controllers\Auditoria::excelRelatorioPedidosLiberados',
		'/auditoria/relatorios/?action=excel-relatorio-faturamento-enviado' 	        => '\App\Controllers\Auditoria::excelRelatorioFaturamentoEnviado',
		'/auditoria/relatorios/?action=relatorio-nao-conformidades-usar-para-novo' 	    => '\App\Controllers\Auditoria::usarParaNovoRelatorioNaoConformidades',
	],
	"POST" => [
		'/auditoria/relatorios/?action=form-relatorio-faturas-liberadas'   	 		    => '\App\Controllers\Auditoria::relatorioFaturasLiberadas',
		'/auditoria/relatorios/?action=form-relatorio-pedidos-liberados'    		    => '\App\Controllers\Auditoria::relatorioPedidosLiberados',
		'/auditoria/relatorios/?action=form-relatorio-faturamento-enviado' 	 		    => '\App\Controllers\Auditoria::relatorioFaturamentoEnviado',
        '/auditoria/relatorios/?action=form-relatorio-saidas'			                => '\App\Controllers\Auditoria::relatorioSaidas',
		'/auditoria/relatorios/?action=salvar-relatorio-nao-conformidades' 			    => '\App\Controllers\Auditoria::salvarRelatorioNaoConformidades',
		'/auditoria/relatorios/?action=relatorio-nao-conformidades-gerenciar'		    => '\App\Controllers\Auditoria::gerenciarRelatorioNaoConformidades',
		'/auditoria/relatorios/?action=update-relatorio-nao-conformidades'			    => '\App\Controllers\Auditoria::updateRelatorioNaoConformidades',
		'/auditoria/relatorios/?action=relatorio-nao-conformidades-cancelar' 		    => '\App\Controllers\Auditoria::cancelarRelatorioNaoConformidades',
		'/auditoria/relatorios/?action=relatorio-nao-conformidades-excel'      		    => '\App\Controllers\Auditoria::excelRelatorioNaoConformidades',
		'/auditoria/relatorios/?action=busca-condensada-relatorio-nao-conformidades'    => '\App\Controllers\Auditoria::getCondensadoRelatorioNaoConformidades',
		'/auditoria/relatorios/?action=relatorio-nao-conformidades-excel-condensado'    => '\App\Controllers\Auditoria::excelCondesnadoRelatorioNaoConformidades',
		'/auditoria/relatorios/?action=relatorio-nao-conformidades-enviar-email'        =>  '\App\Controllers\Auditoria::enviarEmailRelatorioNaoConformidades',
		'/auditoria/relatorios/?action=reabrir-relatorio-nao-conformidades'             =>  '\App\Controllers\Auditoria::reabrirRelatorioNaoConformidades',
		'/auditoria/relatorios/?action=salvar-comentario-relatorio-nao-conformidades'   =>  '\App\Controllers\Auditoria::salvarComentarioRelatorioNaoConformidades',
		'/auditoria/relatorios/?action=remover-comentario-relatorio-nao-conformidades'  =>  '\App\Controllers\Auditoria::removerComentarioRelatorioNaoConformidades',

	]
];


$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}
