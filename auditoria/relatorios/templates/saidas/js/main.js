$(document).ready(function () {
    $('.chosen-select').chosen({search_contains: true});

    $(".tipo-busca-saida").change(function(){
        var opcao = $(this).val();
        var idItem = $(this).attr('id-item');
        if(opcao != 'paciente' && opcao != 'ur') {
            $(".combo").hide();
            $("#combo-item").show();
            $("#label-item").html($(this).attr('opcao') + ':');
            $("#pacientes, #urs").val('');
            $(".tipo-busca-saida[opcao='Paciente']").attr('checked', '');
            $.get(
                "/auditoria/relatorios/",
                {
                    action: "combo-busca-itens",
                    tipo: opcao,
                    idItem: idItem
                },
                function (result) {
                    $("#item").html(result);
                    $('.chosen-select').chosen('destroy');
                    $('.chosen-select').chosen({search_contains: true});
                }
            );
        } else if(opcao == 'paciente') {
            $(".combo").hide();
            $("#combo-paciente").show();
            $('.chosen-select').chosen('destroy');
            $('.chosen-select').chosen({search_contains: true});
        } else if(opcao == 'ur') {
            $(".tipo-busca-saida[opcao='Paciente']").attr('checked', '');
            $(".combo").hide();
            $("#combo-ur").show();
            $('.chosen-select').chosen('destroy');
            $('.chosen-select').chosen({search_contains: true});
        }
    });

    $(".combo").hide();
    $("#combo-paciente").show();

    $("#buscar").live('click', function () {
        var opcao = $(".tipo-busca-saida:checked").val();
        var valid = true;
        console.log(opcao);
        if(opcao == 'paciente' && $("#pacientes").val() == ''){
            $("#paciente-vazio").show();
            valid = !valid;
        } else if(opcao == 'ur' && $("#urs").val() == '') {
            $("#ur-vazio").show();
            valid = !valid;
        } else if((opcao != 'paciente' && opcao != 'ur') && $("#item").val() == ''){
            $("#item-vazio").show();
            valid = !valid;
        }
        return valid;
    });
});