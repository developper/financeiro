$(function($){
    $('.profissionais').chosen({search_contains: true});
    $('#pacientes').chosen({search_contains: true});

    $('#urs').chosen();

    $( "#periodo_inicio, #periodo_fim" ).datepicker();

    $('#pacientes').change(function(){
        var paciente = $(this).val();
        $('#paciente').val(paciente);
    });

    $('#urs').change(function(){
        var empresa = $(this).val();
        $('#empresa').val(empresa);
    });

    $('#periodo_inicio').blur(function(){
        var inicio = $(this).val();
        $('#inicio').val(inicio);
    });
    $('#periodo_inicio').change(function(){
        var inicio = $(this).val();
        $('#inicio').val(inicio);
    });

    $('#periodo_fim').blur(function(){
        var fim = $(this).val();
        $('#fim').val(fim);
    });
    $('#periodo_fim').change(function(){
        var fim = $(this).val();
        $('#fim').val(fim);
    });

    $('.adicionar').click(function(){
        var vm = $(this);
        var tipo = vm.attr('tipo');

        if($('input[name = topico-'+tipo+']').is(':checked')){

            if($('input[name = topico-'+tipo+']:checked').attr('obg') == 1  ){
                if($('#justificativa-'+tipo).val() == ''){
                    alert('Digite uma justificativa antes de adicionar.');
                    return false;
                }

            }
            var topicoLabel         =    $('input[name = topico-'+tipo+']:checked').attr('topico-label');
            var itemLabel           =    $('input[name = topico-'+tipo+']:checked').attr('justificativa-label');
            var justificativa       =    $('#justificativa-'+tipo).val();
            var profissionais       =    $('#profissionais-'+tipo).val();
            var topico              =    $('input[name = topico-'+tipo+']:checked').attr('topico');
            var item                =    $('input[name = topico-'+tipo+']:checked').val();
            var auxInput            =    parseInt($('#aux-input').val())+1;
            $('#aux-input').val(auxInput);
            var inputTopico         = "<input type='hidden' name = 'item["+auxInput+"][topico]' value = '"+topico+"'>";
            var inputItem           = "<input type='hidden' name = 'item["+auxInput+"][item]' value = '"+item+"'>";
            var inputJustificativa  = "<textarea name = 'item["+auxInput+"][justificativa]'>"+justificativa+"</textarea>";
            var inputRelatorioItemId = "<input type='hidden' name = 'item["+auxInput+"][id]' value = 0>";
            var inputCancelar = "<input type='hidden' name = 'item["+auxInput+"][remover]' value ='N' >";
            var inputIdReferencia = "<input type='hidden' name = 'item["+auxInput+"][idReferencia]' value ='' >";
            var inputProfissionais = "<input type='hidden' name = 'item["+auxInput+"][profissionais]' value ='" + profissionais + "' >";
            var profissionais_text = $('#profissionais-'+tipo+' :selected').map(function(){ return this.text }).get().join("<br>");

            var msg = "<tr class='item-adicionado' tipo='normal' aux='"+auxInput+"'>" +
                "<td>"+
                "<img src='/utils/delete_24x24.png' width='16' style='cursor: pointer;' title='Remover' border='0' class='remover'>" +
                inputTopico+
                inputItem+
                inputIdReferencia+
                inputCancelar+
                inputRelatorioItemId+
                inputProfissionais+
                "<b>"+topicoLabel+":</b> " + itemLabel+ ".<br> <b> Justificativa:</b> " + inputJustificativa+
                "</td>" +
                "<td>" +
                profissionais_text +
                "</td>" +
                "<td>"+
                "<select name='item["+auxInput+"][status]' class='status'><option value='NOVO'>NOVO</option></select>"+
                "</td>"+
                "</tr>";
            $('#tab-relatorio-nao-conformidades').append(msg);
            $('input[name = topico-'+tipo+']:checked').attr('checked',false);
            $('#justificativa-'+tipo).val('');
            $('#profissionais-'+tipo).val('');
            $(".chosen-select").trigger('chosen:updated');
            return false;
        }
        alert('Escolha um item antes de clicar em adicionar.');
        return false;
    });
    var $tabs = $("#relatorio-nao-conformidade").tabs();

    $(".next-tab,.prev-tab").click(function () {
        $tabs.tabs('select', parseInt($(this).attr('rel')));
        return false;
    });

    $('.esconder').click(function(){
        if(confirm('Deseja realmente remover o item?')){
            $(this).parent().parent().attr('tipo','remover');
            var aux = $(this).parent().parent().attr('aux');

            $("input[name= 'item["+aux+"][remover]']").val('S');
            $(this).parent().parent().attr('style','display:none');
        }

    });

    $('.remover').live('click',function(){
        if(confirm('Deseja realmente remover o item?')) {

            $(this).parent().parent().remove();
        }
    });

    $("#dialog-aviso").dialog({
        autoOpen: false,
        modal: true,
        width: 500,
        buttons: {
            "Voltar": function () {
                $(this).dialog("close");
            }
        }
    });


    $('#salvar,#finalizar').click(function(){

        var acao = $(this).attr('acao');
        $("#acao").val(acao);
        var url = $(this).attr('url');
        var ur = $('#empresa').val();
        var paciente = $('#paciente').val();
        var inicio = $('#periodo_inicio').val();
        var fim = $('#periodo_fim').val();
        var qtdItemAdicionado = 0;
        var dados = $('form').serialize();
        var topicos = [];

        $('.topicos').each(function(){
            topicos[$(this).attr('attrId')] = $(this).attr('attrLabel');
        });

        var topicosAdiconados = [];
        $('.item-adicionado').each(function(){
            var aux = $(this).attr('aux');
            var tipo = $(this).attr('tipo');
            var status = $(this).find("select[name='item["+aux+"][status]']").val();
            if(tipo == 'normal' && status != 'RESOLVIDO'){
                var id = $(this).find("input[name='item["+aux+"][topico]'").val();
                topicosAdiconados[id] = id;
                qtdItemAdicionado++;
            }
        });

        var topicosNaoAdicionados = [];

        topicos.filter(
            function(topico, key) {
                if (topicosAdiconados.indexOf(String(key)) == -1) {
                    topicosNaoAdicionados.push(topico);
                }
            }
        );

        if(acao == 'finalizar'){
            /*
           // Obrigatoriedade de inserir os campos de todas as areas.
            if(topicosNaoAdicionados.length > 0){
                 $("#dialog-aviso").dialog("open");
                 var string = '';
                 string = topicosNaoAdicionados.join().split(',').join('<br>');
                 var html =" É necessário adicionar um item de cada tópico."+
                     " <br> <b>Tópico(s) sem itens adicionado(s):</b>"+
                     "<br>"+string;

                 $("#dialog-aviso").html(html);
                 return false;
             }
             */
            var auxStatus = 0;
            $('.status').each(function(){
                if($(this).val() == ''){
                    $(this).css({"border":"3px solid red"});
                    auxStatus++;
                }else{
                    $(this).css({"border":""});
                }
            });
            if(auxStatus >0){
                var html ="O campo em vermelho é obrigatório";
                if(auxStatus > 1){
                    var html ="Os campos em vermelho são obrigatórios.";
                }
                $("#dialog-aviso").dialog("open");

                $("#dialog-aviso").html(html);
                return false;
            }
        }
        if(qtdItemAdicionado == 0){
            $("#dialog-aviso").dialog("open");

            var html ="Adicione pelo menos um item";
            $("#dialog-aviso").html(html);
            return false;
        }


        if(ur == '' || paciente == '' || inicio == '' || fim == ''){
            $("#dialog-aviso").dialog("open");
            html = "Os campos Paciente, UR e Período são obrigatorios.";
            $("#dialog-aviso").html(html);
            return false;

        }

        $.post( url,
            dados,

            function(r){
                if(isNumber(r)){
                    if(acao == 'finalizar'){
                        window.location = "?action=relatorio-nao-conformidades-visualizar&id="+r;
                    }else{
                        window.location = "?action=relatorio-nao-conformidades-editar&id="+r;
                    }
                    return;

                }else{
                    alert(r);
                    return false;
                }
            });
    });

    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    $('.add-comentario').click(function(){
        $("#dialog-add-comentario").attr('id-item',$(this).attr('id-item'));
        $("#dialog-add-comentario").dialog('open');
    });
    $("#dialog-add-comentario").dialog({
        autoOpen: false, modal: true, width: 600, position: 'top',
        open: function(event,ui){

        },
        buttons: {
            "Fechar" : function(){
                $("#dialog-add-comentario").attr('id-item','');
                $(this).dialog("close");
            },
            "Salvar" : function(){

                var comentario = $('#texto-comentario').val();
                var itemId = $(this).attr('id-item');

                if(comentario == ''){
                    $('#texto-comentario').css({"border":"3px solid red"});
                    return false;
                } else {
                    $('#texto-comentario').css({"border":""});
                }

                $.post( '/auditoria/relatorios/?action=salvar-comentario-relatorio-nao-conformidades',
                    {comentario:comentario, itemId: itemId},

                    function(r){
                        console.log(r);




                        if(r != -1){
                            var obj = jQuery.parseJSON(r);
                            var excluir ="<img src='/utils/delete.png'  class='remover-comentario' " +
                                "id-comentario ='"+obj.comentario_id+"' title='Excluir Comentário' width='15px'>";

                            $('#comentarios_'+itemId).append(
                                "<p id='comentario-id-"+obj.comentario_id+"'><b>"+excluir+" "+obj.user+" em "+obj.data+"-</b> "+obj.comentario+"</p>"
                            );
                            $('#texto-comentario').val('');
                            $("#dialog-add-comentario").attr('id-item','');
                            $("#dialog-add-comentario").dialog('close');

                        }else{
                            $('#texto-comentario').val('');
                            $("#dialog-add-comentario").attr('id-item','');
                            $("#dialog-add-comentario").dialog('close');
                            alert('Erro ao inserir comentário');
                        }



                    });
            }

        }
    });

    $('.remover-comentario').live('click',function(){


        if(confirm('Deseja realmente excluir o comentário?')){
            var comentarioId = $(this).attr('id-comentario');

            $.post( '/auditoria/relatorios/?action=remover-comentario-relatorio-nao-conformidades',
                {comentarioId:comentarioId},

                function(r){
                    if(r != -1){

                        $("#comentario-id-"+comentarioId).remove();

                    }else{
                        alert('Erro ao remover comentário.');
                    }
                });

        }

    });

});