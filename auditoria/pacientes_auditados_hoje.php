<?php
$id = session_id();
if(empty($id))
  session_start();
  include_once('../validar.php');
include_once('../db/config.php');
include_once('../utils/codigos.php');

class PacientesAuditadosHoje {
    
    function queryPacientesAuditadosHoje($dataInical){
      
              $sql="SELECT
                    UPPER(clientes.nome) as nome1
                    FROM
                    solicitacoes
                    INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
                    WHERE
                    solicitacoes.data_auditado BETWEEN '{$dataInical}' AND now()
                    GROUP BY
                    solicitacoes.paciente
                    ORDER BY
                    clientes.nome ASC ";      
              $result=mysql_query($sql);
              $numeroLinhas= mysql_num_rows($result);
              
              if($numeroLinhas == 0 ){
                   return null;
                  
              }else{                 
                
                  while ($row = mysql_fetch_array($result)) {
                      $pacientes[]=$row['nome1'];                    
                      
                  }
                  
                  
              }
        
              return $pacientes;
        
    }
    
    function listarPacientesAuditadosHoje(){
        
        date_default_timezone_set('America/Sao_Paulo');
        $dataInical = date('Y-m-d 00:00:00');
        $hoje = date('d/m/Y ');
        $pacientes = $this->queryPacientesAuditadosHoje($dataInical);    
     
       
        if(empty($pacientes)){
         echo "<br/>
             <br/>
             <table width='100%' class='mytable'>
                  <thead>
                        <tr>
                          <th align='center'>
                              <b>Nenhum paciente foi auditado hoje.</b>
                          </th>
                        </tr>
                   </thead>
              </table>";
        }else{
            echo "<table width='100%' class='mytable'>"
            . "    <thead><tr><th colspan='3'> Pacientes auditados em {$hoje} </th></tr></thead>";
            for ($i = 0; $i <= count($pacientes); $i += 3) {
                printf('<tr><td>%s</td><td>%s</td><td>%s</td></tr>', $pacientes[$i], $pacientes[$i+1], $pacientes[$i+2]);
            }
            echo"</table></br>";
            //var_dump($listar);
        }
    }
    
    
}
