<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../db/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/validar.php';
require $_SERVER['DOCUMENT_ROOT'] . '/alertas.php';

use \App\Controllers\Exames;



define('URI', $_SERVER['REQUEST_URI']);
define('METHOD', $_SERVER['REQUEST_METHOD']);

$routes = [
	"GET" => [
		'/exames/?action=menu' => '\App\Controllers\Exames::menu',
		'/exames/?action=exames-novo' => '\App\Controllers\Exames::formExames',
		'/exames/?action=exames-consultar' => '\App\Controllers\Exames::formListarExames',
		'/exames/?action=teste' => '\App\Controllers\Exames::teste',
		'/exames/?action=buscar-cid-10' => '\App\Controllers\Exames::buscarCid10',

	],
	"POST" => [
        '/exames/?action=exames-salvar' => '\App\Controllers\Exames::salvarExames',
        '/exames/?action=exames-consultar' => '\App\Controllers\Exames::listarExames',
		'/exames/?action=exames-excluir' => '\App\Controllers\Exames::excluirExames',
		'/exames/?action=salvar-arquivo' => '\App\Controllers\Exames::salvarArquivo',
		'/exames/?action=excluir-arquivo' => '\App\Controllers\Exames::excluirArquivo'
    ]
];

$args = isset($_GET) && !empty($_GET) ? $_GET : null;

try {
	$app = new App\Application;
	$app->setRoutes($routes);
	$app->dispatch(METHOD, URI, $args);
} catch(App\BaseException $e) {
	echo $e->getMessage();
} catch(App\NotFoundException $e) {
	http_response_code(404);
	echo $e->getMessage();
}
