
$(function($) {

    var previewNode = document.querySelector("#template");
    previewNode.id = "";
    var previewTemplate = previewNode.parentNode.innerHTML;
    previewNode.parentNode.removeChild(previewNode);

    var idExame ='';
    var myDropzone =   new Dropzone(document.body, { // Make the whole body a dropzone
        url: "/exames/?action=salvar-arquivo", // Set the url
        thumbnailWidth: 80,
        thumbnailHeight: 80,
        parallelUploads: 20,
        previewTemplate: previewTemplate,
        autoQueue: false, // Make sure the files aren't queued until manually added
        previewsContainer: '', // Define the container to display the previews
        clickable: ".fileinput-button", // Define the element that should be used as click trigger to select files.
        autoDiscover : false,
        success: function(file,response) {
            var node,  _results;
            if (file.previewElement) {
                file.previewElement.classList.add("dz-success");
                file.previewElement.querySelector(".success").innerHTML = 'Arquivo adicionado com sucesso!!';
                return node;
            }
        }

    });

    $(".renomear").live('keyup',function(){

        var $this = $( this );
        $this.val( er_replace( /[^a-zA-Z 0-9 _ -]+/g,'', $this.val() ) );

    });

    function er_replace( pattern, replacement, subject ){
        return subject.replace( pattern, replacement );
    }

    $('.fileinput-button').click(function(){
        var divParent = $('#div-parent').html(previewNode);
        var objetoDivParent = divParent.get(0);


        idExame = $(this).data('idExame');
        myDropzone.previewsContainer = $('#previews-'+idExame).get(0);
        objetoDivParent.querySelector("#template");
        objetoDivParent.querySelector(".start").setAttribute('idexame',idExame);
        myDropzone.options.previewTemplate = objetoDivParent.innerHTML;
        divParent.children('div').remove();


    });

    myDropzone.on("addedfile", function(file) {
      // file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };
        file.previewElement.querySelector(".start").onclick = function() {
            myDropzone.enqueueFile(file);
           var novoNome = file.previewElement.querySelector(".renomear").value;
            file.previewElement.querySelector(".renomear").setAttribute('disabled','disabled');
            if(novoNome != ''){
                var nome = file.previewElement.querySelector(".name").innerHTML;
                nome = nome.split('.');
                novoNome = novoNome+'.'+nome[1];
                file.previewElement.querySelector(".name").innerHTML = novoNome;

            }


        };

    });

// Update the total progress bar
    myDropzone.on("totaluploadprogress", function(progress) {
        document.querySelector(".progress .progress-bar").style.width = progress + "%";
    });

    //myDropzone.on('sending', function(file, xhr, formData){
       // formData.append('userName', 'bob');
    //});

    myDropzone.on("sending", function(file,xhr, formData) {
        var idExame = file.previewElement.querySelector(".start").getAttribute('idexame');
        var novoNome = file.previewElement.querySelector(".renomear").value;
        formData.append('novo-nome', novoNome);
        formData.append('id-exame', idExame);
        // Show the total progress bar when upload starts
       document.querySelector(".progress").style.opacity = "1";
        // And disable the start button
        file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
    });

// Hide the total progress bar when nothing's uploading anymore
    myDropzone.on("queuecomplete", function(progress) {
        document.querySelector(".progress").style.opacity = "0";
    });

// Setup the buttons for all transfers
// The "add files" button doesn't need to be setup because the config
// `clickable` has already been specified.
//    document.querySelector("#actions .start").onclick = function() {
//        myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
//    };
//    document.querySelector("#actions .cancel").onclick = function() {
//        myDropzone.removeAllFiles(true);
//    };

    //$("#my-awesome-dropzone").dropzone({ url: '/exames/file-upload', uploadMultiple: true, addRemoveLinks: true, renameFilename: clearFilename });


   /* Dropzone.autoDiscover = false;

    var myDropzone = new Dropzone(element, {
        url: "/exames/?action=salvar-arquivo",
        autoProcessQueue: false,
    });

    function clearFilename (file) {

    };
    $('.submit-upload').click(function(){
        alert('ok');
        myDropzone.processQueue();
    });

    //$(".dropzone").dropzone({url: "/exames/?action=salvar-arquivo",autoProcessQueue:false,});


    $(".dropzone-file").dropzone({ url: "/exames/?action=salvar-arquivo",
        success: function(file, response){
            if(response == 1){
               alert('Arquivo adicionado com Sucesso!');
                window.location.reload();
            }
        },
        DictDefaultMessage: "Araste os arquivos aqui para fazer upload",
        DictFallbackMessage: "Seu navegador n�o suporta o upload de arquivos drag'n'drop.",
        DictFallbackText: "Por favor, use o formul�rio de reserva abaixo para fazer o upload de seus arquivos como nos velhos tempos.",
        DitFileTooBig: "O arquivo � muito grande ({{filesize}} MiB). Max filesize: {{maxFilesize}} MiB.",
        DictInvalidFileType: "Voc� n�o pode carregar arquivos desse tipo.",
        DictResponseError: "O servidor respondeu com {{statusCode}} c�digo.",
        DictCancelUpload: "Cancelar upload",
        DictCancelUploadConfirmation: "Tem certeza de que deseja cancelar este upload?",
        DictRemoveFile: "Remover arquivo",
        DictRemoveFileConfirmation: null,
        DictMaxFilesExceeded: "Voc� n�o pode carregar mais arquivos.",
        autoProcessQueue:true,
        addRemoveLinks: true,
        maxFilesize: 2, // MB
        addRemoveLinks: true,
        renameFilename: false,
        uploadMultiple: true});


    $('#renomear-arquivo').dialog("close");
    $("#renomear-arquivo").dialog({
        autoOpen: false, modal: true, height: 250, width: 200,
        open: function(event, ui) {

        },
        buttons: {
            salvar: function() {

                if($("#nome-arquivo").val() == ""){
                    $("#nome-arquivo").css({"border":"3px solid red"});
                    return false;
                } else {
                    $(this).parent().css({"border":""});
                }
                return;



            },
            Cancelar: function() {

                $(this).dialog("close");
            }
        }
    });

    $('.add-arquivo').click(function(){
        $('#dialog-cadastra-exame').dialog("open");
    });
*/
});
