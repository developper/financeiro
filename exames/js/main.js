$(function($) {
    $(".chosen-select").chosen({search_contains: true});

    $("#buscar-exame").click(function(){
        if(!validar_campos('validar-paciente')){
            console.log(-1);
            return false;

        }

    });

    $(".tipo").change(function () {
        var $this = $(this);

        if($this.val() == 'em') {
            $("#just-emergencial-box").show();
            $("#justificativa").attr('required', 'required');
        } else {
            $("#just-emergencial-box").hide();
            $("#justificativa").removeAttr('required');
        }
    });

    $(".coleta").change(function () {
        var $this = $(this);

        if($this.val() == 'E') {
            $('.exames').val(['4', '5']);
            $('.exames').trigger("chosen:updated");
        } else {
            $('.exames').val('');
            $('.exames').trigger("chosen:updated");
        }
    });

    $(".excluir").live('click',function(){
        var exame = $(this);

        $.post( '/exames/?action=exames-excluir',
            {
                id: exame.attr('id-exame')
            },
            function(r){
                if(r == 1){
                    exame.parent().parent().parent().parent().find('#tr-'+exame.attr('id-exame')).remove();
                    exame.parent().parent().parent().remove();
                    alert('Exame removido com sucesso!');
                }else{
                    alert('Erro ao excluir exame. Entre em contato com a equipe do TI');
                }
            }
        );
    });

    $(".excluir-arquivo").live('click',function(){
        var arquivo = $(this);

        $.post( '/exames/?action=excluir-arquivo',
            {
                id: arquivo.attr('id-arquivo')
            },
            function(r){
                if(r == 1){
                    arquivo.parent().remove();
                    alert('Arquivo removido com sucesso!');
                }else{
                    alert('Erro ao excluir arquivo. Entre em contato com a equipe do TI');
                }
            }
        );
    });

    $(".data").datepicker({
        inline: true,
        changeYear: true,
        changeMonth: true,
        yearRange: '1900:2100'
    });

    $("#pre-prescricao").dialog('close');

    $("#pre-prescricao").dialog({
        autoOpen: false,
        modal: true,
        width: 400,
        open: function (event, ui) {
            "Iniciar"
        },
        buttons: {
            "Fechar": function () {
                $(this).dialog("close");
            }
        }
    });

    $(".prescrever").click(function(){
        $("#span-nome-paciente").html($(this).attr('nomePaciente'));
        $("#pre-prescricao-paciente").val($(this).attr('idPaciente'));
        $("#pre-prescricao-idexame").val($(this).attr('idExame'));
        $("#pre-prescricao").dialog('open');

    });

    $("#iniciarprescricao").click(function () {
        if(!validacao_prescricao('pre-prescricao'))
            return false;
    });

    function validacao_prescricao(div) {
        ok = true;
        $("#" + div + " .OBG").each(function (i) {

            if(this.value == "") {

                ok = false;
                this.style.border = "3px solid red";
            } else {
                this.style.border = "";
            }
        });
        return ok;
    }

    $(".inicio-periodo").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        onClose: function (selectedDate) {
            $(".fim-periodo").datepicker(
                'option',
                'minDate',
                selectedDate
            )
        }
    });

    $(".fim-periodo").datepicker({
        minDate: $(".inicio-periodo").val(),
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1
    });

    $("#salvar-exame").click(function(){
        if(!validar_campos('div-criar-exame'))
        return false;
    });

    function validar_campos(div){
        var ok = true;
        $("#"+div+" .OBG").each(function (i){

            if(this.value == ""){
                ok = false;
                this.style.border = "3px solid red";
            } else {
                this.style.border = "";
            }

        });

        $("#"+div+" .COMBO_OBG option:selected").each(function (i){
            if(this.value == "-1"){
                ok = false;
                $(this).parent().css({"border":"3px solid red"});
            } else {
                $(this).parent().css({"border":""});
            }
        });

        $("#"+div+" .OBG_CHOSEN option:selected").each(function (i){

            if(this.value == "" || this.value == -1){
                ok = false;
                $(this).parent().parent().children('div').css({"border":"3px solid red"});
            } else {
                //console.log($(this).parent().parent().children('div').attr('style'));
                var estilo = $(this).parent().parent().children('div').attr('style');
                //console.log(estilo.replace("border: 3px solid red",''));
                $(this).parent().parent().children('div').removeAttr('style');
                $(this).parent().parent().children('div').attr('style',estilo.replace("border: 3px solid red",''));
            }
        });

        if(!ok){
            $("#"+div+" .aviso").text("Os campos em vermelho s�o obrigat�rios!");
            $("#"+div+" .div-aviso").show();
        } else {
            $("#"+div+" .aviso").text("");
            $("#"+div+" .div-aviso").hide();
        }
        return ok;
    }
});