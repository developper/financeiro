<?php

namespace app\Models\Medico;

use App\Models\AbstractModel,
    App\Models\DAOInterface;
use App\Models\Administracao\Paciente;

class Avaliacao extends AbstractModel implements DAOInterface
{

    public static function getAll()
    {
        return parent::get(self::getSQL(), 'assoc');
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : "";
        return "SELECT * FROM capmedica WHERE 1=1 {$id}";
    }

    public static function getByUR($filters)
    {
        $ur = $filters['empresa'];
        $inicio = implode('-',array_reverse(explode('/',$filters['inicio'])));
        $fim = implode('-',array_reverse(explode('/',$filters['fim'])));
        $condUr = " pac.empresa = '{$ur}'  AND";
        if($ur == 'T'){
            $condUr = " ";
        }
        $condPlano = '';
        if(!empty($filters['plano'])){
            $condPlano = " pac.convenio = '{$filters['plano']}'  AND";
        }


        $sql = <<<SQL
		SELECT
		cap.id as ID,
		DATE_FORMAT(cap.data,'%d/%m/%Y %H:%i:%s') as data_sistema,
     	DATE_FORMAT(cap.data,'%d/%m/%Y') as data_pt,
		usr.nome as nomeUsuario,
		pac.nome as paciente,
		cap.data as data_us,
		'Avaliação' as tipoDocumento,
		pac.idClientes as paciente_id

  	FROM capmedica as cap
  	LEFT OUTER JOIN comorbidades as comob ON cap.id = comob.capmed
  	LEFT OUTER JOIN problemasativos as prob ON cap.id = prob.capmed
  	LEFT OUTER JOIN usuarios as usr ON cap.usuario = usr.idUsuarios
  	LEFT OUTER JOIN clientes as pac ON cap.paciente = pac.idClientes

  	WHERE
  	$condUr
  	$condPlano
  	cap.data BETWEEN '{$inicio} 00:00:01' AND '{$fim} 23:59:59'
  	
  	GROUP BY
  	  ID
	ORDER BY
  data_us DESC, ID DESC
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getUltimoRelatorioPaciente($paciente)
    {
        $sql = <<<SQL
SELECT id FROM capmedica WHERE paciente = '{$paciente}' ORDER BY id DESC LIMIT 1
SQL;
        return curent(parent::get($sql, 'assoc'));

    }

    public static function getAvaliacoesNaoAutorizadas($filters)
    {
        $sql = <<<SQL
SELECT 
  capmedica.id AS id_origem
  'Avaliação' AS tipo,
  data AS data_origem,
  autorizacao.status_geral
FROM
  capmedica 
  LEFT JOIN autorizacao ON (
    capmedica.id = autorizacao.origem 
    AND autorizacao.tipo = 'relatorio'
    AND autorizacao.carater = 'avaliacao'
  )
WHERE
  data BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
  AND paciente = '{$filters['paciente']}' 
  AND 
SQL;

    }

    public static function getAllAvaliacoesAutorizacaoUr($filters)
    {
        $response = [];
        $compPaciente = $filters['paciente'] != 'T' ? " AND paciente = '{$filters['paciente']}'" : "";
        $compPlano = $filters['plano'] != 'T' ? " AND clientes.convenio = '{$filters['plano']}'" : "";

        $ur_usuario = $_SESSION['empresa_principal'];
        if($ur_usuario != '1') {
            $compUr =  " AND clientes.empresa IN {$_SESSION['empresa_user']}";
        } else {
            $compUr = $filters['ur'] != 'T' ? " AND clientes.empresa = '{$filters['ur']}'" : "";
        }

        $sql = <<<SQL
SELECT 
  *
FROM
  capmedica
  INNER JOIN clientes ON capmedica.paciente = clientes.idClientes {$compPlano}
WHERE
  data BETWEEN '{$filters['inicio']} 00:00:00' AND '{$filters['fim']} 23:59:59'
  {$compPaciente}
  {$compUr}
SQL;
        $avaliacoes = parent::get($sql, 'Assoc');

        foreach ($avaliacoes as $avaliacao) {

            $sql = <<<SQL
SELECT 
  cuidadosespeciais.id,
  cuidadosespeciais.cuidado,
  frequencia.frequencia,
  prescricoes.inicio,
  prescricoes.fim
FROM
  itens_prescricao
  INNER JOIN prescricoes ON itens_prescricao.idPrescricao = prescricoes.id
  INNER JOIN cuidadosespeciais ON itens_prescricao.CATALOGO_ID = cuidadosespeciais.id
  INNER JOIN frequencia ON itens_prescricao.frequencia = frequencia.id
WHERE 
  prescricoes.ID_CAPMEDICA = '{$avaliacao['id']}'
  AND prescricoes.DESATIVADA_POR != 0
  AND itens_prescricao.tipo = 2;
SQL;
            $dados_relatorio = parent::get($sql, 'Assoc');
            if(count($dados_relatorio) > 0) {
                $sql = <<<SQL
SELECT 
  clientes.codigo,
  clientes.dataInternacao AS dataAdmissao,
  clientes.NUM_MATRICULA_CONVENIO AS inscricao,
  clientes.nome AS paciente,
  CONCAT(
    clientes.END_INTERNADO, 
    '. ', 
    clientes.BAI_INTERNADO, 
    ' - ', 
    cidades.NOME, 
    ' - ', 
    cidades.UF,  
    '(REF: ', 
    clientes.REF_ENDERECO_INTERNADO, 
    ')'
  ) AS endereco,
  IF(clientes.TEL_LOCAL_INTERNADO != '', clientes.TEL_LOCAL_INTERNADO, clientes.TEL_RESPONSAVEL) AS contato,
  planosdesaude.nome AS convenio
FROM 
  clientes
  INNER JOIN planosdesaude ON clientes.convenio = planosdesaude.id
  LEFT JOIN cidades ON clientes.CIDADE_ID_INT = cidades.ID
WHERE
  idClientes = '{$avaliacao['paciente']}'
SQL;
                $inicio = new \DateTime($filters['inicio']);
                $fim = new \DateTime($filters['fim']);

                $modalidade = Paciente::getModalidadeByPacientesAtendidos($inicio, $fim, [$avaliacao['paciente']]);
                $dados_paciente = parent::get($sql, 'Assoc');
                $response[$avaliacao['id']] = $dados_paciente[0];
                $response[$avaliacao['id']]['relatorio'] = 'Avaliação';
                $response[$avaliacao['id']]['modalidade'] = isset($modalidade[0]['modalidade']) ? $modalidade[0]['modalidade'] : '-';

                $response[$avaliacao['id']]['aut_medico'] = '-';
                $response[$avaliacao['id']]['aut_enfermagem'] = '-';
                $response[$avaliacao['id']]['aut_tecnico'] = '-';
                $response[$avaliacao['id']]['aut_fisio_motora'] = '-';
                $response[$avaliacao['id']]['aut_fisio_resp'] = '-';
                $response[$avaliacao['id']]['aut_fono'] = '-';
                $response[$avaliacao['id']]['aut_nutricao'] = '-';
                $response[$avaliacao['id']]['aut_psicologo'] = '-';

                foreach ($dados_relatorio as $dados) {
                    $response[$avaliacao['id']]['inicio'] = $dados['inicio'];
                    $response[$avaliacao['id']]['fim'] = $dados['fim'];

                    if(in_array($dados['id'], [7, 8, 9])) {
                        $response[$avaliacao['id']]['aut_medico'] = $dados['cuidado'] . ' - ' . $dados['frequencia'] . ' ';
                    }

                    if(in_array($dados['id'], [6, 32, 33, 34])) {
                        $response[$avaliacao['id']]['aut_enfermagem'] = $dados['cuidado'] . ' - ' . $dados['frequencia'] . ' ';
                    }

                    if(in_array($dados['id'], [2, 4, 5, 28, 29, 30, 31, 40, 41, 42])) {
                        $response[$avaliacao['id']]['aut_tecnico'] = $dados['cuidado'] . ' - ' . $dados['frequencia'] . ' ';
                    }

                    if(in_array($dados['id'], [1, 22])) {
                        $response[$avaliacao['id']]['aut_fisio_motora'] = $dados['cuidado'] . ' - ' . $dados['frequencia'] . ' ';
                    }

                    if(in_array($dados['id'], [1, 23])) {
                        $response[$avaliacao['id']]['aut_fisio_resp'] = $dados['cuidado'] . ' - ' . $dados['frequencia'] . ' ';
                    }

                    if(in_array($dados['id'], [11, 26])) {
                        $response[$avaliacao['id']]['aut_fono'] = $dados['cuidado'] . ' - ' . $dados['frequencia'] . ' ';
                    }

                    if(in_array($dados['id'], [14, 25])) {
                        $response[$avaliacao['id']]['aut_nutricao'] = $dados['cuidado'] . ' - ' . $dados['frequencia'] . ' ';
                    }

                    if(in_array($dados['id'], [12, 27])) {
                        $response[$avaliacao['id']]['aut_psicologo'] = $dados['cuidado'] . ' - ' . $dados['frequencia'] . ' ';
                    }
                }
            }
        }

        return $response;
    }
}