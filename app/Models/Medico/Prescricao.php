<?php

namespace app\Models\Medico;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class Prescricao extends AbstractModel implements DAOInterface
{

	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : "";
		return "SELECT * FROM prescricoes WHERE 1=1 {$id}";
	}

	public static function getPrescricoesAtivas($paciente)
	{
        $sql = <<<SQL
SELECT 
  itens_prescricao.nome,
  itens_prescricao.obs,
  itens_prescricao.descricao AS nomeItem,
  itens_prescricao.CATALOGO_ID AS itemId,
  itens_prescricao.NUMERO_TISS AS tiss,
  prescricoes.id AS prescricao,
  prescricoes.inicio AS inicioPrescricao,
  prescricoes.fim AS fimPrescricao,
  CASE
    WHEN prescricoes.Carater = 2 THEN 'Periódica'
    WHEN prescricoes.Carater = 3 THEN 'Aditiva'
  END AS carater,
  itens_prescricao.id AS itemPrescricaoId,
  itens_prescricao.inicio AS inicioItemPrescricao,
  itens_prescricao.fim AS fimItemPrescricao,
  itens_prescricao.tipo
FROM
  prescricoes
  INNER JOIN itens_prescricao ON prescricoes.id = itens_prescricao.idPrescricao
WHERE
  prescricoes.paciente = '{$paciente}'
  AND prescricoes.DESATIVADA_POR = 0
  AND prescricoes.Carater IN (2, 3)
  AND itens_prescricao.tipo != '-1'
  AND (prescricoes.inicio >= DATE_FORMAT(NOW(),'%Y-%m-%d') OR prescricoes.fim >= DATE_FORMAT(NOW(),'%Y-%m-%d'))
ORDER BY 
  inicioPrescricao, itemPrescricaoId
SQL;

        return parent::get($sql, 'assoc');
    }

    public static function getItensSuspensosPrescricoesAtivas($paciente)
    {
        $sql = <<<SQL
SELECT 
  itens_prescricao.CATALOGO_ID AS itemId,
  itens_prescricao.tipo_item_origem AS tipo_item_origem,
  itens_prescricao.inicio,
  itens_prescricao.fim
FROM
  prescricoes
  INNER JOIN itens_prescricao ON prescricoes.id = itens_prescricao.idPrescricao
WHERE
  prescricoes.paciente = '{$paciente}'
  AND prescricoes.DESATIVADA_POR = 0
  AND prescricoes.Carater IN (3, 11)
  AND itens_prescricao.tipo = '-1'
  AND (prescricoes.inicio >= DATE_FORMAT(NOW(),'%Y-%m-%d') OR prescricoes.fim >= DATE_FORMAT(NOW(),'%Y-%m-%d'))
SQL;
        return parent::get($sql, 'assoc');
    }

    public function suspenderPrescricoesAtivas($data)
    {
        $itens = [];
        $paciente = $data['paciente'];

        foreach ($data['prescricao'] as $key => $prescricao) {
            $itens[$prescricao]['inicio'] = $data['inicio_prescricao'][$key];
            $itens[$prescricao]['fim'] = $data['fim_prescricao'][$key];

            foreach ($data['item_prescricao_id'][$prescricao] as $chave => $item) {
                $itens[$prescricao][$item]['nome']              = anti_injection($data['nome'][$prescricao][$chave], "literal");
                $itens[$prescricao][$item]['obs']               = anti_injection($data['obs'][$prescricao][$chave], "literal");
                $itens[$prescricao][$item]['itemId']            = $data['item_id'][$prescricao][$chave];
                $itens[$prescricao][$item]['tiss']              = $data['tiss'][$prescricao][$chave];
                $itens[$prescricao][$item]['nomeItem']          = anti_injection($data['nome_item'][$prescricao][$chave], "literal");
                $itens[$prescricao][$item]['inicio']            = $data['inicio_item'][$prescricao][$chave];
                $itens[$prescricao][$item]['fim']               = $data['fim_item'][$prescricao][$chave];
                $itens[$prescricao][$item]['tipo_item_origem']  = $data['tipo_item'][$prescricao][$chave];
            }
        }

        return $response = $this->createNewPrescricaoAditivaFromSuspensao($itens, $paciente);
    }

    private function createNewPrescricaoAditivaFromSuspensao($itens, $paciente)
    {
        mysql_query("BEGIN");
        foreach ($itens as $prescricao => $itensPrescricao) {
            $inicio = \DateTime::createFromFormat('d/m/Y', $itensPrescricao['inicio'])->format('Y-m-d');
            $fim = \DateTime::createFromFormat('d/m/Y', $itensPrescricao['fim'])->format('Y-m-d');
            unset($itensPrescricao['inicio'], $itensPrescricao['fim']);
            $sql = <<<SQL
INSERT INTO prescricoes (paciente, criador, data, lockp, inicio, fim, Carater, LIBERADO, PRESCRICAO_ORIGEM, STATUS) VALUES
('{$paciente}', '{$_SESSION['id_user']}', NOW(), NOW(), '{$inicio}', '{$fim}', 11, 'S', '{$prescricao}', 0)
SQL;
            $rs = mysql_query($sql);

            if($rs){
                $prescricaoId = mysql_insert_id();
                $sql = <<<SQL
INSERT INTO itens_prescricao (idPrescricao, tipo, NUMERO_TISS, CATALOGO_ID, nome, inicio, fim, obs, descricao, FATURADO,tipo_item_origem) VALUES
SQL;
                foreach ($itensPrescricao as $id => $item) {
                    $inicio = \DateTime::createFromFormat('d/m/Y', $item['inicio'])->format('Y-m-d');
                    $fim = \DateTime::createFromFormat('d/m/Y', $item['fim'])->format('Y-m-d');
                    $item['nomeItem'] = "SUSPENDER: " . $item['nomeItem'];
                    $sql .= <<<SQL
('{$prescricaoId}', '-1', '{$item['tiss']}', '{$item['itemId']}', '{$item['nome']}', '{$inicio}', '{$fim}', '{$item['obs']}', '{$item['nomeItem']}', 'N','{$item['tipo_item_origem']}'),
SQL;
                }

                $sql = substr_replace($sql, ';', -1, 1);
                $rs = mysql_query($sql);
                if(!$rs){
                    mysql_query("ROLLBACK");
                    throw new \Exception("Erro ao salvar o item da prescrição!");
                }
            } else {
                mysql_query("ROLLBACK");
                throw new \Exception("Erro ao salvar a prescrição aditiva!");
            }
        }
        mysql_query("COMMIT");
        return true;
    }

    public static function getByUR($filters, $carater = null)
    {
        $ur = $filters['empresa'];
        $inicio = implode('-',array_reverse(explode('/',$filters['inicio'])));
        $fim = implode('-',array_reverse(explode('/',$filters['fim'])));
        $conCarater = '';
        if(!empty($carater)){
            $conCarater = "AND Carater in ({$carater})";
        }

        $condUr = " p.empresa = '{$ur}'  AND";
        if($ur == 'T'){
            $condUr = " ";
        }
        $condPlano = '';
        if(!empty($filters['plano'])){
            $condPlano = " p.convenio = '{$filters['plano']}'  AND";
        }

        $sql = <<<SQL
SELECT

prescricoes.id AS ID,
DATE_FORMAT(prescricoes.data,'%d/%m/%Y %H:%i:%s') as data_sistema,
CONCAT(DATE_FORMAT(prescricoes.inicio,'%d/%m/%Y'), ' - ', DATE_FORMAT(prescricoes.fim,'%d/%m/%Y')) as data_pt,
u.nome as nomeUsuario,
p.nome as paciente,
prescricoes.inicio as data_us,
prescricoes.data as data_system,
'Prescrição' as tipoDocumento,
(CASE prescricoes.carater
  WHEN '1' THEN '<label style="color:red;"><b>Emergencial</b></label>'
  WHEN '2' THEN '<label style="color:green;"><b>Peri&oacute;dica</b></label>'
  WHEN '3' THEN concat('<label style="color:blue;"><b>Aditiva ', COALESCE(prescricoes.flag, ''),'</b></label>')
  WHEN '4' THEN '<label style="color:orange;"><b>Avalia&ccedil;&atilde;o</b></label>'
  WHEN '5' THEN '<label style="color:#B8860B;"><b>Peri&oacute;dica Nutri&ccedil;&atilde;o</b></label>'
  WHEN '6' THEN '<label style="color:#6959CD ;"><b>Aditiva Nutri&ccedil;&atilde;o</b></label>'
  WHEN '7' THEN '<label style="color:#9ACD32 ;"><b>Aditiva Avalia&ccedil;&atilde;o</b></label>'
  WHEN '11' THEN '<label ><b>Suspens&atilde;o </b></label>'
END) AS tipoPresc,
p.idClientes as paciente_id
FROM
  prescricoes INNER JOIN
  usuarios as u ON (prescricoes.criador = u.idUsuarios) INNER JOIN
  clientes as p ON (prescricoes.paciente = p.idClientes)

WHERE

   $condPlano
   $condUr
			(
			prescricoes.inicio BETWEEN '{$inicio}' AND'{$fim}' OR
			prescricoes.fim BETWEEN '{$inicio}' AND'{$fim}'
			)
  AND prescricoes.DESATIVADA_POR = 0
  {$conCarater}

ORDER BY
  data_us DESC, data_system DESC

SQL;
        return parent::get($sql, 'assoc');
    }

    public static function createPrescricaoAditivaLaboratorial($data)
    {
        mysql_query('BEGIN');
        $sql = <<<SQL
INSERT INTO prescricoes 
(
    id,
    paciente,
    criador,
    data,
    lockp,
    inicio,
    fim,
    solicitado,
    Carater,
    ID_CAPMEDICA,
    DESATIVADA_POR,
    DATA_DESATIVADA,
    LIBERADO,
    LIBERADO_POR,
    DATA_LIBERADO,
    PRESCRICAO_ORIGEM,
    STATUS,
    flag,
    exames_laboratoriais_paciente_id
)
 VALUES 
(
  NULL,
  '{$data['paciente']}',
  '{$data['criador']}',
  NOW(),
  NOW(),
  '{$data['inicio']}',
  '{$data['fim']}',
  '',
  '3',
  '0',
  '0',
  '',
  'S',
  '0',
  NOW(),
  '0',
  '0',
  'LABORATORIAL',
  '{$data['exameId']}'
)
SQL;
        $rs = mysql_query($sql);
        if($rs){
            $idPrescricao = mysql_insert_id();
            $obs = sprintf(': %s. Obs: %s', $data['tags'], $data['observacao']);
            $sql = <<<SQL
INSERT INTO itens_prescricao
(
    id,
    idPrescricao,
    tipo,
    NUMERO_TISS,
    CATALOGO_ID,
    nome,
    apresentacao,
    via,
    dose,
    umdose,
    frequencia,
    aprazamento,
    inicio,
    fim,
    obs,
    descricao,
    OBS_ENFERMAGEM,
    FATURADO,
    tipo_item_origem
) 
VALUES 
(
  NULL,
  '{$idPrescricao}',
  '2',
  '55',
  '55',
  'COLETA DE MATERIAL PARA EXAMES{$obs}',
  '',
  '0',
  '',
  '0',
  '1',
  '',
  '{$data['inicio']}',
  '{$data['fim']}',
  '{$obs}',
  'Cuidados Especiais: COLETA DE MATERIAL PARA EXAMES{$obs}',
  '',
  'N',
  '0'
), 
(
  NULL,
  '{$idPrescricao}',
  '2',
  '6',
  '6',
  'VISITA ENFERMEIRO ASSISTENTE',
  '',
  '0',
  '',
  '0',
  '1',
  '',
  '{$data['inicio']}',
  '{$data['fim']}',
  '{$data['observacao']}',
  'Cuidados Especiais: VISITA ENFERMEIRO ASSISTENTE',
  '',
  'N',
  '0'
);
SQL;
            $rs = mysql_query($sql);
            if($rs){
                return mysql_query('COMMIT');
            }
            mysql_query('ROLLBACK');
            return false;
        }
        mysql_query('ROLLBACK');
        return false;
    }
}