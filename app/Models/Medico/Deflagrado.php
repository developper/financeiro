<?php

namespace app\Models\Medico;

use App\Models\AbstractModel,
    App\Models\DAOInterface;

class Deflagrado extends AbstractModel implements DAOInterface
{
    public static function getAll()
    {
        return parent::get(self::getSQL(), 'assoc');
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : "";
        return "SELECT * FROM relatorio_deflagrado_med WHERE 1=1 {$id}";
    }

    public static function getByUR($filters)
    {
        $ur = $filters['empresa'];
        $inicio = implode('-',array_reverse(explode('/',$filters['inicio'])));
        $fim = implode('-',array_reverse(explode('/',$filters['fim'])));
        $condUr = " p.empresa = '{$ur}'  AND";
        if($ur == 'T'){
            $condUr = " ";
        }
        $condPlano = '';
        if(!empty($filters['plano'])){
            $condPlano = " p.convenio = '{$filters['plano']}'  AND";
        }

        $sql = <<<SQL

SELECT
r.id as ID,
DATE_FORMAT(r.data,'%d/%m/%Y %H:%i:%s') as data_sistema,
			CONCAT(DATE_FORMAT(r.inicio,'%d/%m/%Y'), ' - ', DATE_FORMAT(r.fim,'%d/%m/%Y')) as data_pt,
			u.nome as nomeUsuario,
			p.nome as paciente,
			r.inicio as data_us,
			r.data as data_system,
			'Deflagrado' as tipoDocumento,
			p.idClientes as paciente_id
FROM
			relatorio_deflagrado_med as r LEFT JOIN
			usuarios as u ON (r.usuario_id= u.idUsuarios) LEFT JOIN
			clientes as p ON (r.cliente_id = p.idClientes)
WHERE
		   $condPlano
		   $condUr
			(
			r.inicio BETWEEN '{$inicio}' AND'{$fim}' OR
			r.fim BETWEEN '{$inicio}' AND'{$fim}'
			)
ORDER BY
			data_us DESC, data_system DESC
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getUltimoRelatorioPacienteByPeriodo($data)
    {
        $sql = <<<SQL
SELECT 
  relatorio_deflagrado_med.id,
  '' AS modalidade,
  servicos_prestados AS quadro_clinico,
  DATE_FORMAT(data, '%Y-%m-%d') AS data_prorrogacao,
  usuarios.nome AS medico,
  usuarios.conselho_regional,
  usuarios.ASSINATURA AS assinatura,
  data AS created_at
FROM 
  relatorio_deflagrado_med
  INNER JOIN usuarios ON relatorio_deflagrado_med.USUARIO_ID = usuarios.idUsuarios 
WHERE 
  cliente_id = '{$data['paciente']}'
  AND (
    inicio BETWEEN '{$data['inicio']}' AND '{$data['fim']}'
    OR fim BETWEEN '{$data['inicio']}' AND '{$data['fim']}'
  ) 
ORDER BY 
  id DESC LIMIT 1
SQL;
        return current(parent::get($sql, 'assoc'));

    }
}