<?php

namespace app\Models\Medico;

use App\Models\AbstractModel,
    App\Models\DAOInterface;
use App\Models\Administracao\Paciente;

class Prorrogacao extends AbstractModel implements DAOInterface
{

    public static function getAll()
    {
        return parent::get(self::getSQL(), 'assoc');
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? 'AND ID = "' . $id . '"' : "";
        return "SELECT * FROM relatorio_prorrogacao_med WHERE 1=1 {$id}";
    }

    public static function getByUR($filters)
    {
        $ur = $filters['empresa'];
        $inicio = implode('-',array_reverse(explode('/',$filters['inicio'])));
        $fim = implode('-',array_reverse(explode('/',$filters['fim'])));
        $condUr = " p.empresa = '{$ur}'  AND";
        if($ur == 'T'){
            $condUr = " ";
        }
        $condPlano = '';
        if(!empty($filters['plano'])){
            $condPlano = " p.convenio = '{$filters['plano']}'  AND";
        }

        $sql = <<<SQL


  SELECT
r.ID,
DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as data_sistema,
CONCAT(DATE_FORMAT(r.INICIO,'%d/%m/%Y'), ' - ', DATE_FORMAT(r.FIM,'%d/%m/%Y')) as data_pt,
u.nome as nomeUsuario,
p.nome as paciente,
r.INICIO as data_us,
r.DATA as data_system,
'Prorrogação' as tipoDocumento,
p.idClientes as paciente_id


FROM
			relatorio_prorrogacao_med as r LEFT JOIN
			usuarios as u ON (r.USUARIO_ID = u.idUsuarios) LEFT JOIN
			clientes as p ON (r.PACIENTE_ID = p.idClientes)
WHERE
			$condUr
			$condPlano
			(
			r.INICIO BETWEEN '{$inicio}' AND'{$fim}' OR
			r.FIM BETWEEN '{$inicio}' AND'{$fim}'
			)
			AND r.STATUS = 's'
ORDER BY
			data_us DESC, data_system DESC

SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getUltimoRelatorioPaciente($paciente)
    {
        $sql = <<<SQL
SELECT ID FROM relatorio_prorrogacao_med WHERE paciente = '{$paciente}' ORDER BY id DESC LIMIT 1
SQL;
        return curent(parent::get($sql, 'assoc'));

    }

    public static function getAllProrrogacoesAutorizacaoUr($filters)
    {
        $response = [];
        $compPaciente = $filters['paciente'] != 'T' ? " AND PACIENTE_ID = '{$filters['paciente']}'" : "";
        $compPlano = $filters['plano'] != 'T' ? " AND clientes.convenio = '{$filters['plano']}'" : "";

        $ur_usuario = $_SESSION['empresa_principal'];
        if($ur_usuario != '1') {
            $compUr =  " AND clientes.empresa IN {$_SESSION['empresa_user']}";
        } else {
            $compUr = $filters['ur'] != 'T' ? " AND clientes.empresa = '{$filters['ur']}'" : "";
        }

        $sql = <<<SQL
SELECT 
  *,
  INICIO AS inicio,
  FIM AS fim
FROM
  relatorio_prorrogacao_med
  INNER JOIN clientes ON relatorio_prorrogacao_med.PACIENTE_ID = clientes.idClientes {$compPlano}
WHERE
  (
    INICIO BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
    OR FIM BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
  )
  AND 
  relatorio_prorrogacao_med.STATUS = 's'
  {$compPaciente}
  {$compUr}
SQL;

        $prorrogacoes = parent::get($sql, 'Assoc');

        foreach ($prorrogacoes as $prorrogacao) {
            $sql = <<<SQL
SELECT 
  cuidadosespeciais.id,
  cuidadosespeciais.cuidado,
  frequencia.frequencia
FROM
  profissionais_relatorio_prorrogacao_med
  INNER JOIN cuidadosespeciais ON profissionais_relatorio_prorrogacao_med.CUIDADOSESPECIAIS_ID = cuidadosespeciais.id
  INNER JOIN frequencia ON profissionais_relatorio_prorrogacao_med.FREQUENCIA_ID = frequencia.id
WHERE 
  profissionais_relatorio_prorrogacao_med.RELATORIO_PRORROGACAO_MED_ID = '{$prorrogacao['ID']}'
SQL;
            $dados_relatorio = parent::get($sql, 'Assoc');
            if(count($dados_relatorio) > 0) {
                $sql = <<<SQL
SELECT 
  clientes.codigo,
  clientes.dataInternacao AS dataAdmissao,
  clientes.NUM_MATRICULA_CONVENIO AS inscricao,
  clientes.nome AS paciente,
  CONCAT(
    clientes.END_INTERNADO, 
    '. ', 
    clientes.BAI_INTERNADO, 
    ' - ', 
    cidades.NOME, 
    ' - ', 
    cidades.UF,  
    '(REF: ', 
    clientes.REF_ENDERECO_INTERNADO, 
    ')'
  ) AS endereco,
  IF(clientes.TEL_LOCAL_INTERNADO != '', clientes.TEL_LOCAL_INTERNADO, clientes.TEL_RESPONSAVEL) AS contato,
  planosdesaude.nome AS convenio
FROM 
  clientes
  INNER JOIN planosdesaude ON clientes.convenio = planosdesaude.id
  LEFT JOIN cidades ON clientes.CIDADE_ID_INT = cidades.ID
WHERE
  idClientes = '{$prorrogacao['PACIENTE_ID']}'
SQL;
                $inicio = new \DateTime($filters['inicio']);
                $fim = new \DateTime($filters['fim']);
                $modalidade = Paciente::getModalidadeByPacientesAtendidos($inicio, $fim, [$prorrogacao['PACIENTE_ID']]);
                $dados_paciente = parent::get($sql, 'Assoc');
                $response[$prorrogacao['ID']] = $dados_paciente[0];
                $response[$prorrogacao['ID']]['relatorio'] = 'Prorrogação';
                $response[$prorrogacao['ID']]['modalidade'] = isset($modalidade[0]['modalidade']) ? $modalidade[0]['modalidade'] : '-';

                $response[$prorrogacao['ID']]['inicio'] = $prorrogacao['INICIO'];
                $response[$prorrogacao['ID']]['fim'] = $prorrogacao['FIM'];

                $response[$prorrogacao['ID']]['aut_medico'] = '-';
                $response[$prorrogacao['ID']]['aut_enfermagem'] = '-';
                $response[$prorrogacao['ID']]['aut_tecnico'] = '-';
                $response[$prorrogacao['ID']]['aut_fisio_motora'] = '-';
                $response[$prorrogacao['ID']]['aut_fisio_resp'] = '-';
                $response[$prorrogacao['ID']]['aut_fono'] = '-';
                $response[$prorrogacao['ID']]['aut_nutricao'] = '-';
                $response[$prorrogacao['ID']]['aut_psicologo'] = '-';

                foreach ($dados_relatorio as $dados) {
                    if (in_array($dados['id'], [7, 8, 9])) {
                        $response[$prorrogacao['ID']]['aut_medico'] = $dados['cuidado'] . ' - ' . $dados['frequencia'] . ' ';
                    }

                    if (in_array($dados['id'], [6, 32, 33, 34])) {
                        $response[$prorrogacao['ID']]['aut_enfermagem'] = $dados['cuidado'] . ' - ' . $dados['frequencia'] . ' ';
                    }

                    if (in_array($dados['id'], [2, 4, 5, 28, 29, 30, 31, 40, 41, 42])) {
                        $response[$prorrogacao['ID']]['aut_tecnico'] = $dados['cuidado'] . ' - ' . $dados['frequencia'] . ' ';
                    }

                    if (in_array($dados['id'], [1, 22])) {
                        $response[$prorrogacao['ID']]['aut_fisio_motora'] = $dados['cuidado'] . ' - ' . $dados['frequencia'] . ' ';
                    }

                    if (in_array($dados['id'], [1, 23])) {
                        $response[$prorrogacao['ID']]['aut_fisio_resp'] = $dados['cuidado'] . ' - ' . $dados['frequencia'] . ' ';
                    }

                    if (in_array($dados['id'], [11, 26])) {
                        $response[$prorrogacao['ID']]['aut_fono'] = $dados['cuidado'] . ' - ' . $dados['frequencia'] . ' ';
                    }

                    if (in_array($dados['id'], [14, 25])) {
                        $response[$prorrogacao['ID']]['aut_nutricao'] = $dados['cuidado'] . ' - ' . $dados['frequencia'] . ' ';
                    }

                    if (in_array($dados['id'], [12, 27])) {
                        $response[$prorrogacao['ID']]['aut_psicologo'] = $dados['cuidado'] . ' - ' . $dados['frequencia'] . ' ';
                    }
                }
            }
        }

        return $response;
    }

    public static function getUltimoRelatorioPacienteByPeriodo($data)
    {
        $sql = <<<SQL
SELECT 
  ID AS id,
  MOD_HOME_CARE AS modalidade,
  quadro_clinico,
  DATE_FORMAT(DATA, '%Y-%m-%d') AS data_prorrogacao,
  usuarios.nome AS medico,
  usuarios.conselho_regional,
  usuarios.ASSINATURA AS assinatura,
  DATA as created_at
FROM 
  relatorio_prorrogacao_med
  INNER JOIN usuarios ON relatorio_prorrogacao_med.USUARIO_ID = usuarios.idUsuarios 
WHERE 
  PACIENTE_ID = '{$data['paciente']}'
  AND (
    INICIO BETWEEN '{$data['inicio']}' AND '{$data['fim']}'
    OR FIM BETWEEN '{$data['inicio']}' AND '{$data['fim']}'
  ) 
ORDER BY 
  ID DESC LIMIT 1
SQL;
        return current(parent::get($sql, 'assoc'));
    }
}
