<?php

namespace app\Models\Medico;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class Intercorrencia extends AbstractModel implements DAOInterface
{

	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND ID = "' . $id . '"' : "";
		return "SELECT * FROM intercorrencia_med WHERE 1 = 1 {$id}";
	}

	public static function getByUR($filters)
	{
		$ur = $filters['empresa'];
		$inicio = implode('-', array_reverse( explode('/', $filters['inicio'])));
		$fim = implode('-', array_reverse(explode('/', $filters['fim'])));
		$condUr = " p.empresa = '{$ur}'  AND";
		if($ur == 'T'){
			$condUr = " ";
		}
		$condPlano = '';
		if(!empty($filters['plano'])){
			$condPlano = " p.convenio = '{$filters['plano']}'  AND";
		}

		$sql = <<<SQL
SELECT
  r.id AS ID,
  DATE_FORMAT(r.data,'%d/%m/%Y %H:%i:%s') as data_sistema,
  CONCAT(DATE_FORMAT(r.data_encaminhamento,'%d/%m/%Y'), ' às ', DATE_FORMAT(r.hora,'%H:%i:%s')) as data_pt,
  u.nome as nomeUsuario,
  r.data_encaminhamento as data_us,
  r.data as data_system,
  p.nome as paciente,
  IF(r.vaga_uti = 'S', 'Intercorrência/Remoção', 'Intercorrência') as tipoDocumento,
  p.idClientes as paciente_id
FROM
  intercorrencia_med as r LEFT JOIN
  usuarios as u ON (r.usuario_id = u.idUsuarios) LEFT JOIN
  clientes as p ON (r.cliente_id = p.idClientes)
WHERE
$condPlano
$condUr
 r.data_encaminhamento BETWEEN '{$inicio}' AND '{$fim}'
ORDER BY
  data_us DESC, data_system DESC
SQL;
		return parent::get($sql, 'assoc');
	}

}