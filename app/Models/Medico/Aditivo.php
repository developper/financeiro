<?php

namespace app\Models\Medico;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class Aditivo extends AbstractModel implements DAOInterface
{

	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND ID = "' . $id . '"' : "";
		return "SELECT * FROM prescricoes WHERE 1=1 {$id} AND Carater = 3";
	}

	public static function getByUR($filters, $carater = null)
	{
		$ur = $filters['empresa'];
		$inicio = implode('-',array_reverse(explode('/',$filters['inicio'])));
		$fim = implode('-',array_reverse(explode('/',$filters['fim'])));
		$conCarater = '';
		if(!empty($carater)){
			$conCarater = "AND Carater in ({$carater})";
		}
		$condUr = " p.empresa = '{$ur}'  AND";
		if($ur == 'T'){
			$condUr = " ";
		}
		$condPlano = '';
		if(!empty($filters['plano'])){
			$condPlano = " p.convenio = '{$filters['plano']}'  AND";
		}

		$sql = <<<SQL
SELECT

prescricoes.id AS ID,
DATE_FORMAT(prescricoes.data,'%d/%m/%Y %H:%i:%s') as data_sistema,
CONCAT(DATE_FORMAT(prescricoes.inicio,'%d/%m/%Y'), ' - ', DATE_FORMAT(prescricoes.fim,'%d/%m/%Y')) as data_pt,
u.nome as nomeUsuario,
p.nome as paciente,
prescricoes.inicio as data_us,
prescricoes.data as data_system,
concat('<label style="color:blue;"><b>Aditivo ', COALESCE(prescricoes.flag, ''),'</b></label>') as tipoDocumento,
p.idClientes as paciente_id
FROM
  prescricoes INNER JOIN
  usuarios as u ON (prescricoes.criador = u.idUsuarios) INNER JOIN
  clientes as p ON (prescricoes.paciente = p.idClientes)

WHERE

   $condPlano
   $condUr
			(
			prescricoes.inicio BETWEEN '{$inicio}' AND'{$fim}' OR
			prescricoes.fim BETWEEN '{$inicio}' AND'{$fim}'
			)
  AND prescricoes.DESATIVADA_POR = 0
  AND Carater = 3

ORDER BY
  data_us DESC, data_system DESC

SQL;
		return parent::get($sql, 'assoc');
	}

    public static function getAllByPeriodo($begin, $end)
    {
        $sql = <<<SQL
SELECT
  prescricoes.id AS cod,
  prescricoes.paciente AS idPaciente,
  'presc-medico' AS tipo,
  prescricoes.data AS dataSistema,
  clientes.nome AS nomeOrder,
  CONCAT('(', clientes.codigo, ') ', clientes.nome) AS nomePaciente,
  usuarios.nome AS usuario,
  prescricoes.inicio,
  prescricoes.fim,
  IF(prescricoes.empresa = 0 OR prescricoes.empresa IS NULL, e2.nome, e1.nome) AS ur,
  IF(prescricoes.plano = 0 OR prescricoes.plano IS NULL, ps2.nome, ps1.nome) AS convenio
FROM
  prescricoes
  INNER JOIN clientes ON prescricoes.paciente = clientes.idClientes
  INNER JOIN usuarios ON prescricoes.criador = usuarios.idUsuarios
  LEFT JOIN empresas AS e1 ON prescricoes.empresa = e1.id
  LEFT JOIN planosdesaude AS ps1 ON prescricoes.plano = ps1.id
  LEFT JOIN empresas AS e2 ON clientes.empresa = e2.id
  LEFT JOIN planosdesaude AS ps2 ON clientes.convenio = ps2.id
WHERE
  (prescricoes.inicio BETWEEN '{$begin}' AND '{$end}'
  OR prescricoes.fim BETWEEN '{$begin}' AND '{$end}')
  AND prescricoes.Carater = 3
  AND clientes.status = 4
  AND prescricoes.solicitado IS NOT NULL 
  AND prescricoes.DESATIVADA_POR = 0
  AND paciente NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
ORDER BY 
  dataSistema, nomeOrder
SQL;
        return parent::get($sql, 'Assoc');
	}
}