<?php

namespace app\Models\Medico;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class Evolucao extends AbstractModel implements DAOInterface
{

	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : "";
		return "SELECT * FROM fichamedicaevolucao WHERE 1=1 {$id}";
	}

	public static function getRelatorioEvolucoes($filters)
	{
        $compUr = $filters['ur'] != '' ? " AND clientes.empresa = '{$filters['ur']}'" : "";

        $sql = <<<SQL
SELECT
	fichamedicaevolucao.ID,
	usuarios.nome AS medico,
	clientes.nome AS paciente,
	empresas.nome AS empresa,
	fichamedicaevolucao.DATA AS dataVisita,
	fichamedicaevolucao.DATA_SISTEMA as dataSistema
FROM
	fichamedicaevolucao
	INNER JOIN clientes ON fichamedicaevolucao.PACIENTE_ID = clientes.idClientes
	INNER JOIN empresas ON clientes.empresa = empresas.id
	INNER JOIN usuarios ON fichamedicaevolucao.USUARIO_ID = usuarios.idUsuarios
WHERE
	fichamedicaevolucao.DATA_SISTEMA BETWEEN '{$filters['inicio']} 00:00:01' AND '{$filters['fim']} 23:59:59'
	{$compUr}
ORDER BY
  paciente, dataSistema
SQL;
        return parent::get($sql, 'assoc');
    }

	public static function getByUR($filters)
	{
		$ur = $filters['empresa'];
		$inicio = implode('-',array_reverse(explode('/',$filters['inicio'])));
		$fim = implode('-',array_reverse(explode('/',$filters['fim'])));
		$condUr = " clientes.empresa = '{$ur}'  AND";
		if($ur == 'T'){
			$condUr = " ";
		}
		$condPlano = '';
		if(!empty($filters['plano'])){
			$condPlano = " clientes.convenio = '{$filters['plano']}'  AND";
		}


		$sql = <<<SQL
SELECT
	fichamedicaevolucao.ID,
	DATE_FORMAT(fichamedicaevolucao.DATA_SISTEMA,'%d/%m/%Y %H:%i:%s') as data_sistema,
     DATE_FORMAT(fichamedicaevolucao.DATA,'%d/%m/%Y à\s %H:%i:%s') as data_pt,
	usuarios.nome AS nomeUsuario,
	clientes.nome AS paciente,
	 fichamedicaevolucao.DATA as data_us,
	 fichamedicaevolucao.DATA_SISTEMA AS data_system,
	 'Evolução' as tipoDocumento,
	clientes.idClientes as paciente_id
FROM
	fichamedicaevolucao
	INNER JOIN clientes ON fichamedicaevolucao.PACIENTE_ID = clientes.idClientes
	INNER JOIN empresas ON clientes.empresa = empresas.id
	INNER JOIN usuarios ON fichamedicaevolucao.USUARIO_ID = usuarios.idUsuarios
WHERE
	$condPlano
	$condUr
	fichamedicaevolucao.DATA BETWEEN '{$inicio} 00:00:01' AND '{$fim} 23:59:59'
ORDER BY
  data_us DESC, data_system DESC
SQL;
		return parent::get($sql, 'assoc');
	}

    public static function getUltimoRelatorioPaciente($paciente)
    {
        $sql = <<<SQL
SELECT ID, USUARIO_ID FROM fichamedicaevolucao WHERE PACIENTE_ID = '{$paciente}' ORDER BY id DESC LIMIT 1
SQL;
        return current(parent::get($sql, 'assoc'));

    }

    public static function getProblemasAtivosEvolucao($evolucao)
    {
        $sql = <<<SQL
SELECT
  usuarios.idUsuarios AS idMedico,
  usuarios.nome AS medico,
  usuarios.conselho_regional,
  problemaativoevolucao.DESCRICAO as problemaAtivo,
  problemaativoevolucao.CID_ID,
  clientes.diagnostico,
  fichamedicaevolucao.*,
  DATE_FORMAT(fichamedicaevolucao.DATA, '%Y-%m-%d') AS data_evolucao
FROM
  fichamedicaevolucao
  INNER JOIN usuarios ON fichamedicaevolucao.USUARIO_ID = usuarios.idUsuarios
  LEFT JOIN problemaativoevolucao ON (problemaativoevolucao.EVOLUCAO_ID = fichamedicaevolucao.ID AND problemaativoevolucao.STATUS != 1)
  INNER JOIN clientes ON fichamedicaevolucao.PACIENTE_ID = clientes.idClientes
WHERE
  fichamedicaevolucao.ID = '{$evolucao}'
SQL;
        return parent::get($sql, 'Assoc');

    }
}
