<?php

namespace app\Models\Remocao;

//ini_set('display_errors',1);
//ini_set('display_startup_erros',1);
//error_reporting(E_ALL);


use App\Helpers\StringHelper;
use App\Models\AbstractModel;
use App\Models\DAOInterface;


class Remocao extends AbstractModel implements DAOInterface
{
    protected $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public static function getSQL($id = null)
    {
        // ...
    }

    public function salvarRemocaoConvenio($solicitante,
                                          $telefoneSolicitante,
                                          $telefonePaciente,
                                          $pacienteId,
                                          $matricula,
                                          $cpf,
                                          $nascimento,
                                          $convenio,
                                          $localOrigem,
                                          $enderecoOrigem,
                                          $enderecoDestino,
                                          $localDestino,
                                          $motivoRemocao,
                                          $tipoAmbulancia,
                                          $retorno,
                                          $nomePaciente,
                                          $dataSolicitacao)
    {
        session_start();
        $this->conn->query("BEGIN");
        $this->conn->autocommit(FALSE);
        $sql = <<<SQL
INSERT INTO
`remocoes`
(
id,
cod,
paciente_telefone,
paciente,
origem,
destino,
solicitante,
telefone_solicitante,
matricula,
data_nascimento,
cpf,
convenio_id,
paciente_id,
endereco_origem,
endereco_destino,
motivo_remocao,
tipo_ambulancia,
retorno,
data_solicitacao,
created_at,
created_by,
tipo_remocao
)
 VALUES (
 NULL,
 now(),
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 now(),
 ?,
 'C'
 );
SQL;
        if($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param (
                'sssssssssiissssssi',
                $telefonePaciente,
                $nomePaciente,
                $localOrigem,
                $localDestino,
                $solicitante,
                $telefoneSolicitante,
                $matricula,
                \DateTime::createFromFormat('d/m/Y', $nascimento)->format('Y-m-d'),
                $cpf,
                $convenio,
                $pacienteId,
                $enderecoOrigem,
                $enderecoDestino,
                $motivoRemocao,
                $tipoAmbulancia,
                $retorno,
                $dataSolicitacao,
                $_SESSION['id_user']
            );

            $rs = $stmt->execute();
        }else{
            return false;
        }
        if(!empty($rs)){
            return $this->conn->query("COMMIT");
        }else{
            $this->conn->query("ROLLBACK");
            return $this->conn->error;
        }
    }

    public static function getRemocaoByFilter($paciente, $tipoRemocao, $inicio, $fim)
    {
        $condicaoPaciente = '';
        $condicaoTipoRemocao = '';
        if(!empty($paciente)){
            $condicaoPaciente = "AND paciente_id = {$paciente}";

        }
        if(!empty($tipoRemocao)){
            $condicaoTipoRemocao = "AND tipo_remocao = '{$tipoRemocao}'";
        }

        $sql = <<<SQL
SELECT
remocoes.*,
planosdesaude.nome as nomeplano,
usuarios.nome as user,
clientes.nome as paciente
FROM
remocoes
INNER JOIN planosdesaude ON remocoes.convenio_id = planosdesaude.id
INNER JOIN usuarios ON remocoes.created_by = usuarios.idUsuarios
LEFT JOIN clientes ON remocoes.paciente_id = clientes.idClientes
WHERE
remocoes.created_at BETWEEN '{$inicio} 00:00:00' and '{$fim} 23:59:59'
AND remocoes.canceled_by IS NULL 
{$condicaoPaciente} 
{$condicaoTipoRemocao}
SQL;

        return parent::get($sql);
    }

    public static function getRemocaoById($id)
    {
        $sql = <<<SQL
SELECT
remocoes.*,
planosdesaude.nome as nomeplano,
usuarios.nome as user,
clientes.nome as paciente,
clientes.cpf as cpf,
clientes.nascimento as data_nascimento
FROM
remocoes
INNER JOIN planosdesaude ON remocoes.convenio_id = planosdesaude.id
INNER JOIN usuarios ON remocoes.created_by = usuarios.idUsuarios
LEFT JOIN clientes ON remocoes.paciente_id = clientes.idClientes
WHERE
remocoes.id = {$id};
SQL;

        return current(parent::get($sql, 'Assoc'));
    }

    public static function getItensRemocao($remocaoId)
    {
        $sql = <<<SQL
SELECT 
  CONCAT('(', remocoes_itens.tiss, ') ', catalogo.principio, ' ', catalogo.apresentacao) as item,
  remocoes_itens.tipo,
  remocoes_itens.qtd,
  remocoes_itens.catalogo_id,
  remocoes_itens.tiss

FROM
  remocoes_itens
  INNER JOIN catalogo ON remocoes_itens.catalogo_id = catalogo.ID
WHERE
  remocoes_itens.remocao_id = {$remocaoId} and remocoes_itens.tipo in (0,1,3)
  Union
  SELECT
  CONCAT(cobrancaplanos.item) as item,
  remocoes_itens.tipo,
  remocoes_itens.qtd,
  remocoes_itens.catalogo_id,
  remocoes_itens.tiss

FROM
  remocoes_itens
  INNER JOIN cobrancaplanos ON remocoes_itens.catalogo_id = cobrancaplanos.id
WHERE
  remocoes_itens.remocao_id = {$remocaoId} and remocoes_itens.tipo = 5
SQL;

        return parent::get($sql, 'Assoc');
    }

    public static function getTipoRemocaoFaturaPlano($idPlano)
    {
        $sql = <<<SQL
        SELECT
valorescobranca.id,
concat(valorescobranca.COD_COBRANCA_PLANO,' ',valorescobranca.DESC_COBRANCA_PLANO) as item,
valorescobranca.valor,
valorescobranca.COD_COBRANCA_PLANO,
valorescobranca.DESC_COBRANCA_PLANO
FROM
valorescobranca
INNER JOIN cobrancaplanos ON valorescobranca.idCobranca = cobrancaplanos.id
WHERE
valorescobranca.idPlano = {$idPlano}
AND
cobrancaplanos.remocao = 'S'
AND
valorescobranca.excluido_por is null
SQL;

        return parent::get($sql);

    }

    public static function getTipoRemocaoFatura()
    {
        $sql= <<<SQL
select
*
from
cobrancaplanos
WHERE
remocao = 'S';
SQL;
        return parent::get($sql);
    }
    public static function getItemCobrancaPlanoById($id)
    {
        $sql= <<<SQL
select
*
from
cobrancaplanos
WHERE
id = {$id} ;
SQL;
        return parent::get($sql);
    }
    public static function getItemValoresCobrancaById($id)
    {

        $sql= <<<SQL
select
*
from
valorescobranca
WHERE
id = {$id} ;
SQL;
        return parent::get($sql);
    }

    public function finalizarRemocaoConvenio($autorizacao,
                                             $remocao_realizada,
                                             $motivo,
                                             $km,
                                             $perimetro,
                                             $data_saida,
                                             $hora_saida,
                                             $data_chegada,
                                             $hora_chegada,
                                             $tabela_propria,
                                             $valor_remocao,
                                             $medico,
                                             $valor_medico,
                                             $tecnico,
                                             $valor_tecnico,
                                             $condutor,
                                             $valor_condutor,
                                             $remocao_fatura,
                                             $remocaoId,
                                             $outrasDespesas,
                                             $itens,
                                             $tabelaOrigemItemFatura,
                                             $empresa,
                                             $enfermeiro,
                                             $valor_enfermeiro,
                                             $remocaoSeraCobrada,
                                             $justificativaNaoCobrar)
    {
        session_start();
        $this->conn->query("BEGIN");
        $this->conn->autocommit(FALSE);
        if($tabelaOrigemItemFatura == 'cobrancaplanos'){
            $condColuna = 'cobrancaplano_id';
        }else{
            $condColuna = 'valorescobranca_id';

        }
        $modoCobranca = NULL;
        if($remocaoSeraCobrada == 'S' ){
            $modoCobranca = 'F';
        }


        $sql = <<<SQL
        UPDATE
        remocoes
        SET
          modo_cobranca = '{$modoCobranca}',
          autorizacao = ?,
          remocao_realizada = ?,
          motivo = ?,
          km = ?,
          perimetro = ?,
          data_saida = ?,
          hora_saida = ?,
          data_chegada = ?,
          hora_chegada = ?,
          tabela_propria = ?,
          valor_remocao = ?,
          medico = ?,
          valor_medico = ?,
          tecnico = ?,
          valor_tecnico = ?,
          condutor = ?,
          valor_condutor = ?,
          $condColuna = ?,
          finalized_at = now(),
          finalized_by = ?,
          outros_custos = ?,
          tabela_origem_item_fatura = ?,
          empresa_id = ?,
          enfermeiro = ?,
          valor_enfermeiro = ?,
          remocao_sera_cobrada = ?,
          justificativa_nao_cobrar = ?
        WHERE
          id = ?;
SQL;
        if($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param (
                'sssissssssdsdsdsdiidsissssi',
                $autorizacao,
                $remocao_realizada,
                $motivo,
                $km,
                $perimetro,
                $data_saida,
                $hora_saida,
                $data_chegada,
                $hora_chegada,
                $tabela_propria,
                StringHelper::moneyStringToFloat($valor_remocao),
                $medico,
                StringHelper::moneyStringToFloat($valor_medico),
                $tecnico,
                StringHelper::moneyStringToFloat($valor_tecnico),
                $condutor,
                StringHelper::moneyStringToFloat($valor_condutor),
                $remocao_fatura,
                $_SESSION['id_user'],
                StringHelper::moneyStringToFloat($outrasDespesas),
                $tabelaOrigemItemFatura,
                $empresa,
                $enfermeiro,
                $valor_enfermeiro,
                $remocaoSeraCobrada,
                $justificativaNaoCobrar,
                $remocaoId

            );

            $rs = $stmt->execute();
        }else{
            return false;
        }

        $rs = $this->saveHistoricoAcoes($remocaoId, 'finalizar');

        if(!empty($rs)){
            if(!empty($itens['catalogo_id'])) {
                $rs = $this->saveItensRemocao($itens, $remocaoId);
            }

            if($rs) {
                return $this->conn->query("COMMIT");
            }
        }else{
            $this->conn->query("ROLLBACK");
            return false;
        }
    }

    public  function updateRemocaoConvenioFinalizada($data)
    {
        session_start();
        $this->conn->query("BEGIN");
        $this->conn->autocommit(FALSE);
        if($data['tabelaOrigemItemFatura'] == 'cobrancaplanos'){
            $condColuna = 'cobrancaplano_id';
        }else{
            $condColuna = 'valorescobranca_id';
        }

       // var_dump($data['nascimento']);
        $dataNasc = $data['nascimento'];
        $valorRemocao = StringHelper::moneyStringToFloat($data['valor_remocao']);

        
        $valorMedico =StringHelper::moneyStringToFloat($data['valor_medico']);
        $valorTecnico =StringHelper::moneyStringToFloat($data['valor_tecnico']);
        $valorEnfermeiro =StringHelper::moneyStringToFloat($data['valor_enfermeiro']);
        $valorCondutor =StringHelper::moneyStringToFloat($data['valor_condutor']);
        $outrasDespesas = StringHelper::moneyStringToFloat($data['outrasDespesas']);

        $modoCobranca = NULL;
        if($data['remocaoSeraCobrada'] == 'S'){
            $modoCobranca = 'F';
        }
        $sql = <<<SQL
        UPDATE
        remocoes
        SET       
        modo_cobranca = '{$modoCobranca}', 
paciente_telefone =  '{$data['telefonePaciente']}',
paciente = '{$data['nomePaciente']}',
origem = '{$data['localOrigem']}',
destino = '{$data['localDestino']}',
solicitante = '{$data['solicitante']}',
telefone_solicitante = '{$data['telefoneSolicitante']}',
matricula = '{$data['matricula']}',
data_nascimento = '{$dataNasc}',
cpf = '{$data['cpf']}',
convenio_id = '{$data['convenio']}',
paciente_id = '{$data['pacienteId']}',
endereco_origem = '{$data['enderecoOrigem']}',
endereco_destino = '{$data['enderecoDestino']}',
motivo_remocao = '{$data['motivoRemocao']}',
tipo_ambulancia = '{$data['tipoAmbulancia']}',
retorno = '{$data['retorno']}',
data_solicitacao = '{$data['dataSolicitacao']}',
autorizacao = '{$data['autorizacao']}',
remocao_realizada = '{$data['remocao_realizada']}',
motivo = '{$data['motivo']}',
km = '{$data['km']}',
perimetro = '{$data['perimetro']}',
data_saida = '{$data['data_saida']}',
hora_saida = '{$data['hora_saida']}',
data_chegada = '{$data['data_chegada']}',
hora_chegada = '{$data['hora_chegada']}',
tabela_propria = '{$data['tabela_propria']}',
valor_remocao = '{$valorRemocao}',
medico = '{$data['medico']}',
valor_medico = '{$valorMedico}',
tecnico = '{$data['tecnico']}',
valor_tecnico = '{$valorTecnico}',
condutor = '{$data['condutor']}',
valor_condutor = '{$valorCondutor}',
$condColuna = '{$data['remocao_fatura']}',
outros_custos = '{$outrasDespesas}',
tabela_origem_item_fatura = '{$data['tabelaOrigemItemFatura']}',
empresa_id = '{$data['empresa']}',
enfermeiro = '{$data['enfermeiro']}',
valor_enfermeiro = '{$valorEnfermeiro}',
remocao_sera_cobrada = '{$data['remocaoSeraCobrada']}',
justificativa_nao_cobrar = '{$data['justificativaNaoCobrar']}',
edited_by = '{$_SESSION['id_user']}',
edited_at = now()
        WHERE
          id = '{$data['remocaoId']}';
SQL;

        if($stmt = $this->conn->prepare($sql)) {

            $rs = $stmt->execute();
        }else{
            return false;
        }
        $rs = $this->saveHistoricoAcoes($data['remocaoId'], 'editar');

        if(!empty($rs)){
            if(!empty($data['itens']['catalogo_id'])) {
                $rs = $this->updateItensRemocao($data['itens'], $data['remocaoId']);
            }
            if($rs) {
                return $this->conn->query("COMMIT");
            }
        }else{
            $this->conn->query("ROLLBACK");
            return false;
        }
    }
    public  function updateRemocaoConvenioNaoFinalizada($data)
    {
        session_start();
        $this->conn->query("BEGIN");
        $this->conn->autocommit(FALSE);



        $dataNasc = $data['nascimento'];

        $sql = <<<SQL
        UPDATE
        remocoes
        SET        
paciente_telefone =  '{$data['telefonePaciente']}',
paciente = '{$data['nomePaciente']}',
origem = '{$data['localOrigem']}',
destino = '{$data['localDestino']}',
solicitante = '{$data['solicitante']}',
telefone_solicitante = '{$data['telefoneSolicitante']}',
matricula = '{$data['matricula']}',
data_nascimento = '{$dataNasc}',
cpf = '{$data['cpf']}',
convenio_id = '{$data['convenio']}',
paciente_id = '{$data['pacienteId']}',
endereco_origem = '{$data['enderecoOrigem']}',
endereco_destino = '{$data['enderecoDestino']}',
motivo_remocao = '{$data['motivoRemocao']}',
tipo_ambulancia = '{$data['tipoAmbulancia']}',
retorno = '{$data['retorno']}',
data_solicitacao = '{$data['dataSolicitacao']}',
edited_by = '{$_SESSION['id_user']}',
edited_at = now()
        WHERE
          id = '{$data['remocaoId']}';
SQL;

        if($stmt = $this->conn->prepare($sql)) {

            $rs = $stmt->execute();
        }else{
            return false;
        }



        $rs = $this->saveHistoricoAcoes($data['remocaoId'], 'editar');

        if(!empty($rs)){

           return $this->conn->query("COMMIT");

        }else{
            $this->conn->query("ROLLBACK");
            return false;
        }
    }
    public  function excluirRemocao($id)
    {
        session_start();
        $response = [];
        $this->conn->query("BEGIN");
        $this->conn->autocommit(FALSE);
        $userId = $_SESSION['id_user'];

        $sql = <<<SQL
UPDATE
  remocoes
SET
  canceled_at = now(),
  canceled_by = ?
WHERE
  id = ?
SQL;
        $rs = false;
        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param(
                'ii',
                $userId,
                $id
            );
            $rs = $stmt->execute();
        }

        if(!empty($rs)){
            return $this->conn->query("COMMIT");
        }else{
            $this->conn->query("ROLLBACK");
            return $rs;
        }
    }

    public function salvarRemocaoParticular($solicitante,
                                            $telefoneSolicitante,
                                            $telefonePaciente,
                                            $paciente,
                                            $matricula,
                                            $convenio,
                                            $localOrigem,
                                            $enderecoOrigem,
                                            $enderecoDestino,
                                            $localDestino,
                                            $motivoRemocao,
                                            $tipoAmbulancia,
                                            $retorno,
                                            $dataSolicitacao,
                                            $nomePaciente

                                           )
    {
        session_start();
        $this->conn->query("BEGIN");
        $this->conn->autocommit(FALSE);
        $sql = <<<SQL
INSERT INTO
`remocoes`
(
id,
cod,
paciente_telefone,
paciente,
paciente_id,
origem,
destino,
solicitante,
telefone_solicitante,
matricula,
convenio_id,
endereco_origem,
endereco_destino,
motivo_remocao,
tipo_ambulancia,
retorno,
data_solicitacao,
created_at,
created_by,
tipo_remocao
)
 VALUES (
 NULL,
 now(),
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 now(),
 ?,
 'P'
 );
SQL;

        if($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param (
                'ssisssssissssssi',
                $telefonePaciente,
                $nomePaciente,
                $paciente,
                $localOrigem,
                $localDestino,
                $solicitante,
                $telefoneSolicitante,
                $matricula,
                $convenio,
                $enderecoOrigem,
                $enderecoDestino,
                $motivoRemocao,
                $tipoAmbulancia,
                $retorno,
                $dataSolicitacao,
                $_SESSION['id_user']
            );

            $rs = $stmt->execute();
        }else{
            return false;
        }
        if(!empty($rs)){
            $idRemocao = $this->conn->insert_id;
            $this->conn->query("COMMIT");
            return $idRemocao;
        }else{
            $this->conn->query("ROLLBACK");
            return $this->conn->error;
        }
    }

    public function finalizarRemocaoParticular($fornecedor_cliente,
                                               $km,
                                               $perimetro,
                                               $data_saida,
                                               $hora_saida,
                                               $data_chegada,
                                               $hora_chegada,
                                               $tabela_propria,
                                               $valor_remocao,
                                               $outros_custos,
                                               $medico,
                                               $valor_medico,
                                               $tecnico,
                                               $valor_tecnico,
                                               $condutor,
                                               $valor_condutor,
                                               $recebido,
                                               $forma_pagamento,
                                               $observacao,
                                               $remocaoId,
                                               $itens,
                                               $empresa,
                                               $enfermeiro,
                                               $valor_enfermeiro,
                                               $remocaoSeraCobrada,
                                                $modoCobranca,
                                                $justificativaNaoCobrar,
                                                $remocaoFatura,
                                                $tabelaOrigemItemFatura,
                                               $contaRecebido,
                                                $recebidoEm
                                            )
    {
        session_start();
        $this->conn->query("BEGIN");
        $this->conn->autocommit(FALSE);

        if($tabelaOrigemItemFatura == 'cobrancaplanos'){
            $condColuna = 'cobrancaplano_id';
        }else{
            $condColuna = 'valorescobranca_id';

        }

        $sql = <<<SQL
        UPDATE
        remocoes
        SET
          fornecedor_cliente = ?,
          km = ?,
          perimetro = ?,
          data_saida = ?,
          hora_saida = ?,
          data_chegada = ?,
          hora_chegada = ?,
          tabela_propria = ?,
          valor_remocao = ?,
          outros_custos = ?,
          medico = ?,
          valor_medico = ?,
          tecnico = ?,
          valor_tecnico = ?,
          condutor = ?,
          valor_condutor = ?,
          recebido = ?,
          forma_pagamento = ?,
          observacao = ?,
          finalized_at = now(),
          finalized_by = ?,
          empresa_id = ?,
          enfermeiro = ?,
          valor_enfermeiro = ?,
          remocao_sera_cobrada = ?,
          modo_cobranca = ?,
          justificativa_nao_cobrar = ?,
           $condColuna = ?,
           tabela_origem_item_fatura = ?,
           conta_recebido = ?,
           recebido_em = '{$recebidoEm}'          

        WHERE
          id = ?;
SQL;
        $valor_remocao = StringHelper::moneyStringToFloat($valor_remocao);
        $outros_custos = StringHelper::moneyStringToFloat($outros_custos);
        $valor_medico = StringHelper::moneyStringToFloat($valor_medico);
        $valor_tecnico = StringHelper::moneyStringToFloat($valor_tecnico);
        $valor_condutor = StringHelper::moneyStringToFloat($valor_condutor);
        if($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param (
                'iissssssddsdsdsdsssiissssssssi',
                $fornecedor_cliente,
                $km,
                $perimetro,
                $data_saida,
                $hora_saida,
                $data_chegada,
                $hora_chegada,
                $tabela_propria,
                $valor_remocao,
                $outros_custos,
                $medico,
                $valor_medico,
                $tecnico,
                $valor_tecnico,
                $condutor,
                $valor_condutor,
                $recebido,
                $forma_pagamento,
                $observacao,
                $_SESSION['id_user'],
                $empresa,
                $enfermeiro,
                $valor_enfermeiro,
                $remocaoSeraCobrada,
                $modoCobranca,
                $justificativaNaoCobrar,
                $remocaoFatura,
                $tabelaOrigemItemFatura,
                $contaRecebido,
                $remocaoId
            );

            $rs = $stmt->execute();
        }else{
            return false;
        }
        if(!empty($rs)){
            if(!empty($itens['catalogo_id'])) {
                $rs = $this->saveItensRemocao($itens, $remocaoId);
            }

            if($rs) {
                return $this->conn->query("COMMIT");
            }
        }else{
            $this->conn->query("ROLLBACK");
            return false;
        }
    }

    private function saveItensRemocao($itens, $remocaoId)
    {
        $rs = false;
        $sql = <<<SQL
INSERT INTO
`remocoes_itens`
(
id,
remocao_id,
catalogo_id,
tipo,
tiss,
qtd
)
 VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?
 );
SQL;
        foreach ($itens['catalogo_id'] as $key => $catalogoId) {
            $tipo = $itens['tipo'][$key];
            $tiss = $itens['tiss'][$key];
            $qtd = $itens['qtd'][$key];

            if($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param (
                    'iiisi',
                    $remocaoId,
                    $catalogoId,
                    $tipo,
                    $tiss,
                    $qtd
                );

                $rs = $stmt->execute();
            }else{
                return false;
            }
        }
        return $rs;
    }

    private function updateItensRemocao($itens, $remocaoId)
    {
        $rs = false;
        $sqlDelete = <<<SQL
 delete from remocoes_itens WHERE remocao_id = ?;
SQL;
        if($stmt = $this->conn->prepare($sqlDelete)) {
            $stmt->bind_param (
                'i',
                $remocaoId
            );

            $rs = $stmt->execute();
        }else{
            return false;
        }

        if(!$rs){
            return $rs;
        }



        $sql = <<<SQL
INSERT INTO
`remocoes_itens`
(
id,
remocao_id,
catalogo_id,
tipo,
tiss,
qtd
)
 VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?
 );
SQL;
        foreach ($itens['catalogo_id'] as $key => $catalogoId) {
            $tipo = $itens['tipo'][$key];
            $tiss = $itens['tiss'][$key];
            $qtd = $itens['qtd'][$key];

            if($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param (
                    'iiisi',
                    $remocaoId,
                    $catalogoId,
                    $tipo,
                    $tiss,
                    $qtd
                );

                $rs = $stmt->execute();
            }else{
                return false;
            }
        }
        return $rs;
    }

    public static function getRemocoesRealizadas($inicio, $fim, $tipoRemocao = 'C', $filtrarPor = 'P', $exibirNaoCobrada,
                                                 $paciente,
                                                 $fornecedorCliente,
                                                 $empresa)
    {
        $compFiltro = $filtrarPor == 'P' ? " AND operadoras_planosdesaude.OPERADORAS_ID = '16'" : "operadoras_planosdesaude.OPERADORAS_ID != 16";
        if( $tipoRemocao == 'C' ){
            $condRemocao = " AND ( remocoes.tipo_remocao = '{$tipoRemocao}' OR remocoes.modo_cobranca = 'F')  ";
        }else{
            $condRemocao = " AND remocoes.tipo_remocao = '{$tipoRemocao}' AND ( remocoes.modo_cobranca = 'NF' OR remocoes.modo_cobranca is null)";
        }
        $condExibirNaoCobrada = '';

        if($exibirNaoCobrada == 'N' ){
            $condExibirNaoCobrada = "AND remocoes.remocao_sera_cobrada = 'S'";
        }

        $condPaciente = empty($paciente) ? '' : "AND remocoes.paciente_id = {$paciente}";
        $condFornecedorCliente = empty($fornecedorCliente) ? '' : "AND remocoes.fornecedor_cliente = {$fornecedorCliente}";
        $condEmpresa = empty($empresa) ? '' : "AND remocoes.empresa_id = {$empresa}";



        $sql = <<<SQL
SELECT
remocoes.*,
planosdesaude.nome as convenio,
fornecedores.FISICA_JURIDICA,
fornecedores.nome AS nomeCliente,
fornecedores.razaoSocial AS razaoSocial,
fornecedores.cnpj AS CNPJ,
fornecedores.CPF,
fornecedores.contato AS contatoCliente,
fornecedores.telefone AS telefoneCliente,
fornecedores.email AS emailCliente,
fornecedores.endereco AS enderecoCliente,
cidades.NOME AS cidade,
cidades.UF,
fornecedores.IES,
fornecedores.IM AS IMRG,
empresas.nome as unidade_regional,
clientes.nome as paciente,
clientes.cpf as cpf,
clientes.nascimento as data_nascimento
FROM
remocoes
INNER JOIN planosdesaude ON remocoes.convenio_id = planosdesaude.id
INNER JOIN usuarios ON remocoes.created_by = usuarios.idUsuarios
LEFT JOIN fornecedores ON remocoes.fornecedor_cliente = fornecedores.idFornecedores
LEFT JOIN cidades ON fornecedores.CIDADE_ID = cidades.ID
Left JOIN empresas on remocoes.empresa_id = empresas.id
LEFT JOIN clientes ON remocoes.paciente_id = clientes.idClientes
LEFT JOIN operadoras_planosdesaude ON operadoras_planosdesaude.PLANOSDESAUDE_ID = remocoes.convenio_id
WHERE
remocoes.data_saida BETWEEN '{$inicio} 00:00:00' AND '{$fim} 23:59:59'
AND remocoes.canceled_by IS NULL 
AND remocoes.finalized_at IS NOT NULL 
AND remocoes.finalized_by IS NOT NULL
$condExibirNaoCobrada
{$condRemocao}
{$compFiltro}
{$condPaciente}
{$condFornecedorCliente}
{$condEmpresa}
SQL;

        return parent::get($sql);
    }
   // utilizada apensa para fazer o rollback na funcionalidade de salvar remoções extrenas.
    public static function deleteRemocao($id){
        mysql_query("begin");
        $sql = "DELETE from remocoes where id = {$id}";
        if(!mysql_query($sql)){
            mysql_query("rollback");
            echo "Erro ao excluir remoção";
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));
        mysql_query("commit");
       return 1;

    }

    private function saveHistoricoAcoes($remocaoId, $acao){

        $sql = <<<SQL
INSERT 
INTO 
remocoes_historico_acoes

(
    
    `remocao_id` ,
    `usuario_id` ,
    `acao` ,
    `data`
 )
  VALUES 
(

 {$remocaoId},
 {$_SESSION['id_user']},
 '{$acao}',
 now()
);
SQL;
        if($stmt = $this->conn->prepare($sql)) {

            $rs = $stmt->execute();
        }else{
            return false;
        }

        return $rs;

    }



    public  function updateRemocaoParticularNaoFinalizada($data)
    {
        session_start();
        $this->conn->query("BEGIN");
        $this->conn->autocommit(FALSE);


        $dataNasc = $data['nascimento'];

        $sql = <<<SQL
        UPDATE
        remocoes
        SET        
paciente_telefone =  '{$data['telefonePaciente']}',
paciente = '{$data['nomePaciente']}',
origem = '{$data['localOrigem']}',
destino = '{$data['localDestino']}',
solicitante = '{$data['solicitante']}',
telefone_solicitante = '{$data['telefoneSolicitante']}',
matricula = '{$data['matricula']}',
data_nascimento = '{$dataNasc}',
cpf = '{$data['cpf']}',
convenio_id = '{$data['convenio']}',
paciente_id = '{$data['pacienteId']}',
endereco_origem = '{$data['enderecoOrigem']}',
endereco_destino = '{$data['enderecoDestino']}',
motivo_remocao = '{$data['motivoRemocao']}',
tipo_ambulancia = '{$data['tipoAmbulancia']}',
retorno = '{$data['retorno']}',
data_solicitacao = '{$data['dataSolicitacao']}',
edited_by = '{$_SESSION['id_user']}',
edited_at = now()
        WHERE
          id = '{$data['remocaoId']}';
SQL;

        if($stmt = $this->conn->prepare($sql)) {

            $rs = $stmt->execute();
        }else{
            return false;
        }



        $rs = $this->saveHistoricoAcoes($data['remocaoId'], 'editar');

        if(!empty($rs)){

            return $this->conn->query("COMMIT");

        }else{
            $this->conn->query("ROLLBACK");
            return false;
        }
    }

    public  function updateRemocaoParticularFinalizada($data)
    {
        session_start();
        $this->conn->query("BEGIN");
        $this->conn->autocommit(FALSE);



        $dataNasc = $data['nascimento'];
        if($data['tabelaOrigemItemFatura'] == 'cobrancaplanos'){
            $condColuna = 'cobrancaplano_id';
        }else{
            $condColuna = 'valorescobranca_id';
        }

        $valorRemocao = StringHelper::moneyStringToFloat($data['valor_remocao']);
        $valorMedico =StringHelper::moneyStringToFloat($data['valor_medico']);
        $valorTecnico =StringHelper::moneyStringToFloat($data['valor_tecnico']);
        $valorEnfermeiro =StringHelper::moneyStringToFloat($data['valor_enfermeiro']);
        $valorCondutor =StringHelper::moneyStringToFloat($data['valor_condutor']);
        $outrasDespesas = StringHelper::moneyStringToFloat($data['outrasDespesas']);

        $sql = <<<SQL
        UPDATE
        remocoes
        SET        
paciente_telefone =  '{$data['telefonePaciente']}',
paciente = '{$data['nomePaciente']}',
origem = '{$data['localOrigem']}',
destino = '{$data['localDestino']}',
solicitante = '{$data['solicitante']}',
telefone_solicitante = '{$data['telefoneSolicitante']}',
matricula = '{$data['matricula']}',
data_nascimento = '{$dataNasc}',
cpf = '{$data['cpf']}',
convenio_id = '{$data['convenio']}',
paciente_id = '{$data['pacienteId']}',
endereco_origem = '{$data['enderecoOrigem']}',
endereco_destino = '{$data['enderecoDestino']}',
motivo_remocao = '{$data['motivoRemocao']}',
tipo_ambulancia = '{$data['tipoAmbulancia']}',
retorno = '{$data['retorno']}',
data_solicitacao = '{$data['dataSolicitacao']}',
km = '{$data['km']}',
perimetro = '{$data['perimetro']}',
data_saida = '{$data['data_saida']}',
hora_saida = '{$data['hora_saida']}',
data_chegada = '{$data['data_chegada']}',
hora_chegada = '{$data['hora_chegada']}',
tabela_propria = '{$data['tabela_propria']}',
valor_remocao = '{$valorRemocao}',
medico = '{$data['medico']}',
valor_medico = '{$valorMedico}',
tecnico = '{$data['tecnico']}',
valor_tecnico = '{$valorTecnico}',
condutor = '{$data['condutor']}',
valor_condutor = '{$valorCondutor}',
$condColuna = '{$data['remocao_fatura']}',
outros_custos = '{$outrasDespesas}',
tabela_origem_item_fatura = '{$data['tabelaOrigemItemFatura']}',
empresa_id = '{$data['empresa']}',
enfermeiro = '{$data['enfermeiro']}',
valor_enfermeiro = '{$valorEnfermeiro}',
remocao_sera_cobrada = '{$data['remocaoSeraCobrada']}',
justificativa_nao_cobrar = '{$data['justificativaNaoCobrar']}',
recebido =  '{$data['recebido']}',
forma_pagamento = '{$data['forma_pagamento']}',
fornecedor_cliente = '{$data['fornecedor_cliente']}',
modo_cobranca = '{$data['modoCobranca']}',
edited_by = '{$_SESSION['id_user']}',
edited_at = now(),
conta_recebido = '{$data['conta_recebido']}',
recebido_em = '{$data['recebido_em']}',
observacao = '{$data['observacao']}'


        WHERE
          id = '{$data['remocaoId']}';
SQL;

        if($stmt = $this->conn->prepare($sql)) {

            $rs = $stmt->execute();
        }else{
            return false;
        }



        $rs = $this->saveHistoricoAcoes($data['remocaoId'], 'editar');

        if(!empty($rs)){

            return $this->conn->query("COMMIT");

        }else{
            $this->conn->query("ROLLBACK");
            return false;
        }
    }

}