<?php

namespace App\Models\DB;

class ConnMysqli extends \Mysqli
{

  /**
   * Recebe a instância do MySQLi Connect
   */
  private static $db;

  const INFOS = 'DB';
  const CHARSET = 'utf8';

  /**
   * Verifica se o arquivo de configurações possui os
   * dados para a conexão com o banco
   */
  private static function checkConfig()
  {
    Config::getConfig();
    if (!array_key_exists(self::INFOS, Config::$configs)) {
      throw new \mysqli_sql_exception('Dados de conexão com o banco de dados não encontrados!');
    }
  }

  /**
   * Verifica se o banco de dados já
   * foi instanciado. Se já tiver sido criado
   * retorna a conexão com o banco existente
   *
   * @return object
   */
  private static function getInstance()
  {
      return self::$db;
  }

  /**
   * Se o banco de dados ainda não tiver sido criado
   * cria uma nova conexão com o banco de dados.
   *
   * @return object
   */

  public static function getConnection()
  {
    self::checkConfig();

    if (!isset(self::$db) && !is_object(self::$db)) {
      self::$db = new \mysqli(
        Config::$configs[self::INFOS]['host'],
        Config::$configs[self::INFOS]['user'],
        Config::$configs[self::INFOS]['pass'],
        Config::$configs[self::INFOS]['dbname']
      );
      self::$db->set_charset(self::CHARSET);

      return self::$db;
    }

    return self::getInstance();
  }
}