<?php

namespace App\Models\DB;

use \App\Core\Config;

class ConnESUSPgsql
{
    private $config;
    private $db_config = [
        '1' => 'DB_ESUS_FSA',
        '2' => 'DB_ESUS_SSA',
        '3' => 'DB_ESUS_SULBA',
    ];
    public $dbc;

    public function __construct($filial) {
        $this->config = Config::get($this->db_config[$filial]);
        $this->getPDOConnection();
    }

    public function __destruct() {
        $this->dbc = NULL;
    }

    private function getPDOConnection() {
        if ($this->dbc == NULL) {
            $dsn = sprintf(
                "pgsql:host=%s;port=%s;dbname=%s",
                $this->config['host'],
                $this->config['port'],
                $this->config['dbname']
            );
            //$configs = [\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"];
            try {
                $this->dbc = new \PDO($dsn, $this->config['user'], $this->config['pass']);
                $this->dbc->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                $this->dbc->query("SET NAMES 'utf8'");
            } catch(\PDOException $e) {
                echo __LINE__ . $e->getMessage();
            }
        }
    }

    public function runQuery($sql) {
        try {
            $count = $this->dbc->exec($sql) or print_r($this->dbc->errorInfo());
        } catch(PDOException $e) {
            echo __LINE__.$e->getMessage();
        }
        return $count;
    }

    public function getQuery($sql) {
        $stmt = $this->dbc->query($sql);
        $stmt->setFetchMode(\PDO::FETCH_ASSOC);
        return $stmt;
    }
}
