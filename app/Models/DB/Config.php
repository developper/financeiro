<?php
/**
 * Created by PhpStorm.
 * User: thiago
 * Date: 16/01/2015
 * Time: 09:25
 */

namespace App\Models\DB;


class Config {
  public static $configs;

  public static function getConfig(){
    self::$configs = require __DIR__ . '/../../../../config.php';
  }
}