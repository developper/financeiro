<?php

namespace App\Models\Financeiro;

use App\Core\Log,
    \App\Models\AbstractModel,
    \App\Models\DAOInterface;


class IPCC extends AbstractModel implements DAOInterface
{

    public static function getAll()
    {
        return parent::get(self::getSQL());
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getByCodigo($match, $level)
    {
        $sql = "SELECT * FROM plano_contas_contabil WHERE COD_N{$level} = '{$match}' ORDER BY COD_N2, COD_N3, COD_N4, COD_N5";
        $rs = mysql_query($sql);
        $fetch = mysql_fetch_array($rs, MYSQL_ASSOC);

        return $fetch;
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? 'AND ID = "' . $id . '"' : "";
        return "SELECT * FROM plano_contas_contabil WHERE 1=1 {$id} ORDER BY COD_N2, COD_N3, COD_N4, COD_N5";
    }

    public static function save($data)
    {
        $data = array_map(function($dados){
            if(isset($dados['descricao']))
                $dados['descricao'] = strtoupper ($dados['descricao']);
            return $dados;
        }, $data);

        

        if(self::validateEntirePlan($data) == '0' && self::validateEntireCod($data) == '0') {
            
       
            mysql_query('BEGIN');
            $sql = "INSERT INTO plano_contas_contabil (CODIGO_DOMINIO, N1, COD_N2, N2, COD_N3, N3, COD_N4, N4, COD_N5, N5)
              VALUES ('{$data['nivel5']['codigo_dominio']}',
              '{$data['nivel1']['codigo']}',
               '{$data['nivel2']['codigo']}', 
               '{$data['nivel2']['descricao']}',
              trim('{$data['nivel3']['codigo']}'), 
              '{$data['nivel3']['descricao']}',
              trim('{$data['nivel4']['codigo']}'), 
              '{$data['nivel4']['descricao']}',
			    trim('{$data['nivel5']['codigo']}'), 
                '{$data['nivel5']['descricao']}')";
                
            $result = mysql_query($sql);
            

            if ($result) {
                
                Log::registerUserLog($sql);
                return mysql_query("COMMIT");
            } else {
                
                mysql_query("ROLLBACK");
                return mysql_error();
            }
        }else{
            return "Já existe um registro com esse código/descrição!";
        }
    }

    public static function update($data)
    {
        //Auxiliar para verificar se o registro a ser cadastrado
        //já existe no banco
        $occurs = 0;

        foreach ($data as &$ipcc)
            $ipcc = array_map(function($dados){
                if(isset($dados['descricao']))
                    $dados['descricao'] = strtoupper ($dados['descricao']);
                return $dados;
            }, $ipcc);

        switch($data['extras']['nivel']){
            case 5:
                $data['ipcc']['nivel5']['codigo'] =
                    isset($data['ipccAntigo']['nivel5']['codigo']) && $data['ipccAntigo']['nivel5']['codigo'] !== $data['ipcc']['nivel5']['codigo'] ?
                        $data['ipcc']['nivel5']['codigo'] :
                        $data['ipccAntigo']['nivel5']['codigo'];
                $data['ipcc']['nivel5']['descricao'] =
                    isset($data['ipccAntigo']['nivel5']['descricao']) && $data['ipccAntigo']['nivel5']['descricao'] !== $data['ipcc']['nivel5']['descricao'] ?
                        $data['ipcc']['nivel5']['descricao'] :
                        $data['ipccAntigo']['nivel5']['descricao'];
                $data['ipcc']['nivel5']['codigo_dominio'] =
                    isset($data['ipccAntigo']['nivel5']['codigo_dominio']) && $data['ipccAntigo']['nivel5']['codigo_dominio'] !== $data['ipcc']['nivel5']['codigo_dominio'] ?
                        $data['ipcc']['nivel5']['codigo_dominio'] :
                        $data['ipccAntigo']['nivel5']['codigo_dominio'];

                //Verifica a existencia de mesmo codigo
                $occursCodigo = false;
                if(
                    ($data['ipccAntigo']['nivel5']['codigo'] !== $data['ipcc']['nivel5']['codigo']) ||
                    ($data['ipccAntigo']['nivel5']['descricao'] !== $data['ipcc']['nivel5']['descricao'])
                ) {
                    $occursCodigo = self::validateEntirePlan($data['ipcc']) == '0' && self::validateEntireCod($data['ipcc']) == '0';
                }

                if($occursCodigo) {
                    $set = "SET COD_N5 = '{$data['ipcc']['nivel5']['codigo']}', N5 = '{$data['ipcc']['nivel5']['descricao']}',
											CODIGO_DOMINIO = '{$data['ipcc']['nivel5']['codigo_dominio']}'";
                    $where  = "WHERE COD_N5 = '{$data['ipccAntigo']['nivel5']['codigo']}'";
                }else {
                    $occurs++;
                }

                break;
            case 4:
                $data['ipcc']['nivel4']['codigo'] =
                    isset($data['ipccAntigo']['nivel4']['codigo']) && $data['ipccAntigo']['nivel4']['codigo'] !== $data['ipcc']['nivel4']['codigo'] ?
                        $data['ipcc']['nivel4']['codigo'] :
                        $data['ipccAntigo']['nivel4']['codigo'];
                $data['ipcc']['nivel4']['descricao'] =
                    isset($data['ipccAntigo']['nivel4']['descricao']) && $data['ipccAntigo']['nivel4']['descricao'] !== $data['ipcc']['nivel4']['descricao'] ?
                        $data['ipcc']['nivel4']['descricao'] :
                        $data['ipccAntigo']['nivel4']['descricao'];

                //Verifica a existencia de mesmo codigo
                //die(var_dump($data['ipccAntigo']['nivel4']['codigo'], $data['ipcc']['nivel4']['codigo']));
                $occursCodigo =
                    ($data['ipccAntigo']['nivel4']['codigo'] !== $data['ipcc']['nivel4']['codigo']) ?
                        self::validate('COD_N4', $data['ipcc']['nivel4']['codigo']) :
                        '0';

                if($occursCodigo === '0') {
                    $set    = "SET N4 = '{$data['ipcc']['nivel4']['descricao']}', COD_N4 = '{$data['ipcc']['nivel4']['codigo']}'";
                    $where  = "WHERE COD_N4 = '{$data['ipccAntigo']['nivel4']['codigo']}'";
                }else {
                    $occurs++;
                }
                break;
            case 3:
                $data['ipcc']['nivel3']['codigo'] =
                    isset($data['ipccAntigo']['nivel3']['codigo']) && $data['ipccAntigo']['nivel3']['codigo'] !== $data['ipcc']['nivel3']['codigo'] ?
                        $data['ipcc']['nivel3']['codigo'] :
                        $data['ipccAntigo']['nivel3']['codigo'];
                $data['ipcc']['nivel3']['descricao'] =
                    isset($data['ipccAntigo']['nivel3']['descricao']) && $data['ipccAntigo']['nivel3']['descricao'] !== $data['ipcc']['nivel3']['descricao'] ?
                        $data['ipcc']['nivel3']['descricao'] :
                        $data['ipccAntigo']['nivel3']['descricao'];

                //Verifica a existencia de mesmo codigo
                $occursCodigo =
                    ($data['ipccAntigo']['nivel3']['codigo'] !== $data['ipcc']['nivel3']['codigo']) ?
                        self::validate('COD_N3', $data['ipcc']['nivel3']['codigo']) :
                        '0';

                if($occursCodigo === '0') {
                    $set    = "SET N3 = '{$data['ipcc']['nivel3']['descricao']}', COD_N3 = '{$data['ipcc']['nivel3']['codigo']}'";
                    $where  = "WHERE COD_N3 = '{$data['ipccAntigo']['nivel3']['codigo']}'";
                }else {
                    $occurs++;
                }
                break;
            case 2:
                $data['ipcc']['nivel2']['codigo'] =
                    isset($data['ipccAntigo']['nivel2']['codigo']) && $data['ipccAntigo']['nivel2']['codigo'] !== $data['ipcc']['nivel2']['codigo'] ?
                        $data['ipcc']['nivel2']['codigo'] :
                        $data['ipccAntigo']['nivel2']['codigo'];
                $data['ipcc']['nivel2']['descricao'] =
                    isset($data['ipccAntigo']['nivel2']['descricao']) && $data['ipccAntigo']['nivel2']['descricao'] !== $data['ipcc']['nivel2']['descricao'] ?
                        $data['ipcc']['nivel2']['descricao'] :
                        $data['ipccAntigo']['nivel2']['descricao'];

                //Verifica a existencia de mesmo codigo
                $occursCodigo =
                    ($data['ipccAntigo']['nivel2']['codigo'] !== $data['ipcc']['nivel2']['codigo']) ?
                        self::validate('COD_N2', $data['ipcc']['nivel2']['codigo']) :
                        '0';

                if($occursCodigo === '0') {
                    $set    = "SET N2 = '{$data['ipcc']['nivel2']['descricao']}', COD_N4 = '{$data['ipcc']['nivel2']['codigo']}'";
                    $where  = "WHERE COD_N2 = '{$data['ipccAntigo']['nivel2']['codigo']}'";
                }else {
                    $occurs++;
                }
                break;
        }

        if($occurs < 1){
            $sql = "UPDATE plano_contas_contabil {$set} {$where}";
            $result = mysql_query($sql);
            Log::registerUserLog($sql);

            return ($result ? mysql_affected_rows() : mysql_error());
        }else{
            return "Já existe um registro com esse código!";
        }
    }

    public static function delete($data)
    {
        $response = [];
        $status = self::checkIntegrity($data['level'], $data['match']);
        if(!$status['ipcf'] && !$status['fornecedores'] && !$status['bancos'] && !$status['childs']){
            $sql = "DELETE FROM plano_contas_contabil WHERE COD_N{$data['level']} = '{$data['match']}'";
            Log::registerUserLog($sql);
            return mysql_query($sql);
        }else{
            $response['msg']['ipcf'] 					= $status['ipcf'] 				? 'Essa conta possui conta(s) associada(s)!' : '';
            $response['msg']['fornecedores'] 	= $status['fornecedores'] ? 'Essa conta possui fornecedor(es) associado(s)!' : '';
            $response['msg']['bancos'] 				= $status['bancos'] 			? 'Essa conta possui banco(s) associado(s)!' : '';
            $response['msg']['childs'] 				= $status['childs'] 			? 'Essa conta possui subcontas. Para excluí-la será necessário remover todas as subcontas.' : '';
            return $response;
        }
    }

    private static function checkIntegrity($level, $match)
    {
        $status = [];
        $status['ipcf'] 				= self::getAssocIPCF($level, $match);
        $status['fornecedores'] = self::getAssocFornecedores($level, $match);
        $status['bancos'] 			= self::getAssocBancos($level, $match);
        $status['childs'] 			= $level < 5 ? self::getChilds($level, $match) : false;
        return $status;
    }

    private static function getAssocIPCF($level, $match)
    {
        $result = self::getByCodigo($match, $level);
        $sql    = "SELECT COUNT(ID) as qtd FROM plano_contas WHERE IPCC = '{$result['ID']}'";
        $result = mysql_query($sql);
        $fetch  = mysql_fetch_array($result, MYSQL_ASSOC);
        return $fetch['qtd'] > 0 ? true : false;
    }

    private static function getAssocFornecedores($level, $match)
    {
        $result = self::getByCodigo($match, $level);
        $sql    = "SELECT COUNT(idFornecedores) as qtd FROM fornecedores WHERE IPCC = '{$result['ID']}'";
        $result = mysql_query($sql);
        $fetch  = mysql_fetch_array($result, MYSQL_ASSOC);
        return $fetch['qtd'] > 0 ? true : false;
    }

    private static function getAssocBancos($level, $match)
    {
        $result = self::getByCodigo($match, $level);
        $sql    = "SELECT COUNT(ID) as qtd FROM contas_bancarias WHERE IPCC = '{$result['ID']}'";
        $result = mysql_query($sql);
        $fetch  = mysql_fetch_array($result, MYSQL_ASSOC);
        return $fetch['qtd'] > 0 ? true : false;
    }

    private static function getChilds($level, $match)
    {
        switch($level){
            case 2:
                $cond = "COD_N2";
                break;
            case 3:
                $cond = "COD_N3";
                break;
            case 4:
                $cond = "COD_N4";
                break;
        }
        $sql    = "SELECT COUNT(ID) as childs FROM plano_contas_contabil WHERE {$cond} = '{$match}'";
        $result = mysql_query($sql);
        $fetch  = mysql_fetch_array($result, MYSQL_ASSOC);
        return $fetch['childs'] > 0 ? true : false;
    }

    public static function getJSON($data)
    {
    /*case 'A' :
						$nivel1 = '1 - ATIVO';
						break;
					case 'P' :
						$nivel1 = '2 - PASSIVO';
						break;
					case 'CRR' :
						$nivel1 = '3 - CONTAS DE RESULTADO - RECEITAS';
						break;
                    case 'C' :
                        $nivel1 = '4 - CUSTOS';
                        break;
                    case 'DO' :
                        $nivel1 = '5 - DESPESAS OPERACIONAIS';
                        break;
                    case 'RF' :
                        $nivel1 = '6 - RESULTADO FINANCEIRO';
                        break;
                    case 'OR' :
                        $nivel1 = '7 - OUTRAS RECEITAS';
                        break;
					case 'PRO' :
						$nivel1 = '8 - PROVISÃO DE IRPJ E CSLL';
						break;*/
        $data = array_map(function($upper){
            return strtoupper($upper);
        }, $data);
        switch($data['nivel']){
            case 1:
                $columns = "(CASE
                                WHEN N1 = 'A' THEN 'ATIVO'
                                WHEN N1 = 'P' THEN 'PASSIVO'
                                WHEN N1 = 'CRR' THEN 'CONTAS DE RESULTADO - RECEITAS'
                                WHEN N1 = 'C' THEN 'CUSTOS'
                                WHEN N1 = 'DO' THEN 'DESPESAS OPERACIONAIS'
                                WHEN N1 = 'RF' THEN 'RESULTADO FINANCEIRO'
                                WHEN N1 = 'OR' THEN 'OUTRAS RECEITAS'
                                WHEN N1 = 'PRO' THEN 'PROVISÃO DE IRPJ E CSLL'
                                END) as label,
                            (CASE
                                WHEN N1 = 'A' THEN '1'
                                WHEN N1 = 'P' THEN '2'
                                WHEN N1 = 'CRR' THEN '3'
                                WHEN N1 = 'C' THEN '4'
                                WHEN N1 = 'DO' THEN '5'
                                WHEN N1 = 'RF' THEN '6'
                                WHEN N1 = 'OR' THEN '7'
                                WHEN N1 = 'PRO' THEN '8'
                                END) as value";
                if(strpos($data['term'], 'ATI') === 0){
                    $data['term'] = "A";
                }elseif(strpos($data['term'], 'PAS') === 0){
                    $data['term'] = "P";
                }elseif(strpos($data['term'], 'RES') === 0){
                    $data['term'] = "CRR";
                }elseif(strpos($data['term'], 'CUS') === 0){
                    $data['term'] = "C";
                }elseif(strpos($data['term'], 'DES') === 0){
                    $data['term'] = "DO";
                }elseif(strpos($data['term'], 'FIN') === 0){
                    $data['term'] = "RF";
                }elseif(strpos($data['term'], 'OUT') === 0){
                    $data['term'] = "OR";
                }elseif(strpos($data['term'], 'PRO') === 0){
                    $data['term'] = "PRO";
                }
                break;
            case 2:
                $columns = "Concat(COD_N2, ' - ', UPPER(N2)) as label, COD_N2 as value, UPPER(N2) as labelSelected";
                break;
            case 3:
                $columns = "Concat(COD_N3, ' - ', UPPER(N3)) as label, COD_N3 as value, UPPER(N3) as labelSelected";
                break;
            case 4:
                $columns = "Concat(COD_N4, ' - ', UPPER(N4)) as label, COD_N4 as value, UPPER(N4) as labelSelected";
                break;
            case 5:
                $columns = "Concat(COD_N5, ' - ', UPPER(N5)) as label, COD_N5 as value, UPPER(N5) as labelSelected";
                
                break;
        }

        $gruopBy = $data['nivel'] == 5 ? '' : "  GROUP BY label";       

          

        $sql  = " SELECT {$columns}
              FROM plano_contas_contabil
              WHERE N{$data['nivel']} LIKE '%{$data['term']}%'
                {$gruopBy}
             ";
        $sql .= $data['nivel'] > 1 ? " ORDER BY COD_N{$data['nivel']}" : "";
       
        return json_encode(parent::get($sql));
    }

    public static function createDataFromCSV($file)
    {
        $data = self::CSVToArray($file);
        $ipcc = [];
        $sql  = "INSERT INTO plano_contas_contabil VALUES ";
        foreach($data as $itens){
            if($itens['grau'] === '1') {
                switch (substr($itens['codigo'], 0, 1)) {
                    case '1' :
                        $N1 = 'A';
                        break;
                    case '2' :
                        $N1 = 'P';
                        break;
                    case '3' :
                        $N1 = 'R';
                        break;
                    case '9' :
                        $N1 = 'ARE';
                        break;
                }
            }
            switch($itens['grau']){
                case '2':
                    $COD_N2 = $itens['codigo'];
                    $N2     = $itens['nome'];
                    break;
                case '3':
                    $COD_N3 = $itens['codigo'];
                    $N3     = $itens['nome'];
                    break;
                case '4':
                    $COD_N4 = $itens['codigo'];
                    $N4     = $itens['nome'];
                    break;
                case '5':
                    $sql .= "('', '{$N1}', '{$COD_N2}', '{$N2}', '{$COD_N3}', '{$N3}', '{$COD_N4}', '{$N4}', '{$itens['codigo']}', '{$itens['nome']}'),";
                    break;
            }
        }

        return mysql_query($sql);
    }

    private static function CSVToArray($file)
    {
        $delimiter = ';';

        $handle = fopen($file, 'r');
        if ($handle) {
            $header = fgetcsv($handle, 0, $delimiter);
            while (!feof($handle)) {
                $row[] = array_combine($header, fgetcsv($handle, 0, $delimiter));
                if (!$row) {
                    continue;
                }
            }
            fclose($handle);
        }
        return $row;
    }

    private static function validate($field, $match)
    {
        $sql    = "SELECT COUNT(ID) as occurs FROM plano_contas_contabil WHERE {$field} = '{$match}'";
        $result = mysql_query($sql);
        $fetch  = mysql_fetch_array($result);
        return $fetch['occurs'];
    }

    private static function validateEntirePlan($data)
    {
        $sql    = "SELECT 
                    COUNT(ID) as occurs 
                   FROM 
                    plano_contas_contabil 
                   WHERE 
                    N1 = '{$data['nivel1']['codigo']}' 
                    AND N2 = '{$data['nivel2']['descricao']}'
                    AND COD_N2 = '{$data['nivel2']['codigo']}'
                    AND N3 = '{$data['nivel3']['descricao']}'
                    AND COD_N3 = '{$data['nivel3']['codigo']}'
                    AND N4 = '{$data['nivel4']['descricao']}'
                    AND COD_N4 = '{$data['nivel4']['codigo']}'
                    AND N5 = '{$data['nivel5']['descricao']}'
                    AND COD_N5 = '{$data['nivel5']['codigo']}'";
        $result = mysql_query($sql);
        $fetch  = mysql_fetch_array($result);
        return $fetch['occurs'];
    }

    private static function validateEntireCod($data)
    {
        $sql    = "SELECT 
                    COUNT(ID) as occurs 
                   FROM 
                    plano_contas_contabil
                   WHERE 
                    N1 = '{$data['nivel1']['codigo']}' 
                    AND COD_N2 = '{$data['nivel2']['codigo']}'
                    AND COD_N3 = '{$data['nivel3']['codigo']}'
                    AND COD_N4 = '{$data['nivel4']['codigo']}'
                    AND COD_N5 = '{$data['nivel5']['codigo']}'";
        $result = mysql_query($sql);
        $fetch  = mysql_fetch_array($result);
        return $fetch['occurs'];
    }

    public static function checkIfContaIsResultadoByCodDominio($codigo)
    {
        $notResultado = ['A', 'P', 'PRO'];
        $sql = <<<SQL
SELECT N1 FROM plano_contas_contabil WHERE CODIGO_DOMINIO = '{$codigo}'
SQL;
        $response = current(parent::get($sql, 'Assoc'));
        return !in_array($response['N1'], $notResultado);
    }

    public static function checkIfJurosDescontoContaIsResultadoByCodDominio($codigoDebito, $codigoCredito)
    {
        $codigo = $codigoDebito != '0000000' ? $codigoDebito : $codigoCredito;
        $notResultado = ['A', 'P', 'PRO'];
        $sql = <<<SQL
SELECT N1 FROM plano_contas_contabil WHERE CODIGO_DOMINIO = '{$codigo}'
SQL;
        $response = current(parent::get($sql, 'Assoc'));
        return !in_array($response['N1'], $notResultado);
    }
}