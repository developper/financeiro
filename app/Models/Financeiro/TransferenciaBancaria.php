<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
	\App\Models\DAOInterface;

class TransferenciaBancaria extends AbstractModel implements DAOInterface
{

	public static function getAll()
	{
		return parent::get(self::getSQL());
	}

	public static function getById ($id)
	{
		return parent::get (self::getSQL ($id));
	}

	public static function getCentroCustoByOrigem($origem)
	{
		$sql = <<<SQL
SELECT
/* REGRAS IMPOSTAS POR JEAN PARA SELECAO DO CODIGO DOMINIO
 * A SEREM UTILIZADAS NA COMPOSICAO DO ARQUIVO NA PARTIDA TRANSFENCIAS
 *
 * DEBITO - CODIGO IPCC DA CONTA DESTINO DA TRANSACAO
 * CREDITO - CODIGO IPCC DA CONTA ORIGEM DA TRANSACAO
 */
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	) AS codigo_credito,
	e.centro_custo_dominio AS centro_custo_credito
FROM
	contas_bancarias AS cb
INNER JOIN empresas AS e ON e.id = cb.UR
WHERE
	cb.ID = {$origem}
SQL;
		return parent::get ($sql);
	}

	public static function getCentroCustoByDestino($destino)
	{
		$sql = <<<SQL
SELECT
(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	) AS codigo_debito,
	e.centro_custo_dominio AS centro_custo_debito
FROM
	contas_bancarias AS cb
INNER JOIN empresas AS e ON e.id = cb.UR
WHERE
	cb.ID = {$destino}
SQL;
		return parent::get ($sql);
	}

	public static function getSQL($id = null)
	{
		return "";
	}

}