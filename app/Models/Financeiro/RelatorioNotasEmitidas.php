<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class RelatorioNotasEmitidas extends AbstractModel
{
    public static function getNotasEmitidasPagar($filtros){
        $inicio = $filtros['inicio'];
		$fim = $filtros['fim'];
		$competenciaInicio = $filtros['competencia_inicio_db'];
		$competenciaFim = $filtros['competencia_fim_db'];
		$inseridaInicio = $filtros['inserida_inicio'];
		$inseridaFim = $filtros['inserida_fim'];
		
        $condImposto = $filtros['reteve_imposto'] == 'S' ? 'and (n.COFINS <> 0 or n.CSLL <> 0 or n.IR <> 0 or n.PCC <> 0 or n.ISSQN <> 0 or n.INSS <> 0 or n.PIS <> 0)' : '';
		$condTipoDocFinanceiro = '';
		if(!empty($filtros['tipo_documento_financeiro_string'])){			
			$condTipoDocFinanceiro = " AND n.tipo_documento_financeiro_id in ({$filtros['tipo_documento_financeiro_string']})";
		}

		if(isset($competenciaInicio) && !empty($competenciaInicio)){
				$condCompetenciaInicio = " and data_competencia >= '{$competenciaInicio}'";
		}

		if(isset($competenciaFim) && !empty($competenciaFim)){
			$condCompetenciaFim = " and data_competencia <= '{$competenciaFim}'";
		}

		if(isset($inicio) && !empty($inicio)){
			$condInicio = " and dataEntrada >= '{$inicio}'";
		}

		if(isset($fim) && !empty($fim)){
			$condFim = " and dataEntrada <= '{$fim}'";
		}

		if(isset($inseridaInicio) && !empty($inseridaInicio)){
			$condInseridaInicio = " and DATE_FORMAT(DATA, '%Y-%m-%d') >= '{$inseridaInicio}'";
		}

		if(isset($inseridaFim) && !empty($inseridaFim)){
			$condInseridaFim = " and DATE_FORMAT(DATA, '%Y-%m-%d') <= '{$inseridaFim}'";
		}

		
		
        $sql = "
		SELECT
		n.idNotas as TR,
'Pagar' AS TipoNota,
IF
	( ( `F`.`FISICA_JURIDICA` = 1 ), F.cnpj, F.cpf ) AS `CNPJ_CPF`,
IF
	(
	( ( `F`.`razaoSocial` IS NOT NULL ) OR ( `F`.`razaoSocial` <> '' ) ),
	`F`.`razaoSocial`,
	`F`.`nome` 
	) AS `NOME_FORNECEDOR_CLIENTE`,
	CONCAT(c.NOME,'/',c.UF) as cidade,	 
	n.codigo AS NumNota,
	date_format( `n`.`dataEntrada`, '%d/%m/%Y' ) AS `DATA_EMISSAO_NOTA`,
	date_format( `n`.`DATA`, '%d/%m/%Y' ) AS `DATA_INSERIDA_NOTA`,
	 `n`.`data_competencia`,
	(
			n.valor + n.COFINS + n.CSLL + n.IPI + n.ISSQN + n.INSS  + n.PIS +  n.IR + n.PCC
			
	) AS valorBruto,	
	 `TDF`.`sigla` AS `TIPO_DOCUMENTO_SIGLA`,	 
	e.nome AS Empresa,
	n.valor AS valorLiquido,
	n.COFINS,
	n.CSLL,
	n.DESCONTO_BONIFICACAO,
	n.DES_ACESSORIAS,
	n.FRETE,
	n.ICMS,
	n.INSS,
	n.IPI,
	n.IR,
	n.ISSQN,
	n.PCC,
	n.PIS,
	n.VAL_PRODUTOS, 
	CONCAT(pl.COD_N4,' ',pl.N4) as naturezaPrincipal,
	date_format(
		(select			
			min(vencimento_real) 
			from
			parcelas as p
			where
			p.status != 'Cancelada'  and
			p.idNota = n.idNotas			
			group by p.idNota
		) , '%d/%m/%Y' ) AS `primeiro_vencimento_real`
	
FROM
	`notas` AS `n`
	INNER JOIN `fornecedores` AS `F` ON ( `F`.`idFornecedores` = `n`.`codFornecedor` )
	 INNER JOIN tipo_documento_financeiro AS TDF ON ( n.tipo_documento_financeiro_id = TDF.id )
	 LEFT JOIN cidades AS c ON ( F.CIDADE_ID = c.ID )
	INNER JOIN empresas AS e ON ( n.empresa = e.id ) 
	INNER JOIN plano_contas AS pl ON (n.natureza_movimentacao = pl.ID)
	

WHERE
    n.`status` <> 'Cancelada' 
    {$condImposto}	
	{$condCompetenciaInicio}
	{$condCompetenciaFim}
	{$condInicio}
	{$condFim}
	{$condInseridaInicio}
	{$condInseridaFim}
	AND n.tipo = 1 
	AND ( n.NOTA_REFERENCIA IS NULL OR n.NOTA_REFERENCIA = 0 OR n.NOTA_REFERENCIA = '' ) 
	 AND  n.nota_aglutinacao = 'n'
     and  n.tipo_documento_financeiro_id not in (23, 25, 26)
      {$condTipoDocFinanceiro}
		";
		
		
		
        return parent::get($sql, 'Assoc');
	}
	
	public static function getNotasEmitidasReceber($filtros){
        $inicio = $filtros['inicio'];
		$fim = $filtros['fim'];
		$competenciaInicio = $filtros['competencia_inicio_db'];
		$competenciaFim = $filtros['competencia_fim_db'];
		
        $condImposto = $filtros['reteve_imposto'] == 'S' ? 'and (n.COFINS <> 0 or n.CSLL <> 0 or n.IR <> 0 or n.PCC <> 0 or n.ISSQN <> 0 or n.INSS <> 0 or n.PIS <> 0)' : '';
		$condTipoDocFinanceiro = '';
		$inseridaInicio = $filtros['inserida_inicio'];
		$inseridaFim = $filtros['inserida_fim'];
		if(!empty($filtros['tipo_documento_financeiro_string'])){
			
			$condTipoDocFinanceiro = " AND n.tipo_documento_financeiro_id in ({$filtros['tipo_documento_financeiro_string']})";
		}
		if(isset($competenciaInicio) && !empty($competenciaInicio)){
			$condCompetenciaInicio = " and data_competencia >= '{$competenciaInicio}'";
	}

	if(isset($competenciaFim) && !empty($competenciaFim)){
		$condCompetenciaFim = " and data_competencia <= '{$competenciaFim}'";
	}
	
	if(isset($inicio) && !empty($inicio)){
		$condInicio = " and dataEntrada >= '{$inicio}'";
	}

	if(isset($fim) && !empty($fim)){
		$condFim = " and dataEntrada <= '{$fim}'";
	}
	if(isset($inseridaInicio) && !empty($inseridaInicio)){
		$condInseridaInicio = " and DATE_FORMAT(DATA, '%Y-%m-%d') >= '{$inseridaInicio}'";
	}

	if(isset($inseridaFim) && !empty($inseridaFim)){
		$condInseridaFim = " and DATE_FORMAT(DATA, '%Y-%m-%d') <= '{$inseridaFim}'";
	}
        $sql = "
		SELECT
		n.idNotas as TR,
'Pagar' AS TipoNota,
IF
	( ( `F`.`FISICA_JURIDICA` = 1 ), F.cnpj, F.cpf ) AS `CNPJ_CPF`,
IF
	(
	( ( `F`.`razaoSocial` IS NOT NULL ) OR ( `F`.`razaoSocial` <> '' ) ),
	`F`.`razaoSocial`,
	`F`.`nome` 
	) AS `NOME_FORNECEDOR_CLIENTE`,
	CONCAT(c.NOME,'/',c.UF) as cidade,	 
	n.codigo AS NumNota,
	date_format( `n`.`dataEntrada`, '%d/%m/%Y' ) AS `DATA_EMISSAO_NOTA`,
	date_format( `n`.`DATA`, '%d/%m/%Y' ) AS `DATA_INSERIDA_NOTA`,
	`n`.`data_competencia`,
	(
			n.valor + n.COFINS + n.CSLL + n.IPI + n.ISSQN + n.INSS  + n.PIS +  n.IR + n.PCC
			
	) AS valorBruto,	
	 `TDF`.`sigla` AS `TIPO_DOCUMENTO_SIGLA`,	 
	e.nome AS Empresa,
	n.valor AS valorLiquido,
	n.COFINS,
	n.CSLL,
	n.DESCONTO_BONIFICACAO,
	n.DES_ACESSORIAS,
	n.FRETE,
	n.ICMS,
	n.INSS,
	n.IPI,
	n.IR,
	n.ISSQN,
	n.PCC,
	n.PIS,
	n.VAL_PRODUTOS, 
	CONCAT(pl.COD_N4,' ',pl.N4) as naturezaPrincipal,
	date_format(
		(select			
			min(vencimento_real) 
			from
			parcelas as p
			where
			p.status != 'Cancelada'  and
			p.idNota = n.idNotas			
			group by p.idNota
		) , '%d/%m/%Y' ) AS `primeiro_vencimento_real`
FROM
	`notas` AS `n`
	INNER JOIN `fornecedores` AS `F` ON ( `F`.`idFornecedores` = `n`.`codFornecedor` )
	 INNER JOIN tipo_documento_financeiro AS TDF ON ( n.tipo_documento_financeiro_id = TDF.id )
	 LEFT JOIN cidades AS c ON ( F.CIDADE_ID = c.ID )
	INNER JOIN empresas AS e ON ( n.empresa = e.id ) 
	INNER JOIN plano_contas AS pl ON (n.natureza_movimentacao = pl.ID)
WHERE
    n.`status` <> 'Cancelada' 
    {$condImposto}	
	{$condCompetenciaInicio}
	{$condCompetenciaFim}
	{$condInicio}
	{$condFim}
	{$condInseridaInicio}
	{$condInseridaFim}
	AND n.tipo = 0 
	AND ( n.NOTA_REFERENCIA IS NULL OR n.NOTA_REFERENCIA = 0 OR n.NOTA_REFERENCIA = '' ) 
	 AND  n.nota_aglutinacao = 'n'
     and  n.tipo_documento_financeiro_id not in (23, 25, 26)
      {$condTipoDocFinanceiro}
		";
				
        return parent::get($sql, 'Assoc');
	}

	public static function getNotasPagarComProcessamento($filtros){
        $inicio = $filtros['inicio'];
		$fim = $filtros['fim'];
		$competenciaInicio = $filtros['competencia_inicio'];
        $competenciaFim = $filtros['competencia_fim'];
        $condImposto = $filtros['reteve_imposto'] == 'S' ? 'and (n.COFINS <> 0 or n.CSLL <> 0 or n.IR <> 0 or n.PCC <> 0 or n.ISSQN <> 0 or n.INSS <> 0 or n.PIS <> 0)' : '';
		$condTipoDocFinanceiro = '';
		if(!empty($filtros['tipo_documento_financeiro_string'])){
			
			$condTipoDocFinanceiro = " AND n.tipo_documento_financeiro_id in ({$filtros['tipo_documento_financeiro_string']})";
		}
		if(isset($competenciaInicio) && !empty($competenciaInicio)){
			$condCompetenciaInicio = " and data_competencia >= '{$competenciaInicio}'";
		}

		if(isset($competenciaFim) && !empty($competenciaFim)){
			$condCompetenciaFim = " and data_competencia <= '{$competenciaFim}'";
		}
		
		if(isset($inicio) && !empty($inicio)){
			$condInicio = " and DATA_PAGAMENTO >= '{$inicio}'";
		}

		if(isset($fim) && !empty($fim)){
			$condFim = " and DATA_PAGAMENTO <= '{$fim}'";
		}
        $sql = "
		SELECT
		n.idNotas as TR,
'Pagar' AS TipoNota,
IF
	( ( `F`.`FISICA_JURIDICA` = 1 ), F.cnpj, F.cpf ) AS `CNPJ_CPF`,
IF
	(
	( ( `F`.`razaoSocial` IS NOT NULL ) OR ( `F`.`razaoSocial` <> '' ) ),
	`F`.`razaoSocial`,
	`F`.`nome` 
	) AS `NOME_FORNECEDOR_CLIENTE`,
	CONCAT(c.NOME,'/',c.UF) as cidade,	 
	n.codigo AS NumNota,
	date_format( `n`.`dataEntrada`, '%d/%m/%Y' ) AS `DATA_EMISSAO_NOTA`,
	`n`.`data_competencia`,
	(
			n.valor + n.COFINS + n.CSLL + n.IPI + n.ISSQN + n.INSS  + n.PIS +  n.IR + n.PCC
			
	) AS valorBruto,	
	 `TDF`.`sigla` AS `TIPO_DOCUMENTO_SIGLA`,	 
	e.nome AS Empresa,
	n.valor AS valorLiquido,
	n.COFINS,
	n.CSLL,
	n.DESCONTO_BONIFICACAO,
	n.DES_ACESSORIAS,
	n.FRETE,
	n.ICMS,
	n.INSS,
	n.IPI,
	n.IR,
	n.ISSQN,
	n.PCC,
	n.PIS,
	n.VAL_PRODUTOS, 
	CONCAT(pl.COD_N4,' ',pl.N4) as naturezaPrincipal,
	tb2.id,
	date_format(tb2.DATA_PAGAMENTO, '%d/%m/%Y' ) as dataPagamento,
	SUM(IF (tb2.VALOR_PAGO = 0 or tb2.VALOR_PAGO is null,   tb2.valor_compensada, tb2.VALOR_PAGO )) as VALOR_PAGO
FROM
	`notas` AS `n`
	INNER JOIN `fornecedores` AS `F` ON ( `F`.`idFornecedores` = `n`.`codFornecedor` )
	 INNER JOIN tipo_documento_financeiro AS TDF ON ( n.tipo_documento_financeiro_id = TDF.id )
	 LEFT JOIN cidades AS c ON ( F.CIDADE_ID = c.ID )
	INNER JOIN empresas AS e ON ( n.empresa = e.id ) 
	INNER JOIN plano_contas AS pl ON (n.natureza_movimentacao = pl.ID)
	INNER JOIN (
		select
	p2.*
from
	parcelas as p2
INNER Join (
			select
				p4.idNota,
				min(p4.DATA_PAGAMENTO) as dataPagamento
			from
				parcelas p4
			inner join (
							select
								p3.idNota
							from
								parcelas as p3
							where
								p3.status = 'Processada'
								and p3.DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}'
							group by
							p3.idNota
						) tb1 on ( p4.idNota = tb1.idNota)
				where
						p4.status = 'Processada'
			group BY
				p4.idNota
			) as tb on
	(p2.idNota = tb.idNota and  p2.DATA_PAGAMENTO = tb.dataPagamento
	and p2.status = 'Processada')
	where p2.DATA_PAGAMENTO BETWEEN  '{$inicio}' and '{$fim}'
		) tb2 on n.idNotas = tb2.idNota
WHERE
    n.`status` <> 'Cancelada' 
    {$condImposto}	
	{$condCompetenciaInicio}
	{$condCompetenciaFim}
	AND DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}'
	AND n.tipo = 1 
	AND ( n.NOTA_REFERENCIA IS NULL OR n.NOTA_REFERENCIA = 0 OR n.NOTA_REFERENCIA = '' ) 
	AND  n.nota_aglutinacao = 'n'
	and  n.tipo_documento_financeiro_id not in (23, 25, 26)
	{$condTipoDocFinanceiro}
	group by TR
		";
		
        return parent::get($sql, 'Assoc');
    }
	
	public static function getNotasReceberComProcessamento($filtros){
        $inicio = $filtros['inicio'];
		$fim = $filtros['fim'];
		$competenciaInicio = $filtros['competencia_inicio_db'];
        $competenciaFim = $filtros['competencia_fim_db'];
        $condImposto = $filtros['reteve_imposto'] == 'S' ? 'and (n.COFINS <> 0 or n.CSLL <> 0 or n.IR <> 0 or n.PCC <> 0 or n.ISSQN <> 0 or n.INSS <> 0 or n.PIS <> 0)' : '';
		$condTipoDocFinanceiro = '';
		if(!empty($filtros['tipo_documento_financeiro_string'])){			
			$condTipoDocFinanceiro = " AND n.tipo_documento_financeiro_id in ({$filtros['tipo_documento_financeiro_string']})";
		}
		if(isset($competenciaInicio) && !empty($competenciaInicio)){
			$condCompetenciaInicio = " and data_competencia >= '{$competenciaInicio}'";
		}

		if(isset($competenciaFim) && !empty($competenciaFim)){
			$condCompetenciaFim = " and data_competencia <= '{$competenciaFim}'";
		}
		
		if(isset($inicio) && !empty($inicio)){
			$condInicio = " and DATA_PAGAMENTO >= '{$inicio}'";
		}

		if(isset($fim) && !empty($fim)){
			$condFim = " and DATA_PAGAMENTO <= '{$fim}'";
		}
        $sql = "
		SELECT
		n.idNotas as TR,
'Pagar' AS TipoNota,
IF
	( ( `F`.`FISICA_JURIDICA` = 1 ), F.cnpj, F.cpf ) AS `CNPJ_CPF`,
IF
	(
	( ( `F`.`razaoSocial` IS NOT NULL ) OR ( `F`.`razaoSocial` <> '' ) ),
	`F`.`razaoSocial`,
	`F`.`nome` 
	) AS `NOME_FORNECEDOR_CLIENTE`,
	CONCAT(c.NOME,'/',c.UF) as cidade,	 
	n.codigo AS NumNota,
	date_format( `n`.`dataEntrada`, '%d/%m/%Y' ) AS `DATA_EMISSAO_NOTA`,
	`n`.`data_competencia`,
	(
			n.valor + n.COFINS + n.CSLL + n.IPI + n.ISSQN + n.INSS  + n.PIS +  n.IR + n.PCC
			
	) AS valorBruto,	
	 `TDF`.`sigla` AS `TIPO_DOCUMENTO_SIGLA`,	 
	e.nome AS Empresa,
	n.valor AS valorLiquido,
	n.COFINS,
	n.CSLL,
	n.DESCONTO_BONIFICACAO,
	n.DES_ACESSORIAS,
	n.FRETE,
	n.ICMS,
	n.INSS,
	n.IPI,
	n.IR,
	n.ISSQN,
	n.PCC,
	n.PIS,
	n.VAL_PRODUTOS, 
	CONCAT(pl.COD_N4,' ',pl.N4) as naturezaPrincipal,
	tb2.id,
	date_format(tb2.DATA_PAGAMENTO, '%d/%m/%Y' ) as dataPagamento,
	SUM(IF (tb2.VALOR_PAGO = 0 or tb2.VALOR_PAGO is null,   tb2.valor_compensada, tb2.VALOR_PAGO )) as VALOR_PAGO

FROM
	`notas` AS `n`
	INNER JOIN `fornecedores` AS `F` ON ( `F`.`idFornecedores` = `n`.`codFornecedor` )
	 INNER JOIN tipo_documento_financeiro AS TDF ON ( n.tipo_documento_financeiro_id = TDF.id )
	 LEFT JOIN cidades AS c ON ( F.CIDADE_ID = c.ID )
	INNER JOIN empresas AS e ON ( n.empresa = e.id ) 
	INNER JOIN plano_contas AS pl ON (n.natureza_movimentacao = pl.ID)

	INNER JOIN (
		select
		p2.*
	from
		parcelas as p2
	INNER Join (
				select
					p4.idNota,
					min(p4.DATA_PAGAMENTO) as dataPagamento
				from
					parcelas p4
				inner join (
								select
									p3.idNota
								from
									parcelas as p3
								where
									p3.status = 'Processada'
									and p3.DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}'
								group by
								p3.idNota
							) tb1 on ( p4.idNota = tb1.idNota)
					where
							p4.status = 'Processada'
				group BY
					p4.idNota
				) as tb on
		(p2.idNota = tb.idNota and  p2.DATA_PAGAMENTO = tb.dataPagamento
		and p2.status = 'Processada')
		where p2.DATA_PAGAMENTO BETWEEN  '{$inicio}' and '{$fim}') 
		tb2 on n.idNotas = tb2.idNota

WHERE
    n.`status` <> 'Cancelada' 
    {$condImposto}	
	{$condCompetenciaInicio}
	{$condCompetenciaFim}
	
    AND DATA_PAGAMENTO BETWEEN '{$inicio}' and '{$fim}'
	AND n.tipo = 0 
	AND ( n.NOTA_REFERENCIA IS NULL OR n.NOTA_REFERENCIA = 0 OR n.NOTA_REFERENCIA = '' ) 
	 AND  n.nota_aglutinacao = 'n'
     and  n.tipo_documento_financeiro_id not in (23, 25, 26)
      {$condTipoDocFinanceiro}
	  group by TR
		";
		
        return parent::get($sql, 'Assoc');
    }

	public static function getRateioValorServicoByNotas($idNotas)
	{
		$condNotas = "(".implode(',', $idNotas).")";
		$sql = "SELECT 
n.idNotas as TR,
n.codigo AS NumNota,
n.VAL_PRODUTOS,
(SUM(r.PORCENTAGEM)/100) * n.VAL_PRODUTOS as valor,
SUM(r.PORCENTAGEM),
CONCAT(pc.COD_N4, ' ', pc.N4) as conta
from 
notas n 
inner join rateio r on (n.idNotas = r.NOTA_ID)
inner join  plano_contas pc on (r.IPCG4 = pc.ID)
where 
n.idNotas in {$condNotas}
group by r.NOTA_ID, r.IPCG4
order BY r.NOTA_ID";

		return parent::get($sql, 'Assoc');
	}
    
    
}
