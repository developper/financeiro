<?php

namespace App\Models\Financeiro;

use App\Models\AbstractModel;


class Financeiro extends AbstractModel
{
    public $date;
    public $date_saldo;


    public static function getContasBancariasByUR($ur)
    {
        $sql = <<<SQL
SELECT
				c.ID as id,
				o.SIGLA_BANCO as banco,
				c.AGENCIA as agencia,
				c.NUM_CONTA as conta,
				e.SIGLA_EMPRESA as ur,
				tipo_conta_bancaria.abreviacao,
				o.COD_BANCO as cod_banco,
				CONCAT(o.SIGLA_BANCO,' - ',tipo_conta_bancaria.abreviacao,' - ',c.AGENCIA,' - ',c.NUM_CONTA,' - ',e.SIGLA_EMPRESA) as nome_conta
			FROM contas_bancarias as c
				INNER JOIN origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id)
				INNER JOIN empresas as e ON (c.UR = e.id)
				LEFT JOIN tipo_conta_bancaria ON tipo_conta_bancaria.id = c.TIPO_CONTA_BANCARIA_ID
      where c.UR= {$ur}
			ORDER BY
			banco
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getContasBancariasByURs($urs, $tipoContaBancaria = null)
    {
        $condTipoConta = '';
        if($tipoContaBancaria){
            $condTipoConta = " AND tipo_conta_bancaria.id = {$tipoContaBancaria}";
        }
        $sql = <<<SQL
SELECT
				c.ID as id,
				o.SIGLA_BANCO as banco,
				c.AGENCIA as agencia,
				c.NUM_CONTA as conta,
				e.SIGLA_EMPRESA as ur,
				tipo_conta_bancaria.abreviacao,
				o.COD_BANCO as cod_banco,
				CONCAT(o.SIGLA_BANCO,' - ',tipo_conta_bancaria.abreviacao,' - ',c.AGENCIA,' - ',c.NUM_CONTA,' - ',e.SIGLA_EMPRESA) as nome_conta
			FROM contas_bancarias as c
				INNER JOIN origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id)
				INNER JOIN empresas as e ON (c.UR = e.id)
				LEFT JOIN tipo_conta_bancaria ON tipo_conta_bancaria.id = c.TIPO_CONTA_BANCARIA_ID
      where c.UR in $urs
      {$condTipoConta}
			ORDER BY
			banco
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getMotivoJuros()
    {
        $sql = <<<SQL
SELECT
*
FROM
motivo_juros
WHERE
1
SQL;
        return parent::get($sql, 'Assoc');

    }

    /**
     * @param array $periodos \DateTime com o Periodo atual da busca
     * @param array $data
     * @param null $conta Caso venha preenchida, a consulta irá filtrar por este parâmetro
     * @return array
     */

    public function getDemonstrativos(Array $periodos, $data = array(), $status = "Processada", $conta = null)
    {
        ini_set('xdebug.var_display_max_depth', 5);
        ini_set('xdebug.var_display_max_children', 512);
        ini_set('xdebug.var_display_max_data', 2048);

        $contMeses = 0;
        $totalN1 = 0;
        $saldoFinal = 0;
        $movimento = 0;

        //PEGA TODOS OS NIVEIS DE IPCF DAS CONTAS DE IMPOSTOS E TARIFAS
        $niveisImpostosTarifas = $this->getNiveisIPCFFromTarifasImpostos();

        foreach ($periodos as $periodo) {

            $this->date_saldo = new \stdClass();
            if(property_exists($periodo, 'dia')){
                $this->date_saldo->dia = new \DateTime($periodo->dia->format('Y-m-d'));
            }  else {
                $this->date_saldo->fim = new \DateTime($periodo->fim->format('Y-m-d'));
            }

            //SALDO ANTERIOR
            $saldo = $this->getSaldoAnterior($conta);

            //SOMA TODOS OS SALDOS ANTERIORES DE TODAS AS CONTAS BANCÁRIAS
            $saldoAnterior = array_reduce($saldo, function($buffer, $valores){
                return $buffer += $valores;
            });

            /*if($periodo->inicio->format('Y-m-d') == '2018-01-01') {
                $saldoAnterior = 12349.87;
            } elseif($periodo->inicio->format('Y-m-d') == '2018-02-01') {
                $saldoAnterior =  176052.21;
            } elseif($periodo->inicio->format('Y-m-d') == '2018-03-01') {
                $saldoAnterior =  26672.85;
            }*/

            //echo '<pre>'; die(var_dump($saldo, $saldoAnterior));

            $this->date = new \stdClass();
            if(!property_exists($periodo, 'dia')){
                $this->date->inicio = new \DateTime($periodo->inicio->format('Y-m-d'));
                $this->date->fim = new \DateTime($periodo->fim->format('Y-m-d'));
                $key = $periodo->inicio->format('m/Y');
            }  else {
                $this->date->dia = new \DateTime($periodo->dia->format('Y-m-d'));
                $key = $periodo->dia->format('d/m/Y');
            }

            //FUNÇÃO ITERATIVA PARA AGRUPAR AS TARIFAS E IMPOSTOS, SOMANDO-SE OS VALORES
            $groupByCODN4 = function($new, $a){
                $new[$a['nivel4']] += round($a['val'], 2);
                return $new;
            };

            //TOTAIS DAS TARIFAS E IMPOSTOS DA NOVA TABELA E RESÍDUOS DA TABELA 'NOTAS'
            $totaisTarifas = array_reduce(
                $this->getTarifasImpostos($periodo, $conta),
                $groupByCODN4
            );

            $totalADID = 0;
            $totalRemocao = 0;
            $totalPrevisao = 0;

            if($status != 'Processada') {
                if(property_exists($periodo, 'dia')){
                    $inicio = $periodo->dia->format('Y-m-d');
                    $fim = $periodo->dia->format('Y-m-d');
                }  else {
                    $inicio = $periodo->inicio->format('Y-m-d');
                    $fim = $periodo->fim->format('Y-m-d');
                }
                /*$filtros = [
                    'inicio' => $inicio,
                    'fim' => $fim,
                    'status' => 'PENDENTE',
                    'tipoPeriodo' => 'PREVISAO_RECEBIMENTO',
                    'ordenarPor' => 'previsao_recebimento',
                ];
                $previsaoRecebimento = FaturamentoControleRecebimento::getRelatorioControleRecebimento($filtros);

                $totalADID = array_reduce($previsaoRecebimento, function($buffer, $handler){
                    return $buffer += $handler['modalidade_fatura'] != 'REMOCAO' ? $handler['valor_liquido'] : 0;
                });

                $totalADID = $totalADID != null ? $totalADID : 0;

                $totalRemocao = array_reduce($previsaoRecebimento, function($buffer, $handler){
                    return $buffer += $handler['modalidade_fatura'] == 'REMOCAO' ? $handler['valor_liquido'] : 0;
                });

                $totalRemocao = $totalRemocao != null ? $totalRemocao : 0;

                $totalPrevisao = $totalADID + $totalRemocao;*/
            }

            $demonstrativos = $this->getDemonstrativo($periodo, $status, $conta);
            $contDemonstrativo = 0;
            $jurosMulta = 0;
            $recebimentos = $this->getTotalSintetico($demonstrativos, 'tipoLabel', '1 - RECEBIMENTO');
            $desembolsos = $this->getTotalSintetico($demonstrativos, 'tipoLabel','2 - DESEMBOLSO');

            $recebimentos = $recebimentos != null ? $recebimentos : 0;
            $desembolsos = $desembolsos != null ? $desembolsos : 0;

            //echo '<pre>'; die(var_dump($periodo, $recebimentos, $desembolsos));

            /*
             * Período de 01/01/2018 a 31/03/2018
             *
             * contas com problemas:
             * 17 - caixa ssa
             * 37 - cc sudoeste 033639-4
             *
             * sem movimento:
             * 16, 18, 19, 20, 21, 22, 25, 34, 36
             *
             * so transferencias:
             * 23, 33
             */

            //CALCULANDO O MOVIMENTO
            $movimento = $recebimentos - $desembolsos;

            //MONTANDO O SALDO FINAL
            $saldoFinal = $movimento + $saldoAnterior;

            $totalN2 = [];
            $totalN3 = [];
            $totalN4 = [];

            if(!empty($demonstrativos)) {
                foreach ($demonstrativos as $demonstrativo) {
                    //PREPARANDO AS LABELS DOS NIVEIS DO IPCF QUE MONTARÃO O RELATÓRIO
                    $labelN1 = strtoupper ($demonstrativo['tipoLabel']);
                    $labelN2 = strtoupper (sprintf ('%s - %s', $demonstrativo['COD_N2'], $demonstrativo['N2']));
                    $labelN3 = strtoupper (sprintf ('%s - %s', $demonstrativo['COD_N3'], $demonstrativo['N3']));
                    $labelN4 = strtoupper (sprintf ('%s - %s', $demonstrativo['COD_N4'], $demonstrativo['N4']));

                    $jurosMulta += $demonstrativo['juros'];
                    //ASSOCIANDO CADA NIVEL AO SEU RESPECTIVO VALOR
                    $data[$labelN1][$key] = $demonstrativo['tipoLabel'] == '1 - RECEBIMENTO' ?
                        $this->money($recebimentos) :
                        $this->money($desembolsos);

                    if(!isset($totalN2[$labelN2])) {
                        $totalN2[$labelN2] = 0;
                    }
                    $totalN2[$labelN2] += $demonstrativo['valor_final'];
                    if(!isset($totalN3[$labelN3])) {
                        $totalN3[$labelN3] = 0;
                    }
                    $totalN3[$labelN3] += $demonstrativo['valor_final'];

                    if(!isset($totalN4[$labelN4])) {
                        $totalN4[$labelN4] = 0;
                    }
                    $totalN4[$labelN4] += $demonstrativo['valor_final'];

                    $data[$labelN2][$key] = $this->money($totalN2[$labelN2]);
                    $data[$labelN3][$key] = $this->money($totalN3[$labelN3]);
                    $data[$labelN4][$key] = $this->money($totalN4[$labelN4]);
                    $data['2.5.1.02 - JUROS/MULTAS POR ATRASO'][$key] = $this->money($jurosMulta);

                    $data['3 - SALDO ANTERIOR'][$key] = $this->money($saldoAnterior);
                    $data['4 - MOVIMENTO'][$key] = $this->money($movimento);
                    $data['5 - SALDO FINAL'][$key] = $this->money($saldoFinal);
                    $contDemonstrativo++;
                }
            } else {

                //CASO O DIA NÃO POSSUA MOVIMENTO, ELE TRAZ AINDA ASSIM O MOVIMENTO E OS SALDOS
                if ($contMeses > 0 && $contDemonstrativo === 0) {
                    $saldoAnterior = $saldoFinal;
                }

                $saldoFinal = $movimento + $saldoAnterior;

                $data['3 - SALDO ANTERIOR'][$key] = $this->money ($saldoAnterior);
                $data['4 - MOVIMENTO'][$key] = $this->money ($movimento);
                $data['5 - SALDO FINAL'][$key] = $this->money ($saldoFinal);
            }

            if($status == "Processada") {
                /*echo '<pre>';
                print_r($data);
                die();*/
                /*var_dump($totaisTarifas);
                foreach ($totaisTarifas as $ipcf4 => $val) {
                    //SOMANDO AS TARIFAS AO TOTAL DOS DESEMBOLSOS
                    $data['2 - DESEMBOLSO'][$key] = str_replace(',', '.', (str_replace ('.', '', $data['2 - DESEMBOLSO'][$key])));
                    $data['2 - DESEMBOLSO'][$key] += $val;
                    $data['2 - DESEMBOLSO'][$key] = $this->money($data['2 - DESEMBOLSO'][$key]);

                    //SUBTRAINDO AS TARIFAS AO MOVIMENTO
                    $data['4 - MOVIMENTO'][$key] = str_replace(',', '.', (str_replace ('.', '', $data['4 - MOVIMENTO'][$key])));
                    $data['4 - MOVIMENTO'][$key] -= $val;
                    $data['4 - MOVIMENTO'][$key] = $this->money ($data['4 - MOVIMENTO'][$key]);

                    //SUBTRAINDO AS TARIFAS AO SALDO FINAL
                    $data['5 - SALDO FINAL'][$key] = str_replace(',', '.', (str_replace ('.', '', $data['5 - SALDO FINAL'][$key])));
                    $data['5 - SALDO FINAL'][$key] -= $val;
                    $data['5 - SALDO FINAL'][$key] = $this->money ($data['5 - SALDO FINAL'][$key]);

                    if($ipcf4 == '2.2.7.11') {
                        //SOMANDO O IOF AO SEU RESPECTIVO NIVEL 2
                        $N2IOF = '2.2 - DESEMBOLSOS ADMINISTRATIVOS';
                        $data[$N2IOF][$key] = str_replace(',', '.', (str_replace('.', '', $data[$N2IOF][$key])));
                        $data[$N2IOF][$key] += $val;
                        $data[$N2IOF][$key] = $this->money($data[$N2IOF][$key]);

                        //SOMANDO O IOF AO SEU RESPECTIVO NIVEL 3
                        $N3IOF = '2.2.7 - IMPOSTOS E TAXAS';
                        $data[$N3IOF][$key] = str_replace(',', '.', (str_replace('.', '', $data[$N3IOF][$key])));
                        $data[$N3IOF][$key] += $val;
                        $data[$N3IOF][$key] = $this->money($data[$N3IOF][$key]);

                        //SOMANDO O IOF AO SEU RESPECTIVO NIVEL 4
                        $data['2.2.7.11 - IOF'][$key] = str_replace(',', '.', (str_replace ('.', '', $data['2.2.7.11 - IOF'][$key])));
                        $data['2.2.7.11 - IOF'][$key] += $val;
                        $data['2.2.7.11 - IOF'][$key] = $this->money ($data['2.2.7.11 - IOF'][$key]);
                    } else {
                        //SOMANDO AS TARIFAS AO SEU RESPECTIVO NIVEL 2
                        $N2Tarifas = '2.5 - DESEMBOLSOS FINANCEIROS';
                        $data[$N2Tarifas][$key] = str_replace(',', '.', (str_replace('.', '', $data[$N2Tarifas][$key])));
                        $data[$N2Tarifas][$key] += $val;
                        $data[$N2Tarifas][$key] = $this->money($data[$N2Tarifas][$key]);

                        //SOMANDO AS TARIFAS AO SEU RESPECTIVO NIVEL 3
                        $N3Tarifas = '2.5.1 - DESPESAS FINANCEIRAS';
                        $data[$N3Tarifas][$key] = str_replace(',', '.', (str_replace('.', '', $data[$N3Tarifas][$key])));
                        $data[$N3Tarifas][$key] += $val;
                        $data[$N3Tarifas][$key] = $this->money($data[$N3Tarifas][$key]);

                        //SOMANDO AS TARIFAS AO SEU RESPECTIVO NIVEL 4
                        $data['2.5.1.01 - TARIFAS BANCARIAS'][$key] = str_replace(',', '.', (str_replace ('.', '', $data['2.5.1.01 - TARIFAS BANCARIAS'][$key])));
                        $data['2.5.1.01 - TARIFAS BANCARIAS'][$key] += $val;
                        $data['2.5.1.01 - TARIFAS BANCARIAS'][$key] = $this->money ($data['2.5.1.01 - TARIFAS BANCARIAS'][$key]);
                    }
                }*/
            } else {
                $data['1.1 - RECEBIMENTOS OPERACIONAIS'][$key] = $this->money($this->moneyToFloat($data['1.1 - RECEBIMENTOS OPERACIONAIS'][$key])/* + $totalPrevisao*/);
                $data['1 - RECEBIMENTO'][$key] = $this->money($this->moneyToFloat($data['1 - RECEBIMENTO'][$key]) /*+ $totalPrevisao*/);
                $data['1.1.1 - ASSISTENCIA DOMICILIAR'][$key] = $this->money($this->moneyToFloat($data['1.1.1 - ASSISTENCIA DOMICILIAR'][$key])/* + $totalADID*/);
                $data['1.1.1.01 - ASSIST. DOMICILIAR'][$key] = $this->money($this->moneyToFloat($data['1.1.1.01 - ASSIST. DOMICILIAR'][$key])/* + $totalADID*/);
                $data['1.1.2 - REMOÇOES E EVENTOS'][$key] = $this->money($this->moneyToFloat($data['1.1.2 - REMOÇOES E EVENTOS'][$key])/* + $totalRemocao*/);
                $data['1.1.2.01 - REMOÇOES E EVENTOS'][$key] = $this->money($this->moneyToFloat($data['1.1.2.01 - REMOÇOES E EVENTOS'][$key])/* + $totalADID*/);
                $data['4 - MOVIMENTO'][$key] = $this->money($this->moneyToFloat($data['4 - MOVIMENTO'][$key])/* + $totalPrevisao*/);
                $data['5 - SALDO FINAL'][$key] = $this->money($this->moneyToFloat($data['5 - SALDO FINAL'][$key])/* + $totalPrevisao*/);
            }

            //GERANDO E COMPONDO O SALDO FINAL COM AS CONTAS BANCÁRIAS
            $contasBancarias = $this->getContasBancarias($periodo, $status, $conta);
            $sTotalContas = 0;
            $movContas = [];
            $data['6 - COMPOSIÇÃO DO SALDO FINAL'][$key] = '';

            if(is_array($conta)) {
                while ($sMovContas = mysql_fetch_array($contasBancarias, MYSQL_ASSOC)) {
                    if (in_array($sMovContas['ID'], $conta)) {
                        $movContas[] = $sMovContas;
                    }
                }
            } else {
                while ($sMovContas = mysql_fetch_array($contasBancarias, MYSQL_ASSOC)) {
                    $movContas[] = $sMovContas;
                }
            }

            $totalTransf = 0;
            foreach($movContas as $sMovContas){
                $sResultado[$sMovContas['ID']] = round($sMovContas['resultado'],2);
                $sTotalContas += round($sMovContas['resultado'],2) + $saldo[$sMovContas['ID']];
                $ScomposicaoConta = round($sMovContas['resultado'],2) + $saldo[$sMovContas['ID']];
                $data['7 - '.$sMovContas['ID'].' - '.$sMovContas['conta']][$key] = $this->money($ScomposicaoConta);
                $saldo[$sMovContas['ID']] = $ScomposicaoConta;
                $totalTransf += round($sMovContas['creditos'], 2) - round($sMovContas['debitos'], 2);
            }

            $data['8 - TRANSFERÊNCIAS BANCÁRIAS'][$key] = $this->money($totalTransf);
            $data['9 - TOTAL'][$key] = $this->money($sTotalContas);
            $contMeses++;
        }
        //echo '<pre>'; die(print_r($data));
        return $data;
    }

    /**
     * @param $periodo \DateTime com o Periodo atual da busca
     * @param string $status Processada ou Desprocessada
     * @param null $conta Caso venha preenchida, a consulta irá filtrar por este parâmetro
     * @param string $tipo R = Recebimento, D = Desembolso
     * @return array
     */
    public function getTotalSintetico($data, $field, $contaSintetica)
    {
        $aux = array_filter($data, function($var) use ($field, $contaSintetica) {
            return $var[$field] == $contaSintetica;
        });

        $total = 0;

        foreach ($aux as $a) {
            $total += (float) $a['valor_final'];
        }

        return $total;

        /*return array_reduce($aux, function($buffer, $aux) use ($field, $contaSintetica){
            return $buffer += (float) $aux['valor_final'];
        });*/
    }

    /**
     * @param $number int,float Inteiro ou Float a ser formatado para o padrão brasileiro de valores
     * @return string
     */
    public function money($number)
    {
        return number_format($number, 2, ',', '.');
    }

    private function moneyToFloat($number)
    {
        return str_replace(',', '.', str_replace('.', '', $number));
    }

    /**
     * @param $periodo \DateTime com o Periodo atual da busca
     * @param string $status Processada ou Desprocessada
     * @param null $conta Caso venha preenchida, a consulta irá filtrar por este parâmetro
     * @return array
     */
    public function getDemonstrativo($periodo, $status = 'Processada', $conta = null)
    {
        $datesearch 		= $this->condicaoParcelasDataQuery($periodo, $status);
        $datesearchTarifas  = $this->condicaoGeralDataQuery($periodo);

        $contaSearch = "";
        $contaSearchTarifas = "";

        if((isset($conta) && !empty($conta)) && is_array($conta)) {
            $contaImp = implode(',', $conta);
            $contaSearch = " AND origem IN ({$contaImp})";
            $contaSearchTarifas = " AND itl.conta IN ({$contaImp})";
        }

        $valorField = "valor_final_pendente";
        $statusSearch = " AND `status` = 'Pendente' ";
        $conciliadoSearch = "";
        $tarifas = "";

        if($status == "Processada") {
            $valorField = "valor_final_conciliado";
            $statusSearch = " AND `status` = 'Processada' ";
            $conciliadoSearch = " AND CONCILIADO = 'S' ";
            $tarifas = <<<SQL
UNION
	SELECT
        CONCAT(
          itl.id, ' - ', 'TAXA',
          ' - ', itl.valor) AS id,
		'2 - DESEMBOLSO' AS tipoLabel,
		pc.N2,
		pc.COD_N2,
		pc.N3,
		pc.COD_N3,
		pc.N4,
		pc.COD_N4,
		0 AS juros,
		itl.conta AS origem,
		itl.valor AS valor_final
	FROM
		impostos_tarifas AS imptar
	INNER JOIN impostos_tarifas_lancto AS itl ON (imptar.id = itl.item)
	LEFT JOIN plano_contas AS pc ON (imptar.ipcf = pc.COD_N4)
	WHERE
		itl.`data` {$datesearchTarifas}
		{$contaSearchTarifas}
SQL;
        }

        $urSearch =	isset($_SESSION['empresa_principal']) && $_SESSION['empresa_principal'] != 1
            ? " AND empresa = {$_SESSION['empresa_principal']}"
            : "";

        $sql = <<<SQL
SELECT
 CONCAT(
      id, ' - ', 'PARCELA',
      ' - ', rateio_id,
      ' - ', valor_final_conciliado) AS id,
 '1 - RECEBIMENTO' AS tipoLabel,
 N2,
 COD_N2,
 N3,
 COD_N3,
 N4,
 COD_N4,
 JurosMulta AS juros,
 origem,
 {$valorField} AS valor_final
FROM
  indexed_demonstrativo_recebimentos
WHERE
  {$datesearch}
  {$statusSearch}
  {$conciliadoSearch}
  {$contaSearch}
  {$urSearch}
  
UNION
    SELECT
      CONCAT(
      id, ' - ', 'PARCELA',
      ' - ', rateio_id,
      ' - ', valor_final_conciliado) AS id,
      '2 - DESEMBOLSO' AS tipoLabel,
      N2,
      COD_N2,
      N3,
      COD_N3,
      N4,
      COD_N4,
      JurosMulta AS juros,
      origem,
      {$valorField} AS valor_final
    FROM
      indexed_demonstrativo_desembolsos
    WHERE
      {$datesearch}
      {$statusSearch}
      {$conciliadoSearch}
      {$contaSearch}
      {$urSearch}
    
      {$tarifas}
        ORDER BY
            tipoLabel ASC,
            COD_N4,
            N2 ASC,
            COD_N3 ASC
SQL;
        //echo '<pre>'; die(print_r($sql));
        $data = [];
        $result = parent::get($sql, 'Assoc');
        if(is_array($conta)) {
            foreach ($result as $row) {
                if (in_array($row['origem'], $conta)) {
                    $data[] = $row;
                }
            }
        } else {
            $data = $result;
        }
        //echo '<pre>'; die(var_dump($sql, $result, $conta, count($data), $data));
        //echo '<pre>'; die(var_dump($data));
        return $data;
    }

    /**
     * @param $periodo \DateTime com o Periodo atual da busca
     * @param null $conta $conta Caso venha preenchida, a consulta irá filtrar por este parâmetro
     * @return array
     */
    public function getTarifasImpostos($periodo, $conta = null)
    {
        $datesearch = $this->condicaoGeralDataQuery($periodo);

        $contaSearch = '';
        if(is_array($conta)) {
            $conta = implode(', ', $conta);
            $contaSearch = " AND conta IN ({$conta}) ";
        }

        $condImpTar = array_reduce($this->getNiveisIPCFFromTarifasImpostos(), function($buffer, $nivel){
            return $buffer .= " PL.COD_N4 = '{$nivel['nivel4']}' OR";
        });
        $condImpTar = substr_replace($condImpTar, "", -3);

        $urTarifaSearch = "";
        if(isset($_SESSION['empresa_principal']) && $_SESSION['empresa_principal'] != 1) {
            $urTarifaSearch = " AND cb.UR = {$_SESSION['empresa_principal']}";
        }

        $sql = <<<SQL
SELECT
	sum(itl.valor) AS val,
	imptar.ipcf AS nivel4
FROM
	impostos_tarifas imptar
INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
INNER JOIN contas_bancarias cb ON (cb.id = itl.conta)
WHERE
	itl. DATA {$datesearch}
{$contaSearch}
{$urTarifaSearch}
GROUP BY 
  nivel4
ORDER BY
  nivel4 ASC;
SQL;
        return parent::get($sql, 'Assoc');
    }

    /**
     * @return array
     */
    private function getNiveisIPCFFromTarifasImpostos()
    {
        $sql = <<<SQL
SELECT DISTINCT
	(SUBSTRING(ipcf, 1, 3)) AS nivel2,
	(SUBSTRING(ipcf, 1, 5)) AS nivel3,
	ipcf AS nivel4,
	tipo
FROM
	impostos_tarifas
ORDER BY
	tipo
SQL;
        return parent::get($sql);
    }

    /**
     * @return mixed
     */
    public function getPlanoContas()
    {
        $data = [];
        $sql = <<<SQL
SELECT
	COD_N2,
	N2,
	COD_N3,
	N3,
	COD_N4,
	N4 as descricao,
	N1 as tipo,
	IF (N1 = 'D', '2 - Desembolso', '1 - Recebimento') as tipoLabel
FROM plano_contas
WHERE ID > 153
ORDER BY COD_N2, COD_N3, COD_N4
SQL;

        $result = mysql_query($sql);
        while ($planoConta = mysql_fetch_array($result)) {
            $labelN1 = strtoupper($planoConta['tipoLabel']);
            $labelN2 = strtoupper(sprintf('%s - %s', $planoConta['COD_N2'], $planoConta['N2']));
            $labelN3 = strtoupper(sprintf('%s - %s', $planoConta['COD_N3'], $planoConta['N3']));
            $labelN4 = strtoupper(sprintf('%s - %s', $planoConta['COD_N4'], $planoConta['descricao']));

            $data[$labelN1] = $data[$labelN2] = $data[$labelN3] = $data[$labelN4] = null;
        }

        return $data;

    }

    /**
     * @param null $conta Caso venha preenchida, a consulta irá filtrar por este parâmetro
     * @return array
     */
    public function getSaldoAnterior($conta = null)
    {
        if(property_exists($this->date_saldo, 'dia')){
            $this->date_saldo->dia = new \DateTime($this->date_saldo->dia->format('Y-m-d'));
            $oneDay     = new \DateInterval('P1D');
            $datesearch = $this->date_saldo->dia->sub($oneDay)->format('Y-m-d');
        }  else {
            $this->date_saldo->fim = new \DateTime($this->date_saldo->fim->format('Y-m-d'));
            $oneMonth = new \DateInterval('P1M');
            $fourDays = new \DateInterval('P4D');
            $datesearch = $this->date_saldo->fim->sub($fourDays)->sub($oneMonth)->modify('last day of this month')->format('Y-m-d');
        }

        $contaSearch = "";

        if((isset($conta) && !empty($conta)) && is_array($conta)) {
            $contaImp = implode(',', $conta);
            $contaSearch = " AND idcb.ID IN ({$contaImp})";
        }

        if(isset($_SESSION['empresa_principal']) && $_SESSION['empresa_principal'] != 1) {
            $urSearch 				= " AND empresa = {$_SESSION['empresa_principal']}";
            $compInnerContasDestino	= " INNER JOIN contas_bancarias cb1 ON cb1.ID = tb1.CONTA_DESTINO";
            $compInnerContasOrigem 	= " INNER JOIN contas_bancarias cb2 ON cb2.ID = tb2.CONTA_ORIGEM";
            $compInnerContasTarifas = " INNER JOIN contas_bancarias cb3 ON cb3.ID = itl.conta";
            $urTransfTarifasSearch	= " AND UR = {$_SESSION['empresa_principal']}";
        } else {
            $urSearch 			    = "";
            $compInnerContasDestino	= "";
            $compInnerContasOrigem 	= "";
            $compInnerContasTarifas = "";
            $urTransfTarifasSearch	= "";
        }

        $sql = <<<SQL
SELECT
	*, 
    ROUND(
      (recebimentos + creditos) - 
      (desembolsos + debitos + taxas), 2
    ) AS saldoAnterior
FROM
	(
		SELECT
			idcb.ID,
			idcb.conta,
			COALESCE (
				(
					SELECT
						SUM(idr.valor_final_conciliado)
					FROM
						indexed_demonstrativo_recebimentos idr
					WHERE
					    1 = 1
						AND origem = idcb.ID
					    AND idr.DATA_CONCILIADO BETWEEN (
                            SELECT
                                DATA_SALDO_INICIAL
                            FROM
                                contas_bancarias cb
                            WHERE
                                cb.ID = idcb.ID
                        )
					    AND '{$datesearch}'
					    AND CONCILIADO = 'S'
					    {$urSearch}
				),
				0.00
			) AS recebimentos,
			COALESCE (
				(
					SELECT
						SUM(idd.valor_final_conciliado)
					FROM
						indexed_demonstrativo_desembolsos idd
					WHERE
						1 = 1
						AND origem = idcb.ID
					    AND idd.DATA_CONCILIADO BETWEEN (
                            SELECT
                                DATA_SALDO_INICIAL
                            FROM
                                contas_bancarias cb
                            WHERE
                                cb.ID = idcb.ID
                        )
                        AND '{$datesearch}'
                        AND CONCILIADO = 'S'
                        {$urSearch}
				),
				0.00
			) AS desembolsos,
			COALESCE (
				(
					SELECT
						SUM(tb1.VALOR)
					FROM
						transferencia_bancaria AS tb1
						{$compInnerContasDestino}
					WHERE
						1 = 1 
                        AND CONTA_DESTINO = idcb.ID
					AND tb1.DATA_CONCILIADO_DESTINO BETWEEN (
						SELECT
							DATA_SALDO_INICIAL
						FROM
							contas_bancarias cb
						WHERE
							cb.ID = idcb.ID
					)
					AND '{$datesearch}'
					AND tb1.CONCILIADO_DESTINO = 'S'
					AND tb1.CANCELADA_POR IS NULL
					{$urTransfTarifasSearch}
				),
				0.00
			) AS creditos,
			COALESCE (
				(
					SELECT
						SUM(tb2.VALOR)
					FROM
						transferencia_bancaria AS tb2
						{$compInnerContasOrigem}
					WHERE
						1 = 1
                        AND CONTA_ORIGEM = idcb.ID
					AND tb2.DATA_CONCILIADO_ORIGEM BETWEEN (
						SELECT
							DATA_SALDO_INICIAL
						FROM
							contas_bancarias cb
						WHERE
							cb.ID = idcb.ID
					)
					AND '{$datesearch}'
					AND tb2.CONCILIADO_ORIGEM = 'S'
					AND tb2.CANCELADA_POR IS NULL
					{$urTransfTarifasSearch}
				),
				0.00
			) AS debitos,
			COALESCE (
				(
					SELECT
						SUM(itl.valor)
					FROM
						impostos_tarifas imptar
					INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
					{$compInnerContasTarifas}
					WHERE	
					itl.data <= '{$datesearch}'
					AND itl.conta = idcb.ID
					{$urTransfTarifasSearch}
				),
				0.00
			) AS taxas
		FROM
			indexed_demonstrativo_contas_bancarias AS idcb
        WHERE
          1 = 1
          {$contaSearch}
		GROUP BY
			idcb.ID
	) AS resultado
GROUP BY
	ID
SQL;
        //die('<pre>' . $sql);
        $result = mysql_query($sql) or die('<pre>' . $sql . '<br>' . mysql_error());
        $saldo = [];
        while($row = mysql_fetch_array($result, MYSQL_ASSOC)){
            $saldo[$row['ID']] = $row['saldoAnterior'];
        }
        $saldo = is_array($conta) ? array_intersect_key($saldo, array_flip($conta)) : $saldo;

        return $saldo;
    }

    /**
     * @param $periodo \DateTime com o Periodo atual da busca
     * @param string $status Diz o tipo do relatorio
     * @return string Se for por dia, retorna '*campo* = %dia', se for por periodo, retorna '*campo* entre %inicio e %fim'
     */
    private function condicaoParcelasDataQuery($periodo, $status)
    {
        $field = $status == "Processada" ? "DATA_CONCILIADO" : "vencimento";
        if(property_exists($periodo, 'dia')){
            return sprintf("%s = '%s'", $field, $periodo->dia->format('Y-m-d'));
        }  else {
            return sprintf("%s BETWEEN '%s' AND '%s'", $field, $periodo->inicio->format('Y-m-d'), $periodo->fim->format('Y-m-d'));
        }
    }

    /**
     * @param $periodo \DateTime com o Periodo atual da busca
     * @return string Se for por dia, retorna '*campo* = %dia', se for por periodo, retorna '*campo* entre %inicio e %fim'
     */
    private function condicaoGeralDataQuery($periodo)
    {
        if(property_exists($periodo, 'dia')){
            return sprintf(" = '%s'", $periodo->dia->format('Y-m-d'));
        }  else {
            return sprintf(" BETWEEN '%s' AND '%s'", $periodo->inicio->format('Y-m-d'), $periodo->fim->format('Y-m-d'));
        }
    }

    /**
     * @param $periodo \DateTime com o Periodo atual da busca
     * @param null $conta Caso venha preenchida, a consulta irá filtrar por este parâmetro
     * @return resource
     */
    private function getContasBancarias($periodo, $status = "Processada", $conta = null)
    {
        $datesearchParcelas = $this->condicaoParcelasDataQuery($periodo, $status);
        $datesearch         = $this->condicaoGeralDataQuery($periodo);

        $contaSearch = ' = idcb.ID';
        $contaCond = "";

        if(isset($_SESSION['empresa_principal']) && $_SESSION['empresa_principal'] != 1) {
            $urSearch 				= " AND empresa = {$_SESSION['empresa_principal']}";
            $compInnerContasDestino	= " INNER JOIN contas_bancarias cb1 ON cb1.ID = tb1.CONTA_DESTINO";
            $compInnerContasOrigem 	= " INNER JOIN contas_bancarias cb2 ON cb2.ID = tb2.CONTA_ORIGEM";
            $compInnerContasTarifas = " INNER JOIN contas_bancarias cb3 ON cb3.ID = itl.conta";
            $urTransfTarifasSearch	= " AND UR = {$_SESSION['empresa_principal']}";
        } else {
            $urSearch 							= "";
            $compInnerContasDestino	= "";
            $compInnerContasOrigem 	= "";
            $compInnerContasTarifas = "";
            $urTransfTarifasSearch	= "";
        }

        $sql = <<<SQL
    SELECT
	*, (
		(recebimentos + creditos) - (desembolsos + debitos + taxas)
	) AS resultado
FROM
	(
		SELECT
			idcb.ID,
			idcb.conta,
			COALESCE (
				(
					SELECT
						SUM(idr.valor_final_conciliado)
					FROM
						indexed_demonstrativo_recebimentos idr
					WHERE
						idr.origem {$contaSearch}
					AND idr.{$datesearchParcelas}
					{$urSearch}
				),
				0.00
			) AS recebimentos,
			COALESCE (
				(
					SELECT
						SUM(idd.valor_final_conciliado)
					FROM
						indexed_demonstrativo_desembolsos idd
					WHERE
						idd.origem {$contaSearch}
					AND idd.{$datesearchParcelas}
					{$urSearch}
				),
				0.00
			) AS desembolsos,
			COALESCE (
				(
				SELECT
          SUM(tb1.VALOR)
        FROM
          `transferencia_bancaria` as tb1
          {$compInnerContasDestino}
        WHERE
          tb1.CONTA_DESTINO {$contaSearch}
        AND tb1.CANCELADA_POR IS NULL
        AND tb1.DATA_CONCILIADO_DESTINO <> '0000-00-00'
        AND tb1.DATA_CONCILIADO_DESTINO {$datesearch}
        AND tb1.CONCILIADO_DESTINO = 'S'
        {$urTransfTarifasSearch}
				),
				0.00
			) AS creditos,
			COALESCE (
				(
					SELECT
            SUM(tb2.VALOR)
          FROM
            `transferencia_bancaria` as tb2
            {$compInnerContasOrigem}
          WHERE
            tb2.CONTA_ORIGEM {$contaSearch}
          AND tb2.CANCELADA_POR IS NULL
          AND tb2.DATA_CONCILIADO_ORIGEM <> '0000-00-00'
          AND tb2.DATA_CONCILIADO_ORIGEM {$datesearch}
          AND tb2.CONCILIADO_ORIGEM = 'S'
          {$urTransfTarifasSearch}
				),
				0.00
			) AS debitos,
			COALESCE (
				(
					SELECT
						SUM(itl.valor)
					FROM
						impostos_tarifas imptar
					INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
					{$compInnerContasTarifas}
					WHERE
						itl.`data` {$datesearch}
					AND itl.conta {$contaSearch}
					{$urTransfTarifasSearch}
				),
				0.00
			) AS taxas
		FROM
			indexed_demonstrativo_contas_bancarias AS idcb
		WHERE
			1 = 1
			{$contaCond}
			{$urTransfTarifasSearch}
		GROUP BY
			idcb.ID
	) AS resultado
GROUP BY
	ID
SQL;

        if($status == "Pendente") {
            $sql = <<<SQL
    SELECT
	*, (
		(recebimentos + creditos) - (desembolsos + debitos + taxas)
	) AS resultado
FROM
	(
		SELECT
			idcb.ID,
			idcb.conta,
			COALESCE (
				(
					SELECT
						SUM(idr.valor)
					FROM
						indexed_demonstrativo_recebimentos idr
					WHERE
						idr.origem {$contaSearch}
					AND idr.{$datesearchParcelas}
					{$urSearch}
				),
				0.00
			) AS recebimentos,
			COALESCE (
				(
					SELECT
						SUM(idd.valor)
					FROM
						indexed_demonstrativo_desembolsos idd
					WHERE
						idd.origem {$contaSearch}
					AND idd.{$datesearchParcelas}
					{$urSearch}
				),
				0.00
			) AS desembolsos,
			0 AS creditos,
			0 AS debitos,
			0 AS taxas
		FROM
			indexed_demonstrativo_contas_bancarias AS idcb
		WHERE
			1 = 1
			{$contaCond}
			{$urTransfTarifasSearch}
		GROUP BY
			idcb.ID
	) AS resultado
GROUP BY
	ID
SQL;
        }
        //die($sql);
        return mysql_query($sql);
    }

    /**
     * @param $periodo \DateTime com o Periodo atual da busca
     * @return resource
     */
    public function getCustos($periodo)
    {
        $sql = <<<SQL
SELECT
	s.CATALOGO_ID,
	s. DATA,
	e.nome AS ur,
	s.NUMERO_TISS AS tiss,
IF (
	s.tipo IN (0, 1, 3),
	concat(
      ct.principio,
      ' - ',
      ct.apresentacao
	),
IF (
  s.segundoNome IS NULL,
  '',
  s.segundoNome
)
) AS item,
 SUM(s.quantidade) AS qtd,
 COALESCE (
  (
    SELECT
        AVG(DISTINCT valor)
    FROM
        entrada AS ent
    WHERE
        ent.CATALOGO_ID = s.CATALOGO_ID
    AND ent.TIPO IN (0, 1, 3)
  ),
  0.00
) AS valor,
 e.percentual,
 DATE_FORMAT(s. DATA, '%d/%m/%Y') AS dataSaida
FROM
	saida AS s
INNER JOIN catalogo AS ct ON (ct.ID = s.CATALOGO_ID)
LEFT JOIN empresas AS e ON (s.UR = e.id)
WHERE
	s.tipo IN (0, 1, 3)
AND s. DATA BETWEEN '{$periodo->inicio->format('Y-m-d')}'
AND '{$periodo->fim->format('Y-m-d')}'
AND s.UR = {$periodo->ur}
GROUP BY
	DATA, CATALOGO_ID

UNION ALL

SELECT
  s.CATALOGO_ID,
  s. DATA,
  e.nome AS ur,
  s.NUMERO_TISS AS tiss,
  cb.item,
  SUM(s.quantidade) AS qtd,
  COALESCE (
      (
        SELECT
            AVG(DISTINCT valor)
        FROM
            entrada AS ent
        WHERE
            ent.CATALOGO_ID = s.CATALOGO_ID
        AND ent.TIPO = 5
      ),
      0.00
  ) AS valor,
  e.percentual,
  DATE_FORMAT(s. DATA, '%d/%m/%Y') AS dataSaida
FROM
  saida AS s
LEFT JOIN empresas AS e ON (s.UR = e.id)
INNER JOIN cobrancaplanos AS cb ON (cb.ID = s.CATALOGO_ID)
WHERE
  s.tipo = 5
AND s. DATA BETWEEN '{$periodo->inicio->format('Y-m-d')}'
AND '{$periodo->fim->format('Y-m-d')}'
AND s.UR = {$periodo->ur}
GROUP BY
	DATA, CATALOGO_ID
ORDER BY
  DATA,
  CATALOGO_ID ASC
SQL;
        return parent::get($sql);
    }

    /**
     * Função utilizada na CronTab para recriar diariamente as tabelas temporárias
     * utilizadas no relatório de Fluxo de Caixa Realizado
     */
    public static function indexTablesDemonstrativo()
    {
        mysql_query("BEGIN");

        # CONTAS BANCARIAS
        $sql = <<<SQL
		DROP TABLE IF EXISTS indexed_demonstrativo_contas_bancarias
SQL;
        $rsDropContas = mysql_query($sql);

        $sql = <<<SQL
		CREATE TABLE indexed_demonstrativo_contas_bancarias AS
		SELECT
			cb.*, CONCAT(
				tcb.abreviacao,
				' - ',
				of.forma,
				': ',
				cb.AGENCIA,
				' - ',
				cb.NUM_CONTA
			) AS conta
		FROM
			contas_bancarias AS cb
		INNER JOIN origemfundos AS of ON (cb.ORIGEM_FUNDOS_ID = of.id)
		INNER JOIN tipo_conta_bancaria AS tcb ON (
			cb.TIPO_CONTA_BANCARIA_ID = tcb.id
		);
SQL;
        $rsContas = mysql_query($sql);

        # RECEBIMENTOS
        $sql = <<<SQL
		DROP TABLE IF EXISTS indexed_demonstrativo_recebimentos;
SQL;
        $rsDropRecebimentos = mysql_query($sql);

        $sql = <<<SQL
		CREATE TABLE indexed_demonstrativo_recebimentos AS
		SELECT
		    r.ID AS rateio_id,
			p.*,
			((p.VALOR_PAGO * r.PORCENTAGEM) / 100) AS valor_final_conciliado, 
			((p.valor * r.PORCENTAGEM) / 100) AS valor_final_pendente,
			pc.N2,
            pc.COD_N2,
            pc.N3,
            pc.COD_N3,
            pc.N4,
            pc.COD_N4 
		FROM
			parcelas AS p
			INNER JOIN notas AS n ON (p.idNota = n.idNotas)
			INNER JOIN rateio AS r ON (n.idNotas = r.NOTA_ID)
			INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
		WHERE
			(
              p.CONCILIADO = 'S' AND p.`status` = 'Processada' AND n.tipo = 0
            )
            OR ( p.`status` = 'Pendente' AND n.tipo = 0 )
            AND n.nota_adiantamento = 'N'
            AND n.nota_aglutinacao = 'n'
            AND n.cancelado_em IS NULL 
SQL;
        $rsRecebimentos = mysql_query($sql);

        # DESEMBOLSOS
        $sql = <<<SQL
		DROP TABLE IF EXISTS indexed_demonstrativo_desembolsos;
SQL;
        $rsDropDesembolsos = mysql_query($sql);

        $sql = <<<SQL
		CREATE TABLE indexed_demonstrativo_desembolsos AS
		SELECT
			r.ID AS rateio_id,
			p.*,
			((p.VALOR_PAGO * r.PORCENTAGEM) / 100) AS valor_final_conciliado, 
			((p.valor * r.PORCENTAGEM) / 100) AS valor_final_pendente,
			pc.N2,
            pc.COD_N2,
            pc.N3,
            pc.COD_N3,
            pc.N4,
            pc.COD_N4 
		FROM
			parcelas AS p
			INNER JOIN notas AS n ON (p.idNota = n.idNotas)
			INNER JOIN rateio AS r ON (n.idNotas = r.NOTA_ID)
			INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
		WHERE
			(
              p.CONCILIADO = 'S' AND p.`status` = 'Processada' AND n.tipo = 1
            )
            OR ( p.`status` = 'Pendente' AND n.tipo = 1 )
            AND n.nota_adiantamento = 'N'
            AND n.nota_aglutinacao = 'n'
            AND n.cancelado_em IS NULL 
SQL;
        $rsDesembolsos = mysql_query($sql);

        if(($rsDropContas && $rsDropRecebimentos && $rsDropDesembolsos) && ($rsContas && $rsRecebimentos && $rsDesembolsos)){
            mysql_query("COMMIT");
        }else{
            mysql_query("ROLLBACK");
        }

        return true;
    }

    /**
     * @param $ur int Recebe a UR que acessa o relatório
     * @param null $filters Contém os filtros utilizados no relatório
     * @return array
     */
    public static function getParcelasByFilter($ur, $filters = null)
    {
        $terms = '';
        if(!is_null($filters)){
            $terms = array_reduce($filters, function($term, $filter){
                return $term .= $filter;
            });
        }

        $sql = <<<SQL
SELECT
	p.idNota,
	e.nome AS UR,
	f.razaoSocial,
	DATE_FORMAT(p.DATA_PAGAMENTO,'%d/%m/%Y') as datapagamento,
	DATE_FORMAT(p.vencimento,'%d/%m/%Y') as datavencimento,
	p.valor,
	CONCAT(p.idNota,'-',p.TR) as TR,
	p.`status`
FROM
	notas AS n
INNER JOIN parcelas AS p ON (p.idNota = n.idNotas)
INNER JOIN fornecedores AS f ON (f.idFornecedores = n.codFornecedor)
INNER JOIN empresas AS e ON (e.id = n.empresa)
WHERE
	e.id = {$ur}
	{$terms}
SQL;
        return parent::get($sql);
    }

    public static function getRelatorioRetencoes($filters)
    {
        $compFornecedor = $filters['fornecedor'] != '' ?
            " AND codFornecedor = '{$filters['fornecedor']}'" :
            "";
        $compUr         = $filters['ur'] != '' ?
            " AND empresa = '{$filters['ur']}'" :
            "";
        $sql = <<<SQL
SELECT
	idNotas,
	codigo,
	fornecedores.nome as fornecedor,
	ISSQN,
	PIS,
	COFINS,
	CSLL,
	IR,
	PCC,
	INSS,
	valor
FROM
	notas
	INNER JOIN fornecedores ON notas.codFornecedor = fornecedores.idFornecedores
WHERE
	dataEntrada BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
	{$compFornecedor}
	{$compUr}
ORDER BY
  idNotas
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getRelatorioSimulacoes($filters)
    {
        $compPaciente = $filters['paciente'] != '' ?
            " AND fatura.PACIENTE_ID = '{$filters['paciente']}'" :
            "";

        $compConvenio = $filters['convenio'] != '' ?
            " AND fatura.PLANO_ID = '{$filters['convenio']}'" :
            "";

        $compUr       = $filters['ur'] != '' ?
            " AND fatura.UR = '{$filters['ur']}'" :
            "";

        $sql = <<<SQL
SELECT
  orcamento_simulacao_item.*,
  orcamento_simulacao.*,
  clientes.nome AS paciente,
  empresas.nome AS ur,
  planosdesaude.nome AS convenio
FROM
  orcamento_simulacao_item
  INNER JOIN orcamento_simulacao ON orcamento_simulacao_item.simulacao_id = orcamento_simulacao.id
  INNER JOIN fatura ON orcamento_simulacao.fatura = fatura.ID
  INNER JOIN clientes ON fatura.PACIENTE_ID = clientes.idClientes
  INNER JOIN empresas ON fatura.UR = empresas.id
  INNER JOIN planosdesaude ON fatura.PLANO_ID = planosdesaude.id
WHERE
  orcamento_simulacao.created_at BETWEEN '{$filters['inicio']} 00:00:00' AND '{$filters['fim']} 23:59:59'
  {$compPaciente}
  {$compConvenio}
  {$compUr}
ORDER BY 
  orcamento_simulacao.id
SQL;

        return parent::get($sql, 'assoc');
    }
}