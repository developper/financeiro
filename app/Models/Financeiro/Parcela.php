<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
	\App\Models\DAOInterface;

class Parcela extends AbstractModel
{
    public static function getParcelaById($id)
    {
        $sql = <<<SQL
SELECT * FROM parcelas WHERE id = '{$id}'
SQL;
        return current(parent::get($sql, 'Assoc'));
    }

    public static function getDadosParcelaById($id)
    {
        $sql = <<<SQL
SELECT 
   parcelas.*, 
   fornecedores.razaoSocial, 
   fornecedores.nome,
   notas.natureza_movimentacao
FROM parcelas 
INNER JOIN notas ON parcelas.idNota = notas.idNotas
LEFT JOIN fornecedores ON notas.codFornecedor = fornecedores.idFornecedores 
WHERE parcelas.id = '{$id}'
SQL;
        return current(parent::get($sql, 'Assoc'));
    }

    public static function getNotaByParcelaId($parcela)
    {
        $sql = <<<SQL
SELECT idNota FROM parcelas WHERE id = '{$parcela}'
SQL;
        return current(parent::get($sql, 'Assoc'));
    }

    public static function checkIfNotaHasParcelasPendentes($notaId)
    {
        $sql = <<<SQL
SELECT * FROM parcelas WHERE status = 'Pendente' AND idNota = '{$notaId}'
SQL;
        $data = parent::get($sql, 'Assoc');

        return count($data) > 0;
    }

    public static function checkIfNotaHasParcelasProcessadas($notaId)
    {
        $sql = <<<SQL
SELECT * FROM parcelas WHERE status = 'Processada' AND idNota = '{$notaId}'
SQL;
        $data = parent::get($sql, 'Assoc');

        return count($data) > 0;
    }

    public static function getNotasFromParcelasByNotaAglutinacaoId($notaId)
    {
        $sql = <<<SQL
SELECT 
  notas.*
FROM 
  parcelas 
  INNER JOIN notas ON parcelas.idNota = notas.idNotas
WHERE 
  nota_id_fatura_aglutinacao = '{$notaId}'
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getParcelasAglutinadas($notaId)
    {
        $sql = <<<SQL
SELECT 
  n.*,        
  p.*,
  p.TR AS num_parcela,
  p.vencimento as data_vencimento,
  coalesce(f.razaoSocial, f.nome) as nome_fornecedor,
  e.nome as ur,
  p.status as pst,         
  p.valor as valor_pagar,
  t.sigla,
  p.id as id_parcela,
  p.idNota as id_nota
FROM
  notas as n inner join 
  fornecedores as f on (f.idFornecedores = n.codFornecedor) inner join
  empresas as e on (n.empresa = e.id) inner join 
  parcelas as p on (n.idNotas = p.idNota) inner join 
  tipo_documento_financeiro as t on (n.tipo_documento_financeiro_id = t.id)    
WHERE
  p.nota_id_fatura_aglutinacao = '{$notaId}'
GROUP BY 
  p.id,p.tr
ORDER BY 
  data_vencimento, nome_fornecedor ASC
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getParcelasBordero($bordero)
    {
        $sql = <<<SQL
SELECT 
  n.*,        
  p.*,
  bt.forma_pagamento,
  p.TR AS num_parcela,
  p.vencimento_real as data_vencimento,
  coalesce(f.razaoSocial, f.nome) as nome_fornecedor,
  e.nome as ur,
  p.status as pst,         
  IF(bp.valor_parcela = 0.00, (p.valor + p.JurosMulta + p.OUTROS_ACRESCIMOS) - p.desconto, bp.valor_parcela) as valor_pagar,
  t.sigla,
  p.id as id_parcela,
  p.idNota as id_nota,
  cf.AG AS agencia_fornecedor,
  cf.N_CONTA AS cc_fornecedor,
  CONCAT(
    orf.forma, 
    ' - Ag.: ',  
    cf.AG, 
    ' - Conta: ', 
    cf.N_CONTA, 
    COALESCE(CONCAT(' - OP: ', cf.OP), '')
  ) AS conta_fornecedor,
  f.chave_pix
FROM
  bordero_parcelas as bp inner join 
  bordero as bd on (bp.bordero_id = bd.id) inner join 
  bordero_tipo as bt on (bt.id = bd.bordero_tipo_id) inner join 
  parcelas as p on (bp.parcela_id = p.id) inner join 
  notas as n on (n.idNotas = p.idNota) inner join 
  fornecedores as f on (f.idFornecedores = n.codFornecedor) inner join 
  empresas as e on (n.empresa = e.id) left join 
  contas_fornecedor as cf on (cf.FORNECEDOR_ID = f.idFornecedores) left join 
  origemfundos as orf on (orf.id = cf.origem_fundos_id) inner join 
  tipo_documento_financeiro as t on (n.tipo_documento_financeiro_id = t.id) 
WHERE
  bp.bordero_id = '{$bordero}'
GROUP BY 
  bp.parcela_id
ORDER BY 
  data_vencimento, nome_fornecedor ASC
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getParcelasRetorno($data)
    {
        $parcelas = array_map(function ($value) {
            if($value['codigoSistema'] != '') {
                return $value['codigoSistema'];
            }
        }, $data['response']);

        $parcelas = self::getParcelasFromArray(array_filter($parcelas));

        foreach ($data['response'] as $key => $parcela) {
            $data['response'][$key]['parcelasBD'] = [];
            if(array_key_exists($parcela['codigoSistema'], $parcelas)) {
                $data['response'][$key]['parcelasBD'] = $parcelas[$parcela['codigoSistema']];
            }
        }
        return $data['response'];
    }

    private static function getParcelasFromArray($array)
    {
        if(count($array) > 0) {
            $parcelas = implode(', ', array_filter($array));
            $sql = <<<SQL
SELECT
    parcelas.*,
    COALESCE(
        IF(fornecedores.FISICA_JURIDICA = 0, fornecedores.nome, fornecedores.razaoSocial),
        fornecedores.razaoSocial
    ) AS nomeFornecedor,
    empresas.nome AS ur,
    tipo_documento_financeiro.sigla AS tipo_doc
FROM
    parcelas
    LEFT JOIN notas ON parcelas.idNota = notas.idNotas
    LEFT JOIN fornecedores ON notas.codFornecedor = fornecedores.idFornecedores
    LEFT JOIN empresas ON notas.empresa = empresas.id
    LEFT JOIN tipo_documento_financeiro ON notas.TIPO_DOCUMENTO = tipo_documento_financeiro.id
WHERE 
    parcelas.id IN ({$parcelas}) and 
    parcelas.status <> 'Cancelada'
SQL;


            $fetch = parent::get($sql, 'Assoc');
            $response = [];
            foreach ($fetch as $row) {
                $response[$row['id']] = $row;
            }
            return $response;
        }
        return [];
    }

    public static function baixarParcelasRetorno($parcelas, $banco, $agencia = '', $conta = '')
    {
        mysql_query('BEGIN');
        $origem = '6';
        if($banco == 341){
            $origem = 12;
        }else if($banco == '237') {
            $origem = ContaBancaria::getContasBancariasByAgenciaConta($agencia, $conta)['id'];
        }
        foreach ($parcelas as $parcela => $dados) {
            $parcelaDados = json_decode($dados, true);
            switch ($parcelaDados['segmento']) {
                case 'A':
                    $aplicacao = '5';
                    break;
                case 'J':
                    $aplicacao = '1';
                    break;
                default:
                    $aplicacao = '6';
                    break;
            }
            $sql = <<<SQL
UPDATE parcelas SET 
  origem = '{$origem}', 
  aplicacao = '{$aplicacao}',
  DATA_PAGAMENTO = '{$parcelaDados['dataPagamento']}',
  VALOR_PAGO = '{$parcelaDados['valorPago']}',
  NUM_DOCUMENTO = '{$parcelaDados['codigoTransacao']}',
  status = 'Processada',
  DATA_PROCESSADO = '{$parcelaDados['dataPagamento']}',
  CONCILIADO = 'S',
  DATA_CONCILIADO = '{$parcelaDados['dataPagamento']}'
WHERE
  id = '{$parcela}' and
  status <> 'Cancelada'
SQL;
            $rs = mysql_query($sql);
            if(!$rs) {
                mysql_query('ROLLBACK');
                return false;
            }

            $nota = Parcela::getNotaByParcelaId($parcela);



            $sql = <<<SQL
SELECT COUNT(id) AS qtd_parcelas
FROM parcelas
WHERE idNota = '{$nota['idNota']}' 
AND status = 'Pendente'
GROUP BY idNota
SQL;
            $parcelasPendente = current(parent::get($sql));


            if(!$rs) {
                mysql_query('ROLLBACK');
                return false;
            }

            if($parcelasPendente['qtd_parcelas'] == 0) {
                $sql = <<<SQL
UPDATE notas
SET status = 'Processada'
WHERE idNotas = '{$nota['idNota']}'
SQL;
                $rs = mysql_query($sql);
                if(!$rs) {
                    mysql_query('ROLLBACK');
                    return false;
                }
            } elseif($parcelasPendente['qtd_parcelas'] > 0) {
                $sql = <<<SQL
UPDATE notas
SET status = 'Pendente'
WHERE idNotas = '{$nota['idNota']}'
SQL;
                $rs = mysql_query($sql);
                if(!$rs) {
                    mysql_query('ROLLBACK');
                    return false;
                }
            }
        }
        return mysql_query('COMMIT');
    }

    public static function estornarParcela($parcela)
    {
        $dadosParcela = self::getParcelaById($parcela);
        $dadosBorderoParcelas = Bordero::getDadosBorderoByParcela($parcela);

        mysql_query('BEGIN');
        $sql = <<<SQL
DELETE FROM bordero_parcelas WHERE parcela_id = '{$parcela}' 
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        $sql = <<<SQL
INSERT INTO 
impostos_tarifas_lancto 
(
 conta, 
 item, 
 documento, 
 data, 
 valor
) VALUES (
 '{$dadosParcela['origem']}',
 '13',
 '',
 NOW(),
 '{$dadosParcela['valor']}'
)
SQL;
        $rs = mysql_query($sql);
        $credito_id = mysql_insert_id();
        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        $sql = <<<SQL
INSERT INTO 
estorno_parcela 
(id, 
 parcela_id, 
 data_processada, 
 processada_por, 
 origem, 
 data_conciliada, 
 valor, 
 aplicacao,
 criado_em,
 credito_id
) VALUES (
 NULL,
 '{$parcela}',
 '{$dadosParcela['DATA_DESPROCESSADA']}',
 '{$dadosParcela['USUARIO_ID']}',
 '{$dadosParcela['origem']}',
 '{$dadosParcela['DATA_CONCILIADO']}',
 '{$dadosParcela['valor']}',
 '{$dadosParcela['aplicacao']}',
 NOW(),
 '{$credito_id}'
)
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        $sql = <<<SQL
SELECT COUNT(*) AS qtd_parcelas FROM bordero_parcelas WHERE id = '{$dadosBorderoParcelas['bordero_id']}' 
SQL;
        $qtdParcelas = current(parent::get($sql, 'Assoc'))['qtd_parcelas'];

        if($qtdParcelas == '0') {
            $sql = <<<SQL
UPDATE bordero SET ativo = 'n' WHERE id = '{$dadosBorderoParcelas['bordero_id']}'
SQL;
            $rs = mysql_query($sql);
            if (!$rs) {
                mysql_query('ROLLBACK');
                return false;
            }
            mysql_query('COMMIT');
            return '2';
        }

        return mysql_query('COMMIT');
    }

    public static function getParcelaAtivaByFiltros($filtros)
    {
      $condIdNota = '';
      $condCompensada = '';
      $condAglutinada = '';

      if(!empty($filtros['idNota'])){
        $condIdNota = " AND idNota = {$filtros['idNota']}";
      }

      if(!empty($filtros['compensada'])){
        $condCompensada = "AND compensada in ({$filtros['compensada']})";
      }

      if($filtros['aglutinada'] == 'S'){
        $condAglutinada = " AND (nota_id_fatura_aglutinacao is not null or nota_id_fatura_aglutinacao > 0) ";
      }

      $sql = <<<SQL
SELECT
* 
FROM 
parcelas
 WHERE
 status <> 'Cancelada'
 {$condIdNota}
 {$condCompensada}
 {$condAglutinada}
SQL;

        return parent::get($sql, 'Assoc');
    }

    public static function estornarParcelaFornecedores($parcela, $conta, $dataAgendamento, $dataEstorno, $valor)
    {
        $dadosParcela = self::getDadosParcelaById($parcela);
        $dadosBorderoParcelas = Bordero::getDadosBorderoByParcela($parcela);

        mysql_query('BEGIN');
        // REMOVE A PARCELA DO BORDERO
        $sql = <<<SQL
DELETE FROM bordero_parcelas WHERE parcela_id = '{$parcela}' 
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        $documento = "ESTORNO BANCÁRIO PARA {$dadosParcela['razaoSocial']}";
        if($dadosParcela['razaoSocial'] == '') {
            $documento = "ESTORNO BANCÁRIO PARA {$dadosParcela['nome']}";
        }

        $valor = (float) str_replace(',', '.', str_replace('.', '', $valor));

        // CRIA O ESTORNO DE CREDITO
        $sql = <<<SQL
INSERT INTO 
impostos_tarifas_lancto 
(
 conta, 
 item, 
 documento, 
 data, 
 valor
) VALUES (
 '{$conta}',
 '13',
 '{$documento}',
 '{$dataEstorno}',
 '{$valor}'
)
SQL;
        $rs = mysql_query($sql);
        $credito_id = mysql_insert_id();
        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        // CRIA O ESTORNO DA PARCELA
        $sql = <<<SQL
INSERT INTO 
estorno_parcela 
(id, 
 parcela_id, 
 data_processada, 
 processada_por, 
 origem, 
 data_conciliada, 
 valor, 
 aplicacao,
 criado_em,
 credito_id
) VALUES (
 NULL,
 '{$parcela}',
 '{$dataAgendamento}',
 '{$_SESSION['id_user']}',
 '{$conta}',
 '{$dataAgendamento}',
 '{$valor}',
 '6',
 NOW(),
 '{$credito_id}'
)
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        # ATUALIZA O STATUS DA PARCELA
        $sql = <<<SQL
UPDATE parcelas 
SET 
    status = 'Pendente', 
    VALOR_PAGO = 0.00, 
    desconto = 0.00, 
    JurosMulta = 0.00,
    DATA_PROCESSADO = NULL,
    aplicacao = 0,
    encargos = 0.00,
    OUTROS_ACRESCIMOS = 0.00,
    DATA_PAGAMENTO = NULL
WHERE id = '{$parcela}'
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        $status = 'Em Aberto';
        if(count(Notas::getParcelasProcessadasByNotaId($dadosParcela['idNota'])) > 0) {
            $status = 'Pendente';
        }

        # ATUALIZA O STATUS DA NOTA
        $sql = <<<SQL
UPDATE notas 
SET status = '{$status}'
WHERE idNotas = '{$dadosParcela['idNota']}'
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        # VERIFICA A QUANTIDADE DE PARCELAS DO BORDERO QUE A PARCELA PERTENCE, SE PERTENCER A UM
        $sql = <<<SQL
SELECT COUNT(*) AS qtd_parcelas FROM bordero_parcelas WHERE id = '{$dadosBorderoParcelas['bordero_id']}' 
SQL;
        $qtdParcelas = current(parent::get($sql, 'Assoc'))['qtd_parcelas'];

        if($qtdParcelas == '0') {
            // SE DEPOIS DE EXCLUÍDA DO BORDERO A QUANTIDADE FOR IGUAL A 0, DESATIVA O BORDERO
            $sql = <<<SQL
UPDATE bordero SET ativo = 'n' WHERE id = '{$dadosBorderoParcelas['bordero_id']}'
SQL;
            $rs = mysql_query($sql);
            if (!$rs) {
                mysql_query('ROLLBACK');
                return false;
            }
            mysql_query('COMMIT');
            return '2';
        }

        return mysql_query('COMMIT');
    }

    public static function getEstornoParcelaById($id)
    {
        $sql = <<<SQL
SELECT * FROM estorno_parcela WHERE id = '{$id}'
SQL;
        return parent::get($sql);

    }

    public static function getEstornoParcelaByParcelaId($parcela)
    {
        $sql = <<<SQL
SELECT * FROM estorno_parcela WHERE parcela_id = '{$parcela}'
SQL;
        return parent::get($sql);

    }

    public static function desfazerEstornoParcela($estorno)
    {
        mysql_query('BEGIN');

        // CONSULTAR O ESTORNO
        $estornoParcela = self::getEstornoParcelaById($estorno)[0];

        // EXCLUI O CRÉDITO DO ESTORNO
        $sql = <<<SQL
DELETE FROM impostos_tarifas_lancto WHERE id = '{$estornoParcela['credito_id']}'
SQL;
        $rs = mysql_query($sql);
        if (!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        // EXCLUI O ESTORNO
        $sql = <<<SQL
DELETE FROM estorno_parcela WHERE id = '{$estorno}'
SQL;
        $rs = mysql_query($sql);
        if (!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }
        return mysql_query('COMMIT');
    }

    public static function getParcelasProcessadaComJurosOuDeconto($filtros)
    {
        $condTipo = '';
      
        if($filtros['tipo_nota'] === '0' || $filtros['tipo_nota'] == 1 ){
            $condTipo = " AND n.tipo = {$filtros['tipo_nota']}";
        }
        $sql = <<<SQL
        select
p.id as parcela_id,
idNota as TR ,
if(n.tipo = 0, 'Recebimento', 'Desembolso') as tipo_nota,
f.razaoSocial as fornecedor,
JurosMulta as juros,
desconto as desconto,
DATE_FORMAT(p.vencimento_real, '%d/%m/%Y') as vencimento_real,
DATE_FORMAT(DATA_PROCESSADO, '%d/%m/%Y') as data_processada,
concat( pl.COD_N4,' ',pl.N4) as natureza_principal,
e.nome as empresa_nota,
concat(a.DESCRICAO, '/',e2.nome) setor_empresa,
p.OUTROS_ACRESCIMOS as tarifas,
p.encargos

from
parcelas as p
INNER JOIN notas as n on (p.idNota = n.idNotas)
INNER JOIN fornecedores as f on (n.codFornecedor = f.idFornecedores)
LEFT JOIN plano_contas as pl on (n.natureza_movimentacao = pl.ID)
LEFT JOIN empresas as e on (n.empresa = e.id)
LEFT JOIN rateio AS r on (n.idNotas = r.NOTA_ID)
LEFT JOIN assinaturas as a on (r.ASSINATURA = a.ID)
LEFT JOIN empresas as e2 on (r.CENTRO_RESULTADO = e2.id)
where
(p.JurosMulta > 0 or p.desconto > 0 or p.encargos > 0 or p.OUTROS_ACRESCIMOS > 0)
and 
DATA_PROCESSADO BETWEEN '{$filtros["inicio"]}' and  '{$filtros["fim"]}'
and 
p.status = "Processada"
{$condTipo}
GROUP BY p.id, a.ID
order by tr
SQL;

return parent::get($sql);
      
    }

    public static function getParcelasVarredura($inicio, $fim)
    {
       
            $sql = <<<SQL
SELECT
    parcelas.*,
    COALESCE(
        IF(fornecedores.FISICA_JURIDICA = 0, fornecedores.nome, fornecedores.razaoSocial),
        fornecedores.razaoSocial
    ) AS nomeFornecedor,    
        IF(fornecedores.FISICA_JURIDICA = 0, fornecedores.CPF, fornecedores.cnpj)
    AS cpfCNPJ,
    fornecedores.FISICA_JURIDICA,
    empresas.nome AS ur,
    tipo_documento_financeiro.sigla AS tipo_doc
FROM
    parcelas
    LEFT JOIN notas ON parcelas.idNota = notas.idNotas
    LEFT JOIN fornecedores ON notas.codFornecedor = fornecedores.idFornecedores
    LEFT JOIN empresas ON notas.empresa = empresas.id
    LEFT JOIN tipo_documento_financeiro ON notas.TIPO_DOCUMENTO = tipo_documento_financeiro.id
WHERE 
    parcelas.vencimento between "{$inicio}" and "{$fim}"   
    and parcelas.status <> 'Cancelada'
SQL;


          return parent::get($sql, 'Assoc');
            
    }
}
