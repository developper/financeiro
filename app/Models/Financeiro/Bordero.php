<?php

namespace App\Models\Financeiro;

use App\Helpers\StringHelper;
use \App\Models\AbstractModel;

class Bordero extends AbstractModel
{
    public static function getBorderoById($id)
    {

        $sql = <<<SQL
SELECT 
    bordero.*,  
    bordero_tipo.descricao_tipo,
    bordero_tipo.forma_pagamento,
    DATE_FORMAT(bordero.created_at, '%d/%m/%Y') as criado_em,
    usuarios.nome as criado_por
FROM
    bordero
    INNER JOIN bordero_tipo ON bordero_tipo.id = bordero.bordero_tipo_id
    INNER JOIN usuarios ON usuarios.idUsuarios = bordero.created_by
WHERE 
    bordero.id = '{$id}'
ORDER BY 
    created_at DESC    
SQL;
        return parent::get($sql);
    }

    public static function getBorderosByFilter($filtros)
    {
        $condCodigoBordero ='';
        $condUr ='';
        $condCriacao = '';
        $condConta = '';
        $condBanco = '';
        if(!empty($filtros['codigo_bordero'])){
            $condCodigoBordero = " AND bordero.id = '{$filtros['codigo_bordero']}'";
        }
        if(!empty($filtros['inicio']) && !empty($filtros['fim'])){
            $condCriacao = " AND bordero.created_at between '{$filtros['inicio']} 00:00:01' AND '{$filtros['fim']} 23:59:59'" ;
        }
        if(!empty($filtros['empresa_user'])){
            $condUr = " AND contas_bancarias.UR in {$filtros['empresa_user']}";
        }
        if(!empty($filtros['contas'])){
            $contas = implode(', ', $filtros['contas']);
            $condConta = " AND bordero.conta_id in ({$contas})";
        }
        if(!empty($filtros['bancos'])){
            $bancos = implode(', ', $filtros['bancos']);
            $condBanco = " AND origemfundos.id in ({$bancos})";
        }

        $sql = <<<SQL
SELECT 
    bordero.*,  
    bordero_tipo.descricao_tipo,
    bordero_tipo.forma_pagamento,
    empresas.nome as ur,
    DATE_FORMAT(bordero.created_at, '%d/%m/%Y') as criado_em,
    usuarios.nome as criado_por,
    CONCAT(
        origemfundos.forma, 
        ' - ', 
        tipo_conta_bancaria.abreviacao, 
        ' - ', 
        contas_bancarias.AGENCIA, 
        ' - ', 
        contas_bancarias.NUM_CONTA, 
        ' - ', 
        ur
    ) AS conta,
    origemfundos.forma AS nome_banco,
    origemfundos.COD_BANCO AS cod_banco
FROM
    bordero
    INNER JOIN bordero_tipo ON bordero_tipo.id = bordero.bordero_tipo_id
    INNER JOIN contas_bancarias ON contas_bancarias.ID = bordero.conta_id
    INNER JOIN origemfundos ON origemfundos.id = contas_bancarias.ORIGEM_FUNDOS_ID
    LEFT JOIN tipo_conta_bancaria ON tipo_conta_bancaria.id = contas_bancarias.TIPO_CONTA_BANCARIA_ID
    INNER JOIN empresas ON empresas.id = contas_bancarias.UR
    INNER JOIN usuarios ON usuarios.idUsuarios = bordero.created_by
WHERE 
    bordero.ativo = 's'
    {$condCodigoBordero}
    {$condCriacao}
    {$condUr}   
    {$condConta}       
    {$condBanco} 
ORDER BY 
    created_at DESC    
SQL;
        $borderos = parent::get($sql, 'assoc');
        foreach ($borderos as $key => $bordero) {
            $borderos[$key]['has_processadas'] = self::verificarBorderoParcelasProcessadas($bordero['id']);
        }
        return $borderos;
    }

    public static function getTotalAPagarBordero($borderoId)
    {
        $sql = <<<SQL
SELECT 
   SUM(
       IF
           (bordero_parcelas.valor_parcela = 0.00, 
           (parcelas.valor + parcelas.JurosMulta + parcelas.OUTROS_ACRESCIMOS) - parcelas.desconto, 
           bordero_parcelas.valor_parcela
       )
   ) AS total 
FROM 
   bordero_parcelas 
   INNER JOIN parcelas ON parcelas.id = bordero_parcelas.parcela_id
WHERE
   bordero_parcelas.bordero_id = '{$borderoId}'
GROUP BY 
   bordero_id
SQL;
        return current(parent::get($sql, 'Assoc'));
    }

    public static function getSituacaoCnab($borderoId)
    {
        $sql = <<<SQL
SELECT 
  p.*,
  bt.forma_pagamento,
  cf.AG AS agencia_fornecedor,
  cf.N_CONTA AS cc_fornecedor,
  CONCAT(
    orf.forma, 
    ' - Ag.: ',  
    cf.AG, 
    ' - Conta: ', 
    cf.N_CONTA, 
    COALESCE(CONCAT(' - OP: ', cf.OP), '')
) AS conta_fornecedor,
f.chave_pix,
f.tipo_chave_pix_id
FROM
  bordero_parcelas as bp inner join 
  bordero as bd on (bp.bordero_id = bd.id) inner join 
  bordero_tipo as bt on (bt.id = bd.bordero_tipo_id) inner join 
  parcelas as p on (bp.parcela_id = p.id) inner join
  notas as n on (n.idNotas = p.idNota) inner join
  fornecedores as f on (f.idFornecedores = n.codFornecedor) left join 
  contas_fornecedor as cf on (cf.FORNECEDOR_ID = f.idFornecedores) left join  
  origemfundos as orf on (orf.id = cf.origem_fundos_id)
WHERE
  bp.bordero_id = '{$borderoId}'
GROUP BY 
  bp.parcela_id
SQL;
        $response = parent::get($sql, 'Assoc');
        foreach ($response as $parcela) {
            if($parcela['forma_pagamento'] == 'TED'){
                if(is_null($parcela['conta_fornecedor'])) {
                    return 'A conta do fornecedor está vazia!';
                } elseif(!is_null($parcela['agencia_fornecedor']) && strlen($parcela['agencia_fornecedor']) > 5) {
                    return 'O número da agência deve ter no máximo 5 dígitos!';
                } elseif(!is_null($parcela['cc_fornecedor']) && strlen($parcela['cc_fornecedor']) > 12) {
                    return 'O número da conta deve ter no máximo 12 dígitos!';
                }
            } elseif(
                ($parcela['forma_pagamento'] == 'BOLETO') &&
                (
                    $parcela['codBarras'] == '' ||
                    (strlen($parcela['codBarras']) != 44 && strlen($parcela['codBarras']) != 47)
                )
            ){
                return 'O código de barras está vazio ou não tem o tamanho adequado para este tipo de lançamento!';
            } elseif(
                ($parcela['forma_pagamento'] == 'CONVENIO') &&
                (
                    $parcela['codBarras'] == '' ||
                    (strlen($parcela['codBarras']) != 44 && strlen($parcela['codBarras']) != 48)
                )
            ){
                return 'O código de barras está vazio ou não tem o tamanho adequado para este tipo de lançamento!';
            }elseif(
                ($parcela['forma_pagamento'] == 'PIX' && ($parcela['chave_pix'] == '' || $parcela['tipo_chave_pix_id'] == '' ))
            ){
                return 'Problema no cadastro da chave Pix do Fornecedor/Cliente!';
            }
        }
        return true;
    }

    public static function criarBordero($data)
    {
        //echo '<pre>'; die(print_r($data));
        mysql_query('BEGIN');

        $banco = ContaBancaria::getById($data['conta_id']);

        $sql = <<<SQL
INSERT INTO bordero (banco_id, conta_id, bordero_tipo_id, descricao, created_by, created_at) VALUES 
('{$banco['cod_banco']}', '{$data['conta_id']}', '{$data['tipo_bordero']}', '{$data['descricao']}', '{$_SESSION['id_user']}', NOW())
SQL;
        $rs = mysql_query($sql);
        if (!$rs) {
            mysql_query('ROLLBACK');
            return [
                "erro" => true,
                "mensagem" => "Erro ao criar Bordero."
            ];
        }
        $bordero_id = mysql_insert_id();

        foreach ($data['parcelas_id'] as $key => $parcela) {
            $valores_parcelas = StringHelper::moneyStringToFloat($data['valores_parcelas'][$key]);
            $juros = StringHelper::moneyStringToFloat($data['juros'][$key]);
            $desconto = StringHelper::moneyStringToFloat($data['descontos'][$key]);
            $tarifas_adicionais = StringHelper::moneyStringToFloat($data['tarifas_adicionais'][$key]);

            $valor_parcela = ($valores_parcelas + $juros + $tarifas_adicionais) - $desconto;

            //check parcela em nordero ativo.
            $sql = " SELECT 
            bp.*
            from
            bordero_parcelas as bp
            inner join bordero as b on (bp.bordero_id = b.id)
            WHERE 
            b.ativo = 's'
            and bp.parcela_id ={$parcela}";
            
            $rs = mysql_query($sql);
            if (!$rs) {
                mysql_query('ROLLBACK');
                return [
                    "erro" => true,
                    "mensagem" => "Erro ao consultar parcela."
                ];
            }
            $existeParcela = parent::get($sql);
            if(!empty($existeParcela)){                
                $vencimento = \DateTime::createFromFormat('Y-m-d', $data['vencimentos'][$key])->format('d/m/Y');
                mysql_query('ROLLBACK');
                return [
                    "erro" => true,
                    "mensagem" => "Parcela de valor R$ ".$data['valores_parcelas'][$key]." e venciemnto ".$vencimento
                    ." já associada ao bordero ".$existeParcela[0]['bordero_id']."."
                ];


            }
            

            $sql = <<<SQL
INSERT INTO bordero_parcelas (id, bordero_id, parcela_id, vencimento, valor_parcela, juros, desconto, tarifas_adicionais) VALUES 
(NULL, '{$bordero_id}', '{$parcela}', '{$data['vencimentos'][$key]}', '{$valor_parcela}', '{$juros}', '{$desconto}', '{$tarifas_adicionais}')
SQL;
            $rs = mysql_query($sql);
            if (!$rs) {
                mysql_query('ROLLBACK');
                return [
                    "erro" => true,
                    "mensagem" => "Erro ao criar Parcela de Bordero."
                ];
            }

            $sql = <<<SQL
UPDATE parcelas
SET JurosMulta = '{$juros}', desconto = '{$desconto}', OUTROS_ACRESCIMOS = '{$tarifas_adicionais}'
WHERE id = '{$parcela}'
SQL;
            $rs = mysql_query($sql);
            if (!$rs) {
                mysql_query('ROLLBACK');
                return [
                    "erro" => true,
                    "mensagem" => "Erro ao atualizar Parcela."
                ];
            }
        }

        mysql_query("COMMIT");
        return [
            "erro" => false,
            "mensagem" => "Bordero #{$bordero_id} criado com sucesso."
        ];
    }

    public static function getDadosBorderoEditar($bordero)
    {
        $response = [];

        $sql = <<<SQL
SELECT * FROM bordero WHERE id = '{$bordero}'
SQL;
        $response['dados'] = current(parent::get($sql, 'Assoc'));

        $sql = <<<SQL
SELECT 
  n.*,        
  p.*,
  bt.forma_pagamento,
  bp.vencimento as vencimento_parcela,
  p.id as parcela_id,
  p.TR AS num_parcela,
  p.vencimento as data_vencimento,
  coalesce(f.razaoSocial, f.nome) as nome_fornecedor,
  e.nome as ur,
  p.status as pst,         
  p.valor as valor_pagar,
  t.sigla,
  p.id as id_parcela,
  p.idNota as id_nota,
  CONCAT(
    orf.forma, 
    ' - Ag.: ',  
    cf.AG, 
    ' - Conta: ', 
    cf.N_CONTA, 
    COALESCE(CONCAT(' - OP: ', cf.OP), '')
) AS conta_fornecedor
FROM
  bordero_parcelas as bp inner join 
  bordero as bd on (bp.bordero_id = bd.id) inner join 
  bordero_tipo as bt on (bt.id = bd.bordero_tipo_id) inner join 
  parcelas as p on (bp.parcela_id = p.id) inner join
  notas as n on (n.idNotas = p.idNota) inner join
  fornecedores as f on (f.idFornecedores = n.codFornecedor) inner join
  empresas as e on (n.empresa = e.id) left join  
  contas_fornecedor as cf on (cf.FORNECEDOR_ID = f.idFornecedores) left join  
  origemfundos as orf on (orf.id = cf.origem_fundos_id) inner join  
  tipo_documento_financeiro as t on (n.tipo_documento_financeiro_id = t.id)    
WHERE
  bp.bordero_id = '{$bordero}'
GROUP BY 
  bp.parcela_id
ORDER BY 
  data_vencimento, nome_fornecedor ASC
SQL;
        $response['parcelas'] = parent::get($sql, 'Assoc');
        return $response;
    }

    public static function atualizarBordero($data)
    {
        //echo '<pre>'; die(print_r($data));
        $bordero_id = $data['bordero_id'];
        mysql_query('BEGIN');

        $banco = ContaBancaria::getById($data['conta_id']);

        # ATUALIZA O BORDERO
        $sql = <<<SQL
UPDATE bordero 
SET 
    banco_id = '{$banco['cod_banco']}', 
    conta_id = '{$data['conta_id']}', 
    bordero_tipo_id = '{$data['tipo_bordero']}', 
    descricao = '{$data['descricao']}', 
    updated_by = '{$_SESSION['id_user']}', 
    updated_at = NOW()
WHERE
    id = '{$bordero_id}'
SQL;
        $rs = mysql_query($sql);
        if (!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        # BUSCA AS PARCELAS DO BORDERO PARA 'RESETAR' AS PARCELAS AO SEU ESTADO ORIGINAL
        $sql = <<<SQL
SELECT parcela_id FROM bordero_parcelas WHERE bordero_id = '{$bordero_id}'
SQL;
        $rs = mysql_query($sql);
        if (!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        # 'RESETA' AS PARCELAS AO SEU ESTADO ORIGINAL
        /*while($row = mysql_fetch_array($rs)) {
            $sql = <<<SQL
UPDATE parcelas
SET JurosMulta = '0.00', desconto = '0.00', OUTROS_ACRESCIMOS = '0.00'
WHERE id = '{$row['parcela_id']}'
SQL;
            $rs = mysql_query($sql);
            if (!$rs) {
                mysql_query('ROLLBACK');
                return false;
            }
        }*/

        # REMOVE AS PARCELAS DO BORDERO
        $sql = <<<SQL
DELETE FROM bordero_parcelas WHERE bordero_id = '{$bordero_id}'
SQL;
        $rs = mysql_query($sql);
        if (!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }
        foreach ($data['parcelas_id'] as $key => $parcela) {
            $valores_parcelas = StringHelper::moneyStringToFloat($data['valores_parcelas'][$key]);
            $juros = StringHelper::moneyStringToFloat($data['juros'][$key]);
            $desconto = StringHelper::moneyStringToFloat($data['descontos'][$key]);
            $tarifas_adicionais = StringHelper::moneyStringToFloat($data['tarifas_adicionais'][$key]);
            $valor_parcela = ($valores_parcelas + $juros + $tarifas_adicionais) - $desconto;

            # CRIA AS NOVAS PARCELAS
            $sql = <<<SQL
INSERT INTO bordero_parcelas (id, bordero_id, parcela_id, vencimento, valor_parcela, juros, desconto, tarifas_adicionais) VALUES 
(NULL, '{$bordero_id}', '{$parcela}', '{$data['vencimentos'][$key]}', '{$valor_parcela}', '{$juros}', '{$desconto}', '{$tarifas_adicionais}')
SQL;
            $rs = mysql_query($sql);
            if (!$rs) {
                mysql_query('ROLLBACK');
                return false;
            }

            # ATUALIZA AS PARCELAS COM SEUS NOVOS VALORES
            $sql = <<<SQL
UPDATE parcelas
SET JurosMulta = '{$juros}', desconto = '{$desconto}', OUTROS_ACRESCIMOS = '{$tarifas_adicionais}'
WHERE id = '{$parcela}'
SQL;
            $rs = mysql_query($sql);
            if (!$rs) {
                mysql_query('ROLLBACK');
                return false;
            }
        }

        return mysql_query("COMMIT");
    }

    public static function desfazerBordero($bordero)
    {
        mysql_query('BEGIN');
        $sql = <<<SQL
SELECT 
   parcelas.id, 
   parcelas.status, 
   parcelas.idNota AS nota
FROM 
   parcelas
   INNER JOIN bordero_parcelas ON bordero_parcelas.parcela_id = parcelas.id
WHERE
   bordero_parcelas.bordero_id = '{$bordero}'
SQL;
        $parcelas = parent::get($sql, 'Assoc');
        //echo '<pre>'; die(print_r($parcelas));

        $processadas = [];
        foreach ($parcelas as $parcela) {
            if($parcela['status'] == 'Processada') {
                $processadas[$parcela['nota']] = 'processada';
            }

            /*$sql = <<<SQL
UPDATE parcelas
SET JurosMulta = '0.00', desconto = '0.00', OUTROS_ACRESCIMOS = '0.00'
WHERE id = '{$parcela['id']}'
SQL;
            $rs = mysql_query($sql);
            if (!$rs) {
                mysql_query('ROLLBACK');
                return false;
            }*/
        }
        if(in_array('processada', $processadas)) {
            mysql_query('ROLLBACK');
            return json_encode($processadas);
        }

        $sql = <<<SQL
UPDATE bordero SET ativo = 'n' WHERE id = '{$bordero}'
SQL;
        $rs = mysql_query($sql);
        if (!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        return mysql_query('COMMIT');
    }

    public static function getBorderoCnab($bordero)
    {
        $response = [];

        $sql = <<<SQL
SELECT 
  bordero.*,
  bordero_tipo.forma_pagamento,
  origemfundos.COD_BANCO AS codigo_banco,
  origemfundos.forma AS nome_banco,
  contas_bancarias.AGENCIA AS agencia_banco,
  contas_bancarias.NUM_CONTA AS conta_banco,
  contas_bancarias.CONVENIO_BANCO AS convenio_banco,
  empresas.cnpj AS cnpj_empresa
FROM 
  bordero
  INNER JOIN bordero_tipo ON bordero.bordero_tipo_id = bordero_tipo.id 
  INNER JOIN contas_bancarias ON bordero.conta_id = contas_bancarias.ID
  INNER JOIN empresas ON empresas.id = contas_bancarias.UR
  INNER JOIN origemfundos ON bordero.banco_id = origemfundos.COD_BANCO
WHERE 
  bordero.id = '{$bordero}'
SQL;
        $response['dados'] = current(parent::get($sql));

        $sql = <<<SQL
SELECT 
  n.*,        
  p.*,
  f.*,
  cid.NOME as cidade,
  f.CPF as cpf,
  cid.UF as uf,
  bp.vencimento as vencimento_parcela,
  p.id as parcela_id,
  p.TR AS num_parcela,
  p.vencimento as data_vencimento,
  coalesce(f.razaoSocial, f.nome) as nome_fornecedor,
  e.nome as ur,
  p.status as pst,         
  IF(bp.valor_parcela = 0.00, (p.valor + p.JurosMulta + p.OUTROS_ACRESCIMOS) - p.desconto, bp.valor_parcela) as valor_pagar,
  t.sigla,
  p.id as id_parcela,
  p.idNota as id_nota,
  orf.COD_BANCO AS codigo_banco_fornecedor,
  orf.forma AS nome_banco_fornecedor,
  cf.AG AS agencia_banco_fornecedor,
  cf.N_CONTA AS conta_banco_fornecedor,
  cf.OP AS operacao_banco_fornecedor,
  e.cnpj as empresa_cnpj_fgts,
  e.nome_fgts 
FROM
  bordero_parcelas as bp inner join 
  parcelas as p on (bp.parcela_id = p.id) inner join
  notas as n on (n.idNotas = p.idNota) inner join
  fornecedores as f on (f.idFornecedores = n.codFornecedor) inner join
  cidades as cid on (f.CIDADE_ID = cid.ID) inner join   
  empresas as e on (n.empresa = e.id) left join  
  contas_fornecedor as cf on (cf.FORNECEDOR_ID = f.idFornecedores) left join  
  origemfundos as orf on (orf.id = cf.origem_fundos_id) inner join  
  tipo_documento_financeiro as t on (n.tipo_documento_financeiro_id = t.id)    
WHERE
  bp.bordero_id = '{$bordero}'
GROUP BY 
  bp.parcela_id
ORDER BY 
  data_vencimento, nome_fornecedor ASC
SQL;
        $parcelas = parent::get($sql);
        $response['parcelas'] = [];
        foreach ($parcelas as $parcela) {
            $parcela['razaoSocial'] = iconv('UTF-8', 'ASCII//TRANSLIT', $parcela['razaoSocial']);
            $parcela['nome'] = iconv('UTF-8', 'ASCII//TRANSLIT', $parcela['nome']);
            $response['parcelas'][] = $parcela;
        }
        return $response;
    }

    public static function verificarBorderoParcelasProcessadas($bordero)
    {
        $sql = <<<SQL
SELECT 
   parcelas.status, 
   parcelas.idNota AS nota
FROM 
   parcelas
   INNER JOIN bordero_parcelas ON bordero_parcelas.parcela_id = parcelas.id
WHERE
   bordero_parcelas.bordero_id = '{$bordero}'
SQL;
        $parcelas = parent::get($sql, 'Assoc');

        $processadas = [];
        foreach ($parcelas as $parcela) {
            if($parcela['status'] == 'Processada') {
                $processadas[$parcela['nota']] = 'processada';
            }
        }
        if(in_array('processada', $processadas)) {
            return true;
        }
        return false;
    }

    public static function getDadosBorderoByParcela($parcela)
    {
        $sql = <<<SQL
SELECT * FROM bordero_parcelas WHERE parcela_id = '{$parcela}' 
SQL;
        return current(parent::get($sql, 'Assoc'));
    }

    public static function getAllParcelasBordero($parcela)
    {
        $sql = <<<SQL
SELECT * FROM bordero_parcelas WHERE parcela_id = '{$parcela}' 
SQL;
        return parent::get($sql, 'Assoc');
    }

}
