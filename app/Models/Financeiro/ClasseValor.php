<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class ClasseValor extends AbstractModel implements DAOInterface
{

    public static function getAll()
    {
        return parent::get(self::getSQL());
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getSQL($id = null)
    {

        $id = isset($id) && !empty($id) ? 'WHERE 1=1 AND id = ' . $id : null;
        return <<<SQL
SELECT 
  *
FROM
  classe_valor
{$id}
ORDER BY descricao
SQL;
    }

    public static function checkIfExistsClasseValor($descricao, $isString = false)
    {
        $where = " id = '{$descricao}' ";
        if($isString) {
            $where = " descricao = '{$descricao}' ";
        }
        $sql = <<<SQL
SELECT 
  *
FROM
  classe_valor 
WHERE 
  {$where}
LIMIT 1
SQL;
        $data = current(parent::get($sql));

        if(count($data) > 0) {
            return $data['id'];
        }
        return false;
    }

    public static function save($descricao)
    {
        $sql = <<<SQL
INSERT INTO classe_valor (id, descricao) VALUES (NULL, '{$descricao}')
SQL;
        $rs = mysql_query($sql);
        if($rs) {
            return mysql_insert_id();
        }
        return false;
    }
}
