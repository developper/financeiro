<?php
namespace App\Models\Financeiro;


use App\Core\Config;
use	\App\Models\AbstractModel;
use	\App\Models\DAOInterface;
use \App\Models\Administracao\Cidade;
use \App\Models\Sistema\Equipamentos;
use \App\Services\Gateways\SendGridGateway;
use \App\Services\MailAdapter;

class Fornecedor extends AbstractModel implements DAOInterface
{

    public static function getAll()
    {
        return parent::get(self::getSQL());
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? "AND idFornecedores = '{$id}'" : "";
        return "SELECT fornecedores.*, COALESCE(razaoSocial, nome) AS fornecedor FROM fornecedores WHERE 1 = 1 {$id} ORDER BY razaoSocial";
    }

    public static function getAllAtivos($id = null)
    {
        $id = isset($id) && !empty($id) ? "AND idFornecedores = '{$id}'" : "";
        $sql = "SELECT fornecedores.*, COALESCE(razaoSocial, nome) AS fornecedor FROM fornecedores WHERE ATIVO = 'S' {$id} ORDER BY razaoSocial";
        return parent::get($sql);
    }

    public static function checkIfExistsFornecedorByCNPJ($cnpj,$tipo = null)
    {
        if(empty($tipo)){
            $tipo = 'F';
        }
        $cnpj = preg_replace('/[^0-9]/', '', $cnpj);
        $sql = <<<SQL
SELECT 
  idFornecedores 
FROM 
  fornecedores 
WHERE 
  REPLACE(
      REPLACE(
          REPLACE(cnpj, '/', ''), 
          '.', ''
      ),
  '-', '') = '{$cnpj}'
  AND TIPO = '{$tipo}'
  AND ATIVO = 'S'
SQL;
        $data = current(parent::get($sql));

        if(count($data) > 0) {
            return $data['idFornecedores'];
        }
        return false;
    }

    public static function checkIfExistsFornecedorByCPF($cpf,$tipo = null)
    {
        if(empty($tipo)){
            $tipo = 'F';
        }
        $sql = <<<SQL
SELECT
  idFornecedores
FROM
  fornecedores
WHERE
  (CPF = '{$cpf}' OR
  replace(replace(CPF,'.',''),'-','') = '{$cpf}')
  AND TIPO = '{$tipo}'
  AND ATIVO = 'S'
SQL;
        $data = current(parent::get($sql));

        if(count($data) > 0) {
            return $data['idFornecedores'];
        }
        return false;
    }

    public static function createFornecedorByXMLData($data)
    {
        $razao      = $data->xNome != '' ? $data->xNome : $data->xFant;
        $fantasia   = $data->xFant != '' ? $data->xFant : $data->xNome;
        $cnpj       = $data->CNPJ;
        $cidade     = Cidade::getCidadeByName($data->enderEmit->xMun);
        $cidade     = (int) $cidade['ID'];
        $logradouro = sprintf('%s, %s %s - %s',
            $data->enderEmit->xLgr,
            $data->enderEmit->nro,
            $data->enderEmit->xCpl,
            $data->enderEmit->xBairro
        );
        $telefone   = $data->enderEmit->fone;
        $ies        = $data->IE;

        $sql = <<<SQL
INSERT INTO fornecedores 
(
 fornecedores.idFornecedores, 
 fornecedores.nome, 
 fornecedores.razaoSocial, 
 fornecedores.cnpj, 
 fornecedores.contato, 
 fornecedores.telefone,
 fornecedores.email, 
 fornecedores.endereco, 
 fornecedores.CIDADE_ID, 
 fornecedores.IES, 
 fornecedores.IM, 
 fornecedores.ATIVO, 
 fornecedores.TIPO, 
 fornecedores.CPF, 
 fornecedores.FISICA_JURIDICA, 
 fornecedores.IPCC, 
 fornecedores.responsavel_equipamento, 
 fornecedores.validado_receita,
 fornecedores.created_at, 
 fornecedores.created_by
 )
VALUES (
  NULL, 
  '{$fantasia}', 
  '{$razao}', 
  '{$cnpj}', 
  'PENDENTE', 
  '{$telefone}', 
  'PENDENTE', 
  '{$logradouro}', 
  '{$cidade}',
  '{$ies}',
  '',
  '',
  'F',
  '',
  1,
  896,
  'N',
  now(),
  '{$_SESSION['id_user']}',
  0
)
SQL;
        mysql_query($sql);
        $fornecedorID = mysql_insert_id();

        $para = [];
        $titulo = "Novo fornecedor #{$fornecedorID}";
        $para[] = ['email' => 'controladoria@mederi.com.br','nome' => 'Controladoria Mederi'];
        $texto = "<p>Um novo fornecedor de ID <b>#{$fornecedorID}</b> e Razão Social <b>{$razao}</b> foi criado!</p>";

        $api_data = (object) Config::get('sendgrid');

        $gateway = new MailAdapter(
            new SendGridGateway(
                new \SendGrid($api_data->user, $api_data->pass),
                new \SendGrid\Email()
            )
        );

        $gateway->adapter->sendSimpleMail($titulo, $texto, $para);

        return $fornecedorID;
    }

    public static function createFornecedorByRemocao($data)
    {
        $razao      = $data['razao_social'];
        $fantasia   = $data['nome_fantasia'];
        $cnpj       = $data['cnpj'];
        $cpf        = $data['cpf'];
        $fisJur     = $data['fisica_juridica'];
        //$cidade     = Cidade::getCidadeByName($data['cidade']);
        $cidade     = $data['cidade'];
        $logradouro = $data['endereco'];
        $telefone   = $data['telefone'];
        $ies        = $data['ies_rg'];
        $im         = $data['im'];
        $contato    = $data['contato'];
        $email      = $data['email'];
        $vldReceita = $data['validado_receita'];

        $sql = <<<SQL
INSERT INTO fornecedores 
(
 fornecedores.idFornecedores, 
 fornecedores.nome, 
 fornecedores.razaoSocial, 
 fornecedores.cnpj, 
 fornecedores.contato, 
 fornecedores.telefone,
 fornecedores.email, 
 fornecedores.endereco, 
 fornecedores.CIDADE_ID, 
 fornecedores.IES, 
 fornecedores.IM, 
 fornecedores.ATIVO, 
 fornecedores.TIPO, 
 fornecedores.CPF, 
 fornecedores.FISICA_JURIDICA, 
 fornecedores.IPCC, 
 fornecedores.responsavel_equipamento, 
 fornecedores.validado_receita,
 fornecedores.created_at, 
 fornecedores.created_by
 )
VALUES (
  NULL, 
  '{$fantasia}', 
  '{$razao}', 
  '{$cnpj}', 
  '{$contato}', 
  '{$telefone}', 
  '{$email}', 
  '{$logradouro}', 
  '{$cidade}',
  '{$ies}',
  '{$im}',
  '',
  'C',
  '{$cpf}',
  '{$fisJur}',
  613,
  'N',
  now(),
  '{$_SESSION['id_user']}',
  '{$vldReceita}'
)
SQL;
        mysql_query($sql);
        $fornecedorID = mysql_insert_id();

        $para = [];
        $titulo = "Novo fornecedor/cliente #{$fornecedorID} criado pelo SAC";
        $para[] = ['email' => 'controladoria@mederi.com.br','nome' => 'Controladoria Mederi'];
        $texto = "<p>Um novo fornecedor/cliente de ID <b>#{$fornecedorID}</b> e Razão Social/Nome <b>{$razao}</b> foi criado!</p>";

        $api_data = (object) Config::get('sendgrid');

        $gateway = new MailAdapter(
            new SendGridGateway(
                new \SendGrid($api_data->user, $api_data->pass),
                new \SendGrid\Email()
            )
        );

        $gateway->adapter->sendSimpleMail($titulo, $texto, $para);

        return $fornecedorID;
    }

    public static function getFornecedoresEquipmentResponsible()
    {
        $sql = "SELECT * FROM fornecedores WHERE responsavel_equipamento = 'SIM' ORDER BY razaoSocial";
        return parent::get($sql);
    }

    public static function getEmailFornecedorEquipmento($fornecedor)
    {

        $sql = <<<SQL
SELECT
 fornecedor_equipamento_email.*
FROM
  fornecedor_equipamento_email
WHERE
  fornecedor_equipamento_email.fornecedor_id = '{$fornecedor}'
AND 
deleted_at is null
SQL;
        $emails = parent::get($sql);
        return json_encode($emails);
    }

    public static function getFornecedorEquipmentCosts($fornecedor)
    {
        $equipamentos = [];
        $custo_equipamento = [];
        $equip = Equipamentos::getAll();

        foreach ($equip as $e) {
            $equipamentos[$e['id']]['item'] = $e['item'];
            $equipamentos[$e['id']]['valor_aluguel'] = '0.00';
            $equipamentos[$e['id']]['unidade'] = $e['unidade'];
            $equipamentos[$e['id']]['id'] = $e['id'];
        }

        $sql = <<<SQL
SELECT
  cobrancaplanos_id,
  valor_aluguel
FROM
  fornecedores_custo_equipamento
WHERE
  fornecedores_custo_equipamento.fornecedor_id = '{$fornecedor}';
SQL;

        $custo_equipamentos = parent::get($sql);

        if(count($custo_equipamentos) > 0) {
            foreach ($custo_equipamentos as $c) {
                $custo_equipamento[$c['cobrancaplanos_id']]['valor_aluguel'] = $c['valor_aluguel'];
                $custo_equipamento[$c['cobrancaplanos_id']]['cobrancaplanos_id'] = $c['cobrancaplanos_id'];
            }

            foreach ($equipamentos as $id => $equipamento) {
                if(isset($equipamentos[$custo_equipamento[$id]['cobrancaplanos_id']])){
                    $equipamentos[$id]['valor_aluguel'] = $custo_equipamento[$id]['valor_aluguel'];
                }
            }
        }

        return json_encode($equipamentos);
    }

    public static function saveFornecedorEquipmentCosts($data, $fornecedor)
    {
        mysql_query('BEGIN');

        // Verifica a existência de custos do fornecedor no banco
        $sql = <<<SQL
SELECT * FROM fornecedores_custo_equipamento WHERE fornecedor_id = '{$fornecedor}';
SQL;
        $custos = parent::get($sql);

        // Se existir custos, remove-os para dar lugar aos novos
        if (count($custos) > 0) {
            $sql = <<<SQL
DELETE FROM fornecedores_custo_equipamento WHERE fornecedor_id = '{$fornecedor}'
SQL;
            $rs = mysql_query($sql);

            // Insere os custos antigos na tabela de histórico
            $sqlHist = <<<SQL
INSERT INTO historico_fornecedores_custo_equipamento 
(
  id,
  fornecedor_id,
  cobrancaplanos_id,
  valor_aluguel,
  data_mudanca
) VALUES 
SQL;
            foreach ($custos as $c) {
                // Insere na tabela de histórico somente os valores que foram atualizados
                if ($data[$c['cobrancaplanos_id']] != $c['valor_aluguel']) {
                    $sqlHist .= <<<SQL
(NULL, '{$fornecedor}', '{$c['cobrancaplanos_id']}', '{$c['valor_aluguel']}', NOW()),
SQL;
                }
            }
            $sqlHist = substr_replace($sqlHist, ';', -1, 1);
            $rs = mysql_query($sqlHist);
        }

        // Grava os novos custos na tabela
        $sql = <<<SQL
INSERT INTO fornecedores_custo_equipamento
(
  id,
  cobrancaplanos_id,
  fornecedor_id,
  valor_aluguel
) VALUES
SQL;

        foreach ($data as $cobrancaplanos_id => $valor_aluguel) {
            $sql .= <<<SQL
(NULL, '{$cobrancaplanos_id}', '{$fornecedor}', '{$valor_aluguel}'),
SQL;
        }
        $sql = substr_replace($sql, ';', -1, 1);
        $rs = mysql_query($sql);
        if ($rs) {
            mysql_query('COMMIT');
            return '1';
        }
        mysql_query('ROLLBACK');
        return false;
    }

    public static function deleteByID($id)
    {
        $sql = <<<SQL
        DELETE
        FROM
        fornecedores
        WHERE
        idFornecedores = {$id}
SQL;
        $r = mysql_query($sql);
        if (!$r) {
            echo "Erro ao deletar fornecedor";
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));
        return 1;
    }

    public static function getFornecedorByCPFCNPJ($CPFCNPJ, $CPFCNPJFiltered)
    {
        $sql = <<<SQL
SELECT
	fornecedores.*
FROM
	fornecedores
WHERE
	(
	  fornecedores.CPF = '{$CPFCNPJ}'
	  OR fornecedores.cnpj = '{$CPFCNPJ}'
    )
    OR 
    (
	  fornecedores.CPF = '{$CPFCNPJFiltered}'
	  OR fornecedores.cnpj = '{$CPFCNPJFiltered}'
    )
SQL;
        return current(parent::get($sql, 'assoc'));
    }

    public static function salvarEmailFornecedorEquipamento($fornecedor, $novoEmail)
    {
        mysql_query('begin');
        $sql =
            "Insert into 
        fornecedor_equipamento_email
        
        (
            fornecedor_id,
            email,
            created_at,
            created_by
        )
        
        VALUES ";

        $quantidadeEmail = count($novoEmail);
        $aux = 1;

        foreach ($novoEmail as $email){
            $sql .= "
            (
            {$fornecedor},
            '{$email}',
            now(),
            {$_SESSION['id_user']}
            )";

            if($aux < $quantidadeEmail ){
                $sql .= ",";
            }
            $aux++;
        }

        $r = mysql_query($sql);
        if (!$r) {
            echo "Erro: Ao salvar e-mail para fornecedor.";
            return;
        }
        mysql_query('commit');
        return 1;
    }

    public static function deletarEmailFornecedorEquipamento($fornecedorEquipamentoEmailId)
    {
        mysql_query('begin');
        $sql =
            "update 
        fornecedor_equipamento_email
        set 
        deleted_at = now(),
        deleted_by =  {$_SESSION['id_user']}
        WHERE 
        id = $fornecedorEquipamentoEmailId        
       ";
        $r = mysql_query($sql);
        if (!$r) {
            echo "Erro: Ao deletar e-mail para fornecedor.";
            return;
        }
        mysql_query('commit');
        return 1;

    }

    public static function getClasseValor($fornecedorId)
    {
        $sql = <<<SQL
SELECT 
  classe_valor_id 
FROM 
  fornecedores_classe_valor
WHERE 
  fornecedor_id = '{$fornecedorId}'
SQL;
        $data = current(parent::get($sql));

        if(count($data) > 0) {
            return $data['classe_valor_id'];
        }
        return false;
    }

    public static function getAllByTipo($tipo)
    {
        $sql = "SELECT fornecedores.*, COALESCE(razaoSocial, nome) AS fornecedor FROM fornecedores WHERE TIPO = '{$tipo}' ORDER BY razaoSocial";
        return parent::get($sql, 'Assoc');
    }

    public static function getAllAtivosByTipo($tipo)
    {
        $sql = "SELECT fornecedores.*, COALESCE(razaoSocial, nome) AS fornecedor FROM fornecedores WHERE TIPO = '{$tipo}' AND ATIVO = 'S' ORDER BY razaoSocial";
        return parent::get($sql, 'Assoc');
    }

    public static function getPosicaoFornecedores($filtros)
    {
        $response = [];
        $condTipoBusca = '';
        $condFornecedoresClientes = '';
        if(isset($filtros['tipo_busca']) && !empty($filtros['tipo_busca'])){
            $condTipoBusca = "AND f.TIPO = '{$filtros['tipo_busca']}'";
        }

        if(isset($filtros['fornecedores_clientes']) && !empty($filtros['fornecedores_clientes'])){
            $fornecedores = implode(', ', $filtros['fornecedores_clientes']);
            $condFornecedoresClientes = "AND n.codFornecedor IN ({$fornecedores})";
        }

        $sql = <<<SQL
select 
    n.codFornecedor, 
    f.cnpj, 
    f.CPF, 
    coalesce(f.razaoSocial, f.nome) as nome_fornecedor,
    ROUND(SUM(p.valor), 2) AS saldo
from 
    notas as n inner join 
    fornecedores as f on (f.idFornecedores = n.codFornecedor) inner join
    parcelas as p on (n.idNotas = p.idNota)
where
    p.status = 'Pendente' AND
    n.status <> 'Cancelada'
    AND p.nota_id_fatura_aglutinacao IS NULL
    AND n.nota_aglutinacao = 'n'
    AND n.tipo_documento_financeiro_id != 25
    {$condFornecedoresClientes}
    {$condTipoBusca}
Group by 
    n.codFornecedor
order by 
    nome_fornecedor ASC
SQL;

        $response['saldos'] = parent::get($sql);
        $response['notas'] = [];
        if(!empty($response['saldos'])){
            if($filtros['modo'] == 'a') {
                $notas = self::getNotasSaldoFornecedoresClientes(
                    array_map(function ($value) {
                        return $value['codFornecedor'];
                    }, $response['saldos'])
                );
                foreach ($notas as $nota) {
                    $response['notas'][$nota['codFornecedor']][$nota['data_emissao'] . '|' . $nota['idNota']][] = $nota;
                }
            }
        }
        
        return $response;
    }

    private static function getNotasSaldoFornecedoresClientes($fornecedores_clientes)
    {
        $fornecedores_clientes = implode(', ', $fornecedores_clientes);
        $sql = <<<SQL
SELECT
    p.idNota,
    n.codFornecedor,
    n.dataEntrada AS data_emissao,
    n.codigo,
    e.nome AS filial,
    p.valor AS valor_parcela,
    p.vencimento_real,
    CONCAT(pc.COD_N4, ' - ', pc.N4) AS natureza
FROM
    notas AS n 
    INNER JOIN parcelas AS p ON p.idNota = n.idNotas
    LEFT JOIN empresas AS e ON e.id = n.empresa
    LEFT JOIN plano_contas AS pc ON pc.ID = n.natureza_movimentacao
WHERE
    p.status = 'Pendente'
    AND n.status <> 'Cancelada'
    AND p.nota_id_fatura_aglutinacao IS NULL
    AND n.nota_aglutinacao = 'n'
    AND n.tipo_documento_financeiro_id != 25
    AND n.codFornecedor IN ({$fornecedores_clientes})
SQL;
        return parent::get($sql);
    }

    public static function getTipoChavePix($id)
    {
        $condId = empty($id) ? '' : " where id = {$id}";       
        $sql = <<<SQL
SELECT
    *
FROM
    tipo_chave_pix
    {$condId}
    
SQL;
        return parent::get($sql);
    }
}
