<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class MultiplasNotas extends AbstractModel
{
    
    public static function criar($dadosNotas, $datas, $rateio)
    {       

        $tipoNota = $dadosNotas['tipo_nota'] == 'S' ? '1' : '0';
        $responseTipoDocumento = TipoDocumentoFinanceiro::getById($dadosNotas['tipo_documento_nota']);
        $tipoDocumento = $responseTipoDocumento[0]['sigla'];
        $aux = 1;
        $arrayNotas = [];
        mysql_query('BEGIN');
        
        foreach($datas as $data){       

        $sql = "
INSERT notas
(
 notas.USUARIO_ID, 
 notas.idNotas, 
 notas.CHAVE_ACESSO, 
 notas.codigo, 
 notas.codNatureza, 
 notas.codCentroCusto, 
 notas.valor, 
 notas.dataEntrada, 
 notas.descricao, 
 notas.codFornecedor, 
 notas.tipo, 
 notas.status, 
 notas.codSubNatureza, 
 notas.empresa, 
 notas.ICMS, 
 notas.FRETE, 
 notas.SEGURO, 
 notas.VAL_PRODUTOS, 
 notas.DES_ACESSORIAS, 
 notas.IPI, 
 notas.ISSQN, 
 notas.DESCONTO_BONIFICACAO, 
 notas.CHAVE, 
 notas.TR, 
 notas.TIPO_DOCUMENTO, 
 notas.PIS, 
 notas.COFINS, 
 notas.CSLL, 
 notas.IR, 
 notas.data, 
 notas.PCC, 
 notas.INSS, 
 notas.NOTA_REFERENCIA, 
 notas.IMPOSTOS_RETIDOS_ID, 
 notas.visualizado, 
 notas.visualizado_por, 
 notas.nota_fatura, 
 notas.tipo_documento_financeiro_id, 
 notas.natureza_movimentacao,
 notas.nota_adiantamento,
 notas.inserida_caixa_fixo,
 notas.data_competencia,
 forma_pagamento
) VALUES
(
 '{$_SESSION['id_user']}',
 NULL,
 '',
 '{$dadosNotas['numero_nota']}',
 0,
 1,
 '{$dadosNotas['valor']}',
 '{$data['dataEmissao']}',
 '{$dadosNotas['descricao']}. CRIADA PELA MULTIPLAS NOTAS',
 '{$dadosNotas['fornecedor_nota']}',
 '{$tipoNota}',
 'Em aberto',
 0,
 '{$dadosNotas['ur_nota']}',
 0,
 0,
 0,
 '{$dadosNotas['valor']}',
 0,
 0,
 0,
 0,
 '',
 0,
 '{$tipoDocumento}',
 0,
 0,
 0,
 0,
 NOW(),
 0,
 0,
 0,
 0,
 1,
 '{$_SESSION['id_user']}',
 0,
 '{$dadosNotas['tipo_documento_nota']}',
 '{$dadosNotas['natureza_nota']}',
 'N',
 'N',
 '{$data['competencia']}',
 '{$dadosNotas['forma_pagamento']}'
)
";

        $rs = mysql_query($sql);
        
        
        if(!$rs) {            
            mysql_query('ROLLBACK');            
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao gerar Nota'

            ];
            return $response;
        }
        $notaId = mysql_insert_id();
        $codigo = " CONCAT(codigo)";
        
        $codigo = " CONCAT(codigo, ' MULT-', '{$aux}')";
        
        $aux++;
        $arrayNotas[] = $notaId;
        $sql = <<<SQL
UPDATE notas 
SET 
    TR = '{$notaId}',
    codigo = {$codigo}
WHERE
    idNotas = '{$notaId}' 
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao atualizar Nota'

            ];
            return $response;
            
        }
$sql = <<<SQL
INSERT INTO parcelas
(
 id,
 idNota,
 valor,
 vencimento,
 codBarras,
 origem,
 aplicacao,
 obs,
 status,
 comprovante,
 empresa,
 JurosMulta,
 desconto,
 encargos,
 TR,
 VALOR_PAGO,
DATA_PAGAMENTO,
 OUTROS_ACRESCIMOS,
 NUM_DOCUMENTO,
 ISSQN,
 PIS,
 COFINS,
 CSLL,
 IR,
 CONCILIADO,
 DATA_PROCESSADO,
 USUARIO_ID,
 DATA_CONCILIADO,
 DATA_DESPROCESSADA,
 DESPROCESSADA_POR,
 JUSTIFICATIVA_DESPROCESSADA,
 PARCELA_ORIGEM,
 motivo_juros_id,
 nota_id_fatura_aglutinacao,
 vencimento_real

) 
VALUES 
(
 NULL,
 '{$notaId}',
 '{$dadosNotas['valor']}',
 '{$data['vencimento']}',
 '{$dadosNotas['cod_barras']}',
 '{$dadosNotas['origem']}',
 '{$dadosNotas['forma_pagamento']}',
 '',
 'Pendente',
 '',
 '{$dadosNotas['ur_nota']}',
 0,
 0,
 0,
 1,
 '{$dadosNotas['valor']}',
 null,
 0,
 '',
 0,
 0,
 0,
 0,
 0,
 'N',
 '',
 '{$_SESSION['id_user']}',
 '',
 '',
 0,
 '',
 0,
 NULL,
 NULL,
 '{$data['vencimento_real']}'
)
SQL;
        $rs = mysql_query($sql) ;

        if(!$rs) {
            mysql_query('ROLLBACK');
            echo $sql;
            die();
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao gerar Parcela'

            ];
            return $response;
            
        }

       
 foreach($rateio as $r){
        $sql = <<<SQL
INSERT INTO rateio
(
 ID, 
 NOTA_ID, 
 NOTA_TR, 
 CENTRO_RESULTADO, 
 VALOR, 
 PORCENTAGEM, 
 IPCG3_ANTIGA, 
 IPCG4_ANTIGA, 
 ASSINATURA, 
 IPCF2, 
 IPCG3, 
 IPCG4
) 
 VALUES
(
 NULL,
 '{$notaId}',
 '{$notaId}',
 '{$r['centro_resultado']}',
 '{$r['valor']}', 
 '{$r['percentual']}',
 '',
 '',
 '{$r['assinatura']}',
 '{$r['ipcf2']}',
 '{$r['ipcg3']}',
 '{$r['ipcg4']}'
)
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao gerar Rateio'

            ];
            return $response;
            
        }
    }
    }
        mysql_query('COMMIT');
        $response = ['tipo' => 'acerto',
            'msg'=>$arrayNotas

            ];
            return $response;
       
    }

    

    
    
}
