<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class Assinatura extends AbstractModel implements DAOInterface
{

  public static function getAll()
  {
    return parent::get(self::getSQL());
  }

  public static function getById($id)
  {
    return parent::get(self::getSQL($id));
  }

  public static function getSQL($id = null)
  {

    $id = isset($id) && !empty($id) ? 'WHERE 1=1 AND ID = ' . $id : null;
    return "SELECT *
            FROM
            assinaturas
            {$id}
            ORDER BY DESCRICAO ASC";
  }

  public static function getInvestimento($id = null)
  {

    
    $sql = "SELECT *
            FROM
            assinaturas
            where
            INVESTIMENTO = 'S'
            ORDER BY DESCRICAO ASC";
            return parent::get($sql);
  }
    

} 