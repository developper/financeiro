<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class OrcamentoGerencial extends AbstractModel
{

	public static function getTopicosByIdPai($id)
    {
		
		$sql = "
		Select 
		* 
		from 
		orcamento_gerencial_topicos 
		where
		id_pai = {$id}
		order by letra, ordem ASC
		";
		
        return parent::get($sql, 'Assoc');
	}
	
    public static function getTopicos(){
        
		
		$sql = "
		Select 
		* 
		from 
		orcamento_gerencial_topicos
		order by letra, ordem ASC
		";
		
        return parent::get($sql, 'Assoc');
	}

	public static function getValoresTopicosByVersao($versao){
        
		
		$sql = "
		Select 
		* 
		from 
		orcamento_gerencial_topicos_valores
		where
		versao_id = {$versao}
		";
		
        return parent::get($sql, 'Assoc');
	}

	public static function getValoresPlanoContasByVersao($versao){
        
		
		$sql = "
		Select 
		o.*,
		p.COD_N4,
		p.N4 
		from 
		orcamento_gerencial_plano_contas_valores as o 
		inner join plano_contas as p on (o.plano_contas_id = p.ID)
		where
		versao_id = {$versao}
		";
		
		
        return parent::get($sql, 'Assoc');
	}

	public static function getVersaoOrcamentoById($id){
        
		
		$sql = "
		Select 
		* 
		from 
		orcamento_gerencial_versao 

		where
		id = {$id}
		";
		
        return parent::get($sql, 'Assoc');
	}

    public static function getNovoTopicosByIdPai($id)
    {

        $sql = "
		Select 
		* 
		from 
		orcamento_gerencial_topicos 
		where
		id_pai = {$id}
		order by letra, ordem ASC
		";

        return parent::get($sql, 'Assoc');
    }
    
}
