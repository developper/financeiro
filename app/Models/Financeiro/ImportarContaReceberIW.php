<?php

namespace App\Models\Financeiro;

use App\Models\AbstractModel;


class ImportarContaReceberIW extends AbstractModel
{
    public static function verificarNFJaExiste($cnpj, $codDocumento)
    { 
        $sql =  "SELECT 
            *
            from
            notas
            INNER JOIN fornecedores on (notas.codFornecedor = fornecedores.idFornecedores)
            WHERE
            (cnpj = '{$cnpj}' OR  REPLACE(REPLACE(REPLACE(cnpj,'/',''),'.',''),'-','') = '{$cnpj}') 
            and 
            LPAD(notas.codigo, 255, '0') = LPAD('{$codDocumento}', 255, '0') AND `STATUS` <> 'Cancelada' ";
            
            

		return current(parent::get($sql));
	}

    public static function inserirNotas($dados, $empresa) 
    {
                
         foreach($dados as $d){
           
            mysql_query('BEGIN');

            $sql = "INSERT notas
    (
     notas.USUARIO_ID, 
     notas.idNotas, 
     notas.CHAVE_ACESSO, 
     notas.codigo, 
     notas.codNatureza, 
     notas.codCentroCusto, 
     notas.valor, 
     notas.dataEntrada, 
     notas.data_competencia, 
     notas.descricao, 
     notas.codFornecedor, 
     notas.tipo, 
     notas.status, 
     notas.codSubNatureza, 
     notas.empresa, 
     notas.ICMS, 
     notas.FRETE, 
     notas.SEGURO, 
     notas.VAL_PRODUTOS, 
     notas.DES_ACESSORIAS, 
     notas.IPI, 
     notas.ISSQN, 
     notas.DESCONTO_BONIFICACAO, 
     notas.CHAVE, 
     notas.TR, 
     notas.TIPO_DOCUMENTO, 
     notas.PIS, 
     notas.COFINS, 
     notas.CSLL, 
     notas.IR, 
     notas.DATA, 
     notas.PCC, 
     notas.INSS, 
     notas.NOTA_REFERENCIA, 
     notas.IMPOSTOS_RETIDOS_ID, 
     notas.visualizado, 
     notas.visualizado_por, 
     notas.nota_fatura, 
     notas.tipo_documento_financeiro_id, 
     notas.natureza_movimentacao,
     notas.nota_aglutinacao,
     notas.importada_iw,
     notas.forma_pagamento

    ) VALUES
    (
     '{$_SESSION['id_user']}',
     NULL,
     '',
     '{$d['num_documento']}',
     0,
     1,
     '{$d['valor_total']}',
     '{$d['data_emissao']}',
     '{$d['data_competencia']}',
     '{$d['descricao']}',
     '{$d['fornecedor_id']}',
     '0',
     'Em aberto',
     0,
     '{$empresa}',
     0,
     0,
     0,
     '{$d['valor_servico']}',
     0,
     0,
     0,
     0,
     '',
     0,
     'FT',
     0,
     0,
     0,
     0,
     NOW(),
     0,
     0,
     0,
     0,
     1,
     '{$_SESSION['id_user']}',
     0,
     '{$d['tipo_documento_financeiro']}',
     '{$d['natureza_principal']}',
     'N',
     'S',
     '{$d['forma_pagamento']}'
    )
    ";
            $rs = mysql_query($sql);
            if(!$rs) {
                mysql_query('ROLLBACK');
                 $response['error'][] = "Erro ao inserir nota da linha {$d['linha']}";
                 return $response;
            }

            $notaId = mysql_insert_id();
            $notasInseridas[] = $notaId;
            //Gerando amarracao do lote gerado no sistema meio com a NF e o id vai ser o numero do documento da nota
            $sql = "Insert
            notas_lote_conta_receber
            (
                id,
                nota_id,
                lote
            )
            values 
            (
                null,
                {$notaId},
                {$d['lote']}
                
            )";
            $rs = mysql_query($sql);
            if(!$rs) {
                mysql_query('ROLLBACK');
                 $response['error'][] = "Erro ao inserir nota_lote da linha {$d['linha']}";
                 return $response;
            }

            $numDoc = mysql_insert_id();
    
            $sql = "    UPDATE notas 
    SET 
        TR = '{$notaId}',
        codigo = '{$numDoc}'
    WHERE
        idNotas = '{$notaId}' 
    ";
            $rs = mysql_query($sql);
            if(!$rs) {
                mysql_query('ROLLBACK');
                 $response['error'][] = "Erro ao atualizar TR da linha {$d['linha']}";
                 return $response;
            }
            $trParcela = 1;
            foreach($d['parcelas'] as $p){
                $sql = <<<SQL
INSERT INTO parcelas
(
 id,
 idNota,
 valor,
 vencimento,
 codBarras,
 origem,
 aplicacao,
 obs,
 status,
 comprovante,
 empresa,
 JurosMulta,
 desconto,
 encargos,
 TR,
 VALOR_PAGO,
 DATA_PAGAMENTO,
 OUTROS_ACRESCIMOS,
 NUM_DOCUMENTO,
 ISSQN,
 PIS,
 COFINS,
 CSLL,
 IR,
 CONCILIADO,
 DATA_PROCESSADO,
 USUARIO_ID,
 DATA_CONCILIADO,
 DATA_DESPROCESSADA,
 DESPROCESSADA_POR,
 JUSTIFICATIVA_DESPROCESSADA,
 PARCELA_ORIGEM,
 motivo_juros_id,
 nota_id_fatura_aglutinacao,
vencimento_real
) 
VALUES 
(
 NULL,
 '{$notaId}',
 '{$p['valor']}',
 '{$p['vencimento']}',
 '',
 0,
 0,
 '',
 'Pendente',
 '',
 '{$empresa}',
 0,
 0,
 0,
 {$trParcela},
 0,
 '',
 0,
 '',
 0,
 0,
 0,
 0,
 0,
 'N',
 '',
 '{$_SESSION['id_user']}',
 '',
 '',
 0,
 '',
 0,
 NULL,
 NULL,
 '{$p['vencimento_real']}'
)
SQL;
        $rs = mysql_query($sql) or die(mysql_error());
        if(!$rs) {
            mysql_query('ROLLBACK');
             $response['error'][] = "Erro ao inserir parcela da linha {$d['linha']}";
             return $response;
        }    

        $trParcela++;  
}
    $somaRateio = 0;
            foreach($d['rateio'] as $r){
                $ipcfRateio = IPCF::getById($d['natureza']);
                $sql = " INSERT INTO rateio
            (
             ID, 
             NOTA_ID, 
             NOTA_TR, 
             CENTRO_RESULTADO, 
             VALOR, 
             PORCENTAGEM, 
             IPCG3_ANTIGA, 
             IPCG4_ANTIGA, 
             ASSINATURA, 
             IPCF2, 
             IPCG3, 
             IPCG4
            ) 
             VALUES
            (
             NULL,
             '{$notaId}',
             '{$notaId}',
             '{$empresa}',
             '{$r['valor']}',
             '{$r['percentual']}',
             '',
             '',
             '{$r['assinatura']}',
             '{$ipcfRateio['N2']}',
             '{$ipcfRateio['N3']}',
             '{$r['natureza']}'
            )
            ";
                    $rs = mysql_query($sql);
                    if(!$rs) {
                        mysql_query('ROLLBACK');
                         $response['error'][] = "Erro ao inserir rateio da linha {$d['linha']}";
                         return $response;
                    }

                    $somaRateio += $r['percentual'];                    
                    $idTabelaRateio = mysql_insert_id();

                    
            
            }

            if($somaRateio > 100){
                $dif = 100 - $somaRateio;
                $sql = "update rateio set PORCENTAGEM = PORCENTAGEM + {$dif} where ID = $idTabelaRateio ";
              $r = mysql_query($sql);
              if(!$r){
                 
                  mysql_query("rollback");
                   $response['error'][] = "Erro ao atualizar percentual do rateio da linha {$d['linha']}";
                   return $response;
              }
          }   
            
                    mysql_query('COMMIT');
    
                    $response['insert'][]= "Linha {$d['linha']}; N. Doc: {$d['lote']}; CNPJ:{$d['cnpj']}. Gerou TR: $notaId";
        }

        return $response;
         }
    }

        

    

    
