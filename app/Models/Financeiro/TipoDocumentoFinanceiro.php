<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class TipoDocumentoFinanceiro extends AbstractModel implements DAOInterface
{

    public static function getAll()
    {
        return parent::get(self::getSQL());
    }

    public static function getById($id)
    {
        return parent::get(self::getSQL($id), 'Assoc');
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? 'WHERE 1=1 AND tipo_documento_financeiro.id = ' . $id : null;
        return "SELECT *
            FROM tipo_documento_financeiro
            {$id}
            ORDER BY sigla ASC";
    }

    public static function getByFiltros($filtros)
    {
        
        $condTipo = isset($filtros['tipo_lancamento'])  ? " and tipo_lancamento = '{$filtros['tipo_lancamento']}'" : '';
        $condProcessar = isset($filtros['adiantamento'])  ? " and adiantamento = '{$filtros['adiantamento']}'" : '';
        $condNotIn = isset($filtros['notIn']) ? " and id not in ({$filtros['notIn']})" : '';
        $condIdIn = isset($filtros['idIn']) && !empty($filtros['idIn']) ? " and id  in ({$filtros['idIn']}) " : '';

        $sql = <<<SQL
            SELECT 
            *
            FROM 
            tipo_documento_financeiro
          where
          1 = 1 
          {$condTipo}
          {$condProcessar}
          {$condNotIn}
          {$condIdIn}
            ORDER BY sigla ASC
SQL;


        return parent::get($sql);
    }

    public static function getTipoByParcelaId($parcela_id)
    {
        $sql = <<<SQL
SELECT 
 tipo_documento_financeiro.* 
FROM 
 parcelas
 INNER JOIN notas ON parcelas.idNota = notas.idNotas 
 INNER JOIN tipo_documento_financeiro ON notas.tipo_documento_financeiro_id = tipo_documento_financeiro.id 
WHERE
 parcelas.id = '{$parcela_id}'
SQL;
        return current(parent::get($sql, 'Assoc'));
    }

} 