<?php

namespace App\Models\Financeiro;

use App\Helpers\StringHelper;
use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class Compensacao extends AbstractModel
{
    public static function getById($id)
    { $sql = "
        Select 
        *
        from
        nota_compensacao 
        where
        id ={$id}
        ";
        return parent::get($sql, 'assoc');
        

    }

    public static function pesquisarParcelaByFiltros($filtros)
    {
        $condVencimento ='';
        $condFornecedor = '';
        $condTipoDocFinanceiro = '';
        $condUr = '';
        $condTr = '';
        $condStatus = '';
        if(!empty($filtros['inicio']) && !empty($filtros['fim'])){
            $condVencimento = "AND p.vencimento_real between '{$filtros['inicio']}' AND '{$filtros['fim']}'" ;
        }
        if(!empty($filtros['tr'])){
            $condTr = "AND p.idNota = '{$filtros['tr']}'" ;
        }
        if(!empty($filtros['fornecedor'])){
            $condFornecedor = "AND n.codFornecedor = '{$filtros['fornecedor']}'" ;
        }
        if(!empty($filtros['tipo_documento_financeiro'])){
            $condTipoDocFinanceiro = "AND n.tipo_documento_financeiro_id = '{$filtros['tipo_documento_financeiro']}'" ;
        }else{
            // caso a parcela a compensar seja PA ou RA as parcelas não podem ser de antecipação
            if($filtros['nota_adiantamento'] == 'S'){
                $condTipoDocFinanceiro = "AND t.adiantamento ='N'" ;                
            }else{
                $condTipoDocFinanceiro = "AND t.adiantamento ='S'" ; 
            }
        }

        // Se parcela a compensar for antecipada deve trazer parcelas pendentes 
        // caso contrario a parcela antecipada pode ser processada
        if($filtros['nota_adiantamento'] == 'S'){
            $condStatus = "AND p.status ='Pendente'" ;
        }

        if($filtros['tipo_nota'] == 0 || $filtros['tipo_nota'] == 1){
           
            $condTipoNota = "AND n.tipo = '{$filtros['tipo_nota']}'" ;
        }
        if(!empty($filtros['empresa_user'])){
            $condUr = " AND n.empresa in {$filtros['empresa_user']}";
        }

        $sql = "select 
        n.codigo,
        n.dataEntrada,       
        p.*,
        p.TR AS num_parcela,
        p.vencimento as data_vencimento,
        p.vencimento_real as data_vencimento_real,
        coalesce(f.razaoSocial, f.nome) as nome_fornecedor,
        e.nome as ur,
        p.status as pst,         
        p.valor as valor_pagar,
        t.sigla,
        p.id as id_parcela,
        p.idNota as id_nota
    from 
        notas as n inner join 
        fornecedores as f on (f.idFornecedores = n.codFornecedor) inner join
        empresas as e on (n.empresa = e.id) inner join 
        parcelas as p on (n.idNotas = p.idNota) inner join 
        tipo_documento_financeiro as t on (n.tipo_documento_financeiro_id = t.id)    
    where
    
    n.status <> 'Cancelada'
    AND p.status <> 'Cancelada'
    AND p.compensada <> 'TOTAL'
    {$condStatus}   
    {$condUr}
    {$condVencimento}
    {$condFornecedor}
    {$condTipoDocFinanceiro}  
    {$condTipoNota}
    {$condTr}
    Group by 
        idNOTAS,p.id,p.tr
    order by 
    data_vencimento, nome_fornecedor ASC";

        return parent::get($sql, 'assoc');
    }

    public static function getNotaByParcelaId($id)
    {
        $sql = "
        select 
        p.*,  
        IF(n.tipo = 0,'RECEBIMENTO','DESEMBOLSO') as tipo_NF,
        DATE_FORMAT(p.vencimento,'%d/%m/%Y') as vencimento_parcela,
        DATE_FORMAT(p.vencimento_real,'%d/%m/%Y') as vencimento_real_parcela,
        coalesce(f.razaoSocial, f.nome) as nome_fornecedor,
        e.nome as ur,
        n.status as pst,     
        t.sigla,
        pl.N4,
        p.CONCILIADO,
        n.dataEntrada,
        n.codigo,
        n.tipo,
        n.nota_adiantamento
            
    from 
        notas as n inner join 
        fornecedores as f on (f.idFornecedores = n.codFornecedor) inner join
        empresas as e on (n.empresa = e.id) inner join 
        tipo_documento_financeiro as t on (n.tipo_documento_financeiro_id = t.id) inner join
        plano_contas as pl on (n.natureza_movimentacao = pl.ID) inner join 
        parcelas as p on (n.idNotas = p.idNota) 
       
    where   
        p.id = {$id}      
    Group by 
       p.id
    order by 
    n.dataEntrada, nome_fornecedor ASC";
    

    return parent::get($sql, 'assoc');

    }

    public function salvarCompensacao($data)
    {
        $auxAdiantamento =  $data['parcelaCompensar']['adiantamento'] == 'S' ? 'S' : 'N';
        $parcelaPivo = $data['parcelaCompensar'];
        $parcelas = $data['parcelas']; 
        $valorCompensarNota = 0;   
        mysql_query('BEGIN');
    
        
        foreach($parcelas as $p){
           // montando historico das compensações
           $p['valor'] = StringHelper::moneyStringToFloat($p['valor']);
                $sql = "
                Insert into
                nota_compensacao
                (
                    parcela_pivo_id,
                    parcela_id,
                    valor,                
                    data_compensado,
                    created_by,
                    created_at                
                )
                values
                (
                    '{$parcelaPivo['idParcela']}',
                    '{$p['idParcela']}',
                    '{$p['valor']}',
                     now(),
                     '{$_SESSION['id_user']}',
                     now()

                )";  

                $valorCompensarNota += $p['valor'];
                $rs = mysql_query($sql);

                if(!$rs) {
                    mysql_query('ROLLBACK');
                    $response = ['tipo' => 'erro',
                          'msg'=>"Erro a inserir compensação"
                        ];                    
                    return $response;
                }

              // Atualizando parcela que tá compensando a pivo  

                $sql = "update
                parcelas,
                notas 
                set
                parcelas.valor_compensada = (valor_compensada + {$p['valor']}),
                parcelas.compensada =  'TOTAL',
                parcelas.status = 'Processada',
                parcelas.DATA_PAGAMENTO = if(parcelas.DATA_PAGAMENTO is null or parcelas.DATA_PAGAMENTO = '0000-00-00' or parcelas.DATA_PAGAMENTO = '', notas.dataEntrada, DATA_PAGAMENTO)
                where
                parcelas.id = '{$p[idParcela]}' and notas.idNotas = parcelas.idNota
                ";
                $rs = mysql_query($sql);

                if(!$rs) {
                    mysql_query('ROLLBACK');
                    $response = ['tipo' => 'erro',
                          'msg'=>"Erro ao atualizar parcela.".$sql
                        ];                    
                    return $response;
                }

            // Atualizando nota da parcela que tá compensando a pivo. 
            $responseNota = self::getNotaByParcelaId($p['idParcela']);
            $responseNota = $responseNota[0];

                $statusNota = 'Processada';
                $check = Parcela::checkIfNotaHasParcelasPendentes($p['nota']);
                if($check) {
                    $statusNota = 'Pendente';
                }

                if($responseNota['nota_adiantamento'] == 'S'){    
                    $statusNota = 'Processada';                                  
                }

                
                
                    $sql = "
                    UPDATE 
                    notas 
                    SET 
                    status = '{$statusNota}'
                    WHERE idNotas = '{$p[nota]}'
                    ";
                    $rs = mysql_query($sql);
                    if(!$rs) {
                        $response = ['tipo' => 'erro',
                          'msg'=>"Erro ao atualizar nota."
                        ]; 
                        return $response; 
                    }

                // atualizando parcela Pivo
                $sql = "update
                parcelas as p,
                notas as n
                set
                
                p.compensada = IF( p.valor = p.valor_compensada + {$p['valor']},'TOTAL', 'PARCIAL'),
                p.valor_compensada = (p.valor_compensada + {$p['valor']}),
                p.status = 'Processada',
                p.DATA_PAGAMENTO = if(p.DATA_PAGAMENTO is null or p.DATA_PAGAMENTO = '0000-00-00' or p.DATA_PAGAMENTO = '', n.dataEntrada, p.DATA_PAGAMENTO)
                
                where
                p.id = {$parcelaPivo[idParcela]} and n.idNotas = p.idNota
                ";
                $rs = mysql_query($sql);

                if(!$rs) {
                    mysql_query('ROLLBACK');
                    $response = ['tipo' => 'erro',
                          'msg'=>"Erro ao atualizar parcela pivo."
                        ];                    
                    return $response;
                }
// Atualizando nota da parcela que tá compensando a pivo. 
$responseNota = self::getNotaByParcelaId($p['idParcela']);
$responseNota = $responseNota[0];
                 // Atualizando nota da parcela pivo. 
                 $statusNota = 'Processada';
                 $check = Parcela::checkIfNotaHasParcelasPendentes($p['idParcela']);
                 if($check) {
                     $statusNota = 'Pendente';
                 }   

                 if($responseNota['nota_adiantamento'] == 'S'){    
                    $statusNota = 'Processada';                                  
                }
                 
                 



                     $sql = "
                     UPDATE 
                     notas 
                     SET 
                     status = '{$statusNota}'
                     WHERE idNotas = '{$parcelaPivo['idNota']}'
                     ";
                     $rs = mysql_query($sql);
                     if(!$rs) {
                         $response = ['tipo' => 'erro',
                           'msg'=>"Erro ao atualizar nota Pivo."
                         ]; 
                         return $response; 
                     }                         
                

            
        }
        mysql_query('COMMIT');
        $response = ['tipo' => 'acerto',
            'msg'=>$parcelaPivo['idNota']

            ];
            
            return  $response;         


    }

    public static function getCompensacaoByIdParcela($idParcela){
        $sql = "
        select 
        nota_compensacao.*,
        nota_compensacao.valor,
        IF(parcela_pivo_id = {$idParcela}, parcela_id, parcela_pivo_id ) as parcelaId,
        coalesce(f.razaoSocial, f.nome) as nome_fornecedor,
        t.sigla,
        DATE_FORMAT(p.vencimento_real,'%d/%m/%Y') as vencimento_real,
        DATE_FORMAT(p.vencimento,'%d/%m/%Y') as dataEntrada,
        CONCAT(n.codigo, ' - ', p.TR) as codNota
        from
        nota_compensacao inner join
         parcelas as p on ( IF(parcela_pivo_id = {$idParcela}, parcela_id, parcela_pivo_id ) = p.id ) inner join 
         notas as n on (p.idNota = n.idNotas) inner join 
         fornecedores as f on (f.idFornecedores = n.codFornecedor) inner join         
         tipo_documento_financeiro as t on (n.tipo_documento_financeiro_id = t.id) 
        where
       ( parcela_pivo_id = {$idParcela}
        or
        parcela_id = {$idParcela})
        AND 
        deleted_by is null
        ";
        
        return parent::get($sql, 'assoc');

    }   
    

   
    public static function cancelarCompensacao($id)
    {
        mysql_query('BEGIN');

        

        // VERIFICA SE A NOTA EM QUESTÃO NÃO ESTÁ PROCESSADA
        $sql = <<<SQL
update
nota_compensacao
set
deleted_at = now(),
deleted_by = {$_SESSION['id_user']}
where
id = {$id}
SQL;

$rs = mysql_query($sql);
if(!$rs) {
    $response = ['tipo' => 'erro',
    'msg'=>"Erro ao cancelar Compensação."
    ]; 
    return $response; 
}
$responseCompensacao = self::getById($id);
$responseCompensacao = $responseCompensacao[0];
$parcelas = [
    0 => $responseCompensacao['parcela_id'],
    1 => $responseCompensacao['parcela_pivo_id'],
];

$valor = $responseCompensacao['valor'];

foreach($parcelas as $p){

    $sql = "update
                parcelas as p,
                notas as n
                set                
                p.compensada = IF( (p.valor_compensada - {$valor}) = 0  ,'NAO', 'PARCIAL'),
                p.status = IF(p.VALOR_PAGO = 0 or p.VALOR_PAGO = 0.00 or ISNULL(p.VALOR_PAGO)  ,'Pendente', 'Processada'),
                p.valor_compensada = (p.valor_compensada - {$valor}),
                p.DATA_PAGAMENTO = IF(n.nota_adiantamento = 's' or n.nota_adiantamento = 'S',  p.DATA_PAGAMENTO, null)
               
                where
                p.id = {$p} and
                p.idNota = n.idNotas
                ";


                
               
                $rs = mysql_query($sql);

                if(!$rs) {
                    mysql_query('ROLLBACK');
                    $response = ['tipo' => 'erro',
                          'msg'=>"Erro ao atualizar parcela."
                        ];                    
                    return $response;
                }

                 // Atualizando nota da parcela pivo. 
                 $responseNota = self::getNotaByParcelaId($p);
                 $responseNota = $responseNota[0];
                
                 $statusNota = 'Em aberto';
                 $check = Parcela::checkIfNotaHasParcelasProcessadas($responseNota['idNota']);
                 if($check && $responseNota['nota_adiantamento'] != 'S') {
                     $statusNota = 'Pendente';
                 }

                 if($responseNota['nota_adiantamento'] == 'S'){    
                    $statusNota = 'Processada';                                  
                }
                 
                     $sql = "
                     UPDATE 
                     notas 
                     SET 
                     status = '{$statusNota}'
                     WHERE
                     idNotas = '{$responseNota['idNota']}'
                     ";

                     $rs = mysql_query($sql);
                     if(!$rs) {
                         $response = ['tipo' => 'erro',
                           'msg'=>"Erro ao atualizar Nota."
                         ]; 
                         return $response; 
                     }

}

            mysql_query('COMMIT');
            $response = [
                'tipo' => 'acerto',
                'msg'=>"Compensação cancelada com sucesso!"
            ];
            
            return  $response;       

    
}
}
