<?php

namespace App\Models\Financeiro;

use App\Helpers\StringHelper;
use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class BaixarNotaAntecipada extends AbstractModel
{
    
    
    static public function salvarBaixa($data)
    {
        mysql_query('BEGIN');
        $responseNota = Notas::getNotaById($data['nota_id_baixa']);       
        if($responseNota){
            // A baixa da nota é uma devolução o tipo de movimentação é diferente do da nota original
            $tipoMovimentacao = $responseNota['tipo'] == 0 ? 1 : 0;
            // tabela plano de contas 
            // Quanado for de RA id 92 natureza 2.1.4.10 DEVOLUCAO DE ADIANTAMENTO quando for tipoMovimentacao =  1 quando conta oroginal é uma RA
            // Quanado for de PA id 167  natureza 1.0.4.08  DEVOLUÇÃO DE PAGAMENTO ANTECIPADO
            $natureza = $tipoMovimentacao == 0 ? 167 : 92;
            $sql = "
            Insert into
            nota_baixas
            (
                `parcela_antecipada_id`, 
                `nota_id`,
                `valor`,
                data_pagamento,
                `tipo_movimentacao`,
                `conta_bancaria_id`,  
                `descricao`,
                `numero_documento`,
                `plano_conta_id`,                
                `conciliado`,
                aplicacao_fundo_id,
                `created_by`,
                `created_at`
                           
            )
            values
            (
                '{$data['parcela_id_baixa']}',
                '{$data['nota_id_baixa']}',
                '{$data['valor']}',
                '{$data['data_pagamento']}',
                '{$tipoMovimentacao}',
                '{$data['origem']}',
                '{$data['descricao']}',
                '{$data['numero_documento']}',
                '{$natureza}',
                'N',
                '{$data['forma_pagamento']}',                                    
                 '{$_SESSION['id_user']}',
                 now()

            )";  
         
            $rs = mysql_query($sql);

            if(!$rs) {
                mysql_query('ROLLBACK');
                $response = ['tipo' => 'erro',
                      'msg'=>"Erro ao inserir baixa. Entre em contato com o TI"
                    ];                    
                return $response;
            }

            $sql = "update
                parcelas 
                set                
                compensada = IF( valor = valor_compensada + {$data['valor']},'TOTAL', 'PARCIAL'),
                valor_compensada = (valor_compensada + {$data['valor']})                
                where
                id = {$data['parcela_id_baixa']}
                ";
                $rs = mysql_query($sql);

                if(!$rs) {
                    mysql_query('ROLLBACK');
                    $response = ['tipo' => 'erro',
                          'msg'=>"Erro ao atualizar Parcela."
                        ];                    
                    return $response;
                }

                mysql_query('COMMIT');

                $response = ['tipo' => 'acerto',
                    'msg'=>'Baixa salva com sucesso!'
        
                    ];
                    
                    return  $response; 
            
        }

         $response = ['tipo' => 'erro',
        'msg'=>"Erro ao encontrar dados da Nota Antecipada. Entre em contato com o TI"
        ];                    
        return $response;              
    }

    public static function getBaixaByIdNota($idNota){
        $sql = "
        select 
        nota_baixas.*,
        CONCAT(o.forma,' - ',tipo_conta_bancaria.abreviacao,' - ',c.AGENCIA,' - ',c.NUM_CONTA,' - ',e.SIGLA_EMPRESA) as nome_conta,
        a.forma       
        from
        nota_baixas  
        INNER JOIN contas_bancarias as c ON (nota_baixas.conta_bancaria_id = c.ID) 
        INNER JOIN origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id)
        INNER JOIN empresas as e ON (c.UR = e.id)
        LEFT JOIN tipo_conta_bancaria ON (tipo_conta_bancaria.id = c.TIPO_CONTA_BANCARIA_ID)
        INNER JOIN aplicacaofundos as a ON (nota_baixas.aplicacao_fundo_id = a.id)
        where
        nota_id = {$idNota} and
        canceled_by is null
        ";
        
        return parent::get($sql, 'assoc');

    }  
    public static function getBaixaById($id){
        $sql = "
        select 
        nota_baixas.*,
        CONCAT(o.forma,' - ',tipo_conta_bancaria.abreviacao,' - ',c.AGENCIA,' - ',c.NUM_CONTA,' - ',e.SIGLA_EMPRESA) as nome_conta,
        a.forma       
        from
        nota_baixas  
        INNER JOIN contas_bancarias as c ON (nota_baixas.conta_bancaria_id = c.ID) 
        INNER JOIN origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id)
        INNER JOIN empresas as e ON (c.UR = e.id)
        LEFT JOIN tipo_conta_bancaria ON (tipo_conta_bancaria.id = c.TIPO_CONTA_BANCARIA_ID)
        INNER JOIN aplicacaofundos as a ON (nota_baixas.aplicacao_fundo_id = a.id)
        where
        nota_baixas.id = {$id}
        ";
        
        return parent::get($sql, 'assoc');

    }   
    

   
    public static function cancelar($id)
    {   
        mysql_query('BEGIN');        

        // VERIFICA SE A NOTA EM QUESTÃO NÃO ESTÁ PROCESSADA
        $sql = <<<SQL
update
nota_baixas
set
canceled_at = now(),
canceled_by = {$_SESSION['id_user']}
where
id = {$id}
SQL;

$rs = mysql_query($sql);
if(!$rs) {
    $response = ['tipo' => 'erro',
    'msg'=>"Erro ao cancelar Baixa.".$sql
    ]; 
    return $response; 
}
$responseBaixa = self::getBaixaById($id);
$responseBaixa = $responseBaixa[0];

$valor = $responseBaixa['valor'];
$parcelaId =  $responseBaixa['parcela_antecipada_id'];

    $sql = "update
                parcelas 
                set                
                compensada = IF( (valor_compensada - {$valor}) = 0  ,'NAO', 'PARCIAL'),
                valor_compensada = (valor_compensada - {$valor})               
                where
                id = {$parcelaId}
                ";                
               
                $rs = mysql_query($sql);

                if(!$rs) {
                    mysql_query('ROLLBACK');
                    $response = ['tipo' => 'erro',
                          'msg'=>"Erro ao atualizar parcela."
                        ];                    
                    return $response;
                }

                

            mysql_query('COMMIT');
            $response = [
                'tipo' => 'acerto',
                'msg'=>"Compensação cancelada com sucesso!"
            ];
            
            return  $response;       

    
    }
}
