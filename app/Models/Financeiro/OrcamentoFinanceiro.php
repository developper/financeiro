<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
	\App\Models\DAOInterface;

class OrcamentoFinanceiro extends AbstractModel
{
	public static  function getAll()
	{
		$sql = "
		Select 
		o.*,
		u.nome as usuario	
		from 
		orcamento_financeiro_versao as o
		inner join usuarios as u on (o.criado_por =u.idUsuarios)
		
		";

		return parent::get($sql, 'Assoc');
	}

	public static  function getMaxVersionEstrutura()
	{
		$sql = "
		Select 
		MAX(versao_estrutura) as versaoEstrutura 
		from 
		orcamento_financeiro_estrutura 
		
		";

		return current(parent::get($sql, 'Assoc'));
	}
	public static function getEstruturaByVersao($id)
	{
		$sql = "
		Select 
		* 
		from 
		orcamento_financeiro_estrutura 
		where
		versao_estrutura = {$id}
		order by  ordem ASC
		";

		return parent::get($sql, 'Assoc');
	}

	public static function gerarOrcamento($versaoEstrutura, $inicio, $fim, $topicos, $arrayData)
	{
		mysql_query('BEGIN');
		$sql = "
            Insert into
            orcamento_financeiro_versao
            (
				inicio,
				fim,
				orcamento_financeiro_versao_estrutura,
				criado_por,
				criado_em                           
            )
            values
            (
                '{$inicio}',
                '{$fim}',
                '{$versaoEstrutura}',                                                                   
                '{$_SESSION['id_user']}',
                 now()

            )";

		$rs = mysql_query($sql);

		if (!$rs) {
			mysql_query('ROLLBACK');
			$response = [
				'tipo' => 'erro',
				'msg' => "Erro ao criar versão. Entre em contato com o TI"
			];
			return $response;
		}

		$idOrcamento = mysql_insert_id();
		$itens = [];

		foreach ($topicos as $t) {

			$topicoId = $t['orcamento_finananceiro_topicos_id'];
			$porcentagem =  $t['aliquota'];

			foreach ($arrayData as $data) {
				$itens[] = "(
							$topicoId,
							$idOrcamento,
							$porcentagem,
							0,
							'" . $data . "',
							'{$_SESSION['id_user']}',
							now()
						)";
			}
		}

		$sql = "
            Insert into
            orcamento_financeiro_topicos_valores
            (
				orcamento_financeiro_topicos,
				orcamento_financeiro_versao_id,
				aliquota,
				valor_mensal,
				data ,
				criado_por,
				criado_em                         
            )
            values
			" . implode(',', $itens);



		$rs = mysql_query($sql);

		if (!$rs) {
			mysql_query('ROLLBACK');
			$response = [
				'tipo' => 'erro',
				'msg' => "Erro ao inserir itens. Entre em contato com o TI"
			];
			return $response;
		}

		mysql_query('COMMIT');
		$response = [
			'tipo' => 'success',
			'id' => $idOrcamento
		];
		return $response;
	}

	public static function getTopicosByIdPaiEVersao($id, $versao)
	{

		$sql = "
		Select 
		* 
		from 
		orcamento_financeiro_estrutura
		where
		id_pai = {$id}
		and 
		versao_estrutura = {$versao}
		order by  ordem ASC
		";


		return parent::get($sql, 'Assoc');
	}

	public static function getTopicos()
	{


		$sql = "
		Select 
		* 
		from 
		orcamento_financeiro_topicos
		order by  id ASC
		";

		return parent::get($sql, 'Assoc');
	}

	public static function getValoresTopicosByVersao($versao)
	{


		$sql = "
		Select 
		* 
		from 
		orcamento_financeiro_topicos_valores
		where
		orcamento_financeiro_versao_id = {$versao}
		and deletado_por is null
		ORDER BY orcamento_financeiro_topicos, data asc
		";

		return parent::get($sql, 'Assoc');
	}

	public static function getValoresPlanoContasByVersao($versao)
	{


		$sql = "
		Select 
		o.*,
		p.COD_N4,
		p.N4 
		from 
		orcamento_financeiro_plano_contas_valores as o 
		inner join plano_contas as p on (o.plano_contas_id = p.ID)
		where
		versao_id = {$versao}
		and deletado_por is null
		order by data asc
		";

		return parent::get($sql, 'Assoc');
	}
	public static function getValoresPlanoContasByVersaoAndPlanoContaId($versaoId, $planoContasId)
	{
		$sql = "
	Select 
	o.*,
	p.COD_N4,
	p.N4 
	from 
	orcamento_financeiro_plano_contas_valores as o 
	inner join plano_contas as p on (o.plano_contas_id = p.ID)
	where
	
	versao_id = {$versaoId}
	and plano_contas_id = {$planoContasId}
	and deletado_por is null
	order by data asc
	";

		return parent::get($sql, 'Assoc');
	}
	public static function getVersaoOrcamentoById($id)
	{


		$sql = "
		Select 
		* 
		from 
		orcamento_financeiro_versao 

		where
		id = {$id}
		";

		return parent::get($sql, 'Assoc');
	}

	public static function updateValoresTopico($data)
	{
		$sql = "		
		Update
		orcamento_financeiro_topicos_valores
		set
		valor_mensal = '{$data['valor']}',
		aliquota = {$data['porcentagem']},
		editado_por = '{$_SESSION['id_user']}', 
		editado_em = now()
			
		where
		id = {$data['id']} ";

		$rs = mysql_query($sql);

		if (!$rs) {
			$response = [
				'tipo' => 'erro',
				'msg' => "Erro ao inserir itens. Entre em contato com o TI"
			];
			return $response;
		}

		return $response = [
			'tipo' => 'success',
			'msg' => "atualizado com sucesso"
		];
	}

	public static function updateValoresPlanoContas($data)
	{

		$sql = "    
        Update
        orcamento_financeiro_plano_contas_valores
        set
        valor_mensal = {$data['valor']},
        aliquota = {$data['porcentagem']},
		editado_por = '{$_SESSION['id_user']}', 
		editado_em = now()
        
        where
        id = {$data['id']}        
";

		$rs = mysql_query($sql);

		if (!$rs) {

			$response = [
				'tipo' => 'erro',
				'msg' => "Erro ao inserir itens. Entre em contato com o TI"
			];
			return $response;
		}

		return $response = [
			'tipo' => 'success',
			'id' => $data['id'],
			'msg' => 'atualizado com sucesso.'
		];
	}





	public static function createPlanoContasValores($dados, $arrayData)
	{
		$itens = [];
		$topicoId = $dados['topico_id'];
		$idOrcamento = $dados['orcamento_id'];
		$planoContasId = $dados['plano_conta_id'];
		foreach ($arrayData as $data) {
			$itens[] = "(
					$planoContasId,
					$topicoId,
					$idOrcamento,
					0,
					'" . $data . "',
					0,
					'{$_SESSION['id_user']}',
					now()
					
				)";
		}
		$sql = "
            Insert into
            orcamento_financeiro_plano_contas_valores
            (
				plano_contas_id,
				orcamento_financeiro_topicos_id,
				versao_id,
				valor_mensal,
				data, 
				aliquota,
				criado_por,
				criado_em                         
            )
            values
			" . implode(',', $itens);




		$rs = mysql_query($sql);

		if (!$rs) {
			mysql_query('ROLLBACK');
			$response = [
				'tipo' => 'erro',
				'msg' => "Erro ao inserir itens. Entre em contato com o TI"
			];
			return $response;
		}

		mysql_query('COMMIT');
		$response = [
			'tipo' => 'success',
			'id' => $idOrcamento
		];
		return $response;
	}

	public static function deleteValoresPlanoContas($data)
	{


		$sql = "	
		Update
		orcamento_financeiro_plano_contas_valores
		set
		deletado_por = '{$_SESSION['id_user']}',
		deletado_em = now()		
		where
		id = {$data['id']}		
";

		$rs = mysql_query($sql);

		if (!$rs) {

			$response = [
				'tipo' => 'erro',
				'msg' => "Erro ao deletar itens. Entre em contato com o TI"
			];
			return $response;
		}

		return $response = [
			'tipo' => 'success',
			'id' => $data['id'],
			'msg' => 'Deletado com sucesso.'
		];
	}

	public static function deleteValoresTopico($data)
	{


		$sql = "		
		Update
		orcamento_financeiro_topicos_valores
		set
		deletado_por = '{$_SESSION['id_user']}',
		deletado_em = now()
			
		where
		id = {$data['id']} ";

		$rs = mysql_query($sql);

		if (!$rs) {
			$response = [
				'tipo' => 'erro',
				'msg' => "Erro ao deletar itens. Entre em contato com o TI"
			];
			return $response;
		}

		return $response = [
			'tipo' => 'success',
			'msg' => "Deletado com sucesso"
		];
	}

	public static function getRealizadoByData($inicio, $fim, $unidadeId)
	{

		$condUnidadeParcela = '';
		$condUnidadeTarifa = '';
		$investimento = Assinatura::getInvestimento();
		$notInAssinatura = '';
		
		if(!empty($investimento)){
			$notInAssinatura = [];
			foreach($investimento as $invest){
				$notInAssinatura[] = $invest['ID'];
			}
			$notInAssinatura = " and r.ASSINATURA not in (".implode(',',$notInAssinatura).")";
		}

		if (!empty($unidadeId)) {
			$condUnidadeParcela = " and r.CENTRO_RESULTADO in ({$unidadeId})";
			$condUnidadeTarifa = " and cb.UR  in ({$unidadeId})";
		} else {
			$unidadesUser = $_SESSION['empresa_user'];
			$condUnidadeParcela = " and r.CENTRO_RESULTADO in {$unidadesUser}";
			$condUnidadeTarifa = " and cb.UR  in {$unidadesUser}";
		}
		/* Não deve trazer itens que foram movimentados nas contas do Banco CAixa Fixo 000
			  coluna na tabela contas_bancarias ORIGEM_FUNDOS_ID  = 8 
		*/
		$sql = "
		SELECT
	'Parcela' AS `tipo_item`,
	concat( 'nota - ', `r`.`ID` ) AS `TIPO_DOCUMENTO`,
	`p`.`idNota` AS `NOTA_ID`,	
	`p`.`DATA_CONCILIADO`,
	`p`.`origem` AS `BANCO_ID`,
	( ( (`p`.`VALOR_PAGO` - (JurosMulta + encargos)) * `r`.`PORCENTAGEM` ) / 100 ) AS `VALOR_PARCELA_CONCILIADO`,
	`pc`.`ID` AS `ID_NATUREZA`,
	`p`.`nota_id_fatura_aglutinacao` AS `notaAglutinacao`,
	`F`.`classificacao` AS `CLASSIFICACAO_CONVENIO`,
	`n`.`descricao` AS `descricao`,
   p.DATA_PROCESSADO,
   pc.COD_N3,   
	CONCAT(IF(F.CPF = '' , F.cnpj, F.CPF),' ', F.nome) as fornecedor,
	n.codigo as numero_nota,
	p.id as PARCELA_ID,
	((p.JurosMulta * `r`.`PORCENTAGEM` ) / 100 ) as juros,
	((p.encargos * `r`.`PORCENTAGEM` ) / 100 ) 	as encargo
FROM
	`parcelas` `p`
	LEFT JOIN `contas_bancarias` `CB` ON ( `p`.`origem` = `CB`.`ID` )
	LEFT JOIN `origemfundos` `OF` ON ( `CB`.`ORIGEM_FUNDOS_ID` = `OF`.`id` )
	JOIN `notas` `n` ON ( `p`.`idNota` = `n`.`idNotas` )
	JOIN `rateio` `r` ON ( `n`.`idNotas` = `r`.`NOTA_ID` )
	JOIN `plano_contas` `pc` ON ( `r`.`IPCG4` = `pc`.`ID` )
	JOIN `fornecedores` `F` ON ( `F`.`idFornecedores` = `n`.`codFornecedor` ) 
WHERE
	( `p`.`status` = 'Processada' ) 
	AND ( n.STATUS <> 'Cancelada' )
	AND  DATE_FORMAT(DATA_CONCILIADO, '%Y-%m')	BETWEEN '{$inicio}' and '{$fim}'
	AND CB.ORIGEM_FUNDOS_ID  <> 8
	{$condUnidadeParcela}
	{$notInAssinatura}

UNION
SELECT
	'Tarifa' AS `tipo_item`,
	concat( 'tarifa - ', `imptar`.`id` ) AS `TIPO_DOCUMENTO`,
	concat( `itl`.`id` - 'tarifa') AS `NOTA_ID`,
	`itl`.`data` AS `DATA_CONCILIADO`,
	`itl`.`conta` AS `BANCO_ID`,
	`itl`.`valor` AS `VALOR_PARCELA_CONCILIADO`,
	`pc`.`ID` AS `ID_NATUREZA`,
	'' AS `notaAglutinacao`,
	'' AS `CLASSIFICACAO_CONVENIO`,
	`itl`.`documento` AS `descricao`,
	`itl`.`data` AS `DATA_PROCESSADA`,
	 pc.COD_N3,
	 imptar.item as fornecedor,
	 itl.documento as numero_nota,
	 concat( `itl`.`id` - 'tarifa') AS PARCELA_ID,
	 0 as juros,
	0 	as encargo
	FROM
	`impostos_tarifas` `imptar`
	JOIN `impostos_tarifas_lancto` `itl` ON ( `imptar`.`id` = `itl`.`item` )
	JOIN `plano_contas` `pc` ON ( `imptar`.`ipcf` = `pc`.`COD_N4` )
	JOIN contas_bancarias as cb on (itl.conta = cb.ID)
WHERE
DATE_FORMAT(itl.data, '%Y-%m')	BETWEEN '{$inicio}' and '{$fim}'
AND cb.ORIGEM_FUNDOS_ID <> 8
{$condUnidadeTarifa}
		";


		return parent::get($sql, 'Assoc');
	}

	public static function getOrcamentoFinanceiro($versao_id, $versoaoEstrutura)
	{
		$sql = "
		#Select Topicos
		SELECT 
			tv.id as id,
			t.id as id_topico,
			e.ordem as ordem,
			e.id_pai as id_pai,
			t.descricao as name,
			'orcamento_financeiro_topicos_valores' as tabela,
			tv.aliquota as aliquota,
			tv.data as data,
			tv.valor_mensal as value,
			0 as id_plano_de_conta,
			0 as realizado
			
		FROM 
			orcamento_financeiro_topicos_valores as tv,
			orcamento_financeiro_topicos as t,
			orcamento_financeiro_estrutura as e
		WHERE
			tv.orcamento_financeiro_topicos like t.id AND
			e.orcamento_finananceiro_topicos_id like t.id AND
			tv.orcamento_financeiro_versao_id = '{$versao_id}' AND
			tv.deletado_em is null
			AND e.versao_estrutura = {$versoaoEstrutura}
			AND t.id not in (69, 92, 93) 
		
		#Select Plano de contas
		Union SELECT 
				pc.id as id,
				pc.id + 1000 as id_topico,
				pc.id + 1000 as ordem,
				pcv.orcamento_financeiro_topicos_id as id_pai,
				CONCAT(pc.COD_N4, ' ', pc.N4) as name,
				'orcamento_financeiro_plano_contas_valores' as tabela,
				pcv.aliquota as aliquota,
				pcv.data as data,
				pcv.valor_mensal as value,
				pc.id as id_plano_de_conta,
				0 as realizado
			FROM 
				orcamento_financeiro_plano_contas_valores as pcv,
				plano_contas as pc
			WHERE
				pcv.plano_contas_id like pc.id AND
				pcv.versao_id = '{$versao_id}' AND
			    pcv.deletado_em is null

		ORDER BY
			id_pai, ordem, data";

		return parent::get($sql);
	}

	public static function getOrcamentoAndValorGerencial($versao_id, $versoaoEstrutura)
	{
		$sql = "
		#Select Topicos
		SELECT 
			tv.id as id,
			t.id as id_topico,
			e.ordem as ordem,
			e.id_pai as id_pai,
			t.descricao as name,
			'orcamento_financeiro_topicos_valores' as tabela,
			tv.aliquota as aliquota,
			tv.data as data,
			tv.valor_mensal as value,
			0 as id_plano_de_conta,
			COALESCE(tvg.valor_mensal,0) as realizado			
		FROM 
			orcamento_financeiro_topicos_valores as tv 
			inner join orcamento_financeiro_topicos as t on (tv.orcamento_financeiro_topicos like t.id)
			inner join orcamento_financeiro_estrutura as e on (e.orcamento_finananceiro_topicos_id like t.id)
			left join orcamento_financeiro_topicos_valores_gerencial as tvg 
			on ( tv.orcamento_financeiro_topicos = tvg.orcamento_financeiro_topicos and tvg.data = tv.data)
		

		WHERE
			
			tv.orcamento_financeiro_versao_id = '{$versao_id}' AND
			tv.deletado_em is null
			AND e.versao_estrutura = {$versoaoEstrutura}
		
		#Select Plano de contas
		Union SELECT 
				pc.id as id,
				pc.id + 1000 as id_topico,
				pc.id + 1000 as ordem,
				pcv.orcamento_financeiro_topicos_id as id_pai,
				CONCAT(pc.COD_N4, ' ', pc.N4) as name,
				'orcamento_financeiro_plano_contas_valores' as tabela,
				pcv.aliquota as aliquota,
				pcv.data as data,
				pcv.valor_mensal as value,
				pc.id as id_plano_de_conta,
				COALESCE(ofpcvg.valor_mensal,0) as realizado			
		
			FROM 
				orcamento_financeiro_plano_contas_valores as pcv
				inner join plano_contas as pc on (pcv.plano_contas_id like pc.id)
				left join orcamento_financeiro_plano_contas_valores_gerencial ofpcvg 
				on(pcv.plano_contas_id = ofpcvg.plano_contas_id and pcv.data = ofpcvg.data)
			WHERE
				
				pcv.versao_id = '{$versao_id}' AND
			    pcv.deletado_em is null AND
				pcv.orcamento_financeiro_topicos_id not IN (2)  

		ORDER BY
			id_pai, ordem, data";

		return parent::get($sql);
	}

	public static function getValoresGerenciaisTopicosByVersao($versao)
	{


		$sql = "
		Select
	ofg.id as id_valor_gerencial,
	ofg.valor_mensal as valor_gerencial,
	o.*
from
	orcamento_financeiro_topicos_valores o
left join orcamento_financeiro_topicos_valores_gerencial ofg on
	(o.orcamento_financeiro_topicos = ofg.orcamento_financeiro_topicos
	and o.data = ofg.data)
where
	orcamento_financeiro_versao_id = {$versao}
		
		and o.deletado_por is null
		ORDER BY o.orcamento_financeiro_topicos, o.data asc
		";

		return parent::get($sql, 'Assoc');
	}

	public static function getValoresGerenciaisPlanoContasByVersao($versao)
	{


		$sql = "
		Select 
		og.id as id_valor_gerencial,
		og.valor_mensal as valor_gerencial,
		o.*,
		p.COD_N4,
		p.N4 
		from 
		orcamento_financeiro_plano_contas_valores as o left Join
		orcamento_financeiro_plano_contas_valores_gerencial as og
		on (o.plano_contas_id = og.plano_contas_id and o.data = og.data)
		inner join plano_contas as p on (o.plano_contas_id = p.ID)
		where
		versao_id = {$versao}
		and o.deletado_por is null
		order by o.data asc
		";

		return parent::get($sql, 'Assoc');
	}

	public static function getOrcamentoAndValorGerencialAutomatico($versao_id, $versoaoEstrutura, $inicio)
	{
		if($versoaoEstrutura == 2){
			$cond = " AND orcamento_financeiro_topicos_id not in (2, 82, 83, 84, 96,97, 69, 92, 93) ";
		}

		$sql = "
		#Select Topicos
		SELECT 
			tv.id as id,
			t.id as id_topico,
			e.ordem as ordem,
			e.id_pai as id_pai,
			t.descricao as name,
			'orcamento_financeiro_topicos_valores' as tabela,
			tv.aliquota as aliquota,
			tv.data as data,
			tv.valor_mensal as value,
			0 as id_plano_de_conta,
			COALESCE(tvg.valor_mensal,0) as realizado			
		FROM 
			orcamento_financeiro_topicos_valores as tv 
			inner join orcamento_financeiro_topicos as t on (tv.orcamento_financeiro_topicos like t.id)
			inner join orcamento_financeiro_estrutura as e on (e.orcamento_finananceiro_topicos_id like t.id)
			left join orcamento_financeiro_topicos_valores_gerencial_automatico as tvg 
			on ( tv.orcamento_financeiro_topicos = tvg.orcamento_financeiro_topicos and tvg.data = tv.data)		

		WHERE
			
			tv.orcamento_financeiro_versao_id = '{$versao_id}' AND
			tv.deletado_em is null
			AND e.versao_estrutura = {$versoaoEstrutura}
			ANd t.id not in (69, 92, 93)
		#Selec mes anterior topico
		UNION SELECT
			tv.id as id,
			t.id as id_topico,
			e.ordem as ordem,
			e.id_pai as id_pai,
			t.descricao as name,
			'orcamento_financeiro_topicos_valores' as tabela,
			tv.aliquota as aliquota,
			'{$inicio}' as `data` ,
			0 as value,
			0 as id_plano_de_conta,
			COALESCE(tvg.valor_mensal,0) as realizado
		FROM
			orcamento_financeiro_topicos_valores as tv
		inner join orcamento_financeiro_topicos as t on
			(tv.orcamento_financeiro_topicos like t.id)
		inner join orcamento_financeiro_estrutura as e on
			(e.orcamento_finananceiro_topicos_id like t.id)
		left join orcamento_financeiro_topicos_valores_gerencial_automatico as tvg on
			( tv.orcamento_financeiro_topicos = tvg.orcamento_financeiro_topicos
			and tvg.data = '{$inicio}')
		WHERE
			tv.orcamento_financeiro_versao_id = '{$versao_id}'
			AND tv.deletado_em is null
			AND e.versao_estrutura = {$versoaoEstrutura}
			ANd t.id not in (69, 92, 93)
			group by id_topico	
		
		#Select Plano de contas
		Union SELECT 
				pc.id as id,
				pc.id + 1000 as id_topico,
				pc.id + 1000 as ordem,
				pcv.orcamento_financeiro_topicos_id as id_pai,
				CONCAT(pc.COD_N4, ' ', pc.N4) as name,
				'orcamento_financeiro_plano_contas_valores' as tabela,
				pcv.aliquota as aliquota,
				pcv.data as data,
				pcv.valor_mensal as value,
				pc.id as id_plano_de_conta,
				COALESCE(tvg.valor_mensal,0) as realizado			
		
			FROM 
				orcamento_financeiro_plano_contas_valores as pcv
				inner join plano_contas as pc on (pcv.plano_contas_id like pc.id)
				left join orcamento_financeiro_plano_contas_valores_gerencial_automatico as tvg
				on (pcv.plano_contas_id = tvg.plano_contas_id and pcv.data = tvg.data)
				WHERE				
				pcv.versao_id = '{$versao_id}' AND
			    pcv.deletado_em is null
				{$cond}
				#select mes anterior planocontas
			Union SELECT
					pc.id as id,
					pc.id + 1000 as id_topico,
					pc.id + 1000 as ordem,
					pcv.orcamento_financeiro_topicos_id as id_pai,
					CONCAT(pc.COD_N4, ' ', pc.N4) as name,
					'orcamento_financeiro_plano_contas_valores' as tabela,
					0 as aliquota,
					'{$inicio}' as data,
					0 as value,
					pc.id  as id_plano_de_conta,
					COALESCE(tvg.valor_mensal,0) as realizado
				FROM
					orcamento_financeiro_plano_contas_valores as pcv
				inner join plano_contas as pc on
					(pcv.plano_contas_id like pc.id)
				left join orcamento_financeiro_plano_contas_valores_gerencial_automatico as tvg on
					(pcv.plano_contas_id = tvg.plano_contas_id
					and tvg.data = '{$inicio}')
				WHERE
					pcv.versao_id = '{$versao_id}'
					AND pcv.deletado_em is null
					{$cond}
					group by pc.COD_N4	

		ORDER BY
			id_pai, ordem, data";
				
			
			

		return parent::get($sql);

	}

	public static function getGerencialAutomatizadoByData($inicio, $fim, $idNaturezas = null)
	{
		$investimento = Assinatura::getInvestimento();
		$notInAssinatura = '';
		$sqlFolhaAvlog = '';
		
		if(!empty($investimento)){
			$notInAssinatura = [];
			foreach($investimento as $invest){
				$notInAssinatura[] = $invest['ID'];
			}
			
			$notInAssinatura = " and r.ASSINATURA not in (".implode(',',$notInAssinatura).")";
			$sqlFolhaAvlog = "
			UNION SELECT
				'Parcela' AS `tipo_item`,
				concat( 'nota - ', `r`.`ID` ) AS `TIPO_DOCUMENTO`,
				`n`.`idNotas` AS `NOTA_ID`,
				concat(data_competencia,'-01') AS `DATA_CONCILIADO`,
				'' AS `BANCO_ID`,
				( ( `n`.`VAL_PRODUTOS` * `r`.`PORCENTAGEM` ) / 100 ) AS `VALOR`,
				`pc`.`ID` AS `ID_NATUREZA`,
				`n`.`nota_aglutinacao` AS `notaAglutinacao`,
				`F`.`classificacao` AS `CLASSIFICACAO_CONVENIO`,
				`n`.`descricao` AS `descricao`,
				`n`.`data_competencia`,
				pc.COD_N3,   
				CONCAT(IF(F.CPF = '' , F.cnpj, F.CPF),' ', F.nome) as fornecedor,
				n.codigo as numero_nota	 	
			FROM
				`notas` `n` 
				JOIN `rateio` `r` ON ( `n`.`idNotas` = `r`.`NOTA_ID` )
				JOIN `plano_contas` `pc` ON ( `r`.`IPCG4` = `pc`.`ID` )
				JOIN `fornecedores` `F` ON ( `F`.`idFornecedores` = `n`.`codFornecedor` ) 
			WHERE
				n.STATUS <> 'Cancelada' 
			and  data_competencia	BETWEEN '{$inicio}' and '{$fim}'
			AND n.TIPO_DOCUMENTO <> 'PR'
			and n.nota_aglutinacao = 'n'	
			and pc.id = 320
			";
		}
		
		$condNatureza = '';
		if($idNaturezas){
			$condNatureza = " AND pc.id in ($idNaturezas)";
		}

		$sqlServicos = "
		
		";

		
		/* Não deve trazer itens que foram movimentados nas contas do Banco CAixa Fixo 000
			  coluna na tabela contas_bancarias ORIGEM_FUNDOS_ID  = 8 
		*/
		
		$sql = "
		SELECT
		'Parcela' AS `tipo_item`,
		concat( 'nota - ', `r`.`ID` ) AS `TIPO_DOCUMENTO`,
		`n`.`idNotas` AS `NOTA_ID`,
		concat(data_competencia,'-01') AS `DATA_CONCILIADO`,
		'' AS `BANCO_ID`,
		( ( `n`.`VAL_PRODUTOS` * `r`.`PORCENTAGEM` ) / 100 ) AS `VALOR`,
		`pc`.`ID` AS `ID_NATUREZA`,
		`n`.`nota_aglutinacao` AS `notaAglutinacao`,
		`F`.`classificacao` AS `CLASSIFICACAO_CONVENIO`,
		`n`.`descricao` AS `descricao`,
		`n`.`data_competencia`,
		pc.COD_N3,   
		CONCAT(IF(F.CPF = '' , F.cnpj, F.CPF),' ', F.nome) as fornecedor,
		n.codigo as numero_nota	 	
	FROM
		`notas` `n` 
		JOIN `rateio` `r` ON ( `n`.`idNotas` = `r`.`NOTA_ID` )
		JOIN `plano_contas` `pc` ON ( `r`.`IPCG4` = `pc`.`ID` )
		JOIN `fornecedores` `F` ON ( `F`.`idFornecedores` = `n`.`codFornecedor` ) 
	WHERE
		 n.STATUS <> 'Cancelada' 
	and  data_competencia	BETWEEN '{$inicio}' and '{$fim}'
	AND n.TIPO_DOCUMENTO <> 'PR'
	and n.nota_aglutinacao = 'n'	
	{$condNatureza}
	{$notInAssinatura}
	{$sqlFolhaAvlog}
	
UNION
SELECT
	'Tarifa' AS `tipo_item`,
	concat( 'tarifa - ', `imptar`.`id` ) AS `TIPO_DOCUMENTO`,
	concat( `itl`.`id`, '-Tarifa') AS `NOTA_ID`,
	`itl`.`data` AS `DATA_CONCILIADO`,
	`itl`.`conta` AS `BANCO_ID`,
	`itl`.`valor` AS VALOR,
	`pc`.`ID` AS `ID_NATUREZA`,
	'' AS `notaAglutinacao`,
	'' AS `CLASSIFICACAO_CONVENIO`,
	`itl`.`documento` AS `descricao`,
	`itl`.`data` AS `DATA_PROCESSADA`,
	 pc.COD_N3,
	 imptar.item as fornecedor,
	 itl.documento as numero_nota 	
	FROM
	`impostos_tarifas` `imptar`
	JOIN `impostos_tarifas_lancto` `itl` ON ( `imptar`.`id` = `itl`.`item` )
	JOIN `plano_contas` `pc` ON ( `imptar`.`ipcf` = `pc`.`COD_N4` )
	JOIN contas_bancarias as cb on (itl.conta = cb.ID)
WHERE
DATE_FORMAT(itl.data, '%Y-%m')	BETWEEN '{$inicio}' and '{$fim}'
AND cb.ORIGEM_FUNDOS_ID <> 8
Union
SELECT
'Parcela' AS `tipo_item`,
concat( 'nota - ', `p`.`id` ) AS `TIPO_DOCUMENTO`,
`p`.`idNota` AS `NOTA_ID`,	
`p`.`DATA_CONCILIADO`,
`p`.`origem` AS `BANCO_ID`,
p.JurosMulta AS `VALOR_PARCELA_CONCILIADO`,
IF (n.tipo = 0 , 5, 132) AS `ID_NATUREZA`,
`p`.`nota_id_fatura_aglutinacao` AS `notaAglutinacao`,
`F`.`classificacao` AS `CLASSIFICACAO_CONVENIO`,
`n`.`descricao` AS `descricao`,
p.DATA_PROCESSADO,
IF (n.tipo = 0 , '1.0.2', '2.1.13') as COD_N3,   
CONCAT(IF(F.CPF = '' , F.cnpj, F.CPF),' ', F.nome) as fornecedor,
n.codigo as numero_nota
FROM
`parcelas` `p`
LEFT JOIN `contas_bancarias` `CB` ON ( `p`.`origem` = `CB`.`ID` )
LEFT JOIN `origemfundos` `OF` ON ( `CB`.`ORIGEM_FUNDOS_ID` = `OF`.`id` )
JOIN `notas` `n` ON ( `p`.`idNota` = `n`.`idNotas` )
JOIN `fornecedores` `F` ON ( `F`.`idFornecedores` = `n`.`codFornecedor` ) 
WHERE
( `p`.`status` = 'Processada' ) 
AND ( n.STATUS <> 'Cancelada' )
AND  DATE_FORMAT(DATA_CONCILIADO, '%Y-%m')	BETWEEN  '{$inicio}' and '{$fim}'
AND CB.ORIGEM_FUNDOS_ID  <> 8 and JurosMulta > 0 
UNION
SELECT
'Parcela' AS `tipo_item`,
concat( 'nota - ', `p`.`id` ) AS `TIPO_DOCUMENTO`,
`p`.`idNota` AS `NOTA_ID`,	
`p`.`DATA_CONCILIADO`,
`p`.`origem` AS `BANCO_ID`,
p.encargos AS `VALOR_PARCELA_CONCILIADO`,
133 AS `ID_NATUREZA`,
`p`.`nota_id_fatura_aglutinacao` AS `notaAglutinacao`,
`F`.`classificacao` AS `CLASSIFICACAO_CONVENIO`,
`n`.`descricao` AS `descricao`,
p.DATA_PROCESSADO,
 '2.1.13' as COD_N3,   
CONCAT(IF(F.CPF = '' , F.cnpj, F.CPF),' ', F.nome) as fornecedor,
n.codigo as numero_nota
FROM
`parcelas` `p`
LEFT JOIN `contas_bancarias` `CB` ON ( `p`.`origem` = `CB`.`ID` )
LEFT JOIN `origemfundos` `OF` ON ( `CB`.`ORIGEM_FUNDOS_ID` = `OF`.`id` )
JOIN `notas` `n` ON ( `p`.`idNota` = `n`.`idNotas` )
JOIN `fornecedores` `F` ON ( `F`.`idFornecedores` = `n`.`codFornecedor` ) 
WHERE
( `p`.`status` = 'Processada' ) 
AND ( n.STATUS <> 'Cancelada' )
AND  DATE_FORMAT(DATA_CONCILIADO, '%Y-%m')	BETWEEN  '{$inicio}' and '{$fim}'
AND CB.ORIGEM_FUNDOS_ID  <> 8 and encargos > 0 
Union
SELECT
		'Parcela' AS `tipo_item`,
		concat( 'nota - ', `r`.`ID` ) AS `TIPO_DOCUMENTO`,
		`n`.`idNotas` AS `NOTA_ID`,
		concat(data_competencia,'-01') AS `DATA_CONCILIADO`,
		'' AS `BANCO_ID`,
		( ( p.VALOR_PAGO * `r`.`PORCENTAGEM` ) / 100 ) AS `VALOR`,
		11 AS `ID_NATUREZA`,
		`n`.`nota_aglutinacao` AS `notaAglutinacao`,
		`F`.`classificacao` AS `CLASSIFICACAO_CONVENIO`,
		`n`.`descricao` AS `descricao`,
		`n`.`data_competencia`,
		'1.0.3.02' as COD_N3,   
		CONCAT(IF(F.CPF = '' , F.cnpj, F.CPF),' ', F.nome) as fornecedor,
		n.codigo as numero_nota
	FROM
		`notas` `n` 
		join  parcelas p on ( `n`.`idNotas` = p.idNota)
		JOIN `rateio` `r` ON ( `n`.`idNotas` = `r`.`NOTA_ID` )
		JOIN `plano_contas` `pc` ON ( `r`.`IPCG4` = `pc`.`ID` )
		JOIN `fornecedores` `F` ON ( `F`.`idFornecedores` = `n`.`codFornecedor` ) 
	WHERE
	n.STATUS <> 'Cancelada' 
	and  data_competencia	BETWEEN '{$inicio}' and '{$fim}'
	and p.status = 'Processada'
	and r.IPCG4 = 114
	and p.origem = 39

";

	return parent::get($sql, 'Assoc');
	}

	public static function getValorTopicoGerencialAutomatico ($array)
	{
		$sql = "SELECT 
			ofvg.*		
		FROM 
			 orcamento_financeiro_topicos_valores_gerencial_automatico as ofvg
		WHERE
		ofvg.orcamento_financeiro_topicos = {$array['topico_id']}
		AND ofvg.data = '{$array['mesano']}'
		AND deletado_em is null
		AND deletado_por is null
		";
		
		
		return parent::get($sql, 'Assoc');
	}

	

	public static function getValorPlanoContaGerencialAutomatico ($array)
	{
		$sql = "SELECT 
			ofvg.*		
		FROM 
			 orcamento_financeiro_plano_contas_valores_gerencial_automatico as ofvg
		WHERE
		ofvg.plano_contas_id = {$array['natureza_id']}
		AND ofvg.data = '{$array['mesano']}'
		AND deletado_em is null
		AND deletado_por is null
		";
		
		

		return parent::get($sql, 'Assoc');
		
	}

	public static function createPlanoContasValoresGerenciais($dados)
	{

		$sql = "
            Insert into
            orcamento_financeiro_plano_contas_valores_gerencial
            (	valor_mensal,
				plano_contas_id,			
				data, 
				criado_em, 
				criado_por
				                        
            )
            values
			" . implode(',', $dados);

		mysql_query('begin');



		$rs = mysql_query($sql);



		if (!$rs) {
			mysql_query('ROLLBACK');
			$response = [
				'tipo' => 'erro',
				'msg' => "Erro ao inserir itens. Entre em contato com o TI"
			];
			return $response;
		}

		mysql_query('COMMIT');
		$response = [
			'tipo' => 'success',
			'id' => $idOrcamento
		];
		return $response;
	}


	public static function createTopicosValoresGerenciais($dados)
	{

		$sql = "
            Insert into
            orcamento_financeiro_topicos_valores_gerencial
            (	valor_mensal,
				orcamento_financeiro_topicos,			
				data, 
				criado_em, 
				criado_por
				                        
            )
            values
			" . implode(',', $dados);

		mysql_query('begin');

		$rs = mysql_query($sql);
		

		if (!$rs) {
			mysql_query('ROLLBACK');
			$response = [
				'tipo' => 'erro',
				'msg' => "Erro ao inserir itens. Entre em contato com o TI"
			];
			return $response;
		}

		mysql_query('COMMIT');
		$response = [
			'tipo' => 'success',
			'id' => $idOrcamento
		];
		return $response;
	}

	public static function updateValoresGerencialTopico($data)
	{
		$sql = "		
		Update
		orcamento_financeiro_topicos_valores_gerencial
		set
		valor_mensal = '{$data['valor']}',		
		editado_por = '{$_SESSION['id_user']}', 
		editado_em = now()
			
		where
		id = {$data['id']} ";
		
		$rs = mysql_query($sql);

		if (!$rs) {
			$response = [
				'tipo' => 'erro',
				'msg' => "Erro ao atualizar itens. Entre em contato com o TI"
			];
		
			return $response;
		}

		return $response = [
			'tipo' => 'success',
			'msg' => "atualizado com sucesso"
		];
	}

	


	public static function updateValoresGerencialPlanoContas($data)
	{

		$sql = "    
        Update
        orcamento_financeiro_plano_contas_valores_gerencial
        set
        valor_mensal = {$data['valor']},        
		editado_por = '{$_SESSION['id_user']}', 
		editado_em = now()
        
        where
        id = {$data['id']}        
";



		$rs = mysql_query($sql);

		if (!$rs) {

			$response = [
				'tipo' => 'erro',
				'msg' => "Erro ao inserir itens. Entre em contato com o TI"
			];
			
			return $response;
		}

		return $response = [
			'tipo' => 'success',
			'id' => $data['id'],
			'msg' => 'atualizado com sucesso.'
		];
	}
	public static function updateValoresGerencialPlanoContasAutomatico($data)
	{	
		$editadoPor = (isset($_SESSION['id_user']) && !empty($_SESSION['id_user'])) ? $_SESSION['id_user'] : 0;
		
		
		$sql = "    
        Update
        orcamento_financeiro_plano_contas_valores_gerencial_automatico
        set
        valor_mensal = {$data['valor']},        
		editado_por = {$editadoPor}, 
		editado_em = now()        
        where
        id = {$data['id']}        
";





		$rs = mysql_query($sql);

		if (!$rs) {

			$response = [
				'tipo' => 'erro',
				'msg' => "Erro ao inserir itens. Entre em contato com o TI"
			];
			
			return $response;
		}

		return $response = [
			'tipo' => 'success',
			'id' => $data['id'],
			'msg' => 'atualizado com sucesso.'
		];
	}
	public static function updateValoresGerencialTopicoAutomatico($data)
	{
		$editadoPor = (isset($_SESSION['id_user']) && !empty($_SESSION['id_user'])) ? $_SESSION['id_user'] : 0;
		
		
		$sql = "		
		Update
		orcamento_financeiro_topicos_valores_gerencial_automatico
		set
		valor_mensal = '{$data['valor']}',		
		editado_por = '{$editadoPor}', 
		editado_em = now()			
		where
		id = {$data['id']} ";
	
		
		$rs = mysql_query($sql);

		if (!$rs) {
			$response = [
				'tipo' => 'erro',
				'msg' => "Erro ao atualizar itens. Entre em contato com o TI"
			];
		
			return $response;
		}

		return $response = [
			'tipo' => 'success',
			'msg' => "atualizado com sucesso"
		];
	}

	public static function createPlanoContasValoresGerenciaisAutomatico($dados)
	{

		$sql = "
            Insert into
            orcamento_financeiro_plano_contas_valores_gerencial_automatico
            (	valor_mensal,
				plano_contas_id,			
				data, 
				criado_em, 
				criado_por
				                        
            )
            values
			" . implode(',', $dados);

		mysql_query('begin');



		$rs = mysql_query($sql);



		if (!$rs) {
			mysql_query('ROLLBACK');
			$response = [
				'tipo' => 'erro',
				'msg' => "Erro ao inserir itens. Entre em contato com o TI"
			];
			return $response;
		}

		mysql_query('COMMIT');
		$response = [
			'tipo' => 'success',
			'id' => $idOrcamento
		];
		return $response;
	}


	public static function createTopicosValoresGerenciaisAutomatico($dados)
	{

		$sql = "
            Insert into
            orcamento_financeiro_topicos_valores_gerencial_automatico
            (	valor_mensal,
				orcamento_financeiro_topicos,			
				data, 
				criado_em, 
				criado_por
				                        
            )
            values
			" . implode(',', $dados);
			

		mysql_query('begin');

		$rs = mysql_query($sql);
		

		if (!$rs) {
			mysql_query('ROLLBACK');
			$response = [
				'tipo' => 'erro',
				'msg' => "Erro ao inserir itens. Entre em contato com o TI"
			];
			return $response;
		}

		mysql_query('COMMIT');
		$response = [
			'tipo' => 'success',
			'id' => $idOrcamento
		];
		return $response;
	}

	public static function getValoresPlanoContasGerencial($inicio, $fim, $idNaturezas = null)
	{
		$condNatureza = '';
		if(!empty($idNaturezas)){
			$condNatureza = "and o.plano_contas_id in (".implode(",", $idNaturezas).")";
		}

		$sql = "
		Select	
	o.*
from
	orcamento_financeiro_plano_contas_valores_gerencial as o 
where
o.data BETWEEN '{$inicio}' and '{$fim}'
{$condNatureza}
		
		and o.deletado_por is null
		ORDER BY o.plano_contas_id, o.data asc
		";
		

		return parent::get($sql, 'Assoc');

		


	}

	public static function getValoresTopicosGerencial($inicio, $fim, $idTopico = null)
	{
		$condTopico = '';
		if(!empty($idTopico)){
			$condTopico= "and o.orcamento_financeiro_topicos in (".implode(",", $idTopico).")";
		}

		$sql = "
		Select	
	o.*
from
	orcamento_financeiro_topicos_valores_gerencial as o 
where
o.data BETWEEN '{$inicio}' and '{$fim}'
{$condTopico}
		
		and o.deletado_por is null
		ORDER BY o.orcamento_financeiro_topicos , o.data asc
		";
		
		return parent::get($sql, 'Assoc');

		


	}

	
}
