<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel;

class FechamentoCaixa extends AbstractModel
{
    public static function getFechamentoCaixaAtual()
    {
        $sql = <<<SQL
SELECT 
 *
FROM
 fechamento_caixa    
SQL;
        return current(parent::get($sql, 'ASSOC'));

    }

    public static function getHistoricoFechamentoCaixa()
    {
        $sql = <<<SQL
SELECT 
  historico_fechamento_caixa.*,
  usuarios.nome AS fechado_por
FROM 
  historico_fechamento_caixa
  INNER JOIN usuarios ON historico_fechamento_caixa.created_by = usuarios.idUsuarios
ORDER BY id DESC
LIMIT 20
SQL;
        return parent::get($sql, 'ASSOC');
    }

    public static function save($data)
    {
        $sql = <<<SQL
SELECT
  COUNT(*) AS countParcelas
FROM
  parcelas
WHERE
  status = 'Pendente'
  AND vencimento_real <= '{$data['data_fechamento']}'
SQL;
        $naoProcessadas = current(parent::get($sql, 'Assoc'));

        $sql = <<<SQL
SELECT
  COUNT(*) AS countParcelas
FROM
  parcelas 
  inner join notas on (parcelas.idNota = notas.idNotas)
WHERE
parcelas.status = 'Processada'
  AND (
      parcelas.nota_id_fatura_aglutinacao IS NULL
      OR parcelas.nota_id_fatura_aglutinacao = 0
  )
  AND vencimento_real <= '{$data['data_fechamento']}'
  AND (
        CONCILIADO IS NULL
        OR CONCILIADO = 'N'
    )
  AND DATA_CONCILIADO IS NULL
  AND  (
      (notas.nota_adiantamento = 'N' and parcelas.compensada ="NAO") or 
      (notas.nota_adiantamento = 'S')
  )
SQL;
        $naoConciliadas = current(parent::get($sql, 'Assoc'));
        $sql = "
        SELECT
COUNT(ID) as countTransferencia
from
transferencia_bancaria 
where
DATA_TRANSFERENCIA <= '{$data['data_fechamento']}'
AND (CONCILIADO_ORIGEM = 'N' OR CONCILIADO_DESTINO = 'N')
AND CANCELADA_POR is NULL
";
$naoConciliadasTransferencias = current(parent::get($sql, 'Assoc'));

        if($naoProcessadas['countParcelas'] == 0 && $naoConciliadas['countParcelas'] == 0
        && $naoConciliadasTransferencias['countTransferencia'] == 0) {
            $sql = <<<SQL
UPDATE 
  fechamento_caixa 
SET 
  data_fechamento = '{$data['data_fechamento']}',
  fechado_em = NOW(),
  fechado_por = '{$_SESSION['id_user']}'
SQL;
            $rs = mysql_query($sql);
            if (!$rs) {
                mysql_query('ROLLBACK');
                return false;
            }

            $sql = <<<SQL
INSERT INTO historico_fechamento_caixa 
(data_fechamento, justificativa, created_at, created_by) VALUES 
('{$data['data_fechamento']}', '{$data['justificativa']}', NOW(), '{$_SESSION['id_user']}')
SQL;
            $rs = mysql_query($sql);
            if (!$rs) {
                mysql_query('ROLLBACK');
                return false;
            }

            return mysql_query("COMMIT");
        }

        return [
            'nao_processadas' => $naoProcessadas['countParcelas'],
            'nao_conciliadas' => $naoConciliadas['countParcelas'],
            'transferencia_nao_conciliada' => $naoConciliadasTransferencias['countTransferencia']

        ];
    }

    public static function consultarNaoConciliadas($data_fechamento)
    {
        $sql = <<<SQL
SELECT
  *
FROM
  parcelas 
  inner join notas on (parcelas.idNota = notas.idNotas)
WHERE
parcelas.status = 'Processada'
  AND vencimento_real <= '{$data_fechamento}'
  AND (
      parcelas.nota_id_fatura_aglutinacao IS NULL
      OR parcelas.nota_id_fatura_aglutinacao = 0
  )
  AND (
        CONCILIADO IS NULL
        OR CONCILIADO = 'N'
    )
  AND DATA_CONCILIADO IS NULL
  AND  ((notas.nota_adiantamento = 'N' and parcelas.compensada ="NAO") or (notas.nota_adiantamento = 'S'))
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function consultarNaoProcessadas($data_fechamento)
    {
        $sql = <<<SQL
SELECT
  *
FROM
  parcelas
WHERE
  status = 'Pendente'
  AND vencimento_real <= '{$data_fechamento}'
SQL;
        return parent::get($sql, 'Assoc');
    }


    public static function consultarTransferenciasNaoConciliadas($data)
    {
        $sql = "
        SELECT 
          (
            SELECT  
                concat(o.SIGLA_BANCO,'-',c.AGENCIA,'-',c.NUM_CONTA,'-',e.SIGLA_EMPRESA)
            FROM
                contas_bancarias as c INNER JOIN
                origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id) INNER JOIN 
                empresas as e ON (c.UR = e.id) 
            WHERE
                c.ID= CONTA_ORIGEM
          ) as origem,
          (
            SELECT  
                concat(o.SIGLA_BANCO,'-',c.AGENCIA,'-',c.NUM_CONTA,'-',e.SIGLA_EMPRESA)
            FROM
                contas_bancarias as c INNER JOIN
                origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id) INNER JOIN 
                empresas as e ON (c.UR = e.id) 
            WHERE
                c.ID= CONTA_DESTINO
          ) as destino,
          VALOR, 
          'Transferência' as NUM_DOCUMENTO,
          DATE_FORMAT(DATA_TRANSFERENCIA, '%d/%m/%Y') as data
          from	
		`transferencia_bancaria` 
WHERE 
  DATA_TRANSFERENCIA <= '{$data}'
  AND (CONCILIADO_ORIGEM = 'N' OR CONCILIADO_DESTINO = 'N')
  AND CANCELADA_POR is NULL
";


        return parent::get($sql, 'Assoc');
    }
}
