<?php

namespace App\Models\Financeiro;

use App\Helpers\StringHelper;
use \App\Models\AbstractModel;
ini_set('display_errors', 1);
class Previsao extends AbstractModel
{
    

    public static function getParcelaPrevisao($filtros)
    {
        
        $condNatureza = empty($filtros['natureza-principal']) ? '' : " and n.natureza_movimentacao = {$filtros['natureza-principal']}";
    $sql = "
    SELECT 
    p.id,
    p.idNota as tr,
  p.vencimento_real,
  DATE_FORMAT(p.vencimento_real, '%d/%m/%Y') as venc_ptbr,
  p.valor,
  CONCAT(pl.COD_N4, ' - ', pl.N4) as natureza_principal
  
FROM
 
  parcelas as p 
  inner join notas as n on (n.idNotas = p.idNota)
  inner join plano_contas as pl on (n.natureza_movimentacao = pl.ID)
 
    
    
WHERE
(n.status <> 'Processada' and n.status <> 'Cancelada')
and p.status = 'Pendente'
and n.tipo_documento_financeiro_id = '{$filtros['tipo-documento']}'
and p.vencimento_real between '{$filtros['inicio']}' and '{$filtros['fim']}'

{$condNatureza} 
ORDER BY 
vencimento_real, pl.N4 ASC
";    

        return parent::get($sql);
    }

    public static function getParcelaByDadosPrevisao($dados)
    {
        

    $sql = "
     
SELECT 
tb.*,
(tb.valor_rateio - tb.valor_baixa) as max_valor_baixa
from 
(
SELECT 
p.id as id_parcela,
p.idNota as tr,
n.codigo,
p.vencimento_real,
DATE_FORMAT(p.vencimento_real, '%d/%m/%Y') as vencimento_PtBr,
p.valor,
CONCAT(pl.COD_N4, ' - ', pl.N4) as natureza,
ROUND(SUM(p.valor * (r.PORCENTAGEM/100)),2) as valor_rateio,
COALESCE ((
	select
		SUM(valor)
	FROM
		parcela_baixa_previsao as pbp
	where
    pbp.parcela_nota_id = p.id), 0) as valor_baixa,
    e.nome as ur 		
FROM 
  parcelas as p 
  inner join notas as n on (n.idNotas = p.idNota)
  inner join rateio as r  on (n.idNotas = r.NOTA_ID)
  inner join plano_contas as pl on (r.IPCG4 = pl.ID) 
  inner join empresas as e on (n.empresa = e.id)  
WHERE
n.nota_aglutinacao = 'n' and
n.status <> 'Cancelada'
and p.status <> 'Cancelada'

and n.tipo_documento_financeiro_id <> '{$dados['tipo_documento']}'
and YEAR(p.vencimento_real) = YEAR('{$dados['vencimento_real']}')
and MONTH(p.vencimento_real) = MONTH('{$dados['vencimento_real']}')

and r.IPCG4 = {$dados['natureza_movimentacao']}
group by p.id
ORDER BY 
vencimento_real, pl.N4 ASC
) as tb
WHERE 
(tb.valor_rateio - tb.valor_baixa) > 0
   
"; 


        return parent::get($sql);
    }

    public static function baixarParcelaPrevisao($dados)
    {
      mysql_query('begin');
      

      // Associando parcela de previsao com parcelas de notas 
      $sql = "Insert into 
      parcela_baixa_previsao
      (
        parcela_previsao_id,
        parcela_nota_id,
        valor,
        created_by,
        created_at
      ) values {$dados['insert']}
      ";
     
      
      $rs = mysql_query($sql);

      if(!$rs) {
          mysql_query('ROLLBACK');
          $response = ['tipo' => 'erro',
                'msg'=>"Erro ao associar parcelas com parcela de previisão."
              ];                    
          return $response;
      }

       // Atualizando parcela de previsão
       $sql = "update
       parcelas as p
       set
            
       p.status = IF(p.valor - {$dados['valorBaixar']} > 0, p.status, 'Cancelada'),
       p.obs =  IF(p.valor - {$dados['valorBaixar']} > 0,
                    obs, 
                    CONCAT(obs,' Cancelada: Cancelamento por baixa de previsão.')
    ),
    p.valor = p.valor - {$dados['valorBaixar']}
      
       where
       p.id = {$dados['parcelaIdPrevisao']}
       ";
       $rs = mysql_query($sql);

       if(!$rs) {
           mysql_query('ROLLBACK');
           $response = ['tipo' => 'erro',
                 'msg'=>"Erro ao atualizar parcela de previsão."
               ];                    
           return $response;
       }
      // Atualizando nota da parcela de previsão 
      $pendentes =  Parcela::checkIfNotaHasParcelasPendentes($dados['notaIdPrevisao']);
     
      if($pendentes){   
        $sql = "
                Update 
                notas as n
                set
                n.VAL_PRODUTOS = n.VAL_PRODUTOS - {$dados['valorBaixar']},
                n.valor = n.valor - {$dados['valorBaixar']}               
                where
                n.idNotas = {$dados['notaIdPrevisao']}
              ";    
        
      }else{
        $sql = "
                Update 
                notas as n
                set
                n.VAL_PRODUTOS = n.VAL_PRODUTOS - {$dados['valorBaixar']},
                n.valor = n.valor - {$dados['valorBaixar']},
                n.status = 'Cancelada',
                n.descricao = CONCAT(descricao,' Cancelada: Cancelamento por baixa de previsão.')
                where
                n.idNotas = {$dados['notaIdPrevisao']}
              ";
        
      }
     
      $rs = mysql_query($sql);
      if(!$rs) {
        mysql_query('ROLLBACK');
        $response = ['tipo' => 'erro',
              'msg'=>"Erro ao atualizar nota de previsão."
            ];                    
        return $response;
      }
// Atualizando rateio da nota de previsão
      $sql = "
                Update 
                rateio
                set
                VALOR = VALOR - {$dados['valorBaixar']}               
                where
                NOTA_ID = {$dados['notaIdPrevisao']}
              ";
       $rs = mysql_query($sql);
      if(!$rs) {
        mysql_query('ROLLBACK');
        $response = ['tipo' => 'erro',
              'msg'=>"Erro ao atualizar rateio da nota de previsão."
            ];                    
        return $response;
      }   

      mysql_query('commit');

    return  $response = ['tipo' => 'sucesso',
                'msg'=>"Parcela baixada com sucesso."
              ]; 

    }

    public static function cancelarParcelaPrevisao($arrayParcelas)
    {
      mysql_query('begin');
      $canledObs = "Cancelada por ".$_SESSION['nome_user']." em ".date('Y-m-d H:i:s');

      foreach($arrayParcelas as $dados){
       // Atualizando parcela de previsão
       $sql = "update
       parcelas as p
       set            
       p.status = 'Cancelada',
       p.obs =   '{$canledObs}'                
      
       where
       p.id = {$dados['parcela_previsao_id']}
       ";
       $rs = mysql_query($sql);
       

       if(!$rs) {
           mysql_query('ROLLBACK');
           $response = ['tipo' => 'erro',
                 'msg'=>"Erro ao cancelar parcela de previsão."
               ];                    
           return $response;
       }

       
      // Atualizando nota da parcela de previsão 
      $pendentes =  Parcela::checkIfNotaHasParcelasPendentes($dados['nota_id']);
   
      if($pendentes){   
        $sql = "
                Update 
                notas as n
                set
                n.valor = n.valor - {$dados['valor']}               
                where
                n.idNotas = {$dados['nota_id']}
              ";    
        
      }else{
        $sql = "
                Update 
                notas as n
                set
                n.valor = n.valor - {$dados['valor']},
                n.status = 'Cancelada',
                n.descricao = CONCAT(descricao,' {$canledObs}')
                where
                n.idNotas = {$dados['nota_id']}
              ";
        
      }
     
      $rs = mysql_query($sql);
      if(!$rs) {
        mysql_query('ROLLBACK');
        $response = ['tipo' => 'erro',
              'msg'=>"Erro ao atualizar nota de previsão."
            ];                    
        return $response;
      }
// Atualizando rateio da nota de previsão
      $sql = "
                Update 
                rateio
                set
                VALOR = VALOR - {$dados['valor']}               
                where
                NOTA_ID = {$dados['nota_id']}
              ";
       $rs = mysql_query($sql);
      if(!$rs) {
        mysql_query('ROLLBACK');
        $response = ['tipo' => 'erro',
              'msg'=>"Erro ao atualizar rateio da nota de previsão."
            ];                    
        return $response;
      }   
    }

      mysql_query('commit');

    return  $response = ['tipo' => 'sucesso',
                'msg'=>"Parcela(s) cancelada(s) com sucesso."
              ]; 

    }
}