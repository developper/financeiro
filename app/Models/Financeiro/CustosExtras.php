<?php
namespace app\Models\Financeiro;

require_once($_SERVER['DOCUMENT_ROOT'].'/utils/codigos.php');

use App\Models\AbstractModel;
use App\Models\DAOInterface;
//ini_set('display_errors',1);
//ini_set('display_startup_erros',1);
//error_reporting(E_ALL);



class CustosExtras extends AbstractModel implements DAOInterface
{

	public static function getAll()
	{
		return parent::get(self::getSQL());
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? "AND id = '{$id}'" : "";
		return "SELECT * FROM custos_extras WHERE 1=1 {$id} ORDER BY nome";
	}

    public static function getAtivos(){
        $sql = <<<SQL
        SELECT * FROM custos_extras WHERE status = 'A' ORDER BY nome
SQL;
        return parent::get($sql);
    }

    public static function salvar($custo)
    {
        $sql = <<<SQL
        Insert INTO
        custos_extras
        (
        nome,
        created_by,
        created_at
        )
        VALUES
        (
        '$custo',
        {$_SESSION['id_user']},
        now()
        )

SQL;
        $r = mysql_query($sql);
        if (!$r) {
            echo "Erro ao salvar Custo Extra";
            return;
        }

        savelog(mysql_escape_string(addslashes($sql)));

        echo 1;

    }

    public static function desativarById($id)
    {
        $sql = <<<SQL
        UPDATE
        custos_extras
       SET
       status = 'D',
       updated_by = {$_SESSION['id_user']},
       updated_at = now()
       WHERE
       id = {$id}

SQL;
        $r = mysql_query($sql);
        if (!$r) {
            echo "Erro ao desativar Custo Extra";
            return;
        }

        savelog(mysql_escape_string(addslashes($sql)));

        echo 1;
    }

    public static function ativarById($id)
    {
        $sql = <<<SQL
        UPDATE
        custos_extras
       SET
       status = 'A',
       updated_by = {$_SESSION['id_user']},
       updated_at = now()
       WHERE
       id = {$id}

SQL;
        $r = mysql_query($sql);
        if (!$r) {
            echo "Erro ao ativar Custo Extra";
            return;
        }

        savelog(mysql_escape_string(addslashes($sql)));

        echo 1;
    }

    public static function getCustoExtraMesAnterior($pacienteID, $dataDocumentoAtual, $tipoDocumentoAtual)
    {
        $sql = <<<SQL
        SELECT
fatura_custos_extras.*,
 custos_extras.nome
from
fatura_custos_extras
INNER JOIN
fatura on fatura_custos_extras.id_fatura = fatura.ID
INNER JOIN
custos_extras on fatura_custos_extras.id_custo_extra = custos_extras.id
WHERE
YEAR(fatura.DATA_INICIO) = YEAR('{$dataDocumentoAtual}' - INTERVAL 1 MONTH)
AND
MONTH(fatura.DATA_INICIO) = MONTH('{$dataDocumentoAtual}' - INTERVAL 1 MONTH)
AND
fatura.PACIENTE_ID = $pacienteID
AND
fatura.ORCAMENTO = $tipoDocumentoAtual
AND
fatura_custos_extras.canceled_at is null
AND
fatura.CANCELADO_EM is null
SQL;

       
        return parent::get($sql);

        
    }



}