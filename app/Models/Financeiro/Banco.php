<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class Banco extends AbstractModel implements DAOInterface
{
    public static function getAll()
    {
        return parent::get(self::getSQL());
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? 'WHERE 1 = 1 AND origemfundos.id = ' . $id : null;
        return "SELECT *
            FROM
            origemfundos
            {$id}
            ORDER BY forma ASC";
    }

    public static function getBancoByCodigo($codigo)
    {
        $sql = <<<SQL
SELECT *
            FROM
            origemfundos
            WHERE
            COD_BANCO = '{$codigo}'
SQL;
        return parent::get($sql, 'Assoc');
    }
}
