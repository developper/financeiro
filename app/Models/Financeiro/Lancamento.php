<?php
namespace App\Models\Financeiro;

use App\Core\Log,
    \App\Models\AbstractModel,
    \App\Models\DAOInterface,
    \App\Models\Financeiro\TransferenciaBancaria as Transferencias,
    \App\Models\Financeiro\TarifaImposto,
    \App\Helpers\StringHelper as String;


class Lancamento extends AbstractModel implements DAOInterface
{
    public static function getAll ()
    {
        return parent::get (self::getSQL ());
    }

    public static function getById ($tipo, $id)
    {
        return parent::get (self::getSQL ($tipo, $id));
    }

    public static function getSQL ($id = null)
    {
        return '';
    }

    public static function getCentroResultadosById ($tipo, $valor, $id)
    {
        return parent::get (self::getCentroResultado ($tipo, $valor, $id));
    }


    public static function getCentroResultado ($tipo, $valor, $id = null)
    {
        $sql = <<<SQL
SELECT
	r.ID,
	e.centro_custo_dominio AS centro_custo_debito,
	(
		SELECT
			e1.centro_custo_dominio
		FROM
			rateio AS r1
		INNER JOIN parcelas AS p1 ON (p1.idNota = r1.NOTA_ID)
		INNER JOIN contas_bancarias AS cb1 ON (p1.origem = cb1.ID)
		INNER JOIN empresas AS e1 ON (e1.id = cb1.UR)
		WHERE
			r1.NOTA_ID = {$id}
		GROUP BY
			e1.centro_custo_dominio
	) AS centro_custo_credito,
	{$valor} AS valor_lancamento,
	r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN parcelas AS p ON (r.NOTA_ID = p.idNota)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
WHERE
	r.NOTA_ID = {$id}
GROUP BY
	r.ID
SQL;
        return $sql;
    }

    public function getLancamentosNotas ($periodo, $type = "inclusao")
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "inclusao"){ 
            # REGRAS UTILIZADAS QUANDO O TIPO DE ARQUIVO ESCOLHIDO FOR INCLUSÃO

            $sql = <<<SQL
SELECT
	/* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO DEBITO SERA COMPOSTO 
	   * PELA ID DA NOTA + NOME DO CLIENTE * SENAO E 1 (DESEMBOLSO), LOGO, 
	   SERA PELO ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR */
IF (
	n.tipo = 0,
	CONCAT(
		'VL. REF. DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'VL. REF. DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS debito_historico,
 /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO CREDITO SERA COMPOSTO 
    * POR UM PREFIXO 'REC. DOC.' + ID DA NOTA + NOME DO CLIENTE * SENAO E 1 (DESEMBOLSO), LOGO, 
    SERA POR UM PREFIXO 'PG. DOC.' + ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR */
IF (
	n.tipo = 0,
	CONCAT(
		'VL. REF. DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'VL. REF. DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS credito_historico,
 /* O HISTORICO DAS RETENCOES SERA COMPOSTO 
    * POR TIPO RETENÇÃO + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO) 
    * OU RAZAO SOCIAL DO FORNECEDOR SE O
    TIPO FOR 1 (DESEMBOLSO) */
IF (
	n.tipo = 0,
	CONCAT(n.codigo, ' ', f.nome),
	CONCAT(n.codigo, ' ', f.razaoSocial)
) AS retencoes_historico,
 n.tipo,
 n.valor,
 n.VAL_PRODUTOS AS valor_bruto,
 n.descricao,
 'n' AS origem,
 'n' AS destino,
 'n' AS conta_tarifa,
 n.idNotas,
 n.IR,
 n.PIS,
 n.COFINS,
 n.CSLL,
 n.PCC,
 n.ISSQN,
 n.INSS,
 DATE_FORMAT(n.dataEntrada, '%d/%m/%Y') AS data_lancamento,
 's' AS lancamento_comum,
 'n' AS nota_aglutinacao,
 'n' AS nota_adiantamento,
 'n' AS nota_compensacao,
 'n' AS nota_baixa,
 'n' AS conciliado,
 'N' AS nota_informe_perda,
 e.centro_custo_dominio
FROM
	notas AS n
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN rateio AS r ON r.NOTA_ID = n.idNotas
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN empresas AS e ON (e.id = n.empresa)
LEFT JOIN tipo_documento_financeiro AS tdf ON n.tipo_documento_financeiro_id = tdf.id
WHERE
	n.dataEntrada {$interval}
AND n.`status` != 'Cancelada'
AND pc.PROVISIONA = 's'
AND n.nota_aglutinacao = 'n'
AND n.nota_adiantamento = 'N'
AND n.TIPO_DOCUMENTO != 'PR'
AND tdf.sigla != 'PR'
GROUP BY
	n.idNotas
ORDER BY
	data_lancamento
SQL;
        } else {

            # REGRAS UTILIZADAS QUANDO O TIPO DE ARQUIVO ESCOLHIDO FOR BAIXA
            $sql = <<<SQL
SELECT
	/* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO DEBITO SERA COMPOSTO
 * PELA ID DA NOTA + NOME DO CLIENTE
 * SENAO E 1 (DESEMBOLSO), LOGO, SERA PELO ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
 */
IF (
	n.tipo = 0,
	CONCAT(
		'REC. DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'PG. DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS debito_historico,
 /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO CREDITO SERA COMPOSTO
 * POR UM PREFIXO 'REC. DOC.' + ID DA NOTA + NOME DO CLIENTE
 * SENAO E 1 (DESEMBOLSO), LOGO, SERA POR UM PREFIXO 'PG. DOC.' + ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
 */
IF (
	n.tipo = 0,
	CONCAT(
		'REC. DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'PG. DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS credito_historico,
 /* SE O CAMPO JUROS/MULTA NAO FOR VAZIO, PROCEDE:
 * O HISTORICO DO JUROS/MULTA SERA COMPOSTO
 * POR UM PREFIXO 'Juros: ' + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
 * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
 */
IF (
	p.JurosMulta != 0,
	CONCAT(
		'VL. REF. JUROS DOC ',
		n.codigo,
		' ',
        IF (
            n.tipo = 0,
            f.nome,
            f.razaoSocial
        )
    ),
	''
) AS juros_historico,
/* SE O CAMPO Trifa carão de Credito NAO FOR VAZIO, PROCEDE:
 * O HISTORICO DO Trifa carão de CreditoSERA COMPOSTO
 * POR UM PREFIXO 'Trifa carão de Credito: ' + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
 * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
 */
IF (
	p.tarifa_cartao != 0,
	CONCAT(
		'VL. REF. TARIFA CARTAO ',
		n.codigo,
		' ',
        IF (
            n.tipo = 0,
            f.nome,
            f.razaoSocial
        )
    ),
	''
) AS tarifa_cartao_historico,
 /* SE O CAMPO DESCONTO NAO FOR VAZIO, PROCEDE:
 * O HISTORICO DO DESCONTO SERA COMPOSTO
 * POR UM PREFIXO 'Juros: ' + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
 * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
 */
IF (
	p.desconto != 0,
	CONCAT(
		'VL. REF. DESCONTO DOC ',
		n.codigo,
		' ',
        IF (
            n.tipo = 0,
            f.nome,
            f.razaoSocial
        )
	),
	''
) AS desconto_historico,
 /* O HISTORICO DAS RETENCOES SERA COMPOSTO
 * POR TIPO DE RETENÇÃO + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
 * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
 */
IF (
	n.tipo = 0,
	CONCAT(n.codigo, ' ', f.nome),
	CONCAT(n.codigo, ' ', f.razaoSocial)
) AS retencoes_historico,
 (
	p.valor
) AS valor,
 p.JurosMulta,
 p.desconto,
 p.OUTROS_ACRESCIMOS AS outras_tarifas,
 p.encargos,
 p.tarifa_cartao,
 n.tipo,
 n.descricao,
 'n' AS origem,
 'n' AS destino,
 'n' AS conta_tarifa,
 p.id AS idNotas,
 DATE_FORMAT(
	p.DATA_CONCILIADO,
	'%d/%m/%Y'
) AS data_lancamento,
 's' AS lancamento_comum,
 'n' AS nota_aglutinacao,
 'n' AS nota_adiantamento,
 'n' AS nota_compensacao,
 'n' AS nota_baixa,
 'N' AS nota_informe_perda,
 p.CONCILIADO AS conciliado,
 e.centro_custo_dominio
 
FROM
	parcelas AS p
INNER JOIN notas AS n ON n.idNotas = p.idNota
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN rateio AS r ON r.NOTA_ID = n.idNotas
INNER JOIN empresas AS e ON (e.id = n.empresa)
WHERE
p.DATA_CONCILIADO {$interval}
AND p.`status` = 'Processada'
AND n.status != 'Cancelada'
AND n.nota_adiantamento = 'N'
AND p.CONCILIADO = 'S'
AND (p.nota_id_fatura_aglutinacao is null or p.nota_id_fatura_aglutinacao = 0) 

 GROUP BY
	  p.id
ORDER BY
    data_lancamento
SQL;

        }

        return parent::get($sql);
    }

    public static function getLancamentosTransferenciaBancaria($periodo, $type = 'baixa')
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "baixa") {
            $sql = <<<SQL
SELECT
CONCAT(
        'TRANSFERENCIA ENTRE CONTAS ',
        (
            SELECT
			 CONCAT(
				tcb.abreviacao,
				' - ',
				of.forma,
				': ',
				cb.AGENCIA,
				' - ',
				cb.NUM_CONTA
			) AS conta
		FROM
			contas_bancarias AS cb
		INNER JOIN origemfundos AS of ON (cb.ORIGEM_FUNDOS_ID = of.id)
		INNER JOIN tipo_conta_bancaria AS tcb ON (
			cb.TIPO_CONTA_BANCARIA_ID = tcb.id
		) 
                    WHERE
         cb.ID = tb.CONTA_ORIGEM
        ),
        ' / ',
        (
            SELECT
			 CONCAT(
				tcb.abreviacao,
				' - ',
				of.forma,
				': ',
				cb.AGENCIA,
				' - ',
				cb.NUM_CONTA
			) AS conta
		FROM
			contas_bancarias AS cb
		INNER JOIN origemfundos AS of ON (cb.ORIGEM_FUNDOS_ID = of.id)
		INNER JOIN tipo_conta_bancaria AS tcb ON (
			cb.TIPO_CONTA_BANCARIA_ID = tcb.id
		) 
            WHERE
         cb.ID = tb.CONTA_DESTINO
        )
    ) AS debito_historico,
    CONCAT(
        'TRANSFERENCIA ENTRE CONTAS ',
        (
            SELECT
			 CONCAT(
				tcb.abreviacao,
				' - ',
				of.forma,
				': ',
				cb.AGENCIA,
				' - ',
				cb.NUM_CONTA
			) AS conta
		FROM
			contas_bancarias AS cb
		INNER JOIN origemfundos AS of ON (cb.ORIGEM_FUNDOS_ID = of.id)
		INNER JOIN tipo_conta_bancaria AS tcb ON (
			cb.TIPO_CONTA_BANCARIA_ID = tcb.id
		) 
            WHERE
         cb.ID = tb.CONTA_ORIGEM
        ),
        ' / ',
        (
            SELECT
			 CONCAT(
				tcb.abreviacao,
				' - ',
				of.forma,
				': ',
				cb.AGENCIA,
				' - ',
				cb.NUM_CONTA
			) AS conta
		FROM
			contas_bancarias AS cb
		INNER JOIN origemfundos AS of ON (cb.ORIGEM_FUNDOS_ID = of.id)
		INNER JOIN tipo_conta_bancaria AS tcb ON (
			cb.TIPO_CONTA_BANCARIA_ID = tcb.id
		) 
            WHERE
         cb.ID = tb.CONTA_DESTINO
        )
    ) AS credito_historico,
    '' AS juros_historico,
    '' AS desconto_historico,
    '' AS retencoes_historico,
    tb.VALOR AS valor,
    0 AS JurosMulta,
    0 AS desconto,
    0 AS outras_tarifas,
    0 AS encargos,
    0 AS tipo,
    '' AS descricao,
    tb.CONTA_ORIGEM AS origem,
    tb.CONTA_DESTINO AS destino,
    't' AS conta_tarifa,
    concat('t',tb.ID) AS idNotas,
    DATE_FORMAT(
        tb.DATA_TRANSFERENCIA,
        '%d/%m/%Y'
    ) AS data_lancamento,
    'transferencia' AS lancamento_comum,
    'n' AS nota_aglutinacao,
    'n' AS nota_adiantamento,
    'n' AS nota_compensacao,
    'n' AS nota_baixa,
    'n' AS nota_informe_perda,
    'S' AS conciliado
FROM
    transferencia_bancaria AS tb
INNER JOIN contas_bancarias AS cb1 ON (tb.CONTA_ORIGEM = cb1.ID)
INNER JOIN contas_bancarias AS cb2 ON (tb.CONTA_DESTINO = cb2.ID)
LEFT JOIN plano_contas_contabil AS pcc1 ON (cb1.IPCC = pcc1.ID)
LEFT JOIN plano_contas_contabil AS pcc2 ON (cb2.IPCC = pcc2.ID)
WHERE
    tb.CONCILIADO_DESTINO = 'S'
AND tb.CONCILIADO_ORIGEM = 'S'
-- AND (tb.CONTA_ORIGEM = 6 OR tb.CONTA_DESTINO = 6)
AND tb.DATA_CONCILIADO_ORIGEM {$interval}
ORDER BY
  data_lancamento
SQL;
            return parent::get($sql, 'Assoc');
        }
        return [];
    }

    public static function getLancamentosTarifasBancaria($periodo, $type = 'baixa')
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "baixa") {
            $sql = <<<SQL
SELECT
    CONCAT(it.tipo, ' ', it.item) AS debito_historico,
    CONCAT(it.tipo, ' ', it.item) AS credito_historico,
    '' AS juros_historico,
    '' AS desconto_historico,
    '' AS retencoes_historico,
    itl.valor AS valor,
    0 AS JurosMulta,
    0 AS desconto,
    0 AS outras_tarifas,
    0 AS encargos,
    IF(it.debito_credito = 'DEBITO', 1, 0) AS tipo,
    '' AS descricao,
    'i' AS origem,
    'i' AS destino,
    it.tipo,
    it.item AS descricao_item,
    itl.conta AS conta_tarifa,
    concat('i',itl.id) AS idNotas,
    DATE_FORMAT(itl.`data`, '%d/%m/%Y') AS data_lancamento,
    'imposto_tarifa' AS lancamento_comum,
    'n' AS nota_aglutinacao,
    'n' AS nota_adiantamento,
    'n' AS nota_compensacao,
    'n' AS nota_baixa,
    'n' AS nota_informe_perda,
    'S' AS conciliado,
    ep.credito_id as estorno_parcela
FROM
impostos_tarifas_lancto AS itl
INNER JOIN impostos_tarifas AS it ON (it.id = itl.item)
INNER JOIN contas_bancarias AS cb ON (cb.id = itl.conta)
INNER JOIN plano_contas AS pc ON (pc.COD_N4 = it.ipcf)
LEFT JOIN plano_contas_contabil AS pcc ON (pcc.COD_N5 = pc.COD_IPCC)
LEFT JOIN estorno_parcela as ep on (itl.id = ep.credito_id)

WHERE
itl.`data` {$interval}

SQL;
            return parent::get($sql);
        }
        return [];
    }

    public static function getLancamentosEstornosParcelas($periodo, $type = 'baixa')
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "baixa") {
            $sql = <<<SQL
SELECT
	/* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO DEBITO SERA COMPOSTO
 * PELA ID DA NOTA + NOME DO CLIENTE
 * SENAO E 1 (DESEMBOLSO), LOGO, SERA PELO ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
 */
IF (
	n.tipo = 0,
	CONCAT(
		'ESTORNO DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'ESTORNO DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS debito_historico,
 /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO CREDITO SERA COMPOSTO
 * POR UM PREFIXO 'REC. DOC.' + ID DA NOTA + NOME DO CLIENTE
 * SENAO E 1 (DESEMBOLSO), LOGO, SERA POR UM PREFIXO 'PG. DOC.' + ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
 */
IF (
	n.tipo = 0,
	CONCAT(
		'ESTORNO DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'ESTORNO DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS credito_historico,
'' AS juros_historico,
'' AS desconto_historico,
IF (
	n.tipo = 0,
	CONCAT(n.codigo, ' ', f.nome),
	CONCAT(n.codigo, ' ', f.razaoSocial)
) AS retencoes_historico,
 ep.valor,
0 as  JurosMulta,
 0 as desconto,
 0 AS outras_tarifas,
 0 as encargos,
 n.tipo,
 n.descricao,
 'n' AS origem,
 'n' AS destino,
 'n' AS conta_tarifa,
 ep.id AS idNotas,
 DATE_FORMAT(
	ep.data_conciliada,
	'%d/%m/%Y'
) AS data_lancamento,
 'estorno' AS lancamento_comum,
 'n' AS nota_aglutinacao,
 'n' AS nota_adiantamento,
 'n' AS nota_compensacao,
 'n' AS nota_baixa,
 'n' AS nota_informe_perda,
 'S' AS conciliado,

 e.centro_custo_dominio
FROM
  estorno_parcela AS ep
INNER JOIN parcelas AS p ON p.id = ep.parcela_id
INNER JOIN notas AS n ON n.idNotas = p.idNota
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
 INNER JOIN rateio AS r ON r.NOTA_ID = n.idNotas
INNER JOIN contas_bancarias AS cb ON cb.id = ep.origem
 INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN empresas AS e ON (e.id = n.empresa)
WHERE
    ep.data_conciliada {$interval}

GROUP BY
    ep.id
ORDER BY
    data_lancamento
SQL;
            return parent::get($sql, 'Assoc');
        }
        return [];
    }

    public static function getLancamentosAntecipacao($periodo, $type = 'baixa')
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "baixa") {
            $sql = <<<SQL
SELECT
    /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO DEBITO SERA COMPOSTO
     * PELA ID DA NOTA + NOME DO CLIENTE
     * SENAO E 1 (DESEMBOLSO), LOGO, SERA PELO ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
     */
    IF (
        n.tipo = 0,
        CONCAT(
            'REC. ANTECIPADO DE ',
            n.codigo,
            ' ',
            f.nome
        ),
        CONCAT(
            'PG. ANTECIPADO A ',
            n.codigo,
            ' ',
            f.razaoSocial
        )
    ) AS debito_historico,
     /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO CREDITO SERA COMPOSTO
     * POR UM PREFIXO 'REC. DOC.' + ID DA NOTA + NOME DO CLIENTE
     * SENAO E 1 (DESEMBOLSO), LOGO, SERA POR UM PREFIXO 'PG. DOC.' + ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
     */
    IF (
        n.tipo = 0,
        CONCAT(
            'REC. ANTECIPADO DE ',
            n.codigo,
            ' ',
            f.nome
        ),
        CONCAT(
            'PG. ANTECIPADO A ',
            n.codigo,
            ' ',
            f.razaoSocial
        )
    ) AS credito_historico,
     /* SE O CAMPO JUROS/MULTA NAO FOR VAZIO, PROCEDE:
     * O HISTORICO DO JUROS/MULTA SERA COMPOSTO
     * POR UM PREFIXO 'Juros: ' + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
     * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
     */
    IF (
        p.JurosMulta != 0,
        CONCAT(
            'VL. REF. JUROS DOC ',
            n.codigo,
            ' ',
    
        IF (
            n.tipo = 0,
            f.nome,
            f.razaoSocial
        )
        ),
        ''
    ) AS juros_historico,
     /* SE O CAMPO DESCONTO NAO FOR VAZIO, PROCEDE:
     * O HISTORICO DO DESCONTO SERA COMPOSTO
     * POR UM PREFIXO 'Juros: ' + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
     * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
     */
    IF (
        p.desconto != 0,
        CONCAT(
            'VL. REF. DESCONTO DOC ',
            n.codigo,
            ' ',
    
        IF (
            n.tipo = 0,
            f.nome,
            f.razaoSocial
        )
        ),
        ''
    ) AS desconto_historico,
     /* O HISTORICO DAS RETENCOES SERA COMPOSTO
     * POR TIPO DE RETENÇÃO + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
     * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
     */
    IF (
        n.tipo = 0,
        CONCAT(n.codigo, ' ', f.nome),
        CONCAT(n.codigo, ' ', f.razaoSocial)
    ) AS retencoes_historico,
     (
        p.valor
    ) AS valor,
     p.JurosMulta,
     p.desconto,
     p.OUTROS_ACRESCIMOS AS outras_tarifas,
     p.encargos,
     n.tipo,
     n.descricao,
     'n' AS origem,
     'n' AS destino,
     'n' AS conta_tarifa,
     p.id AS idNotas,
     DATE_FORMAT(
        p.DATA_CONCILIADO,
        '%d/%m/%Y'
    ) AS data_lancamento,
    'antecipacao' AS lancamento_comum,
    'n' AS nota_aglutinacao,
    's' AS nota_adiantamento,
    'n' AS nota_compensacao,
    'n' AS nota_baixa,
    'n' AS nota_informe_perda,
    p.CONCILIADO AS conciliado
FROM
    parcelas AS p
INNER JOIN notas AS n ON n.idNotas = p.idNota
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores

WHERE
    p.DATA_CONCILIADO {$interval}
AND p.`status` = 'Processada'
AND n.nota_aglutinacao = 'n'
AND n.nota_adiantamento = 'S'

ORDER BY 
    data_lancamento
SQL;
            return parent::get($sql, 'Assoc');
        }
        return [];
    }

    public static function getLancamentosCompensacao($periodo, $type = 'baixa')
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "baixa") {
            $sql = <<<SQL
SELECT
    IF (
        n.tipo = 0,
        'COMPENSACAO DE REC. ANTECIPADO',
        'COMPENSACAO DE PG. ANTECIPADO'
    ) AS debito_historico,
    IF (
        n.tipo = 0,
        'COMPENSACAO DE REC. ANTECIPADO',
        'COMPENSACAO DE PG. ANTECIPADO'
    ) AS credito_historico,
    IF (
        p.JurosMulta != 0,
        CONCAT(
            'VL. REF. JUROS DOC ',
            n.codigo,
            ' ',
            IF (
                n.tipo = 0,
                f.nome,
                f.razaoSocial
            )
        ),
        ''
    ) AS juros_historico,
    IF (
        p.desconto != 0,
        CONCAT(
            'VL. REF. DESCONTO DOC ',
            n.codigo,
            ' ',
        IF (
            n.tipo = 0,
            f.nome,
            f.razaoSocial
        )
        ),
        ''
    ) AS desconto_historico,
    '' AS retencoes_historico,
     nc.valor AS valor,
     p.JurosMulta,
     p.desconto,
     p.OUTROS_ACRESCIMOS AS outras_tarifas,
     p.encargos,
     n.tipo,
     n.descricao,
     'n' AS origem,
     'n' AS destino,
     'n' AS conta_tarifa,
     p.idNota AS idNotas,
     DATE_FORMAT(
        nc.data_compensado,
        '%d/%m/%Y'
    ) AS data_lancamento,
    'compensacao' AS lancamento_comum,
    'n' AS nota_aglutinacao,
    'n' AS nota_adiantamento,
    's' AS nota_compensacao,
    'n' AS nota_baixa,
    'n' AS nota_informe_perda,
    p.CONCILIADO AS conciliado
FROM
    parcelas AS p
    INNER JOIN nota_compensacao AS nc ON (nc.parcela_id = p.id)
    INNER JOIN notas AS n ON n.idNotas = p.idNota     
    INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
   
WHERE
    nc.data_compensado {$interval}
AND p.`status` = 'Processada'
and n.status <> 'Cancelada'

AND n.nota_aglutinacao = 'n'
AND n.nota_adiantamento = 'n'
AND nc.deleted_by IS NULL
union
	SELECT
IF
	( n.tipo = 0, 'COMPENSACAO DE REC. ANTECIPADO', 'COMPENSACAO DE PG. ANTECIPADO' ) AS debito_historico,
IF
	( n.tipo = 0, 'COMPENSACAO DE REC. ANTECIPADO', 'COMPENSACAO DE PG. ANTECIPADO' ) AS credito_historico,
IF
	(
	p.JurosMulta != 0,
	CONCAT( 'VL. REF. JUROS DOC ', n.codigo, ' ', IF ( n.tipo = 0, f.nome, f.razaoSocial ) ),
	'' 
	) AS juros_historico,
IF
	(
	p.desconto != 0,
	CONCAT( 'VL. REF. DESCONTO DOC ', n.codigo, ' ', IF ( n.tipo = 0, f.nome, f.razaoSocial ) ),
	'' 
	) AS desconto_historico,
	'' AS retencoes_historico,
	nc.valor AS valor,
	p.JurosMulta,
	p.desconto,
	p.OUTROS_ACRESCIMOS AS outras_tarifas,
	p.encargos,
	n.tipo,
	n.descricao,
	'n' AS origem,
	'n' AS destino,
	'n' AS conta_tarifa,
	p.idNota AS idNotas,
	DATE_FORMAT( nc.data_compensado, '%d/%m/%Y' ) AS data_lancamento,
	'compensacao' AS lancamento_comum,
	'n' AS nota_aglutinacao,
	'n' AS nota_adiantamento,
	's' AS nota_compensacao,
	'n' AS nota_baixa,
    'n' AS nota_informe_perda,
	p.CONCILIADO AS conciliado 
FROM
	parcelas AS p
	INNER JOIN nota_compensacao AS nc ON ( nc.parcela_pivo_id = p.id)
	INNER JOIN notas AS n ON n.idNotas = p.idNota
	
	INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
	
WHERE
	nc.data_compensado {$interval}
	AND p.`status` = 'Processada' 
    and n.status <> 'Cancelada'
	AND n.nota_aglutinacao = 'n' 
	AND n.nota_adiantamento = 'n' 
	AND nc.deleted_by IS NULL
ORDER BY
    data_lancamento
SQL;



            return parent::get($sql, 'Assoc');
        }

        return [];
    }

    public static function getLancamentosBaixasAntecipacao($periodo, $type = 'baixa')
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "baixa") {
            $sql = <<<SQL
SELECT
    IF (
        n.tipo = 0,
        'BAIXA DE REC. ANTECIPADO',
        'BAIXA DE PG. ANTECIPADO'
    ) AS debito_historico,
    IF (
        n.tipo = 0,
        'BAIXA DE REC. ANTECIPADO',
        'BAIXA DE PG. ANTECIPADO'
    ) AS credito_historico,
    '' AS juros_historico,
    '' AS desconto_historico,
    '' AS retencoes_historico,
     nb.valor AS valor,
     '0' AS JurosMulta,
     '0' AS desconto,
     '0' AS outras_tarifas,
     '0' AS encargos,
     n.tipo,
     n.descricao,
     'n' AS origem,
     'n' AS destino,
     'n' AS conta_tarifa,
     nb.id AS idNotas,
     DATE_FORMAT(
        nb.data_conciliado,
        '%d/%m/%Y'
    ) AS data_lancamento,
    'baixa' AS lancamento_comum,
    'n' AS nota_aglutinacao,
    'n' AS nota_adiantamento,
    'n' AS nota_compensacao,
    's' AS nota_baixa,
    'n' AS nota_informe_perda,
    'S' AS conciliado
FROM
    nota_baixas AS nb
    INNER JOIN notas AS n ON nb.nota_id = n.idNotas
    INNER JOIN contas_bancarias AS cb ON cb.id = nb.conta_bancaria_id
    INNER JOIN plano_contas AS pc ON (nb.plano_conta_id = pc.ID)
WHERE
     nb.data_conciliado {$interval}
AND nb.canceled_by IS NULL
ORDER BY
    data_lancamento
SQL;
            return parent::get($sql, 'Assoc');
        }
        return [];
    }

    public static function getLancamentosAglutinacao($periodo, $type = 'baixa')
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "inclusao" ) {
            $sql = <<<SQL
SELECT
    /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO DEBITO SERA COMPOSTO
     * PELA ID DA NOTA + NOME DO CLIENTE
     * SENAO E 1 (DESEMBOLSO), LOGO, SERA PELO ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
     */
    IF (
        n.tipo = 0,
        CONCAT(
            'AGLUTINACAO DE REC. ',
            n.codigo,
            ' ',
            f.nome
        ),
        CONCAT(
            'AGLUTINACAO DE PG. ',
            n.codigo,
            ' ',
            f.razaoSocial
        )
    ) AS debito_historico,
     /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO CREDITO SERA COMPOSTO
     * POR UM PREFIXO 'REC. DOC.' + ID DA NOTA + NOME DO CLIENTE
     * SENAO E 1 (DESEMBOLSO), LOGO, SERA POR UM PREFIXO 'PG. DOC.' + ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
     */
    IF (
        n.tipo = 0,
        CONCAT(
            'AGLUTINACAO DE REC. ',
            n.codigo,
            ' ',
            f.nome
        ),
        CONCAT(
            'AGLUTINACAO DE PG. ',
            n.codigo,
            ' ',
            f.razaoSocial
        )
    ) AS credito_historico,
     /* SE O CAMPO JUROS/MULTA NAO FOR VAZIO, PROCEDE:
     * O HISTORICO DO JUROS/MULTA SERA COMPOSTO
     * POR UM PREFIXO 'Juros: ' + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
     * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
     */
    IF (
        p.JurosMulta != 0,
        CONCAT(
            'VL. REF. JUROS DOC ',
            n.codigo,
            ' ',
            IF (
                n.tipo = 0,
                f.nome,
                f.razaoSocial
            )
        ),
        ''
    ) AS juros_historico,
     /* SE O CAMPO DESCONTO NAO FOR VAZIO, PROCEDE:
     * O HISTORICO DO DESCONTO SERA COMPOSTO
     * POR UM PREFIXO 'Juros: ' + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
     * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
     */
    IF (
        p.desconto != 0,
        CONCAT(
            'VL. REF. DESCONTO DOC ',
            n.codigo,
            ' ',
            IF (
                n.tipo = 0,
                f.nome,
                f.razaoSocial
            )
        ),
        ''
    ) AS desconto_historico,
     /* O HISTORICO DAS RETENCOES SERA COMPOSTO
     * POR TIPO DE RETENÇÃO + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
     * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
     */
    IF (
        n.tipo = 0,
        CONCAT(n.codigo, ' ', f.nome),
        CONCAT(n.codigo, ' ', f.razaoSocial)
     ) AS retencoes_historico,
     p.valor AS valor,
     p.JurosMulta,
     p.desconto,
     p.OUTROS_ACRESCIMOS AS outras_tarifas,
     p.encargos,
     n.tipo,
     n.descricao,
     'n' AS origem,
     'n' AS destino,
     'n' AS conta_tarifa,
     p.id AS idNotas,
     DATE_FORMAT(
        n.dataEntrada,
        '%d/%m/%Y'
     ) AS data_lancamento,
     'aglutinacao' AS lancamento_comum,
     's' AS nota_aglutinacao,
     'n' AS nota_adiantamento,
     'n' AS nota_compensacao,
     'n' AS nota_baixa,
     'n' AS nota_informe_perda,
     'N' AS conciliado,
     
     p.nota_id_fatura_aglutinacao as idNotaAglutinacao     
      

    FROM
        parcelas AS p
    INNER JOIN notas AS n ON n.idNotas = p.nota_id_fatura_aglutinacao
    INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
    
    WHERE
        n.dataEntrada {$interval}
    
    AND n.nota_aglutinacao = 's'
    AND n.nota_adiantamento = 'N'
    and p.status <> 'Cancelada'
    and n.`status` <> 'Cancelada'  

    
ORDER BY
    data_lancamento
SQL;
            return parent::get($sql, 'Assoc');
        }
        return [];
    }

    public function getLancamentosInformePerda($periodo, $type = 'baixa'){
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "baixa") {
            $sql = <<<SQL
SELECT
    IF (
        n.tipo = 0,
        'INF. PERDA REC',
        'INF. PERDA PG'
    ) AS debito_historico,
    IF (
        n.tipo = 0,
        'INF. PERDA REC',
        'INF. PERDA PG'
    ) AS credito_historico,
    '' AS juros_historico,
    '' AS desconto_historico,
    '' AS retencoes_historico,
     nip.valor AS valor,
     '0' AS JurosMulta,
     '0' AS desconto,
     '0' AS outras_tarifas,
     '0' AS encargos,
     n.tipo,
     n.descricao,
     'n' AS origem,
     'n' AS destino,
     'n' AS conta_tarifa,
     nip.id AS idNotas,
     DATE_FORMAT(
        nip.data,
        '%d/%m/%Y'
    ) AS data_lancamento,
    'informe_perda' AS lancamento_comum,
    'n' AS nota_aglutinacao,
    'n' AS nota_adiantamento,
    'n' AS nota_compensacao,
    'n' AS nota_baixa,
    's' AS nota_informe_perda,
    'S' AS conciliado
FROM
    nota_informe_perda AS nip
    INNER JOIN notas AS n ON nip.nota_id = n.idNotas
    inner join parcelas as p on nip.parcela_id = p.id
WHERE
     nip.data {$interval}
AND nip.canceled_by IS NULL
AND n.status <> "Cancelada"
AND n.tipo_documento_financeiro_id not in (14, 25)
ORDER BY
    nip.data
SQL;
            return parent::get($sql, 'Assoc');
        }
        return [];

    }

    public function getDebitoByNotaId($id, $tipo = "inclusao")
    {
        if("inclusao" == $tipo) {
            $sql = <<<SQL
            SELECT
        /* REGRAS IMPOSTAS POR JEAN PARA SELECAO DO CODIGO DOMINIO * A 
        SEREM UTILIZADAS NA COMPOSICAO DO ARQUIVO NA PARTIDA DEBITO */
        CASE
            /* QUANDO O CADASTRO DO IPCF FOR MARCADO PARA PROVISIONAR * E O IPCC 
            PREENCHIDO: * DEBITO â€“ SE TIPO 0 CODIGO DO CLIENTE, SENAO CONTA DO IPCC ASSOCIADO AO IPCF 
            
            NOVA:
            SE FOR ENTRADA:
            - CODIGO DA DOMINIO REFERENTE AO CLIENTE
            SE FOR SAÍDA:
            - CODIGO DA  DOMINIO REFERENTE AO FORNECEDOR
            */
    WHEN (
        (
            SELECT
                PROVISIONA
            FROM
                plano_contas AS pc1
            WHERE
                ID = r.IPCG4
        ) = 's'
    )
    AND (
        SELECT
            COD_IPCC
        FROM
            plano_contas AS pc2
        WHERE
            ID = r.IPCG4
    ) IS NOT NULL THEN

    IF (
        n.tipo = 0,
        (
            SELECT
                pcc.CODIGO_DOMINIO
            FROM
                plano_contas_contabil AS pcc
            WHERE
                pcc.ID = f.IPCC
        ),
        (
            SELECT
                pcc.CODIGO_DOMINIO
            FROM
                plano_contas_contabil AS pcc
            WHERE
                pcc.COD_N5 = pc.COD_IPCC
        )
    )
    WHEN (
        (
            SELECT
                PROVISIONA
            FROM
                plano_contas AS pc1
            WHERE
                ID = r.IPCG4
        ) = 'n'
    )
    AND (
        SELECT
            COD_IPCC
        FROM
            plano_contas AS pc2
        WHERE
            ID = r.IPCG4
    ) IS NOT NULL THEN

    IF (
        n.tipo = 0,
        (
            SELECT
                pcc.CODIGO_DOMINIO
            FROM
                plano_contas_contabil AS pcc
            WHERE
                pcc.COD_N5 = pc.COD_IPCC
        ),
        (
            SELECT
                pcc.CODIGO_DOMINIO
            FROM
                plano_contas_contabil AS pcc
            WHERE
                pcc.ID = f.IPCC
        )
    )	    
    END AS codigo_debito,
    e.centro_custo_dominio AS codigo_empresa,        
    asn.CODIGO_DOMINIO AS centro_custo_debito,
    (n.valor * r.PORCENTAGEM) / 100 AS valor,
    r.PORCENTAGEM
    FROM
        rateio AS r
    INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
    INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
    INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
    INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
    INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
    WHERE
        r.NOTA_ID = {$id}
  
    AND n.nota_adiantamento = 'N'
ORDER BY
	r.ID
SQL;

        } else {

            $sql = <<<SQL
SELECT
	/* REGRAS IMPOSTAS POR JEAN PARA SELECAO DO CODIGO DOMINIO
 * A SEREM UTILIZADAS NA COMPOSICAO DO ARQUIVO NA PARTIDA DEBITO
 */
CASE 
    /* QUANDO O CADASTRO DO IPCF ESTIVER MARCADO PARA PROVISIONAR
 * E O IPCC PREENCHIDO
 * DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADOA CONTA BANCARIA, 
        SENAO CONTA IPCC ASSOCIADO AO FORNECEDOR
 */
WHEN (
	SELECT
		PROVISIONA
	FROM
		plano_contas AS pc1
	WHERE
		ID = r.IPCG4
) = 's'
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	),
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = f.IPCC
	)
) /* QUANDO O CADASTRO DO IPCF ESTIVER DESMARCADO PARA PROVISIONAR
 * E O IPCC PREENCHIDO
 * DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A CONTA BANCARIA, SENAO CONTA DO IPCC ASSOCIADO AO IPCF
 */
WHEN (
	SELECT
		PROVISIONA
	FROM
		plano_contas AS pc1
	WHERE
		ID = r.IPCG4
) = 'n'
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	),
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	)
)
END AS codigo_debito,
 r.IPCG4,
 e.centro_custo_dominio AS codigo_empresa,
 asn.CODIGO_DOMINIO AS centro_custo_debito,
 (p.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.idNota = n.idNotas)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN contas_bancarias AS cb ON (p.origem = cb.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
  p.id = {$id}
ORDER BY
	r.ID
SQL;
        }

        return parent::get($sql, 'Assoc');
    }

    public function getCreditoByNotaId($id, $tipo = "inclusao")
    {
        if("inclusao" === $tipo) { # ARQUIVO DE INCLUSÃO
            $sql = <<<SQL
SELECT
/* REGRAS IMPOSTAS POR JEAN PARA SELECAO DO CODIGO DOMINIO * A SEREM UTILIZADAS NA COMPOSICAO DO ARQUIVO NA PARTIDA CREDITO */
CASE 
    /* QUANDO O CADASTRO DO IPCF FOR MARCADO PARA PROVISIONAR * E O IPCC PREENCHIDO: * 
       CREDITO â€“ SE TIPO 0 CODIGO IPCC ASSOCIADO AO CLIENTE, 
       SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA */
WHEN (
	(
		SELECT
			PROVISIONA
		FROM
			plano_contas AS pc1
		WHERE
			ID = r.IPCG4
	) = 's'
)
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	),
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = f.IPCC
	)
)
WHEN (
	(
		SELECT
			PROVISIONA
		FROM
			plano_contas AS pc1
		WHERE
			ID = r.IPCG4
	) = 'n'
)
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = f.IPCC
	),
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	)
)
END AS codigo_credito,
 e.centro_custo_dominio AS codigo_empresa,
 asn.CODIGO_DOMINIO AS centro_custo_credito,
 (n.VALOR * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
	r.NOTA_ID = {$id}
ORDER BY
	r.ID
SQL;

        } else { # ARQUIVO DE BAIXA

            $sql = <<<SQL
SELECT
 /* REGRAS IMPOSTAS POR JEAN PARA SELECAO DO CODIGO DOMINIO
 * A SEREM UTILIZADAS NA COMPOSICAO DO ARQUIVO NA PARTIDA CREDITO
 */
CASE 
    /* QUANDO O CADASTRO DO IPCF FOR MARCADO PARA PROVISIONAR
 * E O IPCC PREENCHIDO:
 * CREDITO – SE TIPO 0 CODIGO IPCC ASSOCIADO AO CLIENTE, SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA
 */
WHEN (
	(
		SELECT
			PROVISIONA
		FROM
			plano_contas AS pc1
		WHERE
			ID = r.IPCG4
	) = 's'
)
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = f.IPCC
	),
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	)
) /* QUANDO O CADASTRO DO IPCF ESTIVER DESMARCADO PARA PROVISIONAR
 * E O IPCC PREENCHIDO
 * CREDITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO AO IPCF, SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA
 */
WHEN (
	SELECT
		PROVISIONA
	FROM
		plano_contas AS pc1
	WHERE
		ID = r.IPCG4
) = 'n'
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	),
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	)
) 
END AS codigo_credito,
 e.centro_custo_dominio AS codigo_empresa,
 asn.CODIGO_DOMINIO AS centro_custo_credito,
 (p.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.idNota = n.idNotas)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN contas_bancarias AS cb ON (p.origem = cb.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
	p.id = {$id}
ORDER BY
	r.ID
SQL;
        }

        return parent::get($sql, 'Assoc');
    }

    public static function getDebitoEstornoByNotaId($id)
   {  $sql = <<<SQL
    SELECT
        /* REGRAS IMPOSTAS POR JEAN PARA SELECAO DO CODIGO DOMINIO
     * A SEREM UTILIZADAS NA COMPOSICAO DO ARQUIVO NA PARTIDA DEBITO
     */
    CASE 
        /* QUANDO O CADASTRO DO IPCF ESTIVER MARCADO PARA PROVISIONAR
     * E O IPCC PREENCHIDO
     * DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADOA CONTA BANCARIA, 
            SENAO CONTA IPCC ASSOCIADO AO FORNECEDOR
     */
    WHEN (
        SELECT
            PROVISIONA
        FROM
            plano_contas AS pc1
        WHERE
            ID = r.IPCG4
    ) = 's'
    AND (
        SELECT
            COD_IPCC
        FROM
            plano_contas AS pc2
        WHERE
            ID = r.IPCG4
    ) IS NOT NULL THEN
    
    IF (
        n.tipo = 0,
        (
            SELECT
                pcc.CODIGO_DOMINIO
            FROM
                plano_contas_contabil AS pcc
            WHERE
                pcc.ID = cb.IPCC
        ),
        (
            SELECT
                pcc.CODIGO_DOMINIO
            FROM
                plano_contas_contabil AS pcc
            WHERE
                pcc.ID = f.IPCC
        )
    ) /* QUANDO O CADASTRO DO IPCF ESTIVER DESMARCADO PARA PROVISIONAR
     * E O IPCC PREENCHIDO
     * DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A CONTA BANCARIA, SENAO CONTA DO IPCC ASSOCIADO AO IPCF
     */
    WHEN (
        SELECT
            PROVISIONA
        FROM
            plano_contas AS pc1
        WHERE
            ID = r.IPCG4
    ) = 'n'
    AND (
        SELECT
            COD_IPCC
        FROM
            plano_contas AS pc2
        WHERE
            ID = r.IPCG4
    ) IS NOT NULL THEN
    
    IF (
        n.tipo = 0,
        (
            SELECT
                pcc.CODIGO_DOMINIO
            FROM
                plano_contas_contabil AS pcc
            WHERE
                pcc.ID = cb.IPCC
        ),
        (
            SELECT
                pcc.CODIGO_DOMINIO
            FROM
                plano_contas_contabil AS pcc
            WHERE
                pcc.COD_N5 = pc.COD_IPCC
        )
    )
    END AS codigo_debito,
     r.IPCG4,
     e.centro_custo_dominio AS codigo_empresa,
     asn.CODIGO_DOMINIO AS centro_custo_debito,
     (ep.valor * r.PORCENTAGEM) / 100 AS valor,
     r.PORCENTAGEM
    FROM
     estorno_parcela as ep 
     INNER JOIN parcelas AS p ON (ep.parcela_id = p.id)   
       
    INNER JOIN notas AS n ON (n.idNotas = p.idNota)
    inner join rateio as r on (n.idNotas = r.NOTA_ID)    
    
    INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
    INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
    INNER JOIN contas_bancarias AS cb ON (ep.origem = cb.ID)
    INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
    INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
    WHERE
      ep.id = {$id}
    ORDER BY
        r.ID
SQL;
            
    
            return parent::get($sql, 'Assoc');
        }

        public static function getCreditoEstornoByNotaId($id)
    {
        $sql = <<<SQL
       SELECT
           /* REGRAS IMPOSTAS POR JEAN PARA SELECAO DO CODIGO DOMINIO
        * A SEREM UTILIZADAS NA COMPOSICAO DO ARQUIVO NA PARTIDA DEBITO
        */
        CASE 
    /* QUANDO O CADASTRO DO IPCF FOR MARCADO PARA PROVISIONAR
 * E O IPCC PREENCHIDO:
 * CREDITO – SE TIPO 0 CODIGO IPCC ASSOCIADO AO CLIENTE, SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA
 */
WHEN (
	(
		SELECT
			PROVISIONA
		FROM
			plano_contas AS pc1
		WHERE
			ID = r.IPCG4
	) = 's'
)
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = f.IPCC
	),
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	)
) /* QUANDO O CADASTRO DO IPCF ESTIVER DESMARCADO PARA PROVISIONAR
 * E O IPCC PREENCHIDO
 * CREDITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO AO IPCF, SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA
 */
WHEN (
	SELECT
		PROVISIONA
	FROM
		plano_contas AS pc1
	WHERE
		ID = r.IPCG4
) = 'n'
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	),
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	)
) 
END AS codigo_credito,
 e.centro_custo_dominio AS codigo_empresa,
 asn.CODIGO_DOMINIO AS centro_custo_credito,
        (ep.valor * r.PORCENTAGEM) / 100 AS valor,
        r.PORCENTAGEM
       FROM
        estorno_parcela as ep 
        INNER JOIN parcelas AS p ON (ep.parcela_id = p.id)   
          
       INNER JOIN notas AS n ON (n.idNotas = p.idNota)
       inner join rateio as r on (n.idNotas = r.NOTA_ID)    
       
       INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
       INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
       INNER JOIN contas_bancarias AS cb ON (ep.origem = cb.ID)
       INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
       INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
       WHERE
         ep.id = {$id}
       ORDER BY
           r.ID
SQL;
               
       
               return parent::get($sql, 'Assoc');
           }

    public function getDebitoAntecipacaoByNotaId($id, $tipo = "inclusao")
    {
        $sql = <<<SQL
SELECT
/* DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A CONTA BANCARIA, 
 * SENAO CONTA DO IPCC ASSOCIADO AO IPCF
 */
IF (
	n.tipo = 0,
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	),
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	)
) AS codigo_debito,
 e.centro_custo_dominio AS codigo_empresa,
 asn.CODIGO_DOMINIO AS centro_custo_debito,
 (p.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.idNota = n.idNotas)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN contas_bancarias AS cb ON (p.origem = cb.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
  p.id = {$id}
  AND n.nota_aglutinacao = 'n'
  AND n.nota_adiantamento = 'S'
ORDER BY
	r.ID
SQL;
        return parent::get($sql, 'Assoc');
    }

    public function getCreditoAntecipacaoByNotaId($id, $tipo = "inclusao")
    {
        $sql = <<<SQL
SELECT
/* DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A CONTA BANCARIA, 
 * SENAO CONTA DO IPCC ASSOCIADO AO IPCF
 */
IF (
	n.tipo = 0,
    (
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	),
    (
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	)	
) AS codigo_credito,
 e.centro_custo_dominio AS codigo_empresa,
 asn.CODIGO_DOMINIO AS centro_custo_credito,
 (p.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.idNota = n.idNotas)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN contas_bancarias AS cb ON (p.origem = cb.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
  p.id = {$id}
  AND n.nota_aglutinacao = 'n'
  AND n.nota_adiantamento = 'S'
ORDER BY
	r.ID
SQL;
        return parent::get($sql, 'Assoc');
    }

    public function getDebitoCompensacaoByNotaId($id, $tipo = "inclusao", $valor)
    {
        $contaContabil = $tipo == 0 ? 453 : 154;
        $sql = <<<SQL
SELECT
/* DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO AO ADIANTAMENTO DE CLIENTES, 
 * SENAO CONTA DO FORNECEDOR A PAGAR
 */
IF (
	n.tipo = 0,
	$contaContabil ,
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = f.IPCC
	)
) AS codigo_debito,
 e.centro_custo_dominio AS codigo_empresa,
 asn.CODIGO_DOMINIO AS centro_custo_debito,
 ({$valor} * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.idNota = n.idNotas)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
WHERE
p.idNota = {$id}
  AND n.nota_adiantamento = 'N'
ORDER BY
	r.ID
SQL;

        return parent::get($sql, 'Assoc');
    }

    public function getCreditoCompensacaoByNotaId($id, $tipo, $valor)
    {
        $contaContabil = $tipo == 0 ? 453 : 154;
        $sql = <<<SQL
SELECT
/* DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A DESPESA, 
 * SENAO CONTA DO IPCC ASSOCIADO AO FORNECEDOR
 */
IF (
	n.tipo = 0,
    (
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = f.IPCC
	),
   $contaContabil
) AS codigo_credito,
 e.centro_custo_dominio AS codigo_empresa,
 asn.CODIGO_DOMINIO AS centro_custo_credito,
 ({$valor} * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.idNota = n.idNotas)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
WHERE
p.idNota= {$id}

  AND n.nota_adiantamento = 'N'
ORDER BY
	r.ID

SQL;

        return parent::get($sql, 'Assoc');
    }

    public function getDebitoBaixaAntecipacaoByNotaId($id, $tipo = "inclusao")
    {
        $sql = <<<SQL
SELECT
/* DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A DESPESA, 
 * SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA
 */
IF (
	n.tipo = 0,
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	),
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	)
) AS codigo_debito,
 e.centro_custo_dominio AS codigo_empresa,
 asn.CODIGO_DOMINIO AS centro_custo_debito,
 (nb.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	nota_baixas AS nb
INNER JOIN notas AS n ON (n.idNotas = nb.nota_id)
INNER JOIN rateio AS r ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.id = nb.parcela_antecipada_id)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN contas_bancarias AS cb ON (nb.conta_bancaria_id = cb.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
  nb.id = {$id}
  AND n.nota_aglutinacao = 'n'
  AND n.nota_adiantamento = 'S'
ORDER BY
	r.ID
SQL;

        return parent::get($sql, 'Assoc');
    }

    public function getCreditoBaixaAntecipacaoByNotaId($id, $tipo = "inclusao")
    {
        $sql = <<<SQL
SELECT
/* DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A DESPESA, 
 * SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA
 */
IF (
	n.tipo = 0,
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	),
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	)
) AS codigo_credito,
 e.centro_custo_dominio AS codigo_empresa,
 asn.CODIGO_DOMINIO AS centro_custo_credito,
 (nb.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	nota_baixas AS nb
INNER JOIN notas AS n ON (n.idNotas = nb.nota_id)
INNER JOIN rateio AS r ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.id = nb.parcela_antecipada_id)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN contas_bancarias AS cb ON (nb.conta_bancaria_id = cb.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
  nb.id = {$id}
  AND n.nota_aglutinacao = 'n'
  AND n.nota_adiantamento = 'S'
ORDER BY
	r.ID
SQL;
        return parent::get($sql, 'Assoc');
    }

    public function getDebitoAglutinacaoByNotaId($id, $idNotaAglutinacao, $tipo)
    {
        // $tipo 0 recebimento e 1 saida

       
        
        if($tipo == 1){
            $recebimento =  <<<SQL
            (
            SELECT
pcc.CODIGO_DOMINIO
FROM	
 notas AS n 
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas_contabil AS pcc ON (f.IPCC= pcc.ID)
WHERE
n.idNotas = {$idNotaAglutinacao}
)
SQL;
$desembolso = <<<SQL
(
    SELECT
        pcc.CODIGO_DOMINIO
    FROM
        plano_contas_contabil AS pcc
    WHERE
        pcc.ID = f.IPCC
)
SQL;

    }else{
            $desembolso =  <<<SQL
            (
            SELECT
pcc.CODIGO_DOMINIO
FROM	
 notas AS n 
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas_contabil AS pcc ON (f.IPCC= pcc.ID)
WHERE
n.idNotas = {$idNotaAglutinacao}
)
SQL;
$recebimento = <<<SQL
(
    SELECT
        pcc.CODIGO_DOMINIO
    FROM
        plano_contas_contabil AS pcc
    WHERE
        pcc.ID = f.IPCC
)
SQL;

}

        
       

        $sql = <<<SQL
SELECT
/* DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A DESPESA, 
 * SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA
 */
IF (
	n.tipo = 0,
	{$recebimento},
	{$desembolso}
) AS codigo_debito,
 e.centro_custo_dominio AS codigo_empresa,
 asn.CODIGO_DOMINIO AS centro_custo_debito,
 (p.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.idNota = n.idNotas)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)

INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
  p.id = {$id}
ORDER BY
	r.ID
SQL;


        return parent::get($sql, 'Assoc');
    }

    public function getCreditoAglutinacaoByNotaId($id, $idNotaAglutinacao, $tipo)
    {
        // $tipo 0 recebimento e 1 saida
        
        if($tipo == 0){
            $recebimento =  <<<SQL
            (
            SELECT
pcc.CODIGO_DOMINIO
FROM	
 notas AS n 
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas_contabil AS pcc ON (f.IPCC= pcc.ID)
WHERE
n.idNotas = {$idNotaAglutinacao}
)
SQL;
$desembolso = <<<SQL
(
    SELECT
        pcc.CODIGO_DOMINIO
    FROM
        plano_contas_contabil AS pcc
    WHERE
        pcc.COD_N5 = f.IPCC
)
SQL;

    }else{
            $desembolso =  <<<SQL
            (
            SELECT
pcc.CODIGO_DOMINIO
FROM	
 notas AS n 
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas_contabil AS pcc ON (f.IPCC= pcc.ID)
WHERE
n.idNotas = {$idNotaAglutinacao}
)
SQL;
$recebimento = <<<SQL
(
    SELECT
        pcc.CODIGO_DOMINIO
    FROM
        plano_contas_contabil AS pcc
    WHERE
        pcc.COD_N5 = f.IPCC
)
SQL;

}


        $sql = <<<SQL
SELECT
/* DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A DESPESA, 
 * SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA
 */
IF (
	n.tipo = 0,
	{$recebimento},
	{$desembolso}
) AS codigo_credito,
 e.centro_custo_dominio AS codigo_empresa,
 asn.CODIGO_DOMINIO AS centro_custo_credito,
 (p.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.idNota = n.idNotas)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)

INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
  p.id = {$id}
ORDER BY
	r.ID
SQL;
        return parent::get($sql, 'Assoc');
    }

    public function getDebitoInformePerdaByNotaId($id, $tipo = "baixa")
    {
        if($tipo == 'baixa'){
        $sql = <<<SQL
SELECT
CASE 
    /* QUANDO O CADASTRO DO IPCF ESTIVER MARCADO PARA PROVISIONAR
 * E O IPCC PREENCHIDO
 * DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADOA CONTA BANCARIA, 
        SENAO CONTA IPCC ASSOCIADO AO FORNECEDOR
 */
WHEN (
	SELECT
		PROVISIONA
	FROM
		plano_contas AS pc1
	WHERE
		ID = r.IPCG4
) = 's'
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	(
		SELECT
            pcc.CODIGO_DOMINIO
        FROM
            plano_contas_contabil AS pcc
        inner Join plano_contas as pc3 on (pcc.COD_N5 = pc3.COD_IPCC)
        WHERE
        pc3.ID = nip.plano_conta_id
	),
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = f.IPCC
	)
) /* QUANDO O CADASTRO DO IPCF ESTIVER DESMARCADO PARA PROVISIONAR
 * E O IPCC PREENCHIDO
 * DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A CONTA BANCARIA, SENAO CONTA DO IPCC ASSOCIADO AO IPCF
 */
WHEN (
	SELECT
		PROVISIONA
	FROM
		plano_contas AS pc1
	WHERE
		ID = r.IPCG4
) = 'n'
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	(
		SELECT
            pcc.CODIGO_DOMINIO
        FROM
            plano_contas_contabil AS pcc
        inner Join plano_contas as pc3 on (pcc.COD_N5 = pc3.COD_IPCC)
        WHERE
        pc3.ID = nip.plano_conta_id
	),
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	)
)
END AS codigo_debito,
 e.centro_custo_dominio AS codigo_empresa,
 asn.CODIGO_DOMINIO AS centro_custo_debito,
 (nip.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	nota_informe_perda AS nip
INNER JOIN notas AS n ON (n.idNotas = nip.nota_id)
INNER JOIN rateio AS r ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.id = nip.parcela_id)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
  nip.id = {$id}
ORDER BY
	r.ID
SQL;
    

        return parent::get($sql, 'Assoc');
    }
    }

    public function getCreditoInformePerdaByNotaId($id, $tipo = "baixa")
    {
        if($tipo == 'baixa'){
        $sql = <<<SQL
SELECT
CASE 
    /* QUANDO O CADASTRO DO IPCF FOR MARCADO PARA PROVISIONAR
 * E O IPCC PREENCHIDO:
 * CREDITO – SE TIPO 0 CODIGO IPCC ASSOCIADO AO CLIENTE, SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA
 */
WHEN (
	(
		SELECT
			PROVISIONA
		FROM
			plano_contas AS pc1
		WHERE
			ID = r.IPCG4
	) = 's'
)
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = f.IPCC
	),
	(
		SELECT
            pcc.CODIGO_DOMINIO
        FROM
            plano_contas_contabil AS pcc
        inner Join plano_contas as pc3 on (pcc.COD_N5 = pc3.COD_IPCC)
        WHERE
        pc3.ID = nip.plano_conta_id
	)
) /* QUANDO O CADASTRO DO IPCF ESTIVER DESMARCADO PARA PROVISIONAR
 * E O IPCC PREENCHIDO
 * CREDITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO AO IPCF, SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA
 */
WHEN (
	SELECT
		PROVISIONA
	FROM
		plano_contas AS pc1
	WHERE
		ID = r.IPCG4
) = 'n'
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	(
		SELECT
			pcc.CODIGO_DOMINIO
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	),
	(
		SELECT
            pcc.CODIGO_DOMINIO
        FROM
            plano_contas_contabil AS pcc
        inner Join plano_contas as pc3 on (pcc.COD_N5 = pc3.COD_IPCC)
        WHERE
        pc3.ID = nip.plano_conta_id
	)
) 
END AS codigo_credito,
 e.centro_custo_dominio AS codigo_empresa,
 asn.CODIGO_DOMINIO AS centro_custo_debito,
 (nip.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	nota_informe_perda AS nip
INNER JOIN notas AS n ON (n.idNotas = nip.nota_id)
INNER JOIN rateio AS r ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.id = nip.parcela_id)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
  nip.id = {$id}
ORDER BY
	r.ID
SQL;

        return parent::get($sql, 'Assoc');
    }
    }

    public function getCentroResultadoDebitoByDestino($destino)
    {
        return Transferencias::getCentroCustoByDestino($destino);
    }

    public function getCentroResultadoCreditoByOrigem($origem)
    {
        return Transferencias::getCentroCustoByOrigem($origem);
    }

    public function getCentroResultadoDebitoByTarifaImpostoContaId($lancamento)
    {
        return TarifaImposto::getCentroCustoDebitoByContaId($lancamento);
    }

    public function getCentroResultadoCreditoByTarifaImpostoContaId($lancamento)
    {
        return TarifaImposto::getCentroCustoCreditoByContaId($lancamento);
    }

    public function getDebitoEstornoBanco($lancamento)
    {
        $id = str_replace('i', '', $lancamento['idNotas']);
        $sql = <<<SQL
SELECT pcc.CODIGO_DOMINIO AS codigo_debito,
       '102' AS centro_custo_debito,
       e.centro_custo_dominio AS codigo_empresa
FROM estorno_parcela ep
INNER JOIN contas_bancarias cb ON ep.origem = cb.ID
INNER JOIN plano_contas_contabil pcc ON pcc.ID = cb.IPCC
INNER JOIN empresas e ON e.id = cb.UR
WHERE ep.credito_id = '{$id}';
SQL;
        return parent::get($sql);
	}

    public function getCreditoEstornoBanco($lancamento)
    {
        $id = str_replace('i', '', $lancamento['idNotas']);
        $sql = <<<SQL
SELECT pcc.CODIGO_DOMINIO AS codigo_credito,
       '102' AS centro_custo_credito,
       e.centro_custo_dominio AS codigo_empresa
FROM estorno_parcela ep
INNER JOIN parcelas p ON ep.parcela_id = p.id
INNER JOIN notas n ON n.idNotas = p.idNota
INNER JOIN fornecedores f ON f.idFornecedores = n.codFornecedor
INNER JOIN plano_contas_contabil pcc ON pcc.ID = f.IPCC
INNER JOIN empresas e ON e.id = n.empresa
WHERE ep.credito_id = '{$id}';
SQL;
        return parent::get($sql);
    }
}