<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 14/12/2017
 * Time: 16:32
 */

namespace App\Models\Financeiro;
require_once($_SERVER['DOCUMENT_ROOT'].'/utils/codigos.php');

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;


class ConciliacaoBancaria extends AbstractModel implements DAOInterface
{
    public static function getSql($id = null)
    {
        return;
    }
                                                                            
    public static function getTransacoes($contaId,$status,$inicio,$fim,$exibirParcelaBaixadaAglutinacao = 'N' )
    {
        $selectTarifas = '';
        $unionEstorno = '';
        if($status != 'N'){
            $selectTarifas = "UNION
                SELECT
                    itl.id,
                    'S' AS CONCILIADO,
                    itl.valor as VALOR_PAGO,
                    0 AS idNota,
                    0 AS TR,
                    itl.documento as NUM_DOCUMENTO,
                    itl.data as dt,
					DATE_FORMAT(itl.data, '%d/%m/%Y') AS data_pg,
					itl.data AS data_pg_db,
                    IF(debito_credito = 'DEBITO', 1, 0) AS tipo_nota,
                    imptar.item as fornecedor,
                    0 AS fornecedor_id,
                    'N' as transferencia,
                    DATE_FORMAT(itl.data, '%d/%m/%Y')  AS data_conciliado,
                    '' as encontrou,
                    'impostos_tarifas' as tabela_origem 
		FROM
			impostos_tarifas imptar
                        INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
		WHERE
                    itl.conta = {$contaId}
                    AND 
                    itl.data BETWEEN '{$inicio}' AND '{$fim}'";

            $unionEstorno = <<<SQL
UNION
  SELECT
    hep.id,
    'S' AS CONCILIADO,
    hep.valor as VALOR_PAGO,
    hep.credito_id AS idNota,
    p.TR AS TR,
    CONCAT('ESTORNO BANCÁRIO PARA ', IF(f.razaoSocial != '', f.razaoSocial, f.nome)) AS NUM_DOCUMENTO,
    hep.data_conciliada AS dt,
    DATE_FORMAT(hep.data_conciliada, '%d/%m/%Y') AS data_pg,
	hep.data_conciliada AS data_pg_db,
    '1' AS tipo_nota,
    f.razaoSocial as fornecedor,
    n.codFornecedor AS fornecedor_id,
    'N' as transferencia,
    DATE_FORMAT(hep.data_conciliada, '%d/%m/%Y') AS data_conciliado,
    '' as encontrou,
    'estorno_parcelas' as tabela_origem
  FROM
    estorno_parcela hep
    INNER JOIN parcelas p ON (p.id = hep.parcela_id)
    INNER JOIN notas n ON (n.idNotas = p.idNota)
    LEFT JOIN fornecedores f ON (n.codFornecedor = f.idFornecedores)
  WHERE
    hep.origem = '{$contaId}'
    AND hep.data_conciliada BETWEEN '{$inicio}' AND '{$fim}'
SQL;
        }
        if($status=='S')
        {
            $condicao_conciliacao_parcela="p.DATA_PAGAMENTO BETWEEN '{$inicio}' AND '{$fim}' AND CONCILIADO='{$status}'";
			$condicao_conciliacao_transf = "(`DATA_TRANSFERENCIA`  BETWEEN '{$inicio}' and '{$fim}' AND IF(`CONTA_DESTINO`='{$contaId}',CONCILIADO_DESTINO='".$status."',CONCILIADO_ORIGEM='".$status."'))";
			$condBaixaNotaAtencipada = "nb.data_pagamento BETWEEN '{$inicio}' AND '{$fim}' AND nb.conciliado = '{$status}'";
        }
        elseif($status=='N')
        {   $condicao_conciliacao_parcela="( p.DATA_PAGAMENTO BETWEEN '{$inicio}' AND '{$fim}' AND conciliado <> 'S')";
			$condicao_conciliacao_transf = "(`DATA_TRANSFERENCIA`  BETWEEN '{$inicio}' and '{$fim}' AND (IF(`CONTA_DESTINO`={$contaId},CONCILIADO_DESTINO <>'S' ,CONCILIADO_ORIGEM <> 'S')) )";
			$condBaixaNotaAtencipada = "nb.data_pagamento BETWEEN '{$inicio}' AND '{$fim}' AND nb.conciliado <> 'S'";
        }
        else
        {
            $condicao_conciliacao_parcela="( p.DATA_PAGAMENTO BETWEEN '{$inicio}' AND '{$fim}'  )";
			$condicao_conciliacao_transf = "(`DATA_TRANSFERENCIA`  BETWEEN '{$inicio}' and '{$fim}' )";
			$condBaixaNotaAtencipada = "nb.data_pagamento BETWEEN '{$inicio}' AND '{$fim}'";
		}
		
		$parcelaBaixadaAglutinacao =  "AND (p.nota_id_fatura_aglutinacao is null or p.nota_id_fatura_aglutinacao = 0)";
        //////////sql parcelas e transferências
        $sql =<<<SQL


 Select
         p.id,
         p.CONCILIADO,
         p.VALOR_PAGO,
         p.idNota,
	     p.TR,
         p.NUM_DOCUMENTO,
	     p.DATA_PAGAMENTO as dt,
         DATE_FORMAT(p.DATA_PAGAMENTO,'%d/%m/%Y') as data_pg,
		 p.DATA_PAGAMENTO AS data_pg_db,
	     n.tipo as tipo_nota,
	     f.razaoSocial as fornecedor,
         n.codFornecedor as fornecedor_id,
         'N' as transferencia,
       DATE_FORMAT(p.DATA_CONCILIADO,'%d/%m/%Y') as data_conciliado,
       '' as encontrou,
       'parcelas' as tabela_origem 

from
		parcelas as p inner join
		contas_bancarias as o on (p.origem = o.id) inner join
		notas as n on (p.idNota = n.idNotas) inner join
		fornecedores as f on (n.codFornecedor=f.idFornecedores)
	where
        p.status ='Processada' AND
		
        p.origem = {$contaId} AND {$condicao_conciliacao_parcela}
		{$parcelaBaixadaAglutinacao}

UNION
SELECT
		ID,
		(case when `CONTA_ORIGEM`={$contaId}  then CONCILIADO_ORIGEM when `CONTA_DESTINO`={$contaId}  then  CONCILIADO_DESTINO end) as CONCILIADO,
		( case when `CONTA_ORIGEM`={$contaId}  then `VALOR` when `CONTA_DESTINO`={$contaId}  then  VALOR end) as VALOR_PAGO,
		0 as idNota,
		'0' as TR,
		'Transferência' as NUM_DOCUMENTO,
		`DATA_TRANSFERENCIA` as dt,
		DATE_FORMAT(`DATA_TRANSFERENCIA`,'%d/%m/%Y') as data_pg,
		DATA_TRANSFERENCIA AS data_pg_db,
		( case when `CONTA_ORIGEM`={$contaId}  then '1' when `CONTA_DESTINO`={$contaId}  then '0' end) as tipo_nota,
		( case when `CONTA_ORIGEM`={$contaId}  THEN (
												SELECT
														concat(o.SIGLA_BANCO,'-',c.AGENCIA,'-',c.NUM_CONTA,'-',e.SIGLA_EMPRESA)
												FROM
														contas_bancarias as c INNER JOIN
														origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id) INNER JOIN
														empresas as e ON (c.UR = e.id)
												WHERE
														c.ID= CONTA_DESTINO
											)

			  WHEN `CONTA_DESTINO`={$contaId}  THEN (
											 SELECT
													concat(o.SIGLA_BANCO,'-',c.AGENCIA,'-',c.NUM_CONTA,'-',e.SIGLA_EMPRESA)
											 FROM
													contas_bancarias as c INNER JOIN
													origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id) INNER JOIN
													empresas as e ON (c.UR = e.id)
											 WHERE
													c.ID = CONTA_ORIGEM
											)
											 END
		) as fornecedor,
		( CASE
				WHEN `CONTA_ORIGEM`={$contaId} THEN
					CONTA_DESTINO
				WHEN `CONTA_DESTINO`={$contaId}  THEN
					CONTA_ORIGEM
		END) as fornecedor_id,
        'S' as trasferencia,
        (case
        when `CONTA_ORIGEM`={$contaId}  then DATE_FORMAT(DATA_CONCILIADO_ORIGEM,'%d/%m/%Y')
        when `CONTA_DESTINO`={$contaId}  then  DATE_FORMAT(DATA_CONCILIADO_DESTINO,'%d/%m/%Y')
        end) as data_conciliado,
       '' as encontrou,
       'transferencia_bancaria' as tabela_origem 
FROM
		`transferencia_bancaria`
WHERE
		(`CONTA_DESTINO`={$contaId} or `CONTA_ORIGEM`={$contaId}) and
		{$condicao_conciliacao_transf}  and CANCELADA_POR is NULL
UNION
		SELECT
			nb.id,
			 nb.conciliado as CONCILIADO,
			nb.valor as VALOR_PAGO,
			0 AS idNota,
			0 AS TR,
			nb.numero_documento as NUM_DOCUMENTO,
			nb.data_pagamento as dt,
			DATE_FORMAT(nb.data_pagamento, '%d/%m/%Y') AS data_pg,
			nb.data_pagamento AS data_pg_db,
			nb.tipo_movimentacao AS tipo_nota,
			"Devolução Adiantamento" as fornecedor,
			0 AS fornecedor_id,
			'N' as transferencia,
			DATE_FORMAT(nb.data_pagamento, '%d/%m/%Y')  AS data_conciliado,
			'' as encontrou,
			'nota_baixas' as tabela_origem 
FROM
	nota_baixas as nb
				
WHERE
			nb.conta_bancaria_id = {$contaId}
			AND
			nb.canceled_by is null
			AND
			$condBaixaNotaAtencipada 
		
{$selectTarifas}
		
 {$unionEstorno}
order by dt
SQL;


return parent::get($sql);
    }

	public static function desfazerConciliacao($id,$idBanco,$entrada,$saida,$transferencia,$tabelaOrigem)
	{
		mysql_query('begin');

		$tipoNota= $saida == 0.00 ? 0:1;
		$valor = $saida == 0.00 ? $entrada : $saida;
		if($transferencia == 'S' ){
			$transferenciaId = $id;
			$transferenciaParcela=0;
			if($saida== '0.00'){
				$sql= "Update
						transferencia_bancaria
						set
						 CONCILIADO_DESTINO='N',
						 DATA_CONCILIADO_DESTINO='0000-00-00 00:00:00'
						 where ID={$id}";

			}else{
				$sql= "Update
transferencia_bancaria
set
CONCILIADO_ORIGEM='N',
DATA_CONCILIADO_ORIGEM='0000-00-00 00:00:00'
where
ID={$id}";

			}

		}else{
			$transferenciaId = 0;
			$transferenciaParcela=$id;
			if($tabelaOrigem == 'nota_baixas'){
				$sql ="Update nota_baixas set conciliado='N', data_conciliado ='0000-00-00 00:00:00' where id= {$id}";

			}else{
				$sql ="Update parcelas set CONCILIADO='N', DATA_CONCILIADO ='0000-00-00 00:00:00' where id= {$id}";
			}

		}

		$result = mysql_query($sql);
		if(!$result){
			echo mysql_error().'1';
			mysql_query("roolback");
			return;
		}
		savelog(mysql_escape_string(addslashes($sql)));

		$sql ="Update contas_bancarias
set
 SALDO=(SALDO - {$entrada} + {$saida}),
  SALDO_NCONCILIADO=(SALDO_NCONCILIADO + {$entrada} - {$saida})
  where ID= $idBanco";
		$result = mysql_query($sql);
		if(!$result){
			echo mysql_error().$sql;
			mysql_query("roolback");
			return;
		}
		savelog(mysql_escape_string(addslashes($sql)));

		$sql="INSERT INTO
            historico_desfazer_conciliacao_bancaria
               (`USUARIO_ID`,
                `DATA` ,
                `TRANSFERENCIA_ID`,
                `PARCELA_ID` ,
                `VALOR`,
                `TIPO_NOTA`)
            VALUES
            ({$_SESSION['id_user']},
              now(),
              $transferenciaId,
              $transferenciaParcela,
              $valor,
              $tipoNota
                )
            ";
		$result = mysql_query($sql);
		if(!$result){
			echo mysql_error();
			mysql_query("roolback");
			return;
		}
		savelog(mysql_escape_string(addslashes($sql)));

		mysql_query('commit');
		echo 1;
	}

	public static function salvarConciliacao($bancoId,$inicio,$fim,$linha)
	{
		mysql_query("BEGIN");
		//Insere o cabe�alho da concilia��o

		$iduser=$_SESSION['id_user'];
		$sql ="INSERT
              INTO
              conciliacao_bancaria
              (CONTAS_BANCARIAS_ID,
              DATA,
              SALDO_CONCILIADO,
              SALDO_NCONCILIADO,
              USUARIO_ID,
              DATA_INICIO,
              DATA_FIM)
               VALUES (
               '{$bancoId}',
               now(),
               '{$saldo_conciliado}',
               '{$saldo_n_conciliado}',
               '{$iduser}',
               '{$inicio}',
               '{$fim}')";

		if(!mysql_query($sql)){
			echo"Erro: No insert em conciliacao_bancaria ";
			mysql_query("ROLLBACK");
		}

		///pega o id da conciliacao bancaria
		$id_conciliacao = mysql_insert_id();
		savelog(mysql_escape_string(addslashes($sql)));

		//Separa os itens da concilia��o
		//Salva os itens da concilia��o na tabela correspondente

		$saldo_conciliado=0;
		$itens = explode("*$*",$linha);

		foreach($itens as $item){

			$con++;
			$i = explode("$#$",str_replace("undefined","",$item));

			$tr = anti_injection($i[0],'literal');
			$tipo_nota = anti_injection($i[1],'literal');
			$fornecedor = anti_injection($i[2],'literal');
			$data_pagamento = anti_injection($i[3],'literal');
			$data_pagamento = implode("-",array_reverse(explode("/", $data_pagamento)));
			$conciliado = anti_injection($i[4],'literal');
			$valor = anti_injection($i[5],'numerico');
			//  $valor=parseInt($valor);
			$num_documento = anti_injection($i[6],'literal');
			$id_parcela = $i[7];
			$transferencia = anti_injection($i[8],'literal');
			$data_conciliado= implode("-",array_reverse(explode("/",$i[9])));
			$alterado =anti_injection($i[10],'literal');
			$tabelaOrigem =anti_injection($i[11],'literal');
			//$tipo_nota = 0 entrada, $tipo_nota = 1 saída*/
			if($alterado=='S'){
				$saldo_conciliado = $tipo_nota == 1 ? $saldo_conciliado-$valor : $saldo_conciliado+$valor;

			}
			if($conciliado == 'N'){
				$data_conciliado = '0000-00-00';
			}else{
				$c .= $data_conciliado;
			}
			if($transferencia == 'N'){
				

				if($tabelaOrigem == 'parcelas'){

					$sql = "INSERT INTO `itens_conciliacao_bancaria`
                        (`CONCILIACAO_BANCARIA_ID`, `DATA_PAGAMENTO`, `TR_PARCELA`, `FORNECEDOR_ID`, `NUM_DOCUMENTO`, `TIPO_NOTA`, `CONCILIADO`, `VALOR`,`TRANSFERENCIA_BANCARIA`,DATA_CONCILIADO)
                        VALUES
                        ('{$id_conciliacao}','{$data_pagamento}','{$tr}','{$fornecedor}','{$num_documento}','{$tipo_nota}','{$conciliado}','{$valor}','{$transferencia}','{$data_conciliado}')";

						$r = mysql_query($sql);
						if (!$r) {
							echo mysql_error();
							mysql_query("ROLLBACK");
							return;
						}
						savelog(mysql_escape_string(addslashes($sql)));

						$sql = "Update parcelas set CONCILIADO = '{$conciliado}' , DATA_CONCILIADO='{$data_conciliado}' where id = {$id_parcela} ";
						$r = mysql_query($sql);
						if (!$r) {
							echo mysql_error();
							mysql_query("ROLLBACK");
							return;
						}
						savelog(mysql_escape_string(addslashes($sql)));
					}
					if($tabelaOrigem == 'nota_baixas'){
						$sql = "INSERT INTO `itens_conciliacao_bancaria`
                        (`CONCILIACAO_BANCARIA_ID`, `DATA_PAGAMENTO`, `NOTA_BAIXAS_ID`, `FORNECEDOR_ID`, `NUM_DOCUMENTO`, `TIPO_NOTA`, `CONCILIADO`, `VALOR`,`TRANSFERENCIA_BANCARIA`,DATA_CONCILIADO)
                        VALUES
                        ('{$id_conciliacao}','{$data_pagamento}','{$id_parcela}','{$fornecedor}','{$num_documento}','{$tipo_nota}','{$conciliado}','{$valor}','{$transferencia}','{$data_conciliado}')";

						$r = mysql_query($sql);
						if (!$r) {
							echo mysql_error();
							mysql_query("ROLLBACK");
							return;
						}
						savelog(mysql_escape_string(addslashes($sql)));
						
						
						$sql = "Update 
						nota_baixas 
						set 
						conciliado = '{$conciliado}' , 
						data_conciliado='{$data_conciliado}',
						conciliado_by = '{$_SESSION['id_user']}',
						conciliado_at = now()						
						where id = {$id_parcela} ";
						$r = mysql_query($sql);
						if (!$r) {
							echo mysql_error();
							mysql_query("ROLLBACK");
							return;
						}
						savelog(mysql_escape_string(addslashes($sql)));
					}
			}else{
				$sql = "INSERT INTO `itens_conciliacao_bancaria`
                        (`CONCILIACAO_BANCARIA_ID`, `DATA_PAGAMENTO`, `TRANSFERENCIA_BANCARIA_ID`, `FORNECEDOR_ID`, `NUM_DOCUMENTO`, `TIPO_NOTA`, `CONCILIADO`, `VALOR`,`TRANSFERENCIA_BANCARIA`,DATA_CONCILIADO)
                        VALUES
                        ('{$id_conciliacao}','{$data_pagamento}','{$id_parcela}','{$fornecedor}','{$num_documento}','{$tipo_nota}','{$conciliado}','{$valor}','{$transferencia}','{$data_conciliado}')";

				$r = mysql_query($sql);
				if (!$r) {
					echo mysql_error();
					mysql_query("ROLLBACK");
					return;
				}
				savelog(mysql_escape_string(addslashes($sql)));
				if($tipo_nota==0){
					$sql = "Update transferencia_bancaria set CONCILIADO_DESTINO = '{$conciliado}', DATA_CONCILIADO_DESTINO='{$data_conciliado}' where id = {$id_parcela} ";
					$r = mysql_query($sql);
					if (!$r) {
						echo mysql_error();
						mysql_query("ROLLBACK");
						return;
					}
					savelog(mysql_escape_string(addslashes($sql)));
				}else{
					$sql = "Update transferencia_bancaria set CONCILIADO_ORIGEM = '{$conciliado}', DATA_CONCILIADO_ORIGEM='{$data_conciliado}' where id = {$id_parcela} ";
					$r = mysql_query($sql);
					if (!$r) {
						echo mysql_error();
						mysql_query("ROLLBACK");
						return;
					}
					savelog(mysql_escape_string(addslashes($sql)));

				}

			}

		}


		//Atualiza o saldo

		$sql_saldo_banco = "UPDATE `contas_bancarias`
  	SET
  	`SALDO`=`SALDO` + {$saldo_conciliado},`DATA_ATUALIZACAO_SALDO`=NOW(),`SALDO_NCONCILIADO`=`SALDO_NCONCILIADO`- {$saldo_conciliado}
  	WHERE ID = '{$bancoId}'";

		$result_saldo_banco = mysql_query($sql_saldo_banco);
		if(!$result_saldo_banco){
			echo mysql_error();
			mysql_query("ROLLBACK");
			return;
		}
		savelog(mysql_escape_string(addslashes($sql)));
		$sql="Update
                conciliacao_bancaria,
                contas_bancarias
            set
                conciliacao_bancaria.SALDO_CONCILIADO=contas_bancarias.SALDO,
                conciliacao_bancaria.SALDO_NCONCILIADO=contas_bancarias.SALDO_NCONCILIADO
            where
                conciliacao_bancaria.ID ='{$id_conciliacao}' and
                contas_bancarias.ID='{$bancoId}'
                ";
		$result = mysql_query($sql);
		if(!$result){
			echo mysql_error();
			mysql_query("ROLLBACK");
			return;
		}

		mysql_query("COMMIT");
		echo "1";
	}



}