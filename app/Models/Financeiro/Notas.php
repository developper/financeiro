<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class Notas extends AbstractModel
{
    public static function getNotaById($id)
    {
        $sql = <<<SQL
SELECT * FROM notas WHERE idNotas = '{$id}'
SQL;
        return current(parent::get($sql, 'Assoc'));

    }

    public static function getParcelasProcessadasByNotaId($id)
    {
        $sql = <<<SQL
SELECT * FROM parcelas WHERE idNota = '{$id}' AND status = 'Processada'
SQL;
        return parent::get($sql, 'Assoc');

    }

    public static function verificarNFJaExiste($idFornecedor, $codDocumento)
    { 
        $sql =  "SELECT 
            *
            from
            notas
            
            WHERE
          ( codFornecedor = {$idFornecedor} && codFornecedor <> 158)
            and 
            LPAD(notas.codigo, 255, '0') = LPAD('{$codDocumento}', 255, '0') AND `STATUS` <> 'Cancelada' ";
            
            

		return current(parent::get($sql));
	}

    public static function getParcelasPendentesByFiltros($filtros)
    {
        $condVencimento ='';
        $condFornecedor = '';
        $condTipoDocFinanceiro = '';
        $condUr = '';
        $condTr = '';
        $codNotInDocFinanceiro = '';
        $condNumNota = '';
        if(!empty($filtros['inicio']) && !empty($filtros['fim'])){
            $condVencimento = "AND p.vencimento_real between '{$filtros['inicio']}' AND '{$filtros['fim']}'" ;
        }
        if(!empty($filtros['tr'])){
            $condTr = "AND p.idNota = '{$filtros['tr']}'" ;
        }
        if(!empty($filtros['fornecedor'])){
            $condFornecedor = "AND n.codFornecedor = '{$filtros['fornecedor']}'" ;
        }
        if(!empty($filtros['tipo_documento_financeiro'])){
            $condTipoDocFinanceiro = "AND n.tipo_documento_financeiro_id = '{$filtros['tipo_documento_financeiro']}'" ;
        }
        if(!empty($filtros['tipo_nota'])){
            $tipo_nota = $filtros['tipo_nota'] == 'S' ? 1 : 0;
            $condTipoNota = "AND n.tipo = '{$tipo_nota}'" ;
        }
        if(!empty($filtros['empresa_user'])){
            $condUr = " AND n.empresa in {$filtros['empresa_user']}";
        }
        if(!empty($filtros['not_in_tipo_documento_financeiro'])){
            $codNotInDocFinanceiro = " AND n.tipo_documento_financeiro_id not in ({$filtros['not_in_tipo_documento_financeiro']})";
        }
        if(!empty($filtros['num_nota'])){           
            $condNumNota = "AND n.codigo = '{$filtros['num_nota']}'" ;
        }

        $sql = "select 
        n.*,        
        p.*,
        p.TR AS num_parcela,
        p.vencimento as data_vencimento,
        p.vencimento_real as data_vencimento_real,
        coalesce(f.razaoSocial, f.nome) as nome_fornecedor,
        e.nome as ur,
        p.status as pst,         
        p.valor as valor_pagar,
        t.sigla,
        p.id as id_parcela,
        p.idNota as id_nota,
        p.valor_compensada as valor_compensada_parcela
    from 
        notas as n inner join 
        fornecedores as f on (f.idFornecedores = n.codFornecedor) inner join
        empresas as e on (n.empresa = e.id) inner join 
        parcelas as p on (n.idNotas = p.idNota) inner join 
        tipo_documento_financeiro as t on (n.tipo_documento_financeiro_id = t.id)   
    where  
    p.status = 'Pendente' AND
    n.status <> 'Cancelada'
    AND p.nota_id_fatura_aglutinacao IS NULL
    AND n.nota_aglutinacao = 'n'
    {$condUr}
    {$condVencimento}
    {$condCompetencia}
    {$condFornecedor}
    {$condTipoDocFinanceiro}  
    {$condTipoNota}
    {$condTr}
    {$codNotInDocFinanceiro}
    {$condNumNota}
    and p.id not in(SELECT 
bp.parcela_id 
FROM 
bordero b 
inner join bordero_parcelas bp on (b.id=bp.bordero_id) 
WHERE 
b.ativo = 's')
    Group by 
        idNOTAS,p.id,p.tr
    order by 
    data_vencimento, nome_fornecedor ASC";

        return parent::get($sql, 'assoc');
    }

    public static function getParcelasPendentesBorderoByFiltros($filtros){

        $condVencimento ='';
        $condFornecedor = '';
        $condTipoDocFinanceiro = '';
        $condTr = '';
        $condUr = '';
        $codNotInDocFinanceiro = '';
        $codNotInFormaPagamento = '';
        $condLeftJoinContaFornecedor = '';
        $origemFundofornecedor = " '' as banco_fornecedor";
        
        if(!empty($filtros['inicio']) && !empty($filtros['fim'])){
            $condVencimento = "AND p.vencimento_real between '{$filtros['inicio']}' AND '{$filtros['fim']}'" ;
        }
        if(!empty($filtros['tr'])){
            $condTr = "AND p.idNota = '{$filtros['tr']}'" ;
        }
        if(!empty($filtros['fornecedor'])){
            $condFornecedor = "AND n.codFornecedor = '{$filtros['fornecedor']}'" ;
        }
        if(!empty($filtros['tipo_documento_financeiro'])){
            $condTipoDocFinanceiro = "AND n.tipo_documento_financeiro_id = '{$filtros['tipo_documento_financeiro']}'" ;
        }
        if(!empty($filtros['tipo_nota'])){
            $tipo_nota = $filtros['tipo_nota'] == 'S' ? 1 : 0;
            $condTipoNota = "AND n.tipo = '{$tipo_nota}'" ;
        }
        if(!empty($filtros['empresa_user'])){
            $condUr = " AND n.empresa in {$filtros['empresa_user']}";
        }
        if(!empty($filtros['not_in_tipo_documento_financeiro'])){
            $codNotInDocFinanceiro = " AND n.tipo_documento_financeiro_id not in ({$filtros['not_in_tipo_documento_financeiro']})";
        }
        if(!empty($filtros['not_in_forma_pagamento'])){
            $codNotInFormaPagamento = " AND n.forma_pagamento not in ({$filtros['not_in_forma_pagamento']})";
        }

        if($filtros['codigo_banco'] == 341){
            // origemfundo 136 é o itau
            $origemFundoItau = 136;
            $origemFundofornecedor = "cf.origem_fundos_id as banco_fornecedor";
            $condLeftJoinContaFornecedor = "left join 
            ( SELECT
            FORNECEDOR_ID,
            origem_fundos_id
            FROM 
            contas_fornecedor 
            where
            origem_fundos_id = {$origemFundoItau}) as cf on ( n.codFornecedor = cf.FORNECEDOR_ID)";
        }
        

        $sql = "select 
        n.*,        
        p.*,
        p.TR AS num_parcela,
        p.vencimento_real as data_vencimento,
        coalesce(f.razaoSocial, f.nome) as nome_fornecedor,
        e.nome as ur,
        p.status as pst,         
        p.valor as valor_pagar,
        t.sigla,
        p.id as id_parcela,
        p.idNota as id_nota,
        p.codBarras,
        ap.forma,
        f.chave_pix, 
        {$origemFundofornecedor}   
            
    from 
        notas as n inner join 
        fornecedores as f on (f.idFornecedores = n.codFornecedor) left join
        aplicacaofundos as ap on (n.forma_pagamento = ap.id)  inner join
        empresas as e on (n.empresa = e.id) inner join 
        parcelas as p on (n.idNotas = p.idNota) inner join 
        tipo_documento_financeiro as t on (n.tipo_documento_financeiro_id = t.id)  
        {$condLeftJoinContaFornecedor} 
        left join (SELECT
		p2.parcela_id
	FROM
		bordero_parcelas AS p2
	INNER JOIN bordero AS b ON
		b.id = p2.bordero_id
	WHERE
		b.ativo = 's'
		order by parcela_id) as tb on (p.id = tb.parcela_id) 
    where
    p.status = 'Pendente' AND
    n.status <> 'Cancelada'
    AND tb.parcela_id is null
    {$condUr}
    {$condVencimento}
    {$condFornecedor}
    {$condTipoDocFinanceiro} 
    {$condTipoNota}
    {$condTr}
    {$codNotInDocFinanceiro}
    {$codNotInFormaPagamento}
    Group by 
        idNOTAS,p.id,p.tr
    order by 
    data_vencimento, nome_fornecedor ASC";  

        return parent::get($sql, 'assoc');
    }

    public static function criarNotaAglutinacao($data)
    {
        $valorNota = array_reduce($data['parcelas_valor'], function ($buffer, $valor) {
            return $buffer += $valor;
        });

        $tipoNota = $data['tipo_nota'] == 'S' ? '1' : '0';
        $tipoDocumento = TipoDocumentoFinanceiro::getById($data['tipo_documento_nota'])['sigla'];

        mysql_query('BEGIN');

        $sql = <<<SQL
INSERT notas
(
 notas.USUARIO_ID, 
 notas.idNotas, 
 notas.CHAVE_ACESSO, 
 notas.codigo, 
 notas.codNatureza, 
 notas.codCentroCusto, 
 notas.valor, 
 notas.dataEntrada, 
 notas.descricao, 
 notas.codFornecedor, 
 notas.tipo, 
 notas.status, 
 notas.codSubNatureza, 
 notas.empresa, 
 notas.ICMS, 
 notas.FRETE, 
 notas.SEGURO, 
 notas.VAL_PRODUTOS, 
 notas.DES_ACESSORIAS, 
 notas.IPI, 
 notas.ISSQN, 
 notas.DESCONTO_BONIFICACAO, 
 notas.CHAVE, 
 notas.TR, 
 notas.TIPO_DOCUMENTO, 
 notas.PIS, 
 notas.COFINS, 
 notas.CSLL, 
 notas.IR, 
 notas.DATA, 
 notas.PCC, 
 notas.INSS, 
 notas.NOTA_REFERENCIA, 
 notas.IMPOSTOS_RETIDOS_ID, 
 notas.visualizado, 
 notas.visualizado_por, 
 notas.nota_fatura, 
 notas.tipo_documento_financeiro_id, 
 notas.natureza_movimentacao,
 notas.nota_aglutinacao,
 notas.data_competencia,
 notas.forma_pagamento
) VALUES
(
 '{$_SESSION['id_user']}',
 NULL,
 '',
 '{$data['numero_nota']}',
 0,
 1,
 '{$valorNota}',
 '{$data['data_emissao']}',
 '{$data['descricao']}',
 '{$data['fornecedor_nota']}',
 '{$tipoNota}',
 'Em aberto',
 0,
 '{$data['ur_nota']}',
 0,
 0,
 0,
 $valorNota,
 0,
 0,
 0,
 0,
 '',
 0,
 '{$tipoDocumento}',
 0,
 0,
 0,
 0,
 NOW(),
 0,
 0,
 0,
 0,
 1,
 '{$_SESSION['id_user']}',
 0,
 '{$data['tipo_documento_nota']}',
 '{$data['natureza_nota']}',
 's',
 '{$data['competencia_db']}',
 '{$data['forma_pagamento']}'
)
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }
        $notaId = mysql_insert_id();

        $sql = <<<SQL
UPDATE notas 
SET 
    TR = '{$notaId}'
WHERE
    idNotas = '{$notaId}' 
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        foreach ($data['parcelas_id'] as $key => $parcela) {
            $notaIdParcela = Parcela::getNotaByParcelaId($parcela)['idNota'];
            $valorPagoParcela = $data['parcelas_valor'][$key];

            $sql = <<<SQL
UPDATE parcelas 
SET 
    obs = 'PARCELA BAIXADA NA TR {$notaId}', 
    status = 'Processada',
    DATA_PAGAMENTO = '{$data['data_emissao']}',
    VALOR_PAGO = '{$valorPagoParcela}',
    DATA_PROCESSADO = '{$data['data_emissao']}',
    USUARIO_ID = '{$_SESSION['id_user']}',
    nota_id_fatura_aglutinacao = '{$notaId}'
WHERE
    id = '{$parcela}' 
SQL;
            $rs = mysql_query($sql);
            if(!$rs) {
                mysql_query('ROLLBACK');
                return false;
            }

            $statusNota = self::getStatusNotaUpdate($notaIdParcela);

            $sql = <<<SQL
UPDATE notas 
SET status = '{$statusNota}'
WHERE idNotas = '{$notaIdParcela}' 
SQL;
            $rs = mysql_query($sql);
            if(!$rs) {
                mysql_query('ROLLBACK');
                return false;
            }
        }

        $sql = <<<SQL
INSERT INTO parcelas
(
 id,
 idNota,
 valor,
 vencimento,
 codBarras,
 origem,
 aplicacao,
 obs,
 status,
 comprovante,
 empresa,
 JurosMulta,
 desconto,
 encargos,
 TR,
 VALOR_PAGO,
 DATA_PAGAMENTO,
 OUTROS_ACRESCIMOS,
 NUM_DOCUMENTO,
 ISSQN,
 PIS,
 COFINS,
 CSLL,
 IR,
 CONCILIADO,
 DATA_PROCESSADO,
 USUARIO_ID,
 DATA_CONCILIADO,
 DATA_DESPROCESSADA,
 DESPROCESSADA_POR,
 JUSTIFICATIVA_DESPROCESSADA,
 PARCELA_ORIGEM,
 motivo_juros_id,
 nota_id_fatura_aglutinacao,
 vencimento_real
) 
VALUES 
(
 NULL,
 '{$notaId}',
 '{$valorNota}',
 '{$data['data_vencimento']}',
 '',
 0,
 0,
 '',
 'Pendente',
 '',
 '{$data['ur_nota']}',
 0,
 0,
 0,
 1,
 0,
 '',
 0,
 '',
 0,
 0,
 0,
 0,
 0,
 'N',
 '',
 '{$_SESSION['id_user']}',
 '',
 '',
 0,
 '',
 0,
 NULL,
 NULL,
 '{$data['data_vencimento_real']}'
)
SQL;
        $rs = mysql_query($sql) or die(mysql_error());
        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        $ipcf = IPCF::getById($data['natureza_nota']);

        $sql = <<<SQL
INSERT INTO rateio
(
 ID, 
 NOTA_ID, 
 NOTA_TR, 
 CENTRO_RESULTADO, 
 VALOR, 
 PORCENTAGEM, 
 IPCG3_ANTIGA, 
 IPCG4_ANTIGA, 
 ASSINATURA, 
 IPCF2, 
 IPCG3, 
 IPCG4
) 
 VALUES
(
 NULL,
 '{$notaId}',
 '{$notaId}',
 '{$data['ur_nota']}',
 '{$valorNota}',
 100.00,
 '',
 '',
 '{$data['assinatura']}',
 '{$ipcf['N2']}',
 '{$ipcf['N3']}',
 '{$data['natureza_nota']}'
)
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }
        mysql_query('COMMIT');
        return $notaId;
    }

    public static function getNotasDeAglutinacao($filtros)
    {
        $condVencimento ='';
        $condCompetencia ='';
        $condFornecedor = '';
        $condTipoDocFinanceiro = '';
        if(!empty($filtros['inicio']) && !empty($filtros['fim'])){
            $condVencimento = " AND n.dataEntrada between '{$filtros['inicio']}' AND '{$filtros['fim']}'" ;
        }
        if(!empty($filtros['competencia_inicio']) && !empty($filtros['competencia_fim'])){
            $inicioCompetencia = $filtros['competencia_inicio'];
            $inicioCompetencia = \DateTime::createFromFormat('m/Y', $inicioCompetencia)->format('Y-m');
            $fimCompetencia = $filtros['competencia_fim'];
            $fimCompetencia = \DateTime::createFromFormat('m/Y', $fimCompetencia)->format('Y-m');
            $condCompetencia = "AND n.data_competencia between '{$inicioCompetencia}' AND '{$fimCompetencia}'" ;
        }
        if(!empty($filtros['fornecedor'])){
            $condFornecedor = " AND n.codFornecedor = '{$filtros['fornecedor']}'" ;
        }
        if(!empty($filtros['tipo_documento_financeiro'])){
            $condTipoDocFinanceiro = " AND n.tipo_documento_financeiro_id = '{$filtros['tipo_documento_financeiro']}'" ;
        }
        if(!empty($filtros['empresa_user'])){
            $condUr = " AND n.empresa in {$filtros['empresa_user']}";
        }

        $sql = "select 
        n.*,  
        IF(n.tipo = 0,'RECEBIMENTO','DESEMBOLSO') as tipo_NF,
        DATE_FORMAT(n.dataEntrada,'%d/%m/%Y') as data_emissao,
        coalesce(f.razaoSocial, f.nome) as nome_fornecedor,
        e.nome as ur,
        n.status as pst,         
        n.valor as valor_pagar,
        t.sigla,
        pl.N4
        
    from 
        notas as n inner join 
        fornecedores as f on (f.idFornecedores = n.codFornecedor) inner join
        empresas as e on (n.empresa = e.id) inner join 
        tipo_documento_financeiro as t on (n.tipo_documento_financeiro_id = t.id) inner join
        plano_contas as pl on (n.natureza_movimentacao = pl.ID) 
    where
    n.nota_aglutinacao = 'S' 
    AND n.status != 'Cancelada'
    {$condCompetencia}
    {$condVencimento}
    {$condFornecedor}
    {$condTipoDocFinanceiro} 
    {$condUr}       
    Group by 
        idNOTAS
    order by 
    n.dataEntrada, nome_fornecedor ASC";
    

        return parent::get($sql, 'assoc');
    }

    public static function desfazerNotaAglutinacao($notaId)
    {
        mysql_query('BEGIN');

        // VERIFICA SE A NOTA EM QUESTÃO NÃO ESTÁ PROCESSADA
        $sql = <<<SQL
SELECT status FROM notas WHERE idNotas = '{$notaId}'
SQL;
        $statusRow = current(parent::get($sql, 'Assoc'));

        // VERIFICAR SE EXISTE ALGUMA PARCELA PROCESSADA
        $sql = <<<SQL
SELECT COUNT(*) AS qtd_parcelas FROM parcelas WHERE idNota = '{$notaId}' AND status = 'Processada'
SQL;
        $parcelasRow = current(parent::get($sql, 'Assoc'));

        

        if($statusRow['status'] == 'Processada' || $parcelasRow['qtd_parcelas'] > 0){
            return 'processada';
        }

        // CANCELAMENTO DAS PARCELAS DE AGLUTINAÇÃO
        $sql = <<<SQL
UPDATE parcelas SET status = 'Cancelada' WHERE idNota = '{$notaId}'
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            echo "Erro ao cancelar parcelas";
            mysql_query('ROLLBACK');
            return false;
        }

        // EXCLUSÃO DO RATEIO DA NOTA DE AGLUTINAÇÃO
        /*$sql = <<<SQL
DELETE FROM rateio WHERE NOTA_ID = '{$notaId}'
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }*/

        // ATUALIZA DA NOTA DE AGLUTINAÇÃO
        $sql = <<<SQL
UPDATE notas SET status = 'Cancelada' WHERE idNotas = '{$notaId}'
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            echo "Erro ao cancelar notas";
            mysql_query('ROLLBACK');
            return false;
        }

        // CRIA HISTÓRICO DA DESAGLUTINAÇÃO
//         $sql = <<<SQL
// INSERT INTO historico_notas_desaglutinacao (id, nota_id, usuario_id, data_hora) 
// VALUES (NULL, '{$notaId}', '{$_SESSION['id_user']}', NOW()) 
// SQL;
//         $rs = mysql_query($sql);
//         if(!$rs) {
//             echo "Erro ao gravar historico";
//             mysql_query('ROLLBACK');
//             return false;
//         }

        $notasBaixadas = Parcela::getNotasFromParcelasByNotaAglutinacaoId($notaId);

        foreach ($notasBaixadas as $nota) {
            

        // ATUALIZO A REFERENCIA DA NOTA DE AGLUTINAÇÃO PARA 'NULL'
        $sql = <<<SQL
UPDATE parcelas 
SET nota_id_fatura_aglutinacao = NULL, obs = '', VALOR_PAGO = NULL,
    DATA_PAGAMENTO = NULL, DATA_PROCESSADO = NULL, status = 'Pendente'
WHERE nota_id_fatura_aglutinacao = '{$notaId}'
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        $statusNota = 'Em Aberto';
            $check = Parcela::checkIfNotaHasParcelasProcessadas($nota['idNotas']);
            if($check) {
                $statusNota = 'Pendente';
            }
            $sql = <<<SQL
UPDATE notas SET status = '{$statusNota}' WHERE idNotas = '{$nota['idNotas']}'
SQL;
            $rs = mysql_query($sql);
            if(!$rs) {
                mysql_query('ROLLBACK');
                return false;
            }
        }
        mysql_query('COMMIT');
        return true;
    }

    
public static function criarNotaAdiantamento($data)
    {       

        $tipoNota = $data['tipo_nota'] == 'S' ? '1' : '0';
        $responseTipoDocumento = TipoDocumentoFinanceiro::getById($data['tipo_documento_nota']);
        $tipoDocumento = $responseTipoDocumento[0]['sigla'];
       

        mysql_query('BEGIN');

        $sql = "
INSERT notas
(
 notas.USUARIO_ID, 
 notas.idNotas, 
 notas.CHAVE_ACESSO, 
 notas.codigo, 
 notas.codNatureza, 
 notas.codCentroCusto, 
 notas.valor, 
 notas.dataEntrada, 
 notas.data_competencia,
 notas.descricao, 
 notas.codFornecedor, 
 notas.tipo, 
 notas.status, 
 notas.codSubNatureza, 
 notas.empresa, 
 notas.ICMS, 
 notas.FRETE, 
 notas.SEGURO, 
 notas.VAL_PRODUTOS, 
 notas.DES_ACESSORIAS, 
 notas.IPI, 
 notas.ISSQN, 
 notas.DESCONTO_BONIFICACAO, 
 notas.CHAVE, 
 notas.TR, 
 notas.TIPO_DOCUMENTO, 
 notas.PIS, 
 notas.COFINS, 
 notas.CSLL, 
 notas.IR, 
 notas.DATA, 
 notas.PCC, 
 notas.INSS, 
 notas.NOTA_REFERENCIA, 
 notas.IMPOSTOS_RETIDOS_ID, 
 notas.visualizado, 
 notas.visualizado_por, 
 notas.nota_fatura, 
 notas.tipo_documento_financeiro_id, 
 notas.natureza_movimentacao,
 notas.nota_adiantamento,
 forma_pagamento
) VALUES
(
 '{$_SESSION['id_user']}',
 NULL,
 '',
 '{$data['numero_nota']}',
 0,
 1,
 '{$data['valor']}', 
 '{$data['data_emissao']}',
 '{$data['data_competencia']}',
 '{$data['descricao']}',
 '{$data['fornecedor_nota']}',
 '{$tipoNota}',
 'Processada',
 0,
 '{$data['ur_nota']}',
 0,
 0,
 0,
 '{$data['valor']}',
 0,
 0,
 0,
 0,
 '',
 0,
 '{$tipoDocumento}',
 0,
 0,
 0,
 0,
 NOW(),
 0,
 0,
 0,
 0,
 1,
 '{$_SESSION['id_user']}',
 0,
 '{$data['tipo_documento_nota']}',
 '{$data['natureza_nota']}',
 'S',
 '{$data['forma_pagamento']}' 
)
";
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao gerar Nota'

            ];
            return $response;
        }
        $notaId = mysql_insert_id();

        $sql = <<<SQL
UPDATE notas 
SET 
    TR = '{$notaId}'
WHERE
    idNotas = '{$notaId}' 
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao atualizar Nota'

            ];
            return $response;
            
        }
$sql = <<<SQL
INSERT INTO parcelas
(
 id,
 idNota,
 valor,
 vencimento,
 codBarras,
 origem,
 aplicacao,
 obs,
 status,
 comprovante,
 empresa,
 JurosMulta,
 desconto,
 encargos,
 TR,
 VALOR_PAGO,
 DATA_PAGAMENTO,
 OUTROS_ACRESCIMOS,
 NUM_DOCUMENTO,
 ISSQN,
 PIS,
 COFINS,
 CSLL,
 IR,
 CONCILIADO,
 DATA_PROCESSADO,
 USUARIO_ID,
 DATA_CONCILIADO,
 DATA_DESPROCESSADA,
 DESPROCESSADA_POR,
 JUSTIFICATIVA_DESPROCESSADA,
 PARCELA_ORIGEM,
 motivo_juros_id,
 nota_id_fatura_aglutinacao,
 vencimento_real
) 
VALUES 
(
 NULL,
 '{$notaId}',
 '{$data['valor']}',
 '{$data['data_emissao']}',
 '{$data['cod_barras']}',
 '{$data['origem']}',
 '{$data['forma_pagamento']}',
 '',
 'Processada',
 '',
 '{$data['ur_nota']}',
 0,
 0,
 0,
 1,
 '{$data['valor']}',
 '{$data['data_emissao']}',
 0,
 '',
 0,
 0,
 0,
 0,
 0,
 'N',
 now(),
 '{$_SESSION['id_user']}',
 '',
 '',
 0,
 '',
 0,
 NULL,
 NULL,
 '{$data['data_emissao']}'
)
SQL;
        $rs = mysql_query($sql) ;

        if(!$rs) {
            mysql_query('ROLLBACK');
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao gerar Parcela'

            ];
            return $response;
            
        }

        $ipcf = IPCF::getById($data['natureza_nota']);

        $sql = <<<SQL
INSERT INTO rateio
(
 ID, 
 NOTA_ID, 
 NOTA_TR, 
 CENTRO_RESULTADO, 
 VALOR, 
 PORCENTAGEM, 
 IPCG3_ANTIGA, 
 IPCG4_ANTIGA, 
 ASSINATURA, 
 IPCF2, 
 IPCG3, 
 IPCG4
) 
 VALUES
(
 NULL,
 '{$notaId}',
 '{$notaId}',
 '{$data['ur_nota']}',
 '{$data['valor']}', 
 100.00,
 '',
 '',
 '{$data['assinatura']}',
 '{$ipcf['N2']}',
 '{$ipcf['N3']}',
 '{$data['natureza_nota']}'
)
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao gerar Rateio'

            ];
            return $response;
            
        }
        mysql_query('COMMIT');
        $response = ['tipo' => 'acerto',
            'msg'=>$notaId

            ];
            return $response;
       
    }

    public static function editarNotaAdiantamento($data)
    {      
        
        $tipoNota = $data['tipo_nota'] == 'S' ? '1' : '0';
        $responseTipoDocumento = TipoDocumentoFinanceiro::getById($data['tipo_documento_nota']);
        $tipoDocumento =  $responseTipoDocumento[0]['sigla'];

        mysql_query('BEGIN');

        $sql = "
update notas
set
 notas.USUARIO_ID = '{$_SESSION['id_user']}',  
 notas.codigo = '{$data['numero_nota']}',  
 notas.valor = '{$data['valor']}', 
 notas.dataEntrada = '{$data['data_emissao']}', 
 notas.data_competencia = '{$data['data_competencia']}',
 notas.descricao = '{$data['descricao']}', 
 notas.codFornecedor = '{$data['fornecedor_nota']}', 
 notas.tipo = '{$tipoNota}', 
 notas.status = 'Processada',  
 notas.empresa = '{$data['ur_nota']}',
 notas.VAL_PRODUTOS = '{$data['valor']}',  
 notas.TIPO_DOCUMENTO = '{$tipoDocumento}',   
 notas.DATA = NOW(),  
 notas.visualizado_por = '{$_SESSION['id_user']}', 
 notas.tipo_documento_financeiro_id =  '{$data['tipo_documento_nota']}', 
 notas.natureza_movimentacao = '{$data['natureza_nota']}',
 forma_pagamento  =  '{$data['forma_pagamento']}' 
where
notas.idNotas = '{$data['nota_id']}'
";

        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao editar Nota'

            ];
            return  $response;
        }
        $notaId = mysql_insert_id();

        
$sql = <<<SQL
Update parcelas
set 
 valor = '{$data['valor']}',
 vencimento = '{$data['data_emissao']}',
 codBarras = '{$data['cod_barras']}', 
 status = 'Processada',
 empresa = '{$data['ur_nota']}', 
 VALOR_PAGO = '{$data['valor']}',
 DATA_PAGAMENTO = '{$data['data_emissao']}', 
 DATA_PROCESSADO = now(),
 USUARIO_ID = '{$_SESSION['id_user']}', 
 vencimento_real = '{$data['data_emissao']}',
 origem = '{$data['origem']}', 
 aplicacao = '{$data['forma_pagamento']}'
 where
 idNota = '{$data['nota_id']}'
SQL;
        $rs = mysql_query($sql) ;
        if(!$rs) {
            mysql_query('ROLLBACK');
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao editar Parcela'

            ];
            return $response;
        }

        $ipcf = IPCF::getById($data['natureza_nota']);

        $sql = <<<SQL
update
 rateio
set 
 CENTRO_RESULTADO =  '{$data['ur_nota']}', 
 VALOR = '{$data['valor']}', 
 PORCENTAGEM = 100.00, 
 ASSINATURA = '{$data['assinatura']}', 
 IPCF2 = '{$ipcf['N2']}', 
 IPCG3 = '{$ipcf['N3']}', 
 IPCG4 = '{$data['natureza_nota']}'
 where
 NOTA_ID = '{$data['nota_id']}'
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao editar Rateio'

            ];
            return $response;
        }
        mysql_query('COMMIT');
        $response = ['tipo' => 'acerto',
            'msg'=>$data['nota_id']

            ];
        return $response;
    }

    public static function getNotasAntecipadas($filtros){
        $condVencimento ='';
        $condCompetencia ='';
        $condFornecedor = '';
        $condTipoDocFinanceiro = '';
        $codIdNota = '';
        $condUr = '';
        if(!empty($filtros['id']) && !empty($filtros['id'])){
            $codIdNota = " and idNotas = '{$filtros['id']}'";
        }else{
            if(!empty($filtros['inicio']) && !empty($filtros['fim'])){
                $condVencimento = "AND n.dataEntrada between '{$filtros['inicio']}' AND '{$filtros['fim']}'" ;
            }
            if(!empty($filtros['competencia_inicio']) && !empty($filtros['competencia_fim'])){
                $inicioCompetencia = $filtros['competencia_inicio'];
                $inicioCompetencia = \DateTime::createFromFormat('m/Y', $inicioCompetencia)->format('Y-m');
                $fimCompetencia = $filtros['competencia_fim'];
                $fimCompetencia = \DateTime::createFromFormat('m/Y', $fimCompetencia)->format('Y-m');
                $condCompetencia = "AND n.data_competencia between '{$inicioCompetencia}' AND '{$fimCompetencia}'" ;
            }
            if(!empty($filtros['fornecedor'])){
                $condFornecedor = "AND n.codFornecedor = '{$filtros['fornecedor']}'" ;
            }
            if(!empty($filtros['tipo_documento_financeiro'])){
                $condTipoDocFinanceiro = "AND n.tipo_documento_financeiro_id = '{$filtros['tipo_documento_financeiro']}'" ;
            }
            if(!empty($filtros['empresa_user'])){
                $condUr = " AND n.empresa in {$filtros['empresa_user']}";
            }

        }
        

        $sql = "select 
        n.*,  
        IF(n.tipo = 0,'RECEBIMENTO','DESEMBOLSO') as tipo_NF,
        DATE_FORMAT(n.dataEntrada,'%d/%m/%Y') as data_emissao,
        coalesce(f.razaoSocial, f.nome) as nome_fornecedor,
        e.nome as ur,
        n.status as pst,         
        n.valor as valor_pagar,
        t.sigla,
        pl.N4,
        p.CONCILIADO,
        n.dataEntrada,
        p.origem,
        p.codBarras,
        p.aplicacao,
        r.ASSINATURA,
        p.DATA_CONCILIADO,
        p.id as idParcela,
        p.compensada as parcelaCompensada,
        p.valor_compensada         
    from 
        notas as n inner join 
        fornecedores as f on (f.idFornecedores = n.codFornecedor) inner join
        empresas as e on (n.empresa = e.id) inner join 
        tipo_documento_financeiro as t on (n.tipo_documento_financeiro_id = t.id) inner join
        plano_contas as pl on (n.natureza_movimentacao = pl.ID) inner join 
        parcelas as p on (n.idNotas = p.idNota) inner join
        rateio as r on (n.idNotas = r.NOTA_ID)
    where   
     t.adiantamento = 'S' and
     n.nota_adiantamento = 'S'
     {$codIdNota}
    {$condVencimento}
    {$condCompetencia}
    {$condFornecedor}
    {$condTipoDocFinanceiro} 
    {$condUr}       
    Group by 
        idNOTAS
    order by 
    n.dataEntrada, nome_fornecedor ASC";
    

        return parent::get($sql, 'assoc');
    }

    public static function cancelar($data){

        mysql_query('begin');
        $sql = "UPDATE 
        notas 
        SET 
        status = 'Cancelada', 
        descricao = 'Cancelada: {$data['motivo']}',
        cancelado_por = '{$_SESSION['id_user']}',
        cancelado_em = now() 
        WHERE 
        idNotas = '{$data['id']}'";
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            return 'Erro ao cancelar Nota';
        }

        savelog(mysql_escape_string(addslashes($sql)));
        if(!mysql_query($sql)){ mysql_query('rollback'); echo -1; return;}
        $sql = "UPDATE
         parcelas
          SET 
          status = 'Cancelada', obs = 'Cancelada: Cancelamento da nota.' ,
          CONCILIADO = 'N',
          DATA_CONCILIADO = null
          WHERE 
          idNota = '{$data['id']}'";
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            return 'Erro ao cancelar Parcela';
        }
        mysql_query('commit');
        return $data['id'];
    }

    public static function getStatusNotaUpdate($idNota)
    {
        $sql = <<<SQL
SELECT * FROM parcelas WHERE idNota = '{$idNota}' AND status != 'Cancelada'
SQL;
        $parcelas = parent::get($sql);
        $qtdProcessadas = 0;
        $qtdPendentes = 0;
        foreach ( $parcelas as $parcela ) {
            if($parcela['status'] == 'Pendente') {
                $qtdPendentes++;
            }
            if($parcela['status'] == 'Processada') {
                $qtdProcessadas++;
            }
        }

        if($qtdPendentes > 0 && $qtdProcessadas > 0) {
            return 'Pendente';
        } elseif($qtdPendentes == 0 && $qtdProcessadas > 0) {
            return 'Processada';
        }
        return 'Em aberto';
    }

    public static function getNotasPendentesByFiltros($filtros)
    {
        $condVencimento ='';
        $condFornecedor = '';
        $condTipoDocFinanceiro = '';
        $condUr = '';
        $condTr = '';
        if(
            (isset($filtros['inicio']) && isset($filtros['fim'])) &&
            (!empty($filtros['inicio']) && !empty($filtros['fim']))
        ) {
            $condVencimento = " AND n.dataEntrada between '{$filtros['inicio']}' AND '{$filtros['fim']}' " ;
        }
        if(isset($filtros['tr']) && !empty($filtros['tr'])){
            $condTr = "AND n.idNotas = '{$filtros['tr']}'" ;
        }
        if(isset($filtros['fornecedor']) && !empty($filtros['fornecedor'])) {
            if(!is_array($filtros['fornecedor'])) {
                $condFornecedor = "AND n.codFornecedor = '{$filtros['fornecedor']}'";
            } else {
                $fornecedores = implode(', ', $filtros['fornecedor']);
                $condFornecedor = "AND n.codFornecedor IN ({$fornecedores})";
            }
        }
        if(isset($filtros['tipo_documento_financeiro']) && !empty($filtros['tipo_documento_financeiro'])){
            $condTipoDocFinanceiro = "AND n.tipo_documento_financeiro_id = '{$filtros['tipo_documento_financeiro']}'" ;
        }
        if(isset($filtros['tipo_nota']) && !empty($filtros['tipo_nota'])){
            $tipo_nota = $filtros['tipo_nota'] == 'S' ? 1 : 0;
            $condTipoNota = "AND n.tipo = '{$tipo_nota}'" ;
        }
        if(isset($filtros['empresa_user']) && !empty($filtros['empresa_user'])){
            $condUr = " AND n.empresa in {$filtros['empresa_user']}";
        }

        $sql = "select 
        n.*, 
        coalesce(f.razaoSocial, f.nome) as nome_fornecedor,
        e.nome as ur,
        n.status as pst,         
        p.valor as valor_pagar_receber,
        t.sigla,
        p.id as id_parcela,
        p.idNota as id_nota,
        p.valor_compensada as valor_compensada_parcela
    from 
        notas as n inner join 
        fornecedores as f on (f.idFornecedores = n.codFornecedor) inner join
        empresas as e on (n.empresa = e.id) inner join 
        parcelas as p on (n.idNotas = p.idNota) inner join 
        tipo_documento_financeiro as t on (n.tipo_documento_financeiro_id = t.id)    
    where
        p.status = 'Pendente' AND
        n.status <> 'Cancelada'
        AND p.nota_id_fatura_aglutinacao IS NULL
        AND n.nota_aglutinacao = 'n'
        {$condUr}
        {$condVencimento}
        {$condFornecedor}
        {$condTipoDocFinanceiro}  
        {$condTr}
    Group by 
        idNotas
    order by 
        n.dataEntrada, nome_fornecedor ASC";

        return parent::get($sql, 'assoc');
    }

    public static function criarNotaCaixaFixo($data)
    {       

        $tipoNota = $data['tipo_nota'] == 'S' ? '1' : '0';
        $responseTipoDocumento = TipoDocumentoFinanceiro::getById($data['tipo_documento_nota']);
        $tipoDocumento = $responseTipoDocumento[0]['sigla'];

        mysql_query('BEGIN');

        $sql = "
INSERT notas
(
 notas.USUARIO_ID, 
 notas.idNotas, 
 notas.CHAVE_ACESSO, 
 notas.codigo, 
 notas.codNatureza, 
 notas.codCentroCusto, 
 notas.valor, 
 notas.dataEntrada, 
 notas.descricao, 
 notas.codFornecedor, 
 notas.tipo, 
 notas.status, 
 notas.codSubNatureza, 
 notas.empresa, 
 notas.ICMS, 
 notas.FRETE, 
 notas.SEGURO, 
 notas.VAL_PRODUTOS, 
 notas.DES_ACESSORIAS, 
 notas.IPI, 
 notas.ISSQN, 
 notas.DESCONTO_BONIFICACAO, 
 notas.CHAVE, 
 notas.TR, 
 notas.TIPO_DOCUMENTO, 
 notas.PIS, 
 notas.COFINS, 
 notas.CSLL, 
 notas.IR, 
 notas.DATA, 
 notas.PCC, 
 notas.INSS, 
 notas.NOTA_REFERENCIA, 
 notas.IMPOSTOS_RETIDOS_ID, 
 notas.visualizado, 
 notas.visualizado_por, 
 notas.nota_fatura, 
 notas.tipo_documento_financeiro_id, 
 notas.natureza_movimentacao,
 notas.nota_adiantamento,
 notas.inserida_caixa_fixo,
 notas.data_competencia,
 forma_pagamento
) VALUES
(
 '{$_SESSION['id_user']}',
 NULL,
 '',
 '{$data['numero_nota']}',
 0,
 1,
 '{$data['valor']}',
 '{$data['data_emissao']}',
 '{$data['descricao']}',
 '{$data['fornecedor_nota']}',
 '{$tipoNota}',
 'Processada',
 0,
 '{$data['ur_nota']}',
 0,
 0,
 0,
 '{$data['valor']}',
 0,
 0,
 0,
 0,
 '',
 0,
 '{$tipoDocumento}',
 0,
 0,
 0,
 0,
 NOW(),
 0,
 0,
 0,
 0,
 1,
 '{$_SESSION['id_user']}',
 0,
 '{$data['tipo_documento_nota']}',
 '{$data['natureza_nota']}',
 'N',
 'S',
 '{$data['competencia']}',
 '{$data['forma_pagamento']}'
)
";
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao gerar Nota'

            ];
            return $response;
        }
        $notaId = mysql_insert_id();

        $sql = <<<SQL
UPDATE notas 
SET 
    TR = '{$notaId}'
WHERE
    idNotas = '{$notaId}' 
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao atualizar Nota'

            ];
            return $response;
            
        }
$sql = <<<SQL
INSERT INTO parcelas
(
 id,
 idNota,
 valor,
 vencimento,
 codBarras,
 origem,
 aplicacao,
 obs,
 status,
 comprovante,
 empresa,
 JurosMulta,
 desconto,
 encargos,
 TR,
 VALOR_PAGO,
 DATA_PAGAMENTO,
 OUTROS_ACRESCIMOS,
 NUM_DOCUMENTO,
 ISSQN,
 PIS,
 COFINS,
 CSLL,
 IR,
 CONCILIADO,
 DATA_PROCESSADO,
 USUARIO_ID,
 DATA_CONCILIADO,
 DATA_DESPROCESSADA,
 DESPROCESSADA_POR,
 JUSTIFICATIVA_DESPROCESSADA,
 PARCELA_ORIGEM,
 motivo_juros_id,
 nota_id_fatura_aglutinacao,
 vencimento_real

) 
VALUES 
(
 NULL,
 '{$notaId}',
 '{$data['valor']}',
 '{$data['data_emissao']}',
 '{$data['cod_barras']}',
 '{$data['origem']}',
 '{$data['forma_pagamento']}',
 '',
 'Processada',
 '',
 '{$data['ur_nota']}',
 0,
 0,
 0,
 1,
 '{$data['valor']}',
 '{$data['data_pagamento']}',
 0,
 '',
 0,
 0,
 0,
 0,
 0,
 'N',
 now(),
 '{$_SESSION['id_user']}',
 '',
 '',
 0,
 '',
 0,
 NULL,
 NULL,
 '{$data['data_emissao']}'
)
SQL;
        $rs = mysql_query($sql) ;

        if(!$rs) {
            mysql_query('ROLLBACK');
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao gerar Parcela'

            ];
            return $response;
            
        }

        $ipcf = IPCF::getById($data['natureza_nota']);

        $sql = <<<SQL
INSERT INTO rateio
(
 ID, 
 NOTA_ID, 
 NOTA_TR, 
 CENTRO_RESULTADO, 
 VALOR, 
 PORCENTAGEM, 
 IPCG3_ANTIGA, 
 IPCG4_ANTIGA, 
 ASSINATURA, 
 IPCF2, 
 IPCG3, 
 IPCG4
) 
 VALUES
(
 NULL,
 '{$notaId}',
 '{$notaId}',
 '{$data['ur_nota']}',
 '{$data['valor']}', 
 100.00,
 '',
 '',
 '{$data['assinatura']}',
 '{$ipcf['N2']}',
 '{$ipcf['N3']}',
 '{$data['natureza_nota']}'
)
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao gerar Rateio'

            ];
            return $response;
            
        }
        mysql_query('COMMIT');
        $response = ['tipo' => 'acerto',
            'msg'=>$notaId

            ];
            return $response;
       
    }

    public static function getNotasCaixaFixo($filtros)
    {
        $condUr = '';
        $condIdNota = '';
        $condInicioEntrada ='';
        $condCompetencia ='';
        $condFimEntrada = '';
        $condFornecedor = '';
        $condTipoDocFinanceiro = '';
        $codndInicioPagamento ='';
        $condFimPagamento ='';
        $condDescricaoNota ='';
        if(!empty($filtros['tr']) && !empty($filtros['tr'])){
            $condIdNota = " and n.idNotas = '{$filtros['tr']}'";
        }else{ 
             if(!empty($filtros['inicio'])){
                $condInicioEntrada = " AND n.dataEntrada >= '{$filtros['inicio']}'" ;
            }
            if( !empty($filtros['fim'])){
                $condFimEntrada = " AND n.dataEntrada <= '{$filtros['fim']}' " ;
            }
            if(!empty($filtros['competencia_inicio_db']) && !empty($filtros['competencia_fim_db'])){
                $inicioCompetencia = $filtros['competencia_inicio_db'];
                $fimCompetencia = $filtros['competencia_fim_db'];
                $condCompetencia = "AND n.data_competencia between '{$inicioCompetencia}' AND '{$fimCompetencia}'" ;
            }
            if(!empty($filtros['fornecedor'])){
                $condFornecedor = " AND n.codFornecedor = '{$filtros['fornecedor']}'" ;
            }
            if(!empty($filtros['tipo_documento_financeiro'])){
                $condTipoDocFinanceiro = " AND n.tipo_documento_financeiro_id = '{$filtros['tipo_documento_financeiro']}'" ;
            }
            if(!empty($filtros['empresa_user'])){
                $condUr = " AND n.empresa in {$filtros['empresa_user']}";
            }
            if(!empty($filtros['inicioPagamento'])){
                $codndInicioPagamento = " AND p.DATA_PAGAMENTO  >= '{$filtros['inicioPagamento']}' " ;
            }
            if(!empty($filtros['fimPagamento'])){
                $condFimPagamento = " AND p.DATA_PAGAMENTO  <= '{$filtros['fimPagamento']}' " ;
            }
            if(!empty($filtros['descricaoNota'])){
                $condDescricaoNota = " AND n.descricao LIKE '%{$filtros['descricaoNota']}%'" ;
            }
        }

        $sql = "select 
        n.*,  
        IF(n.tipo = 0,'RECEBIMENTO','DESEMBOLSO') as tipo_NF,
        DATE_FORMAT(n.dataEntrada,'%d/%m/%Y') as data_emissao,
        DATE_FORMAT(p.DATA_PAGAMENTO,'%d/%m/%Y') as data_pg,
        coalesce(f.razaoSocial, f.nome) as nome_fornecedor,
        n.data_competencia,
        e.nome as ur,
        n.status as pst,         
        n.valor as valor_pagar,
        t.sigla,
        pl.N4,
        p.CONCILIADO,
        n.dataEntrada,
        p.origem,
        p.codBarras,
        p.aplicacao,
        r.ASSINATURA,
        p.DATA_CONCILIADO,
        p.id as idParcela,
        p.compensada as parcelaCompensada,
        p.valor_compensada ,
        p.DATA_PAGAMENTO as dataPagamento,
        n.idNotas as trNota 
    from 
        notas as n inner join 
        fornecedores as f on (f.idFornecedores = n.codFornecedor) inner join
        empresas as e on (n.empresa = e.id) inner join 
        tipo_documento_financeiro as t on (n.tipo_documento_financeiro_id = t.id) inner join
        plano_contas as pl on (n.natureza_movimentacao = pl.ID) inner join
        parcelas as p on (n.idNotas = p.idNota) inner join
        rateio as r on (n.idNotas = r.NOTA_ID) 
    where
    n.inserida_caixa_fixo = 'S' 
    AND n.status != 'Cancelada'
    {$condIdNota}
    {$condCompetencia}
    {$condInicioEntrada}
    {$condFimEntrada}
    {$condFornecedor}
    {$condTipoDocFinanceiro} 
    {$condUr}    
    {$codndInicioPagamento}  
    {$condFimPagamento}
    {$condDescricaoNota} 
    Group by 
        idNOTAS
    order by 
    n.dataEntrada, nome_fornecedor ASC";

        return parent::get($sql, 'assoc');
    }

    public static function editarNotaCaixaFixo($data)
    {      
        
        $tipoNota = $data['tipo_nota'] == 'S' ? '1' : '0';
        $responseTipoDocumento = TipoDocumentoFinanceiro::getById($data['tipo_documento_nota']);
        $tipoDocumento =  $responseTipoDocumento[0]['sigla'];

        mysql_query('BEGIN');

        $sql = "
update notas
set
 notas.USUARIO_ID = '{$_SESSION['id_user']}',  
 notas.codigo = '{$data['numero_nota']}',  
 notas.valor = '{$data['valor']}', 
 notas.data_competencia = '{$data['competencia']}', 
 notas.dataEntrada = '{$data['data_emissao']}', 
 notas.descricao = '{$data['descricao']}', 
 notas.codFornecedor = '{$data['fornecedor_nota']}', 
 notas.tipo = '{$tipoNota}', 
 notas.status = 'Processada',  
 notas.empresa = '{$data['ur_nota']}',
 notas.VAL_PRODUTOS = '{$data['valor']}',  
 notas.TIPO_DOCUMENTO = '{$tipoDocumento}',   
 notas.DATA = NOW(),  
 notas.visualizado_por = '{$_SESSION['id_user']}', 
 notas.tipo_documento_financeiro_id =  '{$data['tipo_documento_nota']}', 
 notas.natureza_movimentacao = '{$data['natureza_nota']}'
where
notas.idNotas = '{$data['nota_id']}'
";

        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao editar Nota'

            ];
            return  $response;
        }
        $notaId = mysql_insert_id();

        
$sql = <<<SQL
Update parcelas
set 
 valor = '{$data['valor']}',
 vencimento = '{$data['data_emissao']}',
 codBarras = '{$data['cod_barras']}', 
 status = 'Processada',
 empresa = '{$data['ur_nota']}', 
 VALOR_PAGO = '{$data['valor']}',
 DATA_PAGAMENTO = '{$data['data_pagamento']}', 
 DATA_PROCESSADO = now(),
 USUARIO_ID = '{$_SESSION['id_user']}', 
 vencimento_real = '{$data['data_emissao']}',
 origem = '{$data['origem']}', 
 aplicacao = '{$data['forma_pagamento']}'
 where
 idNota = '{$data['nota_id']}'
SQL;
        $rs = mysql_query($sql) ;
        if(!$rs) {
            mysql_query('ROLLBACK');
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao editar Parcela'

            ];
            return $response;
        }

        $ipcf = IPCF::getById($data['natureza_nota']);

        $sql = <<<SQL
update
 rateio
set 
 CENTRO_RESULTADO =  '{$data['ur_nota']}', 
 VALOR = '{$data['valor']}', 
 PORCENTAGEM = 100.00, 
 ASSINATURA = '{$data['assinatura']}', 
 IPCF2 = '{$ipcf['N2']}', 
 IPCG3 = '{$ipcf['N3']}', 
 IPCG4 = '{$data['natureza_nota']}'
 where
 NOTA_ID = '{$data['nota_id']}'
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            $response = ['tipo' => 'erro',
            'msg'=>'Erro ao editar Rateio'

            ];
            return $response;
        }
        mysql_query('COMMIT');
        $response = ['tipo' => 'acerto',
            'msg'=>$data['nota_id']

            ];
        return $response;
    }

    public static function editarCompetencia($idNota, $competencia)
    { 
        $sql = "Update notas set data_competencia = '{$competencia}' where idNotas ={$idNota}";
    
        $result=  mysql_query($sql);
        if (!$result){
            $response = ['tipo' => 'erro',
            'msg'=>$idNota

            ];
            
        }else{
            $response = ['tipo' => 'acerto',
            'msg'=>$idNota
            ];
        }
        
        return $response;
    }

    
    
}
