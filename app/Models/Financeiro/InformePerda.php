<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
	\App\Models\DAOInterface;

class InformePerda extends AbstractModel
{
    public static function salvarInforme($data){
        mysql_query('BEGIN');
        $responseNota = Notas::getNotaById($data['notaId']);       
        if($responseNota){
            // A baixa da nota é uma devolução o tipo de movimentação é diferente do da nota original
            $tipoMovimentacao = $responseNota['tipo'];
            // tabela plano de contas 
            
            $natureza = 168;
            
            $sql = "
            Insert into
                nota_informe_perda
            (
                `parcela_id`, 
                `nota_id`,
                `valor`,
                data,                
                `tipo_movimentacao`,                
                `plano_conta_id`,
                justificativa,
                `created_by`,
                `created_at`
                           
            )
            values
            (
                '{$data['parcelaId']}',
                '{$data['notaId']}',
                '{$data['valor']}',
                '{$data['data']}',
                '{$tipoMovimentacao}',                
                '{$natureza}',   
                '{$data['justificativa']}',                                 
                 '{$_SESSION['id_user']}',
                 now()
            )";  
         
            $rs = mysql_query($sql);

            if(!$rs) {
                mysql_query('ROLLBACK');
                $response = ['tipo' => 'erro',
                      'msg'=>"Erro ao inserir Informe de Perda. Entre em contato com o TI"
                    ];                    
                return $response;
            }

            $sql = "update
                parcelas 
                set                
                compensada = 'TOTAL',
                valor_compensada = '{$data['valor']}',
                obs = CONCAT('Baixada por Informe de perda.',' ','{$data['justificativa']}'),
                perda = 'S',
                status = 'Processada',
                DATA_PAGAMENTO = '{$data['data']}'             
                where
                id = {$data['parcelaId']}
                ";
                $rs = mysql_query($sql);

                if(!$rs) {
                    mysql_query('ROLLBACK');
                    $response = ['tipo' => 'erro',
                          'msg'=>"Erro ao atualizar Parcela."
                        ];                    
                    return $response ;
                }

                // Atualizando nota da parcela que tá compensando a pivo. 
                $statusNota = 'Processada';
                $check = Parcela::checkIfNotaHasParcelasPendentes($data['notaId']);
                if($check) {
                    $statusNota = 'Pendente';
                }
                
                    $sql = "
                    UPDATE 
                    notas 
                    SET 
                    status = '{$statusNota}'
                    WHERE idNotas = '{$data['notaId']}'
                    ";
                    $rs = mysql_query($sql);
                    if(!$rs) {
                        $response = ['tipo' => 'erro',
                          'msg'=>"Erro ao atualizar nota."
                        ]; 
                        return $response; 
                    }

                mysql_query('COMMIT');

                $response = ['tipo' => 'acerto',
                    'msg'=>'Baixa salva com sucesso!'
        
                    ];
                    
                    return  $response; 
            
        }

         $response = ['tipo' => 'erro',
        'msg'=>"Erro ao encontrar dados da Nota. Entre em contato com o TI"
        ];                    
        return $response;


    }

    public static function cancelarInforme($data){
        mysql_query('BEGIN');
        $responseNota = Notas::getNotaById($data['notaId']);       
        if($responseNota){
            // A baixa da nota é uma devolução o tipo de movimentação é diferente do da nota original
            $tipoMovimentacao = $responseNota['tipo'] == 0 ? 1 : 0;
            // tabela plano de contas 
            
            $natureza = $tipoMovimentacao == 0 ? 0 : 0;
            
            $sql = "
            update
                nota_informe_perda
                set
                canceled_by = '{$_SESSION['id_user']}',
                canceled_at = now()
                where
                id = {$data['informePerdaId']}
                  
            ";  
         
            $rs = mysql_query($sql);

            if(!$rs) {
                mysql_query('ROLLBACK');
                $response = ['tipo' => 'erro',
                      'msg'=>"Erro ao cancelar Informe de Perda. Entre em contato com o TI"
                    ];                    
                return $response;
            }

            $sql = "update
                parcelas 
                set                
                compensada = 'NAO',
                valor_compensada = '0',
                obs = '',
                perda = 'N',
                status = 'Pendente',
                DATA_PAGAMENTO = NULL              
                where
                id = {$data['parcelaId']}
                ";
                $rs = mysql_query($sql);

                if(!$rs) {
                    mysql_query('ROLLBACK');
                    $response = ['tipo' => 'erro',
                          'msg'=>"Erro ao atualizar Parcela."
                        ];                    
                    return $sql;
                }

                // Atualizando nota da parcela que tá compensando a pivo. 
                $statusNota = 'Processada';
                $check = Parcela::checkIfNotaHasParcelasPendentes($data['notaId']);
                if($check) {
                    $statusNota = 'Pendente';
                }
                
                    $sql = "
                    UPDATE 
                    notas 
                    SET 
                    status = '{$statusNota}'
                    WHERE idNotas = '{$data['notaId']}'
                    ";
                    $rs = mysql_query($sql);
                    if(!$rs) {
                        $response = ['tipo' => 'erro',
                          'msg'=>"Erro ao atualizar nota."
                        ]; 
                        return $response; 
                    }

                mysql_query('COMMIT');

                $response = ['tipo' => 'acerto',
                    'msg'=>'Informe de perda cancelado com sucesso!'
        
                    ];
                    
                    return  $response; 
            
        }

         $response = ['tipo' => 'erro',
        'msg'=>"Erro ao encontrar dados da Nota. Entre em contato com o TI"
        ];                    
        return $response;


    }

}