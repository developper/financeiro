<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class BorderoTipo extends AbstractModel implements DAOInterface
{
    public static function getAll()
    {
        return parent::get(self::getSQL());
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? 'WHERE 1=1 AND id = ' . $id : null;
        return <<<SQL
SELECT 
  *
FROM
  bordero_tipo
{$id}
ORDER BY ordem
SQL;
    }
    public static function getByFilter($data = null)
    {
        $condicaoNotIn = '';
        if(isset($data['notIn'])){
            $condicaoNotIn = "where id not in(".implode(',', $data['notIn']).")";
        }
$sql= <<<SQL
SELECT 
  *
FROM
  bordero_tipo
{$condicaoNotIn}
ORDER BY ordem
SQL;
return parent::get($sql);
    }
}
