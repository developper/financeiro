<?php
namespace App\Models\Financeiro;

use App\Core\Log,
    \App\Models\AbstractModel,
    \App\Models\DAOInterface;
use App\Helpers\StringHelper;

class IPCF extends AbstractModel implements DAOInterface
{

    public static function getAll()
    {
        return parent::get(self::getSQL());
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getByCodigo($match, $level)
    {
        $sql = "SELECT * FROM plano_contas WHERE COD_N{$level} = '{$match}' ORDER BY COD_N2, COD_N3, COD_N4";
        $rs = mysql_query($sql);
        $fetch = mysql_fetch_array($rs, MYSQL_ASSOC);

        return $fetch;
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? 'AND ID = "' . $id . '"' : "";
        return "SELECT * FROM plano_contas WHERE 1=1 {$id} ORDER BY COD_N2, COD_N3, COD_N4";
    }

    public static function update($data)
    {
        //Auxiliar para verificar se o registro a ser cadastrado
        //já existe no banco

        $occurs = 0;

        foreach ($data as &$ipcf)
            $ipcf = array_map(function($dados){
                if(isset($dados['descricao']))
                    $dados['descricao'] = strtoupper ($dados['descricao']);
                return $dados;
            }, $ipcf);

        switch($data['extras']['nivel']){
            case 4:
                /*$data['ipcf']['nivel4']['codigo'] =
                    isset($data['ipcfAntigo']['nivel4']['codigo']) &&
                    $data['ipcfAntigo']['nivel4']['codigo'] !== $data['ipcf']['nivel4']['codigo'] ?
                        $data['ipcf']['nivel4']['codigo'] :
                        $data['ipcfAntigo']['nivel4']['codigo'];
                $data['ipcf']['nivel4']['descricao'] =
                    isset($data['ipcfAntigo']['nivel4']['descricao']) &&
                    $data['ipcfAntigo']['nivel4']['descricao'] !== $data['ipcf']['nivel4']['descricao'] ?
                        $data['ipcf']['nivel4']['descricao'] :
                        $data['ipcfAntigo']['nivel4']['descricao'];
                $data['ipcf']['nivel4']['provisiona'] =
                    isset($data['ipcfAntigo']['nivel4']['provisiona']) &&
                    $data['ipcfAntigo']['nivel4']['provisiona'] !== $data['ipcf']['nivel4']['provisiona'] ?
                        $data['ipcf']['nivel4']['provisiona'] :
                        $data['ipcfAntigo']['nivel4']['provisiona'];
                $data['ipcf']['ipcc']['codigo'] =
                    isset($data['ipcfAntigo']['ipcc']['codigo']) &&
                    $data['ipcfAntigo']['ipcc']['codigo'] !== $data['ipcf']['ipcc']['codigo'] ?
                        $data['ipcf']['ipcc']['codigo'] :
                        $data['ipcfAntigo']['ipcc']['codigo'];
                $data['ipcf']['ipcc']['descricao'] =
                    isset($data['ipcfAntigo']['ipcc']['descricao']) && $data['ipcfAntigo']['ipcc']['descricao'] !== $data['ipcf']['ipcc']['descricao'] ?
                        $data['ipcf']['ipcc']['descricao'] :
                        $data['ipcfAntigo']['ipcc']['descricao'];*/

                //Verifica a existencia de mesmo nome e codigo
                $occursCodigo = false;
                $set = '';
                if(
                    ($data['ipcfAntigo']['nivel4']['codigo'] !== $data['ipcf']['nivel4']['codigo']) ||
                    ($data['ipcfAntigo']['nivel4']['descricao'] !== $data['ipcf']['nivel4']['descricao'])
                ) {
                    $occursCodigo = (self::validateEntirePlan($data['ipcf']) == '0' && self::validateEntireCod($data['ipcf']) == '0');
                }

                if(!$occursCodigo) {
                    $set  .= "SET N4 = '{$data['ipcf']['nivel4']['descricao']}', COD_N4 = '{$data['ipcf']['nivel4']['codigo']}',
          							IPCC = '{$data['ipcf']['ipcc']['descricao']}', COD_IPCC = '{$data['ipcf']['ipcc']['codigo']}',
          							PROVISIONA = '{$data['ipcf']['nivel4']['provisiona']}'";
                    $where  = "WHERE COD_N4 = '{$data['ipcfAntigo']['nivel4']['codigo']}'";
                    //Atualiza as tarifas e impostos caso a conta seja pertinente
                    self::updateTarifasImpostos($data['ipcf']['nivel4']['codigo'], $data['ipcfAntigo']['nivel4']['codigo']);
                }else {
                    $occurs++;
                }

                break;
            case 3:
                $data['ipcf']['nivel3']['codigo'] =
                    isset($data['ipcfAntigo']['nivel3']['codigo']) && $data['ipcfAntigo']['nivel3']['codigo'] !== $data['ipcf']['nivel3']['codigo'] ?
                        $data['ipcf']['nivel3']['codigo'] :
                        $data['ipcfAntigo']['nivel3']['codigo'];
                $data['ipcf']['nivel3']['descricao'] =
                    isset($data['ipcfAntigo']['nivel3']['descricao']) && $data['ipcfAntigo']['nivel3']['descricao'] !== $data['ipcf']['nivel3']['descricao'] ?
                        $data['ipcf']['nivel3']['descricao'] :
                        $data['ipcfAntigo']['nivel3']['descricao'];

                //Verifica a existencia de mesmo nome e codigo
                $occursCodigo =
                    ($data['ipcfAntigo']['nivel3']['codigo'] !== $data['ipcf']['nivel3']['codigo']) ?
                        self::validate('COD_N3', $data['ipcf']['nivel3']['codigo']) :
                        '0';

                if($occursCodigo === '0') {
                    $set    = "SET N3 = '{$data['ipcf']['nivel3']['descricao']}', COD_N3 = '{$data['ipcf']['nivel3']['codigo']}'";
                    $where  = "WHERE COD_N3 = '{$data['ipcfAntigo']['nivel3']['codigo']}'";
                }else {
                    $occurs++;
                }
                break;
            case 2:
                $data['ipcf']['nivel2']['codigo'] =
                    isset($data['ipcfAntigo']['nivel2']['codigo']) && $data['ipcfAntigo']['nivel2']['codigo'] !== $data['ipcf']['nivel2']['codigo'] ?
                        $data['ipcf']['nivel2']['codigo'] :
                        $data['ipcfAntigo']['nivel2']['codigo'];
                $data['ipcf']['nivel2']['descricao'] =
                    isset($data['ipcfAntigo']['nivel2']['descricao']) && $data['ipcfAntigo']['nivel2']['descricao'] !== $data['ipcf']['nivel2']['descricao'] ?
                        $data['ipcf']['nivel2']['descricao'] :
                        $data['ipcfAntigo']['nivel2']['descricao'];

                //Verifica a existencia de mesmo nome e codigo
                $occursCodigo =
                    ($data['ipcfAntigo']['nivel2']['codigo'] !== $data['ipcf']['nivel2']['codigo']) ?
                        self::validate('COD_N2', $data['ipcf']['nivel2']['codigo']) :
                        '0';

                if($occursCodigo === '0') {
                    $set    = "SET N2 = '{$data['ipcf']['nivel2']['descricao']}', COD_N2 = '{$data['ipcf']['nivel2']['codigo']}'";
                    $where  = "WHERE COD_N2 = '{$data['ipcfAntigo']['nivel2']['codigo']}'";
                }else {
                    $occurs++;
                }
                break;
        }

        //if($occurs < 1){
            $iss = StringHelper::moneyStringToFloat($data['retencoes']['iss']);
            $ir = StringHelper::moneyStringToFloat($data['retencoes']['ir']);
            $ipi = StringHelper::moneyStringToFloat($data['retencoes']['ipi']);
            $icms = StringHelper::moneyStringToFloat($data['retencoes']['icms']);
            $pis = StringHelper::moneyStringToFloat($data['retencoes']['pis']);
            $cofins = StringHelper::moneyStringToFloat($data['retencoes']['cofins']);
            $csll = StringHelper::moneyStringToFloat($data['retencoes']['csll']);
            $inss = StringHelper::moneyStringToFloat($data['retencoes']['inss']);
            $pcc = StringHelper::moneyStringToFloat($data['retencoes']['pcc']);
            $set = " IPCC = '{$data['ipcf']['ipcc']['descricao']}', 
                     COD_IPCC = '{$data['ipcf']['ipcc']['codigo']}',
                     PROVISIONA = '{$data['ipcf']['nivel4']['provisiona']}',
                     ISS = '{$iss}', ICMS = '{$icms}', 
                     IPI = '{$ipi}', PIS = '{$pis}', 
                     COFINS = '{$cofins}', CSLL = '{$csll}', 
                     IR = '{$ir}', PCC = '{$pcc}', 
                     INSS = '{$inss}', POSSUI_RETENCOES = '{$data['ipcf']['nivel4']['possui_retencoes']}'";
            $sql = "UPDATE plano_contas SET {$set} WHERE COD_N4 = '{$data['ipcfAntigo']['nivel4']['codigo']}'";
            $result = mysql_query($sql);

            Log::registerUserLog($sql);

            return ($result ? mysql_affected_rows() : mysql_error());
        /*}else{
            return "Já existe um registro com esse código/descrição!";
        }*/
    }

    public static function save($data, $retencoes)
    {
        $data = array_map(function($dados){
            if(isset($dados['descricao']))
                $dados['descricao'] = strtoupper ($dados['descricao']);
            return $dados;
        }, $data);

        $retencoes = array_map(function($dados){
            if(isset($dados))
                $dados = StringHelper::moneyStringToFloat($dados);
            return $dados;
        }, $retencoes);

        if(self::validateEntirePlan($data) == '0' && self::validateEntireCod($data) == '0') {
            mysql_query('BEGIN');
            $sql = <<<SQL
INSERT INTO plano_contas 
 (
  N1, COD_N2, N2, COD_N3, N3, COD_N4, N4, COD_IPCC, IPCC, PROVISIONA, 
  ISS, ICMS, IPI, PIS, COFINS, CSLL, IR, PCC, INSS, POSSUI_RETENCOES
 ) VALUES (
  '{$data['nivel1']['codigo']}', trim('{$data['nivel2']['codigo']}'), '{$data['nivel2']['descricao']}',
  trim('{$data['nivel3']['codigo']}'), '{$data['nivel3']['descricao']}',
  trim('{$data['nivel4']['codigo']}'), '{$data['nivel4']['descricao']}',
  '{$data['ipcc']['codigo']}', '{$data['ipcc']['descricao']}', 
  '{$data['nivel4']['provisiona']}', '{$retencoes['iss']}', '{$retencoes['icms']}',
  '{$retencoes['ipi']}', '{$retencoes['pis']}', '{$retencoes['cofins']}', '{$retencoes['csll']}', 
  '{$retencoes['ir']}', '{$retencoes['pcc']}', '{$retencoes['inss']}', 
  '{$data['nivel4']['possui_retencoes']}'
 )
SQL;

            $result = mysql_query($sql);
            if ($result) {
                Log::registerUserLog($sql);
                return mysql_query("COMMIT");
            } else {
                mysql_query("ROLLBACK");
                return mysql_error();
            }
        }else{
            return "Já existe um registro com esse código/descrição!";
        }
    }

    public static function delete($data)
    {
        $response = [];
        $status = self::checkIntegrity($data['level'], $data['match']);
        if(!$status['movs'] && !$status['childs']){
            $sql = "DELETE FROM plano_contas WHERE COD_N{$data['level']} = '{$data['match']}'";
            Log::registerUserLog($sql);
            return mysql_query($sql);
        }else{
            $response['msg']['movs'] = $status['movs'] ? 'Essa conta possui movimentações!' : '';
            $response['msg']['childs'] = $status['childs'] ? 'Essa conta possui subcontas. Para excluí-la será necessário remover todas as subcontas.' : '';
            return $response;
        }
    }

    public static function getJSON($data)
    {
        $data = array_map(function($upper){
            return strtoupper($upper);
        }, $data);
        switch($data['nivel']){
            case 1:
                $columns = "IF(N1 = 'S', 'SAÍDA', 'ENTRADA') as label, IF(N1 = 'S', '2', '1') as value";
                if(strpos($data['term'], 'SAI') === 0){
                    $data['term'] = "S";
                }elseif(strpos($data['term'], 'ENT') === 0){
                    $data['term'] = "E";
                }
                break;
            case 2:
                $columns = "CONCAT(COD_N2, ' - ', UPPER(N2)) as label, COD_N2 as value, UPPER(N2) as labelSelected";
                break;
            case 3:
                $columns = "CONCAT(COD_N3, ' - ', UPPER(N3)) as label, COD_N3 as value, UPPER(N3) as labelSelected";
                break;
            case 4:
                $columns = "CONCAT(COD_N4, ' - ', UPPER(N4)) as label, COD_N4 as value, UPPER(N4) as labelSelected";
                break;
        }

        $sql  = " SELECT {$columns}
              FROM plano_contas
              WHERE N{$data['nivel']} LIKE '%{$data['term']}%'
              GROUP BY N{$data['nivel']}";
        $sql .= $data['nivel'] > 1 ? " ORDER BY COD_N{$data['nivel']}" : "";
        return json_encode(parent::get($sql));
    }

    private static function updateTarifasImpostos($codigoN4, $codigoN4Antigo)
    {
        $sql = "UPDATE impostos_tarifas SET ipcf = {$codigoN4} WHERE ipcf = {$codigoN4Antigo}";
        return mysql_query($sql);
    }

    private static function checkIntegrity($level, $match)
    {
        $status = [];
        $status['movs'] = self::getMovs($level, $match);
        $status['childs'] = $level < 4 ? self::getChilds($level, $match) : false;
        return $status;
    }

    private static function getMovs($level, $match)
    {
        switch($level){
            case 2:
                $cond = "IPCF2";
                $field = "N2";
                break;
            case 3:
                $cond = "IPCG3";
                $field = "N3";
                break;
            case 4:
                $cond = "IPCG4";
                $field = "ID";
                break;
        }
        $result = self::getByCodigo($match, $level);
        $sql    = "SELECT COUNT(ID) as rateios FROM rateio WHERE {$cond} = '{$result[$field]}'";
        $result = mysql_query($sql);
        $fetch  = mysql_fetch_array($result, MYSQL_ASSOC);
        return $fetch['rateios'] > 0 ? true : false;
    }

    private static function getChilds($level, $match)
    {
        switch($level){
            case 2:
                $cond = "COD_N2";
                break;
            case 3:
                $cond = "COD_N3";
                break;
        }
        $sql    = "SELECT COUNT(ID) as childs FROM plano_contas WHERE {$cond} = '{$match}'";
        $result = mysql_query($sql);
        $fetch  = mysql_fetch_array($result, MYSQL_ASSOC);
        return $fetch['childs'] > 0 ? true : false;
    }

    private static function validateEntirePlan($data)
    {
        $sql    = "SELECT 
                    COUNT(ID) as occurs 
                   FROM 
                    plano_contas 
                   WHERE 
                    N1 = '{$data['nivel1']['codigo']}' 
                    AND N2 = '{$data['nivel2']['descricao']}'
                    AND COD_N2 = '{$data['nivel2']['codigo']}'
                    AND N3 = '{$data['nivel3']['descricao']}'
                    AND COD_N3 = '{$data['nivel3']['codigo']}'
                    AND N4 = '{$data['nivel4']['descricao']}'
                    AND COD_N4 = '{$data['nivel4']['codigo']}'";
        $result = mysql_query($sql);
        $fetch  = mysql_fetch_array($result);
        return $fetch['occurs'];
    }

    private static function validateEntireCod($data)
    {
        $sql    = "SELECT 
                    COUNT(ID) as occurs 
                   FROM 
                    plano_contas 
                   WHERE 
                    N1 = '{$data['nivel1']['codigo']}' 
                    AND COD_N2 = '{$data['nivel2']['codigo']}'
                    AND COD_N3 = '{$data['nivel3']['codigo']}'
                    AND COD_N4 = '{$data['nivel4']['codigo']}'";
        $result = mysql_query($sql);
        $fetch  = mysql_fetch_array($result);
        return $fetch['occurs'];
    }

    private static function validate($field, $match)
    {
        $sql    = "SELECT COUNT(ID) as occurs FROM plano_contas WHERE {$field} = '{$match}'";
        $result = mysql_query($sql);
        $fetch  = mysql_fetch_array($result);
        return $fetch['occurs'];
    }

    private static function validateCodAndDescricao($level, $cod, $name)
    {
        $sql    = "SELECT COUNT(ID) as occurs FROM plano_contas WHERE COD_N{$level} = '{$cod}' AND N{$level} = '{$name}'";
        $result = mysql_query($sql);
        $fetch  = mysql_fetch_array($result);
        return $fetch['occurs'];
    }

    public static function getAllNatureza($tipo = '')
    {
        $tipoWhere = $tipo != '' ? " AND N1 = '{$tipo}' " : "";
        $sql = <<<SQL
SELECT ID, N1, N4, COD_N4 FROM plano_contas WHERE  1 = 1 {$tipoWhere} ORDER BY N1, N4;
SQL;
        $data = parent::get($sql, 'assoc');

        $response = [];
        foreach ($data as $row) {
            $grupo = $row['N1'] == 'S' ? 'SAÍDA' : 'ENTRADA';
            $response[$grupo][] = $row;
        }

        return $response;
    }
}