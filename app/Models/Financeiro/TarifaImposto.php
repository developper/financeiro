<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
	\App\Models\DAOInterface;

class TarifaImposto extends AbstractModel implements DAOInterface
{

    const CENTRO_RESULTADO_TARIFA_IMPOSTO = '102';

	public static function getAll()
	{
		return parent::get(self::getSQL());
	}

	public static function getById ($id)
	{
		return parent::get (self::getSQL ($id));
	}

	public static function getCentroCustoDebitoByContaId($lancamento)
	{
        /*
            Débito - Banco (onde o dinheiro foi "Depositado")
            Crédito - Empréstimos (Passivo)

            Quando for IOF vai ser:
            Débito - IOF (Despesa Tributária)
            Crédito - Banco (Onde o IOF foi creditado)

            Quando for Tarifa vai ser:
            Débito - Tarifas Bancárias (Despesa)
            Crédito - Banco (Onde a Tarifa foi creditada)
         */
        $id = str_replace('i', '', $lancamento['idNotas']);
//         $centroResultado = self::CENTRO_RESULTADO_TARIFA_IMPOSTO;

//         $sql = <<<SQL
// SELECT pcc.CODIGO_DOMINIO AS codigo_debito,
//        {$centroResultado} AS centro_custo_debito,
//        e.centro_custo_dominio AS codigo_empresa
// FROM plano_contas_contabil pcc
// INNER JOIN contas_bancarias cb ON cb.IPCC = pcc.ID
// INNER JOIN empresas e ON e.id = cb.UR
// WHERE cb.ID = '{$lancamento['conta_tarifa']}'
// SQL;

	  /*  if($lancamento['tipo'] == 'IMPOSTO' || $lancamento['tipo'] == 'TARIFA') {
            $sql = <<<SQL
SELECT
 pcc.CODIGO_DOMINIO AS codigo_debito,
{$centroResultado} AS centro_custo_debito,
e.centro_custo_dominio AS codigo_empresa
FROM plano_contas_contabil pcc
INNER JOIN plano_contas pc on pcc.COD_N5 = pc.COD_IPCC
INNER JOIN impostos_tarifas it on it.ipcf = pc.COD_N4
INNER JOIN impostos_tarifas_lancto itl on it.id = itl.item
INNER JOIN contas_bancarias cb ON cb.ID = itl.conta
INNER JOIN empresas e ON e.id = cb.UR
WHERE itl.id = '{$id}'
SQL;
       } */
       $sql = <<<SQL
SELECT
IF( it.debito_credito = 'CREDITO', 
(SELECT pcc2.CODIGO_DOMINIO AS codigo_debito       
 FROM plano_contas_contabil pcc2
INNER JOIN contas_bancarias cb2 ON cb2.IPCC = pcc2.ID 
 WHERE cb2.ID = '{$lancamento['conta_tarifa']}'),
pcc.CODIGO_DOMINIO) AS codigo_debito,
'102' AS centro_custo_debito,
e.centro_custo_dominio AS codigo_empresa
FROM plano_contas_contabil pcc
INNER JOIN plano_contas pc on pcc.COD_N5 = pc.COD_IPCC
INNER JOIN impostos_tarifas it on it.ipcf = pc.COD_N4
INNER JOIN impostos_tarifas_lancto itl on it.id = itl.item
INNER JOIN contas_bancarias cb ON cb.ID = itl.conta
INNER JOIN empresas e ON e.id = cb.UR
WHERE itl.id = '{$id}'
SQL;

        return parent::get ($sql);
	}

    public static function getCentroCustoCreditoByContaId($lancamento)
    {
        /*
            Débito - Banco (onde o dinheiro foi "Depositado")
            Crédito - Empréstimos (Passivo)

            Quando for IOF vai ser:
            Débito - IOF (Despesa Tributária)
            Crédito - Banco (Onde o IOF foi creditado)

            Quando for Tarifa vai ser:
            Débito - Tarifas Bancárias (Despesa)
            Crédito - Banco (Onde a Tarifa foi creditada)
         */
        $id = str_replace('i', '', $lancamento['idNotas']);
//         $centroResultado = self::CENTRO_RESULTADO_TARIFA_IMPOSTO;

//         $sql = <<<SQL
// SELECT pcc.CODIGO_DOMINIO AS codigo_credito,
//        {$centroResultado} AS centro_custo_credito,
//        e.centro_custo_dominio AS codigo_empresa
// FROM plano_contas_contabil pcc
// INNER JOIN plano_contas pc on pcc.COD_N5 = pc.COD_IPCC
// INNER JOIN impostos_tarifas it on it.ipcf = pc.COD_N4
// INNER JOIN impostos_tarifas_lancto itl on it.id = itl.item
// INNER JOIN contas_bancarias cb ON cb.ID = itl.conta
// INNER JOIN empresas e ON e.id = cb.UR
// WHERE itl.id = '{$id}'
// SQL;

//         if($lancamento['tipo'] == 'IMPOSTO' || $lancamento['tipo'] == 'TARIFA') {
//             $sql = <<<SQL
// SELECT pcc.CODIGO_DOMINIO AS codigo_credito,
//        {$centroResultado} AS centro_custo_credito,
//        e.centro_custo_dominio AS codigo_empresa
// FROM plano_contas_contabil pcc
// INNER JOIN contas_bancarias cb ON cb.ID = pcc.ID
// INNER JOIN empresas e ON e.id = cb.UR
// WHERE cb.ID = '{$lancamento['conta_tarifa']}'
// SQL;
//         }
$sql = <<<SQL
SELECT
IF( it.debito_credito = 'CREDITO', 
pcc.CODIGO_DOMINIO,
(SELECT pcc2.CODIGO_DOMINIO      
 FROM plano_contas_contabil pcc2
INNER JOIN contas_bancarias cb2 ON cb2.IPCC = pcc2.ID 
 WHERE cb2.ID = '{$lancamento['conta_tarifa']}')) AS codigo_credito,
'102' AS centro_custo_credito,
e.centro_custo_dominio AS codigo_empresa
FROM plano_contas_contabil pcc
INNER JOIN plano_contas pc on pcc.COD_N5 = pc.COD_IPCC
INNER JOIN impostos_tarifas it on it.ipcf = pc.COD_N4
INNER JOIN impostos_tarifas_lancto itl on it.id = itl.item
INNER JOIN contas_bancarias cb ON cb.ID = itl.conta
INNER JOIN empresas e ON e.id = cb.UR
WHERE itl.id = '{$id}'
SQL;

        return parent::get ($sql);
    }

	public static function getSQL($id = null)
	{
		return "";
	}

}