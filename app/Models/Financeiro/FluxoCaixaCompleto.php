<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class FluxoCaixaCompleto extends AbstractModel
{
    public static function getSaldoByUR($data)
    {
        $condFiliais = '';
		$condFiliaisNota = '';
        if(isset($data['filiais']) && ! empty($data['filiais'])){
            $fiiais = implode(', ', $data['filiais']);
            $condFiliais = " AND c.UR IN ({$fiiais})";
			$condFiliaisNota = " AND n.empresa IN ({$fiiais})";
            
		}
		$sqlSaldoPendente = '';

		if($data['status'] != 'processado'){
			$sqlSaldoPendente = "UNION SELECT	
			p.id,
			p.valor as VALOR_PAGO,
			DATE_FORMAT( p.vencimento_real, '%Y-%m-%d' ) AS data_pg,
			if(n.tipo = 0, 'Entrada', 'Saida') AS tipo_nota,
			'nota' as tipo_item,
			'Saldo Pendente' AS tipo_conta,
			0 AS tipo_conta_id,
			0 as conta_bancaria_id
		FROM
			parcelas AS p	
			INNER JOIN notas AS n ON ( p.idNota = n.idNotas )
			inner join empresas as e on (n.empresa = e.id)
			
		WHERE
			
			n.STATUS != 'Cancelada' 
			and p.STATUS = 'Pendente'
			AND vencimento_real < '{$data['inicio']}' 
			{$condFiliaisNota}";

		}
		
        $sql = <<<SQL
SELECT	
transferencia_bancaria.ID,
	`transferencia_bancaria`.`VALOR` AS `VALOR_PAGO`,	
	`transferencia_bancaria`.`DATA_TRANSFERENCIA` AS `data_pg`,
	'Saida' AS `tipo_nota`,	
	'transferencia' AS `tipo_item`,
	`tcb`.`nome` AS `tipo_conta`,
	`tcb`.`id` AS `tipo_conta_id`,
	c.ID as conta_bancaria_id

	 
FROM
(
	(
	( `transferencia_bancaria` JOIN `contas_bancarias` `c` ON ( ( `c`.`ID` = `transferencia_bancaria`.`CONTA_ORIGEM` ) ) )
	JOIN `origemfundos` `o` ON ( ( `c`.`ORIGEM_FUNDOS_ID` = `o`.`id` ) ) 
	)
	JOIN `tipo_conta_bancaria` `tcb` ON ( ( `c`.`TIPO_CONTA_BANCARIA_ID` = `tcb`.`id` ) ) 
	)
	
	
WHERE	
	transferencia_bancaria.CANCELADA_EM is NULL
	AND transferencia_bancaria.DATA_TRANSFERENCIA < "{$data['inicio']}"
    {$condFiliais}
	
	UNION
SELECT
	`transferencia_bancaria`.`ID` AS `ID`,	
	`transferencia_bancaria`.`VALOR` AS `VALOR_PAGO`,	
	`transferencia_bancaria`.`DATA_TRANSFERENCIA` AS `data_pg`,
	'Entrada' AS `tipo_nota`,	
	'Transferencia' AS `tipo_item`,
	`tcb`.`nome` AS `tipo_conta`,
	`tcb`.`id` AS `tipo_conta_id`,
	c.ID as conta_bancaria_id
	
FROM
(
	(
	( `transferencia_bancaria` JOIN `contas_bancarias` `c` ON ( ( `c`.`ID` = `transferencia_bancaria`.`CONTA_DESTINO` ) ) )
	JOIN `origemfundos` `o` ON ( ( `c`.`ORIGEM_FUNDOS_ID` = `o`.`id` ) ) 
	)
	JOIN `tipo_conta_bancaria` `tcb` ON ( ( `c`.`TIPO_CONTA_BANCARIA_ID` = `tcb`.`id` ) ) 
	)
	 
WHERE
	transferencia_bancaria.CANCELADA_EM is NULL
	AND transferencia_bancaria.DATA_TRANSFERENCIA < "{$data['inicio']}"
    {$condFiliais}
	
	UNION
SELECT
	`itl`.`id` AS `id`,	
	`itl`.`valor` AS `valor`,	
	date_format( `itl`.`data`, '%Y-%m-%d' ) AS `data_pg`,
IF	( ( `imptar`.`debito_credito` = 'DEBITO' ), 'Saida', 'Entrada' ) AS `tipo_nota`,	
	'Tarifa' AS `tipo_item`,
	`tcb`.`nome` AS `tipo_conta`,
	`tcb`.`id` AS `tipo_conta_id`,
	c.ID as conta_bancaria_id
 
FROM
(
	(
	(
	( `impostos_tarifas` `imptar` JOIN `impostos_tarifas_lancto` `itl` ON ( ( `imptar`.`id` = `itl`.`item` ) ) )
	JOIN `contas_bancarias` `c` ON ( ( `itl`.`conta` = `c`.`ID` ) ) 
	)
	JOIN `origemfundos` `o` ON ( ( `c`.`ORIGEM_FUNDOS_ID` = `o`.`id` ) ) 
	)
	JOIN `tipo_conta_bancaria` `tcb` ON ( ( `c`.`TIPO_CONTA_BANCARIA_ID` = `tcb`.`id` ) ) 
	)
	
	where
	itl.data < "{$data['inicio']}"
    {$condFiliais}
	
	
	UNION
SELECT
	`hep`.`id` AS `id`,	
	`hep`.`valor` AS `valor`,
	date_format( `hep`.`data_processada`,  '%Y-%m-%d' ) AS `data_pg`,
	'Saida' AS `tipo_nota`,
	'Estorno' AS `tipo_item`,
	`tcb`.`nome` AS `tipo_conta`,
	`tcb`.`id` AS `tipo_conta_id`,
	c.ID as conta_bancaria_id	
FROM
	
(
	(
	(
	(
	( `estorno_parcela` `hep` JOIN `parcelas` `p` ON ( ( `p`.`id` = `hep`.`parcela_id` ) ) )
	JOIN `notas` `n` ON ( ( `n`.`idNotas` = `p`.`idNota` ) ) 
	)
	
	JOIN `contas_bancarias` `c` ON ( ( `hep`.`origem` = `c`.`ID` ) ) 
	)
	JOIN `origemfundos` `o` ON ( ( `c`.`ORIGEM_FUNDOS_ID` = `o`.`id` ) ) 
	) 
	JOIN `tipo_conta_bancaria` `tcb` ON ( ( `c`.`TIPO_CONTA_BANCARIA_ID` = `tcb`.`id` ) ) 
	)
	where
	hep.data_processada < "{$data['inicio']}"
    {$condFiliais}
	
	UNION
SELECT
	`nb`.`id` AS `id`,	
	`nb`.`valor` AS `valor`,	
	date_format( `nb`.`data_pagamento`,  '%Y-%m-%d' ) AS `data_pg`,
IF( ( `nb`.`tipo_movimentacao` = 0 ), 'Entrada', 'Saida' ) AS `tipo_nota`,	
	'Devolucao' AS `tipo_item`,
	`tcb`.`nome` AS `tipo_conta`,
	`tcb`.`id` AS `tipo_conta_id`,
	c.ID as conta_bancaria_id
	 
FROM
(
	(
	( `nota_baixas` `nb` JOIN `contas_bancarias` `c` ON ( ( `c`.`ID` = `nb`.`conta_bancaria_id` ) ) )
	JOIN `origemfundos` `o` ON ( ( `c`.`ORIGEM_FUNDOS_ID` = `o`.`id` ) ) 
	) 
	JOIN `tipo_conta_bancaria` `tcb` ON ( ( `c`.`TIPO_CONTA_BANCARIA_ID` = `tcb`.`id` ) ) 
	)
WHERE
nb.data_pagamento < "{$data['inicio']}"
{$condFiliais}
UNION	
SELECT
	`p`.`id` AS `id`,
	`p`.`VALOR_PAGO` AS `VALOR_PAGO`,	
	date_format( `p`.`DATA_PAGAMENTO`, '%Y-%m-%d' ) AS `data_pg`,
	IF	( ( `n`.`tipo` = 0 ), 'Entrada', 'Saida' ) AS `tipo_nota`,	
	'Parcela' AS `tipo_item`,
	`tcb`.`nome` AS `tipo_conta`,
	`tcb`.`id` AS `tipo_conta_id`,
	c.ID as conta_bancaria_id
	 
FROM
(
	(
	(
	( `parcelas` `p` JOIN `contas_bancarias` `c` ON ( ( `p`.`origem` = `c`.`ID` ) ) )
	JOIN `notas` `n` ON ( ( `p`.`idNota` = `n`.`idNotas` ) ) 
	)
	
	JOIN `origemfundos` `o` ON ( ( `c`.`ORIGEM_FUNDOS_ID` = `o`.`id` ) ) 
	)
	JOIN `tipo_conta_bancaria` `tcb` ON ( ( `c`.`TIPO_CONTA_BANCARIA_ID` = `tcb`.`id` ) ) 
	)
WHERE
	(
	( `p`.`status` = 'Processada' ) 
	AND ( `n`.`status` <> 'Cancelada' ) 
	AND ( `p`.`origem` > 1 ) 
	AND ( isnull( `p`.`nota_id_fatura_aglutinacao` ) OR ( `p`.`nota_id_fatura_aglutinacao` = 0 ) ) 
	AND p.DATA_PAGAMENTO < "{$data['inicio']}"
    {$condFiliais}
	
	) 
	UNION
SELECT
	`c`.`ID` AS `id`,	
	if (	`c`.`SALDO_INICIAL` >= 0,`c`.`SALDO_INICIAL`,`SALDO_INICIAL` * -1) AS `valor`,	
	date_format( `c`.`DATA_SALDO_INICIAL`, '%Y-%m-%d' ) AS `data_pg`,
	if (	`c`.`SALDO_INICIAL` >= 0,  'Entrada', 'Saida') AS `tipo_nota`,
	'Saldo Inicail' AS `tipo_item`,
	`tcb`.`nome` AS `tipo_conta`,
	`tcb`.`id` AS `tipo_conta_id`,
	c.ID as conta_bancaria_id
	 
FROM
	(
	( `contas_bancarias` `c` JOIN `origemfundos` `o` ON ( ( `c`.`ORIGEM_FUNDOS_ID` = `o`.`id` ) ) )
	JOIN `tipo_conta_bancaria` `tcb` ON ( ( `c`.`TIPO_CONTA_BANCARIA_ID` = `tcb`.`id` ) ) 
	)
    where
    1=1
    {$condFiliais}	
	$sqlSaldoPendente
SQL;

        return parent::get($sql, 'Assoc');
    }


   

	public static function getFluxoDeCaixaCompleto($data)
    {      
        

		$sqlPendente = self::sqlPendente($data);
		$sqlProcessada = self::sqlProcessada($data);

		if($data['status'] == 'todos'){
			$sql = $sqlProcessada.' Union '.$sqlPendente.' order by vencimento_real ASC'; 

		}elseif($data['status'] == 'projetado'){
			$sql = $sqlPendente.' order by vencimento_real ASC'; 
		}else{
			$sql = $sqlProcessada.' order by vencimento_real ASC'; 
		} 
		
return parent::get($sql, 'Assoc');

    }

	public static function sqlPendente($data)
	{
		$condFiliais = '';
        if(isset($data['filiais']) && ! empty($data['filiais'])){
            $fiiais = implode(', ', $data['filiais']);
            $condFiliais = " AND n.empresa IN ({$fiiais})";
            
        }
		return $sql = <<<SQL
		SELECT	
        p.valor,
	p.idNota as tr,	
	n.codigo as cod_nota,
	if(n.tipo = 0, 'Entrada', 'Saida') AS tipo_nota,
	f.razaoSocial AS fornecedor,
	if(f.CPF, f.CPF, f.cnpj) as cpf_cnpj, 		
	Month(vencimento_real) as mes,
	YEAR(vencimento_real) as ano,	
	DATE_FORMAT( p.vencimento_real, '%Y-%m-%d' ) AS vencimento_br,
	p.vencimento_real,	
	e.nome as empresa,
	CONCAT(pc.COD_N4,' ',pc.N4) as natureza,
	tpf.sigla as tipo_documento,
	'Pendente' as status_item
FROM
	parcelas AS p	
	INNER JOIN notas AS n ON ( p.idNota = n.idNotas )
	INNER JOIN fornecedores AS f ON ( n.codFornecedor = f.idFornecedores ) 
	inner join empresas as e on (n.empresa = e.id)
	INNER join plano_contas as pc on (n.natureza_movimentacao = pc.ID)    
    inner join tipo_documento_financeiro as tpf on ( n.tipo_documento_financeiro_id = tpf.id)
	
WHERE
	
	n.STATUS != 'Cancelada' 
	and p.`STATUS` = 'Pendente'
	AND vencimento_real BETWEEN '{$data['inicio']}' and '{$data['fim']}'
    {$condFiliais}
SQL;

	}

	public static function sqlProcessada($data)
	{
		$condFiliais = '';
		$condFiliaisContaBancaria = '';
        if(isset($data['filiais']) && ! empty($data['filiais'])){
            $fiiais = implode(', ', $data['filiais']);
            $condFiliais = " AND n.empresa IN ({$fiiais})";
			$condFiliaisContaBancaria = " AND c.UR IN ({$fiiais})";
            
        }


		return $sql = <<<SQL
		SELECT	
        p.valor,
	p.idNota as tr,	
	n.codigo as cod_nota,
	if(n.tipo = 0, 'Entrada', 'Saida') AS tipo_nota,
	f.razaoSocial AS fornecedor,
	if(f.CPF, f.CPF, f.cnpj) as cpf_cnpj, 		
	Month(vencimento_real) as mes,
	YEAR(vencimento_real) as ano,	
	DATE_FORMAT( p.vencimento_real, '%Y-%m-%d' ) AS vencimento_br,
	p.vencimento_real,	
	e.nome as empresa,
	CONCAT(pc.COD_N4,' ',pc.N4) as natureza,
	tpf.sigla as tipo_documento,
	'Processada' as status_item
FROM
	parcelas AS p	
	INNER JOIN notas AS n ON ( p.idNota = n.idNotas )
	INNER JOIN fornecedores AS f ON ( n.codFornecedor = f.idFornecedores ) 
	inner join empresas as e on (n.empresa = e.id)
	INNER join plano_contas as pc on (n.natureza_movimentacao = pc.ID)    
    inner join tipo_documento_financeiro as tpf on ( n.tipo_documento_financeiro_id = tpf.id)
	
WHERE
	p.origem > 1
	and n.STATUS != 'Cancelada' 
	and p.`STATUS` = 'Processada'
	AND vencimento_real BETWEEN '{$data['inicio']}' and '{$data['fim']}'
    {$condFiliais}   
UNION
SELECT
	`itl`.`valor` AS `valor`,
	`itl`.`id` AS `tr`,	
	itl.documento as cod_conta,
	IF	( ( `imptar`.`debito_credito` = 'DEBITO' ), 'Saida', 'Entrada' ) AS `tipo_nota`,
	'' as fornecedor,
	'' as cpf_cnpj,
	Month(`itl`.`data`) as mes,
	YEAR(`itl`.`data`) as ano,		
	date_format( `itl`.`data`, '%Y-%m-%d' ) AS `vencimento_br`,
	`itl`.`data` as vencimento_real,
	e.nome as empresa,
	CONCAT(pc.COD_N4,' ',pc.N4) as natureza,
	imptar.item as tipo_documento,
	'Processada' as status_item
 
FROM
 	`impostos_tarifas` `imptar` JOIN `impostos_tarifas_lancto` `itl` ON ( `imptar`.`id` = `itl`.`item` ) 
	JOIN `contas_bancarias` `c` ON  ( `itl`.`conta` = `c`.`ID` ) 
	join  empresas as e on (c.UR = e.id)	
	INNER join plano_contas as pc on (imptar.ipcf = pc.COD_N4)  	
	where
	itl.data BETWEEN '{$data['inicio']}' and '{$data['fim']}'
    {$condFiliaisContaBancaria}

UNION
	SELECT
	`nb`.`valor` AS `valor`,
	`nb`.`id` AS `tr`,
	nb.numero_documento as cod_conta,	
	IF( ( `nb`.`tipo_movimentacao` = 0 ), 'Entrada', 'Saida' ) AS `tipo_nota`,
	f.razaoSocial AS fornecedor,
	if(f.CPF, f.CPF, f.cnpj) as cpf_cnpj, 
	Month(`nb`.`data_pagamento`) as mes,
	YEAR(`nb`.`data_pagamento`) as ano,			
	date_format( `nb`.`data_pagamento`,  '%Y-%m-%d' ) AS `vencimento_br`,
	`nb`.`data_pagamento` as vencimento_real,
	e.nome as empresa,
	CONCAT(pc.COD_N4,' ',pc.N4) as natureza,
	'Devolução' as tipo_documento,
	'Processada' as status_item	 
FROM
 `nota_baixas` `nb`
  JOIN `contas_bancarias` `c` ON  ( `c`.`ID` = `nb`.`conta_bancaria_id` ) 
	JOIN `origemfundos` `o` ON ( `c`.`ORIGEM_FUNDOS_ID` = `o`.`id` ) 
	join notas as n on (nb.nota_id = n.idNotas )
	INNER JOIN fornecedores AS f ON ( n.codFornecedor = f.idFornecedores ) 
	inner join empresas as e on (c.UR = e.id)
	INNER join plano_contas as pc on (nb.plano_conta_id = pc.ID)  
WHERE
nb.data_pagamento BETWEEN '{$data['inicio']}' and '{$data['fim']}'
{$condFiliaisContaBancaria}
SQL;

	}


   
}
