<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class ContaBancaria extends AbstractModel implements DAOInterface
{

    public static function getAll()
    {
        return parent::get(self::getSQL());
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getSQL($id = null)
    {

        $id = isset($id) && !empty($id) ? 'WHERE 1=1 AND c.ID = ' . $id : null;
        return "SELECT 
            c.*,
            c.ID as id,
            e.SIGLA_EMPRESA as ur,
            tipo_conta_bancaria.abreviacao,
            o.COD_BANCO as cod_banco,
            CONCAT(o.SIGLA_BANCO,' - ',tipo_conta_bancaria.abreviacao,' - ',c.AGENCIA,' - ',c.NUM_CONTA,' - ',e.SIGLA_EMPRESA) as nome_conta
            FROM 
            contas_bancarias as c
				INNER JOIN origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id)
				INNER JOIN empresas as e ON (c.UR = e.id)
				LEFT JOIN tipo_conta_bancaria ON tipo_conta_bancaria.id = c.TIPO_CONTA_BANCARIA_ID
            {$id}
            ORDER BY e.nome, o.SIGLA_BANCO, abreviacao ASC";
    }
    public static function getContasBancariasByUR($ur)
    {
        $sql = <<<SQL
SELECT
    c.ID as id,
    o.forma,
    o.SIGLA_BANCO as banco,
    c.AGENCIA as agencia,
    c.NUM_CONTA as conta,
    e.SIGLA_EMPRESA as ur,
    tipo_conta_bancaria.abreviacao,
    o.COD_BANCO as cod_banco,
    CONCAT(o.forma,' - ',tipo_conta_bancaria.abreviacao,' - ',c.AGENCIA,' - ',c.NUM_CONTA,' - ',e.SIGLA_EMPRESA) as nome_conta
FROM contas_bancarias as c
    INNER JOIN origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id)
    INNER JOIN empresas as e ON (c.UR = e.id)
    LEFT JOIN tipo_conta_bancaria ON tipo_conta_bancaria.id = c.TIPO_CONTA_BANCARIA_ID
where c.UR= {$ur}
ORDER BY
banco
SQL;

        return parent::get($sql, 'Assoc');
    }

    public static function getContasBancariasByURs($urs, $tipoConta = '')
    {
        $condTipoConta = empty($tipoConta) ? '' : "AND tipo_conta_bancaria.id = {$tipoConta}";

        $sql = <<<SQL
SELECT
    c.ID as id,
    o.forma,
    o.SIGLA_BANCO as banco,
    c.AGENCIA as agencia,
    c.NUM_CONTA as conta,
    e.SIGLA_EMPRESA as ur,
    tipo_conta_bancaria.abreviacao,
    o.COD_BANCO as cod_banco,
    CONCAT(o.forma,' - ',tipo_conta_bancaria.abreviacao,' - ',c.AGENCIA,' - ',c.NUM_CONTA,' - ',e.SIGLA_EMPRESA) as nome_conta
FROM contas_bancarias as c
    INNER JOIN origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id)
    INNER JOIN empresas as e ON (c.UR = e.id)
    LEFT JOIN tipo_conta_bancaria ON tipo_conta_bancaria.id = c.TIPO_CONTA_BANCARIA_ID
where
    c.UR in {$urs}
    {$condTipoConta}
ORDER BY
    nome_conta
SQL;

        return parent::get($sql, 'Assoc');
    }

    public static function getContasBancariasCnabByURs($urs, $tipoConta = '', $banco = '')
    {
        $condTipoConta = empty($tipoConta) ? '' : "AND tipo_conta_bancaria.id = {$tipoConta}";
        $condBanco = empty($banco) ? '' : "AND o.id = {$banco}";

        $sql = <<<SQL
SELECT
    c.ID as id,
    o.forma,
    o.SIGLA_BANCO as banco,
    c.AGENCIA as agencia,
    c.NUM_CONTA as conta,
    e.SIGLA_EMPRESA as ur,
    tipo_conta_bancaria.abreviacao,
    o.COD_BANCO as cod_banco,
    CONCAT(o.forma,' - ',tipo_conta_bancaria.abreviacao,' - ',c.AGENCIA,' - ',c.NUM_CONTA,' - ',e.SIGLA_EMPRESA) as nome_conta
FROM contas_bancarias as c
    INNER JOIN origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id)
    INNER JOIN empresas as e ON (c.UR = e.id)
    LEFT JOIN tipo_conta_bancaria ON tipo_conta_bancaria.id = c.TIPO_CONTA_BANCARIA_ID
where
    c.UR in {$urs}
    AND c.CONVENIO_BANCO IS NOT NULL
    {$condTipoConta}
    {$condBanco}
ORDER BY
    nome_conta
SQL;

        return parent::get($sql, 'Assoc');
    }

    public static function getContasBancariasByFilters($filter)
    {
        if($filter['banco'] == 275){
          // caso seja santander cod 033, tabela origem_fundos = ID 275
          $cond = "UPPER(REPLACE(CONCAT(c.AGENCIA,c.NUM_CONTA),'-',''))";

        }else if($filter['banco'] == 7){
          // caso seja bradesco cod 237, tabela origem_fundos = ID 7
          $cond = "LPAD(RPAD(UPPER(REPLACE(c.NUM_CONTA,'-','')),LENGTH(UPPER(
            REPLACE(c.NUM_CONTA, '-', '')
          )) - 1,'0'),10,0)";
            
        }else{
            $cond = "LPAD(UPPER(REPLACE(c.NUM_CONTA,'-','')),10,0)";
        }

        $sql = <<<SQL
SELECT
				c.ID as id,
				o.SIGLA_BANCO as banco,
				c.AGENCIA as agencia,
				c.NUM_CONTA as conta,
				e.SIGLA_EMPRESA as ur,
				tipo_conta_bancaria.abreviacao,
				o.COD_BANCO as cod_banco,
				CONCAT(o.SIGLA_BANCO,' - ',tipo_conta_bancaria.abreviacao,' - ',c.AGENCIA,' - ',c.NUM_CONTA,' - ',e.SIGLA_EMPRESA) as nome_conta
			FROM contas_bancarias as c
				INNER JOIN origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id)
				INNER JOIN empresas as e ON (c.UR = e.id)
				LEFT JOIN tipo_conta_bancaria ON tipo_conta_bancaria.id = c.TIPO_CONTA_BANCARIA_ID
      where
      c.UR in {$filter['ur']}
      AND
      $cond = UPPER('{$filter['conta']}')
      AND
      c.ORIGEM_FUNDOS_ID = '{$filter['banco']}'
      AND
      c.TIPO_CONTA_BANCARIA_ID = {$filter['tipo']}
			ORDER BY
			banco

SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getContasBancariasByAgenciaConta($agencia, $conta)
    {
        $condTipoConta = empty($tipoConta) ? '' : "AND tipo_conta_bancaria.id = {$tipoConta}";

        $sql = <<<SQL
SELECT
 c.ID as id,
 o.forma,
 o.SIGLA_BANCO as banco,
 c.AGENCIA as agencia,
 c.NUM_CONTA as conta,
 e.SIGLA_EMPRESA as ur,
 tipo_conta_bancaria.abreviacao,
 o.COD_BANCO as cod_banco,
 CONCAT(o.forma,' - ',tipo_conta_bancaria.abreviacao,' - ',c.AGENCIA,' - ',c.NUM_CONTA,' - ',e.SIGLA_EMPRESA) as nome_conta
FROM contas_bancarias as c
 INNER JOIN origemfundos as o ON (c.ORIGEM_FUNDOS_ID = o.id)
 INNER JOIN empresas as e ON (c.UR = e.id)
 LEFT JOIN tipo_conta_bancaria ON tipo_conta_bancaria.id = c.TIPO_CONTA_BANCARIA_ID
WHERE
   c.AGENCIA LIKE '%{$agencia}%'
   AND c.NUM_CONTA LIKE '%{$conta}%'
ORDER BY
 banco
LIMIT 1
SQL;

        return current(parent::get($sql, 'Assoc'));
    }

    public static function getSaldoAnteriorContasBancarias($filters)
    {
        $umDia = new \DateInterval('P1D');
        $datesearch = \DateTime::createFromFormat('Y-m-d', $filters['data_saldo'])
            ->sub($umDia)
            ->format('Y-m-d');
        $compContas = "";
        $compEstorno = "";
        $compNotasBaixa = "";
        if(count($filters['contas']) > 0) {
            $impContas = implode(',', $filters['contas']);
            $compContas = " AND idcb.ID IN ({$impContas})";
            $compEstorno = " AND hep.origem IN ({$impContas})";
            $compNotasBaixa = " AND nb.conta_bancaria_id IN ({$impContas})";
        }

        $sql = <<<SQL
SELECT
	*, 
      ROUND(
        (recebimentos + creditos + taxas_credito + notas_baixas_credito) - 
        (desembolsos + debitos + taxas_debito + notas_baixas_debito + historico_estornos), 2
      ) + ROUND(saldo_inicial, 2) AS saldoAnterior
FROM
	(
		SELECT
			idcb.ID,
			idcb.conta,
			COALESCE (
				(
					SELECT
						SUM(idr.valor_final_conciliado)
					FROM
						indexed_demonstrativo_recebimentos idr
					WHERE
					    origem = idcb.ID
					    AND idr.DATA_CONCILIADO BETWEEN (
                            SELECT
                                DATA_SALDO_INICIAL
                            FROM
                                contas_bancarias cb
                            WHERE
                                cb.ID = idcb.ID
                        )
					    AND '{$datesearch}'
					    AND CONCILIADO = 'S'
					    {$compContas}
				),
				0.00
			) AS recebimentos,
			COALESCE (
				(
					SELECT
						SUM(idd.valor_final_conciliado)
					FROM
						indexed_demonstrativo_desembolsos idd
					WHERE
						origem = idcb.ID
					    AND idd.DATA_CONCILIADO BETWEEN (
                            SELECT
                                DATA_SALDO_INICIAL
                            FROM
                                contas_bancarias cb
                            WHERE
                                cb.ID = idcb.ID
                        )
                        AND '{$datesearch}'
                        AND CONCILIADO = 'S'
                        {$compContas}
				),
				0.00
			) AS desembolsos,
			COALESCE (
				(
					SELECT
						SUM(tb1.VALOR)
					FROM
						transferencia_bancaria AS tb1
						INNER JOIN contas_bancarias cb1 ON cb1.ID = tb1.CONTA_DESTINO
					WHERE
						CONTA_DESTINO = idcb.ID
					    AND tb1.DATA_CONCILIADO_DESTINO BETWEEN (
						SELECT
							DATA_SALDO_INICIAL
						FROM
							contas_bancarias cb
						WHERE
							cb.ID = idcb.ID
					)
					AND '{$datesearch}'
					AND tb1.CONCILIADO_DESTINO = 'S'
					AND tb1.CANCELADA_POR IS NULL
				),
				0.00
			) AS creditos,
		    COALESCE(
		        (
		            SELECT
                        SUM(hep.valor)
                    FROM
                        estorno_parcela hep
                        INNER JOIN parcelas p ON (p.id = hep.parcela_id)
                        INNER JOIN notas n ON (n.idNotas = p.idNota)
                        LEFT JOIN fornecedores f ON (n.codFornecedor = f.idFornecedores)
                    WHERE
                        hep.origem = idcb.ID
                        AND hep.data_conciliada <= '{$datesearch}'
                        {$compEstorno}
		        ),
		        0.00
		    ) AS historico_estornos,
		    COALESCE(
		        (
		            SELECT
                        SUM(nb.valor)
                    FROM
	                    nota_baixas as nb
				    WHERE
                        nb.canceled_by IS NULL
                        AND nb.conciliado = 'S'
                        AND nb.data_conciliado <= '{$datesearch}'
		                {$compNotasBaixa}
		                AND nb.conta_bancaria_id = idcb.ID
		                AND nb.tipo_movimentacao = '1'
		        ),
		        0.00
		    ) AS notas_baixas_debito,
           COALESCE(
            (
                SELECT
                    SUM(nb.valor)
                FROM
                    nota_baixas as nb
                WHERE
                    nb.canceled_by IS NULL
                    AND nb.conciliado = 'S'
                    AND nb.data_conciliado <= '{$datesearch}'
                    {$compNotasBaixa}
                    AND nb.conta_bancaria_id = idcb.ID
                    AND nb.tipo_movimentacao = '0'
            ),
            0.00
		    ) AS notas_baixas_credito,
			COALESCE (
				(
					SELECT
						SUM(tb2.VALOR)
					FROM
						transferencia_bancaria AS tb2
						INNER JOIN contas_bancarias cb2 ON cb2.ID = tb2.CONTA_ORIGEM
					WHERE
						CONTA_ORIGEM = idcb.ID
					    AND tb2.DATA_CONCILIADO_ORIGEM BETWEEN (
						SELECT
							DATA_SALDO_INICIAL
						FROM
							contas_bancarias cb
						WHERE
							cb.ID = idcb.ID
					)
					AND '{$datesearch}'
					AND tb2.CONCILIADO_ORIGEM = 'S'
					AND tb2.CANCELADA_POR IS NULL
				),
				0.00
			) AS debitos,
		    COALESCE (
				(
					SELECT
						SUM(itl.valor)
					FROM
						impostos_tarifas imptar
					    INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
					    INNER JOIN contas_bancarias cb3 ON cb3.ID = itl.conta
					WHERE	
					itl.data <= '{$datesearch}'
					AND itl.conta = idcb.ID
				    AND imptar.debito_credito = 'DEBITO'
				),
				0.00
			) AS taxas_debito,
            COALESCE (
             (
                 SELECT
                     SUM(itl.valor)
                 FROM
                     impostos_tarifas imptar
                     INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
                     INNER JOIN contas_bancarias cb3 ON cb3.ID = itl.conta
                 WHERE	
                 itl.data <= '{$datesearch}'
                 AND itl.conta = idcb.ID
                 AND imptar.debito_credito = 'CREDITO'
             ),
             0.00
            ) AS taxas_credito,
		    idcb.SALDO_INICIAL AS saldo_inicial
		FROM
			indexed_demonstrativo_contas_bancarias AS idcb
        WHERE
          1 = 1
          {$compContas}
		GROUP BY
			idcb.ID
	) AS resultado
GROUP BY
	ID
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getSaldoAtualContasBancarias($filters)
    {
        $datesearch = $filters['data_saldo'];
        $compContas = "";
        $compEstorno = "";
        $compNotasBaixa = "";
        if(count($filters['contas']) > 0) {
            $impContas = implode(',', $filters['contas']);
            $compContas = " AND idcb.ID IN ({$impContas})";
            $compEstorno = " AND hep.origem IN ({$impContas})";
            $compNotasBaixa = " AND nb.conta_bancaria_id IN ({$impContas})";
        }

        $sql = <<<SQL
SELECT
	*, 
    ROUND((recebimentos + creditos + taxas_credito + notas_baixas_credito), 2) AS entradas,   
    ROUND((desembolsos + debitos + taxas_debito + notas_baixas_debito + historico_estornos), 2) AS saidas,   
    COALESCE(
      ROUND(
        (recebimentos + creditos + taxas_credito + notas_baixas_credito) - 
        (desembolsos + debitos + taxas_debito + notas_baixas_debito + historico_estornos), 2
      ),
      0.00
    ) AS saldoAtual
FROM
	(
		SELECT
			idcb.ID,
			idcb.conta,
			COALESCE (
				(
				    SELECT
						SUM(idr.valor_final_conciliado)
					FROM
						indexed_demonstrativo_recebimentos idr
					WHERE
					    origem = idcb.ID
					    AND idr.DATA_CONCILIADO = '{$datesearch}'
					    AND CONCILIADO = 'S'
					    {$compContas}
				),
				0.00
			) AS recebimentos,
			COALESCE (
				(
				    SELECT
						SUM(idd.valor_final_conciliado)
					FROM
						indexed_demonstrativo_desembolsos idd
					WHERE
						origem = idcb.ID
					    AND idd.DATA_CONCILIADO = '{$datesearch}'
                        AND CONCILIADO = 'S'
                        {$compContas}
				),
				0.00
			) AS desembolsos,
			COALESCE (
				(
					SELECT
						SUM(tb1.VALOR)
					FROM
						transferencia_bancaria AS tb1
						INNER JOIN contas_bancarias cb1 ON cb1.ID = tb1.CONTA_DESTINO
					WHERE
						CONTA_DESTINO = idcb.ID
					    AND tb1.DATA_CONCILIADO_DESTINO = '{$datesearch}'
					AND tb1.CONCILIADO_DESTINO = 'S'
					AND tb1.CANCELADA_POR IS NULL
				),
				0.00
			) AS creditos,
		    COALESCE(
		        (
		            SELECT
                        SUM(hep.valor)
                    FROM
                        estorno_parcela hep
                        INNER JOIN parcelas p ON (p.id = hep.parcela_id)
                        INNER JOIN notas n ON (n.idNotas = p.idNota)
                        LEFT JOIN fornecedores f ON (n.codFornecedor = f.idFornecedores)
                    WHERE
                        hep.origem = idcb.ID
                        AND hep.data_conciliada = '{$datesearch}'
                        {$compEstorno}
		        ),
		        0.00
		    ) AS historico_estornos,
		    COALESCE(
		        (
		            SELECT
                        SUM(nb.valor)
                    FROM
	                    nota_baixas as nb
				    WHERE
                        nb.canceled_by IS NULL
                        AND nb.conciliado = 'S'
                        AND nb.data_conciliado = '{$datesearch}'
		                {$compNotasBaixa}
		                AND nb.conta_bancaria_id = idcb.ID
		                AND nb.tipo_movimentacao = '1'
		        ),
		        0.00
		    ) AS notas_baixas_debito,
		    COALESCE(
		        (
		            SELECT
                        SUM(nb.valor)
                    FROM
	                    nota_baixas as nb
				    WHERE
                        nb.canceled_by IS NULL
                        AND nb.conciliado = 'S'
                        AND nb.data_conciliado = '{$datesearch}'
		                {$compNotasBaixa}
		                AND nb.conta_bancaria_id = idcb.ID
		                AND nb.tipo_movimentacao = '0'
		        ),
		        0.00
		    ) AS notas_baixas_credito,
			COALESCE (
				(
					SELECT
						SUM(tb2.VALOR)
					FROM
						transferencia_bancaria AS tb2
						INNER JOIN contas_bancarias cb2 ON cb2.ID = tb2.CONTA_ORIGEM
					WHERE
						CONTA_ORIGEM = idcb.ID
					    AND tb2.DATA_CONCILIADO_ORIGEM = '{$datesearch}'
					AND tb2.CONCILIADO_ORIGEM = 'S'
					AND tb2.CANCELADA_POR IS NULL
				),
				0.00
			) AS debitos,
			COALESCE (
				(
					SELECT
						SUM(itl.valor)
					FROM
						impostos_tarifas imptar
					    INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
					    INNER JOIN contas_bancarias cb3 ON cb3.ID = itl.conta
					WHERE	
					itl.data = '{$datesearch}'
					AND itl.conta = idcb.ID
				    AND imptar.debito_credito = 'DEBITO'
				),
				0.00
			) AS taxas_debito,
            COALESCE (
             (
                 SELECT
                     SUM(itl.valor)
                 FROM
                     impostos_tarifas imptar
                     INNER JOIN impostos_tarifas_lancto itl ON (imptar.id = itl.item)
                     INNER JOIN contas_bancarias cb3 ON cb3.ID = itl.conta
                 WHERE	
                 itl.data = '{$datesearch}'
                 AND itl.conta = idcb.ID
                 AND imptar.debito_credito = 'CREDITO'
             ),
             0.00
            ) AS taxas_credito,
		    idcb.SALDO_INICIAL AS saldo_inicial
		FROM
			indexed_demonstrativo_contas_bancarias AS idcb
        WHERE
          1 = 1
          {$compContas}
		GROUP BY
			idcb.ID
	) AS resultado
GROUP BY
	ID
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function salvarHistoricoSaldoContasBancarias($data, $data_saldo)
    {
        $sql = <<<SQL
INSERT INTO historico_saldo_contas_bancarias
(conta_id, data_saldo, saldo_anterior, entrada, saida, saldo_atual, criado_em, criado_por) 
VALUES 
SQL;
        foreach ($data as $key => $row) {
            $sql .= <<<SQL
(
    '{$key}', '{$data_saldo}', '{$row['saldoAnterior']}', 
    '{$row['entradas']}', '{$row['saidas']}', '{$row['saldoAtual']}', NOW(), '{$_SESSION['id_user']}'
),
SQL;
        }
        $sql = substr_replace($sql, '', -1, 1);
        $rs = mysql_query($sql);
        return true;
    }
}
