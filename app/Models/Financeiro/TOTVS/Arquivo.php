<?php
namespace App\Models\Financeiro\TOTVS;

use \App\Models\AbstractModel,
    \App\Helpers\StringHelper;

if (!function_exists('dd')) {
    function dd()
    {
        echo '<pre>';
        $args = func_get_args();
        foreach ($args as $arg) {
            $arg = [$arg];
            call_user_func_array('print_r', $arg);
            echo '<br><br>';
        }
        die();
    }
}

if (!function_exists('d')) {
    function d()
    {
        echo '<pre>';
        $args = func_get_args();
        call_user_func_array('print_r', $args);
        echo '<br><br>';
    }
}

class Arquivo
{

    private $txt = "";
    private $registro = [];

    public function createTextFromData($data)
    {
        $this->createLotesFromData($data);
        foreach($this->registro as $registro){
            $this->txt .= array_reduce($registro, function($txt, $lote){
                # Gera a linha dos dados do lançamento
                unset($lote['partidas']['partilha_dobrada']['nota']);
                unset($lote['partidas']['partilha_dobrada']['tipo_lancto']);
                $txt .= implode('', $lote['partidas']['partilha_dobrada']);
                $txt .= "\r\n";

                # Gera a linha do juros/multa do lançamento (se houver)
                if(!empty($lote['partidas']['juros']) || $lote['partidas']['juros'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['juros']);
                }

                # Gera a linha do desconto do lançamento (se houver)
                if(!empty($lote['partidas']['desconto']) || $lote['partidas']['desconto'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['desconto']);
                }

                # Gera a linha do IRRF do lançamento (se houver)
                if(!empty($lote['partidas']['IRRF']) || $lote['partidas']['IRRF'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['IRRF']);
                }

                # Gera a linha do PIS do lançamento (se houver)
                if(!empty($lote['partidas']['PIS']) || $lote['partidas']['PIS'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['PIS']);
                }

                # Gera a linha do COFINS do lançamento (se houver)
                if(!empty($lote['partidas']['COFINS']) || $lote['partidas']['COFINS'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['COFINS']);
                }

                # Gera a linha do CSLL do lançamento (se houver)
                if(!empty($lote['partidas']['CSLL']) || $lote['partidas']['CSLL'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['CSLL']);
                }

                # Gera a linha do PCC do lançamento (se houver)
                if(!empty($lote['partidas']['PCC']) || $lote['partidas']['PCC'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['PCC']);
                }

                # Gera a linha do ISS do lançamento (se houver)
                if(!empty($lote['partidas']['ISS']) || $lote['partidas']['ISS'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['ISS']);
                }

                # Gera a linha do INSS do lançamento (se houver)
                if(!empty($lote['partidas']['INSS']) || $lote['partidas']['INSS'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['INSS']);
                }

                # Gera a linha das Tarifas Adicionais do lançamento (se houver)
                if(!empty($lote['partidas']['outras_tarifas']) || $lote['partidas']['outras_tarifas'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['outras_tarifas']);
                }

                # Gera a linha dos Encargos Financeiros do lançamento (se houver)
                if(!empty($lote['partidas']['encargos']) || $lote['partidas']['encargos'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['encargos']);
                }

                return $txt;
            });
        }

        return $this->txt;
    }

    private function reduceCreditoDebitoFromData($data)
    {
        return array_reduce($data, function($txt, $source){
            $txt .= implode('', $source);
            $txt .= "\r\n";
            return $txt;
        });
    }

    private function reduceRetencoesFromData($data)
    {
        $txt = array_reduce($data, function($txt, $source){
            $txt .= $source;
            return $txt;
        });
        $txt .= "\r\n";
        return $txt;
    }

    private function createLotesFromData ($data)
    {
        unset($this->registro);
        $contArray = 0;
        $cont = 0;
        $periodo = new \stdClass();
        $periodo->inicio = $data['inicio'];
        $periodo->fim = $data['fim'];
        $lancamento = new Lancamento();
        /*$notas = $lancamento->getLancamentosNotas($periodo, $data['tipo']);
        $transferencias = $lancamento->getLancamentosTransferenciaBancaria($periodo, $data['tipo']);
        $tarifas = $lancamento->getLancamentosTarifasBancaria($periodo, $data['tipo']);
        $antecipacoes = $lancamento->getLancamentosAntecipacao($periodo, $data['tipo']);*/
        $lancamentos_comuns = $lancamento->getLancamentosNotasTodos($periodo, $data['tipo']);
        $compensacoes = $lancamento->getLancamentosCompensacao($periodo, $data['tipo']);
        $aglutinacoes = $lancamento->getLancamentosAglutinacao($periodo, $data['tipo']);

        $lancamentos = array_merge(
            $lancamentos_comuns,
            $compensacoes,
            $aglutinacoes
        );

        /*$totalRecebimentos = 0;
        $totalDesembolsos = 0;
        echo '<pre>';
        foreach($lancamentos as $lancto) {
            echo $lancto['data_lancamento'] . '#' . $lancto['idNotas'] . '#' . $lancto['tipo'] . '#' . $lancto['valor'] . '#' . $lancto['descricao'] . '<br>';
            if(!is_numeric($lancto['destino'])) {
                if ($lancto['tipo'] == '0') {
                    $totalRecebimentos += $lancto['valor'];
                } else {
                    $totalDesembolsos += $lancto['valor'];
                }
            }
        }
        dd();*/
        //dd($lancamentos);

        $totalRecebimentos = 0;
        $totalDesembolsos = 0;
        $totalConta = [];

        foreach($lancamentos as $lancto) {
            $sequencia_lote = 0;

            $partilha_dobrada = [];

            if ($lancto['tipo'] == '1') {
                $tipo_lancamento_ir = '3';
                $conta_ir_debito = StringHelper::stringPad('', 8, 'left', ' ');
                $conta_ir_credito = '21304001';

                $tipo_lancamento_pis = '3';
                $conta_pis_debito = StringHelper::stringPad('', 8, 'left', ' ');
                $conta_pis_credito = '21304006';

                $tipo_lancamento_cofins = '3';
                $conta_cofins_debito = StringHelper::stringPad('', 8, 'left', ' ');
                $conta_cofins_credito = '21304007';

                $tipo_lancamento_csll = '3';
                $conta_csll_debito = StringHelper::stringPad('', 8, 'left', ' ');
                $conta_csll_credito = '21304002';

                $tipo_lancamento_pcc = '3';
                $conta_pcc_debito = StringHelper::stringPad('', 8, 'left', ' ');
                $conta_pcc_credito = '21304002';

                $tipo_lancamento_iss = '3';
                $conta_issqn_debito = StringHelper::stringPad('', 8, 'left', ' ');
                $conta_issqn_credito = '21304003';

                $tipo_lancamento_inss = '3';
                $conta_inss_debito = StringHelper::stringPad('', 8, 'left', ' ');
                $conta_inss_credito = '21304004';

                $tipo_lancamento_juros = '3';
                $conta_juros_debito = '61201002';
                $conta_juros_credito = StringHelper::stringPad('', 8, 'left', ' ');

                $tipo_lancamento_desconto = '3';
                $conta_desconto_debito = StringHelper::stringPad('', 8, 'left', ' ');
                $conta_desconto_credito = '61101002';

                $tipo_lancamento_outras_tarifas = '3';
                $conta_outras_tarifas_debito = '51209001';
                $conta_outras_tarifas_credito = StringHelper::stringPad('', 8, 'left', ' ');

                $tipo_lancamento_encargos = '3';
                $conta_encargos_debito = '51209001';
                $conta_encargos_credito = StringHelper::stringPad('', 8, 'left', ' ');
            } else {
                $tipo_lancamento_ir = '3';
                $conta_ir_debito = '21304001';
                $conta_ir_credito = StringHelper::stringPad('', 8, 'left', ' ');

                $tipo_lancamento_pis = '3';
                $conta_pis_debito = '21304006';
                $conta_pis_credito = StringHelper::stringPad('', 8, 'left', ' ');

                $tipo_lancamento_cofins = '3';
                $conta_cofins_debito = '21304007';
                $conta_cofins_credito = StringHelper::stringPad('', 8, 'left', ' ');

                $tipo_lancamento_csll = '3';
                $conta_csll_debito = '21304002';
                $conta_csll_credito = StringHelper::stringPad('', 8, 'left', ' ');

                $tipo_lancamento_pcc = '3';
                $conta_pcc_debito = '21304002';
                $conta_pcc_credito = StringHelper::stringPad('', 8, 'left', ' ');

                $tipo_lancamento_iss = '3';
                $conta_issqn_debito = '21304003';
                $conta_issqn_credito = StringHelper::stringPad('', 8, 'left', ' ');

                $tipo_lancamento_inss = '3';
                $conta_inss_debito = '21304004';
                $conta_inss_credito = StringHelper::stringPad('', 8, 'left', ' ');

                $tipo_lancamento_juros = '3';
                $conta_juros_debito = StringHelper::stringPad('', 8, 'left', ' ');
                $conta_juros_credito = '61101001';

                $tipo_lancamento_desconto = '3';
                $conta_desconto_debito = '61201001';
                $conta_desconto_credito = StringHelper::stringPad('', 8, 'left', ' ');

                $tipo_lancamento_outras_tarifas = '3';
                $conta_outras_tarifas_debito = StringHelper::stringPad('', 8, 'left', ' ');
                $conta_outras_tarifas_credito = '51209001';

                $tipo_lancamento_encargos = '3';
                $conta_encargos_debito = StringHelper::stringPad('', 8, 'left', ' ');
                $conta_encargos_credito = '51209001';
            }

            $juros_historico = StringHelper::removeAcentosViaEReg($lancto['juros_historico'], true);
            $juros_historico = StringHelper::cropString($juros_historico, 0, 100, '');
            $desconto_historico = StringHelper::removeAcentosViaEReg($lancto['desconto_historico'], true);
            $desconto_historico = StringHelper::cropString($desconto_historico, 0, 100, '');
            $retencoes_historico = StringHelper::removeAcentosViaEReg($lancto['retencoes_historico'], true);
            $retencoes_historico = StringHelper::cropString($retencoes_historico, 0, 100, '');

            if(is_numeric($lancto['idNotas'])) { # Se for Notas/Parcelas
                $partidas = [];
                if(
                    $lancto['lancamento_comum'] == 'nota' &&
                    $lancto['nota_adiantamento'] == 'n' &&
                    $lancto['nota_aglutinacao'] == 'n'
                ) {
                    $partidas[$lancto['idNotas']]['debito'] = $lancamento->getDebitoByNotaId($lancto['idNotas'], $data['tipo']);
                    $partidas[$lancto['idNotas']]['credito'] = $lancamento->getCreditoByNotaId($lancto['idNotas'], $data['tipo']);
                } elseif($lancto['lancamento_comum'] == 'estorno') {
                    $partidas[$lancto['idNotas']]['debito'] = $lancamento->getDebitoByNotaId($lancto['idNotas'], $data['tipo']);
                    $partidas[$lancto['idNotas']]['credito'] = $lancamento->getCreditoByNotaId($lancto['idNotas'], $data['tipo']);
                } elseif($lancto['nota_adiantamento'] == 's') {
                    $partidas[$lancto['idNotas']]['debito'] = $lancamento->getDebitoAntecipacaoByNotaId($lancto['idNotas']);
                    $partidas[$lancto['idNotas']]['credito'] = $lancamento->getCreditoAntecipacaoByNotaId($lancto['idNotas']);
                } elseif($lancto['lancamento_comum'] == 'compensacao') {
                    $partidas[$lancto['idNotas']]['debito'] = $lancamento->getDebitoCompensacaoByNotaId($lancto['idNotas']);
                    $partidas[$lancto['idNotas']]['credito'] = $lancamento->getCreditoCompensacaoByNotaId($lancto['idNotas']);
                } elseif($lancto['nota_baixa'] == 's') {
                    $partidas[$lancto['idNotas']]['debito'] = $lancamento->getDebitoBaixaAntecipacaoByNotaId($lancto['idNotas']);
                    $partidas[$lancto['idNotas']]['credito'] = $lancamento->getCreditoBaixaAntecipacaoByNotaId($lancto['idNotas']);
                } elseif($lancto['nota_aglutinacao'] == 's') {
                    $partidas[$lancto['idNotas']]['debito'] = $lancamento->getDebitoAglutinacaoByNotaId($lancto['idNotas'], $lancto['conciliado']);
                    $partidas[$lancto['idNotas']]['credito'] = $lancamento->getCreditoAglutinacaoByNotaId($lancto['idNotas'], $lancto['conciliado']);
                }

                if(count($partidas[$lancto['idNotas']]['debito']) == 0 || count($partidas[$lancto['idNotas']]['credito']) == 0) {
                    echo "nota sem partilha (Tipo: {$lancto['lancamento_comum']}) 
                    (Adiantamento? {$lancto['nota_adiantamento']}) 
                    (Aglutinação? {$lancto['nota_aglutinacao']}) 
                     (Compensação? {$lancto['nota_compensacao']})" . $lancto['idNotas'] . '<br>';
                }
                $okJuros = false;
                $okDesconto = false;
                foreach($partidas[$lancto['idNotas']]['debito'] as $partida) {
                    $valor = round ($lancto['valor'] * $partida['PORCENTAGEM'] / 100, 2);
                    $jurosMultaValor = $lancto['JurosMulta'];
                    $descontoValor = $lancto['desconto'];
                    if($okJuros) {
                        $jurosMultaValor = 0.00;
                    }
                    if($okDesconto) {
                        $descontoValor = 0.00;
                    }
                    $okJuros = true;
                    $okDesconto = true;

                    if($data['tipo'] == 'inclusao') {
                        $valor += round ($lancto['FRETE'] * $partida['PORCENTAGEM'] / 100, 2);
                        $valor += round ($lancto['DES_ACESSORIAS'] * $partida['PORCENTAGEM'] / 100, 2);
                        $valor -= round ($lancto['DESCONTO_BONIFICACAO'] * $partida['PORCENTAGEM'] / 100, 2);
                        $valor += round ($lancto['SEGURO'] * $partida['PORCENTAGEM'] / 100, 2);
                        $valor += round ($lancto['IPI'] * $partida['PORCENTAGEM'] / 100, 2);
                    }

                    if($data['tipo'] == 'baixa') {
                        if ($lancto['desconto'] > 0) {
                            $valor -= ($lancto['desconto'] * $partida['PORCENTAGEM']) / 100;
                        }
                    }

                    $codigo_debito = $partida['codigo_debito'];
                    if($codigo_debito == '1.1.6.01.002') {
                        $codigo_debito = '1.1.6.01.011';
                    } elseif($codigo_debito == '1.1.6.01.001') {
                        $codigo_debito = '1.1.6.01.012';
                    } elseif($codigo_debito == '1.1.6.01.003') {
                        $codigo_debito = '1.1.6.01.013';
                    } elseif($codigo_debito == '1.1.6.01.004') {
                        $codigo_debito = '1.1.6.01.014';
                    } elseif($codigo_debito == '1.1.6.01.005') {
                        $codigo_debito = '1.1.6.01.015';
                    }

                    $codigo_credito = $partidas[$lancto['idNotas']]['credito'][0]['codigo_credito'];
                    if($codigo_credito == '1.1.6.01.002') {
                        $codigo_credito = '1.1.6.01.011';
                    } elseif($codigo_credito == '1.1.6.01.001') {
                        $codigo_credito = '1.1.6.01.012';
                    } elseif($codigo_credito == '1.1.6.01.003') {
                        $codigo_credito = '1.1.6.01.013';
                    } elseif($codigo_credito == '1.1.6.01.004') {
                        $codigo_credito = '1.1.6.01.014';
                    } elseif($codigo_credito == '1.1.6.01.005') {
                        $codigo_credito = '1.1.6.01.015';
                    }

                    if ($lancto['tipo'] == '0') {
                        $conta_ir_credito = '11202017';
                        $conta_pis_credito = '11202017';
                        $conta_cofins_credito = '11202017';
                        $conta_csll_credito = '11202017';
                        $conta_pcc_credito = '11202017';
                        $conta_issqn_credito = '11202017';
                        $conta_inss_credito = '11202017';
                        $conta_juros_debito = StringHelper::stripDots($codigo_debito);
                        $conta_desconto_credito = StringHelper::stripDots($codigo_credito);
                        $conta_outras_tarifas_debito = StringHelper::stripDots($codigo_debito);
                        $conta_encargos_debito = StringHelper::stripDots($codigo_debito);
                        $totalRecebimentos += $valor;
                    }

                    if ($lancto['tipo'] == '1') {
                        $conta_ir_debito = '21202001';
                        $conta_pis_debito = '21202001';
                        $conta_cofins_debito = '21202001';
                        $conta_csll_debito = '21202001';
                        $conta_pcc_debito = '21202001';
                        $conta_issqn_debito = '21202001';
                        $conta_inss_debito = '21202001';
                        $conta_juros_credito = StringHelper::stripDots($codigo_credito);
                        $conta_desconto_debito = StringHelper::stripDots($codigo_debito);
                        $conta_encargos_credito = StringHelper::stripDots($codigo_credito);
                        $conta_outras_tarifas_credito = StringHelper::stripDots($codigo_credito);
                        $totalDesembolsos += $valor;
                    }

                    $historico = StringHelper::removeAcentosViaEReg($lancto['debito_historico'], true);
                    $historico = StringHelper::cropString($historico, 0, 100, '');
                    $totalConta[StringHelper::stripDots($codigo_debito)]['debito']['total'] += $valor;
                    $totalConta[StringHelper::stripDots($codigo_debito)]['debito'][] = $lancto['idNotas'] . ',' .  $lancto['data_lancamento'] . ',' . $valor;
                    $totalConta[StringHelper::stripDots($codigo_credito)]['credito'][] = $lancto['idNotas'] . ',' .  $lancto['data_lancamento'] . ',' . $valor;
                    $totalConta[StringHelper::stripDots($codigo_credito)]['credito']['total'] += $valor;
                    $partilha_dobrada = [
                        "nota" => $lancto['idNotas'],
                        "tipo_lancto" => 'nota',
                        "cod_lancamento" => "001",
                        "tipo" => "3",
                        "conta_debito" => StringHelper::stripDots($codigo_debito),
                        "conta_credito" => StringHelper::stripDots($codigo_credito),
                        "brancos_1" => '  ',
                        "valor" => StringHelper::money($valor),
                        "brancos_2" => '  ',
                        "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                        "historico" => StringHelper::stringPad($historico, 100, 'right', ' '),
                        "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                        "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                    ];
                    $this->registro['dados_lote'][] = [
                        "partidas" => [
                            "partilha_dobrada" => $partilha_dobrada,
                            "juros" => $jurosMultaValor == 0 ? null : [
                                "cod_lancamento" => "001",
                                "tipo" => $tipo_lancamento_juros,
                                "conta_debito" => $conta_juros_debito,
                                "conta_credito" => $conta_juros_credito,
                                "brancos_1" => '  ',
                                "valor" => StringHelper::money($jurosMultaValor),
                                "brancos_2" => '  ',
                                "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                                "historico" => StringHelper::stringPad($juros_historico, 100, 'right', ' '),
                                "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                                "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                            ],
                            "desconto" => $descontoValor == 0 ? null : [
                                "cod_lancamento" => "001",
                                "tipo" => $tipo_lancamento_desconto,
                                "conta_debito" => $conta_desconto_debito,
                                "conta_credito" => $conta_desconto_credito,
                                "brancos_1" => '  ',
                                "valor" => StringHelper::money($descontoValor),
                                "brancos_2" => '  ',
                                "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                                "historico" => StringHelper::stringPad($desconto_historico, 100, 'right', ' '),
                                "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                                "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                            ],
                            "IRRF" => (empty($lancto['IR']) || $lancto['IR'] == 0) ? null : [
                                "cod_lancamento" => "001",
                                "tipo" => $tipo_lancamento_ir,
                                "conta_debito" => $conta_ir_debito,
                                "conta_credito" => $conta_ir_credito,
                                "brancos_1" => '  ',
                                "valor" => StringHelper::money($lancto['IR']),
                                "brancos_2" => '  ',
                                "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                                "historico" => StringHelper::stringPad('IRRF ' . $retencoes_historico, 100, 'right', ' '),
                                "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                                "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                            ],
                            "PIS" => (empty($lancto['PIS']) || $lancto['PIS'] == 0) ? null : [
                                "cod_lancamento" => "001",
                                "tipo" => $tipo_lancamento_pis,
                                "conta_debito" => $conta_pis_debito,
                                "conta_credito" => $conta_pis_credito,
                                "brancos_1" => '  ',
                                "valor" => StringHelper::money($lancto['PIS']),
                                "brancos_2" => '  ',
                                "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                                "historico" => StringHelper::stringPad('PIS ' . $retencoes_historico, 100, 'right', ' '),
                                "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                                "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                            ],
                            "COFINS" => (empty($lancto['COFINS']) || $lancto['COFINS'] == 0) ? null : [
                                "cod_lancamento" => "001",
                                "tipo" => $tipo_lancamento_cofins,
                                "conta_debito" => $conta_cofins_debito,
                                "conta_credito" => $conta_cofins_credito,
                                "brancos_1" => '  ',
                                "valor" => StringHelper::money($lancto['COFINS']),
                                "brancos_2" => '  ',
                                "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                                "historico" => StringHelper::stringPad('COFINS ' . $retencoes_historico, 100, 'right', ' '),
                                "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                                "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                            ],
                            "CSLL" => (empty($lancto['CSLL']) || $lancto['CSLL'] == 0) ? null : [
                                "cod_lancamento" => "001",
                                "tipo" => $tipo_lancamento_csll,
                                "conta_debito" => $conta_csll_debito,
                                "conta_credito" => $conta_csll_credito,
                                "brancos_1" => '  ',
                                "valor" => StringHelper::money($lancto['CSLL']),
                                "brancos_2" => '  ',
                                "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                                "historico" => StringHelper::stringPad('CSLL ' . $retencoes_historico, 100, 'right', ' '),
                                "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                                "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                            ],
                            "PCC" => (empty($lancto['PCC']) || $lancto['PCC'] == 0) ? null : [
                                "cod_lancamento" => "001",
                                "tipo" => $tipo_lancamento_pcc,
                                "conta_debito" => $conta_pcc_debito,
                                "conta_credito" => $conta_pcc_credito,
                                "brancos_1" => '  ',
                                "valor" => StringHelper::money($lancto['PCC']),
                                "brancos_2" => '  ',
                                "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                                "historico" => StringHelper::stringPad('PCC ' . $retencoes_historico, 100, 'right', ' '),
                                "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                                "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                            ],
                            "ISS" => (empty($lancto['ISSQN']) || $lancto['ISSQN'] == 0) ? null : [
                                "cod_lancamento" => "001",
                                "tipo" => $tipo_lancamento_iss,
                                "conta_debito" => $conta_issqn_debito,
                                "conta_credito" => $conta_issqn_credito,
                                "brancos_1" => '  ',
                                "valor" => StringHelper::money($lancto['ISSQN']),
                                "brancos_2" => '  ',
                                "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                                "historico" => StringHelper::stringPad('ISS ' . $retencoes_historico, 100, 'right', ' '),
                                "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                                "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                            ],
                            "INSS" => (empty($lancto['INSS']) || $lancto['INSS'] == 0 ) ? null : [
                                "cod_lancamento" => "001",
                                "tipo" => $tipo_lancamento_inss,
                                "conta_debito" => $conta_inss_debito,
                                "conta_credito" => $conta_inss_credito,
                                "brancos_1" => '  ',
                                "valor" => StringHelper::money($lancto['INSS']),
                                "brancos_2" => '  ',
                                "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                                "historico" => StringHelper::stringPad('INSS ' . $retencoes_historico, 100, 'right', ' '),
                                "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                                "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                            ],
                            "outras_tarifas" => (empty($lancto['outras_tarifas']) || $lancto['outras_tarifas'] == 0) ? null : [
                                "cod_lancamento" => "001",
                                "tipo" => $tipo_lancamento_outras_tarifas,
                                "conta_debito" => $conta_outras_tarifas_debito,
                                "conta_credito" => $conta_outras_tarifas_credito,
                                "brancos_1" => '  ',
                                "valor" => StringHelper::money($lancto['outras_tarifas']),
                                "brancos_2" => '  ',
                                "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                                "historico" => StringHelper::stringPad('OUTRAS TARIFAS ' . $retencoes_historico, 100, 'right', ' '),
                                "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                                "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                            ],
                            "encargos" => (empty($lancto['encargos']) || $lancto['encargos'] == 0) ? null : [
                                "cod_lancamento" => "001",
                                "tipo" => $tipo_lancamento_encargos,
                                "conta_debito" => $conta_encargos_debito,
                                "conta_credito" => $conta_encargos_credito,
                                "brancos_1" => '  ',
                                "valor" => StringHelper::money($lancto['encargos']),
                                "brancos_2" => '  ',
                                "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                                "historico" => StringHelper::stringPad('ENCARGOS ' . $retencoes_historico, 100, 'right', ' '),
                                "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                                "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                            ],
                        ]
                    ];
                }
            } elseif(is_numeric($lancto['destino'])) { # Se for Transferencia Bancaria
                $partidas = [];
                $partidas[$lancto['idNotas']]['debito'] = $lancamento->getCentroResultadoDebitoByDestino($lancto['destino']);
                $partidas[$lancto['idNotas']]['credito'] = $lancamento->getCentroResultadoCreditoByOrigem($lancto['origem']);

                if(count($partidas[$lancto['idNotas']]['debito']) == 0 || count($partidas[$lancto['idNotas']]['credito']) == 0) {
                    echo 'transf sem partilha ' . $lancto['idNotas'] . '<br>';
                }
                foreach($partidas[$lancto['idNotas']]['debito'] as $partida) {
                    $valor = round ($lancto['valor'], 2);
                    $totalConta[StringHelper::stripDots($partida['codigo_debito'])]['debito'][] = $lancto['idNotas'] . ',' .  $lancto['data_lancamento'] . ',' . $valor;
                    $totalConta[StringHelper::stripDots($partidas[$lancto['idNotas']]['credito'][0]['codigo_credito'])]['credito'][] = $lancto['idNotas'] . ',' .  $lancto['data_lancamento'] . ',' . $valor;
                    $totalConta[StringHelper::stripDots($partida['codigo_debito'])]['debito']['total'] += $valor;
                    $totalConta[StringHelper::stripDots($partidas[$lancto['idNotas']]['credito'][0]['codigo_credito'])]['credito']['total'] += $valor;

                    $historico = StringHelper::removeAcentosViaEReg($lancto['debito_historico'], true);
                    $historico = StringHelper::cropString($historico, 0, 100, '');
                    $partilha_dobrada = [
                        "nota" => $lancto['idNotas'],
                        "tipo_lancto" => 'transf',
                        "cod_lancamento" => "001",
                        "tipo" => "3",
                        "conta_debito" => StringHelper::stripDots($partida['codigo_debito']),
                        "conta_credito" => StringHelper::stripDots($partidas[$lancto['idNotas']]['credito'][0]['codigo_credito']),
                        "brancos_1" => '  ',
                        "valor" => StringHelper::money($valor),
                        "brancos_2" => '  ',
                        "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                        "historico" => StringHelper::stringPad($historico, 100, 'right', ' '),
                        "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                        "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                    ];
                }
                $this->registro['dados_lote'][] = [
                    "partidas" => [
                        "partilha_dobrada" => $partilha_dobrada,
                        "juros" => $lancto['JurosMulta'] == 0 ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_juros,
                            "conta_debito" => $conta_juros_debito,
                            "conta_credito" => $conta_juros_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['JurosMulta']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad($juros_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "desconto" => $lancto['desconto'] == 0 ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_desconto,
                            "conta_debito" => $conta_desconto_debito,
                            "conta_credito" => $conta_desconto_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['desconto']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad($desconto_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "IRRF" => (empty($lancto['IR']) || $lancto['IR'] == 0) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_ir,
                            "conta_debito" => $conta_ir_debito,
                            "conta_credito" => $conta_ir_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['IR']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('IRRF ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "PIS" => (empty($lancto['PIS']) || $lancto['PIS'] == 0) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_pis,
                            "conta_debito" => $conta_pis_debito,
                            "conta_credito" => $conta_pis_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['PIS']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('PIS ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "COFINS" => (empty($lancto['COFINS']) || $lancto['COFINS'] == 0) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_cofins,
                            "conta_debito" => $conta_cofins_debito,
                            "conta_credito" => $conta_cofins_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['COFINS']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('COFINS ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "CSLL" => (empty($lancto['CSLL']) || $lancto['CSLL'] == 0) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_csll,
                            "conta_debito" => $conta_csll_debito,
                            "conta_credito" => $conta_csll_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['CSLL']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('CSLL ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "PCC" => (empty($lancto['PCC']) || $lancto['PCC'] == 0) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_pcc,
                            "conta_debito" => $conta_pcc_debito,
                            "conta_credito" => $conta_pcc_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['PCC']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('PCC ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "ISS" => (empty($lancto['ISSQN']) || $lancto['ISSQN'] == 0) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_iss,
                            "conta_debito" => $conta_issqn_debito,
                            "conta_credito" => $conta_issqn_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['ISSQN']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('ISS ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "INSS" => (empty($lancto['INSS']) || $lancto['INSS'] == 0 ) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_inss,
                            "conta_debito" => $conta_inss_debito,
                            "conta_credito" => $conta_inss_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['INSS']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('INSS ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "outras_tarifas" => (empty($lancto['outras_tarifas']) || $lancto['outras_tarifas'] == 0) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_outras_tarifas,
                            "conta_debito" => $conta_outras_tarifas_debito,
                            "conta_credito" => $conta_outras_tarifas_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['outras_tarifas']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('OUTRAS TARIFAS ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "encargos" => (empty($lancto['encargos']) || $lancto['encargos'] == 0) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_encargos,
                            "conta_debito" => $conta_encargos_debito,
                            "conta_credito" => $conta_encargos_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['encargos']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('ENCARGOS ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                    ]
                ];
            } elseif(is_numeric($lancto['conta_tarifa'])) { # Se for Tarifas e Impostos
                $partidas = [];
                $partidas[$lancto['idNotas']]['debito'] = $lancamento->getCentroResultadoDebitoByTarifaImpostoContaId($lancto['conta_tarifa'], $lancto['descricao'], $lancto['tipo']);
                $partidas[$lancto['idNotas']]['credito'] = $lancamento->getCentroResultadoCreditoByTarifaImpostoContaId($lancto['conta_tarifa'], $lancto['descricao'], $lancto['tipo']);

                if(count($partidas[$lancto['idNotas']]['debito']) == 0 || count($partidas[$lancto['idNotas']]['credito']) == 0) {
                    echo 'tarifa sem partilha ' . $lancto['idNotas'] . '<br>';
                }
                foreach($partidas[$lancto['idNotas']]['debito'] as $partida) {
                    $valor = round ($lancto['valor'], 2);
                    if($lancto['tipo'] == 0) {
                        $totalRecebimentos += $lancto['valor'];
                    } else {
                        $totalDesembolsos += $lancto['valor'];
                    }

                    $totalConta[StringHelper::stripDots($partida['codigo_debito'])]['debito'][] = $lancto['idNotas'] . ',' .  $lancto['data_lancamento'] . ',' . $valor;
                    $totalConta[StringHelper::stripDots($partidas[$lancto['idNotas']]['credito'][0]['codigo_credito'])]['credito'][] = $lancto['idNotas'] . ',' .  $lancto['data_lancamento'] . ',' . $valor;
                    $totalConta[StringHelper::stripDots($partida['codigo_debito'])]['debito']['total'] += $valor;
                    $totalConta[StringHelper::stripDots($partidas[$lancto['idNotas']]['credito'][0]['codigo_credito'])]['credito']['total'] += $valor;

                    $historico = StringHelper::removeAcentosViaEReg($lancto['debito_historico'], true);
                    $historico = StringHelper::cropString($historico, 0, 100, '');
                    $partilha_dobrada = [
                        "nota" => $lancto['idNotas'],
                        "tipo_lancto" => 'tarifa',
                        "cod_lancamento" => "001",
                        "tipo" => "3",
                        "conta_debito" => StringHelper::stripDots($partida['codigo_debito']),
                        "conta_credito" => StringHelper::stripDots($partidas[$lancto['idNotas']]['credito'][0]['codigo_credito']),
                        "brancos_1" => '  ',
                        "valor" => StringHelper::money($valor),
                        "brancos_2" => '  ',
                        "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                        "historico" => StringHelper::stringPad($historico, 100, 'right', ' '),
                        "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                        "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                    ];
                }
                $this->registro['dados_lote'][] = [
                    "partidas" => [
                        "partilha_dobrada" => $partilha_dobrada,
                        "juros" => $lancto['JurosMulta'] == 0 ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_juros,
                            "conta_debito" => $conta_juros_debito,
                            "conta_credito" => $conta_juros_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['JurosMulta']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad($juros_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "desconto" => $lancto['desconto'] == 0 ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_desconto,
                            "conta_debito" => $conta_desconto_debito,
                            "conta_credito" => $conta_desconto_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['desconto']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad($desconto_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "IRRF" => (empty($lancto['IR']) || $lancto['IR'] == 0) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_ir,
                            "conta_debito" => $conta_ir_debito,
                            "conta_credito" => $conta_ir_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['IR']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('IRRF ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "PIS" => (empty($lancto['PIS']) || $lancto['PIS'] == 0) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_pis,
                            "conta_debito" => $conta_pis_debito,
                            "conta_credito" => $conta_pis_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['PIS']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('PIS ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "COFINS" => (empty($lancto['COFINS']) || $lancto['COFINS'] == 0) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_cofins,
                            "conta_debito" => $conta_cofins_debito,
                            "conta_credito" => $conta_cofins_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['COFINS']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('COFINS ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "CSLL" => (empty($lancto['CSLL']) || $lancto['CSLL'] == 0) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_csll,
                            "conta_debito" => $conta_csll_debito,
                            "conta_credito" => $conta_csll_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['CSLL']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('CSLL ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "PCC" => (empty($lancto['PCC']) || $lancto['PCC'] == 0) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_pcc,
                            "conta_debito" => $conta_pcc_debito,
                            "conta_credito" => $conta_pcc_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['PCC']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('PCC ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "ISS" => (empty($lancto['ISSQN']) || $lancto['ISSQN'] == 0) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_iss,
                            "conta_debito" => $conta_issqn_debito,
                            "conta_credito" => $conta_issqn_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['ISSQN']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('ISS ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "INSS" => (empty($lancto['INSS']) || $lancto['INSS'] == 0 ) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_inss,
                            "conta_debito" => $conta_inss_debito,
                            "conta_credito" => $conta_inss_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['INSS']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('INSS ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "outras_tarifas" => (empty($lancto['outras_tarifas']) || $lancto['outras_tarifas'] == 0) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_outras_tarifas,
                            "conta_debito" => $conta_outras_tarifas_debito,
                            "conta_credito" => $conta_outras_tarifas_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['outras_tarifas']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('OUTRAS TARIFAS ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                        "encargos" => (empty($lancto['encargos']) || $lancto['encargos'] == 0) ? null : [
                            "cod_lancamento" => "001",
                            "tipo" => $tipo_lancamento_encargos,
                            "conta_debito" => $conta_encargos_debito,
                            "conta_credito" => $conta_encargos_credito,
                            "brancos_1" => '  ',
                            "valor" => StringHelper::money($lancto['encargos']),
                            "brancos_2" => '  ',
                            "data" => \DateTime::createFromFormat('d/m/Y', $lancto['data_lancamento'])->format('dmY'),
                            "historico" => StringHelper::stringPad('ENCARGOS ' . $retencoes_historico, 100, 'right', ' '),
                            "sequencia_lote" => StringHelper::stringPad(++$sequencia_lote, 3, 'left', '0'),
                            "sequencia_lancamento" => StringHelper::stringPad(++$cont, 4, 'left', '0'),
                        ],
                    ]
                ];
            }
            $sequencia_lote = 0;
        }

        /*$html = <<<HTML
<table width="100%" border="1" cellpadding="0" cellspacing="1">
HTML;

        foreach ($totalConta as $cc => $conta) {
            $tamanhoDebito = isset($conta['debito']) ? count($conta['debito']) : 0;
            $tamanhoCredito = isset($conta['credito']) ? count($conta['credito']) : 0;
            $html .= "<tr><td align='center' colspan='2'>{$cc}</td></tr>";
            $html .= "<tr><td>Débito</td><td>Crédito</td></tr>";
            if($tamanhoDebito > $tamanhoCredito) {
                foreach ($conta['debito'] as $key => $valor) {
                    $creditoTd = isset($conta['credito'][$key]) ? "<td>{$conta['credito'][$key]}</td>" : "<td></td>";
                    $html .= "<tr><td>{$valor}</td>{$creditoTd}</tr>";
                }
            } elseif($tamanhoDebito < $tamanhoCredito) {
                foreach ($conta['credito'] as $key => $valor) {
                    $debitoTd = isset($conta['debito'][$key]) ? "<td>{$conta['debito'][$key]}</td>" : "<td></td>";
                    $html .= "<tr>{$debitoTd}<td>{$valor}</td></tr>";
                }
            }
            $html .= "<tr><td colspan='2'>&nbsp;</td></tr>";
        }

        $html .= <<<HTML
</table>
HTML;
        echo $html;*/

        //dd($totalRecebimentos, $totalDesembolsos, $totalConta);
    }
}
