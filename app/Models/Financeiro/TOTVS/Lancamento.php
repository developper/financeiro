<?php
namespace App\Models\Financeiro\TOTVS;

use \App\Models\AbstractModel;
use App\Models\Financeiro\TipoDocumentoFinanceiro;
use \App\Models\Financeiro\TransferenciaBancaria as Transferencias;
use \App\Models\Financeiro\TarifaImposto;

class Lancamento extends AbstractModel
{
    public static function getCentroResultadosById ($tipo, $valor, $id)
    {
        return parent::get (self::getCentroResultado ($tipo, $valor, $id));
    }

    public static function getCentroResultado ($tipo, $valor, $id = null)
    {
        return <<<SQL
SELECT
	r.ID,
	e.centro_custo_dominio AS centro_custo_debito,
	(
		SELECT
			e1.centro_custo_dominio
		FROM
			rateio AS r1
		INNER JOIN parcelas AS p1 ON (p1.idNota = r1.NOTA_ID)
		INNER JOIN contas_bancarias AS cb1 ON (p1.origem = cb1.ID)
		INNER JOIN empresas AS e1 ON (e1.id = cb1.UR)
		WHERE
			r1.NOTA_ID = {$id}
		GROUP BY
			e1.centro_custo_dominio
	) AS centro_custo_credito,
	{$valor} AS valor_lancamento,
	r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN parcelas AS p ON (r.NOTA_ID = p.idNota)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
WHERE
	r.NOTA_ID = {$id}
GROUP BY
	r.ID
SQL;
    }

    public function getLancamentosNotasTodos ($periodo, $type = "inclusao")
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type == 'baixa') {
        $sql = <<<SQL
SELECT
IF (
	n.tipo = 0,
	CONCAT(
		'REC. DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'PG. DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS debito_historico,
 /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO CREDITO SERA COMPOSTO
 * POR UM PREFIXO 'REC. DOC.' + ID DA NOTA + NOME DO CLIENTE
 * SENAO E 1 (DESEMBOLSO), LOGO, SERA POR UM PREFIXO 'PG. DOC.' + ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
 */
IF (
	n.tipo = 0,
	CONCAT(
		'REC. DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'PG. DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS credito_historico,
 /* SE O CAMPO JUROS/MULTA NAO FOR VAZIO, PROCEDE:
 * O HISTORICO DO JUROS/MULTA SERA COMPOSTO
 * POR UM PREFIXO 'Juros: ' + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
 * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
 */
IF (
	p.JurosMulta != 0,
	CONCAT(
		'VL. REF. JUROS DOC ',
		n.codigo,
		' ',
        IF (
            n.tipo = 0,
            f.nome,
            f.razaoSocial
        )
    ),
	''
) AS juros_historico,
 /* SE O CAMPO DESCONTO NAO FOR VAZIO, PROCEDE:
 * O HISTORICO DO DESCONTO SERA COMPOSTO
 * POR UM PREFIXO 'Juros: ' + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
 * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
 */
IF (
	p.desconto != 0,
	CONCAT(
		'VL. REF. DESCONTO DOC ',
		n.codigo,
		' ',
        IF (
            n.tipo = 0,
            f.nome,
            f.razaoSocial
        )
	),
	''
) AS desconto_historico,
 /* O HISTORICO DAS RETENCOES SERA COMPOSTO
 * POR TIPO DE RETENÇÃO + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
 * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
 */
IF (
	n.tipo = 0,
	CONCAT(n.codigo, ' ', f.nome),
	CONCAT(n.codigo, ' ', f.razaoSocial)
) AS retencoes_historico,
 (
	p.valor
) AS valor,
 p.JurosMulta,
 p.desconto,
 p.OUTROS_ACRESCIMOS AS outras_tarifas,
 p.encargos,
 n.tipo,
 n.descricao,
 'n' AS origem,
 'n' AS destino,
 'n' AS conta_tarifa,
 p.id AS idNotas,
 DATE_FORMAT(
	p.DATA_CONCILIADO,
	'%d/%m/%Y'
) AS data_lancamento,
 'nota' AS lancamento_comum,
 n.nota_aglutinacao,
 IF(n.nota_adiantamento = 'S', 's', 'n') AS nota_adiantamento,
 'n' AS nota_baixa,
 p.CONCILIADO AS conciliado
FROM
parcelas as p
INNER JOIN notas AS n ON n.idNotas = p.idNota
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN contas_bancarias AS cb ON cb.id = p.origem
INNER JOIN empresas AS e ON (e.id = n.empresa)
WHERE
p.status = 'Processada'
AND n.status != 'Cancelada'
AND (p.nota_id_fatura_aglutinacao is null or p.nota_id_fatura_aglutinacao = 0) 
AND p.DATA_CONCILIADO {$interval}
AND p.CONCILIADO = 'S'

UNION       
SELECT
    CONCAT(
        'TRANSFERENCIA ENTRE CONTAS ',
        (
            SELECT
                conta
            FROM
                indexed_demonstrativo_contas_bancarias AS idcb
            WHERE
                idcb.ID = tb.CONTA_ORIGEM
        ),
        ' / ',
        (
            SELECT
                conta
            FROM
                indexed_demonstrativo_contas_bancarias AS idcb
            WHERE
                idcb.ID = tb.CONTA_DESTINO
        )
    ) AS debito_historico,
    CONCAT(
        'TRANSFERENCIA ENTRE CONTAS ',
        (
            SELECT
                conta
            FROM
                indexed_demonstrativo_contas_bancarias AS idcb
            WHERE
                idcb.ID = tb.CONTA_ORIGEM
        ),
        ' / ',
        (
            SELECT
                conta
            FROM
                indexed_demonstrativo_contas_bancarias AS idcb
            WHERE
                idcb.ID = tb.CONTA_DESTINO
        )
    ) AS credito_historico,
    '' AS juros_historico,
    '' AS desconto_historico,
    '' AS retencoes_historico,
    tb.VALOR AS valor,
    0 AS JurosMulta,
    0 AS desconto,
    0 AS outras_tarifas,
    0 AS encargos,
    0 AS tipo,
    '' AS descricao,
    tb.CONTA_ORIGEM AS origem,
    tb.CONTA_DESTINO AS destino,
    't' AS conta_tarifa,
    concat('t',tb.ID) AS idNotas,
    DATE_FORMAT(
        tb.DATA_TRANSFERENCIA,
        '%d/%m/%Y'
    ) AS data_lancamento,
    'transf' AS lancamento_comum,
    'n' AS nota_aglutinacao,
    'n' AS nota_adiantamento,
    'n' AS nota_baixa,
    'S' AS conciliado
FROM
    transferencia_bancaria AS tb
INNER JOIN contas_bancarias AS cb1 ON (tb.CONTA_ORIGEM = cb1.ID)
INNER JOIN contas_bancarias AS cb2 ON (tb.CONTA_DESTINO = cb2.ID)
LEFT JOIN plano_contas_contabil AS pcc1 ON (cb1.IPCC = pcc1.ID)
LEFT JOIN plano_contas_contabil AS pcc2 ON (cb2.IPCC = pcc2.ID)
WHERE
    tb.CONCILIADO_DESTINO = 'S'
AND tb.CONCILIADO_ORIGEM = 'S'
AND tb.DATA_CONCILIADO_ORIGEM {$interval}

UNION
  SELECT
    IF (
        n.tipo = 0,
        'DEVOLUÇÃO DE REC. ANTECIPADO',
        'DEVOLUÇÃO DE PG. ANTECIPADO'
    ) AS debito_historico,
    IF (
        n.tipo = 0,
        'DEVOLUÇÃO DE REC. ANTECIPADO',
        'DEVOLUÇÃO DE PG. ANTECIPADO'
    ) AS credito_historico,
    '' AS juros_historico,
    '' AS desconto_historico,
    '' AS retencoes_historico,
     nb.valor AS valor,
     '0' AS JurosMulta,
     '0' AS desconto,
     '0' AS outras_tarifas,
     '0' AS encargos,
     nb.tipo_movimentacao AS tipo,
     n.descricao,
     'n' AS origem,
     'n' AS destino,
     'n' AS conta_tarifa,
     nb.id AS idNotas,
     DATE_FORMAT(
        nb.data_conciliado,
        '%d/%m/%Y'
    ) AS data_lancamento,
    'baixa' AS lancamento_comum,
    'n' AS nota_aglutinacao,
    'n' AS nota_adiantamento,
    's' AS nota_baixa,
    'S' AS conciliado
        FROM
            nota_baixas as nb      
              INNER JOIN notas AS n ON nb.nota_id = n.idNotas
                INNER JOIN contas_bancarias AS cb ON cb.id = nb.conta_bancaria_id
                INNER JOIN plano_contas AS pc ON (nb.plano_conta_id = pc.ID)
        WHERE
            nb.canceled_by is null
            AND nb.data_conciliado {$interval}
        UNION
  SELECT
    CONCAT(it.tipo, ' ', it.item) AS debito_historico,
    CONCAT(it.tipo, ' ', it.item) AS credito_historico,
    '' AS juros_historico,
    '' AS desconto_historico,
    '' AS retencoes_historico,
    itl.valor AS valor,
    0 AS JurosMulta,
    0 AS desconto,
    0 AS outras_tarifas,
    0 AS encargos,
    IF(it.debito_credito = 'DEBITO', 1, 0) AS tipo,
    '' AS descricao,
    'i' AS origem,
    'i' AS destino,
    itl.conta AS conta_tarifa,
    concat('i',itl.id) AS idNotas,
    DATE_FORMAT(itl.`data`, '%d/%m/%Y') AS data_lancamento,
    'n' AS lancamento_comum,
    'n' AS nota_aglutinacao,
    'n' AS nota_adiantamento,
    'n' AS nota_baixa,
    'S' AS conciliado
  FROM
    impostos_tarifas_lancto AS itl
    INNER JOIN impostos_tarifas AS it ON (it.id = itl.item)
    INNER JOIN contas_bancarias AS cb ON (cb.id = itl.conta)
    INNER JOIN plano_contas AS pc ON (pc.COD_N4 = it.ipcf)
    LEFT JOIN plano_contas_contabil AS pcc ON (pcc.COD_N5 = pc.COD_IPCC)
  WHERE
    itl.data {$interval}
  UNION
  SELECT
    IF (
	n.tipo = 0,
	CONCAT(
		'ESTORNO DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'ESTORNO DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS debito_historico,
 /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO CREDITO SERA COMPOSTO
 * POR UM PREFIXO 'REC. DOC.' + ID DA NOTA + NOME DO CLIENTE
 * SENAO E 1 (DESEMBOLSO), LOGO, SERA POR UM PREFIXO 'PG. DOC.' + ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
 */
IF (
	n.tipo = 0,
	CONCAT(
		'ESTORNO DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'ESTORNO DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS credito_historico,
'' AS juros_historico,
'' AS desconto_historico,
IF (
	n.tipo = 0,
	CONCAT(n.codigo, ' ', f.nome),
	CONCAT(n.codigo, ' ', f.razaoSocial)
) AS retencoes_historico,
 p.valor,
 p.JurosMulta,
 p.desconto,
 p.OUTROS_ACRESCIMOS AS outras_tarifas,
 p.encargos,
 n.tipo,
 n.descricao,
 'n' AS origem,
 'n' AS destino,
 'n' AS conta_tarifa,
 p.id AS idNotas,
 DATE_FORMAT(
	ep.data_conciliada,
	'%d/%m/%Y'
) AS data_lancamento,
 'estorno' AS lancamento_comum,
 'n' AS nota_aglutinacao,
 'n' AS nota_adiantamento,
 'n' AS nota_baixa,
 p.CONCILIADO AS conciliado
  FROM
    estorno_parcela ep
    INNER JOIN parcelas AS p ON p.id = ep.parcela_id
    INNER JOIN notas AS n ON n.idNotas = p.idNota
    INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
    INNER JOIN contas_bancarias AS cb ON cb.id = p.origem
    INNER JOIN empresas AS e ON (e.id = n.empresa)
  WHERE
    ep.data_conciliada {$interval}
    
ORDER BY
	data_lancamento
SQL;
        } else {
            $sql = <<<SQL
SELECT
	/* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO DEBITO SERA COMPOSTO 
	   * PELA ID DA NOTA + NOME DO CLIENTE * SENAO E 1 (DESEMBOLSO), LOGO, 
	   SERA PELO ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR */
IF (
	n.tipo = 0,
	CONCAT(
		'VL. REF. DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'VL. REF. DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS debito_historico,
 /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO CREDITO SERA COMPOSTO 
    * POR UM PREFIXO 'REC. DOC.' + ID DA NOTA + NOME DO CLIENTE * SENAO E 1 (DESEMBOLSO), LOGO, 
    SERA POR UM PREFIXO 'PG. DOC.' + ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR */
IF (
	n.tipo = 0,
	CONCAT(
		'VL. REF. DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'VL. REF. DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS credito_historico,
 /* O HISTORICO DAS RETENCOES SERA COMPOSTO 
    * POR TIPO RETENÇÃO + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO) 
    * OU RAZAO SOCIAL DO FORNECEDOR SE O
    TIPO FOR 1 (DESEMBOLSO) */
IF (
	n.tipo = 0,
	CONCAT(n.codigo, ' ', f.nome),
	CONCAT(n.codigo, ' ', f.razaoSocial)
) AS retencoes_historico,
 n.tipo,
 n.VAL_PRODUTOS AS valor,
 0 AS JurosMulta,
 0 AS desconto,
 0 AS outras_tarifas,
 0 AS encargos,
 n.VAL_PRODUTOS AS valor_bruto,
 n.descricao,
 'n' AS origem,
 'n' AS destino,
 'n' AS conta_tarifa,
 n.idNotas,
 n.IR,
 n.PIS,
 n.COFINS,
 n.CSLL,
 n.PCC,
 n.ISSQN,
 n.INSS,
 n.FRETE,
 n.DES_ACESSORIAS,
 n.DESCONTO_BONIFICACAO,
 n.SEGURO,
 n.IPI,
 DATE_FORMAT(n.dataEntrada, '%d/%m/%Y') AS data_lancamento,
 'nota' AS lancamento_comum,
 'n' AS nota_aglutinacao,
 'n' AS nota_adiantamento,
 'n' AS nota_baixa,
 'N' AS conciliado
FROM
	notas AS n
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN empresas AS e ON (e.id = n.empresa)
INNER JOIN rateio AS r ON r.NOTA_ID = n.idNotas
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
WHERE
	n.dataEntrada {$interval}
AND n.`status` != 'Cancelada'
AND pc.PROVISIONA = 's'
AND n.nota_aglutinacao = 'n'
AND n.nota_adiantamento = 'N'
AND n.NOTA_REFERENCIA = 0
GROUP BY idNotas
ORDER BY data_lancamento
SQL;
        }

        return parent::get($sql);
    }

    public function getLancamentosNotas ($periodo, $type = "inclusao")
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "inclusao") {
            # REGRAS UTILIZADAS QUANDO O TIPO DE ARQUIVO ESCOLHIDO FOR INCLUSÃO

            $sql = <<<SQL
SELECT
	/* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO DEBITO SERA COMPOSTO 
	   * PELA ID DA NOTA + NOME DO CLIENTE * SENAO E 1 (DESEMBOLSO), LOGO, 
	   SERA PELO ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR */
IF (
	n.tipo = 0,
	CONCAT(
		'VL. REF. DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'VL. REF. DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS debito_historico,
 /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO CREDITO SERA COMPOSTO 
    * POR UM PREFIXO 'REC. DOC.' + ID DA NOTA + NOME DO CLIENTE * SENAO E 1 (DESEMBOLSO), LOGO, 
    SERA POR UM PREFIXO 'PG. DOC.' + ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR */
IF (
	n.tipo = 0,
	CONCAT(
		'VL. REF. DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'VL. REF. DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS credito_historico,
 /* O HISTORICO DAS RETENCOES SERA COMPOSTO 
    * POR TIPO RETENÇÃO + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO) 
    * OU RAZAO SOCIAL DO FORNECEDOR SE O
    TIPO FOR 1 (DESEMBOLSO) */
IF (
	n.tipo = 0,
	CONCAT(n.codigo, ' ', f.nome),
	CONCAT(n.codigo, ' ', f.razaoSocial)
) AS retencoes_historico,
 n.tipo,
 n.valor,
 0 AS JurosMulta,
 0 AS desconto,
 0 AS outras_tarifas,
 0 AS encargos,
 n.VAL_PRODUTOS AS valor_bruto,
 n.descricao,
 'n' AS origem,
 'n' AS destino,
 'n' AS conta_tarifa,
 n.idNotas,
 n.IR,
 n.PIS,
 n.COFINS,
 n.CSLL,
 n.PCC,
 n.ISSQN,
 n.INSS,
 DATE_FORMAT(n.dataEntrada, '%d/%m/%Y') AS data_lancamento,
 's' AS lancamento_comum,
 'n' AS nota_aglutinacao,
 'n' AS nota_adiantamento,
 'n' AS nota_compensacao,
 'n' AS nota_baixa,
 'N' AS conciliado,
 e.centro_custo_dominio
FROM
	notas AS n
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN rateio AS r ON r.NOTA_ID = n.idNotas
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN empresas AS e ON (e.id = n.empresa)
WHERE
	n.dataEntrada {$interval}
AND n.`status` != 'Cancelada'
AND pc.PROVISIONA = 's'
AND n.nota_aglutinacao = 'n'
AND n.nota_adiantamento = 'N'
-- AND pc.ID NOT IN (15, 140, 164, 165, 208, 209, 210, 147, 14, 139, 142, 162, 163, 106)
GROUP BY
	r.NOTA_ID
ORDER BY
	data_lancamento
SQL;
        } else {

            # REGRAS UTILIZADAS QUANDO O TIPO DE ARQUIVO ESCOLHIDO FOR BAIXA
            $sql = <<<SQL
SELECT
	/* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO DEBITO SERA COMPOSTO
 * PELA ID DA NOTA + NOME DO CLIENTE
 * SENAO E 1 (DESEMBOLSO), LOGO, SERA PELO ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
 */
IF (
	n.tipo = 0,
	CONCAT(
		'REC. DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'PG. DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS debito_historico,
 /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO CREDITO SERA COMPOSTO
 * POR UM PREFIXO 'REC. DOC.' + ID DA NOTA + NOME DO CLIENTE
 * SENAO E 1 (DESEMBOLSO), LOGO, SERA POR UM PREFIXO 'PG. DOC.' + ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
 */
IF (
	n.tipo = 0,
	CONCAT(
		'REC. DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'PG. DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS credito_historico,
 /* SE O CAMPO JUROS/MULTA NAO FOR VAZIO, PROCEDE:
 * O HISTORICO DO JUROS/MULTA SERA COMPOSTO
 * POR UM PREFIXO 'Juros: ' + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
 * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
 */
IF (
	p.JurosMulta != 0,
	CONCAT(
		'VL. REF. JUROS DOC ',
		n.codigo,
		' ',
        IF (
            n.tipo = 0,
            f.nome,
            f.razaoSocial
        )
    ),
	''
) AS juros_historico,
 /* SE O CAMPO DESCONTO NAO FOR VAZIO, PROCEDE:
 * O HISTORICO DO DESCONTO SERA COMPOSTO
 * POR UM PREFIXO 'Juros: ' + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
 * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
 */
IF (
	p.desconto != 0,
	CONCAT(
		'VL. REF. DESCONTO DOC ',
		n.codigo,
		' ',
        IF (
            n.tipo = 0,
            f.nome,
            f.razaoSocial
        )
	),
	''
) AS desconto_historico,
 /* O HISTORICO DAS RETENCOES SERA COMPOSTO
 * POR TIPO DE RETENÇÃO + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
 * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
 */
IF (
	n.tipo = 0,
	CONCAT(n.codigo, ' ', f.nome),
	CONCAT(n.codigo, ' ', f.razaoSocial)
) AS retencoes_historico,
 (
	p.valor
) AS valor,
 p.JurosMulta,
 p.desconto,
 p.OUTROS_ACRESCIMOS AS outras_tarifas,
 p.encargos,
 n.tipo,
 n.descricao,
 'n' AS origem,
 'n' AS destino,
 'n' AS conta_tarifa,
 p.id AS idNotas,
 DATE_FORMAT(
	p.DATA_CONCILIADO,
	'%d/%m/%Y'
) AS data_lancamento,
 's' AS lancamento_comum,
 'n' AS nota_aglutinacao,
 'n' AS nota_adiantamento,
 'n' AS nota_compensacao,
 'n' AS nota_baixa,
 p.CONCILIADO AS conciliado,
 e.centro_custo_dominio
FROM
	parcelas AS p
INNER JOIN notas AS n ON n.idNotas = p.idNota
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN rateio AS r ON r.NOTA_ID = n.idNotas
INNER JOIN contas_bancarias AS cb ON cb.id = p.origem
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN empresas AS e ON (e.id = n.empresa)
WHERE
	p.DATA_CONCILIADO {$interval}
AND p.`status` = 'Processada'
AND n.status != 'Cancelada'
AND p.CONCILIADO = 'S'
AND n.nota_aglutinacao = 'n'
AND n.nota_adiantamento = 'N'
-- AND pc.ID NOT IN (15, 140, 164, 165, 208, 209, 210, 147, 14, 139, 142, 162, 163, 106)
GROUP BY
	r.NOTA_ID
ORDER BY
    data_lancamento
SQL;
        }

        return parent::get($sql);
    }

    public static function getLancamentosTransferenciaBancaria($periodo, $type = 'baixa')
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "baixa") {
            $sql = <<<SQL
SELECT
    CONCAT(
        'TRANSFERENCIA ENTRE CONTAS ',
        (
            SELECT
                conta
            FROM
                indexed_demonstrativo_contas_bancarias AS idcb
            WHERE
                idcb.ID = tb.CONTA_ORIGEM
        ),
        ' / ',
        (
            SELECT
                conta
            FROM
                indexed_demonstrativo_contas_bancarias AS idcb
            WHERE
                idcb.ID = tb.CONTA_DESTINO
        )
    ) AS debito_historico,
    CONCAT(
        'TRANSFERENCIA ENTRE CONTAS ',
        (
            SELECT
                conta
            FROM
                indexed_demonstrativo_contas_bancarias AS idcb
            WHERE
                idcb.ID = tb.CONTA_ORIGEM
        ),
        ' / ',
        (
            SELECT
                conta
            FROM
                indexed_demonstrativo_contas_bancarias AS idcb
            WHERE
                idcb.ID = tb.CONTA_DESTINO
        )
    ) AS credito_historico,
    '' AS juros_historico,
    '' AS desconto_historico,
    '' AS retencoes_historico,
    tb.VALOR AS valor,
    0 AS JurosMulta,
    0 AS desconto,
    0 AS outras_tarifas,
    0 AS encargos,
    0 AS tipo,
    '' AS descricao,
    tb.CONTA_ORIGEM AS origem,
    tb.CONTA_DESTINO AS destino,
    't' AS conta_tarifa,
    concat('t',tb.ID) AS idNotas,
    DATE_FORMAT(
        tb.DATA_TRANSFERENCIA,
        '%d/%m/%Y'
    ) AS data_lancamento,
    's' AS lancamento_comum,
    'n' AS nota_aglutinacao,
    'n' AS nota_adiantamento,
    'n' AS nota_compensacao,
    'n' AS nota_baixa,
    'S' AS conciliado
FROM
    transferencia_bancaria AS tb
INNER JOIN contas_bancarias AS cb1 ON (tb.CONTA_ORIGEM = cb1.ID)
INNER JOIN contas_bancarias AS cb2 ON (tb.CONTA_DESTINO = cb2.ID)
LEFT JOIN plano_contas_contabil AS pcc1 ON (cb1.IPCC = pcc1.ID)
LEFT JOIN plano_contas_contabil AS pcc2 ON (cb2.IPCC = pcc2.ID)
WHERE
    tb.CONCILIADO_DESTINO = 'S'
AND tb.CONCILIADO_ORIGEM = 'S'
AND tb.DATA_CONCILIADO_ORIGEM {$interval}
ORDER BY
  data_lancamento
SQL;
            return parent::get($sql, 'Assoc');
        }
        return [];
    }

    public static function getLancamentosTarifasBancaria($periodo, $type = 'baixa')
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "baixa") {
            $sql = <<<SQL
SELECT
    CONCAT(it.tipo, ' ', it.item) AS debito_historico,
    CONCAT(it.tipo, ' ', it.item) AS credito_historico,
    '' AS juros_historico,
    '' AS desconto_historico,
    '' AS retencoes_historico,
    itl.valor AS valor,
    0 AS JurosMulta,
    0 AS desconto,
    0 AS outras_tarifas,
    0 AS encargos,
    IF(it.tipo = 'DEBITO', 1, 0) AS tipo,
    '' AS descricao,
    'i' AS origem,
    'i' AS destino,
    itl.conta AS conta_tarifa,
    concat('i',itl.id) AS idNotas,
    DATE_FORMAT(itl.`data`, '%d/%m/%Y') AS data_lancamento,
    's' AS lancamento_comum,
    'n' AS nota_aglutinacao,
    'n' AS nota_adiantamento,
    'n' AS nota_compensacao,
    'n' AS nota_baixa,
    'S' AS conciliado
FROM
impostos_tarifas_lancto AS itl
INNER JOIN impostos_tarifas AS it ON (it.id = itl.item)
INNER JOIN contas_bancarias AS cb ON (cb.id = itl.conta)
INNER JOIN plano_contas AS pc ON (pc.COD_N4 = it.ipcf)
LEFT JOIN plano_contas_contabil AS pcc ON (pcc.COD_N5 = pc.COD_IPCC)
WHERE
itl.`data` {$interval}
SQL;
            return parent::get($sql, 'Assoc');
        }
        return [];
    }

    public static function getLancamentosEstornosParcelas($periodo, $type = 'baixa')
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "baixa") {
            $sql = <<<SQL
SELECT
	/* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO DEBITO SERA COMPOSTO
 * PELA ID DA NOTA + NOME DO CLIENTE
 * SENAO E 1 (DESEMBOLSO), LOGO, SERA PELO ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
 */
IF (
	n.tipo = 0,
	CONCAT(
		'ESTORNO DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'ESTORNO DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS debito_historico,
 /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO CREDITO SERA COMPOSTO
 * POR UM PREFIXO 'REC. DOC.' + ID DA NOTA + NOME DO CLIENTE
 * SENAO E 1 (DESEMBOLSO), LOGO, SERA POR UM PREFIXO 'PG. DOC.' + ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
 */
IF (
	n.tipo = 0,
	CONCAT(
		'ESTORNO DOC. ',
		n.codigo,
		' ',
		f.nome
	),
	CONCAT(
		'ESTORNO DOC. ',
		n.codigo,
		' ',
		f.razaoSocial
	)
) AS credito_historico,
'' AS juros_historico,
'' AS desconto_historico,
IF (
	n.tipo = 0,
	CONCAT(n.codigo, ' ', f.nome),
	CONCAT(n.codigo, ' ', f.razaoSocial)
) AS retencoes_historico,
 p. valor,
 p.JurosMulta,
 p.desconto,
 p.OUTROS_ACRESCIMOS AS outras_tarifas,
 p.encargos,
 n.tipo,
 n.descricao,
 'n' AS origem,
 'n' AS destino,
 'n' AS conta_tarifa,
 p.id AS idNotas,
 DATE_FORMAT(
	ep.data_conciliada,
	'%d/%m/%Y'
) AS data_lancamento,
 'estorno' AS lancamento_comum,
 'n' AS nota_aglutinacao,
 'n' AS nota_adiantamento,
 'n' AS nota_compensacao,
 'n' AS nota_baixa,
 p.CONCILIADO AS conciliado,
 e.centro_custo_dominio
FROM
  estorno_parcela AS ep
INNER JOIN parcelas AS p ON p.id = ep.parcela_id
INNER JOIN notas AS n ON n.idNotas = p.idNota
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN rateio AS r ON r.NOTA_ID = n.idNotas
INNER JOIN contas_bancarias AS cb ON cb.id = p.origem
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN empresas AS e ON (e.id = n.empresa)
WHERE
	ep.data_conciliada {$interval}
AND n.nota_aglutinacao = 'n'
AND n.nota_adiantamento = 'N'
-- AND pc.ID NOT IN (15, 140, 164, 165, 208, 209, 210, 147, 14, 139, 142, 162, 163, 106)
GROUP BY
    ep.parcela_id
ORDER BY
    data_lancamento
SQL;
            return parent::get($sql, 'Assoc');
        }
        return [];
    }

    public static function getLancamentosAntecipacao($periodo, $type = 'baixa')
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "baixa") {
            $sql = <<<SQL
SELECT
    /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO DEBITO SERA COMPOSTO
     * PELA ID DA NOTA + NOME DO CLIENTE
     * SENAO E 1 (DESEMBOLSO), LOGO, SERA PELO ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
     */
    IF (
        n.tipo = 0,
        CONCAT(
            'NF DE REC. ANTECIPADO DE ',
            n.codigo,
            ' ',
            f.nome
        ),
        CONCAT(
            'NF DE PG. ANTECIPADO A ',
            n.codigo,
            ' ',
            f.razaoSocial
        )
    ) AS debito_historico,
     /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO CREDITO SERA COMPOSTO
     * POR UM PREFIXO 'REC. DOC.' + ID DA NOTA + NOME DO CLIENTE
     * SENAO E 1 (DESEMBOLSO), LOGO, SERA POR UM PREFIXO 'PG. DOC.' + ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
     */
    IF (
        n.tipo = 0,
        CONCAT(
            'NF DE REC. ANTECIPADO DE ',
            n.codigo,
            ' ',
            f.nome
        ),
        CONCAT(
            'NF DE PG. ANTECIPADO A ',
            n.codigo,
            ' ',
            f.razaoSocial
        )
    ) AS credito_historico,
     /* SE O CAMPO JUROS/MULTA NAO FOR VAZIO, PROCEDE:
     * O HISTORICO DO JUROS/MULTA SERA COMPOSTO
     * POR UM PREFIXO 'Juros: ' + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
     * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
     */
    IF (
        p.JurosMulta != 0,
        CONCAT(
            'VL. REF. JUROS DOC ',
            n.codigo,
            ' ',
    
        IF (
            n.tipo = 0,
            f.nome,
            f.razaoSocial
        )
        ),
        ''
    ) AS juros_historico,
     /* SE O CAMPO DESCONTO NAO FOR VAZIO, PROCEDE:
     * O HISTORICO DO DESCONTO SERA COMPOSTO
     * POR UM PREFIXO 'Juros: ' + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
     * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
     */
    IF (
        p.desconto != 0,
        CONCAT(
            'VL. REF. DESCONTO DOC ',
            n.codigo,
            ' ',
    
        IF (
            n.tipo = 0,
            f.nome,
            f.razaoSocial
        )
        ),
        ''
    ) AS desconto_historico,
     /* O HISTORICO DAS RETENCOES SERA COMPOSTO
     * POR TIPO DE RETENÇÃO + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
     * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
     */
    IF (
        n.tipo = 0,
        CONCAT(n.codigo, ' ', f.nome),
        CONCAT(n.codigo, ' ', f.razaoSocial)
    ) AS retencoes_historico,
     (
        p.valor
    ) AS valor,
     p.JurosMulta,
     p.desconto,
     p.OUTROS_ACRESCIMOS AS outras_tarifas,
     p.encargos,
     n.tipo,
     n.descricao,
     'n' AS origem,
     'n' AS destino,
     'n' AS conta_tarifa,
     p.id AS idNotas,
     DATE_FORMAT(
        p.DATA_CONCILIADO,
        '%d/%m/%Y'
    ) AS data_lancamento,
    'n' AS lancamento_comum,
    'n' AS nota_aglutinacao,
    's' AS nota_adiantamento,
    'n' AS nota_compensacao,
    'n' AS nota_baixa,
    p.CONCILIADO AS conciliado
FROM
    parcelas AS p
INNER JOIN notas AS n ON n.idNotas = p.idNota
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN rateio AS r ON r.NOTA_ID = n.idNotas
INNER JOIN contas_bancarias AS cb ON cb.id = p.origem
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
WHERE
    p.DATA_PAGAMENTO {$interval}
AND n.`status` != 'Cancelada'
AND n.nota_aglutinacao = 'n'
AND n.nota_adiantamento = 'S'
ORDER BY 
    data_lancamento
SQL;
            return parent::get($sql, 'Assoc');
        }
        return [];
    }

    public static function getLancamentosNFCompensacao($periodo, $type = 'baixa')
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "baixa") {
            $sql = <<<SQL
SELECT
    /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO DEBITO SERA COMPOSTO
     * PELA ID DA NOTA + NOME DO CLIENTE
     * SENAO E 1 (DESEMBOLSO), LOGO, SERA PELO ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
     */
    IF (
        n.tipo = 0,
        CONCAT(
            'REC. ANTECIPADO DE ',
            n.codigo,
            ' ',
            f.nome
        ),
        CONCAT(
            'PG. ANTECIPADO A ',
            n.codigo,
            ' ',
            f.razaoSocial
        )
    ) AS debito_historico,
     /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO CREDITO SERA COMPOSTO
     * POR UM PREFIXO 'REC. DOC.' + ID DA NOTA + NOME DO CLIENTE
     * SENAO E 1 (DESEMBOLSO), LOGO, SERA POR UM PREFIXO 'PG. DOC.' + ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
     */
    IF (
        n.tipo = 0,
        CONCAT(
            'REC. ANTECIPADO DE ',
            n.codigo,
            ' ',
            f.nome
        ),
        CONCAT(
            'PG. ANTECIPADO A ',
            n.codigo,
            ' ',
            f.razaoSocial
        )
    ) AS credito_historico,
     /* SE O CAMPO JUROS/MULTA NAO FOR VAZIO, PROCEDE:
     * O HISTORICO DO JUROS/MULTA SERA COMPOSTO
     * POR UM PREFIXO 'Juros: ' + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
     * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
     */
    IF (
        p.JurosMulta != 0,
        CONCAT(
            'VL. REF. JUROS DOC ',
            n.codigo,
            ' ',
    
        IF (
            n.tipo = 0,
            f.nome,
            f.razaoSocial
        )
        ),
        ''
    ) AS juros_historico,
     /* SE O CAMPO DESCONTO NAO FOR VAZIO, PROCEDE:
     * O HISTORICO DO DESCONTO SERA COMPOSTO
     * POR UM PREFIXO 'Juros: ' + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
     * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
     */
    IF (
        p.desconto != 0,
        CONCAT(
            'VL. REF. DESCONTO DOC ',
            n.codigo,
            ' ',
    
        IF (
            n.tipo = 0,
            f.nome,
            f.razaoSocial
        )
        ),
        ''
    ) AS desconto_historico,
     /* O HISTORICO DAS RETENCOES SERA COMPOSTO
     * POR TIPO DE RETENÇÃO + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
     * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
     */
    IF (
        n.tipo = 0,
        CONCAT(n.codigo, ' ', f.nome),
        CONCAT(n.codigo, ' ', f.razaoSocial)
    ) AS retencoes_historico,
     (
        p.valor
    ) AS valor,
     p.JurosMulta,
     p.desconto,
     p.OUTROS_ACRESCIMOS AS outras_tarifas,
     p.encargos,
     n.tipo,
     n.descricao,
     'n' AS origem,
     'n' AS destino,
     'n' AS conta_tarifa,
     p.id AS idNotas,
     DATE_FORMAT(
        p.DATA_CONCILIADO,
        '%d/%m/%Y'
    ) AS data_lancamento,
    'n' AS lancamento_comum,
    'n' AS nota_aglutinacao,
    's' AS nota_adiantamento,
    'n' AS nota_compensacao,
    'n' AS nota_baixa,
    p.CONCILIADO AS conciliado
FROM
    parcelas AS p
INNER JOIN notas AS n ON n.idNotas = p.idNota
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN rateio AS r ON r.NOTA_ID = n.idNotas
INNER JOIN contas_bancarias AS cb ON cb.id = p.origem
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
WHERE
    p.DATA_CONCILIADO {$interval}
AND p.`status` = 'Processada'
AND n.`status` != 'Cancelada'
AND n.nota_aglutinacao = 'n'
AND n.nota_adiantamento = 'S'
ORDER BY 
    data_lancamento
SQL;
            return parent::get($sql, 'Assoc');
        }
        return [];
    }

    public static function getLancamentosCompensacao($periodo, $type = 'baixa')
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "baixa") {
            $sql = <<<SQL
SELECT
    IF (
        n.tipo = 0,
        'COMPENSACAO DE REC. ANTECIPADO',
        'COMPENSACAO DE PG. ANTECIPADO'
    ) AS debito_historico,
    IF (
        n.tipo = 0,
        'COMPENSACAO DE REC. ANTECIPADO',
        'COMPENSACAO DE PG. ANTECIPADO'
    ) AS credito_historico,
    IF (
        p.JurosMulta != 0,
        CONCAT(
                'VL. REF. JUROS DOC ',
                n.codigo,
                ' ',
                IF (
                            n.tipo = 0,
                            f.nome,
                            f.razaoSocial
                    )
            ),
            ''
        ) AS juros_historico,
    IF (
        p.desconto != 0,
        CONCAT(
            'VL. REF. DESCONTO DOC ',
            n.codigo,
            ' ',
            IF (
                        n.tipo = 0,
                        f.nome,
                        f.razaoSocial
                )
        ),
        ''
        ) AS desconto_historico,
    '' AS retencoes_historico,
    nc.valor AS valor,
    p.JurosMulta,
    p.desconto,
    p.OUTROS_ACRESCIMOS AS outras_tarifas,
    p.encargos,
    n.tipo,
    n.descricao,
    'n' AS origem,
    'n' AS destino,
    'n' AS conta_tarifa,
    p.id AS idNotas,
    DATE_FORMAT(
            nc.data_compensado,
            '%d/%m/%Y'
        ) AS data_lancamento,
    'compensacao' AS lancamento_comum,
    'n' AS nota_aglutinacao,
    'n' AS nota_adiantamento,
    'n' AS nota_baixa,
    'S' AS conciliado
FROM
    nota_compensacao AS nc
        INNER JOIN parcelas AS p ON (nc.parcela_id = p.id OR nc.parcela_pivo_id = p.id)
        INNER JOIN notas AS n ON n.idNotas = p.idNota
        INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
WHERE
    nc.data_compensado {$interval}
AND p.`status` = 'Processada'
AND n.tipo_documento_financeiro_id NOT IN (23, 26)
AND nc.deleted_by IS NULL
ORDER BY
    data_lancamento;
SQL;
            return parent::get($sql, 'Assoc');
        }

        return [];
    }

    public static function getLancamentosBaixasAntecipacao($periodo, $type = 'baixa')
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "baixa") {
            $sql = <<<SQL
SELECT
    IF (
        n.tipo = 0,
        'BAIXA DE REC. ANTECIPADO',
        'BAIXA DE PG. ANTECIPADO'
    ) AS debito_historico,
    IF (
        n.tipo = 0,
        'BAIXA DE REC. ANTECIPADO',
        'BAIXA DE PG. ANTECIPADO'
    ) AS credito_historico,
    '' AS juros_historico,
    '' AS desconto_historico,
    '' AS retencoes_historico,
     nb.valor AS valor,
     '0' AS JurosMulta,
     '0' AS desconto,
     '0' AS outras_tarifas,
     '0' AS encargos,
     nb.tipo_movimentacao,
     n.descricao,
     'n' AS origem,
     'n' AS destino,
     'n' AS conta_tarifa,
     nb.id AS idNotas,
     DATE_FORMAT(
        nb.data_conciliado,
        '%d/%m/%Y'
    ) AS data_lancamento,
    'n' AS lancamento_comum,
    'n' AS nota_aglutinacao,
    'n' AS nota_adiantamento,
    'n' AS nota_compensacao,
    's' AS nota_baixa,
    'S' AS conciliado
FROM
    nota_baixas AS nb
    INNER JOIN notas AS n ON nb.nota_id = n.idNotas
    INNER JOIN contas_bancarias AS cb ON cb.id = nb.conta_bancaria_id
    INNER JOIN plano_contas AS pc ON (nb.plano_conta_id = pc.ID)
WHERE
    nb.data_conciliado {$interval}
    AND n.`status` != 'Cancelada'
    AND nb.canceled_by IS NULL
ORDER BY
    data_lancamento
SQL;
            return parent::get($sql, 'Assoc');
        }
        return [];
    }

    public static function getLancamentosAglutinacao($periodo, $type = 'baixa')
    {
        $interval = " BETWEEN '{$periodo->inicio}' AND '{$periodo->fim}' ";
        if($type === "baixa") {
            $sql = <<<SQL
    SELECT
    /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO DEBITO SERA COMPOSTO
     * PELA ID DA NOTA + NOME DO CLIENTE
     * SENAO E 1 (DESEMBOLSO), LOGO, SERA PELO ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
     */
    IF (
        n.tipo = 0,
        CONCAT(
            'REC. AGLUTINADO ',
            n.codigo,
            ' ',
            f.nome
        ),
        CONCAT(
            'PG. AGLUTINADO. ',
            n.codigo,
            ' ',
            f.razaoSocial
        )
    ) AS debito_historico,
     /* SE O TIPO FOR 0 (RECEBIMENTO) O HISTORICO DO CREDITO SERA COMPOSTO
     * POR UM PREFIXO 'REC. DOC.' + ID DA NOTA + NOME DO CLIENTE
     * SENAO E 1 (DESEMBOLSO), LOGO, SERA POR UM PREFIXO 'PG. DOC.' + ID DA NOTA + RAZAO SOCIAL DO FORNECEDOR
     */
    IF (
        n.tipo = 0,
        CONCAT(
            'REC. AGLUTINADO ',
            n.codigo,
            ' ',
            f.nome
        ),
        CONCAT(
            'PG. AGLUTINADO. ',
            n.codigo,
            ' ',
            f.razaoSocial
        )
    ) AS credito_historico,
     /* SE O CAMPO JUROS/MULTA NAO FOR VAZIO, PROCEDE:
     * O HISTORICO DO JUROS/MULTA SERA COMPOSTO
     * POR UM PREFIXO 'Juros: ' + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
     * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
     */
    IF (
        p.JurosMulta != 0,
        CONCAT(
            'VL. REF. JUROS DOC ',
            n.codigo,
            ' ',
            IF (
                n.tipo = 0,
                f.nome,
                f.razaoSocial
            )
        ),
        ''
    ) AS juros_historico,
     /* SE O CAMPO DESCONTO NAO FOR VAZIO, PROCEDE:
     * O HISTORICO DO DESCONTO SERA COMPOSTO
     * POR UM PREFIXO 'Juros: ' + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
     * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
     */
    IF (
        p.desconto != 0,
        CONCAT(
            'VL. REF. DESCONTO DOC ',
            n.codigo,
            ' ',
            IF (
                n.tipo = 0,
                f.nome,
                f.razaoSocial
            )
        ),
        ''
    ) AS desconto_historico,
     /* O HISTORICO DAS RETENCOES SERA COMPOSTO
     * POR TIPO DE RETENÇÃO + ID DA NOTA + NOME DO CLIENTE SE O TIPO FOR 0 (RECEBIMENTO)
     * OU RAZAO SOCIAL DO FORNECEDOR SE O TIPO FOR 1 (DESEMBOLSO)
     */
    IF (
        n.tipo = 0,
        CONCAT(n.codigo, ' ', f.nome),
        CONCAT(n.codigo, ' ', f.razaoSocial)
     ) AS retencoes_historico,
     p.valor AS valor,
     p.JurosMulta,
     p.desconto,
     p.OUTROS_ACRESCIMOS AS outras_tarifas,
     p.encargos,
     n.tipo,
     n.descricao,
     'n' AS origem,
     'n' AS destino,
     'n' AS conta_tarifa,
     p.id AS idNotas,
     DATE_FORMAT(
        p.DATA_PROCESSADO,
        '%d/%m/%Y'
     ) AS data_lancamento,
     'aglutinada' AS lancamento_comum,
     's' AS nota_aglutinacao,
     'n' AS nota_adiantamento,
     'n' AS nota_baixa,
     'S' AS conciliado
    FROM
        parcelas AS p
    INNER JOIN notas AS n ON n.idNotas = p.idNota
    INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
    INNER JOIN contas_bancarias AS cb ON cb.id = p.origem
    WHERE
        p.DATA_PROCESSADO {$interval}
    AND n.`status` != 'Cancelada'
    AND p.status = 'Processada'
    AND (p.nota_id_fatura_aglutinacao is not null or p.nota_id_fatura_aglutinacao != 0)
    AND n.nota_adiantamento = 'N'
ORDER BY
    data_lancamento
SQL;
            return parent::get($sql, 'Assoc');
        }
        return [];
    }

    public function getDebitoByNotaId($id, $tipo = "inclusao")
    {
        if("inclusao" === $tipo) {
            $sql = <<<SQL
SELECT
	/* REGRAS IMPOSTAS POR JEAN PARA SELECAO DO CODIGO DOMINIO * A 
	   SEREM UTILIZADAS NA COMPOSICAO DO ARQUIVO NA PARTIDA DEBITO */
	CASE
	    /* QUANDO O CADASTRO DO IPCF FOR MARCADO PARA PROVISIONAR * E O IPCC 
	       PREENCHIDO: * DEBITO â€“ SE TIPO 0 CODIGO DO CLIENTE, SENAO CONTA DO IPCC ASSOCIADO AO IPCF 
	       
	       NOVA:
	       SE FOR ENTRADA:
	       - CODIGO DA DOMINIO REFERENTE AO CLIENTE
	       SE FOR SAÍDA:
	       - CODIGO DA  DOMINIO REFERENTE AO FORNECEDOR
	       */
WHEN (
	(
		SELECT
			PROVISIONA
		FROM
			plano_contas AS pc1
		WHERE
			ID = r.IPCG4
	) = 's'
)
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	'1.1.2.02.017',
	(
		SELECT
			pcc.COD_N5
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	)
)
WHEN (
	(
		SELECT
			PROVISIONA
		FROM
			plano_contas AS pc1
		WHERE
			ID = r.IPCG4
	) = 'n'
)
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	(
		SELECT
			pcc.COD_N5
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	),
	'2.1.2.02.001'
)	    
END AS codigo_debito,
 e.centro_custo_dominio AS codigo_empresa,  
 asn.CODIGO_DOMINIO AS centro_custo_debito,
 (n.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
	r.NOTA_ID = {$id}
    AND pc.PROVISIONA = 's'
ORDER BY
	r.ID
SQL;

        } else {
            $sql = <<<SQL
SELECT
	/* REGRAS IMPOSTAS POR JEAN PARA SELECAO DO CODIGO DOMINIO
 * A SEREM UTILIZADAS NA COMPOSICAO DO ARQUIVO NA PARTIDA DEBITO
 */
CASE 
    /* QUANDO O CADASTRO DO IPCF ESTIVER MARCADO PARA PROVISIONAR
 * E O IPCC PREENCHIDO
 * DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADOA CONTA BANCARIA, 
        SENAO CONTA IPCC ASSOCIADO AO FORNECEDOR
 */
WHEN (
	SELECT
		PROVISIONA
	FROM
		plano_contas AS pc1
	WHERE
		ID = r.IPCG4
) = 's'
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	(
		SELECT
			pcc.COD_N5
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	),
	'2.1.2.02.001'
) /* QUANDO O CADASTRO DO IPCF ESTIVER DESMARCADO PARA PROVISIONAR
 * E O IPCC PREENCHIDO
 * DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A CONTA BANCARIA, SENAO CONTA DO IPCC ASSOCIADO AO IPCF
 */
WHEN (
	SELECT
		PROVISIONA
	FROM
		plano_contas AS pc1
	WHERE
		ID = r.IPCG4
) = 'n'
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	(
		SELECT
			pcc.COD_N5
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	),
	(
		SELECT
			pcc.COD_N5
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	)
)
END AS codigo_debito,
 p.DATA_CONCILIADO AS data_conciliado,
 r.IPCG4,
 e.centro_custo_dominio AS codigo_empresa,
 asn.DESCRICAO_GRUPO_DOMINIO AS centro_custo_debito,
 (p.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.idNota = n.idNotas)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN contas_bancarias AS cb ON (p.origem = cb.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
  p.id = '{$id}'
ORDER BY
	r.ID
SQL;
        }

        return parent::get($sql);
    }

    public function getCreditoByNotaId($id, $tipo = "inclusao")
    {
        if("inclusao" === $tipo) { # ARQUIVO DE INCLUSÃO
            $sql = <<<SQL
SELECT
/* REGRAS IMPOSTAS POR JEAN PARA SELECAO DO CODIGO DOMINIO * A SEREM UTILIZADAS NA COMPOSICAO DO ARQUIVO NA PARTIDA CREDITO */
CASE 
    /* QUANDO O CADASTRO DO IPCF FOR MARCADO PARA PROVISIONAR * E O IPCC PREENCHIDO: * 
       CREDITO â€“ SE TIPO 0 CODIGO IPCC ASSOCIADO AO CLIENTE, 
       SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA */
WHEN (
	(
		SELECT
			PROVISIONA
		FROM
			plano_contas AS pc1
		WHERE
			ID = r.IPCG4
	) = 's'
)
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	(
		SELECT
			pcc.COD_N5
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	),
    '2.1.2.02.001'
)
WHEN (
	(
		SELECT
			PROVISIONA
		FROM
			plano_contas AS pc1
		WHERE
			ID = r.IPCG4
	) = 'n'
)
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	'1.1.2.02.017',
	(
		SELECT
			pcc.COD_N5
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	)
)
END AS codigo_credito,
 e.centro_custo_dominio AS codigo_empresa,
 asn.CODIGO_DOMINIO AS centro_custo_credito,
 (n.VALOR * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
	r.NOTA_ID = {$id}   
    AND pc.PROVISIONA = 's'
ORDER BY
	r.ID
SQL;

        } else { # ARQUIVO DE BAIXA
            $sql = <<<SQL
SELECT
 /* REGRAS IMPOSTAS POR JEAN PARA SELECAO DO CODIGO DOMINIO
 * A SEREM UTILIZADAS NA COMPOSICAO DO ARQUIVO NA PARTIDA CREDITO
 */
CASE 
    /* QUANDO O CADASTRO DO IPCF FOR MARCADO PARA PROVISIONAR
 * E O IPCC PREENCHIDO:
 * CREDITO – SE TIPO 0 CODIGO IPCC ASSOCIADO AO CLIENTE, SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA
 */
WHEN (
	(
		SELECT
			PROVISIONA
		FROM
			plano_contas AS pc1
		WHERE
			ID = r.IPCG4
	) = 's'
)
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	'1.1.2.02.017',
	(
		SELECT
			pcc.COD_N5
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	)
) /* QUANDO O CADASTRO DO IPCF ESTIVER DESMARCADO PARA PROVISIONAR
 * E O IPCC PREENCHIDO
 * CREDITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO AO IPCF, SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA
 */
WHEN (
	SELECT
		PROVISIONA
	FROM
		plano_contas AS pc1
	WHERE
		ID = r.IPCG4
) = 'n'
AND (
	SELECT
		COD_IPCC
	FROM
		plano_contas AS pc2
	WHERE
		ID = r.IPCG4
) IS NOT NULL THEN

IF (
	n.tipo = 0,
	(
		SELECT
			pcc.COD_N5
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	),
	(
		SELECT
			pcc.COD_N5
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	)
) 
END AS codigo_credito,
 p.DATA_CONCILIADO AS data_conciliado,
 e.centro_custo_dominio AS codigo_empresa,
 asn.DESCRICAO_GRUPO_DOMINIO AS centro_custo_credito,
 (p.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.idNota = n.idNotas)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN contas_bancarias AS cb ON (p.origem = cb.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
	p.id = {$id}
ORDER BY
	r.ID
SQL;
        }
        return parent::get($sql);
    }

    public function getDebitoAntecipacaoByNotaId($id)
    {
        $sql = <<<SQL
SELECT
/* DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A CONTA BANCARIA, 
 * SENAO CONTA DO IPCC ASSOCIADO AO IPCF
 */
IF (
	n.tipo = 0,
	(
		SELECT
			pcc.COD_N5
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	),
	'1.1.3.05.009'
) AS codigo_debito,
 p.DATA_CONCILIADO AS data_conciliado,
 e.centro_custo_dominio AS codigo_empresa,
 asn.DESCRICAO_GRUPO_DOMINIO AS centro_custo_debito,
 (p.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.idNota = n.idNotas)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN contas_bancarias AS cb ON (p.origem = cb.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
  p.id = {$id}
  AND n.nota_aglutinacao = 'n'
  AND n.nota_adiantamento = 'S'
ORDER BY
	r.ID
SQL;
        return parent::get($sql);
    }

    public function getCreditoAntecipacaoByNotaId($id)
    {
        $sql = <<<SQL
SELECT
/* DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A CONTA BANCARIA, 
 * SENAO CONTA DO IPCC ASSOCIADO AO IPCF
 */
IF (
	n.tipo = 0,
    '2.1.5.08.000',
    (
		SELECT
			pcc.COD_N5
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	)	
) AS codigo_credito,
 p.DATA_CONCILIADO AS data_conciliado,
 e.centro_custo_dominio AS codigo_empresa,
 asn.DESCRICAO_GRUPO_DOMINIO AS centro_custo_credito,
 (p.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.idNota = n.idNotas)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN contas_bancarias AS cb ON (p.origem = cb.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
  p.id = {$id}
  AND n.nota_aglutinacao = 'n'
  AND n.nota_adiantamento = 'S'
ORDER BY
	r.ID
SQL;
        return parent::get($sql);
    }

    public function getDebitoCompensacaoByNotaId($id)
    {
        $sql = <<<SQL
SELECT
/* DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO AO ADIANTAMENTO DE CLIENTES, 
 * SENAO CONTA DO FORNECEDOR A PAGAR
 */
IF (
	n.tipo = 0,
	'1.1.3.12.001',
	'2.1.2.02.001'
) AS codigo_debito,
 nc.data_compensado AS data_conciliado,
 e.centro_custo_dominio AS codigo_empresa,
 asn.DESCRICAO_GRUPO_DOMINIO AS centro_custo_debito,
 (p.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.idNota = n.idNotas)
INNER JOIN nota_compensacao AS nc ON (nc.parcela_id = p.id OR nc.parcela_pivo_id = p.id)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
WHERE
  p.id = {$id} 
ORDER BY
	r.ID
SQL;
        return parent::get($sql);
    }

    public function getCreditoCompensacaoByNotaId($id)
    {
        $sql = <<<SQL
SELECT
/* DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A DESPESA, 
 * SENAO CONTA DO IPCC ASSOCIADO AO FORNECEDOR
 */
IF (
	n.tipo = 0,
    '1.1.2.02.017',
    '1.1.3.05.009'
) AS codigo_credito,
 nc.data_compensado AS data_conciliado,
 e.centro_custo_dominio AS codigo_empresa,
 asn.DESCRICAO_GRUPO_DOMINIO AS centro_custo_credito,
 (p.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.idNota = n.idNotas)
INNER JOIN nota_compensacao AS nc ON (nc.parcela_id = p.id OR nc.parcela_pivo_id = p.id)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
WHERE
  p.id = {$id} 
ORDER BY
	r.ID
SQL;
        return parent::get($sql);
    }

    public function getDebitoBaixaAntecipacaoByNotaId($id)
    {
        $sql = <<<SQL
SELECT
/* DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A DESPESA, 
 * SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA
 */
IF (
	n.tipo = 0,
	'1.1.3.12.001',
	(
		SELECT
			pcc.COD_N5
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	)
) AS codigo_debito,
 nb.data_conciliado AS data_conciliado,
 0 AS codigo_empresa,
 '' AS centro_custo_credito,
 nb.valor AS valor,
 100.00 AS PORCENTAGEM
FROM
	nota_baixas AS nb
INNER JOIN notas AS n ON (n.idNotas = nb.nota_id)
INNER JOIN plano_contas AS pc ON (nb.plano_conta_id = pc.ID)
INNER JOIN empresas AS e ON (n.empresa = e.id)
INNER JOIN contas_bancarias AS cb ON (nb.conta_bancaria_id = cb.ID)
WHERE
  nb.id = {$id}
SQL;
        return parent::get($sql);
    }

    public function getCreditoBaixaAntecipacaoByNotaId($id)
    {
        $sql = <<<SQL
SELECT
/* DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A DESPESA, 
 * SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA
 */
IF (
	n.tipo = 0,
	(
		SELECT
			pcc.COD_N5
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	),
	(
		SELECT
			pcc.COD_N5
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.COD_N5 = pc.COD_IPCC
	)
) AS codigo_credito,
 nb.data_conciliado AS data_conciliado,
 0 AS codigo_empresa,
 '' AS centro_custo_credito,
 nb.valor AS valor,
 100.00 AS PORCENTAGEM
FROM
	nota_baixas AS nb
INNER JOIN notas AS n ON (n.idNotas = nb.nota_id)
INNER JOIN plano_contas AS pc ON (nb.plano_conta_id = pc.ID)
INNER JOIN empresas AS e ON (n.empresa = e.id)
INNER JOIN contas_bancarias AS cb ON (nb.conta_bancaria_id = cb.ID)
WHERE
  nb.id = {$id}
SQL;
        return parent::get($sql);
    }

    public function getDebitoAglutinacaoByNotaId($id, $conciliado = 'S')
    {
        $imposto = TipoDocumentoFinanceiro::getTipoByParcelaId($id)['imposto'];

        $recebimento = <<<SQL
(
    SELECT
        pcc.COD_N5
    FROM
        plano_contas_contabil AS pcc
    WHERE
        pcc.ID = cb.IPCC
)
SQL;
        $desembolso = <<<SQL
(
    SELECT
        pcc.COD_N5
    FROM
        plano_contas_contabil AS pcc
    WHERE
        pcc.COD_N5 = pc.COD_IPCC
)
SQL;

        if($conciliado == 'N') {
            if($imposto == 'N') {
                $recebimento = <<<SQL
(
    SELECT
        pcc.COD_N5
    FROM
        plano_contas_contabil AS pcc
    WHERE
        pcc.COD_N5 = pc.COD_IPCC
)
SQL;
                $desembolso = '2.1.2.02.001';

            } else {
                $recebimento = <<<SQL
(
    SELECT
        pcc.COD_N5
    FROM
        plano_contas_contabil AS pcc
    WHERE
        pcc.ID = cb.IPCC
)
SQL;
                $desembolso = <<<SQL
(
    SELECT
        pcc.COD_N5
    FROM
        plano_contas_contabil AS pcc
    WHERE
        pcc.COD_N5 = pc.COD_IPCC
)
SQL;
            }
        }

        $sql = <<<SQL
SELECT
/* DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A DESPESA, 
 * SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA
 */
IF (
	n.tipo = 0,
	{$recebimento},
	{$desembolso}
) AS codigo_debito,
 p.DATA_CONCILIADO AS data_conciliado,
 e.centro_custo_dominio AS codigo_empresa,
 asn.DESCRICAO_GRUPO_DOMINIO AS centro_custo_debito,
 (p.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.idNota = n.idNotas)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN contas_bancarias AS cb ON (p.origem = cb.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
  p.id = {$id}
  AND n.nota_aglutinacao = 's'
ORDER BY
	r.ID
SQL;
        return parent::get($sql);
    }

    public function getCreditoAglutinacaoByNotaId($id, $conciliado = 'S')
    {
        $imposto = TipoDocumentoFinanceiro::getTipoByParcelaId($id)['imposto'];

        $recebimento = <<<SQL
(
    SELECT
        pcc.COD_N5
    FROM
        plano_contas_contabil AS pcc
    WHERE
        pcc.COD_N5 = pc.COD_IPCC
)
SQL;
        $desembolso = <<<SQL
(
    SELECT
        pcc.COD_N5
    FROM
        plano_contas_contabil AS pcc
    WHERE
        pcc.ID = cb.IPCC
)
SQL;

        if($conciliado == 'N') {
            if($imposto == 'N') {
                $recebimento = <<<SQL
(
    SELECT
        IF(f.TIPO = 'F', '2.1.2.02.001', '1.1.2.02.017') AS COD_N5
    FROM
        plano_contas_contabil AS pcc
    WHERE
        pcc.COD_N5 = f.IPCC
)
SQL;
                $desembolso = <<<SQL
(
    SELECT
        pcc.COD_N5
    FROM
        plano_contas_contabil AS pcc
    WHERE
        pcc.COD_N5 = pc.COD_IPCC
)
SQL;
            } else {
                $recebimento = <<<SQL
(
    SELECT
        pcc.COD_N5
    FROM
        plano_contas_contabil AS pcc
    WHERE
        pcc.COD_N5 = pc.COD_IPCC
)
SQL;
                $desembolso = <<<SQL
(
    SELECT
        pcc.COD_N5
    FROM
        plano_contas_contabil AS pcc
    WHERE
        pcc.ID = cb.IPCC
)
SQL;
            }
        }

        $sql = <<<SQL
SELECT
/* DEBITO – SE TIPO 0 CONTA DO IPCC ASSOCIADO A DESPESA, 
 * SENAO CONTA DO IPCC ASSOCIADO A CONTA BANCARIA
 */
IF (
	n.tipo = 0,
	{$recebimento},
	{$desembolso}
) AS codigo_credito,
 p.DATA_CONCILIADO AS data_conciliado,
 e.centro_custo_dominio AS codigo_empresa,
 asn.DESCRICAO_GRUPO_DOMINIO AS centro_custo_credito,
 (p.valor * r.PORCENTAGEM) / 100 AS valor,
 r.PORCENTAGEM
FROM
	rateio AS r
INNER JOIN notas AS n ON (n.idNotas = r.NOTA_ID)
INNER JOIN parcelas AS p ON (p.idNota = n.idNotas)
INNER JOIN fornecedores AS f ON n.codFornecedor = f.idFornecedores
INNER JOIN plano_contas AS pc ON (r.IPCG4 = pc.ID)
INNER JOIN contas_bancarias AS cb ON (p.origem = cb.ID)
INNER JOIN empresas AS e ON (r.CENTRO_RESULTADO = e.id)
INNER JOIN assinaturas AS asn ON (r.ASSINATURA = asn.ID)
WHERE
  p.id = {$id}
  AND n.nota_aglutinacao = 's'
ORDER BY
	r.ID
SQL;
        return parent::get($sql);
    }

    public function getCentroResultadoDebitoByDestino($destino)
    {
        $sql = <<<SQL
SELECT
(
		SELECT
			pcc.COD_N5
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	) AS codigo_debito,
	e.SIGLA_EMPRESA AS centro_custo_debito
FROM
	contas_bancarias AS cb
INNER JOIN empresas AS e ON e.id = cb.UR
WHERE
	cb.ID = {$destino}
SQL;
        return parent::get ($sql);
    }

    public function getCentroResultadoCreditoByOrigem($origem)
    {
        $sql = <<<SQL
SELECT
/* REGRAS IMPOSTAS POR JEAN PARA SELECAO DO CODIGO DOMINIO
 * A SEREM UTILIZADAS NA COMPOSICAO DO ARQUIVO NA PARTIDA TRANSFENCIAS
 *
 * DEBITO - CODIGO IPCC DA CONTA DESTINO DA TRANSACAO
 * CREDITO - CODIGO IPCC DA CONTA ORIGEM DA TRANSACAO
 */
	(
		SELECT
			pcc.COD_N5
		FROM
			plano_contas_contabil AS pcc
		WHERE
			pcc.ID = cb.IPCC
	) AS codigo_credito,
	e.SIGLA_EMPRESA AS centro_custo_credito
FROM
	contas_bancarias AS cb
INNER JOIN empresas AS e ON e.id = cb.UR
WHERE
	cb.ID = {$origem}
SQL;
        return parent::get ($sql);
    }

    public function getCentroResultadoDebitoByTarifaImpostoContaId($contaId, $tipo, $tipoLancto)
    {
        $sql = <<<SQL
SELECT
/* REGRAS IMPOSTAS POR JEAN PARA SELECAO DO CODIGO DOMINIO
 * A SEREM UTILIZADAS NA COMPOSICAO DO ARQUIVO NA PARTIDA TARIFAS/IMPOSTOS
 *
 * DEBITO - CODIGO IPCC 385 OU 369
 */
    IF(
        {$tipoLancto} = 1,
        IF ('{$tipo}' = 'IMPOSTO', '5.1.2.10.003', '5.1.2.09.001'),
        (
            SELECT
                pcc.COD_N5
            FROM
                plano_contas_contabil AS pcc
            WHERE
                pcc.ID = cb.IPCC
        )
	) AS codigo_debito,
	'102' AS centro_custo_debito,
    e.SIGLA_EMPRESA AS codigo_empresa
FROM
	contas_bancarias AS cb
INNER JOIN empresas AS e ON e.id = cb.UR
WHERE
	cb.ID = {$contaId}
SQL;
        return parent::get ($sql);
    }

    public function getCentroResultadoCreditoByTarifaImpostoContaId($contaId, $tipo, $tipoLancto)
    {
        $sql = <<<SQL
SELECT
/* REGRAS IMPOSTAS POR JEAN PARA SELECAO DO CODIGO DOMINIO
 * A SEREM UTILIZADAS NA COMPOSICAO DO ARQUIVO NA PARTIDA TARIFAS/IMPOSTOS
 *
 * CREDITO - CODIGO IPCC DA BANCO ONDE FOI PAGO
 */
    IF(
        {$tipoLancto} = 1,
	    (
            SELECT
                pcc.COD_N5
            FROM
                plano_contas_contabil AS pcc
            WHERE
                pcc.ID = cb.IPCC
	    ),
        IF ('{$tipo}' = 'IMPOSTO', '5.1.2.10.003', '5.1.2.09.001') 
    ) AS codigo_credito,
	-- e.centro_custo_dominio AS centro_custo_credito
	'        ' AS centro_custo_credito,
    e.SIGLA_EMPRESA AS codigo_empresa
FROM
	contas_bancarias AS cb
INNER JOIN empresas AS e ON e.id = cb.UR
WHERE
	cb.ID = {$contaId}
SQL;
        return parent::get ($sql);
    }
}