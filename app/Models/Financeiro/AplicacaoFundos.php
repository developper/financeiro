<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class AplicacaoFundos extends AbstractModel implements DAOInterface
{

  public static function getAll()
  {
    return parent::get(self::getSQL());
  }
  public static function getById($id)
  {
    return parent::get(self::getSQL($id));
  }    

  public static function getSQL($id = null)
  {    
    $condId = isset($id) && !empty($id) ? 'AND ID = ' . $id : '';
    return "SELECT * FROM aplicacaofundos where id in ( 4,5,6,7,8,1,9,10) {$condId}  ORDER BY forma";
  }

  public static function getByListId($list = null)
  {   
    $list = implode(',', $list);     
    
    $sql = "SELECT * FROM aplicacaofundos where id in ({$list})   ORDER BY forma";
   
    return parent::get($sql);
  }
    

} 