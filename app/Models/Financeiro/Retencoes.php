<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel;

class Retencoes extends AbstractModel
{
    public static function getConfiguracaoRetencoes($tipo = 'desembolso')
    {
        $sql = <<<SQL
SELECT 
  pccr.*,
  CONCAT(pc1.COD_N4, ' - ', pc1.N4) AS desc_iss,
  CONCAT(pc2.COD_N4, ' - ', pc2.N4) AS desc_icms,
  CONCAT(pc3.COD_N4, ' - ', pc3.N4) AS desc_ipi,
  CONCAT(pc4.COD_N4, ' - ', pc4.N4) AS desc_pis,
  CONCAT(pc5.COD_N4, ' - ', pc5.N4) AS desc_cofins,
  CONCAT(pc6.COD_N4, ' - ', pc6.N4) AS desc_csll,
  CONCAT(pc7.COD_N4, ' - ', pc7.N4) AS desc_ir,
  CONCAT(pc8.COD_N4, ' - ', pc8.N4) AS desc_pcc,
  CONCAT(pc9.COD_N4, ' - ', pc9.N4) AS desc_inss
FROM 
  plano_contas_configuracao_retencoes AS pccr
  LEFT JOIN plano_contas AS pc1 ON pccr.iss = pc1.ID
  LEFT JOIN plano_contas AS pc2 ON pccr.icms = pc2.ID
  LEFT JOIN plano_contas AS pc3 ON pccr.ipi = pc3.ID
  LEFT JOIN plano_contas AS pc4 ON pccr.pis = pc4.ID
  LEFT JOIN plano_contas AS pc5 ON pccr.cofins = pc5.ID
  LEFT JOIN plano_contas AS pc6 ON pccr.csll = pc6.ID
  LEFT JOIN plano_contas AS pc7 ON pccr.ir = pc7.ID
  LEFT JOIN plano_contas AS pc8 ON pccr.pcc = pc8.ID
  LEFT JOIN plano_contas AS pc9 ON pccr.inss = pc9.ID
WHERE
  pccr.tipo = '{$tipo}'
SQL;
        return current(parent::get($sql, 'ASSOC'));

    }

    public static function getVencimentosRetencoes()
    {
        $sql = <<<SQL
SELECT 
  *
FROM 
  retencoes_vencimentos
SQL;
        $data = parent::get($sql, 'ASSOC');
        $response = [];
        foreach ($data as $row) {
            $response[$row['tipo']][$row['retencao']] = $row['vencimento'];
        }
        return $response;
    }

    public static function getContasRetencoesByTerm($term)
    {
        $sql = <<<SQL
SELECT 
  ID,
  CONCAT(COD_N4, ' - ', N4) AS value
FROM
  plano_contas 
WHERE 
  (
    plano_contas.N4 LIKE '%{$term}%'
    OR plano_contas.N4 LIKE '%{$term}%'
  )
  AND plano_contas.POSSUI_RETENCOES = 1
ORDER BY 
  N4 ASC
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function save($data)
    {
        //echo '<pre>'; die(print_r($data));
        mysql_query('BEGIN');

        $sql = <<<SQL
UPDATE 
  plano_contas_configuracao_retencoes 
SET 
  iss = '{$data['desembolso']['iss_id']}',
  ir = '{$data['desembolso']['ir_id']}',
  pis = '{$data['desembolso']['pis_id']}',
  cofins = '{$data['desembolso']['cofins_id']}',
  csll = '{$data['desembolso']['csll_id']}',
  pcc = '{$data['desembolso']['pcc_id']}',
  inss = '{$data['desembolso']['inss_id']}'
WHERE
  tipo = 'desembolso'
SQL;
        $rs = mysql_query($sql);
        if (!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        foreach ($data['desembolso'] as $nome => $imposto) {
            if(strlen($imposto) <= 3) {
                //var_dump($nome, $imposto);
                $nome = explode('_', $nome);
                $nome = $nome[0] == 'iss' ? strtoupper($nome[0]) . 'QN' : strtoupper($nome[0]);
                $sql = <<<SQL
SELECT N2, N3, N4 FROM plano_contas WHERE ID = '{$imposto}'
SQL;
                $rs = mysql_query($sql);
                $rowConta = mysql_fetch_array($rs, MYSQL_ASSOC);

                $sql = <<<SQL
UPDATE 
  impostos_retidos 
SET 
  PLANO_CONTA_ID = '{$imposto}',
  IPCF2 = '{$rowConta['N2']}',
  IPCF3 = '{$rowConta['N3']}',
  IPCF4 = '{$rowConta['N4']}'
WHERE
  IMPOSTO = '{$nome}'
SQL;
                $rs = mysql_query($sql);
                if (!$rs) {
                    mysql_query('ROLLBACK');
                    return false;
                }
            }
        }
        //die();

        $sql = <<<SQL
UPDATE 
  plano_contas_configuracao_retencoes 
SET 
  iss = '{$data['recebimento']['iss_id']}',
  ir = '{$data['recebimento']['ir_id']}',
  pis = '{$data['recebimento']['pis_id']}',
  cofins = '{$data['recebimento']['cofins_id']}',
  csll = '{$data['recebimento']['csll_id']}',
  pcc = '{$data['recebimento']['pcc_id']}',
  inss = '{$data['recebimento']['inss_id']}'
WHERE
  tipo = 'recebimento'
SQL;
        $rs = mysql_query($sql);
        if (!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        foreach ($data['vencimentos']['desembolso'] as $retencao => $vencimento) {
            //if($vencimento != '' && $vencimento > 0) {
                $sql = <<<SQL
UPDATE retencoes_vencimentos SET vencimento = '{$vencimento}' 
WHERE retencao = '{$retencao}' AND tipo = 'desembolso'    
SQL;
                $rs = mysql_query($sql);
                if(!$rs) {
                    mysql_query('ROLLBACK');
                    return false;
                }
            //}
        }

        foreach ($data['vencimentos']['recebimento'] as $retencao => $vencimento) {
            //if($vencimento != '' && $vencimento > 0) {
                $sql = <<<SQL
UPDATE retencoes_vencimentos SET vencimento = '{$vencimento}' 
WHERE retencao = '{$retencao}' AND tipo = 'recebimento'
SQL;
                $rs = mysql_query($sql);
                if(!$rs) {
                    mysql_query('ROLLBACK');
                    return false;
                }
            //}
        }

        return mysql_query("COMMIT");
    }
}
