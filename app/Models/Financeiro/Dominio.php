<?php
namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface,
    \App\Helpers\StringHelper;

if (!function_exists('dd')) {
    function dd()
    {
        echo '<pre>';
        $args = func_get_args();
        foreach ($args as $arg) {
            $arg = [$arg];
            call_user_func_array('print_r', $arg);
            echo '<br><br>';
        }
        die();
    }
}

if (!function_exists('d')) {
    function d()
    {
        echo '<pre>';
        $args = func_get_args();
        call_user_func_array('print_r', $args);
        echo '<br><br>';
    }
}

class Dominio extends AbstractModel implements DAOInterface
{

    private $txt = "";
    private $registro = [];
    private $registro99 = "9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999";

    //CONSTANTES DO ARQUIVO
    const BRANCOS =	"                                                                                                                           ";

    #REGISTRO 1
    const CODIGO_EMPRESA = "0000269";
    const CNPJ_MEDERI = "04678190000151";
    const TIPO_NOTA = "05";
    const SISTEMA = "1";

    #REGISTRO 2
    const TIPO = "V";
    const USUARIO = "GERENTE";

    #REGISTRO 3
    const CODIGO_HISTORICO = "0000000";
    const CODIGO_FILIAL = "0000001";

    public static function getAll()
    {
        return parent::get(self::getSQL());
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? 'AND ID = "' . $id . '"' : "";
        return "SELECT * FROM plano_contas WHERE 1=1 {$id} ORDER BY COD_N2, COD_N3, COD_N4";
    }

    private function endLine()
    {
        $this->txt .= "\r\n";//PHP_EOL;
    }

    public function createTextFromData($data)
    {
        unset($this->registro);
        # Gera a linha do cabecalho do arquivo
        $this->createCabecalho($data);
        foreach($this->registro as $registro){
            $this->txt .= $registro;
        }
        self::endLine();
        $text = "";
        $this->createLotesFromData($data);
       
       
        foreach($this->registro as $registro){
            $this->txt .= array_reduce($registro, function($txt, $lote){
                # Gera a linha dos dados do lançamento
                $txt .= implode('', array_slice(array_values($lote), 0, 6));
                $txt .= "\r\n";

                # Gera a linha do débito do lançamento
                $txt .= $this->reduceCreditoDebitoFromData($lote['partidas']['debito']);

                # Gera a linha do juros/multa do lançamento (se houver)
                if(!empty($lote['partidas']['juros']) || $lote['partidas']['juros'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['juros']);
                }

                # Gera a linha do desconto do lançamento (se houver)
                if(!empty($lote['partidas']['desconto']) || $lote['partidas']['desconto'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['desconto']);
                }

                # Gera a linha do IRRF do lançamento (se houver)
                if(!empty($lote['partidas']['IRRF']) || $lote['partidas']['IRRF'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['IRRF']);
                }

                # Gera a linha do PIS do lançamento (se houver)
                if(!empty($lote['partidas']['PIS']) || $lote['partidas']['PIS'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['PIS']);
                }

                # Gera a linha do COFINS do lançamento (se houver)
                if(!empty($lote['partidas']['COFINS']) || $lote['partidas']['COFINS'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['COFINS']);
                }

                # Gera a linha do CSLL do lançamento (se houver)
                if(!empty($lote['partidas']['CSLL']) || $lote['partidas']['CSLL'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['CSLL']);
                }

                # Gera a linha do PCC do lançamento (se houver)
                if(!empty($lote['partidas']['PCC']) || $lote['partidas']['PCC'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['PCC']);
                }

                # Gera a linha do ISS do lançamento (se houver)
                if(!empty($lote['partidas']['ISS']) || $lote['partidas']['ISS'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['ISS']);
                }

                # Gera a linha do INSS do lançamento (se houver)
                if(!empty($lote['partidas']['INSS']) || $lote['partidas']['INSS'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['INSS']);
                }

                # Gera a linha das Tarifas Adicionais do lançamento (se houver)
                if(!empty($lote['partidas']['outras_tarifas']) || $lote['partidas']['outras_tarifas'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['outras_tarifas']);
                }

                # Gera a linha dos Encargos Financeiros do lançamento (se houver)
                if(!empty($lote['partidas']['encargos']) || $lote['partidas']['encargos'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['encargos']);
                }
                # Gera a linha da Tarifa de Cartao de Credito (se houver)
                if(!empty($lote['partidas']['tarifaCartao']) || $lote['partidas']['tarifaCartao'] !== null){
                    $txt .= $this->reduceRetencoesFromData($lote['partidas']['tarifaCartao']);
                }

                # Gera a linha do crédito do lançamento
                $txt .= $this->reduceCreditoDebitoFromData($lote['partidas']['credito']);
                
                return $txt;
            });
        }

        $this->txt .= $this->registro99;
        return $this->txt;
    }

    private function reduceCreditoDebitoFromData($data)
    {
        return array_reduce($data, function($txt, $source){
            $txt .= implode('', array_slice(array_values($source), 0, 9));
            $txt .= "\r\n";
            if(!is_null($source['rateio'])) {
                $txt .= implode('', array_values($source['rateio']));
                return $txt .= "\r\n";
            }
            return $txt;
        });
    }

    private function reduceRetencoesFromData($data)
    {
        $txt = array_reduce(array_slice($data, 0, 9), function($txt, $source){
            $txt .= $source;
            return $txt;
        });
        $txt .= "\r\n";
        if(!is_null($data['rateio'])) {
            $txt .= array_reduce($data['rateio'], function ($txt, $rateio) {
                $txt .= $rateio;
                return $txt;
            });

            return $txt .= "\r\n";
        }
        return $txt;
    }

    private function createCabecalho ($data)
    {
        $this->registro = [
            "identificador" => "01",
            "codigo_empresa" => self::CODIGO_EMPRESA,
            "cnpj" => self::CNPJ_MEDERI,
            "data_inicial" => \DateTime::createFromFormat('Y-m-d', $data['inicio'])->format('d/m/Y'),
            "data_final" => \DateTime::createFromFormat('Y-m-d', $data['fim'])->format('d/m/Y'),
            "valor_fixo" => "N",
            "tipo_nota" => self::TIPO_NOTA,
            "constante" => "00000",
            "sistema" => self::SISTEMA,
            "fixo" => "17"
        ];
    }

    private function createLotesFromData ($data)
    {
        unset($this->registro);
        $contArray = 0;
        $cont = 1;
        $periodo = new \stdClass();
        $periodo->inicio = $data['inicio'];
        $periodo->fim = $data['fim'];
        $lancamento = new Lancamento();
         $notas = $lancamento->getLancamentosNotas($periodo, $data['tipo']);
       
        $transferencias = $lancamento->getLancamentosTransferenciaBancaria($periodo, $data['tipo']);
        $tarifas = $lancamento->getLancamentosTarifasBancaria($periodo, $data['tipo']);
        $aglutinacoes = $lancamento->getLancamentosAglutinacao($periodo, $data['tipo']);
      
        $antecipacoes = $lancamento->getLancamentosAntecipacao($periodo, $data['tipo']);
        $compensacoes = $lancamento->getLancamentosCompensacao($periodo, $data['tipo']);
        $baixas = $lancamento->getLancamentosBaixasAntecipacao($periodo, $data['tipo']);
        $estornos_parcelas = $lancamento->getLancamentosEstornosParcelas($periodo, $data['tipo']);
        $informePerda = $lancamento->getLancamentosInformePerda($periodo, $data['tipo']);

      

       
        $lancamentos = array_merge(           
            $notas,           
            $transferencias,
            $tarifas,
            $aglutinacoes,
            $antecipacoes,
            $compensacoes,
            $baixas,
            $estornos_parcelas,
            $informePerda
        );

        /*
            Todos os lancamentos do sistema do periodo.

        */

        

       /* foreach($lancamentos as $n){
            $texto .= $n['data_lancamento']."@".$n['valor']."@".$n['codigo']."@".$n['razaoSocial']."@".$n['idNotas']."<br>";
        }*/
       

       
       
       

        /*$totalRecebimentos = 0;
        $totalDesembolsos = 0;
        echo '<pre>';
        foreach($lancamentos as $lancto) {
            echo $lancto['lancamento_comum'] . '@' . $lancto['data_lancamento'] . '@' . $lancto['idNotas'] . '@' . $lancto['tipo'] . '@' . $lancto['valor'] . '@' . $lancto['descricao'] . '<br>';
            if ($lancto['tipo'] == '0') {
                $totalRecebimentos += $lancto['valor'];
            } else {
                $totalDesembolsos += $lancto['valor'];
            }

        }

        dd($totalRecebimentos, $totalDesembolsos);*/

        foreach($lancamentos as $lancto) {
            $retencoes_historico = $lancto['retencoes_historico'] != '' ?
                StringHelper::UTF8DecodeString($lancto['retencoes_historico']) :
                '';
            /**
             * Grava o centro de custo de debito por referencia, para ser
             * usado nas retenções
             */
            $centro_custo_debito = 0;

            # Monta as partidas de débito de acordo com a quantidade de rateios
            $debitos = function (&$cont) use ($lancamento, $lancto, $data, &$centro_custo_debito) {
                $debito = [];
                # Campo de Descrição para crédito e débito
                $descricao = !empty($lancto['descricao']) ?
                    ' - ' . preg_replace("/\r|\n/", "",
                        trim(
                            strtoupper(
                                StringHelper::limitString($lancto['descricao'], 0, 69, '.')
                            )
                        )
                    ) :
                    '';

                if (is_numeric($lancto['idNotas'])) { # Se for Notas/Parcelas
                    $partidas = [];
                    
                    if ($lancto['lancamento_comum'] == 's') {
                        $partidas = $lancamento->getDebitoByNotaId($lancto['idNotas'], $data['tipo']);

                    }elseif($lancto['lancamento_comum'] == 'estorno'){
                        $partidas = $lancamento->getDebitoEstornoByNotaId($lancto['idNotas'], $data['tipo']);
                    }  elseif ($lancto['nota_adiantamento'] == 's') {
                        $partidas = $lancamento->getDebitoAntecipacaoByNotaId($lancto['idNotas'], $data['tipo']);
                    } elseif ($lancto['nota_compensacao'] == 's') {
                        $partidas = $lancamento->getDebitoCompensacaoByNotaId($lancto['idNotas'], $lancto['tipo'],$lancto['valor']);
                    } elseif ($lancto['nota_baixa'] == 's') {
                       
                        $partidas = $lancamento->getDebitoBaixaAntecipacaoByNotaId($lancto['idNotas'], $data['tipo']);
                    } elseif ($lancto['nota_aglutinacao'] == 's' ) {
                        $partidas = $lancamento->getDebitoAglutinacaoByNotaId($lancto['idNotas'], $lancto['idNotaAglutinacao'], $lancto['tipo']);
                    }elseif ($lancto['nota_informe_perda'] == 's' ) {                       
                        $partidas = $lancamento->getDebitoInformePerdaByNotaId($lancto['idNotas'],  $data['tipo']);
                    }

                   
                
                    

                    foreach ($partidas as $partida) {
                        $valor_debito = round($partida['valor'], 2);
                        $centro_custo_debito = $partida['centro_custo_debito'];

                        if ($data['tipo'] === 'inclusao') {
                            if ($lancto['tipo'] == '1') {
                                if (isset($lancto['IR']) && $lancto['IR'] != 0)
                                    $valor_debito += ($lancto['IR'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['PIS']) && $lancto['PIS'] != 0)
                                    $valor_debito += ($lancto['PIS'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['COFINS']) && $lancto['COFINS'] != 0)
                                    $valor_debito += ($lancto['COFINS'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['CSLL']) && $lancto['CSLL'] != 0)
                                    $valor_debito += ($lancto['CSLL'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['PCC']) && $lancto['PCC'] != 0)
                                    $valor_debito += ($lancto['PCC'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['ISSQN']) && $lancto['ISSQN'] != 0)
                                    $valor_debito += ($lancto['ISSQN'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['INSS']) && $lancto['INSS'] != 0)
                                    $valor_debito += ($lancto['INSS'] * $partida['PORCENTAGEM']) / 100;

                                /*if (isset($lancto['outras_tarifas']) && $lancto['outras_tarifas'] != 0)
                                    $valor_debito += ($lancto['outras_tarifas'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['encargos']) && $lancto['encargos'] != 0)
                                    $valor_debito += ($lancto['encargos'] * $partida['PORCENTAGEM']) / 100;*/
                            }

                        } else { # $tipo === 'baixa'
                            if ($lancto['tipo'] == '0') {
                                if (isset($lancto['outras_tarifas']) && $lancto['outras_tarifas'] != 0)
                                    $valor_debito += ($lancto['outras_tarifas'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['encargos']) && $lancto['encargos'] != 0)
                                    $valor_debito += ($lancto['encargos'] * $partida['PORCENTAGEM']) / 100;

                                if (!empty($lancto['desconto_historico']))
                                    $valor_debito -= ($lancto['desconto'] * $partida['PORCENTAGEM']) / 100;

                                if (!empty($lancto['juros_historico']))
                                    $valor_debito += ($lancto['JurosMulta'] * $partida['PORCENTAGEM']) / 100;

                                if (!empty($lancto['tarifa_cartao_historico']))
                                $valor_debito -= ($lancto['tarifa_cartao'] * $partida['PORCENTAGEM']) / 100;
                            }
                        }

                        $debito[] = [
                            "identificador" => "03",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => StringHelper::stringPad($partida['codigo_debito'], 7),
                            "conta_credito" => "0000000",
                            "valor" => StringHelper::money($valor_debito),
                            "codigo_historico" => self::CODIGO_HISTORICO,
                            "historico" => StringHelper::stringPad(StringHelper::UTF8DecodeString($lancto['debito_historico'] . $descricao), 512, 'right', " "),
                            "codigo_filial" => StringHelper::stringPad($partida['codigo_empresa'], 7, 'left', '0'),
                            "brancos" => self::BRANCOS,
                            "rateio" => IPCC::checkIfContaIsResultadoByCodDominio($partida['codigo_debito']) ?
                                [
                                    "identificador" => "05",
                                    "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                                    "conta_debito" => StringHelper::stringPad($partida['centro_custo_debito'], 7),
                                    "conta_credito" => "0000000",
                                    "valor" => StringHelper::money($valor_debito),
                                    "brancos" => self::BRANCOS
                                ] : null
                        ];
                    }
                } elseif (is_numeric($lancto['destino'])) { # Se for Transferencia Bancaria
                    $partidas = $lancamento->getCentroResultadoDebitoByDestino($lancto['destino']);

                    foreach ($partidas as $partida) {
                        $valor_debito = round($lancto['valor'], 2);

                        $debito[] = [
                            "identificador" => "03",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => StringHelper::stringPad($partida['codigo_debito'], 7),
                            "conta_credito" => "0000000",
                            "valor" => StringHelper::money($valor_debito),
                            "codigo_historico" => self::CODIGO_HISTORICO,
                            "historico" => StringHelper::stringPad(StringHelper::UTF8DecodeString($lancto['debito_historico'] . $descricao), 512, 'right', " "),
                            "codigo_filial" => StringHelper::stringPad($partida['centro_custo_debito'], 7, 'left', '0'),
                            "brancos" => self::BRANCOS,
                            "rateio" => IPCC::checkIfContaIsResultadoByCodDominio($partida['codigo_debito']) ? [
                                "identificador" => "05",
                                "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                                "conta_debito" => StringHelper::stringPad($partida['centro_custo_debito'], 7),
                                "conta_credito" => "0000000",
                                "valor" => StringHelper::money($valor_debito),
                                "brancos" => self::BRANCOS
                            ] : null
                        ];
                    }
                } elseif (is_numeric($lancto['conta_tarifa'])) { # Se for Tarifas e Impostos
                    if(empty($lancto['estorno_parcela'])) {
                        $partidas = $lancamento->getCentroResultadoDebitoByTarifaImpostoContaId($lancto);
                    } else {
                        $partidas = $lancamento->getDebitoEstornoBanco($lancto);
                    }
                  

                    foreach ($partidas as $partida) {
                        $valor_debito = round($lancto['valor'], 2);

                        $debito[] = [
                            "identificador" => "03",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => StringHelper::stringPad($partida['codigo_debito'], 7),
                            "conta_credito" => "0000000",
                            "valor" => StringHelper::money($valor_debito),
                            "codigo_historico" => self::CODIGO_HISTORICO,
                            "historico" => StringHelper::stringPad(StringHelper::UTF8DecodeString($lancto['debito_historico'] . $descricao), 512, 'right', " "),
                            "codigo_filial" => StringHelper::stringPad($partida['codigo_empresa'], 7, 'left', '0'),
                            "brancos" => self::BRANCOS,
                            "rateio" => IPCC::checkIfContaIsResultadoByCodDominio($partida['codigo_debito']) ? [
                                "identificador" => "05",
                                "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                                "conta_debito" => StringHelper::stringPad($partida['centro_custo_debito'], 7),
                                "conta_credito" => "0000000",
                                "valor" => StringHelper::money($valor_debito),
                                "brancos" => self::BRANCOS
                            ] : null
                        ];
                    }
                }

                return $debito;
            };

            /**
             * Grava o centro de custo de credito por referencia, para ser
             * usado nas retenções
             */
            if (is_numeric($lancto['idNotas']))
                $centro_custo_credito = $lancamento->getCreditoByNotaId($lancto['idNotas'], $data['tipo'])[0]['centro_custo_credito'];
            if (is_numeric($lancto['origem']))
                $centro_custo_credito = $lancamento->getCentroResultadoDebitoByDestino($lancto['destino'])[0]['centro_custo_credito'];
            elseif (is_numeric($lancto['conta_tarifa']))
                $centro_custo_credito = $lancamento->getCentroResultadoCreditoByTarifaImpostoContaId($lancto['conta_tarifa'])[0]['centro_custo_credito'];

            # Monta as partidas de crédito de acordo com a quantidade de rateios
            $creditos = function (&$cont) use ($lancamento, $lancto, $data) {
                $credito = [];
                # Campo de Descrição para crédito e débito
                $descricao = !empty($lancto['descricao']) ?
                    ' - ' . preg_replace("/\r|\n/", "",
                        trim(
                            strtoupper(
                                StringHelper::limitString($lancto['descricao'], 0, 69, '.')
                            )
                        )
                    ) :
                    '';

                if (is_numeric($lancto['idNotas'])) { # Se for Notas/Parcelas
                    $partidas = [];
                    if ($lancto['lancamento_comum'] == 's') {
                        $partidas = $lancamento->getCreditoByNotaId($lancto['idNotas'], $data['tipo']);
                    } elseif ($lancto['lancamento_comum'] == 'estorno') {
                        $partidas = $lancamento->getCreditoEstornoByNotaId($lancto['idNotas'], $data['tipo']);
                    } elseif ($lancto['nota_adiantamento'] == 's') {
                        $partidas = $lancamento->getCreditoAntecipacaoByNotaId($lancto['idNotas'], $data['tipo']);
                    } elseif ($lancto['nota_compensacao'] == 's') {
                        $partidas = $lancamento->getCreditoCompensacaoByNotaId($lancto['idNotas'], $lancto['tipo'], $lancto['valor']);
                    } elseif ($lancto['nota_baixa'] == 's') {
                        $partidas = $lancamento->getCreditoBaixaAntecipacaoByNotaId($lancto['idNotas'], $data['tipo']);
                    } elseif ($lancto['nota_aglutinacao'] == 's') {
                        $partidas = $lancamento->getCreditoAglutinacaoByNotaId($lancto['idNotas'], $lancto['idNotaAglutinacao'],$lancto['tipo']);
                    }elseif ($lancto['nota_informe_perda'] == 's' ) {                        
                        $partidas = $lancamento->getCreditoInformePerdaByNotaId($lancto['idNotas'], $data['tipo']);
                       
                    }
                   

                    foreach ($partidas as $partida) {
                        $valor_credito = round($partida['valor'], 2);
                        if ($data['tipo'] == 'inclusao') {
                            if ($lancto['tipo'] != '1') {
                                if (isset($lancto['IR']) && $lancto['IR'] != 0)
                                    $valor_credito += ($lancto['IR'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['PIS']) && $lancto['PIS'] != 0)
                                    $valor_credito += ($lancto['PIS'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['COFINS']) && $lancto['COFINS'] != 0)
                                    $valor_credito += ($lancto['COFINS'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['CSLL']) && $lancto['CSLL'] != 0)
                                    $valor_credito += ($lancto['CSLL'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['PCC']) && $lancto['PCC'] != 0)
                                    $valor_credito += ($lancto['PCC'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['ISSQN']) && $lancto['ISSQN'] != 0)
                                    $valor_credito += ($lancto['ISSQN'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['INSS']) && $lancto['INSS'] != 0)
                                    $valor_credito += ($lancto['INSS'] * $partida['PORCENTAGEM']) / 100;

                                if (!empty($lancto['desconto_historico']))
                                    $valor_credito -= ($lancto['desconto'] * $partida['PORCENTAGEM']) / 100;

                                if (!empty($lancto['juros_historico']))
                                    $valor_credito += ($lancto['JurosMulta'] * $partida['PORCENTAGEM']) / 100;

                                    if (!empty($lancto['tarifa_cartao_historico']))
                                $valor_credito -= ($lancto['tarifa_cartao'] * $partida['PORCENTAGEM']) / 100;
                     
                            } /*else {
                                if (isset($lancto['IR']) && $lancto['IR'] != 0)
                                    $valor_credito -= ($lancto['IR'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['PIS']) && $lancto['PIS'] != 0)
                                    $valor_credito -= ($lancto['PIS'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['COFINS']) && $lancto['COFINS'] != 0)
                                    $valor_credito -= ($lancto['COFINS'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['CSLL']) && $lancto['CSLL'] != 0)
                                    $valor_credito -= ($lancto['CSLL'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['PCC']) && $lancto['PCC'] != 0)
                                    $valor_credito -= ($lancto['PCC'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['ISSQN']) && $lancto['ISSQN'] != 0)
                                    $valor_credito -= ($lancto['ISSQN'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['INSS']) && $lancto['INSS'] != 0)
                                    $valor_credito -= ($lancto['INSS'] * $partida['PORCENTAGEM']) / 100;

                                if (!empty($lancto['desconto_historico']))
                                    $valor_credito += ($lancto['desconto'] * $partida['PORCENTAGEM']) / 100;

                                if (!empty($lancto['juros_historico']))
                                    $valor_credito -= ($lancto['JurosMulta'] * $partida['PORCENTAGEM']) / 100;
                            }*/
                        } else { # $tipo === 'baixa'
                            if ($lancto['tipo'] == '1') {
                                if (isset($lancto['outras_tarifas']) && $lancto['outras_tarifas'] != 0)
                                    $valor_credito += ($lancto['outras_tarifas'] * $partida['PORCENTAGEM']) / 100;

                                if (isset($lancto['encargos']) && $lancto['encargos'] != 0)
                                    $valor_credito += ($lancto['encargos'] * $partida['PORCENTAGEM']) / 100;

                                if (!empty($lancto['desconto_historico']))
                                    $valor_credito -= ($lancto['desconto'] * $partida['PORCENTAGEM']) / 100;

                                if (!empty($lancto['juros_historico']))
                                    $valor_credito += ($lancto['JurosMulta'] * $partida['PORCENTAGEM']) / 100;
                            }
                        }

                        $credito[] = [
                            "identificador" => "03",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => "0000000",
                            "conta_credito" => StringHelper::stringPad($partida['codigo_credito'], 7),
                            "valor" => StringHelper::money($valor_credito),
                            "codigo_historico" => self::CODIGO_HISTORICO,
                            "historico" => StringHelper::stringPad(StringHelper::UTF8DecodeString($lancto['credito_historico'] . $descricao), 512, 'right', " "),
                            "codigo_filial" => StringHelper::stringPad($partida['codigo_empresa'], 7, 'left', '0'),
                            "brancos" => self::BRANCOS,
                            "rateio" => IPCC::checkIfContaIsResultadoByCodDominio($partida['codigo_credito']) ? [
                                "identificador" => "05",
                                "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                                "conta_debito" => "0000000",
                                "conta_credito" => StringHelper::stringPad($partida['centro_custo_credito'], 7),
                                "valor" => StringHelper::money($valor_credito),
                                "brancos" => self::BRANCOS
                            ] : null
                        ];
                       
                    }

                } elseif (is_numeric($lancto['origem'])) { # Se for Transferencia Bancaria
                    $partidas = $lancamento->getCentroResultadoCreditoByOrigem($lancto['origem']);
                    foreach ($partidas as $partida) {
                        $valor_credito = round($lancto['valor'], 2);
                        $credito[] = [
                            "identificador" => "03",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => "0000000",
                            "conta_credito" => StringHelper::stringPad($partida['codigo_credito'], 7),
                            "valor" => StringHelper::money($valor_credito),
                            "codigo_historico" => self::CODIGO_HISTORICO,
                            "historico" => StringHelper::stringPad(StringHelper::UTF8DecodeString($lancto['credito_historico'] . $descricao), 512, 'right', " "),
                            "codigo_filial" => StringHelper::stringPad($partida['centro_custo_credito'], 7, 'left', '0'),
                            "brancos" => self::BRANCOS,
                            "rateio" => IPCC::checkIfContaIsResultadoByCodDominio($partida['codigo_credito']) ? [
                                "identificador" => "05",
                                "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                                "conta_debito" => "0000000",
                                "conta_credito" => StringHelper::stringPad($partida['centro_custo_credito'], 7),
                                "valor" => StringHelper::money($valor_credito),
                                "brancos" => self::BRANCOS
                            ] : null
                        ];
                    }
                } elseif (is_numeric($lancto['conta_tarifa'])) { # Se for Tarifas e Impostos
                    if(empty($lancto['estorno_parcela'])) {
                        $partidas = $lancamento->getCentroResultadoCreditoByTarifaImpostoContaId($lancto);
                    } else {
                        $partidas = $lancamento->getCreditoEstornoBanco($lancto);
                    }
                    foreach ($partidas as $partida) {
                        $valor_credito = round($lancto['valor'], 2);
                        $credito[] = [
                            "identificador" => "03",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => "0000000",
                            "conta_credito" => StringHelper::stringPad($partida['codigo_credito'], 7),
                            "valor" => StringHelper::money($valor_credito),
                            "codigo_historico" => self::CODIGO_HISTORICO,
                            "historico" => StringHelper::stringPad(StringHelper::UTF8DecodeString($lancto['credito_historico'] . $descricao), 512, 'right', " "),
                            "codigo_filial" => StringHelper::stringPad($partida['codigo_empresa'], 7, 'left', '0'),
                            "brancos" => self::BRANCOS,
                            "rateio" => IPCC::checkIfContaIsResultadoByCodDominio($partida['codigo_credito']) ? [
                                "identificador" => "05",
                                "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                                "conta_debito" => "0000000",
                                "conta_credito" => StringHelper::stringPad($partida['centro_custo_credito'], 7),
                                "valor" => StringHelper::money($valor_credito),
                                "brancos" => self::BRANCOS
                            ] : null
                        ];
                    }
                }
                return $credito;
            };

            if ($lancto['tipo'] == '1') {
                $conta_ir_debito = '0000000';
                $conta_ir_credito = '0000383';

                $conta_pis_debito = '0000000';
                $conta_pis_credito = '0000384';

                $conta_cofins_debito = '0000000';
                $conta_cofins_credito = '0000384';

                $conta_csll_debito = '0000000';
                $conta_csll_credito = '0000384';

                $conta_pcc_debito = '0000000';
                $conta_pcc_credito = '0000384';

                $conta_issqn_debito = '0000000';
                $conta_issqn_credito = '0000385';

                $conta_inss_debito = '0000000';
                $conta_inss_credito = '0000386';

                $conta_juros_debito = '0000810';
                $conta_juros_credito = '0000000';

                $conta_desconto_debito = '0000000';
                $conta_desconto_credito = '0000803'; 

                $conta_tarifa_cartao_debito = '0000000';
                $conta_tarifa_cartao_credito = '0000777'; 

                              

                $conta_outras_tarifas_debito = '0000810';
                $conta_outras_tarifas_credito = '0000000';

                $conta_encargos_debito = '0000811';
                $conta_encargos_credito = '0000000';
            } else {
               
                $conta_ir_debito = '0000187';
                $conta_ir_credito = '0000000';

                $conta_pis_debito = '0000190';
                $conta_pis_credito = '0000000';

                $conta_cofins_debito = '0000189';
                $conta_cofins_credito = '0000000';

                $conta_csll_debito = '0000188';
                $conta_csll_credito = '0000000';

                $conta_pcc_debito = '0000000';
                $conta_pcc_credito = '0000000';

                $conta_issqn_debito = '0000192';
                $conta_issqn_credito = '0000000';

                $conta_inss_debito = '0000191';
                $conta_inss_credito = '0000000';

                $conta_juros_debito = '0000000';
                $conta_juros_credito = '0000802';

                $conta_desconto_debito = '0000809';
                $conta_desconto_credito = '0000000';

                $conta_tarifa_cartao_debito = '0000777';
                $conta_tarifa_cartao_credito = '0000000';
                
                $conta_outras_tarifas_debito = '0000000';
                $conta_outras_tarifas_credito = '0000802';

                $conta_encargos_debito = '0000000';
                $conta_encargos_credito = '0000811';
            }

            /*
             * Nesse bloco é montado to\do o registro 03 do arquivo, onde serão encontrados os dados do
             * lançamento, as partidas de debito e seu(s) rateio(s), descontos, juros, retenções e as
             * partidas de credito e seu(s) rateio(s).
             *
             * Todas as chaves dos arrays seguem a nomenclatura prevista no leiaute de arquivo da
             * Domínio Sistemas.
             */
            
            $this->registro['dados_lote'][] = [
                "identificador" => "02",
                "codigo_sequencial" => StringHelper::stringPad($cont, 7),
                "tipo" => self::TIPO,
                "data_lancamento" => $lancto['data_lancamento'],
                "usuario" => self::USUARIO,
                "brancos" => self::BRANCOS,
                "partidas" => [
                    "debito" => $debitos($cont),
                    "juros" => (empty($lancto['juros_historico'])) ? null : [
                        "identificador" => "03",
                        "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                        "conta_debito" => $conta_juros_debito,
                        "conta_credito" => $conta_juros_credito,
                        "valor" => StringHelper::money($lancto['JurosMulta']),
                        "codigo_historico" => self::CODIGO_HISTORICO,
                        "historico" => StringHelper::stringPad(StringHelper::UTF8DecodeString($lancto['juros_historico']), 512, 'right', " "),
                        "codigo_filial" => StringHelper::stringPad($lancto['centro_custo_dominio'], 7, 'left', '0'),
                        "brancos" => self::BRANCOS,
                        "rateio" => IPCC::checkIfJurosDescontoContaIsResultadoByCodDominio($conta_juros_debito, $conta_juros_credito) ? [
                            "identificador" => "05",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => $lancto['tipo'] == '1' ? StringHelper::stringPad($centro_custo_debito, 7) : '0000000',
                            "conta_credito" => $lancto['tipo'] == '1' ? '0000000' : StringHelper::stringPad($centro_custo_credito, 7),
                            "valor" => StringHelper::money($lancto['JurosMulta']),
                            "brancos" => self::BRANCOS,
                        ] : null
                    ],
                    "desconto" => (empty($lancto['desconto_historico'])) ? null : [
                        "identificador" => "03",
                        "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                        "conta_debito" => $conta_desconto_debito,
                        "conta_credito" => $conta_desconto_credito,
                        "valor" => StringHelper::money($lancto['desconto']),
                        "codigo_historico" => self::CODIGO_HISTORICO,
                        "historico" => StringHelper::stringPad(StringHelper::UTF8DecodeString($lancto['desconto_historico']), 512, 'right', " "),
                        "codigo_filial" => StringHelper::stringPad($lancto['centro_custo_dominio'], 7, 'left', '0'),
                        "brancos" => self::BRANCOS,
                        "rateio" => IPCC::checkIfJurosDescontoContaIsResultadoByCodDominio($conta_desconto_debito, $conta_desconto_credito) ? [
                            "identificador" => "05",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => $lancto['tipo'] == '1' ? '0000000' : StringHelper::stringPad($centro_custo_debito, 7),
                            "conta_credito" => $lancto['tipo'] == '1' ? StringHelper::stringPad($centro_custo_credito, 7) : '0000000',
                            "valor" => StringHelper::money($lancto['desconto']),
                            "brancos" => self::BRANCOS,
                        ] : null
                    ],
                    "tarifaCartao" => (empty($lancto['tarifa_cartao_historico'])) ? null : [
                        "identificador" => "03",
                        "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                        "conta_debito" => $conta_tarifa_cartao_debito,
                        "conta_credito" => $conta_tarifa_cartao_credito,
                        "valor" => StringHelper::money($lancto['tarifa_cartao']),
                        "codigo_historico" => self::CODIGO_HISTORICO,
                        "historico" => StringHelper::stringPad(StringHelper::UTF8DecodeString($lancto['tarifa_cartao_historico']), 512, 'right', " "),
                        "codigo_filial" => StringHelper::stringPad($lancto['centro_custo_dominio'], 7, 'left', '0'),
                        "brancos" => self::BRANCOS,
                        "rateio" => IPCC::checkIfJurosDescontoContaIsResultadoByCodDominio($conta_tarifa_cartao_debito, $conta_tarifa_cartao_credito) ? [
                            "identificador" => "05",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => $lancto['tipo'] == '1' ? '0000000' : StringHelper::stringPad($centro_custo_debito, 7),
                            "conta_credito" => $lancto['tipo'] == '1' ? StringHelper::stringPad($centro_custo_credito, 7) : '0000000',
                            "valor" => StringHelper::money($lancto['tarifa_cartao']),
                            "brancos" => self::BRANCOS,
                        ] : null
                    ],
                    "IRRF" => (empty($lancto['IR']) || $lancto['IR'] == 0) ? null : [
                        "identificador" => "03",
                        "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                        "conta_debito" => "$conta_ir_debito",
                        "conta_credito" => "$conta_ir_credito",
                        "valor" => StringHelper::money($lancto['IR']),
                        "codigo_historico" => self::CODIGO_HISTORICO,
                        "historico" => StringHelper::stringPad('VL. REF. IRRF NF ' . $retencoes_historico, 512, 'right', " "),
                        "codigo_filial" => StringHelper::stringPad($lancto['centro_custo_dominio'], 7, 'left', '0'),
                        "brancos" => self::BRANCOS,
                        /*"rateio" => IPCC::checkIfContaIsResultadoByCodDominio($lancto['codigo_credito']) ? [
                            "identificador" => "05",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => $lancto['tipo'] == '0' ? '0000000' : StringHelper::stringPad($centro_custo_debito, 7),
                            "conta_credito" => $lancto['tipo'] == '0' ? StringHelper::stringPad($centro_custo_credito, 7) : '0000000',
                            "valor" => StringHelper::money($lancto['IR']),
                            "brancos" => self::BRANCOS,
                        ] : null*/
                    ],
                    "PIS" => (empty($lancto['PIS']) || $lancto['PIS'] == 0) ? null : [
                        "identificador" => "03",
                        "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                        "conta_debito" => "$conta_pis_debito",
                        "conta_credito" => "$conta_pis_credito",
                        "valor" => StringHelper::money($lancto['PIS']),
                        "codigo_historico" => self::CODIGO_HISTORICO,
                        "historico" => StringHelper::stringPad('VL. REF. PIS NF ' . $retencoes_historico, 512, 'right', " "),
                        "codigo_filial" => StringHelper::stringPad($lancto['centro_custo_dominio'], 7, 'left', '0'),
                        "brancos" => self::BRANCOS,
                        /*"rateio" => IPCC::checkIfContaIsResultadoByCodDominio($lancto['codigo_credito']) ? [
                            "identificador" => "05",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => $lancto['tipo'] == '0' ? '0000000' : StringHelper::stringPad($centro_custo_debito, 7),
                            "conta_credito" => $lancto['tipo'] == '0' ? StringHelper::stringPad($centro_custo_credito, 7) : '0000000',
                            "valor" => StringHelper::money($lancto['PIS']),
                            "brancos" => self::BRANCOS,
                        ] : null*/
                    ],
                    "COFINS" => (empty($lancto['COFINS']) || $lancto['COFINS'] == 0) ? null : [
                        "identificador" => "03",
                        "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                        "conta_debito" => "$conta_cofins_debito",
                        "conta_credito" => "$conta_cofins_credito",
                        "valor" => StringHelper::money($lancto['COFINS']),
                        "codigo_historico" => self::CODIGO_HISTORICO,
                        "historico" => StringHelper::stringPad('VL. REF. COFINS NF ' . $retencoes_historico, 512, 'right', " "),
                        "codigo_filial" => StringHelper::stringPad($lancto['centro_custo_dominio'], 7, 'left', '0'),
                        "brancos" => self::BRANCOS,
                        /*"rateio" => IPCC::checkIfContaIsResultadoByCodDominio($lancto['codigo_credito']) ? [
                            "identificador" => "05",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => $lancto['tipo'] == '0' ? '0000000' : StringHelper::stringPad($centro_custo_debito, 7),
                            "conta_credito" => $lancto['tipo'] == '0' ? StringHelper::stringPad($centro_custo_credito, 7) : '0000000',
                            "valor" => StringHelper::money($lancto['COFINS']),
                            "brancos" => self::BRANCOS,
                        ] : null*/
                    ],
                    "CSLL" => (empty($lancto['CSLL']) || $lancto['CSLL'] == 0) ? null : [
                        "identificador" => "03",
                        "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                        "conta_debito" => "$conta_csll_debito",
                        "conta_credito" => "$conta_csll_credito",
                        "valor" => StringHelper::money($lancto['CSLL']),
                        "codigo_historico" => self::CODIGO_HISTORICO,
                        "historico" => StringHelper::stringPad('VL. REF. CSLL NF ' . $retencoes_historico, 512, 'right', " "),
                        "codigo_filial" => StringHelper::stringPad($lancto['centro_custo_dominio'], 7, 'left', '0'),
                        "brancos" => self::BRANCOS,
                        /*"rateio" => IPCC::checkIfContaIsResultadoByCodDominio($lancto['codigo_credito']) ? [
                            "identificador" => "05",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => $lancto['tipo'] == '0' ? '0000000' : StringHelper::stringPad($centro_custo_debito, 7),
                            "conta_credito" => $lancto['tipo'] == '0' ? StringHelper::stringPad($centro_custo_credito, 7) : '0000000',
                            "valor" => StringHelper::money($lancto['CSLL']),
                            "brancos" => self::BRANCOS,
                        ] : null*/
                    ],
                    "PCC" => (empty($lancto['PCC']) || $lancto['PCC'] == 0) ? null : [
                        "identificador" => "03",
                        "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                        "conta_debito" => "$conta_pcc_debito",
                        "conta_credito" => "$conta_pcc_credito",
                        "valor" => StringHelper::money($lancto['PCC']),
                        "codigo_historico" => self::CODIGO_HISTORICO,
                        "historico" => StringHelper::stringPad('VL. REF. PCC NF ' . $retencoes_historico, 512, 'right', " "),
                        "codigo_filial" => StringHelper::stringPad($lancto['centro_custo_dominio'], 7, 'left', '0'),
                        "brancos" => self::BRANCOS,
                        /*"rateio" => IPCC::checkIfContaIsResultadoByCodDominio($lancto['codigo_credito']) ? [
                            "identificador" => "05",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => $lancto['tipo'] == '0' ? '0000000' : StringHelper::stringPad($centro_custo_debito, 7),
                            "conta_credito" => $lancto['tipo'] == '0' ? StringHelper::stringPad($centro_custo_credito, 7) : '0000000',
                            "valor" => StringHelper::money($lancto['PCC']),
                            "brancos" => self::BRANCOS,
                        ] : null*/
                    ],
                    "ISS" => (empty($lancto['ISSQN']) || $lancto['ISSQN'] == 0) ? null : [
                        "identificador" => "03",
                        "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                        "conta_debito" => "$conta_issqn_debito",
                        "conta_credito" => "$conta_issqn_credito",
                        "valor" => StringHelper::money($lancto['ISSQN']),
                        "codigo_historico" => self::CODIGO_HISTORICO,
                        "historico" => StringHelper::stringPad('VL. REF. ISS NF ' . $retencoes_historico, 512, 'right', " "),
                        "codigo_filial" => StringHelper::stringPad($lancto['centro_custo_dominio'], 7, 'left', '0'),
                        "brancos" => self::BRANCOS,
                        /*"rateio" => IPCC::checkIfContaIsResultadoByCodDominio($lancto['codigo_credito']) ? [
                            "identificador" => "05",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => $lancto['tipo'] == '0' ? '0000000' : StringHelper::stringPad($centro_custo_debito, 7),
                            "conta_credito" => $lancto['tipo'] == '0' ? StringHelper::stringPad($centro_custo_credito, 7) : '0000000',
                            "valor" => StringHelper::money($lancto['ISSQN']),
                            "brancos" => self::BRANCOS,
                        ] : null*/
                    ],
                    "INSS" => (empty($lancto['INSS']) || $lancto['INSS'] == 0) ? null : [
                        "identificador" => "03",
                        "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                        "conta_debito" => "$conta_inss_debito",
                        "conta_credito" => "$conta_inss_credito",
                        "valor" => StringHelper::money($lancto['INSS']),
                        "codigo_historico" => self::CODIGO_HISTORICO,
                        "historico" => StringHelper::stringPad('VL. REF. INSS NF ' . $retencoes_historico, 512, 'right', " "),
                        "codigo_filial" => StringHelper::stringPad($lancto['centro_custo_dominio'], 7, 'left', '0'),
                        "brancos" => self::BRANCOS,
                        /*"rateio" => IPCC::checkIfContaIsResultadoByCodDominio($lancto['codigo_credito']) ? [
                            "identificador" => "05",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => $lancto['tipo'] == '0' ? '0000000' : StringHelper::stringPad($centro_custo_debito, 7),
                            "conta_credito" => $lancto['tipo'] == '0' ? StringHelper::stringPad($centro_custo_credito, 7) : '0000000',
                            "valor" => StringHelper::money($lancto['INSS']),
                            "brancos" => self::BRANCOS,
                        ] : null*/
                    ],
                    "outras_tarifas" => (empty($lancto['outras_tarifas']) || $lancto['outras_tarifas'] == 0) ? null : [
                        "identificador" => "03",
                        "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                        "conta_debito" => $conta_outras_tarifas_debito,
                        "conta_credito" => $conta_outras_tarifas_credito,
                        "valor" => StringHelper::money($lancto['outras_tarifas']),
                        "codigo_historico" => self::CODIGO_HISTORICO,
                        "historico" => StringHelper::stringPad('VL. REF. TARIFAS ADICIONAIS DOC ' . $retencoes_historico, 512, 'right', " "),
                        "codigo_filial" => StringHelper::stringPad($lancto['centro_custo_dominio'], 7, 'left', '0'),
                        "brancos" => self::BRANCOS,
                        "rateio" => IPCC::checkIfJurosDescontoContaIsResultadoByCodDominio($conta_outras_tarifas_debito, $conta_outras_tarifas_credito) ? [
                            "identificador" => "05",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => $lancto['tipo'] == '0' ? '0000000' : StringHelper::stringPad($centro_custo_credito, 7),
                            "conta_credito" => $lancto['tipo'] == '0' ? StringHelper::stringPad($centro_custo_debito, 7) : '0000000',
                            "valor" => StringHelper::money($lancto['outras_tarifas']),
                            "brancos" => self::BRANCOS,
                        ] : null
                    ],
                    "encargos" => (empty($lancto['encargos']) || $lancto['encargos'] == 0) ? null : [
                        "identificador" => "03",
                        "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                        "conta_debito" => $conta_encargos_debito,
                        "conta_credito" => $conta_encargos_credito,
                        "valor" => StringHelper::money($lancto['encargos']),
                        "codigo_historico" => self::CODIGO_HISTORICO,
                        "historico" => StringHelper::stringPad('VL. REF. ENCARGOS FINANCEIROS DOC ' . $retencoes_historico, 512, 'right', " "),
                        "codigo_filial" => StringHelper::stringPad($lancto['centro_custo_dominio'], 7, 'left', '0'),
                        "brancos" => self::BRANCOS,
                        "rateio" => IPCC::checkIfJurosDescontoContaIsResultadoByCodDominio($conta_encargos_debito, $conta_encargos_credito) ? [
                            "identificador" => "05",
                            "codigosequencial" => StringHelper::stringPad(++$cont, 7),
                            "conta_debito" => $lancto['tipo'] == '0' ? '0000000' : StringHelper::stringPad($centro_custo_debito, 7),
                            "conta_credito" => $lancto['tipo'] == '0' ? StringHelper::stringPad($centro_custo_credito, 7) : '0000000',
                            "valor" => StringHelper::money($lancto['encargos']),
                            "brancos" => self::BRANCOS,
                        ] : null
                    ],
                    "credito" => $creditos($cont),
                ]
            ];

           
          
            $cont++;
            $contArray++;
        }
      
    }
}
