<?php

namespace App\Models\Financeiro;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class TipoContaBancaria extends AbstractModel implements DAOInterface
{

  public static function getAll()
  {
    return parent::get(self::getSQL());
  }

  public static function getSQL($id = null)
  {
    $id = isset($id) && !empty($id) ? 'WHERE 1=1 AND tipo_conta_bancaria.id = ' . $id : null;
    return "SELECT *
            FROM tipo_conta_bancaria
            {$id}
            ORDER BY nome ASC";
  }

} 