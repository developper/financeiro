<?php

namespace App\Models;

abstract class AbstractModel
{

  public static function get($sql, $mode = 'assoc')
  {
    $lista = array();
    $result = mysql_query($sql) or die (mysql_error());
    if($result){
      while ($row = mysql_fetch_array($result, ($mode === 'assoc' ? MYSQL_ASSOC : MYSQL_BOTH)))
        $lista[] = $row;
      return $lista;
    }

    return "Erro. Entre em contato com o TI";

  }

} 