<?php

namespace App\Models\Administracao;

use App\Models\AbstractModel,
    App\Models\DAOInterface;
use App\Models\Administracao\ConselhoRegional;

class Usuario extends AbstractModel implements DAOInterface
{

    public static function getAll()
    {
        return parent::get(self::getSQL());
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id), 'Assoc'));
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? 'AND idUsuarios = "' . $id . '"' : "";
        return "SELECT * FROM usuarios WHERE 1 = 1 {$id} AND block_at = '9999-12-31'";
    }

    public static function getUsersListSQL($data = [])
    {
        $compNome = "";
        $compLogin = "";
        $compUrs = "";
        $compPerfis = "";
        $compStatus = "";
        $compPadronizar = "";
        $compPrestador = "";
        $compConselhoRegional = "";

        if(!empty($data)) {
            $compNome = isset($data['nome']) && $data['nome'] != '' ?
                " AND usuarios.nome LIKE '%{$data['nome']}%'" :
                "";
            $compLogin = isset($data['login']) && $data['login'] != '' ?
                " AND usuarios.login LIKE '%{$data['login']}%'" :
                "";

            $compUrs = isset($data['ur']) && count($data['ur']) > 0 ?
                " AND usuarios.empresa IN (" . implode(', ', $data['ur']) . ")" :
                "";

            $perfis = "";
            foreach ($data['perfis'] as $perfil) {
                $perfis .= " usuarios.{$perfil} = '1' OR ";
            }
            $compPerfis = $perfis != '' ?
                " AND (" . substr_replace($perfis, ")", -3, 4) :
                "";
            if(!empty($data['status']) && isset($data['status']) ){
                $compStatus =   $data['status'] == '2' ?
                    " AND usuarios.block_at < NOW()" :
                    " AND usuarios.block_at = '9999-12-31'";
            }

            if(!empty($data['conselho_regional']) && isset($data['conselho_regional']) ){
                $compConselhoRegional = " AND conselho_regional_id = {$data['conselho_regional']}";
            }


            $compPadronizar = isset($data['padronizar']) && $data['padronizar'] == '1' ?
                " AND usuarios.permissao_padronizar_medicamento = 1" :
                "";

            $compPrestador = isset($data['prestador']) && $data['prestador'] == '1' ?
                " AND usuarios.prestador = 1" :
                " AND usuarios.prestador = 0";
        }

        return <<<SQL
SELECT
    usuarios.idUsuarios,
    usuarios.login,
    usuarios.senha,
    usuarios.nome,
    usuarios.adm,
    usuarios.tipo,
    usuarios.empresa,
    empresas.nome as nempresa,
    usuarios.modadm,
    usuarios.modaud,
    usuarios.modenf,
    usuarios.modfin,
    usuarios.modfat,
    usuarios.modlog,
    usuarios.modmed,
    usuarios.modrem,
    usuarios.modnutri,
    usuarios.modfisio,
    usuarios.modserv,
    usuarios.modfono,
    usuarios.modpsico,
    usuarios.email,
    usuarios.contato1,
    usuarios.contato2,
    usuarios.logado,
    usuarios.block_at                
FROM 
    usuarios INNER JOIN empresas ON (usuarios.empresa = empresas.id)
WHERE
    usuarios.tipo != '' 
    {$compNome}
    {$compLogin}
    {$compUrs}
    {$compPerfis}
    {$compStatus}
    {$compPadronizar}
    {$compPrestador}
SQL;
    }

    public static function save($data)
    {
        $data['senha'] = md5($data['senha']);
        $labelConselho = '';
        if(!empty($data['conselho_regional'])) {
            $arrayConselhoRegional = ConselhoRegional::getById($data['conselho_regional']);
            $labelConselho = "({$arrayConselhoRegional['sigla']}: {$data['num_registro']})";
        }

        $modadm = in_array('modadm', $data['acesso']) ? 1 : 0;
        $modaud = in_array('modaud', $data['acesso']) ? 1 : 0;
        $modenf = in_array('modenf', $data['acesso']) ? 1 : 0;
        $modfin = in_array('modfin', $data['acesso']) ? 1 : 0;
        $modfat = in_array('modfat', $data['acesso']) ? 1 : 0;
        $modlog = in_array('modlog', $data['acesso']) ? 1 : 0;
        $modmed = in_array('modmed', $data['acesso']) ? 1 : 0;
        $modrem = in_array('modrem', $data['acesso']) ? 1 : 0;
        $modnutri = in_array('modnutri', $data['acesso']) ? 1 : 0;
        $modfisio = in_array('modfisio', $data['acesso']) ? 1 : 0;
        $modserv = in_array('modserv', $data['acesso']) ? 1 : 0;
        $modfono = in_array('modfono', $data['acesso']) ? 1 : 0;
        $modpsico = in_array('modpsico', $data['acesso']) ? 1 : 0;

        $data['padronizar'] = $data['padronizar'] == '1' ? 'S' : 'N';

        mysql_query('BEGIN');

        $sql = <<<SQL
INSERT INTO `usuarios` (
    `login` ,  
    `senha` , 
    `password`,  
    `nome` ,  
    `numero_conselho_regional` ,  
    `adm` ,  
    `tipo`, 
    `empresa`, 
    `modadm`, 
    `modaud`, 
    `modenf`, 
    `modfin`, 
    `modfat`, 
    `modlog`,
    `modmed`, 
    `modrem`,
    `modnutri`,
    `modfisio`, 
    `modserv`, 
    `modfono`, 
    `modpsico`,
    ADM_UR,
    permissao_padronizar_medicamento, 
    prestador,
    conselho_regional_id,
    conselho_regional
 ) VALUES( 
    '{$data['login']}',
    '{$data['senha']}', 
    '{$data['senha']}', 
    '{$data['nome']}', 
    '{$data['num_registro']}',  
    '{$data['adm_geral']}',  
    '{$data['perfil']}', 
    '{$data['empresa_principal']}',
    '{$modadm}', 
    '{$modaud}', 
    '{$modenf}', 
    '{$modfin}', 
    '{$modfat}', 
    '{$modlog}', 
    '{$modmed}', 
    '{$modrem}',
    '{$modnutri}',
    '{$modfisio}',
    '{$modserv}', 
    '{$modfono}', 
    '{$modpsico}',
    '{$data['adm_ur']}',
    '{$data['padronizar']}', 
    '{$data['prestador']}',
    '{$data['conselho_regional']}',
    '{$labelConselho}'

)
SQL;
        $rs = mysql_query($sql);

        if(!$rs) {
            mysql_query('ROLLBACK');
            return "Erro ao salvar o usuário!";
        }

        $usuario_id = mysql_insert_id();

        foreach ($data['setor'] as $s) {
            $sql = <<<SQL
INSERT INTO usuarios_setores (
    usuario_id,
    setor_id
) VALUES (

  '{$usuario_id}',
  '{$s}'
)
SQL;
            $rs = mysql_query($sql);

            if(!$rs) {
                mysql_query('ROLLBACK');
                return "Erro ao salvar o(s) setor(es) do usuário!";
            }
        }

        foreach ($data['ur'] as $ur) {
            $sql = <<<SQL
INSERT INTO usuarios_empresa (
    ID, 
    usuarios_id, 
    empresas_id
) VALUES (
  NULL,
  '{$usuario_id}',
  '{$ur}'
)
SQL;
            $rs = mysql_query($sql);

            if(!$rs) {
                mysql_query('ROLLBACK');
                return "Erro ao salvar as urs do usuário!";
            }
        }

        foreach ($data['areas'] as $area) {
            $sql = <<<SQL
INSERT INTO usuarios_area (
    id, 
    area_id, 
    usuario_id
) VALUES (
  NULL,
  '{$area}',
  '{$usuario_id}'
)
SQL;
            $rs = mysql_query($sql);

            if(!$rs) {
                mysql_query('ROLLBACK');
                return "Erro ao salvar as areas do usuário!";
            }
        }

        foreach ($data['permissoes'] as $permissao) {
            $sql = <<<SQL
INSERT INTO usuarios_permissao (
    id, 
    permissao_id, 
    usuario_id
) VALUES (
  NULL,
  '{$permissao}',
  '{$usuario_id}'
)
SQL;
            $rs = mysql_query($sql);

            if(!$rs) {
                mysql_query('ROLLBACK');
                return "Erro ao salvar as permissoes do usuário!";
            }
        }

        return mysql_query('COMMIT');
    }

    public static function update($data)
    {
        $data['senha'] = $data['senha'] != '' ?
            "senha = '" . md5($data['senha']) . "', password = '" . md5($data['senha']) . "', " :
            "";
        $labelConselho = '';
        if(!empty($data['conselho_regional'])) {
            $arrayConselhoRegional = ConselhoRegional::getById($data['conselho_regional']);
            $labelConselho = "({$arrayConselhoRegional['sigla']}: {$data['num_registro']})";
        }

        $modadm = in_array('modadm', $data['acesso']) ? 1 : 0;
        $modaud = in_array('modaud', $data['acesso']) ? 1 : 0;
        $modenf = in_array('modenf', $data['acesso']) ? 1 : 0;
        $modfin = in_array('modfin', $data['acesso']) ? 1 : 0;
        $modfat = in_array('modfat', $data['acesso']) ? 1 : 0;
        $modlog = in_array('modlog', $data['acesso']) ? 1 : 0;
        $modmed = in_array('modmed', $data['acesso']) ? 1 : 0;
        $modrem = in_array('modrem', $data['acesso']) ? 1 : 0;
        $modnutri = in_array('modnutri', $data['acesso']) ? 1 : 0;
        $modfisio = in_array('modfisio', $data['acesso']) ? 1 : 0;
        $modserv = in_array('modserv', $data['acesso']) ? 1 : 0;
        $modfono = in_array('modfono', $data['acesso']) ? 1 : 0;
        $modpsico = in_array('modpsico', $data['acesso']) ? 1 : 0;

        $data['padronizar'] = $data['padronizar'] == '1' ? 'S' : 'N';

        mysql_query('BEGIN');

        $sql = <<<SQL
UPDATE `usuarios` SET
    {$data['senha']}
    `login` = '{$data['login']}',    
    `nome` = '{$data['nome']}',  
    `numero_conselho_regional` = '{$data['num_registro']}',  
    `adm` = '{$data['adm_geral']}',  
    `tipo`= '{$data['perfil']}', 
    `empresa`= '{$data['empresa_principal']}', 
    `modadm` = '{$modadm}', 
    `modaud` = '{$modaud}', 
    `modenf` = '{$modenf}', 
    `modfin` = '{$modfin}', 
    `modfat` = '{$modfat}', 
    `modlog` = '{$modlog}',
    `modmed` = '{$modmed}', 
    `modrem` = '{$modrem}',
    `modnutri` = '{$modnutri}',
    `modfisio` = '{$modfisio}', 
    `modserv` = '{$modserv}', 
    `modfono` = '{$modfono}', 
    `modpsico` = '{$modpsico}',
    ADM_UR = '{$data['adm_ur']}',
    permissao_padronizar_medicamento = '{$data['padronizar']}', 
    prestador = '{$data['prestador']}',
    conselho_regional_id = '{$data['conselho_regional']}',
    conselho_regional = '{$labelConselho}'

WHERE 
    idUsuarios = '{$data['id']}' 
SQL;
        $rs = mysql_query($sql);

        if(!$rs) {
            mysql_query('ROLLBACK');
            return "Erro ao salvar o usuário!";
        }

        $sql = <<<SQL
DELETE FROM usuarios_setores WHERE usuario_id = '{$data['id']}'
SQL;
        $rs = mysql_query($sql);
        if(!$rs) {
            mysql_query('ROLLBACK');
            return "Erro ao atualizar setores!";
        }

        foreach ($data['setor'] as $s) {
            $sql = <<<SQL
INSERT INTO usuarios_setores (
    usuario_id,
    setor_id
) VALUES (

  '{$data['id']}',
  '{$s}'
)
SQL;
            $rs = mysql_query($sql);

            if(!$rs) {
                mysql_query('ROLLBACK');
                return "Erro ao salvar o(s) setor(es) do usuário!";
            }
        }

        $sql = <<<SQL
DELETE FROM usuarios_empresa WHERE usuarios_id = '{$data['id']}'
SQL;
        $rs = mysql_query($sql);

        foreach ($data['ur'] as $ur) {
            $sql = <<<SQL
INSERT INTO usuarios_empresa (
    ID, 
    usuarios_id, 
    empresas_id
) VALUES (
  NULL,
  '{$data['id']}',
  '{$ur}'
)
SQL;
            $rs = mysql_query($sql);

            if(!$rs) {
                mysql_query('ROLLBACK');
                return "Erro ao salvar as urs do usuário!";
            }
        }

        $sql = <<<SQL
DELETE FROM usuarios_area WHERE usuario_id = '{$data['id']}'
SQL;
        $rs = mysql_query($sql);

        foreach ($data['areas'] as $area) {
            $sql = <<<SQL
INSERT INTO usuarios_area (
    id, 
    area_id, 
    usuario_id
) VALUES (
  NULL,
  '{$area}',
  '{$data['id']}'
)
SQL;
            $rs = mysql_query($sql);

            if(!$rs) {
                mysql_query('ROLLBACK');
                return "Erro ao salvar as areas do usuário!";
            }
        }

        $sql = <<<SQL
DELETE FROM usuarios_permissao WHERE usuario_id = '{$data['id']}'
SQL;
        $rs = mysql_query($sql);

        foreach ($data['permissoes'] as $permissao) {
            $sql = <<<SQL
INSERT INTO usuarios_permissao (
    id, 
    permissao_id, 
    usuario_id
) VALUES (
  NULL,
  '{$permissao}',
  '{$data['id']}'
)
SQL;
            $rs = mysql_query($sql);

            if(!$rs) {
                mysql_query('ROLLBACK');
                return "Erro ao salvar as permissoes do usuário!";
            }
        }

        return mysql_query('COMMIT');
    }

    public static function getEmpresasUsuario($id)
    {
        $sql = <<<SQL
SELECT
  *
FROM 
  usuarios_empresa
WHERE
  usuarios_id = '{$id}'
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getAreasUsuario($id)
    {
        $sql = <<<SQL
SELECT
  *
FROM 
  usuarios_area
WHERE
  usuario_id = '{$id}'
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getPermissoesUsuario($id)
    {
        $sql = <<<SQL
SELECT
  *
FROM 
  usuarios_permissao
WHERE
  usuario_id = '{$id}'
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getUserEmpresas($users)
    {
        $response = [];

        foreach ($users as $user) {
            $sql = <<<SQL
SELECT
    ue.usuarios_id,		  		
    e.nome AS empresa                
FROM 
    `usuarios_empresa` AS ue LEFT JOIN
    `empresas` AS e ON (ue.empresas_id = e.id)
WHERE
    ue.usuarios_id = '{$user['idUsuarios']}' 	
SQL;

            $response[$user['idUsuarios']] = parent::get($sql, 'Assoc');
        }

        return $response;
    }

    public static function getSetoresUsuario($id)
    {
        $sql = <<<SQL
SELECT
    us.setor_id,
    s.setor
FROM
    `usuarios_setores` AS us LEFT JOIN
    `setores` AS s ON (us.setor_id = s.id)
WHERE
    us.usuario_id = '{$id}'
SQL;
        return  parent::get($sql, 'Assoc');
    }

    public static function getUsersByRole($role, $ativo = NULL)
    {
        $condAtivo = empty($ativo) ? " AND block_at = '9999-12-31'" : '';

        $sql = "SELECT * FROM usuarios WHERE {$role} = '1'  $condAtivo  AND adm = 0 ORDER BY nome";
        return parent::get($sql, 'assoc');
    }

    public static function getUsersByTipo($tipo)
    {
        $sql = "SELECT * FROM usuarios WHERE tipo = '{$tipo}' AND block_at = '9999-12-31' ORDER BY nome";
        return parent::get($sql, 'assoc');
    }

    public static function getUsersByUR()
    {
        $sql = "SELECT * FROM usuarios WHERE empresa = '{$_SESSION['empresa_principal']}' AND block_at = '9999-12-31' ORDER BY nome";
        return parent::get($sql, 'assoc');
    }


    public static function getAssinaturaPath($id)
    {
        $sql = "SELECT ASSINATURA FROM usuarios WHERE idUsuarios = '{$id}'";
        return current(parent::get($sql, 'assoc'));
    }

    public static function getPermissoes($usuario)
    {
        $response = [];

        $sql = <<<SQL
SELECT 
  area_sistema.id,
  area_sistema.nome
FROM 
  usuarios_area
  INNER JOIN area_sistema ON usuarios_area.area_id = area_sistema.id
WHERE 
  usuarios_area.usuario_id = '{$usuario}'
  AND area_sistema.id_pai = 0
SQL;
        $userAreaPais = parent::get($sql, 'Assoc');
        //echo '<pre>'; die(print_r($userAreas));

        foreach ($userAreaPais as $userAreaPai) {
            $response[$userAreaPai['nome']] = [];

            $sql = <<<SQL
SELECT 
  area_sistema.id,
  area_sistema.nome
FROM 
  usuarios_area
  INNER JOIN area_sistema ON usuarios_area.area_id = area_sistema.id
WHERE 
  usuarios_area.usuario_id = '{$usuario}'
  AND area_sistema.id_pai = '{$userAreaPai['id']}'
SQL;
            $userAreaFilhos = parent::get($sql, 'Assoc');

            foreach ($userAreaFilhos as $userAreaFilho) {
                $response[$userAreaPai['nome']][$userAreaFilho['nome']] = [];

                $sql = <<<SQL
SELECT 
  area_sistema.id,
  area_sistema.nome
FROM 
  usuarios_area
  INNER JOIN area_sistema ON usuarios_area.area_id = area_sistema.id
WHERE 
  usuarios_area.usuario_id = '{$usuario}'
  AND area_sistema.id_pai = '{$userAreaFilho['id']}'
SQL;
                $userAreaSubFilhos = parent::get($sql, 'Assoc');

                if (count($userAreaSubFilhos) > 0) {
                    foreach ($userAreaSubFilhos as $userAreaSubFilho) {
                        $response[$userAreaPai['nome']][$userAreaFilho['nome']][$userAreaSubFilho['nome']] = [];

                        $sql = <<<SQL
SELECT 
  area_permissoes.acao 
FROM 
  usuarios_permissao
  INNER JOIN area_permissoes ON usuarios_permissao.permissao_id = area_permissoes.id
WHERE 
  usuario_id = '{$usuario}'
  AND area_permissoes.area_id = '{$userAreaSubFilho['id']}'
SQL;
                        $userPermissoes = parent::get($sql, 'Assoc');

                        foreach ($userPermissoes as $userPermissao) {
                            $response[$userAreaPai['nome']]
                            [$userAreaFilho['nome']]
                            [$userAreaSubFilho['nome']]
                            [$userPermissao['acao']] = 'true';
                        }
                    }
                } else {
                    $sql = <<<SQL
SELECT 
  area_permissoes.acao 
FROM 
  usuarios_permissao
  INNER JOIN area_permissoes ON usuarios_permissao.permissao_id = area_permissoes.id
WHERE 
  usuario_id = '{$usuario}'
  AND area_permissoes.area_id = '{$userAreaFilho['id']}'
SQL;
                    $userPermissoes = parent::get($sql, 'Assoc');

                    foreach ($userPermissoes as $userPermissao) {
                        $response[$userAreaPai['nome']]
                        [$userAreaFilho['nome']]
                        [$userPermissao['acao']] = 'true';
                    }
                }
            }
        }
        
        return $response;
    }

    public static function block($data)
    {
        mysql_query("BEGIN");

        $sql = <<<SQL
UPDATE usuarios SET 
  block_at = '{$data['data_bloqueio']}'
WHERE
  idUsuarios = '{$data['usuario_id']}'
SQL;
        $rs = mysql_query($sql);

        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        $sql = <<<SQL
INSERT INTO usuario_historico_status 
(
  id,
  usuario_id_modificado,
  usuario_id_modificador,
  data_modificado,
  data,
  observacao,
  acao
) VALUES 
(
  NULL,
  '{$data['usuario_id']}',
  '{$_SESSION['id_user']}',
  '{$data['data_bloqueio']}',
  NOW(),
  '{$data['motivo_bloqueio']}',
  'DESATIVAR'
)
SQL;
        $rs = mysql_query($sql);

        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }
        return mysql_query('COMMIT');
    }

    public static function activate($data)
    {
        mysql_query("BEGIN");

        $sql = <<<SQL
UPDATE usuarios SET 
  block_at = '9999-12-31'
WHERE
  idUsuarios = '{$data['usuario_id']}'
SQL;
        $rs = mysql_query($sql);

        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }

        $sql = <<<SQL
INSERT INTO usuario_historico_status 
(
  id,
  usuario_id_modificado,
  usuario_id_modificador,
  data_modificado,
  data,
  observacao,
  acao
) VALUES 
(
  NULL,
  '{$data['usuario_id']}',
  '{$_SESSION['id_user']}',
  NOW(),
  NOW(),
  'Usuário Ativado',
  'ATIVAR'
)
SQL;
        $rs = mysql_query($sql);

        if(!$rs) {
            mysql_query('ROLLBACK');
            return false;
        }
        return mysql_query('COMMIT');
    }

    public static function getFilialUsuario()
    {
        $ur = $_SESSION['empresa_principal'];
        $filiais = [
            1 => ['1', '5', '7', '11', '13', '14'],
            2 => ['2', '12'],
            3 => ['4'],
        ];

        if(in_array($ur, $filiais[1])) {
            return 1;
        } elseif(in_array($ur, $filiais[2])) {
            return 2;
        } elseif(in_array($ur, $filiais[3])) {
            return 3;
        }
    }
}
