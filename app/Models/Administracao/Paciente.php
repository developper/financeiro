<?php

namespace App\Models\Administracao;

use App\Models\AbstractModel;
use App\Models\DAOInterface;

class Paciente extends AbstractModel implements DAOInterface
{

    public static function getAll($notInStatus = null)
    {
        return parent::get(self::getSQL(null, $notInStatus), 'Assoc');
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id), 'Assoc'));
    }

    public static function getSQL($id = null, $notInStatus = null)
    {
        $id = isset($id) && !empty($id) ? 'AND idClientes = "' . $id . '"' : "";
        $notInStatus = isset($notInStatus) && !empty($notInStatus) ? 'AND status NOT IN (' . implode(',', $notInStatus) . ')'  : "";
        return <<<SQL
SELECT 
  clientes.*, 
  relacionamento_pessoa.RELACIONAMENTO as relac,
  empresas.nome AS ur,
  planosdesaude.nome AS nomeplano,
  cid10.descricao AS descricao_cid10
FROM 
  clientes 
  LEFT JOIN relacionamento_pessoa ON clientes.relcuid = relacionamento_pessoa.ID 
  LEFT JOIN empresas ON clientes.empresa = empresas.id 
  LEFT JOIN planosdesaude ON clientes.convenio = planosdesaude.id 
  LEFT JOIN cid10 ON clientes.diagnostico = cid10.codigo COLLATE utf8_general_ci
  WHERE 
  1=1 
  {$id} 
  {$notInStatus} 
ORDER BY 
  clientes.nome
SQL;

    }

    public static function getCabecalho($id)
    {
        return current(parent::get(self::getCabecalhoSQL($id)));
    }

    public static function getPacientesByUr()
    {
        $ur_usuario = $_SESSION['empresa_principal'];
        $comp = $ur_usuario != '1' ? "AND (clientes.status = '1' OR clientes.empresa IN {$_SESSION['empresa_user']})" : "";
        $sql = <<<SQL
SELECT
	clientes.idClientes AS id,
	clientes.nome,
	clientes.codigo,
	clientes.convenio,
	clientes.empresa,
	clientes.LIMINAR AS liminar,
	PORCENTAGEM AS porcentagem,
	statuspaciente.status
FROM
	clientes
	INNER JOIN statuspaciente ON clientes.status = statuspaciente.id
WHERE
	1=1
	{$comp}
ORDER BY nome
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getPacientesByOperadora($operadora)
    {
        $ur_usuario = $_SESSION['empresa_principal'];
        $comp = $ur_usuario != '1' ? "AND (clientes.status = '1' OR clientes.empresa IN {$_SESSION['empresa_user']})" : "";

        $sql = "SELECT PLANOSDESAUDE_ID FROM operadoras_planosdesaude WHERE OPERADORAS_ID = '{$operadora}'";
        $response = parent::get($sql, 'assoc');
        $planos = '';
        $i = 0;
        $count = count($response);
        foreach ($response as $row) {
            if($count < $i) {
                $planos .= $row['PLANOSDESAUDE_ID'] . ', ';
            } else {
                $planos .= $row['PLANOSDESAUDE_ID'];
            }
            $i++;
        }

        $sql = <<<SQL
SELECT
	clientes.idClientes AS id,
	clientes.nome,
	clientes.codigo,
	clientes.convenio,
	clientes.empresa,
	clientes.LIMINAR AS liminar,
	PORCENTAGEM AS porcentagem,
	statuspaciente.status
FROM
	clientes
	INNER JOIN statuspaciente ON clientes.status = statuspaciente.id
WHERE
	clientes.convenio IN ({$planos})
	{$comp}
ORDER BY nome
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getPacientesPaginate($filters = null)
    {
        $ur_usuario = $_SESSION['empresa_principal'];

        if($ur_usuario != '1') {
            $compUr = " AND clientes.empresa IN {$_SESSION['empresa_user']} ";
        } else {
            $compUr = "";
        }

        $compStatus = '';
        $compLiminar = '';
        $compConvenio = '';

        if($filters['todos'] == '') {
            if($ur_usuario != '1') {
                $compUr = isset($filters['empresa']) && $filters['empresa'] != '' ?
                    " AND clientes.empresa = '{$filters['empresa']}' " :
                    " AND clientes.empresa IN {$_SESSION['empresa_user']} ";
            } else {
                $compUr = isset($filters['empresa']) && $filters['empresa'] != '' ?
                    " AND clientes.empresa = '{$filters['empresa']}' " :
                    "";
            }

            $compStatus = isset($filters['status']) && $filters['status'] != '' ?
                " AND clientes.status = '{$filters['status']}' " :
                "";

            $compLiminar = isset($filters['liminar']) && $filters['liminar'] != '' ?
                " AND clientes.LIMINAR = '{$filters['liminar']}' " :
                "";

            $compConvenio = isset($filters['convenio']) && $filters['convenio'] != '' ?
                " AND clientes.convenio = '{$filters['convenio']}' " :
                "";
        }

        $compNomeCod = isset($filters['nomeSearch']) && $filters['nomeSearch'] != '' ?
            " AND ( clientes.nome LIKE '%{$filters['nomeSearch']}%' OR clientes.codigo LIKE '%{$filters['nomeSearch']}%' )" :
            "";

        $offset = ($filters['pageno'] - 1) * $filters['itemsPerPage'];
        $perPage = $filters['itemsPerPage'];

        $sql = <<<SQL
SELECT
	idClientes
FROM
	clientes
WHERE
	1=1
	{$compUr}
	{$compStatus}
	{$compLiminar}
	{$compConvenio}
	{$compNomeCod}
ORDER BY 
  nome
SQL;
        $total_count = count(parent::get($sql, 'assoc'));

        $sql = <<<SQL
SELECT
	clientes.idClientes AS id,
	UPPER(clientes.nome) AS nome,
	clientes.codigo,
	clientes.convenio,
	clientes.empresa,
	clientes.LIMINAR AS liminar,
	PORCENTAGEM AS porcentagem,
	UPPER(statuspaciente.status) AS status
FROM
	clientes
	INNER JOIN statuspaciente ON clientes.status = statuspaciente.id
WHERE
	1=1
	{$compUr}
	{$compStatus}
	{$compLiminar}
	{$compConvenio}
	{$compNomeCod}
GROUP BY 
    clientes.idClientes
ORDER BY 
  nome
LIMIT {$offset}, {$perPage}
SQL;
        $data = parent::get($sql, 'Assoc');
        $pacientes = array_map(function($value) {
            return $value['id'];
        }, $data);

        $historicoPacientes = [];

        if(count($data) > 0) {
            $historicos = self::getHistoricoMudancaStatusPacientes($pacientes);
            foreach ($historicos as $historico) {
                $historicoPacientes[$historico['paciente']] = $historico;
            }
        }

        $response = [];
        foreach ($data as $key => $row) {
            $response[] = $row;
            $response[$key]['ultima_observacao'] = '';
            $response[$key]['data'] = '';
            $response[$key]['usuario'] = '';
            if (isset($historicoPacientes[$row['id']])) {
                $response[$key]['ultima_observacao'] = $historicoPacientes[$row['id']]['ultima_observacao'];
                $response[$key]['data'] = $historicoPacientes[$row['id']]['data'];
                $response[$key]['usuario'] = $historicoPacientes[$row['id']]['usuario'];
            }
        }
        return [$total_count, $response];
    }

    private static function getHistoricoMudancaStatusPacientes($pacientes)
    {
        $pacientes = implode(', ', $pacientes);
        $sql = <<<SQL
SELECT
 PACIENTE_ID AS paciente,
 usuarios.nome as usuario,
 DATE_FORMAT(DATA, '%d/%m/%Y à\s %H:%i:%s') AS data,
 COALESCE(OBSERVACAO, '') AS ultima_observacao
FROM
 historico_status_paciente
 INNER JOIN usuarios ON (historico_status_paciente.USUARIO_ID = usuarios.idUsuarios)
WHERE 
  historico_status_paciente.PACIENTE_ID IN ({$pacientes})
ORDER BY
 ID DESC
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getPacientesByStatus($status = null)
    {
        $compStatus = is_array($status) && count($status) > 0 ? " AND clientes.status IN (" . implode(',', $status) . ")" : " AND clientes.status = '{$status}' ";
        if($status == null) {
            $compStatus = "";
        }
        $ur_usuario = $_SESSION['empresa_principal'];
        $comp = $ur_usuario != '1' ? "AND (clientes.status = '1' OR clientes.empresa IN {$_SESSION['empresa_user']})" : "";
        $sql = <<<SQL
SELECT
	clientes.idClientes AS id,
	clientes.nome,
	clientes.codigo,
	clientes.convenio,
	clientes.empresa,
	clientes.LIMINAR AS liminar,
	PORCENTAGEM AS porcentagem,
	statuspaciente.status
FROM
	clientes
	INNER JOIN statuspaciente ON clientes.status = statuspaciente.id
WHERE
	1=1
	{$compStatus}
	{$comp}
ORDER BY nome
SQL;
        //echo '<pre>'; die($sql);
        return parent::get($sql, 'assoc');
    }

    public static function getPacientesAdm()
    {
        $ur_usuario = $_SESSION['empresa_principal'];
        $comp = $ur_usuario != '1' ? "AND (c.status = '1' OR c.empresa IN {$_SESSION['empresa_user']})" : "";
        $sql = <<<SQL
SELECT
	c.idClientes AS id,
	c.nome,
	c.codigo,
	c.convenio,
	c.empresa,
	hsp.ultima_observacao,
	hsp.usuario,
	hsp.data,
	c.LIMINAR AS liminar,
	c.PORCENTAGEM AS porcentagem,
	s.status
FROM
	clientes AS c
	INNER JOIN statuspaciente AS s ON (c.STATUS = s.id)
	LEFT JOIN (
				SELECT
					u.nome as usuario,
					DATE_FORMAT(DATA, '%d/%m/%Y à\s %H:%i:%s') AS data,
					PACIENTE_ID,
					OBSERVACAO AS ultima_observacao
				FROM
					historico_status_paciente INNER JOIN usuarios AS u ON (historico_status_paciente.USUARIO_ID = u.idUsuarios)
				ORDER BY
					ID DESC
			  ) AS hsp ON (c.idClientes = hsp.PACIENTE_ID)
WHERE
	1=1
	{$comp}
GROUP BY
	c.idClientes
ORDER BY
	nome ASC
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getModalidadePacienteById($id)
    {
        $sql = <<<SQL
SELECT
	max( hmp.id ) AS maior_mod,
	mhc.modalidade
FROM
	historico_modalidade_paciente AS hmp
	INNER JOIN modalidade_home_care AS mhc ON hmp.modalidade = mhc.id
WHERE
	paciente = '{$id}'
SQL;
        return current(parent::get($sql));
    }

    public static function getDataAdmissaoPacienteById($id)
    {
        $sql = <<<SQL
SELECT
	max( ID ) AS maior_admissao,
	DATA_ADMISSAO
FROM
	historico_status_paciente AS hsp
WHERE
	PACIENTE_ID = '{$id}'
SQL;
        return current(parent::get($sql));
    }

    public static function getUltimaProrrogacaoMedica($id, $inicio, $fim)
    {
        $inicio = \DateTime::createFromFormat('d/m/Y', $inicio)->format('Y-m-d');
        $fim = \DateTime::createFromFormat('d/m/Y', $fim)->format('Y-m-d');
        $sql = <<<SQL
SELECT
	ID,
	justificativa AS just_med,
	quadro_clinico,
	regime_atual,
	indicacao_tecnica,
	usuarios.nome as medico,
	usuarios.conselho_regional
FROM
	relatorio_prorrogacao_med
	INNER JOIN usuarios ON usuarios.idUsuarios = relatorio_prorrogacao_med.USUARIO_ID
WHERE
	PACIENTE_ID = '{$id}'
	AND INICIO BETWEEN '{$inicio}' AND '{$fim}'
	AND FIM BETWEEN '{$inicio}' AND '{$fim}'
ORDER BY ID DESC LIMIT 1
SQL;
        return current(parent::get($sql));
    }

    public static function getProfissionaisUltimaProrrogacaoMedica($id)
    {
        $sql = <<<SQL
SELECT
	p.*,
	f.frequencia as frequencia
FROM
	profissionais_relatorio_prorrogacao_med as p INNER JOIN
	frequencia as f ON (p.FREQUENCIA_ID = f.id)
WHERE
	RELATORIO_PRORROGACAO_MED_ID = {$id}

SQL;
        return parent::get($sql);

    }

    public static function getUltimaProrrogacaoEnfermagem($id, $inicio, $fim)
    {
        $inicio = \DateTime::createFromFormat('d/m/Y', $inicio)->format('Y-m-d');
        $fim = \DateTime::createFromFormat('d/m/Y', $fim)->format('Y-m-d');
        $sql = <<<SQL
SELECT
	ID,
	EVOLUCAO_CLINICA,
	usuarios.nome as enfermeiro
FROM
	relatorio_prorrogacao_enf
	INNER JOIN usuarios ON usuarios.idUsuarios = relatorio_prorrogacao_enf.USUARIO_ID
WHERE
	PACIENTE_ID = '{$id}'
	AND INICIO BETWEEN '{$inicio}' AND '{$fim}'
	AND FIM BETWEEN '{$inicio}' AND '{$fim}'
ORDER BY ID DESC LIMIT 1
SQL;
        return current(parent::get($sql));
    }

    public static function getPacientesByFilter($filtersCapMedica, $filtersOthers, $filtroHistorico)
    {
        if(!is_null($filtersCapMedica)){
            $termsCapMedica = array_reduce($filtersCapMedica, function($term, $filter){
                return $term .= $filter;
            });
        }
        if(!is_null($filtersOthers)){
            $termsOthers = array_reduce($filtersOthers, function($term, $filter){
                return $term .= $filter;
            });
        }
        if(!is_null($filtroHistorico)){
            $termsHistorico = array_reduce($filtroHistorico, function($term, $filter){
                return $term .= $filter;
            });
        }

        $sql = <<<SQL
SELECT
	c.idClientes AS paciente,
	mhc.modalidade,
	cm.`data` AS data_mudanca_unformated,
	DATE_FORMAT(cm.`data`, '%d/%m/%Y') AS data_mudanca,
	'Ficha de Avaliação' AS source
FROM
	clientes AS c
INNER JOIN capmedica AS cm ON (cm.paciente = c.idClientes)
INNER JOIN modalidade_home_care AS mhc ON (mhc.id = cm.MODALIDADE)
WHERE
	cm.MODALIDADE != 0
	{$termsCapMedica}
UNION
	SELECT
		c.idClientes AS paciente,
		mhc.modalidade,
		rpm.INICIO AS data_mudanca_unformated,
		CONCAT('Inicio: ', DATE_FORMAT(rpm.INICIO, '%d/%m/%Y'), ' - Fim: ',  DATE_FORMAT(rpm.FIM, '%d/%m/%Y')) AS data_mudanca,
		'Ficha de Prorrogação' AS source
	FROM
		clientes AS c
	INNER JOIN relatorio_prorrogacao_med AS rpm ON (
		rpm.PACIENTE_ID = c.idClientes
	)
	INNER JOIN modalidade_home_care AS mhc ON (mhc.id = rpm.MOD_HOME_CARE)
	WHERE
		MOD_HOME_CARE != 0
		{$termsOthers}
	UNION
		SELECT
			c.idClientes AS paciente,
			mhc.modalidade,
			fme.`DATA` AS data_mudanca_unformated,
			DATE_FORMAT(fme.`DATA`, '%d/%m/%Y') AS data_mudanca,
			'Ficha de Evolução' AS source
		FROM
			clientes AS c
		INNER JOIN fichamedicaevolucao AS fme ON (
			fme.PACIENTE_ID = c.idClientes
		)
		INNER JOIN modalidade_home_care AS mhc ON (mhc.id = fme.MOD_HOME_CARE)
		WHERE
			MOD_HOME_CARE != 0
			{$termsOthers}
		UNION
			SELECT
				c.idClientes AS paciente,
				mhc.modalidade,
				hmp.data_mudanca AS data_mudanca_unformated,
				DATE_FORMAT(
					hmp.data_mudanca,
					'%d/%m/%Y'
				) AS data_mudanca,
				'Mudança via cadastro de paciente' AS source
			FROM
				clientes AS c
			INNER JOIN historico_modalidade_paciente AS hmp ON (hmp.paciente = c.idClientes)
			INNER JOIN modalidade_home_care AS mhc ON (mhc.id = hmp.modalidade)
			WHERE
				1 = 1
			{$termsHistorico}
		ORDER BY
			data_mudanca_unformated ASC
SQL;
        return parent::get($sql);
    }

    public static function getCabecalhoSQL($id)
    {
        $sql = <<<SQL
		SELECT
			c.idClientes as paciente_id,
			UPPER(c.nome) AS paciente,
			(CASE c.sexo
				WHEN '0' THEN 'Masculino'
				WHEN '1' THEN 'Feminino'
			END) AS sexo,
			FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
			e.nome as empresa,
			DATE_FORMAT(c.`nascimento`,'%d/%m/%Y') as nascimento,
			p.nome as convenio,
			c.NUM_MATRICULA_CONVENIO as matricula,
			c.diagnostico,
			c.convenio AS plano_id
		FROM
			clientes AS c LEFT JOIN
			empresas AS e ON (e.id = c.empresa) INNER JOIN
			planosdesaude as p ON (p.id = c.convenio)
		WHERE
			c.idClientes = '{$id}'
		ORDER BY
		c.nome DESC LIMIT 1

SQL;
        return $sql;
    }

    public static function getPacientesAtendidosByPeriodo($inicio, $fim)
    {
        $inicio = $inicio->format('Y-m-d');
        $fim = $fim->format('Y-m-d');

        $sql = <<<SQL
SELECT
  PACIENTE_ID AS paciente,
  clientes.nome,
  clientes.codigo,
  DATA_INICIO AS inicio
FROM
  clientes
  INNER JOIN fatura ON clientes.idClientes = fatura.PACIENTE_ID
WHERE
  ( 
    fatura.DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}'
    OR fatura.DATA_FIM BETWEEN '{$inicio}' AND '{$fim}'
  )
  AND fatura.ORCAMENTO = 0
  AND fatura.PACIENTE_ID NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
GROUP BY 
  PACIENTE_ID
HAVING COUNT(fatura.ID) > 0
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getPacientesAvaliados($pacientes, $inicio, $fim)
    {
        $pacientes = count($pacientes) > 0 ? implode(', ', $pacientes) . ',' : '';
        $inicio = $inicio->format('Y-m-d');
        $fim = $fim->format('Y-m-d');

        $sql = <<<SQL
SELECT
  paciente,
  clientes.nome,
  clientes.codigo,
  DATE_FORMAT(data, '%Y-%m-%d') AS inicio
FROM
  capmedica
  INNER JOIN clientes ON capmedica.paciente = clientes.idClientes
WHERE
  paciente NOT IN ({$pacientes} 142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
  AND data BETWEEN '{$inicio} 00:00:00' AND '{$fim} 23:59:59'
GROUP BY 
  paciente
HAVING COUNT(id) > 0
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getGeneroByPacientesAtendidos($pacientes)
    {
        $pacientes = implode(', ', $pacientes);

        $sql = <<<SQL
SELECT
  sexo
FROM
  clientes
WHERE
  idClientes IN ({$pacientes})
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getFaixaEtariaByPacientesAtendidos($pacientes)
    {
        $pacientes = implode(', ', $pacientes);

        $sql = <<<SQL
SELECT
  idClientes AS paciente,
  nascimento,
  dataInternacao,
  nome,
  codigo
FROM
  clientes
WHERE
  idClientes IN ({$pacientes})
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getModalidadeByPacientesAtendidos($inicio, $fim, $pacientes)
    {
        $pacientes = implode(', ', $pacientes);
        $inicio = $inicio->format('Y-m-d');
        $fim = $fim->format('Y-m-d');

        $sql = <<<SQL
SELECT
  *
FROM
  (
    SELECT
      c.idClientes AS paciente,
      mhc.modalidade,
      cm.data AS data,
      'CAPMED' AS source
    FROM
      clientes AS c
      INNER JOIN capmedica AS cm ON (cm.paciente = c.idClientes)
      INNER JOIN modalidade_home_care AS mhc ON (mhc.id = cm.MODALIDADE)
    WHERE
      cm.MODALIDADE != 0
      AND cm.paciente IN ({$pacientes})
      AND cm.data BETWEEN '{$inicio} 00:00:01' AND '{$fim} 23:59:59'
      AND cm.id = (
        SELECT
          MAX(id)
        FROM
          capmedica AS cm2
        WHERE
          cm2.paciente = cm.paciente
          AND cm2.data BETWEEN '{$inicio} 00:00:01' AND '{$fim} 23:59:59'
      )
    GROUP BY
      cm.paciente
    UNION
    SELECT
      c.idClientes AS paciente,
      mhc.modalidade,
      rpm.INICIO AS data,
      'PRORROGACAO' AS source
    FROM
      clientes AS c
      INNER JOIN relatorio_prorrogacao_med AS rpm ON (
      rpm.PACIENTE_ID = c.idClientes
      )
      INNER JOIN modalidade_home_care AS mhc ON (mhc.id = rpm.MOD_HOME_CARE)
    WHERE
      MOD_HOME_CARE != 0
      AND rpm.PACIENTE_ID IN ({$pacientes})
      AND (
        rpm.INICIO BETWEEN '{$inicio}' AND '{$fim}'
        OR rpm.FIM BETWEEN '{$inicio}' AND '{$fim}'
      )
      AND rpm.ID = (
        SELECT
          MAX(ID)
        FROM
          relatorio_prorrogacao_med AS rpm2
        WHERE
          rpm2.PACIENTE_ID = rpm.PACIENTE_ID
          AND (
            rpm2.INICIO BETWEEN '{$inicio}' AND '{$fim}'
            OR rpm2.FIM BETWEEN '{$inicio}' AND '{$fim}'
          )
      )
    GROUP BY
      rpm.PACIENTE_ID
    UNION
    SELECT
      c.idClientes AS paciente,
      mhc.modalidade,
      fme.DATA AS data,
      'EVOLUCAO' AS source
    FROM
      clientes AS c
      INNER JOIN fichamedicaevolucao AS fme ON (
      fme.PACIENTE_ID = c.idClientes
      )
      INNER JOIN modalidade_home_care AS mhc ON (mhc.id = fme.MOD_HOME_CARE)
    WHERE
      MOD_HOME_CARE != 0
      AND fme.PACIENTE_ID IN ({$pacientes})
      AND fme.DATA BETWEEN '{$inicio} 00:00:01' AND '{$fim} 23:59:59'
      AND fme.ID = (
        SELECT
          MAX(ID)
        FROM
          fichamedicaevolucao AS fme2
        WHERE
          fme2.PACIENTE_ID = fme.PACIENTE_ID
          AND fme2.DATA BETWEEN '{$inicio} 00:00:01' AND '{$fim} 23:59:59'
      )
    GROUP BY
      fme.PACIENTE_ID
      UNION
		SELECT
		  c.idClientes AS paciente,
		  mhc.modalidade,
		  hmp.data_mudanca AS data,
		  'SECMED' AS source
		FROM
		  clientes AS c
		INNER JOIN historico_modalidade_paciente AS hmp ON (hmp.paciente = c.idClientes)
		INNER JOIN modalidade_home_care AS mhc ON (mhc.id = hmp.modalidade)
		WHERE
		  hmp.data_mudanca BETWEEN '{$inicio}' AND '{$fim}'
		  AND hmp.paciente IN ({$pacientes})
          AND hmp.id = (
            SELECT
              MAX(id)
            FROM
              historico_modalidade_paciente AS hmp2
            WHERE
              hmp2.paciente = hmp.paciente
              AND hmp2.data_mudanca BETWEEN '{$inicio}' AND '{$fim}'
          )
          GROUP BY
            hmp.paciente
    ORDER BY
      data DESC
  ) AS result
GROUP BY
  paciente
ORDER BY
  modalidade ASC, data DESC
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getHistoricoStatusByPacientesAtendidos($inicio, $fim, $pacientes)
    {
        $pacientes = implode(', ', $pacientes);
        $inicio = $inicio->format('Y-m-d');
        $fim = $fim->format('Y-m-d');

        $sql = <<<SQL
SELECT
  c1.idClientes,
  COALESCE(
      st1.status,
      (
        SELECT
          st2.status
        FROM
          clientes AS c2
          INNER JOIN statuspaciente AS st2 ON c2.status = st2.id
        WHERE
          c2.idClientes = c1.idClientes
      )
  ) AS status
FROM
  clientes AS c1
  LEFT JOIN historico_status_paciente AS hsp1 ON c1.idClientes = hsp1.PACIENTE_ID
  INNER JOIN statuspaciente AS st1 ON hsp1.STATUS_PACIENTE_ID = st1.id
WHERE
  hsp1.DATA BETWEEN '{$inicio} 00:00:01' AND '{$fim} 23:59:59'
  AND hsp1.PACIENTE_ID IN ({$pacientes})
  AND hsp1.ID = (
    SELECT
      MAX(ID)
    FROM
      historico_status_paciente AS hsp2
    WHERE
      hsp2.PACIENTE_ID = hsp1.PACIENTE_ID
      AND hsp2.DATA BETWEEN '{$inicio} 00:00:01' AND '{$fim} 23:59:59'
  )
GROUP BY
  hsp1.PACIENTE_ID
ORDER BY
  status
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getStatusByPacientesAtendidos($pacientes)
    {
        $pacientes = implode(', ', $pacientes);

        $sql = <<<SQL
SELECT
  idClientes,
  statuspaciente.status
FROM
  clientes
  INNER JOIN statuspaciente ON clientes.status = statuspaciente.id
WHERE
  idClientes IN ({$pacientes})
ORDER BY
  status
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getPacienteStatusComponent($status, $todos = null,$pacienteId = null, $required = 'required')
    {
        @session_start();
        $compStatus = '';
        if($status !== false) {
            $compStatus = is_array($status) ?
                " AND clientes.status IN (" . implode(',', $status) . ")" :
                " AND clientes.status = '{$status}'";
        }

        $ur_usuario = $_SESSION['empresa_principal'];
        $compUr = "";
        if($ur_usuario != '1') {
            $compUr = " AND  clientes.empresa IN {$_SESSION['empresa_user']}";
        }
      

        $sql = <<<SQL
SELECT
	clientes.idClientes AS id,
	clientes.nome,
	clientes.codigo,
	clientes.convenio,
	clientes.empresa,
	clientes.status,
	statuspaciente.status AS optgroup,
	operadoras_planosdesaude.OPERADORAS_ID AS operadora
FROM
	clientes
	INNER JOIN statuspaciente ON clientes.status = statuspaciente.id
    LEFT JOIN operadoras_planosdesaude ON clientes.convenio = operadoras_planosdesaude.PLANOSDESAUDE_ID 
WHERE
	1 = 1
	{$compStatus}
	{$compUr}
ORDER BY optgroup, nome
SQL;
        $pacientes = parent::get($sql, 'assoc');

        //echo '<pre>'; die(var_dump($pacientes));

        require_once __DIR__ . "/../../../components/paciente-select/paciente-select.phtml";
    }

    public static function getPacienteUrComponent($ur)
    {
        session_start();
        $ur_usuario = $_SESSION['empresa_principal'];
        $compUr = $compUr = is_array($ur) ?
            " AND clientes.empresa IN (" . implode(',', $ur) . ")" :
            " AND clientes.empresa = '{$ur}'";
        if($ur_usuario != '1') {
            $compUr = " AND clientes.empresa = '{$ur}'";
        }

        $sql = <<<SQL
SELECT
	clientes.idClientes AS id,
	clientes.nome,
	clientes.codigo,
	clientes.convenio,
	clientes.empresa,
	clientes.status,
	empresa.nome AS optgroup
FROM
	clientes
	INNER JOIN empresas ON clientes.empresa = empresas.id
WHERE
	1 = 1
	{$compUr}
ORDER BY optgroup, nome
SQL;
        $pacientes = parent::get($sql, 'assoc');

        require_once __DIR__ . "/../../../components/paciente-select/paciente-select.phtml";
    }

    public static function getPacienteConvenioComponent($plano)
    {
        session_start();
        $compConvenio = is_array($plano) ?
            " AND clientes.convenio IN (" . implode(',', $plano) . ")" :
            " AND clientes.convenio = '{$plano}'";

        $ur_usuario = $_SESSION['empresa_principal'];
        $compUr = "";
        if($ur_usuario != '1') {
            $compUr = " AND clientes.empresa = '{$ur_usuario}'";
        }

        $sql = <<<SQL
SELECT
	clientes.idClientes AS id,
	clientes.nome,
	clientes.codigo,
	clientes.convenio,
	clientes.empresa,
	clientes.status,
	planosdesaude.nome AS optgroup
FROM
	clientes
	INNER JOIN planosdesaude ON clientes.convenio = planosdesaude.id
WHERE
	1 = 1
	{$compConvenio}
	{$compUr}
ORDER BY optgroup, nome
SQL;
        $pacientes = parent::get($sql, 'assoc');

        require_once __DIR__ . "/../../../components/paciente-select/paciente-select.phtml";
    }

    public static function insertPacienteRemocao($nomePaciente,
                                                 $convenio,
                                                 $cpf,
                                                 $nascimento,
                                                 $matricula,
                                                 $telefonePaciente,
                                                 $sexo)
    {

        /// paciente de remoção externa é inicialmente colocado com status = inativo e a ur = central

        mysql_query('begin');
        $sql = "
INSERT INTO clientes
 (
 `nome`,
 `nascimento`,
 `sexo`,
 `convenio`,
 NUM_MATRICULA_CONVENIO,
 DATA_CADASTRO,
 cpf,
 TEL_DOMICILIAR,
 status,
 empresa,
 LIMINAR
 )
VALUES
(
'$nomePaciente',
'$nascimento',
'$sexo',
'$convenio',
'$matricula',
now(),
'$cpf',
'$telefonePaciente',
9,
NULL,
0

) ";

        if(!mysql_query($sql)){
            mysql_query("rollback");
            echo "Erro ao inserir paciente.";
            return;
        }
        $id = mysql_insert_id();

        savelog(mysql_escape_string(addslashes($sql)));
        $codigo= "N{$id}";
        $sql="UPDATE clientes SET `codigo`='$codigo'  WHERE `idClientes` = '$id'";
        if(!mysql_query($sql)){
            mysql_query("rollback");
            echo "Erro 02";
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));

        $sql = "INSERT INTO historico_plano_liminar_paciente (PACIENTE_ID,USUARIO_ID,PLANO_ID,DATA) values ('$id','{$_SESSION['id_user']}','$convenio',now())";
        if(!mysql_query($sql)){
            mysql_query("rollback");
            echo "erro";
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));

        $sql= "Insert into historico_status_paciente (USUARIO_ID,PACIENTE_ID,DATA,OBSERVACAO,STATUS_PACIENTE_ID)
             values ('{$_SESSION['id_user']}','{$id}',now(),'Remoção Externa',9)";
        if(!mysql_query($sql)){
            mysql_query("rollback");
            echo "ERRO: Tente mais tarde!";
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));
        mysql_query("commit");
        return $id;

    }
    // caso der erro ao inserir uma  remoção externa deve apagar o paciente inserido por ela.
    public static function deletePacienteRemocao($id)
    {
        mysql_query("begin");
        $sql = "Delete from pacientes where idclientes = {$id}";
        if(!mysql_query($sql)){
            mysql_query("rollback");
            echo "Erro 01";
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));
        $sql = "DELETE from historico_plano_liminar_paciente where PACIENTE_ID = {$id}";
        if(!mysql_query($sql)){
            mysql_query("rollback");
            echo "Erro 01";
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));
        mysql_query("commit");
        return 1;
    }

    public static function updateEmpresaById($id,$empresa)
    {
        $sql = "update clientes set empresa = {$empresa}  where idClientes = {$id}";

        return parent::get($sql, 'Assoc');

    }

    public static function getPacienteByCPF($cpf, $cpfFiltered)
    {
        $sql = <<<SQL
SELECT
	clientes.*,
	planosdesaude.nome AS plano
FROM
	clientes
	INNER JOIN planosdesaude ON clientes.convenio = planosdesaude.id
WHERE
	clientes.cpf = '{$cpf}'
	OR clientes.cpf = '{$cpfFiltered}'
ORDER BY clientes.nome
SQL;
        return current(parent::get($sql, 'assoc'));
    }

    public static function getEquipamentosAtivosPacienteId($paciente)
    {
        $sql = <<<SQL
SELECT 
  c.id AS item_id,
  c.item,
  cl.nome AS paciente,
  DATE_FORMAT(e.DATA_INICIO,'%d/%m/%Y') as dt,
  e.lote,
  e.saida_id,
  e.ID,
  e.COBRANCA_PLANOS_ID,
  f.razaoSocial,
  e.fornecedor
FROM 
  `equipamentosativos` as e inner join 
  cobrancaplanos as c on (e.`COBRANCA_PLANOS_ID` = c.id) left join
  fornecedores as f on e.fornecedor = f.idFornecedores
  INNER JOIN clientes AS cl ON e.PACIENTE_ID = cl.idClientes
WHERE 
  e.`DATA_FIM` = '0000-00-00'
  AND e.`DATA_INICIO` <> '0000-00-00'
  AND e.`PRESCRICAO_AVALIACAO` <> 's'
  AND e.`PACIENTE_ID` = '{$paciente}' 
order by c.item
SQL;
        return parent::get($sql, 'assoc');
    }
}
