<?php

namespace app\Models\Administracao;

use App\Models\AbstractModel,
    App\Models\DAOInterface;



class ProrrogacaoPlanserv extends AbstractModel implements DAOInterface
{
    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id), 'Assoc'));
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? " AND prorrogacao_planserv.id = {$id}" : "";
        return <<<SQL
SELECT
	prorrogacao_planserv.id,
	prorrogacao_planserv.paciente_id,
	clientes.nome AS paciente,
	prorrogacao_planserv.usuario_id,
	prorrogacao_planserv.inicio,
	prorrogacao_planserv.fim,
	prorrogacao_planserv.`data`,
	prorrogacao_planserv.modalidade,
	prorrogacao_planserv.admissao,
	prorrogacao_planserv.criterio,
	prorrogacao_planserv.fonoterapia AS terapia_fonoterapia,
	prorrogacao_planserv.fisioterapia AS terapia_fisioterapia,
	prorrogacao_planserv.o2 AS terapia_o2,
	prorrogacao_planserv.dieta,
	prorrogacao_planserv.via,
	prorrogacao_planserv.deambula,
	prorrogacao_planserv.deambula_auxilio,
	prorrogacao_planserv.drenos,
	prorrogacao_planserv.familia_apta,
	prorrogacao_planserv.integridade_cultanea,
	prorrogacao_planserv.lesoes,
	prorrogacao_planserv.atb,
	prorrogacao_planserv.atb_descricao,
	prorrogacao_planserv.previsao_termino_atb,
	prorrogacao_planserv.antifungico,
	prorrogacao_planserv.antifungico_descricao,
	prorrogacao_planserv.previsao_termino_antifungico,
	prorrogacao_planserv_enfermagem.prorrogacao_enfermagem,
	prorrogacao_planserv_enfermagem.evolucao AS evolucao_enfermagem,
	prorrogacao_planserv_enfermagem.data_visita AS data_visita_enfermagem,
	prorrogacao_planserv_enfermagem.nome  AS enfermeiro,
	prorrogacao_planserv_enfermagem.coren,
	prorrogacao_planserv_fonoaudiologia.evolucao AS evolucao_fonoaudiologia,
	prorrogacao_planserv_fonoaudiologia.data_visita AS data_visita_fonoaudiologia,
	prorrogacao_planserv_fonoaudiologia.nome AS fonoaudiologo,
	prorrogacao_planserv_fonoaudiologia.crfa,
	prorrogacao_planserv_fonoaudiologia.prorrogacao_fonoaudiologia,
	prorrogacao_planserv_medico.prorrogacao_medica,
	prorrogacao_planserv_medico.evolucao AS evolucao_medica,
	prorrogacao_planserv_medico.quadro_clinico,
	prorrogacao_planserv_medico.regime_atual,
	prorrogacao_planserv_medico.indicacao_tecnica,
	prorrogacao_planserv_medico.data_visita AS data_visita_medica,
	prorrogacao_planserv_medico.nome AS medico,
	prorrogacao_planserv_medico.crm,
	prorrogacao_planserv_fisioterapia.evolucao AS evolucao_fisioterapia,
	prorrogacao_planserv_fisioterapia.data_visita AS data_visita_fisioterapia,
	prorrogacao_planserv_fisioterapia.prorrogacao_fisioterapia,
	prorrogacao_planserv_fisioterapia.nome AS fisioterapeuta,
	prorrogacao_planserv_fisioterapia.crefitto,
	prorrogacao_planserv_nutricao.prorrogacao_nutricao,
	prorrogacao_planserv_nutricao.evolucao AS evolucao_nutricao,
	prorrogacao_planserv_nutricao.data_visita AS data_visita_nutricao,
	prorrogacao_planserv_nutricao.nome AS nutricionista,
	prorrogacao_planserv_nutricao.crn,
	prorrogacao_planserv_solicitacoes.modalidade AS solicitacoes_modalidade,
	prorrogacao_planserv_solicitacoes.fisioterapia AS solicitacoes_fisioterapia,
	prorrogacao_planserv_solicitacoes.fonoterapia AS solicitacoes_fonoterapia,
	prorrogacao_planserv_solicitacoes.dietas AS solicitacoes_dietas,
	prorrogacao_planserv_solicitacoes.curativos AS solicitacoes_curativos,
	prorrogacao_planserv_solicitacoes.oxigenoterapia AS solicitacoes_oxigenoterapia,
	prorrogacao_planserv_solicitacoes.equipamentos AS solicitacoes_equipamentos,
	prorrogacao_planserv_solicitacoes.atb_med AS solicitacoes_atb_med,
	prorrogacao_planserv_solicitacoes.visita_enfermagem AS solicitacoes_enfermagem,
	prorrogacao_planserv_solicitacoes.visita_nutricao AS solicitacoes_nutricao,
	prorrogacao_planserv_solicitacoes.visita_medica AS solicitacoes_medico
FROM
	prorrogacao_planserv
LEFT JOIN prorrogacao_planserv_fisioterapia ON prorrogacao_planserv.id = prorrogacao_planserv_fisioterapia.prorrogacao_planserv_id
LEFT JOIN prorrogacao_planserv_fonoaudiologia ON prorrogacao_planserv.id = prorrogacao_planserv_fonoaudiologia.prorrogacao_planserv_id
LEFT JOIN prorrogacao_planserv_medico ON prorrogacao_planserv.id = prorrogacao_planserv_medico.prorrogacao_planserv_id
LEFT JOIN prorrogacao_planserv_nutricao ON prorrogacao_planserv.id = prorrogacao_planserv_nutricao.prorrogacao_planserv_id
LEFT JOIN prorrogacao_planserv_solicitacoes ON prorrogacao_planserv.id = prorrogacao_planserv_solicitacoes.prorrogacao_planserv_id
LEFT JOIN prorrogacao_planserv_enfermagem ON prorrogacao_planserv.id = prorrogacao_planserv_enfermagem.prorrogacao_planserv_id
INNER JOIN clientes ON prorrogacao_planserv.paciente_id = clientes.idClientes
WHERE
    1 = 1
    {$id}
SQL;
    }

    public static function getProrrogacoes($id)
    {
        return parent::get(self::sqlGetProrrogacoes($id));
    }

    public static function saveProrrogacaoPlanserv($data)
    {
        mysql_query('begin');
        $data['integridade_cultanea_lesoes']    = addslashes($data['integridade_cultanea_lesoes']);
        $data['medico']['evolucao']             = addslashes($data['medico']['evolucao']);
        $data['medico']['quadro_clinico']       = addslashes($data['medico']['quadro_clinico']);
        $data['medico']['regime_atual']         = addslashes($data['medico']['regime_atual']);
        $data['medico']['indicacao_tecnica']    = addslashes($data['medico']['indicacao_tecnica']);
        $data['enfermagem']['evolucao']         = addslashes($data['enfermagem']['evolucao']);
        $data['fonoaudiologia']['evolucao']     = addslashes($data['fonoaudiologia']['evolucao']);
        $data['fisioterapia']['evolucao']       = addslashes($data['fisioterapia']['evolucao']);
        $data['nutricao']['evolucao']           = addslashes($data['nutricao']['evolucao']);
        $data['solicitacoes']['modalidade']     = addslashes($data['solicitacoes']['modalidade']);
        $data['solicitacoes']['fisioterapia']   = addslashes($data['solicitacoes']['fisioterapia']);
        $data['solicitacoes']['fonoterapia']    = addslashes($data['solicitacoes']['fonoterapia']);
        $data['solicitacoes']['dietas']         = addslashes($data['solicitacoes']['dietas']);
        $data['solicitacoes']['curativos']      = addslashes($data['solicitacoes']['curativos']);
        $data['solicitacoes']['oxigenoterapia'] = addslashes($data['solicitacoes']['oxigenoterapia']);
        $data['solicitacoes']['equipamentos']   = addslashes($data['solicitacoes']['equipamentos']);
        $data['solicitacoes']['atb_med']        = addslashes($data['solicitacoes']['atb_med']);
        $data['solicitacoes']['enfermagem']     = addslashes($data['solicitacoes']['enfermagem']);
        $data['solicitacoes']['nutricao']       = addslashes($data['solicitacoes']['nutricao']);
        $data['solicitacoes']['medico']         = addslashes($data['solicitacoes']['medico']);

        $sql = <<<SQL
INSERT INTO prorrogacao_planserv VALUES (
    NULL,
    '{$data['paciente']}',
    '{$_SESSION['id_user']}',
    '{$data['inicio']}',
    '{$data['fim']}',
    NOW(),
    '{$data['modalidade']}',
    '{$data['admissao']}',
    '{$data['criterio']}',
    '{$data['terapias']['fonoterapia']['qtd']}',
    '{$data['terapias']['fisioterapia']['qtd']}',
    '{$data['terapias']['o2']}',
    '{$data['dieta']}',
    '{$data['via']}',
    '{$data['deambula']}',
    '{$data['deambula_auxilio']}',
    '{$data['drenos']}',
    '{$data['familia_apta']}',
    '{$data['integridade_cultanea']}',
    '{$data['integridade_cultanea_lesoes']}',
    '{$data['atb']}',
    '{$data['atb_descricao']}',
    '{$data['previsao_termino_atb']}',
    '{$data['antifungico']}',
    '{$data['antifungico_descricao']}',
    '{$data['previsao_termino_antifungico']}'
)
SQL;
        $rs = mysql_query($sql) or die (mysql_error());
        if(!$rs){
            mysql_query('rollback');
            return false;
        }
        $prorrogacaoId = mysql_insert_id();

        $sql = <<<SQL
INSERT INTO prorrogacao_planserv_medico
 (
    id,
    prorrogacao_planserv_id,
    prorrogacao_medica,
    evolucao,
    quadro_clinico,
    regime_atual,
    indicacao_tecnica,
    data_visita,
    nome,
    crm
 )
 VALUES (
    NULL,
    '{$prorrogacaoId}',
    '{$data['medico']['relatorio']}',
    '{$data['medico']['evolucao']}',
    '{$data['medico']['quadro_clinico']}',
    '{$data['medico']['regime_atual']}',
    '{$data['medico']['indicacao_tecnica']}',
    '{$data['medico']['data_visita']}',
    '{$data['medico']['nome']}',
    '{$data['medico']['crm']}'
);
SQL;
        $rs = mysql_query($sql) or die (mysql_error() . ' - ' . $sql);
        $sql = <<<SQL
INSERT INTO prorrogacao_planserv_enfermagem VALUES (
    NULL,
    '{$prorrogacaoId}',
    '{$data['enfermagem']['relatorio']}',
    '{$data['enfermagem']['evolucao']}',
    '{$data['enfermagem']['data_visita']}',
    '{$data['enfermagem']['nome']}',
    '{$data['enfermagem']['coren']}'
);
SQL;
        $rs = mysql_query($sql) or die (mysql_error() . ' - ' . $sql);
        $sql = <<<SQL
INSERT INTO prorrogacao_planserv_nutricao VALUES (
    NULL,
    '{$prorrogacaoId}',
    '',
    '{$data['nutricao']['evolucao']}',
    '{$data['nutricao']['data_visita']}',
    '{$data['nutricao']['nome']}',
    '{$data['nutricao']['crn']}'
);
SQL;
        $rs = mysql_query($sql) or die (mysql_error() . ' - ' . $sql);
        $sql = <<<SQL
INSERT INTO prorrogacao_planserv_fisioterapia VALUES (
    NULL,
    '{$prorrogacaoId}',
    '',
    '{$data['fisioterapia']['evolucao']}',
    '{$data['fisioterapia']['data_visita']}',
    '{$data['fisioterapia']['nome']}',
    '{$data['fisioterapia']['crefitto']}'
);
SQL;
        $rs = mysql_query($sql) or die (mysql_error() . ' - ' . $sql);
        $sql = <<<SQL
INSERT INTO prorrogacao_planserv_fonoaudiologia VALUES (
    NULL,
    '{$prorrogacaoId}',
    '',
    '{$data['fonoaudiologia']['evolucao']}',
    '{$data['fonoaudiologia']['data_visita']}',
    '{$data['fonoaudiologia']['nome']}',
    '{$data['fonoaudiologia']['crfa']}'
);
SQL;
        $rs = mysql_query($sql) or die (mysql_error() . ' - ' . $sql);
        $sql = <<<SQL
INSERT INTO prorrogacao_planserv_solicitacoes VALUES (
    NULL,
    '{$prorrogacaoId}',
    '{$data['solicitacoes']['modalidade']}',
    '{$data['solicitacoes']['fisioterapia']}',
    '{$data['solicitacoes']['fonoterapia']}',
    '{$data['solicitacoes']['dietas']}',
    '{$data['solicitacoes']['curativos']}',
    '{$data['solicitacoes']['oxigenoterapia']}',
    '{$data['solicitacoes']['equipamentos']}',
    '{$data['solicitacoes']['atb_med']}',
    '{$data['solicitacoes']['medico']}',
    '{$data['solicitacoes']['enfermagem']}',
    '{$data['solicitacoes']['nutricao']}'
);
SQL;
        $rs = mysql_query($sql) or die (mysql_error() . ' - ' . $sql);
        if(!$rs){
            mysql_query('rollback');
            return false;
        }
        return [$prorrogacaoId, mysql_query('commit')];
    }

    public static function updateProrrogacaoPlanserv($data)
    {
        mysql_query('begin');
        $data['integridade_cultanea_lesoes']    = addslashes($data['integridade_cultanea_lesoes']);
        $data['medico']['evolucao']             = addslashes($data['medico']['evolucao']);
        $data['medico']['quadro_clinico']       = addslashes($data['medico']['quadro_clinico']);
        $data['medico']['regime_atual']         = addslashes($data['medico']['regime_atual']);
        $data['medico']['indicacao_tecnica']    = addslashes($data['medico']['indicacao_tecnica']);
        $data['enfermagem']['evolucao']         = addslashes($data['enfermagem']['evolucao']);
        $data['fonoaudiologia']['evolucao']     = addslashes($data['fonoaudiologia']['evolucao']);
        $data['fisioterapia']['evolucao']       = addslashes($data['fisioterapia']['evolucao']);
        $data['nutricao']['evolucao']           = addslashes($data['nutricao']['evolucao']);
        $data['solicitacoes']['modalidade']     = addslashes($data['solicitacoes']['modalidade']);
        $data['solicitacoes']['fisioterapia']   = addslashes($data['solicitacoes']['fisioterapia']);
        $data['solicitacoes']['fonoterapia']    = addslashes($data['solicitacoes']['fonoterapia']);
        $data['solicitacoes']['dietas']         = addslashes($data['solicitacoes']['dietas']);
        $data['solicitacoes']['curativos']      = addslashes($data['solicitacoes']['curativos']);
        $data['solicitacoes']['oxigenoterapia'] = addslashes($data['solicitacoes']['oxigenoterapia']);
        $data['solicitacoes']['equipamentos']   = addslashes($data['solicitacoes']['equipamentos']);
        $data['solicitacoes']['atb_med']        = addslashes($data['solicitacoes']['atb_med']);
        $data['solicitacoes']['enfermagem']     = addslashes($data['solicitacoes']['enfermagem']);
        $data['solicitacoes']['nutricao']       = addslashes($data['solicitacoes']['nutricao']);
        $data['solicitacoes']['medico']         = addslashes($data['solicitacoes']['medico']);

        $sql = <<<SQL
UPDATE prorrogacao_planserv SET
    modalidade                      = '{$data['modalidade']}',
    admissao                        = '{$data['admissao']}',
    criterio                        = '{$data['criterio']}',
    fonoterapia                     = '{$data['terapias']['fonoterapia']['qtd']}',
    fisioterapia                    = '{$data['terapias']['fisioterapia']['qtd']}',
    o2                              = '{$data['terapias']['o2']}',
    dieta                           = '{$data['dieta']}',
    via                             = '{$data['via']}',
    deambula                        = '{$data['deambula']}',
    deambula_auxilio                = '{$data['deambula_auxilio']}',
    drenos                          = '{$data['drenos']}',
    familia_apta                    = '{$data['familia_apta']}',
    integridade_cultanea            = '{$data['integridade_cultanea']}',
    lesoes                          = '{$data['integridade_cultanea_lesoes']}',
    atb                             = '{$data['atb']}',
    atb_descricao                   = '{$data['atb_descricao']}',
    previsao_termino_atb            = '{$data['previsao_termino_atb']}',
    antifungico                     = '{$data['antifungico']}',
    antifungico_descricao           = '{$data['antifungico_descricao']}',
    previsao_termino_antifungico    = '{$data['previsao_termino_antifungico']}'
WHERE
    id = {$data['prorrogacao']}
    AND paciente_id = {$data['paciente']}
SQL;
        $rs = mysql_query($sql) or die (mysql_error());
        if(!$rs){
            mysql_query('rollback');
            return false;
        }

        $sql = <<<SQL
UPDATE prorrogacao_planserv_medico SET
    evolucao            = '{$data['medico']['evolucao']}',
    quadro_clinico      = '{$data['medico']['quadro_clinico']}',
    regime_atual        = '{$data['medico']['regime_atual']}',
    indicacao_tecnica   = '{$data['medico']['indicacao_tecnica']}',
    data_visita         = '{$data['medico']['data_visita']}',
    nome                = '{$data['medico']['nome']}',
    crm                 = '{$data['medico']['crm']}'
WHERE
    prorrogacao_planserv_id = {$data['prorrogacao']}
SQL;
        $rs = mysql_query($sql) or die (mysql_error() . ' - ' . $sql);
        $sql = <<<SQL
UPDATE prorrogacao_planserv_enfermagem SET
    evolucao            = '{$data['enfermagem']['evolucao']}',
    data_visita         = '{$data['enfermagem']['data_visita']}',
    nome                = '{$data['enfermagem']['nome']}',
    coren               = '{$data['enfermagem']['coren']}'
WHERE
    prorrogacao_planserv_id = {$data['prorrogacao']}
SQL;
        $rs = mysql_query($sql) or die (mysql_error() . ' - ' . $sql);
        $sql = <<<SQL
UPDATE prorrogacao_planserv_nutricao SET
    evolucao            = '{$data['nutricao']['evolucao']}',
    data_visita         = '{$data['nutricao']['data_visita']}',
    nome                = '{$data['nutricao']['nome']}',
    crn                 = '{$data['nutricao']['crn']}'
WHERE
    prorrogacao_planserv_id = {$data['prorrogacao']}
SQL;
        $rs = mysql_query($sql) or die (mysql_error() . ' - ' . $sql);
        $sql = <<<SQL
UPDATE prorrogacao_planserv_fisioterapia SET
    evolucao            = '{$data['fisioterapia']['evolucao']}',
    data_visita         = '{$data['fisioterapia']['data_visita']}',
    nome                = '{$data['fisioterapia']['nome']}',
    crefitto            = '{$data['fisioterapia']['crefitto']}'
WHERE
    prorrogacao_planserv_id = {$data['prorrogacao']}
SQL;
        $rs = mysql_query($sql) or die (mysql_error() . ' - ' . $sql);
        $sql = <<<SQL
UPDATE prorrogacao_planserv_fonoaudiologia SET
    evolucao            = '{$data['fonoaudiologia']['evolucao']}',
    data_visita         = '{$data['fonoaudiologia']['data_visita']}',
    nome                = '{$data['fonoaudiologia']['nome']}',
    crfa                = '{$data['fonoaudiologia']['crfa']}'
WHERE
    prorrogacao_planserv_id = {$data['prorrogacao']}
SQL;
        $rs = mysql_query($sql) or die (mysql_error() . ' - ' . $sql);
        $sql = <<<SQL
UPDATE prorrogacao_planserv_solicitacoes SET
    modalidade      = '{$data['solicitacoes']['modalidade']}',
    fisioterapia    = '{$data['solicitacoes']['fisioterapia']}',
    fonoterapia     = '{$data['solicitacoes']['fonoterapia']}',
    dietas          = '{$data['solicitacoes']['dietas']}',
    curativos       = '{$data['solicitacoes']['curativos']}',
    oxigenoterapia  = '{$data['solicitacoes']['oxigenoterapia']}',
    equipamentos    = '{$data['solicitacoes']['equipamentos']}',
    atb_med         = '{$data['solicitacoes']['atb_med']}',
    visita_medica   = '{$data['solicitacoes']['medico']}',
    visita_enfermagem = '{$data['solicitacoes']['enfermagem']}',
    visita_nutricao = '{$data['solicitacoes']['nutricao']}'
WHERE
    prorrogacao_planserv_id = {$data['prorrogacao']}
SQL;
        $rs = mysql_query($sql) or die (mysql_error() . ' - ' . $sql);
        if(!$rs){
            mysql_query('rollback');
            return false;
        }
        return mysql_query('commit');
    }

    public  static function sqlGetProrrogacoes($id)
    {
        $sql = <<<SQL
        SELECT
clientes.nome as paciente,
usuarios.nome as user,
prorrogacao_planserv.id,
prorrogacao_planserv.paciente_id,
prorrogacao_planserv.usuario_id,
prorrogacao_planserv.inicio,
prorrogacao_planserv.fim,
prorrogacao_planserv.`data`
FROM
prorrogacao_planserv
INNER JOIN usuarios ON prorrogacao_planserv.usuario_id = usuarios.idUsuarios
INNER JOIN clientes ON prorrogacao_planserv.paciente_id = clientes.idClientes
WHERE
prorrogacao_planserv.paciente_id = '{$id}'
ORDER BY prorrogacao_planserv.data DESC
SQL;
        return $sql;
    }

    public static function getAllProrrogacoesAutorizacaoUr($filters)
    {
        $response = [];
        $compPaciente = $filters['paciente'] != 'T' ? " AND prorrogacao_planserv.paciente_id = '{$filters['paciente']}'" : "";
        $compPlano = $filters['plano'] != 'T' ? " AND clientes.convenio = '{$filters['plano']}'" : "";

        $ur_usuario = $_SESSION['empresa_principal'];
        if($ur_usuario != '1') {
            $compUr =  " AND clientes.empresa IN {$_SESSION['empresa_user']}";
        } else {
            $compUr = $filters['ur'] != 'T' ? " AND clientes.empresa = '{$filters['ur']}'" : "";
        }

        $sql = <<<SQL
SELECT
	prorrogacao_planserv.id,
	prorrogacao_planserv.inicio,
	prorrogacao_planserv.fim,
	prorrogacao_planserv.paciente_id,
	prorrogacao_planserv_solicitacoes.modalidade AS solicitacoes_modalidade,
	prorrogacao_planserv_solicitacoes.fisioterapia AS solicitacoes_fisioterapia,
	prorrogacao_planserv_solicitacoes.fonoterapia AS solicitacoes_fonoterapia,
	prorrogacao_planserv_solicitacoes.dietas AS solicitacoes_dietas,
	prorrogacao_planserv_solicitacoes.curativos AS solicitacoes_curativos,
	prorrogacao_planserv_solicitacoes.oxigenoterapia AS solicitacoes_oxigenoterapia,
	prorrogacao_planserv_solicitacoes.equipamentos AS solicitacoes_equipamentos,
	prorrogacao_planserv_solicitacoes.atb_med AS solicitacoes_atb_med,
	prorrogacao_planserv_solicitacoes.visita_enfermagem AS solicitacoes_enfermagem,
	prorrogacao_planserv_solicitacoes.visita_nutricao AS solicitacoes_nutricao,
	prorrogacao_planserv_solicitacoes.visita_medica AS solicitacoes_medico
FROM
	prorrogacao_planserv
    LEFT JOIN prorrogacao_planserv_solicitacoes ON prorrogacao_planserv.id = prorrogacao_planserv_solicitacoes.prorrogacao_planserv_id
    INNER JOIN clientes ON prorrogacao_planserv.paciente_id = clientes.idClientes {$compPlano}
WHERE
    (
      prorrogacao_planserv.inicio BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
      OR prorrogacao_planserv.fim BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
    )
    {$compPaciente}
    {$compUr}
SQL;
        $prorrogacoes = parent::get($sql, "Assoc");

        foreach ($prorrogacoes as $prorrogacao) {
            $sql = <<<SQL
SELECT 
  clientes.codigo,
  clientes.dataInternacao AS dataAdmissao,
  clientes.NUM_MATRICULA_CONVENIO AS inscricao,
  clientes.nome AS paciente,
  CONCAT(
    clientes.END_INTERNADO, 
    '. ', 
    clientes.BAI_INTERNADO, 
    ' - ', 
    cidades.NOME, 
    ' - ', 
    cidades.UF,  
    '(REF: ', 
    clientes.REF_ENDERECO_INTERNADO, 
    ')'
  ) AS endereco,
  IF(clientes.TEL_LOCAL_INTERNADO != '', clientes.TEL_LOCAL_INTERNADO, clientes.TEL_RESPONSAVEL) AS contato,
  planosdesaude.nome AS convenio
FROM 
  clientes
  INNER JOIN planosdesaude ON clientes.convenio = planosdesaude.id
  LEFT JOIN cidades ON clientes.CIDADE_ID_INT = cidades.ID
WHERE
  idClientes = '{$prorrogacao['paciente_id']}'
SQL;
            $dados_paciente = parent::get($sql, 'Assoc');
            $response[$prorrogacao['id']] = $dados_paciente[0];
            $response[$prorrogacao['id']]['relatorio'] = 'Prorrogação';
            $response[$prorrogacao['id']]['modalidade'] = $prorrogacao['solicitacoes_modalidade'];
            $response[$prorrogacao['id']]['inicio'] = $prorrogacao['inicio'];
            $response[$prorrogacao['id']]['fim'] = $prorrogacao['fim'];
            $response[$prorrogacao['id']]['aut_medico'] = $prorrogacao['solicitacoes_medico'];
            $response[$prorrogacao['id']]['aut_enfermagem'] = $prorrogacao['solicitacoes_enfermagem'];
            $response[$prorrogacao['id']]['aut_tecnico'] = '-';
            $response[$prorrogacao['id']]['aut_fisio_motora'] = $prorrogacao['solicitacoes_fisioterapia'];
            $response[$prorrogacao['id']]['aut_fisio_resp'] = $prorrogacao['solicitacoes_fisioterapia'];
            $response[$prorrogacao['id']]['aut_fono'] = $prorrogacao['solicitacoes_fonoterapia'];
            $response[$prorrogacao['id']]['aut_nutricao'] = $prorrogacao['solicitacoes_nutricao'];
            $response[$prorrogacao['id']]['aut_psicologo'] = '-';
        }

        return $response;
    }
}
