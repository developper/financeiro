<?php

namespace App\Models\Administracao;

use App\Models\AbstractModel;
use App\Models\DAOInterface;

class StatusPaciente extends AbstractModel implements DAOInterface
{

	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : "";
		return "SELECT * FROM statuspaciente WHERE 1=1 {$id} ORDER BY status";
	}
} 