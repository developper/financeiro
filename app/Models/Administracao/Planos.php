<?php

namespace app\Models\Administracao;

use App\Models\AbstractModel;
use App\Models\DAOInterface;

class Planos extends AbstractModel implements DAOInterface
{

	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? "AND id = {$id}" : "";
		return "SELECT * FROM planosdesaude WHERE 1=1 {$id} AND ATIVO = 'S' ORDER BY nome";
	}

	public static function getPlanosAtivos()
	{
		return parent::get("SELECT * FROM planosdesaude WHERE 1=1  AND ATIVO = 'S' ORDER BY nome", 'assoc');
	}

    public static function getOperadoraPlano($plano)
    {
        return parent::get("SELECT * FROM operadoras_planosdesaude WHERE PLANOSDESAUDE_ID = '{$plano}'", 'assoc');
    }

    public static function getFluxoByPlanoId($id)
    {
        $sql = <<<SQL
SELECT 
  id,
  tabela_origem,
  regra_primaria,
  opcao_regra
FROM
  regra_fluxo
WHERE
  plano_id = '{$id}'
SQL;
        return parent::get($sql, 'Assoc');
	}

    public static function getItensByFluxoId($table, $opcao, $plano)
    {
        $tabela_origem = [
            'medicamento' => 'regra_fluxo_medicamentos',
            'material' => 'regra_fluxo_materiais',
            'dieta' => 'regra_fluxo_dietas',
            'equipamento' => 'regra_fluxo_equipamentos',
        ];

        $nomeItem = "COALESCE(categorias_materiais.ITEM, 'TODOS') AS nome_item,";
        $innerItem = " LEFT JOIN categorias_materiais ON {$tabela_origem[$table]}.item_id = categorias_materiais.ID ";
        if($opcao == 'item') {
            $nomeItem = "cobrancaplanos.item AS nome_item,";
            $innerItem = " INNER JOIN cobrancaplanos ON {$tabela_origem[$table]}.item_id = cobrancaplanos.id ";
            if($table != 'equipamento') {
                $nomeItem = "CONCAT(catalogo.principio, ' ', catalogo.apresentacao) AS nome_item,";
                $innerItem = " INNER JOIN catalogo ON {$tabela_origem[$table]}.item_id = catalogo.ID ";
            }
        }

        $sql = <<<SQL
SELECT
  {$nomeItem}
  tipo_solicitacao,
  tipo_validacao,
  regra_fluxo_id,
  item_id,
  campo_comparacao,
  operador,
  valor_referencia
FROM
  {$tabela_origem[$table]}
  {$innerItem}
  INNER JOIN regra_fluxo ON {$tabela_origem[$table]}.regra_fluxo_id = regra_fluxo.id
  LEFT JOIN regra_fluxo_condicionais ON 
    regra_fluxo_condicionais.tabela_origem_id = {$tabela_origem[$table]}.id 
    AND regra_fluxo_condicionais.tabela_origem = '{$table}' 
WHERE 
  regra_fluxo.plano_id = '{$plano}'
  AND regra_fluxo.opcao_regra = '{$opcao}'
SQL;

        $response = parent::get($sql, 'Assoc');
        return $response;
	}

    public static function deleteCascadeFluxoPlano($plano)
    {
        $tabelas_regras = [
            'medicamento' => 'regra_fluxo_medicamentos',
            'material' => 'regra_fluxo_materiais',
            'servico' => 'regra_fluxo_servicos',
            'dieta' => 'regra_fluxo_dietas',
            'equipamento' => 'regra_fluxo_equipamentos',
        ];

        $sql = "SELECT id, tabela_origem FROM regra_fluxo WHERE plano_id = '{$plano}'";
        $fluxos = parent::get($sql, 'Assoc');

        if(count($fluxos) > 0) {
            mysql_query('begin');

            $sql = "DELETE FROM regra_fluxo WHERE plano_id = '{$plano}'";
            mysql_query($sql);

            foreach ($fluxos as $fluxo) {
                $sql = "SELECT id, tipo_validacao FROM {$tabelas_regras[$fluxo['tabela_origem']]} WHERE regra_fluxo_id = '{$fluxo['id']}'";
                $item_fluxo = parent::get($sql, 'Assoc');

                $sql = "DELETE FROM {$tabelas_regras[$fluxo['tabela_origem']]} WHERE regra_fluxo_id = '{$fluxo['id']}'";
                mysql_query($sql);

                foreach ($item_fluxo as $item) {
                    if ($item['tipo_validacao'] == 'condicional') {
                        $sql = "DELETE FROM regra_fluxo_condicionais WHERE tabela_origem_id = '{$item['id']}'";
                        mysql_query($sql);
                    }
                }
            }

            return mysql_query('commit');
        }
        return true;
	}
}
