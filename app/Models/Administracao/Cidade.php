<?php

namespace app\Models\Administracao;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class Cidade extends AbstractModel implements DAOInterface
{

	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : "";
		return "SELECT * FROM cidades WHERE 1=1 {$id}";
	}

	public static function getCidadeByName($cidade)
	{
		$sql = <<<SQL
SELECT * FROM cidades WHERE cidades.NOME = '{$cidade}' 
SQL;
		return current(parent::get($sql));
	}

}