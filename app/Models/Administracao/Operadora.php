<?php

namespace app\Models\Administracao;

use App\Models\AbstractModel;
use App\Models\DAOInterface;

class Operadora extends AbstractModel implements DAOInterface
{

	public static function getAll()
	{
		return parent::get(self::getSQL());
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? "AND ID = {$id}" : "";
		return "SELECT * FROM operadoras WHERE canceled_at is null {$id} ORDER BY NOME";
	}

	public static function getCodigoEmpresaOperadoraByOperadora($idOperadora)
	{
		$response = parent::get(self::getCodigoEmpresaOperadoraByOperadoraSQL($idOperadora));
		return $response;
	}

	public static function getByCodigo($id)
	{
		$response = current(parent::get(self::getSQL($id), 'Assoc'));
		if(!$response)
			throw new \Exception('Nenhuma operadora foi encontrada com esse código!');

		return $response;
	}

	public static function getPlanosByOperadoraId($id)
	{
		$sql = <<<SQL
SELECT
	o.NOME AS operadora,
	p.nome AS plano,
	p.registro_ans
FROM
	planosdesaude p
	INNER JOIN operadoras_planosdesaude op ON p.id = op.PLANOSDESAUDE_ID
	INNER JOIN operadoras o ON o.ID = op.OPERADORAS_ID
WHERE
	op.OPERADORAS_ID = {$id}
	AND ATIVO = 'S'
	and
	o.canceled_at is null
ORDER BY
	plano
SQL;
		return parent::get($sql, 'Assoc');
	}

    public static function getOperadoraByPlanoId($id)
    {
        $sql = <<<SQL
SELECT
	o.ID AS operadora_id,
	o.NOME AS operadora,
	p.nome AS plano,
	p.registro_ans,
	o.data_pagamento
FROM
	planosdesaude p
	INNER JOIN operadoras_planosdesaude op ON p.id = op.PLANOSDESAUDE_ID
	INNER JOIN operadoras o ON o.ID = op.OPERADORAS_ID
WHERE
	op.PLANOSDESAUDE_ID = {$id}
	and
	o.canceled_at is null
ORDER BY
	plano DESC LIMIT 1
SQL;
        return current(parent::get($sql, 'Assoc'));
    }

	public static function save($data)
	{
		mysql_query('begin');
		$sql = <<<SQL
INSERT INTO operadoras (ID, NOME, EMAIL, data_pagamento, dia_maximo_envio_fatura, created_at, created_by)
VALUES
('', '{$data['nome']}', '{$data['email']}','{$data['data_pagamento']}','{$data['dia_maximo_envio_fatura']}', now(),'{$_SESSION['id_user']}')
SQL;
		$response = mysql_query($sql);
		$idOperadora = mysql_insert_id();
		if(!$response){
			mysql_query('rollback');
			throw new \Exception('Houve um problema ao criar essa operadora. Erro: ' . mysql_error());

		}

		$sql = <<<SQL
INSERT INTO historico_operadora (id, operadora_id, nome, email, data_pagamento, dia_maximo_envio_fatura, created_at, created_by,tipo)
VALUES
('', $idOperadora, '{$data['nome']}', '{$data['email']}','{$data['data_pagamento']}','{$data['dia_maximo_envio_fatura']}',
 now(),'{$_SESSION['id_user']}','created')
SQL;
		$response = mysql_query($sql);

		if(!$response){
			mysql_query('rollback');
			throw new \Exception('Houve um problema ao criar o historíco operadora. Erro: ' . mysql_error());

		}
		mysql_query('commit');



		return $idOperadora;
	}

	public static function saveCodigoEmpresaOperadora($idOperadora,$data)
	{
		$response = true;
		foreach($data as $chave => $data){

			if(!empty($data)){
				$sql = <<<SQL
INSERT INTO codigo_empresa_operadora (id,operadoras_id,empresas_id,codigo) VALUES ('','{$idOperadora}','{$chave}','{$data}')
SQL;

				$response = mysql_query($sql);
				if(!$response)
					throw new \Exception('Houve um problema ao criar essa operadora. Erro: ' . mysql_error());

			}
		}
		return $response;

	}

	public static function updateCodigoEmpresaOperadora($idOperadora, $codigos)
	{
		$response = true;
		$sql = <<<SQL
DELETE FROM codigo_empresa_operadora WHERE operadoras_id = '{$idOperadora}'
SQL;

		$response = mysql_query($sql);

		foreach($codigos as $chave => $codigo){
			if(!empty($codigo)){
				$sql = <<<SQL
INSERT INTO codigo_empresa_operadora (id,operadoras_id,empresas_id,codigo) VALUES ('','{$idOperadora}', '{$chave}', '{$codigo}')
SQL;

				$response = mysql_query($sql);
				if(!$response)
					throw new \Exception('Houve um problema ao criar essa operadora. Erro: ' . mysql_error());

			}
		}
		return $response;

	}

	public static function update($data)
	{
		mysql_query('begin');
		$sql = <<<SQL
UPDATE
operadoras
 SET
  NOME = '{$data['nome']}',
  EMAIL = '{$data['email']}',
  dia_maximo_envio_fatura = '{$data['dia_maximo_envio_fatura']}',
  data_pagamento = '{$data['data_pagamento']}',
  updated_at = now(),
  updated_by = '{$_SESSION['id_user']}'
   WHERE ID = {$data['id']}
SQL;
		$response = mysql_query($sql);
		if(!$response){
			mysql_query('rollback');
			throw new \Exception('Houve um problema ao atualizar essa operadora. Erro: ' . mysql_error());
		}

		$sql = <<<SQL
INSERT INTO historico_operadora (id, operadora_id, nome, email, data_pagamento, dia_maximo_envio_fatura, created_at, created_by, tipo)
VALUES
('', {$data['id']}, '{$data['nome']}', '{$data['email']}','{$data['data_pagamento']}',
'{$data['dia_maximo_envio_fatura']}', now(),'{$_SESSION['id_user']}','update')
SQL;
		$response = mysql_query($sql);

		if(!$response){
			mysql_query('rollback');
			throw new \Exception('Houve um problema ao criar o historíco operadora. Erro: ' . mysql_error());

		}
		mysql_query('commit');

		return $response;
	}

	public static function delete($id)
	{
		$sql = <<<SQL
Update
 operadoras
 SET
  canceled_at = now(),
  canceled_by = '{$_SESSION['id_user']}'

  WHERE ID = {$id}

SQL;
		$response = mysql_query($sql);
		if(!$response)
			throw new \Exception('Houve um problema ao excluir essa operadora. Erro: ' . mysql_error());

		return $response;
	}

	public static function getCodigoEmpresaOperadoraByOperadoraSQL($idOperadora)
	{
		$sql = <<<SQL
SELECT
codigo_empresa_operadora.id,
codigo_empresa_operadora.empresas_id,
codigo_empresa_operadora.operadoras_id,
codigo_empresa_operadora.codigo
FROM
codigo_empresa_operadora
WHERE
codigo_empresa_operadora.operadoras_id  = {$idOperadora}
SQL;
		return $sql;
	}
}