<?php

namespace App\Models\Administracao;

use App\Models\AbstractModel;
use App\Models\DAOInterface;

class PacienteIW extends AbstractModel implements DAOInterface
{
    public static function getAll($empresa)
    {
        return parent::get(self::getSQL(null, $empresa), 'Assoc');
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id), 'Assoc'));
    }

    public static function getSQL($id = null, $empresa = null)
    {
        $id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : "";
        $empresa = isset($empresa) && !empty($empresa) ? 'AND empresa = "' . $empresa . '"' : "";
        return <<<SQL
SELECT 
  paciente_iw.*,
  paciente_iw.codigo_iw AS codigo,
  paciente_iw.cns AS NUM_MATRICULA_CONVENIO,
  paciente_iw.data_nascimento AS nascimento,
  '' as relac,
  IF(empresa = 2, 'ASSISTE SSA', 'ASSISTE SUL/BA') AS ur,
  'SESAB/SUS' AS convenio,
  REPLACE(paciente_iw.cid10, '.', '') AS diagnostico,
  cid10.descricao AS descricao_cid10
FROM 
  paciente_iw 
  LEFT JOIN cid10 ON REPLACE(paciente_iw.cid10, '.', '') = cid10.codigo COLLATE utf8_general_ci
WHERE 
  1=1 
  {$id}  
  {$empresa}  
ORDER BY 
  paciente_iw.nome
SQL;

    }
}
