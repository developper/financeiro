<?php

namespace app\Models\Administracao;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class Filial extends AbstractModel implements DAOInterface
{

	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getEmpresas()
	{
		$sql = "SELECT * FROM empresas WHERE 1";
		return parent::get($sql);
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : "";
		return "SELECT * FROM empresas WHERE 1=1 {$id} AND id NOT IN (1, 3, 9, 10) AND ATIVO = 'S' ORDER BY nome";
	}

	public static function getLogoByPacienteId($id)
	{
			$sql = <<<SQL
SELECT e.logo FROM empresas e INNER JOIN clientes c ON e.id = c.empresa WHERE idClientes = {$id}
SQL;

		return current(parent::get($sql));
	}

	public static function getLogoByEmpresaPrincipal()
	{
		$sql = <<<SQL
SELECT logo FROM empresas WHERE id = {$_SESSION['empresa_principal']}
SQL;
		return current(parent::get($sql));
	}

	public static function getUnidadesAtivas($empresas = null)
	{
		$condEmpresa = empty($empresas) ? "NOT IN (3, 9, 10)" : " in $empresas";
		$sql = <<<SQL
SELECT * FROM empresas WHERE ATIVO = 'S' AND id {$condEmpresa} ORDER BY nome
SQL;
		return parent::get($sql, 'Assoc');
	}

	public static function getUnidadesUsuario($list)
    {
		$sql = <<<SQL
SELECT * FROM empresas WHERE id IN {$list} ORDER BY nome
SQL;
		return parent::get($sql);
	}

    public static function getEmpresaById($id)
    {
        $sql = <<<SQL
SELECT * FROM empresas WHERE id = {$id}
SQL;
        return current(parent::get($sql));
	}

    public static function getEmpresaUser()
    {
        $sql = <<<SQL
SELECT *, UPPER(nome) AS nome FROM empresas WHERE id IN {$_SESSION['empresa_user']} AND id NOT IN (1, 3, 9, 10) AND ATIVO = 'S' ORDER BY nome
SQL;
        return parent::get($sql, 'assoc');
    }
}