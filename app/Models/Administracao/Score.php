<?php

namespace app\Models\Administracao;

use App\Models\AbstractModel,
	App\Models\DAOInterface;
use App\Models\Medico\Avaliacao;
use App\Models\Medico\Evolucao;
use App\Models\Medico\Prorrogacao;

class Score extends AbstractModel
{
    public static function getScoreByGrupo($grupo)
    {
        $sql = <<<SQL
SELECT
  sc_item.*,
  gru.DESCRICAO as grupo,
  gru.QTD
FROM
  score_item as sc_item INNER JOIN
  grupos as gru ON (sc_item.GRUPO_ID = gru.ID)
WHERE
  sc_item.SCORE_ID = '{$grupo}'
ORDER BY
  sc_item.GRUPO_ID, sc_item.ID
SQL;
        return parent::get($sql);
	}

    public static function saveScorePaciente($data, $paciente, $tipo_relatorio, $relatorioId)
    {
        mysql_query("begin");
        $score = explode("$", $data);
        foreach ($score as $scr) {
            $scr = explode("#", $scr);
            $sql = <<<SQL
INSERT INTO nead2016_paciente 
(
  id,
  paciente,
  relatorio,
  tipo_relatorio,
  secao,
  opcao,
  valor
) 
  VALUES 
(
  NULL,
  '{$paciente}',
  '{$relatorioId}',
  '{$tipo_relatorio}',
  '{$scr[0]}',
  '{$scr[1]}',
  '{$scr[2]}'
) 
SQL;
                $r = mysql_query($sql);
                if (!$r) {
                    mysql_query("rollback");
                    return false;
                }
        }
        return mysql_query('commit');
    }

    public static function getUltimoScorePaciente($paciente, $relatorio = null, $tipo_relatorio = null)
    {
        if($relatorio == null){
            switch($tipo_relatorio){
                case "capmedica":
                    $relatorio = Avaliacao::getUltimoRelatorioPaciente($paciente)['id'];
                    break;
                case "evolucao":
                    $relatorio = Evolucao::getUltimoRelatorioPaciente($paciente)['ID'];
                    break;
                case "prorrogacao":
                    $relatorio = Prorrogacao::getUltimoRelatorioPaciente($paciente)['ID'];
                    break;
            }
        }
        $condTipoRel = $tipo_relatorio != null ? " AND tipo_relatorio = '{$tipo_relatorio}'" : "";
        $sql = <<<SQL
SELECT * FROM nead2016_paciente WHERE paciente = '{$paciente}' AND relatorio = '{$relatorio}' {$condTipoRel}
SQL;
        return parent::get($sql);

    }

    public static function deleteScorePaciente($tipo_relatorio, $relatorio)
    {
        $sql = "DELETE FROM nead2016_paciente WHERE tipo_relatorio = '{$tipo_relatorio}' AND relatorio = '{$relatorio}'";
        return mysql_query($sql);
    }

    public static function getTempoAndAutorScorePaciente($tipo_relatorio, $relatorio)
    {
        $tableSetup = [
            'capmedica' => [
                'table' => 'capmedica',
                'field_join' => 'id',
                'usuarios_id_field' => 'usuario',
                'date_field' => 'data',
            ],
            'avaliacaoenfermagem' => [
                'table' => 'avaliacaoenfermagem',
                'field_join' => 'ID',
                'usuarios_id_field' => 'USUARIO_ID',
                'date_field' => 'DATA',
            ],
            'evolucao' => [
                'table' => 'fichamedicaevolucao',
                'field_join' => 'ID',
                'usuarios_id_field' => 'USUARIO_ID',
                'date_field' => 'DATA',
            ],
            'prorrogacao' => [
                'table' => 'relatorio_prorrogacao_med',
                'field_join' => 'ID',
                'usuarios_id_field' => 'USUARIO_ID',
                'date_field' => 'DATA',
            ]
        ];

        $tableRel = $tableSetup[$tipo_relatorio];
        $sql = <<<SQL
SELECT 
  {$tableRel['table']}.{$tableRel['date_field']} AS dataCriacao,
  usuarios.nome AS criadoPor
FROM 
  nead2016_paciente 
  INNER JOIN {$tableRel['table']} ON nead2016_paciente.relatorio = {$tableRel['table']}.{$tableRel['field_join']}
  INNER JOIN usuarios ON {$tableRel['table']}.{$tableRel['usuarios_id_field']} = usuarios.idUsuarios
WHERE 
  relatorio = '{$relatorio}' 
  AND tipo_relatorio = '{$tipo_relatorio}'
LIMIT 1
SQL;
        //die($sql);
        return current(parent::get($sql, 'Assoc'));
    }

}