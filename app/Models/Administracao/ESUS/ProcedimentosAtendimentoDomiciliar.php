<?php

namespace App\Models\Administracao\ESUS;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class ProcedimentosAtendimentoDomiciliar extends AbstractModel implements DAOInterface
{

	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : "";
		return "SELECT * FROM esus_procedimentos_atencao_domiciliar WHERE 1=1 {$id} AND id ORDER BY descricao";
	}
}