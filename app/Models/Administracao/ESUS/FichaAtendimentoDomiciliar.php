<?php

namespace App\Models\Administracao\ESUS;

use App\Models\AbstractModel;

class FichaAtendimentoDomiciliar extends AbstractModel
{
    protected $conn;
    const CNES_MEDERI = '3795160';
    const CNES_ASSISTE = '5715768';
    const CNES_CUIDARE = '6477151';
    const MAJOR_VERSAO_XML = 3;
    const MINOR_VERSAO_XML = 15;
    const REVISION_VERSAO_XML = 0;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function getFiltered($data)
    {
        $compPaciente = "";
        if($data['paciente'] != '') {
            $compPaciente = " AND paciente_id = '{$data['paciente']}' ";
        }

        if($data['filial'] == '1') {
            $innerPaciente = " INNER JOIN clientes ON esus_ficha_atendimento_domiciliar.paciente_id = clientes.idClientes ";
            $nomeCampo = " clientes.nome ";
            $compEmpresa = " AND usuarios.empresa IN (1, 5, 7, 11, 13, 14) ";
        } else {
            $innerPaciente = " INNER JOIN paciente_iw ON esus_ficha_atendimento_domiciliar.paciente_id = paciente_iw.id ";
            $nomeCampo = " paciente_iw.nome ";
            $compEmpresa = " AND paciente_iw.empresa = {$data['filial']} ";
        }
        $sql = <<<SQL
SELECT 
  esus_ficha_atendimento_domiciliar.id,
  paciente_cns,
  data_visita,
  turno,
  created_at,
  created_by,
  {$nomeCampo} AS paciente,
  usuarios.nome AS criado_por,
  emitido_xml,
  CONCAT('CPF: ', profissional_cpf, ' / CNS: ', profissional_cns, ' / CBO: ', profissional_cbo, ' / INE: ', profissional_ine, ' / ', profissional_nome) AS profissional
FROM 
  esus_ficha_atendimento_domiciliar
  {$innerPaciente}
  INNER JOIN usuarios ON esus_ficha_atendimento_domiciliar.created_by = usuarios.idUsuarios
WHERE
  data_visita BETWEEN '{$data['inicio']}' AND '{$data['fim']}'
  {$compPaciente}
  {$compEmpresa}
  AND deleted_at IS NULL
  AND deleted_by IS NULL
ORDER BY 
  created_at DESC 
SQL;
        return parent::get($sql, 'assoc');
    }

    public function save($data)
    {
        $this->conn->query("BEGIN");
        $this->conn->autocommit(FALSE);

        if (count($data['data_visita']) > 0) {
            foreach ($data['data_visita'] as $index => $data_visita) {
                $sql = <<<SQL
INSERT INTO
`esus_ficha_atendimento_domiciliar`
(
id,
paciente_id,
paciente_cns,
data_nascimento,
sexo,
profissional_nome,
profissional_cpf,
profissional_cns,
profissional_cbo,
profissional_ator_id,
profissional_ine,
profissional_cnes,
data_visita,
turno,
local_atendimento,
modalidade,
tipo_atendimento,
conduta_desfecho,
created_at,
created_by,
updated_at,
updated_by,
deleted_at,
deleted_by
)
 VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 NOW(),
 ?,
 NULL,
 NULL,
 NULL,
 NULL
);
SQL;
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param(
                        'ississssisssiiiiii',
                        $data['paciente_id'],
                        $data['paciente_cns'],
                        $data['data_nascimento'],
                        $data['sexo'],
                        $data['profissional_nome'],
                        $data['profissional_cpf'],
                        $data['profissional_cns'],
                        $data['profissional_cbo'],
                        $data['profissional_ator_id'],
                        $data['profissional_ine'],
                        $data['profissional_cnes'],
                        $data_visita,
                        $data['turno'][$index],
                        $data['local_atendimento'],
                        $data['modalidade'],
                        $data['tipo_atendimento'],
                        $data['conduta_desfecho'],
                        $_SESSION['id_user']
                    );

                    $rs = $stmt->execute();
                    if($rs) {
                        $fichaId = $stmt->insert_id;
                    } else {
                        echo $stmt->error;
                        return false;
                    }
                } else {
                    return false;
                }

                if (count($data['condicoes_avaliadas']) > 0) {
                    foreach ($data['condicoes_avaliadas'] as $condicao_avaliada) {
                        $sql = <<<SQL
INSERT INTO
`esus_ficha_atendimento_domiciliar_condicoes_avaliadas`
(
id,
ficha_atendimento_domiciliar_id,
condicao_avaliada_id
) VALUES (
 NULL,
 ?,
 ?
);
SQL;
                        if ($stmt = $this->conn->prepare($sql)) {
                            $stmt->bind_param(
                                'ii',
                                $fichaId,
                                $condicao_avaliada
                            );

                            $rs = $stmt->execute();
                        } else {
                            return false;
                        }
                    }
                }

                if (count($data['cid10']) > 0) {
                    foreach ($data['cid10'] as $cid10) {
                        $sql = <<<SQL
INSERT INTO
`esus_ficha_atendimento_domiciliar_cid10`
(
id,
ficha_atendimento_domiciliar_id,
cid10
) VALUES (
 NULL,
 ?,
 ?
);
SQL;
                        if ($stmt = $this->conn->prepare($sql)) {
                            $stmt->bind_param(
                                'is',
                                $fichaId,
                                $cid10
                            );

                            $rs = $stmt->execute();
                        } else {
                            return false;
                        }
                    }
                }

                if (count($data['ciap2']) > 0) {
                    foreach ($data['ciap2'] as $ciap2) {
                        $sql = <<<SQL
INSERT INTO
`esus_ficha_atendimento_domiciliar_ciap2`
(
id,
ficha_atendimento_domiciliar_id,
ciap2_id
) VALUES (
 NULL,
 ?,
 ?
);
SQL;
                        if ($stmt = $this->conn->prepare($sql)) {
                            $stmt->bind_param(
                                'is',
                                $fichaId,
                                $ciap2
                            );

                            $rs = $stmt->execute();
                        } else {
                            return false;
                        }
                    }
                }

                if (count($data['procedimentos']) > 0) {
                    foreach ($data['procedimentos'] as $procedimento) {
                        $sql = <<<SQL
INSERT INTO
`esus_ficha_atendimento_domiciliar_procedimentos`
(
id,
ficha_atendimento_domiciliar_id,
procedimento_id
) VALUES (
 NULL,
 ?,
 ?
);
SQL;
                        if ($stmt = $this->conn->prepare($sql)) {
                            $stmt->bind_param(
                                'is',
                                $fichaId,
                                $procedimento
                            );

                            $rs = $stmt->execute();
                        } else {
                            return false;
                        }
                    }
                }

                if (count($data['sigtap']) > 0) {
                    foreach ($data['sigtap'] as $sigtap) {
                        $sql = <<<SQL
INSERT INTO
`esus_ficha_atendimento_domiciliar_outros_procedimentos`
(
id,
ficha_atendimento_domiciliar_id,
sigtap_id
) VALUES (
 NULL,
 ?,
 ?
);
SQL;
                        if ($stmt = $this->conn->prepare($sql)) {
                            $stmt->bind_param(
                                'is',
                                $fichaId,
                                $sigtap
                            );

                            $rs = $stmt->execute();
                        } else {
                            return false;
                        }
                    }
                }
            }
        }

        if(!empty($rs)){
            return $this->conn->query("COMMIT");
        }else{
            $this->conn->query("ROLLBACK");
            return $this->conn->error;
        }
    }

    public function getDataForUpdate($id)
    {
        $sql = <<<SQL
SELECT
  esus_ficha_atendimento_domiciliar.*,
  CONCAT('CPF: ', profissional_cpf, ' / CNS: ', profissional_cns, ' / CBO: ', profissional_cbo, ' / ', profissional_nome) AS value
FROM
  esus_ficha_atendimento_domiciliar
WHERE 
  id = '{$id}'
SQL;
        $response = current(parent::get($sql, 'assoc'));

        $condicoesAvaliadas = self::getCondicoesAvaliadasByFichaId($response['id']);
        foreach ($condicoesAvaliadas as $row) {
            $response['condicoes_avaliadas'][] = $row['condicao_avaliada_id'];
        }

        $cid10 = self::getCid10ByFichaId($response['id']);
        foreach ($cid10 as $row) {
            $response['cid10'][] = $row;
        }

        $ciap2 = self::getCiap2ByFichaId($response['id']);
        foreach ($ciap2 as $row) {
            $response['ciap2'][] = $row['ciap2_id'];
        }

        $procedimentos = self::getProcedimentosByFichaId($response['id']);
        foreach ($procedimentos as $row) {
            $response['procedimentos'][] = $row['procedimento_id'];
        }

        $outros_procedimentos = self::getOutroProcedimentosByFichaId($response['id']);
        foreach ($outros_procedimentos as $row) {
            $response['outros_procedimentos'][] = $row;
        }

        return $response;
    }

    public function update($data)
    {
        $this->conn->query("BEGIN");
        $this->conn->autocommit(FALSE);

        self::checkAndUpdateMatriculaPaciente($data['paciente_id'], $data['paciente_cns'], $data['filial']);

        $sql = <<<SQL
DELETE FROM esus_ficha_atendimento_domiciliar WHERE id = ?
SQL;
        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param(
                'i',
                $data['ficha_id']
            );

            $rs = $stmt->execute();
        } else {
            return false;
        }

        $sql = <<<SQL
INSERT INTO
`esus_ficha_atendimento_domiciliar`
(
id,
paciente_id,
paciente_cns,
data_nascimento,
sexo,
profissional_nome,
profissional_cpf,
profissional_cns,
profissional_cbo,
profissional_ator_id,
profissional_ine,
profissional_cnes,
data_visita,
turno,
local_atendimento,
modalidade,
tipo_atendimento,
conduta_desfecho,
created_at,
created_by,
updated_at,
updated_by,
deleted_at,
deleted_by
)
 VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 NOW(),
 ?,
 NULL,
 NULL,
 NULL,
 NULL
);
SQL;
        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param(
                'ississssisssiiiiii',
                $data['paciente_id'],
                $data['paciente_cns'],
                $data['data_nascimento'],
                $data['sexo'],
                $data['profissional_nome'],
                $data['profissional_cpf'],
                $data['profissional_cns'],
                $data['profissional_cbo'],
                $data['profissional_ator_id'],
                $data['profissional_ine'],
                $data['profissional_cnes'],
                $data['data_visita'],
                $data['turno'],
                $data['local_atendimento'],
                $data['modalidade'],
                $data['tipo_atendimento'],
                $data['conduta_desfecho'],
                $_SESSION['id_user']
            );

            $rs = $stmt->execute();
            if($rs) {
                $fichaId = $stmt->insert_id;
            } else {
                echo $stmt->error;
                return false;
            }
        } else {
            return false;
        }

        $sql = <<<SQL
DELETE FROM esus_ficha_atendimento_domiciliar_condicoes_avaliadas WHERE ficha_atendimento_domiciliar_id = ?
SQL;
        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param(
                'i',
                $data['ficha_id']
            );

            $rs = $stmt->execute();
        } else {
            return false;
        }

        if (count($data['condicoes_avaliadas']) > 0) {
            foreach ($data['condicoes_avaliadas'] as $condicao_avaliada) {
                $sql = <<<SQL
INSERT INTO
`esus_ficha_atendimento_domiciliar_condicoes_avaliadas`
(
id,
ficha_atendimento_domiciliar_id,
condicao_avaliada_id
) VALUES (
 NULL,
 ?,
 ?
);
SQL;
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param(
                        'ii',
                        $fichaId,
                        $condicao_avaliada
                    );

                    $rs = $stmt->execute();
                } else {
                    return false;
                }
            }
        }

        $sql = <<<SQL
DELETE FROM esus_ficha_atendimento_domiciliar_cid10 WHERE ficha_atendimento_domiciliar_id = ?
SQL;
        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param(
                'i',
                $data['ficha_id']
            );

            $rs = $stmt->execute();
        } else {
            return false;
        }

        if (count($data['cid10']) > 0) {
            foreach ($data['cid10'] as $cid10) {
                $sql = <<<SQL
INSERT INTO
`esus_ficha_atendimento_domiciliar_cid10`
(
id,
ficha_atendimento_domiciliar_id,
cid10
) VALUES (
 NULL,
 ?,
 ?
);
SQL;
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param(
                        'is',
                        $fichaId,
                        $cid10
                    );

                    $rs = $stmt->execute();
                } else {
                    return false;
                }
            }
        }

        $sql = <<<SQL
DELETE FROM esus_ficha_atendimento_domiciliar_ciap2 WHERE ficha_atendimento_domiciliar_id = ?
SQL;
        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param(
                'i',
                $data['ficha_id']
            );

            $rs = $stmt->execute();
        } else {
            return false;
        }

        if (count($data['ciap2']) > 0) {
            foreach ($data['ciap2'] as $ciap2) {
                $sql = <<<SQL
INSERT INTO
`esus_ficha_atendimento_domiciliar_ciap2`
(
id,
ficha_atendimento_domiciliar_id,
ciap2_id
) VALUES (
 NULL,
 ?,
 ?
);
SQL;
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param(
                        'is',
                        $fichaId,
                        $ciap2
                    );

                    $rs = $stmt->execute();
                } else {
                    return false;
                }
            }
        }

        $sql = <<<SQL
DELETE FROM esus_ficha_atendimento_domiciliar_procedimentos WHERE ficha_atendimento_domiciliar_id = ?
SQL;
        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param(
                'i',
                $data['ficha_id']
            );

            $rs = $stmt->execute();
        } else {
            return false;
        }

        if (count($data['procedimentos']) > 0) {
            foreach ($data['procedimentos'] as $procedimento) {
                $sql = <<<SQL
INSERT INTO
`esus_ficha_atendimento_domiciliar_procedimentos`
(
id,
ficha_atendimento_domiciliar_id,
procedimento_id
) VALUES (
 NULL,
 ?,
 ?
);
SQL;
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param(
                        'is',
                        $fichaId,
                        $procedimento
                    );

                    $rs = $stmt->execute();
                } else {
                    return false;
                }
            }
        }

        $sql = <<<SQL
DELETE FROM esus_ficha_atendimento_domiciliar_outros_procedimentos WHERE ficha_atendimento_domiciliar_id = ?
SQL;
        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param(
                'i',
                $data['ficha_id']
            );

            $rs = $stmt->execute();
        } else {
            return false;
        }

        if (count($data['sigtap']) > 0) {
            foreach ($data['sigtap'] as $sigtap) {
                $sql = <<<SQL
INSERT INTO
`esus_ficha_atendimento_domiciliar_outros_procedimentos`
(
id,
ficha_atendimento_domiciliar_id,
sigtap_id
) VALUES (
 NULL,
 ?,
 ?
);
SQL;
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param(
                        'is',
                        $fichaId,
                        $sigtap
                    );

                    $rs = $stmt->execute();
                } else {
                    return false;
                }
            }
        }

        if(!empty($rs)){
            $this->conn->query("COMMIT");
            return $fichaId;
        }else{
            $this->conn->query("ROLLBACK");
            return $this->conn->error;
        }
    }

    private function generateUUID() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0fff ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }

    public static function getByPacientePeriodo($data)
    {
        $response = [];
        $compPaciente = "";
        if($data['paciente'] != '') {
            $compPaciente = " AND paciente_id = '{$data['paciente']}' ";
        }

        $sql = <<<SQL
SELECT 
  * 
FROM 
  esus_ficha_atendimento_domiciliar 
WHERE 
  data_visita BETWEEN '{$data['inicio']}' AND '{$data['fim']}'
  {$compPaciente}
  AND deleted_at IS NULL
  AND deleted_by IS NULL
SQL;
        $ficha = parent::get($sql, 'assoc');

        foreach ($ficha as $row) {
            $response[$row['id']]['ficha'] = $row;
            $response[$row['id']]['condicoes_avaliadas'] = self::getCondicoesAvaliadasByFichaId($row['id']);
            $response[$row['id']]['cid10'] = self::getCid10ByFichaId($row['id']);
            $response[$row['id']]['ciap2'] = self::getCiap2ByFichaId($row['id']);
            $response[$row['id']]['procedimentos'] = self::getProcedimentosByFichaId($row['id']);
            $response[$row['id']]['outros_procedimentos'] = self::getOutroProcedimentosByFichaId($row['id']);
        }

        return $response;
    }

    public static function getByFichasId($data)
    {
        $response = [];
        $ids = implode(', ', $data['fichas_id']);

        $sql = <<<SQL
UPDATE esus_ficha_atendimento_domiciliar SET emitido_xml = '1' WHERE id IN ({$ids})
SQL;
        $rs = mysql_query($sql);

        $sql = <<<SQL
SELECT 
  * 
FROM 
  esus_ficha_atendimento_domiciliar 
WHERE 
  id IN ({$ids})
  AND deleted_at IS NULL
  AND deleted_by IS NULL
SQL;
        $ficha = parent::get($sql, 'assoc');

        foreach ($ficha as $row) {
            $response[$row['id']]['ficha'] = $row;
            $response[$row['id']]['condicoes_avaliadas'] = self::getCondicoesAvaliadasByFichaId($row['id']);
            $response[$row['id']]['cid10'] = self::getCid10ByFichaId($row['id']);
            $response[$row['id']]['ciap2'] = self::getCiap2ByFichaId($row['id']);
            $response[$row['id']]['procedimentos'] = self::getProcedimentosByFichaId($row['id']);
            $response[$row['id']]['outros_procedimentos'] = self::getOutroProcedimentosByFichaId($row['id']);
        }

        return $response;
    }

    private static function getCondicoesAvaliadasByFichaId($fichaId)
    {
        $sql = <<<SQL
SELECT 
  * 
FROM 
  esus_ficha_atendimento_domiciliar_condicoes_avaliadas
WHERE 
  ficha_atendimento_domiciliar_id = '{$fichaId}'
SQL;
        return parent::get($sql, 'assoc');
    }

    private static function getCid10ByFichaId($fichaId)
    {
        $sql = <<<SQL
SELECT 
  esus_ficha_atendimento_domiciliar_cid10.*,
  cid10.descricao
FROM 
  esus_ficha_atendimento_domiciliar_cid10
  LEFT JOIN cid10 ON esus_ficha_atendimento_domiciliar_cid10.cid10 = cid10.codigo
WHERE 
  ficha_atendimento_domiciliar_id = '{$fichaId}'
SQL;
        return parent::get($sql, 'assoc');
    }

    private static function getCiap2ByFichaId($fichaId)
    {
        $sql = <<<SQL
SELECT 
  * 
FROM 
  esus_ficha_atendimento_domiciliar_ciap2
WHERE 
  ficha_atendimento_domiciliar_id = '{$fichaId}'
SQL;
        return parent::get($sql, 'assoc');
    }

    private static function getProcedimentosByFichaId($fichaId)
    {
        $sql = <<<SQL
SELECT 
  efadp.*,
  codigo_sigtap AS codigo
FROM 
  esus_ficha_atendimento_domiciliar_procedimentos AS efadp
  INNER JOIN esus_procedimentos_atencao_domiciliar AS epac ON efadp.procedimento_id = epac.id 
WHERE 
  ficha_atendimento_domiciliar_id = '{$fichaId}'
SQL;
        return parent::get($sql, 'assoc');
    }

    private static function getOutroProcedimentosByFichaId($fichaId)
    {
        $sql = <<<SQL
SELECT 
  efadop.*,
  eps.codigo_sigtap AS codigo,
  eps.descricao
FROM 
  esus_ficha_atendimento_domiciliar_outros_procedimentos AS efadop
INNER JOIN esus_procedimentos_sigtap AS eps ON efadop.sigtap_id = eps.id 
WHERE 
  ficha_atendimento_domiciliar_id = '{$fichaId}'
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function delete($fichaId)
    {
        $sql = <<<SQL
UPDATE esus_ficha_atendimento_domiciliar 
SET deleted_at = NOW(), deleted_by = '{$_SESSION['id_user']}' 
WHERE id = '{$fichaId}'
SQL;
        return mysql_query($sql);
    }

    public function generateXML($data)
    {
        $ficha = self::getByFichasId($data);
        $xmls = [];
        $codIbgeUR = self::getCodIbgeEmpresa($_SESSION['empresa_principal'])['codigo_ibge'];
        foreach ($ficha as $row) {
            $dataVisitaObj = \DateTime::createFromFormat('Y-m-d', $row['ficha']['data_visita']);
            $dataLimite = \DateTime::createFromFormat('Y-m-d', '2019-03-01');
            if($dataVisitaObj < $dataLimite) {
                $cnesFilial = self::CNES_MEDERI;
                $cnpj = "07072541000174";
                $razaoSocial = "MEDERI SAUDE DOMICILIAR LTDA";
                $telefoneEmpresa = "7530229151";
                if ($data['filial'] == '2') {
                    $cnesFilial = self::CNES_ASSISTE;
                    $cnpj = "04678190000151";
                    $razaoSocial = "ASSISTE VIDA LTDA";
                    $telefoneEmpresa = "7134159100";
                } elseif ($data['filial'] == '3') {
                    $cnesFilial = self::CNES_CUIDARE;
                    $cnpj = "08955646000215";
                    $razaoSocial = "CUIDARE VITA PROGRAMA DE ATENCAO DOMICILIAR LTDA ME";
                    $telefoneEmpresa = "7332147400";
                }
            } else {
                $cnesFilial = self::CNES_ASSISTE;
                $cnpj = "04678190000151";
                $razaoSocial = "ASSISTE VIDA LTDA";
                $telefoneEmpresa = "7134159100";
            }

            $uuid = $cnesFilial . '-' . $this->generateUUID();
            $filename = sprintf(
                "Atendimento_Domiciliar_%s_%s.esus.xml",
                $row['ficha']['id'],
                $dataVisitaObj->format('dmY')
            );
            $filepath = $_SERVER['DOCUMENT_ROOT'] . '/storage/xml-esus/' . $filename;
            $xmls[] = [
                'filename' => $filename,
                'filepath' => $filepath
            ];

            $dom = new \DOMDocument('1.0', 'utf-8');

            $dadoTransporteTransportXml = $dom->createElement('ns3:dadoTransporteTransportXml');
            $dadoTransporteTransportXml->setAttribute('xmlns:ns2', 'http://esus.ufsc.br/dadoinstalacao');
            $dadoTransporteTransportXml->setAttribute('xmlns:ns3', 'http://esus.ufsc.br/dadotransporte');
            $dadoTransporteTransportXml->setAttribute('xmlns:ns4', 'http://esus.ufsc.br/fichaatendimentodomiciliarmaster');

            $uuidDadoSerializado = $dom->createElement('uuidDadoSerializado', $uuid);
            $tipoDadoSerializado = $dom->createElement('tipoDadoSerializado', '10');
            $codIbge = $dom->createElement('codIbge', $codIbgeUR);
            $cnesDadoSerializado = $dom->createElement('cnesDadoSerializado', $cnesFilial);
            $numLote = $dom->createElement('numLote', '1');
            $fichaAtendimentoDomiciliarMasterTransport = $dom->createElement('ns4:fichaAtendimentoDomiciliarMasterTransport');

            $dadoTransporteTransportXml->appendChild($uuidDadoSerializado);
            $dadoTransporteTransportXml->appendChild($tipoDadoSerializado);
            $dadoTransporteTransportXml->appendChild($codIbge);
            $dadoTransporteTransportXml->appendChild($cnesDadoSerializado);
            if($row['ficha']['profissional_ine'] != '') {
                $ineDadoSerializado = $dom->createElement('ineDadoSerializado', $row['ficha']['profissional_ine']);
                $dadoTransporteTransportXml->appendChild($ineDadoSerializado);
            }
            $dadoTransporteTransportXml->appendChild($numLote);
            $dadoTransporteTransportXml->appendChild($fichaAtendimentoDomiciliarMasterTransport);
            $uuidFicha = $dom->createElement('uuidFicha', $uuid);
            $tpCdsOrigem = $dom->createElement('tpCdsOrigem', '3');
            $headerTransport = $dom->createElement('headerTransport');

            $fichaAtendimentoDomiciliarMasterTransport->appendChild($uuidFicha);
            $fichaAtendimentoDomiciliarMasterTransport->appendChild($tpCdsOrigem);
            $fichaAtendimentoDomiciliarMasterTransport->appendChild($headerTransport);
            $lotacaoFormPrincipal = $dom->createElement('lotacaoFormPrincipal');
            $dataVisita = $dataVisitaObj->format('U');
            $dataAtendimento = $dom->createElement('dataAtendimento', $dataVisita);
            $codigoIbgeMunicipio = $dom->createElement('codigoIbgeMunicipio', $codIbgeUR);

            $headerTransport->appendChild($lotacaoFormPrincipal);
            $profissionalCNS = $dom->createElement('profissionalCNS', $row['ficha']['profissional_cns']);
            $cboCodigo_2002 = $dom->createElement('cboCodigo_2002', $row['ficha']['profissional_cbo']);
            $cnes = $dom->createElement('cnes', $row['ficha']['profissional_cnes']);
            $lotacaoFormPrincipal->appendChild($profissionalCNS);
            $lotacaoFormPrincipal->appendChild($cboCodigo_2002);
            $lotacaoFormPrincipal->appendChild($cnes);
            if($row['ficha']['profissional_ine'] != '') {
                $ine = $dom->createElement('ine', $row['ficha']['profissional_ine']);
                $lotacaoFormPrincipal->appendChild($ine);
            }
            $headerTransport->appendChild($dataAtendimento);
            $headerTransport->appendChild($codigoIbgeMunicipio);

            $atendimentosDomiciliares = $dom->createElement('atendimentosDomiciliares');
            $fichaAtendimentoDomiciliarMasterTransport->appendChild($headerTransport);
            $turno = $dom->createElement('turno', '2');
            $cnsCidadao = $dom->createElement('cnsCidadao', $row['ficha']['paciente_cns']);
            $nascimento = \DateTime::createFromFormat('Y-m-d', $row['ficha']['data_nascimento'])->format('U');
            $dataNascimento = $dom->createElement('dataNascimento', $nascimento);
            $sexo = $dom->createElement('sexo', $row['ficha']['sexo']);
            $localAtendimento = $dom->createElement('localAtendimento', $row['ficha']['local_atendimento']);
            $atencaoDomiciliarModalidade = $dom->createElement('atencaoDomiciliarModalidade', $row['ficha']['modalidade']);
            $tipoAtendimento = $dom->createElement('tipoAtendimento', $row['ficha']['tipo_atendimento']);
            $atendimentosDomiciliares->appendChild($turno);
            $atendimentosDomiciliares->appendChild($cnsCidadao);
            $atendimentosDomiciliares->appendChild($dataNascimento);
            $atendimentosDomiciliares->appendChild($sexo);
            $atendimentosDomiciliares->appendChild($localAtendimento);
            $atendimentosDomiciliares->appendChild($atencaoDomiciliarModalidade);
            $atendimentosDomiciliares->appendChild($tipoAtendimento);

            if(count($row['condicoes_avaliadas']) > 0) {
                foreach ($row['condicoes_avaliadas'] as $condicaoAvaliada) {
                    $condicoesAvaliadas = $dom->createElement('condicoesAvaliadas', $condicaoAvaliada['condicao_avaliada_id']);
                    $atendimentosDomiciliares->appendChild($condicoesAvaliadas);
                }
            }

            if(count($row['cid10']) > 0) {
                foreach ($row['cid10'] as $cid10) {
                    $cid = $dom->createElement('cid', $cid10['cid10']);
                    $atendimentosDomiciliares->appendChild($cid);
                }
            }

            if(count($row['ciap2']) > 0) {
                foreach ($row['ciap2'] as $ciap2) {
                    $ciap = $dom->createElement('ciap', $ciap2['ciap2_id']);
                    $atendimentosDomiciliares->appendChild($ciap);
                }
            }

            if(count($row['procedimentos']) > 0) {
                foreach ($row['procedimentos'] as $procedimento) {
                    $procedimentos = $dom->createElement('procedimentos', $procedimento['codigo']);
                    $atendimentosDomiciliares->appendChild($procedimentos);
                }
            }

            if(count($row['outros_procedimentos']) > 0) {
                foreach ($row['outros_procedimentos'] as $outro_procedimento) {
                    $outrosProcedimentos = $dom->createElement('outrosProcedimentos', $outro_procedimento['codigo']);
                    $atendimentosDomiciliares->appendChild($outrosProcedimentos);
                }
            }

            $condutaDesfecho = $dom->createElement('condutaDesfecho', $row['ficha']['conduta_desfecho']);
            $atendimentosDomiciliares->appendChild($condutaDesfecho);

            $fichaAtendimentoDomiciliarMasterTransport->appendChild($atendimentosDomiciliares);

            $remetente = $dom->createElement('ns2:remetente');
            $dadoTransporteTransportXml->appendChild($remetente);
            $contraChave = $dom->createElement('contraChave', 'PRODUÇÃO');
            $uuidInstalacao = $dom->createElement('uuidInstalacao', $uuid);
            $cpfOuCnpj = $dom->createElement('cpfOuCnpj', $cnpj);
            $nomeOuRazaoSocial = $dom->createElement('nomeOuRazaoSocial', $razaoSocial);
            $fone = $dom->createElement('fone', $telefoneEmpresa);
            $email = $dom->createElement('email', 'contato@assistevida.com.br');
            $versaoSistema = $dom->createElement('versaoSistema', '3.0.1');
            $nomeBancoDados = $dom->createElement('nomeBancoDados', 'Postgres');
            $remetente->appendChild($contraChave);
            $remetente->appendChild($uuidInstalacao);
            $remetente->appendChild($cpfOuCnpj);
            $remetente->appendChild($nomeOuRazaoSocial);
            $remetente->appendChild($fone);
            $remetente->appendChild($email);
            $remetente->appendChild($versaoSistema);
            $remetente->appendChild($nomeBancoDados);

            $originadora = $dom->createElement('ns2:originadora');
            $dadoTransporteTransportXml->appendChild($originadora);
            $contraChave = $dom->createElement('contraChave', 'PRODUÇÃO');
            $uuidInstalacao = $dom->createElement('uuidInstalacao', $uuid);
            $cpfOuCnpj = $dom->createElement('cpfOuCnpj', $cnpj);
            $nomeOuRazaoSocial = $dom->createElement('nomeOuRazaoSocial', $razaoSocial);
            $fone = $dom->createElement('fone', $telefoneEmpresa);
            $email = $dom->createElement('email', 'contato@assistevida.com.br');
            $versaoSistema = $dom->createElement('versaoSistema', '3.0.1');
            $nomeBancoDados = $dom->createElement('nomeBancoDados', 'Postgres');
            $originadora->appendChild($contraChave);
            $originadora->appendChild($uuidInstalacao);
            $originadora->appendChild($cpfOuCnpj);
            $originadora->appendChild($nomeOuRazaoSocial);
            $originadora->appendChild($fone);
            $originadora->appendChild($email);
            $originadora->appendChild($versaoSistema);
            $originadora->appendChild($nomeBancoDados);

            $versao = $dom->createElement('versao');
            $versao->setAttribute('major', '3');
            $versao->setAttribute('minor', '0');
            $versao->setAttribute('revision', '0');
            $dadoTransporteTransportXml->appendChild($versao);

            $dom->appendChild($dadoTransporteTransportXml);

            $dom->save($filepath);
            unset($dom);
        }

        return $xmls;
    }

    private static function getCodIbgeEmpresa($ur)
    {
        $sql = "SELECT codigo_ibge FROM empresas WHERE id = '{$ur}'";
        return current(parent::get($sql, 'assoc'));
    }

    private function checkAndUpdateMatriculaPaciente($paciente, $matricula, $filial)
    {
        if($filial == '1') {
            $sql = <<<SQL
SELECT NUM_MATRICULA_CONVENIO AS matricula FROM clientes WHERE idClientes = '{$paciente}'
SQL;
        } else {
            $sql = <<<SQL
SELECT cns AS matricula FROM paciente_iw WHERE id = '{$paciente}'
SQL;
        }
        $matPaciente = current(parent::get($sql, 'Assoc'))['matricula'];

        if($matPaciente == '' || $matPaciente != $matricula) {
            if($filial == '1') {
                $sql = <<<SQL
UPDATE clientes SET NUM_MATRICULA_CONVENIO = ? WHERE idClientes = ?
SQL;
            } else {
                $sql = <<<SQL
UPDATE paciente_iw SET cns = ? WHERE id = ?
SQL;
            }
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'si',
                    $matricula,
                    $paciente
                );

                $rs = $stmt->execute();

                if($rs) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return true;
    }
}
