<?php

namespace App\Models\Administracao\ESUS;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class TipoAtendimento extends AbstractModel implements DAOInterface
{

	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : "";
		return "SELECT * FROM esus_tipo_atendimento WHERE 1=1 {$id} ORDER BY descricao";
	}

    public static function getFiltered($id = null)
    {
        $sql = "SELECT * FROM esus_tipo_atendimento WHERE codigo IN (7, 8) ORDER BY descricao";
        return parent::get($sql, 'assoc');
    }
}