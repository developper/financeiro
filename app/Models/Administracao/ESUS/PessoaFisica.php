<?php

namespace App\Models\Administracao\ESUS;

class PessoaFisica
{
    protected $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

	public function getAll()
	{
	    $response = [];
	    $sql = "SELECT nu_cns, no_pessoa_fisica FROM tb_pessoa_fisica ORDER BY no_pessoa_fisica";
        $stmt = $this->conn->getQuery($sql);
        foreach ($stmt as $row) {
            $response[] = $row;
        }
        return $response;
	}

    public function getAllJson($term)
    {
        $response = [];
        $term = isset($term) && !empty($term) ? "(tpf.nu_cns ILIKE '%" . $term . "%' OR tpf.nu_cpf ILIKE '%" . $term . "%' OR tpf.no_pessoa_fisica ILIKE '%" . $term . "%')" : "";
        $sql = <<<SQL
SELECT DISTINCT 
  tpf.nu_cns, 
  tbcbo.co_cbo_2002, 
  tpf.no_pessoa_fisica, 
  tpf.nu_cpf,
  tbe.nu_ine,
  tpf.co_ator,
  tbus.nu_cnes,
  CONCAT('CPF: ', tpf.nu_cpf, ' / CNS: ', tpf.nu_cns, ' / CBO: ', tbcbo.co_cbo_2002, ' / INE: ', tbe.nu_ine, ' / ', tpf.no_pessoa_fisica) AS value 
FROM 
  tb_pessoa_fisica AS tpf
  INNER JOIN tb_ator_papel AS tap ON tpf.co_ator = tap.co_ator AND tap.no_tipo_ator_papel = 'LOTACAO'
  INNER JOIN tb_lotacao AS tbl ON tap.co_seq_ator_papel = tbl.co_ator_papel 
  INNER JOIN tb_unidade_saude AS tbus ON tbl.co_unidade_saude = tbus.co_ator_papel
  INNER JOIN tb_cbo AS tbcbo ON tbl.co_cbo = tbcbo.co_cbo
  LEFT JOIN tb_equipe AS tbe ON tbl.co_equipe = tbe.co_seq_equipe
WHERE 
  {$term} 
ORDER BY 
  tpf.no_pessoa_fisica
SQL;

        $stmt = $this->conn->getQuery($sql);
        foreach ($stmt as $row) {
            $response[] = $row;
        }
        return $response;
    }
}