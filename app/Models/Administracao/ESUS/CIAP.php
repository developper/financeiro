<?php

namespace App\Models\Administracao\ESUS;

class CIAP
{
    protected $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

	public function getAll()
	{
	    $response = [];
	    $sql = "SELECT co_ciap, ds_ciap FROM tb_ciap ORDER BY ds_ciap";
        $stmt = $this->conn->getQuery($sql);
        foreach ($stmt as $row) {
            $response[] = $row;
        }
        return $response;
	}
}