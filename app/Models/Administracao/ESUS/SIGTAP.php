<?php

namespace App\Models\Administracao\ESUS;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class SIGTAP extends AbstractModel implements DAOInterface
{

	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : "";
		return "SELECT * FROM esus_procedimentos_sigtap WHERE 1=1 {$id} AND id ORDER BY descricao";
	}

    public static function getAllJson($term)
    {
        $term = isset($term) && !empty($term) ? 'AND (codigo_sigtap LIKE "%' . $term . '%" OR descricao LIKE "%' . $term . '%")' : "";
        $sql = "SELECT id, codigo_sigtap, descricao, CONCAT(codigo_sigtap, ' - ', descricao) AS value FROM esus_procedimentos_sigtap WHERE 1=1 {$term} AND id ORDER BY descricao";
        return parent::get($sql, 'assoc');
    }
}