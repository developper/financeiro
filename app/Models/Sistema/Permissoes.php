<?php

namespace App\Models\Sistema;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class Permissoes extends AbstractModel implements DAOInterface
{

	public static function getAll()
	{
		return parent::get(self::getSQL(), 'Assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id), 'Assoc'));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : "";
		return "SELECT * FROM area_permissoes WHERE 1 = 1 {$id} ORDER BY ordem";
	}

    public static function getByAreaId($area)
    {
        $sql = "SELECT * FROM area_permissoes WHERE area_id = '{$area}' ORDER BY ordem";
        return parent::get($sql, 'Assoc');
    }
}
