<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 16/03/2017
 * Time: 11:17
 */

namespace app\Models\Sistema;

use App\Models\AbstractModel;
use App\Models\DAOInterface;

class TabelaPrestacaoServicosMederi extends AbstractModel implements DAOInterface
{
    public static function getAll ()
    {
        return parent::get (self::getSQL ());
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }



    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? "WHERE id = {$id}" : null;
        return "SELECT * FROM cobrancaplanos {$id} ORDER BY item";
    }

}