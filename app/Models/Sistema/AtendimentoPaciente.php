<?php

namespace app\Models\Sistema;

use App\Models\AbstractModel;
use App\Models\DAOInterface;


class AtendimentoPaciente extends AbstractModel implements DAOInterface
{
    public static function getAll ()
    {
        return parent::get (self::getSQL ());
    }
    public static function getSQL($id = null)
    {
        
    }

    public static function salvar($paciente,$modalidade,$convenio,$matricula,$empresa)
    {
        $sql = "
INSERT INTO atendimento_paciente
 (
  `paciente_id` ,
  `atendimento_paciente_modalidade_id`,
  `created_at` ,
  `created_by` ,
  `plano_id` ,
  `matricula`,
  empresa
 )
VALUES
(
'$paciente',
'$modalidade',
now(),
'{$_SESSION['id_user']}',
'{$convenio}',
'{$matricula}',
'{$empresa}'
) ";

        if(!mysql_query($sql)){
            mysql_query("rollback");
            echo "Erro ao criar Atendimento para o paciente.";
            return;
        }
        $id = mysql_insert_id();

        savelog(mysql_escape_string(addslashes($sql)));
        return $id;
    }

    public static function salvarProcedimento($atendimentoId,$tabela,$tabelaId)
    {
        $sql = "
INSERT INTO atendimento_paciente_procedimento
 (
 atendimento_paciente_id,
 tabela_procedimento,
 tabela_procedimento_id,
 created_at,
 created_by
 )
VALUES
(

'$atendimentoId',
'$tabela',
$tabelaId,
now(),
'{$_SESSION['id_user']}'
) ";

        if(!mysql_query($sql)){
            mysql_query("rollback");
            echo "Erro ao criar procedimento para o atendimento.";
            return;
        }
        $id = mysql_insert_id();
        savelog(mysql_escape_string(addslashes($sql)));
        return $id;
    }

    // utilizada apensa para fazer o rollback na funcionalidade de salvar remoções extrenas.
    public static function deleteRemocao($id){

        $sql = "DELETE from atendimento_paciente where id = {$id}";
        if(!mysql_query($sql)){

            echo "Erro ao excluir atendimento ";
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));

        echo 1;

    }
     public static function getAtendimentoByTabelaId($tabela,$tebelaId){

         $sql = "Select * from atendimento_paciente_procedimento where tabela_procedimento= '{$tabela}' and tabela_procedimento_id = '{$tebelaId}' ";
         return current(parent::get($sql, 'Assoc'));

     }

    public static function updateEmpresa($empresa,$id)
    {
        $sql = "update atendimento_paciente set empresa = {$empresa} WHERE  id = {$id}";
        mysql_query($sql);

        savelog(mysql_escape_string(addslashes($sql)));
        return;


    }

    public static function finalizar($id, $status, $justificativa = null)
    {
        $sql = "
Insert into
`atendimento_paciente_finalizado`
(
`status`,
`justificativa`,
`atendimento_paciente_id`,
`finalized_by`,
`finalized_at`
)
VALUES
(
'{$status}',
'{$justificativa}',
'{$id}',
'{$_SESSION['id_user']}',
NOW()
)
";
        mysql_query($sql);
        savelog(mysql_escape_string(addslashes($sql)));
        return true;
    }

    public static function getAtendimentoPacienteFinalizadoByfilter($idAtendimento)
    {
        $sql = "Select * from atendimento_paciente_finalizado where atendimento_paciente_id = {$idAtendimento} ";
        return current(parent::get($sql, 'Assoc'));

    }

    public static function getById($id)
    {
        $sql = "Select * from atendimento_paciente where id = {$id}";
        return current(parent::get($sql, 'Assoc'));
    }








}