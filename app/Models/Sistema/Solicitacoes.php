<?php

namespace app\Models\Sistema;

use App\Models\AbstractModel;
use App\Models\DAOInterface;

class Solicitacoes extends AbstractModel implements DAOInterface
{
	public static function getAll ()
	{
		return parent::get (self::getSQL ());
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getByPrescricaoCurativoId($id)
	{
		$sql = "SELECT * FROM solicitacoes WHERE id_prescricao_curativo = '{$id}'";
		return parent::get ($sql, 'Assoc');
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? "WHERE id = {$id}" : null;
		return "SELECT * FROM solicitacoes {$id} ORDER BY id";
	}

	public static function getSolicitacoesPendentesByEmpresaId($empresa)
	{
		// Armazenando as solicitacoes pendentes
		$pendentes = self::getAllPacientesPendentes();

		// Armazenando as solicitacoes nao visualizadas
		$naoVisualizados = self::getAllItensNaoVisualizados();

		$solicitacoesAvAdEm 	= [];
		$solicitacoesPeriodicas = [];

		// Trazendo todos os pacientes a serem auditados do tipo Aditivas, Avulsas e Emergencia
		$sql = <<<SQL
SELECT
	c.nome,
	s.paciente,
	c.empresa,
	s.TIPO_SOLICITACAO,
	DATE_FORMAT(
		s.DATA_SOLICITACAO,
		'%d/%m/%Y %H:%i:%s'
	)AS data,
	s.idPrescricao AS idP,
	p.nome AS convenio,
	emp.nome AS unidade_regional,
	MAX(primeira_prescricao)AS primeira_prescricao
FROM
	solicitacoes AS s
INNER JOIN clientes AS c ON(s.paciente = c.idClientes)
INNER JOIN planosdesaude AS p ON(c.convenio = p.id)
INNER JOIN empresas AS emp ON(c.empresa = emp.id)
WHERE
	(s. STATUS = '0' OR s. STATUS = '2')
AND s.data_auditado = '0000-00-00 00:00:00'
AND emp.id = {$empresa}
AND s.TIPO_SOLICITACAO IN ('-2', '-1', '1', '3', '6', '7', '8')
AND s.qtd <> 0
GROUP BY
	s.paciente
ORDER BY
	primeira_prescricao DESC,
	MIN(s. DATA)ASC
SQL;
		$r = mysql_query($sql);
		while($row = mysql_fetch_array($r, MYSQL_ASSOC)) {
			$row['pend'] = 0;
			$row['nao_visualizado'] = 0;
			if(array_key_exists($row['paciente'], $pendentes))
				$row['pend'] = $pendentes[$row['paciente']];
			if(array_key_exists($row['paciente'], $naoVisualizados))
				$row['nao_visualizado'] = $naoVisualizados[$row['paciente']];

			$solicitacoesAvAdEm[] = $row;
		}

		// Trazendo todos os pacientes a serem auditados do tipo Periodicas
		$sql = <<<SQL
SELECT
	c.nome,
	s.paciente,
	c.empresa,
	s.TIPO_SOLICITACAO,
	DATE_FORMAT(
		s.DATA_SOLICITACAO,
		'%d/%m/%Y %H:%i:%s'
	)AS data,
	s.idPrescricao AS idP,
	p.nome AS convenio,
	emp.nome AS unidade_regional,
	MAX(primeira_prescricao)AS primeira_prescricao
FROM
	solicitacoes AS s
INNER JOIN clientes AS c ON(s.paciente = c.idClientes)
INNER JOIN planosdesaude AS p ON(c.convenio = p.id)
INNER JOIN empresas AS emp ON(c.empresa = emp.id)
WHERE
	(s. STATUS = '0' OR s. STATUS = '2')
AND s.data_auditado = '0000-00-00 00:00:00'
AND emp.id = {$empresa}
AND s.TIPO_SOLICITACAO NOT IN ('-2', '-1', '1', '3', '6', '7', '8')
AND s.qtd <> 0
GROUP BY
	s.paciente
ORDER BY
	primeira_prescricao DESC,
	MIN(s. DATA)ASC
SQL;
		$r = mysql_query($sql);
		while($row = mysql_fetch_array($r, MYSQL_ASSOC)) {
			$row['pend'] = 0;
			$row['nao_visualizado'] = 0;
			if(array_key_exists($row['paciente'], $pendentes))
				$row['pend'] = $pendentes[$row['paciente']];
			if(array_key_exists($row['paciente'], $naoVisualizados))
				$row['nao_visualizado'] = $naoVisualizados[$row['paciente']];

			$solicitacoesPeriodicas[] = $row;
		}

		return [$solicitacoesAvAdEm, $solicitacoesPeriodicas];
	}

	public static function getAllPacientesPendentes()
	{
		$pendentes = [];

		$sqlPendentes = <<<SQL
SELECT
	count(PENDENTE) as pend,
	paciente
FROM
	solicitacoes
WHERE
	(
		STATUS = '0'
		OR STATUS = '2'
	)
AND data_auditado = '0000-00-00 00:00:00'
AND PENDENTE = 'S'
AND qtd <> 0
GROUP BY
	paciente
SQL;
		$rs = mysql_query($sqlPendentes);
		while($row = mysql_fetch_array($rs, MYSQL_ASSOC))
			$pendentes[$row['paciente']] = $row['pend'];

		return $pendentes;
	}

	public static function getAllItensNaoVisualizados()
	{
		$naoVisualizados = [];

		$sqlNaoVisualizados = <<<SQL
SELECT
	count(visualizado) AS nao_visualizado,
	paciente
FROM
	solicitacoes
WHERE
	(STATUS = '0' OR STATUS = '2')
AND data_auditado = '0000-00-00 00:00:00'
AND visualizado = 0
AND qtd <> 0
GROUP BY
	paciente
SQL;
		$rs = mysql_query($sqlNaoVisualizados);
		while($row = mysql_fetch_array($rs, MYSQL_ASSOC))
			$naoVisualizados[$row['paciente']] = $row['nao_visualizado'];

		return $naoVisualizados;
	}
}
