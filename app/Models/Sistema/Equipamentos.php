<?php
namespace app\Models\Sistema;

use App\Models\AbstractModel;
use App\Models\DAOInterface;

class Equipamentos extends AbstractModel implements DAOInterface
{
	public static function getAll ()
	{
		return parent::get(self::getSQL ());
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'WHERE id = "' . $id . '"' : 'WHERE 1 = 1';
		return "SELECT * FROM cobrancaplanos {$id} AND ATIVO = 1 AND tipo = 1 ORDER BY item ASC";
	}

    public static function getEquipamentosAlugados($filters)
    {
        $compPaciente = $filters['paciente'] != '' ?
            " AND equipamentosativos.PACIENTE_ID = '{$filters['paciente']}' " :
            "";
        $compFornecedor = $filters['fornecedor'] != '' ?
            " AND equipamentosativos.fornecedor = '{$filters['fornecedor']}' " :
            "";
        $compEquipamento = $filters['equipamento'] != '' ?
            " AND equipamentosativos.COBRANCA_PLANOS_ID = '{$filters['equipamento']}' " :
            "";
        $compUR = $filters['ur'] != '' ?
            " AND clientes.empresa = '{$filters['ur']}' " : '';


        $sql = <<<SQL
SELECT 
  equipamentosativos.DATA_INICIO,
  equipamentosativos.DATA_FIM,
  IF(equipamentosativos.DATA_FIM = '0000-00-00', 'ATIVO', 'RECOLHIDO') as situacao,
  cobrancaplanos.item AS equipamento,
  saida.LOTE AS tombo,
  clientes.nome AS paciente,
  DATE_FORMAT(clientes.dataInternacao, '%d/%m/%Y') AS admissao,
  IF(fornecedores.razaoSocial != '', fornecedores.razaoSocial, fornecedores.nome) AS empresa,
  fornecedores_custo_equipamento.valor_aluguel,
  empresas.nome as ur
FROM
  equipamentosativos
  INNER JOIN cobrancaplanos ON equipamentosativos.COBRANCA_PLANOS_ID = cobrancaplanos.id
  INNER JOIN clientes ON equipamentosativos.PACIENTE_ID = clientes.idClientes
  INNER JOIN empresas ON  clientes.empresa = empresas.id
  INNER JOIN saida ON equipamentosativos.saida_id = saida.idSaida
  LEFT JOIN fornecedores ON equipamentosativos.fornecedor = fornecedores.idFornecedores
  LEFT JOIN fornecedores_custo_equipamento ON (
    equipamentosativos.fornecedor = fornecedores_custo_equipamento.fornecedor_id
    AND equipamentosativos.COBRANCA_PLANOS_ID = fornecedores_custo_equipamento.cobrancaplanos_id
  )

WHERE
  equipamentosativos.DATA_INICIO <> '0000-00-00 00:00:00'
  AND equipamentosativos.PRESCRICAO_AVALIACAO <> 's'
  AND ( 
    equipamentosativos.DATA_INICIO BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}' 
    OR equipamentosativos.DATA_FIM BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
    OR (
      equipamentosativos.DATA_INICIO < '{$filters['fim']}'
      AND  ( equipamentosativos.DATA_FIM = '0000-00-00 00:00:00' OR equipamentosativos.DATA_FIM > '{$filters['fim']}')
    )
  )
  AND saida.tipo = 5
  {$compPaciente}
  {$compFornecedor}
  {$compEquipamento}
  {$compUR}
SQL;



        return parent::get($sql);

	}

    public static function getEquipamentosAtivosByPaciente($idPaciente)
    {
        $sql = <<<SQL

 SELECT
        equipamentosativos.*,
        cobrancaplanos.item,
        DATE_FORMAT(equipamentosativos.DATA_INICIO,'%d/%m/%Y') as datapt
    FROM
        equipamentosativos
    INNER JOIN cobrancaplanos ON (
        equipamentosativos.COBRANCA_PLANOS_ID = cobrancaplanos.id
    )
    WHERE
        equipamentosativos.PACIENTE_ID = {$idPaciente}
        AND equipamentosativos.DATA_FIM = '0000-00-00 00:00:00'
        AND equipamentosativos.DATA_INICIO > '0000-00-00 00:00:00'
    ORDER BY
	equipamentosativos.DATA_INICIO DESC
SQL;
                return parent::get($sql);

    }
}