<?php

namespace App\Models\Sistema;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class Areas extends AbstractModel implements DAOInterface
{

	public static function getAll()
	{
		return parent::get(self::getSQL(), 'Assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id), 'Assoc'));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : "";
		return "SELECT * FROM area_sistema WHERE 1 = 1 {$id} ORDER BY id_pai, ordem";
	}

    public static function getByIdPai($id_pai = 0)
    {
        $sql = "SELECT * FROM area_sistema WHERE id_pai = '{$id_pai}' ORDER BY ordem";
        return parent::get($sql, 'Assoc');
    }

    public static function getAreas($id_pai = 0)
    {
        $data = [];
        $areas = self::getByIdPai($id_pai);

        foreach ($areas as $area) {
            $data[$area['id']] = $area;
            $data[$area['id']]['filhos'] = self::getAreas($area['id']);
            $data[$area['id']]['permissoes'] = Permissoes::getByAreaId($area['id']);
        }

        return $data;
	}
}
