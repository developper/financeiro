<?php

namespace App\Models\Sistema;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class Profissionais extends AbstractModel implements DAOInterface
{

	public static function getAll()
	{
		return parent::get(self::getSQL(), 'Assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id), 'Assoc'));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : "";
		return "SELECT * FROM profissionais WHERE 1 = 1 {$id} ORDER BY descricao";
	}
}
