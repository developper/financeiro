<?php

namespace app\Models\Sistema;

use App\Models\AbstractModel;
use App\Models\DAOInterface;

class CaraterPrescricao extends AbstractModel implements DAOInterface
{
	public static function getAll ()
	{
		return parent::get (self::getSQL ());
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getByProfissional($profissional, $except = null)
	{
		$except = $except !== null ? "AND id NOT IN (" . implode(',', $except) . ")" : "";
		$sql = "SELECT * FROM carater_prescricao WHERE profissional = '{$profissional}' {$except} ORDER BY nome";
		return parent::get ($sql, 'Assoc');
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? "WHERE id = {$id}" : null;
		return "SELECT * FROM carater_prescricao {$id} ORDER BY id";
	}
}