<?php
namespace app\Models\Sistema;

use App\Models\AbstractModel;
use App\Models\DAOInterface;

class Frequencia extends AbstractModel implements DAOInterface
{
	public static function getAll ()
	{
		return parent::get (self::getSQL ());
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'WHERE id = "' . $id . '"' : null;
		return "SELECT * FROM frequencia {$id} ORDER BY frequencia";
	}

	public static function getFrequenciasPrescricaoCurativo()
	{
		$sql = "SELECT * FROM frequencia WHERE id IN (1,13,15,16,20,27,30,36,37,38) ORDER BY id ASC";
		return parent::get ($sql, 'Assoc');
	}
}