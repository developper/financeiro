<?php

namespace App\Models\Sistema;

use App\Models\AbstractModel,
    App\Models\DAOInterface;

class CategoriaMaterial extends AbstractModel implements DAOInterface
{
    public static function getAll()
    {
        return parent::get(self::getSQL());
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id), 'Assoc'));
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? 'AND ID = "' . $id . '"' : "";
        return "SELECT * FROM categorias_materiais WHERE 1 = 1 {$id}";
    }

    public static function getByTerm($term = null)
    {
        $term = isset($term) && !empty($term) ? 'AND ITEM LIKE "%' . $term . '%"' : "";
        $sql = "SELECT ITEM AS value , ID FROM categorias_materiais WHERE 1 = 1 {$term}";
        return parent::get($sql, 'Assoc');
    }

    public static function getUsersListSQL($data = [])
    {
        $compNome = "";

        if(!empty($data)) {
            $compNome = isset($data['nome']) && $data['nome'] != '' ?
                " AND ITEM LIKE '%{$data['nome']}%'" :
                "";
        }

        return <<<SQL
SELECT
    ID,
    ITEM              
FROM 
    categorias_materiais
WHERE
    DELETED_AT IS NULL
    {$compNome}
SQL;
    }

    public static function save($data)
    {
        mysql_query('BEGIN');

        $sql = <<<SQL
INSERT INTO `categorias_materiais` (
    `ITEM`
 ) VALUES( 
    '{$data['nome']}'
)
SQL;
        $rs = mysql_query($sql);

        if(!$rs) {
            mysql_query('ROLLBACK');
            return "Erro ao salvar o usuário!";
        }

        return mysql_query('COMMIT');
    }

    public static function update($data)
    {
        mysql_query('BEGIN');

        $sql = <<<SQL
UPDATE `categorias_materiais` SET
    `ITEM` = '{$data['nome']}'    
WHERE 
    ID = '{$data['id']}' 
SQL;
        $rs = mysql_query($sql);

        if(!$rs) {
            mysql_query('ROLLBACK');
            return "Erro ao salvar o usuário!";
        }

        return mysql_query('COMMIT');
    }

    public static function checkIfExistsMaterial($id)
    {
        $sql = <<<SQL
SELECT 
  COUNT(*) AS qtd
FROM 
  catalogo
WHERE 
  CATEGORIA_MATERIAL_ID = '{$id}'
SQL;
        $response = parent::get($sql, 'Assoc');
        return $response[0]['qtd'] > 0;
    }

    public static function delete($id)
    {
        $sql = <<<SQL
UPDATE `categorias_materiais` SET
    `DELETED_AT` = NOW(),    
    `DELETED_BY` = '{$_SESSION['id_user']}'    
WHERE 
    ID = '{$id}'
SQL;
        return mysql_query($sql);
    }
}
