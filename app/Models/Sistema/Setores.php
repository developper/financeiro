<?php
namespace App\Models\Sistema;

use App\Models\AbstractModel;
use App\Models\DAOInterface;

class Setores extends AbstractModel implements DAOInterface
{
	public static function getAll ()
	{
		return parent::get(self::getSQL ());
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'WHERE id = "' . $id . '"' : 'WHERE 1 = 1';
		return "SELECT * FROM setores {$id}  ORDER BY setor ASC";
	}


}