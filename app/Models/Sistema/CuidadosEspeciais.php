<?php
namespace App\Models\Sistema;

use App\Models\AbstractModel;
use App\Models\DAOInterface;

class CuidadosEspeciais extends AbstractModel implements DAOInterface
{
	public static function getAll ()
	{
		return parent::get (self::getSQL ());
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'WHERE id = "' . $id . '"' : null;
		return "SELECT * FROM cuidadosespeciais {$id} ORDER BY id";
	}

	public static function getCuidadosEspeciaisByGrupo($grupo = [])
	{
        $grupoComp = "";
	    if(count($grupo) > 0) {
	        $grupoImp = implode(', ', $grupo);
	        $grupoComp = " AND GRUPO IN ({$grupoImp}) ";
        }
		$sql = "SELECT * FROM cuidadosespeciais WHERE 1 = 1 {$grupoComp} ORDER BY id ASC";
		return parent::get ($sql, 'Assoc');
	}
}