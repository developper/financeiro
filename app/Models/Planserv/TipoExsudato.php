<?php
namespace app\Models\Planserv;

use App\Models\AbstractModel,
	\App\Models\DAOInterface;


class TipoExsudato extends AbstractModel implements DAOInterface
{
	public static function getAll ()
	{
		return parent::get (self::getSQL (), 'Assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		return "SELECT * FROM planserv_tipo_exsudato_ferida";
	}
}