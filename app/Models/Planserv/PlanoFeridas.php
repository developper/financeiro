<?php
namespace app\Models\Planserv;

use App\Models\AbstractModel,
    \App\Models\DAOInterface;


class PlanoFeridas extends AbstractModel implements DAOInterface
{
    public static function getAll ()
    {
        return parent::get (self::getSQL (), 'Assoc');
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getSQL($id = null)
    {
        $id = $id != null ? " AND id = {$id}" : "";
        return "SELECT * FROM planserv_plano_feridas WHERE 1=1 {$id} ORDER BY id DESC ";
    }

    public static function listAll($paciente, $inicio, $fim)
    {
        $sql = <<<SQL
SELECT 
  planserv_plano_feridas.*,
  clientes.nome AS paciente_nome,
  usuarios.nome AS usuario
FROM
  planserv_plano_feridas
  INNER JOIN clientes ON planserv_plano_feridas.paciente = clientes.idClientes
  INNER JOIN usuarios ON planserv_plano_feridas.created_by = usuarios.idUsuarios
WHERE
  planserv_plano_feridas.paciente = '{$paciente}'
  AND 
    (
      inicio BETWEEN '{$inicio}' AND '{$fim}'
      OR fim BETWEEN '{$inicio}' AND '{$fim}'
    )
ORDER BY 
  id DESC
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getPlanoFeridas($id)
    {
        $sql = <<<SQL
SELECT 
  planserv_plano_feridas.*,
  clientes.nome AS paciente_nome,
  FLOOR(DATEDIFF(CURDATE(),clientes.`nascimento`)/365.25) AS idade,
  clientes.nascimento AS data_nasc,
  clientes.dataInternacao AS data_admissao,
  clientes.sexo,
  usuarios.nome AS usuario
FROM
  planserv_plano_feridas
  INNER JOIN clientes ON planserv_plano_feridas.paciente = clientes.idClientes
  INNER JOIN usuarios ON planserv_plano_feridas.created_by = usuarios.idUsuarios
WHERE
  planserv_plano_feridas.id = '{$id}'
SQL;
        return current(parent::get($sql, 'assoc'));
    }

    public static function getPlanoFeridasProfissionais($id)
    {
        $sql = <<<SQL
SELECT
  planserv_plano_feridas_profissionais.*,
  usr1.nome AS med,
  usr1.conselho_regional AS cr_med,
  usr2.nome AS enf,
  usr2.conselho_regional AS cr_enf
FROM
  planserv_plano_feridas_profissionais
  LEFT JOIN usuarios AS usr1 ON planserv_plano_feridas_profissionais.medico = usr1.idUsuarios
  LEFT JOIN usuarios AS usr2 ON planserv_plano_feridas_profissionais.enfermeiro = usr2.idUsuarios
WHERE
  planserv_plano_feridas_profissionais.plano_feridas_id = '{$id}'
SQL;
        return current(parent::get($sql, 'assoc'));
    }

    public static function getPlanoFeridasBeneficiario($id)
    {
        $sql = <<<SQL
SELECT 
  planserv_plano_feridas_beneficiario.*
FROM
  planserv_plano_feridas_beneficiario
WHERE
  planserv_plano_feridas_beneficiario.plano_feridas_id = '{$id}'
SQL;
        return current(parent::get($sql, 'assoc'));
    }

    public static function getPlanoFeridasHistorico($id)
    {
        $sql = <<<SQL
SELECT 
  planserv_plano_feridas_historico_paciente.*
FROM
  planserv_plano_feridas_historico_paciente
WHERE
  planserv_plano_feridas_historico_paciente.plano_feridas_id = '{$id}'
SQL;
        return current(parent::get($sql, 'assoc'));
    }

    public static function getPlanoFeridasIncontinencia($id)
    {
        $sql = <<<SQL
SELECT 
  planserv_plano_feridas_incontinencia.*
FROM
  planserv_plano_feridas_incontinencia
WHERE
  planserv_plano_feridas_incontinencia.plano_feridas_id = '{$id}'
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getPlanoFeridasDoencasSistemicas($id)
    {
        $sql = <<<SQL
SELECT 
  planserv_plano_feridas_doencas_sistemicas.*
FROM
  planserv_plano_feridas_doencas_sistemicas
WHERE
  planserv_plano_feridas_doencas_sistemicas.plano_feridas_id = '{$id}'
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getPlanoFeridasLocais($id)
    {
        $sql = <<<SQL
SELECT 
  planserv_plano_feridas_locais.*,
  planserv_plano_feridas_locais.local_ferida AS local_ferida_id,
  quadro_feridas_enf_local.LOCAL AS local_ferida,
  planserv_etiologia_ferida.id AS etiologia_ferida_id,
  planserv_etiologia_ferida.descricao AS etiologia_ferida,
  planserv_tipo_exsudato_ferida.id AS tipo_id,
  planserv_tipo_exsudato_ferida.descricao AS tipo,
  planserv_volume_exsudato_ferida.id AS volume_id,
  planserv_volume_exsudato_ferida.descricao AS volume,
  planserv_cor_borda_ferida.id AS cor_id,
  planserv_cor_borda_ferida.descricao AS cor
FROM
  planserv_plano_feridas_locais
  INNER JOIN quadro_feridas_enf_local ON planserv_plano_feridas_locais.local_ferida = quadro_feridas_enf_local.ID
  INNER JOIN planserv_etiologia_ferida ON planserv_plano_feridas_locais.etiologia = planserv_etiologia_ferida.id
  INNER JOIN planserv_tipo_exsudato_ferida ON planserv_plano_feridas_locais.tipo_exsudato = planserv_tipo_exsudato_ferida.id
  INNER JOIN planserv_volume_exsudato_ferida ON planserv_plano_feridas_locais.volume_exsudato = planserv_volume_exsudato_ferida.id
  INNER JOIN planserv_cor_borda_ferida ON planserv_plano_feridas_locais.cor = planserv_cor_borda_ferida.id
WHERE
  planserv_plano_feridas_locais.plano_feridas_id = '{$id}'
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getPlanoFeridasImgs($evolucao)
    {
        $sql = <<<SQL
SELECT
  quadro_feridas_enfermagem.*, 
  full_path, 
  path, 
  quadro_feridas_enf_local.LOCAL as descricao_local_ferida,
  quadro_feridas_enfermagem.ID as idQuadroFeridas
FROM
quadro_feridas_enfermagem
  LEFT JOIN files_uploaded ON files_uploaded.id = quadro_feridas_enfermagem.FILE_UPLOADED_ID
  LEFT JOIN quadro_feridas_enf_local ON quadro_feridas_enf_local.id = quadro_feridas_enfermagem.LOCAL
WHERE
  EVOLUCAO_ENF_ID = '{$evolucao}'
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getPlanoFeridasImgsFromProrrogacao($data)
    {
        $data['inicio'] = \DateTime::createFromFormat('d/m/Y', $data['inicio'])->format('Y-m-d');
        $data['fim'] = \DateTime::createFromFormat('d/m/Y', $data['fim'])->format('Y-m-d');

        $sql = <<<SQL
SELECT 
  ID 
FROM 
  relatorio_prorrogacao_enf 
WHERE 
  PACIENTE_ID = '{$data['paciente_id']}'
  AND INICIO BETWEEN '{$data['inicio']}' AND '{$data['fim']}'
  AND FIM BETWEEN '{$data['inicio']}' AND '{$data['fim']}'
ORDER BY ID DESC LIMIT 1
SQL;
        $prorrogacao_id = current(parent::get($sql, 'Assoc'))['ID'];
        if(!$prorrogacao_id) {
            $sql = <<<SQL
SELECT 
  ID 
FROM 
  relatorio_prorrogacao_enf 
WHERE 
  PACIENTE_ID = '{$data['paciente_id']}'
ORDER BY ID DESC LIMIT 1
SQL;
            $prorrogacao_id = current(parent::get($sql, 'Assoc'))['ID'];
        }

        $sql = <<<SQL
SELECT
    feridas_relatorio_prorrogacao_enf.ID AS ID,
    feridas_relatorio_prorrogacao_enf.LOCAL as LOCAL,
    feridas_relatorio_prorrogacao_enf.CARACTERISTICA as CARACTERISTICA,
    feridas_relatorio_prorrogacao_enf.TIPO_CURATIVO as curativo,
    feridas_relatorio_prorrogacao_enf.PRESCRICAO_PROX_PERIODO as prescricao,
    feridas_relatorio_prorrogacao_enf.DATA,
    files_uploaded.full_path,
    files_uploaded.path
FROM
    feridas_relatorio_prorrogacao_enf
    LEFT JOIN feridas_relatorio_prorrogacao_enf_imagem ON feridas_relatorio_prorrogacao_enf.ID = feridas_relatorio_prorrogacao_enf_imagem.ferida_relatorio_prorrogacao_enf_id
    INNER JOIN files_uploaded on feridas_relatorio_prorrogacao_enf_imagem.file_uploaded_id = files_uploaded.id
WHERE 
    RELATORIO_PRORROGACAO_ENF_ID = '{$prorrogacao_id}'  
    AND EDITADA_EM IS NULL 
    AND DATA_DESATIVADA IS NULL
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getPlanoFeridasCoberturas($id)
    {
        $sql = <<<SQL
SELECT 
  planserv_plano_feridas_coberturas.*,
  CONCAT(catalogo.principio, ' ', catalogo.apresentacao) AS item,
  frequencia.id AS frequencia_item_id,
  frequencia.frequencia AS frequencia_item
FROM
  planserv_plano_feridas_coberturas
  INNER JOIN catalogo ON planserv_plano_feridas_coberturas.catalogo_id = catalogo.ID
  INNER JOIN frequencia ON planserv_plano_feridas_coberturas.frequencia = frequencia.id
WHERE
  planserv_plano_feridas_coberturas.plano_feridas_id = '{$id}'
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function save($data)
    {
        mysql_query('BEGIN');

        $sql = <<<SQL
INSERT INTO 
  planserv_plano_feridas (
    id, 
    paciente, 
    inicio, 
    fim, 
    justificativa, 
    informacoes_complementares,
    created_by,
    created_at
  ) VALUES (
    NULL,
    '{$data['paciente']}',
    '{$data['inicio_plano']}',
    '{$data['fim_plano']}',
    '{$data['justificativa_solicitacao']}',
    '{$data['informacoes_complementares']}',
    '{$_SESSION['id_user']}',
    NOW()
  )
SQL;
        $rs['plano_feridas'] = mysql_query($sql);

        if(!$rs['plano_feridas']) {
            mysql_query('ROLLBACK');
            return false;
        }

        $planoId = mysql_insert_id();

        $sql = <<<SQL
INSERT INTO planserv_plano_feridas_profissionais (id, plano_feridas_id, medico, enfermeiro)
VALUES (NULL, '{$planoId}', '{$data['medico']}', '{$data['enfermeiro']}')
SQL;

        $rs['profissionais'] = mysql_query($sql);

        if(!$rs['profissionais']) {
            mysql_query('ROLLBACK');
            return false;
        }

        $sql = <<<SQL
INSERT INTO planserv_plano_feridas_beneficiario (id, plano_feridas_id, modalidade, isolamento, tipo_isolamento, deambula, peso, altura)
VALUES (NULL, '{$planoId}', '{$data['modalidade_atendimento']}', '{$data['isolamento']}', '{$data['isolamento_descricao']}', '{$data['deambula']}', '{$data['peso']}', '{$data['altura']}')
SQL;

        $rs['beneficiario'] = mysql_query($sql);

        if(!$rs['beneficiario']) {
            mysql_query('ROLLBACK');
            return false;
        }

        $sql = <<<SQL
INSERT INTO 
  planserv_plano_feridas_historico_paciente
  (
    id, 
    plano_feridas_id,
    diagnostico_base,
    cid,
    outras_doencas_sistemicas,
    tempo_ferida,
    tratamento_atual,
    tipos_coberturas,
    tempo_uso,
    frequencia_troca,
    infeccao,
    microorganismo
  ) VALUES (
    NULL, 
    '{$planoId}', 
    '{$data['diagnostico_base']}', 
    '{$data['cid']}', 
    '{$data['outras_doencas_sistemicas']}', 
    '{$data['tempo_ferida']}', 
    '{$data['tratamento_atual']}',
    '{$data['tipos_coberturas']}',
    '{$data['tempo_uso']}',
    '{$data['frequencia_troca']}',
    '{$data['infeccao']}',
    '{$data['microorganismo']}'
  )
SQL;

        $rs['historico'] = mysql_query($sql);

        if(!$rs['historico']) {
            mysql_query('ROLLBACK');
            return false;
        }

        foreach ($data['incontinencia'] as $incontinencia) {
            $sql = <<<SQL
INSERT INTO 
  planserv_plano_feridas_incontinencia
  (
    id, 
    plano_feridas_id,
    incontinencia_id
  ) VALUES (
    NULL, 
    '{$planoId}', 
    '{$incontinencia}'
  )
SQL;
            $rs['incontinencia'] = mysql_query($sql);

            if(!$rs['incontinencia']) {
                mysql_query('ROLLBACK');
                return false;
            }
        }

        foreach ($data['doencas_sistemicas'] as $doenca_sistemica) {
            $sql = <<<SQL
INSERT INTO 
  planserv_plano_feridas_doencas_sistemicas
  (
    id, 
    plano_feridas_id,
    doenca_sistemica
  ) VALUES (
    NULL, 
    '{$planoId}', 
    '{$doenca_sistemica}'
  )
SQL;
            $rs['doencas_sistemicas'] = mysql_query($sql);

            if(!$rs['doencas_sistemicas']) {
                mysql_query('ROLLBACK');
                return false;
            }
        }

        $data['locais'] = array_filter($data['locais']);
        $data['comprimento'] = array_filter($data['comprimento']);
        $data['largura'] = array_filter($data['largura']);
        $data['profundidade'] = array_filter($data['profundidade']);
        $data['descolamento'] = array_filter($data['descolamento']);
        $data['tecido_granulacao'] = array_filter($data['tecido_granulacao']);
        $data['tecido_esfacelo'] = array_filter($data['tecido_esfacelo']);
        $data['tecido_fibrina'] = array_filter($data['tecido_fibrina']);
        $data['tecido_necrotico'] = array_filter($data['tecido_necrotico']);
        $data['exposicao'] = array_filter($data['exposicao']);
        $data['tipo_exsudato'] = array_filter($data['tipo_exsudato']);
        $data['volume_exsudato'] = array_filter($data['volume_exsudato']);
        $data['odor'] = array_filter($data['odor']);
        $data['cor'] = array_filter($data['cor']);
        $data['edema'] = array_filter($data['edema']);
        $data['maceracao'] = array_filter($data['maceracao']);
        $data['bordas'] = array_filter($data['bordas']);
        $data['instrumental'] = array_filter($data['instrumental']);
        $data['cirurgico'] = array_filter($data['cirurgico']);
        foreach ($data['locais'] as $key => $local) {
            $lado = isset($data['lado'][$key]) && $data['lado'][$key] != '' ? $data['lado'][$key] : 0;
            $sql = <<<SQL
INSERT INTO 
  planserv_plano_feridas_locais
  (
    id, 
    plano_feridas_id,
    local_ferida,
    lado,
    etiologia,
    comprimento,
    largura,
    profundidade,
    descolamento,
    tecido_granulacao,
    tecido_esfacelo,
    tecido_fibrina,
    tecido_necrotico,
    exposicao,
    tipo_exsudato,
    volume_exsudato,
    odor,
    cor,
    edema,
    maceracao,
    bordas,
    instrumental,
    cirurgico
  ) VALUES (
    NULL, 
    '{$planoId}', 
    '{$local}', 
    '{$lado}', 
    '{$data['etiologia'][$key]}', 
    '{$data['comprimento'][$key]}', 
    '{$data['largura'][$key]}', 
    '{$data['profundidade'][$key]}',
    '{$data['descolamento'][$key]}',
    '{$data['tecido_granulacao'][$key]}',
    '{$data['tecido_esfacelo'][$key]}',
    '{$data['tecido_fibrina'][$key]}',
    '{$data['tecido_necrotico'][$key]}',
    '{$data['exposicao'][$key]}',
    '{$data['tipo_exsudato'][$key]}',
    '{$data['volume_exsudato'][$key]}',
    '{$data['odor'][$key]}',
    '{$data['cor'][$key]}',
    '{$data['edema'][$key]}',
    '{$data['maceracao'][$key]}',
    '{$data['bordas'][$key]}',
    '{$data['instrumental'][$key]}',
    '{$data['cirurgico'][$key]}'
)
SQL;
            $rs['locais'] = mysql_query($sql) or die(mysql_error() . $sql);

            $data['feridas'][$key] = mysql_insert_id();
        }

        if(!$rs['locais']) {
            mysql_query('ROLLBACK');
            return false;
        }

        $hasOne = false;
        $sql = <<<SQL
INSERT INTO 
  planserv_plano_feridas_coberturas
  (
    id, 
    plano_feridas_id,
    feridas_id,
    tipo_cobertura,
    catalogo_id,
    tamanho,
    quantidade,
    frequencia
  ) VALUES 
SQL;

        foreach ($data['catalogo_id']['cp'] as $key => $item) {
            if ($item != '' && $data['quantidade']['cp'][$key] > 0) {
                $hasOne = true;
                $sql .= <<<SQL
(
    NULL, 
    '{$planoId}', 
    '{$data['feridas'][$key]}', 
    'cp', 
    '{$item}', 
    '{$data['tamanho']['cp'][$key]}', 
    '{$data['quantidade']['cp'][$key]}', 
    '{$data['frequencia']['cp'][$key]}'
),
SQL;
            }
        }

        $sql = substr_replace($sql, ';', -1, 1);

        if($hasOne) {
            $rs['cp'] = mysql_query($sql) or die(mysql_error());
        } else {
            $rs['cp'] = true;
        }

        if (!$rs['cp']) {
            mysql_query('ROLLBACK');
            return false;
        }

        $hasOne = false;
        $sql = <<<SQL
INSERT INTO 
  planserv_plano_feridas_coberturas
  (
    id, 
    plano_feridas_id,
    feridas_id,
    tipo_cobertura,
    catalogo_id,
    tamanho,
    quantidade,
    frequencia
  ) VALUES 
SQL;

        foreach ($data['catalogo_id']['cs'] as $key => $item) {
            if ($item != '' && $data['quantidade']['cs'][$key] > 0) {
                $hasOne = true;
                $sql .= <<<SQL
(
    NULL, 
    '{$planoId}', 
    '{$data['feridas'][$key]}', 
    'cs', 
    '{$item}', 
    '{$data['tamanho']['cs'][$key]}', 
    '{$data['quantidade']['cs'][$key]}', 
    '{$data['frequencia']['cs'][$key]}'
),
SQL;
            }
        }

        $sql = substr_replace($sql, ';', -1, 1);

        if($hasOne) {
            $rs['cs'] = mysql_query($sql) or die(mysql_error());
        } else {
            $rs['cs'] = true;
        }

        if (!$rs['cs']) {
            mysql_query('ROLLBACK');
            return false;
        }

        return [
            'result' => !in_array(false, $rs) ? mysql_query('COMMIT') : false,
            'id' => $planoId
        ];
    }

    public static function update($data)
    {
        mysql_query('BEGIN');

        $id = $data['plano_feridas_id'];

        $sql = <<<SQL
UPDATE 
  planserv_plano_feridas SET  
    inicio = '{$data['inicio_plano']}', 
    fim = '{$data['fim_plano']}', 
    justificativa = '{$data['justificativa_solicitacao']}', 
    informacoes_complementares = '{$data['informacoes_complementares']}'
  WHERE
    id = '{$id}'
SQL;
        $rs['plano_feridas'] = mysql_query($sql);

        if(!$rs['plano_feridas']) {
            mysql_query('ROLLBACK');
            return false;
        }

        $sql = <<<SQL
UPDATE 
  planserv_plano_feridas_profissionais SET 
    medico = '{$data['medico']}', 
    enfermeiro = '{$data['enfermeiro']}'
  WHERE
    id = '{$id}'
SQL;

        $rs['profissionais'] = mysql_query($sql);

        if(!$rs['profissionais']) {
            mysql_query('ROLLBACK');
            return false;
        }

        $sql = <<<SQL
UPDATE 
  planserv_plano_feridas_beneficiario SET
    modalidade = '{$data['modalidade_atendimento']}', 
    isolamento = '{$data['isolamento']}', 
    tipo_isolamento = '{$data['isolamento_descricao']}', 
    deambula = '{$data['deambula']}', 
    peso = '{$data['peso']}', 
    altura = '{$data['altura']}'
  WHERE
    id = '{$id}'
SQL;

        $rs['beneficiario'] = mysql_query($sql);

        if(!$rs['beneficiario']) {
            mysql_query('ROLLBACK');
            return false;
        }

        $sql = <<<SQL
UPDATE 
  planserv_plano_feridas_historico_paciente SET
    diagnostico_base = '{$data['diagnostico_base']}',
    cid = '{$data['cid']}',
    outras_doencas_sistemicas = '{$data['outras_doencas_sistemicas']}',
    tempo_ferida = '{$data['tempo_ferida']}',
    tratamento_atual = '{$data['tratamento_atual']}',
    tipos_coberturas = '{$data['tipos_coberturas']}',
    tempo_uso = '{$data['tempo_uso']}',
    frequencia_troca = '{$data['frequencia_troca']}',
    infeccao = '{$data['infeccao']}',
    microorganismo = '{$data['microorganismo']}'
  WHERE
    id = '{$id}'
SQL;

        $rs['historico'] = mysql_query($sql);

        if(!$rs['historico']) {
            mysql_query('ROLLBACK');
            return false;
        }

        $sql = <<<SQL
DELETE FROM planserv_plano_feridas_incontinencia WHERE plano_feridas_id = '{$id}'
SQL;
        $rsDel = mysql_query($sql);

        foreach ($data['incontinencia'] as $incontinencia) {
            $sql = <<<SQL
INSERT INTO 
  planserv_plano_feridas_incontinencia
  (
    id, 
    plano_feridas_id,
    incontinencia_id,
  ) VALUES (
    NULL, 
    '{$id}', 
    '{$incontinencia}'
  )
SQL;
            $rs['incontinencia'] = mysql_query($sql);

            if(!$rs['incontinencia']) {
                mysql_query('ROLLBACK');
                return false;
            }
        }

        $sql = <<<SQL
DELETE FROM planserv_plano_feridas_doencas_sistemicas WHERE plano_feridas_id = '{$id}'
SQL;
        $rsDel = mysql_query($sql);

        foreach ($data['doencas_sistemicas'] as $doenca_sistemica) {
            $sql = <<<SQL
INSERT INTO 
  planserv_plano_feridas_doencas_sistemicas
  (
    id, 
    plano_feridas_id,
    doenca_sistemica
  ) VALUES (
    NULL, 
    '{$id}', 
    '{$doenca_sistemica}'
  )
SQL;
            $rs['doencas_sistemicas'] = mysql_query($sql);

            if(!$rs['doencas_sistemicas']) {
                mysql_query('ROLLBACK');
                return false;
            }
        }

        $sql = <<<SQL
DELETE FROM planserv_plano_feridas_locais WHERE plano_feridas_id = '{$id}'
SQL;
        $rsDel = mysql_query($sql);

        $data['locais'] = array_filter($data['locais']);
        $data['comprimento'] = array_filter($data['comprimento']);
        $data['largura'] = array_filter($data['largura']);
        $data['profundidade'] = array_filter($data['profundidade']);
        $data['descolamento'] = array_filter($data['descolamento']);
        $data['tecido_granulacao'] = array_filter($data['tecido_granulacao']);
        $data['tecido_esfacelo'] = array_filter($data['tecido_esfacelo']);
        $data['tecido_fibrina'] = array_filter($data['tecido_fibrina']);
        $data['tecido_necrotico'] = array_filter($data['tecido_necrotico']);
        $data['exposicao'] = array_filter($data['exposicao']);
        $data['tipo_exsudato'] = array_filter($data['tipo_exsudato']);
        $data['volume_exsudato'] = array_filter($data['volume_exsudato']);
        $data['odor'] = array_filter($data['odor']);
        $data['cor'] = array_filter($data['cor']);
        $data['edema'] = array_filter($data['edema']);
        $data['maceracao'] = array_filter($data['maceracao']);
        $data['bordas'] = array_filter($data['bordas']);
        $data['instrumental'] = array_filter($data['instrumental']);
        $data['cirurgico'] = array_filter($data['cirurgico']);

        foreach ($data['locais'] as $key => $local) {
            $lado = isset($data['lado'][$key]) && $data['lado'][$key] != '' ? $data['lado'][$key] : 0;
            $sql = <<<SQL
INSERT INTO 
  planserv_plano_feridas_locais
  (
    id, 
    plano_feridas_id,
    local_ferida,
    lado,
    etiologia,
    comprimento,
    largura,
    profundidade,
    descolamento,
    tecido_granulacao,
    tecido_esfacelo,
    tecido_fibrina,
    tecido_necrotico,
    exposicao,
    tipo_exsudato,
    volume_exsudato,
    odor,
    cor,
    edema,
    maceracao,
    bordas,
    instrumental,
    cirurgico
  ) VALUES (
    NULL, 
    '{$id}', 
    '{$local}', 
    '{$lado}', 
    '{$data['etiologia'][$key]}', 
    '{$data['comprimento'][$key]}', 
    '{$data['largura'][$key]}', 
    '{$data['profundidade'][$key]}',
    '{$data['descolamento'][$key]}',
    '{$data['tecido_granulacao'][$key]}',
    '{$data['tecido_esfacelo'][$key]}',
    '{$data['tecido_fibrina'][$key]}',
    '{$data['tecido_necrotico'][$key]}',
    '{$data['exposicao'][$key]}',
    '{$data['tipo_exsudato'][$key]}',
    '{$data['volume_exsudato'][$key]}',
    '{$data['odor'][$key]}',
    '{$data['cor'][$key]}',
    '{$data['edema'][$key]}',
    '{$data['maceracao'][$key]}',
    '{$data['bordas'][$key]}',
    '{$data['instrumental'][$key]}',
    '{$data['cirurgico'][$key]}'
);
SQL;
            $rs['locais'] = mysql_query($sql);

            $data['feridas'][$key] = mysql_insert_id();
        }

        if(!$rs['locais']) {
            mysql_query('ROLLBACK');
            return false;
        }

        $sql = <<<SQL
DELETE FROM planserv_plano_feridas_coberturas WHERE plano_feridas_id = '{$id}'
SQL;
        $rsDel = mysql_query($sql);

        $hasOne = false;
        $sql = <<<SQL
INSERT INTO 
  planserv_plano_feridas_coberturas
  (
    id, 
    plano_feridas_id,
    feridas_id,
    tipo_cobertura,
    catalogo_id,
    tamanho,
    quantidade,
    frequencia
  ) VALUES 
SQL;

        foreach ($data['catalogo_id']['cp'] as $key => $item) {
            if ($item != '' && $data['quantidade']['cp'][$key] > 0) {
                $hasOne = true;
                $sql .= <<<SQL
(
    NULL, 
    '{$id}', 
    '{$data['feridas'][$key]}', 
    'cp', 
    '{$item}', 
    '{$data['tamanho']['cp'][$key]}', 
    '{$data['quantidade']['cp'][$key]}', 
    '{$data['frequencia']['cp'][$key]}'
),
SQL;
            }
        }

        $sql = substr_replace($sql, ';', -1, 1);

        if($hasOne) {
            $rs['cp'] = mysql_query($sql) or die(mysql_error());
        } else {
            $rs['cp'] = true;
        }

        if (!$rs['cp']) {
            mysql_query('ROLLBACK');
            return false;
        }

        $hasOne = false;
        $sql = <<<SQL
INSERT INTO 
  planserv_plano_feridas_coberturas
  (
    id, 
    plano_feridas_id,
    feridas_id,
    tipo_cobertura,
    catalogo_id,
    tamanho,
    quantidade,
    frequencia
  ) VALUES 
SQL;

        foreach ($data['catalogo_id']['cs'] as $key => $item) {
            if ($item != '' && $data['quantidade']['cs'][$key] > 0) {
                $hasOne = true;
                $sql .= <<<SQL
(
    NULL, 
    '{$id}', 
    '{$data['feridas'][$key]}', 
    'cs', 
    '{$item}', 
    '{$data['tamanho']['cs'][$key]}', 
    '{$data['quantidade']['cs'][$key]}', 
    '{$data['frequencia']['cs'][$key]}'
),
SQL;
            }
        }

        $sql = substr_replace($sql, ';', -1, 1);

        if($hasOne) {
            $rs['cs'] = mysql_query($sql) or die(mysql_error());
        } else {
            $rs['cs'] = true;
        }

        if (!$rs['cs']) {
            mysql_query('ROLLBACK');
            return false;
        }

        return !in_array(false, $rs) ? mysql_query('COMMIT') : false;
    }
}