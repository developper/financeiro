<?php

namespace app\Models\Enfermagem;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class Alta extends AbstractModel implements DAOInterface
{
	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND ID = "' . $id . '"' : "";
		return "SELECT * FROM relatorio_alta_enf WHERE 1=1 {$id}";
	}

	public static function getByUR($filters)
	{
        $ur = $filters['empresa'];
		$inicio = implode('-',array_reverse(explode('/',$filters['inicio'])));
		$fim = implode('-',array_reverse(explode('/',$filters['fim'])));
		$condUr = " p.empresa = '{$ur}'  AND";
		if($ur == 'T'){
			$condUr = " ";
		}
		$condPlano = '';
		if(!empty($filters['plano'])){
			$condPlano = " p.convenio = '{$filters['plano']}'  AND";
		}

        $sql = <<<SQL

SELECT
r.ID,
DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as data_sistema,
			CONCAT(DATE_FORMAT(r.INICIO,'%d/%m/%Y'), ' - ', DATE_FORMAT(r.FIM,'%d/%m/%Y')) as data_pt,
			u.nome as nomeUsuario,
			p.nome as paciente,
			r.INICIO as data_us,
			r.DATA as data_system,
			'Alta' as tipoDocumento,
			p.idClientes as paciente_id
FROM
			relatorio_alta_enf as r LEFT JOIN
			usuarios as u ON (r.USUARIO_ID = u.idUsuarios) LEFT JOIN
			clientes as p ON (r.PACIENTE_ID = p.idClientes)
WHERE
		   $condPlano
		   $condUr
			(
			r.INICIO BETWEEN '{$inicio}' AND'{$fim}' OR
			r.FIM BETWEEN '{$inicio}' AND'{$fim}'
			)
ORDER BY
			data_us DESC, data_system DESC

SQL;

        return parent::get($sql, 'assoc');
    }

}