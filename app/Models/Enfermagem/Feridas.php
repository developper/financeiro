<?php
namespace app\Models\Enfermagem;

use App\Models\AbstractModel,
	\App\Models\DAOInterface;


class Feridas extends AbstractModel implements DAOInterface
{
	public static function getAll ()
	{
		return parent::get (self::getSQL ());
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		return "";
	}

	public static function getLocaisFeridas($id = null)
	{
		$sql = "SELECT * FROM quadro_feridas_enf_local WHERE ID != '6' ORDER BY LOCAL ASC";
		return parent::get ($sql, 'Assoc');
	}

    public static function getFeridasByEvolucao($evolucao)
    {
        $sql = <<<SQL
SELECT 
  quadro_feridas_enfermagem.*
FROM 
  quadro_feridas_enfermagem
WHERE 
  quadro_feridas_enfermagem.EVOLUCAO_ENF_ID = '{$evolucao}' 
ORDER BY 
  quadro_feridas_enfermagem.ID DESC 
  LIMIT 5
SQL;
        return parent::get($sql, 'assoc');

    }
}