<?php

namespace App\Models\Enfermagem;

use \App\Helpers\Storage;

class ImagemFeridaEvolucao
{

  protected $conn;

  public function __construct($conn)
  {
    $this->conn = $conn;
  }

  public function getImagens($feridaId, $relatorio = 'prorrogacao')
  {
    $tabelas = [
      'prorrogacao' => 'feridas_relatorio_prorrogacao_enf_imagem',
      'aditivo'     => 'feridas_relatorio_prorrogacao_enf_imagem',
    ];
    if (isset($tabelas[$relatorio]))
      $tabela = $tabelas[$relatorio];

    $sql = "SELECT *
            FROM {$tabela} as fi
              INNER JOIN files_uploaded ON files_uploaded.id = fi.file_uploaded_id
            WHERE fi.ferida_relatorio_prorrogacao_enf_id = %d;";
    $query = sprintf($sql, $feridaId);
    $rows = array();
    if ($result = $this->conn->query($query))
      while ($row = $result->fetch_assoc())
        $rows[] = $row;
    return $rows;
  }

  public function getFeridaImagensRelatorioHTML($feridaId)
  {
    $feridaImagens = '';
    $dataFerida = '';
    if (isset($feridaId))
      $imagens = $this->getImagens($feridaId);
    foreach ($imagens as $imagem) {
      $dataFerida = '';
      $labelData = '';
      $imagemFeridaUrl = Storage::getImageUrl($imagem['path']);
      if ($imagem['data_imagem'] != '0000-00-00'){
        $dataFerida = \DateTime::createFromFormat('Y-m-d', $imagem['data_imagem'])->format('d/m/Y');
        $labelData = "<strong>Em $dataFerida: </strong>";
      }

      $feridaImagens .= "$labelData <img style='margin-right: 5px; border: 1px solid #ccc;' width='200' src='{$imagemFeridaUrl}' />";
    }

    if (empty($feridaImagens))
      $feridaImagens = 'SEM IMAGEM';

    return $feridaImagens;
  }

  public function getFeridas($pacienteId)
  {
    $now = new \DateTime();
    $rows = $this->getEvolucoesDesc((int) $pacienteId, $now->format('m'), $now->format('Y'));
    $groupedByData= array();
    $rowsAssoc = array();
    while($row = $rows->fetch_assoc())
      $rowsAssoc[] = $row;

    $groupedByLocalLado = $this->groupBy($rowsAssoc, ['local_id', 'LADO']);
    $auxiliar = $groupedByLocalLado;

    foreach($auxiliar as $aux){

      $aux = $this->groupBy($aux, ['DATA_EVOLUCAO']);
      ksort($aux);
      $aux = $this->firstAndLast($aux);
      $groupedByData[] = $aux;
    }

    return $groupedByData;

  }

  protected function firstAndLast($data)
  {

    return array_merge(reset($data), count($data) > 1 ? end($data) : []);
  }

  protected function groupBy($rows, $cols, $separator = ':')
  {
    $grouped = array();
    foreach($rows as $row) {
      $keys = array();
      foreach($cols as $col) {
        $keys[] = $row[$col];
      }
      $grouped[implode($keys, $separator)][] = $row;
    }
    return $grouped;
  }

  protected function getEvolucoesDesc($pacienteId, $month, $year)
  {
    $sql = <<<SQL
SELECT
  quadro_feridas_enfermagem.ID,
  quadro_feridas_enfermagem.LOCAL as local_id,
  quadro_feridas_enf_local.LOCAL,
  quadro_feridas_enfermagem.LADO,
  files_uploaded.id as file_uploaded_id,
  files_uploaded.full_path,
  files_uploaded.path,
  evolucaoenfermagem.ID as evolucaoId,
  evolucaoenfermagem.DATA_EVOLUCAO
FROM quadro_feridas_enfermagem
	INNER JOIN files_uploaded ON files_uploaded.id = quadro_feridas_enfermagem.file_uploaded_id
	INNER JOIN evolucaoenfermagem ON evolucaoenfermagem.ID = quadro_feridas_enfermagem.EVOLUCAO_ENF_ID
	INNER JOIN quadro_feridas_enf_local ON quadro_feridas_enf_local.ID = quadro_feridas_enfermagem.LOCAL
WHERE evolucaoenfermagem.PACIENTE_ID = %d
  AND MONTH(evolucaoenfermagem.DATA_EVOLUCAO) = '%d'
	AND YEAR(evolucaoenfermagem.DATA_EVOLUCAO) = '%d'
	AND quadro_feridas_enfermagem.LOCAL != -1
ORDER BY evolucaoenfermagem.DATA ASC
SQL;


    $query = sprintf($sql, $pacienteId, $month, $year);
    return $this->conn->query($query);
  }

}