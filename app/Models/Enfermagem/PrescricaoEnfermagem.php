<?php
namespace App\Models\Enfermagem;
use App\Models\AbstractModel,
  \App\Models\DAOInterface;

class PrescricaoEnfermagem  extends AbstractModel implements DAOInterface
{
  public static function getAllCuidados()
  {
       return parent::get(self::getSQLCuidados());
  }
  public static function getAllFrequecias()
  {
    return  parent::get(self::getSQLFrequencia());
  }
  public static function getSQL($id=null)
  {

  }
  public static function getPrescricoes($idPaciente,$inicio,$fim,$carater )
  {
    return parent::get(self::getSQLPrescricoes($idPaciente,$inicio,$fim,$carater ));

  }
  public static function getCaraterPrescricaoEnfermagem()
  {
    return parent::get(self::getSqlCaraterPrescricao());

  }
  public static  function getItensPrescricao($idPrescEnf)
  {
    return parent::get(self::getSQLItensPrescricoes($idPrescEnf));
  }
  public static function getSQLFrequencia($id=null)
  {
    return "SELECT
            frequencia.id,
            frequencia.frequencia,
            frequencia.qtd
            FROM
            frequencia";
  }
  public static function getSQLCuidados($id=null)
  {
    return "SELECT
              prescricao_enf_cuidado.id,
              prescricao_enf_cuidado.item,
              prescricao_enf_cuidado.presc_enf_grupo_id,
              prescricao_enf_grupo.grupo
              FROM
              prescricao_enf_cuidado
              INNER JOIN prescricao_enf_grupo ON prescricao_enf_cuidado.presc_enf_grupo_id = prescricao_enf_grupo.id
              WHERE
              1=1
              ORDER BY
              prescricao_enf_cuidado.presc_enf_grupo_id ASC,
              prescricao_enf_cuidado.item ASC";
  }
    public static function getSQLCuidadosAtivos($id=null)
    {
        $sql= "SELECT
              prescricao_enf_cuidado.id,
              prescricao_enf_cuidado.item,
              prescricao_enf_cuidado.presc_enf_grupo_id,
              prescricao_enf_grupo.grupo
              FROM
              prescricao_enf_cuidado
              INNER JOIN prescricao_enf_grupo ON prescricao_enf_cuidado.presc_enf_grupo_id = prescricao_enf_grupo.id
              WHERE
              prescricao_enf_cuidado.ativo = 1
              ORDER BY
              prescricao_enf_cuidado.presc_enf_grupo_id ASC,
              prescricao_enf_cuidado.item ASC";
        return parent::get($sql);
    }

  public static function setSalvar($idPaciente,$inicio,$fim,$dados,$idUser,$carater)
  {
     mysql_query('begin');

     $sql = "SELECT empresa, convenio FROM clientes WHERE idClientes = '{$idPaciente}'";
     $rs = mysql_query($sql);
     $rowPresc = mysql_fetch_array($rs);
     $empresa = $rowPresc['empresa'];
     $plano = $rowPresc['convenio'];

        $sql =" INSERT INTO
                 prescricao_enf
                 (cliente_id,
                 empresa,
                 plano,
                  user_id,
                  `data`,
                  inicio,
                    fim,
                    carater)
                  values
                  ('{$idPaciente}',
                  '{$empresa}',
                  '{$plano}',
                   '{$idUser}',
                   now(),
                   '{$inicio}',
                   '{$fim}',
                   '{$carater}'
                  ) ";
          if(!mysql_query($sql)){
            echo "0";
            mysql_query('roolback');
            return;
          }
          $idPrescricao = mysql_insert_id();
          $itens = explode('$',$dados);
     foreach($itens as $item){
       if ($item != "" && $item != null) {

            $i= explode('#',$item);
          $inicioItem = $i[4];
          $fimItem  =  $i[5];
          $idCuidado = $i[0];
          $idFrequencia = $i[2];
          $inicioObs = \DateTime::createFromFormat('Y-m-d',$inicioItem);
          $fimObs = \DateTime::createFromFormat('Y-m-d',$fimItem);
          $inicioObs= $inicioObs->format('d/m/Y');
           $fimObs = $fimObs->format('d/m/Y');
         $obs = $i[1].' Frequência: '.$i[3].' Período de '.$inicioObs.' até '.$fimObs;
         $aprazamento = $i[6];
        $sqlItens ="Insert
                    into
                    prescricao_enf_itens_prescritos
                    (prescricao_enf_id,
                    inicio,
                    fim,
                    frequencia_id,
                    presc_enf_cuidados_id,
                    aprazamento,
                    obs)
                    values (
                    '{$idPrescricao}',
                    '{$inicioItem}',
                    '{$fimItem}',
                    '{$idFrequencia}',
                    '{$idCuidado}',
                    '{$aprazamento}',
                    '{$obs}'
                    )";
         if(!mysql_query($sqlItens)){
           echo "0";
           mysql_query('roolback');
           return;
         }

       }
     }

     mysql_query('commit');
    echo $idPrescricao;
    return;
   }
  public static function getSQLPrescricoes($idPaciente,$inicio,$fim,$carater )
  {
    $condPaciente ='';
    if($idPaciente > 0) {
      $condPaciente = "prescricao_enf.cliente_id = {$idPaciente} AND ";
    }
    $condCarater = $carater != -1 ? " AND prescricao_enf.carater = {$carater} ": "";

    $inicio = \DateTime::createFromFormat('d/m/Y',$inicio);
    $fim = \DateTime::createFromFormat('d/m/Y',$fim);
    $inicioBD= $inicio->format('Y-m-d');
    $fimBD = $fim->format('Y-m-d');

    $sql= "  SELECT
                prescricao_enf.id,
                clientes.nome as paciente,
                DATE_FORMAT(prescricao_enf.`data`, '%d/%m/%Y %H:%i') as dataPt,
                DATE_FORMAT(prescricao_enf.`inicio`, '%d/%m/%Y ') as inicioPt,
                DATE_FORMAT(prescricao_enf.`fim`, '%d/%m/%Y ') as fimPt,
                usuarios.nome as usuario,
                carater_prescricao.nome as caraterNome
                FROM
                prescricao_enf
                INNER JOIN clientes ON  prescricao_enf.cliente_id = clientes.idClientes
                INNER JOIN usuarios ON  prescricao_enf.user_id = usuarios.idUsuarios
                INNER JOIN carater_prescricao ON  prescricao_enf.carater = carater_prescricao.id
                WHERE
                {$condPaciente}
                prescricao_enf.`data` BETWEEN '{$inicioBD}' AND '{$fimBD}' AND
                prescricao_enf.cancelado_em IS NULL {$condCarater} 
                ORDER BY prescricao_enf.`data` desc";


    return $sql;
  }
  public static function getSQLItensPrescricoes($idPrescEnf)
  {
    $sql = "SELECT
            DATE_FORMAT(prescricao_enf.inicio, '%d/%m/%Y') as inicioPt,
            DATE_FORMAT(prescricao_enf.fim, '%d/%m/%Y') as fimPt,
            clientes.nome as paciente,
            prescricao_enf.cliente_id,
            prescricao_enf.id,
            prescricao_enf.carater,
            usuarios.nome,
            prescricao_enf_itens_prescritos.obs,
            prescricao_enf_itens_prescritos.aprazamento,
            prescricao_enf_grupo.id as idGrupo,
            prescricao_enf_grupo.grupo,
            carater_prescricao.nome as caraterNome,
            prescricao_enf_itens_prescritos.presc_enf_cuidados_id,
            prescricao_enf_itens_prescritos.frequencia_id,
            prescricao_enf_itens_prescritos.aprazamento,
            e.nome as empresa_presc,
            pl.nome as plano_presc,
            prescricao_enf.empresa as empresaRel,
            clientes.empresa as empresaCliente 
            
            FROM
            prescricao_enf
            INNER JOIN usuarios ON prescricao_enf.user_id = usuarios.idUsuarios
            INNER JOIN prescricao_enf_itens_prescritos ON prescricao_enf.id = prescricao_enf_itens_prescritos.prescricao_enf_id
            inner join prescricao_enf_cuidado on prescricao_enf_itens_prescritos.presc_enf_cuidados_id= prescricao_enf_cuidado.id
            INNER JOIN prescricao_enf_grupo ON prescricao_enf_cuidado.presc_enf_grupo_id = prescricao_enf_grupo.id
            INNER JOIN carater_prescricao ON  prescricao_enf.carater = carater_prescricao.id
            INNER JOIN clientes  ON  prescricao_enf.cliente_id = clientes.idClientes
            LEFT JOIN empresas as e on prescricao_enf.empresa = e.id
            left JOIN planosdesaude as pl on prescricao_enf.plano = pl.id

            where
            prescricao_enf.id ={$idPrescEnf}
            ORDER BY prescricao_enf_grupo.id ASC";
    return $sql;
  }

    public static function getSqlCaraterPrescricao()
    {
      $sql = "SELECT
              carater_prescricao.id,
              carater_prescricao.nome
              FROM
              carater_prescricao
              WHERE
              carater_prescricao.profissional = 'e'
              AND carater_prescricao.id != 10";

      return $sql;
    }

    public static function getByUR($filters)
    {
        $ur = $filters['empresa'];
        $inicio = implode('-',array_reverse(explode('/',$filters['inicio'])));
        $fim = implode('-',array_reverse(explode('/',$filters['fim'])));
        $condUr = " clientes.empresa = '{$ur}'  AND";
        if($ur == 'T'){
            $condUr = " ";
        }
        $condPlano = '';
        if(!empty($filters['plano'])){
            $condPlano = " clientes.convenio = '{$filters['plano']}'  AND";
        }

        $sql= "  
SELECT
    prescricao_enf.id as ID,
    DATE_FORMAT(prescricao_enf.`data`,'%d/%m/%Y %H:%i:%s') as data_sistema,
    CONCAT(DATE_FORMAT(prescricao_enf.inicio,'%d/%m/%Y'), ' - ', DATE_FORMAT(prescricao_enf.`fim`,'%d/%m/%Y')) as data_pt,
    usuarios.nome as nomeUsuario,
    clientes.nome as paciente,
    prescricao_enf.inicio as data_us,
    prescricao_enf.`data` as data_system,
    'Prescrição Enfermagem' as tipoDocumento,
    clientes.idClientes as paciente_id
FROM
    prescricao_enf
    INNER JOIN clientes ON  prescricao_enf.cliente_id = clientes.idClientes
    INNER JOIN usuarios ON  prescricao_enf.user_id = usuarios.idUsuarios
    INNER JOIN carater_prescricao ON  prescricao_enf.carater = carater_prescricao.id
WHERE
 $condPlano
 $condUr
  (
    prescricao_enf.inicio BETWEEN '{$inicio}' AND'{$fim}' OR
    prescricao_enf.fim BETWEEN '{$inicio}' AND'{$fim}'
  )
  AND prescricao_enf.cancelado_em IS NULL  
  ORDER BY 
    data_us DESC, data_system DESC";

        return parent::get($sql);
    }

    public static function getAllPrescEnfAditivaByPeriodo($begin, $end)
    {
        $sql = <<<SQL
SELECT
  prescricao_enf.id AS cod,
  prescricao_enf.cliente_id AS idPaciente,
  'presc-enf' AS tipo,
  prescricao_enf.data AS dataSistema,
  clientes.nome AS nomeOrder,
  CONCAT('(', clientes.codigo, ') ', clientes.nome) AS nomePaciente,
  usuarios.nome AS usuario,
  prescricao_enf.inicio,
  prescricao_enf.fim,
  IF(prescricao_enf.empresa = 0 OR prescricao_enf.empresa IS NULL, e2.nome, e1.nome) AS ur,
  IF(prescricao_enf.plano = 0 OR prescricao_enf.plano IS NULL, ps2.nome, ps1.nome) AS convenio
FROM
  prescricao_enf
  INNER JOIN clientes ON prescricao_enf.cliente_id = clientes.idClientes
  INNER JOIN usuarios ON prescricao_enf.user_id = usuarios.idUsuarios
  LEFT JOIN empresas AS e1 ON prescricao_enf.empresa = e1.id
  LEFT JOIN planosdesaude AS ps1 ON prescricao_enf.plano = ps1.id
  LEFT JOIN empresas AS e2 ON clientes.empresa = e2.id
  LEFT JOIN planosdesaude AS ps2 ON clientes.convenio = ps2.id
WHERE
  (prescricao_enf.inicio BETWEEN '{$begin}' AND '{$end}'
  OR prescricao_enf.fim BETWEEN '{$begin}' AND '{$end}')
  AND prescricao_enf.Carater = 8
  AND clientes.status = 4
  AND prescricao_enf.cancelado_por IS NULL
  AND cliente_id NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
ORDER BY 
  dataSistema, nomeOrder
SQL;
        return parent::get($sql, 'Assoc');
    }
}