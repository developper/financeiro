<?php

namespace app\Models\Enfermagem;

use App\Models\AbstractModel;
use \App\Models\DAOInterface;
use	\App\Core\Log;
use \App\Models\Logistica\Catalogo;

class PrescricaoCurativo extends AbstractModel implements DAOInterface
{
	public static function getAll()
	{
		return parent::get(self::getSQL());
	}

	public static function getAllByFilters($paciente, $inicio, $fim)
	{
		$sql = <<<SQL
SELECT
	pc.id,
	c.nome as cliente,
	u.nome as enfermeiro,
	pc.carater,
	pc.enviado,
	pc.registered_at,
	u2.nome as updated,
	pc.updated_at,
	pc.inicio,
	pc.fim
FROM
	prescricao_curativo AS pc
	INNER JOIN clientes AS c ON c.idClientes = pc.paciente
	INNER JOIN usuarios AS u ON u.idUsuarios = pc.profissional
	LEFT JOIN usuarios AS u2 ON u2.idUsuarios = pc.updated_by
WHERE
	pc.paciente = '{$paciente}'
	AND pc.registered_at BETWEEN '{$inicio} 00:00:01' AND '{$fim} 23:59:59'
	AND pc.cancelado = 0
ORDER BY
	pc.registered_at DESC
SQL;
		return parent::get($sql);
	}

	public static function getLastPrescricaoByPacienteIdAndPeriodo($paciente, $inicio, $fim)
	{
		$sql = <<<SQL
SELECT
	pc.id
FROM
	prescricao_curativo AS pc
WHERE
	pc.paciente = '{$paciente}'
	AND ( pc.inicio BETWEEN '{$inicio}' AND '{$fim}'
	OR pc.fim BETWEEN '{$inicio}' AND '{$fim}')
	AND pc.cancelado = 0
ORDER BY
	pc.registered_at DESC
SQL;
		return parent::get($sql);
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id), 'Assoc'));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND prescricao_curativo.id = "' . $id . '"' : "";
		return "SELECT 
                  prescricao_curativo.*, 
                  empresas.nome AS empresa_rel, 
                  planosdesaude.nome AS plano_rel,
                  clientes.empresa as empresaCliente
                FROM 
                  prescricao_curativo 
                  inner join clientes on prescricao_curativo.paciente = clientes.idClientes
                  LEFT JOIN empresas ON prescricao_curativo.empresa = empresas.id 
                  LEFT JOIN planosdesaude ON prescricao_curativo.plano = planosdesaude.id 
                WHERE 
                  1=1 
                  {$id} 
                ORDER BY 
                  registered_at";
	}

	public static function save($data, $paciente, $profissional, $inicio, $fim, $carater)
	{
		mysql_query("begin");
        $sql = "SELECT empresa, convenio FROM clientes WHERE idClientes = '{$paciente}'";
        $rs = mysql_query($sql);
        $rowPresc = mysql_fetch_array($rs);
        $empresa = $rowPresc['empresa'];
        $plano = $rowPresc['convenio'];

		$sql 	= "INSERT INTO prescricao_curativo
						 VALUES
						 (
							 '',
							 '{$paciente}',
							 '{$empresa}',
							 '{$plano}',
							 '{$profissional}',
							 '{$carater}',
							 '{$inicio}',
							 '{$fim}',
							 '0000-00-00 00:00:00',
							 0,
							 0,
							 0,
							 '0000-00-00 00:00:00',
							 0,
							 NOW(),
							 '0000-00-00 00:00:00',
							 0,
							 'N'
						 )";
		$rs		= mysql_query($sql);
		if($rs) {
			$prescricao = mysql_insert_id ();
			Log::registerUserLog($sql);
		}else{
			$erro = mysql_error();
			mysql_query("rollback");
			throw new \Exception("Erro ao cadastrar uma prescrição de curativo: " . $erro, $code = 1);
		}

		foreach($data as $local => $dados){
			$local_ferida = explode('$', $local);

			$sql 	= "INSERT INTO prescricao_curativo_ferida VALUES ('', '{$prescricao}', '{$local_ferida[1]}', '{$dados['lado']}','{$dados['justificativa']}')";

			$rs		= mysql_query($sql);
			if($rs) {
				$ferida = mysql_insert_id ();
				Log::registerUserLog($sql);
			}else{
				$erro = mysql_error();
				mysql_query("rollback");
				throw new \Exception("Erro ao cadastrar uma ferida: " . $erro, $code = 2);
			}

			//echo '<pre>'; die(print_r($dados['cp_itens']));
			foreach($dados['cp_itens'] as $itens){
				$aprazamentos = rtrim($itens['aprazamento']);
				$sql = <<<SQL
INSERT INTO
	prescricao_curativo_ferida_item
VALUES
(
	'',
	'{$prescricao}',
	'{$ferida}',
	'{$itens['catalogo_id']}',
	'{$itens['qtd']}',
	'{$itens['frequencia']}',
	'{$itens['inicio']}',
	'{$itens['fim']}',
	'{$itens['obs']}',
	'p',
	'{$aprazamentos}'
)
SQL;
				$rs	= mysql_query($sql);
				if($rs){
					Log::registerUserLog($sql);
				} else {
					$erro = mysql_error();
					mysql_query("rollback");
					throw new \Exception("Erro ao cadastrar um item de uma ferida da cobertura primária: " . $erro, $code = 3);
				}
			}

			foreach($dados['cs_itens'] as $itens){
				$aprazamentos = rtrim($itens['aprazamento']);
				$sql = <<<SQL
INSERT INTO
	prescricao_curativo_ferida_item
VALUES
(
	'',
	'{$prescricao}',
	'{$ferida}',
	'{$itens['catalogo_id']}',
	'{$itens['qtd']}',
	'{$itens['frequencia']}',
	'{$itens['inicio']}',
	'{$itens['fim']}',
	'{$itens['obs']}',
	's',
	'{$aprazamentos}'
)
SQL;
				$rs	= mysql_query($sql);
				if($rs){
					Log::registerUserLog($sql);
				} else {
					$erro = mysql_error();
					mysql_query("rollback");
					throw new \Exception("Erro ao cadastrar um item de uma ferida da cobertura secundária: " . $erro, $code = 3);
				}
			}
		}
		if(mysql_query("commit")){
			return $prescricao;
		}
	}

	public static function update($data, $prescricao, $profissional, $inicio, $fim, $carater)
	{
		mysql_query("begin");

		$sql 	= "UPDATE prescricao_curativo
						 SET
						 	inicio = '{$inicio}',
						 	fim = '{$fim}',
						 	carater = '{$carater}',
						 	primeira_prescricao = 0,
						 	data_max_entrega = '0000-00-00',
						 	updated_at = NOW(),
						 	updated_by = '{$profissional}',
						 	entrega_imediata = 'N'
						 WHERE id = '{$prescricao}'";
		$rs		= mysql_query($sql);
		if($rs) {
			Log::registerUserLog($sql);
		}else{
			$erro = mysql_error();
			mysql_query("rollback");
			throw new \Exception("Erro ao atualizar a prescrição de curativo: " . $erro, $code = 1);
		}

		$sql 	= "DELETE FROM prescricao_curativo_ferida WHERE prescricao = '{$prescricao}'";
		$rs1	= mysql_query($sql);

		$sql 	= "DELETE FROM prescricao_curativo_ferida_item WHERE prescricao = '{$prescricao}'";
		$rs2	= mysql_query($sql);
		if($rs1 && $rs2) {
			Log::registerUserLog($sql);

			foreach($data as $local => $dados){
				$local_ferida = explode('$', $local);
				$sql 	= "INSERT INTO prescricao_curativo_ferida VALUES ('', '{$prescricao}', '{$local_ferida[1]}', '{$dados['lado']}','{$dados['justificativa']}')";
				$rs		= mysql_query($sql);
				if($rs) {
					$ferida = mysql_insert_id ();
					Log::registerUserLog($sql);
				}else{
					$erro = mysql_error();
					mysql_query("rollback");
					throw new \Exception("Erro ao cadastrar uma ferida: " . $erro, $code = 2);
				}

				foreach($dados['cp_itens'] as $itens){
					$aprazamentos = rtrim($itens['aprazamento']);
					$sql = <<<SQL
INSERT INTO
	prescricao_curativo_ferida_item
VALUES
(
	'',
	'{$prescricao}',
	'{$ferida}',
	'{$itens['catalogo_id']}',
	'{$itens['qtd']}',
	'{$itens['frequencia']}',
	'{$itens['inicio']}',
	'{$itens['fim']}',
	'{$itens['obs']}',
	'p',
	'{$aprazamentos}'
)
SQL;
					$rs	= mysql_query($sql);
					if($rs){
						Log::registerUserLog($sql);
					} else {
						$erro = mysql_error();
						mysql_query("rollback");
						throw new \Exception("Erro ao cadastrar um item de uma ferida da cobertura primária: " . $erro, $code = 3);
					}
				}

				foreach($dados['cs_itens'] as $itens){
					$aprazamentos = rtrim($itens['aprazamento']);
					$sql = <<<SQL
INSERT INTO
	prescricao_curativo_ferida_item
VALUES
(
	'',
	'{$prescricao}',
	'{$ferida}',
	'{$itens['catalogo_id']}',
	'{$itens['qtd']}',
	'{$itens['frequencia']}',
	'{$itens['inicio']}',
	'{$itens['fim']}',
	'{$itens['obs']}',
	's',
	'{$aprazamentos}'
)
SQL;
					$rs	= mysql_query($sql);
					if($rs){
						Log::registerUserLog($sql);
					} else {
						$erro = mysql_error();
						mysql_query("rollback");
						throw new \Exception("Erro ao cadastrar um item de uma ferida da cobertura secundária: " . $erro, $code = 3);
					}
				}
			}
		}else{
			$erro = mysql_error();
			mysql_query("rollback");
			throw new \Exception("Erro ao atualizar a prescrição de curativo: " . $erro, $code = 1);
		}


		if(mysql_query("commit")){
			return $prescricao;
		}
	}

	public static function getFeridasByPrescricaoId($prescricao)
	{
		$sql = "SELECT * FROM prescricao_curativo_ferida WHERE prescricao = '{$prescricao}'";
		return parent::get($sql, 'Assoc');
	}

	public static function getCoberturasByFeridaId($ferida, $cobertura)
	{

		$sql = <<<SQL
SELECT
  CONCAT
  (
  	catalogo.principio,
  	' ',
  	catalogo.apresentacao
  ) as item_name,
  prescricao_curativo_ferida_item.catalogo_id,
  prescricao_curativo_ferida_item.qtd,
  prescricao_curativo_ferida_item.frequencia,
  frequencia.frequencia as frequencia_descricao,
  prescricao_curativo_ferida_item.inicio,
  prescricao_curativo_ferida_item.fim,
  prescricao_curativo_ferida_item.obs,
  prescricao_curativo_ferida_item.cobertura,
  prescricao_curativo_ferida_item.aprazamento
FROM
  prescricao_curativo_ferida_item
INNER JOIN catalogo ON prescricao_curativo_ferida_item.catalogo_id = catalogo.ID
INNER JOIN frequencia ON prescricao_curativo_ferida_item.frequencia = frequencia.id
WHERE
  prescricao_curativo_ferida_item.ferida = '{$ferida}'
  AND prescricao_curativo_ferida_item.cobertura = '{$cobertura}'
SQL;
		return parent::get($sql, 'Assoc');
	}

	public static function getCondutaAtualByFeridaId($ferida, $cobertura, $paciente)
	{
		$sql = <<<SQL
SELECT
  CONCAT
  (
  	catalogo.principio,
  	' ',
  	catalogo.apresentacao
  ) as item_name,
  prescricao_curativo_ferida_item.catalogo_id,
  prescricao_curativo_ferida_item.qtd,
  prescricao_curativo_ferida_item.inicio,
  prescricao_curativo_ferida_item.fim,
  prescricao_curativo_ferida_item.obs,
  prescricao_curativo_ferida_item.cobertura,
  prescricao_curativo_ferida_item.aprazamento
FROM
  prescricao_curativo_ferida_item
INNER JOIN
  catalogo
    ON prescricao_curativo_ferida_item.catalogo_id = catalogo.ID
WHERE
  prescricao_curativo_ferida_item.ferida = '{$ferida}'
  AND prescricao_curativo_ferida_item.cobertura = '{$cobertura}'
  AND prescricao_curativo_ferida_item.prescricao = (
  	SELECT
  		id
  	FROM
  		prescricao_curativo
  	WHERE
  		paciente = '{$paciente}'
  	ORDER BY
			id DESC LIMIT 1, 1
  )
SQL;
		return parent::get($sql, 'Assoc');
	}



	public static function cancel($prescricao, $profissional)
	{
		mysql_query("begin");

		$sql = "UPDATE solicitacoes SET DATA_CANCELADO = NOW(), CANCELADO_POR = '{$profissional}' WHERE id_prescricao_curativo = '{$prescricao}'";
		$rs	 = mysql_query($sql);

		if($rs) {
			Log::registerUserLog ($sql);
			$sql = "UPDATE prescricao_curativo SET cancelado = '1', cancelado_em = NOW(), cancelado_por = '{$profissional}' WHERE id = '{$prescricao}'";
			$rs = mysql_query ($sql);
			if(!$rs){
				$erro = mysql_error();
				mysql_query("rollback");
				throw new \Exception("Erro ao enviar uma prescrição: " . $erro, $code = 2);
			}
			return mysql_query("commit");
		}
		$erro = mysql_error();
		mysql_query("rollback");
		throw new \Exception("Erro ao excluir as solicitações: " . $erro, $code = 1);
	}

	public static function getByUR($filters){
		$ur = $filters['empresa'];
		$inicio = implode('-',array_reverse(explode('/',$filters['inicio'])));
		$fim = implode('-',array_reverse(explode('/',$filters['fim'])));
		$condUr = " c.empresa = '{$ur}'  AND";
		if($ur == 'T'){
			$condUr = " ";
		}
		$condPlano = '';
		if(!empty($filters['plano'])){
			$condPlano = " c.convenio = '{$filters['plano']}'  AND";
		}
		$sql = <<<SQL
SELECT
pc.id as ID,
DATE_FORMAT(pc.registered_at,'%d/%m/%Y %H:%i:%s') as data_sistema,
CONCAT(DATE_FORMAT(pc.inicio,'%d/%m/%Y'), ' - ', DATE_FORMAT(pc.fim,'%d/%m/%Y')) as data_pt,
u.nome as nomeUsuario,
c.nome as paciente,
pc.inicio as data_us,
pc.registered_at as data_system,
'Prescrição Curativo' as tipoDocumento,
c.idClientes as paciente_id
FROM
	prescricao_curativo AS pc
	INNER JOIN clientes AS c ON c.idClientes = pc.paciente
	INNER JOIN usuarios AS u ON u.idUsuarios = pc.profissional
WHERE
$condUr
$condPlano
(
pc.inicio BETWEEN '{$inicio}' AND'{$fim}' OR
pc.fim BETWEEN '{$inicio}' AND'{$fim}'
)
AND pc.cancelado = 0
ORDER BY
	data_us DESC, data_system DESC
SQL;

		return parent::get($sql);

	}

    public static function getAllPrescCurativoAditivaByPeriodo($begin, $end)
    {
        $sql = <<<SQL
SELECT
  prescricao_curativo.id AS cod,
  prescricao_curativo.paciente AS idPaciente,
  'presc-curativo' AS tipo,
  prescricao_curativo.registered_at AS dataSistema,
  clientes.nome AS nomeOrder,
  CONCAT('(', clientes.codigo, ') ', clientes.nome) AS nomePaciente,
  usuarios.nome AS usuario,
  prescricao_curativo.inicio,
  prescricao_curativo.fim,
  IF(prescricao_curativo.empresa = 0 OR prescricao_curativo.empresa IS NULL, e2.nome, e1.nome) AS ur,
  IF(prescricao_curativo.plano = 0 OR prescricao_curativo.plano IS NULL, ps2.nome, ps1.nome) AS convenio
FROM
  prescricao_curativo
  INNER JOIN clientes ON prescricao_curativo.paciente = clientes.idClientes
  INNER JOIN usuarios ON prescricao_curativo.profissional = usuarios.idUsuarios
  LEFT JOIN empresas AS e1 ON prescricao_curativo.empresa = e1.id
  LEFT JOIN planosdesaude AS ps1 ON prescricao_curativo.plano = ps1.id
  LEFT JOIN empresas AS e2 ON clientes.empresa = e2.id
  LEFT JOIN planosdesaude AS ps2 ON clientes.convenio = ps2.id
WHERE
  (prescricao_curativo.inicio BETWEEN '{$begin}' AND '{$end}'
  OR prescricao_curativo.fim BETWEEN '{$begin}' AND '{$end}')
  AND prescricao_curativo.Carater = 8
  AND clientes.status = 4
  AND prescricao_curativo.cancelado_por = 0
  AND paciente NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
ORDER BY 
  dataSistema, nomeOrder
SQL;
        return parent::get($sql, 'Assoc');
    }
}
