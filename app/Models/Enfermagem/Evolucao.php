<?php

namespace app\Models\Enfermagem;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class Evolucao extends AbstractModel implements DAOInterface
{
	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : "";
		return "SELECT * FROM evolucaoenfermagem WHERE 1=1 {$id}";
	}

	public static function getRelatorioEvolucoes($filters)
	{
        $compUr = $filters['ur'] != '' ? " AND clientes.empresa = '{$filters['ur']}'" : "";

        $sql = <<<SQL
SELECT
	evolucaoenfermagem.ID,
	usuarios.nome AS enfermeiro,
	clientes.nome AS paciente,
	empresas.nome AS empresa,
	evolucaoenfermagem.DATA_EVOLUCAO AS dataVisita,
	evolucaoenfermagem.DATA as dataSistema
FROM
	evolucaoenfermagem
	INNER JOIN clientes ON evolucaoenfermagem.PACIENTE_ID = clientes.idClientes
	INNER JOIN empresas ON clientes.empresa = empresas.id
	INNER JOIN usuarios ON evolucaoenfermagem.USUARIO_ID = usuarios.idUsuarios
WHERE
	evolucaoenfermagem.DATA BETWEEN '{$filters['inicio']} 00:00:01' AND '{$filters['fim']} 23:59:59'
	{$compUr}
ORDER BY
  paciente, dataSistema
SQL;
        return parent::get($sql, 'assoc');
    }

	public static function getByUR($filters)
	{
		$ur = $filters['empresa'];
		$inicio = implode('-',array_reverse(explode('/',$filters['inicio'])));
		$fim = implode('-',array_reverse(explode('/',$filters['fim'])));
		$condUr = " clientes.empresa = '{$ur}'  AND";
		if($ur == 'T'){
			$condUr = " ";
		}
		$condPlano = '';
		if(!empty($filters['plano'])){
			$condPlano = " clientes.convenio = '{$filters['plano']}'  AND";
		}

		$sql = <<<SQL
SELECT
	evolucaoenfermagem.ID,
	DATE_FORMAT(evolucaoenfermagem.DATA_EVOLUCAO, '%d/%m/%Y') as data_pt,
    DATE_FORMAT(evolucaoenfermagem.DATA, '%d/%m/%Y %H:%i:%s') as data_sistema,
	usuarios.nome AS nomeUsuario,
	clientes.nome AS paciente,
	evolucaoenfermagem.DATA_EVOLUCAO as data_us,
	evolucaoenfermagem.DATA as data_system,
	'Evolução' as tipoDocumento,
	clientes.idClientes as paciente_id
FROM
	evolucaoenfermagem
	INNER JOIN clientes ON evolucaoenfermagem.PACIENTE_ID = clientes.idClientes
	INNER JOIN empresas ON clientes.empresa = empresas.id
	INNER JOIN usuarios ON evolucaoenfermagem.USUARIO_ID = usuarios.idUsuarios
WHERE
$condPlano
$condUr
	evolucaoenfermagem.DATA BETWEEN '{$inicio} 00:00:01' AND '{$fim} 23:59:59'
ORDER BY
  data_us DESC, data_system DESC
SQL;

		return parent::get($sql, 'assoc');
	}

    public static function getUltimoRelatorioPaciente($paciente)
    {
        $sql = <<<SQL
SELECT 
  ID,
  USUARIO_ID,
  usuarios.nome AS enfermeiro,
  usuarios.conselho_regional,
  DATA_EVOLUCAO,
  PAD
FROM 
  evolucaoenfermagem 
  INNER JOIN usuarios ON evolucaoenfermagem.USUARIO_ID = usuarios.idUsuarios
WHERE 
  PACIENTE_ID = '{$paciente}' 
ORDER BY 
  id DESC 
  LIMIT 1
SQL;
        return current(parent::get($sql, 'assoc'));

    }
}
