<?php

namespace app\Models\Enfermagem;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class Prorrogacao extends AbstractModel implements DAOInterface
{
	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND ID = "' . $id . '"' : "";
		return "SELECT * FROM relatorio_prorrogacao_enf WHERE 1=1 {$id}";
	}

	public static function getByUR($filters)
	{
        $ur = $filters['empresa'];
		$inicio = implode('-',array_reverse(explode('/',$filters['inicio'])));
		$fim = implode('-',array_reverse(explode('/',$filters['fim'])));

        $condUr = " p.empresa = '{$ur}'  AND";
        if($ur == 'T'){
            $condUr = " ";
        }
        $condPlano = '';
        if(!empty($filters['plano'])){
            $condPlano = " p.convenio = '{$filters['plano']}'  AND";
        }

        $sql = <<<SQL
        SELECT
        			r.ID,
                    DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as data_sistema,
                    CONCAT(DATE_FORMAT(r.INICIO,'%d/%m/%Y'), ' - ', DATE_FORMAT(r.FIM,'%d/%m/%Y')) as data_pt,
                    u.nome as nomeUsuario,
                    p.nome as paciente,
                    r.INICIO as data_us,
                    r.DATA as data_system,
                    'Prorrogação' as tipoDocumento,
                    p.idClientes as paciente_id
		FROM
                    relatorio_prorrogacao_enf as r LEFT JOIN
                    usuarios as u ON (r.USUARIO_ID = u.idUsuarios) LEFT JOIN
                    clientes as p ON (r.PACIENTE_ID = p.idClientes)
		WHERE
                   $condUr
                   $condPlano
                    (
                    r.INICIO BETWEEN '{$inicio}' AND'{$fim}' OR
                    r.FIM BETWEEN '{$inicio}' AND'{$fim}'
                    )
		ORDER BY
                    data_us DESC, data_system DESC

SQL;

        return parent::get($sql, 'assoc');
    }

    public static function getUltimoRelatorioPacienteByPeriodo($data)
    {
     
        $response = [];
        $sql = <<<SQL
SELECT 
  ID AS id,
  EVOLUCAO_CLINICA AS quadro_clinico,
  DATE_FORMAT(DATA, '%Y-%m-%d') AS data_prorrogacao,
  FERIDAS,
  usuarios.nome AS enfermeiro,
  usuarios.conselho_regional,
  DATA AS created_at
FROM 
  relatorio_prorrogacao_enf
  INNER JOIN usuarios ON relatorio_prorrogacao_enf.USUARIO_ID = usuarios.idUsuarios 
WHERE 
  PACIENTE_ID = '{$data['paciente']}'
  AND (
    INICIO BETWEEN '{$data['inicio']}' AND '{$data['fim']}'
    OR FIM BETWEEN '{$data['inicio']}' AND '{$data['fim']}'
  ) 
ORDER BY 
  ID DESC LIMIT 1
SQL;
        $response['prorrogacao'] = current(parent::get($sql, 'assoc'));

        if($response['prorrogacao']['FERIDAS'] == '1') {
            $sql = <<<SQL
SELECT
feridas_relatorio_prorrogacao_enf.ID as ID_FERIDA,
  UPPER(LOCAL) AS LOCAL,
  CARACTERISTICA,
  TIPO_CURATIVO,
  PRESCRICAO_PROX_PERIODO
FROM
  feridas_relatorio_prorrogacao_enf
WHERE
  RELATORIO_PRORROGACAO_ENF_ID = '{$response['prorrogacao']['id']}'
  AND EDITADA_EM IS NULL
  AND DATA_DESATIVADA IS NULL
SQL;
            $response['prorrogacao']['feridas'] = parent::get($sql, 'assoc');
        
          $sql = <<<SQL
SELECT
LOCAL,
ferida_relatorio_prorrogacao_enf_id as ferida_id,
file_uploaded_id AS imagem_id,
path,
full_path
FROM
feridas_relatorio_prorrogacao_enf
LEFT JOIN feridas_relatorio_prorrogacao_enf_imagem ON feridas_relatorio_prorrogacao_enf.ID = feridas_relatorio_prorrogacao_enf_imagem.ferida_relatorio_prorrogacao_enf_id
LEFT JOIN files_uploaded ON feridas_relatorio_prorrogacao_enf_imagem.file_uploaded_id = files_uploaded.id
WHERE
  RELATORIO_PRORROGACAO_ENF_ID = '{$response['prorrogacao']['id']}'
  AND EDITADA_EM IS NULL
  AND DATA_DESATIVADA IS NULL
SQL;
          $response['prorrogacao']['feridas_imagem'] = parent::get($sql, 'assoc');
      }
        return $response['prorrogacao'];
    }

}