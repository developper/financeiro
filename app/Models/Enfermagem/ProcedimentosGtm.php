<?php

namespace app\Models\Enfermagem;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class ProcedimentosGtm extends AbstractModel implements DAOInterface
{
	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

    public static function getByIdPrescricao($id)
    {
        $sql = <<<SQL
SELECT 
  procedimentos_gtm.*,
  prescricoes.criador,
  prescricoes.paciente,
  prescricoes.data,
  CONCAT(catalogo.principio, ' ', catalogo.apresentacao) AS item
FROM 
  procedimentos_gtm 
  INNER JOIN prescricoes ON procedimentos_gtm.prescricao_id = prescricoes.id
  INNER JOIN catalogo ON catalogo.ID = procedimentos_gtm.catalogo_id
WHERE 
  procedimentos_gtm.prescricao_id = '{$id}'
SQL;
        return parent::get($sql, 'assoc');
    }

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? "AND procedimentos_gtm.id = '{$id}'" : "";
		return <<<SQL
SELECT 
  procedimentos_gtm.*,
  prescricoes.paciente,
  prescricoes.data,
  CONCAT(catalogo.principio, ' ', catalogo.apresentacao) AS item
FROM 
  procedimentos_gtm 
  INNER JOIN prescricoes ON procedimentos_gtm.prescricao_id = prescricoes.id
  INNER JOIN catalogo ON catalogo.ID = procedimentos_gtm.catalogo_id
WHERE 
  1=1 
  {$id}
SQL;
	}

    public static function getProcedimentosGtmByPacienteId($id)
    {
        $sql = <<<SQL
SELECT 
  procedimentos_gtm.prescricao_id,
  usuarios.nome AS usuario,
  usuarios.tipo AS utipo,
  usuarios.conselho_regional,
  prescricoes.data AS dataSistema,
  procedimentos_gtm.data_troca AS dataTroca
FROM 
  procedimentos_gtm 
  INNER JOIN prescricoes ON procedimentos_gtm.prescricao_id = prescricoes.id
  INNER JOIN clientes ON prescricoes.paciente = clientes.idClientes
  INNER JOIN usuarios ON prescricoes.criador = usuarios.idUsuarios
WHERE 
  prescricoes.paciente = '{$id}'
GROUP BY prescricao_id
ORDER BY prescricoes.data DESC
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function updateProcedimentosGtm($data)
    {
        mysql_query("BEGIN");
        foreach ($data as $proc) {
            $sql = <<<SQL
UPDATE procedimentos_gtm SET
data_troca = '{$proc['data_troca']}',
tipo = '{$proc['tipo']}',
furo = '{$proc['furo']}',
cm = '{$proc['cm']}',
fr = '{$proc['fr']}',
marca = '{$proc['marca']}',
dano = '{$proc['dano']}',
origem_dano = '{$proc['origem_dano']}',
descricao_origem_dano = '{$proc['descricao_origem_dano']}'
WHERE
 id = '{$proc['id']}'
SQL;
            $rs = mysql_query($sql);
            if(!$rs){
                mysql_query('ROLLBACK');
                return false;
            }
        }
        return mysql_query("COMMIT");
    }
}
