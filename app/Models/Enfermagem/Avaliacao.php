<?php

namespace app\Models\Enfermagem;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class Avaliacao extends AbstractModel implements DAOInterface
{
	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND ID = "' . $id . '"' : "";
		return "SELECT * FROM avaliacaoenfermagem WHERE 1=1 {$id}";
	}

	public static function getByUR($filters)
	{
        $ur = $filters['empresa'];
		$inicio = implode('-',array_reverse(explode('/',$filters['inicio'])));
		$fim = implode('-',array_reverse(explode('/',$filters['fim'])));
		$condUr = "clientes.empresa = '{$ur}'  AND";
		if($ur == 'T'){
			$condUr = " ";
		}
		$condPlano = '';

		if(!empty($filters['plano'])){
			$condPlano = " clientes.convenio = '{$filters['plano']}'  AND";
		}

        $sql = <<<SQL
SELECT
avaliacaoenfermagem.ID,
DATE_FORMAT(avaliacaoenfermagem.`DATA`, '%d/%m/%Y') as data_pt,
DATE_FORMAT(avaliacaoenfermagem.`DATA`, '%d/%m/%Y %H:%i:%s') as data_sistema,
usuarios.nome as nomeUsuario,
clientes.nome as paciente,
avaliacaoenfermagem.`DATA` as data_us,
'Avaliação' as tipoDocumento,
clientes.idClientes as paciente_id
FROM
avaliacaoenfermagem INNER JOIN
clientes on avaliacaoenfermagem.PACIENTE_ID = clientes.idClientes INNER JOIN
usuarios on avaliacaoenfermagem.USUARIO_ID = usuarios.idUsuarios
WHERE
$condUr
$condPlano
avaliacaoenfermagem.`DATA`  BETWEEN '{$inicio}' AND'{$fim}'
ORDER BY data_us DESC, ID DESC
SQL;


        return parent::get($sql, 'assoc');
    }

}