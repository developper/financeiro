<?php

namespace app\Models\Enfermagem;

use App\Models\AbstractModel,
	App\Models\DAOInterface;

class Aditivo extends AbstractModel implements DAOInterface
{
	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND ID = "' . $id . '"' : "";
		return "SELECT * FROM relatorio_aditivo_enf WHERE 1=1 {$id}";
	}

	public static function getByUR($filters)
	{
        $ur = $filters['empresa'];
		$inicio = implode('-',array_reverse(explode('/',$filters['inicio'])));
		$fim = implode('-',array_reverse(explode('/',$filters['fim'])));
		$condUr = " p.empresa = '{$ur}'  AND";
		if($ur == 'T'){
			$condUr = " ";
		}
		$condPlano = '';
		if(!empty($filters['plano'])){
			$condPlano = " p.convenio = '{$filters['plano']}'  AND";
		}

        $sql = <<<SQL

SELECT
r.ID,
DATE_FORMAT(r.DATA,'%d/%m/%Y %H:%i:%s') as data_sistema,
			CONCAT(DATE_FORMAT(r.INICIO,'%d/%m/%Y'), ' - ', DATE_FORMAT(r.FIM,'%d/%m/%Y')) as data_pt,
			u.nome as nomeUsuario,
			p.nome as paciente,
			r.INICIO as data_us,
			r.DATA as data_system,
			'Aditivo' as tipoDocumento,
			p.idClientes as paciente_id
FROM
			relatorio_aditivo_enf as r LEFT JOIN
			usuarios as u ON (r.USUARIO_ID = u.idUsuarios) LEFT JOIN
			clientes as p ON (r.PACIENTE_ID = p.idClientes)
WHERE
		   $condUr
		   $condPlano
			(
			r.INICIO BETWEEN '{$inicio}' AND'{$fim}' OR
			r.FIM BETWEEN '{$inicio}' AND'{$fim}'
			)
ORDER BY
			data_us DESC, data_system DESC

SQL;

        return parent::get($sql, 'assoc');
    }

    public static function getAllByPeriodo($begin, $end)
    {
        $sql = <<<SQL
SELECT
  relatorio_aditivo_enf.ID AS cod,
  relatorio_aditivo_enf.PACIENTE_ID AS idPaciente,
  'relatorio-aditivo-enf' AS tipo,
  relatorio_aditivo_enf.DATA AS dataSistema,
  clientes.nome AS nomeOrder,
  CONCAT('(', clientes.codigo, ') ', clientes.nome) AS nomePaciente,
  usuarios.nome AS usuario,
  relatorio_aditivo_enf.INICIO AS inicio,
  relatorio_aditivo_enf.FIM AS fim,
  IF(relatorio_aditivo_enf.empresa = 0 OR relatorio_aditivo_enf.empresa IS NULL, e2.nome, e1.nome) AS ur,
  IF(relatorio_aditivo_enf.plano = 0 OR relatorio_aditivo_enf.plano IS NULL, ps2.nome, ps1.nome) AS convenio
FROM
  relatorio_aditivo_enf
  INNER JOIN clientes ON relatorio_aditivo_enf.PACIENTE_ID = clientes.idClientes
  INNER JOIN usuarios ON relatorio_aditivo_enf.USUARIO_ID = usuarios.idUsuarios
  LEFT JOIN empresas AS e1 ON relatorio_aditivo_enf.empresa = e1.id
  LEFT JOIN planosdesaude AS ps1 ON relatorio_aditivo_enf.plano = ps1.id
  LEFT JOIN empresas AS e2 ON clientes.empresa = e2.id
  LEFT JOIN planosdesaude AS ps2 ON clientes.convenio = ps2.id
WHERE
  (relatorio_aditivo_enf.inicio BETWEEN '{$begin}' AND '{$end}'
  OR relatorio_aditivo_enf.fim BETWEEN '{$begin}' AND '{$end}')
  AND clientes.status = 4
  AND PACIENTE_ID NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
ORDER BY 
  dataSistema, nomeOrder
SQL;
        return parent::get($sql, 'Assoc');
    }
}