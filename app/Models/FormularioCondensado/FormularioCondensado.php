<?php

namespace App\Models\FormularioCondensado;

use App\Models\AbstractModel;
use App\Models\DAOInterface;
use App\Models\Medico\Prorrogacao as ProrrogacaoMedica;
use App\Models\Enfermagem\Prorrogacao as ProrrogacaoEnfermagem;
use App\Models\Medico\Deflagrado as DeflagradoMedico;
use App\Models\Enfermagem\Deflagrado as DeflagradoEnfermagem;
use App\Models\Prestador\Prestador;

class FormularioCondensado extends AbstractModel implements DAOInterface
{
    protected $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public static function getById($id)
    {
        return parent::get(self::getById($id), 'assoc');
    }

    public static function getAll()
    {
        return parent::get(self::getSQL(), 'assoc');
    }

    public static function getSQL($id = null)
    {
        $id = $id != null ? " WHERE id = '{$id}'" : "";
        return <<<SQL
SELECT * FROM formulario_condensado {$id} ORDER BY created_at
SQL;
    }

    public static function getByFilters($data)
    {
        $sql = <<<SQL
SELECT 
  id,
  tipo_formulario,
  ur_id,
  emitido,
  inicio,
  fim,
  created_at,
  clientes.nome AS paciente
FROM 
  formulario_condensado
  INNER JOIN clientes ON formulario_condensado.paciente_id = clientes.idClientes
WHERE
  paciente_id = '{$data['paciente']}'
  AND (
    inicio BETWEEN '{$data['inicio']}' AND '{$data['fim']}' 
    OR fim BETWEEN '{$data['inicio']}' AND '{$data['fim']}' 
  ) 
  AND tipo_formulario = '{$data['tipo_formulario']}'
ORDER BY 
  created_at DESC 
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getByIdForUpdate($id)
    {
        $response = [];
        $sql = <<<SQL
SELECT 
  formulario_condensado.*,
  clientes.sexo,
  clientes.NUM_MATRICULA_CONVENIO AS matricula,
  '' AS modalidade,
  FLOOR(DATEDIFF(CURDATE(), clientes.nascimento) / 365.25) AS idade,
  clientes.nascimento,
  clientes.nome AS paciente,
  planosdesaude.nome AS nomeplano,
  empresas.nome AS nomeempresa
FROM 
  formulario_condensado
  INNER JOIN clientes ON formulario_condensado.paciente_id = clientes.idClientes
  LEFT JOIN empresas ON clientes.empresa = empresas.id
  LEFT JOIN planosdesaude ON clientes.convenio = planosdesaude.id
WHERE
  formulario_condensado.id = '{$id}'
SQL;
        $response = current(parent::get($sql, 'assoc'));

        $data = [
            'inicio' => $response['inicio'],
            'fim' => $response['fim'],
            'paciente' => $response['paciente_id']
        ];

        $sql = <<<SQL
SELECT 
  formulario_condensado_equipamentos.*
FROM 
  formulario_condensado_equipamentos
WHERE
  formulario_condensado_equipamentos.formulario_condensado_id = '{$id}'
SQL;
        $response['equipamentos'] = parent::get($sql, 'assoc');

        $sql = <<<SQL
SELECT 
  formulario_condensado_tipo_lesao.*
FROM 
  formulario_condensado_tipo_lesao
WHERE
  formulario_condensado_tipo_lesao.formulario_condensado_id = '{$id}'
SQL;
        $response['tipos_lesao'] = parent::get($sql, 'assoc');

        $sql = <<<SQL
SELECT 
  formulario_condensado_evolucao_medica.*
FROM 
  formulario_condensado_evolucao_medica 
WHERE
  formulario_condensado_evolucao_medica.formulario_condensado_id = '{$id}'
SQL;
        $response['evolucao_medica'] = current(parent::get($sql, 'assoc'));
        if($response['tipo_formulario'] == 'p' && $response['evolucao_medica']) {
            $sql = <<<SQL
SELECT
  ASSINATURA AS assinatura
FROM
  relatorio_prorrogacao_med
  INNER JOIN usuarios ON relatorio_prorrogacao_med.USUARIO_ID = usuarios.idUsuarios
WHERE
  relatorio_prorrogacao_med.ID = '{$response['evolucao_medica']['relatorio_id']}'
SQL;
            $response['evolucao_medica']['assinatura'] = current(parent::get($sql, 'assoc'))['assinatura'];

        } elseif($response['tipo_formulario'] == 'd' && $response['evolucao_medica']) {
            $sql = <<<SQL
SELECT
  ASSINATURA AS assinatura
FROM
  relatorio_deflagrado_med
  INNER JOIN usuarios ON relatorio_deflagrado_med.USUARIO_ID = usuarios.idUsuarios
WHERE
  relatorio_deflagrado_med.ID = {$response['evolucao_medica']['relatorio_id']}
SQL;
            $response['evolucao_medica']['assinatura'] = current(parent::get($sql, 'assoc'))['assinatura'];
        }

        $response['evolucao_medica_nova'] = $response['tipo_formulario'] == 'p' ?
            ProrrogacaoMedica::getUltimoRelatorioPacienteByPeriodo($data) :
            DeflagradoMedico::getUltimoRelatorioPacienteByPeriodo($data);

        if (!$response['evolucao_medica']) {
            $response['evolucao_medica'] = $response['evolucao_medica_nova'];
            $response['evolucao_medica']['pendente'] = true;

            if ($response['evolucao_medica']) {
                $response['evolucao_medica']['relatorio_id'] = $response['evolucao_medica']['id'];
                $response['evolucao_medica']['evolucao'] = $response['evolucao_medica']['quadro_clinico'];
                $response['evolucao_medica']['data_visita'] = $response['evolucao_medica']['data_prorrogacao'];
                $response['evolucao_medica']['profissional'] = $response['evolucao_medica']['medico'];
                $response['evolucao_medica']['numero_conselho'] = $response['evolucao_medica']['conselho_regional'];
                if(empty($response['modalidade_id'])){
                    $response['modalidade_id'] = $response['evolucao_medica']['modalidade'];
                }                
            }
        }

        $sql = <<<SQL
SELECT 
  formulario_condensado_evolucao_enfermagem.*
FROM 
  formulario_condensado_evolucao_enfermagem 
WHERE
  formulario_condensado_evolucao_enfermagem.formulario_condensado_id = '{$id}'
SQL;
        $response['evolucao_enfermagem'] = current(parent::get($sql, 'assoc'));

        if($response['tipo_formulario'] == 'p' && $response['evolucao_enfermagem']) {
            $sql = <<<SQL
SELECT
  ASSINATURA AS assinatura
FROM
  relatorio_prorrogacao_enf
  INNER JOIN usuarios ON relatorio_prorrogacao_enf.USUARIO_ID = usuarios.idUsuarios
WHERE
  relatorio_prorrogacao_enf.ID = {$response['evolucao_enfermagem']['relatorio_id']}
SQL;
            $response['evolucao_enfermagem']['assinatura'] = current(parent::get($sql, 'assoc'))['assinatura'];

        } elseif($response['tipo_formulario'] == 'd' && $response['evolucao_enfermagem']) {
            $sql = <<<SQL
SELECT
  ASSINATURA AS assinatura
FROM
  relatorio_deflagrado_enf
  INNER JOIN usuarios ON relatorio_deflagrado_enf.USUARIO_ID = usuarios.idUsuarios
WHERE
  relatorio_deflagrado_enf.ID = {$response['evolucao_enfermagem']['relatorio_id']}
SQL;
            $response['evolucao_enfermagem']['assinatura'] = current(parent::get($sql, 'assoc'))['assinatura'];
        }

        $response['evolucao_enfermagem_nova'] = $response['tipo_formulario'] == 'p' ?
            ProrrogacaoEnfermagem::getUltimoRelatorioPacienteByPeriodo($data) :
            DeflagradoEnfermagem::getUltimoRelatorioPacienteByPeriodo($data);

        if($response['evolucao_enfermagem_nova']) {
            $i = 1;
            foreach ($response['evolucao_enfermagem_nova']['feridas'] as $imagem_lesoes) {
                $response['evolucao_enfermagem_nova']['quadro_clinico'] .= <<<FERIDAS
\n\n
####### LESÃO Nº ({$i}) #######\n
LOCAL: {$imagem_lesoes['LOCAL']}
CARACTERISTICA DA FERIDA: {$imagem_lesoes['CARACTERISTICA']} \n 
TIPO DO CURATIVO EM USO:  {$imagem_lesoes['TIPO_CURATIVO']} \n 
PRESCRIÇÃO DE COBERTURA PARA O PRÓXIMO PERÍODO: {$imagem_lesoes['PRESCRICAO_PROX_PERIODO']}
FERIDAS;
                $i++;
            }
        }

        if (!$response['evolucao_enfermagem']) {
            $response['evolucao_enfermagem'] = $response['evolucao_enfermagem_nova'];
            $response['evolucao_enfermagem']['pendente'] = true;
            if ($response['evolucao_enfermagem']) {
                $response['evolucao_enfermagem']['relatorio_id'] = $response['evolucao_enfermagem']['id'];
                $response['evolucao_enfermagem']['evolucao'] = $response['evolucao_enfermagem']['quadro_clinico'];
                $response['evolucao_enfermagem']['data_visita'] = $response['evolucao_enfermagem']['data_prorrogacao'];
                $response['evolucao_enfermagem']['profissional'] = $response['evolucao_enfermagem']['enfermeiro'];
                $response['evolucao_enfermagem']['numero_conselho'] = $response['evolucao_enfermagem']['conselho_regional'];
            }

            if (count($response['evolucao_enfermagem']) > 0 && !$response['evolucao_enfermagem']['pendente']) {
                $imagens_lesoes = $response['evolucao_enfermagem']['feridas'];
                $i = 1;
                foreach ($imagens_lesoes as $imagem_lesoes) {
                    $response['imagens_lesoes'][] = $imagem_lesoes;
                    $response['evolucao_enfermagem']['evolucao'] .= <<<FERIDAS
\n\n
####### LESÃO Nº ({$i}) #######\n
LOCAL: {$imagem_lesoes['LOCAL']}
CARACTERISTICA DA FERIDA: {$imagem_lesoes['CARACTERISTICA']} \n 
TIPO DO CURATIVO EM USO:  {$imagem_lesoes['TIPO_CURATIVO']} \n 
PRESCRIÇÃO DE COBERTURA PARA O PRÓXIMO PERÍODO: {$imagem_lesoes['PRESCRICAO_PROX_PERIODO']}
FERIDAS;
                    $i++;
                }
            }
        } else {
            $sql = <<<SQL
            SELECT 
              formulario_condensado_evolucao_enfermagem_lesoes.*,
              feridas_relatorio_prorrogacao_enf.*,
              UPPER(feridas_relatorio_prorrogacao_enf.LOCAL) AS LOCAL,
              files_uploaded.path,
              files_uploaded.full_path,
              formulario_condensado_evolucao_enfermagem_lesoes.ferida_relatorio_prorrogacao_enf_id as ferida_id
            FROM 
              formulario_condensado_evolucao_enfermagem_lesoes
              INNER JOIN files_uploaded ON formulario_condensado_evolucao_enfermagem_lesoes.imagem_id = files_uploaded.id
              INNER JOIN feridas_relatorio_prorrogacao_enf_imagem ON formulario_condensado_evolucao_enfermagem_lesoes.imagem_id = feridas_relatorio_prorrogacao_enf_imagem.file_uploaded_id
              INNER JOIN feridas_relatorio_prorrogacao_enf on feridas_relatorio_prorrogacao_enf_imagem.ferida_relatorio_prorrogacao_enf_id = feridas_relatorio_prorrogacao_enf.ID
            WHERE
              formulario_condensado_evolucao_enfermagem_lesoes.formulario_condensado_id = '{$id}'
              and formulario_condensado_evolucao_enfermagem_lesoes.ferida_relatorio_prorrogacao_enf_id = feridas_relatorio_prorrogacao_enf_imagem.ferida_relatorio_prorrogacao_enf_id
SQL;
            $response['imagens_lesoes'] = parent::get($sql, 'assoc');
            if ($response['imagens_lesoes']) {
                $response['evolucao_enfermagem']['feridas_imagem'] = $response['imagens_lesoes'];
            }
        }

        $sql = <<<SQL
SELECT 
  formulario_condensado_evolucao_fisioterapia.*,
  usuarios.ASSINATURA AS assinatura
FROM 
  formulario_condensado_evolucao_fisioterapia
  LEFT JOIN relatorio_prestador ON formulario_condensado_evolucao_fisioterapia.relatorio_id = relatorio_prestador.id 
  LEFT JOIN usuarios ON relatorio_prestador.created_by = usuarios.idUsuarios 
WHERE
  formulario_condensado_evolucao_fisioterapia.formulario_condensado_id = '{$id}'
SQL;
        $response['evolucao_fisioterapia'] = current(parent::get($sql, 'assoc'));
        $response['evolucao_fisioterapia_prestador'] = Prestador::getUltimoRelatorioByFilters($data, 'fisioterapia', $response['tipo_formulario']);

        if (!$response['evolucao_fisioterapia']) {
            $response['evolucao_fisioterapia'] = $response['evolucao_fisioterapia_prestador'];
            $response['evolucao_fisioterapia']['pendente'] = true;
            if ($response['evolucao_fisioterapia']) {
                $response['evolucao_fisioterapia']['relatorio_id'] = $response['evolucao_fisioterapia']['id'];
                $response['evolucao_fisioterapia']['numero_conselho'] = $response['evolucao_fisioterapia']['conselho_regional'];
            }
        }

        $sql = <<<SQL
SELECT 
  formulario_condensado_evolucao_fonoaudiologia.*,
  usuarios.ASSINATURA AS assinatura
FROM 
  formulario_condensado_evolucao_fonoaudiologia
  LEFT JOIN relatorio_prestador ON formulario_condensado_evolucao_fonoaudiologia.relatorio_id = relatorio_prestador.id 
  LEFT JOIN usuarios ON relatorio_prestador.created_by = usuarios.idUsuarios 
WHERE
  formulario_condensado_evolucao_fonoaudiologia.formulario_condensado_id = '{$id}'
SQL;
        $response['evolucao_fonoaudiologia'] = current(parent::get($sql, 'assoc'));
        $response['evolucao_fonoaudiologia_prestador'] = Prestador::getUltimoRelatorioByFilters($data, 'fonoaudiologia', $response['tipo_formulario']);

        if (!$response['evolucao_fonoaudiologia']) {
            $response['evolucao_fonoaudiologia'] = $response['evolucao_fonoaudiologia_prestador'];
            $response['evolucao_fonoaudiologia']['pendente'] = true;
            if ($response['evolucao_fonoaudiologia']) {
                $response['evolucao_fonoaudiologia']['relatorio_id'] = $response['evolucao_fonoaudiologia']['id'];
                $response['evolucao_fonoaudiologia']['numero_conselho'] = $response['evolucao_fonoaudiologia']['conselho_regional'];
            }
        }

        $sql = <<<SQL
SELECT 
  formulario_condensado_evolucao_psicologia.*,
  usuarios.ASSINATURA AS assinatura
FROM 
  formulario_condensado_evolucao_psicologia
  LEFT JOIN relatorio_prestador ON formulario_condensado_evolucao_psicologia.relatorio_id = relatorio_prestador.id 
  LEFT JOIN usuarios ON relatorio_prestador.created_by = usuarios.idUsuarios 
WHERE
  formulario_condensado_evolucao_psicologia.formulario_condensado_id = '{$id}'
SQL;
        $response['evolucao_psicologia'] = current(parent::get($sql, 'assoc'));
        $response['evolucao_psicologia_prestador'] = Prestador::getUltimoRelatorioByFilters($data, 'psicologia', $response['tipo_formulario']);

        if (!$response['evolucao_psicologia']) {
            $response['evolucao_psicologia'] = $response['evolucao_psicologia_prestador'];
            $response['evolucao_psicologia']['pendente'] = true;
            if ($response['evolucao_psicologia']) {
                $response['evolucao_psicologia']['relatorio_id'] = $response['evolucao_psicologia']['id'];
                $response['evolucao_psicologia']['numero_conselho'] = $response['evolucao_psicologia']['conselho_regional'];
            }
        }

        $sql = <<<SQL
SELECT 
  formulario_condensado_evolucao_nutricao.*,
  usuarios.ASSINATURA AS assinatura
FROM 
  formulario_condensado_evolucao_nutricao
  LEFT JOIN relatorio_prestador ON formulario_condensado_evolucao_nutricao.relatorio_id = relatorio_prestador.id 
  LEFT JOIN usuarios ON relatorio_prestador.created_by = usuarios.idUsuarios 
WHERE
  formulario_condensado_evolucao_nutricao.formulario_condensado_id = '{$id}'
SQL;
        $response['evolucao_nutricao'] = current(parent::get($sql, 'assoc'));
        $response['evolucao_nutricao_prestador'] = Prestador::getUltimoRelatorioByFilters($data, 'nutricao', $response['tipo_formulario']);

        if (!$response['evolucao_nutricao']) {
            $response['evolucao_nutricao'] = $response['evolucao_nutricao_prestador'];
            $response['evolucao_nutricao']['pendente'] = true;
            if ($response['evolucao_nutricao']) {
                $response['evolucao_nutricao']['relatorio_id'] = $response['evolucao_nutricao']['id'];
                $response['evolucao_nutricao']['numero_conselho'] = $response['evolucao_nutricao']['conselho_regional'];
            }
        }

        if ($response['tipo_formulario'] == 'p') {
            $sql = <<<SQL
SELECT 
  formulario_condensado_pad.*,
  ce1.cuidado AS medico_cuidado,
  ce2.cuidado AS enfermagem_cuidado,
  ce3.cuidado AS tecnico_cuidado,
  ce4.cuidado AS fisio_motora_cuidado,
  ce5.cuidado AS fisio_resp_cuidado,
  ce6.cuidado AS fono_cuidado,
  ce7.cuidado AS psico_cuidado,
  ce8.cuidado AS nutri_cuidado,

  fr1.frequencia AS medico_frequencia,
  fr2.frequencia AS enfermagem_frequencia,
  fr3.frequencia AS tecnico_frequencia,
  fr4.frequencia AS fisio_motora_frequencia,
  fr5.frequencia AS fisio_resp_frequencia,
  fr6.frequencia AS fono_frequencia,
  fr7.frequencia AS psico_frequencia,
  fr8.frequencia AS nutri_frequencia
FROM 
  formulario_condensado_pad
  LEFT JOIN cuidadosespeciais AS ce1 ON formulario_condensado_pad.medico_cuidados_especiais_id = ce1.id
  LEFT JOIN cuidadosespeciais AS ce2 ON formulario_condensado_pad.enfermagem_cuidados_especiais_id = ce2.id
  LEFT JOIN cuidadosespeciais AS ce3 ON formulario_condensado_pad.tecnico_enfermagem_cuidados_especiais_id = ce3.id
  LEFT JOIN cuidadosespeciais AS ce4 ON formulario_condensado_pad.fisioterapia_motora_cuidados_especiais_id = ce4.id
  LEFT JOIN cuidadosespeciais AS ce5 ON formulario_condensado_pad.fisioterapia_respiratoria_cuidados_especiais_id = ce5.id
  LEFT JOIN cuidadosespeciais AS ce6 ON formulario_condensado_pad.fonoterapia_cuidados_especiais_id = ce6.id
  LEFT JOIN cuidadosespeciais AS ce7 ON formulario_condensado_pad.psicologo_cuidados_especiais_id = ce7.id
  LEFT JOIN cuidadosespeciais AS ce8 ON formulario_condensado_pad.nutricionista_cuidados_especiais_id = ce8.id
  
  LEFT JOIN frequencia AS fr1 ON formulario_condensado_pad.medico_frequencia_id = fr1.id
  LEFT JOIN frequencia AS fr2 ON formulario_condensado_pad.enfermagem_frequencia_id = fr2.id
  LEFT JOIN frequencia AS fr3 ON formulario_condensado_pad.tecnico_enfermagem_frequencia_id = fr3.id
  LEFT JOIN frequencia AS fr4 ON formulario_condensado_pad.fisioterapia_motora_frequencia_id = fr4.id
  LEFT JOIN frequencia AS fr5 ON formulario_condensado_pad.fisioterapia_respiratoria_frequencia_id = fr5.id
  LEFT JOIN frequencia AS fr6 ON formulario_condensado_pad.fonoterapia_frequencia_id = fr6.id
  LEFT JOIN frequencia AS fr7 ON formulario_condensado_pad.psicologo_frequencia_id = fr7.id
  LEFT JOIN frequencia AS fr8 ON formulario_condensado_pad.nutricionista_frequencia_id = fr8.id
WHERE
  formulario_condensado_pad.formulario_condensado_id = '{$id}'
SQL;
            $response['pad'] = current(parent::get($sql, 'assoc'));
        }

        return $response;
    }

    public static function getEvolucaoByTipo($id, $tipo_formulario)
    {
        $sql = <<<SQL
SELECT 
  formulario_condensado_evolucao_{$tipo_formulario}.*
FROM 
  formulario_condensado_evolucao_{$tipo_formulario}
WHERE
  formulario_condensado_evolucao_{$tipo_formulario}.formulario_condesado_id = '{$id}'
SQL;
        return current(parent::get($sql, 'assoc'));
    }

    public static function getEquipamentosByFormularioId($id)
    {
        $sql = <<<SQL
SELECT 
  formulario_condensado_equipamentos.*
FROM 
  formulario_condensado_equipamentos
WHERE
  formulario_condensado_equipamentos.formulario_condensado_id = '{$id}'
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getTiposLesaoByFormularioId($id)
    {
        $sql = <<<SQL
SELECT 
  formulario_condensado_tipo_lesao.*
FROM 
  formulario_condensado_tipo_lesao
WHERE
  formulario_condensado_tipo_lesao.formulario_condensado_id = '{$id}'
SQL;
        return parent::get($sql, 'assoc');
    }

    public function save($data)
    {
        $this->conn->query("BEGIN");
        $this->conn->autocommit(false);
        $sql = <<<SQL
INSERT INTO
`formulario_condensado`
(
id,
paciente_id,
ur_id,
convenio_id,
modalidade_id,
tipo_formulario,
inicio,
fim,
dieta,
dieta_nome,
dieta_via,
dieta_adm,
oxigenoterapia,
oxigenoterapia_vazao,
deambula,
drenos_cateteres_ostomias,
equipamentos_outros,
integridade_cutanea,
atb,
atb_nome,
atb_inicio,
atb_previsao_termino,
antifungico,
antifungico_nome,
antifungico_inicio,
antifungico_previsao_termino,
created_by,
created_at,
deleted_by,
deleted_at,
emitido
)
 VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 NOW(),
 NULL,
 NULL,
 0
 );
SQL;
        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param(
                'iiiissssssssssssssssssssss',
                $data['paciente'],
                $data['empresa'],
                $data['convenio'],
                $data['modalidade'],
                $data['tipo_formulario'],
                $data['inicio'],
                $data['fim'],
                $data['dieta'],
                $data['dieta_nome'],
                $data['dieta_via'],
                $data['dieta_adm'],
                $data['oxigenoterapia'],
                $data['vazao'],
                $data['deambula'],
                $data['drenos'],
                $data['equipamentos_outros'],
                $data['integridade_cutanea'],
                $data['atb'],
                $data['atb_nome'],
                $data['atb_inicio'],
                $data['atb_previsao_termino'],
                $data['antifungico'],
                $data['antifungico_nome'],
                $data['antifungico_inicio'],
                $data['antifungico_previsao_termino'],
                $_SESSION['id_user']
            );

            $rs = $stmt->execute();
            $formulario_id = $stmt->insert_id;
        } else {
            return false;
        }

        // EQUIPAMENTOS
        if (count($data['equipamentos']) > 0) {
            foreach ($data['equipamentos'] as $equipamento) {
                $sql = <<<SQL
INSERT INTO
`formulario_condensado_equipamentos`
(
id,
formulario_condensado_id,
equipamento_id
) VALUES (
 NULL,
 ?,
 ?
);
SQL;
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param(
                        'ii',
                        $formulario_id,
                        $equipamento
                    );

                    $rs = $stmt->execute();
                } else {
                    return false;
                }
            }
        }

        // TIPOS LESÃO
        if ($data['integridade_cutanea'] == '2') {
            foreach ($data['tipo_lesao'] as $tipo_lesao) {
                $sql = <<<SQL
INSERT INTO
`formulario_condensado_tipo_lesao`
(
id,
formulario_condensado_id,
lesao_id
) VALUES (
 NULL,
 ?,
 ?
);
SQL;
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param(
                        'ii',
                        $formulario_id,
                        $tipo_lesao
                    );

                    $rs = $stmt->execute();
                } else {
                    return false;
                }
            }
        }

        // EVOLUCAO MEDICA
        if ($data['evolucao_medica']['evolucao'] != '') {
            $sql = <<<SQL
INSERT INTO
`formulario_condensado_evolucao_medica`
(
id,
formulario_condensado_id,
relatorio_id,
evolucao,
data_visita,
profissional,
numero_conselho
) VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?
);
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'iissss',
                    $formulario_id,
                    $data['evolucao_medica']['id'],
                    $data['evolucao_medica']['evolucao'],
                    $data['evolucao_medica']['data_visita'],
                    $data['evolucao_medica']['profissional'],
                    $data['evolucao_medica']['conselho']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }
        }

        // EVOLUCAO ENFERMAGEM
        if ($data['evolucao_enfermagem']['evolucao'] != '') {
            $sql = <<<SQL
INSERT INTO
`formulario_condensado_evolucao_enfermagem`
(
id,
formulario_condensado_id,
relatorio_id,
evolucao,
data_visita,
profissional,
numero_conselho
) VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?
);
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'iissss',
                    $formulario_id,
                    $data['evolucao_enfermagem']['id'],
                    $data['evolucao_enfermagem']['evolucao'],
                    $data['evolucao_enfermagem']['data_visita'],
                    $data['evolucao_enfermagem']['profissional'],
                    $data['evolucao_enfermagem']['conselho']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }

            // IMAGENS DAS LESOES
            foreach ($data['imagens_lesoes'] as $imagem_id) {
                $sql = <<<SQL
INSERT INTO
`formulario_condensado_evolucao_enfermagem_lesoes`
(
id,
formulario_condensado_id,
imagem_id,
ferida_relatorio_prorrogacao_enf_id
) VALUES (
 NULL,
 ?,
 ?,
 ?
);
SQL;
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param(
                        'iii',
                        $formulario_id,
                        $imagem_id['imagem_id'],
                        $imagem_id['ferida_id']
                    );

                    $rs = $stmt->execute();
                } else {
                    return false;
                }
            }
        }

        // EVOLUCAO FISIOTERAPIA
        if ($data['evolucao_fisioterapia']['evolucao'] != '') {
            $sql = <<<SQL
INSERT INTO
`formulario_condensado_evolucao_fisioterapia`
(
id,
formulario_condensado_id,
relatorio_id,
evolucao,
data_visita,
profissional,
numero_conselho
) VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?
);
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'iissss',
                    $formulario_id,
                    $data['evolucao_fisioterapia']['id'],
                    $data['evolucao_fisioterapia']['evolucao'],
                    $data['evolucao_fisioterapia']['data_visita'],
                    $data['evolucao_fisioterapia']['profissional'],
                    $data['evolucao_fisioterapia']['conselho']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }
        }

        // EVOLUCAO FONOAUDIOLOGIA
        if ($data['evolucao_fonoaudiologia']['evolucao'] != '') {
            $sql = <<<SQL
INSERT INTO
`formulario_condensado_evolucao_fonoaudiologia`
(
id,
formulario_condensado_id,
relatorio_id,
evolucao,
data_visita,
profissional,
numero_conselho
) VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?
);
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'iissss',
                    $formulario_id,
                    $data['evolucao_fonoaudiologia']['id'],
                    $data['evolucao_fonoaudiologia']['evolucao'],
                    $data['evolucao_fonoaudiologia']['data_visita'],
                    $data['evolucao_fonoaudiologia']['profissional'],
                    $data['evolucao_fonoaudiologia']['conselho']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }
        }

        // EVOLUCAO PSICOLOGIA
        if ($data['evolucao_psicologia']['evolucao'] != '') {
            $sql = <<<SQL
INSERT INTO
`formulario_condensado_evolucao_psicologia`
(
id,
formulario_condensado_id,
relatorio_id,
evolucao,
data_visita,
profissional,
numero_conselho
) VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?
);
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'iissss',
                    $formulario_id,
                    $data['evolucao_psicologia']['id'],
                    $data['evolucao_psicologia']['evolucao'],
                    $data['evolucao_psicologia']['data_visita'],
                    $data['evolucao_psicologia']['profissional'],
                    $data['evolucao_psicologia']['conselho']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }
        }

        // EVOLUCAO NUTRIÇÃO
        if ($data['evolucao_nutricao']['evolucao'] != '') {
            $sql = <<<SQL
INSERT INTO
`formulario_condensado_evolucao_nutricao`
(
id,
formulario_condensado_id,
relatorio_id,
evolucao,
data_visita,
profissional,
numero_conselho
) VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?
);
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'iissss',
                    $formulario_id,
                    $data['evolucao_nutricao']['id'],
                    $data['evolucao_nutricao']['evolucao'],
                    $data['evolucao_nutricao']['data_visita'],
                    $data['evolucao_nutricao']['profissional'],
                    $data['evolucao_nutricao']['conselho']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }
        }

        // PAD
        if ($data['tipo_formulario'] == 'p') {
            $sql = <<<SQL
INSERT INTO
`formulario_condensado_pad`
(
id,
formulario_condensado_id,
modalidade,
medico_cuidados_especiais_id,
medico_frequencia_id,
enfermagem_cuidados_especiais_id,
enfermagem_frequencia_id,
tecnico_enfermagem_cuidados_especiais_id,
tecnico_enfermagem_frequencia_id,
fisioterapia_motora_cuidados_especiais_id,
fisioterapia_motora_frequencia_id,
fisioterapia_respiratoria_cuidados_especiais_id,
fisioterapia_respiratoria_frequencia_id,
fonoterapia_cuidados_especiais_id,
fonoterapia_frequencia_id,
psicologo_cuidados_especiais_id,
psicologo_frequencia_id,
nutricionista_cuidados_especiais_id,
nutricionista_frequencia_id
) VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?
);
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'isiiiiiiiiiiiiiiii',
                    $formulario_id,
                    $data['pad']['modalidade'],
                    $data['pad']['medico']['cuidados'],
                    $data['pad']['medico']['frequencia'],
                    $data['pad']['enfermagem']['cuidados'],
                    $data['pad']['enfermagem']['frequencia'],
                    $data['pad']['tecnico']['cuidados'],
                    $data['pad']['tecnico']['frequencia'],
                    $data['pad']['fisio-motora']['cuidados'],
                    $data['pad']['fisio-motora']['frequencia'],
                    $data['pad']['fisio-resp']['cuidados'],
                    $data['pad']['fisio-resp']['frequencia'],
                    $data['pad']['fono']['cuidados'],
                    $data['pad']['fono']['frequencia'],
                    $data['pad']['psico']['cuidados'],
                    $data['pad']['psico']['frequencia'],
                    $data['pad']['nutri']['cuidados'],
                    $data['pad']['nutri']['frequencia']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }
        }

        if (!empty($rs)) {
            return $this->conn->query("COMMIT");
        } else {
            $this->conn->query("ROLLBACK");
            return $this->conn->error;
        }
    }

    public function update($data)
    {
        $this->conn->query("BEGIN");
        $this->conn->autocommit(false);
        $sql = <<<SQL
UPDATE
`formulario_condensado`
SET
modalidade_id = ?,
inicio = ?,
fim = ?,
dieta = ?,
dieta_nome = ?,
dieta_via = ?,
dieta_adm = ?,
oxigenoterapia = ?,
oxigenoterapia_vazao = ?,
deambula = ?,
drenos_cateteres_ostomias = ?,
equipamentos_outros = ?,
integridade_cutanea = ?,
atb = ?,
atb_nome = ?,
atb_inicio = ?,
atb_previsao_termino = ?,
antifungico = ?,
antifungico_nome = ?,
antifungico_inicio = ?,
antifungico_previsao_termino = ?,
updated_by = ?,
updated_at = NOW()
WHERE
id = ?
SQL;
        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param(
                'sssssssssssssssssssssii',
                $data['modalidade_id'],
                $data['inicio'],
                $data['fim'],
                $data['dieta'],
                $data['dieta_nome'],
                $data['dieta_via'],
                $data['dieta_adm'],
                $data['oxigenoterapia'],
                $data['vazao'],
                $data['deambula'],
                $data['drenos'],
                $data['equipamentos_outros'],
                $data['integridade_cutanea'],
                $data['atb'],
                $data['atb_nome'],
                $data['atb_inicio'],
                $data['atb_previsao_termino'],
                $data['antifungico'],
                $data['antifungico_nome'],
                $data['antifungico_inicio'],
                $data['antifungico_previsao_termino'],
                $_SESSION['id_user'],
                $data['formulario_id']
            );

            $rs = $stmt->execute();
        } else {
            return false;
        }

        // EQUIPAMENTOS
        $sql = <<<SQL
DELETE FROM formulario_condensado_equipamentos WHERE formulario_condensado_id = ?
SQL;
        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param(
                'i',
                $data['formulario_id']
            );

            $rs = $stmt->execute();
        } else {
            return false;
        }

        if (count($data['equipamentos']) > 0) {
            foreach ($data['equipamentos'] as $equipamento) {
                $sql = <<<SQL
INSERT INTO
`formulario_condensado_equipamentos`
(
id,
formulario_condensado_id,
equipamento_id
) VALUES (
 NULL,
 ?,
 ?
);
SQL;
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param(
                        'ii',
                        $data['formulario_id'],
                        $equipamento
                    );

                    $rs = $stmt->execute();
                } else {
                    return false;
                }
            }
        }

        // TIPOS LESÃO
        $sql = <<<SQL
DELETE FROM formulario_condensado_tipo_lesao WHERE formulario_condensado_id = ?
SQL;
        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param(
                'i',
                $data['formulario_id']
            );

            $rs = $stmt->execute();
        } else {
            return false;
        }

        if ($data['integridade_cutanea'] == '2') {
            foreach ($data['tipo_lesao'] as $tipo_lesao) {
                $sql = <<<SQL
INSERT INTO
`formulario_condensado_tipo_lesao`
(
id,
formulario_condensado_id,
lesao_id
) VALUES (
 NULL,
 ?,
 ?
);
SQL;
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param(
                        'ii',
                        $data['formulario_id'],
                        $tipo_lesao
                    );

                    $rs = $stmt->execute();
                } else {
                    return false;
                }
            }
        }

        // EVOLUCAO MEDICA
        if ($data['evolucao_medica']['evolucao'] != '') {
            $sql = <<<SQL
DELETE FROM formulario_condensado_evolucao_medica WHERE formulario_condensado_id = ?
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'i',
                    $data['formulario_id']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }

            $sql = <<<SQL
INSERT INTO
`formulario_condensado_evolucao_medica`
(
id,
formulario_condensado_id,
relatorio_id,
evolucao,
data_visita,
profissional,
numero_conselho
) VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?
);
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'iissss',
                    $data['formulario_id'],
                    $data['evolucao_medica']['relatorio_id'],
                    $data['evolucao_medica']['evolucao'],
                    $data['evolucao_medica']['data_visita'],
                    $data['evolucao_medica']['profissional'],
                    $data['evolucao_medica']['conselho']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }
        }

        // EVOLUCAO ENFERMAGEM
        if ($data['evolucao_enfermagem']['evolucao'] != '') {
            $sql = <<<SQL
DELETE FROM formulario_condensado_evolucao_enfermagem WHERE formulario_condensado_id = ?
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'i',
                    $data['formulario_id']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }

            $sql = <<<SQL
INSERT INTO
`formulario_condensado_evolucao_enfermagem`
(
id,
formulario_condensado_id,
relatorio_id,
evolucao,
data_visita,
profissional,
numero_conselho
) VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?
);
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'iissss',
                    $data['formulario_id'],
                    $data['evolucao_enfermagem']['relatorio_id'],
                    $data['evolucao_enfermagem']['evolucao'],
                    $data['evolucao_enfermagem']['data_visita'],
                    $data['evolucao_enfermagem']['profissional'],
                    $data['evolucao_enfermagem']['conselho']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }
        }

        // IMAGENS DAS LESOES
        $sql = <<<SQL
DELETE FROM formulario_condensado_evolucao_enfermagem_lesoes WHERE formulario_condensado_id = ?
SQL;
        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param(
                'i',
                $data['formulario_id']
            );

            $rs = $stmt->execute();
        } else {
            return false;
        }

        foreach ($data['imagens_lesoes'] as $imagem_id) {
            $sql = <<<SQL
INSERT INTO
`formulario_condensado_evolucao_enfermagem_lesoes`
(
id,
formulario_condensado_id,
imagem_id,
ferida_relatorio_prorrogacao_enf_id
) VALUES (
NULL,
?,
?,
?
);
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'iii',
                    $data['formulario_id'],
                    $imagem_id['imagem_id'],
                    $imagem_id['ferida_id']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }
        }

        // EVOLUCAO FISIOTERAPIA
        if ($data['evolucao_fisioterapia']['evolucao'] != '') {
            $sql = <<<SQL
DELETE FROM formulario_condensado_evolucao_fisioterapia WHERE formulario_condensado_id = ?
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'i',
                    $data['formulario_id']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }

            $sql = <<<SQL
INSERT INTO
`formulario_condensado_evolucao_fisioterapia`
(
id,
formulario_condensado_id,
relatorio_id,
evolucao,
data_visita,
profissional,
numero_conselho
) VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?
);
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'iissss',
                    $data['formulario_id'],
                    $data['evolucao_fisioterapia']['relatorio_id'],
                    $data['evolucao_fisioterapia']['evolucao'],
                    $data['evolucao_fisioterapia']['data_visita'],
                    $data['evolucao_fisioterapia']['profissional'],
                    $data['evolucao_fisioterapia']['conselho']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }
        }

        // EVOLUCAO FONOAUDIOLOGIA
        if ($data['evolucao_fonoaudiologia']['evolucao'] != '') {
            $sql = <<<SQL
DELETE FROM formulario_condensado_evolucao_fonoaudiologia WHERE formulario_condensado_id = ?
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'i',
                    $data['formulario_id']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }

            $sql = <<<SQL
INSERT INTO
`formulario_condensado_evolucao_fonoaudiologia`
(
id,
formulario_condensado_id,
relatorio_id,
evolucao,
data_visita,
profissional,
numero_conselho
) VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?
);
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'iissss',
                    $data['formulario_id'],
                    $data['evolucao_fonoaudiologia']['relatorio_id'],
                    $data['evolucao_fonoaudiologia']['evolucao'],
                    $data['evolucao_fonoaudiologia']['data_visita'],
                    $data['evolucao_fonoaudiologia']['profissional'],
                    $data['evolucao_fonoaudiologia']['conselho']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }
        }

        // EVOLUCAO PSICOLOGIA
        if ($data['evolucao_psicologia']['evolucao'] != '') {
            $sql = <<<SQL
DELETE FROM formulario_condensado_evolucao_psicologia WHERE formulario_condensado_id = ?
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'i',
                    $data['formulario_id']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }

            $sql = <<<SQL
INSERT INTO
`formulario_condensado_evolucao_psicologia`
(
id,
formulario_condensado_id,
relatorio_id,
evolucao,
data_visita,
profissional,
numero_conselho
) VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?
);
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'iissss',
                    $data['formulario_id'],
                    $data['evolucao_psicologia']['relatorio_id'],
                    $data['evolucao_psicologia']['evolucao'],
                    $data['evolucao_psicologia']['data_visita'],
                    $data['evolucao_psicologia']['profissional'],
                    $data['evolucao_psicologia']['conselho']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }
        }

        // EVOLUCAO NUTRIÇÃO
        if ($data['evolucao_nutricao']['evolucao'] != '') {
            $sql = <<<SQL
DELETE FROM formulario_condensado_evolucao_nutricao WHERE formulario_condensado_id = ?
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'i',
                    $data['formulario_id']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }

            $sql = <<<SQL
INSERT INTO
`formulario_condensado_evolucao_nutricao`
(
id,
formulario_condensado_id,
relatorio_id,
evolucao,
data_visita,
profissional,
numero_conselho
) VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?
);
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'iissss',
                    $data['formulario_id'],
                    $data['evolucao_nutricao']['relatorio_id'],
                    $data['evolucao_nutricao']['evolucao'],
                    $data['evolucao_nutricao']['data_visita'],
                    $data['evolucao_nutricao']['profissional'],
                    $data['evolucao_nutricao']['conselho']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }
        }

        // PAD
        if ($data['tipo_formulario'] == 'p') {
            $sql = <<<SQL
UPDATE
`formulario_condensado_pad`
SET
modalidade = ?,
medico_cuidados_especiais_id = ?,
medico_frequencia_id = ?,
enfermagem_cuidados_especiais_id = ?,
enfermagem_frequencia_id = ?,
tecnico_enfermagem_cuidados_especiais_id = ?,
tecnico_enfermagem_frequencia_id = ?,
fisioterapia_motora_cuidados_especiais_id = ?,
fisioterapia_motora_frequencia_id = ?,
fisioterapia_respiratoria_cuidados_especiais_id = ?,
fisioterapia_respiratoria_frequencia_id = ?,
fonoterapia_cuidados_especiais_id = ?,
fonoterapia_frequencia_id = ?,
psicologo_cuidados_especiais_id = ?,
psicologo_frequencia_id = ?,
nutricionista_cuidados_especiais_id = ?,
nutricionista_frequencia_id = ?
WHERE
formulario_condensado_id = ?
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'siiiiiiiiiiiiiiiii',
                    $data['pad']['modalidade'],
                    $data['pad']['medico']['cuidados'],
                    $data['pad']['medico']['frequencia'],
                    $data['pad']['enfermagem']['cuidados'],
                    $data['pad']['enfermagem']['frequencia'],
                    $data['pad']['tecnico']['cuidados'],
                    $data['pad']['tecnico']['frequencia'],
                    $data['pad']['fisio-motora']['cuidados'],
                    $data['pad']['fisio-motora']['frequencia'],
                    $data['pad']['fisio-resp']['cuidados'],
                    $data['pad']['fisio-resp']['frequencia'],
                    $data['pad']['fono']['cuidados'],
                    $data['pad']['fono']['frequencia'],
                    $data['pad']['psico']['cuidados'],
                    $data['pad']['psico']['frequencia'],
                    $data['pad']['nutri']['cuidados'],
                    $data['pad']['nutri']['frequencia'],
                    $data['formulario_id']
                );

                $rs = $stmt->execute();
            } else {
                return false;
            }
        }

        if (!empty($rs)) {
            return $this->conn->query("COMMIT");
        } else {
            $this->conn->query("ROLLBACK");
            return $this->conn->error;
        }
    }

    public static function getUltimoRelatorioByFilters($data, $tipo_prestador, $tipo_relatorio)
    {
        $sql = <<<SQL
SELECT 
  relatorio_prestador.*
FROM 
  relatorio_prestador
WHERE
  paciente_id = '{$data['paciente']}'
  AND (
    inicio BETWEEN '{$data['inicio']}' AND '{$data['fim']}' 
    OR fim BETWEEN '{$data['inicio']}' AND '{$data['fim']}' 
  ) 
  AND tipo_relatorio = '{$tipo_relatorio}'
  AND tipo_prestador = '{$tipo_prestador}'
ORDER BY 
  id DESC LIMIT 1
SQL;
        return current(parent::get($sql, 'assoc'));
    }
}
