<?php

namespace App\Models\Prestador;

use App\Models\AbstractModel;
use App\Models\DAOInterface;

class Prestador extends AbstractModel implements DAOInterface
{
    protected $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public static function getById($id)
    {
        return parent::get(self::getById($id), 'assoc');
    }

    public static function getAll()
    {
        return parent::get(self::getSQL(), 'assoc');
    }

    public static function getSQL($id = null)
    {
        $id = $id != null ? " WHERE id = '{$id}'" : "";
        return <<<SQL
SELECT * FROM relatorio_prestador {$id} ORDER BY created_at
SQL;
    }

    public static function getByFilters($data)
    {
        $sql = <<<SQL
SELECT 
  id,
  tipo_relatorio,
  emitido,
  inicio,
  fim,
  created_at,
  clientes.nome AS paciente
FROM 
  relatorio_prestador
  INNER JOIN clientes ON relatorio_prestador.paciente_id = clientes.idClientes
WHERE
  paciente_id = '{$data['paciente']}'
  AND (
    inicio BETWEEN '{$data['inicio']}' AND '{$data['fim']}' 
    OR fim BETWEEN '{$data['inicio']}' AND '{$data['fim']}' 
  ) 
  AND tipo_relatorio = '{$data['tipo_relatorio']}'
  AND created_by = '{$_SESSION['id_user']}'
ORDER BY 
  created_at
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getByIdForUpdate($id)
    {
        $sql = <<<SQL
SELECT 
  relatorio_prestador.*,
  clientes.sexo,
  clientes.NUM_MATRICULA_CONVENIO AS matricula,
  '' AS modalidade,
  FLOOR(DATEDIFF(CURDATE(), clientes.nascimento) / 365.25) AS idade,
  clientes.nascimento,
  clientes.nome AS paciente,
  planosdesaude.nome AS nomeplano,
  empresas.nome AS nomeempresa
FROM 
  relatorio_prestador
  INNER JOIN clientes ON relatorio_prestador.paciente_id = clientes.idClientes
  LEFT JOIN empresas ON clientes.empresa = empresas.id
  LEFT JOIN planosdesaude ON clientes.convenio = planosdesaude.id
WHERE
  relatorio_prestador.id = '{$id}'
SQL;
        return current(parent::get($sql, 'assoc'));
    }

    public function save($data)
    {
        session_start();
        $this->conn->query("BEGIN");
        $this->conn->autocommit(FALSE);
        $sql = <<<SQL
INSERT INTO
`relatorio_prestador`
(
id,
paciente_id,
empresa_id,
convenio_id,
tipo_relatorio,
tipo_prestador,
inicio,
fim,
evolucao,
data_visita,
profissional,
conselho_regional,
created_at,
created_by,
deleted_at,
deleted_by,
emitido
)
 VALUES (
 NULL,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 ?,
 NOW(),
 ?,
 NULL,
 NULL,
 0
 );
SQL;
        if($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param (
                'iiissssssssi',
                $data['paciente'],
                $data['empresa'],
                $data['convenio'],
                $data['tipo_relatorio'],
                $data['tipo_prestador'],
                $data['inicio'],
                $data['fim'],
                $data['evolucao'],
                $data['data_visita'],
                $data['profissional'],
                $data['conselho'],
                $_SESSION['id_user']
            );

            $rs = $stmt->execute();
        }else{
            return false;
        }
        if(!empty($rs)){
            return $this->conn->query("COMMIT");
        }else{
            $this->conn->query("ROLLBACK");
            return $this->conn->error;
        }
    }

    public function update($data)
    {
        session_start();
        $this->conn->query("BEGIN");
        $this->conn->autocommit(FALSE);
        $sql = <<<SQL
UPDATE
  `relatorio_prestador`
SET
  inicio = ?,
  fim = ?,
  evolucao = ?,
  data_visita = ?,
  profissional = ?,
  conselho_regional = ?,
  updated_by = ?,
  updated_at = NOW()
WHERE
  id = ?
SQL;
        if($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param (
                'ssssssii',
                $data['inicio'],
                $data['fim'],
                $data['evolucao'],
                $data['data_visita'],
                $data['profissional'],
                $data['conselho'],
                $_SESSION['id_user'],
                $data['relatorio_id']
            );

            $rs = $stmt->execute();
        }else{
            return false;
        }
        if(!empty($rs)){
            return $this->conn->query("COMMIT");
        }else{
            $this->conn->query("ROLLBACK");
            return $this->conn->error;
        }
    }

    public static function getUltimoRelatorioByFilters($data, $tipo_prestador, $tipo_relatorio)
    {
        $sql = <<<SQL
SELECT 
  relatorio_prestador.*
FROM 
  relatorio_prestador
WHERE
  paciente_id = '{$data['paciente']}'
  AND (
    inicio BETWEEN '{$data['inicio']}' AND '{$data['fim']}' 
    OR fim BETWEEN '{$data['inicio']}' AND '{$data['fim']}' 
  ) 
  AND tipo_relatorio = '{$tipo_relatorio}'
  AND tipo_prestador = '{$tipo_prestador}'
ORDER BY 
  id DESC LIMIT 1
SQL;
        return current(parent::get($sql, 'assoc'));
    }
}