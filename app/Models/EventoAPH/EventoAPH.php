<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 29/05/2017
 * Time: 10:03
 */

namespace app\Models\EventoAPH;

use App\Helpers\StringHelper;
use App\Models\AbstractModel;
use App\Models\DAOInterface;





class EventoAPH extends AbstractModel implements DAOInterface
{
    protected $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public static function getSQL($id = null)
    {
        // ...
    }

    public static function salvar($dados)
    {

        $sql = <<<SQL
        Insert INTO
        evento_aph
        (
`data`,
`hora`,
`solicitante`,
`telefone`,
`tipo_servico`,
`publico_alvo`,
`endereco`,
`cidade_id`,
`complemento`,
`observacao`,
`criado_em`,
`criado_por`

        )
        VALUES
        (
'{$dados['dataEventoAph']}',
'{$dados['horaEventoAph']}',
'{$dados['solicitante']}',
'{$dados['telefoneSolicitante']}',
'{$dados['tipoServico']}',
'{$dados['publicoAlvo']}',
'{$dados['endereco']}',
'{$dados['cidade']}',
'{$dados['complemento']}',
'{$dados['observacao']}',
now(),
{$_SESSION['id_user']}


        )

SQL;


        $r = mysql_query($sql);
        if (!$r) {
            echo "Erro ao salvar Evento/APH";
            return;
        }


        savelog(mysql_escape_string(addslashes($sql)));

        echo 1;

    }

    public static function getPreCadastroByFilters($solicitante,$tipoServico,$inicio,$fim)
    {
        $condSolicitante = !empty($solicitante) ? "AND solicitante like '%{$solicitante}%'" : '';
        $condTipoServico = $tipoServico == 'T' ? "" : "AND tipo_servico = '{$tipoServico}'";

        $sql = <<<SQL
        SELECT
        *
        FROM
        evento_aph
        WHERE
        data BETWEEN '{$inicio}' AND '{$fim}'
        {$condSolicitante}
        {$condTipoServico}
        AND cancelado_por is null

SQL;

        return parent::get($sql);

    }

    public static function getById($id)
    {


        $sql = <<<SQL
        SELECT
        *
        FROM
        evento_aph
        WHERE
        id = {$id}


SQL;

        return parent::get($sql);

    }

    public static function finalizar(
                            $idEventoAPH,
                            $fornecedor_cliente,
                              $data_termino,
                              $hora_termino,
                              $valor,
                              $outros_custos,
                              $medico,
                              $valor_medico,
                              $tecnico,
                              $valor_tecnico,
                              $condutor,
                              $valor_condutor,
                              $recebido,
                              $forma_pagamento,
                              $observacao,
                              $itens,
                              $empresa,
                              $enfermeiro,
                              $valor_enfermeiro,
                            $seraCobrada,
                              $justificativaNaoCobrar,
                            $contaRecebido,
                            $recebidoEm)
    {
        session_start();
        mysql_query('BEGIN');
        $sql = <<<SQL
        UPDATE
        evento_aph
        SET
        `finalizado_em` = now(),
        `finalizado_por` = '{$_SESSION['id_user']}'
        WHERE
        id = {$idEventoAPH}
SQL;
        $r = mysql_query($sql);
        if (!$r) {
            echo "Erro ao atualizar Evento/APH";
            mysql_query('ROLLBACK');
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));

        $valor = StringHelper::moneyStringToFloat($valor);
        $outros_custos = StringHelper::moneyStringToFloat($outros_custos);
        $valor_medico = StringHelper::moneyStringToFloat($valor_medico);
        $valor_tecnico = StringHelper::moneyStringToFloat($valor_tecnico);
        $valor_condutor = StringHelper::moneyStringToFloat($valor_condutor);
        $valor_enfermeiro = StringHelper::moneyStringToFloat($valor_enfermeiro);



        $sql = <<<SQL
       INSERT INTO
       evento_aph_finalizar
       (
          evento_aph_id,
          fornecedor_cliente,
          valor,
          outros_custos,
          data_termino,
          hora_termino,
          medico,
          valor_medico,
          tecnico,
          valor_tecnico,
          condutor,
          valor_condutor,
          enfermeiro,
          valor_enfermeiro,
          recebido,
          forma_pagamento,
          observacao_final,
          empresa_id,
          sera_cobrada,
          justificativa_nao_cobrar,
          conta_recebido,
          recebido_em
          )
          VALUES
          (
            '{$idEventoAPH}',
            '{$fornecedor_cliente}',
            '{$valor}',
            '{$outros_custos}',
            '{$data_termino}',
            '{$hora_termino}',
            '{$medico}',
            '{$valor_medico}',
            '{$tecnico}',
            '{$valor_tecnico}',
            '{$condutor}',
            '{$valor_condutor}',
            '{$enfermeiro}',
            '{$valor_enfermeiro}',
            '{$recebido}',
            '{$forma_pagamento}',
            '{$observacao}',
            '{$empresa}',
            '{$seraCobrada}',
            '{$justificativaNaoCobrar}',
            '{$contaRecebido}',
            '{$recebidoEm}'
          )


SQL;
        $r = mysql_query($sql);
        if (!$r) {
            echo "Erro ao finalizar Evento/APH";
            mysql_query('ROLLBACK');
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));


            if(!empty($itens['catalogo_id'])) {

                foreach ($itens['catalogo_id'] as $key => $catalogoId) {
                    $tipo = $itens['tipo'][$key];
                    $tiss = $itens['tiss'][$key];
                    $qtd = $itens['qtd'][$key];

                    $sql = <<<SQL
INSERT INTO
`evento_aph_itens`
(
evento_aph_id,
catalogo_id,
tipo,
tiss,
qtd
)
 VALUES (
 $idEventoAPH,
 $catalogoId,
 $tipo,
 $tiss,
 $qtd
 );
SQL;
                    $r = mysql_query($sql);
                    if (!$r) {
                        echo "Erro ao salvar itens usados no Evento/APH";
                        mysql_query('ROLLBACK');
                        return;
                    }
                    savelog(mysql_escape_string(addslashes($sql)));

                }
            }
        mysql_query('commit');
        return  1;

    }

    public static function getCadastroCompletoById($id)
    {
        $sql = <<<SQL
        SELECT
        evento_aph.*,
        evento_aph_finalizar.*
        FROM
        evento_aph inner JOIN
        evento_aph_finalizar on (evento_aph.id = evento_aph_finalizar.evento_aph_id)
        WHERE
        evento_aph.id = {$id}
SQL;
        return parent::get($sql, 'Assoc');

    }

    public static function getItensEventosAPHById($id)
    {
        $sql = <<<SQL
SELECT
  CONCAT('(', evento_aph_itens.tiss COLLATE utf8_general_ci, ') ', catalogo.principio, ' ', catalogo.apresentacao) as item,
  evento_aph_itens.tipo,
  evento_aph_itens.qtd
FROM
  evento_aph_itens
  INNER JOIN catalogo ON evento_aph_itens.catalogo_id = catalogo.ID
WHERE
  evento_aph_itens.evento_aph_id = {$id} and
  evento_aph_itens.tipo in (0,1,3)
  UNION
  SELECT
  CONCAT('(', evento_aph_itens.tiss, ') ', cobrancaplanos.item) as item,
  evento_aph_itens.tipo,
  evento_aph_itens.qtd
FROM
  evento_aph_itens
  INNER JOIN cobrancaplanos ON evento_aph_itens.catalogo_id = cobrancaplanos.id
WHERE
  evento_aph_itens.evento_aph_id = {$id} and evento_aph_itens.tipo = 5
SQL;

        return parent::get($sql, 'Assoc');

    }

    public static function cancelarById($id)
    {
        session_start();
        $sql = <<<SQL
        UPDATE
        evento_aph
        SET
        `cancelado_em` = now(),
        `cancelado_por` = '{$_SESSION['id_user']}'
        WHERE
        id = {$id}
SQL;
        $r = mysql_query($sql);
        if (!$r) {
            echo "Erro ao cancelar Evento/APH";
            return;
        }
        savelog(mysql_escape_string(addslashes($sql)));
        echo 1;
    }

    public static function getEventosAPHFinalizados($inicio, $fim, $fornecedorCliente, $empresaId, $tipoServico)
    {

        $condFornecedorCliente = empty($fornecedorCliente) ? '' : " AND evento_aph_finalizar.fornecedor_cliente = {$fornecedorCliente}";
        $condEmpressa = empty($empresaId) ? "" : " AND evento_aph_finalizar.empresa_id ={$empresaId}";
        $condTipoServico = empty($tipoServico) ? "" : "AND evento_aph.tipo_servico = '{$tipoServico}'";



        $sql = <<<SQL
SELECT
evento_aph.*,
evento_aph_finalizar.*,
fornecedores.FISICA_JURIDICA,
fornecedores.nome AS nomeCliente,
fornecedores.razaoSocial AS razaoSocial,
fornecedores.cnpj AS CNPJ,
fornecedores.CPF,
fornecedores.contato AS contatoCliente,
fornecedores.telefone AS telefoneCliente,
fornecedores.email AS emailCliente,
fornecedores.endereco AS enderecoCliente,
cidades.NOME AS cidade,
cidades.UF,
fornecedores.IES,
fornecedores.IM AS IMRG,
empresas.nome as unidade_regional
FROM
evento_aph
INNER JOIN evento_aph_finalizar ON evento_aph.id = evento_aph_finalizar.evento_aph_id
LEFT JOIN fornecedores ON evento_aph_finalizar.fornecedor_cliente = fornecedores.idFornecedores
LEFT JOIN cidades ON fornecedores.CIDADE_ID = cidades.ID
Left JOIN empresas on evento_aph_finalizar.empresa_id = empresas.id
WHERE
evento_aph.data BETWEEN '{$inicio} 00:00:00' AND '{$fim} 23:59:59'
AND evento_aph.cancelado_por IS NULL
AND evento_aph.finalizado_por IS NOT NULL
$condTipoServico
$condEmpressa
$condFornecedorCliente
SQL;

        return parent::get($sql);

}


}