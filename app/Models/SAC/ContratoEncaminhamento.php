<?php

namespace App\Models\SAC;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class ContratoEncaminhamento extends AbstractModel implements DAOInterface
{

    public static function getAll()
    {
        return parent::get(self::getSQL());
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : null;
        return "SELECT * FROM contrato_encaminhamento WHERE 1 = 1 {$id} ORDER BY id";
    }


}
