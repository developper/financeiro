<?php

namespace App\Models\SAC;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class EncaminhamentoMedicoConduta extends AbstractModel implements DAOInterface
{

  public static function getAll()
  {
    return parent::get(self::getSQL());
  }

  public static function getById($id)
  {
    return current(parent::get(self::getSQL($id)));
  }

  public static function getSQL($id = null)
  {
    $id = isset($id) && !empty($id) ? 'AND encaminhamento_medico = "' . $id . '"' : null;
    return "SELECT * FROM encaminhamento_medico_conduta WHERE 1 = 1 {$id} ORDER BY id";
  }

} 