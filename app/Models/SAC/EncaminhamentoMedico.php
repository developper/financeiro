<?php

namespace App\Models\SAC;

use \App\Controllers\SendGrid;
use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class EncaminhamentoMedico extends AbstractModel implements DAOInterface
{

    public static function getAll()
    {
        return parent::get(self::getSQL());
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : null;
        return "SELECT * FROM encaminhamento_medico WHERE 1 = 1 {$id} ORDER BY id";
    }

    public static function listEncaminhamentos($filters)
    {
        $compPaciente = '';
        $compContrato = '';
        $comTipo = '';
        if($filters['tipo'] == 'interno' ){
            $compPaciente = $filters['paciente'] != '' ? " AND ecm.paciente = '{$filters['paciente']}'" : "";
            $comTipo = " AND ecm.tipo = 'interno' ";
        }elseif($filters['tipo'] == 'externo' ){
            $compContrato = $filters['paciente'] != '' ? " AND ecm.contrato_encaminhamento_id = '{$filters['contrato']}'" : "";
            $comTipo = " AND ecm.tipo = 'externo' ";
        }

        $sql = <<<SQL
SELECT 
  (
    SELECT
      encaminhamento_medico_mudanca_status.status
    FROM
      encaminhamento_medico_mudanca_status
    WHERE
      encaminhamento_medico = ecm.id
    ORDER BY id DESC LIMIT 1
  ) AS status_encaminhamento,
  IF(ecm.paciente is not null and  ecm.paciente > 0 , clientes.nome, ecm.nome_paciente) AS nome_paciente,
  tb1.nome AS atualizado_por,
  usuarios.nome AS criado_por,
  ecm.id,
  ecm.inicio,
  ecm.fim,
  ce.nome as contrato,
  ecm.tipo,
  tb1.data_hora

FROM 
  encaminhamento_medico AS ecm
  LEFT JOIN clientes ON ecm.paciente = clientes.idClientes
  INNER JOIN usuarios ON ecm.created_by = usuarios.idUsuarios
  LEFT JOIN contrato_encaminhamento as ce on ecm.contrato_encaminhamento_id = ce.id
  LEFT JOIN (
    SELECT
		usuarios.nome,
		encaminhamento_medico_mudanca_status.data_hora,
		encaminhamento_medico_mudanca_status.encaminhamento_medico
	FROM
		encaminhamento_medico_mudanca_status
	INNER JOIN usuarios ON encaminhamento_medico_mudanca_status.usuario = usuarios.idUsuarios
	WHERE
		1
GROUP BY encaminhamento_medico_mudanca_status.encaminhamento_medico
	ORDER BY
		id DESC

  ) as tb1 on ecm.id = tb1.encaminhamento_medico
WHERE
  (
    ecm.inicio BETWEEN '{$filters['inicio']} 00:00:01' AND '{$filters['fim']} 23:59:59'
    OR ecm.fim BETWEEN '{$filters['inicio']} 00:00:01' AND '{$filters['fim']} 23:59:59'
  )
  {$compPaciente}
  {$compContrato}
  {$comTipo}
ORDER BY 
  ecm.inicio DESC
SQL;

        return parent::get($sql, 'assoc');
    }

    public static function viewEncaminhamento($id)
    {
        $sql = <<<SQL
SELECT 
  (
    SELECT
      encaminhamento_medico_mudanca_status.status
    FROM
      encaminhamento_medico_mudanca_status
    WHERE
      encaminhamento_medico = ecm.id
    ORDER BY id DESC LIMIT 1
  ) AS status_encaminhamento,
   IF(ecm.paciente is not null and  ecm.paciente > 0 , clientes.nome, ecm.nome_paciente) AS nome_paciente,
  usuarios.nome AS medico,
  ecm.id,
  ecm.queixa_outros,
  ecm.problema_relatado,
  encaminhamento_medico_conduta.conduta,
  encaminhamento_medico_conduta.medico_plantonista,
  encaminhamento_medico_conduta.remocao,
  encaminhamento_medico_conduta.conduta_medica,
  ce.nome as contrato,
  ecm.tipo,
  ecm.inicio,
  ecm.fim
FROM 
  encaminhamento_medico AS ecm
  LEFT JOIN clientes ON ecm.paciente = clientes.idClientes
  LEFT JOIN encaminhamento_medico_conduta ON encaminhamento_medico_conduta.encaminhamento_medico = ecm.id
  LEFT JOIN usuarios ON encaminhamento_medico_conduta.medico_plantonista = usuarios.idUsuarios
  LEFT JOIN contrato_encaminhamento as ce on ecm.contrato_encaminhamento_id = ce.id
WHERE
  ecm.id = '{$id}'
GROUP BY 
  ecm.id
SQL;

        $response = current(parent::get($sql, 'assoc'));

        $sql = <<<SQL
SELECT 
  descricao
FROM 
  encaminhamento_medico_queixas
  INNER JOIN tipos_queixa ON encaminhamento_medico_queixas.queixa = tipos_queixa.id
WHERE
  encaminhamento_medico_queixas.encaminhamento_medico = '{$id}'
SQL;
        $result = parent::get($sql, 'assoc');
        $response['queixas'] = '';
        foreach ($result as $queixa) {
            $response['queixas'] .= $queixa['descricao'] . ', ';
        }

        $response['queixas'] = substr_replace($response['queixas'], '.', '-2', 1);

        return $response;
    }

    public static function gerarProtocoloAtendimento($data)
    {
        mysql_query('BEGIN');

        $queixa_outros = '';

        if(in_array('18', $data['queixas'])) {
            $queixa_outros = $data['queixa_outros'];
        }

        $sql = <<<SQL
INSERT INTO encaminhamento_medico 
(
  id,
  paciente,
  queixa_outros,
  problema_relatado,
  inicio,
  fim,
  created_at,
  created_by,
  contrato_encaminhamento_id,
  tipo,
  nome_paciente
) VALUES (
  NULL,
  '{$data['paciente']}',
  '{$queixa_outros}',
  '{$data['problemas_relatados']}',
  NOW(),
  '',
  NOW(),
  '{$_SESSION['id_user']}',
  '{$data['contrato']}',
  '{$data['tipo']}',
  '{$data['nome-paciente']}'

)
SQL;
        $rs = mysql_query($sql);

        if(!$rs) {
            mysql_query('ROLLBACK');
            return [0, 'Houve um erro ao gerar o protocolo deste atendimento!'];
        }

        $protocolo = mysql_insert_id();

        foreach ($data['queixas'] as $queixa) {
            $sql = <<<SQL
INSERT INTO encaminhamento_medico_queixas
(
  id,
  encaminhamento_medico,
  queixa
) VALUES (
  NULL,
  '{$protocolo}',
  '{$queixa}'
)
SQL;
            $rs = mysql_query($sql);

            if(!$rs) {
                mysql_query('ROLLBACK');
                return [0, 'Houve um erro ao salvar uma queixa deste atendimento!'];
            }
        }

        mysql_query('COMMIT');

        return [$protocolo, ''];
    }

    public static function salvarCondutaEncaminhamento($data)
    {
        mysql_query('BEGIN');

        $houve_remocao = '';
        $conduta_medica = '';

        if($data['conduta'] == '1') {
            $conduta_medica = $data['conduta_medica'];
        } else {
            $houve_remocao = $data['houve_remocao'];
        }

        $sql = <<<SQL
INSERT INTO encaminhamento_medico_conduta
(
  id,
  encaminhamento_medico,
  medico_plantonista,
  conduta,
  conduta_medica,
  remocao
) VALUES (
  NULL,
  '{$data['protocolo']}',
  '{$data['medico']}',
  '{$data['conduta']}',
  '{$conduta_medica}',
  '{$houve_remocao}'
)
SQL;
        $rs = mysql_query($sql);

        if(!$rs) {
            mysql_query('ROLLBACK');
            return 'Houve um erro ao salvar a conduta deste atendimento!';
        }

        $sql = <<<SQL
INSERT INTO encaminhamento_medico_mudanca_status
(
  id,
  encaminhamento_medico,
  status,
  usuario,
  data_hora
) VALUES (
  NULL,
  '{$data['protocolo']}',
  '{$data['status_encaminhamento']}',
  '{$_SESSION['id_user']}',
  NOW()
)
SQL;
        $rs = mysql_query($sql);

        if(!$rs) {
            mysql_query('ROLLBACK');
            return 'Houve um erro ao salvar a mudança de status deste atendimento!';
        }

        if($data['status_encaminhamento'] == '1') {
            $sql = <<<SQL
UPDATE encaminhamento_medico SET fim = NOW() WHERE id = '{$data['protocolo']}'
SQL;
            $rs = mysql_query($sql);

            if(!$rs) {
                mysql_query('ROLLBACK');
                return 'Houve um erro ao atualizar o fim deste atendimento!';
            }
        }

        return mysql_query('COMMIT');
    }

    public static function atualizarCondutaEncaminhamento($data)
    {
        mysql_query('BEGIN');



        $houve_remocao = '';
        $conduta_medica = '';

        if($data['conduta'] == '1') {
            $conduta_medica = $data['conduta_medica'];
        } else {
            $houve_remocao = $data['houve_remocao'];
        }

        $hasConduta = EncaminhamentoMedicoConduta::getById($data['protocolo']);


        if(!$hasConduta) {
            $sql = <<<SQL
INSERT INTO encaminhamento_medico_conduta
(
  id,
  encaminhamento_medico,
  medico_plantonista,
  conduta,
  conduta_medica,
  remocao
) VALUES (
  NULL,
  '{$data['protocolo']}',
  '{$data['medico']}',
  '{$data['conduta']}',
  '{$conduta_medica}',
  '{$houve_remocao}'
)
SQL;
            $rs = mysql_query($sql);

            if(!$rs) {
                mysql_query('ROLLBACK');
                return 'Houve um erro ao salvar a conduta deste atendimento!';
            }
        } else {
            $sql = <<<SQL
UPDATE encaminhamento_medico_conduta SET 
  medico_plantonista = '{$data['medico']}',
  conduta = '{$data['conduta']}',
  conduta_medica = '{$conduta_medica}',
  remocao = '{$houve_remocao}'
WHERE 
  encaminhamento_medico = '{$data['protocolo']}'
SQL;
            $rs = mysql_query($sql);

            if (!$rs) {
                mysql_query('ROLLBACK');
                return 'Houve um erro ao atualizar a conduta deste atendimento!';
            }
        }

        $sql = <<<SQL
INSERT INTO encaminhamento_medico_mudanca_status
(
  id,
  encaminhamento_medico,
  status,
  usuario,
  data_hora
) VALUES (
  NULL,
  '{$data['protocolo']}',
  '{$data['status_encaminhamento']}',
  '{$_SESSION['id_user']}',
  NOW()
)
SQL;
        $rs = mysql_query($sql);

        if(!$rs) {
            mysql_query('ROLLBACK');
            return 'Houve um erro ao salvar a mudança de status deste atendimento!';
        }

        if($data['status_encaminhamento'] == '1') {
            $sql = <<<SQL
UPDATE encaminhamento_medico SET fim = NOW() WHERE id = '{$data['protocolo']}'
SQL;
            $rs = mysql_query($sql);

            if(!$rs) {
                mysql_query('ROLLBACK');
                return 'Houve um erro ao atualizar o fim deste atendimento!';
            }
            $dadosEncaminhamento = self::viewEncaminhamento($data['protocolo']);
            $finalizadoEm = \DateTime::createFromFormat('Y-m-d H:i:s', $dadosEncaminhamento['fim'])->format('d/m/Y H:i:s');
            $inicio = \DateTime::createFromFormat('Y-m-d H:i:s', $dadosEncaminhamento['inicio'])->format('d/m/Y H:i:s');

            $titulo = "Encaminamento SAC - Médico ECM-{$data['protocolo']} Finalizado";
            $texto = "<p>Encaminamento SAC - Médico ECM-{$data['protocolo']} foi finalizado por
            {$_SESSION['nome_user']} em {$finalizadoEm}.</p>";
            $texto .=  "<table width='100%' border='1px' bordercolor='black'>
                                <tr style='text-aling: center;'>
                                    <th>Tipo</th>
                                    <th>Paciente</th>
                                    <th>Inicio</th>
                                </tr>
                                <tr>
                                    <td>".ucfirst($dadosEncaminhamento['tipo'])."</td>
                                    <td>{$dadosEncaminhamento['nome_paciente']}</td>
                                    <td>{$inicio}</td>
                                </tr>
                            </table>";
            $para[] = ['email' => 'enf.sac@mederi.com.br', 'nome'=> 'Coordenação SAC'];
            SendGrid::enviarEmailSimples($titulo, $texto,$para);
        }

        return mysql_query('COMMIT');
    }
}
