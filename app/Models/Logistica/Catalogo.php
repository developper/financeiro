<?php
namespace app\Models\Logistica;

use App\Models\AbstractModel;
use App\Models\DAOInterface;

class Catalogo extends AbstractModel implements DAOInterface
{
    public static function getAll ()
    {
        return parent::get (self::getSQL ());
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? "WHERE ID = {$id}" : null;
        return "SELECT * FROM catalogo {$id} ORDER BY ID";
    }

    public static function getByEANFornecedorProdId($ean, $fornecedor, $prodId)
    {
        $sql = <<<SQL
SELECT
  catalogo.ID,
  CONCAT( catalogo.principio, ' ', catalogo.apresentacao, '<b> LAB:</b> ',lab_desc) AS nomeItem,
  catalogo.tipo
FROM
  catalogo_codigo_barras
  INNER JOIN catalogo ON catalogo_codigo_barras.catalogo_id = catalogo.ID
WHERE
  catalogo_codigo_barras.codigo_barra = '{$ean}'
  AND catalogo_codigo_barras.fornecedor_id = '{$fornecedor}'
  AND catalogo_codigo_barras.produto_fornecedor_id = '{$prodId}'
  AND catalogo.ATIVO = 'A'
LIMIT 1
SQL;

        $result = current(parent::get($sql));
        if(count($result) > 0){
            return $result;
        }
        return false;
    }

    public static function getByItemAndTipo($item, $tipo)
    {
        $principio = '';
        $join = '';
        if($tipo == 0){
            $principio = ",' (',principio_ativo.principio,') '";
            $join = "INNER JOIN catalogo_principio_ativo ON catalogo.ID = catalogo_principio_ativo.catalogo_id
LEFT JOIN principio_ativo ON principio_ativo.id = catalogo_principio_ativo.principio_ativo_id";
        }
        $sql = <<<SQL
SELECT
  concat('#',catalogo.id,' ', catalogo.principio,' ',catalogo.apresentacao,' LAB: ',catalogo.lab_desc $principio) as value,
  concat('#',catalogo.id,' ', catalogo.principio,' ',catalogo.apresentacao,' LAB: ',catalogo.lab_desc $principio) as nomeItem,
  catalogo.ID,
  estoque.empresas_id_1 AS estoque
FROM
catalogo
INNER JOIN estoque ON catalogo.ID = estoque.CATALOGO_ID
$join
WHERE
catalogo.ATIVO = 'A' AND
catalogo.tipo = $tipo AND
concat(catalogo.principio,' ',catalogo.apresentacao $principio) like '%{$item}%'
ORDER BY catalogo.principio

SQL;
        $result = parent::get($sql);
        return $result;
    }

    public static function getEquipamentoByTerm($term)
    {

        $sql = <<<SQL
SELECT
  item as value,
  id AS ID
FROM 
  cobrancaplanos 
WHERE 
tipo != '0' AND
item like '%{$term}%'
ORDER BY 
  item

SQL;
        $result = parent::get($sql);
        return $result;
    }

    public static function getItensByTipo($tipo)
    {
        $sql = <<<SQL
SELECT 
  ID AS cod, 
  NUMERO_TISS, 
  CONCAT(principio, ' - ', apresentacao, ' LAB: ', lab_desc) AS nome 
FROM 
  catalogo 
WHERE tipo = '{$tipo}'
  AND ATIVO = 'A'
ORDER BY 
  nome
SQL;
        if($tipo == '5') {
            $sql = <<<SQL
SELECT 
  id AS cod, 
  item AS nome 
FROM 
  cobrancaplanos 
WHERE 
  tipo = 1
ORDER BY
  nome
SQL;
        }

        $result = parent::get($sql, 'Assoc');
        return $result;
    }

    public static function getCategoriasMateriais($id = '')
    {
        $id = $id != '' ? " AND ID = '{$id}'" : "";
        $sql = <<<SQL
SELECT 
  categorias_materiais.*
FROM
  categorias_materiais
WHERE
  1 = 1
  {$id}
SQL;
        return parent::get($sql, 'assoc');
    }
}
