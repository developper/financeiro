<?php
namespace app\Models\Logistica;

use App\Models\AbstractModel;
use App\Models\DAOInterface;

class Entrada extends AbstractModel implements DAOInterface
{
    public static function getAll ()
    {
        return parent::get (self::getSQL ());
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? "WHERE id = {$id}" : null;
        return "SELECT * FROM entrada {$id} ORDER BY id";
    }

    public static function getCustoItensEntrada($filters)
    {
        $compFornecedor = $filters['fornecedor'] != '' ?
            " AND notas.codFornecedor = '{$filters['fornecedor']}'" :
            "";

        $empresaPrincipal = $_SESSION['empresa_principal'];

        if($empresaPrincipal == '1') {
            $compUr       = $filters['ur'] != '' ?
                " AND entrada.UR = '{$filters['ur']}'" :
                "";
        } else {
            $compUr = " AND entrada.UR = '{$empresaPrincipal}'";
        }

        $sql = <<<SQL
SELECT
  entrada.data AS dataEntrada,
  CONCAT(catalogo.principio, ' ', catalogo.apresentacao, '<br><b> LAB:</b> ',lab_desc) AS item,
  entrada.qtd,
  fornecedores.razaoSocial,
  entrada.valor
FROM
  entrada
  INNER JOIN catalogo ON entrada.CATALOGO_ID = catalogo.ID
  INNER JOIN notas ON entrada.nota = notas.idNotas
  INNER JOIN fornecedores ON notas.codFornecedor = fornecedores.idFornecedores
WHERE
  entrada.ATIVO = 'true'
  AND entrada.data BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}' 
  {$compFornecedor}
  {$compUr}
ORDER BY 
  dataEntrada DESC
SQL;

        return parent::get($sql, 'Assoc');
    }

    public static function getNotasEntrada($filters)
    {
        $compFornecedor = $filters['fornecedor'] != '' ?
            " AND notas.codFornecedor = '{$filters['fornecedor']}'" :
            "";

        $empresaPrincipal = $_SESSION['empresa_principal'];

        if($empresaPrincipal == '1') {
            $compUr       = $filters['ur'] != '' ?
                " AND entrada.UR = '{$filters['ur']}'" :
                "";
        } else {
            $compUr = " AND entrada.UR = '{$empresaPrincipal}'";
        }

        $sql = <<<SQL
SELECT
  notas.idNotas AS TR,
  notas.codigo AS numeroNota,
  entrada.data AS dataEntrada,
  CONCAT(catalogo.principio, ' ', catalogo.apresentacao, '<br><b> LAB:</b> ',lab_desc) AS item,
  entrada.qtd,
  entrada.LOTE,
  fornecedores.razaoSocial,
  entrada.valor AS custo,
  notas.valor AS valorNota
FROM
  entrada
  INNER JOIN catalogo ON entrada.CATALOGO_ID = catalogo.ID
  INNER JOIN notas ON entrada.nota = notas.idNotas
  INNER JOIN fornecedores ON notas.codFornecedor = fornecedores.idFornecedores
WHERE
  entrada.ATIVO = 'true'
  AND entrada.data BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}' 
  {$compFornecedor}
  {$compUr}
ORDER BY 
  dataEntrada DESC
SQL;

        return parent::get($sql, 'Assoc');
    }
}