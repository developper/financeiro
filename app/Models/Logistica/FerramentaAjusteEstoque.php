<?php
namespace app\Models\Logistica;

use App\Models\AbstractModel,
    App\Models\DAOInterface;


class FerramentaAjusteEstoque extends AbstractModel implements DAOInterface
{
    public static function getSQL($id = null)
    {

    }

    public static function updateEntrada($item)
    {
        $lote = strtoupper($item['lote']);
        $sql = <<<SQL
INSERT
        entrada

        (   nota,
    fornecedor,
    item,
    CATALOGO_ID,
    qtd,
    valor,
    data,
    LOTE,
    VALIDADE,
    DES_BONIFICACAO_ITEM,
    CFOP,
    USUARIO_ID,
    CANCELADO,
    DATA_CANCELADO,
    CANCELADO_POR,
    JUSTIFICATIVA,
    CODIGO_REFERENCIA,
    TIPO_AQUISICAO,
    UR,
    TIPO,
    ATIVO)
    VALUES
    (
        'CORREÇÃO ESTOQUE',
        2129,
        0,
        {$item['catalogoId']},
        {$item['quantidade']},
        0.00,
        now(),
        '{$lote}',
        '{$item['validade']}',
        0,
        0,
        {$_SESSION["id_user"]},
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        {$_SESSION["empresa_principal"]},
        0,
        'true'
    )
SQL;

        $response = mysql_query($sql);
        return $response;
    }

    public static function updateEstoque($item)
    {
        $qtdu = 'empresas_id_' . $_SESSION['empresa_principal']; // nome do campo da respectiva UR na tabela estoque
        $sql = <<<SQL
UPDATE
	estoque
SET
 	{$qtdu} = {$qtdu} + '{$item['quantidade']}'
WHERE
	CATALOGO_ID = '{$item['catalogoId']}'
	AND TIPO = 0
SQL;
        $response = mysql_query($sql);
        return $response;

    }

}