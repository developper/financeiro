<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 02/05/2017
 * Time: 09:34
 */

namespace app\Models\Logistica;

use App\Models\AbstractModel;


class SaidaAntibioticoPaciente extends AbstractModel
{
    public static function getSaida($filters)
    {

        $condPaciente = " AND saida.idCliente = {$filters['paciente']}";
        if( $filters['paciente'] == 'T'){
            $condPaciente = " AND saida.UR = 0";
        }

        $sql = <<<SQL
SELECT
saida.CATALOGO_ID,
CONCAT(catalogo.principio, ' - ', catalogo.apresentacao) AS item,
principio_ativo.principio,
saida.quantidade AS qtd,
'Medicamento' as tipo,
catalogo.lab_desc as Laboratorio,
catalogo.ANTIBIOTICO,
catalogo.GENERICO,
DATE_FORMAT(saida.data,'%d/%m/%Y') as dataSaida,
clientes.nome
FROM
saida
INNER JOIN catalogo ON catalogo.ID = saida.CATALOGO_ID
INNER JOIN clientes ON saida.idCliente = clientes.idClientes
LEFT JOIN catalogo_principio_ativo ON catalogo_principio_ativo.catalogo_id = catalogo.ID
LEFT JOIN principio_ativo ON principio_ativo.id = catalogo_principio_ativo.principio_ativo_id
where
saida.tipo = 0
AND saida.data BETWEEN '{$filters['inicioUS']}' AND '{$filters['fimUS']}'
AND catalogo.ANTIBIOTICO in('S','s')
$condPaciente
Order by clientes.nome,saida.data,principio_ativo.principio
SQL;


        return parent::get($sql, 'assoc');
    }

}