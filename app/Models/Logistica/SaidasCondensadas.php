<?php

namespace app\Models\Logistica;

use App\Models\AbstractModel;

class SaidasCondensadas extends AbstractModel
{


    public static function getMedicamento($filters)
    {

        $sql = <<<SQL
SELECT
  saida.CATALOGO_ID,
  CONCAT(catalogo.principio, ' - ', catalogo.apresentacao) AS nome,
  SUM(saida.quantidade) AS qtd,
 'Medicamento' as tipo,
 catalogo.lab_desc as Laboratorio,
principio_ativo.principio,
catalogo.TAG as tag,
catalogo.valorMedio as custo
FROM
  saida
  INNER JOIN catalogo ON catalogo.ID = saida.CATALOGO_ID
  LEFT JOIN catalogo_principio_ativo ON catalogo_principio_ativo.catalogo_id = catalogo.ID
  LEFT JOIN principio_ativo ON principio_ativo.id = catalogo_principio_ativo.principio_ativo_id
where
 saida.tipo = 0
AND saida.data BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
AND saida.idCliente = 0
GROUP BY CATALOGO_ID
order by principio,nome
SQL;

        return parent::get($sql, 'assoc');
    }

    public static function getMaterial($filters)
    {

        $sql = <<<SQL
SELECT
  saida.CATALOGO_ID,
  CONCAT(catalogo.principio, ' - ', catalogo.apresentacao) AS nome,
  SUM(saida.quantidade) AS qtd,
 'Material' as tipo,
 catalogo.lab_desc as Laboratorio,
'' as principio,
catalogo.TAG as tag,
catalogo.valorMedio as custo
FROM
  saida
  INNER JOIN catalogo ON catalogo.ID = saida.CATALOGO_ID
  LEFT JOIN catalogo_principio_ativo ON catalogo_principio_ativo.catalogo_id = catalogo.ID
  LEFT JOIN principio_ativo ON principio_ativo.id = catalogo_principio_ativo.principio_ativo_id
where
 saida.tipo = 1
AND saida.data BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
AND saida.idCliente = 0
GROUP BY CATALOGO_ID
order by principio,nome
SQL;


         return parent::get($sql, 'assoc');
    }

    public static function getDieta($filters)
    {


        $sql = <<<SQL
SELECT
  saida.CATALOGO_ID,
  CONCAT(catalogo.principio, ' - ', catalogo.apresentacao) AS nome,
  SUM(saida.quantidade) AS qtd,
 'Dieta' as tipo,
 catalogo.lab_desc as Laboratorio,
'' as principio,
catalogo.TAG as tag,
catalogo.valorMedio as custo

FROM
  saida
  INNER JOIN catalogo ON catalogo.ID = saida.CATALOGO_ID
  LEFT JOIN catalogo_principio_ativo ON catalogo_principio_ativo.catalogo_id = catalogo.ID
  LEFT JOIN principio_ativo ON principio_ativo.id = catalogo_principio_ativo.principio_ativo_id
where
 saida.tipo = 3
AND saida.data BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
AND saida.idCliente = 0
GROUP BY CATALOGO_ID
order by principio,nome
SQL;

        return parent::get($sql, 'assoc');
    }





    public static function getEquipamento($filters)
    {


    $sql = <<<SQL
SELECT

saida.CATALOGO_ID ,
cobrancaplanos.item AS nome,
SUM(saida.quantidade) AS qtd,
'Equipamento' as tipo,
'' as Laboratorio,
'' as principio,
'' as tag,
0 as custo
FROM
saida
INNER JOIN cobrancaplanos ON (cobrancaplanos.id = saida.CATALOGO_ID)
WHERE
 saida.tipo = 5
AND saida.data BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
AND idCliente = 0
GROUP BY CATALOGO_ID
ORDER BY
  `nome`
SQL;

    return parent::get($sql, 'assoc');
}

}