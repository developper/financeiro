<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 17/10/2017
 * Time: 10:25
 */

namespace app\Models\Logistica;
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/codigos.php');



use App\Models\AbstractModel;
use \App\Controllers\SendGrid;
use App\Models\Logistica\Catalogo;
use App\Models\Sistema\Equipamentos;


class DevolucaoInterna extends AbstractModel
{
    public static function salvarDevolucaoInterna($arrayCatalogoId,
                                                    $arrayTipo,
                                                    $arrayValidade,
                                                    $arrayQuantidade,
                                                    $arrayObservacao,
                                                    $arrayLote,
                                                    $arrayMotivo)
    {
        mysql_query('begin');

        $sql = <<<SQL
        INSERT INTO
        devolucao_interna_pedido
(

`created_by`,
`created_at`,
`ur_origem`,
`ur_destino`,
status
)
  VALUES
(
{$_SESSION['id_user']},
now(),
{$_SESSION['empresa_principal']},
1,
'PENDENTE'
)

SQL;


        $result = mysql_query($sql);
        if (!$result) {
            $msg = "Erro ao salvar pedido de devolução interna.".$sql;
            mysql_query('rollback');
            return $msg;
        }
        $devolucaoInternaPedidoId = mysql_insert_id();
        savelog(mysql_escape_string(addslashes($sql)));

        foreach($arrayCatalogoId as $key => $catalogoId) {

            $observacao = $arrayObservacao[$key];
            $tipo = $arrayTipo[$key];
            $validade = $arrayValidade[$key];
            $quantidade = $arrayQuantidade[$key];
            $lote = $arrayLote[$key];
            $motivo = $arrayMotivo[$key];




            $sqlItem = <<<SQL
        INSERT INTO
        devolucao_interna_itens
(

`catalogo_id`,
`tipo`,
`lote`,
`vencimento`,
`quantidade`,
`observacao`,
devolucao_interna_pedido_id,
devolucao_interna_motivo_id
)
  VALUES
(
$catalogoId,
$tipo,
'{$lote}',
'{$validade}',
'{$quantidade}',
'{$observacao}',
$devolucaoInternaPedidoId,
'$motivo'
)

SQL;


            $resultItem = mysql_query($sqlItem);
            if (!$resultItem) {
                $msg = "Erro ao salvar item do pedido de devolução.";
                mysql_query('rollback');
                return $msg;
            }
            savelog(mysql_escape_string(addslashes($sqlItem)));


        }
        mysql_query('commit');
        $dataEnvio = date('d/m/Y');
        $titulo = "Pedido de Devolução Interna #{$devolucaoInternaPedidoId}  da  UR  {$_SESSION["nome_empresa"]} ";
        $texto = "Pedido de Devolução Internar <b>#{$devolucaoInternaPedidoId}</b> da UR {$_SESSION["nome_empresa"]},
                    feito por {$_SESSION["nome_user"]} em  {$dataEnvio}";
        $para[] = ['email' => 'fabiosantana@assistevida.com', 'nome' => 'Fábio Santana'];

        SendGrid::enviarEmailSimples($titulo, $texto, $para);
        $msg = 1;
        return $msg;

        }

    public static function getByFilters($empresaId,$inicio,$fim)
    {
        $cond = '';
        if(isset($empresaId) && !empty($empresaId) ){
            $cond = "AND devolucao_interna_pedido.ur_origem = {$empresaId} ";
        }
        $sql = <<<SQL
        SELECT
empresas.nome as empresa,
usuarios.nome as usuario,
devolucao_interna_pedido.created_at,
devolucao_interna_pedido.id,
devolucao_interna_pedido.created_by,
devolucao_interna_pedido.ur_origem,
devolucao_interna_pedido.ur_destino,
devolucao_interna_pedido.canceled_by,
devolucao_interna_pedido.canceled_at,
devolucao_interna_pedido.motivo_cancelamento,
devolucao_interna_pedido.status
FROM
devolucao_interna_pedido
INNER JOIN usuarios ON devolucao_interna_pedido.created_by = usuarios.idUsuarios
INNER JOIN empresas ON devolucao_interna_pedido.ur_origem = empresas.id
WHERE
DATE_FORMAT(devolucao_interna_pedido.created_at, '%Y-%m-%d') BETWEEN '{$inicio}' and '{$fim}'
{$cond}
SQL;

        return parent::get($sql);

    }

    public static function getPedidoById($id)
    {
        $sql = <<<SQL
        SELECT
devolucao_interna_pedido.*,
empresas.nome as empresa,
usuarios.nome as usuario,
uAceitou.nome as usuarioQueAceitou,
uEnviou.nome as usuarioQueEnviou,
uConfirmou.nome as usuarioQueConfirmou,
devolucao_interna_pedido.motivo_cancelamento

FROM
devolucao_interna_pedido
INNER JOIN usuarios ON devolucao_interna_pedido.created_by = usuarios.idUsuarios
INNER JOIN empresas ON devolucao_interna_pedido.ur_origem = empresas.id
LEFT JOIN usuarios as uAceitou ON devolucao_interna_pedido.accepted_by = uAceitou.idUsuarios
LEFT JOIN usuarios as uEnviou ON devolucao_interna_pedido.sent_by = uEnviou.idUsuarios
LEFT JOIN usuarios as uConfirmou ON devolucao_interna_pedido.confirmed_by = uConfirmou.idUsuarios
WHERE
devolucao_interna_pedido.id = {$id}
SQL;
        return parent::get($sql);


    }

    public static function getItensByIdPedido($id)
    {
        $sql = <<<SQL
        SELECT

devolucao_interna_itens.*,
devolucao_interna_itens.id as idItem,
devolucao_interna_motivo.motivo
FROM
devolucao_interna_itens
INNER JOIN devolucao_interna_motivo ON devolucao_interna_itens.devolucao_interna_motivo_id = devolucao_interna_motivo.id
WHERE
devolucao_interna_itens.devolucao_interna_pedido_id = {$id}
SQL;
        return parent::get($sql);


    }

    public static function getMotivos($id = null)
    {
        $cond = empty($id) ? 1 : "id = {$id}";
        $sql = <<<SQL
        SELECT
devolucao_interna_motivo.*
FROM
devolucao_interna_motivo
WHERE
$cond
SQL;
        return parent::get($sql);
    }

    public static function salvarAutorizacaoDevolucaoInterna($pedidoId,
                                                             $arrayQuantidadeAutorizada,
                                                             $arrayDevolucaoInternaItem,
                                                             $urOrigem)
    {



        mysql_query('begin');

        $sql = <<<SQL
        UPDATE
        devolucao_interna_pedido
        SET
        accepted_by = {$_SESSION['id_user']},
        accepted_at = now(),
        status = 'AUTORIZADO'
        WHERE
        id = {$pedidoId}



SQL;


        $result = mysql_query($sql);
        if (!$result) {
            $msg = "Erro ao atualizar a devolução interna.";
            mysql_query('rollback');
            return $msg;
        }
        savelog(mysql_escape_string(addslashes($sql)));

        foreach($arrayDevolucaoInternaItem as $key => $itemId){
            $sql = <<<SQL
UPDATE
devolucao_interna_itens
SET
autorizado = $arrayQuantidadeAutorizada[$key]
WHERE
id =$itemId
SQL;
            $result = mysql_query($sql);
            if (!$result) {
                $msg = "Erro ao atualizar o item {$itemId} do pedido de  devolução interna.".$sql;
                mysql_query('rollback');
                return $msg;
            }
            savelog(mysql_escape_string(addslashes($sql)));

        }

        mysql_query('commit');
        $dataEnvio = date('d/m/Y');
        $titulo = "Autorização do Pedido de Devolução Interna #{$pedidoId}. ";
        $texto = "Pedido de Devolução Internar <b>#{$pedidoId}</b> da UR {$_SESSION["nome_empresa"]},
                    foi Autorizado por {$_SESSION["nome_user"]} em  {$dataEnvio}<br>
                    Verifique quais itens você pode enviar.";
        $configEmail = require $_SERVER['DOCUMENT_ROOT'] . '/app/configs/email.php';
        $para[] = $configEmail['lista']['emails']['logistica'][$urOrigem];

        SendGrid::enviarEmailSimples($titulo, $texto, $para);
        $msg = 1;
        return $msg;

    }



    public static function salvarEnvioItensDevolucaoInterna($pedidoId,
                                                            $arrayQuantidadeEnviar,
                                                            $arrayDevolucaoInternaItem,
                                                            $urOrigem,
                                                            $urDestino,
                                                            $arrayDevolucaoInternaItemTipo,
                                                            $arrayDevolucaoInternaItemCatalogo)
    {



        mysql_query('begin');

        $sql = <<<SQL
        UPDATE
        devolucao_interna_pedido
        SET
        sent_by = {$_SESSION['id_user']},
        sent_at = now(),
        status = 'ENVIADO'
        WHERE
        id = {$pedidoId}



SQL;


        $result = mysql_query($sql);
        if (!$result) {
            $msg = "Erro ao atualizar a devolução interna.";
            mysql_query('rollback');
            return $msg;
        }
        savelog(mysql_escape_string(addslashes($sql)));

        foreach($arrayDevolucaoInternaItem as $key => $itemId){
            $sql = <<<SQL
UPDATE
devolucao_interna_itens
SET
enviado = $arrayQuantidadeEnviar[$key]
WHERE
id =$itemId
SQL;
            $result = mysql_query($sql);
            if (!$result) {
                $msg = "Erro ao atualizar o item {$itemId} do pedido de  devolução interna.";
                mysql_query('rollback');
                return $msg;
            }
            savelog(mysql_escape_string(addslashes($sql)));

            $sql = "UPDATE estoque SET empresas_id_{$urOrigem} = (empresas_id_{$urOrigem} - $arrayQuantidadeEnviar[$key])
                    WHERE CATALOGO_ID = '{$arrayDevolucaoInternaItemCatalogo[$key]}' and TIPO = $arrayDevolucaoInternaItemTipo[$key] ";
            if(!mysql_query($sql)) {

                echo "Error ao ajustar estoque do item {$arrayDevolucaoInternaItem[$key]} ";
                mysql_query("rollback");
                return;
            }
            savelog(mysql_escape_string(addslashes($sql)));

        }



        mysql_query('commit');
        $dataEnvio = date('d/m/Y');
        $titulo = "Envio dos itens do Pedido de Devolução Interna #{$pedidoId}. ";
        $texto = "Pedido de Devolução Internar <b>#{$pedidoId}</b> da UR {$_SESSION["nome_empresa"]},
                    foi enviado por {$_SESSION["nome_user"]} em  {$dataEnvio}<br>
                    Verifique quais itens foram enviados.";
        $configEmail = require $_SERVER['DOCUMENT_ROOT'] . '/app/configs/email.php';
        $para[] = $configEmail['lista']['emails']['logistica'][$urDestino];

        SendGrid::enviarEmailSimples($titulo, $texto, $para);
        $msg = 1;
        return $msg;

    }

    public static function salvarConfirmarcaoEntregaItensDevolucaoInterna($pedidoId,
                                                                          $arrayQuantidadeConfirmada,
                                                                          $arrayDevolucaoInternaItem,
                                                                          $urOrigem,
                                                                          $urDestino,
                                                                          $arrayDevolucaoInternaItemTipo,
                                                                          $arrayDevolucaoInternaItemCatalogo,
                                                                          $status)
    {


// Começo da transação.
        mysql_query('begin');


        $labelStatus = $status == 'F'? 'FINALIZADO': 'CONFIRMADO PARCIALMENTE';
// Atualiza status do pedido da Devolução Interna
        $sql = <<<SQL
        UPDATE
        devolucao_interna_pedido
        SET
        confirmed_by = {$_SESSION['id_user']},
        confirmed_at = now(),
        status = '{$labelStatus}'
        WHERE
        id = {$pedidoId}
SQL;
        $result = mysql_query($sql);
        if (!$result) {
            $msg = "Erro ao atualizar a devolução interna.";
            mysql_query('rollback');
            return $msg;
        }
        savelog(mysql_escape_string(addslashes($sql)));
// Atualiza os itens do pedido da Devolução Interna.

        foreach($arrayDevolucaoInternaItem as $key => $itemId){
// Só atualiza os itens que a quantidade confirmada seja maior ou igual a zero.
            if($arrayQuantidadeConfirmada[$key] != '') {
                $sql = <<<SQL
UPDATE
devolucao_interna_itens
SET
confirmado = IF($arrayQuantidadeConfirmada[$key] = 0 , $arrayQuantidadeConfirmada[$key],
                    IF(($arrayQuantidadeConfirmada[$key]+ COALESCE(confirmado,0)) = 0, '',($arrayQuantidadeConfirmada[$key]+ COALESCE(confirmado,0))))
WHERE
id =$itemId
SQL;
                $result = mysql_query($sql);
                if (!$result) {
                    $msg = "Erro ao atualizar o item {$itemId} do pedido de  devolução interna.";
                    mysql_query('rollback');
                    return $msg;
                }
                savelog(mysql_escape_string(addslashes($sql)));
// Só atualiza o estoque da Ur que esta confirmando o recebimento do item.
                if ($arrayQuantidadeConfirmada[$key] > 0) {
                    $sql = "UPDATE estoque SET empresas_id_{$urDestino} = (empresas_id_{$urDestino} + $arrayQuantidadeConfirmada[$key])
                    WHERE CATALOGO_ID = '{$arrayDevolucaoInternaItemCatalogo[$key]}' and TIPO = $arrayDevolucaoInternaItemTipo[$key] ";
                    if (!mysql_query($sql)) {

                        echo "Error ao ajustar estoque do item {$arrayDevolucaoInternaItem[$key]} ";
                        mysql_query("rollback");
                        return;
                    }
                    savelog(mysql_escape_string(addslashes($sql)));

                }
// Grava um histórico das confirmações
                $sql = "
           Insert into
           `historico_devolucao_interna_item_confirmacao`
           (
           `devolucao_interna_item_id`  ,
            `created_at`,
            `created_by`,
            `quantidade`
           )
           values
           (
              {$itemId} ,
              now(),
              {$_SESSION['id_user']},
              $arrayQuantidadeConfirmada[$key]
            )";
                if (!mysql_query($sql)) {

                    echo "Error ao gravar o histórico do item {$arrayDevolucaoInternaItem[$key]} " ;
                    mysql_query("rollback");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));


            }
        }
// Fim atualiza os itens que a quantidade confirmada seja maior ou igual a zero.
// Caso o pedido seja Finalizado
        if($status == 'F'){
            $textoTr = '';

            $itensPedido = self::getItensByIdPedido($pedidoId);

            foreach($itensPedido as $item){
// Caso o item do pedido não seja confirmado ou confimado parcialmente, o item deve ser devolvido ao estoque da Unidade de Origem
                if($item['enviado'] > 0 && $item['confirmado'] != $item['enviado'] ){
                    $quantidadeDevolver = $item['enviado'] - $item['confirmado'];
                    $sql = "UPDATE
                            estoque
                            SET
                            empresas_id_{$urOrigem} = (empresas_id_{$urOrigem} + $quantidadeDevolver)
                    WHERE
                    CATALOGO_ID = '{$item['catalogo_id']}' and
                    TIPO = '{$item['tipo']}' ";

                    if (!mysql_query($sql)) {
                        echo "Erro ao devolver item ao estoque de origem.";
                        mysql_query("rollback");
                        return;
                    }
                    savelog(mysql_escape_string(addslashes($sql)));

                    $lote = strtoupper($item['lote']);
                    $sqlEntrada = <<<SQL
INSERT
        entrada

        (   nota,
    fornecedor,
    item,
    CATALOGO_ID,
    qtd,
    valor,
    data,
    LOTE,
    VALIDADE,
    DES_BONIFICACAO_ITEM,
    CFOP,
    USUARIO_ID,
    CANCELADO,
    DATA_CANCELADO,
    CANCELADO_POR,
    JUSTIFICATIVA,
    CODIGO_REFERENCIA,
    TIPO_AQUISICAO,
    UR,
    TIPO,
    ATIVO)
    VALUES
    (
        'CORREÇÃO ESTOQUE DEVOLUÇÃO INTERNA #{$pedidoId}',
        2129,
        0,
        {$item['catalogo_id']},
        {$item['quantidade']},
        0.00,
        now(),
        '{$lote}',
        '{$item['vencimento']}',
        0,
        0,
        {$_SESSION["id_user"]},
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        {$urOrigem},
        {$item['tipo']},
        'true'
    )
SQL;
                    if (!mysql_query($sqlEntrada)) {
                        echo "Erro ao ajustar entrada.";
                        mysql_query("rollback");
                        return;
                    }
                    savelog(mysql_escape_string(addslashes($sqlEntrada)));



// Caso o item seja não equipamento
                   if($item['tipo'] != 5) {
                       $responseCatalogo = Catalogo::getById($item['catalogo_id']);
                       $nomeItem =  $responseCatalogo['principio'].' '.
                                    $responseCatalogo['apresentacao'].' Lab: '.
                                    $responseCatalogo['lab_desc'];
                        $validade = \DateTime::createFromFormat('Y-m-d', $item['vencimento'])->format('d/m/Y');

                        $textoTr .= "<tr>
                                        <td>$nomeItem</td>
                                        <td>{$item['lote']}</td>
                                        <td>{$validade}</td>
                                        <td>{$item['quantidade']}</td>
                                    </tr>";

                   }else{
// Caso o item seja  equipamento
                       $responseEquipamento = Equipamentos::getById($item['catalogo_id']);
                       $validade = \DateTime::createFromFormat('Y-m-d', $item['vencimento'])->format('d/m/Y');
                       $textoTr .= "<tr>
                                        <td>{$responseEquipamento['item']}</td>
                                        <td>{$item['lote']}</td>
                                        <td>{$validade}</td>
                                        <td>{$item['quantidade']}</td>
                                    </tr>";

                   }

                }
            }
// E-mail de aviso para a unidade de origem que o pedido de Devolução foi Finalizado
            $dataEnvio = date('d/m/Y');
            $mensagem = "<p>O pedido de Devolução Interna #{$pedidoId} foi finalizado.<p>
                     <p> Finalizado por {$_SESSION["nome_user"]} em  {$dataEnvio}<p>";
// Caso algum item foi devolvido automaticamente para o estoque da Ur de origem.
            if(!empty($textoTr)){
                $mensagem .= "<p>Foram devolvidos para seu estoque os seguintes itens:<p>
                            <table width='100%' border='1px' bordercolor='black'>
                            <tr style='text-aling: center;'>
                                <th>ITEM</th>
                                <th>LOTE</th>
                                <th>VALIDADE</th>
                                <th>QUANTIDADE</th>
                            </tr>
                            {$textoTr}
                            </table>";
            }
            $titulo = "Pedido de Devolução Interna #{$pedidoId} Finalizado. ";
            $texto = $mensagem;
            $configEmail = require $_SERVER['DOCUMENT_ROOT'] . '/app/configs/email.php';
            $para[] = $configEmail['lista']['emails']['logistica'][$urOrigem];
            SendGrid::enviarEmailSimples($titulo, $texto, $para);
        }

        mysql_query('commit');

        $msg = 1;
        return $msg;

    }

    public static function cancelarPedidoDevolucaoInterna($pedidoId, $justificativaCancelar)
    {
        mysql_query('begin');

        $sql = <<<SQL
        UPDATE
        devolucao_interna_pedido
        SET
        canceled_by = {$_SESSION['id_user']},
        canceled_at = now(),
        status = 'CANCELADO',
        motivo_cancelamento = '{$justificativaCancelar}'
        WHERE
        id = {$pedidoId}



SQL;


        $result = mysql_query($sql);
        if (!$result) {
            $msg = "Erro ao cancelar a devolução interna.";
            mysql_query('rollback');
            return $msg;
        }
        savelog(mysql_escape_string(addslashes($sql)));
        mysql_query('commit');
        return 1;
    }

    public static function getHistoricoConfirmacaoByPedido($devolucaoInternaId)
    {
        $sql = " SELECT
usuarios.nome as usuario,
historico_devolucao_interna_item_confirmacao.quantidade,
DATE_FORMAT(historico_devolucao_interna_item_confirmacao.created_at,'%d/%m/%Y %H:%i') as dataBr,
devolucao_interna_itens.id as idPedidoItem
FROM
devolucao_interna_pedido
INNER JOIN devolucao_interna_itens ON devolucao_interna_pedido.id = devolucao_interna_itens.devolucao_interna_pedido_id
INNER JOIN historico_devolucao_interna_item_confirmacao ON devolucao_interna_itens.id = historico_devolucao_interna_item_confirmacao.devolucao_interna_item_id
INNER JOIN usuarios ON historico_devolucao_interna_item_confirmacao.created_by = usuarios.idUsuarios
WHERE
devolucao_interna_pedido.id = $devolucaoInternaId
";
        return parent::get($sql);


    }



}