<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 02/10/2017
 * Time: 09:46
 */

namespace app\Models\Logistica;

require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/codigos.php');

use App\Models\AbstractModel;




class PedidosInternos extends AbstractModel
{
	public static function getBySql($sql)
	{
		return parent::get($sql);

	}
    public static function getMatMedDieEnviadoNaoConfimandoPorPedidoETipo($idPedido,$tipo)
    {
        $sql = "SELECT
        i.QTD ,
        i.ENVIADO ,
        i.CONFIRMADO,
        i.ID,
        i.PEDIDOS_INTERNO_ID,
        i.NUMERO_TISS,
        i.CATALOGO_ID,
        b.principio,
		b.apresentacao,
		e.principio AS principioAtivo,
		b.lab_desc,
		i.TIPO,
		(case i.TIPO
		WHEN 0 THEN  'Medicamento'
		WHEN 1 THEN 'Material'
		WHEN 3 THEN  'Dieta'
		END ) as descricaoTipo

		FROM
		itenspedidosinterno AS i INNER JOIN
		pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID ) INNER JOIN
		catalogo AS b ON ( b.ID = i.CATALOGO_ID )
		LEFT JOIN catalogo_principio_ativo AS d ON d.catalogo_id = b.ID
			LEFT JOIN principio_ativo AS e ON e.id = d.principio_ativo_id
		WHERE
		(i.STATUS = 'P'	OR
                (i.STATUS = 'F' and i.CONFIRMADO < i.ENVIADO and  i.CANCELADO='N' and  i.CANCELADO_ENTRADA='N')
                OR (i.STATUS = 'F' and i.CANCELADO='S' and i.CONFIRMADO <> i.ENVIADO and  i.CANCELADO_ENTRADA='N'))
                and i.TIPO = {$tipo}
                AND p.ID = {$idPedido}

		ORDER BY principio, apresentacao";

        return $sql;
    }

    public static function getEquipamentoEnviadoNaoConfimandoPorPedido($idPedido)
    {
        $sql =
            "SELECT
	i.QTD ,
	i.ENVIADO ,
	i.CONFIRMADO,
	i.ID,
	i.PEDIDOS_INTERNO_ID,
	i.NUMERO_TISS,
	i.CATALOGO_ID,
	cb.item as principio,
	'' as apresentacao,
	'' as principioAtivo,
	'' as lab_desc,
	i.TIPO,
	'Equipamento' as descricaoTipo

	FROM
	itenspedidosinterno AS i INNER JOIN
	pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID )
						INNER JOIN cobrancaplanos as cb ON ( cb.id = i.CATALOGO_ID )
						WHERE

					 (i.STATUS = 'P'
                        OR (i.STATUS = 'F' and i.CONFIRMADO < i.ENVIADO and  i.CANCELADO='N' and  i.CANCELADO_ENTRADA='N')
                        OR (i.STATUS = 'F' and i.CANCELADO='S' and i.CONFIRMADO <> i.ENVIADO and  i.CANCELADO_ENTRADA='N'))
                        and i.TIPO = 5
                        AND
                        p.ID = {$idPedido}
							ORDER BY cb.item";
        return $sql;
    }

    public static function getPedidosEnviadosENaoConfirmadosPorUr($empresa)
    {

        $sql = "SELECT
s.*
FROM
(
SELECT
						p.ID,
						DATE_FORMAT(p.DATA, '%d/%m/%Y') as DATA,
						U.nome,DATE_FORMAT(p.DATA, '%H:%i:%S') as hora,
						(
							(
								CASE WHEN COUNT( * ) >1 THEN
									SUM(i.ENVIADO )
								ELSE
									i.ENVIADO END
							) -
							(
								CASE WHEN COUNT( * ) >1 THEN
									sum(i.CONFIRMADO)
								ELSE
									i.CONFIRMADO END
							)
						) AS itens

		FROM
			itenspedidosinterno AS i INNER JOIN
			pedidosinterno AS p ON ( p.ID = i.PEDIDOS_INTERNO_ID ) inner join
			usuarios AS U on (U.idUsuarios = p.USUARIO) inner join
			empresas AS E on (p.EMPRESA = E.id)
		WHERE
			(
				i.STATUS = 'P'	OR
                                (
					i.STATUS = 'F' AND
					i.CONFIRMADO < i.ENVIADO AND
					i.CANCELADO='N' AND
					i.CANCELADO_ENTRADA='N'
				)
                OR
				(
					i.STATUS = 'F' AND
					i.CANCELADO='S' AND
					i.CONFIRMADO < i.ENVIADO AND
					i.CANCELADO_ENTRADA='N'
				)
			) AND
			p.EMPRESA ={$empresa}  AND
			i.CATALOGO_ID <> 0
                GROUP BY
			p.ID
		ORDER BY
			p.ID
			)as s
WHERE
s.itens >0";
        return parent::get($sql);
    }

	public static function getItensMatMedDiePedidosInternosSaida($idItemPedidoInterno){

		$sql = "SELECT
CONCAT(catalogo.principio,' ',catalogo.apresentacao) as item,
catalogo.lab_desc,
saida.tipo,
saida.CATALOGO_ID,
saida.quantidade,
saida.idSaida,
saida.`data`,
saida.LOTE,
date_format(saida.data, '%d/%m/%Y') as data_br,
saida.modelo,
saida.confirmacao_pedidointerno,
e.principio AS principioAtivo,
saida.ITENSPEDIDOSINTERNOS_ID
FROM
saida
INNER JOIN catalogo ON saida.CATALOGO_ID = catalogo.ID
LEFT JOIN catalogo_principio_ativo AS d ON d.catalogo_id = catalogo.ID
LEFT JOIN principio_ativo AS e ON e.id = d.principio_ativo_id
WHERE
saida.ITENSPEDIDOSINTERNOS_ID = {$idItemPedidoInterno}
 and
 saida.modelo = 'v2'
 and
 saida.confirmacao_pedidointerno < saida.quantidade
order by item asc";
		return $sql;

	}

	public static function getItensEquipamentosPedidosInternosSaida($idItemPedidoInterno){

		$sql = "SELECT
cobrancaplanos.item,
'' as lab_desc,
saida.tipo,
saida.CATALOGO_ID,
saida.quantidade,
saida.idSaida,
saida.`data`,
saida.LOTE,
date_format(saida.data, '%d/%m/%Y') as data_br,
saida.modelo,
saida.confirmacao_pedidointerno,
'' AS principioAtivo,
saida.ITENSPEDIDOSINTERNOS_ID
FROM
saida
INNER JOIN cobrancaplanos ON saida.CATALOGO_ID = cobrancaplanos.id

WHERE
saida.ITENSPEDIDOSINTERNOS_ID = {$idItemPedidoInterno}
  and
 saida.modelo = 'v2'
 and
 saida.confirmacao_pedidointerno < saida.quantidade
order by item asc";

		return $sql;

	}

	public static function cancelarItemEnviadoNaoConfirmado($arrayItens)
	{
		mysql_query('begin');

		foreach($arrayItens as $item) {

			$quantidadeCancelar = $item['cancelar'];
			$itenPedidoInternoId = $item['itens-pedido-id'];
			$saidaId = $item['saida-id'];
			$tipo = $item['tipo'];
			$catalogoId = $item['catalogo-id'];
			$pedidoId = $item['pedido-id'];


			$sql = "Insert into
historico_cancelar_item_pedido_interno_enviado
(
 `saida_id`,
`quantidade`,
`cancelado_por`,
`cancelado_em`,
pedido_interno_id,
item_pedido_interno_id
)
VALUES
(

{$saidaId},
{$quantidadeCancelar},
{$_SESSION['id_user']},
now(),
{$pedidoId},
{$itenPedidoInternoId}
)";
			$result = mysql_query($sql);
			if (!$result) {
				$msg = "Erro ao salvar histórico do cancelamento.";
				mysql_query('rollback');
				return $msg;
			}
			savelog(mysql_escape_string(addslashes($sql)));

			$sqlSaida = "
UPDATE
saida
set
quantidade = quantidade - {$quantidadeCancelar}
WHERE
idSaida = {$saidaId}";

			$resultSaida = mysql_query($sqlSaida);
			if (!$resultSaida) {
				$msg = "Erro ao fazer update em saida.";
				mysql_query('rollback');
				return $msg;
			}
			savelog(mysql_escape_string(addslashes($sqlSaida)));


			$sqlItenPedidoInterno = "
UPDATE
 itenspedidosinterno
 set
 ENVIADO = ENVIADO - {$quantidadeCancelar}

 WHERE
 ID = {$itenPedidoInternoId}

";
			$resultItenPedidoInterno = mysql_query($sqlItenPedidoInterno);
			if (!$resultItenPedidoInterno) {
				$msg = "Erro ao fazer update em itens pedidos interno.";
				mysql_query('rollback');
				return $msg;
			}
			savelog(mysql_escape_string(addslashes($sqlItenPedidoInterno)));

$sqlStatusPedidoInterno = "
			UPDATE
 itenspedidosinterno as i
 set
 i.STATUS = 'P'
			WHERE
  	  		i.STATUS = 'F'
			AND (i.QTD - i.ENVIADO) > 0
			AND i.ID = $itenPedidoInternoId
			AND i.CANCELADO = 'N'
			AND i.CANCELADO_ENTRADA = 'N'
			";
			$resultStatusPedidoInterno  = mysql_query($sqlStatusPedidoInterno );
			if (!$resultStatusPedidoInterno ) {
				$msg = "Erro ao fazer update no status do iten do pedido interno.";
				mysql_query('rollback');
				return $msg;
			}
			savelog(mysql_escape_string(addslashes($sqlStatusPedidoInterno )));

			$sqlEstoque = "
UPDATE
estoque
SET
empresas_id_1 = empresas_id_1 + {$quantidadeCancelar}
 WHERE
 CATALOGO_ID = $catalogoId
 AND
 TIPO = $tipo";

			$resultEstoque = mysql_query($sqlEstoque);
			if (!$resultEstoque) {
				$msg = "Erro ao fazer update em estoque." . $sqlEstoque;
				mysql_query('rollback');
				return $msg;
			}
			savelog(mysql_escape_string(addslashes($sqlEstoque)));
		}
			mysql_query('commit');

		return 1;

	}



}