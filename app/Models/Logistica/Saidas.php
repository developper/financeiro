<?php

namespace app\Models\Logistica;

use App\Models\AbstractModel;

class Saidas extends AbstractModel
{
    public static function getRelatorioSaidas($filters)
    {
        switch ($filters['tipo_busca']) {
            case "paciente":
                $sql = self::getSQLSaidaByPaciente($filters);
                break;
            case "ur":
                $sql = self::getSQLSaidaByUR($filters);
                break;
            case $filters['tipo_busca'] ==  "med" || $filters['tipo_busca'] ==  "mat" || $filters['tipo_busca'] ==  "die":
                $sql = self::getSQLSaidaByMatMedDie($filters);
                break;
            case $filters['tipo_busca'] ==  "eqp":
                $sql = self::getSQLSaidaByEquipamento($filters);
                break;
        }

        return parent::get($sql, 'assoc');
    }

    private static function getSQLSaidaByPaciente($filters)
    {
        $groupBy = " GROUP BY saida.data, saida.CATALOGO_ID";
        if($filters['tipoRelatorio'] == 'sintetico'){
            $groupBy = " GROUP BY saida.CATALOGO_ID ";
        }
        $comp = " saida.idCliente = '{$filters['paciente']}' ";
        $sql = <<<SQL
SELECT 
  clientes.nome AS paciente, 
  COALESCE(usuarios.nome, 'Pedido de Emergencia') AS auditora, 
  saida.NUMERO_TISS,
  saida.CATALOGO_ID,
  CONCAT(catalogo.principio, ' - ', catalogo.apresentacao, ' LAB: ', catalogo.lab_desc) AS nome,
  saida.LOTE, 
  SUM(saida.quantidade) AS qtd,
  saida.data,
  CASE
  WHEN saida.tipo = '0' THEN 'Medicamento'
  WHEN saida.tipo = '1' THEN 'Material'
  WHEN saida.tipo = '3' THEN 'Dieta'
  END AS tipoItem,
  (
    CASE 
	WHEN solicitacoes.TIPO_SOLICITACAO IN (2, 5, 9) THEN 'Periodica'
	WHEN solicitacoes.TIPO_SOLICITACAO IN (3, 6, 8) THEN 'Aditiva'
	WHEN solicitacoes.TIPO_SOLICITACAO = -1  THEN 'Avulsa'
	WHEN solicitacoes.TIPO_SOLICITACAO = -2  THEN 'Emergencia'
	END
  ) AS tipoSolicitacao,
  (
	CASE
	WHEN solicitacoes.status = 2 THEN ' - Entrega Imediata'
	ELSE ''
	END
  ) AS imediata,
  IF(solicitacoes.idPrescricao NOT IN (-1, -2), CONCAT('De ', DATE_FORMAT(prescricoes.inicio, '%d/%m/%Y'), ' a ', DATE_FORMAT(prescricoes.fim, '%d/%m/%Y')), 'Sem Período') AS periodo,
  IF(solicitacoes.TIPO_SOLICITACAO IN (-1, -2), IF(JUSTIFICATIVA_AVULSO_ID != 11, justificativa_solicitacao_avulsa.justificativa, solicitacoes.JUSTIFICATIVA_AVULSO), 'Sem Justificativa') AS justAvulsa,
  COALESCE(justificativa_itens_negados.JUSTIFICATIVA, 'Sem Justificativa') AS justNegItem,
  IF(
    solicitacoes.autorizado > 0, 1, 
    IF(solicitacoes.status = -1, 0, 1)
  ) AS liberado,
  solicitacoes.data_auditado AS auditado 
FROM 
  saida 
  INNER JOIN clientes ON clientes.idClientes = saida.idCliente
  INNER JOIN catalogo ON catalogo.ID = saida.CATALOGO_ID
  INNER JOIN solicitacoes ON saida.idSolicitacao = solicitacoes.idSolicitacoes
  LEFT JOIN prescricoes ON solicitacoes.idPrescricao = prescricoes.id
  LEFT JOIN usuarios ON solicitacoes.AUDITOR_ID = usuarios.idUsuarios
  LEFT JOIN justificativa_solicitacao_avulsa ON solicitacoes.JUSTIFICATIVA_AVULSO_ID = justificativa_solicitacao_avulsa.id
  LEFT JOIN justificativa_itens_negados ON solicitacoes.JUSTIFICATIVA_ITENS_NEGADOS_ID = justificativa_itens_negados.ID
WHERE 
  {$comp} 
  AND saida.tipo IN (0, 1, 3) 
  AND saida.data BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}' 
  AND saida.UR = 0
{$groupBy}
UNION ALL

    SELECT
      clientes.nome AS paciente, 
      COALESCE(usuarios.nome, 'Pedido de Emergencia') AS auditora, 
      saida.numero_tiss,
      saida.CATALOGO_ID,
      cobrancaplanos.item AS nome, 
      saida.LOTE, 
      SUM(saida.quantidade) AS qtd,
      saida.data,
      'Equipamento' tipoItem,
      (
        CASE 
        WHEN solicitacoes.TIPO_SOLICITACAO IN (2, 5, 9) THEN 'Periodica'
        WHEN solicitacoes.TIPO_SOLICITACAO IN (3, 6, 8) THEN 'Aditiva'
        WHEN solicitacoes.TIPO_SOLICITACAO = -1  THEN 'Avulsa'
        WHEN solicitacoes.TIPO_SOLICITACAO = -2  THEN 'Emergencia'
        END
      ) AS tipoSolicitacao,
      (
        CASE
        WHEN solicitacoes.status = 2 THEN ' - Entrega Imediata'
        ELSE ''
        END
      ) AS imediata,
      IF(solicitacoes.idPrescricao NOT IN (-1, -2), CONCAT('De ', DATE_FORMAT(prescricoes.inicio, '%d/%m/%Y'), ' a ', DATE_FORMAT(prescricoes.fim, '%d/%m/%Y')), 'Sem Período') AS periodo,
      IF(solicitacoes.TIPO_SOLICITACAO IN (-1, -2), IF(JUSTIFICATIVA_AVULSO_ID != 11, justificativa_solicitacao_avulsa.justificativa, solicitacoes.JUSTIFICATIVA_AVULSO), 'Sem Justificativa') AS justAvulsa,
      COALESCE(justificativa_itens_negados.JUSTIFICATIVA, 'Sem Justificativa') AS justNegItem,
      IF(
        solicitacoes.autorizado > 0, 1, 
        IF(solicitacoes.status = -1, 0, 1)
      ) AS liberado,
      solicitacoes.data_auditado AS auditado 
    FROM 
    saida
    INNER JOIN clientes ON clientes.idClientes = saida.idCliente  
    INNER JOIN cobrancaplanos ON (cobrancaplanos.id = saida.CATALOGO_ID) 
    INNER JOIN solicitacoes ON saida.idSolicitacao = solicitacoes.idSolicitacoes
    LEFT JOIN prescricoes ON solicitacoes.idPrescricao = prescricoes.id
    LEFT JOIN usuarios ON solicitacoes.AUDITOR_ID = usuarios.idUsuarios
    LEFT JOIN justificativa_solicitacao_avulsa ON solicitacoes.JUSTIFICATIVA_AVULSO_ID = justificativa_solicitacao_avulsa.id
    LEFT JOIN justificativa_itens_negados ON solicitacoes.JUSTIFICATIVA_ITENS_NEGADOS_ID = justificativa_itens_negados.ID
    WHERE 
      {$comp}
      AND saida.tipo = 5 
      AND saida.data BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}' 
      AND saida.UR = 0
    {$groupBy}
ORDER BY 
  `nome`
SQL;
        return $sql;
    }

    private static function getSQLSaidaByUR($filters)
    {
        $groupBy = " GROUP BY saida.data, saida.CATALOGO_ID";
        if($filters['tipoRelatorio'] == 'sintetico'){
            $groupBy = " GROUP BY saida.CATALOGO_ID ";
        }
        $comp = " clientes.empresa = '{$filters['ur']}' ";

        $sql = <<<SQL
SELECT 
  clientes.nome AS paciente, 
  COALESCE(usuarios.nome, 'Pedido de Emergencia') AS auditora, 
  saida.NUMERO_TISS,
  saida.CATALOGO_ID,
  CONCAT(catalogo.principio, ' - ', catalogo.apresentacao, ' LAB: ', catalogo.lab_desc) AS nome,
  saida.LOTE, 
  SUM(saida.quantidade) AS qtd,
  saida.data,
  CASE
  WHEN saida.tipo = '0' THEN 'Medicamento'
  WHEN saida.tipo = '1' THEN 'Material'
  WHEN saida.tipo = '3' THEN 'Dieta'
  END AS tipoItem,
  (
    CASE 
	WHEN solicitacoes.TIPO_SOLICITACAO IN (2, 5, 9) THEN 'Periodica'
	WHEN solicitacoes.TIPO_SOLICITACAO IN (3, 6, 8) THEN 'Aditiva'
	WHEN solicitacoes.TIPO_SOLICITACAO = -1  THEN 'Avulsa'
	WHEN solicitacoes.TIPO_SOLICITACAO = -2  THEN 'Emergencia'
	END
  ) AS tipoSolicitacao,
  (
	CASE
	WHEN solicitacoes.status = 2 THEN ' - Entrega Imediata'
	ELSE ''
	END
  ) AS imediata,
  IF(solicitacoes.idPrescricao NOT IN (-1, -2), CONCAT('De ', DATE_FORMAT(prescricoes.inicio, '%d/%m/%Y'), ' a ', DATE_FORMAT(prescricoes.fim, '%d/%m/%Y')), 'Sem Período') AS periodo,
  IF(solicitacoes.TIPO_SOLICITACAO IN (-1, -2), IF(JUSTIFICATIVA_AVULSO_ID != 11, justificativa_solicitacao_avulsa.justificativa, solicitacoes.JUSTIFICATIVA_AVULSO), 'Sem Justificativa') AS justAvulsa,
  COALESCE(justificativa_itens_negados.JUSTIFICATIVA, 'Sem Justificativa') AS justNegItem,
  IF(
    solicitacoes.autorizado > 0, 1, 
    IF(solicitacoes.status = -1, 0, 1)
  ) AS liberado,
  solicitacoes.data_auditado AS auditado 
FROM 
  saida 
  INNER JOIN clientes ON clientes.idClientes = saida.idCliente
  INNER JOIN catalogo ON catalogo.ID = saida.CATALOGO_ID
  INNER JOIN solicitacoes ON saida.idSolicitacao = solicitacoes.idSolicitacoes
  LEFT JOIN prescricoes ON solicitacoes.idPrescricao = prescricoes.id
  LEFT JOIN usuarios ON solicitacoes.AUDITOR_ID = usuarios.idUsuarios
  LEFT JOIN justificativa_solicitacao_avulsa ON solicitacoes.JUSTIFICATIVA_AVULSO_ID = justificativa_solicitacao_avulsa.id
  LEFT JOIN justificativa_itens_negados ON solicitacoes.JUSTIFICATIVA_ITENS_NEGADOS_ID = justificativa_itens_negados.ID
WHERE 
  {$comp} 
  AND saida.tipo IN (0, 1, 3) 
  AND saida.data BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}' 
  AND saida.UR = 0
{$groupBy}
UNION ALL

    SELECT
      clientes.nome AS paciente, 
      COALESCE(usuarios.nome, 'Pedido de Emergencia') AS auditora, 
      saida.numero_tiss,
      saida.CATALOGO_ID,
      cobrancaplanos.item AS nome, 
      saida.LOTE, 
      SUM(saida.quantidade) AS qtd,
      saida.data,
      'Equipamento' tipoItem,
      (
        CASE 
        WHEN solicitacoes.TIPO_SOLICITACAO IN (2, 5, 9) THEN 'Periodica'
        WHEN solicitacoes.TIPO_SOLICITACAO IN (3, 6, 8) THEN 'Aditiva'
        WHEN solicitacoes.TIPO_SOLICITACAO = -1  THEN 'Avulsa'
        WHEN solicitacoes.TIPO_SOLICITACAO = -2  THEN 'Emergencia'
        END
      ) AS tipoSolicitacao,
      (
        CASE
        WHEN solicitacoes.status = 2 THEN ' - Entrega Imediata'
        ELSE ''
        END
      ) AS imediata,
      IF(solicitacoes.idPrescricao NOT IN (-1, -2), CONCAT('De ', DATE_FORMAT(prescricoes.inicio, '%d/%m/%Y'), ' a ', DATE_FORMAT(prescricoes.fim, '%d/%m/%Y')), 'Sem Período') AS periodo,
      IF(solicitacoes.TIPO_SOLICITACAO IN (-1, -2), IF(JUSTIFICATIVA_AVULSO_ID != 11, justificativa_solicitacao_avulsa.justificativa, solicitacoes.JUSTIFICATIVA_AVULSO), 'Sem Justificativa') AS justAvulsa,
      COALESCE(justificativa_itens_negados.JUSTIFICATIVA, 'Sem Justificativa') AS justNegItem,
      IF(
        solicitacoes.autorizado > 0, 1, 
        IF(solicitacoes.status = -1, 0, 1)
      ) AS liberado,
        solicitacoes.data_auditado AS auditado 
    FROM 
    saida
    INNER JOIN clientes ON clientes.idClientes = saida.idCliente  
    INNER JOIN cobrancaplanos ON (cobrancaplanos.id = saida.CATALOGO_ID) 
    INNER JOIN solicitacoes ON saida.idSolicitacao = solicitacoes.idSolicitacoes
    LEFT JOIN prescricoes ON solicitacoes.idPrescricao = prescricoes.id
    LEFT JOIN usuarios ON solicitacoes.AUDITOR_ID = usuarios.idUsuarios
    LEFT JOIN justificativa_solicitacao_avulsa ON solicitacoes.JUSTIFICATIVA_AVULSO_ID = justificativa_solicitacao_avulsa.id
    LEFT JOIN justificativa_itens_negados ON solicitacoes.JUSTIFICATIVA_ITENS_NEGADOS_ID = justificativa_itens_negados.ID
    WHERE 
      {$comp}
      AND saida.tipo = 5 
      AND saida.data BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}' 
      AND saida.UR = 0
    {$groupBy}
ORDER BY 
  `nome`
SQL;

        return $sql;
    }

    private static function getSQLSaidaByMatMedDie($filters)
    {
        $urPaciente = $_SESSION['empresa_principal'] == 1 ? '11' : $_SESSION['empresa_principal'];
        $comp = " saida.CATALOGO_ID = '{$filters['item']}' ";
        $qtd = " saida.quantidade as qtd, ";
        $groupBy = " ";
        if($filters['tipoRelatorio'] == 'sintetico'){
            $qtd = " SUM(saida.quantidade) AS qtd, ";
            $groupBy = " GROUP BY saida.CATALOGO_ID ";
        }

        $sql = <<<SQL
SELECT 
  clientes.nome AS paciente, 
  COALESCE(usuarios.nome, 'Pedido de Emergencia') AS auditora, 
  saida.NUMERO_TISS,
  saida.CATALOGO_ID,
  CONCAT(catalogo.principio, ' - ', catalogo.apresentacao, ' LAB: ', catalogo.lab_desc) AS nome,
  saida.LOTE, 
  {$qtd}
  saida.data,
  CASE
  WHEN saida.tipo = '0' THEN 'Medicamento'
  WHEN saida.tipo = '1' THEN 'Material'
  WHEN saida.tipo = '3' THEN 'Dieta'
  END AS tipoItem,
  (
    CASE 
	WHEN solicitacoes.TIPO_SOLICITACAO IN (2, 5, 9) THEN 'Periodica'
	WHEN solicitacoes.TIPO_SOLICITACAO IN (3, 6, 8) THEN 'Aditiva'
	WHEN solicitacoes.TIPO_SOLICITACAO = -1  THEN 'Avulsa'
	WHEN solicitacoes.TIPO_SOLICITACAO = -2  THEN 'Emergencia'
	END
  ) AS tipoSolicitacao,
  (
	CASE
	WHEN solicitacoes.status = 2 THEN ' - Entrega Imediata'
	ELSE ''
	END
  ) AS imediata,
  IF(solicitacoes.idPrescricao NOT IN (-1, -2), CONCAT('De ', DATE_FORMAT(prescricoes.inicio, '%d/%m/%Y'), ' a ', DATE_FORMAT(prescricoes.fim, '%d/%m/%Y')), 'Sem Período') AS periodo,
  IF(solicitacoes.TIPO_SOLICITACAO IN (-1, -2), IF(JUSTIFICATIVA_AVULSO_ID != 11, justificativa_solicitacao_avulsa.justificativa, solicitacoes.JUSTIFICATIVA_AVULSO), 'Sem Justificativa') AS justAvulsa,
  COALESCE(justificativa_itens_negados.JUSTIFICATIVA, 'Sem Justificativa') AS justNegItem,
  IF(
    solicitacoes.autorizado > 0, 1, 
    IF(solicitacoes.status = -1 IS NOT NULL, 0, 1)
  ) AS liberado,
  solicitacoes.data_auditado AS auditado 
FROM 
  saida 
  INNER JOIN clientes ON clientes.idClientes = saida.idCliente
  INNER JOIN catalogo ON catalogo.ID = saida.CATALOGO_ID
  INNER JOIN solicitacoes ON saida.idSolicitacao = solicitacoes.idSolicitacoes
  LEFT JOIN prescricoes ON solicitacoes.idPrescricao = prescricoes.id
  LEFT JOIN usuarios ON solicitacoes.AUDITOR_ID = usuarios.idUsuarios
  LEFT JOIN justificativa_solicitacao_avulsa ON solicitacoes.JUSTIFICATIVA_AVULSO_ID = justificativa_solicitacao_avulsa.id
  LEFT JOIN justificativa_itens_negados ON solicitacoes.JUSTIFICATIVA_ITENS_NEGADOS_ID = justificativa_itens_negados.ID
WHERE 
  {$comp} 
  AND saida.data BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
  AND saida.tipo IN (0, 1, 3) 
  AND clientes.idClientes IN (
                 SELECT 
                    idClientes 
                 FROM 
                    clientes 
                 WHERE 
                    empresa = '{$urPaciente}'
                ) 
  {$groupBy}
ORDER BY 
  `nome`
SQL;

        if($_SESSION['empresa_principal'] == 1) {
            $sql = <<<SQL
SELECT 
  clientes.nome AS paciente, 
  COALESCE(usuarios.nome, 'Pedido de Emergencia') AS auditora, 
  saida.NUMERO_TISS,
  saida.CATALOGO_ID,
  CONCAT(catalogo.principio, ' - ', catalogo.apresentacao, ' LAB: ', catalogo.lab_desc) AS nome,
  saida.LOTE, 
  saida.quantidade AS qtd,
  saida.data,
  CASE
  WHEN saida.tipo = '0' THEN 'Medicamento'
  WHEN saida.tipo = '1' THEN 'Material'
  WHEN saida.tipo = '3' THEN 'Dieta'
  END AS tipoItem,
  (
    CASE 
	WHEN solicitacoes.TIPO_SOLICITACAO IN (2, 5, 9) THEN 'Periodica'
	WHEN solicitacoes.TIPO_SOLICITACAO IN (3, 6, 8) THEN 'Aditiva'
	WHEN solicitacoes.TIPO_SOLICITACAO = -1  THEN 'Avulsa'
	WHEN solicitacoes.TIPO_SOLICITACAO = -2  THEN 'Emergencia'
	END
  ) AS tipoSolicitacao,
  (
	CASE
	WHEN solicitacoes.status = 2 THEN ' - Entrega Imediata'
	ELSE ''
	END
  ) AS imediata,
  IF(solicitacoes.idPrescricao NOT IN (-1, -2), CONCAT('De ', DATE_FORMAT(prescricoes.inicio, '%d/%m/%Y'), ' a ', DATE_FORMAT(prescricoes.fim, '%d/%m/%Y')), 'Sem Período') AS periodo,
  IF(solicitacoes.TIPO_SOLICITACAO IN (-1, -2), IF(JUSTIFICATIVA_AVULSO_ID != 11, justificativa_solicitacao_avulsa.justificativa, solicitacoes.JUSTIFICATIVA_AVULSO), 'Sem Justificativa') AS justAvulsa,
  COALESCE(justificativa_itens_negados.JUSTIFICATIVA, 'Sem Justificativa') AS justNegItem,
  IF(
    solicitacoes.autorizado > 0, 1, 
    IF(solicitacoes.status = -1, 0, 1)
  ) AS liberado,
  solicitacoes.data_auditado AS auditado 
FROM 
  saida 
  INNER JOIN clientes ON clientes.idClientes = saida.idCliente
  INNER JOIN catalogo ON catalogo.ID = saida.CATALOGO_ID
  INNER JOIN solicitacoes ON saida.idSolicitacao = solicitacoes.idSolicitacoes
  LEFT JOIN prescricoes ON solicitacoes.idPrescricao = prescricoes.id
  LEFT JOIN usuarios ON solicitacoes.AUDITOR_ID = usuarios.idUsuarios
  LEFT JOIN justificativa_solicitacao_avulsa ON solicitacoes.JUSTIFICATIVA_AVULSO_ID = justificativa_solicitacao_avulsa.id
  LEFT JOIN justificativa_itens_negados ON solicitacoes.JUSTIFICATIVA_ITENS_NEGADOS_ID = justificativa_itens_negados.ID
WHERE 
  {$comp} 
  AND saida.data BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
  AND saida.tipo IN (0, 1, 3) 
  AND clientes.idClientes IN (
                 SELECT 
                    idClientes 
                 FROM 
                    clientes 
                 WHERE 
                    empresa = '{$urPaciente}'
                )
UNION ALL

    SELECT
      empresas.nome AS paciente, 
      COALESCE(usuarios.nome, 'Pedido de Emergencia') AS auditora, 
      saida.numero_tiss,
      saida.CATALOGO_ID,
      cobrancaplanos.item AS nome, 
      saida.LOTE, 
      saida.quantidade AS qtd,
      saida.data,
      CASE
      WHEN saida.tipo = '0' THEN 'Medicamento'
      WHEN saida.tipo = '1' THEN 'Material'
      WHEN saida.tipo = '3' THEN 'Dieta'
      END AS tipoItem,
      (
        CASE 
        WHEN solicitacoes.TIPO_SOLICITACAO IN (2, 5, 9) THEN 'Periodica'
        WHEN solicitacoes.TIPO_SOLICITACAO IN (3, 6, 8) THEN 'Aditiva'
        WHEN solicitacoes.TIPO_SOLICITACAO = -1  THEN 'Avulsa'
        WHEN solicitacoes.TIPO_SOLICITACAO = -2  THEN 'Emergencia'
        END
      ) AS tipoSolicitacao,
      (
        CASE
        WHEN solicitacoes.status = 2 THEN ' - Entrega Imediata'
        ELSE ''
        END
      ) AS imediata,
      IF(solicitacoes.idPrescricao NOT IN (-1, -2), CONCAT('De ', DATE_FORMAT(prescricoes.inicio, '%d/%m/%Y'), ' a ', DATE_FORMAT(prescricoes.fim, '%d/%m/%Y')), 'Sem Período') AS periodo,
      IF(solicitacoes.TIPO_SOLICITACAO IN (-1, -2), IF(JUSTIFICATIVA_AVULSO_ID != 11, justificativa_solicitacao_avulsa.justificativa, solicitacoes.JUSTIFICATIVA_AVULSO), 'Sem Justificativa') AS justAvulsa,
      COALESCE(justificativa_itens_negados.JUSTIFICATIVA, 'Sem Justificativa') AS justNegItem,
      IF(
        solicitacoes.autorizado > 0, 1, 
        IF(solicitacoes.status = -1, 0, 1)
      ) AS liberado,
      solicitacoes.data_auditado AS auditado 
    FROM 
    saida
    INNER JOIN empresas ON empresas.id = saida.UR 
    INNER JOIN cobrancaplanos ON (cobrancaplanos.id = saida.CATALOGO_ID) 
    INNER JOIN solicitacoes ON saida.idSolicitacao = solicitacoes.idSolicitacoes
    LEFT JOIN prescricoes ON solicitacoes.idPrescricao = prescricoes.id
    LEFT JOIN usuarios ON solicitacoes.AUDITOR_ID = usuarios.idUsuarios
    LEFT JOIN justificativa_solicitacao_avulsa ON solicitacoes.JUSTIFICATIVA_AVULSO_ID = justificativa_solicitacao_avulsa.id
    LEFT JOIN justificativa_itens_negados ON solicitacoes.JUSTIFICATIVA_ITENS_NEGADOS_ID = justificativa_itens_negados.ID
    WHERE 
      {$comp}
      AND saida.tipo IN (0, 1, 3)
      AND saida.UR <> 0 
      AND saida.idCliente = 0 
      AND saida.data BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}' 
ORDER BY 
  `nome`
SQL;
        }
        return $sql;
    }

    private static function getSQLSaidaByEquipamento($filters)
    {
        $comp = " saida.CATALOGO_ID = '{$filters['item']}' ";
        $qtd = " saida.quantidade as qtd, ";
        $groupBy = " ";
        if($filters['tipoRelatorio'] == 'sintetico'){
            $qtd = " SUM(saida.quantidade) AS qtd, ";
            $groupBy = " GROUP BY saida.CATALOGO_ID ";
        }

        $sql = <<<SQL
SELECT 
  clientes.nome AS paciente, 
  COALESCE(usuarios.nome, 'Pedido de Emergencia') AS auditora, 
  saida.NUMERO_TISS,
  saida.CATALOGO_ID,
  CONCAT(cobrancaplanos.item) AS nome,
  saida.LOTE, 
  {$qtd}
  saida.data,
  'Equipamento' AS tipoItem,
  (
    CASE 
	WHEN solicitacoes.TIPO_SOLICITACAO IN (2, 5, 9) THEN 'Periodica'
	WHEN solicitacoes.TIPO_SOLICITACAO IN (3, 6, 8) THEN 'Aditiva'
	WHEN solicitacoes.TIPO_SOLICITACAO = -1  THEN 'Avulsa'
	WHEN solicitacoes.TIPO_SOLICITACAO = -2  THEN 'Emergencia'
	END
  ) AS tipoSolicitacao,
  (
	CASE
	WHEN solicitacoes.status = 2 THEN ' - Entrega Imediata'
	ELSE ''
	END
  ) AS imediata,
  IF(solicitacoes.idPrescricao NOT IN (-1, -2), CONCAT('De ', DATE_FORMAT(prescricoes.inicio, '%d/%m/%Y'), ' a ', DATE_FORMAT(prescricoes.fim, '%d/%m/%Y')), 'Sem Período') AS periodo,
  IF(solicitacoes.TIPO_SOLICITACAO IN (-1, -2), IF(JUSTIFICATIVA_AVULSO_ID != 11, justificativa_solicitacao_avulsa.justificativa, solicitacoes.JUSTIFICATIVA_AVULSO), 'Sem Justificativa') AS justAvulsa,
  COALESCE(justificativa_itens_negados.JUSTIFICATIVA, 'Sem Justificativa') AS justNegItem,
  IF(
    solicitacoes.autorizado > 0, 1, 
    IF(solicitacoes.status = -1, 0, 1)
  ) AS liberado,
  solicitacoes.data_auditado AS auditado 
FROM 
  saida 
  INNER JOIN clientes ON clientes.idClientes = saida.idCliente
  INNER JOIN  cobrancaplanos ON (cobrancaplanos.id = saida.CATALOGO_ID) 
  INNER JOIN solicitacoes ON saida.idSolicitacao = solicitacoes.idSolicitacoes
  LEFT JOIN prescricoes ON solicitacoes.idPrescricao = prescricoes.id
  LEFT JOIN usuarios ON solicitacoes.AUDITOR_ID = usuarios.idUsuarios
  LEFT JOIN justificativa_solicitacao_avulsa ON solicitacoes.JUSTIFICATIVA_AVULSO_ID = justificativa_solicitacao_avulsa.id
  LEFT JOIN justificativa_itens_negados ON solicitacoes.JUSTIFICATIVA_ITENS_NEGADOS_ID = justificativa_itens_negados.ID
WHERE 
  {$comp} 
  AND saida.tipo = 5 
  AND saida.UR = 0
  AND saida.data BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}' 
{$groupBy}
UNION ALL

    SELECT
      empresas.nome AS paciente, 
      COALESCE(usuarios.nome, 'Pedido de Emergencia') AS auditora, 
      saida.numero_tiss,
      saida.CATALOGO_ID,
      cobrancaplanos.item AS nome, 
      saida.LOTE, 
      {$qtd}
      saida.data,
      'Equipamento' AS tipoItem,
      (
        CASE 
        WHEN solicitacoes.TIPO_SOLICITACAO IN (2, 5, 9) THEN 'Periodica'
        WHEN solicitacoes.TIPO_SOLICITACAO IN (3, 6, 8) THEN 'Aditiva'
        WHEN solicitacoes.TIPO_SOLICITACAO = -1  THEN 'Avulsa'
        WHEN solicitacoes.TIPO_SOLICITACAO = -2  THEN 'Emergencia'
        END
      ) AS tipoSolicitacao,
      (
        CASE
        WHEN solicitacoes.status = 2 THEN ' - Entrega Imediata'
        ELSE ''
        END
      ) AS imediata,
      IF(solicitacoes.idPrescricao NOT IN (-1, -2), CONCAT('De ', DATE_FORMAT(prescricoes.inicio, '%d/%m/%Y'), ' a ', DATE_FORMAT(prescricoes.fim, '%d/%m/%Y')), 'Sem Período') AS periodo,
      IF(solicitacoes.TIPO_SOLICITACAO IN (-1, -2), IF(JUSTIFICATIVA_AVULSO_ID != 11, justificativa_solicitacao_avulsa.justificativa, solicitacoes.JUSTIFICATIVA_AVULSO), 'Sem Justificativa') AS justAvulsa,
      COALESCE(justificativa_itens_negados.JUSTIFICATIVA, 'Sem Justificativa') AS justNegItem,
      IF(
        solicitacoes.autorizado > 0, 1, 
        IF(solicitacoes.status = -1, 0, 1)
      ) AS liberado,
      solicitacoes.data_auditado AS auditado 
    FROM 
    saida
    INNER JOIN empresas ON empresas.id = saida.UR
    INNER JOIN cobrancaplanos ON (cobrancaplanos.id = saida.CATALOGO_ID) 
    INNER JOIN solicitacoes ON saida.idSolicitacao = solicitacoes.idSolicitacoes
    LEFT JOIN prescricoes ON solicitacoes.idPrescricao = prescricoes.id
    LEFT JOIN usuarios ON solicitacoes.AUDITOR_ID = usuarios.idUsuarios
    LEFT JOIN justificativa_solicitacao_avulsa ON solicitacoes.JUSTIFICATIVA_AVULSO_ID = justificativa_solicitacao_avulsa.id
    LEFT JOIN justificativa_itens_negados ON solicitacoes.JUSTIFICATIVA_ITENS_NEGADOS_ID = justificativa_itens_negados.ID
    WHERE 
      {$comp}
      AND saida.tipo = 5 
      AND saida.idCliente = 0
      AND saida.data BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'  
    {$groupBy}
ORDER BY 
  `nome`
SQL;
        return $sql;
    }

    public static function getSaidaByIdSolicitacao($idSolicitacao)
    {

        $sql = <<<SQL
SELECT
CONCAT(nome,' ',segundoNome) as item,
quantidade,
LOTE,
DATE_FORMAT(`data`,'%d/%m%/%Y') as data
FROM
`saida`
WHERE
idSolicitacao = {$idSolicitacao}
SQL;
        return parent::get($sql, 'assoc');

    }

    public static function getSaidaEquipamentoByPaciente($paciente, $inicio, $fim)
    {
        $sql = "SELECT
		cb.item as nome,
		s.LOTE,
		s.quantidade as qtd,
		s.data
		FROM
		saida as s inner join clientes as p on ( p.idClientes = s.idCliente)
		inner join cobrancaplanos as cb on (cb.id = s.CATALOGO_ID)
		WHERE s.idCliente ={$paciente} and s.tipo = 5
         AND s.data BETWEEN '$inicio' AND '$fim'
		ORDER BY `nome` ASC";
        return parent::get($sql);
    }

    public static function getMaxRemessaByUR($ur)
    {
        $sql = "select * from saida_remessa where empresa_id = {$ur} order by id desc limit 1";

        return parent::get($sql);

    }
}