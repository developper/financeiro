<?php
namespace app\Models\Logistica;

use \App\Models\Financeiro\Fornecedor;
use \App\Models\Logistica\Catalogo;

class XML
{
    public function moveXMLToTemporaryPath($file)
    {
        $errors = [];
        $path = $_SERVER['DOCUMENT_ROOT'] . '/logistica/notas/xml/';
        $size = 1024 * 1024 *1024 * 1024 *1024 * 1024 * 2;

        $errors[1] = 'O arquivo no upload é maior do que o permitido';
        $errors[2] = 'O arquivo ultrapassa o limite de tamanho especifiado';
        $errors[3] = 'O upload do arquivo foi feito parcialmente';
        $errors[4] = 'Não foi feito o upload do arquivo!';

        if ($file['xml']['error'] != 0) {
            return "Não foi possível fazer o upload, erro:<br />" . $errors[$file['xml']['error']];
        }
        $type = $file['xml']['type'];

        if ($type != 'text/xml') {
            return "Por favor, envie apenas arquivos com no formato .xml!";
        }

        if ($size < $file['xml']['size']) {
            return "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
        }

        $name = $file['xml']['name'];
        $fullPath = $path . $name;
        $tmp = $file['xml']['tmp_name'];

        if (move_uploaded_file($tmp, $fullPath)) {
            return $this->retrieveDataFromXML($fullPath, $path);
        }

        return "Não foi possível enviar o arquivo, tente novamente! Caso o erro persista, entre em contato com o setor de TI.";
    }

    private function retrieveDataFromXML($fullPath, $path)
    {
        $xml = @simplexml_load_file($fullPath);
        $response = [];

        $newName = $path . $xml->NFe->infNFe->attributes()['Id'] . '.xml';
        rename($fullPath, $newName);

        $response['fornecedorId'] = Fornecedor::checkIfExistsFornecedorByCNPJ($xml->NFe->infNFe->emit->CNPJ,'F');
        if(!$response['fornecedorId']){
            $response['fornecedorId'] = Fornecedor::createFornecedorByXMLData($xml->NFe->infNFe->emit);
        }

        $response['chaveAcesso']    = str_replace('NFe', '', $xml->NFe->infNFe->attributes()['Id']);
        $response['tipoDocumento']  = 'NF';
        $response['numeroNota']     = current($xml->NFe->infNFe->ide->nNF);
        $response['dataEmissao']    = \DateTime::createFromFormat('Y-m-d\TH:i:sT', $xml->NFe->infNFe->ide->dhEmi)->format('Y-m-d');
        $response['outrasDespesas'] = number_format(current($xml->NFe->infNFe->total->ICMSTot->vOutro), 2, ',', '.');

        $i = 0;
        $response['total'] = 0;
        foreach ($xml->NFe->infNFe->det as $produto) {
            $dadosProduto = Catalogo::getByEANFornecedorProdId(
                current($produto->prod->cEAN),
                $response['fornecedorId'],
                current($produto->prod->cProd)
            );
            if(!$dadosProduto){
                if(property_exists($produto->prod, 'med')) {
                    $response['produto']['SC'.$i]['validade']   = current($produto->prod->med->dVal);
                    $response['produto']['SC'.$i]['lote']       = current($produto->prod->med->nLote);
                } else {
                    $response['produto']['SC'.$i]['validade']   = '';
                    $response['produto']['SC'.$i]['lote']       = '';
                }
                $response['produto']['SC'.$i]['ean']            = current($produto->prod->cEAN);
                $response['produto']['SC'.$i]['cProd']          = current($produto->prod->cProd);
                $response['produto']['SC'.$i]['nomeItem']       = '';
                $response['produto']['SC'.$i]['tipo']           = '';
                $response['produto']['SC'.$i]['qtd']            = '';
                $response['produto']['SC'.$i]['vUnit']          = round(current($produto->prod->vUnCom), 2);
                $response['produto']['SC'.$i]['tipoAquisicao']  = 1;
                $response['produto']['SC'.$i]['CFOP']           = current($produto->prod->CFOP);
                $response['produto']['SC'.$i]['desconto']       = round(((current($produto->prod->vDesc) / $produto->prod->vProd) * 100), 2);
                $subtotal                                       = round(current($produto->prod->vProd) - current($produto->prod->vDesc), 2);
                $response['produto']['SC'.$i]['subtotal']       = $subtotal;
            } else {
                if(property_exists($produto->prod, 'med')) {
                    $response['produto'][$dadosProduto['ID']."-".$i]['validade']   = current($produto->prod->med->dVal);
                    $response['produto'][$dadosProduto['ID']."-".$i]['lote']       = current($produto->prod->med->nLote);
                } else {
                    $response['produto'][$dadosProduto['ID']."-".$i]['validade']   = '';
                    $response['produto'][$dadosProduto['ID']."-".$i]['lote']       = '';
                }
                $response['produto'][$dadosProduto['ID']."-".$i]['ean']            = current($produto->prod->cEAN);
                $response['produto'][$dadosProduto['ID']."-".$i]['cProd']          = current($produto->prod->cProd);
                $response['produto'][$dadosProduto['ID']."-".$i]['nomeItem']       = $dadosProduto['nomeItem'];
                $response['produto'][$dadosProduto['ID']."-".$i]['tipo']           = $dadosProduto['tipo'];
                $response['produto'][$dadosProduto['ID']."-".$i]['qtd']            = '';
                $response['produto'][$dadosProduto['ID']."-".$i]['vUnit']          = round(current($produto->prod->vUnCom), 2);
                $response['produto'][$dadosProduto['ID']."-".$i]['tipoAquisicao']  = 1;
                $response['produto'][$dadosProduto['ID']."-".$i]['CFOP']           = current($produto->prod->CFOP);
                $response['produto'][$dadosProduto['ID']."-".$i]['desconto']       = round((current($produto->prod->vDesc) / current($produto->prod->vProd)) * 100, 2);
                $subtotal                                                   = round(current($produto->prod->vProd) - current($produto->prod->vDesc), 2);
                $response['produto'][$dadosProduto['ID']."-".$i]['subtotal']       = $subtotal;
            }
            $response['total'] += $subtotal;
            $i++;
        }

        $i = 0;
        /*$response['parcelas'] = 0;
        if(isset($xml->NFe->infNFe->cobr->dup)) {
            foreach ($xml->NFe->infNFe->cobr->dup as $parcela) {
                $response['parcelas'][$i]['vencimento'] = current($parcela->dVenc);
                $response['parcelas'][$i]['valor'] = current($parcela->vDup);
                $i++;
            }
        }*/

        $response['totalNota'] = $response['total'] + $response['outrasDespesas'];

        return $response;
    }
}