<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 21/12/2015
 * Time: 09:16
 */

namespace app\Models\Logistica;

use App\Models\AbstractModel,
    App\Models\DAOInterface,
    \App\Core\Log;


class PadronizarMedicamento extends AbstractModel implements DAOInterface
{
    public static function getSQL($id = null)
    {

    }
    public static function inserirPrincipioAtivo($principioAtivo)
    {
        $sql = <<<SQL
            insert
            into
            principio_ativo
            (principio)
            VALUE
          ('$principioAtivo')
SQL;
        mysql_query('begin');
        $rs		= mysql_query($sql);
        if($rs) {
            Log::registerUserLog($sql);
        }else{
            $erro = mysql_error();
            mysql_query("rollback");
            throw new \Exception("Erro ao inserir um Princípio Ativo: " . $erro, $code = 1);
        }
        mysql_query('commit');
        return $mensagem = 'Princípio Ativo inserido com sucesso!!';

    }

    public static function updatePrincipioAtivoByID($principioAtivo, $idPrincipio)
    {
        $sql = <<<SQL
        Update
         principio_ativo
         set
         principio = '{$principioAtivo}'
         WHERE
         id = {$idPrincipio}

SQL;
        mysql_query('begin');
        $rs		= mysql_query($sql);
        if($rs) {
            Log::registerUserLog($sql);
        }else{
            $erro = mysql_error();
            mysql_query("rollback");
            throw new \Exception("Erro ao editar o Princípio Ativo: " . $erro, $code = 1);
        }
        mysql_query('commit');
        return $mensagem = 'Princípio Ativo editado com sucesso!!';


    }

    public static function getByPrincipio($principioAtivo,$query)
    {
        return parent::get(self::getByPrincipioSQL($principioAtivo,$query));
    }

    public static function getByPrincipioSQL($principioAtivo,$query)
    {
        $like = "'%$principioAtivo%'";
        if($query == 'inserir')
            $like = "'$principioAtivo'";
        return <<<SQL
        Select
        id,
        principio
        FROM
        principio_ativo
        WHERE
        principio
        LIKE
        $like;


SQL;

    }
}