<?php
/**
 * Created by PhpStorm.
 * User: jefinho
 * Date: 26/04/2018
 * Time: 09:43
 */

namespace app\Models\Logistica;

use App\Models\AbstractModel;

class SaidaRemessa extends AbstractModel
{
    public static function getRemessaCondensada($filters)
    {

        $condRemessa = empty($filters['remessa']) ? '' : "and s.remessa like '%{$filters['remessa']}%'";
        $condDataInicio = empty($filters['inicio']) ? '' : "and data >= '{$filters['inicio']}'";
        $condDataFim = empty($filters['fim']) ? '' : "and data <= '{$filters['fim']}'";
        $condEmpresa = $_SESSION['empresa_principal'] == 1 ? '' : "and sr.empresa_id = '{$_SESSION['empresa_principal']}'";

        $sql = <<<SQL
        
SELECT		
		s.data,
		s.remessa,
		p.nome as paciente_empresa,
		u.nome as usuario,
		'UR' as tipo_saida,
    s.idSaida,
    sr.alfa,
		sr.numerica

		FROM
		saida as s inner join empresas as p on ( p.id = s.UR)	
		inner join usuarios as u on  (s.idUsuario = u.idUsuarios)	
	inner join saida_remessa as sr on (s.saida_remessa_id = sr.id)	
		WHERE		
		 s.UR > 0
         and s.remessa is not null
         {$condRemessa}
         {$condDataInicio}
         {$condDataFim}
         {$condEmpresa}
		group by remessa		
		UNION
		SELECT		
		s.data,
		s.remessa,
		p.nome as paciente_empresa,
		u.nome as usuario,
		'Paciente' as tipo_saida,
    s.idSaida,
			sr.alfa,
			sr.numerica		
		FROM
		saida as s inner join clientes as p on ( s.idCliente = p.idClientes)		
		inner join usuarios as u on  (s.idUsuario = u.idUsuarios)	
		inner join saida_remessa as sr on (s.saida_remessa_id = sr.id)	
		WHERE		
		 s.idCliente > 0
         and s.remessa is not null	
         {$condRemessa}
         {$condDataInicio}
         {$condDataFim}
         {$condEmpresa}
		group by remessa
		order By data ASC , alfa ASC,numerica ASC
SQL;
        if (!empty($filters['paciente'])){

            $sql = <<<SQL

		SELECT		
		s.data,
		s.remessa,
		p.nome as paciente_empresa,
		u.nome as usuario,
		'Paciente' as tipo_saida,
    s.idSaida,
			sr.alfa,
			sr.numerica		
		FROM
		saida as s inner join clientes as p on ( s.idCliente = p.idClientes)		
		inner join usuarios as u on  (s.idUsuario = u.idUsuarios)	
		inner join saida_remessa as sr on (s.saida_remessa_id = sr.id)	
		WHERE		
		 s.idCliente = {$filters['paciente']}
         and s.remessa is not null	
         {$condRemessa}
         {$condDataInicio}
         {$condDataFim}
         {$condEmpresa}
		group by remessa
		order By data ASC , alfa ASC,numerica ASC
SQL;

}

        return parent::get($sql);

    }

    public static function getRemessaAnalitico($filters){

        $condPaciente = empty($filters['paciente']) ? '' : "and s.idCliente = {$filters['paciente']}";
        $condRemessa = empty($filters['remessa']) ? '' : "and s.remessa like '%{$filters['remessa']}%'";
        $condDataInicio = empty($filters['inicio']) ? '' : "and s.data >= '{$filters['inicio']}'";
        $condDataFim = empty($filters['fim']) ? '' : "and s.data <= '{$filters['fim']}'";
        $condEmpresa = $_SESSION['empresa_principal'] == 1 ? '' : "and sr.empresa_id = '{$_SESSION['empresa_principal']}'";


        $sql = "
        SELECT
		CONCAT(catalogo.principio, ' - ', catalogo.apresentacao, ' LAB: ', catalogo.lab_desc) AS nome, 
		s.LOTE,
		s.quantidade as qtd,
		s.data,
		s.remessa,
		p.nome as paciente_empresa,
		u.nome as usuario,
		'Paciente' as tipo_saida,
        s.idSaida,
        sr.alfa,
        sr.numerica,
        (CASE
    WHEN s.tipo = 0 THEN 'MEDICAMENTO'
    WHEN s.tipo = 1 THEN 'MATERIAL'
    WHEN s.tipo = 3 THEN 'DIETA'
    END) as tipo_item
        		
		FROM
		saida as s inner join clientes as p on ( p.idClientes = s.idCliente)
		inner join catalogo as catalogo on (s.CATALOGO_ID = catalogo.ID) 
		inner join usuarios as u on  (s.idUsuario = u.idUsuarios)
		inner join saida_remessa as sr on (s.saida_remessa_id = sr.id)		
		WHERE
		s.tipo in (0, 1, 3)
		and s.idCliente > 0		
		and s.remessa is not null	 
		{$condRemessa}
         {$condDataInicio}
         {$condDataFim}
         {$condEmpresa}  
         {$condPaciente}   
		UNION
		SELECT
		CONCAT(catalogo.principio, ' - ', catalogo.apresentacao, ' LAB: ', catalogo.lab_desc) AS nome,
		s.LOTE,
		s.quantidade as qtd,
		s.data,
		s.remessa,
		p.nome as paciente_empresa,
		u.nome as usuario,
		'UR' as tipo_saida,
         s.idSaida,
        sr.alfa,
        sr.numerica,
        (CASE
    WHEN s.tipo = 0 THEN 'MEDICAMENTO'
    WHEN s.tipo = 1 THEN 'MATERIAL'
   WHEN s.tipo = 3 THEN 'DIETA'
    END) as tipo_item		
		FROM
		saida as s inner join empresas as p on ( p.id = s.UR)
		inner join catalogo as catalogo on (s.CATALOGO_ID =catalogo.ID)
		inner join usuarios as u on  (s.idUsuario = u.idUsuarios)	
		inner join saida_remessa as sr on (s.saida_remessa_id = sr.id)	
		WHERE
		s.tipo  in (0, 1, 3)
		and s.UR > 0		
		and s.remessa is not null
		{$condRemessa}
         {$condDataInicio}
         {$condDataFim}
         {$condEmpresa}  
         {$condPaciente} 
		UNION
		SELECT
		cb.item as nome,
		s.LOTE,
		s.quantidade as qtd,
		s.data,
		s.remessa,
		p.nome as paciente_empresa,
		u.nome as usuario,
		'Paciente' as tipo_saida,
        s.idSaida,
        sr.alfa,
        sr.numerica,
        'EQUIPAMENTO' as tipo_item			
		FROM
		saida as s inner join clientes as p on ( s.idCliente = p.idClientes  )
		inner join cobrancaplanos as cb on (s.CATALOGO_ID = cb.id) 
		inner join usuarios as u on  (s.idUsuario = u.idUsuarios)
		inner join saida_remessa as sr on (s.saida_remessa_id = sr.id)		
		WHERE
		s.tipo = 5
		and s.idCliente > 0		
		and s.remessa is not null	
		{$condRemessa}
         {$condDataInicio}
         {$condDataFim}
         {$condEmpresa}  
         {$condPaciente}     
		UNION
		SELECT
		cb.item as nome,
		s.LOTE,
		s.quantidade as qtd,
		s.data,
		s.remessa,
		p.nome as paciente_empresa,
		u.nome as usuario,
		'UR' as tipo_saida,
        s.idSaida,
        sr.alfa,
        sr.numerica,
        'EQUIPAMENTO' as tipo_item			
		FROM
		saida as s inner join empresas as p on (  s.UR = p.id)
		inner join cobrancaplanos as cb on ( s.CATALOGO_ID = cb.id ) 
		inner join usuarios as u on  (s.idUsuario = u.idUsuarios)	
		inner join saida_remessa as sr on (s.saida_remessa_id = sr.id)	
		WHERE
		s.tipo = 5
		and s.UR > 0		
		and s.remessa is not null	
		{$condRemessa}
         {$condDataInicio}
         {$condDataFim}
         {$condEmpresa}  
         {$condPaciente} 
		
		ORDER BY data ASC, alfa asc, numerica asc, tipo_item asc, nome asc
        ";
        return parent::get($sql);
    }

}