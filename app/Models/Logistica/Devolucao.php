<?php

namespace app\Models\Logistica;

use App\Models\AbstractModel;

class Devolucao extends AbstractModel
{
    public static function getDevolucaoByPaciente($paciente, $inicio, $fim)
    {
        $sql = " SELECT

                concat(catalogo.principio,' - ',catalogo.apresentacao, ' LAB: ', catalogo.lab_desc) as nome,
                devolucao.LOTE,
                devolucao.QUANTIDADE as qtd,
                devolucao.DATA_DEVOLUCAO,
                devolucao.TIPO,
                (
        CASE
          devolucao.TIPO
          WHEN
            '0'
          THEN
            'Medicamento'
          WHEN
            '1'
          THEN
            'Material'
          WHEN
            '3'
          THEN
            'Dieta'
        END
      )
      AS nomeTipo
              FROM devolucao INNER JOIN clientes on ( clientes.idClientes = devolucao.PACIENTE_ID)
                INNER JOIN catalogo on(catalogo.ID= devolucao.CATALOGO_ID)
              WHERE
                devolucao.PACIENTE_ID ={$paciente} and
                devolucao.TIPO in (0,1,3) AND
                devolucao.DATA_DEVOLUCAO   BETWEEN '$inicio' AND '$fim'

      union
      SELECT
        cobrancaplanos.item as nome,
        devolucao.LOTE,
        devolucao.quantidade as qtd,
        devolucao.DATA_DEVOLUCAO,
        devolucao.TIPO,
        'Equipamento'  AS nomeTipo
      FROM
        devolucao INNER JOIN
        clientes on ( clientes.idClientes = devolucao.PACIENTE_ID) INNER JOIN
        cobrancaplanos on (cobrancaplanos.id = devolucao.CATALOGO_ID)
      WHERE
        devolucao.PACIENTE_ID = {$paciente} and
        devolucao.TIPO = 5 AND
        devolucao.DATA_DEVOLUCAO   BETWEEN '$inicio' AND '$fim'

      ORDER BY
        TIPO ,`nome` ASC";

        return parent::get($sql);
    }

}