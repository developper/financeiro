<?php
namespace app\Models\Logistica;

use App\Models\AbstractModel;
use App\Models\DAOInterface;

class Estoque extends AbstractModel implements DAOInterface
{
    protected $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

	public static function getAll ()
	{
		return parent::get (self::getSQL ());
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? "WHERE ID = {$id}" : null;
		return "SELECT * FROM estoque {$id} ORDER BY ID";
	}

	public function saveEntradaProdutos($data)
	{
        $response = [];
        $quantidadeTotal = 0;

        $this->conn->query("BEGIN");
        $this->conn->autocommit(FALSE);
        $data['outras_despesas'] = str_replace('.', '', $data['outras_despesas']);
        $data['total_produtos'] += $data['outras_despesas'];
        list($rsNota, $data['notaId']) = $this->saveNotaEntradaProdutos(array_slice($data, 0, 6), $data['total_produtos']);
        $this->updateTRNota($data['notaId']);
        $rsParcelas    = $this->saveParcelasEntradaProdutos(array_slice($data, -5));

        $rateio        = ['valor' => $data['total_produtos'], 'notaId' => $data['notaId']];
        $rsRateio      = $this->saveRateioEntradaProdutos($rateio);

        foreach($data['qtd'] as $qtd){
            $quantidadeTotal += $qtd;
        }

        $rateioOutrasDespesas = 0;
        if($data['outras_despesas'] != 0 || $data['outras_despesas'] != '0,00') {
            $rateioOutrasDespesas =  $data['outras_despesas']/$quantidadeTotal;
        }

        if($rsNota && $rsParcelas && $rsRateio){
            foreach($data['tipo'] as $itemLote => $tipo){
                // COMO ESTÁ SENDO USADO O LOTE TAMBÉM PARA DIFERENCIAR OS PRODUTOS,
                // É QUEBRADA A STRING PARA PEGAR APENAS O CATALOGO_ID. EX: 12345-ABC123
                $itemId = explode('-', $itemLote);
                $itemId = $itemId[0];

                $sql = <<<SQL
INSERT INTO entrada
    (
        `nota`,
        `fornecedor`,
        `item`,
        `qtd`,
        `valor`,
        `data`,
        `LOTE`,
        `DES_BONIFICACAO_ITEM`,
        `VALIDADE`,
        `CFOP`,
        `USUARIO_ID`,
        `CATALOGO_ID`,
        CODIGO_REFERENCIA,
        UR,
        TIPO,
        TIPO_AQUISICAO,
        ATIVO
    ) VALUES (
        ?,
        ?,
        0,
        ?,
        ?,
        NOW(),
        ?,
        ?,
        ?,
        ?,
        '{$_SESSION['id_user']}',
        ?,
        ?,
        '{$_SESSION['empresa_principal']}',
        ?,
        ?,
        'true'
    )
SQL;

                if($stmt = $this->conn->prepare($sql)) {
                    $valorFinal = $data['vl_unitario'][$itemLote] + $rateioOutrasDespesas;

                    $stmt->bind_param (
                        'isssssssisss',
                        $data['notaId'],
                        $data['fornecedor'],
                        $data['qtd'][$itemLote],
                        $valorFinal,
                        $data['lote'][$itemLote],
                        $data['desconto'][$itemLote],
                        $data['validade'][$itemLote],
                        $data['cfop'][$itemLote],
                        $itemId,
                        $data['referencia'][$itemLote],
                        $tipo,
                        $data['tipo_aquisicao'][$itemLote]
                    );
                    $response[] = $stmt->execute ();
                    $stmt->close();



                    if ($tipo != 5) {
                        $response[] = $this->updateValorUnitarioProduto ($itemId, $data['vl_unitario'][$itemLote]);
                        if($data['ean'][$itemLote] != '') {
                            $response[] = $this->updateCodigoBarraProduto($itemId, $data['fornecedor'], $data['ean'][$itemLote], $data['cprod'][$itemLote]);
                        }
                        list($custoAtual, $response[]) = $this->getUltimoCustoProduto ($itemId);
                        $custoAtual = round($custoAtual, 2);

                        list($estoqueAtual, $response[]) = $this->getEstoqueAtualEmpresas ($itemId);

                        $novoCustoTotal = $data['qtd'][$itemLote] * $data['vl_unitario'][$itemLote];

                        $updVlMedioArray = [
                            "estoqueAtual" => $estoqueAtual,
                            "custoAtual" => $custoAtual,
                            "novoCustoTotal" => $novoCustoTotal,
                            "qtd" => $data['qtd'][$itemLote],
                            "vlUnitario" => $data['vl_unitario'][$itemLote],
                            "itemId" => $itemId,
                            "tipo" => $tipo,
                        ];

                        $response[] = $this->updateValorMedioProduto ($updVlMedioArray);
                    }

                    $response[] = $this->updateEstoqueProduto ($data['qtd'][$itemLote], $itemId, $tipo);
                } else {
                    throw new \Exception("Erro ao dar entrada de um item: " . $this->conn->error);
                }
            }
            if(!in_array(false, $response))
                return [$this->conn->query("COMMIT"), $data['notaId']];
            $this->conn->query("ROLLBACK");
            return false;
        }
        $this->conn->query("ROLLBACK");
        return false;
	}

    private function saveNotaEntradaProdutos ($nota, $total)
    {
        $sql = <<<SQL
INSERT INTO notas 
(
USUARIO_ID, 
idNotas, 
CHAVE_ACESSO, 
codigo ,
codNatureza, 
codCentroCusto, 
valor, 
dataEntrada, 
descricao, 
codFornecedor, 
tipo, 
status, 
codSubNatureza, 
empresa, 
ICMS, 
FRETE, 
SEGURO, 
VAL_PRODUTOS, 
DES_ACESSORIAS, 
IPI, 
ISSQN, 
DESCONTO_BONIFICACAO, 
CHAVE, 
TR, 
TIPO_DOCUMENTO, 
PIS, 
COFINS, 
CSLL, 
IR, 
DATA, 
PCC, 
INSS, 
NOTA_REFERENCIA, 
IMPOSTOS_RETIDOS_ID, 
visualizado, 
visualizado_por,
nota_fatura 
)
VALUES
(
    ?,           /* USUARIO_ID)*/ 
    NULL,        /* idNotas */
    ?,           /* CHAVE_ACESSO */
    ?,           /* codigo */
    1,           /* codNatureza */
    1,           /* codCentroCusto */
    ?,           /* valor */
    '{$nota['emissao']}',       /* dataEntrada */
    ?,           /* descricao */
    ?,           /* codFornecedor */
    1,           /* tipo */
    'Em aberto', /* status */
    1,           /* codSubNatureza */
    1,           /* empresa */
    0,           /* ICMS */
    0,           /* FRETE */
    0,           /* SEGURO */
    ?,           /* VAL_PRODUTOS */
    0,           /* DES_ACESSORIAS */
    0,           /* IPI */
    0,           /* ISSQN */
    0,           /* DESCONTO_BONIFICACAO */
    '',          /* CHAVE */
    1,           /* TR */
    ?,           /* TIPO_DOCUMENTO */
    0,           /* PIS */
    0,           /* COFINS */
    0,           /* CSLL */
    0,           /* IR */
    NOW(),       /* DATA */
    0,           /* PCC */
    0,           /* INSS */
    0,           /* NOTA_REFERENCIA */
    0,           /* IMPOSTOS_RETIDOS_ID */
    0,           /* visualizado */
    0,            /* visualizado_por */
    0           /*nota_fatura*/
)
SQL;


        if($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param (
                'issdsids',
                $_SESSION['id_user'],
                $nota['chave_acesso'],
                $nota['num_nota'],
                $total,
                $nota['descricao'],
                $nota['fornecedor'],
                $total,
                $nota['tipo_documento']
            );
            
            $rs = $stmt->execute ();
            return [$rs, $stmt->insert_id];
        }
       
        throw new \Exception("Erro ao criar a nota: " . $this->conn->error);
    }

    private function saveParcelasEntradaProdutos ($parcelas)
    {
        $response = [];
        $sql = <<<SQL
INSERT INTO parcelas
(
id,
`idNota`,
`valor`,
`vencimento`,
`codBarras`,
  origem,
  aplicacao,
 `status`,
 `empresa`,
 `TR`)
 VALUES
(
    NULL,                  /* id */
    ?,                     /* idNota */
    ?,                     /* valor */
    ?,                     /* vencimento */
    ?,                     /* codBarras */
    1,                     /* origem */
    0,                     /* aplicacao */
    'Pendente',            /* status */
    ?,                     /* empresa */
    ?                     /* tr */

)
SQL;

        foreach($parcelas['tr_parcela'] as $key => $tr){
            if( $parcelas['vencimento'][$key] != '' &&
                $parcelas['valor_parcela'][$key] != '0,00' &&
                $tr != ''
            ) {
                $valor_parcela = str_replace('.', '', $parcelas['valor_parcela'][$key]);
                $valor_parcela = str_replace(',', '.', $valor_parcela);
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param(
                        'isssss',
                        $parcelas['notaId'],
                        $valor_parcela,
                        $parcelas['vencimento'][$key],
                        $parcelas['codigo_barras'][$key],
                        $_SESSION['empresa_principal'],
                        $tr
                    );
                    $response[] = $stmt->execute();
                    $stmt->close();
                } else {
                    throw new \Exception("Erro ao criar uma parcela: " . $this->conn->error);
                }
            }
        }
        return !in_array(false, $response);
    }

    private function updateTRNota ($notaId)
    {
        $sql = <<<SQL
UPDATE
	notas
SET
	TR = ?
WHERE
	idNotas = ?
SQL;
        if($stmt = $this->conn->prepare ($sql)) {
            $stmt->bind_param (
                'ii',
                $notaId,
                $notaId
            );
            return $stmt->execute ();
        }
        throw new \Exception("Erro ao atualizar a TR da nota: " . $this->conn->error);
    }

    private function saveRateioEntradaProdutos ($rateio)
    {
        $ipcf2              = 'DESEMBOLSOS OPERACIONAIS';
        $ipcf3              = 'MATERIAIS APLIC NA PRESTAÇÃO DE SERVIÇOS';
        $ipcf4              = 208;
        $setor              = 8;

        $this->conn->query("SET NAMES 'utf8'");

        $sql = <<<SQL
INSERT INTO rateio VALUES
(
    NULL,                  /* id */
    ?,                     /* NOTA_ID */
    ?,                     /* NOTA_TR */
    1,                     /* CENTRO_RESULTADO */
    ?,                     /* VALOR */
    100,                   /* PORCENTAGEM */
    0,                     /* IPCG3_ANTIGA */
    0,                     /* IPCG4_ANTIGA */
    ?,                     /* ASSINATURA */
    ?,                     /* IPCF2 */
    ?,                     /* IPCG3 */
    ?                      /* IPCG4 */
)
SQL;

        if($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param (
                'iidissi',
                $rateio['notaId'],
                $rateio['notaId'],
                $rateio['valor'],
                $setor,
                $ipcf2,
                $ipcf3,
                $ipcf4
            );
            return $stmt->execute ();
        }
        throw new \Exception("Erro ao criar o rateio: " . $this->conn->error);
    }

    private function updateValorUnitarioProduto ($itemId, $valorUnitario)
    {
        $sql = <<<SQL
UPDATE
	catalogo
SET
	valor = ?
WHERE
	ID = ?
SQL;
        if($stmt = $this->conn->prepare ($sql)) {
            $stmt->bind_param (
                'di',
                $valorUnitario,
                $itemId
            );
            return $stmt->execute ();
        }
        throw new \Exception("Erro ao atualizar o valor do item: " . $this->conn->error);
    }

    private function updateCodigoBarraProduto ($itemId, $fornecedorId, $codigoBarra, $cprod)
    {
        if(!$this->checkIfEanExists($fornecedorId, $codigoBarra, $cprod)) {

            $sql = <<<SQL
INSERT INTO catalogo_codigo_barras VALUES (NULL, ?, ?, ?, ?)
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'iisi',
                    $itemId,
                    $fornecedorId,
                    $codigoBarra,
                    $cprod
                );
                return $stmt->execute();
            }
            throw new \Exception("Erro ao atualizar o codigo de barras do item: " . $this->conn->error);
        }
        return true;
    }

    private function checkIfEanExists($fornecedorId, $codigoBarra, $cprod)
    {
        $sql = "SELECT 
                  codigo_barra, 
                  fornecedor_id,
                  produto_fornecedor_id 
                FROM 
                  catalogo_codigo_barras 
                WHERE 
                  codigo_barra = ? 
                  AND fornecedor_id = ?
                  AND produto_fornecedor_id = ?";
        if($stmt = $this->conn->prepare ($sql)) {
            $stmt->bind_param ('sii', $codigoBarra, $fornecedorId, $cprod);
            $stmt->execute ();
            $stmt->bind_result ($codigoBarra, $fornecedorId, $cprod);
            $result = $stmt->fetch ();
            if(empty($result)){
                return false;
            }
            return true;
        }
    }

    private function getUltimoCustoProduto ($itemId)
    {
        $custoAtual = 0;
        $sql = "SELECT valorMedio FROM catalogo WHERE ID = ?";
        if($stmt = $this->conn->prepare ($sql)) {
            $stmt->bind_param ('i', $itemId);
            $response = $stmt->execute ();
            $stmt->bind_result ($custoAtual);
            $stmt->fetch ();
            return [$custoAtual, $response];
        }
        throw new \Exception("Erro ao buscar o ultimo custo: " . $this->conn->error);
    }

    private function getEstoqueAtualEmpresas ($itemId)
    {
        $response = [];

        # Buscando todas as UR's ativas
        $empresas = [];
        $complemento = '';
        $sql = "SELECT id FROM empresas WHERE ATIVO = 'S'";
        if($result = $this->conn->query ($sql)) {
            while ($row = $result->fetch_assoc()) {
                $empresas[] = $row['id'];
            }
        }

        # Buscando as quantidades das ur's ativas
        foreach($empresas as $empresa) {
            $complemento .= "empresas_id_{$empresa},";
        }
        $complemento = substr_replace($complemento, '', -1, 1);
        $sql = "SELECT {$complemento} FROM estoque WHERE CATALOGO_ID = {$itemId}";
        if($result = $this->conn->query ($sql)) {
            $row = $result->fetch_assoc();
            $estoqueAtual = 0;
            foreach ($empresas as $empresa) {
                if (!empty($row["empresas_id_{$empresa}"])) {
                    $estoqueAtual += $row["empresas_id_{$empresa}"];
                }
            }
            return [$estoqueAtual, !in_array (false, $response)];
        }
        throw new \Exception("Erro ao buscar o estoque atual das empresas: " . $this->conn->error);
    }

    private function updateValorMedioProduto ($data)
    {
        # FORMULA: (estoque atual * custo atual) + (qtd entrada * custo unitario entrada)/estoque atual + qtd entrada
        # Atualiza o valor médio na tabela catalogo
        $sql = <<<SQL
UPDATE
	catalogo
SET
 	valorMedio = IF(? > 0, ((? * ?) + (?)) / (? + ?), ?),
	valor = ?,
	custo_medio_atualizado_em = now()
WHERE
	ID = ?
	AND TIPO = ?
SQL;
        if($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param (
                'iidiisssis',
                $data['estoqueAtual'],
                $data['estoqueAtual'],
                $data['custoAtual'],
                $data['novoCustoTotal'],
                $data['estoqueAtual'],
                $data['qtd'],
                $data['vlUnitario'],
                $data['vlUnitario'],
                $data['itemId'],
                $data['tipo']
            );

            return $stmt->execute ();
        }
        throw new \Exception("Erro ao atualizar o valor medio do item: " . $this->conn->error);
    }

    private function updateEstoqueProduto ($qtd, $itemId, $tipo)
    {
        # Atualiza o estoque da respectiva UR
        # $qtdUrField = 'empresas_id_' . $_SESSION['empresa_principal'] - nome do campo da respectiva UR na tabela estoque
        $qtdUrField = "empresas_id_{$_SESSION['empresa_principal']}";
        if($this->checkIfProdutoExistsInEstoque($itemId) > 0) {
            $sql = <<<SQL
UPDATE
	estoque
SET
 	{$qtdUrField} = {$qtdUrField} + ?
WHERE
	CATALOGO_ID = ?
	AND TIPO = ?
SQL;
            if ($stmt = $this->conn->prepare ($sql)) {
                $stmt->bind_param (
                    'sis',
                    $qtd,
                    $itemId,
                    $tipo
                );

                return $stmt->execute ();
            }
        } else {
            $sql = <<<SQL
INSERT INTO estoque
(
    ID,
    DATA_ULTIMA_ENTRADA,
    TIPO,
    CATALOGO_ID,
    {$qtdUrField}
)
VALUES
(
    NULL,
    NOW(),
    ?,
    ?,
    ?
)
SQL;
            if ($stmt = $this->conn->prepare ($sql)) {
                $stmt->bind_param (
                    'sis',
                    $tipo,
                    $itemId,
                    $qtd
                );

                return $stmt->execute ();
            }
        }

        throw new \Exception("Erro ao atualizar o estoque do item: " . $this->conn->error);
    }

    private function checkIfProdutoExistsInEstoque ($itemId)
    {
        $sql = "SELECT ID FROM estoque WHERE CATALOGO_ID = {$itemId}";
        $result = $this->conn->query($sql);
        return $result->num_rows;
    }

    public static function getEntradasRelatorioItensVencidos($filters)
    {
        $compItem   = $filters['item'] != '' ? " AND entrada.CATALOGO_ID = '{$filters['item']}'" : "";
        $compTipo   = $filters['tipo'] != '' ? " AND catalogo.tipo = '{$filters['tipo']}'" : " AND catalogo.tipo IN (0, 1, 3) ";

        if($filters['ur'] == '' || ($filters['ur'] == '11' || $filters['ur'] == '1')){
            $compUr = " AND entrada.UR = '1'";
        } else {
            $compUr = " AND entrada.UR = '{$filters['ur']}'";
        }

        $compLab    = $filters['laboratorio'] != '' ? " AND catalogo.lab_desc LIKE '%{$filters['laboratorio']}%'" : "";

        $compMostrar = " 1=1 ";

        if (in_array('red', $filters['mostrar'])) {
            $compMostrar .= " OR entrada.VALIDADE < NOW() ";
        }
        if (in_array('orange', $filters['mostrar'])) {
            $compMostrar .= " OR (entrada.VALIDADE >= NOW() AND DATEDIFF(entrada.VALIDADE, NOW()) <= 31) ";
        }
        if (in_array('gold', $filters['mostrar'])) {
            $compMostrar .= " OR (entrada.VALIDADE >= NOW() AND PERIOD_DIFF(CONCAT(SPLIT_STRING(entrada.VALIDADE, '-', 1), SPLIT_STRING(entrada.VALIDADE, '-', 2)), CONCAT(SPLIT_STRING(NOW(), '-', 1), SPLIT_STRING(NOW(), '-', 2))) >= 1 AND PERIOD_DIFF(CONCAT(SPLIT_STRING(entrada.VALIDADE, '-', 1), SPLIT_STRING(entrada.VALIDADE, '-', 2)), CONCAT(SPLIT_STRING(NOW(), '-', 1), SPLIT_STRING(NOW(), '-', 2))) <= 6 )";
        }
        if (in_array('black', $filters['mostrar'])) {
            $compMostrar .= " OR (entrada.VALIDADE >= NOW() AND PERIOD_DIFF(CONCAT(SPLIT_STRING(entrada.VALIDADE, '-', 1), SPLIT_STRING(entrada.VALIDADE, '-', 2)), CONCAT(SPLIT_STRING(NOW(), '-', 1), SPLIT_STRING(NOW(), '-', 2)) > 6) AND PERIOD_DIFF(CONCAT(SPLIT_STRING(entrada.VALIDADE, '-', 1), SPLIT_STRING(entrada.VALIDADE, '-', 2)), CONCAT(SPLIT_STRING(NOW(), '-', 1), SPLIT_STRING(NOW(), '-', 2))) <= 12)";
        }
        if (in_array('gray', $filters['mostrar'])) {
            $compMostrar .= " OR (entrada.VALIDADE >= NOW() AND PERIOD_DIFF(CONCAT(SPLIT_STRING(entrada.VALIDADE, '-', 1), SPLIT_STRING(entrada.VALIDADE, '-', 2)), CONCAT(SPLIT_STRING(NOW(), '-', 1), SPLIT_STRING(NOW(), '-', 2)) > 12))";
        }

        $sql = <<<SQL
 SELECT
  catalogo.ID AS idItem,
  catalogo.lab_desc as laboratorio,
  entrada.LOTE,
  empresas.nome AS ur,
  CONCAT(catalogo.principio, ' ', catalogo.apresentacao) AS nomeItem,
  COALESCE(SUM(entrada.qtd), 0) AS qtd,
  entrada.data AS dataEntrada,
  entrada.VALIDADE,
  IF(catalogo.ATIVO = 'A', 'Sim', 'Não') AS ativo,
  CASE
  WHEN catalogo.tipo = '0' THEN 'Medicamento'
  WHEN catalogo.tipo = '1' THEN 'Material'
  WHEN catalogo.tipo = '3' THEN 'Dieta'
  END AS tipoItem,
  principio_ativo.principio AS principioAtivo,
  catalogo.valor as custo
FROM
  entrada
  INNER JOIN catalogo ON entrada.CATALOGO_ID = catalogo.ID
  INNER JOIN empresas ON entrada.UR = empresas.id
  LEFT JOIN catalogo_principio_ativo ON catalogo.ID = catalogo_principio_ativo.catalogo_id
  INNER JOIN principio_ativo ON catalogo_principio_ativo.principio_ativo_id = principio_ativo.id 
WHERE
    entrada.VALIDADE BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
    AND (entrada.VALIDADE IS NOT NULL AND entrada.VALIDADE <> '')
    AND (entrada.LOTE IS NOT NULL AND entrada.LOTE <> '' AND entrada.LOTE <> 0)
    AND entrada.qtd != 0
    AND entrada.ATIVO = 'true'
    AND catalogo.ATIVO = 'A'
    {$compItem}
    {$compTipo}
    {$compUr}
    {$compLab}
    AND ({$compMostrar})
GROUP BY
    entrada.LOTE, idItem
ORDER BY
    entrada.VALIDADE;
SQL;
        //echo '<pre>'; die($sql);
        return parent::get($sql, 'assoc');
    }

    public static function getDevolucoesRelatorioItensVencidos($filters)
    {
        $compItem   = $filters['item'] != '' ? " AND devolucao.CATALOGO_ID = '{$filters['item']}'" : "";
        $compTipo   = $filters['tipo'] != '' ? " AND catalogo.tipo = '{$filters['tipo']}'" : " AND catalogo.tipo IN (0, 1, 3) ";
        if($filters['ur'] == '' && ($filters['ur'] == '11' || $filters['ur'] == '1')){
            $compUr = " AND devolucao.UR = '1'";
        } else {
            $compUr = " AND devolucao.UR = '{$filters['ur']}'";
        }
        $compLab    = $filters['laboratorio'] != '' ? " AND catalogo.lab_desc LIKE '%{$filters['laboratorio']}%'" : "";

        $sql = <<<SQL
 SELECT
  catalogo.ID AS idItem,
  devolucao.LOTE,
  CONCAT(catalogo.principio, ' ', catalogo.apresentacao) AS nomeItem,
  COALESCE(SUM(devolucao.QUANTIDADE), 0) AS qtd,
  catalogo.valor as custo
FROM
  devolucao
  INNER JOIN catalogo ON devolucao.CATALOGO_ID = catalogo.ID
WHERE
    (devolucao.DATA_DEVOLUCAO IS NOT NULL AND  devolucao.DATA_DEVOLUCAO <> '')
    AND (devolucao.LOTE IS NOT NULL AND devolucao.LOTE <> '' AND devolucao.LOTE <> 0)
    AND devolucao.QUANTIDADE != 0
    AND devolucao.ATIVO = 'true'
    AND catalogo.ATIVO = 'A'
    {$compItem}
    {$compTipo}
    {$compUr}
    {$compLab}
GROUP BY
    devolucao.LOTE, idItem
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getEntradasUrRelatorioItensVencidos($filters)
    {
        $compItem   = $filters['item'] != '' ? " AND itenspedidosinterno.CATALOGO_ID = '{$filters['item']}'" : "";
        $compTipo   = $filters['tipo'] != '' ? " AND catalogo.tipo = '{$filters['tipo']}'" : " AND catalogo.tipo IN (0, 1, 3) ";
        $compUr     = " AND entrada_ur.UR = '{$filters['ur']}'";
        $compLab    = $filters['laboratorio'] != '' ? " AND catalogo.lab_desc LIKE '%{$filters['laboratorio']}%'" : "";

        $sql = <<<SQL
 SELECT
  catalogo.ID AS idItem,
  entrada_ur.LOTE,
  CONCAT(catalogo.principio, '', catalogo.apresentacao) AS nomeItem,
  COALESCE(SUM(entrada_ur.QTD), 0) AS qtd,
  catalogo.valor as custo
FROM
  entrada_ur
  INNER JOIN itenspedidosinterno ON entrada_ur.ITENSPEDIDOSINTERNOS_ID = itenspedidosinterno.ID
  INNER JOIN catalogo ON itenspedidosinterno.CATALOGO_ID = catalogo.ID
WHERE
    (entrada_ur.DATA IS NOT NULL AND entrada_ur.DATA <> '')
    AND (entrada_ur.LOTE IS NOT NULL AND entrada_ur.LOTE <> '' AND entrada_ur.LOTE <> 0)
    AND entrada_ur.QTD != 0
    AND catalogo.ATIVO = 'A'
    {$compItem}
    {$compTipo}
    {$compUr}
    {$compLab}
GROUP BY
    entrada_ur.LOTE, idItem
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getSaidasRelatorioItensVencidos($filters)
    {
        $compItem   = $filters['item'] != '' ? " AND saida.CATALOGO_ID = '{$filters['item']}'" : "";
        $compTipo   = $filters['tipo'] != '' ? " AND catalogo.tipo = '{$filters['tipo']}'" : " AND catalogo.tipo IN (0, 1, 3) ";
        if($filters['ur'] == '' || ($filters['ur'] == '11' || $filters['ur'] == '1')){
            $compUr = " AND saida.UR_ORIGEM = '1'";
        } else {
            $compUr = " AND saida.UR_ORIGEM = '{$filters['ur']}'";
        }

        $compLab    = $filters['laboratorio'] != '' ? " AND catalogo.lab_desc LIKE '%{$filters['laboratorio']}%'" : "";

        $sql = <<<SQL
 SELECT
  catalogo.ID AS idItem,
  saida.LOTE,
  CONCAT(catalogo.principio, ' ', catalogo.apresentacao) AS nomeItem,
  COALESCE(SUM(saida.quantidade), 0) AS qtd,
  catalogo.valor as custo
FROM
  saida
  INNER JOIN catalogo ON saida.CATALOGO_ID = catalogo.ID
WHERE
  (saida.data IS NOT NULL AND saida.data <> '')
  AND (saida.LOTE IS NOT NULL AND saida.LOTE <> '' AND saida.LOTE <> 0)
  AND saida.quantidade != 0
  AND saida.ATIVO = 'true'
  AND catalogo.ATIVO = 'A'
  {$compItem}
  {$compTipo}
  {$compUr}
  {$compLab}
GROUP BY
    saida.LOTE, idItem;
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getSaidasInternasRelatorioItensVencidos($filters)
    {
        $compItem   = $filters['item'] != '' ? " AND saida_interna.CATALOGO_ID = '{$filters['item']}'" : "";
        $compTipo   = $filters['tipo'] != '' ? " AND catalogo.tipo = '{$filters['tipo']}'" : " AND catalogo.tipo IN (0, 1, 3) ";

        if($filters['ur'] == '' || ($filters['ur'] == '11' || $filters['ur'] == '1')){
            $compUr = " AND saida_interna.EMPRESA_ID = '1'";
        } else {
            $compUr = " AND saida_interna.EMPRESA_ID = '{$filters['ur']}'";
        }

        $compLab    = $filters['laboratorio'] != '' ? " AND catalogo.lab_desc LIKE '%{$filters['laboratorio']}%'" : "";

        $sql = <<<SQL
 SELECT
  catalogo.ID AS idItem,
  saida_interna.LOTE,
  CONCAT(catalogo.principio, ' ', catalogo.apresentacao) AS nomeItem,
  COALESCE(SUM(saida_interna.QUANTIDADE), 0) AS qtd,
  catalogo.valor as custo
FROM
  saida_interna
  INNER JOIN catalogo ON saida_interna.CATALOGO_ID = catalogo.ID
WHERE
    saida_interna.DATA_SAIDA_INTERNA BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
    AND (saida_interna.DATA_SAIDA_INTERNA IS NOT NULL OR saida_interna.DATA_SAIDA_INTERNA <> '')
    AND (saida_interna.LOTE IS NOT NULL OR saida_interna.LOTE <> '')
    AND saida_interna.QUANTIDADE != 0
    AND saida_interna.ATIVO = 'true'
    AND catalogo.ATIVO = 'A'
    {$compItem}
    {$compTipo}
    {$compUr}
    {$compLab}
GROUP BY
    saida_interna.LOTE, idItem;
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getEntradasByFilters($filter)
    {
        $compFornecedor = $filter['fornecedor'] != '' ? " AND entrada.fornecedor = '{$filter['fornecedor']}' " : "";
        $compItem       = $filter['item'] != '' ? " AND entrada.CATALOGO_ID = '{$filter['item']}' " : "";

        $sql = <<<SQL
SELECT
  entrada.id AS idEntrada,
  entrada.data AS dataEntrada,
  entrada.CATALOGO_ID AS idItem,
  entrada.LOTE,
  entrada.VALIDADE,
  entrada.qtd,
  CONCAT(catalogo.principio, ' ', catalogo.apresentacao) AS nomeItem,
  catalogo.lab_desc AS laboratorio,
  IF(catalogo.ATIVO = 'A', 'ATIVO', 'INATIVO') as ativo,
  CASE
  WHEN catalogo.tipo = 0 THEN 'MEDICAMENTO'
  WHEN catalogo.tipo = 1 THEN 'MATERIAL'
  WHEN catalogo.tipo = 3 THEN 'DIETA'
  END AS tipoItem,
  fornecedores.razaoSocial,
  entrada.valor
FROM
  entrada
  INNER JOIN catalogo ON entrada.CATALOGO_ID = catalogo.ID
  INNER JOIN fornecedores ON entrada.fornecedor = fornecedores.idFornecedores
WHERE
  entrada.data BETWEEN '{$filter['inicio']}' AND '{$filter['fim']}'
  {$compFornecedor}
  {$compItem}
ORDER BY
  dataEntrada
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function updateEntradaProdutos($novos, $anteriores)
    {
        mysql_query("BEGIN");

        foreach ($novos as $entrada => $novo) {
            $sql = <<<SQL
UPDATE entrada SET 
  entrada.CATALOGO_ID = '{$novo['item']}', 
  entrada.qtd = '{$novo['qtd']}', 
  entrada.LOTE = '{$novo['lote']}', 
  entrada.VALIDADE = '{$novo['validade']}',
  entrada.valor = '{$novo['valor']}'
WHERE
 entrada.id = '{$entrada}';
SQL;
            $rs = mysql_query($sql) or die(mysql_error());
        }

        if(!$rs){
            mysql_query("ROLLBACK");
            return false;
        }

        $sql = <<<SQL
INSERT INTO historico_mudanca_entrada (
  id, 
  usuario, 
  id_entrada, 
  data,
  catalogo_id_novo, 
  catalogo_id_anterior, 
  lote_novo, 
  lote_anterior, 
  qtd_novo, 
  qtd_anterior, 
  validade_novo, 
  validade_anterior,
  valor_novo,
  valor_anterior
) 
VALUES 
SQL;
        foreach ($anteriores as $entrada => $anterior) {
            $sql .= <<<SQL
( 
  NULL, 
  '{$_SESSION['id_user']}', 
  '{$entrada}',
  NOW(),
  '{$anterior['itemNovo']}',
  '{$anterior['itemAnterior']}',
  '{$anterior['loteNovo']}',
  '{$anterior['loteAnterior']}',
  '{$anterior['qtdNovo']}',
  '{$anterior['qtdAnterior']}',
  '{$anterior['validadeNovo']}',
  '{$anterior['validadeAnterior']}',
  '{$anterior['valorNovo']}',
   '{$anterior['valorAnterior']}'

),
SQL;
        }

        $sql = substr_replace($sql, ';', -1, 1);
        $rs = mysql_query($sql) or die(mysql_error());
        if(!$rs){
            mysql_query("ROLLBACK");
            return false;
        }
        return mysql_query("COMMIT");
    }

    public static function ajusteEstoque($ur, $inicio, $fim)
    {
        $condUr = empty($ur) ? '' : "AND historico_modificacao_estoque.ur = {$ur}";

        $sql = <<<SQL
SELECT
CONCAT(catalogo.principio, ' ',catalogo.apresentacao) as item,
catalogo.lab_desc as laboratorio,
usuarios.nome as nomeUsuario,
empresas.nome as nomeEmpresa,
DATE_FORMAT(historico_modificacao_estoque.`data`,'%d/%m/%Y') as ptBr,
historico_modificacao_estoque.`data`,
historico_modificacao_estoque.justificativa,
historico_modificacao_estoque.quantidade_anterior,
historico_modificacao_estoque.quantidade_modificada,
historico_modificacao_estoque.tipo,
CASE
  WHEN catalogo.tipo = 0 THEN 'MEDICAMENTO'
  WHEN catalogo.tipo = 1 THEN 'MATERIAL'
  WHEN catalogo.tipo = 3 THEN 'DIETA'
  END AS tipoItem
FROM
historico_modificacao_estoque
INNER JOIN catalogo ON historico_modificacao_estoque.catalogo_id = catalogo.ID
INNER JOIN usuarios ON historico_modificacao_estoque.usuario_id = usuarios.idUsuarios
INNER JOIN empresas ON historico_modificacao_estoque.ur = empresas.id
WHERE
historico_modificacao_estoque.tipo in (0,1,3) AND
historico_modificacao_estoque.`data` BETWEEN '{$inicio}' and '{$fim}'
$condUr
UNION
SELECT
cobrancaplanos.item,
'' as laboratorio,
usuarios.nome AS nomeUsuario,
empresas.nome AS nomeEmpresa,
DATE_FORMAT(historico_modificacao_estoque.`data`,'%d/%m/%Y') AS ptBr,
historico_modificacao_estoque.`data`,
historico_modificacao_estoque.justificativa,
historico_modificacao_estoque.quantidade_anterior,
historico_modificacao_estoque.quantidade_modificada,
historico_modificacao_estoque.tipo,
'Equipamento' as tipoItem
FROM
historico_modificacao_estoque
INNER JOIN usuarios ON historico_modificacao_estoque.usuario_id = usuarios.idUsuarios
INNER JOIN empresas ON historico_modificacao_estoque.ur = empresas.id
INNER JOIN cobrancaplanos ON historico_modificacao_estoque.catalogo_id = cobrancaplanos.id
WHERE
historico_modificacao_estoque.tipo = 5 AND
historico_modificacao_estoque.`data` BETWEEN '{$inicio}' and '{$fim}'
$condUr
ORDER BY
data, tipo, item desc
SQL;
        return parent::get($sql, 'assoc');



    }
}
