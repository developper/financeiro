<?php

namespace app\Models\Logistica;

use App\Models\AbstractModel;

class Solicitacoes extends AbstractModel
{

    public static function getAjusteBySolicitacaoId($idSolicitacao)
    {
        $sql = <<<SQL
        select
        historico_ajuste_autorizado_solicitacao.*,
        usuarios.nome as usuario,
        DATE_FORMAT(historico_ajuste_autorizado_solicitacao.created_at, '%d/%m/%Y') as dataBr

        FROM
        historico_ajuste_autorizado_solicitacao INNER JOIN
        usuarios on (historico_ajuste_autorizado_solicitacao.created_by = usuarios.idUsuarios)
        WHERE
        solicitacoes_id = {$idSolicitacao}

SQL;
        return parent::get($sql, 'assoc');

    }
}