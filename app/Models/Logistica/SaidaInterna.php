<?php
namespace app\Models\Logistica;

use App\Models\AbstractModel;
use App\Models\DAOInterface;

class SaidaInterna extends AbstractModel implements DAOInterface
{
    public static function getAll ()
    {
        return parent::get (self::getSQL ());
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getSQL($id = null)
    {
        $id = isset($id) && !empty($id) ? "WHERE id = {$id}" : null;
        return "SELECT * FROM saida_interna {$id} ORDER BY id";
    }

    public static function getSaidasInternas($filters)
    {
        $empresaPrincipal = $_SESSION['empresa_principal'];

        if($empresaPrincipal == '1') {
            $compUr = '';
            if(!empty($filters['ur'])){
                $compUr =  " AND saida_interna.EMPRESA_ID IN ({$filters['ur']}) " ;
            }

        } else {
            $compUr = " AND saida_interna.EMPRESA_ID = '{$empresaPrincipal}'";
        }
        $compVencidos = $filters['filtrarPor'] == 'VD' ?
            " AND saida_interna.VENCIDO = '1'" :
            " AND saida_interna.VENCIDO = '0'";

        $sql = <<<SQL
SELECT
  CONCAT(catalogo.principio, ' ', catalogo.apresentacao, '<br><b> LAB:</b> ', lab_desc) AS item,
  saida_interna.COLABORADOR AS colaborador,
  saida_interna.DATA_SAIDA_INTERNA AS dataSaida,
  saida_interna.DATA_VENCIMENTO AS vencimento,
  saida_interna.QUANTIDADE AS qtd,
  saida_interna.TIPO AS tipo,
  empresas.nome AS ur,
  catalogo.valorMedio AS custo
FROM
  saida_interna
  INNER JOIN catalogo ON saida_interna.CATALOGO_ID = catalogo.ID
  INNER JOIN empresas ON saida_interna.EMPRESA_ID = empresas.id
WHERE
  saida_interna.DATA_SAIDA_INTERNA BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}' 
  {$compUr}
  {$compVencidos}
ORDER BY 
  dataSaida DESC
SQL;




        return parent::get($sql, 'Assoc');
    }
}