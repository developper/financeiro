<?php

namespace App\Models\Exames;

use App\Services\Queue;
use App\Core\Config;

class ExamesQueue
{  

  protected $queueName;
  protected $images = array();
  protected $queue;
  protected $repository;

  public function __construct($repository)
  {
    $this->repository = $repository;
    $this->queue = new Queue;
    $this->queueName = Config::get('GS_EXAMES_QUEUE');
  }

  public function add($imageId)
  {
    $sql = sprintf("SELECT * FROM files_uploaded WHERE id = %d", $imageId);
    $rows = $this->repository->query($sql);
    foreach ($rows as $row) {
      $this->images[] = array('id' => $imageId, 'source' => $row['full_path'], 'target' => $row['path']);
    }
  }

  public function uploaded()
  {
    $data = json_encode($this->getData());
    $this->queue->push($data, $this->queueName);
  }

  protected function getData()
  {
    return ['images' => $this->images];
  }


}