<?php
namespace App\Models\Exames;

use App\Models\AbstractModel;
use	App\Models\DAOInterface;
use App\Models\FileSystem\FileUpload;
use App\Models\Medico\Prescricao;

class ExamesLaboratoriais extends AbstractModel implements DAOInterface
{
    private $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }
    const BASE_DIRECTORY = 'storage/exames/';

	public static function getAll()
	{
		return parent::get(self::getSQL(), 'assoc');
	}

	public static function getById($id)
	{
		return current(parent::get(self::getSQL($id)));
	}

	public static function getSQL($id = null)
	{
		$id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : "";
		return "SELECT * FROM exames_laboratoriais WHERE 1=1 {$id} ORDER BY descricao";
	}

	public function save($data)
    {
        session_start();
        $response = [];
        $this->conn->query("BEGIN");
        $this->conn->autocommit(FALSE);

        $userId = $_SESSION['id_user'];
        $formData = $data['formData'];
        $files = $data['files'];

        list($response[], $exameId) = $this->saveExamePaciente($formData, $userId);

        $response[] = $this->saveExameTags($formData['exames'], $exameId);

        if($files['name'][0] != '') {
            $files = $this->uploadFiles($files, $userId);

            if (!isset($files['messages'])) {
                $response[] = $this->linkExameUploads($files, $exameId);
            } else {
                return $files['messages'];
            }
        }

        if($formData['coleta'] == 'E'){
            $prescricaoData = [];
            $prescricaoData['exameId'] = $exameId;
            $prescricaoData['paciente'] = $formData['paciente'];
            $prescricaoData['inicio'] = implode("-",array_reverse(explode("/",$formData['inicio'])));
            $prescricaoData['fim'] = implode("-",array_reverse(explode("/",$formData['fim'])));
            $prescricaoData['observacao'] = $formData['observacao'];
            $prescricaoData['criador'] = $userId;
            $tags = $this->getExameTags($exameId);
            $tags = implode(', ', $tags['descricao']);
            $prescricaoData['tags'] = $tags;

            $prescricao = Prescricao::createPrescricaoAditivaLaboratorial($prescricaoData);
            if(!$prescricao){
                throw new \Exception("Erro ao criar a prescrição!");
            }
        }
        if(!in_array(false, $response))
            return $this->conn->query("COMMIT");
        $this->conn->query("ROLLBACK");
        return false;
    }

    private function saveExamePaciente($formData, $userId)
    {
        $dataInicio = implode("-",array_reverse(explode("/",$formData['inicio'])));
        $dataFim = implode("-",array_reverse(explode("/",$formData['fim'])));
        $sql = <<<SQL
INSERT INTO exames_laboratoriais_paciente (id, paciente, inicio, fim, coleta, tipo, justificativa_emergencial, observacao, created_by,cid)
VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?);
SQL;
        if($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param (
                'issssssis',
                $formData['paciente'],
                $dataInicio,
                $dataFim,
                $formData['coleta'],
                $formData['tipo'],
                $formData['justificativa_emergencial'],
                $formData['observacao'],
                $userId,
                $formData['cid']
            );
            $rs = $stmt->execute ();
            return [$rs, $stmt->insert_id];
        }
        throw new \Exception("Erro ao criar o exame: " . $this->conn->error);
    }

    private function saveExameTags($tags, $exameId)
    {
        $rs = false;
        foreach ($tags as $tag) {
            $sql = <<<SQL
INSERT INTO exames_laboratoriais_tags (id, exame_paciente, tipo_exame) 
VALUES (NULL, ?, ?);
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'ii',
                    $exameId,
                    $tag
                );
                $rs = $stmt->execute();
            }
        }
        if($rs)
            return $rs;
        throw new \Exception("Erro ao gravar as tags do exame: " . $this->conn->error);
    }

    private function linkExameUploads($files, $exameId)
    {
        $rs = false;
        $examesQueue = new ExamesQueue($this->conn);
        foreach ($files as $file) {
            $sql = <<<SQL
INSERT INTO exames_laboratoriais_upload (id, exame_paciente, upload_id) 
VALUES (NULL, ?, ?);
SQL;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(
                    'ii',
                    $exameId,
                    $file['id']
                );
                $rs = $stmt->execute();

                if($rs) {
                    $examesQueue->add($file['id']);
                }
            }
        }
        if($rs) {
            $examesQueue->uploaded();
            return $rs;
        }
        throw new \Exception("Erro ao linkar os id's dos uploads: " . $this->conn->error);
    }

    private function uploadFiles($files, $userId)
    {
        $baseDirectory = self::BASE_DIRECTORY;
        $extensions = ['jpg', 'jpeg', 'png', 'doc', 'docx', 'pdf', 'odt', 'txt', 'rtf'];
        $fileUpload = new FileUpload($baseDirectory, '5M', $extensions);
        $fileUpload->turnOnPrefix();
        list($handlerResults, $messages) = $fileUpload->process($files);
        $data = array();

        if (empty($messages)) {
            $sqls = $fileUpload->getSqlOnly($handlerResults, $userId);
            foreach ($sqls as $sql) {
                if ($this->conn->query($sql)) {
                    $result = $this->conn->query($fileUpload->getSqlById($this->conn->insert_id));
                    while ($file = $result->fetch_object()) {
                        $data[] = ['id' => $file->id, 'path' => $file->path];
                    }
                }
            }
        } else {
            foreach ($messages as $errors)
                foreach ($errors as $error)
                    $data['messages'][] = sprintf('* %s', (string) $error);
                    $file['error'][] = sprintf('* %s', (string) $error);
        }
        return $data;
    }

    public function examesListar($data)
    {
        $condExame = '';
        if(!empty($data['exames'])){
           $idExame = implode(',',$data['exames']);
            $condExame =" and exames_laboratoriais_tags.tipo_exame in ({$idExame})";
        }

        $response = [];
        $condTipoExame = $data['tipo'] != 'todos' ? " AND exames_laboratoriais_paciente.tipo = '{$data['tipo']}'" : "";
        $data['inicio'] = implode("-",array_reverse(explode("/",$data['inicio'])));
        $data['fim'] = implode("-",array_reverse(explode("/",$data['fim'])));
        $condPeriodo = $data['inicio'] != '' && $data['fim'] != '' ?
            " AND inicio BETWEEN '{$data['inicio']}' AND '{$data['fim']}' 
              AND fim BETWEEN '{$data['inicio']}' AND '{$data['fim']}'" : "";

        $sql = <<<SQL
SELECT
  exames_laboratoriais_paciente.*,
  clientes.nome AS nomePaciente,
  usuarios.nome AS usuario,
  concat('(',cid10.cid10,') ',cid10.descricao) as cid
FROM
  exames_laboratoriais_paciente
  INNER JOIN clientes ON exames_laboratoriais_paciente.paciente = clientes.idClientes
  INNER JOIN usuarios ON exames_laboratoriais_paciente.created_by = usuarios.idUsuarios
  LEFT JOIN exames_laboratoriais_tags on exames_laboratoriais_paciente.id = exames_laboratoriais_tags.exame_paciente
  LEFT JOIN cid10 ON exames_laboratoriais_paciente.cid = cid10.codigo
WHERE
exames_laboratoriais_paciente.canceled_by is null
  and paciente = '{$data['paciente']}'
  {$condTipoExame}
  {$condPeriodo}
  {$condExame}
ORDER BY
  id DESC
SQL;

        if($result = $this->conn->query ($sql)) {
            while ($row = $result->fetch_assoc()) {
                $response['exames'][$row['id']] = $row;
                $tags = $this->getExameTags($row['id']);
                $response['tags'][$row['id']] = $tags['descricao'];
                $tagsIds = $tags['id'];
                $response['files'][$row['id']] = $this->getExameFiles($row['id']);
                $response['prescricoes'][$row['id']] = $this->getPrescricoes($row['id']);
            }
        }
        return $response;
    }

    private function getExameTags($id)
    {
        $tags = [];
        $sql = <<<SQL
SELECT
  exames_laboratoriais_tags.id,
  exames_laboratoriais.descricao
FROM
  exames_laboratoriais_tags
  INNER JOIN exames_laboratoriais ON exames_laboratoriais_tags.tipo_exame = exames_laboratoriais.id
WHERE
  exame_paciente = {$id}
SQL;
        if($result = $this->conn->query ($sql)) {
            while ($row = $result->fetch_assoc()) {
                $tags['descricao'][] = $row['descricao'];
                $tags['id'][] = $row['id'];
            }
        }
        return $tags;
    }

    private function getExameFiles($id)
    {
        $files = [];
        $sql = <<<SQL
SELECT
  files_uploaded.full_path,
  files_uploaded.path,
  exames_laboratoriais_upload.id
FROM
  exames_laboratoriais_upload
  INNER JOIN files_uploaded ON exames_laboratoriais_upload.upload_id = files_uploaded.id
WHERE
  exame_paciente = {$id}
  and canceled_by is NULL
SQL;
        if($result = $this->conn->query ($sql)) {
            while ($row = $result->fetch_assoc()) {
                $files[] = ['id' => $row['id'], 'full_path' => $row['full_path'], 'path' => $row['path']];
            }
        }
        return $files;
    }

    private function getPrescricoes($id)
    {
        $prescricoes = [];

$sql = <<<SQL
SELECT
                presc.id,
				u.nome AS medico,
				DATE_FORMAT(presc.data,'%d/%m/%Y - %T') AS sdata,
				DATE_FORMAT(presc.inicio,'%d/%m/%Y') AS inicio,
				DATE_FORMAT(presc.fim,'%d/%m/%Y') AS fim,
				UPPER(c.nome) AS paciente,
				concat('<label style=\'color:blue;\'><b>Aditiva ',presc.flag,'</b></label>') as tipo_presc

			FROM
				prescricoes AS presc INNER JOIN
				clientes AS c ON (c.idClientes = presc.paciente) INNER JOIN
				usuarios AS u ON (presc.criador = u.idUsuarios)
			WHERE
  presc.exames_laboratoriais_paciente_id = {$id}
  AND presc.DESATIVADA_POR = 0
  AND presc.Carater IN (2, 3)

ORDER BY
  presc.inicio

SQL;
        if($result = $this->conn->query ($sql)) {
            while ($row = $result->fetch_assoc()) {
                $msg = "<b>#{$row['id']} </b>Prescrição {$row['tipo_presc']} período {$row['inicio']} até {$row['fim']} feita por: {$row['medico']} em {$row['sdata']}";
                $prescricoes[] = $msg;
            }
        }
        return $prescricoes;
    }

    public  function excluirExames($idExame)
    {

        session_start();
        $response = [];
        $this->conn->query("BEGIN");
        $this->conn->autocommit(FALSE);
        $userId = $_SESSION['id_user'];


        $sql = <<<SQL
        UPDATE
exames_laboratoriais_paciente
SET
canceled_at = now(),
canceled_by = ?
WHERE
id = ?

SQL;
        $rs = false;
        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param(
                'ii',
                $userId,
                $idExame
            );

            $rs = $stmt->execute();

        }

        if(!empty($rs)){
            return $this->conn->query("COMMIT");
        }else{
            $this->conn->query("ROLLBACK");
            return $rs;

        }
    }

    public  function salvarArquivo($files, $idExame, $idUser)
    {
        ini_set('display_errors', 1);
        foreach($files as $file){
            if($file['name'] != '') {
                $fileResult = $this->uploadFiles($file, $idUser);

                if ($file['error'] == 0) {
                    $response= $this->linkExameUploads($fileResult, $idExame);
                    return $response;
                } else {
                    return $file['error'];
                }
            }
        }
    }

    public  function excluirArquivo($id)
    {

        session_start();
        $response = [];
        $this->conn->query("BEGIN");
        $this->conn->autocommit(FALSE);
        $userId = $_SESSION['id_user'];

        $sql = <<<SQL
UPDATE
exames_laboratoriais_upload
SET
canceled_at = now(),
canceled_by = ?
WHERE
id = ?

SQL;
        $rs = false;
        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param(
                'ii',
                $userId,
                $id
            );
            $rs = $stmt->execute();

        }

        if(!in_array(false, $rs)){
            return $this->conn->query("COMMIT");
        }else{
            $this->conn->query("ROLLBACK");
            return $rs;

        }
    }


}