<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 04/08/2016
 * Time: 10:53
 */

namespace app\Models\Auditoria;
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/codigos.php');


use App\Models\AbstractModel;

class RelatorioNaoConformidade extends AbstractModel
{
    public static function getTopico()
    {
        $sql = <<<SQL
SELECT
relatorio_nao_conformidade_topico.id,
relatorio_nao_conformidade_topico.item
FROM
relatorio_nao_conformidade_topico
WHERE
1
order by item
SQL;
        return parent::get($sql);
    }

    public static function getJustificativasByTopico($idTopico)
    {
        $sql = "SELECT
relatorio_nao_conformidade_justificativa.id,
relatorio_nao_conformidade_justificativa.justificativa,
relatorio_nao_conformidade_topico_justificativa.obrigatorio
FROM
relatorio_nao_conformidade_topico_justificativa
INNER JOIN relatorio_nao_conformidade_justificativa ON relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_justificativa_id = relatorio_nao_conformidade_justificativa.id

WHERE

relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_topico_id = {$idTopico}

order by  justificativa";

      return parent::get($sql);
    }

    public static function getConteudo()
    {
        $sql = <<<SQL
SELECT
relatorio_nao_conformidade_topico.id,
relatorio_nao_conformidade_topico.item,
relatorio_nao_conformidade_justificativa.id,
relatorio_nao_conformidade_justificativa.justificativa,
relatorio_nao_conformidade_topico_justificativa.obrigatorio
FROM
relatorio_nao_conformidade_topico_justificativa
INNER JOIN relatorio_nao_conformidade_justificativa ON relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_justificativa_id = relatorio_nao_conformidade_justificativa.id
INNER JOIN relatorio_nao_conformidade_topico ON relatorio_nao_conformidade_topico_justificativa.relatorio_nao_conformidade_topico_id = relatorio_nao_conformidade_topico.id
WHERE
1
order by item, justificativa
SQL;
        return parent::get($sql);

    }

    public static function saveRelatorioNaoConfomidade($paciente,
                                                       $periodoInicio,
                                                       $periodoFim,
                                                       $usuario,
                                                       $empresa,
                                                       $acao)
    {

        $colunas ='';
        $valores = '';
        if($acao == 'finalizar'){
            $colunas =', finalizado_at, finalizado_by';
            $valores = ", now(), {$usuario}";
        }
        $sql = <<<SQL
INSERT INTO
relatorio_nao_conformidade
(id,
paciente_id,
usuario_id,
empresa_id,
inicio,
fim,
data,
created_by,
created_at
$colunas)
VALUES
('',
$paciente,
$usuario,
$empresa,
'$periodoInicio',
'$periodoFim',
now(),
$usuario,
now()
$valores)
SQL;
        $response = mysql_query($sql);
        if(!$response)
            return $response;
        $idRelatorioNaoConformidade = mysql_insert_id();

        return $idRelatorioNaoConformidade;


    }

    public static function saveItemRelatorioNaoConformidade($topico,
                                                            $item,
                                                            $justificativa,
                                                            $itemProfissionais,
                                                            $idRelatorioNaoConformidade,
                                                            $usuario,
                                                            $status,
                                                            $idReferencia)
    {
        mysql_query('BEGIN');

        $condReferencia ='';
        if(!empty($idReferencia)){
            $condReferencia = ",item_referencia";
            $idReferencia = ",{$idReferencia}";
        }
        $sql = <<<SQL
INSERT
INTO
relatorio_nao_conformidade_item
(
id,
relatorio_nao_conformidade_id,
relatorio_nao_conformidade_topico,
relatorio_nao_conformidade_justificativa,
justificativa,
created_by,
created_at,
status
$condReferencia
)
VALUES
(
'',
$idRelatorioNaoConformidade,
$topico,
$item,
'$justificativa',
$usuario,
now(),
'{$status}'
{$idReferencia})

SQL;
        $response = mysql_query($sql);

        if($itemProfissionais != '') {
            $idItem = mysql_insert_id();

            $profissionais = explode(',', $itemProfissionais);

            $sql = <<<SQL
INSERT INTO relatorio_nao_conformidade_item_profissional VALUES 
SQL;
            $queries = [];
            foreach ($profissionais as $profissional) {
                $queries[] = "(NULL, '{$idItem}', '{$profissional}')";
            }

            $sql .= implode(',', $queries) . ';';

            $response = mysql_query($sql);
        }
        return $response;
    }

    public static function getRelatorioNaoConformidadeById($id)
    {
        $sql = <<<SQL
SELECT
clientes.nome AS paciente,
clientes.codigo AS codigoPaciente,
usuarios.nome AS usuario,
empresas.nome AS empresa,
relatorio_nao_conformidade_topico.item AS topico,
relatorio_nao_conformidade_justificativa.justificativa AS item,
relatorio_nao_conformidade.*,
relatorio_nao_conformidade_item.relatorio_nao_conformidade_topico,
relatorio_nao_conformidade_item.relatorio_nao_conformidade_justificativa,
relatorio_nao_conformidade_item.justificativa,
relatorio_nao_conformidade_item.id as idRelatorioItem,
relatorio_nao_conformidade_item.status,
relatorio_nao_conformidade_item.item_referencia

FROM
relatorio_nao_conformidade
INNER JOIN relatorio_nao_conformidade_item ON relatorio_nao_conformidade.id = relatorio_nao_conformidade_item.relatorio_nao_conformidade_id
INNER JOIN clientes ON relatorio_nao_conformidade.paciente_id = clientes.idClientes
INNER JOIN usuarios ON relatorio_nao_conformidade.usuario_id = usuarios.idUsuarios
INNER JOIN empresas ON relatorio_nao_conformidade.empresa_id = empresas.id
INNER JOIN relatorio_nao_conformidade_justificativa ON relatorio_nao_conformidade_item.relatorio_nao_conformidade_justificativa = relatorio_nao_conformidade_justificativa.id
INNER JOIN relatorio_nao_conformidade_topico ON relatorio_nao_conformidade_item.relatorio_nao_conformidade_topico = relatorio_nao_conformidade_topico.id
WHERE
relatorio_nao_conformidade.id = {$id}
and
relatorio_nao_conformidade_item.canceled_at is null

SQL;
        return parent::get($sql);
    }

    public static function getRelatorioNaoConformidadeByFiltros($filtros)
    {
        $condEmpresa 		= $filtros['empresa'] != '' ? " AND empresas.id = '{$filtros['empresa']}'" : "";
        $condPaciente = $filtros['paciente'] != '' ? " AND clientes.idClientes = '{$filtros['paciente']}'" : "";
        $condFinalizado = '';
        if($_SESSION['empresa_principal'] != 1){
            $condFinalizado = "AND relatorio_nao_conformidade.finalizado_at is not null";
        }



        $sql = <<<SQL
        SELECT
relatorio_nao_conformidade.id,
clientes.nome AS paciente,
usuarios.nome AS usuario,
empresas.nome AS empresa,
relatorio_nao_conformidade.inicio,
relatorio_nao_conformidade.fim,
relatorio_nao_conformidade.data,
clientes.codigo AS codigoPaciente,
relatorio_nao_conformidade.finalizado_at
FROM
relatorio_nao_conformidade
INNER JOIN clientes ON relatorio_nao_conformidade.paciente_id = clientes.idClientes
INNER JOIN usuarios ON relatorio_nao_conformidade.usuario_id = usuarios.idUsuarios
INNER JOIN empresas ON relatorio_nao_conformidade.empresa_id = empresas.id
WHERE
(relatorio_nao_conformidade.inicio BETWEEN '{$filtros['inicio']}' and '{$filtros['fim']}' OR
relatorio_nao_conformidade.fim BETWEEN '{$filtros['inicio']}' and '{$filtros['fim']}')
$condEmpresa
$condPaciente
$condFinalizado
ORDER BY
empresa,
paciente,
relatorio_nao_conformidade.data

SQL;


        return parent::get($sql);

        
    }

    public static function updateRelatorioNaoConformidade(
        $paciente,
        $periodoInicio,
        $periodoFim,
        $usuario,
        $empresa,
        $id_relatorio,
        $itens,
        $acao)

    {
        $update = "";
        if($acao == 'finalizar'){
            $update = ", finalizado_at = now(), finalizado_by = {$usuario} ";
        }

        $sql = <<<SQL
UPDATE
relatorio_nao_conformidade
SET
usuario_id =         $usuario,
empresa_id = $empresa,
inicio = '$periodoInicio',
fim = '$periodoFim',
update_at = now(),
data = now(),
update_by =$usuario
$update
WHERE
id = $id_relatorio
SQL;
        mysql_query('begin');
        $response = mysql_query($sql);
        if(!$response){
            mysql_query('rollback');
            return('Houve um problema ao editar Ralatório de Não Conformidades.  ' );
        }

        savelog(@mysql_escape_string(addslashes($sql)));

        foreach($itens as $item){
            //colocando item de origem como resolvido e cancelando o item do relatório para que não apareça.
            if($item['status'] == 'RESOLVIDO'){
                $sql = <<<SQL
        UPDATE
        `relatorio_nao_conformidade_item`
        SET
        resolvido_by = {$usuario},
        resolvido_at = now()
        WHERE
        id = {$item['idReferencia']}

SQL;
                $response = mysql_query($sql);
                if(!$response){
                    mysql_query('rollback');
                    return('Houve um problema ao colocar um item como Resolvido. Erro: 1 ');
                }
                savelog(@mysql_escape_string(addslashes($sql)));
                $sql = <<<SQL
UPDATE
relatorio_nao_conformidade_item
SET
canceled_by = $usuario,
canceled_at = now(),
justificativa = '{$item['justificativa']}',
status= '{$item['status']}',
resolvido_by = {$usuario},
resolvido_at = now()
WHERE
id = {$item['id']}
SQL;

                $response = mysql_query($sql);
                if(!$response){
                    mysql_query('rollback');
                    return('Houve um problema ao cancelar item. Erro:2 ');
                }
                savelog(@mysql_escape_string(addslashes($sql)));




            }elseif($item['remover'] == 'S'){
            /// cancelando um item do relatório
                $sql = <<<SQL
UPDATE
relatorio_nao_conformidade_item
SET
canceled_by = $usuario,
canceled_at = now()
WHERE
id = {$item['id']}
SQL;

                $response = mysql_query($sql);
                if(!$response){
                    mysql_query('rollback');
                    return('Houve um problema ao cancelar item. Erro: ');
                }
                savelog(@mysql_escape_string(addslashes($sql)));

            }else if($item['id'] == 0){
                //inserindo um novo item
                $sql = <<<SQL
INSERT
INTO
relatorio_nao_conformidade_item
(
id,
relatorio_nao_conformidade_id,
relatorio_nao_conformidade_topico,
relatorio_nao_conformidade_justificativa,
justificativa,
created_by,
created_at,
status
)
VALUES
(
'',
$id_relatorio,
{$item['topico']},
{$item['item']},
'{$item['justificativa']}',
$usuario,
now(),
'{$item['status']}')
SQL;

                $response = mysql_query($sql);
                if(!$response){
                    mysql_query('rollback');
                    return('Houve um problema ao adicionar item. Erro: ' );
                }
                $idItem = mysql_insert_id();

                savelog(@mysql_escape_string(addslashes($sql)));

                if($item['profissionais'] != '') {


                    $profissionais = explode(',', $item['profissionais']);

                    $sql = <<<SQL
INSERT INTO relatorio_nao_conformidade_item_profissional VALUES 
SQL;
                    $queries = [];
                    foreach ($profissionais as $profissional) {
                        $queries[] = "(NULL, '{$idItem}', '{$profissional}')";
                    }

                    $sql .= implode(',', $queries) . ';';

                    $response = mysql_query($sql);
                    if(!$response){
                        mysql_query('rollback');
                        return('Houve um problema ao adicionar o profissinonal. ' );
                    }
                }
                savelog(@mysql_escape_string(addslashes($sql)));
            }else{
                $sql = <<<SQL
UPDATE
relatorio_nao_conformidade_item
SET
justificativa = '{$item['justificativa']}',
status= '{$item['status']}',
updated_by = {$usuario},
updated_at = now()
WHERE
id = {$item['id']}
SQL;

                $response = mysql_query($sql);
                if(!$response){
                    mysql_query('rollback');
                    return('Houve um problema ao atulizar item.  ');
                }
                savelog(@mysql_escape_string(addslashes($sql)));
            }
        }

        mysql_query('commit');
        echo $id_relatorio;
        return ;

    }

    public static function cancelarRelatorioNaoConformidade($id, $usuario)
    {
        $sql = <<<SQL
UPDATE
relatorio_nao_conformidade
SET
canceled_at = now(),
canceled_by = $usuario
WHERE
id = $id
SQL;
        $response = mysql_query($sql);
        if(!$response){
            return('Houve um problema ao cancelar o relatório ' );
        }
        echo $id;
        return ;




    }

    public static function getCondensadoRelatorioNaoConformidade($periodoInicio,$periodoFim,$urBusca)
    {
        $urBusca = implode(',', $urBusca);
        $condEmpresa = $urBusca != '' ? " AND empresas.id IN ({$urBusca})" : "";


        $sql = <<<SQL
SELECT
clientes.nome AS paciente,
clientes.codigo AS codigoPaciente,
usuarios.nome AS usuario,
empresas.nome AS empresa,
relatorio_nao_conformidade_topico.item AS topico,
relatorio_nao_conformidade_justificativa.justificativa AS item,
relatorio_nao_conformidade.*,
relatorio_nao_conformidade_item.relatorio_nao_conformidade_topico,
relatorio_nao_conformidade_item.relatorio_nao_conformidade_justificativa,
relatorio_nao_conformidade_item.justificativa,
relatorio_nao_conformidade_item.id as idRelatorioItem

FROM
relatorio_nao_conformidade
INNER JOIN relatorio_nao_conformidade_item ON relatorio_nao_conformidade.id = relatorio_nao_conformidade_item.relatorio_nao_conformidade_id
INNER JOIN clientes ON relatorio_nao_conformidade.paciente_id = clientes.idClientes
INNER JOIN usuarios ON relatorio_nao_conformidade.usuario_id = usuarios.idUsuarios
INNER JOIN empresas ON relatorio_nao_conformidade.empresa_id = empresas.id
INNER JOIN relatorio_nao_conformidade_justificativa ON relatorio_nao_conformidade_item.relatorio_nao_conformidade_justificativa = relatorio_nao_conformidade_justificativa.id
INNER JOIN relatorio_nao_conformidade_topico ON relatorio_nao_conformidade_item.relatorio_nao_conformidade_topico = relatorio_nao_conformidade_topico.id
WHERE
relatorio_nao_conformidade_item.canceled_at is null and
(relatorio_nao_conformidade.inicio BETWEEN '{$periodoInicio}' and '{$periodoFim}' OR
relatorio_nao_conformidade.fim BETWEEN '{$periodoInicio}' and '{$periodoFim}')
AND finalizado_by is not NULL
$condEmpresa
ORDER BY
empresa,
paciente,
relatorio_nao_conformidade.data

SQL;


        return parent::get($sql);

    }

    public static function reabrirRelatorioNoaConformidades ($id, $justificativa,$usuario )
    {
        $sql = <<<SQL
        UPDATE
        `relatorio_nao_conformidade`
        SET
        finalizado_by = NULL,
        finalizado_at = NULL,
        reopened_by = {$usuario},
         reopened_at = now(),
         justificativa_reopened = '{$justificativa}'
         WHERE
         id = {$id}

SQL;

        $response = mysql_query($sql);

        if(!$response){
            echo 'Erro ao reabrir ralatório' ;
            return ;
        }

        savelog(@mysql_escape_string(addslashes($sql)));


        return $id;

    }

    public static function saveItemResolvidoRelatorioNaoConformidade($idReferencia, $usuario)
    {
        $sql = <<<SQL
        UPDATE
        `relatorio_nao_conformidade_item`
        SET
        resolvido_by = {$usuario},
        resolvido_at = now()
        WHERE
        id = {$idReferencia}

SQL;

        $response = mysql_query($sql);
        return $response;




    }

    public static function salvarComentarioRelatorioNaoConformidade($itemId, $comentario, $userId)
    {
        $sql = <<<SQL
            INSERT
             INTO
            relatorio_nao_conformidade_item_comentario
            (
            id,
            relatorio_nao_conformidade_item_id,
            comentario,
            created_by,
            created_at
            )
            VALUES
            (
            '',
            $itemId,
            '$comentario',
            $userId,
            now()
            )
SQL;
        $response = mysql_query($sql);

        if(!$response){

            return -1;
        }
        $idComentario = mysql_insert_id();
        savelog(@mysql_escape_string(addslashes($sql)));
        return $idComentario;
    }

    public static function removerComentarioRelatorioNaoConformidade($comentarioId, $userId)
    {
        $sql = <<<SQL
            UPDATE
            relatorio_nao_conformidade_item_comentario
            SET
            canceled_at = now(),
            canceled_by = $userId
            where
            id = $comentarioId

SQL;
        $response = mysql_query($sql);

        if(!$response){

            return -1;
        }

        savelog(@mysql_escape_string(addslashes($sql)));
        return $comentarioId;
    }

    public static function getComentariosRelatorioNaoConformidadeById($id)
    {
        $sql = <<<SQL
SELECT
relatorio_nao_conformidade_item_comentario.*,
usuarios.nome,
DATE_FORMAT(relatorio_nao_conformidade_item_comentario.created_at,'%d/%m/%Y %H:%i') as data,
usuarios.idUsuarios
FROM
relatorio_nao_conformidade
INNER JOIN relatorio_nao_conformidade_item ON relatorio_nao_conformidade.id = relatorio_nao_conformidade_item.relatorio_nao_conformidade_id
LEFT JOIN relatorio_nao_conformidade_item_comentario ON relatorio_nao_conformidade_item.id = relatorio_nao_conformidade_item_comentario.relatorio_nao_conformidade_item_id
LEFT JOIN usuarios on relatorio_nao_conformidade_item_comentario.created_by = usuarios.idUsuarios
WHERE
relatorio_nao_conformidade.id = {$id}
and
relatorio_nao_conformidade_item.canceled_at is null AND
relatorio_nao_conformidade_item_comentario.canceled_at is null
SQL;
        return parent::get($sql);

    }

    public static function getComentariosRelatorioNaoConformidadeByItemId($id)
    {
        $sql = <<<SQL
SELECT
relatorio_nao_conformidade_item_comentario.*,
usuarios.nome,
DATE_FORMAT(relatorio_nao_conformidade_item_comentario.created_at,'%d/%m/%Y %H:%i') as data,
usuarios.idUsuarios
FROM
relatorio_nao_conformidade
INNER JOIN relatorio_nao_conformidade_item ON relatorio_nao_conformidade.id = relatorio_nao_conformidade_item.relatorio_nao_conformidade_id
LEFT JOIN relatorio_nao_conformidade_item_comentario ON relatorio_nao_conformidade_item.id = relatorio_nao_conformidade_item_comentario.relatorio_nao_conformidade_item_id
LEFT JOIN usuarios on relatorio_nao_conformidade_item_comentario.created_by = usuarios.idUsuarios
WHERE
relatorio_nao_conformidade_item_comentario.relatorio_nao_conformidade_item_id = {$id}
and
relatorio_nao_conformidade_item.canceled_at is null AND
relatorio_nao_conformidade_item_comentario.canceled_at is null
SQL;
        return parent::get($sql, 'Assoc');

    }

    public static function getProfissionaisRelatorioNaoConformidadeById($id)
    {
        $sql = <<<SQL
SELECT
  relatorio_nao_conformidade_item_profissional.profissional,
  profissionais.descricao
FROM
relatorio_nao_conformidade_item_profissional 
INNER JOIN profissionais ON relatorio_nao_conformidade_item_profissional.profissional = profissionais.id
WHERE
relatorio_nao_conformidade_item_profissional.relatorio_nao_conformidade_item_id = {$id}
SQL;
        return parent::get($sql, 'Assoc');

    }
}