<?php

namespace app\Models\Auditoria;

use App\Models\AbstractModel;

class Solicitacao extends AbstractModel
{

    public static function getRelatorioPedidosLiberados($filters)
    {
        $groupBy        = '';
        $quantidade     = 'solicitacoes.autorizado, ';
        $dataLiberacao  = 'solicitacoes.data_auditado as dataLiberacao,';
        $nomeItem       = "CONCAT(catalogo.principio, '', catalogo.apresentacao) AS nomeItem, ";
        $innerCatalogo  = ' INNER JOIN catalogo ON solicitacoes.CATALOGO_ID = catalogo.ID ';
        $compAuditor    = $filters['auditor'] != '' ? " AND solicitacoes.AUDITOR_ID = '{$filters['auditor']}'" : "";
        $compPaciente   = $filters['paciente'] != '' ? " AND solicitacoes.paciente = '{$filters['paciente']}'" : "";
        $tipo           = "AND solicitacoes.tipo in (0,1,3)";
        $tipoUnion      = "AND solicitacoes.tipo = 5";

        if($filters['tipoRelatorio'] == 'sintetico') {
            $quantidade = 'SUM(solicitacoes.autorizado) as autorizado, ';
            $groupBy = ' GROUP BY dataLiberacao';
            $tipo = '';
            $tipoUnion = '';
        }

        $sqlUnion = <<<SQL
UNION
		SELECT
	solicitacoes.idSolicitacoes,
	usuarios.nome AS auditor,
	cobrancaplanos.item AS nomeItem,
	clientes.nome AS paciente,
	{$quantidade}
	solicitacoes.DATA_SOLICITACAO AS dataSolicitacao,
	solicitacoes.data_auditado as dataLiberacao,
	(case
	when solicitacoes.TIPO_SOLICITACAO in (2,5,9) then 'Periodica'
	when solicitacoes.TIPO_SOLICITACAO in (3,6,8) then 'Aditiva'
	when solicitacoes.TIPO_SOLICITACAO = -1  then 'Avulsa'
	when solicitacoes.TIPO_SOLICITACAO = -2  then 'Emergencia'
	end
	) as tipoSolicitacao,
	(case
	when solicitacoes.status = 2 then ' - Entrega Imediata'
	 else
	 ''
	 end) as imediata
FROM
	solicitacoes
	INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
	INNER JOIN usuarios ON solicitacoes.AUDITOR_ID = usuarios.idUsuarios
	INNER JOIN cobrancaplanos on solicitacoes.CATALOGO_ID = cobrancaplanos.id
WHERE
	solicitacoes.data_auditado BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
	AND (solicitacoes.data_auditado != NULL OR solicitacoes.data_auditado != '0000-00-00 00:00:00')
	AND (solicitacoes.status != -1 OR solicitacoes.status != -2)
	{$tipoUnion}
	{$compAuditor}
	{$compPaciente}
	{$groupBy}
SQL;

        $sql = <<<SQL
SELECT
	solicitacoes.idSolicitacoes,
	usuarios.nome AS auditor,
	{$nomeItem}
	clientes.nome AS paciente,
	{$quantidade}
	solicitacoes.DATA_SOLICITACAO AS dataSolicitacao,
	{$dataLiberacao}
	(case
	when solicitacoes.TIPO_SOLICITACAO in (2,5,9) then 'Periodica'
	when solicitacoes.TIPO_SOLICITACAO in (3,6,8) then 'Aditiva'
	when solicitacoes.TIPO_SOLICITACAO = -1  then 'Avulsa'
	when solicitacoes.TIPO_SOLICITACAO = -2  then 'Emergencia'
	end
	) as tipoSolicitacao,
	(case
	when solicitacoes.status = 2 then ' - Entrega Imediata'
	 else
	 ''
	 end) as imediata

FROM
	solicitacoes
	INNER JOIN clientes ON solicitacoes.paciente = clientes.idClientes
	INNER JOIN usuarios ON solicitacoes.AUDITOR_ID = usuarios.idUsuarios
	{$innerCatalogo}
WHERE
	solicitacoes.data_auditado BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
	AND (solicitacoes.data_auditado != NULL OR solicitacoes.data_auditado != '0000-00-00 00:00:00')
	AND (solicitacoes.status != -1 OR solicitacoes.status != -2)
	{$compAuditor}
	{$compPaciente}
	{$tipo}
	{$groupBy}
	{$sqlUnion}
ORDER BY
	{$filters['ordenarPor']}
SQL;
        return parent::get($sql, 'assoc');
    }

}