<?php

namespace app\Models\Auditoria;

use App\Models\AbstractModel;

class Fatura extends AbstractModel
{

	public static function getRelatorioFaturasLiberadas($filters)
	{
        $compAuditor = $filters['auditor'] != '' ? " AND fatura.created_by = '{$filters['auditor']}'" : "";

        $sql = <<<SQL
SELECT
	fatura.ID,
	fatura.ORCAMENTO,
	usuarios.nome AS auditor,
	clientes.nome AS paciente,
	fatura.DATA_INICIO AS inicio,
	fatura.DATA_FIM AS fim,
	fatura.created_at as dataLiberacao
FROM
	fatura
	INNER JOIN clientes ON fatura.PACIENTE_ID = clientes.idClientes
	INNER JOIN usuarios ON fatura.created_by = usuarios.idUsuarios
WHERE
	fatura.created_at BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
	AND fatura.ORCAMENTO = 0
	{$compAuditor}
ORDER BY
  {$filters['ordenarPor']}
SQL;
        return parent::get($sql, 'assoc');
    }

	public static function getRelatorioFaturamentoEnviado($filters)
	{


		$compAuditor 	= $filters['auditor'] != '' ? " AND fatura.data_envio_plano_by = '{$filters['auditor']}'" : "";
		$compPaciente 	= $filters['paciente'] != '' ? " AND fatura.PACIENTE_ID = '{$filters['paciente']}'" : "";
		$compUr 		= $filters['ur'] != '' ? " AND empresas.id = '{$filters['ur']}'" : "";
		$compPlano 		= $filters['plano'] != '' ? " AND planosdesaude.id = '{$filters['plano']}'" : "";

		$sql = <<<SQL
SELECT
	fatura.ID,
	clientes.nome AS paciente,
	usuarios.nome AS auditor,
	planosdesaude.nome AS convenio,
	empresas.nome AS ur,
	fatura.DATA_ENVIO_PLANO AS dataEnvio,
	DATE_FORMAT(fatura.DATA_INICIO, '%d/%m/%Y') AS inicio,
	DATE_FORMAT(fatura.DATA_FIM, '%d/%m/%Y') AS fim,
	SUM(
		(faturamento.QUANTIDADE * faturamento.VALOR_FATURA) +
		(
			(faturamento.QUANTIDADE * faturamento.VALOR_FATURA) * (faturamento.DESCONTO_ACRESCIMO/100)
		)
	) AS valor,
	fatura.protocolo_envio,
	fatura.remocao,
	fatura.PLANO_ID 
FROM
	fatura
	INNER JOIN faturamento ON fatura.ID = faturamento.FATURA_ID
	INNER JOIN clientes ON fatura.PACIENTE_ID = clientes.idClientes
	INNER JOIN planosdesaude ON fatura.PLANO_ID = planosdesaude.id
	INNER JOIN empresas ON clientes.empresa = empresas.id
	INNER JOIN usuarios ON fatura.data_envio_plano_by = usuarios.idUsuarios
WHERE
	faturamento.CANCELADO_POR IS NULL
	AND fatura.CANCELADO_POR IS NULL
	AND fatura.ORCAMENTO = 0
	AND fatura.DATA_ENVIO_PLANO BETWEEN '{$filters['inicio']} 00:00:00' AND '{$filters['fim']} 23:59:59'
	{$compAuditor}
	{$compPaciente}
	{$compUr}
	{$compPlano}
GROUP BY
  fatura.ID
ORDER BY
  {$filters['ordenarPor']}
SQL;
		return parent::get($sql, 'assoc');
	}

    public static function getModalidadeByFaturaId($faturas)
    {
        $faturas = implode(',', $faturas);
        $sql = <<<SQL
SELECT
    f.FATURA_ID,
    vc.idCobranca AS cod_item
FROM
    faturamento AS f
LEFT JOIN valorescobranca AS vc ON (f.CATALOGO_ID = vc.id)
WHERE
    f.FATURA_ID IN ({$faturas})
AND f.TIPO = 2
AND f.TABELA_ORIGEM = 'valorescobranca'
AND f.CANCELADO_POR IS NULL
AND vc.idCobranca IN (20, 21, 51, 60, 61, 77)
UNION
    SELECT
        f.FATURA_ID,
        vc.id AS cod_item
    FROM
        faturamento AS f
    LEFT JOIN cobrancaplanos AS vc ON (f.CATALOGO_ID = vc.id)
    WHERE
        f.FATURA_ID IN ({$faturas})
    AND f.TIPO = 2
    AND f.TABELA_ORIGEM = 'cobrancaplano'
    AND f.CANCELADO_POR IS NULL
    AND f.CATALOGO_ID IN (20, 21, 51, 60, 61, 77)
SQL;
        return parent::get($sql, 'Assoc');
    }

}