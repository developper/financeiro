<?php
namespace app\Models\Autorizacao;

use App\Models\AbstractModel,
    \App\Models\DAOInterface;
use App\Models\Administracao\Planos;
use App\Models\Administracao\ProrrogacaoPlanserv;
use App\Models\DB\ConnMysqli;
use App\Models\Autorizacao\AutorizacaoQueue;
use App\Models\FileSystem\FileUpload;
use App\Models\Medico\Avaliacao;
use App\Models\Medico\Prorrogacao;
use App\Services\Autorizacoes\RegraFluxoAutorizacoes;


class ComentarioAutorizacao extends AbstractModel implements DAOInterface
{
    public static function getAll ()
    {
        return parent::get (self::getSQL (), 'Assoc');
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getSQL($id = null)
    {
        return "SELECT * FROM autorizacao";
    }

    public static function getByFaturaOrcamentoAutorizacao($orcamento = null, $autorizacao = null)
    {
        $comp = $orcamento != null ?
            " AND autorizacao_comentarios.fatura_orcamento_id = '{$orcamento}'" :
            " AND autorizacao_comentarios.autorizacao_id = '{$autorizacao}'";

        $sql = <<<SQL
SELECT 
  autorizacao_comentarios.id,
  autorizacao_comentarios.autorizacao_id,
  autorizacao_comentarios.fatura_orcamento_id,
  autorizacao_comentarios.usuario_id,
  usuarios.nome AS usuario,
  autorizacao_comentarios.data_hora,
  autorizacao_comentarios.comentario
FROM
  autorizacao_comentarios
  INNER JOIN usuarios ON autorizacao_comentarios.usuario_id = usuarios.idUsuarios
WHERE
  deleted_by IS NULL
  {$comp}
SQL;
        return parent::get ($sql, 'assoc');
    }

    public static function save($comentario, $orcamento = null, $autorizacao = null)
    {
        $usuario = $_SESSION['id_user'];

        $sql = <<<SQL
INSERT INTO autorizacao_comentarios 
(
  id,
  autorizacao_id,
  fatura_orcamento_id,
  usuario_id,
  data_hora,
  comentario
) VALUES (
  NULL,
  '{$autorizacao}',
  '{$orcamento}',
  '{$usuario}',
  NOW(),
  '{$comentario}'
)
SQL;
        $rs = mysql_query($sql) ;

        if(!$rs) {
            return false;
        }
        return mysql_insert_id();
    }

    public static function delete($comentario)
    {
        $sql = "UPDATE autorizacao_comentarios SET deleted_at = NOW(), deleted_by = '{$_SESSION['id_user']}' WHERE id = '{$comentario}'";
        $rs = mysql_query($sql) ;

        if(!$rs) {
            return false;
        }
        return true;
    }

    public static function getTotalComentariosByOrcamentoIds($orcamentos)
    {
        $orcamentos = implode(', ', $orcamentos);
        $sql = <<<SQL
SELECT 
  COUNT(autorizacao_comentarios.id) AS total_comentarios,
  autorizacao_comentarios.fatura_orcamento_id
FROM
  autorizacao_comentarios
WHERE
  autorizacao_comentarios.fatura_orcamento_id IN ('{$orcamentos}')
  AND deleted_by IS NULL
GROUP BY 
  autorizacao_comentarios.fatura_orcamento_id
SQL;
        //echo '<pre>'; die($sql);
        return parent::get($sql, 'Assoc');
    }
}
