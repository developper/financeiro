<?php
namespace app\Models\Autorizacao;

use App\Models\AbstractModel,
    \App\Models\DAOInterface;
use App\Models\Administracao\Planos;
use App\Models\Administracao\ProrrogacaoPlanserv;
use App\Models\DB\ConnMysqli;
use App\Models\Autorizacao\AutorizacaoQueue;
use App\Models\FileSystem\FileUpload;
use App\Models\Medico\Avaliacao;
use App\Models\Medico\Prorrogacao;
use App\Services\Autorizacoes\RegraFluxoAutorizacoes;


class Autorizacao extends AbstractModel implements DAOInterface
{
    const BASE_DIRECTORY = 'storage/autorizacoes/';

    public static function getAll ()
    {
        return parent::get (self::getSQL (), 'Assoc');
    }

    public static function getById($id)
    {
        return current(parent::get(self::getSQL($id)));
    }

    public static function getSQL($id = null)
    {
        return "SELECT * FROM autorizacao";
    }

    public static function getArquivosAutorizacao($autorizacao)
    {
        $sql = <<<SQL
SELECT 
  autorizacao_arquivos.id AS autorizacao_arquivo_id,
  autorizacao_arquivos.data_autorizacao,
  files_uploaded.id AS files_uploaded_id,
  files_uploaded.path,
  files_uploaded.full_path
FROM
  autorizacao_arquivos
  INNER JOIN autorizacao ON autorizacao_arquivos.autorizacao = autorizacao.id
  INNER JOIN files_uploaded ON autorizacao_arquivos.files_uploaded_id = files_uploaded.id
WHERE
  autorizacao.id = '{$autorizacao}'
SQL;
        return parent::get ($sql, 'assoc');

    }

    public static function save($data, $files)
    {
        $usuario = $_SESSION['id_user'];

        mysql_query("BEGIN");


        $sql = <<<SQL
INSERT INTO autorizacao 
(
  id,
  paciente,
  usuario,
  origem,
  tipo,
  carater,
  status_geral,
  created_at,
  created_by
) VALUES (
  NULL,
  '{$data['paciente']}',
  '{$usuario}',
  '{$data['item_id']}',
  '{$data['origem']}',
  '{$data['carater']}',
  '{$data['status_geral']}',
  NOW(),
  '{$usuario}'
)
SQL;
        $rs = mysql_query($sql) ;

        if(!$rs) {
            echo "Houve um problema ao salvar a autorização!";
            mysql_query("ROLLBACK");
            return false;
        }

        $autorizacao_id = mysql_insert_id();
        $carater = $data['carater'] == 'avaliacao' ? 'prorrogacao' : $data['carater'];

        $hasItens = false;
        $hasAutorizado = false;


        foreach ($data['quantidade_autorizada'] as $item => $qtd) {
            $itemExp = explode('-', $item);

            $fatId = $itemExp[0];
            $tipo = $itemExp[1];
            $tabela = $itemExp[2];
            $catalogo_id = $itemExp[3];

            if ($data['status_item'][$item] == '2' || $data['status_item'][$item] == '') {
                $hasItens = true;
            }

            if ($data['status_item'][$item] != '3') {
                $hasAutorizado = true;
            }

            $sql = <<<SQL
INSERT INTO autorizacao_itens
(
  id,
  autorizacao,
  origem_item_id,
  catalogo_id,
  tipo,
  tabela_origem,
  qtd_solicitada,
  qtd_autorizada,
  status_item,
  data_autorizacao,
  autorizado_por,
  autorizado_em
) VALUES (
  NULL,
  '{$autorizacao_id}',
  '{$fatId}',
  '{$catalogo_id}',
  '{$tipo}',
  '{$tabela}',
  '{$data['quantidade_solicitada'][$item]}',
  '{$qtd}',
  '{$data['status_item'][$item]}',
  '{$data['data_autorizacao'][$item]}',
  '{$usuario}',
  NOW()
)
SQL;
            $rs = mysql_query($sql);

            if (!$rs) {
                echo "Houve um problema ao salvar um item da autorização!";
                mysql_query("ROLLBACK");
                return false;
            }

            $autorizacao_itens_id = mysql_insert_id();

            if($data['data_autorizacao'][$item] > 0 && ($data['status_item'][$item] == 1 || $data['status_item'][$item] == 2)){

                $itensAutorizados[] = [
                    'autorizacao_item_id' => $autorizacao_itens_id,
                    'inicio' => $data['inicio'],
                    'fim' => $data['fim'],
                    'plano_id' => $data['plano_id'],
                    'paciente_id' => $data['paciente'],
                    'quantidade_autorizada' => $data['quantidade_autorizada'][$item],
                    'item_id' => $catalogo_id,
                    'tipo' => $tipo,
                    'tipo_solicitacao' => $carater,
                    'grupo' => $data['categoria'][$item]
                 ];

            }

            $sql = <<<SQL
INSERT INTO historico_autorizacao_itens
(
  id,
  autorizacao_itens_id,  
  qtd_autorizada,
  status_item,
  data_autorizacao,
  autorizado_por,
  autorizado_em,
  origem_item_id
) VALUES (
  NULL,
  '{$autorizacao_itens_id}',  
  '{$qtd}',
  '{$data['status_item'][$item]}',
  '{$data['data_autorizacao'][$item]}',
  '{$usuario}',
  NOW(),
  '{$fatId}'
)
SQL;
            $rs = mysql_query($sql);

            if (!$rs) {
                echo "Houve um erro ao salvar o histórico da autorização!";
                mysql_query("ROLLBACK");
                return false;
            }
        }

        $files = $files['arquivos_autorizacao'];

        if($files['name'][0] != '') {
            $files = self::uploadFiles($files, $usuario);

            if (!isset($files['messages'])) {
                //$autorizacaoQueue = new AutorizacaoQueue(ConnMysqli::getConnection());
                $data['data_autorizacao_arquivo'] = $data['data_autorizacao_arquivo'] != '' ? $data['data_autorizacao_arquivo'] : (new \DateTime())->format('Y-m-d');
                foreach ($files as $file) {
                    $sql = <<<SQL
INSERT INTO autorizacao_arquivos
(
  id,
  autorizacao,
  files_uploaded_id,
  data_autorizacao
) VALUES (
  NULL,
  '{$autorizacao_id}',
  '{$file['id']}',
  '{$data['data_autorizacao_arquivo']}'
)
SQL;
                    $rs = mysql_query($sql);

                    if(!$rs) {
                        echo "Houve um problema ao enviar os arquivos da autorização!";
                        mysql_query("ROLLBACK");
                        return false;
                    } else {
                        //$autorizacaoQueue->uploaded();
                    }
                }
            } else {
                echo $files['messages'];
            }
        }


        if($hasItens) {
            mysql_query("COMMIT");
            if($data['carater'] != 'avaliacao'){
                $regras = new RegraFluxoAutorizacoes();
                $regras->verificarItensRestritos($itensAutorizados,$data['carater']);

            }

            return [2, $autorizacao_id];
        }

        if($data['status_geral'] != '1' && $hasAutorizado) {
            $sql = <<<SQL
UPDATE autorizacao SET status_geral = 1 WHERE id = '{$autorizacao_id}'
SQL;
            $rs = mysql_query($sql);
        }


        mysql_query("COMMIT");

        if($data['carater'] != 'avaliacao'){
            $regras = new RegraFluxoAutorizacoes();
            $regras->verificarItensRestritos($itensAutorizados,$data['carater']);

        }

        return [1, $autorizacao_id];
    }

    public static function update($data, $files)
    {
        $autorizacao_id = $data['autorizacao_id'];

        $usuario = $_SESSION['id_user'];

        mysql_query("BEGIN");

        $sql = <<<SQL
UPDATE autorizacao 
SET 
  status_geral = '{$data['status_geral']}'
WHERE
  id = '{$autorizacao_id}'
SQL;
        $rs = mysql_query($sql);

        if(!$rs) {
            echo "Houve um problema ao atualizar a autorização!";
            mysql_query("ROLLBACK");
            return false;
        }

        $hasItens = false;

        $carater = $data['carater'] == 'avaliacao' ? 'prorrogacao' : $data['carater'];


        foreach ($data['quantidade_autorizada'] as $item => $qtd) {
            $itemExp = explode('-', $item);
            $fatId = $itemExp[0];
            $tipo = $itemExp[1];
            $tabela = $itemExp[2];
            $catalogo_id = $itemExp[3];

            if($data['status_item'][$item] == '2' || $data['status_item'][$item] == '') {
                $hasItens = true;
            }

            if($data['data_autorizacao'][$item] > 0 && ($data['status_item'][$item] == 1 || $data['status_item'][$item] == 2)){

                $itensAutorizados[] = [
                    'autorizacao_item_id' => $data['autorizacao_item_id'][$item],
                    'inicio' => $data['inicio'],
                    'fim' => $data['fim'],
                    'plano_id' => $data['plano_id'],
                    'paciente_id' => $data['paciente'],
                    'quantidade_autorizada' => $data['quantidade_autorizada'][$item],
                    'item_id' => $catalogo_id,
                    'tipo' => $tipo,
                    'tipo_solicitacao' => $carater,
                    'grupo' => $data['categoria'][$item]
                ];

            }
if(!empty($data['status_item'][$item])){
            $sql = <<<SQL
UPDATE autorizacao_itens
SET
  qtd_autorizada = qtd_autorizada + '{$qtd}',
  status_item = '{$data['status_item'][$item]}',
  data_autorizacao = '{$data['data_autorizacao'][$item]}', 
  autorizado_por = '{$usuario}', 
  autorizado_em = NOW() 
WHERE
  origem_item_id = '{$fatId}'
SQL;
            $rs = mysql_query($sql);

            if(!$rs) {
                echo "Houve um problema ao salvar um item da autorização!";
                mysql_query("ROLLBACK");
                return false;
            }

                $sql = <<<SQL
INSERT INTO historico_autorizacao_itens
(
  id,
  autorizacao_itens_id,  
  qtd_autorizada,
  status_item,
  data_autorizacao,
  autorizado_por,
  autorizado_em,
  origem_item_id
) VALUES (
  NULL,
  '{$data['autorizacao_item_id'][$item]}',  
  '{$qtd}',
  '{$data['status_item'][$item]}',
  '{$data['data_autorizacao'][$item]}',
  '{$usuario}',
  NOW(),
  '{$fatId}'
)
SQL;
                $rs = mysql_query($sql);

                if (!$rs) {
                    echo "Houve um erro ao salvar o histórico da autorização!".$sql;
                    mysql_query("ROLLBACK");
                    return false;
                }
            }
        }

        $files = $files['arquivos_autorizacao'];

        if($files['name'][0] != '') {
            $files = self::uploadFiles($files, $usuario);

            if (!isset($files['messages'])) {
                //$autorizacaoQueue = new AutorizacaoQueue(ConnMysqli::getConnection());
                $data['data_autorizacao_arquivo'] = $data['data_autorizacao_arquivo'] != '' ? $data['data_autorizacao_arquivo'] : (new \DateTime())->format('Y-m-d');
                foreach ($files as $file) {
                    $sql = <<<SQL
INSERT INTO autorizacao_arquivos
(
  id,
  autorizacao,
  files_uploaded_id,
  data_autorizacao
) VALUES (
  NULL,
  '{$autorizacao_id}',
  '{$file['id']}',
  '{$data['data_autorizacao_arquivo']}'
)
SQL;
                    $rs = mysql_query($sql);

                    if(!$rs) {
                        echo "Houve um problema ao enviar os arquivos da autorização!";
                        mysql_query(    "ROLLBACK");
                        return false;
                    } else {
                        //$autorizacaoQueue->uploaded();
                    }
                }
            } else {
                echo $files['messages'];
            }
        }

        if($hasItens) {
            mysql_query("COMMIT");
            if($data['carater'] != 'avaliacao'){
                $regras = new RegraFluxoAutorizacoes();
                $regras->verificarItensRestritos($itensAutorizados,$data['carater']);

            }
            return [2, $autorizacao_id];
        }

        $sql = <<<SQL
SELECT 
  COUNT(id) AS hasAutorizado
FROM
  autorizacao_itens
WHERE
  status_item IN (1, 2)
  AND autorizacao = '{$autorizacao_id}'
GROUP BY 
  autorizacao
SQL;

        $response = current(parent::get($sql, 'assoc'));

        if($data['status_geral'] != '1' && $response['hasAutorizado'] > 0) {
            $sql = <<<SQL
UPDATE autorizacao SET status_geral = 1 WHERE id = '{$autorizacao_id}'
SQL;
            $rs = mysql_query($sql);
        }

        mysql_query("COMMIT");
        if($data['carater'] != 'avaliacao'){
            $regras = new RegraFluxoAutorizacoes();
            $regras->verificarItensRestritos($itensAutorizados,$data['carater']);

        }

        return [1, $autorizacao_id];
    }

    public static function removerArquivoAutorizacao($arquivo_id)
    {
        $sql = <<<SQL
SELECT 
  path,
  full_path
FROM
  files_uploaded
  INNER JOIN autorizacao_arquivos ON files_uploaded.id = autorizacao_arquivos.files_uploaded_id
WHERE
  autorizacao_arquivos.id = '{$arquivo_id}'
SQL;
        $row = current(parent::get($sql, 'assoc'));

        if(is_file($row['path'])) {
            unlink($row['path']);

            $sql = <<<SQL
DELETE FROM autorizacao_arquivos WHERE id = '{$arquivo_id}'
SQL;
            $rs = mysql_query($sql);

            if(!$rs) {
                echo "Houve um problema ao excluir um arquivo da autorizacao!";
                return false;
            }

        }

        if(is_file($row['full_path'])) {
            unlink($row['full_path']);

            $sql = <<<SQL
DELETE FROM autorizacao_arquivos WHERE id = '{$arquivo_id}'
SQL;
            $rs = mysql_query($sql);

            if(!$rs) {
                echo "Houve um problema ao excluir um arquivo da autorizacao!";
                return false;
            }

        }

        return true;
    }

    private static function uploadFiles($files, $userId)
    {
        $baseDirectory = self::BASE_DIRECTORY;
        $extensions = ['jpg', 'jpeg', 'png', 'doc', 'docx', 'pdf', 'odt', 'txt', 'rtf'];
        $fileUpload = new FileUpload($baseDirectory, '5M', $extensions);
        $fileUpload->turnOnPrefix();
        list($handlerResults, $messages) = $fileUpload->process($files);
        $data = array();

        if (empty($messages)) {
            $conn = ConnMysqli::getConnection();
            $sqls = $fileUpload->getSql($handlerResults, $userId);
            foreach ($sqls as $sql) {
                if ($conn->query($sql)) {
                    $result = $conn->query($fileUpload->getSqlById($conn->insert_id));
                    while ($file = $result->fetch_object()) {
                        $data[] = ['id' => $file->id, 'path' => $file->path];
                    }
                }
            }
        } else {
            foreach ($messages as $errors)
                foreach ($errors as $error)
                    $data['messages'][] = sprintf('* %s', (string) $error);
            $file['error'][] = sprintf('* %s', (string) $error);
        }
        return $data;
    }

    public static function getAutorizacoesUr($filters)
    {
        $avaliacoes = [];
        $prorrogacoes = [];

        if($filters['tipo_documento'] == 'T') {
            $avaliacoes = Avaliacao::getAllAvaliacoesAutorizacaoUr($filters);
            if($filters['plano'] == 'T') {
                $prorrogacoes = array_merge(
                    Prorrogacao::getAllProrrogacoesAutorizacaoUr($filters),
                    ProrrogacaoPlanserv::getAllProrrogacoesAutorizacaoUr($filters)
                );
            } else {
                $operadora = Planos::getOperadoraPlano($filters['plano']);
                if ($operadora[0]['OPERADORAS_ID'] != 11) {
                    $prorrogacoes = Prorrogacao::getAllProrrogacoesAutorizacaoUr($filters);
                } else {
                    $prorrogacoes = ProrrogacaoPlanserv::getAllProrrogacoesAutorizacaoUr($filters);
                }
            }
        }

        if($filters['tipo_documento'] == 'A') {
            $avaliacoes = Avaliacao::getAllAvaliacoesAutorizacaoUr($filters);
        }

        if($filters['tipo_documento'] == 'P') {
            if($filters['plano'] == 'T') {
                $prorrogacoes = array_merge(
                    Prorrogacao::getAllProrrogacoesAutorizacaoUr($filters),
                    ProrrogacaoPlanserv::getAllProrrogacoesAutorizacaoUr($filters)
                );
            } else {
                $operadora = Planos::getOperadoraPlano($filters['plano']);
                if ($operadora[0]['OPERADORAS_ID'] != 11) {
                    $prorrogacoes = Prorrogacao::getAllProrrogacoesAutorizacaoUr($filters);
                } else {
                    $prorrogacoes = ProrrogacaoPlanserv::getAllProrrogacoesAutorizacaoUr($filters);
                }
            }
        }

        $response = array_merge($avaliacoes, $prorrogacoes);

        //echo '<pre>'; echo count($response); print_r($avaliacoes); die(print_r($prorrogacoes));

        return $response;
    }

    public static function saveExtraFiles($data, $files)
    {
        $usuario = $_SESSION['id_user'];

        $files = $files['arquivos_extras_autorizacao'];

        if($files['name'][0] != '') {
            $files = self::uploadFiles($files, $usuario);

            if (!isset($files['messages'])) {
                //$autorizacaoQueue = new AutorizacaoQueue(ConnMysqli::getConnection());
                $data['data_autorizacao_arquivo'] = $data['data_autorizacao_arquivo'] != '' ? $data['data_autorizacao_arquivo'] : (new \DateTime())->format('Y-m-d');
                foreach ($files as $file) {
                    $sql = <<<SQL
INSERT INTO autorizacao_arquivos
(
  id,
  autorizacao,
  files_uploaded_id,
  data_autorizacao
) VALUES (
  NULL,
  '{$data['autorizacao_id']}',
  '{$file['id']}',
  '{$data['data_autorizacao_arquivo']}'
)
SQL;
                    $rs = mysql_query($sql);

                    if(!$rs) {
                        echo "Houve um problema ao enviar os arquivos da autorização!";
                        mysql_query("ROLLBACK");
                        return false;
                    } else {
                        //$autorizacaoQueue->uploaded();
                    }
                }
            } else {
                return $files['messages'];
            }
        }
    }
}
