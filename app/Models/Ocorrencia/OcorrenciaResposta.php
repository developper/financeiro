<?php

namespace App\Models\Ocorrencia;

use \App\Models\AbstractModel;
use App\Models\DB\ConnMysqli;
use App\Models\FileSystem\FileUpload;

class OcorrenciaResposta extends AbstractModel
{
    const BASE_DIRECTORY = 'storage/ocorrencia-respostas/';

    public static function save($data)
    {
        mysql_query("BEGIN");

        $sql = <<<SQL
INSERT INTO ocorrencia_resposta (
  id,
  ocorrencia_id, 
  sugerido_resolvido, 
  resposta, 
  created_at,
  created_by
) VALUES (
  NULL,
  '{$data['ocorrencia']}',
  '{$data['sugerir_resolvido']}',
  '{$data['resposta']}',
  NOW(),
  {$_SESSION['id_user']}
)
SQL;
        $rs = mysql_query($sql);

        if(!$rs) {
            mysql_query("ROLLBACK");
            return mysql_error() . ' - ' . $sql;
        }

        $resposta_id = mysql_insert_id();

        $usuario = $_SESSION['id_user'];
        $files = [];
        if($data['arquivos'][0]['name'] != '') {
            foreach ($data['arquivos'] as $arquivo) {
                $files['name'][] = $arquivo['name'];
                $files['type'][] = $arquivo['type'];
                $files['tmp_name'][] = $arquivo['tmp_name'];
                $files['error'][] = $arquivo['error'];
                $files['size'][] = $arquivo['size'];
            }
        }

        if($files['name'][0] != '') {
            $files = self::uploadFiles($files, $usuario);

            if (!isset($files['messages'])) {
                //$autorizacaoQueue = new AutorizacaoQueue(ConnMysqli::getConnection());
                foreach ($files as $file) {
                    $sql = <<<SQL
INSERT INTO ocorrencia_resposta_arquivos
(
  id,
  ocorrencia_id,
  resposta_id,
  arquivo_id
) VALUES (
  NULL,
  '{$data['ocorrencia']}',
  '{$resposta_id}',
  '{$file['id']}'
)
SQL;
                    $rs = mysql_query($sql);

                    if(!$rs) {
                        echo "Houve um problema ao enviar os arquivos da autorização!";
                        mysql_query("ROLLBACK");
                        return false;
                    } else {
                        //$autorizacaoQueue->uploaded();
                    }
                }
                mysql_query("COMMIT");
                //$autorizacaoQueue->uploaded();
            }
        }
        return $resposta_id;
	}

    public static function getByFilter($data)
    {
        $sql = <<<SQL
SELECT
  ocorrencia.*,
  clientes.nome AS paciente_nome,
  usuarios.nome AS criadoPor,
  ocorrencia_classificacao.classificacao AS descClassificacao
FROM
  ocorrencia
  INNER JOIN ocorrencia_classificacao ON ocorrencia.classificacao = ocorrencia_classificacao.id
  INNER JOIN usuarios ON ocorrencia.created_by = usuarios.idUsuarios
  LEFT JOIN clientes ON ocorrencia.paciente = clientes.idClientes
WHERE
  created_at BETWEEN '{$data['inicio']} 00:00:01' AND '{$data['fim']} 23:59:59'
GROUP BY
  ocorrencia.id
ORDER BY
  created_at DESC
SQL;
        $response = parent::get($sql, 'assoc');

        if(count($response) > 0) {
            foreach ($response as $key => $row) {
                $response[$key]['urs'] = self::getOcorrenciaUrs($row['id']);
                $response[$key]['setores'] = self::getOcorrenciaSetores($row['id']);
            }
        }
        return $response;
	}

    private static function getOcorrenciaUrs($ocorrencia_id)
    {
        $sql = <<<SQL
SELECT
  ocorrencia_ur.*,
  empresas.nome AS ur
FROM 
  ocorrencia_ur
  INNER JOIN empresas ON ocorrencia_ur.empresa_id = empresas.id
WHERE
  ocorrencia_ur.ocorrencia_id = '{$ocorrencia_id}'
SQL;
       return parent::get($sql, 'assoc');
	}

    private static function getOcorrenciaSetores($ocorrencia_id)
    {
        $sql = <<<SQL
SELECT
  ocorrencia_setor.*
FROM 
  ocorrencia_setor
WHERE
  ocorrencia_setor.ocorrencia_id = '{$ocorrencia_id}'
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getArquivosOcorrenciaResposta($resposta)
    {
        $sql = <<<SQL
SELECT 
  ocorrencia_resposta_arquivos.id AS autorizacao_arquivo_id,
  files_uploaded.id AS files_uploaded_id,
  files_uploaded.path,
  files_uploaded.full_path
FROM
  ocorrencia_resposta_arquivos
  INNER JOIN ocorrencia_resposta ON ocorrencia_resposta_arquivos.resposta_id = ocorrencia_resposta.id
  INNER JOIN files_uploaded ON ocorrencia_resposta_arquivos.arquivo_id = files_uploaded.id
WHERE
  ocorrencia_resposta_arquivos.resposta_id = '{$resposta}'
SQL;
        return parent::get ($sql, 'assoc');

    }

    public static function getArquivosOcorrenciaRespostaByOcorrenciaId($ocorrencia)
    {
        $sql = <<<SQL
SELECT 
  ocorrencia_resposta_arquivos.id AS autorizacao_arquivo_id,
  ocorrencia_resposta_arquivos.resposta_id,
  files_uploaded.id AS files_uploaded_id,
  files_uploaded.path,
  files_uploaded.full_path
FROM
  ocorrencia_resposta_arquivos
  INNER JOIN ocorrencia_resposta ON ocorrencia_resposta_arquivos.resposta_id = ocorrencia_resposta.id
  INNER JOIN files_uploaded ON ocorrencia_resposta_arquivos.arquivo_id = files_uploaded.id
WHERE
  ocorrencia_resposta_arquivos.ocorrencia_id = '{$ocorrencia}'
SQL;
        return parent::get ($sql, 'assoc');

    }

    private static function uploadFiles($files, $userId)
    {
        $baseDirectory = self::BASE_DIRECTORY;
        $extensions = ['jpg', 'jpeg', 'png', 'doc', 'docx', 'pdf', 'odt', 'txt', 'rtf'];
        $fileUpload = new FileUpload($baseDirectory, '5M', $extensions);
        $fileUpload->turnOnPrefix();
        list($handlerResults, $messages) = $fileUpload->process($files);
        $data = array();

        if (empty($messages)) {
            $conn = ConnMysqli::getConnection();
            $sqls = $fileUpload->getSql($handlerResults, $userId);
            foreach ($sqls as $sql) {
                if ($conn->query($sql)) {
                    $result = $conn->query($fileUpload->getSqlById($conn->insert_id));
                    while ($file = $result->fetch_object()) {
                        $data[] = ['id' => $file->id, 'path' => $file->path];
                    }
                }
            }
        } else {
            foreach ($messages as $errors)
                foreach ($errors as $error)
                    $data['messages'][] = sprintf('* %s', (string) $error);
            $file['error'][] = sprintf('* %s', (string) $error);
        }
        return $data;
    }
}