<?php

namespace App\Models\Ocorrencia;

use App\Core\Config;
use \App\Models\AbstractModel;
use App\Models\Administracao\Usuario;
use App\Models\DB\ConnMysqli;
use App\Models\FileSystem\FileUpload;
use App\Services\Gateways\SendGridGateway;
use App\Services\MailAdapter;

class Ocorrencia extends AbstractModel
{
    const BASE_DIRECTORY = 'storage/ocorrencias/';

    public static function save($data)
    {
        mysql_query("BEGIN");

        $sql = <<<SQL
INSERT INTO ocorrencia (
  id,
  demandatario, 
  contato, 
  telefone_contato, 
  prioridade, 
  ocorrencia_paciente, 
  paciente, 
  data_ocorrencia, 
  hora_ocorrencia, 
  tipo_contato, 
  descricao, 
  classificacao, 
  subclassificacao, 
  encaminhamento, 
  encaminhamento_nome, 
  status, 
  created_by, 
  created_at, 
  updated_by,
  updated_at,
  setor_origem,
  ur_origem
) VALUES (
  NULL,
  '{$data['demandatario']}',
  '{$data['contato']}',
  '{$data['telefone_contato']}',
  '{$data['prioridade']}',
  '{$data['ocorrencia_paciente']}',
  '{$data['paciente']}',
  '{$data['data_ocorrencia']}',
  '{$data['hora_ocorrencia']}',
  '{$data['tipo_contato']}',
  '{$data['descricao']}',
  '{$data['classificacao']}',
  '{$data['subclassificacao']}',
  '{$data['encaminhamento']}',
  '{$data['nome_encaminhamento']}',
  'Em Aberto',
  '{$_SESSION['id_user']}',
  NOW(),
  '',
  '',
  '{$data['setor_origem']}',
'{$data['ur_origem']}'
)
SQL;
        $rs = mysql_query($sql);

        if(!$rs) {
            mysql_query("ROLLBACK");
            return [mysql_error() . ' - ' . $sql, 0, []];
        }

        $ocorrencia_id = mysql_insert_id();

        $usuario = $_SESSION['id_user'];
        $files = $data['arquivos'];

        if($files['name'][0] != '') {
            $files = self::uploadFiles($files, $usuario);

            if (!isset($files['messages'])) {
                //$autorizacaoQueue = new AutorizacaoQueue(ConnMysqli::getConnection());
                foreach ($files as $file) {
                    $sql = <<<SQL
INSERT INTO ocorrencia_arquivos
(
  id,
  ocorrencia_id,
  arquivo_id
) VALUES (
  NULL,
  '{$ocorrencia_id}',
  '{$file['id']}'
)
SQL;
                    $rs = mysql_query($sql);

                    if(!$rs) {
                        echo "Houve um problema ao enviar os arquivos da autorização!";
                        mysql_query("ROLLBACK");
                        return [false, 0, []];
                    } else {
                        //$autorizacaoQueue->uploaded();
                    }
                }
            } else {
                return $files['messages'];
            }
        }

        foreach ($data['ur'] as $ur) {
            $sql = <<<SQL
INSERT INTO ocorrencia_ur
(
  id, ocorrencia_id, empresa_id
) VALUES (
  NULL, '{$ocorrencia_id}', '{$ur}'
)
SQL;
            $rs = mysql_query($sql);

            if(!$rs) {
                mysql_query("ROLLBACK");
                return [mysql_error() . ' - ' . $sql, 0, []];
            }
        }

        foreach ($data['setor'] as $setor) {
            $sql = <<<SQL
INSERT INTO ocorrencia_setor
(
  id, ocorrencia_id, setor_id
) VALUES (
  NULL, '{$ocorrencia_id}', '{$setor}'
)
SQL;
            $rs = mysql_query($sql);

            if(!$rs) {
                mysql_query("ROLLBACK");
                return [mysql_error() . ' - ' . $sql, 0, []];
            }
        }

        $to = [];
        if(!empty($data['email_encaminhamento'])) {
            $wrongEmails = [];

            foreach ($data['email_encaminhamento'] as $email) {
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $sql = <<<SQL
INSERT INTO ocorrencia_emails_encaminhamento
(
  id, ocorrencia_id, email
) VALUES (
  NULL, '{$ocorrencia_id}', '{$email}'
)
SQL;
                    $rs = mysql_query($sql);
                    $to[] = ['email' => $email, 'nome' => $email];

                    if (!$rs) {
                        mysql_query("ROLLBACK");
                        return [mysql_error() . ' - ' . $sql, 0, []];
                    }
                } else {
                    $wrongEmails[] = $email;
                }
            }
        }

        if(!empty($to)) {
            $descricao = htmlentities($data['descricao']);
            $msg = "<p>Voc&ecirc; foi marcado em uma nova ocorr&ecirc;ncia por <b>{$_SESSION['nome_user']}</b>.</p>";
            $msg .= "<p>
                    <b>Descri&ccedil;&atilde;o: </b><br>
                    {$descricao}
                </p>";
            $subject = "Nova Ocorr&ecirc;ncia #{$ocorrencia_id} - Sistema de Gerenciamento da Mederi";
            self::notify($subject, $msg, $to);
        }

        return [mysql_query("COMMIT"), $ocorrencia_id, $wrongEmails];
	}

    public static function getById($id)
    {
        $sql = <<<SQL
SELECT 
ocorrencia.*
FROM 
ocorrencia
WHERE 
id = {$id}
SQL;
        return parent::get($sql, 'assoc');
	}

    public static function getByFilter($data)
    {
        $response = [];
        $compSetor = "";
        $innerSetor = "";

        $compPaciente = isset($data['paciente']) && $data['paciente'] != '' ?
            " AND ocorrencia.paciente = '{$data['paciente']}' " :
            "";

        $compStatus = isset($data['status']) && $data['status'] != '' ?
            " AND ocorrencia.status = '{$data['status']}' " :
            "";

        $compSolicitadoPor = "";
        if(isset($data['solicitado_por']) && $data['solicitado_por'] == '2') {
            $compSolicitadoPor = " AND ocorrencia.created_by = '{$_SESSION['id_user']}' ";
        } elseif(isset($data['solicitado_por']) && $data['solicitado_por'] == '3') {
            $setores = implode(', ', $_SESSION['user_setores']);
            $compSolicitadoPor = " AND ocorrencia.setor_origem IN ({$setores}) ";
        }

        $compUr = "";
        $innerUr = "";
        if(isset($data['ur']) && count($data['ur']) > 0) {
            $urs = implode(', ', $data['ur']);
            $compUr = " AND ocorrencia_ur.empresa_id IN ({$urs}) ";
            $innerUr = " INNER JOIN ocorrencia_ur ON ocorrencia.id = ocorrencia_ur.ocorrencia_id ";
        }

        if(isset($data['setor']) && count($data['setor']) > 0) {
            $setores = implode(', ', $data['setor']);
            $compSetor = " AND ocorrencia_setor.setor_id IN ({$setores}) ";
            $innerSetor = " LEFT JOIN ocorrencia_setor ON ocorrencia.id = ocorrencia_setor.ocorrencia_id ";
        }

        $compSubclassificacao = isset($data['classificacao']) && $data['classificacao'] != '' ?
            " AND ocorrencia.classificacao = '{$data['classificacao']}' " :
            "";

        $compClassificacao = isset($data['subclassificacao']) && $data['subclassificacao'] != '' ?
            " AND ocorrencia.subclassificacao = '{$data['subclassificacao']}' " :
            "";

        $compTipoContato = isset($data['tipo_contato']) && $data['tipo_contato'] != '' ?
            " AND ocorrencia.tipo_contato = '{$data['tipo_contato']}' " :
            "";

        $compPeriodo = '';
        if(!empty($data['inicio']) && !empty($data['fim'])   ){
            $compPeriodo =  "AND created_at BETWEEN '{$data['inicio']} 00:00:00' AND '{$data['fim']} 23:59:59'";

        }elseif(!empty($data['inicio']) && empty($data['fim'])){
            $compPeriodo = "AND created_at >= '{$data['inicio']} 00:00:00'";
        }elseif(empty($data['inicio']) && !empty($data['fim'])){
            $compPeriodo = "AND created_at <= '{$data['inicio']} 23:59:59'";
        }


        $sql = <<<SQL
SELECT
  ocorrencia.*,
  clientes.nome AS paciente_nome,
  usuarios.nome AS criadoPor,
  u2.nome AS finalizadoPor,
  u3.nome AS desativadaPor,
  ocorrencia_classificacao.classificacao AS descClassificacao,
  ocorrencia_subclassificacao.descricao AS desSubclassificacao,
  setores.setor AS nome_setor
FROM
  ocorrencia
  INNER JOIN ocorrencia_classificacao ON ocorrencia.classificacao = ocorrencia_classificacao.id
  INNER JOIN usuarios ON ocorrencia.created_by = usuarios.idUsuarios
  LEFT JOIN usuarios as u2 ON ocorrencia.finalizado_por = u2.idUsuarios
  LEFT JOIN usuarios as u3 ON ocorrencia.deleted_by = u3.idUsuarios
  LEFT JOIN clientes ON ocorrencia.paciente = clientes.idClientes
  LEFT JOIN ocorrencia_subclassificacao ON ocorrencia.subclassificacao = ocorrencia_subclassificacao.id
  LEFT JOIN setores ON ocorrencia.setor_origem = setores.id
  {$innerUr}
  {$innerSetor}
WHERE
1=1
  {$compSetor}
  {$compPaciente}
  {$compStatus}
  {$compUr}
  {$compPeriodo}
  {$compClassificacao}
  {$compSubclassificacao}
  {$compTipoContato}
  {$compPrioridade}
  {$compSolicitadoPor}
GROUP BY
  ocorrencia.id
ORDER BY
  created_at DESC
SQL;
        //die($sql);
        $response = parent::get($sql, 'assoc');

        if(count($response) > 0) {
            foreach ($response as $key => $row) {
                $response[$key]['urs'] = self::getOcorrenciaUrs($row['id']);
                $response[$key]['setores'] = self::getOcorrenciaSetores($row['id']);
                $response[$key]['respostas'] = self::getOcorrenciaRespostas($row['id']);
                $response[$key]['arquivos'] = self::getOcorrenciaArquivos($row['id']);
                $response[$key]['arquivos_respostas'] = OcorrenciaResposta::getArquivosOcorrenciaRespostaByOcorrenciaId($row['id']);
            }
        }
        return $response;
	}

    public static function resolve($data)
    {
        /*
         * 'Em Aberto'
         * 'Resolvido'
         */

        $sql = <<<SQL
UPDATE ocorrencia SET 
  status = 'Resolvido', 
  retorno_hc = '{$data['retorno_hc']}',
  descricao_qualidade_retorno = '{$data['descricao_qualidade_retorno']}' ,
  finalizado_por = '{$_SESSION['id_user']}',
  finalizado_em = NOW() 
WHERE 
  id = '{$data['ocorrencia_id']}'
SQL;
        return mysql_query($sql);

	}

    public static function deactivate($data)
    {
        $sql = <<<SQL
UPDATE ocorrencia SET 
  status = 'Desativada', 
  deleted_by = '{$_SESSION['id_user']}',
  deleted_at = NOW() 
WHERE 
  id = '{$data['ocorrencia_id']}'
SQL;
        return mysql_query($sql);
    }

    private static function getOcorrenciaUrs($ocorrencia_id)
    {
        $sql = <<<SQL
SELECT
  ocorrencia_ur.*,
  empresas.nome AS ur
FROM 
  ocorrencia_ur
  INNER JOIN empresas ON ocorrencia_ur.empresa_id = empresas.id
WHERE
  ocorrencia_ur.ocorrencia_id = '{$ocorrencia_id}'
SQL;
       return parent::get($sql, 'assoc');
	}

    private static function getOcorrenciaSetores($ocorrencia_id)
    {
        $sql = <<<SQL
SELECT
  ocorrencia_setor.*,
  setores.setor
FROM 
  ocorrencia_setor
  INNER JOIN setores on ocorrencia_setor.setor_id = setores.id

WHERE
  ocorrencia_setor.ocorrencia_id = '{$ocorrencia_id}'
SQL;
        return parent::get($sql, 'assoc');
    }

    private static function getOcorrenciaRespostas($ocorrencia_id)
    {
        $sql = <<<SQL
SELECT
  ocorrencia_resposta.*,
  usuarios.nome as nome_usuario
FROM 
  ocorrencia_resposta
  INNER JOIN usuarios ON ocorrencia_resposta.created_by = usuarios.idUsuarios
WHERE
  ocorrencia_resposta.ocorrencia_id = '{$ocorrencia_id}'
SQL;
        return parent::get($sql, 'assoc');
    }

    private static function getOcorrenciaArquivos($ocorrencia_id)
    {
        $sql = <<<SQL
SELECT
  ocorrencia_arquivos.*,
  files_uploaded.id AS files_uploaded_id,
  files_uploaded.path,
  files_uploaded.full_path
FROM 
  ocorrencia_arquivos
  INNER JOIN files_uploaded ON ocorrencia_arquivos.arquivo_id = files_uploaded.id
WHERE
  ocorrencia_arquivos.ocorrencia_id = '{$ocorrencia_id}'
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function reabrir($data)
    {
        $response = self::getById($data['ocorrencia_id']);

        mysql_query('begin');

        $sql = "
        insert 
        into 
        historico_ocorrencia_reaberta 
        (
        ocorrencia_id,
        reaberto_por,
        reaberto_em,
        finalizado_por,
        finalizado_em,
        retorno_hc,
        descricao_qualidade_retorno
        )
        VALUES (
        
        '{$data['ocorrencia_id']}',
        '{$_SESSION['id_user']}',
        NOW() ,
        '{$response[0]['finalizado_por']}' ,
        '{$response[0]['finalizado_em']}',
        '{$response[0]['retorno_hc']}',
        '{$response[0]['descricao_qualidade_retorno']}'


        
        )

";

        $rs = mysql_query($sql);

        if(!$rs) {
            mysql_query("ROLLBACK");
            return "Erro ao tentar reabrir Ocorrência. COD:01";
        }

        $sql = <<<SQL
UPDATE
ocorrencia
SET
  status = 'Em Aberto',
reaberto_por = '{$_SESSION['id_user']}',
reaberto_em = NOW() ,
finalizado_por = 0 ,
finalizado_em = '0000-00-00 00:00:00',
descricao_qualidade_retorno = '',
retorno_hc = 0
WHERE
  id = '{$data['ocorrencia_id']}'
SQL;

        $rs = mysql_query($sql);

        if(!$rs) {
            mysql_query("ROLLBACK");
            return "Erro ao tentar reabrir Ocorrência. COD:02";
        }

        mysql_query('commit');
        echo 1;

    }

    private static function uploadFiles($files, $userId)
    {
        $baseDirectory = self::BASE_DIRECTORY;
        $extensions = ['jpg', 'jpeg', 'png', 'doc', 'docx', 'pdf', 'odt', 'txt', 'rtf'];
        $fileUpload = new FileUpload($baseDirectory, '5M', $extensions);
        $fileUpload->turnOnPrefix();
        list($handlerResults, $messages) = $fileUpload->process($files);
        $data = array();

        if (empty($messages)) {
            $conn = ConnMysqli::getConnection();
            $sqls = $fileUpload->getSql($handlerResults, $userId);
            foreach ($sqls as $sql) {
                if ($conn->query($sql)) {
                    $result = $conn->query($fileUpload->getSqlById($conn->insert_id));
                    while ($file = $result->fetch_object()) {
                        $data[] = ['id' => $file->id, 'path' => $file->path];
                    }
                }
            }
        } else {
            foreach ($messages as $errors)
                foreach ($errors as $error)
                    $data['messages'][] = sprintf('* %s', (string) $error);
            $file['error'][] = sprintf('* %s', (string) $error);
        }
        return $data;
    }

    private static function notify($subject, $msg, $to)
    {
        $api_data = (object) Config::get('sendgrid');

        $gateway = new MailAdapter(
            new SendGridGateway(
                new \SendGrid($api_data->user, $api_data->pass),
                new \SendGrid\Email()
            )
        );

        $gateway->adapter->sendSimpleMail($subject, $msg, $to);
    }

}