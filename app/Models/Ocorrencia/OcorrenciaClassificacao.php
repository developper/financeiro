<?php

namespace App\Models\Ocorrencia;

use \App\Models\AbstractModel;

class OcorrenciaClassificacao extends AbstractModel
{
    public static function getAll()
    {

        $sql = <<<SQL
SELECT * FROM ocorrencia_classificacao ORDER BY classificacao ASC
SQL;
        return parent::get($sql);
    }
}