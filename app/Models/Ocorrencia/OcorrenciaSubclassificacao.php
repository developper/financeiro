<?php

namespace App\Models\Ocorrencia;

use \App\Models\AbstractModel;

class OcorrenciaSubclassificacao extends AbstractModel
{
    public static function getAll()
    {

        $sql = <<<SQL
SELECT * FROM ocorrencia_subclassificacao
SQL;
        return parent::get($sql, 'assoc');
    }

    public static function getByClassificacaoId($id)
    {

        $sql = <<<SQL
SELECT * FROM ocorrencia_subclassificacao WHERE ocorrencia_classificacao_id = '{$id}'
SQL;
        return parent::get($sql, 'assoc');
    }
}