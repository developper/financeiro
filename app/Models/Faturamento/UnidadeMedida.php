<?php

namespace App\Models\Faturamento;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class UnidadeMedida extends AbstractModel implements DAOInterface
{

  public static function getAll()
  {
    return parent::get(self::getSQL());
  }

  public static function getById($id)
  {
    return current(parent::get(self::getSQL($id)));
  }

  public static function getSQL($id = null)
  {
    $id = isset($id) && !empty($id) ? 'WHERE 1=1 AND id = "' . $id . '"' : "WHERE tiss_codigo != 0";
    return "SELECT * FROM unidades_fatura  {$id} ORDER BY unidade";
  }



} 