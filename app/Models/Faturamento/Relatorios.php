<?php

namespace App\Models\Faturamento;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;
use phpDocumentor\Reflection\Types\Parent_;

class Relatorios extends AbstractModel
{
    public static function relatorioPacientesFaturar($data)
    {
        $compOperadoras = "";
        $innerOperadora = "";
        if(isset($data['operadora']) && count($data['operadora']) > 0) {
            $operadora = implode(', ', $data['operadora']);
            $compOperadoras = " AND ops.OPERADORAS_ID IN ({$operadora})";
            $innerOperadora = " INNER JOIN operadoras_planosdesaude ops ON pl.id = ops.PLANOSDESAUDE_ID ";
        }

        $sql = <<<SQL
SELECT principal.nome,
       principal.paciente,
       principal.nomeplano,
       principal.nome_empresa
FROM
     (  SELECT c.nome,
               c.idClientes AS paciente,
               pl.nome AS nomeplano,
               e.nome AS nome_empresa
        FROM `saida` s
               INNER JOIN `clientes` c ON ( s.idCliente = c.idClientes)
               INNER JOIN empresas AS e ON ( c.empresa = e.id )
               INNER JOIN `catalogo` b ON ( s.catalogo_id = b.id)
               INNER JOIN planosdesaude AS pl ON ( c.convenio = pl.id)
               INNER JOIN solicitacoes AS sol ON ( s.idsolicitacao = sol.idsolicitacoes)
               INNER JOIN relatorio_aditivo_enf AS p ON ( sol.relatorio_aditivo_enf_id = p.id)
               {$innerOperadora}
        WHERE ( p.inicio BETWEEN '{$data['inicio']}' AND '{$data['fim']}'
                  OR p.fim BETWEEN '{$data['inicio']}' AND '{$data['fim']}')
          AND s.quantidade > 0
          {$compOperadoras}
          AND ( c.idClientes = s.idCliente)
          AND c.idClientes NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
          AND c.nome NOT LIKE '%ambulancia%'
          AND c.nome NOT LIKE '%UR %'
          AND c.nome NOT LIKE '%CAIXA %'
          AND c.empresa NOT IN (9, 10)
          AND s.tipo IN (0,
                         1,
                         3)
        GROUP BY paciente) AS principal
GROUP BY paciente
UNION
SELECT principal.nome,
       principal.paciente,
       principal.nomeplano,
       principal.nome_empresa
FROM
     ( SELECT c.nome,
              c.idClientes AS paciente,
              pl.nome AS nomeplano,
              e.nome AS nome_empresa
       FROM `solicitacao_rel_aditivo_enf` s
              LEFT JOIN `frequencia` AS f ON ( s.frequencia = f.id)
              INNER JOIN relatorio_aditivo_enf p ON ( s.`relatorio_enf_aditivo_id` = p.id)
              INNER JOIN `clientes` c ON ( p.paciente_id = c.idClientes)
              INNER JOIN empresas AS e ON ( c.empresa = e.id )
              INNER JOIN cuidadosespeciais AS esp ON ( s.catalogo_id = esp.id)
              INNER JOIN `cobrancaplanos` b ON ( esp.cobrancaplano_id = b.id)
              LEFT JOIN custo_servicos_plano_ur cs ON ( cs.cobrancaplano_id = b.id
                                                          AND cs.ur = c.empresa
                                                          AND cs.status = 'A')
              LEFT JOIN `valorescobranca` bc ON ( b.id = bc.idcobranca
                                                    AND bc.idplano = c.convenio
                                                    AND bc.excluido_por IS NULL)
              INNER JOIN planosdesaude AS pl ON ( c.convenio = pl.id)
              {$innerOperadora}
       WHERE ( p.inicio BETWEEN '{$data['inicio']}' AND '{$data['fim']}')
          OR ( p.fim BETWEEN '{$data['inicio']}' AND '{$data['fim']}')
               AND s.tipo IN (4)
               {$compOperadoras}
               AND c.idClientes NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
               AND c.nome NOT LIKE '%ambulancia%'
               AND c.nome NOT LIKE '%UR %'
               AND c.nome NOT LIKE '%CAIXA %'
               AND c.empresa NOT IN (9, 10)
               AND s.catalogo_id NOT IN (17,
                                         18,
                                         21,
                                         24)
               AND bc.excluido_por IS NULL
       GROUP BY paciente
       UNION SELECT c.nome,
                    c.idClientes AS paciente,
                    pl.nome AS nomeplano,
                    e.nome AS nome_empresa
             FROM `itens_prescricao` s
                    LEFT JOIN `frequencia` AS f ON ( s.frequencia = f.id)
                    INNER JOIN prescricoes p ON ( s.`idprescricao` = p.id)
                    INNER JOIN `clientes` c ON ( p.paciente = c.idClientes)
                    INNER JOIN empresas AS e ON ( c.empresa = e.id )
                    INNER JOIN cuidadosespeciais AS esp ON ( s.catalogo_id = esp.id)
                    INNER JOIN `cobrancaplanos` b ON ( esp.cobrancaplano_id = b.id)
                    LEFT JOIN custo_servicos_plano_ur cs ON ( cs.cobrancaplano_id = b.id
                                                                AND cs.ur = c.empresa
                                                                AND cs.status = 'A')
                    LEFT JOIN `valorescobranca` bc ON ( b.id = bc.idcobranca
                                                          AND bc.idplano = c.convenio
                                                          AND bc.excluido_por IS NULL)
                    INNER JOIN planosdesaude AS pl ON ( c.convenio = pl.id)
                    {$innerOperadora}
             WHERE ( p.inicio BETWEEN '{$data['inicio']}' AND '{$data['fim']}'
                       OR p.fim BETWEEN '{$data['inicio']}' AND '{$data['fim']}')
               AND s.tipo IN (2)
               {$compOperadoras}
               AND c.idClientes NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
               AND c.nome NOT LIKE '%ambulancia%'
               AND c.nome NOT LIKE '%UR %'
               AND c.nome NOT LIKE '%CAIXA %'
               AND c.empresa NOT IN (9, 10)
               AND p.carater NOT IN (4,
                                     7)
               AND s.catalogo_id NOT IN (17,
                                         18,
                                         21,
                                         24)
               AND p.desativada_por = 0
             GROUP BY paciente
       UNION SELECT c.nome,
                    c.idClientes AS paciente,
                    pl.nome AS nomeplano,
                    e.nome AS nome_empresa
             FROM remocoes
                    INNER JOIN clientes AS c ON remocoes.paciente_id = c.idClientes
                    INNER JOIN empresas AS e ON remocoes.empresa_id = e.id
                    INNER JOIN planosdesaude pl ON remocoes.convenio_id = pl.id
                    INNER JOIN cobrancaplanos AS b ON remocoes.cobrancaplano_id = b.id
                    {$innerOperadora}
             WHERE remocoes.data_saida BETWEEN '{$data['inicio']}' AND '{$data['fim']}'
               AND remocoes.modo_cobranca = 'F'
               {$compOperadoras}
               AND remocoes.convenio_id = c.convenio
               AND c.idClientes NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
               AND c.nome NOT LIKE '%ambulancia%'
               AND c.nome NOT LIKE '%UR %'
               AND c.nome NOT LIKE '%CAIXA %'
               AND c.empresa NOT IN (9, 10)
               AND remocoes.finalized_by IS NOT NULL
               AND remocoes.canceled_by IS NULL
               AND remocoes.tabela_origem_item_fatura <> 'valorescobranca'
             GROUP BY paciente
       UNION SELECT c.nome,
                    c.idClientes AS paciente,
                    pl.nome AS nomeplano,
                    e.nome AS nome_empresa
             FROM remocoes
                    INNER JOIN clientes AS c ON remocoes.paciente_id = c.idClientes
                    INNER JOIN empresas AS e ON remocoes.empresa_id = e.id
                    INNER JOIN planosdesaude pl ON remocoes.convenio_id = pl.id
                    INNER JOIN valorescobranca ON remocoes.valorescobranca_id = valorescobranca.id
                    {$innerOperadora}
             WHERE remocoes.data_saida BETWEEN '{$data['inicio']}' AND '{$data['fim']}'
               AND remocoes.modo_cobranca = 'F'
               {$compOperadoras}
               AND remocoes.convenio_id = c.convenio
               AND c.idClientes NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
               AND c.nome NOT LIKE '%ambulancia%'
               AND c.nome NOT LIKE '%UR %'
               AND c.empresa NOT IN (9, 10)
               AND remocoes.finalized_by IS NOT NULL
               AND remocoes.canceled_by IS NULL
               AND remocoes.tabela_origem_item_fatura = 'valorescobranca'
             GROUP BY paciente,
                      valorescobranca.id
       UNION SELECT c.nome,
                    c.idClientes AS paciente,
                    pl.nome AS nomeplano,
                    e.nome AS nome_empresa
             FROM `prescricao_enf_itens_prescritos` AS s
                    LEFT JOIN `frequencia` AS f ON ( s.frequencia_id = f.id)
                    INNER JOIN prescricao_enf p ON ( s.prescricao_enf_id = p.id)
                    INNER JOIN `clientes` c ON ( p.cliente_id = c.idClientes)
                    INNER JOIN empresas AS e ON ( c.empresa = e.id )
                    INNER JOIN prescricao_enf_cuidado AS esp ON ( s.presc_enf_cuidados_id = esp.id)
                    INNER JOIN cuidadosespeciais AS cui ON esp.cuidado_especiais_id = cui.id
                    INNER JOIN `cobrancaplanos` b ON ( cui.cobrancaplano_id = b.id)
                    LEFT JOIN `valorescobranca` bc ON ( b.id = bc.idcobranca
                                                          AND bc.idplano = c.convenio
                                                          AND bc.excluido_por IS NULL)
                    LEFT JOIN custo_servicos_plano_ur cs ON ( cs.cobrancaplano_id = b.id
                                                                AND cs.ur = c.empresa
                                                                AND cs.status = 'A')
                    INNER JOIN planosdesaude AS pl ON ( c.convenio = pl.id)
                    {$innerOperadora}
             WHERE (( p.inicio BETWEEN '{$data['inicio']}' AND '{$data['fim']}')
                      OR ( p.fim BETWEEN '{$data['inicio']}' AND '{$data['fim']}'))
               AND p.cancelado_por IS NULL
               {$compOperadoras}
               AND c.idClientes NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
               AND c.nome NOT LIKE '%ambulancia%'
               AND c.nome NOT LIKE '%UR %'
               AND c.nome NOT LIKE '%CAIXA %'
               AND c.empresa NOT IN (9, 10)
               AND esp.presc_enf_grupo_id = 7
               AND p.carater IN (8,
                                 9)
             GROUP BY paciente
       UNION
       SELECT DISTINCT c.nome,
                       c.idClientes AS paciente,
                       pl.nome AS nomeplano,
                       es.nome AS nome_empresa
       FROM clientes c
              INNER JOIN empresas AS es ON ( c.empresa = es.id )
              INNER JOIN equipamentosativos e ON ( c.idClientes = e.`paciente_id`)
              INNER JOIN cobrancaplanos co ON ( e.`cobranca_planos_id` = co.`id`)
              LEFT JOIN custo_servicos_plano_ur cs ON ( cs.cobrancaplano_id = co.id
                                                          AND cs.ur = c.empresa
                                                          AND cs.status = 'A')
              LEFT JOIN valorescobranca v ON ( co.`id` = v.`idcobranca`
                                                 AND c.convenio = v.`idplano`
                                                 AND v.excluido_por IS NULL)
              INNER JOIN planosdesaude AS pl ON ( c.convenio = pl.id)
              {$innerOperadora}
       WHERE e.prescricao_avaliacao != 'S'
         AND c.idClientes NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
         {$compOperadoras}
         AND c.nome NOT LIKE '%ambulancia%'
         AND c.nome NOT LIKE '%UR %'
         AND c.nome NOT LIKE '%CAIXA %'
         AND c.empresa NOT IN (9, 10)
         AND (e.DATA_INICIO <> '0000-00-00 00:00:00' AND e.DATA_INICIO IS NOT NULL) AND
             ( ( e.DATA_INICIO BETWEEN '{$data['inicio']}' AND '{$data['fim']}' ) OR
               ( e.DATA_FIM BETWEEN '{$data['inicio']}' AND '{$data['fim']}') OR
               (e.DATA_INICIO < '{$data['inicio']}' AND (  e.`DATA_FIM`='0000-00-00 00:00:00' OR e.`DATA_FIM` >'{$data['fim']}' )))
       GROUP BY paciente
     ) AS principal
    GROUP BY paciente
    
    UNION 
    
    SELECT
            c.nome,
            MAX(c.idClientes) AS paciente,
            pl.nome AS nomeplano,
            es.nome AS nome_empresa
          FROM
            historico_status_paciente AS hsp
            INNER JOIN (
                  SELECT
                         MAX(ID) AS id
                  FROM
                       historico_status_paciente
                  WHERE
                      historico_status_paciente.DATA < '{$data['inicio']}'
                  GROUP BY PACIENTE_ID
                  ) as hsp1 ON hsp.ID = hsp1.id
            INNER JOIN clientes AS c ON hsp.PACIENTE_ID = c.idClientes
            INNER JOIN planosdesaude AS pl ON c.convenio = pl.id
            INNER JOIN empresas AS es ON c.empresa = es.id
WHERE
    hsp.STATUS_PACIENTE_ID = 4
  AND hsp.PACIENTE_ID NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
  AND c.nome NOT LIKE '%ambulancia%'
 AND c.nome NOT LIKE '%UR %'
 AND c.nome NOT LIKE '%CAIXA %'
 AND c.empresa NOT IN (9, 10)
 GROUP BY PACIENTE_ID
UNION
  SELECT
    c.nome,
    MAX(c.idClientes) AS paciente,
    pl.nome AS nomeplano,
    es.nome AS nome_empresa
  FROM
     historico_status_paciente AS hsp
     INNER JOIN clientes AS c ON hsp.PACIENTE_ID = c.idClientes
     INNER JOIN planosdesaude AS pl ON c.convenio = pl.id
     INNER JOIN empresas AS es ON c.empresa = es.id
WHERE
    hsp.STATUS_PACIENTE_ID = 4
  AND hsp.DATA BETWEEN '{$data['inicio']}' AND '{$data['fim']}'
  AND hsp.PACIENTE_ID NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
  AND c.nome NOT LIKE '%ambulancia%'
 AND c.nome NOT LIKE '%UR %'
 AND c.nome NOT LIKE '%CAIXA %'
 AND c.empresa NOT IN (9, 10)
  AND PACIENTE_ID NOT IN (
                         SELECT
                                PACIENTE_ID
                         FROM
                              historico_status_paciente AS hsp2
                                INNER JOIN (
                                           SELECT
                                                  MAX(ID) AS id
                                           FROM
                                                historico_status_paciente
                                           WHERE
                                               historico_status_paciente.DATA < '{$data['inicio']}'
                                           GROUP BY PACIENTE_ID
                                           ) as hsp1 ON hsp2.ID = hsp1.id
                                INNER JOIN clientes ON hsp2.PACIENTE_ID = clientes.idClientes
                         WHERE
                             hsp2.STATUS_PACIENTE_ID = 4
                           AND hsp2.PACIENTE_ID NOT IN (142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
                         )
  GROUP BY PACIENTE_ID
ORDER BY nomeplano, nome;
SQL;
        /*$row = parent::get($sql, 'Assoc');

        $notInPaciente = array_reduce($row, function ($buffer, $value) {
            return $buffer .= $value['paciente'] . ', ';
        });

        $sql = <<<SQL
SELECT
            c.nome,
            MAX(c.idClientes) AS paciente,
            pl.nome AS nomeplano,
            es.nome AS nome_empresa
          FROM
            historico_status_paciente AS hsp
            INNER JOIN (
                  SELECT
                         MAX(ID) AS id
                  FROM
                       historico_status_paciente
                  WHERE
                      historico_status_paciente.DATA < '{$data['inicio']}'
                  GROUP BY PACIENTE_ID
                  ) as hsp1 ON hsp.ID = hsp1.id
            INNER JOIN clientes AS c ON hsp.PACIENTE_ID = c.idClientes
            INNER JOIN planosdesaude AS pl ON c.convenio = pl.id
            INNER JOIN empresas AS es ON c.empresa = es.id
WHERE
    hsp.STATUS_PACIENTE_ID = 4
  AND hsp.PACIENTE_ID NOT IN ({$notInPaciente}142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
  AND c.nome NOT LIKE '%ambulancia%'
 AND c.nome NOT LIKE '%UR %'
 AND c.nome NOT LIKE '%CAIXA %'
 AND c.empresa NOT IN (9, 10)
 GROUP BY PACIENTE_ID
UNION
  SELECT
    c.nome,
    MAX(c.idClientes) AS paciente,
    pl.nome AS nomeplano,
    es.nome AS nome_empresa
  FROM
     historico_status_paciente AS hsp
     INNER JOIN clientes AS c ON hsp.PACIENTE_ID = c.idClientes
     INNER JOIN planosdesaude AS pl ON c.convenio = pl.id
     INNER JOIN empresas AS es ON c.empresa = es.id
WHERE
    hsp.STATUS_PACIENTE_ID = 4
  AND hsp.DATA BETWEEN '{$data['inicio']}' AND '{$data['fim']}'
  AND hsp.PACIENTE_ID NOT IN ({$notInPaciente}142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
  AND c.nome NOT LIKE '%ambulancia%'
 AND c.nome NOT LIKE '%UR %'
 AND c.nome NOT LIKE '%CAIXA %'
 AND c.empresa NOT IN (9, 10)
  AND PACIENTE_ID NOT IN (
                         SELECT
                                PACIENTE_ID
                         FROM
                              historico_status_paciente AS hsp2
                                INNER JOIN (
                                           SELECT
                                                  MAX(ID) AS id
                                           FROM
                                                historico_status_paciente
                                           WHERE
                                               historico_status_paciente.DATA < '{$data['inicio']}'
                                           GROUP BY PACIENTE_ID
                                           ) as hsp1 ON hsp2.ID = hsp1.id
                                INNER JOIN clientes ON hsp2.PACIENTE_ID = clientes.idClientes
                         WHERE
                             hsp2.STATUS_PACIENTE_ID = 4
                           AND hsp2.PACIENTE_ID NOT IN ({$notInPaciente}142, 67, 69, 128, 434, 220, 263, 112, 635, 101, 110)
                         )
  GROUP BY PACIENTE_ID
SQL;*/
        return parent::get($sql, 'assoc');
    }

} 