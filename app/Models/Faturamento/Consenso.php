<?php
/**
 * Created by PhpStorm.
 * User: jefinho
 * Date: 15/02/2018
 * Time: 13:51
 */

namespace App\Models\Faturamento;

use App\Helpers\StringHelper;
use App\Models\AbstractModel;
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/codigos.php');


class Consenso extends AbstractModel
{
    public static function salvar($dados)
    {

        mysql_query('begin');

        $idFatura = $dados['fatura-id'];
        $existeConsenso = self::getConsensoByFaturaId($idFatura);

        $usuario = $_SESSION['id_user'];
        $auditoraExterna = $dados['auditora-externa'];
        $paciente = $dados['paciente-id'];



        if(empty($existeConsenso)){

            $sql = "Select * from faturamento where FATURA_ID = '{$idFatura}' and INCLUSO_PACOTE = 0";
            $result =  mysql_query($sql);
            if(!$result){
                echo "-20";
                mysql_query("ROLLBACK");
                return;
            }

            while($row = mysql_fetch_array($result)){
                $sqlItensOriginais = "INSERT INTO fatura_itens_originais_cobrados ( NUMERO_TISS, CODIGO_PROPRIO, QUANTIDADE, TIPO,
                            DATA_FATURADO, PACIENTE_ID, USUARIO_ID, VALOR_FATURA, VALOR_CUSTO, FATURA_ID, UNIDADE, CATALOGO_ID,
                             TABELA_ORIGEM, DESCONTO_ACRESCIMO, INCLUSO_PACOTE, TUSS, CODIGO_REFERENCIA,
                             faturamento_id)
                            VALUES ('{$row['NUMERO_TISS']}','{$row['CODIGO_PROPRIO']}','{$row['QUANTIDADE']}','{$row['TIPO']}',
                           '{$row['DATA_FATURADO']}','{$row['PACIENTE_ID']}','{$row['USUARIO_ID']}','{$row['VALOR_FATURA']}','{$row['VALOR_CUSTO']}','{$row['FATURA_ID']}','{$row['UNIDADE']}','{$row['CATALOGO_ID']}',
                            '{$row['TABELA_ORIGEM']}','{$row['DESCONTO_ACRESCIMO']}','{$row['INCLUSO_PACOTE']}','{$row['TUSS']}','{$row['ID']}',
                            '{$row['ID']}')";
                $rItensOriginais = mysql_query($sqlItensOriginais);
                if(!$rItensOriginais){
                    echo "Erro ao salvar itens originais da fatura";
                    mysql_query("ROLLBACK");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sqlItensOriginais)));
            }

        }

        foreach ($dados['editar'] as $key => $value){
            if($value == 'S'){
                $sql = "Select * from faturamento where ID = $key ";
                $result =  mysql_query($sql);
                if(!$result){
                    echo "Erro ao encontrar item na tabela faturamento";
                    mysql_query("ROLLBACK");
                    return;
                }
                while($row = mysql_fetch_array($result)){
                    $sqlHistorico = "INSERT INTO historico_faturamento 
                      (SOLICITACOES_ID, NUMERO_TISS, CODIGO_PROPRIO, QUANTIDADE, TIPO, DATA_FATURADO,
                       PACIENTE_ID, USUARIO_ID, VALOR_FATURA, VALOR_CUSTO, FATURA_ID, UNIDADE, 
                       CATALOGO_ID, TABELA_ORIGEM, DESCONTO_ACRESCIMO, INCLUSO_PACOTE, TUSS, CODIGO_REFERENCIA,
                             CANCELADO_POR,CANCELADO_EM,custo_atualizado_em,custo_atualizado_por,faturamento_id, MOTIVO_GLOSA)
                            VALUES ('{$row['SOLICITACOES_ID']}', '{$row['NUMERO_TISS']}','{$row['CODIGO_PROPRIO']}','{$row['QUANTIDADE']}','{$row['TIPO']}',
                           '{$row['DATA_FATURADO']}','{$row['PACIENTE_ID']}','{$row['USUARIO_ID']}','{$row['VALOR_FATURA']}','{$row['VALOR_CUSTO']}','{$row['FATURA_ID']}','{$row['UNIDADE']}','{$row['CATALOGO_ID']}',
                            '{$row['TABELA_ORIGEM']}','{$row['DESCONTO_ACRESCIMO']}','{$row['INCLUSO_PACOTE']}','{$row['TUSS']}','{$key}',
                            '{$usuario}', now(),'{$row['custo_atualizado_em']}','{$row['custo_atualizado_por']}','{$key}','{$row['MOTIVO_GLOSA']}')";
                    $rHistorico = mysql_query($sqlHistorico);
                    if(!$rHistorico){
                        echo "Erro ao criar histórico do faturamento.".$sqlHistorico;
                        mysql_query("ROLLBACK");
                        return;
                    }
                    savelog(mysql_escape_string(addslashes($sqlHistorico)));
                }
                $valor = StringHelper::moneyStringToFloat($dados['valor'][$key]);
                $inclusoPacote = $valor == '0.00' ? 1 : 0;
                $taxa = StringHelper::moneyStringToFloat($dados['taxa'][$key]);
                $sqlUpdate = "update
                        faturamento
                        set                         
                        QUANTIDADE = '{$dados['quantidade'][$key]}',
                        DATA_FATURADO = now(),
                        USUARIO_ID = '{$usuario}',
                        VALOR_FATURA = '{$valor}',
                        UNIDADE = '{$dados['unidade'][$key]}',
                        DESCONTO_ACRESCIMO = '{$taxa}',
                        INCLUSO_PACOTE = '{$inclusoPacote}',
                        MOTIVO_GLOSA = '{$dados['motivo'][$key]}'
                        where
                        ID = '{$key}'";

                $rUpdate = mysql_query($sqlUpdate);
                if(!$rUpdate){
                    echo "Erro ao atualizar tabela do faturamento.";
                    mysql_query("ROLLBACK");
                    return;
                }
                savelog(mysql_escape_string(addslashes($sql)));
            }
        }

        if(isset($dados['novoItemCatalogoId'])){
            foreach ($dados['novoItemCatalogoId'] as $key => $catalogoId){
                $valor = StringHelper::moneyStringToFloat($dados['novoItemValor'][$key]);
                $taxa = StringHelper::moneyStringToFloat($dados['novoItemTaxa'][$key]);
                $sql = "INSERT INTO 
                          faturamento 
                        (ID, NUMERO_TISS,QUANTIDADE, TIPO, DATA_FATURADO, PACIENTE_ID, USUARIO_ID, 
                        VALOR_FATURA, VALOR_CUSTO, FATURA_ID ,UNIDADE, CATALOGO_ID, TABELA_ORIGEM, DESCONTO_ACRESCIMO,
                         INCLUSO_PACOTE, TUSS, CODIGO_REFERENCIA)
                            VALUES 
                            (null,'{$dados['novoItemTiss'][$key]}','{$dados['novoItemQtd'][$key]}','{$dados['novoItemTipo'][$key]}',now(),'{$paciente}','{$usuario}',
                            '{$valor}','{$dados['novoItemCusto'][$key]}','{$idFatura}','{$dados['novoItemUnidade'][$key]}','{$catalogoId}','{$dados['novoItemTabela'][$key]}','{$dados['novoItemTaxa'][$key]}',
                            0,'{$dados['novoItemTuss'][$key]}','{$idFatura}')";

                $r = mysql_query($sql);
                if(!$r){
                    echo "Erro ao inserir item no faturamento.";
                    mysql_query("ROLLBACK");
                    return;
                }
            }
        }

        $sql = <<<SQL
INSERT 
fatura_consenso
(
id,                
  fatura_id,           
    created_by,           
  created_at,           
  auditora_externa
)
VALUE 
(
null,
{$idFatura},
{$usuario},
now(),
'{$auditoraExterna}'
)
SQL;
        $r = mysql_query($sql);
        if(!$r){
            echo "Erro ao criar consenso.";
            mysql_query("ROLLBACK");
            return;
        }

        mysql_query("COMMIT");
        echo 1;



    }

    public static function getConsensoByFaturaId($faturaId)
    {
        $sql = <<<SQL
         Select 
        *
        from
        fatura_consenso
        where
        fatura_id = $faturaId

SQL;

       return parent::get($sql); // TODO: Change the autogenerated stub
    }


}