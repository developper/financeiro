<?php

namespace App\Models\Faturamento\XML;

interface GuiaTissXMLInterface
{	public function loadXML();
}

interface GuiaTissXMLElement
{	public function addElementTo(\SimpleXMLElement $guiaTissXML);
}

class Utils
{

    public static function decimal($number, $decimals = 2)
    {
        return number_format($number, $decimals, '.', '');
    }

    public static function simNao($bool)
    {
        return $bool ? 'S' : 'N';
    }

    public static function zeroToLeft($input, $pad_length)
    {
        return str_pad($input, $pad_length, "0", STR_PAD_LEFT);
    }

    public static function format($val, $type = 'cpf')
    {
        $masks = ['cpf' => '012.345.678-910', 'cnpj' => '01.234.567/891011-1213'];
        return isset($masks[$type]) ? strtr($masks[$type], str_split($val)) : false;
    }

    public static function money($val)
    {
        return sprintf('R$ %s', number_format((double) $val, 2, ',', '.'));
    }

}

class GuiaTissXML implements GuiaTissXMLInterface
{

    const PREFIX_NS = '';

    public $xml;

    protected $guiaTiss;
    protected $pathFileXML;
    protected $domDocument;
    protected $epilogo;

    public function __construct(GuiaTiss $guia = null, $xml = null)
    {
        $this->guiaTiss = $guia;
        if (isset($xml) && !empty($xml))
            $this->xml = $xml;
        else
            $this->loadXML();
    }

    public function getGuiaTiss()
    { return $this->guiaTiss;
    }

    public function getEpilogo()
    { return $this->epilogo;
    }

    public function getAsDomDocument()
    {
        if (! isset($this->xml))
            throw new \Exception('XML não foi carregado');

        $domElement = dom_import_simplexml($this->xml);
        if (false === $domElement)
            throw new \Exception('Não foi possível converter XMLElement para DOMElement');

        $dom = new \DOMDocument('1.0');
        $domElement = $dom->importNode($domElement, true);
        $domElement = $dom->appendChild($domElement);
        return $dom;
    }

    public function loadFromFile($pathFileXML)
    {
        if (! file_exists($pathFileXML))
            throw new \Exception(sprintf('Arquivo "%s" não foi encontrado!', $pathFileXML));
        $this->domDocument = new \DOMDocument();
        $this->domDocument->load($pathFileXML);
        return $this->domDocument;
    }

    public function isValid(\DOMDocument $doc = null,
                            $schema = '/V030200/XSD/tissV3_02_00.xsd')
    {
        $doc = isset($doc) ? $doc : $this->getAsDomDocument();
        $schema = (__DIR__) . $schema;
        if (! file_exists($schema))
            throw new \Exception(sprintf('Arquivo "%s" não foi encontrado!', $schema));

        libxml_use_internal_errors(true);

        return $doc->schemaValidate($schema) ? true : libxml_get_errors();
    }

    public function loadXML()
    {
        $xmlBase = <<<XML
<?xml version="1.0" encoding="ISO-8859-1"?>
<ansTISS:mensagemTISS 
	xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns:tns="http://www.ans.gov.br/tiss/ws/tipos/tissLoteGuias/v20203" 
	xmlns:ansTISS="http://www.ans.gov.br/padroes/tiss/schemas">
</ansTISS:mensagemTISS>
XML;

        $this->xml = new \SimpleXMLElement($xmlBase);
        $this->xml->registerXPathNamespace('ansTISS', 'http://www.w3.org/2001/XMLSchema');
        if (isset($this->guiaTiss))
            $this->guiaTiss->addElementTo($this->xml);
        return $this->xml;
    }

    public function getXML()
    {	return $this->xml;
    }

    public function importFromData($data, $versao = '3.03.01', $planoId = null)
    {

        $xmlBase = <<<XML
<?xml version="1.0" encoding="ISO-8859-1"?>
<ans:mensagemTISS
	xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:ans="http://www.ans.gov.br/padroes/tiss/schemas">
</ans:mensagemTISS>
XML;

        $this->xml = new \SimpleXMLElement($xmlBase);
        $this->xml->registerXPathNamespace('ans', 'http://www.w3.org/2001/XMLSchema');

        $now = new \DateTime();

        $cabecalho = $this->xml->addChild('cabecalho');
        $identificacaoTransacao = $cabecalho->addChild('identificacaoTransacao');
        $identificacaoTransacao->addChild('tipoTransacao', $data['tipoTransacao']); //dm_tipoTransacao
        $identificacaoTransacao->addChild('sequencialTransacao', $data['sequencialTransacao']);
        $identificacaoTransacao->addChild('dataRegistroTransacao', $now->format('Y-m-d'));
        $identificacaoTransacao->addChild('horaRegistroTransacao', $now->format('H:i:s'));

        $origem = $cabecalho->addChild('origem');
        $identificacaoPrestador = $origem->addChild('identificacaoPrestador');

        $identificacaoPrestador->addChild($data['identificacaoPrestador']['tag'],
            $data['identificacaoPrestador']['valor']);


        $destino = $cabecalho->addChild('destino');
        $destino->addChild('registroANS', $data['destino']['registroANS']);

        $conselhoPrefixo = '0';
        if($versao == '3.03.01' || $versao == '3.03.00' || $versao == '3.03.02' || $versao == '3.03.03' ) {
            $cabecalho->addChild('Padrao', $versao);
        } elseif ($versao == '3.02.00') {
            $conselhoPrefixo = '';
            $cabecalho->addChild('versaoPadrao', $versao);
        }

        $prestadorParaOperadora = $this->xml->addChild('prestadorParaOperadora');
        $loteGuias = $prestadorParaOperadora->addChild('loteGuias');
        $loteGuias->addChild('numeroLote', $data['numeroLote']);//st_texto12

        $guiaTiss = $loteGuias->addChild('guiasTISS');

        $guiaChildName = 'guiaSP-SADT';
        if ($data['tipoGuia'] === Guia::RESUMO_INTERNACAO)
            $guiaChildName = 'guiaResumoInternacao';
        foreach ($data['guias'] as $guiaData) {

            $guia = $guiaTiss->addChild($guiaChildName);

            $cabecalhoGuia = $guia->addChild('cabecalhoGuia');
            $cabecalhoGuia->addChild('registroANS', $data['destino']['registroANS']);
            $cabecalhoGuia->addChild('numeroGuiaPrestador', $guiaData['numeroGuiaPrestador']);





            if ($data['tipoGuia'] === Guia::RESUMO_INTERNACAO) {
                $guia->addChild('numeroGuiaSolicitacaoInternacao', $guiaData['numeroGuiaSolicitacaoInternacao']);
            }


            $dadosAutorizacao = $guia->addChild('dadosAutorizacao');
            if(!empty($guiaData['numeroGuiaOperadora'])){
                $dadosAutorizacao->addChild('numeroGuiaOperadora', $guiaData['numeroGuiaOperadora']);
            }
            $dadosAutorizacao->addChild('dataAutorizacao', $guiaData['dadosAutorizacao']['dataAutorizacao']);

            if (isset($guiaData['dadosAutorizacao']['senhaAutorizacao']) && !empty($guiaData['dadosAutorizacao']['senhaAutorizacao']))
                $dadosAutorizacao->addChild('senha', $guiaData['dadosAutorizacao']['senhaAutorizacao']);

            $dadosBeneficiario = $guia->addChild('dadosBeneficiario');
            $dadosBeneficiario->addChild('numeroCarteira', $guiaData['dadosBeneficiario']['numeroCarteira']);
            $dadosBeneficiario->addChild('atendimentoRN', $guiaData['dadosBeneficiario']['atendimentoRN']);
            $dadosBeneficiario->addChild('nomeBeneficiario', $guiaData['dadosBeneficiario']['nomeBeneficiario']);
            $dadosBeneficiario->addChild('identificadorBeneficiario', $guiaData['dadosBeneficiario']['identificadorBeneficiario']); //base64Binary

            if ($data['tipoGuia'] === Guia::RESUMO_INTERNACAO) {
                $dadosExecutante = $guia->addChild('dadosExecutante');
                $contratadoExecutante = $dadosExecutante->addChild('contratadoExecutante');
                $contratadoExecutante->addChild('cnpjContratado', $guiaData['dadosExecutante']['contratadoExecutante']['cnpjContratado']);
                $contratadoExecutante->addChild('nomeContratado', $guiaData['dadosExecutante']['contratadoExecutante']['nomeContratado']);
                $cnes = $dadosExecutante->addChild('CNES', $guiaData['dadosExecutante']['CNES']);

                $dadosInternacao = $guia->addChild('dadosInternacao');
                $dadosInternacao->addChild('caraterAtendimento', $guiaData['caraterAtendimento']);
                $dadosInternacao->addChild('tipoFaturamento', $guiaData['tipoFaturamento']);
                $dadosInternacao->addChild('dataInicioFaturamento', $guiaData['dataHoraInicioFaturamento']->format('Y-m-d'));
                $dadosInternacao->addChild('horaInicioFaturamento', $guiaData['dataHoraInicioFaturamento']->format('H:i:s'));
                $dadosInternacao->addChild('dataFinalFaturamento', $guiaData['dataHoraFinalFaturamento']->format('Y-m-d'));
                $dadosInternacao->addChild('horaFinalFaturamento', '23:00:00');
                $dadosInternacao->addChild('tipoInternacao', $guiaData['tipoInternacao']);
                $dadosInternacao->addChild('regimeInternacao', $guiaData['regimeInternacao']);

                $dadosSaidaInternacao = $guia->addChild('dadosSaidaInternacao');
                $dadosSaidaInternacao->addChild('indicadorAcidente', $guiaData['indicadorAcidente']);
                $dadosSaidaInternacao->addChild('motivoEncerramento', $guiaData['motivoEncerramento']);
            }

            if ($data['tipoGuia'] === Guia::SP_SADT) {

                $dadosSolicitante = $guia->addChild('dadosSolicitante');
                $contratadoSolicitante = $dadosSolicitante->addChild('contratadoSolicitante');
                $contratadoSolicitante->addChild('cnpjContratado', $guiaData['contratadoSolicitante']['cnpjContratado']);
                $contratadoSolicitante->addChild('nomeContratado', $guiaData['contratadoSolicitante']['nomeContratado']);

                if (isset($guiaData['profissionalSolicitante']) && !empty($guiaData['profissionalSolicitante'])) {
                    $profissionalSolicitante = $dadosSolicitante->addChild('profissionalSolicitante');
                    $profissionalSolicitante->addChild('nomeProfissional', $guiaData['profissionalSolicitante']['profissionalSolicitante']);
                    $profissionalSolicitante->addChild('conselhoProfissional', $conselhoPrefixo . $guiaData['profissionalSolicitante']['conselhoProfissional']);
                    $profissionalSolicitante->addChild('numeroConselhoProfissional', $guiaData['profissionalSolicitante']['numeroConselhoProfissional']);
                    $profissionalSolicitante->addChild('UF', $guiaData['profissionalSolicitante']['UF']);
                    $profissionalSolicitante->addChild('CBOS', $guiaData['profissionalSolicitante']['CBOS']);
                }

                $dadosSolicitacao = $guia->addChild('dadosSolicitacao');
                $dadosSolicitacao->addChild('caraterAtendimento', $guiaData['caraterAtendimento']);

                $dadosExecutante = $guia->addChild('dadosExecutante');
                $contratadoExecutante = $dadosExecutante->addChild('contratadoExecutante');
                $contratadoExecutante->addChild('cnpjContratado', $guiaData['dadosExecutante']['contratadoExecutante']['cnpjContratado']);
                $contratadoExecutante->addChild('nomeContratado', $guiaData['dadosExecutante']['contratadoExecutante']['nomeContratado']);

                $cnes = $dadosExecutante->addChild('CNES', $guiaData['dadosExecutante']['CNES']);

                $dadosAtendimento = $guia->addChild('dadosAtendimento');
                $dadosAtendimento->addChild('tipoAtendimento', $guiaData['dadosAtendimento']['tipoAtendimento']);
                $dadosAtendimento->addChild('indicacaoAcidente', $guiaData['dadosAtendimento']['indicacaoAcidente']);
            }

            if (isset($guiaData['procedimentos']) && !empty($guiaData['procedimentos'])) {
                $procedimentosExecutados = $guia->addChild('procedimentosExecutados');
                foreach ($guiaData['procedimentos'] as $procedimento) {



                    $procedimentoExecutado = $procedimentosExecutados->addChild('procedimentoExecutado');
                    $procedimentoExecutado->addChild('dataExecucao', $procedimento['dataExecucao']);
                    $procedimentoElement = $procedimentoExecutado->addChild('procedimento');
                    $procedimentoElement->addChild('codigoTabela',          $procedimento['codigoTabela']);
                    $procedimentoElement->addChild('codigoProcedimento',    $procedimento['codigoProcedimento']);
                    $procedimentoElement->addChild('descricaoProcedimento', $procedimento['descricaoProcedimento']);
                    $procedimentoExecutado->addChild('quantidadeExecutada', $procedimento['quantidadeExecutada']);
                    //$procedimentoExecutado->addChild('reducaoAcrescimo',    ($planoId == 62 || $planoId == 55) ? 1.00 :$procedimento['reducaoAcrescimo']);
                   $procedimentoExecutado->addChild('reducaoAcrescimo',    1.00 );
                    $procedimentoExecutado->addChild('valorUnitario',       $procedimento['valorUnitario']);
                    $procedimentoExecutado->addChild('valorTotal',          $procedimento['valorTotal']);
                }
            }

            if ($data['tipoGuia'] === Guia::SP_SADT
                && isset($guiaData['outrasDespesas']) && !empty($guiaData['outrasDespesas'])) {

                $outrasDespesas = $guia->addChild('outrasDespesas');
                foreach ($guiaData['outrasDespesas'] as $outraDespesa) {
                    $despesa = $outrasDespesas->addChild('despesa');
                    $despesa->addChild('codigoDespesa', $outraDespesa['codigoDespesa']); //dm_outrasDespesas
                    $servicosExecutados = $despesa->addChild('servicosExecutados');
                    $servicosExecutados->addChild('dataExecucao',          $outraDespesa['dataExecucao']);
                    $servicosExecutados->addChild('codigoTabela',          $outraDespesa['codigoTabela']); //dm_tabela
                    $servicosExecutados->addChild('codigoProcedimento',    $outraDespesa['codigoProcedimento']);
                    $servicosExecutados->addChild('quantidadeExecutada',   $outraDespesa['quantidadeExecutada']); //st_decimal7-4
                    $servicosExecutados->addChild('unidadeMedida',         $outraDespesa['unidadeMedida']); //dm_unidadeMedida
//                    $servicosExecutados->addChild('reducaoAcrescimo',      ($planoId == 62 || $planoId == 55) ? 1.00 :$outraDespesa['reducaoAcrescimo']); //st_decimal3-2
                    $servicosExecutados->addChild('reducaoAcrescimo',       1.00 ); //st_decimal3-2

                    $servicosExecutados->addChild('valorUnitario',         $outraDespesa['valorUnitario']); //st_decimal8-2
                    $servicosExecutados->addChild('valorTotal',            $outraDespesa['valorTotal']); //st_decimal8-2
                    $servicosExecutados->addChild('descricaoProcedimento', $outraDespesa['descricaoProcedimento']); //st_texto150
                }
            }

            $valorTotal = $guia->addChild('valorTotal');
            $valorTotal->addChild('valorProcedimentos',   $guiaData['valorProcedimentos']); //st_decimal10-2
            $valorTotal->addChild('valorDiarias',         $guiaData['valorDiarias']); //st_decimal10-2
            $valorTotal->addChild('valorTaxasAlugueis',   $guiaData['valorTaxasAlugueis']); //st_decimal10-2
            $valorTotal->addChild('valorMateriais',       $guiaData['valorMateriais']); //st_decimal10-2
            $valorTotal->addChild('valorMedicamentos',    $guiaData['valorMedicamentos']); //st_decimal10-2
            $valorTotal->addChild('valorOPME',            $guiaData['valorOPME']); //st_decimal10-2
            $valorTotal->addChild('valorGasesMedicinais', $guiaData['valorGasesMedicinais']); //st_decimal10-2
            $valorTotal->addChild('valorTotalGeral',      $guiaData['valorTotalGeral']); //st_decimal10-2

            /**
             * Na GUIA RESUMO DE INTERNAÇÃO a tag
             * "Outras despesas" deve ficar depois do "Valor Total"
             */
            if ($data['tipoGuia'] === Guia::RESUMO_INTERNACAO
                && isset($guiaData['outrasDespesas']) && !empty($guiaData['outrasDespesas'])) {

                $outrasDespesas = $guia->addChild('outrasDespesas');
                foreach ($guiaData['outrasDespesas'] as $outraDespesa) {
                    $despesa = $outrasDespesas->addChild('despesa');
                    $despesa->addChild('codigoDespesa', $outraDespesa['codigoDespesa']); //dm_outrasDespesas
                    $servicosExecutados = $despesa->addChild('servicosExecutados');
                    $servicosExecutados->addChild('dataExecucao',          $outraDespesa['dataExecucao']);
                    $servicosExecutados->addChild('codigoTabela',          $outraDespesa['codigoTabela']); //dm_tabela
                    $servicosExecutados->addChild('codigoProcedimento',    $outraDespesa['codigoProcedimento']);
                    $servicosExecutados->addChild('quantidadeExecutada',   $outraDespesa['quantidadeExecutada']); //st_decimal7-4
                    $servicosExecutados->addChild('unidadeMedida',         $outraDespesa['unidadeMedida']); //dm_unidadeMedida
//                    $servicosExecutados->addChild('reducaoAcrescimo',     ($planoId == 62 || $planoId == 55) ? 1.00 : $outraDespesa['reducaoAcrescimo']); //st_decimal3-2
                    $servicosExecutados->addChild('reducaoAcrescimo',      1.00 ); //st_decimal3-2
                    $servicosExecutados->addChild('valorUnitario',         $outraDespesa['valorUnitario']); //st_decimal8-2
                    $servicosExecutados->addChild('valorTotal',            $outraDespesa['valorTotal']); //st_decimal8-2
                    $servicosExecutados->addChild('descricaoProcedimento', $outraDespesa['descricaoProcedimento']); //st_texto150
                }
            }

        }
        $epilogo = $this->xml->addChild('epilogo');
        $this->epilogo = $this->getHash($this->xml);
        $epilogo->addChild('hash', $this->epilogo);
        return $this->xml;
    }

    public function getHash($xml)
    {
        $allValues = $this->getAllValues($xml->children('ans', true));
        return md5(mb_convert_encoding($allValues, 'ISO-8859-1', 'UTF-8'));
    }

    public function getAllValues($nodes, &$buffer = '', $hashTag = 'hash')
    {
        foreach ($nodes as $tag => $node) {
            if ($node->count() > 0)
                $buffer = $this->getAllValues($node, $buffer);
            $buffer .= $tag == $hashTag ? null : trim((string) $node);
        }
        return $buffer;
    }

}

class GuiaTiss implements GuiaTissXMLElement
{

    protected $cabecalho;
    protected $prestadorParaOperadora;
    protected $epilogo;

    public function setCabecalho(Cabecalho $cabecalho)
    {	$this->cabecalho = $cabecalho;
    }

    public function setPretadorParaOperadora(PrestadorParaOperadora $po)
    {	$this->prestadorParaOperadora = $po;
    }

    public function setEpilogo($hash)
    {	$this->epilogo['hash'] = $hash;
    }

    public function getEpilogo()
    { return $this->epilogo;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $this->cabecalho->addElementTo($xml);
        $this->prestadorParaOperadora->addElementTo($xml);
        $epilogo = $xml->addChild('epilogo');
        $epilogo->addChild('hash', $this->epilogo['hash']);
        return $xml;
    }

    public static function separarItens($itens, $plano)
    {
        $totalGeral = array(
            'gases'              => 0,
            'medicamentos'       => 0,
            'materiais'          => 0,
            'taxas'              => 0,
            'diarias'            => 0,
            'opme'               => 0,
            'valorProcedimentos' => 0
        );
        $procedimentos  = array();
        $outrasDespesas = array();

        $profissionaisExecutantes = array();
        $profissionalExecutante   = array();

        foreach ($itens as $item) {
            if (isset($item['profissional']) && !empty($item['profissional'])) {
                foreach($item['profissional'] as $profissional)
                    $profissionaisExecutantes[] = $profissionalExecutante = array(
                        'id'                         => $profissional['id'],
                        'cpf'                        => $profissional['cpf'],
                        'cpfFormatado'               => Utils::format($profissional['cpf']),
                        'profissionalSolicitante'    => $profissional['nome_completo'],
                        'conselhoProfissional'       => $profissional['tiss_conselho_profissional'],
                        'siglaConselho'              => $profissional['sigla_conselho'],
                        'numeroConselhoProfissional' => $profissional['numero_conselho'],
                        'UF'                         => $profissional['tiss_uf_conselho'],
                        'estadoUF'                   => $profissional['uf_conselho'],
                        'CBOS'                       => $profissional['tiss_cbos']
                    );
            }


            $tipo      = $item['tiss_outras_despesas_tipo_id'];
            $totalItem = $item['valor_unitario'] * $item['quantidade'];

            if (Despesa::GASES == $tipo)
                $totalGeral['gases'] += $totalItem;
            elseif (Despesa::MEDICAMENTOS == $tipo)
                $totalGeral['medicamentos'] += $totalItem;
            elseif (Despesa::MATERIAIS == $tipo)
                $totalGeral['materiais'] += $totalItem;
            elseif (Despesa::TAXAS == $tipo)
                $totalGeral['taxas'] += $totalItem;
            elseif (Despesa::DIARIAS == $tipo || Despesa::ALUGUEIS == $tipo)
                $totalGeral['diarias'] += $totalItem;
            elseif (Despesa::OPME == $tipo || Despesa::ALUGUEIS == $tipo)
                $totalGeral['opme'] += $totalItem;


            if (0 == $item['tiss_outras_despesas_tipo_id']) {
                $dataExecucao = \DateTime::createFromFormat('Y-m-d', $item['data']);

//                if($plano == '55' || $plano == '62' )
//                {
//                   $reducaoAcrescimo = Utils::decimal(1.0) ;
//                }
//                else{
//                    $reducaoAcrescimo = Utils::decimal(0.0);
//                }
                $reducaoAcrescimo = Utils::decimal(1.0) ;

                $procedimentos[] = array(
                    'profissionalExecutante'    => $profissionalExecutante,
                    'dataExecucao'              => $dataExecucao->format('Y-m-d'),
                    'dataExecucaoImpressao'     => $dataExecucao->format('d/m/Y'),
                    'codigoTabela'              => Utils::zeroToLeft($item['tiss_tabela'], 2),
                    'codigoProcedimento'        => $item['codigo'],
                    'descricaoProcedimento'     => str_replace("&","&amp;",$item['descricao']),
                    'quantidadeExecutada'       => Utils::zeroToLeft($item['quantidade'], 3),
                    'reducaoAcrescimo'          => $reducaoAcrescimo,
                    'valorUnitario'             => Utils::decimal($item['valor_unitario']),
                    'valorTotal'                => Utils::decimal($totalItem)
                );
                $totalGeral['valorProcedimentos'] += $totalItem;
            } else {
                $dataExecucao = \DateTime::createFromFormat('Y-m-d', $item['data']);
//                if($plano == '55' || $plano == '62' )
//                {
//                    $reducaoAcrescimo = Utils::decimal(1.0) ;
//                }else{
//                    $reducaoAcrescimo = Utils::decimal(0.0);
//                }
                $reducaoAcrescimo = Utils::decimal(1.0) ;
                $outrasDespesas[] = array(
                    'codigoDespesa'         => Utils::zeroToLeft($item['codigoDespesa'], 2),
                    'dataExecucao'          => $dataExecucao->format('Y-m-d'),
                    'dataExecucaoImpressao' => $dataExecucao->format('d/m/Y'),
                    'codigoTabela'          => Utils::zeroToLeft($item['tissTabela']['tiss_codigo'], 2),
                    'codigoProcedimento'    => $item['codigo'],
                    'quantidadeExecutada'   => Utils::zeroToLeft($item['quantidade'], 4),
                    'unidadeMedida'         => Utils::zeroToLeft($item['tiss_unidade_medida_codigo'], 3),
                    'reducaoAcrescimo'      => $reducaoAcrescimo,
                    'valorUnitario'         => Utils::decimal($item['valor_unitario']),
                    'valorTotal'            => Utils::decimal($totalItem),
                    'descricaoProcedimento' => str_replace("&","&amp;",$item['descricao'])
                );
            }
        }
        return (object) [
            'procedimentos'             => $procedimentos,
            'outrasDespesas'            => $outrasDespesas,
            'totalGeral'                => $totalGeral,
            'profissionaisExecutantes'  => $profissionaisExecutantes
        ];
    }

    public static function createData($cabecalho, $guias, $loteId)
    {
        $data = array();

        $data = array(
            'tipoGuia' => $cabecalho['tipoGuia'],
            'tipoTransacao' => $cabecalho['tpTransacao'],
            'sequencialTransacao' => $cabecalho['seqTransacao'],
            'identificacaoPrestador' => array('tag' => $cabecalho['origem']['tipoIdentificacao'],
                'valor' => $cabecalho['origem']['codigoIdentificacao']),
            'destino' => array('registroANS' => Utils::zeroToLeft($cabecalho['destino']['registroANS'], 6)),
            'registroANS' => $cabecalho['destino']['registroANS'],
            'numeroLote' => $cabecalho['numeroLote']
        );


        foreach ($guias as $guia) {

            $guiaData = array(
                'numeroGuiaPrestador'   => Utils::zeroToLeft($guia['guiaId'], 10),
                 'numeroGuiaOperadora'   => $guia['numero_guia_operadora'],
                'contratadoSolicitante' => array(
                    'cnpjContratado' => $guia['contratado']['cnpj'],
                    'cnpjFormatado'  => Utils::format($guia['contratado']['cnpj'], 'cnpj'),
                    'nomeContratado' => $guia['contratado']['razao_social'],
                    'numeroCNES'     => $guia['contratado']['numero_cnes'],
                ),
                'profissionalSolicitante' => array(
                    'id'                         => $guia['profissionalSolicitante']['id'],
                    'cpf'                        => $guia['profissionalSolicitante']['cpf'],
                    'cpfFormatado'               => Utils::format($guia['profissionalSolicitante']['cpf']),
                    'profissionalSolicitante'    => $guia['profissionalSolicitante']['nome_completo'],
                    'conselhoProfissional'       => $guia['profissionalSolicitante']['tiss_conselho_profissional'],
                    'siglaConselho'              => $guia['profissionalSolicitante']['sigla_conselho'],
                    'numeroConselhoProfissional' => $guia['profissionalSolicitante']['numero_conselho'],
                    'UF'                         => $guia['profissionalSolicitante']['tiss_uf_conselho'],
                    'estadoUF'                   => $guia['profissionalSolicitante']['uf_conselho'],
                    'CBOS'                       => $guia['profissionalSolicitante']['tiss_cbos']
                )
            );

            $dataAutorizacao = \DateTime::createFromFormat('Y-m-d', $guia['data_autorizacao']);
            $validadeSenha   = \DateTime::createFromFormat('Y-m-d', $guia['validade_senha']);

            $guiaData['dadosAutorizacao'] = array(
                'dataAutorizacao'  => $dataAutorizacao->format('Y-m-d'),
                'senhaAutorizacao' => $guia['senha']
            );

            if ($validadeSenha){
                $guiaData['dadosAutorizacao']['validadeSenha'] = $validadeSenha->format('Y-m-d');
                $guiaData['dadosAutorizacao']['validadeSenhaFormatado'] = $validadeSenha->format('d/m/Y');
            }

            if (isset($guia['tipo_guia']) && $guia['tipo_guia'] === Guia::RESUMO_INTERNACAO) {
                $guiaData['numeroGuiaSolicitacaoInternacao'] = $guia['guiaId'];
                $guiaData['caraterAtendimento'] = $guia['carater_atendimento'];
                $guiaData['tipoFaturamento'] = $guia['tipoFaturamento']['tiss_codigo'];
                $guiaData['dataHoraInicioFaturamento'] = \DateTime::createFromFormat('Y-m-d H:i:s', $guia['data_inicio_faturamento']);
                $guiaData['dataHoraFinalFaturamento'] = \DateTime::createFromFormat('Y-m-d H:i:s', $guia['data_fim_faturamento']);
                $guiaData['tipoInternacao'] = $guia['tipoInternacao']['tiss_codigo'];
                $guiaData['regimeInternacao'] = $guia['regimeInternacao']['tiss_codigo'];
                $guiaData['indicadorAcidente'] = $guia['indicadorAcidente']['tiss_codigo'];
                $guiaData['motivoEncerramento'] = $guia['motivoSaidaInternacao']['tiss_codigo'];
            }

            $guiaData['dadosBeneficiario'] = array(
                'numeroCarteira'            => str_replace(" ", "", $guia['numero_carteira']),
                'atendimentoRN'             => Utils::simNao(false), // Apenas pacientes adultos
                'nomeBeneficiario'          => $guia['nome_beneficiario'],
                'identificadorBeneficiario' =>  base64_encode($guia['clientes_id'])
            );

            $guiaData['dadosExecutante'] = array(
                'CNES' => $guia['contratado']['numero_cnes'],
                'contratadoExecutante' => array(
                    'cnpjContratado'  => $guia['contratado']['cnpj'],
                    'cnpjFormatado'   => Utils::format($guia['contratado']['cnpj'], 'cnpj'),
                    'nomeContratado'  => $guia['contratado']['razao_social']
                )
            );

            if (isset($guia['faturas']) && !empty ($guia['faturas'])) {
                foreach($guia['faturas'] as $fatura) {

                    $itens = self::separarItens($fatura['itens'], $guia['planosdesaude_id']);

                    $guiaFaturaData = array(
                        'profissionaisExecutantes' => $itens->profissionaisExecutantes,
                        'dataAtendimento'         => $fatura['data_atendimento'],
                        'dadosAtendimento' => array(
                            'tipoAtendimento'   => $fatura['tiss_tipo_atendimento'],
                            'indicacaoAcidente' => $fatura['tiss_indicador_acidente']
                        ),
                        'procedimentos'                 => $itens->procedimentos,
                        'outrasDespesas'                => $itens->outrasDespesas,
                        'valorProcedimentos'            => Utils::decimal($itens->totalGeral['valorProcedimentos']),
                        'valorProcedimentosFormatado'   => Utils::money(Utils::decimal($itens->totalGeral['valorProcedimentos'])),
                        'valorDiarias'                  => Utils::decimal($itens->totalGeral['diarias']),
                        'valorDiariasFormatado'         => Utils::money(Utils::decimal($itens->totalGeral['diarias'])),
                        'valorTaxasAlugueis'            => Utils::decimal($itens->totalGeral['taxas']),
                        'valorTaxasAlugueisFormatado'   => Utils::money(Utils::decimal($itens->totalGeral['taxas'])),
                        'valorMateriais'                => Utils::decimal($itens->totalGeral['materiais']),
                        'valorMateriaisFormatado'       => Utils::money(Utils::decimal($itens->totalGeral['materiais'])),
                        'valorMedicamentos'             => Utils::decimal($itens->totalGeral['medicamentos']),
                        'valorMedicamentosFormatado'    => Utils::money(Utils::decimal($itens->totalGeral['medicamentos'])),
                        'valorOPME'                     => Utils::decimal($itens->totalGeral['opme']),
                        'valorOPMEFormatado'            => Utils::money(Utils::decimal($itens->totalGeral['opme'])),
                        'valorGasesMedicinais'          => Utils::decimal($itens->totalGeral['gases']),
                        'valorGasesMedicinaisFormatado' => Utils::money(Utils::decimal($itens->totalGeral['gases'])),
                        'valorTotalGeral'               => Utils::decimal(array_sum($itens->totalGeral)),
                        'valorTotalGeralFormatado'      => Utils::money(Utils::decimal(array_sum($itens->totalGeral)))
                    );

                    if ((isset($fatura['tiss_carater_atendimento_id']) && !empty($fatura['tiss_carater_atendimento_id']))
                        && ! isset($guiaData['caraterAtendimento']) || empty($guiaData['caraterAtendimento']))
                        $guiaFaturaData['caraterAtendimento'] = $fatura['tiss_carater_atendimento_id'];

                    $guiaData = array_merge($guiaData, $guiaFaturaData);
                }
            }
            $data['guias'][] = $guiaData;
        }
        return $data;
    }
    /**
     *
     * @param type $cabecalho
     * @param type $guiasSPSADT
     * @param type $loteId
     * @param type $nomeTabela
     * @param type $versao
     * @return \App\Models\Faturamento\XML\GuiaTiss
     * @deprecated
     */
    public static function createFromGuiasSADT($cabecalho, $guiasSPSADT, $loteId, $nomeTabela = 'CID-10', $versao = null)
    {
        if (isset($guiasSPSADT) && !empty($guiasSPSADT)) {
            $now = new \DateTime();

            $c = new Cabecalho;
            $c->setIdentificacaoTransacao(new IdentificacaoTransacao($cabecalho['tpTransacao'], $cabecalho['seqTransacao'], $now));
            $c->setOrigem(new Origem($cabecalho['origem']['cnpj']));
            $c->setDestino(new Destino($cabecalho['destino']['registroANS']));
            $c->setIdentificacaoSoftwareGerador(new IdentificacaoSoftwareGerador);

            $guiaFaturamento = new GuiaFaturamento();
            foreach ($guiasSPSADT as $data) {
                $guiaIdGuiaSADTSP = new IdentificacaoGuiaSADTSP(
                    array('registroANS' => $data['planoSaude']['registro_ans']),
                    \DateTime::createFromFormat('Y-m-d', $data['data_emissao_guia']),
                    $data['numero_guia_prestador'],
                    $data['numero_guia_operadora']
                );

                $dadosAutorizacao = new DadosAutorizacao(
                    \DateTime::createFromFormat('Y-m-d', $data['data_autorizacao']),
                    $data['senha_autorizacao'],
                    \DateTime::createFromFormat('Y-m-d', $data['validade_senha'])
                );

                $enderecoContratado = new EnderecoContratado(
                    $data['contratado']['tiss_tp_logradouro'],
                    $data['contratado']['razao_social'],
                    $data['contratado']['numero'],
                    $data['contratado']['codigo_ibge_municipio'],
                    $data['contratado']['municipio'],
                    $data['contratado']['codigo_uf'],
                    $data['contratado']['cep']
                );

                $contratado = new Contratado(
                    array('CNPJ' => $data['contratado']['cnpj']),
                    $data['contratado']['razao_social'],
                    $enderecoContratado,
                    $data['contratado']['numero_cnes']
                );

                $profissional = new Profissional(
                    $data['profissional']['nome_completo'],
                    $data['profissional']['sigla_conselho'],
                    $data['profissional']['numero_conselho'],
                    $data['profissional']['uf_conselho'],
                    $data['profissional']['tiss_cbos'],
                    $data['profissional']['cpf']
                );

                $dadosSolicitante = new DadosSolicitante;
                $dadosSolicitante->setContratado($contratado);
                $dadosSolicitante->setProfissional($profissional);

                $prestadorExecutante = new PrestadorExecutante($contratado, $profissional);

                foreach($data['faturas'] as $fatura) {
                    $dadosBeneficiario = new DadosBeneficiario(
                        str_replace(" ", "", $fatura['numero_carteira']),
                        $fatura['nome_beneficiario'],
                        $fatura['nome_plano']
                    );
                    if (isset($fatura['itens']) && !empty($fatura['itens'])) {
                        $caraterAtendimento = new CaraterAtendimento($fatura['carater_atendimento']);
                        $diagnosticoAtendimento = new DiagnosticoAtendimento(
                            new Cid($nomeTabela, $fatura['cid10']['cid10'], $fatura['cid10']['descricao']),
                            $fatura['indicadorAcidente']['tiss_codigo']
                        );

                        $outrasDespesas = new OutrasDespesas();
                        $procedimentos = array();
                        foreach ($fatura['itens'] as $item) {
                            if (0 == $item['tiss_outras_despesas_tipo_id']) { // Procedimentos & Exames Realizados
                                $procedimento = new Procedimento($item['codigo'], $item['tissTabela']['tiss_codigo'], $item['descricao']);
                                $procedimentos[] = new Procedimentos(
                                    $procedimento,
                                    \DateTime::createFromFormat('Y-m-d', $item['data']),
                                    \DateTime::createFromFormat('Y-m-d H:i:s', $item['data'] . ' 00:00:00'),
                                    \DateTime::createFromFormat('Y-m-d H:i:s', $item['data'] . ' 00:00:00'),
                                    $item['quantidade'],
                                    $item['valor_unitario']
                                );
                            } else { // Outras Despesas
                                $despesa = new Despesa;
                                $despesa->setCodigo($item['codigo']);
                                $despesa->setTipoTabela($item['tissTabela']['tiss_codigo']);
                                $despesa->setDescricao($item['descricao']);
                                $despesa->setTipoDespesa($item['tiss_outras_despesas_tipo_id']);
                                $despesa->setDataRealizacao(\DateTime::createFromFormat('Y-m-d', $item['data']));
                                $despesa->setHoraInicial(new \DateTime());
                                $despesa->setHoraFinal(new \DateTime());
                                $despesa->setQuantidade($item['quantidade']);
                                $despesa->setValorUnitario($item['valor_unitario']);
                                $outrasDespesas->add($despesa);
                            }
                        }
                        $procedimentosRealizados = new ProcedimentosRealizados($procedimentos);
                    }
                    $guiaSPSADT = new GuiaSPSADT;
                    $guiaSPSADT->setIdentificacaoGuiaSADTSP($guiaIdGuiaSADTSP);
                    $guiaSPSADT->setNumeroGuiaPrincipal($data['numero_guia_principal']);
                    $guiaSPSADT->setDadosAutorizacao($dadosAutorizacao);
                    $guiaSPSADT->setDadosBeneficiario($dadosBeneficiario);
                    $guiaSPSADT->setDadosSolicitante($dadosSolicitante);
                    $guiaSPSADT->setPrestadorExecutante($prestadorExecutante);
                    $guiaSPSADT->setCaraterAtendimento($caraterAtendimento);
                    $guiaSPSADT->setDataHoraAtendimento(\DateTime::createFromFormat('Y-m-d', $fatura['data_atendimento']));
                    $guiaSPSADT->setDiagnosticoAtendimento($diagnosticoAtendimento);
                    $guiaSPSADT->setTipoSaida($fatura['tipoSaida']['tiss_codigo']);
                    $guiaSPSADT->setTipoAtendimento($fatura['tipoAtendimento']['tiss_codigo']);
                    $guiaSPSADT->setProcedimentosRealizados($procedimentosRealizados);
                    $guiaSPSADT->setOutrasDespesas($outrasDespesas);

                    $guiaFaturamento->addGuiaSPSADT($guiaSPSADT);
                }
            }
        }

        $guia = new Guia($guiaFaturamento);

        $lg = new LoteGuias;
        $lg->addGuia($guia, $loteId);

        $po = new PrestadorParaOperadora;
        $po->setLoteGuias($lg);

        $versao = isset($versao) && !empty($versao) ? $versao : IdentificacaoSoftwareGerador::VERSAO_20203;

        $guia = new GuiaTiss($versao);
        $guia->setCabecalho($c);
        $guia->setPretadorParaOperadora($po);
        $guia->setEpilogo(md5(serialize($guiasSPSADT)));

        return $guia;
    }

}

class Cabecalho implements GuiaTissXMLElement
{

    const VERSAO_PADRAO = '2.02.03';

    private $identificacaoTransacao;
    private $origem;
    private $destino;
    private $versaoPadrao;
    private $identificacaoSoftwareGerador;

    public function setIdentificacaoTransacao(IdentificacaoTransacao $idTransacao)
    {	$this->identificacaoTransacao = $idTransacao;
    }

    public function setOrigem(Origem $origem)
    {	$this->origem = $origem;
    }

    public function setDestino(Destino $destino)
    {	$this->destino = $destino;
    }

    public function setIdentificacaoSoftwareGerador(IdentificacaoSoftwareGerador $idSoftGerador)
    {	$this->identificacaoSoftwareGerador = $idSoftGerador;
    }

    public function getVersaoPadrao()
    {	return self::VERSAO_PADRAO;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $cabecalho = $xml->addChild('cabecalho');
        $this->identificacaoTransacao->addElementTo($cabecalho);
        $this->origem->addElementTo($cabecalho);
        $this->destino->addElementTo($cabecalho);
        $cabecalho->addChild('versaoPadrao', self::VERSAO_PADRAO);
        $this->identificacaoSoftwareGerador->addElementTo($cabecalho);
        return $xml;
    }

}

class IdentificacaoTransacao implements GuiaTissXMLElement
{
    public $tipoTransacao;
    public $sequencialTransacao;
    public $dataRegistroTransacao;
    public $horaRegistroTransacao;

    public function __construct($tpTransacao,
                                $seqTransacao,
                                \DateTime $dtTransacao)
    {
        $this->tipoTransacao = $tpTransacao;
        $this->sequencialTransacao = $seqTransacao;
        $this->dataRegistroTransacao = $dtTransacao;
    }

    public function getTipoTransacao()
    {	return $this->tipoTransacao;
    }

    public function getSequencialTransacao()
    {	return $this->sequencialTransacao;
    }

    public function getDataRegistroTransacao()
    {	return $this->dataRegistroTransacao;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $idt = $xml->addChild('identificacaoTransacao');
        $idt->addChild('tipoTransacao', $this->tipoTransacao);
        $idt->addChild('sequencialTransacao', $this->sequencialTransacao);
        $idt->addChild('dataRegistroTransacao', $this->dataRegistroTransacao->format('Y-m-d'));
        $idt->addChild('horaRegistroTransacao', $this->dataRegistroTransacao->format('H:i:s'));
        return $xml;
    }

}

class Origem implements GuiaTissXMLElement
{

    public $codigoPrestadorNaOperadora;

    public function __construct($cnpj)
    {	$this->codigoPrestadorNaOperadora['CNPJ'] = $cnpj;
    }

    public function getCodigoPrestadorNaOperadora()
    {	return $this->codigoPrestadorNaOperadora;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $origem = $xml->addChild('origem');
        $cpo = $origem->addChild('codigoPrestadorNaOperadora');
        $cpo->addChild('CNPJ', $this->codigoPrestadorNaOperadora['CNPJ']);
        return $xml;
    }

}

class Destino implements GuiaTissXMLElement
{

    protected $registroANS;

    public function __construct($registroANS)
    {	$this->registroANS = $registroANS;
    }

    public function getRegistroANS()
    {	return $this->registroANS;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $destino = $xml->addChild('destino');
        $destino->addChild('registroANS', $this->registroANS);
        return $xml;
    }

}

class IdentificacaoSoftwareGerador implements GuiaTissXMLElement
{
    const VERSION_030200 = '03.02.00';
    const VERSION_030300 = '03.03.00';
    const VERSION_030301 = '03.03.01';
    const VERSION_030302 = '03.03.02';
    const VERSION_030303 = '03.03.02';
    const VERSAO_20203 = '2.02.03';
    const NOME_APLICATIVO = 'SISMEDERI_V1';
    const VERSAO_APLICATIVO = '1.x.x';
    const FABRICANTE_SOFTWARE = 'Mederi';

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $idsg = $xml->addChild('identificacaoSoftwareGerador');
        $idsg->addChild('nomeAplicativo', self::NOME_APLICATIVO);
        $idsg->addChild('versaoAplicativo', self::VERSAO_APLICATIVO);
        $idsg->addChild('fabricanteAplicativo', self::FABRICANTE_SOFTWARE);
        return $xml;
    }
}

class PrestadorParaOperadora implements GuiaTissXMLElement
{

    public $loteGuias;

    public function setLoteGuias(LoteGuias $loteGuias)
    {	$this->loteGuias = $loteGuias;
    }

    public function getLoteGuias()
    {	return $this->loteGuias;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $ppo = $xml->addChild('prestadorParaOperadora');
        $this->loteGuias->addElementTo($ppo);
        return $xml;
    }

}

class LoteGuias implements GuiaTissXMLElement
{
    protected $numeroLote = 0;
    protected $guias = array();

    public function addGuia(Guia $guia, $numeroLote)
    {
        $this->guias[] = $guia;
        $this->numeroLote = $numeroLote;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $loteGuias = $xml->addChild('loteGuias');
        $loteGuias->addChild('numeroLote', $this->numeroLote);
        $guias = $loteGuias->addChild('guias');
        foreach ($this->guias as $guia) {
            $guia->addElementTo($guias);
        }
        return $xml;
    }
}

class Guia implements GuiaTissXMLElement
{

    protected $guiaFaturamento;

    const SP_SADT = 'spsadt';
    const RESUMO_INTERNACAO = 'resumointernacao';

    public function __construct(GuiaFaturamento $guiaFatu)
    {	$this->guiaFaturamento = $guiaFatu;
    }

    public function getGuiaFaturamento()
    {	return $this->guiaFaturamento;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $this->guiaFaturamento->addElementTo($xml);
        return $xml;
    }

}

class GuiaFaturamento implements GuiaTissXMLElement
{

    private $guiasSPSADT = array();

    public function addGuiaSPSADT(GuiaSPSADT $guiaSPSADT)
    {
        $this->guiasSPSADT[] = $guiaSPSADT;
    }

    public function getGuiasSPSADT()
    { return $this->$guiasSPSADT;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $guiaFatu = $xml->addChild('guiaFaturamento');
        if (isset($this->guiasSPSADT) && !empty($this->guiasSPSADT)) {
            foreach ($this->guiasSPSADT as $guiaSPSADT) {
                $guiaSPSADT->addElementTo($guiaFatu);
            }
        }
        return $xml;
    }

}

class GuiaSPSADT implements GuiaTissXMLElement
{

    protected $identificacaoGuiaSADTSP;
    protected $numeroGuiaPrincipal;
    protected $dadosAutorizacao;
    protected $dadosBeneficiario;
    protected $dadosSolicitante;
    protected $prestadorExecutante;
    protected $caraterAtendimento;
    protected $dataHoraAtendimento;
    protected $diagnosticoAtendimento;
    protected $tipoSaida;
    protected $tipoAtendimento;
    protected $procedimentosRealizados;
    protected $outrasDespesas;
    protected $valorTotal;

    public function setIdentificacaoGuiaSADTSP(IdentificacaoGuiaSADTSP $idGuiaSADTSP)
    {	$this->identificacaoGuiaSADTSP = $idGuiaSADTSP;
    }

    public function getIdentificacaoGuiaSADTSP()
    {	return $this->identificacaoGuiaSADTSP;
    }

    public function setNumeroGuiaPrincipal($numero)
    {	$this->numeroGuiaPrincipal = str_pad($numero, 3, '0', STR_PAD_LEFT);
    }

    public function getNumeroGuiaPrincipal()
    {	return $this->numeroGuiaPrincipal;
    }

    public function setDadosAutorizacao(DadosAutorizacao $dadosAutorizacao)
    {	$this->dadosAutorizacao = $dadosAutorizacao;
    }

    public function getDadosAutorizacao()
    {	return $this->dadosAutorizacao;
    }

    public function setDadosBeneficiario(DadosBeneficiario $dadosBeneficiario)
    {	$this->dadosBeneficiario = $dadosBeneficiario;
    }

    public function getDadosBeneficiario()
    {	return $this->dadosBeneficiario;
    }

    public function setDadosSolicitante(DadosSolicitante $dados)
    {	$this->dadosSolicitante = $dados;
    }

    public function getDadosSolicitante()
    {	return $this->dadosSolicitante;
    }

    public function setPrestadorExecutante(PrestadorExecutante $prestExec)
    {	$this->prestadorExecutante = $prestExec;
    }

    public function getPrestadorExecutante()
    {	return $this->prestExec;
    }

    public function setCaraterAtendimento(CaraterAtendimento $caraterAtend)
    {	$this->caraterAtendimento = $caraterAtend;
    }

    public function getCaraterAtendimento()
    {	return $this->caraterAtendimento;
    }

    public function setDataHoraAtendimento(\DateTime $dataHora)
    {	$this->dataHoraAtendimento = $dataHora;
    }

    public function getDataHoraAtendimento()
    {	return $this->dataHoraAtendimento;
    }

    public function setDiagnosticoAtendimento(DiagnosticoAtendimento $da)
    {	$this->diagnosticoAtendimento = $da;
    }

    public function getDiagnosticoAtendimento()
    {	return $this->diagnosticoAtendimento;
    }

    public function setTipoSaida($tipoSaida)
    {	$this->tipoSaida = $tipoSaida;
    }

    public function getTipoSaida()
    {	return $this->tipoSaida;
    }

    public function setTipoAtendimento($tipoAtendimento)
    {	$this->tipoAtendimento = $tipoAtendimento;
    }

    public function getTipoAtendimento()
    {	return str_pad($this->tipoAtendimento, 2, '0', STR_PAD_LEFT);
    }

    public function setProcedimentosRealizados(ProcedimentosRealizados $pr)
    {	$this->procedimentosRealizados = $pr;
    }

    public function getProcedimentosRealizados()
    {	return $this->procedimentosRealizados;
    }

    public function setOutrasDespesas(OutrasDespesas $outrasDespesas)
    { $this->outrasDespesas = $outrasDespesas;
    }

    public function getOutrasDespesas()
    { return $this->outrasDespesas;
    }

    public function setValorTotal(ValorTotal $vt)
    {	$this->valorTotal = $vt;
    }

    public function getValorTotal()
    {
        $servExec = $diarias = $taxas = $materiais = $medicamentos = $gases  = 0.00;
        $outrasDespesas = $this->getOutrasDespesas();
        if (isset($outrasDespesas) && !empty($outrasDespesas)) {
            $despesas = $outrasDespesas->getDespesas();
            if (isset($despesas) && !empty($despesas)) {
                foreach ($despesas as $despesa) {
                    $tipo  = $despesa->getTipoDespesa();
                    $valor = $despesa->getValorTotal();

                    if (Despesa::GASES == $tipo)
                        $gases += $valor;
                    elseif (Despesa::MEDICAMENTOS == $tipo)
                        $medicamentos += $valor;
                    elseif (Despesa::MATERIAIS == $tipo)
                        $materiais += $valor;
                    elseif (Despesa::TAXAS == $tipo)
                        $taxas += $valor;
                    elseif (Despesa::DIARIAS == $tipo || Despesa::ALUGUEIS == $tipo)
                        $diarias += $valor;
                    elseif (Despesa::OPME == $tipo || Despesa::ALUGUEIS == $tipo)
                        $diarias += $valor;

                }
            }
        }

        $pr = $this->getProcedimentosRealizados();
        if (isset($pr) && !empty($pr) && $pr->getProcedimentos() > 0)
            $servExec = $pr->getTotalGeral();

        return new ValorTotal($servExec, $diarias, $taxas, $materiais, $medicamentos, $gases);
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $sadt = $xml->addChild('guiaSP_SADT');
        $this->identificacaoGuiaSADTSP->addElementTo($sadt);
        $sadt->addChild('numeroGuiaPrincipal', $this->numeroGuiaPrincipal);
        $this->dadosAutorizacao->addElementTo($sadt);
        $this->dadosBeneficiario->addElementTo($sadt);
        $this->dadosSolicitante->addElementTo($sadt);
        $this->prestadorExecutante->addElementTo($sadt);
        $this->caraterAtendimento->addElementTo($sadt);
        $sadt->addChild('dataHoraAtendimento', $this->dataHoraAtendimento->format('Y-m-d\TH:i:s'));
        $this->diagnosticoAtendimento->addElementTo($sadt);
        $sadt->addChild('tipoSaida', $this->tipoSaida);
        $sadt->addChild('tipoAtendimento', $this->getTipoAtendimento());
        $this->procedimentosRealizados->addElementTo($sadt);
        if (isset($this->outrasDespesas) && !empty($this->outrasDespesas))
            $this->outrasDespesas->addElementTo($sadt);
        $this->getValorTotal()->addElementTo($sadt);
        return $xml;
    }

}

class IdentificacaoGuiaSADTSP implements GuiaTissXMLElement
{

    private $identificacaoFontePagadora = array();
    private $dataEmissaoGuia;
    private $numeroGuiaPrestador;
    private $numeroGuiaOperadora;

    public function __construct(Array $identificacaoFontePagadora,
                                \DateTime $dataEmissaoGuia,
                                $numeroGuiaPrestador,
                                $numeroGuiaOperadora)
    {
        if (! array_key_exists('registroANS', $identificacaoFontePagadora))
            throw new Exception('IdentificacaoGuiaSADTSP Class - Parâmetro Inválido: registroANS');

        $this->identificacaoFontePagadora = $identificacaoFontePagadora;
        $this->dataEmissaoGuia = $dataEmissaoGuia;
        $this->numeroGuiaPrestador = $numeroGuiaPrestador;
        $this->numeroGuiaOperadora = $numeroGuiaOperadora;
    }

    public function getIdentificacaoFontePagadora()
    {	return $this->identificacaoFontePagadora;
    }

    public function getDataEmissaoGuia()
    {	return $this->dataEmissaoGuia;
    }

    public function getNumeroGuiaPrestador()
    {	return $this->numeroGuiaPrestador;
    }

    public function getNumeroGuiaOperadora()
    {	return $this->numeroGuiaOperadora;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $idSADTSP = $xml->addChild('identificacaoGuiaSADTSP');
        $idFP = $idSADTSP->addChild('identificacaoFontePagadora');
        foreach ($this->identificacaoFontePagadora as $registroANS) {
            $idFP->addChild('registroANS', $registroANS);
        }
        $idSADTSP->addChild('dataEmissaoGuia', $this->dataEmissaoGuia->format('Y-m-d'));
        $idSADTSP->addChild('numeroGuiaPrestador', str_pad($this->numeroGuiaPrestador, 9, '0', STR_PAD_LEFT));
        $idSADTSP->addChild('numeroGuiaOperadora', str_pad($this->numeroGuiaOperadora, 9, '0', STR_PAD_LEFT));
        return $xml;
    }

}

class DadosAutorizacao implements GuiaTissXMLElement
{

    protected $dataAutorizacao;
    protected $senhaAutorizacao;
    protected $validadeSenha;

    public function __construct(\DateTime $dtAutor, $senhaAutor, \DateTime $validadeSenha)
    {
        $this->dataAutorizacao = $dtAutor;
        $this->senhaAutorizacao = $senhaAutor;
        $this->validadeSenha = $validadeSenha;
    }

    public function getDataAutorizacao()
    {	return $this->dataAutorizacao;
    }

    public function getSenhaAutorizacao()
    {	return $this->senhaAutorizacao;
    }

    public function getValidadeSenha()
    {	return $this->validadeSenha;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $da = $xml->addChild('dadosAutorizacao');
        $da->addChild('dataAutorizacao', $this->dataAutorizacao->format('Y-m-d'));
        $da->addChild('senhaAutorizacao', $this->senhaAutorizacao);
        $da->addChild('validadeSenha', $this->validadeSenha->format('Y-m-d'));
        return $xml;
    }

}

class DadosBeneficiario implements GuiaTissXMLElement
{
    const BASICO = 'BASICO';

    private $numeroCarteira;
    private $atendimentoRN;
    private $nomeBeneficiario;
    private $nomePlano;

    public function __construct($numeroCarteira, $nomeBeneficiario, $nomePlano = null, $atendimentoRN = null)
    {
        $this->numeroCarteira   = $numeroCarteira;
        $this->atendimentoRN    = $atendimentoRN;
        $this->nomeBeneficiario = $nomeBeneficiario;
        $this->nomePlano        = $nomePlano;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $dben = $xml->addChild('dadosBeneficiario');
        $dben->addChild('numeroCarteira', $this->numeroCarteira);

        if (isset($this->atendimentoRN))
            $dben->addChild('atendimentoRN', $this->atendimentoRN ? 'S' : 'N');

        $dben->addChild('nomeBeneficiario', $this->nomeBeneficiario);

        if (isset($this->nomePlano))
            $dben->addChild('nomePlano', $this->nomePlano);

        return $xml;
    }

}

class DadosSolicitante implements GuiaTissXMLElement
{

    private $contratado;
    private $profissional;

    public function setContratado(Contratado $contratado)
    {	$this->contratado = $contratado;
    }

    public function getContratado()
    {	return $this->contratado;
    }

    public function setProfissional(Profissional $profissional)
    {	$this->profissional = $profissional;
    }

    public function getProfissional()
    {	return $this->profissional;
    }
    public function addElementTo(\SimpleXMLElement $xml)
    {
        $ds = $xml->addChild('dadosSolicitante');
        $this->contratado->addElementTo($ds);
        $this->profissional->addElementTo($ds);
        return $xml;
    }

}

class Contratado implements GuiaTissXMLElement
{
    private $identificacao;
    private $nomeContratado;
    private $enderecoContratado;
    private $numeroCNES;

    public function __construct(Array $identificacao,
                                $nomeContratado,
                                EnderecoContratado $end,
                                $numeroCNES)
    {
        $this->identificacao = $identificacao;
        $this->nomeContratado = $nomeContratado;
        $this->enderecoContratado = $end;
        $this->numeroCNES = $numeroCNES;
    }

    public function getIndentificacao()
    {	return $this->identificacao;
    }

    public function getNomeContratado()
    {	return $this->nomeContratado;
    }

    public function getEnderecoContratado()
    {	return $this->enderecoContratado;
    }

    public function getNumeroCNES()
    {	return $this->numeroCNES;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $c = $xml->addChild('contratado');
        $id = $c->addChild('identificacao');
        $id->addChild('CNPJ', $this->identificacao['CNPJ']);
        $c->addChild('nomeContratado', $this->nomeContratado);
        $this->enderecoContratado->addElementTo($c);
        $c->addChild('numeroCNES', $this->numeroCNES);
        return $xml;
    }

}

class EnderecoContratado implements GuiaTissXMLElement
{
    private $tipoLogradouro;
    private $logradouro;
    private $numero;
    private $codigoIBGEMunicipio;
    private $municipio;
    private $codigoUF;
    private $cep;

    public function __construct($tipoLogradouro,
                                $logradouro,
                                $numero,
                                $codigoIBGEMunicipio,
                                $municipio,
                                $codigoUF,
                                $cep)
    {
        $this->tipoLogradouro = $tipoLogradouro;
        $this->logradouro = $logradouro;
        $this->numero = $numero;
        $this->codigoIBGEMunicipio = $codigoIBGEMunicipio;
        $this->municipio = $municipio;
        $this->codigoUF = $codigoUF;
        $this->cep = $cep;
    }

    public function getTipoLogradouro()
    {	return $this->tipoLogradouro;
    }

    public function getLogradouro()
    {	return $this->logradouro;
    }

    public function getNumero()
    {	return $this->numero;
    }

    public function getCodigoIBGEMunicipio()
    {	return $this->codigoIBGEMunicipio;
    }

    public function getMunicipio()
    {	return $this->municipio;
    }

    public function getCodigoUF()
    {	return $this->codigoUF;
    }

    public function getCep()
    {	return $this->cep;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $end = $xml->addChild('enderecoContratado');
        $end->addChild('tipoLogradouro', $this->tipoLogradouro);
        $end->addChild('logradouro', $this->logradouro);
        $end->addChild('numero', $this->numero);
        $end->addChild('codigoIBGEMunicipio', $this->codigoIBGEMunicipio);
        $end->addChild('municipio', $this->municipio);
        $end->addChild('codigoUF', $this->codigoUF);
        $end->addChild('cep', $this->cep);
        return $xml;
    }

}

class Profissional implements GuiaTissXMLElement
{

    const SIGLA_CRM = 'CRM';
    const UF_BA = 'BA';

    private $nome;
    private $siglaConselho;
    private $numeroConselho;
    private $ufConselho;
    private $cbos;
    private $cpf;

    public function __construct($nome, $siglaConselho, $numeroConselho, $ufConselho, $cbos, $cpf = null)
    {
        $this->nome = $nome;
        $this->siglaConselho = $siglaConselho;
        $this->numeroConselho = $numeroConselho;
        $this->ufConselho = $ufConselho;
        $this->cbos = $cbos;
        $this->cpf = $cpf;
    }

    public function getNome()
    {	return $this->nome;
    }

    public function getSiglaConselho()
    {	return $this->siglaConselho;
    }

    public function getNumeroConselho()
    {	return $this->numeroConselho;
    }

    public function getUfConselho()
    {	return $this->ufConselho;
    }

    public function getCbos()
    {	return $this->cbos;
    }

    public function setCPF($cpf)
    {	$this->cpf = $cpf;
    }

    public function getCPF()
    {	return $this->cpf;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $profissional = $xml->addChild('profissional');
        $profissional->addChild('nomeProfissional', $this->nome);
        $cp = $profissional->addChild('conselhoProfissional');
        $cp->addChild('siglaConselho', $this->siglaConselho);
        $cp->addChild('numeroConselho', $this->numeroConselho);
        $cp->addChild('ufConselho', $this->ufConselho);
        $profissional->addChild('cbos', $this->cbos);
        return $xml;
    }

}

class PrestadorExecutante implements GuiaTissXMLElement
{

    private $contratado;
    private $profissional;

    public function __construct(Contratado $contratado, Profissional $profissional)
    {
        $this->contratado = $contratado;
        $this->profissional = $profissional;
    }

    public function getContratado()
    {	return $this->contratado;
    }

    public function getProfissional()
    {	return $this->profissional;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $pe = $xml->addChild('prestadorExecutante');
        $identificacao = $pe->addChild('identificacao');
        $identificacao->addChild('CNPJ', $this->contratado->getIndentificacao()['CNPJ']);
        $pe->addChild('nomeContratado', $this->contratado->getNomeContratado());
        $pec = $pe->addChild('profissionalExecutanteCompl');
        $pec->addChild('nomeExecutante', $this->profissional->getNome());
        $cp = $pec->addChild('conselhoProfissional');
        $cp->addChild('siglaConselho', $this->profissional->getSiglaConselho());
        $cp->addChild('numeroConselho', $this->profissional->getNumeroConselho());
        $cp->addChild('ufConselho', $this->profissional->getUfConselho());
        $pec->addChild('codigoCBOS', $this->profissional->getCbos());
        $cpc = $pec->addChild('codigoProfissionalCompl');
        $cpc->addChild('cpf', $this->profissional->getCPF());
        return $xml;
    }

}

class CaraterAtendimento implements GuiaTissXMLElement
{

    const ELETIVA = 'E';
    const URGENCIA = 'U';

    protected $carater;

    public function __construct($carater)
    {	$this->carater = $carater;
    }

    public function getCarater()
    {	return $this->carater;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $xml->addChild('caraterAtendimento', $this->carater);
        return $xml;
    }

}

class DiagnosticoAtendimento implements GuiaTissXMLElement
{

    protected $cid;
    protected $indicadorAcidente;

    public function __construct(Cid $cid, $indAcidente)
    {
        $this->cid = $cid;
        $this->indicadorAcidente = $indAcidente;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $da = $xml->addChild('diagnosticoAtendimento');
        $this->cid->addElementTo($da);
        $da->addChild('indicadorAcidente', $this->indicadorAcidente);
        return $xml;
    }

}

class Cid implements GuiaTissXMLElement
{

    protected $nomeTabela;
    protected $codigoDiagnostico;
    protected $descricaoDiagnostico;

    public function __construct($nomeTabela, $codDiagnostico, $descDiagnostico)
    {
        $this->nomeTabela = $nomeTabela;
        $this->codigoDiagnostico = $codDiagnostico;
        $this->descricaoDiagnostico = $descDiagnostico;
    }

    public function getNomeTabela()
    {	return $this->nomeTabela;
    }

    public function getCodigoDiagnostico()
    {	return $this->codigoDiagnostico;
    }

    public function getDescricaoDiagnostico()
    {	return $this->descricaoDiagnostico;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $cid = $xml->addChild('CID');
        $cid->addChild('nomeTabela', $this->nomeTabela);
        $cid->addChild('codigoDiagnostico', $this->codigoDiagnostico);
        $cid->addChild('descricaoDiagnostico', substr($this->descricaoDiagnostico, 0, 70));
        return $xml;
    }

}

class ProcedimentosRealizados implements GuiaTissXMLElement
{

    protected $procedimentos = array();

    public function __construct(Array $procedimentos)
    {	$this->procedimentos = $procedimentos;
    }

    public function getProcedimentos()
    {	return $this->procedimentos;
    }

    public function getTotalGeral()
    {
        $totalGeral = 0.00;
        foreach ($this->getProcedimentos() as $procedimento)
            $totalGeral += $procedimento->getValorTotal();
        return $totalGeral;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $pr = $xml->addChild('procedimentosRealizados');
        foreach ($this->getProcedimentos() as $procedimento)
            $procedimento->addElementTo($pr);
        return $xml;
    }

}

class OutrasDespesas implements GuiaTissXMLElement
{

    protected $despesas;

    public function add(Despesa $despesa)
    {
        $this->despesas[] = $despesa;
    }

    public function getDespesas()
    { return $this->despesas;
    }

    public function totalGeralOutrasDespesas()
    {
        $totalGeral = 0.00;
        foreach ($this->despesas as $despesa)
            $totalGeral += $despesa->getValorTotal();
        return $totalGeral;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        if (isset($this->despesas) && !empty($this->despesas)) {
            $outrasDespesas = $xml->addChild('outrasDespesas');
            foreach($this->despesas as $despesa)
                $despesa->addElementTo($outrasDespesas);
            $outrasDespesas->addChild('totalGeralOutrasDespesas', $this->totalGeralOutrasDespesas());
        }
        return $xml;
    }

}

class Despesa implements GuiaTissXMLElement
{

    const GASES = 1;
    const MEDICAMENTOS = 2;
    const MATERIAIS = 3;
    const TAXAS = 7;
    const DIARIAS = 5;
    const ALUGUEIS = 6;
    const OPME = 8;

    protected $codigo;
    protected $tipoTabela;
    protected $descricao;
    protected $tipoDespesa;
    protected $dataRealizacao;
    protected $horaInicial;
    protected $horaFinal;
    protected $quantidade;
    protected $valorUnitario;

    public function setCodigo($codigo)
    { $this->codigo = $codigo;
    }

    public function getCodigo()
    { return $this->codigo;
    }

    public function setDataRealizacao($dataRealizacao)
    { $this->dataRealizacao = $dataRealizacao;
    }

    public function getDataRealizacao()
    { return $this->dataRealizacao;
    }

    public function setDescricao($descricao)
    { $this->descricao = $descricao;
    }

    public function getDescricao()
    { return $this->descricao;
    }

    public function setHoraFinal($horaFinal)
    { $this->horaFinal = $horaFinal;
    }

    public function getHoraFinal()
    { return $this->horaFinal;
    }

    public function setHoraInicial($horaInicial)
    { $this->horaInicial = $horaInicial;
    }

    public function getHoraInicial()
    { return $this->horaInicial;
    }

    public function setQuantidade($quantidade)
    { $this->quantidade = (double) $quantidade;
    }

    public function getQuantidade()
    { return $this->quantidade;
    }

    public function setTipoDespesa($tipoDespesa)
    { $this->tipoDespesa = $tipoDespesa;
    }

    public function getTipoDespesa()
    { return $this->tipoDespesa;
    }

    public function setTipoTabela($tipoTabela)
    { $this->tipoTabela = $tipoTabela;
    }

    public function getTipoTabela()
    { return $this->tipoTabela;
    }

    public function getValorTotal()
    { return ($this->valorUnitario * $this->quantidade);
    }

    public function setValorUnitario($valorUnitario)
    { $this->valorUnitario = (double) $valorUnitario;
    }

    public function getValorUnitario()
    { return $this->valorUnitario;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $despesa = $xml->addChild('despesa');
        $identificador = $despesa->addChild('identificadorDespesa');
        $identificador->addChild('codigo', str_pad($this->getCodigo(), 8, '0', STR_PAD_LEFT));
        $identificador->addChild('tipoTabela', str_pad($this->getTipoTabela(), 2, '0', STR_PAD_LEFT));
        $identificador->addChild('descricao', $this->getDescricao());
        $despesa->addChild('tipoDespesa', $this->getTipoDespesa());
        $despesa->addChild('dataRealizacao', $this->getDataRealizacao()->format('Y-m-d'));
        $despesa->addChild('horaInicial', $this->getDataRealizacao()->format('H:i:s'));
        $despesa->addChild('horaFinal', $this->getDataRealizacao()->format('H:i:s'));
        $despesa->addChild('quantidade', number_format($this->getQuantidade(), 4, '.', ''));
        $despesa->addChild('valorUnitario', number_format($this->getValorUnitario(), 2, '.', ''));
        $despesa->addChild('valorTotal', number_format($this->getValorTotal(), 2, '.', ''));
        return $xml;
    }

}

class Procedimentos implements GuiaTissXMLElement
{

    protected $procedimento;
    protected $data;
    protected $horaInicio;
    protected $horaFim;
    protected $quantidadeRealizada;
    protected $valor;
    protected $valorTotal;

    public function __construct(Procedimento $p,
                                \DateTime $data,
                                $horaInicio,
                                $horaFim,
                                $quantidadeRealizada,
                                $valor)
    {
        $this->procedimento = $p;
        $this->data = $data;
        $this->horaInicio = $horaInicio;
        $this->horaFim = $horaFim;
        $this->quantidadeRealizada = $quantidadeRealizada;
        $this->valor = (double) $valor;
    }

    public function getProcedimento()
    {	return $this->procedimento;
    }

    public function getData()
    {	return $this->data;
    }

    public function getQuatidadeRealizada()
    {	return $this->quantidadeRealizada;
    }

    public function getValor()
    {	return $this->valor;
    }

    public function getValorTotal()
    {
        return ($this->valor * $this->quantidadeRealizada);
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $procedimentos = $xml->addChild('procedimentos');
        $this->procedimento->addElementTo($procedimentos);
        $procedimentos->addChild('data', $this->data->format('Y-m-d'));
        $procedimentos->addChild('horaInicio', $this->horaInicio->format('H:i:s'));
        $procedimentos->addChild('horaFim', $this->horaFim->format('H:i:s'));
        $procedimentos->addChild('quantidadeRealizada', $this->quantidadeRealizada);
        $procedimentos->addChild('valor', number_format($this->valor, 2, '.', ''));
        $procedimentos->addChild('valorTotal', number_format($this->getValorTotal(), 2, '.', ''));
        return $xml;
    }

}

class Procedimento implements GuiaTissXMLElement
{

    protected $codigo;
    protected $tipoTabela;
    protected $descricao;

    public function __construct($codigo, $tipoTabela, $descricao)
    {
        $this->codigo = $codigo;
        $this->tipoTabela = $tipoTabela;
        $this->descricao = $descricao;
    }

    public function getCodigo()
    {	return $codigo;
    }

    public function getTipoTabela()
    {	return $this->tipoTabela;
    }

    public function getDescricao()
    {	return $this->descricao;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $p = $xml->addChild('procedimento');
        $p->addChild('codigo', str_pad($this->codigo, 8, '0', STR_PAD_LEFT));
        $p->addChild('tipoTabela', str_pad($this->tipoTabela, 2, '0', STR_PAD_LEFT));
        $p->addChild('descricao', $this->descricao);
        return $xml;
    }

}

class ValorTotal implements GuiaTissXMLElement
{

    protected $servicosExecutados;
    protected $diarias;
    protected $taxas;
    protected $materiais;
    protected $medicamentos;
    protected $gases;

    public function __construct($servExec, $diarias, $taxas, $materiais, $medicamentos, $gases)
    {
        $this->servicosExecutados = (double) $servExec;
        $this->diarias = (double) $diarias;
        $this->taxas = (double) $taxas;
        $this->materiais = (double) $materiais;
        $this->medicamentos = (double) $medicamentos;
        $this->gases = (double) $gases;
    }

    public function getServicosExecutados()
    {	return $this->servicosExecutados;
    }

    public function getDiarias()
    {	return $this->diarias;
    }

    public function getTaxas()
    {	return $this->taxas;
    }

    public function getMateriais()
    {	return $this->materiais;
    }

    public function getMedicamentos()
    {	return $this->medicamentos;
    }

    public function getGases()
    {	return $this->gases;
    }

    public function getTotalGeral()
    {
        return $this->servicosExecutados +
        $this->diarias +
        $this->taxas +
        $this->materiais +
        $this->medicamentos +
        $this->gases;
    }

    public function addElementTo(\SimpleXMLElement $xml)
    {
        $vt = $xml->addChild('valorTotal');
        $vt->addChild('servicosExecutados', number_format($this->servicosExecutados, 2, '.', ''));
        $vt->addChild('diarias', number_format($this->diarias, 2, '.', ''));
        $vt->addChild('taxas', number_format($this->taxas, 2, '.', ''));
        $vt->addChild('materiais', number_format($this->materiais, 2, '.', ''));
        $vt->addChild('medicamentos', number_format($this->medicamentos, 2, '.', ''));
        $vt->addChild('gases', number_format($this->gases, 2, '.', ''));
        $vt->addChild('totalGeral', number_format($this->getTotalGeral(), 2, '.', ''));
        return $xml;
    }

}


