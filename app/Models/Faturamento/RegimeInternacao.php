<?php

namespace App\Models\Faturamento;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class RegimeInternacao extends AbstractModel implements DAOInterface
{

  public static function getAll()
  {
    return parent::get(self::getSQL());
  }

  public static function getById($id)
  {
    return current(parent::get(self::getSQL($id)));
  }

  public static function getSQL($id = null)
  {
    $id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : null;
    return "SELECT * FROM tiss_regime_internacao WHERE tiss_codigo != 0 {$id} ORDER BY descricao";
  }

} 