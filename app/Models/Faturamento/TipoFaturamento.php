<?php

namespace App\Models\Faturamento;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class TipoFaturamento extends AbstractModel implements DAOInterface
{

  public static function getAll()
  {
    return parent::get(self::getSQL());
  }

  public static function getById($id)
  {
    return current(parent::get(self::getSQL($id)));
  }

  public static function getSQL($id = null)
  {
    $id = isset($id) && !empty($id) ? 'WHERE id = "' . $id . '"' : null;
    return "SELECT * FROM tiss_tipo_faturamento {$id} ORDER BY descricao";
  }

} 