<?php

namespace App\Models\Faturamento;

use App\Models\AbstractModel;

class Cid10 extends AbstractModel
{

    public static function getById($codigo)
    {
        $result = mysql_query(self::getSQL($codigo));
        return (isset($result) && !empty($result)) ? mysql_fetch_array($result) : null;
    }

    public static function getAll()
    {
        $lista = array();
        $result = mysql_query(self::getSQL());
        while ($row = mysql_fetch_array($result)) {
            $lista[] = $row;
        }
        return $lista;
    }

    public static function getSQL($codigo = null)
    {
        $codigo = isset($codigo) && !empty($codigo) ? 'WHERE 1=1 AND cid10.codigo = "' . $codigo . '"' : null;
        return "SELECT *, UPPER (descricao) as UpperDescricao from (SELECT *, CONCAT(cid10.cid10, ' ',descricao) as nome FROM cid10 {$codigo}) as cid10 ORDER BY nome";
    }

    public static function getByTerm($term)
    {
        $sql = <<<SQL
SELECT 
  codigo AS id, 
  UPPER (CONCAT(cid10.cid10, ' ', descricao)) as value 
FROM 
  cid10 
WHERE
  CONCAT(cid10.cid10, ' ', descricao) LIKE '%{$term}%'
ORDER BY 
  value
SQL;
        return parent::get($sql, 'Assoc');
    }
}