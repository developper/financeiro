<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 03/03/2016
 * Time: 10:24
 */

namespace App\Models\Faturamento;

use App\Models\AbstractModel;

class Operadora extends AbstractModel
{
    public static function getAll()
    {
        $sql = <<<SQL
SELECT * FROM operadoras ORDER BY NOME
SQL;
        return parent::get($sql, 'Assoc');
    }

    public static function getByPlanoID($id = null)
    {
        $result = mysql_query(self::getSQLByPlanoID($id));
        return (isset($result) && !empty($result)) ? $operadora = mysql_fetch_array($result) : null;
    }

    public static function getSQLByPlanoID($id = null, $criteriaTiss = true)
    {
        $compPlanoId = !empty($id) ? " AND operadoras_planosdesaude.PLANOSDESAUDE_ID = {$id} " : "";
        return "SELECT
operadoras_planosdesaude.OPERADORAS_ID
FROM
operadoras_planosdesaude
WHERE
1 = 1
{$compPlanoId}";
    }

    public static function getCodigoNaOperadora($idEmpresa,$idOperadora)
    {
       $sql= "SELECT
codigo_empresa_operadora.codigo
FROM
codigo_empresa_operadora
WHERE
codigo_empresa_operadora.empresas_id  = '{$idEmpresa}'
AND
codigo_empresa_operadora.operadoras_id= '{$idOperadora}'";
        $result = mysql_query($sql);
        return (isset($result) && !empty($result)) ? $codigoNaOperadora = mysql_fetch_array($result) : null;

    }

}