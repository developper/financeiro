<?php

namespace App\Models\Faturamento;

class Contratado
{

  public static function getAll()
  {
    $contratados = array();
    $result = mysql_query(self::getSQL());
    while($contratado = mysql_fetch_array($result))
      if (isset($contratado) && !empty($contratado))
        $contratados[] = $contratado;

    return $contratados;
  }

  public static function getById($id)
  {
    $result = mysql_query(self::getSQL($id));
    return (isset($result) && !empty($result)) ? mysql_fetch_array($result) : null;
  }

  private static function getSQL($id = null)
  {
    $id = isset($id) && !empty($id) ? 'WHERE 1=1 AND tiss_contratado.id = ' . $id : null;
    return "SELECT tiss_contratado.*, tipo_logradouro.tiss_tp_logradouro
            FROM tiss_contratado
              INNER JOIN tipo_logradouro ON tipo_logradouro.id = tiss_contratado.tipo_logradouro_id
            {$id}
            ORDER BY razao_social ASC";
  }

} 