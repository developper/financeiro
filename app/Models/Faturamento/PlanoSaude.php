<?php

namespace App\Models\Faturamento;

class PlanoSaude
{

  public static function getAll($criteriaTiss = true)
  {
    $planos = array();
    $result = mysql_query(self::getSQL(null, $criteriaTiss));
    while($plano = mysql_fetch_array($result))
      if (isset($plano) && !empty($plano))
        $planos[] = $plano;

    return $planos;
  }

  public static function getById($id)
  {
    $result = mysql_query(self::getSQL($id));
    return (isset($result) && !empty($result)) ? $plano = mysql_fetch_array($result) : null;
  }

  public static function getSQL($id = null, $criteriaTiss = true)
  {
    $criteriaTiss = $criteriaTiss ? 'AND registro_ans IS NOT NULL' : null;
    $id = isset($id) && !empty($id) ? 'AND planosdesaude.id = ' . $id : null;
    return "SELECT *
            FROM planosdesaude
            WHERE planosdesaude.ATIVO = 'S'
            {$criteriaTiss}
            {$id}
            ORDER BY nome ASC";
  }

} 