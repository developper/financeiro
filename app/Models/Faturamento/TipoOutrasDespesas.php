<?php

namespace App\Models\Faturamento;

use \App\Models\AbstractModel,
    \App\Models\DAOInterface;

class TipoOutrasDespesas extends AbstractModel implements DAOInterface
{

  public static function getAll()
  {
    return parent::get(self::getSQL());
  }

  public static function getAllComCodigoInterno()
  {
    return parent::get('
      SELECT *
      FROM tiss_outras_despesas_tipo_interno
        INNER JOIN tiss_outras_despesas_tipo ON tiss_outras_despesas_tipo.id = tiss_outras_despesas_tipo_interno.tiss_outras_despesas_tipo_id
      WHERE tiss_outras_despesas_tipo.is_active = 1
    ');
  }

  public static function getSQL($id = null)
  {
    $id = isset($id) && !empty($id) ? 'AND id = "' . $id . '"' : null;
    return "SELECT * FROM tiss_outras_despesas_tipo WHERE is_active = 1 {$id} ORDER BY descricao";
  }

} 