<?php

namespace App\Models\Faturamento;

use App\Models\AbstractModel;

class Fatura extends AbstractModel
{
	public static function calculaDescontoAcrescimo($valorItem, $descontoAcrescimo, $quantidade = 1)
	{
		return ($valorItem * $quantidade) * ($descontoAcrescimo / 100);
	}

	public static function getByCapaLote($capaLote, $asMySqlResource = false)
	{
		$faturas = array();
		$sql = "SELECT ID,CAPMEDICA_ID,clientes.idClientes,clientes.nome as nomePaciente,clientes.NOME_PLANO_CONVENIO,
                clientes.NUM_MATRICULA_CONVENIO, clientes.diagnostico, fatura.DATA_INICIO,fatura.ORCAMENTO
            FROM fatura
                LEFT JOIN clientes ON clientes.idClientes = fatura.PACIENTE_ID
             WHERE CAPA_LOTE = '{$capaLote}'";
		$result = mysql_query($sql);
		if ($asMySqlResource)
			return $result;
		while ($fatura = mysql_fetch_array($result))
			if (isset($fatura) && !empty($fatura))
				$faturas[] = $fatura;
		return $faturas;
	}

    public static function getByPrescricaoIdAndTabela($id, $tabela)
    {
        $sql = "Select
fatura.*,
usuarios.nome as nomeUser,
date_format(fatura.created_at, '%d/%m/%Y') as dataBr
 from
 fatura
 INNER JOIN usuarios ON fatura.created_by = usuarios.idUsuarios
 where prescricao_id = {$id} and tag_prescricao = '{$tabela}'";
        return parent::get($sql);

    }

	public static function getItensByFaturaId($faturaId, $condicao)
	{
		$faturas = array();
		$sql = self::getSQL($faturaId, $condicao);
		$result = mysql_query($sql);
		if (isset($result) && !empty($result))
			while ($fatura = mysql_fetch_array($result))
				$faturas[] = $fatura;
		return $faturas;
	}

	public static function getSQL($faturaId, $condicao = null)
	{
		return "SELECT
              f.*,
              fa.OBS,
              IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),B.DESC_MATERIAIS_FATURAR) AS principio,
              (CASE f.TIPO
              WHEN '0' THEN 'Medicamento'
              WHEN '1' THEN 'Material'
               WHEN '3' THEN 'Dieta' END) as apresentacao,
               (CASE f.TIPO
              WHEN '0' THEN '3'
              WHEN '1' THEN '4'
               WHEN '3' THEN '5' END) as ordem,
              1 as cod,
              uf.UNIDADE,
              uf.ID as UNIDADE_ID,
              fa.PACIENTE_ID,
              fa.PLANO_ID,
              B.REFERENCIA as ref,
              B.lab_desc as fabricante,
              B.ANTIBIOTICO as atb,
              fa.DATA_INICIO,
              f.UNIDADE as UNIDADE_FATURA
		        FROM
              faturamento as f  INNER JOIN
              `catalogo` as B ON (f.CATALOGO_ID = B.ID) inner join
              fatura as fa on (f.FATURA_ID=fa.ID) left join
               unidades_fatura as uf on(f.UNIDADE=uf.ID)
		        WHERE
              f.FATURA_ID= {$faturaId} and
              f.TIPO IN (0,1,3) {$condicao}
              and f.CANCELADO_POR is NULL
							
          UNION
            SELECT
              f.*,
              fa.OBS,
              vc.DESC_COBRANCA_PLANO AS principio,
              ('Servi&ccedil;os') AS apresentacao,
              1 as ordem,
              vc.COD_COBRANCA_PLANO as cod,
              uf.UNIDADE,
              uf.ID as UNIDADE_ID,
              fa.PACIENTE_ID,
              fa.PLANO_ID,
              'X' as ref,
              '' as fabricante,
              '' as atb,
              fa.DATA_INICIO,
              f.UNIDADE as UNIDADE_FATURA
		        FROM
              faturamento as f INNER JOIN
              valorescobranca as vc on (f.CATALOGO_ID =vc.id) inner join
              fatura fa on (f.FATURA_ID=fa.ID) left join
              unidades_fatura as uf on (f.UNIDADE=uf.ID)
            WHERE
              f.FATURA_ID= {$faturaId} and
              f.TIPO =2 and
              f.TABELA_ORIGEM='valorescobranca'	{$condicao}
              and f.CANCELADO_POR is NULL
           UNION
              SELECT
                f.*,
                fa.OBS,
                vc.item AS principio,
                ('Servi&ccedil;os') AS apresentacao,
                1 as ordem,
                '' as cod,
                uf.UNIDADE,
                uf.ID as UNIDADE_ID,
                fa.PACIENTE_ID,
                fa.PLANO_ID,
                'X' as ref,
                '' as fabricante,
                '' as atb,
                fa.DATA_INICIO,
              f.UNIDADE as UNIDADE_FATURA
              FROM
                faturamento as f INNER JOIN
                cobrancaplanos as vc on (f.CATALOGO_ID =vc.id) inner join
                fatura fa on (f.FATURA_ID=fa.ID) left join
                unidades_fatura as uf on (f.UNIDADE=uf.ID)
              WHERE
                f.FATURA_ID= {$faturaId} and
                f.TIPO =2 and
                f.TABELA_ORIGEM='cobrancaplano'	{$condicao}
                and f.CANCELADO_POR is NULL
			
            UNION
			        SELECT DISTINCT
                f.*,
                 fa.OBS,
                CO.item AS principio,

                ('Equipamentos') AS apresentacao,
                    2 as ordem,
                 '' as cod,
                 uf.UNIDADE,
                uf.ID as UNIDADE_ID,
                fa.PACIENTE_ID,
                fa.PLANO_ID,
                'X' as ref,
                '' as fabricante,
                '' as atb,
                fa.DATA_INICIO,
              f.UNIDADE as UNIDADE_FATURA
			        FROM
                faturamento as f INNER JOIN
                cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)
                inner join fatura fa on (f.FATURA_ID=fa.ID) left join
                unidades_fatura as uf on (f.UNIDADE=uf.ID)
		          WHERE
                f.FATURA_ID= {$faturaId} and
                f.TIPO =5  and
                f.TABELA_ORIGEM='cobrancaplano' {$condicao}
                and f.CANCELADO_POR is NULL

            UNION
			        SELECT DISTINCT
                f.*,
                fa.OBS,
                CO.DESC_COBRANCA_PLANO AS principio,
                ('Equipamentos') AS apresentacao,
                2 as ordem,
                CO.COD_COBRANCA_PLANO as cod,
                uf.UNIDADE,
                uf.ID as UNIDADE_ID,
                fa.PACIENTE_ID,
                fa.PLANO_ID,
                'X' as ref,
                '' as fabricante,
                '' as atb,
                fa.DATA_INICIO,
              f.UNIDADE as UNIDADE_FATURA
              FROM
                faturamento as f INNER JOIN
                valorescobranca CO ON (f.CATALOGO_ID = CO.`id`)
                inner join fatura fa on (f.FATURA_ID=fa.ID) left join
                unidades_fatura as uf on (f.UNIDADE=uf.ID)
              WHERE
                f.FATURA_ID= {$faturaId} and
                f.TIPO =5  and
                f.TABELA_ORIGEM='valorescobranca' {$condicao}
                and f.CANCELADO_POR is NULL
          	UNION
			        SELECT DISTINCT
                f.*,fa.OBS,
                CO.item AS principio,
                ('Gasoterapia') AS apresentacao,
                6 as ordem,
                '' as cod,
                uf.UNIDADE,
                uf.ID as UNIDADE_ID,
                fa.PACIENTE_ID,
                fa.PLANO_ID,
                'X' as ref,
                '' as fabricante,
                '' as atb,
                fa.DATA_INICIO,
              f.UNIDADE as UNIDADE_FATURA
				      FROM faturamento as f
                INNER JOIN cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)
                INNER JOIN fatura fa on (f.FATURA_ID=fa.ID)
                LEFT JOIN unidades_fatura as uf on (f.UNIDADE=uf.ID)
				      WHERE
					     f.FATURA_ID= {$faturaId} and
					     f.TIPO = 6 and f.TABELA_ORIGEM ='cobrancaplano' {$condicao}
               and f.CANCELADO_POR is NULL
            UNION
              SELECT DISTINCT
                f.*,
                fa.OBS,
					      vc.DESC_COBRANCA_PLANO AS principio,
                ('Gasoterapia') AS apresentacao,
                6 as ordem,
                vc.COD_COBRANCA_PLANO as cod,
                uf.UNIDADE,
                uf.ID as UNIDADE_ID,
                fa.PACIENTE_ID,
                fa.PLANO_ID,
                'X' as ref,
                '' as fabricante,
                '' as atb,
                fa.DATA_INICIO,
              f.UNIDADE as UNIDADE_FATURA
				      FROM
                faturamento as f  inner join
                valorescobranca as vc on (vc.id = f.CATALOGO_ID) inner join
                fatura fa on (f.FATURA_ID=fa.ID) left join
                unidades_fatura as uf on(f.UNIDADE=uf.ID)
              WHERE
                f.FATURA_ID= {$faturaId} and
                f.TIPO =6  and
                f.TABELA_ORIGEM='valorescobranca' {$condicao}
                and f.CANCELADO_POR is NULL
              ORDER BY
                ordem,
                tipo,
                principio";
	}

    public static function getIssByFatura($fatura)
    {
        $sql =  "select imposto_iss from fatura where id = {$fatura}";

        return current(parent::get($sql, 'assoc'));
	}

    public static function getValoresCobrancaById($id)
    {
        $sql= <<<SQL
SELECT
valorescobranca.id,
valorescobranca.idPlano,
valorescobranca.idCobranca,
valorescobranca.valor,
valorescobranca.COD_COBRANCA_PLANO,
valorescobranca.DESC_COBRANCA_PLANO,
valorescobranca.CUSTO_COBRANCA_PLANO,
valorescobranca.excluido_por,
valorescobranca.excluido_em
FROM
`valorescobranca`
WHERE
valorescobranca.id = {$id}

SQL;
        return parent::get ($sql);
    }

    public static function getCustoServicoEquipamentoByUR($idCobranca,$empresa){
        $sql= <<<SQL
SELECT
custo_servicos_plano_ur.ID,
custo_servicos_plano_ur.PLANO_ID,
custo_servicos_plano_ur.UR,
custo_servicos_plano_ur.COBRANCAPLANO_ID,
custo_servicos_plano_ur.`DATA`,
custo_servicos_plano_ur.USUARIO_ID,
custo_servicos_plano_ur.CUSTO,
custo_servicos_plano_ur.`STATUS`
FROM
`custo_servicos_plano_ur`
WHERE
custo_servicos_plano_ur.UR = {$empresa} AND
custo_servicos_plano_ur.COBRANCAPLANO_ID = $idCobranca
SQL;
        return parent::get ($sql);

    }

    public static function getHistoricoCustoServicoEquipamentoByURAndInicio($idCobranca,$empresa,$inicio)
    {
        $sql = <<<SQL
        SELECT
historico_custo_servico_plano_ur.custo
FROM
historico_custo_servico_plano_ur
WHERE
historico_custo_servico_plano_ur.cobrancaplano_id = $idCobranca AND
historico_custo_servico_plano_ur.empresa_id = $empresa AND
DATE_FORMAT(historico_custo_servico_plano_ur.inicio, '%Y-%m-%d') <= '$inicio'
order by
historico_custo_servico_plano_ur.id
LIMIT 1


SQL;
        return parent::get ($sql);



    }

    public static function getCustoCatalogo($idCatalogo)
    {
        $sql = <<<SQL
        SELECT
        valorMedio,
        custo_medio_atualizado_em
FROM
catalogo
WHERE
id = $idCatalogo
SQL;
        return parent::get ($sql);

    }

    public static function getHistoricoCustoEntradaMaterialMedicamentoDieta($idCatalogo,$inicio)
    {
        $sql = <<<SQL
        SELECT
valor
FROM
entrada
WHERE
tipo in (0,1,3)
AND
data <= '$inicio'
AND
CATALOGO_ID = $idCatalogo
order by
id desc
LIMIT 1
SQL;
        return parent::get ($sql);



    }

    public static function getFaturasByCapaLote($lote)
    {
        $sql = <<<SQL
        Select
          fatura.*,
          clientes.nome
         from
         fatura
         inner join clientes on clientes.idClientes = fatura.PACIENTE_ID
         where
          CAPA_LOTE = '{$lote}'

SQL;
        return parent::get ($sql);

    }

    public static function getItensMatDieMedPreFatura()
    {
      $sql=  <<<SQL
        SELECT
                            IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),
                            B.DESC_MATERIAIS_FATURAR) AS principio,
                            S.NUMERO_TISS AS cod,
                            S.CATALOGO_ID as catalogo_id,
                            'catalogo' AS TABELA_ORIGEM,
                            C.nome,
                            S.tipo,
                            'P' as flag,
                            S.idSaida,
                            C.idClientes AS Paciente,
                             $cond1 AS Plano,
                           coalesce(B.valorMedio,'0.00')  AS custo,
                            (CASE S.tipo
                            WHEN '0' THEN 'Medicamento'
                            WHEN '1' THEN 'Material'
                            WHEN '3' THEN 'Dieta' END) as apresentacao,
                            (CASE WHEN COUNT(*)>1 THEN SUM(S.quantidade) ELSE S.quantidade END) AS quantidade,
                            coalesce(B.VALOR_VENDA,'0.00') AS valor,
                            PL.nome as nomeplano,
                            C.empresa,
                            B.TUSS as codTuss,
                            PL.tipo_medicamento
                            FROM
                            `saida` S INNER JOIN
                            `clientes` C ON (S.idCliente = C.idClientes)  INNER JOIN
                            `catalogo` B ON (S.CATALOGO_ID = B.ID) INNER JOIN
                            planosdesaude as PL on ($cond1=PL.id) INNER JOIN
							solicitacoes as Sol on (S.idSolicitacao = Sol.idSolicitacoes) INNER JOIN
                            prescricoes as P on (Sol.idPrescricao = P.id)
                            WHERE

                                              C.idClientes = '{$cod}' AND
                                              
                                             ( P.inicio BETWEEN '{$inicio}' AND '{$fim}'
                                                OR
                                              P.fim BETWEEN '{$inicio}' AND '{$fim}'
                                              )
                                        S.quantidade>0 {$refaturarmed} and (C.idClientes = S.idCliente) and
                                        S.tipo IN (0,1,3)
                                        GROUP BY
                                              Paciente,catalogo_id

                                              select

 principal.principio,
                            principal.cod,
                            principal.catalogo_id,
                            principal.TABELA_ORIGEM,
                            principal.nome,
                            principal.tipo,
                            principal.flag,
                            principal.idSaida,
                            principal.Paciente,
                            principal.Plano,
                           principal.custo,
                            principal.apresentacao,
                            sum(principal.quantidade) as quantidade,
                            principal.valor,
                            principal.nomeplano,
                            principal.empresa,
                            principal.codTuss,
                            principal.tipo_medicamento


from (SELECT
                            IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),
                            B.DESC_MATERIAIS_FATURAR) AS principio,
                            S.NUMERO_TISS AS cod,
                            S.CATALOGO_ID as catalogo_id,
                            'catalogo' AS TABELA_ORIGEM,
                            C.nome,
                            S.tipo,
                            'P' as flag,
                            S.idSaida,
                            C.idClientes AS Paciente,
                             $cond1 AS Plano,
                           coalesce(B.valorMedio,'0.00')  AS custo,
                            (CASE S.tipo
                            WHEN '0' THEN 'Medicamento'
                            WHEN '1' THEN 'Material'
                            WHEN '3' THEN 'Dieta' END) as apresentacao,
                            --(CASE WHEN COUNT(*)>1 THEN SUM(S.quantidade) ELSE S.quantidade END) AS quantidade,
                            coalesce(B.VALOR_VENDA,'0.00') AS valor,
                            PL.nome as nomeplano,
                            C.empresa,
                            B.TUSS as codTuss,
                            PL.tipo_medicamento
                            FROM
                `saida` S INNER JOIN
                `clientes` C ON (S.idCliente = C.idClientes)  INNER JOIN
                `catalogo` B ON (S.CATALOGO_ID = B.ID) INNER JOIN
                planosdesaude as PL on (C.convenio=PL.id) INNER JOIN
							solicitacoes as Sol on (S.idSolicitacao = Sol.idSolicitacoes)
                WHERE

                                              C.idClientes = '{$cod}' AND
                                              S.data BETWEEN '{$inicio}' AND '{$fim}' AND
                                              S.quantidade>0 {$refaturarmed} and (C.idClientes = S.idCliente) and

                                              S.tipo IN (0,1,3) and Sol.idPrescricao = -1 AND
																								Sol.TIPO_SOLICITACAO = -1
                                        GROUP BY
                                              Paciente,catalogo_id
UNION
SELECT
IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),
                            B.DESC_MATERIAIS_FATURAR) AS principio,
                            S.NUMERO_TISS AS cod,
                            S.CATALOGO_ID as catalogo_id,
                            'catalogo' AS TABELA_ORIGEM,
                            C.nome,
                            S.tipo,
                            'P' as flag,
                            S.idSaida,
                            C.idClientes AS Paciente,
                             $cond1 AS Plano,
                           coalesce(B.valorMedio,'0.00')  AS custo,
                            (CASE S.tipo
                            WHEN '0' THEN 'Medicamento'
                            WHEN '1' THEN 'Material'
                            WHEN '3' THEN 'Dieta' END) as apresentacao,
                            --(CASE WHEN COUNT(*)>1 THEN SUM(S.quantidade) ELSE S.quantidade END) AS quantidade,
                            coalesce(B.VALOR_VENDA,'0.00') AS valor,
                            PL.nome as nomeplano,
                            C.empresa,
                            B.TUSS as codTuss,
                            PL.tipo_medicamento
                            FROM
                            `saida` S INNER JOIN
                            `clientes` C ON (S.idCliente = C.idClientes)  INNER JOIN
                            `catalogo` B ON (S.CATALOGO_ID = B.ID) INNER JOIN
                            planosdesaude as PL on (C.convenio=PL.id) INNER JOIN
							solicitacoes as Sol on (S.idSolicitacao = Sol.idSolicitacoes) INNER JOIN
                            prescricoes as P on (Sol.idPrescricao = P.id)
                            WHERE

                                              C.idClientes = '{$cod}' AND

                                             ( P.inicio BETWEEN '{$inicio}' AND '{$fim}'
                                                OR
                                              P.fim BETWEEN '{$inicio}' AND '{$fim}'
                                              )
AND
                                        S.quantidade>0 {$refaturarmed} and (C.idClientes = S.idCliente) and
                                        S.tipo IN (0,1,3)
                                        GROUP BY
                                              Paciente,catalogo_id) as principal
GROUP BY
                                              Paciente,catalogo_id

ORDER BY catalogo_id
SQL;
    }


    public static function getOrcamentosNaoAutorizados($filters)
    {
        $compPaciente = $filters['paciente'] != '' ? " AND PACIENTE_ID = '{$filters['paciente']}' " : "";
        $compUr = $filters['ur'] != '' ? " AND UR = '{$filters['ur']}' " : "";
        $compTipo = $filters['tipo'] != '' ? " AND fatura.ORCAMENTO = '{$filters['tipo']}' " : " AND fatura.ORCAMENTO != 0 ";
        $filters['status'] = $filters['status'] === '0' ? '0 OR autorizacao.status_geral IS NULL' : $filters['status'];
        $compStatus = $filters['status'] != '' ? " AND (autorizacao.status_geral = {$filters['status']}) " : "";

        $sql = <<<SQL
SELECT
  UPPER(clientes.nome) AS paciente,
  fatura.ID as item_id,
  autorizacao.id AS autorizacao_id,
  autorizacao.status_geral,
  COALESCE(
    CASE autorizacao.status_geral
      WHEN '1' THEN 'Autorizado'
      WHEN '2' THEN 'Autorizado Parcial'
      WHEN '3' THEN 'Negado'
    END, 'Autorização Pendente' 
  ) as label_status_geral,
  (
    CASE fatura.ORCAMENTO 
        WHEN '1' THEN 'Or&ccedil;amento de Avalia&ccedil;&atilde;o'
        WHEN '2' THEN 'Or&ccedil;amento de Prorroga&ccedil;&atilde;o'
        WHEN '3' THEN 'Or&ccedil;amento Aditivo'
    END
  ) AS tipo_item,
  'orcamento' AS tipo_primario,
  (
    CASE fatura.ORCAMENTO 
        WHEN '1' THEN 'avaliacao'
        WHEN '2' THEN 'prorrogacao'
        WHEN '3' THEN 'aditivo'
    END
  ) AS carater,
  DATA_INICIO AS inicio,
  DATA_FIM AS fim,
  prescricao_id,
    tag_prescricao,
    fatura.PLANO_ID,
    DATA_INICIO ,
  DATA_FIM 
    
FROM
  fatura
  INNER JOIN clientes ON fatura.PACIENTE_ID = clientes.idClientes
  LEFT JOIN autorizacao ON fatura.ID = autorizacao.origem 
WHERE
  CANCELADO_POR IS NULL
  AND CANCELADO_EM IS NULL
  AND (
    DATA_INICIO BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
    OR DATA_FIM BETWEEN '{$filters['inicio']}' AND '{$filters['fim']}'
  )
  {$compPaciente}
  {$compTipo}
  {$compStatus}
  {$compUr}
GROUP BY 
  fatura.ID
ORDER BY 
  fatura.DATA DESC 
SQL;
        return parent::get($sql);

    }

    public static function getItensOrcamentoAutorizacao($orcamento)
    {
        $sql = <<<SQL
SELECT DISTINCT
    f.QUANTIDADE,
    f.CATALOGO_ID,
    f.TABELA_ORIGEM,
    f.TIPO AS tipo_item,
    autorizacao_itens.*,
    f.ID AS fatId,
    IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),B.DESC_MATERIAIS_FATURAR) AS principio,
    (CASE f.TIPO
    WHEN '0' THEN 'Medicamento'
    WHEN '1' THEN 'Material'
    WHEN '3' THEN 'Dieta' END) as apresentacao,
    if(f.TIPO = 1,B.categoria_material_id, NULL) as categoria
    
FROM
    faturamento as f  INNER JOIN
    `catalogo` B ON (f.CATALOGO_ID = B.ID)
    inner join fatura fa on (f.FATURA_ID=fa.ID)
    LEFT JOIN autorizacao_itens ON (
      autorizacao_itens.origem_item_id = f.ID
    )
    
WHERE
    f.FATURA_ID = {$orcamento} and
    f.TIPO IN (0,1,3) 
    and f.CANCELADO_POR is NULL
GROUP BY
  CONCAT(f.CATALOGO_ID, '-', f.TIPO)
UNION		
    SELECT DISTINCT
        f.QUANTIDADE,
        f.CATALOGO_ID,
        f.TABELA_ORIGEM,
        f.TIPO AS tipo_item,
        autorizacao_itens.*,
        f.ID AS fatId,
        vc.DESC_COBRANCA_PLANO AS principio,                           
        ('Servi&ccedil;os') AS apresentacao,
        NULL as categoria
    FROM 
        faturamento as f INNER JOIN				      
        valorescobranca as vc on (f.CATALOGO_ID =vc.id) 
        inner join fatura fa on (f.FATURA_ID=fa.ID)
        LEFT JOIN autorizacao_itens ON (
          autorizacao_itens.origem_item_id = f.ID
        )
    WHERE 
    f.FATURA_ID= {$orcamento} and 
    f.TIPO =2 and 
    f.TABELA_ORIGEM='valorescobranca'	
    and f.CANCELADO_POR is NULL
    GROUP BY
      CONCAT(f.CATALOGO_ID, '-', f.TIPO)
UNION
           
        SELECT DISTINCT
            f.QUANTIDADE,
            f.CATALOGO_ID,
            f.TABELA_ORIGEM,
            f.TIPO AS tipo_item,
            autorizacao_itens.*,
            f.ID AS fatId,
            vc.item AS principio,
            ('Servi&ccedil;os') AS apresentacao,
            NULL as categoria
        FROM 
            faturamento as f INNER JOIN
            cobrancaplanos as vc on (f.CATALOGO_ID =vc.id) 
            inner join fatura fa on (f.FATURA_ID=fa.ID)
            LEFT JOIN autorizacao_itens ON (
              autorizacao_itens.origem_item_id = f.ID
            )
        WHERE 
            f.FATURA_ID= {$orcamento} and 
            f.TIPO =2 and 
            f.TABELA_ORIGEM='cobrancaplano' 
            and f.CANCELADO_POR is NULL
        GROUP BY
          CONCAT(f.CATALOGO_ID, '-', f.TIPO)
UNION

          SELECT DISTINCT
            f.QUANTIDADE,
            f.CATALOGO_ID,
            f.TABELA_ORIGEM,
            f.TIPO AS tipo_item,
            autorizacao_itens.*,
            f.ID AS fatId,
            CO.item AS principio,
            ('Equipamentos') AS apresentacao,
            NULL as categoria
          FROM 
            faturamento as f INNER JOIN
            cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)  
            inner join fatura fa on (f.FATURA_ID=fa.ID) 
            LEFT JOIN autorizacao_itens ON (
              autorizacao_itens.origem_item_id = f.ID
            )
          WHERE 
            f.FATURA_ID= {$orcamento} and
            f.TIPO =5  and  
            f.TABELA_ORIGEM='cobrancaplano' 
            and f.CANCELADO_POR is NULL
            GROUP BY
              CONCAT(f.CATALOGO_ID, '-', f.TIPO)
UNION

            SELECT DISTINCT
                f.QUANTIDADE,
                f.CATALOGO_ID,
                f.TABELA_ORIGEM,
                f.TIPO AS tipo_item,
                autorizacao_itens.*,
                f.ID AS fatId,
                CO.DESC_COBRANCA_PLANO AS principio,
                ('Equipamentos') AS apresentacao,
                NULL as categoria
            FROM 
                faturamento as f INNER JOIN
                valorescobranca CO ON (f.CATALOGO_ID = CO.`id`)   
                inner join fatura fa on (f.FATURA_ID=fa.ID)
                LEFT JOIN autorizacao_itens ON (
                  autorizacao_itens.origem_item_id = f.ID
                )
            WHERE 
                f.FATURA_ID= {$orcamento} and
                f.TIPO =5  and  
                f.TABELA_ORIGEM='valorescobranca' 
                and f.CANCELADO_POR is NULL
                GROUP BY
                  CONCAT(f.CATALOGO_ID, '-', f.TIPO)

UNION
                SELECT DISTINCT
                    f.QUANTIDADE,
                    f.CATALOGO_ID,
                    f.TABELA_ORIGEM,
                    f.TIPO AS tipo_item,
                    autorizacao_itens.*,
                    f.ID AS fatId,
                    CO.item AS principio,
                    ('Gasoterapia') AS apresentacao,
                    NULL as categoria
                FROM 
                    faturamento as f INNER JOIN
                    cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)
                    inner join fatura fa on (f.FATURA_ID=fa.ID) 
                    LEFT JOIN autorizacao_itens ON (
                      autorizacao_itens.origem_item_id = f.ID
                    )
                WHERE 
                    f.FATURA_ID= {$orcamento} and
                    f.TIPO =6  and f.TABELA_ORIGEM='cobrancaplano' 
                    and f.CANCELADO_POR is NULL
                GROUP BY
                    CONCAT(f.CATALOGO_ID, '-', f.TIPO)
                          
UNION
                    SELECT DISTINCT
                        f.QUANTIDADE,
                        f.CATALOGO_ID,
                        f.TABELA_ORIGEM,
                        f.TIPO AS tipo_item,
                        autorizacao_itens.*,
                        f.ID AS fatId,
                        vc.DESC_COBRANCA_PLANO AS principio,
                        ('Gasoterapia') AS apresentacao,
                        NULL as categoria
                    FROM 
                        faturamento as f  inner join 
                        valorescobranca as vc on (vc.id = f.CATALOGO_ID) 
                        inner join fatura fa on (f.FATURA_ID=fa.ID)
                        LEFT JOIN autorizacao_itens ON (
                          autorizacao_itens.origem_item_id = f.ID
                        )
                    WHERE 
                        f.FATURA_ID= {$orcamento} and
                        f.TIPO =6  and 
                        f.TABELA_ORIGEM='valorescobranca' 	
                        and f.CANCELADO_POR is NULL
                    GROUP BY
                        CONCAT(f.CATALOGO_ID, '-', f.TIPO)
ORDER BY
tipo_item,
principio
SQL;
        return parent::get($sql, 'assoc');

    }


    public static function getItensPreFaturaByPaciente($cod, $cond1, $inicio, $fim, $refaturarmed, $refaturarserv, $refaturareqp )
    {
        return $sql = "
SELECT
  principal.principio,
  principal.cod,
  principal.catalogo_id,
  principal.TABELA_ORIGEM,
  principal.nome,
  principal.tipo,
  principal.flag,
  principal.idSaida,
  principal.Paciente,
  principal.Plano,
  principal.custo,
  principal.apresentacao,
  SUM(principal.quantidade) AS quantidade,
  principal.valor,
  principal.nomeplano,
  principal.empresa,
  principal.codTuss,
  principal.tipo_medicamento
FROM
  (
    SELECT
      IF(B.DESC_MATERIAIS_FATURAR IS NULL, concat(B.principio, ' - ', B.apresentacao), B.DESC_MATERIAIS_FATURAR) AS principio,
      S.NUMERO_TISS AS cod,
      S.CATALOGO_ID AS catalogo_id,
      'catalogo' AS TABELA_ORIGEM,
      C.nome,
      S.tipo,
      'P' AS flag,
      S.idSaida,
      C.idClientes AS Paciente,
      $cond1 AS Plano,
      COALESCE(B.valorMedio, '0.00') AS custo,
      (
        CASE
          S.tipo
          WHEN
            '0'
          THEN
            'Medicamento'
          WHEN
            '1'
          THEN
            'Material'
          WHEN
            '3'
          THEN
            'Dieta'
        END
      )
      AS apresentacao,
      (
        CASE
          WHEN
            COUNT(*) > 1
          THEN
            SUM(S.quantidade)
          ELSE
            S.quantidade
        END
      )
      AS quantidade, COALESCE(B.VALOR_VENDA, '0.00') AS valor, PL.nome AS nomeplano, C.empresa, B.TUSS AS codTuss, PL.tipo_medicamento
    FROM
      `saida` S
      INNER JOIN
        `clientes` C
        ON (S.idCliente = C.idClientes)
      INNER JOIN
        `catalogo` B
        ON (S.CATALOGO_ID = B.ID)
      INNER JOIN
        planosdesaude AS PL
        ON ( $cond1 = PL.id)
      INNER JOIN
        solicitacoes AS Sol
        ON (S.idSolicitacao = Sol.idSolicitacoes)
      LEFT JOIN
        prescricoes AS presc
        ON (Sol.id_periodica_complemento = presc.id)
    WHERE
      C.idClientes = '{$cod}'
      AND IF
        (
            Sol.JUSTIFICATIVA_AVULSO_ID = 2,
            (
                presc.inicio BETWEEN '{$inicio}' AND '{$fim}'
                OR presc.fim BETWEEN '{$inicio}' AND '{$fim}'
            ),
            S.data BETWEEN '{$inicio}' AND '{$fim}'
        )
      AND S.quantidade > 0 {$refaturarmed}
      AND
      (
        C.idClientes = S.idCliente
      )
      AND S.tipo IN
      (
        0, 1, 3
      )
      AND Sol.idPrescricao = -1
      AND Sol.TIPO_SOLICITACAO = -1
    GROUP BY
      Paciente, catalogo_id
    UNION
    SELECT
      IF(B.DESC_MATERIAIS_FATURAR IS NULL, concat(B.principio, ' - ', B.apresentacao), B.DESC_MATERIAIS_FATURAR) AS principio,
      S.NUMERO_TISS AS cod,
      S.CATALOGO_ID AS catalogo_id,
      'catalogo' AS TABELA_ORIGEM,
      C.nome,
      S.tipo,
      'P' AS flag,
      S.idSaida,
      C.idClientes AS Paciente,
      $cond1 AS Plano,
      COALESCE(B.valorMedio, '0.00') AS custo,
      (
        CASE
          S.tipo
          WHEN
            '0'
          THEN
            'Medicamento'
          WHEN
            '1'
          THEN
            'Material'
          WHEN
            '3'
          THEN
            'Dieta'
        END
      )
      AS apresentacao,
      (
        CASE
          WHEN
            COUNT(*) > 1
          THEN
            SUM(S.quantidade)
          ELSE
            S.quantidade
        END
      )
      AS quantidade, COALESCE(B.VALOR_VENDA, '0.00') AS valor, PL.nome AS nomeplano, C.empresa, B.TUSS AS codTuss, PL.tipo_medicamento
    FROM
      `saida` S
      INNER JOIN
        `clientes` C
        ON (S.idCliente = C.idClientes)
      INNER JOIN
        `catalogo` B
        ON (S.CATALOGO_ID = B.ID)
      INNER JOIN
        planosdesaude AS PL
        ON ( $cond1 = PL.id)
      INNER JOIN
        solicitacoes AS Sol
        ON (S.idSolicitacao = Sol.idSolicitacoes)
      INNER JOIN
        relatorio_aditivo_enf AS P
        ON (Sol.relatorio_aditivo_enf_id = P.ID)
    WHERE
      C.idClientes = '{$cod}'
      AND
      (
        P.INICIO BETWEEN '{$inicio}' AND '{$fim}'
        OR P.FIM BETWEEN '{$inicio}' AND '{$fim}'
      )
      AND S.quantidade > 0 {$refaturarmed}
      AND
      (
        C.idClientes = S.idCliente
      )
      AND S.tipo IN
      (
        0, 1, 3
      )
    GROUP BY
      Paciente, catalogo_id
    UNION
    SELECT
      IF(B.DESC_MATERIAIS_FATURAR IS NULL, concat(B.principio, ' - ', B.apresentacao), B.DESC_MATERIAIS_FATURAR) AS principio,
      S.NUMERO_TISS AS cod,
      S.CATALOGO_ID AS catalogo_id,
      'catalogo' AS TABELA_ORIGEM,
      C.nome,
      S.tipo,
      'P' AS flag,
      S.idSaida,
      C.idClientes AS Paciente,
      $cond1 AS Plano,
      COALESCE(B.valorMedio, '0.00') AS custo,
      (
        CASE
          S.tipo
          WHEN
            '0'
          THEN
            'Medicamento'
          WHEN
            '1'
          THEN
            'Material'
          WHEN
            '3'
          THEN
            'Dieta'
        END
      )
      AS apresentacao,
      (
        CASE
          WHEN
            COUNT(*) > 1
          THEN
            SUM(S.quantidade)
          ELSE
            S.quantidade
        END
      )
      AS quantidade, COALESCE(B.VALOR_VENDA, '0.00') AS valor, PL.nome AS nomeplano, C.empresa, B.TUSS AS codTuss, PL.tipo_medicamento
    FROM
      `saida` S
      INNER JOIN
        `clientes` C
        ON (S.idCliente = C.idClientes)
      INNER JOIN
        `catalogo` B
        ON (S.CATALOGO_ID = B.ID)
      INNER JOIN
        planosdesaude AS PL
        ON ( $cond1 = PL.id)
      INNER JOIN
        solicitacoes AS Sol
        ON (S.idSolicitacao = Sol.idSolicitacoes)
      INNER JOIN
        prescricoes AS P
        ON (Sol.idPrescricao = P.id)
    WHERE
      C.idClientes = '{$cod}'
      AND
      (
        P.inicio BETWEEN '{$inicio}' AND '{$fim}'
        OR P.fim BETWEEN '{$inicio}' AND '{$fim}'
      )
      AND S.quantidade > 0 {$refaturarmed}
      AND
      (
        C.idClientes = S.idCliente
      )
      AND S.tipo IN
      (
        0, 1, 3
      )
    GROUP BY
      Paciente, catalogo_id
  )
  AS principal
GROUP BY
  Paciente, catalogo_id
UNION
SELECT
  principal.principio,
  principal.cod,
  principal.catalogo_id,
  principal.TABELA_ORIGEM,
  principal.nome,
  principal.tipo,
  principal.flag,
  principal.idSolicitacoes,
  principal.Paciente,
  principal.Plano,
  principal.custo,
  principal.apresentacao,
  SUM(principal.quantidade) AS quantidade,
  principal.valor,
  principal.nomeplano,
  principal.empresa,
  principal.codTuss,
  principal.tipo_medicamento
FROM
  (
SELECT

	IF (
					BC.DESC_COBRANCA_PLANO IS NULL,
					B.item,
					BC.DESC_COBRANCA_PLANO
	) AS principio,

	IF (
					BC.DESC_COBRANCA_PLANO IS NULL,
					B.id,
					BC.COD_COBRANCA_PLANO
	) AS cod,

	IF (
					BC.DESC_COBRANCA_PLANO IS NULL,
					B.id,
					BC.id
	) AS catalogo_id,

	IF (
					BC.DESC_COBRANCA_PLANO IS NULL,
					'cobrancaplano',
					'valorescobranca'
	) AS TABELA_ORIGEM,
	 C.nome,
	 2 AS tipo,
	 'S' AS flag,
	 P.ID AS idSolicitacoes,
	 C.idClientes AS Paciente,
	 C.convenio AS Plano,
	 COALESCE (
					CS.CUSTO,
					'0.00'
	) AS custo,
	 ('Servi&ccedil;os') AS apresentacao,
	 
	 (
SUM(
CASE
WHEN
ESP.id IN
(
2,
3,
4,
5
)
THEN
IF ( ( '{$inicio}' <= P.INICIO
AND '{$fim}' >= P.INICIO )
AND
(
'{$inicio}' <= P.FIM
AND '{$fim}' >= P.FIM
)
,
(
DATEDIFF(P.FIM, P.INICIO) + 1
)
, IF ( ( '{$inicio}' <= P.INICIO
AND '{$fim}' >= P.INICIO )
AND
(
'{$fim}' <= P.FIM
)
,
(
DATEDIFF('{$fim}', P.INICIO) + 1
)
, IF ( ('{$inicio}' > P.INICIO)
AND
(
'{$fim}' >= P.FIM
AND '{$inicio}' <= P.FIM
)
,
(
DATEDIFF(P.FIM, '{$inicio}') + 1
)
, '1' ) ) )
ELSE
IF ( ( '{$inicio}' <= P.INICIO
AND '{$fim}' >= P.INICIO )
AND
(
'{$inicio}' <= P.FIM
AND '{$fim}' >= P.FIM
)
, IF ( S.frequencia IN
(
2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
)
,
(
DATEDIFF(P.FIM, P.INICIO) + 1
)
* F.qtd, CEILING( (DATEDIFF(P.FIM, P.INICIO) + 1) / 7 ) * F.SEMANA ), IF ( ( '{$inicio}' <= P.INICIO
AND '{$fim}' >= P.INICIO )
AND
(
'{$fim}' <= P.FIM
)
, IF ( S.frequencia IN
(
2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
)
,
(
DATEDIFF('{$fim}', P.INICIO) + 1
)
* F.qtd, CEILING( ( DATEDIFF('{$fim}', P.INICIO) / 7 ) + 1 ) * F.SEMANA ), IF ( ('{$inicio}' > P.INICIO)
AND
(
'{$fim}' >= P.FIM
AND '{$inicio}' <= P.FIM
)
, IF ( S.frequencia IN
(
2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
)
, (DATEDIFF(P.FIM, '{$inicio}')+1) * F.qtd, CEILING( ( DATEDIFF(P.FIM, '{$inicio}') / 7 ) + 1 ) * F.SEMANA ), '1' ) ) )
END
)
)
AS quantidade,
	 COALESCE (BC.valor, '0.00') AS valor,
	 PL.nome AS nomeplano,
	 C.empresa, 
	 '' AS codTuss,
	 PL.tipo_medicamento
	
	FROM
					`solicitacao_rel_aditivo_enf` S
	LEFT JOIN `frequencia` AS F ON (S.frequencia = F.id)
	INNER JOIN relatorio_aditivo_enf P ON (S.`relatorio_enf_aditivo_id` = P.ID)
	INNER JOIN `clientes` C ON (P.PACIENTE_ID = C.idClientes)
	INNER JOIN cuidadosespeciais AS ESP ON (S.CATALOGO_ID = ESP.id)
	INNER JOIN `cobrancaplanos` B ON (ESP.COBRANCAPLANO_ID = B.id)
	LEFT JOIN custo_servicos_plano_ur CS ON (
					CS.COBRANCAPLANO_ID = B.id
					AND CS.UR = C.empresa
					AND CS.STATUS = 'A'
	)
	LEFT JOIN `valorescobranca` BC ON (
					B.id = BC.idCobranca
					AND BC.idPlano = C.convenio and BC.excluido_por is null 
	)
	INNER JOIN planosdesaude AS PL ON (C.convenio = PL.id)
	WHERE
		 C.idClientes = '{$cod}'
  AND
  (
( P.INICIO BETWEEN '{$inicio}' AND '{$fim}' )
    OR
    (
      P.FIM BETWEEN '{$inicio}' AND '{$fim}'
    )
  )
	AND S.tipo IN (4)
	AND S.CATALOGO_ID NOT IN (17, 18, 21, 24)
	AND BC.excluido_por is NULL
	GROUP BY
	Paciente,
	B.id,
	BC.id
	UNION
SELECT
  IF(BC.DESC_COBRANCA_PLANO IS NULL, B.item, BC.DESC_COBRANCA_PLANO)AS principio,
  IF(BC.DESC_COBRANCA_PLANO IS NULL, B.id, BC.COD_COBRANCA_PLANO) AS cod,
  IF(BC.DESC_COBRANCA_PLANO IS NULL, B.id, BC.id) AS catalogo_id,
  IF(BC.DESC_COBRANCA_PLANO IS NULL, 'cobrancaplano', 'valorescobranca') AS TABELA_ORIGEM,
  C.nome,
  S.tipo,
  'S' AS flag,
  S.idPrescricao AS idSolicitacoes,
  C.idClientes AS Paciente,
  $cond1 AS Plano,
  COALESCE(CS.CUSTO, '0.00') AS custo,
  (
    'Servi&ccedil;os'
  )
  AS apresentacao,
  (
    SUM(
    CASE
      WHEN
        ESP.id IN
        (
          2,
          3,
          4,
          5
        )
      THEN
        IF(('{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio)
        AND
        (
          '{$inicio}' <= S.fim
          AND '{$fim}' >= S.fim
        )
,
        (
          DATEDIFF(S.fim, S.inicio) + 1
        )
, IF(('{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio)
        AND
        (
          '{$fim}' <= S.fim
        )
,
        (
          DATEDIFF('{$fim}', S.inicio) + 1
        )
, IF(('{$inicio}' > S.inicio)
        AND
        (
          '{$fim}' >= S.fim
          AND '{$inicio}' <= S.fim
        )
,
        (
          DATEDIFF(S.fim, '{$inicio}') + 1
        )
, '1')))
      ELSE
        IF(('{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio)
        AND
        (
          '{$inicio}' <= S.fim
          AND '{$fim}' >= S.fim
        )
, IF(S.frequencia IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
,
        (
          DATEDIFF(S.fim, S.inicio) + 1
        )
        *F.qtd, CEILING((DATEDIFF(S.fim, S.inicio) + 1) / 7)*F.SEMANA), IF(('{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio)
        AND
        (
          '{$fim}' <= S.fim
        )
, IF(S.frequencia IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
,
        (
          DATEDIFF('{$fim}', S.inicio) + 1
        )
        *F.qtd, CEILING((DATEDIFF('{$fim}', S.inicio) / 7) + 1)*F.SEMANA), IF(('{$inicio}' > S.inicio)
        AND
        (
          '{$fim}' >= S.fim
          AND '{$inicio}' <= S.fim
        )
, IF(S.frequencia IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
, (DATEDIFF(S.fim, '{$inicio}')+1) * F.qtd, CEILING((DATEDIFF(S.fim, '{$inicio}') / 7) + 1)*F.SEMANA), '1')))
    END
)
  )
  AS quantidade, COALESCE(BC.valor, '0.00') AS valor, PL.nome AS nomeplano, C.empresa, '' AS codTuss, PL.tipo_medicamento
FROM
  `itens_prescricao` S
  LEFT JOIN
    `frequencia` AS F
    ON (S.frequencia = F.id)
  INNER JOIN
    prescricoes P
    ON (S.`idPrescricao` = P.id)
  INNER JOIN
    `clientes` C
    ON (P.paciente = C.idClientes)
  INNER JOIN
    cuidadosespeciais AS ESP
    ON (S.CATALOGO_ID = ESP.id)
  INNER JOIN
    `cobrancaplanos` B
    ON (ESP.COBRANCAPLANO_ID = B.id)
  LEFT JOIN
    custo_servicos_plano_ur CS
    ON (CS.COBRANCAPLANO_ID = B.id
    AND CS.UR = C.empresa AND CS.STATUS = 'A')
  LEFT JOIN
    `valorescobranca` BC
    ON (B.id = BC.idCobranca
    AND BC.idPlano = $cond1
    AND BC.excluido_por IS NULL)
  INNER JOIN
    planosdesaude AS PL
    ON ( $cond1 = PL.id)
WHERE
  C.idClientes = '{$cod}'
  AND
  (
    P.inicio BETWEEN '{$inicio}' AND '{$fim}'
    OR P.fim BETWEEN '{$inicio}' AND '{$fim}'
  )
  AND S.tipo IN
  (
    2
  )
  AND P.Carater NOT IN
  (
    4, 7
  )
  {$refaturarserv}
  AND S.CATALOGO_ID NOT IN
  (
    17, 18, 21, 24
  )
  AND P.DESATIVADA_POR = 0
GROUP BY
  Paciente, B.id, BC.id
UNION
SELECT
  B.item AS principio,
  B.id AS cod,
  B.id AS catalogo_id,
  'cobrancaplano' AS TABELA_ORIGEM,
  C.nome,
  2 AS tipo,
  'S' AS flag,
  remocoes.id AS idSolicitacoes,
  C.idClientes AS Paciente,
  {$cond1} AS Plano,
  COALESCE(remocoes.outros_custos + remocoes.outras_despesas + remocoes.valor_medico + remocoes.valor_tecnico + remocoes.valor_condutor + remocoes.valor_enfermeiro, '0.00') AS custo,
  (
    'Servi&ccedil;os'
  )
  AS apresentacao,
  1 AS quantidade,
  COALESCE(remocoes.valor_remocao, '0.00') AS valor,
  planosdesaude.nome AS nomeplano,
  C.empresa,
  '' AS codTuss,
  planosdesaude.tipo_medicamento
FROM
  remocoes
  INNER JOIN
    clientes AS C
    ON remocoes.paciente_id = C.idClientes
  INNER JOIN
    empresas
    ON remocoes.empresa_id = empresas.id
  INNER JOIN
    planosdesaude
    ON remocoes.convenio_id = planosdesaude.id
  INNER JOIN
    cobrancaplanos AS B
    ON remocoes.cobrancaplano_id = B.id
WHERE
  remocoes.data_saida BETWEEN '{$inicio}' AND '{$fim}'
  AND remocoes.paciente_id = $cod
  AND remocoes.modo_cobranca = 'F'
  AND remocoes.convenio_id = {$cond1}
  AND remocoes.finalized_by IS NOT NULL
  AND remocoes.canceled_by IS NULL
  AND remocoes.tabela_origem_item_fatura <> 'valorescobranca'
GROUP BY
  Paciente,
  B.id
UNION
SELECT
  valorescobranca.DESC_COBRANCA_PLANO AS principio,
  valorescobranca.COD_COBRANCA_PLANO AS cod,
  valorescobranca.id AS catalogo_id,
  'valorescobranca' AS TABELA_ORIGEM,
  C.nome,
  2 AS tipo,
  'S' AS flag,
  remocoes.id AS idSolicitacoes,
  C.idClientes AS Paciente,
  {$cond1} AS Plano,
  COALESCE(remocoes.outros_custos + remocoes.outras_despesas + remocoes.valor_medico + remocoes.valor_tecnico + remocoes.valor_condutor + remocoes.valor_enfermeiro, '0.00') AS custo,
  (
    'Servi&ccedil;os'
  )
  AS apresentacao,
  1 AS quantidade,
  COALESCE(remocoes.valor_remocao, '0.00') AS valor,
  planosdesaude.nome AS nomeplano,
  C.empresa,
  '' AS codTuss,
  planosdesaude.tipo_medicamento
FROM
  remocoes
  INNER JOIN
    clientes AS C
    ON remocoes.paciente_id = C.idClientes
  INNER JOIN
    empresas
    ON remocoes.empresa_id = empresas.id
  INNER JOIN
    planosdesaude
    ON remocoes.convenio_id = planosdesaude.id
  INNER JOIN
    valorescobranca
    ON remocoes.valorescobranca_id = valorescobranca.id
WHERE
  remocoes.data_saida BETWEEN '{$inicio}' AND '{$fim}'
  AND remocoes.paciente_id = $cod
  AND remocoes.modo_cobranca = 'F'
  AND remocoes.convenio_id = {$cond1}
  AND remocoes.finalized_by IS NOT NULL
  AND remocoes.canceled_by IS NULL
  AND remocoes.tabela_origem_item_fatura = 'valorescobranca'
GROUP BY
  Paciente,
  valorescobranca.id
	UNION
SELECT
  IF ( BC.DESC_COBRANCA_PLANO IS NULL, B.item, BC.DESC_COBRANCA_PLANO ) AS principio,
  IF ( BC.DESC_COBRANCA_PLANO IS NULL, B.id, BC.COD_COBRANCA_PLANO ) AS cod,
  IF ( BC.DESC_COBRANCA_PLANO IS NULL, B.id, BC.id ) AS catalogo_id,
  IF ( BC.DESC_COBRANCA_PLANO IS NULL, 'cobrancaplano', 'valorescobranca' ) AS TABELA_ORIGEM,
  C.nome,
  2 AS tipo,
  'S' AS flag,
  S.prescricao_enf_id AS idSolicitacoes,
  C.idClientes AS Paciente,
  {$cond1} AS Plano,
  COALESCE(CS.CUSTO, '0.00') AS custo,
  (
    'Servi&ccedil;os'
  )
  AS apresentacao,
  (
    SUM(
    CASE
      WHEN
        ESP.id IN
        (
          2,
          3,
          4,
          5
        )
      THEN
        IF ( ( '{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio )
        AND
        (
          '{$inicio}' <= S.fim
          AND '{$fim}' >= S.fim
        )
,
        (
          DATEDIFF(S.fim, S.inicio) + 1
        )
, IF ( ( '{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio )
        AND
        (
          '{$fim}' <= S.fim
        )
,
        (
          DATEDIFF('{$fim}', S.inicio) + 1
        )
, IF ( ('{$inicio}' > S.inicio)
        AND
        (
          '{$fim}' >= S.fim
          AND '{$inicio}' <= S.fim
        )
,
        (
          DATEDIFF(S.fim, '{$inicio}') + 1
        )
, '1' ) ) )
      ELSE
        IF ( ( '{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio )
        AND
        (
          '{$inicio}' <= S.fim
          AND '{$fim}' >= S.fim
        )
, IF ( S.frequencia_id IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
,
        (
          DATEDIFF(S.fim, S.inicio) + 1
        )
        * F.qtd, CEILING( (DATEDIFF(S.fim, S.inicio) + 1) / 7 ) * F.SEMANA ), IF ( ( '{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio )
        AND
        (
          '{$fim}' <= S.fim
        )
, IF ( S.frequencia_id IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
,
        (
          DATEDIFF('{$fim}', S.inicio) + 1
        )
        * F.qtd, CEILING( ( DATEDIFF('{$fim}', S.inicio) / 7 ) + 1 ) * F.SEMANA ), IF ( ('{$inicio}' > S.inicio)
        AND
        (
          '{$fim}' >= S.fim
          AND '{$inicio}' <= S.fim
        )
, IF ( S.frequencia_id IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
, (DATEDIFF(S.fim, '{$inicio}')+1) * F.qtd, CEILING( ( DATEDIFF(S.fim, '{$inicio}') / 7 ) + 1 ) * F.SEMANA ), '1' ) ) )
    END
)
  )
  AS quantidade, COALESCE (BC.valor, '0.00') AS valor, PL.nome AS nomeplano, C.empresa, '' AS codTuss, PL.tipo_medicamento
FROM
  `prescricao_enf_itens_prescritos` AS S
  LEFT JOIN
    `frequencia` AS F
    ON (S.frequencia_id = F.id)
  INNER JOIN
    prescricao_enf P
    ON (S.prescricao_enf_id = P.id)
  INNER JOIN
    `clientes` C
    ON (P.cliente_id = C.idClientes)
  INNER JOIN
    prescricao_enf_cuidado AS ESP
    ON ( S.presc_enf_cuidados_id = ESP.id)
  INNER JOIN
    cuidadosespeciais AS CUI
    ON ESP.cuidado_especiais_id = CUI.id
  INNER JOIN
    `cobrancaplanos` B
    ON (CUI.COBRANCAPLANO_ID = B.id)
  LEFT JOIN
    `valorescobranca` BC
    ON ( B.id = BC.idCobranca
    AND BC.idPlano = C.convenio
    AND BC.excluido_por IS NULL )
  LEFT JOIN
    custo_servicos_plano_ur CS
    ON (CS.COBRANCAPLANO_ID = B.id
    AND CS.UR = C.empresa AND CS.STATUS = 'A')
  INNER JOIN
    planosdesaude AS PL
    ON ( C.convenio = PL.id)
WHERE
  C.idClientes = '{$cod}'
  AND
  (
( P.inicio BETWEEN '{$inicio}' AND '{$fim}' )
    OR
    (
      P.fim BETWEEN '{$inicio}' AND '{$fim}'
    )
  )
  AND P.cancelado_por IS NULL
  AND ESP.presc_enf_grupo_id = 7
  AND P.carater IN
  (
    8, 9
  )
GROUP BY
  Paciente, B.id, BC.id
 )
	 AS principal
GROUP BY
  Paciente, TABELA_ORIGEM, catalogo_id
	

UNION
SELECT DISTINCT
  IF(V.DESC_COBRANCA_PLANO IS NULL, CO.item, V.DESC_COBRANCA_PLANO)AS principio,
  IF(V.DESC_COBRANCA_PLANO IS NULL, CO.id, V.COD_COBRANCA_PLANO) AS cod,
  IF(V.DESC_COBRANCA_PLANO IS NULL, CO.id, V.id) AS catalogo_id,
  IF(V.DESC_COBRANCA_PLANO IS NULL, 'cobrancaplano', 'valorescobranca') AS TABELA_ORIGEM,
  C.nome,
  5 AS tipo,
  'N' AS flag,
  0 AS idSolicitacoes,
  C.idClientes AS Paciente,
  $cond1 AS Plano,
  COALESCE(CS.CUSTO, '0.00') AS custo,
  (
    'Equipamentos'
  )
  AS apresentacao,
  (SUM(
  IF(
  ('{$inicio}' <= E.DATA_INICIO AND '{$fim}' >= E.DATA_INICIO) 
  AND 
  ('{$inicio}' <= E.DATA_FIM AND '{$fim}' >= E.DATA_FIM),
  (DATEDIFF(E.DATA_FIM, E.DATA_INICIO) + 1),
  IF(
  ('{$inicio}' <= E.DATA_INICIO AND '{$fim}' >= E.DATA_INICIO) 
  AND ('{$fim}' <= E.DATA_FIM), 
  (DATEDIFF('{$fim}', E.DATA_INICIO) + 1),
  IF(
  ('{$inicio}' > E.DATA_INICIO) AND ('{$fim}' >= E.DATA_FIM AND '{$inicio}' <= E.DATA_FIM),
  (DATEDIFF(E.DATA_FIM, '{$inicio}') + 1),
  IF(('{$inicio}' > E.DATA_INICIO) AND ('{$fim}' >= E.DATA_FIM AND '{$inicio}' >= E.DATA_FIM) 
  AND (E.DATA_FIM = '0000-00-00 00:00:00' OR E.DATA_FIM LIKE '%0000-00-00%'),
  (DATEDIFF('{$fim}', '{$inicio}') + 1),
  '1'
  )
  )
  )
  )
  )
  ) AS quantidade,
  COALESCE(V.valor, '0.00') AS valor,
  PL.nome AS nomeplano,
  C.empresa,
  '' AS codTuss,
  PL.tipo_medicamento
FROM
  clientes C
  INNER JOIN
    equipamentosativos E
    ON (C.idClientes = E.`PACIENTE_ID`)
  INNER JOIN
    cobrancaplanos CO
    ON (E.`COBRANCA_PLANOS_ID` = CO.`id`)
  LEFT JOIN
    custo_servicos_plano_ur CS
    ON (CS.COBRANCAPLANO_ID = CO.id
    AND CS.UR = C.empresa AND CS.STATUS = 'A')
  LEFT JOIN
    valorescobranca V
    ON (CO.`id` = V.`idCobranca`
    AND $cond1 = V.`idPlano`
    AND V.excluido_por IS NULL)
  INNER JOIN
    planosdesaude AS PL
    ON ( $cond1 = PL.id)
WHERE
  C.idClientes = '{$cod}'
  AND E.PRESCRICAO_AVALIACAO != 'S' {$refaturareqp}
GROUP BY
  Paciente,
  CO.id,
  V.id

ORDER BY
  Paciente, tipo, principio";



    }

    public static function getItensPreFaturaByUR($cod, $cond1, $inicio, $fim, $refaturarmed, $refaturarserv, $refaturareqp )
    {
        return $sql = "
SELECT
  principal.principio,
  principal.cod,
  principal.catalogo_id,
  principal.TABELA_ORIGEM,
  principal.nome,
  principal.tipo,
  principal.flag,
  principal.idSaida,
  principal.Paciente,
  principal.Plano,
  principal.custo,
  principal.apresentacao,
  SUM(principal.quantidade) AS quantidade,
  principal.valor,
  principal.nomeplano,
  principal.empresa,
  principal.codTuss,
  principal.tipo_medicamento
FROM
  (
    SELECT
      IF(B.DESC_MATERIAIS_FATURAR IS NULL, concat(B.principio, ' - ', B.apresentacao), B.DESC_MATERIAIS_FATURAR) AS principio,
      S.NUMERO_TISS AS cod,
      S.CATALOGO_ID AS catalogo_id,
      'catalogo' AS TABELA_ORIGEM,
      C.nome,
      S.tipo,
      'P' AS flag,
      S.idSaida,
      C.idClientes AS Paciente,
      C.convenio AS Plano,
      COALESCE(B.valorMedio, '0.00') AS custo,
      (
        CASE
          S.tipo
          WHEN
            '0'
          THEN
            'Medicamento'
          WHEN
            '1'
          THEN
            'Material'
          WHEN
            '3'
          THEN
            'Dieta'
        END
      )
      AS apresentacao,
      (
        CASE
          WHEN
            COUNT(*) > 1
          THEN
            SUM(S.quantidade)
          ELSE
            S.quantidade
        END
      )
      AS quantidade, COALESCE(B.VALOR_VENDA, '0.00') AS valor, PL.nome AS nomeplano, C.empresa, B.TUSS AS codTuss, PL.tipo_medicamento
    FROM
      `saida` S
      INNER JOIN
        `clientes` C
        ON (S.idCliente = C.idClientes)
      INNER JOIN
        `catalogo` B
        ON (S.CATALOGO_ID = B.ID)
      INNER JOIN
        planosdesaude AS PL
        ON (C.convenio = PL.id)
      INNER JOIN
        solicitacoes AS Sol
        ON (S.idSolicitacao = Sol.idSolicitacoes)
      LEFT JOIN
        prescricoes AS presc
        ON (Sol.id_periodica_complemento = presc.id)
    WHERE
      C.empresa = '{$cod}'

      AND IF
        (
            Sol.JUSTIFICATIVA_AVULSO_ID = 2,
            (
                presc.inicio BETWEEN '{$inicio}' AND '{$fim}'
                OR presc.fim BETWEEN '{$inicio}' AND '{$fim}'
            ),
           S.data BETWEEN '{$inicio}' AND '{$fim}'
        )
      AND S.quantidade > 0 {$refaturarmed}
      AND
      (
        C.idClientes = S.idCliente
      )
      AND S.tipo IN
      (
        0, 1, 3
      )
      AND Sol.idPrescricao = -1
      AND Sol.TIPO_SOLICITACAO = -1
    GROUP BY
      Paciente, catalogo_id
    UNION
    SELECT
      IF(B.DESC_MATERIAIS_FATURAR IS NULL, concat(B.principio, ' - ', B.apresentacao), B.DESC_MATERIAIS_FATURAR) AS principio,
      S.NUMERO_TISS AS cod,
      S.CATALOGO_ID AS catalogo_id,
      'catalogo' AS TABELA_ORIGEM,
      C.nome,
      S.tipo,
      'P' AS flag,
      S.idSaida,
      C.idClientes AS Paciente,
      C.convenio AS Plano,
      COALESCE(B.valorMedio, '0.00') AS custo,
      (
        CASE
          S.tipo
          WHEN
            '0'
          THEN
            'Medicamento'
          WHEN
            '1'
          THEN
            'Material'
          WHEN
            '3'
          THEN
            'Dieta'
        END
      )
      AS apresentacao,
      (
        CASE
          WHEN
            COUNT(*) > 1
          THEN
            SUM(S.quantidade)
          ELSE
            S.quantidade
        END
      )
      AS quantidade, COALESCE(B.VALOR_VENDA, '0.00') AS valor, PL.nome AS nomeplano, C.empresa, B.TUSS AS codTuss, PL.tipo_medicamento
    FROM
      `saida` S
      INNER JOIN
        `clientes` C
        ON (S.idCliente = C.idClientes)
      INNER JOIN
        `catalogo` B
        ON (S.CATALOGO_ID = B.ID)
      INNER JOIN
        planosdesaude AS PL
        ON (C.convenio = PL.id)
      INNER JOIN
        solicitacoes AS Sol
        ON (S.idSolicitacao = Sol.idSolicitacoes)
      INNER JOIN
        prescricoes AS P
        ON (Sol.idPrescricao = P.id)
    WHERE
      C.empresa = '{$cod}'
      AND
      (
        P.inicio BETWEEN '{$inicio}' AND '{$fim}'
        OR P.fim BETWEEN '{$inicio}' AND '{$fim}'
      )
      AND S.quantidade > 0 {$refaturarmed}
      AND
      (
        C.idClientes = S.idCliente
      )
      AND S.tipo IN
      (
        0, 1, 3
      )
    GROUP BY
      Paciente, catalogo_id
    UNION
    SELECT
      IF(B.DESC_MATERIAIS_FATURAR IS NULL, concat(B.principio, ' - ', B.apresentacao), B.DESC_MATERIAIS_FATURAR) AS principio,
      S.NUMERO_TISS AS cod,
      S.CATALOGO_ID AS catalogo_id,
      'catalogo' AS TABELA_ORIGEM,
      C.nome,
      S.tipo,
      'P' AS flag,
      S.idSaida,
      C.idClientes AS Paciente,
      C.convenio AS Plano,
      COALESCE(B.valorMedio, '0.00') AS custo,
      (
        CASE
          S.tipo
          WHEN
            '0'
          THEN
            'Medicamento'
          WHEN
            '1'
          THEN
            'Material'
          WHEN
            '3'
          THEN
            'Dieta'
        END
      )
      AS apresentacao,
      (
        CASE
          WHEN
            COUNT(*) > 1
          THEN
            SUM(S.quantidade)
          ELSE
            S.quantidade
        END
      )
      AS quantidade, COALESCE(B.VALOR_VENDA, '0.00') AS valor, PL.nome AS nomeplano, C.empresa, B.TUSS AS codTuss, PL.tipo_medicamento
    FROM
      `saida` S
      INNER JOIN
        `clientes` C
        ON (S.idCliente = C.idClientes)
      INNER JOIN
        `catalogo` B
        ON (S.CATALOGO_ID = B.ID)
      INNER JOIN
        planosdesaude AS PL
        ON (C.convenio = PL.id)
      INNER JOIN
        solicitacoes AS Sol
        ON (S.idSolicitacao = Sol.idSolicitacoes)
      INNER JOIN
        relatorio_aditivo_enf AS P
        ON (Sol.relatorio_aditivo_enf_id = P.ID)
    WHERE
      C.empresa = '{$cod}'
      AND
      (
        P.INICIO BETWEEN '{$inicio}' AND '{$fim}'
        OR P.FIM BETWEEN '{$inicio}' AND '{$fim}'
      )
      AND S.quantidade > 0 {$refaturarmed}
      AND
      (
        C.idClientes = S.idCliente
      )
      AND S.tipo IN
      (
        0, 1, 3
      )
    GROUP BY
      Paciente, catalogo_id
  )
  AS principal
GROUP BY
  Paciente, catalogo_id
UNION

SELECT
  principal.principio,
  principal.cod,
  principal.catalogo_id,
  principal.TABELA_ORIGEM,
  principal.nome,
  principal.tipo,
  principal.flag,
  principal.idSolicitacoes,
  principal.Paciente,
  principal.Plano,
  principal.custo,
  principal.apresentacao,
  SUM(principal.quantidade) AS quantidade,
  principal.valor,
  principal.nomeplano,
  principal.empresa,
  principal.codTuss,
  principal.tipo_medicamento
FROM
  (
SELECT

	IF (
					BC.DESC_COBRANCA_PLANO IS NULL,
					B.item,
					BC.DESC_COBRANCA_PLANO
	) AS principio,

	IF (
					BC.DESC_COBRANCA_PLANO IS NULL,
					B.id,
					BC.COD_COBRANCA_PLANO
	) AS cod,

	IF (
					BC.DESC_COBRANCA_PLANO IS NULL,
					B.id,
					BC.id
	) AS catalogo_id,

	IF (
					BC.DESC_COBRANCA_PLANO IS NULL,
					'cobrancaplano',
					'valorescobranca'
	) AS TABELA_ORIGEM,
	 C.nome,
	 2 AS tipo,
	 'S' AS flag,
	 P.ID AS idSolicitacoes,
	 C.idClientes AS Paciente,
	 C.convenio AS Plano,
	 COALESCE (
					CS.CUSTO,
					'0.00'
	) AS custo,
	 ('Servi&ccedil;os') AS apresentacao,
	 
	 (
SUM(
CASE
WHEN
ESP.id IN
(
2,
3,
4,
5
)
THEN
IF ( ( '{$inicio}' <= P.INICIO
AND '{$fim}' >= P.INICIO )
AND
(
'{$inicio}' <= P.FIM
AND '{$fim}' >= P.FIM
)
,
(
DATEDIFF(P.FIM, P.INICIO) + 1
)
, IF ( ( '{$inicio}' <= P.INICIO
AND '{$fim}' >= P.INICIO )
AND
(
'{$fim}' <= P.FIM
)
,
(
DATEDIFF('{$fim}', P.INICIO) + 1
)
, IF ( ('{$inicio}' > P.INICIO)
AND
(
'{$fim}' >= P.FIM
AND '{$inicio}' <= P.FIM
)
,
(
DATEDIFF(P.FIM, '{$inicio}') + 1
)
, '1' ) ) )
ELSE
IF ( ( '{$inicio}' <= P.INICIO
AND '{$fim}' >= P.INICIO )
AND
(
'{$inicio}' <= P.FIM
AND '{$fim}' >= P.FIM
)
, IF ( S.frequencia IN
(
2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
)
,
(
DATEDIFF(P.FIM, P.INICIO) + 1
)
* F.qtd, CEILING( (DATEDIFF(P.FIM, P.INICIO) + 1) / 7 ) * F.SEMANA ), IF ( ( '{$inicio}' <= P.INICIO
AND '{$fim}' >= P.INICIO )
AND
(
'{$fim}' <= P.FIM
)
, IF ( S.frequencia IN
(
2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
)
,
(
DATEDIFF('{$fim}', P.INICIO) + 1
)
* F.qtd, CEILING( ( DATEDIFF('{$fim}', P.INICIO) / 7 ) + 1 ) * F.SEMANA ), IF ( ('{$inicio}' > P.INICIO)
AND
(
'{$fim}' >= P.FIM
AND '{$inicio}' <= P.FIM
)
, IF ( S.frequencia IN
(
2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
)
, (DATEDIFF(P.FIM, '{$inicio}') + 1) * F.qtd, CEILING( ( DATEDIFF(P.FIM, '{$inicio}') / 7 ) + 1 ) * F.SEMANA ), '1' ) ) )
END
)
)
AS quantidade,
	 COALESCE (BC.valor, '0.00') AS valor,
	 PL.nome AS nomeplano,
	 C.empresa, 
	 '' AS codTuss,
	 PL.tipo_medicamento
	
	FROM
					`solicitacao_rel_aditivo_enf` S
	LEFT JOIN `frequencia` AS F ON (S.frequencia = F.id)
	INNER JOIN relatorio_aditivo_enf P ON (S.`relatorio_enf_aditivo_id` = P.ID)
	INNER JOIN `clientes` C ON (P.PACIENTE_ID = C.idClientes)
	INNER JOIN cuidadosespeciais AS ESP ON (S.CATALOGO_ID = ESP.id)
	INNER JOIN `cobrancaplanos` B ON (ESP.COBRANCAPLANO_ID = B.id)
	LEFT JOIN custo_servicos_plano_ur CS ON (
					CS.COBRANCAPLANO_ID = B.id
					AND CS.UR = C.empresa
					AND CS.STATUS = 'A'
	)
	LEFT JOIN `valorescobranca` BC ON (
					B.id = BC.idCobranca
					AND BC.idPlano = C.convenio and BC.excluido_por is null 
	)
	INNER JOIN planosdesaude AS PL ON (C.convenio = PL.id)
	WHERE
		  C.empresa = '{$cod}'
  AND
  (
( P.INICIO BETWEEN '{$inicio}' AND '{$fim}' )
    OR
    (
      P.FIM BETWEEN '{$inicio}' AND '{$fim}'
    )
  )
	AND S.tipo IN (4)
	AND S.CATALOGO_ID NOT IN (17, 18, 21, 24)
	AND BC.excluido_por is NULL
	GROUP BY
	Paciente,
	B.id,
	BC.id
	UNION
SELECT
  cobrancaplanos.item AS principio,
  cobrancaplanos.id AS cod,
  cobrancaplanos.id AS catalogo_id,
  'cobrancaplano' AS TABELA_ORIGEM,
  C.nome,
  2 AS tipo,
  'S' AS flag,
  remocoes.id AS idSolicitacoes,
  C.idClientes AS Paciente,
  remocoes.convenio_id AS Plano,
  COALESCE(remocoes.outros_custos + remocoes.outras_despesas + remocoes.valor_medico + remocoes.valor_tecnico + remocoes.valor_condutor + remocoes.valor_enfermeiro, '0.00') AS custo,
  (
    'Servi&ccedil;os'
  )
  AS apresentacao,
  1 AS quantidade,
  COALESCE(remocoes.valor_remocao, '0.00') AS valor,
  planosdesaude.nome AS nomeplano,
  C.empresa,
  '' AS codTuss,
  planosdesaude.tipo_medicamento
FROM
  remocoes
  INNER JOIN
    clientes AS C
    ON remocoes.paciente_id = C.idClientes
  INNER JOIN
    empresas
    ON remocoes.empresa_id = empresas.id
  INNER JOIN
    planosdesaude
    ON remocoes.convenio_id = planosdesaude.id
  INNER JOIN
    cobrancaplanos
    ON remocoes.cobrancaplano_id = cobrancaplanos.id
WHERE
  remocoes.data_saida BETWEEN '{$inicio}' AND '{$fim}'
  AND remocoes.modo_cobranca = 'F'
  AND remocoes.convenio_id = C.convenio
  AND remocoes.empresa_id = {$cod}
  AND remocoes.finalized_by IS NOT NULL
  AND remocoes.canceled_by IS NULL
  AND remocoes.tabela_origem_item_fatura <> 'valorescobranca'
GROUP BY
  Paciente,
  cobrancaplanos.id
UNION
SELECT
  valorescobranca.DESC_COBRANCA_PLANO AS principio,
  valorescobranca.COD_COBRANCA_PLANO AS cod,
  valorescobranca.id AS catalogo_id,
  'valorescobranca' AS TABELA_ORIGEM,
  C.nome,
  2 AS tipo,
  'S' AS flag,
  remocoes.id AS idSolicitacoes,
  C.idClientes AS Paciente,
  remocoes.convenio_id AS Plano,
  COALESCE(remocoes.outros_custos + remocoes.outras_despesas + remocoes.valor_medico + remocoes.valor_tecnico + remocoes.valor_condutor + remocoes.valor_enfermeiro, '0.00') AS custo,
  (
    'Servi&ccedil;os'
  )
  AS apresentacao,
  1 AS quantidade,
  COALESCE(remocoes.valor_remocao, '0.00') AS valor,
  planosdesaude.nome AS nomeplano,
  C.empresa,
  '' AS codTuss,
  planosdesaude.tipo_medicamento
FROM
  remocoes
  INNER JOIN
    clientes AS C
    ON remocoes.paciente_id = C.idClientes
  INNER JOIN
    empresas
    ON remocoes.empresa_id = empresas.id
  INNER JOIN
    planosdesaude
    ON remocoes.convenio_id = planosdesaude.id
  INNER JOIN
    valorescobranca
    ON remocoes.valorescobranca_id = valorescobranca.id
WHERE
  remocoes.data_saida BETWEEN '{$inicio}' AND '{$fim}'
  AND remocoes.modo_cobranca = 'F'
  AND remocoes.convenio_id = C.convenio
  AND remocoes.empresa_id = '{$cod}'
  AND remocoes.finalized_by IS NOT NULL
  AND remocoes.canceled_by IS NULL
  AND remocoes.tabela_origem_item_fatura = 'valorescobranca'
GROUP BY
  Paciente,
  valorescobranca.id
UNION
SELECT
  IF(BC.DESC_COBRANCA_PLANO IS NULL, B.item, BC.DESC_COBRANCA_PLANO)AS principio,
  IF(BC.DESC_COBRANCA_PLANO IS NULL, B.id, BC.COD_COBRANCA_PLANO) AS cod,
  IF(BC.DESC_COBRANCA_PLANO IS NULL, B.id, BC.id) AS catalogo_id,
  IF(BC.DESC_COBRANCA_PLANO IS NULL, 'cobrancaplano', 'valorescobranca') AS TABELA_ORIGEM,
  C.nome,
  S.tipo,
  'S' AS flag,
  S.idPrescricao AS idSolicitacoes,
  C.idClientes AS Paciente,
  C.convenio AS Plano,
  COALESCE(CS.CUSTO, '0.00') AS custo,
  (
    'Servi&ccedil;os'
  )
  AS apresentacao,
  (
    SUM(
    CASE
      WHEN
        ESP.id IN
        (
          2,
          3,
          4,
          5
        )
      THEN
        IF(('{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio)
        AND
        (
          '{$inicio}' <= S.fim
          AND '{$fim}' >= S.fim
        )
,
        (
          DATEDIFF(S.fim, S.inicio) + 1
        )
, IF(('{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio)
        AND
        (
          '{$fim}' <= S.fim
        )
,
        (
          DATEDIFF('{$fim}', S.inicio) + 1
        )
, IF(('{$inicio}' > S.inicio)
        AND
        (
          '{$fim}' >= S.fim
          AND '{$inicio}' <= S.fim
        )
,
        (
          DATEDIFF(S.fim, '{$inicio}') + 1
        )
, '1')))
      ELSE
        IF(('{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio)
        AND
        (
          '{$inicio}' <= S.fim
          AND '{$fim}' >= S.fim
        )
, IF(S.frequencia IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
,
        (
          DATEDIFF(S.fim, S.inicio) + 1
        )
        *F.qtd, CEILING((DATEDIFF(S.fim, S.inicio) + 1) / 7)*F.SEMANA), IF(('{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio)
        AND
        (
          '{$fim}' <= S.fim
        )
, IF(S.frequencia IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
,
        (
          DATEDIFF('{$fim}', S.inicio) + 1
        )
        *F.qtd, CEILING((DATEDIFF('{$fim}', S.inicio) / 7) + 1)*F.SEMANA), IF(('{$inicio}' > S.inicio)
        AND
        (
          '{$fim}' >= S.fim
          AND '{$inicio}' <= S.fim
        )
, IF(S.frequencia IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
, (DATEDIFF(S.fim, '{$inicio}') +1) *F.qtd, CEILING((DATEDIFF(S.fim, '{$inicio}') / 7) + 1)*F.SEMANA), '1')))
    END
)
  )
  AS quantidade, COALESCE(BC.valor, '0.00') AS valor, PL.nome AS nomeplano, C.empresa, '' AS codTuss, PL.tipo_medicamento
FROM
  `itens_prescricao` S
  LEFT JOIN
    `frequencia` AS F
    ON(S.frequencia = F.id)
  INNER JOIN
    prescricoes P
    ON (S.`idPrescricao` = P.id)
  INNER JOIN
    `clientes` C
    ON (P.paciente = C.idClientes)
  INNER JOIN
    cuidadosespeciais AS ESP
    ON (S.CATALOGO_ID = ESP.id)
  INNER JOIN
    `cobrancaplanos` B
    ON (ESP.COBRANCAPLANO_ID = B.id)
  LEFT JOIN
    custo_servicos_plano_ur CS
    ON (CS.COBRANCAPLANO_ID = B.id
    AND CS.UR = C.empresa AND CS.STATUS = 'A')
  LEFT JOIN
    `valorescobranca` BC
    ON (B.id = BC.idCobranca
    AND BC.idPlano = C.convenio
    AND BC.excluido_por IS NULL)
  INNER JOIN
    planosdesaude AS PL
    ON (C.convenio = PL.id)
WHERE
  C.empresa = '{$cod}'
  AND
  (
    P.inicio BETWEEN '{$inicio}' AND '{$fim}'
    OR P.fim BETWEEN '{$inicio}' AND '{$fim}'
  )
  AND S.tipo IN
  (
    2
  )
  AND P.Carater NOT IN
  (
    4, 7
  )
  {$refaturarserv}
  AND S.CATALOGO_ID NOT IN
  (
    17, 18, 21, 24
  )
  AND P.DESATIVADA_POR = 0 $cond1
GROUP BY
  Paciente, B.id, BC.id

UNION
SELECT
  IF ( BC.DESC_COBRANCA_PLANO IS NULL, B.item, BC.DESC_COBRANCA_PLANO ) AS principio,
  IF ( BC.DESC_COBRANCA_PLANO IS NULL, B.id, BC.COD_COBRANCA_PLANO ) AS cod,
  IF ( BC.DESC_COBRANCA_PLANO IS NULL, B.id, BC.id ) AS catalogo_id,
  IF ( BC.DESC_COBRANCA_PLANO IS NULL, 'cobrancaplano', 'valorescobranca' ) AS TABELA_ORIGEM,
  C.nome,
  2 AS tipo,
  'S' AS flag,
  S.prescricao_enf_id AS idSolicitacoes,
  C.idClientes AS Paciente,
  C.convenio AS Plano,
  COALESCE(CS.CUSTO, '0.00') AS custo,
  (
    'Servi&ccedil;os'
  )
  AS apresentacao,
  (
    SUM(
    CASE
      WHEN
        ESP.id IN
        (
          2,
          3,
          4,
          5
        )
      THEN
        IF ( ( '{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio )
        AND
        (
          '{$inicio}' <= S.fim
          AND '{$fim}' >= S.fim
        )
,
        (
          DATEDIFF(S.fim, S.inicio) + 1
        )
, IF ( ( '{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio )
        AND
        (
          '{$fim}' <= S.fim
        )
,
        (
          DATEDIFF('{$fim}', S.inicio) + 1
        )
, IF ( ('{$inicio}' > S.inicio)
        AND
        (
          '{$fim}' >= S.fim
          AND '{$inicio}' <= S.fim
        )
,
        (
          DATEDIFF(S.fim, '{$inicio}') + 1
        )
, '1' ) ) )
      ELSE
        IF ( ( '{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio )
        AND
        (
          '{$inicio}' <= S.fim
          AND '{$fim}' >= S.fim
        )
, IF ( S.frequencia_id IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
,
        (
          DATEDIFF(S.fim, S.inicio) + 1
        )
        * F.qtd, CEILING( (DATEDIFF(S.fim, S.inicio) + 1) / 7 ) * F.SEMANA ), IF ( ( '{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio )
        AND
        (
          '{$fim}' <= S.fim
        )
, IF ( S.frequencia_id IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
,
        (
          DATEDIFF('{$fim}', S.inicio) + 1
        )
        * F.qtd, CEILING( ( DATEDIFF('{$fim}', S.inicio) / 7 ) + 1 ) * F.SEMANA ), IF ( ('{$inicio}' > S.inicio)
        AND
        (
          '{$fim}' >= S.fim
          AND '{$inicio}' <= S.fim
        )
, IF ( S.frequencia_id IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
, (DATEDIFF(S.fim, '{$inicio}') +1) * F.qtd, CEILING( ( DATEDIFF(S.fim, '{$inicio}') / 7 ) + 1 ) * F.SEMANA ), '1' ) ) )
    END
)
  )
  AS quantidade, COALESCE (BC.valor, '0.00') AS valor, PL.nome AS nomeplano, C.empresa, '' AS codTuss, PL.tipo_medicamento
FROM
  `prescricao_enf_itens_prescritos` AS S
  LEFT JOIN
    `frequencia` AS F
    ON (S.frequencia_id = F.id)
  INNER JOIN
    prescricao_enf P
    ON (S.prescricao_enf_id = P.id)
  INNER JOIN
    `clientes` C
    ON (P.cliente_id = C.idClientes)
  INNER JOIN
    prescricao_enf_cuidado AS ESP
    ON ( S.presc_enf_cuidados_id = ESP.id)
  INNER JOIN
    cuidadosespeciais AS CUI
    ON ESP.cuidado_especiais_id = CUI.id
  INNER JOIN
    `cobrancaplanos` B
    ON (CUI.COBRANCAPLANO_ID = B.id)
  LEFT JOIN
    `valorescobranca` BC
    ON ( B.id = BC.idCobranca
    AND BC.idPlano = C.convenio
    AND BC.excluido_por IS NULL )
  LEFT JOIN
    custo_servicos_plano_ur CS
    ON (CS.COBRANCAPLANO_ID = B.id
    AND CS.UR = C.empresa AND CS.STATUS = 'A')
  INNER JOIN
    planosdesaude AS PL
    ON ( C.convenio = PL.id)
WHERE
  C.empresa = '{$cod}'
  AND
  (
( P.inicio BETWEEN '{$inicio}' AND '{$fim}' )
    OR
    (
      P.fim BETWEEN '{$inicio}' AND '{$fim}'
    )
  )
  AND P.cancelado_por IS NULL
  AND ESP.presc_enf_grupo_id = 7
  AND P.carater IN
  (
    8, 9
  )
GROUP BY
  Paciente, B.id, BC.id)
	AS principal
GROUP BY
  Paciente, TABELA_ORIGEM, catalogo_id
  UNION
SELECT DISTINCT
  IF(V.DESC_COBRANCA_PLANO IS NULL, CO.item, V.DESC_COBRANCA_PLANO)AS principio,
  IF(V.DESC_COBRANCA_PLANO IS NULL, CO.id, V.COD_COBRANCA_PLANO) AS cod,
  IF(V.DESC_COBRANCA_PLANO IS NULL, CO.id, V.id) AS catalogo_id,
  IF(V.DESC_COBRANCA_PLANO IS NULL, 'cobrancaplano', 'valorescobranca') AS TABELA_ORIGEM,
  C.nome,
  5 AS tipo,
  'N' AS flag,
  0 AS idSolicitacoes,
  C.idClientes AS Paciente,
  C.convenio AS Plano,
  COALESCE(CS.`CUSTO`, '0.00') AS custo,
  (
    'Equipamentos'
  )
  AS apresentacao,
  (SUM(
  IF(
  ('{$inicio}' <= E.DATA_INICIO AND '{$fim}' >= E.DATA_INICIO) 
  AND 
  ('{$inicio}' <= E.DATA_FIM AND '{$fim}' >= E.DATA_FIM),
  (DATEDIFF(E.DATA_FIM, E.DATA_INICIO) + 1),
  IF(
  ('{$inicio}' <= E.DATA_INICIO AND '{$fim}' >= E.DATA_INICIO) 
  AND ('{$fim}' <= E.DATA_FIM), 
  (DATEDIFF('{$fim}', E.DATA_INICIO) + 1),
  IF(
  ('{$inicio}' > E.DATA_INICIO) AND ('{$fim}' >= E.DATA_FIM AND '{$inicio}' <= E.DATA_FIM),
  (DATEDIFF(E.DATA_FIM, '{$inicio}') + 1),
  IF(('{$inicio}' > E.DATA_INICIO) AND ('{$fim}' >= E.DATA_FIM AND '{$inicio}' >= E.DATA_FIM) 
  AND (E.DATA_FIM = '0000-00-00 00:00:00' OR E.DATA_FIM LIKE '%0000-00-00%'),
  (DATEDIFF('{$fim}', '{$inicio}') + 1),
  '1'
  )
  )
  )
  )
  )
  ) AS quantidade,
  COALESCE(V.valor, '0.00') AS valor,
  PL.nome AS nomeplano,
  C.empresa,
  '' AS codTuss,
  PL.tipo_medicamento
FROM
  clientes C
  INNER JOIN
    equipamentosativos E
    ON (C.idClientes = E.`PACIENTE_ID`)
  INNER JOIN
    cobrancaplanos CO
    ON (E.`COBRANCA_PLANOS_ID` = CO.`id`)
  LEFT JOIN
    custo_servicos_plano_ur CS
    ON (CS.COBRANCAPLANO_ID = CO.id
    AND CS.UR = C.empresa AND CS.STATUS = 'A')
  LEFT JOIN
    valorescobranca V
    ON (CO.`id` = V.`idCobranca`
    AND C.`convenio` = V.`idPlano`
    AND V.excluido_por IS NULL)
  INNER JOIN
    planosdesaude AS PL
    ON (C.convenio = PL.id)
WHERE
  C.`empresa` = '{$cod}'
  AND E.PRESCRICAO_AVALIACAO != 's' {$refaturareqp} $cond1
GROUP BY
  Paciente,
  CO.id,
  V.id
  
  -- PACIENTES AD SEM PRESCRIÇÃO/PEDIDO NO SISTEMA
  
  UNION SELECT
      '' AS principio,
      MAX(hsp.ID) AS cod,
      0 AS catalogo_id,
      'historico' AS TABELA_ORIGEM,
      C.nome,
      0 AS tipo,
      'P' AS flag,
      0 AS idSaida,
      hsp.PACIENTE_ID AS Paciente,
      C.convenio AS Plano,
      '0.00' AS custo,
      '' AS apresentacao,
      0 AS quantidade, 
      '0.00' AS valor, 
      PL.nome AS nomeplano, 
      C.empresa, 
      '' AS codTuss, 
      '' AS tipo_medicamento
    FROM
      historico_status_paciente AS hsp
            INNER JOIN (
                  SELECT
                         MAX(ID) AS id
                  FROM
                       historico_status_paciente
                  WHERE
                      historico_status_paciente.DATA < '{$inicio}'
                  GROUP BY PACIENTE_ID
                  ) as hsp1 ON hsp.ID = hsp1.id
      INNER JOIN
      `clientes` C ON hsp.PACIENTE_ID = C.idClientes
      INNER JOIN
        planosdesaude AS PL
        ON (C.convenio = PL.id)
    WHERE
      C.empresa = '{$cod}'
      AND STATUS_PACIENTE_ID = 4
    GROUP BY
      Paciente
      
      UNION SELECT
      '' AS principio,
      MAX(hsp.ID) AS cod,
      0 AS catalogo_id,
      'historico' AS TABELA_ORIGEM,
      C.nome,
      0 AS tipo,
      'P' AS flag,
      0 AS idSaida,
      hsp.PACIENTE_ID AS Paciente,
      C.convenio AS Plano,
      '0.00' AS custo,
      '' AS apresentacao,
      0 AS quantidade, 
      '0.00' AS valor, 
      PL.nome AS nomeplano, 
      C.empresa, 
      '' AS codTuss, 
      '' AS tipo_medicamento
    FROM
      historico_status_paciente AS hsp
      INNER JOIN
      `clientes` C ON hsp.PACIENTE_ID = C.idClientes
      INNER JOIN
        planosdesaude AS PL
        ON (C.convenio = PL.id)
    WHERE
      C.empresa = '{$cod}'
      AND hsp.DATA BETWEEN '{$inicio}' AND '{$fim}'
      AND STATUS_PACIENTE_ID = 4
      AND PACIENTE_ID NOT IN (
                         SELECT
                                PACIENTE_ID
                         FROM
                              historico_status_paciente AS hsp2
                                INNER JOIN (
                                           SELECT
                                                  MAX(ID) AS id
                                           FROM
                                                historico_status_paciente
                                           WHERE
                                               historico_status_paciente.DATA < '{$inicio}'
                                           GROUP BY PACIENTE_ID
                                           ) as hsp1 ON hsp2.ID = hsp1.id
                                INNER JOIN clientes ON hsp2.PACIENTE_ID = clientes.idClientes
                         WHERE
                             hsp2.STATUS_PACIENTE_ID = 4
                         )
    GROUP BY
      Paciente
ORDER BY
  Paciente, tipo, principio";
    }

    public static function getItensPreFaturaByPlano($cod, $inicio, $fim, $refaturarmed, $refaturarserv, $refaturareqp )
    {
        return $sql = "
SELECT
  principal.principio,
  principal.cod,
  principal.catalogo_id,
  principal.TABELA_ORIGEM,
  principal.nome,
  principal.tipo,
  principal.flag,
  principal.idSaida,
  principal.Paciente,
  principal.Plano,
  principal.custo,
  principal.apresentacao,
  SUM(principal.quantidade) AS quantidade,
  principal.valor,
  principal.nomeplano,
  principal.empresa,
  principal.codTuss,
  principal.tipo_medicamento
FROM
  (
    SELECT
      IF(B.DESC_MATERIAIS_FATURAR IS NULL, concat(B.principio, ' - ', B.apresentacao), B.DESC_MATERIAIS_FATURAR) AS principio,
      S.NUMERO_TISS AS cod,
      S.CATALOGO_ID AS catalogo_id,
      'catalogo' AS TABELA_ORIGEM,
      C.nome,
      S.tipo,
      'P' AS flag,
      S.idSaida,
      C.idClientes AS Paciente,
      C.convenio AS Plano,
      COALESCE(B.valorMedio, '0.00') AS custo,
      (
        CASE
          S.tipo
          WHEN
            '0'
          THEN
            'Medicamento'
          WHEN
            '1'
          THEN
            'Material'
          WHEN
            '3'
          THEN
            'Dieta'
        END
      )
      AS apresentacao,
      (
        CASE
          WHEN
            COUNT(*) > 1
          THEN
            SUM(S.quantidade)
          ELSE
            S.quantidade
        END
      )
      AS quantidade, COALESCE(B.VALOR_VENDA, '0.00') AS valor, PL.nome AS nomeplano, C.empresa, B.TUSS AS codTuss, PL.tipo_medicamento
    FROM
      `saida` S
      INNER JOIN
        `clientes` C
        ON (S.idCliente = C.idClientes)
      INNER JOIN
        `catalogo` B
        ON (S.CATALOGO_ID = B.ID)
      INNER JOIN
        planosdesaude AS PL
        ON (C.convenio = PL.id)
      INNER JOIN
        solicitacoes AS Sol
        ON (S.idSolicitacao = Sol.idSolicitacoes)
      LEFT JOIN
        prescricoes AS presc
        ON (Sol.id_periodica_complemento = presc.id)
    WHERE
      C.convenio = '{$cod}'

      AND IF
        (
            Sol.JUSTIFICATIVA_AVULSO_ID = 2,
            (
                presc.inicio BETWEEN '{$inicio}' AND '{$fim}'
                OR presc.fim BETWEEN '{$inicio}' AND '{$fim}'
            ),
           S.data BETWEEN '{$inicio}' AND '{$fim}'
        )
      AND S.quantidade > 0 {$refaturarmed}
      AND
      (
        C.idClientes = S.idCliente
      )
      AND S.tipo IN
      (
        0, 1, 3
      )
      AND Sol.idPrescricao = -1
      AND Sol.TIPO_SOLICITACAO = -1
    GROUP BY
      Paciente, catalogo_id
    UNION
    SELECT
      IF(B.DESC_MATERIAIS_FATURAR IS NULL, concat(B.principio, ' - ', B.apresentacao), B.DESC_MATERIAIS_FATURAR) AS principio,
      S.NUMERO_TISS AS cod,
      S.CATALOGO_ID AS catalogo_id,
      'catalogo' AS TABELA_ORIGEM,
      C.nome,
      S.tipo,
      'P' AS flag,
      S.idSaida,
      C.idClientes AS Paciente,
      C.convenio AS Plano,
      COALESCE(B.valorMedio, '0.00') AS custo,
      (
        CASE
          S.tipo
          WHEN
            '0'
          THEN
            'Medicamento'
          WHEN
            '1'
          THEN
            'Material'
          WHEN
            '3'
          THEN
            'Dieta'
        END
      )
      AS apresentacao,
      (
        CASE
          WHEN
            COUNT(*) > 1
          THEN
            SUM(S.quantidade)
          ELSE
            S.quantidade
        END
      )
      AS quantidade, COALESCE(B.VALOR_VENDA, '0.00') AS valor, PL.nome AS nomeplano, C.empresa, B.TUSS AS codTuss, PL.tipo_medicamento
    FROM
      `saida` S
      INNER JOIN
        `clientes` C
        ON (S.idCliente = C.idClientes)
      INNER JOIN
        `catalogo` B
        ON (S.CATALOGO_ID = B.ID)
      INNER JOIN
        planosdesaude AS PL
        ON (C.convenio = PL.id)
      INNER JOIN
        solicitacoes AS Sol
        ON (S.idSolicitacao = Sol.idSolicitacoes)
      INNER JOIN
        prescricoes AS P
        ON (Sol.idPrescricao = P.id)
    WHERE
      C.convenio = '{$cod}'
      AND
      (
        P.inicio BETWEEN '{$inicio}' AND '{$fim}'
        OR P.fim BETWEEN '{$inicio}' AND '{$fim}'
      )
      AND S.quantidade > 0 {$refaturarmed}
      AND
      (
        C.idClientes = S.idCliente
      )
      AND S.tipo IN
      (
        0, 1, 3
      )
    GROUP BY
      Paciente, catalogo_id
    UNION
    SELECT
      IF(B.DESC_MATERIAIS_FATURAR IS NULL, concat(B.principio, ' - ', B.apresentacao), B.DESC_MATERIAIS_FATURAR) AS principio,
      S.NUMERO_TISS AS cod,
      S.CATALOGO_ID AS catalogo_id,
      'catalogo' AS TABELA_ORIGEM,
      C.nome,
      S.tipo,
      'P' AS flag,
      S.idSaida,
      C.idClientes AS Paciente,
      C.convenio AS Plano,
      COALESCE(B.valorMedio, '0.00') AS custo,
      (
        CASE
          S.tipo
          WHEN
            '0'
          THEN
            'Medicamento'
          WHEN
            '1'
          THEN
            'Material'
          WHEN
            '3'
          THEN
            'Dieta'
        END
      )
      AS apresentacao,
      (
        CASE
          WHEN
            COUNT(*) > 1
          THEN
            SUM(S.quantidade)
          ELSE
            S.quantidade
        END
      )
      AS quantidade, COALESCE(B.VALOR_VENDA, '0.00') AS valor, PL.nome AS nomeplano, C.empresa, B.TUSS AS codTuss, PL.tipo_medicamento
    FROM
      `saida` S
      INNER JOIN
        `clientes` C
        ON (S.idCliente = C.idClientes)
      INNER JOIN
        `catalogo` B
        ON (S.CATALOGO_ID = B.ID)
      INNER JOIN
        planosdesaude AS PL
        ON (C.convenio = PL.id)
      INNER JOIN
        solicitacoes AS Sol
        ON (S.idSolicitacao = Sol.idSolicitacoes)
      INNER JOIN
        relatorio_aditivo_enf AS P
        ON (Sol.relatorio_aditivo_enf_id = P.ID)
    WHERE
      C.convenio = '{$cod}'
      AND
      (
        P.INICIO BETWEEN '{$inicio}' AND '{$fim}'
        OR P.FIM BETWEEN '{$inicio}' AND '{$fim}'
      )
      AND S.quantidade > 0 {$refaturarmed}
      AND
      (
        C.idClientes = S.idCliente
      )
      AND S.tipo IN
      (
        0, 1, 3
      )
    GROUP BY
      Paciente, catalogo_id
  )
  AS principal
GROUP BY
  Paciente, catalogo_id
UNION

SELECT
  principal.principio,
  principal.cod,
  principal.catalogo_id,
  principal.TABELA_ORIGEM,
  principal.nome,
  principal.tipo,
  principal.flag,
  principal.idSolicitacoes,
  principal.Paciente,
  principal.Plano,
  principal.custo,
  principal.apresentacao,
  SUM(principal.quantidade) AS quantidade,
  principal.valor,
  principal.nomeplano,
  principal.empresa,
  principal.codTuss,
  principal.tipo_medicamento
FROM
  (
SELECT

	IF (
					BC.DESC_COBRANCA_PLANO IS NULL,
					B.item,
					BC.DESC_COBRANCA_PLANO
	) AS principio,

	IF (
					BC.DESC_COBRANCA_PLANO IS NULL,
					B.id,
					BC.COD_COBRANCA_PLANO
	) AS cod,

	IF (
					BC.DESC_COBRANCA_PLANO IS NULL,
					B.id,
					BC.id
	) AS catalogo_id,

	IF (
					BC.DESC_COBRANCA_PLANO IS NULL,
					'cobrancaplano',
					'valorescobranca'
	) AS TABELA_ORIGEM,
	 C.nome,
	 2 AS tipo,
	 'S' AS flag,
	 P.ID AS idSolicitacoes,
	 C.idClientes AS Paciente,
	 C.convenio AS Plano,
	 COALESCE (
					CS.CUSTO,
					'0.00'
	) AS custo,
	 ('Servi&ccedil;os') AS apresentacao,
	 
	 (
SUM(
CASE
WHEN
ESP.id IN
(
2,
3,
4,
5
)
THEN
IF ( ( '{$inicio}' <= P.INICIO
AND '{$fim}' >= P.INICIO )
AND
(
'{$inicio}' <= P.FIM
AND '{$fim}' >= P.FIM
)
,
(
DATEDIFF(P.FIM, P.INICIO) + 1
)
, IF ( ( '{$inicio}' <= P.INICIO
AND '{$fim}' >= P.INICIO )
AND
(
'{$fim}' <= P.FIM
)
,
(
DATEDIFF('{$fim}', P.INICIO) + 1
)
, IF ( ('{$inicio}' > P.INICIO)
AND
(
'{$fim}' >= P.FIM
AND '{$inicio}' <= P.FIM
)
,
(
DATEDIFF(P.FIM, '{$inicio}') + 1
)
, '1' ) ) )
ELSE
IF ( ( '{$inicio}' <= P.INICIO
AND '{$fim}' >= P.INICIO )
AND
(
'{$inicio}' <= P.FIM
AND '{$fim}' >= P.FIM
)
, IF ( S.frequencia IN
(
2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
)
,
(
DATEDIFF(P.FIM, P.INICIO) + 1
)
* F.qtd, CEILING( (DATEDIFF(P.FIM, P.INICIO) + 1) / 7 ) * F.SEMANA ), IF ( ( '{$inicio}' <= P.INICIO
AND '{$fim}' >= P.INICIO )
AND
(
'{$fim}' <= P.FIM
)
, IF ( S.frequencia IN
(
2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
)
,
(
DATEDIFF('{$fim}', P.INICIO) + 1
)
* F.qtd, CEILING( ( DATEDIFF('{$fim}', P.INICIO) / 7 ) + 1 ) * F.SEMANA ), IF ( ('{$inicio}' > P.INICIO)
AND
(
'{$fim}' >= P.FIM
AND '{$inicio}' <= P.FIM
)
, IF ( S.frequencia IN
(
2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
)
, (DATEDIFF(P.FIM, '{$inicio}') + 1) * F.qtd, CEILING( ( DATEDIFF(P.FIM, '{$inicio}') / 7 ) + 1 ) * F.SEMANA ), '1' ) ) )
END
)
)
AS quantidade,
	 COALESCE (BC.valor, '0.00') AS valor,
	 PL.nome AS nomeplano,
	 C.empresa, 
	 '' AS codTuss,
	 PL.tipo_medicamento
	
	FROM
					`solicitacao_rel_aditivo_enf` S
	LEFT JOIN `frequencia` AS F ON (S.frequencia = F.id)
	INNER JOIN relatorio_aditivo_enf P ON (S.`relatorio_enf_aditivo_id` = P.ID)
	INNER JOIN `clientes` C ON (P.PACIENTE_ID = C.idClientes)
	INNER JOIN cuidadosespeciais AS ESP ON (S.CATALOGO_ID = ESP.id)
	INNER JOIN `cobrancaplanos` B ON (ESP.COBRANCAPLANO_ID = B.id)
	LEFT JOIN custo_servicos_plano_ur CS ON (
					CS.COBRANCAPLANO_ID = B.id
					AND CS.UR = C.empresa
					AND CS.STATUS = 'A'
	)
	LEFT JOIN `valorescobranca` BC ON (
					B.id = BC.idCobranca
					AND BC.idPlano = C.convenio and BC.excluido_por is null 
	)
	INNER JOIN planosdesaude AS PL ON (C.convenio = PL.id)
	WHERE
		   C.convenio = '{$cod}'
  AND
  (
( P.INICIO BETWEEN '{$inicio}' AND '{$fim}' )
    OR
    (
      P.FIM BETWEEN '{$inicio}' AND '{$fim}'
    )
  )
	AND S.tipo IN (4)
	AND S.CATALOGO_ID NOT IN (17, 18, 21, 24)
	AND BC.excluido_por is NULL
	GROUP BY
	Paciente,
	B.id,
	BC.id
	UNION
SELECT
  IF(BC.DESC_COBRANCA_PLANO IS NULL, B.item, BC.DESC_COBRANCA_PLANO)AS principio,
  IF(BC.DESC_COBRANCA_PLANO IS NULL, B.id, BC.COD_COBRANCA_PLANO) AS cod,
  IF(BC.DESC_COBRANCA_PLANO IS NULL, B.id, BC.id) AS catalogo_id,
  IF(BC.DESC_COBRANCA_PLANO IS NULL, 'cobrancaplano', 'valorescobranca') AS TABELA_ORIGEM,
  C.nome,
  S.tipo,
  'S' AS flag,
  S.idPrescricao AS idSolicitacoes,
  C.idClientes AS Paciente,
  C.convenio AS Plano,
  COALESCE(CS.CUSTO, '0.00') AS custo,
  (
    'Servi&ccedil;os'
  )
  AS apresentacao,
  (
    SUM(
    CASE
      WHEN
        ESP.id IN
        (
          2,
          3,
          4,
          5
        )
      THEN
        IF(('{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio)
        AND
        (
          '{$inicio}' <= S.fim
          AND '{$fim}' >= S.fim
        )
,
        (
          DATEDIFF(S.fim, S.inicio) + 1
        )
, IF(('{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio)
        AND
        (
          '{$fim}' <= S.fim
        )
,
        (
          DATEDIFF('{$fim}', S.inicio) + 1
        )
, IF(('{$inicio}' > S.inicio)
        AND
        (
          '{$fim}' >= S.fim
          AND '{$inicio}' <= S.fim
        )
,
        (
          DATEDIFF(S.fim, '{$inicio}') + 1
        )
, '1')))
      ELSE
        IF(('{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio)
        AND
        (
          '{$inicio}' <= S.fim
          AND '{$fim}' >= S.fim
        )
, IF(S.frequencia IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
,
        (
          DATEDIFF(S.fim, S.inicio) + 1
        )
        *F.qtd, CEILING((DATEDIFF(S.fim, S.inicio) + 1) / 7)*F.SEMANA), IF(('{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio)
        AND
        (
          '{$fim}' <= S.fim
        )
, IF(S.frequencia IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
,
        (
          DATEDIFF('{$fim}', S.inicio) + 1
        )
        *F.qtd, CEILING((DATEDIFF('{$fim}', S.inicio) / 7) + 1)*F.SEMANA), IF(('{$inicio}' > S.inicio)
        AND
        (
          '{$fim}' >= S.fim
          AND '{$inicio}' <= S.fim
        )
, IF(S.frequencia IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
, (DATEDIFF(S.fim, '{$inicio}') +1) *F.qtd, CEILING((DATEDIFF(S.fim, '{$inicio}') / 7) + 1)*F.SEMANA), '1')))
    END
)
  )
  AS quantidade, COALESCE(BC.valor, '0.00') AS valor, PL.nome AS nomeplano, C.empresa, '' AS codTuss, PL.tipo_medicamento
FROM
  `itens_prescricao` S
  LEFT JOIN
    `frequencia` AS F
    ON(S.frequencia = F.id)
  INNER JOIN
    prescricoes P
    ON (S.`idPrescricao` = P.id)
  INNER JOIN
    `clientes` C
    ON (P.paciente = C.idClientes)
  INNER JOIN
    cuidadosespeciais AS ESP
    ON (S.CATALOGO_ID = ESP.id)
  INNER JOIN
    `cobrancaplanos` B
    ON (ESP.COBRANCAPLANO_ID = B.id)
  LEFT JOIN
    custo_servicos_plano_ur CS
    ON (CS.COBRANCAPLANO_ID = B.id
    AND CS.UR = C.empresa AND CS.STATUS = 'A')
  LEFT JOIN
    `valorescobranca` BC
    ON (B.id = BC.idCobranca
    AND BC.idPlano = C.convenio
    AND BC.excluido_por IS NULL)
  INNER JOIN
    planosdesaude AS PL
    ON (C.convenio = PL.id)
WHERE
  C.convenio = '{$cod}'
  AND
  (
    P.inicio BETWEEN '{$inicio}' AND '{$fim}'
    OR P.fim BETWEEN '{$inicio}' AND '{$fim}'
  )
  AND S.tipo IN
  (
    2
  )
  AND P.Carater NOT IN
  (
    4, 7
  )
  {$refaturarserv}
  AND S.CATALOGO_ID NOT IN
  (
    17, 18, 21, 24
  )
  AND P.DESATIVADA_POR = 0
GROUP BY
  Paciente, B.id, BC.id
UNION
SELECT
  cobrancaplanos.item AS principio,
  cobrancaplanos.id AS cod,
  cobrancaplanos.id AS catalogo_id,
  'cobrancaplano' AS TABELA_ORIGEM,
  C.nome,
  2 AS tipo,
  'S' AS flag,
  remocoes.id AS idSolicitacoes,
  C.idClientes AS Paciente,
  C.convenio AS Plano,
  COALESCE(remocoes.outros_custos + remocoes.outras_despesas + remocoes.valor_medico + remocoes.valor_tecnico + remocoes.valor_condutor + remocoes.valor_enfermeiro, '0.00') AS custo,
  (
    'Servi&ccedil;os'
  )
  AS apresentacao,
  1 AS quantidade,
  COALESCE(remocoes.valor_remocao, '0.00') AS valor,
  planosdesaude.nome AS nomeplano,
  C.empresa,
  '' AS codTuss,
  planosdesaude.tipo_medicamento
FROM
  remocoes
  INNER JOIN
    clientes AS C
    ON remocoes.paciente_id = C.idClientes
  INNER JOIN
    empresas
    ON remocoes.empresa_id = empresas.id
  INNER JOIN
    planosdesaude
    ON remocoes.convenio_id = planosdesaude.id
  INNER JOIN
    cobrancaplanos
    ON remocoes.cobrancaplano_id = cobrancaplanos.id
WHERE
  remocoes.data_saida BETWEEN '{$inicio}' AND '{$fim}'
  AND remocoes.modo_cobranca = 'F'
  AND remocoes.convenio_id = '{$cod}'
  AND remocoes.finalized_by IS NOT NULL
  AND remocoes.canceled_by IS NULL
  AND remocoes.tabela_origem_item_fatura <> 'valorescobranca'
GROUP BY
  Paciente,
  cobrancaplanos.id
UNION
SELECT
  valorescobranca.DESC_COBRANCA_PLANO AS principio,
  valorescobranca.COD_COBRANCA_PLANO AS cod,
  valorescobranca.id AS catalogo_id,
  'valorescobranca' AS TABELA_ORIGEM,
  C.nome,
  2 AS tipo,
  'S' AS flag,
  remocoes.id AS idSolicitacoes,
  C.idClientes AS Paciente,
  C.convenio AS Plano,
  COALESCE(remocoes.outros_custos + remocoes.outras_despesas + remocoes.valor_medico + remocoes.valor_tecnico + remocoes.valor_condutor + remocoes.valor_enfermeiro, '0.00') AS custo,
  (
    'Servi&ccedil;os'
  )
  AS apresentacao,
  1 AS quantidade,
  COALESCE(remocoes.valor_remocao, '0.00') AS valor,
  planosdesaude.nome AS nomeplano,
  C.empresa,
  '' AS codTuss,
  planosdesaude.tipo_medicamento
FROM
  remocoes
  INNER JOIN
    clientes AS C
    ON remocoes.paciente_id = C.idClientes
  INNER JOIN
    empresas
    ON remocoes.empresa_id = empresas.id
  INNER JOIN
    planosdesaude
    ON remocoes.convenio_id = planosdesaude.id
  INNER JOIN
    valorescobranca
    ON remocoes.valorescobranca_id = valorescobranca.id
WHERE
  remocoes.data_saida BETWEEN '{$inicio}' AND '{$fim}'
  AND remocoes.modo_cobranca = 'F'
  AND remocoes.convenio_id = '{$cod}'
  AND remocoes.finalized_by IS NOT NULL
  AND remocoes.canceled_by IS NULL
  AND remocoes.tabela_origem_item_fatura = 'valorescobranca'
GROUP BY
  Paciente,
  valorescobranca.id

UNION
SELECT
  IF ( BC.DESC_COBRANCA_PLANO IS NULL, B.item, BC.DESC_COBRANCA_PLANO ) AS principio,
  IF ( BC.DESC_COBRANCA_PLANO IS NULL, B.id, BC.COD_COBRANCA_PLANO ) AS cod,
  IF ( BC.DESC_COBRANCA_PLANO IS NULL, B.id, BC.id ) AS catalogo_id,
  IF ( BC.DESC_COBRANCA_PLANO IS NULL, 'cobrancaplano', 'valorescobranca' ) AS TABELA_ORIGEM,
  C.nome,
  2 AS tipo,
  'S' AS flag,
  S.prescricao_enf_id AS idSolicitacoes,
  C.idClientes AS Paciente,
  C.convenio AS Plano,
  COALESCE(CS.CUSTO, '0.00') AS custo,
  (
    'Servi&ccedil;os'
  )
  AS apresentacao,
  (
    SUM(
    CASE
      WHEN
        ESP.id IN
        (
          2,
          3,
          4,
          5
        )
      THEN
        IF ( ( '{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio )
        AND
        (
          '{$inicio}' <= S.fim
          AND '{$fim}' >= S.fim
        )
,
        (
          DATEDIFF(S.fim, S.inicio) + 1
        )
, IF ( ( '{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio )
        AND
        (
          '{$fim}' <= S.fim
        )
,
        (
          DATEDIFF('{$fim}', S.inicio) + 1
        )
, IF ( ('{$inicio}' > S.inicio)
        AND
        (
          '{$fim}' >= S.fim
          AND '{$inicio}' <= S.fim
        )
,
        (
          DATEDIFF(S.fim, '{$inicio}') + 1
        )
, '1' ) ) )
      ELSE
        IF ( ( '{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio )
        AND
        (
          '{$inicio}' <= S.fim
          AND '{$fim}' >= S.fim
        )
, IF ( S.frequencia_id IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
,
        (
          DATEDIFF(S.fim, S.inicio) + 1
        )
        * F.qtd, CEILING( (DATEDIFF(S.fim, S.inicio) + 1) / 7 ) * F.SEMANA ), IF ( ( '{$inicio}' <= S.inicio
        AND '{$fim}' >= S.inicio )
        AND
        (
          '{$fim}' <= S.fim
        )
, IF ( S.frequencia_id IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
,
        (
          DATEDIFF('{$fim}', S.inicio) + 1
        )
        * F.qtd, CEILING( ( DATEDIFF('{$fim}', S.inicio) / 7 ) + 1 ) * F.SEMANA ), IF ( ('{$inicio}' > S.inicio)
        AND
        (
          '{$fim}' >= S.fim
          AND '{$inicio}' <= S.fim
        )
, IF ( S.frequencia_id IN
        (
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
        )
, (DATEDIFF(S.fim, '{$inicio}') + 1) * F.qtd, CEILING( ( DATEDIFF(S.fim, '{$inicio}') / 7 ) + 1 ) * F.SEMANA ), '1' ) ) )
    END
)
  )
  AS quantidade, COALESCE (BC.valor, '0.00') AS valor, PL.nome AS nomeplano, C.empresa, '' AS codTuss, PL.tipo_medicamento
FROM
  `prescricao_enf_itens_prescritos` AS S
  LEFT JOIN
    `frequencia` AS F
    ON (S.frequencia_id = F.id)
  INNER JOIN
    prescricao_enf P
    ON (S.prescricao_enf_id = P.id)
  INNER JOIN
    `clientes` C
    ON (P.cliente_id = C.idClientes)
  INNER JOIN
    prescricao_enf_cuidado AS ESP
    ON ( S.presc_enf_cuidados_id = ESP.id)
  INNER JOIN
    cuidadosespeciais AS CUI
    ON ESP.cuidado_especiais_id = CUI.id
  INNER JOIN
    `cobrancaplanos` B
    ON (CUI.COBRANCAPLANO_ID = B.id)
  LEFT JOIN
    custo_servicos_plano_ur CS
    ON (CS.COBRANCAPLANO_ID = B.id
    AND CS.UR = C.empresa AND CS.STATUS = 'A')
  LEFT JOIN
    `valorescobranca` BC
    ON ( B.id = BC.idCobranca
    AND BC.idPlano = C.convenio
    AND BC.excluido_por IS NULL )
  INNER JOIN
    planosdesaude AS PL
    ON (C.convenio = PL.id)
WHERE
  C.convenio = '{$cod}'
  AND
  (
( P.inicio BETWEEN '{$inicio}' AND '{$fim}' )
    OR
    (
      P.fim BETWEEN '{$inicio}' AND '{$fim}'
    )
  )
  AND P.cancelado_por IS NULL
  AND ESP.presc_enf_grupo_id = 7
  AND P.carater IN
  (
    8, 9
  )
GROUP BY
  Paciente, B.id, BC.id)
	AS principal
GROUP BY
  Paciente, TABELA_ORIGEM, catalogo_id
  
  UNION
SELECT DISTINCT
  IF(V.DESC_COBRANCA_PLANO IS NULL, CO.item, V.DESC_COBRANCA_PLANO)AS principio,
  IF(V.DESC_COBRANCA_PLANO IS NULL, CO.id, V.COD_COBRANCA_PLANO) AS cod,
  IF(V.DESC_COBRANCA_PLANO IS NULL, CO.id, V.id) AS catalogo_id,
  IF(V.DESC_COBRANCA_PLANO IS NULL, 'cobrancaplano', 'valorescobranca') AS TABELA_ORIGEM,
  C.nome,
  5 AS tipo,
  'N' AS flag,
  '1' AS idSolicitacoes,
  C.idClientes AS Paciente,
  C.convenio AS Plano,
  COALESCE(CS.CUSTO, '0.00') AS custo,
  (
    'Equipamentos'
  )
  AS apresentacao,
  (SUM(
  IF(
  ('{$inicio}' <= E.DATA_INICIO AND '{$fim}' >= E.DATA_INICIO) 
  AND 
  ('{$inicio}' <= E.DATA_FIM AND '{$fim}' >= E.DATA_FIM),
  (DATEDIFF(E.DATA_FIM, E.DATA_INICIO) + 1),
  IF(
  ('{$inicio}' <= E.DATA_INICIO AND '{$fim}' >= E.DATA_INICIO) 
  AND ('{$fim}' <= E.DATA_FIM), 
  (DATEDIFF('{$fim}', E.DATA_INICIO) + 1),
  IF(
  ('{$inicio}' > E.DATA_INICIO) AND ('{$fim}' >= E.DATA_FIM AND '{$inicio}' <= E.DATA_FIM),
  (DATEDIFF(E.DATA_FIM, '{$inicio}') + 1),
  IF(('{$inicio}' > E.DATA_INICIO) AND ('{$fim}' >= E.DATA_FIM AND '{$inicio}' >= E.DATA_FIM) 
  AND (E.DATA_FIM = '0000-00-00 00:00:00' OR E.DATA_FIM LIKE '%0000-00-00%'),
  (DATEDIFF('{$fim}', '{$inicio}') + 1),
  '1'
  )
  )
  )
  )
  )
  ) AS quantidade,
  COALESCE(V.valor, '0.00') AS valor,
  PL.nome AS nomeplano,
  C.empresa,
  '' AS codTuss,
  PL.tipo_medicamento
FROM
  clientes C
  INNER JOIN
    equipamentosativos E
    ON (C.idClientes = E.`PACIENTE_ID`)
  INNER JOIN
    cobrancaplanos CO
    ON (E.`COBRANCA_PLANOS_ID` = CO.`id`)
  LEFT JOIN
    custo_servicos_plano_ur CS
    ON (CS.COBRANCAPLANO_ID = CO.id
    AND CS.UR = C.empresa AND CS.STATUS = 'A')
  LEFT JOIN
    valorescobranca V
    ON (CO.`id` = V.`idCobranca`
    AND C.`convenio` = V.`idPlano`
    AND V.excluido_por IS NULL)
  INNER JOIN
    planosdesaude AS PL
    ON (C.convenio = PL.id)
WHERE
  C.`convenio` = '{$cod}'
  AND E.PRESCRICAO_AVALIACAO != 's' {$refaturareqp}
GROUP BY
  Paciente,
  CO.id,
  V.id
  
  -- PACIENTES AD SEM PRESCRIÇÃO/PEDIDO NO SISTEMA
  
  UNION SELECT
      '' AS principio,
      MAX(hsp.ID) AS cod,
      0 AS catalogo_id,
      'historico' AS TABELA_ORIGEM,
      C.nome,
      0 AS tipo,
      'P' AS flag,
      0 AS idSaida,
      hsp.PACIENTE_ID AS Paciente,
      C.convenio AS Plano,
      '0.00' AS custo,
      '' AS apresentacao,
      0 AS quantidade, 
      '0.00' AS valor, 
      PL.nome AS nomeplano, 
      C.empresa, 
      '' AS codTuss, 
      '' AS tipo_medicamento
    FROM
      historico_status_paciente AS hsp
            INNER JOIN (
                  SELECT
                         MAX(ID) AS id
                  FROM
                       historico_status_paciente
                  WHERE
                      historico_status_paciente.DATA < '{$inicio}'
                  GROUP BY PACIENTE_ID
                  ) as hsp1 ON hsp.ID = hsp1.id
      INNER JOIN
      `clientes` C ON hsp.PACIENTE_ID = C.idClientes
      INNER JOIN
        planosdesaude AS PL
        ON (C.convenio = PL.id)
    WHERE
      C.convenio = '{$cod}'
      AND STATUS_PACIENTE_ID = 4
    GROUP BY
      Paciente
      
      UNION SELECT
      '' AS principio,
      MAX(hsp.ID) AS cod,
      0 AS catalogo_id,
      'historico' AS TABELA_ORIGEM,
      C.nome,
      0 AS tipo,
      'P' AS flag,
      0 AS idSaida,
      hsp.PACIENTE_ID AS Paciente,
      C.convenio AS Plano,
      '0.00' AS custo,
      '' AS apresentacao,
      0 AS quantidade, 
      '0.00' AS valor, 
      PL.nome AS nomeplano, 
      C.empresa, 
      '' AS codTuss, 
      '' AS tipo_medicamento
    FROM
      historico_status_paciente AS hsp
      INNER JOIN
      `clientes` C ON hsp.PACIENTE_ID = C.idClientes
      INNER JOIN
        planosdesaude AS PL
        ON (C.convenio = PL.id)
    WHERE
      C.convenio = '{$cod}'
      AND hsp.DATA BETWEEN '{$inicio}' AND '{$fim}'
      AND STATUS_PACIENTE_ID = 4
      AND PACIENTE_ID NOT IN (
                         SELECT
                                PACIENTE_ID
                         FROM
                              historico_status_paciente AS hsp2
                                INNER JOIN (
                                           SELECT
                                                  MAX(ID) AS id
                                           FROM
                                                historico_status_paciente
                                           WHERE
                                               historico_status_paciente.DATA < '{$inicio}'
                                           GROUP BY PACIENTE_ID
                                           ) as hsp1 ON hsp2.ID = hsp1.id
                                INNER JOIN clientes ON hsp2.PACIENTE_ID = clientes.idClientes
                         WHERE
                             hsp2.STATUS_PACIENTE_ID = 4
                         )
    GROUP BY
      Paciente
ORDER BY
  Paciente , tipo, principio";
    }

    public static function getItensPreFaturaSalva($id)
    {
        return $sql = "
SELECT
    f.*,
     IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),B.DESC_MATERIAIS_FATURAR) AS principio,
      (
        CASE f.TIPO
            WHEN '0' THEN 'Medicamento'
            WHEN '1' THEN 'Material'
            WHEN '3' THEN 'Dieta'
         END
    ) as apresentacao,
    B.lab_desc as lab
FROM
    faturamento as f  INNER JOIN
    catalogo as B ON (f.CATALOGO_ID = B.ID)
WHERE
    f.FATURA_ID= {$id} and
    f.TIPO IN (0,1,3) and f.CANCELADO_POR is NULL
UNION
SELECT
    f.*,
    B.DESC_COBRANCA_PLANO AS principio,
   ('Servi&ccedil;os') AS apresentacao,
    '' as lab
FROM
    faturamento as f INNER JOIN
    valorescobranca B ON (f.CATALOGO_ID =B.id)
WHERE
    f.FATURA_ID= {$id} AND
    f.TIPO =2 AND f.TABELA_ORIGEM='valorescobranca'  and f.CANCELADO_POR is NULL
UNION
SELECT
    f.*,
    B.item AS principio,
    ('Servi&ccedil;os') AS apresentacao,
    '' as lab
 FROM
    faturamento as f INNER JOIN
    cobrancaplanos B ON (f.CATALOGO_ID =B.id)
 WHERE
    f.FATURA_ID= {$id} AND
    f.TIPO =2 AND f.TABELA_ORIGEM='cobrancaplano'  and f.CANCELADO_POR is NULL
UNION
  SELECT DISTINCT
      f.*,
      CO.item AS principio,
      ('Equipamentos') AS apresentacao,
    '' as lab
   FROM
      faturamento as f INNER JOIN
      cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)
   WHERE
      f.FATURA_ID= {$id} AND
      f.TIPO =5 AND f.TABELA_ORIGEM='cobrancaplano'  and f.CANCELADO_POR is NULL
UNION
  SELECT DISTINCT
      f.*,
      B.DESC_COBRANCA_PLANO AS principio,
      ('Equipamentos') AS apresentacao,
    '' as lab
   FROM
      faturamento as f INNER JOIN
      valorescobranca B ON (f.CATALOGO_ID =B.id)
   WHERE
      f.FATURA_ID= {$id} AND
      f.TIPO =5 AND f.TABELA_ORIGEM='valorescobranca'  and f.CANCELADO_POR is NULL
UNION
    SELECT DISTINCT
      f.*,
      CO.item AS principio,
      ('Gasoterapia') AS apresentacao,
    '' as lab
    FROM
      faturamento as f INNER JOIN
      cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)
    WHERE
       f.FATURA_ID= {$id} AND
       f.TIPO =6 AND f.TABELA_ORIGEM='cobrancaplano'  and f.CANCELADO_POR is NULL
UNION
    SELECT DISTINCT
      f.*,
       B.DESC_COBRANCA_PLANO AS principio,
      ('Gasoterapia') AS apresentacao,
    '' as lab
    FROM
      faturamento as f INNER JOIN
      valorescobranca B ON (f.CATALOGO_ID =B.id)
    WHERE
       f.FATURA_ID= {$id} AND
       f.TIPO =6 AND f.TABELA_ORIGEM='valorescobranca'  and f.CANCELADO_POR is NULL
    ORDER BY
       tipo,
       principio";
    }

    public static function getStatusById($id)
    {
        $sql = <<<SQL
Select
status
FROM 
fatura_status
WHERE 
id =$id
SQL;
        return parent::get($sql);



    }

    public static function getFaturasByFilters($filters)
    {
        $sql = <<<SQL
SELECT
clientes.nome as paciente,
planosdesaude.nome as plano,
fatura.ID,
fatura.`DATA`,
date_format(fatura.DATA_INICIO,'%d/%m/%Y') as inicio  ,
date_format(fatura.DATA_FIM,'%d/%m/%Y') as fim
FROM
fatura
INNER JOIN clientes ON fatura.PACIENTE_ID = clientes.idClientes
INNER JOIN planosdesaude ON fatura.PLANO_ID = planosdesaude.id
WHERE 
fatura.PACIENTE_ID = {$filters['paciente']}
AND (fatura.DATA_INICIO BETWEEN '{$filters['inicio']}' and '{$filters['fim']}'
OR 
fatura.DATA_FIM BETWEEN '{$filters['inicio']}' and '{$filters['fim']}')
AND fatura.ORCAMENTO = 0
AND fatura.STATUS = 5
SQL;

        return parent::get($sql); // TODO: Change the autogenerated stub
    }

    public static function getCabecalhoFaturaById($idFatura)
    {
        $sql = <<<SQL
        SELECT
                UPPER(c.nome) AS paciente,
                UPPER(u.nome) AS usuario,
                DATE_FORMAT(f.DATA,'%Y-%m-%d') as data_faturado,
                DATE_FORMAT(f.DATA_INICIO,'%Y-%m-%d') as inicio ,
                DATE_FORMAT(f.DATA_FIM,'%Y-%m-%d') as fim,
                (
                    CASE c.sexo
                    WHEN '0' THEN 'Masculino'
                    WHEN '1' THEN 'Feminino'
                    END
                ) AS sexo1,
                FLOOR(DATEDIFF(CURDATE(),c.`nascimento`)/365.25) AS idade,
                e.nome as empresa,
                c.`nascimento`,
                p.nome as nomeConvenio,
                p.id,
                c.*,
                NUM_MATRICULA_CONVENIO,
                f.*,
                p.tipo_medicamento

 	FROM
                fatura as f INNER JOIN
                clientes AS c on (f.PACIENTE_ID = c.idClientes) LEFT JOIN
                empresas AS e ON (e.id = f.UR) INNER JOIN
                planosdesaude as p ON (p.id = f.PLANO_ID) INNER JOIN
                usuarios as u on (f.USUARIO_ID = u.idUsuarios)
 	WHERE
                f.ID = {$idFatura}
 	ORDER BY
                c.nome DESC LIMIT 1

SQL;

  return parent::get($sql);

    }

    public static function getMotivoGlosa($id = null)
    {
        $condicao = empty($id) ? 1 : "id = {$id}";
        $sql = <<<SQL
SELECT 
*
from
 fatura_motivo_glosa
WHERE 

$condicao
SQL;

        return parent::get($sql); // TODO: Change the autogenerated stub
    }

    public static function getSQLItensFaturaOriginal($faturaId, $condicao = null)
    {
        return "SELECT
              f.*,
              fa.OBS,
              IF(B.DESC_MATERIAIS_FATURAR IS NULL,concat(B.principio,' - ',B.apresentacao),B.DESC_MATERIAIS_FATURAR) AS principio,
              (CASE f.TIPO
              WHEN '0' THEN 'Medicamento'
              WHEN '1' THEN 'Material'
               WHEN '3' THEN 'Dieta' END) as apresentacao,
               (CASE f.TIPO
              WHEN '0' THEN '3'
              WHEN '1' THEN '4'
               WHEN '3' THEN '5' END) as ordem,
              1 as cod,
              uf.UNIDADE,
              uf.ID as UNIDADE_ID,
              fa.PACIENTE_ID,
              fa.PLANO_ID,
              B.REFERENCIA as ref,
              fa.DATA_INICIO,
              f.UNIDADE as UNIDADE_FATURA
		        FROM
              fatura_itens_originais_cobrados as f  INNER JOIN
              `catalogo` as B ON (f.CATALOGO_ID = B.ID) inner join
              fatura as fa on (f.FATURA_ID=fa.ID) left join
               unidades_fatura as uf on(f.UNIDADE=uf.ID)
		        WHERE
              f.FATURA_ID= {$faturaId} and
              f.TIPO IN (0,1,3) {$condicao}
              
							
          UNION
            SELECT
              f.*,
              fa.OBS,
              vc.DESC_COBRANCA_PLANO AS principio,
              ('Servi&ccedil;os') AS apresentacao,
              1 as ordem,
              vc.COD_COBRANCA_PLANO as cod,
              uf.UNIDADE,
              uf.ID as UNIDADE_ID,
              fa.PACIENTE_ID,
              fa.PLANO_ID,
              'X' as ref,
              fa.DATA_INICIO,
              f.UNIDADE as UNIDADE_FATURA
		        FROM
              fatura_itens_originais_cobrados as f INNER JOIN
              valorescobranca as vc on (f.CATALOGO_ID =vc.id) inner join
              fatura fa on (f.FATURA_ID=fa.ID) left join
              unidades_fatura as uf on (f.UNIDADE=uf.ID)
            WHERE
              f.FATURA_ID= {$faturaId} and
              f.TIPO =2 and
              f.TABELA_ORIGEM='valorescobranca'	{$condicao}
              
           UNION
              SELECT
                f.*,
                fa.OBS,
                vc.item AS principio,
                ('Servi&ccedil;os') AS apresentacao,
                1 as ordem,
                '' as cod,
                uf.UNIDADE,
                uf.ID as UNIDADE_ID,
                fa.PACIENTE_ID,
                fa.PLANO_ID,
                'X' as ref,
                fa.DATA_INICIO,
                f.UNIDADE as UNIDADE_FATURA
             
              FROM
                fatura_itens_originais_cobrados as f INNER JOIN
                cobrancaplanos as vc on (f.CATALOGO_ID =vc.id) inner join
                fatura fa on (f.FATURA_ID=fa.ID) left join
                unidades_fatura as uf on (f.UNIDADE=uf.ID)
              WHERE
                f.FATURA_ID= {$faturaId} and
                f.TIPO =2 and
                f.TABELA_ORIGEM='cobrancaplano'	{$condicao}
                
			
            UNION
			        SELECT DISTINCT
                f.*,
                 fa.OBS,
                CO.item AS principio,

                ('Equipamentos') AS apresentacao,
                    2 as ordem,
                 '' as cod,
                 uf.UNIDADE,
                uf.ID as UNIDADE_ID,
                fa.PACIENTE_ID,
                fa.PLANO_ID,
                'X' as ref,
                fa.DATA_INICIO,
              f.UNIDADE as UNIDADE_FATURA
			        FROM
                fatura_itens_originais_cobrados as f INNER JOIN
                cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)
                inner join fatura fa on (f.FATURA_ID=fa.ID) left join
                unidades_fatura as uf on (f.UNIDADE=uf.ID)
		          WHERE
                f.FATURA_ID= {$faturaId} and
                f.TIPO =5  and
                f.TABELA_ORIGEM='cobrancaplano' {$condicao}
               

            UNION
			        SELECT DISTINCT
                f.*,
                fa.OBS,
                CO.DESC_COBRANCA_PLANO AS principio,
                ('Equipamentos') AS apresentacao,
                2 as ordem,
                CO.COD_COBRANCA_PLANO as cod,
                uf.UNIDADE,
                uf.ID as UNIDADE_ID,
                fa.PACIENTE_ID,
                fa.PLANO_ID,
                'X' as ref,
                fa.DATA_INICIO,
              f.UNIDADE as UNIDADE_FATURA
              FROM
                fatura_itens_originais_cobrados as f INNER JOIN
                valorescobranca CO ON (f.CATALOGO_ID = CO.`id`)
                inner join fatura fa on (f.FATURA_ID=fa.ID) left join
                unidades_fatura as uf on (f.UNIDADE=uf.ID)
              WHERE
                f.FATURA_ID= {$faturaId} and
                f.TIPO =5  and
                f.TABELA_ORIGEM='valorescobranca' {$condicao}
                
          	UNION
			        SELECT DISTINCT
                f.*,fa.OBS,
                CO.item AS principio,
                ('Gasoterapia') AS apresentacao,
                6 as ordem,
                '' as cod,
                uf.UNIDADE,
                uf.ID as UNIDADE_ID,
                fa.PACIENTE_ID,
                fa.PLANO_ID,
                'X' as ref,
                fa.DATA_INICIO,
              f.UNIDADE as UNIDADE_FATURA
				      FROM fatura_itens_originais_cobrados as f
                INNER JOIN cobrancaplanos CO ON (f.CATALOGO_ID = CO.`id`)
                INNER JOIN fatura fa on (f.FATURA_ID=fa.ID)
                LEFT JOIN unidades_fatura as uf on (f.UNIDADE=uf.ID)
				      WHERE
					     f.FATURA_ID= {$faturaId} and
					     f.TIPO = 6 and f.TABELA_ORIGEM ='cobrancaplano' {$condicao}
              
            UNION
              SELECT DISTINCT
                f.*,
                fa.OBS,
					      vc.DESC_COBRANCA_PLANO AS principio,
                ('Gasoterapia') AS apresentacao,
                6 as ordem,
                vc.COD_COBRANCA_PLANO as cod,
                uf.UNIDADE,
                uf.ID as UNIDADE_ID,
                fa.PACIENTE_ID,
                fa.PLANO_ID,
                'X' as ref,
                fa.DATA_INICIO,
              f.UNIDADE as UNIDADE_FATURA
				      FROM
                fatura_itens_originais_cobrados as f  inner join
                valorescobranca as vc on (vc.id = f.CATALOGO_ID) inner join
                fatura fa on (f.FATURA_ID=fa.ID) left join
                unidades_fatura as uf on(f.UNIDADE=uf.ID)
              WHERE
                f.FATURA_ID= {$faturaId} and
                f.TIPO =6  and
                f.TABELA_ORIGEM='valorescobranca' {$condicao}
               
              ORDER BY
                ordem,
                tipo,
                principio";
    }


} 

