<?php

namespace App\Models\Faturamento;

class Profissional
{

  public static function getAll()
  {
    $profissionais = array();
    $result = mysql_query(self::getSQL());
    while($profissional = mysql_fetch_array($result))
      if (isset($profissional) && !empty($profissional))
        $profissionais[] = $profissional;

    return $profissionais;
  }

  public static function getById($id)
  {
    $result = mysql_query(self::getSQL($id));
    return (isset($result) && !empty($result)) ? $profissional = mysql_fetch_array($result) : null;
  }

  public static function getByFaturaItemId($faturaItemId)
  {
    $profissionais = array();
    $sql = "SELECT * FROM tiss_profissional_fatura_item WHERE tiss_fatura_item_id = {$faturaItemId};";
    $result = mysql_query($sql);
    while($profFatura = mysql_fetch_array($result)) {
      $profissionais[] = self::getById($profFatura['tiss_profissional_id']);
    }

    return $profissionais;
  }
  
  public static function getByGuiaId($guiaId)
  {
    $profissionais = array();
    $sql = "SELECT tiss_profissional_id FROM tiss_guia WHERE id = {$guiaId};";
    $result = mysql_query($sql);
    $profSolicitante = mysql_fetch_array($result);
    $profissionalSolicitante = self::getById($profSolicitante['tiss_profissional_id']);

    return $profissionalSolicitante;
  }

  public static function getSQL($id = null)
  {
    $id = isset($id) && !empty($id) ? 'AND tiss_profissional.id = ' . $id : null;

    return "SELECT tiss_profissional.*,
              tiss_conselho_profissional.tiss_codigo as tiss_conselho_profissional,
              cbos as tiss_cbos,
              estados.tiss_codigo as tiss_uf_conselho,
              tiss_conselho_profissional.sigla as sigla_conselho_profissional
            FROM tiss_profissional
              LEFT JOIN especialidade_medica ON especialidade_medica.id = tiss_profissional.especialidade_medica_id
              INNER JOIN tiss_conselho_profissional ON tiss_conselho_profissional.id = tiss_profissional.tiss_conselho_profissional_id
              INNER JOIN estados ON estados.ID = tiss_profissional.conselho_estado_id
            WHERE 1=1
            {$id}
            ORDER BY nome_completo ASC";
  }

} 