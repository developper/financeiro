<?php

namespace App\Models\Faturamento;

class LoteFatura
{

  public static function getDetalhe($capaLote, $asMySQLObject = false)
  {
    $loteAsResource = mysql_query(self::getSQL($capaLote));
    if ($asMySQLObject)
      return $loteAsResource;
    $lote = array();
    while ($lote = mysql_fetch_array($loteAsResource))
      if (isset($lote) && !empty($lote))
        return $lote;

    return $lote;
  }

  public static function getSQL($capaLote)
  {
    return "SELECT
              f.*,
              c.nome,
              c.NUM_MATRICULA_CONVENIO,
              c.NOME_PLANO_CONVENIO,
              fa.DATA_INICIO,
              fa.DATA_FIM,
              SUM(f.VALOR_FATURA * f.QUANTIDADE + ((f.DESCONTO_ACRESCIMO/100) * (f.VALOR_FATURA * f.QUANTIDADE))) as total
            FROM faturamento as f
              INNER JOIN clientes as c ON (f.PACIENTE_ID = c.idClientes)
              INNER JOIN fatura as fa ON (f.FATURA_ID = fa.ID)
            WHERE fa.CAPA_LOTE= '{$capaLote}'
              AND f.CANCELADO_POR IS NULL GROUP BY FATURA_ID
            ORDER BY PACIENTE_ID, DATA_INICIO";
  }


} 