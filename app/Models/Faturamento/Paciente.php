<?php

namespace App\Models\Faturamento;

class Paciente
{

  public static function getById($id)
  {
    $result = mysql_query(self::getSQL($id));
    return (isset($result) && !empty($result)) ? mysql_fetch_array($result) : null;
  }

  public static function getSQL($id = null)
  {
    $id = isset($id) && !empty($id) ? 'WHERE 1=1 AND clientes.idClientes = ' . $id : null;
    return "SELECT *
            FROM clientes
            {$id}
            ORDER BY nome ASC";
  }

} 