<?php

namespace App\Models\Faturamento;

class TipoSaida
{

  public static function getById($id)
  {
    $result = mysql_query(self::getSQL($id));
    return (isset($result) && !empty($result)) ? mysql_fetch_array($result) : null;
  }

  public static function getAll()
  {
    $lista = array();
    $result = mysql_query(self::getSQL());
    while ($row = mysql_fetch_array($result))
      $lista[] = $row;
    return $lista;
  }

  public static function getSQL($id = null)
  {
    $id = isset($id) && !empty($id) ? 'WHERE 1=1 AND id = "' . $id . '"' : null;
    return "SELECT * FROM tiss_tipo_saida {$id} ORDER BY tiss_codigo";
  }

} 