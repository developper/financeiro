<?php

namespace App\Models\Faturamento;

use App\Models\AbstractModel,
    App\Models\DAOInterface;
use App\Models\Faturamento\Operadora;

class GuiaSADT extends AbstractModel implements DAOInterface
{

  protected $data;

  public function __construct(Array $data)
  {
    $this->data = $data;
  }

  public static function getAll()
  {
    return parent::get(self::getSQL());
  }

  public static function getByCapaLote($capaLote)
  {
    return parent::get(self::getSQL($capaLote));
  }

  public static function getLastLoteId()
  {
    return current(parent::get("SELECT * FROM tiss_lote ORDER BY registration DESC LIMIT 1"));
  }

  public static function fillAll()
  {
    return function ($guia){

      $guia['faturas'] = self::getFaturasByGuiaId($guia['guiaId']);

      if (empty($guia['faturas']))
        throw new \Exception('Guia SADT Inválida: Não existe faturas associadas à ela.');

      foreach ($guia['faturas'] as &$fatura) {
        $fatura['paciente']   = Paciente::getById($fatura['clientes_id']);
        $fatura['itens']      = array_map(function($item) {
          $item['profissional'] = Profissional::getByFaturaItemId($item['id']);
          $item['tissTabela'] = $tabela = TabelaTiss::getById($item['tiss_tabela_id']);
          return $item;
        }, self::getItensFatura($fatura['faturaId']));

        if (isset($fatura['cid10_codigo']) && !empty($fatura['cid10_codigo']))
          $fatura['cid10'] = Cid10::getById($fatura['cid10_codigo']);

        $fatura['tipoAtendimento'] = TipoAtendimento::getById($fatura['tiss_tipo_atendimento_id']);
        $fatura['indicadorAcidente'] = IndicadorAcidente::getById($fatura['tiss_indicador_acidente_id']);
        $fatura['tipoSaida'] = TipoSaida::getById($fatura['tiss_tipo_saida_id']);
      }

      if ($guia['tipo_guia'] === Guia::SP_SADT) {
      } elseif ($guia['tipo_guia'] === Guia::RESUMO_INTERNACAO) {
        $guia['tipoInternacao'] = TipoInternacao::getById($guia['tiss_tipo_internacao_id']);
        $guia['tipoAcomodacao'] = Acomodacao::getById($guia['tiss_acomodacao_id']);
        $guia['regimeInternacao'] = RegimeInternacao::getById($guia['tiss_regime_internacao_id']);
        $guia['motivoSaidaInternacao'] = MotivoSaidaInternacao::getById($guia['tiss_motivo_saida_internacao_id']);
        $guia['tipoFaturamento'] = TipoFaturamento::getById($guia['tiss_tipo_faturamento_id']);
        $guia['indicadorAcidente'] = IndicadorAcidente::getById($guia['tiss_indicador_acidente_id']);
      }

      $guia['contratado']               = Contratado::getById($guia['tiss_contratado_id']);
      $guia['identificacaoPrestador']   = 'CNPJ';
      $guia['codigoNaOperadora'] = $guia['contratado']['cnpj'];


      if($guia['tipo_identificacao'] == 'num_contratado') {
          $guia['identificacaoPrestador'] = 'codigoPrestadorNaOperadora';
          $operadora = Operadora::getByPlanoID($guia['planosdesaude_id']);
          $codigoOperadora = Operadora::getCodigoNaOperadora($guia['contratado']['empresa_id'],$operadora['OPERADORAS_ID']);
          $guia['codigoNaOperadora'] =$codigoOperadora['codigo'];
      }
      $guia['profissionalSolicitante']  = Profissional::getByGuiaId($guia['guiaId']);
      $guia['planoSaude']               = PlanoSaude::getById($guia['planosdesaude_id']);

      return $guia;
    };
  }

  public static function getByGuiaSadtId($guiaId)
  {
    $sql = "SELECT
              tiss_guia.id as guiaId,
              tiss_guia.tipo_guia as tipoGuia,
              tiss_guia.tipo_guia,
              tiss_guia_sadt.id as guiaSadtId,
              tiss_guia_sadt.id as guiaSpSadtId,
              tiss_guia_sadt.tiss_guia_id,
              tiss_guia_sadt.tiss_contratado_id,
              tiss_guia.planosdesaude_id,
              tiss_guia_sadt.data_emissao_guia,
              tiss_guia_sadt.numero_guia_prestador,
              tiss_guia.numero_guia_operadora,
              tiss_guia.data_autorizacao,
              tiss_guia.numero_carteira,
              tiss_guia.nome_beneficiario,
              tiss_guia.clientes_id,
              tiss_guia_sadt.senha_autorizacao,
              tiss_guia.senha,
              tiss_guia.validade_senha,
              tiss_guia_sadt.numero_guia_principal,
              tiss_guia_sadt.numero_guia,
              tiss_fatura.id as fatura_id,
              tiss_guia_resumo_internacao.*
            FROM tiss_guia
              LEFT JOIN tiss_guia_sadt ON tiss_guia_sadt.tiss_guia_id = tiss_guia.id
              LEFT JOIN tiss_guia_resumo_internacao ON tiss_guia_resumo_internacao.tiss_guia_id = tiss_guia.id
              LEFT JOIN tiss_fatura ON tiss_fatura.tiss_guia_id = tiss_guia.id
            WHERE tiss_guia.id = {$guiaId};";
    return parent::get($sql);
  }

  public static function getLoteIdByCapaLote($capaLote)
  {
    return parent::get("SELECT *
                        FROM tiss_lote
                        WHERE tiss_lote.numero_lote = '{$capaLote}'
                        ORDER BY registration DESC LIMIT 1;");
  }

  public static function getSpSadtByLoteId($loteId)
  {
    return parent::get("SELECT tiss_lote.*,
                          tiss_guia.id as guiaId,
                          tiss_guia.tipo_guia,
                          tiss_guia_sadt.id as guiaSadtId,
                          tiss_guia_sadt.tiss_contratado_id,
                          tiss_guia.planosdesaude_id,
                          tiss_guia_sadt.data_emissao_guia,
                          tiss_guia_sadt.numero_guia_prestador,
                          tiss_guia_sadt.numero_guia_operadora,
                          tiss_guia_sadt.data_autorizacao,
                          tiss_guia_sadt.senha_autorizacao,
                          tiss_guia_sadt.validade_senha,
                          tiss_guia_sadt.numero_guia_principal,
                          tiss_guia_sadt.numero_guia
                        FROM tiss_lote
                          INNER JOIN tiss_guia ON tiss_guia.tiss_lote_id = tiss_lote.id
                          LEFT JOIN tiss_guia_sadt ON tiss_guia_sadt.tiss_guia_id = tiss_guia.id
                          LEFT JOIN tiss_guia_resumo_internacao ON tiss_guia_resumo_internacao.tiss_guia_id = tiss_guia.id                          
                        WHERE tiss_lote.id = {$loteId};");
  }

  public static function getGuiasByLoteId($loteId)
  {

    $sql = "SELECT
              tiss_lote.id,
              tiss_lote.numero_lote,
              tiss_lote.registration,
              tiss_guia.tiss_lote_id,
              tiss_guia.tiss_contratado_id,
              tiss_guia.tiss_profissional_id,
              tiss_guia.clientes_id,
              tiss_guia.numero_carteira,
              tiss_guia.tipo_guia,
              tiss_guia.nome_beneficiario,
              tiss_guia.numero_cns,
              tiss_guia.nome_plano,
              tiss_guia.numero_guia_operadora,
              tiss_guia.data_autorizacao,
              tiss_guia.senha,
              tiss_guia.validade_senha,
              tiss_guia.id as guiaId,
              tiss_guia.planosdesaude_id,
              tiss_guia_sadt.id as guiaSpSadtId,
              tiss_guia_sadt.numero_guia,
              tiss_guia_sadt.numero_guia_principal,
              tiss_guia_sadt.data_emissao_guia,
              tiss_guia_sadt.numero_guia_prestador,
              tiss_guia_resumo_internacao.tiss_tipo_internacao_id,
              tiss_guia_resumo_internacao.tiss_guia_id,
              tiss_guia_resumo_internacao.tiss_acomodacao_id,
              tiss_guia_resumo_internacao.data_fim_faturamento,
              tiss_guia_resumo_internacao.tiss_regime_internacao_id,
              tiss_guia_resumo_internacao.tiss_motivo_saida_internacao_id,
              tiss_guia_resumo_internacao.tiss_tipo_faturamento_id,
              tiss_guia_resumo_internacao.tiss_indicador_acidente_id,
              tiss_guia_resumo_internacao.carater_atendimento,
              tiss_guia_resumo_internacao.data_hora_saida_internacao,
              tiss_guia_resumo_internacao.data_inicio_faturamento,
              tiss_guia.tipo_identificacao
            FROM tiss_lote
              INNER JOIN tiss_guia ON tiss_guia.tiss_lote_id = tiss_lote.id
              LEFT JOIN tiss_guia_sadt ON tiss_guia_sadt.tiss_guia_id = tiss_guia.id
              LEFT JOIN tiss_guia_resumo_internacao ON tiss_guia_resumo_internacao.tiss_guia_id = tiss_guia.id
            WHERE tiss_lote.id = {$loteId}";

    return parent::get($sql);
  }

  public static function getFaturasByGuiaId($guiaId)
  {
    return parent::get(
        "SELECT tiss_fatura_spsadt.*,
          tiss_tipo_atendimento.tiss_codigo as tiss_tipo_atendimento,
          tiss_indicador_acidente.tiss_codigo as tiss_indicador_acidente,
          tiss_fatura.id as faturaId
         FROM tiss_fatura
          LEFT JOIN tiss_fatura_spsadt ON tiss_fatura_spsadt.tiss_fatura_id = tiss_fatura.id
          LEFT JOIN tiss_tipo_atendimento ON tiss_tipo_atendimento.id = tiss_fatura_spsadt.tiss_tipo_atendimento_id
          LEFT JOIN tiss_indicador_acidente ON tiss_indicador_acidente.id = tiss_fatura_spsadt.tiss_indicador_acidente_id
         WHERE tiss_fatura.tiss_guia_id = '{$guiaId}';"
    );
  }

  public static function getItensFatura($tissFaturaId)
  {
    return parent::get(
        "SELECT tiss_fatura_item.*, unidades_fatura.tiss_codigo as tiss_unidade_medida_codigo,
           tiss_tabela.tiss_codigo as tiss_tabela, tiss_outras_despesas_tipo.tiss_codigo as codigoDespesa
         FROM tiss_fatura_item
          LEFT JOIN unidades_fatura ON unidades_fatura.id = tiss_fatura_item.unidades_fatura_id
          LEFT JOIN tiss_tabela ON tiss_tabela.id = tiss_fatura_item.tiss_tabela_id
          LEFT JOIN tiss_outras_despesas_tipo ON tiss_outras_despesas_tipo.id = tiss_fatura_item.tiss_outras_despesas_tipo_id
         WHERE tiss_fatura_id = '{$tissFaturaId}';");
  }

  public static function getSQL($id = null)
  {
    $capaLote = isset($id) && !empty($id) ? 'WHERE 1=1 AND numero_lote = "' . $id . '"' : null;
    return "SELECT *
            FROM tiss_guia_sadt
            {$capaLote}
            ORDER BY registration DESC
            LIMIT 1";
  }

  public function save()
  {
    $queries = $this->getQueries();
    if (isset($queries) && !empty($queries)) {
      mysql_query('begin');
      foreach ($queries as $querie) {
        $r = mysql_query($querie);
        if (! $r) {
          mysql_query('rollback');
          return false;
        }
      }
      return mysql_query('commit');
    }
  }

  public static function saveXML($loteId, $plantext, $header, $epilogo, $version = null)
  {
    $sql = "INSERT INTO tiss_xml(tiss_versao, tiss_lote_id, header, plan_text, epilogo) VALUES('{$version}', {$loteId}, '{$header}', '$plantext', '$epilogo');";
    mysql_query($sql);
    return mysql_insert_id();
  }

  public static function getXMLByGuiaId($loteId)
  {
    $sql = "SELECT * FROM tiss_xml WHERE tiss_lote_id = '{$loteId}' ORDER BY registration DESC LIMIT 1";
    return parent::get($sql);
  }

  public static function getXMLById($xmlId)
  {
    $sql = "SELECT * FROM tiss_xml WHERE id = {$xmlId}";
    return parent::get($sql);
  }

  private function getQueries()
  {

    if (! isset($this->data['guiaxml']))
      return false;

    $guiaxml = $this->data['guiaxml'];
    $queries[] = "INSERT INTO `tiss_lote` (`numero_lote`)
                  VALUES
                    ('{$guiaxml['numeroLote']}');";

    foreach ($guiaxml['faturas'] as $faturaId => $fatura) {

      $nomePlano            =  isset($fatura['dadosBeneficiario']['nomePlano']) && !empty($fatura['dadosBeneficiario']['nomePlano']) ?
                               "'{$fatura['dadosBeneficiario']['nomePlano']}'" :
                               'null';
      $validadeSenha        =  isset($fatura['dadosAutorizacao']['validadeSenha']) && !empty($fatura['dadosAutorizacao']['validadeSenha']) ?
                               "'{$fatura['dadosAutorizacao']['validadeSenha']}'" :
                               'null';
      $numeroGuiaOperadora  =  isset($fatura['idGuiaSADTSP']['numeroGuiaOperadora']) && !empty($fatura['idGuiaSADTSP']['numeroGuiaOperadora']) ?
                               "'{$fatura['idGuiaSADTSP']['numeroGuiaOperadora']}'" :
                               'null';

      $queries[] = "INSERT INTO `tiss_guia` (`tiss_lote_id`, `tiss_contratado_id`, `tiss_profissional_id`, `clientes_id`, `numero_carteira`, `nome_beneficiario`, `nome_plano`, `tipo_guia`, `planosdesaude_id`, `numero_guia_operadora`,`data_autorizacao`, `senha`, `validade_senha`,`tipo_identificacao`)
                    VALUES
                      ((SELECT id FROM tiss_lote where numero_lote = '{$guiaxml['numeroLote']}' ORDER BY registration DESC LIMIT 1), {$guiaxml['contratado']}, {$guiaxml['profissional']}, {$fatura['dadosBeneficiario']['pacienteId']}, '{$fatura['dadosBeneficiario']['numeroCarteira']}', '{$fatura['dadosBeneficiario']['nomeBeneficiario']}', {$nomePlano}, '{$guiaxml['tipoGuia']}', {$guiaxml['destino']}, {$numeroGuiaOperadora}, '{$fatura['dadosAutorizacao']['dataAutorizacao']}', '{$fatura['dadosAutorizacao']['senhaAutorizacao']}', {$validadeSenha},'{$guiaxml['tipo_identificacao']}');";


      $queries[] = "INSERT INTO `tiss_fatura` (`tiss_guia_id`, `fatura_id`, `clientes_id`, `numero_carteira`, `nome_beneficiario`)
                    VALUES
                      ((SELECT id FROM tiss_guia ORDER BY id DESC LIMIT 1), {$faturaId}, {$fatura['dadosBeneficiario']['pacienteId']}, '{$fatura['dadosBeneficiario']['numeroCarteira']}', '{$fatura['dadosBeneficiario']['nomeBeneficiario']}');"
                    ;

      if (! isset($guiaxml['tipoGuia']) || $guiaxml['tipoGuia'] == Guia::SP_SADT) {

        $queries[] = "INSERT INTO `tiss_guia_sadt` (`tiss_guia_id`, `tiss_lote_id`, `numero_lote`, `tiss_contratado_id`, `planosdesaude_id`, `numero_guia_operadora`, `data_emissao_guia`, `data_autorizacao`, `senha_autorizacao`, `validade_senha`)
                      VALUES
                        ((SELECT id FROM tiss_guia ORDER BY id DESC LIMIT 1), (SELECT id FROM tiss_lote ORDER BY registration DESC LIMIT 1), '{$guiaxml['numeroLote']}', {$guiaxml['contratado']}, {$guiaxml['destino']},{$numeroGuiaOperadora}, NOW(), '{$fatura['dadosAutorizacao']['dataAutorizacao']}', '{$fatura['dadosAutorizacao']['senhaAutorizacao']}', '{$fatura['dadosAutorizacao']['validadeSenha']}');";

        $queries[] = "INSERT INTO `tiss_fatura_spsadt` (`tiss_guia_id`, `tiss_fatura_id`, `tiss_guia_sadt_id`, `nome_plano`, `data_atendimento`, `cid10_codigo`, `carater_atendimento`, `tiss_carater_atendimento_id`, `tiss_tipo_atendimento_id`, `tiss_indicador_acidente_id`, `tiss_tipo_saida_id`, `tipo_doenca`)
                      VALUES
                      ((SELECT id FROM tiss_guia ORDER BY id DESC LIMIT 1), (SELECT id FROM tiss_fatura ORDER BY id DESC LIMIT 1), (SELECT `id` FROM `tiss_guia_sadt` ORDER BY id DESC LIMIT 1), {$nomePlano}, '{$fatura['atendimento']['data']}', '{$fatura['atendimento']['cid']}', '{$fatura['atendimento']['carater']}', (SELECT tiss_codigo FROM tiss_carater_atendimento WHERE sigla = '{$fatura['atendimento']['carater']}' LIMIT 1), {$fatura['atendimento']['atendimento']}, {$fatura['atendimento']['indicadorAcidente']}, {$fatura['atendimento']['tipoSaida']}, '{$fatura['atendimento']['tipoDoenca']}');";

      } elseif ($guiaxml['tipoGuia'] == Guia::RESUMO_INTERNACAO) {

        $dataInicioFaturamento = \DateTime::createFromFormat('Y-m-d', $fatura['dataInicioFaturamento']);
        $dataFimFaturamento    = \DateTime::createFromFormat('Y-m-d', $fatura['dataFimFaturamento']);

        $queries[] = "INSERT INTO `tiss_guia_resumo_internacao` (`tiss_guia_id`, `tiss_tipo_internacao_id`, `tiss_acomodacao_id`, `tiss_regime_internacao_id`, `tiss_motivo_saida_internacao_id`, `tiss_tipo_faturamento_id`, `tiss_indicador_acidente_id`, `carater_atendimento`, `data_hora_saida_internacao`, `data_inicio_faturamento`, `data_fim_faturamento`)
                      VALUES
                        ((SELECT id FROM tiss_guia ORDER BY id DESC LIMIT 1), {$fatura['tipoInternacao']}, {$fatura['acomodacao']}, {$fatura['regimeInternacao']}, {$fatura['motivoSaidaInternacao']}, {$fatura['tipoFaturamento']}, {$fatura['atendimento']['indicadorAcidente']}, '{$fatura['internacao']['carater']}', NULL, '{$dataInicioFaturamento->format('Y-m-d')}', '{$dataFimFaturamento->format('Y-m-d')}');";
      }

      if (isset($fatura['procedimentos']) || isset($fatura['outrasDespesas'])) {
        $fatura['procedimentos'] = isset($fatura['procedimentos']) ? $fatura['procedimentos'] : [];

        $itens = isset($fatura['outrasDespesas']) && !empty($fatura['outrasDespesas']) ?
                  array_merge($fatura['procedimentos'], $fatura['outrasDespesas']) :
                  $fatura['procedimentos'];        

        foreach ($itens as $item) {
          $tissOutrasDespesasTipoId = isset($item['tipo']) && !empty($item['tipo']) ? $item['tipo'] : 0;
          $tissOutrasDespesasUnidadeMedidaId = isset($item['unidade-medida']) && !empty($item['unidade-medida']) ? $item['unidade-medida'] : 0;
          $queries[] = "INSERT INTO `tiss_fatura_item` (`tiss_fatura_id`, `tiss_tabela_id`, `tiss_outras_despesas_tipo_id`, `unidades_fatura_id`, `data`, `codigo`, `descricao`, `quantidade`, `valor_unitario`)
                        VALUES
                          ((SELECT `id` FROM `tiss_fatura` ORDER BY id DESC LIMIT 1), {$item['tabela']}, {$tissOutrasDespesasTipoId}, '{$tissOutrasDespesasUnidadeMedidaId}', '{$item['data']}', '{$item['codigo']}', '{$item['descricao']}', {$item['quantidade']}, {$item['valorUnitario']});";
        
            if (isset($item['profexecutante']) && !empty($item['profexecutante']))
              $queries[] = "INSERT INTO tiss_profissional_fatura_item(`tiss_profissional_id`, `tiss_fatura_item_id`)
                            VALUES({$item['profexecutante']}, (SELECT id FROM tiss_fatura_item ORDER BY id DESC LIMIT 1));";                  
        }
      }
    }
    return $queries;
  }

} 