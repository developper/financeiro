<?php

namespace App\Models\FileSystem;

class Directory
{

    protected $path;

    public function __construct($path)
    {
        try {
            if (is_dir($path)) {
                $this->path = $path;
            } else {
                throw new \Exception('Diretório inválido');
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function removeFilesIntoDirectory($extensions = [])
    {
        $num_objects = 0;
        if (is_dir($this->path)) {
            $objects = scandir($this->path);
            unset($objects[0]); // .
            unset($objects[1]); // ..
            if($objects[2] == '.gitignore' || $objects[2] == '.gitkeep') {
                unset($objects[2]);
            }
            $num_objects = count($objects);
            if(count($extensions) > 0) {
                if($num_objects > 0) {
                    chmod($this->path, 0777);
                    foreach ($extensions as $extension) {
                        $fullPath = $this->path . "/*." . $extension;
                        $status = array_map('unlink', glob($fullPath));
                    }
                }
            } else {
                foreach ($objects as $object) {
                    if ($object != "." && $object != "..") {
                        unlink($this->path . "/" . $object);
                    }
                }
            }
        }

        return $num_objects;
    }
}