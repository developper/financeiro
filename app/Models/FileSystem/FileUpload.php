<?php

namespace App\Models\FileSystem;

use Sirius\Upload\Handler as UploadHandler;

class FileUpload
{

    protected $folder;
    protected $handler;
    protected $filepaths = array();

    public function __construct($folder, $filesize = '2M', $extensions = ['jpg', 'jpeg', 'png'])
    {
        $this->folder  = $folder;
        $this->handler = new UploadHandler($this->getFullPath());
        $this->addRules($filesize, $extensions);
    }

    public function turnOnPrefix()
    {
        $this->handler->setPrefix(function($fileName) {
            return time() . '_';
        });
    }

    protected function addRules($filesize, $extensions)
    {
        // validation rules
        $extensionsLabel = implode(', ', $extensions);
        $this->handler->addRule('extension', ['allowed' => $extensions], "{label} deve ser estar dentro das extensões permitidas ($extensionsLabel)", 'O arquivo');
        $this->handler->addRule('size', ['max' => $filesize], '{label} deve ser abaixo de {max}', 'O arquivo');
    }

    public function process($files)
    {
        $handlerResults = $this->handler->process($files);

        if ($handlerResults->isValid()) {
            $handlerResults->confirm();
            return array($handlerResults, []);
        }

        return array($handlerResults, $handlerResults->getMessages());
    }

    public function getSql($handlerResults, $uploaded_by)
    {
        $sqls = array();
        foreach ($handlerResults as $file) {
            $path = $this->getBaseDirectory() . $file->name;
            $fullpath = $this->getFullPath() . $file->name;
            $sqls[] = $this->getInsert($fullpath, $path, $file->type, $file->size, $uploaded_by);
        }
        return $sqls;
    }

    public function getSqlOnly($handlerResults, $uploaded_by)
    {
        $sqls = array();
        $path = $this->getBaseDirectory() . $handlerResults->name;
        $fullpath = $this->getFullPath() . $handlerResults->name;
        $sqls[] = $this->getInsert($fullpath, $path, $handlerResults->type, $handlerResults->size, $uploaded_by);
        return $sqls;
    }

    public function getInsert($fullpath, $path, $mimetype, $size_bytes, $uploaded_by)
    {
        return <<<SQL
INSERT INTO files_uploaded(`full_path`, `path`, `mimetype`, `size_bytes`, `uploaded_by`)VALUES("{$fullpath}", "{$path}", "{$mimetype}", {$size_bytes}, {$uploaded_by});
SQL;
    }

    public function getSqlById($id)
    {
        return <<<SQL
SELECT * FROM files_uploaded WHERE id = {$id}
SQL;

    }

    public function getBaseDirectory()
    {
        return $this->folder;
    }

    public function getFullPath($levels = '/../../../')
    {
        return __DIR__ . $levels . $this->folder;
    }

    public function getPublicUrl()
    {
    }

}