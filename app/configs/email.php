<?php
$configEmail = array(
    'altBody' => 'Para ver essa mensagem, use um visualizador de email compat&iacute;vel com HTML',
    'msgHTML' => '<p>Encontra-se dispon&iacute;vel um(a) novo(a) <b>%s</b> para <b>%s</b><br/> feito por <b>%s</b>.
                    <br>
                    Empresa: <b>%s</b>.',
    'addAddress' => array(
        'email' => 'saad@mederi.com.br',
        'nome'  => 'Setor de Acompanhamento a Atenção Domiciliar'
    ),
    // A partir de 03/03/2017, o grupo da Auditoria também recebe emails
    // de avaliação médica e avaliação de enfermagem
    'grupo-auditoria' => array(
        'email' => 'auditoria@mederi.com.br',
        'nome'  => 'Auditoria Interna'
    ),
    'secretaria-fsa' => array(
        'email' => 'secretaria.feira@mederi.com.br',
        'nome'  => 'Secretária UR FSA'
    ),
    'smtp' => array(
        'host'      => 'smtp.mederi.com.br',
       // 'host'      => 'email-ssl.com.br',
        'port'      => 587,
        'username'  => 'avisos@mederi.com.br',
        'password'  => '2017avisosistema',
        //'secure' => "SSL"
    ),
    'smtp-gmail'  => array(
        'host'      => 'ssl://smtp.googlemail.com',
        'port'      => 465,
        'username'  => 'avisosmederi@gmail.com',
        'password'  => 'mederi@2014'
    ),
    'from-gmail'  => array(
        'email'     => 'avisosmederi@gmail.com',
        'nome'      => 'Avisos do Sistema'
    ),
    'from' => array(
        'email' => 'avisos@mederi.com.br',
        'nome'  => 'Avisos do Sistema'
    ),
    'lista' => array(
        'origem' => array(
            'enfermagem' => array(
                1  => 'Ficha de Avaliação de Enfermagem',
                2  => 'Relatório de alta de Enfermagem',
                3  => 'Relatório Aditivo de Enfermagem',
                4  => 'Relatório de Prorrogação de Enfermagem',
                5  => 'Relatório de Visita Semanal de Enfermagem',
                11 => 'Prescrição de Curativo',
                13 => 'Relatório Deflagrado de Enfermagem',
                14 => 'Itens Negados Auditoria - Sistema de Gerenciamento da Mederi',
                15 => 'Pendência na Auditoria - Sistema de Gerenciamento da Mederi',
            ),
            'medico' => array(
                6  => 'Ficha de Avaliação Médica',
                7  => 'Ficha de Evolução Médica',
                8  => 'Relatório Aditivo Médico',
                9  => 'Relatório de Prorrogação Médico',
                10 => 'Relatório Deflagrado Médico',
                12 => 'Relatório de Alta Médica',
            ),
            'exame' => array(
                'eletivo' => 'Solicitação de exame Eletivo',
                'emergencial' => 'Solicitação de exame Emergencial'),
        ),
        'emails' => array(
            'enfermagem' => array(
                1 => array('email' => 'urfsa@mederi.com.br', 'nome' => 'Unidade - Feira de Santana'),
                11 => array('email' => 'urfsa@mederi.com.br', 'nome' => 'Unidade  - Feira de Santana'),
                2 => array('email' => 'supenfermagem.ssa@mederi.com.br', 'nome' => 'Supervisão de Enfermagem - Salvador'),
                4 => array('email' => 'coordenf.sulbahia@mederi.com.br', 'nome' => 'Coordenação de Enfermagem - Sul/Bahia'),
                5 => array('email' => 'karlaenf.alagoinhas@mederi.com.br', 'nome' => 'Coordenação de Enfermagem - Alagoinhas'),
                7 => array('email' => 'coordenf.sudoeste@mederi.com.br', 'nome' => 'Coordenação de Enfermagem - Sudoeste'),
                9 => array('email' => 'coordenf.brasilia@mederi.com.br', 'nome' => 'Coordenação de Enfermagem - Brasília'),
                12 => array('email' => 'urfsa@mederi.com.br', 'nome' => 'Unidade  - Feira de Santana'),
                13 => array('email' => 'urfsa@mederi.com.br', 'nome' => 'Unidade  - Feira de Santana'),
                14 => array('email' => 'urfsa@mederi.com.br', 'nome' => 'Unidade  - Feira de Santana')


            ),
            'medico' => array(
                1 => array('email' => 'givanilto@mederi.com.br', 'nome' => 'Coordenação Médica - Feira de Santana'),
                11 => array('email' => 'givanilto@mederi.com.br', 'nome' => 'Coordenação Médica - Feira de Santana'),
                2 => array('email' => 'coordmed.salvador@mederi.com.br', 'nome' => 'Coordenação Médica - Salvador'),
                4 => array('email' => 'coordmed.sulbahia@mederi.com.br', 'nome' => 'Coordenação Médica - Sul/Bahia'),
                5 => array('email' => 'givanilto@mederi.com.br', 'nome' => 'Coordenação Médica - Alagoinhas'),
                7 => array('email' => 'coordmed.sudoeste@mederi.com.br', 'nome' => 'Coordenação Médica - Sudoeste'),
                9 => array('email' => 'coordmed.brasilia@mederi.com.br', 'nome' => 'Coordenação Médica - Brasília'),
                12 => array('email' => 'urfsa@mederi.com.br', 'nome' => 'Unidade  - Feira de Santana'),
                13 => array('email' => 'urfsa@mederi.com.br', 'nome' => 'Unidade  - Feira de Santana'),
                14 => array('email' => 'urfsa@mederi.com.br', 'nome' => 'Unidade  - Feira de Santana')
            ),
            'logistica' => array(
                1 => array('email' => 'farmacia@mederi.com.br', 'nome' => 'Logística - Feira de Santana'),
                11 => array('email' => 'farmacia@mederi.com.br', 'nome' => 'Logística - Feira de Santana'),
                2 => array('email' => 'farmacia.salvador@mederi.com.br', 'nome' => 'Logística - Salvador-RMS'),
                4 => array('email' => 'farmacia.sulbahia@mederi.com.br', 'nome' => 'Logística - Sul-Bahia'),
                5 => array('email' => 'farmacia.alagoinhas@mederi.com.br', 'nome' => 'Logística - Alagoinhas'),
                7 => array('email' => 'farmacia.sudoeste@mederi.com.br', 'nome' => 'Logística - Sudoeste'),
                12 => array('email' => 'farmacia@mederi.com.br', 'nome' => 'Logística - Feira de Santana'),
                13 => array('email' => 'farmacia@mederi.com.br', 'nome' => 'Logística - Feira de Santana'),
                14 => array('email' => 'farmacia@mederi.com.br', 'nome' => 'Logística - Feira de Santana')

            ),
            'enfermeira_administrativa' => array(
               // 11 => array('email' => 'enfadm.fsa@mederi.com.br','nome'  => 'Enfermeira Administrativa Feira de Santana '),
            ),
            // não existe o email secretaria sulbahia.
            'secretaria' => array(
                1 => array('email' => 'secretaria@mederi.com.br','nome'  => 'Secretária Central'),
                11 => array('email' => 'secretaria.feira@mederi.com.br','nome'  => 'Secretária UR FSA'),
                2 => array('email' => 'secretaria.salvador@mederi.com.br', 'nome' => 'Secretária - Salvador-RMS'),
                5 => array('email' => 'secretaria.alagoinhas@mederi.com.br', 'nome' => 'Secretária - Alagoinhas'),
                7 => array('email' => 'secretaria.sudoeste@mederi.com.br', 'nome' => 'Secretária - Sudoeste'),
                12 => array('email' => 'secretaria.feira@mederi.com.br','nome'  => 'Secretária UR FSA'),
                13 => array('email' => 'secretaria.feira@mederi.com.br','nome'  => 'Secretária UR FSA'),
                14 => array('email' => 'secretaria.feira@mederi.com.br','nome'  => 'Secretária UR FSA')

            ),
            'laboratorio' => array(
                1 => array(
                            array('email' => 'ti@mederi.com.br', 'nome' => 'Dr. Givanilto')
                ),
                11 => array(
                    array('email' => 'ti@mederi.com.br', 'nome' => 'Dr. Givanilto'),
                ),
            )
        ),
        'diretoria' => array(
            'enfermagem' => array(
                'email' => 'direnf@mederi.com.br',
                'nome' => 'Diretoria de Enfermagem'
            )
        )
    )
);

return $configEmail;
