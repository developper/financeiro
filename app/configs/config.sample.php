<?php
/**
 * Esse arquivo deverá ser colocado na pasta um nível 
 * acima do docroot, por exemplo, no sismederi-vagrant, 
 * o caminho é /var/www/config.php.
 */
return array(  
	'APP_ENV' => 'dev',
	'DB' => array(
		'host' => 'localhost',
		'user' => 'sismederi',
		'pass' => 'sismederi',
		'dbname' => 'sismederi'
	),
  'obsequium_api' => array(
    'userId' => 1, // Mederi User ID in the Obsequium database
    'host' => 'http://0.0.0.0:8080',
    'user' => 'mederi_api',
    'pass' => 'foo',
    'active' => true,
  ),
  'queue' => array(
    'driver' => 'beanstalkd',
    'host'   => '127.0.0.1',
    'name'   => 'default',
  ),
  'GS_FERIDAS_QUEUE'    => 'gcs.feridas.dev',
  'GS_SCOPES'           => array('https://www.googleapis.com/auth/devstorage.read_write'),
  'GS_CLIENT_EMAIL'     => '381781926511-98is2lvc8qtuoi393irpuphopdpobr9k@developer.gserviceaccount.com',
  'GS_BUCKET_NAME'      => 'sismederi-dev',
  'GS_BUCKET'           => 'gs://sismederi-dev/', # exemple: gs://sismederi-dev/ (default: false)
  'GS_PUBLIC_DOMAIN'    => 'http://storage.googleapis.com',
  'GAE_PRIVATE_KEY'     => '/var/www/ssh-keys/sismederi-prd-c5dcb0f78c39.p12',
  'SHELL_EXEC_USER'     => 'vagrant',
	'TOKEN_HIPCHAT'       => 'WvxROFpn6a6P8jQZq8nwxZQZrnjlKQbV2ff6HRJB'
);
