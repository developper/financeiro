<?php

namespace App\Helpers;

class DateHelper
{
    public static $arrayFeriadosFixos = [
        0 => '01/01',
        2 => '21/04',
        3 => '01/05',
        4 => '24/06',
        5 => '02/07',
        6 => '07/09',
        7 => '12/10',
        8 => '02/11',
        9 => '15/11',
        10 => '25/12',
        11 => '08/12'
        
    ];

    public static $arrayFeriadosMutaveis = [
        '12/02/2024', // carnaval
        '13/02/2024', //carnaval
        '30/05/2024', //Corpus Christi
        '29/03/2024' // paixão de cristo

    ];

	public static function diaUtilVencimentoReal($vencimento)
	{
        // caso o vencimento não seja dia util pega o proximo dia util        
        $date = \DateTime::createFromFormat('d/m/Y', $vencimento); 
        $isValid == false;       
        if($date->format('w') == 0 || $date->format('w') == 6 ||
         in_array($date->format('d/m'), self::$arrayFeriadosFixos) || in_array($date->format('d/m/Y'), self::$arrayFeriadosMutaveis)){
            while($isValid == false){
                
                if($date->format('w') == 0){
                        $date = $date->add(new \DateInterval('P1D'));
                    
                }else if($date->format('w') == 6){
                        $date = $date->add(new \DateInterval('P2D'));                    
                }                
                if(in_array($date->format('d/m'), self::$arrayFeriadosMutaveis)){
                    $date = $date->add(new \DateInterval('P1D'));	                   
                }
                else if(in_array($date->format('d/m'), self::$arrayFeriadosFixos)){
                    $date = $date->add(new \DateInterval('P1D'));	                   
                }else{                    
                    break;
                }
            
            }
        }      
        
        return  $date->format('d/m/Y');       
		
    }
    
    // traz todos os meses e ano entre duas datas inclundo elas
    public static function getListaMesAno($dataInicio, $dataFim) 
    {
        $startDate = strtotime($dataInicio);
        $endDate   = strtotime($dataFim);
        $mesAno = [];

        $currentDate = $startDate;

        while ($currentDate <= $endDate) {
            $mesAno[] = date('Y-m',$currentDate);    
            $currentDate = strtotime( date('Y-m',$currentDate).' +1 month');    
        }

        return $mesAno;
    }
        

	
}