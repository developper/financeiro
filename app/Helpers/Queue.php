<?php

namespace App\Helpers;

use \Mysqli as Connection;

class Queue
{

  const PENDING    = 'PENDING';
  const PROCESSING = 'PROCESSING';
  const PROCESSED  = 'PROCESSED';
  const ERROR      = 'ERROR';

  protected $table = 'job_queue';
  protected $repository;

  public function __construct(Connection $repository, Array $types)
  {
    $this->repository = $repository;
    $this->loadQueue($types, [self::PENDING, self::ERROR]);
  }

  public function walk($callback)
  {
    foreach ($this->rows as $row) {
      $this->updateStatus($row['id'], self::PROCESSING, true);
      try {
        call_user_func_array($callback, json_decode($row['params'], true));
        $this->updateStatus($row['id'], self::PROCESSED, true);
      } catch (\Exception $e) {
        $this->updateStatus($row['id'], self::ERROR, true, 'Exception: ' . utf8_decode($e->getMessage()));
      }
    }
  }

  public function loadQueue(Array $types, Array $status)
  {
    $types  = sprintf('"%s"', implode('","', $types));
    $status = sprintf('"%s"', implode('","', $status));
    $sql    = "SELECT * FROM $this->table WHERE type IN ($types) AND status IN ($status)";
    $this->rows = $this->repository->query($sql);
  }

  public function updateStatus($id, $status, $attempts = false, $errorMessage = null)
  {
    $attempts = $attempts ? "num_attempts = num_attempts + 1," : '';
    $errorMessage = isset($errorMessage) ? "last_error_message = '$errorMessage'," : '';
    $sql = "UPDATE $this->table SET `status` = '$status', $attempts $errorMessage `last_update_at` = NOW() WHERE id = $id";
    $this->repository->query($sql);
  }

}