<?php

namespace App\Helpers;

class StringHelper
{

    public static function removeAcentos($string)
    {
        return strtr($string, "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ", "aaaaeeiooouucAAAAEEIOOOUUC");
    }

    public static function money($value)
    {
        return self::stringPad(number_format($value, 2, '', ''), 15, "left");
    }

    public static function moneyStringToFloat($value)
    {
        $value = str_replace('.','',$value);
        $value = str_replace(',','.',$value);
        return $value;
    }

    public static function currencyFormat($value)
    {
        return 'R$ ' . number_format($value, 2, ',', '.');
    }

    public static function stringPad($value, $size, $direction = 'left', $char = "0")
    {
        return str_pad ($value, $size, $char, $direction === 'left' ? STR_PAD_LEFT : STR_PAD_RIGHT);
    }

    public static function stripDots($strings)
    {
        return str_replace( '.', '', $strings);
    }

    public static function limitString($value, $start, $break, $filler = '...')
    {
        $size = strlen($value);
        if($size <= $break){
            $new_text = $value;
        }else{
            $last_space = strrpos(substr($value, $start, $break), " ");
            $new_text = trim(substr($value, $start, $last_space)) . $filler;
        }
        return $new_text;
    }

    public static function cropString($value, $start, $break, $filler = '...')
    {
        $size = strlen($value);
        if($size <= $break){
            $new_text = $value;
        }else{
            $new_text = trim(substr($value, $start, $break)) . $filler;
        }
        return $new_text;
    }

    public static function UTF8DecodeString($string)
    {
        return utf8_decode($string);
    }

    public static function UTF8EncodeString($string)
    {
        return utf8_encode($string);
    }

    public static function removeAcentosViaEReg($string, $cedilha = false)
    {
        $rules = [
            "/(á|à|ã|â|ä)/",
            "/(Á|À|Ã|Â|Ä)/",
            "/(é|è|ê|ë)/",
            "/(É|È|Ê|Ë)/",
            "/(í|ì|î|ï)/",
            "/(Í|Ì|Î|Ï)/",
            "/(ó|ò|õ|ô|ö)/",
            "/(Ó|Ò|Õ|Ô|Ö)/",
            "/(ú|ù|û|ü)/",
            "/(Ú|Ù|Û|Ü)/",
            "/(ñ)/",
            "/(Ñ)/"
        ];
        $cedilhas = ["/(ç)/",	"/(Ç)/"];
        if($cedilha)
            $rules = array_merge($rules, $cedilhas);
        return preg_replace(
            $rules,
            explode(" ","a A e E i I o O u U n N c C"),
            $string
        );
    }

    public static function parseFloat($var){
        return empty($var) ? '' : (float) $var;
    }

    public static function parseStringDateFormatDB($data){
        // recebe formato dmY exemplo 28082019

        if(!empty($data) && strlen($data) == 8){
            $data = \DateTime::createFromFormat('dmY', $data);
            return $data == false ? '': $data->format('Y-m-d');
        }
        return '';
    }

    public static function parseDateformatDBToStringBR($data){

        $data = \DateTime::createFromFormat('Y-m-d', $data);
        return $data == false ? '': $data->format('d/m/Y');

    }

    public static function maskCPF($val) {
        $maskared = '';
        $k = 0;
        $mask = '###.###.###-##';
        for($i = 0; $i<=strlen($mask)-1; $i++) {
            if($mask[$i] == '#') {
                if(isset($val[$k])) $maskared .= $val[$k++];
            } else {
                if(isset($mask[$i])) $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }
    public static function maskCNPJ($val) {
        $maskared = '';
        $k = 0;
        $mask = '##.###.###/####-##';
        for($i = 0; $i<=strlen($mask)-1; $i++) {
            if($mask[$i] == '#') {
                if(isset($val[$k])) $maskared .= $val[$k++];
            } else {
                if(isset($mask[$i])) $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }
    
    
}