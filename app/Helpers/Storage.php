<?php

namespace App\Helpers;

use App\Core\Config;
use App\Integration\GoogleCloud\StorageFactory;

class Storage 
{

  protected $privateKey;
  protected $user;
  protected $bucket;
  protected $storageBaseFolder;

  public function __construct($privateKey, $user = null, $keystore = 'notasecret')
  {
    if (! file_exists($privateKey))
      throw new \Exception("Private key not found: $privateKey");

    $this->privateKey = $privateKey;
    $this->keystore = $keystore;
    $this->user = $user;
  }

  public function move($sourceDir, $targetDir)
  {
    $command = $this->withPrevilegies('gsutil mv -a public-read "%s" "%s"');
    return shell_exec(sprintf($command, $sourceDir, $targetDir));
  }

  public function copy($sourceDir, $targetDir)
  {
    $command = $this->withPrevilegies('gsutil cp -a public-read "%s" "%s"');
    return shell_exec(sprintf($command, $sourceDir, $targetDir));
  }

  public function fileExists($filename)
  {
    return file_exists($filename);
  }

  private function withPrevilegies($command)
  {
    if (isset($this->user) && !empty($this->user))
      return sprintf('sudo -u %s %s', $this->user, $command);

    return $this->command;
  }

  public function setBucket($bucket)
  {
    $this->bucket = $bucket;
  }

  public function setStorageBaseFolder($path)
  {
    $this->storageBaseFolder = $path;
  }

  public static function getImageUrl($path, $rootUrl = '/')
  {
    $fullPath = __DIR__ . '/../../' . $path;
    if (file_exists($fullPath))
      return $rootUrl . $path;

    return sprintf('%s/%s/%s', Config::get('GS_PUBLIC_DOMAIN'), Config::get('GS_BUCKET_NAME'), $path);
  }

    public static function getImageUrlByBucketName($path, $bucketName = 'GS_EXAMES_BUCKET_NAME', $rootUrl = '/')
    {
        $fullPath = __DIR__ . '/../../' . $path;
        if (file_exists($fullPath))
            return $path;

        return sprintf('%s/%s/%s', Config::get('GS_PUBLIC_DOMAIN'), Config::get($bucketName), $path);
    }

  public static function getImageUrlWithViewer($path)
  {
    return sprintf('/viewer_image.php?path=%s', $path);
  }

}