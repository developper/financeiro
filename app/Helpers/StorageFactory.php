<?php

namespace App\Helpers;

use \App\Core\Config;

class StorageFactory 
{

  private function __construct() {}

  public static function getInstance()
  {
    $storage = new Storage(
      Config::get('GAE_PRIVATE_KEY'),
      Config::get('SHELL_EXEC_USER')
    );
    $storage->setBucket(Config::get('GS_BUCKET'));
    return $storage;
  }

}