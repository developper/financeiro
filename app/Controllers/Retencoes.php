<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 21/03/2017
 * Time: 09:16
 */

namespace app\Controllers;

use \App\Models\Financeiro\Retencoes AS RetencoesModel;

class Retencoes
{
    public static function configRetencoesForm()
    {
        $response['desembolso'] = RetencoesModel::getConfiguracaoRetencoes('desembolso');
        $response['recebimento'] = RetencoesModel::getConfiguracaoRetencoes('recebimento');
        $response['vencimento'] = RetencoesModel::getVencimentosRetencoes();
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/retencoes/templates/configuracao-retencoes/form.phtml');
    }

    public static function getJsonContasRetencoes()
    {
        $term = filter_input(INPUT_GET, 'term', FILTER_SANITIZE_STRING);
        $response = RetencoesModel::getContasRetencoesByTerm($term);
        echo json_encode($response);
    }

    public static function salvar()
    {
        if (isset($_POST) && !empty($_POST)) {
            $data = [];
            $data['desembolso'] = $_POST['conta']['desembolso'];
            $data['recebimento'] = $_POST['conta']['recebimento'];

            $data['vencimentos'] = $_POST['vencimento'];

            //var_dump($data);

            $response = RetencoesModel::save($data);

            $msg = "Configurações atualizadas com sucesso!";
            $type = 's';
            if(!$response) {
                $msg = "Houve um problema ao atualizar as configurações!";
                $type = 'f';
            }
            header ("Location: /financeiros/retencoes/?action=configuracao-retencoes&msg=" . base64_encode ($msg) . "&type={$type}");
        }
    }
}
