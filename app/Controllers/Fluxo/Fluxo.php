<?php

namespace App\Controllers\Fluxo;

use App\Models\AbstractModel;
use App\Models\Administracao\Planos;
use App\Models\Logistica\Catalogo;
use App\Models\Sistema\CategoriaMaterial;

class Fluxo extends AbstractModel
{
    public static function jsonItensRegraFluxo()
    {
        if(isset($_GET) && !empty($_GET)) {
            $opcao = filter_input(INPUT_GET, 'opcao', FILTER_SANITIZE_NUMBER_INT);
            $tipo = filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_NUMBER_INT);
            $termo = filter_input(INPUT_GET, 'termo', FILTER_SANITIZE_STRING);

            if($opcao == 1) {
                $response = CategoriaMaterial::getByTerm($termo);
                $response[] = ['value' => 'TODOS', 'ID' => 0];
            } else {
                if($tipo == '0' || $tipo == '1' || $tipo == '3') {
                    $response = Catalogo::getByItemAndTipo($termo, $tipo);
                } else {
                    $response = Catalogo::getEquipamentoByTerm($termo);
                }
            }
            echo json_encode($response);
        }
    }

    public static function getFluxoPlano($id)
    {
        $response = [];

        $regra_primaria = [
            'liberar_tudo_exceto' => '1',
            'bloquear_tudo_exceto' => '2',
        ];

        $fluxos = Planos::getFluxoByPlanoId($id);

        foreach ($fluxos as $fluxo) {
            $response[$fluxo['tabela_origem']][$fluxo['opcao_regra']] = [
                'regra_primaria' => $regra_primaria[$fluxo['regra_primaria']],
                'itens' => Planos::getItensByFluxoId($fluxo['tabela_origem'], $fluxo['opcao_regra'], $id)
            ];
        }

        return $response;
    }
}
