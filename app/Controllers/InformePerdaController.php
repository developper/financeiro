<?php

namespace app\Controllers;

use App\Models\Financeiro\BaixaPorPerda;
use \App\Models\Financeiro\Financeiro as Financeiro,
	\App\Models\Financeiro\Fornecedor as Fornecedor;
use App\Models\Financeiro\InformePerda;

class InformePerdaController
{
    public static function salvar()
    {
        if(isset($_POST) && !empty($_POST)){
            $data = [];
            $data['parcelaId']  = filter_input(INPUT_POST, 'parcelaId', FILTER_SANITIZE_NUMBER_INT);
            $data['notaId']  = filter_input(INPUT_POST, 'notaId', FILTER_SANITIZE_NUMBER_INT);
            $data['valor']  = filter_input(INPUT_POST, 'valor', FILTER_SANITIZE_STRING);
            $data['data']  = filter_input(INPUT_POST, 'data', FILTER_SANITIZE_STRING);
            $data['justificativa']  = filter_input(INPUT_POST, 'justificativa', FILTER_SANITIZE_STRING);


           $data['data'] = implode("-",array_reverse(explode("/",$data['data'])));

            $fechamentoCaixa = new \DateTime(\App\Controllers\FechamentoCaixa::getFechamentoCaixa()['data_fechamento']);
           
            $dataObj = new \DateTime($data['data']);
            if($dataObj <= $fechamentoCaixa) {
                $response = ['tipo' => 'erro',
                'msg'=>"Data de Pagamento ".$dataObj->format('d/m/Y')." menor qua a 4321 data de fechamento do período ".$fechamentoCaixa->format('d/m/Y')

            ];
             echo json_encode($response);
                return;
            }
         
            $response = InformePerda::salvarInforme($data);            
          
                     
            
            if($response['tipo'] == 'acerto') {
                $response = ['tipo' => 'acerto',
                'msg'=>$response['msg']
    
                ];
                echo json_encode($response);
                return;
            } else {
                $response = ['tipo' => 'erro',
                'msg'=> $response['msg']
                ];
                echo json_encode($response);
                return;
               
            }
        }

    }
    public static function cancelar()
    {
        if(isset($_POST) && !empty($_POST)){

            $data = [];
            $data['parcelaId']  = filter_input(INPUT_POST, 'parcelaId', FILTER_SANITIZE_NUMBER_INT);
            $data['notaId']  = filter_input(INPUT_POST, 'notaId', FILTER_SANITIZE_NUMBER_INT);
            $data['informePerdaId']  = filter_input(INPUT_POST, 'informePerdaId', FILTER_SANITIZE_NUMBER_INT);
             
                           
            
            $response = InformePerda::cancelarInforme($data);              
                     
            
            if($response['tipo'] == 'acerto') {
                $response = ['tipo' => 'acerto',
                'msg'=>$response['msg']
    
                ];
                echo json_encode($response);
                return;
            } else {
                $response = ['tipo' => 'erro',
                'msg'=> $response['msg']
                ];
                echo json_encode($response);
                return;
               
            }
        }

    }
	
}