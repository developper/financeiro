<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 30/09/2016
 * Time: 08:38
 */

namespace app\Controllers;
use App\Models\Logistica\Catalogo ;
use App\Models\Logistica\FerramentaAjusteEstoque as Ajuste;


class FerramentaAjusteEstoque
{
    public static function form()
    {
        require($_SERVER['DOCUMENT_ROOT'] . '/logistica/FerramentaAjusteEstoque/templates/form.phtml');

    }

    public static function buscaItem()
    {
        $item = filter_input(INPUT_POST,'nome_item',FILTER_SANITIZE_STRING);
        $tipo = filter_input(INPUT_POST,'tipo',FILTER_SANITIZE_STRING);
        $arrayItem = Catalogo::getByItemAndTipo($item,$tipo);
        require($_SERVER['DOCUMENT_ROOT'] . '/logistica/FerramentaAjusteEstoque/templates/form.phtml');



    }

    public static function salvar()
    {
        if(isset($_POST) && !empty($_POST)) {
            $itens           = $_POST['item'];
            foreach($itens as $item){
                if($item['quantidade'] > 0){
                    $response = Ajuste::updateEntrada($item);
                    if(!$response){
                        mysql_query('rollback');
                        echo "Erro ao atualizar entrada.";
                        return ;
                    }

                    $response = Ajuste::updateEstoque($item);
                    if(!$response){
                        mysql_query('rollback');
                        echo "Erro ao atualizar estoque.";
                        return ;
                    }
                }
            }
            echo 'Entrada e estoque atualizados com sucesso.';
        }

    }

}