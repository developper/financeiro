<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 21/03/2017
 * Time: 09:16
 */

namespace app\Controllers;

use \App\Models\Faturamento\Fatura as FaturaModels;


class Fatura
{
    public static function custoServicoEquipamentoByURAndPeriodo($item, $empresa, $tabela_origem, $inicio)
    {
        $idCobranca = $item;
        // pegar idCobranca na tabela valores cobranca
        if($tabela_origem == 'valorescobranca'){
            $result = FaturaModels::getValoresCobrancaById($item);
            $idCobranca = $result[0]['idCobranca'];
        }
        // Pegar custo e data na tabela base
        $resultCustoAtual = FaturaModels::getCustoServicoEquipamentoByUR($idCobranca,$empresa);

        if(empty($resultCustoAtual)){
           return 0;
        }
        $dataInicioAtual = explode(' ',$resultCustoAtual[0]['DATA']);
        $custoAtual = $resultCustoAtual[0]['CUSTO'];
        // caso o início da fatura seja maior que a data de início do item na tabela "custo_servicos_plano_ur"
        // o custo será o dessa tabela caso contrario busca valor na tabela de histórico.
        if($inicio >= $dataInicioAtual[0]){
           return $custoAtual;
        }

        $resultCustoHistorico = FaturaModels::getHistoricoCustoServicoEquipamentoByURAndInicio($idCobranca,$empresa,$inicio);

        if(empty($resultCustoHistorico)){
            return 0;
        }

        return $resultCustoHistorico[0]['custo'];


    }

    public static function custoCatalogoByPeriodo($item, $inicio)
    {
        $idCatalogo = $item;

        // Pegar custo e data na tabela base catalogo
        $resultCustoAtual = FaturaModels::getCustoCatalogo($idCatalogo);


        if(empty($resultCustoAtual)){
            return 0;
        }
        $dataInicioAtual = explode(' ',$resultCustoAtual[0]['custo_medio_atualizado_em']);


        $custoAtual = $resultCustoAtual[0]['valorMedio'];
        // caso o início da fatura seja maior que a data de atualização do custo do item na tabela "catalogo"
        // o custo será o dessa tabela caso contrario busca valor na tabela entrada.
        if($inicio >= $dataInicioAtual[0]){
            return $custoAtual;

        }


        $resultCustoHistorico = FaturaModels::getHistoricoCustoEntradaMaterialMedicamentoDieta($idCatalogo,$inicio);


        if(empty($resultCustoHistorico)){
            return 0;
        }

        return $resultCustoHistorico[0]['valor'];


    }



}