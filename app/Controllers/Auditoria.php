<?php

namespace app\Controllers;

use \App\Models\Administracao\Filial;
use \App\Models\Administracao\Paciente;
use \App\Models\Administracao\Planos;
use \App\Models\Administracao\Usuario;
use \App\Models\Auditoria\Fatura;
use \App\Models\Auditoria\Solicitacao;
use \App\Models\Auditoria\RelatorioNaoConformidade;
use App\Core\Config;
use App\Models\Logistica\Catalogo;
use App\Models\Logistica\Saidas;
use App\Models\Sistema\Profissionais;
use App\Services\Gateways\SendGridGateway;
use App\Services\MailAdapter;

class Auditoria
{
    public static function formRelatorioFaturasLiberadas()
    {
        $auditores = Usuario::getUsersByRole('modaud');
        require($_SERVER['DOCUMENT_ROOT'] . '/auditoria/relatorios/templates/faturas-liberadas/relatorio-faturas-liberadas-form.phtml');
    }

    public static function relatorioFaturasLiberadas()
    {
        if(isset($_POST) && !empty($_POST)){
            $periodoInicio = filter_input(INPUT_POST, 'periodo_inicio', FILTER_SANITIZE_STRING);
            $periodoFim = filter_input(INPUT_POST, 'periodo_fim', FILTER_SANITIZE_STRING);
            $auditorBusca = filter_input(INPUT_POST, 'auditor', FILTER_SANITIZE_NUMBER_INT);
            $ordenarPor = filter_input(INPUT_POST, 'ordenar_por', FILTER_SANITIZE_STRING);

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'auditor'       => $auditorBusca,
                'ordenarPor'    => $ordenarPor
            ];

            $response 	= Fatura::getRelatorioFaturasLiberadas($filtros);
            $auditores  = Usuario::getUsersByRole('modaud');
            $count      = count($response);

            require($_SERVER['DOCUMENT_ROOT'] . '/auditoria/relatorios/templates/faturas-liberadas/relatorio-faturas-liberadas-form.phtml');
        }
    }

    public static function excelRelatorioFaturasLiberadas()
    {
        if(isset($_GET) && !empty($_GET)){
            $periodoInicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $periodoFim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $auditorBusca = filter_input(INPUT_GET, 'auditor', FILTER_SANITIZE_NUMBER_INT);
            $ordenarPor = filter_input(INPUT_GET, 'ordenarpor', FILTER_SANITIZE_STRING);

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'auditor'       => $auditorBusca,
                'ordenarPor'    => $ordenarPor
            ];

            $response 	= Fatura::getRelatorioFaturasLiberadas($filtros);
            $count      = count($response);

            $extension = 'xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $inicio = \DateTime::createFromFormat('Y-m-d', $periodoInicio);
            $fim =  \DateTime::createFromFormat('Y-m-d', $periodoFim);
            $filename = sprintf(
                "relatorio_faturas_liberadas_%s_%s.%s",
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require($_SERVER['DOCUMENT_ROOT'] . '/auditoria/relatorios/templates/faturas-liberadas/relatorio-faturas-liberadas-excel.phtml');
        }
    }

    public static function formRelatorioPedidosLiberados()
    {
        $auditores = Usuario::getUsersByRole('modaud');
        $pacientes = Paciente::getAll();
        require($_SERVER['DOCUMENT_ROOT'] . '/auditoria/relatorios/templates/pedidos-liberados/relatorio-pedidos-liberados-form.phtml');
    }

    public static function relatorioPedidosLiberados()
    {
        if(isset($_POST) && !empty($_POST)){
            $periodoInicio = filter_input(INPUT_POST, 'periodo_inicio', FILTER_SANITIZE_STRING);
            $periodoFim = filter_input(INPUT_POST, 'periodo_fim', FILTER_SANITIZE_STRING);
            $auditorBusca = filter_input(INPUT_POST, 'auditor', FILTER_SANITIZE_NUMBER_INT);
            $pacienteBusca = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $ordenarPor = filter_input(INPUT_POST, 'ordenar_por', FILTER_SANITIZE_STRING);
            $tipoRelatorio = filter_input(INPUT_POST, 'tipo_relatorio', FILTER_SANITIZE_STRING);

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'auditor'       => $auditorBusca,
                'paciente'      => $pacienteBusca,
                'ordenarPor'    => $ordenarPor,
                'tipoRelatorio' => $tipoRelatorio
            ];

            $response 	= Solicitacao::getRelatorioPedidosLiberados($filtros);
            $auditores  = Usuario::getUsersByRole('modaud');
            $pacientes = Paciente::getAll();
            $count      = count($response);

            require($_SERVER['DOCUMENT_ROOT'] . '/auditoria/relatorios/templates/pedidos-liberados/relatorio-pedidos-liberados-form.phtml');
        }
    }

    public static function excelRelatorioPedidosLiberados()
    {
        if(isset($_GET) && !empty($_GET)){
            $periodoInicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $periodoFim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $auditorBusca = filter_input(INPUT_GET, 'auditor', FILTER_SANITIZE_NUMBER_INT);
            $pacienteBusca = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $ordenarPor = filter_input(INPUT_GET, 'ordenarpor', FILTER_SANITIZE_STRING);
            $tipoRelatorio = filter_input(INPUT_GET, 'tiporelatorio', FILTER_SANITIZE_STRING);

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'auditor'       => $auditorBusca,
                'paciente'      => $pacienteBusca,
                'ordenarPor'    => $ordenarPor,
                'tipoRelatorio' => $tipoRelatorio
            ];

            $response 	= Solicitacao::getRelatorioPedidosLiberados($filtros);
            $count      = count($response);

            $extension = 'xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $inicio = \DateTime::createFromFormat('Y-m-d', $periodoInicio);
            $fim =  \DateTime::createFromFormat('Y-m-d', $periodoFim);
            $filename = sprintf(
                "relatorio_pedidos_liberados_%s_%s_%s.%s",
                strtoupper($tipoRelatorio),
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require($_SERVER['DOCUMENT_ROOT'] . '/auditoria/relatorios/templates/pedidos-liberados/relatorio-pedidos-liberados-excel.phtml');
        }
    }

    public static function formRelatorioFaturamentoEnviado()
    {
        $auditores  = Usuario::getUsersByRole('modaud =1 OR modfat');
        $pacientes  = Paciente::getAll();
        $urs        = Filial::getAll();
        $planos     = Planos::getAll();

        require($_SERVER['DOCUMENT_ROOT'] . '/auditoria/relatorios/templates/faturamento-enviado/relatorio-faturamento-enviado-form.phtml');
    }

    public static function relatorioFaturamentoEnviado()
    {
        $faturas = [];
        $modalidades = [];
        if(isset($_POST) && !empty($_POST)){
            $periodoInicio  = filter_input(INPUT_POST, 'periodo_inicio', FILTER_SANITIZE_STRING);
            $periodoFim     = filter_input(INPUT_POST, 'periodo_fim', FILTER_SANITIZE_STRING);
            $auditorBusca   = filter_input(INPUT_POST, 'auditor', FILTER_SANITIZE_NUMBER_INT);
            $pacienteBusca  = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $urBusca        = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_NUMBER_INT);
            $planoBusca        = filter_input(INPUT_POST, 'plano', FILTER_SANITIZE_NUMBER_INT);
            $ordenarPor     = filter_input(INPUT_POST, 'ordenar_por', FILTER_SANITIZE_STRING);

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'auditor'       => $auditorBusca,
                'paciente'      => $pacienteBusca,
                'ur'            => $urBusca,
                'plano'         => $planoBusca,
                'ordenarPor'    => $ordenarPor,
            ];

            $response 	= Fatura::getRelatorioFaturamentoEnviado($filtros);
            $count      = count($response);

            if($count > 0) {

                foreach ($response as $data) {
                    $faturas[] = $data['ID'];
                }

                $responseModalidade = Fatura::getModalidadeByFaturaId($faturas);

                foreach ($responseModalidade as $data) {
                    $modalidades[$data['FATURA_ID']] = $data['cod_item'];
                }





                foreach ($response as $key => $r) {
                    $modalidade = 'ID';
                    if (empty($modalidades[$r['ID']])) {
                        $modalidade = 'AD';
                    }
                    $response[$key]['modalidade'] = $modalidade;
                }
            }


            $auditores  = Usuario::getUsersByRole('modaud');
            $pacientes  = Paciente::getAll();
            $urs        = Filial::getAll();
            $planos     = Planos::getAll();
            $count      = count($response);

            require($_SERVER['DOCUMENT_ROOT'] . '/auditoria/relatorios/templates/faturamento-enviado/relatorio-faturamento-enviado-form.phtml');
        }
    }

    public static function excelRelatorioFaturamentoEnviado()
    {
        if(isset($_GET) && !empty($_GET)){
            $periodoInicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $periodoFim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $auditorBusca = filter_input(INPUT_GET, 'auditor', FILTER_SANITIZE_NUMBER_INT);
            $pacienteBusca = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $urBusca        = filter_input(INPUT_GET, 'ur', FILTER_SANITIZE_NUMBER_INT);
            $planoBusca        = filter_input(INPUT_GET, 'plano', FILTER_SANITIZE_NUMBER_INT);
            $ordenarPor = filter_input(INPUT_GET, 'ordenarpor', FILTER_SANITIZE_STRING);

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'auditor'       => $auditorBusca,
                'paciente'      => $pacienteBusca,
                'ur'            => $urBusca,
                'plano'         => $planoBusca,
                'ordenarPor'    => $ordenarPor
            ];

            $response 	= Fatura::getRelatorioFaturamentoEnviado($filtros);
            $count      = count($response);

            if($count > 0) {

                foreach ($response as $data) {
                    $faturas[] = $data['ID'];
                }

                $responseModalidade = Fatura::getModalidadeByFaturaId($faturas);

                foreach ($responseModalidade as $data) {
                    $modalidades[$data['FATURA_ID']] = $data['cod_item'];
                }



                foreach ($response as $key => $r) {
                    $modalidade = 'ID';
                    if (empty($modalidades[$r['ID']])) {
                        $modalidade = 'AD';
                    }
                    $response[$key]['modalidade'] = $modalidade;
                }
            }

            $extension = 'xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $inicio = \DateTime::createFromFormat('Y-m-d', $periodoInicio);
            $fim =  \DateTime::createFromFormat('Y-m-d', $periodoFim);
            $filename = sprintf(
                "relatorio_faturamento_enviado_%s_%s.%s",
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require($_SERVER['DOCUMENT_ROOT'] . '/auditoria/relatorios/templates/faturamento-enviado/relatorio-faturamento-enviado-excel.phtml');
        }
    }

    public static function formRelatorioSaidas()
    {
        $pacientes  = Paciente::getAll();
        $urs        = Filial::getAll();
        require($_SERVER['DOCUMENT_ROOT'] . '/auditoria/relatorios/templates/saidas/relatorio-saidas-form.phtml');
    }

    public static function relatorioSaidas()
    {
        if(isset($_POST) && !empty($_POST)){
            $periodoInicio  = filter_input(INPUT_POST, 'periodo_inicio', FILTER_SANITIZE_STRING);
            $periodoFim     = filter_input(INPUT_POST, 'periodo_fim', FILTER_SANITIZE_STRING);
            $tipoBusca      = filter_input(INPUT_POST, 'tipo_busca', FILTER_SANITIZE_STRING);
            $urBusca        = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_NUMBER_INT);
            $pacienteBusca  = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $itemBusca      = filter_input(INPUT_POST, 'item', FILTER_SANITIZE_STRING);
            $tipoRelatorio  = filter_input(INPUT_POST, 'tipo_relatorio', FILTER_SANITIZE_STRING);

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'tipo_busca'    => $tipoBusca,
                'ur'            => $urBusca,
                'paciente'      => $pacienteBusca,
                'item'          => $itemBusca,
                'tipoRelatorio' => $tipoRelatorio
            ];

            $response = Saidas::getRelatorioSaidas($filtros);

            $pacientes  = Paciente::getAll();
            $urs        = Filial::getAll();
            $count      = count($response);

            require($_SERVER['DOCUMENT_ROOT'] . '/auditoria/relatorios/templates/saidas/relatorio-saidas-form.phtml');
        }
    }

    public static function excelRelatorioSaidas()
    {
        if(isset($_GET) && !empty($_GET)){
            $periodoInicio  = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $periodoFim     = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $tipoBusca      = filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING);
            $urBusca        = filter_input(INPUT_GET, 'ur', FILTER_SANITIZE_NUMBER_INT);
            $pacienteBusca  = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $itemBusca      = filter_input(INPUT_GET, 'item', FILTER_SANITIZE_STRING);
            $tipoRelatorio  = filter_input(INPUT_GET, 'tiporelatorio', FILTER_SANITIZE_STRING);

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'tipo_busca'    => $tipoBusca,
                'ur'            => $urBusca,
                'paciente'      => $pacienteBusca,
                'item'          => $itemBusca,
                'tipoRelatorio' => $tipoRelatorio
            ];

            $response = Saidas::getRelatorioSaidas($filtros);
            $count      = count($response);

            $extension = 'xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $inicio = \DateTime::createFromFormat('Y-m-d', $periodoInicio);
            $fim =  \DateTime::createFromFormat('Y-m-d', $periodoFim);
            $filename = sprintf(
                "relatorio_saidas_%s_%s.%s",
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require($_SERVER['DOCUMENT_ROOT'] . '/auditoria/relatorios/templates/saidas/relatorio-saidas-excel.phtml');
        }
    }

    public static function comboBuscaItens()
    {
        $tipos = [
          'med' => 0,
          'mat' => 1,
          'die' => 3,
          'eqp' => 5
        ];
        $options = '<option selected disabled value="">Selecione...</option>';
        if(isset($_GET['tipo']) && !empty($_GET['tipo'])) {
            $response = Catalogo::getItensByTipo($tipos[$_GET['tipo']]);
            foreach ($response as $data) {
                $selected = $_GET['idItem']== $data['cod'] ? 'selected' :'';
                $options .= "<option $selected value='{$data['cod']}'>{$data['nome']}</option>";
            }
            echo $options;
        }
    }

    public static function buscaCondensadaRelatorioNaoConformidades()
    {
        $urs = Filial::getUnidadesAtivas();
        require($_SERVER['DOCUMENT_ROOT'].'/auditoria/relatorios/templates/nao-conformidade/relatorio-nao-conformidades-busca-condensada.phtml');
    }

    public static function getCondensadoRelatorioNaoConformidades()
    {
        if(isset($_POST) && !empty($_POST)) {
            $urs        = Filial::getUnidadesAtivas();
            $periodoInicio = filter_input(INPUT_POST, 'periodo_inicio', FILTER_SANITIZE_STRING);
            $periodoFim = filter_input(INPUT_POST, 'periodo_fim', FILTER_SANITIZE_STRING);
            $urBusca = $_POST['ur'];
            $itensRelatorio =  RelatorioNaoConformidade::getCondensadoRelatorioNaoConformidade($periodoInicio,$periodoFim,$urBusca);
            $profissionais = [];
            foreach ($itensRelatorio as $item) {
                $profissionaisItem = RelatorioNaoConformidade::getProfissionaisRelatorioNaoConformidadeById($item['idRelatorioItem']);
                if(count($profissionaisItem) > 0) {
                    $profissionais[$item['idRelatorioItem']] = array_reduce($profissionaisItem, function ($buffer, $value) {
                        return $buffer .= $value['descricao'] . ', ';
                    });
                    $profissionais[$item['idRelatorioItem']] = substr_replace($profissionais[$item['idRelatorioItem']], '.', -2, 2);
                } else {
                    $profissionais[$item['idRelatorioItem']] = 'NENHUM PROFISSIONAL ASSOCIADO.';
                }
            }
            if(empty($itensRelatorio)){
                $msg = "Nenhum resultado encontrado.";
            }

            require($_SERVER['DOCUMENT_ROOT'].'/auditoria/relatorios/templates/nao-conformidade/relatorio-nao-conformidades-busca-condensada.phtml');
        }
    }

    public static function excelCondesnadoRelatorioNaoConformidades()
    {
        if(isset($_POST) && !empty($_POST)) {
            $periodoInicio = filter_input(INPUT_POST, 'periodo_inicio', FILTER_SANITIZE_STRING);
            $periodoFim = filter_input(INPUT_POST, 'periodo_fim', FILTER_SANITIZE_STRING);
            $urBusca = $_POST['ur'];
            $itensRelatorio =  RelatorioNaoConformidade::getCondensadoRelatorioNaoConformidade($periodoInicio,$periodoFim,$urBusca);
            $profissionais = [];
            $comentarios = [];
            foreach ($itensRelatorio as $item) {
                $profissionaisItem = RelatorioNaoConformidade::getProfissionaisRelatorioNaoConformidadeById($item['idRelatorioItem']);
                if(count($profissionaisItem) > 0) {
                    $profissionais[$item['idRelatorioItem']] = array_reduce($profissionaisItem, function ($buffer, $value) {
                        return $buffer .= $value['descricao'] . ', ';
                    });
                    $profissionais[$item['idRelatorioItem']] = substr_replace($profissionais[$item['idRelatorioItem']], '.', -2, 2);
                } else {
                    $profissionais[$item['idRelatorioItem']] = 'NENHUM PROFISSIONAL ASSOCIADO';
                }
                $comentariosRelatorio = RelatorioNaoConformidade::getComentariosRelatorioNaoConformidadeByItemId($item['idRelatorioItem']);

                if(count($comentariosRelatorio) > 0) {
                    $comentarios[$item['idRelatorioItem']] = array_reduce($comentariosRelatorio, function ($buffer, $value) {
                        if(!empty($value['comentario'])) {
                            $text = sprintf(
                                '%s. por %s',
                                utf8_decode($value['comentario']),
                                utf8_decode(\DateTime::createFromFormat('Y-m-d H:i:s', $value['created_at'])->format('d/m/Y à\s H:i:s')),
                                utf8_decode($value['nome'])
                            );
                            return $buffer .= $text . '<br>';
                        }
                        return $buffer .= '';
                    });
                } else {
                    $comentarios[$item['idRelatorioItem']] = utf8_decode('NENHUM COMENTÁRIO');
                }
            }

            $extension = 'xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $inicio = \DateTime::createFromFormat('Y-m-d', $itensRelatorio[0]['inicio']);
            $inicioTitulo = \DateTime::createFromFormat('Y-m-d', $periodoInicio);
            $fim =  \DateTime::createFromFormat('Y-m-d', $itensRelatorio[0]['fim']);
            $fimTitulo = \DateTime::createFromFormat('Y-m-d', $periodoFim);
            $empresa = $itensRelatorio[0]['empresa'];
            $titulo = "Relatorio de não Conformidades {$empresa} de {$inicioTitulo->format('d/m/Y')} até {$fimTitulo->format('d/m/Y')} ";

            $empresaExp = explode(' ', $empresa);
            $empresaTit = implode('_', $empresaExp);
            $filename = sprintf(
                "relatorio_nao_conformidade_%s_%s_%s.%s",
                $empresaTit,
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require($_SERVER['DOCUMENT_ROOT'].'/auditoria/relatorios/templates/nao-conformidade/relatorio-nao-conformidades-excel.phtml');
        }
    }

    public static function formRelatorioNaoConformidades()
    {
        if(isset($_GET['id']) && !empty($_GET['id'])){
            $itensRelatorio = RelatorioNaoConformidade::getRelatorioNaoConformidadeById($_GET['id']);
        }

        $periodoInicio = filter_input(INPUT_POST, 'periodo_inicio', FILTER_SANITIZE_STRING);
        $periodoFim = filter_input(INPUT_POST, 'periodo_fim', FILTER_SANITIZE_STRING);
        $paciente = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
        $empresa = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_NUMBER_INT);

        $topicoRelatorioNaoConformidade = RelatorioNaoConformidade::getTopico();
        $conteudoRelatorioNaoConformidade = array();
        foreach ($topicoRelatorioNaoConformidade as $topico) {
            $justificativaRelatorioNaoConformidade = RelatorioNaoConformidade::getJustificativasByTopico($topico['id']);
            $conteudoRelatorioNaoConformidade[] = ['id' => $topico['id'],
                'topico' => $topico['item'],
                'justificativa' => $justificativaRelatorioNaoConformidade
            ];

        }
        $pacientes  = Paciente::getAll();
        $urs        = Filial::getUnidadesAtivas();
        $profissionais = Profissionais::getAll();

        require($_SERVER['DOCUMENT_ROOT'] . '/auditoria/relatorios/templates/nao-conformidade/relatorio-nao-conformidades-form.phtml');


    }

    public static function salvarRelatorioNaoConformidades()
    {
        if(isset($_POST) && !empty($_POST)) {
            $periodoInicio   = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $periodoInicioBD =implode("-",array_reverse(explode("/",$periodoInicio)));
            $periodoFim      = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $periodoFimBD =implode("-",array_reverse(explode("/",$periodoFim)));
            $paciente        = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $empresa         = filter_input(INPUT_POST, 'empresa', FILTER_SANITIZE_NUMBER_INT);
            $usuario         = $_SESSION['id_user'];
            $itens           = $_POST['item'];
            $acao            = $_POST['acao'];

            mysql_query('begin');

            $idRelatorioNaoConformidade = RelatorioNaoConformidade::saveRelatorioNaoConfomidade(
                $paciente,
                $periodoInicioBD,
                $periodoFimBD,
                $usuario,
                $empresa,
                $acao
            );

            if(!$idRelatorioNaoConformidade){
                mysql_query('rollback');
                echo "Erro ao iserir Relatorio de Não Cnformidade.";
                return ;
            }

            foreach($itens as $item){
                if($item['status'] != 'RESOLVIDO'){
                    $x = RelatorioNaoConformidade::saveItemRelatorioNaoConformidade(
                        $item['topico'],
                        $item['item'],
                        $item['justificativa'],
                        $item['profissionais'],
                        $idRelatorioNaoConformidade,
                        $usuario,
                        $item['status'],
                        $item['idReferencia']
                    );
                    if(!$x){
                        mysql_query('rollback');
                        echo "Erro ao iserir um item no  Relatorio de Não Conformidade.";
                        return ;
                    }
                }else{
                    $x = RelatorioNaoConformidade::saveItemResolvidoRelatorioNaoConformidade($item['idReferencia'], $usuario);
                    if(!$x){
                        echo 'Erro ao colocar item como resolvido' ;
                        return ;
                    }
                }
            }
            mysql_query('commit');

            echo $idRelatorioNaoConformidade;

            return;
        }
    }

    public static function visualizarRelatorioNaoConformidades()
    {
        if(isset($_GET) && !empty($_GET)) {
            $itensRelatorio = RelatorioNaoConformidade::getRelatorioNaoConformidadeById($_GET['id']);
            $profissionais = [];
            foreach ($itensRelatorio as $item) {
                $profissionaisItem = RelatorioNaoConformidade::getProfissionaisRelatorioNaoConformidadeById($item['idRelatorioItem']);
                if(count($profissionaisItem) > 0) {
                    $profissionais[$item['idRelatorioItem']] = array_reduce($profissionaisItem, function ($buffer, $value) {
                        return $buffer .= $value['descricao'] . ', ';
                    });
                    $profissionais[$item['idRelatorioItem']] = substr_replace($profissionais[$item['idRelatorioItem']], '.', -2, 2);
                } else {
                    $profissionais[$item['idRelatorioItem']] = 'NENHUM PROFISSIONAL ASSOCIADO';
                }
            }
            $comentariosRelatorio = RelatorioNaoConformidade::getComentariosRelatorioNaoConformidadeById($_GET['id']);

            $idRelatorio = $_GET['id'];
            require($_SERVER['DOCUMENT_ROOT'] . '/auditoria/relatorios/templates/nao-conformidade/relatorio-nao-conformidades-visualizar.phtml');
        }

    }

    public static function gerenciarRelatorioNaoConformidades()
    {
        if(isset($_POST) && !empty($_POST)) {
            $periodoInicio = filter_input(INPUT_POST, 'periodo_inicio', FILTER_SANITIZE_STRING);

            $periodoFim = filter_input(INPUT_POST, 'periodo_fim', FILTER_SANITIZE_STRING);
            $periodoInicioBD =implode("-",array_reverse(explode("/",$periodoInicio)));
            $periodoFimBD =implode("-",array_reverse(explode("/",$periodoFim)));
            $pacienteBusca = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $empresaBusca = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_NUMBER_INT);


            $filtros = [
                'inicio'        => $periodoInicioBD,
                'fim'           => $periodoFimBD,
                'paciente'      => $pacienteBusca,
                'empresa'            => $empresaBusca
            ];

            $itensRelatorios =  RelatorioNaoConformidade::getRelatorioNaoConformidadeByFiltros($filtros);

        }


        $pacientes  = Paciente::getAll();
        $urs        = Filial::getUnidadesAtivas();
        require($_SERVER['DOCUMENT_ROOT'].'/auditoria/relatorios/templates/nao-conformidade/relatorio-nao-conformidades-gerenciar.phtml');
    }

    public static function formRelatorioNaoConformidadesEditar()
    {
        if(isset($_GET) && !empty($_GET)){
            $relatorioId = $_GET['id'];
            $itensRelatorio = RelatorioNaoConformidade::getRelatorioNaoConformidadeById($relatorioId);
            $profissionaisItem = [];
            foreach ($itensRelatorio as $item) {
                $profissionaisResult = RelatorioNaoConformidade::getProfissionaisRelatorioNaoConformidadeById($item['idRelatorioItem']);
                if(count($profissionaisResult) > 0) {
                    $profissionaisItem[$item['idRelatorioItem']] = array_reduce($profissionaisResult, function ($buffer, $value) {
                        return $buffer .= $value['descricao'] . ', ';
                    });
                    $profissionaisItem[$item['idRelatorioItem']] = substr_replace($profissionaisItem[$item['idRelatorioItem']], '.', -2, 2);
                } else {
                    $profissionaisItem[$item['idRelatorioItem']] = 'NENHUM PROFISSIONAL ASSOCIADO';
                }
            }
            if(!$itensRelatorio){
                require($_SERVER['DOCUMENT_ROOT'] .'/app/views/404.phtml');
                return;
            }
            $profissionais = Profissionais::getAll();
            $periodoInicio = $itensRelatorio[0]['inicio'];
            $periodoInicio =implode("/",array_reverse(explode("-",$periodoInicio)));
            $periodoFim = $itensRelatorio[0]['fim'];
            $periodoFim =implode("/",array_reverse(explode("-",$periodoFim)));
            $pacienteId = $itensRelatorio[0]['paciente_id'];
            $empresaId  = $itensRelatorio[0]['empresa_id'];



            $topicoRelatorioNaoConformidade = RelatorioNaoConformidade::getTopico();
            $conteudoRelatorioNaoConformidade = array();
            foreach ($topicoRelatorioNaoConformidade as $topico) {
                $justificativaRelatorioNaoConformidade = RelatorioNaoConformidade::getJustificativasByTopico($topico['id']);
                $conteudoRelatorioNaoConformidade[] = ['id' => $topico['id'],
                    'topico' => $topico['item'],
                    'justificativa' => $justificativaRelatorioNaoConformidade
                ];

            }
            $pacientes  = Paciente::getAll();
            $urs        = Filial::getUnidadesAtivas();
            $disabled = 'disabled';
            require($_SERVER['DOCUMENT_ROOT'] . '/auditoria/relatorios/templates/nao-conformidade/relatorio-nao-conformidades-form.phtml');

        }

    }

    public static function updateRelatorioNaoConformidades()
    {
        if(isset($_POST) && !empty($_POST)) {
            $periodoInicio   = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $periodoFim      = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $periodoInicioBD =implode("-",array_reverse(explode("/",$periodoInicio)));
            $periodoFimBD =implode("-",array_reverse(explode("/",$periodoFim)));
            $paciente        = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $empresa         = filter_input(INPUT_POST, 'empresa', FILTER_SANITIZE_NUMBER_INT);
            $id_relatorio    = filter_input(INPUT_POST, 'relatorio-id', FILTER_SANITIZE_NUMBER_INT);
            $usuario         = $_SESSION['id_user'];
            $itens           = $_POST['item'];
            $acao            =  $_POST['acao'];

            $updateRelatorioNaoConformidade = RelatorioNaoConformidade::updateRelatorioNaoConformidade(
                $paciente,
                $periodoInicioBD,
                $periodoFimBD,
                $usuario,
                $empresa,
                $id_relatorio,
                $itens,
                $acao
            );
            echo $updateRelatorioNaoConformidade;
            return;

        }
    }

    public static function cancelarRelatorioNaoConformidades()
    {
        if(isset($_POST['id']) && !empty($_POST['id'])) {
            $usuario         = $_SESSION['id_user'];
            $cancelar = RelatorioNaoConformidade::cancelarRelatorioNaoConformidade($_POST['id'],$usuario );
            echo $cancelar;
            return;

        }


    }
    public static function excelRelatorioNaoConformidades()
    {
        if(isset($_POST) && !empty($_POST)) {
            $itensRelatorio = RelatorioNaoConformidade::getRelatorioNaoConformidadeById($_POST['id-relatorio']);
            $profissionais = [];
            foreach ($itensRelatorio as $item) {
                $profissionaisItem = RelatorioNaoConformidade::getProfissionaisRelatorioNaoConformidadeById($item['idRelatorioItem']);
                if(count($profissionaisItem) > 0) {
                    $profissionais[$item['idRelatorioItem']] = array_reduce($profissionaisItem, function ($buffer, $value) {
                        return $buffer .= $value['descricao'] . ', ';
                    });
                    $profissionais[$item['idRelatorioItem']] = substr_replace($profissionais[$item['idRelatorioItem']], '.', -2, 2);
                } else {
                    $profissionais[$item['idRelatorioItem']] = 'NENHUM PROFISSIONAL ASSOCIADO';
                }
                $comentariosRelatorio = RelatorioNaoConformidade::getComentariosRelatorioNaoConformidadeByItemId($item['idRelatorioItem']);

                if(count($comentariosRelatorio) > 0) {
                    $comentarios[$item['idRelatorioItem']] = array_reduce($comentariosRelatorio, function ($buffer, $value) {
                        if(!empty($value['comentario'])) {
                            $text = sprintf(
                                '%s. por %s',
                                utf8_decode($value['comentario']),
                                utf8_decode(\DateTime::createFromFormat('Y-m-d H:i:s', $value['created_at'])->format('d/m/Y à\s H:i:s')),
                                utf8_decode($value['nome'])
                            );
                            return $buffer .= $text . '<br>';
                        }
                        return $buffer .= '';
                    });
                } else {
                    $comentarios[$item['idRelatorioItem']] = utf8_decode('NENHUM COMENTÁRIO');
                }

            }



            $extension = 'xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $inicio = \DateTime::createFromFormat('Y-m-d', $itensRelatorio[0]['inicio']);
            $fim =  \DateTime::createFromFormat('Y-m-d', $itensRelatorio[0]['fim']);
            $paciente = $itensRelatorio[0]['paciente'];
            $titulo = "Relatorio de não Conformidades {$paciente} de {$inicio->format('d/m/Y')} até {$fim->format('d/m/Y')} ";
            $pacienteExcel = explode(' ', trim($paciente));
            $filename = sprintf(
                "relatorio_nao_conformidade_%s_%s_%s.%s",
                implode('_', $pacienteExcel),
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require($_SERVER['DOCUMENT_ROOT'] . '/auditoria/relatorios/templates/nao-conformidade/relatorio-nao-conformidades-excel.phtml');
        }

    }

    public static function enviarEmailRelatorioNaoConformidades()
    {
        if(isset($_POST) && !empty($_POST)) {

            $periodoInicio = filter_input(INPUT_POST, 'periodo_inicio', FILTER_SANITIZE_STRING);
            $periodoFim = filter_input(INPUT_POST, 'periodo_fim', FILTER_SANITIZE_STRING);
            $urBusca = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_NUMBER_INT);
            $itensRelatorio =  RelatorioNaoConformidade::getCondensadoRelatorioNaoConformidade($periodoInicio,$periodoFim,$urBusca);
            $extension = 'xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $inicio = \DateTime::createFromFormat('Y-m-d', $itensRelatorio[0]['inicio']);
            $fim =  \DateTime::createFromFormat('Y-m-d', $itensRelatorio[0]['fim']);
            $empresa = $itensRelatorio[0]['empresa'];
            $titulo = "Relatorio de não Conformidades {$empresa} de {$inicio->format('d/m/Y')} até {$fim->format('d/m/Y')} ";
            $fileName = sprintf(
                "relatorio_nao_conformidade_%s_%s_%s.%s",
                $empresa,
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );
            $x = self::gerarHtml($titulo,$itensRelatorio);
            $filePath = '';
            $arq = '';

            try {
                $dir = $_SERVER['DOCUMENT_ROOT'].'/auditoria/relatorios/temp';
                if (!is_dir($dir)){
                    mkdir($dir);
                }
                $filePath = $dir . "/" . $fileName;
                $arq = fopen($filePath, "w");
                $escreve = fwrite($arq, $x);
                fclose($arq);
                $texto = $titulo."<br> Criado por: ".$_SESSION['nome_user']."<br> Relatório em anexo.";
                $temp = require $_SERVER['DOCUMENT_ROOT'] . '/app/configs/email.php';
                $para[] = ['email' => $temp['lista']['emails']['enfermagem'][$urBusca]['email'], 'nome'=> $temp['lista']['emails']['enfermagem'][$urBusca]['nome']];

                self::enviarEmailRelatorioNaoConformidadeCondensado($titulo, $texto, $para, $filePath,$fileName);
                unlink($filePath);
                $msg = "O E-mail foi enviado com sucesso.";

            } catch (Exception $e) {
                $msg = "Falha ao gravar fatura: ".$e->getMessage();
            }
            $urs        = Filial::getUnidadesAtivas();



            require($_SERVER['DOCUMENT_ROOT'].'/auditoria/relatorios/templates/nao-conformidade/relatorio-nao-conformidades-busca-condensada.phtml');
        }



    }

    public static function gerarHtml($titulo,$itensRelatorio)
    {
        $html = '';
        $html .= "<table >
            <tr><td colspan='8'><b>".htmlentities($titulo)."</b></td></tr>
        <tr></tr>
        <tr style='background: #DCDCDC'>
            <th>USUARIO</th>
            <th>PACIENTE</th>
            <th>UR</th>
            <th>INICIO</th>
            <th>FIM</th>
            <th>TOPICO</th>
            <th>ITEM</th>
            <th>JUSTIFICATIVA</th>
        </tr>";
         $aux= 2;
        foreach($itensRelatorio as $item){
            $inicio =  \DateTime::createFromFormat('Y-m-d', $item['inicio'])->format('d/m/Y');
            $fim =  \DateTime::createFromFormat('Y-m-d', $item['fim'])->format('d/m/Y');
            $cor = '';
            if( $aux % 2 != 0){
                $cor = "style='background: #F5F5F5'";
            }
            $aux++;

            $html .= "<tr $cor>
                <td> ".htmlentities($item['usuario'])."</td>
                <td> (".$item['codigoPaciente'].")  ".htmlentities($item['paciente'])." </td>
                <td> ".htmlentities($item['empresa'])." </td>
                <td> ".$inicio." </td>
                <td> ".$fim." </td>
                <td> ".htmlentities($item['topico'])." </td>
                <td> ".htmlentities($item['item'])." </td>
                <td> ".htmlentities($item['justificativa'])." </td>
            </tr>";
        }
        $html .="</table>";
        return $html;
    }

    public static function enviarEmailRelatorioNaoConformidadeCondensado($titulo, $texto, $para, $filePaht, $fileName)
    {
        $api_data = (object) Config::get('sendgrid');
        $gateway = new MailAdapter(
        new SendGridGateway(
        new \SendGrid($api_data->user, $api_data->pass),
        new \SendGrid\Email()
        )
        );
        $gateway->adapter->sendSimpleMail($titulo, $texto, $para, $filePaht, $fileName);
    }

    public static function reabrirRelatorioNaoConformidades()
    {
        if(isset($_POST) && !empty($_POST)) {
            $justificativa = filter_input(INPUT_POST, 'justificativa', FILTER_SANITIZE_STRING);
            $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
            $usuario         = $_SESSION['id_user'];


                $itensRelatorio =  @RelatorioNaoConformidade::reabrirRelatorioNoaConformidades($id, $justificativa, $usuario);

          if(is_numeric($itensRelatorio)){
               $relatorio =RelatorioNaoConformidade::getRelatorioNaoConformidadeById($id);
               $paciente = $relatorio[0]['paciente'];
                $empresa = $relatorio[0]['empresa'];
                $inicio = \DateTime::createFromFormat('Y-m-d', $relatorio[0]['inicio'])->format('d/m/Y');
                 $fim = \DateTime::createFromFormat('Y-m-d', $relatorio[0]['fim'])->format('d/m/Y');
              $nameUsuario = $_SESSION['nome_user'];
              date_default_timezone_set('America/Bahia');
              $dataAuditado = date('d/m/Y');
              $hora =  date('H:i');

              $titulo = "Relatório de não conformidades Reaberto";
              $texto = "O relatório do(a) paciete: {$paciente}, para a unidade: $empresa, de {$inicio} até {$fim}";
              $texto .= "<br> Pelo usuário: $nameUsuario, em {$dataAuditado} às {$hora}";
              $texto .= "<br> <b>Justificativa:</b> $justificativa.";

              $para[] = ['email' =>'coord.auditoria@mederi.com.br', 'nome' => 'Coord Auditoria'];
              self::enviarEmailRelatorioNaoConformidadeCondensado($titulo, $texto, $para, null,null);
              echo $itensRelatorio;
          }else{
              echo $itensRelatorio;
          }
        }
    }

    public static function usarParaNovoRelatorioNaoConformidades()
    {
        if(isset($_GET) && !empty($_GET)){
            $relatorioId = $_GET['id'];
            $itensRelatorio = RelatorioNaoConformidade::getRelatorioNaoConformidadeById($relatorioId);
            $profissionaisItem = [];
            $profissionaisItemIds = [];
            foreach ($itensRelatorio as $item) {
                $profissionaisItens = RelatorioNaoConformidade::getProfissionaisRelatorioNaoConformidadeById($item['idRelatorioItem']);
                if(count($profissionaisItem) > 0) {
                    $profissionaisItem[$item['idRelatorioItem']] = array_reduce($profissionaisItens, function ($buffer, $value) {
                        return $buffer .= $value['descricao'] . '<br>';
                    });
                    $profissionaisItemIds[$item['idRelatorioItem']] = array_reduce($profissionaisItens, function ($buffer, $value) {
                        return $buffer .= $value['profissional'] . ',';
                    });
                    $profissionaisItemIds[$item['idRelatorioItem']] = substr_replace($profissionaisItemIds[$item['idRelatorioItem']], '', -1, 1);
                } else {
                    $profissionaisItem[$item['idRelatorioItem']] = 'NENHUM PROFISSIONAL ASSOCIADO';
                    $profissionaisItemIds[$item['idRelatorioItem']] = '';
                }
            }
            if(!$itensRelatorio){
                require($_SERVER['DOCUMENT_ROOT'] .'/app/views/404.phtml');
                return;
            }
            $periodoInicio = $itensRelatorio[0]['inicio'];
            $periodoFim = $itensRelatorio[0]['fim'];
            $periodoInicio =implode("/",array_reverse(explode("-",$periodoInicio)));
            $periodoFim =implode("/",array_reverse(explode("-",$periodoFim)));
            $pacienteId = $itensRelatorio[0]['paciente_id'];
            $empresaId  = $itensRelatorio[0]['empresa_id'];


            $topicoRelatorioNaoConformidade = RelatorioNaoConformidade::getTopico();
            $conteudoRelatorioNaoConformidade = array();
            foreach ($topicoRelatorioNaoConformidade as $topico) {
                $justificativaRelatorioNaoConformidade = RelatorioNaoConformidade::getJustificativasByTopico($topico['id']);
                $conteudoRelatorioNaoConformidade[] = ['id' => $topico['id'],
                    'topico' => $topico['item'],
                    'justificativa' => $justificativaRelatorioNaoConformidade
                ];

            }
            $pacientes  = Paciente::getAll();
            $urs        = Filial::getUnidadesAtivas();
            $profissionais = Profissionais::getAll();
            $disabled = 'disabled';
            require($_SERVER['DOCUMENT_ROOT'] . '/auditoria/relatorios/templates/nao-conformidade/relatorio-nao-conformidades-form-usar-para-novo.phtml');

        }
    }

    public static function salvarComentarioRelatorioNaoConformidades()
    {
        if(isset($_POST) && !empty($_POST)) {
            $comentario =  filter_input(INPUT_POST, 'comentario', FILTER_SANITIZE_STRING);
            $itemId = $_POST['itemId'];
        }
        $userId = $_SESSION['id_user'];
        $nomeUser = $_SESSION["nome_user"];


        $comentarioId = RelatorioNaoConformidade::salvarComentarioRelatorioNaoConformidade($itemId, $comentario, $userId);


        if($comentarioId >= 1) {
            date_default_timezone_set('America/Bahia');
            $data = date('d/m/Y H:i');
            echo json_encode(array('comentario_id' => $comentarioId, 'comentario' => $comentario, 'user' => $nomeUser, 'data' => $data ));

        }else{
            echo $comentarioId;
        }


    }

    public static function removerComentarioRelatorioNaoConformidades()
    {
        if(isset($_POST) && !empty($_POST)) {
            $comentarioId = $_POST['comentarioId'];

        }
        $userId = $_SESSION['id_user'];

        $result= RelatorioNaoConformidade::removerComentarioRelatorioNaoConformidade($comentarioId,$userId);
        echo $result;
    }


}
