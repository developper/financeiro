<?php
namespace app\Controllers;

use App\Models\Financeiro\Assinatura;
use \App\Models\Administracao\Filial;
use App\Models\Financeiro\AplicacaoFundos;
use App\Models\Financeiro\Fornecedor;
use App\Models\Financeiro\IPCF;
use App\Models\Financeiro\Parcela;
use App\Models\Financeiro\TipoDocumentoFinanceiro;
use App\Models\Financeiro\Notas;

include_once('../../utils/codigos.php');

class FaturaImposto
{
    public static function indexFaturaImposto()
    {
        $fornecedores = Fornecedor::getAll();
        $tiposDocumentoFinanceiro = TipoDocumentoFinanceiro::getAll();
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/fatura-imposto/templates/index-fatura-imposto.phtml');
    }

    public static function criarFaturaImposto()
    {
        $tipo = filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING);
        $tipo_fornecedor = $tipo == 'S' ? 'F' : 'C';
        $tipo_titulo = $tipo == 'S' ? 'Saída' : 'Entrada';
        if(!isset($_GET['tipo']) || empty($tipo)) {
            header ("Location: /financeiros/fatura-imposto/?action=index-fatura-imposto");
            exit();
        }
       
        $fornecedores = Fornecedor::getAllByTipo($tipo_fornecedor);
        $fornecedoresAtivos = Fornecedor::getAllAtivosByTipo($tipo_fornecedor);
        $tiposDocumentoFinanceiro = TipoDocumentoFinanceiro::getAll();
        $unidades = Filial::getUnidadesAtivas($_SESSION['empresa_user']);
        $naturezas = IPCF::getAllNatureza($tipo);
        $hoje = (new \DateTime())->format('Y-m-d');
        $assinaturas = Assinatura::getAll(); 
        $aplicacoes = AplicacaoFundos::getByListId([1,7,5,9,10]);
        

        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/fatura-imposto/templates/criar-fatura-imposto.phtml');
    }
    public static function pesquisarNFImpostosRetidosPendentes()
    {
        if(isset($_GET) && !empty($_GET)){
            $inicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $tr = filter_input(INPUT_GET, 'tr', FILTER_SANITIZE_NUMBER_INT);
            $tipo_nota = filter_input(INPUT_GET, 'tipo_nota', FILTER_SANITIZE_STRING);
            $fornecedor = filter_input(INPUT_GET, 'fornecedor', FILTER_SANITIZE_NUMBER_INT);
            $tipo_documento_financeiro = filter_input(INPUT_GET, 'tipo_documento_financeiro', FILTER_SANITIZE_NUMBER_INT);
            $num_nota = filter_input(INPUT_GET, 'num_nota', FILTER_SANITIZE_STRING);
            $filtros = [
                'inicio' => $inicio,
                'fim' => $fim,
                'tr' => $tr,
                'tipo_nota' => $tipo_nota,
                'fornecedor' => $fornecedor,
                'tipo_documento_financeiro' => $tipo_documento_financeiro,
                'empresa_user' => $_SESSION['empresa_user'],
                'not_in_tipo_documento_financeiro' => 25,
                'num_nota' => $num_nota
            ];
            $response 	= Notas::getParcelasPendentesByFiltros($filtros);
        }
        echo json_encode($response);
    }   
    
    public static function pesquisarNotasDeAglutinacao()
    {
        if(isset($_POST)){
            $inicio  = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim     = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $competencia_inicio  = filter_input(INPUT_POST, 'competencia_inicio', FILTER_SANITIZE_STRING);
            $competencia_fim     = filter_input(INPUT_POST, 'competencia_fim', FILTER_SANITIZE_STRING);
            $fornecedor  = filter_input(INPUT_POST, 'fornecedor', FILTER_SANITIZE_NUMBER_INT);
            $tipo_documento_financeiro        = filter_input(INPUT_POST, 'tipo_documento_financeiro', FILTER_SANITIZE_NUMBER_INT);
            $filtros = [
                'inicio'        => $inicio,
                'fim'           => $fim,
                'competencia_inicio' => $competencia_inicio,
                'competencia_fim' => $competencia_fim,
                'fornecedor'    => $fornecedor,
                'tipo_documento_financeiro'  => $tipo_documento_financeiro,
                'empresa_user' => $_SESSION['empresa_user']
            ];      
                
            $response 	= Notas::getNotasDeAglutinacao($filtros);
        }
        $fornecedores = Fornecedor::getAll();
        $tiposDocumentoFinanceiro = TipoDocumentoFinanceiro::getAll();
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/fatura-imposto/templates/index-fatura-imposto.phtml');
    }

    public static function criarNotaAglutinacao()
    {
        if(isset($_POST) && !empty($_POST)){
            $data = [];
            $fechamento_caixa = new \DateTime(\App\Controllers\FechamentoCaixa::getFechamentoCaixa()['data_fechamento']);
            $data['tipo_nota']  = filter_input(INPUT_POST, 'tipo_nota', FILTER_SANITIZE_STRING);
            $data['tipo_documento_nota']  = filter_input(INPUT_POST, 'tipo_documento_nota', FILTER_SANITIZE_NUMBER_INT);
            $data['numero_nota']  = filter_input(INPUT_POST, 'numero_nota', FILTER_SANITIZE_STRING);
            $data['natureza_nota']  = filter_input(INPUT_POST, 'natureza_nota', FILTER_SANITIZE_NUMBER_INT);
            $data['fornecedor_nota']  = filter_input(INPUT_POST, 'fornecedor_nota', FILTER_SANITIZE_NUMBER_INT);
            $data['ur_nota']  = filter_input(INPUT_POST, 'ur_nota', FILTER_SANITIZE_NUMBER_INT);
            $data['data_emissao']  = filter_input(INPUT_POST, 'data_emissao', FILTER_SANITIZE_STRING);
            $data['competencia']  = filter_input(INPUT_POST, 'competencia', FILTER_SANITIZE_STRING);
            $data['competencia_db'] = \DateTime::createFromFormat('m/Y', $data['competencia'])->format('Y-m');
            $data['data_vencimento']  = filter_input(INPUT_POST, 'data_vencimento', FILTER_SANITIZE_STRING);
            $data['descricao']  = filter_input(INPUT_POST, 'descricao', FILTER_SANITIZE_STRING);
            $data['descricao'] = mb_strtoupper($data['descricao'], 'UTF-8');
            $data['parcelas_id'] = $_POST['parcelas_id'];
            $data['parcelas_valor'] = $_POST['parcelas_valor'];
            $data['data_vencimento_real']  = filter_input(INPUT_POST, 'data_vencimento_real', FILTER_SANITIZE_STRING);
            $data['assinatura']  = filter_input(INPUT_POST, 'assinatura', FILTER_SANITIZE_NUMBER_INT);
            $data['forma_pagamento']  = filter_input(INPUT_POST, 'forma_pagamento', FILTER_SANITIZE_NUMBER_INT);
            $data_emissao_obj = new \DateTime($data['data_emissao']);
            
            if($data_emissao_obj->format('Y-m-d') <= $fechamento_caixa->format('Y-m-d')) {
                echo '0';
                return false;
            }

            $notaJaCadastrada = Notas::verificarNFJaExiste($data['fornecedor_nota'], $data['numero_nota']);
            if(!empty($notaJaCadastrada)){
                $response = ['tipo' => 'erro',
                'msg'=>"Conta já possui cadastro no sistema TR {$notaJaCadastrada['idNotas']}."

                ];
                echo json_encode($response);
                return;
            }

            $responseAglutinacao = Notas::criarNotaAglutinacao($data);
            if($responseAglutinacao != false) {
               
                $response = ['tipo' => 'acerto',
                'msg'=> "Nota criada com sucesso! TR {$responseAglutinacao}"

                ];
                echo json_encode($response);
                return;
            } else {
                
                $response = ['tipo' => 'erro',
                'msg'=>'Houve um problema ao criar a nota de aglutinação! Entre em contato com o TI!'

                ];
                echo json_encode($response);
                return;
            }
        }
    }

    public static function visualizarParcelasAglutinadas()
    {
        if(isset($_GET) && !empty($_GET)){
            $notaId  = filter_input(INPUT_GET, 'nota_id', FILTER_SANITIZE_NUMBER_INT);

            echo json_encode(Parcela::getParcelasAglutinadas($notaId));
        }
    }

    public static function desfazerNotaAglutinacao()
    {
        if(isset($_GET) && !empty($_GET)){
            $notaId  = filter_input(INPUT_GET, 'nota_id', FILTER_SANITIZE_NUMBER_INT);

            echo Notas::desfazerNotaAglutinacao($notaId);
        }
    }
}
