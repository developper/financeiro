<?php

namespace App\Controllers\CronJobs\Assistencial;

use \App\Core\Config;
use App\Models\DB\ConnMysqli;
use App\Helpers\Storage;
use App\Models\Enfermagem\Aditivo as AditivoEnf;
use App\Models\Enfermagem\PrescricaoCurativo;
use App\Models\Enfermagem\PrescricaoEnfermagem;
use App\Models\Medico\Aditivo as AditivoMed;

class Assistencial
{
    public static function cronPacientesComTresOuMaisAditivas()
    {
        ini_set('display_errors', 1);
        $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__).'/../');
        $auxDocRoot = explode('app/Controllers/CronJobs', $_SERVER['DOCUMENT_ROOT']);
        $_SERVER['DOCUMENT_ROOT'] = $auxDocRoot[0];

        $now = new \DateTime();
        $nowForFirstDay = clone $now;
        $nowForLastDay = clone $nowForFirstDay;
        $firstDayMonth = $nowForFirstDay->modify('first day of this month');
        $lastDayMonth = $nowForLastDay->modify('last day of this month');
        $prescAditivaMed = AditivoMed::getAllByPeriodo($firstDayMonth->format('Y-m-d'), $lastDayMonth->format('Y-m-d'));
        $relAditivoEnf = AditivoEnf::getAllByPeriodo($firstDayMonth->format('Y-m-d'), $lastDayMonth->format('Y-m-d'));
        $prescAditivaEnf = PrescricaoEnfermagem::getAllPrescEnfAditivaByPeriodo($firstDayMonth->format('Y-m-d'), $lastDayMonth->format('Y-m-d'));
        $prescCurativoAditivaEnf = PrescricaoCurativo::getAllPrescCurativoAditivaByPeriodo($firstDayMonth->format('Y-m-d'), $lastDayMonth->format('Y-m-d'));

        $data = array_merge(
            $prescAditivaMed,
            $relAditivoEnf,
            $prescAditivaEnf,
            $prescCurativoAditivaEnf
        );

        $html = self::montarTabelaCronAditivas(
            $data,
            $firstDayMonth->format('d/m/Y'),
            $lastDayMonth->format('d/m/Y')
        );

        $extension = 'pdf';
        /*if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
            $extension = 'xlsx';
        }*/

        $dir = $_SERVER['DOCUMENT_ROOT'].'/cron-jobs/assistencial/temp';

        $fileNameAtual = sprintf(
            "relatorio_aditivas_pacientes_%s_%s.%s",
            $firstDayMonth->format('d-m-Y'),
            $lastDayMonth->format('d-m-Y'),
            $extension
        );

        self::criarArquivoCronAditivas($html, $dir, $fileNameAtual);


        $fileNameAnterior = '';
        $sixDays = new \DateInterval('P6D');
        if($now >= $firstDayMonth && $now <= $firstDayMonth->add($sixDays)) {
            $prevNow = new \DateTime();
            $nowForPrevMonthFirstDay = clone $prevNow;
            $nowForPrevMonthLastDay = clone $nowForPrevMonthFirstDay;
            $prevMonthFirstDay = $nowForPrevMonthFirstDay->modify('first day of last month');
            $prevMonthLastDay = $nowForPrevMonthLastDay->modify('last day of last month');
            $prescAditivaMed = AditivoMed::getAllByPeriodo($prevMonthFirstDay->format('Y-m-d'), $prevMonthLastDay->format('Y-m-d'));
            $relAditivoEnf = AditivoEnf::getAllByPeriodo($prevMonthFirstDay->format('Y-m-d'), $prevMonthLastDay->format('Y-m-d'));
            $prescAditivaEnf = PrescricaoEnfermagem::getAllPrescEnfAditivaByPeriodo($prevMonthFirstDay->format('Y-m-d'), $prevMonthLastDay->format('Y-m-d'));
            $prescCurativoAditivaEnf = PrescricaoCurativo::getAllPrescCurativoAditivaByPeriodo($prevMonthFirstDay->format('Y-m-d'), $prevMonthLastDay->format('Y-m-d'));

            $fileNameAnterior = sprintf(
                "relatorio_aditivas_pacientes_%s_%s.%s",
                $prevMonthFirstDay->format('d-m-Y'),
                $prevMonthLastDay->format('d-m-Y'),
                $extension
            );

            $data = array_merge(
                $prescAditivaMed,
                $relAditivoEnf,
                $prescAditivaEnf,
                $prescCurativoAditivaEnf
            );

            $html = self::montarTabelaCronAditivas(
                $data,
                $prevMonthFirstDay->format('d/m/Y'),
                $prevMonthLastDay->format('d/m/Y'),
                'Anterior'
            );
            self::criarArquivoCronAditivas($html, $dir, $fileNameAnterior);
        }

        return [
            $dir,
            $fileNameAtual,
            $fileNameAnterior
        ];
    }

    private static function montarTabelaCronAditivas($data, $inicio, $fim, $tipo = 'Atual')
    {
        $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__).'/../');
        $auxDocRoot = explode('app/Controllers/CronJobs', $_SERVER['DOCUMENT_ROOT']);
        $_SERVER['DOCUMENT_ROOT'] = $auxDocRoot[0];

        $now = new \DateTime();
        $aux = [];
        $validos = [];
        foreach ($data as $key => $row) {
            $aux[$row['idPaciente']][] = $row;

            if(count($aux[$row['idPaciente']]) > 2) {
                $validos[$row['idPaciente']] = count($aux[$row['idPaciente']]);
            }
        }

        $response = [];
        foreach ($aux as $row) {
            foreach ($row as $key => $subrow) {
                $response[] = $subrow;
            }
        }

        $orderByDate = function ($current, $next) {
            return strtotime($current['dataSistema']) >= strtotime($next['dataSistema']) ? -1 : 1;
        };

        usort($response, $orderByDate);

        $cabecalho = <<<HTML
<tr>
    <td><img src="{$_SERVER['DOCUMENT_ROOT']}/utils/logo2.jpg" width="200"></td>
    <td colspan="5" align="center">
        <b>RESUMO DO PER&Iacute;ODO {$inicio} &agrave; {$fim}</b>
    </td>
</tr>
HTML;

        $css = '/cron-jobs/assistencial/css/pdf.css';
        $html = <<<HTML
<html>
<head>
    <link rel="stylesheet" href="{$css}">
</head>
<body>
    <table align="center">
    <thead>
    <tr>
        <th colspan="6">Itens em <font color="red">Vermelho</font> s&atilde;o anteriores &agrave; semana do resumo!</th>
    </tr>
    {$cabecalho}
    <tr>
        <th style='border: 1px solid darkgrey' align="center"><strong>Criado Em</strong></th>
        <th style='border: 1px solid darkgrey' align="center"><strong>C&oacute;digo/Paciente</strong></th>
        <th style='border: 1px solid darkgrey' align="center"><strong>Per&iacute;odo</strong></th>
        <th style='border: 1px solid darkgrey' align="center"><strong></strong>UR</th></th>
        <th style='border: 1px solid darkgrey' align="center"><strong>Conv&ecirc;nio</strong></th>
        <th style='border: 1px solid darkgrey' align="center"><strong>Criado Por</strong></th>
    </tr>
    </thead>
    <tbody>
HTML;
        $sevenDaysAgoFromNow = $now->modify('-7 days');
        $counts = [];
        $i = 1;
        foreach ($response as $item) {
            if (isset($validos[$item['idPaciente']])) {
                $counts[$item['nomePaciente']] = $validos[$item['idPaciente']];
                $dataSistema = \DateTime::createFromFormat('Y-m-d H:i:s', $item['dataSistema']);
                $inicio = \DateTime::createFromFormat('Y-m-d', $item['inicio']);
                $fim = \DateTime::createFromFormat('Y-m-d', $item['fim']);
                $nomePaciente = htmlentities($item['nomePaciente']);
                $nomeUsuario = htmlentities($item['usuario']);
                $convenio = htmlentities($item['convenio']);
                $ur = htmlentities($item['ur']);

                $cor = 'style="color: #000"';
                if($dataSistema < $sevenDaysAgoFromNow) {
                    $cor = 'style="color: #cd5c5c"';
                }

                $bgcolor = "#eee";
                if($i % 2 == 0) {
                    $bgcolor = "#fff";
                }

                $html .= <<<HTML
<tr bgcolor="{$bgcolor}" style="border: 1px solid darkgrey">
    <td style="border: 1px solid darkgrey" {$cor}>{$dataSistema->format('d/m/Y \a\s H:i:s')}</td>
    <td style="border: 1px solid darkgrey" {$cor}>{$nomePaciente}</td>
    <td style="border: 1px solid darkgrey" {$cor}>{$inicio->format('d/m/Y')} &agrave; {$fim->format('d/m/Y')}</td>
    <td style="border: 1px solid darkgrey" {$cor}>{$ur}</td>
    <td style="border: 1px solid darkgrey" {$cor}>{$convenio}</td>
    <td style="border: 1px solid darkgrey" {$cor}>{$nomeUsuario}</td>
</tr>                
HTML;
                $i++;
            }
        }
        $html .= '</tbody></table>';

        $html .= '<br><br>

                    <div style="page-break-before: always;"></div>
                    
                    <table style="border: 1px solid darkgrey">
                    <thead>
                    <tr>
                      <th colspan="2"><b>Total por Paciente</b></th>
                    </tr>
                    </thead>
                    <tbody>';
        $i = 1;
        ksort($counts, SORT_ASC);
        foreach ($counts as $paciente => $count) {
            $bgcolor = "#eee";
            if($i % 2 == 0) {
                $bgcolor = "#fff";
            }
            $html .= "<tr bgcolor='{$bgcolor}'>
                        <td style='border: 1px solid darkgrey'>{$paciente}</td>
                        <td style='border: 1px solid darkgrey' align='center'>{$count}</td>
                      </tr>";
            $i++;
        }
        $html .= '</tbody></table></body></html>';

        return $html;
    }

    private static function criarArquivoCronAditivas($html, $dir, $filename)
    {
        $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__).'/../');
        $auxDocRoot = explode('app/Controllers/CronJobs', $_SERVER['DOCUMENT_ROOT']);
        $_SERVER['DOCUMENT_ROOT'] = $auxDocRoot[0];

        include_once($_SERVER['DOCUMENT_ROOT'] . '/utils/mpdf/mpdf.php');

        $file = $_SERVER['DOCUMENT_ROOT'] . '/cron-jobs/assistencial/pacientes-3-mais-aditivas.phtml';
        file_put_contents($file, $html);

        $mpdf = new \mPDF('pt','A4-L', 10);
        $mpdf->WriteHTML("<html><body>");
        $mpdf->WriteHTML(file_get_contents($file));
        $mpdf->WriteHTML("</body></html>");
        $mpdf->Output($dir . '/' . $filename,'F');
    }
}