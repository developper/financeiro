<?php

namespace App\Controllers\CronJobs\Financeiro;

use App\Models\Financeiro\OrcamentoFinanceiro;
use DateTime;
use \GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;



class FinanceiroIW
{


    private static function getValoresIWJson()
    {
        $date = date('Y-m-d');
        if( isset($_GET['data']) ){
            $date = $_GET['data'];
        }
                

        try {
            //if (!isset($_SESSION['iw_api_token'])) {
               
                $options = [
                    'json' => [
                        'email' => 'sisassiste@assistevida.com',
                        'password' => 'Kriska#@2019',
                    ],
                    'verify' => false
                ];
                  $client = new Client(["base_url" => "https://sisassiste.assistevida.com.br:16032"]);
               // $client = new Client(["base_url" => "https://stag.assistevida.com.br:1010"]);
                //$client = new Client(["base_url" => "https://sisassiste.assistevida.com.br:16032"]);
                $response = $client->post("/api/login", $options);
                
                $token = json_decode($response->getBody()->getContents());
                $_SESSION['iw_api_token'] = $token;
               
          //  }
         
            if ($_SESSION['iw_api_token']->access_token != '') {
                $headers = [
                    'headers' => [
                        'Authorization' => "Bearer {$_SESSION['iw_api_token']->access_token}",
                        'Accept' => 'application/json',

                    ],
                    'query' => ['date' => $date]

                ];
                $client = new Client(["base_url" => "https://sisassiste.assistevida.com.br:16032"]);
                $response = $client->get("/api/iw/financialData", $headers);
                
                $content = $response->getBody()->getContents(); 
                
           
                 return self::arrangeByItem(json_decode($content, true), $date);
            }
            return 'ok';


            throw new \HttpRequestException('Não autorizado!');
        } catch (ClientException $clientException) {
            
            return $clientException->getCode();
        }
    }

    private static function arrangeByItem($itens, $date)
    {
        
        /*
         topico : Glosa = 86, Home Care = 2
         Naturezas
            Materiais = 38,
            Medicamentos = 39, 
            Dietas = 40,
            curativos = 41,
            Fralda = 42       
        
        */
        
        $idItens = [
            'glosa' => 86,
            'Home' => 2,
            'Dietas' => 40,
            'Fralda' => 42,
            'Mat. Enfermagem' => 38,
            'Medicamentos' => 39,
            'Curativo' => 41
        ];

        
        


        $response = [];
        foreach ($itens as $key => $item) {
            if ($key == 'glosa') {
                foreach ($item as $i) {
                    $response['topicos'][] = [
                        'topico_id' => 86,
                        'valor' => -1 * $i['vlrglosado'],
                        'mesano' => $i['mesano']
                    ];
                }
            } elseif ($key == 'Home') {
                foreach ($item as $i) {
                    $response['topicos'][] = [
                        'topico_id' => 2,
                        'valor' => $i['vlsoma'],
                        'mesano' => $i['mesano']
                    ];
                }
            } elseif ($key == 'BIEstoque') {
                
                foreach ($item as $keyTipo => $competencia) {
                    $idNatureza =$idItens[$keyTipo];
                    foreach($competencia as $keyCompetencia => $comp){
                       $valorEstoqueInicio =  $comp['begin']['sum(vl_saldo)'];
                       $valorEstoqueFim = $comp['end']['sum(vl_saldo)'];
                       $valorEntradaIW = isset($itens['BIEntrada'][$keyTipo][$keyCompetencia]) ? $itens['BIEntrada'][$keyTipo][$keyCompetencia]['valor']: 0;
                       $valorDevolucaoIW = isset($itens['BIDevolucao'][$keyTipo][$keyCompetencia]) ? $itens['BIDevolucao'][$keyTipo][$keyCompetencia]['valor']: 0;
                       $valorEstoque = ($valorEstoqueInicio + $valorEntradaIW + $valorDevolucaoIW) - $valorEstoqueFim;
                      
                       $valorEstoque = $valorEstoque > 0 ? $valorEstoque * -1 : $valorEstoque;
                       $response['naturezas'][] = [
                        'natureza_id' => $idNatureza,
                        'valor' => $valorEstoque,
                        'mesano' => $keyCompetencia
                         ];
                    }
                    
                    
                }

            }
        }

        
        $criadoPor = (isset($_SESSION['id_user']) && !empty($_SESSION['id_user'])) ? $_SESSION['id_user'] : 0;
        $insertPlanoContas = [];
        $insertTopicos = [];
      
        foreach ($response as $key => $resp) {
            if ($key == 'naturezas') {

                foreach ($resp as $r) {
                    $dados = OrcamentoFinanceiro::getValorPlanoContaGerencialAutomatico($r);
                   
                    if (empty($dados)) {
                        $insertPlanoContas[] = "(
                            {$r['valor']},
                            {$r['natureza_id']},
                            '{$r['mesano']}',
                            now(),
                            {$criadoPor}
                        )";
                    } else {                     
                        $responseUpdate = OrcamentoFinanceiro::updateValoresGerencialPlanoContasAutomatico(['id' => $dados[0]['id'], 'valor' => $r['valor']]);
                    }
                }
            } else {
                foreach ($resp as $r) {

                    $dados = OrcamentoFinanceiro::getValorTopicoGerencialAutomatico($r);
                    
                    if (empty($dados)) {
                        $insertTopicos[] = "(
                            {$r['valor']},
                            {$r['topico_id']},
                            '{$r['mesano']}',
                            now(),
                            {$criadoPor}
                        )";
                    } else {
                        
                        
                        $responseUpdate = OrcamentoFinanceiro::updateValoresGerencialTopicoAutomatico(['id' => $dados[0]['id'], 'valor' => $r['valor']]);
                    }
                }
            }
        }
       

        if (!empty($insertPlanoContas)) {
            OrcamentoFinanceiro::createPlanoContasValoresGerenciaisAutomatico($insertPlanoContas);
        }

        if (!empty($insertTopicos)) {
            OrcamentoFinanceiro::createTopicosValoresGerenciaisAutomatico($insertTopicos);
        }
       

        return 1;
    }

    public static function cronFinanceiroIW()
    {

        try {
            $valorFinanceiroIW = self::getValoresIWJson();
            if ($valorFinanceiroIW <> 1) {
                echo 'Não Autorizado!';
                return false;
            }
        } catch (\HttpRequestException $exception) {
            echo $exception->getMessage();
            return false;
        }


        echo '1';
    }

    private static function getValuesNatureza($date)
    {
        $fim = date('Y-m', strtotime("-1 day", strtotime($date)));              
        $inicio = date('Y-01', strtotime("-3 month", strtotime($date)));

        $idNaturezas = "38, 39, 40, 41, 42";
        $valueNatrezas = [];

        $response = OrcamentoFinanceiro::getGerencialAutomatizadoByData($inicio, $fim, $idNaturezas);
        foreach ($response as $resp) {
           
            $valueNatrezas[$resp['ID_NATUREZA']][$resp['data_competencia']] += round($resp['VALOR'], 2);
        }
         
        return $valueNatrezas;
    }

   
}
