<?php

namespace app\Controllers;

use \App\Models\Sistema\Solicitacoes;
use \App\Services\Cache\SimpleCache;
use \App\Services\CacheAdapter;
use App\Helpers\StringHelper as String;

class BuscarSolicitacoes
{
	public static function buscarSolicitacoesNaoAuditadasPorEmpresa($empresa)
	{
		$html 		= "";

		$keyAdAvEm = sprintf('solicitacoes-pendentes-ad-av-em-ur-%s', $empresa);
		$keyPeriodicas = sprintf('solicitacoes-pendentes-periodicas-ur-%s', $empresa);
		$dir = __DIR__ . '/../../auditoria/cache';

		$cache = new CacheAdapter(new SimpleCache($dir));
		$cachedAdAvEm = $cache->adapter->read($keyAdAvEm);
		$cachedPeriodicas = $cache->adapter->read($keyPeriodicas);

		if(!$cachedAdAvEm || !$cachedPeriodicas) {
			list($solicitacoesAdAvEm, $solicitacoesPeriodicas) = Solicitacoes::getSolicitacoesPendentesByEmpresaId ($empresa);
			$cache->adapter->save($keyAdAvEm, $solicitacoesAdAvEm);
			$cache->adapter->save($keyPeriodicas, $solicitacoesPeriodicas);
		} else {
			$solicitacoesAdAvEm = $cachedAdAvEm;
			$solicitacoesPeriodicas = $cachedPeriodicas;
		}

		$html .= '<div style="display: table;" class="solicitacoes-pendentes-tipos-tabs">';
		$html .= '<ul>';
		$html .= "<li><a class='tabs-tipos' href='#tabs-aditivas-avulsas-emergencias-{$empresa}'>Aditivas/Avulsas/Emergências</a></li>";
		$html .= "<li><a class='tabs-tipos' href='#tabs-periodicas-{$empresa}'>Periódicas</a></li>";
		$html .= '</ul>';
		$html .= "<div id='tabs-aditivas-avulsas-emergencias-{$empresa}'>";
		if(count($solicitacoesAdAvEm) > 0) {
			foreach ($solicitacoesAdAvEm as $row) {
				if ($row['pend'] > 0) {
					$pendente = "Pendencia";
				} else {
					$pendente = "";
				}
				$html .= "<div style='border:1px solid #ccc;height:200px;width:240px;display:table;float:left;margin:10px; padding:15px; font-size:12px;' class='wrapper'>";
				if ($row['nao_visualizado'] > 0) {
					$qtd_nao_visualizado = str_pad($row['nao_visualizado'], 2, '0', STR_PAD_LEFT);
					$html .= "<div class='ribbon-wrapper-green'><div class='ribbon-green'> {$qtd_nao_visualizado} Novo(s)</div></div>";
				}

				$class_button_paciente = $row['primeira_prescricao'] == 0 ? "bpaciente" : "bpaciente_prioridade";

				$html .= "<table width='100%'>";
				$html .= "<tr>";
				$html .= "<td color='red'>";
				$html .= "<center><span style='color:red'><b>$pendente</b></span></center>";
				$html .= "</td>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<td>";
				$html .= "<center><button class='{$class_button_paciente}' p='{$row['paciente']}' idP='{$row['idP']}' empresa={$row['empresa']}></button></center>";
				$html .= "</td>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<td>";
				$html .= "<center><b>" . strtoUpper(String::removeAcentosViaEReg($row['nome'])) . "</b></center>";
				$html .= "</td>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<td>";
				$html .= "<center><span style='color:blue'><b>" . strtoUpper($row['convenio']) . "</b></span></center>";
				$html .= "</td>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<tr>";
				$html .= "<td>";
				$html .= "<center><span style='color:blue'><b>" . strtoUpper($row['unidade_regional']) . "</b></span></center>";
				$html .= "</td>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<td>";
				$html .= "<center>" . (isset($row['data']) ? implode('/', array_reverse(explode("-", $row['data']))) : '') . "</center>";
				$html .= "</td>";
				$html .= "</tr>";
				$html .= "</tr>";
				$html .= "</table>";
				$html .= "</div>";
			}
		} else {
			$html .= "<p id='vazio' style='text-align:center;color:red;'><b>Nenhuma solicita&ccedil;&atilde;o pendente!</b></p>";
		}
		$html .= "</div>";
		$html .= "<div id='tabs-periodicas-{$empresa}'>";
		if(count($solicitacoesPeriodicas) > 0) {
			foreach ($solicitacoesPeriodicas as $row) {
				if ($row['pend'] > 0) {
					$pendente = "Pendencia";
				} else {
					$pendente = "";
				}
				$html .= "<div style='border:1px solid #ccc;height:200px;width:240px;display:table;float:left;margin:10px; padding:15px; font-size:12px;' class='wrapper'>";
				if ($row['nao_visualizado'] > 0) {
					$qtd_nao_visualizado = str_pad($row['nao_visualizado'], 2, '0', STR_PAD_LEFT);
					$html .= "<div class='ribbon-wrapper-green'><div class='ribbon-green'> {$qtd_nao_visualizado} Novo(s)</div></div>";
				}

				$class_button_paciente = $row['primeira_prescricao'] == 0 ? "bpaciente" : "bpaciente_prioridade";

				$html .= "<table width='100%'>";
				$html .= "<tr>";
				$html .= "<td color='red'>";
				$html .= "<center><span style='color:red'><b>$pendente</b></span></center>";
				$html .= "</td>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<td>";
				$html .= "<center><button class='{$class_button_paciente}' p='{$row['paciente']}' idP='{$row['idP']}' empresa={$row['empresa']}></button></center>";
				$html .= "</td>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<td>";
				$html .= "<center><b>" . strtoUpper(String::removeAcentosViaEReg($row['nome'])) . "</b></center>";
				$html .= "</td>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<td>";
				$html .= "<center><span style='color:blue'><b>" . strtoUpper($row['convenio']) . "</b></span></center>";
				$html .= "</td>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<tr>";
				$html .= "<td>";
				$html .= "<center><span style='color:blue'><b>" . strtoUpper($row['unidade_regional']) . "</b></span></center>";
				$html .= "</td>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<td>";
				$html .= "<center>" . (isset($row['data']) ? implode('/', array_reverse(explode("-", $row['data']))) : '') . "</center>";
				$html .= "</td>";
				$html .= "</tr>";
				$html .= "</tr>";
				$html .= "</table>";
				$html .= "</div>";
			}
		} else {
			$html .= "<p id='vazio' style='text-align:center;color:red;'><b>Nenhuma solicita&ccedil;&atilde;o pendente!</b></p>";
		}
		$html .= "</div>";

		$html .= "
<script type='text/javascript'>
	$(function(){
		$('.solicitacoes-pendentes-tipos-tabs').tabs();
	});
</script>
";

		return $html;
	}
}