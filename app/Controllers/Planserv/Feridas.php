<?php
namespace app\Controllers\Planserv;

use \App\Core\Config;
use \App\Models\Administracao\Filial;
use \App\Models\Administracao\Usuario;
use \App\Models\Enfermagem\Evolucao AS EvolucaoEnf;
use \App\Models\Medico\Evolucao AS EvolucaoMed;
use \App\Models\Planserv\CorBordaFerida;
use \App\Models\Planserv\EtiologiaFerida;
use \App\Models\Planserv\PlanoFeridas;
use \App\Models\Planserv\TipoExsudato;
use \App\Models\Planserv\VolumeExsudato;
use	\App\Models\Sistema\Frequencia;
use	\App\Models\Administracao\Paciente;
use \App\Models\Enfermagem\Feridas as FeridasEnf;
use \App\Services\Gateways\SendGridGateway;
use \App\Services\MailAdapter;

class Feridas
{
    const DIR = __DIR__;

    public static function formListarPlanoFeridas()
    {
        $pacientes 	= Paciente::getAll();
        $urs = Filial::getAll();

        require ($_SERVER['DOCUMENT_ROOT'] . '/enfermagem/planserv/templates/plano-feridas/listar.phtml');
    }

    public static function buscarPlanoFeridas()
    {
        $pacientes 	= Paciente::getAll();
        $urs = Filial::getAll();

        if(isset($_POST) && !empty($_POST)) {
            $pacienteId = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);

            $response = PlanoFeridas::listAll($pacienteId, $inicio, $fim);
        }

        require ($_SERVER['DOCUMENT_ROOT'] . '/enfermagem/planserv/templates/plano-feridas/listar.phtml');
    }

    public static function formNovoPlanoFeridas()
    {
        $pacientes 	        = Paciente::getAll();
        $usuariosMed        = Usuario::getUsersByRole('modmed','todos');
        $usuariosEnf        = Usuario::getUsersByRole('modenf','todos');
        $locais             = FeridasEnf::getLocaisFeridas();
        $etiologias         = EtiologiaFerida::getAll();
        $tiposExsudato      = TipoExsudato::getAll();
        $volumesExsudato    = VolumeExsudato::getAll();
        $coresBorda         = CorBordaFerida::getAll();
        $frequencias        = self::getAllFrequencias();

        require ($_SERVER['DOCUMENT_ROOT'] . '/enfermagem/planserv/templates/plano-feridas/novo.phtml');
    }

    public static function getAllLocaisFeridas()
    {
        return Feridas::getLocaisFeridas();
    }

    public static function getAllFrequencias()
    {
        return Frequencia::getFrequenciasPrescricaoCurativo();
    }

    public static function salvarPlanoFeridas()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = $_POST;
            $response = PlanoFeridas::save($data);
            if($response['result']) {
                $msg = "Ficha salva com sucesso! Clique <a href='?action=imprimir-plano-feridas&id={$response['id']}' target='_blank'>aqui</a> para imprimir!";
                $type = 's';

                $dados = PlanoFeridas::getPlanoFeridas($response['id']);
                $para = [];
                $titulo = "({$dados['paciente_nome']}) Novo plano de tratamento de feridas Planserv disponível - SisMederi";
                $msgHTML = "<p>O paciente <b>{$dados['paciente_nome']}</b> possui um novo plano de tratamento de feridas do Planserv. <br> Criado por: {$dados['usuario']} </p>.";

                $para[] = ['email' => 'enfermagem.urfsa@mederi.com.br', 'nome' => 'Enfermagem - FSA'];
                $para[] = ['email' => 'secretaria.feira@mederi.com.br', 'nome' => 'Secretaria UR FSA'];
                self::enviarEmailSimples($titulo, $msgHTML, $para);
            } else {
                $msg = "Houve um problema ao salvar essa ficha!";
                $type = 'f';
            }

            $pacientes 	        = Paciente::getAll();
            $locais             = FeridasEnf::getLocaisFeridas();
            $etiologias         = EtiologiaFerida::getAll();
            $tiposExsudato      = TipoExsudato::getAll();
            $volumesExsudato    = VolumeExsudato::getAll();
            $coresBorda         = CorBordaFerida::getAll();
            $frequencias        = self::getAllFrequencias();

            require ($_SERVER['DOCUMENT_ROOT'] . '/enfermagem/planserv/templates/plano-feridas/novo.phtml');
        }
    }

    public static function atualizarPlanoFeridas()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = $_POST;
            $response = PlanoFeridas::update($data);
            if($response) {
                $msg = base64_encode("Ficha atualizada com sucesso!");
                $type = 's';
            } else {
                $msg = base64_encode("Houve um problema ao atualizar essa ficha!");
                $type = 'f';
            }
            header("Location: ?action=feridas&msg={$msg}&type={$type}");
        }
    }

    public static function formEditarPlanoFeridas()
    {
        $id         = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
        $response   = [];

        $response['dados'] = PlanoFeridas::getPlanoFeridas($id);
        $response['dados']['data_nasc'] = \DateTime::createFromFormat('Y-m-d', $response['dados']['data_nasc'])->format('d/m/Y');
        $response['dados']['data_admissao'] = \DateTime::createFromFormat('Y-m-d H:i:s', $response['dados']['data_admissao'])->format('d/m/Y');
        $response['dados']['sexo'] = $response['dados']['sexo'] == 0 ? 'Masculino' : 'Feminino';

        $response['profissionais'] = PlanoFeridas::getPlanoFeridasProfissionais($id);

        $response['beneficiario'] = PlanoFeridas::getPlanoFeridasBeneficiario($id);
        $ultimaEvolucao = EvolucaoEnf::getUltimoRelatorioPaciente($response['dados']['paciente']);
        $response['dados']['data_avaliacao'] = '';
        if(isset($ultimaEvolucao['DATA_EVOLUCAO']) && !empty($ultimaEvolucao['DATA_EVOLUCAO'])) {
            $response['dados']['data_avaliacao'] = \DateTime::createFromFormat('Y-m-d', $ultimaEvolucao['DATA_EVOLUCAO'])->format('d/m/Y');
        }

        $response['historico'] = PlanoFeridas::getPlanoFeridasHistorico($id);

        $response['historico']['incontinencia'] = PlanoFeridas::getPlanoFeridasIncontinencia($id);

        $incontinencias = [];
        foreach ($response['historico']['incontinencia'] as $incontinencia) {
            $incontinencias[] = $incontinencia['incontinencia_id'];
        }

        $response['historico']['doencas_sistemicas'] = PlanoFeridas::getPlanoFeridasDoencasSistemicas($id);

        $doencas_sistemicas = [];
        foreach ($response['historico']['doencas_sistemicas'] as $doenca_sistemica) {
            $doencas_sistemicas[] = $doenca_sistemica['doenca_sistemica'];
        }

        $response = array_merge($response, self::locaisFeridas($id), self::coberturas($id));

        $pacientes 	        = Paciente::getAll();
        $usuariosMed        = Usuario::getUsersByRole('modmed','todos');
        $usuariosEnf        = Usuario::getUsersByRole('modenf','todos');
        $locais             = FeridasEnf::getLocaisFeridas();
        $etiologias         = EtiologiaFerida::getAll();
        $tiposExsudato      = TipoExsudato::getAll();
        $volumesExsudato    = VolumeExsudato::getAll();
        $coresBorda         = CorBordaFerida::getAll();
        $frequencias        = self::getAllFrequencias();

        $target = $_GET['action'] == 'feridas-usar-para-nova' ? 'feridas-salvar' : 'feridas-atualizar';

        require ($_SERVER['DOCUMENT_ROOT'] . '/enfermagem/planserv/templates/plano-feridas/editar.phtml');
    }

    public static function imprimirPlanoFeridas()
    {
        $id         = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
        $response   = [];

        $response['dados'] = PlanoFeridas::getPlanoFeridas($id);
        $response['dados']['inicio'] = \DateTime::createFromFormat('Y-m-d', $response['dados']['inicio'])->format('d/m/Y');
        $response['dados']['fim'] = \DateTime::createFromFormat('Y-m-d', $response['dados']['fim'])->format('d/m/Y');
        $response['dados']['data_nasc'] = \DateTime::createFromFormat('Y-m-d', $response['dados']['data_nasc'])->format('d/m/Y');
        $response['dados']['data_admissao'] = \DateTime::createFromFormat('Y-m-d H:i:s', $response['dados']['data_admissao'])->format('d/m/Y');
        $response['dados']['sexo'] = $response['dados']['sexo'] == 0 ? 'Masculino' : 'Feminino';

        $ultimaEvolucaoMed = EvolucaoMed::getUltimoRelatorioPaciente($response['dados']['paciente']);
        $ultimaEvolucaoEnf = EvolucaoEnf::getUltimoRelatorioPaciente($response['dados']['paciente']);

        $response['profissionais'] = PlanoFeridas::getPlanoFeridasProfissionais($id);
        $response['profissionais']['assinatura_medico'] = substr_replace(Usuario::getAssinaturaPath($response['profissionais']['medico'])['ASSINATURA'], '', 0, 2);
        $response['profissionais']['assinatura_enf'] = substr_replace(Usuario::getAssinaturaPath($response['profissionais']['enfermeiro'])['ASSINATURA'], '', 0, 2);

        $response['beneficiario'] = PlanoFeridas::getPlanoFeridasBeneficiario($id);
        $response['beneficiario']['data_avaliacao'] = $ultimaEvolucaoEnf['DATA_EVOLUCAO'] != '' ? \DateTime::createFromFormat('Y-m-d', $ultimaEvolucaoEnf['DATA_EVOLUCAO'])->format('d/m/Y'): '';

        $response['historico'] = PlanoFeridas::getPlanoFeridasHistorico($id);

        $response['historico']['doencas_sistemicas'] = PlanoFeridas::getPlanoFeridasDoencasSistemicas($id);

        $response['historico']['incontinencia'] = PlanoFeridas::getPlanoFeridasIncontinencia($id);

        $hospitalar = $response['beneficiario']['modalidade'] == 1 ? 'x' : '';
        $ambulatorial = $response['beneficiario']['modalidade'] == 2 ? 'x' : '';
        $domiciliar = $response['beneficiario']['modalidade'] == 3 ? 'x' : '';

        $isolamentoSim = $response['beneficiario']['isolamento'] == '1' ? 'x' : '';
        $isolamentoNao = $response['beneficiario']['isolamento'] == '2' ? 'x' : '';

        $deambulaSim = $response['beneficiario']['deambula'] == '1' ? 'x' : '';
        $deambulaNao = $response['beneficiario']['deambula'] == '2' ? 'x' : '';

        $doencas_sistemicas = [];

        foreach ($response['historico']['doencas_sistemicas'] as $doenca_sistemica) {
            $doencas_sistemicas[] = $doenca_sistemica['doenca_sistemica'];
        }

        $hipertensao = in_array(1, $doencas_sistemicas) ? 'x' : '';
        $vasculopatia = in_array(2, $doencas_sistemicas) ? 'x' : '';
        $neoplasia = in_array(3, $doencas_sistemicas) ? 'x' : '';
        $cardiopatia = in_array(4, $doencas_sistemicas) ? 'x' : '';
        $diabetes = in_array(5, $doencas_sistemicas) ? 'x' : '';
        $pneumonia = in_array(6, $doencas_sistemicas) ? 'x' : '';
        $alergias = in_array(7, $doencas_sistemicas) ? 'x' : '';
        $cirurgica = in_array(8, $doencas_sistemicas) ? 'x' : '';

        $incontinencias = [];

        foreach ($response['historico']['incontinencia'] as $incontinencia) {
            $incontinencias[] = $incontinencia['incontinencia_id'];
        }

        $urinaria = in_array(1, $incontinencias) ? 'x' : '';
        $fecal = in_array(2, $incontinencias) ? 'x' : '';

        $camara = $response['historico']['tratamento_atual'] == '1' ? 'x' : '';
        $coberturas = $response['historico']['tratamento_atual'] == '2' ? 'x' : '';

        $infeccaoSim = $response['historico']['infeccao'] == '1' ? 'x' : '';
        $infeccaoNao = $response['historico']['infeccao'] == '2' ? 'x' : '';
        $microorganismo = $response['historico']['infeccao'] == '3' ? 'x' : '';

        $response = array_merge($response, self::locaisFeridasImpressao($id), self::coberturasImpressao($id));

        $img_feridas = PlanoFeridas::getPlanoFeridasImgs($ultimaEvolucaoEnf['ID']);
        if($img_feridas[0]['full_path'] == '') {
            $img_feridas_prorrogacao = PlanoFeridas::getPlanoFeridasImgsFromProrrogacao([
                'inicio' => $response['dados']['inicio'],
                'fim' => $response['dados']['fim'],
                'paciente_id' => $response['dados']['paciente']
            ]);
        }

        require ($_SERVER['DOCUMENT_ROOT'] . '/enfermagem/planserv/templates/plano-feridas/imprimir.php');
    }

    private static function locaisFeridas($id)
    {
        $response = [];

        $locais = PlanoFeridas::getPlanoFeridasLocais($id);
        $i = 0;

        foreach ($locais as $local) {
            $response['locais'][$i]['id'] = $local['id'];
            $response['locais'][$i]['local'] = $local['local_ferida_id'];
            $response['locais'][$i]['lado'] = $local['lado'];
            $response['locais'][$i]['etiologia'] = $local['etiologia_ferida_id'];
            $response['locais'][$i]['comprimento'] = $local['comprimento'];
            $response['locais'][$i]['largura'] = $local['largura'];
            $response['locais'][$i]['profundidade'] = $local['profundidade'];
            $response['locais'][$i]['descolamento'] = $local['descolamento'];
            $response['locais'][$i]['tecido_granulacao'] = $local['tecido_granulacao'];
            $response['locais'][$i]['tecido_esfacelo'] = $local['tecido_esfacelo'];
            $response['locais'][$i]['tecido_fibrina'] = $local['tecido_fibrina'];
            $response['locais'][$i]['tecido_necrotico'] = $local['tecido_necrotico'];
            $response['locais'][$i]['exposicao'] = $local['exposicao'];
            $response['locais'][$i]['tipo_exsudato'] = $local['tipo_id'];
            $response['locais'][$i]['volume_exsudato'] = $local['volume_id'];
            $response['locais'][$i]['odor'] = $local['odor'];
            $response['locais'][$i]['cor'] = $local['cor_id'];
            $response['locais'][$i]['edema'] = $local['edema'];
            $response['locais'][$i]['maceracao'] = $local['maceracao'];
            $response['locais'][$i]['bordas'] = $local['bordas'];
            $response['locais'][$i]['instrumental'] = $local['instrumental'];
            $response['locais'][$i]['cirurgico'] = $local['cirurgico'];

            $i++;
        }

        return $response;
    }

    private static function locaisFeridasImpressao($id)
    {
        $response = [];
        $lados    = [
            0 => '',
            1 => 'Direito',
            2 => 'Esquerdo',
        ];
        $simNao   = [
            1 => 'Sim',
            2 => 'Não',
        ];
        $bordas   = [
            1 => 'Regular',
            2 => 'Irregular',
        ];


        $locais = PlanoFeridas::getPlanoFeridasLocais($id);
        $i = 0;

        foreach ($locais as $local) {
            $response['locais'][$i]['id'] = $local['id'];
            $response['locais'][$i]['local'] = $local['local_ferida'];
            $response['locais'][$i]['lado'] = $lados[$local['lado']];
            $response['locais'][$i]['etiologia'] = $local['etiologia_ferida'];
            $response['locais'][$i]['comprimento'] = $local['comprimento'];
            $response['locais'][$i]['largura'] = $local['largura'];
            $response['locais'][$i]['profundidade'] = $local['profundidade'];
            $response['locais'][$i]['descolamento'] = $simNao[$local['descolamento']];
            $response['locais'][$i]['tecido_granulacao'] = $simNao[$local['tecido_granulacao']];
            $response['locais'][$i]['tecido_esfacelo'] = $simNao[$local['tecido_esfacelo']];
            $response['locais'][$i]['tecido_fibrina'] = $simNao[$local['tecido_fibrina']];
            $response['locais'][$i]['tecido_necrotico'] = $simNao[$local['tecido_necrotico']];
            $response['locais'][$i]['exposicao'] = $simNao[$local['exposicao']];
            $response['locais'][$i]['tipo'] = $local['tipo'];
            $response['locais'][$i]['volume'] = $local['volume'];
            $response['locais'][$i]['odor'] = $simNao[$local['odor']];
            $response['locais'][$i]['cor'] = $local['cor'];
            $response['locais'][$i]['edema'] = $simNao[$local['edema']];
            $response['locais'][$i]['maceracao'] = $simNao[$local['maceracao']];
            $response['locais'][$i]['bordas'] = $bordas[$local['bordas']];
            $response['locais'][$i]['instrumental'] = $simNao[$local['instrumental']];
            $response['locais'][$i]['cirurgico'] = $simNao[$local['cirurgico']];

            $i++;
        }

        $countLocais = count($response['locais']);

        if($countLocais < 5){
            for($countLocais; $countLocais < 5; $countLocais++) {
                $response['locais'][$i]['local'] = '';
                $response['locais'][$i]['lado'] = '';
                $response['locais'][$i]['etiologia'] = '';
                $response['locais'][$i]['comprimento'] = '';
                $response['locais'][$i]['largura'] = '';
                $response['locais'][$i]['profundidade'] = '';
                $response['locais'][$i]['descolamento'] = '';
                $response['locais'][$i]['tecido_granulacao'] = '';
                $response['locais'][$i]['tecido_esfacelo'] = '';
                $response['locais'][$i]['tecido_fibrina'] = '';
                $response['locais'][$i]['tecido_necrotico'] = '';
                $response['locais'][$i]['exposicao'] = '';
                $response['locais'][$i]['tipo'] = '';
                $response['locais'][$i]['volume'] = '';
                $response['locais'][$i]['odor'] = '';
                $response['locais'][$i]['cor'] = '';
                $response['locais'][$i]['edema'] = '';
                $response['locais'][$i]['maceracao'] = '';
                $response['locais'][$i]['bordas'] = '';
                $response['locais'][$i]['instrumental'] = '';
                $response['locais'][$i]['cirurgico'] = '';
            }
        }

        return $response;
    }

    private static function coberturas($id)
    {
        $response = [];
        $coberturas = PlanoFeridas::getPlanoFeridasCoberturas($id);

        foreach ($coberturas as $cobertura) {
            if($cobertura['tipo_cobertura'] == 'cp') {
                $response['cp'][$cobertura['feridas_id']]['produto'] = $cobertura['item'];
                $response['cp'][$cobertura['feridas_id']]['catalogo_id'] = $cobertura['catalogo_id'];
                $response['cp'][$cobertura['feridas_id']]['tamanho'] = $cobertura['tamanho'];
                $response['cp'][$cobertura['feridas_id']]['quantidade'] = $cobertura['quantidade'];
                $response['cp'][$cobertura['feridas_id']]['frequencia'] = $cobertura['frequencia_item_id'];
            } else {
                $response['cs'][$cobertura['feridas_id']]['produto'] = $cobertura['item'];
                $response['cs'][$cobertura['feridas_id']]['catalogo_id'] = $cobertura['catalogo_id'];
                $response['cs'][$cobertura['feridas_id']]['tamanho'] = $cobertura['tamanho'];
                $response['cs'][$cobertura['feridas_id']]['quantidade'] = $cobertura['quantidade'];
                $response['cs'][$cobertura['feridas_id']]['frequencia'] = $cobertura['frequencia_item_id'];
            }
        }

        return $response;
    }

    private static function coberturasImpressao($id)
    {
        $response = [];
        $coberturas = PlanoFeridas::getPlanoFeridasCoberturas($id);

        foreach ($coberturas as $cobertura) {
            if($cobertura['tipo_cobertura'] == 'cp') {
                $response['cp'][$cobertura['feridas_id']]['produto'] = $cobertura['item'];
                $response['cp'][$cobertura['feridas_id']]['tamanho'] = $cobertura['tamanho'];
                $response['cp'][$cobertura['feridas_id']]['quantidade'] = $cobertura['quantidade'];
                $response['cp'][$cobertura['feridas_id']]['frequencia'] = $cobertura['frequencia_item'];
            } else {
                $response['cs'][$cobertura['feridas_id']]['produto'] = $cobertura['item'];
                $response['cs'][$cobertura['feridas_id']]['tamanho'] = $cobertura['tamanho'];
                $response['cs'][$cobertura['feridas_id']]['quantidade'] = $cobertura['quantidade'];
                $response['cs'][$cobertura['feridas_id']]['frequencia'] = $cobertura['frequencia_item'];
            }
        }

        $countCp = count($response['cp']);
        $countCs = count($response['cs']);

        if($countCp < 5){
            for($i = 5 - $countCp+1; $i < 5; $i++) {
                $response['cp'][$i]['produto'] = '';
                $response['cp'][$i]['tamanho'] = '';
                $response['cp'][$i]['quantidade'] = '';
                $response['cp'][$i]['frequencia'] = '';
            }
        }

        if($countCs < 5){
            for($i = 5 - $countCs+1; $i < 5; $i++) {
                $response['cs'][$i]['produto'] = '';
                $response['cs'][$i]['tamanho'] = '';
                $response['cs'][$i]['quantidade'] = '';
                $response['cs'][$i]['frequencia'] = '';
            }
        }

        return $response;
    }

    public static function enviarEmailSimples($titulo, $texto, $para)
    {
        $api_data = (object) Config::get('sendgrid');

        $gateway = new MailAdapter(
            new SendGridGateway(
                new \SendGrid($api_data->user, $api_data->pass),
                new \SendGrid\Email()
            )
        );

        $gateway->adapter->sendSimpleMail($titulo, $texto, $para);
    }
}
