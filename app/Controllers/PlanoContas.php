<?php
namespace App\Controllers;

use App\Core\DateTime;
use App\Core\Log,
    \App\Models\Financeiro as Financeiro;

class PlanoContas {

    public static function atualizarItem($post, $plano = "Financeiro")
    {
        $response = [];
        if(isset($post) && !empty($post)){
            $data = $post;
            $response['result'] = "Financeiro" === $plano ? Financeiro\IPCF::update($data) : Financeiro\IPCC::update($data);
            if(is_numeric($response['result'])){
                $response['msg']['success'] = 'Plano de Contas atualizado com sucesso!';
            }else{
                $response['msg']['failed'] = $response['result'];
                $backtrace = [
                    'text'      => $response['result'],
                    'module'    => __NAMESPACE__,
                    'file'      => __FILE__,
                    'function'  => __FUNCTION__,
                    'class'     => __CLASS__,
                    'line'      => __LINE__
                ];
                Log::registerErrorLog($backtrace);
            }
        }
        return $response;
    }

    public static function novoItem($post, $retencoes = [], $plano = "Financeiro")
    {
        $response = [];
        if(isset($post) && !empty($post)){
            $response['result'] = "Financeiro" === $plano ? Financeiro\IPCF::save($post, $retencoes) : Financeiro\IPCC::save($post);
            if($response['result'] === true){
                $response['msg']['success'] = 'Item do Plano de Contas criado com sucesso!';
            }else{
                $response['msg']['failed'] = $response['result'];
                $backtrace = [
                    'text'      => $response['result'],
                    'module'    => __NAMESPACE__,
                    'file'      => __FILE__,
                    'function'  => __FUNCTION__,
                    'class'     => __CLASS__,
                    'line'      => __LINE__
                ];
                Log::registerErrorLog($backtrace);
            }
        }
        return $response;
    }

    public static function listarArvore($plano = "Financeiro")
    {
        $response = [];
        if("Financeiro" === $plano) {
            $planoConta = Financeiro\IPCF::getAll();

            foreach($planoConta as $conta){
                $nivel1 = $conta['N1'] === 'E' ? "1 - ENTRADA" : "2 - SAÍDA";
                $nivel2 = strtoupper(sprintf('%s - %s', $conta['COD_N2'], $conta['N2']));
                $nivel3 = strtoupper(sprintf('%s - %s', $conta['COD_N3'], $conta['N3']));
                $nivel4 = strtoupper(sprintf('%s - %s', $conta['COD_N4'], $conta['N4']));

                $response['contas'][$nivel1][$nivel2][$nivel3][] = ['codigo' => $conta['COD_N4'], 'desc' => $nivel4];
            }
        }else{
            $planoConta = Financeiro\IPCC::getAll();

            foreach($planoConta as $conta){
                switch ($conta['N1']) {
                    case 'A' :
                        $nivel1 = '1 - ATIVO';
                        break;
                    case 'P' :
                        $nivel1 = '2 - PASSIVO';
                        break;
                    case 'CRR' :
                        $nivel1 = '3 - CONTAS DE RESULTADO - RECEITAS';
                        break;
                    case 'C' :
                        $nivel1 = '4 - CUSTOS';
                        break;
                    case 'DO' :
                        $nivel1 = '5 - DESPESAS OPERACIONAIS';
                        break;
                    case 'RF' :
                        $nivel1 = '6 - RESULTADO FINANCEIRO';
                        break;
                    case 'OR' :
                        $nivel1 = '7 - OUTRAS RECEITAS';
                        break;
                    case 'PRO' :
                        $nivel1 = '8 - PROVISÃO DE IRPJ E CSLL';
                        break;
                }
                $nivel2 = strtoupper(sprintf('%s - %s', $conta['COD_N2'], $conta['N2']));
                $nivel3 = strtoupper(sprintf('%s - %s', $conta['COD_N3'], $conta['N3']));
                $nivel4 = strtoupper(sprintf('%s - %s', $conta['COD_N4'], $conta['N4']));
                $nivel5 = strtoupper(sprintf('%s - %s (COD. DOMÍNIO.: %s)', $conta['COD_N5'], $conta['N5'], $conta['CODIGO_DOMINIO']));

                $response['contas'][$nivel1][$nivel2][$nivel3][$nivel4][] = $nivel5;
            }
        }
        return $response['contas'];
    }

    public static function preencherCampos($get, $plano = "Financeiro")
    {
        $response =
            "Financeiro" === $plano ?
                Financeiro\IPCF::getByCodigo($get['match'], $get['level']) :
                Financeiro\IPCC::getByCodigo($get['match'], $get['level']);
        return $response;
    }

    public static function excluirItem($data, $plano = "Financeiro"){
        $response = [];
        $response['result'] = "Financeiro" === $plano ? Financeiro\IPCF::delete($data) : Financeiro\IPCC::delete($data);
        if($response['result'] === true){
            $response['msg']['success'] = 'Item excluído com sucesso!';
        }else{
            if("Financeiro" === $plano){
                $response['msg']['failed'] =
                    sprintf("%s %s",
                        $response['result']['msg']['movs'],
                        "/" . $response['result']['msg']['childs']);
            }else{
                $response['msg']['failed'] =
                    sprintf("%s %s %s %s",
                        $response['result']['msg']['ipcf'],
                        ($response['result']['msg']['fornecedores'] != '' ? "/" . $response['result']['msg']['fornecedores'] : ""),
                        ($response['result']['msg']['bancos'] != '' 			? "/" . $response['result']['msg']['bancos'] : ""),
                        ($response['result']['msg']['childs'] != '' 			? "/" . $response['result']['msg']['childs'] : ""));
            }
        }
        return $response;
    }

    public static function autoComplete($data, $plano = "Financeiro"){
        $response = "Financeiro" === $plano ? Financeiro\IPCF::getJSON($data) : Financeiro\IPCC::getJSON($data);
        return $response;
    }

    #--------------------------------------EXCLUSIVO IPCC--------------------------------------
    public static function importarIPCC(){
        $arquivo = $_SERVER['DOCUMENT_ROOT']."/utils/plano-contas-contabil.csv";
        $response = Financeiro\IPCC::createDataFromCSV($arquivo);
        return ($response ? 'Importado com sucesso' : '');
    }

    #---------------------------------EXCLUSIVO ARQUIVO DOMINIO--------------------------------
    public static function formExportarArquivo()
    {
        require($_SERVER['DOCUMENT_ROOT'].'/financeiros/dominio-sistemas/templates/dominio-form.phtml');
    }

    public static function exportarArquivo()
    {
        $dominio 	= new Financeiro\Dominio();
        $response = $dominio->createTextFromData($_POST);

        $inicio 	= filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
        $fim 			= filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);

        $filename = sprintf(
            "%s-DominioSistemas-AssisteVida_%s_%s",
            ucfirst($_POST['tipo']),
            $inicio,
            $fim
        );

        header("Content-type: application/text; charset=utf-8");
        header("Content-Disposition: attachment; filename={$filename}.txt");
        header("Pragma: no-cache");

        echo $response;

    }
}