<?php
namespace app\Controllers;

use App\Helpers\StringHelper;
use \App\Models\Administracao\Filial;
use \App\Models\Administracao\Operadora;
use \App\Models\Administracao\Paciente;
use \App\Models\Administracao\Planos;
use \App\Models\Administracao\Usuario;
use \App\Models\Auditoria\Fatura;
use App\Models\Financeiro\Fornecedor;
use App\Models\Financeiro\FaturamentoControleRecebimento as ModelControleRecebimento;
include_once('../../utils/codigos.php');

class FaturamentoControleRecebimento
{
    public static function formAdicionarPrevisaoRecebimentoFaturamentoEnviado()
    {
        $auditores  = Usuario::getUsersByRole('modaud =1 OR modfat');
        $pacientes  = Paciente::getAll();
        $urs        = Filial::getAll();
        $planos     = Planos::getAll();

        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/faturamento-controle-recebimento/templates/adicionar-previsao-recebimento-form.phtml');
    }

    public static function pesquisarFaturamentoEnviado()
    {
        //ini_set('display_errors',1);
        if(isset($_POST) && !empty($_POST['inicio']) && !empty($_POST['fim'])){
            $inicio  = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim     = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            
//            $auditorBusca   = filter_input(INPUT_POST, 'auditor', FILTER_SANITIZE_NUMBER_INT);
            $pacienteBusca  = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $urBusca        = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_NUMBER_INT);
            $planoBusca        = filter_input(INPUT_POST, 'plano', FILTER_SANITIZE_NUMBER_INT);

            $ordenarPor     = filter_input(INPUT_POST, 'ordenar_por', FILTER_SANITIZE_STRING);

            $filtros = [
                'inicio'        => $inicio,
                'fim'           => $fim,
                'auditor'       => '',
                'paciente'      => $pacienteBusca,
                'ur'            => $urBusca,
                'plano'         => $planoBusca,
                'ordenarPor'    => $ordenarPor
            ];

            $response 	= Fatura::getRelatorioFaturamentoEnviado($filtros);
            $count      = count($response);
            $regraPagamentoConvenio   =  [];

            if($count > 0) {
                foreach ($response as $key => $data) {
                    $responseRecebimento = ModelControleRecebimento::getFaturaPrevisaoRecebimentoByFaturaId($data['ID']);
                    $responseOperadora = Operadora::getOperadoraByPlanoId($data['PLANO_ID']);
                    $regraPagamentoConvenio[$responseOperadora['operadora_id']]['operadora'] = $responseOperadora['operadora'];
                    $regraPagamentoConvenio[$responseOperadora['operadora_id']]['regra_pagamento'] = $responseOperadora['dia_pagamento'];

                    if(!empty($responseRecebimento)){
                        $response[$key]['idPrevisao'] = $responseRecebimento[0]['id'];
                        $response[$key]['usuarioPrevisao'] = $responseRecebimento[0]['updated_by'];
                        $response[$key]['dataPrevisao'] = $responseRecebimento[0]['previsao_recebimento'];
                        $response[$key]['dataSistemaPrevisao'] = $responseRecebimento[0]['updated_at'];
                        $response[$key]['statusRecebimento'] = $responseRecebimento[0]['status'];
                        $response[$key]['valor_faturado_anterior'] = $responseRecebimento[0]['valor_faturado'];
                        $response[$key]['modalidade_fatura'] = $responseRecebimento[0]['modalidade_fatura'];
                        $response[$key]['desconto_percentual_anterior'] = $responseRecebimento[0]['percentual_desconto_imposto'];


                    }else{
                        $response[$key]['idPrevisao'] = '';
                        $response[$key]['usuarioPrevisao'] = '';
                        $response[$key]['dataPrevisao'] = '';
                        $response[$key]['dataSistemaPrevisao'] = '';
                        $response[$key]['statusRecebimento'] = '';
                        $response[$key]['modalidade_fatura'] = '';
                        $response[$key]['desconto_percentual_anterior'] = '';
                    }

                    if($response[$key]['statusRecebimento'] == 'RECEBIDO'){
                        unset($response[$key]);

                    }else{
                        $faturas[] = $data['ID'];
                    }
                }

                //$responseRegraPrevisaoRecebimento = Operador$convenioFatura[$data['PLANO_ID']] = $data['PLANO_ID'];

                $count      = count($response);
                if($count > 0){
                    $responseModalidade = Fatura::getModalidadeByFaturaId($faturas);

                    foreach ($responseModalidade as $data) {
                        $modalidades[$data['FATURA_ID']] = $data['cod_item'];
                    }

                    foreach ($response as $key => $r) {
                        $modalidade = 'ID';
                        if (empty($modalidades[$r['ID']])) {
                            $modalidade = 'AD';
                        }
                        $response[$key]['modalidade'] = $modalidade;
                    }
                }
            }
        }

        $auditores  = Usuario::getUsersByRole('modaud =1 OR modfat');
        $pacientes  = Paciente::getAll();
        $urs        = Filial::getAll();
        $planos     = Planos::getAll();

        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/faturamento-controle-recebimento/templates/adicionar-previsao-recebimento-form.phtml');
    }

    public static function salvarPrevisaoRecebimento()
    {
        if(isset($_POST) && !empty($_POST) ){

            $dataPrevisaoRecebimento    = $_POST['data_previsao_recebimento'];
            $idPrevisaoAnterior     = $_POST['id_previsao_anterior'];
            $usuarioAnterior     = $_POST['usuario_anterior'];
            $dataAnterior     = $_POST['data_anterior'];
            $dataSistemaAnterior    = $_POST['data_sistema_anterior'];
            $valorFaturadoAtual    = $_POST['valor_atual'];
            $valorFaturadoAnterior = $_POST['valor_faturado_anterior'];
            $modalidadeFatura= $_POST['modalidade_fatura'];
            $modalidadeFaturaAnterior = $_POST['modalidade_fatura_anterior'];
            $descontoPercentual = $_POST['desconto_percentual'];
            $descontoPercentualAnterior = $_POST['desconto_percentual_anterior'];

            ModelControleRecebimento::salvarPrevisaoRecebimento(
                $dataPrevisaoRecebimento,
                $idPrevisaoAnterior,
                $usuarioAnterior,
                $dataAnterior,
                $dataSistemaAnterior,
                $valorFaturadoAtual,
                $valorFaturadoAnterior,
                $modalidadeFatura,
                $modalidadeFaturaAnterior,
                $descontoPercentual,
                $descontoPercentualAnterior);

        }


    }

    public static function formRelatorioPrevisaoRecebimentoFaturamentoEnviado()
    {
        $auditores  = Usuario::getUsersByRole('modaud =1 OR modfat');
        $pacientes  = Paciente::getAll();
        $urs        = Filial::getAll();
        $planos     = Planos::getAll();

        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/faturamento-controle-recebimento/templates/relatorio-previsao-recebimento-form.phtml');

    }

    public static function pesquisarRelatorioPrevisaoRecebimento()
    {
        if(isset($_POST) && !empty($_POST['inicio']) && !empty($_POST['fim'])){
            $inicio  = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim     = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
//            $auditorBusca   = filter_input(INPUT_POST, 'auditor', FILTER_SANITIZE_NUMBER_INT);
            $pacienteBusca  = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $urBusca        = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_NUMBER_INT);
            $planoBusca        = filter_input(INPUT_POST, 'plano', FILTER_SANITIZE_NUMBER_INT);
            $ordenarPor     = filter_input(INPUT_POST, 'ordenar_por', FILTER_SANITIZE_STRING);
            $status     = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_STRING);
            $tipoPeriodo        = 'PREVISAO_RECEBIMENTO';

            $filtros = [
                'inicio'        => $inicio,
                'fim'           => $fim,
                'auditor'       => '',
                'paciente'      => $pacienteBusca,
                'ur'            => $urBusca,
                'plano'         => $planoBusca,
                'ordenarPor'    => $ordenarPor,
                'status'        => $status,
                'tipoPeriodo'  => $tipoPeriodo
            ];

            $response 	= ModelControleRecebimento::getRelatorioControleRecebimento($filtros);

            $count      = count($response);

        }

        $auditores  = Usuario::getUsersByRole('modaud =1 OR modfat');
        $pacientes  = Paciente::getAll();
        $urs        = Filial::getAll();
        $planos     = Planos::getAll();

        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/faturamento-controle-recebimento/templates/relatorio-previsao-recebimento-form.phtml');
    }

    public static function formAdicionarInformeRecebimento()
    {
        $auditores  = Usuario::getUsersByRole('modaud =1 OR modfat');
        $pacientes  = Paciente::getAll();
        $urs        = Filial::getAll();
        $planos     = Planos::getAll();
        $fornecedores = Fornecedor::getAll();

        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/faturamento-controle-recebimento/templates/adicionar-informe-recebimento-form.phtml');
    }

    public static function pesquisarFaturasParaInformeRecebimento()
    {
        if(isset($_POST) && !empty($_POST['inicio']) && !empty($_POST['fim'])){
            $inicio  = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim     = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
//            $auditorBusca     = filter_input(INPUT_POST, 'auditor', FILTER_SANITIZE_NUMBER_INT);
            $pacienteBusca      = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $urBusca            = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_NUMBER_INT);
            $planoBusca         = filter_input(INPUT_POST, 'plano', FILTER_SANITIZE_NUMBER_INT);
            $ordenarPor         = filter_input(INPUT_POST, 'ordenar_por', FILTER_SANITIZE_STRING);
            $status             = 'PENDENTE';
            $tipoPeriodo        = 'FATURA';

            $filtros = [
                'inicio'        => $inicio,
                'fim'           => $fim,
                'auditor'       => '',
                'paciente'      => $pacienteBusca,
                'ur'            => $urBusca,
                'plano'         => $planoBusca,
                'ordenarPor'    => $ordenarPor,
                'status'        => $status,
                'tipoPeriodo'  => $tipoPeriodo
            ];

            $response 	= ModelControleRecebimento::getRelatorioControleRecebimento($filtros);
            $count      = count($response);
        }

      echo json_encode($response);
    }

    public static function salvarInformeRecebimento()
    {
        if(isset($_POST)){
            $nota['tipo_documento'] = filter_input(INPUT_POST, 'tipo_documento', FILTER_SANITIZE_STRING);
            $nota['numero_nota'] = filter_input(INPUT_POST, 'numero_nota', FILTER_SANITIZE_STRING);
            $nota['data_emissao'] = filter_input(INPUT_POST, 'data_emissao', FILTER_SANITIZE_STRING);
            $nota['fornecedor'] = filter_input(INPUT_POST, 'fornecedor', FILTER_SANITIZE_NUMBER_INT);
            $nota['valor_servico'] = filter_input(INPUT_POST, 'valor_servico', FILTER_SANITIZE_STRING);
            $nota['valor_servico'] = StringHelper::moneyStringToFloat($nota['valor_servico']);
            $nota['valor_recebido'] = $_POST['valor_recebido'];
            $nota['modalidade'] = $_POST['modalidade'];
            $nota['descricao'] = filter_input(INPUT_POST, 'descricao', FILTER_SANITIZE_STRING);
            $nota['porcentagem_assistencia'] = filter_input(INPUT_POST, 'porcentagem_assistencia', FILTER_SANITIZE_STRING);
            $nota['porcentagem_assistencia'] = StringHelper::moneyStringToFloat($nota['porcentagem_assistencia']);
            $nota['porcentagem_remocao'] = filter_input(INPUT_POST, 'porcentagem_remocao', FILTER_SANITIZE_STRING);
            $nota['porcentagem_remocao'] = StringHelper::moneyStringToFloat($nota['porcentagem_remocao']);
            $valorAssistencia = 0;
            $valorRemocao = 0;
            foreach ($nota['valor_recebido'] as $key => $valor){
                $nota['valor_recebido'][$key] = StringHelper::moneyStringToFloat($valor);
                $nota['modalidade'][$key] == 'REMOCAO' ? $valorRemocao += $nota['valor_recebido'][$key] : $valorAssistencia += $nota['valor_recebido'][$key];
            }

            $nota['valor_assistencia'] = $valorAssistencia;
            $nota['valor_remocao'] = $valorRemocao;

            $response = ModelControleRecebimento::salvarInformeRecebimento($nota);

            echo json_encode($response);
        }
    }

    public static function formAssociarFaturaNFRecebimento()
    {
        $urs      = Filial::getAll();
        $clientes = Fornecedor::getAll();
        $pacientes  = Paciente::getAll();
        $planos     = Planos::getAll();

        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/faturamento-controle-recebimento/templates/associar-fatura-nf-recebimento.phtml');
    }

    public static function pesquisarNFRecebimentoSemFaturaAssociada()
    {
        if(isset($_POST) && !empty($_POST['inicio']) && !empty($_POST['fim'])){
            $inicio  = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim     = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $clienteBusca  = filter_input(INPUT_POST, 'cliente', FILTER_SANITIZE_NUMBER_INT);
            $urBusca        = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_NUMBER_INT);

            $filtros = [
                'inicio'        => $inicio,
                'fim'           => $fim,
                'cliente'       => $clienteBusca,
                'ur'            => $urBusca,
            ];

            $response 	= ModelControleRecebimento::getNFSemFatura($filtros);
            $count      = count($response);
        }

        $urs      = Filial::getAll();
        $clientes = Fornecedor::getAll();
        $pacientes  = Paciente::getAll();
        $planos     = Planos::getAll();

        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/faturamento-controle-recebimento/templates/associar-fatura-nf-recebimento.phtml');
    }

    public static function finalizarAssociarFaturaNotaRecebimento()
    {
        if(isset($_POST)){
            $data = $_POST['data'];

            $response = ModelControleRecebimento::finalizarAssociarFaturaNotaRecebimento($data);

            echo $response;
        }
    }

    public static function informarValorRecebidoFatura()
    {
        if(isset($_POST)){
            $data = $_POST['data'];

            $response = ModelControleRecebimento::informarValorRecebidoFatura($data);

            echo $response;
        }
    }

}