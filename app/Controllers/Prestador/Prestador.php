<?php
namespace App\Controllers\Prestador;

use App\Models\Administracao\Paciente;
use App\Models\DB\ConnMysqli;
use App\Models\Prestador\Prestador AS PrestadorModel;

class Prestador
{
    public static function getLabelsPorTipoDeUsuario()
    {
        return [
            'medico' => [
                'profissional' => 'Médico(a)',
                'conselho' => 'CRM',
            ],
            'enfermagem' => [
                'profissional' => 'Enfermeiro(a)',
                'conselho' => 'COREN',
            ],
            'fisioterapia' => [
                'profissional' => 'Fisioterapeuta',
                'conselho' => 'CREFITO',
            ],
            'fonoaudiologia' => [
                'profissional' => 'Fonoaudiólogo(a)',
                'conselho' => 'CRFa',
            ],
            'psicologia' => [
                'profissional' => 'Psicólogo(a)',
                'conselho' => 'CRP',
            ],
            'nutricao' => [
                'profissional' => 'Nutricionista',
                'conselho' => 'CRN',
            ],
        ];
    }

    public static function menu()
    {
        require ($_SERVER['DOCUMENT_ROOT'] . '/prestador/templates/menu.phtml');
    }

    public static function formListar()
    {
        $data = [];
        $pacientes = Paciente::getPacientesByUr();
        $data['inicio'] = (new \DateTime())->modify('first day of this month')->format('Y-m-d');
        $data['fim'] = (new \DateTime())->modify('last day of this month')->format('Y-m-d');

        require ($_SERVER['DOCUMENT_ROOT'] . '/prestador/templates/listar-form.phtml');
    }

    public static function buscarRelatorioPrestador()
    {
        if (isset($_POST) && !empty($_POST)) {
            $data = [];
            $data['paciente'] = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $data['tipo_relatorio'] = filter_input(INPUT_POST, 'tipo_relatorio', FILTER_SANITIZE_STRING);
            $data['inicio'] = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);

            $relatorios = PrestadorModel::getByFilters($data);
            $pacientes = Paciente::getPacientesByUr();
        }

        require ($_SERVER['DOCUMENT_ROOT'] . '/prestador/templates/listar-form.phtml');
    }

    public static function form()
    {
        $response = [];
        $pacientes = Paciente::getPacientesByUr();
        $titulo_formulario = $_GET['tipo'] == 'p' ? 'Prorrogar Paciente' : 'Deflagrado de Paciente';
        $tipoPrestador = $_SESSION['tipo_user'];
        $labelsPrestador = isset(self::getLabelsPorTipoDeUsuario()[$tipoPrestador]) ? self::getLabelsPorTipoDeUsuario()[$tipoPrestador] : [];
        if(count($labelsPrestador) == 0) {
            $labelsPrestador = [
                'profissional' => 'Profissional',
                'conselho' => 'Conselho'
            ];
        }
        $response['inicio'] = (new \DateTime())->modify('first day of this month')->format('Y-m-d');
        $response['fim'] = (new \DateTime())->modify('last day of this month')->format('Y-m-d');

        require ($_SERVER['DOCUMENT_ROOT'] . '/prestador/templates/form.phtml');
    }

    public static function salvarRelatorioPrestador()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = [];
            $tipoPrestador = $_SESSION['tipo_user'];

            $data['paciente'] = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $data['tipo_relatorio'] = filter_input(INPUT_POST, 'tipo', FILTER_SANITIZE_STRING);
            $data['tipo_prestador'] = $tipoPrestador;
            $data['empresa'] = filter_input(INPUT_POST, 'empresa_id', FILTER_SANITIZE_NUMBER_INT);
            $data['convenio'] = filter_input(INPUT_POST, 'convenio_id', FILTER_SANITIZE_NUMBER_INT);
            $data['inicio'] = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $data['evolucao'] = htmlentities(addslashes(strip_tags($_POST['evolucao'], '<p><br><b><u><i><ol><ul><li><table><tbody><tr><th><td>')));
            $data['data_visita'] = filter_input(INPUT_POST, 'data_visita', FILTER_SANITIZE_STRING);
            $data['profissional'] = filter_input(INPUT_POST, 'profissional', FILTER_SANITIZE_STRING);
            $data['conselho'] = filter_input(INPUT_POST, 'conselho', FILTER_SANITIZE_STRING);

            $prestador = new PrestadorModel(ConnMysqli::getConnection());
            $response = $prestador->save($data);

            if($response) {
                $msg = "Relatório criado com sucesso!";
                $type = 's';
                header ("Location: /prestador/?action=prestador-form&tipo=" . $data['tipo_relatorio'] . "&msg=" . base64_encode ($msg) . "&type=" . $type);
            } else {
                $msg = "Houve um problema ao atualizar os dados desse relatório!";
                $type = 'f';
                header ("Location: /prestador/?action=prestador-form&tipo=" . $data['tipo_relatorio'] . "&msg=" . base64_encode ($msg) . "&type=" . $type);
            }
        }
    }

    public static function formEditar()
    {
        if (isset($_GET) && !empty($_GET)) {
            $relatorio = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

            $response = PrestadorModel::getByIdForUpdate($relatorio);
            $response['genero'] = $response['sexo'] == '0' ? 'Masculino' : 'Feminino';
            $titulo_formulario = $response['tipo_relatorio'] == 'p' ? 'Prorrogação do Paciente' : 'Deflagrado do Paciente';
            $labelsPrestador = self::getLabelsPorTipoDeUsuario()[$response['tipo_prestador']];
            if (count($labelsPrestador) == 0) {
                $labelsPrestador = [
                    'profissional' => 'Profissional',
                    'conselho' => 'Conselho'
                ];
            }
        }

        require ($_SERVER['DOCUMENT_ROOT'] . '/prestador/templates/form-editar.phtml');
    }

    public static function atualizarRelatorioPrestador()
    {
        if (isset($_POST) && !empty($_POST)) {
            $data = [];

            $data['relatorio_id'] = filter_input(INPUT_POST, 'relatorio_id', FILTER_SANITIZE_NUMBER_INT);
            $data['tipo_relatorio'] = filter_input(INPUT_POST, 'tipo', FILTER_SANITIZE_STRING);
            $data['empresa'] = filter_input(INPUT_POST, 'empresa_id', FILTER_SANITIZE_NUMBER_INT);
            $data['convenio'] = filter_input(INPUT_POST, 'convenio_id', FILTER_SANITIZE_NUMBER_INT);
            $data['inicio'] = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $data['evolucao'] = htmlentities(addslashes(strip_tags($_POST['evolucao'], '<p><br><b><u><i><ol><ul><li><table><tbody><tr><th><td>')));
            $data['data_visita'] = filter_input(INPUT_POST, 'data_visita', FILTER_SANITIZE_STRING);
            $data['profissional'] = filter_input(INPUT_POST, 'profissional', FILTER_SANITIZE_STRING);
            $data['conselho'] = filter_input(INPUT_POST, 'conselho', FILTER_SANITIZE_STRING);

            $prestador = new PrestadorModel(ConnMysqli::getConnection());
            $response = $prestador->update($data);

            if($response) {
                $msg = "Relatório atualizado com sucesso!";
                $type = 's';
                header ("Location: /prestador/?action=prestador-form-editar&id=" . $data['relatorio_id'] . "&tipo=" . $data['tipo_relatorio'] . "&msg=" . base64_encode ($msg) . "&type=" . $type);
            } else {
                $msg = "Houve um problema ao atualizar os dados desse relatório!";
                $type = 'f';
                header ("Location: /prestador/?action=prestador-form-editar&id=" . $data['relatorio_id'] . "&tipo=" . $data['tipo_relatorio'] . "&msg=" . base64_encode ($msg) . "&type=" . $type);
            }
        }
    }

    public static function visualizar()
    {
        if (isset($_GET) && !empty($_GET)) {
            $relatorio = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

            $response = PrestadorModel::getByIdForUpdate($relatorio);
            $response['genero'] = $response['sexo'] == '0' ? 'Masculino' : 'Feminino';
            $response['inicio'] = \DateTime::createFromFormat('Y-m-d', $response['inicio'])->format('d/m/Y');
            $response['fim'] = \DateTime::createFromFormat('Y-m-d', $response['fim'])->format('d/m/Y');
            $titulo_formulario = $response['tipo_relatorio'] == 'p' ? 'Prorrogação do Paciente' : 'Deflagrado do Paciente';
            $labelsPrestador = self::getLabelsPorTipoDeUsuario()[$response['tipo_prestador']];
            if (count($labelsPrestador) == 0) {
                $labelsPrestador = [
                    'profissional' => 'Profissional',
                    'conselho' => 'Conselho'
                ];
            }
        }

        require ($_SERVER['DOCUMENT_ROOT'] . '/prestador/templates/visualizar.phtml');
    }

    public static function verificarRelatorioExistente()
    {
        if (isset($_GET) && !empty($_GET)) {
            $data = [];
            $data['paciente'] = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $data['tipo_relatorio'] = filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING);
            $data['inicio'] = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);

            $relatorios = PrestadorModel::getByFilters($data);

            echo count($relatorios);
        }
    }
}
