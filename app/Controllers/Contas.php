<?php

namespace app\Controllers;

use \App\Models\Financeiro\Financeiro as Financeiro,
	\App\Models\Financeiro\Fornecedor as Fornecedor;
use App\Models\Financeiro\Notas;

class Contas
{
	#--------------------------------FINANCEIRO--------------------------------
	public static function buscarParcelasPorUR()
	{
		$filtro = [];
		$buscandoPor = [];

		//TR
		if(isset($_POST['tr']) && !empty($_POST['tr'])){
			$tr	= filter_input(INPUT_POST, 'tr', FILTER_SANITIZE_NUMBER_INT);
			$filtro[] = " AND p.idNota = {$tr} ";
			$buscandoPor[] = " TR nº {$tr} ";
		}else {
            $tipo = filter_input (INPUT_POST, 'tipo', FILTER_SANITIZE_NUMBER_INT);
            $filtro[] = " AND n.tipo = '{$tipo}' ";
            $buscandoPor[] = $tipo == 0 ? "Contas a Receber" : "Contas a Pagar";

		    //NUMERO NOTA FISCAL/RECIBO
			if (isset($_POST['num_nota']) && !empty($_POST['num_nota'])) {
				$num_nota = filter_input (INPUT_POST, 'num_nota', FILTER_SANITIZE_STRING);
				$filtro[] = " AND n.codigo = '{$num_nota}' ";
				$buscandoPor[] = " Nota nº {$num_nota} ";
			}

			//FORNECEDOR
			if (isset($_POST['fornecedor']) && !empty($_POST['fornecedor'])) {
				$fornecedor = filter_input (INPUT_POST, 'fornecedor', FILTER_SANITIZE_NUMBER_INT);
				$filtro[] = " AND n.codFornecedor = {$fornecedor} ";
				$fetch = Fornecedor::getById($fornecedor);
				$buscandoPor[] = " Fornecedor {$fetch['razaoSocial']} ";
			}

			//DESCRIÇÃO DA NOTA
			if (isset($_POST['descricao_nota']) && !empty($_POST['descricao_nota'])) {
				$descricao_nota = filter_input (INPUT_POST, 'descricao_nota', FILTER_SANITIZE_STRING);
				$filtro[] = " AND n.descricao LIKE '%{$descricao_nota}%' ";
				$buscandoPor[] = " Descrição {$descricao_nota} ";
			}

			//STATUS DA NOTA
			if (isset($_POST['status_nota']) && !empty($_POST['status_nota'])) {
				$status_nota = filter_input (INPUT_POST, 'status_nota', FILTER_SANITIZE_STRING);
				$filtro[] = " AND p.status = '{$status_nota}' ";
				$buscandoPor[] = " Status {$status_nota} ";
			}

			//VALOR TOTAL DA NOTA
			if (isset($_POST['valor']) && !empty($_POST['valor'])) {
				$valor = filter_input (INPUT_POST, 'valor', FILTER_SANITIZE_STRING);
				$filtro[] = " AND n.valor = '".str_replace(',','.', $valor)."' ";
				$buscandoPor[] = " Valor {$valor} ";
			}

			//DATA DE VENCIMENTO
			if ((isset($_POST['vencimento_inicio']) && !empty($_POST['vencimento_inicio'])) &&
				(isset($_POST['vencimento_fim']) && !empty($_POST['vencimento_fim']))) {
				$vencimento_inicio = filter_input (INPUT_POST, 'vencimento_inicio', FILTER_SANITIZE_NUMBER_INT);
				$vencimento_fim = filter_input (INPUT_POST, 'vencimento_fim', FILTER_SANITIZE_NUMBER_INT);
				$filtro[] = " AND p.vencimento BETWEEN '{$vencimento_inicio}' AND '{$vencimento_fim}' ";
				$buscandoPor[] = " Vencimento entre " .
					\DateTime::createFromFormat('Y-m-d', $vencimento_inicio)->format('d/m/Y') .
					" e " .
					\DateTime::createFromFormat('Y-m-d', $vencimento_fim)->format('d/m/Y') .
					" ";
			}

			//DATA EM QUE A NOTA FOI PAGA
			if ((isset($_POST['pagamento_inicio']) && !empty($_POST['pagamento_inicio'])) &&
				(isset($_POST['pagamento_fim']) && !empty($_POST['pagamento_fim']))) {
				$pagamento_inicio = filter_input (INPUT_POST, 'pagamento_inicio', FILTER_SANITIZE_NUMBER_INT);
				$pagamento_fim = filter_input (INPUT_POST, 'pagamento_fim', FILTER_SANITIZE_NUMBER_INT);
				$filtro[] = " AND p.DATA_PAGAMENTO BETWEEN '{$pagamento_inicio}' AND '{$pagamento_fim}' ";
				$buscandoPor[] = " Pagamento entre " .
					\DateTime::createFromFormat('Y-m-d', $pagamento_inicio)->format('d/m/Y') .
					" e " .
					\DateTime::createFromFormat('Y-m-d', $pagamento_fim)->format('d/m/Y') .
					" ";
			}

			//DATA DA ENTRADA
			if ((isset($_POST['entrada_inicio']) && !empty($_POST['entrada_inicio'])) &&
				(isset($_POST['entrada_fim']) && !empty($_POST['entrada_fim']))) {
				$entrada_inicio = filter_input (INPUT_POST, 'entrada_inicio', FILTER_SANITIZE_NUMBER_INT);
				$entrada_fim = filter_input (INPUT_POST, 'entrada_fim', FILTER_SANITIZE_NUMBER_INT);
				$filtro[] = " AND n.dataEntrada BETWEEN '{$entrada_inicio}' AND '{$entrada_fim}' ";
				$buscandoPor[] = " Entrada entre " .
					\DateTime::createFromFormat('Y-m-d', $entrada_inicio)->format('d/m/Y') .
					" e " .
					\DateTime::createFromFormat('Y-m-d', $entrada_fim)->format('d/m/Y') .
					" ";
			}
		}

		$response = Financeiro::getParcelasByFilter($_SESSION['empresa_principal'], $filtro);

		$fornecedores = Fornecedor::getAll();

		require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/contas/templates/buscar-contas-ur-form.phtml');
	}

	public static function formBuscarParcelasPorUR()
	{
		$fornecedores = Fornecedor::getAll();

		require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/contas/templates/buscar-contas-ur-form.phtml');
	}

	public static function editarCompetencia()
	{
		
		if(!empty($_POST["notaId"]) && !empty($_POST['competencia'])){
			$idNota = filter_input (INPUT_POST, 'notaId', FILTER_SANITIZE_NUMBER_INT);
			$competencia = filter_input (INPUT_POST, 'competencia', FILTER_SANITIZE_STRING);
			$competencia = implode("-", array_reverse(explode("/", $competencia)));

			$response = Notas::editarCompetencia($idNota, $competencia);
			
			if($response['tipo'] == 'acerto') {
                $response = [
					'tipo' => 'acerto',
                	'msg'=> 'Data de competência atualizada'
                ];
                echo json_encode($response);
                return;
            } 
		}
		$response = [
						'tipo' => 'erro',
                		'msg'=>"Erro ao Atualizar data de Competência."
                	];
                
                echo json_encode($response);
                return;

	}
}