<?php
/**
 * Created by PhpStorm.
 * User: jefinho
 * Date: 06/02/2018
 * Time: 10:27
 */

namespace App\Controllers;


use App\Models\Administracao\Paciente;
use App\Models\Faturamento\Consenso;
use App\Models\Faturamento\Fatura;
use App\Models\Faturamento\UnidadeMedida;

class ConsensoDeFaturamento
{

    public static function pesquisarFatura()
    {
        $pacienteId = '';
        $inicio = '';
        $fim = '';

       if(isset($_POST) && !empty($_POST)){
           $pacienteId = $_POST['paciente'] ;
           $inicio = $_POST['inicio'];
           $fim = $_POST['fim'];
           $faturas = Fatura::getFaturasByFilters($_POST);
       }
       $pacientes = Paciente::getAll();
        require ($_SERVER['DOCUMENT_ROOT'] . '/faturamento/consenso/templates/formPesquisarFatura.phtml');


    }

    public static function formGerarConsenso()
    {
        if(isset($_GET['idFatura']) && !empty($_GET['idFatura'])){

            $idFatura = $_GET['idFatura'];
            $cabecalhoFatura = Fatura::getCabecalhoFaturaById($idFatura);
            $condicao = " AND INCLUSO_PACOTE = 0";
            $itensFatura = Fatura::getItensByFaturaId($idFatura,$condicao);
            $unidadeMedida = UnidadeMedida::getAll();
            $motivosGlosa = Fatura::getMotivoGlosa();


            require ($_SERVER['DOCUMENT_ROOT'] . '/faturamento/consenso/templates/formGerarConsenso.phtml');

        }


    }
    public static function salvarConsensoFatura()
    {

        if(isset($_POST['fatura-id']) && !empty($_POST['fatura-id'])){
            $dados['faturaId'] =  $_POST['fatura-id'];
            $dados = $_POST;
            $responseTemConsenso = Consenso::salvar($dados);
            return $responseTemConsenso;

      }


    }

}