<?php

/**
 * Esse script move um arquivo local para um bucket no Google Cloud Storage
 *
 * ETAPAS:
 * 1. Selecionar uma pasta local para ser movida para o GCS
 * 2. Adicionar em uma lista todos os arquivos existes no diretório
 * 3. Chamar método de envio através da API própria, passando a pasta de origem e de destino
 * 4. Se tiver enviado os arquivos com sucesso, atualizar o status dos arquivos para enviado
 * 5. Se tiver ocorrido algum erro durante o envio, notificar a equipe técnica por e-mail
 */

require __DIR__ . '/../../vendor/autoload.php';

use App\Models\DB\ConnMysqli;
use App\Helpers\Queue;
use App\Helpers\Storage;
use App\Core\Config;

$privateKey = Config::get('GAE_PRIVATE_KEY');

$queue = new Queue(ConnMysqli::getConnection(), ['FILE']);

$queue->walk(function($source, $target) use ($privateKey) {
  $storage = new Storage($privateKey);
  if (! is_readable($source))
    throw new \Exception("Não foi possível ler o arquivo: $source");
  $storage->move($source, $target);
});


