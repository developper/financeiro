<?php
namespace App\Controllers;

//ini_set('display_errors', 1);

use App\Helpers\Ofx;
use App\Models\Financeiro\Banco;
use App\Models\Financeiro\ContaBancaria;
use App\Models\Financeiro\ConciliacaoBancaria as ModelConciliacao;


class ConciliacaoBancaria
{

    public static function getForm()
    {
        $tipoConta = $_SESSION['empresa_principal'] == 1 ? NULL : 4;
        $msgErroOfx = ''; 
        $contaBancarias = ContaBancaria::getContasBancariasByURs($_SESSION['empresa_user'],$tipoConta);
        require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/conciliacao-bancaria/templates/form.phtml');
    }

    public static function importarOfx()
    {
        if(isset($_FILES) && !empty($_FILES['arquivo_ofx']['size'])){
            // salvando arquivo ofx na pasta ofx do financeiro
            $response = self::moveOFXToTemporaryPath($_FILES);
            if(!empty($response['acerto'])){
                // lendo arquivo ofx
                $caminhoArquivo = $response['acerto'];
                $ofx = new Ofx($response['acerto']);
                $dadosBancarios = $ofx->getDadosBancarios();
                $responseTransacoesOfx = $ofx->getTransactions();
                $cont = 0;
                $inicioTransacao = '';
                $fimTransacao = '';
                
                foreach($responseTransacoesOfx as $transacao){
                  
                    $transacoesOfx[$cont]['data'] = date('Y-m-d',strtotime(substr($transacao->DTPOSTED, 0, 8)));
                    $transacoesOfx[$cont]['dataBr'] = date('d/m/Y',strtotime(substr($transacao->DTPOSTED, 0, 8)));
                    $transacoesOfx[$cont]['valor'] = str_replace(',','.',$transacao->TRNAMT);
                    $transacoesOfx[$cont]['descricao'] = utf8_decode($transacao->MEMO);
                    $transacoesOfxData[$cont] = date('Y-m-d',strtotime(substr($transacao->DTPOSTED, 0, 8)));
                    $transacoesOfxValor[$cont] = str_replace(',','.',$transacao->TRNAMT);
                    $transacoesOfxDescricao[$cont] = str_replace(',','.',$transacao->MEMO);

                    if(empty($inicioTransacao) || $transacoesOfx[$cont]['data'] < $inicioTransacao ){
                        $inicioTransacao = $transacoesOfx[$cont]['data'];
                    }
                    if(empty($fimTransacao) || $transacoesOfx[$cont]['data'] > $fimTransacao ){
                        $fimTransacao = $transacoesOfx[$cont]['data'];
                    }
                    $cont++;
                }
                //ordenando array por data e valor asc

                 array_multisort($transacoesOfxData, SORT_ASC, $transacoesOfxValor, SORT_ASC, $transacoesOfx);


                // Alguns OFX vem sóa a conta outros veem agencia/conta
                $conta = end( explode('/', $dadosBancarios['conta']));
                $inicio = $inicioTransacao;
                $fim = $fimTransacao;
                $inicioBr = \DateTime::createFromFormat('Y-m-d', $inicio)->format('d/m/Y');
                $fimBr = \DateTime::createFromFormat('Y-m-d', $fim)->format('d/m/Y');
                //pegando banco do sistema baseado no código passado no OFX

                $banco = Banco::getBancoByCodigo($dadosBancarios['cod_banco']);
                $dadosOFX = <<<DADOSOFX
                    Dados do <b>OFX: </b>Banco  <b>{$dadosBancarios['cod_banco']} - {$banco[0]['forma']} </b> / 
                    Conta: <b>{$dadosBancarios['conta']} </b> / Período: <b>{$inicioBr} à {$fimBr}</b>
DADOSOFX;
                if(empty($banco)){
                    unlink($caminhoArquivo);
                    $msgErroOfx = "O banco do OFX não foi encontrado no sistema.";
                }else{
                    $filter = [
                        'conta' =>str_pad(str_replace('-','',$conta),10,0,STR_PAD_LEFT),
                        'banco' => $banco[0]['id'],
                        'tipo' => 1,
                        'ur' => $_SESSION['empresa_user']

                    ];
                    // Pegando Conta Bancária do sistema baseado nos dados do ofx

                    $responseContaBancaria = ContaBancaria::getContasBancariasByFilters($filter);
                 //  var_dump($responseContaBancaria);
                    if(empty($responseContaBancaria)){
                        unlink($caminhoArquivo);
                        $msgErroOfx = "Não foi encontrado nenhuma conta cadastrada no sistema para os dados do OFX";
                    }else{
                        $contaId = $responseContaBancaria[0]['id'];
                        $status = 'T';
                        $exibirParcelaBaixadaAglutinacao = "N";

                        $reponseTransacoes = ModelConciliacao::getTransacoes($contaId,
                                                                            $status,
                                                                            $inicio,
                                                                            $fim,
                                                                            $exibirParcelaBaixadaAglutinacao);

                        $cont = 0;
                       foreach($reponseTransacoes as $key => $sistema){
                           $valor =  $sistema['tipo_nota'] == 0 ? (float)  $sistema['VALOR_PAGO'] : (float) - $sistema['VALOR_PAGO'];
                           $data = $sistema['dt'];
                           $aux1 = '';
                           $aux2 = 0;


                           foreach($transacoesOfx as $k => $ofx){
                               if($valor == (float) $ofx['valor'] && $data == $ofx['data'] && $aux2 == 0){
                                   $aux1 = $k;
                                   $aux2++;
                                   array_splice($transacoesOfx,$k,1);
                                   $reponseTransacoes[$key]['encontrou'] = 'ok';
                               }

                           }
                           $response = [
                               'ofx-nao-encontrado' => $transacoesOfx,
                               'transacoes-sistema' => $reponseTransacoes
                           ];

                          unlink($caminhoArquivo);

                        }

                    }

                }
            }else{
                $msgErroOfx = $response['erro'];
            }
        }else{
            $msgErroOfx = "Selecione um arquivo antes de tentar enviar";
        }
        
        $contaBancarias = ContaBancaria::getContasBancariasByURs($_SESSION['empresa_user']);
        $fechamento_caixa = FechamentoCaixa::getFechamentoCaixa();
        require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/conciliacao-bancaria/templates/form.phtml');

    }

    public static function moveOFXToTemporaryPath($file)
    {
        $errors = [];
        $path = $_SERVER['DOCUMENT_ROOT'] . '/financeiros/ofx/';
        $size = 1024 * 1024 *1024 * 1024 *1024 * 1024 * 2;
        $array['erro'] = '';
        $array['acerto'] = '';

        $errors[1] = 'O arquivo no upload é maior do que o permitido';
        $errors[2] = 'O arquivo ultrapassa o limite de tamanho especifiado';
        $errors[3] = 'O upload do arquivo foi feito parcialmente';
        $errors[4] = 'Não foi feito o upload do arquivo!';

        if ($file['arquivo_ofx']['error'] != 0) {
            $array['erro'] = "Não foi possível fazer o upload, erro:<br />" . $errors[$file['arquivo_ofx']['error']];
            return $array;
        }

        $ext = explode('.', $file['arquivo_ofx']['name']);

        $extensao = end($ext);

        $type = $file['arquivo_ofx']['type'];

        if ($extensao != 'ofx' && $extensao != 'OFX') {
            $array['erro'] = "Por favor, envie apenas arquivos com no formato .ofx!";
            return $array;
        }

        if ($size < $file['arquivo_ofx']['size']) {
            $array['erro'] = "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
            return $array;
        }

        $agora= date('Y_m_d_H_i_s_');

        $name = $file['arquivo_ofx']['name'];
        $fullPath = $path .$agora.$name;
        $tmp = $file['arquivo_ofx']['tmp_name'];

        if (move_uploaded_file($tmp, $fullPath)) {
            $array['acerto'] = $fullPath;
            return $array;
        }

        $array['erro'] = "Não foi possível enviar o arquivo, tente novamente! Caso o erro persista, entre em contato com o setor de TI.";
        return $array['erro'];
    }



    public static function pesquisarConciliacaoBancaria()
    {
        if(isset($_POST) && !empty($_POST)){
                $contaId = filter_input(INPUT_POST, 'origem', FILTER_SANITIZE_NUMBER_INT);
                $status = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_STRING);
                $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
                $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
                $exibirParcelaBaixadaAglutinacao = "N";


            $reponseTransacoes = ModelConciliacao::getTransacoes($contaId,
                $status,
                $inicio,
                $fim,
                $exibirParcelaBaixadaAglutinacao);
            $response = [
                'ofx-nao-encontrado' => '',
                'transacoes-sistema' => $reponseTransacoes
            ];


        }else{
            $msg = "Algo deu errado entre em contato com o TI";
        }
        $contaBancarias = ContaBancaria::getContasBancariasByURs($_SESSION['empresa_user']);
        $fechamento_caixa = FechamentoCaixa::getFechamentoCaixa();
        require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/conciliacao-bancaria/templates/form.phtml');
    }

    public static function salvarConciliacaoBancaria()
    {
        if(isset($_POST) && !empty($_POST['banco_id']) && !empty($_POST['inicio']) &&
            !empty($_POST['fim']) && !empty($_POST['linha'])){
            $bancoId = filter_input(INPUT_POST, 'banco_id', FILTER_SANITIZE_NUMBER_INT);
            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $linha = filter_input(INPUT_POST, 'linha', FILTER_SANITIZE_STRING);
            $response = ModelConciliacao::salvarConciliacao($bancoId,$inicio,$fim,$linha);
            return $response;
        }else{
            return "Algo deu errado entra em contato com o TI";
        }
    }

    public static function desfazerConciliacao()
    {
        if(isset($_POST) && !empty($_POST)){
                    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
                    $idBanco = filter_input(INPUT_POST, 'id_banco', FILTER_SANITIZE_STRING);
                    $entrada = filter_input(INPUT_POST, 'entrada', FILTER_SANITIZE_STRING);
                    $saida = filter_input(INPUT_POST, 'saida', FILTER_SANITIZE_STRING);
                    $transferencia = filter_input(INPUT_POST, 'transferencia', FILTER_SANITIZE_STRING);
                    $tabelaOrigem = filter_input(INPUT_POST, 'tabela_origem', FILTER_SANITIZE_STRING);
            $response = ModelConciliacao::desfazerConciliacao($id,$idBanco,$entrada,$saida,$transferencia,$tabelaOrigem);
            return $response;

      }else{
            $msg = "Houve um erro entre em contato com o TI";
            return $msg;
        }

    }

}