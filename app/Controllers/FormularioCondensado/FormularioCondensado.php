<?php
namespace App\Controllers\FormularioCondensado;

use App\Models\Administracao\Filial;
use App\Models\Administracao\Paciente;
use App\Models\DB\ConnMysqli;
use App\Models\Medico\Deflagrado as DeflagradoMedico;
use App\Models\Enfermagem\Deflagrado as DeflagradoEnfermagem;
use App\Models\Medico\Prorrogacao as ProrrogacaoMedica;
use App\Models\Enfermagem\Prorrogacao as ProrrogacaoEnfermagem;
use App\Models\Prestador\Prestador;
use App\Models\FormularioCondensado\FormularioCondensado as FormularioCondensadoModel;
use App\Models\Sistema\Frequencia;
use App\Models\Faturamento\Operadora;

class FormularioCondensado
{
    private static $arrayModalidade = [
        '1' => 'Assistencia Domiciliar / Serviços Pontuais',
        '2' => 'Internação Domiciliar 6h',
        '3' => 'Internação Domiciliar 12h',
        '4' => 'Internação Domiciliar 24h com respirador',
        '5' => 'Internação Domiciliar 24h sem respirador'
    ];

    private static $equipamentos = [
        '3' => 1, '4' => 1, '66' => 1, '130' => 1,
        '1' => 2, '7' => 2, '63' => 2,
        '2' => 3, '6' => 3, '65' => 3,
        '5' => 4, '67' => 4,
        '136' => 5,
        '12' => 6, '64' => 6,
        '33' => 7,
        '13' => 8, '14' => 8, '135' => 8,
        '35' => 9, '129' => 9,
        '11' => 10, '79' => 10, '132' => 10,
        '133' => 11,
        '103' => 12, '110' => 12, '134' => 12,
        '102' => 13, '128' => 13,
        '38' => 14, '69' => 14, '114' => 14, '149' => 14, '150' => 14,
        '72' => 15,
        '70' => 16,
    ];

    public static function formListar()
    {
        $data = [];
        $pacientes = Paciente::getAll();
        $data['inicio'] = (new \DateTime())->modify('first day of this month')->format('Y-m-d');
        $data['fim'] = (new \DateTime())->modify('last day of this month')->format('Y-m-d');

        require($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/formulario-condensado/listar-form.phtml');
    }

    public static function buscarFormularioCondensado()
    {
        if (isset($_POST) && !empty($_POST)) {
            $data = [];
            $data['paciente'] = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $data['tipo_formulario'] = filter_input(INPUT_POST, 'tipo_formulario', FILTER_SANITIZE_STRING);
            $data['inicio'] = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);

            $formularios = FormularioCondensadoModel::getByFilters($data);
            $pacientes = Paciente::getAll();
            $responseEmpresas = Filial::getUnidadesAtivas();
        }

        require($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/formulario-condensado/listar-form.phtml');
    }

    public static function form()
    {
        $response = [];
        $pacientes = Paciente::getAll();
        $frequencias = Frequencia::getAll();
        $titulo_formulario = $_GET['tipo'] == 'p' ? 'de Prorrogação' : 'Deflagrado';
        $response['inicio'] = (new \DateTime())->modify('first day of this month')->format('Y-m-d');
        $response['fim'] = (new \DateTime())->modify('last day of this month')->format('Y-m-d');
        $response['modalidade'] = self::$arrayModalidade;


        require($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/formulario-condensado/prorrogacao/form.phtml');
    }

    public static function buscarUltimasProrrogacoes()
    {
        if (isset($_GET) && !empty($_GET)) {
            $data = [];
            $data['paciente'] = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_STRING);
            $data['inicio'] = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $equipamentosAtivos = Paciente::getEquipamentosAtivosPacienteId($data['paciente']);


            foreach ($equipamentosAtivos as $equipamento) {
                $response['equipamentos'][] = self::$equipamentos[$equipamento['item_id']];
            }

            $response['medico'] = ProrrogacaoMedica::getUltimoRelatorioPacienteByPeriodo($data);
            $response['enfermagem'] = ProrrogacaoEnfermagem::getUltimoRelatorioPacienteByPeriodo($data);
            $response['fisioterapia'] = Prestador::getUltimoRelatorioByFilters($data, 'fisioterapia', 'p');
            $response['fisioterapia']['evolucao'] = htmlspecialchars_decode(stripslashes($response['fisioterapia']['evolucao']));
            $response['fonoaudiologia'] = Prestador::getUltimoRelatorioByFilters($data, 'fonoaudiologia', 'p');
            $response['fonoaudiologia']['evolucao'] = htmlspecialchars_decode(stripslashes($response['fonoaudiologia']['evolucao']));
            $response['psicologia'] = Prestador::getUltimoRelatorioByFilters($data, 'psicologia', 'p');
            $response['psicologia']['evolucao'] = htmlspecialchars_decode(stripslashes($response['psicologia']['evolucao']));
            $response['nutricao'] = Prestador::getUltimoRelatorioByFilters($data, 'nutricao', 'p');
            $response['nutricao']['evolucao'] = htmlspecialchars_decode(stripslashes($response['nutricao']['evolucao']));

            echo json_encode($response);
        }
    }

    public static function buscarUltimosDeflagrados()
    {
        if (isset($_GET) && !empty($_GET)) {
            $data = [];

            $data['paciente'] = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_STRING);
            $data['inicio'] = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);

            $response['medico'] = DeflagradoMedico::getUltimoRelatorioPacienteByPeriodo($data);
            $response['enfermagem'] = DeflagradoEnfermagem::getUltimoRelatorioPacienteByPeriodo($data);
            $response['fisioterapia'] = Prestador::getUltimoRelatorioByFilters($data, 'fisioterapia', 'd');
            $response['fonoaudiologia'] = Prestador::getUltimoRelatorioByFilters($data, 'fonoaudiologia', 'd');
            $response['psicologia'] = Prestador::getUltimoRelatorioByFilters($data, 'psicologia', 'd');
            $response['nutricao'] = Prestador::getUltimoRelatorioByFilters($data, 'nutricao', 'd');

            echo json_encode($response);
        }
    }

    public static function salvarFormularioCondensador()
    {
        if (isset($_POST) && !empty($_POST)) {
            $data = [];

            $data['paciente'] = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $data['empresa'] = filter_input(INPUT_POST, 'empresa_id', FILTER_SANITIZE_NUMBER_INT);
            $data['convenio'] = filter_input(INPUT_POST, 'convenio_id', FILTER_SANITIZE_NUMBER_INT);
            $data['modalidade'] = filter_input(INPUT_POST, 'modalidade_id', FILTER_SANITIZE_NUMBER_INT);
            $data['tipo_formulario'] = filter_input(INPUT_POST, 'tipo_formulario', FILTER_SANITIZE_STRING);
            $data['inicio'] = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $data['dieta'] = filter_input(INPUT_POST, 'dieta', FILTER_SANITIZE_STRING);
            $data['dieta_via'] = filter_input(INPUT_POST, 'dieta_via', FILTER_SANITIZE_STRING);
            $data['dieta_nome'] = filter_input(INPUT_POST, 'dieta_nome', FILTER_SANITIZE_STRING);
            $data['dieta_adm'] = filter_input(INPUT_POST, 'dieta_adm', FILTER_SANITIZE_STRING);
            $data['oxigenoterapia'] = filter_input(INPUT_POST, 'oxigenoterapia', FILTER_SANITIZE_STRING);
            $data['vazao'] = filter_input(INPUT_POST, 'vazao', FILTER_SANITIZE_STRING);
            $data['deambula'] = filter_input(INPUT_POST, 'deambula', FILTER_SANITIZE_STRING);
            $data['drenos'] = filter_input(INPUT_POST, 'drenos', FILTER_SANITIZE_STRING);
            $data['equipamentos'] = isset($_POST['equipamentos']) ? $_POST['equipamentos'] : [];
            $data['equipamentos_outros'] = filter_input(INPUT_POST, 'equipamentos_outros', FILTER_SANITIZE_STRING);
            $data['integridade_cutanea'] = filter_input(INPUT_POST, 'integridade_cutanea', FILTER_SANITIZE_STRING);
            $data['tipo_lesao'] = isset($_POST['tipo_lesao']) ? $_POST['tipo_lesao'] : [];
            $data['atb'] = filter_input(INPUT_POST, 'atb', FILTER_SANITIZE_STRING);
            $data['atb_nome'] = filter_input(INPUT_POST, 'atb_nome', FILTER_SANITIZE_STRING);
            $data['atb_inicio'] = filter_input(INPUT_POST, 'atb_inicio', FILTER_SANITIZE_STRING);
            $data['atb_previsao_termino'] = filter_input(INPUT_POST, 'atb_previsao_termino', FILTER_SANITIZE_STRING);
            $data['antifungico'] = filter_input(INPUT_POST, 'antifungico', FILTER_SANITIZE_STRING);
            $data['antifungico_nome'] = filter_input(INPUT_POST, 'antifungico_nome', FILTER_SANITIZE_STRING);
            $data['antifungico_inicio'] = filter_input(INPUT_POST, 'antifungico_inicio', FILTER_SANITIZE_STRING);
            $data['antifungico_previsao_termino'] = filter_input(INPUT_POST, 'antifungico_previsao_termino', FILTER_SANITIZE_STRING);
            $data['evolucao_medica'] = $_POST['evolucao_medica'];
            $data['evolucao_medica']['evolucao'] = htmlentities(addslashes(strip_tags($data['evolucao_medica']['evolucao'], '<p><br><b><u><i><ol><ul><li><table><tbody><tr><th><td>')));
            $data['evolucao_enfermagem'] = $_POST['evolucao_enfermagem'];
            $data['evolucao_enfermagem']['evolucao'] = addslashes(strip_tags(htmlentities($data['evolucao_enfermagem']['evolucao'], '<p><br><b><u><i><ol><ul><li><table><tbody><tr><th><td>')));
            $data['imagens_lesoes'] = isset($_POST['imagens_lesoes']) ? $_POST['imagens_lesoes'] : [];
            //  $data['imagens_feridas_id'] = isset($_POST['imagens_feridas_id']) ? $_POST['imagens_feridas_id'] : [];
            $data['evolucao_fisioterapia'] = $_POST['evolucao_fisioterapia'];
            $data['evolucao_fisioterapia']['evolucao'] = htmlentities(addslashes(strip_tags($data['evolucao_fisioterapia']['evolucao'], '<p><br><b><u><i><ol><ul><li><table><tbody><tr><th><td>')));
            $data['evolucao_fonoaudiologia'] = $_POST['evolucao_fonoaudiologia'];
            $data['evolucao_fonoaudiologia']['evolucao'] = htmlentities(addslashes(strip_tags($data['evolucao_fonoaudiologia']['evolucao'], '<p><br><b><u><i><ol><ul><li><table><tbody><tr><th><td>')));
            $data['evolucao_psicologia'] = $_POST['evolucao_psicologia'];
            $data['evolucao_psicologia']['evolucao'] = htmlentities(addslashes(strip_tags($data['evolucao_psicologia']['evolucao'], '<p><br><b><u><i><ol><ul><li><table><tbody><tr><th><td>')));
            $data['evolucao_nutricao'] = $_POST['evolucao_nutricao'];
            $data['evolucao_nutricao']['evolucao'] = htmlentities(addslashes(strip_tags($data['evolucao_nutricao']['evolucao'], '<p><br><b><u><i><ol><ul><li><table><tbody><tr><th><td>')));
            $data['pad'] = isset($_POST['pad']) ? $_POST['pad'] : [];

            $formulario = new FormularioCondensadoModel(ConnMysqli::getConnection());
            $response = $formulario->save($data);

            if ($response) {
                $msg = "Formulário condensado criado com sucesso!";
                $type = 's';
                header("Location: /adm/secmed/?action=formulario-condensado&tipo=" . $data['tipo_formulario'] . "&msg=" . base64_encode($msg) . "&type=" . $type);
            } else {
                $msg = "Houve um problema ao atualizar os dados desse formulário!";
                $type = 'f';
                header("Location: /adm/secmed/?action=formulario-condensado&tipo=" . $data['tipo_formulario'] . "&msg=" . base64_encode($msg) . "&type=" . $type);
            }
        }
    }

    public static function formEditar()
    {
        if (isset($_GET) && !empty($_GET)) {
            $formulario = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

            $response = FormularioCondensadoModel::getByIdForUpdate($formulario);
            $response['genero'] = $response['sexo'] == '0' ? 'Masculino' : 'Feminino';
            $response['modalidade'] = self::$arrayModalidade;
            $equipamentos = [];
            foreach ($response['equipamentos'] as $equipamento) {
                $equipamentos[] = $equipamento['equipamento_id'];
            }
            $tipos_lesao = [];
            foreach ($response['tipos_lesao'] as $tipo_lesao) {
                $tipos_lesao[] = $tipo_lesao['lesao_id'];
            }
            $frequencias = Frequencia::getAll();
            $titulo_formulario = $response['tipo_formulario'] == 'p' ? 'de Prorrogação' : 'Deflagrado';
        }

        require($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/formulario-condensado/prorrogacao/form-editar.phtml');
    }

    public static function atualizarFormularioCondensado()
    {
        if (isset($_POST) && !empty($_POST)) {
            $data = [];

            $data['formulario_id'] = filter_input(INPUT_POST, 'formulario_id', FILTER_SANITIZE_NUMBER_INT);
            $data['tipo_formulario'] = filter_input(INPUT_POST, 'tipo_formulario', FILTER_SANITIZE_STRING);
            $data['inicio'] = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $data['dieta'] = filter_input(INPUT_POST, 'dieta', FILTER_SANITIZE_STRING);
            $data['dieta_via'] = filter_input(INPUT_POST, 'dieta_via', FILTER_SANITIZE_STRING);
            $data['dieta_nome'] = filter_input(INPUT_POST, 'dieta_nome', FILTER_SANITIZE_STRING);
            $data['dieta_adm'] = filter_input(INPUT_POST, 'dieta_adm', FILTER_SANITIZE_STRING);
            $data['oxigenoterapia'] = filter_input(INPUT_POST, 'oxigenoterapia', FILTER_SANITIZE_STRING);
            $data['vazao'] = filter_input(INPUT_POST, 'vazao', FILTER_SANITIZE_STRING);
            $data['deambula'] = filter_input(INPUT_POST, 'deambula', FILTER_SANITIZE_STRING);
            $data['drenos'] = filter_input(INPUT_POST, 'drenos', FILTER_SANITIZE_STRING);
            $data['equipamentos'] = isset($_POST['equipamentos']) ? $_POST['equipamentos'] : [];
            $data['equipamentos_outros'] = filter_input(INPUT_POST, 'equipamentos_outros', FILTER_SANITIZE_STRING);
            $data['integridade_cutanea'] = filter_input(INPUT_POST, 'integridade_cutanea', FILTER_SANITIZE_STRING);
            $data['tipo_lesao'] = isset($_POST['tipo_lesao']) ? $_POST['tipo_lesao'] : [];
            $data['atb'] = filter_input(INPUT_POST, 'atb', FILTER_SANITIZE_STRING);
            $data['atb_nome'] = filter_input(INPUT_POST, 'atb_nome', FILTER_SANITIZE_STRING);
            $data['atb_inicio'] = filter_input(INPUT_POST, 'atb_inicio', FILTER_SANITIZE_STRING);
            $data['atb_previsao_termino'] = filter_input(INPUT_POST, 'atb_previsao_termino', FILTER_SANITIZE_STRING);
            $data['antifungico'] = filter_input(INPUT_POST, 'antifungico', FILTER_SANITIZE_STRING);
            $data['antifungico_nome'] = filter_input(INPUT_POST, 'antifungico_nome', FILTER_SANITIZE_STRING);
            $data['antifungico_inicio'] = filter_input(INPUT_POST, 'antifungico_inicio', FILTER_SANITIZE_STRING);
            $data['antifungico_previsao_termino'] = filter_input(INPUT_POST, 'antifungico_previsao_termino', FILTER_SANITIZE_STRING);
            $data['evolucao_medica'] = $_POST['evolucao_medica'];
            if(isset($_POST['escolha_medica_evolucao']) && $_POST['escolha_medica_evolucao'] == 2) {
                $data['evolucao_medica'] = $_POST['evolucao_medica_nova'];
            }
            $data['evolucao_medica']['evolucao'] = htmlentities(addslashes(strip_tags($data['evolucao_medica']['evolucao'], '<p><br><b><u><i><ol><ul><li><table><tbody><tr><th><td>')));

            $data['evolucao_enfermagem'] = $_POST['evolucao_enfermagem'];
            $data['imagens_lesoes'] = isset($_POST['imagens_lesoes']) ? $_POST['imagens_lesoes'] : [];
            if(isset($_POST['escolha_enfermagem_evolucao']) && $_POST['escolha_enfermagem_evolucao'] == 2) {
                $data['evolucao_enfermagem'] = $_POST['evolucao_enfermagem_nova'];
                $data['imagens_lesoes'] = isset($_POST['imagens_lesoes_nova']) ? $_POST['imagens_lesoes_nova'] : [];
            }
            $data['evolucao_enfermagem']['evolucao'] = htmlentities(addslashes(strip_tags($data['evolucao_enfermagem']['evolucao'], '<p><br><b><u><i><ol><ul><li><table><tbody><tr><th><td>')));

            $data['evolucao_fisioterapia'] = $_POST['evolucao_fisioterapia'];
            if(isset($_POST['escolha_fisioterapia_evolucao']) && $_POST['escolha_fisioterapia_evolucao'] == 2) {
                $data['evolucao_fisioterapia'] = $_POST['evolucao_fisioterapia_prestador'];
            }
            $data['evolucao_fisioterapia']['evolucao'] = htmlentities(addslashes(strip_tags($data['evolucao_fisioterapia']['evolucao'], '<p><br><b><u><i><ol><ul><li><table><tbody><tr><th><td>')));

            $data['evolucao_fonoaudiologia'] = $_POST['evolucao_fonoaudiologia'];
            if(isset($_POST['escolha_fonoaudiologia_evolucao']) && $_POST['escolha_fonoaudiologia_evolucao'] == 2) {
                $data['evolucao_fonoaudiologia'] = $_POST['evolucao_fonoaudiologia_prestador'];
            }
            $data['evolucao_fonoaudiologia']['evolucao'] = htmlentities(addslashes(strip_tags($data['evolucao_fonoaudiologia']['evolucao'], '<p><br><b><u><i><ol><ul><li><table><tbody><tr><th><td>')));

            $data['evolucao_psicologia'] = $_POST['evolucao_psicologia'];
            if(isset($_POST['escolha_psicologia_evolucao']) && $_POST['escolha_psicologia_evolucao'] == 2) {
                $data['evolucao_psicologia'] = $_POST['evolucao_psicologia_prestador'];
            }
            $data['evolucao_psicologia']['evolucao'] = htmlentities(addslashes(strip_tags($data['evolucao_psicologia']['evolucao'], '<p><br><b><u><i><ol><ul><li><table><tbody><tr><th><td>')));

            $data['evolucao_nutricao'] = $_POST['evolucao_nutricao'];
            if(isset($_POST['escolha_nutricao_evolucao']) && $_POST['escolha_nutricao_evolucao'] == 2) {
                $data['evolucao_nutricao'] = $_POST['evolucao_nutricao_prestador'];
            }
            $data['evolucao_nutricao']['evolucao'] = htmlentities(addslashes(strip_tags($data['evolucao_nutricao']['evolucao'], '<p><br><b><u><i><ol><ul><li><table><tbody><tr><th><td>')));

            $data['pad'] = isset($_POST['pad']) ? $_POST['pad'] : [];
            $data['modalidade_id'] =  $_POST['modalidade_id'];

            $formulario = new FormularioCondensadoModel(ConnMysqli::getConnection());
            $response = $formulario->update($data);

            if ($response) {
                $msg = "Formulário condensado atualizado com sucesso!";
                $type = 's';
                header("Location: /adm/secmed/?action=formulario-condensado-form-editar&id={$data['formulario_id']}&msg=" . base64_encode($msg) . "&type=" . $type);
            } else {
                $msg = "Houve um problema ao atualizar os dados desse formulário!";
                $type = 'f';
                header("Location: /adm/secmed/?action=formulario-condensado-form-editar&id={$data['formulario_id']}&msg=" . base64_encode($msg) . "&type=" . $type);
            }
        }
    }

    public static function visualizarFormularioCondensado()
    {
        if (isset($_GET) && !empty($_GET)) {
            ini_set('display_erros', '1');
            $formulario = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

            $response = FormularioCondensadoModel::getByIdForUpdate($formulario);
            $response['modalidade'] = '';
            if (!empty($response['modalidade_id'])) {
                $response['modalidade'] = self::$arrayModalidade[$response['modalidade_id']];
            }

            $response['inicio'] = \DateTime::createFromFormat('Y-m-d', $response['inicio'])->format('d/m/Y');
            $response['fim'] = \DateTime::createFromFormat('Y-m-d', $response['fim'])->format('d/m/Y');
            $response['genero'] = $response['sexo'] == '0' ? 'Masculino' : 'Feminino';
            $equipamentos = [];
            foreach ($response['equipamentos'] as $equipamento) {
                $equipamentos[] = $equipamento['equipamento_id'];
            }
            $tipos_lesao = [];
            foreach ($response['tipos_lesao'] as $tipo_lesao) {
                $tipos_lesao[] = $tipo_lesao['lesao_id'];
            }
            $frequencias = Frequencia::getAll();
            $titulo_formulario = $response['tipo_formulario'] == 'p' ? 'de Prorrogação' : 'Deflagrado';
        }

        require($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/formulario-condensado/prorrogacao/visualizar.phtml');
    }

    public static function imprimirFormularioCondensado()
    {
        if (isset($_GET) && !empty($_GET)) {
            $formulario = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
            $titulo = filter_input(INPUT_GET, 'titulo', FILTER_SANITIZE_STRING);
            $titulo = base64_decode($titulo);

            $response = FormularioCondensadoModel::getByIdForUpdate($formulario);
            $operadoraId = Operadora::getByPlanoID($response['convenio_id']);
            if($titulo != 'Relatório Condensado') {
                $titulo = $response['tipo_formulario'] == 'p' ? 'Prorrogação' : 'Deflagrado';
            }

            $response['inicio'] = \DateTime::createFromFormat('Y-m-d', $response['inicio'])->format('d/m/Y');
            $response['fim'] = \DateTime::createFromFormat('Y-m-d', $response['fim'])->format('d/m/Y');
            $response['genero'] = $response['sexo'] == '0' ? 'Masculino' : 'Feminino';
            $equipamentos = [];
            foreach ($response['equipamentos'] as $equipamento) {
                $equipamentos[] = $equipamento['equipamento_id'];
            }
            $tipos_lesao = [];
            foreach ($response['tipos_lesao'] as $tipo_lesao) {
                $tipos_lesao[] = $tipo_lesao['lesao_id'];
            }

            $responseEmpresa = Filial::getEmpresaById($_GET['empresa']);
            include_once($_SERVER['DOCUMENT_ROOT'] . '/utils/mpdf/mpdf.php');

            $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__).'/../');
            $auxDocRoot = explode('app/Controllers', $_SERVER['DOCUMENT_ROOT']);
            $_SERVER['DOCUMENT_ROOT'] = $auxDocRoot[0];

            $clearEvolucao = function ($evolucao) {
                if (!is_string ($evolucao) || trim ($evolucao) == '')
                    return $evolucao;

                return htmlspecialchars_decode(
                    preg_replace(
                        "/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i",
                        '<$1$2>',
                        stripslashes(
                            $evolucao
                        )
                    )
                );
            };

            /*ini_set('xdebug.var_display_max_depth', 5);
            ini_set('xdebug.var_display_max_children', 512);
            ini_set('xdebug.var_display_max_data', 2048);

            die(
                '<pre>' . var_dump(
                $clearEvolucao($response['evolucao_medica']['evolucao']),
                $clearEvolucao($response['evolucao_enfermagem']['evolucao']),
                $clearEvolucao($response['evolucao_fisioterapia']['evolucao']),
                $clearEvolucao($response['evolucao_fonoaudiologia']['evolucao']),
                $clearEvolucao($response['evolucao_psicologia']['evolucao']),
                    $clearEvolucao($response['evolucao_nutricao']['evolucao'])
                )
            );*/

            $css = '/adm/secmed/templates/formulario-condensado/css/pdf.css';
            $html = "
<html>
<head>
    <link rel='stylesheet' href='{$css}'>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
</head>
<body>";
            if ($operadoraId[0] == 11) {
                $html .= "
    <table  class='table'>
        <tr>
            <td class='col-xs-2 text-left'>
                <img width='120' height='70' src='{$_SERVER['DOCUMENT_ROOT']}/utils/logo-planserv.jpg'>
            </td>
            <td class='col-xs-8 text-center'>
                <h1>{$titulo}</h1>
            </td>
            <td class='col-xs-2' >
                <img width='120'  height='70' src='{$_SERVER['DOCUMENT_ROOT']}/utils/logos/{$responseEmpresa['logo']}'> 
            </td>
        </tr>
    </table>";
            } else {
                $html .= "
    <table  class='table'>
    <tr>
    <td class='col-xs-2 text-left'>
    <img width='150'  height='80' src='{$_SERVER['DOCUMENT_ROOT']}/utils/logos/{$responseEmpresa['logo']}'> 
    </td>
    <td class='col-xs-10 text-center'><h1>{$titulo}</h1></td>
    
    </tr>
    </table>";
            }

            $html .= "<table class='table table-condensed table-striped table-bordered'>
    <tbody>
    <tr>
        <td colspan='3'><strong>INFORMAÇÕES SOBRE O PACIENTE</strong></td>
    </tr>
    <tr>
        <td class='col-xs-6'><strong>PERÍODO</strong></td>
        <td class='col-xs-6' colspan='2'><b>PACIENTE</b></td>
    </tr>
    <tr>
        <td class='col-xs-6'>
            {$response['inicio']}
            à
            {$response['fim']}
        </td>
        <td class='col-xs-6' colspan='2'>
            {$response['paciente']}
        </td>
    </tr>
    <tr>
        <td class='col-xs-6'><b>GÊNERO</b></td>
        <td class='col-xs-6' colspan='2'><b>IDADE</b></td>
    </tr>
    <tr>
        <td class='col-xs-6' id='genero'>{$response['genero']}</td>
        <td class='col-xs-6' colspan='2' id='idade'>{$response['idade']}</td>
    </tr>
    <tr>
        <td class='col-xs-6'><b>UNIDADE REGIONAL</b></td>
        <td class='col-xs-6' colspan='2'><b>CONVÊNIO</b></td>
    </tr>
    <tr>
        <td class='col-xs-6' id='unidade-regional'>{$response['nomeempresa']}</td>
        <td class='col-xs-6' colspan='2' id='convenio-paciente'>{$response['nomeplano']}</td>
    </tr>
    <tr>
        <td class='col-xs-6'><b>MATRÍCULA</b></td>
        <td class='col-xs-6' colspan='2'><b>MODALIDADE</b></td>
    </tr>
    <tr>
        <td class='col-xs-6' id='matricula'>{$response['matricula']}</td>
        <td class='col-xs-6' colspan='2' id='modalidade'>".self::$arrayModalidade[$response['modalidade_id']]."</td>
    </tr>
    <tr>
        <td class='col-xs-6'><b>DIETA</b></td>
        <td class='col-xs-6' colspan='2'><b>VIA</b></td>
    </tr>
    <tr>
        <td class='col-xs-6'>
            ( " . ($response['dieta'] == '1' ? 'x' : '') . " ) Artesanal
            ( " . ($response['dieta'] == '2' ? 'x' : '') . " ) Industrializada
            ( " . ($response['dieta'] == '3' ? 'x' : '') . " ) Mista
        </td>
        <td class='col-xs-6' colspan='2'>
            ( " . ($response['dieta_via'] == '1' ? 'x' : '') . " ) Oral
            ( " . ($response['dieta_via'] == '2' ? 'x' : '') . " ) SNE
            ( " . ($response['dieta_via'] == '3' ? 'x' : '') . " ) GTT
            ( " . ($response['dieta_via'] == '4' ? 'x' : '') . " ) Jejuno
            ( " . ($response['dieta_via'] == '5' ? 'x' : '') . " ) BI
            ( " . ($response['dieta_via'] == '6' ? 'x' : '') . " ) Gravitacional
        </td>
    </tr>
    <tr>
        <td class='col-xs-6'><b>NOME DA DIETA</b></td>
        <td class='col-xs-6' colspan='2'><b>ADMINISTRAÇÃO DA DIETA</b></td>
    </tr>
    <tr>
        <td class='col-xs-6'>
            {$response['dieta_nome']}
        </td>
        <td class='col-xs-6' colspan='2'>
            {$response['dieta_adm']}
        </td>
    </tr>
    <tr>
        <td class='col-xs-6'><b>OXIGENOTERAPIA</b></td>
        <td class='col-xs-6' colspan='2'><b>VAZÃO</b></td>
    </tr>
    <tr>
        <td class='col-xs-6'>
            ( " . ($response['oxigenoterapia'] == '1' ? 'x' : '') . " ) Concentrador
            ( " . ($response['oxigenoterapia'] == '2' ? 'x' : '') . " ) Torpedo de O²
        </td>
        <td class='col-xs-6' colspan='2'>
            ( " . ($response['oxigenoterapia_vazao'] == '1' ? 'x' : '') . " ) 1 L/M
            ( " . ($response['oxigenoterapia_vazao'] == '2' ? 'x' : '') . " ) 2 L/M
            ( " . ($response['oxigenoterapia_vazao'] == '3' ? 'x' : '') . " ) 3 L/M
            ( " . ($response['oxigenoterapia_vazao'] == '4' ? 'x' : '') . " ) 4 L/M
            ( " . ($response['oxigenoterapia_vazao'] == '5' ? 'x' : '') . " ) 5 L/M
            ( " . ($response['oxigenoterapia_vazao'] == '6' ? 'x' : '') . " ) ACIMA DE 5 L/M
        </td>
    </tr>
    <tr>
        <td class='col-xs-6'><b>DEAMBULA</b></td>
        <td class='col-xs-6' colspan='2'><b>DRENO / CATETERES / OSTOMIAS</b></td>
    </tr>
    <tr>
        <td class='col-xs-6'>
            ( " . ($response['deambula'] == '1' ? 'x' : '') . " ) Sim. sem auxílio
            ( " . ($response['deambula'] == '2' ? 'x' : '') . " ) Sim, com auxílio
            ( " . ($response['deambula'] == '3' ? 'x' : '') . " ) Não
        </td>
        <td class='col-xs-6' colspan='2'>
            ( " . ($response['drenos_cateteres_ostomias'] == '1' ? 'x' : '') . " ) Sim. família apta
            ( " . ($response['drenos_cateteres_ostomias'] == '2' ? 'x' : '') . " ) Sim, família não apta
            ( " . ($response['drenos_cateteres_ostomias'] == '3' ? 'x' : '') . " ) Não
        </td>
    </tr>
    <tr>
        <td class='col-xs-12' colspan='3'><b>EQUIPAMENTOS</b></td>
    </tr>
    <tr>
        <td class='col-xs-6'>
            <div class='col-xs-3'>
                ( " . (in_array('1', $equipamentos) ? 'x' : '') . " ) Cama Completa
                ( " . (in_array('2', $equipamentos) ? 'x' : '') . " ) Cadeira de Rodas
                ( " . (in_array('3', $equipamentos) ? 'x' : '') . " ) Cadeira Higiênica
                ( " . (in_array('4', $equipamentos) ? 'x' : '') . " ) Cama Elétrica
            </div>
            <div class='col-xs-3'>
                ( " . (in_array('5', $equipamentos) ? 'x' : '') . " ) Nebulizador
                ( " . (in_array('6', $equipamentos) ? 'x' : '') . " ) Aspirador
                ( " . (in_array('7', $equipamentos) ? 'x' : '') . " ) Glicosímetro
                ( " . (in_array('8', $equipamentos) ? 'x' : '') . " ) BI
            </div>
        </td>
        <td class='col-xs-6' colspan='2'>
            <div class='col-xs-3'>
                ( " . (in_array('9', $equipamentos) ? 'x' : '') . " ) Oxímetro
                ( " . (in_array('10', $equipamentos) ? 'x' : '') . " ) BI-pap
                ( " . (in_array('11', $equipamentos) ? 'x' : '') . " ) Nobreak
                ( " . (in_array('12', $equipamentos) ? 'x' : '') . " ) Respirador
            </div>
            <div class='col-xs-3'>
                ( " . (in_array('13', $equipamentos) ? 'x' : '') . " ) Suporte de Soro
                ( " . (in_array('14', $equipamentos) ? 'x' : '') . " ) Recarga Torpedo
                ( " . (in_array('15', $equipamentos) ? 'x' : '') . " ) Andador
                ( " . (in_array('16', $equipamentos) ? 'x' : '') . " ) Colchão Pneumático
            </div>
        </td>
    </tr>
    <tr>
        <td class='col-xs-12' colspan='3'>
            <div class='col-xs-12'>
                ( " . (in_array('17', $equipamentos) ? 'x' : '') . " ) Outros: {$response['equipamentos_outros']}
            </div>
        </td>
    </tr>
    <tr>
        <td class='col-xs-6'><b>INTEGRIDADE CUTÂNEA</b></td>
        <td class='col-xs-6' colspan='2'><b>TIPO DA LESÃO</b></td>
    </tr>
    <tr>
        <td class='col-xs-6'>
            ( " . ($response['integridade_cutanea'] == '1' ? 'x' : '') . " ) Íntegra
            ( " . ($response['integridade_cutanea'] == '2' ? 'x' : '') . " ) Com lesão
        </td>
        <td class='col-xs-6'>
            <div class='col-xs-6'>
                ( " . (in_array('1', $tipos_lesao) ? 'x' : '') . " ) LPP
                ( " . (in_array('2', $tipos_lesao) ? 'x' : '') . " ) Úlcera Venosa
                ( " . (in_array('3', $tipos_lesao) ? 'x' : '') . " ) Úlcera Diabética
                ( " . (in_array('4', $tipos_lesao) ? 'x' : '') . " ) Lesão Oncológica
            </div>
            <div class='col-xs-6'>
                ( " . (in_array('5', $tipos_lesao) ? 'x' : '') . " ) FO
                ( " . (in_array('6', $tipos_lesao) ? 'x' : '') . " ) Queimadura
                ( " . (in_array('7', $tipos_lesao) ? 'x' : '') . " ) Deiscencia FO
                ( " . (in_array('8', $tipos_lesao) ? 'x' : '') . " ) Dermatite incontinência
            </div>
        </td>
    </tr>
    <tr>
        <td class='col-xs-6'><b>ANTIBIÓTICO</b></td>
        <td class='col-xs-6' colspan='2'><b>PERÍODO</b></td>
    </tr>
    <tr>
        <td class='col-xs-6'>
            ( " . ($response['atb'] == '1' ? 'x' : '') . " ) Sim
            ( " . ($response['atb'] == '0' ? 'x' : '') . " ) Não
        </td>";

            if ($response['atb'] == '1') {
                $html .= "<td class='col-xs-6' colspan='2'>
            " . ($response['atb_inicio'] != '' ? \DateTime::createFromFormat('Y-m-d', $response['atb_inicio'])->format('d/m/Y') : '') . " à
            " . ($response['atb_previsao_termino'] != '' ? \DateTime::createFromFormat('Y-m-d', $response['atb_previsao_termino'])->format('d/m/Y') : '') . "
        </td>";
            } else {
                $html .= "<td class='col-xs-6' colspan='2'></td>";
            }
            $html .= "</tr>
    <tr>
        <td class='col-xs-6'><b>ANTIFÚNGICO</b></td>
        <td class='col-xs-6' colspan='2'><b>PERÍODO</b></td>
    </tr>
    <tr>
        <td class='col-xs-6'>
            ( " . ($response['antifungico'] == '1' ? 'x' : '') . " ) Sim
            ( " . ($response['antifungico'] == '0' ? 'x' : '') . " ) Não
        </td>";
            if ($response['antifungico'] == '1') {
                $html .= "<td class='col-xs-6' colspan='2'>
            " . ($response['antifungico_inicio'] != '' ? \DateTime::createFromFormat('Y-m-d', $response['antifungico_inicio'])->format('d/m/Y') : '') . " à
            " . ($response['antifungico_previsao_termino'] != '' ? \DateTime::createFromFormat('Y-m-d', $response['antifungico_previsao_termino'])->format('d/m/Y') : '') . "
        </td>";
            } else {
                $html .= "<td class='col-xs-6' colspan='2'></td>";
            }
            $html .= "</tr>
</tbody>
</table>
<div style='page-break-before: always;'></div>
<table class='table  table-bordered'>
    <tbody>
    <tr>
        <td colspan='3' class='col-xs-12'><b>EVOLUÇÃO MÉDICA</b></td>
    </tr>
    <tr>
        <td colspan='2' class='col-xs-10'>". $clearEvolucao($response['evolucao_medica']['evolucao']) . "</td>
        <td width='10%' class='col-xs-2' align='center' valign='middle'>
            " . (!empty($response['evolucao_medica']['assinatura']) ? "<img width='100' height='70' src='{$_SERVER['DOCUMENT_ROOT']}/" . end(explode('../', $response['evolucao_medica']['assinatura'])) . "'>" : '') . "
        </td>
    </tr>
    <tr>
        <td class='col-xs-4'><b>Data da Visita:</b> " . ($response['evolucao_medica']['data_visita'] != '' ? \DateTime::createFromFormat('Y-m-d', $response['evolucao_medica']['data_visita'])->format('d/m/Y') : '') . "</td>
        <td class='col-xs-4'><b>Médico(a):</b> {$response['evolucao_medica']['profissional']}</td>
        <td class='col-xs-4'><b>CRM:</b> {$response['evolucao_medica']['numero_conselho']}</td>
    </tr>
    <tr>
        <td colspan='3' class='col-xs-12'><b>EVOLUÇÃO DE ENFERMAGEM</b></td>
    </tr>
    <tr>
        <td colspan='2' class='col-xs-10'>". $clearEvolucao($response['evolucao_enfermagem']['evolucao']) . "</td>
        <td width='10%' class='col-xs-2' align='center' valign='middle'>
            " . (!empty($response['evolucao_enfermagem']['assinatura']) ? "<img width='100' height='70' src='{$_SERVER['DOCUMENT_ROOT']}/" . end(explode('../', $response['evolucao_enfermagem']['assinatura'])) . "'>" : '') . "
        </td>
    </tr>
    <tr>
        <td class='col-xs-4'><b>Data da Visita:</b> " . ($response['evolucao_enfermagem']['data_visita'] != '' ? \DateTime::createFromFormat('Y-m-d', $response['evolucao_enfermagem']['data_visita'])->format('d/m/Y') : '') . "</td>
        <td class='col-xs-4'><b>Enfermeiro(a):</b> {$response['evolucao_enfermagem']['profissional']}</td>
        <td class='col-xs-4'><b>COREN:</b> {$response['evolucao_enfermagem']['numero_conselho']}</td>
    </tr>
    </tbody>
    </table>
    
    <table class='table'>
    <tbody>
    <tr>
        <td colspan='3' class='col-xs-12' align='center'><b>FOTOS DA LESÃO</b></td>
    </tr>
    <tr>
        <td colspan='3' class='col-xs-12'>";

            if(count($response['imagens_lesoes'])> 0){
                $quantidadeDeImagens = count($response['imagens_lesoes']);
                $aux = 1;
                foreach ($response['imagens_lesoes'] as $imagens_lesoes) {
                    if($aux == 1 || ($aux-1) % 3 == 0){

                        $html .= "<tr>";
                    }

                    $html .= "<td class='col-xs-4' style='margin-bottom: 5px;'>
                <div class='thumbnail text-center'>
                    <img width='150px' src='{$_SERVER['DOCUMENT_ROOT']}/{$imagens_lesoes['path']}'>
                    <div class='caption'>
                        <br>
                        <h6><b>{$aux}. {$imagens_lesoes['LOCAL']}</b></h6>
                    </div>
                </div>
            
            </td>";

                    if($aux == $quantidadeDeImagens || $aux % 3 == 0){

                        $html .= "</tr>";
                    }
                    $aux++;
                }
            }

            $html .=" 
    </tbody>
    </table>
    <table class='table  table-bordered'>
    <tbody>
    <tr>
        <td colspan='3' class='col-xs-12'><b>EVOLUÇÃO DE FISIOTERAPIA</b></td>
    </tr>
    <tr>
        <td colspan='2' width='90%' class='col-xs-10'>". $clearEvolucao($response['evolucao_fisioterapia']['evolucao']) . "</td>
        <td width='10%' class='col-xs-2' align='center' valign='middle'>
            " . (!empty($response['evolucao_fisioterapia']['assinatura']) ? "<img width='100' height='70' src='{$_SERVER['DOCUMENT_ROOT']}/" . end(explode('../', $response['evolucao_fisioterapia']['assinatura'])) . "'>" : '') . "
        </td>
    </tr>
    <tr>
        <td class='col-xs-4'><b>Data da Visita:</b> " . ($response['evolucao_fisioterapia']['data_visita'] != '' ? \DateTime::createFromFormat('Y-m-d', $response['evolucao_fisioterapia']['data_visita'])->format('d/m/Y') : '') . "</td>
        <td class='col-xs-4'><b>Fisioterapeuta:</b> {$response['evolucao_fisioterapia']['profissional']}</td>
        <td class='col-xs-4'><b>CREFITO:</b> {$response['evolucao_fisioterapia']['numero_conselho']}</td>
    </tr>
    
    <tr>
        <td colspan='3' class='col-xs-12'><b>EVOLUÇÃO DE FONOAUDIÓLOGO</b></td>
    </tr>
    <tr>
        <td colspan='2' class='col-xs-10'>". $clearEvolucao($response['evolucao_fonoaudiologia']['evolucao']) . "</td>
        <td width='10%' class='col-xs-2' align='center' valign='middle'>
            " . (!empty($response['evolucao_fonoaudiologia']['assinatura']) ? "<img width='100' height='70' src='{$_SERVER['DOCUMENT_ROOT']}/" . end(explode('../', $response['evolucao_fonoaudiologia']['assinatura'])) . "'>" : '') . "
        </td>
    </tr>
    <tr>
        <td class='col-xs-4'><b>Data da Visita:</b> " . ($response['evolucao_fonoaudiologia']['data_visita'] != '' ? \DateTime::createFromFormat('Y-m-d', $response['evolucao_fonoaudiologia']['data_visita'])->format('d/m/Y') : '') . "</td>
        <td class='col-xs-4'><b>Fonoaudiólogo(a):</b> {$response['evolucao_fonoaudiologia']['profissional']}</td>
        <td class='col-xs-4'><b>CRFa:</b> {$response['evolucao_fonoaudiologia']['numero_conselho']}</td>
    </tr>
    <tr>
        <td colspan='3' class='col-xs-12'><b>EVOLUÇÃO DE NUTRICIONAL</b></td>
    </tr>
    <tr>
        <td colspan='2' class='col-xs-10'>". $clearEvolucao($response['evolucao_nutricao']['evolucao']) . "</td>
        <td width='10%' class='col-xs-2' align='center' valign='middle'>
            " . (!empty($response['evolucao_nutricao']['assinatura']) ? "<img width='100' height='70' src='{$_SERVER['DOCUMENT_ROOT']}/" . end(explode('../', $response['evolucao_nutricao']['assinatura'])) . "'>" : '') . "
        </td>
    </tr>
    <tr>
        <td class='col-xs-4'><b>Data da Visita:</b> " . ($response['evolucao_nutricao']['data_visita'] != '' ? \DateTime::createFromFormat('Y-m-d', $response['evolucao_nutricao']['data_visita'])->format('d/m/Y') : '') . "</td>
        <td class='col-xs-4'><b>Nutricionista:</b> {$response['evolucao_nutricao']['profissional']}</td>
        <td class='col-xs-4'><b>CRN:</b> {$response['evolucao_nutricao']['numero_conselho']}</td>
    </tr>
    <tr>
        <td colspan='3' class='col-xs-12'><b>EVOLUÇÃO PSICÓLOGO</b></td>
    </tr>
    <tr>
        <td colspan='2' class='col-xs-10'>". $clearEvolucao($response['evolucao_psicologia']['evolucao']) . "</td>
        <td width='10%' class='col-xs-2' align='center' valign='middle'>
            " . (!empty($response['evolucao_psicologia']['assinatura']) ? "<img width='100' height='70' src='{$_SERVER['DOCUMENT_ROOT']}/" . end(explode('../', $response['evolucao_psicologia']['assinatura'])) . "'>" : '') . "
        </td>
    </tr>
    <tr>
        <td class='col-xs-4'><b>Data da Visita:</b> " . ($response['evolucao_psicologia']['data_visita'] != '' ? \DateTime::createFromFormat('Y-m-d', $response['evolucao_psicologia']['data_visita'])->format('d/m/Y') : '') . "</td>
        <td class='col-xs-4'><b>Psicólogo(a):</b> {$response['evolucao_psicologia']['profissional']}</td>
        <td class='col-xs-4'><b>CRP:</b> {$response['evolucao_psicologia']['numero_conselho']}</td>
    </tr>
    </tbody>
</table>";

            if ($response['tipo_formulario'] == 'p') {
                $html .= "<br>
<table class='table table-condensed table-striped table-bordered'>
    <tbody>
    <tr>
        <td colspan='3' class='col-xs-12'><b>PAD</b></td>
    </tr>
    <tr>
        <td class='col-xs-6'><b>Modalidade</b></td>
        <td colspan='2' class='col-xs-6'>{$response['pad']['modalidade']}</td>
    </tr>
    <tr>
        <td class='col-xs-4'><b>Médico</b></td>
        <td class='col-xs-4'>{$response['pad']['medico_cuidado']}</td>
        <td class='col-xs-4'>{$response['pad']['medico_frequencia']}</td>
    </tr>
    <tr>
        <td class='col-xs-4'><b>Enfermeiro</b></td>
        <td class='col-xs-4'>{$response['pad']['enfermagem_cuidado']}</td>
        <td class='col-xs-4'>{$response['pad']['enfermagem_frequencia']}</td>
    </tr>
    <tr>
        <td class='col-xs-4'><b>Técnico de Enfermagem</b></td>
        <td class='col-xs-4'>{$response['pad']['tecnico_cuidado']}</td>
        <td class='col-xs-4'>{$response['pad']['tecnico_frequencia']}</td>
    </tr>
    <tr>
        <td class='col-xs-4'><b>Fisoterapia Motora</b></td>
        <td class='col-xs-4'>{$response['pad']['fisio_motora_cuidado']}</td>
        <td class='col-xs-4'>{$response['pad']['fisio_motora_frequencia']}</td>
    </tr>
    <tr>
        <td class='col-xs-4'><b>Fisioterapia Respiratória</b></td>
        <td class='col-xs-4'>{$response['pad']['fisio_resp_cuidado']}</td>
        <td class='col-xs-4'>{$response['pad']['fisio_resp_frequencia']}</td>
    </tr>
    <tr>
        <td class='col-xs-4'><b>Fonoterapia</b></td>
        <td class='col-xs-4'>{$response['pad']['fono_cuidado']}</td>
        <td class='col-xs-4'>{$response['pad']['fono_frequencia']}</td>
    </tr>
    <tr>
        <td class='col-xs-4'><b>Psicólogo</b></td>
        <td class='col-xs-4'>{$response['pad']['psico_cuidado']}</td>
        <td class='col-xs-4'>{$response['pad']['psico_frequencia']}</td>
    </tr>
    <tr>
        <td class='col-xs-4'><b>Nutricionista</b></td>
        <td class='col-xs-4'>{$response['pad']['nutri_cuidado']}</td>
        <td class='col-xs-4'>{$response['pad']['nutri_frequencia']}</td>
    </tr>
    </tbody>
</table>
</body>
</html>";
            }

            $extension = 'pdf';
            $dir = $_SERVER['DOCUMENT_ROOT'].'/adm/secmed/templates/formulario-condensado/temp';
            $fileNameAtual = sprintf(
                "formulario_condensado_%s_%s.%s",
                $response['inicio'],
                $response['fim'],
                $extension
            );

            self::criarArquivoImpressao($html, $dir, $fileNameAtual);
        }
        return true;
    }

    private static function criarArquivoImpressao($html, $dir, $filename)
    {
        //ini_set('display_errors', 1);
        $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__).'/../');
        $auxDocRoot = explode('app/Controllers', $_SERVER['DOCUMENT_ROOT']);
        $_SERVER['DOCUMENT_ROOT'] = $auxDocRoot[0];

        include_once($_SERVER['DOCUMENT_ROOT'] . '/utils/mpdf/mpdf.php');

        $file = $_SERVER['DOCUMENT_ROOT'] . 'adm/secmed/templates/formulario-condensado/temp/imprimir-formulario.phtml';
        file_put_contents($file, $html);
      //  die(print_r(file_get_contents($file)));
        $mpdf = new \mPDF('pt', 'A4-L', 10);
        $mpdf->WriteHTML("<html><body>");
        $mpdf->WriteHTML(file_get_contents($file));
        $mpdf->WriteHTML("</body></html>");
        $mpdf->Output($filename, 'I');
    }
}
