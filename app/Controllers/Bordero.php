<?php
namespace app\Controllers;

use App\Helpers\StringHelper;
use App\Models\Financeiro\Banco;
use App\Models\Financeiro\BorderoTipo;
use App\Models\Financeiro\ContaBancaria;
use App\Models\Financeiro\Fornecedor;
use App\Models\Financeiro\Parcela;
use App\Models\Financeiro\TipoDocumentoFinanceiro;
use App\Models\Financeiro\Notas;
use \App\Models\Financeiro\Bordero as BorderoModel;
use \GuzzleHttp\Client;
use Dompdf\Dompdf;
use Dompdf\Options;

include_once('../../utils/codigos.php');

class Bordero
{
    public static function indexBordero()
    {
        $bancos = Banco::getAll();
        $contas = ContaBancaria::getContasBancariasByURs($_SESSION['empresa_user']);
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/cnab/templates/index-bordero.phtml');
    }

    public static function criarBordero()
    {
        $bancosBordero = [
            33 =>'Santander',
            341 => 'Itaú',
            237 => 'Bradesco',
        ];

        $arrayNaoListarTipoBordero = [
            33 =>[4, 12, 13, 14,15],
            341 => [3, 4, 6, 8, 7],
            237 => [3, 12, 13, 14,15],
        ];
        
        $idOriemFundos = $_GET['banco'] == 341 ? 136 : '';

        $tipo = 'S';
        $tipo_fornecedor = 'F';
        $banco = '';
        $title = '';
        $data = '';
        if(isset($_GET['banco'])){
            $banco = $_GET['banco'];
            $title = $bancosBordero[$banco];             
            $data['notIn'] = $arrayNaoListarTipoBordero[$banco];      
        }        
        $fornecedores = Fornecedor::getAllByTipo($tipo_fornecedor);
        $tipos_bordero = BorderoTipo::getByFilter($data);
        $contas = ContaBancaria::getContasBancariasCnabByURs($_SESSION['empresa_user'], '', $idOriemFundos);
        $tiposDocumentoFinanceiro = TipoDocumentoFinanceiro::getAll();
        $hoje = (new \DateTime())->format('Y-m-d');
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/cnab/templates/criar-bordero.phtml');
    }

    public static function editarBordero()
    {
        if(isset($_GET) && !empty($_GET)) {
            $borderoId  = filter_input(INPUT_GET, 'bordero', FILTER_SANITIZE_NUMBER_INT);
            $tipo = 'S';
            $tipo_fornecedor = 'F';
            $data = '';
            $arrayNaoListarTipoBordero = [
                33 =>[4, 12, 13, 14, 15],
                341 => [3, 4, 6, 8, 7],
                237 => [3, 12, 13, 14, 15],
            ];
            $bancosBordero = [
                33 =>'Santander',
                341 => 'Itaú',
                237 => 'Bradesco',
            ];
            if(isset($_GET['banco'])){
                $banco = (int) $_GET['banco'];
                $title = $bancosBordero[$banco];             
                $data['notIn'] = $arrayNaoListarTipoBordero[$banco];      
            } 
            $idOriemFundos = $_GET['banco'] == 341 ? 136 : '';
            

            $bordero = BorderoModel::getDadosBorderoEditar($borderoId);
            $tipos_bordero = BorderoTipo::getByFilter($data);
            $fornecedores = Fornecedor::getAllByTipo($tipo_fornecedor);
            $contas = ContaBancaria::getContasBancariasCnabByURs($_SESSION['empresa_user'], '', $idOriemFundos);
            $tiposDocumentoFinanceiro = TipoDocumentoFinanceiro::getAll();
            $hoje = (new \DateTime())->format('Y-m-d');
            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/cnab/templates/editar-bordero.phtml');
        }
    }

    public static function pesquisarParcelas()
    {
        if(isset($_GET) && !empty($_GET)){
            $inicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $tr = filter_input(INPUT_GET, 'tr', FILTER_SANITIZE_NUMBER_INT);
            $tipo_nota = filter_input(INPUT_GET, 'tipo_nota', FILTER_SANITIZE_STRING);
            $fornecedor = filter_input(INPUT_GET, 'fornecedor', FILTER_SANITIZE_NUMBER_INT);
            $tipo_documento_financeiro = filter_input(INPUT_GET, 'tipo_documento_financeiro', FILTER_SANITIZE_NUMBER_INT);
            $tipo_bordero = filter_input(INPUT_GET, 'tipo_bordero', FILTER_SANITIZE_NUMBER_INT);
            $codigo_banco = filter_input(INPUT_GET, 'banco', FILTER_SANITIZE_NUMBER_INT);
            $filtros = [
                'inicio' => $inicio,
                'fim' => $fim,
                'tr' => $tr,
                'tipo_nota' => $tipo_nota,
                'fornecedor' => $fornecedor,
                'tipo_documento_financeiro' => $tipo_documento_financeiro,
                'empresa_user' => $_SESSION['empresa_user'],
                'not_in_tipo_documento_financeiro' => 25,
                'not_in_forma_pagamento' => '7,9',
                'tipo_bordero' => $tipo_bordero,
                'codigo_banco' => $codigo_banco
            ];

            $response 	= Notas::getParcelasPendentesBorderoByFiltros($filtros);
            
            
            if($codigo_banco == 341){
                $origemFundoItau = 136;              
                if(in_array($tipo_bordero, [14, 1, 2, 11])){
                    $response = self::validarBancoFornecedor($tipo_bordero, $origemFundoItau, $response);
                }elseif(in_array($tipo_bordero, [12, 5])){                   
                    $response = self::validarCodigoBarrasItau($origemFundoItau, $response, $tipo_bordero);
                }elseif($tipo_bordero == 13){
                    $response = self::existePixFornecedor($response);
                }


            }
            
        }
        echo json_encode($response);
    }
    
    public static function pesquisarBorderos()
    {
        if(isset($_POST) && !empty($_POST)){
            $codigo_bordero  = filter_input(INPUT_POST, 'codigo_bordero', FILTER_SANITIZE_NUMBER_INT);
            $inicio  = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim     = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $contas  = isset($_POST['contas']) ? $_POST['contas'] : [];
            $bancos  = isset($_POST['bancos']) ? $_POST['bancos'] : [];
            $filtros = [
                'codigo_bordero' => $codigo_bordero,
                'inicio' => $inicio,
                'fim' => $fim,
                'contas' => $contas,
                'bancos' => $bancos,
                'empresa_user' => $_SESSION['empresa_user']
            ];      
                
            $response 	= BorderoModel::getBorderosByFilter($filtros);
        }
        $bancos = Banco::getAll();
        $contas = ContaBancaria::getContasBancariasByURs($_SESSION['empresa_user']);
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/cnab/templates/index-bordero.phtml');

    }

    public static function salvarBordero()
    {
        if(isset($_POST) && !empty($_POST)){
            $data = [];
            $data['tipo_bordero'] = filter_input(INPUT_POST, 'tipo_bordero', FILTER_SANITIZE_NUMBER_INT);
            $data['conta_id'] = filter_input(INPUT_POST, 'conta', FILTER_SANITIZE_STRING);
            $data['descricao'] = filter_input(INPUT_POST, 'descricao', FILTER_SANITIZE_STRING);
            $data['vencimentos'] = $_POST['vencimentos'];
            $data['valores_parcelas'] = $_POST['valores_parcelas'];
            $data['juros'] = $_POST['juros'];
            $data['descontos'] = $_POST['descontos'];
            $data['tarifas_adicionais'] = $_POST['tarifas_adicionais'];
            $data['parcelas_id'] = $_POST['parcelas_id'];
            $data['parcelas_valor'] = $_POST['parcelas_valor'];
            $response = BorderoModel::criarBordero($data);
            echo json_encode($response);
        }
    }

    public static function atualizarBordero()
    {
        if(isset($_POST) && !empty($_POST)){
            $data = [];
            $data['tipo_bordero'] = filter_input(INPUT_POST, 'tipo_bordero', FILTER_SANITIZE_NUMBER_INT);
            $data['bordero_id'] = filter_input(INPUT_POST, 'bordero_id', FILTER_SANITIZE_NUMBER_INT);
            $data['conta_id'] = filter_input(INPUT_POST, 'conta', FILTER_SANITIZE_STRING);
            $data['descricao'] = filter_input(INPUT_POST, 'descricao', FILTER_SANITIZE_STRING);
            $data['vencimentos'] = $_POST['vencimentos'];
            $data['valores_parcelas'] = $_POST['valores_parcelas'];
            $data['juros'] = $_POST['juros'];
            $data['descontos'] = $_POST['descontos'];
            $data['tarifas_adicionais'] = $_POST['tarifas_adicionais'];
            $data['parcelas_id'] = $_POST['parcelas_id'];
            $data['parcelas_valor'] = $_POST['parcelas_valor'];
            $response = BorderoModel::atualizarBordero($data);
            if($response != false) {
                echo $response;
            } else {
                echo '0';
            }
        }
    }

    public static function visualizarParcelasBordero()
    {
        if(isset($_GET) && !empty($_GET)){
            $borderoId  = filter_input(INPUT_GET, 'bordero_id', FILTER_SANITIZE_NUMBER_INT);

            echo json_encode(Parcela::getParcelasBordero($borderoId));
        }
    }

    public static function desfazerBordero()
    {
        if(isset($_GET) && !empty($_GET)){
            $borderoId  = filter_input(INPUT_GET, 'bordero_id', FILTER_SANITIZE_NUMBER_INT);

            echo BorderoModel::desfazerBordero($borderoId);
        }
    }

    public static function gerarCnab()
    {
        ini_set('display_errors', 1);
        if(isset($_GET) && !empty($_GET)){
            $bordero = filter_input(INPUT_GET, 'bordero', FILTER_SANITIZE_NUMBER_INT);

            $response = BorderoModel::getBorderoCnab($bordero);
            $banco_id = $response['dados']['banco_id'];

            $options = [
                'json' => json_encode($response),
                'verify' => false
            ];
            
            //echo '<pre>' . $banco_id . ' - '; die(print_r(json_encode($response)));
            
           // $client = new Client(["base_url" => "http://www_cnab"]);          
           $client = new Client(["base_url" => "http://ec2-54-232-233-212.sa-east-1.compute.amazonaws.com"]);
            $response = $client->get("/api/cnab/remessa", $options);

            $content = $response->getBody()->getContents();
            $result = json_decode($content);
           //echo '<pre>'; echo $content; die(print_r($result));

            if(count($result->erros) > 0) {
              //  if(0 > 0) {
                $erros = utf8_decode(implode(' ', $result->erros));
               
                echo <<<HTML
<script type="text/javascript">
alert("Houveram alguns erros ao compor o arquivo do CNAB! \\n Erros: \\n {$erros}");
window.close();
</script>
HTML;
                return false;
            }

            $tstamp = (new \DateTime())->format('U');
            $fileName = sprintf(
                "santa%s.rem",
                str_pad($bordero, 6, '0', STR_PAD_LEFT)
            );
            if($banco_id == '237') {
                $fileName = sprintf(
                    "%s.rem",
                    str_pad($bordero, 6, '0', STR_PAD_LEFT)
                );
            }elseif($banco_id == '341'){
                $fileName = sprintf(
                    "it%s.rem",
                    str_pad($bordero, 6, '0', STR_PAD_LEFT)
                );
            }
            $path = $_SERVER['DOCUMENT_ROOT'] . '/storage/cnab/' . $fileName;
            $fh = fopen($path, "w");
            fwrite($fh, $result->txt);
            fclose($fh);
            ob_start();
            header('Content-Type: application/text-plain');
            header('Content-Description: File Transfer');
            header('Content-Disposition: attachment; filename=' . $fileName);
            header('Content-Transfer-Encoding: binary');
            header('Connection: Keep-Alive');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: cache');
            ob_clean();
            flush();
            readfile($path);
            unlink($path);
        }
    }

    public static function importarRetornoCnab()
    {
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/cnab/templates/importar-retorno-cnab.phtml');
    }

    public static function processarArquivoRetornoCnab()
    {
        if(isset($_GET) && !empty($_GET)){
            $files  = $_FILES['arquivo_retorno'];
            $file = fopen($files['tmp_name'], "r");
            $response = [];
            while (!feof($file)) {
                $response[] = fgets($file);
            }
            fclose($file);
            $response = array_filter($response);

            $options = [
                'json' => json_encode($response),
                'verify' => false
            ];

              $client = new Client(["base_url" => "http://ec2-54-232-233-212.sa-east-1.compute.amazonaws.com"]);
           // $client = new Client(["base_url" => "http://www_cnab"]);
            $response = $client->post("/api/cnab/retorno", $options);

            $result = $response->getBody()->getContents();
              // echo '<pre>';  die(print_r($result));
            
            $result = json_decode($result, true);
            $response = Parcela::getParcelasRetorno($result);
            $fechamento_caixa = \App\Controllers\FechamentoCaixa::getFechamentoCaixa();

            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/cnab/templates/importar-retorno-cnab.phtml');
        }
    }

    public static function baixarParcelasRetorno()
    {
        if(isset($_POST) && !empty($_POST['parcelas'])) {
            $parcelas = $_POST['parcelas'];
            $banco = filter_input(INPUT_POST, 'banco', FILTER_SANITIZE_STRING);
            $agencia = filter_input(INPUT_POST, 'agencia', FILTER_SANITIZE_STRING);
            $conta = filter_input(INPUT_POST, 'conta', FILTER_SANITIZE_STRING);
            $response = Parcela::baixarParcelasRetorno($parcelas, $banco, $agencia, $conta);

            $msg = "Parcelas baixadas com sucesso!";
            $type = 's';
            if(!$response) {
                
               $msg = "Houve um problema ao baixar as parcelas selecionadas! Entre em contato com o TI";
                $type = 'f';
            }
            header ("Location: /financeiros/cnab/?action=importar-retorno-cnab&msg=" . base64_encode ($msg) . "&type={$type}");
            return;
        }
        $msg = "Nenhuma parcela foi selecionada!";
        $type = 'f';
        header ("Location: /financeiros/cnab/?action=importar-retorno-cnab&msg=" . base64_encode ($msg) . "&type={$type}");
        return;
    }

    public static function estornarParcela()
    {
        if(isset($_POST) && !empty($_POST['parcela'])) {
            $parcela  = filter_input(INPUT_POST, 'parcela', FILTER_SANITIZE_NUMBER_INT);
            echo Parcela::estornarParcela($parcela);
        }
    }

    public static function imprimirParcelasBordero()
    {
        if(isset($_GET) && !empty($_GET)) {
            $borderoId = filter_input(INPUT_GET, 'bordero', FILTER_SANITIZE_NUMBER_INT);

            $parcelas = Parcela::getParcelasBordero($borderoId);

            $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__).'/../');
            $auxDocRoot = explode('app', $_SERVER['DOCUMENT_ROOT']);
            $_SERVER['DOCUMENT_ROOT'] = $auxDocRoot[0];

            $html = <<<HTML
<table width='100%' border="1" cellpadding="1" cellspacing="0" style="font-size: 10px; border: solid #000 1px" >
    <tr>
        <td colspan="2"><img src="../../utils/cabec-sismederi3.png" width="200"></td>
        <td colspan="7" align="center">
            <b>BORDERO Nº {$borderoId}</b>
        </td>
    </tr>
    <tr>
        <td align="center">N° Documento</td>
        <td align="center">Parcela N°</td>
        <td align="center">UR</td>
        <td align="center">Fornecedor</td>
        <td align="center">Data da Emissão</td>
        <td align="center">Vencimento</td>
        <td align="center">Valor</td>
        <td align="center">Código de Barras/Conta/Pix</td>
        <td align="center">Tipo</td>
    </tr>
HTML;
            foreach ($parcelas as $parcela) {
                $ur = strtoupper($parcela['ur']);
                $dataEmissao = \DateTime::createFromFormat('Y-m-d', $parcela['dataEntrada'])->format('d/m/Y');
                $dataVencimento = \DateTime::createFromFormat('Y-m-d', $parcela['data_vencimento'])->format('d/m/Y');
                $valorPagar = number_format($parcela['valor_pagar'], 2, ',', '.');
                $contaCodigoPIX = "";
                if($parcela['forma_pagamento'] == 'TED') {
                    $contaCodigoPIX = $parcela['conta_fornecedor'];
                } elseif ($parcela['forma_pagamento'] == 'BOLETO' || $parcela['forma_pagamento'] == 'CONVENIO') {
                    $contaCodigoPIX = $parcela['codBarras'];
                } elseif ($parcela['forma_pagamento'] == 'PIX' ) {
                    $contaCodigoPIX = $parcela['chave_pix'];
            }
                $html .= <<<HTML
<tr>
   <td align="center">{$parcela['codigo']}</td>
   <td align="center">{$parcela['num_parcela']}</td>
   <td>{$ur}</td>
   <td>{$parcela['nome_fornecedor']}</td>
   <td align="center">{$dataEmissao}</td>
   <td align="center">{$dataVencimento}</td>
   <td>R$ {$valorPagar}</td>
   <td>{$contaCodigoPIX}</td>
   <td align="center">{$parcela['sigla']}</td>
</tr>
HTML;
            }

            $html .= <<<HTML
</table>
HTML;

            $filename = sprintf(
                "bordero_%s.pdf",
                $borderoId
            );

            $options = new Options();
            $options->set('defaultFont', 'Courier');
            $options->set('isRemoteEnabled', true);
            $dompdf = new Dompdf($options);
            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'landscape');
            $dompdf->render();
            $dompdf->stream($filename, ["Attachment" => false]);
            exit(0);
        }
    }

    public static function validarBancoFornecedor($tipo_bordero, $origemFundoItau, $response)
    {         
        $condicao = $tipo_bordero == 14 ? $origemFundoItau : null;
         $arrayAux = [];
        foreach($response as $resp){
            if($resp['banco_fornecedor'] == $condicao){
                $arrayAux[] = $resp;
            }
        }

        return $arrayAux;


    }

    public static function existePixFornecedor($response)
    {         
       
         $arrayAux = [];
        foreach($response as $resp){
            if(!empty($resp['chave_pix'])){
                $arrayAux[] = $resp;
            }
        }

        return $arrayAux;


    }

    public static function validarCodigoBarrasItau($origemFundoItau, $response, $tipoBordero)
    {
        $arrayAux = [];
        $condigoBancoItau = 341;
        
        foreach($response as $resp){
            $codigoBarras = $resp['codBarras'];
            $codBanco = substr($codigoBarras,0,3); 
            if((strlen($codigoBarras) == 44 || strlen($codigoBarras) == 47) && $tipoBordero == 12 && $codBanco == $condigoBancoItau) {
                
                    $arrayAux[] = $resp;               
               
            }elseif($tipoBordero == 5 && $codBanco != $condigoBancoItau){
                $arrayAux[] = $resp;
            }

        }
        return $arrayAux;
        
    }

    public static function importarVarreduraCnab()
    {
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/cnab/templates/importar-varredura-cnab.phtml');
    }

    public static function processarArquivoVarreduraCnab()
    {        
        if(isset($_GET) && !empty($_GET)){
            $files  = $_FILES['arquivo_varredura'];
            $file = fopen($files['tmp_name'], "r");
            $response = [];
            while (!feof($file)) {
                $response[] = fgets($file);
            }
            fclose($file);
            $response = array_filter($response);

            $options = [
                'json' => json_encode($response),
                'verify' => false
            ];

            $client = new Client(["base_url" => "http://ec2-54-232-233-212.sa-east-1.compute.amazonaws.com"]);
            //$client = new Client(["base_url" => "http://www_cnab"]);
            $response = $client->post("/api/cnab/varredura", $options);

            $result = $response->getBody()->getContents();
            
            
            $result = json_decode($result, true);
            
            $resultVarredura = self::formatArrayVaredura($result['response']);
            
           
            $responseParcelas = Parcela::getParcelasVarredura($resultVarredura['inicio'], $resultVarredura['fim']);
            $response = self::formatParcelasVaredura($responseParcelas, $resultVarredura['contas']);
           
            $fechamento_caixa = \App\Controllers\FechamentoCaixa::getFechamentoCaixa();

            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/cnab/templates/importar-varredura-cnab.phtml');
        }
    }

    public static function formatArrayVaredura($result)
    {
        
        $inicio = '';
        $fim = '';
        $itens = '';
        foreach($result as $r){
            
            $dataVencimento = \DateTime::createFromFormat('dmY', $r['vencimento']);

            if(empty($inicio) && empty($fim)){
                $inicio = $dataVencimento;
                $fim = $dataVencimento;
            }else{
                $inicio = $inicio <= $dataVencimento ? $inicio : $dataVencimento;
                $fim = $fim >= $dataVencimento ? $fim : $dataVencimento;
            }

            if($r['tipoInscricaoFornecedor'] == 2){
                $tipo = 'CNPJ';
                $inscricao = StringHelper::maskCNPJ(substr($r['inscricaoFornecedor'],-14));

            }else{
                $tipo = 'CPF';
                $inscricao = StringHelper::maskCPF(substr($r['inscricaoFornecedor'],-11));
            }
                
            

            $itens['contas'][$r['inscricaoFornecedor']][$dataVencimento->format('Y-m-d')][$r['valorCodBarras']][]  = [
                'tipo' => $tipo,
                'fornecedor' => $r['nomeFornecedor'],
                'codBarras' => $r['codBarras'],
                'cpfCnpj' => $r['inscricaoFornecedor'],
                'vencimento' => $dataVencimento,
                'valor' => $r['valorCodBarras'],
                'valorFormatado' =>substr_replace( ltrim($r['valorCodBarras'],0), '.', -2, 0),
                'cpfCnpjFormatado' => $inscricao
            ];
           
            $itens['inicio'] = $inicio->format('Y-m-d');
            $itens['fim'] = $fim->format('Y-m-d'); 


        }
      

        return $itens;
    }

    public static function formatParcelasVaredura($resultParcelas, $resultVarredura)
    {
        $parcela = [];
        
       
        
        foreach($resultParcelas as $p){            
          $incricaoFornecedor =  StringHelper::stringPad(preg_replace('/[^0-9]/', '', $p['cpfCNPJ']), 15, 'left','0');
          $valor = StringHelper::stringPad(preg_replace('/[^0-9]/', '', $p['valor']), 10,  'left', '0');
          $parcela = [
            'fornecedor' => $p['nomeFornecedor'],
            'cpfCnpj' => $p['cpfCNPJ'],
            'vencimento' => $p['vencimento'],
            'valor' => $p['valor'],
            'tr' => $p['idNota'],
            'status' => $p['status'],
            'codBarras' => $p['codBarras'],
            'idParcela' => $p['id'],
            'sequencia' => $p['TR']

          ];
          if(isset($resultVarredura[$incricaoFornecedor][$p['vencimento']][$valor])){
                $resultVarredura[$incricaoFornecedor][$p['vencimento']][$valor]['parcela'][] = $parcela;           
          }
        }        

        return $resultVarredura;

    }
}
