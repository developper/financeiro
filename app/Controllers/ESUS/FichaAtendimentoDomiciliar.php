<?php
namespace App\Controllers\ESUS;

use App\Models\Administracao\ESUS\CIAP;
use App\Models\Administracao\ESUS\CondicoesAvaliadas;
use App\Models\Administracao\ESUS\LocalAtendimento;
use App\Models\Administracao\ESUS\ProcedimentosAtendimentoDomiciliar;
use App\Models\Administracao\ESUS\Sexo;
use App\Models\Administracao\ESUS\TipoAtendimento;
use App\Models\Administracao\ESUS\Turno;
use App\Models\Administracao\Paciente;
use App\Models\Administracao\PacienteIW;
use App\Models\Administracao\Usuario;
use App\Models\DB\ConnESUSPgsql;
use App\Models\DB\ConnMysqli;
use App\Models\Administracao\ESUS\FichaAtendimentoDomiciliar AS FichaAtendimentoDomiciliarModel;

class FichaAtendimentoDomiciliar
{
    private static function getParameters()
    {
        return [
            'turno' => [
                '1' => 'Manhã',
                '2' => 'Tarde',
                '3' => 'Noite',
            ]
        ];
    }

    public static function fichaAtendimentoDomiciliarBuscarForm()
    {
        $data = [];
        $filial = Usuario::getFilialUsuario();
        if($filial == '1') {
            $pacientes = Paciente::getPacientesByOperadora(56);
        } else {
            $pacientes = PacienteIW::getAll($filial);
        }
        $data['inicio'] = (new \DateTime())->modify('first day of this month')->format('Y-m-d');
        $data['fim'] = (new \DateTime())->modify('last day of this month')->format('Y-m-d');

        require ($_SERVER['DOCUMENT_ROOT'] . '/adm/esus/templates/ficha-atendimento-domiciliar/listar-form.phtml');
    }

    public static function fichaAtendimentoDomiciliarBuscar()
    {
        if (isset($_POST) && !empty($_POST)) {
            $data = [];
            $data['paciente'] = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $data['inicio'] = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $data['filial'] = Usuario::getFilialUsuario();

            $ficha = new FichaAtendimentoDomiciliarModel(ConnMysqli::getConnection());
            $response = $ficha->getFiltered($data);
            $turnos = self::getParameters()['turno'];
            if($data['filial'] == '1') {
                $pacientes = Paciente::getPacientesByOperadora(56);
            } else {
                $pacientes = PacienteIW::getAll($data['filial']);
            }
        }

        require ($_SERVER['DOCUMENT_ROOT'] . '/adm/esus/templates/ficha-atendimento-domiciliar/listar-form.phtml');
    }

    public static function listarLoteXML()
    {
        if (isset($_GET) && !empty($_GET)) {
            $data = [];
            $data['paciente'] = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $data['inicio'] = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $data['filial'] = Usuario::getFilialUsuario();

            $ficha = new FichaAtendimentoDomiciliarModel(ConnMysqli::getConnection());
            $response = $ficha->getFiltered($data);

            if(count($response) > 0) {
                $response = json_encode($response);
                echo $response;
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static function fichaAtendimentoDomiciliarForm()
    {
        $response = [];
        $filial = Usuario::getFilialUsuario();
        $connEsus = new ConnESUSPgsql($filial);
        $response['paciente'] = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_NUMBER_INT);
        if(empty($response['paciente'])) {
            header ("Location: /adm/esus/?action=buscar-ficha-atendimento-domiciliar");
            exit();
        }
        if($filial == '1') {
            $paciente = Paciente::getById($response['paciente']);
        } else {
            $paciente = PacienteIW::getById($response['paciente']);
        }
        $hoje = new \DateTime();
        $nascimento = \DateTime::createFromFormat('Y-m-d', $paciente['nascimento']);
        $diff = $nascimento->diff($hoje)->days;
        $idade = floor($diff / 365.25);
        $generos = Sexo::getAll();
        $turnos = Turno::getAll();
        $condicoesAvaliadas = CondicoesAvaliadas::getAll();
        $locaisAtendimento = LocalAtendimento::getAll();
        $tiposAtendimento = TipoAtendimento::getFiltered();
        $procedimentos = ProcedimentosAtendimentoDomiciliar::getAll();
        $ciap = new CIAP($connEsus);
        $ciaps = $ciap->getAll();

        $response['data_visita'] = (new \DateTime())->format('Y-m-d');

        require ($_SERVER['DOCUMENT_ROOT'] . '/adm/esus/templates/ficha-atendimento-domiciliar/form.phtml');
    }

    public static function salvarFichaAtendimentoDomiciliar()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = [];

            $data['paciente_id'] = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $data['paciente_cns'] = filter_input(INPUT_POST, 'paciente_cns', FILTER_SANITIZE_STRING);
            $data['data_nascimento'] = filter_input(INPUT_POST, 'data_nascimento', FILTER_SANITIZE_NUMBER_INT);
            $data['sexo'] = filter_input(INPUT_POST, 'sexo', FILTER_SANITIZE_NUMBER_INT);
            $data['profissional_nome'] = filter_input(INPUT_POST, 'profissional_nome', FILTER_SANITIZE_STRING);
            $data['profissional_cpf'] = filter_input(INPUT_POST, 'profissional_cpf', FILTER_SANITIZE_STRING);
            $data['profissional_cns'] = filter_input(INPUT_POST, 'profissional_cns', FILTER_SANITIZE_STRING);
            $data['profissional_cbo'] = filter_input(INPUT_POST, 'profissional_cbo', FILTER_SANITIZE_STRING);
            $data['profissional_ator_id'] = filter_input(INPUT_POST, 'profissional_ator_id', FILTER_SANITIZE_STRING);
            $data['profissional_ine'] = filter_input(INPUT_POST, 'profissional_ine', FILTER_SANITIZE_STRING);
            $data['profissional_cnes'] = filter_input(INPUT_POST, 'profissional_cnes', FILTER_SANITIZE_STRING);
            $data['data_visita'] = $_POST['data_visita'];
            $data['turno'] = $_POST['turno'];
            $data['local_atendimento'] = filter_input(INPUT_POST, 'local_atendimento', FILTER_SANITIZE_STRING);
            $data['modalidade'] = filter_input(INPUT_POST, 'modalidade', FILTER_SANITIZE_STRING);
            $data['tipo_atendimento'] = filter_input(INPUT_POST, 'tipo_atendimento', FILTER_SANITIZE_STRING);
            $data['conduta_desfecho'] = filter_input(INPUT_POST, 'conduta_desfecho', FILTER_SANITIZE_STRING);

            $data['condicoes_avaliadas'] = isset($_POST['condicoes_avaliadas']) ? $_POST['condicoes_avaliadas'] : [];
            $data['cid10'] = isset($_POST['cid10']) ? $_POST['cid10'] : [];
            $data['ciap2'] = isset($_POST['ciap2']) ? $_POST['ciap2'] : [];
            $data['procedimentos'] = isset($_POST['procedimentos']) ? $_POST['procedimentos'] : [];
            $data['sigtap'] = isset($_POST['sigtap']) ? $_POST['sigtap'] : [];

            $ficha = new FichaAtendimentoDomiciliarModel(ConnMysqli::getConnection());
            $response = $ficha->save($data);

            $msg = "Ficha atendimento criada com sucesso!";
            $type = 's';
            if(!$response) {
                $msg = "Houve um problema ao atualizar os dados dessa ficha!";
                $type = 'f';
            }
            header ("Location: /adm/esus/?action=ficha-atendimento-domiciliar&paciente={$data['paciente_id']}&msg=" . base64_encode ($msg) . "&type={$type}");
        }
    }

    public static function fichaAtendimentoDomiciliarFormEditar()
    {
        $response = [];
        $filial = Usuario::getFilialUsuario();
        $fichaId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
        $modo = filter_input(INPUT_GET, 'mode', FILTER_SANITIZE_STRING);
        $subtitle = $modo == 'e' ? '(EDITAR)' : '';
        $disabled = $modo == 'v' ? 'disabled' : '';
        if(empty($fichaId) || empty($modo)) {
            header ("Location: /adm/esus/?action=buscar-ficha-atendimento-domiciliar");
            exit();
        }
        $connEsus = new ConnESUSPgsql($filial);
        $ficha = new FichaAtendimentoDomiciliarModel(ConnMysqli::getConnection());
        $response = $ficha->getDataForUpdate($fichaId);
        if($filial == '1') {
            $paciente = Paciente::getById($response['paciente_id']);
        } else {
            $paciente = PacienteIW::getById($response['paciente_id']);
        }
        $hoje = new \DateTime();
        $nascimento = \DateTime::createFromFormat('Y-m-d', $response['data_nascimento']);
        $diff = $nascimento->diff($hoje)->days;
        $idade = floor($diff / 365.25);
        $generos = Sexo::getAll();
        $turnos = Turno::getAll();
        $condicoesAvaliadas = CondicoesAvaliadas::getAll();
        $locaisAtendimento = LocalAtendimento::getAll();
        $tiposAtendimento = TipoAtendimento::getFiltered();
        $procedimentos = ProcedimentosAtendimentoDomiciliar::getAll();
        $ciap = new CIAP($connEsus);
        $ciaps = $ciap->getAll();

        require ($_SERVER['DOCUMENT_ROOT'] . '/adm/esus/templates/ficha-atendimento-domiciliar/form-editar.phtml');
    }

    public static function atualizarFichaAtendimentoDomiciliar()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = [];

            $data['ficha_id'] = filter_input(INPUT_POST, 'ficha_id', FILTER_SANITIZE_STRING);
            $data['modo'] = filter_input(INPUT_POST, 'mode', FILTER_SANITIZE_STRING);
            $data['paciente_id'] = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $data['paciente_cns'] = filter_input(INPUT_POST, 'paciente_cns', FILTER_SANITIZE_STRING);
            $data['data_nascimento'] = filter_input(INPUT_POST, 'data_nascimento', FILTER_SANITIZE_NUMBER_INT);
            $data['sexo'] = filter_input(INPUT_POST, 'sexo', FILTER_SANITIZE_NUMBER_INT);
            $data['profissional_nome'] = filter_input(INPUT_POST, 'profissional_nome', FILTER_SANITIZE_STRING);
            $data['profissional_cpf'] = filter_input(INPUT_POST, 'profissional_cpf', FILTER_SANITIZE_STRING);
            $data['profissional_cns'] = filter_input(INPUT_POST, 'profissional_cns', FILTER_SANITIZE_STRING);
            $data['profissional_cbo'] = filter_input(INPUT_POST, 'profissional_cbo', FILTER_SANITIZE_STRING);
            $data['profissional_ator_id'] = filter_input(INPUT_POST, 'profissional_ator_id', FILTER_SANITIZE_STRING);
            $data['profissional_ine'] = filter_input(INPUT_POST, 'profissional_ine', FILTER_SANITIZE_STRING);
            $data['profissional_cnes'] = filter_input(INPUT_POST, 'profissional_cnes', FILTER_SANITIZE_STRING);
            $data['data_visita'] = $_POST['data_visita'];
            $data['turno'] = $_POST['turno'];
            $data['local_atendimento'] = filter_input(INPUT_POST, 'local_atendimento', FILTER_SANITIZE_STRING);
            $data['modalidade'] = filter_input(INPUT_POST, 'modalidade', FILTER_SANITIZE_STRING);
            $data['tipo_atendimento'] = filter_input(INPUT_POST, 'tipo_atendimento', FILTER_SANITIZE_STRING);
            $data['conduta_desfecho'] = filter_input(INPUT_POST, 'conduta_desfecho', FILTER_SANITIZE_STRING);

            $data['condicoes_avaliadas'] = isset($_POST['condicoes_avaliadas']) ? $_POST['condicoes_avaliadas'] : [];
            $data['cid10'] = isset($_POST['cid10']) ? $_POST['cid10'] : [];
            $data['ciap2'] = isset($_POST['ciap2']) ? $_POST['ciap2'] : [];
            $data['procedimentos'] = isset($_POST['procedimentos']) ? $_POST['procedimentos'] : [];
            $data['sigtap'] = isset($_POST['sigtap']) ? $_POST['sigtap'] : [];
            $data['filial'] = Usuario::getFilialUsuario();

            $ficha = new FichaAtendimentoDomiciliarModel(ConnMysqli::getConnection());
            $response = $ficha->update($data);

            if(!$response) {
                $msg = "Houve um problema ao atualizar os dados dessa ficha!";
                $type = 'f';
                header ("Location: /adm/esus/?action=ficha-atendimento-domiciliar-editar&id={$data['ficha_id']}&mode={$data['modo']}&msg=" . base64_encode ($msg) . "&type={$type}");
            } else {
                $msg = "Ficha atendimento atualizada com sucesso!";
                $type = 's';
                header ("Location: /adm/esus/?action=ficha-atendimento-domiciliar-editar&id={$response}&mode={$data['modo']}&msg=" . base64_encode ($msg) . "&type={$type}");
            }
        }
    }

    public static function fichaAtendimentoDomiciliarExcluir()
    {
        if (isset($_GET) && !empty($_GET)) {
            $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

            $ficha = new FichaAtendimentoDomiciliarModel(ConnMysqli::getConnection());
            echo $ficha->delete($id);
        }
    }

    public static function gerarLoteXML()
    {
        if (isset($_POST) && !empty($_POST)) {
            $data = [];
            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $data['fichas_id'] = $_POST['fichas_atendimento'];
            $data['filial'] = Usuario::getFilialUsuario();

            $ficha = new FichaAtendimentoDomiciliarModel(ConnMysqli::getConnection());
            $response = $ficha->generateXML($data);

            $inicio = \DateTime::createFromFormat('Y-m-d', $inicio)->format('dmY');
            $fim = \DateTime::createFromFormat('Y-m-d', $fim)->format('dmY');
            $tstamp = (new \DateTime())->format('U');

            $zipName = sprintf("Lote_ESUS_%s_a_%s_%s.zip", $inicio, $fim, $tstamp);
            $zip = new \ZipArchive();
            $zipPath = '../../storage/xml-esus/' . $zipName;
            if( $zip->open($zipPath , \ZipArchive::CREATE) === true){
                foreach ($response as $xml) {
                    $zip->addFile($xml['filepath'], $xml['filename']);
                }
                $zip->close();
                foreach ($response as $xml) {
                    unlink($xml['filepath']);
                }
            }
            $path = $zipPath;
            $mimeType = 'zip';

            ob_start();
            header('Content-Type: application/' . $mimeType);
            header('Content-Description: File Transfer');
            header('Content-Disposition: attachment; filename=' . $zipName);
            header('Content-Transfer-Encoding: binary');
            header('Connection: Keep-Alive');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: cache');
            ob_clean();
            flush();
            readfile($path);
            unlink($zipPath);
        }
    }
}
