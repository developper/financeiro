<?php
namespace App\Controllers\ESUS;

use App\Models\Administracao\ESUS\PessoaFisica as PessoaFisicaModel;
use App\Models\Administracao\Usuario;
use App\Models\DB\ConnESUSPgsql;

class PessoaFisica
{
    public static function buscarPessoaFisicaJson()
    {
        $filial = Usuario::getFilialUsuario();
        $term = filter_input(INPUT_GET, 'term', FILTER_SANITIZE_STRING);
        $connEsus = new ConnESUSPgsql($filial);
        $pessoaFisica = new PessoaFisicaModel($connEsus);
        $json = $pessoaFisica->getAllJson($term);
        echo json_encode($json);
    }
}
