<?php
namespace App\Controllers\ESUS;

use App\Models\Administracao\ESUS\SIGTAP as SIGTAPModel;

class SIGTAP
{
    public static function buscarSIGTAPJson()
    {
        $term = filter_input(INPUT_GET, 'term', FILTER_SANITIZE_STRING);
        $sigtaps = SIGTAPModel::getAllJson($term);
        echo json_encode($sigtaps);
    }
}
