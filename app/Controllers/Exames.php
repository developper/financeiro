<?php

namespace App\Controllers;

use \App\Core\Config;
use App\Models\Administracao\Cidade;
use App\Models\Administracao\Paciente;
use App\Models\Administracao\Planos;
use App\Models\DB\ConnMysqli;
use App\Models\Exames\ExamesLaboratoriais;
use App\Models\Faturamento\Cid10;
use App\Models\Faturamento\Fatura;
use \App\Services\Gateways\SendGridGateway;
use App\Services\MailAdapter;
use App\Helpers\Storage;

class Exames
{
    public static function menu()
    {
        require($_SERVER['DOCUMENT_ROOT'] . '/exames/templates/menu.phtml');
    }

    public static function formExames()
    {
        ini_set('post_max_size', '16M');
        $exames = ExamesLaboratoriais::getAll();
        $listaCid10 = Cid10::getAll();
        require($_SERVER['DOCUMENT_ROOT'] . '/exames/templates/exames-form.phtml');
    }

    public static function salvarArquivo()
    {
        session_start();
        $files = $_FILES;

        $tipo = explode('.',$files['file']['name']);

         if(isset($_POST['novo-nome']) && !empty($_POST['novo-nome'])){
            $files['file']['name'] = $_POST['novo-nome'].'.'.$tipo[1];
        }

        $idUser = $_SESSION['id_user'];

        $idExame = $_POST['id-exame'];
        $exames = new ExamesLaboratoriais(ConnMysqli::getConnection());
        $response = $exames->salvarArquivo($files,$idExame,$idUser);

        echo $response;
    }

    public static function salvarExames()
    {
        $data = ['formData' => $_POST, 'files' => $_FILES['files']];

        $exames = new ExamesLaboratoriais(ConnMysqli::getConnection());
        $type = 'f';
        $msg = '';
        try {

            $response = $exames->save($data);
            if(is_bool($response)) {
                 $data = $_POST;

                $paciente = Paciente::getById($data['paciente']);
                $mensagem = self::mensagemSolicitacaoExame($paciente,$data);

                $caraterExame = $data['tipo'] == 'el' ? 'eletivo': 'emergencial';
                $carateres = array(
                    'eletivo' => 'Solicitação de exame Eletivo',
                    'emergencial' => 'Solicitação de exame Emergencial'
                );
                self::enviarEmail($paciente['nome'],$caraterExame,$paciente['empresa'], $mensagem);

                $compMsg = $data['coleta'] == 'E' ? " Uma prescrição de coleta de material foi criada e enviada para a enfermagem." : "";
                $msg = base64_encode("Exame inserido com sucesso!" . $compMsg);
                $type = 's';
                header("Location: /exames/?action=exames-novo&msg={$msg}&type={$type}");
            } else {
                foreach ($response as $msgs) {
                    $msg .= $msgs . '<br>';
                }
                $msg = base64_encode($msg);
            }
        } catch (\Exception $e) {
            $msg = base64_encode($e->getMessage());
        }
        $data = $_POST;
        $pacientes = Paciente::getAll();
        $exames = ExamesLaboratoriais::getAll();
        require($_SERVER['DOCUMENT_ROOT'] . '/exames/templates/exames-form.phtml');
    }

    public static function formListarExames()
    {
        $pacientes = Paciente::getAll();
        $exames = ExamesLaboratoriais::getAll();
        $pacienteId = null;

        require($_SERVER['DOCUMENT_ROOT'] . '/exames/templates/exames-consultar.phtml');
    }

    public static function listarExames()
    {
        $pacientes = Paciente::getAll();
        $exames = ExamesLaboratoriais::getAll();
        $pacienteId = $_POST['paciente'];

        $examesPaciente = new ExamesLaboratoriais(ConnMysqli::getConnection());
        $response = $examesPaciente->examesListar($_POST);
        $count = count($response['exames']);
        $data = $_POST;

        require($_SERVER['DOCUMENT_ROOT'] . '/exames/templates/exames-consultar.phtml');
    }

    public static function excluirExames()
    {
        if(isset($_POST['id']) && !empty($_POST['id']) && is_int(intval($_POST['id']))) {

            $exames = new ExamesLaboratoriais(ConnMysqli::getConnection());
            $idExame = $_POST['id'];
            try {
                $response = $exames->excluirExames($idExame);
                 echo $response ;
                return ;
            } catch (\Exception $e) {

                $msg = base64_encode($e->getMessage());
            }

        }

        return false;
    }

    public static function excluirArquivo()
    {
        if(isset($_POST['id']) && !empty($_POST['id']) && is_int(intval($_POST['id']))) {

            $exames = new ExamesLaboratoriais(ConnMysqli::getConnection());
            $idArquivo = $_POST['id'];
            try {
                $response = $exames->excluirArquivo($idArquivo);
                echo $response ;
                return ;
            } catch (\Exception $e) {

                $msg = base64_encode($e->getMessage());
            }

        }

        return false;
    }

    public static function mensagemSolicitacaoExame($paciente,$data)
    {
        session_start();
        $caraterExame = $data['tipo'] == 'el' ? 'Eletivo': 'Emergencial';
        $justificativa = $data['tipo'] == 'el' ? '': '<b>Justificativa: </b>'.htmlentities($data['justificativa_emergencial']).'<br>';
        $exames ='';
       
        foreach($data['exames'] as $dt){
          if(!empty($dt)) {
              $array = ExamesLaboratoriais::getById($dt);
              $exames .= "<li>" . $array['descricao'] . "</li>";
          }
        }

        $cid = Cid10::getById($data['cid']);
        $convenio = Planos::getById($paciente['convenio']);
        $cidadePaciente = Cidade::getById($paciente['CIDADE_ID_DOM']);

        $msg =
        "
        <p>
            ".htmlentities('Solicitação')." de exame(s) para o paciente: <b>".htmlentities($paciente['nome'])."</b>,
            feita por: <b>".htmlentities(ucwords(strtolower($_SESSION["nome_user"])))."</b>
        </p>
        <p><b>Cid:</b> ({$cid['cid10']}) ".htmlentities($cid['UpperDescricao'])."</p>
        <p>
            <b>".htmlentities('Período:')."</b> {$data['inicio']} - {$data['fim']}
            <br>
            <b>".htmlentities('Caráter')." do Exame:</b> {$caraterExame}.
            <br>
            <b>".htmlentities('Observação:')."</b>  ".htmlentities($data['observacao'])."
            <br>
            $justificativa
        </p>
        <p>
            <b>".htmlentities('Convênio:')." {$convenio['nome']}</b>   ".htmlentities('Matrícula:')." {$paciente['NUM_MATRICULA_CONVENIO']}
            <br>
            <b>CPF: </b> {$paciente[cpf]} <b>Data de nascimento: </b>".join('/',array_reverse(explode('-',$paciente['nascimento'])))."
            <br>
            <b>".htmlentities('Endereço:')."</b> {$paciente['endereco']}, Bairro: {$paciente['endereco']},
            Cidade: {$cidadePaciente['NOME']}/{$cidadePaciente['UF']}
        </p>

        <p>
             <b>Exame(s) solicitado(s):</b>
        </p>
        <ul>
            {$exames}
        </ul>
        <p  style=' margin-left: auto;margin-right: auto;width: 20em;' >
                 ________________________________________________________________________

       </p>

       <p  style=' margin-left: auto;margin-right: auto;width: 1em;' >
                 Responsável/Cuidador
       </p>

       ";

        return $msg;
    }

    public static function enviarEmail($paciente, $tipo, $empresa, $msgHTML = '')
    {
        $api_data = (object) Config::get('sendgrid');

        $gateway = new MailAdapter(
            new SendGridGateway(
                new \SendGrid($api_data->user, $api_data->pass),
                new \SendGrid\Email()
            )
        );

        $gateway->adapter->setSendData($paciente, $tipo,'exame', $empresa, $msgHTML);

        $gateway->adapter->sendMail();
    }

    private static function enviarEmailSimples($titulo, $texto, $para)
    {
        $api_data = (object) Config::get('sendgrid');

        $gateway = new MailAdapter(
            new SendGridGateway(
                new \SendGrid($api_data->user, $api_data->pass),
                new \SendGrid\Email()
            )
        );

        $gateway->adapter->sendSimpleMail($titulo, $texto, $para);
    }

    public static function buscarCid10()
    {
        if(isset($_GET) && !empty($_GET)) {
            echo json_encode(Cid10::getByTerm($_GET['term']));
        }
    }
}