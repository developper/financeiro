<?php

namespace app\Controllers;

use App\Core\DateTime;
use \App\Models\Administracao\Filial;
use \App\Models\Administracao\Paciente;
use \App\Models\Medico\Evolucao;
use \App\Models\Medico\Prescricao;
use \App\Models\Administracao\Score as ScoreModel;

class Score
{
    public static function scoreNead2016(
        $paciente = null,
        $relatorio = null,
        $tipo_relatorio = null,
        $classificacao_paciente = null,
        $readonly = null
    ) {
        $grupo = '5';
        $totalKatz = 0;
        $totalGrupo3 = 0;
        $tempoCriado = 0;
        $classAlert = '';
        $criadoPor = '';
        $hasAvaliacao = false;
        if($paciente != null && $relatorio != null) {
            if($tipo_relatorio == 'capmedica' || $tipo_relatorio == 'avaliacaoenfermagem') {
                $dadosScore = ScoreModel::getTempoAndAutorScorePaciente($tipo_relatorio, $relatorio);
                if (count($dadosScore) >= '1') {
                    $hasAvaliacao = true;
                    $criadoPor = mb_strtoupper($dadosScore['criadoPor'], 'UTF8');
                    $agora = new \DateTime();
                    $tempoCriado = (new \DateTime($dadosScore['dataCriacao']))->diff($agora);
                    $classAlert = $tempoCriado->days <= 2 ? 'warning' : 'danger';
                    $tempoCriadoTxt = $tempoCriado->days . ' dias';
                    if($tempoCriado->days == 0) {
                        $tempoCriadoTxt = $tempoCriado->h < 1 ? $tempoCriado->i . ' minuto(s)' : $tempoCriado->h . " hora(s) e " . $tempoCriado->i . ' minuto(s)';
                        $classAlert = 'warning';
                    }
                }
            }
            $scoreNead = [];
            $ultimoScore = ScoreModel::getUltimoScorePaciente($paciente, $relatorio, $tipo_relatorio);
            foreach($ultimoScore as $item) {
                $scoreNead[$item['secao']][$item['opcao']] = $item['valor'];
                if($item['secao'] == 'grupos') {
                    $totalGrupo3 += $item['valor'];
                } else {
                    $totalKatz += $item['valor'];
                }
            }
        }

        require __DIR__ . '/../../components/scores/score-nead-2016/table-score.phtml';
        return true;
    }

    public static function salvarScorePaciente($data, $paciente, $tipo_relatorio, $relatorioId)
    {
        return ScoreModel::saveScorePaciente($data, $paciente, $tipo_relatorio, $relatorioId);
    }

    public static function excluirScorePaciente($tipo_relatorio, $relatorioId)
    {
        return ScoreModel::deleteScorePaciente($tipo_relatorio, $relatorioId);
    }

    public static function scoreNead2016Imprimir(
        $paciente,
        $relatorio,
        $tipo_relatorio,
        $classificacao_paciente,
        $viewMode = 'view'
    ) {
        $grupo = '5';
        $totalKatz = 0;
        $totalGrupo3 = 0;
        $scoreNead = [];

        $ultimoScore = ScoreModel::getUltimoScorePaciente($paciente, $relatorio, $tipo_relatorio);
        foreach($ultimoScore as $item) {
            $scoreNead[$item['secao']][$item['opcao']] = $item['valor'];
            if($item['secao'] == 'grupos') {
               // $totalGrupo3 += $item['valor'];
            } else {
                $totalKatz += $item['valor'];
            }
        }

        if($viewMode == 'view') {
            $html = require __DIR__ . '/../../components/scores/score-nead-2016/table-score-print.phtml';
        } else {
            $html = self::getScorePrintHtml($scoreNead, $totalKatz, $totalGrupo3, $classificacao_paciente);
        }
        return $html;
    }

    private static function getScorePrintHtml($scoreNead, $totalKatz, $totalGrupo3, $classificacao_paciente)
    {
        $grupo = '5';
        $html = '
        <hr>
<table class="mytable" width="100%">
    <thead>
    <tr>
        <th align="center" colspan="5">Tabela de Avaliação para Planejamento de Atenção Domiciliar - NEAD<sup>2016<sup></th>
    </tr>
    </thead>
    <tr bgcolor="#5f9ea0">
        <td colspan="5">ESCORE DE KATZ</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td colspan="2" align="center" bgcolor="grey">INDEPENDÊNCIA<br><small><strong>1 PONTO</strong></small></td>
        <td align="center" colspan="2" bgcolor="grey">DEPENDÊNCIA<br><small><strong>0 PONTO</strong></small></td>
    </tr>
    <tr>
        <td bgcolor="grey">ATIVIDADES</td>
        <td colspan="2" align="left">
                ' .
                (
                $scoreNead != "" &&
                isset($scoreNead["katz"]["atividades"]) &&
                $scoreNead["katz"]["atividades"] == "1" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
                )
                . '
            <label for="atividades-0">SEM SUPERVISÃO, ORIENTAÇÃO OU ASSISTÊNCIA PESSOAL.</label>
        </td>
        <td colspan="2" align="left">
                ' .
                (
                $scoreNead != "" &&
                isset($scoreNead["katz"]["atividades"]) &&
                $scoreNead["katz"]["atividades"] == "0" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
                )
                . '
            <label for="atividades-1">COM SUPERVISÃO, ORIENTAÇÃO OU ASSISTÊNCIA PESSOAL OU CUIDADO INTEGRAL.</label>
        </td>
    </tr>
    <tr>
        <td bgcolor="grey">BANHAR-SE</td>
        <td colspan="2" align="left">
                ' .
                (
                $scoreNead != "" &&
                isset($scoreNead["katz"]["banho"]) &&
                $scoreNead["katz"]["banho"] == "1" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
                )
                . '
                        <label for="banho-0">
                BANHA-SE COMPLETAMENTE OU NECESSITA DE AUXÍLIO SOMENTE PARA LAVAR UMA PARTE DO CORPO,
                COMO AS COSTAS, GENITAIS OU UMA EXTREMIDADE INCAPACITADA.
            </label>
        </td>
        <td align="left" colspan="2">
                ' .
                (
                $scoreNead != "" &&
                isset($scoreNead["katz"]["banho"]) &&
                $scoreNead["katz"]["banho"] == "0" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
                )
                . '
            <label for="banho-1">
                NECESSITA DE AJUDA PARA BANHAR-SE EM MAIS DE UMA PARTE DO CORPO, ENTRAR E SAIR DO CHUVEIRO OU BANHEIRA
                OU REQUER ASSISTÊNCIA TOTAL NO BANHO.
            </label>
        </td>
    </tr>
    <tr>
        <td bgcolor="grey">VESTIR-SE</td>
        <td colspan="2" align="left">
                ' .
                (
                $scoreNead != "" &&
                isset($scoreNead["katz"]["vestir"]) &&
                $scoreNead["katz"]["vestir"] == "1" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
                )
                . '
                        <label for="vestir-0">
                PEGA AS ROUPAS DO ARMÁRIO E VESTE AS ROUPAS ÍNTIMAS EXTERNAS E CINTOS. PODE RECEBER AJUDAR
                PARA AMARRAR OS SAPATOS.
            </label>
        </td>
        <td align="left" colspan="2">
                ' .
                (
                $scoreNead != "" &&
                isset($scoreNead["katz"]["vestir"]) &&
                $scoreNead["katz"]["vestir"] == "0" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
                )
                . '
            <label for="vestir-1">
                NECESSITA DE AJUDA PARA VESTIR-SE OU NECESSITA SER COMPLETAMENTE VESTIDO.
            </label>
        </td>
    </tr>
    <tr>
        <td bgcolor="grey">IR AO BANHEIRO</td>
        <td colspan="2" align="left">
                ' .
                (
                $scoreNead != "" &&
                isset($scoreNead["katz"]["banheiro"]) &&
                $scoreNead["katz"]["banheiro"] == "1" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
                )
                . '
                        <label for="banheiro-0">
                DIRIGE-SE AO BANHEIRO, ENTRA E SAI DO MESMO, ARRUMA SUAS PRÓPRIAS ROUPAS, LIMPA A ÁREA
                GENITAL SEM AJUDA.
            </label>
        </td>
        <td align="left" colspan="2">
                ' .
                (
                $scoreNead != "" &&
                isset($scoreNead["katz"]["banheiro"]) &&
                $scoreNead["katz"]["banheiro"] == "0" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
                )
                . '
            <label for="banheiro-1">
                NECESSITA DE AJUDA PARA IR AO BANHEIRO, LIMPAR-SE OU USA URINOL OU COMADRE.
            </label>
        </td>
    </tr>
    <tr>
        <td bgcolor="grey">TRANSFERÊNCIA</td>
        <td colspan="2" align="left">
                ' .
                (
                $scoreNead != "" &&
                isset($scoreNead["katz"]["transferencia"]) &&
                $scoreNead["katz"]["transferencia"] == "1" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
                )
                . '
                        <label for="transferencia-0">
                SENTA-SE, DEITA-SE E SE LEVANTA DA CAMA OU CADEIRA SEM AJUDA. EQUIPAMENTOS MECÂNICOS DE AJUDA
                SÃO ACEITÁVEIS.
            </label>
        </td>
        <td align="left" colspan="2">
                ' .
                (
                $scoreNead != "" &&
                isset($scoreNead["katz"]["transferencia"]) &&
                $scoreNead["katz"]["transferencia"] == "0" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
                )
                . '
            <label for="transferencia-1">
                NECESSITA DE AJUDA PARA SENTAR-SE, DEITAR-SE OU SE LEVANTAR DA CAMA OU CADEIRA.
            </label>
        </td>
    </tr>
    <tr>
        <td bgcolor="grey">CONTINÊNCIA</td>
        <td colspan="2" align="left">
                ' .
                (
                $scoreNead != "" &&
                isset($scoreNead["katz"]["continencia"]) &&
                $scoreNead["katz"]["continencia"] == "1" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
                )
                . '
                        <label for="continencia-0">
                TEM COMPLETO CONTROLE SOBRE SUAS ELIMINAÇÕES (URINAR E EVACUAR).
            </label>
        </td>
        <td align="left" colspan="2">
                ' .
                (
                $scoreNead != "" &&
                isset($scoreNead["katz"]["continencia"]) &&
                $scoreNead["katz"]["continencia"] == "0" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
                )
                . '
            <label for="continencia-1">
                É PARCIAL OU TOTALMENTE INCONTINENTE DO INTESTINO OU BEXIGA.
            </label>
        </td>
    </tr>
    <tr>
        <td bgcolor="grey">ALIMENTAÇÃO</td>
        <td colspan="2" align="left">
                ' .
                (
                $scoreNead != "" &&
                isset($scoreNead["katz"]["alimentacao"]) &&
                $scoreNead["katz"]["alimentacao"] == "1" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
                )
                . '
            <label for="alimentacao-0">
                LEVA A COMIDA DO PRATO À BOCA SEM AJUDA. PREPARAÇÃO DA COMIDA PODE SER FEITA POR OUTRA PESSOA.
            </label>
        </td>
        <td align="left" colspan="2">
                ' .
                (
                $scoreNead != "" &&
                isset($scoreNead["katz"]["alimentacao"]) &&
                $scoreNead["katz"]["alimentacao"] == "0" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
                )
                . '
            <label for="alimentacao-1">
                NECESSITA DE AJUDA PARCIAL OU TOTAL COM A ALIMENTAÇÃO OU REQUER ALIMENTAÇÃO PARENTERAL.
            </label>
        </td>
    </tr>
    <tr>
        <td align="center" colspan="5" style="font-size: 14px !important;">
            PONTUAÇÃO KATZ: <strong id="pontuacao-katz" pontuacao="0">' . $totalKatz . '</strong>
        </td>
    </tr>
    <tr bgcolor="#5f9ea0">
        <td colspan="5">CLASSIFICAÇÃO KATZ</td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            5 OU 6 - INDEPENDENTE
        </td>
        <td colspan="2" align="center">
            3 OU 4 - DEPENDÊNCIA PARCIAL
        </td>
        <td align="center">
            1 OU 2 - DEPENDENTE TOTAL
        </td>
    </tr>
    </table>
    <pagebreak>
    <table class="mytable" width="100%">
    <tr bgcolor="#5f9ea0">
        <td colspan="5">GRUPO 1 - ELEGIBILIDADE</td>
    </tr>
    <tr bgcolor="grey">
        <td colspan="2">ELEGIBILIDADE AO ATENDIMENTO DOMICILIAR</td>
        <td align="center" bgcolor="#228b22">SIM</td>
        <td align="center" colspan="2">NÃO</td>
    </tr>
    <tr>
        <td colspan="2">Apresenta cuidador em período integral?</td>
        <td align="center">
            ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"]["cuidado_periodo_integral"]) &&
            $scoreNead["grupos"]["cuidado_periodo_integral"] == "1" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
        </td>
        <td align="center" colspan="2">
            ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"]["cuidado_periodo_integral"]) &&
            $scoreNead["grupos"]["cuidado_periodo_integral"] == "0" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
        </td>
    </tr>
    <tr>
        <td colspan="2">O domicílio é livre de risco?</td>
        <td align="center">
            ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"]["domicilio_livre_risco"]) &&
            $scoreNead["grupos"]["domicilio_livre_risco"] == "1" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
        </td>
        <td align="center" colspan="2">
            ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"]["domicilio_livre_risco"]) &&
            $scoreNead["grupos"]["domicilio_livre_risco"] == "0" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
        </td>
    </tr>
    <tr>
        <td colspan="2">Existe algum impedimento para se deslocar até a rede credenciada?</td>
        <td align="center">
            ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"]["impedimento_rede_credenciada"]) &&
            $scoreNead["grupos"]["impedimento_rede_credenciada"] == "1" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
        </td>
        <td align="center" colspan="2">
            ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"]["impedimento_rede_credenciada"]) &&
            $scoreNead["grupos"]["impedimento_rede_credenciada"] == "0" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
        </td>
    </tr>
    <tr bgcolor="grey" align="center">
        <td colspan="5">
            Se responder <strong>"NÃO"</strong> a qualquer uma das questões acima,
            considerar contraindicar Atenção Domiciliar.
        </td>
    </tr>
    <tr bgcolor="#5f9ea0">
        <td colspan="5">GRUPO 2 - CRITÉRIOS PARA A INDICAÇÃO IMEDIATA DE INTERNAÇÃO DOMICILIAR</td>
    </tr>
    <tr>
        <td rowspan="2">&nbsp;</td>
        <td align="center" bgcolor="grey" colspan="4">PERFIL DE INTERNAÇÃO DOMICILIAR</td>
    </tr>
    <tr>
        <td align="center" bgcolor="#228b22">24 HORAS</td>
        <td align="center" bgcolor="#228b22">12 HORAS</td>
        <td align="center" bgcolor="#228b22" colspan="2">ATENDIMENTO DOMICILIAR / OUTROS PROGRAMAS</td>
    </tr>
    <tr>
        <td>ALIMENTAÇÃO PARENTERAL</td>
        <td align="center">
            ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"]["alimentacao_parenteral"]) &&
            $scoreNead["grupos"]["alimentacao_parenteral"] == "0" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
            <label for="alimentacao-parenteral-0">Por mais de 12 horas/dia</label>
        </td>
        <td align="center">
            ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"]["alimentacao_parenteral"]) &&
            $scoreNead["grupos"]["alimentacao_parenteral"] == "1" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
            <label for="alimentacao-parenteral-1">Até 12 horas/dia</label>
        </td>
        <td align="center" colspan="2">
            ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"]["alimentacao_parenteral"]) &&
            $scoreNead["grupos"]["alimentacao_parenteral"] == "2" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
            <label for="alimentacao-parenteral-2">Não utiliza</label>
        </td>
    </tr>
    <tr>
        <td>ASPIRAÇÃO DE TRAQUEOSTOMIA / VIAS AÉREAS INFERIORES</td>
        <td align="center">
            ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"]["aspiracao"]) &&
            $scoreNead["grupos"]["aspiracao"] == "0" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
            <label for="aspiracao-0">Mais de 5 vezes/dia</label>
        </td>
        <td align="center">
            ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"]["aspiracao"]) &&
            $scoreNead["grupos"]["aspiracao"] == "1" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
            <label for="aspiracao-1">Até 5 vezes/dia</label>
        </td>
        <td align="center" colspan="2">
            ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"]["aspiracao"]) &&
            $scoreNead["grupos"]["aspiracao"] == "2" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
            <label for="aspiracao-2">Não utiliza</label>
        </td>
    </tr>
    <tr>
        <td>VENTILAÇÃO MECÂNICA CONTÍNUA INVASIVA OU NÃO</td>
        <td align="center">
            ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"]["ventilacao_mecanica"]) &&
            $scoreNead["grupos"]["ventilacao_mecanica"] == "0" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
            <label for="ventilacao-mecanica-0">Por mais de 12 horas/dia</label>
        </td>
        <td align="center">
            ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"]["ventilacao_mecanica"]) &&
            $scoreNead["grupos"]["ventilacao_mecanica"] == "1" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
            <label for="ventilacao-mecanica-1">Até 12 horas/dia</label>
        </td>
        <td align="center" colspan="2">
            ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"]["ventilacao_mecanica"]) &&
            $scoreNead["grupos"]["ventilacao_mecanica"] == "2" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
            <label for="ventilacao-mecanica-2">Não utiliza</label>
        </td>
    </tr>
    <tr>
        <td>MEDICAÇÃO PARENTERAL OU HIPODERMÓCLISE</td>
        <td align="center" colspan="2">
            ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"]["medicacao_hipodermoclise"]) &&
            $scoreNead["grupos"]["medicacao_hipodermoclise"] == "0" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
            <label for="medicacao-hipodermoclise-0">Mais de 4 vezes/dia</label>
        </td>
        <td align="center" colspan="2">
            ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"]["medicacao_hipodermoclise"]) &&
            $scoreNead["grupos"]["medicacao_hipodermoclise"] == "2" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
            <label for="medicacao-hipodermoclise-2">Até 4 vezes/dia</label>
        </td>
    </tr>
    <tr bgcolor="grey" align="center">
        <td colspan="5">
            Para indicação de Planejamento de Atenção Domiciliar (P.A.D.),
            <strong>considerar a maior complexidade assinalada</strong>, ainda que uma única vez.
        </td>
    </tr>

    <tr bgcolor="#5f9ea0">
        <td colspan="5">GRUPO 3 - CRITÉRIOS DE APOIO PARA INDICAÇÃO DE PLANEJAMENTO DE ATENÇÃO DOMICILIAR</td>
    </tr>';

        $scores = \App\Models\Administracao\Score::getScoreByGrupo($grupo);
        $score = [];
        $i = 0;
        $grupoId = 0;
        $totalGrupo3 =0;
        foreach ($scores as $row) {
            if($grupoId != $row["GRUPO_ID"]){
                $grupoId = $row["GRUPO_ID"];
                $i = 0;
            }
            $score[$row["grupo"]][$i]["grupo_id"] = $row["GRUPO_ID"];
            $score[$row["grupo"]][$i]["item_id"] = $row["ID"];
            $score[$row["grupo"]][$i]["descricao"] = $row["DESCRICAO"];
            $score[$row["grupo"]][$i]["peso"] = $row["PESO"];
            $score[$row["grupo"]][$i]["qtd"] = $row["QTD"];
            $i++;
        }

        $colspan = false;
        $j = 1;
        foreach($score as $nome => $grupo):
            $i = 0;
            $colspan = false;
            $html .= '<tr>
                        <td bgcolor="grey">' .  $nome . '</td>';

            foreach($grupo as $item) :
                if($item["qtd"] == "3" && $i == 2) {
                    $colspan = true;
                    $colN = 2;
                }
                if($item["qtd"] == "2" && $i == 1) {
                    $colspan = true;
                    $colN = 3;
                }
                $descricaoTratada = strtolower(\App\Helpers\StringHelper::removeAcentosViaEReg($nome, true));
                $descricaoTratada = preg_replace('/[^\da-z]/i', '', $descricaoTratada);
                $slices = explode(" ", $descricaoTratada);
                $radioId = implode("-", $slices) . "-" . $item["item_id"];
                $radioName = implode("_", $slices);

                $html .= '<td align="center" ' .  ($colspan ? "colspan='{$colN}'" : "") . '>
                    ' .
            (
            $scoreNead != "" &&
            isset($scoreNead["grupos"][$radioName]) &&
            $scoreNead["grupos"][$radioName] == $item["peso"] ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>"
            )
            . '
                    <label for="' .  $radioId . '">' .  $item["descricao"] . '</label>
                </td>';
                $i++;
                if($scoreNead != "" &&  isset($scoreNead["grupos"][$radioName]) && $scoreNead["grupos"][$radioName] == $item["peso"]){
                    $totalGrupo3 += $item["peso"];
                };
            endforeach;

            $j++;
            $html .= '</tr>';
        endforeach;

        $html .= '<tr>
        <td align="center" colspan="5" style="font-size: 14px !important;">
            PONTUAÇÃO FINAL: <strong id="pontuacao-nead-2016" pontuacao="0">' .  $totalGrupo3 . '</strong>
        </td>
    </tr>
    <tr bgcolor="#5f9ea0">
        <td colspan="5">CLASSIFICAÇÃO DO PACIENTE</td>
    </tr>
    <tr>
        <td style="border-right: none !important;">Até 5 pontos</td>
        <td colspan="2" style="border-left: none !important;">
            Considerar procedimentos pontuais exclusivos ou outros programas: <br>
            ' .  ($classificacao_paciente != "" && isset($classificacao_paciente) && $classificacao_paciente == "0" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>" ) . '
            <label for="cp-curativos">Curativos<strong></strong></label>
            ' .  ($classificacao_paciente != "" && isset($classificacao_paciente) && $classificacao_paciente == "1" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>" ) . '
            <label for="cp-medicacoes">Medicações Parenterais<strong></strong></label>
            ' .  ($classificacao_paciente != "" && isset($classificacao_paciente) && $classificacao_paciente == "2" ? "<img src='../utils/processada_16x16.jpg' width='12'>" : "<img src='../utils/cancelada_16x16.jpg' width='12'>" ) . '
            <label for="cp-outros">Outros Programas <strong></strong></label>
        </td>
        <td style="border-right: none !important;">De 12 a 17 pontos</td>
        <td colspan="2" style="border-left: none !important;">
            Considerar <strong>Internação Domiciliar 12h</strong>
        </td>
    </tr>
    <tr>
        <td style="border-right: none !important;">De 6 a 11 pontos</td>
        <td colspan="2" style="border-left: none !important;">
            Considerar <strong>Atendimento Domiciliar Multiprofissional</strong>
            (inclui procedimentos pontuais, desde que não exclusivos)
        </td>
        <td style="border-right: none !important;">18 ou mais pontos</td>
        <td colspan="2" style="border-left: none !important;">
            Considerar <strong>Internação Domiciliar 24h</strong>
        </td>
    </tr>
</table>
<hr>';
        return $html;
    }
}
